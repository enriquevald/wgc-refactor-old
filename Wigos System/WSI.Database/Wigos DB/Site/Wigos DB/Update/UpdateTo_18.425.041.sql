/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 424;

SET @New_ReleaseId = 425;

SET @New_ScriptName = N'UpdateTo_18.425.041.sql';
SET @New_Description = N'New release v03.006.0026'; 

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO



/**** GENERAL PARAM *****/


/**** TABLES *****/

IF ((SELECT DATA_TYPE FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'pinpad_transactions_reconciliation' AND COLUMN_NAME = 'ptc_code') = 'int')
BEGIN 
	Declare @pContraintName as nvarchar(100)

	SELECT   @pContraintName = name
	  FROM   SYS.DEFAULT_CONSTRAINTS
	 WHERE   COL_NAME (PARENT_OBJECT_ID, PARENT_COLUMN_ID) = 'ptc_code'

	IF (@pContraintName IS NOT NULL)
	Begin
		EXEC('ALTER TABLE [dbo].[pinpad_transactions_reconciliation] DROP CONSTRAINT ' + @pContraintName)
	End

	ALTER TABLE pinpad_transactions_reconciliation ALTER COLUMN ptc_code bigint;

	ALTER TABLE pinpad_transactions_reconciliation ADD  DEFAULT ((0)) FOR [ptc_code]
END

GO


/**** RECORDS *****/

IF NOT EXISTS ( SELECT 1 FROM CAGE_CONCEPTS WHERE CC_CONCEPT_ID = 21)
BEGIN
	INSERT INTO cage_concepts 
			      ( cc_concept_id
			      , cc_description
			      , cc_is_provision
			      , cc_show_in_report
			      , cc_unit_price
			      , cc_name
			      , cc_type
			      , cc_enabled
			      , cc_sequence_id
			      , cc_operation_code
			      , cc_voucher_type
            )
    VALUES  ( 21          -- cc_concept_id - bigint
            , 'Unclaimed' -- cc_description - nvarchar(50)
            , 0           -- cc_is_provision - bit
            , 1           -- cc_show_in_report - bit
            , NULL        -- cc_unit_price - money
            , 'Unclaimed' -- cc_name - nvarchar(20)
            , 1           -- cc_type - int
            , 1           -- cc_enabled - bit
            , NULL        -- cc_sequence_id - int
            , NULL        -- cc_operation_code - int
            , NULL        -- cc_voucher_type - int
            )
END

IF NOT EXISTS ( SELECT 1 FROM CAGE_SOURCE_TARGET_CONCEPTS WHERE CSTC_CONCEPT_ID = 21)
BEGIN
  INSERT INTO cage_source_target_concepts
            ( cstc_concept_id 
            , cstc_source_target_id 
            , cstc_type 
            , cstc_only_national_currency 
            , cstc_cashier_action 
            , cstc_cashier_container 
            , cstc_cashier_btn_group 
            , cstc_enabled 
            , cstc_price_factor
            )
    VALUES  ( 21                        -- cstc_concept_id - bigint
            , 10                        -- cstc_source_target_id - bigint
            , 0                         -- cstc_type - int
            , 0                         -- cstc_only_national_currency - bit
            , 0                         -- cstc_cashier_action - int
            , 0                         -- cstc_cashier_container - int
            , 'STR_CASHIER_CONCEPTS_IN' -- cstc_cashier_btn_group - varchar(50)
            , 1                         -- cstc_enabled - bit
            , 1                         -- cstc_price_factor - money
            )          
END    


/**** PROCEDURES *****/

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID('GetCageMovementType') AND type in ('P', 'PC'))
DROP PROCEDURE GetCageMovementType
GO

CREATE PROCEDURE GetCageMovementType
 (@pStarDateTime DATETIME,
  @pEndDateTime  DATETIME)
AS
BEGIN
  
  CREATE TABLE #TABLE_ISO_CODE 
  ( 
    id INT Identity(1,1),
    iso_code_column VARCHAR(5),
    iso_code VARCHAR(3),
    data_type INTEGER
  ) 
  DECLARE @_count INTEGER	
  DECLARE @_count_aux INTEGER	
  DECLARE @_isoCodeLocal AS NVARCHAR(MAX)
  DECLARE @cols AS NVARCHAR(MAX)
  DECLARE @cols_type_1 AS NVARCHAR(MAX)
  DECLARE @cols_type_2 AS NVARCHAR(MAX) 
  DECLARE @colsFormat AS NVARCHAR(MAX)   
  DECLARE @query  AS VARCHAR(MAX) 
  DECLARE @_counter INTEGER    
  DECLARE @_isoCode NVARCHAR(MAX)
  DECLARE @_isoCodeColumn NVARCHAR(MAX)
  DECLARE @_dataType INTEGER
  DECLARE @_queryColumns NVARCHAR(MAX)
  DECLARE @_activeDualCurrency NVARCHAR(1)
 
  -- Get All IsoCodes
  INSERT INTO #TABLE_ISO_CODE
  SELECT
    CD.CMD_ISO_CODE AS CMD_ISO_CODE_COLUMN
   ,CD.CMD_ISO_CODE AS CMD_ISO_CODE
   ,1
  FROM CAGE_MOVEMENTS AS CM
  LEFT JOIN CAGE_MOVEMENT_DETAILS AS CD ON CM.CGM_MOVEMENT_ID = CD.CMD_MOVEMENT_ID 
  WHERE   CM.CGM_MOVEMENT_DATETIME >= @pStarDateTime
          AND CM.CGM_MOVEMENT_DATETIME < @pEndDateTime 
          AND CD.CMD_ISO_CODE IS NOT NULL
          AND CD.CMD_ISO_CODE NOT IN ('X01')
          AND CD.CMD_QUANTITY > 0
          AND CM.CGM_STATUS NOT IN(0, 1)
          AND CM.CGM_TYPE NOT IN(200)
  GROUP BY CD.CMD_ISO_CODE
  UNION 
  SELECT
   (CD.CMD_ISO_CODE + '_0') AS CMD_ISO_CODE
   ,CD.CMD_ISO_CODE
    ,2
  FROM CAGE_MOVEMENTS AS CM
  LEFT JOIN CAGE_MOVEMENT_DETAILS AS CD ON CM.CGM_MOVEMENT_ID = CD.CMD_MOVEMENT_ID 
  WHERE  CM.CGM_MOVEMENT_DATETIME >= @pStarDateTime
         AND CM.CGM_MOVEMENT_DATETIME < @pEndDateTime
         AND CD.CMD_ISO_CODE IS NOT NULL
         AND CD.CMD_ISO_CODE NOT IN ('X01')   
         AND CD.CMD_QUANTITY IN(-1, -2, -100)
         AND CM.CGM_STATUS NOT IN(0, 1)
         AND CM.CGM_TYPE NOT IN(200)
  GROUP BY CD.CMD_ISO_CODE
  ORDER BY CMD_ISO_CODE
  
    -- Get current IsoCode for GENERAL_PARAMS
  SET @_isoCodeLocal = (SELECT GP.GP_KEY_VALUE 
                        FROM GENERAL_PARAMS AS GP 
                        WHERE GP.GP_GROUP_KEY = 'RegionalOptions' AND GP.GP_SUBJECT_KEY = 'CurrencyISOCode')
  
  SET @_activeDualCurrency = (SELECT CAST(GP.GP_KEY_VALUE AS BIT)  
                           FROM GENERAL_PARAMS AS GP 
                           WHERE GP.GP_GROUP_KEY = 'FloorDualCurrency' AND GP.GP_SUBJECT_KEY = 'Enabled')
      
  CREATE TABLE #GET_CAGE_MOVEMENT_TYPE_TABLE(        
         CMT_MOVEMENT_ID BIGINT,
         CMT_MOVEMENT_DATETIME DATETIME, 
         CMT_TYPE INTEGER,
         CMT_TYPE_NAME NVARCHAR(MAX),       
         CMT_STATUS_NAME NVARCHAR(MAX),
         CMT_USER_NAME NVARCHAR(MAX),
         CMT_CAGE_USER_NAME NVARCHAR(MAX),
         CMT_MACHINE_NAME NVARCHAR(MAX),  
         CMT_COLLECTION_ID BIGINT,
         CMT_TERMINAL_ID INTEGER,
         CMT_CHIPS_AMOUNT MONEY
    )   
        
    SET @_counter = 1		
    SET @_count = (SELECT COUNT(*) FROM #TABLE_ISO_CODE) + 1
    SET @_queryColumns = ''
    SET @cols = ''   
    SET @cols_type_1 = ''  
    SET @cols_type_2 = ''  
    SET @colsFormat =''
    WHILE NOT @_counter = @_count
    BEGIN

      SELECT 
        @_isoCode  =  TIC.iso_code,
        @_isoCodeColumn = TIC.iso_code_column,
        @_dataType = TIC.data_type  
      FROM #TABLE_ISO_CODE AS TIC
      WHERE TIC.id = @_counter

      -- Add column dynamic to #GET_CAGE_MOVEMENT_TYPE_TABLE for isocode
      SET @_queryColumns = 'ALTER TABLE #GET_CAGE_MOVEMENT_TYPE_TABLE ADD [' + @_isoCodeColumn + '] MONEY '
      EXEC SP_EXECUTESQL @_queryColumns
      
      --Get colums isocode
      
      IF @_dataType = 1
        BEGIN
            SET @cols = @cols + ',AM.'+ @_isoCode
            SET @cols_type_1 = @cols_type_1 + ',' + @_isoCode
            SET @colsFormat = @colsFormat + ',ISNULL('+@_isoCodeColumn+', 0) AS '+@_isoCode+''
        END
      IF @_dataType = 2
        BEGIN
            SET @cols = @cols + ',AZ.'+@_isoCode+''
            IF @cols_type_2 = ''
              BEGIN
                SET @cols_type_2 = @_isoCode 
              END
            ELSE
              BEGIN            
                SET @cols_type_2 = @cols_type_2 + ',' + @_isoCode 
              END
            SET @colsFormat = @colsFormat + ',ISNULL('+@_isoCodeColumn+', 0) AS ''Otros ('+@_isoCode+')'''
         END
     
      SET @_counter = @_counter + 1

    END  
                          
   SET @query = '   
            INSERT INTO #GET_CAGE_MOVEMENT_TYPE_TABLE 
            SELECT
             CM.CGM_MOVEMENT_ID   
            ,CM.CGM_MOVEMENT_DATETIME
            ,CM.CGM_TYPE
            ,CASE 
                WHEN CM.CGM_TYPE = 0   THEN ''Env�o a Cajero''                           WHEN CM.CGM_TYPE = 1   THEN ''Env�o a destino personalizado''
                WHEN CM.CGM_TYPE = 2   THEN ''Cierre de b�veda''                         WHEN CM.CGM_TYPE = 3   THEN ''Arqueo de b�veda''
                WHEN CM.CGM_TYPE = 4   THEN ''Env�o a mesa de juego''                    WHEN CM.CGM_TYPE = 5   THEN ''Env�o a terminal''
                WHEN CM.CGM_TYPE = 99  THEN ''P�rdida de cuadratura''                    WHEN CM.CGM_TYPE = 100 THEN ''Recaudaci�n de Cajero'' 
                WHEN CM.CGM_TYPE = 101 THEN ''Recaudaci�n de origen personalizado''      WHEN CM.CGM_TYPE = 102 THEN ''Apertura de b�veda''
                WHEN CM.CGM_TYPE = 103 THEN ''Recaudaci�n de mesa de juego''             WHEN CM.CGM_TYPE = 104 THEN ''Recaudaci�n de terminal''
                WHEN CM.CGM_TYPE = 105 THEN ''Reapertura de b�veda''                     WHEN CM.CGM_TYPE = 106 THEN ''Recaudaci�n de drop box de mesa de juego''		
				WHEN CM.CGM_TYPE = 107 THEN ''Recaudaci�n de drop box de mesa de juego'' WHEN CM.CGM_TYPE = 108 THEN ''Recaudaci�n de cierre de Cajero''	
				WHEN CM.CGM_TYPE = 109 THEN ''Recaudaci�n de cierre de Mesa de Juego''	 WHEN CM.CGM_TYPE = 199 THEN ''Ganancia de cuadratura''
                WHEN CM.CGM_TYPE = 200 THEN ''Solicitud desde Cajero''                   WHEN CM.CGM_TYPE = 300 THEN ''Provisi�n de progresivo''
                WHEN CM.CGM_TYPE = 301 THEN ''Progresivo otorgado''                      WHEN CM.CGM_TYPE = 302 THEN ''(Anulaci�n)Provisi�n de progresivo''
                WHEN CM.CGM_TYPE = 303 THEN ''(Anulaci�n)Progresivo otorgado''           ELSE CAST(CM.CGM_TYPE AS NVARCHAR(MAX))
             END AS CGM_TYPE_NAME
            ,CASE
                WHEN CM.CGM_STATUS = 0 AND CM.CGM_TYPE IN (0, 4)                         THEN ''Enviado''
                WHEN CM.CGM_STATUS = 0 AND CM.CGM_TYPE IN (200)                          THEN ''Solicitud pendiente''
                WHEN CM.CGM_STATUS = 0 AND CM.CGM_TYPE IN (5, 100, 103, 104)             THEN ''Pendiente recaudar''            
                WHEN CM.CGM_STATUS = 2 AND CM.CGM_TYPE IN (0, 4, 5, 300, 301, 302, 303)  THEN ''Finalizado''
                WHEN CM.CGM_STATUS = 2 AND CM.CGM_TYPE IN (100, 103, 104)                THEN ''Recaudado''
                WHEN CM.CGM_STATUS = 2 AND CM.CGM_TYPE IN (200)                          THEN ''Solicitud tramitada''            
                WHEN CM.CGM_STATUS = 1 AND CM.CGM_TYPE IN (200)                          THEN ''Solicitud cancelada''
                WHEN CM.CGM_STATUS = 1 AND CM.CGM_TYPE NOT IN (200)                      THEN ''Cancelado''            
                WHEN CM.CGM_STATUS = 3 AND CM.CGM_TYPE IN (0)                            THEN ''Finalizado con descuadre denominaciones''
                WHEN CM.CGM_STATUS = 3 AND CM.CGM_TYPE NOT IN (0)                        THEN ''Recaudado con descuadre denominaciones''            
                WHEN CM.CGM_STATUS = 4 AND CM.CGM_TYPE IN (0)                            THEN ''Finalizado con descuadre del monto''
                WHEN CM.CGM_STATUS = 4 AND CM.CGM_TYPE NOT IN (0)                        THEN ''Recaudado con descuadre total''            
                WHEN CM.CGM_STATUS IN (9, 11, 12)                                        THEN ''Finalizado''            
                WHEN CM.CGM_STATUS = 10                                                  THEN ''Recaudado''            
                WHEN CM.CGM_STATUS = 13                                                  THEN ''(Cancelado) Pendiente recaudar'' 
                ELSE ''---''
             END AS CGM_STATUS_NAME ' 
        SET @query += 
		   ',ISNULL(GU.GU_FULL_NAME, '''') AS GU_FULL_NAME
            ,ISNULL(GC.GU_FULL_NAME,'''') AS GU_CAGE_FULL_NAME
            ,ISNULL(ISNULL(CA.CT_NAME, CT.CT_NAME),TE.TE_NAME) AS CGM_NAME   
            ,CM.CGM_MC_COLLECTION_ID
            ,CM.CGM_TERMINAL_CASHIER_ID
            ,X01 
            ' + @cols + '
            FROM CAGE_MOVEMENTS AS CM
            LEFT JOIN GUI_USERS AS GU ON GU.GU_USER_ID = CM.CGM_USER_CASHIER_ID  
            LEFT JOIN GUI_USERS AS GC ON GC.GU_USER_ID = CM.CGM_USER_CAGE_ID 
            LEFT JOIN CASHIER_TERMINALS AS CA ON CA.CT_CASHIER_ID = CM.CGM_TERMINAL_CASHIER_ID
            LEFT JOIN CASHIER_SESSIONS AS CS ON CS.CS_SESSION_ID = CM.CGM_CASHIER_SESSION_ID  
            LEFT JOIN CASHIER_TERMINALS AS CT ON CT.CT_CASHIER_ID = CS.CS_CASHIER_ID
            LEFT JOIN TERMINALS AS TE ON TE.TE_TERMINAL_ID = CM.CGM_TERMINAL_CASHIER_ID      
            LEFT JOIN (
                SELECT * FROM(
                              SELECT  
                                     CM.CGM_MOVEMENT_ID 
                                    ,CASE
                                        WHEN CM.CGM_TYPE IN(0, 1, 4, 5) THEN (CD.CMD_QUANTITY * CD.CMD_DENOMINATION * -1)                                    
                                        ELSE (CD.CMD_QUANTITY * CD.CMD_DENOMINATION)
                                     END AS CD_AMOUNT
                                    ,CD.CMD_ISO_CODE
                               FROM CAGE_MOVEMENTS AS CM
                                    LEFT JOIN CAGE_MOVEMENT_DETAILS AS CD 
                                    ON CM.CGM_MOVEMENT_ID = CD.CMD_MOVEMENT_ID 
                               WHERE  CD.CMD_ISO_CODE IS NOT NULL AND CD.CMD_QUANTITY > 0
                             ) AS PU        
                             PIVOT
                             (
                                SUM(CD_AMOUNT)
                                FOR CMD_ISO_CODE IN(X01' + @cols_type_1 + ')
                             ) AS PVT 
            ) AS AM ON CM.CGM_MOVEMENT_ID = AM.CGM_MOVEMENT_ID
   '     
   IF @cols_type_2 <> ''
     BEGIN
       SET @query += ' 
            LEFT JOIN (
                SELECT * FROM(
                            SELECT  
                                   CM.CGM_MOVEMENT_ID 
                                  ,CASE
                                      WHEN CM.CGM_TYPE IN(0, 1, 4, 5) THEN (CD.CMD_DENOMINATION * -1)                                    
                                      ELSE CD.CMD_DENOMINATION
                                   END AS CD_AMOUNT
                                  ,CD.CMD_ISO_CODE
                             FROM CAGE_MOVEMENTS AS CM
                                  LEFT JOIN CAGE_MOVEMENT_DETAILS AS CD 
                                  ON CM.CGM_MOVEMENT_ID = CD.CMD_MOVEMENT_ID 
                             WHERE  CD.CMD_ISO_CODE IS NOT NULL AND CD.CMD_QUANTITY IN(-1, -2, -100) AND CD.CMD_ISO_CODE NOT IN (''X01'')
                           ) AS PU        
                           PIVOT
                           (
                              SUM(CD_AMOUNT)
                              FOR CMD_ISO_CODE IN('+@cols_type_2+')
                           ) AS PVT             
            ) AS AZ ON CM.CGM_MOVEMENT_ID = AZ.CGM_MOVEMENT_ID
       '
     END
   
   SET @query += '       
        WHERE CM.CGM_TYPE NOT IN(200) AND CM.CGM_STATUS NOT IN(0, 1) AND CM.CGM_MOVEMENT_DATETIME >= ''' + CONVERT(VARCHAR, @pStarDateTime, 21) + '''
        AND CM.CGM_MOVEMENT_DATETIME <  ''' + CONVERT(VARCHAR, @pEndDateTime, 21) + '''   
        ORDER BY CM.CGM_MOVEMENT_ID    
   '       

   EXEC (@query) 
   
   CREATE TABLE #TABLE_MACHINE_ISO_CODE 
   ( 
      id INT Identity(1,1),
      iso_code VARCHAR(5)
   ) 
   
   INSERT INTO #TABLE_MACHINE_ISO_CODE
   SELECT TE.TE_ISO_CODE FROM(SELECT 
      CM.CMT_MOVEMENT_ID
     ,CASE
         WHEN @_activeDualCurrency = '1'
           THEN ISNULL(TE_ISO_CODE,@_isoCodeLocal)
         ELSE @_isoCodeLocal
      END AS TE_ISO_CODE 
      FROM #GET_CAGE_MOVEMENT_TYPE_TABLE  AS CM
      INNER JOIN MONEY_COLLECTIONS AS MCU
      ON CM.CMT_COLLECTION_ID = MCU.MC_COLLECTION_ID AND CM.CMT_TYPE IN (5, 104) 
      LEFT JOIN TERMINALS AS TE
      ON CM.CMT_TERMINAL_ID = TE.TE_TERMINAL_ID 
      WHERE ((MCU.MC_COLLECTED_BILL_AMOUNT IS NOT NULL AND MCU.MC_COLLECTED_BILL_AMOUNT > 0) OR
                            (MCU.MC_EXPECTED_BILL_AMOUNT IS NOT NULL AND MCU.MC_EXPECTED_BILL_AMOUNT > 0) OR
                            (MCU.MC_COLLECTED_COIN_AMOUNT IS NOT NULL AND MCU.MC_COLLECTED_COIN_AMOUNT > 0) OR
                            (MCU.MC_EXPECTED_COIN_AMOUNT IS NOT NULL AND MCU.MC_EXPECTED_COIN_AMOUNT > 0))
      )AS TE
   GROUP BY TE.TE_ISO_CODE 
      
   SET @_counter = 1		
   SET @_count_aux = (SELECT COUNT(*) FROM #TABLE_MACHINE_ISO_CODE) + 1
   
   WHILE NOT @_counter = @_count_aux
     BEGIN
    
      SELECT 
        @_isoCode  =  TIC.iso_code
      FROM #TABLE_MACHINE_ISO_CODE AS TIC
      WHERE TIC.id = @_counter
      
      SET @_count =(SELECT COUNT(*) FROM #TABLE_ISO_CODE AS tic 
                                    WHERE tic.iso_code = @_isoCode AND tic.data_type = 1)                   
      IF @_count = 0
      BEGIN
        SET @_queryColumns = 'ALTER TABLE #GET_CAGE_MOVEMENT_TYPE_TABLE ADD [' + @_isoCode + '] MONEY '
        EXEC SP_EXECUTESQL @_queryColumns
        SET @colsFormat = @colsFormat + ',ISNULL('+@_isoCode+', 0) AS '+@_isoCode + '' 
      END
      
      SET @_queryColumns ='        
            UPDATE
              MTP
              SET
                MTP.'+@_isoCode+' = (CASE WHEN MTP.CMT_TYPE = 104 THEN MCU.COLLECTED_AMOUNT
                                                        ELSE MCU.EXPECTED_AMOUNT END)     
            FROM #GET_CAGE_MOVEMENT_TYPE_TABLE AS MTP 
            INNER JOIN (SELECT 
                          CM.CMT_MOVEMENT_ID
                         ,MC_EXPECTED_BILL_AMOUNT + MC_EXPECTED_COIN_AMOUNT AS EXPECTED_AMOUNT
                         ,MC_COLLECTED_BILL_AMOUNT + MC_COLLECTED_COIN_AMOUNT AS COLLECTED_AMOUNT
                         ,CASE
                             WHEN '''+@_activeDualCurrency+''' = ''1''
                               THEN ISNULL(TE_ISO_CODE,'''+@_isoCodeLocal+''')
                             ELSE '''+@_isoCodeLocal+'''
                          END AS TE_ISO_CODE 
                          FROM #GET_CAGE_MOVEMENT_TYPE_TABLE  AS CM
                          INNER JOIN MONEY_COLLECTIONS AS MCU
                          ON CM.CMT_COLLECTION_ID = MCU.MC_COLLECTION_ID AND CM.CMT_TYPE IN (5, 104) 
                          LEFT JOIN TERMINALS AS TE
                          ON CM.CMT_TERMINAL_ID = TE.TE_TERMINAL_ID 
                          WHERE ((MCU.MC_COLLECTED_BILL_AMOUNT IS NOT NULL AND MCU.MC_COLLECTED_BILL_AMOUNT > 0) OR
                                                (MCU.MC_EXPECTED_BILL_AMOUNT IS NOT NULL AND MCU.MC_EXPECTED_BILL_AMOUNT > 0) OR
                                                (MCU.MC_COLLECTED_COIN_AMOUNT IS NOT NULL AND MCU.MC_COLLECTED_COIN_AMOUNT > 0) OR
                                                (MCU.MC_EXPECTED_COIN_AMOUNT IS NOT NULL AND MCU.MC_EXPECTED_COIN_AMOUNT > 0))  
            )AS MCU ON MTP.CMT_MOVEMENT_ID = MCU.CMT_MOVEMENT_ID AND MCU.TE_ISO_CODE = '''+@_isoCode+'''         
          '
       EXEC SP_EXECUTESQL @_queryColumns      
       SET @_counter = @_counter + 1
     END  
   
   SET @_queryColumns ='   
     SELECT      
        CONVERT(char(11), CMT.CMT_MOVEMENT_DATETIME, 103) + CONVERT(char(8), CMT.CMT_MOVEMENT_DATETIME, 108) AS ''Fecha''
       ,CMT.CMT_CAGE_USER_NAME AS ''Usuario de B�veda''           
       ,CASE WHEN CMT.CMT_USER_NAME LIKE ''SYS-%'' THEN '''' ELSE ISNULL(CMT.CMT_USER_NAME,'''') END  AS ''Origen/Destino''
       ,ISNULL(CMT.CMT_MACHINE_NAME,'''') AS ''Terminal''           
       ,CMT.CMT_TYPE_NAME AS ''Tipo de movimiento''
       ,CMT.CMT_STATUS_NAME AS ''Estado''
       '+@colsFormat+'  
       ,ISNULL(CMT.CMT_CHIPS_AMOUNT, 0) AS ''Fichas''   
     FROM #GET_CAGE_MOVEMENT_TYPE_TABLE  AS CMT
     '
   EXEC SP_EXECUTESQL @_queryColumns 
   
   DROP TABLE #GET_CAGE_MOVEMENT_TYPE_TABLE  
   DROP TABLE #TABLE_MACHINE_ISO_CODE
   DROP TABLE #TABLE_ISO_CODE     
  
END
GO

GRANT EXECUTE ON GetCageMovementType TO wggui WITH GRANT OPTION 



IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MontlyReportPhilippines]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[MontlyReportPhilippines]
GO
CREATE PROCEDURE [dbo].[MontlyReportPhilippines]
(
@pMonthFrom AS DATETIME,
@pMonthTo AS DATETIME
)
AS
BEGIN 

  DECLARE @TypeUnclaimed    INT
  DECLARE @AuxDate          DATETIME
  DECLARE @Message01        VARCHAR(100)
  DECLARE @Message02        VARCHAR(100)
  DECLARE @Winnings         MONEY
  DECLARE @TypeCurrency     INT 
  DECLARE @IdLanguage       INT 
  DECLARE @TextMonth01      VARCHAR(3)
  DECLARE @TextMonth02      VARCHAR(3)
  DECLARE @TextMonth03      VARCHAR(3)
  DECLARE @TextMonth04      VARCHAR(3)
  DECLARE @TextMonth05      VARCHAR(3)
  DECLARE @TextMonth06      VARCHAR(3)
  DECLARE @TextMonth07      VARCHAR(3)
  DECLARE @TextMonth08      VARCHAR(3)
  DECLARE @TextMonth09      VARCHAR(3)
  DECLARE @TextMonth10      VARCHAR(3)
  DECLARE @TextMonth11      VARCHAR(3)
  DECLARE @TextMonth12      VARCHAR(3)
  DECLARE @TextDay01        VARCHAR(3)
  DECLARE @TextDay02        VARCHAR(3)
  DECLARE @TextDay03        VARCHAR(3)
  DECLARE @TextDay04        VARCHAR(3)
  DECLARE @TextDay05        VARCHAR(3)
  DECLARE @TextDay06        VARCHAR(3)
  DECLARE @TextDay07        VARCHAR(3)
  
  
  SET @IdLanguage = 9
  SELECT @IdLanguage = CASE LTRIM(RTRIM(ISNULL(gp_key_value,'en-US')))
                      WHEN 'es' THEN 10
                      ELSE 9 END                        
  FROM general_params
  WHERE gp_group_key = 'WigosGUI'
    AND gp_subject_key = 'Language'
 
      
  SET @pMonthFrom = dbo.Opening(0, DATEADD(DAY, (DAY(@pMonthFrom) - 1) * -1, @pMonthFrom))
  SET @pMonthTo = dbo.Opening(0, DATEADD(DAY, -1, DATEADD(MONTH, 1, DATEADD(DAY, (DAY(@pMonthTo) - 1) * -1, @pMonthTo))))
  SET @TypeUnclaimed = 1000021
  SET @TypeCurrency = 0
  SET @TextMonth02 = 'Feb'
  SET @TextMonth03 = 'Mar'
  SET @TextMonth05 = 'May'
  SET @TextMonth06 = 'Jun'
  SET @TextMonth07 = 'Jul'
  SET @TextMonth09 = 'Sep'      
  SET @TextMonth10 = 'Oct'
  SET @TextMonth11 = 'Nov'
      
  IF @IdLanguage = 10
  BEGIN
    SET @Message01 = 'La fecha final debe ser mayor que la fecha inicial'
    SET @Message02 = 'No hay datos que cumplan las condiciones de b�squeda'
    SET @TextMonth01  = 'Ene'
    SET @TextMonth04  = 'Abr'
    SET @TextMonth08  = 'Ago'
    SET @TextMonth12  = 'Dic'
    SET @TextDay01    = 'Dom'
    SET @TextDay02    = 'Lun'
    SET @TextDay03    = 'Mar'
    SET @TextDay04    = 'Mi�'
    SET @TextDay05    = 'Jue'
    SET @TextDay06    = 'Vie'
    SET @TextDay07    = 'S�b'    
  END
  ELSE
  BEGIN
    SET @Message01 = 'Final date must be greater than end date'
    SET @Message02 = 'There is no data that meets the search criteria'
    SET @TextMonth01  = 'Jan'
    SET @TextMonth04  = 'Apr'
    SET @TextMonth08  = 'Aug'
    SET @TextMonth12  = 'Dec'
    SET @TextDay01    = 'Sun'
    SET @TextDay02    = 'Mon'
    SET @TextDay03    = 'Tue'
    SET @TextDay04    = 'Wed'
    SET @TextDay05    = 'Thu'
    SET @TextDay06    = 'Fri'
    SET @TextDay07    = 'Sat'    
  END
  
  IF @pMonthFrom > @pMonthTo
  BEGIN
    RAISERROR(@Message01, 1, 1)  
    RETURN 
  END

  -- Create temp table Meters
  CREATE TABLE #TT_METERS
      ( DAY_WEEK        DATETIME, 
        TOTAL_BET       MONEY, 
        TOTAL_JACKPOTS  MONEY,
        TOTAL_PAYOUTS   MONEY 
      ) 

  -- Create temp table Unclaimed
  CREATE TABLE #TT_UNCLAIMED
      ( DAY_WEEK        DATETIME, 
        TOTAL_UNCLAIMED MONEY
      ) 
      
  -- Create temp table Result
  CREATE TABLE #TT_RESULT
      ( TYPE_ROW        INT,
        DAY_WEEK        DATETIME,
        TOTAL_BET       MONEY,
        TOTAL_PAYOUTS   MONEY,
        TOTAL_JACKPOTS  MONEY,
        TOTAL_UNCLAIMED MONEY
      )
        
  INSERT INTO #TT_METERS       
  SELECT  /*DAY*/
        dbo.Opening(0,TSMH_DATETIME)  AS DATE
        /*BET*/
        , SUM( CASE WHEN TSMH_TYPE <> 20 THEN
                  - CASE WHEN (TSMH_METER_CODE = 0)                           THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               ELSE
                    CASE WHEN (TSMH_METER_CODE = 0)                           THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               END ) AS TOTAL_BET
        /*JACKPOTS*/
        , SUM( CASE WHEN TSMH_TYPE <> 20 THEN
                  - CASE WHEN (TSMH_METER_CODE = 2)                            THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               ELSE
                    CASE WHEN (TSMH_METER_CODE = 2)                            THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               END ) AS TOTAL_JACKPOTS
        /*PAYOUTS*/
        , SUM( CASE WHEN TSMH_TYPE <> 20 THEN
                  - CASE WHEN (TSMH_METER_CODE = 1)                            THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               ELSE
                    CASE WHEN (TSMH_METER_CODE = 1)                            THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               END ) AS TOTAL_PAYOUTS              
  FROM TERMINAL_SAS_METERS_HISTORY  WITH(INDEX(PK_terminal_sas_meters_history))
  WHERE TSMH_DATETIME >= @pMonthFrom 
    AND TSMH_DATETIME < @pMonthTo
    AND TSMH_TYPE IN ( 20   /*MINCETUR_DAILY_REPORT*/
                       , 10   /*METER_RESET*/
                       , 11   /*METER_ROLLOVER*/
                       , 15   /*METER_SERVICE_RAM_CLEAR*/
                       , 16   /*METER_MACHINE_RAM_CLEAR*/
                         )
    AND TSMH_METER_CODE IN  ( 0   /*Total IN*/
                            , 1   /*Total OUT*/
                            , 2   /*Jackpots*/
                            )
  GROUP BY TSMH_DATETIME
  
 
  INSERT INTO #TT_UNCLAIMED
  SELECT  TB.CM_DATE
        , SUM(TB.CM_AMOUNT)
  FROM (        
        SELECT  dbo.Opening(0, CM_DATE) CM_DATE
              , (SELECT ISNULL(CE_CHANGE, 1) 
                  FROM  CURRENCY_EXCHANGE
                  WHERE CE_TYPE = @TypeCurrency
                    AND CE_CURRENCY_ISO_CODE = CM_CURRENCY_ISO_CODE) * SUM(CM_ADD_AMOUNT) CM_AMOUNT   
        FROM CASHIER_MOVEMENTS WITH (INDEX (IX_cm_date_type))
        WHERE CM_TYPE = @TypeUnclaimed
          AND CM_DATE >= @pMonthFrom 
          AND CM_DATE < @pMonthTo
        GROUP BY dbo.Opening(0, CM_DATE), CM_CURRENCY_ISO_CODE   
  ) TB GROUP BY TB.CM_DATE
  
  
  SET @AuxDate = @pMonthFrom
  
  WHILE(@AuxDate <= @pMonthTo)
  BEGIN
    
    INSERT INTO #TT_RESULT (TYPE_ROW, DAY_WEEK, TOTAL_BET, TOTAL_PAYOUTS, TOTAL_JACKPOTS, TOTAL_UNCLAIMED) 
    VALUES (1, @AuxDate, 0 , 0, 0 ,0)
    
    UPDATE #TT_RESULT
    SET #TT_RESULT.TOTAL_BET = ISNULL(#TT_METERS.TOTAL_BET, 0)
      , #TT_RESULT.TOTAL_PAYOUTS = ISNULL(#TT_METERS.TOTAL_PAYOUTS, 0)
      , #TT_RESULT.TOTAL_JACKPOTS = ISNULL(#TT_METERS.TOTAL_JACKPOTS, 0)
    FROM  #TT_RESULT
    INNER JOIN #TT_METERS ON #TT_METERS.DAY_WEEK = #TT_RESULT.DAY_WEEK
    WHERE #TT_RESULT.DAY_WEEK =  @AuxDate
    
    UPDATE #TT_RESULT
    SET #TT_RESULT.TOTAL_UNCLAIMED = ISNULL(#TT_UNCLAIMED.TOTAL_UNCLAIMED, 0)
    FROM  #TT_RESULT
    INNER JOIN #TT_UNCLAIMED ON #TT_UNCLAIMED.DAY_WEEK = #TT_RESULT.DAY_WEEK
    WHERE #TT_RESULT.DAY_WEEK =  @AuxDate    
    
    SET @AuxDate = DATEADD(DAY, 1, @AuxDate)
    
    IF MONTH(@AuxDate)  <> MONTH(DATEADD(DAY, -1, @AuxDate))
    BEGIN 
      INSERT INTO #TT_RESULT 
                ( TYPE_ROW
                , DAY_WEEK
                , TOTAL_BET
                , TOTAL_PAYOUTS
                , TOTAL_JACKPOTS
                , TOTAL_UNCLAIMED
                ) 
      SELECT  2
              , DATEADD(DAY, -1, @AuxDate)
              , SUM(TOTAL_BET)
              , SUM(TOTAL_PAYOUTS)
              , SUM(TOTAL_JACKPOTS)
              , SUM(TOTAL_UNCLAIMED)
      FROM  #TT_RESULT
      WHERE MONTH(DAY_WEEK) = MONTH(DATEADD(DAY, -1, @AuxDate))
    END 
    
  END 
    
  INSERT INTO #TT_RESULT 
            ( TYPE_ROW
            , DAY_WEEK
            , TOTAL_BET
            , TOTAL_PAYOUTS
            , TOTAL_JACKPOTS
            , TOTAL_UNCLAIMED
            ) 
  SELECT  3
        , DATEADD(DAY, -1, @AuxDate)
        , SUM(TOTAL_BET)
        , SUM(TOTAL_PAYOUTS)
        , SUM(TOTAL_JACKPOTS)
        , SUM(TOTAL_UNCLAIMED)
  FROM  #TT_RESULT
  WHERE TYPE_ROW = 2
  
  SELECT @Winnings = TOTAL_BET - (TOTAL_PAYOUTS + TOTAL_JACKPOTS - TOTAL_UNCLAIMED)
  FROM #TT_RESULT
  WHERE TYPE_ROW = 3
  
  IF ISNULL(@Winnings,0) = 0
  BEGIN
    RAISERROR(@Message02, 1, 1)  
    RETURN 
  END
 
  SELECT  RTRIM(CASE  
          WHEN MONTH(DAY_WEEK) = 1 AND TYPE_ROW <> 3 THEN @TextMonth01
          WHEN MONTH(DAY_WEEK) = 2 AND TYPE_ROW <> 3 THEN @TextMonth02
          WHEN MONTH(DAY_WEEK) = 3 AND TYPE_ROW <> 3 THEN @TextMonth03
          WHEN MONTH(DAY_WEEK) = 4 AND TYPE_ROW <> 3 THEN @TextMonth04
          WHEN MONTH(DAY_WEEK) = 5 AND TYPE_ROW <> 3 THEN @TextMonth05
          WHEN MONTH(DAY_WEEK) = 6 AND TYPE_ROW <> 3 THEN @TextMonth06
          WHEN MONTH(DAY_WEEK) = 7 AND TYPE_ROW <> 3 THEN @TextMonth07
          WHEN MONTH(DAY_WEEK) = 8 AND TYPE_ROW <> 3 THEN @TextMonth08
          WHEN MONTH(DAY_WEEK) = 9 AND TYPE_ROW <> 3 THEN @TextMonth09
          WHEN MONTH(DAY_WEEK) = 10 AND TYPE_ROW <> 3 THEN @TextMonth10
          WHEN MONTH(DAY_WEEK) = 11 AND TYPE_ROW <> 3 THEN @TextMonth11
          WHEN MONTH(DAY_WEEK) = 12 AND TYPE_ROW <> 3 THEN @TextMonth12
          ELSE 'TOTAL' END + 
          CASE TYPE_ROW 
          WHEN 1 THEN '-' + CAST(DAY(DAY_WEEK) AS CHAR)
          WHEN 2 THEN ' ' + 'Total'
          ELSE '' END) DAY_MONTH
        , CASE             
          WHEN DATEPART(dw, DAY_WEEK) = 1 AND TYPE_ROW = 1 THEN @TextDay01
          WHEN DATEPART(dw, DAY_WEEK) = 2 AND TYPE_ROW = 1 THEN @TextDay02
          WHEN DATEPART(dw, DAY_WEEK) = 3 AND TYPE_ROW = 1 THEN @TextDay03
          WHEN DATEPART(dw, DAY_WEEK) = 4 AND TYPE_ROW = 1 THEN @TextDay04
          WHEN DATEPART(dw, DAY_WEEK) = 5 AND TYPE_ROW = 1 THEN @TextDay05
          WHEN DATEPART(dw, DAY_WEEK) = 6 AND TYPE_ROW = 1 THEN @TextDay06
          WHEN DATEPART(dw, DAY_WEEK) = 7 AND TYPE_ROW = 1 THEN @TextDay07
          END DAY_WEEK
      , TOTAL_BET
      , TOTAL_PAYOUTS AS TOTAL_PAYOUTS_1
      , TOTAL_UNCLAIMED
      , TOTAL_JACKPOTS
      , TOTAL_PAYOUTS + TOTAL_JACKPOTS - TOTAL_UNCLAIMED AS TOTAL_PAYOUTS_2
      , TOTAL_BET - (TOTAL_PAYOUTS + TOTAL_JACKPOTS - TOTAL_UNCLAIMED) AS WINNINGS
  FROM #TT_RESULT
  
    
  DROP TABLE #TT_METERS    
  DROP TABLE #TT_UNCLAIMED    
  DROP TABLE #TT_RESULT    
    
END

GO

GRANT EXECUTE ON [dbo].[MontlyReportPhilippines] TO wggui WITH GRANT OPTION
GO

BEGIN
  DECLARE @rtc_form_id INT
  DECLARE @rtc_design_sheet VARCHAR(MAX)
  DECLARE @rtc_design_filter VARCHAR(MAX)

  SET @rtc_design_sheet = '<ArrayOfReportToolDesignSheetsDTO>
   <ReportToolDesignSheetsDTO>
      <LanguageResources>
         <NLS09>
            <Label>Monthly Report</Label>
         </NLS09>
         <NLS10>
            <Label>Reporte mensual</Label>
         </NLS10>
      </LanguageResources>
      <Columns>
         <!--Date-->	  
         <ReportToolDesignColumn>
            <Code>DAY_MONTH</Code>
            <Width>300</Width>
            <EquityMatchType>Equality</EquityMatchType>
            <LanguageResources>
               <NLS09>
                  <Label>Date</Label>
               </NLS09>
               <NLS10>
                  <Label>Fecha</Label>
               </NLS10>
            </LanguageResources>
         </ReportToolDesignColumn>
         <!--Days-->	           
		 <ReportToolDesignColumn>
            <Code>DAY_WEEK</Code>
            <Width>300</Width>
            <EquityMatchType>Equality</EquityMatchType>
            <LanguageResources>
               <NLS09>
                  <Label>Days</Label>
               </NLS09>
               <NLS10>
                  <Label>D�as</Label>
               </NLS10>
            </LanguageResources>
         </ReportToolDesignColumn>
         <!--Total Bet-->	  
         <ReportToolDesignColumn>
            <Code>TOTAL_BET</Code>
            <Width>300</Width>
            <EquityMatchType>Equality</EquityMatchType>
            <LanguageResources>
               <NLS09>
                  <Label>Total Bet</Label>
               </NLS09>
               <NLS10>
                  <Label>Total Apuestas</Label>
               </NLS10>
            </LanguageResources>
         </ReportToolDesignColumn>
         <!--Total Payouts-->	  
         <ReportToolDesignColumn>
            <Code>TOTAL_PAYOUTS_1</Code>
            <Width>300</Width>
            <EquityMatchType>Equality</EquityMatchType>
            <LanguageResources>
               <NLS09>
                  <Label>Total - Payouts</Label>
               </NLS09>
               <NLS10>
                  <Label>Total - Pagos</Label>
               </NLS10>
            </LanguageResources>
         </ReportToolDesignColumn>
         <!--Unclaimed-->	  
         <ReportToolDesignColumn>
            <Code>TOTAL_UNCLAIMED</Code>
            <Width>300</Width>
            <EquityMatchType>Equality</EquityMatchType>
            <LanguageResources>
               <NLS09>
                  <Label>Unclaimed</Label>
               </NLS09>
               <NLS10>
                  <Label>No Reclamos</Label>
               </NLS10>
            </LanguageResources>
         </ReportToolDesignColumn>
         <!--Jackpot 20%-->	  
         <ReportToolDesignColumn>
            <Code>TOTAL_JACKPOTS</Code>
            <Width>300</Width>
            <EquityMatchType>Equality</EquityMatchType>
            <LanguageResources>
               <NLS09>
                  <Label>Jackpot 20%</Label>
               </NLS09>
               <NLS10>
                  <Label>Jackpot 20%</Label>
               </NLS10>
            </LanguageResources>
         </ReportToolDesignColumn>
         <!--Total Payouts-->	           
		 <ReportToolDesignColumn>
            <Code>TOTAL_PAYOUTS_2</Code>
            <Width>300</Width>
            <EquityMatchType>Equality</EquityMatchType>
            <LanguageResources>
               <NLS09>
                  <Label>Total Payouts</Label>
               </NLS09>
               <NLS10>
                  <Label>Total Pagos</Label>
               </NLS10>
            </LanguageResources>
         </ReportToolDesignColumn>
         <!--Winnings-->	                    
		 <ReportToolDesignColumn>
            <Code>WINNINGS</Code>
            <Width>300</Width>
            <EquityMatchType>Equality</EquityMatchType>
            <LanguageResources>
               <NLS09>
                  <Label>Winnings</Label>
               </NLS09>
               <NLS10>
                  <Label>Ganancias</Label>
               </NLS10>
            </LanguageResources>
         </ReportToolDesignColumn>
      </Columns>
   </ReportToolDesignSheetsDTO>
</ArrayOfReportToolDesignSheetsDTO>'
  
  SET @rtc_design_filter = '<ReportToolDesignFilterDTO>
   <FilterType>CustomFilters</FilterType>
   <FilterText>
     <LanguageResources>
       <NLS09>
         <Label>Months</Label>
       </NLS09>
       <NLS10>
	     <Label>Meses</Label>
       </NLS10>
     </LanguageResources>
   </FilterText>
   <Filters>
      <ReportToolDesignFilter>
         <TypeControl>uc_date_picker</TypeControl>
         <TextControl>
            <LanguageResources>
               <NLS09>
                  <Label>From</Label>
               </NLS09>
               <NLS10>
                  <Label>Desde</Label>
               </NLS10>
            </LanguageResources>
         </TextControl>
         <ParameterStoredProcedure>
            <Name>@pMonthFrom</Name>
            <Type>DateTime</Type>
         </ParameterStoredProcedure>
         <Methods>
            <Method>
               <Name>SetFormat</Name>
               <Parameters>14,0</Parameters>
            </Method>
         </Methods>
         <Propertys>
            <Property>
               <Name>ShowUpDown</Name>
               <Value>True</Value>
            </Property>
         </Propertys>
         <Value>TodayOpening()-30</Value>
      </ReportToolDesignFilter>
      <ReportToolDesignFilter>
         <TypeControl>uc_date_picker</TypeControl>
         <TextControl>
            <LanguageResources>
               <NLS09>
                  <Label>To</Label>
               </NLS09>
               <NLS10>
                  <Label>Hasta</Label>
               </NLS10>
            </LanguageResources>
         </TextControl>
         <ParameterStoredProcedure>
            <Name>@pMonthTo</Name>
            <Type>DateTime</Type>
         </ParameterStoredProcedure>
         <Methods>
            <Method>
               <Name>SetFormat</Name>
               <Parameters>14,0</Parameters>
            </Method>
         </Methods>
         <Propertys>
            <Property>
               <Name>ShowUpDown</Name>
               <Value>True</Value>
            </Property>
         </Propertys>         
         <Value>TodayOpening()</Value>
      </ReportToolDesignFilter>
   </Filters>
</ReportToolDesignFilterDTO>'
  
  
  IF NOT EXISTS(SELECT 1 
				        FROM report_tool_config 
				        WHERE rtc_store_name = 'MontlyReportPhilippines')
  BEGIN
    SELECT @rtc_form_id = ISNULL(MAX(rtc_form_id), 10999) + 1
    FROM report_tool_config
    
    INSERT INTO [dbo].[REPORT_TOOL_CONFIG] 
			(RTC_FORM_ID,
			RTC_LOCATION_MENU,
			RTC_REPORT_NAME,
			RTC_STORE_NAME,
			RTC_DESIGN_FILTER,
			RTC_DESIGN_SHEETS,
			RTC_MAILING,
			RTC_STATUS,
			RTC_MODE_TYPE,
      RTC_HTML_HEADER,
      RTC_HTML_FOOTER)
	   VALUES
      (@rtc_form_id,
      8,
      '<LanguageResources><NLS09><Label>Monthly Report</Label></NLS09><NLS10><Label>Reporte mensual</Label></NLS10></LanguageResources>',
      'MontlyReportPhilippines',
      @rtc_design_filter,
      @rtc_design_sheet,
      0,
      0,
      1,
      NULL,
      NULL)
                                      
  END
  ELSE
  BEGIN
      UPDATE [dbo].[REPORT_TOOL_CONFIG] 
      SET RTC_DESIGN_SHEETS = /*RTC_DESIGN_SHEETS*/   @rtc_design_sheet
        , RTC_DESIGN_FILTER = /*RTC_DESIGN_FILTER*/   @rtc_design_filter
      WHERE RTC_STORE_NAME = 'MontlyReportPhilippines'
  END
END
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ScoreReport]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[ScoreReport]
GO

CREATE  PROCEDURE [dbo].[ScoreReport]
(
    @pDateFrom        AS DATETIME
  , @pCageFillerIn    AS INT
  , @pCageFillerOut   AS INT
  , @pChipRE          AS INT
  , @pSaleChips       AS INT
 )
AS
BEGIN
  DECLARE @old_time AS DATETIME
  DECLARE @DateTo   AS DATETIME
  DECLARE @Horas    AS NVARCHAR(1000)
  
  DECLARE @cols             AS NVARCHAR(MAX)
  DECLARE @query            AS NVARCHAR(MAX)
  DECLARE @NationalCurrency AS NVARCHAR(3)

  SET @old_time = '2007-01-01T07:00:00'
  SET @DateTo = DATEADD(DAY, 1, @pDateFrom)  
  SET @Horas = N'[0],[1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12],[13],[14],[15],[16],[17],[18],[19],[20],[21],[22],[23],[9999]'
  SET @NationalCurrency = (SELECT GP_KEY_VALUE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'RegionalOptions' AND GP_SUBJECT_KEY = 'CurrencyIsoCode')
  
  SELECT * 
    INTO #GROUPS_BY_SESSION 
    FROM SplitStringIntoTable(REPLACE(REPLACE(@Horas, '[', ''), ']', ''), ',', 1)

      SELECT   GTT_NAME                             AS 'TYPE_NAME'
             , GT_NAME                              AS 'TABLE_NAME'
             , CS_OPENING_DATE                      AS 'OPENING_DATE'
             , CS_CLOSING_DATE                      AS 'CLOSING_DATE'
             , GTS_GAMING_TABLE_SESSION_ID          AS 'GAMING_TABLE_SESSION_ID'
             , GTS_CASHIER_SESSION_ID               AS 'CASHIER_SESSION_ID'
             , CS_NAME                              AS 'SESION_NAME'
             , ISNULL(GTSC_INITIAL_CHIPS_AMOUNT, 0) AS 'INITIAL_BALANCE'
             , ISNULL(GTSC_FINAL_CHIPS_AMOUNT, 0)   AS 'FINAL_BALANCE'
             , CAST(GT_NUM_SEATS AS DECIMAL(18,2))  AS 'NUM_SEATS_IN_TABLE'
             , CAST(SST_VALUE AS INT)               AS 'HORA'
             , SST_VALUE                            AS 'HORA_STR'
             , CAST(0 AS DECIMAL(18,2))             AS 'GT_DROP'
             , CAST(0 AS DECIMAL(18,2))             AS 'GT_WIN_LOSS'
             , CAST(0 AS DECIMAL(18,2))             AS 'GT_HOLD'
             , CAST(0 AS DECIMAL(18,2))             AS 'GT_OCCUPATION_NUMBER'
        INTO   #VALUE_TABLE
        FROM   GAMING_TABLES
  INNER JOIN   GAMING_TABLES_TYPES                ON GT_TYPE_ID                    = GTT_GAMING_TABLE_TYPE_ID
  INNER JOIN   GAMING_TABLES_SESSIONS             ON GT_GAMING_TABLE_ID            = GTS_GAMING_TABLE_ID
   LEFT JOIN   GAMING_TABLES_SESSIONS_BY_CURRENCY ON GTSC_GAMING_TABLE_SESSION_ID  = GTS_GAMING_TABLE_SESSION_ID AND GTSC_TYPE = @pChipRE AND GTSC_ISO_CODE = @NATIONALCURRENCY
  INNER JOIN   CASHIER_SESSIONS                   ON GTS_CASHIER_SESSION_ID        = CS_SESSION_ID 
 CROSS APPLY   #GROUPS_BY_SESSION
       WHERE   CS_OPENING_DATE >= @pDateFrom
         AND   CS_OPENING_DATE <  @DateTo         
    ORDER BY   GTT_NAME                 
             , GT_NAME                  
             , CS_OPENING_DATE          
             , CAST(SST_VALUE AS INT)                                            

     --  SET DROP BY HOUR - TICKETS  
      UPDATE   #VALUE_TABLE
         SET   GT_DROP = GT_DROP + ISNULL((SELECT   SUM(TI_AMOUNT)
                                             FROM   GT_COPY_DEALER_VALIDATED 
                                       INNER JOIN   TICKETS  ON GTCD_TICKET_ID = TI_TICKET_ID
                                            WHERE   GAMING_TABLE_SESSION_ID = GTCD_GAMING_TABLE_SESSION_ID 
                                              AND   DATEADD(HOUR, DATEDIFF(HOUR, @old_time, GTCD_VALIDATION_DATETIME), @old_time) = DATEADD(HOUR, HORA,  @pDateFrom)), 0)
 
     --  SET DROP BY HOUR - AMOUNTS
      UPDATE   #VALUE_TABLE
         SET   GT_DROP = GT_DROP + ISNULL((SELECT   SUM(CASE WHEN GTPM_ISO_CODE <> @NationalCurrency THEN dbo.ApplyExchange2(GTPM_VALUE, GTPM_ISO_CODE, @NationalCurrency) ELSE  GTPM_VALUE END)
                                             FROM   GT_PLAYERTRACKING_MOVEMENTS 
                                            WHERE   GAMING_TABLE_SESSION_ID = GTPM_GAMING_TABLE_SESSION_ID 
                                              AND   DATEADD(HOUR, DATEDIFF(HOUR, @old_time, GTPM_DATETIME), @old_time) = DATEADD(HOUR, HORA,  @pDateFrom)
                                              AND   GTPM_TYPE = @pSaleChips), 0)                                                                                           
                                              
      --  SET WIN/LOSS WITH CURSORS      
      DECLARE @GT_SESSION_ID BIGINT
      DECLARE GT_SESSION_WIN_LOSS_CURSOR CURSOR GLOBAL
      FOR SELECT GAMING_TABLE_SESSION_ID
            FROM #VALUE_TABLE 
        GROUP BY GAMING_TABLE_SESSION_ID
                                                            
      OPEN GT_SESSION_WIN_LOSS_CURSOR
      FETCH NEXT FROM GT_SESSION_WIN_LOSS_CURSOR INTO @GT_SESSION_ID
      
      -- FIRST CURSOR: ITERANCE THROUGH ALL SESSIONS OF #VALUE_TABLE
      WHILE(@@FETCH_STATUS = 0)
        BEGIN        
        
          DECLARE @WIN_LOSS_HOUR INT
          DECLARE @WIN_LOSS_LAST_AMOUNT MONEY
          DECLARE @WIN_LOSS_CURRENT_AMOUNT MONEY
          DECLARE @WIN_LOSS_FINAL_AMOUNT MONEY
          DECLARE @WIN_LOSS_CLOSING_HOUR AS INT
          DECLARE @WIN_LOSS_CLOSING_TABLE_AMOUNT MONEY
          DECLARE @CREDITS_SUB_FILLS MONEY     
          DECLARE @DROP_CURRENT_AMOUNT MONEY		
            
          DECLARE HOUR_WIN_LOSS_CURSOR CURSOR GLOBAL
          FOR SELECT HORA
                FROM #VALUE_TABLE
               WHERE GAMING_TABLE_SESSION_ID = @GT_SESSION_ID              
                                                            
          OPEN HOUR_WIN_LOSS_CURSOR
          FETCH NEXT FROM HOUR_WIN_LOSS_CURSOR INTO @WIN_LOSS_HOUR     
                                      
          -- WE HAVE THE HOUR THE TABLE WAS CLOSED                                                            
          SET @WIN_LOSS_CLOSING_HOUR = (SELECT DATEPART(HOUR, CLOSING_DATE)
                                          FROM #VALUE_TABLE
                                         WHERE GAMING_TABLE_SESSION_ID = @GT_SESSION_ID
                                      GROUP BY CLOSING_DATE)  
                                      
          SET @WIN_LOSS_FINAL_AMOUNT = 0
          SET @WIN_LOSS_CLOSING_TABLE_AMOUNT = (SELECT FINAL_BALANCE
                                                  FROM #VALUE_TABLE
                                                 WHERE GAMING_TABLE_SESSION_ID = @GT_SESSION_ID
                                              GROUP BY FINAL_BALANCE)                 
                                                          
          -- SECOND CURSOR: ITERANCE THE 24 HOURS OF THE PREVIOUS CURSOR SESSION
          WHILE(@@FETCH_STATUS = 0)
            BEGIN
              
              IF @WIN_LOSS_HOUR <> 9999
              BEGIN
                -- GET CURRENT WIN LOSS AMOUNT BY THE CURRENT TIME ITERANCE              
                SET @WIN_LOSS_CURRENT_AMOUNT = (SELECT ISNULL(GTWL_WIN_LOSS_AMOUNT, 0)
                                                  FROM GAMING_TABLES_WIN_LOSS
                                                 WHERE GTWL_GAMING_TABLE_SESSION_ID = @GT_SESSION_ID AND GTWL_DATETIME_HOUR = DATEADD(HOUR, @WIN_LOSS_HOUR, @pDateFrom))
                
                SET @CREDITS_SUB_FILLS = (SELECT SUM(CASE WHEN CM_CURRENCY_ISO_CODE <> @NationalCurrency THEN dbo.ApplyExchange2(CM_SUB_AMOUNT, CM_CURRENCY_ISO_CODE, @NationalCurrency) ELSE  CM_SUB_AMOUNT END -
                                                     CASE WHEN CM_CURRENCY_ISO_CODE <> @NationalCurrency THEN dbo.ApplyExchange2(CM_ADD_AMOUNT, CM_CURRENCY_ISO_CODE, @NationalCurrency) ELSE  CM_ADD_AMOUNT END)
                                            FROM CASHIER_MOVEMENTS
                                           WHERE CM_DATE BETWEEN DATEADD(HOUR, @WIN_LOSS_HOUR, @pDateFrom) AND DATEADD(HOUR, @WIN_LOSS_HOUR + 1, @pDateFrom)
                                             AND CM_TYPE IN (@pCageFillerIn, @pCageFillerOut)
                                             AND CM_CAGE_CURRENCY_TYPE = @pChipRE
                                             AND CM_SESSION_ID = (SELECT CASHIER_SESSION_ID
                                                                    FROM #VALUE_TABLE
                                                                   WHERE GAMING_TABLE_SESSION_ID = @GT_SESSION_ID
                                                                GROUP BY CASHIER_SESSION_ID))                                           
                
                UPDATE #VALUE_TABLE
                   SET GT_WIN_LOSS = ISNULL(@WIN_LOSS_CURRENT_AMOUNT, 0) - ISNULL(@WIN_LOSS_LAST_AMOUNT, 0) + ISNULL(@CREDITS_SUB_FILLS, 0) + GT_DROP, @DROP_CURRENT_AMOUNT = GT_DROP
                 WHERE GAMING_TABLE_SESSION_ID = @GT_SESSION_ID AND HORA = @WIN_LOSS_HOUR                              
                
                -- IF THE ITERATED TIME IS EQUAL TO THE CLOSURE OF THE GAMBLING TABLE WE SET THE VALUE OF WIN LOSS
                IF DATEPART(HOUR, DATEADD(HOUR, @WIN_LOSS_HOUR,  @pDateFrom)) = @WIN_LOSS_CLOSING_HOUR
                BEGIN
                  SET @WIN_LOSS_FINAL_AMOUNT = ISNULL(@WIN_LOSS_CURRENT_AMOUNT, 0) - ISNULL(@WIN_LOSS_LAST_AMOUNT, 0) + ISNULL(@CREDITS_SUB_FILLS, 0) + ISNULL(@WIN_LOSS_CLOSING_TABLE_AMOUNT, 0)
                END
                
                -- UPDATE WIN LOSS LAST AMOUNT WITH CURRENT WIN LOSS AMOUNT
                IF DATEADD(HOUR, @WIN_LOSS_HOUR,  @pDateFrom) <= GETDATE()
                BEGIN
                  SET @WIN_LOSS_LAST_AMOUNT = ISNULL(@WIN_LOSS_CURRENT_AMOUNT, 0)
                END
                ELSE
                BEGIN
                  SET @WIN_LOSS_LAST_AMOUNT = 0
                END
              END
              ELSE
                UPDATE #VALUE_TABLE
                   SET GT_WIN_LOSS = @WIN_LOSS_FINAL_AMOUNT
                 WHERE GAMING_TABLE_SESSION_ID = @GT_SESSION_ID AND HORA = 9999
                           
              FETCH NEXT FROM HOUR_WIN_LOSS_CURSOR INTO @WIN_LOSS_HOUR                         
            
            END       
            CLOSE HOUR_WIN_LOSS_CURSOR
            DEALLOCATE HOUR_WIN_LOSS_CURSOR
  
        FETCH NEXT FROM GT_SESSION_WIN_LOSS_CURSOR INTO @GT_SESSION_ID
      END
      CLOSE GT_SESSION_WIN_LOSS_CURSOR
      DEALLOCATE GT_SESSION_WIN_LOSS_CURSOR         

      --  SET HOLD BY HOUR    
      UPDATE   #VALUE_TABLE
         SET   GT_HOLD = A.GT_WIN_LOSS / A.GT_DROP * 100
        FROM   #VALUE_TABLE AS A
       WHERE   GAMING_TABLE_SESSION_ID = A.GAMING_TABLE_SESSION_ID
         AND   HORA = A.HORA
         AND   A.GT_DROP <> 0
 
      --  SET OCCUPATION BY HOUR 
      UPDATE   #VALUE_TABLE
         SET   GT_OCCUPATION_NUMBER = (SELECT   CAST(COUNT(DISTINCT GTPS_SEAT_ID) AS DECIMAL(18,2))
                                  FROM   GT_PLAY_SESSIONS 
                                 WHERE   GAMING_TABLE_SESSION_ID = GTPS_GAMING_TABLE_SESSION_ID 
                                   AND ( GTPS_STARTED <= DATEADD(HOUR, HORA,  @pDateFrom) 
                                   AND   ISNULL(GTPS_FINISHED, GETDATE()) > DATEADD(HOUR, HORA,  @pDateFrom)
                                    OR   GTPS_STARTED >= DATEADD(HOUR, HORA,  @pDateFrom)
                                   AND   GTPS_STARTED < DATEADD(HOUR, 1, DATEADD(HOUR, HORA,  @pDateFrom))))
       WHERE  NUM_SEATS_IN_TABLE <> 0
 
      --  SET OCCUPATION BY HOUR -- TOTAL --
      UPDATE   #VALUE_TABLE
         SET   GT_OCCUPATION_NUMBER = ISNULL((SELECT   SUM(A.GT_OCCUPATION_NUMBER) / CAST(SUM(CASE WHEN A.GT_OCCUPATION_NUMBER > 0 THEN 1 ELSE 0 END)  AS DECIMAL(18,2))
                                  FROM   #VALUE_TABLE AS A
                                 WHERE   #VALUE_TABLE.GAMING_TABLE_SESSION_ID = A.GAMING_TABLE_SESSION_ID 
                                   AND   A.GT_OCCUPATION_NUMBER > 0), 0)
       WHERE  HORA = 9999
       
      -- SET HOUR = 0 WHEN WIN/LOSS IS NOT ENTER 
      DECLARE @Table_Name AS NVARCHAR(50)
      DECLARE @Hour       AS DATETIME
      
      DECLARE BLANK_HOUR_CURSOR CURSOR GLOBAL
        FOR SELECT   GT_NAME
                   , GTWL_DATETIME_HOUR
              FROM   GAMING_TABLES_WIN_LOSS
        INNER JOIN   GAMING_TABLES_SESSIONS ON GTS_GAMING_TABLE_SESSION_ID = GTWL_GAMING_TABLE_SESSION_ID
        INNER JOIN   GAMING_TABLES  ON GT_GAMING_TABLE_ID = GTS_GAMING_TABLE_ID
             WHERE   GTWL_DATETIME_HOUR >= @pDateFrom
               AND   GTWL_DATETIME_HOUR <= DATEADD(DAY, 1, @pDateFrom)
          GROUP BY   GT_NAME, GTWL_DATETIME_HOUR
            HAVING   MAX(GTWL_WIN_LOSS_AMOUNT) IS NULL
                                                            
      OPEN BLANK_HOUR_CURSOR
      FETCH NEXT FROM BLANK_HOUR_CURSOR INTO @Table_Name, @Hour     
      WHILE(@@FETCH_STATUS = 0)
        BEGIN
          UPDATE   #VALUE_TABLE
             SET   GT_DROP = 0
                 , GT_WIN_LOSS = 0
                 , GT_HOLD = 0
                 , GT_OCCUPATION_NUMBER = 0
           WHERE   TABLE_NAME = @Table_Name
             AND   DATEADD(HOUR, HORA,  @pDateFrom) = @Hour
        
          FETCH NEXT FROM BLANK_HOUR_CURSOR INTO @Table_Name, @Hour                          
        END       
      CLOSE BLANK_HOUR_CURSOR
      DEALLOCATE BLANK_HOUR_CURSOR           
            
            
      --PIVOT COLUMS FOR REPORT
      SELECT   @cols = STUFF((SELECT ',' + QUOTENAME( CASE WHEN HORA < 25 
                                                           THEN REPLACE(LEFT(RIGHT(CONVERT(NVARCHAR(20), DATEADD(HOUR, HORA, @pDateFrom), 120), 8), 5), ':', '') 
                                                           ELSE '9999'
                                                           END   +'-'+COL) 
        FROM   #VALUE_TABLE t
 CROSS APPLY ( SELECT 'GT_DROP', 1 UNION ALL
               SELECT 'GT_WIN_LOSS', 2 UNION ALL
               SELECT 'GT_HOLD', 3 UNION ALL
               SELECT 'GT_OCCUPATION_NUMBER', 4
             ) C (COL, SO)
    GROUP BY   COL
             , SO
             , HORA
    ORDER BY   HORA
             , SO FOR XML PATH('')
             , TYPE ).value('.', 'NVARCHAR(MAX)'),1,1,'')


set @query = 'SELECT   TYPE_NAME
                     , TABLE_NAME
                     , OPENING_DATE
                     , CLOSING_DATE
                     , GAMING_TABLE_SESSION_ID
                     , SESION_NAME
                     , INITIAL_BALANCE
                     , FINAL_BALANCE
                     , NUM_SEATS_IN_TABLE
                     , ' + @cols + ' 
                FROM ( SELECT   TYPE_NAME
                              , TABLE_NAME
                              , OPENING_DATE
                              , CLOSING_DATE
                              , GAMING_TABLE_SESSION_ID
                              , SESION_NAME
                              , INITIAL_BALANCE
                              , FINAL_BALANCE
                              , NUM_SEATS_IN_TABLE
                              , COL = CASE WHEN HORA < 25 
                                           THEN REPLACE( LEFT(  RIGHT(CONVERT(NVARCHAR(20), DATEADD(HOUR, HORA, ''' + CONVERT(NVARCHAR(20), @pDateFrom, 120) + '''), 120), 8), 5), '':'', '''' )
                                           ELSE ''9999'' 
                                           END  +''-''+COL
                              , VALUE
                         FROM   #VALUE_TABLE T
                  CROSS APPLY ( SELECT   ''GT_DROP''
                                       , GT_DROP UNION ALL
                                SELECT   ''GT_WIN_LOSS''
                                       , GT_WIN_LOSS UNION ALL
                                SELECT   ''GT_HOLD''
                                       , GT_HOLD UNION ALL
                                SELECT   ''GT_OCCUPATION_NUMBER''
                                       , GT_OCCUPATION_NUMBER
                              ) C (COL, VALUE)
                     ) X
               PIVOT ( MAX(VALUE) FOR COL IN (' + @cols + ') ) PVT '


  EXECUTE SP_EXECUTESQL @query

  DROP TABLE #GROUPS_BY_SESSION
  
END --ScoreReport
GO

GRANT EXECUTE ON [dbo].[ScoreReport] TO [wggui] WITH GRANT OPTION
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetSectionCanHaveContent]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[GetSectionCanHaveContent]
GO

CREATE PROCEDURE [dbo].[GetSectionCanHaveContent] 
	@pSectionName NVARCHAR(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT SS_SECTION_SCHEMA_ID SectionSchemaId, 
			 SS_Name Name, 
			 LO_VALUE MainTitle,
			 SS_PARENT_ID ParentId
	FROM MAPP_SECTION_SCHEMA SS
	INNER JOIN MAPP_LOCALIZATION LO ON SS.SS_SECTION_SCHEMA_ID = LO.LO_ENTITY_ITEM_ID
	WHERE LO.LO_ENTITY_NAME = 'SectionSchema' AND 
			LO.LO_RESOURCE_ID = 'MainTitle' AND
			[dbo].SectionHasPromotionalTemplatesChilds(SS_SECTION_SCHEMA_ID) = 1 AND
			SS.SS_TYPE = 1 AND
			SS.SS_PARENT_ID IN (SELECT SS_SECTION_SCHEMA_ID
													FROM MAPP_SECTION_SCHEMA
													WHERE SS_NAME = @pSectionName)
END

GO

GRANT EXECUTE ON [dbo].[GetSectionCanHaveContent] TO [wggui] WITH GRANT OPTION
GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetMeterHistory]') AND type in (N'P', N'PC'))
DROP PROCEDURE GetMeterHistory
GO
 
CREATE PROCEDURE GetMeterHistory
@pFrom datetime
AS
BEGIN
 
DECLARE @pTo AS DATETIME
SET @pTo = DATEADD(DAY, 1, @pFrom)
 
 
SELECT  
     (CASE WHEN TSMH_GROUP_ID IS NULL THEN TSMH_DATETIME ELSE TSMH_CREATED_DATETIME END)                    AS TE_DATETIME                                  
    , PV_NAME                                                                                               AS TE_PROVIDER         
    , CONVERT(NVARCHAR, TE_NAME)                                                                            AS TE_NAME                                    
    , TE_FLOOR_ID                                                                                           AS TE_FLOOR_ID         
    , AR_NAME                                                                                               AS TE_AREA             
    , BK_NAME                                                                                               AS TE_BANK             
    , TE_POSITION                                                                                           AS TE_POSITION         
    , TSMH_SAS_ACCOUNTING_DENOM                                                                             AS TE_SAS_ACCOUNTING_DENOM 
    , TE_ISO_CODE                                                                                           AS TE_ISO_CODE

    , ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 128)  THEN TSMH_METER_INI_VALUE   ELSE NULL END), 0) * 0.01   AS A10001128a      
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 128)  THEN TSMH_METER_FIN_VALUE   ELSE NULL END), 0) * 0.01   AS A10001128b      
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 128)  THEN TSMH_METER_INCREMENT   ELSE NULL END), 0) * 0.01   AS A10001128c      
		   
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 129)  THEN TSMH_METER_INI_VALUE   ELSE NULL END), 0)          AS A10001129a      
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 129)  THEN TSMH_METER_FIN_VALUE   ELSE NULL END), 0)          AS A10001129b      
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 129)  THEN TSMH_METER_INCREMENT   ELSE NULL END), 0)          AS A10001129c     
		   
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 130)  THEN TSMH_METER_INI_VALUE   ELSE NULL END), 0) * 0.01   AS A10001130a      
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 130)  THEN TSMH_METER_FIN_VALUE   ELSE NULL END), 0) * 0.01   AS A10001130b      
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 130)  THEN TSMH_METER_INCREMENT   ELSE NULL END), 0) * 0.01   AS A10001130c      
		   
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 131)  THEN TSMH_METER_INI_VALUE   ELSE NULL END), 0)          AS A10001131a      
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 131)  THEN TSMH_METER_FIN_VALUE   ELSE NULL END), 0)          AS A10001131b      
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 131)  THEN TSMH_METER_INCREMENT   ELSE NULL END), 0)          AS A10001131c      
		   
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 132)  THEN TSMH_METER_INI_VALUE   ELSE NULL END), 0) * 0.01   AS A10001132a     
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 132)  THEN TSMH_METER_FIN_VALUE   ELSE NULL END), 0) * 0.01   AS A10001132b     
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 132)  THEN TSMH_METER_INCREMENT   ELSE NULL END), 0) * 0.01   AS A10001132c     
		   
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 133)  THEN TSMH_METER_INI_VALUE   ELSE NULL END), 0)          AS A10001133a      
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 133)  THEN TSMH_METER_FIN_VALUE   ELSE NULL END), 0)          AS A10001133b      
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 133)  THEN TSMH_METER_INCREMENT   ELSE NULL END), 0)          AS A10001133c     
		   
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 134)  THEN TSMH_METER_INI_VALUE   ELSE NULL END), 0) * 0.01   AS A10001134a     
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 134)  THEN TSMH_METER_FIN_VALUE   ELSE NULL END), 0) * 0.01   AS A10001134b      
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 134)  THEN TSMH_METER_INCREMENT   ELSE NULL END), 0) * 0.01   AS A10001134c      
		   
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 135)  THEN TSMH_METER_INI_VALUE   ELSE NULL END), 0)          AS A10001135a      
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 135)  THEN TSMH_METER_FIN_VALUE   ELSE NULL END), 0)          AS A10001135b      
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 135)  THEN TSMH_METER_INCREMENT   ELSE NULL END), 0)          AS A10001135c     
		   
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 136)  THEN TSMH_METER_INI_VALUE   ELSE NULL END), 0) * 0.01   AS A10001136a     
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 136)  THEN TSMH_METER_FIN_VALUE   ELSE NULL END), 0) * 0.01   AS A10001136b     
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 136)  THEN TSMH_METER_INCREMENT   ELSE NULL END), 0) * 0.01   AS A10001136c     

	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 137)  THEN TSMH_METER_INI_VALUE   ELSE NULL END), 0)          AS A10001137a      
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 137)  THEN TSMH_METER_FIN_VALUE   ELSE NULL END), 0)          AS A10001137b     
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 137)  THEN TSMH_METER_INCREMENT   ELSE NULL END), 0)          AS A10001137c      
		   
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 138)  THEN TSMH_METER_INI_VALUE   ELSE NULL END), 0) * 0.01   AS A10001138a     
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 138)  THEN TSMH_METER_FIN_VALUE   ELSE NULL END), 0) * 0.01   AS A10001138b      
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 138)  THEN TSMH_METER_INCREMENT   ELSE NULL END), 0) * 0.01   AS A10001138c      
		   
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 139)  THEN TSMH_METER_INI_VALUE   ELSE NULL END), 0)          AS A10001139a      
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 139)  THEN TSMH_METER_FIN_VALUE   ELSE NULL END), 0)          AS A10001139b      
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 139)  THEN TSMH_METER_INCREMENT   ELSE NULL END), 0)          AS A10001139c      

    , ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 8)    THEN TSMH_METER_INI_VALUE   ELSE NULL END), 0) * 0.01   AS A100028a      
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 8)    THEN TSMH_METER_FIN_VALUE   ELSE NULL END), 0) * 0.01   AS A100028b      
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 8)    THEN TSMH_METER_INCREMENT   ELSE NULL END), 0) * 0.01   AS A100028c      
	
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 9)    THEN TSMH_METER_INI_VALUE   ELSE NULL END), 0) * 0.01   AS A100029a      
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 9)    THEN TSMH_METER_FIN_VALUE   ELSE NULL END), 0) * 0.01   AS A100029b      
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 9)    THEN TSMH_METER_INCREMENT   ELSE NULL END), 0) * 0.01   AS A100029c      
	
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 11)   THEN TSMH_METER_INI_VALUE   ELSE NULL END), 0) * 0.01   AS A1000211a      
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 11)   THEN TSMH_METER_FIN_VALUE   ELSE NULL END), 0) * 0.01   AS A1000211b      
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 11)   THEN TSMH_METER_INCREMENT   ELSE NULL END), 0) * 0.01   AS A1000211c      
	
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 36)   THEN TSMH_METER_INI_VALUE   ELSE NULL END), 0) * 0.01   AS A1000236a      
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 36)   THEN TSMH_METER_FIN_VALUE   ELSE NULL END), 0) * 0.01   AS A1000236b      
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 36)   THEN TSMH_METER_INCREMENT   ELSE NULL END), 0) * 0.01   AS A1000236c      
		  
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 75)   THEN TSMH_METER_INI_VALUE   ELSE NULL END), 0)          AS A1000275a      
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 75)   THEN TSMH_METER_FIN_VALUE   ELSE NULL END), 0)          AS A1000275b      
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 75)   THEN TSMH_METER_INCREMENT   ELSE NULL END), 0)          AS A1000275c      
		   
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 76)   THEN TSMH_METER_INI_VALUE   ELSE NULL END), 0)          AS A1000276a      
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 76)   THEN TSMH_METER_FIN_VALUE   ELSE NULL END), 0)          AS A1000276b     
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 76)   THEN TSMH_METER_INCREMENT   ELSE NULL END), 0)          AS A1000276c      
		   
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 78)   THEN TSMH_METER_INI_VALUE   ELSE NULL END), 0)          AS A1000278a      
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 78)   THEN TSMH_METER_FIN_VALUE   ELSE NULL END), 0)          AS A1000278b      
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 78)   THEN TSMH_METER_INCREMENT   ELSE NULL END), 0)          AS A1000278c      
		    
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 79)   THEN TSMH_METER_INI_VALUE   ELSE NULL END), 0)          AS A1000279a      
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 79)   THEN TSMH_METER_FIN_VALUE   ELSE NULL END), 0)          AS A1000279b      
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 79)   THEN TSMH_METER_INCREMENT   ELSE NULL END), 0)          AS A1000279c   
	
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 80)   THEN TSMH_METER_INI_VALUE   ELSE NULL END), 0)          AS A1000280a      
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 80)   THEN TSMH_METER_FIN_VALUE   ELSE NULL END), 0)          AS A1000280b      
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 80)   THEN TSMH_METER_INCREMENT   ELSE NULL END), 0)          AS A1000280c   
	
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 110)  THEN TSMH_METER_INI_VALUE   ELSE NULL END), 0) * 0.01   AS A10002110a      
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 110)  THEN TSMH_METER_FIN_VALUE   ELSE NULL END), 0) * 0.01   AS A10002110b      
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 110)  THEN TSMH_METER_INCREMENT   ELSE NULL END), 0) * 0.01   AS A10002110c 
		   
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 0)    THEN TSMH_METER_INI_VALUE   ELSE NULL END), 0) * 0.01   AS A100040a     
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 0)    THEN TSMH_METER_FIN_VALUE   ELSE NULL END), 0) * 0.01   AS A100040b      
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 0)    THEN TSMH_METER_INCREMENT   ELSE NULL END), 0) * 0.01   AS A100040c      
		   
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 1)    THEN TSMH_METER_INI_VALUE   ELSE NULL END), 0) * 0.01   AS A100041a      
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 1)    THEN TSMH_METER_FIN_VALUE   ELSE NULL END), 0) * 0.01   AS A100041b     
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 1)    THEN TSMH_METER_INCREMENT   ELSE NULL END), 0) * 0.01   AS A100041c      
		   
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 2)    THEN TSMH_METER_INI_VALUE   ELSE NULL END), 0) * 0.01   AS A100042a      
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 2)    THEN TSMH_METER_FIN_VALUE   ELSE NULL END), 0) * 0.01   AS A100042b     
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 2)    THEN TSMH_METER_INCREMENT   ELSE NULL END), 0) * 0.01   AS A100042c      
		   
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 3)    THEN TSMH_METER_INI_VALUE   ELSE NULL END), 0) * 0.01   AS A100043a      
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 3)    THEN TSMH_METER_FIN_VALUE   ELSE NULL END), 0) * 0.01   AS A100043b      
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 3)    THEN TSMH_METER_INCREMENT   ELSE NULL END), 0) * 0.01   AS A100043c      
		   
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 5)    THEN TSMH_METER_INI_VALUE   ELSE NULL END), 0)          AS A100045a     
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 5)    THEN TSMH_METER_FIN_VALUE   ELSE NULL END), 0)          AS A100045b      
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 5)    THEN TSMH_METER_INCREMENT   ELSE NULL END), 0)          AS A100045c      
		   
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 6)    THEN TSMH_METER_INI_VALUE   ELSE NULL END), 0)          AS A100046a     
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 6)    THEN TSMH_METER_FIN_VALUE   ELSE NULL END), 0)          AS A100046b      
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 6)    THEN TSMH_METER_INCREMENT   ELSE NULL END), 0)          AS A100046c     
		   
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 25)   THEN TSMH_METER_INI_VALUE   ELSE NULL END), 0) * 0.01   AS A1000425a     
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 25)   THEN TSMH_METER_FIN_VALUE   ELSE NULL END), 0) * 0.01   AS A1000425b      
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 25)   THEN TSMH_METER_INCREMENT   ELSE NULL END), 0) * 0.01   AS A1000425c     
		   
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 26)   THEN TSMH_METER_INI_VALUE   ELSE NULL END), 0) * 0.01   AS A1000426a      
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 26)   THEN TSMH_METER_FIN_VALUE   ELSE NULL END), 0) * 0.01   AS A1000426b      
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 26)   THEN TSMH_METER_INCREMENT   ELSE NULL END), 0) * 0.01   AS A1000426c      
		   
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 29)   THEN TSMH_METER_INI_VALUE   ELSE NULL END), 0) * 0.01   AS A1000429a      
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 29)   THEN TSMH_METER_FIN_VALUE   ELSE NULL END), 0) * 0.01   AS A1000429b      
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 29)   THEN TSMH_METER_INCREMENT   ELSE NULL END), 0) * 0.01   AS A1000429c      
		   
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 30)   THEN TSMH_METER_INI_VALUE   ELSE NULL END), 0) * 0.01   AS A1000430a     
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 30)   THEN TSMH_METER_FIN_VALUE   ELSE NULL END), 0) * 0.01   AS A1000430b     
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 30)   THEN TSMH_METER_INCREMENT   ELSE NULL END), 0) * 0.01   AS A1000430c     
		  
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 32)   THEN TSMH_METER_INI_VALUE   ELSE NULL END), 0) * 0.01   AS A1000432a      
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 32)   THEN TSMH_METER_FIN_VALUE   ELSE NULL END), 0) * 0.01   AS A1000432b     
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 32)   THEN TSMH_METER_INCREMENT   ELSE NULL END), 0) * 0.01   AS A1000432c     
		  
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 33)   THEN TSMH_METER_INI_VALUE   ELSE NULL END), 0) * 0.01   AS A1000433a      
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 33)   THEN TSMH_METER_FIN_VALUE   ELSE NULL END), 0) * 0.01   AS A1000433b      
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 33)   THEN TSMH_METER_INCREMENT   ELSE NULL END), 0) * 0.01   AS A1000433c      
		   
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 35)   THEN TSMH_METER_INI_VALUE   ELSE NULL END), 0) * 0.01   AS A1000435a      
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 35)   THEN TSMH_METER_FIN_VALUE   ELSE NULL END), 0) * 0.01   AS A1000435b      
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 35)   THEN TSMH_METER_INCREMENT   ELSE NULL END), 0) * 0.01   AS A1000435c      
		   
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 37)   THEN TSMH_METER_INI_VALUE   ELSE NULL END), 0)          AS A1000437a      
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 37)   THEN TSMH_METER_FIN_VALUE   ELSE NULL END), 0)          AS A1000437b      
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 37)   THEN TSMH_METER_INCREMENT   ELSE NULL END), 0)          AS A1000437c      
		   
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 160)  THEN TSMH_METER_INI_VALUE   ELSE NULL END), 0) * 0.01   AS A10004160a      
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 160)  THEN TSMH_METER_FIN_VALUE   ELSE NULL END), 0) * 0.01   AS A10004160b     
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 160)  THEN TSMH_METER_INCREMENT   ELSE NULL END), 0) * 0.01   AS A10004160c      
		   
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 161)  THEN TSMH_METER_INI_VALUE   ELSE NULL END), 0)          AS A10004161a      
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 161)  THEN TSMH_METER_FIN_VALUE   ELSE NULL END), 0)          AS A10004161b      
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 161)  THEN TSMH_METER_INCREMENT   ELSE NULL END), 0)          AS A10004161c      
		   
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 162)  THEN TSMH_METER_INI_VALUE   ELSE NULL END), 0) * 0.01   AS A10004162a      
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 162)  THEN TSMH_METER_FIN_VALUE   ELSE NULL END), 0) * 0.01   AS A10004162b      
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 162)  THEN TSMH_METER_INCREMENT   ELSE NULL END), 0) * 0.01   AS A10004162c      
		   
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 163)  THEN TSMH_METER_INI_VALUE   ELSE NULL END), 0)          AS A10004163a      
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 163)  THEN TSMH_METER_FIN_VALUE   ELSE NULL END), 0)          AS A10004163b      
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 163)  THEN TSMH_METER_INCREMENT   ELSE NULL END), 0)          AS A10004163c      
		   
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 184)  THEN TSMH_METER_INI_VALUE   ELSE NULL END), 0) * 0.01   AS A10004184a      
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 184)  THEN TSMH_METER_FIN_VALUE   ELSE NULL END), 0) * 0.01   AS A10004184b      
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 184)  THEN TSMH_METER_INCREMENT   ELSE NULL END), 0) * 0.01   AS A10004184c      
		   
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 185)  THEN TSMH_METER_INI_VALUE   ELSE NULL END), 0)          AS A10004185a      
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 185)  THEN TSMH_METER_FIN_VALUE   ELSE NULL END), 0)          AS A10004185b      
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 185)  THEN TSMH_METER_INCREMENT   ELSE NULL END), 0)          AS A10004185c      
		   
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 186)  THEN TSMH_METER_INI_VALUE   ELSE NULL END), 0) * 0.01   AS A10004186a      
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 186)  THEN TSMH_METER_FIN_VALUE   ELSE NULL END), 0) * 0.01   AS A10004186b      
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 186)  THEN TSMH_METER_INCREMENT   ELSE NULL END), 0) * 0.01   AS A10004186c      
		   
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 187)  THEN TSMH_METER_INI_VALUE   ELSE NULL END), 0)          AS A10004187a      
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 187)  THEN TSMH_METER_FIN_VALUE   ELSE NULL END), 0)          AS A10004187b      
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 187)  THEN TSMH_METER_INCREMENT   ELSE NULL END), 0)          AS A10004187c      
		   
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 4096) THEN TSMH_METER_INI_VALUE   ELSE NULL END), 0) * 0.01   AS A100044096a      
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 4096) THEN TSMH_METER_FIN_VALUE   ELSE NULL END), 0) * 0.01   AS A100044096b      
	, ISNULL(SUM(CASE WHEN (TSMH_METER_CODE = 4096) THEN TSMH_METER_INCREMENT   ELSE NULL END), 0) * 0.01   AS A100044096c   

  FROM TERMINAL_SAS_METERS_HISTORY WITH(INDEX(IX_tsmh_datetime_type))               
  LEFT JOIN  TERMINALS  ON TE_TERMINAL_ID = TSMH_TERMINAL_ID
  LEFT JOIN PROVIDERS ON TE_PROV_ID = PV_ID
  LEFT JOIN BANKS     ON TE_BANK_ID = BK_BANK_ID
  LEFT JOIN AREAS     ON BK_AREA_ID = AR_AREA_ID

WHERE TSMH_DATETIME >= @pFrom AND TSMH_DATETIME < @pTo AND TSMH_TYPE IN (20)

      AND TSMH_METER_CODE IN(128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 8, 9, 11, 36, 
	                          75, 76, 78, 79, 80, 110, 0, 1, 2, 3, 5, 6, 
							  25, 26, 29, 30, 32, 33, 35, 37, 160, 161, 162, 163, 184, 185, 186, 187, 4096) 

      AND TE_TERMINAL_TYPE IN ( 5, 108)

GROUP BY  (CASE WHEN TSMH_GROUP_ID IS NULL 
                   THEN TSMH_DATETIME         
                   ELSE TSMH_CREATED_DATETIME 
             END)                             
          ,TE_NAME, PV_NAME, TSMH_TYPE, TSMH_SAS_ACCOUNTING_DENOM, TE_ISO_CODE, TSMH_GROUP_ID
		  ,TE_FLOOR_ID, TE_POSITION, AR_NAME, BK_NAME
ORDER BY TE_NAME, TE_DATETIME ASC
 
END
GO
 
GRANT EXECUTE ON [dbo].[GetMeterHistory] TO wggui WITH GRANT OPTION
GO


DECLARE @rtc_design_sheet  XML
DECLARE @rtc_design_filter XML
DECLARE @rtc_form_id INT

SET @rtc_design_sheet ='
<ArrayOfReportToolDesignSheetsDTO>
	<ReportToolDesignSheetsDTO>
		<LanguageResources>
			<NLS09>
				<Label>Hist�rico de contadores</Label>
			</NLS09>
			<NLS10>
				<Label>Hist�rico de contadores</Label>
			</NLS10>
		</LanguageResources>
		<Columns>
			<ReportToolDesignColumn>
				<Code>TE_DATETIME</Code>
				<Width>300</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>�ltima actualizaci�n</Label>
					</NLS09>
					<NLS10>
						<Label>�ltima actualizaci�n</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>TE_PROVIDER</Code>
				<Width>300</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Proveedor</Label>
					</NLS09>
					<NLS10>
						<Label>Proveedor</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>TE_NAME</Code>
				<Width>300</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Terminal</Label>
					</NLS09>
					<NLS10>
						<Label>Terminal</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>TE_FLOOR_ID</Code>
				<Width>300</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Id de planta</Label>
					</NLS09>
					<NLS10>
						<Label>Id de planta</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>TE_AREA</Code>
				<Width>300</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>�rea</Label>
					</NLS09>
					<NLS10>
						<Label>�rea</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>TE_BANK</Code>
				<Width>300</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Isla</Label>
					</NLS09>
					<NLS10>
						<Label>Isla</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>TE_POSITION</Code>
				<Width>300</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Posici�n</Label>
					</NLS09>
					<NLS10>
						<Label>Posici�n</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>TE_SAS_ACCOUNTING_DENOM</Code>
				<Width>300</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Denominaci�n contable</Label>
					</NLS09>
					<NLS10>
						<Label>Denominaci�n contable</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>TE_ISO_CODE</Code>
				<Width>300</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Iso Code</Label>
					</NLS09>
					<NLS10>
						<Label>Iso Code</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A10001128a</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Entrada ticket RE Inicial</Label>
					</NLS09>
					<NLS10>
						<Label>Entrada ticket RE Inicial</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A10001128b</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Entrada ticket RE Final</Label>
					</NLS09>
					<NLS10>
						<Label>Entrada ticket RE Final</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A10001128c</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Entrada ticket RE Incremento</Label>
					</NLS09>
					<NLS10>
						<Label>Entrada ticket RE Incremento</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A10001129a</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Entrada ticket redimible (cantidad) Inicial</Label>
					</NLS09>
					<NLS10>
						<Label>Entrada ticket redimible (cantidad) Inicial</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A10001129b</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Entrada ticket redimible (cantidad) Final</Label>
					</NLS09>
					<NLS10>
						<Label>Entrada ticket redimible (cantidad) Final</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A10001129c</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Entrada ticket redimible (cantidad) Incremento</Label>
					</NLS09>
					<NLS10>
						<Label>Entrada ticket redimible (cantidad) Incremento</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A10001130a</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Entrada ticket promocional NR Inicial</Label>
					</NLS09>
					<NLS10>
						<Label>Entrada ticket promocional NR Inicial</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A10001130b</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Entrada ticket promocional NR Final</Label>
					</NLS09>
					<NLS10>
						<Label>Entrada ticket promocional NR Final</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A10001130c</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Entrada ticket promocional NR Incremento</Label>
					</NLS09>
					<NLS10>
						<Label>Entrada ticket promocional NR Incremento</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A10001131a</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Entrada ticket promocional NR (cantidad) Inicial</Label>
					</NLS09>
					<NLS10>
						<Label>Entrada ticket promocional NR (cantidad) Inicial</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A10001131b</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Entrada ticket promocional NR (cantidad) Final</Label>
					</NLS09>
					<NLS10>
						<Label>Entrada ticket promocional NR (cantidad) Final</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A10001131c</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Entrada ticket promocional NR (cantidad) Incremento</Label>
					</NLS09>
					<NLS10>
						<Label>Entrada ticket promocional NR (cantidad) Incremento</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A10001132a</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Entrada ticket promocional RE Inicial</Label>
					</NLS09>
					<NLS10>
						<Label>Entrada ticket promocional RE Inicial</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A10001132b</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Entrada ticket promocional RE Final</Label>
					</NLS09>
					<NLS10>
						<Label>Entrada ticket promocional RE Final</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A10001132c</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Entrada ticket promocional RE Incremento</Label>
					</NLS09>
					<NLS10>
						<Label>Entrada ticket promocional RE Incremento</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A10001133a</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Entrada ticket promocional RE (cantidad) Inicial</Label>
					</NLS09>
					<NLS10>
						<Label>Entrada ticket promocional RE (cantidad) Inicial</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A10001133b</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Entrada ticket promocional RE (cantidad) Final</Label>
					</NLS09>
					<NLS10>
						<Label>Entrada ticket promocional RE (cantidad) Final</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A10001133c</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Entrada ticket promocional RE (cantidad) Incremento</Label>
					</NLS09>
					<NLS10>
						<Label>Entrada ticket promocional RE (cantidad) Incremento</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A10001134a</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Salida ticket RE Inicial</Label>
					</NLS09>
					<NLS10>
						<Label>Salida ticket RE Inicial</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A10001134b</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Salida ticket RE Final</Label>
					</NLS09>
					<NLS10>
						<Label>Salida ticket RE Final</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A10001134c</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Salida ticket RE Incremento</Label>
					</NLS09>
					<NLS10>
						<Label>Salida ticket RE Incremento</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A10001135a</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Salida ticket RE (cantidad) Inicial</Label>
					</NLS09>
					<NLS10>
						<Label>Salida ticket RE (cantidad) Inicial</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A10001135b</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Salida ticket RE (cantidad) Final</Label>
					</NLS09>
					<NLS10>
						<Label>Salida ticket RE (cantidad) Final</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A10001135c</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Salida ticket RE (cantidad) Incremento</Label>
					</NLS09>
					<NLS10>
						<Label>Salida ticket RE (cantidad) Incremento</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A10001136a</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Salida ticket promocional NR Inicial</Label>
					</NLS09>
					<NLS10>
						<Label>Salida ticket promocional NR Inicial</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A10001136b</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Salida ticket promocional NR Final</Label>
					</NLS09>
					<NLS10>
						<Label>Salida ticket promocional NR Final</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A10001136c</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Salida ticket promocional NR Incremento</Label>
					</NLS09>
					<NLS10>
						<Label>Salida ticket promocional NR Incremento</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A10001137a</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Salida ticket promocional NR (cantidad) Inicial</Label>
					</NLS09>
					<NLS10>
						<Label>Salida ticket promocional NR (cantidad) Inicial</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A10001137b</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Salida ticket promocional NR (cantidad) Final</Label>
					</NLS09>
					<NLS10>
						<Label>Salida ticket promocional NR (cantidad) Final</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A10001137c</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Salida ticket promocional NR (cantidad) Incremento</Label>
					</NLS09>
					<NLS10>
						<Label>Salida ticket promocional NR (cantidad) Incremento</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A10001138a</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Salida ticket de d�bito Inicial</Label>
					</NLS09>
					<NLS10>
						<Label>Salida ticket de d�bito Inicial</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A10001138b</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Salida ticket de d�bito Final</Label>
					</NLS09>
					<NLS10>
						<Label>Salida ticket de d�bito Final</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A10001138c</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Salida ticket de d�bito Incremento</Label>
					</NLS09>
					<NLS10>
						<Label>Salida ticket de d�bito Incremento</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A10001139a</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Salida ticket de d�bito (cantidad) Inicial</Label>
					</NLS09>
					<NLS10>
						<Label>Salida ticket de d�bito (cantidad) Inicial</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A10001139b</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Salida ticket de d�bito (cantidad) Final</Label>
					</NLS09>
					<NLS10>
						<Label>Salida ticket de d�bito (cantidad) Final</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A10001139c</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Salida ticket de d�bito (cantidad) Incremento</Label>
					</NLS09>
					<NLS10>
						<Label>Salida ticket de d�bito (cantidad) Incremento</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A100028a</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Total cr�ditos de aceptador de monedas Inicial</Label>
					</NLS09>
					<NLS10>
						<Label>Total cr�ditos de aceptador de monedas Inicial</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A100028b</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Total cr�ditos de aceptador de monedas Final</Label>
					</NLS09>
					<NLS10>
						<Label>Total cr�ditos de aceptador de monedas Final</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A100028c</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Total cr�ditos de aceptador de monedas Incremento</Label>
					</NLS09>
					<NLS10>
						<Label>Total cr�ditos de aceptador de monedas Incremento</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A100029a</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Total out Inicial</Label>
					</NLS09>
					<NLS10>
						<Label>Total out Inicial</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A100029b</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Total out Final</Label>
					</NLS09>
					<NLS10>
						<Label>Total out Final</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A100029c</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Total out Incremento</Label>
					</NLS09>
					<NLS10>
						<Label>Total out Incremento</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A1000211a</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Total cr�ditos de billetes aceptados Inicial</Label>
					</NLS09>
					<NLS10>
						<Label>Total cr�ditos de billetes aceptados Inicial</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A1000211b</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Total cr�ditos de billetes aceptados Final</Label>
					</NLS09>
					<NLS10>
						<Label>Total cr�ditos de billetes aceptados Final</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A1000211c</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Total cr�ditos de billetes aceptados Incremento</Label>
					</NLS09>
					<NLS10>
						<Label>Total cr�ditos de billetes aceptados Incremento</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A1000236a</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Total in Inicial</Label>
					</NLS09>
					<NLS10>
						<Label>Total in Inicial</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A1000236b</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Total in Final</Label>
					</NLS09>
					<NLS10>
						<Label>Total in Final</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A1000236c</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Total in Incremento</Label>
					</NLS09>
					<NLS10>
						<Label>Total in Incremento</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A1000275a</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>N�m. de billetes de $1,000.00 aceptados Inicial</Label>
					</NLS09>
					<NLS10>
						<Label>N�m. de billetes de $1,000.00 aceptados Inicial</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A1000275b</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>N�m. de billetes de $1,000.00 aceptados Final</Label>
					</NLS09>
					<NLS10>
						<Label>N�m. de billetes de $1,000.00 aceptados Final</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A1000275c</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>N�m. de billetes de $1,000.00 aceptados Incremento</Label>
					</NLS09>
					<NLS10>
						<Label>N�m. de billetes de $1,000.00 aceptados Incremento</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A1000276a</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>N�m. de billetes de $2,000.00 aceptados Inicial</Label>
					</NLS09>
					<NLS10>
						<Label>N�m. de billetes de $2,000.00 aceptados Inicial</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A1000276b</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>N�m. de billetes de $2,000.00 aceptados Final</Label>
					</NLS09>
					<NLS10>
						<Label>N�m. de billetes de $2,000.00 aceptados Final</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A1000276c</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>N�m. de billetes de $2,000.00 aceptados Incremento</Label>
					</NLS09>
					<NLS10>
						<Label>N�m. de billetes de $2,000.00 aceptados Incremento</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A1000278a</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>N�m. de billetes de $5,000.00 aceptados Inicial</Label>
					</NLS09>
					<NLS10>
						<Label>N�m. de billetes de $5,000.00 aceptados Inicial</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A1000278b</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>N�m. de billetes de $5,000.00 aceptados Final</Label>
					</NLS09>
					<NLS10>
						<Label>N�m. de billetes de $5,000.00 aceptados Final</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A1000278c</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>N�m. de billetes de $5,000.00 aceptados Incremento</Label>
					</NLS09>
					<NLS10>
						<Label>N�m. de billetes de $5,000.00 aceptados Incremento</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A1000279a</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>N�m. de billetes de $10,000.00 aceptados Inicial</Label>
					</NLS09>
					<NLS10>
						<Label>N�m. de billetes de $10,000.00 aceptados Inicial</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A1000279b</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>N�m. de billetes de $10,000.00 aceptados Final</Label>
					</NLS09>
					<NLS10>
						<Label>N�m. de billetes de $10,000.00 aceptados Final</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A1000279c</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>N�m. de billetes de $10,000.00 aceptados Incremento</Label>
					</NLS09>
					<NLS10>
						<Label>N�m. de billetes de $10,000.00 aceptados Incremento</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>			
			<ReportToolDesignColumn>
				<Code>A1000280a</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>N�m. de billetes de $20,000.00 aceptados Inicial</Label>
					</NLS09>
					<NLS10>
						<Label>N�m. de billetes de $20,000.00 aceptados Inicial</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A1000280b</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>N�m. de billetes de $20,000.00 aceptados Final</Label>
					</NLS09>
					<NLS10>
						<Label>N�m. de billetes de $20,000.00 aceptados Final</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A1000280c</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>N�m. de billetes de $20,000.00 aceptados Incremento</Label>
					</NLS09>
					<NLS10>
						<Label>N�m. de billetes de $20,000.00 aceptados Incremento</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>			
			<ReportToolDesignColumn>
				<Code>A1000279a</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>N�m. de billetes de $20,000.00 aceptados Inicial</Label>
					</NLS09>
					<NLS10>
						<Label>N�m. de billetes de $20,000.00 aceptados Inicial</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A1000279b</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>N�m. de billetes de $20,000.00 aceptados Final</Label>
					</NLS09>
					<NLS10>
						<Label>N�m. de billetes de $20,000.00 aceptados Final</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A1000279c</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>N�m. de billetes de $20,000.00 aceptados Incremento</Label>
					</NLS09>
					<NLS10>
						<Label>N�m. de billetes de $20,000.00 aceptados Incremento</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A10002110a</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Total de cr�ditos por billetes dispensados por el hopper Inicial</Label>
					</NLS09>
					<NLS10>
						<Label>Total de cr�ditos por billetes dispensados por el hopper Inicial</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A10002110b</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Total de cr�ditos por billetes dispensados por el hopper Final</Label>
					</NLS09>
					<NLS10>
						<Label>Total de cr�ditos por billetes dispensados por el hopper Final</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A10002110c</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Total de cr�ditos por billetes dispensados por el hopper Incremento</Label>
					</NLS09>
					<NLS10>
						<Label>Total de cr�ditos por billetes dispensados por el hopper Incremento</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A100040a</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Monto jugado Inicial</Label>
					</NLS09>
					<NLS10>
						<Label>Monto jugado Inicial</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A100040b</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Monto jugado Final</Label>
					</NLS09>
					<NLS10>
						<Label>Monto jugado Final</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A100040c</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Monto jugado Incremento</Label>
					</NLS09>
					<NLS10>
						<Label>Monto jugado Incremento</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A100041a</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Monto ganado Inicial</Label>
					</NLS09>
					<NLS10>
						<Label>Monto ganado Inicial</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A100041b</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Monto ganado Final</Label>
					</NLS09>
					<NLS10>
						<Label>Monto ganado Final</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A100041c</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Monto ganado Incremento</Label>
					</NLS09>
					<NLS10>
						<Label>Monto ganado Incremento</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A100042a</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Total cr�ditos jackpot Inicial</Label>
					</NLS09>
					<NLS10>
						<Label>Total cr�ditos jackpot Inicial</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A100042b</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Total cr�ditos jackpot Final</Label>
					</NLS09>
					<NLS10>
						<Label>Total cr�ditos jackpot Final</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A100042c</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Total cr�ditos jackpot Incremento</Label>
					</NLS09>
					<NLS10>
						<Label>Total cr�ditos jackpot Incremento</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A100043a</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Total pago manual de cr�ditos cancelados Inicial</Label>
					</NLS09>
					<NLS10>
						<Label>Total pago manual de cr�ditos cancelados Inicial</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A100043b</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Total pago manual de cr�ditos cancelados Final</Label>
					</NLS09>
					<NLS10>
						<Label>Total pago manual de cr�ditos cancelados Final</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A100043c</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Total pago manual de cr�ditos cancelados Incremento</Label>
					</NLS09>
					<NLS10>
						<Label>Total pago manual de cr�ditos cancelados Incremento</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A100045a</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Jugadas Inicial</Label>
					</NLS09>
					<NLS10>
						<Label>Jugadas Inicial</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A100045b</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Jugadas Final</Label>
					</NLS09>
					<NLS10>
						<Label>Jugadas Final</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A100045c</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Jugadas Incremento</Label>
					</NLS09>
					<NLS10>
						<Label>Jugadas Incremento</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A100046a</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Jugadas ganadas Inicial</Label>
					</NLS09>
					<NLS10>
						<Label>Jugadas ganadas Inicial</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A100046b</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Jugadas ganadas Final</Label>
					</NLS09>
					<NLS10>
						<Label>Jugadas ganadas Final</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A100046c</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Jugadas ganadas Incremento</Label>
					</NLS09>
					<NLS10>
						<Label>Jugadas ganadas Incremento</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A1000425a</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Total monto jugado promocional NR Inicial</Label>
					</NLS09>
					<NLS10>
						<Label>Total monto jugado promocional NR Inicial</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A1000425b</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Total monto jugado promocional NR Final</Label>
					</NLS09>
					<NLS10>
						<Label>Total monto jugado promocional NR Final</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A1000425c</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Total monto jugado promocional NR Incremento</Label>
					</NLS09>
					<NLS10>
						<Label>Total monto jugado promocional NR Incremento</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A1000426a</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Total monto jugado promocional RE Inicial</Label>
					</NLS09>
					<NLS10>
						<Label>Total monto jugado promocional RE Inicial</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A1000426b</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Total monto jugado promocional RE Final</Label>
					</NLS09>
					<NLS10>
						<Label>Total monto jugado promocional RE Final</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A1000426c</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Total monto jugado promocional RE Incremento</Label>
					</NLS09>
					<NLS10>
						<Label>Total monto jugado promocional RE Incremento</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A1000429a</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Total pagos de m�quina por jackpot Inicial</Label>
					</NLS09>
					<NLS10>
						<Label>Total pagos de m�quina por jackpot Inicial</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A1000429b</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Total pagos de m�quina por jackpot Final</Label>
					</NLS09>
					<NLS10>
						<Label>Total pagos de m�quina por jackpot Final</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A1000429c</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Total pagos de m�quina por jackpot Incremento</Label>
					</NLS09>
					<NLS10>
						<Label>Total pagos de m�quina por jackpot Incremento</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A1000430a</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Total pagos de m�quina por bonus externos Inicial</Label>
					</NLS09>
					<NLS10>
						<Label>Total pagos de m�quina por bonus externos Inicial</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A1000430b</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Total pagos de m�quina por bonus externos Final</Label>
					</NLS09>
					<NLS10>
						<Label>Total pagos de m�quina por bonus externos Final</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A1000430c</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Total pagos de m�quina por bonus externos Incremento</Label>
					</NLS09>
					<NLS10>
						<Label>Total pagos de m�quina por bonus externos Incremento</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A1000432a</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Total pagos a trav�s de encargado por jackpot Inicial</Label>
					</NLS09>
					<NLS10>
						<Label>Total pagos a trav�s de encargado por jackpot Inicial</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A1000432b</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Total pagos a trav�s de encargado por jackpot Final</Label>
					</NLS09>
					<NLS10>
						<Label>Total pagos a trav�s de encargado por jackpot Final</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A1000432c</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Total pagos a trav�s de encargado por jackpot Incremento</Label>
					</NLS09>
					<NLS10>
						<Label>Total pagos a trav�s de encargado por jackpot Incremento</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A1000433a</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Total pagos a trav�s de encargado por bonus externos Inicial</Label>
					</NLS09>
					<NLS10>
						<Label>Total pagos a trav�s de encargado por bonus externos Inicial</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A1000433b</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Total pagos a trav�s de encargado por bonus externos Final</Label>
					</NLS09>
					<NLS10>
						<Label>Total pagos a trav�s de encargado por bonus externos Final</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A1000433c</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Total pagos a trav�s de encargado por bonus externos Incremento</Label>
					</NLS09>
					<NLS10>
						<Label>Total pagos a trav�s de encargado por bonus externos Incremento</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A1000435a</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Total cr�ditos pagos manuales (suma de total pago manual de cr�ditos cancelados y total de jackpot) Inicial</Label>
					</NLS09>
					<NLS10>
						<Label>Total cr�ditos pagos manuales (suma de total pago manual de cr�ditos cancelados y total de jackpot) Inicial</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A1000435b</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Total cr�ditos pagos manuales (suma de total pago manual de cr�ditos cancelados y total de jackpot) Final</Label>
					</NLS09>
					<NLS10>
						<Label>Total cr�ditos pagos manuales (suma de total pago manual de cr�ditos cancelados y total de jackpot) Final</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A1000435c</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Total cr�ditos pagos manuales (suma de total pago manual de cr�ditos cancelados y total de jackpot) Incremento</Label>
					</NLS09>
					<NLS10>
						<Label>Total cr�ditos pagos manuales (suma de total pago manual de cr�ditos cancelados y total de jackpot) Incremento</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A1000437a</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Jugadas desde el �ltimo reinicio Inicial</Label>
					</NLS09>
					<NLS10>
						<Label>Jugadas desde el �ltimo reinicio Inicial</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A1000437b</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Jugadas desde el �ltimo reinicio Final</Label>
					</NLS09>
					<NLS10>
						<Label>Jugadas desde el �ltimo reinicio Final</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A1000437c</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Jugadas desde el �ltimo reinicio Incremento</Label>
					</NLS09>
					<NLS10>
						<Label>Jugadas desde el �ltimo reinicio Incremento</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A10004160a</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Entrada transferencia RE Inicial</Label>
					</NLS09>
					<NLS10>
						<Label>Entrada transferencia RE Inicial</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A10004160b</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Entrada transferencia RE Final</Label>
					</NLS09>
					<NLS10>
						<Label>Entrada transferencia RE Final</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A10004160c</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Entrada transferencia RE Incremento</Label>
					</NLS09>
					<NLS10>
						<Label>Entrada transferencia RE Incremento</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A10004161a</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Entrada transferencia RE (cantidad) Inicial</Label>
					</NLS09>
					<NLS10>
						<Label>Entrada transferencia RE (cantidad) Inicial</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A10004161b</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Entrada transferencia RE (cantidad) Final</Label>
					</NLS09>
					<NLS10>
						<Label>Entrada transferencia RE (cantidad) Final</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A10004161c</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Entrada transferencia RE (cantidad) Incremento</Label>
					</NLS09>
					<NLS10>
						<Label>Entrada transferencia RE (cantidad) Incremento</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A10004162a</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Entrada transferencia promocional NR Inicial</Label>
					</NLS09>
					<NLS10>
						<Label>Entrada transferencia promocional NR Inicial</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A10004162b</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Entrada transferencia promocional NR Final</Label>
					</NLS09>
					<NLS10>
						<Label>Entrada transferencia promocional NR Final</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A10004162c</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Entrada transferencia promocional NR Incremento</Label>
					</NLS09>
					<NLS10>
						<Label>Entrada transferencia promocional NR Incremento</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A10004163a</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Entrada transferencia promocional NR (cantidad) Inicial</Label>
					</NLS09>
					<NLS10>
						<Label>Entrada transferencia promocional NR (cantidad) Inicial</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A10004163b</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Entrada transferencia promocional NR (cantidad) Final</Label>
					</NLS09>
					<NLS10>
						<Label>Entrada transferencia promocional NR (cantidad) Final</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A10004163c</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Entrada transferencia promocional NR (cantidad) Incremento</Label>
					</NLS09>
					<NLS10>
						<Label>Entrada transferencia promocional NR (cantidad) Incremento</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A10004184a</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Salida transferencia RE Inicial</Label>
					</NLS09>
					<NLS10>
						<Label>Salida transferencia RE Inicial</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A10004184b</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Salida transferencia RE Final</Label>
					</NLS09>
					<NLS10>
						<Label>Salida transferencia RE Final</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A10004184c</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Salida transferencia RE Incremento</Label>
					</NLS09>
					<NLS10>
						<Label>Salida transferencia RE Incremento</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A10004185a</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Salida transferencia RE (cantidad) Inicial</Label>
					</NLS09>
					<NLS10>
						<Label>Salida transferencia RE (cantidad) Inicial</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A10004185b</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Salida transferencia RE (cantidad) Final</Label>
					</NLS09>
					<NLS10>
						<Label>Salida transferencia RE (cantidad) Final</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A10004185c</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Salida transferencia RE (cantidad) Incremento</Label>
					</NLS09>
					<NLS10>
						<Label>Salida transferencia RE (cantidad) Incremento</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A10004186a</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Salida transferencia promocional NR Inicial</Label>
					</NLS09>
					<NLS10>
						<Label>Salida transferencia promocional NR Inicial</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A10004186b</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Salida transferencia promocional NR Final</Label>
					</NLS09>
					<NLS10>
						<Label>Salida transferencia promocional NR Final</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A10004186c</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Salida transferencia promocional NR Incremento</Label>
					</NLS09>
					<NLS10>
						<Label>Salida transferencia promocional NR Incremento</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A10004187a</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Salida transferencia promocional NR (cantidad) Inicial</Label>
					</NLS09>
					<NLS10>
						<Label>Salida transferencia promocional NR (cantidad) Inicial</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A10004187b</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Salida transferencia promocional NR (cantidad) Final</Label>
					</NLS09>
					<NLS10>
						<Label>Salida transferencia promocional NR (cantidad) Final</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A10004187c</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Salida transferencia promocional NR (cantidad) Incremento</Label>
					</NLS09>
					<NLS10>
						<Label>Salida transferencia promocional NR (cantidad) Incremento</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A100044096a</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Total cr�ditos jackpot progresivo Inicial</Label>
					</NLS09>
					<NLS10>
						<Label>Total cr�ditos jackpot progresivo Inicial</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A100044096b</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Total cr�ditos jackpot progresivo Final</Label>
					</NLS09>
					<NLS10>
						<Label>Total cr�ditos jackpot progresivo Final</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
			<ReportToolDesignColumn>
				<Code>A100044096c</Code>
				<Width>400</Width>
				<EquityMatchType>Equality</EquityMatchType>
				<LanguageResources>
					<NLS09>
						<Label>Total cr�ditos jackpot progresivo Incremento</Label>
					</NLS09>
					<NLS10>
						<Label>Total cr�ditos jackpot progresivo Incremento</Label>
					</NLS10>
				</LanguageResources>
			</ReportToolDesignColumn>
		</Columns>
	</ReportToolDesignSheetsDTO>
</ArrayOfReportToolDesignSheetsDTO>
'

SET @rtc_design_filter = '
<ReportToolDesignFilterDTO>
	<FilterType>CustomFilters</FilterType>
	<Filters>
		<ReportToolDesignFilter>
		  <TypeControl>uc_date_picker</TypeControl>
		  <TextControl>
			<LanguageResources>
			  <NLS09>
				<Label>Fecha</Label>
			  </NLS09>
			  <NLS10>
				<Label>Fecha</Label>
			  </NLS10>
			</LanguageResources>
		  </TextControl>
		  <ParameterStoredProcedure>
			<Name>@pFrom</Name>
			<Type>DateTime</Type>
		  </ParameterStoredProcedure>
		  <Methods>
			<Method>
			  <Name>SetFormat</Name>
			  <Parameters>1,4</Parameters>
			</Method>
		  </Methods>		  
		  <Value>TodayOpening()-1</Value>
		</ReportToolDesignFilter>                                                                                       
	</Filters>
</ReportToolDesignFilterDTO>
'

IF NOT EXISTS(SELECT 1 
				FROM report_tool_config 
				WHERE rtc_store_name = 'GetMeterHistory')
  BEGIN
    SELECT @rtc_form_id = ISNULL(MAX(rtc_form_id), 10999) + 1
    FROM report_tool_config
    
    INSERT INTO [dbo].[REPORT_TOOL_CONFIG] 
			(RTC_FORM_ID,
			RTC_LOCATION_MENU,
			RTC_REPORT_NAME,
			RTC_STORE_NAME,
			RTC_DESIGN_FILTER,
			RTC_DESIGN_SHEETS,
			RTC_MAILING,
			RTC_STATUS,
			RTC_MODE_TYPE,
      RTC_HTML_HEADER,
      RTC_HTML_FOOTER)
		VALUES
      (@rtc_form_id,
      2, 
      '<LanguageResources><NLS09><Label>Hist�rico de contadores</Label></NLS09><NLS10><Label>Hist�rico de contadores</Label></NLS10></LanguageResources>',
      'GetMeterHistory',
      @rtc_design_filter,
      @rtc_design_sheet,
      0,
      0,
      1,
      NULL,
      NULL)
                                      
  END
ELSE
	BEGIN
		UPDATE [dbo].[REPORT_TOOL_CONFIG] 
		SET RTC_DESIGN_SHEETS = /*RTC_DESIGN_SHEETS*/   @rtc_design_sheet
		  , RTC_DESIGN_FILTER = /*RTC_DESIGN_FILTER*/   @rtc_design_filter
		WHERE RTC_STORE_NAME = 'GetMeterHistory'
	END
GO

