/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 205;

SET @New_ReleaseId = 206;
SET @New_ScriptName = N'UpdateTo_18.206.037.sql';
SET @New_Description = N'New GP Features.GamingTables;Draws in promobox;Name in sas_meters_catalog';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/******* TABLES  *******/

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[sas_meters_catalog]') and name = 'smc_name')
  ALTER TABLE [dbo].sas_meters_catalog ADD smc_name nvarchar(200) NULL;
GO

-- Draws
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[draws]') and name = 'dr_visible_on_promobox')
   ALTER TABLE [dbo].draws ADD dr_visible_on_promobox bit NOT NULL DEFAULT (0);
GO
   
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[draws]') and name = 'dr_award_on_promobox')
   ALTER TABLE [dbo].draws ADD dr_award_on_promobox bit NOT NULL DEFAULT (0);
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[draws]') and name = 'dr_text_on_promobox')
   ALTER TABLE [dbo].draws ADD dr_text_on_promobox nvarchar(50) NULL;
GO   
   
-- Gifts
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[gifts]') and name = 'gi_award_on_promobox')
   ALTER TABLE [dbo].gifts ADD gi_award_on_promobox bit NOT NULL DEFAULT (0);
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[gifts]') and name = 'gi_text_on_promobox')
   ALTER TABLE [dbo].gifts ADD gi_text_on_promobox nvarchar(50) NULL;
GO

/******* INDEXES *******/

/******* RECORDS *******/

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Features' AND GP_SUBJECT_KEY = 'GamingTables')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Features', 'GamingTables', '0')
GO