/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 309;

SET @New_ReleaseId = 310;
SET @New_ScriptName = N'UpdateTo_18.310.039.sql';
SET @New_Description = N'SP for cage reports';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/******* TABLES  *******/

ALTER TABLE bucket_levels ALTER COLUMN bul_a_factor decimal(18, 2) NOT NULL
GO

ALTER TABLE bucket_levels ALTER COLUMN bul_b_factor decimal(18, 2) NOT NULL
GO

/******* INDEXES *******/

/******* RECORDS *******/
-- Handpays Alarm 
IF NOT EXISTS ( SELECT 1 FROM general_params WHERE gp_group_key = 'Alarms' AND gp_subject_key = 'HandpaysAmountThreshold')
      INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Alarms', 'HandpaysAmountThreshold', 0)
GO

IF NOT EXISTS (SELECT [alcg_alarm_code] from [dbo].[alarm_catalog] WHERE [alcg_alarm_code] = 212995 AND [alcg_language_id] = 10)
BEGIN
  INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) 
    VALUES ( 212995, 10, 0, N'Pago manual', N'Pago manual', 1)
END
GO

IF NOT EXISTS (SELECT [alcg_alarm_code] from [dbo].[alarm_catalog] WHERE [alcg_alarm_code] = 212995 AND [alcg_language_id] = 9)
BEGIN
  INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) 
    VALUES ( 212995,  9, 0, N'Handpay', N'Handpay', 1)
END 
GO



IF NOT EXISTS (SELECT [alcc_alarm_code] from [dbo].[alarm_catalog_per_category] WHERE [alcc_alarm_code] = 212995 AND [alcc_category] = 14) 
BEGIN
  INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) 
    VALUES ( 212995, 14, 0, GETDATE() )
END
GO

-- Bill In Alarm
IF NOT EXISTS (SELECT [alcg_alarm_code] from [dbo].[alarm_catalog] WHERE [alcg_alarm_code] = 212996 AND [alcg_language_id] = 10)
BEGIN
  INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) 
    VALUES ( 212996, 10, 0, N'Billete aceptado', N'Billete aceptado', 1)
END
GO

IF NOT EXISTS (SELECT [alcg_alarm_code] from [dbo].[alarm_catalog] WHERE [alcg_alarm_code] = 212996 AND [alcg_language_id] = 9)
BEGIN
  INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) 
    VALUES ( 212996,  9, 0, N'Bill accepted', N'Bill accepted', 1)
END 
GO

IF NOT EXISTS (SELECT [alcc_alarm_code] from [dbo].[alarm_catalog_per_category] WHERE [alcc_alarm_code] = 212996 AND [alcc_category] = 50) 
BEGIN
  INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) 
    VALUES ( 212996, 9, 0, GETDATE() )
END
GO    

/******* PROCEDURES *******/

--***** CageGetGlobalReportData
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CageGetGlobalReportData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CageGetGlobalReportData]
GO

CREATE PROCEDURE [dbo].[CageGetGlobalReportData]
        @pCageSessionIds NVARCHAR(MAX) = NULL
AS
BEGIN
  
  DECLARE @NationalIsoCode NVARCHAR(3)
  DECLARE @CageOperationType_FromTerminal INT
  DECLARE @TicketsId INT
  DECLARE @BillsId INT
  DECLARE @Currencies AS NVARCHAR(200)
  DECLARE @CountSessions INT
  DECLARE @List VARCHAR(MAX)
  DECLARE @StockIsStored BIT

  SELECT @NationalIsoCode = GP_KEY_VALUE 
    FROM GENERAL_PARAMS 
   WHERE GP_GROUP_KEY = 'RegionalOptions' 
     AND GP_SUBJECT_KEY = 'CurrencyISOCode'

  SELECT @Currencies = GP_KEY_VALUE 
    FROM GENERAL_PARAMS 
   WHERE GP_GROUP_KEY = 'RegionalOptions' 
     AND GP_SUBJECT_KEY = 'CurrenciesAccepted'

  -- Split currencies ISO codes
  SELECT SST_VALUE AS CURRENCY_ISO_CODE INTO #TMP_CURRENCIES_ISO_CODES FROM [SplitStringIntoTable] (@Currencies, ';', 1)
               
  SET @CageOperationType_FromTerminal = 104
  SET @TicketsId = -200
  SET @BillsId = 0
   
  -- Session IDs Split
  SELECT SST_VALUE AS CAGE_SESSION_ID INTO #TMP_CAGE_SESSIONS FROM [SplitStringIntoTable] (@pCageSessionIds,','  , 1)
  SET @CountSessions = (SELECT COUNT(*) FROM #TMP_CAGE_SESSIONS )
  
  IF @CountSessions = 1 
    SELECT @List = CGS_CAGE_STOCK FROM CAGE_SESSIONS 
     WHERE CGS_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)

  -- TABLE[0]: Get cage stock 
  IF (@CountSessions = 1 AND @List IS NOT NULL) 
  BEGIN
    --Get stock on close session
    SET @StockIsStored =  1
    SELECT SST_VALUE AS STOCK_ROW INTO #TMP_STOCK_ROWS FROM [SplitStringIntoTable] (@List, '|', 1)
    
    SELECT REPLACE(PARSENAME(REPLACE(REPLACE(STOCK_ROW, '.', '|'), ';', '.'), 4), '|', '.') AS CGS_ISO_CODE,                        
       CAST(REPLACE(PARSENAME(REPLACE(REPLACE(STOCK_ROW, '.', '|'), ';', '.'), 3), '|', '.') AS MONEY) AS CGS_DENOMINATION,   
       CAST(REPLACE(PARSENAME(REPLACE(REPLACE(STOCK_ROW, '.', '|'), ';', '.'), 2), '|', '.') AS DECIMAL(15,2)) AS CGS_QUANTITY,       
       CAST(REPLACE(PARSENAME(REPLACE(REPLACE(STOCK_ROW, '.', '|'), ';', '.'), 1), '|', '.') AS INT) AS CGS_CAGE_CURRENCY_TYPE  
       INTO #TMP_STOCK                                                                                                               
    FROM #TMP_STOCK_ROWS                                                                                                          

    SELECT * FROM #TMP_STOCK
    ORDER BY CGS_ISO_CODE, CGS_CAGE_CURRENCY_TYPE ASC, CGS_DENOMINATION DESC
    , CASE WHEN CGS_DENOMINATION >= 0 THEN CGS_DENOMINATION 
         ELSE CGS_DENOMINATION * (-100000) END                                                                    

    DROP TABLE #TMP_STOCK_ROWS                                                                                                      
    DROP TABLE #TMP_STOCK     
    
  END --IF (@CountSessions = 1 AND @List IS NOT NULL)    
  ELSE --IF (@CountSessions = 1 AND @List IS NOT NULL) 
  BEGIN
    -- Get current stock
    SET @StockIsStored =  0
    EXEC CageCurrentStock  
  END --ELSE
  
  IF @pCageSessionIds IS NOT NULL
  BEGIN
      
    -- TABLE[1]: Get cage sessions information
    SELECT CSM.CSM_SOURCE_TARGET_ID AS CM_SOURCE_TARGET_ID
         , CSM.CSM_CONCEPT_ID AS CM_CONCEPT_ID
         , CSM.CSM_ISO_CODE AS CM_ISO_CODE
         , SUM(CSM.CSM_VALUE_IN) AS CM_VALUE_IN
         , SUM(CSM.CSM_VALUE_OUT) AS CM_VALUE_OUT
         , CST.CST_SOURCE_TARGET_NAME
         , CC.CC_DESCRIPTION
      FROM CAGE_SESSION_METERS AS CSM
           INNER JOIN CAGE_SOURCE_TARGET_CONCEPTS AS CSTC ON CSTC.CSTC_CONCEPT_ID = CSM.CSM_CONCEPT_ID AND CSTC.CSTC_SOURCE_TARGET_ID = CSM.CSM_SOURCE_TARGET_ID
           INNER JOIN CAGE_CONCEPTS AS CC ON CC.CC_CONCEPT_ID = CSTC.CSTC_CONCEPT_ID
           INNER JOIN CAGE_SOURCE_TARGET AS CST ON CST.CST_SOURCE_TARGET_ID = CSTC.CSTC_SOURCE_TARGET_ID
     WHERE CSM.CSM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
       AND CC.CC_SHOW_IN_REPORT = 1
       AND CC.CC_ENABLED = 1
       AND (CC.CC_TYPE = 1 OR CC_CONCEPT_ID IN (9, 10,11))
       AND CSTC.CSTC_ENABLED = 1
       AND CSM_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)
  GROUP BY CSM.CSM_SOURCE_TARGET_ID, CSM.CSM_CONCEPT_ID, CSM.CSM_ISO_CODE, CST.CST_SOURCE_TARGET_NAME, CC.CC_DESCRIPTION
  ORDER BY CSM.CSM_SOURCE_TARGET_ID
         , CASE WHEN CSM.CSM_CONCEPT_ID = 0 THEN '000' ELSE CC.CC_DESCRIPTION END
         , CASE WHEN CSM.CSM_ISO_CODE = @NationalIsoCode THEN '000' ELSE CSM.CSM_ISO_CODE END                    

    -- TABLE[2]: Get dynamic liabilities
    SELECT CC.CC_CONCEPT_ID AS LIABILITY_CONCEPT_ID
         , CC.CC_DESCRIPTION AS LIABILITY_NAME 
         , CSM.CSM_ISO_CODE AS LIABILITY_ISO_CODE
         , SUM(CSM.CSM_VALUE) AS LIABILITY_VALUE
      FROM CAGE_SESSION_METERS AS CSM
           INNER JOIN CAGE_CONCEPTS AS CC ON CC.CC_CONCEPT_ID = CSM.CSM_CONCEPT_ID
     WHERE CC.CC_ENABLED = 1
       AND CC.CC_SHOW_IN_REPORT = 1
       AND CC.CC_IS_PROVISION = 1
       AND CSM.CSM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
       AND CSM_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)
 GROUP BY CC.CC_CONCEPT_ID, CC.CC_DESCRIPTION, CSM.CSM_ISO_CODE
  ORDER BY CC.CC_CONCEPT_ID ASC
         , CASE WHEN CSM.CSM_ISO_CODE = @NationalIsoCode THEN '000' ELSE CSM.CSM_ISO_CODE END 
      
    -- TABLE[3]: Get session sumary
    SELECT CMD_ISO_CODE AS ISO_CODE
         , ISNULL(ORIGIN, 0) AS ORIGIN
         , SUM(CMD_DEPOSITS) AS DEPOSITS
         , SUM(CMD_WITHDRAWALS) AS WITHDRAWALS
      FROM (SELECT CMD_ISO_CODE
                 , ORIGIN
                 , SUM(CMD_DEPOSITS) CMD_DEPOSITS
                 , SUM(CMD_WITHDRAWALS) CMD_WITHDRAWALS      
              FROM (SELECT CASE WHEN CMD_QUANTITY >= 0 THEN @BillsId
                                ELSE CMD_QUANTITY END  AS ORIGIN
                         , CMD_ISO_CODE
                         , CMD_DENOMINATION
                         , CASE WHEN CGM_TYPE IN (100, 101, 103, 104) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY < 0 AND CMD_QUANTITY <> @TicketsId THEN CMD_DENOMINATION
                                                                                            WHEN CMD_QUANTITY = @TicketsId THEN 0
                                                                                       ELSE CMD_QUANTITY * CMD_DENOMINATION END), 0)
                                ELSE 0 END AS CMD_DEPOSITS
                         , CASE WHEN CGM_TYPE IN (0, 1, 4, 5) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY < 0 AND CMD_QUANTITY <> @TicketsId THEN CMD_DENOMINATION
                                                                                    WHEN CMD_QUANTITY = @TicketsId THEN 0
                                                                                    ELSE CMD_QUANTITY * CMD_DENOMINATION END), 0)
                                ELSE 0 END AS CMD_WITHDRAWALS
                      FROM CAGE_MOVEMENTS 
                           INNER JOIN   CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          
                     WHERE CGM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
                       AND (CGM_STATUS IN (2, 3, 4, 8, 9, 10) 
                            OR (CGM_STATUS = 0 AND CGM_TYPE = 0))
                  GROUP BY CMD_ISO_CODE, CMD_DENOMINATION, CGM_TYPE, CMD_QUANTITY

                  -- Tickets 
                  UNION ALL
                  
                    SELECT @TicketsId AS ORIGIN
                         , ISNULL(TI_CUR0, @NationalIsoCode) CMD_ISO_CODE                                     
                         , 1 CMD_DENOMINATION
                         , CASE WHEN CGM_TYPE IN (100, 101, 103, 104) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY = @TicketsId THEN 
                                                                                                 CASE WHEN TI_AMT0 IS NULL THEN TI_AMOUNT 
                                                                                                      ELSE TI_AMT0 END
                                                                                       ELSE 0 END), 0)
                                ELSE 0 END AS CMD_DEPOSITS
                         , CASE WHEN CGM_TYPE IN (0, 1, 4, 5) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY = @TicketsId THEN 
                                                                                         CASE WHEN TI_AMT0 IS NULL THEN TI_AMOUNT 
                                                                                              ELSE TI_AMT0 END
                                                                                    ELSE 0 END), 0)
                                ELSE 0 END AS CMD_WITHDRAWALS
                      FROM CAGE_MOVEMENTS 
                           INNER JOIN   CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          
                           INNER JOIN   TICKETS ON TICKETS.TI_COLLECTED_MONEY_COLLECTION = CAGE_MOVEMENTS.CGM_MC_COLLECTION_ID     
                     WHERE  CGM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
					        AND (CGM_STATUS IN (2, 3, 4, 8, 9, 10) 
                            OR (CGM_STATUS = 0 AND CGM_TYPE = 0)) AND CMD_QUANTITY = @TicketsId  
							AND CAGE_MOVEMENTS.CGM_TYPE <> @CageOperationType_FromTerminal
                     GROUP by TI_CUR0, CGM_TYPE

                   ) AS T1        
          GROUP BY ORIGIN, CMD_ISO_CODE

            -- GET Terminal value-in amounts
              
              UNION ALL 
              
              SELECT ISNULL(TI_CUR0, @NationalIsoCode) AS CMD_ISO_CODE
                    , @TicketsId AS ORIGIN
                    , ISNULL(SUM (CASE WHEN TI_AMT0 IS NULL THEN TI_AMOUNT 
                                        ELSE TI_AMT0 END),0)  AS DEPOSITS
                    , 0 AS WITHDRAWS
              FROM MONEY_COLLECTIONS
                  INNER JOIN CAGE_MOVEMENTS ON CGM_MC_COLLECTION_ID = MC_COLLECTION_ID 
				  INNER JOIN TICKETS ON TICKETS.TI_COLLECTED_MONEY_COLLECTION = CAGE_MOVEMENTS.CGM_MC_COLLECTION_ID
             WHERE CAGE_MOVEMENTS.CGM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
               AND CAGE_MOVEMENTS.CGM_TYPE = @CAGEOPERATIONTYPE_FROMTERMINAL
               AND (CGM_STATUS IN (2, 3, 4, 8, 9, 10) 
                    OR (CGM_STATUS = 0 AND CGM_TYPE = 0))           
             GROUP BY TI_CUR0, CGM_TYPE

              UNION ALL
              
                SELECT ISNULL(TE_ISO_CODE, @NationalIsoCode) AS CMD_ISO_CODE
                    , @BillsId AS ORIGIN
                    , MC_COLLECTED_BILL_AMOUNT + MC_COLLECTED_COIN_AMOUNT AS DEPOSITS
                    , 0 AS WITHDRAWS
              FROM MONEY_COLLECTIONS
                  INNER JOIN CAGE_MOVEMENTS ON CGM_MC_COLLECTION_ID = MC_COLLECTION_ID 
                  INNER JOIN TERMINALS ON TE_TERMINAL_ID = MC_TERMINAL_ID
             WHERE CAGE_MOVEMENTS.CGM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
               AND CAGE_MOVEMENTS.CGM_TYPE = @CageOperationType_FromTerminal
               AND (CGM_STATUS IN (2, 3, 4, 8, 9, 10) 
                    OR (CGM_STATUS = 0 AND CGM_TYPE = 0))
                                      
          ) AS _TBL_GLB
      WHERE CMD_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)
   GROUP BY _TBL_GLB.CMD_ISO_CODE, _TBL_GLB.ORIGIN
   ORDER BY CASE WHEN _TBL_GLB.CMD_ISO_CODE = @NationalIsoCode THEN '000' ELSE _TBL_GLB.CMD_ISO_CODE END 
          , _TBL_GLB.ORIGIN DESC
         
    -- TABLE[4]: Other System Concepts           
    SELECT CSM_CONCEPT_ID AS CM_CONCEPT_ID
         , CC.CC_DESCRIPTION AS CM_DESCRIPTION
         , CSM_ISO_CODE AS CM_ISO_CODE
         , SUM(CSM_VALUE) AS CM_VALUE
      FROM CAGE_SESSION_METERS AS CSM
           INNER JOIN CAGE_CONCEPTS AS CC ON CSM.CSM_CONCEPT_ID = CC.CC_CONCEPT_ID
     WHERE CSM_SOURCE_TARGET_ID = 0
       AND CSM_CONCEPT_ID IN (7, 8)
       AND CSM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
       AND CSM_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)
  GROUP BY CSM_CONCEPT_ID, CSM_ISO_CODE, CSM_VALUE, CC.CC_DESCRIPTION 
  ORDER BY CM_CONCEPT_ID
         , CASE WHEN CSM_ISO_CODE = @NationalIsoCode THEN '000' ELSE CSM_ISO_CODE END 
         
     -- TABLE[5]: Cage Counts
    SELECT ORIGIN AS ORIGIN
         , CMD_ISO_CODE AS ISO_CODE
         , ISNULL(SUM(CMD_DEPOSITS), 0) AS DEPOSITS
      FROM (SELECT CASE WHEN CMD_QUANTITY >= 0 THEN 0
                        ELSE CMD_QUANTITY END  AS ORIGIN
                 , CMD_ISO_CODE
                 , ISNULL(SUM (CASE WHEN CMD_QUANTITY >= 0 THEN CMD_QUANTITY 
                                    ELSE 1 END * CASE WHEN CGM_TYPE = 99 THEN -1 * CMD_DENOMINATION 
                                                      ELSE CMD_DENOMINATION END), 0) AS CMD_DEPOSITS
              FROM CAGE_MOVEMENTS 
                   INNER JOIN CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          
--             WHERE CGM_TYPE = 3
        WHERE CGM_TYPE IN (99, 199)
               AND CGM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
          GROUP BY CMD_ISO_CODE, CMD_DENOMINATION, CGM_TYPE, CMD_QUANTITY
            ) AS T1           
   GROUP BY ORIGIN, CMD_ISO_CODE
           
    UNION ALL
              
    SELECT NULL AS ORIGIN
         , CMD_ISO_CODE AS ISO_CODE
         , ISNULL(SUM(CMD_DEPOSITS), 0) AS DEPOSITS
      FROM (SELECT CMD_ISO_CODE
                 , ISNULL(SUM (CASE WHEN CMD_QUANTITY >= 0 THEN CMD_QUANTITY 
                                    ELSE 1 END * CASE WHEN CGM_TYPE = 99 THEN -1 * CMD_DENOMINATION 
                                                      ELSE CMD_DENOMINATION END), 0) AS CMD_DEPOSITS
              FROM CAGE_MOVEMENTS 
                   INNER JOIN CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          
             WHERE CGM_TYPE IN (99, 199)
               AND CGM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
          GROUP BY CMD_ISO_CODE, CMD_DENOMINATION, CGM_TYPE, CMD_QUANTITY
           ) AS T1           
  GROUP BY CMD_ISO_CODE

    DROP TABLE #TMP_CAGE_SESSIONS 

  END -- IF @pCageSessionIds IS NOT NULL THEN
      
  ELSE -- IF @pCageSessionIds IS NULL
  BEGIN
      
    -- TABLE[1]: Get cage sessions information
    SELECT CM.CM_SOURCE_TARGET_ID
         , CM.CM_CONCEPT_ID
         , CM.CM_ISO_CODE
         , CM.CM_VALUE_IN
         , CM.CM_VALUE_OUT
         , CST.CST_SOURCE_TARGET_NAME
         , CC.CC_DESCRIPTION
      FROM CAGE_METERS AS CM
           INNER JOIN CAGE_SOURCE_TARGET_CONCEPTS AS CSTC ON CSTC.CSTC_CONCEPT_ID = CM.CM_CONCEPT_ID AND CSTC.CSTC_SOURCE_TARGET_ID = CM.CM_SOURCE_TARGET_ID
           INNER JOIN CAGE_CONCEPTS AS CC ON CC.CC_CONCEPT_ID = CSTC.CSTC_CONCEPT_ID
           INNER JOIN CAGE_SOURCE_TARGET AS CST ON CST.CST_SOURCE_TARGET_ID = CSTC.CSTC_SOURCE_TARGET_ID
     WHERE (CC.CC_TYPE = 1 OR CC_CONCEPT_ID IN (9, 10, 11))
       AND CC.CC_ENABLED = 1
       AND CSTC.CSTC_ENABLED = 1
       AND CC.CC_SHOW_IN_REPORT = 1
       AND CM_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)
  ORDER BY CM.CM_SOURCE_TARGET_ID
         , CASE WHEN CM.CM_CONCEPT_ID = 0 THEN '000' ELSE CC.CC_DESCRIPTION END
         , CASE WHEN CM.CM_ISO_CODE = @NationalIsoCode THEN '000' ELSE CM.CM_ISO_CODE END   

    -- TABLE[2]: Get dynamic liabilities
    SELECT CC.CC_CONCEPT_ID AS LIABILITY_CONCEPT_ID
         , CC.CC_DESCRIPTION AS LIABILITY_NAME 
         , CM.CM_ISO_CODE AS LIABILITY_ISO_CODE
         , SUM(CM.CM_VALUE) AS LIABILITY_VALUE
      FROM CAGE_METERS AS CM
           INNER JOIN CAGE_CONCEPTS AS CC ON CC.CC_CONCEPT_ID = CM.CM_CONCEPT_ID
     WHERE CC.CC_ENABLED = 1
       AND CC.CC_SHOW_IN_REPORT = 1
       AND CC.CC_IS_PROVISION = 1
       AND CM_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)
  GROUP BY CC.CC_CONCEPT_ID, CC.CC_DESCRIPTION, CM.CM_ISO_CODE
  ORDER BY CC.CC_CONCEPT_ID ASC 
         , CASE WHEN CM.CM_ISO_CODE = @NationalIsoCode THEN '000' ELSE CM.CM_ISO_CODE END 
      
    -- TABLE[3]: Get session sumary
    SELECT CMD_ISO_CODE AS ISO_CODE
         , ISNULL(ORIGIN, 0) AS ORIGIN
         , SUM(CMD_DEPOSITS) AS DEPOSITS
         , SUM(CMD_WITHDRAWALS) AS WITHDRAWALS     
      FROM (SELECT CMD_ISO_CODE
                 , ORIGIN
                 , SUM(CMD_DEPOSITS) CMD_DEPOSITS
                 , SUM(CMD_WITHDRAWALS) CMD_WITHDRAWALS      
              FROM (SELECT CASE WHEN CMD_QUANTITY >= 0 THEN @BillsId
                                ELSE CMD_QUANTITY END AS ORIGIN
                         , CMD_ISO_CODE
                         , CMD_DENOMINATION
                         , CASE WHEN CGM_TYPE IN (100, 101, 103, 104) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY < 0 THEN CMD_DENOMINATION
                                                                                            ELSE CMD_QUANTITY * CMD_DENOMINATION END), 0)
                                ELSE 0 END AS CMD_DEPOSITS
                         , CASE WHEN CGM_TYPE IN (0, 1, 4, 5) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY < 0 THEN CMD_DENOMINATION             
                                                                                    ELSE CMD_QUANTITY * CMD_DENOMINATION END), 0)
                                ELSE 0 END AS CMD_WITHDRAWALS
                      FROM CAGE_MOVEMENTS 
                           INNER JOIN   CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          
                     WHERE CGM_STATUS IN (2, 3, 4, 8, 9, 10) 
                        OR (CGM_STATUS = 0 AND CGM_TYPE = 0)
                  GROUP BY CMD_ISO_CODE, CMD_DENOMINATION, CGM_TYPE, CMD_QUANTITY


                    ) AS T1
           GROUP BY ORIGIN,CMD_ISO_CODE

              -- GET Terminal value-in amounts
              
              UNION ALL 
              
              SELECT ISNULL(TI_CUR0, @NationalIsoCode) AS CMD_ISO_CODE
                    , @TicketsId AS ORIGIN
                    , ISNULL(SUM (CASE WHEN TI_AMT0 IS NULL THEN TI_AMOUNT 
                                       ELSE TI_AMT0 END),0)  AS DEPOSITS
                    , 0 AS WITHDRAWS
              FROM MONEY_COLLECTIONS
                  INNER JOIN CAGE_MOVEMENTS ON CGM_MC_COLLECTION_ID = MC_COLLECTION_ID 
				  INNER JOIN   TICKETS ON TICKETS.TI_COLLECTED_MONEY_COLLECTION = CAGE_MOVEMENTS.CGM_MC_COLLECTION_ID            
             WHERE CAGE_MOVEMENTS.CGM_TYPE = @CAGEOPERATIONTYPE_FROMTERMINAL
               AND (CGM_STATUS IN (2, 3, 4, 8, 9, 10) 
                    OR (CGM_STATUS = 0 AND CGM_TYPE = 0))
             GROUP BY TI_CUR0, CGM_TYPE

              UNION ALL
              
                SELECT ISNULL(te_iso_code,@NationalIsoCode) AS CMD_ISO_CODE
                    , @BillsId AS ORIGIN
                    , MC_COLLECTED_BILL_AMOUNT + MC_COLLECTED_COIN_AMOUNT  AS DEPOSITS
                    , 0 AS WITHDRAWS
              FROM MONEY_COLLECTIONS
                  INNER JOIN CAGE_MOVEMENTS ON CGM_MC_COLLECTION_ID = MC_COLLECTION_ID 
				  INNER JOIN TERMINALS ON TE_TERMINAL_ID = MC_TERMINAL_ID
             WHERE CAGE_MOVEMENTS.CGM_TYPE = @CageOperationType_FromTerminal
               AND (CGM_STATUS IN (2, 3, 4, 8, 9, 10) 
                    OR (CGM_STATUS = 0 AND CGM_TYPE = 0))
                          
          ) AS _TBL_GLB 
      WHERE CMD_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)             
   GROUP BY _TBL_GLB.CMD_ISO_CODE, _TBL_GLB.ORIGIN
   ORDER BY CASE WHEN _TBL_GLB.CMD_ISO_CODE = @NationalIsoCode THEN '000' ELSE _TBL_GLB.CMD_ISO_CODE END 
          , _TBL_GLB.ORIGIN DESC 
         
    -- TABLE[4]: Other System Concepts           
    SELECT CM_CONCEPT_ID
         , CC.CC_DESCRIPTION AS CM_DESCRIPTION
         , CM_ISO_CODE
         , SUM(CM_VALUE) AS CM_VALUE
      FROM CAGE_METERS AS CM
           INNER JOIN CAGE_CONCEPTS AS CC ON CM.CM_CONCEPT_ID = CC.CC_CONCEPT_ID
     WHERE CM_SOURCE_TARGET_ID = 0
       AND CM_CONCEPT_ID IN (7, 8)
       AND CM_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)
  GROUP BY CM_CONCEPT_ID, CM_ISO_CODE, CM_VALUE, CC.CC_DESCRIPTION
  ORDER BY CM_CONCEPT_ID
         , CASE WHEN CM_ISO_CODE = @NationalIsoCode THEN '000' ELSE CM_ISO_CODE END
    
    -- TABLE[5]: Cage Counts
    SELECT ORIGIN AS ORIGIN
         , CMD_ISO_CODE AS ISO_CODE
         , ISNULL(SUM(CMD_DEPOSITS), 0) AS DEPOSITS
      FROM (SELECT CASE WHEN CMD_QUANTITY >= 0 THEN 0
                        ELSE CMD_QUANTITY END  AS ORIGIN
                 , CMD_ISO_CODE
                 , ISNULL(SUM (CASE WHEN CMD_QUANTITY >= 0 THEN CMD_QUANTITY 
                                    ELSE 1 END * CASE WHEN CGM_TYPE = 99 THEN -1 * CMD_DENOMINATION 
                                                      ELSE CMD_DENOMINATION END), 0) AS CMD_DEPOSITS
              FROM CAGE_MOVEMENTS 
                   INNER JOIN CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          
             WHERE CGM_TYPE IN (99, 199)
          GROUP BY CMD_ISO_CODE, CMD_DENOMINATION, CGM_TYPE, CMD_QUANTITY
            ) AS T1           
   GROUP BY ORIGIN, CMD_ISO_CODE
           
    UNION ALL
              
    SELECT NULL AS ORIGIN
         , CMD_ISO_CODE AS ISO_CODE
         , ISNULL(SUM(CMD_DEPOSITS), 0) AS DEPOSITS
      FROM (SELECT CMD_ISO_CODE
                 , ISNULL(SUM (CASE WHEN CMD_QUANTITY >= 0 THEN CMD_QUANTITY 
                                    ELSE 1 END * CASE WHEN CGM_TYPE = 99 THEN -1 * CMD_DENOMINATION 
                                                      ELSE CMD_DENOMINATION END), 0) AS CMD_DEPOSITS
              FROM CAGE_MOVEMENTS 
                   INNER JOIN CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          
             WHERE CGM_TYPE IN (99, 199)
          GROUP BY CMD_ISO_CODE, CMD_DENOMINATION, CGM_TYPE, CMD_QUANTITY
           ) AS T1           
  GROUP BY CMD_ISO_CODE


  END -- ELSE
  
  -- TABLE[6]: Stock Stored 
  SELECT @StockIsStored
  
  DROP TABLE #TMP_CURRENCIES_ISO_CODES
                   
END -- CageGetGlobalReportData

GO
GRANT EXECUTE ON [dbo].[CageGetGlobalReportData] TO [wggui] WITH GRANT OPTION 
GO

--***** CageStockOnCloseSession 
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CageStockOnCloseSession]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[CageStockOnCloseSession]
GO

CREATE PROCEDURE [dbo].[CageStockOnCloseSession]
        @pCageSessionId NVARCHAR(MAX) = NULL
AS
BEGIN
  
  DECLARE @List VARCHAR(MAX) 

  DECLARE @TMP_STOCK_GEN TABLE
  (
     CGS_ISO_CODE NVARCHAR(MAX)                                        
     , CGS_DENOMINATION MONEY                                      
     , CGS_QUANTITY MONEY  
     , CGS_CAGE_CURRENCY_TYPE INT
  )
  --Get Current Stock
  INSERT INTO @TMP_STOCK_GEN
  EXEC CageCurrentStock
  
  --Parse current Stock                                                
  SELECT  @List = COALESCE(@List + '', '') + RTRIM(LTRIM(CGS_ISO_CODE))      
      + ';'+ RTRIM(LTRIM(CAST(CGS_DENOMINATION AS VARCHAR)))       
      + ';'+ RTRIM(LTRIM(CAST(CGS_QUANTITY AS VARCHAR)))           
      + ';'+ RTRIM(LTRIM(CAST(CGS_CAGE_CURRENCY_TYPE AS VARCHAR))) 
      + '|'                                                        
   FROM @TMP_STOCK_GEN
   
   --Save current stock on session    
   UPDATE CAGE_SESSIONS                                                       
    SET CGS_CAGE_STOCK = @List                                              
   WHERE CGS_CAGE_SESSION_ID = @pCageSessionId                                     

END --CageStockOnCloseSession

GO
GRANT EXECUTE ON [dbo].[CageStockOnCloseSession] TO [wggui] WITH GRANT OPTION 
GO

--***** CageCurrentStock 
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CageCurrentStock]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CageCurrentStock]
GO

CREATE PROCEDURE [dbo].[CageCurrentStock]
AS
BEGIN
  --Get current stock     
  SELECT CGS_ISO_CODE                                          
     , CGS_DENOMINATION                                      
     , CGS_QUANTITY 
     , CGS_CAGE_CURRENCY_TYPE
  FROM (SELECT CGS_ISO_CODE                                          
         , CGS_DENOMINATION                                      
         , CGS_QUANTITY                     
         , CASE WHEN CGS_DENOMINATION > 0 THEN ISNULL(CGS_CAGE_CURRENCY_TYPE,0) ELSE 99 END AS CGS_CAGE_CURRENCY_TYPE                     
      FROM CAGE_STOCK
                
      UNION ALL
                
      SELECT C.CH_ISO_CODE AS CGS_ISO_CODE
         , C.CH_DENOMINATION AS CGS_DENOMINATION
         , CST.CHSK_QUANTITY AS CGS_QUANTITY
         , 0 AS CGS_CAGE_CURRENCY_TYPE
      FROM CHIPS_STOCK AS CST
         INNER JOIN CHIPS AS C ON CST.CHSK_CHIP_ID = C.CH_CHIP_ID
      ) AS _tbl 
  WHERE CGS_QUANTITY > 0  OR CGS_DENOMINATION = -100  
  ORDER BY CGS_ISO_CODE, CGS_CAGE_CURRENCY_TYPE ASC, CGS_DENOMINATION DESC
    , CASE WHEN CGS_DENOMINATION >= 0 THEN CGS_DENOMINATION 
         ELSE CGS_DENOMINATION * (-100000) END

END --CageCurrentStock

GO
GRANT EXECUTE ON [dbo].[CageCurrentStock] TO [wggui] WITH GRANT OPTION 
GO 
