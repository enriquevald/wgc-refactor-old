/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 437;

SET @New_ReleaseId = 438;

SET @New_ScriptName = N'UpdateTo_18.438.041.sql';
SET @New_Description = N'New release v03.007.0005'; 

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO



/**** GENERAL PARAM *****/
IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Terminal' AND GP_SUBJECT_KEY = 'Reservation.Enabled' )
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Terminal', 'Reservation.Enabled', '0' ); 
GO  

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Terminal' AND GP_SUBJECT_KEY = 'Reservation.MonitorRefreshInterval' )
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Terminal', 'Reservation.MonitorRefreshInterval', '5' ); 
GO

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'BucketsWebService' AND GP_SUBJECT_KEY = 'Enabled')
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('BucketsWebService', 'Enabled', '0')
GO

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'BucketsWebService' AND GP_SUBJECT_KEY = 'WCP.FixedIP')
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('BucketsWebService', 'WCP.FixedIP', '')
GO

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'BucketsWebService' AND GP_SUBJECT_KEY = 'WCP.Port')
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('BucketsWebService', 'WCP.Port', '7788')
GO

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'BucketsWebService' AND GP_SUBJECT_KEY = 'ExtendedLog')
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('BucketsWebService', 'ExtendedLog', '0')
GO

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'BucketsWebService' AND GP_SUBJECT_KEY = 'Update.AllowNegativeResults')
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('BucketsWebService', 'Update.AllowNegativeResults', '0')
GO


/**** TABLES *****/
IF (EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'DBO' 
                 AND  TABLE_NAME = 'RESERVED_TERMINAL_CONFIGURATION'))
BEGIN
	IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[reserved_terminal_configuration]') and name = 'rtc_enabled')
	BEGIN 
		ALTER TABLE [dbo].[reserved_terminal_configuration] ADD rtc_enabled BIT DEFAULT 'FALSE'	
	END
	IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[reserved_terminal_configuration]') and name = 'rtc_coin_in')
	BEGIN 
		ALTER TABLE [dbo].[reserved_terminal_configuration] ADD rtc_coin_in money default 0
	END
END
GO

IF NOT EXISTS( SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'account_movements' AND COLUMN_NAME = 'am_modified_bucket_reason') 
BEGIN
	ALTER TABLE [DBO].[ACCOUNT_MOVEMENTS] ADD am_modified_bucket_reason INT NULL;
END
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_money_collection_contadores_diarios]'))
	DROP VIEW [dbo].[vw_money_collection_contadores_diarios]
GO

CREATE VIEW [dbo].[vw_money_collection_contadores_diarios]
	AS
		SELECT     TOP (100) PERCENT mc_collection_id, mc_datetime, mc_collected_bill_amount, mc_collected_coin_amount
		FROM         dbo.money_collections
		ORDER BY mc_datetime DESC
GO

IF EXISTS(SELECT * FROM sys.objects WHERE object_id  = OBJECT_ID('reserved_terminal_configuration') AND type IN ('U'))
	DROP TABLE reserved_terminal_configuration

CREATE TABLE reserved_terminal_configuration
  (
	  rtc_holder_level  		INT NOT NULL,
	  rtc_minutes       		INT NOT NULL,
	  rtc_terminal_list 		XML NULL,
	  rtc_selected_terminals 	XML NULL,
	  rtc_create_date   		DATETIME NOT NULL,
	  rtc_update_date   		DATETIME NOT NULL,
	  rtc_status        		INT NOT NULL,
	  rtc_enabled				BIT DEFAULT 'FALSE',
	  rtc_coin_in money 		DEFAULT 0
	  CONSTRAINT pk_reserved_terminal_configuration PRIMARY KEY (rtc_holder_level)
  )     
GO
  
/**** RECORDS *****/


/**** STORED PROCEDURES *****/


/******* TRIGGERS *******/
