/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 43;

SET @New_ReleaseId = 44;
SET @New_ScriptName = N'UpdateTo_18.044.010.sql';
SET @New_Description = N'Changes required for GLI Certification (13 and 16).';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO


/****** TABLES ******/

/* Accounts */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[Accounts]') and name = 'ac_pin')
  ALTER TABLE [dbo].[Accounts]
          ADD [ac_pin] [nvarchar] (12) NULL;
ELSE
  SELECT '***** Field Accounts.ac_pin already exists *****';

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[Accounts]') and name = 'ac_pin_failures')
  ALTER TABLE [dbo].[Accounts]
          ADD [ac_pin_failures] [int] NULL;
ELSE
  SELECT '***** Field Accounts.ac_pin_failures already exists *****';

/* Terminals */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[Terminals]') and name = 'te_denomination')
  ALTER TABLE [dbo].[Terminals]
          ADD [te_denomination] [money] NULL;
ELSE
  SELECT '***** Field Terminals.te_denomination already exists *****';

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[Terminals]') and name = 'te_multi_denomination')
  ALTER TABLE [dbo].[Terminals]
          ADD [te_multi_denomination] [nvarchar] (40) NULL;
ELSE
  SELECT '***** Field Terminals.te_multi_denomination already exists *****';

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[Terminals]') and name = 'te_program')
  ALTER TABLE [dbo].[Terminals]
          ADD [te_program] [nvarchar] (40) NULL;
ELSE
  SELECT '***** Field Terminals.te_program already exists *****';

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[Terminals]') and name = 'te_theoretical_payout')
  ALTER TABLE [dbo].[Terminals]
          ADD [te_theoretical_payout] [money] NULL;
ELSE
  SELECT '***** Field Terminals.te_theoretical_payout already exists *****';
/* Force previous statements to be executed so that pre-requisite field will be ready */  
GO  

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[Terminals]') and name = 'te_theoretical_hold')
  ALTER TABLE [dbo].[Terminals]
          ADD [te_theoretical_hold] AS (1.00-te_theoretical_payout);
ELSE
  SELECT '***** Field Terminals.te_theoretical_hold already exists *****';

/* Machine_Meters */
IF NOT EXISTS (SELECT * FROM SYS.OBJECTS WHERE TYPE = 'U' AND NAME = 'machine_meters')
  BEGIN
    CREATE TABLE [dbo].[machine_meters](
      [mm_terminal_id] [int] NOT NULL,
      [mm_wcp_sequence_id] [bigint] NOT NULL,
      [mm_played_count] [bigint] NULL,
      [mm_played_amount] [money] NULL,
      [mm_won_count] [bigint] NULL,
      [mm_won_amount] [money] NULL,
      [mm_aft_to_gm_count] [bigint] NULL,
      [mm_aft_to_gm_amount] [money] NULL,
      [mm_aft_from_gm_count] [bigint] NULL,
      [mm_aft_from_gm_amount] [money] NULL,
      [mm_eft_to_gm_count] [bigint] NULL,
      [mm_eft_to_gm_amount] [money] NULL,
      [mm_eft_from_gm_count] [bigint] NULL,
      [mm_eft_from_gm_amount] [money] NULL,
      [mm_last_reported] [datetime] NOT NULL,
      [mm_delta_played_count] [bigint] NOT NULL CONSTRAINT [DF_machine_meters_mm_delta_played_count]  DEFAULT ((0)),
      [mm_delta_played_amount] [money] NOT NULL CONSTRAINT [DF_machine_meters_mm_delta_played_amount]  DEFAULT ((0)),
      [mm_delta_won_count] [bigint] NOT NULL CONSTRAINT [DF_machine_meters_mm_delta_won_count]  DEFAULT ((0)),
      [mm_delta_won_amount] [money] NOT NULL CONSTRAINT [DF_machine_meters_mm_delta_won_amount]  DEFAULT ((0)),
      [mm_delta_to_gm_count] [bigint] NOT NULL CONSTRAINT [DF_machine_meters_mm_delta_aft_to_gm_count]  DEFAULT ((0)),
      [mm_delta_to_gm_amount] [money] NOT NULL CONSTRAINT [DF_machine_meters_mm_delta_aft_to_gm_amount]  DEFAULT ((0)),
      [mm_delta_from_gm_count] [bigint] NOT NULL CONSTRAINT [DF_machine_meters_mm_delta_aft_from_gm_count]  DEFAULT ((0)),
      [mm_delta_from_gm_amount] [money] NOT NULL CONSTRAINT [DF_machine_meters_mm_delta_aft_from_gm_amount]  DEFAULT ((0)),
      [mm_delta_updating] [bit] NOT NULL CONSTRAINT [DF_machine_meters_mm_delta_updating]  DEFAULT ((0)),
    CONSTRAINT [PK_machine_meters] PRIMARY KEY CLUSTERED 
    (
      [mm_terminal_id] ASC
    )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
    ) ON [PRIMARY]
  END
ELSE
    SELECT '***** Table Machine_Meters already exists *****';

/* Machine_stats_per_hour */
IF NOT EXISTS (SELECT * FROM SYS.OBJECTS WHERE TYPE = 'U' AND NAME = 'machine_stats_per_hour')
  BEGIN
    CREATE TABLE [dbo].[machine_stats_per_hour](
      [msh_base_hour] [datetime] NOT NULL,
      [msh_terminal_id] [int] NOT NULL,
      [msh_played_count] [bigint] NOT NULL,
      [msh_played_amount] [money] NOT NULL,
      [msh_won_count] [bigint] NOT NULL,
      [msh_won_amount] [money] NOT NULL,
      [msh_to_gm_count] [bigint] NOT NULL,
      [msh_to_gm_amount] [money] NOT NULL,
      [msh_from_gm_count] [bigint] NOT NULL,
      [msh_from_gm_amount] [money] NOT NULL,
      [msh_timestamp] [timestamp] NOT NULL,
    CONSTRAINT [PK_machine_stats_per_hour] PRIMARY KEY CLUSTERED 
    (
      [msh_base_hour] ASC,
      [msh_terminal_id] ASC
    )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
    ) ON [PRIMARY]
  END
ELSE
    SELECT '***** Table Machine_stats_per_hour already exists *****';


/****** INDEXES ******/


/****** RECORDS ******/
/* Account.RequestPin */
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Account' AND GP_SUBJECT_KEY ='RequestPin')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key]
             ,[gp_subject_key]
             ,[gp_key_value])
       VALUES
             ('Account'
             ,'RequestPin'
             ,'0');
ELSE
  SELECT '***** Record Account.RequestPin already exists *****';



/**************************************** 3GS Section ****************************************/

