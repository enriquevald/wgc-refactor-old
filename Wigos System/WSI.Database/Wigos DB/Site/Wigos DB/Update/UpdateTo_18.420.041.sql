/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 419;

SET @New_ReleaseId = 420;

SET @New_ScriptName = N'UpdateTo_18.420.041.sql';
SET @New_Description = N'New release v03.006.0021'; 

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO



/**** GENERAL PARAM *****/


/******* TABLES  *******/


/******* RECORDS  *******/


/******* STORED PROCEDURES  *******/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GenerateTablesActivity]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[sp_GenerateTablesActivity]
GO

CREATE PROCEDURE [dbo].[sp_GenerateTablesActivity]
AS
BEGIN

	DECLARE @CurrentDay AS DATETIME
	SELECT @CurrentDay = [dbo].[TodayOpening] (1)

	SET NOCOUNT ON;


	-- Insert all gaming tables to terminals connected with default values to 0
	INSERT INTO   GAMING_TABLES_CONNECTED
		   SELECT   GT_GAMING_TABLE_ID, CONVERT(INT,CONVERT(VARCHAR(8), @CurrentDay, 112)), 0, 0
		     FROM   GAMING_TABLES
	  LEFT JOIN   GAMING_TABLES_CONNECTED ON GMC_GAMINGTABLE_ID = GT_GAMING_TABLE_ID AND GMC_GAMING_DAY = CONVERT(INT,CONVERT(VARCHAR(8), @CurrentDay, 112))
		    WHERE   GMC_GAMING_DAY IS NULL

    -- Update Enabled/Used
	UPDATE   GAMING_TABLES_CONNECTED
	   SET   GMC_ENABLED = (CASE WHEN S.S_ENABLED = 1 OR GMC_ENABLED = 1 THEN 1 ELSE 0 END)
		     , GMC_USED = CASE WHEN S.S_USED = 1 OR GMC_USED = 1 THEN 1 ELSE 0 END
	  FROM 
			(
				  SELECT   GT_GAMING_TABLE_ID AS S_GAMING_TABLE_ID
					       , CONVERT(INT,CONVERT(VARCHAR(8), @CurrentDay, 112)) AS S_GAMING_DAY
					       , GT_ENABLED AS S_ENABLED
					       , CASE WHEN MIN(GTS.GTS_CASHIER_SESSION_ID) > 0 THEN 1 ELSE 0 END AS S_USED
				    FROM   GAMING_TABLES
			INNER JOIN   GAMING_TABLES_CONNECTED ON GMC_GAMINGTABLE_ID = GT_GAMING_TABLE_ID AND GMC_GAMING_DAY = CONVERT(INT,CONVERT(VARCHAR(8), @CurrentDay, 112))
			 LEFT JOIN   GAMING_TABLES_SESSIONS GTS ON GMC_GAMINGTABLE_ID = GTS_GAMING_TABLE_ID  AND GMC_GAMING_DAY = CONVERT(INT,CONVERT(VARCHAR(8), @CurrentDay, 112))
			 LEFT JOIN   CASHIER_SESSIONS CS ON CS.CS_SESSION_ID = GTS.GTS_CASHIER_SESSION_ID
						 AND (
							   (@CurrentDay >= CONVERT(DATETIME,CS.CS_OPENING_DATE) AND @CurrentDay <= CONVERT(DATETIME,CS.CS_CLOSING_DATE)) 
							OR (@CurrentDay >= CONVERT(DATETIME,CS.CS_OPENING_DATE) AND CONVERT(DATETIME,CS.CS_CLOSING_DATE) IS NULL)
							OR (CONVERT(DATETIME,CS.CS_OPENING_DATE) > @CurrentDay AND CONVERT(DATETIME,CS.CS_OPENING_DATE) <= DATEADD(D,1,@CurrentDay))
							OR (CONVERT(DATETIME,CS.CS_CLOSING_DATE) > @CurrentDay AND CONVERT(DATETIME,CS.CS_CLOSING_DATE) <= DATEADD(D,1,@CurrentDay))
							 )
			  GROUP BY   GT_GAMING_TABLE_ID, GT_ENABLED  
			)S
	 WHERE   S.S_GAMING_TABLE_ID = GAMING_TABLES_CONNECTED.GMC_GAMINGTABLE_ID AND GMC_GAMING_DAY = S_GAMING_DAY

END

GO

GRANT EXECUTE ON [sp_GenerateTablesActivity] TO [WGGUI] WITH GRANT OPTION
GO

IF  EXISTS (SELECT * FROM SYS.OBJECTS WHERE OBJECT_ID = OBJECT_ID(N'[DBO].[sp_GetTablesActivity]') AND TYPE IN (N'P', N'PC'))
DROP PROCEDURE [DBO].[sp_GetTablesActivity]
GO

CREATE PROCEDURE [DBO].[sp_GetTablesActivity]
@pDate AS DATETIME
AS
BEGIN

DECLARE @Count INT
DECLARE @Cols VARCHAR(MAX)
DECLARE @ColsVal VARCHAR(MAX)
DECLARE @ColsValU VARCHAR(MAX)
DECLARE @ColsEnabled VARCHAR(MAX)
DECLARE @ColsUsed VARCHAR(MAX)

SELECT @Count = 0
SELECT @Cols = ''
SELECT @ColsVal = ''
SELECT @ColsValU = ''
SELECT @ColsEnabled = ''
SELECT @ColsUsed = ''

WHILE @Count < DAY(DATEADD(DD,-1,DATEADD(MM,DATEDIFF(MM,-1,@pDate),0)))
BEGIN
   SELECT @Count = @Count + 1
   
   IF @Count <> 1
    BEGIN
		SELECT @Cols = @Cols + ','
		SELECT @ColsVal = @ColsVal + ','
		SELECT @ColsValU = @ColsValU + ','
		SELECT @ColsEnabled = @ColsEnabled + ','
		SELECT @ColsUsed = @ColsUsed + ','
	END
	
   SELECT @Cols = @Cols + '[' + CONVERT(VARCHAR,@Count) + ']'
   SELECT @ColsVal = @ColsVal + ' ISNULL([' + CONVERT(VARCHAR,@Count) + '],0) As D' + CONVERT(VARCHAR,@Count) + ''
   SELECT @ColsValU = @ColsValU + ' ISNULL([' + CONVERT(VARCHAR,@Count) + '],0) As DU' + CONVERT(VARCHAR,@Count) + ''
   SELECT @ColsEnabled = @ColsEnabled + '[D' + CONVERT(VARCHAR,@Count) + ']'
   SELECT @ColsUsed = @ColsUsed + '[DU' + CONVERT(VARCHAR,@Count) + ']'
    
END

DECLARE @Query NVARCHAR(MAX)
SET @Query = '           
			
			DECLARE @DATE AS DATETIME
			SELECT @DATE = ''' + CONVERT(VARCHAR(10),@pDate,112) + '''

			SELECT 
				A.GMC_GAMINGTABLE_ID T_ID,
				A.GT_NAME T_NAME,
				A.GT_TYPE T_TYPE,
				A.GMC_MONTH T_MONTH,
				A.T_DAYS,
				' + @ColsEnabled + ',
				A.T_DAYSUSE,
				' + @ColsUsed + '
			FROM 
			(
				SELECT 
					GMC_GAMINGTABLE_ID , 
					GT_TYPE,
					GT_NAME,
					CASE WHEN T_DAYS > 0 THEN 1 ELSE 0 END GMC_MONTH, 
					T_DAYS,
					T_DAYSUSE,
					' + @ColsVal + '
				FROM 
				(
					SELECT 
						GMC.GMC_GAMINGTABLE_ID, 
						GTT.GTT_NAME GT_TYPE,
						GT.GT_NAME GT_NAME,
						YEAR(CONVERT(DATETIME,CONVERT(VARCHAR(8),GMC.GMC_GAMING_DAY))) AS GMC_YEAR, 
						MONTH(CONVERT(DATETIME,CONVERT(VARCHAR(8),GMC.GMC_GAMING_DAY))) AS GMC_MONTH, 
						DAY(CONVERT(DATETIME,CONVERT(VARCHAR(8),GMC.GMC_GAMING_DAY))) AS GMN_DAY, 
						CONVERT(INT,GMC.GMC_ENABLED) GMC_ENABLED, 
						(SELECT COUNT(*) FROM GAMING_TABLES_CONNECTED AS CO 
						  WHERE CO.GMC_ENABLED = 1 
							AND YEAR(CONVERT(DATETIME,CONVERT(VARCHAR(8),CO.GMC_GAMING_DAY))) = YEAR(@DATE) 
							AND MONTH(CONVERT(DATETIME,CONVERT(VARCHAR(8),CO.GMC_GAMING_DAY))) = MONTH(@DATE) 
							AND CO.GMC_GAMINGTABLE_ID = GMC.GMC_GAMINGTABLE_ID ) AS T_DAYS,
						(SELECT COUNT(*) FROM GAMING_TABLES_CONNECTED AS CO 
						  WHERE CO.GMC_USED  = 1 
							AND YEAR(CONVERT(DATETIME,CONVERT(VARCHAR(8),CO.GMC_GAMING_DAY))) = YEAR(@DATE) 
							AND MONTH(CONVERT(DATETIME,CONVERT(VARCHAR(8),CO.GMC_GAMING_DAY))) = MONTH(@DATE) 
							AND CO.GMC_GAMINGTABLE_ID = GMC.GMC_GAMINGTABLE_ID ) AS T_DAYSUSE
						FROM GAMING_TABLES_CONNECTED AS GMC
							INNER JOIN GAMING_TABLES GT
								ON GT.GT_GAMING_TABLE_ID = GMC.GMC_GAMINGTABLE_ID
							INNER JOIN GAMING_TABLES_TYPES GTT
								ON GTT.GTT_GAMING_TABLE_TYPE_ID = GT.GT_TYPE_ID
						WHERE  YEAR(CONVERT(DATETIME,CONVERT(VARCHAR(8),GMC.GMC_GAMING_DAY))) = YEAR(@DATE)
							AND MONTH(CONVERT(DATETIME,CONVERT(VARCHAR(8),GMC.GMC_GAMING_DAY))) = MONTH(@DATE) 
						GROUP BY GMC.GMC_GAMINGTABLE_ID, GTT.GTT_NAME, GT.GT_NAME, YEAR(CONVERT(DATETIME,CONVERT(VARCHAR(8),GMC.GMC_GAMING_DAY))), MONTH(CONVERT(DATETIME,CONVERT(VARCHAR(8),GMC.GMC_GAMING_DAY))), DAY(CONVERT(DATETIME,CONVERT(VARCHAR(8),GMC.GMC_GAMING_DAY)))
								,GMC.GMC_ENABLED, GMC.GMC_USED
				) AS SourceTable
				PIVOT
				(
					MAX(GMC_ENABLED) FOR GMN_DAY IN (' + @Cols + ')
				) AS PivotTable
			) A 
			INNER JOIN
			( 
				SELECT 
						GMC_GAMINGTABLE_ID , 
						GT_TYPE,
						GT_NAME,
						CASE WHEN T_DAYS > 0 THEN 1 ELSE 0 END GMC_MONTH, 
						T_DAYS,
						T_DAYSUSE,
						' + @ColsValU + '
					FROM 
					(
						SELECT 
							GMC.GMC_GAMINGTABLE_ID, 
							GTT.GTT_NAME GT_TYPE,
							GT.GT_NAME GT_NAME,
							YEAR(CONVERT(DATETIME,CONVERT(VARCHAR(8),GMC.GMC_GAMING_DAY))) AS GMC_YEAR, 
							MONTH(CONVERT(DATETIME,CONVERT(VARCHAR(8),GMC.GMC_GAMING_DAY))) AS GMC_MONTH, 
							DAY(CONVERT(DATETIME,CONVERT(VARCHAR(8),GMC.GMC_GAMING_DAY))) AS GMN_DAY, 
							CONVERT(INT,GMC.GMC_ENABLED) GMC_ENABLED, 
							(SELECT COUNT(*) FROM GAMING_TABLES_CONNECTED AS CO 
							  WHERE CO.GMC_ENABLED = 1 
								AND YEAR(CONVERT(DATETIME,CONVERT(VARCHAR(8),CO.GMC_GAMING_DAY))) = YEAR(@DATE) 
								AND MONTH(CONVERT(DATETIME,CONVERT(VARCHAR(8),CO.GMC_GAMING_DAY))) = MONTH(@DATE) 
								AND CO.GMC_GAMINGTABLE_ID = GMC.GMC_GAMINGTABLE_ID ) AS T_DAYS,
							CONVERT(INT,GMC.GMC_USED) GMC_USED,
							(SELECT COUNT(*) FROM GAMING_TABLES_CONNECTED AS CO 
							  WHERE CO.GMC_USED  = 1 
								AND YEAR(CONVERT(DATETIME,CONVERT(VARCHAR(8),CO.GMC_GAMING_DAY))) = YEAR(@DATE) 
								AND MONTH(CONVERT(DATETIME,CONVERT(VARCHAR(8),CO.GMC_GAMING_DAY))) = MONTH(@DATE) 
								AND CO.GMC_GAMINGTABLE_ID = GMC.GMC_GAMINGTABLE_ID ) AS T_DAYSUSE
							FROM GAMING_TABLES_CONNECTED AS GMC
								INNER JOIN GAMING_TABLES GT
									ON GT.GT_GAMING_TABLE_ID = GMC.GMC_GAMINGTABLE_ID
								INNER JOIN GAMING_TABLES_TYPES GTT
									ON GTT.GTT_GAMING_TABLE_TYPE_ID = GT.GT_TYPE_ID
							WHERE  YEAR(CONVERT(DATETIME,CONVERT(VARCHAR(8),GMC.GMC_GAMING_DAY))) = YEAR(@DATE)
								AND MONTH(CONVERT(DATETIME,CONVERT(VARCHAR(8),GMC.GMC_GAMING_DAY))) = MONTH(@DATE) 
							GROUP BY GMC.GMC_GAMINGTABLE_ID, GTT.GTT_NAME, GT.GT_NAME, YEAR(CONVERT(DATETIME,CONVERT(VARCHAR(8),GMC.GMC_GAMING_DAY))), MONTH(CONVERT(DATETIME,CONVERT(VARCHAR(8),GMC.GMC_GAMING_DAY))), DAY(CONVERT(DATETIME,CONVERT(VARCHAR(8),GMC.GMC_GAMING_DAY)))
									,GMC.GMC_ENABLED, GMC.GMC_USED
					) AS SourceTable
					PIVOT
					(
						MAX(GMC_USED) FOR GMN_DAY IN (' + @Cols + ')
					) AS PivotTable
			) B
			ON B.gmc_gamingtable_id = A.gmc_gamingtable_id
			AND B.GMC_MONTH = A.GMC_MONTH

            '     

EXEC SP_EXECUTESQL @Query

END

GO

GRANT EXECUTE ON [sp_GetTablesActivity] TO [WGGUI] WITH GRANT OPTION
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GT_Base_Report_Data]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[GT_Base_Report_Data]
GO

CREATE PROCEDURE [dbo].[GT_Base_Report_Data]
(
      @pBaseType                   INTEGER
    , @pTimeInterval               INTEGER
    , @pDateFrom                   DATETIME
    , @pDateTo                     DATETIME
    , @pOnlyActivity               INTEGER
    , @pOrderBy                    INTEGER
    , @pValidTypes                 VARCHAR(4096)
    , @pSelectedCurrency           NVARCHAR(100)
    , @pTotalToSelectedCurrency    INTEGER
)
AS
BEGIN

-- DECLARATIONS
   -- @_DAYS_AND_TABLES:    A temporary table with all intervals and all base type selected (used to show base types without activity).
   DECLARE @_DAYS_AND_TABLES TABLE(DATE_TIME DATETIME, TABLE_IDENTIFIER_VALUE BIGINT, TABLE_IDENT_NAME VARCHAR(50), TABLE_TYPE_IDENT BIGINT, TABLE_TYPE_NAME VARCHAR(50))
   -- @_DAY_VAR:            Variable to create the intervals
   DECLARE @_DAY_VAR DATETIME

-- PARAMETERS
     DECLARE @_BASE_TYPE                  AS   INTEGER -- 0 BY TABLE TYPE, 1 BY TABLE
     DECLARE @_TIME_INTERVAL              AS   INTEGER -- -1, 0 DAYS, 1 MONTH, 2 YEAR
     DECLARE @_DATE_FROM                  AS   DATETIME
     DECLARE @_DATE_TO                    AS   DATETIME
     DECLARE @_ONLY_ACTIVITY              AS   INTEGER -- 0 SHOW ALL, 1 SHOW ONLY WITH ACTIVITY
     DECLARE @_ORDER_BY                   AS   INTEGER -- 0 BY INTERVAL DESC, 1 BY IDENTIFIER ASC
     DECLARE @_DELIMITER                  AS   CHAR(1)
     DECLARE @_TYPES_TABLE                TABLE(SST_ID INT, SST_VALUE VARCHAR(50)) -- TO STORE THE VALID GAMING TABLE TYPES
     DECLARE @_SELECTED_CURRENCY          AS   NVARCHAR(3)
     DECLARE @_TOTAL_TO_SELECTED_CURRENCY AS   INTEGER
     DECLARE @_CHIP_RE                    AS   INTEGER
     DECLARE @_CHIP_NRE                   AS   INTEGER
     DECLARE @_CHIP_COLOR                 AS   INTEGER
----------------------------------------------------------------------------------------------------------------

-- Initialization --
  SET @_BASE_TYPE                  =   @pBaseType
  SET @_TIME_INTERVAL              =   @pTimeInterval
  SET @_DATE_FROM                  =   @pDateFrom
  SET @_DATE_TO                    =   @pDateTo
  SET @_ONLY_ACTIVITY              =   @pOnlyActivity
  SET @_ORDER_BY                   =   @pOrderBy
  SET @_DELIMITER                  =   ','
  SET @_SELECTED_CURRENCY          =   @pSelectedCurrency
  SET @_TOTAL_TO_SELECTED_CURRENCY =   @pTotalToSelectedCurrency
  SET @_CHIP_RE                    =   1001
  SET @_CHIP_NRE                   =   1002
  SET @_CHIP_COLOR                 =   1003
----------------------------------------------------------------------------------------------------------------

-- CHECK DATE PARAMETERS
IF @_DATE_FROM IS NULL
BEGIN
   -- IF DATE FROM IS NULL, USER FIRST POSIBLE DATE
   SET @_DATE_FROM = CAST('' AS DATETIME)
   SET @pDateFrom  = CAST('' AS DATETIME)
END

IF @_DATE_TO IS NULL
BEGIN
   -- IF DATE TO IS NULL, QUERY UNTIL CURRENT DATE
   SET @_DATE_TO = CAST(GETDATE() AS DATETIME)
   SET @pDateTo  = CAST(GETDATE() AS DATETIME)
END

-- ASSIGN TYPES PARAMETER INTO TABLE
INSERT INTO @_TYPES_TABLE SELECT * FROM dbo.SplitStringIntoTable(@pValidTypes, @_DELIMITER, DEFAULT)

IF @_TIME_INTERVAL <> -1
 BEGIN
   -- INTERVALS DATES AND TABLE PREPARATION

   -- PREPARE DATES RANGE DEPENDING ON TIME INTERVAL
   SET @_DATE_FROM = CASE
                        WHEN @_TIME_INTERVAL = 0 THEN -- BY DAY
                           DATEADD(dd, 0, DATEDIFF(dd, 0, @_DATE_FROM))
                        WHEN @_TIME_INTERVAL = 1 THEN -- BY MONTH
                           CAST(CAST(YEAR(@_DATE_FROM) AS VARCHAR(4)) + RIGHT('0' + CAST(MONTH(@_DATE_FROM) AS VARCHAR(2)), 2) + '01' AS DATETIME)
                        WHEN @_TIME_INTERVAL = 2 THEN -- BY YEAR
                           CAST(CAST(YEAR(@_DATE_FROM) AS VARCHAR(4)) + '0101' AS DATETIME)
                     END

   SET @_DATE_TO = CASE
          WHEN @_TIME_INTERVAL = 0 THEN -- BY DAY
                           DATEADD(DAY, 1, CAST(@_DATE_TO AS DATETIME))
                        WHEN @_TIME_INTERVAL = 1 THEN -- BY MONTH
                           DATEADD(MONTH, 1, CAST(CAST(YEAR(@_DATE_TO) AS VARCHAR(4)) + RIGHT('0' + CAST(MONTH(@_DATE_TO) AS VARCHAR(2)), 2) + '01' AS DATETIME))
                        WHEN @_TIME_INTERVAL = 2 THEN -- BY YEAR
                           DATEADD(YEAR, 1, CAST(CAST(YEAR(@_DATE_TO) AS VARCHAR(4)) + '0101' AS DATETIME))
                     END

   -- PREPARE THE TIME INTERVALS TABLE
   SET @_DAY_VAR = @_DATE_FROM
   WHILE @_DAY_VAR < @_DATE_TO
   BEGIN
         -- SET THE LINK FIELD FOR THE RESULTS
         IF @_BASE_TYPE = 0
          BEGIN
            -- LINK WITH TABLE TYPES
            INSERT INTO @_DAYS_AND_TABLES
                 SELECT   CAST(@_DAY_VAR AS DATETIME), X.GTT_GAMING_TABLE_TYPE_ID, X.GTT_NAME AS TABLE_IDENT, NULL, NULL
                   FROM   GAMING_TABLES_TYPES AS X
                  WHERE   X.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
                          AND     X.GTT_ENABLED = 1
          END
         ELSE
          BEGIN
            -- LINK WITH TABLES
            INSERT INTO @_DAYS_AND_TABLES
                 SELECT   CAST(@_DAY_VAR AS DATETIME), X.GT_GAMING_TABLE_ID, X.GT_NAME AS TABLE_IDENT, Z.GTT_GAMING_TABLE_TYPE_ID, Z.GTT_NAME
                   FROM   GAMING_TABLES AS X
              LEFT JOIN   GAMING_TABLES_TYPES AS Z
                     ON   Z.GTT_GAMING_TABLE_TYPE_ID = X.GT_TYPE_ID
                  WHERE   Z.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
                          AND X.GT_ENABLED = 1
          END

          -- SET INCREMENT
          SET @_DAY_VAR = CASE
                               WHEN @_TIME_INTERVAL = 0 THEN DATEADD(DAY,1,@_DAY_VAR)     -- DAY
                               WHEN @_TIME_INTERVAL = 1 THEN DATEADD(MONTH,1,@_DAY_VAR)   -- MONTH
                               WHEN @_TIME_INTERVAL = 2 THEN DATEADD(YEAR,1,@_DAY_VAR)    -- YEAR
                          END
   END

END -- IF INTERVALS <> - 1
ELSE
BEGIN

   -- SET THE LINK FIELD FOR THE RESULTS WITHOUT INTERVALS
   IF @_BASE_TYPE = 0
    BEGIN
      -- LINK WITH TABLE TYPES
      INSERT INTO @_DAYS_AND_TABLES
           SELECT   NULL, X.GTT_GAMING_TABLE_TYPE_ID, X.GTT_NAME AS TABLE_IDENT, NULL, NULL
             FROM   GAMING_TABLES_TYPES AS X
            WHERE   X.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
                    AND X.GTT_ENABLED = 1
    END
   ELSE
    BEGIN
      -- LINK WITH TABLES
      INSERT INTO @_DAYS_AND_TABLES
           SELECT   NULL, X.GT_GAMING_TABLE_ID, X.GT_NAME AS TABLE_IDENT, Z.GTT_GAMING_TABLE_TYPE_ID, Z.GTT_NAME
             FROM   GAMING_TABLES AS X
        LEFT JOIN   GAMING_TABLES_TYPES AS Z
               ON   Z.GTT_GAMING_TABLE_TYPE_ID = X.GT_TYPE_ID
            WHERE   Z.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
                    AND X.GT_ENABLED = 1
    END

 END


-- SELECT INTO TEMPORARY TABLE WITH ALL DATA
-- SPECIFIC REPORT QUERY
  SELECT   X.TABLE_IDENTIFIER
         , X.TABLE_NAME
         , X.TABLE_TYPE
         , X.TABLE_TYPE_NAME
         , SUM(X.COPY_DEALER_VALIDATED_AMOUNT) AS COPY_DEALER_VALIDATED_AMOUNT         
         , SUM(X.S_DROP_GAMBLING_TABLE) + SUM (X.S_DROP_CASHIER) TOTAL_DROP
         , SUM(X.S_DROP_GAMBLING_TABLE) TOTAL_DROP_GAMBLING_TABLE
         , SUM(X.S_DROP_CASHIER) TOTAL_DROP_CASHIER
         , SUM(X.S_WIN) TOTAL_WIN
         , ISNULL(SUM(X.S_TIP),0)  TOTAL_TIP
         , CASE WHEN SUM(X.S_DROP_GAMBLING_TABLE) + SUM (X.S_DROP_CASHIER) = 0 THEN 0 ELSE CAST(SUM(X.S_WIN) AS DECIMAL(18,2)) / CAST(SUM(X.S_DROP_GAMBLING_TABLE) + SUM (X.S_DROP_CASHIER) AS DECIMAL(18,2)) * 100 END AS WIN_DROP
         , MIN(X.THEORIC_HOLD) AS THEORIC_HOLD
         , CASE WHEN SUM(X.S_DROP_GAMBLING_TABLE) + SUM (X.S_DROP_CASHIER) = 0 THEN 0 ELSE SUM(CAST(X.S_TIP AS DECIMAL(18,2))) / CAST(SUM(X.S_DROP_GAMBLING_TABLE) + SUM (X.S_DROP_CASHIER) AS DECIMAL(18,2)) * 100 END AS TIP_DROP
         , SUM(X.S_WIN) + SUM(X.S_TIP) AS WIN_TIPS
         , X.CM_DATE_ONLY
         , MIN(X.OPEN_HOUR) AS OPEN_HOUR
         , MAX(X.CLOSE_HOUR) AS CLOSE_HOUR
         , SUM(X.SESSION_SECONDS) AS SESSION_SECONDS
         , COUNT(X.SESSION_SECONDS) AS SESSION_SUM         
    INTO   #GT_TEMPORARY_REPORT_DATA
   FROM (
           -- CORE QUERY
         SELECT CASE WHEN @_TIME_INTERVAL = 0      -- TO FILTER BY DAY
                     THEN DATEADD(dd, 0, DATEDIFF(dd, 0, CS_OPENING_DATE))
                     WHEN @_TIME_INTERVAL = 1      -- TO FILTER BY MONTH
                     THEN CAST(CAST(YEAR(CS_OPENING_DATE) AS VARCHAR(4)) + RIGHT('0' + CAST(MONTH(CS_OPENING_DATE) AS VARCHAR(2)), 2) + '01' AS DATETIME)
                     WHEN @_TIME_INTERVAL = 2      -- TO FILTER BY YEAR
                     THEN CAST(CAST(YEAR(CS_OPENING_DATE) AS VARCHAR(4)) + '01'+ '01' AS DATETIME)
                        END                                                                                       AS CM_DATE_ONLY
                     , (CASE WHEN @_BASE_TYPE = 0 THEN GTT_GAMING_TABLE_TYPE_ID ELSE GT.GT_GAMING_TABLE_ID END)   AS TABLE_IDENTIFIER -- GET THE BASE TYPE IDENTIFIER
                     , (CASE WHEN @_BASE_TYPE = 0 THEN GTT_NAME ELSE GT.GT_NAME END)                              AS TABLE_NAME       -- GET THE BASE TYPE IDENTIFIER NAME
                     , (CASE WHEN @_BASE_TYPE = 0 THEN NULL ELSE GTT_GAMING_TABLE_TYPE_ID END)                    AS TABLE_TYPE       -- TYPE
                     , (CASE WHEN @_BASE_TYPE = 0 THEN NULL ELSE GTT_NAME END)                                    AS TABLE_TYPE_NAME  -- TYPE NAME

                ,  CASE WHEN @_TOTAL_TO_SELECTED_CURRENCY = 0 
                        THEN DBO.GT_CALCULATE_DROP_GAMBLING_TABLES(ISNULL(GTS_COLLECTED_AMOUNT                , 0)
                                                                         , ISNULL(GTS_COLLECTED_DROPBOX_AMOUNT        , 0)
                                                                         , ISNULL(GTS_COLLECTED_DROPBOX_CHIPS_AMOUNT  , 0)
                                                                         , 0 -- It does not allow takings of dropbox with tickets
                                                                         , GT_HAS_INTEGRATED_CASHIER)
                        WHEN @_TOTAL_TO_SELECTED_CURRENCY = 1 
                        THEN DBO.GT_CALCULATE_DROP_GAMBLING_TABLES(SUM(ISNULL(GTSC_COLLECTED_AMOUNT                , 0))
                                                                         , SUM(ISNULL(CASE WHEN GTSC_TYPE = 0            THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                                           WHEN GTSC_TYPE = 1            THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                                      ELSE 0 END,  0))
                                                                         , SUM(ISNULL(CASE WHEN GTSC_TYPE = @_CHIP_RE    THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                                           WHEN GTSC_TYPE = @_CHIP_NRE   THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                                           WHEN GTSC_TYPE = @_CHIP_COLOR THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                             ELSE 0 END,  0))
                                                                         , 0 -- It does not allow takings of dropbox with tickets
                                                                         , GT_HAS_INTEGRATED_CASHIER)
                             END                                                                                  AS S_DROP_GAMBLING_TABLE

                ,  CASE WHEN @_TOTAL_TO_SELECTED_CURRENCY = 0 
                        THEN DBO.GT_CALCULATE_DROP_CASHIER(ISNULL(GTS_OWN_SALES_AMOUNT, 0)
                                                                 , ISNULL(GTS_EXTERNAL_SALES_AMOUNT, 0))
                        WHEN @_TOTAL_TO_SELECTED_CURRENCY = 1 
                        THEN DBO.GT_CALCULATE_DROP_CASHIER(SUM(ISNULL(GTSC_OWN_SALES_AMOUNT, 0))
                                                                 , SUM(ISNULL(GTSC_EXTERNAL_SALES_AMOUNT, 0)))
                        END                                                                              AS S_DROP_CASHIER

                ,  GT_THEORIC_HOLD AS THEORIC_HOLD
                ,  CASE WHEN @_TOTAL_TO_SELECTED_CURRENCY = 0 
                        THEN DBO.GT_CALCULATE_WIN(ISNULL(GTS_FINAL_CHIPS_AMOUNT      , 0)
                                                        , ISNULL(GTS_INITIAL_CHIPS_AMOUNT    , 0)
                                                        , ISNULL(GTS_FILLS_CHIPS_AMOUNT      , 0)
                                                        , ISNULL(GTS_CREDITS_CHIPS_AMOUNT    , 0)                                                        
                                                        , ISNULL(GTS_TIPS                    , 0)
                                                        , ISNULL(GTS_OWN_SALES_AMOUNT        , 0)
                                                        , ISNULL(GTS_EXTERNAL_SALES_AMOUNT   , 0)
                                                        , ISNULL(GTS_COLLECTED_AMOUNT        , 0)
                                                        , ISNULL(GTS_COLLECTED_DROPBOX_AMOUNT        , 0)
                                                        , ISNULL(GTS_COLLECTED_DROPBOX_CHIPS_AMOUNT  , 0)
                                                        , 0 -- It does not allow takings of dropbox with tickets)
                                                        , GT_HAS_INTEGRATED_CASHIER)
                        WHEN @_TOTAL_TO_SELECTED_CURRENCY = 1 
                        THEN DBO.GT_CALCULATE_WIN(SUM(ISNULL(GTSC_FINAL_CHIPS_AMOUNT   , 0))
                                                        , SUM(ISNULL(GTSC_INITIAL_CHIPS_AMOUNT , 0))
                                                        , SUM(ISNULL(GTSC_FILLS_CHIPS_AMOUNT   , 0))
                                                , SUM(ISNULL(GTSC_CREDITS_CHIPS_AMOUNT , 0))
                                                        , SUM(ISNULL(GTSC_TIPS                 , 0))
                                                        , SUM(ISNULL(GTSC_OWN_SALES_AMOUNT     , 0))
                                                        , SUM(ISNULL(GTSC_EXTERNAL_SALES_AMOUNT, 0))
                                                        , SUM(ISNULL(GTSC_COLLECTED_AMOUNT     , 0))
                                                        , SUM(ISNULL(CASE WHEN GTSC_TYPE = 0            THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                          WHEN GTSC_TYPE = 1            THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                          ELSE 0 END,  0))
                                                        , SUM(ISNULL(CASE WHEN GTSC_TYPE = @_CHIP_RE    THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                          WHEN GTSC_TYPE = @_CHIP_NRE   THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                          WHEN GTSC_TYPE = @_CHIP_COLOR THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                          ELSE 0 END,  0))
                                                        , 0 -- It does not allow takings of dropbox with tickets
                                                        , GT_HAS_INTEGRATED_CASHIER)
                             END AS S_WIN

                     ,  CASE WHEN @_TOTAL_TO_SELECTED_CURRENCY = 0  AND GTSC_TYPE = 0 THEN
                                     SUM(ISNULL(GTS_TIPS, 0))
                             WHEN @_TOTAL_TO_SELECTED_CURRENCY = 1 THEN
                                     SUM(ISNULL(GTSC_TIPS, 0))
                             ELSE 0
                             END                                                                       AS S_TIP
                     , CS.CS_OPENING_DATE                                                              AS OPEN_HOUR
                     , CS.CS_CLOSING_DATE                                                              AS CLOSE_HOUR
                     , DATEDIFF(SECOND, CS_OPENING_DATE, ISNULL(CS_CLOSING_DATE, GETDATE()))           AS SESSION_SECONDS
                     , CASE WHEN @_TOTAL_TO_SELECTED_CURRENCY = 0 
                            THEN ISNULL(GTS_COPY_DEALER_VALIDATED_AMOUNT, 0)
                            WHEN @_TOTAL_TO_SELECTED_CURRENCY = 1 
                            THEN SUM(ISNULL(GTSC_COPY_DEALER_VALIDATED_AMOUNT, 0))
                            END                                                           AS COPY_DEALER_VALIDATED_AMOUNT
                                   FROM   GAMING_TABLES_SESSIONS GTS
                LEFT   JOIN GAMING_TABLES_SESSIONS_BY_CURRENCY GTSC ON GTSC.GTSC_GAMING_TABLE_SESSION_ID = GTS.GTS_GAMING_TABLE_SESSION_ID
                                                                   AND GTSC_ISO_CODE IN (@_SELECTED_CURRENCY)
               INNER   JOIN CASHIER_SESSIONS CS  ON GTS.GTS_CASHIER_SESSION_ID = CS.CS_SESSION_ID
                                                AND CS_OPENING_DATE >= @pDateFrom AND CS_OPENING_DATE < @pDateTo
               INNER   JOIN GAMING_TABLES GT ON GTS.GTS_GAMING_TABLE_ID = GT.GT_GAMING_TABLE_ID
                                            AND GT.GT_TYPE_ID IN (SELECT SST_VALUE FROM @_TYPES_TABLE)
               INNER   JOIN GAMING_TABLES_TYPES GTT ON GT.GT_TYPE_ID = GTT.GTT_GAMING_TABLE_TYPE_ID
               WHERE   CS_STATUS = 1  -- Only closed sessions
               GROUP BY CS_OPENING_DATE, GTT_GAMING_TABLE_TYPE_ID, GT_GAMING_TABLE_ID, GTT_NAME, GT_NAME
                      , GTS_OWN_SALES_AMOUNT, GTS_EXTERNAL_SALES_AMOUNT, GTS_COLLECTED_AMOUNT, GT_HAS_INTEGRATED_CASHIER
                      , GTS_COLLECTED_DROPBOX_AMOUNT, GTS_COLLECTED_DROPBOX_AMOUNT, GTS_DROPBOX_ENABLED
                      , GTS_COLLECTED_DROPBOX_CHIPS_AMOUNT, GT_THEORIC_HOLD, GTS_FINAL_CHIPS_AMOUNT, GTS_INITIAL_CHIPS_AMOUNT
                      , GTS_FILLS_CHIPS_AMOUNT, GTS_CREDITS_CHIPS_AMOUNT, GTSC_TYPE, GTS_TIPS, GTSC_TIPS, CS_CLOSING_DATE
                      , GTS_COPY_DEALER_VALIDATED_AMOUNT
          -- END CORE QUERY
        ) AS X
  GROUP BY   X.TABLE_TYPE, X.TABLE_IDENTIFIER, X.TABLE_TYPE_NAME, X.TABLE_NAME, X.CM_DATE_ONLY  -- Group by indentifier and time interval

IF @_TIME_INTERVAL <> -1
BEGIN
   -- INTERVALS DATES AND TABLE FINAL PREPARATION
   -- FILTER ACTIVITY
   IF @_ONLY_ACTIVITY = 0
    BEGIN
      -- JOIN THE SELECT WITH ALL DATA
      SELECT   DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER
             , DT.TABLE_IDENT_NAME AS TABLE_NAME
             , DT.TABLE_TYPE_IDENT AS TABLE_TYPE
             , DT.TABLE_TYPE_NAME AS TABLE_TYPE_NAME
             , ISNULL(zz.COPY_DEALER_VALIDATED_AMOUNT, 0) AS COPY_DEALER_AMOUNT
             , ISNULL(ZZ.TOTAL_DROP_GAMBLING_TABLE,0) AS TOTAL_DROP_GAMBLING_TABLE
             , ISNULL(ZZ.TOTAL_DROP_CASHIER,0) AS  TOTAL_DROP_CASHIER
             , ISNULL(ZZ.TOTAL_DROP, 0) AS TOTAL_DROP
             , ISNULL(ZZ.TOTAL_WIN, 0) AS TOTAL_WIN
             , ISNULL(ZZ.TOTAL_TIP, 0) AS TOTAL_TIP
             , ISNULL(ZZ.WIN_DROP, 0) AS WIN_DROP
             , ISNULL(ZZ.THEORIC_HOLD, 0) AS THEORIC_HOLD
             , ISNULL(ZZ.TIP_DROP, 0) AS TIP_DROP
             , ISNULL(ZZ.WIN_TIPS, 0) AS WIN_TIPS
             , ZZ.OPEN_HOUR
             , ZZ.CLOSE_HOUR
             , CASE WHEN ZZ.CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED
             , CASE WHEN ZZ.SESSION_SECONDS = 0 THEN DATEDIFF(SECOND,ZZ.OPEN_HOUR,GETDATE()) ELSE ISNULL(ZZ.SESSION_SECONDS, 0) END AS SESSION_SECONDS
             , ISNULL(ZZ.SESSION_SUM, 0) AS SESSION_SUM
             , DATE_TIME              
        FROM   @_DAYS_AND_TABLES DT
   LEFT JOIN   #GT_TEMPORARY_REPORT_DATA ZZ
          ON   (  (@_TIME_INTERVAL = 0 AND DATE_TIME = ZZ.CM_DATE_ONLY)
                           OR (@_TIME_INTERVAL = 1 AND MONTH(DATE_TIME) = MONTH(ZZ.CM_DATE_ONLY) AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
                           OR (@_TIME_INTERVAL = 2 AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
               ) AND TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER
       -- SET ORDER
    ORDER BY   DT.TABLE_TYPE_IDENT ASC
             , CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC
             , DATE_TIME DESC;

    END
   ELSE
    BEGIN
      -- JOIN THE SELECT HIDDING WITHOUT ACTIVITY
      SELECT   DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER
             , DT.TABLE_IDENT_NAME AS TABLE_NAME
             , DT.TABLE_TYPE_IDENT AS TABLE_TYPE
             , DT.TABLE_TYPE_NAME AS TABLE_TYPE_NAME
             , ISNULL(zz.COPY_DEALER_VALIDATED_AMOUNT, 0) AS COPY_DEALER_AMOUNT
             , ISNULL(ZZ.TOTAL_DROP_GAMBLING_TABLE,0) AS TOTAL_DROP_GAMBLING_TABLE 
             , ISNULL(ZZ.TOTAL_DROP_CASHIER,0) AS  TOTAL_DROP_CASHIER
             , ISNULL(ZZ.TOTAL_DROP, 0) AS TOTAL_DROP 
             , ISNULL(ZZ.TOTAL_WIN, 0) AS TOTAL_WIN
             , ISNULL(ZZ.TOTAL_TIP, 0) AS TOTAL_TIP
             , ISNULL(ZZ.WIN_DROP, 0) AS WIN_DROP
             , ISNULL(ZZ.THEORIC_HOLD, 0) AS THEORIC_HOLD
             , ISNULL(ZZ.TIP_DROP, 0) AS TIP_DROP
             , ISNULL(ZZ.WIN_TIPS, 0) AS WIN_TIPS
             , ZZ.OPEN_HOUR
             , ZZ.CLOSE_HOUR
             , CASE WHEN ZZ.CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED
             , CASE WHEN ZZ.SESSION_SECONDS = 0 THEN DATEDIFF(SECOND,ZZ.OPEN_HOUR,GETDATE()) ELSE ISNULL(ZZ.SESSION_SECONDS, 0) END AS SESSION_SECONDS
             , ISNULL(ZZ.SESSION_SUM, 0) AS SESSION_SUM
             , DATE_TIME
        FROM   @_DAYS_AND_TABLES DT
  INNER JOIN   #GT_TEMPORARY_REPORT_DATA ZZ
          ON   (  (@_TIME_INTERVAL = 0 AND DATE_TIME = ZZ.CM_DATE_ONLY)
                           OR (@_TIME_INTERVAL = 1 AND MONTH(DATE_TIME) = MONTH(ZZ.CM_DATE_ONLY) AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
                           OR (@_TIME_INTERVAL = 2 AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
               ) AND TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER
       -- SET ORDER
    ORDER BY   DT.TABLE_TYPE_IDENT ASC
             , CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC
             , DATE_TIME DESC;
    END -- IF ONLY_ACTIVITY
END
ELSE  -- ELSE WITHOUT INTERVALS
BEGIN
   -- FINAL WITHOUT INTERVALS
      -- FILTER ACTIVITY
   IF @_ONLY_ACTIVITY = 0
    BEGIN
            -- JOIN DATA INCLUDING WITHOUT ACTIVITY
      SELECT   DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER
             , DT.TABLE_IDENT_NAME AS TABLE_NAME
             , DT.TABLE_TYPE_IDENT AS TABLE_TYPE
             , DT.TABLE_TYPE_NAME
             , ISNULL(zz.COPY_DEALER_VALIDATED_AMOUNT, 0) AS COPY_DEALER_AMOUNT
             , ISNULL(ZZ.TOTAL_DROP_GAMBLING_TABLE,0) AS TOTAL_DROP_GAMBLING_TABLE
             , ISNULL(ZZ.TOTAL_DROP_CASHIER,0) AS  TOTAL_DROP_CASHIER
             , ISNULL(TOTAL_DROP, 0) AS TOTAL_DROP
             , ISNULL(TOTAL_WIN, 0) AS TOTAL_WIN
             , ISNULL(TOTAL_TIP, 0) AS TOTAL_TIP
             , ISNULL(WIN_DROP, 0) AS WIN_DROP
             , ISNULL(ZZ.THEORIC_HOLD, 0) AS THEORIC_HOLD
             , ISNULL(TIP_DROP, 0) AS TIP_DROP
             , ISNULL(WIN_TIPS, 0) AS WIN_TIPS
             , OPEN_HOUR
             , CLOSE_HOUR
             , CASE WHEN CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED
             , CASE WHEN SESSION_SECONDS = 0 THEN DATEDIFF(SECOND, OPEN_HOUR, GETDATE()) ELSE ISNULL(SESSION_SECONDS, 0) END AS SESSION_SECONDS
             , ISNULL(SESSION_SUM, 0) AS SESSION_SUM
              FROM   @_DAYS_AND_TABLES DT
   LEFT JOIN   #GT_TEMPORARY_REPORT_DATA ZZ ON DT.TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER
    ORDER BY   DT.TABLE_TYPE_IDENT ASC
             , CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC
             , DATE_TIME DESC;

     END
    ELSE
     BEGIN
            -- JOIN DATA WITH ONLY ACTIVITY
       SELECT   DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER
              , DT.TABLE_IDENT_NAME AS TABLE_NAME
              , DT.TABLE_TYPE_IDENT AS TABLE_TYPE
              , DT.TABLE_TYPE_NAME
              , ISNULL(zz.COPY_DEALER_VALIDATED_AMOUNT, 0) AS COPY_DEALER_AMOUNT
              , ISNULL(ZZ.TOTAL_DROP_GAMBLING_TABLE,0) AS TOTAL_DROP_GAMBLING_TABLE
              , ISNULL(ZZ.TOTAL_DROP_CASHIER,0) AS  TOTAL_DROP_CASHIER
              , ISNULL(TOTAL_DROP, 0) AS TOTAL_DROP
              , ISNULL(TOTAL_WIN, 0) AS TOTAL_WIN
              , ISNULL(TOTAL_TIP, 0) AS TOTAL_TIP
              , ISNULL(WIN_DROP, 0) AS WIN_DROP
              , ISNULL(ZZ.THEORIC_HOLD, 0) AS THEORIC_HOLD
              , ISNULL(TIP_DROP, 0) AS TIP_DROP
              , ISNULL(WIN_TIPS, 0) AS WIN_TIPS
              , OPEN_HOUR
              , CLOSE_HOUR
              , CASE WHEN CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED
              , CASE WHEN SESSION_SECONDS = 0 THEN DATEDIFF(SECOND, OPEN_HOUR, GETDATE()) ELSE ISNULL(SESSION_SECONDS, 0) END AS SESSION_SECONDS
              , ISNULL(SESSION_SUM, 0) AS SESSION_SUM
              FROM   @_DAYS_AND_TABLES DT
   INNER JOIN   #GT_TEMPORARY_REPORT_DATA ZZ ON DT.TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER
     ORDER BY   DT.TABLE_TYPE_IDENT ASC
              , CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC
              , DATE_TIME DESC;
     END
 END

-- ERASE THE TEMPORARY DATA
DROP TABLE #GT_TEMPORARY_REPORT_DATA

END -- END PROCEDURE
GO
-- PERMISSIONS
GRANT EXECUTE ON [dbo].[GT_Base_Report_Data] TO [wggui] WITH GRANT OPTION
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ScoreReport]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[ScoreReport]
GO

CREATE  PROCEDURE [dbo].[ScoreReport]
(
    @pDateFrom        AS DATETIME
  , @pCageFillerIn    AS INT
  , @pCageFillerOut   AS INT
  , @pChipRE          AS INT
  , @pSaleChips       AS INT
)
AS
BEGIN
  DECLARE @old_time AS DATETIME
  DECLARE @DateTo   AS DATETIME
  DECLARE @Horas    AS NVARCHAR(1000)
  
  DECLARE @cols  AS NVARCHAR(MAX)
  DECLARE @query AS NVARCHAR(MAX)

  SET @old_time = '2007-01-01T07:00:00'
  SET @DateTo = DATEADD(DAY, 1, @pDateFrom)  
  SET @Horas = N'[0],[1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12],[13],[14],[15],[16],[17],[18],[19],[20],[21],[22],[23],[9999]'
  
  SELECT * 
    INTO #GROUPS_BY_SESSION 
    FROM SplitStringIntoTable(REPLACE(REPLACE(@Horas, '[', ''), ']', ''), ',', 1)

      SELECT   GTT_NAME                             AS 'TYPE_NAME'
             , GT_NAME                              AS 'TABLE_NAME'
             , CS_OPENING_DATE                      AS 'OPENING_DATE'
             , CS_CLOSING_DATE                      AS 'CLOSING_DATE'
             , GTS_GAMING_TABLE_SESSION_ID          AS 'GAMING_TABLE_SESSION_ID'
             , GTS_CASHIER_SESSION_ID               AS 'CASHIER_SESSION_ID'
             , CS_NAME                              AS 'SESION_NAME'
             , ISNULL(GTSC_INITIAL_CHIPS_AMOUNT, 0) AS 'INITIAL_BALANCE'
             , ISNULL(GTSC_FINAL_CHIPS_AMOUNT, 0)   AS 'FINAL_BALANCE'
             , CAST(GT_NUM_SEATS AS DECIMAL(18,2))  AS 'NUM_SEATS_IN_TABLE'
             , CAST(SST_VALUE AS INT)               AS 'HORA'
             , SST_VALUE                            AS 'HORA_STR'
             , CAST(0 AS DECIMAL(18,2))             AS 'GT_DROP'
             , CAST(0 AS DECIMAL(18,2))             AS 'GT_WIN_LOSS'
             , CAST(0 AS DECIMAL(18,2))             AS 'GT_HOLD'
             , CAST(0 AS DECIMAL(18,2))             AS 'GT_OCCUPATION_NUMBER'
        INTO   #VALUE_TABLE
        FROM   GAMING_TABLES
  INNER JOIN   GAMING_TABLES_TYPES                ON GT_TYPE_ID                    = GTT_GAMING_TABLE_TYPE_ID
  INNER JOIN   GAMING_TABLES_SESSIONS             ON GT_GAMING_TABLE_ID            = GTS_GAMING_TABLE_ID
   LEFT JOIN   GAMING_TABLES_SESSIONS_BY_CURRENCY ON GTSC_GAMING_TABLE_SESSION_ID  = GTS_GAMING_TABLE_SESSION_ID AND GTSC_TYPE = @pChipRE
  INNER JOIN   CASHIER_SESSIONS                   ON GTS_CASHIER_SESSION_ID        = CS_SESSION_ID 
 CROSS APPLY   #GROUPS_BY_SESSION
       WHERE   CS_OPENING_DATE >= @pDateFrom
         AND   CS_OPENING_DATE <  @DateTo
    ORDER BY   GTT_NAME                 
             , GT_NAME                  
             , CS_OPENING_DATE          
             , CAST(SST_VALUE AS INT)

     --  SET DROP BY HOUR - TICKETS  
      UPDATE   #VALUE_TABLE
         SET   GT_DROP = GT_DROP + ISNULL((SELECT   SUM(TI_AMOUNT)
                                             FROM   GT_COPY_DEALER_VALIDATED 
                                       INNER JOIN   TICKETS  ON GTCD_TICKET_ID = TI_TICKET_ID
                                            WHERE   GAMING_TABLE_SESSION_ID = GTCD_GAMING_TABLE_SESSION_ID 
                                              AND   DATEADD(HOUR, DATEDIFF(HOUR, @old_time, GTCD_VALIDATION_DATETIME), @old_time) = DATEADD(HOUR, HORA,  @pDateFrom)), 0)
 
     --  SET DROP BY HOUR - AMOUNTS  
      UPDATE   #VALUE_TABLE
         SET   GT_DROP = GT_DROP + ISNULL((SELECT   SUM(GTPM_VALUE)
                                             FROM   GT_PLAYERTRACKING_MOVEMENTS 
                                            WHERE   GAMING_TABLE_SESSION_ID = GTPM_GAMING_TABLE_SESSION_ID 
                                              AND   DATEADD(HOUR, DATEDIFF(HOUR, @old_time, GTPM_DATETIME), @old_time) = DATEADD(HOUR, HORA,  @pDateFrom)
                                              AND   GTPM_TYPE = @pSaleChips), 0)

      --  SET WIN/LOSS WITH INITIAL BALANCE 
      UPDATE   #VALUE_TABLE
         SET   GT_WIN_LOSS = INITIAL_BALANCE
       WHERE   DATEADD(HOUR, DATEDIFF(HOUR, @old_time, OPENING_DATE), @old_time) <= DATEADD(HOUR, HORA,  @pDateFrom)
         AND   DATEADD(HOUR, DATEDIFF(HOUR, @old_time, CLOSING_DATE), @old_time) >= DATEADD(HOUR, HORA,  @pDateFrom)
          OR   HORA = 9999

      --  ADD SCORE BY HOUR TO WIN/LOSS
      UPDATE   #VALUE_TABLE
         SET   GT_WIN_LOSS = GT_WIN_LOSS - ISNULL((SELECT   GTWL_WIN_LOSS_AMOUNT
                                                     FROM   GAMING_TABLES_WIN_LOSS 
                                                    WHERE   GAMING_TABLE_SESSION_ID = GTWL_GAMING_TABLE_SESSION_ID 
                                                      AND ( DATEADD(HOUR, DATEDIFF(HOUR, @old_time, GTWL_DATETIME_HOUR), @old_time) = DATEADD(HOUR, HORA,  @pDateFrom)
                                                       OR   HORA = 9999 
                                                      AND   GTWL_DATETIME_HOUR = (SELECT  MAX(GTWL_DATETIME_HOUR) 
                                                                                    FROM  GAMING_TABLES_WIN_LOSS 
                                                                                   WHERE  GAMING_TABLE_SESSION_ID = GTWL_GAMING_TABLE_SESSION_ID 
                                                                                     AND  GTWL_DATETIME_HOUR < ISNULL(CLOSING_DATE, GETDATE())))), 0)

      --  ADD FILLS-CREDITS CHIPS BY HOUR TO WIN/LOSS
      UPDATE   #VALUE_TABLE
         SET   GT_WIN_LOSS = GT_WIN_LOSS + ISNULL(CREDITS_SUB_FILLS, 0) + INITIAL_BALANCE   --- Add Initial balance for remove Initial movement
        FROM   (      SELECT   CM_SESSION_ID
                             , SUM(CM_SUB_AMOUNT - CM_ADD_AMOUNT) AS CREDITS_SUB_FILLS
                             , DATEADD(HOUR, DATEDIFF(HOUR, @old_time, CM_DATE), @old_time) AS HORA_CM
                        FROM   #VALUE_TABLE
                  INNER JOIN   CASHIER_MOVEMENTS AS CM ON CASHIER_SESSION_ID = CM_SESSION_ID
                       WHERE   DATEDIFF(HOUR,  DATEADD(HOUR, DATEDIFF(HOUR, @old_time, CM_DATE), @old_time), DATEADD(HOUR, HORA,  @pDateFrom)) = 0
                         AND   CM_TYPE IN (@pCageFillerIn, @pCageFillerOut)
                         AND   CM_CAGE_CURRENCY_TYPE = @pChipRE
                    GROUP BY   CM_SESSION_ID
                             , DATEADD(HOUR, DATEDIFF(HOUR, @old_time, CM_DATE), @old_time)) AS A
       WHERE   CASHIER_SESSION_ID = CM_SESSION_ID
         AND   (DATEDIFF(HOUR,  HORA_CM, DATEADD(HOUR, HORA,  @pDateFrom)) >= 0
          OR   HORA = 9999 )

      --  SET HOLD BY HOUR    
      UPDATE   #VALUE_TABLE
         SET   GT_HOLD = A.GT_WIN_LOSS / A.GT_DROP * 100
        FROM   #VALUE_TABLE AS A
       WHERE   GAMING_TABLE_SESSION_ID = A.GAMING_TABLE_SESSION_ID
         AND   HORA = A.HORA
         AND   A.GT_DROP <> 0
 
      --  SET OCCUPATION BY HOUR    
      UPDATE   #VALUE_TABLE
         SET   GT_OCCUPATION_NUMBER = (SELECT   CAST(COUNT(DISTINCT GTPS_SEAT_ID) AS DECIMAL(18,2))
                                  FROM   GT_PLAY_SESSIONS 
                                 WHERE   GAMING_TABLE_SESSION_ID = GTPS_GAMING_TABLE_SESSION_ID 
                                   AND ( GTPS_STARTED <= DATEADD(HOUR, HORA,  @pDateFrom) 
                                   AND   ISNULL(GTPS_FINISHED, GETDATE()) > DATEADD(HOUR, HORA,  @pDateFrom)
                                    OR   GTPS_STARTED >= DATEADD(HOUR, HORA,  @pDateFrom)
                                   AND   GTPS_STARTED < DATEADD(HOUR, 1, DATEADD(HOUR, HORA,  @pDateFrom))))
       WHERE  NUM_SEATS_IN_TABLE <> 0
 
      --  SET OCCUPATION BY HOUR -- TOTAL --
      UPDATE   #VALUE_TABLE
         SET   GT_OCCUPATION_NUMBER = ISNULL((SELECT   SUM(A.GT_OCCUPATION_NUMBER) / CAST(SUM(CASE WHEN A.GT_OCCUPATION_NUMBER > 0 THEN 1 ELSE 0 END)  AS DECIMAL(18,2))
                                  FROM   #VALUE_TABLE AS A
                                 WHERE   #VALUE_TABLE.GAMING_TABLE_SESSION_ID = A.GAMING_TABLE_SESSION_ID 
                                   AND   A.GT_OCCUPATION_NUMBER > 0), 0)
       WHERE  HORA = 9999

 --PIVOT COLUMS FOR REPORT
      SELECT   @cols = STUFF((SELECT ',' + QUOTENAME( CASE WHEN HORA < 25 
                                                           THEN REPLACE(LEFT(RIGHT(CONVERT(NVARCHAR(20), DATEADD(HOUR, HORA, @pDateFrom), 120), 8), 5), ':', '') 
                                                           ELSE '9999'
                                                           END   +'-'+COL) 
        FROM   #VALUE_TABLE t
 CROSS APPLY ( SELECT 'GT_DROP', 1 UNION ALL
               SELECT 'GT_WIN_LOSS', 2 UNION ALL
               SELECT 'GT_HOLD', 3 UNION ALL
               SELECT 'GT_OCCUPATION_NUMBER', 4
             ) C (COL, SO)
    GROUP BY   COL
             , SO
             , HORA
    ORDER BY   HORA
             , SO FOR XML PATH('')
             , TYPE ).value('.', 'NVARCHAR(MAX)'),1,1,'')


set @query = 'SELECT   TYPE_NAME
                     , TABLE_NAME
                     , OPENING_DATE
                     , CLOSING_DATE
                     , GAMING_TABLE_SESSION_ID
                     , SESION_NAME
                     , INITIAL_BALANCE
                     , FINAL_BALANCE
                     , NUM_SEATS_IN_TABLE
                     , ' + @cols + ' 
                FROM ( SELECT   TYPE_NAME
                              , TABLE_NAME
                              , OPENING_DATE
                              , CLOSING_DATE
                              , GAMING_TABLE_SESSION_ID
                              , SESION_NAME
                              , INITIAL_BALANCE
                              , FINAL_BALANCE
                              , NUM_SEATS_IN_TABLE
                              , COL = CASE WHEN HORA < 25 
                                           THEN REPLACE( LEFT(  RIGHT(CONVERT(NVARCHAR(20), DATEADD(HOUR, HORA, ''' + CONVERT(NVARCHAR(20), @pDateFrom, 120) + '''), 120), 8), 5), '':'', '''' )
                                           ELSE ''9999'' 
                                           END  +''-''+COL
                              , VALUE
                         FROM   #VALUE_TABLE T
                  CROSS APPLY ( SELECT   ''GT_DROP''
                                       , GT_DROP UNION ALL
                                SELECT   ''GT_WIN_LOSS''
                                       , GT_WIN_LOSS UNION ALL
                                SELECT   ''GT_HOLD''
                                       , GT_HOLD UNION ALL
                                SELECT   ''GT_OCCUPATION_NUMBER''
                                       , GT_OCCUPATION_NUMBER
                              ) C (COL, VALUE)
                     ) X
               PIVOT ( MAX(VALUE) FOR COL IN (' + @cols + ') ) PVT '


  EXECUTE SP_EXECUTESQL @query

  DROP TABLE #GROUPS_BY_SESSION
  
END --ScoreReport
GO

GRANT EXECUTE ON [dbo].[ScoreReport] TO [wggui] WITH GRANT OPTION
GO
