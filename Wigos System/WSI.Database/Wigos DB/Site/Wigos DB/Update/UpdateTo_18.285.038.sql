/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 284;

SET @New_ReleaseId = 285;
SET @New_ScriptName = N'UpdateTo_18.285.038.sql';
SET @New_Description = N'Regionalization for dominican rep./trinidad and tobago;';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/******* TABLES *******/

-- TABLE ACCOUNT_POINTS_EXPIRED_LIST
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[account_points_expired_list]') AND type in (N'U'))
DROP TABLE account_points_expired_list
GO

CREATE TABLE [dbo].account_points_expired_list(
	[apel_account_id]        [bigint]   NOT NULL,
	[apel_points_to_expire]  [money]    NOT NULL,
	[apel_datetime]          [datetime] NOT NULL
 CONSTRAINT [PK_account_points_expired_list] PRIMARY KEY CLUSTERED 
(
	[apel_account_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO   

-- TABLE ACCOUNT_POINTS_EXPIRED_CONTROL
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[account_points_expired_control]') AND type in (N'U'))
DROP TABLE account_points_expired_control
GO

CREATE TABLE [dbo].account_points_expired_control(
	[apec_day_month]  nvarchar(5) NOT NULL,
	[apec_year]      [int]         NOT NULL,
	[apec_execution] [datetime]    NULL
 CONSTRAINT [PK_account_points_expired_control] PRIMARY KEY CLUSTERED 
(
	[apec_day_month],[apec_year] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO  

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[monitor_data]') AND type in (N'U'))
DROP TABLE monitor_data
GO

CREATE TABLE [dbo].[monitor_data](
	[md_unique_id] [bigint] NOT NULL IDENTITY (1, 1),
	[md_datetime] Datetime NOT NULL DEFAULT GETDATE(),	
	[md_type] [int] NOT NULL,	
	[md_data] [xml] NOT NULL,	
	[md_schema] [xml] NOT NULL,	
 CONSTRAINT [PK_monitor_data] PRIMARY KEY CLUSTERED 
(
	[md_unique_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

-- TABLE GUI_FILTERS
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[gui_filters]') AND type in (N'U'))
DROP TABLE gui_filters
GO

CREATE TABLE [dbo].[gui_filters](
	[gf_user_id] [int] NOT NULL,
	[gf_form_id] [int] NOT NULL,
	[gf_filter] [xml] NOT NULL,
 CONSTRAINT [PK_gui_filters] PRIMARY KEY CLUSTERED 
(
	[gf_user_id] ASC,
	[gf_form_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

IF  NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wxp_002_messages]') AND type in (N'U'))
BEGIN
   CREATE TABLE [dbo].[wxp_002_messages](         
         [wxm_id] [bigint] IDENTITY(1,1) NOT NULL,
         [wxm_terminal_id] [int] NOT NULL,
         [wxm_datetime] [datetime] NOT NULL,
         [wxm_nuc] [nvarchar](50) NULL,
         [wxm_nuid] [nvarchar](40) NULL,
         [wxm_serial] [nvarchar](50) NULL,
         [wxm_event_id] [int] NOT NULL,
         [wxm_coin_in] [money] NOT NULL,
         [wxm_coin_out] [money] NOT NULL,
         [wxm_jackpot] [money] NOT NULL,
         [wxm_hand_paid] [money] NOT NULL,
         [wxm_bill_in] [money] NOT NULL,
         [wxm_games_played] [bigint] NOT NULL,
         [wxm_payout] [money] NOT NULL
   CONSTRAINT [PK_wxp_002_messages] PRIMARY KEY CLUSTERED 
   (
         [wxm_id] ASC
   )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
   ) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[operation_voucher_parameters]') AND type in (N'U'))
BEGIN
  CREATE TABLE operation_voucher_parameters(
	  ovp_operation_code int NOT NULL,
	  ovp_enabled bit NOT NULL,
	  ovp_generate bit  NOT NULL,
	  ovp_reprint bit NOT NULL,
	  ovp_print_copy int NOT NULL,
   CONSTRAINT PK_operation_voucher_parameters PRIMARY KEY CLUSTERED 
  (
	  ovp_operation_code ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
  ) ON [PRIMARY]

END
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[terminals]') and name = 'te_brand_code')
ALTER TABLE [dbo].[terminals] 
	ADD [te_brand_code] nvarchar(50) NULL,
	    [te_model] nvarchar(50) NULL,
	    [te_manufacture_year] int NULL,
	    [te_met_homologated] bit NULL,
	    [te_bet_code] nvarchar(50) NULL;
ELSE
SELECT '***** Field terminals for Coljuegos already exists *****';

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[alarms]') and name = 'al_technician_id')
  ALTER TABLE dbo.alarms ADD al_technician_id INT NULL 
GO

ALTER TABLE dbo.cashier_sessions ADD cs_short_over_history bit NOT NULL CONSTRAINT DF_cashier_sessions_cs_short_over_history DEFAULT ((0))
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_cashier_sessions_cs_short_over_history]') AND type = 'D')
  ALTER TABLE [dbo].[cashier_sessions] DROP CONSTRAINT [DF_cashier_sessions_cs_short_over_history]
GO

ALTER TABLE [dbo].[cashier_sessions] ADD CONSTRAINT [DF_cashier_sessions_cs_short_over_history]  DEFAULT ((1)) FOR [cs_short_over_history]
GO

/******* INDEXES *******/

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[cashier_sessions]') AND name = N'IX_cs_short_over_history')
  DROP INDEX [IX_cs_short_over_history] ON [dbo].[cashier_sessions] WITH ( ONLINE = OFF )
GO
CREATE NONCLUSTERED INDEX [IX_cs_short_over_history] ON [dbo].[cashier_sessions] 
(
	[cs_short_over_history] ASC, 
	[cs_closing_date] DESC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[wxp_002_messages]') AND name = N'IX_wx2m_datetime')
  DROP INDEX [IX_wx2m_datetime] ON [dbo].[wxp_002_messages] WITH ( ONLINE = OFF )
GO
CREATE NONCLUSTERED INDEX [IX_wx2m_datetime] ON [dbo].[wxp_002_messages] 
(
      [wxm_datetime] DESC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

/******* RECORDS *******/

IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='SASHost' AND GP_SUBJECT_KEY = 'TechnicianTimeout')
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('SasHost', 'TechnicianTimeout', '1')
GO 

IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='EGM' AND GP_SUBJECT_KEY = 'ExternalMachine.Enabled')
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('EGM', 'ExternalMachine.Enabled', '0')
GO

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Account' AND GP_SUBJECT_KEY ='Level00.MinAllowedCashIn.Amount')
   INSERT INTO general_params (gp_group_key, gp_subject_key, gp_key_value) VALUES ('Account', 'Level00.MinAllowedCashIn.Amount', '0');
GO

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.Voucher' AND GP_SUBJECT_KEY ='Concept.In.Footer')
   INSERT INTO general_params (gp_group_key, gp_subject_key, gp_key_value) VALUES ('Cashier.Voucher', 'Concept.In.Footer', '');

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.Voucher' AND GP_SUBJECT_KEY ='Concept.Out.Footer')
   INSERT INTO general_params (gp_group_key, gp_subject_key, gp_key_value) VALUES ('Cashier.Voucher', 'Concept.Out.Footer', '');

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.Voucher' AND GP_SUBJECT_KEY ='Concept.Comments.Enabled')
   INSERT INTO general_params (gp_group_key, gp_subject_key, gp_key_value) VALUES ('Cashier.Voucher', 'Concept.Comments.Enabled', '0');
GO

-- ADD NEW GENERAL PARAMS
IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='PlayerTracking' AND GP_SUBJECT_KEY = 'PointsExpirationDayMonth')
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('PlayerTracking', 'PointsExpirationDayMonth', '')
GO   

DECLARE @printCopy int;

SET @printCopy = (SELECT gp_key_value FROM general_params WHERE gp_group_key = 'Cashier.Voucher' AND gp_subject_key = 'PrintCopy') + 1

IF @printCopy IS NULL
 SET @printCopy = 1
 
-- OPERATION CODE: NOT_SET = 0
IF NOT EXISTS (SELECT * FROM operation_voucher_parameters WHERE ovp_operation_code = 0)
BEGIN
  INSERT INTO   operation_voucher_parameters
              ( ovp_operation_code, ovp_enabled, ovp_generate, ovp_reprint, ovp_print_copy)
       VALUES
              (                 0,            1,           1,            1,     @printCopy)
END

 
-- OPERATION CODE: CASH_IN = 1
IF NOT EXISTS (SELECT * FROM operation_voucher_parameters WHERE ovp_operation_code = 1)
BEGIN
  INSERT INTO   operation_voucher_parameters
              ( ovp_operation_code, ovp_enabled, ovp_generate, ovp_reprint, ovp_print_copy)
       VALUES
              (                 1,            1,           1,            1,     @printCopy)
END

-- OPERATION CODE: PROMOTION = 3
IF NOT EXISTS (SELECT * FROM operation_voucher_parameters WHERE ovp_operation_code = 3)
BEGIN
  INSERT INTO   operation_voucher_parameters
              ( ovp_operation_code, ovp_enabled, ovp_generate, ovp_reprint, ovp_print_copy)
       VALUES
              (                 3,            1,           1,            1,     @printCopy)
END

-- OPERATION CODE: CASH_OUT = 2
IF NOT EXISTS (SELECT * FROM operation_voucher_parameters WHERE ovp_operation_code = 2)
BEGIN
  INSERT INTO   operation_voucher_parameters
              ( ovp_operation_code, ovp_enabled, ovp_generate, ovp_reprint, ovp_print_copy)
       VALUES
              (                 2,            1,           1,            1,     @printCopy)
END

-- OPERATION CODE: HANDPAY = 113
IF NOT EXISTS (SELECT * FROM operation_voucher_parameters WHERE ovp_operation_code = 113)
BEGIN
  INSERT INTO   operation_voucher_parameters
              ( ovp_operation_code, ovp_enabled, ovp_generate, ovp_reprint, ovp_print_copy)
       VALUES
              (               113,            1,           1,            1,     @printCopy)
END

-- OPERATION CODE: HANDPAY_CANCELLATION = 114
IF NOT EXISTS (SELECT * FROM operation_voucher_parameters WHERE ovp_operation_code = 114)
BEGIN
  INSERT INTO   operation_voucher_parameters
              ( ovp_operation_code, ovp_enabled, ovp_generate, ovp_reprint, ovp_print_copy)
       VALUES
              (               114,            1,           1,            1,     @printCopy)
END
              
-- OPERATION CODE: HANDPAY_VALIDATION = 115
IF NOT EXISTS (SELECT * FROM operation_voucher_parameters WHERE ovp_operation_code = 115)
BEGIN
  INSERT INTO   operation_voucher_parameters
              ( ovp_operation_code, ovp_enabled, ovp_generate, ovp_reprint, ovp_print_copy)
       VALUES
              (               115,            1,           1,            1,     @printCopy)
END
GO

-- DELETE GENERAL PARAM 'Cashier.Voucher - PrintCopy'
IF EXISTS (SELECT * FROM general_params WHERE gp_group_key = 'Cashier.Voucher' and gp_subject_key = 'PrintCopy')
BEGIN
 DELETE FROM general_params WHERE gp_group_key = 'Cashier.Voucher' and gp_subject_key = 'PrintCopy'
END
GO

--  General Params for card pay
IF NOT EXISTS (SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.PlayerCard' AND GP_SUBJECT_KEY = 'AmountToCompanyB')
      INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY, GP_KEY_VALUE ) VALUES ('Cashier.PlayerCard','AmountToCompanyB', 0)
GO

IF NOT EXISTS (SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.PlayerCard' AND GP_SUBJECT_KEY = 'ConceptName')
      INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Cashier.PlayerCard','ConceptName', 'Tarjeta de jugador')
GO

IF NOT EXISTS (SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.Recharge' AND GP_SUBJECT_KEY = 'ShowCurrencyExchange')
      INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY, GP_KEY_VALUE ) VALUES ('Cashier.Recharge','ShowCurrencyExchange', 0)
GO

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Interface.Coljuegos' AND GP_SUBJECT_KEY = 'Enabled')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Interface.Coljuegos', 'Enabled', '0')
GO

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Interface.Coljuegos' AND GP_SUBJECT_KEY = 'ReportTime')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Interface.Coljuegos', 'ReportTime', '00:00')
GO

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Interface.Coljuegos' AND GP_SUBJECT_KEY = 'Operador.NIT')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Interface.Coljuegos', 'Operador.NIT', '')
GO
IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Interface.Coljuegos' AND GP_SUBJECT_KEY = 'Operador.Contrato.Numero')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Interface.Coljuegos', 'Operador.Contrato.Numero', '')
GO
IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Interface.Coljuegos' AND GP_SUBJECT_KEY = 'Operador.Nombre')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Interface.Coljuegos', 'Operador.Nombre', '')
GO
IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Interface.Coljuegos' AND GP_SUBJECT_KEY = 'Operador.Direccion')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Interface.Coljuegos', 'Operador.Direccion', '')
GO
IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Interface.Coljuegos' AND GP_SUBJECT_KEY = 'Operador.Municipio.CodigoDANE')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Interface.Coljuegos', 'Operador.Municipio.CodigoDANE', '')
GO
IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Interface.Coljuegos' AND GP_SUBJECT_KEY = 'Operador.Contrato.FechaInicio')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Interface.Coljuegos', 'Operador.Contrato.FechaInicio', '')
GO
IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Interface.Coljuegos' AND GP_SUBJECT_KEY = 'Operador.Contrato.FechaFinal')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Interface.Coljuegos', 'Operador.Contrato.FechaFinal', '')
GO
IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Interface.Coljuegos' AND GP_SUBJECT_KEY = 'Establecimiento.Codigo')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Interface.Coljuegos', 'Establecimiento.Codigo', '')
GO
IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Interface.Coljuegos' AND GP_SUBJECT_KEY = 'Establecimiento.Nombre')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Interface.Coljuegos', 'Establecimiento.Nombre', '')
GO
IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Interface.Coljuegos' AND GP_SUBJECT_KEY = 'Establecimiento.Direccion')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Interface.Coljuegos', 'Establecimiento.Direccion', '')
GO
IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Interface.Coljuegos' AND GP_SUBJECT_KEY = 'Establecimiento.Municipio.CodigoDANE')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Interface.Coljuegos', 'Establecimiento.Municipio.CodigoDANE', '')
GO
IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Interface.Coljuegos' AND GP_SUBJECT_KEY = 'Establecimiento.Codigo')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Interface.Coljuegos', 'Establecimiento.Codigo', '')
GO
IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Interface.Coljuegos' AND GP_SUBJECT_KEY = 'Fabricante.Certificacion.Numero')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Interface.Coljuegos', 'Fabricante.Certificacion.Numero', '')
GO
IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Interface.Coljuegos' AND GP_SUBJECT_KEY = 'Fabricante.Certificacion.Fecha')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Interface.Coljuegos', 'Fabricante.Certificacion.Fecha', '')
GO
IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Interface.Coljuegos' AND GP_SUBJECT_KEY = 'Fabricante.Certificacion.Laboratorio')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Interface.Coljuegos', 'Fabricante.Certificacion.Laboratorio', '')
GO
IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Interface.Coljuegos' AND GP_SUBJECT_KEY = 'Fabricante.Nombre')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Interface.Coljuegos', 'Fabricante.Nombre', '')
GO
IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Interface.Coljuegos' AND GP_SUBJECT_KEY = 'Fabricante.Software.Version')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Interface.Coljuegos', 'Fabricante.Software.Version', '')
GO
IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Interface.Coljuegos' AND GP_SUBJECT_KEY = 'Fabricante.Clave')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Interface.Coljuegos', 'Fabricante.Clave', '')
GO

IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='SasHost' AND GP_SUBJECT_KEY = 'TechnicianTimeout')
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('SasHost', 'TechnicianTimeout', '1')
GO 

-- CREATE GENERAL PARAM 'Cashier.Voucher - Generate'
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.Voucher' AND GP_SUBJECT_KEY = 'Generate')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('Cashier.Voucher','Generate','1');
GO

-- CREATE GENERAL PARAM 'Cashier.Voucher - Reprint'
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.Voucher' AND GP_SUBJECT_KEY = 'Reprint')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('Cashier.Voucher','Reprint','1');
GO

IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='PSAClient' AND GP_SUBJECT_KEY = 'LastDayAsNextMonth')
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('PSAClient', 'LastDayAsNextMonth', '0')
GO

UPDATE GENERAL_PARAMS
   SET GP_KEY_VALUE = 'Nombre de quien entrega el arqueo
# de C�dula
Firma



Nombre de quien recibe el arqueo
# de C�dula
Firma'
 WHERE GP_SUBJECT_KEY = 'CashCageCount.Signatures.Text'
   AND GP_GROUP_KEY = 'Cage'
GO

-- Block
IF NOT EXISTS (SELECT [alcg_alarm_code] from [dbo].[alarm_catalog] WHERE [alcg_alarm_code] = 393266 AND [alcg_language_id] = 10)
BEGIN
  INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) 
    VALUES ( 393266, 10, 0, N'Terminal de juego bloqueado', N'Terminal de juego bloqueado', 1)
END
GO

IF NOT EXISTS (SELECT [alcg_alarm_code] from [dbo].[alarm_catalog] WHERE [alcg_alarm_code] = 393266 AND [alcg_language_id] = 9)
BEGIN
  INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) 
    VALUES ( 393266,  9, 0, N'Gaming terminal blocked', N'Gaming terminal blocked', 1)
END 
GO

IF NOT EXISTS (SELECT [alcc_alarm_code] from [dbo].[alarm_catalog_per_category] WHERE [alcc_alarm_code] = 393266 AND [alcc_category] = 18) 
BEGIN
  INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) 
    VALUES ( 393266, 18, 0, GETDATE() )
END
GO

-- Unblock
IF NOT EXISTS (SELECT [alcg_alarm_code] from [dbo].[alarm_catalog] WHERE [alcg_alarm_code] = 393267 AND [alcg_language_id] = 10)
BEGIN
  INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) 
    VALUES ( 393267, 10, 0, N'Terminal de juego desbloqueado', N'Terminal de juego desbloqueado', 1)
END
GO

IF NOT EXISTS (SELECT [alcg_alarm_code] from [dbo].[alarm_catalog] WHERE [alcg_alarm_code] = 393267 AND [alcg_language_id] = 9)
BEGIN
  INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) 
    VALUES ( 393267,  9, 0, N'Gaming terminal unblocked', N'Gaming terminal unblocked', 1)
END 
GO

IF NOT EXISTS (SELECT [alcc_alarm_code] from [dbo].[alarm_catalog_per_category] WHERE [alcc_alarm_code] = 393267 AND [alcc_category] = 18) 
BEGIN
  INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) 
    VALUES ( 393267, 18, 0, GETDATE() )
END
GO

IF NOT EXISTS ( SELECT * FROM [dbo].[terminal_types] WHERE [tt_name] = 'Offline')
BEGIN
INSERT INTO [dbo].[terminal_types] ([tt_type], [tt_name]) VALUES(108, 'Offline')
END
GO

IF NOT EXISTS ( SELECT * FROM [dbo].[games] WHERE [gm_name] = 'GAME_OFFLINE')
BEGIN
INSERT INTO [dbo].[games] ([gm_name]) VALUES ('GAME_OFFLINE')
END
GO

/******* PROCEDURES *******/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CustomersPlaying]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CustomersPlaying]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Alberto Marcos>
-- Create date: <21-JUL-2015>
-- Description:	<Customers Playing>
-- =============================================
CREATE PROCEDURE [dbo].[CustomersPlaying]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

				 -- TERMINALES
SELECT   TE_TERMINAL_ID
			 , TE_PROV_ID
			 , ISNULL(PV_NAME,'') AS PV_NAME
			 , ISNULL(TE_NAME,'') AS TE_NAME
			 , ISNULL(TE_FLOOR_ID,'') AS TE_FLOOR_ID
			 , ISNULL(BK_AREA_ID, -1) AS BK_AREA_ID --'AREA'
			 , ISNULL(AR_NAME,'') AS AR_NAME
			 , ISNULL(TE_BANK_ID, -1) AS TE_BANK_ID --'ISLA'
			 , ISNULL(BK_NAME,'') AS BK_NAME
			 , ISNULL(TE_POSITION, -1) AS TE_POSITION --'POSICION'
			   -- JUGADORES
			 , AC_ACCOUNT_ID
			 , ISNULL(AC_HOLDER_NAME,'') AS AC_HOLDER_NAME
			 , ISNULL(AC_TRACK_DATA,'') AS AC_TRACK_DATA
			 , AC_HOLDER_LEVEL AS AC_HOLDER_LEVEL
       , (SELECT   GP_KEY_VALUE     
            FROM   GENERAL_PARAMS   
           WHERE   GP_GROUP_KEY   = 'PlayerTracking'  
             AND   GP_SUBJECT_KEY = 'Level' + RIGHT('0' + CAST(AC_HOLDER_LEVEL AS NVARCHAR), 2) + '.Name' 
            ) AS AC_LEVEL             		
       , AC_HOLDER_IS_VIP	 
			   -- SESION DE JUEGO
			 , PS1.PS_PLAY_SESSION_ID
			 , PS1.PS_STARTED
			 , DATEDIFF(SECOND, PS1.PS_STARTED, ISNULL(PS1.PS_FINISHED, GETDATE())) AS PS1PS_DURATION
			 , PS1.PS_PLAYED_COUNT
			 , PS1.PS_PLAYED_AMOUNT
			 , CASE WHEN  PS1.PS_PLAYED_COUNT = 0 THEN ''
			        ELSE ROUND((PS1.PS_PLAYED_AMOUNT / PS1.PS_PLAYED_COUNT), 2) END AS PS_AVERAGE_BET -- 'APUESTA MEDIA'             
			   -- CLIENTE JORNADA
			 , WD.DURATION AS WD_DURATION
			 , WD.PLAYED_COUNT AS WD_PLAYED_COUNT
			 , WD.PLAYED_AMOUNT AS WD_PLAYED_AMOUNT
			 , WD.PS_AVERAGE_BET AS WD_AVERAGE_BET
			 , AC_TYPE
FROM
(SELECT   PS.PS_ACCOUNT_ID
        , PS.DURATION
        , PS.PLAYED_COUNT
        , PS.PLAYED_AMOUNT
			  , CASE WHEN  PS.PLAYED_COUNT = 0 THEN NULL
			         ELSE ROUND((PS.PLAYED_AMOUNT / PS.PLAYED_COUNT), 2) END AS PS_AVERAGE_BET -- 'APUESTA MEDIA'             
   FROM
  (SELECT   PS_ACCOUNT_ID
          , SUM(DATEDIFF(SECOND, PS_STARTED, ISNULL(PS_FINISHED, GETDATE()))) AS DURATION
          , SUM(PS_PLAYED_COUNT) AS PLAYED_COUNT
			    , SUM(PS_PLAYED_AMOUNT) AS PLAYED_AMOUNT
     FROM PLAY_SESSIONS
    WHERE PS_STARTED >= dbo.TodayOpening(0) 
    GROUP BY PS_ACCOUNT_ID) AS PS) AS WD, 
 TERMINALS
INNER JOIN PROVIDERS ON TE_PROV_ID = PV_ID
INNER JOIN BANKS ON TE_BANK_ID = BK_BANK_ID
INNER JOIN AREAS ON BK_AREA_ID = AR_AREA_ID
INNER JOIN PLAY_SESSIONS PS1 ON TE_CURRENT_PLAY_SESSION_ID = PS_PLAY_SESSION_ID
INNER JOIN ACCOUNTS ON PS_ACCOUNT_ID = AC_ACCOUNT_ID
WHERE   TE_CURRENT_PLAY_SESSION_ID IS NOT NULL
  AND   WD.PS_ACCOUNT_ID = AC_ACCOUNT_ID
  
END
GO

GRANT EXECUTE ON [dbo].[CustomersPlaying] TO [wggui] WITH GRANT OPTION
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_Coljuegos_ReporteDiario]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_Coljuegos_ReporteDiario]
GO

CREATE PROCEDURE [dbo].[SP_Coljuegos_ReporteDiario]
  (@Date DATETIME) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  DECLARE @FechaReporte nvarchar(1024)
  DECLARE @NIT          nvarchar(1024)
  DECLARE @Contrato     nvarchar(1024)
  DECLARE @Codigo       nvarchar(1024)
      
  -- FechaReporte
  SELECT   @FechaReporte = CONVERT(VARCHAR(25), @Date, 112)

  -- NIT
  SELECT   @NIT = gp_key_value
    FROM   general_params
   WHERE   gp_group_key = 'Interface.Coljuegos'
     AND   gp_subject_key = 'Operador.NIT'
 
  -- Contrato
  SELECT   @Contrato = gp_key_value
    FROM   general_params
   WHERE   gp_group_key = 'Interface.Coljuegos'
     AND   gp_subject_key = 'Operador.Contrato.Numero'
     
  -- Codigo
  SELECT   @Codigo = gp_key_value
    FROM   general_params
   WHERE   gp_group_key = 'Interface.Coljuegos'
     AND   gp_subject_key = 'Operador.Municipio.CodigoDANE'     

  -------------------
  -- ReporteDiario --
  -------------------
  SELECT   4             AS NumColumns
         , 'RI'          AS Indicador
         , @FechaReporte AS Col1
         , @NIT          AS Col2
         , (SELECT   gp_key_value
              FROM   general_params
             WHERE   gp_group_key = 'Interface.Coljuegos'
               AND   gp_subject_key = 'Fabricante.Clave') AS Col3
         , NULL          AS Col4
         , NULL          AS Col5
         , NULL          AS Col6
         , NULL          AS Col7
         , NULL          AS Col8
         , NULL          AS Col9
         , NULL          AS Col10
         , NULL          AS Col11
         , NULL          AS Col12
   UNION
  SELECT   3         AS NumColumns
         , 'RC'      AS Indicador
         , @Contrato AS Col1
         , @Codigo   AS Col2
         , NULL      AS Col3
         , NULL      AS Col4
         , NULL      AS Col5
         , NULL      AS Col6
         , NULL      AS Col7
         , NULL      AS Col8
         , NULL      AS Col9
         , NULL      AS Col10
         , NULL      AS Col11
         , NULL      AS Col12
   UNION
  SELECT   13                      AS NumColumns
         , 'RD'                    AS Indicador
         , WXM_NUC                 AS Col1
         , WXM_NUID                AS Col2
         , WXM_SERIAL              AS Col3
         , ROUND(WXM_COIN_IN, 0, 1)   AS Col4
         , ROUND(WXM_COIN_OUT, 0, 1)  AS Col5
         , ROUND(WXM_JACKPOT, 0, 1)   AS Col6
         , ROUND(WXM_HAND_PAID, 0, 1) AS Col7
         , ROUND(WXM_BILL_IN, 0, 1)   AS Col8
         , WXM_GAMES_PLAYED        AS Col9
         , WXM_PAYOUT              AS Col10
         , RIGHT('00' + CAST(WXM_EVENT_ID AS VARCHAR), 2) AS Col11
         , REPLACE(
            REPLACE(
             REPLACE(CONVERT(VARCHAR(19), CONVERT(DATETIME, wxm_datetime, 112), 126)
             , '-', '')
            , 'T', '')
           , ':', '')           AS Col12
    FROM   WXP_002_MESSAGES
   WHERE   WXM_DATETIME >= @Date
     AND   WXM_DATETIME <  DATEADD(DAY,1,@Date)
   UNION
  SELECT   3         AS NumColumns
         , 'RE'      AS Indicador
         , @Contrato AS Col1
         , @Codigo   AS Col2
         , NUll      AS Col3
         , NULL      AS Col4
         , NULL      AS Col5
         , NULL      AS Col6
         , NULL      AS Col7
         , NULL      AS Col8
         , NULL      AS Col9
         , NULL      AS Col10
         , NULL      AS Col11
         , NULL      AS Col12
  UNION  
  SELECT   3             AS NumColumns
         , 'RF'          AS Indicador
         , @FechaReporte AS Col1
         , @NIT          AS Col2
         , NULL          AS Col3
         , NULL          AS Col4
         , NULL          AS Col5
         , NULL          AS Col6
         , NULL          AS Col7
         , NULL          AS Col8
         , NULL          AS Col9
         , NULL          AS Col10
         , NULL          AS Col11
         , NULL          AS Col12
                    
END

GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_GenerateColombiaEventRegister]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_GenerateColombiaEventRegister]
GO

CREATE PROCEDURE [dbo].[SP_GenerateColombiaEventRegister]
(
   @pTerminal Integer, 
   @pEventId Integer,
   @pEndDayRegisterDatetime DateTime
) 
AS

BEGIN

DECLARE @DefaultPayout AS INTEGER
DECLARE @Now as Datetime

set @Now = GETDATE()

--If Terminal payout is not defined, it uses the GP default payout
SELECT   @DefaultPayout = GP_KEY_VALUE
  FROM   GENERAL_PARAMS
 WHERE   GP_SUBJECT_KEY = 'TerminalDefaultPayout' AND GP_GROUP_KEY = 'PlayerTracking'

--Gets and pivots the specified terminal meters
    SELECT   TE_TERMINAL_ID
           , TSM_METER_CODE
           , CASE WHEN TSM_METER_CODE IN (0,1,2,3,11) THEN TSM_METER_VALUE/100.0 ELSE TSM_METER_VALUE END AS METER_VALUE
           , CASE WHEN TE_THEORETICAL_PAYOUT IS NOT NULL THEN TE_THEORETICAL_PAYOUT * 100 ELSE @DefaultPayout END AS TE_THEORETICAL_PAYOUT
           , TE_REGISTRATION_CODE-- AS NUC
           , TE_EXTERNAL_ID --AS NUID
           , TE_SERIAL_NUMBER --AS SERIAL
      INTO   #TEMP 
      FROM   TERMINALS
 LEFT JOIN   TERMINAL_SAS_METERS ON TSM_TERMINAL_ID = TE_TERMINAL_ID
     WHERE   TE_TERMINAL_ID = CASE WHEN @pTerminal <> 0 THEN  @pTerminal ELSE TE_TERMINAL_ID END 
       AND   TE_STATUS = CASE WHEN @pEventId <> 9 THEN 0 ELSE TE_STATUS END 
       AND   TE_TERMINAL_TYPE = 5

IF @pEndDayRegisterDatetime IS NOT NULL and @pEventId = 0
BEGIN
INSERT INTO   WXP_002_MESSAGES
            ( WXM_TERMINAL_ID
            , wxm_datetime
            , wxm_event_id
            , wxm_nuc
            , wxm_nuid
            , wxm_serial
            , wxm_coin_in
            , wxm_coin_out
            , wxm_jackpot
            , wxm_hand_paid
            , wxm_games_played
            , wxm_bill_in
            , wxm_payout
            )
            SELECT   TE_TERMINAL_ID 
                   , @pEndDayRegisterDatetime
                   , @pEventId
                   , TE_REGISTRATION_CODE
                   , TE_EXTERNAL_ID
                   , TE_SERIAL_NUMBER
                   , ISNULL([0],0)  AS COIN_IN
                   , ISNULL([1],0)  AS COIN_OUT
                   , ISNULL([2],0)  AS JACKPOT
                   , ISNULL([3],0)  AS HANDPAY
                   , ISNULL([5],0)  AS GAMES_PLAYED
                   , ISNULL([11],0) AS BILL_IN
                   , ISNULL(TE_THEORETICAL_PAYOUT, @DefaultPayout)
              FROM   #TEMP 
             PIVOT   (SUM(METER_VALUE) 
               FOR   TSM_METER_CODE IN ([0],[1],[2],[3],[5],[11])) AS METERS
END
--Inserts the row
INSERT INTO   WXP_002_MESSAGES
            ( WXM_TERMINAL_ID
            , wxm_datetime
            , wxm_event_id
            , wxm_nuc
            , wxm_nuid
            , wxm_serial
            , wxm_coin_in
            , wxm_coin_out
            , wxm_jackpot
            , wxm_hand_paid
            , wxm_games_played
            , wxm_bill_in
            , wxm_payout
            )
            SELECT   TE_TERMINAL_ID 
                   , @Now
                   , @pEventId
                   , TE_REGISTRATION_CODE
                   , TE_EXTERNAL_ID
                   , TE_SERIAL_NUMBER
                   , ISNULL([0],0)  AS COIN_IN
                   , ISNULL([1],0)  AS COIN_OUT
                   , ISNULL([2],0)  AS JACKPOT
                   , ISNULL([3],0)  AS HANDPAY
                   , ISNULL([5],0)  AS GAMES_PLAYED
                   , ISNULL([11],0) AS BILL_IN
                   , ISNULL(TE_THEORETICAL_PAYOUT, @DefaultPayout)
              FROM   #TEMP 
             PIVOT   (SUM(METER_VALUE) 
               FOR   TSM_METER_CODE IN ([0],[1],[2],[3],[5],[11])) AS METERS

--Inserts the row
     DROP TABLE #TEMP
END 
GO

GRANT EXECUTE ON [dbo].[SP_GenerateColombiaEventRegister] TO [wggui] WITH GRANT OPTION
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Tax_Report_Per_Day]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Tax_Report_Per_Day]
GO

CREATE PROCEDURE [dbo].[Tax_Report_Per_Day]        
                 @pDateFrom DATETIME
               , @pDateTo   DATETIME       
  AS
BEGIN 

  DECLARE @CARD_DEPOSIT_IN                              INT
  DECLARE @CARD_REPLACEMENT                             INT
  DECLARE @CARD_DEPOSIT_IN_CHECK                        INT
  DECLARE @CARD_DEPOSIT_IN_CURRENCY_EXCHANGE            INT
  DECLARE @CARD_DEPOSIT_IN_CARD_CREDIT                  INT
  DECLARE @CARD_DEPOSIT_IN_CARD_DEBIT                   INT
  DECLARE @CARD_DEPOSIT_IN_CARD_GENERIC                 INT
  DECLARE @CARD_REPLACEMENT_CHECK                       INT
  DECLARE @CARD_REPLACEMENT_CARD_CREDIT                 INT
  DECLARE @CARD_REPLACEMENT_CARD_DEBIT                  INT
  DECLARE @CARD_REPLACEMENT_CARD_GENERIC                INT
  DECLARE @COMPANY_B_CARD_DEPOSIT_IN                    INT
  DECLARE @COMPANY_B_CARD_REPLACEMENT                   INT
  DECLARE @COMPANY_B_CARD_DEPOSIT_IN_CHECK              INT
  DECLARE @COMPANY_B_CARD_DEPOSIT_IN_CURRENCY_EXCHANGE  INT
  DECLARE @COMPANY_B_CARD_DEPOSIT_IN_CARD_CREDIT        INT
  DECLARE @COMPANY_B_CARD_DEPOSIT_IN_CARD_DEBIT         INT
  DECLARE @COMPANY_B_CARD_DEPOSIT_IN_CARD_GENERIC       INT
  DECLARE @COMPANY_B_CARD_REPLACEMENT_CHECK             INT
  DECLARE @COMPANY_B_CARD_REPLACEMENT_CARD_CREDIT       INT
  DECLARE @COMPANY_B_CARD_REPLACEMENT_CARD_DEBIT        INT
  DECLARE @COMPANY_B_CARD_REPLACEMENT_CARD_GENERIC      INT

  DECLARE @CASH_IN_SPLIT2                               INT
  DECLARE @MB_CASH_IN_SPLIT2                            INT
  DECLARE @NA_CASH_IN_SPLIT2                            INT
  DECLARE @_DAY_VAR                                     DATETIME 

  SET @CARD_DEPOSIT_IN                                  =   9
  SET @CARD_REPLACEMENT                                 =   28

  SET @CARD_DEPOSIT_IN_CHECK                            =   532
  SET @CARD_DEPOSIT_IN_CURRENCY_EXCHANGE                =   533
  SET @CARD_DEPOSIT_IN_CARD_CREDIT                      =   534
  SET @CARD_DEPOSIT_IN_CARD_DEBIT                       =   535
  SET @CARD_DEPOSIT_IN_CARD_GENERIC                     =   536
  SET @CARD_REPLACEMENT_CHECK                           =   537
  SET @CARD_REPLACEMENT_CARD_CREDIT                     =   538
  SET @CARD_REPLACEMENT_CARD_DEBIT                      =   539
  SET @CARD_REPLACEMENT_CARD_GENERIC                    =   540
  SET @COMPANY_B_CARD_DEPOSIT_IN                        =   541
  SET @COMPANY_B_CARD_REPLACEMENT                       =   543
  SET @COMPANY_B_CARD_DEPOSIT_IN_CHECK                  =   544
  SET @COMPANY_B_CARD_DEPOSIT_IN_CURRENCY_EXCHANGE      =   545
  SET @COMPANY_B_CARD_DEPOSIT_IN_CARD_CREDIT            =   546
  SET @COMPANY_B_CARD_DEPOSIT_IN_CARD_DEBIT             =   547
  SET @COMPANY_B_CARD_DEPOSIT_IN_CARD_GENERIC           =   548
  SET @COMPANY_B_CARD_REPLACEMENT_CHECK                 =   549
  SET @COMPANY_B_CARD_REPLACEMENT_CARD_CREDIT           =   550
  SET @COMPANY_B_CARD_REPLACEMENT_CARD_DEBIT            =   551
  SET @COMPANY_B_CARD_REPLACEMENT_CARD_GENERIC          =   552

  SET @CASH_IN_SPLIT2                                   =   34
  SET @MB_CASH_IN_SPLIT2                                =   35
  SET @NA_CASH_IN_SPLIT2                                =   55

  IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID('#TEMP_TABLE') AND type in (N'U'))
  BEGIN                                
    DROP TABLE #TEMP_TABLE  
  END       

  CREATE  TABLE #TEMP_TABLE ( TODAY   DATETIME PRIMARY KEY ) 
  
  IF @pDateTo IS NULL
  BEGIN
     -- IF DATE TO IS NULL, QUERY UNTIL CURRENT DATE
     SET @pDateTo = CAST(GETDATE() AS DATETIME)
  END

  -- TEMP TABLE IS FILLED WITH THE RANGE OF DATES
  SET @_DAY_VAR = @pDateFrom

  WHILE @_DAY_VAR < @pDateTo 
  BEGIN 
     INSERT INTO   #TEMP_TABLE (Today) VALUES (@_DAY_VAR)
     SET @_DAY_VAR =  DATEADD(Day,1,@_DAY_VAR)
  END 
  
  ;
  -- CASHIER MOVEMENTS FILTERED BY DATE AND MOVEMENT TYPES
  WITH MovementsPerWorkingday  AS 
   (
     SELECT   dbo.Opening(0, CM_DATE) 'WorkingDate'
            , CM_TYPE
            , CM_ADD_AMOUNT
            , CM_AUX_AMOUNT
       FROM   CASHIER_MOVEMENTS_GROUPED_BY_HOUR
      WHERE   (CM_DATE >= @pDateFrom AND (CM_DATE < @pDateTo))
              AND  CM_TYPE IN (  @MB_CASH_IN_SPLIT2
                               , @CASH_IN_SPLIT2
                               , @NA_CASH_IN_SPLIT2
                               , @CARD_DEPOSIT_IN
                               , @CARD_REPLACEMENT 
                               , @CARD_DEPOSIT_IN_CHECK                       
                               , @CARD_DEPOSIT_IN_CURRENCY_EXCHANGE           
                               , @CARD_DEPOSIT_IN_CARD_CREDIT                 
                               , @CARD_DEPOSIT_IN_CARD_DEBIT                  
                               , @CARD_DEPOSIT_IN_CARD_GENERIC                
                               , @CARD_REPLACEMENT_CHECK                      
                               , @CARD_REPLACEMENT_CARD_CREDIT                
                               , @CARD_REPLACEMENT_CARD_DEBIT                 
                               , @CARD_REPLACEMENT_CARD_GENERIC               
                               , @COMPANY_B_CARD_DEPOSIT_IN                   
                               , @COMPANY_B_CARD_REPLACEMENT                  
                               , @COMPANY_B_CARD_DEPOSIT_IN_CHECK             
                               , @COMPANY_B_CARD_DEPOSIT_IN_CURRENCY_EXCHANGE 
                               , @COMPANY_B_CARD_DEPOSIT_IN_CARD_CREDIT       
                               , @COMPANY_B_CARD_DEPOSIT_IN_CARD_DEBIT        
                               , @COMPANY_B_CARD_DEPOSIT_IN_CARD_GENERIC      
                               , @COMPANY_B_CARD_REPLACEMENT_CHECK            
                               , @COMPANY_B_CARD_REPLACEMENT_CARD_CREDIT      
                               , @COMPANY_B_CARD_REPLACEMENT_CARD_DEBIT       
                               , @COMPANY_B_CARD_REPLACEMENT_CARD_GENERIC     
                            )
  )

  SELECT   TimeRange.Today WorkingDate
         , ISNULL(((GrossCard - TaxCard) + (GrossCompanyB - TaxCompanyB)),0) BaseTotal
         , ISNULL((TaxCard + TaxCompanyB),0)       TaxTotal
         , ISNULL((GrossCard + GrossCompanyB),0)   GrossTotal
         , ISNULL((GrossCard - TaxCard),0)         BaseCard
         , ISNULL(TaxCard,0)                       TaxCard
         , ISNULL(GrossCard,0)                     GrossCard
         , ISNULL((GrossCompanyB - TaxCompanyB),0) BaseCompanyB         
         , ISNULL(TaxCompanyB,0)                   TaxCompanyB
         , ISNULL(GrossCompanyB,0)                 GrossCompanyB
  FROM (
           SELECT  WorkingDate
           ,
                   SUM(ISNULL(CASE WHEN CM_TYPE IN (  @CARD_DEPOSIT_IN
                                                    , @CARD_REPLACEMENT 
                                                    , @CARD_DEPOSIT_IN_CHECK                       
                                                    , @CARD_DEPOSIT_IN_CURRENCY_EXCHANGE           
                                                    , @CARD_DEPOSIT_IN_CARD_CREDIT                 
                                                    , @CARD_DEPOSIT_IN_CARD_DEBIT                  
                                                    , @CARD_DEPOSIT_IN_CARD_GENERIC                
                                                    , @CARD_REPLACEMENT_CHECK                      
                                                    , @CARD_REPLACEMENT_CARD_CREDIT                
                                                    , @CARD_REPLACEMENT_CARD_DEBIT                 
                                                    , @CARD_REPLACEMENT_CARD_GENERIC               
                                                    , @COMPANY_B_CARD_DEPOSIT_IN                   
                                                    , @COMPANY_B_CARD_REPLACEMENT                  
                                                    , @COMPANY_B_CARD_DEPOSIT_IN_CHECK             
                                                    , @COMPANY_B_CARD_DEPOSIT_IN_CURRENCY_EXCHANGE 
                                                    , @COMPANY_B_CARD_DEPOSIT_IN_CARD_CREDIT       
                                                    , @COMPANY_B_CARD_DEPOSIT_IN_CARD_DEBIT        
                                                    , @COMPANY_B_CARD_DEPOSIT_IN_CARD_GENERIC      
                                                    , @COMPANY_B_CARD_REPLACEMENT_CHECK            
                                                    , @COMPANY_B_CARD_REPLACEMENT_CARD_CREDIT      
                                                    , @COMPANY_B_CARD_REPLACEMENT_CARD_DEBIT       
                                                    , @COMPANY_B_CARD_REPLACEMENT_CARD_GENERIC     
                                                   )  THEN CM_ADD_AMOUNT
                        ELSE 0
                        END, 0)) 'GrossCard'
                  , SUM(ISNULL(CASE WHEN CM_TYPE IN (   @CARD_DEPOSIT_IN
                                                      , @CARD_REPLACEMENT 
                                                      , @CARD_DEPOSIT_IN_CHECK                       
                                                      , @CARD_DEPOSIT_IN_CURRENCY_EXCHANGE           
                                                      , @CARD_DEPOSIT_IN_CARD_CREDIT                 
                                                      , @CARD_DEPOSIT_IN_CARD_DEBIT                  
                                                      , @CARD_DEPOSIT_IN_CARD_GENERIC                
                                                      , @CARD_REPLACEMENT_CHECK                      
                                                      , @CARD_REPLACEMENT_CARD_CREDIT                
                                                      , @CARD_REPLACEMENT_CARD_DEBIT                 
                                                      , @CARD_REPLACEMENT_CARD_GENERIC               
                                                      , @COMPANY_B_CARD_DEPOSIT_IN                   
                                                      , @COMPANY_B_CARD_REPLACEMENT                  
                                                      , @COMPANY_B_CARD_DEPOSIT_IN_CHECK             
                                                      , @COMPANY_B_CARD_DEPOSIT_IN_CURRENCY_EXCHANGE 
                                                      , @COMPANY_B_CARD_DEPOSIT_IN_CARD_CREDIT       
                                                      , @COMPANY_B_CARD_DEPOSIT_IN_CARD_DEBIT        
                                                      , @COMPANY_B_CARD_DEPOSIT_IN_CARD_GENERIC      
                                                      , @COMPANY_B_CARD_REPLACEMENT_CHECK            
                                                      , @COMPANY_B_CARD_REPLACEMENT_CARD_CREDIT      
                                                      , @COMPANY_B_CARD_REPLACEMENT_CARD_DEBIT       
                                                      , @COMPANY_B_CARD_REPLACEMENT_CARD_GENERIC  
                                                    )  THEN CM_AUX_AMOUNT
                        ELSE 0
                        END, 0)) 'TaxCard'
                  , SUM(ISNULL(CASE WHEN CM_TYPE IN ( @MB_CASH_IN_SPLIT2,
                                                      @CASH_IN_SPLIT2, 
                                                      @NA_CASH_IN_SPLIT2 ) THEN CM_ADD_AMOUNT
                        ELSE 0
                        END, 0)) 'GrossCompanyB'
                  , SUM(ISNULL(CASE WHEN CM_TYPE IN ( @MB_CASH_IN_SPLIT2,
                                                      @CASH_IN_SPLIT2, 
                                                      @NA_CASH_IN_SPLIT2 ) THEN CM_AUX_AMOUNT
                        ELSE 0
                        END, 0)) 'TaxCompanyB'
           FROM MovementsPerWorkingday 
           GROUP BY WorkingDate
         ) MOVEMENTS RIGHT JOIN #TEMP_TABLE TimeRange on TimeRange.Today = MOVEMENTS.WorkingDate
   ORDER BY TimeRange.Today ASC       
   
   DROP TABLE #TEMP_TABLE
END
GO

GRANT EXECUTE ON [dbo].[Tax_Report_Per_Day] TO [wggui] WITH GRANT OPTION
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MBAndCashierSessionsCashMonitor]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[MBAndCashierSessionsCashMonitor]
GO

CREATE PROCEDURE [dbo].[MBAndCashierSessionsCashMonitor]
AS
BEGIN  
  SELECT     '0'                             AS ROW_TYPE                   
   	           , ISNULL(GU_USER_ID, '')        AS ROW_ID                   
               , ISNULL(GU_USERNAME, '')       AS USR_NAME                    
               , ISNULL(GU_FULL_NAME, '')      AS USR_FULL_NAME                   
               , ISNULL(GU_EMPLOYEE_CODE, '')  AS EMPLOYEE_CODE                   
               , ISNULL(CS_BALANCE, '')        AS CASHIER_BALANCE                   
               , 0                             AS MB_CASH      
               , 0                             AS MB_RECHARGE_LIMIT
               , 0                             AS MB_NUM_RECHARGES
               , ''                            AS MB_LAST_ACTIVITY
               , CT_NAME                       AS CASHIER_NAME
               , 0                             AS MB_RECHARGE
          FROM   GUI_USERS                            
     LEFT JOIN   CASHIER_SESSIONS                     
            ON   CS_USER_ID = GU_USER_ID     
  
     LEFT JOIN   CASHIER_TERMINALS                     
            ON   CS_CASHIER_ID = CT_CASHIER_ID
  
         WHERE   CS_STATUS = 0
           AND	 GU_USER_TYPE = 0
         UNION                                        

        SELECT   '1'                               			                AS ROW_TYPE  
               , ISNULL(MB_ACCOUNT_ID, '')				                      AS ROW_ID    
               , ISNULL('MB-' + CAST(MB_ACCOUNT_ID AS NVARCHAR), '')  	AS USR_NAME         
               , ISNULL(MB_HOLDER_NAME, '')				                      AS USR_FULL_NAME    
               , ISNULL(MB_EMPLOYEE_CODE, '')				                    AS EMPLOYEE_CODE    
               , 0         						                                  AS CASHIER_BALANCE  
               , ISNULL(MB_PENDING_CASH, 0)				                      AS MB_CASH      
               , ISNULL(MB_BALANCE, 0)					                        AS MB_RECHARGE_LIMIT
               , ISNULL(MB_ACTUAL_NUMBER_OF_RECHARGES, 0)			          AS MB_NUM_RECHARGES
               , ISNULL(MB_LAST_ACTIVITY, '')				                    AS MB_LAST_ACTIVITY
               , CT_NAME                                              	AS CASHIER_NAME
               , MB_CASH_IN                                             AS MB_RECHARGE
 
          FROM  MOBILE_BANKS                           
     LEFT JOIN  CASHIER_SESSIONS                     
            ON	CS_SESSION_ID = MB_CASHIER_SESSION_ID 
     LEFT JOIN  CASHIER_TERMINALS                     
            ON  CS_CASHIER_ID = CT_CASHIER_ID
         WHERE  CS_STATUS = 0
         AND	MB_ACCOUNT_TYPE IN ( 1 , 0 ) 
END
GO

GRANT EXECUTE ON [dbo].[MBAndCashierSessionsCashMonitor] TO [wggui] WITH GRANT OPTION;
GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ReportMachineInputOutputBalance]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ReportMachineInputOutputBalance]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	<Andreu Juli� & Fernando Jim�nez >
-- Create date: <20-AUG-2015>
-- Description:	<Pantalla de cuadratura de entradas y salidas de m�quina>
-- =============================================

CREATE PROCEDURE [dbo].[ReportMachineInputOutputBalance]
  @pDateFrom        DATETIME,
  @pDateTo          DATETIME,
  @pShowTerminal    BIT,
  @pShowImbalance   BIT,
  @pTerminalWhere   NVARCHAR(MAX) 
  
AS
BEGIN  
  
  DECLARE @_QUERY   NVARCHAR(MAX)
  
  SET @_QUERY = 
  '
    SELECT 
          P.PV_NAME
        , TOTAL.* 
      FROM
      (
        SELECT'
  IF (@pShowTerminal = 1)  --GROUP BY TERMINAL
  BEGIN
    SET @_QUERY = @_QUERY + '         
              [ProviderID]                                                                               [ProviderID]                    
            , [TerminalName]                                                                             [TerminalName]                  
            , T.TerminalID                                                                               [TerminalID]                    
            , ISNULL([SYSToEGM],0) - ISNULL([SYSToEGM.Cancel],0)						                             [SYS.Total.IN]                  
            , ISNULL([SYSFromEGM],0) + ISNULL([SYSFromEGM.Abandoned],0)                                  [SYS.Total.OUT]                 
	          , (ISNULL([SYSToEGM],0) - ISNULL([SYSToEGM.Cancel],0)) - 																	  
	             (ISNULL([SYSFromEGM],0) + ISNULL([SYSFromEGM.Abandoned],0))                               [SYS.Total.DIFF]                
            , ISNULL([FTToEGM],0)												                                                 [METER.Total.IN]                
            , ISNULL([FTFromEGM],0)											                                                 [METER.Total.OUT]               
	          , ISNULL([FTToEGM],0)  - ISNULL([FTFromEGM],0)					                                     [METER.Total.DIFF]              
            , (ISNULL([SYSToEGM],0) - ISNULL([SYSToEGM.Cancel],0)) - ISNULL([FTToEGM],0)			           [DIFF.Total.IN]                 
            , (ISNULL([SYSFromEGM],0) + ISNULL([SYSFromEGM.Abandoned],0) - ISNULL([FTFromEGM],0))	       [DIFF.Total.OUT]                
            , ((ISNULL([SYSToEGM],0) - ISNULL([SYSToEGM.Cancel],0)) - ISNULL([FTToEGM],0)) - 					  
               ((ISNULL([SYSFromEGM],0) + ISNULL([SYSFromEGM.Abandoned],0) - ISNULL([FTFromEGM],0)))	   [DIFF.Total]                    
            , ISNULL([SYSToEGM],0)                                                                       [SYSToEGM]                      
            , ISNULL([SYSToEGM.Cancel],0)                                                                [SYSToEGM.Cancel]
            , ISNULL([SYSFromEGM],0)                                                                     [SYSFromEGM]
            , ISNULL([SYSFromEGM.Abandoned],0)                                                           [SYSFromEGM.Abandoned]
            , ISNULL([SYSToEGM.Count],0)                                                                 [SYSToEGM.Count]
            , ISNULL([SYSFromEGM.Count],0)                                                               [SYSFromEGM.Count]
            , ISNULL([SYSFromEGM.Abandoned.Count],0)                                                     [SYSFromEGM.Abandoned.Count]
            , ISNULL([SYSToEGM.Cancel.Count],0)                                                          [SYSToEGM.Cancel.Count]
            , ISNULL([FTToEGM],0)                                                                        [FTToEGM]
            , ISNULL([FTFromEGM],0)                                                                      [FTFromEGM]
            , ISNULL([FTToEGM.Count],0)                                                                  [FTToEGM.Count]
            , ISNULL([FTFromEGM.Count],0)                                                                [FTFromEGM.Count]'

  END
  ELSE                  -- GROUP BY PROVIDER
  BEGIN
    SET @_QUERY = @_QUERY +' 
              [ProviderID]
            , NULL                                                                                       [TerminalName]
            , NULL                                                                                       [TerminalID]
            , SUM(ISNULL([SYSToEGM],0) - ISNULL([SYSToEGM.Cancel],0))						                         [SYS.Total.IN]
            , SUM(ISNULL([SYSFromEGM],0) + ISNULL([SYSFromEGM.Abandoned],0))                             [SYS.Total.OUT]
	          , SUM((ISNULL([SYSToEGM],0) - ISNULL([SYSToEGM.Cancel],0)) - 															   
	                (ISNULL([SYSFromEGM],0) + ISNULL([SYSFromEGM.Abandoned],0)))                            [SYS.Total.DIFF]
            , SUM(ISNULL([FTToEGM],0))												                                           [METER.Total.IN]
            , SUM(ISNULL([FTFromEGM],0))											                                           [METER.Total.OUT]
	          , SUM(ISNULL([FTToEGM],0)  - ISNULL([FTFromEGM],0))					                                 [METER.Total.DIFF]
            , SUM((ISNULL([SYSToEGM],0) - ISNULL([SYSToEGM.Cancel],0)) - ISNULL([FTToEGM],0))			       [DIFF.Total.IN]
            , SUM((ISNULL([SYSFromEGM],0) + ISNULL([SYSFromEGM.Abandoned],0) - ISNULL([FTFromEGM],0)))   [DIFF.Total.OUT]
            , SUM(((ISNULL([SYSToEGM],0) - ISNULL([SYSToEGM.Cancel],0)) - ISNULL([FTToEGM],0)) - 
                  ((ISNULL([SYSFromEGM],0) + ISNULL([SYSFromEGM.Abandoned],0) - ISNULL([FTFromEGM],0)))) [DIFF.Total] 
            , SUM(ISNULL([SYSToEGM],0))                                                                  [SYSToEGM]
            , SUM(ISNULL([SYSToEGM.Cancel],0))                                                           [SYSToEGM.Cancel]
            , SUM(ISNULL([SYSFromEGM],0))                                                                [SYSFromEGM]
            , SUM(ISNULL([SYSFromEGM.Abandoned],0))                                                      [SYSFromEGM.Abandoned]
            , SUM(ISNULL([SYSToEGM.Count],0))                                                            [SYSToEGM.Count]
            , SUM(ISNULL([SYSFromEGM.Count],0))                                                          [SYSFromEGM.Count]
            , SUM(ISNULL([SYSFromEGM.Abandoned.Count],0))                                                [SYSFromEGM.Abandoned.Count]
            , SUM(ISNULL([SYSToEGM.Cancel.Count],0))                                                     [SYSToEGM.Cancel.Count]
            , SUM(ISNULL([FTToEGM],0))                                                                   [FTToEGM]
            , SUM(ISNULL([FTFromEGM],0))                                                                 [FTFromEGM]
            , SUM(ISNULL([FTToEGM.Count],0))                                                             [FTToEGM.Count]
            , SUM(ISNULL([FTFromEGM.Count],0))                                                           [FTFromEGM.Count]'
  END  

SET @_QUERY = @_QUERY + '    
      FROM
      (
          SELECT    TE_TERMINAL_ID [TerminalID]
                  , TE_PROV_ID     [ProviderID]
                  , TE_PROVIDER_ID [ProviderNAME]
                  , TE_NAME        [TerminalName]
           FROM     TERMINALS
           WHERE    ' + @pTerminalWhere  + '
       ) T
       
       LEFT JOIN
       (   
        SELECT   AM_TERMINAL_ID   TERMINALID                                                       
               , SUM (CASE WHEN (AM_TYPE = 5) THEN ISNULL(am_sub_amount, 0) ELSE 0 END) [SYSToEGM]
               , SUM (CASE WHEN (AM_TYPE = 6) THEN ISNULL(am_add_amount, 0) ELSE 0 END) [SYSFromEGM]
               , SUM (CASE WHEN (AM_TYPE = 5 AND am_sub_amount > 0) THEN 1 ELSE 0 END)  [SYSToEGM.Count]
               , SUM (CASE WHEN (AM_TYPE = 6 AND am_add_amount > 0) THEN 1 ELSE 0 END)  [SYSFromEGM.Count]
               , SUM (CASE WHEN (AM_TYPE = 56) THEN ISNULL(am_add_amount, 0) ELSE 0 END)[SYSToEGM.Cancel]
               , SUM (CASE WHEN (AM_TYPE = 56 AND am_add_amount > 0) THEN 1 ELSE 0 END) [SYSToEGM.Cancel.Count]
          FROM   ACCOUNT_MOVEMENTS 
         WHERE   AM_TYPE IN (5,6, 56) -- StartCardSession = 5,EndCardSession = 6,CancelStartCardSession
           AND   AM_DATETIME  >= CAST(''' + CAST(@pDateFrom AS VARCHAR(50)) + ''' AS DATETIME)  
           AND   AM_DATETIME  <  CAST(''' + CAST(@pDateTo AS VARCHAR(50)) + ''' AS DATETIME)  
      GROUP BY   AM_TERMINAL_ID
      ) S   ON   T.TERMINALID = S.TERMINALID 
      LEFT JOIN
      (
      SELECT   MSH_TERMINAL_ID                   [TerminalID] 
             , SUM(ISNULL(MSH_TO_GM_AMOUNT,0))   [FTToEGM]
             , SUM(ISNULL(MSH_FROM_GM_AMOUNT,0)) [FTFromEGM]
             , SUM(ISNULL(MSH_TO_GM_COUNT,0))    [FTToEGM.Count]
             , SUM(ISNULL(MSH_FROM_GM_COUNT,0))  [FTFromEGM.Count]
        FROM   MACHINE_STATS_PER_HOUR 
       WHERE   MSH_BASE_HOUR >= CAST(''' + CAST(@pDateFrom AS VARCHAR(50)) + ''' AS DATETIME) 
         AND   MSH_BASE_HOUR < CAST(''' + CAST(@pDateTo AS VARCHAR(50)) + ''' AS DATETIME) 
    GROUP BY   MSH_TERMINAL_ID
      ) M ON   T.TERMINALID = M.TERMINALID 
      LEFT JOIN 
      (
      SELECT    HP_TERMINAL_ID  
             ,  SUM (HP_AMOUNT) [SYSFROMEGM.ABANDONED]
             ,  SUM (1) [SYSFROMEGM.ABANDONED.COUNT]
       FROM     HANDPAYS 
      WHERE     HP_DATETIME >= CAST(''' + CAST(@pDateFrom AS VARCHAR(50)) + ''' AS DATETIME) 
        AND     HP_DATETIME < CAST(''' + CAST(@pDateTo AS VARCHAR(50)) + ''' AS DATETIME) 
        AND     HP_TYPE = 2
   GROUP BY     HP_TERMINAL_ID
     ) H ON     T.TERMINALID = HP_TERMINAL_ID'
      
  IF (@pShowTerminal = 0)  --GROUP BY PROVIDER
  BEGIN
    SET @_QUERY = @_QUERY + '
      GROUP BY [ProviderID]'
  END

  SET @_QUERY = @_QUERY + '      
      ) TOTAL 
   
   INNER JOIN  PROVIDERS P 
           ON  P.PV_ID = TOTAL.PROVIDERID'
  
  
  IF (@pShowImbalance = 1) -- SHOW IMBALANCE
  BEGIN
    SET @_QUERY = @_QUERY + '
      WHERE [DIFF.Total] <> 0'
  END
  
  SET @_QUERY = @_QUERY + '
    ORDER BY 1'
    
  EXEC (@_QUERY)
 
END
  
GO

GRANT EXECUTE ON [dbo].[ReportMachineInputOutputBalance] TO [wggui] WITH GRANT OPTION

GO

 
--------------------------------------------------------------------------------
-- Copyright � 2014 Win Systems International
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: ReportShortfallExcess.sql
-- 
--   DESCRIPTION: Procedure for Cash ShortFall and Excess Report 
-- 
--        AUTHOR: Fernando Jim�nez
-- 
-- CREATION DATE: 04-OCT-2014
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 04-OCT-2014 FJC    First release.
-- 21-NOV-2014 FJC    Changes in Mobile bank query.
-- 24-NOV-2014 FJC    Changes in Mobile bank query.
-- 16-JUN-2015 AMF    Search by gaming day
-- 06-AUG-2015 JML    Change short/over movements 
--------------------------------------------------------------------------------


--------------------------------------------------------------------------------
-- PURPOSE: Selects cash shortfall and excess report.
-- 
--  PARAMS:
--      - INPUT:
--   @pDateFrom			 DATETIME     
--   @pDateTo				 DATETIME      
--   @pGamingDayMode BIT
--
--      - OUTPUT:
--
-- RETURNS:
--                  
--
--   NOTES:
--

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ReportShortfallExcess]') AND type in (N'P', N'PC'))
   DROP PROCEDURE [dbo].[ReportShortfallExcess]
GO

CREATE PROCEDURE [dbo].[ReportShortfallExcess]
  @pDateFrom DATETIME,  
  @pDateTo   DATETIME,
	@pGamingDayMode BIT
    
AS 
BEGIN
    --GUI USERS
		SELECT   GUI_USERS.GU_USER_ID        as  USERID 
					 , 0                           as  TYPE_USER
					 , GUI_USERS.GU_USERNAME       as  USERNAME 
					 , GUI_USERS.GU_FULL_NAME      as  FULLNAME 
					 , GUI_USERS.GU_EMPLOYEE_CODE  as  EMPLOYEECODE 
					 , X.CM_CURRENCY_ISO_CODE      as  CURRENCY 
					 , X.FALTANTE                  as  FALTANTE 
					 , X.SOBRANTE                  as  SOBRANTE 
					 , (X.SOBRANTE - X.FALTANTE)   as  DIFERENCIA
      FROM
      (        
      SELECT CS_USER_ID
             , CM_CURRENCY_ISO_CODE
             , SUM (CASE WHEN CM_TYPE = 160 THEN CM_ADD_AMOUNT ELSE 0 END) as FALTANTE
             , SUM (CASE WHEN CM_TYPE = 161 THEN CM_ADD_AMOUNT ELSE 0 END) as SOBRANTE             
        FROM CASHIER_MOVEMENTS  WITH (INDEX (IX_CM_DATE_TYPE))
				INNER JOIN CASHIER_SESSIONS ON  CM_SESSION_ID  = CS_SESSION_ID				
       WHERE CASE WHEN @pGamingDayMode = 1 THEN CS_GAMING_DAY ELSE CM_DATE END >= @pDateFrom  
         AND CASE WHEN @pGamingDayMode = 1 THEN CS_GAMING_DAY ELSE CM_DATE END <= @pDateTo
         AND CM_TYPE IN (160,  -- CASH_CLOSING_SHORT
                         161)  -- CASH_CLOSING_OVER
			GROUP BY CS_USER_ID
							 , CM_CURRENCY_ISO_CODE
    ) X
         
  INNER JOIN GUI_USERS
          ON GU_USER_ID = X.CS_USER_ID
         AND (X.FALTANTE<>0 OR X.SOBRANTE<>0)
         AND GU_USER_TYPE=0

    UNION ALL
    
    --BANK MOBILE USERS (this kind of users have national currency only)
    SELECT   MOBILE_BANKS.MB_ACCOUNT_ID                       as  USERID 
           , 1                                                as  TYPE_USER 
           , 'MB-' + CAST(MOBILE_BANKS.mb_account_id as CHAR) as  USERNAME
           , MOBILE_BANKS.MB_HOLDER_NAME                      as  FULLNAME
           , ''                                               as  EMPLOYEECODE
           , ''                                               as  CURRENCY
           , X2.FALTANTE                                      as  FALTANTE
           , X2.SOBRANTE                                      as  SOBRANTE 
           , (X2.SOBRANTE - X2.FALTANTE)                      as  DIFERENCIA
     FROM
     (
          
        SELECT   MBM_MB_ID
                 , SUM(CASE WHEN  MBM_TYPE IN (8,11)  THEN MBM_SUB_AMOUNT ELSE 0 END) AS FALTANTE
                 , SUM(CASE WHEN  MBM_TYPE = 9        THEN MBM_ADD_AMOUNT ELSE 0 END) AS SOBRANTE
            FROM   MB_MOVEMENTS MB1 WITH (INDEX (IX_MBM_DATETIME_TYPE))    
           WHERE   
                  (MBM_DATETIME  >= @pDateFrom  
             AND   MBM_DATETIME  <= @pDateTo)
             AND  ( ( MBM_TYPE IN (8,9) ) 
              OR    ( MBM_TYPE IN (11) 
                     
                     AND  NOT EXISTS 
                        (SELECT   MB2.MBM_MB_ID
                           FROM   MB_MOVEMENTS MB2 
                          WHERE   MB2.MBM_CASHIER_SESSION_ID = MB1.MBM_CASHIER_SESSION_ID 
                            AND   MB2.MBM_DATETIME > MB1.MBM_DATETIME 
                            AND   MB2.MBM_DATETIME <= @pDateTo
                            AND   MB2.MBM_TYPE IN (3) ) ) ) 
        GROUP BY   MBM_MB_ID
        
     ) X2
    
      INNER JOIN   MOBILE_BANKS
              ON   X2.MBM_MB_ID = MOBILE_BANKS.MB_ACCOUNT_ID
             AND   (X2.FALTANTE<>0 OR X2.SOBRANTE<>0)
      
        ORDER BY   USERNAME
END
GO

GRANT EXECUTE ON [dbo].[ReportShortfallExcess] TO [wggui] WITH GRANT OPTION
GO

ALTER PROCEDURE [dbo].[CageUpdateSystemMeters]
        @pCashierSessionId BIGINT
  AS
BEGIN             

  DECLARE @CageSessionId BIGINT
  DECLARE @NationalCurrency VARCHAR(3)
  DECLARE @Currencies AS VARCHAR(200)
  DECLARE @CurrentCurrency AS VARCHAR(3)
    
  DECLARE @Tax1 MONEY
  DECLARE @Tax2 MONEY
  DECLARE @ClosingShort MONEY
  DECLARE @ClosingOver MONEY
  
  DECLARE @CardRefundable BIT
  DECLARE @CardDeposit MONEY
  
  SELECT @NationalCurrency = GP_KEY_VALUE FROM GENERAL_PARAMS 
   WHERE GP_GROUP_KEY = 'RegionalOptions' 
     AND GP_SUBJECT_KEY = 'CurrencyISOCode'
  
  
  ---- GET CAGE SESSION ID ----
  
  SET @CageSessionId = (SELECT TOP 1 CGM_CAGE_SESSION_ID 
                                     FROM CAGE_MOVEMENTS
                                   WHERE CGM_CASHIER_SESSION_ID = @pCashierSessionId 
                                      AND CGM_TYPE IN (0, 1, 4, 5)
                               ORDER BY CGM_MOVEMENT_DATETIME DESC)

  IF @CageSessionId IS NULL
      SET @CageSessionId = (SELECT TOP 1 CGM_CAGE_SESSION_ID 
                                FROM CAGE_MOVEMENTS
                               WHERE CGM_CASHIER_SESSION_ID = @pCashierSessionId 
                                 AND CGM_TYPE IN (100, 101, 103, 104)
                          ORDER BY CGM_MOVEMENT_DATETIME DESC)
      
  IF @CageSessionId IS NULL
      SET @CageSessionId = (SELECT TOP 1 CGS_CAGE_SESSION_ID 
                                FROM CAGE_SESSIONS
                               WHERE CGS_CLOSE_DATETIME IS NULL
                          ORDER BY CGS_CAGE_SESSION_ID DESC)
   
   IF @CageSessionId IS NULL
      SET @CageSessionId = (SELECT TOP 1 CGS_CAGE_SESSION_ID 
                                FROM CAGE_SESSIONS
                             ORDER BY CGS_CAGE_SESSION_ID DESC)
   
      IF @CageSessionId IS NOT NULL
      BEGIN
  
            -- FEDERAL TAX --
  
            SELECT @Tax1 = SUM(CM_ADD_AMOUNT) 
              FROM CASHIER_MOVEMENTS_GROUPED_BY_SESSION_ID  
           WHERE CM_TYPE IN (142, 6) 
             AND CM_SESSION_ID = @pCashierSessionId

            SET @Tax1 = ISNULL(@Tax1, 0)
            
            EXEC [dbo].[CageUpdateMeter]
                  @pValueIn = @Tax1,
            @pValueOut = 0,
            @pSourceTagetId = 0,
            @pConceptId = 3,
            @pIsoCode = @NationalCurrency,
            @pOperation = 1,
            @pSessionId = @CageSessionId,
            @pSessiongetMode = 0 
    
     
            -- STATE TAX --
   
            SELECT @Tax2 = SUM(CM_ADD_AMOUNT) 
              FROM CASHIER_MOVEMENTS_GROUPED_BY_SESSION_ID  
             WHERE CM_TYPE IN (146, 14) 
               AND CM_SESSION_ID = @pCashierSessionId 
          
          SET @Tax2 = ISNULL(@Tax2, 0)
         
            EXEC [dbo].[CageUpdateMeter]
                  @pValueIn = @Tax2,
            @pValueOut = 0,
            @pSourceTagetId = 0,
            @pConceptId = 2,
            @pIsoCode = @NationalCurrency,
            @pOperation = 1,
            @pSessionId = @CageSessionId,
            @pSessiongetMode = 0 
          
          
            ---- CARD_PAYMENT ---- (IF @CardRefundable IS FALSE: WE DON'T NEED TO RETURN VALUE FOR LIABILITIES)                                                                    


            SELECT @CardRefundable = GP_KEY_VALUE 
              FROM GENERAL_PARAMS  
             WHERE GP_GROUP_KEY = 'Cashier'                             
               AND GP_SUBJECT_KEY = 'CardRefundable'                    

            IF ISNULL(@CardRefundable, 0) = 1                                                                        
            BEGIN                                                                                                   
                  SELECT @CardDeposit = SUM(CASE WHEN CM_TYPE = 10 THEN -1 * CM_SUB_AMOUNT   
                                                              ELSE CM_ADD_AMOUNT END)                                                  
                FROM CASHIER_MOVEMENTS_GROUPED_BY_SESSION_ID                                                    
               WHERE CM_TYPE IN (9, 10, 532, 533, 534, 535, 536, 544, 545, 546, 547, 548)                                             
                 AND CM_SESSION_ID IN(SELECT DISTINCT(CGM_CASHIER_SESSION_ID)                                   
                                                       FROM CAGE_MOVEMENTS                                                                     
                                                    WHERE CGM_CASHIER_SESSION_ID = @pCashierSessionId)
                                                    
                  SET @CardDeposit = ISNULL(@CardDeposit, 0)
            
                  EXEC [dbo].[CageUpdateMeter]
                        @pValueIn = @CardDeposit,
                        @pValueOut = 0,
                        @pSourceTagetId = 0,
                        @pConceptId = 5,
                        @pIsoCode = @NationalCurrency,
                        @pOperation = 1,
                        @pSessionId = @CageSessionId,
                        @pSessiongetMode = 0
                     
            END                                                                                                     

			SELECT @Currencies = GP_KEY_VALUE 
			  FROM GENERAL_PARAMS 
             WHERE GP_GROUP_KEY = 'RegionalOptions' 
               AND GP_SUBJECT_KEY = 'CurrenciesAccepted'
    
			-- Split currencies ISO codes
			SELECT SST_VALUE AS CURRENCY_ISO_CODE INTO #TMP_CURRENCIES_ISO_CODES FROM [SplitStringIntoTable] (@Currencies, ';', 1)

			
			DECLARE Curs_CageUpdateSystemMeters CURSOR FOR 
			SELECT CURRENCY_ISO_CODE
			  FROM #TMP_CURRENCIES_ISO_CODES 
      
            SET NOCOUNT ON;

			OPEN Curs_CageUpdateSystemMeters

			FETCH NEXT FROM Curs_CageUpdateSystemMeters INTO @CurrentCurrency
    
			WHILE @@FETCH_STATUS = 0
			BEGIN
	
				-- CLOSING SHORT --
				
				SELECT @ClosingShort = SUM(CM_ADD_AMOUNT) 
					FROM CASHIER_MOVEMENTS_GROUPED_BY_SESSION_ID  
				WHERE CM_TYPE IN (160) 
					AND CM_SESSION_ID = @pCashierSessionId 
					AND CM_CURRENCY_ISO_CODE = @CurrentCurrency
				
				SET @ClosingShort = ISNULL(@ClosingShort, 0)
				
				EXEC [dbo].[CageUpdateMeter]
						@pValueIn = @ClosingShort,
						@pValueOut = 0,
						@pSourceTagetId = 0,
						@pConceptId = 7,
						@pIsoCode = @CurrentCurrency,
						@pOperation = 1,
						@pSessionId = @CageSessionId,
						@pSessiongetMode = 0 
				
				
				-- CLOSING OVER --
				
				SELECT @ClosingOver = SUM(CM_ADD_AMOUNT) 
					FROM CASHIER_MOVEMENTS_GROUPED_BY_SESSION_ID  
				WHERE CM_TYPE IN (161) 
					AND CM_SESSION_ID = @pCashierSessionId 
					AND CM_CURRENCY_ISO_CODE = @CurrentCurrency
				
				SET @ClosingOver = ISNULL(@ClosingOver, 0)
				
				EXEC [dbo].[CageUpdateMeter]
						@pValueIn = @ClosingOver,
						@pValueOut = 0,
						@pSourceTagetId = 0,
						@pConceptId = 8,
						@pIsoCode = @CurrentCurrency,
						@pOperation = 1,
						@pSessionId = @CageSessionId,
						@pSessiongetMode = 0 
					
					
				FETCH NEXT FROM Curs_CageUpdateSystemMeters INTO @CurrentCurrency
                
            END

			CLOSE Curs_CageUpdateSystemMeters
			DEALLOCATE Curs_CageUpdateSystemMeters

      END                                      

END -- CageUpdateSystemMeters
GO

/******* REGIONALIZATION *******/

-- DOMINICAN REPUBLIC STATE NAME
DECLARE @CountryISOCode2 AS VARCHAR(10)
SELECT @CountryISOCode2 = GP_KEY_VALUE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'RegionalOptions' AND GP_SUBJECT_KEY = 'CountryISOCode2'
IF (@CountryISOCode2 = 'DO')
BEGIN
	UPDATE GENERAL_PARAMS SET GP_KEY_VALUE = 'Provincia' WHERE GP_GROUP_KEY = 'Account.Fields'  AND GP_SUBJECT_KEY = 'State.Name'
	UPDATE GENERAL_PARAMS SET GP_KEY_VALUE = 'DO'     WHERE GP_GROUP_KEY = 'Account.DefaultValues' AND GP_SUBJECT_KEY = 'Country'
	UPDATE GENERAL_PARAMS SET GP_KEY_VALUE = '071'    WHERE GP_GROUP_KEY = 'Account.DefaultValues' AND GP_SUBJECT_KEY = 'DocumentType' -- C�dula de Identidad
END
GO

-- DOMINICAN REPUBLIC REGIONS
IF (NOT EXISTS (SELECT * FROM federal_states WHERE fs_country_iso_code2 = 'DO' ))
BEGIN
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Azua','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Bahoruco','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Barahona','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Dajab�n','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Distrito Nacional','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Duarte','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('El Seibo','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('El�as Pi�a','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Espaillat','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Hato Mayor','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Hermanas Mirabal','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Independencia','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('La Altagracia','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('La Romana','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('La Vega','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Mar�a Trinidad S�nchez','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Monse�or Nouel','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Monte Cristi','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Monte Plata','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Pedernales','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Peravia','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Puerto Plata','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Saman�','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('San Crist�bal','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('San Jos� de Ocoa','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('San Juan','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('San Pedro de Macor�s','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('S�nchez Ram�rez','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Santiago','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Santiago Rodr�guez','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Santo Domingo','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Valverde','DO')
END
GO

-- DOMINICAN REPUBLIC OCCUPATIONS
IF (NOT EXISTS (SELECT * FROM occupations WHERE oc_country_iso_code2 = 'DO' ))
BEGIN
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('NO DISPONIBLE','0000001',1,1000,'DO')
END
GO

-- DOMINICAN REPUBLIC IDENTIFICATION_TYPES
IF (NOT EXISTS (SELECT * FROM identification_types WHERE idt_country_iso_code2 = 'DO' ))
BEGIN
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (70,1,100,'Otro','DO')
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (71,1,1,'C�dula de Identidad','DO')
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (72,1,2,'Pasaporte','DO')
END
GO

-- TRINIDAD AND TOBAGO STATE NAME
DECLARE @CountryISOCode2 AS VARCHAR(10)
SELECT @CountryISOCode2 = GP_KEY_VALUE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'RegionalOptions' AND GP_SUBJECT_KEY = 'CountryISOCode2'
IF (@CountryISOCode2 = 'TT')
BEGIN
	UPDATE GENERAL_PARAMS SET GP_KEY_VALUE = 'Region' WHERE GP_GROUP_KEY = 'Account.Fields'  AND GP_SUBJECT_KEY = 'State.Name'
	UPDATE GENERAL_PARAMS SET GP_KEY_VALUE = 'TT'     WHERE GP_GROUP_KEY = 'Account.DefaultValues' AND GP_SUBJECT_KEY = 'Country'
	UPDATE GENERAL_PARAMS SET GP_KEY_VALUE = '081'    WHERE GP_GROUP_KEY = 'Account.DefaultValues' AND GP_SUBJECT_KEY = 'DocumentType' -- Drivers License
END
GO

--TRINIDAD AND TOBAGO REGIONS
IF (NOT EXISTS (SELECT * FROM federal_states WHERE fs_country_iso_code2 = 'TT' ))
BEGIN
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Port of Spain','TT')
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('San Fernando','TT')
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Chaguanas Borough','TT')
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Arima Borough','TT')
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Point Fortin','TT')
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Couva-Tabaquite-Talparo','TT')
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Diego Martin','TT')
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Penal-Debe','TT')
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Princes Town','TT')
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Rio Claro-Mayaro','TT')
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('San Juan-Laventille','TT')
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Sangre Grande','TT')
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Siparia','TT')
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Tunapuna-Piarco','TT')
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Tobago','TT')
END
GO

-- TRINIDAD AND TOBAGO OCCUPATIONS
IF (NOT EXISTS (SELECT * FROM occupations WHERE oc_country_iso_code2 = 'TT' ))
BEGIN
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('NOT AVAILABLE','0000001',1,1000,'TT')
END
GO

-- TRINIDAD AND TOBAGO IDENTIFICATION_TYPES
IF (NOT EXISTS (SELECT * FROM identification_types WHERE idt_country_iso_code2 = 'TT' ))
BEGIN
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (80,1,100,'Other','TT')
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (81,1,1,'Driver''s license','TT')
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (82,1,2,'Passport','TT')
END
GO

---
-- Assembly WSI.WigosBL02
---

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetOperationFromBarcode]') AND type in (N'P', N'PC'))
	DROP PROCEDURE GetOperationFromBarcode
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetSalesAndPaymentsByQtyOperations]') AND type in (N'P', N'PC'))
	DROP PROCEDURE GetSalesAndPaymentsByQtyOperations
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetSalesAndPaymentsByOperationList]') AND type in (N'P', N'PC'))
	DROP PROCEDURE GetSalesAndPaymentsByOperationList
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetSalesAndPaymentsByOperation]') AND type in (N'P', N'PC'))
	DROP PROCEDURE GetSalesAndPaymentsByOperation
GO

IF  EXISTS (SELECT * FROM sys.assemblies asms WHERE asms.name = N'WSI.WigosBL02')
	DROP ASSEMBLY [WSI.WigosBL02]
GO

CREATE ASSEMBLY [WSI.WigosBL02]
AUTHORIZATION [dbo]
FROM 0x4D5A90000300000004000000FFFF0000B800000000000000400000000000000000000000000000000000000000000000000000000000000000000000800000000E1FBA0E00B409CD21B8014CCD21546869732070726F6772616D2063616E6E6F742062652072756E20696E20444F53206D6F64652E0D0D0A2400000000000000504500004C010300FBBED1550000000000000000E00002210B010B00003A01000006000000000000EE58010000200000006001000000001000200000000200000400000000000000040000000000000000A0010000020000000000000300408500001000001000000000100000100000000000001000000000000000000000009C5801004F000000006001007003000000000000000000000000000000000000008001000C000000645701001C0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000200000080000000000000000000000082000004800000000000000000000002E74657874000000F438010000200000003A010000020000000000000000000000000000200000602E72737263000000700300000060010000040000003C0100000000000000000000000000400000402E72656C6F6300000C0000000080010000020000004001000000000000000000000000004000004200000000000000000000000000000000D0580100000000004800000002000500E067000084EF000001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001E02281100000A2A133004002E00000001000011027B150000040A7201000070068C02000002027B140000048C25000001027B160000048C26000001281200000A2A1E027B130000042A1E027B140000042A1E027B150000042A1E027B160000042A867243000070027B140000048C25000001027B160000048C26000001281300000A2ADE02281100000A02037B130000047D1300000402037B140000047D1400000402037B150000047D1500000402037B160000047D160000042A13300500A000000002000011020B0716323D071F642F380407031200280F0000062D22725D000070048C25000001028C02000002038C26000001281200000A731400000A7A04070306730E0000062A0220010003002E100220020003002E08022004000300332972E20000700C022001000300330672F20000700C16020308038C26000001281500000A730E0000062A7200010070028C02000002038C26000001281300000A731400000A7A13300400A500000003000011031451021200280D0000062D02162A20FFFFFF001304026F1600000A130511051E2E1811051F105945030000000B00000033000000140000002B31200100030013042B2F200200030013042B262002000300130406210000C16FF28623005C166A3610200400030013042B0720FFFFFF001304110420FFFFFF002E0D0311040616280900000651172A0212011202120328100000062C0C03080907280900000651172A162A000000133002001400000004000011021200280A0000062C08067B16000004692A152A133002000900000005000011021200280D0000062A000000133002003A0000000600001103166A550203281800000A2D02162A020C160D2B1808096F1900000A0A06281A00000A2D04160BDE0F0917580D09086F1600000A32DF172A072A9202281100000A020E047D1300000402037D1400000402047D1500000402057D160000042A00133004006200000007000011057E1B00000A51021632080220102700003202162A03163205031F643202162A04166A3209042000CA9A3B6A3202162A726C010070028C25000001048C26000001038C25000001281200000A0A0628110000060A16060528120000062D02162A172A0000133004005B0000000800001103155404155405156A551702120028120000062D02162A0628110000060B0607281C00000A2C02162A03061B1A6F1D00000A281E00000A5405061F091F096F1D00000A281F00000A5504061F12186F1D00000A281E00000A54172A00133003008400000009000011026F2000000A0D091F0F1F306F2100000A0D09096F1600000A1F0F596F2200000A0D091200281800000A2D067E1B00000A2A06282300000A0B160C0713051613062B1211051106911304081104580C110617581306110611058E6932E6087E120000045D0C7292010070088C2500000109281300000A0D091F141F306F2100000A0D092A1B300400AA0000000A000011047E1B00000A51031202281800000A2D08161306DD8E00000073320000060A061F406F2400000A022C13067E100000047E110000046F2500000A0B2B11067E100000047E110000046F2600000A0B732700000A130411040717732800000A130508282300000A0D11050916098E696F2900000A11056F2A00000A11046F2B00000A0D0916282C00000A0C0472A6010070088C2A000001281500000A51171306DE0626161306DE0011062A000001100000000007009AA100060200000123215E48475E2423464453484A76622D13300300330000000B0000111F108D3100000125D08A030004282E00000A8010000004178D310000010A0616179C06801100000420F526000080120000042ADE02281100000A02036F2F00000A740300001B7D1700000402038E697D180000040220000100008D310000017D1900000402281B0000062A0A172A0A172A0A172A0A172A0013300500240100000C000011027B1C0000042C1102283000000A6F3100000A733200000A7A032D1072B601007072CE010070733300000A7A0E042D1072180200707232020070733300000A7A041632170E05163212040558038E69300A0E0505580E048E69310B727E020070733400000A7A0405580C38AC00000002027B1A000004175820000100005DD27D1A00000402027B1B000004027B19000004027B1A000004915820000100005DD27D1B000004027B19000004027B1A000004910B027B19000004027B1A000004027B19000004027B1B000004919C027B19000004027B1B000004079C027B19000004027B1A00000491027B19000004027B1B000004915820000100005DD20A0E040E05030491027B19000004069161D29C04175810020E051758100504083F4DFFFFFF052A13300600340000000B000011027B1C0000042C1102283000000A6F3100000A733200000A7A058D310000010A02030405061628190000062602281B000006062A13300400840000000D000011160B2B0E027B190000040707D29C0717580B07027B190000048E6932E702167D1A00000402167D1B000004160C160D2B4708027B19000004099158027B1700000409027B180000045D915820000100005D0C027B1900000409910A027B1900000409027B1900000408919C027B1900000408069C0917580D09027B190000048E6932AE2A033003004C00000000000000027B1C0000042D43027B1700000416027B170000048E69283500000A027B1900000416027B190000048E69283500000A02167D1A00000402167D1B00000402177D1C00000402283600000A2A3E02283700000A021F407D3800000A2A0A1E2A42031E2E0B72CE020070733900000A7A2A1A733A00000A7A1A733A00000A7A1E178D310000012A56032C11038E6917310B7216030070733900000A7A2A13300500140000000E000011178D080000010A06161E1E16733B00000AA2062A13300500180000000E000011178D080000010A06161E20000800001E733B00000AA2062A0A192A4203192E0B724A030070733900000A7A2A0A172A4203172E0B728A030070733900000A7A2A062A000013300200230000000F000011733C00000A0A026F3D00000A1E5B8D310000010B06076F3E00000A02076F3F00000A2A2E72D2030070282D0000062AF6022D1072E203007072F2030070733300000A7A02722204007019284000000A2C0673320000062A0272D203007019284000000A2C06732E0000062A142A1E02281D0000062A000000033002006100000000000000027B1D0000042C1102283000000A6F3100000A733200000A7A032D10722A0400707238040070733300000A7A038E692C0A038E692000010000310B7278040070733900000A7A042C11048E6917310B72BC040070733900000A7A0373140000062A260203046F2500000A2A3E0203284100000A02177D1D0000042A4A02281D00000602732E0000067D1E0000042A32027B1E0000046F4200000A2A72031E2E0B72FE040070733900000A7A027B1E000004036F4300000A2A32027B1E0000046F4400000A2A36027B1E000004036F4500000A2A32027B1E0000046F4600000A2A36027B1E000004036F4700000A2A32027B1E0000046F4800000A2A36027B1E000004036F3F00000A2A32027B1E0000046F3D00000A2A36027B1E000004036F2400000A2A32027B1E0000046F4900000A2A32027B1E0000046F4A00000A2A32027B1E0000046F4B00000A2A36027B1E000004036F4C00000A2A32027B1E0000046F4D00000A2A36027B1E000004036F4E00000A2A32027B1E0000046F4F00000A2A32027B1E0000046F5000000A2A0000033003006D00000000000000027B1F0000042C1602283000000A6F3100000A723A050070735100000A7A032D10726C050070727A050070733300000A7A038E692C0A038E692000010000310B72BA050070733900000A7A042C11048E6917310B72FE050070733900000A7A027B1E00000403046F2500000A2A260203046F2500000A2AC2027B1F0000042D2702177D1F000004027B1E0000042C12027B1E0000046F5200000A02147D1E00000402283600000A2A62160220E8030000147E5300000A7E5400000A28520000062A7602166A3004176A1000170203147E5300000A7E5400000A28520000062A001B300600460000001000001102178D2B0000010B07161F2C9D076F5500000A0A062C0F068E692C0A068E6920E80300003102DE1DDE0326DE1818166A20E8030000027E5300000A7E5400000A28520000062A000001100000000000002A2A0003020000014619166A20E803000014020328520000062A00001B3004008901000011000011026F5600000A6F5700000A735800000A0A026F5600000A6F5900000A130838A900000011086F5A00000A74450000010B076F5B00000AD027000001285C00000A332B076F5D00000A076F5B00000A6F5E00000A284D00000620FF0000006A735F00000A0C06086F6000000A2B5F076F5B00000AD013000001285C00000A3328076F5D00000A076F5B00000A6F5E00000A284D0000061F121A736100000A0D06096F6000000A2B25076F5D00000A076F5B00000A6F5E00000A284D000006736200000A13040611046F6000000A11086F6300000A3A4BFFFFFFDE1511087504000001130911092C0711096F6400000ADC066F6500000A736600000A1305286700000A11056F6800000A026F6900000A6F5900000A130A2B47110A6F5A00000A741200000113061613072B191105110711066F6A00000A11079A6F6B00000A1107175813071107026F5600000A6F5700000A32D8286700000A11056F6C00000A110A6F6300000A2DB0DE15110A7504000001130B110B2C07110B6F6400000ADC286700000A6F6D00000A2A000000011C000002001E00BCDA00150000000002001501546901150000000013300400F70000001200001102250A39EC000000FE137E8B0300043A870000001F0A736E00000A25724006007016286F00000A25725E06007017286F00000A25727E06007018286F00000A25729C06007019286F00000A2572B80600701A286F00000A2572D00600701B286F00000A2572EA0600701C286F00000A2572040700701D286F00000A25721E0700701E286F00000A25723A0700701F09286F00000AFE13808B030004FE137E8B030004061201287000000A2C4807450A000000020000000400000006000000080000000A0000000D000000100000001200000014000000170000002B18182A1A2A1B2A1C2A1F0E2A1F102A1E2A162A1F0C2A1F152A1F172A2603737100000A51162A2604737100000A51162A2603737100000A51162A2604737100000A51162A0013300800B403000013000011140A7256070070737200000A0C086F7300000A020304050E040E0508120028530000062D012A086F7400000A066F5600000A6F5700000A0B0217404F030000066F5600000A72860700706F7500000A166F7600000A066F5600000A729E0700706F7500000A176F7600000A066F5600000A72AA0700706F7500000A186F7600000A066F5600000A72BE0700706F7500000A196F7600000A066F5600000A72CA0700706F7500000A1A6F7600000A066F5600000A72D80700706F7500000A1B6F7600000A066F5600000A72F40700706F7500000A1C6F7600000A066F5600000A72260800706F7500000A1D6F7600000A066F5600000A72400800706F7500000A1E6F7600000A066F5600000A72520800706F7500000A1F096F7600000A066F5600000A72640800706F7500000A1F0A6F7600000A066F5600000A72780800706F7500000A1F0B6F7600000A066F5600000A728C0800706F7500000A1F0C6F7600000A066F5600000A72AE0800706F7500000A1F0D6F7600000A066F5600000A72CC0800706F7500000A1F0E6F7600000A066F5600000A72EA0800706F7500000A1F0F6F7600000A066F5600000A72020900706F7500000A1F106F7600000A066F5600000A72340900706F7500000A1F116F7600000A066F5600000A724A0900706F7500000A1F126F7600000A066F5600000A72580900706F7500000A1F136F7600000A066F5600000A72620900706F7500000A1F146F7600000A066F5600000A726C0900706F7500000A1F156F7600000A066F5600000A728E0900706F7500000A1F166F7600000A066F5600000A72B80900706F7500000A1F176F7600000A066F5600000A72E20900706F7500000A1F186F7600000A066F5600000A72FE0900706F7500000A1F196F7600000A066F5600000A72200A00706F7500000A1F1A6F7600000A066F5600000A723E0A00706F7500000A1F1B6F7600000A066F5600000A725C0A00706F7500000A1F1C6F7600000A066F5600000A726E0A00706F7500000A1F1D6F7600000A066F5600000A727E0A00706F7500000A1F1E6F7600000A066F5600000A72920A00706F7500000A1F1F6F7600000A066F5600000A72A40A00706F7500000A1F206F7600000A066F5600000A72B80A00706F7500000A1F216F7600000A066F5600000A72D00A00706F7500000A1F226F7600000A066F5600000A72EC0A00706F7500000A1F236F7600000A066F5600000A72FC0A00706F7500000A1F246F7600000A1F250B2B111F2F0B2B0C066F5600000A076F7700000A066F5600000A6F5700000A0730E606284C0000062A1B300800CF000000140000110E07737100000A510416287800000A20E8030000287900000A10020219330720FFFFFF7F10020E066F7A00000A0C0828B10000060B737B00000A0D09086F7C00000A6F7D00000A09086F7E00000A020304050E040E050928540000062D05161305DE69020304050E040E0512000828590000062D05161305DE52096F7F00000A1304041104090607080E0728550000062D05161305DE35DE0C11042C0711046F6400000ADCDE0A092C06096F6400000ADCDE0A082C06086F6400000ADC171305DE0A260E071451161305DE0011052A0001340000020082001799000C0000000002003B006CA7000A0000000002002E0085B3000A0000000000000000C2C2000A020000011B300400DC09000015000011738000000A0A738000000A0B0672100B00706F8100000A260672460B00706F8100000A2606726C0B00706F8100000A260672920B00706F8100000A260672C40B00706F8100000A260672F60B00706F8100000A2606728F0C00706F8100000A260672280D00706F8100000A260672BB0D00706F8100000A260672500E00706F8100000A260672030F00706F8100000A260672370F00706F8100000A260672690F00706F8100000A2606729B0F00706F8100000A260672C90F00706F8100000A260672C81000706F8100000A260672C11100706F8100000A260672BC1200706F8100000A260672BF1300706F8100000A260672C21400706F8100000A260672C51500706F8100000A260672371600706F8100000A260672A91600706F8100000A2606721B1700706F8100000A2606724D1700706F8100000A260672811700706F8100000A260672A51700706F8100000A260672131800706F8100000A260672851800706F8100000A260672CD1800706F8100000A260672311900706F8100000A260672C81900706F8100000A2606725F1A00706F8100000A260672DB1A00706F8100000A2606727E1B00706F8100000A260672251C00706F8100000A260672CA1C00706F8100000A260672711D00706F8100000A260E066F8200000A720C1E0070166F8300000A048C250000016F8400000A020D094504000000050000003F000000760000009B00000038EA0000000E066F8200000A722A1E0070166F8300000A038C260000016F8400000A0672461E00706F8100000A260672D71E00706F8100000A2638B00000000E066F8200000A723F1F0070166F8300000A038C260000016F8400000A0672461E00706F8100000A2606725F1F00706F8100000A262B790672461E00706F8100000A260672CB1F0070057219200070288500000A6F8100000A262B540E066F8200000A72252000701A6F8300000A0E048C0B0000016F8400000A0E066F8200000A723B2000701A6F8300000A0E058C0B0000016F8400000A06724D2000706F8100000A260672D82000706F8100000A260672582100706F8100000A260672922100706F8100000A260672582100706F8100000A260672062200706F8100000A260E066F8200000A723C2200701E6F8300000A1F268C110000026F8400000A0E066F8200000A726A2200701E6F8300000A1F288C110000026F8400000A0E066F8200000A72902200701E6F8300000A1F258C110000026F8400000A0E066F8200000A72B62200701E6F8300000A1F278C110000026F8400000A0E066F8200000A72E02200701E6F8300000A1F368C110000026F8400000A0E066F8200000A720A2300701E6F8300000A1F558C110000026F8400000A0E066F8200000A72462300701E6F8300000A1F228C110000026F8400000A0E066F8200000A726C2300701E6F8300000A1F378C110000026F8400000A0E066F8200000A72962300701E6F8300000A1F238C110000026F8400000A0E066F8200000A72C02300701E6F8300000A1F568C110000026F8400000A0E066F8200000A72FC2300701E6F8300000A208E0000008C110000026F8400000A0E066F8200000A72262400701E6F8300000A20920000008C110000026F8400000A0E066F8200000A72522400701E6F8300000A1B8C110000026F8400000A0E066F8200000A726E2400701E6F8300000A1E8C110000026F8400000A0E066F8200000A72882400701E6F8300000A1C8C110000026F8400000A0E066F8200000A72AC2400701E6F8300000A1F0E8C110000026F8400000A0E066F8200000A72D02400701E6F8300000A1F5E8C110000026F8400000A0E066F8200000A72002500701E6F8300000A1F5F8C110000026F8400000A0E066F8200000A722E2500701E6F8300000A1F608C110000026F8400000A0E066F8200000A725C2500701E6F8300000A1F648C110000026F8400000A0E066F8200000A728C2500701E6F8300000A1F658C110000026F8400000A0E066F8200000A72BA2500701E6F8300000A1F668C110000026F8400000A0E066F8200000A72E82500701E6F8300000A1F4F8C110000026F8400000A0E066F8200000A72142600701E6F8300000A20930000008C110000026F8400000A0E066F8200000A724C2600701E6F8300000A20940000008C110000026F8400000A0E066F8200000A72822600701E6F8300000A20030200008C110000026F8400000A0E066F8200000A72BE2600701E6F8300000A20050200008C110000026F8400000A0E066F8200000A72062700701E6F8300000A20070200008C110000026F8400000A0E066F8200000A724C2700701E6F8300000A1F5C8C110000026F8400000A0E066F8200000A72722700701E6F8300000A200C0200008C110000026F8400000A0E066F8200000A72A82700701E6F8300000A1F4A8C110000026F8400000A0E066F8200000A72DA2700701E6F8300000A1F458C110000026F8400000A0E066F8200000A72002800701E6F8300000A1F468C110000026F8400000A0E066F8200000A722C2800701E6F8300000A208F0000008C110000026F8400000A0E066F8200000A72662800701E6F8300000A1F478C110000026F8400000A0E066F8200000A728E2800701E6F8300000A1F418C110000026F8400000A0E066F8200000A72C22800701E6F8300000A202F0100008C110000026F8400000A0E066F8200000A72E42800701E6F8300000A20300100008C110000026F8400000A0E066F8200000A720E2900701E6F8300000A1A8C110000026F8400000A0E066F8200000A72282900701E6F8300000A1F248C110000026F8400000A0E066F8200000A72562900701E6F8300000A1F298C110000026F8400000A0E066F8200000A727C2900701E6F8300000A20370100008C110000026F8400000A0E066F8200000A72AE2900701E6F8300000A1F0D8C290000026F8400000A0E066F8200000A72D02900701E6F8300000A1F0E8C290000026F8400000A0E066F8200000A720A2A00701E6F8300000A1F0F8C290000026F8400000A0E066F8200000A72342A00701E6F8300000A1F108C290000026F8400000A0772742A00706F8100000A260772032B00706F8100000A260772922B00706F8100000A260772232C00706F8100000A2607729F2C00706F8100000A260772322D00706F8100000A260772B52D00706F8100000A260772FB2D00706F8100000A260772A22E00706F8100000A260772492F00706F8100000A2607721E3000706F8100000A260772033100706F8100000A260772AC3100706F8100000A260772EE3100706F8100000A2607722C3200706F8100000A2607727C3200706F8100000A2607720F3300706F8100000A260772963300706F8100000A260772C63300706F8100000A2607722C3400706F8100000A260772763400706F8100000A260772B23400706F8100000A260E06066F5E00000A72BA340070076F5E00000A6F8600000A6F8700000A170CDE0526160CDE00082A411C0000000000000C000000C9090000D5090000050000000200000113300800C1100000160000112100000000000000800C160D737100000A130514130616738800000A130716131212052856000006267E1B00000A130E7E1B00000A13100E0472EE3400707202350070176FB6000006263808100000037E7E0000046F8900000A2D0D037E7E0000046F8A00000A2B02166A0A037E940000046F8900000A2D0D037E940000046F8A00000A2B02166A0B08063B9B0700001112175813121112023DC40F0000082100000000000000802E1C092C1911060E042858000006130411056F6900000A11046F8B00000A110716738800000A288C00000A2C1311067E4D00000411078C130000016F8D00000A037E830000046F8A00000A130811056F8E00000A130611067E43000004068C260000016F8D00000A037E7F0000046F8F00000A130911067E440000041209289000000A1209289100000A1209289200000A1209289300000A1209289400000A1209289500000A739600000A8C0B0000016F8D00000A11067E46000004037E960000046F8900000A2D0D037E960000046F9700000A2B01158C250000016F8D00000A037E960000046F9800000AA5290000021315111517594510000000140000004D00000014000000140000001400000084000000140000008400000084000000140000008400000084000000270000003A000000600000007300000011151F6D59450200000002000000020000002B7011067E7900000472383500706F8D00000A2B5D11067E7900000472483500706F8D00000A2B4A11067E7900000472683500706F8D00000A2B3711067E7900000472A83500706F8D00000A2B2411067E7900000472B23500706F8D00000A2B1111067E7900000472D43500706F8D00000A11067E4500000411088C260000016F8D00000A11067E55000004037E840000046F8900000A2D12037E840000046F9900000A6F2000000A2B01146F8D00000A11067E53000004037E850000046F8900000A2D12037E850000046F9900000A6F2000000A2B01146F8D00000A11067E54000004037E860000046F8900000A2D12037E860000046F9900000A6F2000000A2B01146F8D00000A11067E56000004037E870000046F8900000A2D12037E870000046F9900000A6F2000000A2B01146F8D00000A11067E57000004037E890000046F8900000A2D12037E890000046F9900000A6F2000000A2B01146F8D00000A11067E58000004037E890000046F8900000A2D12037E890000046F9900000A6F2000000A2B01146F8D00000A037E930000046F8900000A2D12037E930000046F9900000A6F2000000A2B0114130A037E910000046F8900000A2D12037E910000046F9900000A6F2000000A2B0114130B037E920000046F8900000A2D12037E920000046F9900000A6F2000000A2B0114130C11067E5B00000417110A7E1B00000A110B110C0E0428B00000066F8D00000A11067E59000004037E8C0000046F8900000A2D12037E8C0000046F9900000A6F2000000A2B01146F8D00000A11067E5A000004037E8D0000046F8900000A2D12037E8D0000046F9900000A6F2000000A2B01146F8D00000A11067E5F000004037E900000046F8900000A2D0D037E900000046F9A00000A2B01168C530000016F8D00000A11067E60000004037E8E0000046F8900000A2D0D037E8E0000046F9800000A2B057E9B00000A6F8D00000A11067E64000004078C260000016F8D00000A11067E66000004037E950000046F8900000A2C077E9B00000A2B0B037E950000046F9800000A6F8D00000A11067E6D000004168C250000016F8D00000A0E0472EE3400707212360070723C3600706FBB000006131011067E6C00000411106F8D00000A0E0472EE340070724E3600706FBA000006130E110E289C00000A2C130E0472EE340070727C3600706FBA000006130E11067E6B000004110E6F8D00000A11067E78000004168C250000016F8D00000A11067E6E000004037E990000046F8900000A2C077E9B00000A2B0B037E990000046F9800000A6F8D00000A11067E6F000004037E9A0000046F8900000A2C077E9B00000A2B0B037E9A0000046F9800000A6F8D00000A11067E70000004037E9B0000046F8900000A2C077E9B00000A2B0B037E9B0000046F9800000A6F8D00000A11067E71000004037E9C0000046F8900000A2C077E9B00000A2B0B037E9C0000046F9800000A6F8D00000A7298360070078C2600000111088C26000001068C26000001281200000A13130511136F9D00000A1314111439B301000011148E69163EA90100001114169A72253700706F9E00000A7E9B00000A2E4811067E680000041114169A72253700706F9E00000AA5260000018C260000016F8D00000A11067E690000041114169A72253700706F9E00000AA5260000018C260000016F8D00000A1114169A72413700706F9E00000A7E9B00000A2E2411067E7A0000041114169A72413700706F9E00000AA5260000018C260000016F8D00000A1114169A72593700706F9E00000A7E9B00000A2E2411067E720000041114169A72593700706F9E00000AA5260000018C260000016F8D00000A1114169A72753700706F9E00000A7E9B00000A2E2411067E730000041114169A72753700706F9E00000AA5250000018C250000016F8D00000A1114169A728B3700706F9E00000A7E9B00000A2E1F11067E740000041114169A728B3700706F9E00000A74270000016F8D00000A1114169A72A53700706F9E00000A7E9B00000A2E2411067E760000041114169A72A53700706F9E00000AA5250000018C250000016F8D00000A1114169A72C13700706F9E00000A7E9B00000A2E1F11067E770000041114169A72C13700706F9E00000A74270000016F8D00000A16738800000A13071106285700000611056F6900000A11066F8B00000A060C160D037E800000046F9800000AA511000002131611161F4A3DA900000011161F29305511161A59450500000060010000CC030000E0040000EE0700003204000011161F0E3B6A05000011161F22594508000000DF020000DF0200008F040000A50100008F040000A50100008F0400008F04000038B607000011161F365945020000008E010000C802000011161F3F5945090000005706000076070000E20500007607000076070000760700008E050000C6050000AA05000011161F4A3B6A060000386807000011161F66305611161F4F3BDE06000011161F55594502000000310100006B02000011161F5C59450B0000000E06000011070000710300000304000096040000110700001107000011070000710300000304000096040000380C0700001116203001000030431116208E00000059450700000080020000B9060000DA060000DA0600009C0200005F0600005F0600001116202F010000594502000000ED0200007E01000038C006000011162003020000594505000000240600009F060000240600009F060000240600001116200C0200003B90050000388E060000037E960000046F9800000AA5290000021F0F3B77060000037E960000046F9800000AA5290000021F0D3B60060000037E960000046F9800000AA5290000021F103B49060000037E960000046F9800000AA5290000021F0E3B3206000011067E7B000004037E810000046F9800000A6F8D00000A3816060000037E800000046F9800000AA5110000021F55331411067E78000004178C250000016F8D00000A2B2511067E780000046F9F00000AA5250000012D1211067E78000004188C250000016F8D00000A11067E48000004037E810000046F9800000A6F8D00000A11067E470000046F9F00000A7E9B00000A331C11067E47000004037E810000046F9800000A6F8D00000A388305000011067E4700000411067E470000046F9F00000AA513000001037E810000046F9800000AA51300000128A000000A8C130000016F8D00000A384705000011067E7D0000046F9F00000A7E9B00000A331C11067E7D000004037E810000046F9800000A6F8D00000A381805000011067E7D00000411067E470000046F9F00000AA513000001037E810000046F9800000AA51300000128A000000A8C130000016F8D00000A38DC04000011067E49000004037E810000046F9800000A6F8D00000A11067E470000046F9F00000A7E9B00000A331C11067E47000004037E810000046F9800000A6F8D00000A389604000011067E4700000411067E470000046F9F00000AA513000001037E810000046F9800000AA51300000128A000000A8C130000016F8D00000A385A04000011067E4A000004037E810000046F9800000A6F8D00000A383E04000011067E4B000004037E810000046F9800000A6F8D00000A382204000011067E4C000004037E820000046F9800000A6F8D00000A11067E51000004037E970000046F9800000A6F8D00000A11067E52000004037E980000046F9800000A6F8D00000A38D803000011067E7C000004037E820000046F9800000A6F8D00000A38BC03000011067E4E000004037E820000046F9800000A6F8D00000A38A003000011067E650000046F9F00000A7E9B00000A332311067E65000004037E820000046F9800000AA5130000018C130000016F8D00000A2B3711067E6500000411067E650000046F9F00000AA513000001037E820000046F9800000AA51300000128A000000A8C130000016F8D00000A170D382C0300001107037E820000046F9800000AA51300000128A000000A1307380E030000037E800000046F9800000AA5110000021F6533097E82000004130D2B077E81000004130D11067E4F0000046F9F00000A7E9B00000A332311067E4F00000403110D6F9800000AA5130000018C130000016F8D00000A38B402000011067E4F00000411067E4F0000046F9F00000AA51300000103110D6F9800000AA51300000128A000000A8C130000016F8D00000A387B020000037E800000046F9800000AA5110000021F6633097E82000004130D2B077E81000004130D11067E500000046F9F00000A7E9B00000A332311067E5000000403110D6F9800000AA5130000018C130000016F8D00000A382102000011067E5000000411067E500000046F9F00000AA51300000103110D6F9800000AA51300000128A000000A8C130000016F8D00000A38E801000011067E61000004037E820000046F9800000A6F8D00000A38CC01000011067E62000004037E810000046F9800000A6F8D00000A38B001000011067E63000004037E820000046F9800000A6F8D00000A389401000011067E6A0000046F9F00000A7E9B00000A2E3C11067E6A00000411067E6A0000046F9F00000AA513000001037E810000046F9800000AA51300000128A000000A8C130000016F8D00000A384501000011067E6A000004037E810000046F9800000AA5130000018C130000016F8D00000A381F01000011067E67000004037E820000046F9800000A6F8D00000A38030100000E0472EE34007072E1370070110E6FBB000006130F11067E6B000004110F6F8D00000A0E0472EE340070720B38007011106FBB000006131111067E6C00000411116F8D00000A037E800000046F9800000AA5110000021F5C2E1A037E800000046F9800000AA511000002200C020000408F00000011067E78000004198C250000016F8D00000A2B7B0E0472EE3400707231380070110E6FBB000006130F11067E6B000004110F6F8D00000A0E0472EE340070725D38007011106FBB000006131111067E6C00000411116F8D00000A11067E780000041A8C250000016F8D00000A2B2111067E6D000004037E820000046F9800000AA5130000018C130000016F8D00000A036FA100000A3AEDEFFFFF036FA200000A110716738800000A288C00000A2C1311067E4D00000411078C130000016F8D00000A092C25082100000000000000802E1911060E042858000006130411056F6900000A11046F8B00000A0E061105510E06506FA300000A172A00000003300300120A000000000000025072853800706FA400000A02506F5600000A7286070070720407007028A500000A6FA600000A166FA700000A02506F5600000A72AA070070725E06007028A500000A6FA600000A166FA700000A02506F5600000A725C0A0070720407007028A500000A6FA600000A166FA700000A02506F5600000A72D807007072EA06007028A500000A6FA600000A166FA700000A02506F5600000A7226080070727E06007028A500000A6FA600000A176FA700000A02506F5600000A7240080070727E06007028A500000A6FA600000A176FA700000A02506F5600000A7252080070727E06007028A500000A6FA600000A176FA700000A02506F5600000A7264080070727E06007028A500000A6FA600000A176FA700000A02506F5600000A7278080070727E06007028A500000A6FA600000A176FA700000A02506F5600000A72EA080070727E06007028A500000A6FA600000A176FA700000A02506F5600000A7234090070727E06007028A500000A6FA600000A176FA700000A02506F5600000A724A090070727E06007028A500000A6FA600000A176FA700000A02506F5600000A7258090070727E06007028A500000A6FA600000A176FA700000A02506F5600000A7262090070727E06007028A500000A6FA600000A176FA700000A02506F5600000A72FE090070727E06007028A500000A6FA600000A176FA700000A02506F5600000A72200A0070727E06007028A500000A6FA600000A176FA700000A02506F5600000A729B380070721E07007028A500000A6FA600000A176FA700000A02506F5600000A72A3380070721E07007028A500000A6FA600000A176FA700000A02506F5600000A72AD380070721E07007028A500000A6FA600000A176FA700000A02506F5600000A72BB380070721E07007028A500000A6FA600000A176FA700000A02506F5600000A72CF380070721E07007028A500000A6FA600000A176FA700000A02506F5600000A72E3380070721E07007028A500000A6FA600000A176FA700000A02506F5600000A72FD380070721E07007028A500000A6FA600000A176FA700000A02506F5600000A721B390070721E07007028A500000A6FA600000A176FA700000A02506F5600000A723B390070721E07007028A500000A6FA600000A176FA700000A02506F5600000A725F390070721E07007028A500000A6FA600000A176FA700000A02506F5600000A7289390070721E07007028A500000A6FA600000A176FA700000A02506F5600000A72B3390070721E07007028A500000A6FA600000A176FA700000A02506F5600000A72E3390070724006007028A500000A6FA600000A176FA700000A02506F5600000A72F939007072EA06007028A500000A6FA600000A176FA700000A02506F5600000A728E090070727E06007028A500000A6FA600000A176FA700000A02506F5600000A726C090070727E06007028A500000A6FA600000A176FA700000A02506F5600000A72B8090070727E06007028A500000A6FA600000A176FA700000A02506F5600000A720F3A0070720407007028A500000A6FA600000A166FA700000A02506F5600000A72E2090070727E06007028A500000A6FA600000A176FA700000A02506F5600000A72253A0070721E07007028A500000A6FA600000A166FA700000A02506F5600000A723D3A0070727E06007028A500000A6FA600000A176FA700000A02506F5600000A725B3A0070720407007028A500000A6FA600000A176FA700000A02506F5600000A72BE070070720407007028A500000A6FA600000A176FA700000A02506F5600000A726F3A0070727E06007028A500000A6FA600000A176FA700000A02506F5600000A727C360070721E07007028A500000A6FA600000A176FA700000A02506F5600000A72913A0070721E07007028A500000A6FA600000A176FA700000A02506F5600000A728C080070727E06007028A500000A6FA600000A176FA700000A02506F5600000A729E070070725E06007028A500000A6FA600000A176FA700000A02506F5600000A723E0A0070721E07007028A500000A6FA600000A176FA700000A02506F5600000A726E0A0070721E07007028A500000A6FA600000A176FA700000A02506F5600000A727E0A0070721E07007028A500000A6FA600000A176FA700000A02506F5600000A72920A0070720407007028A500000A6FA600000A176FA700000A02506F5600000A72A40A0070721E07007028A500000A6FA600000A176FA700000A02506F5600000A72B80A0070721E07007028A500000A6FA600000A176FA700000A02506F5600000A72D00A0070721E07007028A500000A6FA600000A176FA700000A02506F5600000A72EC0A0070720407007028A500000A6FA600000A176FA700000A02506F5600000A72FC0A0070721E07007028A500000A6FA600000A176FA700000A02506F5600000A72A53A007072EA06007028A500000A6FA600000A176FA700000A02506F5600000A72F4070070721E07007028A500000A6FA600000A176FA700000A02506F5600000A72CA070070720407007028A500000A6FA600000A176FA700000A02506F5600000A7202090070727E06007028A500000A6FA600000A176FA700000A02506F5600000A72AE080070727E06007028A500000A6FA600000A176FA700000A02506F5600000A72CC080070727E06007028A500000A6FA600000A176FA700000A02506F5600000A72260800706F7500000A168C250000016FA800000A02506F5600000A72400800706F7500000A168C250000016FA800000A02506F5600000A72520800706F7500000A168C250000016FA800000A02506F5600000A72640800706F7500000A168C250000016FA800000A02506F5600000A72780800706F7500000A168C250000016FA800000A02506F5600000A72EA0800706F7500000A168C250000016FA800000A02506F5600000A72340900706F7500000A168C250000016FA800000A02506F5600000A724A0900706F7500000A168C250000016FA800000A02506F5600000A72580900706F7500000A168C250000016FA800000A02506F5600000A72620900706F7500000A168C250000016FA800000A02506F5600000A72FE0900706F7500000A168C250000016FA800000A02506F5600000A72200A00706F7500000A168C250000016FA800000A02506F5600000A728E0900706F7500000A168C250000016FA800000A02506F5600000A726C0900706F7500000A168C250000016FA800000A02506F5600000A72B80900706F7500000A168C250000016FA800000A02506F5600000A72E20900706F7500000A168C250000016FA800000A02506F5600000A723D3A00706F7500000A168C250000016FA800000A02506F5600000A726F3A00706F7500000A168C250000016FA800000A02506F5600000A728C0800706F7500000A168C250000016FA800000A02506F5600000A72020900706F7500000A168C250000016FA800000A02506F5600000A72AE0800706F7500000A168C250000016FA800000A02506F5600000A72CC0800706F7500000A168C250000016FA800000A02502A00001B3003007F00000017000011026FA900000A6F5600000A6F5900000A0B2B50076F5A00000A74450000010A066FAA00000A2C3C02066FAB00000A7E9B00000A2E2E02066FAB00000A75270000012C2002066FAB00000A74270000016F2000000A289C00000A2C080206146FAC00000A076F6300000A2DA8DE110775040000010C082C06086F6400000ADC2A0001100000020011005C6D001100000000133004004A01000018000011026FA900000A6F8E00000A0A06026F6A00000A6F2F00000A740600001B6FAD00000A067E48000004168C250000016F8D00000A067E4D000004168C250000016F8D00000A067E4E000004168C250000016F8D00000A067E4F000004168C250000016F8D00000A067E50000004168C250000016F8D00000A067E61000004168C250000016F8D00000A067E62000004168C250000016F8D00000A067E63000004168C250000016F8D00000A067E67000004168C250000016F8D00000A067E6A000004168C250000016F8D00000A067E65000004027E650000046F9F00000A6F8D00000A067E6D000004168C250000016F8D00000A027E65000004168C250000016F8D00000A0372EE34007072CF3A007072FF3A00706FBB0000060B067E6B000004076F8D00000A0372EE34007072073B007072333B00706FBB0000060C067E6C000004086F8D00000A062A00001B3004005C020000190000110E06737100000A51738000000A0A0672493B00700F0228AE00000A72693B0070288500000A6F8100000A2606726D3B00706F8100000A260672A33B00706F8100000A260672D93B00706F8100000A2606720F3C00706F8100000A260672453C00706F8100000A2606727B3C00706F8100000A260672B13C00706F8100000A260672E73C00706F8100000A2606721D3D00706F8100000A260672533D00706F8100000A260219330E0672893D00706F8100000A262B0C0672F53D00706F8100000A260672823E00706F8100000A2602130411044504000000020000002100000040000000590000002B6F0672F03E00700F0128AF00000A722A3F0070288500000A6F8100000A262B5006722E3F00700F0128AF00000A722A3F0070288500000A6F8100000A262B3106726A3F00700572B2340070288500000A6F8100000A262B180672AA3F00706F8100000A260672F43F00706F8100000A2606723E4000706F8100000A26066F5E00000A0E076F7C00000A0E0773B000000A0B076F8200000A722B4100701E6F8300000A178C2A0000026F8400000A076F8200000A72574100701E6F8300000A198C2A0000026F8400000A076F8200000A72854100701E6F8300000A1F098C2A0000026F8400000A076F8200000A72B34100701E6F8300000A1E8C2A0000026F8400000A0219333A076F8200000A72252000701A6F8300000A0E048C0B0000016F8400000A076F8200000A723B2000701A6F8300000A0E058C0B0000016F8400000A0773B100000A0C080E06506FB200000A26DE0A082C06086F6400000ADCDE0A072C06076F6400000ADC170DDE0526DE00162A092A414C0000020000002F0200000C0000003B0200000A000000000000000200000079010000CE000000470200000A0000000000000000000000080000004D0200005502000003000000020000010330010095010000000000001680430000041780440000041880450000041980460000041A80470000041B80480000041C80490000041D804A0000041E804B0000041F09804C0000041F0A804D0000041F0B804E0000041F0C804F0000041F0D80500000041F0E80510000041F0F80520000041F1080530000041F1180540000041F1280550000041F1380560000041F1480570000041F1580580000041F1680590000041F17805A0000041F18805B0000041F19805C0000041F1A805D0000041F1B805E0000041F1C805F0000041F1D80600000041F1E80610000041F1F80620000041F2080630000041F2180640000041F2280650000041F2380660000041F2480670000041F2580680000041F2680690000041F27806A0000041F28806B0000041F29806C0000041F2A806D0000041F2B806E0000041F2C806F0000041F2D80700000041F2E80710000041F2F80720000041F3080730000041F3180740000041F3280750000041F3380760000041F3480770000041F3580780000041F3680790000041F37807A0000041F38807B0000041F39807C0000041F3A807D0000042A00000003300100D10000000000000016807E00000417807F0000041880800000041980810000041A80820000041B80830000041C80840000041D80850000041E80860000041F0980870000041F0A80880000041F0B80890000041F0C808A0000041F0D808B0000041F0E808C0000041F0F808D0000041F10808E0000041F11808F0000041F1280900000041F1380910000041F1480920000041F1580930000041F1680940000041F1780950000041F1880960000041F1980970000041F1A80980000041F1B80990000041F1C809A0000041F1D809B0000041F1E809C0000042A1E027B140300042A2202037D140300042A1E027B220300042A2202037D220300042A1E027B150300042A2202037D150300042A1E027B1D0300042A2202037D1D0300042A1E027B1E0300042A2202037D1E0300042A1E027B1F0300042A2202037D1F0300042A1E027B160300042A2202037D160300042A1E027B170300042A2202037D170300042A4A027B18030004027B1903000428A000000A2A1E027B180300042A2202037D180300042A1E027B190300042A2202037D190300042A1E027B1A0300042A2202037D1A0300042A1E027B1B0300042A2202037D1B0300042A1E027B1C0300042A2202037D1C0300042A1E027B200300042A2202037D200300042A1E027B210300042A2202037D210300042A1E027B240300042A2202037D240300042A1E027B230300042A2202037D230300042A4A0273A20000067D2303000402281100000A2A1E027B250300042A2202037D250300042A1E027B260300042A2202037D260300042A1E027B270300042A2202037D270300042A1E027B280300042A2202037D280300042A1E027B290300042A2202037D290300042A1E027B2A0300042A2202037D2A0300042A1E027B2B0300042A2202037D2B0300042A1E027B2C0300042A2202037D2C0300042A1E027B2D0300042A2202037D2D0300042A1E027B2E0300042A2202037D2E0300042A1E027B300300042A2202037D300300042A1E027B310300042A2202037D310300042A1E027B2F0300042A2202037D2F0300042A1E027B320300042A2202037D320300042A1E027B330300042A2202037D330300042A1E027B340300042A2202037D340300042A1E027B350300042A2202037D350300042A1E02281100000A2A1E027B7B0300042A2202037D7B0300042A1E027B7C0300042A2202037D7C0300042A1E027B7D0300042A2202037D7D0300042A1E027B7E0300042A2202037D7E0300042A1E027B7A0300042A2202037D7A0300042A1E027B7F0300042A2202037D7F0300042A1E02281100000A2A0013300400FB0000001A0000110372E341007028B500000A2C130572E341007028B500000A2C0672E34100702A020B07450200000043000000020000002B41056F2000000A0A0E0472E3410070281C00000A2C1306722A3F00700E046F2000000A288500000A0A0672E5410070036F2000000A288500000A0A066F2000000A2A0E0572EB4100707215420070176FB60000062D0772E341007010040E0572EB4100707221420070176FB60000062D0772E34100701002036F2000000A722A3F007028B600000A0A04289C00000A2D1206046F2000000A722A3F0070288500000A0A06056F2000000A722A3F0070288500000A0A060E046F2000000A28B600000A0A066F2000000A2A001B300600DA0000001B00001173BE0000060A738000000A0B07722D4200706F8100000A260772D64200706F8100000A260772DE4200706F8100000A26076F5E00000A026F7C00000A0273B000000A0C086F7F00000A0D2B250609166F9900000A72D343007009176F9900000A288500000A09186F9900000A6FB700000A096FA100000A2DD3096FB800000A262B2A0672D743007009166F9900000A72D343007009176F9900000A28B900000A09186F9900000A6FB700000A096FA100000A2DCEDE0A092C06096F6400000ADCDE0A082C06086F6400000ADC061304DE0626141304DE0011042A00000128000002004A006CB6000A00000000020043007FC2000A0000000000000000D1D10006020000011330030020000000080000110372D343007004288500000A0A0206120128BA00000A2D067E1B00000A0B072A13300300310000001200001102030428B20000060A06289C00000A2C02052A06120128BB00000A2D02052A070E042F030E040B070E0531030E050B072A5202030416200000008020FFFFFF7F28B30000062A5202030405200000008020FFFFFF7F28B30000062A5A020304052D03162B011728B500000616FE0116FE012A2A0203041628B60000062A00000013300400350000001C00001102030428B20000060A06289C00000A2C02052A0620FF01000072ED43007028BC00000A6FBD00000A120128BE00000A2D02052A072A3E02030416738800000A28B80000062A3A0203047E1B00000A28BB0000062A13300300150000000700001102030428B20000060A06289C00000A2C02050A062A00000013300300230000001D00001102030428B20000060A06289C00000A2C03056A2A06120128BF00000A2D03056A2A072A2A0203041628BC0000062A1E0228C000000A2A00001B300600C50000001E00001173C00000060A738000000A0B0772F94300706F8100000A260772354400706F8100000A260772654400706F8100000A2607728F4400706F8100000A260772C34400706F8100000A260772134500706F8100000A26076F5E00000A026F7C00000A0273B000000A0C086F7F00000A0D2B270609166F9700000A09176F9A00000A09186F9A00000A09196F9700000A73C10000066FC100000A096FA100000A2DD1DE0A092C06096F6400000ADCDE0A082C06086F6400000ADC061304DE0626141304DE0011042A0000000128000002006E0033A1000A000000000200670046AD000A0000000000000000BCBC0006020000011E0228C200000A2A7202281100000A02037D8703000402047D8803000402057D890300042A1E02281100000A2A00000042534A4201000100000000000C00000076322E302E35303732370000000005006C000000C0450000237E00002C460000B056000023537472696E677300000000DC9C00004C4500002355530028E2000010000000234755494400000038E200004C0D000023426C6F620000000000000002000001579FA2290902000000FA253300160000010000005D000000450000008B030000C2000000C600000002000000C2000000C302000015000000030000001E000000070000004200000076000000060000000100000001000000020000000800000000000A000100000000000600DE04D7040600E304D70406000705EA0406001805D70406002405EA0406003705D70406005C0541050600C908EA040600F808EA0406001509EA040600430CD7040A00780C6C0C0A00980C6C0C0A00760D600D0A00840D600D0A00990D600D0A00A70D600D0A00E10D6C0C0600683FD70406002A4B0B4B06003F4D2D4D0600564D2D4D0600734D2D4D0600924D2D4D0600AB4D2D4D0600C44D2D4D0600DF4D2D4D0600FA4D2D4D0600134E0B4B0600274E0B4B0600354E2D4D06004E4E2D4D06007E4E6B4E8700924E00000600C14EA14E0600E14EA14E0600FF4ED7040600054FD70406000B4FD7040600194FD7040A00494F2E4F06005F4FD7040600704FD7040600AE4FD7040600CE4FC44F0600DB4FEA040600E84FC44F0600EF4FEA0406002E50D70406007850A14E0600C450A14E0600D350D7040600D950D70406004A07D70406001751D70406002F51D70406004551D70406006751D70406008851EA0406009F51D7040600B551EA040600CE51EA040600E451D7040A0002526C0C0A0023526C0C0600485241050A004F522E4F06006E525B520A0094526C0C0600AC52D7040A00EC522E4F0A00FA522E4F0A0005532E4F0A0027536C0C0A00AA5397530600DF53D7040600455439540A005E54600D0A008454600D0A00915497530A00B65497530A00D054975306003C55D70406004455D7040A00D655600D0A00E55597530600F8550B4B06000E560B4B06001956D704060054563F5606006F563F56060091563F5606009E56D7040000000001000000000001000100010100001C0028000500010001000100100033002800090010000100000110003B002800090017001400810010005B00280015001D001D00010110006700280014001D002E00010110007E00280014001E003200810110009F00280009002000480002010000B0000000050038005A0002010000BC00000005003D005A000A011000CC000000190043005A000B011000D500000019007E005B0001010000DC00280005009D005C0001010000E60028000500A3005C0001010000000128000500AC005C0001010000150128000500B9005C0001010000220128000500C2005C000101000033012800050090015C000101000040012800050005025C000101000050012800050012025C00010100005C012800050019025C00010100006901280005001D025C00010100007B012800050029025C00010100008F01280005002D025C0001010000A5012800050032025C0001010000BE012800050037025C0001010000DB01280005003A025C0001010000EE012800050047025C0001010000FC012800050055025C000101000009022800050060025C000101000018022800050064025C00010100002C02280005006A025C00010100004002280005006E025C000101000056022800050072025C000101000070022800050092025C00010100007B0228000500A3025C0001010000910228000500A8025C0001010000A80228000500AD025C0001010000C00228000500B3025C0001010000D50228000500B9025C0001010000E50228000500BE025C0001010000F30228000500EF025C00010100000603280005000D035C000101000011032800050011035C000100100020032800090014035C000100100037032800090025038000010100004B03280005003603A300010100005E03280005003F03A300010100007303280005004703A300010100008D03280005004B03A30001010000A403280005005003A30001010000B603280005005603A30001010000C903280005005F03A30001010000D503280005006303A30001010000E203280005006903A30001010000EF03280005006E03A300010100000004280005007203A300010100000D04280005007503A300010010002604280009007A03A300010100004404280005008003B000010100004D04280005008303B000810110006404280009008603B000810110007404280009008603B100020010008104000006008603B100020010008C0400000A008703BF0002001000A704000009008703C10001001000B904C00409008A03C200000000003350000009008A03C300130100009350000019008C03C300060669051C00568071051F00568079051F00568082051F00568087051F0056808F051F00568098051F005680AF051F005680CC051F005680E1051F005680E9051F005680FA051F00568008061F00568017061F00568027061F0031003106690031003D066900310048061C00040066067100040076061C00040080061C00040087067400010065076900010073071C0001007E07690001008F07EA0001009607EA0001009D07ED0001009509ED000100C109660101009D07ED005180040A1C005180180A1C0051802E0A1C005180480A1C0051805D0A1C005180710A1C005180850A1C005180920A1C005180A00A1C005180AE0A1C005180BF0A1C005180CD0A1C005180DA0A1C005180EE0A1C0051800B0B1C005180290B1C005180490B1C005180660B71005180730B71005180870B710051809A0B71005180B50B71005180BF0B1C005180CE0B2202060669051C0056802E0EDB0256803B0EDB0256804D0EDB025680600EDB02060669051C0056806E0EDF025680780EDF025680820EDF0256808A0EDF025680930EDF0236009B0E1C003600A80E1C003600B10E1C003600BC0E1C003600CB0E1C003600D90E1C003600E80E1C003600F70E1C003600040F1C003600110F1C003600200F1C003600320F1C0036003F0F1C003600540F1C003600690F1C003600780F1C003600870F1C003600920F1C0036009E0F1C003600AA0F1C003600B20F1C003600B70F1C003600BB0F1C003600CF0F1C003600E40F1C003600F90F1C0036000A101C00360018101C00360025101C0036002E101C0036003A101C00360048101C00360057101C00360068101C00360078101C0036008D101C0036009A101C003600A8101C003600B3101C003600B9101C003600CD101C003600DE101C003600ED101C00360007111C00360017111C0036002B111C0036003B111C0036004D111C00360060111C00360070111C00360082111C00360097111C003600AA111C003600BF111C003600D9111C003600F4111C003600FB111C00360015121C00360026121C0036009B0E1C003600A80E1C00360037121C0036003C121C00360047121C003600B10E1C0036009E0F1C00360052121C00360056121C0036005B121C0036006A121C00360076121C00360081121C003600E40F1C0036008C121C00360099121C003600A7121C003600BD121C00360025101C003600D3121C003600E9121C003600FF121C00360068101C0036008D101C00360015131C00360024131C00360038131C00360007111C00360017111C0036002B111C0036003B111C00060669051C0056804D13E30256805213E30256806613E30256808413E30256809C13E302060669051C005680B613E7025680BE13E7025680C213E7025680C613E7025680D113E7025680D713E7025680DC13E7025680E113E702060669051C005680B613EB025680E513EB025680ED13EB025680F413EB025680FE13EB0256800614EB0256800F14EB0256801B14EB0256802114EB0256803514EB0256803F14EB0256805314EB02060669051C0056805F14EF0256806C14EF0256807114EF0256807E14EF0256808B14EF0256809614EF0256809F14EF025680B014EF02060669051C0056805F14F8025680BA14F8025680C714F8025680D514F8025680DF14F8025680EA14F8025680F214F8025680FB14F80256800915F80256802215F80256802915F80256803915F80256804A15F80256806015F80256807715F80256808215F80256809015F80256809E15F8025680B215F8025680C015F8025680CD15F8025680E115F8025680F315F80256800416F80256801616F80256801E16F80256803316F80256804216F8025680E80EF80256805E16F80256807016F8025680D90EF80256807B16F80256808616F80256809816F8025680A616F8025680B416F8025680D016F8025680DD16F8025680EB16F80256800017F80256801317F80256802617F80256803B17F80256805317F80256806D17F80256808517F8025680A017F8025680B017F8025680C217F8025680D417F8025680E417F8025680FE17F80256801618F80256802418F80256803D18F80256805218F80256806918F80256808218F80256809918F8025680AE18F8025680CA18F8025680DA18F8025680F118F80256805710F80256804810F80256800F19F80256801C19F80256803719F80256804519F80256805A19F80256806A19F80256807C19F80256809519F8025680B319F8025680D019F8025680F519F80256801B1AF8025680411AF80256805A1AF8025680781AF8025680951AF8025680AF1AF8025680CE1AF8025680E41AF8025680FA1AF80256800F1BF8025680231BF8025680371BF80256804F1BF8025680631BF8025680741BF8025680891BF80256809D1BF8025680B11BF8025680D31BF8025680001CF8025680241CF8025680551CF8025680691CF80256808D1CF8025680B11CF8025680D61CF8025680FF1CF80256801E1DF8025680441DF80256806A1DF80256808E1DF8025680B21DF8025680D31DF8025680F41DF8025680091EF80256802E1EF80256805F1EF80256808B1EF8025680BB1EF8025680E51EF80256800F1FF8025680411FF80256806A1FF8025680931FF8025680A61FF8025680BA1FF8025680CD1FF8025680E01FF80256800520F80256801F20F80256803A20F80256804D20F80256807220F80256809620F8025680BB20F8025680DF20F8025680FE20F80256801D21F80256803B21F80256805821F80256806B21F80256807D21F80256809A21F8025680AD21F8025680BF21F8025680D121F8025680E421F8025680F321F80256800322F80256801622F80256802122F80256803022F80256804F22F80256806022F80256807522F80256808522F80256809622F8025680AE22F8025680CB22F8025680E522F8025680F922F80256801223F80256803123F80256804423F80256805923F80256806823F80256807E23F80256808823F8025680B023F8025680D823F80256800724F80256803624F80256806424F80256809224F8025680A624F8025680C924F8025680EC24F80256800C25F80256802C25F80256805525F80256807E25F8025680A625F8025680CD25F8025680EA25F80256800526F80256801B26F80256803D26F80256805926F80256807426F80256809126F8025680A826F8025680C526F8025680E126F8025680FF26F80256801927F80256803427F80256804F27F80256806F27F80256809B27F8025680C127F8025680E627F80256800D28F80256802E28F80256805528F80256807B28F8025680A328F8025680B928F8025680D028F802060669051C005680E828AD065680ED28AD065680F428AD065680FC28AD0656800729AD0656801329AD0656802429AD0656803329AD0656803C29AD0656804729AD0656805E29AD0656806829AD0656807329AD0656808729AD0656809329AD065680A029AD065680B229AD065680BF29AD065680CB29AD065680DD29AD065680ED29AD065680FC29AD065680182AAD065680282AAD0656808705AD065680382AAD0656804C2AAD0656805D2AAD065680732AAD065680812AAD0656809B2AAD065680A92AAD065680BD2AAD065680D32AAD065680E62AAD065680F42AAD0656800C2BAD0656801F2BAD065680322BAD0656803E2BAD065680692BAD0656807C2BAD065680892BAD065680952BAD065680AC2BAD065680CD2BAD065680DE2BAD065680EF2BAD0656800C2CAD0656801E2CAD065680332CAD065680462CAD065680532CAD065680672CAD065680812CAD065680902CAD065680A52CAD065680BE2CAD065680D82CAD065680E82CAD065680F62CAD065680092DAD0656801D2DAD065680392DAD065680492DAD065680652DAD0656807C2DAD065680922DAD065680A12DAD065680B22DAD065680BF2DAD065680D12DAD065680E22DAD065680EC2DAD065680032EAD0656801B2EAD065680272EAD065680452EAD065680612EAD065680832EAD065680A22EAD065680CB2EAD065680EC2EAD065680FF2EAD0656801C2FAD065680392FAD0656804C2FAD065680782FAD065680992FAD065680BA2FAD065680D82FAD065680F62FAD0656801830AD0656804430AD0656806A30AD0656809230AD065680BD30AD065680E330AD0656801031AD0656803531AD0656805A31AD0656807431AD0656808C31AD065680A731AD065680B631AD065680C931AD065680E131AD065680F431AD0656801232AD0656802A32AD0656804132AD0656805A32AD0656806F32AD0656808932AD065680A232AD065680BD32AD06060669051C0056804D13FC065680DC32FC065680EA32FC065680EE32FC065680F532FC0656800F33FC0656801F33FC0656803633FC0656804533FC0656805A33FC0656805F33FC0656807533FC06060669051C005680893300075680993300075680AF3300075680BB3300075680CA3300075680E2330007060669051C0056804D1304075680EA3204075680EE320407060669051C005680FB33080756800234080756800934080756801334080756802034080756802A34080756803934080756804E340807568059340807568063340807568076340807060669051C005680FB330C07568002340C0756808B340C07060669051C005680E105100756809034100756809534100756809C341007060669051C005680E10514075680A13414075680A63414075680AE341407060669051C005680B33418075680BB341807060669051C0056804D131C075680DA341C075680E7341C075680F3341C07568002351C0756800B351C0756801C351C07568023351C07568033351C0756804A351C0756805F351C07568078351C07060669051C005680B61352075680EA32520756808535520756808A3552075680933552075680983552075680A53552075680B13552075680C13552075680C83552075680D13552075680DF3552075680F1355207060669051C005680F935560756800B3656075680133656075680223656075680983556075680293656075680B613560756803D365607568056365607568065365607060669051C005680ED136907568073366907568082366907060669051C0056808A366D0756809A366D075680B4366D075680C7366D075680D8366D07060669051C005680B61371075680EA3671075680F4367107060669051C005680B613760756800037760756800A377607060669051C005680E1057B07568017377B0756802D377B07568043377B07568056377B0756806D377B07568081377B07568094377B075680AE377B075680C8377B075680E1377B075680F8377B0756800D387B0756801F387B07568038387B0756804B387B0756805E387B0756806E387B0756807A387B07568086387B07568096387B075680AF387B075680C6387B075680DD387B075680F4387B07568007397B0756802A397B07568037397B0756804D397B07568061397B07568070397B07060669051C0056808339800756808A3980075680993980075680A83980075680BD3980075680C73980075680DB3980075680ED3980075680FE39800756800D3A80075680173A80075680283A80075680353A80075680403A80075680563A80075680633A8007060669051C005680753A85075680813A85075680913A85075680B23A8507060669051C005680CE3A8A075680D33A8A075680DA3A8A075680E73A8A07060669051C0056804D138F075680F73A8F075680FD3A8F075680073B8F075680133B8F07060669051C0056801E3B94075680273B94075680F73A940756802C3B94075680373B9407060669051C0056803C3B99075680493B99075680523B99075680F3159907060669051C005680693B9E075680EA149E075680F2149E075680713B9E07568077159E0756807B3B9E075680883B9E075680953B9E075680A33B9E075680AF3B9E075680C03B9E075680BA1F9E075680A61F9E07568016229E075680DA3B9E07568021229E075680AE229E0756803C3B9E075680F33B9E075680FE3B9E0756800B3C9E0756801B3C9E075680363C9E075680553C9E075680643C9E075680793C9E0756808A3C9E075680A23C9E07568013179E075680B63C9E075680D63C9E075680E13C9E075680EE3C9E075680033D9E07568016169E0756801E169E075680223D9E075680353D9E075680433D9E075680503D9E0756805D3D9E075680743D9E075680963D9E075680AB3D9E075680EA259E075680F3159E075680C13D9E075680CF3D9E07060669051C0056808339B2075680E23DB2075680EB3DB2075680F43DB2075680FE3DB2075680073EB2075680173EB20756802B3EB2075680353EB20756803F3EB2075680483EB2075680543EB2075680683EB2075680743EB2075680803EB2075680933EB20756809D3EB2075680A73EB2075680B43EB2075680C53EB2075680D83EB2075680EC3EB2075680F83EB2075680073FB2075680173FB2075680293FB2075680182AB2075680563AB2075680633AB207060669051C0056803A3FB70756803F3FB7075680463FB707060669051C005680543FBC0756805C3FBC070100703FC10701007C3F710001008A3FC1070100993FC1070100A53FC1070100B93FC1070100CD3FC1070100DA3FC1070100E73FC1070100F93FC107010007401C0001001640ED0001002D40ED0001004640940701005040740001006740C50701007F408F070100BE437400010080068F070100CD4371000100DF4371000100EB4371000100F9431C000100084471000100703FC10701001A44710001002A44710001004144ED0001005444710001006B44710001008244710001008C441D0801009D4499070100B0447100060669051C0056809147440856809547440856809B4744085680A04744085680A54744085680AA4744085680AF4744085680B4474408060669051C005680954749085680713B49085680BA47490856800B3649085680CF4749085680D84749085680E4474908060669051C005680F0474E085680F8474E085680FC474E08060669051C0056803A3F53085680FF47530856800C485308568019485308060669051C005680F047580856801E48580856803248580856803A485808568041485808060669051C00568052485D0856805A485D08568062485D08568068485D08568070485D08568078485D08568080485D08568087485D08060669051C0056808F48620856809848620856809D486208060669051C005680B61367085680A24867085680AE4867085680C84867085680D1486708060669051C005680E1056C085680EA486C085680FC486C08568007496C08060669051C0056801C497108568021497108568026497108060669051C0056802D497608568035497608060669051C0056803E497B08568046497B08568052497B0856806A497B0801008149740001008E491C0001009949C1070100A24971000100AD499407010082447100060669051C005680854A80085680EE328008060669051C005680F047850856808C4A85085380A84A71000600ED4AED000600F64AED000600FC4A1C001301B050C10913007753BE0A502000000000841860066D000100582000000000C60093067700010092200000000086089C06770001009A20000000008608AD067B000100A220000000008608B8067F000100AA20000000008608C10684000100B220000000008608CE0677000100D4200000000086186006880001000C21000000009600DF068E000200B821000000009600E606970005006C22000000009600EF069F0007008C220000000096000707A4000800A4220000000096000707A9000900EA220000000081186006B0000B0010230000000091000F07B8000F0080230000000091001607C1001300E8230000000091001D07CC00170078240000000091003007D100180050250000000091182750BD091B008F250000000086186006F0001B00C72500000000E609A507F6001C00CA2500000000E609BB07F6001C00CD2500000000E609DA077B001C00D02500000000E609ED077B001C00D42500000000E6010108FA001C00042700000000E601100805012100442700000000810024086D002400D42700000000E60129086D0024002C2800000000841860066D0024003C2800000000C6087D087B0024003F2800000000C6088B0812012400502800000000C60899087B002500572800000000C608AA08120125005E2800000000C608BB0817012600662800000000C608C208F00026007C2800000000C608D2081C0127009C2800000000C608E6081C012700C02800000000C608030922012700C32800000000C6080C0927012700D42800000000C60821092D012800D72800000000C6082D0932012800E82800000000C60039096D002900EC2800000000C60044096D0029001B29000000009600DF06380129002729000000009600DF063D012900652900000000861860066D002A00702900000000C600A10958012A00DD2900000000C600B10958012C00E72900000000C400290861012E00F72900000000861860066D002F000A2A00000000C6087D087B002F00172A00000000C6088B0812012F00342A00000000C60899087B003000412A00000000C608AA08120130004F2A00000000C608BB08170131005C2A00000000C608C208F00031006A2A00000000C608D00917013200772A00000000C608D809F0003200852A00000000C608E0097B003300922A00000000C608EC0912013300A02A00000000C608D2081C013400AD2A00000000C608E6081C013400BA2A00000000C608030922013400C72A00000000C6080C0927013400D52A00000000C60821092D013500E22A00000000C6082D0932013500F02A00000000C60039096D003600FD2A00000000C60044096D0036000C2B00000000C600A10958013600852B00000000C600B109580138008F2B00000000810029086D003A00C02B000000009600DE0B28023A00D92B000000009600FD0B2D023B00F82B000000009600200C33023D005C2C0000000096004C0C38023E00702C000000009100820C40024000242E000000009100A20C46024100272F000000009600AF0C4C024200312F000000009600D40C540244003B2F000000009600FD0C5D024700452F000000009600260D65024900502F0000000091004C0D70024C0010330000000091004C0D7D02520020340000000091008F0D8F025A00243E000000009100B60D9E026100F44E000000009100C30DB10268001459000000009100E90DB9026900B059000000009100FA0DBF026A00085B000000009100100EC9026C00BC5D0000000091182750BD097400605F0000000091182750BD0974003D600000000086088940CA07740045600000000086089640CF0774004E60000000008608A340840075005660000000008608BA40D50775005F60000000008608D140770076006760000000008608E440DA0776007060000000008608F740CA07770078600000000086080641CF077700816000000000860815417B007800896000000000860822411201780092600000000086082F41F60079009A60000000008608464161017900A3600000000086085D41CA077A00AB600000000086086D41CF077A00B4600000000086087D41CA077B00BC600000000086088B41CF077B00C5600000000086089941CA077C00D860000000008608A741CA077C00E060000000008608BB41CF077C00E960000000008608CF41CA077D00F160000000008608E341CF077D00FA60000000008608F741CA077E0002610000000086080542CF077E000B610000000086081342CA077F0013610000000086082142CF077F001C610000000086082F42CA07800024610000000086084242CF0780002D610000000086085542F600810035610000000086086E42610181003E610000000086088742DF07820046610000000086089242E50782004F610000000086089D42EC0783005761000000008608A942F20783006061000000008608B542F90784006861000000008608CD42FF078400716100000000861860066D0085008461000000008608C644840085008C61000000008608D644D50785009561000000008608B806EC0786009D61000000008608E644F2078600A661000000008608EF4477008700AE610000000086080245DA078700B761000000008608154577008800BF610000000086082245DA078800C8610000000086082F4577008900D0610000000086083E45DA078900D9610000000086084D457B008A00E1610000000086085D4512018A00EA610000000086086D4577008B00F2610000000086087B45DA078B00FB610000000086088945CA078C0003620000000086089645CF078C000C62000000008608A34577008D001462000000008608B445DA078D001D62000000008608C54577008E002562000000008608D845DA078E002E62000000008608EB4577008F003662000000008608FD45DA078F003F620000000086080F467700900047620000000086082146DA07900050620000000086083346F60091005862000000008608424661019100616200000000860851467700920069620000000086085E46DA07920072620000000086086B46210893007A62000000008608794626089300836200000000860887462C0894008B620000000086089B46320894009462000000008608AF46770095009C62000000008608C546DA079500A56200000000861860066D009600AD62000000008608BD497B009600B562000000008608CA4912019600BE62000000008608D749CA079700C662000000008608E249CF079700CF62000000008608ED4977009800D762000000008608F949DA079800E062000000008608054ADF079900E862000000008608164AE5079900F162000000008608274A84009A00F962000000008608354AD5079A000263000000008608434A77009B000A630000000086084F4ADA079B00136300000000861860066D009C001C63000000009600994A8A089C002464000000009600DF06AB08A2003465000000008600B54AB308A3006065000000008600BB4AB908A5009D65000000008600BB4AC208AA00B265000000008600BB4AC808AC00C765000000008600C44ACF08AF00DE65000000008600C44AD608B200EC65000000008600CF4ADC08B4002D66000000008600CF4AE508B7003D66000000008600DA4AB308B9004C66000000008600DA4AEC08BB007066000000008600E44AF308BE009F66000000008600E44AFA08C100AA6600000000861860066D00C300B466000000009600DF060009C300B06700000000861860066D00C400B86700000000861860060809C400D56700000000861860066D00C700000001003300000001004A07000002004F0700000300430700000100360702000200064B00000100360700000100374B00000100374B020002004B4B000001004307000002004A07000003004F07000004003607000001004307000002004A07000003004F0702000400574B00000100574B020002004307020003004A07020004004F0700000100624B00000100734B000002007B4B02000300814B00000100F80900000100884B00000200944B00000300A04B00000400AB4B00000500B84B00000100884B00000200944B00000300A04B00000100C54B00000100C54B00000100C54B00000100C54B00000100C54B00000100CB4B00000100D34B00000200DA4B00000100D34B00000200DA4B00000100E04B00000100C54B00000100C54B00000100C54B00000100C54B00000100C54B00000100C54B00000100C54B00000100D34B00000200DA4B00000100D34B00000200DA4B00000100DB4600000100DB4600000200EA4B00000100F74B00000100044C000002000D4C00000100144C000001004A0700000100DB4602000200184C00000100DB4600000200EA4B02000300184C00000100F74B02000200184C00000100044C000002000D4C02000300184C00000100274C00000200DB4600000300EA4B00000400F74B00000500324C000006003B4C00000100274C00000200DB4600000300EA4B00000400F74B00000500324C000006003B4C00000700424C02000800484C00000100274C00000200DB4600000300EA4B00000400F74B00000500324C000006003B4C00000700840D00000100EA4B000002005B4C00000300654C00000400724C00000500740400000600834C02000700874C00000100944C00000100AA4C00000100AE4C00000200740400000100274C00000200DB4600000300EA4B00000400F74B00000500324C000006003B4C02000700BA4C00000800834C00000100C54B00000100C54B00000100C54B00000100C54B00000100C54B00000100C54B00000100C54B00000100C54B00000100C54B00000100C54B00000100C54B00000100C54B00000100C54B00000100C54B00000100C54B00000100C54B00000100C54B00000100C54B00000100C54B00000100C54B00000100C54B00000100C54B00000100C54B00000100C54B00000100C54B00000100C54B00000100C54B00000100C54B00000100C54B00000100C54B00000100C54B00000100C54B00000100C54B00000100C54B00000100C54B00000100C54B00000100C54B00000100C54B00000100C54B00000100C54B00000100C94C00000200D64C00000300DB4C00000400E64C00000500F04C00000600740400000100834C00000100FA4C00000200034D00000100FA4C00000200034D000003000E4D000004001B4D00000500244D00000100FA4C00000200034D00000100FA4C00000200034D000003000E4D00000100FA4C00000200034D000003000E4D00000100FA4C00000200034D00000100FA4C00000200034D000003000E4D00000100FA4C00000200034D00000100FA4C00000200034D00000100FA4C00000200034D000003000E4D00000100FA4C00000200034D000003000E4D00000100FA4C00000200034D00000100834C00000100ED4A00000200F64A00000300FC4A04000D0004001100A10060066D00A9006006DA07B1006006DA07B9006006DA07C1006006DA07C9006006DA07D1006006DA07D9006006DA07E1006006DA07E90060066101F1006006DA07F9006006DA0701016006DA07090160060F09190160061201210160066D00110060066D003901124F16093901124F230941016006DA073901124F2A093901234F7B00490160066D005101E606A9003901664F49095901754F4E0939017D4F71003901834F5E093901914F640929019B4F9F0031019B4F6A093901A14F77003901A64F74093901914F7A096101BB4F7F092900EC0912012900A10958012900B1095801690160066D00710160069109790100509D09710106506D0069011650170161011E50A509910160066D009901EC50C609A101FC50D50911000251DC09B1010A517700B9016006DA07C1016006E209C9016006DA07A1016151F509D1016A51FE09290060066D0029007B511C00D9016006DA07E10160066D0041006006030AE90160066D002900E0097B00F101BB4FF0002900D809F0003901F551180A29002908610129007D087B0029008B081201290099087B002900AA0812012900BB0817012900C208F0002900D00917012900D2081C012900E6081C0129000309220129000C092701290021092D0129002D093201290039096D00290044096D00B9016006E209290061516D0059001B4D1D085900244D1D083901FC51210A610017522F0A09023E527B0024006006120109027A523E0A21028852D50929029F52DC09B101BE52440A2902D0527700110093067700190260064D0A2400DF52550A190260065B0A19026006640A2102E352F600210029086D00240016506B0A39026006710A41020D53790A490216537F0A61003953860A910042538C0A39025053910A490259537F0A490268536D002C00600612012C00DF52CD0A2C008B53D50A610060066D0071006006DA075902B7536D005902BC536D000102C253E30A2902CB5312010102D65312016102E453F20A6102E853F20A7100EC53F80A790060066D008900FD53FD0A79000C54020B79001B54080B79002B540E0B690260066D0069025354220B79007554290B7102DF522F0B81029D54380B3901A7543D0B3901AE54B3088902C054DA079900600612019102DD54500B9102E44A550B5102DF525A0B9900834F600B9100E654910A6100EF54680B9102F6546D0B590002557B0059000B557B00590015557B0059001D557B00590026557B00590031557B0059006006730B9102BB4A7D0B9102C253820B9102DA4A7A099102C44A500BA102B54A870B39014B55A400610059558C0B9100C253930B9100C253820B99006055980B91026C55F6009102BC536D00610071556D0061007F55DA07B1010251C50B0102DF52CC0B29028D55610129029D55380B9100AE55D60B2902B855F6009100C253DB0B9100E654E20B9100C855F80B29019306770031019306770079006006050CA90260060E0CB102F355140CB9026006280CC90260066D00390128565E093901A7542F0C0C00DF52CD0A91023456F6003901A7543C0C0C008B53D50A2901E606540CD10260565B0CD1028056620C9900E606680C3101E6067C0C0C0060066D001400DF52CD0A140060066D0008000800230008000C002800080010002D0008001400320008001800370008001C003C00080020004100080024004600080028004B0008002C005000080030005500080034005A00080038005F0008003C006400080080002300080084002800080088002D0008008C003200080090003700080094006A01080098006F0108009C0074010800A00079010800A4007E010800A80083010800AC0088010800B0008D010800B40092010800B80097010800BC009C010800C000A1010E00C400A6010E00C800C9010E00CC00DE010E00D000EB010E00D4000C020800D8001D020300DC0025020800E40023000800E80028000800EC002D000800F00032000800F80023000800FC002800080000012D0008000401320008000801370008007802230008007C022800080080022D00080084023700080088027901080090022300080094022800080098022D0008009C0232000800A00237000800A4026A010800A8026F010800AC0274010800B40223000800B80228000800BC022D000800C00232000800C40237000800C8026A010800CC026F010800D00274010800D40279010800D8027E010800DC0283010800E00288010800E802F3020800EC0223000800F00228000800F4022D000800F80232000800FC023700080000036A0108000403830108000C03F302080010032300080014032800080018032D0008001C033200080020033700080024036A01080028036F0108002C037401080030037901080034037E0108003803830108003C038801080040038D0108004403920108004803970108004C03FC02080050030103080054030603080058030B0308005C031003080060031503080064031A03080068031F0308006C032403080070032903080074032E0308007803330308007C033803080080033D0308008403420308008803470308008C034C03080090035103080094035603080098035B0308009C0360030800A00365030800A4036A030800A8036F030800AC0374030800B00379030800B4037E030800B80383030800BC0388030800C0038D030800C40392030800C80397030800CC039C030800D003A1030800D403A6030800D803AB030800DC03B0030800E003B5030800E403BA030800E803BF030800EC03C4030800F003C9030800F403CE030800F803D3030800FC03D80308000004DD0308000404E20308000804E70308000C04EC0308001004F10308001404F60308001804FB0308001C040004080020040504080024040A04080028040F0408002C041404080030041904080034041E0408003804230408003C042804080040042D0408004404320408004804370408004C043C04080050044104080054044604080058044B0408005C045004080060045504080064045A04080068045F0408006C046404080070046904080074046E0408007804730408007C047804080080047D0408008404820408008804870408008C048C04080090049104080094049604080098049B0408009C04A0040800A004A5040800A404AA040800A804AF040800AC04B4040800B004B9040800B404BE040800B804C3040800BC04C8040800C004CD040800C404D2040800C804D7040800CC04DC040800D004E1040800D404E6040800D804EB040800DC04F0040800E004F5040800E404FA040800E804FF040800EC0404050800F00409050800F4040E050800F80413050800FC041805080000051D0508000405220508000805270508000C052C05080010053105080014053605080018053B0508001C054005080020054505080024054A05080028054F0508002C055405080030055905080034055E0508003805630508003C056805080040056D0508004405720508004805770508004C057C05080050058105080054058605080058058B0508005C059005080060059505080064059A05080068059F0508006C05A40508007005A90508007405AE0508007805B30508007C05B80508008005BD0508008405C20508008805C70508008C05CC0508009005D10508009405D60508009805DB0508009C05E0050800A005E5050800A405EA050800A805EF050800AC05F4050800B005F9050800B405FE050800B80503060800BC0508060800C0050D060800C40512060800C80517060800CC051C060800D00521060800D40526060800D8052B060800DC0530060800E00535060800E4053A060800E8053F060800EC0544060800F00549060800F4054E060800F80553060800FC055806080000065D0608000406620608000806670608000C066C06080010067106080014067606080018067B0608001C068006080020068506080024068A06080028068F0608002C069406080030069906080034069E0608003806A30608003C06A80608004406230008004806280008004C062D00080050063200080054063700080058066A0108005C066F01080060067401080064067901080068067E0108006C068301080070068801080074068D0108007806920108007C06FC02080080060103080084060603080088060B0308008C06100308009006150308009406B10608009806B60608009C061A030800A0061F030800A40624030800A80629030800AC062E030800B00633030800B40638030800B8063D030800BC0642030800C00647030800C4064C030800C80651030800CC0656030800D0065B030800D40660030800D80665030800DC066A030800E0066F030800E40674030800E80679030800EC067E030800F00683030800F40688030800F8068D030800FC069203080000079703080004079C0308000807A10308000C07A60308001007AB0308001407B00308001807B50308001C07BA0308002007BF0308002407C40308002807C90308002C07CE0308003007D30308003407D80308003807DD0308003C07E20308004007EC0308004407F10308004807F60308004C07FB03080050070504080054070A04080058070F0408005C07140408006007190408006407BB0608006807C00608006C072304080070072804080074072D0408007807780408007C078C04080080079104080084079604080088079B0408008C07A00408009007A50408009407AA0408009807AF0408009C07B4040800A007B9040800A407BE040800A807C3040800AC07C8040800B007CD040800B407D2040800B807D7040800BC07DC040800C007E1040800C407E6040800C807EB040800CC07F0040800D007F5040800D4076D050800D80772050800DC0777050800E0077C050800E407C5060800E807CA060800EC07CF060800F007D4060800F407D9060800F807DE060800FC07E30608000008E80608000408ED0608000808F20608000C08F706080010081D0208001808230008001C082800080020082D0008002408320008002808730408002C087804080030087D0408003408A50408003808D70408003C086D05080040088605080044088B0508004C082300080050082800080054082D0008005808320008005C083700080060086A0108006808230008006C082800080070082D0008007808230008007C082800080080082D0008008408320008008808370008008C086A01080090086F0108009408830108009808880108009C08FC020800A00801030800A80823000800AC0828000800B0082D000800B80823000800BC0828000800C0082D000800C40832000800CC0823000800D00828000800D4082D000800D80832000800E00823000800E40828000800EC0823000800F00820070800F408D1050800F80825070800FC082A07080000092F0708000409340708000809390708000C093E07080010094307080014094807080018094D0708002009F30208002409280008002809320008002C096A01080030097304080034097804080038097D0408003C098204080040098704080044098C0408004809910408004C099604080050099B0408005809230008005C092800080060092D0008006409830108006809FC0208006C092403080070095A07080074091D02080078095F0708007C09640708008409230008008809280008008C092D0008009409230008009809280008009C092D000800A00932000800A40937000800AC0923000800B00928000800B4092D000800BC0923000800C00928000800C4092D000800CC0923000800D00928000800D40988010800D8098D010800DC0992010800E00901030800E40906030800E80929030800EC092E030800F00933030800F40956030800F8095B030800FC0960030800000A65030800040A88030800080A8D0308000C0A92030800100A97030800140A9C030800180AA10308001C0AB0030800200AB5030800240ABA030800280ABF0308002C0AC4030800300AC9030800340ACE030800380AD30308003C0AD8030800400ADD030800440AE20308004C0A23000800500A28000800540A2D000800580A320008005C0A37000800600A83010800640A88010800680A8D0108006C0A92010800700A97010800740A9C010800780A5B0308007C0A60030800800A73040800840A9E060800880AA3060800900A23000800940A28000800980A2D0008009C0A32000800A40A23000800A80A28000800AC0A2D000800B00A32000800B80A23000800BC0A28000800C00A2D000800C40A32000800C80A37000800D00A23000800D40A28000800D80A2D000800DC0A32000800E00A37000800E80A23000800EC0A28000800F00A2D000800F40A32000800FC0A23000800000B28000800040B2D000800080B320008000C0B37000800100B6A010800140B6F010800180B740108001C0B79010800200B7E010800240B83010800280B880108002C0B8D010800300B92010800340B97010800380B9C0108003C0BA1010800400BA3070800440BA8070800480BAD0708004C0BFC020800500B73040800540B78040800580B7D0408005C0B82040800600B87040800640B8C040800680B910408006C0B96040800700B9B040800740BA0040800780BA50408007C0BAA040800800BAF040800840BB4040800880BB90408008C0BBE040800900BC8040800940BCD040800980BD20408009C0BD7040800A00BDC040800A40BE1040800A80BE6040800AC0BEB040800B00BF0040800B40B9E060800B80BA3060800C00B23000800C40B28000800C80B2D000800CC0B32000800D00B37000800D40B6A010800D80B6F010800DC0B74010800E00B79010800E40B7E010800E80B83010800EC0B88010800F00B8D010800F40B92010800F80B97010800FC0B9C010800000CA1010800040CA3070800080CA80708000C0CAD070800100CFC020800140C01030800180C060308001C0C0B030800200C10030800240C15030800280CB10608002C0C9E060800300CA3060800380C230008003C0C28000800400C2D000800480C230008004C0C28000800DC0C23000800E00C28000800E40C2D000800E80C32000800EC0C37000800F00C6A010800F40C6F010800F80C74010800000D28000800040D2D000800080D320008000C0D37000800100D6A010800140D6F010800180D74010800200D23000800240D28000800280D2D000800300D23000800340D28000800380D2D0008003C0D32000800440D23000800480D280008004C0D2D000800500D6F010800540D740108005C0D23000800600D37000800640D6A010800680D7E0108006C0D83010800700DA1010800740DA8070800780D15030800800D23000800840D28000800880D2D000800900D23000800940D28000800980D2D0008009C0D32000800A00D37000800A80DF3020800AC0D23000800B00D28000800B40D2D000800BC0D23000800C00D28000800C40D6E040800CC0D23000800D00D28000800D80D23000800DC0D28000800E00D2D000800E40D37000800040E28000800080E2D000800100E23000800140E28000E00180E98082E002300AB0C2E0083002A0D2E001300980C2E001B00AB0C2E002B00B10C2E003300980C2E003B00C00C2E004300AB0C2E005300AB0C2E005B00E10C2E006B000B0D2E007300180D2E007B00210D6001BB002800A301A30528006303A305280083086B0128000009BB0028002009BB0028004009BB0028006009BB0028000000010000000B000000010000000C0001001000000045001E09300936094009450953095A096F098509AC09D009E809EE090A0A100A280A970ADE0AEA0A130B440BA10BEA0BFE0B1A0C350C440C760C830C880C030001000400060005000A00070011002D001A002E002C003B003D0000003607D90000004307DD0000004A07E10000004F07E60000005807D900000031080E01000043080E0100005E08DD0000006D08DD0000005009DD0000005A09DD0000006709430100006A09480100007A094801000088094E0100008D09530100005009DD0000005A09DD000000670943010000F80943010000FC09DD0000006A09480100007A094801000088094E0100008D0953010000E54206080000EE42E60000000143D90000001043060800001B43DD00000024430E0100003743060800004343060800004D43060800005743060800006743060800007743060800008143060800008B43060800009A430E010000AF430B080000B64311080000370317080000DB46E60000004A0711080000E746D9000000F646D9000000FF46D90000000A47DD0000001647D90000002047060800002947D90000003647D90000004547D90000005347D900000061470E0100006C47D9000000754739080000D5023E0800007F47D90000005B4ADD000000644A060800006B4AD900000000040B080000734AE60000007D4AD900020003000300020004000500020005000700020006000900020007000B00020015000D00020016000F0002001700110002001800130002001E00150001001F001500020020001700010021001700020022001900010023001900020024001B00020025001D00020026001F00010027001F0001002900210002002800210002003300230001003400230002003500250001003600250002003700270001003800270001003A00290002003900290001003C002B0002003B002B0002003D002D0002003E002F0001004000310002003F00310002004100330001004200330002005C00350001005D00350002005E00370001005F003700010061003900020060003900020062003B00010063003B00020064003D00010065003D00020066003F00010067003F0002006800410001006900410002006A00430001006B00430002006C00450002006D00470001006E00470002006F004900010070004900020071004B00010072004B00010074004D00020073004D00020075004F00010076004F0001007800510002007700510002007900530001007A00530002007B00550001007C00550002007D00570001007E005700010081005900020080005900020082005B00010083005B00020084005D00010085005D00020086005F00010087005F0002008800610001008900610002008A00630001008B00630002008C00650001008D00650002008E00670001008F006700020090006900010091006900010093006B00020092006B00020094006D00010095006D00010097006F00020096006F0002009800710001009900710002009A00730001009B00730002009C00750001009D00750001009F00770002009E0077000200A00079000100A10079000200A3007B000100A4007B000200A5007D000100A6007D000200A7007F000100A8007F000200A90081000100AA0081000200AB0083000100AC0083000200AD0085000100AE0085000A001100D909350AC60AF50B402500008A03048000000100000000000000000000000000C00400000200000000000000000000000100CE040000000002000000000000000000000001006C0C00000000090008000A0008000B0008000C00080040003F0041003F0042003F004500440000000000003C4D6F64756C653E005753492E5769676F73424C30322E646C6C00426172636F646554797065005753492E436F6D6D6F6E00426172636F646500415243466F75724D616E616765645472616E73666F726D45787465726E616C0052433445787465726E616C00415243466F75724D616E6167656445787465726E616C0052433443727970746F5365727669636550726F766964657245787465726E616C00536861726564424C3032436F6D6D6F6E0053454C4543545F5459504500434C4156455F5449504F5F5041474F00434F4C5F4D4F565300434F4C5F414D005341535F464C414753004143434F554E545F50524F4D4F5F4352454449545F54595045004143434F554E545F50524F4D4F5F5354415455530047555F555345525F5459504500434153484945525F4D4F56454D454E54004D6F76656D656E745479706500506C617953657373696F6E54797065004163636F756E745479706500436173686C6573734D6F646500506C617953657373696F6E537461747573004754506C617953657373696F6E537461747573004754506C61796572547261636B696E675370656564004754506C617953657373696F6E506C61796572536B696C6C0050656E64696E6747616D65506C617953657373696F6E537461747573004163636F756E74426C6F636B526561736F6E005465726D696E616C54797065730048414E445041595F54595045005465726D696E616C537461747573004143434F554E54535F50524F4D4F5F434F5354005757505F434F4E4E454354494F4E5F54595045005757505F434F4E4E454354494F4E5F53544154555300545950455F4D554C5449534954455F534954455F5441534B530053657175656E6365496400506172616D617465727354797065466F725369746500434153484945525F53455353494F4E5F5354415455530043757272656E637945786368616E6765537562547970650043757272656E637945786368616E676554797065005472616E73616374696F6E54797065004F7065726174696F6E436F64650043617368696572566F75636865725479706500556E646F5374617475730046554C4C5F4E414D455F545950450043757272656E637945786368616E6765526573756C740042616E6B5472616E73616374696F6E446174610047524F55505F454C454D454E545F54595045004558504C4F49545F454C454D454E545F54595045005061727469636970617465496E436173684465736B447261770055706772616465446F776E6772616465416374696F6E004177617264506F696E7473537461747573004C616E67756167654964656E7469666965720053595354454D5F4D4F4445005041545445524E5F54595045004754506C6179657254797065004361676543757272656E6379547970650043757272656E6379547970650053746174757355706461746543757272656E6379547970650043617368696572436F6E636570744F7065726174696F6E526573756C74005053415F5459504500505341436C69656E745F457370656369616C4D6F64650053686172656446756E6374696F6E730047656E6572616C506172616D0044696374696F6E617279004F7065726174696F6E566F756368657244696374696F6E61727900566F7563686572506172616D657465727300436C61737331005753492E5769676F73424C3032006D73636F726C69620053797374656D00456E756D004F626A6563740053797374656D2E53656375726974792E43727970746F677261706879004943727970746F5472616E73666F726D0049446973706F7361626C650053796D6D6574726963416C676F726974686D0056616C7565547970650053797374656D2E436F6C6C656374696F6E732E47656E657269630044696374696F6E61727960320076616C75655F5F004163636F756E7400526573657276656400476966740048616E64706179005061794F72646572005469746F5374616E6461726456616C69646174696F6E005469746F536563757265456E68616E63656456616C69646174696F6E005469746F53797374656D56616C69646174696F6E00556E6B6E6F776E0046696C746572416E79426172636F64650046696C746572416E795469746F0046696C74657242616E6B436172640046696C746572547261636B446174610046696C746572416E7900424152434F44455F4B455900424152434F44455F495600424152434F44455F434845434B53554D5F4D4F44554C45002E63746F72006D5F65787465726E616C5F636F6465006D5F736974655F6964006D5F74797065006D5F756E697175655F696400546F537472696E67006765745F45787465726E616C436F6465006765745F536974654964006765745F54797065006765745F556E697175654964006765745F496E7465726E616C436F646500437265617465005472795061727365004765744F7065726174696F6E46726F6D426172636F646500497356616C696400456E636F6465004465636F64650050726566697857697468436865636B73756D0043727970740045787465726E616C436F646500536974654964005479706500556E69717565496400496E7465726E616C436F6465007472616E73666F726D5F6B6579006B65795F6C656E67746800646174615F7065726D75746174696F6E00696E6465783100696E6465783200646973706F7365006765745F43616E52657573655472616E73666F726D006765745F43616E5472616E73666F726D4D756C7469706C65426C6F636B73006765745F496E707574426C6F636B53697A65006765745F4F7574707574426C6F636B53697A65005472616E73666F726D426C6F636B005472616E73666F726D46696E616C426C6F636B00496E697400446973706F73650043616E52657573655472616E73666F726D0043616E5472616E73666F726D4D756C7469706C65426C6F636B7300496E707574426C6F636B53697A65004F7574707574426C6F636B53697A65006765745F426C6F636B53697A65007365745F426C6F636B53697A65006765745F466565646261636B53697A65007365745F466565646261636B53697A65006765745F4956007365745F4956004B657953697A6573006765745F4C6567616C426C6F636B53697A6573006765745F4C6567616C4B657953697A6573004369706865724D6F6465006765745F4D6F6465007365745F4D6F64650050616464696E674D6F6465006765745F50616464696E67007365745F50616464696E670047656E657261746549560047656E65726174654B657900426C6F636B53697A6500466565646261636B53697A65004956004C6567616C426C6F636B53697A6573004C6567616C4B657953697A6573004D6F64650050616464696E670069735F646973706F73656400437265617465446563727970746F7200437265617465456E63727970746F7200617263666F75726D616E61676564006765745F4B6579007365745F4B6579006765745F4B657953697A65007365745F4B657953697A65004B6579004B657953697A6500434F4C5F414343535F4143434F554E545F494400434F4C5F414343535F544F54414C5F43415348494E00434F4C5F414343535F544F54414C5F4445564F4C5554494F4E00434F4C5F414343535F544F54414C5F5052495A4500434F4C5F414343535F544F54414C5F4953523100434F4C5F414343535F544F54414C5F4953523200434F4C5F414343535F52464300434F4C5F414343535F4355525000434F4C5F414343535F4E414D4500434F4C5F414343535F4144445245535300434F4C5F414343535F4349545900434F4C5F414343535F5A495000434F4C5F414343535F545241434B5F4441544100434F4C5F414343535F544F54414C5F5441585F52455455524E494E4700434F4C5F414343535F544F54414C5F534552564943455F43484152474500434F4C5F414343535F544F54414C5F444543494D414C5F524F554E44494E4700434F4C5F414343535F544F54414C5F5052495A455F494E5F4B494E4400444154415345545F4E414D45005441424C455F4E414D455F4D4F56454D454E54005441424C455F4E414D455F4143434F554E54005441424C455F4E414D455F544F54414C5F4D4F56454D454E5453005449504F5F5041474F004D41585F4F5045524154494F4E530041525241595F534550415241544F520047657453616C6573416E645061796D656E747342794F7065726174696F6E0047657453616C6573416E645061796D656E747342795174794F7065726174696F6E730047657453616C6573416E645061796D656E747342794F7065726174696F6E4C697374004461746554696D650047657453616C6573416E645061796D656E74734265747765656E44617465730053797374656D2E4461746100446174615461626C650053656E64446174615461626C654F766572506970650053716C4462547970650047657453716C4462547970650047657453616C6573416E645061796D656E747342794F7065726174696F6E4469726563740047657453616C6573416E645061796D656E747342795174794F7065726174696F6E734469726563740047657453616C6573416E645061796D656E747342794F7065726174696F6E4C6973744469726563740047657453616C6573416E645061796D656E74734265747765656E44617465734469726563740047657453616C6573416E645061796D656E74730053797374656D2E446174612E53716C436C69656E740053716C436F6E6E656374696F6E0053716C436F6D6D616E640047657453656C6563740053716C446174615265616465720053716C5472616E73616374696F6E004765745472616E73706F736500437265617465436173686965724D6F76656D656E7473436F6C756D6E730044617461526F7700436865636B456D7074794669656C647300437265617465526F77466F72554E5243726564697400476574566F756368657246726F6D436173686965724D6F76656D656E740062795F6F7065726174696F6E0062795F7174795F6F7065726174696F6E730062795F6F7065726174696F6E735F6C697374006265747765656E5F646174657300756E646566696E65640062795F706F696E74730062795F636173680062795F636865636B0062795F63617264004F5045524154494F4E5F4944004441544554494D45004143434F554E545F4944004F5045524154494F4E5F434F444500434153485F494E5F544F54414C00434153485F494E5F53504C49543100434153485F494E5F53504C49543200434153485F494E5F5441583100434153485F494E5F5441583200434153485F4F55545F544F54414C004445564F4C5554494F4E5F414D4F554E54005052495A455F414D4F554E54005441585F4F4E5F5052495A45315F414D4F554E54005441585F4F4E5F5052495A45325F414D4F554E5400434153485F4F55545F4D4F4E455900434153485F4F55545F434845434B00484F4C4445525F52464300484F4C4445525F4355525000484F4C4445525F4E414D4500414444524553530043495459005A49500045564944454E43455F484F4C4445525F5246430045564944454E43455F484F4C4445525F435552500045564944454E43455F484F4C4445525F4E414D450045564944454E43455F414444524553530045564944454E43455F434954590045564944454E43455F5A49500045564944454E434500444F43554D454E545F4944005441585F52455455524E494E4700534552564943455F43484152474500444543494D414C5F524F554E44494E4700434153484945525F53455353494F4E005052495A455F494E5F4B494E445F414D4F554E5400434153484945525F4E414D45005052495A455F4558504952454400564F55434845525F494400464F4C494F0050524F4D4F54494F4E414C5F42414C414E4345005041594D454E545F545950455F4B4559005041594D454E545F4D4554484F440052454348415247455F464F525F504F494E54535F5052495A45004F50454E494E475F434D5F4441544500545241434B5F444154415F45585445524E414C0041435F484F4C4445525F4E414D45530041435F484F4C4445525F5355524E414D4500434153484945525F53455353494F4E5F494400434153484945525F555345525F494400434153484945525F46554C4C5F4E414D4500434153484945525F46554C4C5F5355524E414D4500434153484945525F434153484945525F494400434153484945525F434153484945525F4E414D450052454348415247455F545950455F4E554D455249435F4B4559005449504F5F4F5045524143494F4E5F4445534352495043494F4E00464F4C494F3200434153485F494E5F544F54414C5F574954484F55545F5441580042414C414E43455F415F464943484153004649434841535F415F42414C414E43450054595045004144445F414D4F554E54005355425F414D4F554E5400524643004355525000484F4C4445525F4144445245535300484F4C4445525F4349545900484F4C4445525F5A495000545241434B5F444154410045564944454E43455F5246430045564944454E43455F435552500045564944454E43455F444F43554D454E545F4944310045564944454E43455F444F43554D454E545F4944320045564944454E43455F504C415945525F4E414D45310045564944454E43455F504C415945525F4E414D45320045564944454E43455F504C415945525F4E414D4533004F5045524154494F4E5F5459504500434153485F5041594D454E545F414D4F554E5400434845434B5F5041594D454E545F414D4F554E54004E4F4E45005553455F455854454E4445445F4D455445525300435245444954535F504C41595F4D4F44455F5341535F4D414348494E45005553455F48414E445041595F494E464F524D4154494F4E005350454349414C5F50524F47524553534956455F4D4554455200554E4B4E4F574E004E5231004E52320052454445454D41424C4500504F494E5400554E523100554E5232004E52330041574152444544004143544956450045584841555354454400455850495245440052454445454D4544004E4F545F41574152444544004552524F520043414E43454C4C45445F4E4F545F41444445440043414E43454C4C45440043414E43454C4C45445F42595F504C415945520050524541535349474E4544004E4F545F41535349474E45440055534552005359535F4143434550544F52005359535F50524F4D4F424F58005359535F53595354454D005359535F5449544F005359535F47414D494E475F5441424C4500535550455255534552004F50454E5F53455353494F4E00434C4F53455F53455353494F4E0046494C4C45525F494E0046494C4C45525F4F555400434153485F494E00434153485F4F5554005441585F4F4E5F5052495A45310050524F4D4F54494F4E5F4E4F545F52454445454D41424C45005052495A455300434152445F4445504F5349545F494E00434152445F4445504F5349545F4F55540043414E43454C5F4E4F545F52454445454D41424C45004D425F4D414E55414C5F4348414E47455F4C494D4954004D425F434153485F494E005441585F4F4E5F5052495A45320050524F4D4F5F435245444954530050524F4D4F5F544F5F52454445454D41424C450050524F4D4F5F455850495245440050524F4D4F5F43414E43454C0050524F4D4F5F53544152545F53455353494F4E0050524F4D4F5F454E445F53455353494F4E00434152445F5245504C4143454D454E5400445241575F5449434B45545F5052494E540048414E445041590048414E445041595F43414E43454C4C4154494F4E004D414E55414C5F48414E44504159004D414E55414C5F48414E445041595F43414E43454C4C4154494F4E004D425F434153485F494E5F53504C495432004445565F53504C495432004445565F53504C495431004D425F434153485F494E5F53504C4954310043414E43454C5F53504C4954310043414E43454C5F53504C49543200504F494E54535F544F5F445241575F5449434B45545F5052494E54005052495A455F434F55504F4E00434152445F4352454154494F4E00434152445F504552534F4E414C495A4154494F4E004143434F554E545F50494E5F4348414E4745004143434F554E545F50494E5F52414E444F4D00434153485F494E5F434F5645525F434F55504F4E00434153485F494E5F4E4F545F52454445454D41424C4532004D425F434153485F494E5F4E4F545F52454445454D41424C45004D425F434153485F494E5F434F5645525F434F55504F4E004D425F434153485F494E5F4E4F545F52454445454D41424C4532004D425F5052495A455F434F55504F4E004E415F434153485F494E5F53504C495431004E415F434153485F494E5F53504C495432004E415F5052495A455F434F55504F4E004E415F434153485F494E5F4E4F545F52454445454D41424C45004E415F434153485F494E5F434F5645525F434F55504F4E00434152445F52454359434C454400504F494E54535F544F5F4E4F545F52454445454D41424C4500504F494E54535F544F5F52454445454D41424C450052454445454D41424C455F4445565F455850495245440052454445454D41424C455F5052495A455F45585049524544004E4F545F52454445454D41424C455F455850495245440050524F4D4F54494F4E5F52454445454D41424C450043414E43454C5F50524F4D4F54494F4E5F52454445454D41424C450050524F4D4F54494F4E5F504F494E540043414E43454C5F50524F4D4F54494F4E5F504F494E54005441585F52455455524E494E475F4F4E5F5052495A455F434F55504F4E004D425F434153485F4C4F535400524551554553545F544F5F564F55434845525F52455052494E5400434845434B5F5041594D454E5400434153485F50454E44494E475F434C4F53494E47004143434F554E545F424C4F434B4544004143434F554E545F554E424C4F434B45440043555252454E43595F45584348414E47455F53504C4954310043555252454E43595F434152445F45584348414E47455F53504C49543100434153494E4F5F43484950535F45584348414E47455F53504C495431005449544F5F5449434B45545F434153484945525F5052494E5445445F4341534841424C4500504F494E54535F544F5F52454445454D41424C455F41535F43415348494E5F53504C49543100504F494E54535F544F5F52454445454D41424C455F41535F43415348494E5F53504C4954320043555252454E43595F45584348414E47455F53504C4954320043555252454E43595F434152445F45584348414E47455F53504C49543200434153494E4F5F43484950535F45584348414E47455F53504C49543200524551554553545F544F5F5449434B45545F52455052494E540050415254494349504154494F4E5F494E5F434153484445534B5F445241570043555252454E43595F434845434B5F53504C4954310043555252454E43595F434845434B5F53504C495432005052495A455F494E5F4B494E44315F47524F5353005052495A455F494E5F4B494E44315F54415831005052495A455F494E5F4B494E44315F5441583200434C4F53455F53455353494F4E5F42414E4B5F4341524400434C4F53455F53455353494F4E5F434845434B0046494C4C45525F4F55545F434845434B005052495A455F494E5F4B494E44325F47524F5353005052495A455F494E5F4B494E44325F54415831005052495A455F494E5F4B494E44325F54415832005449544F5F5449434B45545F434153484945525F504149445F4341534841424C45005449544F5F5449434B45545F434153484945525F5052494E5445445F50524F4D4F5F52454445454D41424C45005449544F5F5449434B45545F4D414348494E455F504C415945445F4341534841424C45005449544F5F5449434B45545F434153484945525F5052494E5445445F50524F4D4F5F4E4F545F52454445454D41424C45005449544F5F5449434B45545F52454953535545005449544F5F5449434B45545F4352454154455F4341534841424C455F4F46464C494E45005449544F5F5449434B45545F4352454154455F504C415941424C455F4F46464C494E45005449544F5F5449434B45545F455850495245445F50524F4D4F5F52454445454D41424C45005449544F5F5449434B45545F455850495245445F50524F4D4F5F4E4F545F52454445454D41424C45005449544F5F5449434B45545F455850495245445F52454445454D41424C45005449544F5F5449434B45545F4341534841424C455F52454445454D45445F4F46464C494E45005449544F5F5449434B45545F504C415941424C455F52454445454D45445F4F46464C494E45005449544F5F5449434B45545F4D414348494E455F5052494E5445445F48414E44504159005449544F5F5449434B45545F4D414348494E455F5052494E5445445F4A41434B504F54005449544F5F5449434B45545F434153484945525F504149445F48414E44504159005449544F5F5449434B45545F434153484945525F504149445F4A41434B504F54005449544F5F56414C49444154455F5449434B4554005449544F5F5449434B45545F4D414348494E455F5052494E5445445F4341534841424C45005449544F5F5449434B45545F4D414348494E455F5052494E5445445F50524F4D4F5F4E4F545F52454445454D41424C45005449544F5F5449434B45545F4D414348494E455F504C415945445F50524F4D4F5F52454445454D41424C45005449544F5F5449434B45545F4D414348494E455F504C415945445F50524F4D4F5F4E4F545F52454445454D41424C45005449544F5F5449434B45545F434153484945525F504149445F50524F4D4F5F52454445454D41424C45005449544F5F5449434B45545F434153484945525F455850495245445F504149445F4341534841424C45005449544F5F5449434B45545F434153484945525F455850495245445F504149445F50524F4D4F5F52454445454D41424C45005449544F5F5449434B45545F434153484945525F455850495245445F504149445F4A41434B504F54005449544F5F5449434B45545F434153484945525F455850495245445F504149445F48414E44504159005449544F5F4C4153545F4D4F56454D454E54005452414E534645525F4352454449545F4F5554005452414E534645525F4352454449545F494E00434153485F494E5F5441585F53504C49543100504F494E54535F544F5F52454445454D41424C455F41535F43415348494E5F5052495A4500434152445F5245504C4143454D454E545F464F525F4652454500434152445F5245504C4143454D454E545F494E5F504F494E545300434153485F494E5F5441585F53504C4954320043555252454E43595F4352454449545F434152445F45584348414E47455F53504C4954310043555252454E43595F44454249545F434152445F45584348414E47455F53504C4954310043555252454E43595F4352454449545F434152445F45584348414E47455F53504C4954320043555252454E43595F44454249545F434152445F45584348414E47455F53504C49543200434153485F414456414E43455F43555252454E43595F45584348414E474500434153485F414456414E43455F47454E455249435F42414E4B5F4341524400434153485F414456414E43455F4352454449545F42414E4B5F4341524400434153485F414456414E43455F44454249545F42414E4B5F4341524400434153485F414456414E43455F434845434B00434153485F414456414E43455F43415348004E4F545F504C415945445F52454348415247455F524546554E44454400434153485F434C4F53494E475F53484F525400434153485F434C4F53494E475F4F56455200434147455F4F50454E5F53455353494F4E00434147455F434C4F53455F53455353494F4E00434147455F46494C4C45525F494E00434147455F46494C4C45525F4F555400434147455F4C4153545F4D4F56454D454E540043484950535F53414C450043484950535F50555243484153450043484950535F53414C455F4445564F4C5554494F4E5F464F525F5449544F0043484950535F53414C455F544F54414C0043484950535F50555243484153455F544F54414C0043484950535F4348414E47455F494E0043484950535F4348414E47455F4F55540043484950535F53414C455F574954485F434153485F494E0043484950535F50555243484153455F574954485F434153485F4F55540043484950535F53414C455F52454749535445525F544F54414C0043484950535F4C4153545F4D4F56454D454E540047414D494E475F5441424C455F47414D455F4E455457494E0047414D494E475F5441424C455F47414D455F4E455457494E5F434C4F53450048414E445041595F415554484F52495A45440048414E445041595F554E415554484F52495A45440048414E445041595F564F494445440048414E445041595F564F494445445F554E444F4E45004D425F45584345535300434F4D50414E595F425F43555252454E43595F434152445F45584348414E47455F53504C49543100434F4D50414E595F425F43555252454E43595F434152445F45584348414E47455F53504C49543200434F4D50414E595F425F43555252454E43595F4352454449545F434152445F45584348414E47455F53504C49543100434F4D50414E595F425F43555252454E43595F4352454449545F434152445F45584348414E47455F53504C49543200434F4D50414E595F425F43555252454E43595F44454249545F434152445F45584348414E47455F53504C49543100434F4D50414E595F425F43555252454E43595F44454249545F434152445F45584348414E47455F53504C4954320048414E445041595F57495448484F4C44494E4700434F4D50414E595F425F43555252454E43595F45584348414E47455F53504C49543100434F4D50414E595F425F43555252454E43595F45584348414E47455F53504C49543200434F4D50414E595F425F43555252454E43595F434845434B5F53504C49543100434F4D50414E595F425F43555252454E43595F434845434B5F53504C49543200434F4D50414E595F425F434153485F414456414E43455F43555252454E43595F45584348414E474500434F4D50414E595F425F434153485F414456414E43455F47454E455249435F42414E4B5F4341524400434F4D50414E595F425F434153485F414456414E43455F4352454449545F42414E4B5F4341524400434F4D50414E595F425F434153485F414456414E43455F44454249545F42414E4B5F4341524400434F4D50414E595F425F434153485F414456414E43455F434845434B0057494E5F4C4F53535F53544154454D454E545F5245515545535400434152445F4445504F5349545F494E5F434845434B00434152445F4445504F5349545F494E5F43555252454E43595F45584348414E474500434152445F4445504F5349545F494E5F434152445F43524544495400434152445F4445504F5349545F494E5F434152445F444542495400434152445F4445504F5349545F494E5F434152445F47454E4552494300434152445F5245504C4143454D454E545F434845434B00434152445F5245504C4143454D454E545F434152445F43524544495400434152445F5245504C4143454D454E545F434152445F444542495400434152445F5245504C4143454D454E545F434152445F47454E4552494300434F4D50414E595F425F434152445F4445504F5349545F494E00434F4D50414E595F425F434152445F4445504F5349545F4F555400434F4D50414E595F425F434152445F5245504C4143454D454E5400434F4D50414E595F425F434152445F4445504F5349545F494E5F434845434B00434F4D50414E595F425F434152445F4445504F5349545F494E5F43555252454E43595F45584348414E474500434F4D50414E595F425F434152445F4445504F5349545F494E5F434152445F43524544495400434F4D50414E595F425F434152445F4445504F5349545F494E5F434152445F444542495400434F4D50414E595F425F434152445F4445504F5349545F494E5F434152445F47454E4552494300434F4D50414E595F425F434152445F5245504C4143454D454E545F434845434B00434F4D50414E595F425F434152445F5245504C4143454D454E545F434152445F43524544495400434F4D50414E595F425F434152445F5245504C4143454D454E545F434152445F444542495400434F4D50414E595F425F434152445F5245504C4143454D454E545F434152445F47454E4552494300434153484945525F42595F434F4E434550545F494E00434153484945525F42595F434F4E434550545F4F555400434153484945525F42595F434F4E434550545F4C41535400506C61790043617368496E00436173684F7574004465766F6C7574696F6E005461784F6E5072697A65310053746172744361726453657373696F6E00456E644361726453657373696F6E00416374697661746500446561637469766174650050726F6D6F74696F6E4E6F7452656465656D61626C65004465706F736974496E004465706F7369744F75740043616E63656C4E6F7452656465656D61626C65005461784F6E5072697A65320050726F6D6F437265646974730050726F6D6F546F52656465656D61626C650050726F6D6F457870697265640050726F6D6F43616E63656C0050726F6D6F537461727453657373696F6E0050726F6D6F456E6453657373696F6E00437265646974734578706972656400437265646974734E6F7452656465656D61626C654578706972656400436172645265706C6163656D656E7400447261775469636B65745072696E740048616E6470617943616E63656C6C6174696F6E004D616E75616C456E6453657373696F6E0050726F6D6F4D616E75616C456E6453657373696F6E004D616E75616C48616E64706179004D616E75616C48616E6470617943616E63656C6C6174696F6E00506F696E74734177617264656400506F696E7473546F476966745265717565737400506F696E7473546F4E6F7452656465656D61626C6500506F696E74734769667444656C697665727900506F696E74734578706972656400506F696E7473546F447261775469636B65745072696E7400506F696E747347696674536572766963657300486F6C6465724C6576656C4368616E676564005072697A65436F75706F6E00444550524543415445445F52656465656D61626C655370656E7455736564496E50726F6D6F74696F6E7300506F696E7473546F52656465656D61626C65005072697A6545787069726564004361726443726561746564004163636F756E74506572736F6E616C697A6174696F6E004D616E75616C6C794164646564506F696E74734F6E6C79466F7252656465656D004163636F756E7450494E4368616E6765004163636F756E7450494E52616E646F6D00437265646974734E6F7452656465656D61626C6532457870697265640043617368496E436F766572436F75706F6E0043617368496E4E6F7452656465656D61626C65320043616E63656C537461727453657373696F6E004361726452656379636C65640050726F6D6F74696F6E52656465656D61626C650043616E63656C50726F6D6F74696F6E52656465656D61626C650050726F6D6F74696F6E506F696E740043616E63656C50726F6D6F74696F6E506F696E74004D616E75616C486F6C6465724C6576656C4368616E6765640054617852657475726E696E674F6E5072697A65436F75706F6E00446563696D616C526F756E64696E6700536572766963654368617267650043616E63656C47696674496E7374616E636500506F696E74735374617475734368616E676564004D616E75616C6C794164646564506F696E7473466F724C6576656C00496D706F727465644163636F756E7400496D706F72746564506F696E74734F6E6C79466F7252656465656D00496D706F72746564506F696E7473466F724C6576656C00496D706F72746564506F696E7473486973746F7279004163636F756E74426C6F636B6564004163636F756E74556E626C6F636B65640043616E63656C6C6174696F6E005472616E736665724372656469744F7574005472616E73666572437265646974496E0043617368496E54617800436172645265706C6163656D656E74466F724672656500436172645265706C6163656D656E74496E506F696E74730043617368416476616E63650045787465726E616C53797374656D506F696E7473537562737472616374004D756C74695369746543757272656E744C6F63616C506F696E7473005449544F5F5469636B6574436173686965725072696E7465644361736861626C65005449544F5F5469636B657443617368696572506169644361736861626C65005449544F5F5469636B6574436173686965725072696E74656450726F6D6F52656465656D61626C65005449544F5F5469636B65744D616368696E65506C617965644361736861626C65005449544F5F5469636B657452656973737565005449544F5F4163636F756E74546F5465726D696E616C437265646974005449544F5F5465726D696E616C546F4163636F756E74437265646974005449544F5F5469636B65744F66666C696E65005449544F5F5469636B6574436173686965725072696E74656450726F6D6F4E6F7452656465656D61626C65005449544F5F5469636B65744D616368696E655072696E74656448616E64706179005449544F5F5469636B65744D616368696E655072696E7465644A61636B706F74005449544F5F5469636B6574436173686965725061696448616E64706179005449544F5F5469636B657443617368696572506169644A61636B706F74005449544F5F5469636B65744D616368696E655072696E7465644361736861626C65005449544F5F5469636B65744D616368696E655072696E74656450726F6D6F4E6F7452656465656D61626C65005449544F5F5469636B6574436173686965725061696450726F6D6F52656465656D61626C65005449544F5F5469636B65744D616368696E65506C6179656450726F6D6F52656465656D61626C65005449544F5F7469636B65744D616368696E65506C6179656450726F6D6F4E6F7452656465656D61626C65005449544F5F5469636B65744361736869657245787069726564506169644361736861626C65005449544F5F5469636B657443617368696572457870697265645061696450726F6D6F52656465656D61626C65005449544F5F5469636B65744361736869657245787069726564506169644A61636B706F74005449544F5F5469636B657443617368696572457870697265645061696448616E6470617900436173686465736B4472617750617274696369706174696F6E00436173686465736B44726177506C617953657373696F6E00436869707353616C654465766F6C7574696F6E466F725469746F00436869707353616C65546F74616C0043686970735075726368617365546F74616C0057696E4C6F737353746174656D656E745265717565737400436172644465706F736974496E436865636B00436172644465706F736974496E43757272656E637945786368616E676500436172644465706F736974496E4361726443726564697400436172644465706F736974496E43617264446562697400436172644465706F736974496E4361726447656E6572696300436172645265706C6163656D656E74436865636B00436172645265706C6163656D656E744361726443726564697400436172645265706C6163656D656E7443617264446562697400436172645265706C6163656D656E744361726447656E657269630048494444454E5F52656368617267654E6F74446F6E65576974684361736800434144494C4C41435F4A41434B0057494E00414C455349530048414E445041595F43414E43454C4C45445F435245444954530048414E445041595F4A41434B504F540048414E445041595F4F525048414E5F435245444954530048414E445041595F4D414E55414C0048414E445041595F534954455F4A41434B504F5400445241570050524F47524553534956455F50524F564953494F4E0050524F47524553534956455F52455345525645004143434F554E545F554E4B4E4F574E004143434F554E545F434144494C4C41435F4A41434B004143434F554E545F57494E004143434F554E545F414C45534953004143434F554E545F5649525455414C5F43415348494552004143434F554E545F5649525455414C5F5445524D494E414C004F70656E656400436C6F736564004162616E646F6E6564004D616E75616C436C6F7365640043616E63656C6C65640048616E647061795061796D656E740048616E6470617943616E63656C5061796D656E74004472617757696E6E657200447261774C6F7365720050726F6772657373697665526573657276650050726F677265737369766550726F766973696F6E004177617900536C6F77004D656469756D004661737400536F66740041766572616765004861726400496E697469616C00454C5030315F53706C69745F50535F52656464656D5F4E6F52656465656D0046524F4D5F43415348494552004D41585F42414C414E4345004142414E444F4E45445F434152440046524F4D5F475549004D41585F50494E5F4641494C5552455300494D504F52540045585445524E414C5F53595354454D00484F4C4445525F4C4556454C5F444F574E475241444500484F4C4445525F4C4556454C5F555047524144450045585445524E414C5F53595354454D5F43414E43454C45440045585445524E414C5F414D4C0054334753005341535F484F5354005349544500534954455F4A41434B504F54004D4F42494C455F42414E4B004D4F42494C455F42414E4B5F494D42004953544154530050524F4D4F424F5800434153484445534B5F445241570047414D494E475F5441424C455F53454154004F46464C494E450043414E43454C4C45445F43524544495453004A41434B504F54004F525048414E5F43524544495453004D414E55414C005350454349414C5F50524F4752455353495645004D414E55414C5F43414E43454C4C45445F43524544495453004D414E55414C5F4A41434B504F54004D414E55414C5F4F5448455253004F55545F4F465F5345525649434500524554495245440052455345545F4F4E5F52454445454D005345545F4F4E5F46495253545F4E525F50524F4D4F54494F4E005550444154455F4F4E5F5245434841524745005550444154455F4F4E5F52454445454D0052455345545F4F4E5F5245434841524745004D554C54495349544500535550455243454E54455200434F4E4E454354454400444953434F4E4E454354454400446F776E6C6F6164436F6E66696775726174696F6E0055706C6F6164506F696E74734D6F76656D656E74730055706C6F6164506572736F6E616C496E666F0055706C6F61644163636F756E74446F63756D656E747300446F776E6C6F6164506F696E74735F5369746500446F776E6C6F6164506F696E74735F416C6C00446F776E6C6F6164506572736F6E616C496E666F5F4361726400446F776E6C6F6164506572736F6E616C496E666F5F5369746500446F776E6C6F6164506572736F6E616C496E666F5F416C6C00446F776E6C6F616450726F76696465727347616D65730055706C6F616450726F76696465727347616D657300446F776E6C6F616450726F76696465727300446F776E6C6F61644163636F756E74446F63756D656E74730055706C6F616453616C6573506572486F75720055706C6F6164506C617953657373696F6E730055706C6F616450726F7669646572730055706C6F616442616E6B730055706C6F616441726561730055706C6F61645465726D696E616C730055706C6F61645465726D696E616C73436F6E6E65637465640055706C6F616447616D65506C617953657373696F6E7300446F776E6C6F61644D617374657250726F66696C657300446F776E6C6F6164436F72706F7261746555736572730055706C6F61644C61737441637469766974790055706C6F6164436173686965724D6F76656D656E74734772757065644279486F75720055706C6F6164416C61726D7300446F776E6C6F6164416C61726D5061747465726E7300446F776E6C6F61644C63644D657373616765730055706C6F616448616E64706179730055706C6F616453656C6C73416E6442757973004E6F7453657400566F75636865727353706C69744100566F75636865727353706C69744200566F75636865727353706C69744243616E63656C004163636F756E74496400547261636B4163636F756E744368616E67657300547261636B506F696E744368616E676573004163636F756E744D6F76656D656E74730050726F76696465727347616D65730050726F766964657273004163636F756E74446F63756D656E747300436173684465736B44726177004C43444D657373616765005449544F5F56616C69646174696F6E4E756D6265720043616765436F6E63657074730043616765436F6E63657074735F4C617374004E6F74446F776E6C6F616400446F776E6C6F61645F55706461746500446F776E6C6F61645F5570646174655072696F726974794D756C74697369746500446F776E6C6F61645F5570646174655072696F7269747953697465004F50454E00434C4F534544004F50454E5F50454E44494E470050454E44494E475F434C4F53494E4700434845434B0042414E4B5F43415244004352454449545F434152440044454249545F434152440043555252454E4359004341524400434153494E4F43484950004652454500434153485F414456414E43450052454348415247450052454348415247455F434152445F4352454154494F4E004E4F545F5345540050524F4D4F54494F4E004D425F50524F4D4F54494F4E00474946545F5245515545535400474946545F44454C495645525900445241575F5449434B455400474946545F445241575F5449434B455400474946545F52454445454D41424C455F41535F43415348494E0043484950535F53414C455F574954485F5245434841524745004D425F4445504F53495400434153485F4445504F53495400434153485F5749544844524157414C0052454445454D41424C455F435245444954535F45585049524544004E4F545F52454445454D41424C455F435245444954535F4558504952454400504F494E54535F4558504952454400484F4C4445525F4C4556454C5F4348414E474544004143434F554E545F4352454154494F4E004143434F554E545F504552534F4E414C495A4154494F4E004143434F554E545F50494E5F4348414E474544004E4F545F52454445454D41424C45325F435245444954535F45585049524544004E415F434153485F494E004E415F50524F4D4F54494F4E0043414E43454C5F474946545F494E5354414E43450050524F4D4F424F585F544F54414C5F5245434841524745535F5052494E540048414E445041595F56414C49444154494F4E00494D504F52545F504F494E5453005449544F5F4F46464C494E45005449544F5F52454953535545005449544F5F5449434B45545F56414C49444154494F4E00494D504F52545F47414D494E475F5441424C455F504C41595F53455353494F4E5300534146455F4B454550494E475F4445504F53495400534146455F4B454550494E475F574954484452415700434147455F434F4E434550545300434147455F434F4E43455054535F4C4153540043617368496E5F410043617368496E5F4200436173684F75745F410043616E63656C5F4200536572766963654368617267655F4200436173684465736B447261775F57696E6E6572005469636B65744F757400436869707353616C650043686970734275790043686970734368616E676500436869707353616C654465616C6572436F707900436173684F70656E696E670043617368436C6F73696E67004D4253616C65734C696D69744368616E6765004D424465706F736974004D42436C6F73696E6700436F6D6D697373696F6E5F4200436865636B436F6D6D697373696F6E4200436173684465736B447261775F4C6F7365720045786368616E6765436F6D6D697373696F6E4200436173684465706F73697400436173685769746864726177616C00436172644465706F736974496E5F4200436172645265706C6163656D656E745F4200436172644465706F7369744F75745F42004E6F6E6500556E646F6E6500556E646F4F7065726174696F6E004143434F554E540057495448484F4C44494E4700446563696D616C006D5F696E5F616D6F756E74006D5F696E5F69736F5F636F6465006D5F67726F73735F616D6F756E74006D5F636F6D697373696F6E006D5F6E65745F616D6F756E745F73706C697431006D5F6E65745F616D6F756E745F73706C697432006D5F4E52325F616D6F756E74006D5F636172645F7072696365006D5F636172645F636F6D697373696F6E73006D5F6368616E67655F72617465006D5F6E756D5F646563696D616C73006D5F7761735F666F7265696E675F63757272656E6379006D5F6172655F70726F6D6F74696F6E735F656E61626C6564006D5F696E5F74797065006D5F6163636F756E745F70726F6D6F74696F6E5F6964006D5F62616E6B5F7472616E73616374696F6E5F64617461006D5F73756274797065006765745F496E416D6F756E74007365745F496E416D6F756E74006765745F4163636F756E7450726F6D6F74696F6E4964007365745F4163636F756E7450726F6D6F74696F6E4964006765745F496E43757272656E6379436F6465007365745F496E43757272656E6379436F6465006765745F4368616E676552617465007365745F4368616E676552617465006765745F446563696D616C73007365745F446563696D616C73006765745F576173466F7265696E6743757272656E6379007365745F576173466F7265696E6743757272656E6379006765745F47726F7373416D6F756E74007365745F47726F7373416D6F756E74006765745F436F6D697373696F6E007365745F436F6D697373696F6E006765745F4E6574416D6F756E74006765745F4E6574416D6F756E7453706C697431007365745F4E6574416D6F756E7453706C697431006765745F4E6574416D6F756E7453706C697432007365745F4E6574416D6F756E7453706C697432006765745F4E5232416D6F756E74007365745F4E5232416D6F756E74006765745F436172645072696365007365745F436172645072696365006765745F43617264436F6D697373696F6E73007365745F43617264436F6D697373696F6E73006765745F41726550726F6D6F74696F6E73456E61626C6564007365745F41726550726F6D6F74696F6E73456E61626C6564006765745F496E54797065007365745F496E54797065006765745F53756254797065007365745F53756254797065006765745F42616E6B5472616E73616374696F6E44617461007365745F42616E6B5472616E73616374696F6E4461746100496E416D6F756E74004163636F756E7450726F6D6F74696F6E496400496E43757272656E6379436F6465004368616E67655261746500446563696D616C7300576173466F7265696E6743757272656E63790047726F7373416D6F756E7400436F6D697373696F6E004E6574416D6F756E74004E6574416D6F756E7453706C697431004E6574416D6F756E7453706C697432004E5232416D6F756E74004361726450726963650043617264436F6D697373696F6E730041726550726F6D6F74696F6E73456E61626C656400496E547970650053756254797065006D5F6F7065726174696F6E5F6964006D5F646F63756D656E745F6E756D626572006D5F62616E6B5F6E616D65006D5F686F6C6465725F6E616D65006D5F62616E6B5F636F756E747279006D5F636172645F747261636B5F64617461006D5F63757272656E63795F636F6465006D5F636172645F65787069726174696F6E5F64617465006D5F636172645F646174615F656469746564006D5F636865636B5F726F7574696E675F6E756D626572006D5F636865636B5F6163636F756E745F6E756D626572006D5F636F6D6D656E74006D5F636865636B5F6461746574696D65006D5F7472616E73616374696F6E5F74797065006D5F6163636F756E745F686F6C6465725F6E616D65006765745F4F7065726174696F6E4964007365745F4F7065726174696F6E4964007365745F54797065006765745F446F63756D656E744E756D626572007365745F446F63756D656E744E756D626572006765745F42616E6B4E616D65007365745F42616E6B4E616D65006765745F486F6C6465724E616D65007365745F486F6C6465724E616D65006765745F42616E6B436F756E747279007365745F42616E6B436F756E747279006765745F547261636B44617461007365745F547261636B44617461006765745F696E416D6F756E74007365745F696E416D6F756E74006765745F43757272656E6379436F6465007365745F43757272656E6379436F6465006765745F45787069726174696F6E44617465007365745F45787069726174696F6E44617465006765745F526F7574696E674E756D626572007365745F526F7574696E674E756D626572006765745F4163636F756E744E756D626572007365745F4163636F756E744E756D626572006765745F43617264456469746564007365745F43617264456469746564006765745F436F6D6D656E7473007365745F436F6D6D656E7473006765745F436865636B44617465007365745F436865636B44617465006765745F5472616E73616374696F6E54797065007365745F5472616E73616374696F6E54797065006765745F4163636F756E74486F6C6465724E616D65007365745F4163636F756E74486F6C6465724E616D65004F7065726174696F6E496400446F63756D656E744E756D6265720042616E6B4E616D6500486F6C6465724E616D650042616E6B436F756E74727900547261636B4461746100696E416D6F756E740043757272656E6379436F64650045787069726174696F6E4461746500526F7574696E674E756D626572004163636F756E744E756D626572004361726445646974656400436F6D6D656E747300436865636B44617465004163636F756E74486F6C6465724E616D6500414C4C0047524F55500050524F56005A4F4E4500415245410042414E4B005445524D0051554552590050524F4D4F54494F4E5F52455354524943544544005041545445524E530050524F4752455353495645004C43445F4D4553534147450044656661756C7400596573004E6F00426C6F636B4F6E456E74657200426C6F636B4F6E4C6561766500426F74680043616C63756C61746564416E645265776172640050656E64696E67004D616E75616C004D616E75616C43616C63756C61746564004E65757472616C004368696E65736500437A65636800456E676C697368005370616E697368004974616C69616E004B6F7265616E005275737369616E00434153484C455353005449544F005741535300434F4E5345435554495645004E4F5F434F4E53454355544956455F574954485F4F52444552004E4F5F4F52444552004E4F5F4F524445525F574954485F52455045544954494F4E00416E6F6E796D6F7573576974684361726400437573746F6D697A656400416E6F6E796D6F7573576974686F7574436172640042696C6C00436F696E004F746865727300466F726569676E004E6174696F6E616C004E6F7468696E67004E657743757272656E6379004E6577536974654E6174696F6E616C43757272656E6379004E657753697465466F726569676E43757272656E6379006D5F636F6E636570745F6964006D5F7175616E74697479006D5F616D6F756E74006D5F69736F5F636F6465006D5F63757272656E63795F74797065006765745F5175616E74697479007365745F5175616E74697479006765745F416D6F756E74007365745F416D6F756E74006765745F49736F436F6465007365745F49736F436F6465006765745F43757272656E637954797065007365745F43757272656E637954797065006765745F436F6E636570744964007365745F436F6E636570744964006765745F436F6D6D656E74007365745F436F6D6D656E74005175616E7469747900416D6F756E740049736F436F646500436F6E63657074496400436F6D6D656E740057494E50534100416C6573697357696E706F7400466F726D617446756C6C4E616D6500414C4941535F5052454649580056616C756500476574496E74333200476574426F6F6C65616E00476574446563696D616C00476574537472696E6700476574496E7436340047656E6572617465005072696E74005072696E74436F707900436F64650053797374656D2E52756E74696D652E496E7465726F705365727669636573004F7574417474726962757465005061727469616C45787465726E616C436F6465004E756D62657256616C75650045787465726E616C496400496E7465726E616C45787465726E616C004465637279707400496E707574004F757470757400496E70757442756666657200496E7075744F666673657400496E707574436F756E74004F7574707574427566666572004F75747075744F66667365740076616C756500416C674E616D65005267624B657900526762495600446973706F73696E67004E6F4F7065726174696F6E73004F7065726174696F6E496473004461746546726F6D0044617465546F0074626C00445453616C6573416E64506179730053656C656374547970650046726F6D4461746500546F44617465005F636F6E6E00445453616C6573416E645061796D656E74730053716C526561646572005F73716C5F636F6D6D616E64005F636173686965725F766F7563686572005472780044544F7065726174696F6E7300436173686965724D6F76656D656E74735461626C6500526F7700526F774D6F76656D656E740043617368696572566F75636865720046756C6C4E616D6554797065004E616D65004D6964646C654E616D65004C6173744E616D6531004C6173744E616D65320047726F75704B6579005375626A6563744B65790044656661756C7456616C7565004D696E56616C7565004D617856616C75650053797374656D2E5265666C656374696F6E00417373656D626C795469746C6541747472696275746500417373656D626C794465736372697074696F6E41747472696275746500417373656D626C79436F6E66696775726174696F6E41747472696275746500417373656D626C79436F6D70616E7941747472696275746500417373656D626C7950726F6475637441747472696275746500417373656D626C79436F7079726967687441747472696275746500417373656D626C7954726164656D61726B41747472696275746500417373656D626C7943756C7475726541747472696275746500436F6D56697369626C65417474726962757465004775696441747472696275746500417373656D626C7956657273696F6E41747472696275746500417373656D626C7946696C6556657273696F6E4174747269627574650053797374656D2E446961676E6F73746963730044656275676761626C6541747472696275746500446562756767696E674D6F6465730053797374656D2E52756E74696D652E436F6D70696C6572536572766963657300436F6D70696C6174696F6E52656C61786174696F6E734174747269627574650052756E74696D65436F6D7061746962696C69747941747472696275746500496E74333200496E74363400537472696E6700466F726D617400457863657074696F6E006765745F4C656E677468004D6963726F736F66742E53716C5365727665722E5365727665720053716C50726F6365647572654174747269627574650055496E743634006765745F43686172730043686172004973446967697400456D707479006F705F496E657175616C69747900537562737472696E67005061727365005472696D005061644C65667400426974436F6E7665727465720047657442797465730053797374656D2E494F004D656D6F727953747265616D0043727970746F53747265616D0053747265616D0043727970746F53747265616D4D6F646500577269746500466C75736846696E616C426C6F636B00546F417272617900546F55496E743634002E6363746F720042797465003C50726976617465496D706C656D656E746174696F6E44657461696C733E7B33373739423943312D343330452D343339302D414645462D4245374530313446363533317D00436F6D70696C657247656E657261746564417474726962757465005F5F5374617469634172726179496E69745479706553697A653D31360024246D6574686F643078363030303063302D310052756E74696D6548656C706572730041727261790052756E74696D654669656C6448616E646C6500496E697469616C697A65417272617900436C6F6E650047657454797065006765745F46756C6C4E616D65004F626A656374446973706F736564457863657074696F6E00417267756D656E744E756C6C457863657074696F6E00417267756D656E744F75744F6652616E6765457863657074696F6E00436C65617200474300537570707265737346696E616C697A65004B657953697A6556616C75650043727970746F67726170686963457863657074696F6E004E6F74537570706F72746564457863657074696F6E00524E4743727970746F5365727669636550726F76696465720052616E646F6D4E756D62657247656E657261746F7200537472696E67436F6D70617269736F6E00457175616C730053706C69740044617461436F6C756D6E436F6C6C656374696F6E006765745F436F6C756D6E7300496E7465726E616C44617461436F6C6C656374696F6E42617365006765745F436F756E74004C69737460310053716C4D657461446174610053797374656D2E436F6C6C656374696F6E730049456E756D657261746F7200476574456E756D657261746F72006765745F43757272656E740044617461436F6C756D6E006765745F44617461547970650052756E74696D655479706548616E646C65004765745479706546726F6D48616E646C65006765745F436F6C756D6E4E616D6500416464004D6F76654E6578740053716C446174615265636F72640053716C436F6E746578740053716C50697065006765745F506970650053656E64526573756C747353746172740044617461526F77436F6C6C656374696F6E006765745F526F7773006765745F4974656D41727261790053657456616C75650053656E64526573756C7473526F770053656E64526573756C7473456E640024246D6574686F643078363030303034632D310054727947657456616C75650053797374656D2E446174612E436F6D6D6F6E004462436F6E6E656374696F6E004F70656E00436C6F7365006765745F4974656D005365744F7264696E616C0052656D6F76654174004D617468004D6178004D696E00426567696E5472616E73616374696F6E006765745F436F6E6E656374696F6E007365745F436F6E6E656374696F6E007365745F5472616E73616374696F6E00457865637574655265616465720053797374656D2E5465787400537472696E674275696C64657200417070656E644C696E650053716C506172616D65746572436F6C6C656374696F6E006765745F506172616D65746572730053716C506172616D65746572004462506172616D65746572007365745F56616C756500436F6E636174005265706C616365004462436F6D6D616E64007365745F436F6D6D616E64546578740044624461746152656164657200497344424E756C6C007365745F4974656D004E6577526F77004765744461746554696D65006765745F59656172006765745F4D6F6E7468006765745F446179006765745F486F7572006765745F4D696E757465006765745F5365636F6E6400426F6F6C65616E0044424E756C6C0049734E756C6C4F72456D7074790053656C656374006F705F4164646974696F6E0052656164004163636570744368616E676573007365745F5461626C654E616D65007365745F416C6C6F7744424E756C6C007365745F44656661756C7456616C7565006765745F5461626C65006765745F416C6C6F7744424E756C6C007365745F4974656D41727261790053716C446174614164617074657200446244617461416461707465720046696C6C005374727563744C61796F7574417474726962757465004C61796F75744B696E6400466C616773417474726962757465006F705F457175616C697479004E657874526573756C740053797374656D2E476C6F62616C697A6174696F6E0043756C74757265496E666F0047657443756C74757265496E666F004E756D626572466F726D6174496E666F006765745F4E756D626572466F726D6174004E756D6265725374796C65730049466F726D617450726F7669646572000000004154007900700065003A007B0030007D002C00200053006900740065003A007B0031007D002C00200055006E006900710075006500490064003A007B0032007D0000197B0030003A00440034007D007B0031003A00440039007D0000808355006E00610062006C0065006400200074006F0020006300720065006100740065002000740068006500200042006100720063006F00640065002E0020005300690074006500490064003A007B0030007D002C00200054007900700065003A007B0031007D002C00200055006E006900710075006500490064003A007B0032007D00000F7B0030003A004400310038007D00000D7B0030003A00440038007D00006B55006E00610062006C0065006400200074006F0020006300720065006100740065002000740068006500200042006100720063006F00640065002E00200054007900700065003A007B0030007D002C00200055006E006900710075006500490064003A007B0031007D0000257B0030003A00440034007D007B0031003A00440039007D007B0032003A00440032007D0000137B0030003A00440034007D007B0031007D00000F7B0030003A004400320030007D00001769006E0070007500740042007500660066006500720000495400720061006E00730066006F0072006D0042006C006F0063006B003A00200049006E00700075007400200062007500660066006500720020006900730020006E0075006C006C0000196F0075007400700075007400420075006600660065007200004B5400720061006E00730066006F0072006D0042006C006F0063006B003A0020004F0075007400700075007400200062007500660066006500720020006900730020006E0075006C006C00004F5400720061006E00730066006F0072006D0042006C006F0063006B003A00200050006100720061006D006500740065007200730020006F007500740020006F0066002000720061006E0067006500004742006C006F0063006B00530069007A0065003A0020004500720072006F007200200049006E00760061006C0069006400200042006C006F0063006B002000530069007A0065000033490056003A0020004500720072006F007200200049006E00760061006C00690064002000490056002000530069007A006500003F4D006F00640065003A0020004500720072006F007200200049006E00760061006C0069006400200043006900700068006500720020004D006F00640065000047500061006400640069006E0067003A0020004500720072006F007200200049006E00760061006C00690064002000500061006400640069006E00670020004D006F0064006500000F41005200430046004F0055005200000F61006C0067004E0061006D006500002F4300720065006100740065003A0020004500720072006F0072005F0050006100720061006D004E0075006C006C000007520043003400000D5200670062004B0065007900003F43007200650061007400650044006500630072007900700074006F0072003A0020005200670062006B006500790020006900730020006E0075006C006C00004343007200650061007400650044006500630072007900700074006F0072003A00200049006E00760061006C006900640020006B00650079002000730069007A006500004143007200650061007400650044006500630072007900700074006F0072003A00200049006E00760061006C00690064002000490056002000730069007A006500003B42006C006F0063006B00530069007A0065003A00200049006E00760061006C0069006400200062006C006F0063006B002000730069007A006500003143007200650061007400650044006500630072007900700074006F0072003A00200044006900730070006F0073006500000D7200670062004B0065007900003F43007200650061007400650044006500630072007900700074006F0072003A0020007200620067006B006500790020006900730020006E0075006C006C00004343007200650061007400650044006500630072007900700074006F0072003A00200049006E00760061006C006900640020004B00650079002000530069007A006500004143007200650061007400650044006500630072007900700074006F0072003A00200049006E00760061006C00690064002000490056002000530069007A006500001D530079007300740065006D002E0042006F006F006C00650061006E00001F530079007300740065006D002E004400610074006500540069006D006500001D530079007300740065006D002E0044006500630069006D0061006C00001B530079007300740065006D002E0044006F00750062006C0065000017530079007300740065006D002E0047007500690064000019530079007300740065006D002E0049006E007400310036000019530079007300740065006D002E0049006E007400330032000019530079007300740065006D002E0049006E00740036003400001B530079007300740065006D002E0053007400720069006E006700001B530079007300740065006D002E0042007900740065005B005D00002F63006F006E007400650078007400200063006F006E006E0065006300740069006F006E003D0074007200750065000017490064004F007000650072006100630069006F006E00000B4600650063006800610000134600650063006800610048006F0072006100000B46006F006C0069006F00000D46006F006C0069006F003200001B5400690070006F004F007000650072006100630069006F006E0000315400690070006F004F007000650072006100630069006F006E004400650073006300720069007000630069006F006E00001954006F00740061006C005200650063006100720067006100001145006D00700072006500730061004100001145006D00700072006500730061004200001349006D00700075006500730074006F003100001349006D00700075006500730074006F0032000021520065006300610072006700610050006F007200500075006E0074006F007300001D420061006C0061006E00630065004100460069006300680061007300001D4600690063006800610073004100420061006C0061006E0063006500001754006F00740061006C00520065007400690072006F00003154006F00740061006C005200650063006100720067006100530069006E0049006D00700075006500730074006F00730000154400650076006F006C007500630069006F006E00000D5000720065006D0069006F000009490053005200310000094900530052003200002143006100720067006F0050006F00720053006500720076006900630069006F0000295200650064006F006E00640065006F0050006F00720052006500740065006E00630069006F006E0000295200650064006F006E00640065006F0050006F00720044006500630069006D0061006C0065007300001B5000720065006D0069006F0045007300700065006300690065000021520065007400690072006F0045006E0045006600650063007400690076006F00001D520065007400690072006F0045006E00430068006500710075006500001D5400610072006A0065007400610043006C00690065006E007400650000114E006F004300750065006E0074006100000F4E006F006D00620072006500730000134100700065006C006C00690064006F0073000011530068006900660074005F00490064000013430061006A006500720061005F00490064000017430061006A0061004E006F006D006200720065007300001B430061006A0061004100700065006C006C00690064006F007300000F430061006A0061005F00490064000013430061006A0061005F004E0061006D0065000035530045004C00450043005400200020002000200043004D005F004F005000450052004100540049004F004E005F004900440020000025200020002000200020002000200020002C00200043004D005F00440041005400450020000025200020002000200020002000200020002C00200043004D005F00540059005000450020000031200020002000200020002000200020002C00200043004D005F004100440044005F0041004D004F0055004E00540020000031200020002000200020002000200020002C00200043004D005F005300550042005F0041004D004F0055004E0054002000008097200020002000200020002000200020002C00200043004D005F004100430043004F0055004E0054005F00490044002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002D002D0020004300750065006E00740061002000018097200020002000200020002000200020002C002000410043005F0048004F004C004400450052005F004E0041004D004500200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002D002D0020004E006F006D006200720065002000018091200020002000200020002000200020002C002000490053004E0055004C004C0020002800410043005F0048004F004C004400450052005F004900440031002C002000410043005F0048004F004C004400450052005F0049004400290020002000410043005F0048004F004C004400450052005F0049004400310020002000200020002D002D0020005200460043002000018093200020002000200020002000200020002C002000410043005F0048004F004C004400450052005F004900440032002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002D002D002000430055005200500020000180B1090020002000200020002000200020002C002000410043005F0048004F004C004400450052005F0041004400440052004500530053005F003000310020002B00200027002000270020002B002000410043005F0048004F004C004400450052005F0041004400440052004500530053005F003000320020002B00200027002000270020002B002000410043005F0048004F004C004400450052005F0041004400440052004500530053005F00300033000133200020002000200020002000200020002C002000410043005F0048004F004C004400450052005F00430049005400590020000031200020002000200020002000200020002C002000410043005F0048004F004C004400450052005F005A004900500020000031200020002000200020002000200020002C002000410043005F0054005200410043004B005F0044004100540041002000002D200020002000200020002000200020002D002D002000450056004900440045004E004300450020002D002D000180FD200020002000200020002000200020002C002000490053004E0055004C004C00280041004D0050005F0050004C0041005900450052005F004E0041004D0045002C0020002000410043005F0048004F004C004400450052005F004E0041004D004500290020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000450056004900440045004E00430045005F00410043005F0048004F004C004400450052005F004E0041004D0045002000200020002D002D00200043006F006E007300740061006E006300690061002E004E006F006D0062007200650020000180F7200020002000200020002000200020002C002000490053004E0055004C004C00280041004D0050005F0050004C0041005900450052005F004900440031002C00200020002000490053004E0055004C004C0020002800410043005F0048004F004C004400450052005F004900440031002C002000410043005F0048004F004C004400450052005F0049004400290029002000200020002000200020002000450056004900440045004E00430045005F00410043005F0048004F004C004400450052005F0049004400310020002000200020002D002D00200043006F006E007300740061006E006300690061002E0052004600430020000180F9200020002000200020002000200020002C002000490053004E0055004C004C00280041004D0050005F0050004C0041005900450052005F004900440032002C00200020002000410043005F0048004F004C004400450052005F004900440032002900200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000450056004900440045004E00430045005F00410043005F0048004F004C004400450052005F0049004400320020002000200020002D002D00200043006F006E007300740061006E006300690061002E0043005500520050002000018101200020002000200020002000200020002C00200041004D0050005F0044004F00430055004D0045004E0054005F0049004400310020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002D002D00200044006F00630075006D0065006E0074006F002000330037002D004100200049005300520031002000018101200020002000200020002000200020002C00200041004D0050005F0044004F00430055004D0045004E0054005F0049004400320020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002D002D00200044006F00630075006D0065006E0074006F002000330037002D004100200049005300520032002000018101200020002000200020002000200020002C00200063006F006E007600650072007400200028006200690074002C002000630061007300650020007700680065006E00200041004D0050005F004F005000450052004100540049004F004E005F004900440020006900730020006E0075006C006C0020007400680065006E0020003000200065006C007300650020003100200065006E006400290020002000450056004900440045004E0043004500200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000071200020002000200020002000200020002C002000490053004E0055004C004C00280041004D0050005F0050004C0041005900450052005F004E0041004D00450031002C002000270027002900200041004D0050005F0050004C0041005900450052005F004E0041004D004500310020000171200020002000200020002000200020002C002000490053004E0055004C004C00280041004D0050005F0050004C0041005900450052005F004E0041004D00450032002C002000270027002900200041004D0050005F0050004C0041005900450052005F004E0041004D004500320020000171200020002000200020002000200020002C002000490053004E0055004C004C00280041004D0050005F0050004C0041005900450052005F004E0041004D00450033002C002000270027002900200041004D0050005F0050004C0041005900450052005F004E0041004D004500330020000131200020002000200020002000200020002C00200043004D005F00530045005300530049004F004E005F004900440020000033200020002000200020002000200020002C00200043004D005F0043004100530048004900450052005F004E0041004D0045000023200020002000200020002000200020002C00200041004F005F0043004F0044004500006D200020002000200020002000200020002C002000490053004E0055004C004C002800410050004F005F0043004100530048005F005000410059004D0045004E0054002C002000300029002000410050004F005F0043004100530048005F005000410059004D0045004E0054000071200020002000200020002000200020002C002000490053004E0055004C004C002800410050004F005F0043004800450043004B005F005000410059004D0045004E0054002C002000300029002000410050004F005F0043004800450043004B005F005000410059004D0045004E0054000047200020002000200020002000200020002C002000640062006F002E004F00700065006E0069006E006700280030002C00200043004D005F0044004100540045002900200020000063200020002000200020002000200020002C002000640062006F002E0054007200610063006B00440061007400610054006F00450078007400650072006E0061006C002800410043005F0054005200410043004B005F00440041005400410029002000008095200020002000200020002000200020002C002000490053004E0055004C004C002800410043005F0048004F004C004400450052005F004E0041004D00450033002C00200027002700290020002B00200027002000270020002B002000490053004E0055004C004C002800410043005F0048004F004C004400450052005F004E0041004D00450034002C0020002700270029002000018095200020002000200020002000200020002C002000490053004E0055004C004C002800410043005F0048004F004C004400450052005F004E0041004D00450031002C00200027002700290020002B00200027002000270020002B002000490053004E0055004C004C002800410043005F0048004F004C004400450052005F004E0041004D00450032002C0020002700270029002000017B20002000460052004F004D00200020002000200043004100530048004900450052005F004D004F00560045004D0045004E0054005300200077006900740068002000280069006E0064006500780020002800490058005F0063006D005F006F007000650072006100740069006F006E005F0069006400290029000080A1200020002000200020002000200020002000200049004E004E004500520020004A004F0049004E0020004100430043004F0055004E00540053002000200020002000200020002000200020002000200020002000200020004F004E00200043004D005F004100430043004F0055004E0054005F00490044002000200020003D002000410043005F004100430043004F0055004E0054005F0049004400200020000080A520002000200020002000200020002000200020004C00450046005400200020004A004F0049004E0020004100430043004F0055004E0054005F004D0041004A004F0052005F005000520049005A00450053002000200020004F004E00200043004D005F004F005000450052004100540049004F004E005F004900440020003D00200041004D0050005F004F005000450052004100540049004F004E005F004900440020000080A320002000200020002000200020002000200020004C00450046005400200020004A004F0049004E0020004100430043004F0055004E0054005F004F005000450052004100540049004F004E005300200020002000200020004F004E00200043004D005F004F005000450052004100540049004F004E005F004900440020003D00200041004F005F004F005000450052004100540049004F004E005F004900440020000080A520002000200020002000200020002000200020004C00450046005400200020004A004F0049004E0020004100430043004F0055004E0054005F005000410059004D0045004E0054005F004F005200440045005200530020004F004E00200043004D005F004F005000450052004100540049004F004E005F004900440020003D002000410050004F005F004F005000450052004100540049004F004E005F00490044002000008099200057004800450052004500200020002000200043004D005F004F005000450052004100540049004F004E005F0049004400200049004E00200028002000530045004C004500430054002000200054004F00500020002800400070004E006F004F007000650072006100740069006F006E0073002900200043004D005F004F005000450052004100540049004F004E005F00490044002000001D400070004E006F004F007000650072006100740069006F006E007300001B400070004F007000650072006100740069006F006E004900640000808F20002000200020002000200020002000200020002000460052004F004D002000200043004100530048004900450052005F004D004F00560045004D0045004E0054005300200057004900540048002000280049004E0044004500580020002800490058005F0063006D005F006F007000650072006100740069006F006E005F00690064002900290020002000200000672000200020002000200020002000200020002000570048004500520045002000200043004D005F004F005000450052004100540049004F004E005F004900440020003D002000400070004F007000650072006100740069006F006E0049006400200020002000001F400070004F007000650072006100740069006F006E00460072006F006D00006B2000200020002000200020002000200020002000570048004500520045002000200043004D005F004F005000450052004100540049004F004E005F004900440020003E003D002000400070004F007000650072006100740069006F006E00460072006F006D0020002000004D2000200020002000200020002000200020002000570048004500520045002000200043004D005F004F005000450052004100540049004F004E005F0049004400200069006E00200028002000000B20002900200020002000001540007000460072006F006D00440061007400650000114000700054006F00440061007400650000808920002000200020002000200020002000200020002000460052004F004D002000200043004100530048004900450052005F004D004F00560045004D0045004E0054005300200057004900540048002000280049004E0044004500580020002800490058005F0063006D005F0064006100740065005F00740079007000650029002900200020002000007F2000200020002000200020002000200020002000570048004500520045002000200043004D005F00440041005400450020003E003D00200040007000460072006F006D004400610074006500200041004E004400200043004D005F00440041005400450020003C0020004000700054006F004400610074006500200020000039200020002300570048004500520045005F004100430043004F0055004E0054005F004D004F00560045004D0045004E005400530023002000007320002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000470052004F00550050002000420059002000200043004D005F004F005000450052004100540049004F004E005F0049004400200029002000003520004F005200440045005200200042005900200043004D005F004F005000450052004100540049004F004E005F00490044002000002D4000700054007900700065004400650076006F006C007500740069006F006E00530070006C006900740031000025400070005400790070006500430061006E00630065006C00530070006C006900740031000025400070005400790070006500430061007300680049006E00530070006C006900740031000029400070005400790070006500430061007300680049006E004D004200530070006C006900740031000029400070005400790070006500430061007300680049006E004E004100530070006C00690074003100003B4000700054007900700065005200650063006800610072006700650046006F00720050006F0069006E0074007300530070006C006900740031000025400070005400790070006500430061007300680049006E00530070006C006900740032000029400070005400790070006500430061007300680049006E004E004100530070006C006900740032000029400070005400790070006500430061007300680049006E004D004200530070006C00690074003200003B4000700054007900700065005200650063006800610072006700650046006F00720050006F0069006E0074007300530070006C006900740032000029400070005400790070006500430061007300680049006E00540061007800530070006C0069007400002B400070005400790070006500430061007300680049006E00540061007800530070006C00690074003200001B40007000540079007000650043006100730068004F007500740000194000700054007900700065005000720069007A006500730000234000700054007900700065005400610078004F006E005000720069007A006500310000234000700054007900700065005400610078004F006E005000720069007A0065003200002F4000700054007900700065005000720069007A00650049006E004B0069006E0064003100470072006F0073007300002D40007000540079007000650054006100780031004F006E005000720069007A0065004B0069006E0064003100002D40007000540079007000650054006100780032004F006E005000720069007A0065004B0069006E0064003100002F4000700054007900700065005000720069007A00650049006E004B0069006E0064003200470072006F0073007300002D40007000540079007000650054006100780031004F006E005000720069007A0065004B0069006E0064003200002D40007000540079007000650054006100780032004F006E005000720069007A0065004B0069006E0064003200002B400070005400790070006500420061006E006B0043006100720064005000610079006D0065006E0074000037400070005400790070006500420061006E006B0043006100720064005000610079006D0065006E0074004300720065006400690074000035400070005400790070006500420061006E006B0043006100720064005000610079006D0065006E00740044006500620069007400003B4000700043006F006D00700061006E00790042005400790070006500420061006E006B0043006100720064005000610079006D0065006E00740000474000700043006F006D00700061006E00790042005400790070006500420061006E006B0043006100720064005000610079006D0065006E00740043007200650064006900740000454000700043006F006D00700061006E00790042005400790070006500420061006E006B0043006100720064005000610079006D0065006E00740044006500620069007400002540007000540079007000650043006800650063006B005000610079006D0065006E00740000354000700043006F006D00700061006E0079004200540079007000650043006800650063006B005000610079006D0065006E007400003140007000540079007000650043006800650063006B005000610079006D0065006E00740052006500660075006E0064000025400070005400790070006500540061007800520065007400750072006E0069006E006700002B40007000540079007000650044006500630069006D0061006C0052006F0075006E00640069006E00670000394000700054007900700065005200650063006800610072006700650046006F00720050006F0069006E00740073005000720069007A006500002740007000540079007000650053006500720076006900630065004300680061007200670065000033400070005400790070006500500072006F006D006F00740069006F006E00520065006400650065006D00610062006C00650000214000700043006800690070007300530061006C00650054006F00740061006C00002940007000430068006900700073005000750072006300680061007300650054006F00740061006C000019400070005400790070006500430061007300680049006E00002D4000700054007900700065004400650076006F006C007500740069006F006E00530070006C006900740032000025400070005400790070006500430061006E00630065006C00530070006C0069007400320000314000700043006800690070007300530061006C0065005200650067006900730074006500720054006F00740061006C000021400070004F0043006F006400650043006800690070007300530061006C0065000039400070004F0043006F006400650043006800690070007300530061006C0065005700690074006800520065006300680061007200670065000029400070004F0043006F00640065004300680069007000730050007500720063006800610073006500003F400070004F0043006F00640065004300680069007000730050007500720063006800610073006500570069007400680043006100730068004F007500740000808D090041004E004400200020002000200043004D005F005400590050004500200049004E00280020004000700054007900700065004400650076006F006C007500740069006F006E00530070006C006900740031002C002000400070005400790070006500430061006E00630065006C00530070006C006900740031002000200020002000200020002000200000808D09002000200020002000200020002000200020002000200020002000200020002000200020002C004000700054007900700065004400650076006F006C007500740069006F006E00530070006C006900740032002C002000400070005400790070006500430061006E00630065006C00530070006C006900740032002000200020002000200020002000200000808F20002000200020002000200020002000200020002000200020002000200020002000200020002C004000700054007900700065005000720069007A00650073002C0020004000700054007900700065005400610078004F006E005000720069007A00650031002C0020004000700054007900700065005400610078004F006E005000720069007A00650032002000007B20002000200020002000200020002000200020002000200020002000200020002000200020002C00400070005400790070006500430061007300680049006E00530070006C006900740032002C002000400070005400790070006500430061007300680049006E004E004100530070006C00690074003200200000809120002000200020002000200020002000200020002000200020002000200020002000200020002C00400070005400790070006500430061007300680049006E004D004200530070006C006900740032002C0020004000700054007900700065005200650063006800610072006700650046006F00720050006F0069006E0074007300530070006C00690074003200200000808120002000200020002000200020002000200020002000200020002000200020002000200020002C00400070005400790070006500430061007300680049006E00540061007800530070006C00690074002C002000400070005400790070006500430061007300680049006E00540061007800530070006C006900740032002000004520002000200020002000200020002000200020002000200020002000200020002000200020002C0040007000540079007000650043006100730068004F007500740020000080A52000200020002000200020002000200020002C004000700054007900700065005000720069007A00650049006E004B0069006E0064003100470072006F00730073002C00200040007000540079007000650054006100780031004F006E005000720069007A0065004B0069006E00640031002C00200040007000540079007000650054006100780032004F006E005000720069007A0065004B0069006E006400310020000080A52000200020002000200020002000200020002C004000700054007900700065005000720069007A00650049006E004B0069006E0064003200470072006F00730073002C00200040007000540079007000650054006100780031004F006E005000720069007A0065004B0069006E00640032002C00200040007000540079007000650054006100780032004F006E005000720069007A0065004B0069006E006400320020000080D32000200020002000200020002000200020002C00400070005400790070006500420061006E006B0043006100720064005000610079006D0065006E0074002C00200020002000200020002000200020002000400070005400790070006500420061006E006B0043006100720064005000610079006D0065006E0074004300720065006400690074002C00200020002000200020002000200020002000400070005400790070006500420061006E006B0043006100720064005000610079006D0065006E0074004400650062006900740020000080E32000200020002000200020002000200020002C004000700043006F006D00700061006E00790042005400790070006500420061006E006B0043006100720064005000610079006D0065006E0074002C0020004000700043006F006D00700061006E00790042005400790070006500420061006E006B0043006100720064005000610079006D0065006E0074004300720065006400690074002C0020004000700043006F006D00700061006E00790042005400790070006500420061006E006B0043006100720064005000610079006D0065006E0074004400650062006900740020000080A72000200020002000200020002000200020002C0040007000540079007000650043006800650063006B005000610079006D0065006E0074002C0020004000700043006F006D00700061006E0079004200540079007000650043006800650063006B005000610079006D0065006E0074002C00200040007000540079007000650043006800650063006B005000610079006D0065006E00740052006500660075006E006400200000412000200020002000200020002000200020002C0040007000540079007000650044006500630069006D0061006C0052006F0075006E00640069006E0067002000003D2000200020002000200020002000200020002C0040007000540079007000650053006500720076006900630065004300680061007200670065002000004F2000200020002000200020002000200020002C004000700054007900700065005200650063006800610072006700650046006F00720050006F0069006E00740073005000720069007A00650020000080912000200020002000200020002000200020002C00400070005400790070006500430061007300680049006E00530070006C006900740031002C00400070005400790070006500430061007300680049006E004D004200530070006C006900740031002C002000400070005400790070006500430061007300680049006E004E004100530070006C0069007400310020000080852000200020002000200020002000200020002C004000700054007900700065005200650063006800610072006700650046006F00720050006F0069006E0074007300530070006C006900740031002C00400070005400790070006500500072006F006D006F00740069006F006E00520065006400650065006D00610062006C0065002000002F2000200020002000200020002000200020002C00400070005400790070006500430061007300680049006E00200000652000200020002000200020002000200020002C0020004000700043006800690070007300530061006C00650054006F00740061006C002C00200040007000430068006900700073005000750072006300680061007300650054006F00740061006C00200000492000200020002000200020002000200020002C0020004000700043006800690070007300530061006C0065005200650067006900730074006500720054006F00740061006C002000003B2000200020002000200020002000200020002C00400070005400790070006500540061007800520065007400750072006E0069006E0067002000000720002900200000332300570048004500520045005F004100430043004F0055004E0054005F004D004F00560045004D0045004E00540053002300001350005300410043006C00690065006E00740000355200650070006F0072007400520065006400650065006D00610062006C006500500072006F006D006F00740069006F006E007300000F5200650063006100720067006100001F560065006E00740061002000640065002000660069006300680061007300003F560065006E007400610020006400650020006600690063006800610073002000280063006F006E0020007200650069006E0074006500670072006F00290000095000610067006F00002143006F006D007000720061002000640065002000660069006300680061007300003D43006F006D0070007200610020006400650020006600690063006800610073002000280063006F006E00200072006500630061007200670061002900002946006F0072006D006100440065005000610067006F002E0045006600650063007400690076006F00001145006600650063007400690076006F00002D43006C006100760065005400690070006F005000610067006F002E0045006600650063007400690076006F00001B43006C006100760065005400690070006F005000610067006F0000808B430056005F00530045005300530049004F004E005F004900440020003D0020007B0030007D00200061006E0064002000430056005F004100430043004F0055004E0054005F004900440020003D0020007B0031007D00200061006E0064002000430056005F004F005000450052004100540049004F004E005F004900440020003D0020007B0032007D00001B430056005F0056004F00550043004800450052005F00490044000017430056005F00530045005100550045004E0043004500001B430056005F00530045005300530049004F004E005F00490044000015430056005F0055005300450052005F00490044000019470055005F00460055004C004C005F004E0041004D004500001B430056005F0043004100530048004900450052005F0049004400001F430056005F0043004100530048004900450052005F004E0041004D004500002943006C006100760065005400690070006F005000610067006F002E00430068006500710075006500002546006F0072006D006100440065005000610067006F002E00430068006500710075006500002B43006C006100760065005400690070006F005000610067006F002E005400610072006A00650074006100002746006F0072006D006100440065005000610067006F002E005400610072006A0065007400610000154D006F00760069006D00690065006E0074006F00000752004600430000094300550052005000000D4E006F006D00620072006500001344006F006D006900630069006C0069006F0000134C006F00630061006C006900640061006400001943006F006400690067006F0050006F007300740061006C00001D43006F006E007300740061006E006300690061002E00520046004300001F43006F006E007300740061006E006300690061002E004300550052005000002343006F006E007300740061006E006300690061002E004E006F006D00620072006500002943006F006E007300740061006E006300690061002E0044006F006D006900630069006C0069006F00002943006F006E007300740061006E006300690061002E004C006F00630061006C006900640061006400002F43006F006E007300740061006E006300690061002E0043006F006400690067006F0050006F007300740061006C00001543006F006E007300740061006E00630069006100001544006F00630075006D0065006E00740049004400001553006500730069006F006E00430061006A006100001743006100730068006900650072004E0061006D006500001D5000720065006D0069006F0043006100640075006300610064006F00001356006F0075006300680065007200490064000021530061006C0064006F00500072006F006D006F00630069006F006E0061006C00001346006F0072006D0061005000610067006F00002943006C006100760065005400690070006F005000610067006F005200650063006100720067006100002F43006C006100760065005400690070006F005000610067006F002E0045006E0045007300700065006300690065000007300030003300002B46006F0072006D006100440065005000610067006F002E0045006E004500730070006500630069006500001545006E0020004500730070006500630069006500001F20002000530045004C004500430054002000200054004F005000200028000003290000352000200020002000200020002000200020002000430056005F0056004F00550043004800450052005F00490044002000200020000035200020002000200020002000200020002C002000430056005F00530045005100550045004E0043004500200020002000200020000035200020002000200020002000200020002C002000430056005F00530045005300530049004F004E005F00490044002000200020000035200009002000200020002000200020002C002000430056005F004100430043004F0055004E0054005F00490044002000200020000035200009002000200020002000200020002C002000430056005F004F005000450052004100540049004F004E005F004900440020000035200009002000200020002000200020002C002000430056005F00530045005300530049004F004E005F00490044002000200020000035200009002000200020002000200020002C002000430056005F0055005300450052005F00490044002000200020002000200020000035200009002000200020002000200020002C002000470055005F00460055004C004C005F004E0041004D00450020002000200020000035200009002000200020002000200020002C002000430056005F0043004100530048004900450052005F00490044002000200020000035200009002000200020002000200020002C002000430056005F0043004100530048004900450052005F004E0041004D0045002000006B200020002000460052004F004D0020002000200043004100530048004900450052005F0056004F00550043004800450052005300200057004900540048002000280049004E004400450058002800490058005F006400610074006500740069006D00650029002900200000808B200020002000460052004F004D0020002000200043004100530048004900450052005F0056004F00550043004800450052005300200057004900540048002000280049004E004400450058002800490058005F00630076005F006F007000650072006100740069006F006E005F00690064005F006400610074006500740069006D006500290029002000006D2000200049004E004E00450052002000200020004A004F0049004E0020004700550049005F0055005300450052005300200020004F004E002000470055005F0055005300450052005F0049004400200020003D002000430056005F0055005300450052005F0049004400200000392000200057004800450052004500200020002000430056005F004F005000450052004100540049004F004E005F004900440020003D00200000032000003B2000200057004800450052004500200020002000430056005F004F005000450052004100540049004F004E005F004900440020003E003D002000003F2000200057004800450052004500200020002000430056005F004F005000450052004100540049004F004E005F0049004400200069006E0020002800200000492000200057004800450052004500200020002000430056005F004400410054004500540049004D00450020003E003D00200040007000460072006F006D00440061007400650020000049200020002000200041004E004400200020002000430056005F004400410054004500540049004D00450020003C00200020004000700054006F0044006100740065002000200020000080EB200020002000200041004E004400200020002000430056005F005400590050004500200049004E00200028004000700056006F007500630068006500720054007900700065005F00430061007300680049006E0041002C0020004000700056006F007500630068006500720054007900700065005F0043006100730068004F007500740041002C0020004000700056006F007500630068006500720054007900700065005F00430068006900700073004200750079002C0020004000700056006F007500630068006500720054007900700065005F0043006800690070007300530061006C0065002900002B4000700056006F007500630068006500720054007900700065005F00430061007300680049006E004100002D4000700056006F007500630068006500720054007900700065005F0043006100730068004F00750074004100002D4000700056006F007500630068006500720054007900700065005F0043006800690070007300420075007900002F4000700056006F007500630068006500720054007900700065005F0043006800690070007300530061006C006500000100052C00200000294100630063006F0075006E0074002E00560069007300690062006C0065004600690065006C006400000B4E0061006D0065003200000B4E0061006D00650034000080A72000530045004C004500430054002000470050005F00470052004F00550050005F004B00450059002C002000470050005F005300550042004A004500430054005F004B00450059002C002000490053004E0055004C004C002800470050005F004B00450059005F00560041004C00550045002C0020002700270029002000460052004F004D002000470045004E004500520041004C005F0050004100520041004D0053002000010720003B0020000080F32000530045004C0045004300540020004100500050005F004E0041004D0045002C0020005500500050004500520028004100500050005F004D0041004300480049004E00450029002C002000490053004E0055004C004C0028004100500050005F0041004C004900410053002C0020005500500050004500520028004100500050005F004D0041004300480049004E004500290029002000460052004F004D0020004100500050004C00490043004100540049004F004E00530020005700480045005200450020004100500050005F004E0041004D00450020003D002000270043004100530048004900450052002700200001032E0000152400240041004C00490041005300240024002E00000B65006E002D0055005300013B2000530045004C004500430054002000200020004F00560050005F004F005000450052004100540049004F004E005F0043004F00440045002000002F200020002000200020002000200020002C0020004F00560050005F00470045004E004500520041005400450020000029200020002000200020002000200020002C0020004F00560050005F005000520049004E00540020000033200020002000200020002000200020002C0020004F00560050005F005000520049004E0054005F0043004F00500059002000004F200020002000460052004F004D002000200020004F005000450052004100540049004F004E005F0056004F00550043004800450052005F0050004100520041004D00450054004500520053002000003520002000570048004500520045002000200020004F00560050005F0045004E00410042004C004500440020003D0020003100200000000000C1B979370E439043AFEFBE7E014F65310008B77A5C561934E0890615121D020E0E0A15121D021180A4128108020608030611080400000000040100000004020000000403000000040400000004010003000402000300040400030004FFFFFF0004FF00100F04FF00200F04FF00400F04FF00800F04FF00E01103061D050320000102060E02060A0320000E0320000804200011080320000A05200101120C080003120C11080A08070002020E10120C040001080E040001020E060002020E100B0720040108080A0E0800040208080A100E0A0004020E10081008100A0400010E0E07000302020E100E0328000E0328000804280011080328000A020605020602052001011D05032000020A2005081D0508081D05080820031D051D0508080328000204200101080420001D050520001D122104200011250520010111250420001129052001011129040000121405000112140E0428001D050528001D122104280011250428001129082002120D1D051D0504200101020306121804050000000406000000040700000004080000000409000000040A000000040B000000040C000000040D000000040E000000040F0000000410000000224D006F00760069006D00690065006E0074006F0073004300750065006E0074006100144D006F00760069006D00690065006E0074006F000C4300750065006E00740061002054006F00740061006C004D006F00760069006D00690065006E0074006F0073001045006600650063007400690076006F0004E8030000020603022C00040001010A050002010A08040001010E07000201112D112D05000101123105000111350E070002020A101231080003020A08101231070002020E1012310A000302112D112D1012310C00060111240A080E112D112D1100080211240A080E112D112D12391012310E00070211240A080E112D112D123D12000702081241123D123112810012451012310700011231101231050001011249090002124912491281001100080211240A080E112D112D1012311245030611240306112803061134030611380306113C0306114004FFFFFFFF03061144041400000004150000000416000000041700000004180000000419000000041C000000041D000000041E000000041F0000000420000000042100000004220000000423000000042400000004250000000426000000042700000004280000000429000000042A000000042B000000042C000000042D000000042E000000042F0000000430000000043100000004320000000433000000043400000004350000000436000000043700000004380000000439000000043A000000043B000000043C000000043D000000043E000000043F0000000440000000044100000004420000000443000000044400000004450000000446000000044700000004480000000449000000044A000000044B000000044C000000044D000000044E000000044F0000000450000000045400000004550000000456000000045700000004580000000459000000045A000000045B000000045C000000045D000000045E000000045F0000000460000000046100000004620000000463000000046400000004650000000466000000046700000004680000000469000000046A000000046B000000046C000000046D000000046E000000046F0000000470000000047100000004720000000473000000047400000004750000000476000000047700000004780000000479000000047A000000047B000000047C000000047D000000047E000000047F0000000480000000048B000000048C000000048D000000048E000000048F0000000490000000049100000004920000000493000000049400000004950000000496000000049700000004980000000499000000049A000000049B000000049C000000049D000000049E000000049F00000004C800000004C900000004CA00000004CB000000042B010000042C010000042D010000042E010000042F010000043001000004310100000432010000043301000004340100000437010000048F01000004F401000004F501000004FE01000004FF0100000400020000040102000004020200000403020000040402000004050200000406020000040702000004080200000409020000040A020000040B020000040C020000040D020000040E020000040F0200000410020000041102000004120200000413020000041402000004150200000416020000041702000004180200000419020000041A020000041B020000041C020000041D020000041E020000041F0200000420020000042102000004220200000423020000042402000004250200000426020000042702000004280200000440420F000480841E0004BFC62D0003061148041A000000041B0000000452000000045300000004CC00000004CD00000004CE00000004CF00000004D000000004D100000004D200000004D300000004D400000004D500000004D60000000306114C0306115003061154030611580306115C0306116003061164030611680306116C0400010000040004000004000800000400100000040020000004004000000400800000040000010004000002000400000400030611700306117404E703000004E903000004F2030000030611780306117C040611808004061180840406118088040611808C040611809004061180940406118098040611809C04061180A004061180A404110000000412000000041300000004061180A804061180AC04061180B00306114D04061280B8042000114D05200101114D042001010A042001010E05200011809C0620010111809C052000118098062001011180980520001280B8062001011280B8042800114D05280011809C0528001180980528001280B80306112D042000112D05200101112D0520001180A0062001011180A0042800112D0528001180A004061180BC04061180C004061180C404061180C804061180CC04061180D004061180D404061180D804061180DC04061180E004061180E404061180E804061180F004061180F40D00060E1180B00E0E0E0E128100122400240041004C004900410053002400240007000112810012450520020E0E0E082005080E0E080808052002080E0E062003080E0E08062003020E0E02052002020E0E082003114D0E0E114D062002114D0E0E0620030E0E0E0E0620030A0E0E080520020A0E0E070001128104124506200301020208062001011180890700040E0E1C1C1C04070111080600030E0E1C1C0500020E0E1C0507030E080E0907060B08080A110808040701120C0307010B0420010308040001020306070403020E080307010E050002020E0E0520020E08080400010A0E0407020E0E0520020E08030420010E080500011D050B0B07070B1D05080E051D05080B2003011280BD120D1180C1072003011D0508080600020B1D0508100707121C120D0B1D051280B51280B902030000010406118114090002011280D11180D50407011D050320001C021D050520001280D9052002010E0E05070305050806070405080808080003011280D10808040001011C062003010808080507011D12210707021280F51D05080003020E0E1180FD0620011D0E1D030607021D0E1D0305200012810108151281090112810D0520001281110800011280D9118119072003010E11350A052001011300082004010E11350505062002010E11350520001D1300072001011D12810D0500001281250620010112811D0520001281290420001D1C05200201081C26070C151281090112810D12811512810D12810D12810D12811D12490812811112111281111211070615121D020E080615121D020E0807200201130013010820020213001013010407020E080620011281150E07070312310812390500020808080420001245042000123905200101123905200101124504200012410E070612311281001245123D1241020620011281350E05200012813908200212813D0E1135042001011C0600030E0E0E0E0B070412813512813502112404200102080420010A0805200101124907000202114D114D0420001249052001112D080920060108080808080804200108080420011C0804061281510620011D12490E0420011C0E080002114D114D114D2307170A0A0A02124912311249114D0A112D0E0E0E080E0E0E0E080E1D12491180A411440600011280D90E0920021281150E1280D904200012310620011C128115072002011281151C0A07031281151281111211021D1C052001011D1C06070312490E0E082003010E1239124505200101123D0520010812310D0705128135123D128155021124062001011181610500020E0E0E0607020E1180B00700040E0E0E0E0E0F0705128100128135123D1241128100060002020E10080600011281690E05200012816D0D0004020E11817112817510114D0507020E114D060002020E100A0407020E0A0F0705128104128135123D12411281041201000D5753492E5769676F73424C303200000501000000000E0100094D6963726F736F667400002001001B436F7079726967687420C2A9204D6963726F736F6674203230313500002901002435363331323730362D343035632D343664352D386233332D33373734323133653464336100000C010007312E302E302E3000000801000200000000000801000800000000001E01000100540216577261704E6F6E457863657074696F6E5468726F77730100000000000000FBBED15500000000020000001C0100008057010080390100525344533104A4F3A462984499318289C7FF703201000000633A5C5446535C5769676F735C4465765C5769676F732053797374656D5C5753492E5769676F73424C30325C6F626A5C52656C656173655C5753492E5769676F73424C30322E70646200000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000C45801000000000000000000DE580100002000000000000000000000000000000000000000000000D0580100000000000000000000005F436F72446C6C4D61696E006D73636F7265652E646C6C0000000000FF25002000100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000100100000001800008000000000000000000000000000000100010000003000008000000000000000000000000000000100000000004800000058600100180300000000000000000000180334000000560053005F00560045005200530049004F004E005F0049004E0046004F0000000000BD04EFFE00000100000001000000000000000100000000003F000000000000000400000002000000000000000000000000000000440000000100560061007200460069006C00650049006E0066006F00000000002400040000005400720061006E0073006C006100740069006F006E00000000000000B00478020000010053007400720069006E006700460069006C00650049006E0066006F00000054020000010030003000300030003000340062003000000034000A00010043006F006D00700061006E0079004E0061006D006500000000004D006900630072006F0073006F0066007400000044000E000100460069006C0065004400650073006300720069007000740069006F006E00000000005700530049002E005700690067006F00730042004C00300032000000300008000100460069006C006500560065007200730069006F006E000000000031002E0030002E0030002E003000000044001200010049006E007400650072006E0061006C004E0061006D00650000005700530049002E005700690067006F00730042004C00300032002E0064006C006C0000005C001B0001004C006500670061006C0043006F007000790072006900670068007400000043006F0070007900720069006700680074002000A90020004D006900630072006F0073006F006600740020003200300031003500000000004C00120001004F0072006900670069006E0061006C00460069006C0065006E0061006D00650000005700530049002E005700690067006F00730042004C00300032002E0064006C006C0000003C000E000100500072006F0064007500630074004E0061006D006500000000005700530049002E005700690067006F00730042004C00300032000000340008000100500072006F006400750063007400560065007200730069006F006E00000031002E0030002E0030002E003000000038000800010041007300730065006D0062006C0079002000560065007200730069006F006E00000031002E0030002E0030002E0030000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000005001000C000000F03800000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
WITH PERMISSION_SET = SAFE
GO

CREATE PROCEDURE GetSalesAndPaymentsByOperation (@OperationId BigInt)
  AS EXTERNAL NAME [WSI.WigosBL02].[WSI.Common.SharedBL02Common].GetSalesAndPaymentsByOperation
GO

CREATE PROCEDURE GetSalesAndPaymentsByOperationList (@OperationIds nvarchar(max))
  AS EXTERNAL NAME [WSI.WigosBL02].[WSI.Common.SharedBL02Common].GetSalesAndPaymentsByOperationList
GO

CREATE PROCEDURE GetSalesAndPaymentsByQtyOperations (@OperationId BigInt, @NoOperations Int)
  AS EXTERNAL NAME [WSI.WigosBL02].[WSI.Common.SharedBL02Common].GetSalesAndPaymentsByQtyOperations
GO

CREATE PROCEDURE GetOperationFromBarcode (@Barcode NVARCHAR(20))
  AS EXTERNAL NAME [WSI.WigosBL02].[WSI.Common.Barcode].GetOperationFromBarcode
GO
