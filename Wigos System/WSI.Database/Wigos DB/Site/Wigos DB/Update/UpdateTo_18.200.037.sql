/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 199;

SET @New_ReleaseId = 200;
SET @New_ScriptName = N'UpdateTo_18.200.037.sql';
SET @New_Description = N'Updated Cashier sessions by history and index in account_promotions.';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/******* INDEXES *******/

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[account_promotions]') AND name = N'IX_acp_promo_id_account_date_ini_balance')
  DROP INDEX [IX_acp_promo_id_account_date_ini_balance] ON [dbo].[account_promotions] WITH ( ONLINE = OFF )
GO

IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[account_promotions]') AND name = N'IX_acp_promo_id_account_date')
  CREATE NONCLUSTERED INDEX [IX_acp_promo_id_account_date] ON [dbo].[account_promotions] 
  (
	  [acp_promo_id] ASC,
	  [acp_account_id] ASC,
	  [acp_promo_date] ASC
  )
  INCLUDE ( [acp_ini_balance]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

/******* STORED PROCEDURES *******/

--UPDATE SP CashierMovementsHistory
ALTER PROCEDURE [dbo].[CashierMovementsHistory]        
                        @pSessionId             BIGINT
                       ,@MovementId             BIGINT
                       ,@pType                  INT    
                       ,@pSubType               INT
                       ,@pInitialBalance        MONEY 
                       ,@pAddAmount             MONEY
                       ,@pSubAmmount            MONEY
                       ,@pFinalBalance          MONEY
                       ,@pCurrencyCode          NVARCHAR(3)
                       ,@pAuxAmount             MONEY
                       ,@pCurrencyDenomination  MONEY
AS
BEGIN 
  DECLARE @_idx_try int
  DECLARE @_row_count int

  SET @pCurrencyDenomination = ISNULL(@pCurrencyDenomination, 0)
  
/******** Grouped by ID ***************/

  SET @_idx_try = 0

  WHILE @_idx_try < 2
  BEGIN
    UPDATE   CASHIER_MOVEMENTS_GROUPED_BY_SESSION_ID
       SET   CM_TYPE_COUNT            = CM_TYPE_COUNT      + 1      
           , CM_SUB_AMOUNT            = CM_SUB_AMOUNT      + ISNULL(@pSubAmmount, 0)
           , CM_ADD_AMOUNT            = CM_ADD_AMOUNT      + ISNULL(@pAddAmount, 0)
           , CM_AUX_AMOUNT            = CM_AUX_AMOUNT      + ISNULL(@pAuxAmount, 0)
           , CM_INITIAL_BALANCE       = CM_INITIAL_BALANCE + ISNULL(@pInitialBalance, 0)
           , CM_FINAL_BALANCE         = CM_FINAL_BALANCE   + ISNULL(@pFinalBalance, 0)
     WHERE   CM_SESSION_ID            = @pSessionId 
       AND   CM_TYPE                  = @pType 
       AND   CM_SUB_TYPE              = @pSubType 
       AND   CM_CURRENCY_ISO_CODE     = @pCurrencyCode
       AND   CM_CURRENCY_DENOMINATION = @pCurrencyDenomination

    SET @_row_count = @@ROWCOUNT
   
    IF @_row_count = 1 BREAK

    IF @_idx_try = 1
    BEGIN
      RAISERROR ('Update into historicized table failed. Table = [cashier_movements_grouped_by_session_id]', 11, 0) 
    END

    BEGIN TRY
      -- Entry doesn't exists, lets insert that
      INSERT INTO   CASHIER_MOVEMENTS_GROUPED_BY_SESSION_ID
                  ( CM_SESSION_ID
                  , CM_TYPE
                  , CM_SUB_TYPE
                  , CM_TYPE_COUNT
                  , CM_CURRENCY_ISO_CODE
                  , CM_CURRENCY_DENOMINATION
                  , CM_SUB_AMOUNT
                  , CM_ADD_AMOUNT
                  , CM_AUX_AMOUNT
                  , CM_INITIAL_BALANCE
                  , CM_FINAL_BALANCE)
           VALUES
                  ( @pSessionId
                  , @pType        
                  , @pSubType
                  , 1  
                  , @pCurrencyCode
                  , @pCurrencyDenomination
                  , ISNULL(@pSubAmmount, 0)
                  , ISNULL(@pAddAmount, 0)
                  , ISNULL(@pAuxAmount, 0)             
                  , ISNULL(@pInitialBalance, 0)
                  , ISNULL(@pFinalBalance, 0))

                  SET @_row_count = @@ROWCOUNT

      -- All ok, exit WHILE      
      BREAK

    END TRY
    BEGIN CATCH
    END CATCH

    SET @_idx_try = @_idx_try + 1
  END
  
  IF @_row_count = 0
  BEGIN
    RAISERROR ('Insert into historicized table failed. Table = [cashier_movements_grouped_by_session_id]', 11, 0) 
  END

  -- AJQ & RCI & RMS 10-FEB-2014: Movements historicized by hour are not inserted here. They serialize all workers and reduce performance.
  --                              A pending cashier movement to historify will be inserted in table CASHIER_MOVEMENTS_PENDING_HISTORY.
  --                              Later a thread will historify these pending movements.
  INSERT INTO CASHIER_MOVEMENTS_PENDING_HISTORY (CMPH_MOVEMENT_ID, CMPH_SUB_TYPE) VALUES (@MovementId, @pSubType) 

END --[CashierMovementsHistory]
GO

--SET DEFAULT = 1 CS_HISTORY FLAG
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_cashier_sessions_cs_history]') AND type = 'D')
  ALTER TABLE [dbo].[cashier_sessions] DROP CONSTRAINT [DF_cashier_sessions_cs_history]
GO
ALTER TABLE [dbo].[cashier_sessions] ADD  CONSTRAINT [DF_cashier_sessions_cs_history]  DEFAULT ((1)) FOR [cs_history]
GO

--UPDATE CURRENT CASHIER_SESSIONS HISTORIFY FLAG
UPDATE CASHIER_SESSIONS SET CS_HISTORY = 0
GO
