/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 388;

SET @New_ReleaseId = 389;
SET @New_ScriptName = N'UpdateTo_18.389.041.sql';
SET @New_Description = N'zsp_CardInfo'; 

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/**** GENERAL PARAM *****/

/******* TABLES  *******/

/******* RECORDS *******/

/******* PROCEDURES *******/


IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'zsp_CardInfo')
       DROP PROCEDURE [zsp_CardInfo]
GO
CREATE PROCEDURE [dbo].[zsp_CardInfo]
    @TrackData			varchar(24)

WITH EXECUTE AS OWNER
AS

BEGIN
                  
BEGIN TRANSACTION
	DECLARE @Owner				nvarchar(250)
	DECLARE @Account			bigint
	DECLARE @CardIndex			int 	
	DECLARE @CustumerId			bigint 	
	DECLARE @AccountType		int 	
	DECLARE @CardStatus			int 	
	DECLARE @Status_code  		int
	DECLARE @Status_text  		varchar (254)
	DECLARE @Exception   		bit
	DECLARE @Error_text   		nvarchar (MAX)
	DECLARE @Input        		nvarchar(MAX)
	DECLARE @Output       		nvarchar(MAX)

	SET @Status_code = 1
	SET @Status_text = 'TrackData wrong'
	SET @Account = 0
	SET @CardIndex = 0
	SET @CustumerId = 0
	SET @AccountType = 0
	SET @CardStatus = 0
	      
	BEGIN
		BEGIN TRY      
			IF @TrackData IS NOT NULL  
			BEGIN
				SELECT	@Account		= AC_ACCOUNT_ID, 
						@Owner			= LTRIM(RTRIM(ISNULL(ac_holder_name,'')))
				FROM	ACCOUNTS
				WHERE	AC_TRACK_DATA	= dbo.TrackDataToInternal(@TrackData)
			    
				IF @Account > 0
					BEGIN
						SET @CardIndex 		= 1
						SET @CustumerId 	= @Account
						SET @CardStatus		= 1
						SET @Status_code	= 0
						SET @Status_text	= 'OK'
						  
						 IF LEN(@Owner) = 0 	SET @AccountType = 0
						 ELSE 					SET @AccountType = 1
					  END
			  END   
		END TRY
		BEGIN CATCH
			  ROLLBACK TRANSACTION
				SET @Exception  = 1;
				SET @status_code = 4;
				SET @status_text = 'Access Denied';
				SET @Error_text  = ' ERROR_NUMBER: '    + CAST(ERROR_NUMBER()               AS NVARCHAR)
								 + ' ERROR_SEVERITY: '  + CAST(ERROR_SEVERITY()             AS NVARCHAR)
								 + ' ERROR_STATE: '     + CAST(ERROR_STATE()                AS NVARCHAR)
								 + ' ERROR_PROCEDURE: ' + CAST(ISNULL(ERROR_PROCEDURE(),'') AS NVARCHAR)
								 + ' ERROR_LINE: '      + CAST(ERROR_LINE()                 AS NVARCHAR)
								 + ' ERROR_MESSAGE: '   + CAST(ERROR_MESSAGE()              AS NVARCHAR(MAX))

			  BEGIN TRANSACTION
		END CATCH
	END	  
	  
	  
	SET @Input = '@TrackData='       + @TrackData 
	SET @Output = 'StatusCode='      + CAST (@Status_code     AS NVARCHAR)
				  +';StatusText='      + @Status_text
	              
	EXECUTE dbo.zsp_Audit 'zsp_CardInfo', @CustumerId, 0, 0, 0, 0, @status_code, 0, 1, @Input, @Output
	COMMIT TRANSACTION 
      
	SELECT	@Status_code	AS StatusCode, 
			@Status_text	AS StatusText, 
			@CardIndex		AS CardIndex, 
			@CustumerId		AS CustomerId, 
			@AccountType	AS AccountType, 
			@CardStatus		AS CardStatus
     
END --Zsp_GetCardInfo

				  
GO

IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'S2S_CardInfo')
       DROP PROCEDURE [S2S_CardInfo]
GO

CREATE PROCEDURE [dbo].[S2S_CardInfo]
      @TrackData       varchar(24)
      
WITH EXECUTE AS OWNER
AS

BEGIN
      EXECUTE dbo.[zsp_CardInfo] @TrackData  
END

GO


GRANT EXECUTE ON [dbo].[zsp_CardInfo] TO [3GS] WITH GRANT OPTION 
GO
GRANT EXECUTE ON [dbo].[S2S_CardInfo] TO [3GS] WITH GRANT OPTION 
GO

GRANT EXECUTE ON [dbo].[zsp_SessionUpdate] TO [3GS] WITH GRANT OPTION 
GO
GRANT EXECUTE ON [dbo].[zsp_ReportMeters] TO [3GS] WITH GRANT OPTION 
GO

-- [EIBE]
GRANT EXECUTE ON [dbo].[zsp_CardInfo] TO [EIBE] WITH GRANT OPTION 
GO
GRANT EXECUTE ON [dbo].[S2S_CardInfo] TO [EIBE] WITH GRANT OPTION 
GO

GRANT EXECUTE ON [dbo].[zsp_SessionUpdate] TO [EIBE] WITH GRANT OPTION 
GO
GRANT EXECUTE ON [dbo].[zsp_ReportMeters] TO [EIBE] WITH GRANT OPTION 
GO

-- [wg_interface]
GRANT EXECUTE ON [dbo].[zsp_CardInfo] TO [wg_interface] WITH GRANT OPTION 
GO
GRANT EXECUTE ON [dbo].[S2S_CardInfo] TO [wg_interface] WITH GRANT OPTION 
GO

GRANT EXECUTE ON [dbo].[zsp_SessionUpdate] TO [wg_interface] WITH GRANT OPTION 
GO
GRANT EXECUTE ON [dbo].[zsp_ReportMeters] TO [wg_interface] WITH GRANT OPTION 
GO

/******* TRIGGERS *******/
