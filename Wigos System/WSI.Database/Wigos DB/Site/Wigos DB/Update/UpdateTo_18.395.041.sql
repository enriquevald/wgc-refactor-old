/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 394;

SET @New_ReleaseId = 395;
SET @New_ScriptName = N'UpdateTo_18.395.041.sql';
SET @New_Description = N''; 

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/**** GENERAL PARAM *****/
IF EXISTS(SELECT 1 
            FROM GENERAL_PARAMS 
           WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration.02' 
             AND GP_SUBJECT_KEY = 'IfTimeOutExpiresParticipateInDraw')
BEGIN
  UPDATE GENERAL_PARAMS 
     SET GP_SUBJECT_KEY = 'TerminalGameFirsIfTimeOutExpiresParticipateInDraw'
   WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration.02' 
     AND GP_SUBJECT_KEY = 'IfTimeOutExpiresParticipateInDraw'
END
GO
/******* TABLES  *******/

/******* RECORDS *******/

/******* PROCEDURES *******/

IF OBJECT_ID('sp_CheckCashDeskDrawConfig', 'P') IS NOT NULL
DROP PROC sp_CheckCashDeskDrawConfig
GO
create PROCEDURE [dbo].[sp_CheckCashDeskDrawConfig]
      -- Add the parameters for the stored procedure here
      @pID INT = 0,
      @pName VARCHAR(20) = ''
AS
BEGIN
SET NOCOUNT ON;

DELETE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CashDesk.Draw.00'
DELETE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration.00'
   
DECLARE @POSTFIJ AS VARCHAR(5)

IF @pID = 0
BEGIN
  SET @POSTFIJ = ''
END
ELSE 
BEGIN 
  SET @POSTFIJ = '.' + RIGHT( '00' + CONVERT(VARCHAR(2),@pID),2)
END


  --'CashDesk.Draw', 'Enabled', 0
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ
                        AND GP_SUBJECT_KEY = 'Enabled'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'Enabled', '0')
END


--'CashDesk.Draw', 'ShowCashDeskDraws', 0
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ
                        AND GP_SUBJECT_KEY = 'ShowCashDeskDraws'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'ShowCashDeskDraws', '0')
END


--'CashDesk.Draw', 'VoucherOnCashDeskDraws.Winner', 0
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'VoucherOnCashDeskDraws.Winner'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'VoucherOnCashDeskDraws.Winner', '0')
END


--'CashDesk.Draw', 'WinnerPrizeVoucherTitle', 'Sorteo en XXXX'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrizeVoucherTitle'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'WinnerPrizeVoucherTitle', 'Sorteo en ' + @pName)
END


--'CashDesk.Draw', 'VoucherOnCashDeskDraws.Loser', 0
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'VoucherOnCashDeskDraws.Loser'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'VoucherOnCashDeskDraws.Loser', '0')
END


--'CashDesk.Draw', 'AskForParticipation', 0
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'AskForParticipation'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'AskForParticipation', '0')
END


--'CashDesk.Draw', 'LoserPrizeVoucherTitle','Cortes�a sorteo en XXXX'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'LoserPrizeVoucherTitle'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'LoserPrizeVoucherTitle', 'Cortes�a sorteo en ' + @pName)
END


--'CashDesk.Draw', 'ActionOnServerDown','0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'ActionOnServerDown'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'ActionOnServerDown', '0')
END


--'CashDesk.Draw', 'LocalServer', '1'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'LocalServer'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'LocalServer', '1')
END


--'CashDesk.Draw', 'ServerBDConnectionString',''
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'ServerBDConnectionString'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'ServerBDConnectionString', '')
END


--'CashDesk.Draw', 'ServerAddress1',''
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'ServerAddress1'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'ServerAddress1', '')
END


--'CashDesk.Draw', 'ServerAddress2',''
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'ServerAddress2'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'ServerAddress2', '')
END


--'CashDesk.DrawConfiguration', 'BallsExtracted','0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'BallsExtracted'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'BallsExtracted', '0')
END


--'CashDesk.DrawConfiguration', 'BallsOfParticipant', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'BallsOfParticipant'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'BallsOfParticipant', '0')
END


--'CashDesk.DrawConfiguration', 'TotalsBallsNumber', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'TotalsBallsNumber'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'TotalsBallsNumber', '0')
END


--'CashDesk.DrawConfiguration', 'LoserPrize1.Fixed', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'LoserPrize1.Fixed'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'LoserPrize1.Fixed', '0')
END


--'CashDesk.DrawConfiguration', 'LoserPrize1.Percentage', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'LoserPrize1.Percentage'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'LoserPrize1.Percentage', '0')
END


--'CashDesk.DrawConfiguration', 'LoserPrize1.Enabled', 0
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'LoserPrize1.Enabled'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'LoserPrize1.Enabled', '0')
END


--'CashDesk.DrawConfiguration', 'NumberOfParticipants', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'NumberOfParticipants'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'NumberOfParticipants', '0')
END


--'CashDesk.DrawConfiguration', 'NumberOfWinners', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'NumberOfWinners'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'NumberOfWinners', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize1.Fixed', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize1.Fixed'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize1.Fixed', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize1.Percentage', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize1.Percentage'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize1.Percentage', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize1.Enabled', 0
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize1.Enabled'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize1.Enabled', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize2.Fixed', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize2.Fixed'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize2.Fixed', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize2.Percentage', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize2.Percentage'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize2.Percentage', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize2.Enabled', 0
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize2.Enabled'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize2.Enabled', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize3.Fixed', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize3.Fixed'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize3.Fixed', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize3.Percentage', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize3.Percentage'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize3.Percentage', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize3.Enabled', 0
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize3.Enabled'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize3.Enabled', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize4.Fixed', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize4.Fixed'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize4.Fixed', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize4.Percentage', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize4.Percentage'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize4.Percentage', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize4.Enabled', 0
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize4.Enabled'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize4.Enabled', '0')
END



--'CashDesk.DrawConfiguration', 'WinnerPrize1.PrizeType', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize1.PrizeType'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize1.PrizeType', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize2.PrizeType', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize2.PrizeType'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize2.PrizeType','0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize3.PrizeType', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize3.PrizeType'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize3.PrizeType', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize4.PrizeType', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize4.PrizeType'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize4.PrizeType', '0')
END


--'CashDesk.DrawConfiguration', 'LoserPrize1.PrizeType', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'LoserPrize1.PrizeType'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'LoserPrize1.PrizeType', '0')
END


-- 'CashDesk.Draw', 'AccountingMode', ''
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'AccountingMode'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'AccountingMode', '0')
END


-- 'CashDesk.DrawConfiguration', 'ParticipationPrice', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'ParticipationPrice'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'ParticipationPrice', '0')
END   
      
      
-- 'CashDesk.Draw', 'IsCashDeskDraw', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'IsCashDeskDraw'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'IsCashDeskDraw', '0')
END         


-- 'CashDesk.Draw', 'ReportUNRFromSP_SalesAndPayment', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'ReportUNRFromSP_SalesAndPayment'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'ReportUNRFromSP_SalesAndPayment', '0')
END   


-- 'CashDesk.Draw', 'AccountingMode.PromoNameMovement', 'PROMOCI�N'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'AccountingMode.PromoNameMovement'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'AccountingMode.PromoNameMovement', 'Promoci�n')
END   

-- 'CashDesk.Draw', 'AccountingMode.KindOfNameMovement', 'Premio en especie'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'AccountingMode.KindOfNameMovement'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'AccountingMode.KindOfNameMovement', 'Premio en especie')
END   

-- 'CashDesk.Draw', 'AccountingMode.RE.KindOfNameMovement', 'Premio en especie (RE)'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'AccountingMode.RE.KindOfNameMovement'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'AccountingMode.RE.KindOfNameMovement', 'Premio en especie (RE)')
END   

-- 'CashDesk.Draw', 'AccountingMode.RE.PromoNameMovement', 'Promoci�n (RE)'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'AccountingMode.RE.PromoNameMovement'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'AccountingMode.RE.PromoNameMovement', 'Promoci�n (RE)')
END   


-- 'CashDesk.Draw', 'Voucher.HideCurrencySymbol', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'Voucher.HideCurrencySymbol'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'Voucher.HideCurrencySymbol', '0')
END   
      
-- 'CashDesk.Draw', 'Voucher.LoserPrizeLabel', 'Cortesia'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'Voucher.LoserPrizeLabel'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'Voucher.LoserPrizeLabel', 'Cortesia')
END   
      
-- 'CashDesk.Draw', 'Voucher.UNRLabel', 'FD'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'Voucher.UNRLabel'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'Voucher.UNRLabel', 'FD')
END   

-- 'CashDesk.Draw', 'Voucher.WinnerPrizeLabel', 'Premio'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'Voucher.WinnerPrizeLabel'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'Voucher.WinnerPrizeLabel', 'Premio')
END   


--/// Probability


IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'LoserPrize1.Probability'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'LoserPrize1.Probability', '0')
END

IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize1.Probability'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize1.Probability', '0')
END

IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize2.Probability'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize2.Probability', '0')
END

IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize3.Probability'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize3.Probability', '0')
END

IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'WinnerPrize4.Probability'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize4.Probability', '0')
END


IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'ParticipationMaxPrice'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'ParticipationMaxPrice', '1')
END


IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameUrl'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameUrl', '')
END


IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameTimeout'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameTimeout', '30')
END

IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameName'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameName', 'Terminal Draw')
END

IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameInitialDrawScreenMessageLine0'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameInitialDrawScreenMessageLine0', '')
END

IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameInitialDrawScreenMessageLine1'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameInitialDrawScreenMessageLine1', 'Bienvenido')
END

IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameInitialDrawScreenMessageLine2'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameInitialDrawScreenMessageLine2', 'Pulse[1][2][3][4][5]')
END

--
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameResultDrawScreenMessageLine0'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameResultDrawScreenMessageLine0', 'Su saldo es de ')
END


IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameResultDrawScreenMessageLine1'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameResultDrawScreenMessageLine1', '')
END


IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameResultDrawScreenMessageLine2'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameResultDrawScreenMessageLine2', '')
END


IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameFirstDrawScreenMessageLine0'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameFirstDrawScreenMessageLine0', '')
END


IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameFirstDrawScreenMessageLine1'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameFirstDrawScreenMessageLine1', 'Pulse [1] para jugar')
END


IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameFirstDrawScreenMessageLine2'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameFirstDrawScreenMessageLine2', '')
END


IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameFirstDrawScreenTimeOut'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameFirstDrawScreenTimeOut', '10')
END


IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameFirsIfTimeOutExpiresParticipateInDraw'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameFirsIfTimeOutExpiresParticipateInDraw', '0')
END


IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'CloseOpenedSessionsBeforeNewGame'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'CloseOpenedSessionsBeforeNewGame', '0')
END
      
END
GO

GRANT EXECUTE ON [dbo].[sp_CheckCashDeskDrawConfig] TO [wggui]
GO

/******* TRIGGERS *******/
