/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 146;

SET @New_ReleaseId = 147;
SET @New_ScriptName = N'UpdateTo_18.147.027.sql';
SET @New_Description = N'Groups - Add columns';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END
 
/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/
DELETE FROM GENERAL_PARAMS WHERE gp_group_key = 'WCP'      AND gp_subject_key = 'Groups.WaitMinutesToExpand'
DELETE FROM GENERAL_PARAMS WHERE gp_group_key = 'WCP'      AND gp_subject_key = 'Groups.MaxMinutesToExpandGroups'
DELETE FROM GENERAL_PARAMS WHERE gp_group_key = 'WigosGUI' AND gp_subject_key = 'GroupsEnabled'
GO

INSERT INTO [dbo].[general_params] ([gp_group_key] ,[gp_subject_key],[gp_key_value]) VALUES ('WCP', 'Groups.CheckChangesAfterMinutes', '5');
INSERT INTO [dbo].[general_params] ([gp_group_key] ,[gp_subject_key],[gp_key_value]) VALUES ('WCP', 'Groups.AutoExpandAfterMinutes'  , '60');
GO

-- DELETE GP
DELETE FROM general_params WHERE gp_group_key = 'CashDesk.Draw' AND gp_subject_key = 'Voucher.HideCashDeskDrawWinner'
DELETE FROM general_params WHERE gp_group_key = 'CashDesk.Draw' AND gp_subject_key = 'Voucher.HideCashDeskDrawLoser'
GO

-- INSERT GP
IF  NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.Voucher' AND GP_SUBJECT_KEY ='VoucherOnPromotion.HideCashDeskDrawWinner')
INSERT INTO [dbo].[GENERAL_PARAMS]  ([GP_group_key]    ,[GP_subject_key]                            ,[GP_key_value])
       VALUES                       ('Cashier.Voucher' ,'VoucherOnPromotion.HideCashDeskDrawWinner' ,'1');
GO

IF  NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.Voucher' AND GP_SUBJECT_KEY ='VoucherOnPromotion.HideCashDeskDrawLoser')
INSERT INTO [dbo].[GENERAL_PARAMS]  ([GP_group_key]    ,[GP_subject_key]                           ,[GP_key_value])
     VALUES                         ('Cashier.Voucher' ,'VoucherOnPromotion.HideCashDeskDrawLoser' ,'1');
GO

--
-- New description for USD
--
UPDATE DBO.CURRENCY_EXCHANGE SET CE_DESCRIPTION = 'D�lar USA' WHERE CE_TYPE = 0 AND CE_CURRENCY_ISO_CODE = 'USD'
GO
