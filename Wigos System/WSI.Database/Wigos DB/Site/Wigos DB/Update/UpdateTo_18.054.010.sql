/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 53;

SET @New_ReleaseId = 54;
SET @New_ScriptName = N'UpdateTo_18.054.010.sql';
SET @New_Description = N'New columns in table site_jackpot_parameters and new table reports.';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/
/* site_jackpot_parameters.sjp_show_winner_name */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[site_jackpot_parameters]') and name = 'sjp_show_winner_name')
  ALTER TABLE [dbo].[site_jackpot_parameters]
          ADD [sjp_show_winner_name] [bit] NOT NULL CONSTRAINT [DF_site_jackpot_parameters_sjp_show_winner_name] DEFAULT ((0))
ELSE
  SELECT '***** Field site_jackpot_parameters.sjp_show_winner_name already exists *****';
GO

/* site_jackpot_parameters.sjp_show_terminal_name */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[site_jackpot_parameters]') and name = 'sjp_show_terminal_name')
  ALTER TABLE [dbo].[site_jackpot_parameters]
          ADD [sjp_show_terminal_name] [bit] NOT NULL CONSTRAINT [DF_site_jackpot_parameters_sjp_show_terminal_name] DEFAULT ((0))
ELSE
  SELECT '***** Field site_jackpot_parameters.sjp_show_terminal_name already exists *****';
GO

/* reports */
CREATE TABLE [dbo].[reports](
	[rep_type] [int] NOT NULL,
	[rep_date] [datetime] NOT NULL,
	[rep_data] [xml] NOT NULL,
	[rep_created] [datetime] NOT NULL CONSTRAINT [DF_reports_rep_created]  DEFAULT (getdate()),
 CONSTRAINT [PK_reports] PRIMARY KEY CLUSTERED 
(
	[rep_date] ASC,
	[rep_type] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
