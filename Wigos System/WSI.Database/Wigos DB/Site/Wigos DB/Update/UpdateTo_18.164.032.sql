/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 163;

SET @New_ReleaseId = 164;
SET @New_ScriptName = N'UpdateTo_18.164.032.sql';
SET @New_Description = N'PlayerTracking Procedure updated. Added multisite_requests indexes';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/

/****** INDEXES ******/

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[multisite_requests]') AND name = N'IX_ms_status_priority')
  DROP INDEX [IX_ms_status_priority] ON [dbo].[multisite_requests] WITH ( ONLINE = OFF )
GO

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[multisite_requests]') AND name = N'IX_mr_status_changed_mr_status_included_all')
  DROP INDEX [IX_mr_status_changed_mr_status_included_all] ON [dbo].[multisite_requests] WITH ( ONLINE = OFF )
GO

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[multisite_requests]') AND name = N'IX_mr_status_included_mr_input_data')
  DROP INDEX [IX_mr_status_included_mr_input_data] ON [dbo].[multisite_requests] WITH ( ONLINE = OFF )
GO

--
-- NEW INDEXES
--
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[multisite_requests]') AND name = N'IX_mr_status')
  CREATE NONCLUSTERED INDEX [IX_mr_status] ON [dbo].[multisite_requests] 
  (
        [mr_status] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[multisite_requests]') AND name = N'IX_mr_status_changed_mr_status')
  CREATE NONCLUSTERED INDEX [IX_mr_status_changed_mr_status] ON [dbo].[multisite_requests] 
  (
        [mr_status_changed] ASC,
        [mr_status] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

/****** RECORDS ******/

--
-- RECORDS FOR PSA Client
--
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PSAClient' AND GP_SUBJECT_KEY ='Constancias.ReportarCURP')
  INSERT INTO [dbo].[GENERAL_PARAMS]
             ([GP_group_key] ,[GP_subject_key] ,[GP_key_value])
       VALUES
             ('PSAClient', 'Constancias.ReportarCURP', '0');
GO

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PSAClient' AND GP_SUBJECT_KEY ='Salidas.ISR1')
  INSERT INTO [dbo].[GENERAL_PARAMS]
             ([GP_group_key] ,[GP_subject_key] ,[GP_key_value])
       VALUES
             ('PSAClient', 'Salidas.ISR1', '1');
GO

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PSAClient' AND GP_SUBJECT_KEY ='Salidas.ISR2')
  INSERT INTO [dbo].[GENERAL_PARAMS]
             ([GP_group_key] ,[GP_subject_key] ,[GP_key_value])
       VALUES
             ('PSAClient', 'Salidas.ISR2', '1');
GO