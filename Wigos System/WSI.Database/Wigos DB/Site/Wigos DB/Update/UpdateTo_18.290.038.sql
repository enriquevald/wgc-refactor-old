/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 289;

SET @New_ReleaseId = 290;
SET @New_ScriptName = N'UpdateTo_18.290.038.sql';
SET @New_Description = N'SP Modified: CageUpdateSystemMeters/CageGetGlobalReportData - New meter group Gaming Hall - Spanish regionalization - SP for Conrad';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
	 SET db_release_id = @New_ReleaseId
		 , db_updated_script = @New_ScriptName
		 , db_updated = GetDate()
		 , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
	 AND db_common_build_id = @Exp_CommonBuildId
	 AND db_client_build_id = @Exp_ClientBuildId
	 AND db_release_id = @Exp_ReleaseId;
GO

/******* TABLES  *******/

-- CREATE CUSTOMER VISITS
IF  NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[customer_visits]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[customer_visits](
	[cut_visit_id] [bigint] IDENTITY(1,1) NOT NULL,
	[cut_gaming_day] [int] NOT NULL,
	[cut_customer_id] [bigint] NOT NULL,
	[cut_level] [int] NOT NULL,
	[cut_is_vip] [bit] NOT NULL,
 CONSTRAINT [PK_customer_visits] PRIMARY KEY CLUSTERED 
(
	[cut_visit_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[customer_visits] ADD  CONSTRAINT [DF_customer_visits_cut_level]  DEFAULT ((0)) FOR [cut_level]

ALTER TABLE [dbo].[customer_visits] ADD  CONSTRAINT [DF_customer_visits_cut_is_vip]  DEFAULT ((0)) FOR [cut_is_vip]
END
GO

-- CREATE CUSTOMER ENTRANCES
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[customer_entrances]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[customer_entrances](
	[cue_entrance_datetime] [datetime] NOT NULL,
	[cue_visit_id] [bigint] NOT NULL,
	[cue_cashier_session_id] [bigint] NULL,
	[cue_cashier_user_id] [int] NULL,
	[cue_entrance_block_reason] [int] NULL,
	[cue_entrance_block_reason2] [int] NULL,
	[cue_entrance_block_description] [nvarchar](256) NULL,
	[cue_remarks] [nvarchar](256) NULL,
	[cue_ticket_entry_id] [bigint] NULL,
	[cue_ticket_entry_price] [money] NULL,
	[cue_document_type] [int] NULL,
	[cue_document_number] [nvarchar](50) NULL,
	[cue_voucher_id] [bigint] NULL,
 CONSTRAINT [PK_customer_entrances] PRIMARY KEY CLUSTERED 
(
	[cue_entrance_datetime] ASC,
	[cue_visit_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[customer_entrances] ADD  CONSTRAINT [DF_customer_entrances_ce_entrance_datetime]  DEFAULT (getdate()) FOR [cue_entrance_datetime]

ALTER TABLE [dbo].[customer_entrances]  WITH CHECK ADD  CONSTRAINT [FK_customer_entrances_customer_visits] FOREIGN KEY([cue_visit_id])
REFERENCES [dbo].[customer_visits] ([cut_visit_id])

ALTER TABLE [dbo].[customer_entrances] CHECK CONSTRAINT [FK_customer_entrances_customer_visits]
END
GO

/******* INDEXES *******/

IF  NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[accounts]') AND name = N'IX_ac_external_reference')
BEGIN
	CREATE NONCLUSTERED INDEX [IX_ac_external_reference] ON [dbo].ACCOUNTS 
	(
			ac_external_reference ASC
	)WITH (STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
END
GO

/******* RECORDS *******/

DECLARE @SystemMode INT

IF NOT EXISTS (SELECT * FROM CAGE_CONCEPTS WHERE CC_CONCEPT_ID = 12)
BEGIN
	SELECT @SystemMode = GP_KEY_VALUE from GENERAL_PARAMS where (GP_GROUP_KEY = 'TITO' and gp_subject_key = 'TITOMode') OR (GP_GROUP_KEY = 'Site' and GP_SUBJECT_KEY = 'SystemMode')
	IF (ISNULL(@SystemMode, 0) = 0)
		INSERT INTO CAGE_CONCEPTS VALUES(12, 'Impuesto IEJC', 1, 1, NULL, 'Impuesto IEJC', 0, 1,NULL,NULL,NULL)
	ELSE
		INSERT INTO CAGE_CONCEPTS VALUES(12, 'Impuesto IEJC', 1, 1, NULL, 'Impuesto IEJC', 0, 0,NULL,NULL,NULL)
END
GO

-- SPAIN STATE NAME
DECLARE @CountryISOCode2 AS VARCHAR(10)
SELECT @CountryISOCode2 = GP_KEY_VALUE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'RegionalOptions' AND GP_SUBJECT_KEY = 'CountryISOCode2'
IF (@CountryISOCode2 = 'ES')
BEGIN
	UPDATE GENERAL_PARAMS SET GP_KEY_VALUE = 'Provincia' WHERE GP_GROUP_KEY = 'Account.Fields'  AND GP_SUBJECT_KEY = 'State.Name'
	UPDATE GENERAL_PARAMS SET GP_KEY_VALUE = 'ES'     WHERE GP_GROUP_KEY = 'Account.DefaultValues' AND GP_SUBJECT_KEY = 'Country'
	UPDATE GENERAL_PARAMS SET GP_KEY_VALUE = '091'    WHERE GP_GROUP_KEY = 'Account.DefaultValues' AND GP_SUBJECT_KEY = 'DocumentType' -- DNI
END
GO

-- SPAIN OCCUPATIONS
IF (NOT EXISTS (SELECT * FROM occupations WHERE oc_country_iso_code2 = 'ES' ))
BEGIN
	INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('NO DISPONIBLE','0000001',1,1000,'ES')
END
GO

-- SPAIN IDENTIFICATION_TYPES
IF (NOT EXISTS (SELECT * FROM identification_types WHERE idt_country_iso_code2 = 'ES' ))
BEGIN
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (90,1,100,'Otro','ES')
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (91,1,1,'DNI','ES')
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (92,1,2,'Pasaporte','ES')
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (93,1,3,'Permiso Conducir','ES')
END
GO

-- SPAIN REGIONS
IF (NOT EXISTS (SELECT * FROM federal_states WHERE fs_country_iso_code2 = 'ES' ))
BEGIN
	INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('�lava','ES')
	INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Albacete','ES')
	INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Alicante','ES')
	INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Almer�a','ES')
	INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Asturias','ES')
	INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('�vila','ES')
	INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Badajoz','ES')
	INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Barcelona','ES')
	INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Burgos','ES')
	INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('C�ceres','ES')
	INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('C�diz','ES')
	INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Cantabria','ES')
	INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Castell�n','ES')
	INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Ciudad Real','ES')
	INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('C�rdoba','ES')
	INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('La Coru�a','ES')
	INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Cuenca','ES')
	INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Gerona','ES')
	INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Granada','ES')
	INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Guadalajara','ES')
	INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Guip�zcoa','ES')
	INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Huelva','ES')
	INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Huesca','ES')
	INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Islas Baleares','ES')
	INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Ja�n','ES')
	INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Le�n','ES')
	INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('L�rida','ES')
	INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Lugo','ES')
	INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Madrid','ES')
	INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('M�laga','ES')
	INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Murcia','ES')
	INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Navarra','ES')
	INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Orense','ES')
	INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Palencia','ES')
	INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Las Palmas','ES')
	INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Pontevedra','ES')
	INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('La Rioja','ES')
	INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Salamanca','ES')
	INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Segovia','ES')
	INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Sevilla','ES')
	INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Soria','ES')
	INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Tarragona','ES')
	INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Santa Cruz de Tenerife','ES')
	INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Teruel','ES')
	INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Toledo','ES')
	INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Valencia','ES')
	INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Valladolid','ES')
	INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Vizcaya','ES')
	INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Zamora','ES')
	INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Zaragoza','ES')
	INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Melilla','ES')
	INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Ceuta','ES')
END
GO

UPDATE   sas_meters_groups 
	 SET   smg_name        = 'PCD Meters'
			 , smg_description = 'PCD Meters'
			 , smg_required    = 0
 WHERE   smg_group_id    = 3
GO
 
DELETE FROM sas_meters_catalog_per_group WHERE smcg_group_id = 3
GO

-- Meters needed for Peru
--
IF NOT EXISTS(SELECT 1 FROM sas_meters_catalog_per_group WHERE smcg_group_id = 3 AND smcg_meter_code = 0)
	INSERT INTO sas_meters_catalog_per_group (smcg_group_id, smcg_meter_code) VALUES (3, 0)
IF NOT EXISTS(SELECT 1 FROM sas_meters_catalog_per_group WHERE smcg_group_id = 3 AND smcg_meter_code = 1)
	INSERT INTO sas_meters_catalog_per_group (smcg_group_id, smcg_meter_code) VALUES (3, 1)
IF NOT EXISTS(SELECT 1 FROM sas_meters_catalog_per_group WHERE smcg_group_id = 3 AND smcg_meter_code = 2)
	INSERT INTO sas_meters_catalog_per_group (smcg_group_id, smcg_meter_code) VALUES (3, 2)
IF NOT EXISTS(SELECT 1 FROM sas_meters_catalog_per_group WHERE smcg_group_id = 3 AND smcg_meter_code = 3)
	INSERT INTO sas_meters_catalog_per_group (smcg_group_id, smcg_meter_code) VALUES (3, 3)
IF NOT EXISTS(SELECT 1 FROM sas_meters_catalog_per_group WHERE smcg_group_id = 3 AND smcg_meter_code = 5)
	INSERT INTO sas_meters_catalog_per_group (smcg_group_id, smcg_meter_code) VALUES (3, 5)
IF NOT EXISTS(SELECT 1 FROM sas_meters_catalog_per_group WHERE smcg_group_id = 3 AND smcg_meter_code = 6)
	INSERT INTO sas_meters_catalog_per_group (smcg_group_id, smcg_meter_code) VALUES (3, 6)

-- Meters needed for Gaming Halls
--
--Meter Code = 0x09 (9) - Total credits paid from hopper (Total Out)
IF NOT EXISTS(SELECT 1 FROM sas_meters_catalog_per_group WHERE smcg_group_id = 10002 AND smcg_meter_code = 9)
	INSERT INTO sas_meters_catalog_per_group (smcg_group_id, smcg_meter_code) VALUES (10002, 9)
IF NOT EXISTS(SELECT 1 FROM sas_meters_catalog_per_group WHERE smcg_group_id = 3 AND smcg_meter_code = 9)
	INSERT INTO sas_meters_catalog_per_group (smcg_group_id, smcg_meter_code) VALUES (3, 9)

--Meter Code = 0x24 (36) - Total drop, including but not limited to coins to drop, bills to drop, tickets to drop, and electronic in (credits)
--                         (Total In)
IF NOT EXISTS(SELECT 	smcg_meter_code FROM sas_meters_catalog_per_group WHERE smcg_group_id = 10002 AND smcg_meter_code = 36)
	INSERT INTO sas_meters_catalog_per_group (smcg_group_id, smcg_meter_code) VALUES (10002, 36)
IF NOT EXISTS(SELECT 1 FROM sas_meters_catalog_per_group WHERE smcg_group_id = 3 AND smcg_meter_code = 36)
	INSERT INTO sas_meters_catalog_per_group (smcg_group_id, smcg_meter_code) VALUES (3, 36)

-- Additional meters (just in case)
--
--Meter Code = 0x08 (8) - Total credits from coin acceptor
IF NOT EXISTS(SELECT 1 FROM sas_meters_catalog_per_group WHERE smcg_group_id = 3 AND smcg_meter_code = 8)
	INSERT INTO sas_meters_catalog_per_group (smcg_group_id, smcg_meter_code) VALUES (3, 8)

--Meter Code = 0x0B (11) - Total credits from bills accepted
IF NOT EXISTS(SELECT 1 FROM sas_meters_catalog_per_group WHERE smcg_group_id = 3 AND smcg_meter_code = 11)
	INSERT INTO sas_meters_catalog_per_group (smcg_group_id, smcg_meter_code) VALUES (3, 11)

--Meter Code = 0x6E (110) - Total credits from bills dispensed from hopper
IF NOT EXISTS(SELECT 	smcg_meter_code FROM sas_meters_catalog_per_group WHERE smcg_group_id = 10002 AND smcg_meter_code = 110)
	INSERT INTO sas_meters_catalog_per_group (smcg_group_id, smcg_meter_code) VALUES (10002, 110)
IF NOT EXISTS(SELECT 1 FROM sas_meters_catalog_per_group WHERE smcg_group_id = 3 AND smcg_meter_code = 110)
	INSERT INTO sas_meters_catalog_per_group (smcg_group_id, smcg_meter_code) VALUES (3, 110)
GO

DECLARE @_system_mode AS VARCHAR(50)
IF NOT EXISTS(SELECT * FROM GENERAL_PARAMS WHERE gp_group_key = 'Site' AND GP_SUBJECT_KEY = 'SystemMode')
BEGIN
  SELECT @_system_mode = ISNULL(GP_KEY_VALUE,'0') FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'TITO' AND GP_SUBJECT_KEY ='TITOMode'
  INSERT INTO general_params (gp_group_key, gp_subject_key, gp_key_value) VALUES ('Site', 'SystemMode', @_system_mode);
END
GO

IF NOT EXISTS(SELECT * FROM SAS_METERS_GROUPS WHERE smg_group_id = 10005)
BEGIN
	INSERT INTO SAS_METERS_GROUPS (smg_group_id,smg_name,smg_description,smg_required) VALUES (10005,'Gaming Hall','Gaming Hall',0)
	INSERT INTO SAS_METERS_CATALOG_PER_GROUP (smcg_group_id, smcg_meter_code)          VALUES (10005,0)
	INSERT INTO SAS_METERS_CATALOG_PER_GROUP (smcg_group_id, smcg_meter_code)          VALUES (10005,1)
	INSERT INTO SAS_METERS_CATALOG_PER_GROUP (smcg_group_id, smcg_meter_code)          VALUES (10005,2)
	INSERT INTO SAS_METERS_CATALOG_PER_GROUP (smcg_group_id, smcg_meter_code)          VALUES (10005,3)
	INSERT INTO SAS_METERS_CATALOG_PER_GROUP (smcg_group_id, smcg_meter_code)          VALUES (10005,5)
	INSERT INTO SAS_METERS_CATALOG_PER_GROUP (smcg_group_id, smcg_meter_code)          VALUES (10005,6)
	INSERT INTO SAS_METERS_CATALOG_PER_GROUP (smcg_group_id, smcg_meter_code)          VALUES (10005,9)
	INSERT INTO SAS_METERS_CATALOG_PER_GROUP (smcg_group_id, smcg_meter_code)          VALUES (10005,36)
END 
GO

-- GENERAL PARAM
IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='Reception' AND GP_SUBJECT_KEY = 'Enabled')
	 INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Reception', 'Enabled', '0')
GO

/******* PROCEDURES *******/

ALTER PROCEDURE [dbo].[CageUpdateSystemMeters]
				@pCashierSessionId BIGINT
	AS
BEGIN             

	DECLARE @CageSessionId BIGINT
	DECLARE @NationalCurrency VARCHAR(3)
	DECLARE @Currencies AS VARCHAR(200)
	DECLARE @CurrentCurrency AS VARCHAR(3)
		
	DECLARE @Tax1 MONEY
	DECLARE @Tax2 MONEY
	DECLARE @TaxIEJC MONEY

	DECLARE @ClosingShort MONEY
	DECLARE @ClosingOver MONEY
	
	DECLARE @CardRefundable BIT
	DECLARE @CardDeposit MONEY
	
	SELECT @NationalCurrency = GP_KEY_VALUE FROM GENERAL_PARAMS 
	 WHERE GP_GROUP_KEY = 'RegionalOptions' 
		 AND GP_SUBJECT_KEY = 'CurrencyISOCode'
	
	
	---- GET CAGE SESSION ID ----
	
	SET @CageSessionId = (SELECT TOP 1 CGM_CAGE_SESSION_ID 
																		 FROM CAGE_MOVEMENTS
																	 WHERE CGM_CASHIER_SESSION_ID = @pCashierSessionId 
																			AND CGM_TYPE IN (0, 1, 4, 5)
															 ORDER BY CGM_MOVEMENT_DATETIME DESC)

	IF @CageSessionId IS NULL
			SET @CageSessionId = (SELECT TOP 1 CGM_CAGE_SESSION_ID 
																FROM CAGE_MOVEMENTS
															 WHERE CGM_CASHIER_SESSION_ID = @pCashierSessionId 
																 AND CGM_TYPE IN (100, 101, 103, 104)
													ORDER BY CGM_MOVEMENT_DATETIME DESC)
			
	IF @CageSessionId IS NULL
			SET @CageSessionId = (SELECT TOP 1 CGS_CAGE_SESSION_ID 
																FROM CAGE_SESSIONS
															 WHERE CGS_CLOSE_DATETIME IS NULL
													ORDER BY CGS_CAGE_SESSION_ID DESC)
	 
	 IF @CageSessionId IS NULL
			SET @CageSessionId = (SELECT TOP 1 CGS_CAGE_SESSION_ID 
																FROM CAGE_SESSIONS
														 ORDER BY CGS_CAGE_SESSION_ID DESC)
	 
			IF @CageSessionId IS NOT NULL
			BEGIN
	
						-- FEDERAL TAX --
	
						SELECT @Tax1 = SUM(CM_ADD_AMOUNT) 
							FROM CASHIER_MOVEMENTS_GROUPED_BY_SESSION_ID  
					 WHERE CM_TYPE IN (6) 
						 AND CM_SESSION_ID = @pCashierSessionId

						SET @Tax1 = ISNULL(@Tax1, 0)
						
						EXEC [dbo].[CageUpdateMeter]
									@pValueIn = @Tax1,
						@pValueOut = 0,
						@pSourceTagetId = 0,
						@pConceptId = 3,
						@pIsoCode = @NationalCurrency,
						@pOperation = 1,
						@pSessionId = @CageSessionId,
						@pSessiongetMode = 0 				 
		 
						-- STATE TAX --
	 
						SELECT @Tax2 = SUM(CM_ADD_AMOUNT) 
							FROM CASHIER_MOVEMENTS_GROUPED_BY_SESSION_ID  
						 WHERE CM_TYPE IN (14) 
							 AND CM_SESSION_ID = @pCashierSessionId 
					
					SET @Tax2 = ISNULL(@Tax2, 0)
				 
						EXEC [dbo].[CageUpdateMeter]
									@pValueIn = @Tax2,
						@pValueOut = 0,
						@pSourceTagetId = 0,
						@pConceptId = 2,
						@pIsoCode = @NationalCurrency,
						@pOperation = 1,
						@pSessionId = @CageSessionId,
						@pSessiongetMode = 0 
					
						-- IEJC TAX --
	
						SELECT @TaxIEJC = SUM(CM_ADD_AMOUNT) 
							FROM CASHIER_MOVEMENTS_GROUPED_BY_SESSION_ID  
						 WHERE CM_TYPE IN (142,146) 
							 AND CM_SESSION_ID = @pCashierSessionId
 
						SET @TaxIEJC = ISNULL(@TaxIEJC, 0)
						
						EXEC [dbo].[CageUpdateMeter]
						@pValueIn = @TaxIEJC,
						@pValueOut = 0,
						@pSourceTagetId = 0,
						@pConceptId = 12,
						@pIsoCode = @NationalCurrency,
						@pOperation = 1,
						@pSessionId = @CageSessionId,
						@pSessiongetMode = 0 
					
						---- CARD_PAYMENT ---- (IF @CardRefundable IS FALSE: WE DON'T NEED TO RETURN VALUE FOR LIABILITIES)                                                                    


						SELECT @CardRefundable = GP_KEY_VALUE 
							FROM GENERAL_PARAMS  
						 WHERE GP_GROUP_KEY = 'Cashier'                             
							 AND GP_SUBJECT_KEY = 'CardRefundable'                    

						IF ISNULL(@CardRefundable, 0) = 1                                                                        
						BEGIN                                                                                                   
									SELECT @CardDeposit = SUM(CASE WHEN CM_TYPE = 10 THEN -1 * CM_SUB_AMOUNT   
																															ELSE CM_ADD_AMOUNT END)                                                  
								FROM CASHIER_MOVEMENTS_GROUPED_BY_SESSION_ID                                                    
							 WHERE CM_TYPE IN (9, 10, 532, 533, 534, 535, 536, 544, 545, 546, 547, 548)                                             
								 AND CM_SESSION_ID IN(SELECT DISTINCT(CGM_CASHIER_SESSION_ID)                                   
																											 FROM CAGE_MOVEMENTS                                                                     
																										WHERE CGM_CASHIER_SESSION_ID = @pCashierSessionId)
																										
									SET @CardDeposit = ISNULL(@CardDeposit, 0)
						
									EXEC [dbo].[CageUpdateMeter]
												@pValueIn = @CardDeposit,
												@pValueOut = 0,
												@pSourceTagetId = 0,
												@pConceptId = 5,
												@pIsoCode = @NationalCurrency,
												@pOperation = 1,
												@pSessionId = @CageSessionId,
												@pSessiongetMode = 0
										 
						END                                                                                                     

			SELECT @Currencies = GP_KEY_VALUE 
				FROM GENERAL_PARAMS 
						 WHERE GP_GROUP_KEY = 'RegionalOptions' 
							 AND GP_SUBJECT_KEY = 'CurrenciesAccepted'
		
			-- Split currencies ISO codes
			SELECT SST_VALUE AS CURRENCY_ISO_CODE INTO #TMP_CURRENCIES_ISO_CODES FROM [SplitStringIntoTable] (@Currencies, ';', 1)

			
			DECLARE Curs_CageUpdateSystemMeters CURSOR FOR 
			SELECT CURRENCY_ISO_CODE
				FROM #TMP_CURRENCIES_ISO_CODES 
			
						SET NOCOUNT ON;

			OPEN Curs_CageUpdateSystemMeters

			FETCH NEXT FROM Curs_CageUpdateSystemMeters INTO @CurrentCurrency
		
			WHILE @@FETCH_STATUS = 0
			BEGIN
	
				-- CLOSING SHORT --
				
				SELECT @ClosingShort = SUM(CM_ADD_AMOUNT) 
					FROM CASHIER_MOVEMENTS_GROUPED_BY_SESSION_ID  
				WHERE CM_TYPE IN (160) 
					AND CM_SESSION_ID = @pCashierSessionId 
					AND CM_CURRENCY_ISO_CODE = @CurrentCurrency
				
				SET @ClosingShort = ISNULL(@ClosingShort, 0)
				
				EXEC [dbo].[CageUpdateMeter]
						@pValueIn = @ClosingShort,
						@pValueOut = 0,
						@pSourceTagetId = 0,
						@pConceptId = 7,
						@pIsoCode = @CurrentCurrency,
						@pOperation = 1,
						@pSessionId = @CageSessionId,
						@pSessiongetMode = 0 
				
				
				-- CLOSING OVER --
				
				SELECT @ClosingOver = SUM(CM_ADD_AMOUNT) 
					FROM CASHIER_MOVEMENTS_GROUPED_BY_SESSION_ID  
				WHERE CM_TYPE IN (161) 
					AND CM_SESSION_ID = @pCashierSessionId 
					AND CM_CURRENCY_ISO_CODE = @CurrentCurrency
				
				SET @ClosingOver = ISNULL(@ClosingOver, 0)
				
				EXEC [dbo].[CageUpdateMeter]
						@pValueIn = @ClosingOver,
						@pValueOut = 0,
						@pSourceTagetId = 0,
						@pConceptId = 8,
						@pIsoCode = @CurrentCurrency,
						@pOperation = 1,
						@pSessionId = @CageSessionId,
						@pSessiongetMode = 0 
					
					
				FETCH NEXT FROM Curs_CageUpdateSystemMeters INTO @CurrentCurrency
								
						END

			CLOSE Curs_CageUpdateSystemMeters
			DEALLOCATE Curs_CageUpdateSystemMeters

			END                                      

END -- CageUpdateSystemMeters
GO

ALTER PROCEDURE [dbo].[CageGetGlobalReportData]
				@pCageSessionIds NVARCHAR(MAX) = NULL
AS
BEGIN
	
	DECLARE @NationalIsoCode NVARCHAR(3)
	DECLARE @CageOperationType_FromTerminal INT
	DECLARE @TicketsId INT
	DECLARE @BillsId INT
	DECLARE @Currencies AS NVARCHAR(200)
	DECLARE @CountSessions INT
	DECLARE @List VARCHAR(MAX)
	DECLARE @StockIsStored BIT

	SELECT @NationalIsoCode = GP_KEY_VALUE 
		FROM GENERAL_PARAMS 
	 WHERE GP_GROUP_KEY = 'RegionalOptions' 
		 AND GP_SUBJECT_KEY = 'CurrencyISOCode'

	SELECT @Currencies = GP_KEY_VALUE 
		FROM GENERAL_PARAMS 
	 WHERE GP_GROUP_KEY = 'RegionalOptions' 
		 AND GP_SUBJECT_KEY = 'CurrenciesAccepted'

	-- Split currencies ISO codes
	SELECT SST_VALUE AS CURRENCY_ISO_CODE INTO #TMP_CURRENCIES_ISO_CODES FROM [SplitStringIntoTable] (@Currencies, ';', 1)
							 
	SET @CageOperationType_FromTerminal = 104
	SET @TicketsId = -200
	SET @BillsId = 0
	 
	-- Session IDs Split
	SELECT SST_VALUE AS CAGE_SESSION_ID INTO #TMP_CAGE_SESSIONS FROM [SplitStringIntoTable] (@pCageSessionIds,','  , 1)
	SET @CountSessions = (SELECT COUNT(*) FROM #TMP_CAGE_SESSIONS )
	
	IF @CountSessions = 1 
		SELECT @List = CGS_CAGE_STOCK FROM CAGE_SESSIONS 
		 WHERE CGS_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)

	-- TABLE[0]: Get cage stock 
	IF (@CountSessions = 1 AND @List IS NOT NULL) 
	BEGIN
		--Get stock on close session
		SET @StockIsStored =  1
		SELECT SST_VALUE AS STOCK_ROW INTO #TMP_STOCK_ROWS FROM [SplitStringIntoTable] (@List, '|', 1)
		
		SELECT REPLACE(PARSENAME(REPLACE(REPLACE(STOCK_ROW, '.', '|'), ';', '.'), 4), '|', '.') AS CGS_ISO_CODE,                        
			 CAST(REPLACE(PARSENAME(REPLACE(REPLACE(STOCK_ROW, '.', '|'), ';', '.'), 3), '|', '.') AS MONEY) AS CGS_DENOMINATION,   
			 CAST(REPLACE(PARSENAME(REPLACE(REPLACE(STOCK_ROW, '.', '|'), ';', '.'), 2), '|', '.') AS DECIMAL(15,2)) AS CGS_QUANTITY,       
			 CAST(REPLACE(PARSENAME(REPLACE(REPLACE(STOCK_ROW, '.', '|'), ';', '.'), 1), '|', '.') AS INT) AS CGS_CAGE_CURRENCY_TYPE  
			 INTO #TMP_STOCK                                                                                                               
		FROM #TMP_STOCK_ROWS                                                                                                          

		SELECT * FROM #TMP_STOCK
		ORDER BY CGS_ISO_CODE, CGS_CAGE_CURRENCY_TYPE ASC, CGS_DENOMINATION DESC
		, CASE WHEN CGS_DENOMINATION >= 0 THEN CGS_DENOMINATION 
				 ELSE CGS_DENOMINATION * (-100000) END                                                                    

		DROP TABLE #TMP_STOCK_ROWS                                                                                                      
		DROP TABLE #TMP_STOCK     
		
	END --IF (@CountSessions = 1 AND @List IS NOT NULL)    
	ELSE --IF (@CountSessions = 1 AND @List IS NOT NULL) 
	BEGIN
		-- Get current stock
		SET @StockIsStored =  0
		EXEC CageCurrentStock  
	END --ELSE
	
	IF @pCageSessionIds IS NOT NULL
	BEGIN
			
		-- TABLE[1]: Get cage sessions information
		SELECT CSM.CSM_SOURCE_TARGET_ID AS CM_SOURCE_TARGET_ID
				 , CSM.CSM_CONCEPT_ID AS CM_CONCEPT_ID
				 , CSM.CSM_ISO_CODE AS CM_ISO_CODE
				 , SUM(CSM.CSM_VALUE_IN) AS CM_VALUE_IN
				 , SUM(CSM.CSM_VALUE_OUT) AS CM_VALUE_OUT
				 , CST.CST_SOURCE_TARGET_NAME
				 , CC.CC_DESCRIPTION
			FROM CAGE_SESSION_METERS AS CSM
					 INNER JOIN CAGE_SOURCE_TARGET_CONCEPTS AS CSTC ON CSTC.CSTC_CONCEPT_ID = CSM.CSM_CONCEPT_ID AND CSTC.CSTC_SOURCE_TARGET_ID = CSM.CSM_SOURCE_TARGET_ID
					 INNER JOIN CAGE_CONCEPTS AS CC ON CC.CC_CONCEPT_ID = CSTC.CSTC_CONCEPT_ID
					 INNER JOIN CAGE_SOURCE_TARGET AS CST ON CST.CST_SOURCE_TARGET_ID = CSTC.CSTC_SOURCE_TARGET_ID
		 WHERE CSM.CSM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
			 AND CC.CC_SHOW_IN_REPORT = 1
			 AND CC.CC_ENABLED = 1
			 AND (CC.CC_TYPE = 1 OR CC_CONCEPT_ID IN (9, 10,11))
			 AND CSTC.CSTC_ENABLED = 1
			 AND CSM_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)
	GROUP BY CSM.CSM_SOURCE_TARGET_ID, CSM.CSM_CONCEPT_ID, CSM.CSM_ISO_CODE, CST.CST_SOURCE_TARGET_NAME, CC.CC_DESCRIPTION
	ORDER BY CSM.CSM_SOURCE_TARGET_ID
				 , CASE WHEN CSM.CSM_CONCEPT_ID = 0 THEN '000' ELSE CC.CC_DESCRIPTION END
				 , CASE WHEN CSM.CSM_ISO_CODE = @NationalIsoCode THEN '000' ELSE CSM.CSM_ISO_CODE END                    

		-- TABLE[2]: Get dynamic liabilities
		SELECT CC.CC_CONCEPT_ID AS LIABILITY_CONCEPT_ID
				 , CC.CC_DESCRIPTION AS LIABILITY_NAME 
				 , CSM.CSM_ISO_CODE AS LIABILITY_ISO_CODE
				 , SUM(CSM.CSM_VALUE) AS LIABILITY_VALUE
			FROM CAGE_SESSION_METERS AS CSM
					 INNER JOIN CAGE_CONCEPTS AS CC ON CC.CC_CONCEPT_ID = CSM.CSM_CONCEPT_ID
		 WHERE CC.CC_ENABLED = 1
			 AND CC.CC_SHOW_IN_REPORT = 1
			 AND CC.CC_IS_PROVISION = 1
			 AND CSM.CSM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
			 AND CSM_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)
 GROUP BY CC.CC_CONCEPT_ID, CC.CC_DESCRIPTION, CSM.CSM_ISO_CODE
	ORDER BY CC.CC_CONCEPT_ID ASC
				 , CASE WHEN CSM.CSM_ISO_CODE = @NationalIsoCode THEN '000' ELSE CSM.CSM_ISO_CODE END 
			
		-- TABLE[3]: Get session sumary
		SELECT CMD_ISO_CODE AS ISO_CODE
				 , ISNULL(ORIGIN, 0) AS ORIGIN
				 , SUM(CMD_DEPOSITS) AS DEPOSITS
				 , SUM(CMD_WITHDRAWALS) AS WITHDRAWALS
			FROM (SELECT CMD_ISO_CODE
								 , ORIGIN
								 , SUM(CMD_DEPOSITS) CMD_DEPOSITS
								 , SUM(CMD_WITHDRAWALS) CMD_WITHDRAWALS      
							FROM (SELECT CASE WHEN CMD_QUANTITY >= 0 THEN @BillsId
																ELSE CMD_QUANTITY END  AS ORIGIN
												 , CMD_ISO_CODE
												 , CMD_DENOMINATION
												 , CASE WHEN CGM_TYPE IN (100, 101, 103, 104) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY < 0 THEN CMD_DENOMINATION
																																														ELSE CMD_QUANTITY * CMD_DENOMINATION END), 0)
																ELSE 0 END AS CMD_DEPOSITS
												 , CASE WHEN CGM_TYPE IN (0, 1, 4, 5) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY < 0 THEN CMD_DENOMINATION             
																																										ELSE CMD_QUANTITY * CMD_DENOMINATION END), 0)
																ELSE 0 END AS CMD_WITHDRAWALS
											FROM CAGE_MOVEMENTS 
													 INNER JOIN   CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          
										 WHERE CGM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
											 AND (CGM_STATUS IN (2, 3, 4, 8, 9, 10) 
														OR (CGM_STATUS = 0 AND CGM_TYPE = 0))
									GROUP BY CMD_ISO_CODE, CMD_DENOMINATION, CGM_TYPE, CMD_QUANTITY
									 ) AS T1        
					GROUP BY ORIGIN, CMD_ISO_CODE

						-- GET Terminal value-in amounts
							
							UNION ALL 
							
							SELECT @NationalIsoCode AS CMD_ISO_CODE
										, @TicketsId AS ORIGIN
										, MC_COLLECTED_TICKET_AMOUNT AS DEPOSITS
										, 0 AS WITHDRAWS
							FROM MONEY_COLLECTIONS
									INNER JOIN CAGE_MOVEMENTS ON CGM_MC_COLLECTION_ID = MC_COLLECTION_ID 
						 WHERE CAGE_MOVEMENTS.CGM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
							 AND CAGE_MOVEMENTS.CGM_TYPE = @CAGEOPERATIONTYPE_FROMTERMINAL
							 AND (CGM_STATUS IN (2, 3, 4, 8, 9, 10) 
										OR (CGM_STATUS = 0 AND CGM_TYPE = 0))           
						 
							UNION ALL
							
								SELECT @NationalIsoCode AS CMD_ISO_CODE
										, @BillsId AS ORIGIN
										, MC_COLLECTED_BILL_AMOUNT + MC_COLLECTED_COIN_AMOUNT AS DEPOSITS
										, 0 AS WITHDRAWS
							FROM MONEY_COLLECTIONS
									INNER JOIN CAGE_MOVEMENTS ON CGM_MC_COLLECTION_ID = MC_COLLECTION_ID 
						 WHERE CAGE_MOVEMENTS.CGM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
							 AND CAGE_MOVEMENTS.CGM_TYPE = @CageOperationType_FromTerminal
							 AND (CGM_STATUS IN (2, 3, 4, 8, 9, 10) 
										OR (CGM_STATUS = 0 AND CGM_TYPE = 0))
																			
					) AS _TBL_GLB
			WHERE CMD_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)
	 GROUP BY _TBL_GLB.CMD_ISO_CODE, _TBL_GLB.ORIGIN
	 ORDER BY CASE WHEN _TBL_GLB.CMD_ISO_CODE = @NationalIsoCode THEN '000' ELSE _TBL_GLB.CMD_ISO_CODE END 
					, _TBL_GLB.ORIGIN DESC
				 
		-- TABLE[4]: Other System Concepts           
		SELECT CSM_CONCEPT_ID AS CM_CONCEPT_ID
				 , CC.CC_DESCRIPTION AS CM_DESCRIPTION
				 , CSM_ISO_CODE AS CM_ISO_CODE
				 , SUM(CSM_VALUE) AS CM_VALUE
			FROM CAGE_SESSION_METERS AS CSM
					 INNER JOIN CAGE_CONCEPTS AS CC ON CSM.CSM_CONCEPT_ID = CC.CC_CONCEPT_ID
		 WHERE CSM_SOURCE_TARGET_ID = 0
			 AND CSM_CONCEPT_ID IN (7, 8)
			 AND CSM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
			 AND CSM_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)
	GROUP BY CSM_CONCEPT_ID, CSM_ISO_CODE, CSM_VALUE, CC.CC_DESCRIPTION 
	ORDER BY CM_CONCEPT_ID
				 , CASE WHEN CSM_ISO_CODE = @NationalIsoCode THEN '000' ELSE CSM_ISO_CODE END 
				 
		 -- TABLE[5]: Cage Counts
		SELECT ORIGIN AS ORIGIN
				 , CMD_ISO_CODE AS ISO_CODE
				 , ISNULL(SUM(CMD_DEPOSITS), 0) AS DEPOSITS
			FROM (SELECT CASE WHEN CMD_QUANTITY >= 0 THEN 0
												ELSE CMD_QUANTITY END  AS ORIGIN
								 , CMD_ISO_CODE
								 , ISNULL(SUM (CASE WHEN CMD_QUANTITY >= 0 THEN CMD_QUANTITY 
																		ELSE 1 END * CASE WHEN CGM_TYPE = 99 THEN -1 * CMD_DENOMINATION 
																											ELSE CMD_DENOMINATION END), 0) AS CMD_DEPOSITS
							FROM CAGE_MOVEMENTS 
									 INNER JOIN CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          
--             WHERE CGM_TYPE = 3
				WHERE CGM_TYPE IN (99, 199)
							 AND CGM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
					GROUP BY CMD_ISO_CODE, CMD_DENOMINATION, CGM_TYPE, CMD_QUANTITY
						) AS T1           
	 GROUP BY ORIGIN, CMD_ISO_CODE
					 
		UNION ALL
							
		SELECT NULL AS ORIGIN
				 , CMD_ISO_CODE AS ISO_CODE
				 , ISNULL(SUM(CMD_DEPOSITS), 0) AS DEPOSITS
			FROM (SELECT CMD_ISO_CODE
								 , ISNULL(SUM (CASE WHEN CMD_QUANTITY >= 0 THEN CMD_QUANTITY 
																		ELSE 1 END * CASE WHEN CGM_TYPE = 99 THEN -1 * CMD_DENOMINATION 
																											ELSE CMD_DENOMINATION END), 0) AS CMD_DEPOSITS
							FROM CAGE_MOVEMENTS 
									 INNER JOIN CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          
						 WHERE CGM_TYPE IN (99, 199)
							 AND CGM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
					GROUP BY CMD_ISO_CODE, CMD_DENOMINATION, CGM_TYPE, CMD_QUANTITY
					 ) AS T1           
	GROUP BY CMD_ISO_CODE

		DROP TABLE #TMP_CAGE_SESSIONS 

	END -- IF @pCageSessionIds IS NOT NULL THEN
			
	ELSE -- IF @pCageSessionIds IS NULL
	BEGIN
			
		-- TABLE[1]: Get cage sessions information
		SELECT CM.CM_SOURCE_TARGET_ID
				 , CM.CM_CONCEPT_ID
				 , CM.CM_ISO_CODE
				 , CM.CM_VALUE_IN
				 , CM.CM_VALUE_OUT
				 , CST.CST_SOURCE_TARGET_NAME
				 , CC.CC_DESCRIPTION
			FROM CAGE_METERS AS CM
					 INNER JOIN CAGE_SOURCE_TARGET_CONCEPTS AS CSTC ON CSTC.CSTC_CONCEPT_ID = CM.CM_CONCEPT_ID AND CSTC.CSTC_SOURCE_TARGET_ID = CM.CM_SOURCE_TARGET_ID
					 INNER JOIN CAGE_CONCEPTS AS CC ON CC.CC_CONCEPT_ID = CSTC.CSTC_CONCEPT_ID
					 INNER JOIN CAGE_SOURCE_TARGET AS CST ON CST.CST_SOURCE_TARGET_ID = CSTC.CSTC_SOURCE_TARGET_ID
		 WHERE (CC.CC_TYPE = 1 OR CC_CONCEPT_ID IN (9, 10, 11))
			 AND CC.CC_ENABLED = 1
			 AND CSTC.CSTC_ENABLED = 1
			 AND CC.CC_SHOW_IN_REPORT = 1
			 AND CM_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)
	ORDER BY CM.CM_SOURCE_TARGET_ID
				 , CASE WHEN CM.CM_CONCEPT_ID = 0 THEN '000' ELSE CC.CC_DESCRIPTION END
				 , CASE WHEN CM.CM_ISO_CODE = @NationalIsoCode THEN '000' ELSE CM.CM_ISO_CODE END   

		-- TABLE[2]: Get dynamic liabilities
		SELECT CC.CC_CONCEPT_ID AS LIABILITY_CONCEPT_ID
				 , CC.CC_DESCRIPTION AS LIABILITY_NAME 
				 , CM.CM_ISO_CODE AS LIABILITY_ISO_CODE
				 , SUM(CM.CM_VALUE) AS LIABILITY_VALUE
			FROM CAGE_METERS AS CM
					 INNER JOIN CAGE_CONCEPTS AS CC ON CC.CC_CONCEPT_ID = CM.CM_CONCEPT_ID
		 WHERE CC.CC_ENABLED = 1
			 AND CC.CC_SHOW_IN_REPORT = 1
			 AND CC.CC_IS_PROVISION = 1
			 AND CM_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)
	GROUP BY CC.CC_CONCEPT_ID, CC.CC_DESCRIPTION, CM.CM_ISO_CODE
	ORDER BY CC.CC_CONCEPT_ID ASC 
				 , CASE WHEN CM.CM_ISO_CODE = @NationalIsoCode THEN '000' ELSE CM.CM_ISO_CODE END 
			
		-- TABLE[3]: Get session sumary
		SELECT CMD_ISO_CODE AS ISO_CODE
				 , ISNULL(ORIGIN, 0) AS ORIGIN
				 , SUM(CMD_DEPOSITS) AS DEPOSITS
				 , SUM(CMD_WITHDRAWALS) AS WITHDRAWALS     
			FROM (SELECT CMD_ISO_CODE
								 , ORIGIN
								 , SUM(CMD_DEPOSITS) CMD_DEPOSITS
								 , SUM(CMD_WITHDRAWALS) CMD_WITHDRAWALS      
							FROM (SELECT CASE WHEN CMD_QUANTITY >= 0 THEN @BillsId
																ELSE CMD_QUANTITY END AS ORIGIN
												 , CMD_ISO_CODE
												 , CMD_DENOMINATION
												 , CASE WHEN CGM_TYPE IN (100, 101, 103, 104) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY < 0 THEN CMD_DENOMINATION
																																														ELSE CMD_QUANTITY * CMD_DENOMINATION END), 0)
																ELSE 0 END AS CMD_DEPOSITS
												 , CASE WHEN CGM_TYPE IN (0, 1, 4, 5) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY < 0 THEN CMD_DENOMINATION             
																																										ELSE CMD_QUANTITY * CMD_DENOMINATION END), 0)
																ELSE 0 END AS CMD_WITHDRAWALS
											FROM CAGE_MOVEMENTS 
													 INNER JOIN   CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          
										 WHERE CGM_STATUS IN (2, 3, 4, 8, 9, 10) 
												OR (CGM_STATUS = 0 AND CGM_TYPE = 0)
									GROUP BY CMD_ISO_CODE, CMD_DENOMINATION, CGM_TYPE, CMD_QUANTITY
										) AS T1
					 GROUP BY ORIGIN,CMD_ISO_CODE

							-- GET Terminal value-in amounts
							
							UNION ALL 
							
							SELECT @NationalIsoCode AS CMD_ISO_CODE
										, @TicketsId AS ORIGIN
										, MC_COLLECTED_TICKET_AMOUNT AS DEPOSITS
										, 0 AS WITHDRAWS
							FROM MONEY_COLLECTIONS
									INNER JOIN CAGE_MOVEMENTS ON CGM_MC_COLLECTION_ID = MC_COLLECTION_ID 
						 WHERE CAGE_MOVEMENTS.CGM_TYPE = @CAGEOPERATIONTYPE_FROMTERMINAL
							 AND (CGM_STATUS IN (2, 3, 4, 8, 9, 10) 
										OR (CGM_STATUS = 0 AND CGM_TYPE = 0))           
						 
							UNION ALL
							
								SELECT @NationalIsoCode AS CMD_ISO_CODE
										, @BillsId AS ORIGIN
										, MC_COLLECTED_BILL_AMOUNT + MC_COLLECTED_COIN_AMOUNT  AS DEPOSITS
										, 0 AS WITHDRAWS
							FROM MONEY_COLLECTIONS
									INNER JOIN CAGE_MOVEMENTS ON CGM_MC_COLLECTION_ID = MC_COLLECTION_ID 
						 WHERE CAGE_MOVEMENTS.CGM_TYPE = @CageOperationType_FromTerminal
							 AND (CGM_STATUS IN (2, 3, 4, 8, 9, 10) 
										OR (CGM_STATUS = 0 AND CGM_TYPE = 0))
													
					) AS _TBL_GLB 
			WHERE CMD_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)             
	 GROUP BY _TBL_GLB.CMD_ISO_CODE, _TBL_GLB.ORIGIN
	 ORDER BY CASE WHEN _TBL_GLB.CMD_ISO_CODE = @NationalIsoCode THEN '000' ELSE _TBL_GLB.CMD_ISO_CODE END 
					, _TBL_GLB.ORIGIN DESC 
				 
		-- TABLE[4]: Other System Concepts           
		SELECT CM_CONCEPT_ID
				 , CC.CC_DESCRIPTION AS CM_DESCRIPTION
				 , CM_ISO_CODE
				 , SUM(CM_VALUE) AS CM_VALUE
			FROM CAGE_METERS AS CM
					 INNER JOIN CAGE_CONCEPTS AS CC ON CM.CM_CONCEPT_ID = CC.CC_CONCEPT_ID
		 WHERE CM_SOURCE_TARGET_ID = 0
			 AND CM_CONCEPT_ID IN (7, 8)
			 AND CM_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)
	GROUP BY CM_CONCEPT_ID, CM_ISO_CODE, CM_VALUE, CC.CC_DESCRIPTION
	ORDER BY CM_CONCEPT_ID
				 , CASE WHEN CM_ISO_CODE = @NationalIsoCode THEN '000' ELSE CM_ISO_CODE END
		
		-- TABLE[5]: Cage Counts
		SELECT ORIGIN AS ORIGIN
				 , CMD_ISO_CODE AS ISO_CODE
				 , ISNULL(SUM(CMD_DEPOSITS), 0) AS DEPOSITS
			FROM (SELECT CASE WHEN CMD_QUANTITY >= 0 THEN 0
												ELSE CMD_QUANTITY END  AS ORIGIN
								 , CMD_ISO_CODE
								 , ISNULL(SUM (CASE WHEN CMD_QUANTITY >= 0 THEN CMD_QUANTITY 
																		ELSE 1 END * CASE WHEN CGM_TYPE = 99 THEN -1 * CMD_DENOMINATION 
																											ELSE CMD_DENOMINATION END), 0) AS CMD_DEPOSITS
							FROM CAGE_MOVEMENTS 
									 INNER JOIN CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          
						 WHERE CGM_TYPE IN (99, 199)
					GROUP BY CMD_ISO_CODE, CMD_DENOMINATION, CGM_TYPE, CMD_QUANTITY
						) AS T1           
	 GROUP BY ORIGIN, CMD_ISO_CODE
					 
		UNION ALL
							
		SELECT NULL AS ORIGIN
				 , CMD_ISO_CODE AS ISO_CODE
				 , ISNULL(SUM(CMD_DEPOSITS), 0) AS DEPOSITS
			FROM (SELECT CMD_ISO_CODE
								 , ISNULL(SUM (CASE WHEN CMD_QUANTITY >= 0 THEN CMD_QUANTITY 
																		ELSE 1 END * CASE WHEN CGM_TYPE = 99 THEN -1 * CMD_DENOMINATION 
																											ELSE CMD_DENOMINATION END), 0) AS CMD_DEPOSITS
							FROM CAGE_MOVEMENTS 
									 INNER JOIN CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          
						 WHERE CGM_TYPE IN (99, 199)
					GROUP BY CMD_ISO_CODE, CMD_DENOMINATION, CGM_TYPE, CMD_QUANTITY
					 ) AS T1           
	GROUP BY CMD_ISO_CODE


	END -- ELSE
	
	-- TABLE[6]: Stock Stored 
	SELECT @StockIsStored
	
	DROP TABLE #TMP_CURRENCIES_ISO_CODES
									 
END -- CageGetGlobalReportData
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GamingDayFromDateTime]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION [dbo].[GamingDayFromDateTime]
GO

CREATE FUNCTION [dbo].[GamingDayFromDateTime] 
(
			-- Add the parameters for the function here
			@DateTime DATETIME
)
RETURNS int
AS
BEGIN
			-- Declare the return variable here
			DECLARE @_open               AS DATETIME
			DECLARE @_gaming_day    AS INT
			
			SET @_open       = dbo.Opening (0, @DateTime)
			SET @_gaming_day = YEAR (@_open) * 10000 + MONTH(@_open) * 100 + DAY(@_open)

			RETURN @_gaming_day

END

GO

GRANT EXECUTE ON [dbo].[GamingDayFromDateTime] TO [wggui] WITH GRANT OPTION
GO

--------------------------------------------------------------------------------
-- PURPOSE: Get play sessions summary
-- 
--  PARAMS:
--      - INPUT:
--        @CustomerId      BIGINT,
--        @StartDateTime  DATETIME,
--        @EndDateTime    DATETIME
--
--      - OUTPUT:
--
-- RETURNS:
--        CustomerId
--        DurationSeconds
--        TotalPlayed
--        TheoreticalTotalWon
--        TotalWon
--        EarnedPoints
--        RePlayed
--        ReWon
--        NrPlayed
--        NrWon
--
--   NOTES: If @CustomerId is NULL is going to return all records
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WSP_002_GetSumPlaySessions]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].WSP_002_GetSumPlaySessions 
GO
CREATE PROCEDURE [dbo].[WSP_002_GetSumPlaySessions]
		@CustomerId     BIGINT,
		@StartDateTime  DATETIME,
		@EndDateTime    DATETIME
	AS
BEGIN

	DECLARE @_theoretical_payout MONEY

	SET @_theoretical_payout = ISNULL ( ( SELECT  CAST (GP_KEY_VALUE AS MONEY)
																					FROM  GENERAL_PARAMS
																				 WHERE  GP_GROUP_KEY   = 'PlayerTracking'
																					 AND  GP_SUBJECT_KEY = 'TerminalDefaultPayout' ), 0) / 100

	IF  @CustomerId IS NULL
	BEGIN
			SELECT    PS_ACCOUNT_ID                              AS CustomerId
							, SUM(DATEDIFF(s,PS_STARTED,PS_FINISHED))    AS DurationSeconds
							, SUM(PS_TOTAL_PLAYED)                       AS TotalPlayed
							, SUM(PS_TOTAL_PLAYED * ISNULL(TE_THEORETICAL_PAYOUT, @_theoretical_payout)) AS TheoreticalTotalWon
							, SUM(PS_TOTAL_WON)                          AS TotalWon
							, SUM(ISNULL(PS_AWARDED_POINTS,0))           AS EarnedPoints
							, SUM(PS_REDEEMABLE_PLAYED)                  AS RePlayed
							, SUM(PS_REDEEMABLE_WON)                     AS ReWon
							, SUM(ISNULL(PS_NON_REDEEMABLE_PLAYED,0))    AS NrPlayed
							, SUM(ISNULL(PS_NON_REDEEMABLE_WON,0))       AS NrWon
				FROM    PLAY_SESSIONS WITH (INDEX(IX_ps_finished_status))
	INNER JOIN    TERMINALS ON TE_TERMINAL_ID = PS_TERMINAL_ID
			 WHERE    PS_STATUS <> 0
				 AND    PS_FINISHED >= @StartDateTime
				 AND    PS_FINISHED  < @EndDateTime
		GROUP BY    PS_ACCOUNT_ID 
		ORDER BY    PS_ACCOUNT_ID   
	END
	ELSE
	BEGIN
			SELECT    PS_ACCOUNT_ID                              AS CustomerId
							, SUM(DATEDIFF(s,PS_STARTED,PS_FINISHED))    AS DurationSeconds
							, SUM(PS_TOTAL_PLAYED)                       AS TotalPlayed
							, SUM(PS_TOTAL_PLAYED * ISNULL(TE_THEORETICAL_PAYOUT, @_theoretical_payout)) AS TheoreticalTotalWon
							, SUM(PS_TOTAL_WON)                          AS TotalWon
							, SUM(ISNULL(PS_AWARDED_POINTS,0))           AS EarnedPoints
							, SUM(PS_REDEEMABLE_PLAYED)                  AS RePlayed
							, SUM(PS_REDEEMABLE_WON)                     AS ReWon
							, SUM(ISNULL(PS_NON_REDEEMABLE_PLAYED,0))    AS NrPlayed
							, SUM(ISNULL(PS_NON_REDEEMABLE_WON,0))       AS NrWon
				FROM    PLAY_SESSIONS WITH (INDEX(IX_ps_account_id_finished))
	INNER JOIN    TERMINALS ON TE_TERMINAL_ID = PS_TERMINAL_ID
			 WHERE    PS_ACCOUNT_ID = @CustomerId
				 AND    PS_STATUS <> 0
				 AND    PS_FINISHED >= @StartDateTime
				 AND    PS_FINISHED  < @EndDateTime
		GROUP BY    PS_ACCOUNT_ID 
		ORDER BY    PS_ACCOUNT_ID   
	END

END  -- WSP_002_GetSumPlaySessions
																																											 
GO

IF  EXISTS (SELECT * FROM sys.server_principals WHERE name = N'wg_datareader' AND type in (N'S'))
	IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'wg_datareader' AND type in (N'S'))
	BEGIN
		GRANT EXECUTE ON [dbo].[WSP_002_GetSumPlaySessions] TO [wg_datareader] WITH GRANT OPTION
	END
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WSP_002_GetPlaySessions]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].WSP_002_GetPlaySessions 
GO
CREATE PROCEDURE [dbo].[WSP_002_GetPlaySessions]
		@CustomerId      BIGINT,
		@StartDateTime  DATETIME,
		@EndDateTime    DATETIME
	AS
BEGIN
	DECLARE @_theoretical_payout MONEY

	SET @_theoretical_payout = ISNULL ( ( SELECT  CAST (GP_KEY_VALUE AS MONEY)
																					FROM  GENERAL_PARAMS
																				 WHERE  GP_GROUP_KEY   = 'PlayerTracking'
																					 AND  GP_SUBJECT_KEY = 'TerminalDefaultPayout' ), 0) / 100

		SELECT    PS_PLAY_SESSION_ID                  AS PlaySessionId
						, PS_STARTED                          AS Started
						, PS_FINISHED                         AS Finished
						, DATEDIFF(s,PS_STARTED,PS_FINISHED)  AS DurationSeconds
						, PS_TOTAL_PLAYED                     AS TotalPlayed
						, (PS_TOTAL_PLAYED * ISNULL(TE_THEORETICAL_PAYOUT, @_theoretical_payout)) AS TheoreticalTotalWon
						, PS_TOTAL_WON                        AS TotalWon
						, PS_AWARDED_POINTS                   AS EarnedPoints
						, PS_REDEEMABLE_PLAYED                AS RePlayed
						, PS_REDEEMABLE_WON                   AS ReWon
						, PS_NON_REDEEMABLE_PLAYED            AS NrPlayed
						, PS_NON_REDEEMABLE_WON               AS NrWon
			FROM    PLAY_SESSIONS WITH (INDEX(IX_ps_account_id_finished))
INNER JOIN    TERMINALS ON TE_TERMINAL_ID = PS_TERMINAL_ID      
		 WHERE    PS_ACCOUNT_ID = @CustomerId
			 AND    PS_STATUS <> 0
			 AND    PS_FINISHED >= @StartDateTime 
			 AND    PS_FINISHED  < @EndDateTime

END  -- WSP_002_GetPlaySessions
																																											 
GO

IF  EXISTS (SELECT * FROM sys.server_principals WHERE name = N'wg_datareader' AND type in (N'S'))
	IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'wg_datareader' AND type in (N'S'))
	BEGIN
		GRANT EXECUTE ON [dbo].[WSP_002_GetPlaySessions] TO [wg_datareader] WITH GRANT OPTION
	END
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WSP_002_CreateAccount]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].WSP_002_CreateAccount 
GO
CREATE PROCEDURE [dbo].[WSP_002_CreateAccount]
		@ExternalPlayerId   BIGINT,
		@Name1              NVARCHAR (50),
		@Name2              NVARCHAR (50),
		@LastName1          NVARCHAR (50),
		@LastName2          NVARCHAR (50),
		@DocType            INT,
		@DocId              NVARCHAR (20),
		@DateOfBirth        DATETIME,
		@Gender             INT,
		@TrackData          NVARCHAR (50),
		@AccountId          BIGINT OUTPUT,
		@StatusCode         INT OUTPUT,
		@StatusText         NVARCHAR (254) OUTPUT
	AS
BEGIN
	DECLARE @_holder_name         NVARCHAR (254)
	DECLARE @_internal_track_data NVARCHAR(50)
	
	SET @AccountId = 0
	SET @StatusCode = 2
	SET @StatusText = 'Unexpected error'

	---------------------------------------------------------
	-- Parameters Validations
	---------------------------------------------------------
	
	---------------------------------------------------------
	SET @ExternalPlayerId = ISNULL(@ExternalPlayerId, 0)
	SET @Name1 = ISNULL(RTRIM(LTRIM(@Name1)), '')
	SET @LastName1 = ISNULL(RTRIM(LTRIM(@LastName1)), '')
	SET @DocId = ISNULL(RTRIM(LTRIM(@DocId)), '')
	SET @DocType = ISNULL(@DocType, 0)
	SET @Gender = ISNULL(@Gender, -1)

	IF (@ExternalPlayerId <= 0)
	BEGIN
		SET @StatusCode = 10
		SET @StatusText = 'The @ExternalPlayerId parameter is mandatory and should be different to 0'
		GOTO ERROR_PROCEDURE
	END 

	IF (@Name1 = '')
	BEGIN
		SET @StatusCode = 11
		SET @StatusText = 'The @Name1 parameter is mandatory and should be different to empty'
		GOTO ERROR_PROCEDURE
	END 

	IF (@LastName1 = '')
	BEGIN
		SET @StatusCode = 12
		SET @StatusText = 'The @LastName1 parameter is mandatory and should be different to empty'
		GOTO ERROR_PROCEDURE
	END 
		 
	IF (@DocId = '')
	BEGIN
		SET @StatusCode = 13
		SET @StatusText = 'The @DocId parameter is mandatory and should be different to empty'
		GOTO ERROR_PROCEDURE
	END  

	IF (@DateOfBirth IS NULL)
	BEGIN
		SET @StatusCode = 14
		SET @StatusText = 'The @DateOfBirth parameter is mandatory'
		GOTO ERROR_PROCEDURE
	END  
 
	IF (@DocType < 110 OR @DocType > 117)
	BEGIN
		SET @StatusCode = 15
		SET @StatusText = 'The @DocType parameter is invalid'
		GOTO ERROR_PROCEDURE
	END 

	IF (@Gender < 1 OR @Gender > 2)
	BEGIN
		SET @StatusCode = 16
		SET @StatusText = 'The @Gender parameter is invalid'
		GOTO ERROR_PROCEDURE
	END 
	---------------------------------------------------------

	---------------------------------------------------------
	-- Data Validations
	---------------------------------------------------------

	IF EXISTS (SELECT 1 FROM ACCOUNTS WITH (INDEX(IX_ac_external_reference)) WHERE AC_EXTERNAL_REFERENCE = CAST(@ExternalPlayerId AS NVARCHAR(50)))
	BEGIN
		SET @StatusCode = 30
		SET @StatusText = 'The @ExternalPlayerId is assgined to another customer'
		GOTO ERROR_PROCEDURE
	END

	IF EXISTS (SELECT 1 FROM ACCOUNTS WITH (INDEX(IX_ac_holder_id)) WHERE AC_HOLDER_ID = @DocId AND AC_HOLDER_ID_TYPE = @DocType )
	BEGIN
		SET @StatusCode = 31
		SET @StatusText = 'The document @DocId is assgined to another customer'
		GOTO ERROR_PROCEDURE
	END

	IF EXISTS (SELECT 1 FROM ACCOUNTS WITH (INDEX(IX_ac_holder_birth_date)) 
										 WHERE AC_HOLDER_BIRTH_DATE = @DateOfBirth 
											 AND AC_HOLDER_NAME3 = @Name1 
											 AND AC_HOLDER_NAME4 = @Name2 
											 AND AC_HOLDER_NAME1 = @LastName1 
											 AND AC_HOLDER_NAME2 = @LastName2 
											 AND AC_HOLDER_GENDER = @Gender)
	BEGIN
		SET @StatusCode = 32
		SET @StatusText = 'A customer with this data already exists'
		GOTO ERROR_PROCEDURE
	END

	---------------------------------------------------------
	-- Create account
	---------------------------------------------------------

	BEGIN TRY
		-- Validate TrackData
		SET @_internal_track_data = ''

		IF @TrackData is not null and @TrackData <> ''
		BEGIN
			SET @_internal_track_data = dbo.TrackDataToInternal(@TrackData)
			IF @_internal_track_data = ''
			BEGIN
				SET @StatusCode = 33
				SET @StatusText = 'Trackdata is not a valid Wigos card'
				GOTO ERROR_PROCEDURE
			END
		END

		-- Build holder name
		IF @Name1 <> ''
			SET @_holder_name = @Name1
		IF @Name2 <> ''
			SET @_holder_name = @_holder_name + ' ' + @Name2
		IF @LastName1 <> ''
			SET @_holder_name =  @_holder_name + ' ' + @LastName1
		IF @LastName2 <> ''
			SET @_holder_name =  @_holder_name + ' ' + @LastName2

		BEGIN TRANSACTION
				
		-- Create account     
		EXECUTE CreateAccount @AccountId OUTPUT

		IF (@_internal_track_data = '')
		BEGIN
				SET @_internal_track_data =  '00000000000000000000-RECYCLED-VIRTUAL-' + CONVERT(VARCHAR(19), @AccountId)
		END

		-- Update account
		UPDATE    ACCOUNTS
			 SET    AC_EXTERNAL_REFERENCE   = CAST(@ExternalPlayerId AS NVARCHAR(50))
						, AC_HOLDER_NAME3         = @Name1
						, AC_HOLDER_NAME4         = @Name2
						, AC_HOLDER_NAME1         = @LastName1
						, AC_HOLDER_NAME2         = @LastName2
						, AC_HOLDER_NAME          = @_holder_name
						, AC_HOLDER_ID_TYPE       = @DocType
						, AC_HOLDER_ID            = @DocId
						, AC_HOLDER_BIRTH_DATE    = @DateOfBirth
						, AC_HOLDER_GENDER        = @Gender
						, AC_TRACK_DATA           = @_internal_track_data
						, AC_BLOCKED              = 0
						, AC_USER_TYPE            = 1
						, AC_HOLDER_LEVEL         = 1
						, AC_HOLDER_LEVEL_ENTERED = GETDATE()
						, AC_POINTS_STATUS        = 0
						, AC_CARD_PAID            = 1
		 WHERE    AC_ACCOUNT_ID           = @AccountId

		COMMIT TRANSACTION

		SET @StatusCode = 0
		SET @StatusText = 'Success'

	END TRY
	BEGIN CATCH
		ROLLBACK
		SET @StatusCode = 1
		SET @StatusText = 'EXCEPTION in WSP_002_CreateAccount. Operation canceled! ERROR: ' + CAST(ERROR_MESSAGE() AS NVARCHAR(MAX))
	END CATCH

	ERROR_PROCEDURE:
	
END  -- WSP_002_CreateAccount                                                                                  

GO

IF  EXISTS (SELECT * FROM sys.server_principals WHERE name = N'wg_datareader' AND type in (N'S'))
	IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'wg_datareader' AND type in (N'S'))
	BEGIN
		GRANT EXECUTE ON [dbo].[WSP_002_CreateAccount] TO [wg_datareader] WITH GRANT OPTION
	END
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WSP_002_GetCustomerIdFromExternalId]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[WSP_002_GetCustomerIdFromExternalId]
GO
CREATE FUNCTION dbo.WSP_002_GetCustomerIdFromExternalId
 (@ExternalPlayerId       Bigint) 
RETURNS Bigint
AS
BEGIN

	RETURN (ISNULL((SELECT AC_ACCOUNT_ID FROM ACCOUNTS WITH (INDEX(IX_ac_external_reference)) WHERE AC_EXTERNAL_REFERENCE = CAST(@ExternalPlayerId AS NVARCHAR(50))),0))
	
END -- WSP_002_GetCustomerIdFromExternalId

GO

IF  EXISTS (SELECT * FROM sys.server_principals WHERE name = N'wg_datareader' AND type in (N'S'))
	IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'wg_datareader' AND type in (N'S'))
	BEGIN
		GRANT EXECUTE ON [dbo].[WSP_002_GetCustomerIdFromExternalId] TO [wg_datareader] WITH GRANT OPTION
	END
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WSP_002_GetCustomerIdFromTrackdata]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[WSP_002_GetCustomerIdFromTrackdata]
GO
CREATE FUNCTION dbo.WSP_002_GetCustomerIdFromTrackdata
 (@Trackdata       NVarchar(40)) 
RETURNS Bigint
AS
BEGIN
		
	RETURN (ISNULL((SELECT AC_ACCOUNT_ID FROM ACCOUNTS WITH (INDEX(IX_track_data)) WHERE AC_TRACK_DATA = dbo.TrackDataToInternal(@Trackdata)),0))
	
END -- WSP_002_GetCustomerIdFromTrackdata

GO

IF  EXISTS (SELECT * FROM sys.server_principals WHERE name = N'wg_datareader' AND type in (N'S'))
	IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'wg_datareader' AND type in (N'S'))
	BEGIN
		GRANT EXECUTE ON [dbo].[WSP_002_GetCustomerIdFromTrackdata] TO [wg_datareader] WITH GRANT OPTION
	END
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WSP_002_GetTrackdata]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[WSP_002_GetTrackdata]
GO
CREATE FUNCTION dbo.WSP_002_GetTrackdata
 (@CustomerId       BigInt) 
RETURNS NVarchar(40)
AS
BEGIN

	RETURN (ISNULL((SELECT  dbo.TrackDataToExternal(AC_TRACK_DATA) FROM ACCOUNTS WITH (INDEX(PK_accounts)) WHERE AC_ACCOUNT_ID = @CustomerId),''))
	
END -- WSP_002_GetTrackdata

GO

IF  EXISTS (SELECT * FROM sys.server_principals WHERE name = N'wg_datareader' AND type in (N'S'))
	IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'wg_datareader' AND type in (N'S'))
	BEGIN
		GRANT EXECUTE ON [dbo].[WSP_002_GetTrackdata] TO [wg_datareader] WITH GRANT OPTION
	END
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WSP_002_GetExternalId]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[WSP_002_GetExternalId]
GO
CREATE FUNCTION dbo.WSP_002_GetExternalId
 (@CustomerId       BigInt) 
RETURNS Bigint
AS
BEGIN

	DECLARE @_external_reference NVARCHAR(50)
	SELECT @_external_reference = AC_EXTERNAL_REFERENCE FROM ACCOUNTS WITH (INDEX(IX_ac_external_reference)) WHERE AC_ACCOUNT_ID = @CustomerId
	
	IF (@_external_reference IS NULL OR ISNUMERIC (@_external_reference) <> 1) RETURN 0

	RETURN CAST(@_external_reference AS BIGINT)
	
END -- WSP_002_GetExternalId

GO

IF  EXISTS (SELECT * FROM sys.server_principals WHERE name = N'wg_datareader' AND type in (N'S'))
	IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'wg_datareader' AND type in (N'S'))
	BEGIN
		GRANT EXECUTE ON [dbo].[WSP_002_GetExternalId] TO [wg_datareader] WITH GRANT OPTION
	END
GO

-- TRINIDAD AND TOBAGO OCCUPATIONS
IF (NOT EXISTS (SELECT * FROM occupations WHERE oc_country_iso_code2 = 'TT' AND oc_description = 'ACCOUNTANT'))
BEGIN
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('OTHER','0000',1,1,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('ACCOUNTANT','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('ACCOUNTANT ASSISTANT','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('ACCOUNTS CLERK','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('ADMIN ASSISTANT','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('ADMIN CLERK','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('ADMIN COMMUNICATIONS','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('ADMINISTRATOR','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('AIR GUARD OFFICER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('AIRCRAFT TECHNICIAN','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('AIRPORT AUTHORITY STORE CLERK','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('ASSISTANT SUPERVISOR','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('ASSISTANT TRANSPORT COMMISSIONER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('AUDITOR','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('AUTO ELECTRICIAN','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('AUTO MECHANIC','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('BANK TELLER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('BAR MANAGER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('BAR OWNER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('BARBER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('BILINGUAL AGENT','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('BLENDING TECHNICIAN','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('BRANCH MANAGER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('BUSINESS OWNER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('CARGO AGENT','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('CARGO HANDLER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('CARGO TECHNICIAN','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('CARIBBEAN AIRLINES MAINTENANCE PERSONNEL','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('CARPENTER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('CARWASH OWNER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('CASH PROCESSING OFFICER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('CASHIER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('CATERER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('CATERSERVE UNILEVER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('CCTV OPERATOR','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('CEO','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('CEPEP LABORER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('CHECKER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('CHIEF','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('CIVIL ENGINEER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('CLEANER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('CLERICAL ASSISTANT','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('CLERK','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('CLERK II','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('CME FOREMAN','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('COAST GUARD','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('COMMUNITY ACTION OFFICER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('COMPUTER OPERATOR','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('COMPUTER SERVICE REPRESENTATIVE','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('CONSTRUCTION COMPANY OWNER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('CONSTRUCTION WORKER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('CONSULTANT','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('CONTRACTED WELDER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('CONTRACTOR','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('COOK','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('COORDINATOR','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('CORPORATE ENGAGEMENT OFFICER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('CORRECTIONAL OFFICER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('COURT MARSHALL','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('CRAFTSMAN','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('CREDIT CONTROL ASSISTANT','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('CSR','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('CSR SUPERVISOR','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('CUSTODIAN','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('CUSTOMS OFFICER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('DATA ENTRY CLARK','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('DAY CARE ASSISTANT','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('DEAN','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('DENTAL HYGIENIST','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('DEVELOPMENT COACH','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('DIALYSIS TECHNICIAN','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('DIRECTOR','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('DISPATCHING AND RECEIVING CLERK','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('DOCTOR','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('DOMESTIC WORKER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('DOMESTIC WORKER PRIVATE HOMES','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('DOUBLES VENDOR','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('DRIVER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('DRIVER / SALESMAN','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('ELECTRICIAN','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('ENA NURSE ASSISTANT','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('ENGINEER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('ENROLLED NURSE','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('ENTERTAINMENT','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('ESTATE CONSTABLE','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('ESTHETICIAN','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('EVENT MANAGER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('EXECUTIVE ASSISTANT','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('FACILITY SUPERVISOR','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('FARMER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('FIRE FIGHTER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('FISHERMAN','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('FOREMAN','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('FORKLIFT DRIVER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('FORM MISTRESS','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('FREELANCE ADVERTISER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('FREELANCE CONSTRUCTION WORKER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('FREELANCE CONSULTANT','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('FREELANCE CONTRACTOR','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('FREELANCE MASON','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('FREELANCE REAL ESTATE AGENT','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('FRUIT VENDOR','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('GARDENER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('GENERAL MANAGER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('GERIATRIC NURSE','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('GLASS TECHNICIAN','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('GRAPHIC ARTIST','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('GRAPHICS INSTALLER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('GRILL FOOD SHOP OWNER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('GYM INSTRUCTOR','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('HAIRDRESSER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('HANDYMAN','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('HEAD CUSTODIAN','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('HEALTH AND SAFETY OFFICER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('HOMEMAKER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('HOSTESS','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('HOUSEWIFE','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('HOUSEWIFE / PENSIONER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('HUMAN RESOURCES CLERK','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('HUMAN RESOURCES MANAGER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('IMMIGRATION DETENTION OFFICER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('IMPORTER / CONSULTANT','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('INDEPENDENT EDUCATION CONSULTANT','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('INSURANCE AGENT','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('INSURANCE MANAGER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('INSURANCE UNDERWRITER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('INTERNAL AUDITOR','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('IT TECHNICIAN','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('JANITOR','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('JEWELER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('JOURNALIST','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('KITCHEN ASSISTANT','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('LAB TECHNICIAN','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('LABORER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('LAND ASSISTANCE OFFICER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('LANDLADY','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('LANDLORD AND FRUIT VENDOR','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('LANDSCAPER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('LANDSCAPER / FARMER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('LAUNDRY ATTENDANT','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('LECTURER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('LEGAL CONSULTANT','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('LEGAL SECRETARY','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('LINES MAN','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('LOAN CONTROL AGENT','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('LOTTO MACHINE OPERATOR','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('MACHINE OPERATOR','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('MAILMAN','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('MAINTENANCE','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('MAINTENANCE SUPERVISOR','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('MAINTENANCE TECHNICIAN','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('MANAGER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('MARINE OPERATOR','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('MARKETING AND COMMUNICATIONS SPECIALIST','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('MARKETING CONSULTANT','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('MECHANIC','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('MEDICAL CLERK','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('MEDICAL SALES REPRESENTATIVE','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('MERCHANDISER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('NAIL TECHNICIAN','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('NANNY','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('NURSE','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('OFFICE ASSISTANT','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('OFFICE ATTENDANT','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('OFFICE CLERK','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('OFFICE MANAGER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('OFFICER / PLUMBER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('OPERATIONS MANAGER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('OPERATOR','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('ORDERLY','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('PAINT TECHNICIAN','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('PAINTER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('PATIENT CARE ASSISTANT','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('PERISHABLE MANAGER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('PET STORE OWNER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('PHARMACY TECHNICIAN','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('PLANT PROCESS OPERATOR','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('PLANT SHOP OWNER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('PLUMBER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('PLUMBING SUPERVISOR','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('POLICE OFFICER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('POSTAL WORKER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('PRE-SCHOOL TEACHER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('PRISON OFFICER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('PRIVATE CONTRACTOR','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('PROCESS TECHNICIAN','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('PRODUCTION WORKER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('PROFESSIONAL MERCHANDISER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('PROGRAM COORDINATOR','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('PROJECT ASSISTANT','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('PROJECT OFFICER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('PROPRIETOR','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('PROPRIETOR BEVECA SPORTS BAR AND LOUNGE','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('PROPRIETOR CALDON''S CORNER BAR','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('PROPRIETOR HAMLET APPLIANCE PARTS AND REPAIR SERVICES','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('PROPRIETOR HULDER ENTERPRISES','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('PROPRIETOR JOINER AT CASKET CLASSIQUE','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('PROPRIETOR RODUBA CONTRACTING CONCEPTS AND CAR SERVICES','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('PROPRIETOR VERTEX SECURITY SOLUTIONS','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('PSYCHOLOGIST','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('PUBLIC HEALTH OFFICER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('PURCHASING OFFICER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('QUALITY CONTROL INSPECTOR','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('RADIOGRAPHER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('RAMP ATTENDANT','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('REAL ESTATE AGENT','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('REAL ESTATE BROKER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('REALTOR','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('RECEIVING CLERK','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('RECEPTIONIST','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('RECRUITMENT OFFICER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('REGISTERED NURSE','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('REGISTRATION OFFICER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('REHABILITATION AID','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('RETIRED','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('RETIRED PILOT','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('RETIRED POLICE OFFICER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('RETIRED SOLDIER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('RIGGER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('SALES AGENT','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('SALES ASSOCIATE','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('SALES CLERK','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('SALES CONSULTANT','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('SALES REPRESENTATIVE','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('SALESMAN HELPER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('SALESWOMAN','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('SANITATION WORKER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('SCHOOL LABORATORY TECHNICIAN','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('SEAMAN','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('SECRETARY','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('SECURITY GUARD','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('SECURITY OFFICER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('SELF-EMPLOYED BARBECUE BOSS','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('SELF-EMPLOYED BHAM STYLING','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('SENIOR COURSE ADMINISTRATOR','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('SENIOR INTERFACE OFFICER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('SENIOR PROJECT OFFICER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('SENIOR REGISTRATION OFFICER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('SERVICE TECHNICIAN','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('SHIFT SUPERVISOR','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('SHIPPING MANAGER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('SKY CAP PORTER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('SOLDIER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('STENOGRAPHER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('STORE CLERK','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('STUDENT','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('SUPERVISOR','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('TAXI DRIVER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('TEACHER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('TEAM LEADER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('TECHNICAL INSTRUCTOR','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('TECHNICAL OFFICER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('TECHNICIAN','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('TELEPHONE OPERATOR','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('TELEPHONE TECHNICIAN','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('TRADE DEVELOPMENT SPECIALIST','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('TRADESMAN','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('TRAINING REPRESENTATIVE','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('TRANSPORT DRIVER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('TRUCK CONTRACTOR','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('TRUCK DRIVER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('TSTT ENGINEER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('TUTOR / CATERER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('UNDERTAKER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('UNDERWRITER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('UPHOLSTERER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('VALIDATOR','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('VICE PRINCIPAL','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('WARDSMAID','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('WAREHOUSE CLERK','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('WAREHOUSE COORDINATOR','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('WAREHOUSE MANAGER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('WELDER','0000',1,1000,'TT')
	 INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('WOODSMAN','0000',1,1000,'TT')
END
GO

-- TRINIDAD AND TOBAGO IDENTIFICATION_TYPES
-- INSERTED
-- INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (80,1,100,'Other','TT')
-- INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (81,1,1,'Driver''s license','TT')
-- INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (82,1,2,'Passport','TT')

IF (NOT EXISTS (SELECT * FROM identification_types WHERE idt_country_iso_code2 = 'TT' AND idt_id = 120))
BEGIN
	 INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (120,1,3,'National ID','TT')
	 INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (121,1,4,'Membership Form','TT')
	 INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (122,1,5,'Proof of Address','TT')
END
GO

