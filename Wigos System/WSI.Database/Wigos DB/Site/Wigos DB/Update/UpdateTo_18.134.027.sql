/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 133;

SET @New_ReleaseId = 134;
SET @New_ScriptName = N'UpdateTo_18.134.027.sql';
SET @New_Description = N'';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END
 
/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/

--
--
-- 

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[gui_user_profiles]') and name = 'gup_master_sequence_id')  
BEGIN 
  EXECUTE sp_rename N'dbo.gui_user_profiles.gup_master_secuence_id', N'gup_master_sequence_id', 'COLUMN' 
END
GO

/****** VIEWS ******/

/****** CONSTRAINTS ******/

/****** FUNCTIONS ******/

/****** INDEXES ******/

/****** STORED PROCEDURES ******/

--------------------------------------------------------------------------------
-- Copyright � 2013 Win Systems International
--------------------------------------------------------------------------------
--
--   MODULE NAME: Update_PersonalInfo.sql.sql
--
--   DESCRIPTION: Update personal Information 
--
--        AUTHOR: Dani Dom�nguez
--
-- CREATION DATE: 08-MAR-2013
--
-- REVISION HISTORY:
--
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 08-MAR-2013 DDM    First release.  
-- 22-MAY-2013 DDM    Fixed bugs 783,793 and 693
--                    Added field AC_BLOCK_DESCRIPTION
-- 28-MAY-2013 DDM    Fixed bug #803
--                    Added field AC_EXTERNAL_REFERENCE
-- 03-JUL-2013 DDM    Added new fields about Money Laundering
--------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Update_PersonalInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Update_PersonalInfo]
GO
CREATE PROCEDURE [dbo].[Update_PersonalInfo]
  @pAccountId bigint
, @pTrackData nvarchar(50)
,	@pHolderName nvarchar(200)
,	@pHolderId nvarchar(20)
, @pHolderIdType int
, @pHolderAddress01 nvarchar(50)
, @pHolderAddress02 nvarchar(50)
, @pHolderAddress03 nvarchar(50)
, @pHolderCity nvarchar(50)
, @pHolderZip  nvarchar(10) 
, @pHolderEmail01 nvarchar(50)
,	@pHolderEmail02 nvarchar(50)
,	@pHolderTwitter nvarchar(50)
,	@pHolderPhoneNumber01 nvarchar(20)
, @pHolderPhoneNumber02 nvarchar(20)
, @pHolderComments nvarchar(100)
, @pHolderId1 nvarchar(20)
, @pHolderId2 nvarchar(20)
, @pHolderDocumentId1 bigint
, @pHolderDocumentId2 bigint
, @pHolderName1 nvarchar(50)
,	@pHolderName2 nvarchar(50)
,	@pHolderName3 nvarchar(50)
, @pHolderGender  int
, @pHolderMaritalStatus int
, @pHolderBirthDate datetime
, @pHolderWeddingDate datetime
, @pHolderLevel int
, @pHolderLevelNotify int
, @pHolderLevelEntered datetime
,	@pHolderLevelExpiration datetime
,	@pPin nvarchar(12)
, @pPinFailures int
, @pPinLastModified datetime
, @pBlocked bit
, @pActivated bit
, @pBlockReason int
, @pHolderIsVip int
, @pHolderTitle nvarchar(15)                       
, @pHolderName4 nvarchar(50)                       
, @pHolderPhoneType01  int
, @pHolderPhoneType02  int
, @pHolderState    nvarchar(50)                    
, @pHolderCountry  nvarchar(50)                
, @pPersonalInfoSequenceId  bigint                     
, @pUserType int
, @pPointsStatus int
, @pDeposit money
, @pCardPay bit
, @pBlockDescription nvarchar(256) 
, @pMSHash varbinary(20) 
, @pMSCreatedOnSiteId int
, @pCreated datetime
, @pExternalReference nvarchar(50) 
, @pHolderOccupation  nvarchar(50) 	
, @pHolderExtNum      nvarchar(10) 
, @pHolderNationality  Int 
, @pHolderBirthCountry Int 
, @pHolderFedEntity    Int 
, @pHolderId1Type      Int -- RFC
, @pHolderId2Type      Int -- CURP
, @pHolderId3Type      Int 
, @pHolderId3           nvarchar(20) 
, @pHolderAsBeneficiary bit  
, @pBeneficiaryName     nvarchar(200) 
, @pBeneficiaryName1    nvarchar(50) 
, @pBeneficiaryName2    nvarchar(50) 
, @pBeneficiaryName3    nvarchar(50) 
, @pBeneficiaryBirthDate Datetime 
, @pBeneficiaryGender    int 
, @pBeneficiaryOccupation nvarchar(50) 
, @pBeneficiaryId1Type    int   -- RFC
, @pBeneficiaryId1        nvarchar(20) 
, @pBeneficiaryId2Type    int   -- CURP
, @pBeneficiaryId2        nvarchar(20) 
, @pBeneficiaryId3Type    int 
, @pBeneficiaryId3  	    nvarchar(20)
AS
BEGIN

DECLARE @pOtherAccountId as BIGINT

SET @pOtherAccountId = ISNULL ((SELECT AC_ACCOUNT_ID FROM ACCOUNTS WHERE AC_TRACK_DATA = @pTrackData), 0)

SET @pUserType = ISNULL(@pUserType,CASE WHEN ISNULL(@pHolderLevel,0) > 0  THEN 1 ELSE 0 END)

IF @pOtherAccountId <> 0 AND @pOtherAccountId <> @pAccountId
  UPDATE   ACCOUNTS   
     SET   AC_TRACK_DATA =  '-RECYCLED-NEW-' + CAST (NEWID() AS NVARCHAR(50))
   WHERE   AC_ACCOUNT_ID = @pOtherAccountId


IF NOT EXISTS (SELECT 1 FROM ACCOUNTS WHERE ac_account_id = @pAccountId )  
BEGIN
  INSERT INTO   ACCOUNTS 
              ( AC_ACCOUNT_ID, AC_TYPE, AC_BLOCKED, AC_TRACK_DATA) 
       VALUES ( @pAccountId,         2,  @pBlocked, @pTrackData )
END  

 
  UPDATE   ACCOUNTS                   
     SET   AC_TRACK_DATA              = @pTrackData 
         , AC_HOLDER_NAME             = @pHolderName 
         , AC_HOLDER_ID               = @pHolderId 
         , AC_HOLDER_ID_TYPE          = @pHolderIdType 
         , AC_HOLDER_ADDRESS_01       = @pHolderAddress01 
         , AC_HOLDER_ADDRESS_02       = @pHolderAddress02 
         , AC_HOLDER_ADDRESS_03       = @pHolderAddress03 
         , AC_HOLDER_CITY             = @pHolderCity 
         , AC_HOLDER_ZIP              = @pHolderZip  
         , AC_HOLDER_EMAIL_01         = @pHolderEmail01 
         , AC_HOLDER_EMAIL_02         = @pHolderEmail02 
         , AC_HOLDER_TWITTER_ACCOUNT  = @pHolderTwitter 
         , AC_HOLDER_PHONE_NUMBER_01  = @pHolderPhoneNumber01 
         , AC_HOLDER_PHONE_NUMBER_02  = @pHolderPhoneNumber02 
         , AC_HOLDER_COMMENTS         = @pHolderComments 
         , AC_HOLDER_ID1              = @pHolderId1 
         , AC_HOLDER_ID2              = @pHolderId2 
         , AC_HOLDER_DOCUMENT_ID1     = @pHolderDocumentId1 
         , AC_HOLDER_DOCUMENT_ID2     = @pHolderDocumentId2 
         , AC_HOLDER_NAME1            = @pHolderName1 
         , AC_HOLDER_NAME2            = @pHolderName2 
         , AC_HOLDER_NAME3            = @pHolderName3 
         , AC_HOLDER_GENDER           = @pHolderGender  
         , AC_HOLDER_MARITAL_STATUS   = @pHolderMaritalStatus 
         , AC_HOLDER_BIRTH_DATE       = @pHolderBirthDate 
         , AC_HOLDER_WEDDING_DATE     = @pHolderWeddingDate 
         , AC_HOLDER_LEVEL            = @pHolderLevel 
         , AC_HOLDER_LEVEL_NOTIFY     = @pHolderLevelNotify 
         , AC_HOLDER_LEVEL_ENTERED    = @pHolderLevelEntered 
         , AC_HOLDER_LEVEL_EXPIRATION = @pHolderLevelExpiration 
         , AC_PIN                     = @pPin 
         , AC_PIN_FAILURES            = @pPinFailures 
         , AC_PIN_LAST_MODIFIED       = @pPinLastModified 
         , AC_BLOCKED                 = @pBlocked 
         , AC_ACTIVATED               = @pActivated 
         , AC_BLOCK_REASON            = @pBlockReason 
         , AC_HOLDER_IS_VIP           = @pHolderIsVip 
         , AC_HOLDER_TITLE            = @pHolderTitle 
         , AC_HOLDER_NAME4            = @pHolderName4         
         , AC_HOLDER_PHONE_TYPE_01    = @pHolderPhoneType01  
         , AC_HOLDER_PHONE_TYPE_02    = @pHolderPhoneType02  
         , AC_HOLDER_STATE            = @pHolderState    
         , AC_HOLDER_COUNTRY          = @pHolderCountry  					
         , AC_MS_PERSONAL_INFO_SEQ_ID = @pPersonalInfoSequenceId           
         , AC_USER_TYPE				        = @pUserType
         , AC_POINTS_STATUS           = @pPointsStatus
         , AC_MS_CHANGE_GUID          = AC_MS_CHANGE_GUID -- avoid trigger on update
         , AC_DEPOSIT                 = @pDeposit 
         , AC_CARD_PAID               = @pCardPay 
         , AC_BLOCK_DESCRIPTION       = @pBlockDescription                
         , AC_CREATED                 = @pCreated
         , AC_MS_CREATED_ON_SITE_ID   = @pMSCreatedOnSiteId
         , AC_MS_HASH                 = @pMSHash
         , AC_EXTERNAL_REFERENCE      = ISNULL(@pExternalReference,AC_EXTERNAL_REFERENCE)
         , AC_HOLDER_OCCUPATION       = @pHolderOccupation
         , AC_HOLDER_EXT_NUM          = @pHolderExtNum
         , AC_HOLDER_NATIONALITY      = @pHolderNationality 
         , AC_HOLDER_BIRTH_COUNTRY    = @pHolderBirthCountry 
         , AC_HOLDER_FED_ENTITY       = @pHolderFedEntity
         , AC_HOLDER_ID1_TYPE         = @pHolderId1Type 
         , AC_HOLDER_ID2_TYPE         = @pHolderId2Type 
         , AC_HOLDER_ID3_TYPE         = @pHolderId3Type 
         , AC_HOLDER_ID3              = @pHolderId3
         , AC_HOLDER_AS_BENEFICIARY   = @pHolderAsBeneficiary 
         , AC_BENEFICIARY_NAME        = @pBeneficiaryName 
         , AC_BENEFICIARY_NAME1       = @pBeneficiaryName1
         , AC_BENEFICIARY_NAME2       = @pBeneficiaryName2
         , AC_BENEFICIARY_NAME3       = @pBeneficiaryName3
         , AC_BENEFICIARY_BIRTH_DATE  = @pBeneficiaryBirthDate 
         , AC_BENEFICIARY_GENDER      = @pBeneficiaryGender 
         , AC_BENEFICIARY_OCCUPATION  = @pBeneficiaryOccupation
         , AC_BENEFICIARY_ID1_TYPE    = @pBeneficiaryId1Type 
         , AC_BENEFICIARY_ID1         = @pBeneficiaryId1 
         , AC_BENEFICIARY_ID2_TYPE    = @pBeneficiaryId2Type 
         , AC_BENEFICIARY_ID2         = @pBeneficiaryId2 
         , AC_BENEFICIARY_ID3_TYPE    = @pBeneficiaryId3Type 
         , AC_BENEFICIARY_ID3         = @pBeneficiaryId3 
   WHERE   AC_ACCOUNT_ID              = @pAccountId 

END
GO

--------------------------------------------------------------------------------
-- Copyright � 2013 Win Systems International
--------------------------------------------------------------------------------
--
--   MODULE NAME: Update_MasterProfiles.sql
--
--   DESCRIPTION: Update Master Profiles in the Site
--
--        AUTHOR: Dani Dom�nguez
--
-- CREATION DATE: 21-JUN-2013
--
-- REVISION HISTORY:
--
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 21-JUN-2013 DDM    First release.  
-- 25-JUN-2013 JML    Add insertion of the form in the site
-- 26-JUN-2013 JML    Add "order" and "NLS" of the form
-- 09-JUL-2013 JML    Change GUP_MASTER_SECUENCE_ID by GUP_MASTER_SEQUENCE_ID
--                    Add condition for GUI_FORMS (defect WIG-46)
-------------------------------------------------------------------------------- 

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Update_MasterProfiles]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Update_MasterProfiles]
GO 

CREATE PROCEDURE [dbo].[Update_MasterProfiles]
  @pReturn INT OUTPUT
AS
BEGIN

  DECLARE @MasterId           AS BIGINT
  DECLARE @ProfileName        AS NVARCHAR(40)
  DECLARE @ProfileId          AS BIGINT
  DECLARE @MaxUsers           AS INT
  DECLARE @MasterTimeStamp    AS BIGINT
  
  DECLARE @ProfileGuiId       AS INT
  DECLARE @ProfileFormId      AS INT
  DECLARE @ProfileReadPerm    AS BIT
  DECLARE @ProfileWritePerm   AS BIT
  DECLARE @ProfileDeletePerm  AS BIT
  DECLARE @ProfileExecutePerm AS BIT

  DECLARE @FormOrder          AS INT
  DECLARE @FormNLSId          AS INT

  SET @pReturn = 0

  -- (Defect WIG-46) Added the condition "IS NOT NULL GPF_GUI_ID", because when the profile is left without permissions tries to insert with nulls. 
  DECLARE FormsCursor CURSOR FOR SELECT DISTINCT GPF_GUI_ID, GPF_FORM_ID, GF_FORM_ORDER, GF_NLS_ID FROM #TEMP_MASTERS_PROFILES WHERE GPF_GUI_ID IS NOT NULL
  OPEN    FormsCursor

  FETCH NEXT FROM FormsCursor INTO @ProfileGuiId, @ProfileFormId, @FormOrder, @FormNLSId
  WHILE @@FETCH_STATUS = 0
  BEGIN
	  IF NOT EXISTS (SELECT 1 FROM GUI_FORMS WHERE GF_GUI_ID = @ProfileGuiId AND GF_FORM_ID = @ProfileFormId )
    BEGIN
		  INSERT INTO GUI_FORMS (GF_GUI_ID,     GF_FORM_ID,     GF_FORM_ORDER, GF_NLS_ID  ) 
                     VALUES (@ProfileGuiId, @ProfileFormId, @FormOrder,    @FormNLSId )
	  END
	  FETCH NEXT FROM FormsCursor INTO @ProfileGuiId, @ProfileFormId, @FormOrder, @FormNLSId
  END

  CLOSE      FormsCursor
  DEALLOCATE FormsCursor

  DECLARE ProfilesCursor CURSOR FOR SELECT DISTINCT GUP_MASTER_ID, GUP_NAME, GUP_MAX_USERS, GUP_TIMESTAMP  FROM #TEMP_MASTERS_PROFILES 
  OPEN    ProfilesCursor

  FETCH NEXT FROM ProfilesCursor INTO @MasterId, @ProfileName, @MaxUsers, @MasterTimeStamp
  WHILE @@FETCH_STATUS = 0
  BEGIN
    SET @ProfileId = NULL
    SET @ProfileId = ( SELECT GUP_PROFILE_ID FROM GUI_USER_PROFILES WHERE GUP_MASTER_ID = @MasterId )

    IF @ProfileId IS NULL 
    BEGIN
      SET @ProfileId = ( SELECT GUP_PROFILE_ID FROM GUI_USER_PROFILES WHERE GUP_NAME = @ProfileName )
    END

    IF @ProfileId IS NULL 
    BEGIN
      -- is new profile
      SET @ProfileId = ISNULL((SELECT MAX(GUP_PROFILE_ID) FROM GUI_USER_PROFILES), 0) + 1
      
      INSERT INTO GUI_USER_PROFILES (GUP_PROFILE_ID, GUP_NAME) 
                             VALUES (@ProfileId,     @ProfileName)
    END

    UPDATE   GUI_USER_PROFILES 
       SET   GUP_MAX_USERS          = ISNULL(@MaxUsers, 0) 
           , GUP_NAME               = @ProfileName 
           , GUP_MASTER_ID          = @MasterId
           , GUP_MASTER_SEQUENCE_ID = @MasterTimeStamp
     WHERE   GUP_PROFILE_ID = @ProfileId

    DELETE   GUI_PROFILE_FORMS
     WHERE   GPF_PROFILE_ID = @ProfileId
    
    INSERT   INTO GUI_PROFILE_FORMS 
		       ( GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM )
		       
	  SELECT   @ProfileId,     GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM 
	    FROM   #TEMP_MASTERS_PROFILES
	   WHERE   GUP_NAME = @ProfileName
	     AND   GPF_READ_PERM IS NOT NULL

    FETCH NEXT FROM ProfilesCursor INTO @MasterId, @ProfileName, @MaxUsers, @MasterTimeStamp
  END
  CLOSE      ProfilesCursor
  DEALLOCATE ProfilesCursor

  DROP TABLE #TEMP_MASTERS_PROFILES
  
  SET @pReturn = 1
 
END
GO

  --------------------------------------------------------------------------------
  -- Copyright � 2013 Win Systems International
  --------------------------------------------------------------------------------
  --
  --   MODULE NAME: Insert_Providers.sql
  --
  --   DESCRIPTION: Insert_Providers
  --
  --        AUTHOR: Jos� Mart�nez
  --
  -- CREATION DATE: 21-MAY-2013
  --
  -- REVISION HISTORY:
  --
  -- Date        Author Description
  -- ----------- ------ ----------------------------------------------------------
  -- 21-MAY-2013 JML    First release. Different from the center.
  -------------------------------------------------------------------------------- 
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Update_Providers]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Update_Providers]
GO

CREATE PROCEDURE [dbo].[Update_Providers]
                   @pProviderId INT
                 , @pName NVARCHAR(50)
                 , @pHide BIT
                 , @pPointsMultiplier MONEY
                 , @p3gs BIT
                 , @p3gsVendorId NVARCHAR(50)
                 , @p3gsVendorIp NVARCHAR(50)
                 , @pSiteJackpot BIT
                 , @pOnlyRedeemable BIT 
                 , @pMsSequenceId BIGINT		   
AS
BEGIN 

  DECLARE @pOtherProviderId as BIGINT
   
  IF EXISTS (SELECT 1 FROM PROVIDERS WHERE PV_ID = @pProviderId)
  BEGIN
  
    SET @pOtherProviderId = (SELECT PV_ID FROM PROVIDERS WHERE PV_NAME = @pName)
    
    IF (@pOtherProviderId IS NOT NULL AND @pOtherProviderId <> @pProviderId)
    BEGIN  
      UPDATE   PROVIDERS
         SET   PV_NAME   = 'CHANGING-' + CAST( @pOtherProviderId AS NVARCHAR)
       WHERE   PV_ID   = @pOtherProviderId	     
    END
    
    UPDATE   PROVIDERS
       SET   PV_NAME           = @pName				
           , PV_HIDE           = @pHide		  
           , PV_MS_SEQUENCE_ID = @pMsSequenceId
     WHERE   PV_ID             = @pProviderId        
  END
  ELSE
  BEGIN
    SET IDENTITY_INSERT PROVIDERS ON
    
    INSERT INTO   PROVIDERS
                ( PV_ID
                , PV_NAME
                , PV_HIDE
                , PV_MS_SEQUENCE_ID)
         VALUES ( @pProviderId			  
                , @pName				  
                , @pHide		  
                , @pMsSequenceId)
    
      SET IDENTITY_INSERT PROVIDERS OFF
  END
END 
GO

/****** TRIGGERS ******/

/****** RECORDS ******/

--
--
--

-- UploadAccountDocuments
UPDATE MS_SITE_TASKS SET ST_MAX_ROWS_TO_UPLOAD = 1 WHERE ST_TASK_ID = 13 
GO

-- DownloadAccountsDocuments
UPDATE MS_SITE_TASKS SET ST_MAX_ROWS_TO_UPLOAD = 1 WHERE ST_TASK_ID = 43 
GO

--
--
-- 

UPDATE terminals SET te_contract_type = '0'
ALTER TABLE  dbo.terminals ALTER COLUMN te_contract_type int NOT NULL
ALTER TABLE [dbo].[terminals] ADD  CONSTRAINT [DF_terminals_te_contract_type]  DEFAULT ((0)) FOR [te_contract_type]
GO

--
-- Disable/enable triggers for site members
--

IF EXISTS (SELECT   1
             FROM   GENERAL_PARAMS 
            WHERE   GP_GROUP_KEY   = N'Site' 
              AND   GP_SUBJECT_KEY = N'MultiSiteMember' 
              AND   GP_KEY_VALUE   = N'1')
BEGIN
      EXEC MultiSiteTriggersEnable 1
END
ELSE
BEGIN
      EXEC MultiSiteTriggersEnable 0

      --DELETE PENDINGS
      DELETE FROM ms_site_pending_account_movements
      DELETE FROM ms_site_pending_accounts
DELETE FROM ms_site_pending_account_documents

END
GO


