/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 42;

SET @New_ReleaseId = 43;
SET @New_ScriptName = N'UpdateTo_18.043.010.sql';
SET @New_Description = N'3GS: Check account balance changes when inserting new play session.';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/


/****** INDEXES ******/


/****** RECORDS ******/


/**************************************** 3GS Section ****************************************/

/**** CHECK DATABASE EXISTENCE SECTION *****/
GO
IF (NOT EXISTS (SELECT * FROM sys.databases WHERE name = 'sPOS'))
BEGIN
  SELECT 'No 3GS interface to update.';

  raiserror('No 3GS interface to update', 20, -1) with log;
END
ELSE 
  SELECT 'Updating 3GS interface...';


/**** 3GS DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);
    
/**** 3GS INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 9;

SET @New_ReleaseId = 10;
SET @New_ScriptName = N'UpdateTo_18.043.010.sql';
SET @New_Description = N'Check account balance changes when inserting new play session.';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version_interface_3gs WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
  /**** VERSION FAILURE SECTION *****/
  SELECT 'Wrong 3GS DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
  FROM DB_VERSION_INTERFACE_3GS

  raiserror('Not updated', 20, -1) with log
END

/**** UPDATE VERSION SECTION *****/
UPDATE db_version_interface_3gs
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;


/**** UPDATE BODY SECTION *****/
GO


/**** InsertPlaySession *****/
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InsertPlaySession]') AND type in (N'P', N'PC'))
BEGIN
  SELECT 'Procedure InsertPlaySession not found!';
  
  raiserror('Not updated', 20, -1) with log;
END
GO

ALTER PROCEDURE [dbo].[InsertPlaySession]
  	@TerminalId 	  int,
  	@AccountId 	    bigint,
  	@InitialBalance money,
  	@Promo          int
AS
BEGIN
  
  DECLARE @activity_due_to_card_in int  
  DECLARE @ac_promo_balance        money
  DECLARE @ac_balance              money
  DECLARE @playsession_id          int
  DECLARE @status_code             int
  DECLARE @status_text             varchar (254)
  DECLARE @rc                      int
  
  SET @activity_due_to_card_in = 0
  SET @ac_promo_balance        = 0 
  SET @ac_balance              = 0 
  SET @status_code             = 0
  SET @status_text             = 'Successful Session Start'
  SET @playsession_id          = 0

  SELECT   @activity_due_to_card_in = CAST (GP_KEY_VALUE AS INT) 
    FROM   GENERAL_PARAMS 
   WHERE   GP_GROUP_KEY   = 'Play' 
     AND   GP_SUBJECT_KEY = 'ActivityDueToCardIn' 
     
   -- Mark abandoned play session
  UPDATE   PLAY_SESSIONS SET PS_STATUS        = 2 -- Abandoned
         -- AJQ 18-SEP-2010 Don't touch the final balance! , PS_FINAL_BALANCE = PS_INITIAL_BALANCE - PS_PLAYED_AMOUNT + PS_WON_AMOUNT + PS_CASH_IN - PS_CASH_OUT  
         , PS_FINISHED      = GETDATE()  
   WHERE   PS_TERMINAL_ID   = @TerminalId  
     AND   PS_STATUS        = 0 -- Opened
     AND   PS_STAND_ALONE   = 0 -- Not stand alone  

  -- Get current balance on Card
  SELECT   @ac_promo_balance       = AC_PROMO_BALANCE
         , @ac_balance             = AC_BALANCE
    FROM   ACCOUNTS
   WHERE  AC_ACCOUNT_ID = @AccountId
       
  IF ( @Promo = 1 )
    BEGIN
      SET @ac_balance = @ac_promo_balance
    END
  
  IF ( @InitialBalance <> @ac_balance )
    BEGIN
      SET @status_code = 4
      SET @status_text = 'Access Denied'
      GOTO ERROR_PROCEDURE
    END
        
  INSERT INTO PLAY_SESSIONS (PS_ACCOUNT_ID 
                           , PS_TERMINAL_ID
                           , PS_TYPE
                           , PS_TYPE_DATA 
                           , PS_INITIAL_BALANCE 
                           , PS_FINAL_BALANCE 
                           , PS_FINISHED 
                           , PS_STAND_ALONE 
                           , PS_PROMO) 
                     VALUES (@AccountId
                           , @TerminalId
                           , 2            -- 2 = TYPE WIN
                           , NULL
                           , @InitialBalance
                           , @InitialBalance
                           , CASE WHEN (@activity_due_to_card_in = 1) THEN GETDATE() ELSE NULL END
                           , 0
                           , @Promo ) 
                         
  SET @playsession_id = SCOPE_IDENTITY()
   
  IF ( @playsession_id = 0 )
  BEGIN
      SET @status_code = 4
      SET @status_text = 'Access Denied'
    GOTO ERROR_PROCEDURE
  END
   
  -- RCI 21-NOV-2011: Check balance has not been changed.
  UPDATE  ACCOUNTS 
     SET  AC_CURRENT_TERMINAL_ID      = @TerminalId
        , AC_CURRENT_TERMINAL_NAME    = (SELECT TE_NAME FROM TERMINALS WHERE TE_TYPE = 1 AND TE_TERMINAL_ID = @TerminalId)
        , AC_CURRENT_PLAY_SESSION_ID  = @playsession_id 
   WHERE  AC_ACCOUNT_ID               = @AccountId 
     AND  AC_CURRENT_TERMINAL_ID     IS NULL 
     AND  AC_CURRENT_TERMINAL_NAME   IS NULL  
     AND  AC_CURRENT_PLAY_SESSION_ID IS NULL
     AND  AC_BALANCE                  = @InitialBalance
 
  SET @rc = @@ROWCOUNT
  IF ( @rc <> 1 )
  BEGIN
    SET @status_code = 4
    SET @status_text = 'Access Denied'
    GOTO ERROR_PROCEDURE
  END
   
 ERROR_PROCEDURE:
	SELECT @status_code AS StatusCode, @status_text AS StatusText, @playsession_id AS SessionID

END -- InsertPlaySession 

GO


/**** FINAL SECTION *****/
SELECT '3GS Update OK.';
