/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 324;

SET @New_ReleaseId = 325;
SET @New_ScriptName = N'UpdateTo_18.325.041.sql';
SET @New_Description = N'Pasivo en cuentas';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/**** GENERAL PARAM *****/

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Liabilities' AND GP_SUBJECT_KEY = 'Mode')
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES('Liabilities', 'Mode', '1')
GO


IF NOT EXISTS (SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='Cashier.Undo' AND GP_SUBJECT_KEY = 'BankCard.Undo.Mode')
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Cashier.Undo', 'BankCard.Undo.Mode', '0')
GO

IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'AFIPClient' AND GP_SUBJECT_KEY = 'ShowExtendedMeterType')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('AFIPClient', 'ShowExtendedMeterType', '7')
GO


IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='GamingTables' AND GP_SUBJECT_KEY = 'Dropbox.CollectionMode')
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('GamingTables', 'Dropbox.CollectionMode', '0')
GO


IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='Cashier' AND GP_SUBJECT_KEY = 'MaxScreenResolutionWidth')
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Cashier', 'MaxScreenResolutionWidth', '1280')
GO

IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='Cashier' AND GP_SUBJECT_KEY = 'MaxScreenResolutionHeigth')
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Cashier', 'MaxScreenResolutionHeigth', '800')
GO

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.Withdrawal' AND GP_SUBJECT_KEY = 'BankCard.Conciliate')
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES('Cashier.Withdrawal', 'BankCard.Conciliate', '0')
GO

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.CloseCash' AND GP_SUBJECT_KEY = 'BankCard.Conciliate')
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES('Cashier.CloseCash', 'BankCard.Conciliate', '0')
GO

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'GamingTables' AND GP_SUBJECT_KEY = 'Split.A.ChipsSalePct')
BEGIN  
  DECLARE @pct as NVARCHAR(50)
  
  SELECT @pct = GP_KEY_VALUE FROM GENERAL_PARAMS WHERE gp_group_key = 'Cashier' AND gp_subject_key = 'Split.A.CashInPct'
  
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES('GamingTables', 'Split.A.ChipsSalePct', @pct)
END
GO

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'GamingTables' AND GP_SUBJECT_KEY = 'Split.B.ChipsSalePct')
BEGIN
  DECLARE @pct as NVARCHAR(50)
  
  SELECT @pct = GP_KEY_VALUE FROM GENERAL_PARAMS WHERE gp_group_key = 'Cashier' AND gp_subject_key = 'Split.B.CashInPct'
  
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES('GamingTables', 'Split.B.ChipsSalePct', @pct)
  
END
GO

IF NOT EXISTS ( SELECT 1 FROM general_params WHERE gp_group_key = 'TITO' AND gp_subject_key = 'MICO.EnableDisableNoteAcceptor')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('TITO', 'MICO.EnableDisableNoteAcceptor', 0)
GO

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.Voucher' AND GP_SUBJECT_KEY = 'PaymentMethod.BankCard.HideVoucherRecharge')
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES('Cashier.Voucher', 'PaymentMethod.BankCard.HideVoucherRecharge', '0')
GO

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.PaymentMethod' AND GP_SUBJECT_KEY = 'BankCard.Recharge.Limit.Amount')
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES('Cashier.PaymentMethod', 'BankCard.Recharge.Limit.Amount', '0')
GO

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.PaymentMethod' AND GP_SUBJECT_KEY = 'BankCard.CashAdvance.Limit.Amount')
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES('Cashier.PaymentMethod', 'BankCard.CashAdvance.Limit.Amount', '0')
GO

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY = 'Cage.ShowButtonTotalizingStrip')
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES('Cashier', 'Cage.ShowButtonTotalizingStrip', '1')
GO

IF NOT EXISTS (SELECT * FROM [dbo].[general_params] WHERE gp_group_key LIKE 'GamingTables' AND gp_subject_key LIKE 'Chips.TypeNoRedeemable.Text')
      INSERT INTO [dbo].[general_params]
      VALUES ('GamingTables','Chips.TypeNoRedeemable.Text','No redimible') 
GO

IF NOT EXISTS (SELECT * FROM [dbo].[general_params] WHERE gp_group_key LIKE 'GamingTables' AND gp_subject_key LIKE 'Chips.MaxChipsForChipsSet')
      INSERT INTO [dbo].[general_params]
      VALUES ('GamingTables','Chips.MaxChipsForChipsSet','20') 
GO

IF NOT EXISTS (SELECT * FROM [dbo].[general_params] WHERE gp_group_key LIKE 'GamingTables' AND gp_subject_key LIKE 'Chips.MaxAllowedSales.Currency2')
      INSERT INTO [dbo].[general_params]
      VALUES ('GamingTables','Chips.MaxAllowedSales.Currency2','0') 
GO

IF NOT EXISTS (SELECT * FROM [dbo].[general_params] WHERE gp_group_key LIKE 'GamingTables' AND gp_subject_key LIKE 'Chips.MaxAllowedPurchases.Currency2')
      INSERT INTO [dbo].[general_params]
      VALUES ('GamingTables','Chips.MaxAllowedPurchases.Currency2','0') 
GO

IF NOT EXISTS(SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'GamingTables' AND GP_SUBJECT_KEY = 'Chips.MaxChipsSetsColor')
  INSERT INTO general_params (gp_group_key, gp_subject_key, gp_key_value) VALUES ('GamingTables', 'Chips.MaxChipsSetsColor', '1')
GO

IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PinPad' AND GP_SUBJECT_KEY = 'ControlNumber')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('PinPad', 'ControlNumber', '{0}')
GO

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PinPad' AND GP_SUBJECT_KEY ='Enabled')
  INSERT INTO [dbo].[general_params] ([gp_group_key], [gp_subject_key], [gp_key_value]) VALUES ('PinPad', 'Enabled', '0');                  
GO

IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PinPad' AND GP_SUBJECT_KEY = 'ThreadUndoTime')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('PinPad', 'ThreadUndoTime', '10000')
GO

IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PinPad' AND GP_SUBJECT_KEY = 'ThreadUndoMaxRetry')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('PinPad', 'ThreadUndoMaxRetry', '20')
GO

IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='Cashier' AND GP_SUBJECT_KEY = 'Menu.Timeout.Seconds')
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Cashier', 'Menu.Timeout.Seconds', '3')
GO

DECLARE @ChipsEnabled As BIT
SET @ChipsEnabled = 0

IF EXISTS ( SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'RegionalOptions' AND GP_SUBJECT_KEY = 'CurrenciesAccepted' AND gp_key_value like '%X01%')
  SET @ChipsEnabled = 1

IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='FeatureChips' AND GP_SUBJECT_KEY = 'ValueType.Enabled')
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('FeatureChips', 'ValueType.Enabled', @ChipsEnabled)
GO

IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='FeatureChips' AND GP_SUBJECT_KEY = 'NonRedeemableType.Enabled')
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('FeatureChips', 'NonRedeemableType.Enabled', '0')
GO

IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='FeatureChips' AND GP_SUBJECT_KEY = 'ColorType.Enabled')
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('FeatureChips', 'ColorType.Enabled', '0')
GO

IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='FeatureChips' AND GP_SUBJECT_KEY = 'ValueType.Name')
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('FeatureChips', 'ValueType.Name', 'Fichas redimibles')
GO

IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='FeatureChips' AND GP_SUBJECT_KEY = 'NonRedeemableType.Name')
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('FeatureChips', 'NonRedeemableType.Name', 'Fichas no redimibles')
GO

IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='FeatureChips' AND GP_SUBJECT_KEY = 'ColorType.Name')
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('FeatureChips', 'ColorType.Name', 'Fichas color')
GO

IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='FeatureChips' AND GP_SUBJECT_KEY = 'ValueType.NumMaxGroup')
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('FeatureChips', 'ValueType.NumMaxGroup', '1')
GO

IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='FeatureChips' AND GP_SUBJECT_KEY = 'NonRedeemableType.NumMaxGroup')
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('FeatureChips', 'NonRedeemableType.NumMaxGroup', '1')
GO

IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='FeatureChips' AND GP_SUBJECT_KEY = 'ColorType.NumMaxGroup')
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('FeatureChips', 'ColorType.NumMaxGroup', '1')
GO

IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='FeatureChips' AND GP_SUBJECT_KEY = 'MaxChipsForSet')
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('FeatureChips', 'MaxChipsForSet', '20')
GO

UPDATE GENERAL_PARAMS SET GP_KEY_VALUE = REPLACE(REPLACE(REPLACE(GP_KEY_VALUE, 'X01', ''),' ',''),';;',';') WHERE GP_GROUP_KEY = 'RegionalOptions' AND GP_SUBJECT_KEY = 'CurrenciesAccepted'
GO

UPDATE GENERAL_PARAMS SET GP_KEY_VALUE =RIGHT(GP_KEY_VALUE, LEN(gp_key_value)-1) WHERE GP_GROUP_KEY = 'RegionalOptions' AND GP_SUBJECT_KEY = 'CurrenciesAccepted' AND gp_key_value like ';%'
GO

UPDATE GENERAL_PARAMS SET GP_KEY_VALUE = LEFT(GP_KEY_VALUE, LEN(gp_key_value)-1) WHERE GP_GROUP_KEY = 'RegionalOptions' AND GP_SUBJECT_KEY = 'CurrenciesAccepted' AND gp_key_value like '%;'
GO

IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Reception' AND GP_SUBJECT_KEY ='OpenCashierAutomatically')
	INSERT INTO general_params (gp_group_key, gp_subject_key, gp_key_value) VALUES ('Reception', 'OpenCashierAutomatically', '2');
GO

IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Reception' AND GP_SUBJECT_KEY ='ResultsView.Collapsed.TopResults')
	INSERT INTO [dbo].[general_params] ([gp_group_key], [gp_subject_key], [gp_key_value]) VALUES ('Reception', 'ResultsView.Collapsed.TopResults', '4')
GO

IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Reception' AND GP_SUBJECT_KEY ='ResultsView.Extended.TopResults')
	INSERT INTO [dbo].[general_params] ([gp_group_key], [gp_subject_key], [gp_key_value]) VALUES ('Reception', 'ResultsView.Extended.TopResults', '50')
GO

IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Reception' AND GP_SUBJECT_KEY ='ResultsView.Mode')
	INSERT INTO [dbo].[general_params] ([gp_group_key], [gp_subject_key], [gp_key_value]) VALUES ('Reception', 'ResultsView.Mode', '1') 
GO
/******* TABLES  *******/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[buckets_multiplier_schedule]') AND type in (N'U'))
  DROP TABLE buckets_multiplier_schedule
GO

CREATE TABLE [dbo].[buckets_multiplier_schedule](
	[bm_bucket_id] [bigint] NOT NULL,
	[bm_bucket_description] [nvarchar](250) NOT NULL,
	[bm_bucket_afected_id] [bigint] NOT NULL,
	[bm_bucket_multiplier] [dec](3, 2) NOT NULL,
	[bm_bucket_terminals_list] [xml] NULL,
	[bm_bucket_schedule_start] [datetime] NOT NULL,
	[bm_bucket_schedule_end] [datetime] NULL,
	[bm_bucket_schedule_weekday] [int] NOT NULL,
	[bm_bucket_schedule1_time_from] [int] NOT NULL,
	[bm_bucket_schedule1_time_to] [int] NOT NULL,
	[bm_bucket_schedule2_time_from] [int] NULL,
	[bm_bucket_schedule2_time_to] [int] NULL,
	[bm_bucket_schedule2_enabled][bit] NOT NULL DEFAULT 0,
	[bm_bucket_order] [int] NULL,
	[bm_bucket_enabled] [bit] NOT NULL DEFAULT 0,
	[bm_bucket_master_sequence_id] [bigint] NULL,
	[bm_bucket_computed_order]  AS (case when isnull([BM_BUCKET_MASTER_SEQUENCE_ID],(0))=(0) then [BM_BUCKET_ORDER] else (1000000000)+[BM_BUCKET_ORDER] end),
 
 CONSTRAINT [PK_buckets_multiplier_schedule] PRIMARY KEY CLUSTERED 
(
	[bm_bucket_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[buckets_multiplier_to_apply]') AND type in (N'U'))
  DROP TABLE buckets_multiplier_to_apply
GO

CREATE TABLE [dbo].[buckets_multiplier_to_apply](
	[bma_Bucket_ID] [bigint] NOT NULL,
	[bma_Terminal_ID] [int] NOT NULL,
	[bma_Multiplier] [decimal](3, 2) NULL,
 CONSTRAINT [PK_buckets_multiplier_to_apply] PRIMARY KEY CLUSTERED 
(
	[bma_Bucket_ID],
	[bma_Terminal_ID]
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pinpad_transactions_reconciliation]') AND type in (N'U'))
	DROP TABLE [dbo].[pinpad_transactions_reconciliation]
GO

CREATE TABLE [dbo].[pinpad_transactions_reconciliation](
	[ptc_drawal] [datetime] NOT NULL,
	[ptc_user_id] [int] NOT NULL,
	[ptc_id] [bigint] NOT NULL,
	[ptc_reconciliate] [bit] NOT NULL,
  [ptc_drawal_status] [bit] DEFAULT (0),
 CONSTRAINT [PK_pinpad_transactions_reconciliation] PRIMARY KEY CLUSTERED 
(
	[ptc_drawal] ASC,
	[ptc_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[pinpad_transactions_reconciliation] ADD  CONSTRAINT [DF_pinpad_transactions_reconciliation_ptc_reconciliate]  DEFAULT ((0)) FOR [ptc_reconciliate]
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[chips_sets_chips]') AND type in (N'U'))
  CREATE TABLE chips_sets_chips(
	  csc_chip_id int NOT NULL,
	  csc_set_id int NOT NULL	  
   CONSTRAINT PK_chip_id_set_id PRIMARY KEY CLUSTERED 
  (
	  csc_chip_id ASC,
	  csc_set_id ASC	  
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
  ) ON [PRIMARY]

GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[chips]') and name = 'ch_chip_type')
   ALTER TABLE dbo.chips ADD ch_chip_type INT
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[chips_sets]') and name = 'chs_chip_type')
   ALTER TABLE dbo.chips_sets ADD chs_chip_type INT
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[play_sessions]') and name = 'ps_re_found_in_egm')
   ALTER TABLE play_sessions ADD [ps_re_found_in_egm] [money] NULL
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[play_sessions]') and name = 'ps_nr_found_in_egm')
   ALTER TABLE play_sessions ADD [ps_nr_found_in_egm] [money] NULL
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[play_sessions]') and name = 'ps_re_remaining_in_egm')
   ALTER TABLE play_sessions ADD [ps_re_remaining_in_egm] [money] NULL
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[play_sessions]') and name = 'ps_nr_remaining_in_egm')
   ALTER TABLE play_sessions ADD [ps_nr_remaining_in_egm] [money] NULL
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[play_sessions]') and name = 'ps_total_cash_in')
  ALTER TABLE play_sessions ADD [ps_total_cash_in]  AS (((((((([ps_initial_balance]+[ps_cash_in])+isnull([ps_re_ticket_in],(0)))+isnull([ps_promo_re_ticket_in],(0)))+isnull([ps_aux_ft_re_cash_in],(0)))+isnull([ps_promo_nr_ticket_in],(0)))+isnull([ps_aux_ft_nr_cash_in],(0)))+isnull([ps_re_found_in_egm],(0)))+isnull([ps_nr_found_in_egm],(0)))
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[play_sessions]') and name = 'ps_total_cash_out')
  ALTER TABLE play_sessions ADD [ps_total_cash_out]  AS (((((isnull([ps_final_balance],(0))+[ps_cash_out])+isnull([ps_re_ticket_out],(0)))+isnull([ps_promo_nr_ticket_out],(0)))+isnull([ps_re_remaining_in_egm],(0)))+isnull([ps_nr_remaining_in_egm],(0)))
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.pinpad_transactions') AND type in (N'U'))
	CREATE TABLE dbo.pinpad_transactions(
	  pt_id bigint NOT NULL IDENTITY (1, 1),
	  pt_transaction nvarchar(50) NOT NULL,
	  pt_user_id int NOT NULL,
	  pt_account_id bigint NOT NULL,
	  pt_created datetime NOT NULL,
	  pt_bank_name nvarchar(20) NOT NULL,
	  pt_card_number nvarchar(16) NOT NULL,
	  pt_card_type int NOT NULL,
	  pt_card_iso_code nvarchar(3) NOT NULL,
	  pt_card_holder nvarchar(50) NOT NULL,
	  pt_transaction_amount money NOT NULL,
	  pt_commission_amount money NOT NULL,
	  pt_total_amount money NOT NULL,
	  pt_status int NOT NULL,
	  pt_error_message nvarchar(200) NULL,
	  pt_control_number nvarchar(50) NULL,
	  pt_reference nvarchar(50) NULL,
	  pt_operation_id bigint NULL,
	  pt_merchant_id bigint NULL,
	  pt_auth_code nvarchar(10) NULL,
	  pt_collected BIT NOT NULL,
	  pt_last_modified datetime NOT NULL
	CONSTRAINT [PK_pt_id] PRIMARY KEY CLUSTERED 
	(
	  pt_id ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.pinpad_cashier_terminals') AND type in (N'U'))
	CREATE TABLE dbo.pinpad_cashier_terminals(
	  pct_id int NOT NULL IDENTITY (1, 1),
	  pct_cashier_id int NOT NULL,
	  pct_type int NOT NULL,
	  pct_port int NOT NULL,
	  pct_enabled BIT NOT NULL,
	  pct_last_modified datetime NOT NULL
	CONSTRAINT [PK_pct_id] PRIMARY KEY CLUSTERED 
	(
	  pct_id ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.UNDO_PIN_PAD_TRANSACTION') AND type in (N'U'))
	CREATE TABLE dbo.UNDO_PIN_PAD_TRANSACTION(
	  uppt_id int NOT NULL IDENTITY (1, 1),
	  uppt_pinpadmodel int NOT NULL,
	  uppt_control_number NVARCHAR(50) NULL,
	  uppt_terminal_id NVARCHAR(50) NOT NULL,
	  uppt_cancel_retries int NOT NULL,
	CONSTRAINT [PK_upp_id] PRIMARY KEY CLUSTERED 
	(
	  uppt_id ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]
GO

IF NOT EXISTS(SELECT * FROM sys.columns WHERE Name = N'ce_configuration' AND Object_ID = Object_ID(N'CURRENCY_EXCHANGE'))
BEGIN

      ALTER TABLE currency_exchange add ce_configuration xml NULL
      
END

GO
      UPDATE currency_exchange
         SET ce_configuration =
'<configuration>
      <data>
            <type>0</type>
            <scope>0</scope>
            <fixed_commission>'+ CAST(ce_fixed_commission as nvarchar)+'</fixed_commission>
            <variable_commission>' + CAST(ce_variable_commission as nvarchar)+ '</variable_commission>
            <fixed_nr2>'+CAST(ce_fixed_nr2 as nvarchar)+'</fixed_nr2>
            <variable_nr2>'+CAST(ce_variable_nr2 as nvarchar) +'</variable_nr2>
      </data>     
</configuration>
'
WHERE ce_configuration IS NULL


GO

IF NOT EXISTS( SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'buckets' AND COLUMN_NAME = 'bu_multiplier_enabled') 
BEGIN
	ALTER TABLE [dbo].[buckets] ADD [bu_multiplier_enabled]  BIT NOT NULL DEFAULT 0
END
GO

IF NOT EXISTS(SELECT * FROM sys.columns WHERE Name = N'ct_pinpad_enabled' AND Object_ID = Object_ID(N'cashier_terminals'))
	ALTER TABLE cashier_terminals add ct_pinpad_enabled bit NOT NULL DEFAULT 0
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[gaming_table_bet]') AND type in (N'U'))
BEGIN
    CREATE TABLE [dbo].[gaming_table_bet](
      [gtb_id] [int] IDENTITY(1,1) NOT NULL,
      [gtb_gaming_table_id] [int] NOT NULL,
      [gtb_iso_code] [nvarchar](5) NOT NULL,
      [gtb_min_bet] [MONEY] NOT NULL,
      [gtb_max_bet] [MONEY] NOT NULL,
      [gtb_status] [bit] NOT NULL,
     CONSTRAINT [PK_gaming_table_bet] PRIMARY KEY CLUSTERED 
    (
      [GTB_ID] ASC
    )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
    ) ON [PRIMARY]


    ALTER TABLE [dbo].[gaming_table_bet] ADD  CONSTRAINT [DF_gaming_table_bet_GTB_MAX_BET]  DEFAULT ((0)) FOR [gtb_max_bet]

    ALTER TABLE [dbo].[gaming_table_bet] ADD  CONSTRAINT [DF_gaming_table_bet_GTB_MIN_BET]  DEFAULT ((0)) FOR [gtb_min_bet]

    ALTER TABLE [dbo].[gaming_table_bet] ADD  CONSTRAINT [DF_gaming_table_bet_GTB_STATUS]  DEFAULT ((0)) FOR [gtb_status]

END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[gaming_table_configuration]') AND type in (N'U'))
BEGIN

    CREATE TABLE [dbo].[gaming_table_configuration](
      [gtc_id] [int] IDENTITY(1,1),
      [gtc_iso_code] [nvarchar](5) NULL,
      [gtc_max_allowed_purchase] [decimal](18, 0) NOT NULL,
      [gtc_max_allowed_sales] [decimal](18, 0) NOT NULL,
      [gtc_status] [bit] NOT NULL,
     CONSTRAINT [PK_gaming_table_configuration] PRIMARY KEY CLUSTERED 
    (
      [GTC_ID] ASC
    )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
    ) ON [PRIMARY]


    ALTER TABLE [dbo].[gaming_table_configuration] ADD  CONSTRAINT [DF_gaming_table_configuration_GTC_MAX_ALLOWED_PURCHASE]  DEFAULT ((0)) FOR [gtc_max_allowed_purchase]

    ALTER TABLE [dbo].[gaming_table_configuration] ADD  CONSTRAINT [DF_gaming_table_configuration_GTC_MAX_ALLOWED_SALES]  DEFAULT ((0)) FOR [gtc_max_allowed_sales]

    ALTER TABLE [dbo].[gaming_table_configuration] ADD  CONSTRAINT [DF_gaming_table_configuration_GTC_STATUS]  DEFAULT ((0)) FOR [gtc_status]
END
GO

IF NOT  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[gaming_tables_sessions_by_currency]') AND type in (N'U'))
BEGIN
  CREATE TABLE dbo.gaming_tables_sessions_by_currency
        (
        gtsc_gaming_table_session_id bigint NOT NULL,
        gtsc_iso_code nvarchar(3) NOT NULL,
        gtsc_type int NOT NULL,
        gtsc_initial_chips_amount money NOT NULL,
        gtsc_final_chips_amount money NOT NULL,
        gtsc_own_sales_amount money NOT NULL,
        gtsc_external_sales_amount money NOT NULL,   
        gtsc_collected_amount money NOT NULL, 
        gtsc_collected_amount_converted money NOT NULL,
        gtsc_own_purchase_amount money NOT NULL,
        gtsc_external_purchase_amount money NOT NULL,
        gtsc_tips money NOT NULL,
        gtsc_total_sales_amount money NOT NULL,
        gtsc_total_purchase_amount money NOT NULL,
        )  ON [PRIMARY]

  ALTER TABLE dbo.gaming_tables_sessions_by_currency ADD CONSTRAINT
        DF_gaming_tables_sessions_by_currency_gtsc_initial_chips_amount DEFAULT 0 FOR gtsc_initial_chips_amount

  ALTER TABLE dbo.gaming_tables_sessions_by_currency ADD CONSTRAINT
        DF_gaming_tables_sessions_by_currency_gtsc_final_chips_amount DEFAULT 0 FOR gtsc_final_chips_amount

  ALTER TABLE dbo.gaming_tables_sessions_by_currency ADD CONSTRAINT
        DF_gaming_tables_sessions_by_currency_gtsc_own_sales_amount DEFAULT 0 FOR gtsc_own_sales_amount

  ALTER TABLE dbo.gaming_tables_sessions_by_currency ADD CONSTRAINT
        DF_gaming_tables_sessions_by_currency_gtsc_external_sales_amount DEFAULT 0 FOR gtsc_external_sales_amount
        
   ALTER TABLE dbo.gaming_tables_sessions_by_currency ADD CONSTRAINT
        DF_gaming_tables_sessions_by_currency_gtsc_collected_amount DEFAULT 0 FOR gtsc_collected_amount       
        
   ALTER TABLE dbo.gaming_tables_sessions_by_currency ADD CONSTRAINT
        DF_gaming_tables_sessions_by_currency_gtsc_collected_amount_converted DEFAULT 0 FOR gtsc_collected_amount_converted  

  ALTER TABLE dbo.gaming_tables_sessions_by_currency ADD CONSTRAINT
        DF_gaming_tables_sessions_by_currency_gtsc_own_purchase_amount DEFAULT 0 FOR gtsc_own_purchase_amount
  
  ALTER TABLE dbo.gaming_tables_sessions_by_currency ADD CONSTRAINT
        DF_gaming_tables_sessions_by_currency_gtsc_external_purchase_amount DEFAULT 0 FOR gtsc_external_purchase_amount      
  
  ALTER TABLE dbo.gaming_tables_sessions_by_currency ADD CONSTRAINT
        DF_gaming_tables_sessions_by_currency_gtsc_tips DEFAULT 0 FOR gtsc_tips
  
  ALTER TABLE dbo.gaming_tables_sessions_by_currency ADD CONSTRAINT
        DF_gaming_tables_sessions_by_currency_gtsc_total_sales_amount DEFAULT 0 FOR gtsc_total_sales_amount      

  ALTER TABLE dbo.gaming_tables_sessions_by_currency ADD CONSTRAINT
        DF_gaming_tables_sessions_by_currency_gtsc_total_purchase_amount DEFAULT 0 FOR gtsc_total_purchase_amount

  ALTER TABLE dbo.gaming_tables_sessions_by_currency ADD CONSTRAINT
        PK_gaming_tables_sessions_by_currency PRIMARY KEY CLUSTERED 
        (
        gtsc_gaming_table_session_id,
        gtsc_iso_code,
        gtsc_type
        ) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

END
GO

IF NOT  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[closing_stocks]') AND type IN (N'U'))
BEGIN
  CREATE TABLE dbo.closing_stocks(cs_cashier_id bigint NOT NULL,
    cs_closing_stock_type int NOT NULL,
    cs_sleeps_on_table bit NOT NULL DEFAULT 0,
    cs_stock xml NULL
    CONSTRAINT [PK_closing_stocks] PRIMARY KEY CLUSTERED
    (
      cs_cashier_id ASC
    ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[gaming_tables]') and name = 'gt_dropbox_enabled')
	ALTER TABLE [dbo].[gaming_tables] ADD [gt_dropbox_enabled] [bit] NOT NULL DEFAULT 0;
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[gaming_tables_sessions]') and name = 'gts_pre_closing')
	ALTER TABLE [dbo].[gaming_tables_sessions] ADD [gts_pre_closing] [bit] NOT NULL DEFAULT 0;
GO
	
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[gaming_tables_sessions]') and name = 'gts_dropbox_enabled')
	ALTER TABLE [dbo].[gaming_tables_sessions] ADD [gts_dropbox_enabled] [bit] NOT NULL DEFAULT 0;
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[gaming_tables_sessions]') and name = 'gts_collected_dropbox_amount')
	ALTER TABLE [dbo].[gaming_tables_sessions] ADD [gts_collected_dropbox_amount] [money] NOT NULL DEFAULT 0;
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[gaming_tables_sessions]') and name = 'gts_collected_dropbox_chips_amount')
	ALTER TABLE [dbo].[gaming_tables_sessions] ADD [gts_collected_dropbox_chips_amount] [money] NOT NULL DEFAULT 0;
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[gaming_tables_sessions]') and name = 'gts_collected_dropbox_tickets_amount')
	ALTER TABLE [dbo].[gaming_tables_sessions] ADD [gts_collected_dropbox_tickets_amount] [money] NOT NULL DEFAULT 0;
GO

IF NOT EXISTS (SELECT * FROM SYS.COLUMNS WHERE OBJECT_ID = OBJECT_ID(N'[DBO].[GT_PLAY_SESSIONS]') AND NAME = 'gtps_iso_code')
      ALTER TABLE [DBO].[GT_PLAY_SESSIONS] ADD gtps_iso_code [NVARCHAR](3) NULL
GO

IF NOT EXISTS (SELECT * FROM SYS.COLUMNS WHERE OBJECT_ID = OBJECT_ID(N'[DBO].[GT_PLAYERTRACKING_MOVEMENTS]') AND NAME = 'gtpm_iso_code')
      ALTER TABLE [DBO].[GT_PLAYERTRACKING_MOVEMENTS] ADD gtpm_iso_code [NVARCHAR](3) NULL
GO

DECLARE @CurrencyISOCode AS NVARCHAR(3)
SELECT @CurrencyISOCode = GP_KEY_VALUE  FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'RegionalOptions' AND GP_SUBJECT_KEY = 'CurrencyISOCode'

IF EXISTS (SELECT * FROM SYS.COLUMNS WHERE OBJECT_ID = OBJECT_ID(N'[DBO].[GT_PLAY_SESSIONS]') AND NAME = 'gtps_iso_code') AND EXISTS (SELECT COUNT(1) FROM [DBO].[GT_PLAY_SESSIONS] WHERE gtps_iso_code IS NULL)
BEGIN
      UPDATE [DBO].[GT_PLAY_SESSIONS] SET gtps_iso_code = @CurrencyISOCode WHERE gtps_iso_code IS NULL
      ALTER TABLE [DBO].[GT_PLAY_SESSIONS] ALTER COLUMN gtps_iso_code [NVARCHAR](3) NOT NULL
	  
	  UPDATE gt_playertracking_movements SET gtpm_iso_code = @CurrencyISOCode WHERE gtpm_type IN (24, 25, 26, 27) AND gtpm_iso_code IS NULL
END
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[gaming_tables_sessions_by_currency]') and name = 'gtsc_collected_dropbox_amount')
	ALTER TABLE [dbo].[gaming_tables_sessions_by_currency] ADD [gtsc_collected_dropbox_amount] [money] NOT NULL DEFAULT 0;
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[gaming_tables_sessions_by_currency]') and name = 'gtsc_collected_dropbox_amount_converted')
	ALTER TABLE [dbo].[gaming_tables_sessions_by_currency] ADD [gtsc_collected_dropbox_amount_converted] [money] NOT NULL DEFAULT 0;
	
IF EXISTS (SELECT * FROM SYS.COLUMNS WHERE OBJECT_ID = OBJECT_ID(N'[DBO].[CASHIER_MOVEMENTS]') AND NAME = 'CM_CAGE_CURRENCY_TYPE') AND NOT EXISTS (SELECT * FROM SYS.DEFAULT_CONSTRAINTS WHERE NAME = 'DF_cm_cage_currency_type')
BEGIN
    ALTER TABLE  [dbo].[cashier_movements] ADD CONSTRAINT DF_cm_cage_currency_type DEFAULT 0 FOR cm_cage_currency_type
    UPDATE [dbo].[cashier_movements] SET cm_cage_currency_type = 0 WHERE cm_cage_currency_type IS NULL
    ALTER TABLE  [dbo].[cashier_movements] ALTER COLUMN cm_cage_currency_type [int] NOT NULL 
END
GO

IF NOT EXISTS (SELECT * FROM SYS.COLUMNS WHERE OBJECT_ID = OBJECT_ID(N'[DBO].[CASHIER_MOVEMENTS_GROUPED_BY_HOUR]') AND NAME = 'CM_CAGE_CURRENCY_TYPE')
      ALTER TABLE [DBO].[CASHIER_MOVEMENTS_GROUPED_BY_HOUR] ADD CM_CAGE_CURRENCY_TYPE INT NOT NULL DEFAULT 0
GO 

IF  EXISTS (SELECT * FROM SYS.INDEXES WHERE OBJECT_ID = OBJECT_ID(N'[DBO].[CASHIER_MOVEMENTS_GROUPED_BY_HOUR]') AND NAME = N'PK_dbo.cashier_movements_grouped_by_hour')
  ALTER TABLE [DBO].[CASHIER_MOVEMENTS_GROUPED_BY_HOUR] DROP CONSTRAINT [PK_dbo.cashier_movements_grouped_by_hour]
GO

IF  EXISTS (SELECT * FROM SYS.INDEXES WHERE OBJECT_ID = OBJECT_ID(N'[DBO].[CASHIER_MOVEMENTS_GROUPED_BY_HOUR]') AND NAME = N'PK_CASHIER_MOVEMENTS_GROUPED_BY_HOUR')
  ALTER TABLE [DBO].[CASHIER_MOVEMENTS_GROUPED_BY_HOUR] DROP CONSTRAINT [PK_CASHIER_MOVEMENTS_GROUPED_BY_HOUR]
GO

ALTER TABLE [DBO].[CASHIER_MOVEMENTS_GROUPED_BY_HOUR] ADD  CONSTRAINT [PK_CASHIER_MOVEMENTS_GROUPED_BY_HOUR] PRIMARY KEY CLUSTERED 
(
  [CM_DATE] ASC,
  [CM_TYPE] ASC,
  [CM_SUB_TYPE] ASC,
  [CM_CURRENCY_ISO_CODE] ASC,
  [CM_CAGE_CURRENCY_TYPE] ASC,
  [CM_CURRENCY_DENOMINATION] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

IF NOT EXISTS (SELECT * FROM SYS.COLUMNS WHERE OBJECT_ID = OBJECT_ID(N'[DBO].[CASHIER_MOVEMENTS_GROUPED_BY_SESSION_ID]') AND NAME = 'CM_CAGE_CURRENCY_TYPE')
  ALTER TABLE [DBO].[CASHIER_MOVEMENTS_GROUPED_BY_SESSION_ID] ADD CM_CAGE_CURRENCY_TYPE INT NOT NULL DEFAULT 0
GO 

IF  EXISTS (SELECT * FROM SYS.INDEXES WHERE OBJECT_ID = OBJECT_ID(N'[DBO].[CASHIER_MOVEMENTS_GROUPED_BY_SESSION_ID]') AND NAME = N'PK_dbo.cashier_movements_grouped_by_session_id')
  ALTER TABLE [DBO].[CASHIER_MOVEMENTS_GROUPED_BY_SESSION_ID] DROP CONSTRAINT [PK_dbo.cashier_movements_grouped_by_session_id]
GO
IF  EXISTS (SELECT * FROM SYS.INDEXES WHERE OBJECT_ID = OBJECT_ID(N'[DBO].[CASHIER_MOVEMENTS_GROUPED_BY_SESSION_ID]') AND NAME = N'PK_CASHIER_MOVEMENTS_GROUPED_BY_SESSION_ID')
  ALTER TABLE [DBO].[CASHIER_MOVEMENTS_GROUPED_BY_SESSION_ID] DROP CONSTRAINT [PK_CASHIER_MOVEMENTS_GROUPED_BY_SESSION_ID]
GO

ALTER TABLE [DBO].[CASHIER_MOVEMENTS_GROUPED_BY_SESSION_ID] ADD  CONSTRAINT [PK_CASHIER_MOVEMENTS_GROUPED_BY_SESSION_ID] PRIMARY KEY CLUSTERED 
(
  [CM_SESSION_ID] ASC,
  [CM_TYPE] ASC,
  [CM_SUB_TYPE] ASC,
  [CM_CURRENCY_ISO_CODE] ASC,
  [CM_CAGE_CURRENCY_TYPE] ASC,
  [CM_CURRENCY_DENOMINATION] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

IF NOT EXISTS (SELECT * FROM SYS.COLUMNS WHERE OBJECT_ID = OBJECT_ID(N'[DBO].[CAGE_METERS]') AND NAME = 'CM_CAGE_CURRENCY_TYPE')
      ALTER TABLE [DBO].[CAGE_METERS] ADD cm_cage_currency_type INT NOT NULL DEFAULT 0
GO 

IF  EXISTS (SELECT * FROM SYS.INDEXES WHERE OBJECT_ID = OBJECT_ID(N'[DBO].[CAGE_METERS]') AND NAME = N'PK_cage_meters')
  ALTER TABLE [DBO].[CAGE_METERS] DROP CONSTRAINT [PK_cage_meters]
GO

ALTER TABLE [DBO].[CAGE_METERS] ADD  CONSTRAINT [PK_cage_meters] PRIMARY KEY CLUSTERED 
(
  [cm_source_target_id] ASC,
  [cm_concept_id] ASC,
  [cm_iso_code] ASC,
  [cm_cage_currency_type] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

IF NOT EXISTS (SELECT * FROM SYS.COLUMNS WHERE OBJECT_ID = OBJECT_ID(N'[DBO].[CAGE_SESSION_METERS]') AND NAME = 'CSM_CAGE_CURRENCY_TYPE')
  ALTER TABLE [DBO].[CAGE_SESSION_METERS] ADD csm_cage_currency_type INT NOT NULL DEFAULT 0
GO 

IF  EXISTS (SELECT * FROM SYS.INDEXES WHERE OBJECT_ID = OBJECT_ID(N'[DBO].[CAGE_SESSION_METERS]') AND NAME = N'PK_cage_session_meters')
  ALTER TABLE [DBO].[CAGE_SESSION_METERS] DROP CONSTRAINT [PK_cage_session_meters]
GO

ALTER TABLE [DBO].[CAGE_SESSION_METERS] ADD  CONSTRAINT [PK_cage_session_meters] PRIMARY KEY CLUSTERED 
(
  [CSM_CAGE_SESSION_ID] ASC,
  [CSM_SOURCE_TARGET_ID] ASC,
  [CSM_CONCEPT_ID] ASC,
  [CSM_ISO_CODE] ASC,
  [CSM_CAGE_CURRENCY_TYPE] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

IF NOT EXISTS (SELECT * FROM SYS.COLUMNS WHERE OBJECT_ID = OBJECT_ID(N'[DBO].[cage_concept_movement_detail]') AND NAME = 'CCMD_CAGE_CURRENCY_TYPE')
   ALTER TABLE [DBO].[cage_concept_movement_detail] ADD ccmd_cage_currency_type INT NOT NULL DEFAULT 0
GO 

IF EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[chips]') and name = 'ch_chip_set_id')
   ALTER TABLE dbo.chips DROP COLUMN  ch_chip_set_id
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[chips]') and name = 'ch_chip_type')
   ALTER TABLE dbo.chips ADD ch_chip_type INT
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[chips_sets]') and name = 'chs_chip_type')
    ALTER TABLE dbo.chips_sets ADD chs_chip_type INT
GO

IF EXISTS(SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[chips_sets]') and name = 'chs_chip_type' AND system_type_id <> 56)
	ALTER TABLE dbo.chips_sets ALTER COLUMN chs_chip_type INT
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[chips_sets]') and name = 'chs_allowed_operations_flags')
	ALTER TABLE [dbo].[chips_sets] ADD [chs_allowed_operations_flags] [int] NOT NULL DEFAULT 0;
GO

IF NOT EXISTS( SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'GAMING_TABLES' AND COLUMN_NAME = 'GT_BET_MIN_CURR_2')
   ALTER TABLE GAMING_TABLES ADD gt_bet_min_curr_2 MONEY NULL;
GO
   
IF NOT EXISTS( SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'GAMING_TABLES' AND COLUMN_NAME = 'GT_BET_MAX_CURR_2')
   ALTER TABLE GAMING_TABLES ADD gt_bet_max_curr_2 MONEY NULL;
GO

IF NOT EXISTS(SELECT * FROM sys.objects WHERE object_id  = OBJECT_ID('ONLINE_PLAYER_TRACKING') AND type IN ('U'))
  CREATE TABLE online_player_tracking
  (
		opt_type        INT NOT NULL,
		opt_account_id  BIGINT NOT NULL,
		opt_first_seen  DATETIME NOT NULL,
		opt_last_seen   DATETIME NULL,       
		opt_data        NVARCHAR(200) NULL,
		CONSTRAINT pk_online_player_tracking PRIMARY KEY (opt_type, opt_account_id)
  )     
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID('report_tool_config') AND type in ('U'))

  CREATE TABLE report_tool_config(
    rtc_report_tool_id INTEGER IDENTITY(1,1) PRIMARY KEY,
    rtc_form_id        INTEGER NOT NULL,
    rtc_location_menu  INTEGER NOT NULL,
    rtc_report_name    XML NOT NULL,
    rtc_store_name     NVARCHAR(200) NOT NULL, 
    rtc_design_filter  XML NOT NULL,
    rtc_design_sheets  XML NOT NULL,
    rtc_mailing        INTEGER NOT NULL,
    rtc_status         INTEGER NOT NULL   
  )
 GO

 IF EXISTS ( SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'buckets_multiplier_schedule' AND COLUMN_NAME =  'bm_bucket_multiplier') 
BEGIN
	ALTER TABLE [dbo].[buckets_multiplier_schedule]
		ALTER COLUMN bm_bucket_multiplier [decimal](4, 2) NOT NULL
END
GO

IF EXISTS( SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'buckets_multiplier_to_apply' AND COLUMN_NAME = 'bma_Multiplier') 
BEGIN
	ALTER TABLE [dbo].[buckets_Multiplier_To_Apply]
		ALTER COLUMN bma_Multiplier [decimal](4, 2) NOT NULL
END
GO

/******* INDEXES *******/

/******* RECORDS *******/

UPDATE dbo.buckets SET bu_multiplier_enabled = 1 WHERE bu_bucket_id = 1


IF EXISTS (SELECT 1 FROM CHIPS WHERE CH_ISO_CODE = 'X01') OR EXISTS (SELECT 1 FROM CHIPS_SETS WHERE CHS_ISO_CODE = 'X01')
BEGIN
   INSERT INTO CHIPS_SETS_CHIPS ( CSC_CHIP_ID, CSC_SET_ID)
                         SELECT   CH_CHIP_ID,  CHS_CHIP_SET_ID         
                           FROM   CHIPS    
                           LEFT   JOIN CHIPS_SETS ON CH_ISO_CODE = CHS_ISO_CODE 
                          WHERE   CH_ISO_CODE = 'X01'
                            AND   NOT EXISTS (SELECT 1 FROM CHIPS_SETS_CHIPS WHERE CSC_CHIP_ID = CH_CHIP_ID)

  UPDATE   CHIPS 
     SET   CH_ISO_CODE = (SELECT GP_KEY_VALUE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'RegionalOptions' AND GP_SUBJECT_KEY = 'CurrencyISOCode')
         , CH_CHIP_TYPE = 1001
   WHERE   CH_ISO_CODE = 'X01'
   
  UPDATE   CHIPS_SETS 
     SET   CHS_ISO_CODE = (SELECT GP_KEY_VALUE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'RegionalOptions' AND GP_SUBJECT_KEY = 'CurrencyISOCode')
         , CHS_CHIP_TYPE = 1001
         , CHS_ALLOWED_OPERATIONS_FLAGS = 1
   WHERE   CHS_ISO_CODE = 'X01'                
END

IF NOT EXISTS (SELECT [alcg_alarm_code] from [dbo].[alarm_catalog] WHERE [alcg_alarm_code] = 393268 AND [alcg_language_id] = 10)
  INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES ( 393268, 10, 0, N'Denominación contable cambiada', N'Denominación contable cambiada', 1)
GO

IF NOT EXISTS (SELECT [alcg_alarm_code] from [dbo].[alarm_catalog] WHERE [alcg_alarm_code] = 393268 AND [alcg_language_id] = 9)
  INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES ( 393268,  9, 0, N'Accounting denomination changed', N'Accounting denomination changed', 1)
GO

IF NOT EXISTS (SELECT [alcc_alarm_code] from [dbo].[alarm_catalog_per_category] WHERE [alcc_alarm_code] = 393268 AND [alcc_category] = 18) 
  INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES ( 393268, 18, 0, GETDATE() )
GO

IF NOT EXISTS (SELECT 1 FROM COUNTRIES WHERE co_iso2 = 'XK')
BEGIN
	INSERT INTO COUNTRIES VALUES(252,10,'KOSOVO','KOSOVO','XK')
	INSERT INTO COUNTRIES VALUES(252,9,'KOSOVO','KOSOVO','XK')
END

GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[gaming_tables_sessions]') and name = 'gts_fills_chips_amount')  
BEGIN 
  EXECUTE sp_rename N'dbo.gaming_tables_sessions.gts_initial_chips_amount', N'gts_fills_chips_amount', 'COLUMN' 
END
GO
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[gaming_tables_sessions]') and name = 'gts_credits_chips_amount')  
BEGIN 
  EXECUTE sp_rename N'dbo.gaming_tables_sessions.gts_final_chips_amount', N'gts_credits_chips_amount', 'COLUMN' 
END
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[gaming_tables_sessions_by_currency]') and name = 'gtsc_fills_chips_amount')  
BEGIN 
  EXECUTE sp_rename N'dbo.gaming_tables_sessions_by_currency.gtsc_initial_chips_amount', N'gtsc_fills_chips_amount', 'COLUMN' 
END
GO
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[gaming_tables_sessions_by_currency]') and name = 'gtsc_credits_chips_amount')  
BEGIN 
  EXECUTE sp_rename N'dbo.gaming_tables_sessions_by_currency.gtsc_final_chips_amount', N'gtsc_credits_chips_amount', 'COLUMN' 
END
GO

IF NOT EXISTS (SELECT * FROM SYS.COLUMNS WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[gaming_tables_sessions]') AND NAME = 'gts_initial_chips_amount')
BEGIN 
	ALTER TABLE [dbo].[gaming_tables_sessions] ADD [gts_initial_chips_amount] [money] NOT NULL DEFAULT 0
END
GO
IF NOT EXISTS (SELECT * FROM SYS.COLUMNS WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[gaming_tables_sessions]') AND NAME = 'gts_final_chips_amount')
BEGIN 
	ALTER TABLE [dbo].[gaming_tables_sessions] ADD [gts_final_chips_amount] [money] NOT NULL DEFAULT 0
END
GO

IF NOT EXISTS (SELECT * FROM SYS.COLUMNS WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[gaming_tables_sessions_by_currency]') AND NAME = 'gtsc_initial_chips_amount')
BEGIN 
	ALTER TABLE [dbo].[gaming_tables_sessions_by_currency] ADD [gtsc_initial_chips_amount] [money] NOT NULL DEFAULT 0
END
GO
IF NOT EXISTS (SELECT * FROM SYS.COLUMNS WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[gaming_tables_sessions_by_currency]') AND NAME = 'gtsc_final_chips_amount')
BEGIN 
	ALTER TABLE [dbo].[gaming_tables_sessions_by_currency] ADD [gtsc_final_chips_amount] [money] NOT NULL DEFAULT 0
END
GO

/******* PROCEDURES *******/

-- Clean older functions if exists

IF OBJECT_ID (N'dbo.SplitStringIntoTable', N'TF') IS NOT NULL
    DROP FUNCTION DBO.SplitStringIntoTable;
GO

IF OBJECT_ID (N'dbo.GT_Calculate_DROP', N'FN') IS NOT NULL
    DROP FUNCTION dbo.GT_Calculate_DROP;
GO

IF OBJECT_ID (N'dbo.GT_Calculate_DROP_GAMBLING_TABLES', N'FN') IS NOT NULL
    DROP FUNCTION dbo.GT_Calculate_DROP_GAMBLING_TABLES;
GO

IF OBJECT_ID (N'dbo.GT_Calculate_DROP_CASHIER', N'FN') IS NOT NULL
    DROP FUNCTION dbo.GT_Calculate_DROP_CASHIER;
GO

IF OBJECT_ID (N'dbo.GT_Calculate_WIN', N'FN') IS NOT NULL
    DROP FUNCTION dbo.GT_Calculate_WIN;
GO

IF OBJECT_ID (N'dbo.ApplyExchange', N'FN') IS NOT NULL
    DROP FUNCTION DBO.ApplyExchange;
GO

IF OBJECT_ID (N'dbo.GT_Base_Report_Data', N'P') IS NOT NULL
    DROP PROCEDURE dbo.GT_Base_Report_Data;
GO

IF OBJECT_ID (N'dbo.GT_Base_Report_Data_Player_Tracking', N'P') IS NOT NULL
    DROP PROCEDURE dbo.GT_Base_Report_Data_Player_Tracking;
GO

IF OBJECT_ID (N'dbo.GT_Chips_Operations', N'P') IS NOT NULL
    DROP PROCEDURE dbo.GT_Chips_Operations;
GO

IF OBJECT_ID (N'dbo.GT_Session_Information', N'P') IS NOT NULL
    DROP PROCEDURE dbo.GT_Session_Information;
GO

IF OBJECT_ID (N'dbo.GT_Cancellations', N'P') IS NOT NULL
    DROP PROCEDURE dbo.GT_Cancellations;
GO

IF OBJECT_ID (N'dbo.GT_Get_Drop', N'P') IS NOT NULL
    DROP PROCEDURE dbo.GT_Get_Drop;
GO


CREATE FUNCTION ApplyExchange
(
  @pAmount        MONEY,
  @pIsoCode       VARCHAR(3),
  @pMovementDate  DATETIME
)
RETURNS MONEY
AS
BEGIN
      DECLARE @Exchanged      MONEY
      DECLARE @Change         MONEY
      DECLARE @NumDecimals    INT

      IF(@pIsoCode IS NULL)
            SET @Exchanged = @pAmount;
      ELSE
      BEGIN
          SELECT TOP 1
                  @Change = CEA_NEW_CHANGE,
                  @NumDecimals = CEA_NEW_NUM_DECIMALS
            FROM  CURRENCY_EXCHANGE_AUDIT WITH (INDEX(IX_CEA_ISO_CODE_TYPE_DATETIME))
           WHERE  CEA_TYPE = 0
             AND  CEA_CURRENCY_ISO_CODE = @pIsoCode
             AND  CEA_DATETIME <= @pMovementDate
           ORDER  BY CEA_DATETIME DESC

            SET @NumDecimals = ISNULL(@NumDecimals, 0)
            SET @Exchanged = ROUND(ISNULL(@Change * @pAmount, 0), @NumDecimals, 1)
      END

      RETURN @Exchanged
END -- ApplyExchange
GO

GRANT EXECUTE ON [dbo].[ApplyExchange] TO [wggui]
GO

CREATE FUNCTION SplitStringIntoTable
(
    @pString VARCHAR(4096),
    @pDelimiter VARCHAR(10),
    @pTrimItems BIT = 1
)
RETURNS
@ReturnTable TABLE
(
    [SST_ID] [INT] IDENTITY(1,1) NOT NULL,
    [SST_VALUE] [VARCHAR](50) NULL
)
AS
BEGIN
        DECLARE @_ISPACES INT
        DECLARE @_PART VARCHAR(50)

        -- INITIALIZE SPACES
        SELECT @_ISPACES = CHARINDEX(@pDelimiter,@pString,0)

        WHILE @_ISPACES > 0
        BEGIN
            SELECT @_PART = SUBSTRING(@pString,0,CHARINDEX(@pDelimiter,@pString,0))

            IF @pTrimItems = 1
             SET @_PART = LTRIM(RTRIM(@_PART))

            INSERT INTO @ReturnTable(SST_VALUE)
            SELECT @_PART

            SELECT @pString = SUBSTRING(@pString, @_ISPACES + LEN(@pDelimiter), LEN(@pString) - CHARINDEX(@pDelimiter, @pString, 0))

            SELECT @_ISPACES = CHARINDEX(@pDelimiter,@pString,0)
        END

        IF LEN(@pString) > 0
            INSERT INTO @ReturnTable
            SELECT CASE WHEN @pTrimItems = 1 THEN LTRIM(RTRIM(@pString)) ELSE @pString END

    RETURN
END
GO

CREATE FUNCTION GT_Calculate_DROP
(
    @pOwnSalesAmount        AS MONEY,
    @pExternalSalesAmount   AS MONEY,
    @pCollectedAmount       AS MONEY,
    @pIsIntegratedCashier   AS BIT
)
RETURNS MONEY
BEGIN
  DECLARE @drop as Money
    IF @pIsIntegratedCashier = 1
      SET @drop = @pOwnSalesAmount + @pExternalSalesAmount
    ELSE
     SET @drop = @pOwnSalesAmount + @pExternalSalesAmount + @pCollectedAmount

    IF @drop < 0
      SET @drop = 0

    RETURN @drop
END
GO

-- PERMISSIONS  GT_Calculate_DROP
GRANT EXECUTE ON [dbo].[GT_Calculate_DROP] TO [wggui] WITH GRANT OPTION
GO


CREATE FUNCTION [dbo].[GT_Calculate_DROP_GAMBLING_TABLES] ( @pCollectedAmount AS MONEY, @pCollectedChipsAmount as MONEY, @pCollectedTicketsAmount as MONEY, @pIsIntegratedCashier AS BIT, @pIsDropBoxEnabled as BIT ) RETURNS MONEY
  BEGIN
  DECLARE @drop as Money
    IF @pIsDropBoxEnabled = 1
      SET @drop = @pCollectedAmount + @pCollectedChipsAmount + @pCollectedTicketsAmount
    ELSE
      IF @pIsIntegratedCashier = 0
       SET @drop = @pCollectedAmount

     IF @drop < 0
      set @drop = 0

   RETURN @drop
  END
GO

-- PERMISSIONS  GT_Calculate_DROP_GAMBLING_TABLES
GRANT EXECUTE ON [dbo].[GT_Calculate_DROP_GAMBLING_TABLES] TO [wggui] WITH GRANT OPTION
GO


CREATE FUNCTION [dbo].[GT_Calculate_DROP_CASHIER] ( @pOwnSalesAmount AS MONEY, @pExternalSalesAmount as MONEY) RETURNS MONEY
  BEGIN
  DECLARE @drop as Money
      SET @drop = @pOwnSalesAmount + @pExternalSalesAmount

   IF @drop < 0
     set @drop = 0

   RETURN @drop
  END
GO

-- PERMISSIONS  GT_Calculate_DROP_CASHIER
GRANT EXECUTE ON [dbo].[GT_Calculate_DROP_CASHIER] TO [wggui] WITH GRANT OPTION
GO

CREATE FUNCTION [dbo].[GT_Calculate_WIN] ( @pFinalAmount AS MONEY, @pInitialAmount AS MONEY, @pCollectedAmount AS MONEY, @pTipsAmount AS MONEY ) RETURNS MONEY
  BEGIN

    RETURN (@pFinalAmount - @pInitialAmount) + @pCollectedAmount - @pTipsAmount
  END
GO

-- END FUNCTIONS

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GT_Get_Drop]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[GT_Get_Drop]
GO

CREATE PROCEDURE [dbo].[GT_Get_Drop]
 (
    @pGamblingTableId   BIGINT
  , @pDateFrom          DATETIME
  , @pDateTo            DATETIME
 )
AS
BEGIN

----------------------------------------------------------------------------------------------------------------
DECLARE @_GAMBLING_TABLE_ID  AS   BIGINT
DECLARE @_DATE_FROM          AS   DATETIME
DECLARE @_DATE_TO            AS   DATETIME

-- CASHIER MOVEMENTS
DECLARE @_CHIPS_MOVEMENT_SALES_TOTAL    AS   INTEGER

----------------------------------------------------------------------------------------------------------------

-- Initialization --

SET @_GAMBLING_TABLE_ID             =   @pGamblingTableId
SET @_DATE_FROM                     =   @pDateFrom
SET @_DATE_TO                       =   @pDateTo
SET @_CHIPS_MOVEMENT_SALES_TOTAL    =   303

----------------------------------------------------------------------------------------------------------------
-- CHECK DATE PARAMETERS
IF @_DATE_FROM IS NULL
BEGIN
   -- IF DATE FROM IS NULL, USER FIRST POSIBLE DATE
   SET @_DATE_FROM = CAST('' AS DATETIME)
END

IF @_DATE_TO IS NULL
BEGIN
   -- IF DATE TO IS NULL, QUERY UNTIL CURRENT DATE
   SET @_DATE_TO = CAST(GETDATE() AS DATETIME)
END
----------------------------------------------------------------------------------------------------------------

IF @_GAMBLING_TABLE_ID IS NOT NULL
  BEGIN
    -- DROP BY GAMING TABLE
    SELECT   @_GAMBLING_TABLE_ID  AS GAMBLING_TABLE_ID
           , CM_GAMING_TABLE_SESSION_ID AS GAMBLING_TABLE_SESSION_ID
           , CM_CASHIER_ID AS CASHIER_ID
           , CM_SESSION_ID AS CASHIER_SESSION_ID
           , CM_DATE AS DATE_DROP
            , CV_VOUCHER_ID AS VOUCHER_ID
           , CM_OPERATION_ID AS OPERATION_ID
           , CM_ACCOUNT_ID AS ACCOUNT_ID
           , CM_SUB_AMOUNT AS DROP_AMOUNT
      FROM   CASHIER_MOVEMENTS
      LEFT JOIN CASHIER_VOUCHERS ON CASHIER_VOUCHERS.CV_OPERATION_ID = CASHIER_MOVEMENTS.CM_OPERATION_ID AND CASHIER_VOUCHERS.cv_type = 8   --(enum CashierVoucherType.ChipsSale = 8)
     WHERE   CM_TYPE = @_CHIPS_MOVEMENT_SALES_TOTAL
       AND  (CM_GAMING_TABLE_SESSION_ID IN (SELECT   GTS_GAMING_TABLE_SESSION_ID
                                              FROM   GAMING_TABLES_SESSIONS
                                             WHERE   GTS_GAMING_TABLE_ID IN (@_GAMBLING_TABLE_ID))
                       OR CM_SESSION_ID IN (SELECT   GTS_CASHIER_SESSION_ID
                                              FROM   GAMING_TABLES_SESSIONS
                                             WHERE   GTS_GAMING_TABLE_ID IN (@_GAMBLING_TABLE_ID))
            )
       AND   CM_DATE >= @_DATE_FROM
       AND   CM_DATE <= @_DATE_TO
  END

ELSE
  BEGIN
    SELECT   ISNULL((SELECT   GTS_GAMING_TABLE_ID
                       FROM   GAMING_TABLES_SESSIONS
                      WHERE   GTS_GAMING_TABLE_SESSION_ID = CM_GAMING_TABLE_SESSION_ID )
           , (SELECT   GTS_GAMING_TABLE_ID
                FROM   GAMING_TABLES_SESSIONS
               WHERE   GTS_CASHIER_SESSION_ID = CM_SESSION_ID ))  AS  GAMBLING_TABLE_ID
           , CM_GAMING_TABLE_SESSION_ID AS GAMBLING_TABLE_SESSION_ID
           , CM_CASHIER_ID AS CASHIER_ID
           , CM_SESSION_ID AS CASHIER_SESSION_ID
           , CM_DATE AS DATE_DROP
           , CV_VOUCHER_ID AS VOUCHER_ID
           , CM_OPERATION_ID AS OPERATION_ID
           , CM_ACCOUNT_ID AS ACCOUNT_ID
           , CM_SUB_AMOUNT AS DROP_AMOUNT
      FROM   CASHIER_MOVEMENTS
      LEFT JOIN CASHIER_VOUCHERS ON CASHIER_VOUCHERS.CV_OPERATION_ID = CASHIER_MOVEMENTS.CM_OPERATION_ID AND CASHIER_VOUCHERS.cv_type = 8
     WHERE   CM_TYPE = @_CHIPS_MOVEMENT_SALES_TOTAL
       AND   CM_DATE >= @_DATE_FROM
       AND   CM_DATE <= @_DATE_TO
  END

END -- END PROCEDURE
GO

-- PERMISSIONS
GRANT EXECUTE ON [dbo].[GT_Get_Drop] TO [wggui] WITH GRANT OPTION
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GT_Base_Report_Data_Player_Tracking]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[GT_Base_Report_Data_Player_Tracking]
GO

CREATE PROCEDURE [dbo].[GT_Base_Report_Data_Player_Tracking]
(
    @pBaseType            INTEGER
  , @pTimeInterval        INTEGER
  , @pDateFrom            DATETIME
  , @pDateTo              DATETIME
  , @pOnlyActivity        INTEGER
  , @pOrderBy             INTEGER
  , @pValidTypes          VARCHAR(4096)
  , @pSelectedCurrency    NVARCHAR(3)
)
AS
BEGIN
-- DECLARATIONS
   -- @_DAYS_AND_TABLES:    A temporary table with all intervals and all base type selected (used to show base types without activity).
   DECLARE @_DAYS_AND_TABLES TABLE(DATE_TIME DATETIME, TABLE_IDENTIFIER_VALUE BIGINT, TABLE_IDENT_NAME VARCHAR(50), TABLE_TYPE_IDENT BIGINT, TABLE_TYPE_NAME VARCHAR(50))
   -- @_DAY_VAR:            Variable to create the intervals
   DECLARE @_DAY_VAR DATETIME

-- PARAMETERS
   DECLARE @_BASE_TYPE        AS   INTEGER -- 0 BY TABLE TYPE, 1 BY TABLE
   DECLARE @_TIME_INTERVAL    AS   INTEGER -- -1, 0 DAYS, 1 MONTH, 2 YEAR
   DECLARE @_DATE_FROM        AS   DATETIME
   DECLARE @_DATE_TO          AS   DATETIME
   DECLARE @_ONLY_ACTIVITY    AS   INTEGER -- 0 SHOW ALL, 1 SHOW ONLY WITH ACTIVITY
   DECLARE @_ORDER_BY         AS   INTEGER -- 0 BY INTERVAL DESC, 1 BY IDENTIFIER ASC
   DECLARE @_DELIMITER        AS   CHAR(1)
   DECLARE @_TYPES_TABLE      TABLE(SST_ID INT, SST_VALUE VARCHAR(50)) -- TO STORE THE VALID GAMING TABLE TYPES
   DECLARE @_SELECTED_CURRENCY AS  NVARCHAR(3)

----------------------------------------------------------------------------------------------------------------

-- Initialization --
SET @_BASE_TYPE        =   @pBaseType
SET @_TIME_INTERVAL    =   @pTimeInterval
SET @_DATE_FROM        =   @pDateFrom
SET @_DATE_TO          =   @pDateTo
SET @_ONLY_ACTIVITY    =   @pOnlyActivity
SET @_ORDER_BY         =   @pOrderBy
SET @_DELIMITER        =   ','
SET @_SELECTED_CURRENCY =  @pSelectedCurrency

----------------------------------------------------------------------------------------------------------------

-- CHECK DATE PARAMETERS
IF @_DATE_FROM IS NULL
BEGIN
   -- IF DATE FROM IS NULL, USER FIRST POSIBLE DATE
   SET @_DATE_FROM = CAST('' AS DATETIME)
   SET @pDateFrom  = CAST('' AS DATETIME)
END

IF @_DATE_TO IS NULL
BEGIN
   -- IF DATE TO IS NULL, QUERY UNTIL CURRENT DATE
   SET @_DATE_TO = CAST(GETDATE() AS DATETIME)
   SET @pDateTo  = CAST(GETDATE() AS DATETIME)
END

-- ASSIGN TYPES PARAMETER INTO TABLE
INSERT INTO @_TYPES_TABLE SELECT * FROM dbo.SplitStringIntoTable(@pValidTypes, @_DELIMITER, DEFAULT)

IF @_TIME_INTERVAL <> -1
 BEGIN
   -- INTERVALS DATES AND TABLE PREPARATION
   -- PREPARE DATES RANGE DEPENDING ON TIME INTERVAL
   SET @_DATE_FROM = CASE
                        WHEN @_TIME_INTERVAL = 0 THEN -- BY DAY
                           DATEADD(dd, 0, DATEDIFF(dd, 0, @_DATE_FROM))
                        WHEN @_TIME_INTERVAL = 1 THEN -- BY MONTH
                           CAST(CAST(YEAR(@_DATE_FROM) AS VARCHAR(4)) + RIGHT('0' + CAST(MONTH(@_DATE_FROM) AS VARCHAR(2)), 2) + '01' AS DATETIME)
                        WHEN @_TIME_INTERVAL = 2 THEN -- BY YEAR
                           CAST(CAST(YEAR(@_DATE_FROM) AS VARCHAR(4)) + '0101' AS DATETIME)
                     END

   SET @_DATE_TO = CASE
                        WHEN @_TIME_INTERVAL = 0 THEN -- BY DAY
                           DATEADD(DAY, 1, CAST(@_DATE_TO AS DATETIME))
                        WHEN @_TIME_INTERVAL = 1 THEN -- BY MONTH
                           DATEADD(MONTH, 1, CAST(CAST(YEAR(@_DATE_TO) AS VARCHAR(4)) + RIGHT('0' + CAST(MONTH(@_DATE_TO) AS VARCHAR(2)), 2) + '01' AS DATETIME))
                        WHEN @_TIME_INTERVAL = 2 THEN -- BY YEAR
                           DATEADD(YEAR, 1, CAST(CAST(YEAR(@_DATE_TO) AS VARCHAR(4)) + '0101' AS DATETIME))
                     END

   -- PREPARE THE TIME INTERVALS TABLE
   SET @_DAY_VAR = @_DATE_FROM
   WHILE @_DAY_VAR < @_DATE_TO AND @_DAY_VAR < GETDATE()
   BEGIN
         -- SET THE LINK FIELD FOR THE RESULTS
         IF @_BASE_TYPE = 0
          BEGIN
            -- LINK WITH TABLE TYPES
            INSERT INTO @_DAYS_AND_TABLES
                 SELECT   CAST(@_DAY_VAR AS DATETIME), X.GTT_GAMING_TABLE_TYPE_ID, X.GTT_NAME AS TABLE_IDENT, NULL, NULL
                   FROM   GAMING_TABLES_TYPES AS X
                  WHERE   X.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
          END
         ELSE
          BEGIN
            -- LINK WITH TABLES
            INSERT INTO @_DAYS_AND_TABLES
                 SELECT   CAST(@_DAY_VAR AS DATETIME), X.GT_GAMING_TABLE_ID, X.GT_NAME AS TABLE_IDENT, Z.GTT_GAMING_TABLE_TYPE_ID, Z.GTT_NAME
                   FROM   GAMING_TABLES AS X
              LEFT JOIN   GAMING_TABLES_TYPES AS Z
                     ON   Z.GTT_GAMING_TABLE_TYPE_ID = X.GT_TYPE_ID
                  WHERE   Z.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
          END

          -- SET INCREMENT
          SET @_DAY_VAR = CASE
                               WHEN @_TIME_INTERVAL = 0 THEN DATEADD(DAY,1,@_DAY_VAR)     -- DAY
                               WHEN @_TIME_INTERVAL = 1 THEN DATEADD(MONTH,1,@_DAY_VAR)   -- MONTH
                               WHEN @_TIME_INTERVAL = 2 THEN DATEADD(YEAR,1,@_DAY_VAR)    -- YEAR
                          END
   END

END -- IF INTERVALS <> - 1
ELSE
BEGIN

   -- SET THE LINK FIELD FOR THE RESULTS WITHOUT INTERVALS
   IF @_BASE_TYPE = 0
    BEGIN
      -- LINK WITH TABLE TYPES
      INSERT INTO   @_DAYS_AND_TABLES
           SELECT   NULL, X.GTT_GAMING_TABLE_TYPE_ID, X.GTT_NAME AS TABLE_IDENT, NULL, NULL
             FROM   GAMING_TABLES_TYPES AS X
            WHERE   X.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
    END
   ELSE
    BEGIN
      -- LINK WITH TABLES
      INSERT INTO   @_DAYS_AND_TABLES
           SELECT   NULL, X.GT_GAMING_TABLE_ID, X.GT_NAME AS TABLE_IDENT, Z.GTT_GAMING_TABLE_TYPE_ID, Z.GTT_NAME
             FROM   GAMING_TABLES AS X
        LEFT JOIN   GAMING_TABLES_TYPES AS Z
               ON   Z.GTT_GAMING_TABLE_TYPE_ID = X.GT_TYPE_ID
            WHERE   Z.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
    END

END

-- SELECT INTO TEMPORARY TABLE WITH ALL DATA
-- SPECIFIC REPORT QUERY
  SELECT   X.TABLE_IDENTIFIER
         , X.TABLE_NAME
         , X.TABLE_TYPE
         , X.TABLE_TYPE_NAME
         , X.CM_DATE_ONLY
         , MIN(X.OPEN_HOUR) AS OPEN_HOUR
         , MAX(X.CLOSE_HOUR) AS CLOSE_HOUR
         , SUM(X.SESSION_HOURS) AS SESSION_HOURS
         , SUM(X.BUY_IN) AS BUY_IN
         , SUM(X.CHIPS_IN) AS CHIPS_IN
         , SUM(X.CHIPS_OUT) AS CHIPS_OUT
         , SUM(X.TOTAL_PLAYED) AS TOTAL_PLAYED
         , SUM(X.AVERAGE_BET) AS AVERAGE_BET
         , SUM(X.CHIPS_IN) + SUM(X.BUY_IN) AS TOTAL_DROP
         , CASE WHEN (SUM(X.CHIPS_IN) + SUM(X.BUY_IN)) =0
              THEN 0
              ELSE ((SUM(X.CHIPS_OUT)/(SUM(X.CHIPS_IN) + SUM(X.BUY_IN)))*100)
           END AS HOLD
         , MIN(X.THEORIC_HOLD) AS THEORIC_HOLD
         , CASE WHEN SUM(X.TOTAL_PLAYED) =0
              THEN 0
              ELSE ((SUM(X.NETWIN)/SUM(X.TOTAL_PLAYED)) * 100)
           END AS PAYOUT
         , SUM(X.NETWIN) AS NETWIN
         , X.ISO_CODE

   INTO #GT_TEMPORARY_REPORT_DATA

   FROM (
            -- CORE QUERY
            SELECT   CASE
                        WHEN @_TIME_INTERVAL = 0 THEN     -- TO FILTER BY DAY
                            DATEADD(dd, 0, DATEDIFF(dd, 0, CS_OPENING_DATE))
                        WHEN @_TIME_INTERVAL = 1 THEN     -- TO FILTER BY MONTH
                            CAST(CAST(YEAR(CS_OPENING_DATE) AS VARCHAR(4)) + RIGHT('0' + CAST(MONTH(CS_OPENING_DATE) AS VARCHAR(2)), 2) + '01' AS DATETIME)
                        WHEN @_TIME_INTERVAL = 2 THEN     -- TO FILTER BY YEAR
                            CAST(CAST(YEAR(CS_OPENING_DATE) AS VARCHAR(4)) + '01'+ '01' AS DATETIME)
                     END AS CM_DATE_ONLY
                   , (CASE WHEN @_BASE_TYPE = 0 THEN GTT_GAMING_TABLE_TYPE_ID ELSE GTPS_GAMING_TABLE_ID END)  AS TABLE_IDENTIFIER     -- GET THE BASE TYPE IDENTIFIER
                   , (CASE WHEN @_BASE_TYPE = 0 THEN GTT_NAME ELSE GT_NAME END) AS TABLE_NAME                                         -- GET THE BASE TYPE IDENTIFIER NAME
                   , (CASE WHEN @_BASE_TYPE = 0 THEN NULL ELSE GTT_GAMING_TABLE_TYPE_ID END) AS TABLE_TYPE                            -- TYPE
                   , (CASE WHEN @_BASE_TYPE = 0 THEN NULL ELSE GTT_NAME END) AS TABLE_TYPE_NAME                                       -- TYPE NAME
                   , SUM(GTPS_TOTAL_SELL_CHIPS) AS BUY_IN
                   , SUM(GTPS_CHIPS_IN) AS CHIPS_IN
                   , SUM(GTPS_CHIPS_OUT)  AS CHIPS_OUT
                   , SUM(GTPS_PLAYED_AMOUNT) AS TOTAL_PLAYED
                   , SUM(GTPS_PLAYED_AVERAGE)  AS AVERAGE_BET
                   , SUM(GTPS_NETWIN) AS NETWIN
                   , GTPS_ISO_CODE AS ISO_CODE
                   , GT_THEORIC_HOLD  AS THEORIC_HOLD
                   , CS_OPENING_DATE AS OPEN_HOUR
                   , CS_CLOSING_DATE AS CLOSE_HOUR
                   , DATEDIFF(MINUTE, CS_OPENING_DATE, ISNULL(CS_CLOSING_DATE, GETDATE())) AS SESSION_HOURS
              FROM   GT_PLAY_SESSIONS
             INNER   JOIN GAMING_TABLES ON GT_GAMING_TABLE_ID = GTPS_GAMING_TABLE_ID
             INNER   JOIN GAMING_TABLES_TYPES ON GT_TYPE_ID = GTT_GAMING_TABLE_TYPE_ID
             INNER   JOIN GAMING_TABLES_SESSIONS ON GTPS_GAMING_TABLE_SESSION_ID = GTS_GAMING_TABLE_SESSION_ID
             INNER   JOIN CASHIER_SESSIONS ON GTS_CASHIER_SESSION_ID = CS_SESSION_ID
               AND   CS_OPENING_DATE >= @pDateFrom
               AND   CS_OPENING_DATE < @pDateTo
             WHERE   CS_STATUS = 1
              AND    GT_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
              AND    GTPS_ISO_CODE IN (@pSelectedCurrency)
             GROUP   BY   GTPS_GAMING_TABLE_ID
                   , GT_NAME
                   , CS_OPENING_DATE
                   , CS_CLOSING_DATE
                   , GTT_GAMING_TABLE_TYPE_ID
                   , GTT_NAME
                   , GT_THEORIC_HOLD
                   , GTPS_ISO_CODE
             -- END CORE QUERY
        ) AS X
 GROUP BY  X.TABLE_IDENTIFIER
         , X.TABLE_NAME
         , X.TABLE_TYPE
         , X.TABLE_TYPE_NAME
         , X.CM_DATE_ONLY
         , X.ISO_CODE

  -- Group by indentifier and time interval

IF @_TIME_INTERVAL <> -1
BEGIN
   -- INTERVALS DATES AND TABLE FINAL PREPARATION

   -- FILTER ACTIVITY
   IF @_ONLY_ACTIVITY = 0
    BEGIN
      -- JOIN THE SELECT WITH ALL DATA
      SELECT   DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER,
               DT.TABLE_IDENT_NAME AS TABLE_NAME,
               DT.TABLE_TYPE_IDENT AS TABLE_TYPE,
               DT.TABLE_TYPE_NAME AS TABLE_TYPE_NAME,
               ISNULL(ZZ.BUY_IN, 0) AS BUY_IN,
               ISNULL(ZZ.CHIPS_IN, 0) AS CHIPS_IN,
               ISNULL(ZZ.CHIPS_OUT, 0) AS CHIPS_OUT,
               ISNULL(ZZ.TOTAL_PLAYED, 0) AS TOTAL_PLAYED,
               ISNULL(ZZ.AVERAGE_BET, 0) AS AVERAGE_BET,
               ISNULL(ZZ.TOTAL_DROP, 0) AS TOTAL_DROP,
               ISNULL(ZZ.HOLD, 0) AS HOLD,
               ISNULL(ZZ.THEORIC_HOLD, 0) AS THEORIC_HOLD,
               ISNULL(ZZ.PAYOUT, 0) AS PAYOUT,
               ISNULL(ZZ.NETWIN, 0) AS NETWIN,
               ISO_CODE,
               ZZ.OPEN_HOUR,
               ZZ.CLOSE_HOUR,
               CASE WHEN ZZ.CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED,
               CASE WHEN ZZ.SESSION_HOURS = 0 THEN DATEDIFF(MINUTE,ZZ.OPEN_HOUR,GETDATE()) ELSE ISNULL(ZZ.SESSION_HOURS, 0) END AS SESSION_HOURS,
               DATE_TIME
        FROM   @_DAYS_AND_TABLES DT
   LEFT JOIN   #GT_TEMPORARY_REPORT_DATA ZZ
               ON (
                  (@_TIME_INTERVAL = 0 AND DATE_TIME = ZZ.CM_DATE_ONLY)
                  OR (@_TIME_INTERVAL = 1 AND MONTH(DATE_TIME) = MONTH(ZZ.CM_DATE_ONLY) AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
                  OR (@_TIME_INTERVAL = 2 AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
                )
         AND   TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER
       -- SET ORDER
    ORDER BY   DT.TABLE_TYPE_IDENT ASC,
               CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC,
               DATE_TIME DESC;

    END
   ELSE
    BEGIN
      -- JOIN THE SELECT HIDDING WITHOUT ACTIVITY
      SELECT  DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER,
              DT.TABLE_IDENT_NAME AS TABLE_NAME,
              DT.TABLE_TYPE_IDENT AS TABLE_TYPE,
              DT.TABLE_TYPE_NAME AS TABLE_TYPE_NAME,
              ISNULL(ZZ.BUY_IN, 0) AS BUY_IN,
              ISNULL(ZZ.CHIPS_IN, 0) AS CHIPS_IN,
              ISNULL(ZZ.CHIPS_OUT, 0) AS CHIPS_OUT,
              ISNULL(ZZ.TOTAL_PLAYED, 0) AS TOTAL_PLAYED,
              ISNULL(ZZ.AVERAGE_BET, 0) AS AVERAGE_BET,
              ISNULL(ZZ.TOTAL_DROP, 0) AS TOTAL_DROP,
              ISNULL(ZZ.HOLD, 0) AS HOLD,
              ISNULL(ZZ.THEORIC_HOLD, 0) AS THEORIC_HOLD,
              ISNULL(ZZ.PAYOUT, 0) AS PAYOUT,
              ISNULL(ZZ.NETWIN, 0) AS NETWIN,
              ISO_CODE,
              ZZ.OPEN_HOUR,
              ZZ.CLOSE_HOUR,
              CASE WHEN ZZ.CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED,
              CASE WHEN ZZ.SESSION_HOURS = 0 THEN DATEDIFF(MINUTE,ZZ.OPEN_HOUR,GETDATE()) ELSE ISNULL(ZZ.SESSION_HOURS, 0) END AS SESSION_HOURS,
              DATE_TIME
        FROM  @_DAYS_AND_TABLES DT
  INNER JOIN  #GT_TEMPORARY_REPORT_DATA ZZ
              ON(
                 (@_TIME_INTERVAL = 0 AND DATE_TIME = ZZ.CM_DATE_ONLY)
                 OR (@_TIME_INTERVAL = 1 AND MONTH(DATE_TIME) = MONTH(ZZ.CM_DATE_ONLY) AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
                 OR (@_TIME_INTERVAL = 2 AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
              )
              AND   TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER
       -- SET ORDER
   ORDER BY   DT.TABLE_TYPE_IDENT ASC,
              CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC,
              DATE_TIME DESC;

    END -- IF ONLY_ACTIVITY

END
ELSE  -- ELSE WITHOUT INTERVALS
BEGIN

   -- FINAL WITHOUT INTERVALS

      -- FILTER ACTIVITY
   IF @_ONLY_ACTIVITY = 0
    BEGIN
            -- JOIN DATA INCLUDING WITHOUT ACTIVITY
            SELECT   DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER,
                     DT.TABLE_IDENT_NAME AS TABLE_NAME,
                     DT.TABLE_TYPE_IDENT AS TABLE_TYPE,
                     DT.TABLE_TYPE_NAME,
                     ISNULL(ZZ.BUY_IN, 0) AS BUY_IN,
                     ISNULL(ZZ.CHIPS_IN, 0) AS CHIPS_IN,
                     ISNULL(ZZ.CHIPS_OUT, 0) AS CHIPS_OUT,
                     ISNULL(ZZ.TOTAL_PLAYED, 0) AS TOTAL_PLAYED,
                     ISNULL(ZZ.AVERAGE_BET, 0) AS AVERAGE_BET,
                     ISNULL(ZZ.TOTAL_DROP, 0) AS TOTAL_DROP,
                     ISNULL(ZZ.HOLD, 0) AS HOLD,
                     ISNULL(ZZ.THEORIC_HOLD, 0) AS THEORIC_HOLD,
                     ISNULL(ZZ.PAYOUT, 0) AS PAYOUT,
                     ISNULL(ZZ.NETWIN, 0) AS NETWIN,
                     ISO_CODE,
                     OPEN_HOUR,
                     CLOSE_HOUR,
                     CASE WHEN CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED,
                     CASE WHEN SESSION_HOURS = 0 THEN DATEDIFF(MINUTE, OPEN_HOUR, GETDATE()) ELSE ISNULL(SESSION_HOURS, 0) END AS SESSION_HOURS
              FROM   @_DAYS_AND_TABLES DT
         LEFT JOIN   #GT_TEMPORARY_REPORT_DATA ZZ
                ON   DT.TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER
          ORDER BY   DT.TABLE_TYPE_IDENT ASC,
                     CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC,
                     DATE_TIME DESC;

     END
    ELSE
     BEGIN
            -- JOIN DATA WITH ONLY ACTIVITY
            SELECT   DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER,
                     DT.TABLE_IDENT_NAME AS TABLE_NAME,
                     DT.TABLE_TYPE_IDENT AS TABLE_TYPE,
                     DT.TABLE_TYPE_NAME,
                     ISNULL(ZZ.BUY_IN, 0) AS BUY_IN,
                     ISNULL(ZZ.CHIPS_IN, 0) AS CHIPS_IN,
                     ISNULL(ZZ.CHIPS_OUT, 0) AS CHIPS_OUT,
                     ISNULL(ZZ.TOTAL_PLAYED, 0) AS TOTAL_PLAYED,
                     ISNULL(ZZ.AVERAGE_BET, 0) AS AVERAGE_BET,
                     ISNULL(ZZ.TOTAL_DROP, 0) AS TOTAL_DROP,
                     ISNULL(ZZ.HOLD, 0) AS HOLD,
                     ISNULL(ZZ.THEORIC_HOLD, 0) AS THEORIC_HOLD,
                     ISNULL(ZZ.PAYOUT, 0) AS PAYOUT,
                     ISNULL(ZZ.NETWIN, 0) AS NETWIN,
                     ISO_CODE,
                     OPEN_HOUR,
                     CLOSE_HOUR,
                     CASE WHEN CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED,
                     CASE WHEN SESSION_HOURS = 0 THEN DATEDIFF(MINUTE, OPEN_HOUR, GETDATE()) ELSE ISNULL(SESSION_HOURS, 0) END AS SESSION_HOURS
              FROM   @_DAYS_AND_TABLES DT
        INNER JOIN   #GT_TEMPORARY_REPORT_DATA ZZ
                ON   DT.TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER
          ORDER BY   DT.TABLE_TYPE_IDENT ASC,
                     CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC,
                     DATE_TIME DESC;

     END
 END

-- ERASE THE TEMPORARY DATA
DROP TABLE #GT_TEMPORARY_REPORT_DATA

END -- END PROCEDURE
GO

-- PERMISSIONS
GRANT EXECUTE ON [dbo].[GT_Base_Report_Data_Player_Tracking] TO [wggui] WITH GRANT OPTION
GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetBankTransacctionsToReconciliate]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[GetBankTransacctionsToReconciliate]
GO

CREATE PROCEDURE [dbo].[GetBankTransacctionsToReconciliate]
@pUser INT = 0
AS
BEGIN

SELECT 
  T.pt_created, 
  T.pt_id, 
  T.pt_operation_id, 
  T.pt_total_amount, 
  T.pt_card_number,
  R.ptc_reconciliate,
  U.gu_full_name  
FROM pinpad_transactions T
  INNER JOIN gui_users U
    ON U.gu_user_id = T.pt_user_id
  INNER JOIN account_operations A
    ON A.ao_operation_id = T.pt_operation_id
  INNER JOIN pinpad_transactions_reconciliation R
    ON R.ptc_id = T.pt_id
WHERE T.pt_status = 2
  AND A.ao_code IN (1,17)
  AND U.gu_user_id = @pUser
  AND R.ptc_reconciliate = 0
  
ORDER BY T.pt_created

END
GO

GRANT EXECUTE ON GetBankTransacctionsToReconciliate TO wggui WITH GRANT OPTION 
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID('GetReportBanking') AND type in ('P', 'PC'))
  DROP PROCEDURE GetReportBanking
GO
CREATE PROCEDURE GetReportBanking
   @pFrom DATETIME,
   @pTo  DATETIME,
   @pStatus NVARCHAR(MAX),
   @pCardType NVARCHAR(MAX),
   @pNational NVARCHAR(MAX)
AS
BEGIN 
  DECLARE @NationalCurrency NVARCHAR(3)
  SET @NationalCurrency = (SELECT GP.GP_KEY_VALUE FROM GENERAL_PARAMS AS GP 
                           WHERE GP.GP_GROUP_KEY = 'RegionalOptions' AND GP.GP_SUBJECT_KEY = 'CurrencyISOCode')
  IF @pFrom IS NULL
  BEGIN
   SET @pFrom = (SELECT MIN(PT_CREATED) FROM PINPAD_TRANSACTIONS)
  END
  SELECT * FROM
  (                       
    SELECT 
      PT.PT_CREATED
     ,PT.PT_CONTROL_NUMBER
     ,GU.GU_USERNAME
     ,PT.PT_ACCOUNT_ID
     ,AC.AC_HOLDER_NAME
     ,PT.PT_BANK_NAME
     ,PT.PT_CARD_NUMBER
     ,PT.PT_CARD_TYPE  
     ,PT.PT_CARD_HOLDER
     ,PT.PT_TRANSACTION_AMOUNT
     ,PT.PT_COMMISSION_AMOUNT
     ,PT.PT_TOTAL_AMOUNT
     ,PT.PT_STATUS
     ,PT.PT_ERROR_MESSAGE
     ,PT.PT_REFERENCE
     ,PT.PT_OPERATION_ID
     ,PT.PT_MERCHANT_ID
     ,PT.PT_AUTH_CODE
     ,PT.PT_COLLECTED
     ,CASE 
          WHEN PT.PT_CARD_ISO_CODE = @NationalCurrency  THEN 1
          ELSE 0
      END AS PT_IS_NATIONAL
    FROM PINPAD_TRANSACTIONS AS PT
    LEFT JOIN GUI_USERS AS GU ON PT.PT_USER_ID = GU.GU_USER_ID
    LEFT JOIN ACCOUNTS AS AC ON PT.PT_ACCOUNT_ID = AC.AC_ACCOUNT_ID
    WHERE PT.PT_CREATED >= @pFrom AND PT.PT_CREATED < @pTo
  ) AS PT
  WHERE PT.PT_STATUS IN (SELECT SST_VALUE FROM SplitStringIntoTable(@pStatus,',',1))
    AND PT.PT_CARD_TYPE IN (SELECT SST_VALUE FROM SplitStringIntoTable(@pCardType,',',1))
    AND PT.PT_IS_NATIONAL IN (SELECT SST_VALUE FROM SplitStringIntoTable(@pNational,',',1))
END
GO

GRANT EXECUTE ON GetReportBanking TO wggui WITH GRANT OPTION 
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CageGetGlobalReportData]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[CageGetGlobalReportData]
GO

CREATE PROCEDURE [dbo].[CageGetGlobalReportData]
        @pCageSessionIds NVARCHAR(MAX) = NULL
AS
BEGIN
  
  DECLARE @NationalIsoCode NVARCHAR(3)
  DECLARE @CageOperationType_FromTerminal INT
  DECLARE @TicketsId INT
  DECLARE @BillsId INT
  DECLARE @Currencies AS NVARCHAR(200)
  DECLARE @CountSessions INT
  DECLARE @List VARCHAR(MAX)
  DECLARE @StockIsStored BIT
  DECLARE @CageCurrencyType_ColorChips INT

  SELECT @NationalIsoCode = GP_KEY_VALUE 
    FROM GENERAL_PARAMS 
   WHERE GP_GROUP_KEY = 'RegionalOptions' 
     AND GP_SUBJECT_KEY = 'CurrencyISOCode'

  SELECT @Currencies = GP_KEY_VALUE 
    FROM GENERAL_PARAMS 
   WHERE GP_GROUP_KEY = 'RegionalOptions' 
     AND GP_SUBJECT_KEY = 'CurrenciesAccepted'

  -- Split currencies ISO codes
  SELECT   CURRENCY_ISO_CODE INTO #TMP_CURRENCIES_ISO_CODES 
    FROM ( SELECT   SST_VALUE AS CURRENCY_ISO_CODE FROM [SplitStringIntoTable] (@Currencies, ';', 1)
            UNION ALL 
           SELECT   'X01' AS CURRENCY_ISO_CODE    -- for retrocompatibility
            UNION ALL 
           SELECT   'X02' AS CURRENCY_ISO_CODE ) AS CURRENCIES
               
  SET @CageOperationType_FromTerminal = 104
  SET @TicketsId = -200
  SET @BillsId = 0
  SET @CageCurrencyType_ColorChips = 1003
   
  -- Session IDs Split
  SELECT SST_VALUE AS CAGE_SESSION_ID INTO #TMP_CAGE_SESSIONS FROM [SplitStringIntoTable] (@pCageSessionIds,','  , 1)
  SET @CountSessions = (SELECT COUNT(*) FROM #TMP_CAGE_SESSIONS )
  
  IF @CountSessions = 1 
    SELECT @List = CGS_CAGE_STOCK FROM CAGE_SESSIONS 
     WHERE CGS_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)

  -- TABLE[0]: Get cage stock 
  IF (@CountSessions = 1 AND @List IS NOT NULL) 
  BEGIN
    --Get stock on close session
    SET @StockIsStored =  1
    SELECT SST_VALUE AS STOCK_ROW INTO #TMP_STOCK_ROWS FROM [SplitStringIntoTable] (@List, '|', 1)
    
    SELECT left(STOCK_ROW, 3) AS CGS_ISO_CODE,                        
       CAST(REPLACE(PARSENAME(REPLACE(REPLACE(RIGHT(STOCK_ROW, LEN(STOCK_ROW)-4),'.', '|'), ';', '.'), 4), '|', '.') AS MONEY) AS CGS_DENOMINATION,   
       CAST(REPLACE(PARSENAME(REPLACE(REPLACE(RIGHT(STOCK_ROW, LEN(STOCK_ROW)-4), '.', '|'), ';', '.'), 3), '|', '.') AS DECIMAL(15,2)) AS CGS_QUANTITY,       
       CAST(REPLACE(PARSENAME(REPLACE(REPLACE(RIGHT(STOCK_ROW, LEN(STOCK_ROW)-4), '.', '|'), ';', '.'), 2), '|', '.') AS INT) AS CGS_CAGE_CURRENCY_TYPE,  
       CAST(REPLACE(PARSENAME(REPLACE(REPLACE(RIGHT(STOCK_ROW, LEN(STOCK_ROW)-4), '.', '|'), ';', '.'), 1), '|', '.') AS INT) AS CGS_CHIP_ID, 
       CH_NAME AS NAME, CH_DRAWING AS DRAWING  
       INTO #TMP_STOCK                                                                                                               
    FROM #TMP_STOCK_ROWS 
    LEFT JOIN chips ON ch_chip_id = CAST(REPLACE(PARSENAME(REPLACE(REPLACE(RIGHT(STOCK_ROW, LEN(STOCK_ROW)-4), '.', '|'), ';', '.'), 1), '|', '.') AS INT)

    SELECT   A.*
           , CE_CURRENCY_ORDER 
      FROM   #TMP_STOCK AS A
      LEFT   JOIN CURRENCY_EXCHANGE ON CGS_ISO_CODE = CE_CURRENCY_ISO_CODE AND CE_TYPE = 0
  ORDER BY   CASE WHEN CGS_CAGE_CURRENCY_TYPE < 1000 THEN 0
                  WHEN CGS_CAGE_CURRENCY_TYPE = 1003 THEN 9999999
                  ELSE 1 END
           , CE_CURRENCY_ORDER
           , CGS_ISO_CODE
           , CGS_CAGE_CURRENCY_TYPE
           , CGS_DENOMINATION DESC
           , CASE WHEN CGS_DENOMINATION >= 0 THEN CGS_DENOMINATION 
                  ELSE CGS_DENOMINATION * (-100000) END                                                                    

    DROP TABLE #TMP_STOCK_ROWS                                                                                                      
    DROP TABLE #TMP_STOCK     
    
  END --IF (@CountSessions = 1 AND @List IS NOT NULL)    
  ELSE --IF (@CountSessions = 1 AND @List IS NOT NULL) 
  BEGIN
    -- Get current stock
    SET @StockIsStored =  0
    EXEC CageCurrentStock  
  END --ELSE
  
  IF @pCageSessionIds IS NOT NULL
  BEGIN
      
    -- TABLE[1]: Get cage sessions information
    SELECT  a.*
          , (ROW_NUMBER() OVER(ORDER BY CM_SOURCE_TARGET_ID
          , CASE CM_CONCEPT_ID WHEN 0 THEN '000' ELSE CC_DESCRIPTION END
          , CASE WHEN CAGE_CURRENCY_TYPE < 1000 THEN 0
                 WHEN CAGE_CURRENCY_TYPE = 1003 THEN 9999999
                 ELSE 1 END
          , CE_CURRENCY_ORDER
          , CM_ISO_CODE
          , CAGE_CURRENCY_TYPE)) as ORDEN
    FROM (
    SELECT CSM.CSM_SOURCE_TARGET_ID AS CM_SOURCE_TARGET_ID
         , CSM.CSM_CONCEPT_ID AS CM_CONCEPT_ID
         , CASE CSM.CSM_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CSM.CSM_ISO_CODE END AS CM_ISO_CODE
         , CASE CSM.CSM_ISO_CODE WHEN 'X01' THEN 1001 ELSE CASE WHEN CSM_CAGE_CURRENCY_TYPE < 1000 THEN 0 ELSE CSM_CAGE_CURRENCY_TYPE END END AS CAGE_CURRENCY_TYPE
         , SUM(CSM.CSM_VALUE_IN) AS CM_VALUE_IN
         , SUM(CSM.CSM_VALUE_OUT) AS CM_VALUE_OUT
         , CST.CST_SOURCE_TARGET_NAME
         , CC.CC_DESCRIPTION
      FROM CAGE_SESSION_METERS AS CSM
           INNER JOIN CAGE_SOURCE_TARGET_CONCEPTS AS CSTC ON CSTC.CSTC_CONCEPT_ID = CSM.CSM_CONCEPT_ID AND CSTC.CSTC_SOURCE_TARGET_ID = CSM.CSM_SOURCE_TARGET_ID
           INNER JOIN CAGE_CONCEPTS AS CC ON CC.CC_CONCEPT_ID = CSTC.CSTC_CONCEPT_ID
           INNER JOIN CAGE_SOURCE_TARGET AS CST ON CST.CST_SOURCE_TARGET_ID = CSTC.CSTC_SOURCE_TARGET_ID
     WHERE CSM.CSM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
       AND CC.CC_SHOW_IN_REPORT = 1
       AND CC.CC_ENABLED = 1
       AND (CC.CC_TYPE = 1 OR CC_CONCEPT_ID IN (9, 10,11))
       AND CSTC.CSTC_ENABLED = 1
       AND CSM_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)
  GROUP BY CSM.CSM_SOURCE_TARGET_ID
         , CSM.CSM_CONCEPT_ID
         , CASE CSM.CSM_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CSM.CSM_ISO_CODE END 
         , CASE CSM.CSM_ISO_CODE WHEN 'X01' THEN 1001 ELSE CASE WHEN CSM_CAGE_CURRENCY_TYPE < 1000 THEN 0 ELSE CSM_CAGE_CURRENCY_TYPE END END  
         , CST.CST_SOURCE_TARGET_NAME
         , CC.CC_DESCRIPTION
         ) as a
    LEFT  JOIN CURRENCY_EXCHANGE ON CM_ISO_CODE = CE_CURRENCY_ISO_CODE AND CE_TYPE = 0
  ORDER BY CM_SOURCE_TARGET_ID
         , CASE CM_CONCEPT_ID WHEN 0 THEN '000' ELSE CC_DESCRIPTION END
         , CASE WHEN CAGE_CURRENCY_TYPE < 1000 THEN 0
                WHEN CAGE_CURRENCY_TYPE = 1003 THEN 9999999
                ELSE 1 END
         , CE_CURRENCY_ORDER
         , CM_ISO_CODE
         , CAGE_CURRENCY_TYPE 
         
     -- TABLE[2]: Get dynamic liabilities
    SELECT CC.CC_CONCEPT_ID AS LIABILITY_CONCEPT_ID
         , CASE CC_CONCEPT_ID WHEN 0 THEN '000' ELSE CC_DESCRIPTION END AS LIABILITY_NAME 
         , CASE CSM_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CSM_ISO_CODE END AS LIABILITY_ISO_CODE
         , CASE WHEN CASE CSM_ISO_CODE WHEN 'X01' THEN 1001 ELSE CSM_CAGE_CURRENCY_TYPE END < 1000 THEN 0
                ELSE CSM_CAGE_CURRENCY_TYPE END   AS LIABILITY_CAGE_CURRENCY_TYPE
         , SUM(CSM.CSM_VALUE) AS LIABILITY_VALUE
      FROM CAGE_SESSION_METERS AS CSM
     INNER JOIN CAGE_CONCEPTS AS CC ON CC.CC_CONCEPT_ID = CSM.CSM_CONCEPT_ID
      LEFT JOIN CURRENCY_EXCHANGE ON CSM_ISO_CODE = CE_CURRENCY_ISO_CODE AND CE_TYPE = 0
     WHERE CC.CC_ENABLED = 1
       AND CC.CC_SHOW_IN_REPORT = 1
       AND CC.CC_IS_PROVISION = 1
       AND CSM.CSM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
       AND CSM_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)
  GROUP BY CC.CC_CONCEPT_ID  
         , CASE CC_CONCEPT_ID WHEN 0 THEN '000' ELSE CC_DESCRIPTION END
         , CASE WHEN CASE CSM_ISO_CODE WHEN 'X01' THEN 1001 ELSE CSM_CAGE_CURRENCY_TYPE END < 1000 THEN 0
                ELSE CSM_CAGE_CURRENCY_TYPE END
         , CE_CURRENCY_ORDER
         , CASE CSM_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CSM_ISO_CODE END
  ORDER BY CC.CC_CONCEPT_ID  
         , CASE CC_CONCEPT_ID WHEN 0 THEN '000' ELSE CC_DESCRIPTION END
         , CASE WHEN CASE CSM_ISO_CODE WHEN 'X01' THEN 1001 ELSE CSM_CAGE_CURRENCY_TYPE END < 1000 THEN 0
                ELSE CSM_CAGE_CURRENCY_TYPE END
         , CE_CURRENCY_ORDER
         , CASE CSM_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CSM_ISO_CODE END
      
    -- TABLE[3]: Get session sumary
    SELECT CMD_ISO_CODE AS ISO_CODE
         , CASE WHEN CAGE_CURRENCY_TYPE = 99 THEN 0 ELSE CAGE_CURRENCY_TYPE END AS CAGE_CURRENCY_TYPE
         , ISNULL(ORIGIN, 0) AS ORIGIN
         , SUM(CMD_DEPOSITS) AS DEPOSITS
         , SUM(CMD_WITHDRAWALS) AS WITHDRAWALS
      FROM (SELECT CASE CMD_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CMD_ISO_CODE END AS CMD_ISO_CODE
                 , CASE WHEN CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE CAGE_CURRENCY_TYPE END < 1000 THEN 0
                        ELSE CAGE_CURRENCY_TYPE END AS CAGE_CURRENCY_TYPE
                 , ORIGIN
                 , SUM(CMD_DEPOSITS) CMD_DEPOSITS
                 , SUM(CMD_WITHDRAWALS) CMD_WITHDRAWALS      
              FROM (SELECT CASE WHEN CMD_QUANTITY >= 0 THEN @BillsId
                                ELSE CMD_QUANTITY END  AS ORIGIN
                         , CASE CMD_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CMD_ISO_CODE END AS CMD_ISO_CODE
                         , CASE WHEN CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE CMD_CAGE_CURRENCY_TYPE END < 1000 THEN 0
                                ELSE CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE ISNULL(CMD_CAGE_CURRENCY_TYPE, 0) END END  AS CAGE_CURRENCY_TYPE
                         , CMD_DENOMINATION
                         , CASE WHEN CGM_TYPE IN (100, 101, 103, 104) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY < 0 AND CMD_QUANTITY <> @TicketsId THEN CMD_DENOMINATION
                                                                                            WHEN CMD_QUANTITY = @TicketsId THEN 0
                                                                                            WHEN CMD_CAGE_CURRENCY_TYPE = @CageCurrencyType_ColorChips THEN CMD_QUANTITY
                                                                                       ELSE CMD_QUANTITY * CMD_DENOMINATION END), 0)
                                ELSE 0 END AS CMD_DEPOSITS
                         , CASE WHEN CGM_TYPE IN (0, 1, 4, 5) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY < 0 AND CMD_QUANTITY <> @TicketsId THEN CMD_DENOMINATION
                                                                                    WHEN CMD_QUANTITY = @TicketsId THEN 0
                                                                                    WHEN CMD_CAGE_CURRENCY_TYPE = @CageCurrencyType_ColorChips THEN CMD_QUANTITY
                                                                               ELSE CMD_QUANTITY * CMD_DENOMINATION END), 0)
                                ELSE 0 END AS CMD_WITHDRAWALS
                      FROM CAGE_MOVEMENTS 
                INNER JOIN CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          
                     WHERE CGM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
                       AND (CGM_STATUS IN (2, 3, 4, 8, 9, 10) 
                            OR (CGM_STATUS = 0 AND CGM_TYPE = 0))
                  GROUP BY CASE WHEN CMD_QUANTITY >= 0 THEN @BillsId
                                ELSE CMD_QUANTITY END  
                         , CASE CMD_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CMD_ISO_CODE END 
                         , CASE WHEN CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE CMD_CAGE_CURRENCY_TYPE END < 1000 THEN 0
                                ELSE CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE ISNULL(CMD_CAGE_CURRENCY_TYPE, 0) END END 
                         , CMD_DENOMINATION
                         , CGM_TYPE 
                  -- Tickets 
                  UNION ALL
                  
                    SELECT @TicketsId AS ORIGIN
                         , ISNULL(TI_CUR0, @NationalIsoCode) AS CMD_ISO_CODE 
                         , ISNULL(CMD_CAGE_CURRENCY_TYPE, 0) AS CAGE_CURRENCY_TYPE                                    
                         , 1 AS CMD_DENOMINATION
                         , CASE WHEN CGM_TYPE IN (100, 101, 103, 104) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY = @TicketsId THEN 
                                                                                                 CASE WHEN TI_AMT0 IS NULL THEN TI_AMOUNT 
                                                                                                      ELSE TI_AMT0 END
                                                                                       ELSE 0 END), 0)
                                ELSE 0 END AS CMD_DEPOSITS
                         , CASE WHEN CGM_TYPE IN (0, 1, 4, 5) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY = @TicketsId THEN 
                                                                                         CASE WHEN TI_AMT0 IS NULL THEN TI_AMOUNT 
                                                                                              ELSE TI_AMT0 END
                                                                                    ELSE 0 END), 0)
                                ELSE 0 END AS CMD_WITHDRAWALS
                      FROM CAGE_MOVEMENTS 
                           INNER JOIN   CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          
                           INNER JOIN   TICKETS ON TICKETS.TI_COLLECTED_MONEY_COLLECTION = CAGE_MOVEMENTS.CGM_MC_COLLECTION_ID     
                     WHERE  CGM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
                  AND (CGM_STATUS IN (2, 3, 4, 8, 9, 10) 
                            OR (CGM_STATUS = 0 AND CGM_TYPE = 0)) AND CMD_QUANTITY = @TicketsId  
              AND CAGE_MOVEMENTS.CGM_TYPE <> @CageOperationType_FromTerminal
                     GROUP BY ISNULL(TI_CUR0, @NationalIsoCode) 
                         , ISNULL(CMD_CAGE_CURRENCY_TYPE, 0)                                 
                         , CGM_TYPE
                   ) AS T1        
          GROUP BY ORIGIN
                  ,CASE CMD_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CMD_ISO_CODE END 
                 , CASE WHEN CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE CAGE_CURRENCY_TYPE END < 1000 THEN 0
                        ELSE CAGE_CURRENCY_TYPE END 
                 

            -- GET Terminal value-in amounts
              
              UNION ALL 
              
              SELECT CASE TI_CUR0 WHEN 'X01' THEN @NationalIsoCode ELSE ISNULL(TI_CUR0, @NationalIsoCode) END AS CMD_ISO_CODE
                    , 0 AS CAGE_CURRENCY_TYPE
                    , @TicketsId AS ORIGIN
                    , ISNULL(SUM (CASE WHEN TI_AMT0 IS NULL THEN TI_AMOUNT 
                                        ELSE TI_AMT0 END),0)  AS DEPOSITS
                    , 0 AS WITHDRAWS
              FROM MONEY_COLLECTIONS
                  INNER JOIN CAGE_MOVEMENTS ON CGM_MC_COLLECTION_ID = MC_COLLECTION_ID 
          INNER JOIN TICKETS ON TICKETS.TI_COLLECTED_MONEY_COLLECTION = CAGE_MOVEMENTS.CGM_MC_COLLECTION_ID
             WHERE CAGE_MOVEMENTS.CGM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
               AND CAGE_MOVEMENTS.CGM_TYPE = @CAGEOPERATIONTYPE_FROMTERMINAL
               AND (CGM_STATUS IN (2, 3, 4, 8, 9, 10) 
                    OR (CGM_STATUS = 0 AND CGM_TYPE = 0))           
             GROUP BY CASE TI_CUR0 WHEN 'X01' THEN @NationalIsoCode ELSE ISNULL(TI_CUR0, @NationalIsoCode) END, CGM_TYPE

              UNION ALL
              
               SELECT CASE TE_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE ISNULL(TE_ISO_CODE, @NationalIsoCode) END AS CMD_ISO_CODE
                    , 0 AS CAGE_CURRENCY_TYPE
                    , @BillsId AS ORIGIN
                    , MC_COLLECTED_BILL_AMOUNT + MC_COLLECTED_COIN_AMOUNT AS DEPOSITS
                    , 0 AS WITHDRAWS
              FROM MONEY_COLLECTIONS
                  INNER JOIN CAGE_MOVEMENTS ON CGM_MC_COLLECTION_ID = MC_COLLECTION_ID 
                  INNER JOIN TERMINALS ON TE_TERMINAL_ID = MC_TERMINAL_ID
             WHERE CAGE_MOVEMENTS.CGM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
               AND CAGE_MOVEMENTS.CGM_TYPE = @CageOperationType_FromTerminal
               AND (CGM_STATUS IN (2, 3, 4, 8, 9, 10) 
                    OR (CGM_STATUS = 0 AND CGM_TYPE = 0))
                                      
          ) AS _TBL_GLB
  LEFT JOIN CURRENCY_EXCHANGE ON CMD_ISO_CODE = CE_CURRENCY_ISO_CODE AND CE_TYPE = 0
      WHERE CMD_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)
   GROUP BY CAGE_CURRENCY_TYPE 
          , CE_CURRENCY_ORDER
          , CMD_ISO_CODE 
          , ORIGIN  
   ORDER BY CAGE_CURRENCY_TYPE 
          , CE_CURRENCY_ORDER
          , CMD_ISO_CODE 
          , ORIGIN DESC 
        
    -- TABLE[4]: Other System Concepts    
    SELECT * FROM (       
    SELECT CSM_CONCEPT_ID AS CM_CONCEPT_ID
         , CC.CC_DESCRIPTION AS CM_DESCRIPTION
         , CASE CSM_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CSM.CSM_ISO_CODE END AS CM_ISO_CODE
         , CASE CSM_ISO_CODE WHEN 'X01' THEN 1001 ELSE CASE WHEN CSM_CAGE_CURRENCY_TYPE < 1000 THEN 0 ELSE CSM_CAGE_CURRENCY_TYPE END END AS CAGE_CURRENCY_TYPE
         , SUM(CSM_VALUE) AS CM_VALUE
      FROM CAGE_SESSION_METERS AS CSM
           INNER JOIN CAGE_CONCEPTS AS CC ON CSM.CSM_CONCEPT_ID = CC.CC_CONCEPT_ID
     WHERE CSM_SOURCE_TARGET_ID = 0
       AND CSM_CONCEPT_ID IN (7, 8)
       AND CSM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
       AND CSM_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)
  GROUP BY CSM_CONCEPT_ID
         , CC.CC_DESCRIPTION
         , CASE CSM_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CSM.CSM_ISO_CODE END 
         , CASE CSM_ISO_CODE WHEN 'X01' THEN 1001 ELSE CASE WHEN CSM_CAGE_CURRENCY_TYPE < 1000 THEN 0 ELSE CSM_CAGE_CURRENCY_TYPE END END
         ) AS A
  ORDER BY CM_CONCEPT_ID
         , CASE CM_ISO_CODE WHEN @NationalIsoCode THEN '000' ELSE CM_ISO_CODE END
         , CAGE_CURRENCY_TYPE
         
     -- TABLE[5]: Cage Counts
    SELECT ORIGIN AS ORIGIN
         , CASE CMD_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CMD_ISO_CODE END AS ISO_CODE
         , CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE CASE WHEN CAGE_CURRENCY_TYPE = 1 THEN 0 ELSE CAGE_CURRENCY_TYPE END END AS CAGE_CURRENCY_TYPE
         , ISNULL(SUM(CMD_DEPOSITS), 0) AS DEPOSITS
      FROM (SELECT CASE WHEN CMD_QUANTITY >= 0 THEN 0
                        ELSE CMD_QUANTITY END  AS ORIGIN
                 , CMD_ISO_CODE
                 , ISNULL(CMD_CAGE_CURRENCY_TYPE, 0) AS CAGE_CURRENCY_TYPE
                 , ISNULL(SUM (  CASE WHEN CMD_QUANTITY >= 0 THEN CMD_QUANTITY ELSE 1 END 
                               * CASE WHEN CGM_TYPE = 99 THEN -1 ELSE 1 END
                               * CASE WHEN CMD_CAGE_CURRENCY_TYPE = @CageCurrencyType_ColorChips THEN 1 ELSE CMD_DENOMINATION END), 0) AS CMD_DEPOSITS
              FROM CAGE_MOVEMENTS 
                   INNER JOIN CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          
             WHERE CGM_TYPE IN (99, 199)
          GROUP BY CMD_ISO_CODE, ISNULL(CMD_CAGE_CURRENCY_TYPE, 0), CMD_DENOMINATION, CGM_TYPE, CMD_QUANTITY
            ) AS T1           
   GROUP BY ORIGIN, CASE CMD_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CMD_ISO_CODE END 
         , CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE CASE WHEN CAGE_CURRENCY_TYPE = 1 THEN 0 ELSE CAGE_CURRENCY_TYPE END END 
           
    UNION ALL
              
    SELECT NULL AS ORIGIN
         , CASE CMD_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CMD_ISO_CODE END AS ISO_CODE
         , CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE CASE WHEN CAGE_CURRENCY_TYPE = 1 THEN 0 ELSE CAGE_CURRENCY_TYPE END END AS CAGE_CURRENCY_TYPE
         , ISNULL(SUM(CMD_DEPOSITS), 0) AS DEPOSITS
      FROM (SELECT CMD_ISO_CODE
                 , ISNULL(CMD_CAGE_CURRENCY_TYPE, 0) AS CAGE_CURRENCY_TYPE
                 , ISNULL(SUM (  CASE WHEN CMD_QUANTITY >= 0 THEN CMD_QUANTITY ELSE 1 END 
                               * CASE WHEN CGM_TYPE = 99 THEN -1 ELSE 1 END
                               * CASE WHEN CMD_CAGE_CURRENCY_TYPE = @CageCurrencyType_ColorChips THEN 1 ELSE CMD_DENOMINATION END), 0) AS CMD_DEPOSITS
              FROM CAGE_MOVEMENTS 
                   INNER JOIN CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          
             WHERE CGM_TYPE IN (99, 199)
          GROUP BY CMD_ISO_CODE, ISNULL(CMD_CAGE_CURRENCY_TYPE, 0), CMD_DENOMINATION, CGM_TYPE, CMD_QUANTITY
           ) AS T1           
  GROUP BY CASE CMD_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CMD_ISO_CODE END 
         , CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE CASE WHEN CAGE_CURRENCY_TYPE = 1 THEN 0 ELSE CAGE_CURRENCY_TYPE END END 
 
    DROP TABLE #TMP_CAGE_SESSIONS 

  END -- IF @pCageSessionIds IS NOT NULL THEN
      
  ELSE -- IF @pCageSessionIds IS NULL
  BEGIN
      
    -- TABLE[1]: Get cage sessions information
    SELECT a.* 
          , (ROW_NUMBER() OVER(ORDER BY CM_SOURCE_TARGET_ID
          , CASE CM_CONCEPT_ID WHEN 0 THEN '000' ELSE CC_DESCRIPTION END
          , CASE WHEN CAGE_CURRENCY_TYPE < 1000 THEN 0
                 WHEN CAGE_CURRENCY_TYPE = 1003 THEN 9999999
                 ELSE 1 END
          , CE_CURRENCY_ORDER
          , CM_ISO_CODE
          , CAGE_CURRENCY_TYPE)) as ORDEN
    FROM (
    SELECT CM.CM_SOURCE_TARGET_ID
         , CM.CM_CONCEPT_ID
         , CASE CM.CM_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CM.CM_ISO_CODE END AS CM_ISO_CODE
         , CASE CM.CM_ISO_CODE WHEN 'X01' THEN 1001 ELSE CASE WHEN CM_CAGE_CURRENCY_TYPE < 1000 THEN 0 ELSE CM_CAGE_CURRENCY_TYPE END END AS CAGE_CURRENCY_TYPE
         , SUM(CM.CM_VALUE_IN) AS CM_VALUE_IN
         , SUM(CM.CM_VALUE_OUT) AS CM_VALUE_OUT
         , CST.CST_SOURCE_TARGET_NAME
         , CC.CC_DESCRIPTION
      FROM CAGE_METERS AS CM
           INNER JOIN CAGE_SOURCE_TARGET_CONCEPTS AS CSTC ON CSTC.CSTC_CONCEPT_ID = CM.CM_CONCEPT_ID AND CSTC.CSTC_SOURCE_TARGET_ID = CM.CM_SOURCE_TARGET_ID
           INNER JOIN CAGE_CONCEPTS AS CC ON CC.CC_CONCEPT_ID = CSTC.CSTC_CONCEPT_ID
           INNER JOIN CAGE_SOURCE_TARGET AS CST ON CST.CST_SOURCE_TARGET_ID = CSTC.CSTC_SOURCE_TARGET_ID
     WHERE (CC.CC_TYPE = 1 OR CC_CONCEPT_ID IN (9, 10, 11))
       AND CC.CC_ENABLED = 1
       AND CSTC.CSTC_ENABLED = 1
       AND CC.CC_SHOW_IN_REPORT = 1
       AND CM_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)
  GROUP BY CM.CM_SOURCE_TARGET_ID
         , CM.CM_CONCEPT_ID
         , CASE CM.CM_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CM.CM_ISO_CODE END 
         , CASE CM.CM_ISO_CODE WHEN 'X01' THEN 1001 ELSE CASE WHEN CM_CAGE_CURRENCY_TYPE < 1000 THEN 0 ELSE CM_CAGE_CURRENCY_TYPE END END 
         , CST.CST_SOURCE_TARGET_NAME
         , CC.CC_DESCRIPTION
         ) as a
    LEFT  JOIN CURRENCY_EXCHANGE ON CM_ISO_CODE = CE_CURRENCY_ISO_CODE AND CE_TYPE = 0
  ORDER BY CM_SOURCE_TARGET_ID
         , CASE CM_CONCEPT_ID WHEN 0 THEN '000' ELSE CC_DESCRIPTION END
         , CASE WHEN CAGE_CURRENCY_TYPE < 1000 THEN 0
                WHEN CAGE_CURRENCY_TYPE = 1003 THEN 9999999
                ELSE 1 END
         , CE_CURRENCY_ORDER
         , CM_ISO_CODE
         , CAGE_CURRENCY_TYPE

    -- TABLE[2]: Get dynamic liabilities
    SELECT CC_CONCEPT_ID AS LIABILITY_CONCEPT_ID
         , CC_DESCRIPTION AS LIABILITY_NAME 
         , CASE CM.CM_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CM.CM_ISO_CODE END AS LIABILITY_ISO_CODE
         , CASE WHEN CASE CM.CM_ISO_CODE WHEN 'X01' THEN 1001 ELSE CM_CAGE_CURRENCY_TYPE END < 1000 THEN 0
                ELSE CM_CAGE_CURRENCY_TYPE END AS LIABILITY_CAGE_CURRENCY_TYPE
         , SUM(CM.CM_VALUE) AS LIABILITY_VALUE
      FROM CAGE_METERS AS CM
     INNER JOIN CAGE_CONCEPTS AS CC ON CC.CC_CONCEPT_ID = CM.CM_CONCEPT_ID
      LEFT JOIN CURRENCY_EXCHANGE ON CM_ISO_CODE = CE_CURRENCY_ISO_CODE AND CE_TYPE = 0
     WHERE CC.CC_ENABLED = 1
       AND CC.CC_SHOW_IN_REPORT = 1
       AND CC.CC_IS_PROVISION = 1
       AND CM_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)
  GROUP BY CC.CC_CONCEPT_ID  
         , CC_DESCRIPTION
         , CASE WHEN CASE CM.CM_ISO_CODE WHEN 'X01' THEN 1001 ELSE CM_CAGE_CURRENCY_TYPE END < 1000 THEN 0
                ELSE CM_CAGE_CURRENCY_TYPE END
         , CE_CURRENCY_ORDER
         , CASE CM.CM_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CM.CM_ISO_CODE END
  ORDER BY CC.CC_CONCEPT_ID  
         , CASE WHEN CASE CM.CM_ISO_CODE WHEN 'X01' THEN 1001 ELSE CM_CAGE_CURRENCY_TYPE END < 1000 THEN 0
                ELSE CM_CAGE_CURRENCY_TYPE END
         , CE_CURRENCY_ORDER
         , CASE CM.CM_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CM.CM_ISO_CODE END

      
    -- TABLE[3]: Get session sumary
    SELECT CMD_ISO_CODE AS ISO_CODE
         , CASE WHEN CAGE_CURRENCY_TYPE = 99 THEN 0 ELSE CAGE_CURRENCY_TYPE END AS CAGE_CURRENCY_TYPE
         , ISNULL(ORIGIN, 0) AS ORIGIN
         , SUM(CMD_DEPOSITS) AS DEPOSITS
         , SUM(CMD_WITHDRAWALS) AS WITHDRAWALS     
      FROM (SELECT CASE CMD_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CMD_ISO_CODE END AS CMD_ISO_CODE
                 , CASE WHEN CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE CAGE_CURRENCY_TYPE END < 1000 THEN 0
                        ELSE CAGE_CURRENCY_TYPE END AS CAGE_CURRENCY_TYPE
                 , ORIGIN
                 , SUM(CMD_DEPOSITS) CMD_DEPOSITS
                 , SUM(CMD_WITHDRAWALS) CMD_WITHDRAWALS      
              FROM (SELECT CASE WHEN CMD_QUANTITY >= 0 THEN @BillsId
                                ELSE CMD_QUANTITY END AS ORIGIN
                         , CASE CMD_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CMD_ISO_CODE END AS CMD_ISO_CODE
                         , CASE WHEN CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE CMD_CAGE_CURRENCY_TYPE END < 1000 THEN 0
                                ELSE CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE ISNULL(CMD_CAGE_CURRENCY_TYPE, 0) END END  AS CAGE_CURRENCY_TYPE
                         , CMD_DENOMINATION
                         , CASE WHEN CGM_TYPE IN (100, 101, 103, 104) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY < 0 AND CMD_QUANTITY <> @TicketsId THEN CMD_DENOMINATION
                                                                                            WHEN CMD_QUANTITY = @TicketsId THEN 0
                                                                                            WHEN CMD_CAGE_CURRENCY_TYPE = @CageCurrencyType_ColorChips THEN CMD_QUANTITY
                                                                                       ELSE CMD_QUANTITY * CMD_DENOMINATION END), 0)
                                ELSE 0 END AS CMD_DEPOSITS
                         , CASE WHEN CGM_TYPE IN (0, 1, 4, 5) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY < 0 AND CMD_QUANTITY <> @TicketsId THEN CMD_DENOMINATION
                                                                                    WHEN CMD_QUANTITY = @TicketsId THEN 0
                                                                                    WHEN CMD_CAGE_CURRENCY_TYPE = @CageCurrencyType_ColorChips THEN CMD_QUANTITY
                                                                                    ELSE CMD_QUANTITY * CMD_DENOMINATION END), 0)
                                ELSE 0 END AS CMD_WITHDRAWALS
                      FROM CAGE_MOVEMENTS 
                INNER JOIN CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          
                     WHERE CGM_STATUS IN (2, 3, 4, 8, 9, 10) 
                        OR (CGM_STATUS = 0 AND CGM_TYPE = 0)
                  GROUP BY CASE WHEN CMD_QUANTITY >= 0 THEN @BillsId
                                ELSE CMD_QUANTITY END 
                         , CASE CMD_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CMD_ISO_CODE END 
                         , CASE WHEN CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE CMD_CAGE_CURRENCY_TYPE END < 1000 THEN 0
                                ELSE CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE ISNULL(CMD_CAGE_CURRENCY_TYPE, 0) END END 
                         , CMD_DENOMINATION
                         , CGM_TYPE

          -- Tickets 
                  UNION ALL
                  
                    SELECT @TicketsId AS ORIGIN
                         , ISNULL(TI_CUR0, @NationalIsoCode) AS CMD_ISO_CODE 
                         , ISNULL(CMD_CAGE_CURRENCY_TYPE, 0) AS CAGE_CURRENCY_TYPE                                   
                         , 1 AS CMD_DENOMINATION
                         , CASE WHEN CGM_TYPE IN (100, 101, 103, 104) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY = @TicketsId THEN 
                                                                                                 CASE WHEN TI_AMT0 IS NULL THEN TI_AMOUNT 
                                                                                                      ELSE TI_AMT0 END
                                                                                       ELSE 0 END), 0)
                                ELSE 0 END AS CMD_DEPOSITS
                         , CASE WHEN CGM_TYPE IN (0, 1, 4, 5) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY = @TicketsId THEN 
                                                                                         CASE WHEN TI_AMT0 IS NULL THEN TI_AMOUNT 
                                                                                              ELSE TI_AMT0 END
                                                                                    ELSE 0 END), 0)
                                ELSE 0 END AS CMD_WITHDRAWALS
                      FROM CAGE_MOVEMENTS 
                           INNER JOIN   CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          
                           INNER JOIN   TICKETS ON TICKETS.TI_COLLECTED_MONEY_COLLECTION = CAGE_MOVEMENTS.CGM_MC_COLLECTION_ID     
                     WHERE  (CGM_STATUS IN (2, 3, 4, 8, 9, 10) 
                            OR (CGM_STATUS = 0 AND CGM_TYPE = 0)) AND CMD_QUANTITY = @TicketsId  
                            AND CAGE_MOVEMENTS.CGM_TYPE <> @CageOperationType_FromTerminal
                     GROUP BY ISNULL(TI_CUR0, @NationalIsoCode) 
                         , ISNULL(CMD_CAGE_CURRENCY_TYPE, 0)                             
                         , CGM_TYPE 
                    ) AS T1
           GROUP BY ORIGIN
                  ,CASE CMD_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CMD_ISO_CODE END 
                 , CASE WHEN CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE CAGE_CURRENCY_TYPE END < 1000 THEN 0
                        ELSE CAGE_CURRENCY_TYPE END 
                 

              -- GET Terminal value-in amounts
              
              UNION ALL 
              
              SELECT ISNULL(TI_CUR0, @NationalIsoCode) AS CMD_ISO_CODE
                    , 0 AS CAGE_CURRENCY_TYPE
                    , @TicketsId AS ORIGIN
                    , ISNULL(SUM (CASE WHEN TI_AMT0 IS NULL THEN TI_AMOUNT 
                                       ELSE TI_AMT0 END),0)  AS DEPOSITS
                    , 0 AS WITHDRAWS
              FROM MONEY_COLLECTIONS
        INNER JOIN CAGE_MOVEMENTS ON CGM_MC_COLLECTION_ID = MC_COLLECTION_ID 
        INNER JOIN TICKETS ON TICKETS.TI_COLLECTED_MONEY_COLLECTION = CAGE_MOVEMENTS.CGM_MC_COLLECTION_ID            
             WHERE CAGE_MOVEMENTS.CGM_TYPE = @CAGEOPERATIONTYPE_FROMTERMINAL
               AND (CGM_STATUS IN (2, 3, 4, 8, 9, 10) 
                    OR (CGM_STATUS = 0 AND CGM_TYPE = 0))
             GROUP BY ISNULL(TI_CUR0, @NationalIsoCode) 

              UNION ALL
              
                SELECT ISNULL(TE_ISO_CODE, @NationalIsoCode) AS CMD_ISO_CODE
                    , 0 AS CAGE_CURRENCY_TYPE
                    , @BillsId AS ORIGIN
                    , MC_COLLECTED_BILL_AMOUNT + MC_COLLECTED_COIN_AMOUNT  AS DEPOSITS
                    , 0 AS WITHDRAWS
              FROM MONEY_COLLECTIONS
                  INNER JOIN CAGE_MOVEMENTS ON CGM_MC_COLLECTION_ID = MC_COLLECTION_ID 
          INNER JOIN TERMINALS ON TE_TERMINAL_ID = MC_TERMINAL_ID
             WHERE CAGE_MOVEMENTS.CGM_TYPE = @CageOperationType_FromTerminal
               AND (CGM_STATUS IN (2, 3, 4, 8, 9, 10) 
                    OR (CGM_STATUS = 0 AND CGM_TYPE = 0))
                          
          ) AS _TBL_GLB
  LEFT JOIN CURRENCY_EXCHANGE ON CMD_ISO_CODE = CE_CURRENCY_ISO_CODE AND CE_TYPE = 0
      WHERE CMD_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)             
   GROUP BY CAGE_CURRENCY_TYPE 
          , CE_CURRENCY_ORDER
          , CMD_ISO_CODE 
          , ORIGIN  
   ORDER BY CAGE_CURRENCY_TYPE 
          , CE_CURRENCY_ORDER
          , CMD_ISO_CODE 
          , ORIGIN DESC 


         
    -- TABLE[4]: Other System Concepts    
    SELECT * FROM (       
    SELECT CM_CONCEPT_ID
         , CC.CC_DESCRIPTION AS CM_DESCRIPTION
         , CASE CM_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CM.CM_ISO_CODE END AS CM_ISO_CODE
         , CASE CM_ISO_CODE WHEN 'X01' THEN 1001 ELSE CASE WHEN CM_CAGE_CURRENCY_TYPE < 1000 THEN 0 ELSE CM_CAGE_CURRENCY_TYPE END END AS CAGE_CURRENCY_TYPE
         , SUM(CM_VALUE) AS CM_VALUE
      FROM CAGE_METERS AS CM
           INNER JOIN CAGE_CONCEPTS AS CC ON CM.CM_CONCEPT_ID = CC.CC_CONCEPT_ID
     WHERE CM_SOURCE_TARGET_ID = 0
       AND CM_CONCEPT_ID IN (7, 8)
       AND CM_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)
  GROUP BY CM_CONCEPT_ID
         , CASE CM_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CM.CM_ISO_CODE END
         , CASE CM_ISO_CODE WHEN 'X01' THEN 1001 ELSE CASE WHEN CM_CAGE_CURRENCY_TYPE < 1000 THEN 0 ELSE CM_CAGE_CURRENCY_TYPE END END
         , CC.CC_DESCRIPTION
         ) AS A
  ORDER BY CM_CONCEPT_ID
         , CASE CM_ISO_CODE WHEN @NationalIsoCode THEN '000' ELSE CM_ISO_CODE END
         , CAGE_CURRENCY_TYPE
    
    -- TABLE[5]: Cage Counts
    SELECT ORIGIN AS ORIGIN
         , CASE CMD_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CMD_ISO_CODE END AS ISO_CODE
         , CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE CASE WHEN CAGE_CURRENCY_TYPE = 1 THEN 0 ELSE CAGE_CURRENCY_TYPE END END AS CAGE_CURRENCY_TYPE
         , ISNULL(SUM(CMD_DEPOSITS), 0) AS DEPOSITS
      FROM (SELECT CASE WHEN CMD_QUANTITY >= 0 THEN 0
                        ELSE CMD_QUANTITY END  AS ORIGIN
                 , CMD_ISO_CODE
                 , ISNULL(CMD_CAGE_CURRENCY_TYPE, 0) AS CAGE_CURRENCY_TYPE
                 , ISNULL(SUM (  CASE WHEN CMD_QUANTITY >= 0 THEN CMD_QUANTITY ELSE 1 END 
                               * CASE WHEN CGM_TYPE = 99 THEN -1 ELSE 1 END
                               * CASE WHEN CMD_CAGE_CURRENCY_TYPE = @CageCurrencyType_ColorChips THEN 1 ELSE CMD_DENOMINATION END), 0) AS CMD_DEPOSITS
              FROM CAGE_MOVEMENTS 
                   INNER JOIN CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          
             WHERE CGM_TYPE IN (99, 199)
          GROUP BY CMD_ISO_CODE, ISNULL(CMD_CAGE_CURRENCY_TYPE, 0), CMD_DENOMINATION, CGM_TYPE, CMD_QUANTITY
            ) AS T1           
   GROUP BY ORIGIN, CASE CMD_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CMD_ISO_CODE END 
         , CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE CASE WHEN CAGE_CURRENCY_TYPE = 1 THEN 0 ELSE CAGE_CURRENCY_TYPE END END 
           
    UNION ALL
              
    SELECT NULL AS ORIGIN
         , CASE CMD_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CMD_ISO_CODE END AS ISO_CODE
         , CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE CASE WHEN CAGE_CURRENCY_TYPE = 1 THEN 0 ELSE CAGE_CURRENCY_TYPE END END AS CAGE_CURRENCY_TYPE
         , ISNULL(SUM(CMD_DEPOSITS), 0) AS DEPOSITS
      FROM (SELECT CMD_ISO_CODE
                 , ISNULL(CMD_CAGE_CURRENCY_TYPE, 0) AS CAGE_CURRENCY_TYPE
                 , ISNULL(SUM (  CASE WHEN CMD_QUANTITY >= 0 THEN CMD_QUANTITY ELSE 1 END 
                               * CASE WHEN CGM_TYPE = 99 THEN -1 ELSE 1 END
                               * CASE WHEN CMD_CAGE_CURRENCY_TYPE = @CageCurrencyType_ColorChips THEN 1 ELSE CMD_DENOMINATION END), 0) AS CMD_DEPOSITS
              FROM CAGE_MOVEMENTS 
                   INNER JOIN CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          
             WHERE CGM_TYPE IN (99, 199)
          GROUP BY CMD_ISO_CODE, ISNULL(CMD_CAGE_CURRENCY_TYPE, 0), CMD_DENOMINATION, CGM_TYPE, CMD_QUANTITY
           ) AS T1           
  GROUP BY CASE CMD_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CMD_ISO_CODE END 
         , CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE CASE WHEN CAGE_CURRENCY_TYPE = 1 THEN 0 ELSE CAGE_CURRENCY_TYPE END END 
 

  END -- ELSE
  
  -- TABLE[6]: Stock Stored 
  SELECT @StockIsStored
  
  DROP TABLE #TMP_CURRENCIES_ISO_CODES
                   
                   
END -- CageGetGlobalReportData
GO

GRANT EXECUTE ON [dbo].[CageGetGlobalReportData] TO [wggui] WITH GRANT OPTION 
GO



--***** CageStockOnCloseSession 
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CageStockOnCloseSession]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[CageStockOnCloseSession]
GO

CREATE PROCEDURE [dbo].[CageStockOnCloseSession]
        @pCageSessionId NVARCHAR(MAX) = NULL
AS
BEGIN
  
  DECLARE @List VARCHAR(MAX) 

  DECLARE @TMP_STOCK_GEN TABLE
  (
     CGS_ISO_CODE NVARCHAR(MAX)                                        
     , CGS_DENOMINATION MONEY                                      
     , CGS_QUANTITY MONEY  
     , CGS_CAGE_CURRENCY_TYPE INT
     , SET_ID BIGINT
     , CGS_CHIP_ID BIGINT
     , COLOR INT
     , NAME NVARCHAR(50)
     , DRAWING NVARCHAR(50)
  )
  --Get Current Stock
  INSERT INTO @TMP_STOCK_GEN
  EXEC CageCurrentStock
  
  --Parse current Stock                                                
  SELECT  @List = COALESCE(@List + '', '') + RTRIM(LTRIM(CGS_ISO_CODE))      
      + ';'+ RTRIM(LTRIM(CAST(CGS_DENOMINATION AS VARCHAR)))       
      + ';'+ RTRIM(LTRIM(CAST(CGS_QUANTITY AS VARCHAR)))           
      + ';'+ RTRIM(LTRIM(CAST(CGS_CAGE_CURRENCY_TYPE AS VARCHAR))) 
      + ';'+ RTRIM(LTRIM(CAST(CGS_CHIP_ID AS VARCHAR))) 
      + '|'                                                        
   FROM @TMP_STOCK_GEN
   
   --Save current stock on session    
   UPDATE CAGE_SESSIONS                                                       
    SET CGS_CAGE_STOCK = @List                                              
   WHERE CGS_CAGE_SESSION_ID = @pCageSessionId                                     

END --CageStockOnCloseSession

GO
GRANT EXECUTE ON [dbo].[CageStockOnCloseSession] TO [wggui] WITH GRANT OPTION 
GO

--***** CageCurrentStock 
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CageCurrentStock]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CageCurrentStock]
GO

CREATE PROCEDURE [dbo].[CageCurrentStock]
AS
BEGIN
  --Get current stock     
  SELECT   CGS_ISO_CODE                                          
         , CGS_DENOMINATION                                      
         , CGS_QUANTITY 
         , CGS_CAGE_CURRENCY_TYPE
         , SET_ID 
         , CGS_CHIP_ID
         , COLOR
         , NAME
         , DRAWING
  --       , CE_CURRENCY_ORDER 
    FROM ( SELECT   CGS_ISO_CODE                                          
                  , CGS_DENOMINATION                                      
                  , CGS_QUANTITY                     
                  , CASE WHEN CGS_DENOMINATION > 0 THEN ISNULL(CGS_CAGE_CURRENCY_TYPE,0) ELSE 99 END AS CGS_CAGE_CURRENCY_TYPE   
                  , -1 AS CGS_CHIP_ID  
                  , -1 AS SET_ID                
                  , '' AS COLOR
                  , '' AS NAME
                  , '' AS DRAWING
             FROM   CAGE_STOCK
        UNION ALL
           SELECT   C.CH_ISO_CODE AS CGS_ISO_CODE
                  , C.CH_DENOMINATION AS CGS_DENOMINATION
                  , CST.CHSK_QUANTITY AS CGS_QUANTITY
                  , C.CH_CHIP_TYPE AS CGS_CAGE_CURRENCY_TYPE
                  , C.CH_CHIP_ID AS CGS_CHIP_ID
                  , CSC_SET_ID AS SET_ID
                  , C.CH_COLOR AS COLOR
                  , C.CH_NAME AS NAME
                  , C.CH_DRAWING AS DRAWING
             FROM   CHIPS_STOCK AS CST
       INNER JOIN   CHIPS AS C ON CST.CHSK_CHIP_ID = C.CH_CHIP_ID
       INNER JOIN   CHIPS_SETS_CHIPS ON C.CH_CHIP_ID = CSC_CHIP_ID
                  ) AS _TBL 
    LEFT   JOIN CURRENCY_EXCHANGE ON CGS_ISO_CODE = CE_CURRENCY_ISO_CODE AND CE_TYPE = 0
   WHERE   CGS_QUANTITY > 0  OR CGS_DENOMINATION = -100  
   ORDER   BY CASE WHEN CGS_CAGE_CURRENCY_TYPE < 1000 THEN 0
                   WHEN CGS_CAGE_CURRENCY_TYPE = 1003 THEN 9999999
                   ELSE 1 END
         , CE_CURRENCY_ORDER
         , CGS_ISO_CODE
         , CGS_CAGE_CURRENCY_TYPE ASC
         , SET_ID ASC
         , CGS_DENOMINATION DESC
         , CASE WHEN CGS_DENOMINATION >= 0 THEN CGS_DENOMINATION 
                ELSE CGS_DENOMINATION * (-100000) END
END --CageCurrentStock

GO
GRANT EXECUTE ON [dbo].[CageCurrentStock] TO [wggui] WITH GRANT OPTION 
GO 

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CashierMovementsGroupedPerSessionId]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CashierMovementsGroupedPerSessionId]
GO

CREATE PROCEDURE [dbo].[CashierMovementsGroupedPerSessionId]
        @pCashierSessionId BIGINT     
  AS
BEGIN            
DECLARE @CashierSessionId NVARCHAR(MAX)
SET @CashierSessionId = CONVERT(VARCHAR, @pCashierSessionId)

  IF EXISTS (SELECT CM_SESSION_ID FROM cashier_movements_grouped_by_session_id WHERE CM_SESSION_ID = @pCashierSessionId)
  BEGIN
    DELETE FROM cashier_movements_grouped_by_session_id WHERE CM_SESSION_ID = @pCashierSessionId
  END
    
    INSERT INTO   CASHIER_MOVEMENTS_GROUPED_BY_SESSION_ID 
          ( CM_SESSION_ID
          , CM_TYPE
          , CM_SUB_TYPE
          , CM_TYPE_COUNT
          , CM_CURRENCY_ISO_CODE
          , CM_CURRENCY_DENOMINATION
          , CM_SUB_AMOUNT
          , CM_ADD_AMOUNT
          , CM_AUX_AMOUNT
          , CM_INITIAL_BALANCE
          , CM_FINAL_BALANCE
          , CM_CAGE_CURRENCY_TYPE
          )
    EXEC CashierMovementsGrouped_Select @CashierSessionId, NULL, NULL
    --UPDATE HISTORY FLAG IN CASHIER_SESSIONS
    UPDATE cashier_sessions SET cs_history = 1 WHERE cs_session_id = @pCashierSessionId

  
 END --CreateCashierMovementsGroupedPerSessionId
GO

IF OBJECT_ID (N'dbo.SP_Cashier_Sessions_Report', N'P') IS NOT NULL
   DROP PROCEDURE dbo.SP_Cashier_Sessions_Report;                 
GO

CREATE PROCEDURE [dbo].[SP_Cashier_Sessions_Report]
(  @pDateFrom DateTime
  , @pDateTo DateTime
  , @pReportMode as Int
  , @pShowSystemSessions as Bit
  , @pSystemUsersTypes as VarChar(4096)
  , @pSessionStatus as VarChar(4096)
  , @pUserId as Integer
  , @pCashierId as Integer
  , @pCurrencyISOCode as VarChar(3)
)
AS
BEGIN

  -- Members --
  DECLARE @_delimiter AS CHAR(1)
  SET @_delimiter = ','
  DECLARE @pSystemUsersTypesTable AS TABLE (SUT_ID INT, SUT_VALUE VARCHAR(50))
  DECLARE @pSessionsStatusTable AS TABLE (SUT_ID INT, SUT_VALUE VARCHAR(50))
  DECLARE @_table_sessions_ AS TABLE (  sess_id                      BIGINT 
                    , sess_name                    NVARCHAR(50) 
                    , sess_user_id                 INT      
                    , sess_full_name               NVARCHAR(50) 
                    , sess_opening_date            DATETIME 
                    , sess_closing_date            DATETIME  
                    , sess_cashier_id              INT 
                    , sess_ct_name                 NVARCHAR(50) 
                    , sess_status                  INT 
                    , sess_cs_limit_sale           MONEY
                    , sess_mb_limit_sale           MONEY
                    , sess_collected_amount        MONEY
                    , sess_gaming_day              DATETIME
                    , sess_te_iso_code             NVARCHAR(3)
                     ) 
                                 
  -- SYSTEM USER TYPES --
  IF @pShowSystemSessions = 0
  BEGIN
  INSERT INTO @pSystemUsersTypesTable SELECT * FROM dbo.SplitStringIntoTable(@pSystemUsersTypes, @_delimiter, DEFAULT)
  END

  -- SESSIONS STATUS
  INSERT INTO @pSessionsStatusTable SELECT * FROM dbo.SplitStringIntoTable(@pSessionStatus, @_delimiter, DEFAULT)

  IF @pReportMode = 0 OR @pReportMode = 2
     BEGIN -- Filter Dates on cashier session opening date  

    IF @pReportMode = 0
      BEGIN
      -- SESSIONS BY OPENING DATE --
      INSERT   INTO @_table_sessions_
      SELECT   DISTINCT(CS_SESSION_ID) AS SESS_ID
           , CS_NAME AS SESS_NAME
           , CS_USER_ID AS SESS_USER_ID
           , GU_FULL_NAME AS SESS_FULL_NAME
           , CS_OPENING_DATE AS SESS_OPENING_DATE
           , CS_CLOSING_DATE AS SESS_CLOSING_DATE
           , CS_CASHIER_ID AS SESS_CASHIER_ID
           , CT_NAME AS SESS_CT_NAME
           , CS_STATUS AS SESS_STATUS
           , CS_SALES_LIMIT AS SESS_CS_LIMIT_SALE
           , CS_MB_SALES_LIMIT AS SESS_MB_LIMIT_SALE
           , CS_COLLECTED_AMOUNT AS SESS_COLLECTED_AMOUNT
           , CS_GAMING_DAY AS SESS_GAMING_DAY
           , ISNULL(TE_ISO_CODE, @pCurrencyISOCode) AS SESS_TE_ISO_CODE 
        FROM   CASHIER_SESSIONS --WITH (INDEX (IX_cs_opening_date)) /* TODO: REVIEW INDEXES */
       INNER   JOIN GUI_USERS         ON CS_USER_ID    = GU_USER_ID
       INNER   JOIN CASHIER_TERMINALS ON CS_CASHIER_ID = CT_CASHIER_ID
        LEFT   JOIN TERMINALS         ON CT_TERMINAL_ID = TE_TERMINAL_ID   
       WHERE   (CS_OPENING_DATE >= @pDateFrom AND CS_OPENING_DATE <= @pDateTo)
        --   usuarios sistema s/n & user 
         AND   (      GU_USER_TYPE NOT IN ( SELECT SUT_VALUE FROM @pSystemUsersTypesTable )
            AND   CS_USER_ID = (CASE WHEN @pUserId IS NULL THEN CS_USER_ID ELSE @pUserId END) )
        --   cashier id
         AND   CS_CASHIER_ID = (CASE WHEN @pCashierId IS NULL THEN CS_CASHIER_ID ELSE @pCashierId END) 
        --   session status
         AND   CS_STATUS IN ( SELECT SUT_VALUE FROM @pSessionsStatusTable )
       ORDER   BY CS_OPENING_DATE DESC  
      END
    ELSE
      BEGIN
      -- @pReportMode = 2
      -- SESSIONS BY GAMING DAY --
      INSERT   INTO @_table_sessions_
      SELECT   DISTINCT(CS_SESSION_ID) AS SESS_ID
           , CS_NAME AS SESS_NAME
           , CS_USER_ID AS SESS_USER_ID
           , GU_FULL_NAME AS SESS_FULL_NAME
           , CS_OPENING_DATE AS SESS_OPENING_DATE
           , CS_CLOSING_DATE AS SESS_CLOSING_DATE
           , CS_CASHIER_ID AS SESS_CASHIER_ID
           , CT_NAME AS SESS_CT_NAME
           , CS_STATUS AS SESS_STATUS
           , CS_SALES_LIMIT AS SESS_CS_LIMIT_SALE
           , CS_MB_SALES_LIMIT AS SESS_MB_LIMIT_SALE
           , CS_COLLECTED_AMOUNT AS SESS_COLLECTED_AMOUNT
           , CS_GAMING_DAY AS SESS_GAMING_DAY
           , ISNULL(TE_ISO_CODE, @pCurrencyISOCode) AS SESS_TE_ISO_CODE 
        FROM   CASHIER_SESSIONS --WITH (INDEX (IX_cs_opening_date)) /* TODO: REVIEW INDEXES */
       INNER   JOIN GUI_USERS         ON CS_USER_ID    = GU_USER_ID
       INNER   JOIN CASHIER_TERMINALS ON CS_CASHIER_ID = CT_CASHIER_ID
        LEFT   JOIN TERMINALS         ON CT_TERMINAL_ID = TE_TERMINAL_ID   
       WHERE   (CS_GAMING_DAY >= @pDateFrom AND CS_GAMING_DAY <= @pDateTo)
        --   usuarios sistema s/n & user 
         AND   (      GU_USER_TYPE NOT IN ( SELECT SUT_VALUE FROM @pSystemUsersTypesTable )
            AND   CS_USER_ID = (CASE WHEN @pUserId IS NULL THEN CS_USER_ID ELSE @pUserId END) )
        --   cashier id
         AND   CS_CASHIER_ID = (CASE WHEN @pCashierId IS NULL THEN CS_CASHIER_ID ELSE @pCashierId END) 
        --   session status
         AND   CS_STATUS IN ( SELECT SUT_VALUE FROM @pSessionsStatusTable )
       ORDER   BY CS_GAMING_DAY DESC  
      END

    -- MOVEMENTS
    -- By Movements in Historical Data --    
    SELECT   CM_SESSION_ID
         , CM_TYPE
         , CM_SUB_TYPE
         , ISNULL(CM_CURRENCY_ISO_CODE, @pCurrencyISOCode) AS CM_CURRENCY_ISO_CODE
         , CM_CURRENCY_DENOMINATION
         , CM_TYPE_COUNT
         , CM_SUB_AMOUNT
         , CM_ADD_AMOUNT
         , CM_AUX_AMOUNT
         , CM_INITIAL_BALANCE
         , CM_FINAL_BALANCE
         , CM_CAGE_CURRENCY_TYPE
      FROM   CASHIER_MOVEMENTS_GROUPED_BY_SESSION_ID
     WHERE   CM_SESSION_ID IN (SELECT SESS_ID FROM @_table_sessions_)

     END
  ELSE
    BEGIN -- Filter Dates on cashier movements date (@pReportMode = 1)
       
      -- MOVEMENTS
      -- By Cashier_Movements --   
      SELECT * 
      INTO #CASHIER_MOVEMENTS
      FROM ( 
        SELECT   CM_DATE
             , CM_SESSION_ID
             , CM_TYPE
             , 0 as CM_SUB_TYPE
             , ISNULL(CM_CURRENCY_ISO_CODE, @pCurrencyISOCode) AS CM_CURRENCY_ISO_CODE
             , CM_CURRENCY_DENOMINATION
             , 1 as CM_TYPE_COUNT
             , CM_SUB_AMOUNT
             , CM_ADD_AMOUNT
             , CM_AUX_AMOUNT
             , CM_INITIAL_BALANCE
             , CM_FINAL_BALANCE
             , ISNULL(CM_CAGE_CURRENCY_TYPE, 0) AS CM_CAGE_CURRENCY_TYPE
          FROM   CASHIER_MOVEMENTS 
         WHERE   CM_DATE >= @pDateFrom AND CM_DATE < @pDateTo
         UNION   ALL
        SELECT   MBM_DATETIME AS CM_DATE
             , MBM_CASHIER_SESSION_ID AS CM_SESSION_ID
             , MBM_TYPE AS CM_TYPE
             , 1 AS CM_SUB_TYPE
             , @pCurrencyISOCode AS CM_CURRENCY_ISO_CODE
             , 0 AS CM_CURRENCY_DENOMINATION
             , 1 AS CM_TYPE_COUNT
             , ISNULL(MBM_SUB_AMOUNT, 0) AS CM_SUB_AMOUNT
             , ISNULL(MBM_ADD_AMOUNT, 0) AS CM_ADD_AMOUNT
             , 0 AS CM_AUX_AMOUNT
             , 0 AS CM_INITIAL_BALANCE
             , 0 AS CM_FINAL_BALANCE
             , 0 AS CM_CAGE_CURRENCY_TYPE
          FROM   MB_MOVEMENTS 
         WHERE   MBM_DATETIME >= @pDateFrom AND MBM_DATETIME < @pDateTo
       ) AS MOVEMENTS
       
      CREATE NONCLUSTERED INDEX IX_CM_SESSION_ID ON #CASHIER_MOVEMENTS(CM_SESSION_ID)

      -- SESSIONS --  
      INSERT   INTO @_table_sessions_  
      SELECT   DISTINCT(CS_SESSION_ID) AS SESS_ID  
         , CS_NAME AS SESS_NAME  
         , CS_USER_ID AS SESS_USER_ID  
         , GU_FULL_NAME AS SESS_FULL_NAME  
         , CS_OPENING_DATE AS SESS_OPENING_DATE  
             , CS_CLOSING_DATE AS SESS_CLOSING_DATE  
         , CS_CASHIER_ID AS SESS_CASHIER_ID  
         , CT_NAME AS SESS_CT_NAME  
         , CS_STATUS AS SESS_STATUS  
         , CS_SALES_LIMIT AS SESS_CS_LIMIT_SALE  
         , CS_MB_SALES_LIMIT AS SESS_MB_LIMIT_SALE  
         , CS_COLLECTED_AMOUNT AS SESS_COLLECTED_AMOUNT  
         , CS_GAMING_DAY AS SESS_GAMING_DAY  
             , ISNULL(TE_ISO_CODE, @pCurrencyISOCode) AS SESS_TE_ISO_CODE  
      FROM   CASHIER_SESSIONS --WITH (INDEX (IX_cs_opening_date)) /* TODO: REVIEW INDEXES */  
       INNER   JOIN GUI_USERS         ON CS_USER_ID    = GU_USER_ID  
       INNER   JOIN CASHIER_TERMINALS ON CS_CASHIER_ID = CT_CASHIER_ID  
        LEFT   JOIN TERMINALS         ON CT_TERMINAL_ID = TE_TERMINAL_ID   
       WHERE   CS_SESSION_ID IN (SELECT CM_SESSION_ID FROM #CASHIER_MOVEMENTS)  
        --   usuarios sistema s/n & user   
       AND   (      GU_USER_TYPE NOT IN ( SELECT SUT_VALUE FROM @pSystemUsersTypesTable )  
          AND   CS_USER_ID = (CASE WHEN @pUserId IS NULL THEN CS_USER_ID ELSE @pUserId END) )  
        --   cashier id  
       AND   CS_CASHIER_ID = (CASE WHEN @pCashierId IS NULL THEN CS_CASHIER_ID ELSE @pCashierId END)   
        --   session status  
       AND   CS_STATUS IN ( SELECT SUT_VALUE FROM @pSessionsStatusTable )  
       ORDER   BY CS_OPENING_DATE DESC    
       
       SELECT * FROM #CASHIER_MOVEMENTS INNER JOIN @_table_sessions_ ON SESS_ID = CM_SESSION_ID  
       
       DROP TABLE #CASHIER_MOVEMENTS        
    
     END   
    
  -- SESSIONS with CAGE INFO  
  SELECT   SESS_ID  
       , SESS_NAME  
       , SESS_USER_ID  
       , SESS_FULL_NAME  
       , SESS_OPENING_DATE  
       , SESS_CLOSING_DATE  
       , SESS_CASHIER_ID  
       , SESS_CT_NAME  
       , SESS_STATUS  
       , SESS_CS_LIMIT_SALE  
       , SESS_MB_LIMIT_SALE  
       , SESS_COLLECTED_AMOUNT   
       , X.CGS_CAGE_SESSION_ID AS CGS_CAGE_SESSION_ID  
       , X.CGS_SESSION_NAME AS CGS_SESSION_NAME   
       , SESS_GAMING_DAY  
       , SESS_TE_ISO_CODE 
    FROM   @_table_sessions_  
    LEFT   JOIN ( SELECT   CGS_SESSION_NAME  
               , CGM_CASHIER_SESSION_ID  
               , CGS_CAGE_SESSION_ID   
            FROM   CAGE_MOVEMENTS AS CM  
           INNER   JOIN CAGE_SESSIONS ON CM.CGM_CAGE_SESSION_ID = CGS_CAGE_SESSION_ID  
           WHERE   CM.CGM_MOVEMENT_ID = (SELECT    TOP 1 CGM.CGM_MOVEMENT_ID  
                           FROM    CAGE_MOVEMENTS AS CGM   
                          WHERE    CGM.CGM_CASHIER_SESSION_ID = CM.CGM_CASHIER_SESSION_ID)  
           ) AS X ON X.CGM_CASHIER_SESSION_ID = [@_table_sessions_].SESS_ID 
                   
  -- SESSIONS CURRENCIES INFORMATION
  IF @pReportMode = 0 OR @pReportMode = 2
    SELECT   CSC_SESSION_ID
         , CSC_ISO_CODE
         , CSC_TYPE
         , CSC_BALANCE
         , CSC_COLLECTED 
      FROM   CASHIER_SESSIONS_BY_CURRENCY 
     INNER JOIN @_table_sessions_ ON SESS_ID = CSC_SESSION_ID

END -- End Procedure
GO

GRANT EXECUTE ON [dbo].[SP_Cashier_Sessions_Report] TO [wggui] WITH GRANT OPTION
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CashierMovementsHistory]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CashierMovementsHistory]
GO

CREATE PROCEDURE [dbo].[CashierMovementsHistory]        
                        @pSessionId             BIGINT
                       ,@MovementId             BIGINT
                       ,@pType                  INT    
                       ,@pSubType               INT
                       ,@pInitialBalance        MONEY 
                       ,@pAddAmount             MONEY
                       ,@pSubAmmount            MONEY
                       ,@pFinalBalance          MONEY
                       ,@pCurrencyCode          NVARCHAR(3)
                       ,@pAuxAmount             MONEY
                       ,@pCurrencyDenomination  MONEY
                       ,@pCurrencyCageType      INT
AS
BEGIN 
  DECLARE @_idx_try int
  DECLARE @_row_count int

  SET @pCurrencyDenomination = ISNULL(@pCurrencyDenomination, 0)
  SET @pCurrencyCageType     = ISNULL(@pCurrencyCageType, 0)
  
/******** Grouped by ID ***************/

  SET @_idx_try = 0

  WHILE @_idx_try < 2
  BEGIN
    UPDATE   CASHIER_MOVEMENTS_GROUPED_BY_SESSION_ID
       SET   CM_TYPE_COUNT            = CM_TYPE_COUNT      + 1      
           , CM_SUB_AMOUNT            = CM_SUB_AMOUNT      + ISNULL(@pSubAmmount, 0)
           , CM_ADD_AMOUNT            = CM_ADD_AMOUNT      + ISNULL(@pAddAmount, 0)
           , CM_AUX_AMOUNT            = CM_AUX_AMOUNT      + ISNULL(@pAuxAmount, 0)
           , CM_INITIAL_BALANCE       = CM_INITIAL_BALANCE + ISNULL(@pInitialBalance, 0)
           , CM_FINAL_BALANCE         = CM_FINAL_BALANCE   + ISNULL(@pFinalBalance, 0)
     WHERE   CM_SESSION_ID            = @pSessionId 
       AND   CM_TYPE                  = @pType 
       AND   CM_SUB_TYPE              = @pSubType 
       AND   CM_CURRENCY_ISO_CODE     = @pCurrencyCode
       AND   CM_CURRENCY_DENOMINATION = @pCurrencyDenomination
       AND   CM_CAGE_CURRENCY_TYPE    = @pCurrencyCageType

    SET @_row_count = @@ROWCOUNT
   
    IF @_row_count = 1 BREAK

    IF @_idx_try = 1
    BEGIN
      RAISERROR ('Update into historicized table failed. Table = [cashier_movements_grouped_by_session_id]', 11, 0) 
    END

    BEGIN TRY
      -- Entry doesn't exists, lets insert that
      INSERT INTO   CASHIER_MOVEMENTS_GROUPED_BY_SESSION_ID
                  ( CM_SESSION_ID
                  , CM_TYPE
                  , CM_SUB_TYPE
                  , CM_TYPE_COUNT
                  , CM_CURRENCY_ISO_CODE
                  , CM_CURRENCY_DENOMINATION
                  , CM_SUB_AMOUNT
                  , CM_ADD_AMOUNT
                  , CM_AUX_AMOUNT
                  , CM_INITIAL_BALANCE
                  , CM_FINAL_BALANCE
                  , CM_CAGE_CURRENCY_TYPE)
           VALUES
                  ( @pSessionId
                  , @pType        
                  , @pSubType
                  , 1  
                  , @pCurrencyCode
                  , @pCurrencyDenomination
                  , ISNULL(@pSubAmmount, 0)
                  , ISNULL(@pAddAmount, 0)
                  , ISNULL(@pAuxAmount, 0)             
                  , ISNULL(@pInitialBalance, 0)
                  , ISNULL(@pFinalBalance, 0)
                  , ISNULL(@pCurrencyCageType, 0))

      SET @_row_count = @@ROWCOUNT

      -- Update history flag in CASHIER_SESSIONS
      -- UPDATE CASHIER_SESSIONS SET CS_HISTORY = 1 WHERE CS_SESSION_ID = @pSessionId

      -- All ok, exit WHILE      
      BREAK

    END TRY
    BEGIN CATCH
    END CATCH

    SET @_idx_try = @_idx_try + 1
  END
  
  IF @_row_count = 0
  BEGIN
    RAISERROR ('Insert into historicized table failed. Table = [cashier_movements_grouped_by_session_id]', 11, 0) 
  END

  -- AJQ & RCI & RMS 10-FEB-2014: Movements historicized by hour are not inserted here. They serialize all workers and reduce performance.
  --                              A pending cashier movement to historify will be inserted in table CASHIER_MOVEMENTS_PENDING_HISTORY.
  --                              Later a thread will historify these pending movements.
  INSERT INTO CASHIER_MOVEMENTS_PENDING_HISTORY (CMPH_MOVEMENT_ID, CMPH_SUB_TYPE) VALUES (@MovementId, @pSubType) 

END --[CashierMovementsHistory]
GO

GRANT EXECUTE ON [dbo].[CashierMovementsHistory] TO [wggui] WITH GRANT OPTION
GO

IF OBJECT_ID (N'dbo.SP_GenerateHistoricalMovements_ByHour', N'P') IS NOT NULL
    DROP PROCEDURE dbo.SP_GenerateHistoricalMovements_ByHour;                 
GO  

CREATE PROCEDURE [dbo].[SP_GenerateHistoricalMovements_ByHour]
(
  @pMaxItemsPerLoop Integer, 
  @pCurrecyISOCode VarChar(20)
) 
AS
BEGIN

  -- Declarations
  DECLARE @_max_movements_per_loop Integer
  DECLARE @_total_movements_count   Integer                                   -- To get the total number of items pending to historize
  DECLARE @_updated_movements_count Integer                                  -- To get the number of historized movements
  DECLARE @_tmp_updating_movements  TABLE  (CMPH_MOVEMENT_ID BigInt,         -- To get the movements id's of items to historize 
                        CMPH_SUB_TYPE Integer)   
  DECLARE @_tmp_updated_movements   TABLE  (UM_DATE DateTime,                -- To get the updated data
                        UM_TYPE Integer, 
                        UM_SUBTYPE Integer, 
                        UM_CURRENCY_ISO_CODE NVarChar(3), 
                        UM_CURRENCY_DENOMINATION Money,
                        UM_CAGE_CURRENCY_TYPE Integer )

  -- Initialize max items per loop
  SET @_max_movements_per_loop = ISNULL(@pMaxItemsPerLoop, 2000) 

  -- Get total movements pending of historize
  SELECT   @_total_movements_count = COUNT(*) 
    FROM   CASHIER_MOVEMENTS_PENDING_HISTORY 

  -- If there are no items in pending movements table
  IF @_total_movements_count = 0
  BEGIN
     -- Return number of updated movements, and total number of movements
     SELECT CAST(0 AS Integer), CAST(0 AS Integer)
     
     RETURN
  END

  -- Get the movements id's to be updated
  INSERT   INTO @_tmp_updating_movements
  SELECT   TOP(@_max_movements_per_loop) 
       CMPH_MOVEMENT_ID
       , CMPH_SUB_TYPE
    FROM   CASHIER_MOVEMENTS_PENDING_HISTORY 

  -- Obtain the number of movements to update
  SELECT   @_updated_movements_count = COUNT(*) 
    FROM   @_tmp_updating_movements 

  -- If there are items to update process it
  IF @_updated_movements_count = 0
  BEGIN
     -- Return number of updated movements, and total number of movements
     SELECT CAST(0 AS Integer), @_total_movements_count 
     
     RETURN
  END

  -- DELETE THE TO BE UPDATED REGS FROM PENDING
  DELETE   X
    FROM   CASHIER_MOVEMENTS_PENDING_HISTORY AS X
   INNER   JOIN @_tmp_updating_movements AS Z ON Z.CMPH_MOVEMENT_ID = X.CMPH_MOVEMENT_ID AND Z.CMPH_SUB_TYPE = X.CMPH_SUB_TYPE

  -- Create a temporary table with grouped items by PK for the CASHIER_MOVEMENTS (SUB_TYPE = 0)
  SELECT   X.CM_DATE
       , X.CM_TYPE
       , X.CM_SUB_TYPE
       , X.CM_CURRENCY_ISO_CODE
       , X.CM_CURRENCY_DENOMINATION
       , X.CM_TYPE_COUNT
       , X.CM_SUB_AMOUNT
       , X.CM_ADD_AMOUNT
       , X.CM_AUX_AMOUNT
       , X.CM_INITIAL_BALANCE
       , X.CM_FINAL_BALANCE
       , X.CM_CAGE_CURRENCY_TYPE
    INTO   #TMP_MOVEMENTS_HISTORIZE_PENDING
    FROM
       (
       SELECT   DATEADD(HOUR, DATEDIFF(HOUR, 0, CM_DATE), 0) AS CM_DATE
          , CM_TYPE
          , 0 AS CM_SUB_TYPE
          , ISNULL(CM_CURRENCY_ISO_CODE, @pCurrecyISOCode) AS CM_CURRENCY_ISO_CODE
          , ISNULL(CM_CURRENCY_DENOMINATION, 0)            AS CM_CURRENCY_DENOMINATION
          , COUNT(CM_TYPE)          AS CM_TYPE_COUNT
          , SUM(CM_SUB_AMOUNT)      AS CM_SUB_AMOUNT
          , SUM(CM_ADD_AMOUNT)      AS CM_ADD_AMOUNT
          , SUM(CM_AUX_AMOUNT)      AS CM_AUX_AMOUNT
          , SUM(CM_INITIAL_BALANCE) AS CM_INITIAL_BALANCE
          , SUM(CM_FINAL_BALANCE)   AS CM_FINAL_BALANCE
          , ISNULL(CM_CAGE_CURRENCY_TYPE, 0)               AS CM_CAGE_CURRENCY_TYPE
         FROM   @_tmp_updating_movements
        INNER   JOIN CASHIER_MOVEMENTS ON CMPH_MOVEMENT_ID = CM_MOVEMENT_ID
        WHERE   CMPH_SUB_TYPE = 0      -- Cashier movements
       GROUP BY   DATEADD(HOUR, DATEDIFF(HOUR, 0, CM_DATE), 0)
          , CM_TYPE
          , ISNULL(CM_CURRENCY_ISO_CODE, @pCurrecyISOCode)
          , ISNULL(CM_CURRENCY_DENOMINATION, 0)
          , ISNULL(CM_CAGE_CURRENCY_TYPE, 0)
       ) AS X 

  ---- Add to the temporary table items by PK for the MOBILE_BANK_MOVEMENTS (SUB_TYPE = 1)
  INSERT INTO   #TMP_MOVEMENTS_HISTORIZE_PENDING
     SELECT   DATEADD(HOUR, DATEDIFF(HOUR, 0, MBM_DATETIME), 0) AS CM_DATE
        , MBM_TYPE   AS CM_TYPE
        , 1          AS CM_SUB_TYPE
        , @pCurrecyISOCode AS CM_CURRENCY_ISO_CODE
        , 0                AS CM_CURRENCY_DENOMINATION
        , COUNT(MBM_TYPE)  AS CM_TYPE_COUNT
        , SUM(MBM_SUB_AMOUNT) AS CM_SUB_AMOUNT
        , SUM(MBM_ADD_AMOUNT) AS CM_ADD_AMOUNT
        , 0 AS CM_AUX_AMOUNT
        , 0 AS CM_INITIAL_BALANCE
        , 0 AS CM_FINAL_BALANCE
        , 0 AS CM_CAGE_CURRENCY_TYPE
       FROM   @_tmp_updating_movements
      INNER   JOIN MB_MOVEMENTS ON CMPH_MOVEMENT_ID = MBM_MOVEMENT_ID
      WHERE   CMPH_SUB_TYPE = 1       -- Mobile Bank movements
     GROUP BY   DATEADD(HOUR, DATEDIFF(HOUR, 0, MBM_DATETIME), 0)
        , MBM_TYPE 

  -- Update the items already in database triggering the updates into a temporary table of updated elements
  UPDATE   CASHIER_MOVEMENTS_GROUPED_BY_HOUR
     SET   CM_TYPE_COUNT      = T.CM_TYPE_COUNT       + S.CM_TYPE_COUNT
       , CM_SUB_AMOUNT      = T.CM_SUB_AMOUNT       + ISNULL(S.CM_SUB_AMOUNT, 0)
       , CM_ADD_AMOUNT      = T.CM_ADD_AMOUNT       + ISNULL(S.CM_ADD_AMOUNT, 0)
       , CM_AUX_AMOUNT      = T.CM_AUX_AMOUNT       + ISNULL(S.CM_AUX_AMOUNT, 0)
       , CM_INITIAL_BALANCE = T.CM_INITIAL_BALANCE  + ISNULL(S.CM_INITIAL_BALANCE, 0)
       , CM_FINAL_BALANCE   = T.CM_FINAL_BALANCE    + ISNULL(S.CM_FINAL_BALANCE, 0)
  OUTPUT   INSERTED.CM_DATE
       , INSERTED.CM_TYPE
       , INSERTED.CM_SUB_TYPE
       , INSERTED.CM_CURRENCY_ISO_CODE
       , INSERTED.CM_CURRENCY_DENOMINATION
       , INSERTED.CM_CAGE_CURRENCY_TYPE
    INTO   @_tmp_updated_movements(UM_DATE, UM_TYPE, UM_SUBTYPE, UM_CURRENCY_ISO_CODE, UM_CURRENCY_DENOMINATION, UM_CAGE_CURRENCY_TYPE)
    FROM   CASHIER_MOVEMENTS_GROUPED_BY_HOUR T
   INNER   JOIN #TMP_MOVEMENTS_HISTORIZE_PENDING S ON T.CM_DATE                  = S.CM_DATE
                          AND T.CM_TYPE                  = S.CM_TYPE
                          AND T.CM_SUB_TYPE              = S.CM_SUB_TYPE
                          AND T.CM_CURRENCY_ISO_CODE     = S.CM_CURRENCY_ISO_CODE
                          AND T.CM_CURRENCY_DENOMINATION = S.CM_CURRENCY_DENOMINATION 
                          AND T.CM_CAGE_CURRENCY_TYPE    = S.CM_CAGE_CURRENCY_TYPE

  -- Insert the elements not previously updated (update items not present in temporary updated items)
  INSERT INTO   CASHIER_MOVEMENTS_GROUPED_BY_HOUR
         (   CM_DATE
         , CM_TYPE
         , CM_SUB_TYPE
         , CM_CURRENCY_ISO_CODE
         , CM_CURRENCY_DENOMINATION
         , CM_TYPE_COUNT
         , CM_SUB_AMOUNT     
         , CM_ADD_AMOUNT      
         , CM_AUX_AMOUNT     
         , CM_INITIAL_BALANCE 
         , CM_FINAL_BALANCE  
         , CM_CAGE_CURRENCY_TYPE )
     SELECT   CM_DATE
        , CM_TYPE
        , CM_SUB_TYPE
        , CM_CURRENCY_ISO_CODE
        , CM_CURRENCY_DENOMINATION
        , CM_TYPE_COUNT
        , CM_SUB_AMOUNT     
        , CM_ADD_AMOUNT      
        , CM_AUX_AMOUNT     
        , CM_INITIAL_BALANCE 
        , CM_FINAL_BALANCE
        , CM_CAGE_CURRENCY_TYPE 
       FROM   #TMP_MOVEMENTS_HISTORIZE_PENDING 
      WHERE   NOT EXISTS(SELECT   1
                 FROM   @_tmp_updated_movements NTI
                WHERE   NTI.UM_DATE                  = CM_DATE
                AND   NTI.UM_TYPE                  = CM_TYPE
                AND   NTI.UM_SUBTYPE               = CM_SUB_TYPE
                AND   NTI.UM_CURRENCY_ISO_CODE     = CM_CURRENCY_ISO_CODE
                AND   NTI.UM_CURRENCY_DENOMINATION = CM_CURRENCY_DENOMINATION
                AND   NTI.UM_CAGE_CURRENCY_TYPE    = CM_CAGE_CURRENCY_TYPE) 

  -- Get total movements pending of historize
  SELECT   @_total_movements_count = COUNT(*) 
    FROM   CASHIER_MOVEMENTS_PENDING_HISTORY 
    
  -- Return values
  SELECT @_updated_movements_count, @_total_movements_count

  -- Erase the temporary data
  DROP TABLE #TMP_MOVEMENTS_HISTORIZE_PENDING 
  --

END  -- End Procedure
GO

-- Permissions
GRANT EXECUTE ON [dbo].[SP_GenerateHistoricalMovements_ByHour] TO [wggui] WITH GRANT OPTION ;
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CageCreateMeters]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[CageCreateMeters]
GO

CREATE PROCEDURE [dbo].[CageCreateMeters]
        @pSourceTargetId BIGINT
      , @pConceptId BIGINT
      , @pCageSessionId BIGINT = NULL
      , @CreateMetersOption INT
      , @CurrencyForced VARCHAR(3) =  NULL
  AS
BEGIN   

  -- @@CreateMetersOption  =    0 - Create in cage_meters and cage_session_meters
  --                       =    1 - Create in cage_meters only
  --                       =    2 - Create in cage_session_meters only 
  --                       =    3 - Create in cage_meters and all opened cage sessions 


  DECLARE @Currencies VARCHAR(100)
  DECLARE @OnlyNationalCurrency BIT
  DECLARE @CageSessionId BIGINT
	DECLARE @_dual_currency_enabled BIT
	DECLARE @_cage_currency_type_others INT
                
  SET @OnlyNationalCurrency = 0
  SET @_cage_currency_type_others = 99  -- Others
                
  SELECT @OnlyNationalCurrency = CSTC_ONLY_NATIONAL_CURRENCY 
    FROM CAGE_SOURCE_TARGET_CONCEPTS 
   WHERE CSTC_SOURCE_TARGET_ID = @pSourceTargetId
     AND  CSTC_CONCEPT_ID = @pConceptId
    
  -- Terminals: ONLY NATIONAL CURRENCY FORCED 
	SELECT @_dual_currency_enabled = CAST(ISNULL(GP_KEY_VALUE,'0') AS BIT) FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'FloorDualCurrency' AND GP_SUBJECT_KEY = 'Enabled'
  SET @_dual_currency_enabled = ISNULL(@_dual_currency_enabled,0)

  IF @pSourceTargetId = 12
	begin
		if @_dual_currency_enabled = 1
			SET @OnlyNationalCurrency = 0
		else
			SET @OnlyNationalCurrency = 1
	end

  -- Get national currency
  IF @OnlyNationalCurrency = 1 AND @CurrencyForced IS NULL
  BEGIN 
    SELECT @Currencies = GP_KEY_VALUE 
      FROM GENERAL_PARAMS 
     WHERE GP_GROUP_KEY = 'RegionalOptions' 
       AND GP_SUBJECT_KEY = 'CurrencyISOCode'
  END
  
  -- Get accepted currencies
  ELSE
  BEGIN
    IF @CurrencyForced IS NOT NULL
      SET @Currencies = @CurrencyForced
    ELSE
      SELECT @Currencies = GP_KEY_VALUE 
        FROM GENERAL_PARAMS 
       WHERE GP_GROUP_KEY = 'RegionalOptions' 
         AND GP_SUBJECT_KEY = 'CurrenciesAccepted'
  END
    
  -- Split currencies ISO codes
  SELECT * INTO #TMP_CURRENCIES_ISO_CODES FROM 
  (  SELECT   DISTINCT CGC_ISO_CODE AS CURRENCY_ISO_CODE
            , CGC_CAGE_CURRENCY_TYPE AS CAGE_CURRENCY_TYPE 
       FROM   CAGE_CURRENCIES 
      WHERE   CGC_ISO_CODE IN (SELECT SST_VALUE FROM [SplitStringIntoTable] (@Currencies, ';', 1))
      UNION   ALL
     SELECT   DISTINCT CGC_ISO_CODE AS CURRENCY_ISO_CODE
            , @_cage_currency_type_others AS CAGE_CURRENCY_TYPE 
       FROM   CAGE_CURRENCIES 
      WHERE   CGC_ISO_CODE IN (SELECT SST_VALUE FROM [SplitStringIntoTable] (@Currencies, ';', 1))
      UNION   ALL
     SELECT   DISTINCT CHS_ISO_CODE AS CURRENCY_ISO_CODE
            , CHS_CHIP_TYPE AS CAGE_CURRENCY_TYPE  
       FROM   CHIPS_SETS  ) AS A

  -- Create a new meters for each currency
  DECLARE @CurrentCurrency VARCHAR(3)
  DECLARE @CageCurrencyType INT

              
  DECLARE Curs_CageCreateMeters CURSOR FOR 
   SELECT CURRENCY_ISO_CODE, CAGE_CURRENCY_TYPE
    FROM #TMP_CURRENCIES_ISO_CODES 
    
  SET NOCOUNT ON;

  OPEN Curs_CageCreateMeters

  FETCH NEXT FROM Curs_CageCreateMeters INTO @CurrentCurrency, @CageCurrencyType
  
  WHILE @@FETCH_STATUS = 0
  BEGIN
                
    -- CAGE METERS --
    IF @CreateMetersOption = 0 OR @CreateMetersOption = 1 OR @CreateMetersOption = 3
    BEGIN
      IF NOT EXISTS (SELECT TOP 1 CM_VALUE 
                       FROM CAGE_METERS 
                      WHERE CM_SOURCE_TARGET_ID = @pSourceTargetId
                        AND CM_CONCEPT_ID = @pConceptId
                        AND CM_ISO_CODE = @CurrentCurrency
                        AND CM_CAGE_CURRENCY_TYPE = @CageCurrencyType)
        INSERT INTO CAGE_METERS
                  ( CM_SOURCE_TARGET_ID
                  , CM_CONCEPT_ID
                  , CM_ISO_CODE
                  , CM_CAGE_CURRENCY_TYPE
                  , CM_VALUE
                  , CM_VALUE_IN
                  , CM_VALUE_OUT )
             VALUES
                  ( @pSourceTargetId
                  , @pConceptId
                  , @CurrentCurrency
                  , @CageCurrencyType
                  , 0
                  , 0
                  , 0 )
                  
    END

    -- CAGE SESSION METERS --
    IF (@CreateMetersOption = 0 OR @CreateMetersOption = 2) AND @pCageSessionId IS NOT NULL
    BEGIN
      IF NOT EXISTS (SELECT TOP 1 CSM_VALUE 
                       FROM CAGE_SESSION_METERS 
                      WHERE CSM_SOURCE_TARGET_ID = @pSourceTargetId
                        AND CSM_CONCEPT_ID = @pConceptId
                        AND CSM_ISO_CODE = @CurrentCurrency
                        AND CSM_CAGE_CURRENCY_TYPE = @CageCurrencyType
                        AND CSM_CAGE_SESSION_ID = @pCageSessionId)
        INSERT INTO CAGE_SESSION_METERS
                  ( CSM_CAGE_SESSION_ID
                  , CSM_SOURCE_TARGET_ID
                  , CSM_CONCEPT_ID
                  , CSM_ISO_CODE
                  , CSM_CAGE_CURRENCY_TYPE
                  , CSM_VALUE
                  , CSM_VALUE_IN
                  , CSM_VALUE_OUT )
            VALUES
                  ( @pCageSessionId
                  , @pSourceTargetId
                  , @pConceptId
                  , @CurrentCurrency
                  , @CageCurrencyType
                  , 0
                  , 0
                  , 0 )

    END
                               
    IF (@CreateMetersOption = 3)
    BEGIN
                               
      DECLARE Cur2 CURSOR FOR 
       SELECT CGS_CAGE_SESSION_ID  
         FROM CAGE_SESSIONS 
        WHERE (CGS_CLOSE_DATETIME IS NULL)
                                                           
      OPEN Cur2
                                                     
      FETCH NEXT FROM Cur2 INTO @CageSessionId
                                                     
      WHILE @@FETCH_STATUS = 0
      BEGIN
      
        IF NOT EXISTS (SELECT TOP 1 CSM_VALUE 
                         FROM CAGE_SESSION_METERS 
                        WHERE CSM_SOURCE_TARGET_ID = @pSourceTargetId
                          AND CSM_CONCEPT_ID = @pConceptId
                          AND CSM_ISO_CODE = @CurrentCurrency
                          AND CSM_CAGE_CURRENCY_TYPE = @CageCurrencyType
                          AND CSM_CAGE_SESSION_ID = @CageSessionId)
          INSERT INTO CAGE_SESSION_METERS
                    ( CSM_CAGE_SESSION_ID
                    , CSM_SOURCE_TARGET_ID
                    , CSM_CONCEPT_ID
                    , CSM_ISO_CODE
                    , CSM_CAGE_CURRENCY_TYPE
                    , CSM_VALUE
                    , CSM_VALUE_IN
                    , CSM_VALUE_OUT )
              VALUES
                    ( @CageSessionId
                    , @pSourceTargetId
                    , @pConceptId
                    , @CurrentCurrency
                    , @CageCurrencyType
                    , 0
                    , 0
                    , 0 )
                                                                               
        FETCH NEXT FROM Cur2 INTO @CageSessionId
        
      END
                                                           
      CLOSE Cur2
      DEALLOCATE Cur2                       
    
    END
                               
    FETCH NEXT FROM Curs_CageCreateMeters INTO @CurrentCurrency, @CageCurrencyType
                
  END

  CLOSE Curs_CageCreateMeters
  DEALLOCATE Curs_CageCreateMeters
              
END -- CageCreateMeters

GO
GRANT EXECUTE ON [dbo].[CageCreateMeters] TO [wggui] WITH GRANT OPTION 
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CageUpdateMeter]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[CageUpdateMeter]
GO

/****** Object:  StoredProcedure [dbo].[CageUpdateMeter]    Script Date: 09/30/2014 17:17:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[CageUpdateMeter]
       @pValueIn MONEY
    , @pValueOut MONEY
    ,  @pSourceTagetId BIGINT
    , @pConceptId BIGINT
    , @pIsoCode VARCHAR(3)
    , @pCageCurrencyType INT
    , @pOperation INT
    , @pSessionId BIGINT = NULL
    , @pSessionGetMode INT
      
  AS
BEGIN 

  -- @pOperationId  =  0 - Set
  --          =  1 - Update
  
  -- , @pCageSessionGetMode = 0 - Get cage session from parameters
  --              = 1 - Get cage_sessions


  DECLARE @CageSessionId BIGINT
  DECLARE @GetFromMode INT
  DECLARE @out_CageMetersValue TABLE(CM_VALUE DECIMAL)
  DECLARE @out_CageSessionMetersValue TABLE (CSM_VALUE DECIMAL)

  -- Get Cage Session
  
  IF @pSessionGetMode = 0
    SET @CageSessionId = @pSessionId

  ELSE IF @pSessionGetMode = 1
  BEGIN
  
    -------------- TODO : GET FROM GP!! -------------- 
    --SELECT @GetFromMode = GP_KEY_VALUE FROM GENERAL_PARAMS 
    -- WHERE GP_GROUP_KEY = 'RegionalOptions' 
    --   AND GP_SUBJECT_KEY = 'CurrencyISOCode'
    SET @GetFromMode = 1
     
    IF @GetFromMode = 1
      SET @CageSessionId = (SELECT TOP 1 cgs_cage_session_id
                  FROM CAGE_SESSIONS
                ORDER BY cgs_cage_session_id DESC)
    ELSE
      SET @CageSessionId = (SELECT TOP 1 cgs_cage_session_id
                  FROM CAGE_SESSIONS
                ORDER BY cgs_cage_session_id ASC)
  END           

  -- Create Global and CageSession Meters. It checks if they don't exist and create them if necessary
  -- RCI 17-JUN-2015: Fixed Bug WIG-2443: Create them before update
  EXEC CageCreateMeters @pSourceTagetId
                      , @pConceptId
                      , @CageSessionId  -- @pCageSessionId
                      , 0     -- Create in cage_meters and cage_session_meters
                      , NULL  --@CurrencyForced

  -- CAGE METERS --

  UPDATE CAGE_METERS SET                                                                  
          CM_VALUE_IN = CASE WHEN @pOperation = 1 THEN ISNULL(CM_VALUE_IN, 0) + @pValueIn    
                              ELSE CM_VALUE_IN END
         , CM_VALUE_OUT = CASE WHEN @pOperation = 1 THEN ISNULL(CM_VALUE_OUT, 0) + @pValueOut  
                               ELSE CM_VALUE_OUT END                                                                       
         , CM_VALUE = CASE WHEN @pOperation = 1 THEN ISNULL(CM_VALUE, 0) + @pValueIn - @pValueOut            
                           WHEN @pOperation = 0 THEN @pValueIn - @pValueOut                                
                           ELSE CM_VALUE END 
    OUTPUT  INSERTED.CM_VALUE INTO @out_CageMetersValue
    WHERE CM_SOURCE_TARGET_ID = @pSourceTagetId
      AND CM_CONCEPT_ID = @pConceptId                                                     
      AND CM_ISO_CODE = @pIsoCode                                                         
      AND CM_CAGE_CURRENCY_TYPE = @pCageCurrencyType                                                         

      
  -- CAGE SESSION METERS --

  IF @CageSessionId IS NOT NULL
  BEGIN
     UPDATE CAGE_SESSION_METERS SET                                                          
            CSM_VALUE_IN = ISNULL(CSM_VALUE_IN, 0) + @pValueIn 
          , CSM_VALUE_OUT = ISNULL(CSM_VALUE_OUT, 0) + @pValueOut                                  
           , CSM_VALUE = ISNULL(CSM_VALUE, 0) + @pValueIn - @pValueOut                                      
     OUTPUT INSERTED.CSM_VALUE INTO @out_CageSessionMetersValue                                                            
      WHERE CSM_SOURCE_TARGET_ID = @pSourceTagetId
        AND CSM_CONCEPT_ID = @pConceptId                                                                                              
        AND CSM_ISO_CODE = @pIsoCode                                                        
       AND CSM_CAGE_CURRENCY_TYPE = @pCageCurrencyType                                                         
        AND CSM_CAGE_SESSION_ID = @CageSessionId 
  END                                                        
 
  SELECT  ISNULL((SELECT TOP 1 CM_VALUE FROM @out_CageMetersValue),0) as CM_VALUE
      , ISNULL((SELECT TOP 1 CSM_VALUE FROM @out_CageSessionMetersValue),0) as CSM_VALUE
      
 END  -- CageUpdateMeter
                                                                                       
 GO
 GRANT EXECUTE ON [dbo].[CageUpdateMeter] TO [wggui] WITH GRANT OPTION 
 GO
 
 IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CageUpdateSystemMeters]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[CageUpdateSystemMeters]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[CageUpdateSystemMeters]
    @pCashierSessionId BIGINT
  AS
BEGIN             

  DECLARE @CageSessionId BIGINT
  DECLARE @NationalCurrency VARCHAR(3)
  DECLARE @Currencies AS VARCHAR(200)
  DECLARE @CurrentCurrency AS VARCHAR(3)
  DECLARE @CageCurrencyType INT
      
  DECLARE @Tax1 MONEY
  DECLARE @Tax2 MONEY
  DECLARE @TaxIEJC MONEY

  DECLARE @ClosingShort MONEY
  DECLARE @ClosingOver MONEY
  
  DECLARE @CardRefundable BIT
  DECLARE @CardDeposit MONEY
  
  SELECT @NationalCurrency = GP_KEY_VALUE FROM GENERAL_PARAMS 
   WHERE GP_GROUP_KEY = 'RegionalOptions' 
     AND GP_SUBJECT_KEY = 'CurrencyISOCode'
  
  
  ---- GET CAGE SESSION ID ----
  
  SET @CageSessionId = (SELECT TOP 1 CGM_CAGE_SESSION_ID 
              FROM CAGE_MOVEMENTS
              WHERE CGM_CASHIER_SESSION_ID = @pCashierSessionId 
              AND CGM_TYPE IN (0, 1, 4, 5)
              ORDER BY CGM_MOVEMENT_DATETIME DESC)

  IF @CageSessionId IS NULL
    SET @CageSessionId = (SELECT TOP 1 CGM_CAGE_SESSION_ID 
                FROM CAGE_MOVEMENTS
                WHERE CGM_CASHIER_SESSION_ID = @pCashierSessionId 
                AND CGM_TYPE IN (100, 101, 103, 104)
                ORDER BY CGM_MOVEMENT_DATETIME DESC)
      
  IF @CageSessionId IS NULL
      SET @CageSessionId = (SELECT TOP 1 CGS_CAGE_SESSION_ID 
                  FROM CAGE_SESSIONS
                  WHERE CGS_CLOSE_DATETIME IS NULL
                  ORDER BY CGS_CAGE_SESSION_ID DESC)
   
   IF @CageSessionId IS NULL
      SET @CageSessionId = (SELECT TOP 1 CGS_CAGE_SESSION_ID 
                  FROM CAGE_SESSIONS
                  ORDER BY CGS_CAGE_SESSION_ID DESC)
   
      IF @CageSessionId IS NOT NULL
      BEGIN
  
        -- FEDERAL TAX --
  
        SELECT @Tax1 = SUM(CM_ADD_AMOUNT) 
        FROM CASHIER_MOVEMENTS_GROUPED_BY_SESSION_ID  
           WHERE CM_TYPE IN (6) 
          AND CM_SESSION_ID = @pCashierSessionId

        SET @Tax1 = ISNULL(@Tax1, 0)
        
        EXEC [dbo].[CageUpdateMeter]
        @pValueIn = @Tax1,
        @pValueOut = 0,
        @pSourceTagetId = 0,
        @pConceptId = 3,
        @pIsoCode = @NationalCurrency,
        @pCageCurrencyType = @CageCurrencyType,
        @pOperation = 1,
        @pSessionId = @CageSessionId,
        @pSessiongetMode = 0          
     
        -- STATE TAX --
   
        SELECT @Tax2 = SUM(CM_ADD_AMOUNT) 
        FROM CASHIER_MOVEMENTS_GROUPED_BY_SESSION_ID  
        WHERE CM_TYPE IN (14) 
          AND CM_SESSION_ID = @pCashierSessionId 
        
        SET @Tax2 = ISNULL(@Tax2, 0)
         
        EXEC [dbo].[CageUpdateMeter]
        @pValueIn = @Tax2,
        @pValueOut = 0,
        @pSourceTagetId = 0,
        @pConceptId = 2,
        @pIsoCode = @NationalCurrency,
        @pCageCurrencyType = @CageCurrencyType,
        @pOperation = 1,
        @pSessionId = @CageSessionId,
        @pSessiongetMode = 0 
        
        -- IEJC TAX --
  
        SELECT @TaxIEJC = SUM(CM_ADD_AMOUNT) 
        FROM CASHIER_MOVEMENTS_GROUPED_BY_SESSION_ID  
        WHERE CM_TYPE IN (142,146) 
          AND CM_SESSION_ID = @pCashierSessionId
 
        SET @TaxIEJC = ISNULL(@TaxIEJC, 0)
        
        EXEC [dbo].[CageUpdateMeter]
        @pValueIn = @TaxIEJC,
        @pValueOut = 0,
        @pSourceTagetId = 0,
        @pConceptId = 12,
        @pIsoCode = @NationalCurrency,
        @pCageCurrencyType = @CageCurrencyType,
        @pOperation = 1,
        @pSessionId = @CageSessionId,
        @pSessiongetMode = 0 
        
        ---- CARD_PAYMENT ---- (IF @CardRefundable IS FALSE: WE DON'T NEED TO RETURN VALUE FOR LIABILITIES)                                                                    

        SELECT @CardRefundable = GP_KEY_VALUE 
        FROM GENERAL_PARAMS  
        WHERE GP_GROUP_KEY = 'Cashier'                             
          AND GP_SUBJECT_KEY = 'CardRefundable'                    

        IF ISNULL(@CardRefundable, 0) = 1                                                                        
        BEGIN                                                                                                   
          SELECT @CardDeposit = SUM(CASE WHEN CM_TYPE = 10 THEN -1 * CM_SUB_AMOUNT   
        ELSE CM_ADD_AMOUNT END)                                                  
                FROM CASHIER_MOVEMENTS_GROUPED_BY_SESSION_ID                                                    
                WHERE CM_TYPE IN (9, 10, 532, 533, 534, 535, 536, 544, 545, 546, 547, 548)                                             
                  AND CM_SESSION_ID IN(SELECT DISTINCT(CGM_CASHIER_SESSION_ID)                                   
                             FROM CAGE_MOVEMENTS                                                                     
                             WHERE CGM_CASHIER_SESSION_ID = @pCashierSessionId)
                                                    
          SET @CardDeposit = ISNULL(@CardDeposit, 0)
              
          EXEC [dbo].[CageUpdateMeter]
            @pValueIn = @CardDeposit,
            @pValueOut = 0,
            @pSourceTagetId = 0,
            @pConceptId = 5,
            @pIsoCode = @NationalCurrency,
            @pCageCurrencyType = @CageCurrencyType,
            @pOperation = 1,
            @pSessionId = @CageSessionId,
            @pSessiongetMode = 0
                     
        END                                                                                                     

      SELECT @Currencies = GP_KEY_VALUE 
      FROM GENERAL_PARAMS 
      WHERE GP_GROUP_KEY = 'RegionalOptions' 
        AND GP_SUBJECT_KEY = 'CurrenciesAccepted'
    
      -- Split currencies ISO codes      
       SELECT * INTO #TMP_CURRENCIES_ISO_CODES FROM 
       (  SELECT SST_VALUE AS CURRENCY_ISO_CODE, 0 AS CAGE_CURRENCY_TYPE FROM [SplitStringIntoTable] (@Currencies, ';', 1)
        UNION ALL
        SELECT [chs_iso_code] AS CURRENCY_ISO_CODE, [chs_chip_type] AS CAGE_CURRENCY_TYPE  FROM [chips_sets] 
       ) AS A
           
      DECLARE Curs_CageUpdateSystemMeters CURSOR FOR 
      SELECT CURRENCY_ISO_CODE, CAGE_CURRENCY_TYPE
        FROM #TMP_CURRENCIES_ISO_CODES 
      
      SET NOCOUNT ON;

      OPEN Curs_CageUpdateSystemMeters

      FETCH NEXT FROM Curs_CageUpdateSystemMeters INTO @CurrentCurrency, @CageCurrencyType
    
      WHILE @@FETCH_STATUS = 0
      BEGIN
  
        -- CLOSING SHORT --
        
        SELECT @ClosingShort = SUM(CM_ADD_AMOUNT) 
        FROM CASHIER_MOVEMENTS_GROUPED_BY_SESSION_ID  
        WHERE CM_TYPE IN (160) 
          AND CM_SESSION_ID = @pCashierSessionId 
          AND CM_CURRENCY_ISO_CODE = @CurrentCurrency
          AND CM_CAGE_CURRENCY_TYPE = @CageCurrencyType
            
        SET @ClosingShort = ISNULL(@ClosingShort, 0)
        
        EXEC [dbo].[CageUpdateMeter]
            @pValueIn = @ClosingShort,
            @pValueOut = 0,
            @pSourceTagetId = 0,
            @pConceptId = 7,
            @pIsoCode = @CurrentCurrency,
            @pCageCurrencyType = @CageCurrencyType, 
            @pOperation = 1,
            @pSessionId = @CageSessionId,
            @pSessiongetMode = 0 
        
        -- CLOSING OVER --
        
        SELECT @ClosingOver = SUM(CM_ADD_AMOUNT) 
        FROM CASHIER_MOVEMENTS_GROUPED_BY_SESSION_ID  
        WHERE CM_TYPE IN (161) 
          AND CM_SESSION_ID = @pCashierSessionId 
          AND CM_CURRENCY_ISO_CODE = @CurrentCurrency
          AND CM_CAGE_CURRENCY_TYPE = @CageCurrencyType
        
        SET @ClosingOver = ISNULL(@ClosingOver, 0)
        
        EXEC [dbo].[CageUpdateMeter]
            @pValueIn = @ClosingOver,
            @pValueOut = 0,
            @pSourceTagetId = 0,
            @pConceptId = 8,
            @pIsoCode = @CurrentCurrency,
            @pCageCurrencyType = @CageCurrencyType,
            @pOperation = 1,
            @pSessionId = @CageSessionId,
            @pSessiongetMode = 0 
          
        FETCH NEXT FROM Curs_CageUpdateSystemMeters INTO @CurrentCurrency, @CageCurrencyType
                
      END

      CLOSE Curs_CageUpdateSystemMeters
      DEALLOCATE Curs_CageUpdateSystemMeters

      END                                      

END -- CageUpdateSystemMeters

GO
GRANT EXECUTE ON [dbo].[CageUpdateSystemMeters] TO [wggui] WITH GRANT OPTION 
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CashierMovementsGrouped_Read]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CashierMovementsGrouped_Read]
GO

CREATE PROCEDURE [dbo].[CashierMovementsGrouped_Read]        
         @pCashierSessionId NVARCHAR(MAX)
        ,@pDateFrom DATETIME
        ,@pDateTo DATETIME       
  AS
BEGIN 
DECLARE @_sql NVARCHAR(MAX)

  IF (@pCashierSessionId IS NOT NULL AND @pDateFrom IS NULL AND @pDateTo IS NULL)  --ByID
  BEGIN  
  SET @_sql =  'IF EXISTS (SELECT TOP 1 CM_SESSION_ID FROM cashier_movements_grouped_by_session_id WHERE CM_SESSION_ID IN ( ' + @pCashierSessionId + '))  
        BEGIN
  
            SELECT
               CM_SESSION_ID
              ,CM_TYPE
              ,CM_SUB_TYPE
              ,CM_TYPE_COUNT
              ,CM_CURRENCY_ISO_CODE
              ,CM_CURRENCY_DENOMINATION
              ,CM_SUB_AMOUNT
              ,CM_ADD_AMOUNT
              ,CM_AUX_AMOUNT
              ,CM_INITIAL_BALANCE
              ,CM_FINAL_BALANCE
              ,CM_CAGE_CURRENCY_TYPE
            FROM cashier_movements_grouped_by_session_id WHERE CM_SESSION_ID IN (' + @pCashierSessionId + ')
        END
        ELSE
        BEGIN 
          
          EXEC CashierMovementsGrouped_Select ''' + REPLACE(@pCashierSessionId,'''','''''') + ''',NULL ,NULL
        END  '
        
    EXEC sp_executesql @_sql
        
  END
  ELSE
  BEGIN    
    IF DATEPART(MINUTE,@pDateFrom) = 0 AND DATEPART(MINUTE,@pDateTo) = 0 AND (DATEDIFF(HOUR,@pDateFrom,@pDateTo) = (SELECT COUNT(DISTINCT(CM_DATE)) FROM cashier_movements_grouped_by_hour WHERE CM_DATE >= @pDateFrom AND CM_DATE < @pDateTo)) AND @pCashierSessionId IS NULL
    BEGIN
      SET @_sql ='
            SELECT
               NULL
              ,CM_TYPE
              ,CM_SUB_TYPE
              ,CM_TYPE_COUNT              
              ,CM_CURRENCY_ISO_CODE
              ,CM_CURRENCY_DENOMINATION
              ,CM_SUB_AMOUNT
              ,CM_ADD_AMOUNT
              ,CM_AUX_AMOUNT
              ,CM_INITIAL_BALANCE
              ,CM_FINAL_BALANCE
              ,CM_CAGE_CURRENCY_TYPE
            FROM cashier_movements_grouped_by_hour WHERE CM_DATE >= ''' +CONVERT(VARCHAR, @pDateFrom, 21) + '''  AND CM_DATE < ''' + CONVERT(VARCHAR, @pDateTo, 21) +''''
            
        EXEC sp_executesql @_sql
        
    END
    ELSE
    BEGIN
        EXEC CashierMovementsGrouped_Select @pCashierSessionId, @pDateFrom, @pDateTo
    END
  END

  
END --CashierMovementsGrouped_Read
GO

GRANT EXECUTE ON [dbo].[CashierMovementsGrouped_Read] TO [wggui] WITH GRANT OPTION
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CashierMovementsGrouped_Select]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CashierMovementsGrouped_Select]
GO

CREATE PROCEDURE [dbo].[CashierMovementsGrouped_Select]
        @pCashierSessionId NVARCHAR(MAX)
       ,@pDateFrom DATETIME
       ,@pDateTo DATETIME 
  AS
BEGIN

DECLARE @_sql NVARCHAR(MAX)

DECLARE @_CM_CURRENCY_ISO_CODE NVARCHAR(20)
  SELECT @_CM_CURRENCY_ISO_CODE = gp_key_value FROM general_params 
  WHERE gp_group_key = 'RegionalOptions' and gp_subject_key = 'CurrencyISOCode'
  SET @_CM_CURRENCY_ISO_CODE = ISNULL(@_CM_CURRENCY_ISO_CODE, 'NULL')

  IF (@pCashierSessionId IS NOT NULL AND @pDateFrom IS NULL AND @pDateTo IS NULL)  --ByID
  BEGIN     
      SET @_sql =  '
      SELECT   CM_SESSION_ID
          ,CM_TYPE
          ,0                AS CM_SUB_TYPE
          ,COUNT(CM_TYPE)               AS CM_TYPE_COUNT
          ,ISNULL(CM_CURRENCY_ISO_CODE ,'''+@_CM_CURRENCY_ISO_CODE+''')  AS CM_CURRENCY_ISO_CODE
          ,ISNULL(CM_CURRENCY_DENOMINATION, 0) AS CM_CURRENCY_DENOMINATION
          ,SUM(ISNULL(CM_SUB_AMOUNT,0)) CM_SUB_AMOUNT
          ,SUM(ISNULL(CM_ADD_AMOUNT,0)) CM_ADD_AMOUNT
          ,SUM(ISNULL(CM_AUX_AMOUNT,0)) CM_AUX_AMOUNT
          ,SUM(ISNULL(CM_INITIAL_BALANCE,0)) CM_INITIAL_BALANCE
          ,SUM(ISNULL(CM_FINAL_BALANCE,0)) CM_FINAL_BALANCE
          ,CM_CAGE_CURRENCY_TYPE
          
      FROM   CASHIER_MOVEMENTS WITH (INDEX (IX_CM_SESSION_ID)) 
      WHERE   CM_SESSION_ID IN ('+@pCashierSessionId+')
      GROUP BY CM_SESSION_ID, CM_TYPE, ISNULL(CM_CURRENCY_ISO_CODE ,'''+@_CM_CURRENCY_ISO_CODE+'''), ISNULL(CM_CURRENCY_DENOMINATION, 0), CM_CAGE_CURRENCY_TYPE
         
         UNION ALL 
         SELECT  mbm_cashier_session_id             AS CM_SESSION_ID
             , mbm_type                              AS CM_TYPE  
             , 1                     AS CM_SUB_TYPE          
             , COUNT(mbm_type)                 AS CM_TYPE_COUNT     
             ,'''+@_CM_CURRENCY_ISO_CODE+'''         AS CM_CURRENCY_ISO_CODE
             , 0                     AS CM_CURRENCY_DENOMINATION
             , SUM(ISNULL(mbm_sub_amount,0))       AS CM_SUB_AMOUNT
             , SUM(ISNULL(mbm_add_amount,0))       AS CM_ADD_AMOUNT        
             , 0                     AS CM_AUX_AMOUNT 
             , 0                     AS CM_INITIAL_BALANCE
             , 0                                     AS CM_FINAL_BALANCE
             , 0                     AS CM_CAGE_CURRENCY_TYPE         
              
         FROM    MB_MOVEMENTS WITH (INDEX (IX_MB_MOVEMENTS))          
        WHERE   mbm_cashier_session_id IN ('+@pCashierSessionId+')
        GROUP BY    mbm_cashier_session_id, mbm_type '
  END --ByID
  ELSE--ByDate
  BEGIN
  SET @_sql =  '
        SELECT   
            NULL
           , CM_TYPE
           , 0                AS CM_SUB_TYPE      
           , COUNT(CM_TYPE)                AS CM_TYPE_COUNT
           , ISNULL(CM_CURRENCY_ISO_CODE ,'''+@_CM_CURRENCY_ISO_CODE+''') AS CM_CURRENCY_ISO_CODE
           , ISNULL(CM_CURRENCY_DENOMINATION, 0) AS CM_CURRENCY_DENOMINATION
            , SUM(ISNULL(CM_SUB_AMOUNT,      0)) CM_SUB_AMOUNT
          , SUM(ISNULL(CM_ADD_AMOUNT,      0)) CM_ADD_AMOUNT
          , SUM(ISNULL(CM_AUX_AMOUNT,      0)) CM_AUX_AMOUNT
          , SUM(ISNULL(CM_INITIAL_BALANCE, 0)) CM_INITIAL_BALANCE
          , SUM(ISNULL(CM_FINAL_BALANCE,   0)) CM_FINAL_BALANCE
          , CM_CAGE_CURRENCY_TYPE
            
         FROM    CASHIER_MOVEMENTS WITH (INDEX (IX_cm_date_type)) 
        WHERE  ' +
        CASE WHEN @pDateFrom IS NULL THEN '' ELSE ' CM_DATE >=  '''+CONVERT(VARCHAR, @pDateFrom, 21) +''' AND '  END +
        ' CM_DATE <  '''+ISNULL(CONVERT(VARCHAR, @pDateTo, 21),CONVERT(VARCHAR, GETDATE(), 21)) +''''+
          
       CASE WHEN @pCashierSessionId IS NULL THEN '' ELSE
       'AND CM_SESSION_ID IN ('+@pCashierSessionId +')'END +'

       GROUP BY   CM_TYPE, ISNULL(CM_CURRENCY_ISO_CODE ,'''+@_CM_CURRENCY_ISO_CODE+'''),ISNULL(CM_CURRENCY_DENOMINATION, 0), CM_CAGE_CURRENCY_TYPE
       UNION ALL 
       SELECT
           NULL   
           , mbm_type                              AS CM_TYPE 
           , 1                     AS CM_SUB_TYPE         
           , COUNT(mbm_type)             AS CM_TYPE_COUNT
           ,'''+@_CM_CURRENCY_ISO_CODE+'''         AS CM_CURRENCY_ISO_CODE
           , 0                     AS CM_CURRENCY_DENOMINATION
           , SUM(ISNULL(mbm_sub_amount,0))       AS CM_SUB_AMOUNT
           , SUM(ISNULL(mbm_add_amount,0))       AS CM_ADD_AMOUNT        
           , 0                     AS CM_AUX_AMOUNT 
           , 0                     AS CM_INITIAL_BALANCE
           , 0                                     AS CM_FINAL_BALANCE    
           , 0                     AS CM_CAGE_CURRENCY_TYPE         
       FROM    MB_MOVEMENTS WITH (INDEX (IX_MBM_DATETIME_TYPE))             
      WHERE   ' +
        CASE WHEN @pDateFrom IS NULL THEN '' ELSE ' MBM_DATETIME >=  '''+CONVERT(VARCHAR, @pDateFrom, 21) +''' AND '  END +
        ' MBM_DATETIME <  '''+ISNULL(CONVERT(VARCHAR, @pDateTo, 21),CONVERT(VARCHAR, GETDATE(), 21)) +''''+
      
          CASE WHEN @pCashierSessionId IS NULL THEN '' ELSE
          'AND mbm_cashier_session_id IN ('+@pCashierSessionId +')'END +'
      GROUP BY    mbm_type '
    
  END--ByDate
  
  EXEC sp_executesql @_sql
  
 END --CashierMovementsGrouped_Select
GO 

GRANT EXECUTE ON [dbo].[CashierMovementsGrouped_Select] TO [wggui] WITH GRANT OPTION
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GT_Cancellations]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GT_Cancellations]
GO

CREATE PROCEDURE [dbo].[GT_Cancellations]
(
	@pCashierId BIGINT,
    @pDateFrom DATETIME,
    @pDateTo DATETIME,
    @pType INT, -- 0: only Sales ; 1: only Purchases ; -1: all
    @pUserId INT,
    @pSelectedCurrency NVARCHAR(3)
)
AS
BEGIN
      DECLARE @TYPE_CHIPS_SALE AS INT
      DECLARE @TYPE_CHIPS_PURCHASE AS INT
      DECLARE @TYPE_CHIPS_SALE_TOTAL AS INT
      DECLARE @TYPE_CHIPS_PURCHASE_TOTAL AS INT
      DECLARE @TYPE_CHIPS_SALE_TOTAL_EXCHANGE AS INT
      DECLARE @TYPE_CHIPS_PURCHASE_TOTAL_EXCHANGE AS INT
      DECLARE @UNDO_STATUS AS INT
      DECLARE @NATIONAL_CURRENCY AS NVARCHAR(3)

      SET @TYPE_CHIPS_SALE = 300
      SET @TYPE_CHIPS_PURCHASE = 301
      SET @TYPE_CHIPS_SALE_TOTAL = 303
      SET @TYPE_CHIPS_PURCHASE_TOTAL = 304
      SET @TYPE_CHIPS_SALE_TOTAL_EXCHANGE = 309
      SET @TYPE_CHIPS_PURCHASE_TOTAL_EXCHANGE = 310
      SET @UNDO_STATUS = 2
      SET @pType = ISNULL(@pType, -1)
      SET @NATIONAL_CURRENCY = (SELECT GP_KEY_VALUE 
                                FROM GENERAL_PARAMS 
                                WHERE GP_GROUP_KEY = 'RegionalOptions' 
                                  AND GP_SUBJECT_KEY = 'CurrencyISOCode')
                            
      SELECT
            CASE WHEN CM_TYPE IN (@TYPE_CHIPS_SALE_TOTAL, @TYPE_CHIPS_PURCHASE_TOTAL, @TYPE_CHIPS_SALE_TOTAL_EXCHANGE, @TYPE_CHIPS_PURCHASE_TOTAL_EXCHANGE) THEN CM_CASHIER_NAME
                  ELSE NULL 
                  END AS ORIGEN,
            CASE WHEN CM_TYPE IN (@TYPE_CHIPS_SALE_TOTAL, @TYPE_CHIPS_PURCHASE_TOTAL, @TYPE_CHIPS_SALE_TOTAL_EXCHANGE, @TYPE_CHIPS_PURCHASE_TOTAL_EXCHANGE) THEN CM_DATE
                  ELSE NULL 
                  END AS CM_DATE,                                    
            CASE WHEN CM_TYPE IN (@TYPE_CHIPS_SALE, @TYPE_CHIPS_SALE_TOTAL) THEN ABS(CM_SUB_AMOUNT)                 
                 WHEN CM_TYPE IN (@TYPE_CHIPS_PURCHASE, @TYPE_CHIPS_PURCHASE_TOTAL) THEN ABS(CM_ADD_AMOUNT)
                 WHEN CM_TYPE IN (@TYPE_CHIPS_PURCHASE_TOTAL_EXCHANGE, @TYPE_CHIPS_SALE_TOTAL_EXCHANGE) THEN ABS(CM_INITIAL_BALANCE)
                 END AS AMOUNT,                
            ABS(CASE WHEN CM_CURRENCY_DENOMINATION IS NULL OR CM_CURRENCY_DENOMINATION = 0 THEN 0 
                      ELSE 
                          CASE WHEN CM_TYPE IN (@TYPE_CHIPS_SALE, CASE WHEN @pSelectedCurrency = @NATIONAL_CURRENCY THEN @TYPE_CHIPS_SALE_TOTAL ELSE @TYPE_CHIPS_SALE_TOTAL_EXCHANGE END) THEN CM_SUB_AMOUNT
                                ELSE CM_ADD_AMOUNT 
                                END / CM_CURRENCY_DENOMINATION 
                      END) AS QUANTITY,
            CM_CURRENCY_DENOMINATION,
            CM.CM_MOVEMENT_ID,
            CM.CM_TYPE,
            CM.CM_USER_NAME,
            CM.CM_OPERATION_ID,
            CASE WHEN CM_TYPE IN (@TYPE_CHIPS_SALE_TOTAL, @TYPE_CHIPS_PURCHASE_TOTAL, @TYPE_CHIPS_SALE_TOTAL_EXCHANGE, @TYPE_CHIPS_PURCHASE_TOTAL_EXCHANGE) THEN AO.AO_DATETIME
                  ELSE NULL 
                  END AS AO_DATETIME
      FROM CASHIER_MOVEMENTS CM
            LEFT JOIN ACCOUNT_OPERATIONS CANCEL ON CM.CM_OPERATION_ID          = CANCEL.AO_OPERATION_ID
            LEFT JOIN ACCOUNT_OPERATIONS AO     ON CANCEL.AO_UNDO_OPERATION_ID = AO.AO_OPERATION_ID
      WHERE (@pCashierId IS NULL OR @pCashierId = CM_CASHIER_ID)
              AND (@pUserId IS NULL OR @pUserId = CM_USER_ID)
              AND CM_UNDO_STATUS = @UNDO_STATUS 
              AND CM_TYPE IN (@TYPE_CHIPS_SALE, @TYPE_CHIPS_PURCHASE, @TYPE_CHIPS_SALE_TOTAL, @TYPE_CHIPS_PURCHASE_TOTAL, @TYPE_CHIPS_SALE_TOTAL_EXCHANGE, @TYPE_CHIPS_PURCHASE_TOTAL_EXCHANGE)
              AND CM_DATE >= @pDateFrom AND CM_DATE < @pDateTo
              AND ((         @pType = 1  AND CM.CM_TYPE IN (@TYPE_CHIPS_PURCHASE, 
                                                            CASE WHEN @pSelectedCurrency = @NATIONAL_CURRENCY THEN @TYPE_CHIPS_PURCHASE_TOTAL 
                                                              ELSE @TYPE_CHIPS_PURCHASE_TOTAL_EXCHANGE END))
                         OR (@pType = 0  AND CM.CM_TYPE IN (@TYPE_CHIPS_SALE, 
                                                            CASE WHEN @pSelectedCurrency = @NATIONAL_CURRENCY THEN @TYPE_CHIPS_SALE_TOTAL 
                                                              ELSE @TYPE_CHIPS_SALE_TOTAL_EXCHANGE END))
                         OR (@pType = -1 AND CM.CM_TYPE IN (@TYPE_CHIPS_SALE, 
                                                            @TYPE_CHIPS_PURCHASE, 
                                                            CASE WHEN @pSelectedCurrency = @NATIONAL_CURRENCY THEN @TYPE_CHIPS_SALE_TOTAL 
                                                              ELSE @TYPE_CHIPS_SALE_TOTAL_EXCHANGE END,
                                                            CASE WHEN @pSelectedCurrency = @NATIONAL_CURRENCY THEN @TYPE_CHIPS_PURCHASE_TOTAL 
                                                              ELSE @TYPE_CHIPS_PURCHASE_TOTAL_EXCHANGE END)))              
              AND CM_CURRENCY_ISO_CODE IN (@pSelectedCurrency)
      ORDER BY CM.CM_MOVEMENT_ID
	
END --GT_CANCELLATIONS
GO

GRANT EXECUTE ON [dbo].[GT_Cancellations] TO [wggui] WITH GRANT OPTION
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GT_Chips_Operations]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GT_Chips_Operations]
GO

CREATE PROCEDURE [dbo].[GT_Chips_Operations] 
(
   @pDateFrom           DATETIME
  ,@pDateTo             DATETIME
  ,@pStatus             INTEGER
  ,@pArea               NVARCHAR(50)
  ,@pBank               NVARCHAR(50)
  ,@pOnlyTables         INTEGER
  ,@pCashierGroupName   NVARCHAR(50)
  ,@pValidTypes         NVARCHAR(50)
  ,@pSelectedCurrency   NVARCHAR(3)
  ,@pSelectedOptionForm INTEGER
)
AS
BEGIN
  ----------------------------------------------------------------------------------------------------------------
  DECLARE @_DATE_FROM                  AS DATETIME
  DECLARE @_DATE_TO                    AS DATETIME
  DECLARE @_STATUS                     AS INTEGER
  DECLARE @_AREA                       AS NVARCHAR(50)
  DECLARE @_BANK                       AS NVARCHAR(50)
  DECLARE @_DELIMITER                  AS CHAR(1)
  DECLARE @_ONLY_TABLES                AS INTEGER
  DECLARE @_CASHIERS_NAME              AS NVARCHAR(50)
  DECLARE @_TYPES_TABLE                TABLE(SST_ID INT, SST_VALUE NVARCHAR(50)) -- TO STORE THE VALID GAMING TABLE TYPES 
  DECLARE @_TYPE_CHIPS_SALE_TOTAL      AS INTEGER
  DECLARE @_TYPE_CHIPS_PURCHASE_TOTAL  AS INTEGER
  DECLARE @_CHIP_RE                    AS INTEGER
  DECLARE @_CHIP_NR                    AS INTEGER
  DECLARE @_SELECTED_CURRENCY          AS NVARCHAR(3)
  DECLARE @_SELECTED_OPTION_FORM       AS INTEGER
  DECLARE @_TYPE_CHIPS_SALE_TOTAL_EXCHANGE     AS INTEGER
  DECLARE @_TYPE_CHIPS_PURCHASE_TOTAL_EXCHANGE AS INTEGER
  ----------------------------------------------------------------------------------------------------------------

  -- Initialzitation --
  SET @_TYPE_CHIPS_SALE_TOTAL              = 303
  SET @_TYPE_CHIPS_PURCHASE_TOTAL          = 304
  SET @_TYPE_CHIPS_SALE_TOTAL_EXCHANGE     = 309
  SET @_TYPE_CHIPS_PURCHASE_TOTAL_EXCHANGE = 310

  SET @_DELIMITER            =   ','
  SET @_DATE_FROM            =   @pDateFrom
  SET @_DATE_TO              =   @pDateTo
  SET @_STATUS               =   ISNULL(@pStatus, -1)
  SET @_AREA                 =   ISNULL(@pArea, '')
  SET @_BANK                 =   ISNULL(@pBank, '')
  SET @_ONLY_TABLES          =   ISNULL(@pOnlyTables, 1)
  SET @_CHIP_RE              =   1001
  SET @_CHIP_NR              =   1002
  SET @_SELECTED_CURRENCY    = @pSelectedCurrency
  SET @_SELECTED_OPTION_FORM = @pSelectedOptionForm
                                  
  IF ISNULL(@_CASHIERS_NAME,'') = '' BEGIN
    SET @_CASHIERS_NAME  = '---CASHIER---'
  END

  ----------------------------------------------------------------------------------------------------------------
  -- CHECK DATE PARAMETERS
  IF @_DATE_FROM IS NULL
  BEGIN
     -- IF DATE FROM IS NULL, USER FIRST POSIBLE DATE
     SET @_DATE_FROM = CAST('' AS DATETIME)
  END
  IF @_DATE_TO IS NULL
  BEGIN
     -- IF DATE TO IS NULL, QUERY UNTIL CURRENT DATE
     SET @_DATE_TO = CAST(GETDATE() AS DATETIME)
  END

  -- ASSIGN TYPES PARAMETER INTO TABLE
  INSERT INTO @_TYPES_TABLE SELECT * FROM dbo.SplitStringIntoTable(@pValidTypes, @_DELIMITER, DEFAULT)

  ----------------------------------------------------------------------------------------------------------------
  -- MAIN QUERY
  -- Total to local isocode
  IF @_SELECTED_OPTION_FORM = 0
  BEGIN
     SELECT   0 AS TYPE_SESSION
            , CS_SESSION_ID AS SESSION_ID
            , GT_NAME AS GT_NAME
            , CS_OPENING_DATE AS SESSION_DATE
            , GTT_NAME AS GTT_NAME
            , ISNULL(SUM(GTS_TOTAL_SALES_AMOUNT)   , 0) AS GTS_TOTAL_SALES_AMOUNT
            , ISNULL(SUM(GTS_TOTAL_PURCHASE_AMOUNT), 0) AS GTS_TOTAL_PURCHASE_AMOUNT
            , ISNULL(SUM(GTS_TOTAL_SALES_AMOUNT)   , 0) - ISNULL(SUM(GTS_TOTAL_PURCHASE_AMOUNT), 0) AS DELTA
      INTO   #CHIPS_OPERATIONS_TABLE_OPTION_1      
     FROM   CASHIER_MOVEMENTS
        INNER JOIN CASHIER_SESSIONS       ON CS_SESSION_ID            = CM_SESSION_ID
        LEFT JOIN GAMING_TABLES_SESSIONS  ON GTS_CASHIER_SESSION_ID   = CM_SESSION_ID      
        INNER JOIN GAMING_TABLES GT       ON GTS_GAMING_TABLE_ID      = GT_GAMING_TABLE_ID
        INNER JOIN GAMING_TABLES_TYPES    ON GTT_GAMING_TABLE_TYPE_ID = GT_TYPE_ID 
     WHERE   GT_ENABLED = CASE WHEN @_STATUS = -1 THEN GT_ENABLED ELSE @_STATUS END
       AND GT_AREA_ID   = CASE WHEN @_AREA   = '' THEN GT_AREA_ID ELSE @_AREA END
       AND GT_BANK_ID   = CASE WHEN @_BANK   = '' THEN GT_BANK_ID ELSE @_BANK END
       AND GT_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
       AND CM_DATE >= @_DATE_FROM
       AND CM_DATE <  @_DATE_TO
       AND GT_HAS_INTEGRATED_CASHIER = 1
       AND CM_TYPE IN (@_TYPE_CHIPS_SALE_TOTAL, @_TYPE_CHIPS_PURCHASE_TOTAL)  -- 303 = CHIPS_SALE_TOTAL; 304 = CHIPS_PURCHASE_TOTAL
       AND CM_CAGE_CURRENCY_TYPE IN (@_CHIP_RE, @_CHIP_NR)      
    GROUP BY GT_NAME, GTT_NAME, CS_SESSION_ID, CS_OPENING_DATE, CM_CURRENCY_ISO_CODE, GTS_TOTAL_SALES_AMOUNT, GTS_TOTAL_PURCHASE_AMOUNT
    ORDER BY GTT_NAME
    
    -- Check if cashiers must be visible  
    IF @_ONLY_TABLES = 0
    BEGIN

      -- Select and join data to show cashiers
      -- Adding cashiers without gaming tables

      INSERT INTO #CHIPS_OPERATIONS_TABLE_OPTION_1
        SELECT 1 AS TYPE_SESSION
             , CM_SESSION_ID AS SESSION_ID
             , CT_NAME as GT_NAME
             , CS_OPENING_DATE AS SESSION_DATE
             , @_CASHIERS_NAME as GTT_NAME
             , ISNULL(SUM(CM_SUB_AMOUNT), 0) AS GTS_TOTAL_SALES_AMOUNT
             , ISNULL(SUM(CM_ADD_AMOUNT), 0) AS GTS_TOTAL_PURCHASE_AMOUNT           
             , ISNULL(SUM(CM_SUB_AMOUNT), 0) - ISNULL(SUM(CM_ADD_AMOUNT), 0) AS DELTA                       
        FROM   CASHIER_MOVEMENTS
          INNER JOIN CASHIER_SESSIONS       ON CS_SESSION_ID          = CM_SESSION_ID
          INNER JOIN CASHIER_TERMINALS      ON CM_CASHIER_ID          = CT_CASHIER_ID
          LEFT JOIN  GAMING_TABLES_SESSIONS ON GTS_CASHIER_SESSION_ID = CM_SESSION_ID
        WHERE GTS_CASHIER_SESSION_ID IS NULL -- We want the chips operations that are from cashiers that are not gambling tables
          -- 303 = CHIPS_SALE_TOTAL; 304 = CHIPS_PURCHASE_TOTAL; 309 = CHIPS_SALE_TOTAL_EXCHANGE; 310 = CHIPS_PURCHASE_TOTAL_EXCHANGE
          AND CM_TYPE IN (@_TYPE_CHIPS_SALE_TOTAL, @_TYPE_CHIPS_PURCHASE_TOTAL, @_TYPE_CHIPS_SALE_TOTAL_EXCHANGE, @_TYPE_CHIPS_PURCHASE_TOTAL_EXCHANGE)
          AND CM_DATE >= @_DATE_FROM
          AND CM_DATE <  @_DATE_TO
          AND CM_CAGE_CURRENCY_TYPE IN (@_CHIP_RE, @_CHIP_NR)
        GROUP BY CT_NAME, CM_SESSION_ID, CS_OPENING_DATE

    END
    -- Select results
    SELECT * FROM #CHIPS_OPERATIONS_TABLE_OPTION_1 ORDER BY GTT_NAME,GT_NAME
      
    -- DROP TEMPORARY TABLE  
    DROP TABLE #CHIPS_OPERATIONS_TABLE_OPTION_1
    
  END
  
  -- By selected currency
  IF @_SELECTED_OPTION_FORM = 1
  BEGIN
   SELECT 0 AS TYPE_SESSION
        , CS_SESSION_ID AS SESSION_ID
        , GT_NAME AS GT_NAME
        , CS_OPENING_DATE AS SESSION_DATE
        , GTT_NAME AS GTT_NAME
        , ISNULL(SUM(GTSC_TOTAL_SALES_AMOUNT)   , 0) AS GTS_TOTAL_SALES_AMOUNT
        , ISNULL(SUM(GTSC_TOTAL_PURCHASE_AMOUNT), 0) AS GTS_TOTAL_PURCHASE_AMOUNT
        , ISNULL(SUM(GTSC_TOTAL_SALES_AMOUNT)   , 0) - ISNULL(SUM(GTSC_TOTAL_PURCHASE_AMOUNT), 0) AS DELTA
        , CM_CURRENCY_ISO_CODE AS CURRENCY_ISO_CODE
  INTO   #CHIPS_OPERATIONS_TABLE_OPTION_2          
  FROM   CASHIER_MOVEMENTS
    INNER JOIN CASHIER_SESSIONS       ON CS_SESSION_ID            = CM_SESSION_ID
    LEFT JOIN GAMING_TABLES_SESSIONS  ON GTS_CASHIER_SESSION_ID   = CM_SESSION_ID
    LEFT JOIN GAMING_TABLES_SESSIONS_BY_CURRENCY ON GTS_GAMING_TABLE_SESSION_ID = GTSC_GAMING_TABLE_SESSION_ID 
    INNER JOIN GAMING_TABLES GT       ON GTS_GAMING_TABLE_ID      = GT_GAMING_TABLE_ID
    INNER JOIN GAMING_TABLES_TYPES    ON GTT_GAMING_TABLE_TYPE_ID = GT_TYPE_ID 
  WHERE   GT_ENABLED = CASE WHEN @_STATUS = -1 THEN GT_ENABLED ELSE @_STATUS END
    AND GT_AREA_ID = CASE WHEN @_AREA   = '' THEN GT_AREA_ID ELSE @_AREA END
    AND GT_BANK_ID = CASE WHEN @_BANK   = '' THEN GT_BANK_ID ELSE @_BANK END
    AND GT_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
    AND CM_DATE >= @_DATE_FROM
    AND CM_DATE <  @_DATE_TO
    AND GT_HAS_INTEGRATED_CASHIER = 1
    AND CM_TYPE IN (@_TYPE_CHIPS_SALE_TOTAL, @_TYPE_CHIPS_PURCHASE_TOTAL)  -- 303 = CHIPS_SALE_TOTAL; 304 = CHIPS_PURCHASE_TOTAL
    AND CM_CAGE_CURRENCY_TYPE IN (@_CHIP_RE, @_CHIP_NR)
    AND CM_CURRENCY_ISO_CODE = @_SELECTED_CURRENCY
  GROUP BY GT_NAME, GTT_NAME, CS_SESSION_ID, CS_OPENING_DATE, CM_CURRENCY_ISO_CODE
  ORDER BY GTT_NAME
  
  -- Check if cashiers must be visible  
  IF @_ONLY_TABLES = 0
  BEGIN

    -- Select and join data to show cashiers
    -- Adding cashiers without gaming tables

    INSERT INTO #CHIPS_OPERATIONS_TABLE_OPTION_2
      SELECT 1 AS TYPE_SESSION
           , CM_SESSION_ID AS SESSION_ID
           , CT_NAME as GT_NAME
           , CS_OPENING_DATE AS SESSION_DATE
           , @_CASHIERS_NAME as GTT_NAME
           , ISNULL(SUM(CASE WHEN (CM_TYPE = @_TYPE_CHIPS_SALE_TOTAL) THEN CM_SUB_AMOUNT
                             WHEN (CM_TYPE = @_TYPE_CHIPS_SALE_TOTAL_EXCHANGE) THEN CM_INITIAL_BALANCE END), 0) AS GTS_TOTAL_SALES_AMOUNT
           , ISNULL(SUM(CASE WHEN (CM_TYPE = @_TYPE_CHIPS_PURCHASE_TOTAL) THEN CM_ADD_AMOUNT
                             WHEN (CM_TYPE = @_TYPE_CHIPS_PURCHASE_TOTAL_EXCHANGE) THEN CM_INITIAL_BALANCE END), 0) AS GTS_TOTAL_PURCHASE_AMOUNT
           
           -- CALCULATE DELTA          
           , ISNULL(SUM(CASE WHEN (CM_TYPE = @_TYPE_CHIPS_SALE_TOTAL) THEN CM_SUB_AMOUNT
                             WHEN (CM_TYPE = @_TYPE_CHIPS_SALE_TOTAL_EXCHANGE) THEN CM_INITIAL_BALANCE END), 0)
           - ISNULL(SUM(CASE WHEN (CM_TYPE = @_TYPE_CHIPS_PURCHASE_TOTAL) THEN CM_ADD_AMOUNT
                             WHEN (CM_TYPE = @_TYPE_CHIPS_PURCHASE_TOTAL_EXCHANGE) THEN CM_INITIAL_BALANCE END), 0) AS DELTA                      
          
           , CM_CURRENCY_ISO_CODE AS CURRENCY_ISO_CODE
      FROM   CASHIER_MOVEMENTS
        INNER JOIN CASHIER_SESSIONS       ON CS_SESSION_ID          = CM_SESSION_ID
        INNER JOIN CASHIER_TERMINALS      ON CM_CASHIER_ID          = CT_CASHIER_ID
        LEFT JOIN  GAMING_TABLES_SESSIONS ON GTS_CASHIER_SESSION_ID = CM_SESSION_ID
        LEFT JOIN  GAMING_TABLES_SESSIONS_BY_CURRENCY ON GTS_GAMING_TABLE_SESSION_ID = GTSC_GAMING_TABLE_SESSION_ID 
      WHERE GTS_CASHIER_SESSION_ID IS NULL -- We want the chips operations that are from cashiers that are not gambling tables
        -- 303 = CHIPS_SALE_TOTAL; 304 = CHIPS_PURCHASE_TOTAL; 309 = CHIPS_SALE_TOTAL_EXCHANGE; 310 = CHIPS_PURCHASE_TOTAL_EXCHANGE
        AND CM_TYPE IN (@_TYPE_CHIPS_SALE_TOTAL, @_TYPE_CHIPS_PURCHASE_TOTAL, @_TYPE_CHIPS_SALE_TOTAL_EXCHANGE, @_TYPE_CHIPS_PURCHASE_TOTAL_EXCHANGE)        
		AND CM_DATE >= @_DATE_FROM
        AND CM_DATE <  @_DATE_TO
        AND CM_CAGE_CURRENCY_TYPE IN (@_CHIP_RE, @_CHIP_NR)
        AND CM_CURRENCY_ISO_CODE = @_SELECTED_CURRENCY
      GROUP BY CT_NAME, CM_SESSION_ID, CS_OPENING_DATE, CM_CURRENCY_ISO_CODE

    END
    -- Select results
    SELECT * FROM #CHIPS_OPERATIONS_TABLE_OPTION_2 ORDER BY GTT_NAME,GT_NAME 
      
    -- DROP TEMPORARY TABLE  
    DROP TABLE #CHIPS_OPERATIONS_TABLE_OPTION_2
 END

END  -- END PROCEDURE
GO

-- PERMISSIONS
GRANT EXECUTE ON [dbo].[GT_Chips_Operations] TO [wggui] WITH GRANT OPTION
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GT_Base_Report_Data_Player_Tracking]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GT_Base_Report_Data_Player_Tracking]
GO

CREATE PROCEDURE [dbo].[GT_Base_Report_Data_Player_Tracking] 
 ( @pBaseType INTEGER
  ,@pTimeInterval INTEGER
  ,@pDateFrom DATETIME 
  ,@pDateTo DATETIME
  ,@pOnlyActivity INTEGER
  ,@pOrderBy INTEGER
  ,@pValidTypes VARCHAR(4096)
  ,@pSelectedCurrency NVARCHAR(3)
)
AS
BEGIN

-- DECLARATIONS
   -- @_DAYS_AND_TABLES:    A temporary table with all intervals and all base type selected (used to show base types without activity).
   DECLARE @_DAYS_AND_TABLES TABLE(DATE_TIME DATETIME, TABLE_IDENTIFIER_VALUE BIGINT, TABLE_IDENT_NAME VARCHAR(50), TABLE_TYPE_IDENT BIGINT, TABLE_TYPE_NAME VARCHAR(50))
   -- @_DAY_VAR:            Variable to create the intervals
   DECLARE @_DAY_VAR DATETIME

-- PARAMETERS
   DECLARE @_BASE_TYPE        AS   INTEGER -- 0 BY TABLE TYPE, 1 BY TABLE 
   DECLARE @_TIME_INTERVAL    AS   INTEGER -- -1, 0 DAYS, 1 MONTH, 2 YEAR
   DECLARE @_DATE_FROM        AS   DATETIME
   DECLARE @_DATE_TO          AS   DATETIME
   DECLARE @_ONLY_ACTIVITY    AS   INTEGER -- 0 SHOW ALL, 1 SHOW ONLY WITH ACTIVITY
   DECLARE @_ORDER_BY         AS   INTEGER -- 0 BY INTERVAL DESC, 1 BY IDENTIFIER ASC
   DECLARE @_DELIMITER        AS   CHAR(1)
   DECLARE @_TYPES_TABLE      TABLE(SST_ID INT, SST_VALUE VARCHAR(50)) -- TO STORE THE VALID GAMING TABLE TYPES 
   DECLARE @_SELECTED_CURRENCY AS  NVARCHAR(3)
   
----------------------------------------------------------------------------------------------------------------

-- Initialzitation --
SET @_BASE_TYPE        =   @pBaseType
SET @_TIME_INTERVAL    =   @pTimeInterval
SET @_DATE_FROM        =   @pDateFrom
SET @_DATE_TO          =   @pDateTo
SET @_ONLY_ACTIVITY    =   @pOnlyActivity
SET @_ORDER_BY         =   @pOrderBy
SET @_DELIMITER        =   ','
SET @_SELECTED_CURRENCY =  @pSelectedCurrency

----------------------------------------------------------------------------------------------------------------

-- CHECK DATE PARAMETERS
IF @_DATE_FROM IS NULL
BEGIN
   -- IF DATE FROM IS NULL, USER FIRST POSIBLE DATE
   SET @_DATE_FROM = CAST('' AS DATETIME)
   SET @pDateFrom  = CAST('' AS DATETIME)
END
IF @_DATE_TO IS NULL
BEGIN
   -- IF DATE TO IS NULL, QUERY UNTIL CURRENT DATE
   SET @_DATE_TO = CAST(GETDATE() AS DATETIME)
   SET @pDateTo  = CAST(GETDATE() AS DATETIME)
END

-- ASSIGN TYPES PARAMETER INTO TABLE
INSERT INTO @_TYPES_TABLE SELECT * FROM dbo.SplitStringIntoTable(@pValidTypes, @_DELIMITER, DEFAULT)

IF @_TIME_INTERVAL <> -1 
 BEGIN
   -- INTERVALS DATES AND TABLE PREPARATION
   -- PREPARE DATES RANGE DEPENDING ON TIME INTERVAL
   SET @_DATE_FROM = CASE 
                        WHEN @_TIME_INTERVAL = 0 THEN -- BY DAY
                           DATEADD(dd, 0, DATEDIFF(dd, 0, @_DATE_FROM))
                        WHEN @_TIME_INTERVAL = 1 THEN -- BY MONTH
                           CAST(CAST(YEAR(@_DATE_FROM) AS VARCHAR(4)) + RIGHT('0' + CAST(MONTH(@_DATE_FROM) AS VARCHAR(2)), 2) + '01' AS DATETIME)
                        WHEN @_TIME_INTERVAL = 2 THEN -- BY YEAR
                           CAST(CAST(YEAR(@_DATE_FROM) AS VARCHAR(4)) + '0101' AS DATETIME)
                     END

   SET @_DATE_TO = CASE 
                        WHEN @_TIME_INTERVAL = 0 THEN -- BY DAY
                           DATEADD(DAY, 1, CAST(@_DATE_TO AS DATETIME))
                        WHEN @_TIME_INTERVAL = 1 THEN -- BY MONTH
                           DATEADD(MONTH, 1, CAST(CAST(YEAR(@_DATE_TO) AS VARCHAR(4)) + RIGHT('0' + CAST(MONTH(@_DATE_TO) AS VARCHAR(2)), 2) + '01' AS DATETIME))
                        WHEN @_TIME_INTERVAL = 2 THEN -- BY YEAR
                           DATEADD(YEAR, 1, CAST(CAST(YEAR(@_DATE_TO) AS VARCHAR(4)) + '0101' AS DATETIME))
                     END
              
   -- PREPARE THE TIME INTERVALS TABLE
   SET @_DAY_VAR = @_DATE_FROM
   WHILE @_DAY_VAR < @_DATE_TO AND @_DAY_VAR < GETDATE()
   BEGIN

         -- SET THE LINK FIELD FOR THE RESULTS 
         IF @_BASE_TYPE = 0
          BEGIN
            -- LINK WITH TABLE TYPES
            INSERT INTO @_DAYS_AND_TABLES 
                 SELECT   CAST(@_DAY_VAR AS DATETIME), X.GTT_GAMING_TABLE_TYPE_ID, X.GTT_NAME AS TABLE_IDENT, NULL, NULL
                   FROM   GAMING_TABLES_TYPES AS X
                  WHERE   X.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )  
          END 
         ELSE
          BEGIN
            -- LINK WITH TABLES
            INSERT INTO @_DAYS_AND_TABLES 
                 SELECT   CAST(@_DAY_VAR AS DATETIME), X.GT_GAMING_TABLE_ID, X.GT_NAME AS TABLE_IDENT, Z.GTT_GAMING_TABLE_TYPE_ID, Z.GTT_NAME 
                   FROM   GAMING_TABLES AS X
              LEFT JOIN   GAMING_TABLES_TYPES AS Z
                     ON   Z.GTT_GAMING_TABLE_TYPE_ID = X.GT_TYPE_ID
                  WHERE   Z.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
          END
          
          -- SET INCREMENT
          SET @_DAY_VAR = CASE 
                               WHEN @_TIME_INTERVAL = 0 THEN DATEADD(DAY,1,@_DAY_VAR)     -- DAY
                               WHEN @_TIME_INTERVAL = 1 THEN DATEADD(MONTH,1,@_DAY_VAR)   -- MONTH
                               WHEN @_TIME_INTERVAL = 2 THEN DATEADD(YEAR,1,@_DAY_VAR)    -- YEAR
                          END
   END

END -- IF INTERVALS <> - 1
ELSE
BEGIN
   
   -- SET THE LINK FIELD FOR THE RESULTS WITHOUT INTERVALS
   IF @_BASE_TYPE = 0
    BEGIN
      -- LINK WITH TABLE TYPES
      INSERT INTO   @_DAYS_AND_TABLES 
           SELECT   NULL, X.GTT_GAMING_TABLE_TYPE_ID, X.GTT_NAME AS TABLE_IDENT, NULL, NULL
             FROM   GAMING_TABLES_TYPES AS X
            WHERE   X.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE ) 
    END 
   ELSE
    BEGIN
      -- LINK WITH TABLES
      INSERT INTO   @_DAYS_AND_TABLES 
           SELECT   NULL, X.GT_GAMING_TABLE_ID, X.GT_NAME AS TABLE_IDENT, Z.GTT_GAMING_TABLE_TYPE_ID, Z.GTT_NAME 
             FROM   GAMING_TABLES AS X
        LEFT JOIN   GAMING_TABLES_TYPES AS Z
               ON   Z.GTT_GAMING_TABLE_TYPE_ID = X.GT_TYPE_ID
            WHERE   Z.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )   
    END
   
 END
  
-- SELECT INTO TEMPORARY TABLE WITH ALL DATA
-- SPECIFIC REPORT QUERY 
  SELECT   X.TABLE_IDENTIFIER   
         , X.TABLE_NAME
         , X.TABLE_TYPE
         , X.TABLE_TYPE_NAME
         , X.CM_DATE_ONLY    
         , MIN(X.OPEN_HOUR) AS OPEN_HOUR
         , MAX(X.CLOSE_HOUR) AS CLOSE_HOUR
         , SUM(X.SESSION_HOURS) AS SESSION_HOURS
         , SUM(X.BUY_IN) AS BUY_IN
         , SUM(X.CHIPS_IN) AS CHIPS_IN
         , SUM(X.CHIPS_OUT) AS CHIPS_OUT
         , SUM(X.TOTAL_PLAYED) AS TOTAL_PLAYED
         , SUM(X.AVERAGE_BET) AS AVERAGE_BET
         , SUM(X.CHIPS_IN) + SUM(X.BUY_IN) AS TOTAL_DROP
         , CASE WHEN (SUM(X.CHIPS_IN) + SUM(X.BUY_IN)) =0
		   		   THEN 0
		   		   ELSE ((SUM(X.CHIPS_OUT)/(SUM(X.CHIPS_IN) + SUM(X.BUY_IN)))*100)
		       END AS HOLD
		     , MIN(X.THEORIC_HOLD) AS THEORIC_HOLD
         , CASE WHEN SUM(X.TOTAL_PLAYED) =0
		   		   THEN 0
		   		   ELSE ((SUM(X.NETWIN)/SUM(X.TOTAL_PLAYED)) * 100)
		       END AS PAYOUT
		     , SUM(X.NETWIN) AS NETWIN
		     , X.ISO_CODE
         
   INTO #GT_TEMPORARY_REPORT_DATA
   
   FROM (  
            -- CORE QUERY
					  SELECT   CASE 
												WHEN @_TIME_INTERVAL = 0 THEN     -- TO FILTER BY DAY
														DATEADD(dd, 0, DATEDIFF(dd, 0, CS_OPENING_DATE))
												WHEN @_TIME_INTERVAL = 1 THEN     -- TO FILTER BY MONTH
														CAST(CAST(YEAR(CS_OPENING_DATE) AS VARCHAR(4)) + RIGHT('0' + CAST(MONTH(CS_OPENING_DATE) AS VARCHAR(2)), 2) + '01' AS DATETIME)
												WHEN @_TIME_INTERVAL = 2 THEN     -- TO FILTER BY YEAR
														CAST(CAST(YEAR(CS_OPENING_DATE) AS VARCHAR(4)) + '01'+ '01' AS DATETIME)
										 END AS CM_DATE_ONLY 
									 , (CASE WHEN @_BASE_TYPE = 0 THEN GTT_GAMING_TABLE_TYPE_ID ELSE GTPS_GAMING_TABLE_ID END)  AS TABLE_IDENTIFIER			-- GET THE BASE TYPE IDENTIFIER
									 , (CASE WHEN @_BASE_TYPE = 0 THEN GTT_NAME ELSE GT_NAME END) AS TABLE_NAME																					-- GET THE BASE TYPE IDENTIFIER NAME
									 , (CASE WHEN @_BASE_TYPE = 0 THEN NULL ELSE GTT_GAMING_TABLE_TYPE_ID END) AS TABLE_TYPE														-- TYPE 
									 , (CASE WHEN @_BASE_TYPE = 0 THEN NULL ELSE GTT_NAME END) AS TABLE_TYPE_NAME																				-- TYPE NAME
									 , SUM(GTPS_TOTAL_SELL_CHIPS) AS BUY_IN  
									 , SUM(GTPS_CHIPS_IN) AS CHIPS_IN
									 , SUM(GTPS_CHIPS_OUT)  AS CHIPS_OUT
									 , SUM(GTPS_PLAYED_AMOUNT) AS TOTAL_PLAYED
									 , SUM(GTPS_PLAYED_AVERAGE)  AS AVERAGE_BET
									 , SUM(GTPS_NETWIN) AS NETWIN
									 , GTPS_ISO_CODE AS ISO_CODE
									 , GT_THEORIC_HOLD  AS THEORIC_HOLD                                             
									 , CS_OPENING_DATE AS OPEN_HOUR
									 , CS_CLOSING_DATE AS CLOSE_HOUR
									 , DATEDIFF(MINUTE, CS_OPENING_DATE, ISNULL(CS_CLOSING_DATE, GETDATE())) AS SESSION_HOURS									 
						  FROM   GT_PLAY_SESSIONS   
						 INNER   JOIN GAMING_TABLES ON GT_GAMING_TABLE_ID = GTPS_GAMING_TABLE_ID
						 INNER   JOIN GAMING_TABLES_TYPES ON GT_TYPE_ID = GTT_GAMING_TABLE_TYPE_ID
						 INNER   JOIN GAMING_TABLES_SESSIONS ON GTPS_GAMING_TABLE_SESSION_ID = GTS_GAMING_TABLE_SESSION_ID
						 INNER   JOIN CASHIER_SESSIONS ON GTS_CASHIER_SESSION_ID = CS_SESSION_ID
						   AND   CS_OPENING_DATE >= @pDateFrom AND CS_OPENING_DATE < @pDateTo
						 WHERE   CS_STATUS = 1   
						  AND    GT_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
						  AND    GTPS_ISO_CODE IN (@pSelectedCurrency)
						 GROUP   BY   GTPS_GAMING_TABLE_ID
									 , GT_NAME
									 , CS_OPENING_DATE 
									 , CS_CLOSING_DATE 
									 , GTT_GAMING_TABLE_TYPE_ID
									 , GTT_NAME
									 , GT_THEORIC_HOLD
									 , GTPS_ISO_CODE
             -- END CORE QUERY      
        ) AS X          
 GROUP BY  X.TABLE_IDENTIFIER   
         , X.TABLE_NAME
         , X.TABLE_TYPE
         , X.TABLE_TYPE_NAME
         , X.CM_DATE_ONLY
         , X.ISO_CODE    

  -- Group by indentifier and time interval 

IF @_TIME_INTERVAL <> -1
BEGIN
   -- INTERVALS DATES AND TABLE FINAL PREPARATION

   -- FILTER ACTIVITY
   IF @_ONLY_ACTIVITY = 0
    BEGIN
      -- JOIN THE SELECT WITH ALL DATA
			SELECT   DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER,
							 DT.TABLE_IDENT_NAME AS TABLE_NAME,
							 DT.TABLE_TYPE_IDENT AS TABLE_TYPE, 
							 DT.TABLE_TYPE_NAME AS TABLE_TYPE_NAME,
							 ISNULL(ZZ.BUY_IN, 0) AS BUY_IN, 
			         ISNULL(ZZ.CHIPS_IN, 0) AS CHIPS_IN, 
               ISNULL(ZZ.CHIPS_OUT, 0) AS CHIPS_OUT, 
               ISNULL(ZZ.TOTAL_PLAYED, 0) AS TOTAL_PLAYED, 
               ISNULL(ZZ.AVERAGE_BET, 0) AS AVERAGE_BET, 
               ISNULL(ZZ.TOTAL_DROP, 0) AS TOTAL_DROP, 
               ISNULL(ZZ.HOLD, 0) AS HOLD, 
               ISNULL(ZZ.THEORIC_HOLD, 0) AS THEORIC_HOLD,
               ISNULL(ZZ.PAYOUT, 0) AS PAYOUT, 
               ISNULL(ZZ.NETWIN, 0) AS NETWIN,
               ISO_CODE,
               ZZ.OPEN_HOUR,
               ZZ.CLOSE_HOUR,
               CASE WHEN ZZ.CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED,
               CASE WHEN ZZ.SESSION_HOURS = 0 THEN DATEDIFF(MINUTE,ZZ.OPEN_HOUR,GETDATE()) ELSE ISNULL(ZZ.SESSION_HOURS, 0) END AS SESSION_HOURS,
               DATE_TIME               
        FROM   @_DAYS_AND_TABLES DT
   LEFT JOIN   #GT_TEMPORARY_REPORT_DATA ZZ 
               ON (
									(@_TIME_INTERVAL = 0 AND DATE_TIME = ZZ.CM_DATE_ONLY)
                  OR (@_TIME_INTERVAL = 1 AND MONTH(DATE_TIME) = MONTH(ZZ.CM_DATE_ONLY) AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
                  OR (@_TIME_INTERVAL = 2 AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
                )
         AND   TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER
       -- SET ORDER 
		ORDER BY   DT.TABLE_TYPE_IDENT ASC,
							 CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC,
							 DATE_TIME DESC;                                             
                    
    END
   ELSE
    BEGIN
      -- JOIN THE SELECT HIDDING WITHOUT ACTIVITY
      SELECT	DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER,
              DT.TABLE_IDENT_NAME AS TABLE_NAME,
              DT.TABLE_TYPE_IDENT AS TABLE_TYPE, 
              DT.TABLE_TYPE_NAME AS TABLE_TYPE_NAME,
              ISNULL(ZZ.BUY_IN, 0) AS BUY_IN, 
			        ISNULL(ZZ.CHIPS_IN, 0) AS CHIPS_IN, 
              ISNULL(ZZ.CHIPS_OUT, 0) AS CHIPS_OUT, 
              ISNULL(ZZ.TOTAL_PLAYED, 0) AS TOTAL_PLAYED, 
              ISNULL(ZZ.AVERAGE_BET, 0) AS AVERAGE_BET, 
              ISNULL(ZZ.TOTAL_DROP, 0) AS TOTAL_DROP, 
              ISNULL(ZZ.HOLD, 0) AS HOLD, 
              ISNULL(ZZ.THEORIC_HOLD, 0) AS THEORIC_HOLD,
              ISNULL(ZZ.PAYOUT, 0) AS PAYOUT, 
              ISNULL(ZZ.NETWIN, 0) AS NETWIN,
              ISO_CODE,
              ZZ.OPEN_HOUR,
              ZZ.CLOSE_HOUR,
              CASE WHEN ZZ.CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED,
              CASE WHEN ZZ.SESSION_HOURS = 0 THEN DATEDIFF(MINUTE,ZZ.OPEN_HOUR,GETDATE()) ELSE ISNULL(ZZ.SESSION_HOURS, 0) END AS SESSION_HOURS,
              DATE_TIME
        FROM	@_DAYS_AND_TABLES DT        
  INNER JOIN	#GT_TEMPORARY_REPORT_DATA ZZ 
              ON(
                 (@_TIME_INTERVAL = 0 AND DATE_TIME = ZZ.CM_DATE_ONLY)
                 OR (@_TIME_INTERVAL = 1 AND MONTH(DATE_TIME) = MONTH(ZZ.CM_DATE_ONLY) AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
                 OR (@_TIME_INTERVAL = 2 AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
              )
              AND   TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER
       -- SET ORDER             
   ORDER BY   DT.TABLE_TYPE_IDENT ASC,
              CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC,
              DATE_TIME DESC;
                  
    END -- IF ONLY_ACTIVITY

END
ELSE  -- ELSE WITHOUT INTERVALS
BEGIN
   
   -- FINAL WITHOUT INTERVALS 
   
      -- FILTER ACTIVITY
   IF @_ONLY_ACTIVITY = 0
    BEGIN
            -- JOIN DATA INCLUDING WITHOUT ACTIVITY
            SELECT   DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER,
                     DT.TABLE_IDENT_NAME AS TABLE_NAME,
                     DT.TABLE_TYPE_IDENT AS TABLE_TYPE, 
                     DT.TABLE_TYPE_NAME,
										 ISNULL(ZZ.BUY_IN, 0) AS BUY_IN, 
										 ISNULL(ZZ.CHIPS_IN, 0) AS CHIPS_IN, 
										 ISNULL(ZZ.CHIPS_OUT, 0) AS CHIPS_OUT, 
										 ISNULL(ZZ.TOTAL_PLAYED, 0) AS TOTAL_PLAYED, 
										 ISNULL(ZZ.AVERAGE_BET, 0) AS AVERAGE_BET, 
										 ISNULL(ZZ.TOTAL_DROP, 0) AS TOTAL_DROP, 
										 ISNULL(ZZ.HOLD, 0) AS HOLD,
										 ISNULL(ZZ.THEORIC_HOLD, 0) AS THEORIC_HOLD, 
										 ISNULL(ZZ.PAYOUT, 0) AS PAYOUT, 
										 ISNULL(ZZ.NETWIN, 0) AS NETWIN, 
										 ISO_CODE,
                     OPEN_HOUR,
                     CLOSE_HOUR,
                     CASE WHEN CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED,
                     CASE WHEN SESSION_HOURS = 0 THEN DATEDIFF(MINUTE, OPEN_HOUR, GETDATE()) ELSE ISNULL(SESSION_HOURS, 0) END AS SESSION_HOURS
              FROM   @_DAYS_AND_TABLES DT                 
         LEFT JOIN   #GT_TEMPORARY_REPORT_DATA ZZ
                ON   DT.TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER              
          ORDER BY   DT.TABLE_TYPE_IDENT ASC,
                     CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC,
                     DATE_TIME DESC;                            

     END
    ELSE
     BEGIN
            -- JOIN DATA WITH ONLY ACTIVITY
            SELECT   DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER,
                     DT.TABLE_IDENT_NAME AS TABLE_NAME,
                     DT.TABLE_TYPE_IDENT AS TABLE_TYPE, 
                     DT.TABLE_TYPE_NAME,
                     ISNULL(ZZ.BUY_IN, 0) AS BUY_IN, 
										 ISNULL(ZZ.CHIPS_IN, 0) AS CHIPS_IN, 
										 ISNULL(ZZ.CHIPS_OUT, 0) AS CHIPS_OUT, 
										 ISNULL(ZZ.TOTAL_PLAYED, 0) AS TOTAL_PLAYED, 
										 ISNULL(ZZ.AVERAGE_BET, 0) AS AVERAGE_BET, 
										 ISNULL(ZZ.TOTAL_DROP, 0) AS TOTAL_DROP, 
										 ISNULL(ZZ.HOLD, 0) AS HOLD,
										 ISNULL(ZZ.THEORIC_HOLD, 0) AS THEORIC_HOLD, 
										 ISNULL(ZZ.PAYOUT, 0) AS PAYOUT,
										 ISNULL(ZZ.NETWIN, 0) AS NETWIN,
										 ISO_CODE,  
                     OPEN_HOUR,
                     CLOSE_HOUR,
                     CASE WHEN CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED,
                     CASE WHEN SESSION_HOURS = 0 THEN DATEDIFF(MINUTE, OPEN_HOUR, GETDATE()) ELSE ISNULL(SESSION_HOURS, 0) END AS SESSION_HOURS
              FROM   @_DAYS_AND_TABLES DT               
        INNER JOIN   #GT_TEMPORARY_REPORT_DATA ZZ
                ON   DT.TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER                
          ORDER BY   DT.TABLE_TYPE_IDENT ASC,
                     CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC,
                     DATE_TIME DESC;                            
                      
     END  
 END

-- ERASE THE TEMPORARY DATA 
DROP TABLE #GT_TEMPORARY_REPORT_DATA

END -- END PROCEDURE
GO

  
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GT_Session_Information]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[GT_Session_Information]
GO


CREATE PROCEDURE [dbo].[GT_Session_Information]
(
    @pBaseType            INTEGER
  , @pDateFrom            DATETIME
  , @pDateTo              DATETIME
  , @pStatus              INTEGER
  , @pEnabled             INTEGER
  , @pAreaId              INTEGER
  , @pBankId              INTEGER
  , @pChipsISOCode        VARCHAR(50)
  , @pChipsCoinsCode      INTEGER
  , @pValidTypes          VARCHAR(4096)
)
AS
BEGIN

----------------------------------------------------------------------------------------------------------------
DECLARE @_BASE_TYPE        AS   INTEGER
DECLARE @_DATE_FROM        AS   DATETIME
DECLARE @_DATE_TO          AS   DATETIME
DECLARE @_STATUS           AS   INTEGER
DECLARE @_ENABLED          AS   INTEGER
DECLARE @_AREA             AS   INTEGER
DECLARE @_BANK             AS   INTEGER
DECLARE @_CHIPS_ISO_CODE   AS   VARCHAR(50)
DECLARE @_CHIPS_COINS_CODE AS   VARCHAR(50)
DECLARE @_DELIMITER        AS   CHAR(1)
DECLARE @_TYPES_TABLE           TABLE(SST_ID INT, SST_VALUE VARCHAR(50)) -- TO STORE THE VALID GAMING TABLE TYPES

-- CHIP TYPES
DECLARE @_CHIPS_RE    AS INTEGER
DECLARE @_CHIPS_NR    AS INTEGER
DECLARE @_CHIPS_COLOR AS INTEGER

-- MOVEMENTS
DECLARE @_CHIPS_MOVEMENT_PURCHASE_TOTAL          AS   INTEGER
DECLARE @_CHIPS_MOVEMENT_SALES_TOTAL             AS   INTEGER
DECLARE @_CHIPS_MOVEMENT_PURCHASE_TOTAL_EXCHANGE AS   INTEGER
DECLARE @_CHIPS_MOVEMENT_SALES_TOTAL_EXCHANGE    AS   INTEGER
DECLARE @_MOVEMENT_FILLER_IN                     AS   INTEGER
DECLARE @_MOVEMENT_FILLER_OUT                    AS   INTEGER
DECLARE @_MOVEMENT_CAGE_FILLER_OUT               AS   INTEGER
DECLARE @_MOVEMENT_CAGE_CLOSE_SESSION            AS   INTEGER
DECLARE @_MOVEMENT_CLOSE_SESSION                 AS   INTEGER

----------------------------------------------------------------------------------------------------------------
-- Initialization --
SET @_DELIMITER        =   ','
SET @_BASE_TYPE        =   @pBaseType
SET @_DATE_FROM        =   @pDateFrom
SET @_DATE_TO          =   @pDateTo
SET @_STATUS           =   ISNULL(@pStatus, -1)
SET @_ENABLED          =   ISNULL(@pEnabled, -1)
SET @_AREA             =   ISNULL(@pAreaId, 0)
SET @_BANK             =   ISNULL(@pBankId, 0)
SET @_CHIPS_ISO_CODE   =   ISNULL(@pChipsISOCode, 'X01')
SET @_CHIPS_COINS_CODE =   ISNULL(@pChipsCoinsCode, -100)
SET @_CHIPS_RE         =   1001
SET @_CHIPS_NR         =   1002
SET @_CHIPS_COLOR      =   1003

SET @_CHIPS_MOVEMENT_PURCHASE_TOTAL          =   304
SET @_CHIPS_MOVEMENT_SALES_TOTAL             =   303
SET @_CHIPS_MOVEMENT_PURCHASE_TOTAL_EXCHANGE =   310
SET @_CHIPS_MOVEMENT_SALES_TOTAL_EXCHANGE    =   309
SET @_MOVEMENT_FILLER_IN                     =   2
SET @_MOVEMENT_FILLER_OUT                    =   3
SET @_MOVEMENT_CAGE_FILLER_OUT               =   203
SET @_MOVEMENT_CAGE_CLOSE_SESSION            =   201
SET @_MOVEMENT_CLOSE_SESSION                 =   1

----------------------------------------------------------------------------------------------------------------
-- CHECK DATE PARAMETERS
IF @_DATE_FROM IS NULL
BEGIN
   -- IF DATE FROM IS NULL, USER FIRST POSIBLE DATE
   SET @_DATE_FROM = CAST('' AS DATETIME)
END

IF @_DATE_TO IS NULL
BEGIN
   -- IF DATE TO IS NULL, QUERY UNTIL CURRENT DATE
   SET @_DATE_TO = CAST(GETDATE() AS DATETIME)
END

-- ASSIGN TYPES PARAMETER INTO TABLE
INSERT INTO @_TYPES_TABLE SELECT * FROM dbo.SplitStringIntoTable(@pValidTypes, @_DELIMITER, DEFAULT)

----------------------------------------------------------------------------------------------------------------

IF @_BASE_TYPE = 0
BEGIN
  -- REPORT BY GAMING TABLE SESSION
  SELECT    GTS_CASHIER_SESSION_ID
          , CS_NAME
          , CS_STATUS
          , GT_NAME
          , GTT_NAME
          , GT_HAS_INTEGRATED_CASHIER
          , CT_NAME
          , GU_USERNAME
          , CS_OPENING_DATE
          , CS_CLOSING_DATE
          , SUM(ISNULL(GTSC_TOTAL_PURCHASE_AMOUNT, 0) )   AS GTS_TOTAL_PURCHASE_AMOUNT
          , SUM(ISNULL(GTSC_TOTAL_SALES_AMOUNT, 0)    )   AS GTS_TOTAL_SALES_AMOUNT
          , SUM(ISNULL(GTSC_INITIAL_CHIPS_AMOUNT, 0)  )   AS GTS_INITIAL_CHIPS_AMOUNT
          , SUM(ISNULL(GTSC_FILLS_CHIPS_AMOUNT, 0)    )   AS GTS_FILLS_CHIPS_AMOUNT
          , SUM(ISNULL(GTSC_CREDITS_CHIPS_AMOUNT, 0)  )   AS GTS_CREDITS_CHIPS_AMOUNT
          , SUM(ISNULL(GTSC_FINAL_CHIPS_AMOUNT, 0)    )   AS GTS_FINAL_CHIPS_AMOUNT
          , SUM(ISNULL(GTSC_TIPS, 0)                  )   AS GTS_TIPS
          , SUM(ISNULL(GTSC_COLLECTED_AMOUNT, 0)      )   AS GTS_COLLECTED_AMOUNT
          , SUM(ISNULL(GTS_CLIENT_VISITS, 0)          )   AS GTS_CLIENT_VISITS
          , AR_NAME
          , BK_NAME
          , GTS_GAMING_TABLE_SESSION_ID
          , dbo.GT_Calculate_DROP(SUM(ISNULL(GTSC_OWN_SALES_AMOUNT, 0)), SUM(ISNULL(GTSC_EXTERNAL_SALES_AMOUNT, 0)), SUM(ISNULL(GTSC_COLLECTED_AMOUNT, 0)), GT_HAS_INTEGRATED_CASHIER) AS DROP_BY_CURRENCY
          , GTSC_ISO_CODE
          , dbo.GT_Calculate_DROP(ISNULL(GTS_OWN_SALES_AMOUNT, 0), ISNULL(GTS_EXTERNAL_SALES_AMOUNT, 0), ISNULL(GTS_COLLECTED_AMOUNT, 0), GT_HAS_INTEGRATED_CASHIER) AS DROP_LOCAL_CURRENCY
   FROM         GAMING_TABLES_SESSIONS
   LEFT    JOIN GAMING_TABLES_SESSIONS_BY_CURRENCY ON GTS_GAMING_TABLE_SESSION_ID = GTSC_GAMING_TABLE_SESSION_ID AND GTSC_TYPE <> @_CHIPS_COLOR
   INNER   JOIN CASHIER_SESSIONS                   ON CS_SESSION_ID               = GTS_CASHIER_SESSION_ID
   LEFT    JOIN GUI_USERS                           ON GU_USER_ID                  = CS_USER_ID
   LEFT    JOIN GAMING_TABLES                       ON GT_GAMING_TABLE_ID          = GTS_GAMING_TABLE_ID
   LEFT    JOIN GAMING_TABLES_TYPES                 ON GTT_GAMING_TABLE_TYPE_ID    = GT_TYPE_ID
   LEFT    JOIN AREAS                               ON GT_AREA_ID                  = AR_AREA_ID
   LEFT    JOIN BANKS                               ON GT_BANK_ID                  = BK_BANK_ID
   LEFT    JOIN CASHIER_TERMINALS                   ON GT_CASHIER_ID               = CT_CASHIER_ID
 WHERE   CS_OPENING_DATE >= @_DATE_FROM
   AND   CS_OPENING_DATE < @_DATE_TO
   AND   (GT_ENABLED = CASE WHEN @_ENABLED = -1 THEN GT_ENABLED ELSE @_ENABLED END)
   AND   (CS_STATUS  = CASE WHEN @_STATUS  = -1 THEN CS_STATUS  ELSE @_STATUS  END)
   AND   (GT_AREA_ID = CASE WHEN @_AREA    = 0  THEN GT_AREA_ID ELSE @_AREA    END)
   AND   (GT_BANK_ID = CASE WHEN @_BANK    = 0  THEN GT_BANK_ID ELSE @_BANK    END)
   AND   (GT_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE ))
   GROUP BY GTS_CASHIER_SESSION_ID
         , CS_NAME
         , CS_STATUS
         , GT_NAME
         , GTT_NAME
         , GT_HAS_INTEGRATED_CASHIER
         , CT_NAME
         , GU_USERNAME
         , CS_OPENING_DATE
         , CS_CLOSING_DATE
         , AR_NAME
         , BK_NAME
         , GTS_GAMING_TABLE_SESSION_ID
         , GTS_OWN_SALES_AMOUNT
         , GTS_EXTERNAL_SALES_AMOUNT
         , GTS_COLLECTED_AMOUNT
         , GTSC_ISO_CODE
     ORDER BY CS_OPENING_DATE
;

END
ELSE
BEGIN
  -- REPORT BY CASHIER MOVEMENTS

  SELECT   CM_SESSION_ID
         , CS_NAME
         , CS_STATUS
         , GT_NAME
         , GTT_NAME
         , GT_HAS_INTEGRATED_CASHIER
         , CT_NAME
         , GU_USERNAME
         , CS_OPENING_DATE
         , CS_CLOSING_DATE
         , SUM(CASE WHEN CM_TYPE                  = @_CHIPS_MOVEMENT_PURCHASE_TOTAL          THEN CM_ADD_AMOUNT
                    WHEN CM_TYPE                  = @_CHIPS_MOVEMENT_PURCHASE_TOTAL_EXCHANGE THEN CM_INITIAL_BALANCE
                    ELSE 0
               END
              )  AS CM_TOTAL_PURCHASE_AMOUNT
         , SUM(CASE WHEN CM_TYPE                  = @_CHIPS_MOVEMENT_SALES_TOTAL             THEN CM_SUB_AMOUNT
                    WHEN CM_TYPE                  = @_CHIPS_MOVEMENT_SALES_TOTAL_EXCHANGE    THEN CM_INITIAL_BALANCE
                    ELSE 0
               END
              )  AS CM_TOTAL_SALES_AMOUNT
         , SUM(CASE WHEN (CM_CURRENCY_ISO_CODE    = @_CHIPS_ISO_CODE
                     OR  CM_CAGE_CURRENCY_TYPE  IN (@_CHIPS_RE, @_CHIPS_NR))
                     AND CM_TYPE                  = @_MOVEMENT_FILLER_IN
                    THEN CM_ADD_AMOUNT ELSE 0 END
              )  AS CM_INITIAL_CHIPS_AMOUNT
         , SUM(CASE WHEN (CM_CURRENCY_ISO_CODE    = @_CHIPS_ISO_CODE
                     OR  CM_CAGE_CURRENCY_TYPE  IN (@_CHIPS_RE, @_CHIPS_NR))
                     AND CM_TYPE                  = @_MOVEMENT_FILLER_IN
                    THEN CM_ADD_AMOUNT
                    ELSE 0
               END
              )  AS CM_FILLS_CHIPS_AMOUNT
         , SUM(CASE WHEN (CM_CURRENCY_ISO_CODE    = @_CHIPS_ISO_CODE
                       OR  CM_CAGE_CURRENCY_TYPE   IN (@_CHIPS_RE, @_CHIPS_NR))
                       AND CM_TYPE                  = @_MOVEMENT_FILLER_OUT
                      THEN CM_SUB_AMOUNT
                    WHEN (CM_CURRENCY_ISO_CODE    = @_CHIPS_ISO_CODE
                       OR  CM_CAGE_CURRENCY_TYPE  IN (@_CHIPS_RE, @_CHIPS_NR))
                       AND CM_TYPE                  = @_MOVEMENT_CAGE_CLOSE_SESSION
                     THEN CM_INITIAL_BALANCE
                    ELSE 0
               END
              )  AS CM_CREDITS_CHIPS_AMOUNT
         , SUM(CASE WHEN (CM_CURRENCY_ISO_CODE    = @_CHIPS_ISO_CODE
                     OR  CM_CAGE_CURRENCY_TYPE   IN (@_CHIPS_RE, @_CHIPS_NR))
                     AND CM_TYPE                  = @_MOVEMENT_FILLER_OUT
                    THEN CM_SUB_AMOUNT
                    WHEN (CM_CURRENCY_ISO_CODE    = @_CHIPS_ISO_CODE
                     OR  CM_CAGE_CURRENCY_TYPE  IN (@_CHIPS_RE, @_CHIPS_NR))
                     AND CM_TYPE                  = @_MOVEMENT_CAGE_CLOSE_SESSION
                    THEN CM_INITIAL_BALANCE
                    ELSE 0 END
              )  AS CM_FINAL_CHIPS_AMOUNT
         , SUM(CASE WHEN CM_TYPE                  = @_MOVEMENT_CAGE_FILLER_OUT
                     AND ((CM_CURRENCY_ISO_CODE   = @_CHIPS_ISO_CODE
                     AND CM_CURRENCY_DENOMINATION = @_CHIPS_COINS_CODE)
                      OR CM_CAGE_CURRENCY_TYPE  IN (@_CHIPS_RE, @_CHIPS_NR))
                    THEN CM_SUB_AMOUNT
                    WHEN CM_TYPE                  = @_MOVEMENT_CAGE_CLOSE_SESSION
                     AND ((CM_CURRENCY_ISO_CODE   = @_CHIPS_ISO_CODE
                     AND CM_CURRENCY_DENOMINATION = @_CHIPS_COINS_CODE)
                      OR CM_CAGE_CURRENCY_TYPE  IN (@_CHIPS_RE, @_CHIPS_NR))
                    THEN CM_INITIAL_BALANCE
                    ELSE 0
               END
              )  AS CM_TIPS
         , SUM(CASE WHEN GT_HAS_INTEGRATED_CASHIER = 0 THEN --Only gaming table
                        CASE WHEN CM_CURRENCY_ISO_CODE IS NULL
                               OR (CM_CURRENCY_ISO_CODE <> @_CHIPS_ISO_CODE
                              AND  CM_CAGE_CURRENCY_TYPE NOT IN (@_CHIPS_RE, @_CHIPS_NR, @_CHIPS_COLOR) ) THEN
                             CASE WHEN CM_TYPE  = @_MOVEMENT_FILLER_IN            THEN -1 * ISNULL(CM_AUX_AMOUNT, CM_ADD_AMOUNT)
                                  WHEN CM_TYPE  = @_MOVEMENT_FILLER_OUT           THEN      ISNULL(CM_AUX_AMOUNT, CM_SUB_AMOUNT)
                                  WHEN CM_TYPE  = @_MOVEMENT_CAGE_CLOSE_SESSION   THEN      ISNULL(CM_AUX_AMOUNT, CM_INITIAL_BALANCE)
                                  ELSE 0
                             END
                             ELSE 0
                        END
                    WHEN GT_HAS_INTEGRATED_CASHIER = 1 THEN --Gaming table with cashier
                        CASE WHEN CM_TYPE   = @_CHIPS_MOVEMENT_PURCHASE_TOTAL THEN -1 * CM_ADD_AMOUNT
                             WHEN CM_TYPE   = @_CHIPS_MOVEMENT_SALES_TOTAL    THEN      CM_SUB_AMOUNT
                             ELSE 0
                        END
                    ELSE 0
               END
              )  AS CM_COLLECTED_AMOUNT
         , SUM(ISNULL(GTS_CLIENT_VISITS, 0))   AS GTS_CLIENT_VISITS
         , AR_NAME
         , BK_NAME
         , CM_GAMING_TABLE_SESSION_ID
         , dbo.GT_Calculate_DROP(SUM(ISNULL(GTSC_OWN_SALES_AMOUNT, 0)), SUM(ISNULL(GTSC_EXTERNAL_SALES_AMOUNT, 0)), SUM(ISNULL(GTSC_COLLECTED_AMOUNT, 0)), GT_HAS_INTEGRATED_CASHIER) AS DROP_BY_CURRENCY
         , CM_CURRENCY_ISO_CODE
         , dbo.GT_Calculate_DROP(SUM(ISNULL(GTS_OWN_SALES_AMOUNT, 0)), SUM(ISNULL(GTS_EXTERNAL_SALES_AMOUNT, 0)), SUM(ISNULL(GTS_COLLECTED_AMOUNT, 0)), GT_HAS_INTEGRATED_CASHIER) AS DROP_LOCAL_CURRENCY
   FROM         CASHIER_MOVEMENTS
  INNER   JOIN  CASHIER_SESSIONS                   ON CS_SESSION_ID               = CM_SESSION_ID
  INNER   JOIN  GAMING_TABLES_SESSIONS             ON CS_SESSION_ID               = GTS_CASHIER_SESSION_ID
   LEFT   JOIN  GAMING_TABLES_SESSIONS_BY_CURRENCY ON GTS_GAMING_TABLE_SESSION_ID = GTSC_GAMING_TABLE_SESSION_ID AND GTSC_TYPE <> @_CHIPS_COLOR
   LEFT   JOIN  GUI_USERS                          ON GU_USER_ID                  = CS_USER_ID
   LEFT   JOIN  GAMING_TABLES                      ON GT_GAMING_TABLE_ID          = GTS_GAMING_TABLE_ID
   LEFT   JOIN  GAMING_TABLES_TYPES                ON GTT_GAMING_TABLE_TYPE_ID    = GT_TYPE_ID
   LEFT   JOIN  AREAS                              ON GT_AREA_ID                  = AR_AREA_ID
   LEFT   JOIN  BANKS                              ON GT_BANK_ID                  = BK_BANK_ID
   LEFT   JOIN  CASHIER_TERMINALS                  ON GT_CASHIER_ID               = CT_CASHIER_ID
 WHERE   CM_DATE >= @_DATE_FROM
   AND   CM_DATE <= @_DATE_TO
   AND   (GT_ENABLED = CASE WHEN @_ENABLED = -1 THEN GT_ENABLED ELSE @_ENABLED END)
   AND   (CS_STATUS  = CASE WHEN @_STATUS  = -1 THEN CS_STATUS  ELSE @_STATUS  END)
   AND   (GT_AREA_ID = CASE WHEN @_AREA    = 0  THEN GT_AREA_ID ELSE @_AREA    END)
   AND   (GT_BANK_ID = CASE WHEN @_BANK    = 0  THEN GT_BANK_ID ELSE @_BANK    END)
   AND   (GT_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE ))
  GROUP BY  CM_SESSION_ID
      , CS_NAME
      , CS_STATUS
      , GT_NAME
      , GTT_NAME
      , GT_HAS_INTEGRATED_CASHIER
      , CT_NAME
      , GU_USERNAME
      , CS_OPENING_DATE
      , CS_CLOSING_DATE
      , AR_NAME
      , BK_NAME
      , CM_GAMING_TABLE_SESSION_ID
      , CM_CURRENCY_ISO_CODE
     ORDER BY CS_OPENING_DATE
END

END -- END PROCEDURE
GO

-- PERMISSIONS

GRANT EXECUTE ON [dbo].[GT_Session_Information] TO [wggui] WITH GRANT OPTION
GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetCashierTransactionsTaxes]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[GetCashierTransactionsTaxes]
GO

CREATE PROCEDURE [dbo].[GetCashierTransactionsTaxes] 
(     @pStartDatetime DATETIME,
      @pEndDatetime DATETIME,
      @pTerminalId INT,
      @pCmMovements NVARCHAR(MAX),
      @pHpMovements NVARCHAR(MAX),
      @pPaymentUserId INT,
      @pAuthorizeUserId INT,
      @pNationalIsoCode as NVARCHAR(5)
)
AS 
BEGIN
  DECLARE @_PAID int
  DECLARE @_SQL nVarChar(max)
  DECLARE @SelectPartToExecute INT -- 1:HP,  2:CM,  3:Both
      
  SET @_PAID = 32768
  
  SET @SelectPartToExecute = 3 

  IF @pHpMovements IS NOT NULL AND @pCmMovements IS NULL
  BEGIN
     SET @SelectPartToExecute = 2
  END
  ELSE IF @pHpMovements IS NULL AND @pCmMovements IS NOT NULL
  BEGIN
     SET @SelectPartToExecute = 1
  END
     
  
  SET @_SQL = '
    SELECT   AO_OPERATION_ID
           , MAX(CM_DATE)                  AS CM_DATE
           , MAX(CM_TYPE)                  AS CM_TYPE
           , PAY.GU_USERNAME               AS PAYMENT_USER 
           , CM_USER_NAME                  AS AUTHORIZATION_USER
           , SUM(CASE CM_TYPE 
                 WHEN   6 THEN CM_ADD_AMOUNT
                 WHEN  14 THEN CM_ADD_AMOUNT
                 ELSE 0 END )             AS TOTAL_TAX
           , SUM(CASE CM_TYPE 
                 WHEN 103 THEN CM_SUB_AMOUNT
                 WHEN 124 THEN CM_SUB_AMOUNT
                 WHEN 125 THEN CM_SUB_AMOUNT
                 WHEN 126 THEN CM_SUB_AMOUNT
                 ELSE 0 END )             AS TITO_TICKET_PAID
           , SUM(CASE CM_TYPE 
                 WHEN 304 THEN CM_ADD_AMOUNT
                 WHEN 310 THEN CM_ADD_AMOUNT
                 ELSE 0 END )             AS CHIPS_PURCHASE_TOTAL
           , SUM(CASE CM_TYPE 
                 WHEN  30 THEN CM_SUB_AMOUNT
                 WHEN  32 THEN CM_SUB_AMOUNT
                 ELSE 0 END )             AS HANDPAY
           , CAI_NAME                      AS REASON
           , CS_CASHIER_ID
           , CM_USER_ID
           , AO_COMMENT
           , CM_CASHIER_NAME
                     , SUM(CASE CM_TYPE 
                 WHEN 310 THEN CM_INITIAL_BALANCE
                 ELSE 0 END )             AS ORIGINAL_CHIPS_PURCHASE_TOTAL
            , MAX(CASE CM_CURRENCY_ISO_CODE WHEN ''' + @pNationalIsoCode + ''' THEN NULL ELSE CM_CURRENCY_ISO_CODE END) EXCHANGE_CURRENCY

      INTO   #TEMP_CASHIER_SESSIONS     
      FROM   CASHIER_MOVEMENTS  WITH(INDEX(IX_cm_date_type))
INNER JOIN   CASHIER_SESSIONS   ON CS_SESSION_ID   = CM_SESSION_ID 
INNER JOIN   ACCOUNT_OPERATIONS ON AO_OPERATION_ID = CM_OPERATION_ID
INNER JOIN   GUI_USERS AS PAY   ON PAY.GU_USER_ID  = CS_USER_ID
 LEFT JOIN   CATALOG_ITEMS      ON CAI_ID          = AO_REASON_ID
     WHERE   1 = 1 '
    + CASE WHEN @pStartDatetime IS NOT NULL THEN ' AND CM_DATE >= ''' + CONVERT(VARCHAR, @pStartDatetime, 21) + '''' ELSE '' END + CHAR(10) 
    + CASE WHEN @pEndDatetime IS NOT NULL THEN ' AND CM_DATE < ''' + CONVERT(VARCHAR, @pEndDatetime, 21) + '''' ELSE '' END + CHAR(10) 
+ '    AND   CM_TYPE IN (6, 14, 103, 124, 125, 126, 304, 310, 30, 32) '
    + CASE WHEN @pTerminalId IS NOT NULL THEN ' AND CS_CASHIER_ID = ' + RTRIM(LTRIM(CONVERT(VARCHAR, @pTerminalId, 21))) + '' ELSE '' END + CHAR(10) 
    + CASE WHEN @pPaymentUserId IS NOT NULL THEN ' AND PAY.GU_USER_ID = ' + RTRIM(LTRIM(CONVERT(VARCHAR, @pPaymentUserId, 21))) + '' ELSE '' END + CHAR(10) 
    + CASE WHEN @pAuthorizeUserId IS NOT NULL THEN ' AND CM_USER_ID =' + RTRIM(LTRIM(CONVERT(VARCHAR, @pAuthorizeUserId, 21))) + '' ELSE '' END + CHAR(10) 
+'GROUP BY   AO_OPERATION_ID
           , PAY.GU_USERNAME
           , CM_USER_NAME
           , CAI_NAME
           , CS_CASHIER_ID
           , CM_USER_ID
           , AO_COMMENT
           , CM_CASHIER_NAME
' + CHAR(10) 

IF @SelectPartToExecute = 3 OR @SelectPartToExecute = 2
BEGIN
  SET @_SQL = @_SQL + '
    SELECT   AO_OPERATION_ID
           , HP_DATETIME               AS DATETIME
           , HP_TE_NAME                AS TERMINAL_NAME
           , HP_TYPE                   AS TRANSACTION_TYPE
           , PAYMENT_USER              AS PAYMENT_USER 
           , AUTHORIZATION_USER        AS AUTHORIZATION_USER
           , HP_AMOUNT                 AS AFTER_TAX_AMOUNT
           , ISNULL(HP_AMOUNT,0) 
             - ISNULL(HP_TAX_AMOUNT,0) AS BEFORE_TAX_AMOUNT
           , REASON                    AS REASON
           , AO_COMMENT                AS COMMENT      
           , 1                         AS ORIGEN
           , HP_TYPE                   AS TYPE1
           , 0                         AS ORIGINAL_CHIPS_PURCHASE_TOTAL
           , hp_cur0                   AS EXCHANGE_CURRENCY
      FROM   #TEMP_CASHIER_SESSIONS
INNER JOIN   HANDPAYS          ON HP_OPERATION_ID  = AO_OPERATION_ID
     WHERE   1 = 1 '
    + CASE WHEN @pHpMovements IS NOT NULL THEN ' AND HP_TYPE IN (' + @pHpMovements + ') ' ELSE '' END + CHAR(10) 
--------      + CASE WHEN @pWithTaxes = 1 THEN ' AND HP_TAX_AMOUNT > 0' ELSE ' AND HP_TAX_AMOUNT = 0' END + CHAR(10) 
+ '   AND   HP_STATUS = ' + CAST(@_PAID AS NVARCHAR) + CHAR(10) 
END


IF @SelectPartToExecute = 3
BEGIN
  SET @_SQL = @_SQL + ' UNION ' + CHAR(10)
END

IF @SelectPartToExecute = 3 OR @SelectPartToExecute = 1
BEGIN
  SET @_SQL = @_SQL + '
    SELECT   AO_OPERATION_ID
           , CM_DATE              AS DATETIME
           , CM_CASHIER_NAME      AS TERMINAL_NAME
           , CM_TYPE              AS TRANSACTION_TYPE
           , PAYMENT_USER         AS PAYMENT_USER 
           , AUTHORIZATION_USER   AS AUTHORIZATION_USER
           , CHIPS_PURCHASE_TOTAL
           + TITO_TICKET_PAID
           - TOTAL_TAX            AS AFTER_TAX_AMOUNT
           , CHIPS_PURCHASE_TOTAL
           + TITO_TICKET_PAID     AS BEFORE_TAX_AMOUNT
           , REASON               AS REASON
           , AO_COMMENT           AS COMMENT      
           , 2                    AS ORIGEN
           , CM_TYPE              AS TYPE1
           , ORIGINAL_CHIPS_PURCHASE_TOTAL
           , EXCHANGE_CURRENCY
      FROM   #TEMP_CASHIER_SESSIONS
     WHERE   1 = 1 '
    + CASE WHEN @pCmMovements IS NOT NULL THEN ' AND CM_TYPE IN (' + ISNULL(@pCmMovements, '0') + ')  ' ELSE '' END + CHAR(10) 
----     + CASE WHEN @pWithTaxes = 1 THEN ' AND CM_ADD_AMOUNT > 0' ELSE ' AND CM_ADD_AMOUNT = 0' END + CHAR(10) 
END
 
SET @_SQL = @_SQL + ' ORDER   BY DATETIME DESC ' + CHAR(10) 
+ 'DROP TABLE #TEMP_CASHIER_SESSIONS '

--PRINT @_SQL     
EXEC sp_executesql @_SQL  
   
END  

GO
GRANT EXECUTE ON [dbo].[GetCashierTransactionsTaxes] TO [wggui] WITH GRANT OPTION 
GO

 IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GT_Base_Report_Data]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[GT_Base_Report_Data]
 GO

CREATE PROCEDURE [dbo].[GT_Base_Report_Data]
(
      @pBaseType                   INTEGER
    , @pTimeInterval               INTEGER
    , @pDateFrom                   DATETIME
    , @pDateTo                     DATETIME
    , @pOnlyActivity               INTEGER
    , @pOrderBy                    INTEGER
    , @pValidTypes                 VARCHAR(4096)
    , @pSelectedCurrency           NVARCHAR(3)
    , @pTotalToSelectedCurrency    INTEGER
)
AS
BEGIN

-- DECLARATIONS
   -- @_DAYS_AND_TABLES:    A temporary table with all intervals and all base type selected (used to show base types without activity).
   DECLARE @_DAYS_AND_TABLES TABLE(DATE_TIME DATETIME, TABLE_IDENTIFIER_VALUE BIGINT, TABLE_IDENT_NAME VARCHAR(50), TABLE_TYPE_IDENT BIGINT, TABLE_TYPE_NAME VARCHAR(50))
   -- @_DAY_VAR:            Variable to create the intervals
   DECLARE @_DAY_VAR DATETIME

-- PARAMETERS
     DECLARE @_BASE_TYPE                  AS   INTEGER -- 0 BY TABLE TYPE, 1 BY TABLE
     DECLARE @_TIME_INTERVAL              AS   INTEGER -- -1, 0 DAYS, 1 MONTH, 2 YEAR
     DECLARE @_DATE_FROM                  AS   DATETIME
     DECLARE @_DATE_TO                    AS   DATETIME
     DECLARE @_ONLY_ACTIVITY              AS   INTEGER -- 0 SHOW ALL, 1 SHOW ONLY WITH ACTIVITY
     DECLARE @_ORDER_BY                   AS   INTEGER -- 0 BY INTERVAL DESC, 1 BY IDENTIFIER ASC
     DECLARE @_DELIMITER                  AS   CHAR(1)
     DECLARE @_TYPES_TABLE                TABLE(SST_ID INT, SST_VALUE VARCHAR(50)) -- TO STORE THE VALID GAMING TABLE TYPES
     DECLARE @_SELECTED_CURRENCY          AS   NVARCHAR(3)
     DECLARE @_TOTAL_TO_SELECTED_CURRENCY AS   INTEGER
     DECLARE @_CHIP_RE                    AS   INTEGER
     DECLARE @_CHIP_NRE                   AS   INTEGER
     DECLARE @_CHIP_COLOR                 AS   INTEGER
----------------------------------------------------------------------------------------------------------------

-- Initialization --
  SET @_BASE_TYPE                  =   @pBaseType
  SET @_TIME_INTERVAL              =   @pTimeInterval
  SET @_DATE_FROM                  =   @pDateFrom
  SET @_DATE_TO                    =   @pDateTo
  SET @_ONLY_ACTIVITY              =   @pOnlyActivity
  SET @_ORDER_BY                   =   @pOrderBy
  SET @_DELIMITER                  =   ','
  SET @_SELECTED_CURRENCY          =   @pSelectedCurrency
  SET @_TOTAL_TO_SELECTED_CURRENCY =   @pTotalToSelectedCurrency
  SET @_CHIP_RE                    =   1001
  SET @_CHIP_NRE                   =   1002
  SET @_CHIP_COLOR                 =   1003
----------------------------------------------------------------------------------------------------------------

-- CHECK DATE PARAMETERS
IF @_DATE_FROM IS NULL
BEGIN
   -- IF DATE FROM IS NULL, USER FIRST POSIBLE DATE
   SET @_DATE_FROM = CAST('' AS DATETIME)
   SET @pDateFrom  = CAST('' AS DATETIME)
END

IF @_DATE_TO IS NULL
BEGIN
   -- IF DATE TO IS NULL, QUERY UNTIL CURRENT DATE
   SET @_DATE_TO = CAST(GETDATE() AS DATETIME)
   SET @pDateTo  = CAST(GETDATE() AS DATETIME)
END

-- ASSIGN TYPES PARAMETER INTO TABLE
INSERT INTO @_TYPES_TABLE SELECT * FROM dbo.SplitStringIntoTable(@pValidTypes, @_DELIMITER, DEFAULT)

IF @_TIME_INTERVAL <> -1
 BEGIN
   -- INTERVALS DATES AND TABLE PREPARATION

   -- PREPARE DATES RANGE DEPENDING ON TIME INTERVAL
   SET @_DATE_FROM = CASE
                        WHEN @_TIME_INTERVAL = 0 THEN -- BY DAY
                           DATEADD(dd, 0, DATEDIFF(dd, 0, @_DATE_FROM))
                        WHEN @_TIME_INTERVAL = 1 THEN -- BY MONTH
                           CAST(CAST(YEAR(@_DATE_FROM) AS VARCHAR(4)) + RIGHT('0' + CAST(MONTH(@_DATE_FROM) AS VARCHAR(2)), 2) + '01' AS DATETIME)
                        WHEN @_TIME_INTERVAL = 2 THEN -- BY YEAR
                           CAST(CAST(YEAR(@_DATE_FROM) AS VARCHAR(4)) + '0101' AS DATETIME)
                     END

   SET @_DATE_TO = CASE
                        WHEN @_TIME_INTERVAL = 0 THEN -- BY DAY
                           DATEADD(DAY, 1, CAST(@_DATE_TO AS DATETIME))
                        WHEN @_TIME_INTERVAL = 1 THEN -- BY MONTH
                           DATEADD(MONTH, 1, CAST(CAST(YEAR(@_DATE_TO) AS VARCHAR(4)) + RIGHT('0' + CAST(MONTH(@_DATE_TO) AS VARCHAR(2)), 2) + '01' AS DATETIME))
                        WHEN @_TIME_INTERVAL = 2 THEN -- BY YEAR
                           DATEADD(YEAR, 1, CAST(CAST(YEAR(@_DATE_TO) AS VARCHAR(4)) + '0101' AS DATETIME))
                     END

   -- PREPARE THE TIME INTERVALS TABLE
   SET @_DAY_VAR = @_DATE_FROM
   WHILE @_DAY_VAR < @_DATE_TO
   BEGIN
         -- SET THE LINK FIELD FOR THE RESULTS
         IF @_BASE_TYPE = 0
          BEGIN
            -- LINK WITH TABLE TYPES
            INSERT INTO @_DAYS_AND_TABLES
                 SELECT   CAST(@_DAY_VAR AS DATETIME), X.GTT_GAMING_TABLE_TYPE_ID, X.GTT_NAME AS TABLE_IDENT, NULL, NULL
                   FROM   GAMING_TABLES_TYPES AS X
                  WHERE   X.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
          END
         ELSE
          BEGIN
            -- LINK WITH TABLES
            INSERT INTO @_DAYS_AND_TABLES
                 SELECT   CAST(@_DAY_VAR AS DATETIME), X.GT_GAMING_TABLE_ID, X.GT_NAME AS TABLE_IDENT, Z.GTT_GAMING_TABLE_TYPE_ID, Z.GTT_NAME
                   FROM   GAMING_TABLES AS X
              LEFT JOIN   GAMING_TABLES_TYPES AS Z
                     ON   Z.GTT_GAMING_TABLE_TYPE_ID = X.GT_TYPE_ID
                  WHERE   Z.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
          END

          -- SET INCREMENT
          SET @_DAY_VAR = CASE
                               WHEN @_TIME_INTERVAL = 0 THEN DATEADD(DAY,1,@_DAY_VAR)     -- DAY
                               WHEN @_TIME_INTERVAL = 1 THEN DATEADD(MONTH,1,@_DAY_VAR)   -- MONTH
                               WHEN @_TIME_INTERVAL = 2 THEN DATEADD(YEAR,1,@_DAY_VAR)    -- YEAR
                          END
   END

END -- IF INTERVALS <> - 1
ELSE
BEGIN

   -- SET THE LINK FIELD FOR THE RESULTS WITHOUT INTERVALS
   IF @_BASE_TYPE = 0
    BEGIN
      -- LINK WITH TABLE TYPES
      INSERT INTO @_DAYS_AND_TABLES
           SELECT   NULL, X.GTT_GAMING_TABLE_TYPE_ID, X.GTT_NAME AS TABLE_IDENT, NULL, NULL
             FROM   GAMING_TABLES_TYPES AS X
            WHERE   X.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
    END
   ELSE
    BEGIN
      -- LINK WITH TABLES
      INSERT INTO @_DAYS_AND_TABLES
           SELECT   NULL, X.GT_GAMING_TABLE_ID, X.GT_NAME AS TABLE_IDENT, Z.GTT_GAMING_TABLE_TYPE_ID, Z.GTT_NAME
             FROM   GAMING_TABLES AS X
        LEFT JOIN   GAMING_TABLES_TYPES AS Z
               ON   Z.GTT_GAMING_TABLE_TYPE_ID = X.GT_TYPE_ID
            WHERE   Z.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
    END

 END


-- SELECT INTO TEMPORARY TABLE WITH ALL DATA
-- SPECIFIC REPORT QUERY
  SELECT   X.TABLE_IDENTIFIER
         , X.TABLE_NAME
         , X.TABLE_TYPE
         , X.TABLE_TYPE_NAME
         , SUM(X.S_DROP_GAMBLING_TABLE) + SUM (X.S_DROP_CASHIER) TOTAL_DROP
         , SUM(X.S_DROP_GAMBLING_TABLE) TOTAL_DROP_GAMBLING_TABLE
         , SUM(X.S_DROP_CASHIER) TOTAL_DROP_CASHIER
         , SUM(X.S_WIN)  TOTAL_WIN
         , ISNULL(SUM(X.S_TIP),0)  TOTAL_TIP
         , CASE WHEN SUM(X.S_DROP) = 0 THEN 0 ELSE SUM(X.S_WIN) / SUM(X.S_DROP) * 100 END AS WIN_DROP
         , MIN(X.THEORIC_HOLD) AS THEORIC_HOLD
         , CASE WHEN SUM(X.S_DROP) = 0 THEN 0 ELSE SUM(X.S_TIP) / SUM(X.S_DROP) * 100 END AS TIP_DROP
         , SUM(X.S_WIN) + SUM(X.S_TIP) AS WIN_TIPS
         , X.CM_DATE_ONLY
         , MIN(X.OPEN_HOUR) AS OPEN_HOUR
         , MAX(X.CLOSE_HOUR) AS CLOSE_HOUR
         , SUM(X.SESSION_SECONDS) AS SESSION_SECONDS
         , COUNT(X.SESSION_SECONDS) AS SESSION_SUM

   INTO #GT_TEMPORARY_REPORT_DATA

   FROM (
           -- CORE QUERY
            SELECT CASE WHEN @_TIME_INTERVAL = 0 THEN     -- TO FILTER BY DAY
                        DATEADD(dd, 0, DATEDIFF(dd, 0, CS_OPENING_DATE))
                      WHEN @_TIME_INTERVAL = 1 THEN     -- TO FILTER BY MONTH
                        CAST(CAST(YEAR(CS_OPENING_DATE) AS VARCHAR(4)) + RIGHT('0' + CAST(MONTH(CS_OPENING_DATE) AS VARCHAR(2)), 2) + '01' AS DATETIME)
                      WHEN @_TIME_INTERVAL = 2 THEN     -- TO FILTER BY YEAR
                        CAST(CAST(YEAR(CS_OPENING_DATE) AS VARCHAR(4)) + '01'+ '01' AS DATETIME)
                        END                                                                                       AS CM_DATE_ONLY
                     , (CASE WHEN @_BASE_TYPE = 0 THEN GTT_GAMING_TABLE_TYPE_ID ELSE GT.GT_GAMING_TABLE_ID END)   AS TABLE_IDENTIFIER -- GET THE BASE TYPE IDENTIFIER
                     , (CASE WHEN @_BASE_TYPE = 0 THEN GTT_NAME ELSE GT.GT_NAME END)                              AS TABLE_NAME       -- GET THE BASE TYPE IDENTIFIER NAME
                     , (CASE WHEN @_BASE_TYPE = 0 THEN NULL ELSE GTT_GAMING_TABLE_TYPE_ID END)                    AS TABLE_TYPE       -- TYPE
                     , (CASE WHEN @_BASE_TYPE = 0 THEN NULL ELSE GTT_NAME END)                                    AS TABLE_TYPE_NAME  -- TYPE NAME

                     ,  CASE WHEN @_TOTAL_TO_SELECTED_CURRENCY = 0 THEN
                                     DBO.GT_CALCULATE_DROP(ISNULL(GTS_OWN_SALES_AMOUNT      , 0)
                                                         , ISNULL(GTS_EXTERNAL_SALES_AMOUNT , 0)
                                                         , ISNULL(GTS_COLLECTED_AMOUNT      , 0)
                                                         , GT_HAS_INTEGRATED_CASHIER)
                             WHEN @_TOTAL_TO_SELECTED_CURRENCY = 1 THEN
                                     DBO.GT_CALCULATE_DROP(SUM(ISNULL(GTSC_OWN_SALES_AMOUNT       , 0))
                                                         , SUM(ISNULL(GTSC_EXTERNAL_SALES_AMOUNT  , 0))
                                                         , SUM(ISNULL(GTSC_COLLECTED_AMOUNT       , 0))
                                                         , GT_HAS_INTEGRATED_CASHIER)
                             END AS S_DROP

                     ,  CASE WHEN @_TOTAL_TO_SELECTED_CURRENCY = 0 THEN
                                     DBO.GT_CALCULATE_DROP_GAMBLING_TABLES(ISNULL(GTS_COLLECTED_DROPBOX_AMOUNT        , 0)
                                                                         , ISNULL(GTS_COLLECTED_DROPBOX_CHIPS_AMOUNT  , 0)
                                                                         , 0 -- It does not allow takings of dropbox with tickets
                                                                         , GT_HAS_INTEGRATED_CASHIER
                                                                         , GTS_DROPBOX_ENABLED)
                             WHEN @_TOTAL_TO_SELECTED_CURRENCY = 1 THEN
                                     DBO.GT_CALCULATE_DROP_GAMBLING_TABLES(SUM(ISNULL(CASE WHEN GTSC_TYPE = 0            THEN GTSC_COLLECTED_DROPBOX_AMOUNT_CONVERTED
                                                                                           WHEN GTSC_TYPE = 1            THEN GTSC_COLLECTED_DROPBOX_AMOUNT_CONVERTED
                                                                                      ELSE 0 END,  0))
                                                                         , SUM(ISNULL(CASE WHEN GTSC_TYPE = @_CHIP_RE    THEN GTSC_COLLECTED_DROPBOX_AMOUNT_CONVERTED
                                                                                           WHEN GTSC_TYPE = @_CHIP_NRE   THEN GTSC_COLLECTED_DROPBOX_AMOUNT_CONVERTED
                                                                                           WHEN GTSC_TYPE = @_CHIP_COLOR THEN GTSC_COLLECTED_DROPBOX_AMOUNT_CONVERTED
                                                                                      ELSE 0 END,  0))
                                                                         , 0 -- It does not allow takings of dropbox with tickets
                                                                         , GT_HAS_INTEGRATED_CASHIER
                                                                         , GTS_DROPBOX_ENABLED)
                             END AS S_DROP_GAMBLING_TABLE

                     ,  CASE WHEN @_TOTAL_TO_SELECTED_CURRENCY = 0 THEN
                                     DBO.GT_CALCULATE_DROP_CASHIER(ISNULL(GTS_OWN_SALES_AMOUNT, 0)
                                                                 , ISNULL(GTS_EXTERNAL_SALES_AMOUNT, 0))
                             WHEN @_TOTAL_TO_SELECTED_CURRENCY = 1 THEN
                                     DBO.GT_CALCULATE_DROP_CASHIER(SUM(ISNULL(GTSC_OWN_SALES_AMOUNT, 0))
                                                                 , SUM(ISNULL(GTSC_EXTERNAL_SALES_AMOUNT, 0)))
                             END AS S_DROP_CASHIER

                     , GT_THEORIC_HOLD AS THEORIC_HOLD
                     ,  CASE WHEN @_TOTAL_TO_SELECTED_CURRENCY = 0 THEN
                                     DBO.GT_CALCULATE_WIN(ISNULL(GTS_FINAL_CHIPS_AMOUNT      , 0)
                                                        , ISNULL(GTS_INITIAL_CHIPS_AMOUNT    , 0)
                                                        , ISNULL(GTS_COLLECTED_AMOUNT        , 0)
                                                        , ISNULL(GTS_TIPS                    , 0))
                             WHEN @_TOTAL_TO_SELECTED_CURRENCY = 1 THEN
                                     DBO.GT_CALCULATE_WIN(SUM(ISNULL(GTSC_FINAL_CHIPS_AMOUNT  , 0))
                                                        , SUM(ISNULL(GTSC_INITIAL_CHIPS_AMOUNT, 0))
                                                        , SUM(ISNULL(GTSC_COLLECTED_AMOUNT    , 0))
                                                        , SUM(ISNULL(GTSC_TIPS                , 0)))
                             END AS S_WIN

                     ,  CASE WHEN @_TOTAL_TO_SELECTED_CURRENCY = 0 THEN
                                     ISNULL(GTS_TIPS, 0)
                             WHEN @_TOTAL_TO_SELECTED_CURRENCY = 1 THEN
                                     ISNULL(GTSC_TIPS, 0)
                             END AS S_TIP

                     , CS.CS_OPENING_DATE                                                                         AS OPEN_HOUR
                     , CS.CS_CLOSING_DATE                                                                         AS CLOSE_HOUR
                     , DATEDIFF(SECOND, CS_OPENING_DATE, ISNULL(CS_CLOSING_DATE, GETDATE()))                      AS SESSION_SECONDS
                                   FROM   GAMING_TABLES_SESSIONS GTS
                    LEFT  JOIN   GAMING_TABLES_SESSIONS_BY_CURRENCY GTSC
                           ON    GTSC.GTSC_GAMING_TABLE_SESSION_ID = GTS.GTS_GAMING_TABLE_SESSION_ID
                  INNER JOIN   CASHIER_SESSIONS CS ON GTS.GTS_CASHIER_SESSION_ID = CS.CS_SESSION_ID
                                         AND   CS_OPENING_DATE >= @pDateFrom AND CS_OPENING_DATE < @pDateTo
                  INNER JOIN   GAMING_TABLES GT ON GTS.GTS_GAMING_TABLE_ID = GT.GT_GAMING_TABLE_ID
                           AND   GT.GT_TYPE_ID IN (SELECT SST_VALUE FROM @_TYPES_TABLE)
                  INNER JOIN   GAMING_TABLES_TYPES GTT ON GT.GT_TYPE_ID = GTT.GTT_GAMING_TABLE_TYPE_ID
               WHERE   CS_STATUS = 1  -- Only closed sessions
                   AND GTSC_ISO_CODE IN (@_SELECTED_CURRENCY)
               GROUP BY CS_OPENING_DATE, GTT_GAMING_TABLE_TYPE_ID, GT_GAMING_TABLE_ID, GTT_NAME, GT_NAME, GTS_OWN_SALES_AMOUNT, GTS_EXTERNAL_SALES_AMOUNT, GTS_COLLECTED_AMOUNT, GT_HAS_INTEGRATED_CASHIER
                      , GTS_COLLECTED_DROPBOX_AMOUNT, GTS_COLLECTED_DROPBOX_AMOUNT, GTS_DROPBOX_ENABLED, GTS_COLLECTED_DROPBOX_CHIPS_AMOUNT, GT_THEORIC_HOLD, GTS_FINAL_CHIPS_AMOUNT, GTS_INITIAL_CHIPS_AMOUNT
                      , GTS_TIPS, GTSC_TIPS, CS_CLOSING_DATE
          -- END CORE QUERY

        ) AS X

 GROUP BY   X.TABLE_TYPE, X.TABLE_IDENTIFIER, X.TABLE_TYPE_NAME, X.TABLE_NAME, X.CM_DATE_ONLY  -- Group by indentifier and time interval

IF @_TIME_INTERVAL <> -1
BEGIN
   -- INTERVALS DATES AND TABLE FINAL PREPARATION

   -- FILTER ACTIVITY
   IF @_ONLY_ACTIVITY = 0
    BEGIN
      -- JOIN THE SELECT WITH ALL DATA
      SELECT
              DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER,
              DT.TABLE_IDENT_NAME AS TABLE_NAME,
              DT.TABLE_TYPE_IDENT AS TABLE_TYPE,
              DT.TABLE_TYPE_NAME AS TABLE_TYPE_NAME,
              ISNULL(ZZ.TOTAL_DROP_GAMBLING_TABLE,0) AS TOTAL_DROP_GAMBLING_TABLE,
              ISNULL(ZZ.TOTAL_DROP_CASHIER,0) AS  TOTAL_DROP_CASHIER,
              ISNULL(ZZ.TOTAL_DROP, 0) AS TOTAL_DROP,
              ISNULL(ZZ.TOTAL_WIN, 0) AS TOTAL_WIN,
              ISNULL(ZZ.TOTAL_TIP, 0) AS TOTAL_TIP,
              ISNULL(ZZ.WIN_DROP, 0) AS WIN_DROP,
              ISNULL(ZZ.THEORIC_HOLD, 0) AS THEORIC_HOLD,
              ISNULL(ZZ.TIP_DROP, 0) AS TIP_DROP,
              ISNULL(ZZ.WIN_TIPS, 0) AS WIN_TIPS,
              ZZ.OPEN_HOUR,
              ZZ.CLOSE_HOUR,
              CASE WHEN ZZ.CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED,
              CASE WHEN ZZ.SESSION_SECONDS = 0 THEN DATEDIFF(SECOND,ZZ.OPEN_HOUR,GETDATE()) ELSE ISNULL(ZZ.SESSION_SECONDS, 0) END AS SESSION_SECONDS,
              ISNULL(ZZ.SESSION_SUM, 0) AS SESSION_SUM,
              DATE_TIME

      FROM @_DAYS_AND_TABLES DT

        LEFT JOIN #GT_TEMPORARY_REPORT_DATA ZZ
                     ON
                          (
                              (@_TIME_INTERVAL = 0 AND DATE_TIME = ZZ.CM_DATE_ONLY)
                           OR (@_TIME_INTERVAL = 1 AND MONTH(DATE_TIME) = MONTH(ZZ.CM_DATE_ONLY) AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
                           OR (@_TIME_INTERVAL = 2 AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
                          )
                    AND   TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER

       -- SET ORDER
       ORDER BY   DT.TABLE_TYPE_IDENT ASC,
                  CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC,
                  DATE_TIME DESC;

    END
   ELSE
    BEGIN
      -- JOIN THE SELECT HIDDING WITHOUT ACTIVITY
      SELECT
              DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER,
              DT.TABLE_IDENT_NAME AS TABLE_NAME,
              DT.TABLE_TYPE_IDENT AS TABLE_TYPE,
              DT.TABLE_TYPE_NAME AS TABLE_TYPE_NAME,
              ISNULL(ZZ.TOTAL_DROP_GAMBLING_TABLE,0) AS TOTAL_DROP_GAMBLING_TABLE,
              ISNULL(ZZ.TOTAL_DROP_CASHIER,0) AS  TOTAL_DROP_CASHIER,
              ISNULL(ZZ.TOTAL_DROP, 0) AS TOTAL_DROP,
              ISNULL(ZZ.TOTAL_WIN, 0) AS TOTAL_WIN,
              ISNULL(ZZ.TOTAL_TIP, 0) AS TOTAL_TIP,
              ISNULL(ZZ.WIN_DROP, 0) AS WIN_DROP,
              ISNULL(ZZ.THEORIC_HOLD, 0) AS THEORIC_HOLD,
              ISNULL(ZZ.TIP_DROP, 0) AS TIP_DROP,
              ISNULL(ZZ.WIN_TIPS, 0) AS WIN_TIPS,
              ZZ.OPEN_HOUR,
              ZZ.CLOSE_HOUR,
              CASE WHEN ZZ.CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED,
              CASE WHEN ZZ.SESSION_SECONDS = 0 THEN DATEDIFF(SECOND,ZZ.OPEN_HOUR,GETDATE()) ELSE ISNULL(ZZ.SESSION_SECONDS, 0) END AS SESSION_SECONDS,
              ISNULL(ZZ.SESSION_SUM, 0) AS SESSION_SUM,
              DATE_TIME

      FROM @_DAYS_AND_TABLES DT

       INNER JOIN #GT_TEMPORARY_REPORT_DATA ZZ
                     ON
                          (
                              (@_TIME_INTERVAL = 0 AND DATE_TIME = ZZ.CM_DATE_ONLY)
                           OR (@_TIME_INTERVAL = 1 AND MONTH(DATE_TIME) = MONTH(ZZ.CM_DATE_ONLY) AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
                           OR (@_TIME_INTERVAL = 2 AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
                          )
                    AND   TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER

       -- SET ORDER
       ORDER BY   DT.TABLE_TYPE_IDENT ASC,
                  CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC,
                  DATE_TIME DESC;

    END -- IF ONLY_ACTIVITY

END
ELSE  -- ELSE WITHOUT INTERVALS
BEGIN

   -- FINAL WITHOUT INTERVALS

      -- FILTER ACTIVITY
   IF @_ONLY_ACTIVITY = 0
    BEGIN
            -- JOIN DATA INCLUDING WITHOUT ACTIVITY
            SELECT   DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER,
                     DT.TABLE_IDENT_NAME AS TABLE_NAME,
                     DT.TABLE_TYPE_IDENT AS TABLE_TYPE,
                     DT.TABLE_TYPE_NAME,
                     ISNULL(ZZ.TOTAL_DROP_GAMBLING_TABLE,0) AS TOTAL_DROP_GAMBLING_TABLE,
                     ISNULL(ZZ.TOTAL_DROP_CASHIER,0) AS  TOTAL_DROP_CASHIER,
                     ISNULL(TOTAL_DROP, 0) AS TOTAL_DROP,
                     ISNULL(TOTAL_WIN, 0) AS TOTAL_WIN,
                     ISNULL(TOTAL_TIP, 0) AS TOTAL_TIP,
                     ISNULL(WIN_DROP, 0) AS WIN_DROP,
                     ISNULL(ZZ.THEORIC_HOLD, 0) AS THEORIC_HOLD,
                     ISNULL(TIP_DROP, 0) AS TIP_DROP,
                     ISNULL(WIN_TIPS, 0) AS WIN_TIPS,
                     OPEN_HOUR,
                     CLOSE_HOUR,
                     CASE WHEN CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED,
                     CASE WHEN SESSION_SECONDS = 0 THEN DATEDIFF(SECOND, OPEN_HOUR, GETDATE()) ELSE ISNULL(SESSION_SECONDS, 0) END AS SESSION_SECONDS,
                   ISNULL(SESSION_SUM, 0) AS SESSION_SUM
              FROM   @_DAYS_AND_TABLES DT

         LEFT JOIN   #GT_TEMPORARY_REPORT_DATA ZZ
                ON   DT.TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER

                          ORDER BY   DT.TABLE_TYPE_IDENT ASC,
                     CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC,
                     DATE_TIME DESC;

     END
    ELSE
     BEGIN
            -- JOIN DATA WITH ONLY ACTIVITY
          SELECT     DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER,
                     DT.TABLE_IDENT_NAME AS TABLE_NAME,
                     DT.TABLE_TYPE_IDENT AS TABLE_TYPE,
                     DT.TABLE_TYPE_NAME,
                     ISNULL(ZZ.TOTAL_DROP_GAMBLING_TABLE,0) AS TOTAL_DROP_GAMBLING_TABLE,
                     ISNULL(ZZ.TOTAL_DROP_CASHIER,0) AS  TOTAL_DROP_CASHIER,
                     ISNULL(TOTAL_DROP, 0) AS TOTAL_DROP,
                     ISNULL(TOTAL_WIN, 0) AS TOTAL_WIN,
                     ISNULL(TOTAL_TIP, 0) AS TOTAL_TIP,
                     ISNULL(WIN_DROP, 0) AS WIN_DROP,
                     ISNULL(ZZ.THEORIC_HOLD, 0) AS THEORIC_HOLD,
                     ISNULL(TIP_DROP, 0) AS TIP_DROP,
                     ISNULL(WIN_TIPS, 0) AS WIN_TIPS,
                     OPEN_HOUR,
                     CLOSE_HOUR,
                     CASE WHEN CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED,
                     CASE WHEN SESSION_SECONDS = 0 THEN DATEDIFF(SECOND, OPEN_HOUR, GETDATE()) ELSE ISNULL(SESSION_SECONDS, 0) END AS SESSION_SECONDS,
                   ISNULL(SESSION_SUM, 0) AS SESSION_SUM
              FROM   @_DAYS_AND_TABLES DT

         INNER JOIN   #GT_TEMPORARY_REPORT_DATA ZZ
                ON   DT.TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER

           ORDER BY   DT.TABLE_TYPE_IDENT ASC,
                      CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC,
                      DATE_TIME DESC;

     END
 END

-- ERASE THE TEMPORARY DATA
DROP TABLE #GT_TEMPORARY_REPORT_DATA

END -- END PROCEDURE
GO

-- PERMISSIONS
GRANT EXECUTE ON [dbo].[GT_Base_Report_Data] TO [wggui] WITH GRANT OPTION
GO
 

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GT_Calculate_DROP_GAMBLING_TABLES]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[GT_Calculate_DROP_GAMBLING_TABLES]
GO

CREATE FUNCTION [dbo].[GT_Calculate_DROP_GAMBLING_TABLES] ( @pCollectedAmount AS MONEY, @pCollectedChipsAmount as MONEY, @pCollectedTicketsAmount as MONEY, @pIsIntegratedCashier AS BIT, @pIsDropBoxEnabled as BIT ) RETURNS MONEY
  BEGIN 
  DECLARE @drop as Money
    IF @pIsDropBoxEnabled = 1 
      SET @drop = @pCollectedAmount + @pCollectedChipsAmount + @pCollectedTicketsAmount
    ELSE
      IF @pIsIntegratedCashier = 0
       SET @drop = @pCollectedAmount
     
     IF @drop < 0
      set @drop = 0
     
   RETURN @drop
  END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID('GetCageMovementType') AND type in ('P', 'PC'))
  DROP PROCEDURE GetCageMovementType
GO

CREATE PROCEDURE GetCageMovementType
 (@pStarDateTime DATETIME,
  @pEndDateTime  DATETIME)
AS
BEGIN
  
  CREATE TABLE #TABLE_ISO_CODE 
  ( 
    id INT Identity(1,1),
    iso_code_column VARCHAR(5),
    iso_code VARCHAR(3),
    data_type INTEGER
  ) 
  DECLARE @_count INTEGER	
  DECLARE @_count_aux INTEGER	
  DECLARE @_isoCodeLocal AS NVARCHAR(MAX)
  DECLARE @cols AS NVARCHAR(MAX)
  DECLARE @cols_type_1 AS NVARCHAR(MAX)
  DECLARE @cols_type_2 AS NVARCHAR(MAX) 
  DECLARE @colsFormat AS NVARCHAR(MAX)   
  DECLARE @query  AS NVARCHAR(MAX)
  DECLARE @_counter INTEGER    
  DECLARE @_isoCode NVARCHAR(MAX)
  DECLARE @_isoCodeColumn NVARCHAR(MAX)
  DECLARE @_dataType INTEGER
  DECLARE @_queryColumns NVARCHAR(MAX)
  DECLARE @_activeDualCurrency NVARCHAR(1)
 
  -- Get All IsoCodes
  INSERT INTO #TABLE_ISO_CODE
  SELECT
    CD.CMD_ISO_CODE AS CMD_ISO_CODE_COLUMN
   ,CD.CMD_ISO_CODE AS CMD_ISO_CODE
   ,1
  FROM CAGE_MOVEMENTS AS CM
  LEFT JOIN CAGE_MOVEMENT_DETAILS AS CD ON CM.CGM_MOVEMENT_ID = CD.CMD_MOVEMENT_ID 
  WHERE   CM.CGM_MOVEMENT_DATETIME >= @pStarDateTime
          AND CM.CGM_MOVEMENT_DATETIME < @pEndDateTime 
          AND CD.CMD_ISO_CODE IS NOT NULL
          AND CD.CMD_ISO_CODE NOT IN ('X01')
          AND CD.CMD_QUANTITY > 0
          AND CM.CGM_STATUS NOT IN(0, 1)
          AND CM.CGM_TYPE NOT IN(200)
  GROUP BY CD.CMD_ISO_CODE
  UNION 
  SELECT
   (CD.CMD_ISO_CODE + '_0') AS CMD_ISO_CODE
   ,CD.CMD_ISO_CODE
    ,2
  FROM CAGE_MOVEMENTS AS CM
  LEFT JOIN CAGE_MOVEMENT_DETAILS AS CD ON CM.CGM_MOVEMENT_ID = CD.CMD_MOVEMENT_ID 
  WHERE  CM.CGM_MOVEMENT_DATETIME >= @pStarDateTime
         AND CM.CGM_MOVEMENT_DATETIME < @pEndDateTime
         AND CD.CMD_ISO_CODE IS NOT NULL
         AND CD.CMD_ISO_CODE NOT IN ('X01')   
         AND CD.CMD_QUANTITY IN(-1, -2, -100)
         AND CM.CGM_STATUS NOT IN(0, 1)
         AND CM.CGM_TYPE NOT IN(200)
  GROUP BY CD.CMD_ISO_CODE
  ORDER BY CMD_ISO_CODE
  
    -- Get current IsoCode for GENERAL_PARAMS
  SET @_isoCodeLocal = (SELECT GP.GP_KEY_VALUE 
                        FROM GENERAL_PARAMS AS GP 
                        WHERE GP.GP_GROUP_KEY = 'RegionalOptions' AND GP.GP_SUBJECT_KEY = 'CurrencyISOCode')
  
  SET @_activeDualCurrency = (SELECT CAST(GP.GP_KEY_VALUE AS BIT)  
                           FROM GENERAL_PARAMS AS GP 
                           WHERE GP.GP_GROUP_KEY = 'FloorDualCurrency' AND GP.GP_SUBJECT_KEY = 'Enabled')
      
  CREATE TABLE #GET_CAGE_MOVEMENT_TYPE_TABLE(        
         CMT_MOVEMENT_ID BIGINT,
         CMT_MOVEMENT_DATETIME DATETIME, 
         CMT_TYPE INTEGER,
         CMT_TYPE_NAME NVARCHAR(MAX),       
         CMT_STATUS_NAME NVARCHAR(MAX),
         CMT_USER_NAME NVARCHAR(MAX),
         CMT_CAGE_USER_NAME NVARCHAR(MAX),
         CMT_MACHINE_NAME NVARCHAR(MAX),  
         CMT_COLLECTION_ID BIGINT,
         CMT_TERMINAL_ID INTEGER,
         CMT_CHIPS_AMOUNT MONEY
    )   
        
    SET @_counter = 1		
    SET @_count = (SELECT COUNT(*) FROM #TABLE_ISO_CODE) + 1
    SET @_queryColumns = ''
    SET @cols = ''   
    SET @cols_type_1 = ''  
    SET @cols_type_2 = ''  
    SET @colsFormat =''
    WHILE NOT @_counter = @_count
    BEGIN

      SELECT 
        @_isoCode  =  TIC.iso_code,
        @_isoCodeColumn = TIC.iso_code_column,
        @_dataType = TIC.data_type  
      FROM #TABLE_ISO_CODE AS TIC
      WHERE TIC.id = @_counter

      -- Add column dynamic to #GET_CAGE_MOVEMENT_TYPE_TABLE for isocode
      SET @_queryColumns = 'ALTER TABLE #GET_CAGE_MOVEMENT_TYPE_TABLE ADD [' + @_isoCodeColumn + '] MONEY '
      EXEC SP_EXECUTESQL @_queryColumns
      
      --Get colums isocode
      
      IF @_dataType = 1
        BEGIN
            SET @cols = @cols + ',AM.'+ @_isoCode
            SET @cols_type_1 = @cols_type_1 + ',' + @_isoCode
            SET @colsFormat = @colsFormat + ',ISNULL('+@_isoCodeColumn+', 0) AS '+@_isoCode+''
        END
      IF @_dataType = 2
        BEGIN
            SET @cols = @cols + ',AZ.'+@_isoCode+''
            IF @cols_type_2 = ''
              BEGIN
                SET @cols_type_2 = @_isoCode 
              END
            ELSE
              BEGIN            
                SET @cols_type_2 = @cols_type_2 + ',' + @_isoCode 
              END
            SET @colsFormat = @colsFormat + ',ISNULL('+@_isoCodeColumn+', 0) AS ''Otros ('+@_isoCode+')'''
         END
     
      SET @_counter = @_counter + 1

    END  
                          
   SET @query = '   
            INSERT INTO #GET_CAGE_MOVEMENT_TYPE_TABLE 
            SELECT
             CM.CGM_MOVEMENT_ID   
            ,CM.CGM_MOVEMENT_DATETIME
            ,CM.CGM_TYPE
            ,CASE 
                WHEN CM.CGM_TYPE = 0   THEN ''Envío a Cajero''
                WHEN CM.CGM_TYPE = 1   THEN ''Envío a destino personalizado''
                WHEN CM.CGM_TYPE = 2   THEN ''Cierre de bóveda''
                WHEN CM.CGM_TYPE = 3   THEN ''Arqueo de bóveda''
                WHEN CM.CGM_TYPE = 4   THEN ''Envío a mesa de juego'' 
                WHEN CM.CGM_TYPE = 5   THEN ''Envío a terminal''
                WHEN CM.CGM_TYPE = 99  THEN ''Pérdida de cuadratura''
                WHEN CM.CGM_TYPE = 100 THEN ''Recaudación de Cajero'' 
                WHEN CM.CGM_TYPE = 101 THEN ''Recaudación de origen personalizado''
                WHEN CM.CGM_TYPE = 102 THEN ''Apertura de bóveda''
                WHEN CM.CGM_TYPE = 103 THEN ''Recaudación de mesa de juego''
                WHEN CM.CGM_TYPE = 104 THEN ''Recaudación de terminal''
                WHEN CM.CGM_TYPE = 105 THEN ''Reapertura de bóveda''
                WHEN CM.CGM_TYPE = 199 THEN ''Ganancia de cuadratura''
                WHEN CM.CGM_TYPE = 200 THEN ''Solicitud desde Cajero'' 
                WHEN CM.CGM_TYPE = 300 THEN ''Provisión de progresivo''
                WHEN CM.CGM_TYPE = 301 THEN ''Progresivo otorgado''
                WHEN CM.CGM_TYPE = 302 THEN ''(Anulación)Provisión de progresivo''
                WHEN CM.CGM_TYPE = 303 THEN ''(Anulación)Progresivo otorgado''
                ELSE CAST(CM.CGM_TYPE AS NVARCHAR(MAX))
             END AS CGM_TYPE_NAME
            ,CASE
                WHEN CM.CGM_STATUS = 0 AND CM.CGM_TYPE IN (0, 4)                         THEN ''Enviado''
                WHEN CM.CGM_STATUS = 0 AND CM.CGM_TYPE IN (200)                          THEN ''Solicitud pendiente''
                WHEN CM.CGM_STATUS = 0 AND CM.CGM_TYPE IN (5, 100, 103, 104)             THEN ''Pendiente recaudar''            
                WHEN CM.CGM_STATUS = 2 AND CM.CGM_TYPE IN (0, 4, 5, 300, 301, 302, 303)  THEN ''Finalizado''
                WHEN CM.CGM_STATUS = 2 AND CM.CGM_TYPE IN (100, 103, 104)                THEN ''Recaudado''
                WHEN CM.CGM_STATUS = 2 AND CM.CGM_TYPE IN (200)                          THEN ''Solicitud tramitada''            
                WHEN CM.CGM_STATUS = 1 AND CM.CGM_TYPE IN (200)                          THEN ''Solicitud cancelada''
                WHEN CM.CGM_STATUS = 1 AND CM.CGM_TYPE NOT IN (200)                      THEN ''Cancelado''            
                WHEN CM.CGM_STATUS = 3 AND CM.CGM_TYPE IN (0)                            THEN ''Finalizado con descuadre denominaciones''
                WHEN CM.CGM_STATUS = 3 AND CM.CGM_TYPE NOT IN (0)                        THEN ''Recaudado con descuadre denominaciones''            
                WHEN CM.CGM_STATUS = 4 AND CM.CGM_TYPE IN (0)                            THEN ''Finalizado con descuadre del monto''
                WHEN CM.CGM_STATUS = 4 AND CM.CGM_TYPE NOT IN (0)                        THEN ''Recaudado con descuadre total''            
                WHEN CM.CGM_STATUS IN (9, 11, 12)                                        THEN ''Finalizado''            
                WHEN CM.CGM_STATUS = 10                                                  THEN ''Recaudado''            
                WHEN CM.CGM_STATUS = 13                                                  THEN ''(Cancelado) Pendiente recaudar'' 
                ELSE ''---''
             END AS CGM_STATUS_NAME 
            ,ISNULL(GU.GU_FULL_NAME, '''') AS GU_FULL_NAME
            ,ISNULL(GC.GU_FULL_NAME,'''') AS GU_CAGE_FULL_NAME
            ,ISNULL(ISNULL(CA.CT_NAME, CT.CT_NAME),TE.TE_NAME) AS CGM_NAME   
            ,CM.CGM_MC_COLLECTION_ID
            ,CM.CGM_TERMINAL_CASHIER_ID
            ,X01 
            ' + @cols + '
            FROM CAGE_MOVEMENTS AS CM
            LEFT JOIN GUI_USERS AS GU ON GU.GU_USER_ID = CM.CGM_USER_CASHIER_ID  
            LEFT JOIN GUI_USERS AS GC ON GC.GU_USER_ID = CM.CGM_USER_CAGE_ID 
            LEFT JOIN CASHIER_TERMINALS AS CA ON CA.CT_CASHIER_ID = CM.CGM_TERMINAL_CASHIER_ID
            LEFT JOIN CASHIER_SESSIONS AS CS ON CS.CS_SESSION_ID = CM.CGM_CASHIER_SESSION_ID  
            LEFT JOIN CASHIER_TERMINALS AS CT ON CT.CT_CASHIER_ID = CS.CS_CASHIER_ID
            LEFT JOIN TERMINALS AS TE ON TE.TE_TERMINAL_ID = CM.CGM_TERMINAL_CASHIER_ID      
            LEFT JOIN (
                SELECT * FROM(
                              SELECT  
                                     CM.CGM_MOVEMENT_ID 
                                    ,CASE
                                        WHEN CM.CGM_TYPE IN(0, 1, 4, 5) THEN (CD.CMD_QUANTITY * CD.CMD_DENOMINATION * -1)                                    
                                        ELSE (CD.CMD_QUANTITY * CD.CMD_DENOMINATION)
                                     END AS CD_AMOUNT
                                    ,CD.CMD_ISO_CODE
                               FROM CAGE_MOVEMENTS AS CM
                                    LEFT JOIN CAGE_MOVEMENT_DETAILS AS CD 
                                    ON CM.CGM_MOVEMENT_ID = CD.CMD_MOVEMENT_ID 
                               WHERE  CD.CMD_ISO_CODE IS NOT NULL AND CD.CMD_QUANTITY > 0
                             ) AS PU        
                             PIVOT
                             (
                                SUM(CD_AMOUNT)
                                FOR CMD_ISO_CODE IN(X01' + @cols_type_1 + ')
                             ) AS PVT 
            ) AS AM ON CM.CGM_MOVEMENT_ID = AM.CGM_MOVEMENT_ID
   '     
   IF @cols_type_2 <> ''
     BEGIN
       SET @query = @query + ' 
            LEFT JOIN (
                SELECT * FROM(
                            SELECT  
                                   CM.CGM_MOVEMENT_ID 
                                  ,CASE
                                      WHEN CM.CGM_TYPE IN(0, 1, 4, 5) THEN (CD.CMD_DENOMINATION * -1)                                    
                                      ELSE CD.CMD_DENOMINATION
                                   END AS CD_AMOUNT
                                  ,CD.CMD_ISO_CODE
                             FROM CAGE_MOVEMENTS AS CM
                                  LEFT JOIN CAGE_MOVEMENT_DETAILS AS CD 
                                  ON CM.CGM_MOVEMENT_ID = CD.CMD_MOVEMENT_ID 
                             WHERE  CD.CMD_ISO_CODE IS NOT NULL AND CD.CMD_QUANTITY IN(-1, -2, -100) AND CD.CMD_ISO_CODE NOT IN (''X01'')
                           ) AS PU        
                           PIVOT
                           (
                              SUM(CD_AMOUNT)
                              FOR CMD_ISO_CODE IN('+@cols_type_2+')
                           ) AS PVT             
            ) AS AZ ON CM.CGM_MOVEMENT_ID = AZ.CGM_MOVEMENT_ID
       '
     END
   
   SET @query = @query + '       
        WHERE CM.CGM_TYPE NOT IN(200) AND CM.CGM_STATUS NOT IN(0, 1) AND CM.CGM_MOVEMENT_DATETIME >= ''' + CONVERT(VARCHAR, @pStarDateTime, 21) + '''
        AND CM.CGM_MOVEMENT_DATETIME <  ''' + CONVERT(VARCHAR, @pEndDateTime, 21) + '''   
        ORDER BY CM.CGM_MOVEMENT_ID    
   '       
   EXEC SP_EXECUTESQL @query
   
   CREATE TABLE #TABLE_MACHINE_ISO_CODE 
   ( 
      id INT Identity(1,1),
      iso_code VARCHAR(5)
   ) 
   
   INSERT INTO #TABLE_MACHINE_ISO_CODE
   SELECT TE.TE_ISO_CODE FROM(SELECT 
      CM.CMT_MOVEMENT_ID
     ,CASE
         WHEN @_activeDualCurrency = '1'
           THEN ISNULL(TE_ISO_CODE,@_isoCodeLocal)
         ELSE @_isoCodeLocal
      END AS TE_ISO_CODE 
      FROM #GET_CAGE_MOVEMENT_TYPE_TABLE  AS CM
      INNER JOIN MONEY_COLLECTIONS AS MCU
      ON CM.CMT_COLLECTION_ID = MCU.MC_COLLECTION_ID AND CM.CMT_TYPE IN (5, 104) 
      LEFT JOIN TERMINALS AS TE
      ON CM.CMT_TERMINAL_ID = TE.TE_TERMINAL_ID 
      WHERE ((MCU.MC_COLLECTED_BILL_AMOUNT IS NOT NULL AND MCU.MC_COLLECTED_BILL_AMOUNT > 0) OR
                            (MCU.MC_EXPECTED_BILL_AMOUNT IS NOT NULL AND MCU.MC_EXPECTED_BILL_AMOUNT > 0) OR
                            (MCU.MC_COLLECTED_COIN_AMOUNT IS NOT NULL AND MCU.MC_COLLECTED_COIN_AMOUNT > 0) OR
                            (MCU.MC_EXPECTED_COIN_AMOUNT IS NOT NULL AND MCU.MC_EXPECTED_COIN_AMOUNT > 0))
      )AS TE
   GROUP BY TE.TE_ISO_CODE 
      
   SET @_counter = 1		
   SET @_count_aux = (SELECT COUNT(*) FROM #TABLE_MACHINE_ISO_CODE) + 1
   
   WHILE NOT @_counter = @_count_aux
     BEGIN
    
      SELECT 
        @_isoCode  =  TIC.iso_code
      FROM #TABLE_MACHINE_ISO_CODE AS TIC
      WHERE TIC.id = @_counter
      
      SET @_count =(SELECT COUNT(*) FROM #TABLE_ISO_CODE AS tic 
                                    WHERE tic.iso_code = @_isoCode AND tic.data_type = 1)                   
      IF @_count = 0
      BEGIN
        SET @_queryColumns = 'ALTER TABLE #GET_CAGE_MOVEMENT_TYPE_TABLE ADD [' + @_isoCode + '] MONEY '
        EXEC SP_EXECUTESQL @_queryColumns
        SET @colsFormat = @colsFormat + ',ISNULL('+@_isoCode+', 0) AS '+@_isoCode + '' 
      END
      
      SET @_queryColumns ='        
            UPDATE
              MTP
              SET
                MTP.'+@_isoCode+' = (CASE WHEN MTP.CMT_TYPE = 104 THEN MCU.COLLECTED_AMOUNT
                                                        ELSE MCU.EXPECTED_AMOUNT END)     
            FROM #GET_CAGE_MOVEMENT_TYPE_TABLE AS MTP 
            INNER JOIN (SELECT 
                          CM.CMT_MOVEMENT_ID
                         ,MC_EXPECTED_BILL_AMOUNT + MC_EXPECTED_COIN_AMOUNT AS EXPECTED_AMOUNT
                         ,MC_COLLECTED_BILL_AMOUNT + MC_COLLECTED_COIN_AMOUNT AS COLLECTED_AMOUNT
                         ,CASE
                             WHEN '''+@_activeDualCurrency+''' = ''1''
                               THEN ISNULL(TE_ISO_CODE,'''+@_isoCodeLocal+''')
                             ELSE '''+@_isoCodeLocal+'''
                          END AS TE_ISO_CODE 
                          FROM #GET_CAGE_MOVEMENT_TYPE_TABLE  AS CM
                          INNER JOIN MONEY_COLLECTIONS AS MCU
                          ON CM.CMT_COLLECTION_ID = MCU.MC_COLLECTION_ID AND CM.CMT_TYPE IN (5, 104) 
                          LEFT JOIN TERMINALS AS TE
                          ON CM.CMT_TERMINAL_ID = TE.TE_TERMINAL_ID 
                          WHERE ((MCU.MC_COLLECTED_BILL_AMOUNT IS NOT NULL AND MCU.MC_COLLECTED_BILL_AMOUNT > 0) OR
                                                (MCU.MC_EXPECTED_BILL_AMOUNT IS NOT NULL AND MCU.MC_EXPECTED_BILL_AMOUNT > 0) OR
                                                (MCU.MC_COLLECTED_COIN_AMOUNT IS NOT NULL AND MCU.MC_COLLECTED_COIN_AMOUNT > 0) OR
                                                (MCU.MC_EXPECTED_COIN_AMOUNT IS NOT NULL AND MCU.MC_EXPECTED_COIN_AMOUNT > 0))  
            )AS MCU ON MTP.CMT_MOVEMENT_ID = MCU.CMT_MOVEMENT_ID AND MCU.TE_ISO_CODE = '''+@_isoCode+'''         
          '
       EXEC SP_EXECUTESQL @_queryColumns      
       SET @_counter = @_counter + 1
     END  
   
   SET @_queryColumns ='   
     SELECT      
        CONVERT(char(11), CMT.CMT_MOVEMENT_DATETIME, 103) + CONVERT(char(8), CMT.CMT_MOVEMENT_DATETIME, 108) AS ''Fecha''
       ,CMT.CMT_CAGE_USER_NAME AS ''Usuario de Bóveda''           
       ,CASE WHEN CMT.CMT_USER_NAME LIKE ''SYS-%'' THEN '''' ELSE ISNULL(CMT.CMT_USER_NAME,'''') END  AS ''Origen/Destino''
       ,ISNULL(CMT.CMT_MACHINE_NAME,'''') AS ''Terminal''           
       ,CMT.CMT_TYPE_NAME AS ''Tipo de movimiento''
       ,CMT.CMT_STATUS_NAME AS ''Estado''
       '+@colsFormat+'  
       ,ISNULL(CMT.CMT_CHIPS_AMOUNT, 0) AS ''Fichas''   
     FROM #GET_CAGE_MOVEMENT_TYPE_TABLE  AS CMT
     '
   EXEC SP_EXECUTESQL @_queryColumns 
   
   DROP TABLE #GET_CAGE_MOVEMENT_TYPE_TABLE  
   DROP TABLE #TABLE_MACHINE_ISO_CODE
   DROP TABLE #TABLE_ISO_CODE     
  
END
GO
GRANT EXECUTE ON GetCageMovementType TO wggui WITH GRANT OPTION 

GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GT_Calculate_DROP_CASHIER]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[GT_Calculate_DROP_CASHIER]
GO

CREATE FUNCTION [dbo].[GT_Calculate_DROP_CASHIER] ( @pOwnSalesAmount AS MONEY, @pExternalSalesAmount as MONEY) RETURNS MONEY
  BEGIN 
  DECLARE @drop as Money
      SET @drop = @pOwnSalesAmount + @pExternalSalesAmount
     
   IF @drop < 0
     set @drop = 0
     
   RETURN @drop
  END
GO

IF EXISTS( SELECT 1 FROM SYS.OBJECTS WHERE NAME = 'ReceptionCustomerVisits')
	DROP PROCEDURE ReceptionCustomerVisits
GO

CREATE PROCEDURE [dbo].[ReceptionCustomerVisits] @pStartDate BIGINT
AS BEGIN

       SELECT                                                                                                                          
                ACCOUNT_PHOTO.APH_PHOTO                                                                                                
               ,ACCOUNTS.AC_HOLDER_NAME                                                                                                
               ,CAST(CUSTOMER_VISITS.CUT_GENDER AS VARCHAR) CUT_GENDER                                                                 
               ,ACCOUNTS.AC_HOLDER_BIRTH_DATE                                                                                          
               ,ADIC_DATA.CUE_DOCUMENT_TYPE                                                                                            
               ,ADIC_DATA.CUE_DOCUMENT_NUMBER                                                                                          
               ,CAST(ACCOUNTS.AC_HOLDER_LEVEL AS VARCHAR)   AC_HOLDER_LEVEL                                                            
               ,ADIC_DATA.ENTRANCES_COUNT                                                                                              
               ,ADIC_DATA.SUM_CUE_TICKET_ENTRY_PRICE_PAID                                                                              
               ,ADIC_DATA.CUE_ENTRANCE_DATETIME
               ,ACCOUNTS.AC_ACCOUNT_ID                                                                                        
                                                                                                                                       
         FROM   CUSTOMER_VISITS                                                                                                        
   INNER JOIN  ACCOUNTS ON     CUSTOMER_VISITS.CUT_CUSTOMER_ID = ACCOUNTS.AC_ACCOUNT_ID                                                
    LEFT JOIN  ACCOUNT_PHOTO ON    CUSTOMER_VISITS.CUT_CUSTOMER_ID = ACCOUNT_PHOTO.APH_ACCOUNT_ID                                      
                                                                                                                                       
                                                                                                                                       
   INNER JOIN                                                                                                                          
               (                                                                                                                       
                  /* datos de la ultima entrada de cada una de esas visitas de esa jornada*/                                           
                  SELECT                                                                                                               
                           CUSTOMER_ENTRANCES.CUE_VISIT_ID                                                                             
                          ,CAST(CUSTOMER_ENTRANCES.CUE_DOCUMENT_TYPE AS VARCHAR) CUE_DOCUMENT_TYPE                                     
                          ,CUSTOMER_ENTRANCES.CUE_DOCUMENT_NUMBER                                                                      
                          ,CUSTOMER_ENTRANCES.CUE_TICKET_ENTRY_PRICE_PAID                                                              
                          ,Z.ENTRANCES_COUNT                                                                                           
                          ,Z.CUE_ENTRANCE_DATETIME                                                                                     
                          ,Z.SUM_CUE_TICKET_ENTRY_PRICE_PAID                                                                           
                    FROM                                                                                                               
                          CUSTOMER_ENTRANCES                                                                                           
              INNER JOIN (                                                                                                             
                           SELECT                                                                                                      
                                    MAX(CUE_ENTRANCE_DATETIME) AS CUE_ENTRANCE_DATETIME                                                
                                   ,CUE_VISIT_ID                                                                                       
                                   ,COUNT(*) AS ENTRANCES_COUNT                                                                        
                                   ,SUM(CUE_TICKET_ENTRY_PRICE_PAID) AS SUM_CUE_TICKET_ENTRY_PRICE_PAID                                
                             FROM                                                                                                      
                                   CUSTOMER_ENTRANCES                                                                                  
                            WHERE                                                                                                      
                                   CUSTOMER_ENTRANCES.CUE_VISIT_ID IN (                                                                
                                                                       SELECT                                                          
                                                                               CUT_VISIT_ID                                            
                                                                         FROM                                                          
                                                                               CUSTOMER_VISITS                                         
                                                                        WHERE                                                          
                                                                               CUT_GAMING_DAY = @pStartDate /* visitas de la jornada*/ 
                                                                      )                                                                
                           GROUP BY CUSTOMER_ENTRANCES.CUE_VISIT_ID  /* ultima entrada de cada una de esas visitas*/                   
                           ) Z                                                                                                         
                      ON CUSTOMER_ENTRANCES.CUE_ENTRANCE_DATETIME = Z.CUE_ENTRANCE_DATETIME                                            
                         AND CUSTOMER_ENTRANCES.CUE_VISIT_ID = Z.CUE_VISIT_ID                                                          
               ) ADIC_DATA                                                                                                             
                                                                                                                                       
           ON CUSTOMER_VISITS.CUT_VISIT_ID = ADIC_DATA.CUE_VISIT_ID                                                                    
                                                                                                                                       
        WHERE  CUT_GAMING_DAY = @pStartDate                                                                                            
        ORDER BY ADIC_DATA.CUE_ENTRANCE_DATETIME                                                                                       

END

GO

GRANT EXECUTE ON RECEPTIONCUSTOMERVISITS TO WGGUI

GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID('GetMetersCollectionToCollection') AND type in ('P', 'PC'))
DROP PROCEDURE GetMetersCollectionToCollection
GO

CREATE PROCEDURE GetMetersCollectionToCollection
 (@pStarDateTime DATETIME,
  @pEndDateTime  DATETIME)
AS
BEGIN

DECLARE @DEFAULT_FLOOR_ID NVARCHAR(20);
DECLARE @_Meter_Coin_In INT;
DECLARE @_Meter_Coin_Out INT;
DECLARE @_Meter_Jackpots INT;
DECLARE @_Meter_Door_Open INT;

DECLARE @_1_Bill_Value INTEGER;
DECLARE @_2_Bill_Value INTEGER;
DECLARE @_5_Bill_Value INTEGER;
DECLARE @_10_Bill_Value INTEGER;
DECLARE @_20_Bill_Value INTEGER;
DECLARE @_25_Bill_Value INTEGER;
DECLARE @_50_Bill_Value INTEGER;
DECLARE @_100_Bill_Value INTEGER;
DECLARE @_200_Bill_Value INTEGER;
DECLARE @_250_Bill_Value INTEGER;
DECLARE @_500_Bill_Value INTEGER;
DECLARE @_1000_Bill_Value INTEGER;
DECLARE @_2000_Bill_Value INTEGER;
DECLARE @_2500_Bill_Value INTEGER;
DECLARE @_5000_Bill_Value INTEGER;
DECLARE @_10000_Bill_Value INTEGER;
DECLARE @_20000_Bill_Value INTEGER;
DECLARE @_25000_Bill_Value INTEGER;
DECLARE @_50000_Bill_Value INTEGER;
DECLARE @_100000_Bill_Value INTEGER;
DECLARE @_200000_Bill_Value INTEGER;
DECLARE @_250000_Bill_Value INTEGER;
DECLARE @_500000_Bill_Value INTEGER;
DECLARE @_1000000_Bill_Value INTEGER;

SET @_Meter_Coin_In = 0
SET @_Meter_Coin_Out = 1
SET @_Meter_Jackpots = 2
SET @_Meter_Door_Open = 65553

SET @DEFAULT_FLOOR_ID = '---'

SET @_1_Bill_Value = 1
SET @_2_Bill_Value = 2
SET @_5_Bill_Value = 5
SET @_10_Bill_Value = 10
SET @_20_Bill_Value = 20
SET @_25_Bill_Value = 25
SET @_50_Bill_Value = 50
SET @_100_Bill_Value = 100
SET @_200_Bill_Value = 200
SET @_250_Bill_Value = 250
SET @_500_Bill_Value = 500
SET @_1000_Bill_Value = 1000
SET @_2000_Bill_Value = 2000
SET @_2500_Bill_Value = 2500
SET @_5000_Bill_Value = 5000
SET @_10000_Bill_Value = 10000
SET @_20000_Bill_Value = 20000
SET @_25000_Bill_Value = 25000
SET @_50000_Bill_Value = 50000
SET @_100000_Bill_Value = 100000
SET @_200000_Bill_Value = 200000
SET @_250000_Bill_Value = 250000
SET @_500000_Bill_Value = 500000
SET @_1000000_Bill_Value = 1000000

CREATE TABLE #WORK_TABLE(
   ID INT Identity(1,1),
   MC_START_INSERT_DATETIME  DATETIME,
   MC_END_INSERT_DATETIME DATETIME,
   MC_TERMINAL_ID INTEGER,
   MC_FLOOR_ID VARCHAR(20),
   MC_DENOMINATION VARCHAR(MAX),
   ME_COIN_IN MONEY,
   ME_COIN_OUT MONEY,
   ME_JACKPOT MONEY,
   ME_DOOR_OPEN INTEGER,
   MCM_1_BILL_EXPECTED INTEGER,
   MCM_2_BILL_EXPECTED INTEGER,
   MCM_5_BILL_EXPECTED INTEGER,
   MCM_10_BILL_EXPECTED INTEGER,
   MCM_20_BILL_EXPECTED INTEGER,
   MCM_25_BILL_EXPECTED INTEGER,
   MCM_50_BILL_EXPECTED INTEGER,
   MCM_100_BILL_EXPECTED INTEGER,
   MCM_200_BILL_EXPECTED INTEGER,
   MCM_250_BILL_EXPECTED INTEGER,
   MCM_500_BILL_EXPECTED INTEGER,
   MCM_1000_BILL_EXPECTED INTEGER,
   MCM_2000_BILL_EXPECTED INTEGER,
   MCM_2500_BILL_EXPECTED INTEGER,
   MCM_5000_BILL_EXPECTED INTEGER,
   MCM_10000_BILL_EXPECTED INTEGER,
   MCM_20000_BILL_EXPECTED INTEGER,
   MCM_25000_BILL_EXPECTED INTEGER,
   MCM_50000_BILL_EXPECTED INTEGER,
   MCM_100000_BILL_EXPECTED INTEGER,
   MCM_200000_BILL_EXPECTED INTEGER,
   MCM_250000_BILL_EXPECTED INTEGER,
   MCM_500000_BILL_EXPECTED INTEGER,
   MCM_1000000_BILL_EXPECTED INTEGER,
   MC_EXPECTED_BILL_AMOUNT INTEGER,   
   MCM_1_BILL_COLLECTED INTEGER,
   MCM_2_BILL_COLLECTED INTEGER,
   MCM_5_BILL_COLLECTED INTEGER,
   MCM_10_BILL_COLLECTED INTEGER,
   MCM_20_BILL_COLLECTED INTEGER,
   MCM_25_BILL_COLLECTED INTEGER,
   MCM_50_BILL_COLLECTED INTEGER,
   MCM_100_BILL_COLLECTED INTEGER,
   MCM_200_BILL_COLLECTED INTEGER,
   MCM_250_BILL_COLLECTED INTEGER,
   MCM_500_BILL_COLLECTED INTEGER,
   MCM_1000_BILL_COLLECTED INTEGER,
   MCM_2000_BILL_COLLECTED INTEGER,
   MCM_2500_BILL_COLLECTED INTEGER,
   MCM_5000_BILL_COLLECTED INTEGER,
   MCM_10000_BILL_COLLECTED INTEGER,
   MCM_20000_BILL_COLLECTED INTEGER,
   MCM_25000_BILL_COLLECTED INTEGER,
   MCM_50000_BILL_COLLECTED INTEGER,
   MCM_100000_BILL_COLLECTED INTEGER,
   MCM_200000_BILL_COLLECTED INTEGER,
   MCM_250000_BILL_COLLECTED INTEGER,
   MCM_500000_BILL_COLLECTED INTEGER,
   MCM_1000000_BILL_COLLECTED INTEGER,
   MC_COLLECTED_BILL_AMOUNT INTEGER
) 
INSERT INTO #WORK_TABLE
SELECT    
     MC.MC_START_INSERT_DATETIME 
    ,MC.MC_END_INSERT_DATETIME
    ,MC.MC_TERMINAL_ID  
    ,MC.MC_FLOOR_ID
    ,MC.MC_DENOMINATION 
    ,NULL AS ME_COIN_IN
    ,NULL AS ME_COIN_OUT
    ,NULL AS ME_JACKPOT
    ,NULL AS ME_DOOR_OPEN
	,ISNULL(MCM.MCM_1_BILL_NUM, 0) * @_1_Bill_Value 
	,ISNULL(MCM.MCM_2_BILL_NUM, 0) * @_2_Bill_Value
	,ISNULL(MCM.MCM_5_BILL_NUM, 0) * @_5_Bill_Value
	,ISNULL(MCM.MCM_10_BILL_NUM, 0) * @_10_Bill_Value
	,ISNULL(MCM.MCM_20_BILL_NUM, 0) * @_20_Bill_Value
	,ISNULL(MCM.MCM_25_BILL_NUM, 0) * @_25_Bill_Value
	,ISNULL(MCM.MCM_50_BILL_NUM, 0) * @_50_Bill_Value
	,ISNULL(MCM.MCM_100_BILL_NUM, 0) * @_100_Bill_Value
	,ISNULL(MCM.MCM_200_BILL_NUM, 0) * @_200_Bill_Value
	,ISNULL(MCM.MCM_250_BILL_NUM, 0) * @_250_Bill_Value
	,ISNULL(MCM.MCM_500_BILL_NUM, 0) * @_500_Bill_Value
	,ISNULL(MCM.MCM_1000_BILL_NUM, 0) * @_1000_Bill_Value
	,ISNULL(MCM.MCM_2000_BILL_NUM, 0) * @_2000_Bill_Value
	,ISNULL(MCM.MCM_2500_BILL_NUM, 0) * @_2500_Bill_Value
	,ISNULL(MCM.MCM_5000_BILL_NUM, 0) * @_5000_Bill_Value
	,ISNULL(MCM.MCM_10000_BILL_NUM, 0) * @_10000_Bill_Value
	,ISNULL(MCM.MCM_20000_BILL_NUM, 0) * @_20000_Bill_Value
	,ISNULL(MCM.MCM_25000_BILL_NUM, 0) * @_25000_Bill_Value
	,ISNULL(MCM.MCM_50000_BILL_NUM, 0) * @_50000_Bill_Value
	,ISNULL(MCM.MCM_100000_BILL_NUM, 0) * @_100000_Bill_Value
	,ISNULL(MCM.MCM_200000_BILL_NUM, 0) * @_200000_Bill_Value
	,ISNULL(MCM.MCM_250000_BILL_NUM, 0) * @_250000_Bill_Value
	,ISNULL(MCM.MCM_500000_BILL_NUM, 0) * @_500000_Bill_Value
	,ISNULL(MCM.MCM_1000000_BILL_NUM, 0) * @_1000000_Bill_Value
	,MC.MC_EXPECTED_BILL_AMOUNT 	
	,ISNULL(MCD.MCD_1_BILL_COLLECTED , 0)
	,ISNULL(MCD.MCD_2_BILL_COLLECTED , 0)
	,ISNULL(MCD.MCD_5_BILL_COLLECTED , 0)
	,ISNULL(MCD.MCD_10_BILL_COLLECTED , 0)
	,ISNULL(MCD.MCD_20_BILL_COLLECTED , 0)
	,ISNULL(MCD.MCD_25_BILL_COLLECTED , 0)
	,ISNULL(MCD.MCD_50_BILL_COLLECTED , 0)
	,ISNULL(MCD.MCD_100_BILL_COLLECTED , 0)
	,ISNULL(MCD.MCD_200_BILL_COLLECTED , 0)
	,ISNULL(MCD.MCD_250_BILL_COLLECTED , 0)
	,ISNULL(MCD.MCD_500_BILL_COLLECTED , 0)
	,ISNULL(MCD.MCD_1000_BILL_COLLECTED , 0)
	,ISNULL(MCD.MCD_2000_BILL_COLLECTED , 0)
	,ISNULL(MCD.MCD_2500_BILL_COLLECTED , 0)
	,ISNULL(MCD.MCD_5000_BILL_COLLECTED , 0)
	,ISNULL(MCD.MCD_10000_BILL_COLLECTED , 0)
	,ISNULL(MCD.MCD_20000_BILL_COLLECTED , 0)
	,ISNULL(MCD.MCD_25000_BILL_COLLECTED , 0)
	,ISNULL(MCD.MCD_50000_BILL_COLLECTED , 0)
	,ISNULL(MCD.MCD_100000_BILL_COLLECTED , 0)
	,ISNULL(MCD.MCD_200000_BILL_COLLECTED , 0)
	,ISNULL(MCD.MCD_250000_BILL_COLLECTED , 0)
	,ISNULL(MCD.MCD_500000_BILL_COLLECTED , 0)
	,ISNULL(MCD.MCD_1000000_BILL_COLLECTED , 0)
	,MC.MC_COLLECTED_BILL_AMOUNT 
      FROM
		(SELECT 			
			   MC.MC_COLLECTION_ID  AS MC_COLLECTION_ID
		    ,DATEADD(HOUR, DATEDIFF(HOUR, 0, MC.MC_DATETIME), 0) AS MC_START_INSERT_DATETIME	
		    ,MC_END_INSERT_DATETIME = DATEADD(HOUR, DATEDIFF(HOUR, 0,
												          ISNULL((SELECT TOP 1 MT.MC_DATETIME 
												                    FROM MONEY_COLLECTIONS AS MT 
                                           WHERE MT.MC_TERMINAL_ID = MC.MC_TERMINAL_ID AND MT.MC_COLLECTION_ID > MC.MC_COLLECTION_ID
												                   ORDER BY MT.MC_COLLECTION_ID ASC
												                  )
												                  ,DATEADD(HOUR, 1, MC.MC_EXTRACTION_DATETIME)))  , 0
												                 )
			,TE.TE_TERMINAL_ID AS MC_TERMINAL_ID
			,ISNULL(TE.TE_FLOOR_ID,@DEFAULT_FLOOR_ID) MC_FLOOR_ID 																			   
			,ISNULL(CAST(TE.TE_DENOMINATION AS VARCHAR(MAX)), TE.TE_MULTI_DENOMINATION) AS MC_DENOMINATION	
			,MC.MC_EXPECTED_BILL_AMOUNT AS MC_EXPECTED_BILL_AMOUNT
			,MC.MC_COLLECTED_BILL_AMOUNT AS MC_COLLECTED_BILL_AMOUNT
			,MC.MC_COLLECTION_DATETIME AS  MC_COLLECTION_DATETIME
		 FROM MONEY_COLLECTIONS AS MC 
		 INNER JOIN TERMINALS AS TE 
		 ON MC.MC_TERMINAL_ID = TE.TE_TERMINAL_ID AND TE.TE_TERMINAL_TYPE IN(5, 105) AND MC.MC_STATUS IN(1,2,3)
		)
AS MC
LEFT JOIN MONEY_COLLECTION_METERS AS MCM
ON MC.MC_COLLECTION_ID = MCM.MCM_MONEY_COLLECTION_ID 
LEFT JOIN(
		SELECT  MD.MCD_COLLECTION_ID
				,SUM(CASE MD.MCD_FACE_VALUE WHEN @_1_Bill_Value THEN MD.MCD_FACE_VALUE * MD.MCD_NUM_COLLECTED  ELSE 0 END) AS MCD_1_BILL_COLLECTED
				,SUM(CASE MD.MCD_FACE_VALUE WHEN @_2_Bill_Value THEN MD.MCD_FACE_VALUE * MD.MCD_NUM_COLLECTED  ELSE 0 END) AS MCD_2_BILL_COLLECTED
				,SUM(CASE MD.MCD_FACE_VALUE WHEN @_5_Bill_Value THEN MD.MCD_FACE_VALUE * MD.MCD_NUM_COLLECTED  ELSE 0 END) AS MCD_5_BILL_COLLECTED
				,SUM(CASE MD.MCD_FACE_VALUE WHEN @_10_Bill_Value THEN MD.MCD_FACE_VALUE * MD.MCD_NUM_COLLECTED  ELSE 0 END) AS MCD_10_BILL_COLLECTED
				,SUM(CASE MD.MCD_FACE_VALUE WHEN @_20_Bill_Value THEN MD.MCD_FACE_VALUE * MD.MCD_NUM_COLLECTED  ELSE 0 END) AS MCD_20_BILL_COLLECTED
				,SUM(CASE MD.MCD_FACE_VALUE WHEN @_25_Bill_Value THEN MD.MCD_FACE_VALUE * MD.MCD_NUM_COLLECTED  ELSE 0 END) AS MCD_25_BILL_COLLECTED
				,SUM(CASE MD.MCD_FACE_VALUE WHEN @_50_Bill_Value THEN MD.MCD_FACE_VALUE * MD.MCD_NUM_COLLECTED  ELSE 0 END) AS MCD_50_BILL_COLLECTED
				,SUM(CASE MD.MCD_FACE_VALUE WHEN @_100_Bill_Value THEN MD.MCD_FACE_VALUE * MD.MCD_NUM_COLLECTED  ELSE 0 END) AS MCD_100_BILL_COLLECTED
				,SUM(CASE MD.MCD_FACE_VALUE WHEN @_200_Bill_Value THEN MD.MCD_FACE_VALUE * MD.MCD_NUM_COLLECTED  ELSE 0 END) AS MCD_200_BILL_COLLECTED
				,SUM(CASE MD.MCD_FACE_VALUE WHEN @_250_Bill_Value THEN MD.MCD_FACE_VALUE * MD.MCD_NUM_COLLECTED  ELSE 0 END) AS MCD_250_BILL_COLLECTED
				,SUM(CASE MD.MCD_FACE_VALUE WHEN @_500_Bill_Value THEN MD.MCD_FACE_VALUE * MD.MCD_NUM_COLLECTED  ELSE 0 END) AS MCD_500_BILL_COLLECTED
				,SUM(CASE MD.MCD_FACE_VALUE WHEN @_1000_Bill_Value THEN MD.MCD_FACE_VALUE * MD.MCD_NUM_COLLECTED  ELSE 0 END) AS MCD_1000_BILL_COLLECTED
				,SUM(CASE MD.MCD_FACE_VALUE WHEN @_2000_Bill_Value THEN MD.MCD_FACE_VALUE * MD.MCD_NUM_COLLECTED  ELSE 0 END) AS MCD_2000_BILL_COLLECTED
				,SUM(CASE MD.MCD_FACE_VALUE WHEN @_2500_Bill_Value THEN MD.MCD_FACE_VALUE * MD.MCD_NUM_COLLECTED  ELSE 0 END) AS MCD_2500_BILL_COLLECTED
				,SUM(CASE MD.MCD_FACE_VALUE WHEN @_5000_Bill_Value THEN MD.MCD_FACE_VALUE * MD.MCD_NUM_COLLECTED  ELSE 0 END) AS MCD_5000_BILL_COLLECTED
				,SUM(CASE MD.MCD_FACE_VALUE WHEN @_10000_Bill_Value THEN MD.MCD_FACE_VALUE * MD.MCD_NUM_COLLECTED  ELSE 0 END) AS MCD_10000_BILL_COLLECTED
				,SUM(CASE MD.MCD_FACE_VALUE WHEN @_20000_Bill_Value THEN MD.MCD_FACE_VALUE * MD.MCD_NUM_COLLECTED  ELSE 0 END) AS MCD_20000_BILL_COLLECTED
				,SUM(CASE MD.MCD_FACE_VALUE WHEN @_25000_Bill_Value THEN MD.MCD_FACE_VALUE * MD.MCD_NUM_COLLECTED  ELSE 0 END) AS MCD_25000_BILL_COLLECTED
				,SUM(CASE MD.MCD_FACE_VALUE WHEN @_50000_Bill_Value THEN MD.MCD_FACE_VALUE * MD.MCD_NUM_COLLECTED  ELSE 0 END) AS MCD_50000_BILL_COLLECTED
				,SUM(CASE MD.MCD_FACE_VALUE WHEN @_100000_Bill_Value THEN MD.MCD_FACE_VALUE * MD.MCD_NUM_COLLECTED  ELSE 0 END) AS MCD_100000_BILL_COLLECTED
				,SUM(CASE MD.MCD_FACE_VALUE WHEN @_200000_Bill_Value THEN MD.MCD_FACE_VALUE * MD.MCD_NUM_COLLECTED  ELSE 0 END) AS MCD_200000_BILL_COLLECTED
		    ,SUM(CASE MD.MCD_FACE_VALUE WHEN @_250000_Bill_Value THEN MD.MCD_FACE_VALUE * MD.MCD_NUM_COLLECTED  ELSE 0 END) AS MCD_250000_BILL_COLLECTED
		    ,SUM(CASE MD.MCD_FACE_VALUE WHEN @_500000_Bill_Value THEN MD.MCD_FACE_VALUE * MD.MCD_NUM_COLLECTED  ELSE 0 END) AS MCD_500000_BILL_COLLECTED
		    ,SUM(CASE MD.MCD_FACE_VALUE WHEN @_1000000_Bill_Value THEN MD.MCD_FACE_VALUE * MD.MCD_NUM_COLLECTED  ELSE 0 END) AS MCD_1000000_BILL_COLLECTED		
		FROM MONEY_COLLECTION_DETAILS AS MD
		GROUP BY MD.MCD_COLLECTION_ID
	  ) AS MCD	
ON MC.MC_COLLECTION_ID = MCD.MCD_COLLECTION_ID
WHERE (MC.mc_collection_datetime  >= @pStarDateTime AND MC.mc_collection_datetime  < @pEndDateTime)
ORDER BY MC.MC_COLLECTION_ID


DECLARE @_counter INTEGER;	
DECLARE @_count INTEGER	;
DECLARE @_start_date DATETIME;
DECLARE @_end_date DATETIME;
DECLARE @_terminal_id INTEGER;

DECLARE @_coin_in_value MONEY;
DECLARE @_coin_out_value MONEY;
DECLARE @_jackpot_value MONEY;
DECLARE @_door_open_value INTEGER;

SET @_counter = 1				
SET @_count = (SELECT COUNT(*)  FROM #WORK_TABLE) + 1

	WHILE NOT @_counter = @_count
	BEGIN

		SELECT 
		   @_start_date = WT.MC_START_INSERT_DATETIME,
		   @_end_date = WT.MC_END_INSERT_DATETIME,
		   @_terminal_id = WT.MC_TERMINAL_ID    
		FROM #WORK_TABLE AS WT
		WHERE WT.ID = @_counter 
	   
		SELECT                
			 @_coin_in_value   =  ISNULL(SUM(CASE TH.TSMH_METER_CODE WHEN @_Meter_Coin_In    THEN TSMH_METER_INCREMENT  ELSE 0 END)  * 0.01, 0)
			,@_coin_out_value  =  ISNULL(SUM(CASE TH.TSMH_METER_CODE WHEN @_Meter_Coin_Out   THEN TSMH_METER_INCREMENT  ELSE 0 END)  * 0.01, 0) 
			,@_jackpot_value   =  ISNULL(SUM(CASE TH.TSMH_METER_CODE WHEN @_Meter_Jackpots   THEN TSMH_METER_INCREMENT  ELSE 0 END)  * 0.01, 0) 
			,@_door_open_value =  ISNULL(SUM(CASE TH.TSMH_METER_CODE WHEN @_Meter_Door_Open  THEN TSMH_METER_INCREMENT  ELSE 0 END), 0)
		FROM TERMINAL_SAS_METERS_HISTORY AS TH
		WHERE TH.TSMH_TERMINAL_ID = @_terminal_id 
			  AND (TH.TSMH_DATETIME >= @_start_date AND TH.TSMH_DATETIME < @_end_date) 
			  AND TH.TSMH_METER_CODE IN (@_Meter_Coin_In, @_Meter_Coin_Out, @_Meter_Jackpots, @_Meter_Door_Open) 
			  AND TH.TSMH_TYPE = 1 	        
	    
		UPDATE #WORK_TABLE 
		SET
		 ME_COIN_IN = @_coin_in_value,
		 ME_COIN_OUT = @_coin_out_value,
		 ME_JACKPOT = @_jackpot_value,
		 ME_DOOR_OPEN = @_door_open_value
		WHERE ID = @_counter 
			
		SET @_counter = @_counter + 1
	END

SELECT 
   ID,
   MC_START_INSERT_DATETIME,
   MC_END_INSERT_DATETIME,
   MC_FLOOR_ID,
   MC_DENOMINATION,
   ME_COIN_IN,
   ME_COIN_OUT,
   ME_JACKPOT,
   ME_DOOR_OPEN,
   MCM_1_BILL_EXPECTED,
   MCM_2_BILL_EXPECTED,
   MCM_5_BILL_EXPECTED,
   MCM_10_BILL_EXPECTED,
   MCM_20_BILL_EXPECTED,
   MCM_25_BILL_EXPECTED,
   MCM_50_BILL_EXPECTED,
   MCM_100_BILL_EXPECTED,
   MCM_200_BILL_EXPECTED,
   MCM_250_BILL_EXPECTED,
   MCM_500_BILL_EXPECTED,
   MCM_1000_BILL_EXPECTED,
   MCM_2000_BILL_EXPECTED,
   MCM_2500_BILL_EXPECTED,
   MCM_5000_BILL_EXPECTED,
   MCM_10000_BILL_EXPECTED,
   MCM_20000_BILL_EXPECTED,
   MCM_25000_BILL_EXPECTED,
   MCM_50000_BILL_EXPECTED,
   MCM_100000_BILL_EXPECTED,
   MCM_200000_BILL_EXPECTED,
   MCM_250000_BILL_EXPECTED,
   MCM_500000_BILL_EXPECTED,
   MCM_1000000_BILL_EXPECTED,
   MC_EXPECTED_BILL_AMOUNT,   
   MCM_1_BILL_COLLECTED,
   MCM_2_BILL_COLLECTED,
   MCM_5_BILL_COLLECTED,
   MCM_10_BILL_COLLECTED,
   MCM_20_BILL_COLLECTED,
   MCM_25_BILL_COLLECTED,
   MCM_50_BILL_COLLECTED,
   MCM_100_BILL_COLLECTED,
   MCM_200_BILL_COLLECTED,
   MCM_250_BILL_COLLECTED,
   MCM_500_BILL_COLLECTED,
   MCM_1000_BILL_COLLECTED,
   MCM_2000_BILL_COLLECTED,
   MCM_2500_BILL_COLLECTED,
   MCM_5000_BILL_COLLECTED,
   MCM_10000_BILL_COLLECTED,
   MCM_20000_BILL_COLLECTED,
   MCM_25000_BILL_COLLECTED,
   MCM_50000_BILL_COLLECTED,
   MCM_100000_BILL_COLLECTED,
   MCM_200000_BILL_COLLECTED,
   MCM_250000_BILL_COLLECTED,
   MCM_500000_BILL_COLLECTED,
   MCM_1000000_BILL_COLLECTED,
   MC_COLLECTED_BILL_AMOUNT
FROM #WORK_TABLE

DROP TABLE #WORK_TABLE

END
GO
GRANT EXECUTE ON GetMetersCollectionToCollection TO wggui WITH GRANT OPTION 

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID('GetCageMovementType_GR') AND type in ('P', 'PC'))
DROP PROCEDURE GetCageMovementType_GR
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID('GetMetersCollectionToCollection_GR') AND type in ('P', 'PC'))
DROP PROCEDURE GetMetersCollectionToCollection_GR
GO

CREATE PROCEDURE GetCageMovementType_GR
 (@pFrom DATETIME,
  @pTo  DATETIME)
AS
BEGIN
	EXEC GetCageMovementType @pFrom, @pTo
END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID('GetMetersCollectionToCollection_PR') AND type in ('P', 'PC'))
DROP PROCEDURE GetMetersCollectionToCollection_PR
GO

CREATE PROCEDURE GetMetersCollectionToCollection_PR
 (@pStarDateTime DATETIME,
  @pEndDateTime  DATETIME)
AS
BEGIN

CREATE TABLE #WORK_TABLE_PR(
   ID INTEGER,
   MC_START_INSERT_DATETIME  DATETIME,
   MC_END_INSERT_DATETIME DATETIME,
   MC_FLOOR_ID VARCHAR(20),
   MC_DENOMINATION VARCHAR(MAX),
   ME_COIN_IN MONEY,
   ME_COIN_OUT MONEY,
   ME_JACKPOT MONEY,
   ME_DOOR_OPEN INTEGER,
   MCM_1_BILL_EXPECTED INTEGER,
   MCM_2_BILL_EXPECTED INTEGER,
   MCM_5_BILL_EXPECTED INTEGER,
   MCM_10_BILL_EXPECTED INTEGER,
   MCM_20_BILL_EXPECTED INTEGER,
   MCM_25_BILL_EXPECTED INTEGER,
   MCM_50_BILL_EXPECTED INTEGER,
   MCM_100_BILL_EXPECTED INTEGER,
   MCM_200_BILL_EXPECTED INTEGER,
   MCM_250_BILL_EXPECTED INTEGER,
   MCM_500_BILL_EXPECTED INTEGER,
   MCM_1000_BILL_EXPECTED INTEGER,
   MCM_2000_BILL_EXPECTED INTEGER,
   MCM_2500_BILL_EXPECTED INTEGER,
   MCM_5000_BILL_EXPECTED INTEGER,
   MCM_10000_BILL_EXPECTED INTEGER,
   MCM_20000_BILL_EXPECTED INTEGER,
   MCM_25000_BILL_EXPECTED INTEGER,
   MCM_50000_BILL_EXPECTED INTEGER,
   MCM_100000_BILL_EXPECTED INTEGER,
   MCM_200000_BILL_EXPECTED INTEGER,
   MCM_250000_BILL_EXPECTED INTEGER,
   MCM_500000_BILL_EXPECTED INTEGER,
   MCM_1000000_BILL_EXPECTED INTEGER,
   MC_EXPECTED_BILL_AMOUNT INTEGER,   
   MCM_1_BILL_COLLECTED INTEGER,
   MCM_2_BILL_COLLECTED INTEGER,
   MCM_5_BILL_COLLECTED INTEGER,
   MCM_10_BILL_COLLECTED INTEGER,
   MCM_20_BILL_COLLECTED INTEGER,
   MCM_25_BILL_COLLECTED INTEGER,
   MCM_50_BILL_COLLECTED INTEGER,
   MCM_100_BILL_COLLECTED INTEGER,
   MCM_200_BILL_COLLECTED INTEGER,
   MCM_250_BILL_COLLECTED INTEGER,
   MCM_500_BILL_COLLECTED INTEGER,
   MCM_1000_BILL_COLLECTED INTEGER,
   MCM_2000_BILL_COLLECTED INTEGER,
   MCM_2500_BILL_COLLECTED INTEGER,
   MCM_5000_BILL_COLLECTED INTEGER,
   MCM_10000_BILL_COLLECTED INTEGER,
   MCM_20000_BILL_COLLECTED INTEGER,
   MCM_25000_BILL_COLLECTED INTEGER,
   MCM_50000_BILL_COLLECTED INTEGER,
   MCM_100000_BILL_COLLECTED INTEGER,
   MCM_200000_BILL_COLLECTED INTEGER,
   MCM_250000_BILL_COLLECTED INTEGER,
   MCM_500000_BILL_COLLECTED INTEGER,
   MCM_1000000_BILL_COLLECTED INTEGER,
   MC_COLLECTED_BILL_AMOUNT INTEGER
) 
INSERT INTO #WORK_TABLE_PR 
EXEC GetMetersCollectionToCollection @pStarDateTime, @pEndDateTime 

SELECT  
    CONVERT(char(10), WT.MC_START_INSERT_DATETIME, 103)  AS 'Gaming Date'
   ,WT.MC_FLOOR_ID                                       AS 'Floor ID'    
   ,WT.MC_DENOMINATION                                   AS 'Machine Denomination' 
   ,WT.ME_COIN_IN                                        AS 'Coin In' 
   ,WT.ME_COIN_OUT                                       AS 'Coin Out'
   ,0                                                    AS 'Coin Drop'
   ,WT.ME_JACKPOT                                        AS 'Jackpots'                  
   ,WT.ME_DOOR_OPEN                                      AS 'Main Door Open'
   ,WT.MCM_1_BILL_EXPECTED                               AS 'Soft Count $1 Meter'  
   ,WT.MCM_5_BILL_EXPECTED                               AS 'Soft Count $5 Meter'
   ,WT.MCM_10_BILL_EXPECTED                              AS 'Soft Count $10 Meter'
   ,WT.MCM_20_BILL_EXPECTED                              AS 'Soft Count $20 Meter'
   ,WT.MCM_50_BILL_EXPECTED                              AS 'Soft Count $50 Meter'
   ,WT.MCM_100_BILL_EXPECTED                             AS 'Soft Count $100 Meter'
   ,WT.MC_EXPECTED_BILL_AMOUNT                           AS 'Soft Count Total Meter'
   ,0                                                    AS 'Total Coin Drop fisico'
   ,WT.MCM_1_BILL_COLLECTED                              AS '$1 fisico'
   ,WT.MCM_5_BILL_COLLECTED                              AS '$5 fisico'
   ,WT.MCM_10_BILL_COLLECTED                             AS '$10 fisico'
   ,WT.MCM_20_BILL_COLLECTED                             AS '$20 fisico'
   ,WT.MCM_50_BILL_COLLECTED                             AS '$50 fisico'
   ,WT.MCM_100_BILL_COLLECTED                            AS '$100 fisico'
   ,WT.MC_COLLECTED_BILL_AMOUNT                          AS 'Total contado en el soft count'
FROM #WORK_TABLE_PR AS WT

DROP TABLE #WORK_TABLE_PR
END
GO
GRANT EXECUTE ON GetMetersCollectionToCollection_PR TO wggui WITH GRANT OPTION 
GO

CREATE PROCEDURE GetMetersCollectionToCollection_GR
(@pFrom DATETIME,
@pTo  DATETIME)
AS
BEGIN
EXEC GetMetersCollectionToCollection_PR @pFrom,  @pTo
END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID('report_tool_config') AND type in ('U'))
  BEGIN      
      DELETE FROM report_tool_config
      DBCC CHECKIDENT (report_tool_config, RESEED,0)

      INSERT [report_tool_config] ([rtc_form_id], [rtc_location_menu], [rtc_report_name], [rtc_store_name], [rtc_design_filter], [rtc_design_sheets], [rtc_mailing], [rtc_status]) 
      VALUES (11000, 7, N'<LanguageResources><NLS09><Label>Cage movements for currency</Label></NLS09><NLS10><Label>Movimientos de bóveda por divisa</Label></NLS10></LanguageResources>', N'GetCageMovementType_GR', N'<ReportToolDesignFilterDTO><FilterType>FromToDate</FilterType></ReportToolDesignFilterDTO>', 
      N'<ArrayOfReportToolDesignSheetsDTO><ReportToolDesignSheetsDTO><LanguageResources><NLS09><Label>Cage movements for currency</Label></NLS09><NLS10><Label>Movimientos por divisa</Label></NLS10></LanguageResources><Columns><ReportToolDesignColumn><Code>Fecha</Code><Width>300</Width><EquityMatchType>Equality</EquityMatchType><LanguageResources><NLS09><Label>Date</Label></NLS09><NLS10><Label>Fecha</Label></NLS10></LanguageResources></ReportToolDesignColumn><ReportToolDesignColumn><Code>Usuario de Bóveda</Code><Width>300</Width><EquityMatchType>Equality</EquityMatchType><LanguageResources><NLS09><Label>Cash cage user</Label></NLS09><NLS10><Label>Usuario de Bóveda</Label></NLS10></LanguageResources></ReportToolDesignColumn><ReportToolDesignColumn><Code>Origen/Destino</Code><Width>300</Width><EquityMatchType>Equality</EquityMatchType><LanguageResources><NLS09><Label>Source/Destination</Label></NLS09><NLS10><Label>Origen/Destino</Label></NLS10></LanguageResources></ReportToolDesignColumn><ReportToolDesignColumn><Code>Tipo de movimiento</Code><Width>300</Width><EquityMatchType>Equality</EquityMatchType><LanguageResources><NLS09><Label>Type</Label></NLS09><NLS10><Label>Tipo de movimiento</Label></NLS10></LanguageResources></ReportToolDesignColumn><ReportToolDesignColumn><Code>Estado</Code><Width>300</Width><EquityMatchType>Equality</EquityMatchType><LanguageResources><NLS09><Label>Status</Label></NLS09><NLS10><Label>Estado</Label></NLS10></LanguageResources></ReportToolDesignColumn><ReportToolDesignColumn><Code>Otros</Code><Width>300</Width><EquityMatchType>Contains</EquityMatchType><LanguageResources><NLS09><Label>Other</Label></NLS09><NLS10><Label>Otros</Label></NLS10></LanguageResources></ReportToolDesignColumn><ReportToolDesignColumn><Code>Fichas</Code><Width>300</Width><EquityMatchType>Equality</EquityMatchType><LanguageResources><NLS09><Label>Chips</Label></NLS09><NLS10><Label>Fichas</Label></NLS10></LanguageResources></ReportToolDesignColumn></Columns></ReportToolDesignSheetsDTO></ArrayOfReportToolDesignSheetsDTO>', 1, 1)
      
      INSERT [report_tool_config] ([rtc_form_id], [rtc_location_menu], [rtc_report_name], [rtc_store_name], [rtc_design_filter], [rtc_design_sheets], [rtc_mailing], [rtc_status]) 
      VALUES (11001, 12, N'<LanguageResources><NLS09><Label>Table gaming activity</Label></NLS09><NLS10><Label>Actividad en mesa de juego</Label></NLS10></LanguageResources>', N'sp_GetTablesActivityFormatted', N'<ReportToolDesignFilterDTO><FilterType>MonthYear</FilterType></ReportToolDesignFilterDTO>', 
      N'<ArrayOfReportToolDesignSheetsDTO><ReportToolDesignSheetsDTO><LanguageResources><NLS09><Label>Gaming table activity</Label></NLS09><NLS10><Label>Actividad en mesa de juego</Label></NLS10></LanguageResources><Columns><ReportToolDesignColumn><Code>TIPO</Code><Width>300</Width><EquityMatchType>Equality</EquityMatchType><LanguageResources><NLS09><Label>Type</Label></NLS09><NLS10><Label>Tipo</Label></NLS10></LanguageResources></ReportToolDesignColumn><ReportToolDesignColumn><Code>MESA</Code><Width>300</Width><EquityMatchType>Equality</EquityMatchType><LanguageResources><NLS09><Label>Table</Label></NLS09><NLS10><Label>Mesa</Label></NLS10></LanguageResources></ReportToolDesignColumn><ReportToolDesignColumn><Code>MES</Code><Width>300</Width><EquityMatchType>Equality</EquityMatchType><LanguageResources><NLS09><Label>Month</Label></NLS09><NLS10><Label>Mes</Label></NLS10></LanguageResources></ReportToolDesignColumn><ReportToolDesignColumn><Code>DIAS HABILITADA</Code><Width>300</Width><EquityMatchType>Equality</EquityMatchType><LanguageResources><NLS09><Label>Enabled days</Label></NLS09><NLS10><Label>Dias habilitada</Label></NLS10></LanguageResources></ReportToolDesignColumn><ReportToolDesignColumn><Code>DIAS USADO</Code><Width>300</Width><EquityMatchType>Equality</EquityMatchType><LanguageResources><NLS09><Label>Used days</Label></NLS09><NLS10><Label>Dias usado</Label></NLS10></LanguageResources></ReportToolDesignColumn></Columns></ReportToolDesignSheetsDTO></ArrayOfReportToolDesignSheetsDTO>', 1, 1)
      
      INSERT [report_tool_config] ([rtc_form_id], [rtc_location_menu], [rtc_report_name], [rtc_store_name], [rtc_design_filter], [rtc_design_sheets], [rtc_mailing], [rtc_status]) 
      VALUES (11002, 2, N'<LanguageResources><NLS09><Label>Meters collection to collection</Label></NLS09><NLS10><Label>Contadores recolección ha recolección</Label></NLS10></LanguageResources>', N'GetMetersCollectionToCollection_GR', N'<ReportToolDesignFilterDTO><FilterType>FromToDate</FilterType></ReportToolDesignFilterDTO>', 
      N'<ArrayOfReportToolDesignSheetsDTO><ReportToolDesignSheetsDTO><LanguageResources><NLS09><Label>Collection to collection</Label></NLS09><NLS10><Label>Recolección a recolección</Label></NLS10></LanguageResources><Columns><ReportToolDesignColumn><Code>Gaming Date</Code><Width>300</Width><EquityMatchType>Equality</EquityMatchType><LanguageResources><NLS09><Label>Gaming Date</Label></NLS09><NLS10><Label>Fecha de juego</Label></NLS10></LanguageResources></ReportToolDesignColumn><ReportToolDesignColumn><Code>Machine Number</Code><Width>300</Width><EquityMatchType>Equality</EquityMatchType><LanguageResources><NLS09><Label>Machine Number</Label></NLS09><NLS10><Label>Numero de maquina</Label></NLS10></LanguageResources></ReportToolDesignColumn><ReportToolDesignColumn><Code>Machine Denomination</Code><Width>300</Width><EquityMatchType>Equality</EquityMatchType><LanguageResources><NLS09><Label>Machine Denomination</Label></NLS09><NLS10><Label>Denominación de maquina</Label></NLS10></LanguageResources></ReportToolDesignColumn><ReportToolDesignColumn><Code>fisico</Code><Width>300</Width><EquityMatchType>Contains</EquityMatchType><LanguageResources><NLS09><Label>physical</Label></NLS09><NLS10><Label>fisico</Label></NLS10></LanguageResources></ReportToolDesignColumn><ReportToolDesignColumn><Code>Total contado en el soft count</Code><Width>300</Width><EquityMatchType>Equality</EquityMatchType><LanguageResources><NLS09><Label>Total count in soft count</Label></NLS09><NLS10><Label>Total contado en el soft count</Label></NLS10></LanguageResources></ReportToolDesignColumn></Columns></ReportToolDesignSheetsDTO></ArrayOfReportToolDesignSheetsDTO>', 1, 1)     
  END  
GO

GRANT EXECUTE ON GetCageMovementType_GR TO wggui WITH GRANT OPTION 
GO

GRANT EXECUTE ON GetMetersCollectionToCollection_GR TO wggui WITH GRANT OPTION
GO

/**************************************** 3GS Section ****************************************/

--
-- Load 3GS Assembly
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Trx_3GS_UpdateCardSession]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Trx_3GS_UpdateCardSession]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Trx_3GS_StartCardSession]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Trx_3GS_StartCardSession]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Trx_3GS_SendEvent]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Trx_3GS_SendEvent]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Trx_3GS_EndCardSession]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Trx_3GS_EndCardSession]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Trx_3GS_AccountStatus]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Trx_3GS_AccountStatus]
GO

IF  EXISTS (SELECT * FROM sys.assemblies asms WHERE asms.name = N'SQLBusinessLogic')
DROP ASSEMBLY [SQLBusinessLogic]
GO

CREATE ASSEMBLY [SQLBusinessLogic]
AUTHORIZATION [dbo]
FROM 0x4D5A90000300000004000000FFFF0000B800000000000000400000000000000000000000000000000000000000000000000000000000000000000000800000000E1FBA0E00B409CD21B8014CCD21546869732070726F6772616D2063616E6E6F742062652072756E20696E20444F53206D6F64652E0D0D0A240000000000000050450000648602002990CE570000000000000000F00022200B020B00005C040000040000000000000000000000200000000000800100000000200000000200000400000000000000040000000000000000A00400000200000000000003004085000040000000000000400000000000000000100000000000002000000000000000000000100000000000000000000000000000000000000000800400A0030000000000000000000000000000000000000000000000000000A87A04001C0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000002000004800000000000000000000002E74657874000000E05B040000200000005C040000020000000000000000000000000000200000602E72737263000000A00300000080040000040000005E0400000000000000000000000000400000402E72656C6F6300000000000000A004000000000000620400000000000000000000000000400000424800000002000500848F010024EB02000100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000013300700CF000000010000110073F20000060A166A05280E01000616731100000A1612020E0428CA000006130411042D1700061F0A087B9C0400046FEE00000600060D389100000002030412010E042833010006130411042D1300061F1472010000706FEE00000600060D2B6C0E0472030000706F1200000A00166A0708160E04289C0000060A067B2D04000416FE01130411042D0F000E0472030000706F1300000A0000087B8B040004087B90040004077BD60400048C270000017219000070077BC9040004281400000A190E0428D400000626060D2B00092A001B300700DC000000020000110073020100060A0E0772210000706F1200000A0000166A05280E01000616731100000A1612020E0728CA000006130411042D1700061F0A087B9C0400046FFE00000600060DDD8F000000087B940400040E072832010006130411042D1300061F1472010000706FFE00000600060DDE69087B9404000412010E072835010006130411042D1300061F1472010000706FFE00000600060DDE4107080E040E050E060E0728060000060A060DDE2D00067B5D0400042C0B067B5D0400041AFE012B011700130411042D0F000E0772210000706F1300000A000000DC00092A011000000200140098AC002D000000001B30070080010000030000110073020100060A0E0772390000706F1200000A0000166A05280E01000616731100000A1612020E0728CA000006130511052D1800061F0A087B9C0400046FFE00000600061304DD31010000087B940400040E072832010006130511052D1700061F1472010000706FFE00000600061304DD07010000087B9404000412010E072835010006130511052D1700061F1472010000706FFE00000600061304DDDB00000007080E040E050E060E0728060000060A067B5D0400042C0B067B5D0400041AFE012B011700130511052D0900061304DDA700000073F30000060D09087B8B0400047D31040004090E067D3504000409177D36040004090E057D37040004091C7D2F040004090E047D3204000409166A7D33040004077BC6040004077BC8040004077BC90400040912000E0728A8000006130511052D13000618724B0000706FFE00000600061304DE32061304DE2D00067B5D0400042C0B067B5D0400041AFE012B011700130511052D0F000E0772390000706F1300000A000000DC0011042A411C000002000000140000003B0100004F0100002D000000000000001B300700F4000000010000110073F20000060A0572850000706F1200000A0000166A02280E01000616731100000A1612020528CA000006130411042D1700061F0A087B9C0400046FEE00000600060DDDA9000000082C0F087B8B040004166AFE0116FE012B011600130411042D1300061F0A72010000706FEE00000600060DDE7C087B8C04000416FE01130411042D1300061F0B72010000706FEE00000600060DDE5A087B93040004166AFE01130411042D1300061F0C72010000706FEE00000600060DDE3706087B9A0400047D29040004066FF000000600060DDE2000067B2D04000416FE01130411042D0E000572850000706F1300000A000000DC00092A0110000002001300BED10020000000001B300700FF020000040000110073F20000060A0E0672AB0000706F1200000A000003040512010E062833010006130811082D1700061F1472010000706FEE00000600061307DDBD020000072C0E077BC604000416FE0116FE012B011600130811082D1700061F1472010000706FEE00000600061307DD8D0200000E0413091109196A301E1109166A326911096945040000003A0000002B000000300000003500000011091F126A301B11091F116A324311091F116A59694502000000270000002B00000011091F1D6A2E112B261F100D2B361F110D2B311F120D2B2C066FF000000600061307DD1C020000170D2B19180D2B15061772C90000706FEE00000600061307DDFF010000166A02280E01000616731100000A1612020E0628CA00000626166A1305082C0C087B93040004166AFE012B011700130811082D0A00087B93040004130500731500000A1304110472EB0000706F1600000A26110472450100706F1600000A261104729D0100706F1600000A26110472F10100706F1600000A26110472490200706F1600000A26110472A90200706F1600000A261104720B0300706F1600000A26110472610300706F1600000A26110472BD0300706F1600000A261104720D0400706F1600000A26110472610400706F1600000A26110472BD0400706F1600000A2611046F1700000A0E066F1800000A0E06731900000A13060011066F1A00000A720D0500701E6F1B00000A077BC60400048C2D0000016F1C00000A0011066F1A00000A72270500701E6F1B00000A11058C270000016F1C00000A0011066F1A00000A72470500701E6F1B00000A188C2D0000016F1C00000A0011066F1A00000A725F0500701E6F1B00000A098C2D0000016F1C00000A0011066F1A00000A727F0500701F096F1B00000A0E058C080000016F1C00000A0011066F1D00000A17FE01130811082D1300061772910500706FEE00000600061307DE4500DE14110614FE01130811082D0811066F1E00000A00DC00066FF000000600061307DE2100067B2D04000416FE01130811082D0F000E0672AB0000706F1300000A000000DC0011072A004134000002000000F4010000C5000000B902000014000000000000000200000014000000C6020000DA02000021000000000000001B30060002050000050000110073020100060A022C0E027BC604000416FE0116FE012B011600130A110A2D1700061F1472010000706FFE0000060006130938C7040000032C0F037B8B040004166AFE0116FE012B011600130A110A2D1700061F0A72010000706FFE000006000613093896040000037B93040004043314027BCE04000404330B037B9504000416FE012B011600130A110A2D3E00061F0D72CF0500700F02281F00000A037C93040004281F00000A037B950400048C110000026F1700000A282000000A6FFE000006000613093833040000037B94040004027BC6040004FE01130A110A2D3200061F14724F060070027CC6040004282100000A037C94040004282100000A282200000A6FFE0000060006130938ED030000027BCF040004037B8B040004FE01130A110A2D3200061F0A72AB060070037C8B040004281F00000A027CCF040004281F00000A282200000A6FFE0000060006130938A70300000E0416731100000A282300000A2D50057B4204000416731100000A282300000A2D3D057B41040004166A3233057B4404000416731100000A282300000A2D20057B43040004166A3216057B4604000416731100000A282300000A16FE012B011600130A110A2D6D00061F1572050700701B8D01000001130B110B160F04282400000AA2110B17057C42040004282400000AA2110B18057C41040004281F00000AA2110B19057C44040004282400000AA2110B1A057C43040004281F00000AA2110B282500000A6FFE0000060006130938D302000016731100000A0E04282600000A0D16731100000A037B97040004057B42040004282700000A057B44040004282800000A057B46040004282800000A282600000A0C0E0408282700000A1305110516731100000A282900000A1306161307110616FE01130A110A2D5B007283070070729D0700700E0528EC0000061204282A00000A2611041F64731100000A282B00000A13041105282C00000A1104282D00000A16FE01130A110A2D190017130716731100000A080E04282E00000A282600000A0D000019040512010E0528A7000006130A110A2D1600061872CF0700706FFE0000060006130938E801000004090E0411061C0E0528E4000006130A110A2D16000618721D0800706FFE0000060006130938BE0100000E05283B01000613080011086F1A00000A720D0500701E6F1B00000A027BC60400048C2D0000016F1C00000A0011086F1A00000A726B0800701F0C1F326F2F00000A72890800706F1C00000A0011086F1A00000A729B0800701F096F1B00000A237B14AE47E17A843F8C310000016F1C00000A0011086F1A00000A72B9080070166F1B00000A168C2D0000016F1C00000A0011086F1A00000A72D9080070166F1B00000A077B410400048C270000016F1C00000A0011086F1A00000A72FF0800701F096F1B00000A077B420400048C080000016F1C00000A0011086F1A00000A7227090070166F1B00000A077B430400048C270000016F1C00000A0011086F1A00000A72470900701F096F1B00000A077B440400048C080000016F1C00000A0011086F1A00000A72690900701F096F1B00000A168C2D0000016F1C00000A0011086F1D00000A17FE01130A110A2D1300061872930900706FFE00000600061309DE5C00DE14110814FE01130A110A2D0811086F1E00000A00DC00110716FE01130A110A2D2D00061A72C50900700F04282400000A1202282400000A1203282400000A282000000A6FFE000006000613092B0C066F00010006000613092B000011092A0000411C000002000000490300005C010000A50400001400000000000000133002008A000000060000110005026FF100000651027B2D0400040A064502000000280000005F000000061F0A594503000000260000004A00000032000000061F145945020000002D0000002D0000002B370316540472390A007051057201000070512B300317540472490A0070512B240318540472770A0070512B1803195404729D0A0070512B0C031A540472D50A0070512B002A000013300200B1000000070000110005026F0101000651027B5D0400040A06450E000000130000004A0000004A0000004A000000560000004A0000004A0000004A0000004A0000004A000000260000004A0000004A0000003E000000061F1459450200000021000000210000002B370316540472390A007051057201000070512B3C0317540472490A0070512B3003185404729D0A0070512B240319540472F10A0070512B18031A540472D50A0070512B0C031B540472250B0070512B002A000000133008006E000000080000110003730F0000060A1F310B06176A20930300006A071F65731100000A16731100000A16731100000A1F65731100000A6F130000062606186A20930300006A0720CA000000731100000A20CA000000731100000A16731100000A16731100000A6F130000062606046F1D000006262A00001330080076000000080000110003040573110000060A1B0B06196A20930300006A07202F010000731100000A16731100000A16731100000A202F010000731100000A6F1300000626061A6A20930300006A072094010000731100000A2094010000731100000A16731100000A16731100000A6F1300000626060E046F1D000006262A1E02283000000A2A0000133002001A0000000900001100027B05000004166AFE010A062D03002B0702037D050000042A0000133002002E000000090000110004283100000A16FE010A062D0F000272010000707D06000004002B100002047D0600000402037D07000004002A0000133005005B0200000A0000110002733200000A7D01000004027B010000046F3300000A72770B007072950B0070283400000A6F3500000A0A06176F3600000A0006156A6F3700000A0006156A6F3800000A00027B010000046F3300000A72AF0B007072950B0070283400000A6F3500000A26027B010000046F3300000A72CF0B007072950B0070283400000A6F3500000A166F3900000A00027B010000046F3300000A72EB0B007072FB0B0070283400000A6F3500000A166F3900000A00027B010000046F3300000A72150C0070723B0C0070283400000A6F3500000A166F3900000A00027B010000046F3300000A72590C0070723B0C0070283400000A6F3500000A166F3900000A00027B010000046F3300000A72750C0070723B0C0070283400000A6F3500000A166F3900000A00027B010000046F3300000A72910C0070723B0C0070283400000A6F3500000A166F3900000A00027B010000046F3300000A72B30C007072C90C0070283400000A6F3500000A176F3900000A00027B010000046F3300000A72E50C007072C90C0070283400000A6F3500000A176F3900000A00027B010000046F3300000A72FB0C007072C90C0070283400000A6F3500000A176F3900000A00027B010000046F3300000A72170D007072FB0B0070283400000A6F3500000A176F3900000A00027B010000046F3300000A72350D007072C90C0070283400000A6F3500000A176F3900000A00027B010000046F3300000A72570D007072C90C0070283400000A6F3500000A176F3900000A00027B01000004178D340000010B0716027B010000046F3300000A72770B00706F3A00000AA2076F3B00000A002A0003300200840000000000000002147D0100000402147D0200000402167D030000040272010000707D0400000402166A7D0500000402147D0600000402166A7D070000040272010000707D0800000402283000000A000002280E0000060002036F200000067D0200000402167D030000040272010000707D0400000402166A7D050000040272010000707D08000004002A133004008D000000090000110203280F000006000005283100000A0A062D0E00027B02000004057D0C00000400027B020000047B1200000418FE010A062D580002047D0300000402027B020000047B0C0000047D04000004027B020000047B0F000004283100000A0A062D230002027B020000047B0F000004727D0D0070027B04000004283C00000A7D040000040002147D0200000400002A4A02030405720100007028120000060000002A03300200770000000000000002147D0100000402147D0200000402167D030000040272010000707D0400000402166A7D0500000402147D0600000402166A7D070000040272010000707D0800000402283000000A000002280E0000060002147D0200000402037D0300000402047D0400000402057D05000004020E047D08000004002A0013300B00220000000900001100020304050E040E050E060E07720100007015720100007028160000060A2B00062A000013300D00290000000900001100020304050E040E050E060E07720100007015720100007072010000700E0828180000060A2B00062A00000013300B001F0000000900001100020304050E040E050E060E070E0815720100007028160000060A2B00062A0013300D002A0000000900001100020304050E040E050E060E070E081572010000707201000070027B0500000428180000060A2B00062A000013300D00250000000900001100020304050E040E050E060E070E080E090E0A0E0B027B0500000428180000060A2B00062A0000001B300300B90100000B0000110000027B010000046F3D00000A0A0672AF0B0070038C270000016F3E00000A000672CF0B0070048C270000016F3E00000A000672EB0B0070058C0D0000026F3E00000A000672150C00700E048C080000016F3E00000A000672590C00700E058C080000016F3E00000A000672750C00700E068C080000016F3E00000A000672910C00700E078C080000016F3E00000A000E08283100000A16FE010C082D15000672B30C00707E3F00000A6F3E00000A00002B10000672B30C00700E086F3E00000A00000E0B283100000A16FE010C082D15000672E50C00707E3F00000A6F3E00000A00002B10000672E50C00700E0B6F3E00000A00000E0915FE010C082D17000672170D00700E098C2D0000016F3E00000A00002B13000672170D00707E3F00000A6F3E00000A00000E0A283100000A16FE010C082D15000672350D00707E3F00000A6F3E00000A00002B10000672350D00700E0A6F3E00000A00000E0C166AFE010C082D17000672570D00700E0C8C270000016F3E00000A00002B19000672570D0070027B050000048C270000016F3E00000A0000027B010000046F4000000A066F4100000A00170BDE0A260000DE0000160B2B0000072A000000411C00000000000001000000AB010000AC01000005000000010000011B300300860000000C0000110072010000700C0072810D00700A06036F1800000A03731900000A0D00096F1A00000A72420E0070166F1B00000A028C270000016F1C00000A00096F4200000A0B00DE120914FE01130511052D07096F1E00000A00DC00072C0A077E3F00000AFE012B011700130511052D09000774280000010C0000DE05260000DE00000813042B0011042A0000011C000002001C00284400120000000000000700717800053700000113300300560000000D00001100027B010000046F4000000A6F4300000A16FE0216FE010B072D34027B010000046F4000000A027B010000046F4000000A6F4300000A17596F4400000A72770B00706F4500000AA5270000010A2B05156A0A2B00062A3A00027B010000046F4600000A002A00000013300200610000000D00001100027B010000046F4000000A6F4300000A16311C0316321803027B010000046F4000000A6F4300000AFE0416FE012B0117000B072D2400027B010000046F4000000A036F4400000A72770B00706F4500000AA5270000010A2B05156A0A2B00062A0000001B300400F50800000E00001100725A0E0070726E0E00700328EC0000061203284700000A260917FE010C00027B010000046F4000000A6F4300000A16FE0116FE01130B110B2D090017130ADDAD08000002281A000006166AFE0216FE01130B110B2D090017130ADD91080000027B0600000414FE0116FE01130B110B2D3000027B010000046F4000000A166F4400000A72CF0B00706F4500000AA527000001130511050328190000061306002B1200027B070000041305027B0600000413060000027B010000046F4000000A6F4800000A130C2B3F110C6F4900000A740B0000011307001105110772CF0B00706F4500000AA527000001FE0116FE01130B110B2D1100110772FB0C007011066F3E00000A000000110C6F4A00000A130B110B2DB4DE1D110C7505000001130D110D14FE01130B110B2D08110D6F1E00000A00DC00731500000A0A0672800E00706F1600000A260672D00E00706F1600000A2606720A0F00706F1600000A2606723A0F00706F1600000A2606721D1000706F1600000A260672491000706F1600000A260816FE01130B110B2D62000672931000706F1600000A260672E11000706F1600000A260672E51000706F1600000A2606720D1100706F1600000A260672691100706F1600000A260672E11000706F1600000A260672B51100706F1600000A260672E11000706F1600000A260006726A1200706F1600000A260672CC1200706F1600000A2606722E1300706F1600000A260672901300706F1600000A260672F21300706F1600000A260672541400706F1600000A260672B61400706F1600000A260672181500706F1600000A2606727A1500706F1600000A260672DC1500706F1600000A2606723E1600706F1600000A260672A01600706F1600000A260672021700706F1600000A260672641700706F1600000A260672C61700706F1600000A260672281800706F1600000A2606728A1800706F1600000A260816FE01130B110B2D0E000672EC1800706F1600000A260006724A1900706F1600000A260672501900706F1600000A260672B21900706F1600000A260672141A00706F1600000A260672761A00706F1600000A260672D81A00706F1600000A2606723A1B00706F1600000A2606729C1B00706F1600000A260672FE1B00706F1600000A260672601C00706F1600000A260672C21C00706F1600000A260672241D00706F1600000A260672DB1D00706F1600000A2606723D1E00706F1600000A2606729F1E00706F1600000A260672011F00706F1600000A260672631F00706F1600000A260816FE01130B110B2D0E000672C51F00706F1600000A260006724A1900706F1600000A260816FE01130B110B2D10000672232000706F1600000A26002B0E0006726F2000706F1600000A2600066F1700000A036F1800000A03731900000A13080011086F1A00000A72B9200070166F1B00000A72AF0B00706F4B00000A0011086F1A00000A72420E0070166F1B00000A72CF0B00706F4B00000A0011086F1A00000A72D52000701E6F1B00000A72EB0B00706F4B00000A0011086F1A00000A72E32000701F096F1B00000A72150C00706F4B00000A0011086F1A00000A72052100701F096F1B00000A72590C00706F4B00000A0011086F1A00000A721D2100701F096F1B00000A72750C00706F4B00000A0011086F1A00000A72352100701F096F1B00000A72910C00706F4B00000A0011086F1A00000A72532100701F0C20000100006F2F00000A72B30C00706F4B00000A0011086F1A00000A72672100701F0C1F406F2F00000A72E50C00706F4B00000A0011086F1A00000A727B2100701F0C1F326F2F00000A72FB0C00706F4B00000A00027B0200000414FE01130B110B3A8201000000027B020000047B0A00000416FE0216FE01130B110B2D2C0011086F1A00000A72912100701E6F1B00000A027B020000047B0A0000048C2D0000016F1C00000A00002B1F0011086F1A00000A72912100701E6F1B00000A7E3F00000A6F1C00000A0000027B020000047B0C000004283100000A130B110B3A8400000000027B020000047B0F000004283100000A16FE01130B110B2D2A0011086F1A00000A72A92100701F0C1F326F2F00000A027B020000047B0C0000046F1C00000A00002B3D0011086F1A00000A72A92100701F0C1F326F2F00000A027B020000047B0F000004727D0D0070027B020000047B0C000004283C00000A6F1C00000A0000002B1F0011086F1A00000A72A92100701E6F1B00000A7E3F00000A6F1C00000A000011086F1A00000A720D0500701E6F1B00000A72170D00706F4B00000A0011086F1A00000A72C52100701F0C1F326F2F00000A72350D00706F4B00000A0011086F1A00000A72270500701F0C1F326F2F00000A72570D00706F4B00000A0000384D0100000011086F1A00000A72912100701E6F1B00000A7E3F00000A6F1C00000A00027B08000004283100000A16FE01130B110B2D240011086F1A00000A72A92100701F0C1F326F2F00000A7E3F00000A6F1C00000A00002B230011086F1A00000A72A92100701F0C1F326F2F00000A027B080000046F1C00000A0000027B0300000416FE0116FE01130B110B2D210011086F1A00000A720D0500701E6F1B00000A7E3F00000A6F1C00000A00002B250011086F1A00000A720D0500701E6F1B00000A027B030000048C2D0000016F1C00000A0000027B04000004283100000A16FE01130B110B2D240011086F1A00000A72C52100701F0C1F326F2F00000A7E3F00000A6F1C00000A00002B230011086F1A00000A72C52100701F0C1F326F2F00000A027B040000046F1C00000A000011086F1A00000A72270500701F0C1F326F2F00000A7E3F00000A6F1C00000A00000816FE01130B110B2D210011086F1A00000A72E32100701E6F1B00000A1F0C8C2D0000016F1C00000A000011086F1A00000A72F7210070166F1B00000A1304110472770B00706F4B00000A001104186F4C00000A001108176F4D00000A00734E00000A130900110911086F4F00000A001109027B010000046F5000000A0B07027B010000046F4000000A6F4300000AFE0116FE01130B110B2D060017130ADE3E00DE14110914FE01130B110B2D0811096F1E00000A00DC0000DE14110814FE01130B110B2D0811086F1E00000A00DC0000DE05260000DE000016130A2B0000110A2A0000004164000002000000C700000050000000170100001D0000000000000002000000780800003E000000B6080000140000000000000002000000FF030000CF040000CE0800001400000000000000000000001E000000C8080000E6080000050000000100000113300600380000000F0000110020842900006A0A1F32731100000A0B20030900006A0C0373250000060D09061F2B070872010000706F270000062609046F3F000006262A1E02283000000A2A1330010011000000100000110002285100000A74060000020A2B00062A1E02283000000A2A000000133001000C0000001000001100027B150000040A2B00062A260002037D130000042A0000033003000F030000000000000002733200000A7D13000004027B130000046F3300000A721122007072950B0070283400000A6F3500000A26027B130000046F3300000A723122007072950B0070283400000A6F3500000A26027B130000046F3300000A724D22007072C90C0070283400000A6F3500000A26027B130000046F3300000A727322007072FB0B0070283400000A6F3500000A166F3900000A00027B130000046F3300000A7283220070723B0C0070283400000A6F3500000A166F3900000A00027B130000046F3300000A72A9220070723B0C0070283400000A6F3500000A166F3900000A00027B130000046F3300000A72C5220070723B0C0070283400000A6F3500000A166F3900000A00027B130000046F3300000A72E1220070723B0C0070283400000A6F3500000A166F3900000A00027B130000046F3300000A7203230070723B0C0070283400000A6F3500000A166F3900000A00027B130000046F3300000A722D23007072C90C0070283400000A6F3500000A176F3900000A00027B130000046F3300000A724323007072C90C0070283400000A6F3500000A176F3900000A00027B130000046F3300000A726D230070723B0C0070283400000A6F3500000A176F3900000A00027B130000046F3300000A726D2300706F3A00000A168C2D0000016F5200000A00027B130000046F3300000A7289230070723B0C0070283400000A6F3500000A176F3900000A00027B130000046F3300000A72892300706F3A00000A168C2D0000016F5200000A00027B130000046F3300000A72BB23007072950B0070283400000A6F3500000A176F3900000A00027B130000046F3300000A72F123007072950B0070283400000A6F3500000A176F3900000A00027B130000046F3300000A720724007072FB0B0070283400000A6F3500000A176F3900000A00027B130000046F3300000A723324007072FB0B0070283400000A6F3500000A176F3900000A00027B130000046F3300000A724F24007072950B0070283400000A6F3500000A176F3900000A00027B130000046F3300000A726B240070727B240070283400000A6F3500000A176F3900000A002ACA02147D1300000402147D1400000402147D1500000402283000000A00000228240000060002036F200000067D15000004002A000013300200230000000900001102032825000006000004283100000A0A062D0E00027B15000004047D0C00000400002A0013300700180000000900001100020304050E040E05720100007028280000060A2B00062A133008001A0000000900001100020304050E040E050E067201000070282A0000060A2B00062A0000133009001C0000000900001100020304050E040E050E0672010000700E07282B0000060A2B00062A133009001D0000000900001100020304050E040E050E060E0716731100000A282D0000060A2B00062A00000013300A001F0000000900001100020304050E040E050E060E0716731100000A0E08282C0000060A2B00062A0013300C001F0000000900001100020304050E040E050E060E070E08166A166A0E09282F0000060A2B00062A0013300B001D0000000900001100020304050E040E050E060E070E08166A166A282E0000060A2B00062A00000013300C00230000000900001100020304050E040E050E060E070E080E090E0A15731100000A28300000060A2B00062A0013300D00250000000900001100020304050E040E050E060E070E080E090E0A15731100000A0E0B28310000060A2B00062A00000013301000350000001100001100020304050E040E050E060E070E080E090E0A0E0B16735300000A16735400000A1201FE150300001B071628320000060A2B00062A00000013301000360000001100001100020304050E040E050E060E070E080E090E0A0E0B16735300000A16735400000A1201FE150300001B070E0C28320000060A2B00062A00001B30060066030000120000110000020E097D160000040405120112020F050E0F2840000006000407080228220000067B120000041CFE0128410000060A02027B130000046F3D00000A7D14000004027B140000047211220070038C270000016F3E00000A00027B140000047273220070048C0C0000026F3E00000A00027B140000047283220070168C2D0000016F3E00000A00027B1400000472A9220070078C080000016F3E00000A00027B1400000472C5220070088C080000016F3E00000A00027B1400000472E1220070168C2D0000016F3E00000A00027B140000047203230070068C080000016F3E00000A00027B1400000472892300700E088C080000016F3E00000A00027B1400000472BB2300700E0A8C270000016F3E00000A000E06283100000A16FE01130411042D1A00027B14000004722D2300707E3F00000A6F3E00000A00002B1500027B14000004722D2300700E066F3E00000A00000E05283100000A130411042D1700027B14000004724D2200700E056F3E00000A00002B1800027B14000004724D2200707E3F00000A6F3E00000A00000E04166AFE01130411042D1C00027B1400000472312200700E048C270000016F3E00000A00002B1800027B1400000472312200707E3F00000A6F3E00000A00000E077201000070285500000A16FE01130411042D1700027B1400000472432300700E076F3E00000A00002B1800027B1400000472432300707E3F00000A6F3E00000A00000E0B16731100000A285600000A16FE01130411042D1C00027B14000004726D2300700E0B8C080000016F3E00000A00002B1800027B14000004726D2300707E3F00000A6F3E00000A00000F0D285700000A16FE01130411042D1C00027B1400000472072400700E0D8C0200001B6F3E00000A00002B1800027B1400000472072400707E3F00000A6F3E00000A00000F0E285800000A16FE01130411042D1C00027B1400000472F12300700E0E8C0300001B6F3E00000A00002B1800027B1400000472F12300707E3F00000A6F3E00000A00000F0C285900000A16FE01130411042D1C00027B1400000472332400700E0C8C0100001B6F3E00000A00002B1800027B1400000472332400707E3F00000A6F3E00000A0000027B14000004726B2400707E3F00000A6F3E00000A00027B130000046F4000000A027B140000046F4100000A00170DDE0A260000DE0000160D2B0000092A0000411C000000000000010000005803000059030000050000000100000113300600170000000900001100020304050E0416731100000A28340000060A2B00062A00133007002F0100001300001100160C0E046F610000061304110445030000009200000005000000B300000038CF0000000E046F63000006130511051959450200000002000000290000002B4E08130611062D100020930000000A20950000000B002B0E0020050200000A20060200000B002B4808130611062D100020940000000A20960000000B002B0E0020070200000A20080200000B002B2108130611062D0A001F4F0A1F580B002B0E0020030200000A20040200000B002B002B4608130611062D0A001F4E0A1F570B002B0E00200A0200000A200B0200000B002B2508130611062D0A001F5C0A1F5D0B002B0E00200C0200000A200D0200000B002B04160D2B36020304050E04060E052837000006130611062D0500160D2B1D020304050E04070E052837000006130611062D0500160D2B04170D2B00092A00133007009D00000014000011000E046F6100000616FE010C082D07160B38850000000E046F460000060E05285A00000A2C130E046F480000060E05285500000A16FE012B0117000C082D08204D0200000A2B350E046F460000060E05285500000A2C130E046F480000060E05285A00000A16FE012B0117000C082D08204B0200000A2B04160B2B1F020304050E040616731100000A28370000060C082D0500160B2B04170B2B00072A00000013300700FF0000001500001100160B0E046F610000060D0945030000000500000023000000A000000038B600000007130411042D0A0020970000000A002B0800200E0200000A00389C0000000E046F63000006130511054505000000020000005300000002000000380000001D0000002B5107130411042D0A0020980000000A002B0800200F0200000A002B3A07130411042D0A00209A0000000A002B080020110200000A002B1F07130411042D0A0020990000000A002B080020100200000A002B04160C2B422B1F07130411042D0A00209B0000000A002B080020120200000A002B04160C2B21020304050E040616731100000A2837000006130411042D0500160C2B04170C2B00082A0013300300CD040000160000110002027B130000046F3D00000A7D14000004027B140000047211220070038C270000016F3E00000A00027B1400000472732200700E058C0C0000026F3E00000A0004166AFE010C082D1B00027B140000047231220070048C270000016F3E00000A00002B1800027B1400000472312200707E3F00000A6F3E00000A000005283100000A0C082D1600027B14000004724D220070056F3E00000A00002B1800027B14000004724D2200707E3F00000A6F3E00000A0000027B14000004722D2300707E3F00000A6F3E00000A00027B140000047203230070168C2D0000016F3E00000A00027B1400000472832200700E046F420000068C080000016F3E00000A00027B1400000472A92200700E046F500000068C080000016F3E00000A00027B1400000472432300700E046F460000066F3E00000A000E051F4F3BB90000000E0520930000003BAD0000000E0520940000003BA10000000E051F4E3B980000000E051F5C3B8F0000000E0520970000003B830000000E0520980000002E7A0E0520990000002E710E05209A0000002E680E05209B0000002E5F0E0520030200002E560E0520050200002E4D0E0520070200002E440E05200A0200002E3B0E05200C0200002E320E05200E0200002E290E05200F0200002E200E0520100200002E170E0520110200002E0E0E052012020000FE0116FE012B0116000C083AE200000000027B1400000472C52200700E046F520000068C080000016F3E00000A00027B1400000472E12200700E046F550000068C080000016F3E00000A00027B14000004726D2300700E046F590000068C080000016F3E00000A000E0520970000002E560E0520980000002E4D0E0520990000002E440E05209A0000002E3B0E05209B0000002E320E05200E0200002E290E05200F0200002E200E0520100200002E170E0520110200002E0E0E052012020000FE0116FE012B0116000C082D1F00027B1400000472E12200700E046F540000068C080000016F3E00000A00000038830100000E05204B020000FE0116FE010C082D5E00027B1400000472C52200700E046F520000068C080000016F3E00000A00027B1400000472E12200700E046F540000068C080000016F3E00000A00027B14000004726D2300700E046F590000068C080000016F3E00000A000038150100000E05204D020000FE0116FE010C083AB5000000000E046F420000060E046F67000006282700000A0A027B1400000472832200700E046F540000068C080000016F3E00000A00027B1400000472C52200700E046F520000068C080000016F3E00000A00027B1400000472A9220070068C080000016F3E00000A00027B1400000472E1220070068C080000016F3E00000A00027B14000004726D2300700E046F590000068C080000016F3E00000A00027B1400000472432300700E046F480000066F3E00000A00002B4D00027B1400000472C5220070168C2D0000016F3E00000A00027B1400000472E12200700E046F570000068C080000016F3E00000A00027B14000004726D230070168C2D0000016F3E00000A0000027B1400000472892300700E068C080000016F3E00000A00027B1400000472BB230070168C2D0000016F3E00000A00027B1400000472332400700E046F610000068C240000026F3E00000A00027B130000046F4000000A027B140000046F4100000A00170B2B00072A00000013300300280000000900001100027B1400000414FE010A062D1900027B14000004724F240070038C270000016F3E00000A00002A13300300280000000900001100027B1400000414FE010A062D1900027B1400000472C5220070038C080000016F3E00000A00002A13300300280000000900001100027B1400000414FE010A062D1900027B1400000472A9220070038C080000016F3E00000A00002A13300300280000000900001100027B1400000414FE010A062D1900027B140000047283220070038C080000016F3E00000A00002A13300300280000000900001100027B1400000414FE010A062D1900027B14000004726B240070038C0A0000016F3E00000A00002A133003003F0000000900001100027B1400000414FE010A062D3000027B140000047283220070038C080000016F3E00000A00027B1400000472E1220070048C080000016F3E00000A00002A0013300200260000001700001100150A027B1400000414FE010C082D1000027B140000046F5B00000A8E690A00060B2B00072A00001B3004008D280000180000110000027B130000046F4000000A6F4300000A16FE0116FE01131511152D0900171314DD62280000166A130A7201000070130B00027B130000046F4000000A6F4800000A131638B000000011166F4900000A740B000001130E00110E72312200706F5C00000A16FE01131511152D06003886000000110E724D2200706F5C00000A131511152D03002B71110A166AFE0116FE01131511152D3000110E72312200706F4500000AA527000001130A110A032819000006130B110E724D220070110B6F3E00000A00002B3100110A110E72312200706F4500000AA527000001FE0116FE01131511152D1100110E724D220070110B6F3E00000A0000000011166F4A00000A131511153A40FFFFFFDE1D111675050000011317111714FE01131511152D0811176F1E00000A00DC00731500000A0A06729B2400706F1600000A260672D52400706F1600000A260672E52400706F1600000A260672232500706F1600000A260672A32500706F1600000A260672E32500706F1600000A260672272600706F1600000A2606727F2600706F1600000A260672D92600706F1600000A260672412700706F1600000A260672A52700706F1600000A260672B92700706F1600000A260672052800706F1600000A260672172800706F1600000A2606724F2800706F1600000A260672BB2800706F1600000A260672272900706F1600000A2606728F2900706F1600000A260672F32900706F1600000A2606724F2A00706F1600000A260672EC2A00706F1600000A260672362B00706F1600000A260672822B00706F1600000A260672EE2B00706F1600000A260672562C00706F1600000A260672B82C00706F1600000A260672EC2C00706F1600000A260672482D00706F1600000A260672AA2D00706F1600000A260672282E00706F1600000A260672A82E00706F1600000A260672372F00706F1600000A260672C22F00706F1600000A260672FA2F00706F1600000A2606720A3000706F1600000A2606726C3000706F1600000A260672B23000706F1600000A260672F43000706F1600000A2606722E3100706F1600000A2606726E3100706F1600000A260672B43100706F1600000A260672DC3100706F1600000A2606720C3200706F1600000A2606725E3200706F1600000A260672883200706F1600000A260672C83200706F1600000A260672043300706F1600000A2606723A3300706F1600000A260672983300706F1600000A260672B43100706F1600000A260672C83300706F1600000A26066F1700000A036F1800000A03731900000A130F00110F6F1A00000A72D8330070166F1B00000A027B150000047B090000048C270000016F1C00000A00110F6F1A00000A72F03300701E6F1B00000A168C220000026F1C00000A00110F6F1A00000A720A3400701E6F1B00000A188C220000026F1C00000A00110F6F1A00000A72323400701E6F1B00000A198C220000026F1C00000A00110F6F1A00000A72523400701F166F1B00000A0C110F6F1A00000A72D52000701E6F1B00000A0D110F6F1A00000A72663400701F096F1B00000A1304110F6F1A00000A728C3400701F096F1B00000A1305110F6F1A00000A72C2340070186F1B00000A1306110F6F1A00000A72E8340070186F1B00000A1307722235007072423500700328EC00000613087262350070727C3500700328EC000006130C110C7296350070285A00000A130D00027B130000046F4000000A6F4800000A131638361B000011166F4900000A740B000001130E0008110E72432300706F4500000A6F1C00000A0009110E72332400706F4500000A6F1C00000A001106178C2D0000016F1C00000A001107168C2D0000016F1C00000A001105168C2D0000016F1C00000A00110E72732200706F4500000AA50C000002130911091318111820BF0000003DFE00000011181F58305B11184504000000FF0500008E0600008E0600008E06000011181F0D3B520D000011181F4E59450B0000003F0300003F0300008F1600008F1600008F1600008F1600008F16000017030000170300003F0300003F030000388A16000011181F5C5945080000001003000010030000601600006016000060160000E3070000260800003F0500001118208F0000005945130000007C020000071600000716000007160000B7020000B7020000B7020000B7020000590A0000590A0000590A0000590A0000590A0000A60A00000716000007160000071600006306000063060000111820BE000000594502000000B2050000CF09000038ED150000111820360100003057111820C90000003BEA060000111820CC0000005945030000004A0E0000BF1500004A0E00001118202F0100005945080000009B0700008F080000DA130000B11400009215000092150000F7070000DC080000388D150000111820F40100005945760000006803000085070000A8130000A8130000A8130000A8130000A8130000A8130000A8130000A8130000A8130000A8130000A8130000A8130000A8130000580000005800000058000000580000005800000058000000A813000058000000580000005800000058000000A8130000FA070000FA070000FA070000FA070000A8130000760A0000760A0000760A0000760A0000760A0000760A0000760A0000760A0000760A0000A8130000A8130000A8130000760A0000760A0000760A0000760A0000760A0000760A0000760A0000760A0000760A000068030000680300008B0D0000A8130000C10C0000A8130000A8130000C10C0000A8130000A8130000A8130000330C0000330C0000A8130000A8130000A8130000A8130000A8130000A8130000840B0000580000005800000058000000A8130000A8130000A8130000A8130000A8130000A8130000A8130000A8130000A8130000A8130000A8130000F80F00005A0E00007E1000002E0F0000A813000009110000760A0000760A0000760A0000760A0000DD110000DD110000A8130000A8130000A8130000A8130000A8130000A8130000A8130000A8130000A8130000A8130000A8130000A81300000902000009020000480200004802000048020000480200004802000011182040420F003B7108000011182080841E003B3C090000388B1300001104168C2D0000016F1C00000A00384C1400001104110E72A92200706F4500000AA508000001285D00000A8C080000016F1C00000A0038241400001104110E72E12200706F4500000AA508000001285D00000A8C080000016F1C00000A0011091F4E2E6B11091F4F2E65110920930000002E5C110920940000002E5311091F5C2E4D110920030200002E44110920050200002E3B110920070200002E321109200A0200002E291109200C0200002E201109203F0200002E171109204B0200002E0E1109204D020000FE0116FE012B011600131511152D16001105110E72832200706F4500000A6F1C00000A00001107178C2D0000016F1C00000A00110E72432300706F5C00000A2D1D110E72432300706F4500000A74280000011108285A00000A16FE012B011700131511153AB6000000001109131811181F62303711181F4F3B8300000011181F582E7D11181F5C594507000000660000006600000075000000750000007500000057000000660000002B7311182093000000594504000000380000003800000038000000380000001118200302000059450500000017000000350000001700000035000000170000001118200C02000059450200000011000000110000002B1E09178C2D0000016F1C00000A002B0F09188C2D0000016F1C00000A002B000038731200001107168C2D0000016F1C00000A001106178C2D0000016F1C00000A001104110E72C52200706F4500000AA5080000018C080000016F1C00000A0038341200001107168C2D0000016F1C00000A001106178C2D0000016F1C00000A001104110E72A92200706F4500000AA5080000018C080000016F1C00000A0038F51100001107178C2D0000016F1C00000A001106168C2D0000016F1C00000A001104168C2D0000016F1C00000A001105110E72C52200706F4500000AA508000001285D00000A8C080000016F1C00000A0038A31100001104110E72032300706F4500000A6F1C00000A00110E72432300706F5C00000A2D20110E72432300706F4500000A74280000017201000070285500000A16FE012B011700131511152D40001104168C2D0000016F1C00000A001105110E72032300706F4500000A6F1C00000A001107178C2D0000016F1C00000A001106168C2D0000016F1C00000A000038141100001105110E72032300706F4500000A6F1C00000A001104110E72032300706F4500000A6F1C00000A00110E72432300706F5C00000A2D1D110E72432300706F4500000A74280000017201000070285500000A2D10096F5E00000AA52400000216FE012B011600131511152D2C001104168C2D0000016F1C00000A001107178C2D0000016F1C00000A001106168C2D0000016F1C00000A000038781000001105110E72032300706F4500000A6F1C00000A001104110E72032300706F4500000A6F1C00000A00110E72432300706F5C00000A2D47110E72432300706F4500000A74280000017201000070285500000A2C2A110E72432300706F4500000A74280000011108285A00000A2C10096F5E00000AA52400000216FE012B011700131511152D2C001104168C2D0000016F1C00000A001107178C2D0000016F1C00000A001106168C2D0000016F1C00000A000038C20F0000110E7283220070110E72A92200706F4500000AA5080000018C080000016F3E00000A00110E72E1220070110E72A92200706F4500000AA5080000018C080000016F3E00000A00110E7203230070168C2D0000016F3E00000A00110E72A9220070168C2D0000016F3E00000A0038510F00001105110E72032300706F4500000A6F1C00000A001104168C2D0000016F1C00000A001107178C2D0000016F1C00000A001106168C2D0000016F1C00000A00380E0F00001105110E72032300706F4500000A6F1C00000A001104168C2D0000016F1C00000A001107178C2D0000016F1C00000A001106168C2D0000016F1C00000A0038CB0E00001107178C2D0000016F1C00000A001106178C2D0000016F1C00000A001104168C2D0000016F1C00000A00110D131511152D25001105110E72C52200706F4500000AA508000001285D00000A8C080000016F1C00000A0000386F0E0000110E72432300706F5C00000A2D1A110E72432300706F4500000A74280000011108285500000A2D10096F5E00000AA52400000216FE012B011600131511152D51001106168C2D0000016F1C00000A001107178C2D0000016F1C00000A001104168C2D0000016F1C00000A001105110E72832200706F4500000AA508000001285D00000A8C080000016F1C00000A00002B02000038D70D00001107178C2D0000016F1C00000A001106178C2D0000016F1C00000A001104168C2D0000016F1C00000A001105110E72A92200706F4500000AA5080000018C080000016F1C00000A00388A0D0000110E72432300706F5C00000A2D1A110E72432300706F4500000A74280000011108285500000A2D10096F5E00000AA52400000216FE012B011600131511152D4C001106168C2D0000016F1C00000A001107178C2D0000016F1C00000A001104168C2D0000016F1C00000A001105110E72832200706F4500000AA5080000018C080000016F1C00000A00002B02000038F70C00001105168C2D0000016F1C00000A001104168C2D0000016F1C00000A00110E72432300706F5C00000A2D20110E72432300706F4500000A74280000017201000070285500000A16FE012B011700131511152D1E001107178C2D0000016F1C00000A001106168C2D0000016F1C00000A000038820C00001106168C2D0000016F1C00000A001104168C2D0000016F1C00000A001107178C2D0000016F1C00000A001105110E72832200706F4500000AA5080000018C080000016F1C00000A0038350C00001106178C2D0000016F1C00000A001104110E72C52200706F4500000AA508000001285D00000A8C080000016F1C00000A0038FF0B00001106178C2D0000016F1C00000A001107168C2D0000016F1C00000A001104110E72A92200706F4500000AA5080000018C080000016F1C00000A001105168C2D0000016F1C00000A00110E72432300706F5C00000A2D1A110E72432300706F4500000A74280000011108285500000A2D10096F5E00000AA52400000216FE012B011600131511152D4A001106168C2D0000016F1C00000A001107178C2D0000016F1C00000A001104168C2D0000016F1C00000A001105110E72A92200706F4500000AA5080000018C080000016F1C00000A000038280B00001106178C2D0000016F1C00000A001107168C2D0000016F1C00000A001104110E72C52200706F4500000AA508000001285D00000A8C080000016F1C00000A001105168C2D0000016F1C00000A00110E72432300706F5C00000A2D1A110E72432300706F4500000A74280000011108285500000A2D10096F5E00000AA52400000216FE012B011600131511152D4F001106168C2D0000016F1C00000A001107178C2D0000016F1C00000A001104168C2D0000016F1C00000A001105110E72C52200706F4500000AA508000001285D00000A8C080000016F1C00000A000038470A00001105110E72A92200706F4500000AA5080000018C080000016F1C00000A001104110E72A92200706F4500000AA5080000018C080000016F1C00000A0038060A00001106168C2D0000016F1C00000A001104168C2D0000016F1C00000A001107178C2D0000016F1C00000A001105110E72032300706F4500000A6F1C00000A0009178C2D0000016F1C00000A00110E72432300706F5C00000A16FE01131511152D2400110E724323007011086F3E00000A0008110E72432300706F4500000A6F1C00000A0000110920150200002E0E11092021020000FE0116FE012B011600131511152D1D0009168C2D0000016F1C00000A001105168C2D0000016F1C00000A0000110920140200002E29110920200200002E20110920190200002E17110920250200002E0E11092054020000FE0116FE012B011600131511152D0F0009188C2D0000016F1C00000A000038F808000009168C2D0000016F1C00000A001106168C2D0000016F1C00000A001104168C2D0000016F1C00000A001107168C2D0000016F1C00000A001105168C2D0000016F1C00000A00086F5E00000A6F1700000A1108285A00000A16FE01131511152D26001106178C2D0000016F1C00000A001104110E72032300706F4500000A6F1C00000A00002B24001107178C2D0000016F1C00000A001105110E72032300706F4500000A6F1C00000A000038490800001106178C2D0000016F1C00000A001107168C2D0000016F1C00000A001104168C2D0000016F1C00000A001105168C2D0000016F1C00000A00110E72432300706F5C00000A2D1D110E72432300706F4500000A74280000011108285500000A16FE012B011700131511152D1E001106168C2D0000016F1C00000A001107178C2D0000016F1C00000A000038BB0700001106178C2D0000016F1C00000A001107168C2D0000016F1C00000A001104110E72A92200706F4500000AA5080000018C080000016F1C00000A001105168C2D0000016F1C00000A00110E72432300706F5C00000A2D1D110E72432300706F4500000A74280000011108285500000A16FE012B011700131511152D4A001106168C2D0000016F1C00000A001107178C2D0000016F1C00000A001104168C2D0000016F1C00000A001105110E72A92200706F4500000AA5080000018C080000016F1C00000A000038F10600001106178C2D0000016F1C00000A001107168C2D0000016F1C00000A001104110E72C52200706F4500000AA508000001285D00000A8C080000016F1C00000A001105168C2D0000016F1C00000A00110E72432300706F5C00000A2D1D110E72432300706F4500000A74280000011108285500000A16FE012B011700131511152D4A001106168C2D0000016F1C00000A001107178C2D0000016F1C00000A001104168C2D0000016F1C00000A001105110E72C52200706F4500000AA5080000018C080000016F1C00000A000038220600001106178C2D0000016F1C00000A001107168C2D0000016F1C00000A001104110E72C52200706F4500000AA508000001285D00000A8C080000016F1C00000A001105168C2D0000016F1C00000A00110E72432300706F5C00000A2D1D110E72432300706F4500000A74280000011108285500000A16FE012B011700131511152D4F001106168C2D0000016F1C00000A001107178C2D0000016F1C00000A001104168C2D0000016F1C00000A001105110E72C52200706F4500000AA508000001285D00000A8C080000016F1C00000A0000384E0500001106178C2D0000016F1C00000A001107168C2D0000016F1C00000A001104110E72A92200706F4500000AA5080000018C080000016F1C00000A001105168C2D0000016F1C00000A00110E72432300706F5C00000A2D1D110E72432300706F4500000A74280000011108285500000A16FE012B011700131511152D4A001106168C2D0000016F1C00000A001107178C2D0000016F1C00000A001104168C2D0000016F1C00000A001105110E72A92200706F4500000AA5080000018C080000016F1C00000A00003884040000110E72432300706F5C00000A2D1D110E72432300706F4500000A74280000011108285500000A16FE012B011700131511152D4C001106168C2D0000016F1C00000A001107178C2D0000016F1C00000A001104168C2D0000016F1C00000A001105110E72832200706F4500000AA5080000018C080000016F1C00000A00002B02000038FE030000110E72432300706F5C00000A2D1D110E72432300706F4500000A74280000011108285500000A16FE012B011700131511152D51001106168C2D0000016F1C00000A001107178C2D0000016F1C00000A001104168C2D0000016F1C00000A001105110E72832200706F4500000AA508000001285D00000A8C080000016F1C00000A00002B02000038730300001106178C2D0000016F1C00000A001107168C2D0000016F1C00000A001104110E72C52200706F4500000AA508000001285D00000A8C080000016F1C00000A001105168C2D0000016F1C00000A00110E72432300706F5C00000A2D1D110E72432300706F4500000A74280000011108285500000A16FE012B011700131511152D4F001106168C2D0000016F1C00000A001107178C2D0000016F1C00000A001104168C2D0000016F1C00000A001105110E72C52200706F4500000AA508000001285D00000A8C080000016F1C00000A0000389F0200001104168C2D0000016F1C00000A00388C0200001106178C2D0000016F1C00000A001107168C2D0000016F1C00000A001104110E72A92200706F4500000AA5080000018C080000016F1C00000A001105168C2D0000016F1C00000A00110E72432300706F5C00000A2D1A110E72432300706F4500000A74280000011108285500000A2D10096F5E00000AA52400000216FE012B011600131511152D4A001106168C2D0000016F1C00000A001107178C2D0000016F1C00000A001104168C2D0000016F1C00000A001105110E72A92200706F4500000AA5080000018C080000016F1C00000A000038B50100001106178C2D0000016F1C00000A001107168C2D0000016F1C00000A001104110E72C52200706F4500000AA508000001285D00000A8C080000016F1C00000A001105168C2D0000016F1C00000A00110E72432300706F5C00000A2D1A110E72432300706F4500000A74280000011108285500000A2D10096F5E00000AA52400000216FE012B011600131511152D4F001106168C2D0000016F1C00000A001107178C2D0000016F1C00000A001104168C2D0000016F1C00000A001105110E72C52200706F4500000AA508000001285D00000A8C080000016F1C00000A000038D40000001105110E72032300706F4500000A6F1C00000A001104110E72032300706F4500000A6F1C00000A0011092040420F00310911092080841E00327211092080841E003109110920BFC62D0031601109204C0400003109110920AF040000314E110920B0040000310911092013050000313C11092014050000310911092077050000312A110920780500003109110920DB0500003118110920DC050000310B1109203F060000FE022B0117002B011600131511152D1E001105168C2D0000016F1C00000A001104168C2D0000016F1C00000A00002B00110920CA0000002E2F110920CB0000002E26110920C90000002E1D110920CC0000002E14110920CE0000002E0B110920D0000000FE012B011700131511153AB102000000110F6F5F00000A13100011106F6000000A131511152D0900161314DDF60A000011091318111820360100003D9700000011181F5D303D11181F4E5945020000004C0100004C01000011181F5759450700000026010000260100006401000064010000640100002601000026010000385F01000011181F633B1801000011182093000000594509000000E7000000E7000000E7000000E7000000E7000000E7000000E7000000E7000000E700000011182035010000594502000000D2000000D2000000380B01000011182035020000306711182003020000594510000000770000007700000077000000770000007700000077000000B5000000770000007700000077000000770000007700000077000000770000007700000077000000111820340200005945020000006700000067000000389B0000001118203D0200005945030000004400000044000000440000001118204B0200005945030000002B000000690000002B00000011182063020000594507000000020000000200000002000000020000000200000002000000020000002B3E38FE000000110E72832200701110166F6100000A8C080000016F3E00000A00110E72E12200701110166F6100000A8C080000016F3E00000A0038C500000011092040420F00310911092080841E00327211092080841E003109110920BFC62D0031601109204C0400003109110920AF040000314E110920B0040000310911092013050000313C11092014050000310911092077050000312A110920780500003109110920DB0500003118110920DC050000310B1109203F060000FE022B0117002B011600131511152D03002B36110E72832200701110166F6100000A8C080000016F3E00000A00110E72E12200701110176F6100000A8C080000016F3E00000A002B0000DE14111014FE01131511152D0811106F1E00000A00DC00000011166F4A00000A131511153ABAE4FFFFDE1D111675050000011317111714FE01131511152D0811176F1E00000A00DC0000DE14110F14FE01131511152D08110F6F1E00000A00DC0006166F6200000A0006729A3500706F1600000A260672800E00706F1600000A260672D00E00706F1600000A260672E63500706F1600000A2606726D3600706F1600000A2606723A0F00706F1600000A2606721D1000706F1600000A260672491000706F1600000A260672D53600706F1600000A2606722D3700706F1600000A260672853700706F1600000A260672DD3700706F1600000A260672353800706F1600000A2606728D3800706F1600000A260672E53800706F1600000A2606723D3900706F1600000A260672953900706F1600000A260672ED3900706F1600000A260672453A00706F1600000A2606729D3A00706F1600000A260672F53A00706F1600000A2606724D3B00706F1600000A260672A53B00706F1600000A260672FD3B00706F1600000A260672553C00706F1600000A260672AD3C00706F1600000A260672053D00706F1600000A2606725D3D00706F1600000A260672B53D00706F1600000A2606720D3E00706F1600000A260672653E00706F1600000A260672BD3E00706F1600000A260672153F00706F1600000A2606726D3F00706F1600000A260672C53F00706F1600000A2606721D4000706F1600000A260672754000706F1600000A260672CD4000706F1600000A260672254100706F1600000A2606727D4100706F1600000A260672D54100706F1600000A2606722D4200706F1600000A260672854200706F1600000A260672DD4200706F1600000A260672354300706F1600000A2606728D4300706F1600000A260672E54300706F1600000A2606723D4400706F1600000A260672954400706F1600000A260672ED4400706F1600000A260672454500706F1600000A2606729D4500706F1600000A260672F54500706F1600000A2606724D4600706F1600000A260672BD3E00706F1600000A260672A54600706F1600000A260672FD4600706F1600000A260672AC4700706F1600000A260672B84700706F1600000A260672184800706F1600000A2606723A4800706F1600000A260672804800706F1600000A260672C24800706F1600000A260672E44800706F1600000A260672EC4800706F1600000A260672024900706F1600000A260672624900706F1600000A260672C24900706F1600000A260672774A00706F1600000A260672404B00706F1600000A260672234C00706F1600000A260672374C00706F1600000A260672534C00706F1600000A260672AD4C00706F1600000A260672F34C00706F1600000A260672334D00706F1600000A2606724D4D00706F1600000A260672A14D00706F1600000A260672F94D00706F1600000A260672454E00706F1600000A260672834E00706F1600000A260672C34E00706F1600000A260672FD4E00706F1600000A26066F1700000A036F1800000A03731900000A130F00110F6F1A00000A72B9200070166F1B00000A72112200706F4B00000A00110F6F1A00000A72420E0070166F1B00000A72312200706F4B00000A00110F6F1A00000A727B2100701F0C1F326F2F00000A724D2200706F4B00000A00110F6F1A00000A72D52000701E6F1B00000A72732200706F4B00000A00110F6F1A00000A72E32000701F096F1B00000A72832200706F4B00000A00110F6F1A00000A721D2100701F096F1B00000A72A92200706F4B00000A00110F6F1A00000A72052100701F096F1B00000A72C52200706F4B00000A00110F6F1A00000A72352100701F096F1B00000A72E12200706F4B00000A00110F6F1A00000A72663400701F096F1B00000A72032300706F4B00000A00110F6F1A00000A72532100701F0C20000100006F2F00000A722D2300706F4B00000A00110F6F1A00000A72134F00701F0C6F1B00000A72432300706F4B00000A00110F6F1A00000A72314F00701F096F1B00000A726D2300706F4B00000A00110F6F1A00000A72494F00701F0C6F1B00000A11086F1C00000A00110F6F1A00000A72D8330070166F1B00000A027B150000047B090000048C270000016F1C00000A00110F6F1A00000A7291210070166F1B00000A027B150000047B0A0000048C2D0000016F1C00000A00110F6F1A00000A72A92100701F0C1F326F2F00000A027B150000047B0C0000046F1C00000A00110F6F1A00000A72774F0070166F1B00000A027B150000047B0E0000048C2D0000016F1C00000A00110F6F1A00000A72894F00701F0C1F326F2F00000A027B150000047B0F0000046F1C00000A00110F6F1A00000A729F4F00701E6F1B00000A168C2D0000016F1C00000A00110F6F1A00000A72B34F00701F096F1B00000A72892300706F4B00000A00110F6F1A00000A72E14F0070166F1B00000A72BB2300706F4B00000A00110F6F1A00000A720F5000701E6F1B00000A20C80000008C0C0000026F1C00000A00110F6F1A00000A72315000701E6F1B00000A202B0100008C0C0000026F1C00000A00110F6F1A00000A7251500070166F1B00000A027B160000048C270000016F1C00000A00110F6F1A00000A7277500070166F1B00000A72F12300706F4B00000A00110F6F1A00000A72895000701E6F1B00000A72072400706F4B00000A00110F6F1A00000A72B3500070166F1B00000A724F2400706F4B00000A00110F6F1A00000A72CB5000701A6F1B00000A726B2400706F4B00000A00110F166F4D00000A00734E00000A1311001111110F6F4F00000A00111120F40100006F6300000A001111027B130000046F5000000A0B07027B130000046F4000000A6F4300000AFE0116FE01131511152D0600171314DE4500DE14111114FE01131511152D0811116F1E00000A00DC0000DE14110F14FE01131511152D08110F6F1E00000A00DC0000DE0C13120011127A13130011137A001613142B000011142A00000041C400000200000045000000C70000000C0100001D00000000000000020000007C1D0000910200000D200000140000000000000002000000E90400004D1B0000362000001D0000000000000002000000A8030000AF1C000057200000140000000000000002000000FC2700004B000000472800001400000000000000020000006C240000F30300005F28000014000000000000000000000001000000762800007728000006000000400000010000000001000000762800007D280000060000003700000113300200930F000019000011000E04500A0416731100000A81080000010516731100000A81080000010E04720100007051020B0720370100003D8B0300000745D1000000F40100000C020000D30200001B030000330300008703000051040000190400003504000089040000A5040000DD040000F904000015050000510400000D0B00000D0B00000D0B00000D0B00000D0B00005E02000096020000B20200007A02000041020000410200000D0B00000D0B0000C104000085050000A6050000C2050000DE050000FA0500004E0600004D05000086060000320600006A06000031050000A2060000BE06000085050000DA060000F606000017070000590700007A070000190400000D0B00000D0B00000D0B00000D0B00000D0B0000310500004D0500000D0B00000D0B00000D0B00003807000069050000690500007A0200007A0200007A02000019040000DD0400009B070000B70700006D0400006D040000510400004F030000D3070000EB070000070800000D0B00000D0B00000D0B00000D0B00000D0B00000D0B00000D0B00000D0B0000A3030000320600004E0600000D0B00000D0B00000D0B0000D3070000240800000D0B00000D0B0000410800005D0800005D0800000D0B00000D0B00001B030000410800007908000079080000FD030000E1030000FD030000E1030000E1030000E1030000E1030000B9090000B9090000B9090000E1030000E1030000E1030000E1030000FD030000FD030000E1030000E1030000E1030000FD030000FD030000FD030000FD030000FD030000FD030000FD0300000D0B00000D0B00000D0B00000D0B00000D0B00000D0B00000D0B00000D0B00000D0B00000D0B00000D0B0000A10900008909000021090000160600000D0B00000D0B0000210900000D0B00000D0B00000D0B00000D0B0000D1090000D1090000D1090000D1090000D1090000E9090000310A00000D0B00000D0B000029020000290200000D0B00000D0B00000D0B00000D0B00000D0B00000D0B00000D0B00000D0B00000D0B00000D0B00000D0B00000D0B00000D0B00000D0B00000D0B00000D0B00000D0B00000D0B00000D0B00000D0B00000D0B00000D0B00000D0B00000D0B00000D0B00000D0B00000D0B00000D0B0000D30200001B0300000D0B00000D0B00000D0B00000D0B00000D0B00000D0B00000D0B00000D0B0000F4010000F4010000D30200001B030000EB0200000D0B0000030300000D0B00001B03000007202C01000059450C000000C9080000AD080000C9080000C9080000AD0800001D09000035090000D10A0000D10A0000A10A0000B90A0000C908000038CC0A00000720F4010000594563000000FA000000FA000000340900003409000034090000340900003409000034090000340900003409000034090000340900003409000034090000920100003409000034090000340900003409000034090000340900007408000034090000340900003409000034090000F8070000F8070000F8070000F8070000F807000034090000B0020000B0020000B0020000B0020000B0020000E8020000E8020000E8020000E8020000B0020000CC020000E8020000B0020000B0020000B0020000B0020000B0020000E8020000E8020000E8020000E802000042010000FA0000002A0100003409000012010000340900003409000012010000340900000802000008020000120100002A010000BC060000BC060000D8060000D8060000F4060000F4060000120100003409000034090000340900008C0800008C0800008C0800008C0800008C0800006407000040020000640700009402000034090000E802000034090000BC08000034090000A408000008020000D408000012010000120100001201000012010000EC080000EC080000072040420F003B1D080000072080841E003B2A0800003819090000040381080000010516731100000A8108000001380A0A00000416731100000A81080000010516731100000A810800000138ED090000040381080000010516731100000A810800000138D50900000416731100000A81080000010516731100000A810800000138B8090000040381080000010516731100000A81080000010E040651389C0900000416731100000A8108000001050381080000010E0406513880090000040381080000010516731100000A81080000010E04065138640900000416731100000A81080000010516731100000A81080000010E0406513843090000040381080000010516731100000A8108000001382B090000040381080000010516731100000A810800000138130900000416731100000A81080000010503810800000138FB0800000416731100000A81080000010503810800000138E3080000040381080000010516731100000A81080000010E04065138C70800000416731100000A8108000001050381080000010E04065138AB080000040381080000010516731100000A81080000010E040651388F0800000416731100000A8108000001050381080000010E04065138730800000E0516FE010C082D17000416731100000A810800000105038108000001002B1500040381080000010516731100000A8108000001000E0406513835080000040381080000010516731100000A81080000010E04065138190800000416731100000A8108000001050381080000010E04065138FD070000040381080000010516731100000A81080000010E04065138E10700000416731100000A8108000001050381080000010E04065138C5070000040381080000010516731100000A81080000010E04065138A90700000416731100000A8108000001050381080000010E040651388D070000040381080000010516731100000A81080000010E04065138710700000416731100000A8108000001050381080000010E0406513855070000040381080000010516731100000A81080000010E04065138390700000416731100000A8108000001050381080000010E040651381D070000040381080000010516731100000A81080000010E0406513801070000040381080000010516731100000A81080000010E04065138E5060000040381080000010516731100000A81080000010E04065138C9060000040381080000010516731100000A81080000010E04065138AD060000040381080000010516731100000A81080000010E04065138910600000416731100000A81080000010516731100000A81080000010E04065138700600000416731100000A8108000001050381080000010E0406513854060000040381080000010516731100000A81080000010E04065138380600000416731100000A8108000001050381080000010E040651381C060000040381080000010516731100000A81080000010E04065138000600000416731100000A8108000001050381080000010E04065138E4050000040381080000010516731100000A81080000010E04065138C8050000040381080000010516731100000A81080000010E04065138AC0500000416731100000A8108000001050381080000010E04065138900500000416731100000A8108000001050381080000010E04065138740500000416731100000A8108000001050381080000010E04065138580500000416731100000A8108000001050381080000010E040651383C050000040381080000010516731100000A81080000010E04065138200500000416731100000A81080000010516731100000A81080000010E04065138FF0400000416731100000A81080000010516731100000A81080000010E04065138DE0400000416731100000A81080000010516731100000A81080000010E04065138BD0400000416731100000A81080000010516731100000A81080000010E040651389C0400000416731100000A81080000010516731100000A81080000010E040651387B040000040381080000010516731100000A81080000010E040651385F0400000416731100000A8108000001050381080000010E0406513843040000040381080000010516731100000A8108000001382B040000040381080000010516731100000A81080000010E040651380F0400000416731100000A81080000010516731100000A810800000138F20300000416731100000A81080000010516731100000A810800000138D50300000416731100000A8108000001050381080000010E04065138B9030000040381080000010516731100000A81080000010E040651389D0300000416731100000A8108000001050381080000010E04065138810300000416731100000A8108000001050381080000010E0406513865030000040381080000010516731100000A81080000010E04065138490300000416731100000A8108000001050381080000010E040651382D030000040381080000010516731100000A81080000010E04065138110300000416731100000A8108000001050381080000010E04065138F5020000040381080000010516731100000A81080000010E04065138D9020000040381080000010516731100000A81080000010E04065138BD020000040381080000010516731100000A810800000138A50200000416731100000A810800000105038108000001388D020000040381080000010516731100000A810800000138750200000416731100000A810800000105038108000001385D0200000416731100000A8108000001050381080000013845020000040381080000010516731100000A8108000001382D0200000416731100000A8108000001050381080000013815020000040381080000010516731100000A810800000138FD0100000416731100000A81080000010503810800000138E50100000416731100000A8108000001050381080000010E04065138C9010000040381080000010516731100000A810800000138B1010000040381080000010516731100000A81080000013899010000040381080000010516731100000A810800000138810100000416731100000A81080000010503810800000138690100000416731100000A8108000001050381080000013851010000040381080000010516731100000A810800000138390100000416731100000A8108000001050381080000013821010000040381080000010516731100000A81080000013809010000022040420F00310D022080841E00FE0416FE012B0117000C082D1900040381080000010516731100000A810800000138D5000000022080841E00310A0220BFC62D00FE022B0117000C082D19000416731100000A81080000010503810800000138A4000000022014050000310A022077050000FE022B0117000C082D1600040381080000010516731100000A81080000012B76022078050000310A0220DB050000FE022B0117000C082D16000416731100000A8108000001050381080000012B4802204C040000310A0220AF040000FE022B0117000C082D1600040381080000010516731100000A81080000012B1A0416731100000A81080000010516731100000A81080000012B002A0013300200240400001A00001100020C0820D00000003DB8020000081C59459C0000003E0100003E0100003E01000069010000690100003E0100003E010000690100003E01000069010000690100006901000069010000690100003E0100003E010000690100003E0100006901000069010000690100006901000069010000690100003E0100003E0100003E0100003E0100003E0100003E0100003E0100003E0100003E0100003E0100003E0100003E010000690100003E010000690100006901000069010000690100003E01000069010000690100006901000069010000690100003E0100003E010000690100006901000069010000690100003E0100003E0100003E0100003E0100003E0100003E0100003E0100003E0100003E0100003E0100003E0100003E0100003E0100003E010000690100006901000069010000690100006901000069010000690100006901000069010000690100003E0100003E0100003E0100006901000069010000690100003E0100003E01000069010000690100003E0100003E0100003E01000069010000690100003E0100003E0100003E0100003E0100004B0100003E0100003E0100003E0100003E0100003E0100003E0100003E0100003E0100003E01000069010000690100003E0100003E0100004B0100004B0100003E0100003E0100003E0100003E0100003E0100004B0100004B0100004B0100004B0100004B01000069010000690100006901000069010000690100006901000069010000690100006901000069010000690100003E0100003E0100003E0100003E01000069010000690100003E010000690100006901000069010000690100006901000069010000690100006901000069010000690100003E01000069010000690100003E0100003E0100000820BF0000003B330100000820C800000059450900000003010000030100000301000003010000030100002E0100000301000003010000030100003829010000082002020000306108202C01000059450C000000BA000000BA000000E5000000BA000000BA000000E5000000E5000000E5000000E5000000BA000000BA000000BA0000000820FE0100005945050000009A0000009A0000009A0000009A0000009A00000038C00000000820090200003B8A00000008203202000059451F000000020000000200000002000000020000000200000002000000020000000200000002000000020000002D0000002D0000002D0000002D0000000200000002000000020000000200000002000000020000002D0000002D000000020000002D0000002D0000002D000000020000002D000000020000000B000000020000002B2B16731100000A0A2B51030A2B4D0516FE010D092D0B0004285D00000A0A002B090016731100000A0A002B2F02204C040000310A02203F060000FE022B0117000D092D0B0016731100000A0A002B0A000304282700000A0A002B00060B2B00072A133001000C0000001B00001100027B680300040A2B00062A260002037D680300042A0000133001000C0000001C00001100027B770300040A2B00062A260002037D770300042A0000133001000C0000001D00001100027B690300040A2B00062A260002037D690300042A0000133001000C0000001D00001100027B6A0300040A2B00062A260002037D6A0300042A0000133001000C0000001B00001100027B720300040A2B00062A260002037D720300042A0000133001000C0000001E00001100027B730300040A2B00062A260002037D730300042A0000133001000C0000000900001100027B740300040A2B00062A260002037D740300042A0000133001000C0000001B00001100027B6B0300040A2B00062A260002037D6B0300042A0000133001000C0000001B00001100027B6C0300040A2B00062A260002037D6C0300042A000013300200170000001B00001100027B6D030004027B6E030004282800000A0A2B00062A00133001000C0000001B00001100027B6D0300040A2B00062A260002037D6D0300042A0000133001000C0000001B00001100027B6E0300040A2B00062A260002037D6E0300042A0000133001000C0000001B00001100027B6F0300040A2B00062A260002037D6F0300042A0000133001000C0000001B00001100027B700300040A2B00062A260002037D700300042A0000133001000C0000001B00001100027B710300040A2B00062A260002037D710300042A0000133001000C0000000900001100027B750300040A2B00062A260002037D750300042A0000133001000C0000001F00001100027B760300040A2B00062A260002037D760300042A0000133001000C0000002000001100027B7A0300040A2B00062A260002037D7A0300042A0000133001000C0000002100001100027B780300040A2B00062A260002037D780300042A0000133001000C0000001B00001100027B790300040A2B00062A260002037D790300042A8602167D7503000402738C0000067D7803000402167D7A03000402283000000A002A133001000C0000001C00001100027B7B0300040A2B00062A260002037D7B0300042A0000133001000C0000002000001100027B7C0300040A2B00062A260002037D7C0300042A0000133001000C0000001D00001100027B7D0300040A2B00062A260002037D7D0300042A0000133001000C0000001D00001100027B7E0300040A2B00062A260002037D7E0300042A0000133001000C0000001D00001100027B7F0300040A2B00062A260002037D7F0300042A0000133001000C0000001E00001100027B800300040A2B00062A260002037D800300042A0000133001000C0000001D00001100027B810300040A2B00062A260002037D810300042A0000133001000C0000001B00001100027B820300040A2B00062A260002037D820300042A0000133001000C0000001D00001100027B830300040A2B00062A260002037D830300042A0000133001000C0000001D00001100027B840300040A2B00062A260002037D840300042A0000133001000C0000001D00001100027B860300040A2B00062A260002037D860300042A0000133001000C0000001D00001100027B870300040A2B00062A260002037D870300042A0000133001000C0000000900001100027B850300040A2B00062A260002037D850300042A0000133001000C0000001D00001100027B880300040A2B00062A260002037D880300042A0000133001000C0000002200001100027B890300040A2B00062A260002037D890300042A0000133001000C0000002300001100027B8A0300040A2B00062A260002037D8A0300042A0000133001000C0000001D00001100027B8B0300040A2B00062A260002037D8B0300042A2A02283000000A0000002A000000133001000C0000001E00001100027BD90300040A2B00062A260002037DD90300042A0000133001000C0000001B00001100027BDA0300040A2B00062A260002037DDA0300042A0000133001000C0000001D00001100027BDB0300040A2B00062A260002037DDB0300042A0000133001000C0000001F00001100027BDC0300040A2B00062A260002037DDC0300042A0000133001000C0000001C00001100027BD80300040A2B00062A260002037DD80300042A0000133001000C0000001D00001100027BDD0300040A2B00062A260002037DDD0300042A1E02283000000A2A00001B3005006B050000240000110004280E010006510516540003280E010006120212050E0428C5000006130C110C2D090016130BDD3B0500001105166AFE01130C110C2D090016130BDD26050000731500000A0A02130D110D1759450200000005000000D700000038600100001F0A130406166F6200000A000672D95000706F1600000A260672355100706F1600000A260672975100706F1600000A260672285200706F1600000A260672945200706F1600000A26066F1700000A0D06166F6200000A000672DE5200700972CD530070283C00000A6F1600000A260672DB53007009286600000A6F1600000A260672CA5400706F1600000A260672D65400700972CD530070283C00000A6F1600000A260672C155007009286600000A6F1600000A260672FA2F00706F1600000A260672AC5600706F1600000A2638960000001B130406166F6200000A000672D95000706F1600000A260672355100706F1600000A260672F05600706F1600000A2606723E5700706F1600000A260672A25700706F1600000A260672FE5700706F1600000A26066F1700000A0D06166F6200000A000672545800706F1600000A260672865800706F1600000A2606720659007009286600000A6F1600000A262B0816130BDD9F030000066F1700000A0E046F1800000A0E04731900000A13060011066F1A00000A72420E0070166F1B00000A038C270000016F1C00000A0011066F1A00000A721C5900701E6F1B00000A188C0A0000026F1C00000A0011066F1A00000A723A5900701E6F1B00000A178C090000026F1C00000A0011066F1A00000A725A5900701E6F1B00000A188C090000026F1C00000A0011066F1A00000A727A5900701E6F1B00000A188C2D0000016F1C00000A001106736700000A130700733200000A0B1107076F6800000A2600DE14110714FE01130C110C2D0811076F1E00000A00DC0000DE14110614FE01130C110C2D0811066F1E00000A00DC00076F4000000A6F4300000A16FE02130C110C2D090017130BDD8C02000006166F6200000A000672A05900706F1600000A260672DC5900706F1600000A260672385A00706F1600000A260672765A00706F1600000A260672F95A00706F1600000A260672535B007009286600000A6F1600000A26066F1700000A0E046F1800000A0E04731900000A13060011066F1A00000A72695B0070166F1B00000A130811066F1A00000A727F5B00701E6F1B00000A11048C0A0000026F1C00000A0011066F1A00000A72420E0070166F1B00000A038C270000016F1C00000A0011066F1A00000A721C5900701E6F1B00000A188C0A0000026F1C00000A0011066F1A00000A723A5900701E6F1B00000A178C090000026F1C00000A0011066F1A00000A725A5900701E6F1B00000A188C090000026F1C00000A0011066F1A00000A727A5900701E6F1B00000A188C2D0000016F1C00000A0000076F4000000A6F4800000A130E389F000000110E6F4900000A740B00000113090011081109166F6900000A6F1C00000A001109166F6900000AA527000001156AFE0116FE01130C110C2D13000516540450087B820400047D820400042B6411066F5F00000A130A00110A6F6000000A130C110C2D0300DE3905254A1758540450257B82040004110A166F6100000A282800000A7D8204000400DE14110A14FE01130C110C2D08110A6F1E00000A00DC000000110E6F4A00000A130C110C3A50FFFFFFDE1D110E7505000001130F110F14FE01130C110C2D08110F6F1E00000A00DC0000DE14110614FE01130C110C2D0811066F1E00000A00DC00045004507B80040004087B80040004282E00000A7D80040004045004507B81040004087B81040004282E00000A7D81040004045004507B82040004087B82040004282E00000A7D8204000417130BDE0B260000DE000016130B2B0000110B2A0041940000020000007E0200001300000091020000140000000000000002000000DE010000CB000000A90200001400000000000000020000007904000034000000AD0400001400000000000000020000001F040000B7000000D60400001D000000000000000200000047030000B0010000F70400001400000000000000000000000B000000510500005C050000050000000100000113300800B4000000010000110073F20000060A027B2104000416FE01130411042D1500061772975B00706FEE00000600060D388700000000166A027B20040004280E01000616731100000A1612020328CA000006130411042D1300061F0A72010000706FEE00000600060D2B51027B1F0400041201032835010006130411042D1300061F1472010000706FEE00000600060D2B2A00027B1E0400040708027B21040004027B22040004027B23040004027B2404000403289D0000060D2B00092A133008001800000025000011000203040516280E010006160E04289D0000060A2B00062A13300B007C010000260000110073F20000060A72F15B007072FB5B00700E0728EC0000061201284700000A260716FE010D092D1600061F1572215C00706FEE00000600060C383C010000032C0E037BC604000416FE0116FE012B0116000D092D1600061F1472010000706FEE00000600060C380F010000037BCB0400042D0B037BCC04000416FE012B0116000D092D1600061F1572010000706FEE00000600060C38E0000000042C0F047B8B040004166AFE0116FE012B0116000D092D1600061F0A72010000706FEE00000600060C38B2000000047B8C04000416FE010D092D1600061F0B72010000706FEE00000600060C388F000000050D092D5500047B93040004166AFE010D092D4500037BC80400041B3321047B94040004037BC60400043313047B950400042D0B047B9604000416FE012B0116000D092D1300061F0C72010000706FEE00000600060C2B3700000402037BC6040004037BC8040004037BC9040004050E040E050E0612000E07289E0000062606047B900400047D2C040004060C2B00082A1B3009001209000027000011001613170E0973F2000006510E09501772010000706FEE00000600027B8B040004130F001613111613121613190E05131C72F15B0070726D5C00700E0A28EC00000672835C00706F6A00000A131D111C16FE01132911292D37000E06182E1D0E061733140E076F0D01000616731100000A282900000A2B0116002B01170013110E061A3304111D2B0116001319002B06000E0813120011192D0F111D2C07111116FE012B0117002B011600132911292D050016131C0011112D0411122B0117001313111116FE01132911292D05001A131700111216FE01132911292D0E0011171A60131711171E60131700166A131B722235007072423500700E0A28EC000006131A72875C00700E0A6F1800000A0E0A731900000A131E00111E6F1A00000A720D0500701F0C1F326F2F00000A048C2D0000016F1C00000A00111E6F5F00000A131F00111F6F6000000A132911292D18000E09501772425D00706FEE00000600161328DDA6070000111F166F6B00000A131A111F176F6C00000A2D0A111F176F6D00000A2B02166A00131B00DE14111F14FE01132911292D08111F6F1E00000A00DC0000DE14111E14FE01132911292D08111E6F1E00000A00DC00111116FE01132911293A3C010000001B1310166A130F731500000A130E110E72A65D00706F1600000A26110E72025E00706F1600000A26110E725E5E00706F1600000A26110E72BA5E00706F1600000A26110E72165F00706F1600000A26110E72725F00706F1600000A26110E72CE5F00706F1600000A26110E6F1700000A0E0A6F1800000A0E0A731900000A13200011206F1A00000A720D0500701E6F1B00000A048C2D0000016F1C00000A0011206F1A00000A722A6000701E6F1B00000A168C110000026F1C00000A0011206F5F00000A13210011216F6000000A16FE01132911292D33001121166F6C00000A2D0A1121166F6D00000A2B02166A00130F1121176F6C00000A2D0A1121176F6E00000A2B01160013100000DE14112114FE01132911292D0811216F1E00000A00DC0000DE14112014FE01132911292D0811206F1E00000A00DC0000110F7E6F00000A280E01000616731100000A16111712140E0A28CB000006132911292D18000E095017723C6000706FEE00000600161328DDCD05000011147B9A0400040B11147B9B040004130811147B93040004130B11147B990400041305111216FE01132911292D2A0016731100000A077B87040004282600000A131516731100000A077B88040004282600000A1316002B340016731100000A131616731100000A0E077B82040004077B82040004077B87040004282700000A282700000A282600000A13150011151F64731100000A287000000A287100000A1F64731100000A282B00000A131511161F64731100000A287000000A287100000A1F64731100000A282B00000A13160E0617FE0116FE01132911292D4A0007077B820400040E077B82040004282E00000A7D8204000407077B810400040E077B81040004282E00000A7D8104000407077B800400040E077B80040004282E00000A7D8004000400027B8B0400041322111916FE01132911292D0600111B13220011220405111C0E060E07120012070E0A28A3000006132911292D18000E09501772986000706FEE00000600161328DD5B040000111316FE01132911292D18000E09501772D26000706FEE00000600161328DD38040000111116FE01132911293AFB0000000016130A110F046A060307111D16FE01120312230E0A28BA000006132911292D18000E095017721A6100706FEE00000600161328DDF1030000096F0D01000616731100000A287200000A16FE01132911292D6F000E0950171C8D28000001132A112A167258610070A2112A17096F0D010006132B122B282400000AA2112A1872C8610070A2112A190E076F0D010006132B122B282400000AA2112A1A72F6610070A2112A1B0F02282100000AA2112A287300000A6FEE00000600161328DD68030000111D132911292D2E00110F09046A11230E0A28D6000006132911292D18000E09501772166200706FEE00000600161328DD3303000000002B7C00110B166AFE01130A110A2D07111C16FE012B011600132911292D3100110F166A12241225120C0E0A28B2000006132911292D18000E09501772726200706FEE00000600161328DDE302000000110F046A06030712030E0A28B8000006132911292D18000E095017721A6100706FEE00000600161328DDB40200000009097B810400041F64731100000A287000000A287100000A1F64731100000A282B00000A7D8104000409097B820400041F64731100000A287000000A287100000A1F64731100000A282B00000A7D82040004110F09281601000616731100000A12021209120B12050E0A28C8000006132911292D23000E09501772B6620070096F1700000A286600000A6FEE00000600161328DD1B020000040E0406731100000613061106166A110F1B076F0D010006096F0D01000616731100000A086F0D0100066F13000006132911292D18000E095017721C6300706FEE00000600161328DDCE01000011060E0A6F1D000006132911292D18000E09501772846300706FEE00000600161328DDA7010000110F0E0A28CC000006132911292D18000E09501772CA6300706FEE00000600161328DD80010000722235007072423500700E0A28EC0000061326091126111A12180E0A28A0000006132911292D2F000E09501772FA6300701200281F00000A721A640070096F1700000A287400000A6FEE00000600161328DD2A010000110A061118111C11190E0A28D1000006132911292D5D000E0950171C8D28000001132A112A167242640070A2112A17120A287500000AA2112A18729E640070A2112A191200281F00000AA2112A1A721A640070A2112A1B096F1700000AA2112A287300000A6FEE00000600161328DDB700000073060100061304111C132911292D3B001107110F03091204120C120D0E0A28CF000006132911292D20000E09501772C2640070096F1700000A286600000A6FEE00000600161328DE6E000E0950067D250400040E0950110A7D260400040E0950077D270400040E0950097D280400040E095011047D2A0400040E0950087D290400040E095011097D2B0400040E09506FF000000600171328DE1D1327000E09501711276F7600000A6FEE0000060000DE00001613282B000011282A0000417C000002000000420100004C0000008E010000140000000000000002000000170100008F000000A60100001400000000000000020000009002000047000000D70200001400000000000000020000004A020000A5000000EF02000014000000000000000000000023000000CE080000F108000017000000370000011B3004002D01000028000011000502810800000103046F6A00000A16FE01130411042D0800170D380A01000000731500000A0A06722C6500706F1600000A2606728C6500706F1600000A260672EC6500706F1600000A2606724C6600706F1600000A26066F1700000A0E046F1800000A0E04731900000A0B00076F1A00000A727F0500701F096F1B00000A028C080000016F1C00000A00076F1A00000A72AC6600701F0C6F1B00000A036F1C00000A00076F1A00000A72C06600701F0C6F1B00000A046F1C00000A00076F5F00000A0C00086F6000000A16FE01130411042D1C0005080872D06600706F7700000A6F6100000A8108000001170DDE3A00DE120814FE01130411042D07086F1E00000A00DC0000DE120714FE01130411042D07076F1E00000A00DC0000DE062600160DDE0500160D2B0000092A000000012800000200C4002FF300120000000002006C009D0901120000000000002000FF1F01063700000113300500AF000000290000110005025103046F6A00000A16FE010C082D0800170B3893000000027B80040004030405507C800400040E04289F0000060A062C18027B81040004030405507C810400040E04289F0000062B0116000A062C18027B82040004030405507C820400040E04289F0000062B0116000A062C18027B87040004030405507C870400040E04289F0000062B0116000A062C18027B88040004030405507C880400040E04289F0000062B0116000A060B2B00072A001B3003002C0100002A0000110000731500000A0A0672E46600706F1600000A260672676700706F1600000A260672586800706F1600000A260672E16800706F1600000A260516FE010D092D26000672616900706F1600000A260672D56900706F1600000A260672496A00706F1600000A26000672BD6A00706F1600000A260672C56A00706F1600000A260672456B00706F1600000A260672CE6B00706F1600000A26066F1700000A0E046F1800000A0E04731900000A0B00076F1A00000A72420E0070166F1B00000A028C270000016F1C00000A00076F1A00000A720D0500701E6F1B00000A038C2D0000016F1C00000A00076F1A00000A7227050070166F1B00000A048C270000016F1C00000A00076F1D00000A18FE010CDE1A0714FE010D092D07076F1E00000A00DC260000DE0000160C2B0000082A4134000002000000AB000000640000000F010000100000000000000000000000010000001E0100001F01000005000000010000011B3003004F0100002A0000110000731500000A0A0672516C00706F1600000A260672EA6C00706F1600000A260672876D00706F1600000A260672286E00706F1600000A2606729C6E00706F1600000A260672106F00706F1600000A260672846F00706F1600000A260672077000706F1600000A2606728C7000706F1600000A260672BD6A00706F1600000A260672177100706F1600000A2606728B7100706F1600000A260672FF7100706F1600000A260672847200706F1600000A260672077300706F1600000A26066F1700000A056F1800000A05731900000A0B00076F1A00000A72420E0070166F1B00000A028C270000016F1C00000A00076F1A00000A720D0500701E6F1B00000A038C2D0000016F1C00000A00076F1A00000A7227050070166F1B00000A048C270000016F1C00000A00076F1D00000A26170CDE1A0714FE010D092D07076F1E00000A00DC260000DE0000160C2B0000082A004134000002000000CF000000630000003201000010000000000000000000000001000000410100004201000005000000010000011B3007001D0400002B000011000E06166A550E071654160B166A0C160D1613047201000070130516130C16130D16731100000A130E16731100000A130F16731100000A131016731100000A131116731100000A1312731500000A0A000672927300706F1600000A260672D67300706F1600000A260672227400706F1600000A260672487400706F1600000A260672727400706F1600000A260672A47400706F1600000A260672E27400706F1600000A260672267500706F1600000A260672707500706F1600000A260672BA7500706F1600000A260672107600706F1600000A260672667600706F1600000A260672B27600706F1600000A2606720A7700706F1600000A260672C97700706F1600000A26066F1700000A0E086F1800000A0E08731900000A13130011136F1A00000A72420E0070166F1B00000A028C270000016F1C00000A0011136F5F00000A13140011146F6000000A131611162D0900161315DDC00200001114166F6C00000A2D0A1114166F6E00000A2B0116000B1114176F6C00000A2D0A1114176F6D00000A2B02166A000C1114186F6C00000A2D0A1114186F6E00000A2B0116000D1114196F6C00000A2D0A1114196F6E00000A2B011700130405131611162D350011141A6F6C00000A131611162D2500120611141A6F6B00000A1628440100061307110716FE01131611162D06001106130500000011141B6F6C00000A2D0A11141B6F7800000A2B011600130811141C6F6E00000A130C11141D6F6E00000A130D11141E6F6100000A130E11141F096F6100000A130F11141F0A6F6100000A131011141F0B6F6100000A131111141F0C6F6100000A131200DE14111414FE01131611162D0811146F1E00000A00DC0000DE14111314FE01131611162D0811136F1E00000A00DC00052C070E0416FE022B011700131611163AF800000000110C163053110D16304E110E16731100000A282D00000A2D3F110F16731100000A282D00000A2D30111016731100000A282D00000A2D21111116731100000A282D00000A2D12111216731100000A282D00000A16FE012B011600131611163A930000000018081209120A120B0E0828E3000006131611162D0900161315DDFC00000011092D07110A16FE012B011700131611162D1900080E0828E9000006131611162D0900161315DDD10000000002091105030412020E0828A4000006131611162D0900161315DDB2000000020308160E0828A1000006131611162D0900161315DD980000000E0717540E060855171315DD880000000008166AFE0116FE01131611162D400002091105030412020E0828A4000006131611162D0600161315DE5D020308170E0828A1000006131611162D0600161315DE460E0717540E060855171315DE390307330F11042D0B041B3307110816FE012B011600131611162D0600161315DE180E0718540E060855171315DE0B260000DE00001613152B000011152A000000414C0000020000004201000016010000580200001400000000000000020000001A01000056010000700200001400000000000000000000004F000000BF0300000E04000005000000010000011B300300090300002C000011000E05166A55731500000A0B160A03130511051759450500000002000000060000000A0000000E000000060000002B0C170A2B0C180A2B08190A2B04160A2B000007166F6200000A000772177800706F1600000A260772E11000706F1600000A2607726B7800706F1600000A260772E57800706F1600000A2607725F7900706F1600000A260772D97900706F1600000A260772E11000706F1600000A260772537A00706F1600000A2607729F7A00706F1600000A260772EB7A00706F1600000A260772377B00706F1600000A260772E11000706F1600000A260772837B00706F1600000A260772E57B00706F1600000A260772477C00706F1600000A260772A97C00706F1600000A2607720B7D00706F1600000A2607726D7D00706F1600000A260772CF7D00706F1600000A260772317E00706F1600000A260772937E00706F1600000A260772F57E00706F1600000A260772577F00706F1600000A260772B97F00706F1600000A2607721B8000706F1600000A2607721B8000706F1600000A2607727D8000706F1600000A2607723C8100706F1600000A260772E11000706F1600000A260772A08100706F1600000A26076F1700000A0E066F1800000A0E06731900000A0D00096F1A00000A72420E0070166F1B00000A028C270000016F1C00000A00096F1A00000A72F28100701E6F1B00000A058C2D0000016F1C00000A00096F1A00000A720E8200701E6F1B00000A068C0E0000026F1C00000A00096F1A00000A72228200701F096F1B00000A168C2D0000016F1C00000A00096F1A00000A72448200701F196F1B00000A04283100000A2D03042B057E3F00000A006F1C00000A00096F1A00000A725A820070186F1B00000A0E041BFE018C430000016F1C00000A00096F1A00000A7227050070166F1B00000A0C08186F4C00000A00096F1D00000A17FE01130611062D0600161304DE50086F5E00000A7E3F00000AFE0116FE01130611062D0600161304DE340E05086F5E00000AA5270000015500DE120914FE01130611062D07096F1E00000A00DC00171304DE0B260000DE00001613042B000011042A0000004134000002000000C60100001C010000E202000012000000000000000000000040000000BA020000FA02000005000000010000011B300300860000002D00001100007274820070046F1800000A04731900000A0A00066F1A00000A72420E0070166F1B00000A038C270000016F1C00000A00066F1A00000A72358300701E6F1B00000A028C270000016F1C00000A00066F1D00000A17FE010C082D0500160BDE2200DE100614FE010C082D07066F1E00000A00DC00170BDE0A260000DE0000160B2B0000072A0000011C00000200140050640010000000000000010078790005010000011B300300F20300002E000011000E06156A55731500000A0A0E056F0D01000616731100000A282D00000A16FE01130711072D06001B0D002B04001C0D0016731100000A130402130811081F14302111084503000000380000003D0000004B00000011081F0A2E4A11081F142E492B5511081F1E2E3C110820E7030000594503000000360000002300000023000000110820F20300002E1A2B2B1F640C2B2E1F650C0E056F0D01000613042B201F660C2B1B1F6E0C2B161F780C0E056F0D01000613042B08161306382E030000000672578300706F1600000A260672AD8300706F1600000A260672058400706F1600000A2606724F8400706F1600000A260672AF8400706F1600000A2606720B8500706F1600000A260672658500706F1600000A260672C18500706F1600000A260672158600706F1600000A2606726B8600706F1600000A260672BB8600706F1600000A2606720D8700706F1600000A2606725B8700706F1600000A260672AB8700706F1600000A260672FD8700706F1600000A260672558800706F1600000A260672A18800706F1600000A260672DB8800706F1600000A2606722D8900706F1600000A260672818900706F1600000A260672C98900706F1600000A260672258A00706F1600000A2606727D8A00706F1600000A2606727D8A00706F1600000A2606727D8A00706F1600000A260672BB8A00706F1600000A2606727D8A00706F1600000A2606727D8A00706F1600000A2606720D8B00706F1600000A260672598B00706F1600000A260672598B00706F1600000A260672A78B00706F1600000A2606727D8A00706F1600000A260672A18800706F1600000A260672FB8B00706F1600000A26066F1700000A0E076F1800000A0E07731900000A13050011056F1A00000A72420E0070166F1B00000A038C270000016F1C00000A0011056F1A00000A720D0500701E6F1B00000A048C2D0000016F1C00000A0011056F1A00000A72D52000701E6F1B00000A088C0E0000026F1C00000A0011056F1A00000A722A6000701E6F1B00000A098C110000026F1C00000A0011056F1A00000A72E32000701F096F1B00000A0E046F0D0100068C080000016F1C00000A0011056F1A00000A72828C00701F096F1B00000A11048C080000016F1C00000A0011056F1A00000A72352100701F096F1B00000A0E046F0D0100060E056F0D010006282800000A8C080000016F1C00000A0011056F1A00000A725A820070186F1B00000A051BFE018C430000016F1C00000A0011056F1A00000A7227050070166F1B00000A0B07186F4C00000A0011056F1D00000A17FE01130711072D0600161306DE320E06076F5E00000AA5270000015500DE14110514FE01130711072D0811056F1E00000A00DC00171306DE072600161306DE000011062A000041340000020000007B02000052010000CD030000140000000000000000000000C000000027030000E703000007000000010000011B3003005C02000028000011000573F40000065100731500000A0A06729A8C00706F1600000A260219FE0116FE01130411042D34000672CA8C00706F1600000A260672C18D00706F1600000A2606721F8E00706F1600000A260672168F00706F1600000A26002B32000672CA8C00706F1600000A260672748F00706F1600000A2606721F8E00706F1600000A2606726B9000706F1600000A26000672629100706F1600000A260672AA9100706F1600000A260672FC9100706F1600000A2606727A9200706F1600000A260672F89200706F1600000A260672769300706F1600000A260672F49300706F1600000A26066F1700000A0E046F1800000A0E04731900000A0B00076F1A00000A7227050070166F1B00000A038C270000016F1C00000A00076F1A00000A7252940070166F1B00000A166A047B41040004287900000A8C270000016F1C00000A00076F1A00000A726E9400701F096F1B00000A16731100000A047B42040004282600000A8C080000016F1C00000A00076F1A00000A728C940070166F1B00000A166A047B43040004287900000A8C270000016F1C00000A00076F1A00000A72828C00701F096F1B00000A16731100000A047B44040004282600000A8C080000016F1C00000A00076F5F00000A0C00086F6000000A130411042D0500160DDE7D055008166F6E00000A6A7D41040004055008176F6100000A7D42040004055008186F6E00000A6A7D43040004055008196F6100000A7D44040004055016731100000A7D4504000400DE120814FE01130411042D07086F1E00000A00DC00170DDE1C0714FE01130411042D07076F1E00000A00DC260000DE0000160D2B0000092A414C000002000000CA0100005C00000026020000120000000000000002000000F7000000460100003D02000012000000000000000000000008000000470200004F02000005000000010000011B300800C60800002F000011000E047302010006510E04501772010000706FFE00000600057B380400041308057B3C040004130911092C0C057B31040004166AFE012B011700131111112D05001613080000057B32040004166A310F057B33040004166AFE0416FE012B011600131111112D38000E04501872A2940070057C32040004281F00000A72F4940070057C33040004281F00000A287400000A6FFE00000600161310DD23080000110816FE01131111112D0C00057B350400041305002B150016731100000A057B35040004282600000A1305001105057B35040004282900000A16FE01131111112D2C000E045072FA940070057C35040004282400000A72329500701205282400000A287400000A6FFF000006000003131211121759450500000002000000670000006500000067000000070000002B653887000000057B33040004166A2E08057B360400042B011600131111112D4200057B2F0400041F20FE01131111112D30000E0450723E950070057C33040004281F00000A7298950070057C36040004287500000A287400000A6FFF0000060000002B292B270E04501872B8950070038C190000026F1700000A286600000A6FFE00000600161310DD07070000057B2F0400041CFE0116FE01131111112D360003057B32040004057B37040004120A0E0528A7000006131111112D18000E04501772EC9500706FFE00000600161310DDC0060000000E05723A9600706F1200000A00110816FE01131111112D0F000528BF00000613060038B600000000057B3204000412060E0528BE000006131111112D18000E04501772669600706FFE00000600161310DD6A060000110916FE01131111112D4D001106177D6B0400041106057B3D0400047D6C0400041106057B370400047B420400047D620400041106057B370400047B440400047D630400041106057B3E04000473070100067D6D0400040011060E04507B5E0400040E0528BD000006131111112D18000E04501772A29600706FFE00000600161310DDE3050000001106130711067B6804000473070100060A0673070100060B031312111217594505000000020000003B0000000E0000003B0000000C0000002B39066F0D01000613052B562B54057B2F0400041F20FE0116FE01131111112D1800057B34040004131111112D0A00066F0D010006130500002B270E04501872B8950070038C190000026F1700000A286600000A6FFE00000600161310DD48050000066F0D0100061105282900000A0D090C0916FE01131111113A4B020000000673070100060B1105066F0D010006282700000A130B160C031B2E090317FE0116FE012B011600131111112D410072E0960070729D0700700E0528EC000006120C282A00000A26110C1F64731100000A282B00000A130C110B110C282D00000A16FE01131111112D0400170C00000319FE0116FE01131111112D46007283070070729D0700700E0528EC000006120C282A00000A26110C1F64731100000A282B00000A130C110B282C00000A110C282D00000A16FE01131111112D0400170C00007303010006130D110D057B320400047D5F040004110D11067B600400047D60040004110D0673070100067D68040004110B16731100000A282D00000A16FE01131111112D1A00110D16731100000A7D62040004110D110B7D63040004002B1D00110D110B282C00000A7D62040004110D16731100000A7D6304000400110816FE01131111112D110073060100060B7306010006130E002B4200110D0E04507B5E0400040E0528BD000006131111112D18000E04501772A29600706FFE00000600161310DDA1030000110D7B68040004130E110E73070100060B00031B2E090317FE0116FE012B011600131111112D430002057B320400040E0528EA000006131111112D2D000E04501772F09600700F00282100000A723E970070057C32040004281F00000A287400000A6FFE000006000000057B2F0400041F20FE0116FE01131111112D0600160D160C000816FE01131111112D3300110E6F0D010006066F0D010006282D00000A16FE01131111112D09000673070100060B000E05723A9600706F1300000A0000001108131111112D3C00057B32040004076F0D010006110509057B2F0400040E0528E4000006131111112D18000E04501772609700706FFE00000600161310DD9D0200000011092C37057B400400046F0D01000616731100000A282D00000A2D1B057B3F0400046F0D01000616731100000A282D00000A16FE012B0116002B011700131111112D3900057B32040004057B40040004057B3F0400040E0528A9000006131111112D18000E04501772AE9700706FFE00000600161310DD21020000000816FE01131111112D4C000E04501A72C5090070057B350400048C08000001066F1700000A076F1700000A282000000A6FFE000006000E04500773070100067D5C0400041108131111112D0900171310DDCB01000000730401000613041104057B2F0400047D6E0400041104057B300400047D6F0400041104057B320400047D700400041104057B310400047D710400041104027D720400041104047D730400041104097D7404000411040773070100067D7504000411040E04507B5E0400047D760400041104057B370400047B410400047D77040004110411077B690400047D780400041104057B370400047B430400047D79040004110411077B6A0400047D7A0400041104057B3B0400047D7E0400041104057B3C0400047D7F040004110816FE01131111113A93000000001104177D7B0400041104057B390400047D7C0400041104057B3A0400047D7D040004057B32040004057B3A0400047B48040004057B3A0400047B49040004057B3A0400047B47040004057B3A0400047B4A040004057B3A0400047B4B040004057B3A0400047B4C0400047B4F0400040E0528B0000006131111112D15000E04501772009800706FFE00000600161310DE610011040E04500E0528AB000006131111112D15000E045017724A9800706FFE00000600161310DE390E04500773070100067D5C0400040E04506F0001000600171310DE1D130F000E045017110F6F7600000A6FFE0000060000DE00001613102B000011102A0000411C0000000000004500000060080000A508000017000000370000011B300300620100002A00001100731500000A0A0672849800706F1600000A260672B29800706F1600000A260672149900706F1600000A260672789900706F1600000A260672E49900706F1600000A260672509A00706F1600000A2600066F1700000A056F1800000A05731900000A0B00076F1A00000A72B69A0070166F1B00000A028C270000016F1C00000A00076F1A00000A72D69A00701F096F1B00000A037B8004000416731100000A282600000A8C080000016F1C00000A00076F1A00000A72F49A00701F096F1B00000A036F0C01000616731100000A282600000A8C080000016F1C00000A00076F1A00000A72129B00701F096F1B00000A047B8004000416731100000A282600000A8C080000016F1C00000A00076F1A00000A72389B00701F096F1B00000A046F0C01000616731100000A282600000A8C080000016F1C00000A00076F1D00000A17FE010CDE1A0714FE010D092D07076F1E00000A00DC260000DE0000160C2B0000082A0000413400000200000063000000E2000000450100001000000000000000000000004F000000060100005501000005000000010000011B300300F90000003000001100031652160B00731500000A0A06725E9B00706F1600000A260672CE9B00706F1600000A2606723E9C00706F1600000A260672AE9C00706F1600000A2606721E9D00706F1600000A2606728E9D00706F1600000A26066F1700000A046F1800000A04731900000A0D00096F1A00000A72420E0070166F1B00000A028C270000016F1C00000A00096F1A00000A72FE9D00701E6F1B00000A1B8C0F0000026F1C00000A00096F4200000A0C00DE120914FE01130611062D07096F1E00000A00DC00082C0A087E3F00000AFE012B011700130611062D090008A52D0000010B00030716FE015200DE08130400161305DE06001713052B000011052A000000011C00000200680045AD00120000000000000600E1E70008370000011B300900770600003100001100027B7B040004130C027B7F0400042C0C027B71040004166AFE012B011700131611162D050016130C00110C131611162D1400027B71040004130D027B71040004130E002B2500027B71040004166A3308027B7C0400042B06027B7104000400130D027B7C040004130E00027B7F0400042C0C027B71040004166AFE012B011700131611163ACC00000000027B71040004130D027B7C040004130E110E166AFE0116FE01131611163AA8000000000072309E0070046F1800000A04731900000A13120011126F1A00000A720D0500701F0C1F326F2F00000A027B720400048C2D0000016F1C00000A0011126F4200000A131111112C0B11117E3F00000AFE012B011700131611162D0B001111A527000001130E0000DE14111214FE01131611162D0811126F1E00000A00DC0000DE24131300031772D19E007011136F1700000A286600000A6FFE00000600161315DD2005000000000073060100060A73060100060B027B72040004120A042835010006131611162D1600031772519F00706FFE0000060016131538E7040000110C131611162D3F00027B71040004027B7504000412010428C4000006131611162D1600031772899F00706FFE0000060016131538AE04000007027B7504000428110100060A00110D0428CC000006131611162D1600031772CA6300706FFE00000600161315387C040000110C131611162D6B0019027B71040004166A027B750400041202120312040428CF000006131611162D1600031772CB9F00706FFE00000600161315383C040000027B71040004027B70040004166A0428B3000006131611162D160003177211A000706FFE00000600161315380C040000002B4B00027B7D0400046FF5000006027B7D0400046FF6000006027B7D0400046FF700000673080100060C027B7D0400046FF900000616731100000A027B7D0400046FFA00000673080100060D00027B6E040004131711171C2E0811171F202E0F2B15171306027B7204000413092B321913061613092B2A0318725DA00070027B6E0400048C0D0000026F1700000A286600000A6FFE00000600161315386A030000027B7004000408027B78040004027B7A040004090428AF000006131611162D160003177291A000706FFE0000060016131538340300001106027B7004000412071208120B0428E3000006131611162D1600031772D3A000706FFE00000600161315380403000011072D07110816FE012B011700131611162D2A00027B700400040428E9000006131611162D16000317721BA100706FFE0000060016131538C802000000110E027B72040004027B700400040428A2000006131611162D16000317726BA100706FFE000006001613153897020000110C16FE01131611162D2C00027B70040004110D0428A5000006131611162D1600031772AFA100706FFE00000600161315386102000000027B780400046F0D01000616731100000A282D00000A16FE01131611163AE200000000110D0428B1000006131611162D1600031772EBA100706FFE000006001613153819020000110A027B780400040428AE000006131611162D160003177237A200706FFE0000060016131538EF010000027B6E0400041C3308027B740400042B011700131611162D7900110C16FE01131611162D4200027B70040004027B7D0400047B4A040004027B7D0400046FF50000060428B6000006131611162D16000317727FA200706FFE00000600161315388B010000002B2A00027B710400040428B5000006131611162D1600031772C7A200706FFE00000600161315385F0100000000001109027B73040004027B70040004027B6F04000473120000061305027B710400041314110C16FE01131611162D0A00027B7C0400041314001105166A1114027B6E040004066F0D01000616731100000A027B750400046F0D010006076F0D010006027B7E0400046F15000006131611162D300003177209A30070027B6E0400048C0D0000026F1700000A723FA30070283C00000A6FFE0000060016131538BA0000001105046F1D000006131611162D1600031772846300706FFE000006001613153896000000120F0428D3000006131611162D050016130F00110F16FE0116FE01131611162D7000027B780400046F0B01000616731100000A282D00000A16FE01131611162D4F00027B7104000412100428AA000006131611162D0500161310001110131611162D2C00110B0203110A7BC80400040428AD000006131611162D130003177253A300706FFE000006001613152B080000001713152B000011152A00011C00000200C20055170114000000000000AE00812F01243700000113300800B50100003200001100731500000A0D0312020E0428BE000006130C110C2D090016130B389201000008090E0428BD000006130C110C2D090016130B387A010000087B680400047307010006130411047307010006130511046F0D010006056F0D010006282900000A1306110616FE01130C110C2D0200000311056F0D010006056F0D01000611061C0E0428E4000006130C110C2D090016130B381C01000002110512000E0428C4000006130C110C2D090016130B380101000006110528110100060B1902166A110512081209120A0E0428CF000006130C110C2D090016130B38D60000000203166A0E0428B3000006130C110C2D090016130B38BC000000031108087B69040004087B6A04000411090E0428AF000006130C110C2D090016130B38950000000204030E0428A2000006130C110C2D060016130B2B7F087B690400046F0D01000616731100000A282D00000A16FE01130C110C2D5B00020E0428B1000006130C110C2D060016130B2B4B0412070E042835010006130C110C2D060016130B2B351107087B690400040E0428AE000006130C110C2D060016130B2B1A020E0428B5000006130C110C2D060016130B2B060017130B2B00110B2A0000001B300300640200002A0000110000731500000A0A067295A300706F1600000A26067205A400706F1600000A26067275A400706F1600000A260672E5A400706F1600000A26067255A500706F1600000A260672C5A500706F1600000A26067235A600706F1600000A260672A5A600706F1600000A26067215A700706F1600000A26067285A700706F1600000A260672F5A700706F1600000A26067265A800706F1600000A260672D5A800706F1600000A26067245A900706F1600000A260672B5A900706F1600000A26067225AA00706F1600000A26067295AA00706F1600000A26067205AB00706F1600000A26067275AB00706F1600000A260672E5AB00706F1600000A26067255AC00706F1600000A260672C5AC00706F1600000A26066F1700000A0E046F1800000A0E04731900000A0B00076F1A00000A7235AD0070166F1B00000A037B700400048C270000016F1C00000A00076F1A00000A7255AD0070166F1B00000A037B710400048C270000016F1C00000A00076F1A00000A7275AD00701F096F1B00000A037B780400046F0B0100068C080000016F1C00000A00076F1A00000A728FAD00701E6F1B00000A037B720400048C2D0000016F1C00000A00076F1A00000A72B1AD00701E6F1B00000A0F00287A00000A8C310000016F1C00000A00076F1A00000A72E9AD0070166F1B00000A037B770400048C270000016F1C00000A00076F1A00000A7215AE0070186F1B00000A037B740400048C430000016F1C00000A00076F1A00000A724FAE00701E6F1B00000A058C190000026F1C00000A00076F1D00000A2600DE100714FE010D092D07076F1E00000A00DC00170CDE0A260000DE0000160C2B0000082A4134000002000000250100001D0100004202000010000000000000000000000001000000560200005702000005000000010000011B300300C40000002A0000110000027BD20400040D092D0800170CDDAD000000731500000A0A067275AE00706F1600000A260672B7AE00706F1600000A260672B0AF00706F1600000A26066F1700000A046F1800000A04731900000A0B00076F1A00000A72E2AF00701F096F1B00000A036F0B0100068C080000016F1C00000A00076F1A00000A7212B000701F096F1B00000A036F0D0100068C080000016F1C00000A00076F1D00000A2600DE100714FE010D092D07076F1E00000A00DC00170CDE0A260000DE0000160C2B0000082A011C00000200510051A200100000000000000100B6B70005010000011B3003001A0300002A0000110000731500000A0A06166F6200000A0006722EB000706F1600000A26067260B000706F1600000A260672E3B000706F1600000A26067257B100706F1600000A260672C1B100706F1600000A26067235B200706F1600000A260672B8B200706F1600000A2606722EB300706F1600000A2606729AB300706F1600000A26067210B400706F1600000A26067290B400706F1600000A2606720AB500706F1600000A2606727AB500706F1600000A260672F4B500706F1600000A26067268B600706F1600000A260672D2B600706F1600000A26066F1700000A0E056F1800000A0E05731900000A0B00076F1A00000A7248B700701F096F1B00000A036F0C0100068C080000016F1C00000A00076F1A00000A7274B700701F096F1B00000A036F0B0100068C080000016F1C00000A00076F1A00000A7294B700701F096F1B00000A037B800400048C080000016F1C00000A00076F1A00000A72AAB700701F096F1B00000A037B810400048C080000016F1C00000A00076F1A00000A72CAB700701F096F1B00000A0E046F0C0100068C080000016F1C00000A00076F1A00000A72F8B700701F096F1B00000A0E046F0B0100068C080000016F1C00000A00076F1A00000A721AB800701F096F1B00000A0E047B800400048C080000016F1C00000A00076F1A00000A7232B800701F096F1B00000A0E047B810400048C080000016F1C00000A00076F1A00000A7254B800701F096F1B00000A046F0C0100068C080000016F1C00000A00076F1A00000A7280B800701F096F1B00000A046F0B0100068C080000016F1C00000A00076F1A00000A7212B000701F096F1B00000A046F0D0100068C080000016F1C00000A00076F1A00000A72A6B800701F096F1B00000A056F0C0100068C080000016F1C00000A00076F1A00000A72CCB800701F096F1B00000A056F0B0100068C080000016F1C00000A00076F1A00000A72ECB800701F096F1B00000A056F0D0100068C080000016F1C00000A00076F1A00000A7235830070166F1B00000A028C270000016F1C00000A00076F1D00000A17FE010CDE1A0714FE010D092D07076F1E00000A00DC260000DE0000160C2B0000082A00004134000002000000E500000018020000FD020000100000000000000000000000010000000C0300000D03000005000000010000011B300300830100002A0000110000731500000A0A06166F6200000A0006722EB000706F1600000A26067202B900706F1600000A2606726EB900706F1600000A260672DAB900706F1600000A2606723CBA00706F1600000A260672AABA00706F1600000A2606720EBB00706F1600000A26067274BB00706F1600000A26066F1700000A0E076F1800000A0E07731900000A0B00076F1A00000A72DEBB00701F096F1B00000A038C080000016F1C00000A00076F1A00000A7202BC00701F096F1B00000A048C080000016F1C00000A00076F1A00000A7226BC00701F096F1B00000A058C080000016F1C00000A00076F1A00000A7240BC00701F096F1B00000A0E058C080000016F1C00000A00076F1A00000A7266BC00701F096F1B00000A0E048C080000016F1C00000A00076F1A00000A7282BC00701F096F1B00000A0E068C080000016F1C00000A00076F1A00000A7235830070166F1B00000A028C270000016F1C00000A00076F1D00000A17FE010CDE1A0714FE010D092D07076F1E00000A00DC260000DE0000160C2B0000082A00413400000200000085000000E10000006601000010000000000000000000000001000000750100007601000005000000010000011B300300860000002A0000110000731500000A0A0672A0BC00706F1600000A260672C8BC00706F1600000A26067224BD00706F1600000A26066F1700000A036F1800000A03731900000A0B00076F1A00000A72420E0070166F1B00000A028C270000016F1C00000A00076F1D00000A17FE010CDE1A0714FE010D092D07076F1E00000A00DC260000DE0000160C2B0000082A0000011C000002003F002A690010000000000000010078790005010000011B300500D602000028000011000416731100000A81080000010516731100000A81080000010E04280E0100065100731500000A0A067292BD00706F1600000A26067217BE00706F1600000A2606729CBE00706F1600000A26067221BF00706F1600000A260672A6BF00706F1600000A26066F7B00000A260672A0BC00706F1600000A2606722FC000706F1600000A2606727FC000706F1600000A260672CFC000706F1600000A2606721FC100706F1600000A2606726FC100706F1600000A260672BFC100706F1600000A2606720FC200706F1600000A2606726BC200706F1600000A260672C7C200706F1600000A26067223C300706F1600000A2606727FC300706F1600000A260672DBC300706F1600000A26067237C400706F1600000A26067293C400706F1600000A260672EFC400706F1600000A2606724BC500706F1600000A260672BBC500706F1600000A2606722BC600706F1600000A2606729BC600706F1600000A2606720BC700706F1600000A2606725BC700706F1600000A260672A5C700706F1600000A260672FDC700706F1600000A26067261C800706F1600000A260672C5C800706F1600000A260672C97700706F1600000A2603166AFE01130411042D0E000672F5C800706F1600000A2600067265C900706F1600000A26067291C900706F1600000A26066F1700000A0E056F1800000A0E05731900000A0B00076F1A00000A72420E0070166F1B00000A028C270000016F1C00000A00076F1A00000A7227050070166F1B00000A038C270000016F1C00000A00076F5F00000A0C00086F6000000A130411042D0800160DDD870000000408166F6100000A81080000010508176F6100000A81080000010E0408186F6100000A08196F6100000A081A6F6100000A73080100065100DE120814FE01130411042D07086F1E00000A00DC0000DE120714FE01130411042D07076F1E00000A00DC000203166A0E0528B3000006130411042D0500160DDE0E170DDE0A260000DE0000160D2B0000092A0000414C000002000000370200004F00000086020000120000000000000002000000F5010000A70000009C02000012000000000000000000000021000000A8020000C902000005000000010000011B3003001B030000330000110000731500000A0A0672545800706F1600000A260672CDC900706F1600000A26067250CA00706F1600000A260672355100706F1600000A2603166AFE01130711072D29000672ACCA00706F1600000A2604166AFE01130711072D0E0006720ECB00706F1600000A2600002B0E00067270CB00706F1600000A2600066F1700000A056F1800000A05731900000A0C00086F1A00000A72420E0070166F1B00000A028C270000016F1C00000A00086F1A00000A721C5900701E6F1B00000A188C0A0000026F1C00000A00086F1A00000A7227050070166F1B00000A038C270000016F1C00000A00086F1A00000A72C6CB0070166F1B00000A048C270000016F1C00000A0008736700000A0D00733200000A0B09076F6800000A2600DE120914FE01130711072D07096F1E00000A00DC0000DE120814FE01130711072D07086F1E00000A00DC00076F4000000A6F4300000A16FE0116FE01130711072D0900171306DDB301000006166F6200000A000672A05900706F1600000A260672E6CB00706F1600000A26067232CC00706F1600000A260672765A00706F1600000A2606727ECC00706F1600000A260672D6CC00706F1600000A26067230CD00706F1600000A2603166AFE01130711072D27000672ACCA00706F1600000A2604166AFE01130711072D0E0006720ECB00706F1600000A260000066F1700000A056F1800000A05731900000A0C00086F1A00000A72695B0070166F1B00000A1304086F1A00000A72420E0070166F1B00000A028C270000016F1C00000A00086F1A00000A721C5900701E6F1B00000A188C0A0000026F1C00000A00086F1A00000A7227050070166F1B00000A038C270000016F1C00000A00086F1A00000A72C6CB0070166F1B00000A048C270000016F1C00000A0000076F4000000A6F4800000A13082B2711086F4900000A740B00000113050011041105166F6900000A6F1C00000A00086F1D00000A260011086F4A00000A130711072DCCDE1D110875050000011309110914FE01130711072D0811096F1E00000A00DC0000DE120814FE01130711072D07086F1E00000A00DC00171306DE0B260000DE00001613062B000011062A00417C00000200000009010000120000001B0100001200000000000000020000008D000000A4000000310100001200000000000000020000009B02000038000000D30200001D000000000000000200000005020000EF000000F4020000120000000000000000000000010000000B0300000C03000005000000010000011B3003003F020000330000110000731500000A0A0672545800706F1600000A260672CDC900706F1600000A26067290CD00706F1600000A260672E2CD00706F1600000A2606723ACE00706F1600000A26066F1700000A036F1800000A03731900000A0C00086F1A00000A72420E0070166F1B00000A028C270000016F1C00000A00086F1A00000A721C5900701E6F1B00000A188C0A0000026F1C00000A0008736700000A0D00733200000A0B09076F6800000A2600DE120914FE01130711072D07096F1E00000A00DC0000DE120814FE01130711072D07086F1E00000A00DC00076F4000000A6F4300000A16FE0116FE01130711072D0900171306DD4701000006166F6200000A000672A05900706F1600000A26067278CE00706F1600000A260672765A00706F1600000A2606724BCF00706F1600000A2606729BCF00706F1600000A260672E2CD00706F1600000A2606723ACE00706F1600000A26066F1700000A036F1800000A03731900000A0C00086F1A00000A72EDCF0070166F1B00000A1304086F1A00000A72420E0070166F1B00000A028C270000016F1C00000A00086F1A00000A721C5900701E6F1B00000A188C0A0000026F1C00000A0000076F4000000A6F4800000A13082B2711086F4900000A740B00000113050011041105166F6900000A6F1C00000A00086F1D00000A260011086F4A00000A130711072DCCDE1D110875050000011309110914FE01130711072D0811096F1E00000A00DC0000DE120814FE01130711072D07086F1E00000A00DC00171306DE0B260000DE00001613062B000011062A00417C0000020000009900000012000000AB000000120000000000000002000000570000006A000000C1000000120000000000000002000000BF01000038000000F70100001D000000000000000200000063010000B500000018020000120000000000000000000000010000002F0200003002000005000000010000011B300400670300003400001100160C733200000A0B150D00731500000A0A0672545800706F1600000A260672CDC900706F1600000A26067290CD00706F1600000A260672E2CD00706F1600000A260672F05600706F1600000A26067201D000706F1600000A26066F1700000A036F1800000A03731900000A13040011046F1A00000A72420E0070166F1B00000A028C270000016F1C00000A0011046F1A00000A723A5900701E6F1B00000A178C090000026F1C00000A0011046F1A00000A725A5900701E6F1B00000A188C090000026F1C00000A0011046F1A00000A721C5900701E6F1B00000A188C0A0000026F1C00000A0011046F1A00000A7284D000701E6F1B00000A198C0A0000026F1C00000A001104736700000A1305001105076F6800000A2600DE14110514FE01130911092D0811056F1E00000A00DC0000DE14110414FE01130911092D0811046F1E00000A00DC00160D076F4000000A6F4300000A16FE0116FE01130911092D0900171308DDF901000006166F6200000A000672A05900706F1600000A260672A8D000706F1600000A260672765A00706F1600000A2606724BCF00706F1600000A2606729BCF00706F1600000A260672E2CD00706F1600000A26067204D100706F1600000A260672F05600706F1600000A26067201D000706F1600000A26066F1700000A036F1800000A03731900000A13040011046F1A00000A72695B0070166F1B00000A130611046F1A00000A72420E0070166F1B00000A028C270000016F1C00000A0011046F1A00000A723A5900701E6F1B00000A178C090000026F1C00000A0011046F1A00000A725A5900701E6F1B00000A188C090000026F1C00000A0011046F1A00000A721C5900701E6F1B00000A188C0A0000026F1C00000A0011046F1A00000A7284D000701E6F1B00000A198C0A0000026F1C00000A0000076F4000000A6F4800000A130A2B2A110A6F4900000A740B00000113070011061107166F6900000A6F1C00000A000911046F1D00000A580D00110A6F4A00000A130911092DC9DE1D110A7505000001130B110B14FE01130911092D08110B6F1E00000A00DC0000DE14110414FE01130911092D0811046F1E00000A00DC00171308DE422600170C00DE0000DE3200082D13076F4000000A6F4300000A09FE0116FE012B011700130911092D1100021616731100000A0328E8000006260000DC001613082B000011082A0041940000020000000E0100000D0000001B0100001400000000000000020000006E000000C500000033010000140000000000000002000000AB0200003B000000E60200001D0000000000000002000000F201000015010000070300001400000000000000000000000B00000016030000210300000700000001000001020000000B000000200300002B03000032000000000000001B300300050100003500001100166A0C00731500000A0A067242D100706F1600000A26067292D100706F1600000A260672BCD100706F1600000A26067232D200706F1600000A260672A2D200706F1600000A26066F1700000A056F1800000A05731900000A0D00096F1A00000A7227050070166F1B00000A028C270000016F1C00000A00096F1A00000A72F6D200701E6F1B00000A188C2D0000016F1C00000A00096F4200000A0B072C0A077E3F00000AFE012B011700130511052D090007A5270000010C0000DE120914FE01130511052D07096F1E00000A00DC0008166AFE0116FE01130511052D0600171304DE1D0304282700000A080528B70000061304DE0B260000DE00001613042B000011042A000000011C000002005A0063BD00120000000000000400F2F60005010000011B300300CC0000002A00001100731500000A0A067210D300706F1600000A2606724ED300706F1600000A260672E3D300706F1600000A26067251D400706F1600000A260672F4D400706F1600000A2600066F1700000A046F1800000A04731900000A0B00076F1A00000A7256D50070166F1B00000A038C270000016F1C00000A00076F1A00000A727F0500701F096F1B00000A028C080000016F1C00000A00076F1D00000A17FE0116FE010D092D0500170CDE2100DE100714FE010D092D07076F1E00000A00DC0000DE05260000DE0000160C2B0000082A011C00000200570054AB001000000000000043007CBF00050100000113300900180000003600001100020304050E04160E0512000E0628BA0000060B2B00072A1B300300760100002A0000110002165400731500000A0A067280D500706F1600000A26067227D600706F1600000A260672CED600706F1600000A26067275D700706F1600000A2606721CD800706F1600000A260672C3D800706F1600000A2606726AD900706F1600000A26067227D600706F1600000A26067211DA00706F1600000A260672CED600706F1600000A26067275D700706F1600000A2606721CD800706F1600000A260672B8DA00706F1600000A2606725FDB00706F1600000A26067206DC00706F1600000A26067227D600706F1600000A26067211DA00706F1600000A260672ADDC00706F1600000A26067254DD00706F1600000A260672FBDD00706F1600000A26067227D600706F1600000A260672A2DE00706F1600000A26066F1700000A046F1800000A04731900000A0B00076F1A00000A72420E0070166F1B00000A038C270000016F1C00000A0002076F4200000AA52D0000015400DE100714FE010D092D07076F1E00000A00DC00170CDE0A260000DE0000160C2B0000082A00004134000002000000260100002E0000005401000010000000000000000000000004000000650100006901000005000000010000011B3004001F07000037000011000E07733200000A510E0616731100000A0E047B80040004282600000A16731100000A0E047B81040004282600000A16731100000A7308010006511206020E0828B900000616FE01130C110C2D38001106172E0A110618FE0116FE012B011600130C110C2D1F000E060E046F0F0100067307010006510E065016731100000A7D82040004000004210000000000000080FE01130C110C2D26000E047B8204000416731100000A287200000A16FE01130C110C2D090017130B385E0600000072010000701304160D731500000A0A0006166F6200000A00067249DF00706F1600000A2606726DDF00706F1600000A260672A7DF00706F1600000A26067225E000706F1600000A26066F1700000A0E086F1800000A0E08731900000A13070011076F1A00000A720D050070166F1B00000A038C270000016F1C00000A0011076F5F00000A13080011086F6000000A130C110C2D090016130BDDC00500001108166F6B00000A13041108176F7800000A0D00DE14110814FE01130C110C2D0811086F1E00000A00DC0000DE14110714FE01130C110C2D0811076F1E00000A00DC000916FE01130C110C2D090017130BDD6A0500000E07506F3300000A7275E0007072950B0070283400000A6F3500000A260E07506F3300000A7291E0007072FB0B0070283400000A6F3500000A260E07506F3300000A72B1E00070723B0C0070283400000A6F3500000A2606166F6200000A000672545800706F1600000A260672C9E000706F1600000A26067254E100706F1600000A260672B3E200706F1600000A260672E9E200706F1600000A260672CDC900706F1600000A26067217E300706F1600000A26067250CA00706F1600000A260672355100706F1600000A2606729AE300706F1600000A26067208E400706F1600000A260672F05600706F1600000A260672A1E400706F1600000A26066F1700000A0E086F1800000A0E08731900000A13070011076F1A00000A72420E0070166F1B00000A028C270000016F1C00000A0011076F1A00000A721C5900701E6F1B00000A188C0A0000026F1C00000A0011076F1A00000A72DDE400701E6F1B00000A178C090000026F1C00000A0011076F1A00000A72F1E400701E6F1B00000A188C090000026F1C00000A0011076F1A00000A720D0500701E6F1B00000A038C270000016F1C00000A0011076F1A00000A7205E500701E6F1B00000A198C2E0000026F1C00000A0011076F5F00000A1308002B5F001108176F6E00000A16FE0116FE01130C110C2D03002B470E07506F3D00000A13051105161108166F6D00000A8C270000016F7C00000A001105181108186F6100000A8C080000016F7C00000A000E07506F4000000A11056F4100000A000011086F6000000A130C110C2D9400DE14110814FE01130C110C2D0811086F1E00000A00DC0000DE14110714FE01130C110C2D0811076F1E00000A00DC0006166F6200000A00067233E500706F1600000A2606726DE500706F1600000A260672CDE500706F1600000A2606722DE600706F1600000A26067271E600706F1600000A260672B5E600706F1600000A26067235E700706F1600000A2606728BE700706F1600000A260672E3E700706F1600000A26067241E800706F1600000A260672ADE800706F1600000A26067244E900706F1600000A26066F1700000A0E086F1800000A0E08731900000A13070011076F1A00000A72695B0070166F1B00000A130911076F1A00000A72420E0070166F1B00000A028C270000016F1C00000A0011076F1A00000A721C5900701E6F1B00000A188C0A0000026F1C00000A0011076F1A00000A72DDE400701E6F1B00000A178C090000026F1C00000A0011076F1A00000A72F1E400701E6F1B00000A188C090000026F1C00000A000E0516FE01130C110C2D3E0011076F1A00000A7227050070166F1B00000A7E3F00000A6F1C00000A0011076F1A00000A72C6CB0070166F1B00000A7E3F00000A6F1C00000A00002B3E0011076F1A00000A7227050070166F1B00000A048C270000016F1C00000A0011076F1A00000A72C6CB0070166F1B00000A058C270000016F1C00000A0000000E07506F4000000A6F4800000A130D38A5000000110D6F4900000A740B000001130A001109110A166F6900000A6F1C00000A0011076F5F00000A13080011086F6000000A130C110C2D0300DE6C1108166F6E00000A0B1108176F6100000A0C16731100000A08282600000A0C07130E110E4504000000180000000200000002000000180000002B160E0650257B8204000408282800000A7D820400042B022B0000DE14110814FE01130C110C2D0811086F1E00000A00DC000000110D6F4A00000A130C110C3A4AFFFFFFDE1D110D7505000001130F110F14FE01130C110C2D08110F6F1E00000A00DC0000DE14110714FE01130C110C2D0811076F1E00000A00DC000E065016731100000A0E06507B820400040E047B82040004282E00000A282600000A7D8204000417130BDE0B260000DE000016130B2B0000110B2A0041C4000002000000440100002D000000710100001400000000000000020000001C0100006D000000890100001400000000000000020000008003000072000000F2030000140000000000000002000000C2020000480100000A0400001400000000000000020000001E0600006700000085060000140000000000000002000000F1050000BD000000AE0600001D0000000000000002000000CD04000002020000CF060000140000000000000000000000CD0000004306000010070000050000000100000113300300500000003800001100731500000A0B03280E0100065102166AFE0116FE010D092D0500160C2B2F0212000428BE0000060D092D0500160C2B1D06070428BD0000060D092D0500160C2B0C03067B6804000451170C2B00082A1B30030081000000390000110002165400731500000A0A06166F6200000A00067290E900706F1600000A26066F1700000A036F1800000A03731900000A0B0002076F4200000AA52D0000015400DE100714FE010D092D07076F1E00000A00DC00024A13041104450200000002000000020000002B022B050216542B00170CDE0A260000DE0000160C2B0000082A000000011C00000200320011430010000000000000040070740005010000011B300600500B00003A00001100027B6204000416731100000A282900000A2D13027B6304000416731100000A282900000A2B0117000D027B6B04000416FE01131011102D3E00092D18027B6D0400047B8204000416731100000A282D00000A2B0117000D092D18027B6D0400047B8104000416731100000A282D00000A2B0117000D0000731500000A0A0916FE01131011103A290700000012020428BC000006131011102D15000372EFEA00706F1600000A2616130FDD9D0A0000027B6B04000416FE01131011102D0400170C0006166F6200000A0006722DEB00706F1600000A2606725FEB00706F1600000A26067293EB00706F1600000A260672C5EB00706F1600000A260672F5EB00706F1600000A2606721FEC00706F1600000A26067259EC00706F1600000A2606728FEC00706F1600000A26067218ED00706F1600000A26067276ED00706F1600000A260672DAED00706F1600000A26067240EE00706F1600000A26066F1700000A046F1800000A04731900000A13040011046F1A00000A7227050070166F1B00000A027B5F0400048C270000016F1C00000A0011046F1A00000A72420E0070166F1B00000A027B600400048C270000016F1C00000A0011046F1A00000A721C5900701E6F1B00000A188C2D0000016F1C00000A0002733200000A7D6104000411046F5F00000A130500027B6104000411056F7D00000A0000DE14110514FE01131011102D0811056F1E00000A00DC0000DE14110414FE01131011102D0811046F1E00000A00DC0008131111114502000000050000005A010000389E01000016731100000A1308027B680400046F0D01000616731100000A282D00000A2C1B027B680400046F0C01000616731100000A282D00000A16FE012B011700131011102D1F00027B680400046F0C010006027B680400046F0D010006282B00000A130800170228E100000600180228DE000006000228E20000060016731100000A027B62040004027B63040004282E00000A282600000A130911091108287000000A1817287E00000A1309110916731100000A282D00000A16FE01131011102D6600027B620400041109282700000A1306027B630400041109282700000A13070211097D620400040211097D63040004021728DC00000600170228DE0000060002027B620400041106282800000A7D6204000402027B630400041107282800000A7D6304000400021628DC000006000228DD00000600170228E100000600180228DE000006000228E200000600170228DE000006002B680228DD00000600180228DE00000600170228DE000006000228E200000600027B6B04000416FE01131011102D1A00180228DF00000600170228DF00000600190228E000000600002B1F03727EEE0070088C5D000002287F00000A6F1600000A2616130FDD520700000216731100000A027B62040004282600000A7D620400040216731100000A027B63040004282600000A7D63040004027B61040004720100007072010000701F106F8000000A0B078E6916FE0216FE01131011103A610300000006166F6200000A00067233E500706F1600000A260672B6EE00706F1600000A2606720AEF00706F1600000A26067256EF00706F1600000A260672B5E600706F1600000A26067235E700706F1600000A2606728BE700706F1600000A260672E3E700706F1600000A260672A8EF00706F1600000A26734E00000A130A00110A066F1700000A046F1800000A04731900000A6F8100000A00110A6F8200000A6F1A00000A7208F000701F096F1B00000A72B1E000706F4B00000A00110A6F8200000A6F1A00000A721CF000701F096F1B00000A7228F000706F4B00000A00110A6F8200000A6F1A00000A7238F000701F096F1B00000A724AF000706F4B00000A00110A6F8200000A6F1A00000A72695B0070166F1B00000A7275E000706F4B00000A00110A6F8200000A6F1A00000A72420E0070166F1B00000A027B600400048C270000016F1C00000A00110A6F8200000A6F1A00000A721C5900701E6F1B00000A188C2D0000016F1C00000A00110A6F8200000A6F1A00000A7227050070166F1B00000A027B5F0400048C270000016F1C00000A00110A6F8200000A166F4D00000A00110A176F6300000A00110A176F8300000A00110A076F8400000A130B110B078E69FE01131011103A7901000000037260F00070110B8C2D000001078E698C2D000001282200000A6F1600000A260007131216131338A4000000111211139A130C000372C8F000701B8D010000011314111416110C7275E000706F4500000AA2111417110C7291E000706F4500000AA2111418110C72B1E000706F4500000AA2111419110C724AF000706F4500000AA211141A110C7228F000706F4500000AA21114282500000A6F1600000A26110C6F8500000A16FE01131011102D1A00037265F10070110C6F8600000A286600000A6F1600000A260000111317581313111311128E69FE04131011103A4BFFFFFF037275F10070110A6F8200000A6F8700000A286600000A6F1600000A2600110A6F8200000A6F1A00000A6F8800000A13152B2F11156F4900000A742B000001130D0003729DF10070110D6F8900000A110D6F5E00000A282200000A6F1600000A260011156F4A00000A131011102DC4DE1D111575050000011316111614FE01131011102D0811166F1E00000A00DC0016130FDDB203000000DE14110A14FE01131011102D08110A6F1E00000A00DC000000027B6B04000416FE01131011102D0E0002027B6C0400047D680400040006166F6200000A000672C5F100706F1600000A2606723BF200706F1600000A260672B1F200706F1600000A26067227F300706F1600000A26066F7B00000A260672A0BC00706F1600000A260672A3F300706F1600000A26067230F400706F1600000A260672BDF400706F1600000A2606724AF500706F1600000A260672FFF500706F1600000A260672B4F600706F1600000A26067263F700706F1600000A26067212F800706F1600000A260672C3F800706F1600000A2606726EF900706F1600000A260672C8F900706F1600000A26067222FA00706F1600000A2606727CFA00706F1600000A260672C5C800706F1600000A260672D6FA00706F1600000A260672C97700706F1600000A26067265C900706F1600000A26067291C900706F1600000A26066F1700000A046F1800000A04731900000A13040011046F1A00000A72420E0070166F1B00000A027B600400048C270000016F1C00000A0011046F1A00000A72FEFA00701F096F1B00000A027B680400047B800400048C080000016F1C00000A0011046F1A00000A7228FB00701F096F1B00000A027B680400047B810400048C080000016F1C00000A0011046F1A00000A725CFB00701F096F1B00000A027B680400047B820400048C080000016F1C00000A0011046F1A00000A7290FB00701F096F1B00000A027B640400048C080000016F1C00000A0011046F1A00000A72B8FB00701F096F1B00000A027B650400048C080000016F1C00000A0011046F1A00000A72E0FB00701F096F1B00000A027B660400048C080000016F1C00000A0011046F1A00000A7202FC00701F096F1B00000A027B670400048C080000016F1C00000A0011046F1A00000A7224FC00701F096F1B00000A027B620400048C080000016F1C00000A0011046F1A00000A7248FC00701F096F1B00000A027B630400048C080000016F1C00000A0011046F5F00000A13050011056F6000000A131011102D2500037266FC0070027B600400048C27000001287F00000A6F1600000A2616130FDD91000000021105166F6100000A16731100000A1105176F6100000A73080100067D69040004021105186F6100000A16731100000A1105196F6100000A73080100067D6A04000400DE14110514FE01131011102D0811056F1E00000A00DC0000DE14110414FE01131011102D0811046F1E00000A00DC0017130FDE1A130E0003110E6F7600000A6F1600000A2600DE000016130F2B0000110F2A41AC000002000000E701000012000000F90100001400000000000000020000006E010000A3000000110200001400000000000000020000003407000040000000740700001D0000000000000002000000CE040000CF0200009D070000140000000000000002000000880A000078000000000B00001400000000000000020000000809000010020000180B000014000000000000000000000077000000BB0A0000320B000014000000370000011B300500E9010000280000110003730301000651731500000A0A0672BCFC00706F1600000A26067241FD00706F1600000A2606729CBE00706F1600000A26067221BF00706F1600000A260672C6FD00706F1600000A2606724BFE00706F1600000A26066F7B00000A260672A0BC00706F1600000A260672D6FE00706F1600000A260672EBFF00706F1600000A260672000101706F1600000A260672B10101706F1600000A260672A5C700706F1600000A260672FDC700706F1600000A26067261C800706F1600000A260672620201706F1600000A260672C5C800706F1600000A260672A40201706F1600000A260672690301706F1600000A26066F7B00000A26067265C900706F1600000A26067291C900706F1600000A2600066F1700000A046F1800000A04731900000A0B00076F1A00000A7227050070166F1B00000A028C270000016F1C00000A00076F5F00000A0C00086F6000000A130411042D0800160DDD8C00000003730301000651035008166F6100000A7D62040004035008176F6100000A7D63040004035008186F6100000A08196F6100000A081A6F6100000A73080100067D680400040350081B6F6D00000A7D600400040350027D5F040004170DDE2E0814FE01130411042D07086F1E00000A00DC0714FE01130411042D07076F1E00000A00DC260000DE0000160D2B0000092A000000012800000200450173B801120000000002002001AACA01120000000000000C01D0DC01050100000113300400170100003B00001100027B3A0400046FF50000060B027B3A0400046FF60000060C027B3A0400046FF70000060D027B3A0400046FFA000006130473030100060A06027B370400047B420400047D6204000406027B370400047B440400047D6304000406027B310400047D6004000406067B62040004091104282700000A16731100000A282600000A282E00000A7D6504000406067B62040004067B65040004282700000A7D6404000406027B370400047B440400047D660400040616731100000A7D6704000406067B6404000416731100000A067B6504000473080100067D6904000406067B6604000416731100000A067B6704000473080100067D6A04000406067B69040004067B6A04000428110100067D680400040613052B0011052A00133008001A0000003C0000110002280E01000603120004120212010528C80000060D2B00092A0000133009001C0000003C0000110002280E0100060304120005120212010E0428C90000060D2B00092A13300800170000003D00001100020304050E04120112000E0528C80000060C2B00082A00133008004D0000003E000011000E0414510E0516731100000A8108000001027E6F00000A0304160512000E0628CB00000616FE010C082D1B000E04067B9A040004510E05067B9B0400048108000001170B2B04160B2B00072A000000133008001B0000003F00001100020316731100000A041201120212000528C80000060D2B00092A00133008001B0000004000001100020316731100000A0412000512010E0428C80000060C2B00082A00133008001B0000004100001100020316731100000A04050E0412000E0528C80000060B2B00072A00133008001B0000004200001100020316731100000A041200050E040E0528C80000060B2B00072A00133007006A0000003E0000110005280E010006510E0416731100000A81080000010E05166A550E06280E01000651027E6F00000A03041612000E0728CA0000060C082D0500160B2B2B05067B9A040004510E04067B9B04000481080000010E05067B93040004550E06067B9904000451170B2B00072A0000133007006C0000003E000011000E04280E010006510E0516731100000A81080000010E06166A550E07280E01000651027E6F00000A03040512000E0828CA0000060C082D0500160B2B2C0E04067B9A040004510E05067B9B04000481080000010E06067B93040004550E07067B9904000451170B2B00072A13300800160000000900001100020304050E04160E050E0628CB0000060A2B00062A00001B300900620B00004300001100170C190D1A130472010000700B0E067324010006510E051A5F16FE0116FE0113070E051E5F16FE0116FE011308725A0E0070726E0E00700E0728EC0000061205284700000A26110517FE011306731500000A0A0672CF0301706F1600000A260672170401706F1600000A2606727B0401706F1600000A260672C30401706F1600000A2606720B0501706F1600000A260672530501706F1600000A2606729B0501706F1600000A260672F70501706F1600000A260672430601706F1600000A2606728F0601706F1600000A260672DB0601706F1600000A260672270701706F1600000A260672850701706F1600000A260672DD0701706F1600000A260672570801706F1600000A260672CF0801706F1600000A260672470901706F1600000A260672BF0901706F1600000A260672230A01706F1600000A260672830A01706F1600000A260672E30A01706F1600000A260672430B01706F1600000A260672A30B01706F1600000A26066F7B00000A260672FF0B01706F1600000A2606723D0C01706F1600000A260672930C01706F1600000A260672AF0C01706F1600000A2602166AFE01130D110D2D10000672780D01706F1600000A26002B1A000672CA0D01706F1600000A2606725B0E01706F1600000A260006721E0F01706F1600000A260672920F01706F1600000A260672061001706F1600000A260516731100000A282300000A2C040E042B011700130D110D2D0E0006727A1001706F1600000A26000672EE1001706F1600000A260672661101706F1600000A260672DE1101706F1600000A2606725A1201706F1600000A260672B21201706F1600000A260672281301706F1600000A2606728A1301706F1600000A260672EC1301706F1600000A2606724E1401706F1600000A260672B01401706F1600000A260672121501706F1600000A260672741501706F1600000A260672D61501706F1600000A260672381601706F1600000A2606729A1601706F1600000A260672121501706F1600000A260672FC1601706F1600000A260672A0BC00706F1600000A2606725E1701706F1600000A2606727F1801706F1600000A2606720A1901706F1600000A2606729F1901706F1600000A261106130D110D2D5F000672341A01706F1600000A2606724F1B01706F1600000A2606726A1C01706F1600000A260672851D01706F1600000A26047B8604000416FE01130D110D2D10000672A01E01706F1600000A26002B0E000672021F01706F1600000A2600000672681F01706F1600000A260672AC1F01706F1600000A260672F61F01706F1600000A260672402001706F1600000A2606728A2001706F1600000A260672EA2001706F1600000A2606724A2101706F1600000A2606729A2101706F1600000A260672EA2101706F1600000A2606723A2201706F1600000A260672842201706F1600000A260672E22201706F1600000A260672402301706F1600000A260672BA2301706F1600000A2606721C2401706F1600000A2606728A2401706F1600000A260672F82401706F1600000A261106130D110D2D1C0006726E2501706F1600000A260672CE2501706F1600000A26002B1A0006722E2601706F1600000A2606722E2601706F1600000A2600110716FE01130D110D2D10000672482601706F1600000A26002B0E0006722E2601706F1600000A2600110816FE01130D110D2D10000672922601706F1600000A26002B0E000672D82601706F1600000A26000672EE2601706F1600000A260672C5C800706F1600000A260672262701706F1600000A260672522701706F1600000A26110716FE01130D110D2D0E000672412801706F1600000A2600110816FE01130D110D2D0E000672302901706F1600000A26000672232A01706F1600000A26066F7B00000A26067265C900706F1600000A26067291C900706F1600000A2600066F1700000A0E076F1800000A0E07731900000A13090002166AFE0116FE01130D110D2D4C00036F8A00000A1F141F306F8B00000A0B11096F1A00000A729B2A01701F0C1F326F2F00000A036F1C00000A0011096F1A00000A72BF2A01701F0C1F326F2F00000A076F1C00000A00002B200011096F1A00000A72420E0070166F1B00000A028C270000016F1C00000A000011096F1A00000A72DD2A01701F096F1B00000A047B800400048C080000016F1C00000A0011096F1A00000A72F52A01701F096F1B00000A047B810400048C080000016F1C00000A0011096F1A00000A72172B01701F096F1B00000A047B820400048C080000016F1C00000A0011096F1A00000A72392B01701F096F1B00000A058C080000016F1C00000A001106130D110D2D490011096F1A00000A724B2B01701F096F1B00000A047B830400048C080000016F1C00000A0011096F1A00000A72612B0170186F1B00000A047B850400048C430000016F1C00000A000011096F1A00000A727F2B01701E6F1B00000A088C2D0000016F1C00000A00110716FE01130D110D2D200011096F1A00000A729B2B01701E6F1B00000A098C2D0000016F1C00000A0000110816FE01130D110D2D210011096F1A00000A72B72B01701E6F1B00000A11048C2D0000016F1C00000A000011096F5F00000A130A00110A6F6000000A130D110D2D580002166AFE0116FE01130D110D2D21000E065072D32B0170036F8A00000A72FD2B0170283C00000A7D9C040004002B20000E0650722F2C01700F00281F00000A72472C0170283C00000A7D9C0400040016130CDD940300000E0650110A166F6D00000A7D8B0400040E0650110A176F7800000A7D8C0400040E0650110A186F6E00000A7D8D0400040E0650110A196F6E00000A7D8E0400040E06507B8E04000416FE0216FE01130D110D2D22000E0650110A1A6F6E00000A7D8F0400040E0650110A1B6F6B00000A7D90040004000E0650110A1C6F6100000A110A1D6F6100000A110A1E6F6100000A110A1F116F6100000A110A1F126F7800000A110A1F136F6100000A110A1F146F6100000A730A0100067D9A0400040E0650110A1F096F6100000A7D9B0400040E0650110A1F0A6F6C00000A2D0B110A1F0A6F6D00000A2B02166A007D930400040E0650110A1F0B6F6C00000A2D0B110A1F0B6F6E00000A2B0116007D940400040E06507B93040004166AFE01130D110D2D71000E0650110A1F0C6F6C00000A2D0B110A1F0C6F6D00000A2B02166A007D980400040E06507B98040004166AFE01130D110D2D3C000E0650110A1F0D6F6100000A110A1F0E6F6100000A110A1F0F6F6100000A110A1F116F6100000A110A1F126F7800000A73090100067D9904000400000E0650110A1F106F6C00000A2D0B110A1F106F6D00000A2B02166A007D920400040E0650110A1F156F6E00000A7D9104000400DE14110A14FE01130D110D2D08110A6F1E00000A00DC0000DE14110914FE01130D110D2D0811096F1E00000A00DC000E06507B93040004166AFE01130D110D3A1B0100000006166F6200000A000672772C01706F1600000A260672A12C01706F1600000A260672F52C01706F1600000A260672312D01706F1600000A2606725D2D01706F1600000A2606728F2D01706F1600000A26066F1700000A0E076F1800000A0E07731900000A13090011096F1A00000A7227050070166F1B00000A0E06507B930400048C270000016F1C00000A0011096F5F00000A130A00110A6F6000000A130D110D2D090016130CDDDC0000000E0650110A166F6E00000A7D950400040E0650110A176F7800000A7D960400040E0650110A186F6100000A110A196F6100000A282800000A7D9704000400DE14110A14FE01130D110D2D08110A6F1E00000A00DC0000DE14110914FE01130D110D2D0811096F1E00000A00DC000017130CDE69130B0002166AFE0116FE01130D110D2D28000E065072D32B0170036F8A00000A72EF2D0170110B6F7600000A287400000A7D9C040004002B27000E0650722F2C01700F00281F00000A72132E0170110B6F7600000A287400000A7D9C0400040000DE000016130C2B0000110C2A0000417C000002000000640700002F020000930900001400000000000000020000008F0500001C040000AB0900001400000000000000020000006B0A000057000000C20A00001400000000000000020000003C0A00009E000000DA0A0000140000000000000000000000780500007D050000F50A000063000000370000011B300300860000002A00001100731500000A0A0672352E01706F1600000A2606725B2E01706F1600000A260672AB2E01706F1600000A2600066F1700000A036F1800000A03731900000A0B00076F1A00000A72420E0070166F1B00000A028C270000016F1C00000A00076F1D00000A17FE010CDE1A0714FE010D092D07076F1E00000A00DC260000DE0000160C2B0000082A0000011C000002003F002A6900100000000000002B004E790005010000011B300300F90000002A00001100731500000A0A0672352E01706F1600000A260672F92E01706F1600000A260672352F01706F1600000A260672EE2F01706F1600000A260672383001706F1600000A260672883001706F1600000A26066F1700000A046F1800000A04731900000A0B00076F1A00000A72233101701E6F1B00000A038C170000026F1C00000A00076F1A00000A72420E0070166F1B00000A028C270000016F1C00000A00076F1A00000A723F3101701E6F1B00000A1A8C0F0000026F1C00000A00076F1A00000A72FE9D00701E6F1B00000A1B8C0F0000026F1C00000A00076F1D00000A17FE0216FE010CDE100714FE010D092D07076F1E00000A00DC00082A000000011000000200620084E60010000000001B30080014050000440000110004165205166A5517130E16731100000A131016731100000A131117130B000E04726F3101706F1200000A00731500000A0A06729D3101706F1600000A260672D93101706F1600000A260672413201706F1600000A260672A93201706F1600000A2606720F3301706F1600000A260672753301706F1600000A260672DB3301706F1600000A260672413401706F1600000A260672A73401706F1600000A2606720D3501706F1600000A260672733501706F1600000A260672D93501706F1600000A26066F7B00000A260672A0BC00706F1600000A260672513601706F1600000A260672BBC500706F1600000A2606722BC600706F1600000A2606729BC600706F1600000A260672C13601706F1600000A2606722D3701706F1600000A260672993701706F1600000A260672053801706F1600000A260672713801706F1600000A260672DD3801706F1600000A260672313901706F1600000A260672913901706F1600000A260672F13901706F1600000A260672333A01706F1600000A260672653A01706F1600000A260672C5C800706F1600000A260672D6FA00706F1600000A260672913A01706F1600000A260672563B01706F1600000A2606721B3C01706F1600000A260672893C01706F1600000A2606721C3D01706F1600000A260672AF3D01706F1600000A260672423E01706F1600000A260672D53E01706F1600000A260672683F01706F1600000A260672FB3F01706F1600000A260672884001706F1600000A260672EC4001706F1600000A26067265C900706F1600000A26067291C900706F1600000A26066F1700000A0E046F1800000A0E04731900000A13120011126F1A00000A720D0500701E6F1B00000A028C2D0000016F1C00000A0011126F1A00000A72C6CB0070166F1B00000A038C270000016F1C00000A0011126F1A00000A723E4101701E6F1B00000A168C110000026F1C00000A0011126F5F00000A13130011136F6000000A131611162D0900171315DD360200001113166F6D00000A0B1113176F6D00000A0C1113186F6100000A1113196F6100000A11131A6F6100000A7308010006130411131B6F6100000A11131C6F6100000A11131D6F6100000A7308010006130511131E6F6100000A131011131F096F6100000A131111131F0A6F6B00000A0D00DE14111314FE01131611162D0811136F1E00000A00DC0000DE14111214FE01131611162D0811126F1E00000A00DC00110411052812010006130E110E2C21111016731100000A288C00000A2C12111116731100000A282D00000A16FE012B011700131611162D050016130E001A070311041206120712080E0428CF000006131611162D0900161315DD39010000071104120A0E0428C4000006131611162D0900161315DD1E010000110A110428110100061309110E0811042816010006160E0428D0000006131611162D0900161315DDF2000000110E16FE01131611162D3C001A08120C120D120F0E0428E3000006131611162D0900161315DDC80000000508550702080E0428A2000006131611162D0900161315DDAC000000000708030E0428B3000006131611162D0900161315DD92000000020908731100000613141114166A071F3811096F0D01000616731100000A11046F0D010006110A6F0D0100066F13000006131611162D0600161315DE5511140E046F1D000006131611162D0600161315DE4004175216130B171315DE35260000DE0000DE2700110B16FE01131611162D1900000E04726F3101706F1300000A0000DE05260000DE00000000DC001613152B000011152A417C000002000000C3020000890000004C0300001400000000000000020000005F02000005010000640300001400000000000000000000001E000000BD040000DB040000050000000100000100000000F004000011000000010500000500000001000001020000001E000000C5040000E304000027000000000000001B3005006F05000045000011000E04280E010006510E05280E010006510E06280E01000651057B8004000416731100000A282300000A2D29057B8104000416731100000A282300000A2D16057B8204000416731100000A282300000A16FE012B011600130911092D090016130838050500000002130A110A17594504000000020000000200000014000000300000002B4F051304050C280E0100060D050B0413052B450528160100061304280E0100060C050D280E0100060B166A13052B2905281601000613040528160100060C280E0100060D280E0100060B166A13052B08161308DD8F040000731500000A0A06729D3101706F1600000A2606725C4101706F1600000A260672BA4101706F1600000A260672184201706F1600000A260672764201706F1600000A260672D44201706F1600000A260672324301706F1600000A260672904301706F1600000A260672EE4301706F1600000A2606724C4401706F1600000A26066F7B00000A260672A0BC00706F1600000A260672AE4401706F1600000A260672634501706F1600000A260672124601706F1600000A260672C14601706F1600000A2606727E4701706F1600000A260672354801706F1600000A260672EC4801706F1600000A260672AD4901706F1600000A260672684A01706F1600000A260672234B01706F1600000A260672BC4B01706F1600000A2606724F4C01706F1600000A260672DC4C01706F1600000A260672694D01706F1600000A260672BF4D01706F1600000A260672214E01706F1600000A260672834E01706F1600000A260672DD4E01706F1600000A260672434F01706F1600000A260672A94F01706F1600000A260672035001706F1600000A260672695001706F1600000A260672C5C800706F1600000A260672D6FA00706F1600000A260672C97700706F1600000A26066F7B00000A26067265C900706F1600000A26067291C900706F1600000A26066F1700000A0E076F1800000A0E07731900000A13060011066F1A00000A72420E0070166F1B00000A038C270000016F1C00000A0011066F1A00000A72CF5001701B6F1B00000A11047B800400048C080000016F1C00000A0011066F1A00000A72E95001701B6F1B00000A11047B810400048C080000016F1C00000A0011066F1A00000A72FD5001701B6F1B00000A11047B820400048C080000016F1C00000A0011066F1A00000A72115101701B6F1B00000A087B800400048C080000016F1C00000A0011066F1A00000A72335101701B6F1B00000A087B810400048C080000016F1C00000A0011066F1A00000A724F5101701B6F1B00000A087B820400048C080000016F1C00000A0011066F1A00000A726B5101701B6F1B00000A097B800400048C080000016F1C00000A0011066F1A00000A72915101701B6F1B00000A097B810400048C080000016F1C00000A0011066F1A00000A72B15101701B6F1B00000A097B820400048C080000016F1C00000A0011066F1A00000A72D1510170166F1B00000A11058C270000016F1C00000A0011066F1A00000A72FD5101701B6F1B00000A077B800400048C080000016F1C00000A0011066F1A00000A72235201701B6F1B00000A077B810400048C080000016F1C00000A0011066F1A00000A72435201701B6F1B00000A077B820400048C080000016F1C00000A0011066F5F00000A13070011076F6000000A130911092D0900161308DD980000000E041107166F6100000A1107176F6100000A1107186F6100000A7308010006510E051107196F6100000A11071A6F6100000A11071B6F6100000A7308010006510E0611071C6F6100000A11071D6F6100000A11071E6F6100000A730801000651171308DE33110714FE01130911092D0811076F1E00000A00DC110614FE01130911092D0811066F1E00000A00DC260000DE00001613082B000011082A00414C000002000000BC0400007C00000038050000140000000000000002000000CE0200007E0200004C05000014000000000000000000000066000000FA0400006005000005000000010000011330060012000000090000110002030405160E0428D10000060A2B00062A00001B3003005B0100002A0000110000731500000A0A0672635201706F1600000A260672975201706F1600000A260516FE010D092D1C000672AE5301706F1600000A260672515401706F1600000A26002B0E000672F45401706F1600000A26000672335601706F1600000A26066F1700000A0E056F1800000A0E05731900000A0B00076F1A00000A7297560170186F1B00000A028C430000016F1C00000A00076F1A00000A72BB560170186F1B00000A0E048C430000016F1C00000A00076F1A00000A7227050070166F1B00000A038C270000016F1C00000A00076F1A00000A72ED5601701B6F1B00000A046F0D0100068C080000016F1C00000A00076F1A00000A72115701701B6F1B00000A046F0C0100068C080000016F1C00000A00076F1A00000A72295701701B6F1B00000A046F0B0100068C080000016F1C00000A00076F1D00000A17FE010CDE1A0714FE010D092D07076F1E00000A00DC260000DE0000160C2B0000082A00413400000200000073000000CB0000003E010000100000000000000000000000010000004D0100004E01000005000000010000011B3004000C04000046000011000E0616520E0516731100000A810800000112050E0728D300000616FE01130911092D1600110516FE01130911092D090017130838CF03000000731500000A0D0972415701706F1600000A260972D6FA00706F1600000A260972C97700706F1600000A2600096F1700000A0E076F1800000A0E07731900000A13060011066F1A00000A72420E0070166F1B00000A028C270000016F1C00000A0011066F1A00000A72FE9D00701E6F1B00000A1B8C0F0000026F1C00000A0011066F4200000AA52D000001130400DE14110614FE01130911092D0811066F1E00000A00DC00110416FE0116FE01130911092D0900171308DD1303000009166F6200000A0009720A5801706F1600000A2609723A5801706F1600000A2609726E5801706F1600000A260972C65801706F1600000A260972425901706F1600000A2609720A5801706F1600000A2609723A5801706F1600000A2609726E5801706F1600000A260972485901706F1600000A260972425901706F1600000A2609720A5801706F1600000A2609723A5801706F1600000A2609726E5801706F1600000A260972C25901706F1600000A260972425901706F1600000A260972365A01701204723C5A0170288D00000A6F8E00000A26096F1700000A0E076F1800000A0E07731900000A13060011066F5F00000A13070011076F6000000A130911092D0900161308DD080200001107166F6B00000A288F00000A0B11076F9000000A130911092D0900161308DDE401000011076F6000000A130911092D0900161308DDCE0100001107166F6B00000A288F00000A0A11076F9000000A130911092D0900161308DDAA01000011076F6000000A130911092D0900161308DD940100001107166F6B00000A288F00000A0C00DE14110714FE01130911092D0811076F1E00000A00DC0000DE14110614FE01130911092D0811066F1E00000A00DC00037BD104000416731100000A288C00000A16FE01130911092D1A000E0617520E0516731100000A8108000001171308DD220100000716731100000A282600000A0B0616731100000A282600000A0A0816731100000A282600000A0C0716731100000A282D00000A16FE01130911092D2A000E0617520E0525710800000116731100000A05282600000A07282B00000A282800000A8108000001000616731100000A282D00000A16FE01130911092D2A000E0617520E0525710800000116731100000A04282600000A06282B00000A282800000A8108000001000816731100000A282D00000A16FE01130911092D2B000E0617520E0525710800000116731100000A0E04282600000A08282B00000A282800000A8108000001000E050E06462D0816731100000A2B180E057108000001037BD1040004287000000A1A289100000A008108000001171308DE0B260000DE00001613082B000011082A41640000020000007B0000004E000000C9000000140000000000000002000000E90100009C00000085020000140000000000000002000000DF010000BE0000009D0200001400000000000000000000006400000099030000FD03000005000000010000011B300300950000002A0000110002165400731500000A0A06166F6200000A000672425A01706F1600000A260672B05A01706F1600000A260672065B01706F1600000A260672B15B01706F1600000A260672195C01706F1600000A26066F1700000A036F1800000A03731900000A0B0002076F4200000AA52D0000015400DE100714FE010D092D07076F1E00000A00DC00170CDE0A260000DE0000160C2B0000082A000000011C00000200620011730010000000000000040084880005010000011B300700280100004700001100160A166A0B7E6F00000A0C0072975C01700E046F1800000A0E04731900000A0D00091A6F9200000A0072420E007016739300000A13041104028C270000016F1C00000A001104176F4C00000A00096F1A00000A11046F9400000A2672B35C01701E739300000A13051105186F4C00000A00096F1A00000A11056F9400000A26096F1D00000A26096F1A00000A72B35C01706F9500000A6F5E00000AA5540000020A00DE120914FE01130711072D07096F1E00000A00DC0072CB5C01700C0616FE01130711072D4C0006130811081759450200000002000000130000002B2220080020006A0B0804286600000A0C2B1320090020006A0B0804286600000A0C2B022B00196A0203076D08170E0428D50000062600171306DE0B260000DE00001613062B000011062A41340000020000002100000084000000A50000001200000000000000000000000C0000000D0100001901000005000000010000011B300300FB0100002A0000110000731500000A0A0672E15C01706F1600000A260672395D01706F1600000A260672915D01706F1600000A260672E95D01706F1600000A260672415E01706F1600000A260672995E01706F1600000A260672F15E01706F1600000A260672495F01706F1600000A260672A15F01706F1600000A260672F95F01706F1600000A260672516001706F1600000A260672A96001706F1600000A260672016101706F1600000A260672596101706F1600000A260672B16101706F1600000A260672096201706F1600000A260672616201706F1600000A260672B96201706F1600000A260672B96201706F1600000A260672F95F01706F1600000A26066F1700000A0E066F1800000A0E06731900000A0B00076F1A00000A72116301701E6F1B00000A028C270000016F1C00000A00076F1A00000A722B630170166F1B00000A038C270000016F1C00000A00076F1A00000A72416301701F0C6F1B00000A046F1C00000A00076F1A00000A725B6301701E6F1B00000A058C4B0000016F1C00000A00076F1A00000A72736301701F0C6F1B00000A728B6301706F1C00000A00076F1A00000A72956301701F0C6F1B00000A0E046F1C00000A00076F1A00000A72BB6301701E6F1B00000A0E058C2D0000016F1C00000A00076F1D00000A26170CDE1A0714FE010D092D07076F1E00000A00DC260000DE0000160C2B0000082A0041340000020000000D010000D1000000DE01000010000000000000000000000001000000ED010000EE01000005000000010000011B3003009B020000480000110000166A0B056F4000000A6F4300000A2C16036F0C01000616731100000A288C00000A16FE012B011600130A110A2D0900171309DD5E020000731500000A0A0672A05900706F1600000A260672D16301706F1600000A2606721F6401706F1600000A260672765A00706F1600000A2606727ECC00706F1600000A260672D6CC00706F1600000A26067230CD00706F1600000A26066F1700000A0E046F1800000A0E04731900000A13040011046F1A00000A72695B0070166F1B00000A130511046F1A00000A72696401701F096F1B00000A130611046F1A00000A727F5B00701E6F1B00000A130711046F1A00000A72420E0070166F1B00000A028C270000016F1C00000A0011046F1A00000A721C5900701E6F1B00000A188C0A0000026F1C00000A00036F0C0100060C00056F4000000A6F4800000A130B38E8000000110B6F4900000A740B0000011308001108166F6900000AA5270000010B1105078C270000016F1C00000A001108186F6900000AA5080000010D0908285600000A16FE01130A110A2D620011060908282700000A8C080000016F1C00000A000908288C00000A16FE01130A110A2D12001107198C0A0000026F1C00000A00002B10001107188C0A0000026F1C00000A000011046F1D00000A17FE01130A110A2D0900161309DDB10000002B4D000809282700000A0C1106168C2D0000016F1C00000A001107198C0A0000026F1C00000A0011046F1D00000A17FE01130A110A2D0600161309DE740000110B6F4A00000A130A110A3A08FFFFFFDE1D110B7505000001130C110C14FE01130A110A2D08110C6F1E00000A00DC0000DE14110414FE01130A110A2D0811046F1E00000A00DC0007166AFE0216FE01130A110A2D0C0004070E0428D70000062600171309DE0B260000DE00001613092B000011092A00414C00000200000038010000FF000000370200001D0000000000000002000000A9000000AF01000058020000140000000000000000000000010000008B0200008C02000005000000010000011B300300860000002D00001100007285640170046F1800000A04731900000A0A00066F1A00000A7240650170166F1B00000A038C270000016F1C00000A00066F1A00000A720D0500701E6F1B00000A028C270000016F1C00000A00066F1D00000A17FE010C082D0500160BDE2200DE100614FE010C082D07066F1E00000A00DC00170BDE0A260000DE0000160B2B0000072A0000011C00000200140050640010000000000000010078790005010000011B300800C803000049000011001713060516520E04166A550073060100060B731500000A0A0672546501706F1600000A260672F36501706F1600000A260672926601706F1600000A260672316701706F1600000A260672D06701706F1600000A2606726F6801706F1600000A2606720E6901706F1600000A260672AD6901706F1600000A2606724C6A01706F1600000A260672EB6A01706F1600000A2606728A6B01706F1600000A26066F1700000A0E056F1800000A0E05731900000A13090011096F1A00000A720D0500701E6F1B00000A028C2D0000016F1C00000A0011096F1A00000A72296C01701E6F1B00000A188C0F0000026F1C00000A0011096F1A00000A72456C01701E6F1B00000A1B8C0F0000026F1C00000A0011096F1A00000A722A6000701E6F1B00000A168C110000026F1C00000A0011096F5F00000A130A00110A6F6000000A16FE01130C110C2D2A00110A166F6D00000A0C110A176F6D00000A0D110A186F6D00000A1304110A196F6B00000A1305002B090017130BDD4B02000000DE14110A14FE01130C110C2D08110A6F1E00000A00DC0000DE14110914FE01130C110C2D0811096F1E00000A00DC000E05726F3101706F1200000A00080412010E0528C4000006130C110C2D090016130BDDF4010000161104042816010006170E0528D0000006130C110C2D090016130BDDD4010000047B81040004047B82040004282800000A16731100000A282D00000A16FE01130C110C3A0C0100000006166F6200000A000672A05900706F1600000A2606726F6C01706F1600000A260672DB6C01706F1600000A260672316D01706F1600000A260672936D01706F1600000A26066F1700000A0E056F1800000A0E05731900000A13090011096F1A00000A72115701701F096F1B00000A046F0D0100068C080000016F1C00000A0011096F1A00000A721C5900701E6F1B00000A188C0A0000026F1C00000A0011096F1A00000A7256D50070166F1B00000A098C270000016F1C00000A0011096F1A00000A72420E0070166F1B00000A088C270000016F1C00000A0011096F1D00000A17FE01130C110C2D090016130BDDB900000000DE14110914FE01130C110C2D0811096F1E00000A00DC0000076F0D010006046F0D010006282700000A13070211051104731100000613081108166A081F38110716731100000A046F0D010006076F0D0100066F13000006130C110C2D060016130BDE5511080E056F1D000006130C110C2D060016130BDE4016130605175217130BDE35260000DE0000DE2700110616FE01130C110C2D1900000E05726F3101706F1300000A0000DE05260000DE00000000DC0016130B2B0000110B2A419400000200000035010000470000007C010000140000000000000002000000B3000000E100000094010000140000000000000002000000730200009B0000000E0300001400000000000000000000000C000000830300008F030000050000000100000100000000A403000011000000B50300000500000001000001020000000C0000008B03000097030000270000000000000013300500100000000900001100020304170528DA0000060A2B00062A133002000B00000009000011000E041754160A2B00062A001B300500630200004A000011000E0516731100000A81080000010E041754000316731100000A282900000A2D090418FE0116FE012B011600130511053A1C02000000731500000A0C02280E010006120012010E0628C500000616FE01130511053AF7010000000E05067B830400048108000001041306110645030000000500000059000000A200000038C10000000316731100000A282300000A16FE01130511052D2200066F0B010006067B83040004282700000A066F0B010006282E00000A1001002B1B00066F0B010006067B83040004282700000A03282E00000A1001002B750316731100000A282300000A16FE01130511052D1700067B8304000415731100000A287000000A1001002B1B00067B8304000403282E00000A15731100000A287000000A1001002B2C03066F0B010006282D00000A16FE01130511052D0D000E041854161304DD1E0100002B08161304DD14010000731500000A0C0872E36D01706F1600000A260418FE0116FE01130511052D1C0008724F6E01706F1600000A260872BB6E01706F1600000A26002B0E000872276F01706F1600000A26000872936F01706F1600000A26086F1700000A0E066F1800000A0E06731900000A0D00096F1A00000A727F0500701F096F1B00000A038C080000016F1C00000A00096F1A00000A72420E0070166F1B00000A028C270000016F1C00000A000418FE0116FE01130511052D1F00096F1A00000A72612B0170186F1B00000A058C430000016F1C00000A0000096F1D00000A17FE0116FE01130511052D0A000E041654171304DE2600DE120914FE01130511052D07096F1E00000A00DC00000000DE05260000DE00001613042B000011042A004134000002000000B5010000870000003C02000012000000000000000000000012000000420200005402000005000000010000011B3004009C0100004B0000110000027B610400046F4000000A6F4800000A0C3859010000086F4900000A740B0000010B00027B6304000416731100000A287200000A16FE010D092D0600383B010000077291E000706F4500000AA50900000217FE010D092D060038110100000772FF6F01706F5C00000A2C060316FE012B0116000D092D0B00027B630400040A002B4A000772177001706F4500000AA5080000010772B1E000706F4500000AA5080000010772177001706F4500000AA508000001282E00000A282700000A0A06027B63040004282E00000A0A000616731100000A287200000A16FE010D092D0600388A0000000772B1E000700772B1E000706F4500000AA50800000106282800000A8C080000016F3E00000A00077228F00070077228F000706F4500000AA50800000106282800000A8C080000016F3E00000A0002257B6304000406282700000A7D6304000402257B6704000406282800000A7D67040004027B68040004257B8204000406282800000A7D8204000400086F4A00000A0D093A9AFEFFFFDE1A0875050000011304110414FE010D092D0811046F1E00000A00DC002A411C000002000000130000006D010000800100001A00000000000000133003005F0000004200001100027B6304000416731100000A287200000A16FE010B072D03002B42027B630400040A027B68040004257B8004000406282800000A7D8004000402257B6604000406282800000A7D6604000402257B6304000406282700000A7D630400042A001B3004005C0100004B0000110000037B610400046F4000000A6F4800000A0C3819010000086F4900000A740B0000010B00037B6204000416731100000A287200000A16FE010D092D060038FB000000077291E000706F4500000AA50900000202FE010D092D060038D10000000772B1E000706F4500000AA508000001037B62040004282E00000A0A06037B680400047B82040004282E00000A0A0616731100000A287200000A16FE010D092D0600388A0000000772B1E000700772B1E000706F4500000AA50800000106282700000A8C080000016F3E00000A0007724AF0007007724AF000706F4500000AA50800000106282800000A8C080000016F3E00000A0003257B6504000406282800000A7D6504000403257B6204000406282700000A7D62040004037B68040004257B8204000406282700000A7D8204000400086F4A00000A0D093ADAFEFFFFDE1A0875050000011304110414FE010D092D0811046F1E00000A00DC002A411C000002000000130000002D010000400100001A000000000000001B3004002D0100004B0000110000037B610400046F4000000A6F4800000A0C38EA000000086F4900000A740B0000010B00037B6D0400047B8204000416731100000A287200000A16FE010D092D060038C7000000077291E000706F4500000AA50900000202FE010D092D0600389D0000000772B1E000706F4500000AA508000001037B6D0400047B82040004282E00000A0A0616731100000A287200000A16FE010D092D03002B660772B1E000700772B1E000706F4500000AA50800000106282700000A8C080000016F3E00000A0007724AF0007007724AF000706F4500000AA50800000106282800000A8C080000016F3E00000A00037B6D040004257B8204000406282700000A7D8204000400086F4A00000A0D093A09FFFFFFDE1A0875050000011304110414FE010D092D0811046F1E00000A00DC002A0000000110000002001300FE11011A000000001B3004002D0100004B0000110000037B610400046F4000000A6F4800000A0C38EA000000086F4900000A740B0000010B00037B6D0400047B8104000416731100000A287200000A16FE010D092D060038C7000000077291E000706F4500000AA50900000202FE010D092D0600389D0000000772B1E000706F4500000AA508000001037B6D0400047B81040004282E00000A0A0616731100000A287200000A16FE010D092D03002B660772B1E000700772B1E000706F4500000AA50800000106282700000A8C080000016F3E00000A0007724AF0007007724AF000706F4500000AA50800000106282800000A8C080000016F3E00000A00037B6D040004257B8104000406282700000A7D8104000400086F4A00000A0D093A09FFFFFFDE1A0875050000011304110414FE010D092D0811046F1E00000A00DC002A0000000110000002001300FE11011A000000001B300400A20100004C0000110000037B610400046F4000000A6F4800000A0D385B010000096F4900000A740B0000010C00037B6204000416731100000A287200000A16FE01130411042D0600383D010000087291E000706F4500000AA50900000202FE01130411042D0600380F0100000872FF6F01706F5C00000A16FE01130411042D060038F500000016731100000A0872B1E000706F4500000AA5080000010872177001706F4500000AA508000001282700000A282600000A0B037B6204000407282E00000A0A037B680400047B8204000406282E00000A0A0616731100000A287200000A16FE01130411042D0600388A0000000872B1E000700872B1E000706F4500000AA50800000106282700000A8C080000016F3E00000A0008724AF0007008724AF000706F4500000AA50800000106282800000A8C080000016F3E00000A0003257B6504000406282800000A7D6504000403257B6204000406282700000A7D62040004037B68040004257B8204000406282700000A7D8204000400096F4A00000A130411043A96FEFFFFDE1C0975050000011305110514FE01130411042D0811056F1E00000A00DC002A0000411C0000020000001300000071010000840100001C0000000000000013300300D00000004200001100027B680400047B81040004027B62040004282E00000A0A0616731100000A282D00000A16FE010B072D3D00027B68040004257B8104000406282700000A7D8104000402257B6404000406282800000A7D6404000402257B6204000406282700000A7D6204000400027B680400047B80040004027B62040004282E00000A0A0616731100000A282D00000A16FE010B072D3D00027B68040004257B8004000406282700000A7D8004000402257B6404000406282800000A7D6404000402257B6204000406282700000A7D62040004002A1B3003008A0100004D000011000416540516541201FE150A0000011202FE150A0000010E04FE150D00000100731500000A0A06729A8C00706F1600000A260672317001706F1600000A2606720A7101706F1600000A260672E37101706F1600000A260672BC7201706F1600000A260672F47201706F1600000A2606722E7301706F1600000A260672687301706F1600000A260672CA7301706F1600000A26066F1700000A0E056F1800000A0E05731900000A0D00096F1A00000A7227050070166F1B00000A038C270000016F1C00000A00096F1A00000A723E4101701E6F1B00000A168C110000026F1C00000A00096F1A00000A727F5B00701E6F1B00000A028C110000026F1C00000A00096F5F00000A13040011046F6000000A130611062D0600161305DE6B041104166F6E00000A54051104176F6E00000A541104186F9600000A0B1104196F9600000A0C0E04120207289700000A810D000001171305DE31110414FE01130611062D0811046F1E00000A00DC0914FE01130611062D07096F1E00000A00DC260000DE00001613052B000011052A0000414C000002000000070100004E00000055010000140000000000000002000000A7000000C2000000690100001200000000000000000000001F0000005C0100007B01000005000000010000011B300300690100004E0000110016731100000A03282600000A100116731100000A04282600000A100272267401700A067256740170286600000A0A067267750170286600000A0A0672D7750170286600000A0A06724D760170286600000A0A0E041F20FE0116FE010D092D10000672BF760170286600000A0A002B0E00067260770170286600000A0A0000060E056F1800000A0E05731900000A0B00076F1A00000A72B69A0070166F1B00000A028C270000016F1C00000A00076F1A00000A72352100701F096F1B00000A038C080000016F1C00000A00076F1A00000A72D07701701F096F1B00000A048C080000016F1C00000A00076F1A00000A72F4770170186F1B00000A058C430000016F1C00000A00076F1A00000A723E4101701E6F1B00000A168C2D0000016F1C00000A00076F1A00000A72187801701E6F1B00000A188C2D0000016F1C00000A00076F1D00000A17FE010CDE1A0714FE010D092D07076F1E00000A00DC260000DE0000160C2B0000082A000000011C000002008F00BD4C01100000000000007E00DE5C0105010000011B300300BE0000004F0000110000731500000A0A06723C7801706F1600000A260672A47801706F1600000A2606720C7901706F1600000A260672747901706F1600000A26066F1700000A046F1800000A04731900000A0B00076F1A00000A72187801701E6F1B00000A188C110000026F1C00000A00076F1A00000A7227050070166F1B00000A028C270000016F1C00000A00076F1D00000A17FE010DDE290714FE01130411042D07076F1E00000A00DC0C0003086F7600000A6F1600000A2600DE0000160D2B0000092A0000011C000002004B00479200120000000000000100A3A40012370000011B3009002E01000050000011000E07731500000A510073F30000060A061F207D2F04000406047D31040004060E067D3004000406037D3504000406177D3604000406027D3204000406166A7D33040004050E040E050612010E0828A8000006130511052D26000E0750076F010100066F1600000A260E075072DC7901706F1600000A26161304DDAB000000020E07500E0828E5000006130511052D17000E075072167A01706F1600000A26161304DD830000000420000010000E0828CD000006130511052D14000E075072647A01706F1600000A26161304DE5C050E050273110000060C08166A041F4B0316731100000A16731100000A037E6F00000A6F1500000626080E086F1D000006130511052D0600161304DE1F171304DE1A0D000E0750096F7600000A6F1600000A2600DE00001613042B000011042A0000411C00000000000009000000070100001001000014000000370000011B300300200100002800001100731500000A0A0672A07A01706F1600000A260672D67A01706F1600000A2606720E7B01706F1600000A2606723E7B01706F1600000A2606725D2D01706F1600000A2606728F2D01706F1600000A2600066F1700000A046F1800000A04731900000A0B00076F1A00000A7227050070166F1B00000A028C270000016F1C00000A00076F5F00000A0C00086F6000000A130411042D0800160DDD80000000037B3704000408166F6E00000A6A7D41040004037B3704000408176F6100000A7D42040004037B3704000408186F6E00000A6A7D43040004037B3704000408196F6100000A7D4404000400DE120814FE01130411042D07086F1E00000A00DC00170DDE1C0714FE01130411042D07076F1E00000A00DC260000DE0000160D2B0000092A012800000200880062EA001200000000020063009E0101120000000000004F00C4130105010000011B30030085030000510000110000731500000A0A160B03130511054502000000050000000500000038F000000006166F6200000A000672707B01706F1600000A260672987B01706F1600000A260672D47B01706F1600000A260672247C01706F1600000A2606727A7C01706F1600000A26066F1700000A056F1800000A05731900000A0C00086F1A00000A72420E0070166F1B00000A028C270000016F1C00000A00086F1A00000A72D07C01701E6F1B00000A178C090000026F1C00000A00086F1A00000A72DC7C01701E6F1B00000A188C090000026F1C00000A00086F1A00000A721C5900701E6F1B00000A188C0A0000026F1C00000A00086F4200000AA52D0000010B00DE120814FE01130611062D07086F1E00000A00DC002B0003130511054505000000F1000000F10000000500000005000000F1000000383D0200000416731100000A288C00000A16FE01130611062D0900171304DD2F02000006166F6200000A000672352E01706F1600000A260672E87C01706F1600000A260672957D01706F1600000A260672487E01706F1600000A260672D77E01706F1600000A260672477F01706F1600000A26066F1700000A056F1800000A05731900000A0D00096F1A00000A72420E0070166F1B00000A028C270000016F1C00000A00096F1A00000A727F0500701F096F1B00000A03182E0804285D00000A2B0104008C080000016F1C00000A00096F1D00000A17FE011304DD730100000914FE01130611062D07096F1E00000A00DC06166F6200000A000672352E01706F1600000A260672937F01706F1600000A260672477F01706F1600000A26066F1700000A056F1800000A05731900000A0D00096F1A00000A72420E0070166F1B00000A028C270000016F1C00000A00096F1A00000A727F0500701F096F1B00000A260316FE0116FE01130611062D4E000716FE0116FE01130611062D1F00096F1A00000A727F0500706F9500000A7E3F00000A6F1C00000A00002B1E00096F1A00000A727F0500706F9500000A048C080000016F1C00000A0000002B67071733090317FE0116FE012B011700130611062D2000096F1A00000A727F0500706F9500000A048C080000016F1C00000A00002B32031AFE0116FE01130611062D1F00096F1A00000A727F0500706F9500000A7E3F00000A6F1C00000A00002B0600171304DE2F096F1D00000A17FE011304DE220914FE01130611062D07096F1E00000A00DC161304DE0B260000DE00001613042B000011042A00000041640000020000007800000084000000FC000000120000000000000002000000B5010000590000000E0200001200000000000000020000005F020000000100005F03000012000000000000000000000001000000750300007603000005000000010000011B300300C1000000520000110072F15B007072F17F01700328EC0000061201284700000A260716FE0116FE01130411042D0800170D389000000000731500000A0A0672118001706F1600000A260672698001706F1600000A260672B18001706F1600000A26066F1700000A036F1800000A03731900000A0C00086F1A00000A7227050070166F1B00000A028C270000016F1C00000A00086F1D00000A17FE01130411042D0500160DDE20170DDE1C0814FE01130411042D07086F1E00000A00DC260000DE0000160D2B0000092A000000011C000002006C0036A200120000000000002E0086B40005010000011B300300E90000002A0000110000731500000A0A0672F38001706F1600000A260672278101706F1600000A260672618101706F1600000A2606728D8101706F1600000A260672BD8101706F1600000A260672EF8101706F1600000A2606722F8201706F1600000A2606725F8201706F1600000A26066F1700000A046F1800000A04731900000A0B00076F1A00000A72F88201701E6F1B00000A028C2D0000016F1C00000A00076F1A00000A7210830170166F1B00000A038C270000016F1C00000A00076F1D00000A17FE010D092D0500160CDE1E170CDE1A0714FE010D092D07076F1E00000A00DC260000DE0000160C2B0000082A000000011C000002007B0051CC00100000000000000100DBDC0005010000011B300400E205000053000011000E051752031305731500000A1306731500000A1307160A160B160C160D731500000A13081108166F6200000A001108720A5801706F1600000A261108723A5801706F1600000A261108722E8301706F1600000A26110872AA8301706F1600000A26110872268401706F1600000A261108720A5801706F1600000A261108723A5801706F1600000A261108722E8301706F1600000A26110872408401706F1600000A26110872268401706F1600000A261108720A5801706F1600000A261108723A5801706F1600000A261108722E8301706F1600000A26110872CF8401706F1600000A26110872268401706F1600000A261108720A5801706F1600000A261108723A5801706F1600000A261108722E8301706F1600000A26110872398501706F1600000A26110872425901706F1600000A260011086F1700000A0E066F1800000A0E06731900000A13090011096F5F00000A130A00110A6F6000000A16FE01130C110C2D1B00110A166F6B00000A1200284700000A130C110C2D0400160A0000110A6F6000000A16FE01130C110C2D1B00110A166F6B00000A1201284700000A130C110C2D0400160B0000110A6F6000000A16FE01130C110C2D1B00110A166F6B00000A1202289800000A130C110C2D0400160C0000110A6F6000000A16FE01130C110C2D1B00110A166F6B00000A1203289800000A130C110C2D0400160D000000DE14110A14FE01130C110C2D08110A6F1E00000A00DC0000DE14110914FE01130C110C2D0811096F1E00000A00DC000E054616FE01130C110C2D4D000616311106289900000A03282300000A16FE012B011700130C110C2D2E000E051652110672B38501700F01282400000A6F9A00000A26110672DD8501701200282100000A6F9A00000A2600000E054616FE01130C110C3ACF000000000F02287A00000A230000000000000000FE0216FE01130C110C3AAF000000000716312307289900000A030F02287A00000A6C289B00000A282B00000A282300000A16FE012B011700130C110C2D7E000E051652110672B38501700F01282400000A6F9A00000A261106721B8601700F02287A00000A130D120D7247860170289C00000A6F9A00000A2611067251860170030F02287A00000A6C289B00000A282B00000A130E120E7247860170289D00000A6F9A00000A26110672838601701201282100000A6F9A00000A260000000E054616FE01130C110C2D66000817330F057B77040004166AFE0116FE012B011700130C110C2D4900027BC804000417FE0116FE01130C110C2D0400002B31000E051652110672D7860170057C77040004281F00000A6F9A00000A26110672F9860170168D010000016F9E00000A260000000E054616FE01130C110C2D4A000917330B057B7404000416FE012B011700130C110C2D31000E05165211067223870170057C74040004287500000A6F9A00000A261106724F870170168D010000016F9E00000A2600001713040E0546130C110C2D760016731100000A1305181304110772898701706F9F00000A26110772AF870170057B710400048C270000016F9A00000A26110772CD870170057B720400048C2D0000016F9A00000A26110772F18701706F9F00000A2611061611076F1700000A6FA000000A260E0411066F1700000A6FFF0000060000731500000A13081108720D8801706F1600000A26110872438801706F1600000A261108724E8901706F1600000A26110872318A01706F1600000A26110872148B01706F1600000A2611086F1700000A0E066F1800000A0E06731900000A13090011096F1A00000A72848B01701E6F1B00000A168C2D0000016F1C00000A0011096F1A00000A72A08B01701E6F1B00000A11048C2D0000016F1C00000A0011096F1A00000A72BE8B01701F096F1B00000A038C080000016F1C00000A0011096F1A00000A72E08B01701F096F1B00000A11058C080000016F1C00000A0011096F1A00000A7227050070166F1B00000A057B700400048C270000016F1C00000A0011096F1D00000A17FE01130BDE1F110914FE01130C110C2D0811096F1E00000A00DC260000DE000016130B2B0000110B2A0000416400000200000054010000B0000000040200001400000000000000020000004A010000D20000001C02000014000000000000000200000011050000AE000000BF05000014000000000000000000000032010000A1040000D305000005000000010000011B300400D4000000540000110072010000700C00731500000A0A06720A5801706F1600000A2606723A5801706F1600000A260672008C01706F1600000A2606724A8C01706F1600000A26066F1700000A046F1800000A04731900000A0D00096F1A00000A72988C01701F0C1F326F2F00000A026F1C00000A00096F1A00000A72A88C01701F0C1F326F2F00000A036F1C00000A00096F4200000A0B072C0A077E3F00000AFE012B011700130511052D09000774280000010C0000DE120914FE01130511052D07096F1E00000A00DC0000DE05260000DE00000813042B0011042A011C0000020051005FB000120000000000000700BFC60005010000018602167D2204000402280E0100067D2304000402167D2404000402283000000A002A00001330020022000000090000110002037D2D04000404283100000A0A062D0F00027B2E040004046F1600000A26002A00001330030025000000090000110003283100000A0A062D1900027B2E04000472BC8C017003286600000A6F1600000A26002A260002167D2D0400042A0013300100110000001D00001100027B2E0400046F1700000A0A2B00062A00000003300200610000000000000002166A7D2504000402167D2604000402280E0100067D2704000402280E0100067D2804000402280E0100067D2904000402280E0100067D2A0400040216731100000A7D2B04000402177D2D04000402731500000A7D2E04000402283000000A002A00000003300200AF00000000000000021C7D2F0400040272010000707D3004000402156A7D3104000402156A7D3204000402166A7D3304000402167D340400040216731100000A7D3504000402167D360400040273F40000067D3704000402167D3804000402166A7D390400040273FC0000067D3A0400040272010000707D3B04000402167D3C04000402280E0100067D3D04000402280E0100067D3E04000402280E0100067D3F04000402280E0100067D4004000402283000000A002A0003300200480000000000000002166A7D410400040216731100000A7D4204000402166A7D430400040216731100000A7D440400040216731100000A7D450400040216731100000A7D4604000402283000000A002A133002001C0000001B00001100027B47040004027B4C0400047B4F040004282800000A0A2B00062A133001000C0000001B00001100027B490400040A2B00062A133001000C0000001B00001100027B480400040A2B00062A13300200220000001B000011000228F50000060228F6000006282800000A0228F7000006282800000A0A2B00062A0000133001000C0000001B00001100027B4A0400040A2B00062A133001000C0000001B00001100027B4B0400040A2B00062A13300200170000001B000011000228F90000060228FA000006282800000A0A2B00062A4E0273FD0000067D4C04000402283000000A002A1E02283000000A2A001330020022000000090000110002037D5D04000404283100000A0A062D0F00027B5E040004046F1600000A26002A00001330030025000000090000110003283100000A0A062D1900027B5E04000472BC8C017003286600000A6F1600000A26002A260002167D5D0400042A0013300100110000001D00001100027B5E0400046F1700000A0A2B00062A9602280E0100067D5C04000402177D5D04000402731500000A7D5E04000402283000000A002A0003300200A50000000000000002166A7D5F04000402166A7D6004000402147D610400040216731100000A7D620400040216731100000A7D630400040216731100000A7D640400040216731100000A7D650400040216731100000A7D660400040216731100000A7D6704000402280E0100067D6804000402280E0100067D6904000402280E0100067D6A04000402167D6B04000402280E0100067D6C04000402280E0100067D6D04000402283000000A002A00000003300200AB00000000000000021C7D6E0400040272010000707D6F04000402166A7D7004000402166A7D7104000402167D720400040272010000707D7304000402177D7404000402280E0100067D7504000402731500000A7D7604000402166A7D7704000402280E0100067D7804000402166A7D7904000402280E0100067D7A04000402167D7B04000402166A7D7C0400040273FC0000067D7D0400040272010000707D7E04000402167D7F04000402283000000A002A001330040053000000550000110072D08C01701A8D010000010B071602280D0100060C1202282400000AA20717027C80040004282400000AA20718027C81040004282400000AA20719027C82040004282400000AA207282500000A0A2B00062A4602283000000A000002167D84040004002A00000003300200820000000000000002283000000A000002037B800400047D8004000402037B810400047D8104000402037B820400047D8204000402037B800400047D8004000402037B830400047D8304000402037B840400047D8404000402037B850400047D8504000402037B860400047D8604000402037B870400047D8704000402037B880400047D88040004002A000003300200580000000000000002283000000A000002037D8004000402047D8104000402057D820400040216731100000A7D8304000402167D8404000402167D8504000402167D860400040216731100000A7D870400040216731100000A7D88040004002A920203040528080100060000020E047D83040004020E057D8504000402177D86040004002A000000033006004700000000000000020304050E040E052809010006000002257B820400040E06282800000A7D82040004020E067D8704000402257B810400040E07282800000A7D81040004020E077D88040004002A0013300200170000001B00001100027B80040004027B81040004282800000A0A2B00062A00133001000C0000001B00001100027B820400040A2B00062A13300200220000001B00001100027B80040004027B81040004282800000A027B82040004282800000A0A2B00062A0000133001000B000000560000110073060100060A2B00062A0013300300100100004000001100027B840400042D16027B8304000416731100000A288C00000A16FE012B0116000C082D0800020B38E100000002280B010006027B83040004282300000A16FE010C082D20000216731100000A7D800400040216731100000A7D81040004020B38A9000000027B830400040A027B8004000406285600000A16FE010C082D240002257B8004000406282700000A7D8004000416731100000A0A02177D84040004002B1B0006027B80040004282700000A0A0216731100000A7D80040004000616731100000A282D00000A2C11027B8104000406285600000A16FE012B0117000C082D1B0002257B8104000406282700000A7D8104000402177D84040004000216731100000A7D87040004020B2B00072A133004003E0000005600001100027B80040004037B80040004282800000A027B81040004037B81040004282800000A027B82040004037B82040004282800000A73080100060A2B00062A0000133004003E0000005600001100027B80040004037B80040004282700000A027B81040004037B81040004282700000A027B82040004037B82040004282700000A73080100060A2B00062A000013300200410000000900001100027B80040004037B80040004288C00000A2C26027B81040004037B81040004288C00000A2C13027B82040004037B82040004288C00000A2B0116000A2B00062A000000133002001000000009000011000203281201000616FE010A2B00062A133002001200000009000011000203745000000228120100060A2B00062A0000133001000C0000001E000011000228A100000A0A2B00062A133002000D0000005600001100021628170100060A2B00062A000000133005007000000041000011000316FE010B072D3B00027B80040004285D00000A027B81040004285D00000A027B82040004285D00000A027B83040004285D00000A027B8504000473090100060A2B2A00027B80040004285D00000A027B81040004285D00000A027B82040004285D00000A73080100060A2B00062A1330020061000000570000110073060100060A06027B800400047D8004000406027B810400047D8104000406027B820400047D8204000406027B830400047D8304000406027B840400047D8404000406027B850400047D8504000406027B860400047D86040004060B2B00072A00000013300300260000001D0000110072108D0170027B890400046F1700000A027C8A040004282400000A282200000A0A2B00062A8602283000000A000002280E0100067D890400040216731100000A7D8A040004002A9E02283000000A000002037B8904000473070100067D8904000402037B8A0400047D8A040004002A7602283000000A0000020373070100067D8904000402047D8A040004002A0000133001000B0000005800001100731A0100060A2B00062A00133003002D0000005800001100027B89040004037B890400042810010006027B8A040004037B8A040004282800000A731C0100060A2B00062A000000133003002D0000005800001100027B89040004037B890400042811010006027B8A040004037B8A040004282700000A731C0100060A2B00062A000000133002002E0000000900001100027B89040004037B8904000428120100062C13027B8A040004037B8A040004288C00000A2B0116000A2B00062A0000133002001000000009000011000203282001000616FE010A2B00062A133002001200000009000011000203745100000228200100060A2B00062A0000133001000C0000001E000011000228A100000A0A2B00062A033002009D0000000000000002166A7D8B04000402177D8C04000402167D8D04000402167D8E04000402167D8F0400040272010000707D9004000402166A7D9204000402166A7D9304000402167D9404000402177D9504000402177D960400040216731100000A7D9704000402166A7D9804000402280E0100067D9904000402280E0100067D9A0400040216731100000A7D9B0400040272010000707D9C04000402283000000A002A9A02147DBB04000402147DBC04000402147DBD04000402283000000A000002282E01000600002A13300200220000005900001100027BBC0400046F4000000A166F4400000A166F6900000AA55B0000020A2B00062AB200027BBC0400046F4000000A166F4400000A16038C5B0000026F7C00000A00027BBB0400046F4600000A002A001B300400300100005A00001100027BBC0400046F4000000A166F4400000A166F6900000AA55B00000213041104450300000002000000090000000D0000002B0F030D38F3000000040A2B35050A2B31722A8D0170027BBC0400046F4000000A166F4400000A166F6900000AA52D0000018C2D00000128A200000A73A300000A7A731500000A0B00027BBB0400046F4000000A6F4800000A13052B4711056F4900000A740B0000010C00076FA400000A16FE0216FE01130611062D0E000772F49400706F9F00000A26000708166F6900000A74280000016FA500000A6F9F00000A260011056F4A00000A130611062DACDE1D110575050000011307110714FE01130611062D0811076F1E00000A00DC000672608D0170076F1700000A7E6F00000A285500000A2D040E042B06076F1700000A00283C00000A0D2B00092A0110000002008D0058E5001D0000000013300300630000005B00001100027BBC0400046F4000000A166F4400000A166F6900000AA55B00000216FE0116FE010B072D03002B380203282F01000614FE010B072D03002B27027BBB0400046F3D00000A0A0616036F7C00000A00027BBB0400046F4000000A066F4100000A002A0013300200270000005B000011000203282F0100060A0614FE010B072D1500066FA600000A00027BBB0400046FA700000A00002A0013300200590000005C00001100027BBC0400046F4000000A166F4400000A166F6900000AA55B0000020B0745030000000200000006000000160000002B21170A2B210203282F01000614FE0116FE010A2B110203282F01000614FE010A2B04160A2B00062A000000133003003E0000005D00001100027BBC0400046F4000000A166F4400000A166F6900000AA55B00000216FE0116FE010B072D0500140A2B1002027BBD0400041728300100060A2B00062A0000133004003F00000009000011000314FE0116FE010A062D0B0002162827010006002B27027BBC0400046F4600000A00027BBB0400046F4600000A0002027BBD04000403182831010006002A0013300500F70000005E00001100160B0272668D017073A800000A7DBD0400040272808D017073A900000A7DBC040004027BBC0400046F3300000A72928D0170078C5B00000228AA00000A6F3500000A26027BBC0400046F3D00000A0A0616168C5B0000026F7C00000A00027BBC0400046F4000000A066F4100000A0002729C8D017073A900000A7DBB040004027BBB0400046F3300000A72AE8D017072C90C0070283400000A6F3500000A26027BBB040004178D340000010C0816027BBB0400046F3300000A166FAB00000AA2086F3B00000A00027BBD0400046FAC00000A027BBC0400046FAD00000A00027BBD0400046FAC00000A027BBB0400046FAD00000A002A001B300300700000005F0000110000027BBB0400046F4000000A6F4800000A0C2B2D086F4900000A740B0000010A000306166F6900000A7428000001196FAE00000A16FE010D092D0500060BDE2C00086F4A00000A0D092DC9DE1A0875050000011304110414FE010D092D0811046F1E00000A00DC00140B2B0000072A01100000020013003B4E001A000000001B30030038000000600000110072010000700A73AF00000A0B000307046FB000000A00076F1700000A0A00DE100714FE010D092D07076F1E00000A00DC00060C2B00082A0110000002000D0014210010000000001B3003002700000061000011000473B100000A0A000306056FB200000A2600DE100614FE010B072D07066F1E00000A00DC002A0001100000020008000D150010000000001B300300D1000000620000110000731500000A0B07166F6200000A000772B88D01706F1600000A260772E08D01706F1600000A260772128E01706F1600000A260772908E01706F1600000A260772A28E01706F1600000A26076F1700000A036F1800000A03731900000A0C00086F1A00000A72F88201701E6F1B00000A028C2D0000016F1C00000A00086F4200000A0A062C0A067E3F00000AFE012B011700130511052D100006A52D0000010D0916FE021304DE2400DE120814FE01130511052D07086F1E00000A00DC0000DE05260000DE00001613042B000011042A000000011C000002005F004DAC00120000000000000100C1C20005010000011B3005009F010000630000110005733F0100065100731500000A0A06166F6200000A000672E88E01706F1600000A2606723A5801706F1600000A260672608F01706F1600000A260672079001706F1600000A2606723B9001706F1600000A260672839001706F1600000A260672E79001706F1600000A260672929101706F1600000A2606723D9201706F1600000A26066F1700000A0E046F1800000A0E04731900000A0C00086F1A00000A72A99201701F0C6F1B00000A026F1C00000A00086F1A00000A72BF9201701F0C6F1B00000A032C03032B057201000070006F1C00000A00086F1A00000A72DD9201701E6F1B00000A0314FE018C430000016F1C00000A00086F1A00000A72059301701E6F1B00000A048C2D0000016F1C00000A00086F1A00000A72259301701E6F1B00000A168C2D0000016F1C00000A00086F4200000A0B072C0A077E3F00000AFE012B011700130511052D150007A52D0000010D09050E0428350100061304DE3A0314FE01130511052D0E00160203040E0428340100062600171304DE1D0814FE01130511052D07086F1E00000A00DC260000DE00001613042B000011042A00413400000200000098000000E60000007E01000012000000000000000000000008000000880100009001000005000000010000011B300300CD0100002A0000110000731500000A0A06166F6200000A0006724F9301706F1600000A2606726F9301706F1600000A260672779301706F1600000A260672919301706F1600000A260672CB9301706F1600000A2606721B9401706F1600000A2606726F9401706F1600000A260672CB9401706F1600000A260672D39401706F1600000A2606722F9501706F1600000A260672919501706F1600000A260672FB9501706F1600000A2606726B9601706F1600000A260672C59601706F1600000A260672239701706F1600000A260672899701706F1600000A260672F59701706F1600000A260672039801706F1600000A260672419801706F1600000A260672A39801706F1600000A260672F79801706F1600000A2606724F9901706F1600000A26066F1700000A0E046F1800000A0E04731900000A0B00076F1A00000A72AF9901701E6F1B00000A028C2D0000016F1C00000A00076F1A00000A72A99201701F0C6F1B00000A036F1C00000A00076F1A00000A72BF9201701F0C6F1B00000A046F1C00000A00076F1A00000A72059301701E6F1B00000A058C2D0000016F1C00000A00076F1D00000A17FE010D092D0500160CDE1E170CDE1A0714FE010D092D07076F1E00000A00DC260000DE0000160C2B0000082A00000041340000020000002D01000083000000B001000010000000000000000000000001000000BF010000C00100000500000001000001133005001800000009000011000272010000707201000070030428380100060A2B00062A1330050014000000090000110016027201000070030428380100060A2B00062A1330050014000000090000110016720100007002030428380100060A2B00062A1B300400E8040000640000110072E096007072C19901700E0428EC0000061203284700000A130711072D0400160D0005733F01000651731500000A0A000672ED9901706F1600000A260672729A01706F1600000A260672F79A01706F1600000A2606727C9B01706F1600000A260672019C01706F1600000A260672869C01706F1600000A2606720B9D01706F1600000A260672909D01706F1600000A260672159E01706F1600000A2606729A9E01706F1600000A2606721F9F01706F1600000A260672A49F01706F1600000A26067229A001706F1600000A260672AEA001706F1600000A26067233A101706F1600000A260672B8A101706F1600000A2606723BA201706F1600000A260672C0A201706F1600000A26067245A301706F1600000A260672CAA301706F1600000A2606724FA401706F1600000A260672D4A401706F1600000A26067259A501706F1600000A260672DEA501706F1600000A26067261A601706F1600000A260672E4A601706F1600000A26067267A701706F1600000A260672EAA701706F1600000A26067283A801706F1600000A26067208A901706F1600000A2603283100000A130711072D100006728DA901706F1600000A26002B2C0004283100000A130711072D10000672D5A901706F1600000A26002B0E00067237AA01706F1600000A260000066F1700000A0E046F1800000A0E04731900000A13040011046F1A00000A7289AA01701E6F1B00000A1A8C2E0000026F1C00000A0003283100000A130711072D200011046F1A00000A72C52100701F0C1F326F2F00000A036F1C00000A00002B4E0004283100000A130711072D200011046F1A00000A72A5AA01701F0C1F326F2F00000A046F1C00000A00002B200011046F1A00000A720D0500701E6F1B00000A028C2D0000016F1C00000A00000011046F5F00000A13050011056F6000000A130711072D0900161306DD3402000005501105166F6E00000A7DC60400041105176F6C00000A130711072D110005501105176F6E00000A7DC70400040005501105186FB300000A7DC804000405501105196F6B00000A7DC9040004055011051A6F6B00000A7DCA040004055011051B6F7800000A7DCB040004055011051C6F6E00000A7DCC040004055011051D6F6C00000A2D0A11051D6F6D00000A2B02166A007DCE040004055011051E6F6C00000A2D0A11051E6F6D00000A2B02166A007DCF040004055016731100000A11051F096F6100000A282600000A7DD1040004055011051F0A6F7800000A7DD2040004055011051F0B6F7800000A7DD304000411051F0C6F6E00000A0B11051F0D6F6E00000A0C055009085F0708665F607DD0040004055011051F0E6F6E00000A7DCD040004055011051F0F6F6C00000A2D0B11051F0F6F6B00000A2B057E6F00000A007DE2040004055011051F106F6C00000A2D0B11051F106F6B00000A2B057E6F00000A007DE3040004055011051F116F6B00000A7DE4040004055011051F126F6B00000A7DE504000405507BCD04000413081108450200000002000000020000002B022B0A0550167DCD0400042B0005507BCD04000417FE0116FE01130711072D320005507BC80400041BFE0116FE01130711072D1D000550186F3E01000616FE01130711072D0A000550187DCD04000400000000DE14110514FE01130711072D0811056F1E00000A00DC0000DE14110414FE01130711072D0811046F1E00000A00DC00171306DE0B260000DE00001613062B000011062A414C000002000000990200000E020000A7040000140000000000000002000000F7010000C8020000BF04000014000000000000000000000030000000A9040000D904000005000000010000011B3003007401000065000011000313047E6F00000A0B7E6F00000A0C7E6F00000A0D047E6F00000A51731500000A0A0672CFAA01706F1600000A2606720BAB01706F1600000A26067247AB01706F1600000A26067283AB01706F1600000A260672BFAB01706F1600000A2600066F1700000A056F1800000A05731900000A13050011056F1A00000A7215AC0170166F1B00000A028C2D0000016F1C00000A0011056F5F00000A1306002B53001106166F6C00000A2D0A1106166F6B00000A2B057201000070000B1106176F6C00000A2D0A1106176F6B00000A2B057201000070000C1106186F6C00000A2D0A1106186F6B00000A2B057201000070000D0011066F6000000A130811082DA000DE14110614FE01130811082D0811066F1E00000A00DC0000DE14110514FE01130811082D0811056F1E00000A00DC001104722DAC0170076FB400000A130411047239AC0170086FB400000A13041104724DAC0170096FB400000A130404110451171307DE0B260000DE00001613072B000011072A414C0000020000009C000000660000000201000014000000000000000200000074000000A60000001A0100001400000000000000000000005F00000006010000650100000500000001000001133002000C000000090000110002035F03FE010A2B00062A13300300D80200006600001100731500000A0A06725FAC01706F1600000A26067281AC01706F1600000A260672A5AC01706F1600000A260672DDAC01706F1600000A2606723FAD01706F1600000A260672A5AD01706F1600000A260672B7AD01706F1600000A260672F3AD01706F1600000A26067235AE01706F1600000A2606727DAE01706F1600000A260672C7AE01706F1600000A2606720BAF01706F1600000A2606724FAF01706F1600000A26067295AF01706F1600000A260672D3AF01706F1600000A26067213B001706F1600000A2606725BB001706F1600000A260672A5B001706F1600000A260672F5B001706F1600000A26067247B101706F1600000A26067291B101706F1600000A260672DDB101706F1600000A26067223B201706F1600000A26067247B201706F1600000A26067267B201706F1600000A260672A5B201706F1600000A260672E7B201706F1600000A2606722BB301706F1600000A2606726DB301706F1600000A260672B7B301706F1600000A26067203B401706F1600000A26067247B401706F1600000A2606728DB401706F1600000A260672A5B201706F1600000A2606726DB301706F1600000A260672B7B301706F1600000A26067203B401706F1600000A26067247B401706F1600000A260672DBB401706F1600000A26067223B201706F1600000A260672CA5400706F1600000A26067213B501706F1600000A26067233B501706F1600000A26067265B501706F1600000A260672D1B501706F1600000A2606723BB601706F1600000A260672E2B601706F1600000A2606728BB701706F1600000A2606722CB801706F1600000A260672CFB801706F1600000A26067239B901706F1600000A260672E0B901706F1600000A26067289BA01706F1600000A2606722ABB01706F1600000A260672CDBB01706F1600000A26067278BC01706F1600000A260672D8BC01706F1600000A2606723EBD01706F1600000A26066F1700000A026F1800000A02731900000A0B070C2B00082A1B300300890100006700001100160A731500000A0B0772A8BD01706F1600000A2607726BBE01706F1600000A2607722EBF01706F1600000A260772F1BF01706F1600000A260772B4C001706F1600000A26077277C101706F1600000A2607723AC201706F1600000A260772FDC201706F1600000A260772C0C301706F1600000A26077283C401706F1600000A26077246C501706F1600000A26077209C601706F1600000A260772CCC601706F1600000A2607728FC701706F1600000A26077252C801706F1600000A26077215C901706F1600000A260772D8C901706F1600000A2607729BCA01706F1600000A2607725ECB01706F1600000A2600076F1700000A036F1800000A03731900000A0C00086F1A00000A7221CC01701E6F1B00000A198C220000026F1C00000A00086F1A00000A720D0500701E6F1B00000A028C2D0000016F1C00000A00086F1A00000A724FCC01701E6F1B00000A168C220000026F1C00000A00086F1D00000A26170A00DE120814FE01130411042D07086F1E00000A00DC0000DE072600160A00DE0000060D2B00092A000000011C00000200010164650112000000000000ED008E7B0107370000011B300300630000002D00001100041554007269CC0170036F1800000A03731900000A0A00066F1A00000A720D0500701E6F1B00000A028C2D0000016F1C00000A0004066F4200000AA54C00000154170BDE1A0614FE010C082D07066F1E00000A00DC260000DE0000160B2B0000072A00011C0000020017002F4600100000000000000400525600050100000113300200120000000900001100027BD004000403283A0100060A2B00062A000003300200790000000000000002167DC604000402167DC704000402157DC80400040272010000707DC90400040272010000707DCA04000402177DCB04000402187DCC04000402167DCD04000402156A7DCE04000402166A7DCF04000402167DD00400040216731100000A7DD104000402177DD204000402167DD304000402283000000A002A000000133003002D0000006800001100022C0B026FB600000A16FE022B0116000D092D0872010000700C2B0E1200021628440100060B060C2B00082A00000013300300390000006900001100022C0B026FB600000A16FE022B0116000D092D0872010000700C2B1A021200120128430100060D092D0872010000700C2B04060C2B00082A1E02283000000A2A0000001B300400A00100006A000011001E8D54000001130A0372010000705104165400026FB600000A1F0DFE0416FE01131311132D0900161312DD6C01000073690100060A061F406FB700000A00067EE80400047EE90400046FB800000A130D156A1314121428B900000A130C02110C28BA00000A16FE0216FE01131311132D0900161312DD210100000228BB00000A1307110728BC00000A130E110E73BD00000A130F110F110D1673BE00000A13101110110A161E6FBF00000A26110A1628C000000A1308120828B900000A1000021F141F306F8B00000A10000216176FC100000A0B02171A6FC100000A0C021B1F0D6FC100000A0D021F12186FC100000A1304077200CD0170285500000A16FE01131311132D0900161312DD8C000000161305091104286600000A28BB00000A1309110928BC00000A130B1613112B12001105110B111191581305001111175813111111110B8E69FE04131311132DE0110520F52600005E13057204CD017011058C4B000001287F00000A1306081106285500000A16FE01131311132D0600161312DE1803095104110428C200000A54171312DE072600161312DE000011122A411C00000000000013000000820100009501000007000000010000011B300400460100006B000011000272010000705100036FB600000A1F0DFE01130D110D2D090016130CDD200100000416FE01130D110D2D090016130CDD0D01000073690100060A061F406FB700000A00067EE80400047EE90400046FC300000A130673C400000A1307110711061773BE00000A1308037212CD0170048C2D000001287F00000A286600000A28BB00000A1304110428BC00000A1305160D1613092B1000091105110991580D00110917581309110911058E69FE04130D110D2DE20920F52600005E0D7204CD0170098C4B000001287F00000A0C08037212CD0170048C2D000001287F00000A283C00000A0B0728BB00000A1304110428BC00000A130A1108110A16110A8E696FC500000A0011086FC600000A0011076FC700000A130B02110B1628C000000A130E120E28B900000A510202501F141F306F8B00000A5117130CDE07260016130CDE0000110C2A0000411C00000000000008000000330100003B0100000700000001000001133003002B0000001D000011007204CD0170028C2D000001287F00000A7220CD0170038C27000001287F00000A286600000A0A2B00062A7E000302161A6FC100000A28C800000A5404021A6FC900000A28CA00000A552A00133003003D0000006C0000110003120212012843010006130511052D06001613042B230716FE01130511052D06001613042B1308120012032846010006000206FE0113042B0011042A00000013300300220000006D000011000403510217FE0116FE010C082D0D000304120028430100060B2B04170B2B00072A00000000000023215E48475E2423464453484A76622D13300300290000006E0000111F108D5400000125D0F604000428CC00000A80E8040004178D540000010A0616179C0680E90400042A1E02283000000A2AEE02283000000A000002036FCD00000A740400001B7DED04000402038E697DEE0400040220000100008D540000017DEF04000402285201000600002A00000013300100070000000900001100170A2B00062A0013300100070000000900001100170A2B00062A0013300100070000001E00001100170A2B00062A0013300100070000001E00001100170A2B00062A00133005005D0100006F00001100027BF204000416FE01130411042D12000228AA00000A6FCE00000A73CF00000A7A0314FE0116FE01130411042D1100722ECD01707246CD017073D000000A7A0E0414FE0116FE01130411042D11007290CD017072AACD017073D000000A7A0416321C0E05163217040558038E69300F0E0505580E048E69FE0216FE012B011600130411042D0C0072F6CD017073D100000A7A0405580C38AE0000000002027BF0040004175820000100005DD27DF004000402027BF1040004027BEF040004027BF0040004915820000100005DD27DF1040004027BEF040004027BF0040004910B027BEF040004027BF0040004027BEF040004027BF1040004919C027BEF040004027BF1040004079C027BEF040004027BF004000491027BEF040004027BF1040004915820000100005DD20A0E040E05030491027BEF040004069161D29C0004175810020E05175810050408FE04130411043A45FFFFFF050D2B00092A00000013300600400000007000001100027BF204000416FE010C082D12000228AA00000A6FCE00000A73CF00000A7A058D540000010A02030405061628500100062602285201000600060B2B00072A13300400910000007100001100160B2B1000027BEF0400040707D29C000717580B07027BEF0400048E69FE040D092DE102167DF004000402167DF1040004160C160B2B490008027BEF040004079158027BED04000407027BEE0400045D915820000100005D0C027BEF04000407910A027BEF04000407027BEF04000408919C027BEF04000408069C000717580B07027BEF0400048E69FE040D092DA82A00000013300300540000000900001100027BF20400040A062D4800027BED04000416027BED0400048E6928D200000A00027BEF04000416027BEF0400048E6928D200000A0002167DF004000402167DF104000402177DF20400040228D300000A00002A4A0228D400000A0000021F407DD500000A002A0013300100070000001E000011001E0A2B00062A0013300200160000000900001100031EFE010A062D0C007246CE017073D600000A7A2A1E0073D700000A7A1E0073D700000A7A0000133001000C0000006E00001100178D540000010A2B00062A13300200220000000900001100032C0B038E6917FE0216FE012B0117000A062D0C00728ECE017073D600000A7A2A000013300500190000007200001100178D130000010B07161E1E1673D800000AA2070A2B00062A000000133005001D0000007200001100178D130000010B07161E20000800001E73D800000AA2070A2B00062A00000013300100070000007300001100190A2B00062A00133002001600000009000011000319FE010A062D0C0072C2CE017073D600000A7A2A000013300100070000007400001100170A2B00062A00133002001600000009000011000317FE010A062D0C007202CF017073D600000A7A2A0A002A0000001330020026000000750000110073D900000A0A026FDA00000A1E5B8D540000010B06076FDB00000A0002076FDC00000A002A000013300100100000007600001100724ACF017028640100060A2B00062A133003005B00000077000011000214FE0116FE010B072D1100725ACF0170726ACF017073D000000A7A02729ACF01701928DD00000A16FE010B072D090073690100060A2B2002724ACF01701928DD00000A16FE010B072D090073650100060A2B04140A2B00062A2A0228540100060000002A0000133002008D0000007800001100027BF304000416FE010B072D12000228AA00000A6FCE00000A73CF00000A7A0314FE0116FE010B072D110072A2CF017072B0CF017073D000000A7A038E692C0F038E692000010000FE0216FE012B0116000B072D0C0072F0CF017073D600000A7A042C0B048E6917FE0216FE012B0117000B072D0C007234D0017073D600000A7A03734B0100060A2B00062A000000133003000E00000079000011000203046FB800000A0A2B00062A4600020328DE00000A0002177DF30400042A5602285401000600000273650100067DF4040004002A000013300100110000001E00001100027BF40400046FDF00000A0A2B00062A00000013300200230000000900001100031EFE010A062D0C007276D0017073D600000A7A027BF4040004036FE000000A002A0013300100110000001E00001100027BF40400046FE100000A0A2B00062A3E00027BF4040004036FE200000A002A00000013300100110000006E00001100027BF40400046FE300000A0A2B00062A3E00027BF4040004036FE400000A002A00000013300100110000006E00001100027BF40400046FE500000A0A2B00062A3E00027BF4040004036FDC00000A002A00000013300100110000001E00001100027BF40400046FDA00000A0A2B00062A3E00027BF4040004036FB700000A002A00000013300100110000007A00001100027BF40400046FE600000A0A2B00062A00000013300100110000007A00001100027BF40400046FE700000A0A2B00062A00000013300100110000007300001100027BF40400046FE800000A0A2B00062A3E00027BF4040004036FE900000A002A00000013300100110000007400001100027BF40400046FEA00000A0A2B00062A3E00027BF4040004036FEB00000A002A3A00027BF40400046FEC00000A002A3A00027BF40400046FED00000A002A0013300300990000007800001100027BF504000416FE010B072D17000228AA00000A6FCE00000A72B2D0017073EE00000A7A0314FE0116FE010B072D110072E4D0017072F2D0017073D000000A7A038E692C0F038E692000010000FE0216FE012B0116000B072D0C007232D1017073D600000A7A042C0B048E6917FE0216FE012B0117000B072D0C007276D1017073D600000A7A027BF404000403046FB800000A0A2B00062A000000133003000E00000079000011000203046FB800000A0A2B00062A0000133002003E0000000900001100027BF50400040A062D320002177DF5040004027BF404000414FE010A062D1500027BF40400046FEF00000A0002147DF4040004000228D300000A00002A00001B3005002E0100007B000011000E061A540E0772D50A0070510E087201000070510E04166A550E0516731100000A81080000010072B8D1017073F100000A0B00076FF200000A00076FF300000A0C000872E8D101706F1200000A0000020304050828010000060A060E060E070E08280700000600067B2D04000416FE0116FE01130411042D34000E04067B25040004550E05067B280400046F0D01000681080000010E0616540E07721AD20170067B2C040004286600000A510000DE1A0D00000872E8D101706F1300000A0000DE05260000DE0000097A00DE0A00086FF400000A0000DC0000DE120814FE01130411042D07086F1E00000A00DC0000DE120714FE01130411042D07076F1E00000A00DC0000DE240D000E061A540E0772D50A0070510E087232D20170096F7600000A286600000A5100DE00002A0000014C00000000B30010C300050100000100004F0062B1001A3700000102004F007FCE000A00000000020042009ADC00120000000002003300BFF200120000000000002700E1080124370000011B3008001B0100007C000011000E0B1A540E0C72D50A0070510E0D7201000070510072B8D1017073F100000A0C00086FF200000A00086FF300000A0D0009728ED201706F1200000A000073F40000060B070E076A7D41040004070E057D42040004070E086A7D43040004070E067D440400040716731100000A7D45040004070E0A7D46040004020304050E04070E090928020000060A060E0B0E0C0E0D28080000060000DE1C1304000009728ED201706F1300000A0000DE05260000DE000011047A00DE0A00096FF400000A0000DC0000DE120914FE01130511052D07096F1E00000A00DC0000DE120814FE01130511052D07086F1E00000A00DC0000DE261304000E0B1A540E0C72D50A0070510E0D72C2D2017011046F7600000A286600000A5100DE00002A00014C000000009D0010AD00050100000100003D005D9A001C3700000102003D007CB9000A000000000200300097C700120000000002002100BCDD00120000000000001500DEF30026370000011B3008001B0100007C000011000E0B1A540E0C72D50A0070510E0D7201000070510072B8D1017073F100000A0C00086FF200000A00086FF300000A0D00097220D301706F1200000A000073F40000060B070E076A7D41040004070E057D42040004070E086A7D43040004070E067D440400040716731100000A7D45040004070E0A7D46040004020304050E04070E090928030000060A060E0B0E0C0E0D28080000060000DE1C13040000097220D301706F1300000A0000DE05260000DE000011047A00DE0A00096FF400000A0000DC0000DE120914FE01130511052D07096F1E00000A00DC0000DE120814FE01130511052D07086F1E00000A00DC0000DE261304000E0B1A540E0C72D50A0070510E0D724ED3017011046F7600000A286600000A5100DE00002A00014C000000009D0010AD00050100000100003D005D9A001C3700000102003D007CB9000A000000000200300097C700120000000002002100BCDD00120000000000001500DEF30026370000011B300400F20000007B000011000516731100000A81080000010E041A540E0572D50A0070510E067201000070510072B8D1017073F100000A0B00076FF200000A00076FF300000A0C000872A6D301706F1200000A00000203040828040000060A05067B290400046F0D0100068108000001060E040E050E0628070000060000DE1A0D00000872A6D301706F1300000A0000DE05260000DE0000097A00DE0A00086FF400000A0000DC0000DE120814FE01130411042D07086F1E00000A00DC0000DE120714FE01130411042D07076F1E00000A00DC0000DE240D000E041A540E0572D50A0070510E0672D2D30170096F7600000A286600000A5100DE00002A0000014C0000000077001087000501000001000049002C75001A37000001020049004992000A0000000002003C0064A000120000000002002D0089B600120000000000002100ABCC0024370000011B300700000100007B000011000E061A540E0772D50A0070510E087201000070510072B8D1017073F100000A0B00076FF200000A00076FF300000A0C00087228D401706F1200000A0000020304050E040E050828050000060A060E060E070E08280700000600067B2D04000416FE0116FE01130411042D14000E060E041F646A5869540E077201000070510000DE1A0D0000087228D401706F1300000A0000DE05260000DE0000097A00DE0A00086FF400000A0000DC0000DE120814FE01130411042D07086F1E00000A00DC0000DE120714FE01130411042D07076F1E00000A00DC0000DE240D000E061A540E0772D50A0070510E08724CD40170096F7600000A286600000A5100DE00002A014C000000008500109500050100000100003D004683001A3700000102003D0063A0000A00000000020030007EAE00120000000002002100A3C400120000000000001500C5DA00243700000142534A4201000100000000000C00000076322E302E35303732370000000005006C0000006C750000237E0000D8750000FC83000023537472696E677300000000D4F900009CD401002355530070CE020010000000234755494400000080CE0200A41C000023426C6F620000000000000002000010579FA2290902000000FA253300160000010000006A00000068000000F6040000830100009803000003000000F4000000D103000018000000010000007C0000000E0000005200000089000000040000000100000001000000020000001B00000000000A0001000000000006006B07640706007207640706007707640706009F0782070600B00764070600BC0782070A00F107DB070600060864070A005D08CF070600DB0964070A00FC09CF070600430A640706002A5B64070600DF5CD35C0A00206ACF070A00976ACF070A00B16ACF070A006C6BDB070600EC6F820706001B70820706003870820706001C72FD710600B278A0780600C978A0780600E678A07806000579A07806001E79A07806003779A07806005279A07806006D79A07806008679FD7106009A79FD710600A879A0780600C179A0780600F179DE798F00057A00000600347A147A0600547A147A06008C7A64070600927A64070A00AB7ADB070A00C87ADB070A00EE7ADB070A00FB7ACF070600057B64070A001E7B0B7B0A00347B0B7B0600617B64070600967B64070A00AB7BCF0706000F5464070A00D47BCF070600557C64070A005C7CCF070600857C64070A008F7CCF070600C77CB47C0A00077DCF070A00287DCF070A004D7DDB070A006E7D0B7B0A00F07DDB070A000C7E0B7B0A00487EDB070600557E64070600647E64070600D27E64070A00017FCF070600127F64070A00297FCF070A00657F0B7B0A00B67F0B7B0600F37F64070A001080CF0706002C8064070600488064070A00A980CF070600C88064070600E380D9800600F080D98006000481D98006001181D9800A0048812D8106005D81640706006281640706007781640706008D81D98006009A8182070600A781D9800600AE81820706004C82147A0600678264070600A282147A0600B18264070600B78264070600E78264070600FF82640706001583640706003183640706005283820706006983640706007F8382070600988382070A00AE832D810A00C4830B7B0A00E7830B7B0000000001000000000001000100810110001F002400050001000100010010002F0024000500010009000100100044002400050001000C00010010005A002400050009001E00010010006F0024000500090020000100100082002400050013002200010100009800240009001700420001010000A200240009002A00420001010000BC00240009003400420001010000D100240009004100420001010000DE00240009004C00420001010000EF00240009006B01420001010000FC002400090005024200010100000C012400090013024200010100001801240009001A024200010100002501240009001E024200010100003701240009002D024200010100004B0124000900310242000101000061012400090036024200010100007A01240009003B024200010100009701240009003E02420001010000B901240009004102420001010000CC01240009005302420001010000E601240009005802420001010000F401240009006702420001010000010224000900720242000101000010022400090076024200010100002402240009007C0242000101000038022400090080024200010100004E02240009008402420001010000680224000900AB02420001010000730224000900C402420001010000890224000900C902420001010000A00224000900CE02420001210000B80224000900D402420001010000CD0224000900DF02420001010000DD0224000900E402420001010000EB02240009003503420001010000FE02240009005E034200010100000903240009006203420001010000180324000900650342000100100029032400050068034200010010004003240005007B036A00010100005403240009008C038D000101000067032400090095038D00010100007C0324000900A0038D0001010000960324000900A4038D0001010000AD0324000900A9038D0001010000BF0324000900AF038D0001010000D20324000900B8038D0001010000DE0324000900BE038D0001010000EB0324000900C4038D0001010000F80324000900C9038D0001010000090424000900D0038D0001010000160424000900D3038D00010010002F0424000500D8038D00010100004D0424000900DE039A0001010000560424000900E1039A0001010000730424000900E6039A0001010000830424000900EA039A0001010000990424000900EE039A0001010000A60424000900F7039A0001010000C60424000900FB039A0001010000D10424000900FF039A0001010000E1042400090005049A0081011000EA04240005000F049A0002010000F604000009001004ED00020100000905000009001804ED00020010002205000005001E04ED00020010003405000005002504EE00020010004705000005002F04F300020010005705000005004104F400020010006705000005004704F500020010007905000005004D04FD00020100009105000009005004FE0002001000A205000005005C04FE0002001000B305000005005F04030102001000C705000005006E04040102001000E205000005008004050102001000F105000005008904190102001000FE05000005008B042401020100000A06000009009D04250102010000220600000900A004250102010000310600000900A4042501020100003E0600000900A904250102010000540600000900AD042501020100006F0600000900B104250102010000960600000900B504250101001000A60624000500BB04250102010000B30600000900BE04320181011000C40624000500C204320102010000CD0600000900C2043E0102001000DD0600000500C6043E0101001000EA0600000500E704400101001000F40600000500E704430102010000FF0600000900EA044B0100011000090700000500ED044B0181001000210700001900F304540101011000250700008C01F304650101011000340700008C01F4046901810110004D0724000500F6047F0100000000078200000500F604840113010000718200007101F704840101006708850001006F088900010086088D000100940890000100A40893000100B60890000100C30893000100D008900006006D09930006007E098D00060089098D0006009009900006009D0990000600A6098D000600B90990000600CE0946010600E40949010600EE094D010100670885000100040A560101006F08890001000A0A930006068D0B8D005680950BA40256809A0BA4025680AE0BA4025680CC0BA4025680E40BA4025680FE0BA40256801F0CA4025680400CA4025680610CA4025680820CA4025680A30CA4025680C40CA4025680E50CA4025680060DA40256801D0DA40256802F0DA40256804D0DA4025680700DA40206068D0B8D005680910D02035680990D020356809D0D02035680A10D02035680AC0D02035680B20D02035680B70D02035680BC0D02035680C00D020306068D0B8D005680910D1A035680D30D1A035680DB0D1A035680E20D1A035680EC0D1A035680F40D1A035680FD0D1A035680090E1A0356800F0E1A035680230E1A0356802D0E1A035680410E1A0306068D0B8D0056804D0E4D0156805A0E4D0156805F0E4D0156806C0E4D015680790E4D015680840E4D0156808D0E4D0156809E0E4D015680AD0E4D015680B60E4D0106068D0B8D0056804D0E32035680C00E32035680CD0E32035680DB0E32035680E50E32035680F00E32035680F80E32035680010F320356800F0F32035680280F320356802F0F320356803F0F32035680500F32035680660F320356807D0F32035680880F32035680960F32035680A40F32035680B80F32035680C60F32035680D30F32035680E70F32035680F90F320356800A10320356801C10320356802410320356803910320356804810320356806410320356807310320356808510320356809010320356809F1032035680AA1032035680BC1032035680CA1032035680D81032035680F410320356800111320356800F11320356802411320356803711320356804A11320356805F1132035680771132035680911132035680A91132035680C41132035680D41132035680E61132035680F811320356800812320356802212320356803A12320356804812320356806112320356807612320356808D1232035680A61232035680BD1232035680D21232035680EE1232035680FE12320356801513320356803313320356804413320356805313320356806013320356807B13320356808913320356809E1332035680AE1332035680C01332035680D91332035680F713320356801414320356803914320356805F14320356808514320356809E1432035680BC1432035680D91432035680F314320356801215320356802815320356803E15320356805315320356806715320356807B1532035680931532035680A71532035680B81532035680CD1532035680E11532035680F51532035680171632035680441632035680681632035680991632035680AD1632035680D11632035680F516320356801A1732035680431732035680621732035680881732035680AE1732035680D21732035680F617320356801718320356803818320356804D1832035680721832035680A31832035680CF1832035680FF1832035680291932035680531932035680851932035680AE1932035680D71932035680EA1932035680FE1932035680111A32035680241A32035680491A32035680631A320356807E1A32035680911A32035680B61A32035680DA1A32035680FF1A32035680231B32035680421B32035680611B320356807F1B320356809C1B32035680AF1B32035680C11B32035680DE1B32035680FC1B32035680191C320356802C1C320356803E1C32035680561C320356806F1C32035680811C32035680941C32035680A31C32035680B31C32035680D91C32035680FD1C32035680261D32035680561D32035680691D32035680741D32035680831D32035680A21D32035680B31D32035680C81D32035680D81D32035680E91D32035680011E320356801E1E32035680381E32035680561E32035680701E32035680841E320356809D1E32035680BC1E32035680CF1E32035680E41E32035680F31E32035680091F32035680131F320356803B1F32035680631F32035680921F32035680C11F32035680EF1F320356801D2032035680312032035680542032035680772032035680972032035680B72032035680E02032035680092132035680312132035680582132035680752132035680902132035680A62132035680C82132035680E42132035680FF21320356801C22320356803322320356805022320356806C22320356808A2232035680A42232035680BF2232035680DA2232035680FA22320356802623320356804C2332035680712332035680982332035680B92332035680E023320356800624320356802E24320356804524320356805F24320356807E2432035680952432035680B52432035680D624320356800825320356803625320356805C2532035680802532035680982532035680B02532035680C72532035680DE2532035680F525320356800C26320356801E2632035680492632035680742632035680932632035680B52632035680DF26320356800D27320356802F27320356805D27320356806C27320356808F27320356809B2732035680B12732035680C02732035680E727320356800428320356802A28320356804628320356806A28320356808D2832035680A42832035680C12832035680DF2832035680F728320356801829320356803729320356805229320356806D2932035680832932035680992932035680B02932035680C72932035680E62932035680072A320356802A2A320356804D2A32035680632A320356807C2A320356809F2A32035680C22A32035680E62A320356800A2B320356802D2B320356804F2B32035680672B32035680842B32035680A62B32035680BF2B32035680DD2B32035680F92B320356801A2C32035680362C32035680572C32035680732C32035680942C32035680AA2C32035680C12C320306068D0B8D005680D92C77085680DE2C77085680E52C77085680ED2C77085680F82C77085680042D77085680152D77085680242D770856802D2D77085680382D770856804F2D77085680592D77085680642D77085680782D77085680842D77085680912D77085680A32D77085680B02D77085680BC2D77085680CE2D77085680DE2D77085680ED2D77085680092E77085680192E77085680292E77085680312E77085680452E77085680562E770856806C2E770856807A2E77085680942E77085680A22E77085680B62E77085680CC2E77085680DF2E77085680ED2E77085680052F77085680182F770856802B2F77085680372F77085680622F77085680752F77085680822F770856808E2F77085680A52F77085680C62F77085680D72F77085680E82F770856800530770856801730770856802C30770856803F30770856804C30770856806030770856807A30770856808930770856809E3077085680B73077085680D13077085680E13077085680EF30770856800231770856801631770856803231770856804231770856805E31770856807531770856808B31770856809A3177085680AB3177085680B83177085680CA3177085680DB3177085680E53177085680FC31770856801432770856802032770856803E32770856805A32770856807C32770856809B3277085680C43277085680E53277085680F83277085680153377085680323377085680453377085680713377085680923377085680B33377085680D13377085680EF33770856801134770856803D34770856806334770856808B3477085680B63477085680DC34770856800935770856802E35770856805335770856806D3577085680853577085680A03577085680AF3577085680C23577085680DA3577085680ED35770856800B36770856802336770856803A36770856805336770856806836770856808236770856809B3677085680B63677085680C53677085680D636770856800337770856802D37770856804637770856805D37770856806B3777085680763777085680873777085680A23777085680C33777085680D13777085680EB37770856800938770856802538770856803838770856804B38770856805F38770856807338770856808F3877085680AD3877085680C53877085680DD3877085680FD38770856801D3977085680303977085680463977085680653977085680813977085680A13977085680BA3977085680D73977085680F33977085680143A77085680303A77085680513A770856806D3A770806068D0B8D005680950B3E0956808E3A3E0956809C3A3E095680A03A3E095680A73A3E095680C13A3E095680D13A3E095680E83A3E095680F73A3E0956800C3B3E095680113B3E095680273B3E0956803B3B3E0906068D0B8D005680473B47095680573B470956806D3B47095680793B47095680883B47095680A03B470906068D0B8D005680950B4B0956809C3A4B095680A03A4B0906068D0B8D005680B93B4F095680C03B4F095680C73B4F095680D13B4F095680DE3B4F095680E83B4F095680F73B4F0956800C3C4F095680173C4F095680213C4F095680343C4F095680B6364F095680C5364F09568046374F0906068D0B8D005680B93B53095680C03B53095680493C530906068D0B8D0056804E3C57095680563C570956805B3C57095680623C570906068D0B8D0056804E3C5B095680673C5B0956806C3C5B095680743C5B0906068D0B8D005680793C5F095680813C5F0906068D0B8D005680A03C63095680A53C630906068D0B8D005680950B67095680AD3C67095680BA3C67095680C63C67095680D53C67095680DE3C67095680EF3C67095680F63C67095680063D670956801D3D67095680323D670956804B3D67095680583D67095680623D67095680783D67095680863D67095680943D670906068D0B8D0056809E3D8E095680A23D8E095680AA3D8E095680AF3D8E0906068D0B8D005680910D920956809C3A920956801F0092095680C33D92095680CC3D92095680D13D92095680DE3D92095680EA3D92095680FA3D92095680013E920956800A3E92095680183E920956802A3E920956803B3B920906068D0B8D005680323E96095680443E960956804C3E960956805B3E96095680D13D96095680623E96095680910D96095680763E960956808F3E960956809E3E960906068D0B8D005680DB0DA9095680AC3EA9095680BB3EA90906068D0B8D005680C33EAD095680D33EAD095680ED3EAD095680003FAD095680113FAD0906068D0B8D005680910DB1095680233FB10956802D3FB10906068D0B8D005680910DB5095680393FB5095680433FB50906068D0B8D0056804E3CB9095680503FB9095680663FB90956807C3FB90956808F3FB9095680A63FB9095680BA3FB9095680CD3FB9095680E73FB90956800140B90956801A40B90956803140B90956804640B90956805840B90956807140B90956808440B90956809740B9095680A740B9095680B340B9095680BF40B9095680CF40B9095680E840B9095680FF40B90956801641B90956802D41B90956804041B90956806341B90956807041B90956808641B90956809A41B9095680A941B9095680BC41B9095680CB41B9095680E141B9095680FE41B90956801C42B90956803B42B90956805F42B90906068D0B8D0056808242BD0956808942BD0956809842BD095680A742BD095680BC42BD095680C642BD095680DA42BD095680EC42BD095680FD42BD0956800C43BD0956801643BD0956802743BD0956803443BD0956803F43BD0956805043BD0956806543BD0956807743BD0956809143BD095680A843BD095680BE43BD095680DA43BD095680EC43BD095680F443BD0956800144BD0906068D0B8D0056801344C70956801F44C70956802F44C70956805044C70906068D0B8D0056806C44CC0956807144CC0956807844CC0956808544CC0906068D0B8D005680950BD10956809544D10956809B44D1095680A544D1095680B144D10906068D0B8D005680BC44D6095680C544D60956809544D6095680CA44D6095680D544D6095680E444D6095680F444D60956800645D60956800B45D60956801245D60906068D0B8D0056801A45E50956802745E50956803045E5095680F90FE50906068D0B8D0056804745EA095680F00EEA095680F80EEA0956804F45EA0956807D0FEA0956805945EA0956806645EA0956807345EA0956808145EA0956808D45EA0956809E45EA095680FE19EA095680EA19EA095680691DEA095680B845EA095680741DEA095680011EEA0956801A45EA095680D145EA095680DC45EA095680E945EA095680F945EA0956801446EA0956803346EA0956804246EA0956805746EA0956806846EA0956808046EA0956803711EA0956809446EA095680B446EA095680BF46EA095680CC46EA095680E146EA0956801C10EA0956802410EA0956800047EA0956801347EA0956802147EA0956802E47EA0956803B47EA0956805247EA0956807447EA0956808947EA0956807521EA095680F90FEA0956809F47EA095680C447EA0956807E24EA095680E647EA095680FF47EA0956801A48EA095680A62BEA0956803A48EA0956805648EA0956800C26EA0956806E48EA0956808448EA0956809F48EA095680B648EA095680D248EA095680EB48EA095680B127EA095680F648EA0956800349EA0956801D49EA0956803649EA0956805349EA0956806D29EA0956808329EA0956809929EA095680B029EA095680C729EA095680E629EA0956803729EA0956805229EA095680072AEA0956802A2AEA0956806849EA0956807649EA0906068D0B8D0056808242300A56808949300A56809249300A56809B49300A5680A549300A5680AE49300A5680BE49300A5680D249300A5680DC49300A5680E649300A5680EF49300A5680FB49300A56800F4A300A56801B4A300A5680274A300A56803A4A300A5680444A300A56804E4A300A56805B4A300A56806C4A300A56807F4A300A5680934A300A56809F4A300A5680AE4A300A5680BE4A300A5680D04A300A5680092E300A5680E14A300A5680F94A300A5680144B300A5680324B300A5680454B300A5680574B300A56805D37300A5680684B300A56806B37300A5680724B300A5680F443300A56800144300A5680854B300A06068D0B8D005680954B350A56809A4B350A5680A14B350A06068D0B8D005680AF4B3A0A5680B74B3A0A06068D0B8D005680C34B3F0A5680D64B3F0A0100E54B440A0100F14B90000100FF4B900001000E4C440A01001D4C440A0100294C440A01003D4C440A0100514C440A01005E4C440A01006B4C440A01007D4C440A01008B4C8D0001009A4C46010100B14C46010100CA4CD6090100D44C93000100EB4C480A0100034D440A01001B4DD1090100D65093000100E550D1090100EC5090000100FE50900001000A519000010018518D000100275190000100E54B440A01003951900001004951900001006051460101007351900001008A5190000100A15190000100AB5149010100BC51E5090100CF51900006068D0B8D005680B454D00A5680B854D00A5680BE54D00A5680C354D00A5680C854D00A5680CD54D00A5680D254D00A5680D754D00A06068D0B8D005680B854D50A56804F45D50A5680DD54D50A5680443ED50A5680F254D50A5680FB54D50A56800755D50A56801355D50A56802455D50A56803155D50A06068D0B8D0056804455DA0A56804C55DA0A56805055DA0A06068D0B8D005680954BDF0A56805355DF0A56806055DF0A56806D55DF0A06068D0B8D0056804455E40A56807255E40A56808655E40A56808E55E40A56809555E40A06068D0B8D005680A655E90A5680AE55E90A5680B655E90A5680BC55E90A5680C455E90A5680CC55E90A5680D455E90A5680DB55E90A06068D0B8D005680E355EE0A5680EC55EE0A5680F155EE0A5680F655EE0A56800156EE0A06068D0B8D005680910DF30A56800756F30A56801356F30A56802D56F30A56803656F30A06068D0B8D0056804E3CF80A56804F56F80A56806156F80A56806C56F80A06068D0B8D0056808156FD0A56808656FD0A56808B56FD0A56809256FD0A5680A156FD0A5680B256FD0A06068D0B8D005680BD56020B5680C556020B06068D0B8D005680CE56070B5680D656070B5680E256070B5680FA56070B01001157930001001E578D0001002957440A01003257900001003D57D6090100A151900006068D0B8D00568015580C0B5680A03A0C0B06068D0B8D005680954B110B56801C58110B56802158110B5680DE3B110B06068D0B8D005680954B160B56802A58160B56802E58160B06068D0B8D0056806D551B0B568035581B0B568038581B0B06068D0B8D005680950B200B56803C58200B56804858200B56805458200B56805758200B56805A58200B56806558200B56806D58200B06068D0B8D005680950B250B56807558250B56808A58250B06068D0B8D005680950B2A0B5680A3582A0B5680A9582A0B06068D0B8D005680954B2F0B5680B1582F0B5680B3582F0B5680B5582F0B5680B7582F0B06068D0B8D005680B958340B5680CA58340B5680DD58340B5680E758340B5680F158340B56800859340B56801859340B56802859340B56804559340B56806559930006068D0B8D0056806D60700F56807060700F56807660700F56808B31700F56808560700F56809660700F5680A660700F06068D0B8D0056804455750F5680B660750F5680BE60750F5680C260750F5680D260750F0600D860930006007E098D000600EA0690000600E66046010600F160750F0600FE607A0F06001761460106002B61930006003961460106004A617A0F060055617A0F060065617A0F060070617A0F06008161440A06002C54900006008861700F060093617F0F0600EF0077080600CC6190000600BC42930006002B6193000600D86093000600D861460106000062440A06000E624601060018628B0F06001F62460106002662930006003762900F06004962900006005D624601060065627A0F060076627A0F060091627A0F0600A0627A0F0600AB6293000600B762440A0600C46293000600CD62440A0600D762440A0600E562440A0600EC62440A0600FD62440A06000D63440A06001D63440A06002F63440A0600DE2C950F06002A64440A06003664440A06004264440A06068D0B8D0056806D609A0F56804E649A0F568059649A0F568064649A0F56806E649A0F568076609A0F56808B319A0F568085609A0F56807E649A0F568096609A0F5680A6609A0F060091647A0F060088619A0F060093617F0F06002B6193000600BC4293000600A96485000600BA64440A0600C664440A0600CF64440A0600DD64440A0600EB64440A0600F664440A060001657A0F060009657A0F060010657A0F06005D624601060065627A0F060076627A0F0600EF0077080600CC61900006002B6193000600BC42930006007E098D0006009009900006006E644601060014657A0F060093617F0F060027659300060009657A0F060031659300060010657A0F06001F62460106002662930006003762900F06004962900006005D62460106003865440A06004365440A06005365440A06006665440A06006F65460106008265460106008F6546010600A365440A0600B265440A060001657A0F06008161440A0600BC4293000600B36646010600BB668D000600C7668D000600D3668D0006002C5490000600E06647090600E76693000600FE669300060013678D00060025674F0906003E67460106006067440A060081679300060092677A0F0600A5677A0F0600B267440A0600BE67900006068D0B8D005680CB6752105680DA67521006068D0B8D005680F46757105680FF67571056801468571006068D0B8D0056802B685C10568042685C10568059685C10568070685C1006068D0B8D0056808268611056809B6861105680B768611006068D0B8D005680DF68661056800A69661056803269661006068D0B8D00568053696B1056807A696B105680AA696B1006068D0B8D0056804E3C70105680D86970105680E56970105680F46970105680FF6970100100156A85000100E55085000100286A751006068D0B8D005680BE60B0105680D56AB0105680DC6AB01006068D0B8D005680CA6B18115680DC6B18115680EE6B181106007E098D000600F96B8D000600006C920906000D6C90000600126C90000600B366460106001F6CA9090600266C18110600FE66930006002F6C93000600406C8D000600496C440A06005C6C460106006A6C460106008C6C8D000600680293000600956C930006009F6C46010600B66C46010600D06C46010600DE6C8D000600F86C8D000600156D900006002C6D90000600406D90000600546D900006006E6D90000600836D930006009C6D90000600A76D900006002354900006004C5490000600AF6D8D005180CC6D8D003100DC6D2D113100F16D2D1106068D0B8D005680950B61115680896E611101008D6E2D1101009B6E8D000100A66E2D110100B76E66110100BE6E66110100C56E46010100BF7046010100EB70CF110100C56E460113018E823B1B502000000000960000080A0001002C210000000096000E08160006002422000000009600150816000E00CC23000000009600190828001600DC24000000009600270833001A001C28000000009100310842002100482D000000009600410856002700E02D000000009600410863002B00A02E0000000086004B0870002F001C2F0000000086005108780031009E2F000000008618570881003500A82F000000008600DF0896003500D02F000000008600F0089B0036000C3000000000860004098100380074320000000086185708A100380004330000000086185708A70039009D330000000086185708AF003C00B0330000000086185708B6003F0034340000000086000F09BE00430064340000000086000F09CE004A009C340000000086000F09DF005200C8340000000086000F09F0005A0000350000000086000F090301640034350000000086000F0917016F00183700000000960013092C017B00C837000000008608250933017D002A380000000086003D0981007D003C38000000008600430937017D00AC3800000000860054093C017E0014420000000086004B0870007F0058420000000086185708810081006042000000008600F709510181007D420000000086185708810081008842000000008608180A51018100A0420000000086082F0A5A018100AC42000000008600040981008200C7450000000086185708A1008200FC450000000086185708600183002C460000000086000F096701850050460000000086000F0972018A0078460000000086000F097E019000A0460000000086000F098B019700CC460000000086000F0998019E00F8460000000086000F09A601A60024470000000086000F09B601AF0050470000000086000F09C501B70080470000000086000F09D601C100B4470000000086000F09E801CC00F8470000000086000F09FB01D7003C480000000086000F090F02E300CC4B0000000086004E0A3602F200F04B0000000086004E0A4002F6002C4D0000000086005A0A4C02FB00D84D0000000086006E0A36020001E44E000000008100850A57020401C0530000000086009A0A96000A01F453000000008600B00A65020B012854000000008600C60A65020C015C54000000008600DC0A65020D019054000000008600F70A6B020E01C454000000008600080B71020F011055000000008600310B79021101445500000000860054093C011101A47E0000000096003B0B7D021201448E0000000096005A0B8E0218017492000000008608254D4D0A1C018C92000000008608324D65021C0198920000000086083F4D33011D01B092000000008608564D96001D01BC920000000086086D4D520A1E01D492000000008608804D560A1E01E092000000008608934D520A1F01F892000000008608A74D560A1F010493000000008608BB4D4D0A20011C93000000008608CA4D650220012893000000008608D94D790221014093000000008608E64D5B0A21014C93000000008608F34D600A220164930000000086080A4E640A22017093000000008608214E4D0A23018893000000008608314E650223019493000000008608414E4D0A2401AC930000000086084F4E65022401B8930000000086085D4E4D0A2501DC930000000086086B4E4D0A2501F4930000000086087F4E650225010094000000008608934E4D0A26011894000000008608A74E650226012494000000008608BB4E4D0A27013C94000000008608C94E650227014894000000008608D74E4D0A28016094000000008608E54E650228016C94000000008608F34E4D0A29018494000000008608064F650229019094000000008608194F600A2A01A894000000008608324F640A2A01B4940000000086084B4F690A2B01CC94000000008608564F6F0A2B01D894000000008608614F760A2C01F0940000000086086D4F7C0A2C01FC94000000008608794F830A2D011495000000008608914F890A2D012095000000008608A94F4D0A2E013895000000008608C14F65022E014295000000008618570881002F016495000000008608E55133012F017C95000000008608F55196002F0188950000000086080552760A3001A0950000000086080E527C0A3001AC950000000086081752520A3101C4950000000086082A52560A3101D0950000000086083D52520A3201E8950000000086084A52560A3201F4950000000086085752520A33010C960000000086086652560A33011896000000008608755279023401309600000000860885525B0A34013C960000000086089552520A35015496000000008608A352560A35016096000000008608B1524D0A36017896000000008608BE52650236018496000000008608CB52520A37019C96000000008608DC52560A3701A896000000008608ED52520A3801C0960000000086080053560A3801CC960000000086081353520A3901E4960000000086082553560A3901F0960000000086083753520A3A0108970000000086084953560A3A0114970000000086085B53600A3B012C970000000086086A53640A3B0138970000000086087953520A3C0150970000000086088653560A3C015C970000000086089353B30A3D017497000000008608A1536B023D018097000000008608AF53B80A3E019897000000008608C353BE0A3E01A497000000008608D753520A3F01BC97000000008608ED53560A3F01C697000000008618570881004001D4970000000086084D5779024001EC970000000086085A575B0A4001F89700000000860867574D0A410110980000000086087257650241011C980000000086087D57520A420134980000000086088957560A420140980000000086089557690A43015898000000008608A6576F0A43016498000000008608B757330144017C98000000008608C557960044018898000000008608D357520A4501A098000000008608DF57560A4501AA98000000008618570881004601B4980000000096008559420B4601C09E000000009600A159520B4B01809F000000009600B9595D0B4D01A49F000000009600B9596D0B52012CA1000000009100D459840B5A01C8AA000000009600EB599E0B65012CAC000000009600EB59AB0B6A01E8AC000000009600FB59BA0B6F0154AE000000009600135AC40B7401E4AF0000000096002D5ACD0B78015CB4000000009600425AE40B8101A8B7000000009600585AF30B880158B80000000096006E5AFB0B8B018CBC0000000096008B5A0F0C930140BF000000009600AA5A1F0C980130C8000000009100BF5A300C9E01D4C9000000009100E05A3D0CA201F8CA000000009100FC5A460CA50198D1000000009600115B520CA8015CD3000000009600335B5E0CAD0100D60000000091004C5B6E0CB201ECD6000000009600685B7A0CB50148DA000000009600815B8D0CBB010CDC0000000096009E5BA00CC301BCDC000000009600BC5BA70CC501ECDF000000009600D65BB90CCB0190E3000000009600F45BA00CCF0158E6000000009600195CA00CD10160EA000000009600325CC20CD30190EB0000000096004D5CCD0CD70184EC0000000096006C5CD60CDA01A8EC000000009100835CE70CE10160EE0000000096006C5CF00CE40150F6000000009600A55C050DED01ACF6000000009100BC5C100DF00158F7000000009600A55C1A0DF2016003010000009600ED5C250DF5018005010000009600035D300DF801A4060100000096001A5D390DF901CC060100000096001A5D450DFD01F406010000009600325D520D02021807010000009600325D650D08027407010000009600325D7B0D0F029C07010000009600325D890D1302C407010000009600325D990D1802EC07010000009600325DAC0D1E021408010000009600325DC00D24028C08010000009600325DD90D2C020409010000009600325DF30D35022809010000009600325D050E3C0214150100000096004B5DA00C4402C4150100000096005B5D1A0E4602DC16010000009600715D230E4902781C0100000096008C5D2F0E4E024022010000009600A75D490E56026022010000009600A75D550E5B02FC23010000009600C05D620E61027828010000009100E05D770E69023829010000009600025E7F0E6B02A02A010000009600155E8A0E7002DC2C0100000091001E5E960E7702D02F010000009600455EF30B7C028030010000009600675EA30E7F02E8340100000096009B5EB20E850204350100000096009B5EC00E89021C350100000096009B5ECF0E8E02C037010000009100BA5EE30E95028439010000009100D05EEB0E9702F039010000009100DE5EF20E9802743B010000009100FC5EF20E9A02C03C0100000091001B5FF20E9C020C3E0100000091003A5FF20E9E02D83F0100000091004D5FEB0EA002B4400100000091005C5FFB0EA1029842010000009600785F0D0FA7022C44010000009100975F1B0FAD021445010000009600B65F240FB0026C46010000009600D55F360FB902C047010000009600F95F400FBC02B84B0100000091001260A00CC002A44C01000000910032604B0FC202B84D0100000091004360530FC5020C540100000096005B60680FCC02085501000000861857088100CF022C550100000086009C61830FCF025C55010000008600A561560AD1028D55010000008600AD618100D2029855010000008608B861520AD202B85501000000861857088100D202285601000000861857088100D202E45601000000861857088100D202385701000000860840634D0AD202605701000000860855634D0AD202785701000000860867634D0AD202905701000000860879634D0AD202C05701000000860889634D0AD202D8570100000086089F634D0AD202F057010000008608B2634D0AD202135801000000861857088100D202275801000000861857088100D20230580100000086009C619F0FD2026058010000008600A561560AD4029158010000008600AD618100D5029C58010000008608B861520AD502B95801000000861857088100D502E05801000000861857088100D502945901000000861857088100D5024C5A01000000C600C165520AD502AB5A01000000861857088100D502C05A0100000086185708A70FD502505B0100000086185708AE0FD602B45B0100000086185708B80FD902DC5B0100000086185708C50FDE02305C010000008608CA654D0AE502545C010000008608DE654D0AE5026C5C010000008608F5654D0AE5029C5C0100000096080666D60FE502B45C0100000086080F66DC0FE502D05D0100000096081E66E20FE5021C5E0100000096082A66E20FE702685E0100000096083966EE0FE902B85E0100000096084566EE0FEB02D45E01000000C6005366F80FED02F45E01000000C6005A667902EE020C5F0100000096006666FD0FEE02285F01000000960066660610EF02A45F01000000E6016D661010F102146001000000C600C165520AF102466001000000861857088100F102686001000000861857082010F102906001000000861857082710F202B06001000000960806663010F402C8600100000096081E663610F40204610100000096082A663610F602406101000000960839664210F8027C6101000000960845664210FA02986101000000C6005366F80FFC02B86101000000C6005A667902FD02D06101000000861857088100FD02796201000000861857088100FD02A0620100000086082D6A7910FD02CE620100000086083A6A7F10FD02FC62010000008600476A8610FE024864010000008600536A560A0203B8640100000086005F6A560A0303EC640100000086006E6A8E1004035465010000008600776A520A0503A0650100000086007D6A560A0503EC65010000008100856A81000603F0660100000081008A6A931006037C67010000008100A46A99100703D067010000008100BD6AA11009031468010000009600E66AB5100C031069010000009600F86ABC100E03F06A0100000096000C6BC9101303006D010000009600F86AD3101803246D010000009600F86ADE101B03446D010000009600266BDE101E03646D010000009100F86AE9102103A472010000009600466BF610260370740100000096005D6B00112A038874010000009600776B07112C036C77010000009600926BB5102D032079010000009600B66B0E112F03AC790100000086007B6C1D113203CC79010000008618570881003303547A010000009600B66D23113303907A010000009600C16D23113403D57A010000008618570881003503E07A010000009600056E31113503A87C010000009600196E3A113803187E0100000096002D6E42113B034F7E010000009600436E48113D03707E0100000096005A6E51114003BC7E010000009600786E57114203357F010000008618570881004503007F0100000091180082371B45033D7F0100000086185708691145037C7F01000000E609CD6E600A4603907F01000000E609E36E600A4603A47F01000000E609026F79024603B87F01000000E609156F79024603CC7F01000000E601296F6F114603388101000000E601386F7A114B038481010000008100856A81004E03248201000000E6014C6F81004E038482010000008418570881004E03988201000000C608A06F79024E03AC8201000000C608AE6F5B0A4E03CE8201000000C608BC6F79024F03D68201000000C608CD6F5B0A4F03E08201000000C608DE6F83115003F88201000000C608E56F69115003288301000000C608F56F88115103508301000000C6080970881151037C8301000000C60826708E115103908301000000C6082F7093115103B48301000000C608447099115203C88301000000C60850709E115203EA8301000000C6005C7081005303F08301000000C60067708100530324840100000096007370A411530340840100000096007370AA115303A784010000008618570881005403B48401000000C600CB70C6115403508501000000C600DB70C61156036A8501000000C4004C6F640A58037C85010000008618570881005903948501000000C608A06F79025903B48501000000C608AE6F5B0A5903E48501000000C608BC6F79025A03018601000000C608CD6F5B0A5A03148601000000C608DE6F83115B03318601000000C608E56F69115B03448601000000C608FA7083115C03618601000000C608027169115C03748601000000C6080A7179025D03918601000000C60816715B0A5D03A48601000000C608F56F88115E03C48601000000C608097088115E03E48601000000C60826708E115E03018701000000C6082F7093115E03148701000000C608447099115F03318701000000C60850709E115F03418701000000C6005C7081006003508701000000C600677081006003608701000000C600CB70C6116003088801000000C600DB70C611620324880100000081004C6F8100640370880100000096002E71D4116403F8890100000096004771E7116D036C8B0100000096006171E7117B03E08C0100000096007871001289032C8E0100000096008E711012900300000100A07100000200A97100000300B67100000400130900000500C47100000100A07100000200A97100000300B671000004001309000005002B6100000600C87100000700A56700000800C47100000100A07100000200A97100000300B671000004001309000005002B6100000600C87100000700A56700000800C47100000100130900000200A07100000300DA7100000400C47100000100130900000200A07100000300A97100000400B67100000500ED7100000600F45700000700C47100000100C40600000200F571000003002B6100000400C87100000500A56700000600C47100000100D86902000200886102000300297202000400347200000100F469020002008861020003002972020004003472000001006F00000002003E72000001007E09000002009009000003002B61000004003E72000001002B6100000100BC4200000200EA06000001006F00000001006F00000002007E09000003009009000001007E09000002009009000003002B61000001007E09000002009009000003002B6100000400CC6100000100035400000200BC42000003000F54000004004572000005005472000006005E7200000700A56700000100035400000200BC42000003000F54000004004572000005005472000006005E7200000700A567000008002B6100000100035400000200BC42000003000F54000004004572000005005472000006005E7200000700A56700000800687200000100035400000200BC42000003000F54000004004572000005005472000006005E7200000700A567000008006872000009007E0900000A00900900000100035400000200BC42000003000F54000004004572000005005472000006005E7200000700A567000008006872000009007E0900000A00900900000B00707200000100035400000200BC42000003000F54000004004572000005005472000006005E7200000700A567000008006872000009007E0900000A00900900000B00707200000C002B6100000100BC4200000200C47100000100787200000100C471000001006F00000002003E72000001007E72000001006F00000001006F0000000200900900000100035400000200EF0000000300F45700000400BC4200000500847200000100035400000200EF0000000300F45700000400BC4200000500847200000600927200000100035400000200EF0000000300F45700000400BC42000005008472000006009D7200000700A77200000100035400000200EF0000000300F45700000400BC4200000500847200000600927200000700FB5700000100035400000200EF0000000300F45700000400BC42000005008472000006009D7200000700FB5700000800A77200000100035400000200EF0000000300F45700000400BC42000005008472000006009D7200000700FB5700000800AE7200000900A77200000100035400000200EF0000000300F45700000400BC4200000500847200000600927200000700FB5700000800AE7200000100035400000200EF0000000300F45700000400BC4200000500847200000600927200000700FB5700000800AE7200000900C37200000A00CE7200000100035400000200EF0000000300F45700000400BC42000005008472000006009D7200000700FB5700000800AE7200000900C37200000A00CE7200000B00A77200000100035400000200EF0000000300F45700000400BC4200000500847200000600927200000700FB5700000800AE7200000900C37200000A00CE7200000B00E37200000100035400000200EF0000000300F45700000400BC42000005008472000006009D7200000700FB5700000800AE7200000900C37200000A00CE7200000B00E37200000C00A77200000100035400000200EF0000000300F45700000400BC4200000500847200000600927200000700FB5700000800AE7200000900C37200000A00CE7200000B00E37200000C00ED7200000D00090400000E00FE7210100F00A77200000100035400000200BC4200000300847200000400057300000100035400000200BC4200000300847200000400057300000500AE7200000100035400000200BC4200000300847200000400057300000500147300000100035400000200BC4200000300847200000400057300000100035400000200BC4200000300847200000400057300000500257300000600AE72000001002E73000001005472000001005E72000001005E7200000100DB0900000100387300000200467300000100C47100000100EF0000000200F457020003005E7202000400547200000500847210100600A77200000100EF00000002005E72000003005472101004005273000001007E72000001007E72000001007E72000001007E72000001007E72000001007E72000001007E72000001007E72000001007E72000001007E72000001007E72000001007E72000001007E72000001007E72000001007E72000001007E72000001007E72000001007E72000001007E72000001007E72000001007E72000001007E72000001007E72000001007E72000001007E72000001007E72000001007E72000001007E72000001007E72000001007E72000001007E72000001007E72000001007E72000001007E72000001007E72000001007E72000001007E72000001007E72000001007E72000001007E72000001007E72000001007E72000001005B7300000200BC4202000300627302000400727300000500C471000001007C7300000200C47100000100D86000000200C40600000300F57100000400827300000500C47100000100D86000000200C40600000300F57100000400827300000500F160000006008B7300000700176100000800C47100000100F57100000200D860000003007E0900000400006C00000500900900000600827300000700F160000008008B7300000900176102000A009D7300000B00C47100000100F45700000200A47300000300AC7302000400B273000005003E7200000100F45700000200A47300000300AC7302000400B273000005003E7200000100BC42000002007E09000003002B6100000400BC7300000500C47100000100BC42000002007E09000003002B6100000400C47100000100BC42000002007E0900000300006C00000400827300000500F160000006008B73020007002B61020008005B7300000900C47100000100BC42000002000C0100000300CF73000004007E0900000500006C020006002B6100000700C471000001002B6100000200BC4200000300C471000001000F5400000200BC42000003007E0900000400006C00000500D87300000600F273020007002B6100000800C47100000100006C000002002B6100000300C87102000400007400000500C471000001007E0900000200006C000003009009000004007C73020005009D7300000600C471000001002B61000002000674000003000C7400000400C47100000100BC4202000200167400000300C471000001002B74000002009D7300000300C47100000100BC42000002002B61000003007E0900000400327400000500C471000001003F74000002002B74000003009D7300000400006C00000500C47100000100DD0600000200096500000300C471000001002B6100000200DE2C00000300096500000400106500000500E52C00000600C471000001002B61000002005574000003006574000004007574000005008074000006008C74000007009D7400000800C47100000100BC4200000200C47100000100BC42000002002B6102000300AA7402000400BA7402000500C774000006003E7200000100BC42000002002B6100000300D860000004003E7200000100BC42000002003E7200000100BC42000002003E72000001002B6100000200D87400000300E474000004003E7200000100F45700000200E24F000003003E7200000100BC42000002007E09000003002B6100000400D86000000500ED7402000600F574000007003E7202000100B27000000200BC4200000300C47100000100BC42000002007E09000003002B6100000400D86000000500ED7400000600FE7402000700F574020008000D75000009003E72000001002B6102000200C77400000300C47102000100266C00000200C47100000100B30500000200936100000300C471000001002B6102000200B30500000300C471000001007C7300000100BC42000002001A7502000300267500000400C47100000100BC42000002001A75000003003E7502000400267500000500C47100000100BC42000002005275000003001A7502000400587502000500B26700000600C47100000100BC42000002005275000003001A75000004005E7502000500587502000600B26700000700C47100000100BC4200000200527502000300587500000400C47100000100BC42000002005275020003005875020004002B6100000500C47100000100BC4200000200527502000300587502000400B267020005002B6100000600C47100000100BC42000002005275020003005875020004002B61020005006C7500000600C47100000100BC42000002005275000003001A7502000400587502000500B267020006002B61020007006C7500000800C47100000100BC42000002005275000003001A75000004003E7502000500587502000600B267020007002B61020008006C7500000900C47100000100BC42000002001309000003005275000004001A75000005003E7502000600FE0500000700C47100000100BC42000002001309000003005275000004001A75000005003E75000006005E7502000700FE0500000800C47100000100BC4200000200C47100000100BC4200000200BB66000003003E72000001007E0900000200D86002000300DE3B02000400787500000500C471000001005B7300000200BC4200000300D860000004005561020005007061020006008C75020007009F7500000800C47100000100AE75000002002B6100000300556100000400827300000500C47100000100AE75000002002B6100000300556100000400827300000500BE7500000600C47100000100BC4200000200DD0600000300D57500000400E67500000500AA7402000600F87502000700027600000800C47102000100B27000000200C47100000100BC42000002002C5400000300900900000400006C00000500C471000001001776000002002276000003002B76000004003676000005004076000006004C76000007003E7200000100BC42000002008B73000003007E09000004005576000005003E72000001007E09000002006576000003003E72000001007E0900000200D860000003006D7602000400DE3B02000500787500000600C47100000100BC42000002007E76000003008C76020004001F6C00000100BC42000002007E76000003008C76000004008265020005001F6C00000100BC42000002007E76000003008C76000004008265020005001F6C02000600987600000700C471000001002B7400000200AF76000001002B7400000100BF76000002002B7400000100BF76000002002B7400000100BF76000002002B7400000100BF76000002002B74000001002B7400000100CA76000002002B6102000300D87602000400E27602000500EC7600000600C471000001002B6100000200F976000003000E77000004006E6400000500EF0000000600C471000001002B6100000200936100000300C471000001002B6100000200A56700000300BC42000004007E0900000500006C00000600900900000700237702000800936100000900C471000001002B6100000200186200000300C47100000100BC42000002005B7300000300F457000004003E72000001002B6100000200C471000001007E09000002002B6100000300C47100000100C40600000200357700000300EC76000004002B74000005009D7302000600942E00000700C471000001004477000002004A7700000300C47100000100706000000200C46100000100C46100000100706000000200C46100000100C46100000100527700000100386500000200436500000300536500000100386500000200436500000300536500000400666500000500826500000100386500000200436500000300536500000400666500000500826500000600A36500000700B265000001005877000002005D77000001005877000002005D77000001005877000002005D77000001005877000002005D7700000100627700000100587700000100587700000200667700000100F105000001000165000002008161000001005877000002005D77000001005877000002005D77000001005877000002005D77000001005877000002005D77000001006277000001007E7200000100BE6000000200D56A00000300757700000400954B000001007E77000001007E77000001007E77000001008777000001007E7700000100206A000002008B7700000100206A000002008777000003009577000001007E0900000200C47100000100A07100000200A97100000300B67102000400C40600000500C471000001009E7700000200A07100000300A97100000400B67100000500C471000001007E0902000200A57700000300C47100000100900902000200A57700000300C47100000100B57702000200A57700000300C471000001007E0900000200900900000300B57702000400A57700000500C471000001007E0900000200C87702000300D17700000400C47100000100DD7700000200EA7700000100C471000001007E0900000200C471000001007E0900000200C47102000300006C00000100EA7700000100847200000100847200000100130902000200EF7702000300017802000100130900000200EF77000003000178000001000A7800000200117800000100EF77020002000A78020003001178000001000A7800000200130900000100B270000002001A78020003002C78000001002271000001003878000002004478000003005078000004005B78000005006878000001003878000002004478000003005078000001007E72000001007E72000001007E72000001007E72000001007E72000001007578000001007D78000002008478000001007D78000002008478000001008A78000001007E72000001007E72000001007E72000001007E72000001007E72000001007E72000001007E72000001007D78000002008478000001007D7800000200847800000100A07100000200A97100000300B671000004001309020005002B6102000600556102000700886102000800297202000900947800000100A07100000200A97100000300B671000004001309000005002B6100000600B76200000700CD6200000800AB6200000900C46200000A00A56700000B00E56202000C00886102000D00297202000E00947800000100A07100000200A97100000300B671000004001309000005002B6100000600B76200000700CD6200000800AB6200000900C46200000A00A56700000B00E56202000C00886102000D00297202000E00947800000100130900000200A07100000300DA7102000400016502000500886102000600297202000700947800000100130900000200A07100000300A97100000400B67100000500ED7100000600F45702000700886102000800297202000900947850000D006200110062001500B10057088100B9005708560AC1005708560AC9005708560AD1005708560AD9005708560AE1005708560AE9005708560AF1005708560AF9005708640A01015708560A09015708560A11015708560A190157082312290157085B0A310157088100410057085B0A39005409560A3900837A560A4101997A2A127100570881007100A07A64120900C165520A3900B97A6A129100570870129100DF7A7A1251010F09801271012A7B8A1279013E7B790229004C6F81003901C165520A41014E7BA6126901C165520A41014E7BAE124100557BB5124100C165520A41014E7BBD128101667BC41241002A66C41241001E66C41241004566B51241006A7BCD124100737BC41281017F7BD5124100837BB5128101927BC41251010F09DC1209005708810041019D7B19134900570881004900C07B1E139901CC7B241391010F092B13A101DF7B640AA101F17B9600A101077C9600A1011D7C640A91012D7C35134900367C3C134101997A4E134900457C551359004C7C5A13A9015277601349006E7C6513B1010F096B137901777C1010C101AA7C7902B1012D7C821359002D7C881349003D09810069016A7B9213C101D37C9913C901E17C1010C901ED7C600A7101F67C560A71011A7D9F137901387DA613E10157088100E1015C7DAD13E9010E08B31309007C7D1010A1018C7D8A120C005708EA1314005708EA1341014566071441009D7DB5121400B37D600A1C00B37D600A0C00B37D600A4101396607145900C07D44145900CE7D8E104100D57DD5127101E67D10109100FE7D4F14F901197E600AF9011E7E55147100297E5B0AE901347E5B0A0902570881001102570881004101997AD214E1015708AD13E9017A7EB31359002D7CD814410153668E10F9017F7E1315F901897E1815F901927E3701F9019B7E1D154101A47E90004100AA7EC4128101B67ED5124100BF7EB5124101997A22154101997A28151902C165520AB901B861520AF901DA7E8915F901E57E18158101667BFC156900F07E8F167100A07A931659004C7CDB1649000D7F18178101237F1F1741014E7B2A1749003A7F3017E101417FAD13E101537F3B173902717F640AE9010E08401759008B7F600A5900997F520A7901A67F520A4102D37C99137101CC7F520A4101DE7F520A4101E37FC81741003966B5126901C16533187100EB7F38184902FB7F3F18F9010580600A8101237F451879011C80611859015708681851010F09701851012D7C7918F9013380F71851003F80FD1861026A7B501941004E80571971005A805D194100678064198901C16533184100C165331871005A806A1971007380641271007A80721909005A6679024101997AC119B9015708560A71008180790241018C80520A59009480810049009B80810079005708560A49005708560A0900CC7BEB1991012D7CF1197900BD80F81969020F095A01410153660A1A7902570881007900FB801F1A89025708560A79001C81311AF9012481581A4101EB7F6F1A990257088100410181807902310016715B0A3100CB70C611A902C165520A41016981A61AA9027181AC1AB1028481B11AB90257086911C1025708B71AC902197EC31AB102BF81CB1A4101C881D21A69017181D81A3100DB70C611B90257088100C902D281FF1AC102D8818100B902E88183114902F081D81A4101C88113154902F881231BD90257088100E902CA82401BF1026D6610109901DA82520A01035708560A09035708521B11035708560AF1023D096F1B19033483781B310057088100310045838D0021035708560A290357088100990057087D1B31035708810031000A71790239038481691131000271691141015366A51B31004C6F640A3100A06F79023100AE6F5B0A3100BC6F79023100CD6F5B0A3100DE6F83113100E56F69113100FA7083113100F56F8811310009708811310026708E1131002F709311310044709911310050709E1131005C70810031006770810001035708521B31003D09810041035708810049015708560A4903D18381004901D683C61B5103F583810008006000A80208006400AD0208006800B20208006C00B70208007000BC0208007400C10208007800C60208007C00CB0208008000D00208008400D50208008800DA0208008C00DF0208009000E40208009400E90208009800EE0208009C00F3020800A000F8020800A400FD020800AC00A8020800B000AD020800B400B2020800B80006030800BC00B7020800C0000B030800C40010030800C80015030800CC00BC020800D400A8020800D800AD020800DC00B2020800E00006030800E400B7020800E8000B030800EC0010030800F00015030800F400BC020800F8001E030800FC002303080000012803080008012D0308000C01A80208001001AD0208001401B20208001801060308001C01B702080020010B0308002401100308002801150308002C012303080034012D0308003801A80208003C01AD0208004001B20208004401060308004801B70208004C010B0308005001100308005401150308005801BC0208005C011E0308006001230308006401280308006801360308006C013B03080070014003080074014503080078014A0308007C014F03080080015403080084015903080088015E0308008C016303080090016803080094016D0308009801720308009C01C6020800A00177030800A4017C030800A80181030800AC0186030800B0018B030800B40190030800B80195030800BC019A030800C0019F030800C401A4030800C801A9030800CC01AE030800D001B3030800D401B8030800D801BD030800DC01C2030800E001C7030800E401CC030800E801D1030800EC01D6030800F001DB030800F401E0030800F801E5030800FC01EA0308000002EF0308000402F40308000802F90308000C02FE03080010020304080014020804080018020D0408001C02CB02080020021204080024021704080028021C0408002C022104080030022604080034022B0408003802300408003C023504080040023A04080044023F0408004802440408004C024904080050024E0408005402530408005802580408005C025D04080060026204080064026704080068026C0408006C027104080070027604080074027B0408007802800408007C028504080080028A04080084028F0408008802940408008C029904080090029E0408009402A30408009802A80408009C02AD040800A002B2040800A402B7040800A802BC040800AC02C1040800B002C6040800B402CB040800B802D0040800BC02D5040800C002DA040800C402DF040800C802E4040800CC02E9040800D002EE040800D402F3040800D802F8040800DC02FD040800E00202050800E40207050800E8020C050800EC0211050800F00216050800F4021B050800F80220050800FC022505080000032A05080004032F0508000803340508000C03390508001003D002080014033E0508001803430508001C034805080020034D0508002403520508002803570508002C035C05080030036105080034036605080038036B0508003C037005080040037505080044037A05080048037F0508004C038405080050038905080054038E0508005803930508005C039805080060039D0508006403A20508006803A70508006C03AC0508007003B10508007403B60508007803BB0508007C03C00508008003C50508008403CA0508008803CF0508008C03D40508009003D90508009403DE0508009803E30508009C03E8050800A003ED050800A403F2050800A803F7050800AC03FC050800B00301060800B40306060800B8030B060800BC0310060800C00315060800C4031A060200C50321120800C8031F060800CC0324060800D00329060800D4032E060800D80333060800DC0338060800E003DA020800E4033D060800E80342060800EC0347060800F0034C060800F40351060800F80356060800FC035B06080000046006080004046506080008046A0608000C046F06080010047406080014047906080018047E0608001C048306080020048806080024048D0608002804920608002C049706080030049C0608003404A10608003804A60608003C04AB0608004004B00608004404B50608004804BA0608004C04BF0608005004C40608005404C90608005804CE0608005C04D30602005D04211208006004D80608006404DD0608006804E20608006C04E70602006D04211208007004EC0608007404F10608007804F60608007C04FB06080080040007080084040507080088040A0708008C040F07080090041407080094041907080098041E0708009C0423070800A00428070800A4042D070800A80432070800AC0437070800B0043C070800B40441070800B80446070800BC044B070800C00450070800C40455070800C8045A070800CC045F070800D00464070800D40469070800D8046E070800DC0473070800E00478070800E4047D070800E80482070800EC0487070800F0048C070800F40491070800F80496070800FC049B0708000005A00708000405A50708000805AA0708000C05AF0708001005B40708001405B90708001805BE0708001C05C30708002005C80708002405CD0708002805D20708002C05D70708003005DC0708003405E10708003805E60708003C05EB0708004005F00708004405F50708004805FA0708004C05FF07080050050408080054050908080058050E0808005C051308080060051808080064051D0808006805220808006C052708080070052C0808007405310808007805360808007C053B08080080054008080084054508080088054A0808008C054F08080090055408080094055908080098055E0808009C0563080800A00568080800A4056D080800A80572080800B005A8020800B405AD020800B805B2020800BC0506030800C005B7020800C4050B030800C80510030800CC0515030800D005BC020800D4051E030800D80523030800DC0528030800E00536030800E4053B030800E80545030800EC054A030800F0054F030800F40554030800F80559030800FC055E03080000067B0808000406800808000806630308000C066803080010066D0308001406720308001806C60208001C067703080020067C0308002406810308002806860308002C068B03080030069003080034069503080038069A0308003C069F0308004006A40308004406A90308004806AE0308004C06B30308005006B80308005406BD0308005806C20308005C06C70308006006CC0308006406D10308006806D60308006C06DB0308007006E00308007406E50308007806EA0308007C06EF0308008006F40308008406F90308008806FE0308008C060304080090060804080094060D0408009806CB0208009C0612040800A00617040800A4061C040800A80621040800AC062B040800B00630040800B40635040800B8063A040800BC0644040800C00649040800C4064E040800C80653040800CC0658040800D00685080800D4068A080800D80662040800DC0667040800E0066C040800E406B7040800E806CB040800EC06D0040800F006D5040800F406DA040800F806DF040800FC06E40408000007E90408000407EE0408000807F30408000C07F80408001007FD0408001407020508001807070508001C070C05080020071105080024071605080028071B0508002C072005080030072505080034072A05080038072F0508003C07340508004007BB0508004407C00508004807C50508004C07CA0508005007CF05080054078F0808005807D40508005C07D90508006007DE0508006407940808006807990808006C079E0808007007A30808007407A80808007807AD0808007C07B20808008007B70808008407BC0808008807C10808008C07C60808009007CB0808009407D00808009807D50808009C07DA080800A007DF080800A407E4080800A807E9080800AC07EE080800B007F3080800B407F8080800B807FD080800BC0702090800C00707090800C4070C090800C80711090800CC0716090800D0071B090800D40720090800D80725090800DC072A090800E0072F090800E40734090800E80739090800EC0736080800F0073B080800F40740080800F80745080800FC074A08080000084F0808000408540808000808590808000C085E0808001008630808001808A80208001C08AD0208002008B20208002408060308002808B20408002C08B70408003008BC0408003408E40408003808160508003C08BB0508004008E80508004408ED0508004808420908005008A80208005408AD0208005808B20208005C08060308006008B702080064080B0308006C08A80208007008AD0208007408B20208007C08A80208008008AD0208008408B20208008808060308008C08B702080090080B0308009408100308009808230308009C0828030800A00845030800A4084A030800A8086D030800AC0872030800B008C6020800B808A8020800BC08AD020800C008B2020800C808A8020800CC08AD020800D008B2020800D40806030800DC08A8020800E008AD020800E408B2020800E80806030800F008A8020800F408AD020800FC08A80208000009AD0208000809A80208000C09D50208001009DA0208001409DF0208001809E40208001C09E90208002009EE0208002409F30208002809F80208002C09FD02080030096B0908003409700908003809750908003C097A09080040097F0908004409840908004809890908005009A80208005409AD0208005809B20208005C090603080064092D0308006809AD0208006C090603080070090B0308007409B20408007809B70408007C09BC0408008009C10408008409C60408008809CB0408008C09D00408009009D50408009409DA0408009809DF040800A009A8020800A409AD020800A809B2020800AC0923030800B00945030800B4096D030800B8099A090800BC0939090800C0099F090800C409A4090800CC09A8020800D009AD020800D409B2020800DC09A8020800E009AD020800E409B2020800E80906030800EC09B7020800F409A8020800F809AD020800FC09B2020800040AA8020800080AAD0208000C0AB2020800140AA8020800180AAD0208001C0A28030800200A36030800240A3B030800280A4A0308002C0A4F030800300A72030800340AC6020800380A770308003C0A9A030800400A9F030800440AA4030800480AA90308004C0ACC030800500AD1030800540AD6030800580ADB0308005C0AE0030800600AE5030800640AF4030800680AF90308006C0AFE030800700A03040800740A08040800780A0D0408007C0ACB020800800A12040800840A17040800880A1C0408008C0A21040800900A26040800940A2B040800980A300408009C0A35040800A00A3A040800A40A3F040800A80A44040800B00AA8020800B40AAD020800B80AB2020800BC0A06030800C00AB7020800C40A23030800C80A28030800CC0A36030800D00A3B030800D40A40030800D80AC2090800DC0A9F030800E00AA4030800E40AA9030800E80AAE030800EC0AB3030800F00AB8030800F40ABD030800F80AB2040800FC0AB7040800000BC1040800040BBC040800080B680808000C0B6D080800140BA8020800180BAD0208001C0BB2020800200B06030800280BA80208002C0BAD020800300BB2020800340B060308003C0BA8020800400BAD020800440BB2020800480B060308004C0BB7020800540BA8020800580BAD0208005C0BB2020800600B06030800640B9F090800680BDB0908006C0BE0090800700BB7020800740B0B030800780B10030800800BA8020800840BAD020800880BB20208008C0B06030800940BA8020800980BAD0208009C0BB2020800A00B06030800A40BB7020800A80B0B030800AC0B10030800B00B15030800B40BBC020800B80B1E030800BC0B23030800C00B28030800C40B36030800C80B3B030800CC0B40030800D00BC2090800D40BC1020800D80BEF090800DC0BF4090800E00BF9090800E40B45030800E80BB2040800EC0BB7040800F00BBC040800F40BC1040800F80BC6040800FC0BCB040800000CD0040800040CD5040800080CDA0408000C0CDF040800100CE4040800140CE9040800180CEE0408001C0CF3040800200CF8040800240CFD040800280C070508002C0C0C050800300C11050800340C16050800380C1B0508003C0C20050800400C25050800440C2A050800480C2F0508004C0C34050800500C39050800540CD0020800580CFE0908005C0C030A0800600C080A0800640C0D0A0800680C120A08006C0C170A0800700C1C0A0800740C210A0800780C260A08007C0C2B0A0800800C3E050800840C43050800880C480508008C0C4D050800900C52050800940C57050800980C5C0508009C0C61050800A00C66050800A40C6B050800A80C70050800AC0C75050800B00C7A050800B40C7F050800B80C84050800BC0C89050800C00C8E050800C40C93050800C80C98050800CC0C68080800D00C6D080800D80CA8020800DC0CAD020800E00CB2020800E40C06030800E80CB7020800EC0C0B030800F00C10030800F40C15030800F80CBC020800FC0C1E030800000D23030800040D28030800080D360308000C0D3B030800100D40030800140DC2090800180DC10208001C0DEF090800200DF4090800240DF9090800280D450308002C0D4A030800300D4F030800340D54030800380D590308003C0D5E030800400D7B080800440D80080800480D630308004C0D68030800500D6D030800540D72030800580DC60208005C0D77030800600D7C030800640D81030800680D860308006C0D68080800700D6D080800740D860308007C0DA8020800800DAD020800840DB20208008C0DA8020800900DAD020800980DA80208009C0DAD020800340EA8020800380EAD0208003C0EB2020800400E06030800440EB7020800480E0B0308004C0E10030800500E15030800580EAD0208005C0EB2020800600E06030800640EB7020800680E0B0308006C0E10030800700E15030800740EBC020800780E1E0308007C0E23030800840EA8020800880EAD0208008C0EB2020800940EA8020800980EAD0208009C0EB2020800A00E06030800A80EA8020800AC0EAD020800B00EB2020800B40E10030800B80E15030800C00EA8020800C40EB7020800C80E0B030800CC0E1E030800D00E23030800D40EC1020800D80EF4090800DC0E5E030800E40EA8020800E80EAD020800EC0EB2020800F00E06030800F40EB7020800FC0EA8020800000FAD020800040FB2020800080F060308000C0FB7020800140F2D030800180FA80208001C0FAD020800200FB2020800280FA80208002C0FAD020800300FAD040800340F9F090800380FDB0908003C0FE0090800440FA8020800480FAD020800500FA8020800540FAD020800580FB20208005C0FB70208007C0FAD020800800FB2020800880FA80208008C0FAD020800900FB2020800940F060308009C0FA8020800A00FAD020800A40FB2020800AC0FA8020800B00FAD020800B40FB2020800BC0FA8020800C00FAD020800C40FB2020800C80FB7020800CC0FBC020800D00FC1020800D40FC6020800D80FCB020800E00FA8020800E40FAD020800E80FB2020800F00F2D030800F40FA8020800F80FAD0208000010A80208000410AD0208000810B20208000C10060308001010B70208001810AD0208001C10B20208002010060308002410B702080028100B0308002C10100308003010150308003410BC02080038101E030A003C10390B08004410A80208004810AD0208004C10230308005010280308005410360308005810450308005C104A0308006410A80208006810AD0208006C10B20208007010060308007410B70208004411A80208004811AD0208004C11B20208005011060308005411B70208005811230308005C112803080060113603080064113B0308006811450308006C114A0308007812AD0208007C12B20208008412A80208008812AD0208008C12B20208009412A80208009812AD0208009C12B2020800A01206030800A812A8020800AC12AD020800B012B2020800B812A8020800BC12AD020800C012B2020800C812A8020800CC12AD020800D012B2020800D812A8020800DC12AD020800E012B2020800E41206030800E812B7020800FC12A80208000013AD0208000413B20208000C13A80208001013AD0208001413B20208009C1328110800AC13A8020800B013AD022E001300EC1B2E001B00061C2E008300851C2E004300061C2E007B007C1C2E002B000C1C2E003300EC1B2E003B001B1C2E002300061C2E005300061C2E005B003C1C2E006B00661C2E007300731C03012303AD02E3022303AD02C3072303AD02E30C5B06AD020028AB05AD022028AB05AD02E02F8307AD0200308307AD0220308307AD0240308307AD0260308307AD0201001000000068003112411251128F12E712021308130E1315134413711378138D13B913D413DD13FE130D14181428142F143D1449145B1493149A14A414A914AD14B114B514BB14C114C714CC14DD140215081530158E159A15A015A915D115E115E81502162E163C166C169816B016CA16D516E11602170C1747177C178D179817A017A817B217BB17C217CE17E51717184D1880189518B318CB18DB18E818041914191C1928193819461979199519A019A819AE19B519BB19C719D919DF19E619FE19121A281A3B1A421A4D1A5D1A751A851A8E1A981A9F1ADD1A071B281B311B4A1B581B601B681B841B8D1B921B971B9F1BAE1BB51BBB1BC01BCB1BDA1B04000100070002002B0004002C0018003900290047002F004A0030004D0037005000380051003D005A003E0062003F006300430065004A0000005909420100006F009A0200007D0B9F020000D94F900A0000E24F42010000F54F950A00000450950A00001450900A00001F50990A000028509D0A00003B50900A00004750900A00005150900A00005B50900A00006B50900A00007B50900A00008550900A00008F50900A00009E509D0A0000B350A10A0000BA50A70A00004003AD0A0000C250900A00000354420100000F54A70A00001454950A00002354950A00002C54950A00003754990A0000EA06950A00004354900A00004C54950A00005954950A00006854950A00007654950A000084549D0A00008F54950A00009854C50A0000CD02CA0A0000A254950A0000EB57990A0000F457900A0000FB57950A00000904A10A00000358420100000D58950A0000C461950A0000C363900A0000D463900A0000E263900A0000F063900A0000FC63900A00000E64900A00001D64900A0000C461950A00007366900A00008366900A00009666900A0000A36614100000A8661A100000A3664C100000CC6AAA100000546F9D0A0000666F9D0A0000816F990A0000906F990A00007A70990A00008470990A00009170B11100009470B6110000A470B6110000B270BC110000B770C11100007A70990A00008470990A00009170B11100002271B11100002671990A00009470B6110000A470B6110000B270BC110000B770C11102001A000300020022000500010023000700020042000900010043000900020044000B00010045000B00020046000D00010047000D00020048000F00010049000F0002004A00110001004B00110002004C00130001004D00130002004E00150001004F001500010051001700020050001700020052001900010053001900020054001B00010056001D00020055001D00020057001F00010058001F0001005A00210002005900210001005C00230002005B00230002005D00250001005E00250001006000270002005F002700020061002900010062002900020063002B00010064002B00020065002D00010066002D00010068002F00020067002F0002006A00310001006B00310001006D00330002006C00330001006F00350002006E003500020070003700010071003700020072003900010073003900020074003B00010075003B00020076003D00010077003D00020078003F00010079003F0001007B00410002007A00410002007C00430001007D00430001007F00450002007E004500010081004700020080004700020082004900010083004900020084004B00010085004B00020086004D00010087004D00020088004F00010089004F0002008A00510001008B00510002008D00530001008E00530002008F005500010090005500020091005700010092005700020093005900010094005900010096005B00020095005B00020097005D00010098005D000200F1005F000200F50061000200F60063000200F70065000200F80067000200F90069000200FA006B000200FB006D00020001016F0002000B01710002000C01730002000D01750002000E01770002000F01790002001D017B00020026017D00010027017D0002004C017F0002004D01810002004E01830002004F01850001005601870002005501870002005701890001005801890001005A018B00020059018B0002005B018D0002005C018F0002005D01910001005E01910002005F01930001006001930002006A01950001006B01950002006C01970001006D01970002006E01990001006F019900010071019B00020070019B00020072019D00010073019D00020074019F0002007501A10002007601A30001007701A30002007801A50001007901A500E213F013F8134F1BF07E0100F604048000000100000000000000000000000000727A000002000000000000000000000001005B07000000000200000000000000000000000100CF07000000004400430045004300460043004700430048004300490043004A0043004B0043004C0043004D0043004E0043004F004300500043005100430052004300530043005400430055004300560043005700430058004300590043005B005A005D005C005E005C00610060006800670000000000003C4D6F64756C653E0053514C427573696E6573734C6F6769632E646C6C0054334753005753492E436F6D6D6F6E004163636F756E744D6F76656D656E747354657374004163636F756E744D6F76656D656E74735461626C6500436173686965724D6F76656D656E747354657374004361736869657253657373696F6E496E666F00436173686965724D6F76656D656E74735461626C65005341535F464C414753004143434F554E545F50524F4D4F5F4352454449545F54595045004143434F554E545F50524F4D4F5F5354415455530047555F555345525F5459504500434153484945525F4D4F56454D454E54004D6F76656D656E745479706500506C617953657373696F6E54797065004163636F756E745479706500436173686C6573734D6F646500506C617953657373696F6E537461747573004754506C617953657373696F6E537461747573004754506C61796572547261636B696E675370656564004754506C617953657373696F6E506C61796572536B696C6C0050656E64696E6747616D65506C617953657373696F6E5374617475730047616D696E675461626C6544726F70426F78436F6C6C656374696F6E436F756E74004163636F756E74426C6F636B526561736F6E004163636F756E74426C6F636B556E626C6F636B536F75726365005465726D696E616C54797065730048414E445041595F54595045005465726D696E616C537461747573004143434F554E54535F50524F4D4F5F434F5354005757505F434F4E4E454354494F4E5F54595045005757505F434F4E4E454354494F4E5F53544154555300545950455F4D554C5449534954455F534954455F5441534B530053657175656E6365496400506172616D617465727354797065466F725369746500434153484945525F53455353494F4E5F5354415455530043757272656E637945786368616E6765537562547970650043757272656E637945786368616E676554797065005472616E73616374696F6E54797065004F7065726174696F6E436F64650043617368696572566F75636865725479706500556E646F5374617475730046554C4C5F4E414D455F54595045005743505F5465726D696E616C547970650043757272656E637945786368616E6765526573756C740042616E6B5472616E73616374696F6E446174610047524F55505F454C454D454E545F54595045004558504C4F49545F454C454D454E545F54595045005061727469636970617465496E436173684465736B447261770055706772616465446F776E6772616465416374696F6E004177617264506F696E7473537461747573004C616E67756167654964656E7469666965720053595354454D5F4D4F4445005041545445524E5F54595045004754506C6179657254797065004361676543757272656E6379547970650043757272656E6379547970650053746174757355706461746543757272656E6379547970650043617368696572436F6E636570744F7065726174696F6E526573756C74005053415F54595045004361736869657253657373696F6E5472616E73666572537461747573005465726D696E616C426F7854797065005465726D696E616C43617368446972656374696F6E00466C61674275636B6574496400486F6C646572486170707942697274684461794E6F74696669636174696F6E0050696E5061645479706500486F6C6465724C6576656C54797065004275636B65744964004D756C746950726F6D6F7300537461727453657373696F6E53746174757300537461727453657373696F6E5472616E736665724D6F646500537461727453657373696F6E496E70757400537461727453657373696F6E4F757470757400456E6453657373696F6E496E70757400506C61796564576F6E4D6574657273005449544F53657373696F6E4D6574657273005449544F53657373696F6E4D657465727343617368496E00456E6453657373696F6E53746174757300456E6453657373696F6E4F757470757400496E53657373696F6E506172616D657465727300506C617953657373696F6E436C6F7365506172616D6574657273004163636F756E7442616C616E63650050726F6D6F42616C616E6365004163636F756E74496E666F004163636F756E7450726F6D6F74696F6E73416374696F6E00414C41524D5F424952544844415900524551554553545F5459504500524551554553545F545950455F524553504F4E53450047414D455F474154455741595F52455345525645445F4D4F444500454E554D5F47414D455F474154455741595F50524F4D4F5F42414C414E43455F414354494F4E00496E53657373696F6E416374696F6E0050726F76696465724C6973740050726F76696465724C69737454797065005465726D696E616C0043726564697473506C61794D6F6465005465726D696E616C496E666F00547261636B4461746100436172644E756D6265720043726970744D6F646500415243466F75724D616E616765645472616E73666F726D0052433400415243466F75724D616E616765640052433443727970746F5365727669636550726F76696465720053716C50726F63656475726573006D73636F726C69620053797374656D004F626A65637400456E756D0049436C6F6E6561626C650053797374656D2E53656375726974792E43727970746F677261706879004943727970746F5472616E73666F726D0049446973706F7361626C650053796D6D6574726963416C676F726974686D0053797374656D2E446174610053797374656D2E446174612E53716C436C69656E740053716C5472616E73616374696F6E00537461727400446563696D616C0055706461746500456E64004163636F756E745374617475730053656E644576656E74005570646174655F496E7465726E616C005472616E736C617465005465737431005465737432002E63746F7200446174615461626C65006D5F7461626C65006D5F636173686965725F73657373696F6E5F696E666F006D5F7465726D696E616C5F6964006D5F7465726D696E616C5F6E616D65006D5F706C61795F73657373696F6E5F6964006D5F747261636B5F64617461006D5F6163636F756E745F6964006D5F636173686965725F6E616D6500536574506C617953657373696F6E4964005365744163636F756E74547261636B4461746100496E6974536368656D61004164640045787465726E616C547261636B44617461006765745F4C61737453617665644D6F76656D656E74496400436C6561720053617665644D6F76656D656E747349640053617665004C61737453617665644D6F76656D656E744964004361736869657253657373696F6E4964005465726D696E616C496400557365724964005465726D696E616C4E616D6500557365724E616D6500417574686F72697A6564427955736572496400417574686F72697A65644279557365724E616D650049734D6F62696C6542616E6B004461746554696D650047616D696E6744617900557365725479706500436F70790044617461526F77006D5F726F77006D5F6D6F76656D656E745F6964006765745F4361736869657253657373696F6E496E666F007365745F446174615461626C65546F53617665004E756C6C61626C6560310041646445786368616E67650041646443757272656E637945786368616E67650041646443617368416476616E636545786368616E67650041646445786368616E67655F496E7465726E616C0041646452656C617465644964496E4C6173744D6F7600416464537562416D6F756E74496E4C6173744D6F7600536574416464416D6F756E74496E4C6173744D6F7600536574496E697469616C42616C616E6365496E4C6173744D6F760053657444617465496E4C6173744D6F7600536574496E697469616C416E6446696E616C42616C616E6365416D6F756E74496E4C6173744D6F7600526F7773436F756E7400476574436173686965724D6F76656D656E74416464537562416D6F756E7400476574436173686965724D6F76656D656E7442616C616E6365496E6372656D656E7400446174615461626C65546F536176650076616C75655F5F004E4F4E45005553455F455854454E4445445F4D455445525300435245444954535F504C41595F4D4F44455F5341535F4D414348494E45005553455F48414E445041595F494E464F524D4154494F4E005350454349414C5F50524F47524553534956455F4D45544552005553455F4D455445525F46495845445F344244435F4C454E4754485F42495430005553455F4D455445525F46495845445F344244435F4C454E4754485F42495431005553455F4D455445525F46495845445F344244435F4C454E4754485F42495432005553455F4D455445525F46495845445F344244435F4C454E4754485F42495433005553455F4D455445525F46495845445F354244435F4C454E4754485F42495430005553455F4D455445525F46495845445F354244435F4C454E4754485F42495431005553455F4D455445525F46495845445F354244435F4C454E4754485F42495432005553455F4D455445525F46495845445F354244435F4C454E4754485F42495433005553455F464C4147535F464F5243455F4E4F5F52544500414C4C4F575F53494E474C455F42595445005341535F464C4147535F49474E4F52455F4E4F5F4245545F504C415953005341535F464C41475F44495341424C45445F4146545F4F4E5F494E54525553494F4E005341535F464C4147535F4146545F414C54484F5547485F4C4F434B5F5A45524F00554E4B4E4F574E004E5231004E52320052454445454D41424C4500504F494E5400554E523100554E5232004E52330052454445454D41424C455F494E5F4B494E440041574152444544004143544956450045584841555354454400455850495245440052454445454D4544004E4F545F41574152444544004552524F520043414E43454C4C45445F4E4F545F41444445440043414E43454C4C45440043414E43454C4C45445F42595F504C415945520050524541535349474E4544004E4F545F41535349474E45440055534552005359535F4143434550544F52005359535F50524F4D4F424F58005359535F53595354454D005359535F5449544F005359535F47414D494E475F5441424C45005359535F524544454D5054494F4E005359535F4332474F00535550455255534552004F50454E5F53455353494F4E00434C4F53455F53455353494F4E0046494C4C45525F494E0046494C4C45525F4F555400434153485F494E00434153485F4F5554005441585F4F4E5F5052495A45310050524F4D4F54494F4E5F4E4F545F52454445454D41424C45005052495A455300434152445F4445504F5349545F494E00434152445F4445504F5349545F4F55540043414E43454C5F4E4F545F52454445454D41424C45004D425F4D414E55414C5F4348414E47455F4C494D4954004D425F434153485F494E005441585F4F4E5F5052495A45320050524F4D4F5F435245444954530050524F4D4F5F544F5F52454445454D41424C450050524F4D4F5F455850495245440050524F4D4F5F43414E43454C0050524F4D4F5F53544152545F53455353494F4E0050524F4D4F5F454E445F53455353494F4E00434152445F5245504C4143454D454E5400445241575F5449434B45545F5052494E540048414E445041590048414E445041595F43414E43454C4C4154494F4E004D414E55414C5F48414E44504159004D414E55414C5F48414E445041595F43414E43454C4C4154494F4E00434153485F494E5F53504C495432004D425F434153485F494E5F53504C495432004445565F53504C49543200434153485F494E5F53504C495431004445565F53504C495431004D425F434153485F494E5F53504C4954310043414E43454C5F53504C4954310043414E43454C5F53504C49543200504F494E54535F544F5F445241575F5449434B45545F5052494E54005052495A455F434F55504F4E00434152445F4352454154494F4E00434152445F504552534F4E414C495A4154494F4E004143434F554E545F50494E5F4348414E4745004143434F554E545F50494E5F52414E444F4D00434153485F494E5F434F5645525F434F55504F4E00434153485F494E5F4E4F545F52454445454D41424C4532004D425F434153485F494E5F4E4F545F52454445454D41424C45004D425F434153485F494E5F434F5645525F434F55504F4E004D425F434153485F494E5F4E4F545F52454445454D41424C4532004D425F5052495A455F434F55504F4E004E415F434153485F494E5F53504C495431004E415F434153485F494E5F53504C495432004E415F5052495A455F434F55504F4E004E415F434153485F494E5F4E4F545F52454445454D41424C45004E415F434153485F494E5F434F5645525F434F55504F4E00434152445F52454359434C454400504F494E54535F544F5F4E4F545F52454445454D41424C4500504F494E54535F544F5F52454445454D41424C450052454445454D41424C455F4445565F455850495245440052454445454D41424C455F5052495A455F45585049524544004E4F545F52454445454D41424C455F455850495245440050524F4D4F54494F4E5F52454445454D41424C450043414E43454C5F50524F4D4F54494F4E5F52454445454D41424C450050524F4D4F54494F4E5F504F494E540043414E43454C5F50524F4D4F54494F4E5F504F494E54005441585F52455455524E494E475F4F4E5F5052495A455F434F55504F4E00444543494D414C5F524F554E44494E4700534552564943455F434841524745004D425F434153485F4C4F535400524551554553545F544F5F564F55434845525F52455052494E5400434845434B5F5041594D454E5400434153485F50454E44494E475F434C4F53494E47004143434F554E545F424C4F434B4544004143434F554E545F554E424C4F434B45440043555252454E43595F45584348414E47455F53504C4954310043555252454E43595F434152445F45584348414E47455F53504C49543100434153494E4F5F43484950535F45584348414E47455F53504C495431005449544F5F5449434B45545F434153484945525F5052494E5445445F4341534841424C4500504F494E54535F544F5F52454445454D41424C455F41535F43415348494E5F53504C49543100504F494E54535F544F5F52454445454D41424C455F41535F43415348494E5F53504C4954320043555252454E43595F45584348414E47455F53504C4954320043555252454E43595F434152445F45584348414E47455F53504C49543200434153494E4F5F43484950535F45584348414E47455F53504C49543200524551554553545F544F5F5449434B45545F52455052494E540050415254494349504154494F4E5F494E5F434153484445534B5F445241570043555252454E43595F434845434B5F53504C4954310043555252454E43595F434845434B5F53504C495432005052495A455F494E5F4B494E44315F47524F5353005052495A455F494E5F4B494E44315F54415831005052495A455F494E5F4B494E44315F5441583200434C4F53455F53455353494F4E5F42414E4B5F4341524400434C4F53455F53455353494F4E5F434845434B0046494C4C45525F4F55545F434845434B005052495A455F494E5F4B494E44325F47524F5353005052495A455F494E5F4B494E44325F54415831005052495A455F494E5F4B494E44325F54415832005449544F5F5449434B45545F434153484945525F504149445F4341534841424C45005449544F5F5449434B45545F434153484945525F5052494E5445445F50524F4D4F5F52454445454D41424C45005449544F5F5449434B45545F4D414348494E455F504C415945445F4341534841424C45005449544F5F5449434B45545F434153484945525F5052494E5445445F50524F4D4F5F4E4F545F52454445454D41424C45005449544F5F5449434B45545F52454953535545005449544F5F5449434B45545F4352454154455F4341534841424C455F4F46464C494E45005449544F5F5449434B45545F4352454154455F504C415941424C455F4F46464C494E45005449544F5F5449434B45545F455850495245445F50524F4D4F5F52454445454D41424C45005449544F5F5449434B45545F455850495245445F50524F4D4F5F4E4F545F52454445454D41424C45005449544F5F5449434B45545F455850495245445F52454445454D41424C45005449544F5F5449434B45545F4341534841424C455F52454445454D45445F4F46464C494E45005449544F5F5449434B45545F504C415941424C455F52454445454D45445F4F46464C494E45005449544F5F5449434B45545F4D414348494E455F5052494E5445445F48414E44504159005449544F5F5449434B45545F4D414348494E455F5052494E5445445F4A41434B504F54005449544F5F5449434B45545F434153484945525F504149445F48414E44504159005449544F5F5449434B45545F434153484945525F504149445F4A41434B504F54005449544F5F56414C49444154455F5449434B4554005449544F5F5449434B45545F4D414348494E455F5052494E5445445F4341534841424C45005449544F5F5449434B45545F4D414348494E455F5052494E5445445F50524F4D4F5F4E4F545F52454445454D41424C45005449544F5F5449434B45545F4D414348494E455F504C415945445F50524F4D4F5F52454445454D41424C45005449544F5F5449434B45545F4D414348494E455F504C415945445F50524F4D4F5F4E4F545F52454445454D41424C45005449544F5F5449434B45545F434153484945525F504149445F50524F4D4F5F52454445454D41424C45005449544F5F5449434B45545F434153484945525F455850495245445F504149445F4341534841424C45005449544F5F5449434B45545F434153484945525F455850495245445F504149445F50524F4D4F5F52454445454D41424C45005449544F5F5449434B45545F434153484945525F455850495245445F504149445F4A41434B504F54005449544F5F5449434B45545F434153484945525F455850495245445F504149445F48414E44504159005449544F5F4C4153545F4D4F56454D454E54005452414E534645525F4352454449545F4F5554005452414E534645525F4352454449545F494E00434153485F494E5F5441585F53504C49543100504F494E54535F544F5F52454445454D41424C455F41535F43415348494E5F5052495A4500434152445F5245504C4143454D454E545F464F525F4652454500434152445F5245504C4143454D454E545F494E5F504F494E545300434153485F494E5F5441585F53504C4954320043555252454E43595F4352454449545F434152445F45584348414E47455F53504C4954310043555252454E43595F44454249545F434152445F45584348414E47455F53504C4954310043555252454E43595F4352454449545F434152445F45584348414E47455F53504C4954320043555252454E43595F44454249545F434152445F45584348414E47455F53504C49543200434153485F414456414E43455F43555252454E43595F45584348414E474500434153485F414456414E43455F47454E455249435F42414E4B5F4341524400434153485F414456414E43455F4352454449545F42414E4B5F4341524400434153485F414456414E43455F44454249545F42414E4B5F4341524400434153485F414456414E43455F434845434B00434153485F414456414E43455F43415348004E4F545F504C415945445F52454348415247455F524546554E44454400434153485F434C4F53494E475F53484F52545F4445505245434154454400434153485F434C4F53494E475F4F5645525F4445505245434154454400434153485F434C4F53494E475F53484F525400434153485F434C4F53494E475F4F5645520046494C4C45525F494E5F434C4F53494E475F53544F434B0046494C4C45525F4F55545F434C4F53494E475F53544F434B00434147455F4F50454E5F53455353494F4E00434147455F434C4F53455F53455353494F4E00434147455F46494C4C45525F494E00434147455F46494C4C45525F4F555400434147455F434153484945525F434F4C4C4543545F44524F50424F585F5445524D494E414C00434147455F434153484945525F524546494C4C5F484F505045525F5445524D494E414C00434147455F434153484945525F434F4C4C4543545F44524F50424F585F47414D494E475441424C4500434147455F434153484945525F434F4C4C4543545F44524F50424F585F47414D494E475441424C455F44455441494C00434147455F4C4153545F4D4F56454D454E540043484950535F53414C450043484950535F50555243484153450043484950535F53414C455F4445564F4C5554494F4E5F464F525F5449544F0043484950535F53414C455F544F54414C0043484950535F50555243484153455F544F54414C0043484950535F4348414E47455F494E0043484950535F4348414E47455F4F55540043484950535F53414C455F574954485F434153485F494E0043484950535F50555243484153455F574954485F434153485F4F55540043484950535F53414C455F544F54414C5F45584348414E47450043484950535F50555243484153455F544F54414C5F45584348414E47450043484950535F53414C455F52454749535445525F544F54414C0043484950535F4C4153545F4D4F56454D454E540047414D494E475F5441424C455F47414D455F4E455457494E0047414D494E475F5441424C455F47414D455F4E455457494E5F434C4F53450048414E445041595F415554484F52495A45440048414E445041595F554E415554484F52495A45440048414E445041595F564F494445440048414E445041595F564F494445445F554E444F4E45004D425F45584345535300434F4D50414E595F425F43555252454E43595F434152445F45584348414E47455F53504C49543100434F4D50414E595F425F43555252454E43595F434152445F45584348414E47455F53504C49543200434F4D50414E595F425F43555252454E43595F4352454449545F434152445F45584348414E47455F53504C49543100434F4D50414E595F425F43555252454E43595F4352454449545F434152445F45584348414E47455F53504C49543200434F4D50414E595F425F43555252454E43595F44454249545F434152445F45584348414E47455F53504C49543100434F4D50414E595F425F43555252454E43595F44454249545F434152445F45584348414E47455F53504C4954320048414E445041595F57495448484F4C44494E4700434F4D50414E595F425F43555252454E43595F45584348414E47455F53504C49543100434F4D50414E595F425F43555252454E43595F45584348414E47455F53504C49543200434F4D50414E595F425F43555252454E43595F434845434B5F53504C49543100434F4D50414E595F425F43555252454E43595F434845434B5F53504C49543200434F4D50414E595F425F434153485F414456414E43455F43555252454E43595F45584348414E474500434F4D50414E595F425F434153485F414456414E43455F47454E455249435F42414E4B5F4341524400434F4D50414E595F425F434153485F414456414E43455F4352454449545F42414E4B5F4341524400434F4D50414E595F425F434153485F414456414E43455F44454249545F42414E4B5F4341524400434F4D50414E595F425F434153485F414456414E43455F434845434B0057494E5F4C4F53535F53544154454D454E545F5245515545535400434152445F4445504F5349545F494E5F434845434B00434152445F4445504F5349545F494E5F43555252454E43595F45584348414E474500434152445F4445504F5349545F494E5F434152445F43524544495400434152445F4445504F5349545F494E5F434152445F444542495400434152445F4445504F5349545F494E5F434152445F47454E4552494300434152445F5245504C4143454D454E545F434845434B00434152445F5245504C4143454D454E545F434152445F43524544495400434152445F5245504C4143454D454E545F434152445F444542495400434152445F5245504C4143454D454E545F434152445F47454E4552494300434F4D50414E595F425F434152445F4445504F5349545F494E00434F4D50414E595F425F434152445F4445504F5349545F4F555400434F4D50414E595F425F434152445F5245504C4143454D454E5400434F4D50414E595F425F434152445F4445504F5349545F494E5F434845434B00434F4D50414E595F425F434152445F4445504F5349545F494E5F43555252454E43595F45584348414E474500434F4D50414E595F425F434152445F4445504F5349545F494E5F434152445F43524544495400434F4D50414E595F425F434152445F4445504F5349545F494E5F434152445F444542495400434F4D50414E595F425F434152445F4445504F5349545F494E5F434152445F47454E4552494300434F4D50414E595F425F434152445F5245504C4143454D454E545F434845434B00434F4D50414E595F425F434152445F5245504C4143454D454E545F434152445F43524544495400434F4D50414E595F425F434152445F5245504C4143454D454E545F434152445F444542495400434F4D50414E595F425F434152445F5245504C4143454D454E545F434152445F47454E45524943005452414E534645525F43415348494552535F53454E44005452414E534645525F43415348494552535F5245434549564500434153484945525F524546494C4C5F484F505045525F5445524D494E414C005445524D494E414C5F524546494C4C5F484F5050455200434153484945525F4348414E47455F535441434B45525F5445524D494E414C00434153484945525F434F4C4C4543545F44524F50424F585F5445524D494E414C005449544F5F5449434B45545F50524F4D4F424F585F5052494E5445445F50524F4D4F5F4E4F545F52454445454D41424C45005449544F5F5449434B45545F50524F4D4F424F585F5052494E5445445F50524F4D4F5F52454445454D41424C4500434153484945525F434F4C4C4543545F44524F50424F585F5445524D494E414C5F4341474500434153484945525F524546494C4C5F484F505045525F5445524D494E414C5F43414745005052495A455F52455F494E5F4B494E44315F47524F5353005052495A455F52455F494E5F4B494E44325F47524F5353005052495A455F52455F494E5F4B494E44315F54415831005052495A455F52455F494E5F4B494E44315F54415832005052495A455F52455F494E5F4B494E44325F54415831005052495A455F52455F494E5F4B494E44325F5441583200435553544F4D45525F454E5452414E4345005449544F5F5449434B4554535F4352454154455F45584348414E47455F4455414C5F43555252454E4359005449544F5F5449434B4554535F504C415945445F45584348414E47455F4455414C5F43555252454E43590042494C4C5F494E5F45584348414E47455F4455414C5F43555252454E4359005449544F5F5449434B45545F434153484945525F564F49445F4341534841424C45005449544F5F5449434B45545F434153484945525F564F49445F50524F4D4F5F52454445454D41424C45005449544F5F5449434B45545F434153484945525F564F49445F50524F4D4F5F4E4F545F52454445454D41424C45005449544F5F5449434B45545F4D414348494E455F564F49445F4341534841424C45005449544F5F5449434B45545F4D414348494E455F564F49445F50524F4D4F5F4E4F545F52454445454D41424C45005441585F50524F564953494F4E53005449544F5F5449434B45545F434153484945525F535741505F464F525F4348495053005441585F435553544F4459005441585F52455455524E494E475F435553544F445900434152445F4153534F43494154450043555252454E43595F45584348414E47455F544F5F4E4154494F4E414C5F43555252454E43590043555252454E43595F45584348414E47455F414D4F554E545F4F55540043555252454E43595F45584348414E47455F544F5F464F524549474E5F43555252454E43590043555252454E43595F45584348414E47455F414D4F554E545F494E005449544F5F5449434B45545F434F554E54525F5052494E5445445F4341534841424C450043555252454E43595F45584348414E47455F414D4F554E545F494E5F4348414E474500435553544F4D45525F454E5452414E43455F4341524400435553544F4D45525F454E5452414E43455F44454249545F4341524400435553544F4D45525F454E5452414E43455F4352454449545F4341524400435553544F4D45525F454E5452414E43455F434845434B0050524F4D4F54494F4E5F52454445454D41424C455F4645444552414C5F5441580050524F4D4F54494F4E5F52454445454D41424C455F53544154455F544158004332474F5F4C4F434B5F4143434F554E545F494E5445524E414C004332474F5F4C4F434B5F4143434F554E545F45585445524E414C004332474F5F434153485F494E5F494E5445524E414C004332474F5F434153485F494E5F45585445524E414C004332474F5F434153485F4F55545F494E5445524E414C004332474F5F434153485F4F55545F45585445524E414C004332474F5F41435449564154455F4143434F554E545F494E5445524E414C004332474F5F444541435449564154455F4143434F554E545F494E5445524E414C004332474F5F52454E4F4D494E4154494F4E5F4143434F554E545F494E5445524E414C004332474F5F52454E4F4D494E4154494F4E5F4143434F554E545F45585445524E414C004143434F554E545F4144445F424C41434B4C495354004143434F554E545F52454D4F56455F424C41434B4C4953540043555252454E43595F434152445F45584348414E47455F53504C4954315F554E444F0043555252454E43595F434152445F45584348414E47455F53504C4954325F554E444F00434153485F414456414E43455F43555252454E43595F45584348414E47455F554E444F00434153485F414456414E43455F47454E455249435F42414E4B5F434152445F554E444F00434153485F414456414E43455F4352454449545F42414E4B5F434152445F554E444F00434153485F414456414E43455F44454249545F42414E4B5F434152445F554E444F00434153485F414456414E43455F434845434B5F554E444F004D554C5449504C455F4255434B4554535F454E445F53455353494F4E004D554C5449504C455F4255434B4554535F454E445F53455353494F4E5F4C415354004D554C5449504C455F4255434B4554535F45585049524544004D554C5449504C455F4255434B4554535F455850495245445F4C415354004D554C5449504C455F4255434B4554535F4D414E55414C5F414444004D554C5449504C455F4255434B4554535F4D414E55414C5F4144445F4C415354004D554C5449504C455F4255434B4554535F4D414E55414C5F535542004D554C5449504C455F4255434B4554535F4D414E55414C5F5355425F4C415354004D554C5449504C455F4255434B4554535F4D414E55414C5F534554004D554C5449504C455F4255434B4554535F4D414E55414C5F5345545F4C41535400434153484945525F42595F434F4E434550545F494E00434153484945525F42595F434F4E434550545F4F555400434153484945525F42595F434F4E434550545F4C41535400506C61790043617368496E00436173684F7574004465766F6C7574696F6E005461784F6E5072697A65310053746172744361726453657373696F6E00456E644361726453657373696F6E00416374697661746500446561637469766174650050726F6D6F74696F6E4E6F7452656465656D61626C65004465706F736974496E004465706F7369744F75740043616E63656C4E6F7452656465656D61626C65005461784F6E5072697A65320050726F6D6F437265646974730050726F6D6F546F52656465656D61626C650050726F6D6F457870697265640050726F6D6F43616E63656C0050726F6D6F537461727453657373696F6E0050726F6D6F456E6453657373696F6E00437265646974734578706972656400437265646974734E6F7452656465656D61626C654578706972656400436172645265706C6163656D656E7400447261775469636B65745072696E740048616E647061790048616E6470617943616E63656C6C6174696F6E004D616E75616C456E6453657373696F6E0050726F6D6F4D616E75616C456E6453657373696F6E004D616E75616C48616E64706179004D616E75616C48616E6470617943616E63656C6C6174696F6E00506F696E74734177617264656400506F696E7473546F476966745265717565737400506F696E7473546F4E6F7452656465656D61626C6500506F696E74734769667444656C697665727900506F696E74734578706972656400506F696E7473546F447261775469636B65745072696E7400506F696E747347696674536572766963657300486F6C6465724C6576656C4368616E676564005072697A65436F75706F6E00444550524543415445445F52656465656D61626C655370656E7455736564496E50726F6D6F74696F6E7300506F696E7473546F52656465656D61626C65005072697A6545787069726564004361726443726561746564004163636F756E74506572736F6E616C697A6174696F6E004D616E75616C6C794164646564506F696E74734F6E6C79466F7252656465656D004163636F756E7450494E4368616E6765004163636F756E7450494E52616E646F6D00437265646974734E6F7452656465656D61626C6532457870697265640043617368496E436F766572436F75706F6E0043617368496E4E6F7452656465656D61626C65320043616E63656C537461727453657373696F6E004361726452656379636C65640050726F6D6F74696F6E52656465656D61626C650043616E63656C50726F6D6F74696F6E52656465656D61626C650050726F6D6F74696F6E506F696E740043616E63656C50726F6D6F74696F6E506F696E74004D616E75616C486F6C6465724C6576656C4368616E6765640054617852657475726E696E674F6E5072697A65436F75706F6E00446563696D616C526F756E64696E6700536572766963654368617267650043616E63656C47696674496E7374616E636500506F696E74735374617475734368616E676564004D616E75616C6C794164646564506F696E7473466F724C6576656C00496D706F727465644163636F756E7400496D706F72746564506F696E74734F6E6C79466F7252656465656D00496D706F72746564506F696E7473466F724C6576656C00496D706F72746564506F696E7473486973746F7279004163636F756E74426C6F636B6564004163636F756E74556E626C6F636B65640043616E63656C6C6174696F6E005472616E736665724372656469744F7574005472616E73666572437265646974496E0043617368496E54617800436172645265706C6163656D656E74466F724672656500436172645265706C6163656D656E74496E506F696E74730043617368416476616E63650045787465726E616C53797374656D506F696E7473537562737472616374004D756C74695369746543757272656E744C6F63616C506F696E7473005449544F5F5469636B6574436173686965725072696E7465644361736861626C65005449544F5F5469636B657443617368696572506169644361736861626C65005449544F5F5469636B6574436173686965725072696E74656450726F6D6F52656465656D61626C65005449544F5F5469636B65744D616368696E65506C617965644361736861626C65005449544F5F5469636B657452656973737565005449544F5F4163636F756E74546F5465726D696E616C437265646974005449544F5F5465726D696E616C546F4163636F756E74437265646974005449544F5F5469636B65744F66666C696E65005449544F5F5469636B6574436173686965725072696E74656450726F6D6F4E6F7452656465656D61626C65005449544F5F5469636B65744D616368696E655072696E74656448616E64706179005449544F5F5469636B65744D616368696E655072696E7465644A61636B706F74005449544F5F5469636B6574436173686965725061696448616E64706179005449544F5F5469636B657443617368696572506169644A61636B706F74005449544F5F5469636B65744D616368696E655072696E7465644361736861626C65005449544F5F5469636B65744D616368696E655072696E74656450726F6D6F4E6F7452656465656D61626C65005449544F5F5469636B6574436173686965725061696450726F6D6F52656465656D61626C65005449544F5F5469636B65744D616368696E65506C6179656450726F6D6F52656465656D61626C65005449544F5F7469636B65744D616368696E65506C6179656450726F6D6F4E6F7452656465656D61626C65005449544F5F5469636B65744361736869657245787069726564506169644361736861626C65005449544F5F5469636B657443617368696572457870697265645061696450726F6D6F52656465656D61626C65005449544F5F5469636B65744361736869657245787069726564506169644A61636B706F74005449544F5F5469636B657443617368696572457870697265645061696448616E6470617900436173686465736B4472617750617274696369706174696F6E00436173686465736B44726177506C617953657373696F6E00436869707353616C654465766F6C7574696F6E466F725469746F00436869707353616C65546F74616C0043686970735075726368617365546F74616C0057696E4C6F737353746174656D656E745265717565737400436172644465706F736974496E436865636B00436172644465706F736974496E43757272656E637945786368616E676500436172644465706F736974496E4361726443726564697400436172644465706F736974496E43617264446562697400436172644465706F736974496E4361726447656E6572696300436172645265706C6163656D656E74436865636B00436172645265706C6163656D656E744361726443726564697400436172645265706C6163656D656E7443617264446562697400436172645265706C6163656D656E744361726447656E657269630047616D65476174657761794265740047616D65476174657761795072697A65005449544F5F5469636B657450726F6D6F426F785072696E74656450726F6D6F4E6F7452656465656D61626C65005449544F5F5469636B657450726F6D6F426F785072696E74656450726F6D6F52656465656D61626C650047616D6547617465776179526573657276654372656469740047616D6547617465776179426574526F6C6C6261636B0054617850726F766973696F6E7300546178437573746F64790054617852657475726E437573746F64790043757272656E637945786368616E6765436173686965724F7574005449544F5F5469636B6574436F756E74525072696E7465644361736861626C6500436172644173736F63696174650043757272656E637945786368616E676543617368696572496E0050726F6D6F74696F6E52656465656D61626C654665646572616C5461780050726F6D6F74696F6E52656465656D61626C655374617465546178004332474F43617368496E496E7465726E616C004332474F43617368496E45787465726E616C004332474F436173684F7574496E7465726E616C004332474F436173684F757445787465726E616C004332474F41637469766174654163636F756E74496E7465726E616C004332474F446561637469766174654163636F756E74496E7465726E616C004332474F4C6F636B4163636F756E74496E7465726E616C004332474F4C6F636B4163636F756E7445787465726E616C004332474F52656E6F6D696E6174696F6E4163636F756E74496E7465726E616C004332474F52656E6F6D696E6174696F6E4163636F756E7445787465726E616C004163636F756E74496E426C61636B6C697374004163636F756E744E6F74496E426C61636B6C6973740048494444454E5F52656368617267654E6F74446F6E655769746843617368004D554C5449504C455F4255434B4554535F456E6453657373696F6E004D554C5449504C455F4255434B4554535F456E6453657373696F6E4C617374004D554C5449504C455F4255434B4554535F45787069726564004D554C5449504C455F4255434B4554535F457870697265644C617374004D554C5449504C455F4255434B4554535F4D616E75616C5F416464004D554C5449504C455F4255434B4554535F4D616E75616C5F4164645F4C617374004D554C5449504C455F4255434B4554535F4D616E75616C5F537562004D554C5449504C455F4255434B4554535F4D616E75616C5F5375625F4C617374004D554C5449504C455F4255434B4554535F4D616E75616C5F536574004D554C5449504C455F4255434B4554535F4D616E75616C5F5365745F4C61737400434144494C4C41435F4A41434B0057494E00414C455349530048414E445041595F43414E43454C4C45445F435245444954530048414E445041595F4A41434B504F540048414E445041595F4F525048414E5F435245444954530048414E445041595F4D414E55414C0048414E445041595F534954455F4A41434B504F5400445241570050524F47524553534956455F50524F564953494F4E0050524F47524553534956455F524553455256450047414D4547415445574159004143434F554E545F554E4B4E4F574E004143434F554E545F434144494C4C41435F4A41434B004143434F554E545F57494E004143434F554E545F414C45534953004143434F554E545F5649525455414C5F43415348494552004143434F554E545F5649525455414C5F5445524D494E414C004F70656E656400436C6F736564004162616E646F6E6564004D616E75616C436C6F7365640043616E63656C6C65640048616E647061795061796D656E740048616E6470617943616E63656C5061796D656E74004472617757696E6E657200447261774C6F7365720050726F6772657373697665526573657276650050726F677265737369766550726F766973696F6E004177617900556E6B6E6F776E00536C6F77004D656469756D004661737400536F66740041766572616765004861726400496E697469616C00454C5030315F53706C69745F50535F52656464656D5F4E6F52656465656D004361676500436173686965720046524F4D5F43415348494552004D41585F42414C414E4345004142414E444F4E45445F434152440046524F4D5F475549004D41585F50494E5F4641494C5552455300494D504F52540045585445524E414C5F53595354454D00484F4C4445525F4C4556454C5F444F574E475241444500484F4C4445525F4C4556454C5F555047524144450045585445524E414C5F53595354454D5F43414E43454C45440045585445524E414C5F414D4C00494E54525553494F4E00494E4143544956455F504C41595F53455353494F4E004332474F5F494E5445524E414C004332474F5F45585445524E414C00424C41434B4C495354004755490043415348494552004332474F00524543455054494F4E5F424C41434B4C495354005341535F484F5354005349544500534954455F4A41434B504F54004D4F42494C455F42414E4B004D4F42494C455F42414E4B5F494D42004953544154530050524F4D4F424F5800434153484445534B5F445241570047414D494E475F5441424C455F53454154004F46464C494E450043414E43454C4C45445F43524544495453004A41434B504F54004F525048414E5F43524544495453004D414E55414C005350454349414C5F50524F4752455353495645004D414E55414C5F43414E43454C4C45445F43524544495453004D414E55414C5F4A41434B504F54004D414E55414C5F4F5448455253004F55545F4F465F5345525649434500524554495245440052455345545F4F4E5F52454445454D005345545F4F4E5F46495253545F4E525F50524F4D4F54494F4E005550444154455F4F4E5F5245434841524745005550444154455F4F4E5F52454445454D0052455345545F4F4E5F5245434841524745004D554C54495349544500535550455243454E54455200434F4E4E454354454400444953434F4E4E454354454400446F776E6C6F6164436F6E66696775726174696F6E0055706C6F6164506F696E74734D6F76656D656E74730055706C6F6164506572736F6E616C496E666F0055706C6F61644163636F756E74446F63756D656E747300446F776E6C6F6164506F696E74735F5369746500446F776E6C6F6164506F696E74735F416C6C00446F776E6C6F6164506572736F6E616C496E666F5F4361726400446F776E6C6F6164506572736F6E616C496E666F5F5369746500446F776E6C6F6164506572736F6E616C496E666F5F416C6C00446F776E6C6F616450726F76696465727347616D65730055706C6F616450726F76696465727347616D657300446F776E6C6F616450726F76696465727300446F776E6C6F61644163636F756E74446F63756D656E74730055706C6F616453616C6573506572486F75720055706C6F6164506C617953657373696F6E730055706C6F616450726F7669646572730055706C6F616442616E6B730055706C6F616441726561730055706C6F61645465726D696E616C730055706C6F61645465726D696E616C73436F6E6E65637465640055706C6F616447616D65506C617953657373696F6E7300446F776E6C6F61644D617374657250726F66696C657300446F776E6C6F6164436F72706F7261746555736572730055706C6F61644C61737441637469766974790055706C6F6164436173686965724D6F76656D656E74734772757065644279486F75720055706C6F6164416C61726D7300446F776E6C6F6164416C61726D5061747465726E7300446F776E6C6F61644C63644D657373616765730055706C6F616448616E64706179730055706C6F616453656C6C73416E644275797300526563657074696F6E456E74727900446F776E6C6F61644275636B657473436F6E6669670055706C6F61644C696E6B656445787465726E616C4163636F756E747300446F776E6C6F616445787465726E616C4163636F756E74735F5369746500446F776E6C6F61644C696E6B656445787465726E616C4163636F756E747300446F776E6C6F61644C696E6B656445787465726E616C4163636F756E74735F5369746500446F776E6C6F61644C696E6B656445787465726E616C4163636F756E74735F416C6C004E6F7453657400566F75636865727353706C69744100566F75636865727353706C69744200566F75636865727353706C69744243616E63656C004163636F756E74496400547261636B4163636F756E744368616E67657300547261636B506F696E744368616E676573004163636F756E744D6F76656D656E74730050726F76696465727347616D65730050726F766964657273004163636F756E74446F63756D656E747300436173684465736B44726177004C43444D65737361676500566F7563686572456E7472616E63657300566F756368657254617850726F766973696F6E7300566F7563686572546178437573746F6479004F70656E436C6F73654C6F6F70536974655265717565737473004C696E6B656445787465726E616C4163636F756E7473005449544F5F56616C69646174696F6E4E756D626572005465726D696E616C4D6574657273486973746F72794576656E74730050696E5061645472616E73616374696F6E00417564697449640043616765436F6E63657074730043616765436F6E63657074735F4C617374004E6F74446F776E6C6F616400446F776E6C6F61645F55706461746500446F776E6C6F61645F5570646174655072696F726974794D756C74697369746500446F776E6C6F61645F5570646174655072696F7269747953697465004F50454E00434C4F534544004F50454E5F50454E44494E470050454E44494E475F434C4F53494E4700434845434B0042414E4B5F43415244004352454449545F434152440044454249545F434152440043555252454E4359004341524400434153494E4F4348495000434153494E4F5F434849505F524500434153494E4F5F434849505F4E524500434153494E4F5F434849505F434F4C4F520046524545005449434B4554004341524432474F00434153485F414456414E43450052454348415247450052454348415247455F434152445F4352454154494F4E004E4F545F5345540050524F4D4F54494F4E004D425F50524F4D4F54494F4E00474946545F5245515545535400474946545F44454C495645525900445241575F5449434B455400474946545F445241575F5449434B455400474946545F52454445454D41424C455F41535F43415348494E0043484950535F53414C455F574954485F5245434841524745004D425F4445504F53495400434153485F4445504F53495400434153485F5749544844524157414C0052454445454D41424C455F435245444954535F45585049524544004E4F545F52454445454D41424C455F435245444954535F4558504952454400504F494E54535F4558504952454400484F4C4445525F4C4556454C5F4348414E474544004143434F554E545F4352454154494F4E004143434F554E545F504552534F4E414C495A4154494F4E004143434F554E545F50494E5F4348414E474544004E4F545F52454445454D41424C45325F435245444954535F45585049524544004E415F434153485F494E004E415F50524F4D4F54494F4E0043414E43454C5F474946545F494E5354414E43450050524F4D4F424F585F544F54414C5F5245434841524745535F5052494E540048414E445041595F56414C49444154494F4E00494D504F52545F504F494E5453005449544F5F4F46464C494E45005449544F5F52454953535545005449544F5F5449434B45545F56414C49444154494F4E00494D504F52545F47414D494E475F5441424C455F504C41595F53455353494F4E5300534146455F4B454550494E475F4445504F53495400534146455F4B454550494E475F5749544844524157005452414E534645525F434153484945525F53455353494F4E535F5749544844524157414C005452414E534645525F434153484945525F53455353494F4E535F4445504F534954005445524D494E414C5F434F4C4C4543545F44524F50424F580047414D45474154455741595F524553455256455F435245444954005445524D494E414C5F4348414E47455F535441434B45525F52455155455354004D554C5449504C455F4255434B4554535F454E4453455353494F4E004D554C5449504C455F4255434B4554535F4D414E55414C00434153484945525F524546494C4C5F484F5050455200434153484945525F524546494C4C5F484F505045525F4341474500434153484945525F434F4C4C4543545F524546494C4C00434153484945525F434F4C4C4543545F524546494C4C5F43414745005449544F5F564F49445F4D414348494E455F5449434B45540043484950535F53574150005052495A455F5041594F5554005052495A455F5041594F55545F414E445F52454348415247450043555252454E43595F45584348414E47455F4348414E47450043555252454E43595F45584348414E47455F4445564F4C5554494F4E0050524F4D4F54494F4E5F574954485F544158455300434147455F434F4E434550545300434147455F434F4E43455054535F4C4153540043617368496E5F410043617368496E5F4200436173684F75745F410043616E63656C5F4200536572766963654368617267655F4200436173684465736B447261775F57696E6E6572005469636B65744F757400436869707353616C650043686970734275790043686970734368616E676500436869707353616C654465616C6572436F707900436173684F70656E696E670043617368436C6F73696E67004D4253616C65734C696D69744368616E6765004D424465706F736974004D42436C6F73696E6700436F6D6D697373696F6E5F4200436865636B436F6D6D697373696F6E4200436173684465736B447261775F4C6F7365720045786368616E6765436F6D6D697373696F6E4200436173684465706F73697400436173685769746864726177616C00436172644465706F736974496E5F4200436172645265706C6163656D656E745F4200436172644465706F7369744F75745F4200436173684465736B436C6F7365556E62616C616E63656400436173686965725472616E73666572436173684465706F73697400436173686965725472616E73666572436173685769746864726177616C005465726D696E616C73546F436F6C6C656374005465726D696E616C73546F526566696C6C00437573746F6D6572456E7472616E63650043686970735377617000436173685769746864726177616C4361726400546F74616C697A696E675374726970004E6F6E6500556E646F6E6500556E646F4F7065726174696F6E004143434F554E540057495448484F4C44494E4700496E7465726D6564696174655365727665720047616D696E675465726D696E616C006D5F696E5F616D6F756E74006D5F696E5F69736F5F636F6465006D5F6F75745F69736F5F636F6465006D5F67726F73735F616D6F756E74006D5F636F6D697373696F6E006D5F6E65745F616D6F756E745F73706C697431006D5F6E65745F616D6F756E745F73706C697432006D5F4E52325F616D6F756E74006D5F636172645F7072696365006D5F636172645F636F6D697373696F6E73006D5F6368616E67655F72617465006D5F6E756D5F646563696D616C73006D5F7761735F666F7265696E675F63757272656E6379006D5F6172655F70726F6D6F74696F6E735F656E61626C6564006D5F696E5F74797065006D5F6163636F756E745F70726F6D6F74696F6E5F6964006D5F62616E6B5F7472616E73616374696F6E5F64617461006D5F6E65745F616D6F756E745F6465766F6C7574696F6E006D5F73756274797065006765745F496E416D6F756E74007365745F496E416D6F756E74006765745F4163636F756E7450726F6D6F74696F6E4964007365745F4163636F756E7450726F6D6F74696F6E4964006765745F496E43757272656E6379436F6465007365745F496E43757272656E6379436F6465006765745F4F757443757272656E6379436F6465007365745F4F757443757272656E6379436F6465006765745F4368616E676552617465007365745F4368616E676552617465006765745F446563696D616C73007365745F446563696D616C73006765745F576173466F7265696E6743757272656E6379007365745F576173466F7265696E6743757272656E6379006765745F47726F7373416D6F756E74007365745F47726F7373416D6F756E74006765745F436F6D697373696F6E007365745F436F6D697373696F6E006765745F4E6574416D6F756E74006765745F4E6574416D6F756E7453706C697431007365745F4E6574416D6F756E7453706C697431006765745F4E6574416D6F756E7453706C697432007365745F4E6574416D6F756E7453706C697432006765745F4E5232416D6F756E74007365745F4E5232416D6F756E74006765745F436172645072696365007365745F436172645072696365006765745F43617264436F6D697373696F6E73007365745F43617264436F6D697373696F6E73006765745F41726550726F6D6F74696F6E73456E61626C6564007365745F41726550726F6D6F74696F6E73456E61626C6564006765745F496E54797065007365745F496E54797065006765745F53756254797065007365745F53756254797065006765745F42616E6B5472616E73616374696F6E44617461007365745F42616E6B5472616E73616374696F6E44617461006765745F4E6574416D6F756E744465766F6C7574696F6E007365745F4E6574416D6F756E744465766F6C7574696F6E00496E416D6F756E74004163636F756E7450726F6D6F74696F6E496400496E43757272656E6379436F6465004F757443757272656E6379436F6465004368616E67655261746500446563696D616C7300576173466F7265696E6743757272656E63790047726F7373416D6F756E7400436F6D697373696F6E004E6574416D6F756E74004E6574416D6F756E7453706C697431004E6574416D6F756E7453706C697432004E5232416D6F756E74004361726450726963650043617264436F6D697373696F6E730041726550726F6D6F74696F6E73456E61626C656400496E547970650053756254797065004E6574416D6F756E744465766F6C7574696F6E006D5F6F7065726174696F6E5F6964006D5F74797065006D5F646F63756D656E745F6E756D626572006D5F62616E6B5F6E616D65006D5F686F6C6465725F6E616D65006D5F62616E6B5F636F756E747279006D5F636172645F747261636B5F64617461006D5F63757272656E63795F636F6465006D5F636172645F65787069726174696F6E5F64617465006D5F636172645F646174615F656469746564006D5F636865636B5F726F7574696E675F6E756D626572006D5F636865636B5F6163636F756E745F6E756D626572006D5F636F6D6D656E74006D5F636865636B5F6461746574696D65006D5F7472616E73616374696F6E5F74797065006D5F6163636F756E745F686F6C6465725F6E616D65006765745F4F7065726174696F6E4964007365745F4F7065726174696F6E4964006765745F54797065007365745F54797065006765745F446F63756D656E744E756D626572007365745F446F63756D656E744E756D626572006765745F42616E6B4E616D65007365745F42616E6B4E616D65006765745F486F6C6465724E616D65007365745F486F6C6465724E616D65006765745F42616E6B436F756E747279007365745F42616E6B436F756E747279006765745F547261636B44617461007365745F547261636B44617461006765745F696E416D6F756E74007365745F696E416D6F756E74006765745F43757272656E6379436F6465007365745F43757272656E6379436F6465006765745F45787069726174696F6E44617465007365745F45787069726174696F6E44617465006765745F526F7574696E674E756D626572007365745F526F7574696E674E756D626572006765745F4163636F756E744E756D626572007365745F4163636F756E744E756D626572006765745F43617264456469746564007365745F43617264456469746564006765745F436F6D6D656E7473007365745F436F6D6D656E7473006765745F436865636B44617465007365745F436865636B44617465006765745F5472616E73616374696F6E54797065007365745F5472616E73616374696F6E54797065006765745F4163636F756E74486F6C6465724E616D65007365745F4163636F756E74486F6C6465724E616D65004F7065726174696F6E4964005479706500446F63756D656E744E756D6265720042616E6B4E616D6500486F6C6465724E616D650042616E6B436F756E74727900696E416D6F756E740043757272656E6379436F64650045787069726174696F6E4461746500526F7574696E674E756D626572004163636F756E744E756D626572004361726445646974656400436F6D6D656E747300436865636B44617465004163636F756E74486F6C6465724E616D6500414C4C0047524F55500050524F56005A4F4E4500415245410042414E4B005445524D0051554552590050524F4D4F54494F4E5F52455354524943544544005041545445524E530050524F4752455353495645004C43445F4D4553534147450050524F4D4F54494F4E5F504C415945440047414D455F47415445574159004255434B4554535F4D554C5449504C4945520044656661756C7400596573004E6F00426C6F636B4F6E456E74657200426C6F636B4F6E4C6561766500426F74680043616C63756C61746564416E645265776172640050656E64696E67004D616E75616C004D616E75616C43616C63756C61746564004E65757472616C004368696E65736500437A65636800456E676C697368005370616E697368004974616C69616E004B6F7265616E005275737369616E00434153484C455353005449544F00574153530047414D494E4748414C4C004D49434F3200434F4E5345435554495645004E4F5F434F4E53454355544956455F574954485F4F52444552004E4F5F4F52444552004E4F5F4F524445525F574954485F52455045544954494F4E00416E6F6E796D6F7573576974684361726400437573746F6D697A656400416E6F6E796D6F7573576974686F7574436172640042696C6C00436F696E004F7468657273004368697073526564696D69626C650043686970734E6F526564696D69626C65004368697073436F6C6F7200466F726569676E004E6174696F6E616C004E6F7468696E67004E657743757272656E6379004E6577536974654E6174696F6E616C43757272656E6379004E657753697465466F726569676E43757272656E6379006D5F636F6E636570745F6964006D5F7175616E74697479006D5F616D6F756E74006D5F69736F5F636F6465006D5F63757272656E63795F74797065006765745F5175616E74697479007365745F5175616E74697479006765745F416D6F756E74007365745F416D6F756E74006765745F49736F436F6465007365745F49736F436F6465006765745F43757272656E637954797065007365745F43757272656E637954797065006765745F436F6E636570744964007365745F436F6E636570744964006765745F436F6D6D656E74007365745F436F6D6D656E74005175616E7469747900416D6F756E740049736F436F646500436F6E63657074496400436F6D6D656E740057494E5053410053656E7400526563656976656400426F7800486F7070657200496E004F75740050756E746F7343616E6A650050756E746F734E6976656C004E5200524500436F6D703143616E6A6500436F6D70324E5200436F6D703352450048415050595F42495254484441595F544F4441590048415050595F42495254484441595F494E434F4D4D494E470044554D4D590042414E4F525445004100420043004400526564656D7074696F6E506F696E74730052616E6B696E674C6576656C506F696E7473004372656469745F4E52004372656469745F524500436F6D70315F526564656D7074696F6E506F696E747300436F6D70325F4372656469745F4E5200436F6D70335F4372656469745F52450052616E6B696E674C6576656C506F696E74735F47656E6572617465640052616E6B696E674C6576656C506F696E74735F44697363726574696F6E616C0048414E445041595F5649525455414C5F504C41595F53455353494F4E5F4944005472785F4163636F756E7450726F6D6F74696F6E73416374696F6E005472785F57637053746172744361726453657373696F6E005472785F436F6D6D6F6E53746172744361726453657373696F6E005472785F4F6E53746172744361726453657373696F6E005472785F47657445786368616E6765005472785F4C696E6B4163636F756E745465726D696E616C005472785F556E6C696E6B4163636F756E745465726D696E616C005472785F476574506C617953657373696F6E4964005472785F496E73657274506C617953657373696F6E005472785F55706461746550734163636F756E744964005472785F48616E64706179496E73657274506C617953657373696F6E005472785F557064617465506C617953657373696F6E506C61796564576F6E005472785F4F6E456E644361726453657373696F6E005472785F557064617465466F756E64416E6452656D61696E696E67496E45676D0049734163636F756E74416E6F6E796D6F75734F725669727475616C005472785F506C617953657373696F6E436C6F7365005472785F41646442616C616E636573546F4163636F756E740054696D655370616E005472785F506C617953657373696F6E546F50656E64696E67005472785F536974654A61636B706F74436F6E747269627574696F6E005472785F506C617953657373696F6E5365744D6574657273005472785F506C617953657373696F6E5449544F5365744D6574657273005472785F526573657443616E63656C6C61626C654F7065726174696F6E005472785F52657365744163636F756E74496E53657373696F6E005472785F556E6C696E6B50726F6D6F74696F6E73496E53657373696F6E005472785F50726F6D6F5265636F6D7075746557697468686F6C644F6E5265636861726765005472785F50726F6D6F4D61726B4173457868617573746564005472785F5449544F5F55706461746550726F6D6F4E52436F7374005472785F5570646174654163636F756E7450726F6D6F74696F6E436F7374005472785F476574506C617961626C6542616C616E6365005472785F47657447616D65476174657761795265736572766564456E61626C6564005472785F557064617465506C61796564416E64576F6E005472785F47657443726564697473506C61794D6F64650053797374656D2E5465787400537472696E674275696C646572005472785F47657444656C7461506C61796564576F6E005449544F5F47657444656C7461506C61796564576F6E005472785F5570646174654163636F756E74506F696E7473005472785F5570646174654163636F756E7442616C616E6365005472785F5365744163746976697479005472785F5365744163636F756E74426C6F636B6564005472785F43616E63656C53746172744361726453657373696F6E005472785F5570646174654163636F756E74496E53657373696F6E005472785F42616C616E6365546F506C617953657373696F6E00444550524543415445445F5472785F436F6D70757465576F6E506F696E7473005472785F47657445787465726E616C4C6F79616C747950726F6772616D4D6F646500436865636B4269727468646179416C61726D005265676973746572005472785F5449544F5F5570646174654163636F756E7450726F6D6F74696F6E42616C616E63650044425F5570646174655465726D696E616C4163636F756E7450726F6D6F74696F6E005472785F5449544F5F5472616E7366657246726F6D4163636F756E745F43616E63656C53746172744361726453657373696F6E005472785F47616D65476174657761795F526573657276655F43726564697400576F6E50726F6D6F4E6F7452656465656D61626C6500576F6E52656465656D61626C6500506C617950726F6D6F4E6F7452656465656D61626C6542616C616E6365005370656E7452656D61696E696E67496E53657373696F6E50726F6D6F4E52005370656E7452656D61696E696E67496E53657373696F6E50726F6D6F524500506C617950726F6D6F5072697A654F6E6C7900506C617952656465656D61626C65005472785F506C617953657373696F6E4368616E6765537461747573005472785F506C617953657373696F6E53657446696E616C42616C616E6365005472785F506C617953657373696F6E4D61726B41734162616E646F6E6564005472785F52656C65617365496E616374697665506C617953657373696F6E005472785F476574506C617953657373696F6E5449544F53657373696F6E4D6574657273005472785F4163636F756E7450726F6D6F74696F6E436F7374005472785F496E736572744167726F757047616D65506C617953657373696F6E00496E73657274576370436F6D6D616E64005472785F506C617953657373696F6E546F526576697365005265616447656E6572616C506172616D73004F6B004572726F72004163636F756E74556E6B6E6F776E004163636F756E74496E53657373696F6E005465726D696E616C556E6B6E6F776E005465726D696E616C426C6F636B6564005061727469616C00416C6C00496E636C7564655265736572766564004D69636F32005472616E73616374696F6E49640049735469746F4D6F6465005472616E736665724D6F6465004163636F756E7442616C616E6365546F5472616E73666572005472616E73666572576974684275636B65747300506C617953657373696F6E49640049734E6577506C617953657373696F6E00496E6942616C616E636500506C617961626C6542616C616E63650046696E42616C616E636500546F74616C546F474D42616C616E636500506F696E747300537461747573436F6465004572726F724D7367005365744572726F72005761726E696E670053657453756363657373006765745F4D657373616765004D65737361676500436173686965724E616D650046696E616C42616C616E636553656C656374656442795573657241667465724D69736D617463680042616C616E636546726F6D474D004861734D6574657273004D65746572730049735449544F005669727475616C4163636F756E744964005469746F53657373696F6E4D657465727300436C6F736553657373696F6E436F6D6D656E740049734D69636F320042616C616E6365546F4163636F756E740044656C746142616C616E6365466F756E64496E53657373696F6E0052656D61696E696E67496E45676D00466F756E64496E45676D00506C61796564436F756E7400506C61796564416D6F756E7400576F6E436F756E7400576F6E416D6F756E74004A61636B706F74416D6F756E740042696C6C496E005469636B6574496E4361736861626C65005469636B6574496E50726F6D6F4E72005469636B6574496E50726F6D6F5265005469636B65744F75744361736861626C65005469636B65744F757450726F6D6F4E72006765745F52656465656D61626C6543617368496E006765745F50726F6D6F526543617368496E006765745F50726F6D6F4E7243617368496E006765745F546F74616C43617368496E006765745F52656465656D61626C65436173684F7574006765745F50726F6D6F4E72436173684F7574006765745F546F74616C436173684F75740052656465656D61626C6543617368496E0050726F6D6F526543617368496E0050726F6D6F4E7243617368496E00546F74616C43617368496E0052656465656D61626C65436173684F75740050726F6D6F4E72436173684F757400546F74616C436173684F75740043617368496E436F696E730043617368496E42696C6C730043617368496E546F74616C004572726F72526574727900466174616C4572726F72004E6F744F70656E65640042616C616E63654D69736D6174636800496E76616C6964506C617953657373696F6E00506C617953657373696F6E46696E616C42616C616E6365004163636F756E7450726F6D6F74696F6E0044656C7461506C617965640044656C7461576F6E0044656C7461506C6179656452650044656C7461506C617965644E720044656C7461576F6E52650044656C7461576F6E4E720042616C616E636500506C6179656400576F6E0042616C616E636546726F6D474D546F416464004E756D506C61796564004E756D576F6E0052656465656D61626C650050726F6D6F52656465656D61626C650050726F6D6F4E6F7452656465656D61626C65005265736572766564005265736572766564446973636F756E746564004D6F64655265736572766564004D6F646552657365727665644368616E676564004275636B65744E52437265646974004275636B6574524543726564697400546F537472696E67006765745F546F74616C52656465656D61626C65006765745F546F74616C4E6F7452656465656D61626C65006765745F546F74616C42616C616E6365006765745F5A65726F006765745F42616C616E636545474D006F705F4164646974696F6E006F705F5375627472616374696F6E006F705F457175616C697479006F705F496E657175616C69747900457175616C730047657448617368436F6465004E656761746500436C6F6E6500546F74616C52656465656D61626C6500546F74616C4E6F7452656465656D61626C6500546F74616C42616C616E6365005A65726F0042616C616E636545474D00426C6F636B656400426C6F636B526561736F6E00486F6C6465724C6576656C00486F6C64657247656E646572004163547970650043616E63656C6C61626C654F7065726174696F6E49640043757272656E74506C617953657373696F6E49640043757272656E745465726D696E616C49640043757272656E74506C617953657373696F6E5374617475730043757272656E74506C617953657373696F6E42616C616E63654D69736D617463680043757272656E74506C617953657373696F6E496E697469616C42616C616E63650043616E63656C6C61626C6554727849640043616E63656C6C61626C6542616C616E63650046696E616C42616C616E63650046696E616C506F696E7473004572726F724D6573736167650043616E63656C4279506C61796572004E6F7452656465656D61626C65546F52656465656D61626C6500444F5F4E4F5448494E470053454E445F4249525448444154455F414C41524D0053454E445F4249525448444154455F49535F4E454152005245504F52545F52455345525645445F4352454449540043414E43454C5F52455345525645445F435245444954004D4F444946595F52455345525645445F435245444954005245504F52545F4745545F43524544495400524551554553545F545950455F524553504F4E53455F4F4B00524551554553545F545950455F524553504F4E53455F4552524F5200524551554553545F545950455F524553504F4E53455F4E4F545F454E4F5547485F4352454449540047414D455F474154455741595F52455345525645445F4D4F44455F574954484F55545F4255434B4554530047414D455F474154455741595F52455345525645445F4D4F44455F574954485F4255434B4554530047414D455F474154455741595F52455345525645445F4D4F44455F4D495845440047414D455F474154455741595F50524F4D4F5F42414C414E43455F414354494F4E5F4E4F4E450047414D455F474154455741595F50524F4D4F5F42414C414E43455F414354494F4E5F434F4C4C4543545F5052495A450047414D455F474154455741595F50524F4D4F5F42414C414E43455F414354494F4E5F574954484F55545F50494E00537461727453657373696F6E005265537461727453657373696F6E00456E6453657373696F6E00537461727453657373696F6E43616E63656C6C6564006D5F70726F76696465720044617461536574006D5F6473006765745F4C69737454797065007365745F4C69737454797065004175646974537472696E670041646450726F76696465720052656D6F766550726F766964657200436F6E7461696E7300546F586D6C0046726F6D586D6C00496E6974005365656B50726F766964657200586D6C57726974654D6F64650044617461536574546F586D6C00586D6C526561644D6F6465004461746153657446726F6D586D6C004C69737454797065004C6973746564004E6F744C6973746564005472785F49735433535465726D696E616C005472785F4765745465726D696E616C496E666F005472785F496E7365727450656E64696E675465726D696E616C005472785F4765745465726D696E616C496E666F427945787465726E616C4964004765745465726D696E616C446973706C61794E616D6500534153466C6167416374697665640053716C436F6D6D616E640047657447616D654D6574657273557064617465436F6D6D616E6400436C6F7365416C6C4275744E657765724F70656E4361736869657253657373696F6E73005472785F4765745465726D696E616C54797065004E6F7452656465656D61626C654C6173740050726F6D6F74696F6E616C734669727374005361734D616368696E650050726F764964005465726D696E616C54797065004E616D650050726F76696465724E616D650053746174757300506C61794D6F64650043757272656E744163636F756E74496400534153466C616773005076506F696E74734D756C7469706C696572005076536974654A61636B706F740050764F6E6C7952656465656D61626C6500534153466C61674163746976617465640053797374656D4964004D616368696E65496400416C6C6F774361736861626C655469636B65744F757400416C6C6F7750726F6D6F74696F6E616C5469636B65744F757400416C6C6F775469636B6574496E0045787069726174696F6E5469636B6574734361736861626C650045787069726174696F6E5469636B65747350726F6D6F74696F6E616C005469746F5469636B65744C6F63616C697A6174696F6E005469746F5469636B6574416464726573735F31005469746F5469636B6574416464726573735F32005469746F5469636B65745469746C6552657374726963746564005469746F5469636B65745469746C654465626974005469636B6574734D61784372656174696F6E416D6F756E740045787465726E616C496400466C6F6F72496400486F7374496400546F45787465726E616C00546F496E7465726E616C00434845434B53554D5F4D4F44554C4500545241434B444154415F43525950544F5F4B455900545241434B444154415F43525950544F5F495600547261636B44617461546F496E7465726E616C00547261636B44617461546F45787465726E616C004D616B65496E7465726E616C547261636B446174610053706C6974496E7465726E616C547261636B446174610045787465726E616C547261636B4461746142656C6F6E67546F5369746500436F6E76657274547261636B4461746100574350007472616E73666F726D5F6B6579006B65795F6C656E67746800646174615F7065726D75746174696F6E00696E6465783100696E6465783200646973706F7365006765745F43616E52657573655472616E73666F726D006765745F43616E5472616E73666F726D4D756C7469706C65426C6F636B73006765745F496E707574426C6F636B53697A65006765745F4F7574707574426C6F636B53697A65005472616E73666F726D426C6F636B005472616E73666F726D46696E616C426C6F636B00446973706F73650043616E52657573655472616E73666F726D0043616E5472616E73666F726D4D756C7469706C65426C6F636B7300496E707574426C6F636B53697A65004F7574707574426C6F636B53697A65006765745F426C6F636B53697A65007365745F426C6F636B53697A65006765745F466565646261636B53697A65007365745F466565646261636B53697A65006765745F4956007365745F4956004B657953697A6573006765745F4C6567616C426C6F636B53697A6573006765745F4C6567616C4B657953697A6573004369706865724D6F6465006765745F4D6F6465007365745F4D6F64650050616464696E674D6F6465006765745F50616464696E67007365745F50616464696E670047656E657261746549560047656E65726174654B65790043726561746500426C6F636B53697A6500466565646261636B53697A65004956004C6567616C426C6F636B53697A6573004C6567616C4B657953697A6573004D6F64650050616464696E670069735F646973706F73656400437265617465446563727970746F7200437265617465456E63727970746F7200617263666F75726D616E61676564006765745F4B6579007365745F4B6579006765745F4B657953697A65007365745F4B657953697A65004B6579004B657953697A65005472785F3347535F53746172744361726453657373696F6E005472785F3347535F5570646174654361726453657373696F6E005472785F3347535F456E644361726453657373696F6E005472785F3347535F4163636F756E74537461747573005472785F3347535F53656E644576656E740056656E646F7249640053657269616C4E756D626572004D616368696E654E756D6265720054727800506C617953657373696F6E4D6574657273004D616368696E654E756D62657244756D6D79004576656E744964004163636F756E740053797374656D2E52756E74696D652E496E7465726F705365727669636573004F75744174747269627574650053746174757354657874004572726F72546578740053716C54727800496E697469616C42616C616E636500537562416D6F756E7400416464416D6F756E740044657461696C7300526561736F6E7300496E6465780076616C75650043617264547261636B44617461004D6F7644657461696C730050726F6D6F4E616D65004973537761700043757272656E637944656E6F6D696E6174696F6E004D6F76656D656E7449640047616D696E675461626C6553657373696F6E496400417578416D6F756E74004375727245786368616E676554797065004368697049640045786368616E6765526573756C74004E6174696F6E616C43757272656E6379004D6F76656D656E740052656C61746564496400496E697469616C416D6F756E740046696E616C416D6F756E74004973436F756E745200416374696F6E00416666656374656442616C616E6365004E756D50726F6D6F7300496E707574005449544F4D6F64650042616C616E6365546F5472616E73666572004F75747075740046726F6D49534F00546F49534F0045786368616E67656400456E737572654E6F74496E53657373696F6E00547970654461746100506C617953657373696F6E496E697469616C42616C616E63650048616E64706179416D6F756E740044656C746100466F756E640052656D61696E696E67004973416E6F6E796D6F75734F725669727475616C00506172616D730042616C616E6365546F41646400506C61795F53657373696F6E5F6475726174696F6E0050726F6D6F4E725469636B6574496E0050726F6D6F52655469636B6574496E0052655469636B6574496E0052655469636B65744F75740050726F6D6F4E725469636B65744F75740043617368496E416D6F756E7400496E53657373696F6E506C6179656400496E53657373696F6E576F6E00496E53657373696F6E42616C616E6365005469636B65744F757452650043617368496E52650043757272656E7400506C617961626C65005449544F49735472616E736665720041637469766550726F6D6F7300506F696E7473546F41646400506F696E7473546F41646446696E616C42616C616E636500416C6C6F774E65676174697665506F696E747300546F4164640046696E616C00496E636C7564654275636B65740043616E63656C6C61626C6500436C6F736564506C617953657373696F6E496400546F74616C46726F6D474D42616C616E63650053657373696F6E42616C616E636500497346697273745472616E736665720049676E6F7265506C617961626C65496E43617368496E00496E53657373696F6E52655370656E7400496E53657373696F6E5265506C6179656400576F6E506F696E747300506F696E7448617665546F42654177617264656400536F75726365436F646500536F75726365496400536F757263654E616D6500416C61726D436F6465004465736372697074696F6E0053657665726974790050726F6D6F73546F436F6E73756D650050726F6D6F49640042616C616E636543616E63656C6C65640052657175657374416D6F756E74005265717565737454797065005265736572766564496E697469616C42616C616E63650053696D756C617465576F6E4C6F636B00437265646974547970650044657369726564537461747573004F6C64537461747573004E657753746174757300506C617954696D655370616E00456665637469766546696E616C42616C616E6365005265706F7274656446696E616C42616C616E63650053797374656D436173686965724E616D6500436F6D7075746564506F696E74730047726F7570005375626A6563740056616C75650042616C310042616C32006F626A004E65676174655265736572766564004578636C756465640050726F766964657200586D6C0057726974654D6F646500526561644D6F646500536F75726365004F75745465726D696E616C496E666F005465726D696E616C45787465726E616C4964004D61736B4E616D6500446973706C61794E616D650043757272656E74466C61677300466C616700496E7465726E616C547261636B44617461004361726454797065005369746549640053657175656E6365005265636569766564547261636B44617461004442547261636B4461746100496E70757442756666657200496E7075744F666673657400496E707574436F756E74004F7574707574427566666572004F75747075744F666673657400416C674E616D65005267624B657900526762495600446973706F73696E67005374617475734572726F720053797374656D2E5265666C656374696F6E00417373656D626C795469746C6541747472696275746500417373656D626C794465736372697074696F6E41747472696275746500417373656D626C79436F6E66696775726174696F6E41747472696275746500417373656D626C79436F6D70616E7941747472696275746500417373656D626C7950726F6475637441747472696275746500417373656D626C79436F7079726967687441747472696275746500417373656D626C7954726164656D61726B41747472696275746500417373656D626C7943756C7475726541747472696275746500436F6D56697369626C65417474726962757465004775696441747472696275746500417373656D626C7956657273696F6E41747472696275746500417373656D626C7946696C6556657273696F6E4174747269627574650053797374656D2E446961676E6F73746963730044656275676761626C6541747472696275746500446562756767696E674D6F6465730053797374656D2E52756E74696D652E436F6D70696C6572536572766963657300436F6D70696C6174696F6E52656C61786174696F6E734174747269627574650052756E74696D65436F6D7061746962696C6974794174747269627574650053514C427573696E6573734C6F67696300526F6C6C6261636B00496E74363400537472696E6700436F6E63617400417070656E644C696E650053716C436F6E6E656374696F6E006765745F436F6E6E656374696F6E0053716C506172616D65746572436F6C6C656374696F6E006765745F506172616D65746572730053716C506172616D657465720053716C44625479706500496E7433320053797374656D2E446174612E436F6D6D6F6E004462506172616D65746572007365745F56616C7565004462436F6D6D616E6400457865637574654E6F6E517565727900466F726D6174006F705F4C6573735468616E004D617468004D6178005472795061727365006F705F4469766973696F6E00416273006F705F477265617465725468616E004D696E00446F75626C650049734E756C6C4F72456D7074790044617461436F6C756D6E436F6C6C656374696F6E006765745F436F6C756D6E7300476574547970650044617461436F6C756D6E007365745F4175746F496E6372656D656E74007365745F4175746F496E6372656D656E7453656564007365745F4175746F496E6372656D656E7453746570007365745F416C6C6F7744424E756C6C006765745F4974656D007365745F5072696D6172794B6579004E6577526F77007365745F4974656D0044424E756C6C0044617461526F77436F6C6C656374696F6E006765745F526F777300457865637574655363616C617200457863657074696F6E00496E7465726E616C44617461436F6C6C656374696F6E42617365006765745F436F756E740053797374656D2E436F6C6C656374696F6E730049456E756D657261746F7200476574456E756D657261746F72006765745F43757272656E74004D6F76654E657874007365745F536F75726365436F6C756D6E00506172616D65746572446972656374696F6E007365745F446972656374696F6E00557064617465526F77536F75726365007365745F55706461746564526F77536F757263650053716C4461746141646170746572007365745F496E73657274436F6D6D616E640044624461746141646170746572004D656D62657277697365436C6F6E65007365745F44656661756C7456616C7565006F705F477265617465725468616E4F72457175616C006765745F48617356616C7565006765745F4974656D41727261790049734E756C6C006F705F556E6172794E65676174696F6E006765745F56616C75650053716C44617461526561646572004578656375746552656164657200446244617461526561646572005265616400476574446563696D616C007365745F4C656E677468007365745F557064617465426174636853697A650053716C457863657074696F6E00466C6167734174747269627574650053657269616C697A61626C654174747269627574650046696C6C00476574537472696E6700497344424E756C6C00476574496E74363400476574496E74333200456D707479006F705F4D756C7469706C79005472756E63617465006F705F4C6573735468616E4F72457175616C00426F6F6C65616E004765744F7264696E616C00476574426F6F6C65616E006765745F546F74616C4D696E75746573004944617461526561646572004C6F6164004D6964706F696E74526F756E64696E6700526F756E64004461746156696577526F7753746174650053656C656374007365745F557064617465436F6D6D616E64006765745F557064617465436F6D6D616E64004461746141646170746572007365745F436F6E74696E75655570646174654F6E4572726F72006765745F4861734572726F7273006765745F526F774572726F72006765745F436F6D6D616E6454657874004462506172616D65746572436F6C6C656374696F6E006765745F506172616D657465724E616D65005472696D005061644C656674005265706C61636500436F6E7665727400546F446563696D616C004E657874526573756C7400436F6D6D616E6454797065007365745F436F6D6D616E64547970650055496E743332004765744461746554696D6500537562747261637400496E743136006F705F496D706C6963697400417070656E64466F726D6174006F705F4578706C6963697400417070656E6400496E73657274006765745F4C656E67746800546F55707065720044656C657465004163636570744368616E67657300446174615461626C65436F6C6C656374696F6E006765745F5461626C657300537472696E67436F6D70617269736F6E0053797374656D2E494F00537472696E675772697465720054657874577269746572005772697465586D6C00537472696E6752656164657200546578745265616465720052656164586D6C00476574496E743136004D6963726F736F66742E53716C5365727665722E5365727665720053716C46756E6374696F6E41747472696275746500427974650055496E74363400436F6D7061726500506172736500426974436F6E766572746572004765744279746573004D656D6F727953747265616D0043727970746F53747265616D0053747265616D0043727970746F53747265616D4D6F646500546F55496E74363400537562737472696E6700577269746500466C75736846696E616C426C6F636B00546F417272617900546F496E74333200546F496E743634002E6363746F72003C50726976617465496D706C656D656E746174696F6E44657461696C733E7B35443936313341372D313946462D343535342D394543322D3431443141383232374534347D00436F6D70696C657247656E6572617465644174747269627574650056616C756554797065005F5F5374617469634172726179496E69745479706553697A653D31360024246D6574686F643078363030303138332D310052756E74696D6548656C706572730041727261790052756E74696D654669656C6448616E646C6500496E697469616C697A654172726179006765745F46756C6C4E616D65004F626A656374446973706F736564457863657074696F6E00417267756D656E744E756C6C457863657074696F6E00417267756D656E744F75744F6652616E6765457863657074696F6E00474300537570707265737346696E616C697A65004B657953697A6556616C75650043727970746F67726170686963457863657074696F6E004E6F74537570706F72746564457863657074696F6E00524E4743727970746F5365727669636550726F76696465720052616E646F6D4E756D62657247656E657261746F720053716C50726F636564757265417474726962757465004462436F6E6E656374696F6E004F70656E00426567696E5472616E73616374696F6E0044625472616E73616374696F6E00436F6D6D6974000001001554003300470053002E0053007400610072007400000720002D002000011754003300470053002E00550070006400610074006500001154003300470053002E0045006E00640000395400720078005F004F006E0045006E0064004300610072006400530065007300730069006F006E0020006600610069006C00650064002100002554003300470053002E004100630063006F0075006E007400530074006100740075007300001D54003300470053002E00530065006E0064004500760065006E007400002155006E006B006E006F0077006E0020004500760065006E007400490064002E000059200049004E005300450052005400200049004E0054004F0020004500560045004E0054005F0048004900530054004F0052005900200028002000450048005F005400450052004D0049004E0041004C005F0049004400200000572000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000450048005F00530045005300530049004F004E005F0049004400200000532000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000450048005F004400410054004500540049004D004500200000572000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000450048005F004500560045004E0054005F0054005900500045002000005F2000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000450048005F004F005000450052004100540049004F004E005F0043004F0044004500200000612000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000450048005F004F005000450052004100540049004F004E005F00440041005400410029002000005520002000200020002000200020002000200020002000200020002000200020002000200020002000560041004C00550045005300200028002000400070005400650072006D0069006E0061006C00490064002000005B2000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C0020004000700050006C0061007900530065007300730069006F006E00490064002000004F2000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000470045005400440041005400450028002900200000532000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000400070004500760065006E00740054007900700065002000005B2000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000400070004F007000650072006100740069006F006E0043006F00640065002000004F2000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C0020004000700041006D006F0075006E007400290020000019400070005400650072006D0069006E0061006C0049006400001F4000700050006C0061007900530065007300730069006F006E00490064000017400070004500760065006E0074005400790070006500001F400070004F007000650072006100740069006F006E0043006F006400650000114000700041006D006F0075006E007400003D4500720072006F00720020006F006E00200069006E00730065007200740020006500760065006E0074005F0068006900730074006F00720079002100007F49006E00760061006C0069006400200050006C0061007900530065007300730069006F006E00490064002E0020005200650070006F0072007400650064003A0020007B0030007D002C002000430075007200720065006E0074003A0020007B0031007D002C0020005300740061007400750073003A0020007B0032007D00005B49006E00760061006C006900640020005400650072006D0069006E0061006C002E0020005200650070006F0072007400650064003A0020007B0030007D002C002000430075007200720065006E0074003A0020007B0031007D00005949006E00760061006C006900640020004100630063006F0075006E0074002E0020005200650070006F0072007400650064003A0020007B0030007D002C002000430075007200720065006E0074003A0020007B0031007D00007D4E00650067006100740069007600650020004D00650074006500720073002E002000460069006E0061006C003A0020007B0030007D003B00200050006C0061007900650064003A0020007B0031007D002C00200023007B0032007D003B00200057006F006E003A0020007B0033007D002C00200023007B0034007D00001949006E00740065007200660061006300650033004700530000314D006100780069006D0075006D00420061006C0061006E00630065004500720072006F007200430065006E0074007300004D5400720078005F0055007000640061007400650050006C0061007900530065007300730069006F006E0050006C00610079006500640057006F006E0020006600610069006C00650064002100004D5400720078005F0050006C0061007900530065007300730069006F006E00530065007400460069006E0061006C00420061006C0061006E006300650020006600610069006C00650064002100001D40007000470061006D00650042006100730065004E0061006D0065000011470041004D0045005F00330047005300001D40007000440065006E006F006D0069006E006100740069006F006E00001F40007000570063007000530065007100750065006E006300650049006400002540007000440065006C007400610050006C00610079006500640043006F0075006E007400002740007000440065006C007400610050006C00610079006500640041006D006F0075006E007400001F40007000440065006C007400610057006F006E0043006F0075006E007400002140007000440065006C007400610057006F006E0041006D006F0075006E007400002940007000440065006C00740061004A00610063006B0070006F00740041006D006F0075006E0074000031470061006D0065004D006500740065007200730055007000640061007400650020006600610069006C006500640021000073420061006C0061006E006300650020004D00690073006D0061007400630068003A0020005200650070006F0072007400650064003D007B0030007D002C002000430061006C00630075006C0061007400650064003D007B0031007D002C002000460069006E0061006C003D007B0032007D00000F5300750063006300650073007300002D49006E00760061006C006900640020004100630063006F0075006E00740020004E0075006D0062006500720000254100630063006F0075006E007400200049006E002000530065007300730069006F006E00003749006E00760061006C006900640020004D0061006300680069006E006500200049006E0066006F0072006D006100740069006F006E00001B4100630063006500730073002000440065006E00690065006400003349006E00760061006C00690064002000530065007300730069006F006E0020004900440020004E0075006D006200650072000051460069006E0061006C002000620061006C0061006E0063006500200064006F006500730020006E006F00740020006D0061007400630068002000760061006C00690064006100740069006F006E002E00001D41004D005F004D004F00560045004D0045004E0054005F00490044000019530079007300740065006D002E0049006E00740036003400001F41004D005F004F005000450052004100540049004F004E005F0049004400001B41004D005F004100430043004F0055004E0054005F0049004400000F41004D005F0054005900500045000019530079007300740065006D002E0049006E00740033003200002541004D005F0049004E0049005400490041004C005F00420041004C0041004E0043004500001D530079007300740065006D002E0044006500630069006D0061006C00001B41004D005F005300550042005F0041004D004F0055004E005400001B41004D005F004100440044005F0041004D004F0055004E005400002141004D005F00460049004E0041004C005F00420041004C0041004E0043004500001541004D005F00440045005400410049004C005300001B530079007300740065006D002E0053007400720069006E006700001541004D005F0052004500410053004F004E005300001B41004D005F0054005200410043004B005F004400410054004100001D41004D005F005400450052004D0049004E0041004C005F0049004400002141004D005F005400450052004D0049004E0041004C005F004E0041004D004500002541004D005F0050004C00410059005F00530045005300530049004F004E005F0049004400000340000080BF2000530045004C004500430054002000640062006F002E0054007200610063006B00440061007400610054006F00450078007400650072006E0061006C002800410043005F0054005200410043004B005F00440041005400410029002000460052004F004D0020004100430043004F0055004E00540053002000570048004500520045002000410043005F004100430043004F0055004E0054005F004900440020003D002000400070004100630063006F0075006E0074004900640020000017400070004100630063006F0075006E0074004900640000134D0075006C00740069005300690074006500001149007300430065006E00740065007200004F20004400450043004C0041005200450020002000200040005F0074007200610063006B00640061007400610020004100530020004E0056004100520043004800410052002800350030002900200000392000530045005400200020002000200020002000200040005F0074007200610063006B00640061007400610020003D002000270027002000012F2000490046002000400070004300610072006400440061007400610020004900530020004E0055004C004C0020000080E1200020002000530045004C004500430054002000200040005F0074007200610063006B00640061007400610020003D002000640062006F002E0054007200610063006B00440061007400610054006F00450078007400650072006E0061006C002800410043005F0054005200410043004B005F00440041005400410029002000460052004F004D0020004100430043004F0055004E00540053002000570048004500520045002000410043005F004100430043004F0055004E0054005F004900440020003D002000400070004100630063006F0075006E007400490064002000002B200049004600200040005F0074007200610063006B00640061007400610020003D00200027002700200001492000200020005300450054002000200020002000200040005F0074007200610063006B00640061007400610020003D00200040007000430061007200640044006100740061002000004D4400450043004C0041005200450020002000200040007000530065007100750065006E006300650031003200560061006C0075006500200041005300200042004900470049004E0054002000000320000027550050004400410054004500200020002000530045005100550045004E004300450053002000005B2000200020005300450054002000200020005300450051005F004E004500580054005F00560041004C005500450020003D0020005300450051005F004E004500580054005F00560041004C005500450020002B00200031002000004B2000570048004500520045002000200020005300450051005F00490044002000200020002000200020002000200020003D0020004000700053006500710049006400310032003B0020000080B3530045004C0045004300540020002000200040007000530065007100750065006E006300650031003200560061006C007500650020003D0020005300450051005F004E004500580054005F00560041004C005500450020002D00200031002000460052004F004D002000530045005100550045004E0043004500530020005700480045005200450020005300450051005F004900440020003D0020004000700053006500710049006400310032003B002000016149004E005300450052005400200049004E0054004F002000200020004100430043004F0055004E0054005F004D004F00560045004D0045004E00540053002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002800200041004D005F004100430043004F0055004E0054005F004900440020002000200020002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002C00200041004D005F00540059005000450020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002C00200041004D005F0049004E0049005400490041004C005F00420041004C0041004E0043004500200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002C00200041004D005F005300550042005F0041004D004F0055004E00540020002000200020002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002C00200041004D005F004100440044005F0041004D004F0055004E00540020002000200020002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002C00200041004D005F00460049004E0041004C005F00420041004C0041004E004300450020002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002C00200041004D005F004400410054004500540049004D0045002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002C00200041004D005F0043004100530048004900450052005F004900440020002000200020002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002C00200041004D005F0043004100530048004900450052005F004E0041004D004500200020002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002C00200041004D005F004F005000450052004100540049004F004E005F0049004400200020002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002C00200041004D005F0050004C00410059005F00530045005300530049004F004E005F0049004400200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002C00200041004D005F005400450052004D0049004E0041004C005F00490044002000200020002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002C00200041004D005F005400450052004D0049004E0041004C005F004E0041004D00450020002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002C00200041004D005F00440045005400410049004C00530020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002C00200041004D005F0052004500410053004F004E00530020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002C00200041004D005F0054005200410043004B005F004400410054004100200020002000200020002000200020002000200020002000200020002000200020002000200020002000005D2000200020002000200020002000200020002000200020002C00200041004D005F004D004F00560045004D0045004E0054005F0049004400200020002000200020002000200020002000200020002000200020002000200020002000000529002000006120002000200020002000560041004C00550045005300200028002000400070004100630063006F0075006E007400490064002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002C002000400070005400790070006500200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002C0020004000700049006E0069007400690061006C00420061006C0061006E006300650020002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002C0020004000700053007500620041006D006F0075006E0074002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002C0020004000700041006400640041006D006F0075006E0074002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002C00200040007000460069006E0061006C00420061006C0061006E00630065002000200020002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002C002000470045005400440041005400450028002900200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002C002000400070004300610073006800690065007200490064002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002C0020004000700043006100730068006900650072004E0061006D00650020002000200020002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002C002000400070004F007000650072006100740069006F006E00490064002000200020002000200020002000200020002000200020002000200020002000200020002000200020000080B52000200020002000200020002000200020002000200020002C002000430041005300450020005700480045004E002000490053004E0055004C004C0028004000700050006C0061007900530065007300730069006F006E00490064002C003000290020003D002000300020005400480045004E0020004E0055004C004C00200045004C005300450020004000700050006C0061007900530065007300730069006F006E0049006400200045004E0044002000200000612000200020002000200020002000200020002000200020002C002000400070005400650072006D0069006E0061006C0049006400200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002C002000400070005400650072006D0069006E0061006C004E0061006D0065002000200020002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002C00200040007000440065007400610069006C007300200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002C0020004000700052006500610073006F006E007300200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002C00200040005F0074007200610063006B00640061007400610020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000005D2000200020002000200020002000200020002000200020002C00200040007000530065007100750065006E006300650031003200560061006C0075006500200020002000200020002000200020002000200020002000200020002000004B20005300450054002000400070004D006F00760065006D0065006E0074004900640020003D00200040007000530065007100750065006E006300650031003200560061006C0075006500004920005300450054002000400070004D006F00760065006D0065006E0074004900640020003D002000530043004F00500045005F004900440045004E00540049005400590028002900001B400070004F007000650072006100740069006F006E0049006400000D40007000540079007000650000214000700049006E0069007400690061006C00420061006C0061006E006300650000174000700053007500620041006D006F0075006E00740000174000700041006400640041006D006F0075006E007400001D40007000460069006E0061006C00420061006C0061006E0063006500001340007000440065007400610069006C00730000134000700052006500610073006F006E00730000154000700043006100720064004400610074006100001740007000430061007300680069006500720049006400001B4000700043006100730068006900650072004E0061006D006500001D400070005400650072006D0069006E0061006C004E0061006D00650000134000700053006500710049006400310032000019400070004D006F00760065006D0065006E00740049006400001F43004D005F004F005000450052004100540049004F004E005F0049004400001B43004D005F004100430043004F0055004E0054005F0049004400002543004D005F0043004100520044005F0054005200410043004B005F004400410054004100000F43004D005F005400590050004500002543004D005F0049004E0049005400490041004C005F00420041004C0041004E0043004500001B43004D005F004100440044005F0041004D004F0055004E005400001B43004D005F005300550042005F0041004D004F0055004E005400002143004D005F00460049004E0041004C005F00420041004C0041004E0043004500002943004D005F00420041004C0041004E00430045005F0049004E004300520045004D0045004E005400001543004D005F00440045005400410049004C005300002943004D005F00430055005200520045004E00430059005F00490053004F005F0043004F0044004500001B43004D005F004100550058005F0041004D004F0055004E005400003143004D005F00430055005200520045004E00430059005F00440045004E004F004D0049004E004100540049004F004E00003543004D005F00470041004D0049004E0047005F005400410042004C0045005F00530045005300530049004F004E005F0049004400001543004D005F0043004800490050005F0049004400002B43004D005F0043004100470045005F00430055005200520045004E00430059005F005400590050004500001B430055005200520045004E00430059005F005400590050004500001B43004D005F00520045004C0041005400450044005F0049004400000F43004D005F004400410054004500001F530079007300740065006D002E004400610074006500540069006D00650000392000490046002000400049006E004300610073006800690065007200530065007300730069006F006E00730020003D002000310020002000000F200042004500470049004E002000003D20002000200055005000440041005400450020002000200043004100530048004900450052005F00530045005300530049004F004E00530020002000007F200020002000200020002000530045005400200020002000430053005F00420041004C0041004E0043004500200020002000200020003D002000430053005F00420041004C0041004E004300450020002B002000280040007000420061006C0061006E006300650049006E006300720065006D0065006E00740029002000003F2000200020004F0055005400500055005400200020002000440045004C0045005400450044002E00430053005F00420041004C0041004E00430045002000004320002000200020002000200020002000200020002C00200049004E005300450052005400450044002E00430053005F00420041004C0041004E0043004500200020000057200020002000200057004800450052004500200020002000430053005F00530045005300530049004F004E005F0049004400200020003D0020002000200040007000530065007300730069006F006E00490064002000005920002000200020002000200041004E004400200020002000430053005F005300540041005400550053002000200020002000200049004E00200028002000400070005300740061007400750073004F00700065006E0020000067200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000400070005300740061007400750073004F00700065006E00500065006E00640069006E00670020000063200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C00200040007000530074006100740075007300500065006E00640069006E00670020002900200000132000200020002000200045004E0044002000004B2000490046002000400049006E004300610073006800690065007200530065007300730069006F006E00730042007900430075007200720065006E006300790020003D002000310020000011200042004500470049004E00200020000037200020002000490046002000450058004900530054005300200028002000530045004C00450043005400200020002000310020002000006B20002000200020002000200020002000200020002000200020002000200020002000460052004F004D0020002000200043004100530048004900450052005F00530045005300530049004F004E0053005F00420059005F00430055005200520045004E00430059002000006B2000200020002000200020002000200020002000200020002000200020002000570048004500520045002000200020004300530043005F00530045005300530049004F004E005F004900440020003D00200040007000530065007300730069006F006E00490064002000006720002000200020002000200020002000200020002000200020002000200020002000200041004E0044002000200020004300530043005F00490053004F005F0043004F00440045002000200020003D00200040007000490073006F0043006F00640065002000006320002000200020002000200020002000200020002000200020002000200020002000200041004E0044002000200020004300530043005F00540059005000450020002000200020002000200020003D00200040007000540079007000650029002000005B200020002000200020002000200055005000440041005400450020002000200043004100530048004900450052005F00530045005300530049004F004E0053005F00420059005F00430055005200520045004E0043005900200000809B20002000200020002000200020002000200020005300450054002000200020004300530043005F00420041004C0041004E0043004500200020002000200020003D0020004300530043005F00420041004C0041004E004300450020002B002000280040007000420061006C0061006E0063006500430075007200720065006E006300790049006E006300720065006D0065006E00740029002000004920002000200020002000200020004F0055005400500055005400200020002000440045004C0045005400450044002E004300530043005F00420041004C0041004E00430045002000004B200020002000200020002000200020002000200020002000200020002C00200049004E005300450052005400450044002E004300530043005F00420041004C0041004E00430045002000006B20002000200020002000200020002000570048004500520045002000200020004300530043005F00530045005300530049004F004E005F00490044002000200020002000200020002000200020003D00200040007000530065007300730069006F006E004900640020000067200020002000200020002000200020002000200041004E0044002000200020004300530043005F00490053004F005F0043004F0044004500200020002000200020002000200020002000200020003D00200040007000490073006F0043006F006400650020000061200020002000200020002000200020002000200041004E0044002000200020004300530043005F0054005900500045002000200020002000200020002000200020002000200020002000200020003D00200040007000540079007000650020000033200020002000200020002000200020002000200041004E004400200020002000450058004900530054005300200028002000005B200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000530045004C004500430054002000430053005F00530045005300530049004F004E005F00490044002000006120002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000460052004F004D00200043004100530048004900450052005F00530045005300530049004F004E0053002000007D2000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000570048004500520045002000430053005F00530045005300530049004F004E005F0049004400200020003D0020002000200040007000530065007300730069006F006E00490064002000007F20002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200041004E0044002000430053005F005300540041005400550053002000200020002000200049004E00200028002000400070005300740061007400750073004F00700065006E00200000808D2000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000400070005300740061007400750073004F00700065006E00500065006E00640069006E00670020000080892000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C00200040007000530074006100740075007300500065006E00640069006E0067002000290020000037200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200029002000000F2000200045004C0053004500200000612000200020002000200049004E005300450052005400200049004E0054004F0020002000200043004100530048004900450052005F00530045005300530049004F004E0053005F00420059005F00430055005200520045004E00430059002000004520002000200020002000200020002000200020002000200020002000200020002000280020004300530043005F00530045005300530049004F004E005F004900440020000041200020002000200020002000200020002000200020002000200020002000200020002C0020004300530043005F00490053004F005F0043004F004400450020000039200020002000200020002000200020002000200020002000200020002000200020002C0020004300530043005F0054005900500045002000003F200020002000200020002000200020002000200020002000200020002000200020002C0020004300530043005F00420041004C0041004E004300450020000045200020002000200020002000200020002000200020002000200020002000200020002C0020004300530043005F0043004F004C004C00450043005400450044002000200000272000200020002000200020002000200020002000200020002000200020002000200029002000002F20002000200020002000200020002000200020004F005500540050005500540020002000200030002E00300020000051200020002000200020002000200020002000200020002000200020002000200020002C00200049004E005300450052005400450044002E004300530043005F00420041004C0041004E0043004500200000292000200020002000200020002000200020002000560041004C005500450053002000200020002000003F200020002000200020002000200020002000200020002000200020002000200020002800200040007000530065007300730069006F006E00490064002000003B200020002000200020002000200020002000200020002000200020002000200020002C00200040007000490073006F0043006F006400650020000035200020002000200020002000200020002000200020002000200020002000200020002C0020004000700054007900700065002000005D200020002000200020002000200020002000200020002000200020002000200020002C00200040007000420061006C0061006E0063006500430075007200720065006E006300790049006E006300720065006D0065006E0074002000002F200020002000200020002000200020002000200020002000200020002000200020002C00200030002E0030002000000F20002000200045004E0044002000001740007000530065007300730069006F006E00490064000019400070005300740061007400750073004F00700065006E000027400070005300740061007400750073004F00700065006E00500065006E00640069006E006700001F40007000530074006100740075007300500065006E00640069006E006700001340007000490073006F0043006F0064006500002540007000420061006C0061006E006300650049006E006300720065006D0065006E007400003540007000420061006C0061006E0063006500430075007200720065006E006300790049006E006300720065006D0065006E0074000025400049006E004300610073006800690065007200530065007300730069006F006E0073000039400049006E004300610073006800690065007200530065007300730069006F006E00730042007900430075007200720065006E0063007900001F52006500670069006F006E0061006C004F007000740069006F006E007300001F430075007200720065006E0063007900490053004F0043006F00640065000019470061006D0069006E0067005400610062006C0065007300001943006100730068006900650072002E004D006F006400650000033100004B20004400450043004C0041005200450020002000200040005F0063006D005F006D006F00760065006D0065006E0074005F0069006400200041005300200042004900470049004E00540000808520005300450054002000200020002000200020002000400070004300610067006500430075007200720065006E006300690065007300540079007000650020003D002000490053004E0055004C004C002800400070004300610067006500430075007200720065006E00630069006500730054007900700065002C00200030002900200000672000490046002000400070004300610072006400440061007400610020004900530020004E0055004C004C00200041004E0044002000400070004100630063006F0075006E0074004900640020004900530020004E004F00540020004E0055004C004C0020000057200049004E005300450052005400200049004E0054004F0020002000200043004100530048004900450052005F004D004F00560045004D0045004E005400530020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002800200043004D005F00530045005300530049004F004E005F0049004400200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C00200043004D005F0043004100530048004900450052005F0049004400200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C00200043004D005F0055005300450052005F0049004400200020002000200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C00200043004D005F005400590050004500200020002000200020002000200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C00200043004D005F0049004E0049005400490041004C005F00420041004C0041004E00430045002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C00200043004D005F004100440044005F0041004D004F0055004E005400200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C00200043004D005F005300550042005F0041004D004F0055004E005400200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C00200043004D005F00460049004E0041004C005F00420041004C0041004E0043004500200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C00200043004D005F0055005300450052005F004E0041004D0045002000200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C00200043004D005F0043004100530048004900450052005F004E0041004D0045002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C00200043004D005F0043004100520044005F0054005200410043004B005F0044004100540041002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C00200043004D005F004100430043004F0055004E0054005F0049004400200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C00200043004D005F004F005000450052004100540049004F004E005F00490044002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C00200043004D005F00440045005400410049004C005300200020002000200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C00200043004D005F00430055005200520045004E00430059005F00490053004F005F0043004F004400450020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C00200043004D005F004100550058005F0041004D004F0055004E005400200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C00200043004D005F00430055005200520045004E00430059005F00440045004E004F004D0049004E004100540049004F004E002000200020002000005720002000200020002000200020002000200020002000200020002C00200043004D005F00470041004D0049004E0047005F005400410042004C0045005F00530045005300530049004F004E005F004900440020002000005720002000200020002000200020002000200020002000200020002C00200043004D005F0043004800490050005F0049004400200020002000200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C00200043004D005F0043004100470045005F00430055005200520045004E00430059005F0054005900500045002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C00200043004D005F00520045004C0041005400450044005F0049004400200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C00200043004D005F004400410054004500200020002000200020002000200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002900200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000057200020002000560041004C0055004500530020002000200020002800200040007000530065007300730069006F006E004900640020002000200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C0020004000700043006100730068006900650072004900640020002000200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C0020004000700055007300650072004900640020002000200020002000200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C0020004000700054007900700065002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C0020004000700049006E0069007400690061006C00420061006C0061006E0063006500200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C0020004000700041006400640041006D006F0075006E00740020002000200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C0020004000700053007500620041006D006F0075006E00740020002000200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C00200040007000460069006E0061006C00420061006C0061006E006300650020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C0020004000700055007300650072004E0061006D006500200020002000200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C0020004000700043006100730068006900650072004E0061006D006500200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C00200040005F0074007200610063006B00640061007400610020002000200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C002000400070004100630063006F0075006E0074004900640020002000200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C002000400070004F007000650072006100740069006F006E0049006400200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C00200040007000440065007400610069006C0073002000200020002000200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C00200040007000430075007200720065006E006300790043006F006400650020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C0020004000700041007500780041006D006F0075006E00740020002000200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C00200040007000430075007200720065006E0063007900440065006E006F006D0069006E006100740069006F006E00200020002000200020002000005720002000200020002000200020002000200020002000200020002C00200040007000470061006D0069006E0067005400610062006C006500530065007300730069006F006E0049006400200020002000200020002000005720002000200020002000200020002000200020002000200020002C0020004000700043006800690070004900640020002000200020002000200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C002000400070004300610067006500430075007200720065006E006300690065007300540079007000650020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C00200040007000520065006C0061007400650064004900640020002000200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C002000690073004E0075006C006C0028004000700044006100740065002C00200047004500540044004100540045002800290029002000200020000057200053004500540020002000200040005F0063006D005F006D006F00760065006D0065006E0074005F006900640020003D002000530043004F00500045005F004900440045004E0054004900540059002800290020000080AD49004600200040007000540079007000650020003E003D00200040007000430061006700650046006900720073007400560061006C0075006500200041004E004400200040007000540079007000650020003C003D0020004000700043006100670065004C00610073007400560061006C0075006500200041004E004400200040007000450078007400650072006E004D006F00760065006D0065006E0074004900640020003E0020003000000B42004500470049004E00005F2000200049004E005300450052005400200049004E0054004F0020002000200043004100470045005F0043004100530048004900450052005F004D004F00560045004D0045004E0054005F00520045004C004100540049004F004E00200000212000200020002000200020002000560041004C005500450053002000280020000045200020002000200020002000200020002000200020002000200020002000200040007000450078007400650072006E004D006F00760065006D0065006E007400490064000041200020002000200020002000200020002000200020002000200020002C00200040005F0063006D005F006D006F00760065006D0065006E0074005F006900640000212000200020002000200020002000200020002000200020002000200029002000000745004E0044000015200042004500470049004E002000540052005900005F200049004600200040007000430075007200720065006E006300790043006F006400650020004900530020004E0055004C004C00200020002000200020002000200020002000200020002000200020002000200020002000200020002000005F200020002000530045005400200040007000430075007200720065006E006300790043006F006400650020003D002000400070004E006100740069006F006E0061006C00430075007200720065006E006300790043006F006400650020000080B320002000200020002000200045005800450043002000640062006F002E0043006100730068006900650072004D006F00760065006D0065006E007400730048006900730074006F0072007900200040007000530065007300730069006F006E00490064002C00200040005F0063006D005F006D006F00760065006D0065006E0074005F00690064002C0020004000700054007900700065002C0020004000700053007500620054007900700065002C0020000080C72000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020004000700049006E0069007400690061006C00420061006C0061006E00630065002C0020004000700041006400640041006D006F0075006E0074002C0020004000700053007500620041006D006F0075006E0074002C00200040007000460069006E0061006C00420061006C0061006E00630065002C0020000080E120002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200040007000430075007200720065006E006300790043006F00640065002C0020004000700041007500780041006D006F0075006E0074002C00200040007000430075007200720065006E0063007900440065006E006F006D0069006E006100740069006F006E002C002000400070004300610067006500430075007200720065006E00630069006500730054007900700065000013200045004E00440020005400520059002000001B200042004500470049004E002000430041005400430048002000005920002000200020004400450043004C0041005200450020002000200040004500720072006F0072004D0065007300730061006700650020004E0056004100520043004800410052002800340030003000300029003B002000004520002000200020004400450043004C0041005200450020002000200040004500720072006F00720053006500760065007200690074007900200049004E0054003B002000003F20002000200020004400450043004C0041005200450020002000200040004500720072006F00720053007400610074006500200049004E0054003B00200000192000200020002000530045004C004500430054002000200000532000200020002000200020002000200040004500720072006F0072004D0065007300730061006700650020003D0020004500520052004F0052005F004D00450053005300410047004500280029002C00200000572000200020002000200020002000200040004500720072006F0072005300650076006500720069007400790020003D0020004500520052004F0052005F0053004500560045005200490054005900280029002C002000004B2000200020002000200020002000200040004500720072006F0072005300740061007400650020003D0020004500520052004F0052005F0053005400410054004500280029003B002000003D200020002000200052004100490053004500520052004F0052002000280040004500720072006F0072004D006500730073006100670065002C002000003F20002000200020002000200020002000200020002000200020002000200040004500720072006F007200530065007600650072006900740079002C002000003920002000200020002000200020002000200020002000200020002000200040004500720072006F0072005300740061007400650029003B000015200045004E004400200043004100540043004800001D40007000430075007200720065006E006300790043006F006400650000174000700041007500780041006D006F0075006E007400002D400070004E006100740069006F006E0061006C00430075007200720065006E006300790043006F006400650000114000700055007300650072004900640000154000700055007300650072004E0061006D0065000013400070005300750062005400790070006500002D40007000430075007200720065006E0063007900440065006E006F006D0069006E006100740069006F006E00002D40007000470061006D0069006E0067005400610062006C006500530065007300730069006F006E0049006400002140007000430061006700650046006900720073007400560061006C0075006500001F4000700043006100670065004C00610073007400560061006C0075006500002540007000450078007400650072006E004D006F00760065006D0065006E007400490064000011400070004300680069007000490064000029400070004300610067006500430075007200720065006E0063006900650073005400790070006500001740007000520065006C00610074006500640049006400000D400070004400610074006500005B20002000200020002000200020002000200020004100430050005F004100430043004F0055004E0054005F004900440020002000200020002000200020003D002000400070004100630063006F0075006E0074004900640020000061200020002000200041004E0044002000200020004100430050005F00530054004100540055005300200020002000200020002000200020002000200020003D00200040007000530074006100740075007300410063007400690076006500200000808F200020002000200041004E0044002000200020004100430050005F004300520045004400490054005F00540059005000450020002000200020002000200049004E002000280020004000700043007200650064006900740054007900700065004E00520031002C0020004000700043007200650064006900740054007900700065004E0052003200200029002000006B200020002000200041004E0044002000200020004100430050005F00500052004F004D004F005F00540059005000450020002000200020002000200020003C003E00200040007000500072006F006D006F0043006F0076006500720043006F00750070006F006E0020000049200020002000200041004E0044002000200020004100430050005F00420041004C0041004E004300450020002000200020002000200020002000200020003E003D002000300020000080ED4900460020002800530045004C00450043005400200043004F0055004E00540028002A002900200020002000200020002000460052004F004D0020004100430043004F0055004E0054005F00500052004F004D004F00540049004F004E00530020005700490054004800280049004E004400450058002800490058005F004100430050005F004100430043004F0055004E0054005F005300540041005400550053002900290020005700480045005200450020004100430050005F0057004F004E004C004F0043004B0020004900530020004E004F00540020004E0055004C004C00200041004E0044002000000D290020003E002000300020000080ED2000200020002000530045004C0045004300540020004100430050005F0055004E0049005100550045005F00490044002000460052004F004D0020004100430043004F0055004E0054005F00500052004F004D004F00540049004F004E00530020005700490054004800280049004E004400450058002800490058005F004100430050005F004100430043004F0055004E0054005F005300540041005400550053002900290020005700480045005200450020004100430050005F0057004F004E004C004F0043004B0020004900530020004E004F00540020004E0055004C004C00200041004E0044002000000B45004C005300450020000080E9200020004900460020002800530045004C00450043005400200043004F0055004E00540028002A002900200020002000200020002000460052004F004D0020004100430043004F0055004E0054005F00500052004F004D004F00540049004F004E00530020005700490054004800280049004E004400450058002800490058005F004100430050005F004100430043004F0055004E0054005F005300540041005400550053002900290020005700480045005200450020004100430050005F0057004F004E004C004F0043004B0020004900530020004E0055004C004C00200041004E00440020000080E9200020002000200020002000530045004C0045004300540020004100430050005F0055004E0049005100550045005F00490044002000460052004F004D0020004100430043004F0055004E0054005F00500052004F004D004F00540049004F004E00530020005700490054004800280049004E004400450058002800490058005F004100430050005F004100430043004F0055004E0054005F005300540041005400550053002900290020005700480045005200450020004100430050005F0057004F004E004C004F0043004B0020004900530020004E0055004C004C00200041004E00440020000043200020002000200020002000530045004C0045004300540020004300410053005400200028002D003100200041005300200042004900470049004E00540029002000014D200020002000200041004E0044002000200020004100430050005F0050004C00410059005F00530045005300530049004F004E005F004900440020004900530020004E0055004C004C0020000063200020002000200041004E0044002000200020004100430050005F004300520045004400490054005F0054005900500045002000200020002000200020003D0020004000700043007200650064006900740054007900700065004E00520031002000005B200020002000200041004E0044002000200020004100430050005F00420041004C0041004E00430045002000200020002000200020002000200020003E003D0020004100430050005F0057004F004E004C004F0043004B0020000055200020002000200041004E0044002000200020004100430050005F0057004F004E004C004F0043004B002000200020002000200020002000200020004900530020004E004F00540020004E0055004C004C00200000312000530045004C004500430054002000200020004100430050005F0055004E0049005100550045005F00490044002000007F200020002000460052004F004D002000200020004100430043004F0055004E0054005F00500052004F004D004F00540049004F004E00530020005700490054004800280049004E004400450058002800490058005F006100630070005F006100630063006F0075006E0074005F007300740061007400750073002900290000152000200057004800450052004500200020002000001D40007000530074006100740075007300410063007400690076006500001F4000700043007200650064006900740054007900700065004E0052003100001F4000700043007200650064006900740054007900700065004E0052003200002540007000500072006F006D006F0043006F0076006500720043006F00750070006F006E00003B20005500500044004100540045002000200020004100430043004F0055004E0054005F00500052004F004D004F00540049004F004E0053002000005B20002000200020005300450054002000200020004100430050005F00530054004100540055005300200020002000200020002000200020002000200020003D002000400070004E00650077005300740061007400750073002000003D20004F0055005400500055005400200020002000440045004C0045005400450044002E004100430050005F00420041004C0041004E00430045002000008081200020002000460052004F004D002000200020004100430043004F0055004E0054005F00500052004F004D004F00540049004F004E00530020005700490054004800280049004E00440045005800280050004B005F006100630063006F0075006E0074005F00700072006F006D006F00740069006F006E007300290029002000005920002000570048004500520045002000200020004100430050005F0055004E0049005100550045005F0049004400200020002000200020002000200020003D0020004000700055006E0069007100750065004900640020000015200020002000200041004E00440020002000200000154000700055006E006900710075006500490064000017400070004E00650077005300740061007400750073000059530051004C005F0042005500530049004E004500530053005F004C004F00470049004300200064006F00650073006E0027007400200073007500700070006F007200740020005400490054004F0020004D006F0064006500010953006900740065000025440069007300610062006C0065004E0065007700530065007300730069006F006E007300004B530079007300740065006D0020006800610073002000640069007300610062006C006500640020006E0065007700200050006C0061007900530065007300730069006F006E0073002E000015530079007300740065006D004D006F0064006500000334000080B9530045004C004500430054002000540045005F00490053004F005F0043004F00440045002C002000540045005F005600490052005400550041004C005F004100430043004F0055004E0054005F00490044002000460052004F004D0020005400450052004D0049004E0041004C0053002000570048004500520045002000540045005F005400450052004D0049004E0041004C005F004900440020003D002000400070005400650072006D0069006E0061006C004900640000635400720078005F005500700064006100740065004100630063006F0075006E007400420061006C0061006E0063006500200047006500740020007400650072006D0069006E0061006C00200069006E0066006F0020006600610069006C0065006400005B2000200020002000530045004C00450043005400200020002000500053005F004100430043004F0055004E0054005F00490044002000200020002000200020002000200020002000200020002000200020002000200020002000005B200020002000200020002000200020002000200020002C002000410043005F0054005900500045002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000005B200020002000200020002000460052004F004D0020002000200050004C00410059005F00530045005300530049004F004E0053002000200020002000200020002000200020002000200020002000200020002000200020002000005B20004C0045004600540020004A004F0049004E002000200020004100430043004F0055004E0054005300200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000005B200020002000200020002000200020004F004E00200020002000410043005F004100430043004F0055004E0054005F0049004400200020003D002000500053005F004100430043004F0055004E0054005F004900440020002000005B2000200020002000200057004800450052004500200020002000500053005F005400450052004D0049004E0041004C005F004900440020003D002000400070005400650072006D0069006E0061006C0049006400200020002000005B200020002000200020002000200041004E004400200020002000500053005F005300540041005400550053002000200020002000200020003D002000400070005300740061007400750073002000200020002000200020002000001140007000530074006100740075007300005B5400720078005F005500700064006100740065004100630063006F0075006E007400420061006C0061006E006300650020004C004F0043004B0020006100630063006F0075006E00740020006600610069006C00650064002E0000395400720078005F0047006500740050006C0061007900530065007300730069006F006E004900640020006600610069006C00650064002E0000474C004300440020005400720061006E007300660065007200200069006E00200033004700530020006E006F007400200073007500700070006F00720074006500640021002100003D5400720078005F0047006500740050006C0061007900610062006C006500420061006C0061006E006300650020006600610069006C00650064002E00006F57006900740068006F00750074002000700072006F006D006F00740069006F006E007300200074006F0020007400720061006E0073006600650072003A002000420061006C0061006E00630065002000410063007400690076006500500072006F006D006F00730020003D002000002D2C002000420061006C0061006E006300650054006F005400720061006E00730066006500720020003D002000001F2C0020005400650072006D0069006E0061006C004900640020003D002000005B5400720078005F005400690074006F005500700064006100740065004100630063006F0075006E007400500072006F006D006F00740069006F006E00420061006C0061006E006300650020006600610069006C00650064002E0000435400720078005F00520065007300650074004100630063006F0075006E00740049006E00530065007300730069006F006E0020006600610069006C00650064002E0000655400720078005F005500700064006100740065004100630063006F0075006E007400420061006C0061006E006300650020006600610069006C00650064002E00200050006C0061007900610062006C006500420061006C0061006E00630065003A00200000674100630063006F0075006E0074004D006F00760065006D0065006E00740073005400610062006C0065002E004100640064002800530074006100720074004300610072006400530065007300730069006F006E00290020006600610069006C00650064002E0000454100630063006F0075006E0074004D006F00760065006D0065006E00740073005400610062006C0065002E00530061007600650020006600610069006C00650064002E00002F5400720078005F005300650074004100630074006900760069007400790020006600610069006C00650064002E00001F50006C0061007900530065007300730069006F006E00490064003A00200000272C00200050006C0061007900610062006C006500420061006C0061006E00630065003A002000005B5400720078005F00420061006C0061006E006300650054006F0050006C0061007900530065007300730069006F006E0020006600610069006C00650064003A0020004E0065007700530065007300730069006F006E003A00200000232C00200050006C0061007900530065007300730069006F006E00490064003A00200000695400720078005F005500700064006100740065004100630063006F0075006E00740049006E00530065007300730069006F006E0020006600610069006C00650064002E00200050006C0061007900610062006C006500420061006C0061006E00630065003A002000005F200020002000530045004C0045004300540020002000200020002000640062006F002E004100700070006C007900450078006300680061006E006700650032002800200020004000700041006D006F0075006E0074002C00200020002000005F2000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200040007000460072006F006D00490073006F002C0020002000005F200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020004000700054006F00490073006F0020002000200020002000005F20002000200020002000200020002000200020002000200020002000290020002000410053002000450078006300680061006E00670065006400200020002000200020002000200020002000200020002000200020002000200020002000001340007000460072006F006D00490073006F00000F4000700054006F00490073006F000013450078006300680061006E0067006500640000808155005000440041005400450020004100430043004F0055004E00540053002000530045005400200020002000410043005F00430055005200520045004E0054005F005400450052004D0049004E0041004C005F0049004400200020002000200020003D002000400070005400650072006D0069006E0061006C004900640020000080EF200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000410043005F00430055005200520045004E0054005F005400450052004D0049004E0041004C005F004E0041004D0045002000200020003D0020002800530045004C004500430054002000540045005F004E0041004D0045002000460052004F004D0020005400450052004D0049004E0041004C0053002000570048004500520045002000540045005F005400450052004D0049004E0041004C005F004900440020003D002000400070005400650072006D0069006E0061006C004900640029002000008087200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000410043005F00430055005200520045004E0054005F0050004C00410059005F00530045005300530049004F004E005F004900440020003D0020004000700050006C0061007900530065007300730069006F006E00490064002000007F2000200020002000200020002000200020002000200020002000200057004800450052004500200020002000410043005F004100430043004F0055004E0054005F0049004400200020002000200020002000200020002000200020002000200020003D002000400070004100630063006F0075006E0074004900640020000073200020002000200020002000200020002000200020002000200020002000200041004E004400200020002000410043005F00430055005200520045004E0054005F005400450052004D0049004E0041004C005F0049004400200020002000200020004900530020004E0055004C004C0020000073200020002000200020002000200020002000200020002000200020002000200041004E004400200020002000410043005F00430055005200520045004E0054005F005400450052004D0049004E0041004C005F004E0041004D0045002000200020004900530020004E0055004C004C0020000073200020002000200020002000200020002000200020002000200020002000200041004E004400200020002000410043005F00430055005200520045004E0054005F0050004C00410059005F00530045005300530049004F004E005F004900440020004900530020004E0055004C004C002000000720003B002000007F55005000440041005400450020005400450052004D0049004E0041004C005300200053004500540020002000540045005F00430055005200520045004E0054005F004100430043004F0055004E0054005F00490044002000200020002000200020003D002000400070004100630063006F0075006E007400490064002000008087200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000540045005F00430055005200520045004E0054005F0050004C00410059005F00530045005300530049004F004E005F004900440020003D0020004000700050006C0061007900530065007300730069006F006E004900640020000080812000200020002000200020002000200020002000200020002000200057004800450052004500200020002000540045005F005400450052004D0049004E0041004C005F004900440020002000200020002000200020002000200020002000200020003D002000400070005400650072006D0069006E0061006C00490064002000008097200055005000440041005400450020004100430043004F0055004E00540053002000530045005400200020002000410043005F004C004100530054005F005400450052004D0049004E0041004C005F0049004400200020002000200020002000200020003D002000410043005F00430055005200520045004E0054005F005400450052004D0049004E0041004C005F0049004400200000809B2000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000410043005F004C004100530054005F005400450052004D0049004E0041004C005F004E0041004D0045002000200020002000200020003D002000410043005F00430055005200520045004E0054005F005400450052004D0049004E0041004C005F004E0041004D004500200000809F2000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000410043005F004C004100530054005F0050004C00410059005F00530045005300530049004F004E005F004900440020002000200020003D002000410043005F00430055005200520045004E0054005F0050004C00410059005F00530045005300530049004F004E005F0049004400200000732000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000410043005F00430055005200520045004E0054005F005400450052004D0049004E0041004C005F0049004400200020002000200020003D0020004E0055004C004C00200000732000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000410043005F00430055005200520045004E0054005F005400450052004D0049004E0041004C005F004E0041004D0045002000200020003D0020004E0055004C004C00200000732000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000410043005F00430055005200520045004E0054005F0050004C00410059005F00530045005300530049004F004E005F004900440020003D0020004E0055004C004C00200000808120002000200020002000200020002000200020002000200020002000200057004800450052004500200020002000410043005F004100430043004F0055004E0054005F0049004400200020002000200020002000200020002000200020002000200020003D002000400070004100630063006F0075006E0074004900640020000080832000200020002000200020002000200020002000200020002000200020002000200041004E004400200020002000410043005F00430055005200520045004E0054005F005400450052004D0049004E0041004C005F0049004400200020002000200020003D002000400070005400650072006D0069006E0061006C004900640020000080892000200020002000200020002000200020002000200020002000200020002000200041004E004400200020002000410043005F00430055005200520045004E0054005F0050004C00410059005F00530045005300530049004F004E005F004900440020003D0020004000700050006C0061007900530065007300730069006F006E004900640020000073200055005000440041005400450020005400450052004D0049004E0041004C005300200053004500540020002000540045005F00430055005200520045004E0054005F004100430043004F0055004E0054005F00490044002000200020002000200020003D0020004E0055004C004C00200000732000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000540045005F00430055005200520045004E0054005F0050004C00410059005F00530045005300530049004F004E005F004900440020003D0020004E0055004C004C00200000808320002000200020002000200020002000200020002000200020002000200057004800450052004500200020002000540045005F005400450052004D0049004E0041004C005F004900440020002000200020002000200020002000200020002000200020003D002000400070005400650072006D0069006E0061006C004900640020000080812000200020002000200020002000200020002000200020002000200020002000200041004E004400200020002000540045005F00430055005200520045004E0054005F004100430043004F0055004E0054005F00490044002000200020002000200020003D002000400070004100630063006F0075006E0074004900640020000080892000200020002000200020002000200020002000200020002000200020002000200041004E004400200020002000540045005F00430055005200520045004E0054005F0050004C00410059005F00530045005300530049004F004E005F004900440020003D0020004000700050006C0061007900530065007300730069006F006E0049006400200000432000530045004C00450043005400200020002000410043005F00430055005200520045004E0054005F005400450052004D0049004E0041004C005F00490044002000004B200020002000200020002000200020002C002000410043005F00430055005200520045004E0054005F0050004C00410059005F00530045005300530049004F004E005F004900440020000025200020002000200020002000200020002C002000410043005F00540059005000450020000029200020002000200020002000200020002C002000500053005F0053005400410054005500530020000031200020002000200020002000200020002C002000410043005F0054005200410043004B005F0044004100540041002000003D200020002000200020002000200020002C002000500053005F00420041004C0041004E00430045005F004D00490053004D00410054004300480020000043200020002000200020002000200020002C002000490053004E0055004C004C002800500053005F0057004F004E005F0043004F0055004E0054002C003000290020000049200020002000200020002000200020002C002000490053004E0055004C004C002800500053005F0050004C0041005900450044005F0043004F0055004E0054002C003000290020000049200020002000200020002000200020002C002000490053004E0055004C004C002800500053005F00520045005F005400490043004B00450054005F0049004E002C003000290020000055200020002000200020002000200020002C002000490053004E0055004C004C002800500053005F00500052004F004D004F005F00520045005F005400490043004B00450054005F0049004E002C003000290020000055200020002000200020002000200020002C002000490053004E0055004C004C002800500053005F00500052004F004D004F005F004E0052005F005400490043004B00450054005F0049004E002C00300029002000004B200020002000200020002000200020002C002000490053004E0055004C004C002800500053005F00520045005F005400490043004B00450054005F004F00550054002C003000290020000057200020002000200020002000200020002C002000490053004E0055004C004C002800500053005F00500052004F004D004F005F004E0052005F005400490043004B00450054005F004F00550054002C003000290020000080BD200020002000460052004F004D002000200020004100430043004F0055004E005400530020004C0045004600540020004A004F0049004E00200050004C00410059005F00530045005300530049004F004E00530020004F004E002000410043005F00430055005200520045004E0054005F0050004C00410059005F00530045005300530049004F004E005F004900440020003D002000500053005F0050004C00410059005F00530045005300530049004F004E005F00490044002000004D2000200057004800450052004500200020002000410043005F004100430043004F0055004E0054005F004900440020003D002000400070004100630063006F0075006E007400490064002000005320004400450043004C00410052004500200040004100630074006900760069007400790044007500650054006F00430061007200640049006E00200041005300200049004E0054004500470045005200200000792000530045004C0045004300540020002000200040004100630074006900760069007400790044007500650054006F00430061007200640049006E0020003D002000430041005300540020002800470050005F004B00450059005F00560041004C0055004500200041005300200049004E005400290020000079200020002000460052004F004D00200020002000470045004E004500520041004C005F0050004100520041004D00530020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000792000200057004800450052004500200020002000470050005F00470052004F00550050005F004B00450059002000200020003D002000270050006C006100790027002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000179200020002000200041004E004400200020002000470050005F005300550042004A004500430054005F004B004500590020003D00200027004100630074006900760069007400790044007500650054006F00430061007200640049006E002700200020002000200020002000200020002000200020002000014B20004900460020002800200040004100630074006900760069007400790044007500650054006F00430061007200640049006E0020004900530020004E0055004C004C00200029002000004B200042004500470049004E002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000004B200020002000530045005400200040004100630074006900760069007400790044007500650054006F00430061007200640049006E0020003D0020003000200020002000200020002000004B200045004E0044002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000061200049004E005300450052005400200049004E0054004F00200050004C00410059005F00530045005300530049004F004E00530020002800500053005F004100430043004F0055004E0054005F004900440020002000200020002000200020000061200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000500053005F005400450052004D0049004E0041004C005F00490044002000200020002000200020000061200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000500053005F00540059005000450020002000200020002000200020002000200020002000200020000061200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000500053005F0054005900500045005F004400410054004100200020002000200020002000200020000061200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000500053005F0049004E0049005400490041004C005F00420041004C0041004E0043004500200020000061200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000500053005F00460049004E0041004C005F00420041004C0041004E004300450020002000200020000061200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000500053005F00460049004E00490053004800450044002000200020002000200020002000200020000061200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000500053005F005300540041004E0044005F0041004C004F004E004500200020002000200020002900006120002000200020002000200020002000200020002000200020002000200020002000200020002000560041004C0055004500530020002800400070004100630063006F0075006E007400490064002000200020002000200020002000200020000061200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000400070005400650072006D0069006E0061006C0049006400200020002000200020002000200020000061200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000400070005400790070006500570069006E00200020002000200020002000200020002000200020000061200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000400070005400790070006500440061007400610020002000200020002000200020002000200020000061200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C00200040007000440065006600610075006C007400420061006C0061006E006300650020002000200020000080BD200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000430041005300450020005700480045004E002000280040004100630074006900760069007400790044007500650054006F00430061007200640049006E0020003D0020003100290020005400480045004E002000470045005400440041005400450028002900200045004C005300450020004E0055004C004C00200045004E00440020000063200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000400070005300740061006E00640041006C006F006E0065002000200020002000200020002000200029000051200053004500540020004000700050006C0061007900530065007300730069006F006E004900640020003D002000530043004F00500045005F004900440045004E005400490054005900280029002000001B400070005400650072006D0069006E0061006C004900640020000013400070005400790070006500570069006E00002140007000440065006600610075006C007400420061006C0061006E0063006500001540007000540079007000650044006100740061000019400070005300740061006E00640041006C006F006E0065000080BF550050004400410054004500200050004C00410059005F00530045005300530049004F004E00530020005300450054002000500053005F004100430043004F0055004E0054005F004900440020003D002000400070004100630063006F0075006E007400490064002000570048004500520045002000500053005F0050004C00410059005F00530045005300530049004F004E005F004900440020003D0020004000700050006C0061007900530065007300730069006F006E004900640000214000700050006C0061007900530065007300730069006F006E00490064002000005549004E005300450052005400200049004E0054004F00200050004C00410059005F00530045005300530049004F004E005300200028002000500053005F004100430043004F0055004E0054005F004900440020000057200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000500053005F005400450052004D0049004E0041004C005F004900440020000049200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000500053005F0054005900500045002000005F200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000500053005F0049004E0049005400490041004C005F00420041004C0041004E00430045002000005B200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000500053005F00460049004E0041004C005F00420041004C0041004E004300450020000059200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000500053005F0050004C0041005900450044005F0043004F0055004E0054002000005B200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000500053005F0050004C0041005900450044005F0041004D004F0055004E00540020000053200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000500053005F0057004F004E005F0043004F0055004E00540020000055200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000500053005F0057004F004E005F0041004D004F0055004E0054002000004F200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000500053005F0043004100530048005F0049004E0020000051200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000500053005F0043004100530048005F004F00550054002000004D200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000500053005F005300540041005400550053002000004F200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000500053005F00530054004100520054004500440020000051200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000500053005F00460049004E004900530048004500440020000057200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000500053005F005300540041004E0044005F0041004C004F004E0045002000004B200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000500053005F00500052004F004D004F0020000039200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002900200000512000200020002000200020002000200020002000200020002000200020002000200020002000560041004C00550045005300200028002000400070004100630063006F0075006E0074004900640020000053200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000400070005400650072006D0069006E0061006C004900640020000047200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C0020004000700054007900700065002000005B200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C0020004000700049006E0069007400690061006C00420061006C0061006E006300650020000057200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C00200040007000460069006E0061006C00420061006C0061006E00630065002000003D200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000300020000051200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C0020004000700057006F006E0041006D006F0075006E0074002000004B200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000400070005300740061007400750073002000004D200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C00200047004500540044004100540045002800290020000053200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000400070005300740061006E00640041006C006F006E006500200000808520002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200053004500540020004000700050006C0061007900530065007300730069006F006E004900640020003D002000530043004F00500045005F004900440045004E00540049005400590028002900200000174000700057006F006E0041006D006F0075006E007400002F55005000440041005400450020002000200050004C00410059005F00530045005300530049004F004E00530020000080F5200020002000530045005400200020002000500053005F0050004C0041005900450044005F0043004F0055004E00540020002000200020003D002000430041005300450020005700480045004E00200028004000700050006C00610079006500640043006F0075006E007400200020003E002000500053005F0050004C0041005900450044005F0043004F0055004E0054002900200020005400480045004E0020004000700050006C00610079006500640043006F0075006E0074002000200045004C00530045002000500053005F0050004C0041005900450044005F0043004F0055004E0054002000200045004E0044002000005D20002000200020002000200020002C002000500053005F0050004C0041005900450044005F0041004D004F0055004E0054002000200020003D0020004000700050006C00610079006500640041006D006F0075006E007400200020000080F520002000200020002000200020002C002000500053005F0057004F004E005F0043004F0055004E00540020002000200020002000200020003D002000430041005300450020005700480045004E00200028004000700057006F006E0043006F0075006E007400200020002000200020003E002000500053005F0057004F004E005F0043004F0055004E0054002900200020002000200020005400480045004E0020004000700057006F006E0043006F0075006E0074002000200020002000200045004C00530045002000500053005F0057004F004E005F0043004F0055004E0054002000200020002000200045004E0044002000005D20002000200020002000200020002C002000500053005F0057004F004E005F0041004D004F0055004E0054002000200020002000200020003D0020004000700057006F006E0041006D006F0075006E007400200020002000200020000080F520002000200020002000200020002C002000500053005F0050004C0041005900450044005F0041004D004F0055004E0054002000200020003D002000430041005300450020005700480045004E00200028004000700050006C00610079006500640041006D006F0075006E00740020003E002000500053005F0050004C0041005900450044005F0041004D004F0055004E005400290020005400480045004E0020004000700050006C00610079006500640041006D006F0075006E007400200045004C00530045002000500053005F0050004C0041005900450044005F0041004D004F0055004E005400200045004E00440020000080F520002000200020002000200020002C002000500053005F0057004F004E005F0041004D004F0055004E0054002000200020002000200020003D002000430041005300450020005700480045004E00200028004000700057006F006E0041006D006F0075006E00740020002000200020003E002000500053005F0057004F004E005F0041004D004F0055004E005400290020002000200020005400480045004E0020004000700057006F006E0041006D006F0075006E007400200020002000200045004C00530045002000500053005F0057004F004E005F0041004D004F0055004E005400200020002000200045004E0044002000004720002000200020002000200020002C002000500053005F004C004F0043004B004500440020002000200020002000200020002000200020003D0020004E0055004C004C002000005120002000200020002000200020002C002000500053005F00460049004E0049005300480045004400200020002000200020002000200020003D0020004700450054004400410054004500280029002000007D4F005500540050005500540020002000200049004E005300450052005400450044002E00500053005F0050004C0041005900450044005F0043004F0055004E005400200020002D002000440045004C0045005400450044002E00500053005F0050004C0041005900450044005F0043004F0055004E00540020002000017D20002000200020002000200020002C00200049004E005300450052005400450044002E00500053005F0050004C0041005900450044005F0041004D004F0055004E00540020002D002000440045004C0045005400450044002E00500053005F0050004C0041005900450044005F0041004D004F0055004E0054002000017D20002000200020002000200020002C00200049004E005300450052005400450044002E00500053005F0057004F004E005F0043004F0055004E005400200020002000200020002D002000440045004C0045005400450044002E00500053005F0057004F004E005F0043004F0055004E00540020002000200020002000017D20002000200020002000200020002C00200049004E005300450052005400450044002E00500053005F0057004F004E005F0041004D004F0055004E00540020002000200020002D002000440045004C0045005400450044002E00500053005F0057004F004E005F0041004D004F0055004E0054002000200020002000015D200057004800450052004500200020002000500053005F0050004C00410059005F00530045005300530049004F004E005F004900440020003D0020004000700050006C0061007900530065007300730069006F006E00490064002000001B4000700050006C00610079006500640043006F0075006E007400001D4000700050006C00610079006500640041006D006F0075006E00740000154000700057006F006E0043006F0075006E007400005155006E0065007800700065006300740065006400200050006C0061007900530065007300730069006F006E00490064002F005400720061006E00730061006300740069006F006E00490064003A00200000052C0020000037420061006C0061006E0063006500460072006F006D0047004D0020006300680061006E006700650064002000660072006F006D002000000B2C00200074006F0020000059530065007300730069006F006E0020004D006500740065007200730020006E006F0074002000720065006300650069007600650064002E0020005400720061006E00730061006300740069006F006E00490064003A002000001F2C00200048006100730043006F0075006E0074006500720073003A002000003355006E006500780070006500630074006500640020005400650072006D0069006E0061006C0054007900700065003A002000004D5400720078005F0055007000640061007400650050006C0061007900530065007300730069006F006E0050006C00610079006500640057006F006E0020006600610069006C00650064002E00002B430061006C00630075006C0061007400650050006C00610079006500640041006E00640057006F006E00003B5400720078005F00470065007400440065006C007400610050006C00610079006500640057006F006E0020006600610069006C00650064002E00003D5400720078005F0055007000640061007400650050006C00610079006500640041006E00640057006F006E0020006600610069006C00650064002E00000F53006100730048006F0073007400004D430061006C006C0069006E006700200049006E00730065007200740057006300700043006F006D006D0061006E0064003A0020005400650072006D0069006E0061006C00490064003A0020000021200050006C0061007900530065007300730069006F006E00490064003A002000004D5400720078005F0050006C0061007900530065007300730069006F006E00530065007400460069006E0061006C00420061006C0061006E006300650020006600610069006C00650064002E0000515400720078005F0055007000640061007400650046006F0075006E00640041006E006400520065006D00610069006E0069006E00670049006E00450067006D0020006600610069006C00650064002E0000495400720078005F0050006C0061007900530065007300730069006F006E005400490054004F005300650074004D006500740065007200730020006600610069006C00650064002E0000395400720078005F0050006C0061007900530065007300730069006F006E0043006C006F007300650020006600610069006C00650064002E00002D55005000440041005400450020002000200050004C00410059005F00530045005300530049004F004E0053000061200020002000530045005400200020002000500053005F00520045005F0046004F0055004E0044005F0049004E005F00450047004D00200020002000200020003D002000400070005200450046006F0075006E00640049006E00450067006D00006320002000200020002000200020002C002000500053005F004E0052005F0046004F0055004E0044005F0049004E005F00450047004D00200020002000200020003D002000400070004E00520046006F0075006E00640049006E00450067006D002000006B20002000200020002000200020002C002000500053005F00520045005F00520045004D00410049004E0049004E0047005F0049004E005F00450047004D0020003D0020004000700052004500520065006D00610069006E0069006E00670049006E00450067006D002000006B20002000200020002000200020002C002000500053005F004E0052005F00520045004D00410049004E0049004E0047005F0049004E005F00450047004D0020003D002000400070004E005200520065006D00610069006E0069006E00670049006E00450067006D0020000065200057004800450052004500200020002000500053005F0050004C00410059005F00530045005300530049004F004E005F0049004400200020002000200020003D0020004000700050006C0061007900530065007300730069006F006E00490044002000001F4000700050006C0061007900530065007300730069006F006E0049004400001D400070005200450046006F0075006E00640049006E00450067006D00001D400070004E00520046006F0075006E00640049006E00450067006D0000254000700052004500520065006D00610069006E0069006E00670049006E00450067006D000025400070004E005200520065006D00610069006E0069006E00670049006E00450067006D00006F2000530045004C00450043005400200020002000430041005300450020005700480045004E002000410043005F00540059005000450020003D002000400070004100630063006F0075006E0074005600690072007400750061006C005400650072006D0069006E0061006C002000006F200020002000200020002000200020002000200020002000200020002000200020005400480045004E002000300020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000006F2000200020002000200020002000200020002000200020002000200020002000200045004C00530045002000490053004E0055004C004C002800410043005F0048004F004C004400450052005F004C004500560045004C002C002000300029002000200020002000200020002000006F200020002000200020002000200020002000200045004E004400200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000006F200020002000460052004F004D002000200020004100430043004F0055004E00540053002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000006F2000200057004800450052004500200020002000410043005F004100430043004F0055004E0054005F004900440020003D002000400070004100630063006F0075006E007400490064002000200020002000200020002000200020002000200020002000200020002000200020000031400070004100630063006F0075006E0074005600690072007400750061006C005400650072006D0069006E0061006C0000809F530045004C004500430054002000540045005F005600490052005400550041004C005F004100430043004F0055004E0054005F00490044002000460052004F004D0020005400450052004D0049004E0041004C0053002000570048004500520045002000540045005F005400450052004D0049004E0041004C005F004900440020003D002000400070005400650072006D0069006E0061006C0049006400007F5400720078005F0050006C0061007900530065007300730069006F006E0043006C006F00730065003A0020004500720072006F00720020007700680065006E00200067006500740020005600690072007400750061006C004100630063006F0075006E00740049006400200020006600610069006C00650064002E00200000375400720078005F004700650074005400650072006D0069006E0061006C0049006E0066006F0020006600610069006C00650064002E0000415400720078005F005500700064006100740065004100630063006F0075006E007400420061006C0061006E006300650020006600610069006C00650064002E0000455400720078005F005500700064006100740065004100630063006F0075006E00740049006E00530065007300730069006F006E0020006600610069006C00650064002E00004B5400720078005F0055006E006C0069006E006B00500072006F006D006F00740069006F006E00730049006E00530065007300730069006F006E0020006600610069006C00650064002E00003355006E006500780070006500630074006500640020004D006F00760065006D0065006E00740054007900700065003A00200000415400720078005F0050006C0061007900530065007300730069006F006E005300650074004D006500740065007200730020006600610069006C00650064002E0000475400720078005F0050006C0061007900530065007300730069006F006E004300680061006E006700650053007400610074007500730020006600610069006C00650064002E00004F5400720078005F0049006E0073006500720074004100670072006F0075007000470061006D00650050006C0061007900530065007300730069006F006E0020006600610069006C00650064002E0000435400720078005F0055006E006C0069006E006B004100630063006F0075006E0074005400650072006D0069006E0061006C0020006600610069006C00650064002E00003B5400720078005F00550070006400610074006500500073004100630063006F0075006E0074004900640020006600610069006C00650064002E00004B5400720078005F0052006500730065007400430061006E00630065006C006C00610062006C0065004F007000650072006100740069006F006E0020006600610069006C00650064002E0000475400720078005F0053006900740065004A00610063006B0070006F00740043006F006E0074007200690062007500740069006F006E0020006600610069006C00650064002E0000475400720078005F005400490054004F005F00430061006C00630075006C00610074006500500072006F006D006F0043006F007300740020006600610069006C00650064002E0000415400720078005F00500072006F006D006F004D00610072006B004100730045007800680061007500730074006500640020006600610069006C00650064002E0000354100630063006F0075006E0074004D006F00760065006D0065006E00740073005400610062006C0065002E0041006400640028000013290020006600610069006C00650064002E0000415400720078005F0050006C0061007900530065007300730069006F006E0054006F00500065006E00640069006E00670020006600610069006C00650064002E00006F2000200049004E005300450052005400200049004E0054004F002000500045004E00440049004E0047005F0050004C00410059005F00530045005300530049004F004E0053005F0054004F005F0050004C0041005900450052005F0054005200410043004B0049004E0047002000006F2000200028002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000006F20002000200020002000200020005000500053005F00530045005300530049004F004E005F00490044002C00200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000006F20002000200020002000200020005000500053005F004100430043004F0055004E0054005F00490044002C00200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000006F20002000200020002000200020005000500053005F0043004F0049004E005F0049004E002C00200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000006F20002000200020002000200020005000500053005F005400450052004D0049004E0041004C005F00490044002C0020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000006F20002000200020002000200020005000500053005F004400550052004100540049004F004E005F0054004F00540041004C005F004D0049004E0055005400450053002C00200020002000200020002000200020002000200020002000200020002000200020002000200020002000006F20002000200020002000200020005000500053005F0050004100520041004D0053005F004E0055004D0050004C0041005900450044002C00200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000006F20002000200020002000200020005000500053005F0050004100520041004D0053005F00420041004C0041004E00430045005F004D00490053004D0041005400430048002C0020002000200020002000200020002000200020002000200020002000200020002000200020002000006F20002000200020002000200020005000500053005F005400450052004D0049004E0041004C005F0054005900500045002C00200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000006F20002000200020002000200020005000500053005F004400410054004500540049004D004500430052004500410054004500440020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000006F2000200029002000560041004C00550045005300200028002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000006F200020002000200020002000200040007000700073005F00730065007300730069006F006E005F00690064002C0020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000006F200020002000200020002000200040007000700073005F006100630063006F0075006E0074005F00690064002C0020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000006F200020002000200020002000200040007000700073005F0063006F0069006E005F0069006E002C0020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000006F200020002000200020002000200040007000700073005F007400650072006D0069006E0061006C005F00690064002C002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000006F200020002000200020002000200040007000700073005F006400750072006100740069006F006E005F0074006F00740061006C005F006D0069006E0075007400650073002C0020002000200020002000200020002000200020002000200020002000200020002000200020002000006F200020002000200020002000200040007000700073005F0070006100720061006D0073005F006E0075006D0070006C0061007900650064002C0020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000006F200020002000200020002000200040007000700073005F0070006100720061006D0073005F00620061006C0061006E00630065005F006D00690073006D0061007400630068002C002000200020002000200020002000200020002000200020002000200020002000200020002000006F200020002000200020002000200040007000700073005F007400650072006D0069006E0061006C005F0074007900700065002C0020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000006F2000200020002000200020002000670065007400640061007400650028002900200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000006F2000200029002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000001F40007000700073005F00730065007300730069006F006E005F0069006400001F40007000700073005F006100630063006F0075006E0074005F0069006400001940007000700073005F0063006F0069006E005F0069006E00002140007000700073005F007400650072006D0069006E0061006C005F0069006400003740007000700073005F006400750072006100740069006F006E005F0074006F00740061006C005F006D0069006E007500740065007300002B40007000700073005F0070006100720061006D0073005F006E0075006D0070006C006100790065006400003940007000700073005F0070006100720061006D0073005F00620061006C0061006E00630065005F006D00690073006D006100740063006800002540007000700073005F007400650072006D0069006E0061006C005F007400790070006500004120002000550050004400410054004500200053004900540045005F004A00410043004B0050004F0054005F0050004100520041004D00450054004500520053000080F720002000200020002000530045005400200053004A0050005F0050004C004100590045004400200020003D00200053004A0050005F0050004C00410059004500440020002B002000430041005300450020005700480045004E002000280053004A0050005F004F004E004C0059005F00520045004400450045004D00410042004C00450020003D0020003100290020005400480045004E0020004000700054006F00740061006C00520065006400650065006D00610062006C00650050006C006100790065006400200045004C005300450020004000700054006F00740061006C0050006C006100790065006400200045004E004400003120002000200057004800450052004500200053004A0050005F0045004E00410042004C004500440020003D0020003100002F4000700054006F00740061006C00520065006400650065006D00610062006C00650050006C006100790065006400001B4000700054006F00740061006C0050006C0061007900650064000031200055005000440041005400450020002000200050004C00410059005F00530045005300530049004F004E00530020000080812000200020002000530045005400200020002000500053005F004E004F004E005F00520045004400450045004D00410042004C0045005F0043004100530048005F0049004E0020002000200020003D002000400070004E006F006E00520065006400650065006D00610062006C006500430061007300680049006E00200020000073200020002000200020002000200020002C002000500053005F00520045004400450045004D00410042004C0045005F0043004100530048005F0049004E00200020002000200020002000200020003D0020004000700054006F00740061006C0052006500430061007300680049006E0020000069200020002000200020002000200020002C002000500053005F00520045005F0043004100530048005F0049004E0020002000200020002000200020002000200020002000200020002000200020003D0020004000700052006500430061007300680049006E0020000073200020002000200020002000200020002C002000500053005F00500052004F004D004F005F00520045005F0043004100530048005F0049004E0020002000200020002000200020002000200020003D00200040007000500072006F006D006F0052006500430061007300680049006E002000008081200020002000200020002000200020002C002000500053005F004E004F004E005F00520045004400450045004D00410042004C0045005F0043004100530048005F004F00550054002000200020003D002000400070004E006F006E00520065006400650065006D00610062006C00650043006100730068004F007500740020000075200020002000200020002000200020002C002000500053005F00520045004400450045004D00410042004C0045005F0043004100530048005F004F005500540020002000200020002000200020003D0020004000700054006F00740061006C005200650043006100730068004F00750074002000006B200020002000200020002000200020002C002000500053005F00520045005F0043004100530048005F004F00550054002000200020002000200020002000200020002000200020002000200020003D002000400070005200650043006100730068004F007500740020000075200020002000200020002000200020002C002000500053005F00500052004F004D004F005F00520045005F0043004100530048005F004F00550054002000200020002000200020002000200020003D00200040007000500072006F006D006F005200650043006100730068004F00750074002000007F200020002000200020002000200020002C002000500053005F004E004F004E005F00520045004400450045004D00410042004C0045005F0050004C004100590045004400200020002000200020003D002000400070004E006F006E00520065006400650065006D00610062006C00650050006C00610079006500640020000079200020002000200020002000200020002C002000500053005F00520045004400450045004D00410042004C0045005F0050004C0041005900450044002000200020002000200020002000200020003D00200040007000520065006400650065006D00610062006C00650050006C0061007900650064002000006F200020002000200020002000200020002C002000500053005F0050004C0041005900450044005F0041004D004F0055004E00540020002000200020002000200020002000200020002000200020003D0020004000700054006F00740061006C0050006C00610079006500640020000079200020002000200020002000200020002C002000500053005F004E004F004E005F00520045004400450045004D00410042004C0045005F0057004F004E00200020002000200020002000200020003D002000400070004E006F006E00520065006400650065006D00610062006C00650057006F006E0020000073200020002000200020002000200020002C002000500053005F00520045004400450045004D00410042004C0045005F0057004F004E002000200020002000200020002000200020002000200020003D00200040007000520065006400650065006D00610062006C00650057006F006E0020000069200020002000200020002000200020002C002000500053005F0057004F004E005F0041004D004F0055004E00540020002000200020002000200020002000200020002000200020002000200020003D0020004000700054006F00740061006C0057006F006E00200000752000200057004800450052004500200020002000500053005F0050004C00410059005F00530045005300530049004F004E005F0049004400200020002000200020002000200020002000200020003D0020004000700050006C0061007900530065007300730069006F006E004900640020002000002B400070004E006F006E00520065006400650065006D00610062006C006500430061007300680049006E00001F4000700054006F00740061006C0052006500430061007300680049006E0000154000700052006500430061007300680049006E00001F40007000500072006F006D006F0052006500430061007300680049006E00002D400070004E006F006E00520065006400650065006D00610062006C00650043006100730068004F007500740000214000700054006F00740061006C005200650043006100730068004F00750074000017400070005200650043006100730068004F0075007400002140007000500072006F006D006F005200650043006100730068004F0075007400002B400070004E006F006E00520065006400650065006D00610062006C00650050006C006100790065006400002540007000520065006400650065006D00610062006C00650050006C0061007900650064000025400070004E006F006E00520065006400650065006D00610062006C00650057006F006E00001F40007000520065006400650065006D00610062006C00650057006F006E0000154000700054006F00740061006C0057006F006E00006B2000200020002000530045005400200020002000500053005F00500052004F004D004F005F004E0052005F005400490043004B00450054005F0049004E00200020003D00200040007000500072006F006D006F004E0072005400690063006B006500740049006E002000006B200020002000200020002000200020002C002000500053005F00500052004F004D004F005F00520045005F005400490043004B00450054005F0049004E00200020003D00200040007000500072006F006D006F00520065005400690063006B006500740049006E0020000061200020002000200020002000200020002C002000500053005F00520045005F005400490043004B00450054005F0049004E00200020002000200020002000200020003D00200040007000520065005400690063006B006500740049006E002000006D200020002000200020002000200020002C002000500053005F00500052004F004D004F005F004E0052005F005400490043004B00450054005F004F005500540020003D00200040007000500072006F006D006F004E0072005400690063006B00650074004F007500740020000063200020002000200020002000200020002C002000500053005F00520045005F005400490043004B00450054005F004F005500540020002000200020002000200020003D00200040007000520065005400690063006B00650074004F007500740020000065200020002000200020002000200020002C002000500053005F0043004100530048005F0049004E0020002000200020002000200020002000200020002000200020003D00200040007000430061007300680049006E0041006D006F0075006E007400200000692000200057004800450052004500200020002000500053005F0050004C00410059005F00530045005300530049004F004E005F0049004400200020002000200020003D0020004000700050006C0061007900530065007300730069006F006E004900640020002000002340007000500072006F006D006F004E0072005400690063006B006500740049006E00002340007000500072006F006D006F00520065005400690063006B006500740049006E00001940007000520065005400690063006B006500740049006E00002540007000500072006F006D006F004E0072005400690063006B00650074004F0075007400001B40007000520065005400690063006B00650074004F0075007400001D40007000430061007300680049006E0041006D006F0075006E007400002720005500500044004100540045002000200020004100430043004F0055004E00540053002000005B2000200020002000530045005400200020002000410043005F00430041004E00430045004C004C00410042004C0045005F004F005000450052004100540049004F004E005F004900440020003D0020004E0055004C004C002000006D2000200057004800450052004500200020002000410043005F004100430043004F0055004E0054005F00490044002000200020002000200020002000200020002000200020002000200020003D002000400070004100630063006F0075006E007400490064002000200020000080834400450043004C00410052004500200040004F00750074007000750074005400610062006C00650020005400410042004C004500200028002000410043005F0049004E005F00530045005300530049004F004E005F0050004C004100590045004400200020002000200020002000200020002000200020004D004F004E00450059000080832000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F0057004F004E00200020002000200020002000200020002000200020002000200020004D004F004E00450059000080832000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F00420041004C0041004E004300450020002000200020002000200020004D004F004E00450059000080832000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F00520045005F00420041004C0041004E004300450020004D004F004E00450059000080872000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F00420041004C0041004E004300450020004D004F004E004500590029003B00004F2000200020002000530045005400200020002000410043005F0049004E005F00530045005300530049004F004E005F0050004C004100590045004400200020002000200020003D00200030002000004F200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F0057004F004E00200020002000200020002000200020003D00200030002000004F200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F0050004C004100590045004400200020003D00200030002000004F200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F004E0052005F0050004C004100590045004400200020003D00200030002000004F200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F0057004F004E00200020002000200020003D00200030002000004F200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F004E0052005F0057004F004E00200020002000200020003D00200030002000005B200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F00420041004C0041004E004300450020002000200020002000200020003D00200030002000005B200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F00520045005F00420041004C0041004E004300450020003D00200030002000005B200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F00420041004C0041004E004300450020003D00200030002000005B200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F0054004F005F0047004D002000200020002000200020002000200020003D00200030002000005B200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F00520045005F0054004F005F0047004D002000200020003D00200030002000005B200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F0054004F005F0047004D002000200020003D00200030002000005B200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F00460052004F004D005F0047004D0020002000200020002000200020003D00200030002000005B200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F00520045005F00460052004F004D005F0047004D0020003D00200030002000005B200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F00460052004F004D005F0047004D0020003D00200030002000006F200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00430041004E00430045004C004C00410042004C0045005F005400520041004E00530041004300540049004F004E005F004900440020003D00200030002000006F200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F00430041004E00430045004C004C00410042004C00450020002000200020002000200020002000200020002000200020003D00200030002000006F200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F00520045005F00430041004E00430045004C004C00410042004C00450020002000200020002000200020003D00200030002000006F200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F00430041004E00430045004C004C00410042004C00450020002000200020002000200020003D00200030002000004F20004F0055005400500055005400200020002000440045004C0045005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F0050004C00410059004500440020000049200020002000200020002000200020002C002000440045004C0045005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F0057004F004E0020000057200020002000200020002000200020002C002000440045004C0045005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F00520045005F00420041004C0041004E004300450020000063200020002000200020002000200020002C002000440045004C0045005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F00520045005F00420041004C0041004E004300450020000063200020002000200020002000200020002C002000440045004C0045005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F00420041004C0041004E00430045002000002F20002000200049004E0054004F0020002000200040004F00750074007000750074005400610062006C0065002000006F200020002000200041004E004400200020002000410043005F00430055005200520045004E0054005F0050004C00410059005F00530045005300530049004F004E005F004900440020003D0020004000700050006C0061007900530065007300730069006F006E00490064002000002B490046002000280020004000400052004F00570043004F0055004E00540020003D002000310020002900003B200020002000530045004C0045004300540020002A002000460052004F004D00200040004F00750074007000750074005400610062006C006500008081200020002000460052004F004D002000200020004100430043004F0055004E0054005F00500052004F004D004F00540049004F004E00530020005700490054004800280049004E004400450058002800490058005F006100630070005F006100630063006F0075006E0074005F00730074006100740075007300290029002000005B20002000570048004500520045002000200020004100430050005F004100430043004F0055004E0054005F004900440020002000200020002000200020003D002000400070004100630063006F0075006E0074004900640020000061200020002000200041004E0044002000200020004100430050005F0050004C00410059005F00530045005300530049004F004E005F004900440020003D0020004000700050006C0061007900530065007300730069006F006E004900640020000061200020002000200041004E0044002000200020004100430050005F005400520041004E00530041004300540049004F004E005F0049004400200020003D002000400070005400720061006E00730061006300740069006F006E004900640020000055200020002000200041004E0044002000200020004100430050005F0050004C00410059005F00530045005300530049004F004E005F004900440020004900530020004E004F00540020004E0055004C004C002000001F400070005400720061006E00730061006300740069006F006E0049006400004B20002000200020005300450054002000200020004100430050005F0050004C00410059005F00530045005300530049004F004E005F004900440020003D0020004E0055004C004C002000004B200020002000200020002000200020002C0020004100430050005F005400520041004E00530041004300540049004F004E005F0049004400200020003D0020004E0055004C004C002000005720002000570048004500520045002000200020004100430050005F0055004E0049005100550045005F004900440020002000200020002000200020003D0020004000700055006E0069007100750065004900640020000059200020002000200041004E0044002000200020004100430050005F004100430043004F0055004E0054005F00490044002000200020002000200020003D002000400070004100630063006F0075006E007400490064002000005F200020002000200041004E0044002000200020004100430050005F0053005400410054005500530020002000200020002000200020002000200020003D002000400070005300740061007400750073004100630074006900760065002000005120002000570048004500520045002000200020004100430050005F004100430043004F0055004E0054005F0049004400200020003D002000400070004100630063006F0075006E0074004900640020000057200020002000200041004E0044002000200020004100430050005F005300540041005400550053002000200020002000200020003D002000400070005300740061007400750073004100630074006900760065002000003D200020002000200041004E0044002000200020004100430050005F00570049005400480048004F004C00440020002000200020003E002000300020000080D120002000200020005300450054002000200020004100430050005F00570049005400480048004F004C00440020002000200020003D002000430041005300450020005700480045004E0020004100430050005F00420041004C0041004E004300450020003C0020004100430050005F00570049005400480048004F004C00440020005400480045004E0020004100430050005F00420041004C0041004E0043004500200045004C005300450020004100430050005F00570049005400480048004F004C004400200045004E0044002000004F20002000570048004500520045002000200020004100430050005F0055004E0049005100550045005F00490044002000200020003D0020004000700055006E0069007100750065004900640020000051200020002000200041004E0044002000200020004100430050005F004100430043004F0055004E0054005F0049004400200020003D002000400070004100630063006F0075006E0074004900640020000013700055006E00690071007500650049006400008081200020002000200041004E0044002000200020004100430050005F004300520045004400490054005F005400590050004500200049004E00200028004000700043007200650064006900740054007900700065004E00520031002C0020004000700043007200650064006900740054007900700065004E005200320029002000002340007000530074006100740075007300450078006800610075007300740065006400005B20002000200020005300450054002000200020004100430050005F005300540041005400550053002000200020002000200020003D00200040007000530074006100740075007300450078006800610075007300740065006400003D200020002000200041004E0044002000200020004100430050005F00420041004C0041004E0043004500200020002000200020003D00200030002000004F200020002000530045004C0045004300540020002000200054004F005000200031002000540049005F004100430043004F0055004E0054005F00500052004F004D004F00540049004F004E002000002920002000200020002000460052004F004D002000200020005400490043004B0045005400530020000075200020002000200057004800450052004500200020002000540049005F00430041004E00430045004C00450044005F0050004C00410059005F00530045005300530049004F004E005F004900440020003D0020004000700050006C0061007900530065007300730069006F006E00490064002000006F20002000200020002000200041004E004400200020002000540049005F0054005900500045005F00490044002000200020002000200020002000200020002000200020002000200020002000200020003D002000400070005400690063006B006500740054007900700065002000005320004F005200440045005200200042005900200020002000540049005F004C004100530054005F0041004300540049004F004E005F004400410054004500540049004D0045002000440045005300430020000019400070005400690063006B00650074005400790070006500003D200020005500500044004100540045002000200020004100430043004F0055004E0054005F00500052004F004D004F00540049004F004E0053002000008093200020002000200020005300450054002000200020004100430050005F00520045004400450045004D00410042004C0045005F0043004F005300540020003D002000430041005300450020005700480045004E002000200020004100430050005F00520045004400450045004D00410042004C0045005F0043004F005300540020004900530020004E0055004C004C002000006D200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020005400480045004E002000200020004000700041006D006F0075006E00740020000080A12000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200045004C00530045002000200020004100430050005F00520045004400450045004D00410042004C0045005F0043004F005300540020002B0020004000700041006D006F0075006E007400200045004E00440020000061200020002000570048004500520045002000200020004100430050005F0055004E0049005100550045005F004900440020003D002000400070004100630063006F0075006E007400500072006F006D006F00740069006F006E004900640020000029400070004100630063006F0075006E007400500072006F006D006F00740069006F006E00490064000080A520004400450043004C00410052004500200040004D006F006400650052006500730065007200760065006400200041005300200049004E005400200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000080A520002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000080A52000530045005400200040004D006F00640065005200650073006500720076006500640020003D0020002800530045004C004500430054002000490053004E0055004C004C0028002800530045004C00450043005400200020002000430041005300540020002800470050005F004B00450059005F00560041004C0055004500200041005300200049004E005400290020002000200020002000200020002000200020000080A5200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000460052004F004D00200020002000470045004E004500520041004C005F0050004100520041004D00530020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000080A52000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200057004800450052004500200020002000470050005F00470052004F00550050005F004B00450059002000200020003D0020002700470061006D006500470061007400650077006100790027002000200020002000200020000180A5200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200041004E004400200020002000470050005F005300550042004A004500430054005F004B004500590020003D002000270045006E00610062006C0065006400270020002000200020002000200020002000200020000180A5200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200041004E004400200020002000490053004E0055004D0045005200490043002800470050005F004B00450059005F00560041004C0055004500290020003D0020003100200029002C002000300029002900200020000080A52000490046002000280040004D006F00640065005200650073006500720076006500640020003D00200031002900200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000080A5200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200041004E004400200020002000470050005F005300550042004A004500430054005F004B004500590020003D0020002700520065007300650072007600650064002E0045006E00610062006C0065006400270020000180A5200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200041004E0044002000200020002800490053004E0055004D0045005200490043002800470050005F004B00450059005F00560041004C0055004500290020003D0020003100200020002000200020002000200020000080A5200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020004F0052002000490053004E0055004D0045005200490043002800470050005F004B00450059005F00560041004C0055004500290020003D00200032002900200029002C0020003000290029000080A5200020002000530045005400200040004D006F00640065005200650073006500720076006500640020003D0020002800530045004C004500430054002000200043004100530054002800410043005F004D004F00440045005F0052004500530045005200560045004400200041005300200049004E00540029002000200020002000200020002000200020002000200020002000200020002000200020002000200020000080A520002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000460052004F004D002000200020004100430043004F0055004E0054005300200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000080A5200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200057004800450052004500200020002000410043005F004100430043004F0055004E0054005F004900440020003D002000400070004100630063006F0075006E0074004900640029002000200020002000200020002000200020002000200020002000200020002000200020002000200020000080A52000530045004C00450043005400200040004D006F00640065005200650073006500720076006500640020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000023530045004C00450043005400200020002000500056005F004E0041004D0045002000003920002000200020002000200020002C002000500056005F004F004E004C0059005F00520045004400450045004D00410042004C0045002000007D20002000460052004F004D00200020002000500052004F00560049004400450052005300200049004E004E004500520020004A004F0049004E0020005400450052004D0049004E0041004C00530020004F004E002000540045005F00500052004F0056005F004900440020003D002000500056005F00490044002000004F200057004800450052004500200020002000540045005F005400450052004D0049004E0041004C005F004900440020003D002000400070005400650072006D0069006E0061006C00490064002000001B4100430050005F0055004E0049005100550045005F0049004400001F4100430050005F004300520045004400490054005F00540059005000450000174100430050005F00420041004C0041004E0043004500008089200020002000200020002000200020002C002000430041005300450020005700480045004E00200050004D005F0052004500530054005200490043005400450044005F0054004F005F005400450052004D0049004E0041004C005F004C0049005300540020004900530020004E004F00540020004E0055004C004C0020005400480045004E00200000815D20002000200020002000200020002000200020002000200020002000200020002000490053004E0055004C004C0028002800530045004C00450043005400200031002000460052004F004D0020005400450052004D0049004E0041004C005F00470052004F005500500053002000570048004500520045002000540047005F005400450052004D0049004E0041004C005F004900440020003D002000400070005400650072006D0069006E0061006C0049006400200041004E0044002000540047005F0045004C0045004D0045004E0054005F004900440020003D0020004100430050005F00500052004F004D004F005F0049004400200041004E0044002000540047005F0045004C0045004D0045004E0054005F00540059005000450020003D0020004000700045006C0065006D0065006E0074005400790070006500500072006F006D006F00740069006F006E0029002C002000300029002000003520002000200020002000200020002000200020002000200020002000200045004C005300450020003100200045004E0044002000002D200020002000200020002000200020002C0020004100430050005F00420041004C0041004E0043004500200000808120002000200049004E004E0045005200200020004A004F0049004E002000500052004F004D004F00540049004F004E00530020004F004E00200050004D005F00500052004F004D004F00540049004F004E005F004900440020003D0020004100430050005F00500052004F004D004F005F004900440020002000200020002000006D200020002000200041004E0044002000200020004100430050005F004300520045004400490054005F005400590050004500200049004E0020002000200028004000700054007900700065004E00520031002C0020004000700054007900700065004E005200320029002000008097200020002000200041004E004400200028002000280020004100430050005F00420041004C0041004E004300450020003E00200030002000290020004F0052002000280020004100430050005F00420041004C0041004E004300450020003D0020003000200041004E00440020004100430050005F00570049005400480048004F004C00440020003E00200030002000290029002000003B200020004F00520044004500520020004200590020004100430050005F0055004E0049005100550045005F00490044002000410053004300200000134000700054007900700065004E005200310000134000700054007900700065004E0052003200002D4000700045006C0065006D0065006E0074005400790070006500500072006F006D006F00740069006F006E0000395500500044004100540045002000200020004100430043004F0055004E0054005F00500052004F004D004F00540049004F004E0053002000005F2000200020005300450054002000200020004100430050005F0050004C00410059005F00530045005300530049004F004E005F004900440020003D0020004000700050006C0061007900530065007300730069006F006E00490064002000005F20002000200020002000200020002C0020004100430050005F005400520041004E00530041004300540049004F004E005F0049004400200020003D002000400070005400720061006E00730061006300740069006F006E0049006400200000434F0055005400500055005400200020002000440045004C0045005400450044002E004100430050005F004300520045004400490054005F0054005900500045002000004320002000200020002000200020002C002000440045004C0045005400450044002E004100430050005F00420041004C0041004E004300450020002000200020002000007F20002000460052004F004D002000200020004100430043004F0055004E0054005F00500052004F004D004F00540049004F004E00530020005700490054004800280049004E00440045005800280050004B005F006100630063006F0075006E0074005F00700072006F006D006F00740069006F006E00730029002900200000552000570048004500520045002000200020004100430050005F0055004E0049005100550045005F004900440020002000200020002000200020003D0020004000700055006E006900710075006500490064002000005720002000200041004E0044002000200020004100430050005F004100430043004F0055004E0054005F00490044002000200020002000200020003D002000400070004100630063006F0075006E007400490064002000005D20002000200041004E0044002000200020004100430050005F0053005400410054005500530020002000200020002000200020002000200020003D002000400070005300740061007400750073004100630074006900760065002000006B20002000200041004E0044002000200020004100430050005F004300520045004400490054005F005400590050004500200049004E0020002000200028004000700054007900700065004E00520031002C0020004000700054007900700065004E00520032002900200000809520002000200041004E004400200028002000280020004100430050005F00420041004C0041004E004300450020003E00200030002000290020004F0052002000280020004100430050005F00420041004C0041004E004300450020003D0020003000200041004E00440020004100430050005F00570049005400480048004F004C00440020003E00200030002000290029002000004B20002000200041004E0044002000200020004100430050005F0050004C00410059005F00530045005300530049004F004E005F004900440020004900530020004E0055004C004C00200000815D530045004C004500430054002000490053004E0055004C004C0020002800200028002000530045004C004500430054002000430041005300540020002800470050005F004B00450059005F00560041004C0055004500200041005300200049004E00540029002000460052004F004D002000470045004E004500520041004C005F0050004100520041004D0053002000570048004500520045002000470050005F00470052004F00550050005F004B004500590020003D002000270043007200650064006900740073002700200041004E0044002000470050005F005300550042004A004500430054005F004B004500590020003D002000270050006C00610079004D006F00640065002700200041004E0044002000490053004E0055004D0045005200490043002800470050005F004B00450059005F00560041004C0055004500290020003D0020003100200029002C0020003000200029002000013D5400720078005F00470065007400430072006500640069007400730050006C00610079004D006F006400650020006600610069006C00650064002E000031200020002000530045004C004500430054002000200020004100430050005F00420041004C0041004E00430045002000003320002000200020002000200020002000200020002C0020004100430050005F00570049005400480048004F004C0044002000003120002000200020002000200020002000200020002C0020004100430050005F0057004F004E004C004F0043004B002000002F20002000200020002000200020002000200020002C0020004100430050005F0050004C0041005900450044002000002920002000200020002000200020002000200020002C0020004100430050005F0057004F004E002000003920002000200020002000200020002000200020002C0020004100430050005F004300520045004400490054005F0054005900500045002000003520002000200020002000200020002000200020002C0020004100430050005F0055004E0049005100550045005F0049004400200000808720002000200020002000460052004F004D002000200020004100430043004F0055004E0054005F00500052004F004D004F00540049004F004E005300200057004900540048002000280049004E004400450058002800490058005F006100630070005F006100630063006F0075006E0074005F00730074006100740075007300290029002000005D2000200020002000570048004500520045002000200020004100430050005F004100430043004F0055004E0054005F00490044002000200020002000200020003D002000400070004100630063006F0075006E007400490064002000006320002000200020002000200041004E0044002000200020004100430050005F0053005400410054005500530020002000200020002000200020002000200020003D002000400070005300740061007400750073004100630074006900760065002000006520002000200020002000200041004E0044002000200020004100430050005F0050004C00410059005F00530045005300530049004F004E005F004900440020003D0020004000700050006C0061007900530065007300730069006F006E00490064002000003D20004F0052004400450052002000420059002000200020004100430050005F0055004E0049005100550045005F004900440020004100530043002000003755006E006B006E006F0077006E00200043007200650064006900740050006C00610079004D006F00640065003A0020007B0030007D0000532000200020005300450054002000200020004100430050005F00420041004C0041004E00430045002000200020002000200020002000200020003D00200040007000420061006C0061006E00630065002000004B20002000200020002000200020002C0020004100430050005F0057004F004E0020002000200020002000200020002000200020002000200020003D0020004000700057006F006E002000005120002000200020002000200020002C0020004100430050005F0050004C00410059004500440020002000200020002000200020002000200020003D0020004000700050006C0061007900650064002000005F20002000200041004E0044002000200020004100430050005F0050004C00410059005F00530045005300530049004F004E005F004900440020003D0020004000700050006C0061007900530065007300730069006F006E00490064002000001340007000420061006C0061006E0063006500000B4000700057006F006E00000F4100430050005F0057004F004E0000114000700050006C00610079006500640000154100430050005F0050004C0041005900450044000067430061006E0027007400200075007000640061007400650020006100630063006F0075006E0074002000700072006F006D006F00740069006F006E0073002E002000550070006400610074006500640020007B0030007D0020006F00660020007B0031007D0001809B500072006F006D006F00740069006F006E00200055006E006900710075006500490064003A0020007B0030007D002C00200043007200650064006900740054007900700065003A0020007B0031007D002C002000420061006C0061006E00630065003A0020007B0032007D002C00200050006C0061007900650064003A0020007B0033007D002C00200057006F006E003A0020007B0034007D00000F4500720072006F0072003A0020000027510075006500720079002E0043006F006D006D0061006E00640054006500780074003A002000002750006100720061006D00650074006500720020007B0030007D0020003D0020007B0031007D0000754400450043004C00410052004500200040004F00750074007000750074005400610062006C00650020005400410042004C004500200028002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F0050004C00410059004500440020004D004F004E004500590000752000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F004E0052005F0050004C00410059004500440020004D004F004E004500590000752000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F0057004F004E0020002000200020004D004F004E0045005900007B2000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F004E0052005F0057004F004E0020002000200020004D004F004E0045005900200029003B0000808B2000200020002000530045005400200020002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F00420041004C0041004E004300450020002000200020002000200020003D0020004000700049006E00530065007300730069006F006E0052006500420061006C0061006E006300650020002000200020002000200000808B200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F00520045005F00420041004C0041004E004300450020003D0020004000700049006E00530065007300730069006F006E00500072006F006D006F0052006500420061006C0061006E0063006500200000808B200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F00420041004C0041004E004300450020003D0020004000700049006E00530065007300730069006F006E00500072006F006D006F004E007200420061006C0061006E006300650020000080B3200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F0050004C004100590045004400200020002000200020002000200020003D002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F0050004C00410059004500440020002B0020004000700049006E00530065007300730069006F006E005200650050006C00610079006500640020000080B3200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F004E0052005F0050004C004100590045004400200020002000200020002000200020003D002000410043005F0049004E005F00530045005300530049004F004E005F004E0052005F0050004C00410059004500440020002B0020004000700049006E00530065007300730069006F006E004E00720050006C00610079006500640020000080AD200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F0057004F004E00200020002000200020002000200020002000200020003D002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F0057004F004E0020002000200020002B0020004000700049006E00530065007300730069006F006E005200650057006F006E0020000080AD200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F004E0052005F0057004F004E00200020002000200020002000200020002000200020003D002000410043005F0049004E005F00530045005300530049004F004E005F004E0052005F0057004F004E0020002000200020002B0020004000700049006E00530065007300730069006F006E004E00720057006F006E0020000080AF200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F0050004C004100590045004400200020002000200020002000200020002000200020003D002000410043005F0049004E005F00530045005300530049004F004E005F0050004C00410059004500440020002000200020002D0020004000700049006E00530065007300730069006F006E0050006C00610079006500640020000180A9200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F0057004F004E00200020002000200020002000200020002000200020002000200020003D002000410043005F0049004E005F00530045005300530049004F004E005F0057004F004E0020002000200020002000200020002D0020004000700049006E00530065007300730069006F006E0057006F006E002000015920004F005500540050005500540020002000200049004E005300450052005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F00520045005F0050004C004100590045004400200020000059200020002000200020002000200020002C00200049004E005300450052005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F004E0052005F0050004C004100590045004400200020000059200020002000200020002000200020002C00200049004E005300450052005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F00520045005F0057004F004E00200020002000200020000059200020002000200020002000200020002C00200049004E005300450052005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F004E0052005F0057004F004E00200020002000200020000027200020002000460052004F004D002000200020004100430043004F0055004E0054005300200000294000700049006E00530065007300730069006F006E0052006500420061006C0061006E006300650000334000700049006E00530065007300730069006F006E00500072006F006D006F0052006500420061006C0061006E006300650000334000700049006E00530065007300730069006F006E00500072006F006D006F004E007200420061006C0061006E006300650000274000700049006E00530065007300730069006F006E005200650050006C00610079006500640000274000700049006E00530065007300730069006F006E004E00720050006C00610079006500640000214000700049006E00530065007300730069006F006E005200650057006F006E0000214000700049006E00530065007300730069006F006E004E00720057006F006E0000234000700049006E00530065007300730069006F006E0050006C006100790065006400001D4000700049006E00530065007300730069006F006E0057006F006E000055430061006E002700740020007500700064006100740065002000270069006E002000730065007300730069006F006E00270020006100630063006F0075006E0074002C002000490064003A0020007B0030007D000180834400450043004C00410052004500200040004F00750074007000750074005400610062006C00650020005400410042004C004500200028002000440045004C00540041005F0050004C00410059004500440020002000200020002000200020002000200020002000200020002000200020002000200020004D004F004E00450059000080832000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000440045004C00540041005F0057004F004E0020002000200020002000200020002000200020002000200020002000200020002000200020002000200020004D004F004E00450059000080832000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F00420041004C0041004E004300450020004D004F004E00450059000080892000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000410043005F004100430043004F0055004E0054005F004900440020002000200020002000200020002000200020002000200020002000200020002000200042004900470049004E00540029003B000081132000200020002000530045005400200020002000410043005F0049004E005F00530045005300530049004F004E005F0050004C00410059004500440020003D002000430041005300450020005700480045004E0020002800500053005F0050004C0041005900450044005F0041004D004F0055004E00540020003E002000410043005F0049004E005F00530045005300530049004F004E005F0050004C004100590045004400290020005400480045004E002000500053005F0050004C0041005900450044005F0041004D004F0055004E005400200045004C00530045002000410043005F0049004E005F00530045005300530049004F004E005F0050004C004100590045004400200045004E0044002000008113200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F0057004F004E0020002000200020003D002000430041005300450020005700480045004E0020002800500053005F0057004F004E005F0041004D004F0055004E00540020002000200020003E002000410043005F0049004E005F00530045005300530049004F004E005F0057004F004E00290020002000200020005400480045004E002000500053005F0057004F004E005F0041004D004F0055004E005400200020002000200045004C00530045002000410043005F0049004E005F00530045005300530049004F004E005F0057004F004E00200020002000200045004E00440020000080AF20004F005500540050005500540020002000200049004E005300450052005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F0050004C00410059004500440020002D002000440045004C0045005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F0050004C0041005900450044002000410053002000440045004C00540041005F0050004C00410059004500440020000180AF200020002000200020002000200020002C00200049004E005300450052005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F0057004F004E0020002000200020002D002000440045004C0045005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F0057004F004E002000200020002000410053002000440045004C00540041005F0057004F004E0020002000200020000141200020002000200020002000200020002C002000440045004C0045005400450044002E00410043005F004100430043004F0055004E0054005F004900440020000080C3200020002000460052004F004D0020002000200050004C00410059005F00530045005300530049004F004E005300200041005300200050005300200049004E004E004500520020004A004F0049004E0020004100430043004F0055004E0054005300200041005300200041004300430020004F004E002000500053002E00500053005F004100430043004F0055004E0054005F004900440020003D0020004100430043002E00410043005F004100430043004F0055004E0054005F0049004400200000652000200057004800450052004500200020002000500053002E00500053005F0050004C00410059005F00530045005300530049004F004E005F004900440020003D0020004000700050006C0061007900530065007300730069006F006E0049006400200000474400450043004C00410052004500200040004100630063006F0075006E007400490064005F0074006F00550070006400610074006500200042004900470049004E005400200000634400450043004C00410052004500200040004F00750074007000750074005400610062006C00650020005400410042004C004500200028002000410043005F004100430043004F0055004E0054005F0049004400200042004900470049004E0054000047200020002000200020002000200020002C002000410043005F0042004C004F0043004B00450044002000200020002000200020002000200020002000200020004200490054000047200020002000200020002000200020002C002000410043005F0042004C004F0043004B005F0052004500410053004F004E00200020002000200020002000200049004E0054000047200020002000200020002000200020002C002000410043005F0048004F004C004400450052005F004C004500560045004C00200020002000200020002000200049004E0054000047200020002000200020002000200020002C002000410043005F0048004F004C004400450052005F00470045004E0044004500520020002000200020002000200049004E005400005B200020002000200020002000200020002C002000410043005F0048004F004C004400450052005F004E0041004D004500200020002000200020002000200020004E00560041005200430048004100520028003200300030002900004B200020002000200020002000200020002C002000410043005F00520045005F00420041004C0041004E00430045002000200020002000200020002000200020004D004F004E0045005900004B200020002000200020002000200020002C002000410043005F00500052004F004D004F005F00520045005F00420041004C0041004E00430045002000200020004D004F004E0045005900004B200020002000200020002000200020002C002000410043005F00500052004F004D004F005F004E0052005F00420041004C0041004E00430045002000200020004D004F004E0045005900004B200020002000200020002000200020002C0020004300420055005F00560041004C005500450020002000200020002000200020002000200020002000200020004D004F004E0045005900005D200020002000200020002000200020002C002000410043005F00430055005200520045004E0054005F0050004C00410059005F00530045005300530049004F004E005F0049004400200020002000200042004900470049004E0054000057200020002000200020002000200020002C002000410043005F00430055005200520045004E0054005F005400450052004D0049004E0041004C005F00490044002000200020002000200020002000200049004E0054000079200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00430041004E00430045004C004C00410042004C0045005F005400520041004E00530041004300540049004F004E005F0049004400200020002000200042004900470049004E0054000077200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F00430041004E00430045004C004C00410042004C00450020002000200020002000200020002000200020002000200020002000200020004D004F004E00450059000077200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F00520045005F00430041004E00430045004C004C00410042004C00450020002000200020002000200020002000200020004D004F004E00450059000077200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F00430041004E00430045004C004C00410042004C00450020002000200020002000200020002000200020004D004F004E00450059000063200020002000200020002000200020002C002000410043005F00430041004E00430045004C004C00410042004C0045005F004F005000450052004100540049004F004E005F004900440020002000200042004900470049004E005400200020002000005F200020002000200020002000200020002C002000410043005F00520045005F005200450053004500520056004500440020002000200020002000200020004D004F004E004500590020002000200020002000200020002000200020002000005F200020002000200020002000200020002C002000410043005F004D004F00440045005F0052004500530045005200560045004400200020002000200020004200490054002000200020002000200020002000200020002000200020002000005F200020002000200020002000200020002C002000420055005F004E0052005F004300520045004400490054002000200020002000200020002000200020004D004F004E004500590020002000200020002000200020002000200020002000005F200020002000200020002000200020002C002000420055005F00520045005F004300520045004400490054002000200020002000200020002000200020004D004F004E004500590020002000200020002000200020002000200020002000005B200020002000200020002000200020002C002000410043005F0054005900500045002000200020002000200020002000200020002000200020002000200049004E00540029003B00200020002000200020002000200020002000003D530045005400200040004100630063006F0075006E007400490064005F0074006F0055007000640061007400650020003D0020004E0055004C004C000055530045004C00450043005400200040004100630063006F0075006E007400490064005F0074006F0055007000640061007400650020003D002000410043005F004100430043004F0055004E0054005F0049004400001B460052004F004D0020004100430043004F0055004E00540053000080C74C0045004600540020004A004F0049004E00200043005500530054004F004D00450052005F004200550043004B004500540020004F004E0020002800410043005F004100430043004F0055004E0054005F004900440020003D0020004300420055005F0043005500530054004F004D00450052005F00490044002900200041004E004400200028004300420055005F004200550043004B00450054005F004900440020003D002000400070004200750063006B0065007400490064005F00500054002900200000512000200057004800450052004500200020002000410043005F004100430043004F0055004E0054005F00490044002000200020003D002000400070004100630063006F0075006E00740049006400200000808F200020005700480045005200450020002000280020002000200028002000410043005F005400590050004500200020003D00200020003300200041004E0044002000410043005F0054005200410043004B005F0044004100540041002000200020003D0020004000700041006C00650073006900730054007200610063006B0044006100740061002000290020000080C120002000200020002000200020002000200020004F005200200028002000410043005F00540059005000450020003C003E00200020003300200041004E0044002000410043005F0054005200410043004B005F0044004100540041002000200020003D002000640062006F002E0054007200610063006B00440061007400610054006F0049006E007400650072006E0061006C002000280040007000570069006E0054007200610063006B0044006100740061002900200029002000290020000073200020002000200041004E0044002000200020002800410043005F00520045005F00420041004C0041004E0043004500200020002000200020002000200020002B0020004000700052006500420061006C0061006E006300650020002000200020002000290020003E003D002000300020000073200020002000200041004E0044002000200020002800410043005F00500052004F004D004F005F00520045005F00420041004C0041004E0043004500200020002B00200040007000500072006F006D006F0052006500420061006C0061006E0063006500290020003E003D002000300020000073200020002000200041004E0044002000200020002800410043005F00500052004F004D004F005F004E0052005F00420041004C0041004E0043004500200020002B00200040007000500072006F006D006F004E007200420061006C0061006E0063006500290020003E003D002000300020000073200020002000200041004E0044002000200020002800490053004E0055004C004C0028004300420055005F00560041004C00550045002C0020003000290020002B0020004000700050006F0069006E007400730020002000200020002000200020002000290020003E003D002000300020000077200020002000200055005000440041005400450020002000200043005500530054004F004D00450052005F004200550043004B00450054002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000007720002000200020002000200020005300450054002000200020004300420055005F00560041004C005500450020003D002000490053004E0055004C004C0028004300420055005F00560041004C00550045002C0020003000290020002B0020004000700050006F0069006E0074007300200020002000007B20002000200020002000570048004500520045002000200020004300420055005F0043005500530054004F004D00450052005F004900440020003D002000490053004E0055004C004C00280040004100630063006F0075006E007400490064005F0074006F005500700064006100740065002C002000300029000057200020002000200020002000200041004E0044002000200020004300420055005F004200550043004B00450054005F004900440020003D002000400070004200750063006B0065007400490064005F00500054002000007520004900460020004000400052004F00570043004F0055004E00540020003D0020003000200041004E0044002000490053004E0055004C004C00280040004100630063006F0075006E007400490064005F0074006F005500700064006100740065002C0020003000290020003C003E00200030000061200042004500470049004E00200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000006120002000200049004E005300450052005400200049004E0054004F00200043005500530054004F004D00450052005F004200550043004B0045005400200028004300420055005F0043005500530054004F004D00450052005F0049004400200000612000200020002000200020002000200020002000200020002000200020002000200020002C0020004300420055005F004200550043004B00450054005F004900440020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002000200020002000200020002C0020004300420055005F00560041004C0055004500200020002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002000200020002000200020002C0020004300420055005F005500500044004100540045004400200020002000200020002000200020002000200020002000200020002000200020000061200020002000200020002000200020002000200020002000200020002000200020002000290020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000006120002000200020002000200020002000200020002000200020002000560041004C005500450053002000280040004100630063006F0075006E007400490064005F0074006F00550070006400610074006500200020002000200020002000200000612000200020002000200020002000200020002000200020002000200020002000200020002C002000400070004200750063006B0065007400490064005F005000540020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002000200020002000200020002C0020004000700050006F0069006E00740073002000200020002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002000200020002000200020002C00200047004500540044004100540045002800290020002000200020002000200020002000200020002000200020002000200020002000200020000061200045004E0044002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000811F2000200020002000530045005400200020002000410043005F00420041004C0041004E0043004500200020002000200020002000200020002000200020003D002000410043005F00520045005F00420041004C0041004E004300450020002B002000410043005F00500052004F004D004F005F00520045005F00420041004C0041004E004300450020002B002000410043005F00500052004F004D004F005F004E0052005F00420041004C0041004E004300450020002B0020004000700052006500420061006C0061006E006300650020002B00200040007000500072006F006D006F0052006500420061006C0061006E006300650020002B00200040007000500072006F006D006F004E007200420061006C0061006E00630065002000008089200020002000200020002000200020002C002000410043005F00520045005F00420041004C0041004E0043004500200020002000200020002000200020003D002000410043005F00520045005F00420041004C0041004E0043004500200020002000200020002000200020002B0020004000700052006500420061006C0061006E00630065002000008093200020002000200020002000200020002C002000410043005F00500052004F004D004F005F00520045005F00420041004C0041004E0043004500200020003D002000410043005F00500052004F004D004F005F00520045005F00420041004C0041004E0043004500200020002B00200040007000500072006F006D006F0052006500420061006C0061006E00630065002000008093200020002000200020002000200020002C002000410043005F00500052004F004D004F005F004E0052005F00420041004C0041004E0043004500200020003D002000410043005F00500052004F004D004F005F004E0052005F00420041004C0041004E0043004500200020002B00200040007000500072006F006D006F004E007200420061006C0061006E00630065002000008119200020002000200020002000200020002C002000410043005F00520045005F005200450053004500520056004500440020002000200020002000200020003D002000430041005300450020005700480045004E002000410043005F00520045005F0052004500530045005200560045004400200020002B002000400070005200650073006500720076006500640020003E0020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000081192000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000410043005F00520045005F00420041004C0041004E00430045002000200020002B002000410043005F00500052004F004D004F005F00520045005F00420041004C0041004E004300450020002B0020004000700052006500420061006C0061006E006300650020002B00200040007000500072006F006D006F0052006500420061006C0061006E00630065002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000081192000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020005400480045004E002000410043005F00520045005F00420041004C0041004E0043004500200020002B002000410043005F00500052004F004D004F005F00520045005F00420041004C0041004E004300450020002B0020004000700052006500420061006C0061006E006300650020002B00200040007000500072006F006D006F0052006500420061006C0061006E006300650020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000811920002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200045004C00530045002000410043005F00520045005F005200450053004500520056004500440020002B0020004000700052006500730065007200760065006400200045004E004400200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000061200020002000200020002000200020002C002000410043005F004D004F00440045005F0052004500530045005200560045004400200020002000200020003D002000400070004D006F00640065005200650073006500720076006500640020000065200020002000200020002000200020002C002000410043005F004D004F00440045005F0052004500530045005200560045004400200020002000200020003D002000410043005F004D004F00440045005F00520045005300450052005600450044002000004320004F005500540050005500540020002000200049004E005300450052005400450044002E00410043005F004100430043004F0055004E0054005F004900440020000049200020002000200020002000200020002C00200049004E005300450052005400450044002E00410043005F0042004C004F0043004B004500440020002000200020002000200020000049200020002000200020002000200020002C00200049004E005300450052005400450044002E00410043005F0042004C004F0043004B005F0052004500410053004F004E00200020000049200020002000200020002000200020002C00200049004E005300450052005400450044002E00410043005F0048004F004C004400450052005F004C004500560045004C0020002000005F200020002000200020002000200020002C002000490053004E0055004C004C00280049004E005300450052005400450044002E00410043005F0048004F004C004400450052005F00470045004E004400450052002C002000300029002000005F200020002000200020002000200020002C002000490053004E0055004C004C00280049004E005300450052005400450044002E00410043005F0048004F004C004400450052005F004E0041004D0045002C00200020002700270029002000014F200020002000200020002000200020002C00200049004E005300450052005400450044002E00410043005F00520045005F00420041004C0041004E00430045002000200020002000200020002000004F200020002000200020002000200020002C00200049004E005300450052005400450044002E00410043005F00500052004F004D004F005F00520045005F00420041004C0041004E00430045002000004F200020002000200020002000200020002C00200049004E005300450052005400450044002E00410043005F00500052004F004D004F005F004E0052005F00420041004C0041004E004300450020000049200020002000200020002000200020002C002000490053004E0055004C004C002800430042005F00500054002E004300420055005F00560041004C00550045002C00200030002900005D200020002000200020002000200020002C00200049004E005300450052005400450044002E00410043005F00430055005200520045004E0054005F0050004C00410059005F00530045005300530049004F004E005F00490044002000005D200020002000200020002000200020002C00200049004E005300450052005400450044002E00410043005F00430055005200520045004E0054005F005400450052004D0049004E0041004C005F0049004400200020002000200020000079200020002000200020002000200020002C00200049004E005300450052005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F00430041004E00430045004C004C00410042004C0045005F005400520041004E00530041004300540049004F004E005F004900440020000061200020002000200020002000200020002C00200049004E005300450052005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F00520045005F00430041004E00430045004C004C00410042004C0045002000006D200020002000200020002000200020002C00200049004E005300450052005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F00520045005F00430041004E00430045004C004C00410042004C0045002000006D200020002000200020002000200020002C00200049004E005300450052005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F00430041004E00430045004C004C00410042004C00450020000075200020002000200020002000200020002C002000490053004E0055004C004C00280049004E005300450052005400450044002E00410043005F00430041004E00430045004C004C00410042004C0045005F004F005000450052004100540049004F004E005F00490044002C002000300029002000005F200020002000200020002000200020002C002000490053004E0055004C004C00280049004E005300450052005400450044002E00410043005F00520045005F00520045005300450052005600450044002C00200030002900200020002000005F200020002000200020002000200020002C00200049004E005300450052005400450044002E00410043005F004D004F00440045005F00520045005300450052005600450044002000200020002000200020002000200020002000200020000019200020002000200020002000200020002C002000300020000049200020002000200020002000200020002C002000490053004E0055004C004C002800430042005F004E0052002E004300420055005F00560041004C00550045002C0020003000290000452000200020002000200020002C002000490053004E0055004C004C002800430042005F00520045002E004300420055005F00560041004C00550045002C0020003000290000152000200020002000200020002C002000300020000037200020002000200020002000200020002C00200049004E005300450052005400450044002E00410043005F0054005900500045002000002B200020002000200020002000460052004F004D002000200020004100430043004F0055004E00540053000080ED49004E004E004500520020004A004F0049004E0020002000200043005500530054004F004D00450052005F004200550043004B00450054002000430042005F005000540020004F004E0020002000430042005F00500054002E004300420055005F0043005500530054004F004D00450052005F004900440020003D002000410043005F004100430043004F0055004E0054005F00490044002000200041004E00440020002000430042005F00500054002E004300420055005F004200550043004B00450054005F004900440020003D002000400070004200750063006B0065007400490064005F00500054000080ED4C00450046005400200020004A004F0049004E0020002000200043005500530054004F004D00450052005F004200550043004B00450054002000430042005F004E00520020004F004E0020002000430042005F004E0052002E004300420055005F0043005500530054004F004D00450052005F004900440020003D002000410043005F004100430043004F0055004E0054005F00490044002000200041004E00440020002000430042005F004E0052002E004300420055005F004200550043004B00450054005F004900440020003D002000400070004200750063006B0065007400490064005F004E0052000080F14C00450046005400200020004A004F0049004E0020002000200043005500530054004F004D00450052005F004200550043004B00450054002000430042005F005200450020004F004E00200020002000430042005F00520045002E004300420055005F0043005500530054004F004D00450052005F004900440020003D002000410043005F004100430043004F0055004E0054005F00490044002000200041004E004400200020002000430042005F00520045002E004300420055005F004200550043004B00450054005F004900440020003D002000400070004200750063006B0065007400490064005F005200450000772000200020002000200057004800450052004500200020002000410043005F004100430043004F0055004E0054005F004900440020003D002000490053004E0055004C004C00280040004100630063006F0075006E007400490064005F0074006F005500700064006100740065002C0020003000290000234000700041006C00650073006900730054007200610063006B004400610074006100001D40007000570069006E0054007200610063006B00440061007400610000174000700052006500420061006C0061006E0063006500002140007000500072006F006D006F0052006500420061006C0061006E0063006500002140007000500072006F006D006F004E007200420061006C0061006E006300650000114000700050006F0069006E007400730000154000700052006500730065007200760065006400001D400070004D006F006400650052006500730065007200760065006400001B400070004200750063006B0065007400490064005F0050005400001B400070004200750063006B0065007400490064005F004E005200001B400070004200750063006B0065007400490064005F00520045000029450078007400650072006E0061006C0054007200610063006B0044006100740061003A00200027000131270020002D002D003E0020004100630063006F0075006E00740020004E006F007400200046006F0075006E0064002E0001174100630063006F0075006E007400490064003A002000002F20002D002D003E0020004100630063006F0075006E00740020004E006F007400200046006F0075006E0064002E0001292000530045004C00450043005400200020002000500053005F0053005400410054005500530020000053200020002000200020002000200020002C002000490053004E0055004C004C002800500053005F00420041004C0041004E00430045005F004D00490053004D0041005400430048002C002000300029002000003B200020002000200020002000200020002C002000500053005F0049004E0049005400490041004C005F00420041004C0041004E00430045002000002B200020002000200020002000200020002C002000500053005F0043004100530048005F0049004E0020000031200020002000460052004F004D0020002000200050004C00410059005F00530045005300530049004F004E0053002000005F2000200057004800450052004500200020002000500053005F0050004C00410059005F00530045005300530049004F004E005F004900440020003D0020004000700050006C0061007900530065007300730069006F006E004900640020000023270020002D002D003E00200045007800630065007000740069006F006E003A002000012120002D002D003E00200045007800630065007000740069006F006E003A00200001255500500044004100540045002000200020004100430043004F0055004E00540053002000004F200020002000530045005400200020002000410043005F004C004100530054005F0041004300540049005600490054005900200020003D0020004700450054004400410054004500280029002000004D200057004800450052004500200020002000410043005F004100430043004F0055004E0054005F0049004400200020003D002000400070004100630063006F0075006E007400490064002000003B200020002000530045005400200020002000410043005F0042004C004F0043004B00450044002000200020002000200020003D002000310020000080B720002000200020002000200020002C002000410043005F0042004C004F0043004B005F0052004500410053004F004E0020003D002000640062006F002E0042006C006F0063006B0052006500610073006F006E005F0054006F004E006500770045006E0075006D00650072006100740065002800410043005F0042004C004F0043004B005F0052004500410053004F004E00290020007C0020004000700042006C006F0063006B0052006500610073006F006E002000004920002000200020002000200020002C002000410043005F0042004C004F0043004B005F004400450053004300520049005000540049004F004E0020003D0020004E0055004C004C00004F200057004800450052004500200020002000410043005F004100430043004F0055004E0054005F00490044002000200020003D002000400070004100630063006F0075006E00740049006400200000809920002000200041004E004400200020002000410043005F00540059005000450020004E004F005400200049004E0020002800400070004100630063006F0075006E0074005600690072007400750061006C0043006100730068006900650072002C002000400070004100630063006F0075006E0074005600690072007400750061006C005400650072006D0069006E0061006C0029002000001B4000700042006C006F0063006B0052006500610073006F006E00002F400070004100630063006F0075006E0074005600690072007400750061006C004300610073006800690065007200002D430061006E00630065006C00530074006100720074004300610072006400530065007300730069006F006E00003B4400450043004C00410052004500200040004F00750074007000750074005400610062006C00650020005400410042004C00450020002800200000672000200020002000200020002000200020002000410043005F004100430043004F0055004E0054005F0049004400200020002000200020002000200020002000200020002000200020002000200020002000200020002000200042004900470049004E0054000067200020002000200020002000200020002C002000410043005F00430055005200520045004E0054005F0050004C00410059005F00530045005300530049004F004E005F004900440020002000200020002000200020002000200042004900470049004E0054000065200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F00430041004E00430045004C004C00410042004C00450020002000200020002000200020004D004F004E00450059000065200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F00520045005F00430041004E00430045004C004C00410042004C00450020004D004F004E00450059000065200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F00430041004E00430045004C004C00410042004C00450020004D004F004E00450059000065200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F0054004F005F0047004D0020002000200020002000200020002000200020002000200020004D004F004E00450059000065200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F00520045005F0054004F005F0047004D0020002000200020002000200020004D004F004E00450059000065200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F0054004F005F0047004D0020002000200020002000200020004D004F004E00450059000065200020002000200020002000200020002C002000500053005F0049004E0049005400490041004C005F00420041004C0041004E0043004500200020002000200020002000200020002000200020002000200020002000200020004D004F004E00450059000065200020002000200020002000200020002C002000500053005F0043004100530048005F0049004E0020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020004D004F004E00450059000077200020002000200020002000200020002C002000540045005F004E0041004D00450020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020004E005600410052004300480041005200280035003000290029003B00006F2000200020002000530045005400200020002000410043005F0049004E005F00530045005300530049004F004E005F00430041004E00430045004C004C00410042004C0045005F005400520041004E00530041004300540049004F004E005F004900440020003D00200030002000006B20004F0055005400500055005400200020002000440045004C0045005400450044002E00410043005F004100430043004F0055004E0054005F00490044002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000006B200020002000200020002000200020002C002000440045004C0045005400450044002E00410043005F00430055005200520045004E0054005F0050004C00410059005F00530045005300530049004F004E005F0049004400200020002000200020002000200020002000006B200020002000200020002000200020002C002000440045004C0045005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F00520045005F00430041004E00430045004C004C00410042004C0045002000200020002000200020002000006B200020002000200020002000200020002C002000440045004C0045005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F00520045005F00430041004E00430045004C004C00410042004C0045002000006B200020002000200020002000200020002C002000440045004C0045005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F00430041004E00430045004C004C00410042004C00450020000053200020002000200020002000200020002C002000440045004C0045005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F00520045005F0054004F005F0047004D002000005F200020002000200020002000200020002C002000440045004C0045005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F00520045005F0054004F005F0047004D002000005F200020002000200020002000200020002C002000440045004C0045005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F0054004F005F0047004D0020000041200020002000200020002000200020002C002000500053002E00500053005F0049004E0049005400490041004C005F00420041004C0041004E004300450020000031200020002000200020002000200020002C002000500053002E00500053005F0043004100530048005F0049004E002000002B200020002000200020002000200020002C002000540045002E00540045005F004E0041004D00450020000080C32000200049004E004E00450052002000200020004A004F0049004E0020005400450052004D0049004E0041004C005300200020002000200020004100530020005400450020004F004E002000410043005F00430055005200520045004E0054005F005400450052004D0049004E0041004C005F0049004400200020002000200020003D002000540045002E00540045005F005400450052004D0049004E0041004C005F004900440020002000200020002000200020002000200020002000200020000080C32000200049004E004E00450052002000200020004A004F0049004E00200050004C00410059005F00530045005300530049004F004E00530020004100530020005000530020004F004E002000410043005F00430055005200520045004E0054005F0050004C00410059005F00530045005300530049004F004E005F004900440020003D002000500053002E00500053005F0050004C00410059005F00530045005300530049004F004E005F0049004400200020002000200020002000200020002000006D2000200057004800450052004500200020002000540045002E00540045005F005400450052004D0049004E0041004C005F004900440020002000200020002000200020002000200020002000200020003D002000400070005400650072006D0069006E0061006C0049006400008091200020002000200041004E004400200020002000540045002E00540045005F00430055005200520045004E0054005F0050004C00410059005F00530045005300530049004F004E005F004900440020003D002000410043005F00430055005200520045004E0054005F0050004C00410059005F00530045005300530049004F004E005F00490044002000200020002000008091200020002000200041004E004400200020002000500053002E00500053005F0050004C00410059005F00530045005300530049004F004E005F00490044002000200020002000200020002000200020003D002000540045002E00540045005F00430055005200520045004E0054005F0050004C00410059005F00530045005300530049004F004E005F00490044002000008091200020002000200041004E004400200020002000500053002E00500053005F005400450052004D0049004E0041004C005F004900440020002000200020002000200020002000200020002000200020003D002000400070005400650072006D0069006E0061006C0049006400200020002000200020002000200020002000200020002000200020002000200020002000008091200020002000200041004E004400200020002000410043005F004100430043004F0055004E0054005F0049004400200020002000200020002000200020002000200020002000200020002000200020003D002000540045002E00540045005F00430055005200520045004E0054005F004100430043004F0055004E0054005F0049004400200020002000200020002000008091200020002000200041004E004400200020002000410043005F00430055005200520045004E0054005F005400450052004D0049004E0041004C005F0049004400200020002000200020002000200020003D002000400070005400650072006D0069006E0061006C0049006400200020002000200020002000200020002000200020002000200020002000200020002000008091200020002000200041004E004400200020002000410043005F00430055005200520045004E0054005F0050004C00410059005F00530045005300530049004F004E005F004900440020002000200020003D002000540045002E00540045005F00430055005200520045004E0054005F0050004C00410059005F00530045005300530049004F004E005F0049004400200000808B200020002000200041004E004400200020002000410043005F0049004E005F00530045005300530049004F004E005F00430041004E00430045004C004C00410042004C0045005F005400520041004E00530041004300540049004F004E005F004900440020003D002000400070005400720061006E00730061006300740069006F006E004900640020000063200020002000200041004E004400200020002000410043005F00430055005200520045004E0054005F0050004C00410059005F00530045005300530049004F004E005F004900440020004900530020004E004F00540020004E0055004C004C0020000051200020002000200041004E004400200020002000500053002E00500053005F0053005400410054005500530020003D002000400070005300740061007400750073004F00700065006E00650064002000001D400070005300740061007400750073004F00700065006E0065006400005D2000200020002000200020002000200020002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F0054004F005F0047004D002000200020002000200020002000200020004D004F004E0045005900005D200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F00520045005F0054004F005F0047004D002000200020004D004F004E0045005900005D200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F0054004F005F0047004D002000200020004D004F004E0045005900005D200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F00460052004F004D005F0047004D0020002000200020002000200020004D004F004E0045005900005D200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F00520045005F00460052004F004D005F0047004D0020004D004F004E0045005900005D200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F00460052004F004D005F0047004D0020004D004F004E0045005900005D200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F00420041004C0041004E004300450020002000200020002000200020004D004F004E0045005900005D200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F00520045005F00420041004C0041004E004300450020004D004F004E00450059000061200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F00420041004C0041004E004300450020004D004F004E004500590029003B000080B32000200020002000530045005400200020002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F00420041004C0041004E004300450020002000200020002000200020003D002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F00420041004C0041004E004300450020002000200020002000200020002B00200040007000520065006400650065006D00610062006C00650020000080AD200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F00520045005F00420041004C0041004E004300450020003D002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F00520045005F00420041004C0041004E004300450020002B00200040007000500072006F006D006F005200650020000080AD200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F00420041004C0041004E004300450020003D002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F00420041004C0041004E004300450020002B00200040007000500072006F006D006F004E00720020000080BB200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F0054004F005F0047004D002000200020002000200020002000200020003D002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F0054004F005F0047004D002000200020002000200020002000200020002B0020004000700054006F0047006D00520065006400650065006D00610062006C00650020000080B5200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F00520045005F0054004F005F0047004D002000200020003D002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F00520045005F0054004F005F0047004D002000200020002B0020004000700054006F0047006D00500072006F006D006F005200650020000080B5200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F0054004F005F0047004D002000200020003D002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F0054004F005F0047004D002000200020002B0020004000700054006F0047006D00500072006F006D006F004E00720020000080BF200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F00460052004F004D005F0047004D0020002000200020002000200020003D002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F00460052004F004D005F0047004D0020002000200020002000200020002B00200040007000460072006F006D0047006D00520065006400650065006D00610062006C00650020000080B9200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F00520045005F00460052004F004D005F0047004D0020003D002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F00520045005F00460052004F004D005F0047004D0020002B00200040007000460072006F006D0047006D00500072006F006D006F005200650020000080B9200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F00460052004F004D005F0047004D0020003D002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F00460052004F004D005F0047004D0020002B00200040007000460072006F006D0047006D00500072006F006D006F004E0072002000008097200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00430041004E00430045004C004C00410042004C0045005F005400520041004E00530041004300540049004F004E005F004900440020003D00200040007000430061006E00630065006C005400720061006E00730061006300740069006F006E00490064002000008091200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F00430041004E00430045004C004C00410042004C00450020002000200020002000200020002000200020002000200020003D00200040007000430061006E00630065006C00520065006400650065006D00610062006C006500200000808B200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F00520045005F00430041004E00430045004C004C00410042004C00450020002000200020002000200020003D00200040007000430061006E00630065006C00500072006F006D006F0052006500200000808B200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F00430041004E00430045004C004C00410042004C00450020002000200020002000200020003D00200040007000430061006E00630065006C00500072006F006D006F004E0072002000005520004F005500540050005500540020002000200049004E005300450052005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F00520045005F0054004F005F0047004D0020000061200020002000200020002000200020002C00200049004E005300450052005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F00520045005F0054004F005F0047004D0020000061200020002000200020002000200020002C00200049004E005300450052005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F0054004F005F0047004D0020000059200020002000200020002000200020002C00200049004E005300450052005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F00520045005F00460052004F004D005F0047004D0020000065200020002000200020002000200020002C00200049004E005300450052005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F00520045005F00460052004F004D005F0047004D0020000065200020002000200020002000200020002C00200049004E005300450052005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F00460052004F004D005F0047004D0020000059200020002000200020002000200020002C00200049004E005300450052005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F00520045005F00420041004C0041004E004300450020000065200020002000200020002000200020002C00200049004E005300450052005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F00520045005F00420041004C0041004E004300450020000065200020002000200020002000200020002C00200049004E005300450052005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F00420041004C0041004E00430045002000001940007000520065006400650065006D00610062006C006500001340007000500072006F006D006F0052006500001340007000500072006F006D006F004E00720000214000700054006F0047006D00520065006400650065006D00610062006C006500001B4000700054006F0047006D00500072006F006D006F0052006500001B4000700054006F0047006D00500072006F006D006F004E007200002540007000460072006F006D0047006D00520065006400650065006D00610062006C006500001F40007000460072006F006D0047006D00500072006F006D006F0052006500001F40007000460072006F006D0047006D00500072006F006D006F004E007200002B40007000430061006E00630065006C005400720061006E00730061006300740069006F006E0049006400002540007000430061006E00630065006C00520065006400650065006D00610062006C006500001F40007000430061006E00630065006C00500072006F006D006F0052006500001F40007000430061006E00630065006C00500072006F006D006F004E00720000332000200055005000440041005400450020002000200050004C00410059005F00530045005300530049004F004E005300200000811520002000200020002000530045005400200020002000500053005F0049004E0049005400490041004C005F00420041004C0041004E004300450020003D002000430041005300450020005700480045004E00200028004000700049007300460069007200730074005400720061006E00730066006500720020003D0020003100290020005400480045004E002000500053005F0049004E0049005400490041004C005F00420041004C0041004E004300450020002B0020004000700050006C0061007900610062006C006500420061006C0061006E0063006500200045004C00530045002000500053005F0049004E0049005400490041004C005F00420041004C0041004E0043004500200045004E00440020000080A120002000200020002000200020002000200020002C002000500053005F004100550058005F00460054005F004E0052005F0043004100530048005F0049004E0020003D002000490053004E0055004C004C002800500053005F004100550058005F00460054005F004E0052005F0043004100530048005F0049004E002C0020003000290020002B002000400070004E005200420061006C0061006E00630065000080A120002000200020002000200020002000200020002C002000500053005F004100550058005F00460054005F00520045005F0043004100530048005F0049004E0020003D002000490053004E0055004C004C002800500053005F004100550058005F00460054005F00520045005F0043004100530048005F0049004E002C0020003000290020002B0020004000700052004500420061006C0061006E006300650000813D20002000200020002000200020002000200020002C002000500053005F0043004100530048005F0049004E0020003D002000430041005300450020005700480045004E002000280040007000490067006E006F007200650050006C0061007900610062006C00650049006E00430061007300680049006E0020003D0020003100290020005400480045004E002000500053005F0043004100530048005F0049004E0020005700480045004E00200028004000700049007300460069007200730074005400720061006E00730066006500720020003D0020003100290020005400480045004E0020003000200045004C00530045002000500053005F0043004100530048005F0049004E0020002B0020004000700050006C0061007900610062006C006500420061006C0061006E0063006500200045004E0044002000006320002000200057004800450052004500200020002000500053005F0050004C00410059005F00530045005300530049004F004E005F004900440020003D0020004000700050006C0061007900530065007300730069006F006E00490064002000200000234000700049007300460069007200730074005400720061006E007300660065007200003140007000490067006E006F007200650050006C0061007900610062006C00650049006E00430061007300680049006E0000234000700050006C0061007900610062006C006500420061006C0061006E00630065000017400070004E005200420061006C0061006E006300650000174000700052004500420061006C0061006E00630065000080C72000530045004C00450043005400200020002000430041005300450020005700480045004E002000410043005F00540059005000450020003D002000400070004100630063006F0075006E0074005600690072007400750061006C005400650072006D0069006E0061006C0020005400480045004E0020003000200045004C00530045002000490053004E0055004C004C002800410043005F0048004F004C004400450052005F004C004500560045004C002C00200030002900200045004E00440020002000002F2000530045004C00450043005400200020002000470050005F004B00450059005F00560041004C005500450020000033200020002000460052004F004D00200020002000470045004E004500520041004C005F0050004100520041004D005300200000572000200057004800450052004500200020002000470050005F00470052004F00550050005F004B0045005900200020003D002000270050006C00610079006500720054007200610063006B0069006E00670027002000017B200020002000200041004E004400200020002000470050005F005300550042004A004500430054005F004B004500590020003D00200027004C006500760065006C00580058002E00520065006400650065006D00610062006C00650050006C00610079006500640054006F00310050006F0069006E0074002700010520003B000079200020002000200041004E004400200020002000470050005F005300550042004A004500430054005F004B004500590020003D00200027004C006500760065006C00580058002E00520065006400650065006D00610062006C0065005300700065006E00740054006F00310050006F0069006E00740027000173200020002000200041004E004400200020002000470050005F005300550042004A004500430054005F004B004500590020003D00200027004C006500760065006C00580058002E0054006F00740061006C0050006C00610079006500640054006F00310050006F0069006E00740027002000010558005800000530003000006D530045004C004500430054002000490053004E0055004C004C0020002800200028002000530045004C00450043005400200020002000430041005300540020002800470050005F004B00450059005F00560041004C0055004500200041005300200049004E00540029002000005520002000200020002000200020002000200020002000200020002000200020002000200020002000460052004F004D00200020002000470045004E004500520041004C005F0050004100520041004D00530020000080A9200020002000200020002000200020002000200020002000200020002000200020002000200057004800450052004500200020002000470050005F00470052004F00550050005F004B00450059002000200020003D002000270050006C00610079006500720054007200610063006B0069006E0067002E00450078007400650072006E0061006C004C006F00790061006C0074007900500072006F006700720061006D0027002000016720002000200020002000200020002000200020002000200020002000200020002000200020002000200041004E004400200020002000470050005F005300550042004A004500430054005F004B004500590020003D00200027004D006F006400650027002000017D20002000200020002000200020002000200020002000200020002000200020002000200020002000200041004E004400200020002000490053004E0055004D0045005200490043002800470050005F004B00450059005F00560041004C0055004500290020003D0020003100200029002C0020003000200029002000001B4200690072007400680044006100790041006C00610072006D0000174000700041006C00610072006D00540079007000650000155400650072006D0069006E0061006C003A0020000057200049004E005300450052005400200049004E0054004F002000200041004C00410052004D0053002000280041004C005F0053004F0055005200430045005F0043004F004400450020002000200020002000200020000057200020002000200020002000200020002000200020002000200020002000200020002000200020002C00200041004C005F0053004F0055005200430045005F00490044002000200020002000200020002000200020000057200020002000200020002000200020002000200020002000200020002000200020002000200020002C00200041004C005F0053004F0055005200430045005F004E0041004D00450020002000200020002000200020000057200020002000200020002000200020002000200020002000200020002000200020002000200020002C00200041004C005F0041004C00410052004D005F0043004F0044004500200020002000200020002000200020000057200020002000200020002000200020002000200020002000200020002000200020002000200020002C00200041004C005F0041004C00410052004D005F004E0041004D004500200020002000200020002000200020000057200020002000200020002000200020002000200020002000200020002000200020002000200020002C00200041004C005F0041004C00410052004D005F004400450053004300520049005000540049004F004E0020000057200020002000200020002000200020002000200020002000200020002000200020002000200020002C00200041004C005F005300450056004500520049005400590020002000200020002000200020002000200020000057200020002000200020002000200020002000200020002000200020002000200020002000200020002C00200041004C005F005200450050004F00520054004500440020002000200020002000200020002000200020000057200020002000200020002000200020002000200020002000200020002000200020002000200020002C00200041004C005F004400410054004500540049004D004500200020002000200020002000200020002000200000572000200020002000200020002000200020002000200020002000200020002000200020002000200020002900200020002000200020002000200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002000560041004C00550045005300200028004000700053006F00750072006300650043006F00640065002000200020002000200020002000200020000057200020002000200020002000200020002000200020002000200020002000200020002000200020002C0020004000700053006F00750072006300650049006400200020002000200020002000200020002000200020000057200020002000200020002000200020002000200020002000200020002000200020002000200020002C0020004000700053006F0075007200630065004E0061006D0065002000200020002000200020002000200020000057200020002000200020002000200020002000200020002000200020002000200020002000200020002C0020004000700041006C00610072006D0043006F006400650020002000200020002000200020002000200020000057200020002000200020002000200020002000200020002000200020002000200020002000200020002C0020004000700041006C00610072006D004E0061006D00650020002000200020002000200020002000200020000057200020002000200020002000200020002000200020002000200020002000200020002000200020002C0020004000700041006C00610072006D004400650073006300720069007000740069006F006E002000200020000057200020002000200020002000200020002000200020002000200020002000200020002000200020002C0020004000700053006500760065007200690074007900200020002000200020002000200020002000200020000057200020002000200020002000200020002000200020002000200020002000200020002000200020002C00200047004500540044004100540045002000280029002000200020002000200020002000200020002000200000194000700053006F00750072006300650043006F006400650000154000700053006F0075007200630065004900640000194000700053006F0075007200630065004E0061006D00650000174000700041006C00610072006D0043006F006400650000174000700041006C00610072006D004E0061006D0065000009550073006500720000254000700041006C00610072006D004400650073006300720069007000740069006F006E0000154000700053006500760065007200690074007900004D20002000200020005300450054002000200020004100430050005F00420041004C0041004E004300450020003D00200040007000420061006C0061006E0063006500520065007300740020000049200020002000200020002000200020002C0020004100430050005F00530054004100540055005300200020003D002000400070004E00650077005300740061007400750073002000001B40007000420061006C0061006E006300650052006500730074000080B955005000440041005400450020005400450052004D0049004E0041004C00530020005300450054002000540045005F004100430043004F0055004E0054005F00500052004F004D004F00540049004F004E005F004900440020003D00200040007000500072006F006D006F00490064002000570048004500520045002000540045005F005400450052004D0049004E0041004C005F004900440020003D002000400070005400650072006D0069006E0061006C0049006400001340007000500072006F006D006F004900640000809D2000200020002000530045004C00450043005400200020002000410043005F004100430043004F0055004E0054005F0049004400200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000809D200020002000200020002000200020002000200020002C002000490053004E0055004C004C002800540045005F004100430043004F0055004E0054005F00500052004F004D004F00540049004F004E005F00490044002C00200030002900200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000809D200020002000200020002000200020002000200020002C002000500053005F0050004C00410059005F00530045005300530049004F004E005F00490044002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000809D200020002000200020002000200020002000200020002C002000540045005F004E0041004D004500200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000809D200020002000200020002000460052004F004D002000200020005400450052004D0049004E0041004C0053002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000809D20004C0045004600540020004A004F0049004E0020002000200050004C00410059005F00530045005300530049004F004E00530020004F004E002000500053005F0050004C00410059005F00530045005300530049004F004E005F004900440020003D002000540045005F00430055005200520045004E0054005F0050004C00410059005F00530045005300530049004F004E005F0049004400200000809D20004C0045004600540020004A004F0049004E002000200020004100430043004F0055004E00540053002000200020002000200020004F004E002000410043005F004100430043004F0055004E0054005F00490044002000200020002000200020003D002000500053005F004100430043004F0055004E0054005F00490044002000200020002000200020002000200020002000200020002000200000809D2000200020002000200057004800450052004500200020002000540045005F005400450052004D0049004E0041004C005F004900440020003D002000400070005400650072006D0069006E0061006C004900640020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000809D200020002000200020002000200041004E0044002000200020002800200020002000410043005F005400590050004500200020002000200020002000200020003D002000400070004100630063006F0075006E0074005400790070006500200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000809D200020002000200020002000200020002000200020002000200020004F0052002000410043005F005400590050004500200020002000200020002000200020003D002000400070005600690072007400750061006C004100630063006F0075006E007400540079007000650020002900200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000809D200020002000200020002000200041004E004400200020002000500053005F005300540041005400550053002000200020002000200020003D002000400070005300740061007400750073002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000001B400070004100630063006F0075006E00740054007900700065000029400070005600690072007400750061006C004100630063006F0075006E0074005400790070006500006B20002000200020005300450054002000200020004100430050005F00420041004C0041004E004300450020002000200020003D0020004100430050005F00420041004C0041004E004300450020002B002000400070004E005200420061006C0061006E006300650020000055200020002000200020002000200020002C0020004100430050005F00530054004100540055005300200020002000200020003D002000400070005300740061007400750073004100630074006900760065002000006120002000570048004500520045002000200020004100430050005F0055004E0049005100550045005F0049004400200020003D002000400070004100630063006F0075006E007400500072006F006D006F00740069006F006E00490064002000004F200020002000200041004E0044002000200020004100430050005F004100430043004F0055004E0054005F004900440020003D002000400070004100630063006F0075006E007400490064002000006B20005500500044004100540045002000200020004100430043004F0055004E005400530020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000006B2000200020002000530045005400200020002000410043005F00520045005F005200450053004500520056004500440020003D0020004000700041006D006F0075006E007400200020002000200020002000200020002000200020002000200020002000200020002000006B200020002000200020002000200020002C002000410043005F004D004F00440045005F005200450053004500520056004500440020003D002000400070004D006F0064006500520065007300650072007600650064002000200020002000200020002000200020002000006B2000200020002000530045005400200020002000410043005F00520045005F005200450053004500520056004500440020003D002000410043005F00520045005F005200450053004500520056004500440020002B0020004000700041006D006F0075006E0074002000006B2000200057004800450052004500200020002000410043005F004100430043004F0055004E0054005F0049004400200020003D002000400070004100630063006F0075006E0074004900640020002000200020002000200020002000200020002000200020002000200000174100430050005F0057004F004E004C004F0043004B0000194100430050005F00570049005400480048004F004C0044000080D7200020002000530045005400200020002000500053005F0053005400410054005500530020002000200020002000200020002000200020003D002000430041005300450020005700480045004E0020002800500053005F0053005400410054005500530020003D002000400070005300740061007400750073004F00700065006E0065006400290020005400480045004E002000400070004E0065007700530074006100740075007300200045004C00530045002000500053005F0053005400410054005500530020002000200045004E00440020000080D720002000200020002000200020002C002000500053005F004C004F0043004B004500440020002000200020002000200020002000200020003D002000430041005300450020005700480045004E0020002800500053005F0053005400410054005500530020003D002000400070005300740061007400750073004F00700065006E0065006400290020005400480045004E0020004E0055004C004C002000200020002000200020002000200045004C00530045002000500053005F004C004F0043004B004500440020002000200045004E00440020000080D720002000200020002000200020002C002000500053005F00460049004E0049005300480045004400200020002000200020002000200020003D002000430041005300450020005700480045004E0020002800500053005F0053005400410054005500530020003D002000400070005300740061007400750073004F00700065006E0065006400290020005400480045004E00200047004500540044004100540045002800290020002000200045004C00530045002000500053005F00460049004E0049005300480045004400200045004E004400200000374F0055005400500055005400200020002000440045004C0045005400450044002E00500053005F005300540041005400550053002000003920002000200020002000200020002C00200049004E005300450052005400450044002E00500053005F005300540041005400550053002000003920002000200020002000200020002C002000440045004C0045005400450044002E00500053005F0053005400410052005400450044002000006120002000200020002000200020002C002000490053004E0055004C004C002800440045004C0045005400450044002E00500053005F00460049004E00490053004800450044002C00200047004500540044004100540045002800290029002000005B200057004800450052004500200020002000500053005F0050004C00410059005F00530045005300530049004F004E005F004900440020003D0020004000700050006C0061007900530065007300730069006F006E0049006400002F200055005000440041005400450020002000200050004C00410059005F00530045005300530049004F004E00530000810F200020002000530045005400200020002000500053005F005200450050004F0052005400450044005F00420041004C0041004E00430045005F004D00490053004D00410054004300480020003D002000430041005300450020005700480045004E002000280040007000420061006C0061006E00630065004D00690073006D00610074006300680020003D0020003100290020005400480045004E002000400070005200650070006F007200740065006400420061006C0061006E0063006500200045004C00530045002000500053005F005200450050004F0052005400450044005F00420041004C0041004E00430045005F004D00490053004D004100540043004800200045004E0044002000006F20002000200020002000200020002C002000500053005F00460049004E0041004C005F00420041004C0041004E004300450020002000200020002000200020002000200020002000200020003D00200040007000460069006E0061006C00420061006C0061006E00630065002000007520002000200020002000200020002C002000500053005F00420041004C0041004E00430045005F004D00490053004D00410054004300480020002000200020002000200020002000200020003D00200040007000420061006C0061006E00630065004D00690073006D00610074006300680020000071200057004800450052004500200020002000500053005F0050004C00410059005F00530045005300530049004F004E005F0049004400200020002000200020002000200020002000200020003D0020004000700050006C0061007900530065007300730069006F006E0049004400200000809F20002000200041004E004400200020002000500053005F005300540041005400550053002000200020002000200020002000200020002000200020002000200020002000200020002000200049004E00200028002000400070005300740061007400750073004F00700065006E00650064002C002000400070005300740061007400750073004100620061006E0064006F006E0065006400200029002000006F20002000200041004E004400200020002000500053005F00530054004100540055005300200020002000200020002000200020002000200020002000200020002000200020002000200020003D002000400070005300740061007400750073004F00700065006E006500640020000023400070005200650070006F007200740065006400420061006C0061006E0063006500002340007000420061006C0061006E00630065004D00690073006D0061007400630068000023400070005300740061007400750073004100620061006E0064006F006E0065006400006720002000200055005000440041005400450020002000200050004C00410059005F00530045005300530049004F004E005300200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000067200020002000200020002000530045005400200020002000500053005F0053005400410054005500530020002000200020002000200020002000200020003D002000400070005300740061007400750073004100620061006E0064006F006E00650064002000006720002000200020002000200020002000200020002C002000500053005F00460049004E0049005300480045004400200020002000200020002000200020003D0020004700450054004400410054004500280029002000200020002000200020002000200020000067200020002000200057004800450052004500200020002000500053005F0050004C00410059005F00530045005300530049004F004E005F004900440020003D0020004000700050006C0061007900530065007300730069006F006E004900640020002000200000395400720078005F004F006E0045006E0064004300610072006400530065007300730069006F006E0020006600610069006C00650064002E00004D5400720078005F0050006C0061007900530065007300730069006F006E004D00610072006B00410073004100620061006E0064006F006E006500640020006600610069006C00650064002E00003B5400720078005F005300650074004100630063006F0075006E00740042006C006F0063006B006500640020006600610069006C00650064002E0000352000530045004C00450043005400200020002000500053005F0050004C0041005900450044005F0043004F0055004E00540020000037200020002000200020002000200020002C002000500053005F0050004C0041005900450044005F0041004D004F0055004E0054002000002F200020002000200020002000200020002C002000500053005F0057004F004E005F0043004F0055004E00540020000031200020002000200020002000200020002C002000500053005F0057004F004E005F0041004D004F0055004E005400200000272000530045004C0045004300540020002000200043004F0055004E0054002800310029002000003B200020002000460052004F004D002000200020004100430043004F0055004E0054005F00500052004F004D004F00540049004F004E0053002000004F20002000570048004500520045002000200020004100430050005F004100430043004F0055004E0054005F0049004400200020003D002000400070004100630063006F0075006E007400490064000055200020002000200041004E0044002000200020004100430050005F005300540041005400550053002000200020002000200020003D002000400070005300740061007400750073004100630074006900760065000055200020002000200041004E0044002000200020004100430050005F004300520045004400490054005F005400590050004500200049004E0020002800400070004E00520031002C00400070004E00520032002900000B400070004E0052003100000B400070004E00520032000080AB200020002000530045005400200020002000410043005F00500052004F004D004F005F0049004E0049005F00520045005F00420041004C0041004E004300450020003D002000430041005300450020005700480045004E00200028002000410043005F00500052004F004D004F005F0049004E0049005F00520045005F00420041004C0041004E004300450020004900530020004E004F00540020004E0055004C004C002000290020000080B12000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020005400480045004E002000430041005300450020005700480045004E00200028002000410043005F00500052004F004D004F005F0049004E0049005F00520045005F00420041004C0041004E004300450020002B0020004000700041006D006F0075006E00740020003E003D00200030002900200000808D2000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020005400480045004E002000410043005F00500052004F004D004F005F0049004E0049005F00520045005F00420041004C0041004E004300450020002B0020004000700041006D006F0075006E0074002000006F20002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200045004C005300450020003000200045004E004400200045004C005300450020004E0055004C004C00200045004E0044002000004B200057004800450052004500200020002000410043005F004100430043004F0055004E0054005F004900440020003D002000400070004100630063006F0075006E007400490064002000005D200020002000530045005400200020002000410043005F00500052004F004D004F005F0049004E0049005F00520045005F00420041004C0041004E004300450020002000200020003D0020004000700041006D006F0075006E007400001F4D0075006C007400690053006900740065004D0065006D00620065007200005749004E005300450052005400200049004E0054004F002000200020004D0053005F00500045004E00440049004E0047005F00470041004D0045005F0050004C00410059005F00530045005300530049004F004E00530000472000200020002000200020002000200020002000200020002000200028004D00500053005F0050004C00410059005F00530045005300530049004F004E005F00490044002900004120002000200020002000560041004C0055004500530020002000200028004000700050006C0061007900530065007300730069006F006E004900640029002000003349004E005300450052005400200049004E0054004F0020005700430050005F0043004F004D004D0041004E00440053002000003920002000200020002000200020002000200020002000280043004D0044005F005400450052004D0049004E0041004C005F00490044002000002B20002000200020002000200020002000200020002C00200043004D0044005F0043004F00440045002000002F20002000200020002000200020002000200020002C00200043004D0044005F005300540041005400550053002000003120002000200020002000200020002000200020002C00200043004D0044005F0043005200450041005400450044002000003F20002000200020002000200020002000200020002C00200043004D0044005F005300540041005400550053005F004300480041004E004700450044002000002F20002000200020002000200020002000200020002C00200043004D0044005F00500053005F00490044002900200000809720002000200020002000560041004C0055004500530020002800200040005400650072006D0069006E0061006C004900640020002C002000310020002C002000300020002C00200047004500540044004100540045002800290020002C00200047004500540044004100540045002800290020002C00400050006C0061007900530065007300730069006F006E004900640020002900001740005400650072006D0069006E0061006C0049006400001D400050006C0061007900530065007300730069006F006E0049006400007B2000200057004800450052004500200020002000470050005F00470052004F00550050005F004B00450059002000200020003D002000270050006C00610079006500720054007200610063006B0069006E0067002E0044006F004E006F0074004100770061007200640050006F0069006E007400730027002000017B200020002000200041004E004400200020002000470050005F005300550042004A004500430054005F004B004500590020003D002000270050006C0061007900530065007300730069006F006E002E0050006F0069006E007400730041007200650047007200650061007400650072005400680061006E00270001192000200055004E0049004F004E00200041004C004C00200000808D200020002000200041004E004400200020002000470050005F005300550042004A004500430054005F004B004500590020003D002000270050006C0061007900530065007300730069006F006E002E0050006F0069006E00740073005000650072004D0069006E0075007400650041007200650047007200650061007400650072005400680061006E0027000169200020002000200041004E004400200020002000470050005F005300550042004A004500430054005F004B004500590020003D002000270050006C0061007900530065007300730069006F006E002E004800610073004E006F0050006C00610079007300270020000179200020002000200041004E004400200020002000470050005F005300550042004A004500430054005F004B004500590020003D002000270050006C0061007900530065007300730069006F006E002E00480061007300420061006C0061006E00630065004D00690073006D00610074006300680027002000012943006F006D0070007500740065006400200050006F0069006E00740073003A0020007B0030007D00003D2C00200050006F0069006E007400730020004100720065002000470072006500610074006500720020005400680061006E003A0020007B0030007D00002B2C0020004D0069006E007500740065007300200050006C0061007900650064003A0020007B0030007D00000930002E002300230000312C00200050006F0069006E0074007300200050006500720020004D0069006E007500740065003A0020007B0030007D0000532C00200050006F0069006E0074007300200050006500720020004D0069006E0075007400650020004100720065002000470072006500610074006500720020005400680061006E003A0020007B0030007D0000214E0075006D002E00200050006C0061007900650064003A0020007B0030007D0000292C00200048006100730020004E006F00200050006C006100790073003A0020005400720075006500002B420061006C0061006E006300650020004D00690073006D0061007400630068003A0020007B0030007D0000392C0020004800610073002000420061006C0061006E006300650020004D00690073006D0061007400630068003A0020005400720075006500002550006C0061007900200074006F00200072006500760069007300650020002D003E002000011D4100630063006F0075006E007400490064003A0020007B0030007D0000232C0020005400650072006D0069006E0061006C00490064003A0020007B0030007D00001B20004300720069007400650072006900610020002D003E002000013520002000200055005000440041005400450020002000200050004C00410059005F00530045005300530049004F004E0053002000008109200020002000200020002000530045005400200020002000500053005F0041005700410052004400450044005F0050004F0049004E00540053005F0053005400410054005500530020003D002000430041005300450020005700480045004E002000500053005F0053005400410054005500530020003C003E00200040007000530074006100740065004F00700065006E006500640020005400480045004E0020004000700050006F0069006E007400730053007400610074007500730020002000200045004C00530045002000500053005F0041005700410052004400450044005F0050004F0049004E00540053005F00530054004100540055005300200045004E00440020000080E120002000200020002000200020002000200020002C002000500053005F0043004F004D00500055005400450044005F0050004F0049004E005400530020002000200020002000200020003D002000430041005300450020005700480045004E002000500053005F0053005400410054005500530020003C003E00200040007000530074006100740065004F00700065006E006500640020005400480045004E0020004000700043006F006D007000750074006500640050006F0069006E0074007300200045004C005300450020004E0055004C004C00200045004E00440020000080E120002000200020002000200020002000200020002C002000500053005F0041005700410052004400450044005F0050004F0049004E0054005300200020002000200020002000200020003D002000430041005300450020005700480045004E002000500053005F0053005400410054005500530020003C003E00200040007000530074006100740065004F00700065006E006500640020005400480045004E00200040007000410077006100720064006500640050006F0069006E00740073002000200045004C005300450020004E0055004C004C00200045004E0044002000006F200020002000200057004800450052004500200020002000500053005F0050004C00410059005F00530045005300530049004F004E005F004900440020002000200020002000200020003D0020004000700050006C0061007900530065007300730069006F006E00490064002000001B40007000530074006100740065004F00700065006E0065006400001D4000700050006F0069006E007400730053007400610074007500730000214000700043006F006D007000750074006500640050006F0069006E0074007300001F40007000410077006100720064006500640050006F0069006E007400730000492000200057004800450052004500200020002000470050005F00470052004F00550050005F004B004500590020002000200020003D00200040007000470072006F00750070002000004D200020002000200041004E004400200020002000470050005F005300550042004A004500430054005F004B0045005900200020003D002000400070005300750062006A006500630074002000000F40007000470072006F00750070000013400070005300750062006A0065006300740000135700610072006E0069006E0067003A002000003F7B0030007D0020003C00520045003A0020007B0031007D002C002000500052003A0020007B0032007D002C0020004E0052003A0020007B0033007D003E0000197B0030007D002C002000500054003A0020007B0031007D00003555006E006B006E006F0077006E002000500072006F00760069006400650072004C0069007300740054007900700065003A00200000053A0020000019500072006F00760069006400650072004C0069007300740000114C006900730074005400790070006500000954007900700065000011500072006F007600690064006500720000094E0061006D00650000272000530045004C0045004300540020002000200043004F0055004E00540028002A00290020000031200020002000460052004F004D002000200020005400450052004D0049004E0041004C0053005F003300470053002000007D20002000200049004E004E004500520020004A004F0049004E0020005400450052004D0049004E0041004C00530020004F004E002000200054003300470053005F005400450052004D0049004E0041004C005F0049004400200020003D002000540045005F005400450052004D0049004E0041004C005F004900440000112000200057004800450052004500200000452000200054003300470053005F005400450052004D0049004E0041004C005F0049004400200020003D00200040005400650072006D0069006E0061006C0049006400200000772000530045004C004500430054002000200020004000490067006E006F00720065004D0061006300680069006E0065004E0075006D0062006500720020003D00200043004100530054002800470050005F004B00450059005F00560041004C0055004500200041005300200069006E007400290020000080A52000200057004800450052004500200020002000470050005F00470052004F00550050005F004B004500590020003D002000270049006E0074006500720066006100630065003300470053002700200041004E0044002000470050005F005300550042004A004500430054005F004B004500590020003D002700490067006E006F00720065004D0061006300680069006E0065004E0075006D006200650072002700200001332000530045004C00450043005400200020002000540045005F005400450052004D0049004E0041004C005F004900440020000047200020002000460052004F004D002000200020005400450052004D0049004E0041004C0053005F003300470053002C0020005400450052004D0049004E0041004C00530020000063200020005700480045005200450020002000200054003300470053005F00560045004E0044004F0052005F00490044002000200020002000200020002000200020002000200020003D00200040007000560065006E0064006F0072004900640020000080A9200020002000200020002000200020002000200041004E00440020002800200054003300470053005F00530045005200490041004C005F004E0055004D00420045005200200020003D00200040007000530065007200690061006C004E0075006D00620065007200200020004F00520020004000490067006E006F0072006500530065007200690061006C004E0075006D00620065007200200020003D00200031002000290020000080A9200020002000200020002000200020002000200041004E00440020002800200054003300470053005F004D0041004300480049004E0045005F004E0055004D0042004500520020003D002000400070004D0061006300680069006E0065004E0075006D0062006500720020004F00520020004000490067006E006F00720065004D0061006300680069006E0065004E0075006D0062006500720020003D0020003100200029002000006B200020002000200020002000200020002000200041004E004400200054003300470053005F005400450052004D0049004E0041004C005F00490044002000200020002000200020003D002000540045005F005400450052004D0049004E0041004C005F00490044002000001540007000560065006E0064006F00720049006400001D40007000530065007200690061006C004E0075006D0062006500720000274000490067006E006F0072006500530065007200690061006C004E0075006D00620065007200001F400070004D0061006300680069006E0065004E0075006D0062006500720000294000490067006E006F00720065004D0061006300680069006E0065004E0075006D00620065007200001F20004900460020004E004F00540020004500580049005300540053002000000720002800200000192000530045004C00450043005400200020002000310020000039200020002000460052004F004D002000200020005400450052004D0049004E0041004C0053005F00500045004E00440049004E0047002000004F2000200057004800450052004500200020002000540050005F0053004F0055005200430045002000200020002000200020002000200020003D0020004000700053006F00750072006300650020000053200020002000200041004E004400200020002000540050005F00560045004E0044004F0052005F00490044002000200020002000200020003D00200040007000560065006E0064006F007200490064002000005B200020002000200041004E004400200020002000540050005F00530045005200490041004C005F004E0055004D00420045005200200020003D00200040007000530065007200690061006C004E0075006D0062006500720020000007200029002000005B20002000200049004E005300450052005400200049004E0054004F0020005400450052004D0049004E0041004C0053005F00500045004E00440049004E004700200028002000540050005F0053004F005500520043004500200000612000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000540050005F00560045004E0044004F0052005F0049004400200000692000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000540050005F00530045005200490041004C005F004E0055004D004200450052002000006F2000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000540050005F004D0041004300480049004E0045005F004E0055004D00420045005200200029002000005920002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000560041004C005500450053002000280020004000700053006F0075007200630065002000005D2000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C00200040007000560065006E0064006F00720049006400200000652000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C00200040007000530065007200690061006C004E0075006D006200650072002000006B2000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000400070004D0061006300680069006E0065004E0075006D00620065007200200029002000000D200045004C00530045002000003D2000200020005500500044004100540045002000200020005400450052004D0049004E0041004C0053005F00500045004E00440049004E00470020000061200020002000200020002000530045005400200020002000540050005F004D0041004300480049004E0045005F004E0055004D0042004500520020003D002000400070004D0061006300680069006E0065004E0075006D0062006500720020000053200020002000200057004800450052004500200020002000540050005F0053004F0055005200430045002000200020002000200020002000200020003D0020004000700053006F0075007200630065002000005720002000200020002000200041004E004400200020002000540050005F00560045004E0044004F0052005F00490044002000200020002000200020003D00200040007000560065006E0064006F007200490064002000005F20002000200020002000200041004E004400200020002000540050005F00530045005200490041004C005F004E0055004D00420045005200200020003D00200040007000530065007200690061006C004E0075006D00620065007200200000114000700053006F007500720063006500002B530079007300740065006D00440065006600610075006C00740053006100730046006C006100670073000080832000530045004C00450043005400200020002000540045005F005400450052004D0049004E0041004C005F004900440020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000008083200020002000200020002000200020002C002000540045005F00500052004F0056005F0049004400200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000008083200020002000200020002000200020002C002000540045005F005400450052004D0049004E0041004C005F005400590050004500200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000008083200020002000200020002000200020002C002000540045005F004E0041004D004500200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000008083200020002000200020002000200020002C002000540045005F00500052004F00560049004400450052005F004900440020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000008083200020002000200020002000200020002C002000540045005F0042004C004F0043004B0045004400200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000008083200020002000200020002000200020002C002000540045005F005300540041005400550053002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000008083200020002000200020002000200020002C002000540045005F00430055005200520045004E0054005F0050004C00410059005F00530045005300530049004F004E005F004900440020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000008083200020002000200020002000200020002C002000540045005F00430055005200520045004E0054005F004100430043004F0055004E0054005F00490044002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000008083200020002000200020002000200020002C002000500056005F0050004F0049004E00540053005F004D0055004C005400490050004C0049004500520020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000008083200020002000200020002000200020002C00200043004100530054002800490053004E0055004C004C0020002800200028002000530045004C0045004300540020002000200031002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000808320002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000460052004F004D002000200020005400450052004D0049004E0041004C005F00470052004F0055005000530020002000200020002000200020002000200020002000200020002000200020002000200020002000008083200020002000200020002000200020002000200020002000200020002000200020002000200020002000200057004800450052004500200020002000540047005F0045004C0045004D0045004E0054005F00540059005000450020003D0020004000700045006C0065006D0065006E0074005400790070006500200020002000200000808320002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200041004E004400200020002000540047005F005400450052004D0049004E0041004C005F004900440020003D002000540045005F005400450052004D0049004E0041004C005F0049004400200020002000200000808320002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200041004E004400200020002000540047005F0045004C0045004D0045004E0054005F004900440020003D0020003100200029002C0020003000200029002000410053002000420049005400290020002000200000808109002000200020002000200020002000200041005300200053004900540045005F004A00410043004B0050004F0054002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000008083200020002000200020002000200020002C002000500056005F004F004E004C0059005F00520045004400450045004D00410042004C0045002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000008083200020002000200020002000200020002C002000540045005F005300410053005F0046004C004100470053002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000008083200020002000200020002000200020002C002000540045005F005300410053005F0046004C004100470053005F005500530045005F0053004900540045005F00440045004600410055004C00540020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000008083200020002000200020002000200020002C002000490053004E0055004C004C0020002800200028002000530045004C00450043005400200020002000430041005300540020002800470050005F004B00450059005F00560041004C0055004500200041005300200049004E005400290020002000200020002000200020002000200000808320000900200020002000200020002000200020002000200020002000200020002000200020002000200020002000460052004F004D00200020002000470045004E004500520041004C005F0050004100520041004D005300200020002000200020002000200020002000200020002000200020002000200020002000200020002000008083200009002000200020002000200020002000200020002000200020002000200020002000200020002000200057004800450052004500200020002000470050005F00470052004F00550050005F004B00450059002000200020003D00200027004300720065006400690074007300270020002000200020002000200020002000200001808320000900200020002000200020002000200020002000200020002000200020002000200020002000200020002000200041004E004400200020002000470050005F005300550042004A004500430054005F004B004500590020003D002000270050006C00610079004D006F00640065002700200020002000200020002000200020000180810900200020002000200020002000200020002000200020002000200020002000200020002000200020002000200041004E004400200020002000490053004E0055004D0045005200490043002800470050005F004B00450059005F00560041004C0055004500290020003D0020003100200029002C0020003000200029002000008081090020002000200020002000200020002000410053002000530059005300540045004D005F0050004C00410059005F004D004F004400450020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000808109002000200020002000200020002C002000540045005F00450058005400450052004E0041004C005F00490044002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000808109002000200020002000200020002C002000540045005F0046004C004F004F0052005F004900440020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000008097200020002000200020002000200020002C0020002800530045004C00450043005400200042004B005F004E0041004D0045002000460052004F004D002000420041004E004B005300200057004800450052004500200042004B005F00420041004E004B005F004900440020003D002000540045005F00420041004E004B005F004900440029002000410053002000420041004E004B00008083200020002000200020002000200020002C002000540045005F00490053004F005F0043004F004400450020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000008083200020002000460052004F004D002000200020005400450052004D0049004E0041004C00530020004C0045004600540020004A004F0049004E002000500052004F0056004900440045005200530020004F004E002000500056005F004900440020003D002000540045005F00500052004F0056005F0049004400200020002000200000472000200057004800450052004500200020002000540045005F004E0041004D00450020003D002000400070005400650072006D0069006E0061006C004E0061006D006500200000612000200057004800450052004500200020002000540045005F00450058005400450052004E0041004C005F004900440020003D002000400070005400650072006D0069006E0061006C00450078007400650072006E0061006C0049006400200000512000200057004800450052004500200020002000540045005F005400450052004D0049004E0041004C005F004900440020003D002000400070005400650072006D0069006E0061006C00490064002000001B4000700045006C0065006D0065006E00740054007900700065000029400070005400650072006D0069006E0061006C00450078007400650072006E0061006C0049006400003B20002000530045004C00450043005400200020002000540045005F004E0041004D00450020002000200020002000200020002000200020002000003B2000200020002000200020002000200020002C002000540045005F0042004100530045005F004E0041004D004500200020002000200020002000003B2000200020002000200020002000200020002C002000540045005F0046004C004F004F0052005F00490044002000200020002000200020002000003B2000200020002000460052004F004D002000200020005400450052004D0049004E0041004C005300200020002000200020002000200020002000005520002000200057004800450052004500200020002000540045005F005400450052004D0049004E0041004C005F00490044002000200020003D002000400070005400650072006D0069006E0061004900640020000017400070005400650072006D0069006E00610049006400000B25004E0061006D0065000013250042006100730065004E0061006D0065000011250046006C006F006F0072004900640000214900460020004E004F00540020004500580049005300540053002000280020000023200020002000200020002000530045004C0045004300540020002000200031002000003720002000200020002000200020002000460052004F004D00200020002000470041004D0045005F004D00450054004500520053002000006120002000200020002000200020005700480045005200450020002000200047004D005F005400450052004D0049004E0041004C005F004900440020002000200020003D002000400070005400650072006D0069006E0061006C00490064002000006520002000200020002000200020002000200041004E00440020002000200047004D005F00470041004D0045005F0042004100530045005F004E0041004D00450020003D00200040007000470061006D00650042006100730065004E0061006D0065002000001120002000200020002000200029002000003B20002000200049004E005300450052005400200049004E0054004F00200020002000470041004D0045005F004D0045005400450052005300200000412000200020002000200020002000200020002000200020002000200020002800200047004D005F005400450052004D0049004E0041004C005F0049004400200000472000200020002000200020002000200020002000200020002000200020002C00200047004D005F00470041004D0045005F0042004100530045005F004E0041004D004500200000492000200020002000200020002000200020002000200020002000200020002C00200047004D005F005700430050005F00530045005100550045004E00430045005F0049004400200000432000200020002000200020002000200020002000200020002000200020002C00200047004D005F00440045004E004F004D0049004E004100540049004F004E00200000432000200020002000200020002000200020002000200020002000200020002C00200047004D005F0050004C0041005900450044005F0043004F0055004E005400200000452000200020002000200020002000200020002000200020002000200020002C00200047004D005F0050004C0041005900450044005F0041004D004F0055004E0054002000003D2000200020002000200020002000200020002000200020002000200020002C00200047004D005F0057004F004E005F0043004F0055004E0054002000003F2000200020002000200020002000200020002000200020002000200020002C00200047004D005F0057004F004E005F0041004D004F0055004E005400200000472000200020002000200020002000200020002000200020002000200020002C00200047004D005F004A00410043004B0050004F0054005F0041004D004F0055004E005400200000492000200020002000200020002000200020002000200020002000200020002C00200047004D005F00440045004C00540041005F00470041004D0045005F004E0041004D0045002000004F2000200020002000200020002000200020002000200020002000200020002C00200047004D005F00440045004C00540041005F0050004C0041005900450044005F0043004F0055004E005400200000512000200020002000200020002000200020002000200020002000200020002C00200047004D005F00440045004C00540041005F0050004C0041005900450044005F0041004D004F0055004E005400200000492000200020002000200020002000200020002000200020002000200020002C00200047004D005F00440045004C00540041005F0057004F004E005F0043004F0055004E0054002000004B2000200020002000200020002000200020002000200020002000200020002C00200047004D005F00440045004C00540041005F0057004F004E005F0041004D004F0055004E005400200000452000200020002000200020002000200020002000200020002000200020002C00200047004D005F004C004100530054005F005200450050004F0052005400450044002000002320002000200020002000200020002000200020002000200020002000200029002000001F20002000200020002000200020002000560041004C005500450053002000003D20002000200020002000200020002000200020002000200020002000200028002000400070005400650072006D0069006E0061006C0049006400200000412000200020002000200020002000200020002000200020002000200020002C00200040007000470061006D00650042006100730065004E0061006D006500200000432000200020002000200020002000200020002000200020002000200020002C00200040007000570063007000530065007100750065006E006300650049006400200000412000200020002000200020002000200020002000200020002000200020002C00200040007000440065006E006F006D0069006E006100740069006F006E00200000492000200020002000200020002000200020002000200020002000200020002C00200040007000440065006C007400610050006C00610079006500640043006F0075006E0074002000004B2000200020002000200020002000200020002000200020002000200020002C00200040007000440065006C007400610050006C00610079006500640041006D006F0075006E007400200000432000200020002000200020002000200020002000200020002000200020002C00200040007000440065006C007400610057006F006E0043006F0075006E007400200000452000200020002000200020002000200020002000200020002000200020002C00200040007000440065006C007400610057006F006E0041006D006F0075006E0074002000004D2000200020002000200020002000200020002000200020002000200020002C00200040007000440065006C00740061004A00610063006B0070006F00740041006D006F0075006E007400200000372000200020002000200020002000200020002000200020002000200020002C0020004700450054004400410054004500280029002000001F2000200020002000200020002000200020002000200020002000200020000031200020002000550050004400410054004500200020002000470041004D0045005F004D00450054004500520053002000006B20002000200020002000200053004500540020002000200047004D005F005700430050005F00530045005100550045004E00430045005F0049004400200020002000200020003D00200040007000570063007000530065007100750065006E0063006500490064002000006920002000200020002000200020002000200020002C00200047004D005F00440045004E004F004D0049004E004100540049004F004E00200020002000200020002000200020003D00200040007000440065006E006F006D0069006E006100740069006F006E0020000080A520002000200020002000200020002000200020002C00200047004D005F0050004C0041005900450044005F0043004F0055004E005400200020002000200020002000200020003D00200047004D005F0050004C0041005900450044005F0043004F0055004E0054002000200020002000200020002000200020002B00200040007000440065006C007400610050006C00610079006500640043006F0075006E00740020000080A720002000200020002000200020002000200020002C00200047004D005F0050004C0041005900450044005F0041004D004F0055004E00540020002000200020002000200020003D00200047004D005F0050004C0041005900450044005F0041004D004F0055004E005400200020002000200020002000200020002B00200040007000440065006C007400610050006C00610079006500640041006D006F0075006E007400200000809F20002000200020002000200020002000200020002C00200047004D005F0057004F004E005F0043004F0055004E005400200020002000200020002000200020002000200020003D00200047004D005F0057004F004E005F0043004F0055004E0054002000200020002000200020002000200020002000200020002B00200040007000440065006C007400610057006F006E0043006F0075006E00740020000080A120002000200020002000200020002000200020002C00200047004D005F0057004F004E005F0041004D004F0055004E00540020002000200020002000200020002000200020003D00200047004D005F0057004F004E005F0041004D004F0055004E005400200020002000200020002000200020002000200020002B00200040007000440065006C007400610057006F006E0041006D006F0075006E0074002000006920002000200020002000200020002000200020002C00200047004D005F00440045004C00540041005F00470041004D0045005F004E0041004D004500200020002000200020003D00200040007000470061006D00650042006100730065004E0061006D00650020000080A520002000200020002000200020002000200020002C00200047004D005F00440045004C00540041005F0050004C0041005900450044005F0043004F0055004E005400200020003D00200047004D005F00440045004C00540041005F0050004C0041005900450044005F0043004F0055004E0054002000200020002B00200040007000440065006C007400610050006C00610079006500640043006F0075006E00740020000080A720002000200020002000200020002000200020002C00200047004D005F00440045004C00540041005F0050004C0041005900450044005F0041004D004F0055004E00540020003D00200047004D005F00440045004C00540041005F0050004C0041005900450044005F0041004D004F0055004E005400200020002B00200040007000440065006C007400610050006C00610079006500640041006D006F0075006E007400200000809F20002000200020002000200020002000200020002C00200047004D005F00440045004C00540041005F0057004F004E005F0043004F0055004E005400200020002000200020003D00200047004D005F00440045004C00540041005F0057004F004E005F0043004F0055004E0054002000200020002000200020002B00200040007000440065006C007400610057006F006E0043006F0075006E00740020000080A120002000200020002000200020002000200020002C00200047004D005F00440045004C00540041005F0057004F004E005F0041004D004F0055004E00540020002000200020003D00200047004D005F00440045004C00540041005F0057004F004E005F0041004D004F0055004E005400200020002000200020002B00200040007000440065006C007400610057006F006E0041006D006F0075006E00740020000080A920002000200020002000200020002000200020002C00200047004D005F004A00410043004B0050004F0054005F0041004D004F0055004E0054002000200020002000200020003D00200047004D005F004A00410043004B0050004F0054005F0041004D004F0055004E00540020002000200020002000200020002B00200040007000440065006C00740061004A00610063006B0070006F00740041006D006F0075006E0074002000005F20002000200020002000200020002000200020002C00200047004D005F004C004100530054005F005200450050004F00520054004500440020002000200020002000200020003D0020004700450054004400410054004500280029002000006520002000200020005700480045005200450020002000200047004D005F005400450052004D0049004E0041004C005F00490044002000200020002000200020002000200020003D002000400070005400650072006D0069006E0061006C00490064002000006920002000200020002000200041004E00440020002000200047004D005F00470041004D0045005F0042004100530045005F004E0041004D0045002000200020002000200020003D00200040007000470061006D00650042006100730065004E0061006D00650020000080C12000200055005000440041005400450020002000200043004100530048004900450052005F00530045005300530049004F004E0053002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000080C120002000200020002000530045005400200020002000430053005F0053005400410054005500530020003D00200040007000500065006E00640069006E00670043006C006F00730069006E0067005300740061007400750073002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000080C120002000200057004800450052004500200020002000430053005F0043004100530048004900450052005F004900440020003D0020002800200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000080C12000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000530045004C00450043005400200020002000430054005F0043004100530048004900450052005F004900440020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000080C1200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000460052004F004D0020002000200043004100530048004900450052005F005400450052004D0049004E0041004C0053002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000080C12000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200057004800450052004500200020002000430054005F005400450052004D0049004E0041004C005F004900440020003D002000400070005400650072006D0069006E0061006C00490064002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000080C12000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002900200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000080C12000200020002000200041004E004400200020002000430053005F0053005400410054005500530020003D002000400070004F00700065006E0053007400610074007500730020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000080C12000200020002000200041004E004400200020002000430053005F00530045005300530049004F004E005F004900440020003C003E002000280020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000080C12000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000530045004C0045004300540020002000200054004F005000200031002000430053005F00530045005300530049004F004E005F004900440020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000080C1200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000460052004F004D0020002000200043004100530048004900450052005F00530045005300530049004F004E00530020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000080C12000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200057004800450052004500200020002000430053005F0043004100530048004900450052005F004900440020003D00200028002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000080C1200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000530045004C00450043005400200020002000430054005F0043004100530048004900450052005F0049004400200020002000200020002000200020002000200020002000200020002000200020000080C120002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000460052004F004D0020002000200043004100530048004900450052005F005400450052004D0049004E0041004C00530020002000200020002000200020002000200020002000200020000080C1200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200057004800450052004500200020002000430054005F005400450052004D0049004E0041004C005F004900440020003D002000400070005400650072006D0069006E0061006C004900640020000080C12000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200029002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000080C1200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200041004E004400200020002000430053005F0053005400410054005500530020003D002000300020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000080C1200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020004F005200440045005200200042005900200020002000430053005F004F00500045004E0049004E0047005F004400410054004500200044004500530043002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000080C1200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200029002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000002D40007000500065006E00640069006E00670043006C006F00730069006E0067005300740061007400750073000019400070004F00700065006E00530074006100740075007300008095530045004C004500430054002000540045005F005400450052004D0049004E0041004C005F0054005900500045002000460052004F004D0020005400450052004D0049004E0041004C0053002000570048004500520045002000540045005F005400450052004D0049004E0041004C005F004900440020003D002000400070005400650072006D0069006E0061006C004900640000033000000D7B0030003A00440034007D00000D7B0030003A00440032007D00000D7B0030003A00440039007D00001769006E0070007500740042007500660066006500720000495400720061006E00730066006F0072006D0042006C006F0063006B003A00200049006E00700075007400200062007500660066006500720020006900730020006E0075006C006C0000196F0075007400700075007400420075006600660065007200004B5400720061006E00730066006F0072006D0042006C006F0063006B003A0020004F0075007400700075007400200062007500660066006500720020006900730020006E0075006C006C00004F5400720061006E00730066006F0072006D0042006C006F0063006B003A00200050006100720061006D006500740065007200730020006F007500740020006F0066002000720061006E0067006500004742006C006F0063006B00530069007A0065003A0020004500720072006F007200200049006E00760061006C0069006400200042006C006F0063006B002000530069007A0065000033490056003A0020004500720072006F007200200049006E00760061006C00690064002000490056002000530069007A006500003F4D006F00640065003A0020004500720072006F007200200049006E00760061006C0069006400200043006900700068006500720020004D006F00640065000047500061006400640069006E0067003A0020004500720072006F007200200049006E00760061006C00690064002000500061006400640069006E00670020004D006F0064006500000F41005200430046004F0055005200000F61006C0067004E0061006D006500002F4300720065006100740065003A0020004500720072006F0072005F0050006100720061006D004E0075006C006C000007520043003400000D5200670062004B0065007900003F43007200650061007400650044006500630072007900700074006F0072003A0020005200670062006B006500790020006900730020006E0075006C006C00004343007200650061007400650044006500630072007900700074006F0072003A00200049006E00760061006C006900640020006B00650079002000730069007A006500004143007200650061007400650044006500630072007900700074006F0072003A00200049006E00760061006C00690064002000490056002000730069007A006500003B42006C006F0063006B00530069007A0065003A00200049006E00760061006C0069006400200062006C006F0063006B002000730069007A006500003143007200650061007400650044006500630072007900700074006F0072003A00200044006900730070006F0073006500000D7200670062004B0065007900003F43007200650061007400650044006500630072007900700074006F0072003A0020007200620067006B006500790020006900730020006E0075006C006C00004343007200650061007400650044006500630072007900700074006F0072003A00200049006E00760061006C006900640020004B00650079002000530069007A006500004143007200650061007400650044006500630072007900700074006F0072003A00200049006E00760061006C00690064002000490056002000530069007A006500002F63006F006E007400650078007400200063006F006E006E0065006300740069006F006E003D00740072007500650000315400720078005F003300470053005F00530074006100720074004300610072006400530065007300730069006F006E0000174200690065006E00760065006E00690064006F002000005B5400720078005F003300470053005F00530074006100720074004300610072006400530065007300730069006F006E0020006600610069006C006500640021002C00200045007800630065007000740069006F006E003A00200000335400720078005F003300470053005F005500700064006100740065004300610072006400530065007300730069006F006E00005D5400720078005F003300470053005F005500700064006100740065004300610072006400530065007300730069006F006E0020006600610069006C006500640021002C00200045007800630065007000740069006F006E003A002000002D5400720078005F003300470053005F0045006E0064004300610072006400530065007300730069006F006E0000575400720078005F003300470053005F0045006E0064004300610072006400530065007300730069006F006E0020006600610069006C006500640021002C00200045007800630065007000740069006F006E003A002000002B5400720078005F003300470053005F004100630063006F0075006E00740053007400610074007500730000555400720078005F003300470053005F004100630063006F0075006E00740053007400610074007500730020006600610069006C006500640021002C00200045007800630065007000740069006F006E003A00200000235400720078005F003300470053005F00530065006E0064004500760065006E007400004D5400720078005F003300470053005F00530065006E0064004500760065006E00740020006600610069006C006500640021002C00200045007800630065007000740069006F006E003A002000000000A713965DFF1954459EC241D1A8227E440008B77A5C561934E0890B000512811C0E0E080E121D1100081281340E0E080E0A1281241121121D0A000412811C0E0E08121D0E000712811C0E0E0E080A1121121D1300061281341281781281480A1281241121121D0C00040112811C1008100E100E0C0004011281341008100E100E072002011218121D08200401080E0A121D03200001030612250306121802060802060E02060A042001010A052002010A0E052001011218072003011218080E06200301080E0A07200401080E0A0E0F2007020A0A11341121112111211121102008020A0A113411211121112111210A102008020A0A113411211121112111210E12200A020A0A113411211121112111210E080E13200B020A0A113411211121112111210E080E0E14200C020A0A113411211121112111210E080E0E0A0600020E0A121D0320000A0420010A0805200102121D0328000A020602030611290306112C04200012180306122D0520010112250620020112180E0A2005020A113011210A0E0B2006020A113011210A0E0E0C2007020A113011210A0E0E020C2007020A113011210A0E0E0E0D2008020A113011210A0E0E0E020F2009020A113011210A0E0E0E1121020E2008020A113011210A0E0E0E112110200A020A113011210A0E0E0E11210A0A11200B020A113011210A0E0E0E11210A0A0212200B020A113011210A0E0E0E11210A0A112113200C020A113011210A0E0E0E11210A0A11210226200F020A113011210A0E0E0E11210A0A112115113101118090151131011180D8151131010A02092004020A0A0E1280AC0B2005020A0A0E1280AC11210A2005020A0A0E1280AC0E0D2006020A0A0E1280AC113011210520010111210520010111290720020111211121032000081000060111301121101121101121100E020B00041121113011211121020428001218042800122503061120040000000004010000000402000000040400000004080000000410000000042000000004400000000480000000040001000004000200000400040000040008000004001000000400200000040040000004008000000400000100030611240403000000040500000004060000000407000000030611280409000000040A000000040B00000004FFFFFFFF03061130040C000000040D000000040E000000041400000004150000000416000000041700000004180000000419000000041C000000041D000000041E000000041F000000042100000004220000000423000000042400000004250000000426000000042700000004280000000429000000042A000000042B000000042C000000042D000000042E000000042F0000000430000000043100000004320000000433000000043400000004350000000436000000043700000004380000000439000000043A000000043B000000043C000000043D000000043E000000043F000000044100000004420000000443000000044400000004450000000446000000044700000004480000000449000000044A000000044B000000044C000000044D000000044E000000044F0000000450000000045400000004550000000456000000045700000004580000000459000000045A000000045B000000045C000000045D000000045E000000045F0000000460000000046100000004620000000463000000046400000004650000000466000000046700000004680000000469000000046A000000046B000000046C000000046D000000046E000000046F0000000470000000047100000004720000000473000000047400000004750000000476000000047700000004780000000479000000047A000000047B000000047C000000047D000000047E000000047F000000048B000000048C000000048D000000048E000000048F0000000490000000049100000004920000000493000000049400000004950000000496000000049700000004980000000499000000049A000000049B000000049C000000049D000000049E000000049F00000004A000000004A100000004BE00000004BF00000004C800000004C900000004CA00000004CB00000004CC00000004CE00000004CF00000004D0000000042B010000042C010000042D010000042E010000042F01000004300100000431010000043201000004330100000434010000043501000004360100000437010000048F01000004F401000004F501000004FE01000004FF010000040102000004020200000403020000040402000004050200000406020000040702000004080200000409020000040A020000040B020000040C020000040D020000040E020000040F0200000410020000041102000004120200000413020000041402000004150200000416020000041702000004180200000419020000041A020000041B020000041C020000041D020000041E020000041F0200000420020000042102000004220200000423020000042402000004250200000426020000042702000004280200000429020000042A020000042B020000042D020000042F020000043002000004320200000433020000043402000004350200000436020000043702000004380200000439020000043A020000043B020000043C020000043D020000043E020000043F020000044002000004410200000442020000044302000004440200000445020000044602000004470200000448020000044A020000044B020000044C020000044D020000044E020000044F0200000450020000045102000004520200000453020000045402000004550200000456020000045702000004580200000459020000045A020000045B020000045C020000045D020000045E020000045F0200000460020000046102000004620200000463020000046402000004650200000466020000046702000004680200000469020000044C04000004AF04000004B0040000041305000004140500000477050000047805000004DB05000004DC050000043F0600000440420F000480841E0004BFC62D0003061134041A000000041B0000000452000000045300000004CD00000004D100000004D200000004D300000004D400000004D500000004D600000004D700000004D800000004D900000004DA00000004DB00000004DC00000004DD00000004DE00000004DF00000004E000000004E100000004E200000004E300000004E400000004E500000004E600000004E700000004E800000004E900000004EA00000004EB00000004EC00000004ED00000004EE00000004EF00000004F000000004F100000004E80300000306113804900100000306113C0306114003061144030611480306114C0306115003061154030611580306115C040000020004000004000400000800040000100004000020000400004000040000800003061160030611640306116804E703000004E903000004F20300000306116C0306117003061174030611780306117C0406118080040F00000004061180840406118088040611808C040611809004EA03000004EB03000004061180940406118098041100000004120000000413000000048100000004820000000483000000048400000004850000000486000000048700000004880000000489000000048A000000040611809C04061180A004061180A404061180A80306112104061280B004200011210320000E042001010E04200101080320000204200101020520001180900620010111809005200011808C0620010111808C0520001280B0062001011280B004280011210328000E032800080328000205280011809005280011808C0528001280B0042000112905200011809406200101118094042800112905280011809404061180B404061180B804061180BC04061180C004061180C404061180C804061180CC04061180D004061180D404061180D804061180DC04061180E004061180E804061180EC04061180F004061180F404061180F804061180FC0406118100040611810404061181080800000000000000800F00050211814C0A101281401008121D0A000212811C128118121D0F000512811C0A12817812814802121D16000812811C0A1281781281480211811412814002121D19000B021281480A0811640E02118114128140021012811C121D0C00050211210E0E101121121D0E0005021281400E0E10128140121D090005020A080A02121D080004020A080A121D160009020A08116402118114128140100A10118164121D0E0007020A113C0E081164100A121D070003020A0A121D1300080211680A081164128140128140100A121D0F00050211640A12812410128124121D100006020811640E12812010128134121D0C0004020A128140128140121D080003020A1002121D0B00030212813C128134121D0B0005020A0A08128140121D0F000502113512813C1281341164121D0B000302128178128140121D120006020A128140128140128140128140121D120008020A112111211121112111211121121D060002020A121D110006020A0A10112110112110128140121D080004020A0A0A121D0A0004020A11211121121D0800030211210A121D100007020A0A0A0A12814010128140121D0800030210080A121D140009020A0A0A0A1281400210128140101225121D0A0003020A10128140121D0900020210118174121D0A0003021281381239121D0A0003020A10128138121D0800011281381281200B0004020A1121101121121D0C0005020A112102101121121D120006020A128140112110128140101121121D150007020A12814011211180F810128140101121121D0D0004020A12814010128140121D0F0005020A12814010128140100A121D120006020A12814010128140101121100A121D130006020A12814010128140100A10128140121D180008020A128140112110128140101121100A10128140121D190009020A12814011210210128140101121100A10128140121D110007020A0E12814011210210128148121D140008020A0E1281401121021180F810128148121D080003020A115C121D0B000502080A1002100A121D190008021181640A0A128140101281401012814010128140121D0B000502020A12814002121D0C000602020A1281400202121D140008020A1281781121112111211011211002121D070002021008121D0A0005020A0E0E1164121D0B0007020A0A0E090E08121D0C0005020A1281400A1225121D0E000602080A1281401002100A121D0D0004020A1121118154101181580E0005020A11211181540210118158130007020A11211181540210118158101121121D0700020112813802060001011281380800020111241281381100060211440A101144101144101135121D0D0006020A11211121021134121D080003020A1239121D110009020A11210A0811640E0E101239121D090003020A128120121D0A0004020A11701121121D07000302080A121D140007021281781121113512813C1281341002121D0700030E0E0E121D04061181100406118114040612814003061239072002011181100E04061281240406128128040612812C0406118130072002011181300E06200101128140092003011121112111210C20050111211121112111210210200701112111211121112102112111210500001281400520001281400B000212814012814012814009000202128140128140042001021C080001128140128140090002128140128140020320001C050800128140052800128140062001011281440820020112814011210500001281440B000212814412814412814409000202128144128144050800128144040611814C040611815004061181540406118158040611815C040611816004061181640306123D05200011816C0620010111816C0720040E0E0E0E0E042001020E052001122D0E0720020E123D114108200301123D0E114505280011816C040611816C0600020208121D0C0005020E0E0810128178121D09000502080E0E08121D0A0003020810128178121D0A0003020E10128178121D0C000502080E0E10128178121D09000402080E100E121D060002020811200600011249121D0900030208121D10116404061181740520010211200400010E0E04F526000003061D05080003020E100E100807000302100E0E080500020E080A080003010E1008100A05000202080E090003021181840E100E0406118184020605052001011D050A2005081D0508081D05080820031D051D0508080420001D050520001D124D0420001151052001011151042000115505200101115505000012818C06000112818C0E0428001D050528001D124D0428001151042800115508200212111D051D050406128190120009010E0E080E100A1011211008100E100E18000E010E0E080E0A112111210808112111211008100E100E0F0007010E0E081011211008100E100E100009010E0E0E080A11211008100E100E0100062001011180910600030E1C1C1C0F070512811C12817812814812811C020F0705128134128178128148128134021207061281341281781281481281201281340205200112390E0520001280A5092003010E1280A5121D0520001280A90920021280AD0E1180B1042001011C16070A12811C1281781281480812390A124912811C020A0700040E0E1C1C1C0600030E0E1C1C07000202112111210600020E0E1D1C080002112111211121070002020E101121060001112111210A20031280AD0E1180B1081A070C128134128124112111211121112102021249128134021D1C0507011181100507011181300607021210113403070102040001020E0520001280C90600011280CD0E0920021280D10E1280CD0620011280D10E072001011D1280D10907021280D11D1280D10600030E0E0E0E042000122D052002010E1C04061280D50520001280D905200101122D060703122D02020907060E1C0E12490E02052001122D080420011C0E0407020A02060002020E10080520001280E5062001011180E9062001011180ED0520010112490520010812251A070E12390802081280AD0A0E122D12491280F102021280E512150807040A11210A121C0407011218071511310111809005200101130007151131011180D805151131010A08070202151131010A050002020E0E0A070511211121112102020F070711301130020211809011808C02060703113002020D0706113002021180900211808C060703112102020420001D1C0507030808020520001280F90520011121083707191239081280AD1280AD1280AD1280AD1280AD1280AD0E11300A0E0E02122D12491280F91280F11281011280DD02021280E5121511300607030E1130020907041121112111300204070111210307010A0307010E0307010805070111809005070111808C0507011280B004070111290507011180940500020E0E0E0420011C08240710123912251281400E11280A12491280F11280AD122D1280F9020211814C1280E5121505070112811C0A070412811C0812811C020420010E08042001020804200108080500010E1D0E0700040E0E0E0E0E58072C0A128140128140128140128140128140121011816411211121020A12814012814012390A113C020202128148112111211180F8128140020E0A020212491280F912491280F90A1225112111210E1280DD02021D0E1121042001080E0B0705123912491280F902020507030202020807041239124902022707171239080A113C11440E0E020211441144113508081121112111211121112112491280F902020F0707113812391280AD124902113C020607031249020213070912391280AD1138114411211249020211680500020A0A0A2B0713128140128140020212813C11211281381281380202128124112111211281381281401280DD020211640D07071239081C12491280DD02022F07181281401281401281401281401281401210114411441144081281781135020A0A08021C12491280DD0A0202113422070D12814012814012813812391281401281400212817812814012814012814002020320000D042000123917070A1239122512491280F11280AD122D02021280E5121519070C12391225020812491280F11280AD122D02021280E512150A070612391C0A1249020205070212250205200201081C200710123911241121020E122D0812491280F91280AD122D02021280E511241215090704128138123902020B0705123912490202118174062001011281110A000311211121081181150500020E0E1C0A20031D122D0E0E1181190420001249062001081D122D34071712391D122D1181740212491280F911211121112111211280F108122D1280AD1280DD02021181741D122D081D1C1280E5121510070612813811211121112111211281380A07041281401281400A020707031281400A02070703128148020209070412814011210A02080703112112814002060702128140020507021121020520020E080316070E12390E0808080802020212491280F91280DD020231071712390A0A0E12814012814012814012814012814012814012814002114411440211351121112112491280F9121002021B070B12391281401281401281401281400A12491280F902021181640420010E0E06200212390E0E05000111210E070002112111210813070A1121112111211239080812491280F9020206200101118129072002010E1180B10820011280AD1280AD0620011280AD0E1407091181500A0E12491280AD1280AD02021181501D070D12390A1121112112491280AD1280AD1280AD122D02021280E5121517070D12391281400A0A0A0E021121121012491280F902020F07071281400A1239124902021181540C07051121122D1280E50212150E070611211121122D1280E5021215052001112908062001113511290F070712391129112912491280F902020707040E124902020B0705123912491280DD02020F070612812012813412101280DD02020D0707123908124912490211700209070512390812490202060002020E100605000111210806200212390E1C05000111210D07200212390E1D1C0620021239080E1B070F080806061180C4112112391239123912491280F902020D11210A070612391C0E12490E020707030E1D1C11210507011281400607021281401C05070112814405070111816C0500020E1C1C1107080E1239122D0E11816C1280E5021215050702122D020607020211816C0407020E020520001280CD0620011280D1080520001281350B0703122D11816C1D1280D1072002020E1181390C0705122D122D1280E50212150820020112814111410807040E12813D0E0209200211451281491145060702128145020A07061C123912490802020A070612391C12490802020420010608110709123908080812491280F902021181740520020E0E0E0F070912390E0E0E0E12491280F90202080703123912491249090705021239124902020607040E020E020607040E080E02050002080E0E0400010B0E0500011D050B0B2003011281651211118169072003081D0508080600020B1D05080520020E0808040001080E2107151281940E0E0E0E090E0B0B0B1D051D050E12111D0512815D1281610802020B072003011D0508081B070F1281940E0E090B1D05121112815D128161081D051D0502020B0400010A0E08070608080E0A02020507030802020300000104061181A00900020112817911817D0407011D05021D05052002010E0E07070505050808020707031D051D050206070405080802080003011281790808040001011C062003010808080807021D124D1D124D040701115104070111550707021281991D0505070112818C080003020E0E11813906070212818C0205070212110204070112110507011D124D042000121D0E070512811C1280A5121D1280DD021107061281341281241280A5121D1280DD02190100145753492E53514C427573696E6573734C6F67696300000501000000000E0100094D6963726F736F667400002001001B436F7079726967687420C2A9204D6963726F736F6674203230313200002901002431306432653537662D303966322D343639642D396665312D35366639356262383932303600000C010007312E302E302E3000000801000701000000000801000800000000001E01000100540216577261704E6F6E457863657074696F6E5468726F777301000000002990CE5700000000020000001C010000C47A0400C45C0400525344535EB88BE15E245A43AE0198D813A2771101000000633A5C5446535C5769676F735C4D61696E5C5769676F732053797374656D5C5753492E53514C427573696E6573734C6F6769635C6F626A5C44656275675C53514C427573696E6573734C6F6769632E70646200000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000100100000001800008000000000000000000000000000000100010000003000008000000000000000000000000000000100000000004800000058800400480300000000000000000000480334000000560053005F00560045005200530049004F004E005F0049004E0046004F0000000000BD04EFFE00000100000001000000000000000100000000003F000000000000000400000002000000000000000000000000000000440000000100560061007200460069006C00650049006E0066006F00000000002400040000005400720061006E0073006C006100740069006F006E00000000000000B004A8020000010053007400720069006E006700460069006C00650049006E0066006F00000084020000010030003000300030003000340062003000000034000A00010043006F006D00700061006E0079004E0061006D006500000000004D006900630072006F0073006F00660074000000540015000100460069006C0065004400650073006300720069007000740069006F006E00000000005700530049002E00530051004C0042007500730069006E006500730073004C006F0067006900630000000000300008000100460069006C006500560065007200730069006F006E000000000031002E0030002E0030002E00300000004C001500010049006E007400650072006E0061006C004E0061006D0065000000530051004C0042007500730069006E006500730073004C006F006700690063002E0064006C006C00000000005C001B0001004C006500670061006C0043006F007000790072006900670068007400000043006F0070007900720069006700680074002000A90020004D006900630072006F0073006F006600740020003200300031003200000000005400150001004F0072006900670069006E0061006C00460069006C0065006E0061006D0065000000530051004C0042007500730069006E006500730073004C006F006700690063002E0064006C006C00000000004C0015000100500072006F0064007500630074004E0061006D006500000000005700530049002E00530051004C0042007500730069006E006500730073004C006F0067006900630000000000340008000100500072006F006400750063007400560065007200730069006F006E00000031002E0030002E0030002E003000000038000800010041007300730065006D0062006C0079002000560065007200730069006F006E00000031002E0030002E0030002E0030000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
WITH PERMISSION_SET = SAFE

GO

CREATE PROCEDURE [dbo].[Trx_3GS_UpdateCardSession]
	@VendorId [nvarchar](50),
	@SerialNumber [nvarchar](50),
	@MachineNumber [int],
	@ExternalTrackData [nvarchar](50),
	@PlaySessionId [bigint],
	@PlayedAmount [decimal](20, 2),
	@WonAmount [decimal](20, 2),
	@PlayedCount [int],
	@WonCount [int],
	@FinalBalance [decimal](20, 2),
	@BillIn [decimal](20, 2),
	@StatusCode [int] OUTPUT,
	@StatusText [nvarchar](max) OUTPUT,
	@StatusError [nvarchar](max) OUTPUT
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [SQLBusinessLogic].[WSI.Common.SqlProcedures].[Trx_3GS_UpdateCardSession]
GO

CREATE PROCEDURE [dbo].[Trx_3GS_StartCardSession]
	@VendorId [nvarchar](50),
	@SerialNumber [nvarchar](50),
	@MachineNumber [int],
	@ExternalTrackData [nvarchar](50),
	@PlaySessionId [bigint] OUTPUT,
	@PlayableBalance [decimal](20, 2) OUTPUT,
	@StatusCode [int] OUTPUT,
	@StatusText [nvarchar](max) OUTPUT,
	@StatusError [nvarchar](max) OUTPUT
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [SQLBusinessLogic].[WSI.Common.SqlProcedures].[Trx_3GS_StartCardSession]
GO

CREATE PROCEDURE [dbo].[Trx_3GS_SendEvent]
	@ExternalTrackData [nvarchar](50),
	@VendorId [nvarchar](50),
	@SerialNumber [nvarchar](50),
	@MachineNumber [int],
	@EventId [bigint],
	@Amount [decimal](20, 2),
	@StatusCode [int] OUTPUT,
	@StatusText [nvarchar](max) OUTPUT,
	@StatusError [nvarchar](max) OUTPUT
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [SQLBusinessLogic].[WSI.Common.SqlProcedures].[Trx_3GS_SendEvent]
GO

CREATE PROCEDURE [dbo].[Trx_3GS_EndCardSession]
	@VendorId [nvarchar](50),
	@SerialNumber [nvarchar](50),
	@MachineNumber [int],
	@ExternalTrackData [nvarchar](50),
	@PlaySessionId [bigint],
	@PlayedAmount [decimal](20, 2),
	@WonAmount [decimal](20, 2),
	@PlayedCount [int],
	@WonCount [int],
	@FinalBalance [decimal](20, 2),
	@BillIn [decimal](20, 2),
	@StatusCode [int] OUTPUT,
	@StatusText [nvarchar](max) OUTPUT,
	@StatusError [nvarchar](max) OUTPUT
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [SQLBusinessLogic].[WSI.Common.SqlProcedures].[Trx_3GS_EndCardSession]
GO

CREATE PROCEDURE [dbo].[Trx_3GS_AccountStatus]
	@ExternalTrackData [nvarchar](50),
	@VendorId [nvarchar](50),
	@MachineNumber [int],
	@Balance [decimal](20, 2) OUTPUT,
	@StatusCode [int] OUTPUT,
	@StatusText [nvarchar](max) OUTPUT,
	@StatusError [nvarchar](max) OUTPUT
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [SQLBusinessLogic].[WSI.Common.SqlProcedures].[Trx_3GS_AccountStatus]
GO

IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'zsp_SessionEnd')
  DROP PROCEDURE zsp_SessionEnd
GO

CREATE PROCEDURE [dbo].[zsp_SessionEnd]
  @AccountId      varchar(24),
  @VendorId       varchar(16),
  @SerialNumber   varchar(30),
  @MachineNumber  int,
  @SessionId      bigint,
  @AmountPlayed   money,
  @AmountWon      money,
  @GamesPlayed    int,
  @GamesWon       int,
  @CreditBalance  money,
  @CurrentJackpot money = 0,
  @BillIn		      money = 0		
WITH EXECUTE AS OWNER
AS
BEGIN

  DECLARE @status_code  int
  DECLARE @status_text  varchar (254)
  DECLARE @error_text   nvarchar (MAX)
  DECLARE @input        nvarchar(MAX)
  DECLARE @output       nvarchar(MAX)
  DECLARE @_try         int
  DECLARE @_max_tries   int
  DECLARE @_exception   bit
  DECLARE @_completed   bit

  SET @_try       = 0
  SET @_max_tries = 6		-- AJQ 19-DES-2014, The number of retries has been incremented to 6 (retries seem to work so we try one more time)
  SET @_completed = 0

  BEGIN TRANSACTION

  WHILE (@_completed = 0)
  BEGIN

    SET @_exception   = 0
    SET @status_code  = 4
    SET @status_text  = 'Access Denied'
    SET @error_text   = ''

    BEGIN TRY

      SET @_try = @_try + 1
      
      EXECUTE dbo.Trx_3GS_EndCardSession @VendorId, @SerialNumber, @MachineNumber,
                         @AccountID,
                         @SessionId,
                         @AmountPlayed, @AmountWon, @GamesPlayed, @GamesWon,
                         @CreditBalance, @BillIn,
                         @status_code OUTPUT, @status_text OUTPUT, @error_text OUTPUT
      SET @_completed = 1

    END TRY

    BEGIN CATCH
    
      ROLLBACK TRANSACTION

      IF (@_try >= @_max_tries) 
      BEGIN
        SET @_completed  = 1;
        SET @_exception  = 1;
        SET @status_code = 4;
        SET @status_text = 'Access Denied';
        SET @error_text  = ' ERROR_NUMBER: '    + CAST(ERROR_NUMBER()               AS NVARCHAR)
                         + ' ERROR_SEVERITY: '  + CAST(ERROR_SEVERITY()             AS NVARCHAR)
                         + ' ERROR_STATE: '     + CAST(ERROR_STATE()                AS NVARCHAR)
                         + ' ERROR_PROCEDURE: ' + CAST(ISNULL(ERROR_PROCEDURE(),'') AS NVARCHAR)
                         + ' ERROR_LINE: '      + CAST(ERROR_LINE()                 AS NVARCHAR)
                         + ' ERROR_MESSAGE: '   + CAST(ERROR_MESSAGE()              AS NVARCHAR(MAX))
      END
      ELSE
      BEGIN
        WAITFOR DELAY '00:00:02'
      END

      BEGIN TRANSACTION

    END CATCH
  END -- WHILE
  
  SET @input = '@AccountID='      + @AccountID
             +';@VendorId='       + @VendorId
             +';@SerialNumber='   + CAST (@SerialNumber   AS NVARCHAR)
             +';@MachineNumber='  + CAST (@MachineNumber  AS NVARCHAR)
             +';@SessionId='      + CAST (@SessionId      AS NVARCHAR)
             +';@AmountPlayed='   + CAST (@AmountPlayed   AS NVARCHAR)
             +';@AmountWon='      + CAST (@AmountWon      AS NVARCHAR)
             +';@GamesPlayed='    + CAST (@GamesPlayed    AS NVARCHAR)
             +';@GamesWon='       + CAST (@GamesWon       AS NVARCHAR)
             +';@CreditBalance='  + CAST (@CreditBalance  AS NVARCHAR)
             +';@CurrentJackpot=' + CAST (@CurrentJackpot AS NVARCHAR)

  IF @error_text <> ''
    SET @error_text = ';Details='     + @error_text

  SET @output = 'StatusCode='     + CAST (@status_code    AS NVARCHAR)
              +';StatusText='     + @status_text
              + @error_text
              + '; TryIndex='     + CAST (@_try AS NVARCHAR) 
  
  EXECUTE dbo.zsp_Audit 'zsp_SessionEnd', @AccountID, @VendorId, @SerialNumber, @MachineNumber,
                                          @SessionId, @status_code, @CreditBalance, 1,
                                          @input, @output

  COMMIT TRANSACTION

  -- AJQ 21-AUG-2014, Always return 0 "Success" to the caller.
  --                  This is to avoid retries from the client side.
  --                  When an exception has occurred, we will return status=4
  IF (@_exception = 0)
    SET  @status_code = 0

  SELECT @status_code AS StatusCode, @status_text AS StatusText

END -- zsp_SessionEnd

GO

IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'zsp_SessionUpdate')
       DROP PROCEDURE zsp_SessionUpdate
GO

 CREATE PROCEDURE [dbo].[zsp_SessionUpdate]
  @AccountId      varchar(24),
  @VendorId       varchar(16),
  @SerialNumber   varchar(30),
  @MachineNumber  int,
  @SessionId      bigint,
  @AmountPlayed   money,
  @AmountWon      money,
  @GamesPlayed    int,
  @GamesWon       int,
  @CreditBalance  money,
  @CurrentJackpot money = 0,
  @BillIn		      money = 0
WITH EXECUTE AS OWNER
AS
BEGIN
  DECLARE @update_freq  int
  DECLARE @status_code  int
  DECLARE @status_text  varchar (254)
  DECLARE @error_text   nvarchar (MAX)
  DECLARE @input        nvarchar(MAX)
  DECLARE @output       nvarchar(MAX)
  DECLARE @_try         int
  DECLARE @_max_tries   int
  DECLARE @_exception   bit
  DECLARE @_completed   bit

  SET @update_freq = 30

  -------- For Cadillac store all session updates received
  ------IF (@VendorId like '%CADILLAC%')
  ------    SET @update_freq = 1

  -- AJQ & XI 12-12-2013, Limit the number of updates 
  IF ( @GamesPlayed % @update_freq <> 0 ) 
  BEGIN
    -- When the provider calls every:
    -- 1 play --> 1 / 30 -->  3.33% trx --> 3.33% plays
    -- 2 play --> 1 / 15 -->  6.67% trx --> 3.33% plays
    -- 3 play --> 1 / 10 --> 10.00% trx --> 3.33% plays
    -- 4 play --> 1 / 60 -->  1.67% trx --> 0.42% plays
    -- 5 play --> 1 /  6 --> 16.67% trx --> 3.33% plays
    SELECT CAST (0 AS INT) AS StatusCode, CAST ('Success' AS varchar (254)) AS StatusText
    RETURN
  END

  SET @_try       = 0
  SET @_max_tries = 3
  SET @_completed = 0

  BEGIN TRANSACTION

  WHILE (@_completed = 0)
  BEGIN

    SET @_exception   = 0
    SET @status_code  = 4
    SET @status_text  = 'Access Denied'
    SET @error_text   = ''

    BEGIN TRY

      SET @_try = @_try + 1
    
      EXECUTE dbo.Trx_3GS_UpdateCardSession @VendorId, @SerialNumber, @MachineNumber,
                                            @AccountID,
                                            @SessionId,
                                            @AmountPlayed, @AmountWon, @GamesPlayed, @GamesWon,
                                            @CreditBalance, @BillIn,
                                            @status_code OUTPUT, @status_text OUTPUT, @error_text OUTPUT

      SET @_completed = 1

    END TRY

    BEGIN CATCH
    
      ROLLBACK TRANSACTION

      IF (@_try >= @_max_tries) 
      BEGIN
        SET @_completed  = 1;
        SET @_exception  = 1;
        SET @status_code = 4;
        SET @status_text = 'Access Denied';
        SET @error_text  = ' ERROR_NUMBER: '    + CAST(ERROR_NUMBER()               AS NVARCHAR)
                         + ' ERROR_SEVERITY: '  + CAST(ERROR_SEVERITY()             AS NVARCHAR)
                         + ' ERROR_STATE: '     + CAST(ERROR_STATE()                AS NVARCHAR)
                         + ' ERROR_PROCEDURE: ' + CAST(ISNULL(ERROR_PROCEDURE(),'') AS NVARCHAR)
                         + ' ERROR_LINE: '      + CAST(ERROR_LINE()                 AS NVARCHAR)
                         + ' ERROR_MESSAGE: '   + CAST(ERROR_MESSAGE()              AS NVARCHAR(MAX))
      END
      ELSE
      BEGIN
        WAITFOR DELAY '00:00:02'
      END

      BEGIN TRANSACTION

    END CATCH
  END

  SET @input = '@AccountID='      + @AccountID
             +';@VendorId='       + @VendorId
             +';@SerialNumber='   + CAST (@SerialNumber   AS NVARCHAR)
             +';@MachineNumber='  + CAST (@MachineNumber  AS NVARCHAR)
             +';@SessionId='      + CAST (@SessionId      AS NVARCHAR)
             +';@AmountPlayed='   + CAST (@AmountPlayed   AS NVARCHAR)
             +';@AmountWon='      + CAST (@AmountWon      AS NVARCHAR)
             +';@GamesPlayed='    + CAST (@GamesPlayed    AS NVARCHAR)
             +';@GamesWon='       + CAST (@GamesWon       AS NVARCHAR)
             +';@CreditBalance='  + CAST (@CreditBalance  AS NVARCHAR)
             +';@CurrentJackpot=' + CAST (@CurrentJackpot AS NVARCHAR)

  IF @error_text <> ''
    SET @error_text = ';Details='     + @error_text

  SET @output = 'StatusCode='     + CAST (@status_code    AS NVARCHAR)
              +';StatusText='     + @status_text
              + @error_text
              + '; TryIndex='     + CAST (@_try AS NVARCHAR)

  EXECUTE dbo.zsp_Audit 'zsp_SessionUpdate', @AccountID, @VendorId, @SerialNumber, @MachineNumber,
                                             @SessionId, @status_code, @CreditBalance, 1,
                                             @input, @output

  COMMIT TRANSACTION

  -- AJQ 19-DES-2014, When an exception occurred we will return 0-"Success" to the caller.
  IF (@_exception = 1)
    SET  @status_code = 0
    
  SELECT @status_code AS StatusCode, @status_text AS StatusText

END -- zsp_SessionUpdate


GO

IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'zsp_ReportMeters')
       DROP PROCEDURE zsp_ReportMeters
GO


CREATE PROCEDURE [dbo].[zsp_ReportMeters]
	@pVendorId varchar(16) = null,
    @pSerialNumber varchar(30) = null,
    @pMachineNumber int= null,
    @pXmlMeters Xml                              
 
AS
BEGIN
  declare @terminal_id int
  declare @status_code int
  declare @status_text varchar(254)
  declare @exception_info varchar
  
  SET @terminal_id = 0
  SET @status_code = 0
  SET @status_text = ''
  SET @exception_info = ''
  
  select @terminal_id =t3gs_terminal_id from TERMINALS_3GS where (t3gs_machine_number = @pMachineNumber) or (t3gs_vendor_id = @pVendorId and t3gs_serial_number = @pSerialNumber)
  if(@terminal_id<1)
    set @status_code = 1
  else
    begin
      begin transaction
      begin try

        DECLARE @metercode int
        DECLARE @metervalue bigint
        DECLARE @denom int

        select  
              @metercode = meters.meter.value('(@Id)[1]', 'int') , 
              @metervalue = meters.meter.value('(@Value)[1]', 'bigint'), 
              @denom = case meters.meter.value('(@Id)[1]', 'int')
                      when 66 then 5
                      when 67 then 10
                      when 68 then 20
                      when 70 then 50
                      when 71 then 100
                      when 72 then 200
                      when 74 then 500
                      when 75 then 1000
                      else 0.000
                    end 
                  FROM @pXmlMeters.nodes('Meters/Meter') meters(meter)

      IF EXISTS (SELECT 1 FROM terminal_sas_meters where terminal_sas_meters.tsm_terminal_id = @terminal_id 
                    AND terminal_sas_meters.tsm_meter_code = @metercode 
                    AND terminal_sas_meters.tsm_denomination = @denom)
      BEGIN
        UPDATE terminal_sas_meters SET tsm_meter_value = @metervalue, tsm_last_reported = GETDATE()
          WHERE terminal_sas_meters.tsm_terminal_id = @terminal_id 
                              AND terminal_sas_meters.tsm_meter_code = @metercode 
                              AND terminal_sas_meters.tsm_denomination = @denom
      END
      ELSE
      BEGIN
            INSERT INTO terminal_sas_meters ([tsm_terminal_id], [tsm_meter_code],  [tsm_game_id], [tsm_denomination],[tsm_wcp_sequence_id],
              [tsm_last_reported], [tsm_last_modified], [tsm_meter_value], [tsm_meter_max_value], [tsm_delta_value],
              [tsm_raw_delta_value], [tsm_delta_updating], [tsm_sas_accounting_denom])
            VALUES (@terminal_id, @metercode, 0, @denom, 0, getdate(), null, @metervalue, 9999999999, 0, 0, 0, null)
      END
          

        set @status_code=0
        commit transaction 
      end try
      begin catch
        rollback transaction
        set @exception_info= ' (ERROR_NUMBER: '    + CAST(ERROR_NUMBER()               AS NVARCHAR)
                       + ' ERROR_SEVERITY: '  + CAST(ERROR_SEVERITY()             AS NVARCHAR)
                       + ' ERROR_STATE: '     + CAST(ERROR_STATE()                AS NVARCHAR)
                       + ' ERROR_PROCEDURE: ' + CAST(ISNULL(ERROR_PROCEDURE(),'') AS NVARCHAR)
                       + ' ERROR_LINE: '      + CAST(ERROR_LINE()                 AS NVARCHAR)
                       + ' ERROR_MESSAGE: '   + CAST(ERROR_MESSAGE()              AS NVARCHAR(MAX))
                       + ')'
        set @status_code=4
      end catch
     end

  set @status_text = case @status_code 
                       when 0 then 'OK'
                       when 1 then 'Numero de terminal invalido'
                       when 4 then 'Acceso denegado'
                       else 'Unknown' 
                     end
  
  DECLARE @input AS nvarchar(MAX)

  set @input = CAST( @pXmlMeters as nvarchar(max));
  DECLARE @output AS nvarchar(MAX)

  SET @output = 'StatusCode='    + CAST (@status_code AS NVARCHAR)
              +';StatusText='    + @status_text
              + @exception_info;
         
  EXECUTE dbo.zsp_Audit 'zsp_ReportMeters', '', @pVendorId , @pSerialNumber, @terminal_id, NULL,  
                                         @status_code, NULL, 1, @input , @output


  select @status_code as StatusCode, @status_text  as StatusText
  
END -- [zsp_ReportMeters]

GO



-- [3GS]
GRANT EXECUTE ON [dbo].[zsp_SessionUpdate] TO [3GS] WITH GRANT OPTION 
GO
GRANT EXECUTE ON [dbo].[zsp_SessionEnd] TO [3GS] WITH GRANT OPTION 
GO
GRANT EXECUTE ON [dbo].[zsp_ReportMeters] TO [3GS] WITH GRANT OPTION 
GO


-- [EIBE]
GRANT EXECUTE ON [dbo].[zsp_SessionUpdate] TO [EIBE] WITH GRANT OPTION 
GO
GRANT EXECUTE ON [dbo].[zsp_SessionEnd] TO [EIBE] WITH GRANT OPTION 
GO
GRANT EXECUTE ON [dbo].[zsp_ReportMeters] TO [EIBE] WITH GRANT OPTION 
GO

-- [wg_interface]
GRANT EXECUTE ON [dbo].[zsp_SessionUpdate] TO [wg_interface] WITH GRANT OPTION 
GO
GRANT EXECUTE ON [dbo].[zsp_SessionEnd] TO [wg_interface] WITH GRANT OPTION 
GO
GRANT EXECUTE ON [dbo].[zsp_ReportMeters] TO [wg_interface] WITH GRANT OPTION 
GO

IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'S2S_AccountStatus')
       DROP PROCEDURE S2S_AccountStatus
GO

CREATE PROCEDURE S2S_AccountStatus
  @pVendorId        varchar(16),
  @pTrackdata       varchar(24)
AS
BEGIN
  DECLARE
  @MachineNumber   int  
  SET @MachineNumber = 0
  
  EXECUTE zsp_AccountStatus  @pTrackdata, @pVendorId, @MachineNumber
    
END
GO
IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'S2S_EndCardSession')
       DROP PROCEDURE S2S_EndCardSession
GO
CREATE PROCEDURE S2S_EndCardSession
	@pVendorId			varchar(16),
	@pSessionId			bigint,
	@pAmountPlayed		money,
	@pAmountWon			money,
	@pGamesPlayed		int,
	@pGamesWon			int,
	@pBillIn			money,
	@pCurrentBalance	money
AS
BEGIN

	DECLARE @TrackData	varchar(24)
	DECLARE @SerialNumber varchar(30)
	DECLARE @MachineNumber int  
	SET @MachineNumber = 0

	select @TrackData = dbo.TrackDataToExternal(a.ac_track_data), @SerialNumber = t.te_serial_number from play_sessions p
	inner join terminals t on p.ps_terminal_id= t.te_terminal_id
	inner join accounts a on a.ac_account_id=p.ps_account_id
	where p.ps_play_session_id = @pSessionId

	-- @AccountId
	--,@VendorId	OK
	--,@SerialNumber
	--,@MachineNumber	OK
	--,@SessionId	OK
	--,@AmountPlayed	OK
	--,@AmountWon	OK
	--,@GamesPlayed	OK
	--,@GamesWon	OK
	--,@CreditBalance	@pCurrentBalance??
	--,@CurrentJackpot

	EXECUTE zsp_SessionEnd  @TrackData, @pVendorId, @SerialNumber, @MachineNumber,@pSessionId,@pAmountPlayed,@pAmountWon,@pGamesPlayed, @pGamesWon,@pCurrentBalance,0,@pBillIn

    
END
GO
IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'S2S_ReportEvent')
       DROP PROCEDURE S2S_ReportEvent
GO
CREATE PROCEDURE [dbo].[S2S_ReportEvent]
  @pVendorId varchar(16),
  @pSerialNumber varchar(30),
  @pMachineNumber int,
  @pEventId int,
  @pAmount money,
  @pSessionId bigint
AS
  declare @AccountId varchar(24)

  select @AccountId = dbo.TrackDataToExternal(a.ac_track_data) 
    from play_sessions p
      inner join accounts a on a.ac_account_id=p.ps_account_id
    where p.ps_play_session_id = @pSessionId
  
  execute zsp_SendEvent @AccountId,
      @pVendorId,
      @pSerialNumber,
      @pMachineNumber,
      @pEventId,
      @pAmount
GO
IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'S2S_ReportMeters')
       DROP PROCEDURE S2S_ReportMeters
GO
CREATE PROCEDURE [dbo].[S2S_ReportMeters]
@pVendorId NvarChar(50)=null,
@pSerialNumber NvarChar(50)= null,
@pMachineNumber int= null,
@pXmlMeters Xml
AS
execute zsp_ReportMeters @pVendorId=@pVendorId ,
                      @pSerialNumber=@pSerialNumber ,
                      @pMachineNumber=@pMachineNumber ,
                      @pXmlMeters=@pXmlMeters
GO
IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'S2S_SessionStart')
       DROP PROCEDURE S2S_SessionStart
GO
CREATE PROCEDURE S2S_SessionStart
  @pTrackData        varchar(24),
  @pVendorID        varchar(16),
  @pSerialNumber    varchar(30),
  @pMachineNumber   int,
  @pVendorSessionID bigint
AS
BEGIN

  DECLARE @CurrentJackPot money
  SET @CurrentJackPot = 0

  EXECUTE zsp_SessionStart @pTrackData, @pVendorID, @pSerialNumber, @pMachineNumber, @CurrentJackPot, @pVendorSessionID
    
END
GO
IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'S2S_UpdateCardSession')
       DROP PROCEDURE S2S_UpdateCardSession
GO
CREATE PROCEDURE S2S_UpdateCardSession
	@pVendorId			varchar(16),
	@pSessionId			bigint,
	@pAmountPlayed		money,
	@pAmountWon			money,
	@pGamesPlayed		int,
	@pGamesWon			int,
	@pBillIn			money,
	@pCurrentBalance	money
AS
BEGIN

	DECLARE @TrackData	varchar(24)
	DECLARE @SerialNumber varchar(30)
	DECLARE @MachineNumber int  
	SET @MachineNumber = 0

	select @TrackData = dbo.TrackDataToExternal(a.ac_track_data), @SerialNumber = t.te_serial_number from play_sessions p
	inner join terminals t on p.ps_terminal_id= t.te_terminal_id
	inner join accounts a on a.ac_account_id=p.ps_account_id
	where p.ps_play_session_id = @pSessionId

	-- @AccountId
	--,@VendorId	OK
	--,@SerialNumber
	--,@MachineNumber	OK
	--,@SessionId	OK
	--,@AmountPlayed	OK
	--,@AmountWon	OK
	--,@GamesPlayed	OK
	--,@GamesWon	OK
	--,@CreditBalance	@pCurrentBalance??
	--,@CurrentJackpot

  EXECUTE zsp_SessionUpdate  @TrackData, @pVendorId, @SerialNumber, @MachineNumber,@pSessionId,@pAmountPlayed,@pAmountWon,@pGamesPlayed, @pGamesWon,@pCurrentBalance,0,@pBillIn

    
END
GO

-- [3GS]
GRANT EXECUTE ON [dbo].[S2S_UpdateCardSession] TO [3GS] WITH GRANT OPTION 
GO
GRANT EXECUTE ON [dbo].[S2S_SessionStart] TO [3GS] WITH GRANT OPTION 
GO
GRANT EXECUTE ON [dbo].[S2S_ReportEvent] TO [3GS] WITH GRANT OPTION 
GO
GRANT EXECUTE ON [dbo].[S2S_EndCardSession] TO [3GS] WITH GRANT OPTION 
GO
GRANT EXECUTE ON [dbo].[S2S_AccountStatus] TO [3GS] WITH GRANT OPTION 
GO
GRANT EXECUTE ON [dbo].[S2S_ReportMeters] TO [3GS] WITH GRANT OPTION 
GO


-- [EIBE]
GRANT EXECUTE ON [dbo].[S2S_UpdateCardSession] TO [EIBE] WITH GRANT OPTION 
GO
GRANT EXECUTE ON [dbo].[S2S_SessionStart] TO [EIBE] WITH GRANT OPTION 
GO
GRANT EXECUTE ON [dbo].[S2S_ReportEvent] TO [EIBE] WITH GRANT OPTION 
GO
GRANT EXECUTE ON [dbo].[S2S_EndCardSession] TO [EIBE] WITH GRANT OPTION 
GO
GRANT EXECUTE ON [dbo].[S2S_AccountStatus] TO [EIBE] WITH GRANT OPTION 
GO
GRANT EXECUTE ON [dbo].[S2S_ReportMeters] TO [EIBE] WITH GRANT OPTION 
GO


-- [wg_interface]
GRANT EXECUTE ON [dbo].[S2S_UpdateCardSession] TO [wg_interface] WITH GRANT OPTION 
GO
GRANT EXECUTE ON [dbo].[S2S_SessionStart] TO [wg_interface] WITH GRANT OPTION 
GO
GRANT EXECUTE ON [dbo].[S2S_ReportEvent] TO [wg_interface] WITH GRANT OPTION 
GO
GRANT EXECUTE ON [dbo].[S2S_EndCardSession] TO [wg_interface] WITH GRANT OPTION 
GO
GRANT EXECUTE ON [dbo].[S2S_AccountStatus] TO [wg_interface] WITH GRANT OPTION 
GO
GRANT EXECUTE ON [dbo].[S2S_ReportMeters] TO [wg_interface] WITH GRANT OPTION 
GO

