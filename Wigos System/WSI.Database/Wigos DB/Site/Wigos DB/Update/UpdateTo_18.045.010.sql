/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 44;

SET @New_ReleaseId = 45;
SET @New_ScriptName = N'UpdateTo_18.045.010.sql';
SET @New_Description = N'Changes required for GLI Certification (13 and 16); required fixes.';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO


/****** TABLES ******/


/****** INDEXES ******/


/****** RECORDS ******/
/* Cashier.Voucher.Handpay.SignaturesRoom */
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.Voucher' AND GP_SUBJECT_KEY ='Handpay.SignaturesRoom')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key]
             ,[gp_subject_key]
             ,[gp_key_value])
       VALUES
             ('Cashier.Voucher'
             ,'Handpay.SignaturesRoom'
             ,'0');
ELSE
  SELECT '***** Record Cashier.Voucher.Handpay.SignaturesRoom already exists *****';

/* Cashier.Voucher.Handpay.GameMetersRoom */
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.Voucher' AND GP_SUBJECT_KEY ='Handpay.GameMetersRoom')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key]
             ,[gp_subject_key]
             ,[gp_key_value])
       VALUES
             ('Cashier.Voucher'
             ,'Handpay.GameMetersRoom'
             ,'0');
ELSE
  SELECT '***** Record Cashier.Voucher.Handpay.GameMetersRoom already exists *****';

/* Cashier.Voucher.Handpay.PrintPlayedAmount */
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.Voucher' AND GP_SUBJECT_KEY ='Handpay.PrintPlayedAmount')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key]
             ,[gp_subject_key]
             ,[gp_key_value])
       VALUES
             ('Cashier.Voucher'
             ,'Handpay.PrintPlayedAmount'
             ,'0');
ELSE
  SELECT '***** Record Cashier.Voucher.Handpay.PrintPlayedAmount already exists *****';


/* Gui Form Name */
IF EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 14 AND GF_FORM_ID = 80 AND GF_NLS_ID = 11551)
BEGIN
  UPDATE GUI_FORMS 
     SET GF_NLS_ID = 16551 
   WHERE GF_GUI_ID = 14 
     AND GF_FORM_ID = 80 
     AND GF_NLS_ID = 11551;
END


/**************************************** 3GS Section ****************************************/

