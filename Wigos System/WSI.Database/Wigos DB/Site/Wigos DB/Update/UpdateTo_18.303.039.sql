/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 302;

SET @New_ReleaseId = 303;
SET @New_ScriptName = N'UpdateTo_18.303.039.sql';
SET @New_Description = N'Update SP for Site/Multisite';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/******* TABLES  *******/

-- ELP01_SPACE_REQUESTS
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[elp01_space_requests]') AND type in (N'U'))
DROP TABLE elp01_space_requests
GO

CREATE TABLE [dbo].[elp01_space_requests](
	[es_account_id]       [bigint] NOT NULL,
	[es_transaction_id]   [nvarchar](50) NOT NULL,
  [es_credit_type]      [int]	NOT NULL,
  [es_amount]           [money] NOT NULL,
	[es_group_id]         [int] NOT NULL,
	[es_timestamp]        [timestamp] NOT NULL,
 CONSTRAINT [PK_elp01_space_requests] PRIMARY KEY CLUSTERED 
(
	[es_account_id] ASC, [es_transaction_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[invalid_play_sessions_to_player_tracking]') AND type in (N'U'))
  DROP TABLE invalid_play_sessions_to_player_tracking
GO

CREATE TABLE [dbo].[invalid_play_sessions_to_player_tracking](
      [ips_unique_ud] [bigint] IDENTITY(1,1) NOT NULL,
      [ips_play_session_id] [bigint] NOT NULL,
      [ips_account_id] [bigint] NOT NULL,
      [ips_coin_in] [decimal](12, 3) NOT NULL
) ON [PRIMARY]
GO

/******* INDEXES *******/

/******* RECORDS *******/

DECLARE @Puntos_canje_id int
declare @Now datetime
declare @Today datetime
declare @ClosingTime Integer
declare @DaysCountingPoints Integer
declare @Date_From datetime
declare @Date_To datetime
declare @MaxDays Integer
declare @LastActivity datetime
declare @Puntos_nivel_id int

-- Bucket Puntos de Nivel
SET @Puntos_nivel_id = 2
SET @Puntos_canje_id = 1

-- Today
select @ClosingTime = gp_key_value from general_params where gp_group_key = 'WigosGUI' and gp_subject_key = 'ClosingTime'
SET @ClosingTime = isnull(@ClosingTime, 0)
SET @Now = DATEADD(dd, 0, DATEDIFF(dd, 0, getdate()))
SET @Today = DATEADD(hh, @ClosingTime, @Now)

-- Date From
SELECT @DaysCountingPoints = gp_key_value from general_params where gp_group_key = 'PlayerTracking' and gp_subject_key = 'Levels.DaysCountingPoints'
SET @Date_From = DATEADD(dd, -@DaysCountingPoints, @Today)

-- LastActivity
SET @MaxDays = 400 + @DaysCountingPoints
SET @LastActivity = DATEADD (DAY, -@MaxDays,@Today)

DELETE FROM CUSTOMER_BUCKET

INSERT INTO   CUSTOMER_BUCKET 
             (CBU_CUSTOMER_ID
            , CBU_BUCKET_ID
            , CBU_VALUE
            , CBU_UPDATED)
     SELECT   AC_ACCOUNT_ID
            , @Puntos_canje_id
            , ISNULL(AC_POINTS,0)
            , GETDATE()
       FROM   ACCOUNTS
  LEFT JOIN   CUSTOMER_BUCKET
         ON   AC_ACCOUNT_ID = CBU_CUSTOMER_ID
      WHERE   CBU_CUSTOMER_ID IS NULL
        AND   ISNULL(AC_POINTS,0) <> 0
UNION

SELECT     AC.ACCOUNT_ID      
         , @Puntos_nivel_id
         , ISNULL (POINTS, 0) 
         , LAST_ACTIVITY      
FROM
		(
		SELECT   AC_ACCOUNT_ID              AS ACCOUNT_ID
			   , AC_LAST_ACTIVITY           AS LAST_ACTIVITY
		  FROM   ACCOUNTS WITH (INDEX (IX_ac_last_activity))
		 WHERE   AC_LAST_ACTIVITY >= @LastActivity
		   AND   AC_HOLDER_LEVEL  >= 1
		) AC LEFT JOIN
		(
			SELECT   AM_ACCOUNT_ID      AS ACCOUNT_ID
				   , SUM(AM_ADD_AMOUNT) AS POINTS 
			  FROM   ACCOUNT_MOVEMENTS WITH (INDEX (IX_type_date_account)) 
		INNER JOIN   ACCOUNTS ON AC_ACCOUNT_ID = AM_ACCOUNT_ID
			WHERE   AM_TYPE IN (36, 68, 72, 73)
			  AND   AM_DATETIME  >= @Date_From 
			  AND   AM_DATETIME  <  @Today
		 GROUP BY   AM_ACCOUNT_ID
		) PO ON AC.ACCOUNT_ID = PO.ACCOUNT_ID
WHERE ISNULL (POINTS, 0) > 0
GO

-- BUG 8791
UPDATE GENERAL_PARAMS SET gp_key_value = 'http://www.google.com'
WHERE GP_GROUP_KEY ='GameGateway' AND GP_SUBJECT_KEY = 'Provider.001.Uri' and gp_key_value = 'http://www.xxxxx.xxx'
GO

IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'GamingHall.SasMeter' AND GP_SUBJECT_KEY = 'Meter.OutBills')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('GamingHall.SasMeter', 'Meter.OutBills', '0')
  
IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'GamingHall.SasMeter' AND GP_SUBJECT_KEY = 'Meter.OutCoins')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('GamingHall.SasMeter', 'Meter.OutCoins', '0')

IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'GamingHall.SasMeter' AND GP_SUBJECT_KEY = 'Meter.OutCents')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('GamingHall.SasMeter', 'Meter.OutCents', '0')
GO

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'GamingHall' AND GP_SUBJECT_KEY ='Collection.ToCashier.Enabled')
  INSERT INTO general_params (gp_group_key, gp_subject_key, gp_key_value) VALUES ('GamingHall', 'Collection.ToCashier.Enabled', '0'); 
GO

/******* PROCEDURES *******/

--------------------------------------------------------------------------------
-- Copyright � 2013 Win Systems International
--------------------------------------------------------------------------------
--
--   MODULE NAME: Update_PersonalInfo.sql.sql
--
--   DESCRIPTION: Update personal Information 
--
--        AUTHOR: Dani Dom�nguez
--
-- CREATION DATE: 08-MAR-2013
--
-- REVISION HISTORY:
--
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 08-MAR-2013 DDM    First release.  
-- 22-MAY-2013 DDM    Fixed bugs 783,793 and 693
--                    Added field AC_BLOCK_DESCRIPTION
-- 28-MAY-2013 DDM    Fixed bug #803
--                    Added field AC_EXTERNAL_REFERENCE
-- 03-JUL-2013 DDM    Added new fields about Money Laundering
-- 11-NOV-2013 JPJ    Added new fields AC_HOLDER_OCCUPATION_ID and AC_BENEFICIARY_OCCUPATION_ID
-- 23-APR-2014 SMN		
--------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Update_PersonalInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Update_PersonalInfo]
GO
CREATE PROCEDURE [dbo].[Update_PersonalInfo]
  @pAccountId                  bigint
, @pTrackData                  nvarchar(50)
,	@pHolderName                 nvarchar(200)
,	@pHolderId                   nvarchar(20)
, @pHolderIdType               int
, @pHolderAddress01            nvarchar(50)
, @pHolderAddress02            nvarchar(50)
, @pHolderAddress03            nvarchar(50)
, @pHolderCity                 nvarchar(50)
, @pHolderZip                  nvarchar(10) 
, @pHolderEmail01              nvarchar(50)
,	@pHolderEmail02              nvarchar(50)
,	@pHolderTwitter              nvarchar(50)
,	@pHolderPhoneNumber01        nvarchar(20)
, @pHolderPhoneNumber02        nvarchar(20)
, @pHolderComments             nvarchar(100)
, @pHolderId1                  nvarchar(20)
, @pHolderId2                  nvarchar(20)
, @pHolderDocumentId1          bigint
, @pHolderDocumentId2          bigint
, @pHolderName1                nvarchar(50)
,	@pHolderName2                nvarchar(50)
,	@pHolderName3                nvarchar(50)
, @pHolderGender               int
, @pHolderMaritalStatus        int
, @pHolderBirthDate            datetime
, @pHolderWeddingDate          datetime
, @pHolderLevel                int
, @pHolderLevelNotify          int
, @pHolderLevelEntered         datetime
,	@pHolderLevelExpiration      datetime
,	@pPin                        nvarchar(12)
, @pPinFailures                int
, @pPinLastModified            datetime
, @pBlocked                    bit
, @pActivated                  bit
, @pBlockReason                int
, @pHolderIsVip                int
, @pHolderTitle                nvarchar(15)                       
, @pHolderName4                nvarchar(50)                       
, @pHolderPhoneType01          int
, @pHolderPhoneType02          int
, @pHolderState                nvarchar(50)                    
, @pHolderCountry              nvarchar(50)                
, @pPersonalInfoSequenceId     bigint                     
, @pUserType                   int
, @pPointsStatus               int
, @pDeposit                    money
, @pCardPay                    bit
, @pBlockDescription           nvarchar(256) 
, @pMSHash                     varbinary(20) 
, @pMSCreatedOnSiteId          int
, @pCreated                    datetime
, @pExternalReference          nvarchar(50) 
, @pHolderOccupation           nvarchar(50) 	
, @pHolderExtNum               nvarchar(10) 
, @pHolderNationality          Int 
, @pHolderBirthCountry         Int 
, @pHolderFedEntity            Int 
, @pHolderId1Type              Int -- RFC
, @pHolderId2Type              Int -- CURP
, @pHolderId3Type              nvarchar(50) 
, @pHolderId3                  nvarchar(20) 
, @pHolderHasBeneficiary       bit  
, @pBeneficiaryName            nvarchar(200) 
, @pBeneficiaryName1           nvarchar(50) 
, @pBeneficiaryName2           nvarchar(50) 
, @pBeneficiaryName3           nvarchar(50) 
, @pBeneficiaryBirthDate       Datetime 
, @pBeneficiaryGender          int 
, @pBeneficiaryOccupation      nvarchar(50) 
, @pBeneficiaryId1Type         int   -- RFC
, @pBeneficiaryId1             nvarchar(20) 
, @pBeneficiaryId2Type         int   -- CURP
, @pBeneficiaryId2             nvarchar(20) 
, @pBeneficiaryId3Type         nvarchar(50) 
, @pBeneficiaryId3             nvarchar(20)
, @pHolderOccupationId         int
, @pBeneficiaryOccupationId    int
, @pPlayerTrackingMode         int 
AS
BEGIN

DECLARE @pOtherAccountId as BIGINT
DECLARE @UserTypeSiteAccount as BIT

SELECT
       @pOtherAccountId = AC_ACCOUNT_ID 
      ,@UserTypeSiteAccount = AC_USER_TYPE
FROM ACCOUNTS WHERE AC_TRACK_DATA = @pTrackData

SET @pOtherAccountId = ISNULL(@pOtherAccountId, 0)
SET @UserTypeSiteAccount = ISNULL(@UserTypeSiteAccount, 1)

SET @pUserType = ISNULL(@pUserType,CASE WHEN ISNULL(@pHolderLevel,0) > 0  THEN 1 ELSE 0 END)

IF @pOtherAccountId <> 0 AND @pOtherAccountId <> @pAccountId
	   IF (ISNULL((SELECT gp_key_value FROM general_params WHERE gp_group_key = 'PlayerTracking.ExternalLoyaltyProgram' AND gp_subject_key = 'Mode'),0) = 1)
			AND @UserTypeSiteAccount = 0 -- IS ANONYMOUS
       BEGIN
			SET @pTrackData = '-RECYCLED-DUP-' + CAST (NEWID() AS NVARCHAR(50))
       END
       ELSE 
       BEGIN -- NOT IS ANONYMOUS OR ELP IS NOT ENABLED
			  UPDATE   ACCOUNTS   
				 SET   AC_TRACK_DATA =  '-RECYCLED-DUP-' + CAST (NEWID() AS NVARCHAR(50))
			   WHERE   AC_ACCOUNT_ID = @pOtherAccountId
       END


IF NOT EXISTS (SELECT 1 FROM ACCOUNTS WHERE ac_account_id = @pAccountId )  
BEGIN
  INSERT INTO   ACCOUNTS 
              ( AC_ACCOUNT_ID, AC_TYPE, AC_BLOCKED, AC_TRACK_DATA) 
       VALUES ( @pAccountId,         2,  @pBlocked, @pTrackData )
END  

 
  UPDATE   ACCOUNTS                   
     SET   AC_TRACK_DATA                = @pTrackData 
         , AC_HOLDER_NAME               = @pHolderName 
         , AC_HOLDER_ID                 = @pHolderId 
         , AC_HOLDER_ID_TYPE            = @pHolderIdType 
         , AC_HOLDER_ADDRESS_01         = @pHolderAddress01 
         , AC_HOLDER_ADDRESS_02         = @pHolderAddress02 
         , AC_HOLDER_ADDRESS_03         = @pHolderAddress03 
         , AC_HOLDER_CITY               = @pHolderCity 
         , AC_HOLDER_ZIP                = @pHolderZip  
         , AC_HOLDER_EMAIL_01           = @pHolderEmail01 
         , AC_HOLDER_EMAIL_02           = @pHolderEmail02 
         , AC_HOLDER_TWITTER_ACCOUNT    = @pHolderTwitter 
         , AC_HOLDER_PHONE_NUMBER_01    = @pHolderPhoneNumber01 
         , AC_HOLDER_PHONE_NUMBER_02    = @pHolderPhoneNumber02 
         , AC_HOLDER_COMMENTS           = @pHolderComments 
         , AC_HOLDER_ID1                = @pHolderId1 
         , AC_HOLDER_ID2                = @pHolderId2 
         , AC_HOLDER_DOCUMENT_ID1       = @pHolderDocumentId1 
         , AC_HOLDER_DOCUMENT_ID2       = @pHolderDocumentId2 
         , AC_HOLDER_NAME1              = @pHolderName1 
         , AC_HOLDER_NAME2              = @pHolderName2 
         , AC_HOLDER_NAME3              = @pHolderName3 
         , AC_HOLDER_GENDER             = @pHolderGender  
         , AC_HOLDER_MARITAL_STATUS     = @pHolderMaritalStatus 
         , AC_HOLDER_BIRTH_DATE         = @pHolderBirthDate 
         , AC_HOLDER_WEDDING_DATE       = @pHolderWeddingDate 
         , AC_HOLDER_LEVEL              = CASE WHEN (@pPlayerTrackingMode = 1) THEN AC_HOLDER_LEVEL ELSE @pHolderLevel END
         , AC_HOLDER_LEVEL_NOTIFY       = CASE WHEN (@pPlayerTrackingMode = 1) THEN AC_HOLDER_LEVEL_NOTIFY ELSE @pHolderLevelNotify END
         , AC_HOLDER_LEVEL_ENTERED      = CASE WHEN (@pPlayerTrackingMode = 1) THEN AC_HOLDER_LEVEL_ENTERED ELSE @pHolderLevelEntered END
         , AC_HOLDER_LEVEL_EXPIRATION   = CASE WHEN (@pPlayerTrackingMode = 1) THEN AC_HOLDER_LEVEL_EXPIRATION ELSE @pHolderLevelExpiration END
         , AC_PIN                       = @pPin 
         , AC_PIN_FAILURES              = @pPinFailures 
         , AC_PIN_LAST_MODIFIED         = @pPinLastModified 
         , AC_BLOCKED                   = @pBlocked 
         , AC_ACTIVATED                 = @pActivated 
         , AC_BLOCK_REASON              = CASE WHEN @pBlocked = 0 THEN 0 ELSE (dbo.BlockReason_ToNewEnumerate(AC_BLOCK_REASON) | @pBlockReason) END 
         , AC_HOLDER_IS_VIP             = @pHolderIsVip 
         , AC_HOLDER_TITLE              = @pHolderTitle 
         , AC_HOLDER_NAME4              = @pHolderName4         
         , AC_HOLDER_PHONE_TYPE_01      = @pHolderPhoneType01  
         , AC_HOLDER_PHONE_TYPE_02      = @pHolderPhoneType02  
         , AC_HOLDER_STATE              = @pHolderState    
         , AC_HOLDER_COUNTRY            = @pHolderCountry  					
         , AC_MS_PERSONAL_INFO_SEQ_ID   = @pPersonalInfoSequenceId           
         , AC_USER_TYPE				          = @pUserType
         , AC_POINTS_STATUS             = CASE WHEN (@pPlayerTrackingMode = 1) THEN AC_POINTS_STATUS ELSE @pPointsStatus END
         , AC_MS_CHANGE_GUID            = AC_MS_CHANGE_GUID -- avoid trigger on update
         , AC_DEPOSIT                   = @pDeposit 
         , AC_CARD_PAID                 = @pCardPay 
         , AC_BLOCK_DESCRIPTION         = @pBlockDescription                
         , AC_CREATED                   = @pCreated
         , AC_MS_CREATED_ON_SITE_ID     = @pMSCreatedOnSiteId
         , AC_MS_HASH                   = @pMSHash
         , AC_EXTERNAL_REFERENCE        = ISNULL(@pExternalReference,AC_EXTERNAL_REFERENCE)
         , AC_HOLDER_OCCUPATION         = @pHolderOccupation
         , AC_HOLDER_EXT_NUM            = @pHolderExtNum
         , AC_HOLDER_NATIONALITY        = @pHolderNationality 
         , AC_HOLDER_BIRTH_COUNTRY      = @pHolderBirthCountry 
         , AC_HOLDER_FED_ENTITY         = @pHolderFedEntity
         , AC_HOLDER_ID1_TYPE           = @pHolderId1Type 
         , AC_HOLDER_ID2_TYPE           = @pHolderId2Type 
         , AC_HOLDER_ID3_TYPE           = @pHolderId3Type 
         , AC_HOLDER_ID3                = @pHolderId3
         , AC_HOLDER_HAS_BENEFICIARY    = @pHolderHasBeneficiary 
         , AC_BENEFICIARY_NAME          = @pBeneficiaryName 
         , AC_BENEFICIARY_NAME1         = @pBeneficiaryName1
         , AC_BENEFICIARY_NAME2         = @pBeneficiaryName2
         , AC_BENEFICIARY_NAME3         = @pBeneficiaryName3
         , AC_BENEFICIARY_BIRTH_DATE    = @pBeneficiaryBirthDate 
         , AC_BENEFICIARY_GENDER        = @pBeneficiaryGender 
         , AC_BENEFICIARY_OCCUPATION    = @pBeneficiaryOccupation
         , AC_BENEFICIARY_ID1_TYPE      = @pBeneficiaryId1Type 
         , AC_BENEFICIARY_ID1           = @pBeneficiaryId1 
         , AC_BENEFICIARY_ID2_TYPE      = @pBeneficiaryId2Type 
         , AC_BENEFICIARY_ID2           = @pBeneficiaryId2 
         , AC_BENEFICIARY_ID3_TYPE      = @pBeneficiaryId3Type 
         , AC_BENEFICIARY_ID3           = @pBeneficiaryId3 
         , AC_HOLDER_OCCUPATION_ID      = ISNULL(@pHolderOccupationId, AC_HOLDER_OCCUPATION_ID)
         , AC_BENEFICIARY_OCCUPATION_ID = ISNULL(@pBeneficiaryOccupationId, AC_BENEFICIARY_OCCUPATION_ID)
   WHERE   AC_ACCOUNT_ID                = @pAccountId 

END
GO

--------------------------------------------------------------------------------
-- Copyright � 2013 Win Systems International
--------------------------------------------------------------------------------
--
--   MODULE NAME: Update_PointsInAccount.sql
--
--   DESCRIPTION: Update points of the account in site 
--
--        AUTHOR: Dani Dom�nguez
--
-- CREATION DATE: 07-MAR-2013
--
-- REVISION HISTORY:
--
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 07-MAR-2013 DDM    First release.
-- 03-SEP-2013 JML    Avoid excessive points generated by connection problems.
-- 13-JAN-2016 JML    Product Backlog Item 2541: MultiSite Multicurrency PHASE3: Loyalty program in the sites.
--------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Update_PointsInAccount]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Update_PointsInAccount]
GO
CREATE PROCEDURE [dbo].[Update_PointsInAccount]
  @pMovementId                 BIGINT
, @pAccountId                  BIGINT
, @pErrorCode                  INT         
, @pPointsSequenceId           BIGINT      
, @pReceivedPoints             MONEY  
, @pPlayerTrackingMode         INT         

AS
BEGIN   
  DECLARE @LocalDelta       AS MONEY
  DECLARE @ActualPoints     AS MONEY
  DECLARE @ReceivedAddLocal AS MONEY
  DECLARE @NewPoints        AS MONEY
  DECLARE @bucket_puntos_canje AS INT
  DECLARE @TableTemp table ( T_VALUE DECIMAL);
	
  SET @bucket_puntos_canje = 1

  IF NOT EXISTS (SELECT 1 FROM ACCOUNTS WHERE ac_account_id = @pAccountId )   
    INSERT INTO   ACCOUNTS 
                ( AC_ACCOUNT_ID, AC_TYPE, AC_BLOCKED, AC_TRACK_DATA) 
         VALUES ( @pAccountId,         2,          1, '-RECYCLED-NEW-' + CAST (NEWID() AS NVARCHAR(50)) )
         
  IF ((@pMovementId <> 0)AND(@pErrorCode = 0 OR @pErrorCode = 2))
    DELETE   MS_SITE_PENDING_ACCOUNT_MOVEMENTS
     WHERE   SPM_MOVEMENT_ID = @pMovementId
  
  IF (@pErrorCode <> 0) RETURN

  IF (@pPlayerTrackingMode = 1) RETURN

  DECLARE @PrevSequence as BIGINT
  
  -- Lock the account and later add the local points
  UPDATE ACCOUNTS SET AC_TYPE = AC_TYPE + 0 WHERE AC_ACCOUNT_ID = @pAccountId
  UPDATE CUSTOMER_BUCKET SET CBU_VALUE = CBU_VALUE + 0 OUTPUT INSERTED.CBU_VALUE INTO @TableTemp WHERE CBU_CUSTOMER_ID = @pAccountId AND CBU_BUCKET_ID = @bucket_puntos_canje

  -- SET @PrevSequence = (SELECT ISNULL(AC_MS_POINTS_SEQ_ID, 0) FROM ACCOUNTS WHERE AC_ACCOUNT_ID = @pAccountId)
  SELECT   @PrevSequence = ISNULL(AC_MS_POINTS_SEQ_ID, 0) 
    FROM   ACCOUNTS 
   WHERE   AC_ACCOUNT_ID = @pAccountId

   SELECT @ActualPoints = T_VALUE FROM @TableTemp
   SET @ActualPoints = ISNULL(@ActualPoints, 0)
   
  IF ( @PrevSequence > @pPointsSequenceId ) RETURN
  
-- Local Points
  SELECT   @LocalDelta = SUM(am_add_amount - am_sub_amount) 
    FROM   ACCOUNT_MOVEMENTS, MS_SITE_PENDING_ACCOUNT_MOVEMENTS
   WHERE   AM_MOVEMENT_ID = SPM_MOVEMENT_ID
     AND   AM_ACCOUNT_ID  = @pAccountId
     AND   AM_TYPE IN (36,37,38,40,41,46,50,60,61,66) -- Not included level movements,(39)PointsGiftDelivery, 
                                                      -- (42)PointsGiftServices and (67)PointsStatusChanged
  
  SET @ReceivedAddLocal = @pReceivedPoints + ISNULL(@LocalDelta , 0) 
  
  SET @NewPoints = CASE WHEN (ISNULL(@LocalDelta , 0) <> 0 AND @ActualPoints    <= @pReceivedPoints AND @ActualPoints    <= @ReceivedAddLocal)
                             THEN @ActualPoints
                        WHEN (ISNULL(@LocalDelta , 0) <> 0 AND @pReceivedPoints <= @ActualPoints    AND @pReceivedPoints <= @ReceivedAddLocal)
                             THEN @pReceivedPoints
                        ELSE @ReceivedAddLocal 
                        END 
/*
  UPDATE   ACCOUNTS
     SET   AC_POINTS                  = @NewPoints
         , AC_MS_POINTS_SEQ_ID        = @pPointsSequenceId
         , AC_MS_POINTS_SYNCHRONIZED  = GETDATE ()
   WHERE   AC_ACCOUNT_ID                   = @pAccountId
     AND   ISNULL(AC_MS_POINTS_SEQ_ID, 0) <= @pPointsSequenceId
*/
  UPDATE   ACCOUNTS
     SET   AC_MS_POINTS_SEQ_ID        = @pPointsSequenceId
         , AC_MS_POINTS_SYNCHRONIZED  = GETDATE ()
   WHERE   AC_ACCOUNT_ID                   = @pAccountId
     AND   ISNULL(AC_MS_POINTS_SEQ_ID, 0) <= @pPointsSequenceId

	 IF @@ROWCOUNT = 1
		UPDATE CUSTOMER_BUCKET SET CBU_VALUE = @NewPoints 
		 WHERE CBU_CUSTOMER_ID = @pAccountId AND CBU_BUCKET_ID = @bucket_puntos_canje 

END
GO

