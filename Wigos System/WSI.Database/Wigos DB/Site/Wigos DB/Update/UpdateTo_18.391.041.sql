/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 390;

SET @New_ReleaseId = 391;
SET @New_ScriptName = N'UpdateTo_18.391.041.sql';
SET @New_Description = N'SiteJackpotBuffering;'; 

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/**** GENERAL PARAM *****/
IF (NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'SiteJackpot' AND GP_SUBJECT_KEY = 'Buffering.Enabled'))
BEGIN 
    INSERT INTO general_params (gp_group_key, gp_subject_key, gp_key_value)  VALUES ('SiteJackpot','Buffering.Enabled', 1)
END 
GO

IF (NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS   WHERE GP_GROUP_KEY = 'SiteJackpot' AND GP_SUBJECT_KEY = 'Bonusing.ShowOnNotificationEGM'))
BEGIN 
    INSERT INTO general_params (gp_group_key, gp_subject_key, gp_key_value)  VALUES ('SiteJackpot','Bonusing.ShowOnNotificationEGM', 0)
END 
GO
/******* TABLES  *******/

/******* RECORDS *******/

/******* PROCEDURES *******/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CashierMovementsGroupedPerSessionId]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CashierMovementsGroupedPerSessionId]
GO

CREATE PROCEDURE [dbo].[CashierMovementsGroupedPerSessionId]
        @pCashierSessionId BIGINT     
  AS
BEGIN          	
DECLARE @CashierSessionId NVARCHAR(MAX)
SET @CashierSessionId = CONVERT(VARCHAR, @pCashierSessionId)

	IF EXISTS (SELECT CM_SESSION_ID FROM cashier_movements_grouped_by_session_id WHERE CM_SESSION_ID = @pCashierSessionId)
	BEGIN
		DELETE FROM cashier_movements_grouped_by_session_id WHERE CM_SESSION_ID = @pCashierSessionId
	END
		
		INSERT INTO   CASHIER_MOVEMENTS_GROUPED_BY_SESSION_ID 
					( CM_SESSION_ID
					, CM_TYPE
					, CM_SUB_TYPE
					, CM_TYPE_COUNT
					, CM_CURRENCY_ISO_CODE
					, CM_CURRENCY_DENOMINATION
					, CM_SUB_AMOUNT
					, CM_ADD_AMOUNT
					, CM_AUX_AMOUNT
					, CM_INITIAL_BALANCE
					, CM_FINAL_BALANCE
          , CM_CAGE_CURRENCY_TYPE
					)
		EXEC CashierMovementsGrouped_Select @CashierSessionId, NULL, NULL
		--UPDATE HISTORY FLAG IN CASHIER_SESSIONS
		UPDATE cashier_sessions SET cs_history = 1 WHERE cs_session_id = @pCashierSessionId

 END --CreateCashierMovementsGroupedPerSessionId
 GO
 
 GRANT EXECUTE ON CashierMovementsGroupedPerSessionId TO wggui WITH GRANT OPTION 
 GO
/******* TRIGGERS *******/
