/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 167;

SET @New_ReleaseId = 168;
SET @New_ScriptName = N'UpdateTo_18.168.032.sql';
SET @New_Description = N'TITO Updates';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO


/****** TABLES ******/

--CASHIER_TERMINALS
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = OBJECT_ID(N'[dbo].[cashier_terminals]') AND name = 'ct_gaming_table_id')
	ALTER TABLE dbo.cashier_terminals ADD ct_gaming_table_id int NULL
GO

--CASHIER_MOVEMENTS
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = OBJECT_ID(N'[dbo].[cashier_movements]') AND name = 'cm_gaming_table_session_id')
	ALTER TABLE dbo.cashier_movements ADD cm_gaming_table_session_id bigint NULL
GO

--TIPOS DE MESAS DE JUEGO
IF NOT EXISTS ( SELECT name FROM sys.tables WHERE name = 'gaming_tables_types')
BEGIN
	CREATE TABLE [dbo].[gaming_tables_types](
		[gtt_gaming_table_type_id] [int] IDENTITY(1,1) NOT NULL,
		[gtt_name] [nvarchar](50) NOT NULL,
		[gtt_enabled] [bit] NOT NULL,
	 CONSTRAINT [pk_gaming_tables_types] PRIMARY KEY CLUSTERED 
	(
		[gtt_gaming_table_type_id] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]

	ALTER TABLE [dbo].[gaming_tables_types] ADD  CONSTRAINT [DF_gaming_tables_types_gtt_enabled]  DEFAULT ((0)) FOR [gtt_enabled]
END
GO

--SESIONES DE MESAS DE JUEGO

IF NOT EXISTS ( SELECT name FROM sys.tables WHERE name = 'gaming_tables_sessions')
BEGIN
	CREATE TABLE [dbo].[gaming_tables_sessions](
	[gts_gaming_table_session_id] [bigint] IDENTITY(1,1) NOT NULL,
	[gts_gaming_table_id] [bigint] NOT NULL,
	[gts_cashier_session_id] [bigint] NOT NULL,
	[gts_initial_amount] [money] NULL,
	[gts_own_sales_amount] [money] NULL, -- VENTAS DE FICHAS
	[gts_external_sales_amount] [money] NULL, --VENTAS EXTERNAS DESDE OTRO CAJERO
	[gts_purchase_amount] [money] NULL, --COMPRAS DE FICHAS
	[gts_collected_amount] [money] NULL, --EFECTIVO EN CAJA
	[gts_tips] [money] NULL, --PROPINAS 
	[gts_client_visits] [int] NULL,
	 CONSTRAINT [PK_gaming_tables_sessions] PRIMARY KEY CLUSTERED 
	(
		[gts_gaming_table_session_id] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]
END

--MESAS DE JUEGO
IF NOT EXISTS ( SELECT name FROM sys.tables WHERE name = 'gaming_tables')
BEGIN
	CREATE TABLE [dbo].[gaming_tables](
		[gt_gaming_table_id] [int] IDENTITY(1,1) NOT NULL,
		[gt_code] [nvarchar](20) NOT NULL,
		[gt_type_id] [int] NOT NULL,
		[gt_name] [nvarchar](50) NOT NULL,
		[gt_description] [nvarchar](512) NULL,
		[gt_area_id] [int] NULL,
		[gt_bank_id] [int] NULL,
		[gt_floor_id] [nvarchar](20) NULL,
		[gt_cur_iso_code] [nvarchar](3) NULL,
		[gt_enabled] [bit] NOT NULL,
		[gt_has_integrated_cashier] [bit] NOT NULL,
		[gt_cashier_id] [int] NULL,
	 CONSTRAINT [pk_gaming_tables] PRIMARY KEY CLUSTERED 
	(
		[gt_gaming_table_id] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]

	ALTER TABLE [dbo].[gaming_tables]  WITH CHECK ADD  CONSTRAINT [fk_gaming_tables_areas] FOREIGN KEY([gt_area_id])
	REFERENCES [dbo].[areas] ([ar_area_id])
	ALTER TABLE [dbo].[gaming_tables] CHECK CONSTRAINT [fk_gaming_tables_areas]
	ALTER TABLE [dbo].[gaming_tables]  WITH CHECK ADD  CONSTRAINT [fk_gaming_tables_currencies] FOREIGN KEY([gt_cur_iso_code])
	REFERENCES [dbo].[currencies] ([cur_iso_code])
	ALTER TABLE [dbo].[gaming_tables] CHECK CONSTRAINT [fk_gaming_tables_currencies]
	ALTER TABLE [dbo].[gaming_tables]  WITH CHECK ADD  CONSTRAINT [fk_gt_type_id] FOREIGN KEY([gt_type_id])
	REFERENCES [dbo].[gaming_tables_types] ([gtt_gaming_table_type_id])
	ALTER TABLE [dbo].[gaming_tables] CHECK CONSTRAINT [fk_gt_type_id]
	ALTER TABLE [dbo].[gaming_tables] ADD  CONSTRAINT [DF_gaming_tables_gt_enabled]  DEFAULT ((0)) FOR [gt_enabled]
	ALTER TABLE [dbo].[gaming_tables] ADD  CONSTRAINT [DF_gaming_tables_gt_integrated_cashier]  DEFAULT ((0)) FOR [gt_has_integrated_cashier]
END
GO


/******* FUNCTIONS **********/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        JML
-- Create date: 21-DEC-2013
-- Description:   Get the value of bill 
-- =============================================
CREATE FUNCTION GetBillValue
(
  @MeterCode as Int
)
RETURNS Money
AS
BEGIN
      DECLARE @Value Money

     SELECT  @Value = CASE @MeterCode
                      WHEN 64 THEN 1 
                   WHEN 65 THEN 2 
                   WHEN 66 THEN 5
                   WHEN 67 THEN 10
                   WHEN 68 THEN 20
                   WHEN 69 THEN 25
                   WHEN 70 THEN 50
                   WHEN 71 THEN 100
                   WHEN 72 THEN 200
                   WHEN 73 THEN 250
                   WHEN 74 THEN 500
                   WHEN 75 THEN 1000
                   WHEN 76 THEN 2000
                   WHEN 77 THEN 2500
                   WHEN 78 THEN 5000
                   WHEN 79 THEN 10000
                   WHEN 80 THEN 20000
                   WHEN 81 THEN 25000
                   WHEN 82 THEN 50000
                   WHEN 83 THEN 100000
                   WHEN 84 THEN 200000
                   WHEN 85 THEN 250000
                   WHEN 86 THEN 500000
                   WHEN 87 THEN 1000000
                   ELSE 0 END 

      RETURN @Value

END
GO

GRANT EXECUTE ON [dbo].[GetBillValue] TO [wggui] WITH GRANT OPTION 
GO


/****** RECORDS *******/

--CREATE USER 'GamingTables’ for the gaming tables virtual cashier sessions.
DECLARE @user_id AS INT
IF NOT EXISTS (
               SELECT   1
                 FROM   GUI_USERS
                WHERE   GU_USER_TYPE = 5
              )
BEGIN
  SELECT   @user_id = MAX(ISNULL(GU_USER_ID, 0)) + 1 
    FROM   GUI_USERS                      

  INSERT INTO GUI_USERS ( GU_USER_ID
                        , GU_PROFILE_ID
                        , GU_USERNAME
                        , GU_ENABLED
                        , GU_PASSWORD
                        , GU_NOT_VALID_BEFORE
                        , GU_NOT_VALID_AFTER
                        , GU_LAST_CHANGED
                        , GU_PASSWORD_EXP
                        , GU_PWD_CHG_REQ
                        , GU_FULL_NAME
                       , GU_LOGIN_FAILURES
                        , GU_USER_TYPE
                        )
                 VALUES ( @user_id
                        , 0
                        , 'SYS-GamingTables'
                        , 1
                        , CAST('0000' AS BINARY(40))
                        , GETDATE() - 3
                        , NULL
                        , GETDATE()
                        , NULL
                        , 0
                        , 'SYS-GamingTables'
                        , 0
                        , 5
                        )
END
GO

--GENERAL PARAMS
IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'GamingTables' AND GP_SUBJECT_KEY = 'Enabled')
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE)  VALUES ('GamingTables', 'Enabled', 0)
GO
IF NOT EXISTS ( SELECT 1 FROM general_params WHERE gp_group_key = 'GamingTables' AND gp_subject_key = 'Chips.Weighing.High')
      INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('GamingTables', 'Chips.Weighing.High', '70;15;10;5;0;0;0;0;0;0;0;0;0;')
GO
IF NOT EXISTS ( SELECT 1 FROM general_params WHERE gp_group_key = 'GamingTables' AND gp_subject_key = 'Chips.Weighing.Medium')
      INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('GamingTables', 'Chips.Weighing.Medium','0;0;0;0;70;15;10;5;0;0;0;0;0;')
GO
IF NOT EXISTS ( SELECT 1 FROM general_params WHERE gp_group_key = 'GamingTables' AND gp_subject_key = 'Chips.Weighing.Small')
      INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('GamingTables', 'Chips.Weighing.Small', '0;0;0;0;0;0;0;0;60;15;15;5;5;')
GO
IF NOT EXISTS ( SELECT * FROM GENERAL_PARAMS WHERE gp_group_key = 'Cage' AND gp_subject_key = 'CashierQuickOpen')
   INSERT INTO GENERAL_PARAMS (gp_group_key, gp_subject_key, gp_key_value) VALUES ('Cage', 'CashierQuickOpen', 0)
GO   
IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'GamingTables' AND GP_SUBJECT_KEY = 'Chips.MaxAllowedSales')
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE)  VALUES ('GamingTables', 'Chips.MaxAllowedSales', 0)     
GO
IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'GamingTables' AND GP_SUBJECT_KEY = 'Chips.MaxAllowedPurchases')
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE)  VALUES ('GamingTables', 'Chips.MaxAllowedPurchases', 0)
GO


IF NOT EXISTS ( SELECT 1 FROM cage_amounts WHERE caa_iso_code = 'X01' AND caa_denomination = 1)
BEGIN
INSERT INTO cage_amounts
           (caa_iso_code,caa_denomination,caa_allowed)
     VALUES
           ('X01',1,1)
END
GO

IF NOT EXISTS ( SELECT 1 FROM cage_amounts WHERE caa_iso_code = 'X01' AND caa_denomination = 2)
BEGIN
INSERT INTO cage_amounts
		      (caa_iso_code,caa_denomination,caa_allowed)
     VALUES
           ('X01',2,1)
END
GO

IF NOT EXISTS ( SELECT 1 FROM cage_amounts WHERE caa_iso_code = 'X01' AND caa_denomination = 5)
BEGIN
INSERT INTO cage_amounts
           (caa_iso_code,caa_denomination,caa_allowed)
     VALUES
           ('X01',5,1)
END
GO

IF NOT EXISTS ( SELECT 1 FROM cage_amounts WHERE caa_iso_code = 'X01' AND caa_denomination = 10)
BEGIN
INSERT INTO cage_amounts
           (caa_iso_code,caa_denomination,caa_allowed)
     VALUES
           ('X01',10,1)
END          
GO

IF NOT EXISTS ( SELECT 1 FROM cage_amounts WHERE caa_iso_code = 'X01' AND caa_denomination = 20)
BEGIN
INSERT INTO cage_amounts
           (caa_iso_code,caa_denomination,caa_allowed)
     VALUES
           ('X01',20,1)
END
GO

IF NOT EXISTS ( SELECT 1 FROM cage_amounts WHERE caa_iso_code = 'X01' AND caa_denomination = 25)
BEGIN
INSERT INTO cage_amounts
           (caa_iso_code,caa_denomination,caa_allowed)
     VALUES
           ('X01',25,1)
END
GO

IF NOT EXISTS ( SELECT 1 FROM cage_amounts WHERE caa_iso_code = 'X01' AND caa_denomination = 50)
BEGIN
INSERT INTO cage_amounts
           (caa_iso_code,caa_denomination,caa_allowed)
     VALUES
           ('X01',50,1)
END
GO

IF NOT EXISTS ( SELECT 1 FROM cage_amounts WHERE caa_iso_code = 'X01' AND caa_denomination = 100)
BEGIN
INSERT INTO cage_amounts
           (caa_iso_code,caa_denomination,caa_allowed)
     VALUES
           ('X01',100,1)
END
GO

IF NOT EXISTS ( SELECT 1 FROM cage_amounts WHERE caa_iso_code = 'X01' AND caa_denomination = 250)
BEGIN
INSERT INTO cage_amounts
           (caa_iso_code,caa_denomination,caa_allowed)
     VALUES
           ('X01',250,1)
END
GO

IF NOT EXISTS ( SELECT 1 FROM cage_amounts WHERE caa_iso_code = 'X01' AND caa_denomination = 500)
BEGIN
INSERT INTO cage_amounts
           (caa_iso_code,caa_denomination,caa_allowed)
     VALUES
           ('X01',500,1)
END
GO

IF NOT EXISTS ( SELECT 1 FROM cage_amounts WHERE caa_iso_code = 'X01' AND caa_denomination = 1000)
BEGIN
INSERT INTO cage_amounts
           (caa_iso_code,caa_denomination,caa_allowed)
     VALUES
           ('X01',1000,1)
END
GO

IF NOT EXISTS ( SELECT 1 FROM cage_amounts WHERE caa_iso_code = 'X01' AND caa_denomination = 2000)
BEGIN
INSERT INTO cage_amounts
           (caa_iso_code,caa_denomination,caa_allowed)
     VALUES
           ('X01',2000,1)
END
GO

IF NOT EXISTS ( SELECT 1 FROM cage_amounts WHERE caa_iso_code = 'X01' AND caa_denomination = 5000)
BEGIN
INSERT INTO cage_amounts
           (caa_iso_code,caa_denomination,caa_allowed)
     VALUES
           ('X01',5000,1)
END
GO





