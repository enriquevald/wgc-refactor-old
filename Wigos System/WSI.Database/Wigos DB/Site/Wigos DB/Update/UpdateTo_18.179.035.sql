/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 178;

SET @New_ReleaseId = 179;
SET @New_ScriptName = N'UpdateTo_18.179.035.sql';
SET @New_Description = N'TITO: Procedure GT_Session_Information and Function ApplyExchange ';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/******* STORED  *******/

IF OBJECT_ID (N'dbo.GT_Session_Information', N'P') IS NOT NULL
    DROP PROCEDURE dbo.GT_Session_Information;                 
GO  

CREATE PROCEDURE [dbo].[GT_Session_Information] 
 ( @pBaseType INTEGER
  ,@pDateFrom DATETIME 
  ,@pDateTo DATETIME
  ,@pStatus INTEGER
  ,@pEnabled INTEGER
  ,@pAreaId INTEGER
  ,@pBankId INTEGER
  ,@pChipsISOCode VARCHAR(50)
  ,@pChipsCoinsCode INTEGER
  ,@pValidTypes VARCHAR(4096)
)
AS
BEGIN

----------------------------------------------------------------------------------------------------------------
DECLARE @_BASE_TYPE        AS   INTEGER
DECLARE @_DATE_FROM        AS   DATETIME
DECLARE @_DATE_TO          AS   DATETIME
DECLARE @_STATUS           AS   INTEGER
DECLARE @_ENABLED          AS   INTEGER
DECLARE @_AREA             AS   INTEGER
DECLARE @_BANK             AS   INTEGER
DECLARE @_CHIPS_ISO_CODE   AS   VARCHAR(50)
DECLARE @_CHIPS_COINS_CODE AS   VARCHAR(50)
DECLARE @_DELIMITER        AS   CHAR(1)
DECLARE @_TYPES_TABLE           TABLE(SST_ID INT, SST_VALUE VARCHAR(50)) -- TO STORE THE VALID GAMING TABLE TYPES 

-- MOVEMENTS
DECLARE @_CHIPS_MOVEMENT_PURCHASE_TOTAL AS   INTEGER
DECLARE @_CHIPS_MOVEMENT_SALES_TOTAL    AS   INTEGER
DECLARE @_MOVEMENT_FILLER_IN            AS   INTEGER
DECLARE @_MOVEMENT_FILLER_OUT           AS   INTEGER
DECLARE @_MOVEMENT_CAGE_FILLER_OUT      AS   INTEGER
DECLARE @_MOVEMENT_CAGE_CLOSE_SESSION   AS   INTEGER

----------------------------------------------------------------------------------------------------------------

-- Initialzitation --
SET @_DELIMITER        =   ','
SET @_BASE_TYPE        =   @pBaseType
SET @_DATE_FROM        =   @pDateFrom
SET @_DATE_TO          =   @pDateTo
SET @_STATUS           =   ISNULL(@pStatus, -1)
SET @_ENABLED          =   ISNULL(@pEnabled, -1)
SET @_AREA             =   ISNULL(@pAreaId, 0)
SET @_BANK             =   ISNULL(@pBankId, 0)
SET @_CHIPS_ISO_CODE   =   ISNULL(@pChipsISOCode, 'X01')
SET @_CHIPS_COINS_CODE =   ISNULL(@pChipsCoinsCode, -100)

SET @_CHIPS_MOVEMENT_PURCHASE_TOTAL =   304
SET @_CHIPS_MOVEMENT_SALES_TOTAL    =   303
SET @_MOVEMENT_FILLER_IN            =   2
SET @_MOVEMENT_FILLER_OUT           =   3
SET @_MOVEMENT_CAGE_FILLER_OUT      =   203
SET @_MOVEMENT_CAGE_CLOSE_SESSION   =   201

----------------------------------------------------------------------------------------------------------------
-- CHECK DATE PARAMETERS
IF @_DATE_FROM IS NULL
BEGIN
   -- IF DATE FROM IS NULL, USER FIRST POSIBLE DATE
   SET @_DATE_FROM = CAST('' AS DATETIME)
END
IF @_DATE_TO IS NULL
BEGIN
   -- IF DATE TO IS NULL, QUERY UNTIL CURRENT DATE
   SET @_DATE_TO = CAST(GETDATE() AS DATETIME)
END

-- ASSIGN TYPES PARAMETER INTO TABLE
INSERT INTO @_TYPES_TABLE SELECT * FROM dbo.SplitStringIntoTable(@pValidTypes, @_DELIMITER, DEFAULT)

----------------------------------------------------------------------------------------------------------------

IF @_BASE_TYPE = 0 
BEGIN
   -- REPORT BY GAMING TABLE SESSION
SELECT    GTS_CASHIER_SESSION_ID                                                           
         , CS_NAME                                                                          
         , CS_STATUS                                                                        
         , GT_NAME                                                                          
         , GTT_NAME                                                                         
         , GT_HAS_INTEGRATED_CASHIER                                                        
         , CT_NAME                                                                          
         , GU_USERNAME                                                                      
         , CS_OPENING_DATE                                                                  
         , CS_CLOSING_DATE                                                                  
         , GTS_TOTAL_PURCHASE_AMOUNT                                                        
         , GTS_TOTAL_SALES_AMOUNT                                                           
         , GTS_INITIAL_CHIPS_AMOUNT                                                         
         , GTS_FINAL_CHIPS_AMOUNT                                                           
         , GTS_TIPS                                                                         
         , GTS_COLLECTED_AMOUNT                                                             
         , GTS_CLIENT_VISITS                                                                
         , AR_NAME                                                                             
         , BK_NAME                                                                             
   FROM         GAMING_TABLES_SESSIONS                                                      
  INNER    JOIN CASHIER_SESSIONS      ON CS_SESSION_ID            = GTS_CASHIER_SESSION_ID  
   LEFT    JOIN GUI_USERS             ON GU_USER_ID               = CS_USER_ID              
   LEFT    JOIN GAMING_TABLES         ON GT_GAMING_TABLE_ID       = GTS_GAMING_TABLE_ID     
   LEFT    JOIN GAMING_TABLES_TYPES   ON GTT_GAMING_TABLE_TYPE_ID = GT_TYPE_ID              
   LEFT    JOIN AREAS                 ON GT_AREA_ID               = AR_AREA_ID              
   LEFT    JOIN BANKS                 ON GT_BANK_ID               = BK_BANK_ID              
   LEFT    JOIN CASHIER_TERMINALS     ON GT_CASHIER_ID            = CT_CASHIER_ID           
 WHERE   (CS_OPENING_DATE >= @_DATE_FROM AND (CS_OPENING_DATE < @_DATE_TO))
   AND   (GT_ENABLED = CASE WHEN @_ENABLED = -1 THEN GT_ENABLED ELSE @_ENABLED END)  
   AND   (CS_STATUS  = CASE WHEN @_STATUS  = -1 THEN CS_STATUS  ELSE @_STATUS  END) 
   AND   (GT_AREA_ID = CASE WHEN @_AREA    = 0  THEN GT_AREA_ID ELSE @_AREA    END) 
   AND   (GT_BANK_ID = CASE WHEN @_BANK    = 0  THEN GT_BANK_ID ELSE @_BANK    END) 
   AND   (GT_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE ))
   ;
   
END
ELSE
BEGIN
   -- REPORT BY CASHIER MOVEMENTS

SELECT     CM_SESSION_ID                 
          , CS_NAME                       
          , CS_STATUS                     
          , GT_NAME                       
          , GTT_NAME                      
          , GT_HAS_INTEGRATED_CASHIER     
          , CT_NAME                       
          , GU_USERNAME                   
          , CS_OPENING_DATE               
          , CS_CLOSING_DATE               
          , CM_TOTAL_PURCHASE_AMOUNT      
          , CM_TOTAL_SALES_AMOUNT         
          , CM_INITIAL_CHIPS_AMOUNT       
          , CM_FINAL_CHIPS_AMOUNT         
          , CM_TIPS                       
          , CASE WHEN CM_COLLECTED_AMOUNT < 0 THEN 0 
                ELSE CM_COLLECTED_AMOUNT END as CM_COLLECTED_AMOUNT 
          , GTS_CLIENT_VISITS             
          , AR_NAME                       
          , BK_NAME                       
 FROM      (                             
 SELECT    CM_SESSION_ID                                                                      
         , CS_NAME                                                                            
         , CS_STATUS                                                                          
         , GT_NAME                                                                            
         , GTT_NAME                                                                           
         , GT_HAS_INTEGRATED_CASHIER                                                          
         , CT_NAME                                                                            
         , GU_USERNAME                                                                        
         , CS_OPENING_DATE                                                                    
         , CS_CLOSING_DATE                                                                    
         , SUM(CASE WHEN CM_TYPE                  = @_CHIPS_MOVEMENT_PURCHASE_TOTAL
                    THEN CM_ADD_AMOUNT ELSE 0 END
              )  AS CM_TOTAL_PURCHASE_AMOUNT           
         , SUM(CASE WHEN CM_TYPE                  = @_CHIPS_MOVEMENT_SALES_TOTAL
                    THEN CM_SUB_AMOUNT ELSE 0 END
              )  AS CM_TOTAL_SALES_AMOUNT                  
         , SUM(CASE WHEN CM_CURRENCY_ISO_CODE     = @_CHIPS_ISO_CODE             
                     AND CM_TYPE                  = @_MOVEMENT_FILLER_IN
                    THEN CM_ADD_AMOUNT ELSE 0 END
              )  AS CM_INITIAL_CHIPS_AMOUNT              
         , SUM(CASE WHEN CM_CURRENCY_ISO_CODE     = @_CHIPS_ISO_CODE             
                     AND CM_TYPE                  = @_MOVEMENT_FILLER_OUT
                    THEN CM_SUB_AMOUNT ELSE 0 END
              )  AS CM_FINAL_CHIPS_AMOUNT                  
         , SUM(CASE WHEN CM_TYPE                  = @_MOVEMENT_CAGE_FILLER_OUT
                     AND CM_CURRENCY_ISO_CODE     = @_CHIPS_ISO_CODE             
                     AND CM_CURRENCY_DENOMINATION = @_CHIPS_COINS_CODE
                    THEN CM_SUB_AMOUNT ELSE 0 END
              )  AS CM_TIPS                            
         , SUM(CASE WHEN CM_CURRENCY_ISO_CODE IS NULL OR CM_CURRENCY_ISO_CODE <> @_CHIPS_ISO_CODE THEN 
                    CASE WHEN CM_TYPE   = @_MOVEMENT_FILLER_IN            THEN -1 * DBO.ApplyExchange(CM_ADD_AMOUNT, CM_CURRENCY_ISO_CODE) 
                         WHEN CM_TYPE   = @_MOVEMENT_FILLER_OUT           THEN      DBO.ApplyExchange(CM_SUB_AMOUNT, CM_CURRENCY_ISO_CODE) 
                         WHEN CM_TYPE   = @_MOVEMENT_CAGE_CLOSE_SESSION   THEN      DBO.ApplyExchange(CM_INITIAL_BALANCE, CM_CURRENCY_ISO_CODE) 
                         ELSE 0 END 
               ELSE 0 END
              )  AS CM_COLLECTED_AMOUNT 
         , GTS_CLIENT_VISITS                                                                  
         , AR_NAME                                                                               
         , BK_NAME                                                                               
   FROM        CASHIER_MOVEMENTS                                                             
  INNER   JOIN    CASHIER_SESSIONS        ON CS_SESSION_ID            = CM_SESSION_ID           
  INNER   JOIN    GAMING_TABLES_SESSIONS  ON CS_SESSION_ID            = GTS_CASHIER_SESSION_ID  
   LEFT   JOIN    GUI_USERS               ON GU_USER_ID               = CS_USER_ID              
   LEFT   JOIN    GAMING_TABLES           ON GT_GAMING_TABLE_ID       = GTS_GAMING_TABLE_ID     
   LEFT   JOIN    GAMING_TABLES_TYPES     ON GTT_GAMING_TABLE_TYPE_ID = GT_TYPE_ID              
   LEFT   JOIN    AREAS                   ON GT_AREA_ID               = AR_AREA_ID              
   LEFT   JOIN    BANKS                   ON GT_BANK_ID               = BK_BANK_ID              
   LEFT   JOIN    CASHIER_TERMINALS       ON GT_CASHIER_ID            = CT_CASHIER_ID           
 WHERE   CM_MOVEMENT_ID IN 
            (
               SELECT   CM_MOVEMENT_ID 
                 FROM   CASHIER_MOVEMENTS  
                WHERE   (CM_DATE >= @_DATE_FROM AND CM_DATE <= @_DATE_TO) 
                  AND   CM_SESSION_ID = CS_SESSION_ID 
            ) 
   AND   (GT_ENABLED = CASE WHEN @_ENABLED = -1 THEN GT_ENABLED ELSE @_ENABLED END)  
   AND   (CS_STATUS  = CASE WHEN @_STATUS  = -1 THEN CS_STATUS  ELSE @_STATUS  END)             
   AND   (GT_AREA_ID = CASE WHEN @_AREA    = 0  THEN GT_AREA_ID ELSE @_AREA    END) 
   AND   (GT_BANK_ID = CASE WHEN @_BANK    = 0  THEN GT_BANK_ID ELSE @_BANK    END) 
   AND   (GT_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE ))
                  
  GROUP BY  CM_SESSION_ID         
      , CS_NAME                   
      , CS_STATUS                 
      , GT_NAME                   
      , GTT_NAME                  
      , GT_HAS_INTEGRATED_CASHIER 
      , CT_NAME                   
      , GU_USERNAME               
      , CS_OPENING_DATE           
      , CS_CLOSING_DATE           
      , GTS_CLIENT_VISITS         
      , AR_NAME                   
      , BK_NAME                   
  ) AS   CASHIER_MOVEMENTS        
  ;
  
END

END -- END PROCEDURE
GO

-- PERMISSIONS

GRANT EXECUTE ON [dbo].[GT_Session_Information] TO [wggui] WITH GRANT OPTION
GO

/******* FUNCTIONS *******/

IF OBJECT_ID (N'dbo.ApplyExchange', N'FN') IS NOT NULL
    DROP FUNCTION DBO.ApplyExchange;                 
GO  

CREATE FUNCTION ApplyExchange
(@pAmount  MONEY,
  @pIsoCode VARCHAR(3))
RETURNS MONEY
AS
BEGIN

      DECLARE @Exchanged      MONEY
      DECLARE @Change         MONEY
      DECLARE @NumDecimals INT
      
      IF(@pIsoCode IS NULL)
            SET @Exchanged = @pAmount;
      ELSE
      BEGIN
            SELECT 
                  @Change = CE_CHANGE,
                  @NumDecimals = CE_NUM_DECIMALS
            FROM CURRENCY_EXCHANGE
            WHERE CE_TYPE = 0
                    AND CE_CURRENCY_ISO_CODE = @pIsoCode

            SET @NumDecimals = ISNULL(@NumDecimals, 0)     
            SET @Exchanged = ROUND(ISNULL(@Change * @pAmount, 0), @NumDecimals, 1)   

      END
      
      
      RETURN @Exchanged
END -- ApplyExchange
GO
GRANT EXECUTE ON [dbo].[ApplyExchange] TO [wggui]
GO
