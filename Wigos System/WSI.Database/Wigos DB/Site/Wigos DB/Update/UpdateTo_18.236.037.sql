/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 235;

SET @New_ReleaseId = 236;
SET @New_ScriptName = N'UpdateTo_18.236.037.sql';
SET @New_Description = N'Added column gui_user_id to table progressives_provisions, change tips to a system concept.';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/******* TABLES *******/

--  PROGRESSIVES
--
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[progressives_provisions]') and name = 'pgp_gui_user_id')
  ALTER TABLE dbo.progressives_provisions ADD pgp_gui_user_id int NULL
GO

-- CAGE CONCEPTS
--
/*    ELIMINACI�N DEL ANTIGUO CONCEPTO PROPINAS PRE-DEFINIDO EN EL SISTEMA (CON ID = 1000)
      Y LOS USOS DE ESTE CONCEPTO PARA LOS CAJEROS Y MESAS DE JUEGO */      

      IF EXISTS (SELECT TOP 1 CC_CONCEPT_ID 
                   FROM CAGE_CONCEPTS 
                  WHERE CC_CONCEPT_ID = 1000 
                    AND CC_DESCRIPTION = 'Propinas')
      BEGIN
            DELETE FROM CAGE_SOURCE_TARGET_CONCEPTS WHERE CSTC_CONCEPT_ID = 1000
            DELETE FROM CAGE_METERS                 WHERE CM_CONCEPT_ID   = 1000
            DELETE FROM CAGE_SESSION_METERS         WHERE CSM_CONCEPT_ID  = 1000
      END

      DELETE FROM CAGE_CONCEPTS WHERE CC_CONCEPT_ID = 1000 AND CC_DESCRIPTION = 'Propinas'

/*    CREACI�N DEL CONCEPTO PROPINAS PRE-DEFINIDO EN EL SISTEMA (CON ID = 6)
      SE CREAN USOS DE ESTE CONCEPTO PARA LOS CAJEROS Y MESAS DE JUEGO */      

        -- Propinas ([6])
      IF NOT EXISTS (SELECT TOP 1 CC_CONCEPT_ID FROM CAGE_CONCEPTS WHERE CC_CONCEPT_ID = 6)
        INSERT INTO   CAGE_CONCEPTS
                    ( CC_CONCEPT_ID, CC_DESCRIPTION, CC_NAME
                    , CC_IS_PROVISION, CC_SHOW_IN_REPORT
                    , CC_TYPE, CC_ENABLED )
             VALUES
                    ( 6, 'Propinas', 'Propinas'
                    , 1, 1
                    , 1, 1)
                  
      -- PROPINAS-CAJAS ([6]-[10])
      -- Create Relation
      IF NOT EXISTS (SELECT TOP 1 * FROM CAGE_SOURCE_TARGET_CONCEPTS WHERE CSTC_CONCEPT_ID = 6 
                                                                       AND CSTC_SOURCE_TARGET_ID = 10)
        INSERT INTO  CAGE_SOURCE_TARGET_CONCEPTS
                   ( CSTC_CONCEPT_ID, CSTC_SOURCE_TARGET_ID
                   , CSTC_TYPE, CSTC_ONLY_NATIONAL_CURRENCY
                   , CSTC_ENABLED
                   , CSTC_CASHIER_ACTION
                   , CSTC_CASHIER_BTN_GROUP
                   , CSTC_CASHIER_CONTAINER)
             VALUES
                   ( 6, 10
                   , 0, 0
                   , 1
                   , 0 
                   , 'STR_CASHIER_CONCEPTS_IN'
                   , 0)

    -- Create Meters
    EXEC CageCreateMeters
                @pSourceTargetId = 10
              , @pConceptId = 6
              , @pCageSessionId = NULL
              , @CreateMetersOption = 3
              , @CurrencyForced = NULL

      -- PROPINAS-MESAS ([6]-[11])
      -- Create Relation
      IF NOT EXISTS (SELECT TOP 1 * FROM CAGE_SOURCE_TARGET_CONCEPTS WHERE CSTC_CONCEPT_ID = 6 
                                                                       AND CSTC_SOURCE_TARGET_ID = 11)
        INSERT INTO CAGE_SOURCE_TARGET_CONCEPTS
             ( CSTC_CONCEPT_ID, CSTC_SOURCE_TARGET_ID
             , CSTC_TYPE, CSTC_ONLY_NATIONAL_CURRENCY
             , CSTC_ENABLED )
        VALUES
             ( 6, 11
             , 0, 0
             , 1 )
    -- Create Meters
    EXEC CageCreateMeters
                @pSourceTargetId = 11
              , @pConceptId = 6
              , @pCageSessionId = NULL
              , @CreateMetersOption = 3
              , @CurrencyForced = NULL
GO

/******* INDEXES *******/

/******* RECORDS *******/

IF NOT EXISTS (SELECT TOP 1 GP_KEY_VALUE FROM GENERAL_PARAMS
                      WHERE GP_GROUP_KEY   = 'Cage' AND GP_SUBJECT_KEY = 'Meters.Report.ShowItemsWithZeroAmount')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE)
       VALUES ('Cage', 'Meters.Report.ShowItemsWithZeroAmount', 1)
      

IF NOT EXISTS (SELECT TOP 1 GP_KEY_VALUE FROM GENERAL_PARAMS 
                      WHERE GP_GROUP_KEY = 'Cage' AND GP_SUBJECT_KEY = 'Transfer.CashDesk.HideItems')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('Cage', 'Transfer.CashDesk.HideItems', 1)

GO

/******* STORED PROCEDURES *******/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CageGetGlobalReportData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CageGetGlobalReportData]
GO

CREATE PROCEDURE [dbo].[CageGetGlobalReportData]
        @pCageSessionIds NVARCHAR(MAX) = NULL
AS
BEGIN
      DECLARE @NationalIsoCode NVARCHAR(3)
      DECLARE @CageOperationType_FromTerminal INT

      SET @CageOperationType_FromTerminal = 104
      
      -- TABLE[0]: Get cage stock
    SELECT CGS_ISO_CODE                                          
         , CGS_DENOMINATION                                      
             , CGS_QUANTITY 
      FROM (SELECT CGS_ISO_CODE                                          
                   , CGS_DENOMINATION                                      
                         , CGS_QUANTITY                                          
                    FROM CAGE_STOCK
                  
                  UNION ALL
                  
                  SELECT C.CH_ISO_CODE AS CGS_ISO_CODE
                        , C.CH_DENOMINATION AS CGS_DENOMINATION
                        , CST.CHSK_QUANTITY AS CGS_QUANTITY
                    FROM CHIPS_STOCK AS CST
                           INNER JOIN CHIPS AS C ON CST.CHSK_CHIP_ID = C.CH_CHIP_ID
                  ) AS _tbl         
                                                   
  ORDER BY CGS_ISO_CODE                                          
              ,CASE WHEN CGS_DENOMINATION >= 0 THEN CGS_DENOMINATION 
                      ELSE CGS_DENOMINATION * (-100000) END -- show negative denominations at the end (coins, cheks, etc.)

      IF @pCageSessionIds IS NOT NULL
      BEGIN
            -- Session IDs Split
            SELECT SST_VALUE AS CAGE_SESSION_ID INTO #TMP_CAGE_SESSIONS FROM [SplitStringIntoTable] (@pCageSessionIds,','  , 1)
      
          -- TABLE[1]: Get cage sessions information
            SELECT      CSM.CSM_SOURCE_TARGET_ID AS CM_SOURCE_TARGET_ID
                    , CSM.CSM_CONCEPT_ID AS CM_CONCEPT_ID
                    , CSM.CSM_ISO_CODE AS CM_ISO_CODE
                    , SUM(CSM.CSM_VALUE_IN) AS CM_VALUE_IN
                    , SUM(CSM.CSM_VALUE_OUT) AS CM_VALUE_OUT
                    , CST.CST_SOURCE_TARGET_NAME
                    , CC.CC_DESCRIPTION
              FROM      CAGE_SESSION_METERS AS CSM
                        INNER JOIN CAGE_SOURCE_TARGET_CONCEPTS AS CSTC ON CSTC.CSTC_CONCEPT_ID = CSM.CSM_CONCEPT_ID AND CSTC.CSTC_SOURCE_TARGET_ID = CSM.CSM_SOURCE_TARGET_ID
                        INNER JOIN CAGE_CONCEPTS AS CC ON CC.CC_CONCEPT_ID = CSTC.CSTC_CONCEPT_ID
                        INNER JOIN CAGE_SOURCE_TARGET AS CST ON CST.CST_SOURCE_TARGET_ID = CSTC.CSTC_SOURCE_TARGET_ID
            WHERE      CSM.CSM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
               AND      CC.CC_SHOW_IN_REPORT = 1
               AND      CC.CC_ENABLED = 1
               AND      CC.CC_TYPE = 1
             AND  CSTC.CSTC_ENABLED = 1
        GROUP BY  CSM.CSM_SOURCE_TARGET_ID, CSM.CSM_CONCEPT_ID, CSM.CSM_ISO_CODE, CST.CST_SOURCE_TARGET_NAME, CC.CC_DESCRIPTION
        ORDER BY  CSM.CSM_SOURCE_TARGET_ID                   

            -- TABLE[2]: Get dynamic liabilities
            SELECT  CC.CC_CONCEPT_ID AS LIABILITY_CONCEPT_ID
                    , CC.CC_DESCRIPTION AS LIABILITY_NAME 
                    , CSM.CSM_ISO_CODE AS LIABILITY_ISO_CODE
                    , SUM(CSM.CSM_VALUE) AS LIABILITY_VALUE
              FROM CAGE_SESSION_METERS AS CSM
                        INNER JOIN CAGE_CONCEPTS AS CC ON CC.CC_CONCEPT_ID = CSM.CSM_CONCEPT_ID
            WHERE CC.CC_ENABLED = 1
                    AND CC.CC_SHOW_IN_REPORT = 1
              AND CC.CC_IS_PROVISION = 1
              AND CSM.CSM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS) 
      GROUP BY CC.CC_CONCEPT_ID, CC.CC_DESCRIPTION, CSM.CSM_ISO_CODE
      ORDER BY CC.CC_CONCEPT_ID ASC
      
            -- TABLE[3]: Get session sumary
            SELECT @NationalIsoCode = GP_KEY_VALUE 
              FROM GENERAL_PARAMS 
             WHERE GP_GROUP_KEY = 'RegionalOptions' 
               AND GP_SUBJECT_KEY = 'CurrencyISOCode'

            SELECT CMD_ISO_CODE AS ISO_CODE
               , ISNULL(ORIGIN, 0) AS ORIGIN
               , SUM(CMD_DEPOSITS) AS DEPOSITS
               , SUM(CMD_WITHDRAWALS) AS WITHDRAWALS
                  
              FROM      (SELECT CMD_ISO_CODE
                               , ORIGIN
                               , SUM(CMD_DEPOSITS) CMD_DEPOSITS
                               , SUM(CMD_WITHDRAWALS) CMD_WITHDRAWALS
                           
                           FROM     (SELECT CASE WHEN CMD_QUANTITY >= 0 THEN NULL
                                                     ELSE CMD_QUANTITY END  AS ORIGIN
                                           , CMD_ISO_CODE
                                           , CMD_DENOMINATION
                                           , CASE WHEN CGM_TYPE >= 100 AND CGM_TYPE NOT IN (2, 102, 99, 199) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY < 0 THEN CMD_DENOMINATION
                                                                                                                                                                          ELSE CMD_QUANTITY * CMD_DENOMINATION END), 0)
                                                     ELSE 0 END AS CMD_DEPOSITS
                                           , CASE WHEN CGM_TYPE < 100 AND CGM_TYPE NOT IN (2, 102, 99, 199) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY < 0 THEN CMD_DENOMINATION             
                                                                                                                                                                           ELSE CMD_QUANTITY * CMD_DENOMINATION END), 0)                              
                                                      ELSE 0 END AS CMD_WITHDRAWALS

                                      FROM CAGE_MOVEMENTS 
                                               INNER JOIN   CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          
                                     WHERE CGM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)

                                GROUP BY CMD_ISO_CODE, CMD_DENOMINATION, CGM_TYPE, CMD_QUANTITY
                                ) AS T1
                                
                        GROUP BY ORIGIN,CMD_ISO_CODE

                        UNION ALL 

                        --The next one is for the Stackers / Terminals collection
                        SELECT @NationalIsoCode AS CMD_ISO_CODE                                                                                     
                              , 0 AS ORIGIN
                             , CASE WHEN ISNULL(MCD_FACE_VALUE,0) < 0 THEN MCD_NUM_COLLECTED    
                                         ELSE (ISNULL(MCD_FACE_VALUE,0) * ISNULL(MCD_NUM_COLLECTED,0)) END AS CMD_DEPOSITS     
                              , 0 AS CMD_WITHDRAWALS                                                                                     
                          FROM MONEY_COLLECTION_DETAILS                                                                                    
                                INNER JOIN MONEY_COLLECTIONS ON MONEY_COLLECTIONS.MC_COLLECTION_ID = MONEY_COLLECTION_DETAILS.MCD_COLLECTION_ID  
                                INNER JOIN CAGE_MOVEMENTS ON MC_COLLECTION_ID = CAGE_MOVEMENTS.CGM_MC_COLLECTION_ID                              
                         WHERE CAGE_MOVEMENTS.CGM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
                           AND CAGE_MOVEMENTS.CGM_TYPE = @CAGEOPERATIONTYPE_FROMTERMINAL
                           AND ISNULL(MCD_NUM_COLLECTED,0) > 0
                           
                        ) AS _TBL_GLB

         GROUP BY _TBL_GLB.CMD_ISO_CODE, _TBL_GLB.ORIGIN
         ORDER BY _TBL_GLB.CMD_ISO_CODE

         DROP TABLE #TMP_CAGE_SESSIONS 

      END -- IF @pCageSessionIds IS NOT NULL THEN
      
      ELSE
      BEGIN
      
            -- TABLE[1]: Get cage sessions information
            SELECT CM.CM_SOURCE_TARGET_ID
                  , CM.CM_CONCEPT_ID
                  , CM.CM_ISO_CODE
                  , CM.CM_VALUE_IN
                  , CM.CM_VALUE_OUT
                  , CST.CST_SOURCE_TARGET_NAME
                  , CC.CC_DESCRIPTION
              FROM CAGE_METERS AS CM
                     INNER JOIN CAGE_SOURCE_TARGET_CONCEPTS AS CSTC ON CSTC.CSTC_CONCEPT_ID = CM.CM_CONCEPT_ID AND CSTC.CSTC_SOURCE_TARGET_ID = CM.CM_SOURCE_TARGET_ID
                     INNER JOIN CAGE_CONCEPTS AS CC ON CC.CC_CONCEPT_ID = CSTC.CSTC_CONCEPT_ID
                     INNER JOIN CAGE_SOURCE_TARGET AS CST ON CST.CST_SOURCE_TARGET_ID = CSTC.CSTC_SOURCE_TARGET_ID
            WHERE CC.CC_TYPE = 1
               AND CC.CC_ENABLED = 1
             AND CSTC.CSTC_ENABLED = 1
             AND  CC.CC_SHOW_IN_REPORT = 1
        ORDER BY CM.CM_SOURCE_TARGET_ID 

            -- TABLE[2]: Get dynamic liabilities
            SELECT CC.CC_CONCEPT_ID AS LIABILITY_CONCEPT_ID
                  , CC.CC_DESCRIPTION AS LIABILITY_NAME 
                   , CM.CM_ISO_CODE AS LIABILITY_ISO_CODE
                  , SUM(CM.CM_VALUE) AS LIABILITY_VALUE
              FROM CAGE_METERS AS CM
                     INNER JOIN CAGE_CONCEPTS AS CC ON CC.CC_CONCEPT_ID = CM.CM_CONCEPT_ID
            WHERE CC.CC_ENABLED = 1
                    AND CC.CC_SHOW_IN_REPORT = 1
              AND CC.CC_IS_PROVISION = 1
      GROUP BY CC.CC_CONCEPT_ID, CC.CC_DESCRIPTION, CM.CM_ISO_CODE
      ORDER BY CC.CC_CONCEPT_ID ASC 
      
                  -- TABLE[3]: Get session sumary

            SELECT @NationalIsoCode = GP_KEY_VALUE 
              FROM GENERAL_PARAMS 
             WHERE GP_GROUP_KEY = 'RegionalOptions' 
               AND GP_SUBJECT_KEY = 'CurrencyISOCode'

            SELECT CMD_ISO_CODE AS ISO_CODE
               , ISNULL(ORIGIN, 0) AS ORIGIN
               , SUM(CMD_DEPOSITS) AS DEPOSITS
               , SUM(CMD_WITHDRAWALS) AS WITHDRAWALS
                  
              FROM      (SELECT CMD_ISO_CODE
                               , ORIGIN
                               , SUM(CMD_DEPOSITS) CMD_DEPOSITS
                               , SUM(CMD_WITHDRAWALS) CMD_WITHDRAWALS
                           
                           FROM     (SELECT CASE WHEN CMD_QUANTITY >= 0 THEN NULL
                                                     ELSE CMD_QUANTITY END  AS ORIGIN
                                           , CMD_ISO_CODE
                                           , CMD_DENOMINATION
                                           , CASE WHEN CGM_TYPE >= 100 AND CGM_TYPE NOT IN (2, 102, 99, 199) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY < 0 THEN CMD_DENOMINATION
                                                                                                                                                                          ELSE CMD_QUANTITY * CMD_DENOMINATION END), 0)
                                                     ELSE 0 END AS CMD_DEPOSITS
                                           , CASE WHEN CGM_TYPE < 100 AND CGM_TYPE NOT IN (2, 102, 99, 199) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY < 0 THEN CMD_DENOMINATION             
                                                                                                                                                                           ELSE CMD_QUANTITY * CMD_DENOMINATION END), 0)                              
                                                      ELSE 0 END AS CMD_WITHDRAWALS

                                      FROM CAGE_MOVEMENTS 
                                               INNER JOIN   CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          

                                GROUP BY CMD_ISO_CODE, CMD_DENOMINATION, CGM_TYPE, CMD_QUANTITY
                                ) AS T1
                                
                        GROUP BY ORIGIN,CMD_ISO_CODE

                        UNION ALL 

                        --The next one is for the Stackers / Terminals collection
                        SELECT @NationalIsoCode AS CMD_ISO_CODE                                                                                     
                              , 0 AS ORIGIN
                             , CASE WHEN ISNULL(MCD_FACE_VALUE,0) < 0 THEN MCD_NUM_COLLECTED    
                                         ELSE (ISNULL(MCD_FACE_VALUE,0) * ISNULL(MCD_NUM_COLLECTED,0)) END AS CMD_DEPOSITS     
                              , 0 AS CMD_WITHDRAWALS                                                                                     
                          FROM MONEY_COLLECTION_DETAILS                                                                                    
                                INNER JOIN MONEY_COLLECTIONS ON MONEY_COLLECTIONS.MC_COLLECTION_ID = MONEY_COLLECTION_DETAILS.MCD_COLLECTION_ID  
                                INNER JOIN CAGE_MOVEMENTS ON MC_COLLECTION_ID = CAGE_MOVEMENTS.CGM_MC_COLLECTION_ID                              
                         WHERE CAGE_MOVEMENTS.CGM_TYPE = @CAGEOPERATIONTYPE_FROMTERMINAL
                           AND ISNULL(MCD_NUM_COLLECTED,0) > 0
                           
                        ) AS _TBL_GLB

         GROUP BY _TBL_GLB.CMD_ISO_CODE, _TBL_GLB.ORIGIN
         ORDER BY _TBL_GLB.CMD_ISO_CODE

      END --ELSE
                   
END -- CageGetGlobalReportData

GO

GRANT EXECUTE ON [dbo].[CageGetGlobalReportData] TO [wggui] WITH GRANT OPTION 
GO
