/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 210;

SET @New_ReleaseId = 211;
SET @New_ScriptName = N'UpdateTo_18.211.037.sql';
SET @New_Description = N'Deleted GP: Cage - CageAllowTotalImbalance & AllowDenominationImbalance; Added GP: GamingTables & Patterns; Pattern: Added an item into the catalog.';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/******* TABLES *******/

/******* INDEXES *******/

/******* RECORDS *******/

IF EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='Cage' AND GP_SUBJECT_KEY='CageAllowTotalImbalance')
  DELETE FROM general_params WHERE GP_GROUP_KEY ='Cage' AND GP_SUBJECT_KEY='CageAllowTotalImbalance'

IF EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='Cage' AND GP_SUBJECT_KEY='AllowDenominationImbalance')
  DELETE FROM general_params WHERE GP_GROUP_KEY ='Cage' AND GP_SUBJECT_KEY='AllowDenominationImbalance'

IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='PSAClient' AND GP_SUBJECT_KEY='Salidas.Redondeo')
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('PSAClient', 'Salidas.Redondeo', '0')

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'GamingTables' AND GP_SUBJECT_KEY = 'Cashier.Mode')
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE)  VALUES ('GamingTables', 'Cashier.Mode', '0')

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'GamingTables' AND GP_SUBJECT_KEY = 'Cashier.Mode.DealerCopy.Title')
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE)  VALUES ('GamingTables', 'Cashier.Mode.DealerCopy.Title', 'Copia Dealer')

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'GamingTables' AND GP_SUBJECT_KEY = 'Cashier.Mode.DealerCopy.Signature')
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE)  VALUES ('GamingTables', 'Cashier.Mode.DealerCopy.Signature', '0')

IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Pattern' AND GP_SUBJECT_KEY = 'Enabled')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Pattern', 'Enabled', '0')

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'WigosGUI' AND GP_SUBJECT_KEY ='HideTaxReport')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('WigosGUI', 'HideTaxReport', '1')

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY ='ReloadUserPermissionsMinutes')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Cashier', 'ReloadUserPermissionsMinutes', '15')

-- Salto de los contadores de transferencias
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (212992, 1, 0, GETDATE())
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (131088, 0, N'Modificado el valor m�ximo del contador', N'Modificado el valor m�ximo del contador', 1)
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (131088, 1, 0, GETDATE())

GO
