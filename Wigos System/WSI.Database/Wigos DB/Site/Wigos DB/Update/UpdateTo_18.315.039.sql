/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 314;

SET @New_ReleaseId = 315;
SET @New_ScriptName = N'UpdateTo_18.315.039.sql';
SET @New_Description = N'Historificacion;';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/******* TABLES  *******/

/******* INDEXES *******/

/******* RECORDS *******/


/****** Object:  Table [dbo].[h_m2d_tmh]    Script Date: 03/18/2016 13:13:31 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[h_m2d_tmh]') AND type in (N'U'))
DROP TABLE [dbo].[h_m2d_tmh]
GO


GO

/****** Object:  Table [dbo].[h_m2d_tmh]    Script Date: 03/18/2016 13:13:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[h_m2d_tmh](
	[x2d_date] [int] NOT NULL,
	[x2d_weekday] [tinyint] NOT NULL,
	[x2d_id] [int] NOT NULL,
	[x2d_meter_id] [int] NOT NULL,
	[x2d_meter_item] [int] NOT NULL,
	[x2d_00_min] [decimal](12, 3) NULL,
	[x2d_00_max] [decimal](12, 3) NULL,
	[x2d_00_acc] [decimal](12, 3) NULL,
	[x2d_00_avg] [decimal](12, 3) NULL,
	[x2d_00_num] [bigint] NULL,
	[x2d_01_min] [decimal](12, 3) NULL,
	[x2d_01_max] [decimal](12, 3) NULL,
	[x2d_01_acc] [decimal](12, 3) NULL,
	[x2d_01_avg] [decimal](12, 3) NULL,
	[x2d_01_num] [bigint] NULL,
	[x2d_02_min] [decimal](12, 3) NULL,
	[x2d_02_max] [decimal](12, 3) NULL,
	[x2d_02_acc] [decimal](12, 3) NULL,
	[x2d_02_avg] [decimal](12, 3) NULL,
	[x2d_02_num] [bigint] NULL,
	[x2d_03_min] [decimal](12, 3) NULL,
	[x2d_03_max] [decimal](12, 3) NULL,
	[x2d_03_acc] [decimal](12, 3) NULL,
	[x2d_03_avg] [decimal](12, 3) NULL,
	[x2d_03_num] [bigint] NULL,
	[x2d_04_min] [decimal](12, 3) NULL,
	[x2d_04_max] [decimal](12, 3) NULL,
	[x2d_04_acc] [decimal](12, 3) NULL,
	[x2d_04_avg] [decimal](12, 3) NULL,
	[x2d_04_num] [bigint] NULL,
	[x2d_05_min] [decimal](12, 3) NULL,
	[x2d_05_max] [decimal](12, 3) NULL,
	[x2d_05_acc] [decimal](12, 3) NULL,
	[x2d_05_avg] [decimal](12, 3) NULL,
	[x2d_05_num] [bigint] NULL,
	[x2d_06_min] [decimal](12, 3) NULL,
	[x2d_06_max] [decimal](12, 3) NULL,
	[x2d_06_acc] [decimal](12, 3) NULL,
	[x2d_06_avg] [decimal](12, 3) NULL,
	[x2d_06_num] [bigint] NULL,
	[x2d_07_min] [decimal](12, 3) NULL,
	[x2d_07_max] [decimal](12, 3) NULL,
	[x2d_07_acc] [decimal](12, 3) NULL,
	[x2d_07_avg] [decimal](12, 3) NULL,
	[x2d_07_num] [bigint] NULL,
	[x2d_08_min] [decimal](12, 3) NULL,
	[x2d_08_max] [decimal](12, 3) NULL,
	[x2d_08_acc] [decimal](12, 3) NULL,
	[x2d_08_avg] [decimal](12, 3) NULL,
	[x2d_08_num] [bigint] NULL,
	[x2d_09_min] [decimal](12, 3) NULL,
	[x2d_09_max] [decimal](12, 3) NULL,
	[x2d_09_acc] [decimal](12, 3) NULL,
	[x2d_09_avg] [decimal](12, 3) NULL,
	[x2d_09_num] [bigint] NULL,
	[x2d_10_min] [decimal](12, 3) NULL,
	[x2d_10_max] [decimal](12, 3) NULL,
	[x2d_10_acc] [decimal](12, 3) NULL,
	[x2d_10_avg] [decimal](12, 3) NULL,
	[x2d_10_num] [bigint] NULL,
	[x2d_11_min] [decimal](12, 3) NULL,
	[x2d_11_max] [decimal](12, 3) NULL,
	[x2d_11_acc] [decimal](12, 3) NULL,
	[x2d_11_avg] [decimal](12, 3) NULL,
	[x2d_11_num] [bigint] NULL,
	[x2d_12_min] [decimal](12, 3) NULL,
	[x2d_12_max] [decimal](12, 3) NULL,
	[x2d_12_acc] [decimal](12, 3) NULL,
	[x2d_12_avg] [decimal](12, 3) NULL,
	[x2d_12_num] [bigint] NULL,
	[x2d_13_min] [decimal](12, 3) NULL,
	[x2d_13_max] [decimal](12, 3) NULL,
	[x2d_13_acc] [decimal](12, 3) NULL,
	[x2d_13_avg] [decimal](12, 3) NULL,
	[x2d_13_num] [bigint] NULL,
	[x2d_14_min] [decimal](12, 3) NULL,
	[x2d_14_max] [decimal](12, 3) NULL,
	[x2d_14_acc] [decimal](12, 3) NULL,
	[x2d_14_avg] [decimal](12, 3) NULL,
	[x2d_14_num] [bigint] NULL,
	[x2d_15_min] [decimal](12, 3) NULL,
	[x2d_15_max] [decimal](12, 3) NULL,
	[x2d_15_acc] [decimal](12, 3) NULL,
	[x2d_15_avg] [decimal](12, 3) NULL,
	[x2d_15_num] [bigint] NULL,
	[x2d_16_min] [decimal](12, 3) NULL,
	[x2d_16_max] [decimal](12, 3) NULL,
	[x2d_16_acc] [decimal](12, 3) NULL,
	[x2d_16_avg] [decimal](12, 3) NULL,
	[x2d_16_num] [bigint] NULL,
	[x2d_17_min] [decimal](12, 3) NULL,
	[x2d_17_max] [decimal](12, 3) NULL,
	[x2d_17_acc] [decimal](12, 3) NULL,
	[x2d_17_avg] [decimal](12, 3) NULL,
	[x2d_17_num] [bigint] NULL,
	[x2d_18_min] [decimal](12, 3) NULL,
	[x2d_18_max] [decimal](12, 3) NULL,
	[x2d_18_acc] [decimal](12, 3) NULL,
	[x2d_18_avg] [decimal](12, 3) NULL,
	[x2d_18_num] [bigint] NULL,
	[x2d_19_min] [decimal](12, 3) NULL,
	[x2d_19_max] [decimal](12, 3) NULL,
	[x2d_19_acc] [decimal](12, 3) NULL,
	[x2d_19_avg] [decimal](12, 3) NULL,
	[x2d_19_num] [bigint] NULL,
	[x2d_20_min] [decimal](12, 3) NULL,
	[x2d_20_max] [decimal](12, 3) NULL,
	[x2d_20_acc] [decimal](12, 3) NULL,
	[x2d_20_avg] [decimal](12, 3) NULL,
	[x2d_20_num] [bigint] NULL,
	[x2d_21_min] [decimal](12, 3) NULL,
	[x2d_21_max] [decimal](12, 3) NULL,
	[x2d_21_acc] [decimal](12, 3) NULL,
	[x2d_21_avg] [decimal](12, 3) NULL,
	[x2d_21_num] [bigint] NULL,
	[x2d_22_min] [decimal](12, 3) NULL,
	[x2d_22_max] [decimal](12, 3) NULL,
	[x2d_22_acc] [decimal](12, 3) NULL,
	[x2d_22_avg] [decimal](12, 3) NULL,
	[x2d_22_num] [bigint] NULL,
	[x2d_23_min] [decimal](12, 3) NULL,
	[x2d_23_max] [decimal](12, 3) NULL,
	[x2d_23_acc] [decimal](12, 3) NULL,
	[x2d_23_avg] [decimal](12, 3) NULL,
	[x2d_23_num] [bigint] NULL,
	[x2d_dd_min] [decimal](12, 3) NULL,
	[x2d_dd_max] [decimal](12, 3) NULL,
	[x2d_dd_acc] [decimal](12, 3) NULL,
	[x2d_dd_avg] [decimal](12, 3) NULL,
	[x2d_dd_num] [bigint] NULL,
 CONSTRAINT [PK_M2D_TMH] PRIMARY KEY CLUSTERED 
(
	[x2d_date] ASC,
	[x2d_weekday] ASC,
	[x2d_id] ASC,
	[x2d_meter_id] ASC,
	[x2d_meter_item] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


GO

/****** Object:  Table [dbo].[h_t2d_tmh]    Script Date: 03/18/2016 13:14:08 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[h_t2d_tmh]') AND type in (N'U'))
DROP TABLE [dbo].[h_t2d_tmh]
GO


GO

/****** Object:  Table [dbo].[h_t2d_tmh]    Script Date: 03/18/2016 13:14:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[h_t2d_tmh](
	[x2d_date] [int] NOT NULL,
	[x2d_weekday] [tinyint] NOT NULL,
	[x2d_id] [int] NOT NULL,
	[x2d_meter_id] [int] NOT NULL,
	[x2d_meter_item] [int] NOT NULL,
	[x2d_00_min] [decimal](12, 3) NULL,
	[x2d_00_max] [decimal](12, 3) NULL,
	[x2d_00_acc] [decimal](12, 3) NULL,
	[x2d_00_avg] [decimal](12, 3) NULL,
	[x2d_00_num] [bigint] NULL,
	[x2d_01_min] [decimal](12, 3) NULL,
	[x2d_01_max] [decimal](12, 3) NULL,
	[x2d_01_acc] [decimal](12, 3) NULL,
	[x2d_01_avg] [decimal](12, 3) NULL,
	[x2d_01_num] [bigint] NULL,
	[x2d_02_min] [decimal](12, 3) NULL,
	[x2d_02_max] [decimal](12, 3) NULL,
	[x2d_02_acc] [decimal](12, 3) NULL,
	[x2d_02_avg] [decimal](12, 3) NULL,
	[x2d_02_num] [bigint] NULL,
	[x2d_03_min] [decimal](12, 3) NULL,
	[x2d_03_max] [decimal](12, 3) NULL,
	[x2d_03_acc] [decimal](12, 3) NULL,
	[x2d_03_avg] [decimal](12, 3) NULL,
	[x2d_03_num] [bigint] NULL,
	[x2d_04_min] [decimal](12, 3) NULL,
	[x2d_04_max] [decimal](12, 3) NULL,
	[x2d_04_acc] [decimal](12, 3) NULL,
	[x2d_04_avg] [decimal](12, 3) NULL,
	[x2d_04_num] [bigint] NULL,
	[x2d_05_min] [decimal](12, 3) NULL,
	[x2d_05_max] [decimal](12, 3) NULL,
	[x2d_05_acc] [decimal](12, 3) NULL,
	[x2d_05_avg] [decimal](12, 3) NULL,
	[x2d_05_num] [bigint] NULL,
	[x2d_06_min] [decimal](12, 3) NULL,
	[x2d_06_max] [decimal](12, 3) NULL,
	[x2d_06_acc] [decimal](12, 3) NULL,
	[x2d_06_avg] [decimal](12, 3) NULL,
	[x2d_06_num] [bigint] NULL,
	[x2d_07_min] [decimal](12, 3) NULL,
	[x2d_07_max] [decimal](12, 3) NULL,
	[x2d_07_acc] [decimal](12, 3) NULL,
	[x2d_07_avg] [decimal](12, 3) NULL,
	[x2d_07_num] [bigint] NULL,
	[x2d_08_min] [decimal](12, 3) NULL,
	[x2d_08_max] [decimal](12, 3) NULL,
	[x2d_08_acc] [decimal](12, 3) NULL,
	[x2d_08_avg] [decimal](12, 3) NULL,
	[x2d_08_num] [bigint] NULL,
	[x2d_09_min] [decimal](12, 3) NULL,
	[x2d_09_max] [decimal](12, 3) NULL,
	[x2d_09_acc] [decimal](12, 3) NULL,
	[x2d_09_avg] [decimal](12, 3) NULL,
	[x2d_09_num] [bigint] NULL,
	[x2d_10_min] [decimal](12, 3) NULL,
	[x2d_10_max] [decimal](12, 3) NULL,
	[x2d_10_acc] [decimal](12, 3) NULL,
	[x2d_10_avg] [decimal](12, 3) NULL,
	[x2d_10_num] [bigint] NULL,
	[x2d_11_min] [decimal](12, 3) NULL,
	[x2d_11_max] [decimal](12, 3) NULL,
	[x2d_11_acc] [decimal](12, 3) NULL,
	[x2d_11_avg] [decimal](12, 3) NULL,
	[x2d_11_num] [bigint] NULL,
	[x2d_12_min] [decimal](12, 3) NULL,
	[x2d_12_max] [decimal](12, 3) NULL,
	[x2d_12_acc] [decimal](12, 3) NULL,
	[x2d_12_avg] [decimal](12, 3) NULL,
	[x2d_12_num] [bigint] NULL,
	[x2d_13_min] [decimal](12, 3) NULL,
	[x2d_13_max] [decimal](12, 3) NULL,
	[x2d_13_acc] [decimal](12, 3) NULL,
	[x2d_13_avg] [decimal](12, 3) NULL,
	[x2d_13_num] [bigint] NULL,
	[x2d_14_min] [decimal](12, 3) NULL,
	[x2d_14_max] [decimal](12, 3) NULL,
	[x2d_14_acc] [decimal](12, 3) NULL,
	[x2d_14_avg] [decimal](12, 3) NULL,
	[x2d_14_num] [bigint] NULL,
	[x2d_15_min] [decimal](12, 3) NULL,
	[x2d_15_max] [decimal](12, 3) NULL,
	[x2d_15_acc] [decimal](12, 3) NULL,
	[x2d_15_avg] [decimal](12, 3) NULL,
	[x2d_15_num] [bigint] NULL,
	[x2d_16_min] [decimal](12, 3) NULL,
	[x2d_16_max] [decimal](12, 3) NULL,
	[x2d_16_acc] [decimal](12, 3) NULL,
	[x2d_16_avg] [decimal](12, 3) NULL,
	[x2d_16_num] [bigint] NULL,
	[x2d_17_min] [decimal](12, 3) NULL,
	[x2d_17_max] [decimal](12, 3) NULL,
	[x2d_17_acc] [decimal](12, 3) NULL,
	[x2d_17_avg] [decimal](12, 3) NULL,
	[x2d_17_num] [bigint] NULL,
	[x2d_18_min] [decimal](12, 3) NULL,
	[x2d_18_max] [decimal](12, 3) NULL,
	[x2d_18_acc] [decimal](12, 3) NULL,
	[x2d_18_avg] [decimal](12, 3) NULL,
	[x2d_18_num] [bigint] NULL,
	[x2d_19_min] [decimal](12, 3) NULL,
	[x2d_19_max] [decimal](12, 3) NULL,
	[x2d_19_acc] [decimal](12, 3) NULL,
	[x2d_19_avg] [decimal](12, 3) NULL,
	[x2d_19_num] [bigint] NULL,
	[x2d_20_min] [decimal](12, 3) NULL,
	[x2d_20_max] [decimal](12, 3) NULL,
	[x2d_20_acc] [decimal](12, 3) NULL,
	[x2d_20_avg] [decimal](12, 3) NULL,
	[x2d_20_num] [bigint] NULL,
	[x2d_21_min] [decimal](12, 3) NULL,
	[x2d_21_max] [decimal](12, 3) NULL,
	[x2d_21_acc] [decimal](12, 3) NULL,
	[x2d_21_avg] [decimal](12, 3) NULL,
	[x2d_21_num] [bigint] NULL,
	[x2d_22_min] [decimal](12, 3) NULL,
	[x2d_22_max] [decimal](12, 3) NULL,
	[x2d_22_acc] [decimal](12, 3) NULL,
	[x2d_22_avg] [decimal](12, 3) NULL,
	[x2d_22_num] [bigint] NULL,
	[x2d_23_min] [decimal](12, 3) NULL,
	[x2d_23_max] [decimal](12, 3) NULL,
	[x2d_23_acc] [decimal](12, 3) NULL,
	[x2d_23_avg] [decimal](12, 3) NULL,
	[x2d_23_num] [bigint] NULL,
	[x2d_dd_min] [decimal](12, 3) NULL,
	[x2d_dd_max] [decimal](12, 3) NULL,
	[x2d_dd_acc] [decimal](12, 3) NULL,
	[x2d_dd_avg] [decimal](12, 3) NULL,
	[x2d_dd_num] [bigint] NULL,
 CONSTRAINT [PK_T2D_TMH] PRIMARY KEY CLUSTERED 
(
	[x2d_date] ASC,
	[x2d_weekday] ASC,
	[x2d_id] ASC,
	[x2d_meter_id] ASC,
	[x2d_meter_item] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO



GO

/****** Object:  Table [dbo].[h_t2d_smh]    Script Date: 03/18/2016 13:14:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[h_t2d_smh]') AND type in (N'U'))
DROP TABLE [dbo].[h_t2d_smh]
GO


GO

/****** Object:  Table [dbo].[h_t2d_smh]    Script Date: 03/18/2016 13:14:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[h_t2d_smh](
	[x2d_date] [int] NOT NULL,
	[x2d_weekday] [tinyint] NOT NULL,
	[x2d_id] [int] NOT NULL,
	[x2d_meter_id] [int] NOT NULL,
	[x2d_meter_item] [int] NOT NULL,
	[x2d_00_min] [decimal](12, 3) NULL,
	[x2d_00_max] [decimal](12, 3) NULL,
	[x2d_00_acc] [decimal](12, 3) NULL,
	[x2d_00_avg] [decimal](12, 3) NULL,
	[x2d_00_num] [bigint] NULL,
	[x2d_01_min] [decimal](12, 3) NULL,
	[x2d_01_max] [decimal](12, 3) NULL,
	[x2d_01_acc] [decimal](12, 3) NULL,
	[x2d_01_avg] [decimal](12, 3) NULL,
	[x2d_01_num] [bigint] NULL,
	[x2d_02_min] [decimal](12, 3) NULL,
	[x2d_02_max] [decimal](12, 3) NULL,
	[x2d_02_acc] [decimal](12, 3) NULL,
	[x2d_02_avg] [decimal](12, 3) NULL,
	[x2d_02_num] [bigint] NULL,
	[x2d_03_min] [decimal](12, 3) NULL,
	[x2d_03_max] [decimal](12, 3) NULL,
	[x2d_03_acc] [decimal](12, 3) NULL,
	[x2d_03_avg] [decimal](12, 3) NULL,
	[x2d_03_num] [bigint] NULL,
	[x2d_04_min] [decimal](12, 3) NULL,
	[x2d_04_max] [decimal](12, 3) NULL,
	[x2d_04_acc] [decimal](12, 3) NULL,
	[x2d_04_avg] [decimal](12, 3) NULL,
	[x2d_04_num] [bigint] NULL,
	[x2d_05_min] [decimal](12, 3) NULL,
	[x2d_05_max] [decimal](12, 3) NULL,
	[x2d_05_acc] [decimal](12, 3) NULL,
	[x2d_05_avg] [decimal](12, 3) NULL,
	[x2d_05_num] [bigint] NULL,
	[x2d_06_min] [decimal](12, 3) NULL,
	[x2d_06_max] [decimal](12, 3) NULL,
	[x2d_06_acc] [decimal](12, 3) NULL,
	[x2d_06_avg] [decimal](12, 3) NULL,
	[x2d_06_num] [bigint] NULL,
	[x2d_07_min] [decimal](12, 3) NULL,
	[x2d_07_max] [decimal](12, 3) NULL,
	[x2d_07_acc] [decimal](12, 3) NULL,
	[x2d_07_avg] [decimal](12, 3) NULL,
	[x2d_07_num] [bigint] NULL,
	[x2d_08_min] [decimal](12, 3) NULL,
	[x2d_08_max] [decimal](12, 3) NULL,
	[x2d_08_acc] [decimal](12, 3) NULL,
	[x2d_08_avg] [decimal](12, 3) NULL,
	[x2d_08_num] [bigint] NULL,
	[x2d_09_min] [decimal](12, 3) NULL,
	[x2d_09_max] [decimal](12, 3) NULL,
	[x2d_09_acc] [decimal](12, 3) NULL,
	[x2d_09_avg] [decimal](12, 3) NULL,
	[x2d_09_num] [bigint] NULL,
	[x2d_10_min] [decimal](12, 3) NULL,
	[x2d_10_max] [decimal](12, 3) NULL,
	[x2d_10_acc] [decimal](12, 3) NULL,
	[x2d_10_avg] [decimal](12, 3) NULL,
	[x2d_10_num] [bigint] NULL,
	[x2d_11_min] [decimal](12, 3) NULL,
	[x2d_11_max] [decimal](12, 3) NULL,
	[x2d_11_acc] [decimal](12, 3) NULL,
	[x2d_11_avg] [decimal](12, 3) NULL,
	[x2d_11_num] [bigint] NULL,
	[x2d_12_min] [decimal](12, 3) NULL,
	[x2d_12_max] [decimal](12, 3) NULL,
	[x2d_12_acc] [decimal](12, 3) NULL,
	[x2d_12_avg] [decimal](12, 3) NULL,
	[x2d_12_num] [bigint] NULL,
	[x2d_13_min] [decimal](12, 3) NULL,
	[x2d_13_max] [decimal](12, 3) NULL,
	[x2d_13_acc] [decimal](12, 3) NULL,
	[x2d_13_avg] [decimal](12, 3) NULL,
	[x2d_13_num] [bigint] NULL,
	[x2d_14_min] [decimal](12, 3) NULL,
	[x2d_14_max] [decimal](12, 3) NULL,
	[x2d_14_acc] [decimal](12, 3) NULL,
	[x2d_14_avg] [decimal](12, 3) NULL,
	[x2d_14_num] [bigint] NULL,
	[x2d_15_min] [decimal](12, 3) NULL,
	[x2d_15_max] [decimal](12, 3) NULL,
	[x2d_15_acc] [decimal](12, 3) NULL,
	[x2d_15_avg] [decimal](12, 3) NULL,
	[x2d_15_num] [bigint] NULL,
	[x2d_16_min] [decimal](12, 3) NULL,
	[x2d_16_max] [decimal](12, 3) NULL,
	[x2d_16_acc] [decimal](12, 3) NULL,
	[x2d_16_avg] [decimal](12, 3) NULL,
	[x2d_16_num] [bigint] NULL,
	[x2d_17_min] [decimal](12, 3) NULL,
	[x2d_17_max] [decimal](12, 3) NULL,
	[x2d_17_acc] [decimal](12, 3) NULL,
	[x2d_17_avg] [decimal](12, 3) NULL,
	[x2d_17_num] [bigint] NULL,
	[x2d_18_min] [decimal](12, 3) NULL,
	[x2d_18_max] [decimal](12, 3) NULL,
	[x2d_18_acc] [decimal](12, 3) NULL,
	[x2d_18_avg] [decimal](12, 3) NULL,
	[x2d_18_num] [bigint] NULL,
	[x2d_19_min] [decimal](12, 3) NULL,
	[x2d_19_max] [decimal](12, 3) NULL,
	[x2d_19_acc] [decimal](12, 3) NULL,
	[x2d_19_avg] [decimal](12, 3) NULL,
	[x2d_19_num] [bigint] NULL,
	[x2d_20_min] [decimal](12, 3) NULL,
	[x2d_20_max] [decimal](12, 3) NULL,
	[x2d_20_acc] [decimal](12, 3) NULL,
	[x2d_20_avg] [decimal](12, 3) NULL,
	[x2d_20_num] [bigint] NULL,
	[x2d_21_min] [decimal](12, 3) NULL,
	[x2d_21_max] [decimal](12, 3) NULL,
	[x2d_21_acc] [decimal](12, 3) NULL,
	[x2d_21_avg] [decimal](12, 3) NULL,
	[x2d_21_num] [bigint] NULL,
	[x2d_22_min] [decimal](12, 3) NULL,
	[x2d_22_max] [decimal](12, 3) NULL,
	[x2d_22_acc] [decimal](12, 3) NULL,
	[x2d_22_avg] [decimal](12, 3) NULL,
	[x2d_22_num] [bigint] NULL,
	[x2d_23_min] [decimal](12, 3) NULL,
	[x2d_23_max] [decimal](12, 3) NULL,
	[x2d_23_acc] [decimal](12, 3) NULL,
	[x2d_23_avg] [decimal](12, 3) NULL,
	[x2d_23_num] [bigint] NULL,
	[x2d_dd_min] [decimal](12, 3) NULL,
	[x2d_dd_max] [decimal](12, 3) NULL,
	[x2d_dd_acc] [decimal](12, 3) NULL,
	[x2d_dd_avg] [decimal](12, 3) NULL,
	[x2d_dd_num] [bigint] NULL,
 CONSTRAINT [PK_T2D_SMH] PRIMARY KEY CLUSTERED 
(
	[x2d_date] ASC,
	[x2d_weekday] ASC,
	[x2d_id] ASC,
	[x2d_meter_id] ASC,
	[x2d_meter_item] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


GO

/****** Object:  Table [dbo].[h_w2d_tmh]    Script Date: 03/18/2016 13:14:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[h_w2d_tmh]') AND type in (N'U'))
DROP TABLE [dbo].[h_w2d_tmh]
GO


GO

/****** Object:  Table [dbo].[h_w2d_tmh]    Script Date: 03/18/2016 13:14:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[h_w2d_tmh](
	[x2d_date] [int] NOT NULL,
	[x2d_weekday] [tinyint] NOT NULL,
	[x2d_id] [int] NOT NULL,
	[x2d_meter_id] [int] NOT NULL,
	[x2d_meter_item] [int] NOT NULL,
	[x2d_00_min] [decimal](12, 3) NULL,
	[x2d_00_max] [decimal](12, 3) NULL,
	[x2d_00_acc] [decimal](12, 3) NULL,
	[x2d_00_avg] [decimal](12, 3) NULL,
	[x2d_00_num] [bigint] NULL,
	[x2d_01_min] [decimal](12, 3) NULL,
	[x2d_01_max] [decimal](12, 3) NULL,
	[x2d_01_acc] [decimal](12, 3) NULL,
	[x2d_01_avg] [decimal](12, 3) NULL,
	[x2d_01_num] [bigint] NULL,
	[x2d_02_min] [decimal](12, 3) NULL,
	[x2d_02_max] [decimal](12, 3) NULL,
	[x2d_02_acc] [decimal](12, 3) NULL,
	[x2d_02_avg] [decimal](12, 3) NULL,
	[x2d_02_num] [bigint] NULL,
	[x2d_03_min] [decimal](12, 3) NULL,
	[x2d_03_max] [decimal](12, 3) NULL,
	[x2d_03_acc] [decimal](12, 3) NULL,
	[x2d_03_avg] [decimal](12, 3) NULL,
	[x2d_03_num] [bigint] NULL,
	[x2d_04_min] [decimal](12, 3) NULL,
	[x2d_04_max] [decimal](12, 3) NULL,
	[x2d_04_acc] [decimal](12, 3) NULL,
	[x2d_04_avg] [decimal](12, 3) NULL,
	[x2d_04_num] [bigint] NULL,
	[x2d_05_min] [decimal](12, 3) NULL,
	[x2d_05_max] [decimal](12, 3) NULL,
	[x2d_05_acc] [decimal](12, 3) NULL,
	[x2d_05_avg] [decimal](12, 3) NULL,
	[x2d_05_num] [bigint] NULL,
	[x2d_06_min] [decimal](12, 3) NULL,
	[x2d_06_max] [decimal](12, 3) NULL,
	[x2d_06_acc] [decimal](12, 3) NULL,
	[x2d_06_avg] [decimal](12, 3) NULL,
	[x2d_06_num] [bigint] NULL,
	[x2d_07_min] [decimal](12, 3) NULL,
	[x2d_07_max] [decimal](12, 3) NULL,
	[x2d_07_acc] [decimal](12, 3) NULL,
	[x2d_07_avg] [decimal](12, 3) NULL,
	[x2d_07_num] [bigint] NULL,
	[x2d_08_min] [decimal](12, 3) NULL,
	[x2d_08_max] [decimal](12, 3) NULL,
	[x2d_08_acc] [decimal](12, 3) NULL,
	[x2d_08_avg] [decimal](12, 3) NULL,
	[x2d_08_num] [bigint] NULL,
	[x2d_09_min] [decimal](12, 3) NULL,
	[x2d_09_max] [decimal](12, 3) NULL,
	[x2d_09_acc] [decimal](12, 3) NULL,
	[x2d_09_avg] [decimal](12, 3) NULL,
	[x2d_09_num] [bigint] NULL,
	[x2d_10_min] [decimal](12, 3) NULL,
	[x2d_10_max] [decimal](12, 3) NULL,
	[x2d_10_acc] [decimal](12, 3) NULL,
	[x2d_10_avg] [decimal](12, 3) NULL,
	[x2d_10_num] [bigint] NULL,
	[x2d_11_min] [decimal](12, 3) NULL,
	[x2d_11_max] [decimal](12, 3) NULL,
	[x2d_11_acc] [decimal](12, 3) NULL,
	[x2d_11_avg] [decimal](12, 3) NULL,
	[x2d_11_num] [bigint] NULL,
	[x2d_12_min] [decimal](12, 3) NULL,
	[x2d_12_max] [decimal](12, 3) NULL,
	[x2d_12_acc] [decimal](12, 3) NULL,
	[x2d_12_avg] [decimal](12, 3) NULL,
	[x2d_12_num] [bigint] NULL,
	[x2d_13_min] [decimal](12, 3) NULL,
	[x2d_13_max] [decimal](12, 3) NULL,
	[x2d_13_acc] [decimal](12, 3) NULL,
	[x2d_13_avg] [decimal](12, 3) NULL,
	[x2d_13_num] [bigint] NULL,
	[x2d_14_min] [decimal](12, 3) NULL,
	[x2d_14_max] [decimal](12, 3) NULL,
	[x2d_14_acc] [decimal](12, 3) NULL,
	[x2d_14_avg] [decimal](12, 3) NULL,
	[x2d_14_num] [bigint] NULL,
	[x2d_15_min] [decimal](12, 3) NULL,
	[x2d_15_max] [decimal](12, 3) NULL,
	[x2d_15_acc] [decimal](12, 3) NULL,
	[x2d_15_avg] [decimal](12, 3) NULL,
	[x2d_15_num] [bigint] NULL,
	[x2d_16_min] [decimal](12, 3) NULL,
	[x2d_16_max] [decimal](12, 3) NULL,
	[x2d_16_acc] [decimal](12, 3) NULL,
	[x2d_16_avg] [decimal](12, 3) NULL,
	[x2d_16_num] [bigint] NULL,
	[x2d_17_min] [decimal](12, 3) NULL,
	[x2d_17_max] [decimal](12, 3) NULL,
	[x2d_17_acc] [decimal](12, 3) NULL,
	[x2d_17_avg] [decimal](12, 3) NULL,
	[x2d_17_num] [bigint] NULL,
	[x2d_18_min] [decimal](12, 3) NULL,
	[x2d_18_max] [decimal](12, 3) NULL,
	[x2d_18_acc] [decimal](12, 3) NULL,
	[x2d_18_avg] [decimal](12, 3) NULL,
	[x2d_18_num] [bigint] NULL,
	[x2d_19_min] [decimal](12, 3) NULL,
	[x2d_19_max] [decimal](12, 3) NULL,
	[x2d_19_acc] [decimal](12, 3) NULL,
	[x2d_19_avg] [decimal](12, 3) NULL,
	[x2d_19_num] [bigint] NULL,
	[x2d_20_min] [decimal](12, 3) NULL,
	[x2d_20_max] [decimal](12, 3) NULL,
	[x2d_20_acc] [decimal](12, 3) NULL,
	[x2d_20_avg] [decimal](12, 3) NULL,
	[x2d_20_num] [bigint] NULL,
	[x2d_21_min] [decimal](12, 3) NULL,
	[x2d_21_max] [decimal](12, 3) NULL,
	[x2d_21_acc] [decimal](12, 3) NULL,
	[x2d_21_avg] [decimal](12, 3) NULL,
	[x2d_21_num] [bigint] NULL,
	[x2d_22_min] [decimal](12, 3) NULL,
	[x2d_22_max] [decimal](12, 3) NULL,
	[x2d_22_acc] [decimal](12, 3) NULL,
	[x2d_22_avg] [decimal](12, 3) NULL,
	[x2d_22_num] [bigint] NULL,
	[x2d_23_min] [decimal](12, 3) NULL,
	[x2d_23_max] [decimal](12, 3) NULL,
	[x2d_23_acc] [decimal](12, 3) NULL,
	[x2d_23_avg] [decimal](12, 3) NULL,
	[x2d_23_num] [bigint] NULL,
	[x2d_dd_min] [decimal](12, 3) NULL,
	[x2d_dd_max] [decimal](12, 3) NULL,
	[x2d_dd_acc] [decimal](12, 3) NULL,
	[x2d_dd_avg] [decimal](12, 3) NULL,
	[x2d_dd_num] [bigint] NULL,
 CONSTRAINT [PK_W2D_TMH] PRIMARY KEY CLUSTERED 
(
	[x2d_date] ASC,
	[x2d_weekday] ASC,
	[x2d_id] ASC,
	[x2d_meter_id] ASC,
	[x2d_meter_item] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


GO

/****** Object:  Table [dbo].[h_y2d_tmh]    Script Date: 03/18/2016 13:15:12 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[h_y2d_tmh]') AND type in (N'U'))
DROP TABLE [dbo].[h_y2d_tmh]
GO


GO

/****** Object:  Table [dbo].[h_y2d_tmh]    Script Date: 03/18/2016 13:15:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[h_y2d_tmh](
	[x2d_date] [int] NOT NULL,
	[x2d_weekday] [tinyint] NOT NULL,
	[x2d_id] [int] NOT NULL,
	[x2d_meter_id] [int] NOT NULL,
	[x2d_meter_item] [int] NOT NULL,
	[x2d_00_min] [decimal](12, 3) NULL,
	[x2d_00_max] [decimal](12, 3) NULL,
	[x2d_00_acc] [decimal](12, 3) NULL,
	[x2d_00_avg] [decimal](12, 3) NULL,
	[x2d_00_num] [bigint] NULL,
	[x2d_01_min] [decimal](12, 3) NULL,
	[x2d_01_max] [decimal](12, 3) NULL,
	[x2d_01_acc] [decimal](12, 3) NULL,
	[x2d_01_avg] [decimal](12, 3) NULL,
	[x2d_01_num] [bigint] NULL,
	[x2d_02_min] [decimal](12, 3) NULL,
	[x2d_02_max] [decimal](12, 3) NULL,
	[x2d_02_acc] [decimal](12, 3) NULL,
	[x2d_02_avg] [decimal](12, 3) NULL,
	[x2d_02_num] [bigint] NULL,
	[x2d_03_min] [decimal](12, 3) NULL,
	[x2d_03_max] [decimal](12, 3) NULL,
	[x2d_03_acc] [decimal](12, 3) NULL,
	[x2d_03_avg] [decimal](12, 3) NULL,
	[x2d_03_num] [bigint] NULL,
	[x2d_04_min] [decimal](12, 3) NULL,
	[x2d_04_max] [decimal](12, 3) NULL,
	[x2d_04_acc] [decimal](12, 3) NULL,
	[x2d_04_avg] [decimal](12, 3) NULL,
	[x2d_04_num] [bigint] NULL,
	[x2d_05_min] [decimal](12, 3) NULL,
	[x2d_05_max] [decimal](12, 3) NULL,
	[x2d_05_acc] [decimal](12, 3) NULL,
	[x2d_05_avg] [decimal](12, 3) NULL,
	[x2d_05_num] [bigint] NULL,
	[x2d_06_min] [decimal](12, 3) NULL,
	[x2d_06_max] [decimal](12, 3) NULL,
	[x2d_06_acc] [decimal](12, 3) NULL,
	[x2d_06_avg] [decimal](12, 3) NULL,
	[x2d_06_num] [bigint] NULL,
	[x2d_07_min] [decimal](12, 3) NULL,
	[x2d_07_max] [decimal](12, 3) NULL,
	[x2d_07_acc] [decimal](12, 3) NULL,
	[x2d_07_avg] [decimal](12, 3) NULL,
	[x2d_07_num] [bigint] NULL,
	[x2d_08_min] [decimal](12, 3) NULL,
	[x2d_08_max] [decimal](12, 3) NULL,
	[x2d_08_acc] [decimal](12, 3) NULL,
	[x2d_08_avg] [decimal](12, 3) NULL,
	[x2d_08_num] [bigint] NULL,
	[x2d_09_min] [decimal](12, 3) NULL,
	[x2d_09_max] [decimal](12, 3) NULL,
	[x2d_09_acc] [decimal](12, 3) NULL,
	[x2d_09_avg] [decimal](12, 3) NULL,
	[x2d_09_num] [bigint] NULL,
	[x2d_10_min] [decimal](12, 3) NULL,
	[x2d_10_max] [decimal](12, 3) NULL,
	[x2d_10_acc] [decimal](12, 3) NULL,
	[x2d_10_avg] [decimal](12, 3) NULL,
	[x2d_10_num] [bigint] NULL,
	[x2d_11_min] [decimal](12, 3) NULL,
	[x2d_11_max] [decimal](12, 3) NULL,
	[x2d_11_acc] [decimal](12, 3) NULL,
	[x2d_11_avg] [decimal](12, 3) NULL,
	[x2d_11_num] [bigint] NULL,
	[x2d_12_min] [decimal](12, 3) NULL,
	[x2d_12_max] [decimal](12, 3) NULL,
	[x2d_12_acc] [decimal](12, 3) NULL,
	[x2d_12_avg] [decimal](12, 3) NULL,
	[x2d_12_num] [bigint] NULL,
	[x2d_13_min] [decimal](12, 3) NULL,
	[x2d_13_max] [decimal](12, 3) NULL,
	[x2d_13_acc] [decimal](12, 3) NULL,
	[x2d_13_avg] [decimal](12, 3) NULL,
	[x2d_13_num] [bigint] NULL,
	[x2d_14_min] [decimal](12, 3) NULL,
	[x2d_14_max] [decimal](12, 3) NULL,
	[x2d_14_acc] [decimal](12, 3) NULL,
	[x2d_14_avg] [decimal](12, 3) NULL,
	[x2d_14_num] [bigint] NULL,
	[x2d_15_min] [decimal](12, 3) NULL,
	[x2d_15_max] [decimal](12, 3) NULL,
	[x2d_15_acc] [decimal](12, 3) NULL,
	[x2d_15_avg] [decimal](12, 3) NULL,
	[x2d_15_num] [bigint] NULL,
	[x2d_16_min] [decimal](12, 3) NULL,
	[x2d_16_max] [decimal](12, 3) NULL,
	[x2d_16_acc] [decimal](12, 3) NULL,
	[x2d_16_avg] [decimal](12, 3) NULL,
	[x2d_16_num] [bigint] NULL,
	[x2d_17_min] [decimal](12, 3) NULL,
	[x2d_17_max] [decimal](12, 3) NULL,
	[x2d_17_acc] [decimal](12, 3) NULL,
	[x2d_17_avg] [decimal](12, 3) NULL,
	[x2d_17_num] [bigint] NULL,
	[x2d_18_min] [decimal](12, 3) NULL,
	[x2d_18_max] [decimal](12, 3) NULL,
	[x2d_18_acc] [decimal](12, 3) NULL,
	[x2d_18_avg] [decimal](12, 3) NULL,
	[x2d_18_num] [bigint] NULL,
	[x2d_19_min] [decimal](12, 3) NULL,
	[x2d_19_max] [decimal](12, 3) NULL,
	[x2d_19_acc] [decimal](12, 3) NULL,
	[x2d_19_avg] [decimal](12, 3) NULL,
	[x2d_19_num] [bigint] NULL,
	[x2d_20_min] [decimal](12, 3) NULL,
	[x2d_20_max] [decimal](12, 3) NULL,
	[x2d_20_acc] [decimal](12, 3) NULL,
	[x2d_20_avg] [decimal](12, 3) NULL,
	[x2d_20_num] [bigint] NULL,
	[x2d_21_min] [decimal](12, 3) NULL,
	[x2d_21_max] [decimal](12, 3) NULL,
	[x2d_21_acc] [decimal](12, 3) NULL,
	[x2d_21_avg] [decimal](12, 3) NULL,
	[x2d_21_num] [bigint] NULL,
	[x2d_22_min] [decimal](12, 3) NULL,
	[x2d_22_max] [decimal](12, 3) NULL,
	[x2d_22_acc] [decimal](12, 3) NULL,
	[x2d_22_avg] [decimal](12, 3) NULL,
	[x2d_22_num] [bigint] NULL,
	[x2d_23_min] [decimal](12, 3) NULL,
	[x2d_23_max] [decimal](12, 3) NULL,
	[x2d_23_acc] [decimal](12, 3) NULL,
	[x2d_23_avg] [decimal](12, 3) NULL,
	[x2d_23_num] [bigint] NULL,
	[x2d_dd_min] [decimal](12, 3) NULL,
	[x2d_dd_max] [decimal](12, 3) NULL,
	[x2d_dd_acc] [decimal](12, 3) NULL,
	[x2d_dd_avg] [decimal](12, 3) NULL,
	[x2d_dd_num] [bigint] NULL,
 CONSTRAINT [PK_Y2D_TMH] PRIMARY KEY CLUSTERED 
(
	[x2d_date] ASC,
	[x2d_weekday] ASC,
	[x2d_id] ASC,
	[x2d_meter_id] ASC,
	[x2d_meter_item] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

/****** Object:  UserDefinedFunction [dbo].[GetRangeDate]    Script Date: 04/07/2016 12:47:03 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_get_range_date]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
  DROP FUNCTION [dbo].[sp_get_range_date]
GO

/****** Object:  UserDefinedFunction [dbo].[GetRangeDate]    Script Date: 04/07/2016 12:47:03 ******/
SET ANSI_NULLS ON
GO

/****** Object:  UserDefinedFunction [dbo].[GetRangeDate]    Script Date: 04/07/2016 12:47:03 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetRangeDate]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
  DROP FUNCTION [dbo].[GetRangeDate]
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE function [dbo].[GetRangeDate] 
( @pDate as datetime
, @pRange as varchar(1)
, @pSiteId as bit = 0
)
returns datetime
as
begin

  declare @_result_datetime as datetime
  declare @_result_datepart as datetime
  
  set @_result_datetime = dbo.Opening(@pSiteId, @pdate)

  if (@prange = 't')
    begin
      return @_result_datetime
    end

  if (@prange = 'w') 
  begin
        -- First day of week
            set @_result_datepart = DATEPART(WEEKDAY, @_result_datetime)

            while (@_result_datepart > 1)
            begin
                  set @_result_datetime = DATEADD(DAY, -1, @_result_datetime)
                  set @_result_datepart = DATEPART(WEEKDAY, @_result_datetime)
            end
  end

  if (@prange = 'm')
  begin
            -- First day of month
            set @_result_datetime = dateadd(DAY, -1 * datepart(DAY, @_result_datetime) +1,  @_result_datetime)
  end

  if (@prange = 'y')
  begin
            -- First day of year
      set @_result_datetime = dateadd(DAY, -1 * datepart(DAY, @_result_datetime) +1,  @_result_datetime)
      set @_result_datetime = dateadd(MONTH, -1 * datepart(MONTH, @_result_datetime) +1,  @_result_datetime)
  end

return @_result_datetime

end

GO

/****** Object:  StoredProcedure [dbo].[SmartFloor_TVH]    Script Date: 04/07/2016 12:48:26 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SmartFloor_TVH]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[SmartFloor_TVH]
GO

/****** Object:  StoredProcedure [dbo].[SmartFloor_TVH]    Script Date: 04/07/2016 12:48:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/* */

CREATE PROCEDURE [dbo].[SmartFloor_TVH]
  @DateStart       DATETIME,
  @DateEnd         DATETIME,
  @IsTitoMode      BIT
AS
BEGIN
  DECLARE @DateStartZero AS DATETIME  
  DECLARE @DateEndZero AS DATETIME  
  DECLARE @BetAvgMinHighRoller AS MONEY
  DECLARE @SiteIdentifier AS INT
  DECLARE @DateOpening AS DATETIME
 
  SET @DateStartZero = (SELECT DATEADD(DAY,DATEDIFF(DAY,0,@DateStart),0)) 
  SET @DateEndZero = (SELECT DATEADD(DAY,DATEDIFF(DAY,0,@DateEnd),0)) 
  SET @SiteIdentifier = ISNULL((SELECT GP_KEY_VALUE 
								  FROM GENERAL_PARAMS 
								 WHERE GP_GROUP_KEY = 'Site' 
								   AND GP_SUBJECT_KEY = 'Identifier'),0)
  SET @DateOpening = dbo.Opening(@SiteIdentifier, @DateStart)                     
  
  SELECT    TERMINALS.TE_TERMINAL_ID                                                          AS TERMINAL_ID
          , CAST(CONVERT(NVARCHAR(8),@DateOpening,112) AS INTEGER)                            AS T_DATE
          , SPH.[APUESTA MEDIA]                                                               AS BET_AVG
          , SPH.JUGADO                                                                        AS PLAYED
          , SPH.PREMIOS                                                                       AS PRIZE
          , CASE WHEN (@IsTitoMode = 1) THEN 
              ACCOUNT_MOV.[DEPOSIT A] + ACCOUNT_MOV.[PRIZE COUPON] + PS2.PS_CASH_IN
            ELSE 
              ACCOUNT_MOV.[DEPOSIT A] + ACCOUNT_MOV.[PRIZE COUPON]
           END                                                                               AS COIN_IN          -- CASH IN (Recargas / Bill In)
            
          , SPH.[NET WIN]                                                                     AS NET_WIN
          , SPH.[THEORICAL WON]                                                               AS THEORETICAL_WIN
          , SPH.[NUM JUGADAS]                                                                 AS PLAYED_COUNT
          , TE_DENOMINATION                                                                   AS DENOMINATION     --�TE_MULTI_DENOMINATION?
          , SPH.LOSS                                                                          AS LOSS
          , CAST(CAST(OCUP.OCCUPIED * 100 AS DECIMAL) / 
            (CAST( (OCUP.TERMINAL_COUNT_MASTER * 24 * 60 * 60) AS DECIMAL)) AS DECIMAL(10,3)) AS MACHINE_OCUPATTION
     FROM   TERMINALS

-- SALES PER HOUR
INNER JOIN  
        (
          SELECT  SPH_TERMINAL_ID  
                , SUM(CASE WHEN SALES_PER_HOUR_V2.SPH_PLAYED_COUNT > 0 THEN 
                    ROUND( SALES_PER_HOUR_V2.SPH_PLAYED_AMOUNT / 
                           SALES_PER_HOUR_V2.SPH_PLAYED_COUNT, 2)
                  ELSE
                    0                                                               
                  END)                                                              AS [APUESTA MEDIA]
                , SUM(SALES_PER_HOUR_V2.SPH_PLAYED_AMOUNT)                          AS [JUGADO]
                , SUM(SALES_PER_HOUR_V2.SPH_WON_AMOUNT)                             AS [PREMIOS]
                , SUM(ISNULL(SALES_PER_HOUR_V2.SPH_PLAYED_AMOUNT,0) - 
                  (ISNULL(SALES_PER_HOUR_V2.SPH_WON_AMOUNT,0) + 
                  ISNULL(SALES_PER_HOUR_V2.SPH_PROGRESSIVE_PROVISION_AMOUNT,0) - 
                  ISNULL(SALES_PER_HOUR_V2.SPH_PROGRESSIVE_JACKPOT_AMOUNT_0,0)))    AS [NET WIN]
                , SUM(SALES_PER_HOUR_V2.SPH_THEORETICAL_WON_AMOUNT)                 AS [THEORICAL WON]
                , SUM(SALES_PER_HOUR_V2.SPH_PLAYED_COUNT)                           AS [NUM JUGADAS]
                , SUM(ISNULL(SALES_PER_HOUR_V2.SPH_PLAYED_AMOUNT,0) - 
                  (ISNULL(SALES_PER_HOUR_V2.SPH_WON_AMOUNT,0) + 
                  ISNULL(SALES_PER_HOUR_V2.SPH_PROGRESSIVE_PROVISION_AMOUNT,0) - 
                  ISNULL(SALES_PER_HOUR_V2.SPH_PROGRESSIVE_JACKPOT_AMOUNT_0,0)))    AS [LOSS]
            FROM  SALES_PER_HOUR_V2
           WHERE  SPH_BASE_HOUR >= @DateStart 
             AND  SPH_BASE_HOUR < @DateEnd
        GROUP BY  SPH_TERMINAL_ID) SPH
              ON  TERMINALS.TE_TERMINAL_ID = SPH.SPH_TERMINAL_ID

-- ACCOUNT_MOVEMENTS (PDTE REVISAR)
  LEFT JOIN
         ( 
        SELECT  AM_TERMINAL_ID 
              , SUM(CASE AM_TYPE WHEN  1  THEN AM_ADD_AMOUNT ELSE 0 END)  AS  [DEPOSIT A]         -- Recarga / Cash In
              , SUM(CASE AM_TYPE WHEN 44  THEN AM_ADD_AMOUNT ELSE 0 END)  AS  [PRIZE COUPON]      -- Cup�n Premio
          FROM  ACCOUNT_MOVEMENTS WITH (INDEX (IX_AM_DATETIME) )                       
         WHERE  AM_DATETIME >= @DateStart                                                   
            AND  AM_DATETIME <  @DateEnd 
      GROUP BY  AM_TERMINAL_ID) 
ACCOUNT_MOV ON  ACCOUNT_MOV.AM_TERMINAL_ID = TERMINALS.TE_TERMINAL_ID

-- MACHINE OCUPATTION % (PDTE REVISAR)
LEFT JOIN 
        (
        SELECT  TE_TERMINAL_ID
              , TE_MASTER_ID
              , SUM(DATEDIFF (SECOND, PS_STARTED, PS_FINISHED))     AS OCCUPIED 
              , COUNT(DISTINCT TC_DATE)                             AS CONNECTED
              , CASE WHEN COUNT(DISTINCT TC_DATE) = 0 THEN 
                  CASE WHEN TERMINALS.TE_MASTER_ID IS NULL THEN 0
                  ELSE 1
                  END 
                  ELSE
                    COUNT(DISTINCT TC_DATE)
                  END                                               AS TERMINAL_COUNT_MASTER
          FROM  TERMINALS
     LEFT JOIN  PLAY_SESSIONS 
            ON  TE_TERMINAL_ID = PS_TERMINAL_ID 
           AND  PS_FINISHED >= @DateStart 
           AND  PS_FINISHED <  @DateEnd 
     LEFT JOIN  TERMINALS_CONNECTED  
            ON  TC_TERMINAL_ID = TE_TERMINAL_ID 
           AND  TC_DATE >= @DateStartZero
           AND  TC_DATE <  @DateEndZero
      GROUP BY  TE_TERMINAL_ID, TE_MASTER_ID
      ) OCUP
    ON OCUP.TE_TERMINAL_ID = TERMINALS.TE_TERMINAL_ID
    
-- ACCOUNTS (GENDER: MALE, FEMALE; USER TYPE: REGISTERED(level), ANONYMOUS, UNKNOWN)
LEFT JOIN 
  (
  SELECT 
        PS.PS_TERMINAL_ID AS PSI
      , SUM(PS.PS_CASH_IN)                  AS PS_CASH_IN
  FROM 
    (
      SELECT  PS_TERMINAL_ID
            , AC_ACCOUNT_ID
            , SUM(PS_CASH_IN)               AS PS_CASH_IN
        FROM  PLAY_SESSIONS
  INNER JOIN  ACCOUNTS
          ON  PS_ACCOUNT_ID = ACCOUNTS.AC_ACCOUNT_ID
       WHERE  PS_FINISHED >= @DATESTART                       
         AND  PS_FINISHED <  @DATEEND                       
         AND  PS_STATUS   <> 0
    GROUP BY  PS_TERMINAL_ID
            , AC_ACCOUNT_ID) PS
    GROUP BY PS_TERMINAL_ID
      ) PS2
   ON PS2.PSI = TERMINALS.TE_TERMINAL_ID


-- PROMOTIONS (ASSIGNED & GRANTED)
   SELECT   PM.PROMOTIONS_TOTAL
          , PMG.PROMOTIONS_GRANTED
          , PLAYERS2.GENDER_MALE
          , PLAYERS2.GENDER_FEMALE
          , PLAYERS2.GENDER_UNKNOWN
          , PLAYERS2.ANONYMOUSS
          , PLAYERS2.REGISTERED_LEVEL_SILVER
          , PLAYERS2.REGISTERED_LEVEL_GOLD
          , PLAYERS2.REGISTERED_LEVEL_PLATINUM
          , PLAYERS2.REGISTERED_LEVEL_WIN
          , PLAYERS2.NEW_CLIENTS
     FROM   
            (SELECT COUNT(*) AS PROMOTIONS_TOTAL
               FROM PROMOTIONS
              WHERE  @DATESTART <= promotions.pm_date_finish AND @DATEEND >= promotions.pm_date_start) PM
          , (SELECT COUNT(*) AS PROMOTIONS_GRANTED
               FROM account_promotions
              WHERE account_promotions.acp_created BETWEEN @DATESTART AND @DATEEND) PMG
          
          , (SELECT   COUNT(PLAYERS.GENDER_MALE)               AS GENDER_MALE 
                    , COUNT(PLAYERS.GENDER_FEMALE)             AS GENDER_FEMALE
                    , COUNT(PLAYERS.GENDER_UNKNOWN)            AS GENDER_UNKNOWN
                    , COUNT(PLAYERS.ANONYMOUSS)                AS ANONYMOUSS
                    , COUNT(PLAYERS.REGISTERED_LEVEL_SILVER)   AS REGISTERED_LEVEL_SILVER
                    , COUNT(PLAYERS.REGISTERED_LEVEL_GOLD)     AS REGISTERED_LEVEL_GOLD
                    , COUNT(PLAYERS.REGISTERED_LEVEL_PLATINUM) AS REGISTERED_LEVEL_PLATINUM
                    , COUNT(PLAYERS.REGISTERED_LEVEL_WIN)      AS REGISTERED_LEVEL_WIN
                    , COUNT(PLAYERS.NEW_CLIENTS)               AS NEW_CLIENTS 
                FROM 
                  (
                    SELECT  AC_ACCOUNT_ID
                          , SUM(CASE WHEN AC_HOLDER_GENDER = 1 THEN 1 END)                    AS GENDER_MALE
                          , SUM(CASE WHEN AC_HOLDER_GENDER = 2 THEN 1 END)                    AS GENDER_FEMALE
                          , SUM(CASE WHEN AC_HOLDER_GENDER NOT IN (1, 2) THEN 1 END)          AS GENDER_UNKNOWN
                          , SUM(CASE WHEN AC_TYPE = 2 
                                      AND AC_HOLDER_LEVEL = 0 THEN 1 
                                END)                                                          AS ANONYMOUSS
                          , SUM(CASE WHEN AC_TYPE = 2 
                                      AND AC_HOLDER_LEVEL = 1 THEN 1 
                                END)                                                          AS REGISTERED_LEVEL_SILVER
                          , SUM(CASE WHEN AC_TYPE = 2 
                                      AND AC_HOLDER_LEVEL = 2 THEN 1 
                                END)                                                          AS REGISTERED_LEVEL_GOLD
                          , SUM(CASE WHEN AC_TYPE = 2 
                                      AND AC_HOLDER_LEVEL = 3 THEN 1 
                                END)                                                          AS REGISTERED_LEVEL_PLATINUM
                          , SUM(CASE WHEN AC_TYPE = 2 
                                      AND AC_HOLDER_LEVEL = 4 THEN 1 
                                END)                                                          AS REGISTERED_LEVEL_WIN
                          , SUM(CASE WHEN DATEDIFF(DAY,AC_CREATED, GETDATE())<=1 THEN 1 END)  AS NEW_CLIENTS
                          , SUM(PS_CASH_IN)                                                   AS PS_CASH_IN
                      FROM  PLAY_SESSIONS
                INNER JOIN  ACCOUNTS
                        ON  PS_ACCOUNT_ID = ACCOUNTS.AC_ACCOUNT_ID
                     WHERE  PS_FINISHED >= @DATESTART                       
                       AND  PS_FINISHED <  @DATEEND                       
                       AND  PS_STATUS   <> 0
                  GROUP BY  AC_ACCOUNT_ID) PLAYERS
                 ) PLAYERS2
               



END
GO

/****** Object:  StoredProcedure [dbo].[SP_Update_Meter_Data_Site]    Script Date: 04/07/2016 13:13:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


/****** Object:  StoredProcedure [dbo].[SP_Update_Meter_Data_Site]    Script Date: 04/07/2016 13:13:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_Update_Meter_Data_Site]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_Update_Meter_Data_Site]
GO


CREATE PROCEDURE [dbo].[SP_Update_Meter_Data_Site] (
  @pTableId CHAR(1) -- L (Life), Y (Year), M (Month), W (Week), T (Today)
  ,@pEntityId CHAR(1) -- T (Terminales), S (Site)
  ,@pDate DATETIME
  ,@pId INT -- Terminal ID or Site Id
  ,@pMeterId INT -- 1=CoinIn, 2= NetWin, 3=BetAvg, 4=Gender...
  ,@pMeterItem INT -- 0=Simple meter data, 1=Gender-Male, 2=Gender-female...
  ,@pValue DECIMAL(12,3) -- Value to aggregate
  )
AS
BEGIN
/*
  JRC - 20150925
  This SP receives the information from the thread and the calls the _CAL sp maybe many times based on:
  the value in the @pTableId parameter, creating a waterfall efect. If it is T(oday) it will also be called for 
  W(eek), M(onth) and Y. Each of these accumulate the information on several tables (by Day, by Week, by Month and by Year).
  But if the parameter is for example M(onth), then it should be accumulated only for M(onth) and Y(year).
  If the @pEntityId is X, the information goes for T(terminal) and also for S(ite), so it should be processed twice by the _CALC SP

*/

  DECLARE @_EntityId CHAR(1) 
  SET @_EntityId = @pEntityId
  DECLARE @_accion VARCHAR(10)
  SET @_accion = CASE 
            WHEN @pTableId = 'L' THEN 'L'
            WHEN @pTableId = 'Y' THEN 'LY'
            WHEN @pTableId = 'M' THEN 'LYM'
            WHEN @pTableId = 'W' THEN 'LYMW'
            ELSE 'LYMWT' END


  IF CHARINDEX('Y', @_accion) > 0 
    EXECUTE SP_Update_Meter_Data_Site_Calc 'Y',@_EntityId, @pDate, @pId ,@pMeterId, @pMeterItem ,@pValue
  IF CHARINDEX('M', @_accion) > 0 
    EXECUTE SP_Update_Meter_Data_Site_Calc 'M',@_EntityId, @pDate, @pId ,@pMeterId, @pMeterItem ,@pValue
  IF CHARINDEX('W', @_accion) > 0 
    EXECUTE SP_Update_Meter_Data_Site_Calc 'W',@_EntityId, @pDate, @pId ,@pMeterId, @pMeterItem ,@pValue
  IF CHARINDEX('T', @_accion) > 0 
    EXECUTE SP_Update_Meter_Data_Site_Calc 'T',@_EntityId, @pDate, @pId ,@pMeterId, @pMeterItem ,@pValue
      
END

GO

/****** Object:  StoredProcedure [dbo].[SP_Update_Meter_Data_Site_Calc]    Script Date: 04/07/2016 12:49:25 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_Update_Meter_Data_Site_Calc]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_Update_Meter_Data_Site_Calc]
GO

/****** Object:  StoredProcedure [dbo].[SP_Update_Meter_Data_Site_Calc]    Script Date: 04/07/2016 12:49:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SP_Update_Meter_Data_Site_Calc] (
  @pTableId CHAR(1) -- L (Life), Y (Year), M (Month), W (Week), T (Today)
  ,@pEntityId CHAR(1) -- T (Terminales), S (Site)
  ,@pDate DATETIME
  ,@pId INT -- Terminal ID or Site Id
  ,@pMeterId INT -- 1=CoinIn, 2= NetWin, 3=BetAvg, 4=Gender...
  ,@pMeterItem INT -- 0=Simple meter data, 1=Gender-Male, 2=Gender-female...
  ,@pValue DECIMAL(12, 3) -- Value to aggregate
  /*   for debug
DECLARE 
  @pTableId CHAR(1) = 'T'
  ,@pEntityId CHAR(1) = 'T'
  ,@pDate DATETIME = '2015-10-07 22:01:00.000'
  ,@pValue DECIMAL(12,3)     = 12
  ,@pId INT = 10006
  ,@pMeterId INT = 1
  ,@pMeterItem INT = 0
*/
  )
AS
BEGIN

/*
  JRC 20150928

  Version 1: sql server pivot
  Version 2: manual pivot
  Version 3: direct update
  Version 4: CAST & Insert Else update
  This sp stores the information in the corresponding table based on the @pTableID and @pEntityId parameters
  ie HT_2D_SMH for T(oday) and S(ite).
  
  on which column is based on the time of the item @pDate (one set of column for each 24 hours)
  the stored info on each set of columns is
  Max value
  Min Value
  Measurements Count
  Average Value
  Total Value
  
  and one special set of columns sumarizinf the information on the row
  Max value from the max fields of the row
  Min value from the min fields of the row
  Measurements Count from the Count fields of the row
  Total value from the total fields of the row.
  Average value from this group of fields.
  
  Average recalc formula explained:
  In order to calculate an average value we need : total and count.
  total can be replace by average * count
  so the new average can be expressed as: ((average * count) + new value) / (count + 1)
  and this is how it is done in this SP.
  
*/

  DECLARE @_date AS DATETIME
  DECLARE @_date_val AS INT
  DECLARE @_wk AS INT
  DECLARE @_h AS VARCHAR(2)
  DECLARE @_get_sql NVARCHAR(MAX)
  DECLARE @_get_sql_terminals NVARCHAR(MAX)
  DECLARE @_insert_sql NVARCHAR(MAX)
  DECLARE @_update_sql NVARCHAR(MAX)
  DECLARE @_row_count_terminals NVARCHAR(MAX)
  DECLARE @_row_count NVARCHAR(MAX)
  DECLARE @pCurrentMin DECIMAL(12, 3)
  DECLARE @pCurrentMax DECIMAL(12, 3)
  DECLARE @pCurrentAcc DECIMAL(12, 3)
  DECLARE @pCurrentAvg DECIMAL(12, 3)
  DECLARE @pCurrentNum BIGINT
  DECLARE @SiteIdentifier AS INT

  SET @SiteIdentifier = ISNULL((SELECT GP_KEY_VALUE 
								  FROM GENERAL_PARAMS 
								 WHERE GP_GROUP_KEY = 'Site' 
								   AND GP_SUBJECT_KEY = 'Identifier'),0)

  SET @_date = dbo.GetRangeDate(@pDate, @pTableId, @SiteIdentifier)
  SET @_date_val = CAST(CONVERT(VARCHAR(8), @_date, 112) AS INT)
  
  SET @_wk = DATEPART(WEEKDAY, dbo.Opening(@SiteIdentifier, @pDate))
  
  SET @_h = DATEPART(HOUR, @pDate)
  SET @_h = REPLICATE('0', 2 - LEN(RTRIM(@_h))) + RTRIM(@_h)

  DECLARE @_date_val_V VARCHAR(20) 
  SET @_date_val_V = CAST(@_date_val AS VARCHAR)
  
  DECLARE @pId_V VARCHAR(20) 
  SET @pId_V = CAST(@pId AS VARCHAR)
  
  DECLARE @pMeterId_V VARCHAR(20) 
  SET @pMeterId_V = CAST(@pMeterId AS VARCHAR)
  DECLARE @pMeterItem_V VARCHAR(20) 
  SET @pMeterItem_V = CAST(@pMeterItem AS VARCHAR)
  DECLARE @_wk_V VARCHAR(20) 
  SET @_wk_V = CAST(@_wk AS VARCHAR)
  DECLARE @pValue_V VARCHAR(20) 
  SET @pValue_V = CAST(@pValue AS VARCHAR)



  SET @pCurrentMin = NULL
  SET @pCurrentMax = NULL
  SET @pCurrentAcc = 0
  SET @pCurrentAvg = 0
  SET @pCurrentNum = 0

  DECLARE @DailyMin Decimal(12,3) 
  SET @DailyMin = NULL
  DECLARE @DailyMax Decimal(12,3) 
  SET @DailyMax = NULL
  DECLARE @DailyAcc Decimal(12,3) 
  SET @DailyAcc = 0
  DECLARE @DailyNum BIGINT 
  SET @DailyNum = 0
  DECLARE @DailyAvg Decimal(12,3) 
  SET @DailyAvg = 0

  ------------------------------------------------------------------------------------------------
  -- Obtain current meter data
  -- Use the sentence as EntityId
  SET @_get_sql = ' SELECT @pCurrentMin = (x2d_' + @_h + '_min)
               , @pCurrentMax = (x2d_' + @_h + '_max)
               , @pCurrentAcc = ISNULL(x2d_' + @_h + '_acc, 0)
               , @pCurrentNum = ISNULL(x2d_' + @_h + '_num, 0)
               , @pCurrentAvg = ISNULL(x2d_' + @_h + '_avg, 0)
               , @DailyMin    = x2d_dd_min 
               , @DailyMax    = x2d_dd_max
               , @DailyAcc    =  x2d_dd_acc
               , @DailyNum    =  x2d_dd_num
               , @DailyAvg    =  x2d_dd_avg
            FROM   H_' + @pTableId + '2D_' + @pEntityId + 'MH
            WHERE     x2d_date       = ' + @_date_val_V  + '
              AND   x2d_id         = ' + @pId_V        + '
              AND   x2d_meter_id   = ' + @pMeterId_V   + '
              AND   x2d_meter_item = ' + @pMeterItem_V + '
              AND   x2d_weekday    = ' + @_wk_V
  EXEC sp_executesql @_get_sql
              ,N'@pCurrentMin DECIMAL(12,3) OUTPUT, 
                 @pCurrentMax DECIMAL(12,3) OUTPUT, 
                 @pCurrentAcc DECIMAL(12,3) OUTPUT, 
                 @pCurrentNum BIGINT OUTPUT,
                 @pCurrentAvg DECIMAL(12,3) OUTPUT,
                 @DailyMin    DECIMAL(12,3) OUTPUT,
                 @DailyMax  DECIMAL(12,3) OUTPUT,
                 @DailyAcc  DECIMAL(12,3) OUTPUT,
                 @DailyNum  BIGINT OUTPUT,
                 @DailyAvg  DECIMAL(12,3) OUTPUT
                 '
    ,@pCurrentMin OUTPUT
    ,@pCurrentMax OUTPUT
    ,@pCurrentAcc OUTPUT
    ,@pCurrentNum OUTPUT
    ,@pCurrentAvg OUTPUT
    ,@DailyMin  OUTPUT
    ,@DailyMax  OUTPUT
    ,@DailyAcc  OUTPUT
    ,@DailyNum  OUTPUT
    ,@DailyAvg  OUTPUT

  SET @_row_count = @@ROWCOUNT

  ------------------------------------------------------------------------------------------------
  -- Prepare data for update
  -- Increment number
  SET @pCurrentNum = @pCurrentNum + 1
  -- Average calculation. Must be calculated first JRC
  SET @pCurrentAvg = (@pCurrentAvg * (@pCurrentNum - 1) + @pValue) / CONVERT(DECIMAL(38, 4), @pCurrentNum)

  -- Detect Min value
  IF (@pCurrentMin IS NULL)
    OR (@pCurrentMin > @pValue)
  BEGIN
    SET @pCurrentMin = @pValue
  END

  -- Detect Max value
  IF (@pCurrentMax IS NULL)
    OR (@pCurrentMax < @pValue)
  BEGIN
    SET @pCurrentMax = @pValue
  END

  -- Accumulated calculation
  SET @pCurrentAcc = @pCurrentAcc + @pValue
  ------------------------------------------------------------------------------------------------
  
  IF (@DailyMin IS NULL) OR (@DailyMin > @pValue)
    SET  @DailyMin = @pValue

  IF (@DailyMax IS NULL) OR (@DailyMax < @pValue) 
    SET @DailyMax = @pValue
  


  IF (@_row_count = 0  )
  BEGIN
    -- Create Meter Data
    SET @_insert_sql = 'INSERT INTO H_' + @pTableId + '2D_' + @pEntityId + 'MH
                                     ( x2d_date
                                     , x2d_weekday
                                     , x2d_id
                                     , x2d_meter_id
                                     , x2d_meter_item 
                   , x2d_' + @_h + '_min  
                   , x2d_' + @_h + '_max  
                   , x2d_' + @_h + '_acc  
                   , x2d_' + @_h + '_avg  
                   , x2d_' + @_h + '_num  
                   , x2d_dd_min    
                   , x2d_dd_max    
                   , x2d_dd_acc 
                   , x2d_dd_avg 
                   , x2d_dd_num 
                                   )
                             VALUES
                                     (' + @_date_val_V   + '
                                     ,' + @_wk_V         + '
                                     ,' + @pId_V         + '
                                     ,' + @pMeterId_V    + '                                          
                                     ,' + @pMeterItem_V  + '
                   ,' + @pValue_V      + '
                   ,' + @pValue_V      + '
                   ,' + @pValue_V      + ' 
                   ,' + @pValue_V      + ' 
                   , 1
                   ,' + @pValue_V      + ' 
                   ,' + @pValue_V      + ' 
                   ,' + @pValue_V      + ' 
                   ,' + @pValue_V      + ' 
                   , 1
                                    )'

    EXEC sys.sp_executesql @_insert_sql
  END ELSE BEGIN
    -- Update Meter data
    SET @_update_sql = 'UPDATE   H_' + @pTableId + '2D_' + @pEntityId + 'MH
               SET   x2d_' + @_h + '_min = ' + CAST(@pCurrentMin AS VARCHAR(20)) + '
                 , x2d_' + @_h + '_max = ' + CAST(@pCurrentMax AS VARCHAR(20)) + '
                 , x2d_' + @_h + '_acc = ' + CAST(@pCurrentAcc AS VARCHAR(20)) + '
                 , x2d_' + @_h + '_avg = ' + CAST(@pCurrentAvg AS VARCHAR(20)) + '
                 , x2d_' + @_h + '_num = ' + CAST(@pCurrentNum AS VARCHAR(20)) + '
                 , x2d_dd_min =   ' + CAST(@DailyMin AS VARCHAR(20)) + '
                 , x2d_dd_max =   ' + CAST(@DailyMax AS VARCHAR(20)) + '
                 , x2d_dd_acc = ISNULL([x2d_dd_acc],0) +  ' + @pValue_V + '
                 , x2d_dd_num = ISNULL([x2d_dd_num],0) +  1
                 , x2d_dd_avg = (ISNULL(x2d_dd_acc,0) + ' + @pValue_V +  ')/ (ISNULL(NULLIF(x2d_dd_num,0),1)+1) 
                 WHERE   x2d_date       = ' + @_date_val_V + '
                 AND   x2d_id         = ' + @pId_V + '
                 AND   x2d_meter_id   = ' + @pMeterId_V + '
                 AND   x2d_meter_item = ' + @pMeteritem_V + '
                 AND   x2d_weekday    = ' + @_wk_V

    EXEC sys.sp_executesql @_update_sql
  END

END
GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[h_meters_definition]') AND type in (N'U'))
  TRUNCATE TABLE [dbo].[h_meters_definition]
GO

INSERT [dbo].[h_meters_definition] ([hmd_meter_id], [hmd_meter_item], [hmd_meter_type], [hmd_description], [hmd_meter_mask], [hmd_meter_enabled], [hmd_meter_last_update], [hmd_meter_device], [hmd_visible], [hmd_stored_name], [hmd_meter_name]) VALUES (12, 0, 3, N'PLAYED', 0, 1, 20150622, 0, 1, N'SmartFloor_TVH', N'PLAYED')
INSERT [dbo].[h_meters_definition] ([hmd_meter_id], [hmd_meter_item], [hmd_meter_type], [hmd_description], [hmd_meter_mask], [hmd_meter_enabled], [hmd_meter_last_update], [hmd_meter_device], [hmd_visible], [hmd_stored_name], [hmd_meter_name]) VALUES (1, 0, 3, N'BET AVG', 0, 1, 20150622, 0, 1, N'SmartFloor_TVH', N'BET_AVG')
INSERT [dbo].[h_meters_definition] ([hmd_meter_id], [hmd_meter_item], [hmd_meter_type], [hmd_description], [hmd_meter_mask], [hmd_meter_enabled], [hmd_meter_last_update], [hmd_meter_device], [hmd_visible], [hmd_stored_name], [hmd_meter_name]) VALUES (2, 0, 2, N'OCC. GENDER', 0, 1, 20150622, 0, 1, N'SmartFloor_TVH', NULL)
INSERT [dbo].[h_meters_definition] ([hmd_meter_id], [hmd_meter_item], [hmd_meter_type], [hmd_description], [hmd_meter_mask], [hmd_meter_enabled], [hmd_meter_last_update], [hmd_meter_device], [hmd_visible], [hmd_stored_name], [hmd_meter_name]) VALUES (2, 1, 2, N'MALE', 0, 1, 20150622, 0, 1, N'SmartFloor_TVH', N'GENDER_MALE')
INSERT [dbo].[h_meters_definition] ([hmd_meter_id], [hmd_meter_item], [hmd_meter_type], [hmd_description], [hmd_meter_mask], [hmd_meter_enabled], [hmd_meter_last_update], [hmd_meter_device], [hmd_visible], [hmd_stored_name], [hmd_meter_name]) VALUES (2, 2, 2, N'FEMALE', 0, 1, 20150622, 0, 1, N'SmartFloor_TVH', N'GENDER_FEMALE')
INSERT [dbo].[h_meters_definition] ([hmd_meter_id], [hmd_meter_item], [hmd_meter_type], [hmd_description], [hmd_meter_mask], [hmd_meter_enabled], [hmd_meter_last_update], [hmd_meter_device], [hmd_visible], [hmd_stored_name], [hmd_meter_name]) VALUES (2, 3, 2, N'UNKNOWN', 0, 1, 20150622, 0, 1, N'SmartFloor_TVH', N'GENDER_UNKNOW')
INSERT [dbo].[h_meters_definition] ([hmd_meter_id], [hmd_meter_item], [hmd_meter_type], [hmd_description], [hmd_meter_mask], [hmd_meter_enabled], [hmd_meter_last_update], [hmd_meter_device], [hmd_visible], [hmd_stored_name], [hmd_meter_name]) VALUES (3, 0, 2, N'OCC. PLAYER TYPE', 0, 1, 20150622, 0, 1, N'SmartFloor_TVH', NULL)
INSERT [dbo].[h_meters_definition] ([hmd_meter_id], [hmd_meter_item], [hmd_meter_type], [hmd_description], [hmd_meter_mask], [hmd_meter_enabled], [hmd_meter_last_update], [hmd_meter_device], [hmd_visible], [hmd_stored_name], [hmd_meter_name]) VALUES (3, 1, 2, N'ANONYMOUSS', 0, 1, 20150622, 0, 1, N'SmartFloor_TVH', N'ANONYMOUSS')
INSERT [dbo].[h_meters_definition] ([hmd_meter_id], [hmd_meter_item], [hmd_meter_type], [hmd_description], [hmd_meter_mask], [hmd_meter_enabled], [hmd_meter_last_update], [hmd_meter_device], [hmd_visible], [hmd_stored_name], [hmd_meter_name]) VALUES (3, 2, 2, N'REGISTERED_LEVEL_SILVER', 0, 1, 20150622, 0, 1, N'SmartFloor_TVH', N'REGISTERED_LEVEL_SILVER')
INSERT [dbo].[h_meters_definition] ([hmd_meter_id], [hmd_meter_item], [hmd_meter_type], [hmd_description], [hmd_meter_mask], [hmd_meter_enabled], [hmd_meter_last_update], [hmd_meter_device], [hmd_visible], [hmd_stored_name], [hmd_meter_name]) VALUES (3, 3, 2, N'REGISTERED_LEVEL_GOLD', 0, 1, 20150622, 0, 1, N'SmartFloor_TVH', N'REGISTERED_LEVEL_GOLD')
INSERT [dbo].[h_meters_definition] ([hmd_meter_id], [hmd_meter_item], [hmd_meter_type], [hmd_description], [hmd_meter_mask], [hmd_meter_enabled], [hmd_meter_last_update], [hmd_meter_device], [hmd_visible], [hmd_stored_name], [hmd_meter_name]) VALUES (4, 0, 2, N'OCCUPANCY', 0, 1, 20150622, 0, 1, N'SmartFloor_TVH', N'OCCUPANCY')
INSERT [dbo].[h_meters_definition] ([hmd_meter_id], [hmd_meter_item], [hmd_meter_type], [hmd_description], [hmd_meter_mask], [hmd_meter_enabled], [hmd_meter_last_update], [hmd_meter_device], [hmd_visible], [hmd_stored_name], [hmd_meter_name]) VALUES (5, 0, 3, N'COIN IN', 0, 1, 20150622, 0, 1, N'SmartFloor_TVH', N'COIN_IN')
INSERT [dbo].[h_meters_definition] ([hmd_meter_id], [hmd_meter_item], [hmd_meter_type], [hmd_description], [hmd_meter_mask], [hmd_meter_enabled], [hmd_meter_last_update], [hmd_meter_device], [hmd_visible], [hmd_stored_name], [hmd_meter_name]) VALUES (6, 0, 2, N'NEW CLIENTS', 0, 1, 20150622, 0, 1, N'SmartFloor_TVH', N'NEW_CLIENTS')
INSERT [dbo].[h_meters_definition] ([hmd_meter_id], [hmd_meter_item], [hmd_meter_type], [hmd_description], [hmd_meter_mask], [hmd_meter_enabled], [hmd_meter_last_update], [hmd_meter_device], [hmd_visible], [hmd_stored_name], [hmd_meter_name]) VALUES (7, 0, 2, N'HIGH ROLLERS', 0, 1, 20150622, 0, 1, N'SmartFloor_TVH', N'HIGH_ROLLERS')
INSERT [dbo].[h_meters_definition] ([hmd_meter_id], [hmd_meter_item], [hmd_meter_type], [hmd_description], [hmd_meter_mask], [hmd_meter_enabled], [hmd_meter_last_update], [hmd_meter_device], [hmd_visible], [hmd_stored_name], [hmd_meter_name]) VALUES (8, 0, 2, N'PROMOTIONS', 0, 1, 20150622, 0, 1, N'SmartFloor_TVH', N'PROMOTIONS')
INSERT [dbo].[h_meters_definition] ([hmd_meter_id], [hmd_meter_item], [hmd_meter_type], [hmd_description], [hmd_meter_mask], [hmd_meter_enabled], [hmd_meter_last_update], [hmd_meter_device], [hmd_visible], [hmd_stored_name], [hmd_meter_name]) VALUES (9, 0, 3, N'NET WIN', 0, 1, 20150622, 0, 1, N'SmartFloor_TVH', N'NET_WIN')
INSERT [dbo].[h_meters_definition] ([hmd_meter_id], [hmd_meter_item], [hmd_meter_type], [hmd_description], [hmd_meter_mask], [hmd_meter_enabled], [hmd_meter_last_update], [hmd_meter_device], [hmd_visible], [hmd_stored_name], [hmd_meter_name]) VALUES (10, 0, 3, N'THEORETICAL WIN', 0, 1, 20150622, 0, 1, N'SmartFloor_TVH', N'THEORETICAL_WIN')
INSERT [dbo].[h_meters_definition] ([hmd_meter_id], [hmd_meter_item], [hmd_meter_type], [hmd_description], [hmd_meter_mask], [hmd_meter_enabled], [hmd_meter_last_update], [hmd_meter_device], [hmd_visible], [hmd_stored_name], [hmd_meter_name]) VALUES (11, 0, 3, N'LOSS', 0, 1, 20150622, 0, 1, N'SmartFloor_TVH', N'LOSS')
INSERT [dbo].[h_meters_definition] ([hmd_meter_id], [hmd_meter_item], [hmd_meter_type], [hmd_description], [hmd_meter_mask], [hmd_meter_enabled], [hmd_meter_last_update], [hmd_meter_device], [hmd_visible], [hmd_stored_name], [hmd_meter_name]) VALUES (13, 0, 3, N'PRIZE', 0, 1, 20150622, 0, 1, N'SmartFloor_TVH', N'PRIZE')
INSERT [dbo].[h_meters_definition] ([hmd_meter_id], [hmd_meter_item], [hmd_meter_type], [hmd_description], [hmd_meter_mask], [hmd_meter_enabled], [hmd_meter_last_update], [hmd_meter_device], [hmd_visible], [hmd_stored_name], [hmd_meter_name]) VALUES (14, 0, 3, N'PLAYED COUNT', 0, 1, 20150622, 0, 1, N'SmartFloor_TVH', N'PLAYED_COUNT')
INSERT [dbo].[h_meters_definition] ([hmd_meter_id], [hmd_meter_item], [hmd_meter_type], [hmd_description], [hmd_meter_mask], [hmd_meter_enabled], [hmd_meter_last_update], [hmd_meter_device], [hmd_visible], [hmd_stored_name], [hmd_meter_name]) VALUES (15, 0, 3, N'DENOMINATION', 0, 1, 20150622, 0, 1, N'SmartFloor_TVH', N'DENOMINATION')
INSERT [dbo].[h_meters_definition] ([hmd_meter_id], [hmd_meter_item], [hmd_meter_type], [hmd_description], [hmd_meter_mask], [hmd_meter_enabled], [hmd_meter_last_update], [hmd_meter_device], [hmd_visible], [hmd_stored_name], [hmd_meter_name]) VALUES (16, 0, 3, N'MACHINE OCUPATTION', 0, 1, 20150622, 0, 1, N'SmartFloor_TVH', N'MACHINE_OCUPATTION')
INSERT [dbo].[h_meters_definition] ([hmd_meter_id], [hmd_meter_item], [hmd_meter_type], [hmd_description], [hmd_meter_mask], [hmd_meter_enabled], [hmd_meter_last_update], [hmd_meter_device], [hmd_visible], [hmd_stored_name], [hmd_meter_name]) VALUES (8, 1, 2, N'PROMOTIONS_TOTAL', 0, 1, 20150622, 0, 1, N'SmartFloor_TVH', N'PROMOTIONS_TOTAL')
INSERT [dbo].[h_meters_definition] ([hmd_meter_id], [hmd_meter_item], [hmd_meter_type], [hmd_description], [hmd_meter_mask], [hmd_meter_enabled], [hmd_meter_last_update], [hmd_meter_device], [hmd_visible], [hmd_stored_name], [hmd_meter_name]) VALUES (8, 2, 2, N'PROMOTIONS_GRANTED', 0, 1, 20150622, 0, 1, N'SmartFloor_TVH', N'PROMOTIONS_GRANTED')
INSERT [dbo].[h_meters_definition] ([hmd_meter_id], [hmd_meter_item], [hmd_meter_type], [hmd_description], [hmd_meter_mask], [hmd_meter_enabled], [hmd_meter_last_update], [hmd_meter_device], [hmd_visible], [hmd_stored_name], [hmd_meter_name]) VALUES (3, 4, 2, N'REGISTERED_LEVEL_PLATINUM', 0, 1, 20150622, 0, 1, N'SmartFloor_TVH', N'REGISTERED_LEVEL_PLATINUM')
INSERT [dbo].[h_meters_definition] ([hmd_meter_id], [hmd_meter_item], [hmd_meter_type], [hmd_description], [hmd_meter_mask], [hmd_meter_enabled], [hmd_meter_last_update], [hmd_meter_device], [hmd_visible], [hmd_stored_name], [hmd_meter_name]) VALUES (3, 5, 2, N'REGISTERED_LEVEL_WIN', 0, 1, 20150622, 0, 1, N'SmartFloor_TVH', N'REGISTERED_LEVEL_WIN')

GO

TRUNCATE TABLE [dbo].[h_x2d_control]
TRUNCATE TABLE [dbo].[h_w2d_smh]
TRUNCATE TABLE [dbo].[h_m2d_smh]
TRUNCATE TABLE [dbo].[h_y2d_smh]

GO

/****** Object:  StoredProcedure [dbo].[SmartFloor_TVH]    Script Date: 04/08/2016 09:37:23 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SmartFloor_TVH]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SmartFloor_TVH]
GO

/****** Object:  StoredProcedure [dbo].[SmartFloor_TVH]    Script Date: 04/08/2016 09:37:23 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/* */

CREATE PROCEDURE [dbo].[SmartFloor_TVH]
  @DateStart       DATETIME,
  @DateEnd         DATETIME,
  @IsTitoMode      BIT
AS
BEGIN
  DECLARE @DateStartZero AS DATETIME  
  DECLARE @DateEndZero AS DATETIME  
  DECLARE @BetAvgMinHighRoller AS MONEY
  DECLARE @SiteIdentifier AS INT
  DECLARE @DateOpening AS DATETIME
 
  SET @DateStartZero = (SELECT DATEADD(DAY,DATEDIFF(DAY,0,@DateStart),0)) 
  SET @DateEndZero = (SELECT DATEADD(DAY,DATEDIFF(DAY,0,@DateEnd),0)) 
  SET @SiteIdentifier = ISNULL((SELECT GP_KEY_VALUE 
								  FROM GENERAL_PARAMS 
								 WHERE GP_GROUP_KEY = 'Site' 
								   AND GP_SUBJECT_KEY = 'Identifier'),0)
  SET @DateOpening = dbo.Opening(@SiteIdentifier, @DateStart)                     
  
  SELECT    TERMINALS.TE_TERMINAL_ID                                                          AS TERMINAL_ID
          , CAST(CONVERT(NVARCHAR(8),@DateOpening,112) AS INTEGER)                            AS T_DATE
          , SPH.[APUESTA MEDIA]                                                               AS BET_AVG
          , SPH.JUGADO                                                                        AS PLAYED
          , SPH.PREMIOS                                                                       AS PRIZE
          , CASE WHEN (@IsTitoMode = 1) THEN 
              ACCOUNT_MOV.[DEPOSIT A] + ACCOUNT_MOV.[PRIZE COUPON] + PS2.PS_CASH_IN
            ELSE 
              ACCOUNT_MOV.[DEPOSIT A] + ACCOUNT_MOV.[PRIZE COUPON]
           END                                                                               AS COIN_IN          -- CASH IN (Recargas / Bill In)
            
          , SPH.[NET WIN]                                                                     AS NET_WIN
          , SPH.[THEORICAL WON]                                                               AS THEORETICAL_WIN
          , SPH.[NUM JUGADAS]                                                                 AS PLAYED_COUNT
          , TE_DENOMINATION                                                                   AS DENOMINATION     --�TE_MULTI_DENOMINATION?
          , SPH.LOSS                                                                          AS LOSS
          , CAST(CAST(OCUP.OCCUPIED * 100 AS DECIMAL) / 
            (CAST( (OCUP.TERMINAL_COUNT_MASTER * 24 * 60 * 60) AS DECIMAL)) AS DECIMAL(10,3)) AS MACHINE_OCUPATTION
     FROM   TERMINALS

-- SALES PER HOUR
INNER JOIN  
        (
          SELECT  SPH_TERMINAL_ID  
                , SUM(CASE WHEN SALES_PER_HOUR_V2.SPH_PLAYED_COUNT > 0 THEN 
                    ROUND( SALES_PER_HOUR_V2.SPH_PLAYED_AMOUNT / 
                           SALES_PER_HOUR_V2.SPH_PLAYED_COUNT, 2)
                  ELSE
                    0                                                               
                  END)                                                              AS [APUESTA MEDIA]
                , SUM(SALES_PER_HOUR_V2.SPH_PLAYED_AMOUNT)                          AS [JUGADO]
                , SUM(SALES_PER_HOUR_V2.SPH_WON_AMOUNT)                             AS [PREMIOS]
                , SUM(ISNULL(SALES_PER_HOUR_V2.SPH_PLAYED_AMOUNT,0) - 
                  (ISNULL(SALES_PER_HOUR_V2.SPH_WON_AMOUNT,0) + 
                  ISNULL(SALES_PER_HOUR_V2.SPH_PROGRESSIVE_PROVISION_AMOUNT,0) - 
                  ISNULL(SALES_PER_HOUR_V2.SPH_PROGRESSIVE_JACKPOT_AMOUNT_0,0)))    AS [NET WIN]
                , SUM(SALES_PER_HOUR_V2.SPH_THEORETICAL_WON_AMOUNT)                 AS [THEORICAL WON]
                , SUM(SALES_PER_HOUR_V2.SPH_PLAYED_COUNT)                           AS [NUM JUGADAS]
                , SUM(ISNULL(SALES_PER_HOUR_V2.SPH_PLAYED_AMOUNT,0) - 
                  (ISNULL(SALES_PER_HOUR_V2.SPH_WON_AMOUNT,0) + 
                  ISNULL(SALES_PER_HOUR_V2.SPH_PROGRESSIVE_PROVISION_AMOUNT,0) - 
                  ISNULL(SALES_PER_HOUR_V2.SPH_PROGRESSIVE_JACKPOT_AMOUNT_0,0)))    AS [LOSS]
            FROM  SALES_PER_HOUR_V2
           WHERE  SPH_BASE_HOUR >= @DateStart 
             AND  SPH_BASE_HOUR < @DateEnd
        GROUP BY  SPH_TERMINAL_ID) SPH
              ON  TERMINALS.TE_TERMINAL_ID = SPH.SPH_TERMINAL_ID

-- ACCOUNT_MOVEMENTS (PDTE REVISAR)
  LEFT JOIN
         ( 
        SELECT  AM_TERMINAL_ID 
              , SUM(CASE AM_TYPE WHEN  1  THEN AM_ADD_AMOUNT ELSE 0 END)  AS  [DEPOSIT A]         -- Recarga / Cash In
              , SUM(CASE AM_TYPE WHEN 44  THEN AM_ADD_AMOUNT ELSE 0 END)  AS  [PRIZE COUPON]      -- Cup�n Premio
          FROM  ACCOUNT_MOVEMENTS WITH (INDEX (IX_TYPE_DATE_ACCOUNT) )                       
         WHERE  AM_DATETIME >= @DateStart                                                   
            AND  AM_DATETIME <  @DateEnd 
			AND (AM_TYPE=1 OR AM_TYPE=44) 
      GROUP BY  AM_TERMINAL_ID) 
ACCOUNT_MOV ON  ACCOUNT_MOV.AM_TERMINAL_ID = TERMINALS.TE_TERMINAL_ID

-- MACHINE OCUPATTION % (PDTE REVISAR)
LEFT JOIN 
        (
        SELECT  TE_TERMINAL_ID
              , TE_MASTER_ID
              , SUM(DATEDIFF (SECOND, PS_STARTED, PS_FINISHED))     AS OCCUPIED 
              , COUNT(DISTINCT TC_DATE)                             AS CONNECTED
              , CASE WHEN COUNT(DISTINCT TC_DATE) = 0 THEN 
                  CASE WHEN TERMINALS.TE_MASTER_ID IS NULL THEN 0
                  ELSE 1
                  END 
                  ELSE
                    COUNT(DISTINCT TC_DATE)
                  END                                               AS TERMINAL_COUNT_MASTER
          FROM  TERMINALS
     LEFT JOIN  PLAY_SESSIONS 
            ON  TE_TERMINAL_ID = PS_TERMINAL_ID 
           AND  PS_FINISHED >= @DateStart 
           AND  PS_FINISHED <  @DateEnd 
     LEFT JOIN  TERMINALS_CONNECTED  
            ON  TC_TERMINAL_ID = TE_TERMINAL_ID 
           AND  TC_DATE >= @DateStartZero
           AND  TC_DATE <  @DateEndZero
      GROUP BY  TE_TERMINAL_ID, TE_MASTER_ID
      ) OCUP
    ON OCUP.TE_TERMINAL_ID = TERMINALS.TE_TERMINAL_ID
    
-- ACCOUNTS (GENDER: MALE, FEMALE; USER TYPE: REGISTERED(level), ANONYMOUS, UNKNOWN)
LEFT JOIN 
  (
  SELECT 
        PS.PS_TERMINAL_ID AS PSI
      , SUM(PS.PS_CASH_IN)                  AS PS_CASH_IN
  FROM 
    (
      SELECT  PS_TERMINAL_ID
            , AC_ACCOUNT_ID
            , SUM(PS_CASH_IN)               AS PS_CASH_IN
        FROM  PLAY_SESSIONS
  INNER JOIN  ACCOUNTS
          ON  PS_ACCOUNT_ID = ACCOUNTS.AC_ACCOUNT_ID
       WHERE  PS_FINISHED >= @DATESTART                       
         AND  PS_FINISHED <  @DATEEND                       
         AND  PS_STATUS   <> 0
    GROUP BY  PS_TERMINAL_ID
            , AC_ACCOUNT_ID) PS
    GROUP BY PS_TERMINAL_ID
      ) PS2
   ON PS2.PSI = TERMINALS.TE_TERMINAL_ID


-- PROMOTIONS (ASSIGNED & GRANTED)
   SELECT   PM.PROMOTIONS_TOTAL
          , PMG.PROMOTIONS_GRANTED
          , PLAYERS2.GENDER_MALE
          , PLAYERS2.GENDER_FEMALE
          , PLAYERS2.GENDER_UNKNOWN
          , PLAYERS2.ANONYMOUSS
          , PLAYERS2.REGISTERED_LEVEL_SILVER
          , PLAYERS2.REGISTERED_LEVEL_GOLD
          , PLAYERS2.REGISTERED_LEVEL_PLATINUM
          , PLAYERS2.REGISTERED_LEVEL_WIN
          , PLAYERS2.NEW_CLIENTS
     FROM   
            (SELECT COUNT(*) AS PROMOTIONS_TOTAL
               FROM PROMOTIONS
              WHERE  @DATESTART <= promotions.pm_date_finish AND @DATEEND >= promotions.pm_date_start) PM
          , (SELECT COUNT(*) AS PROMOTIONS_GRANTED
               FROM account_promotions
              WHERE account_promotions.acp_created BETWEEN @DATESTART AND @DATEEND) PMG
          
          , (SELECT   COUNT(PLAYERS.GENDER_MALE)               AS GENDER_MALE 
                    , COUNT(PLAYERS.GENDER_FEMALE)             AS GENDER_FEMALE
                    , COUNT(PLAYERS.GENDER_UNKNOWN)            AS GENDER_UNKNOWN
                    , COUNT(PLAYERS.ANONYMOUSS)                AS ANONYMOUSS
                    , COUNT(PLAYERS.REGISTERED_LEVEL_SILVER)   AS REGISTERED_LEVEL_SILVER
                    , COUNT(PLAYERS.REGISTERED_LEVEL_GOLD)     AS REGISTERED_LEVEL_GOLD
                    , COUNT(PLAYERS.REGISTERED_LEVEL_PLATINUM) AS REGISTERED_LEVEL_PLATINUM
                    , COUNT(PLAYERS.REGISTERED_LEVEL_WIN)      AS REGISTERED_LEVEL_WIN
                    , COUNT(PLAYERS.NEW_CLIENTS)               AS NEW_CLIENTS 
                FROM 
                  (
                    SELECT  AC_ACCOUNT_ID
                          , SUM(CASE WHEN AC_HOLDER_GENDER = 1 THEN 1 END)                    AS GENDER_MALE
                          , SUM(CASE WHEN AC_HOLDER_GENDER = 2 THEN 1 END)                    AS GENDER_FEMALE
                          , SUM(CASE WHEN AC_HOLDER_GENDER NOT IN (1, 2) THEN 1 END)          AS GENDER_UNKNOWN
                          , SUM(CASE WHEN AC_TYPE = 2 
                                      AND AC_HOLDER_LEVEL = 0 THEN 1 
                                END)                                                          AS ANONYMOUSS
                          , SUM(CASE WHEN AC_TYPE = 2 
                                      AND AC_HOLDER_LEVEL = 1 THEN 1 
                                END)                                                          AS REGISTERED_LEVEL_SILVER
                          , SUM(CASE WHEN AC_TYPE = 2 
                                      AND AC_HOLDER_LEVEL = 2 THEN 1 
                                END)                                                          AS REGISTERED_LEVEL_GOLD
                          , SUM(CASE WHEN AC_TYPE = 2 
                                      AND AC_HOLDER_LEVEL = 3 THEN 1 
                                END)                                                          AS REGISTERED_LEVEL_PLATINUM
                          , SUM(CASE WHEN AC_TYPE = 2 
                                      AND AC_HOLDER_LEVEL = 4 THEN 1 
                                END)                                                          AS REGISTERED_LEVEL_WIN
                          , SUM(CASE WHEN DATEDIFF(DAY,AC_CREATED, GETDATE())<=1 THEN 1 END)  AS NEW_CLIENTS
                          , SUM(PS_CASH_IN)                                                   AS PS_CASH_IN
                      FROM  PLAY_SESSIONS
                INNER JOIN  ACCOUNTS
                        ON  PS_ACCOUNT_ID = ACCOUNTS.AC_ACCOUNT_ID
                     WHERE  PS_FINISHED >= @DATESTART                       
                       AND  PS_FINISHED <  @DATEEND                       
                       AND  PS_STATUS   <> 0
                  GROUP BY  AC_ACCOUNT_ID) PLAYERS
                 ) PLAYERS2
               



END

GO

/****** Object:  StoredProcedure [dbo].[SmartFloor_PVH]    Script Date: 04/08/2016 09:37:42 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SmartFloor_PVH]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SmartFloor_PVH]
GO


/****** Object:  StoredProcedure [dbo].[SmartFloor_PVH]    Script Date: 04/08/2016 09:37:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
 
/* */
  
CREATE PROCEDURE [dbo].[SmartFloor_PVH]
  @DateStart       DATETIME,
  @DateEnd         DATETIME,
  @IsTitoMode      BIT
  
  -- JRC 21-SEP-2015 Reads and Inserts or Updates as needed into H_PVH

AS
BEGIN
  
  DECLARE @ROW_COUNT INT 
  SET @ROW_COUNT= 0
  
  -- ACCOUNTS
     SELECT   ACCOUNT_INFO.AC_ACCOUNT_ID                                            AS PVH_ACCOUNT_ID 
            , CAST(CONVERT(NVARCHAR(8),@DateEnd,112) AS INTEGER)                    AS PVH_DATE
            , DATEPART(DW,@DateEnd)                                                 AS PVH_WEEKDAY
            , 1                                                                     AS PVH_VISIT
            
            -- INFO CHECK-IN, GAME TIME, ROOM TIME, etc... BY PLAYER.
            , CASE WHEN ACCOUNT_MOV.[FIRST MOV] < PLAY_SES.STARTSESSION THEN
                ACCOUNT_MOV.[FIRST MOV]
              ELSE
                PLAY_SES.STARTSESSION
              END                                                                   AS PVH_CHECK_IN               -- Fecha en la que ha entrado el player al casino
            , CASE WHEN ACCOUNT_MOV.[LAST MOV] > PLAY_SES.LASTSESSION THEN
                ACCOUNT_MOV.[LAST MOV]
              ELSE
                PLAY_SES.LASTSESSION
              END                                                                   AS PVH_CHECK_OUT              -- Fecha en la que ha ha salido el player del casino
            , DATEDIFF(MINUTE, 
                CASE WHEN ACCOUNT_MOV.[FIRST MOV] < PLAY_SES.STARTSESSION THEN
                  ACCOUNT_MOV.[FIRST MOV]
                ELSE
                  PLAY_SES.STARTSESSION
                END,
                CASE WHEN ACCOUNT_MOV.[LAST MOV] > PLAY_SES.LASTSESSION THEN
                  ACCOUNT_MOV.[LAST MOV]
                ELSE
                  PLAY_SES.LASTSESSION
                END)                                                                AS PVH_ROOM_TIME              -- Tiempo (minutos) que est� el "player" dentro del Casino
            
            -- INFO TOTAL PLAYED, JACKPOTS WON, etc...    
            , PLAY_SES.GAME_TIME                                                    AS PVH_GAME_TIME              -- Tiempo de Juego (minutos) que lleva el player jugando en la m�quina
            , PLAY_SES.TOTAL_PLAYED_COUNT                                           AS PVH_TOTAL_PLAYED_COUNT     -- Num Jugadas (count)
            , PLAY_SES.TOTAL_WON_COUNT                                              AS PVH_PLAYED_WON_COUNT       -- Num Jugadas Ganadas (count)
            , PLAY_SES.TOTAL_PLAYED                                                 AS PVH_TOTAL_PLAYED           -- Total Monto Jugado
            , CASE WHEN PLAY_SES.TOTAL_PLAYED_COUNT > 0 THEN 
                ROUND(PLAY_SES.TOTAL_PLAYED / PLAY_SES.TOTAL_PLAYED_COUNT,2) 
              ELSE 
                0 
              END                                                                   AS PVH_TOTAL_BET_AVG          -- Apuesta Media (Total Jugado / Num Jugadas)
            , JACKPOTS.HP_AMOUNT                                                    AS PVH_JACKPOTS_WON           -- JACKPOTS Won (amount)
            , AMOUNT_WON                                                            AS PVH_TOTAL_WON
            --, ISNULL(ACCOUNT_MOV.REFUNDS,0) + 
            --  ISNULL(ACCOUNT_MOV.PRIZE,0) +                                                                     
            --  ISNULL(JACKPOTS.HP_AMOUNT,0)                                          AS PVH_TOTAL_WON              -- MONEY_OUT + JACKPOTS
              
            -- INFO TOTAL PLAYED (REEDEMABLE AND NOT REEDEMABLE)...   
            , PLAY_SES.TOTAL_PLAYED_REDEEM                                          AS PVH_RE_PLAYED              -- Total Jugado Redimible
            , PLAY_SES.TOTAL_PLAYED_NON_REDEEM                                      AS PVH_NR_PLAYED              -- Total Jugado NO Redimible
            
            -- INFO THEORICAL WON
            , POS.PO_THEO_WON                                                       AS PVH_THEORICAL_WON          -- Te�rico Ganado (Jugado * PayOut Te�rico de la m�quina)
            
            -- INFO MONEY IN, MONEY OUT, JACKPOTS, TAX, etc...
            , CASE WHEN (@IsTitoMode = 1) THEN 
                ISNULL(ACCOUNT_MOV.[DEPOSIT A],0) + 
                ISNULL(ACCOUNT_MOV.[PRIZE COUPON],0) + 
                ISNULL(PLAY_SES.BILL_IN,0)
              ELSE 
                ISNULL(ACCOUNT_MOV.[DEPOSIT A],0) + 
                ISNULL(ACCOUNT_MOV.[PRIZE COUPON],0)
              END                                                                   AS PVH_MONEY_IN               -- CASH IN (Recargas / Bill In)
              
            , ISNULL(ACCOUNT_MOV.[MONEY IN TAX],0)                                  AS PVH_MONEY_IN_TAX           -- CASH IN TAX
            
            , ISNULL(ACCOUNT_MOV.REFUNDS,0) + 
              ISNULL(ACCOUNT_MOV.PRIZE,0)                                           AS PVH_MONEY_OUT              -- TOTAL DINERO RETIRADO (BRUTO)
            
            , PLAY_SES.TOTAL_RE_WON_AMOUNT                                          AS PVH_RE_WON
            , PLAY_SES.TOTAL_NR_WON_AMOUNT                                          AS PVH_NR_WON
            , ACCOUNT_MOV.REFUNDS                                                   AS PVH_DEVOLUTION             -- DEVOLUCI�N
            , ACCOUNT_MOV.PRIZE                                                     AS PVH_PRIZE                  -- PREMIO
            , ACCOUNT_MOV.[TAX ON PRIZE 1]                                          AS PVH_TAX1                   -- TAX 1 (PREMIO)
            , ACCOUNT_MOV.[TAX ON PRIZE 2]                                          AS PVH_TAX2                   -- TAX 2 (PREMIO)
            , ACCOUNT_MOV.NUM_RECHARGES                                             AS PVH_MONEY_IN_COUNT         -- CAHSLESS (NUM RECARGAS) | TITO (NUM BILL IN) 
            , ACCOUNT_MOV.NUM_REFUNDS                                               AS PVH_MONEY_OUT_COUNT        -- NUM RETIROS DE CAJA O COBRO DE TICKETS
            
            -- INFO PLAYER (AGE, LEVEL, NUM REGISTERED DAYS, etc...)
            , ISNULL(DATEDIFF(YEAR,AC_HOLDER_BIRTH_DATE, GETDATE()),0)              AS PVH_AGE                    
            , DATEDIFF(DAY,AC_CREATED, GETDATE())                                    AS PVH_REGISTERED_NUM_DAYS
            , ACCOUNT_INFO.AC_HOLDER_LEVEL                                          AS PVH_LEVEL
       INTO #TEMP_ACCOUNT_INFO
     FROM   ACCOUNTS AS ACCOUNT_INFO
      
  -- PLAY SESSIONS
    INNER JOIN 
        (                                                     
        SELECT    PS_ACCOUNT_ID                                
              , MIN(PS_STARTED)                                               AS   STARTSESSION               -- Primera play sessi�n
              , MAX(PS_FINISHED)                                              AS   LASTSESSION                -- �ltima play sessi�n
              , DATEDIFF (MINUTE,   MIN(PS_STARTED), MAX(PS_FINISHED))        AS   GAME_TIME                  -- Tiempo que lleva el player jugando
              , SUM(PS_PLAYED_AMOUNT)                                         AS   TOTAL_PLAYED               -- Total monto jugado (redimible y no redimible)
              , SUM(PS_PLAYED_COUNT)                                          AS   TOTAL_PLAYED_COUNT         -- Total jugadas 
              , SUM(PS_WON_COUNT)                                             AS   TOTAL_WON_COUNT            -- Total numero de jugadas ganadas  
              , SUM(PS_REDEEMABLE_PLAYED)                                     AS   TOTAL_PLAYED_REDEEM        -- Total jugado redimible
              , SUM(PS_NON_REDEEMABLE_PLAYED)                                 AS   TOTAL_PLAYED_NON_REDEEM    -- Total jugado no redimible
              , SUM(PS_WON_AMOUNT)                                            AS   AMOUNT_WON                 -- Total ganado (redimible y no redimible)
              , SUM(PS_CASH_IN)                                               AS   BILL_IN                    -- Bill In (ONLY FOR TITO)
              , SUM(PS_REDEEMABLE_WON)                                        AS   TOTAL_RE_WON_AMOUNT        -- Total ganado (redimible)
              , SUM(PS_NON_REDEEMABLE_WON)                                    AS   TOTAL_NR_WON_AMOUNT        -- Total ganado (no redimible)
        FROM    PLAY_SESSIONS                                   
         WHERE    PS_FINISHED >= @DateStart                       
           AND    PS_FINISHED <  @DateEnd                       
           AND    PS_STATUS   <> 0                               
     GROUP  BY    PS_ACCOUNT_ID) AS PLAY_SES 
    ON    PLAY_SES.PS_ACCOUNT_ID = ACCOUNT_INFO.AC_ACCOUNT_ID
     
  -- ACCOUNT_MOVEMENTS
    LEFT JOIN
          ( 
        SELECT  AM_ACCOUNT_ID 
            , SUM(CASE AM_TYPE WHEN  1  THEN AM_ADD_AMOUNT ELSE 0 END)        AS  [DEPOSIT A]         -- Recarga / Cash In
            , SUM(CASE AM_TYPE WHEN  1  THEN 1 ELSE 0 END)                    AS  [NUM_RECHARGES]     -- N�mero de Recargas / Num de Billetes Introducidos
            
            , SUM(CASE AM_TYPE WHEN  3  THEN AM_SUB_AMOUNT ELSE 0 END)        AS  [REFUNDS]           -- Reembolso (Devoluci�n)
            , SUM(CASE AM_TYPE WHEN  3  THEN 1 ELSE 0 END)                    AS  [NUM_REFUNDS]       -- N�mero de Retiros (o cobro de tickets)
            
            , SUM(CASE AM_TYPE WHEN  2  THEN AM_SUB_AMOUNT ELSE 0 END)        AS  [PRIZE]             -- Premio
            , SUM(CASE AM_TYPE WHEN 44  THEN AM_ADD_AMOUNT ELSE 0 END)        AS  [PRIZE COUPON]      -- Cup�n Premio
            
            , SUM(CASE AM_TYPE WHEN 82  THEN AM_SUB_AMOUNT ELSE 0 END)        AS  [MONEY IN TAX]        
            , SUM(CASE AM_TYPE WHEN 4   THEN AM_SUB_AMOUNT ELSE 0 END)        AS  [TAX ON PRIZE 1]        
            , SUM(CASE AM_TYPE WHEN 13  THEN AM_SUB_AMOUNT ELSE 0 END)        AS  [TAX ON PRIZE 2]        
            , MIN(AM_DATETIME)                                                AS  [FIRST MOV]         -- Primer movimiento de cuenta 
            , MAX(AM_DATETIME)                                                AS  [LAST MOV]          -- �ltimo movimiento de cuenta 
          FROM  ACCOUNT_MOVEMENTS WITH (INDEX (IX_TYPE_DATE_ACCOUNT) )                       
         WHERE  AM_DATETIME >= @DateStart                                                   
             AND  AM_DATETIME <  @DateEnd
			 AND (AM_TYPE=1 OR AM_TYPE=3 OR AM_TYPE=2 OR AM_TYPE=4 OR AM_TYPE=13 OR AM_TYPE=44 OR AM_TYPE=82) 
      GROUP BY  AM_ACCOUNT_ID) ACCOUNT_MOV
    ON  ACCOUNT_MOV.AM_ACCOUNT_ID = ACCOUNT_INFO.AC_ACCOUNT_ID
        
  -- THEO WON
    LEFT JOIN 
      (
      SELECT    PO_RESULT.PS_ID                                               AS PS_ID
          , SUM(PO_RESULT.PS_RESULT)                                      AS PO_THEO_WON 
        FROM  
        (    
        SELECT    PS_ACCOUNT_ID                                                         AS PS_ID
            , SUM(TE_THEORETICAL_PAYOUT)                                            AS PS_PO
            , SUM(PLAY_SESSIONS.PS_PLAYED_AMOUNT)                                   AS PS_PA
            , SUM(TE_THEORETICAL_PAYOUT) * SUM(PLAY_SESSIONS.PS_PLAYED_AMOUNT)      AS PS_RESULT
          FROM    TERMINALS 
      INNER JOIN    PLAY_SESSIONS
          ON    PLAY_SESSIONS.PS_TERMINAL_ID = TERMINALS.TE_TERMINAL_ID
         WHERE    PS_FINISHED >= @DateStart                       
             AND    PS_FINISHED <  @DateEnd                       
             AND    PS_STATUS   <> 0                      
        GROUP BY    PS_ACCOUNT_ID
            , PS_PLAY_SESSION_ID
        ) PO_RESULT
      GROUP BY PO_RESULT.PS_ID
    ) POS       
    ON POS.PS_ID = ACCOUNT_INFO.AC_ACCOUNT_ID  
    
  -- HANDPAYS (JACKPOTS)
    LEFT JOIN
      (
      SELECT    PS_ACCOUNT_ID             AS PS_ACCOUNT_ID
          , SUM(HP_AMOUNT)            AS HP_AMOUNT
        FROM    HANDPAYS  
    INNER JOIN    PLAY_SESSIONS
        ON    PS_PLAY_SESSION_ID = HP_PLAY_SESSION_ID
       WHERE    HP_DATETIME >= @DateStart   
         AND    HP_DATETIME <  @DateEnd 
         AND    HP_TYPE IN(1, 20, 1001)   --  Jackpots(Progressive, Non Progressive, Major Prize), Site Jackpots and Manual Jackpots
         AND    HP_STATUS & 61440 = 32768 --  Paid
      GROUP BY    PS_ACCOUNT_ID
    ) JACKPOTS
    ON ACCOUNT_INFO.AC_ACCOUNT_ID = JACKPOTS.PS_ACCOUNT_ID
 
 
  CREATE INDEX IDX_TEMP_ACCOUNT_INFO ON #TEMP_ACCOUNT_INFO (PVH_ACCOUNT_ID, PVH_DATE)
  
  INSERT INTO H_PVH
    (
        PVH_ACCOUNT_ID
      , PVH_DATE
      , PVH_WEEKDAY             
      , PVH_VISIT               
      , PVH_CHECK_IN            
      , PVH_CHECK_OUT           
      , PVH_ROOM_TIME           
      , PVH_GAME_TIME           
      , PVH_TOTAL_PLAYED_COUNT  
      , PVH_PLAYED_WON_COUNT    
      , PVH_TOTAL_PLAYED        
      , PVH_TOTAL_BET_AVG       
      , PVH_JACKPOTS_WON        
      , PVH_TOTAL_WON           
      , PVH_RE_PLAYED           
      , PVH_NR_PLAYED           
      , PVH_THEORICAL_WON       
      , PVH_MONEY_IN            
      , PVH_MONEY_IN_TAX        
      , PVH_MONEY_OUT           
      , PVH_RE_WON              
      , PVH_NR_WON              
      , PVH_DEVOLUTION          
      , PVH_PRIZE               
      , PVH_TAX1                
      , PVH_TAX2                
      , PVH_MONEY_IN_COUNT      
      , PVH_MONEY_OUT_COUNT     
      , PVH_AGE                 
      , PVH_REGISTERED_NUM_DAYS 
      , PVH_LEVEL                 
    )
  SELECT
      TEMP.PVH_ACCOUNT_ID
    , TEMP.PVH_DATE
    , TEMP.PVH_WEEKDAY             
    , TEMP.PVH_VISIT              
    , TEMP.PVH_CHECK_IN           
    , TEMP.PVH_CHECK_OUT          
    , TEMP.PVH_ROOM_TIME          
    , TEMP.PVH_GAME_TIME          
    , TEMP.PVH_TOTAL_PLAYED_COUNT 
    , TEMP.PVH_PLAYED_WON_COUNT   
    , TEMP.PVH_TOTAL_PLAYED       
    , TEMP.PVH_TOTAL_BET_AVG      
    , TEMP.PVH_JACKPOTS_WON       
    , TEMP.PVH_TOTAL_WON          
    , TEMP.PVH_RE_PLAYED          
    , TEMP.PVH_NR_PLAYED          
    , TEMP.PVH_THEORICAL_WON      
    , TEMP.PVH_MONEY_IN           
    , TEMP.PVH_MONEY_IN_TAX       
    , TEMP.PVH_MONEY_OUT          
    , TEMP.PVH_RE_WON             
    , TEMP.PVH_NR_WON             
    , TEMP.PVH_DEVOLUTION         
    , TEMP.PVH_PRIZE              
    , TEMP.PVH_TAX1               
    , TEMP.PVH_TAX2               
    , TEMP.PVH_MONEY_IN_COUNT     
    , TEMP.PVH_MONEY_OUT_COUNT    
    , TEMP.PVH_AGE                
    , TEMP.PVH_REGISTERED_NUM_DAYS
    , TEMP.PVH_LEVEL                
  FROM #TEMP_ACCOUNT_INFO TEMP
  WHERE NOT EXISTS (SELECT 1 FROM H_PVH WHERE TEMP.PVH_ACCOUNT_ID = H_PVH.PVH_ACCOUNT_ID AND TEMP.PVH_DATE = H_PVH.PVH_DATE)  
  SET @ROW_COUNT = @@ROWCOUNT
  
  UPDATE H_PVH
  SET
      H_PVH.PVH_WEEKDAY             =  TEMP.PVH_WEEKDAY             
    , H_PVH.PVH_VISIT               =  TEMP.PVH_VISIT              
    , H_PVH.PVH_CHECK_IN            =  TEMP.PVH_CHECK_IN           
    , H_PVH.PVH_CHECK_OUT           =  TEMP.PVH_CHECK_OUT          
    , H_PVH.PVH_ROOM_TIME           =  TEMP.PVH_ROOM_TIME          
    , H_PVH.PVH_GAME_TIME           =  TEMP.PVH_GAME_TIME          
    , H_PVH.PVH_TOTAL_PLAYED_COUNT  =  TEMP.PVH_TOTAL_PLAYED_COUNT 
    , H_PVH.PVH_PLAYED_WON_COUNT    =  TEMP.PVH_PLAYED_WON_COUNT   
    , H_PVH.PVH_TOTAL_PLAYED        =  TEMP.PVH_TOTAL_PLAYED       
    , H_PVH.PVH_TOTAL_BET_AVG       =  TEMP.PVH_TOTAL_BET_AVG      
    , H_PVH.PVH_JACKPOTS_WON        =  TEMP.PVH_JACKPOTS_WON       
    , H_PVH.PVH_TOTAL_WON           =  TEMP.PVH_TOTAL_WON          
    , H_PVH.PVH_RE_PLAYED           =  TEMP.PVH_RE_PLAYED          
    , H_PVH.PVH_NR_PLAYED           =  TEMP.PVH_NR_PLAYED          
    , H_PVH.PVH_THEORICAL_WON       =  TEMP.PVH_THEORICAL_WON      
    , H_PVH.PVH_MONEY_IN            =  TEMP.PVH_MONEY_IN           
    , H_PVH.PVH_MONEY_IN_TAX        =  TEMP.PVH_MONEY_IN_TAX       
    , H_PVH.PVH_MONEY_OUT           =  TEMP.PVH_MONEY_OUT          
    , H_PVH.PVH_RE_WON              =  TEMP.PVH_RE_WON             
    , H_PVH.PVH_NR_WON              =  TEMP.PVH_NR_WON             
    , H_PVH.PVH_DEVOLUTION          =  TEMP.PVH_DEVOLUTION         
    , H_PVH.PVH_PRIZE               =  TEMP.PVH_PRIZE              
    , H_PVH.PVH_TAX1                =  TEMP.PVH_TAX1               
    , H_PVH.PVH_TAX2                =  TEMP.PVH_TAX2               
    , H_PVH.PVH_MONEY_IN_COUNT      =  TEMP.PVH_MONEY_IN_COUNT     
    , H_PVH.PVH_MONEY_OUT_COUNT     =  TEMP.PVH_MONEY_OUT_COUNT    
    , H_PVH.PVH_AGE                 =  TEMP.PVH_AGE                
    , H_PVH.PVH_REGISTERED_NUM_DAYS =  TEMP.PVH_REGISTERED_NUM_DAYS
    , H_PVH.PVH_LEVEL               =  TEMP.PVH_LEVEL                
  FROM H_PVH
  INNER JOIN #TEMP_ACCOUNT_INFO TEMP ON TEMP.PVH_ACCOUNT_ID = H_PVH.PVH_ACCOUNT_ID AND TEMP.PVH_DATE = H_PVH.PVH_DATE
  
  SET @ROW_COUNT = @ROW_COUNT + @@ROWCOUNT
  SELECT @ROW_COUNT
  
END


GO


