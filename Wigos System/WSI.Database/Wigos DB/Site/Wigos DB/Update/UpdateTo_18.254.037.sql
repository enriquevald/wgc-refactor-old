/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 253;

SET @New_ReleaseId = 254;
SET @New_ScriptName = N'UpdateTo_18.254.037.sql';
SET @New_Description = N'New GPs, added and modified SP and modified tables';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/******* TABLES *******/

IF NOT EXISTS(SELECT * FROM sys.columns where object_id = OBJECT_ID(N'[dbo].[handpays]') and name = 'hp_timestamp')
  ALTER TABLE dbo.HANDPAYS ADD HP_TIMESTAMP TIMESTAMP NOT NULL  
GO
  
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.account_points_cache') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[account_points_cache](
      apc_account_id bigint NOT NULL,
      apc_days int NOT NULL,
      apc_history_points_generated_for_level money NOT NULL,
      apc_history_points_discretional_for_level money NOT NULL,
      apc_history_points_discretional_only_for_redeem money NOT NULL,
      apc_history_points_promotion_only_for_redeem money NOT NULL,
      apc_today datetime NULL,
      apc_today_points_generated_for_level money NULL,
      apc_today_points_discretional_for_level money NULL,
      apc_today_points_discretional_only_for_redeem money NULL,
      apc_today_points_promotion_only_for_redeem money NULL,
      apc_today_last_movement_id bigint NULL,
      apc_today_last_updated datetime NULL,
  CONSTRAINT [PK_account_points_cache] PRIMARY KEY CLUSTERED 
(
      [apc_account_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

IF NOT EXISTS(SELECT * FROM sys.columns where object_id = OBJECT_ID(N'[dbo].[terminals]') and name = 'te_account_promotion_id')
  ALTER TABLE dbo.terminals ADD te_account_promotion_id bigint NULL
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[mb_movements]') and name = 'mbm_undo_status')
  ALTER TABLE mb_movements ADD mbm_undo_status INT NULL
GO

UPDATE SAS_METERS_CATALOG_PER_GROUP SET SMCG_GROUP_ID = 10004 WHERE (SMCG_METER_CODE = 25 OR  SMCG_METER_CODE = 26) AND  SMCG_GROUP_ID = 10001
GO

IF EXISTS (SELECT * FROM general_params WHERE gp_group_key = 'TITO' AND gp_subject_key = 'TITOMode' AND gp_key_value = '1')
  DELETE FROM REPORTS WHERE REP_TYPE = 0
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[play_sessions]') and name = 'ps_aux_ft_re_cash_in')
  ALTER TABLE [dbo].[play_sessions] ADD ps_aux_ft_re_cash_in MONEY NULL;
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[play_sessions]') and name = 'ps_aux_ft_nr_cash_in')
  ALTER TABLE [dbo].[play_sessions] ADD ps_aux_ft_nr_cash_in MONEY NULL;
GO

IF EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[play_sessions]') and name = 'ps_total_cash_in')
  ALTER TABLE [dbo].[play_sessions] DROP COLUMN [ps_total_cash_in] 
GO

ALTER TABLE [dbo].[play_sessions] ADD [ps_total_cash_in] AS  ((((([ps_initial_balance]+[ps_cash_in])+isnull([ps_re_ticket_in],(0)))+isnull([ps_promo_re_ticket_in],(0)))+ isnull([ps_aux_ft_re_cash_in],(0))+isnull([ps_promo_nr_ticket_in],(0))+isnull([ps_aux_ft_nr_cash_in],(0))))
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[handpays]') and name = 'hp_payment_mode')
  ALTER TABLE [dbo].[handpays] ADD hp_payment_mode INT NOT NULL DEFAULT 0;
GO

DECLARE @_is_tito AS INT
SELECT @_is_tito = GP_KEY_VALUE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'TITO' AND  GP_SUBJECT_KEY = 'TITOMode'

-- hp_payment_mode = 0-Manual, 1-Automatic, 2-Bonusing
-- hp_type         = 20-SITE JACKPOT 

IF (@_is_tito = 1)
  UPDATE handpays SET hp_payment_mode = 2 WHERE hp_type = 20       
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[handpays]') and name = 'hp_status_calculated')
  ALTER TABLE dbo.handpays ADD hp_status_calculated AS (hp_status & 0xF000)
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[progressives_provisions]')   AND name = 'pgp_current_amount') BEGIN 
  ALTER TABLE dbo.progressives_provisions ADD pgp_current_amount money NULL END
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[progressives_provisions_levels]')   AND name = 'ppl_current_amount') BEGIN 
  ALTER TABLE dbo.progressives_provisions_levels ADD ppl_current_amount money NULL END
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[progressives_provisions_terminals]')   AND name = 'ppt_diff_amount') BEGIN 
  ALTER TABLE dbo.progressives_provisions_terminals ADD ppt_diff_amount money NULL END
GO

/******* INDEXES *******/

IF  NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[handpays]') AND name = N'IX_hp_status_calculated')
  CREATE NONCLUSTERED INDEX [IX_hp_status_calculated] ON [dbo].[handpays] 
  (
        [hp_status_calculated] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[terminal_sas_meters_history]') AND name = N'IX_tsmh_type_code_datetime')
	CREATE NONCLUSTERED INDEX [IX_tsmh_type_code_datetime]
	ON [dbo].[terminal_sas_meters_history] (
		[tsmh_type] ASC,
		[tsmh_meter_code] ASC,
		[tsmh_datetime] ASC
	)
	INCLUDE ([tsmh_terminal_id],[tsmh_meter_increment])
	WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

/******* RECORDS *******/

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.Voucher' AND GP_SUBJECT_KEY = 'ShowVoucherId')
     INSERT INTO [dbo].[general_params] ([gp_group_key],[gp_subject_key],[gp_key_value])
                                VALUES ('Cashier.Voucher' ,'ShowVoucherId','1')
GO

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.Voucher' AND GP_SUBJECT_KEY = 'ShowOperationId')
     INSERT INTO [dbo].[general_params] ([gp_group_key],[gp_subject_key],[gp_key_value])
                                VALUES ('Cashier.Voucher' ,'ShowOperationId','1')
GO

UPDATE ALARM_CATALOG SET ALCG_NAME = 'Meter Big Increment', ALCG_DESCRIPTION = 'Meter Big Increment' WHERE ALCG_ALARM_CODE = 212994
UPDATE ALARM_CATALOG SET ALCG_NAME = 'Meter Rollover'     , ALCG_DESCRIPTION = 'Meter Rollover'      WHERE ALCG_ALARM_CODE = 131081
GO

INSERT INTO ALARM_CATALOG (ALCG_ALARM_CODE, ALCG_LANGUAGE_ID, ALCG_TYPE, ALCG_NAME, ALCG_DESCRIPTION, ALCG_VISIBLE)
  SELECT 589830, ALCG_LANGUAGE_ID, ALCG_TYPE, ALCG_NAME, ALCG_DESCRIPTION, ALCG_VISIBLE FROM  ALARM_CATALOG WHERE  ALCG_ALARM_CODE = 589827 
GO

INSERT INTO ALARM_CATALOG_PER_CATEGORY (ALCC_ALARM_CODE, ALCC_CATEGORY, ALCC_TYPE, ALCC_DATETIME) 
  VALUES (589830, 38, 0, GETDATE())
GO

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'WigosGUI' AND GP_SUBJECT_KEY ='HideMachineAndGameReport')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('WigosGUI', 'HideMachineAndGameReport', '1')
GO

DELETE GENERAL_PARAMS WHERE GP_GROUP_KEY = 'WCP' AND GP_SUBJECT_KEY = 'DailySasMetersMinutes'
GO

IF NOT EXISTS ( SELECT ALCG_ALARM_CODE FROM [ALARM_CATALOG] WHERE  ALCG_ALARM_CODE = 131082 )
  BEGIN
    INSERT INTO [ALARM_CATALOG] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (131082, 9, 0, 'Meter First Time', 'Meter First Time', 1)
    INSERT INTO [ALARM_CATALOG] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (131082, 10, 0, 'Meter Primera vez', 'Meter Primera vez', 1)	
	INSERT INTO [ALARM_CATALOG_PER_CATEGORY] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (131082, 1, 0, GETDATE())
  END
GO  

UPDATE [ALARM_CATALOG] SET [alcg_name] = 'Meters reset', [ALCG_DESCRIPTION] = 'Meters reset' WHERE ALCG_ALARM_CODE = 131080 AND ALCG_LANGUAGE_ID = 9		
UPDATE [ALARM_CATALOG] SET [alcg_name] = 'Reinicio Meters', [ALCG_DESCRIPTION] = 'Reinicio Meters' WHERE ALCG_ALARM_CODE = 131080 AND ALCG_LANGUAGE_ID = 10		
GO

IF NOT EXISTS(SELECT ST_TASK_ID FROM MS_SITE_TASKS WHERE ST_TASK_ID = 67) -- UploadHandpays
BEGIN
  INSERT   INTO MS_SITE_TASKS
         ( ST_TASK_ID, ST_ENABLED, ST_INTERVAL_SECONDS, ST_MAX_ROWS_TO_UPLOAD) 
  VALUES ( 67        , 1         , 3600               , 100                  )
END
GO

DELETE FROM LCD_FUNCTIONALITIES WHERE FUN_NAME = 'RAFFLES' OR FUN_NAME = 'CASINO JACKPOT'
GO

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'RegionalOptions' AND GP_SUBJECT_KEY ='DynamicFormatDecimals')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('RegionalOptions', 'DynamicFormatDecimals', '0')
GO

-- Commission check
IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.PaymentMethod' AND GP_SUBJECT_KEY = 'CommissionToCompanyB.Check.VoucherTitle')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Cashier.PaymentMethod', 'CommissionToCompanyB.Check.VoucherTitle', 'Comisi�n uso de cheque')
GO

-- Commission currency
IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.PaymentMethod' AND GP_SUBJECT_KEY = 'CommissionToCompanyB.Currency.VoucherTitle')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Cashier.PaymentMethod', 'CommissionToCompanyB.Currency.VoucherTitle', 'Comisi�n uso de divisa')
GO

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'WigosGUI' AND GP_SUBJECT_KEY ='HideProviderCashDailyReport')
  INSERT INTO [dbo].[general_params] VALUES ('WigosGUI', 'HideProviderCashDailyReport', '1');
GO 

IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Accounts' AND GP_SUBJECT_KEY = 'Player.BirthdayWarningDays')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Accounts', 'Player.BirthdayWarningDays', '7')
GO

IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY = 'Promotion.NR.MinAmountToCalculateCost')
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Cashier', 'Promotion.NR.MinAmountToCalculateCost', '0.00')
GO

IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY = 'Promotion.NR.ResetCostOnExtraBall')
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Cashier', 'Promotion.NR.ResetCostOnExtraBall', '0')
GO

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Progressives' AND GP_SUBJECT_KEY = 'Provision.MaxAllowedPerDay')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Progressives', 'Provision.MaxAllowedPerDay', '0')
GO

/******* STORED PROCEDURES *******/

--------------------------------------------------------------------------------
-- Copyright � 2014 Win Systems International
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: MachineAndGameReport.sql
-- 
--   DESCRIPTION: Procedure for Daily Machine Report 
-- 
--        AUTHOR: Jordi Masachs 
-- 
-- CREATION DATE: 30-DEC-2014
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 30-DEC-2014 JMV    First release
--------------------------------------------------------------------------------


--------------------------------------------------------------------------------
-- PURPOSE: Show meter history grouped by machine and by name
-- 
--  PARAMS:
--      - INPUT:
--  @pDateFrom DATETIME,  
--  @pDateTo   DATETIME,
--  @pTotalOnTop BIT = 0 
--
--      - OUTPUT:
--
-- RETURNS:
--                  
--
--   NOTES:
-- 
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MachineAndGameReport]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[MachineAndGameReport]
GO  

CREATE PROCEDURE [dbo].[MachineAndGameReport]
  @pDateFrom DATETIME,  
  @pDateTo   DATETIME,
  @pTotalOnTop BIT = 0 
AS

BEGIN

-- Cash in fields
DECLARE @_ticket_in_RE INT
DECLARE @_ticket_in_pro_RE INT
DECLARE @_ticket_in_pro_NR INT
DECLARE @_credit_from_bills INT

-- Cash out fields
DECLARE @_ticket_out_RE INT
DECLARE @_ticket_out_pro_RE INT
DECLARE @_ticket_out_pro_NR INT

-- Set field codes
SET @_ticket_in_RE = 0x80
SET @_ticket_in_pro_RE = 0x84
SET @_ticket_in_pro_NR = 0x82
SET @_credit_from_bills = 0x0B
SET @_ticket_out_RE = 0x86
SET @_ticket_out_pro_RE = 0x8A
SET @_ticket_out_pro_NR = 0x88

-- Create temp table for whole report
CREATE TABLE #TT_MACHINEGAME (TERMINAL_ID INT, TERMINAL_NAME NVARCHAR(50), GAMENAME NVARCHAR(50), CASH_IN MONEY, CASH_OUT MONEY, HANDPAYS MONEY, NETWIN MONEY)

-- Create temp table for terminal - game relation
CREATE TABLE #TT_GAMES (TTG_TERMINAL_ID INT, TTG_GAMENAME NVARCHAR(50))
INSERT INTO #TT_GAMES

    SELECT TGT_TERMINAL_ID AS TTG_TERMINAL_ID, CASE WHEN TOTAL > 1 THEN 'Multigame' ELSE TTG_GAMENAME END AS TTG_GAMENAME
    FROM
    (
        SELECT TGT_TERMINAL_ID, TOTAL, (SELECT TOP 1 PG_GAME_NAME FROM TERMINAL_GAME_TRANSLATION, PROVIDERS_GAMES WHERE TGT_TRANSLATED_GAME_ID = PG_GAME_ID AND TGT_TERMINAL_ID = XX.TGT_TERMINAL_ID) AS TTG_GAMENAME
        FROM
        (
            SELECT TGT_TERMINAL_ID, count(*) AS TOTAL FROM
            (
              SELECT DISTINCT TGT_TERMINAL_ID, TGT_TRANSLATED_GAME_ID, PG_GAME_NAME
                FROM TERMINAL_GAME_TRANSLATION, PROVIDERS_GAMES
               WHERE TGT_TRANSLATED_GAME_ID = PG_GAME_ID
            ) YY
            GROUP BY TGT_TERMINAL_ID
        ) AS XX
    ) AS HH

-- Insert into the result values
INSERT INTO #TT_MACHINEGAME
	SELECT TSMH_TERMINAL_ID AS TERMINAL_ID, TE_NAME AS TERMINAL_NAME, (CASE WHEN TTG_GAMENAME IS NOT NULL THEN TTG_GAMENAME ELSE 'UNKNOWN' END) AS GAME_NAME, 
	CASH_IN, CASH_OUT, ISNULL(HANDPAYS, 0) AS HANDPAYS, CASH_IN - CASH_OUT - ISNULL(HANDPAYS, 0) AS NETWIN 
	FROM  
	(	SELECT TSMH_TERMINAL_ID, 
			 SUM(CASE WHEN 
				TSMH_METER_CODE IN (@_ticket_in_RE, @_ticket_in_pro_RE, @_ticket_in_pro_NR, @_credit_from_bills) 		    
				THEN TSMH_METER_INCREMENT ELSE 0 END)/100.00 AS CASH_IN,
			 SUM(CASE WHEN 			
				TSMH_METER_CODE IN (@_ticket_out_RE, @_ticket_out_pro_RE, @_ticket_out_pro_NR) 		    
				THEN TSMH_METER_INCREMENT ELSE 0 END)/100.00 AS CASH_OUT
		FROM	TERMINAL_SAS_METERS_HISTORY WITH (INDEX(IX_tsmh_type_code_datetime))
		WHERE	TSMH_TYPE = 1  
				AND TSMH_DATETIME >= @pDateFrom AND TSMH_DATETIME < @pDateTo 
				AND TSMH_METER_CODE IN (@_ticket_in_RE, @_ticket_in_pro_RE, @_ticket_in_pro_NR, @_credit_from_bills, @_ticket_out_RE, @_ticket_out_pro_RE, @_ticket_out_pro_NR) 	
		GROUP BY TSMH_TERMINAL_ID ) AS TSMH_SUM 
	LEFT JOIN TERMINALS ON TSMH_TERMINAL_ID = TE_TERMINAL_ID
  LEFT JOIN #TT_GAMES ON TSMH_TERMINAL_ID = TTG_TERMINAL_ID
	LEFT JOIN 
	(
		SELECT 	
			HP_TERMINAL_ID,
			SUM(HP_AMOUNT) as HANDPAYS
			FROM HANDPAYS
			WHERE HP_DATETIME >= @pDateFrom AND HP_DATETIME < @pDateTo
		GROUP BY HP_TERMINAL_ID
		) AS HP_SUM ON TSMH_TERMINAL_ID = HP_TERMINAL_ID

-- Prepare report values and view
SELECT BLANK, CTYPE, CONCEPT, 
(CASE WHEN CASH_IN IS NOT NULL THEN CASH_IN ELSE 0 END) AS CASH_IN, 
(CASE WHEN CASH_OUT IS NOT NULL THEN CASH_OUT ELSE 0 END) AS CASH_OUT, 
(CASE WHEN HANDPAYS IS NOT NULL THEN HANDPAYS ELSE 0 END) AS HANDPAYS, 
(CASE WHEN NETWIN IS NOT NULL THEN NETWIN ELSE 0 END) AS NETWIN FROM
(
	SELECT '' as BLANK, 1 AS CTYPE, 2 AS CTYPE2, TERMINAL_NAME AS CONCEPT, SUM(CASH_IN) AS CASH_IN, SUM(CASH_OUT) AS CASH_OUT, SUM(HANDPAYS) AS HANDPAYS, SUM(NETWIN) AS NETWIN 
	FROM #TT_MACHINEGAME
	GROUP BY TERMINAL_NAME

	UNION

	SELECT '' as BLANK, 2 AS CTYPE, 3 AS CTYPE2, GAMENAME AS CONCEPT, SUM(CASH_IN) AS CASH_IN, SUM(CASH_OUT) AS CASH_OUT, SUM(HANDPAYS) AS HANDPAYS, SUM(NETWIN) AS NETWIN 
	FROM #TT_MACHINEGAME
	GROUP BY GAMENAME

	UNION

	SELECT '' as BLANK, 3 AS CTYPE, 1 AS CTYPE2, '' AS CONCEPT, SUM(CASH_IN) AS CASH_IN, SUM(CASH_OUT) AS CASH_OUT, SUM(HANDPAYS) AS HANDPAYS, SUM(NETWIN) AS NETWIN
	FROM #TT_MACHINEGAME
	) X

ORDER BY CASE @pTotalOnTop WHEN 0 THEN CTYPE ELSE CTYPE2 END

DROP TABLE #TT_MACHINEGAME

DROP TABLE #TT_GAMES

END
GO

GRANT EXECUTE ON [dbo].[MachineAndGameReport] TO [wggui] WITH GRANT OPTION
GO

--------------------------------------------------------------------------------
-- Copyright � 2010 Win Systems International
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: AccountPointsCache.sql
-- 
--   DESCRIPTION: Procedures for Accounts 
-- 
--        AUTHOR: Jos� Mart�nez L�pez
-- 
-- NOTES:
--       Product Backlog Item 151:AccountPointsCache --> Task 153: Procedures creation
-- 
-- CREATION DATE: 13-JAN-2015
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 13-JAN-2015 JML    First release.
--------------------------------------------------------------------------------


--------------------------------------------------------------------------------
-- PURPOSE: Calculate AccountPoints for level
-- 
--  PARAMS:
--      - INPUT:
--           @AccountId                        BIGINT,         
--           @DateFrom                         DATETIME,
--           @DateTo                           DATETIME,
--           @MovementId                       BIGINT, 
--
--      - OUTPUT:
--           @LastMovementId                   BIGINT OUTPUT, 
--           @PointsGeneratedForLevel          MONEY OUTPUT,
--           @PointsDiscretionalForLevel       MONEY OUTPUT,
--           @PointsDiscretionalOnlyForRedeem  MONEY OUTPUT,
--           @PointsPromotionOnlyForRedeem     MONEY OUTPUT
--
-- RETURNS:
--
--   NOTES:
--------------------------------------------------------------------------------


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AccountPointsCache_CalculatePoints]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[AccountPointsCache_CalculatePoints]
GO

CREATE PROCEDURE [dbo].[AccountPointsCache_CalculatePoints]
       @AccountId                        BIGINT,
       @DateFrom                         DATETIME,
       @DateTo                           DATETIME,
       @MovementId                       BIGINT, 
       @LastMovementId                   BIGINT OUTPUT, 
       @PointsGeneratedForLevel          MONEY  OUTPUT,
       @PointsDiscretionalForLevel       MONEY  OUTPUT,
       @PointsDiscretionalOnlyForRedeem  MONEY  OUTPUT,
       @PointsPromotionOnlyForRedeem     MONEY  OUTPUT
AS
BEGIN
  DECLARE @points_awarded                        AS INT;
  DECLARE @manually_added_points_for_level       AS INT;
  DECLARE @imported_points_for_level             AS INT;
  DECLARE @imported_points_history               AS INT;
  DECLARE @manually_added_points_only_for_redeem AS INT;
  DECLARE @imported_points_only_for_redeem       AS INT;
  DECLARE @promotion_point                       AS INT;
  DECLARE @cancel_promotion_point                AS INT;
 
  DECLARE @Sql             AS NVARCHAR(MAX);
  DECLARE @ParamDefinition AS NVARCHAR(MAX);
  DECLARE @Index           AS NVARCHAR(MAX);
  DECLARE @Where           AS NVARCHAR(MAX); 
  
  -- Points for level
     -- Generated
  SET @points_awarded                        = 36
     -- Discretional
  SET @manually_added_points_for_level       = 68
  SET @imported_points_for_level             = 72
  SET @imported_points_history               = 73
  -- Points only for redeem
     -- Discretional 
  SET @manually_added_points_only_for_redeem = 50
  SET @imported_points_only_for_redeem       = 71
     -- Promotion 
  SET @promotion_point                       = 60
  SET @cancel_promotion_point                = 61
  
  SET @LastMovementId = @MovementId

  SET @Index = 'PK_movements'
  SET @Where = ' WHERE AM_MOVEMENT_ID > ' + CAST(@MovementId AS NVARCHAR(MAX)) +
               '   AND AM_ACCOUNT_ID  = @AccountId '
  IF (@MovementId = 0)
  BEGIN
    SET @Index = 'IX_am_account_id_type_datetime'
    SET @Where = ' WHERE AM_ACCOUNT_ID  = @AccountId ' +
                 '   AND AM_DATETIME >= CAST(''' + CAST(@DateFrom AS VARCHAR(50)) + ''' AS DATETIME) ' +
                 '   AND AM_DATETIME  < CAST(''' + CAST(@DateTo AS VARCHAR(50)) + ''' AS DATETIME) '
  END

  SET @Where = @Where + 
               '   AND AM_TYPE IN (@points_awarded, @manually_added_points_for_level, @imported_points_for_level, @imported_points_history, ' +
               '                   @manually_added_points_only_for_redeem, @imported_points_only_for_redeem, @promotion_point, @cancel_promotion_point) '

  SET @Sql = '
  SELECT   @PointsGeneratedForLevel_out          = ISNULL(SUM (CASE WHEN AM_TYPE IN (@points_awarded)
                                                               THEN AM_ADD_AMOUNT 
                                                               ELSE 0 
                                                               END ), 0) 
         , @PointsDiscretionalForLevel_out       = ISNULL(SUM (CASE WHEN AM_TYPE IN (@manually_added_points_for_level, @imported_points_for_level, @imported_points_history)
                                                               THEN AM_ADD_AMOUNT 
                                                               ELSE 0 
                                                               END ), 0) 
         , @PointsDiscretionalOnlyForRedeem_out  = ISNULL(SUM (CASE WHEN AM_TYPE IN (@manually_added_points_only_for_redeem, @imported_points_only_for_redeem)
                                                               THEN AM_ADD_AMOUNT 
                                                               ELSE 0 
                                                               END ), 0)
         , @PointsPromotionOnlyForRedeem_out     = ISNULL(SUM (CASE WHEN AM_TYPE IN (@promotion_point, @cancel_promotion_point)
                                                               THEN AM_ADD_AMOUNT-AM_SUB_AMOUNT
                                                               ELSE 0 
                                                               END ), 0) 
         , @LastMovementId_out                   = ISNULL(MAX (AM_MOVEMENT_ID), @LastMovementId_out)
    FROM   ACCOUNT_MOVEMENTS WITH (INDEX (' + @Index + ')) ' 
  + @Where
 
SET @ParamDefinition = N'@AccountId                             BIGINT, 
                        @points_awarded                         INT, 
                        @manually_added_points_for_level        INT, 
                        @imported_points_for_level              INT, 
                        @imported_points_history                INT, 
                        @manually_added_points_only_for_redeem  INT, 
                        @imported_points_only_for_redeem        INT, 
                        @promotion_point                        INT, 
                        @cancel_promotion_point                 INT, 
                        @LastMovementId_out                     BIGINT OUTPUT, 
                        @PointsGeneratedForLevel_out            MONEY  OUTPUT, 
                        @PointsDiscretionalForLevel_out         MONEY  OUTPUT, 
                        @PointsDiscretionalOnlyForRedeem_out    MONEY  OUTPUT, 
                        @PointsPromotionOnlyForRedeem_out       MONEY  OUTPUT' 

EXEC sp_executesql @Sql, 
                   @ParamDefinition,
                   @AccountId                             = @AccountId, 
                   @points_awarded                        = @points_awarded,                        
                   @manually_added_points_for_level       = @manually_added_points_for_level,
                   @imported_points_for_level             = @imported_points_for_level,
                   @imported_points_history               = @imported_points_history, 
                   @manually_added_points_only_for_redeem = @manually_added_points_only_for_redeem, 
                   @imported_points_only_for_redeem       = @imported_points_only_for_redeem, 
                   @promotion_point                       = @promotion_point,
                   @cancel_promotion_point                = @cancel_promotion_point,
                   @LastMovementId_out                    = @LastMovementId                   OUTPUT, 
                   @PointsGeneratedForLevel_out           = @PointsGeneratedForLevel          OUTPUT,
                   @PointsDiscretionalForLevel_out        = @PointsDiscretionalForLevel       OUTPUT,
                   @PointsDiscretionalOnlyForRedeem_out   = @PointsDiscretionalOnlyForRedeem  OUTPUT,
                   @PointsPromotionOnlyForRedeem_out      = @PointsPromotionOnlyForRedeem     OUTPUT 

---------------------------------------------------------------------------------------------------------------------------------
-- Note:
-- @LastMovementId_out : Is the variable�s name of the output parameter of '@Sql execute'
-- When run 'EXEC sp_executesql' is finished, the value of @LastMovementId_out is assigned to @LastMovementId
---------------------------------------------------------------------------------------------------------------------------------

END  -- PROCEDURE [dbo].[AccountPointsCache_CalculatePoints]
GO

--------------------------------------------------------------------------------
-- PURPOSE: Save AccountPoints for level in DB (for simulate a cache)
-- 
--  PARAMS:
--      - INPUT:
--           @AccountId       BIGINT       
--
--      - OUTPUT:
--
-- RETURNS:
--
--   NOTES:
--------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AccountPointsCache_CalculateToday]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[AccountPointsCache_CalculateToday]
GO

CREATE PROCEDURE [dbo].[AccountPointsCache_CalculateToday]
       @AccountId     BIGINT
AS
BEGIN

  DECLARE @update_history                                  AS BIT;
  
  DECLARE @apc_today                                       AS DATETIME;
  DECLARE @apc_days                                        AS INT;
	DECLARE @apc_history_points_generated_for_level          AS MONEY;
	DECLARE @apc_history_points_discretional_for_level       AS MONEY;
	DECLARE @apc_history_points_discretional_only_for_redeem AS MONEY;
	DECLARE @apc_history_points_promotion_only_for_redeem    AS MONEY;
	DECLARE @apc_today_points_generated_for_level            AS MONEY;
	DECLARE @apc_today_points_discretional_for_level         AS MONEY;
	DECLARE @apc_today_points_discretional_only_for_redeem   AS MONEY;
	DECLARE @apc_today_points_promotion_only_for_redeem      AS MONEY;
  DECLARE @apc_today_last_movement_id                      AS BIGINT;
  
  DECLARE @estudy_period                                   AS INT;
  DECLARE @date_from                                       AS DATETIME;
  DECLARE @now                                             AS DATETIME;
  DECLARE @today_opening                                   AS DATETIME;
  
  SET @update_history = 0;
  
  -- Get register from ACCOUNT_POINTS_CACHE
  SELECT   @apc_today                  = APC_TODAY 
         , @apc_days                   = APC_DAYS
         , @apc_today_last_movement_id = APC_TODAY_LAST_MOVEMENT_ID
    FROM   ACCOUNT_POINTS_CACHE 
   WHERE   APC_ACCOUNT_ID = @AccountId 
     
  -- GET General param: "PlayerTracking", "Levels.DaysCountingPoints"
  SET @estudy_period = (SELECT CAST(GP_KEY_VALUE AS INT) FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PlayerTracking' AND GP_SUBJECT_KEY = 'Levels.DaysCountingPoints')
     
  --_day_start_counting_points = Misc.TodayOpening().AddDays(-(_days_counting_points - 1));
  SET @now = GETDATE();
  SET @today_opening = dbo.Opening(0, @now)
  SET @date_from = DATEADD(DAY, -1*(@estudy_period-1), @today_opening);
    
  IF (   @today_opening <> ISNULL(@apc_today, DATEADD(d, -1, GETDATE()))   --> no hay registro o cambio de jornada.
      OR @today_opening <> ISNULL(@apc_days, -1) )                         --> no hay registro o cambio en el periodo de estudio.
  BEGIN
    SET @update_history = 1;
  END   
    
  IF (@update_history = 1)
  BEGIN
    DELETE  FROM ACCOUNT_POINTS_CACHE 
     WHERE  APC_ACCOUNT_ID = @AccountId 

    SET @apc_today_last_movement_id = 0;
    
    -- Get history data 
    EXEC AccountPointsCache_CalculatePoints @AccountId, @date_from, @today_opening, 
                                            @apc_today_last_movement_id, 
                                            @apc_today_last_movement_id                      OUTPUT, 
                                            @apc_history_points_generated_for_level          OUTPUT, 
                                            @apc_history_points_discretional_for_level       OUTPUT, 
                                            @apc_history_points_discretional_only_for_redeem OUTPUT, 
                                            @apc_history_points_promotion_only_for_redeem    OUTPUT
    --> Get today data
    EXEC AccountPointsCache_CalculatePoints @AccountId, @today_opening, @now, 
                                            @apc_today_last_movement_id, 
                                            @apc_today_last_movement_id                    OUTPUT, 
                                            @apc_today_points_generated_for_level          OUTPUT, 
                                            @apc_today_points_discretional_for_level       OUTPUT, 
                                            @apc_today_points_discretional_only_for_redeem OUTPUT, 
                                            @apc_today_points_promotion_only_for_redeem    OUTPUT
  
    
    INSERT INTO ACCOUNT_POINTS_CACHE ( APC_ACCOUNT_ID, 
                                       APC_DAYS,       
                                       APC_HISTORY_POINTS_GENERATED_FOR_LEVEL,          
                                       APC_HISTORY_POINTS_DISCRETIONAL_FOR_LEVEL,
                                       APC_HISTORY_POINTS_DISCRETIONAL_ONLY_FOR_REDEEM,
                                       APC_HISTORY_POINTS_PROMOTION_ONLY_FOR_REDEEM,
                                       APC_TODAY, 
                                       APC_TODAY_POINTS_GENERATED_FOR_LEVEL,
                                       APC_TODAY_POINTS_DISCRETIONAL_FOR_LEVEL, 
                                       APC_TODAY_POINTS_DISCRETIONAL_ONLY_FOR_REDEEM, 
                                       APC_TODAY_POINTS_PROMOTION_ONLY_FOR_REDEEM,
                                       APC_TODAY_LAST_MOVEMENT_ID, 
                                       APC_TODAY_LAST_UPDATED ) 
                              VALUES ( @AccountId,     
                                       @estudy_period, 
                                       @apc_history_points_generated_for_level,              
                                       @apc_history_points_discretional_for_level, 
                                       @apc_history_points_discretional_only_for_redeem,     
                                       @apc_history_points_promotion_only_for_redeem,
                                       @today_opening, 
                                       @apc_today_points_generated_for_level, 
                                       @apc_today_points_discretional_for_level, 
                                       @apc_today_points_discretional_only_for_redeem, 
                                       @apc_today_points_promotion_only_for_redeem, 
                                       @apc_today_last_movement_id, 
                                       @now )  
  END
  ELSE
  BEGIN
    --> Get today data
    EXEC AccountPointsCache_CalculatePoints @AccountId, @today_opening, @now, 
                                            @apc_today_last_movement_id, 
                                            @apc_today_last_movement_id                    OUTPUT, 
                                            @apc_today_points_generated_for_level          OUTPUT, 
                                            @apc_today_points_discretional_for_level       OUTPUT, 
                                            @apc_today_points_discretional_only_for_redeem OUTPUT, 
                                            @apc_today_points_promotion_only_for_redeem    OUTPUT
    
    --update today
    UPDATE   ACCOUNT_POINTS_CACHE  
       SET   APC_TODAY                                      = @today_opening
           , APC_TODAY_POINTS_GENERATED_FOR_LEVEL           = APC_TODAY_POINTS_GENERATED_FOR_LEVEL + @apc_today_points_generated_for_level         
           , APC_TODAY_POINTS_DISCRETIONAL_FOR_LEVEL        = APC_TODAY_POINTS_DISCRETIONAL_FOR_LEVEL + @apc_today_points_discretional_for_level      
           , APC_TODAY_POINTS_DISCRETIONAL_ONLY_FOR_REDEEM  = APC_TODAY_POINTS_DISCRETIONAL_ONLY_FOR_REDEEM + @apc_today_points_discretional_only_for_redeem
           , APC_TODAY_POINTS_PROMOTION_ONLY_FOR_REDEEM     = APC_TODAY_POINTS_PROMOTION_ONLY_FOR_REDEEM + @apc_today_points_promotion_only_for_redeem   
           , APC_TODAY_LAST_MOVEMENT_ID                     = @apc_today_last_movement_id 
           , APC_TODAY_LAST_UPDATED                         = @now
     WHERE   APC_ACCOUNT_ID = @AccountId
     
  END
    
END  -- PROCEDURE [dbo].[AccountPointsCache_CalculateToday]
GO  

--------------------------------------------------------------------------------
-- PURPOSE: Return Accounts & AccountPointsCache data for account
-- 
--  PARAMS:
--      - INPUT:
--           @AccountId       BIGINT 
--
--      - OUTPUT:
--           AC_HOLDER_NAME 
--           AC_HOLDER_GENDER 
--           AC_HOLDER_BIRTH_DATE 
--           AC_BALANCE 
--           AC_POINTS 
--           AC_CURRENT_HOLDER_LEVEL 
--           AC_HOLDER_LEVEL_EXPIRATION 
--           AC_HOLDER_LEVEL_ENTERED 
--           AC_RE_BALANCE 
--           AC_PROMO_RE_BALANCE 
--           AC_PROMO_NR_BALANCE 
--           AC_CURRENT_PLAY_SESSION_ID 
--           AC_CURRENT_TERMINAL_NAME 
--           AM_POINTS_GENERATED 
--           AM_POINTS_DISCRETIONARIES 
--           AM_POINTS_PROMO_DISCRETIONARIES 
--           AM_POINTS_PROMO_ONLY_FOR_REDEEM 
--           APC_TODAY_LAST_MOVEMENT_ID 
-- RETURNS:
--
--   NOTES:
--------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetAccountPointsCache]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[GetAccountPointsCache]
GO

CREATE PROCEDURE [dbo].[GetAccountPointsCache]
       @pAccountId    bigint
AS
BEGIN
  
  EXEC AccountPointsCache_CalculateToday @pAccountId

  --Realizar select ACCOUNT_POINTS_CACHE con inner join a ACCOUNTS. -- select similar al que se hace en WKT_Player.cs funci�n Read(..)
  SELECT   ISNULL(AC_HOLDER_NAME, '')             AS AC_HOLDER_NAME 
         , ISNULL(AC_HOLDER_GENDER, 0)            AS AC_HOLDER_GENDER 
         , AC_HOLDER_BIRTH_DATE 
         , AC_BALANCE 
         , ISNULL(AC_POINTS, 0)                   AS AC_POINTS 
         , AC_HOLDER_LEVEL                        AS AC_CURRENT_HOLDER_LEVEL
         , AC_HOLDER_LEVEL_EXPIRATION 
         , AC_HOLDER_LEVEL_ENTERED 
         , AC_RE_BALANCE 
         , AC_PROMO_RE_BALANCE 
         , AC_PROMO_NR_BALANCE 
         , ISNULL (AC_CURRENT_PLAY_SESSION_ID, 0) AS AC_CURRENT_PLAY_SESSION_ID 
         , AC_CURRENT_TERMINAL_NAME 
         , ISNULL(APC_HISTORY_POINTS_GENERATED_FOR_LEVEL         , 0) + ISNULL(APC_TODAY_POINTS_GENERATED_FOR_LEVEL         , 0)  AS AM_POINTS_GENERATED 
         , ISNULL(APC_HISTORY_POINTS_DISCRETIONAL_FOR_LEVEL      , 0) + ISNULL(APC_TODAY_POINTS_DISCRETIONAL_FOR_LEVEL      , 0)  AS AM_POINTS_DISCRETIONARIES
         , ISNULL(APC_HISTORY_POINTS_DISCRETIONAL_ONLY_FOR_REDEEM, 0) + ISNULL(APC_TODAY_POINTS_DISCRETIONAL_ONLY_FOR_REDEEM, 0)  AS AM_POINTS_PROMO_DISCRETIONARIES 
         , ISNULL(APC_HISTORY_POINTS_PROMOTION_ONLY_FOR_REDEEM   , 0) + ISNULL(APC_TODAY_POINTS_PROMOTION_ONLY_FOR_REDEEM   , 0)  AS AM_POINTS_PROMO_ONLY_FOR_REDEEM
         , APC_TODAY_LAST_MOVEMENT_ID 
    FROM   ACCOUNTS 
   INNER   JOIN ACCOUNT_POINTS_CACHE ON APC_ACCOUNT_ID = AC_ACCOUNT_ID
   WHERE   AC_ACCOUNT_ID = @pAccountId; 
  
END  -- PROCEDURE [dbo].[GetAccountPointsCache]
GO

GRANT EXECUTE ON [dbo].[GetAccountPointsCache] TO [wggui] WITH GRANT OPTION
GO

/*
   PURPOSE: Returns if the system has to send alarm for client birthday
   
    PARAMS: @pAccountid
            @pAlarmType
   
    RETURN: DO_NOTHING=0, SEND_BIRTHDATE_ALARM = 1, SEND_BIRTHDATE_IS_NEAR=2
*/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[BirthDayAlarm]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[BirthDayAlarm]
GO

CREATE PROCEDURE [dbo].[BirthDayAlarm]
      @pAccountId BIGINT ,
      @pAlarmType INT OUTPUT
AS
BEGIN
      DECLARE @_birthday                  DATETIME
      DECLARE @_date_from_range           DATETIME
      DECLARE @_results_count             INT
      DECLARE @_today                     DATETIME
      DECLARE @_gp_days_to_remove         INT
      DECLARE @_birthday_alarm_code       INT
      DECLARE @_near_birthday_alarm_code  INT
      DECLARE @_holder_name               NVARCHAR(200)
      DECLARE @_is_leap_year              INT

      SET @pAlarmType = 0
      SET @_today = DATEADD(dd, DATEDIFF(dd, 0, getdate()), 0)
      SET @_birthday_alarm_code = 2097160
      SET @_near_birthday_alarm_code = 2097161

      SELECT   @_birthday = DATEADD(dd, DATEDIFF(dd, 0, AC_HOLDER_BIRTH_DATE ), 0),
               @_holder_name = ISNULL(AC_HOLDER_NAME,'')
        FROM   ACCOUNTS 
       WHERE   AC_ACCOUNT_ID = @pAccountId
       
      SET @_birthday = DATEADD(yy, DATEDIFF(yy, @_birthday, @_today), @_birthday)
       
      IF MONTH(@_birthday) = 2 AND DAY(@_birthday) = 29
      BEGIN
        SELECT @_is_leap_year = DATEPART(MM, DATEADD(DD, 1, CAST((CAST(YEAR(@_today) AS VARCHAR(4)) + '0228') AS DATETIME))) 
        
         IF @_is_leap_year = 0
           BEGIN
             SET @_birthday = DATEADD(dd,-1,@_birthday)
           END
      END
      
      IF @_birthday = @_today
      BEGIN
      /*SEARCH IF ALARMS EXISTS*/
        SELECT   @_results_count = COUNT(*) 
          FROM   ALARMS WITH (INDEX(IX_ALARMS_DATETIME_SEVERITY_SOURCE))
         WHERE   DATEADD(dd, 0, DATEDIFF(dd, 0, AL_DATETIME)) = @_birthday
           AND   AL_SEVERITY= 1
           AND   AL_SOURCE_ID = @pAccountId
           AND   AL_ALARM_CODE = @_birthday_alarm_code
        
        IF @_results_count = 0
        BEGIN
          SET @pAlarmType = 1
          
          RETURN
        END --IF @_results_count = 0
      END --IF @_birthday = @_today
      
      ELSE
      BEGIN
        SELECT   @_gp_days_to_remove = GP_KEY_VALUE
          FROM   GENERAL_PARAMS
         WHERE   GP_GROUP_KEY = 'Accounts' AND GP_SUBJECT_KEY = 'Player.BirthdayWarningDays'

        SET @_gp_days_to_remove = CAST(@_gp_days_to_remove AS INT)
        SET @_date_from_range = DATEADD(dd,-@_gp_days_to_remove,@_birthday)

        /*Today's date is within range*/

        IF  @_today >=  @_date_from_range
        AND @_today <  @_birthday
        
        BEGIN
      /*SEARCH IF ALARMS EXISTS*/
        SELECT   @_results_count= COUNT(*) 
          FROM   ALARMS WITH (INDEX(IX_ALARMS_DATETIME_SEVERITY_SOURCE))
         WHERE   AL_DATETIME >= @_date_from_range 
           AND   AL_DATETIME < @_birthday
           AND   AL_SEVERITY= 1
           AND   AL_SOURCE_ID=@pAccountId 
           AND   AL_ALARM_CODE=@_near_birthday_alarm_code
           
          IF @_results_count = 0
          BEGIN
            SET @pAlarmType=2
          END --IF @_results_count = 0
        END --IF  @_today >=  @_date_from_range
      END --ELSE
 END

GO

GRANT EXECUTE ON [dbo].[BirthDayAlarm] TO [wggui] WITH GRANT OPTION
GO

ALTER PROCEDURE [dbo].[WSP_AccountPendingHandpays]
  @AccountId  BigInt
AS
BEGIN
	SET NOCOUNT ON;

  DECLARE @TypeCancelledCredits AS Int
  DECLARE @TypeJackpot AS Int
  DECLARE @TypeSiteJackpot AS Int
  DECLARE @TypeOrphanCredits AS Int
  DECLARE @oldest_handpay  AS Datetime
  DECLARE @candidate_exists as bit

  DECLARE @TimePeriod as int
  DECLARE @oldest_session_start as Datetime
  DECLARE @newest_session_end   as Datetime
  DECLARE @count                as int
  DECLARE @now   as Datetime

  DECLARE @cph_terminal_id as int
  DECLARE @cph_datetime as Datetime
  DECLARE @cph_amount as Money
  DECLARE @expired as int

  --
  -- Handpay types
  --
  SET @TypeCancelledCredits = 0
  SET @TypeJackpot          = 1
  SET @TypeSiteJackpot      = 20
  SET @TypeOrphanCredits    = 2

  SET @expired              = 16384 -- 0x4000 Expired

  --
  -- Oldest Handpay 'Not expired'
  --
  SET @now = GETDATE()

  SELECT   @TimePeriod              = CAST (GP_KEY_VALUE AS INT)
    FROM   GENERAL_PARAMS
   WHERE   GP_GROUP_KEY             = 'Cashier.HandPays'
     AND   GP_SUBJECT_KEY           = 'TimePeriod'
     AND   ISNUMERIC (GP_KEY_VALUE) = 1 --- Ensure is a numeric value

  SET @TimePeriod = ISNULL (@TimePeriod, 60)      -- Default value: 1 hour
  IF ( @TimePeriod <   5 ) SET @TimePeriod =   5  -- Minimum: 5 minutes
  IF ( @TimePeriod > 2500 ) SET @TimePeriod = 2500  -- Maximum: 40 h x 60 min/h = 2500 min approximately

  SET @oldest_handpay = DATEADD (MINUTE, -@TimePeriod, @now)

  --
  -- Pending Handpays
  --
  SELECT   *
    INTO   #PENDING_HANDPAYS
    FROM   HANDPAYS 
   WHERE   HP_MOVEMENT_ID IS NULL
     AND   HP_DATETIME >= @oldest_handpay
     AND   HP_STATUS_CALCULATED <> @expired
     AND   ( HP_TYPE IN (@TypeCancelledCredits, @TypeJackpot, @TypeOrphanCredits)
        OR ( HP_TYPE = @TypeSiteJackpot AND HP_SITE_JACKPOT_AWARDED_TO_ACCOUNT_ID = @AccountId ) )

  ---
  --- Loop on pending Handpays (excluding 'SiteJackpot')
  ---
  DECLARE   cursor_pending_handpays CURSOR FOR
   SELECT   HP_TERMINAL_ID, HP_DATETIME, HP_AMOUNT
      FROM  #PENDING_HANDPAYS
    WHERE   HP_TYPE <> @TypeSiteJackpot
    
  OPEN cursor_pending_handpays
  FETCH NEXT FROM cursor_pending_handpays INTO @cph_terminal_id, @cph_datetime, @cph_amount

  WHILE @@Fetch_Status = 0
  BEGIN
    
    SET @oldest_session_start = DATEADD (DAY, -1, @cph_datetime)
    SET @newest_session_end   = DATEADD (MINUTE, @TimePeriod, @cph_datetime)

    SELECT   @count = COUNT(*)
      FROM   PLAY_SESSIONS
     WHERE   PS_TERMINAL_ID = @cph_terminal_id
       AND   PS_ACCOUNT_ID  = @AccountId
       AND   PS_STARTED     >= @oldest_session_start
       AND   PS_STARTED     <  @cph_datetime
       AND   PS_STARTED     < PS_FINISHED -- Exclude 'paid handpays' sessions 
       AND   PS_FINISHED    >= @oldest_session_start
       AND   PS_FINISHED    <  @newest_session_end

    IF ( @count = 0 )
    BEGIN
      ---
      --- Not a candidate: delete it!
      ---
      DELETE   #PENDING_HANDPAYS
       WHERE   HP_TERMINAL_ID = @cph_terminal_id
         AND   HP_DATETIME    = @cph_datetime
         AND   HP_AMOUNT      = @cph_amount
    END

    --
    -- Next
    --
    FETCH NEXT FROM cursor_pending_handpays INTO @cph_terminal_id, @cph_datetime, @cph_amount

  END

  CLOSE cursor_pending_handpays
  DEALLOCATE cursor_pending_handpays

  SELECT   HP_TERMINAL_ID 
         , HP_DATETIME 
         , HP_TE_PROVIDER_ID 
         , HP_TE_NAME 
         , HP_TYPE 
         , ' '  HP_DUMMY
         , ISNULL (HP_SITE_JACKPOT_NAME, ' ')  HP_SITE_JACKPOT_NAME
         , HP_AMOUNT 
         , ISNULL (HP_SITE_JACKPOT_AWARDED_ON_TERMINAL_ID, 0)  HP_SITE_JACKPOT_AWARDED_ON_TERMINAL_ID
    FROM   #PENDING_HANDPAYS
   ORDER BY HP_DATETIME DESC

  DROP TABLE #PENDING_HANDPAYS

END -- WSP_AccountPendingHandpays
GO
