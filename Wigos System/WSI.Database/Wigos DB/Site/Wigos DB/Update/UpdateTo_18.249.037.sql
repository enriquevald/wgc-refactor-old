/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 248;

SET @New_ReleaseId = 249;
SET @New_ScriptName = N'UpdateTo_18.249.037.sql';
SET @New_Description = N'Modified 3GS Stored procedures and new GP.';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/******* TABLES *******/

/******* INDEXES *******/

/******* RECORDS *******/

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'MobileBank' AND GP_SUBJECT_KEY = 'Deposit.ShowDenominations')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('MobileBank','Deposit.ShowDenominations','0')
GO

/******* STORED PROCEDURES *******/

--------------------------------------------------------------------------------
-- PURPOSE: Start play session
--
--  PARAMS:
--      - INPUT:
--         @AccountID        varchar(24)
--         @VendorId        varchar(16)
--         @SerialNumber    varchar(30)
--         @MachineNumber   int
--         @CurrentJackpot  money = 0
--         @VendorSessionID bigint = 0
--
--      - OUTPUT:
--
-- RETURNS:
--      StatusCode
--      StatusText
--      SessionID
--      Balance
--
--   NOTES:
--
ALTER PROCEDURE [dbo].[zsp_SessionStart]
  @AccountID       varchar(24),
  @VendorId        varchar(16),
  @SerialNumber    varchar(30),
  @MachineNumber   int,
  @CurrentJackpot  money = 0,
  @VendorSessionID bigint = 0
WITH EXECUTE AS OWNER
AS
BEGIN
  DECLARE @session_id   bigint
  DECLARE @balance      money
  DECLARE @status_code  int
  DECLARE @status_text  varchar (254)
  DECLARE @error_text   nvarchar (MAX)
  DECLARE @input        nvarchar(MAX)
  DECLARE @output       nvarchar(MAX)
  DECLARE @_try         int
  DECLARE @_max_tries   int
  DECLARE @_exception   bit
  DECLARE @_completed   bit

  SET @_try       = 0
  SET @_max_tries = 6		-- AJQ 22-DES-2014, The number of retries has been incremented to 6
  SET @_completed = 0

  BEGIN TRANSACTION

  WHILE (@_completed = 0)
  BEGIN

    SET @_exception   = 0
    SET @status_code  = 4
    SET @status_text  = 'Access Denied'
    SET @error_text   = ''
    SET @session_id   = 0
    SET @balance      = 0

    BEGIN TRY

      SET @_try = @_try + 1

      EXECUTE dbo.Trx_3GS_StartCardSession @VendorId, @SerialNumber, @MachineNumber,
                         @AccountID,
                         @session_id OUTPUT, @balance OUTPUT,
                         @status_code OUTPUT, @status_text OUTPUT, @error_text OUTPUT
      SET @_completed = 1

    END TRY

    BEGIN CATCH
    
      ROLLBACK TRANSACTION

      SET @session_id = 0
      SET @balance    = 0
  
      IF (@_try >= @_max_tries) 
      BEGIN
        SET @_completed  = 1;
        SET @_exception  = 1;
        SET @status_code = 4;
        SET @status_text = 'Access Denied';
        SET @error_text  = ' ERROR_NUMBER: '    + CAST(ERROR_NUMBER()               AS NVARCHAR)
                         + ' ERROR_SEVERITY: '  + CAST(ERROR_SEVERITY()             AS NVARCHAR)
                         + ' ERROR_STATE: '     + CAST(ERROR_STATE()                AS NVARCHAR)
                         + ' ERROR_PROCEDURE: ' + CAST(ISNULL(ERROR_PROCEDURE(),'') AS NVARCHAR)
                         + ' ERROR_LINE: '      + CAST(ERROR_LINE()                 AS NVARCHAR)
                         + ' ERROR_MESSAGE: '   + CAST(ERROR_MESSAGE()              AS NVARCHAR(MAX))
      END
      ELSE
      BEGIN
        WAITFOR DELAY '00:00:02'
      END

      BEGIN TRANSACTION

    END CATCH
  END

  SET @input = '@AccountID='       + @AccountID
             +';@VendorId='        + @VendorId
             +';@SerialNumber='    + CAST (@SerialNumber    AS NVARCHAR)
             +';@MachineNumber='   + CAST (@MachineNumber   AS NVARCHAR)
             +';@CurrentJackpot='  + CAST (@CurrentJackpot  AS NVARCHAR)
             +';@VendorSessionID=' + CAST (@VendorSessionID AS NVARCHAR)

  IF @error_text <> ''
    SET @error_text = ';Details='      + @error_text

  SET @output = 'StatusCode='      + CAST (@status_code     AS NVARCHAR)
              +';StatusText='      + @status_text
              +';SessionID='       + CAST(@session_id       AS NVARCHAR)
              +';Balance='         + CAST(@balance          AS NVARCHAR)
              + @error_text
              + '; TryIndex='      + CAST (@_try AS NVARCHAR)

  EXECUTE dbo.zsp_Audit 'zsp_SessionStart', @AccountID, @VendorId, @SerialNumber, @MachineNumber,
                                            @session_id, @status_code, @balance, 1,
                                            @input, @output

  COMMIT TRANSACTION

  SELECT @status_code AS StatusCode, @status_text AS StatusText, @session_id AS SessionId, @balance AS Balance

END -- zsp_SessionStart

GO

--------------------------------------------------------------------------------
-- PURPOSE: Update play session
--
--  PARAMS:
--      - INPUT:
--          @AccountId      varchar(24)
--          @VendorId       varchar(16)
--          @SerialNumber   varchar(30)
--          @MachineNumber  int
--          @SessionId      bigint
--          @AmountPlayed   money
--          @AmountWon      money
--          @GamesPlayed    int
--          @GamesWon       int
--          @CreditBalance  money,
--          @CurrentJackpot money = 0
--
--      - OUTPUT:
--
-- RETURNS:
--      StatusCode
--      StatusText
--
--   NOTES:
--
 ALTER PROCEDURE [dbo].[zsp_SessionUpdate]
  @AccountId      varchar(24),
  @VendorId       varchar(16),
  @SerialNumber   varchar(30),
  @MachineNumber  int,
  @SessionId      bigint,
  @AmountPlayed   money,
  @AmountWon      money,
  @GamesPlayed    int,
  @GamesWon       int,
  @CreditBalance  money,
  @CurrentJackpot money = 0
WITH EXECUTE AS OWNER
AS
BEGIN
  DECLARE @update_freq  int
  DECLARE @status_code  int
  DECLARE @status_text  varchar (254)
  DECLARE @error_text   nvarchar (MAX)
  DECLARE @input        nvarchar(MAX)
  DECLARE @output       nvarchar(MAX)
  DECLARE @_try         int
  DECLARE @_max_tries   int
  DECLARE @_exception   bit
  DECLARE @_completed   bit

  SET @update_freq = 30

  -------- For Cadillac store all session updates received
  ------IF (@VendorId like '%CADILLAC%')
  ------    SET @update_freq = 1

  -- AJQ & XI 12-12-2013, Limit the number of updates 
  IF ( @GamesPlayed % @update_freq <> 0 ) 
  BEGIN
    -- When the provider calls every:
    -- 1 play --> 1 / 30 -->  3.33% trx --> 3.33% plays
    -- 2 play --> 1 / 15 -->  6.67% trx --> 3.33% plays
    -- 3 play --> 1 / 10 --> 10.00% trx --> 3.33% plays
    -- 4 play --> 1 / 60 -->  1.67% trx --> 0.42% plays
    -- 5 play --> 1 /  6 --> 16.67% trx --> 3.33% plays
    SELECT CAST (0 AS INT) AS StatusCode, CAST ('Success' AS varchar (254)) AS StatusText
    RETURN
  END

  SET @_try       = 0
  SET @_max_tries = 3
  SET @_completed = 0

  BEGIN TRANSACTION

  WHILE (@_completed = 0)
  BEGIN

    SET @_exception   = 0
    SET @status_code  = 4
    SET @status_text  = 'Access Denied'
    SET @error_text   = ''

    BEGIN TRY

      SET @_try = @_try + 1
    
      EXECUTE dbo.Trx_3GS_UpdateCardSession @VendorId, @SerialNumber, @MachineNumber,
                                            @AccountID,
                                            @SessionId,
                                            @AmountPlayed, @AmountWon, @GamesPlayed, @GamesWon,
                                            @CreditBalance,
                                            @status_code OUTPUT, @status_text OUTPUT, @error_text OUTPUT

      SET @_completed = 1

    END TRY

    BEGIN CATCH
    
      ROLLBACK TRANSACTION

      IF (@_try >= @_max_tries) 
      BEGIN
        SET @_completed  = 1;
        SET @_exception  = 1;
        SET @status_code = 4;
        SET @status_text = 'Access Denied';
        SET @error_text  = ' ERROR_NUMBER: '    + CAST(ERROR_NUMBER()               AS NVARCHAR)
                         + ' ERROR_SEVERITY: '  + CAST(ERROR_SEVERITY()             AS NVARCHAR)
                         + ' ERROR_STATE: '     + CAST(ERROR_STATE()                AS NVARCHAR)
                         + ' ERROR_PROCEDURE: ' + CAST(ISNULL(ERROR_PROCEDURE(),'') AS NVARCHAR)
                         + ' ERROR_LINE: '      + CAST(ERROR_LINE()                 AS NVARCHAR)
                         + ' ERROR_MESSAGE: '   + CAST(ERROR_MESSAGE()              AS NVARCHAR(MAX))
      END
      ELSE
      BEGIN
        WAITFOR DELAY '00:00:02'
      END

      BEGIN TRANSACTION

    END CATCH
  END

  SET @input = '@AccountID='      + @AccountID
             +';@VendorId='       + @VendorId
             +';@SerialNumber='   + CAST (@SerialNumber   AS NVARCHAR)
             +';@MachineNumber='  + CAST (@MachineNumber  AS NVARCHAR)
             +';@SessionId='      + CAST (@SessionId      AS NVARCHAR)
             +';@AmountPlayed='   + CAST (@AmountPlayed   AS NVARCHAR)
             +';@AmountWon='      + CAST (@AmountWon      AS NVARCHAR)
             +';@GamesPlayed='    + CAST (@GamesPlayed    AS NVARCHAR)
             +';@GamesWon='       + CAST (@GamesWon       AS NVARCHAR)
             +';@CreditBalance='  + CAST (@CreditBalance  AS NVARCHAR)
             +';@CurrentJackpot=' + CAST (@CurrentJackpot AS NVARCHAR)

  IF @error_text <> ''
    SET @error_text = ';Details='     + @error_text

  SET @output = 'StatusCode='     + CAST (@status_code    AS NVARCHAR)
              +';StatusText='     + @status_text
              + @error_text
              + '; TryIndex='     + CAST (@_try AS NVARCHAR)

  EXECUTE dbo.zsp_Audit 'zsp_SessionUpdate', @AccountID, @VendorId, @SerialNumber, @MachineNumber,
                                             @SessionId, @status_code, @CreditBalance, 1,
                                             @input, @output

  COMMIT TRANSACTION

  -- AJQ 19-DES-2014, When an exception occurred we will return 0-"Success" to the caller.
  IF (@_exception = 1)
    SET  @status_code = 0
    
  SELECT @status_code AS StatusCode, @status_text AS StatusText

END -- zsp_SessionUpdate

GO

--------------------------------------------------------------------------------
-- PURPOSE: End play session
--
--  PARAMS:
--      - INPUT:
--          @AccountId      varchar(24)
--          @VendorId       varchar(16)
--          @SerialNumber   varchar(30)
--          @MachineNumber  int
--          @SessionId      bigint
--          @AmountPlayed   money
--          @AmountWon      money
--          @GamesPlayed    int
--          @GamesWon       int
--          @CreditBalance  money,
--          @CurrentJackpot money = 0
--
--      - OUTPUT:
--
-- RETURNS:
--      StatusCode
--      StatusText
--
--   NOTES:
--
ALTER PROCEDURE [dbo].[zsp_SessionEnd]
  @AccountId      varchar(24),
  @VendorId       varchar(16),
  @SerialNumber   varchar(30),
  @MachineNumber  int,
  @SessionId      bigint,
  @AmountPlayed   money,
  @AmountWon      money,
  @GamesPlayed    int,
  @GamesWon       int,
  @CreditBalance  money,
  @CurrentJackpot money = 0
WITH EXECUTE AS OWNER
AS
BEGIN

  DECLARE @status_code  int
  DECLARE @status_text  varchar (254)
  DECLARE @error_text   nvarchar (MAX)
  DECLARE @input        nvarchar(MAX)
  DECLARE @output       nvarchar(MAX)
  DECLARE @_try         int
  DECLARE @_max_tries   int
  DECLARE @_exception   bit
  DECLARE @_completed   bit

  SET @_try       = 0
  SET @_max_tries = 6		-- AJQ 19-DES-2014, The number of retries has been incremented to 6 (retries seem to work so we try one more time)
  SET @_completed = 0

  BEGIN TRANSACTION

  WHILE (@_completed = 0)
  BEGIN

    SET @_exception   = 0
    SET @status_code  = 4
    SET @status_text  = 'Access Denied'
    SET @error_text   = ''

    BEGIN TRY

      SET @_try = @_try + 1
      
      EXECUTE dbo.Trx_3GS_EndCardSession @VendorId, @SerialNumber, @MachineNumber,
                         @AccountID,
                         @SessionId,
                         @AmountPlayed, @AmountWon, @GamesPlayed, @GamesWon,
                         @CreditBalance,
                         @status_code OUTPUT, @status_text OUTPUT, @error_text OUTPUT
      SET @_completed = 1

    END TRY

    BEGIN CATCH
    
      ROLLBACK TRANSACTION

      IF (@_try >= @_max_tries) 
      BEGIN
        SET @_completed  = 1;
        SET @_exception  = 1;
        SET @status_code = 4;
        SET @status_text = 'Access Denied';
        SET @error_text  = ' ERROR_NUMBER: '    + CAST(ERROR_NUMBER()               AS NVARCHAR)
                         + ' ERROR_SEVERITY: '  + CAST(ERROR_SEVERITY()             AS NVARCHAR)
                         + ' ERROR_STATE: '     + CAST(ERROR_STATE()                AS NVARCHAR)
                         + ' ERROR_PROCEDURE: ' + CAST(ISNULL(ERROR_PROCEDURE(),'') AS NVARCHAR)
                         + ' ERROR_LINE: '      + CAST(ERROR_LINE()                 AS NVARCHAR)
                         + ' ERROR_MESSAGE: '   + CAST(ERROR_MESSAGE()              AS NVARCHAR(MAX))
      END
      ELSE
      BEGIN
        WAITFOR DELAY '00:00:02'
      END

      BEGIN TRANSACTION

    END CATCH
  END -- WHILE
  
  SET @input = '@AccountID='      + @AccountID
             +';@VendorId='       + @VendorId
             +';@SerialNumber='   + CAST (@SerialNumber   AS NVARCHAR)
             +';@MachineNumber='  + CAST (@MachineNumber  AS NVARCHAR)
             +';@SessionId='      + CAST (@SessionId      AS NVARCHAR)
             +';@AmountPlayed='   + CAST (@AmountPlayed   AS NVARCHAR)
             +';@AmountWon='      + CAST (@AmountWon      AS NVARCHAR)
             +';@GamesPlayed='    + CAST (@GamesPlayed    AS NVARCHAR)
             +';@GamesWon='       + CAST (@GamesWon       AS NVARCHAR)
             +';@CreditBalance='  + CAST (@CreditBalance  AS NVARCHAR)
             +';@CurrentJackpot=' + CAST (@CurrentJackpot AS NVARCHAR)

  IF @error_text <> ''
    SET @error_text = ';Details='     + @error_text

  SET @output = 'StatusCode='     + CAST (@status_code    AS NVARCHAR)
              +';StatusText='     + @status_text
              + @error_text
              + '; TryIndex='     + CAST (@_try AS NVARCHAR) 
  
  EXECUTE dbo.zsp_Audit 'zsp_SessionEnd', @AccountID, @VendorId, @SerialNumber, @MachineNumber,
                                          @SessionId, @status_code, @CreditBalance, 1,
                                          @input, @output

  COMMIT TRANSACTION

  -- AJQ 21-AUG-2014, Always return 0 "Success" to the caller.
  --                  This is to avoid retries from the client side.
  --                  When an exception has occurred, we will return status=4
  IF (@_exception = 0)
    SET  @status_code = 0

  SELECT @status_code AS StatusCode, @status_text AS StatusText

END -- zsp_SessionEnd

GO
