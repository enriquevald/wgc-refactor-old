/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 232;

SET @New_ReleaseId = 233;
SET @New_ScriptName = N'UpdateTo_18.233.037.sql';
SET @New_Description = N'Added table bank_transactions and PK handpays';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/******* TABLES *******/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[bank_transactions]') AND type in (N'U'))
	DROP TABLE [dbo].[bank_transactions]
GO

CREATE TABLE [dbo].[bank_transactions](
      [bt_operation_id] [bigint] NOT NULL,
      [bt_type] [int] NOT NULL,
      [bt_document_number] [nvarchar](50) NOT NULL,
      [bt_bank_name] [nvarchar](50) NULL,
      [bt_bank_country] [int] NULL,
      [bt_holder_name] [nvarchar](200) NULL,
      [bt_card_expiration_date] [nvarchar](5) NULL,
      [bt_card_track_data] [varchar](256) NULL,
      [bt_amount] [money] NOT NULL,
      [bt_iso_code] [nvarchar](3) NOT NULL,
      [bt_user_id] [int] NOT NULL,
      [bt_cashier_id] [int] NOT NULL,
      [bt_user_name] [nvarchar](50) NOT NULL,
      [bt_cashier_name] [nvarchar](50) NOT NULL,
      [bt_transaction_type] [int] NOT NULL,
      [bt_edited] [bit] NOT NULL,
      [bt_check_routing_number] [nvarchar](50) NULL,
      [bt_check_account_number] [nvarchar](50) NULL,
      [bt_check_date] [datetime] NULL,
      [bt_comments] [nvarchar](256) NULL,
      [bt_account_track_data] [nvarchar](50) NULL,
      [bt_account_holder_name] [nvarchar](200) NULL
CONSTRAINT [PK_bank_transactions] PRIMARY KEY CLUSTERED 
(
      [bt_operation_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE name = N'PK_handpays' AND type = 'PK')
	ALTER TABLE dbo.handpays ADD CONSTRAINT
		PK_handpays PRIMARY KEY CLUSTERED 
		(
		hp_id
		) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
		
/******* INDEXES *******/

/******* RECORDS *******/

/******* STORED PROCEDURES *******/
