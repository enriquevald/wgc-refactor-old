/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 279;

SET @New_ReleaseId = 280;
SET @New_ScriptName = N'UpdateTo_18.280.038.sql';
SET @New_Description = N'New GP; Update Procedures; New indexes';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/******* TABLES *******/

/******* INDEXES *******/

--DELETE INDEX
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[tickets]') AND name = N'IX_ti_collected_money_collection_id')
BEGIN
  DROP INDEX IX_ti_collected_money_collection_id ON TICKETS
END
GO

--CREATE INDEX 1
IF  NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[tickets]') AND name = N'IX_ti_money_collection_id')
BEGIN
  CREATE NONCLUSTERED INDEX [IX_ti_money_collection_id] ON [dbo].TICKETS 
  (
      ti_money_collection_id ASC
  )WITH (STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
END
GO

--CREATE INDEX 2
IF  NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[tickets]') AND name = N'IX_ti_collected_money_collection')
BEGIN
  CREATE NONCLUSTERED INDEX [IX_ti_collected_money_collection] ON [dbo].TICKETS 
  (
      ti_collected_money_collection ASC
  )WITH (STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
END
GO

/******* RECORDS *******/

/* TITO - Payment.GracePeriod.Mode
    0: Disabled
    1: Relative mode (Days)
    2: Absolute mode (Date)
*/           
IF  NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'TITO' AND GP_SUBJECT_KEY ='Payment.GracePeriod.Mode')
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('TITO','Payment.GracePeriod.Mode','0');
GO

-- TITO - Payment.GracePeriod.Days
IF  NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'TITO' AND GP_SUBJECT_KEY ='Payment.GracePeriod.Days')
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('TITO','Payment.GracePeriod.Days','');
GO

/*
-- TITO - Payment.GracePeriod.ExclusiveDateMMDD
  Format: 0415
  Only works if Payment.GracePerior.Mode is mode 2
*/
IF  NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'TITO' AND GP_SUBJECT_KEY ='Payment.GracePeriod.ExclusiveDateMMDD')
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('TITO','Payment.GracePeriod.ExclusiveDateMMDD','');
GO    

IF  NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'OrphanCredits' AND GP_SUBJECT_KEY ='AutomaticPayment')
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('OrphanCredits','AutomaticPayment','1');
GO    

/******* PROCEDURES *******/
--------------------------------------------------------------------------------
-- Copyright � 2014 Win Systems International
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: ReportShortfallExcess.sql
-- 
--   DESCRIPTION: Procedure for Cash ShortFall and Excess Report 
-- 
--        AUTHOR: Fernando Jim�nez
-- 
-- CREATION DATE: 04-OCT-2014
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 04-OCT-2014 FJC    First release.
-- 21-NOV-2014 FJC    Changes in Mobile bank query.
-- 24-NOV-2014 FJC    Changes in Mobile bank query.
-- 16-JUN-2015 AMF    Search by gaming day
--------------------------------------------------------------------------------


--------------------------------------------------------------------------------
-- PURPOSE: Selects cash shortfall and excess report.
-- 
--  PARAMS:
--      - INPUT:
--   @pDateFrom			 DATETIME     
--   @pDateTo				 DATETIME      
--   @pGamingDayMode BIT
--
--      - OUTPUT:
--
-- RETURNS:
--                  
--
--   NOTES:
--

ALTER PROCEDURE [dbo].[ReportShortfallExcess]
  @pDateFrom DATETIME,  
  @pDateTo   DATETIME,
	@pGamingDayMode BIT
    
AS 
BEGIN
      --GUI USERS
		SELECT   GUI_USERS.GU_USER_ID        as  USERID 
					 , 0                           as  TYPE_USER
					 , GUI_USERS.GU_USERNAME       as  USERNAME 
					 , GUI_USERS.GU_FULL_NAME      as  FULLNAME 
					 , GUI_USERS.GU_EMPLOYEE_CODE  as  EMPLOYEECODE 
					 , X.CM_CURRENCY_ISO_CODE      as  CURRENCY 
					 , X.FALTANTE                  as  FALTANTE 
					 , X.SOBRANTE                  as  SOBRANTE 
					 , (X.SOBRANTE - X.FALTANTE)   as  DIFERENCIA
      FROM
      (        
      SELECT CS_USER_ID
             , CM_CURRENCY_ISO_CODE
             , SUM (CASE WHEN CM_TYPE = 158 THEN CM_ADD_AMOUNT ELSE 0 END) as FALTANTE
             , SUM (CASE WHEN CM_TYPE = 159 THEN CM_ADD_AMOUNT ELSE 0 END) as SOBRANTE             
        FROM CASHIER_MOVEMENTS  WITH (INDEX (IX_CM_DATE_TYPE))
				INNER JOIN CASHIER_SESSIONS ON  CM_SESSION_ID  = CS_SESSION_ID				
       WHERE CASE WHEN @pGamingDayMode = 1 THEN CS_GAMING_DAY ELSE CM_DATE END >= @pDateFrom  
         AND CASE WHEN @pGamingDayMode = 1 THEN CS_GAMING_DAY ELSE CM_DATE END <= @pDateTo
         AND CM_TYPE    IN (158, 159)     
			GROUP BY CS_USER_ID
							 , CM_CURRENCY_ISO_CODE
    ) X
         
  INNER JOIN GUI_USERS
          ON GU_USER_ID = X.CS_USER_ID
         AND (X.FALTANTE<>0 OR X.SOBRANTE<>0)
         AND GU_USER_TYPE=0

    UNION ALL
    
    --BANK MOBILE USERS (this kind of users have national currency only)
    SELECT   MOBILE_BANKS.MB_ACCOUNT_ID                       as  USERID 
           , 1                                                as  TYPE_USER 
           , 'MB-' + CAST(MOBILE_BANKS.mb_account_id as CHAR) as  USERNAME
           , MOBILE_BANKS.MB_HOLDER_NAME                      as  FULLNAME
           , ''                                               as  EMPLOYEECODE
           , ''                                               as  CURRENCY
           , X2.FALTANTE                                      as  FALTANTE
           , X2.SOBRANTE                                      as  SOBRANTE 
           , (X2.SOBRANTE - X2.FALTANTE)                      as  DIFERENCIA
     FROM
     (
          
        SELECT   MBM_MB_ID
                 , SUM(CASE WHEN  MBM_TYPE IN (8,11)  THEN MBM_SUB_AMOUNT ELSE 0 END) AS FALTANTE
                 , SUM(CASE WHEN  MBM_TYPE = 9        THEN MBM_ADD_AMOUNT ELSE 0 END) AS SOBRANTE
            FROM   MB_MOVEMENTS MB1 WITH (INDEX (IX_MBM_DATETIME_TYPE))    
           WHERE   
                  (MBM_DATETIME  >= @pDateFrom  
             AND   MBM_DATETIME  <= @pDateTo)
             AND  ( ( MBM_TYPE IN (8,9) ) 
              OR    ( MBM_TYPE IN (11) 
                     
                     AND  NOT EXISTS 
                        (SELECT   MB2.MBM_MB_ID
                           FROM   MB_MOVEMENTS MB2 
                          WHERE   MB2.MBM_CASHIER_SESSION_ID = MB1.MBM_CASHIER_SESSION_ID 
                            AND   MB2.MBM_DATETIME > MB1.MBM_DATETIME 
                            AND   MB2.MBM_DATETIME <= @pDateTo
                            AND   MB2.MBM_TYPE IN (3) ) ) ) 
        GROUP BY   MBM_MB_ID
        
     ) X2
    
      INNER JOIN   MOBILE_BANKS
              ON   X2.MBM_MB_ID = MOBILE_BANKS.MB_ACCOUNT_ID
             AND   (X2.FALTANTE<>0 OR X2.SOBRANTE<>0)
      
        ORDER BY   USERNAME
END
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetGameIdForTerminal]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetGameIdForTerminal]
GO

CREATE PROCEDURE [dbo].[GetGameIdForTerminal]
      @pFromDate                  DATETIME 
,     @pToDate                    DATETIME
,     @pTerminalId                INT

AS
BEGIN 

  DECLARE @pGameId AS INT

  -- Get game id from sales per hour
  SELECT   @pGameId = ISNULL(MIN(SPH_GAME_ID), 0)
    FROM   SALES_PER_HOUR
   WHERE   SPH_BASE_HOUR >= @pFromDate
     AND   SPH_BASE_HOUR < @pToDate
     AND   SPH_TERMINAL_ID = @pTerminalId
     
  IF @pGameId = 0
  BEGIN
    -- Get game id from terminals
    SELECT   @pGameId = ISNULL(TE_LAST_GAME_PLAYED_ID, 0)
      FROM   TERMINALS
     WHERE   TE_TERMINAL_ID = @pTerminalId
  END

  IF @pGameId = 0
  BEGIN
    -- Get game id from terminal game translation
    SELECT   @pGameId = ISNULL(TGT_SOURCE_GAME_ID, 0)
      FROM   TERMINAL_GAME_TRANSLATION
     WHERE   TGT_TERMINAL_ID = @pTerminalId
  END
    
  IF @pGameId = 0
  BEGIN
    RAISERROR (N'Terminal without activity can not be adjusted', 11, 10)
  END
  ELSE
  BEGIN
   SELECT @pGameId
  END
END
GO

GRANT EXECUTE ON [dbo].[GetGameIdForTerminal] TO [wggui] WITH GRANT OPTION
GO
      
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Update_TerminalSasMeterHistory]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Update_TerminalSasMeterHistory]
GO

CREATE PROCEDURE [dbo].[Update_TerminalSasMeterHistory]
    @pTerminaId       Int
  , @pMeterCode       Int
  , @pGameId          Int
  , @pDenomination    Money
  , @pType            Int
  , @pDateTime        DateTime
  , @pMeterIniValue   BigInt
  , @pMeterFinValue   BigInt
  , @pMeterIncrement  BigInt
AS
BEGIN

  IF NOT EXISTS (SELECT 1 
                   FROM TERMINAL_SAS_METERS_HISTORY 
                  WHERE TSMH_TERMINAL_ID  = @pTerminaId
                    AND TSMH_METER_CODE   = @pMeterCode
                    AND TSMH_GAME_ID      = @pGameId
                    AND TSMH_DENOMINATION = @pDenomination
                    AND TSMH_TYPE         = @pType
                    AND TSMH_DATETIME     = @pDateTime )
  BEGIN
    INSERT INTO TERMINAL_SAS_METERS_HISTORY
              ( TSMH_TERMINAL_ID  
              , TSMH_METER_CODE  
              , TSMH_GAME_ID  
              , TSMH_DENOMINATION  
              , TSMH_TYPE  
              , TSMH_DATETIME  
              , TSMH_METER_INI_VALUE  
              , TSMH_METER_FIN_VALUE  
              , TSMH_METER_INCREMENT  
              , TSMH_RAW_METER_INCREMENT  
              , TSMH_LAST_REPORTED  )
       VALUES
             (  @pTerminaId     
              , @pMeterCode     
              , @pGameId        
              , @pDenomination  
              , @pType          
              , @pDateTime      
              , @pMeterIniValue 
              , @pMeterFinValue 
              , @pMeterIncrement
              , 0
              , NULL)
  END  
  ELSE
  BEGIN

    UPDATE   TERMINAL_SAS_METERS_HISTORY
       SET   TSMH_METER_INI_VALUE = @pMeterIniValue
           , TSMH_METER_FIN_VALUE = @pMeterFinValue
           , TSMH_METER_INCREMENT = @pMeterIncrement
     WHERE   TSMH_TERMINAL_ID = @pTerminaId
       AND   TSMH_METER_CODE = @pMeterCode
       AND   TSMH_GAME_ID = @pGameId
       AND   TSMH_DENOMINATION = @pDenomination
       AND   TSMH_TYPE = @pType
       AND   TSMH_DATETIME = @pDateTime
  END
END
GO

GRANT EXECUTE ON [dbo].[Update_TerminalSasMeterHistory] TO [wggui] WITH GRANT OPTION;
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Update_Meters_MachineStatsPerHour]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Update_Meters_MachineStatsPerHour]
GO

CREATE PROCEDURE  [dbo].[Update_Meters_MachineStatsPerHour]
      @pBaseHour                  DATETIME 
,     @pTerminalId                 INT
,     @pMSH_Played_Count          BIGINT = NULL
,     @pMSH_Played_Amount         MONEY = NULL
,     @pMSH_Won_Count             BIGINT = NULL
,     @pMSH_Won_Amount            MONEY = NULL
,     @pMSH_Jackpot_Amount        MONEY = NULL
,     @pMSH_To_GM_Amount          MONEY = NULL
,     @pMSH_From_GM_Amount        MONEY = NULL
,     @pMSH_HPC_Handpays_Amount   MONEY = NULL

AS
BEGIN 

DECLARE @pTerminalName AS NVARCHAR(50)
DECLARE @pGameName AS NVARCHAR(50)

IF NOT EXISTS ( SELECT   *
                  FROM   MACHINE_STATS_PER_HOUR
                 WHERE   MSH_BASE_HOUR = @pBaseHour
                   AND   MSH_TERMINAL_ID = @pTerminalId)
BEGIN

  INSERT INTO MACHINE_STATS_PER_HOUR
             ( MSH_BASE_HOUR
             , MSH_TERMINAL_ID
             , MSH_PLAYED_COUNT
             , MSH_PLAYED_AMOUNT
             , MSH_WON_COUNT
             , MSH_WON_AMOUNT
             , MSH_TO_GM_COUNT
             , MSH_TO_GM_AMOUNT
             , MSH_FROM_GM_COUNT
             , MSH_FROM_GM_AMOUNT
             , MSH_JACKPOT_AMOUNT
             , MSH_HPC_HANDPAYS_AMOUNT)
       VALUES
             ( @pBaseHour
             , @pTerminalId
             , ISNULL(@pMSH_Played_Count, 0)
             , ISNULL(@pMSH_Played_Amount, 0)
             , ISNULL(@pMSH_Won_Count, 0)
             , ISNULL(@pMSH_Won_Amount, 0)
             , 0
             , ISNULL(@pMSH_To_GM_Amount, 0)
             , 0
             , ISNULL(@pMSH_From_GM_Amount, 0)
             , ISNULL(@pMSH_Jackpot_Amount, 0)
             , ISNULL(@pMSH_HPC_Handpays_Amount, 0))

END
ELSE
BEGIN

  UPDATE   MACHINE_STATS_PER_HOUR
     SET   MSH_PLAYED_COUNT = ISNULL(@pMSH_Played_Count, MSH_PLAYED_COUNT)
         , MSH_PLAYED_AMOUNT = ISNULL(@pMSH_Played_Amount, MSH_PLAYED_AMOUNT)
         , MSH_WON_COUNT = ISNULL(@pMSH_Won_Count, MSH_WON_COUNT)
         , MSH_WON_AMOUNT = ISNULL(@pMSH_Won_Amount, MSH_WON_AMOUNT)
         , MSH_TO_GM_AMOUNT = ISNULL(@pMSH_To_GM_Amount, MSH_TO_GM_AMOUNT)
         , MSH_FROM_GM_AMOUNT = ISNULL(@pMSH_From_GM_Amount, MSH_FROM_GM_AMOUNT)
         , MSH_JACKPOT_AMOUNT  = ISNULL(@pMSH_Jackpot_Amount, MSH_JACKPOT_AMOUNT)
         , MSH_HPC_HANDPAYS_AMOUNT = ISNULL(@pMSH_HPC_Handpays_Amount, MSH_HPC_HANDPAYS_AMOUNT)
   WHERE   MSH_BASE_HOUR = @pBaseHour
     AND   MSH_TERMINAL_ID = @pTerminalId

END
            
END 
GO

GRANT EXECUTE ON [dbo].[Update_Meters_MachineStatsPerHour] TO [wggui] WITH GRANT OPTION
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Update_Meters_SalesPerHour]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Update_Meters_SalesPerHour]
GO

CREATE PROCEDURE [dbo].[Update_Meters_SalesPerHour]
      @pBaseHour                  DATETIME 
,     @pTerminalId                 INT
,     @pGameId                    INT
,     @pSPH_Played_Count          BIGINT
,     @pSPH_Played_Amount         MONEY
,     @pSPH_Won_Count             BIGINT
,     @pSPH_Won_Amount            MONEY
,     @pSPH_Jackpot_Amount        MONEY

AS
BEGIN 

  DECLARE @pTerminalName AS NVARCHAR(50)
  DECLARE @pGameName AS NVARCHAR(50)

  IF @pGameId = 0
  BEGIN
    -- Get game id from sales per hour
    SELECT   @pGameId = ISNULL(MIN(SPH_GAME_ID), 0)
      FROM   SALES_PER_HOUR
     WHERE   SPH_BASE_HOUR >= dbo.Opening(0, @pBaseHour)
       AND   SPH_BASE_HOUR < DATEADD(DAY, 1, dbo.Opening(0, @pBaseHour))
       AND   SPH_TERMINAL_ID = @pTerminalId
     
    IF @pGameId = 0
    BEGIN
      -- Get game id from terminals
      SELECT   @pGameId = ISNULL(TE_LAST_GAME_PLAYED_ID, 0)
        FROM   TERMINALS
       WHERE   TE_TERMINAL_ID = @pTerminalId
    END
    
    IF @pGameId = 0
    BEGIN
      -- Get game id from terminal game translation
      SELECT   @pGameId = ISNULL(TGT_SOURCE_GAME_ID, 0)
        FROM   TERMINAL_GAME_TRANSLATION
       WHERE   TGT_TERMINAL_ID = @pTerminalId
    END
  END
  
  IF @pGameId = 0
  BEGIN
    RAISERROR (N'Terminal without activity can not be adjusted', 16, 10)
    RETURN
  END

  IF NOT EXISTS ( SELECT   *
                    FROM   SALES_PER_HOUR
                   WHERE   SPH_BASE_HOUR = @pBaseHour
                     AND   SPH_TERMINAL_ID = @pTerminalId
                     AND   SPH_GAME_ID = @pGameId)
  BEGIN

    -- Get terminal name and game id
    SELECT   @pTerminalName = TE_NAME
      FROM   TERMINALS
     WHERE   TE_TERMINAL_ID = @pTerminalId
    
    -- Get game name
    SELECT   @pGameName = GM_NAME
      FROM   GAMES
     WHERE   GM_GAME_ID = @pGameId   
     
    INSERT INTO SALES_PER_HOUR
               ( SPH_BASE_HOUR
               , SPH_TERMINAL_ID
               , SPH_TERMINAL_NAME
               , SPH_GAME_ID
               , SPH_GAME_NAME
               , SPH_PLAYED_COUNT
               , SPH_PLAYED_AMOUNT
               , SPH_WON_COUNT
               , SPH_WON_AMOUNT
               , SPH_NUM_ACTIVE_TERMINALS
               , SPH_LAST_PLAY_ID
               , SPH_THEORETICAL_WON_AMOUNT
               , SPH_JACKPOT_AMOUNT
               , SPH_PROGRESSIVE_JACKPOT_AMOUNT
               , SPH_PROGRESSIVE_JACKPOT_AMOUNT_0
               , SPH_PROGRESSIVE_PROVISION_AMOUNT)
         VALUES
               ( @pBaseHour
               , @pTerminalId
               , @pTerminalName
               , @pGameId
               , @pGameName
               , ISNULL(@pSPH_Played_Count, 0)
               , ISNULL(@pSPH_Played_Amount, 0)
               , ISNULL(@pSPH_Won_Count, 0)
               , ISNULL(@pSPH_Won_Amount, 0)
               , 0
               , 0
               , 0
               , ISNULL(@pSPH_Jackpot_Amount, 0)
               , 0
               , 0
               , 0)

  END
  ELSE
  BEGIN

    UPDATE   SALES_PER_HOUR
       SET   SPH_PLAYED_COUNT   = ISNULL(@pSPH_Played_Count, SPH_PLAYED_COUNT)
           , SPH_PLAYED_AMOUNT  = ISNULL(@pSPH_Played_Amount, SPH_PLAYED_AMOUNT)
           , SPH_WON_COUNT      = ISNULL(@pSPH_Won_Count, SPH_WON_COUNT)
           , SPH_WON_AMOUNT     = ISNULL(@pSPH_Won_Amount, SPH_WON_AMOUNT)
           , SPH_JACKPOT_AMOUNT = ISNULL(@pSPH_Jackpot_Amount, SPH_JACKPOT_AMOUNT)
     WHERE   SPH_BASE_HOUR = @pBaseHour
       AND   SPH_TERMINAL_ID = @pTerminalId
       AND   SPH_GAME_ID = @pGameId

  END
            
END 
GO

GRANT EXECUTE ON [dbo].[Update_Meters_SalesPerHour] TO [wggui] WITH GRANT OPTION
GO

--------------------------------------------------------------------------------
-- PURPOSE: Save AccountPoints for level in DB (for simulate a cache)
-- 
--  PARAMS:
--      - INPUT:
--           @AccountId       BIGINT       
--
--      - OUTPUT:
--
-- RETURNS:
--
--   NOTES:
--------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[AccountPointsCache_CalculateToday]
       @AccountId     BIGINT
AS
BEGIN

  DECLARE @update_history                                  AS BIT;
  
  DECLARE @apc_today                                       AS DATETIME;
  DECLARE @apc_days                                        AS INT;
	DECLARE @apc_history_points_generated_for_level          AS MONEY;
	DECLARE @apc_history_points_discretional_for_level       AS MONEY;
	DECLARE @apc_history_points_discretional_only_for_redeem AS MONEY;
	DECLARE @apc_history_points_promotion_only_for_redeem    AS MONEY;
	DECLARE @apc_today_points_generated_for_level            AS MONEY;
	DECLARE @apc_today_points_discretional_for_level         AS MONEY;
	DECLARE @apc_today_points_discretional_only_for_redeem   AS MONEY;
	DECLARE @apc_today_points_promotion_only_for_redeem      AS MONEY;
  DECLARE @apc_today_last_movement_id                      AS BIGINT;
  
  DECLARE @estudy_period                                   AS INT;
  DECLARE @date_from                                       AS DATETIME;
  DECLARE @now                                             AS DATETIME;
  DECLARE @today_opening                                   AS DATETIME;
  
  SET @update_history = 0;
  
  -- Get register from ACCOUNT_POINTS_CACHE
  SELECT   @apc_today                  = APC_TODAY 
         , @apc_days                   = APC_DAYS
         , @apc_today_last_movement_id = APC_TODAY_LAST_MOVEMENT_ID
    FROM   ACCOUNT_POINTS_CACHE 
   WHERE   APC_ACCOUNT_ID = @AccountId 
     
  -- GET General param: "PlayerTracking", "Levels.DaysCountingPoints"
  SET @estudy_period = (SELECT CAST(GP_KEY_VALUE AS INT) FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PlayerTracking' AND GP_SUBJECT_KEY = 'Levels.DaysCountingPoints')
     
  --_day_start_counting_points = Misc.TodayOpening().AddDays(-(_days_counting_points - 1));
  SET @now = GETDATE();
  SET @today_opening = dbo.Opening(0, @now)
  SET @date_from = DATEADD(DAY, -1*(@estudy_period-1), @today_opening);
    
  IF (   @today_opening <> ISNULL(@apc_today, DATEADD(d, -1, GETDATE()))   --> no hay registro o cambio de jornada.
      OR @estudy_period <> ISNULL(@apc_days, -1) )                         --> no hay registro o cambio en el periodo de estudio.
  BEGIN
    SET @update_history = 1;
  END   
    
  IF (@update_history = 1)
  BEGIN
    SET @apc_today_last_movement_id = 0;
    
    -- Get history data 
    EXEC AccountPointsCache_CalculatePoints @AccountId, @date_from, @today_opening, 
                                            @apc_today_last_movement_id, 
                                            @apc_today_last_movement_id                      OUTPUT, 
                                            @apc_history_points_generated_for_level          OUTPUT, 
                                            @apc_history_points_discretional_for_level       OUTPUT, 
                                            @apc_history_points_discretional_only_for_redeem OUTPUT, 
                                            @apc_history_points_promotion_only_for_redeem    OUTPUT
    --> Get today data
    EXEC AccountPointsCache_CalculatePoints @AccountId, @today_opening, @now, 
                                            @apc_today_last_movement_id, 
                                            @apc_today_last_movement_id                    OUTPUT, 
                                            @apc_today_points_generated_for_level          OUTPUT, 
                                            @apc_today_points_discretional_for_level       OUTPUT, 
                                            @apc_today_points_discretional_only_for_redeem OUTPUT, 
                                            @apc_today_points_promotion_only_for_redeem    OUTPUT
  
    DELETE  FROM ACCOUNT_POINTS_CACHE 
     WHERE  APC_ACCOUNT_ID = @AccountId 
   
    INSERT INTO ACCOUNT_POINTS_CACHE ( APC_ACCOUNT_ID, 
                                       APC_DAYS,       
                                       APC_HISTORY_POINTS_GENERATED_FOR_LEVEL,          
                                       APC_HISTORY_POINTS_DISCRETIONAL_FOR_LEVEL,
                                       APC_HISTORY_POINTS_DISCRETIONAL_ONLY_FOR_REDEEM,
                                       APC_HISTORY_POINTS_PROMOTION_ONLY_FOR_REDEEM,
                                       APC_TODAY, 
                                       APC_TODAY_POINTS_GENERATED_FOR_LEVEL,
                                       APC_TODAY_POINTS_DISCRETIONAL_FOR_LEVEL, 
                                       APC_TODAY_POINTS_DISCRETIONAL_ONLY_FOR_REDEEM, 
                                       APC_TODAY_POINTS_PROMOTION_ONLY_FOR_REDEEM,
                                       APC_TODAY_LAST_MOVEMENT_ID, 
                                       APC_TODAY_LAST_UPDATED ) 
                              VALUES ( @AccountId,     
                                       @estudy_period, 
                                       @apc_history_points_generated_for_level,              
                                       @apc_history_points_discretional_for_level, 
                                       @apc_history_points_discretional_only_for_redeem,     
                                       @apc_history_points_promotion_only_for_redeem,
                                       @today_opening, 
                                       @apc_today_points_generated_for_level, 
                                       @apc_today_points_discretional_for_level, 
                                       @apc_today_points_discretional_only_for_redeem, 
                                       @apc_today_points_promotion_only_for_redeem, 
                                       @apc_today_last_movement_id, 
                                       @now )  
  END
  ELSE
  BEGIN
    --> Get today data
    EXEC AccountPointsCache_CalculatePoints @AccountId, @today_opening, @now, 
                                            @apc_today_last_movement_id, 
                                            @apc_today_last_movement_id                    OUTPUT, 
                                            @apc_today_points_generated_for_level          OUTPUT, 
                                            @apc_today_points_discretional_for_level       OUTPUT, 
                                            @apc_today_points_discretional_only_for_redeem OUTPUT, 
                                            @apc_today_points_promotion_only_for_redeem    OUTPUT
    
    --update today
    UPDATE   ACCOUNT_POINTS_CACHE  
       SET   APC_TODAY                                     = @today_opening
           , APC_TODAY_POINTS_GENERATED_FOR_LEVEL          = APC_TODAY_POINTS_GENERATED_FOR_LEVEL          + @apc_today_points_generated_for_level         
           , APC_TODAY_POINTS_DISCRETIONAL_FOR_LEVEL       = APC_TODAY_POINTS_DISCRETIONAL_FOR_LEVEL       + @apc_today_points_discretional_for_level      
           , APC_TODAY_POINTS_DISCRETIONAL_ONLY_FOR_REDEEM = APC_TODAY_POINTS_DISCRETIONAL_ONLY_FOR_REDEEM + @apc_today_points_discretional_only_for_redeem
           , APC_TODAY_POINTS_PROMOTION_ONLY_FOR_REDEEM    = APC_TODAY_POINTS_PROMOTION_ONLY_FOR_REDEEM    + @apc_today_points_promotion_only_for_redeem   
           , APC_TODAY_LAST_MOVEMENT_ID                    = @apc_today_last_movement_id 
           , APC_TODAY_LAST_UPDATED                        = @now
     WHERE   APC_ACCOUNT_ID = @AccountId
     
  END
    
END  -- PROCEDURE [dbo].[AccountPointsCache_CalculateToday]
GO  

ALTER PROCEDURE [dbo].[AccountPointsCache_CalculatePoints]
       @AccountId                        BIGINT,
       @DateFrom                         DATETIME,
       @DateTo                           DATETIME,
       @MovementId                       BIGINT, 
       @LastMovementId                   BIGINT OUTPUT, 
       @PointsGeneratedForLevel          MONEY  OUTPUT,
       @PointsDiscretionalForLevel       MONEY  OUTPUT,
       @PointsDiscretionalOnlyForRedeem  MONEY  OUTPUT,
       @PointsPromotionOnlyForRedeem     MONEY  OUTPUT
AS
BEGIN
  DECLARE @points_awarded                        AS INT;
  DECLARE @manually_added_points_for_level       AS INT;
  DECLARE @imported_points_for_level             AS INT;
  DECLARE @imported_points_history               AS INT;
  DECLARE @manually_added_points_only_for_redeem AS INT;
  DECLARE @imported_points_only_for_redeem       AS INT;
  DECLARE @promotion_point                       AS INT;
  DECLARE @cancel_promotion_point                AS INT;
 
  DECLARE @Sql             AS NVARCHAR(MAX);
  DECLARE @ParamDefinition AS NVARCHAR(MAX);
  DECLARE @Index           AS NVARCHAR(MAX);
  DECLARE @Where           AS NVARCHAR(MAX); 
  
  -- Points for level
     -- Generated
  SET @points_awarded                        = 36
     -- Discretional
  SET @manually_added_points_for_level       = 68
  SET @imported_points_for_level             = 72
  SET @imported_points_history               = 73
  -- Points only for redeem
     -- Discretional 
  SET @manually_added_points_only_for_redeem = 50
  SET @imported_points_only_for_redeem       = 71
     -- Promotion 
  SET @promotion_point                       = 60
  SET @cancel_promotion_point                = 61
  
  SET @LastMovementId = @MovementId

  SET @Index = 'IX_am_account_id_type_datetime'
  SET @Where = ' WHERE AM_ACCOUNT_ID  = @AccountId ' +
               '   AND AM_TYPE IN (@points_awarded, @manually_added_points_for_level, @imported_points_for_level, @imported_points_history, ' +
               '                   @manually_added_points_only_for_redeem, @imported_points_only_for_redeem, @promotion_point, @cancel_promotion_point) ' + 
               '   AND AM_DATETIME >= CAST(''' + CAST(@DateFrom AS VARCHAR(50)) + ''' AS DATETIME) ' +
               '   AND AM_DATETIME  < CAST(''' + CAST(@DateTo AS VARCHAR(50)) + ''' AS DATETIME) ' +
               '   AND AM_MOVEMENT_ID > ' + CAST(@MovementId AS NVARCHAR(MAX)) 
  
  SET @Sql = '
  SELECT   @PointsGeneratedForLevel_out          = ISNULL(SUM (CASE WHEN AM_TYPE IN (@points_awarded)
                                                               THEN AM_ADD_AMOUNT 
                                                               ELSE 0 
                                                               END ), 0) 
         , @PointsDiscretionalForLevel_out       = ISNULL(SUM (CASE WHEN AM_TYPE IN (@manually_added_points_for_level, @imported_points_for_level, @imported_points_history)
                                                               THEN AM_ADD_AMOUNT 
                                                               ELSE 0 
                                                               END ), 0) 
         , @PointsDiscretionalOnlyForRedeem_out  = ISNULL(SUM (CASE WHEN AM_TYPE IN (@manually_added_points_only_for_redeem, @imported_points_only_for_redeem)
                                                               THEN AM_ADD_AMOUNT 
                                                               ELSE 0 
                                                               END ), 0)
         , @PointsPromotionOnlyForRedeem_out     = ISNULL(SUM (CASE WHEN AM_TYPE IN (@promotion_point, @cancel_promotion_point)
                                                               THEN AM_ADD_AMOUNT-AM_SUB_AMOUNT
                                                               ELSE 0 
                                                               END ), 0) 
         , @LastMovementId_out                   = ISNULL(MAX (AM_MOVEMENT_ID), @LastMovementId_out)
    FROM   ACCOUNT_MOVEMENTS WITH (INDEX (' + @Index + ')) ' 
  + @Where
 
SET @ParamDefinition = N'@AccountId                             BIGINT, 
                        @points_awarded                         INT, 
                        @manually_added_points_for_level        INT, 
                        @imported_points_for_level              INT, 
                        @imported_points_history                INT, 
                        @manually_added_points_only_for_redeem  INT, 
                        @imported_points_only_for_redeem        INT, 
                        @promotion_point                        INT, 
                        @cancel_promotion_point                 INT, 
                        @LastMovementId_out                     BIGINT OUTPUT, 
                        @PointsGeneratedForLevel_out            MONEY  OUTPUT, 
                        @PointsDiscretionalForLevel_out         MONEY  OUTPUT, 
                        @PointsDiscretionalOnlyForRedeem_out    MONEY  OUTPUT, 
                        @PointsPromotionOnlyForRedeem_out       MONEY  OUTPUT' 

EXEC sp_executesql @Sql, 
                   @ParamDefinition,
                   @AccountId                             = @AccountId, 
                   @points_awarded                        = @points_awarded,                        
                   @manually_added_points_for_level       = @manually_added_points_for_level,
                   @imported_points_for_level             = @imported_points_for_level,
                   @imported_points_history               = @imported_points_history, 
                   @manually_added_points_only_for_redeem = @manually_added_points_only_for_redeem, 
                   @imported_points_only_for_redeem       = @imported_points_only_for_redeem, 
                   @promotion_point                       = @promotion_point,
                   @cancel_promotion_point                = @cancel_promotion_point,
                   @LastMovementId_out                    = @LastMovementId                   OUTPUT, 
                   @PointsGeneratedForLevel_out           = @PointsGeneratedForLevel          OUTPUT,
                   @PointsDiscretionalForLevel_out        = @PointsDiscretionalForLevel       OUTPUT,
                   @PointsDiscretionalOnlyForRedeem_out   = @PointsDiscretionalOnlyForRedeem  OUTPUT,
                   @PointsPromotionOnlyForRedeem_out      = @PointsPromotionOnlyForRedeem     OUTPUT 

---------------------------------------------------------------------------------------------------------------------------------
-- Note:
-- @LastMovementId_out : Is the variable�s name of the output parameter of '@Sql execute'
-- When run 'EXEC sp_executesql' is finished, the value of @LastMovementId_out is assigned to @LastMovementId
---------------------------------------------------------------------------------------------------------------------------------

END  -- PROCEDURE [dbo].[AccountPointsCache_CalculatePoints]
GO
