/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 297;

SET @New_ReleaseId = 298;
SET @New_ScriptName = N'UpdateTo_18.298.038.sql';
SET @New_Description = N'';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/******* TABLES  *******/
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[terminal_status]') and name = 'ts_highroller_alarm_id')
  ALTER TABLE [dbo].[terminal_status] ADD ts_highroller_alarm_id bigint null
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[terminal_status]') and name = 'ts_highroller_anonymous_alarm_id')
  ALTER TABLE [dbo].[terminal_status] ADD ts_highroller_anonymous_alarm_id bigint null
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[Terminals]') and name = 'te_terminal_currency_id')
  ALTER TABLE Terminals ADD te_terminal_currency_id INT NULL
GO

-- NEW FIELD RESERVED IN ACCOUNTS
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[accounts]') and name = 'ac_re_reserved')
  ALTER TABLE dbo.accounts ADD ac_re_reserved money NOT NULL CONSTRAINT [DF_accounts_ac_re_reserved] DEFAULT ((0))
GO

-- NEW FIELD MODE_RESERVED IN ACCOUNTS
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[accounts]') and name = 'ac_mode_reserved')
  ALTER TABLE dbo.accounts ADD ac_mode_reserved BIT NOT NULL CONSTRAINT [DF_accounts_ac_mode_reserved] DEFAULT ((0))
GO

-- GAMEGATEWAY_TERMINAL_LIST
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[gamegateway_terminal_list]') AND type in (N'U'))
BEGIN
   CREATE TABLE [dbo].[gamegateway_terminal_list] (gtl_terminal_list XML)
   INSERT INTO [dbo].[gamegateway_terminal_list] (gtl_terminal_list) VALUES (null);
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[catalogs]') AND type in (N'U'))
  CREATE TABLE [dbo].[catalogs](
    [cat_id] [bigint] IDENTITY(1,1) NOT NULL,
    [cat_type] [smallint] NOT NULL,
    [cat_name] [nvarchar](50) NULL,
    [cat_description] [nvarchar](250) NULL,
    [cat_enabled] [bit] NOT NULL,
    [cat_system_type] [int] NULL DEFAULT 0,
   CONSTRAINT [PK_catalogs] PRIMARY KEY CLUSTERED 
  (
    [cat_id] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
  ) ON [PRIMARY] 
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[catalog_items]') AND type in (N'U'))
  CREATE TABLE [dbo].[catalog_items](
    [cai_id] [bigint] IDENTITY(1,1) NOT NULL,
    [cai_catalog_id] [bigint] NOT NULL,
    [cai_name] [nvarchar](50) NULL,
    [cai_description] [nvarchar](250) NULL,
    [cai_enabled] [bit] NOT NULL,
   CONSTRAINT [PK_catalog_items] PRIMARY KEY CLUSTERED 
  (
    [cai_id] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
  ) ON [PRIMARY]  
GO

ALTER TABLE ACCOUNTS DROP COLUMN ac_external_aml_file_sequence;
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[ACCOUNTS]') and name = 'ac_external_aml_file_sequence')
  ALTER TABLE ACCOUNTS ADD ac_external_aml_file_sequence UNIQUEIDENTIFIER NULL;
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[terminal_currency]') AND type in (N'U'))
BEGIN
   CREATE TABLE [dbo].[terminal_currency](
      [tc_id] [int] IDENTITY(1,1) NOT NULL,
      [tc_name] [nvarchar](50) NULL,
    CONSTRAINT [PK_terminal_currency] PRIMARY KEY CLUSTERED 
   (
      [tc_id] ASC
   )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
   ) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[terminal_currency_details]') AND type in (N'U'))
BEGIN
   CREATE TABLE [dbo].[terminal_currency_details](
      [tcd_terminal_currency_id] [int] NOT NULL,
      [tcd_iso_code] [nvarchar](3) NOT NULL,
      [tcd_denomination] [money] NOT NULL,
      [tcd_currency_type] [int] NOT NULL,
      [tcd_direction] [int] NOT NULL,
    CONSTRAINT [PK_terminal_currency_details] PRIMARY KEY CLUSTERED 
   (
      [tcd_terminal_currency_id] ASC,
      [tcd_iso_code] ASC,
      [tcd_denomination] ASC,
      [tcd_currency_type] ASC
   )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
   ) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[gamegateway_games]') AND type in (N'U'))
BEGIN
  CREATE TABLE [dbo].[gamegateway_games](
    [gg_game_id]          [bigint] NOT NULL,
    [gg_partner_id]       [int] NOT NULL,
    [gg_name]             [nvarchar](50) NULL,
    [gg_name_translated]  [nvarchar](50) NULL,
    [gg_logo]             [nvarchar] (200) NULL,
    [gg_show_lcd_message] [bit]   DEFAULT 1 NOT NULL,
    [gg_played]           [money] DEFAULT 0 NOT NULL,
    [gg_won]              [money] DEFAULT 0 NOT NULL,
    [gg_won_jackpot]      [money] DEFAULT 0 NOT NULL,
    [gg_num_bets]         [int]   DEFAULT 0 NOT NULL,
    [gg_num_prizes]       [int]   DEFAULT 0 NOT NULL,
    [gg_created]          [datetime]  NOT NULL,
    [ggi_finished]        [datetime]  NULL,
    [gg_last_updated]     [timestamp] NOT NULL,
   CONSTRAINT [PK_gamegateway_games] PRIMARY KEY CLUSTERED 
  (
  	[gg_game_id] ASC, [gg_partner_id] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
  ) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[gamegateway_game_instances]') AND type in (N'U'))
BEGIN
  CREATE TABLE [dbo].[gamegateway_game_instances](
    [ggi_game_instance_id] [bigint] NOT NULL,
    [ggi_partner_id]       [int] NOT NULL,	
    [ggi_game_id]          [bigint] NOT NULL,	
    [ggi_first_prize]      [money] NULL,
    [ggi_entry_cost]       [money] NULL,
    [ggi_jackpot]          [money] NULL,	
    [ggi_played]           [money] DEFAULT 0 NOT NULL,
    [ggi_won]              [money] DEFAULT 0 NOT NULL,
    [ggi_won_jackpot]      [money] DEFAULT 0 NOT NULL,
    [ggi_num_bets]         [int]   DEFAULT 0 NOT NULL,
    [ggi_num_prizes]       [int]   DEFAULT 0 NOT NULL,
    [ggi_starts]           [datetime]  NULL,
    [ggi_created]          [datetime]  NOT NULL,
    [ggi_last_updated]     [timestamp] NOT NULL
   CONSTRAINT [PK_gamegateway_game_instances] PRIMARY KEY CLUSTERED 
  (
  	[ggi_game_instance_id] ASC, [ggi_partner_id] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
  ) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[gamegateway_log]') AND type in (N'U'))
BEGIN
  CREATE TABLE [dbo].[gamegateway_log](
    [gl_id]       [bigint] IDENTITY(1,1) NOT NULL,
    [gl_request]  [xml] NULL,
    [gl_response] [xml] NULL,
    [gl_datetime] [datetime] DEFAULT GETDATE() NOT NULL
   CONSTRAINT [PK_gamegateway_log] PRIMARY KEY CLUSTERED 
  (
    [gl_id] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
  ) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[gamegateway_bulk]') AND type in (N'U'))
BEGIN
  CREATE TABLE [dbo].[gamegateway_bulk](
    [gb_id]               [bigint] IDENTITY(1,1) NOT NULL,
    [gb_user_id]          [bigint]   NOT NULL,
    [gb_amount]           [money]    NOT NULL,
    [gb_jackpot]          [money]    NOT NULL,
    [gb_num_credits]      [int]      NOT NULL,
    [gb_type]             [int]      NOT NULL,
    [gb_game_id]          [bigint]   NOT NULL,
    [gb_game_instance_id] [bigint]   NOT NULL,
    [gb_partner_id]       [int]      NOT NULL,
    [gb_created]          [datetime] NOT NULL,
    [gb_execution]        [datetime] NULL
   CONSTRAINT [PK_gamegateway_bulk] PRIMARY KEY CLUSTERED 
  (
    [gb_id] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
  ) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT * FROM sys.columns  WHERE object_id = object_id(N'[dbo].[account_major_prizes]')   AND name = 'amp_generated_document') 
  ALTER TABLE dbo.account_major_prizes ADD amp_generated_document BIT NULL 
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[promotions]') and name = 'pm_created_account_filter')
  ALTER TABLE [dbo].[promotions] ADD [pm_created_account_filter] int NULL;
GO
  
IF NOT EXISTS(SELECT * FROM sys.columns WHERE Name = N'ao_reason_id' AND Object_ID = Object_ID(N'account_operations'))
  ALTER TABLE dbo.account_operations ADD ao_reason_id BIGINT NULL
GO

IF NOT EXISTS(SELECT * FROM sys.columns WHERE Name = N'ao_comment' AND Object_ID = Object_ID(N'account_operations'))
  ALTER TABLE dbo.account_operations ADD ao_comment NVARCHAR(250) NULL -- revisar estandar maximo
GO



/******* INDEXES *******/

/******* RECORDS *******/

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Alarms' AND GP_SUBJECT_KEY ='HighRoller.Enabled')
  INSERT INTO general_params (gp_group_key, gp_subject_key, gp_key_value) VALUES ('Alarms', 'HighRoller.Enabled', '');

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Alarms' AND GP_SUBJECT_KEY ='HighRoller.Limit.BetAverage')
  INSERT INTO general_params (gp_group_key, gp_subject_key, gp_key_value) VALUES ('Alarms', 'HighRoller.Limit.BetAverage', '0');

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Alarms' AND GP_SUBJECT_KEY ='HighRoller.PeriodUnderStudy.Unit')
  INSERT INTO general_params (gp_group_key, gp_subject_key, gp_key_value) VALUES ('Alarms', 'HighRoller.PeriodUnderStudy.Unit', '');

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Alarms' AND GP_SUBJECT_KEY ='HighRoller.PeriodUnderStudy.Value')
  INSERT INTO general_params (gp_group_key, gp_subject_key, gp_key_value) VALUES ('Alarms', 'HighRoller.PeriodUnderStudy.Value', '0');

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Alarms' AND GP_SUBJECT_KEY ='HighRoller.Limit.CoinIn')
  INSERT INTO general_params (gp_group_key, gp_subject_key, gp_key_value) VALUES ('Alarms', 'HighRoller.Limit.CoinIn', '0');
 
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Alarms' AND GP_SUBJECT_KEY ='HighRoller.ProcessAnonymous')
  INSERT INTO general_params (gp_group_key, gp_subject_key, gp_key_value) VALUES ('Alarms', 'HighRoller.ProcessAnonymous', ''); 
GO

declare @catalogId as int

IF NOT EXISTS (SELECT * FROM catalogs WHERE cat_system_type = 1)
BEGIN
  INSERT INTO catalogs (cat_type,cat_name,cat_description,cat_enabled,cat_system_type) VALUES (1,'ApplyTax','C�talogo de razones para aplicar impuesto', 1, 1)
  set @catalogId = (SELECT cat_id  FROM catalogs WHERE  cat_system_type = 1)
  INSERT INTO catalog_items(cai_catalog_id,cai_name,cai_description,cai_enabled) VALUES (@catalogId, 'Premio de progresivo','Premio de progresivo',1)
  INSERT INTO catalog_items(cai_catalog_id,cai_name,cai_description,cai_enabled) VALUES (@catalogId, 'RTM', 'RTM',1)
  INSERT INTO catalog_items(cai_catalog_id,cai_name,cai_description,cai_enabled) VALUES (@catalogId, 'RTE', 'RTE',1)
END


IF NOT EXISTS (SELECT * FROM catalogs WHERE cat_system_type = 2)
BEGIN
  INSERT INTO catalogs (cat_type,cat_name,cat_description,cat_enabled,cat_system_type) VALUES (1,'RemoveTax','C�talogo de razones para eliminar impuestos', 1, 2)
  SET @catalogId = (SELECT cat_id  FROM catalogs WHERE  cat_system_type = 2)
  INSERT INTO catalog_items(cai_catalog_id,cai_name,cai_description,cai_enabled) VALUES (@catalogId, 'Por prueba de m�quinas', 'Por prueba de m�quinas',1)
  INSERT INTO catalog_items(cai_catalog_id,cai_name,cai_description,cai_enabled) VALUES (@catalogId, 'Por mal funcionamiento de m�quinas', 'Por mal funcionamiento de m�quinas',1)
  INSERT INTO catalog_items(cai_catalog_id,cai_name,cai_description,cai_enabled) VALUES (@catalogId, 'Otros', 'Otros', 1)
END
GO

UPDATE terminal_meter_delta_description SET tmdd_field_description = REPLACE(tmdd_field_description, 'BillIn', 'BillAccepted') WHERE tmdd_field_description like 'Bill%'
GO  

IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='Witholding.Document' AND GP_SUBJECT_KEY = 'GenerateDocument')
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Witholding.Document', 'GenerateDocument', '1')
GO

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.TITOTaxWaiver' AND GP_SUBJECT_KEY = 'Mode')
  INSERT INTO [dbo].[general_params]    ([gp_group_key],[gp_subject_key],[gp_key_value]) VALUES ('Cashier.TITOTaxWaiver','Mode', 0)
GO

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.TITOTaxWaiver' AND GP_SUBJECT_KEY = 'TicketPayment.ApplyTaxesByDefault')
  INSERT INTO [dbo].[general_params] ([gp_group_key],[gp_subject_key],[gp_key_value]) VALUES ('Cashier.TITOTaxWaiver','TicketPayment.ApplyTaxesByDefault', 0)
GO

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.TITOTaxWaiver' AND GP_SUBJECT_KEY = 'ChipsPurchase.ApplyTaxesByDefault')
  INSERT INTO [dbo].[general_params] ([gp_group_key],[gp_subject_key],[gp_key_value]) VALUES ( 'Cashier.TITOTaxWaiver','ChipsPurchase.ApplyTaxesByDefault', 0)
GO

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.TITOTaxWaiver' AND GP_SUBJECT_KEY = 'Handpay.Machine.ApplyTaxesByDefault')
  INSERT INTO [dbo].[general_params] ([gp_group_key],[gp_subject_key],[gp_key_value]) VALUES ( 'Cashier.TITOTaxWaiver','Handpay.Machine.ApplyTaxesByDefault', 0)
GO

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.TITOTaxWaiver' AND GP_SUBJECT_KEY = 'Handpay.Manual.CancelCredits.ApplyTaxesByDefault')
  INSERT INTO [dbo].[general_params]   ([gp_group_key],[gp_subject_key],[gp_key_value])  VALUES ( 'Cashier.TITOTaxWaiver','Handpay.Manual.CancelCredits.ApplyTaxesByDefault', 0)
GO

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.TITOTaxWaiver' AND GP_SUBJECT_KEY = 'Handpay.Manual.Jackpot.ApplyTaxesByDefault')
  INSERT INTO [dbo].[general_params] ([gp_group_key],[gp_subject_key],[gp_key_value]) VALUES ( 'Cashier.TITOTaxWaiver','Handpay.Manual.Jackpot.ApplyTaxesByDefault', 0)
GO

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.TITOTaxWaiver' AND GP_SUBJECT_KEY = 'Handpay.Manual.ProgJackpot.ApplyTaxesByDefault')
  INSERT INTO [dbo].[general_params]  ([gp_group_key],[gp_subject_key],[gp_key_value]) VALUES ( 'Cashier.TITOTaxWaiver','Handpay.Manual.ProgJackpot.ApplyTaxesByDefault', 0)
GO

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.TITOTaxWaiver' AND GP_SUBJECT_KEY = 'Handpay.Manual.MaxPrize.ApplyTaxesByDefault')
  INSERT INTO [dbo].[general_params] ([gp_group_key],[gp_subject_key],[gp_key_value]) VALUES ( 'Cashier.TITOTaxWaiver','Handpay.Manual.MaxPrize.ApplyTaxesByDefault', 0)
GO

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.TITOTaxWaiver' AND GP_SUBJECT_KEY = 'Handpay.Manual.Other.ApplyTaxesByDefault')
  INSERT INTO [dbo].[general_params] ([gp_group_key],[gp_subject_key],[gp_key_value]) VALUES ( 'Cashier.TITOTaxWaiver','Handpay.Manual.Other.ApplyTaxesByDefault', 0)
GO

IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='WigosKiosk' AND GP_SUBJECT_KEY = 'Promotions.NR.PrintTITOTicket')
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('WigosKiosk', 'Promotions.NR.PrintTITOTicket', '0')
GO
   
IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='WigosKiosk' AND GP_SUBJECT_KEY = 'Promotions.RE.PrintTITOTicket')
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('WigosKiosk', 'Promotions.RE.PrintTITOTicket', '0')
GO

IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='SmartFloor' AND GP_SUBJECT_KEY = 'Terminal.QRCode.Footer')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ( 'SmartFloor' , 'Terminal.QRCode.Footer' , 'AssetNumber:{@AssetNumber}')
GO

IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='SmartFloor' AND GP_SUBJECT_KEY = 'Terminal.QRCode')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ( 'SmartFloor' , 'Terminal.QRCode',   '{@QR}')
GO

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'SoftCount' AND GP_SUBJECT_KEY = 'Format001.Type')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('SoftCount', 'Format001.Type', '1') 
-- 1: Cummins
IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'SoftCount' AND GP_SUBJECT_KEY = 'Format002.Type')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('SoftCount', 'Format002.Type', '1') 
-- 1: Cummins
IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'SoftCount' AND GP_SUBJECT_KEY = 'Format002.Identifier')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('SoftCount', 'Format002.Identifier', '0') 
-- 0: STACKER ID, 1: EGM-BASE_NAME, 2: EGM-FLOOR_ID
IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'SoftCount' AND GP_SUBJECT_KEY = 'Format002.Denominations')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('SoftCount', 'Format002.Denominations', '1;2;5;10;20;50;100')
IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'SoftCount' AND GP_SUBJECT_KEY = 'Format003.Type')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('SoftCount', 'Format003.Type', '1') 
-- 1: Cummins
IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'SoftCount' AND GP_SUBJECT_KEY = 'Format003.Identifier')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('SoftCount', 'Format003.Identifier', '0') 
-- 0: STACKER ID, 1: EGM-BASE_NAME, 2: EGM-FLOOR_ID
IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'SoftCount' AND GP_SUBJECT_KEY = 'Format003.Denominations')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('SoftCount', 'Format003.Denominations', '1;5;10;20;50;100')
GO

IF NOT EXISTS (SELECT * FROM terminals WHERE te_terminal_type = 109)
BEGIN
  INSERT INTO [dbo].[terminals]
             ([te_type]
             ,[te_base_name]
             ,[te_external_id]
             ,[te_blocked]
             ,[te_active]
             ,[te_provider_id]
             ,[te_terminal_type]
             ,[te_status]
             ,[te_master_id])
       VALUES (1
              ,'BONOPLAY'
              ,'BONOPLAY'
              ,0
              ,1
              ,'CASINO'
              ,109
              ,0
              ,1);
   
  UPDATE [dbo].[terminals] SET [te_master_id] = [te_terminal_id] WHERE [te_terminal_type] = 109;
END

IF NOT EXISTS (SELECT * FROM TERMINAL_TYPES WHERE TT_TYPE = 109)
  INSERT INTO TERMINAL_TYPES ( TT_TYPE, TT_NAME) VALUES (109,'BONOPLAY')
GO

IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='GameGateway' AND GP_SUBJECT_KEY = 'Enabled')
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('GameGateway', 'Enabled', '0')

IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='GameGateway' AND GP_SUBJECT_KEY = 'Provider')
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('GameGateway', 'Provider', '001')

IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='GameGateway' AND GP_SUBJECT_KEY = 'Log.Mode')
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('GameGateway', 'Log.Mode', '0')
   
IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='GameGateway' AND GP_SUBJECT_KEY = 'Uri')
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('GameGateway', 'Uri', 'http://+:7777/')

IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='GameGateway' AND GP_SUBJECT_KEY = 'Reserved.Enabled')
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('GameGateway', 'Reserved.Enabled', '0')
            
IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='GameGateway' AND GP_SUBJECT_KEY = 'Provider.001.Name')
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('GameGateway', 'Provider.001.Name', 'BonoPLAY')
   
IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='GameGateway' AND GP_SUBJECT_KEY = 'Provider.001.Enabled')
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('GameGateway', 'Provider.001.Enabled', '0')   
   
IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='GameGateway' AND GP_SUBJECT_KEY = 'Provider.001.Uri')
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('GameGateway', 'Provider.001.Uri', 'http://www.xxxxx.xxx')
   
IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='GameGateway' AND GP_SUBJECT_KEY = 'Provider.001.PartnerId')
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('GameGateway', 'Provider.001.PartnerId', '001')
   
IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='GameGateway' AND GP_SUBJECT_KEY = 'Provider.001.Password')
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('GameGateway', 'Provider.001.Password', 'password')
   
IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='GameGateway' AND GP_SUBJECT_KEY = 'Provider.001.CreditTimeOut')
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('GameGateway', 'Provider.001.CreditTimeOut', '10')               

IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='GameGateway' AND GP_SUBJECT_KEY = 'Provider.001.ShowNewGameMessage')
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('GameGateway', 'Provider.001.ShowNewGameMessage', '1')  

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'DisplayTouch' AND GP_SUBJECT_KEY = 'ProductName')
   INSERT INTO general_params (gp_group_key, gp_subject_key, gp_key_value) VALUES ('DisplayTouch', 'ProductName', 'InTouch');
GO

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PlayerTracking' AND GP_SUBJECT_KEY ='PayoutVarianceThreshold')
  INSERT INTO general_params (gp_group_key, gp_subject_key, gp_key_value) VALUES ('PlayerTracking', 'PayoutVarianceThreshold', '3'); 
GO

DECLARE @_table_name NVARCHAR(50)
DECLARE @_col_name   NVARCHAR(50)
DECLARE @_command    NVARCHAR(100)

IF NOT EXISTS (select DATA_TYPE from INFORMATION_SCHEMA.COLUMNS IC where lower(TABLE_NAME) = 'promotions' and lower(COLUMN_NAME) = 'pm_award_on_promobox' and lower(DATA_TYPE) = 'int')
BEGIN

   SET @_table_name = N'promotions'
   SET @_col_name   = N'pm_award_on_promobox'

   SELECT   @_command = 'ALTER TABLE ' + @_table_name + ' DROP CONSTRAINT ' + d.name
     FROM   sys.tables t join sys.default_constraints d on d.parent_object_id = t.object_id 
     JOIN   sys.columns c on c.object_id = t.object_id and c.column_id = d.parent_column_id
    WHERE   t.name = @_table_name
      AND   c.name = @_col_name

   EXECUTE (@_command)   

	ALTER TABLE [dbo].[promotions] ALTER COLUMN [pm_award_on_promobox] INT
	ALTER TABLE [dbo].[promotions] ADD CONSTRAINT DF_promotions_pm_award_on_promobox DEFAULT (3) FOR [pm_award_on_promobox]
	UPDATE [dbo].[promotions] SET [pm_award_on_promobox] = [pm_award_on_promobox] * 3
END
GO

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'DisplayTouch' AND GP_SUBJECT_KEY ='CustomButtons.TransferAmount.NR')
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('DisplayTouch', 'CustomButtons.TransferAmount.NR', 'MXN;5;25;50;75;100')
GO

IF  NOT EXISTS (SELECT * FROM  [dbo].[alarm_catalog]  WHERE [alcg_alarm_code] = 2097168)
BEGIN
  INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) 
    VALUES ( 2097168, 10, 0, N'Alarma personalizada por HighRoller', N'Alarma personalizada por HighRoller', 1)
    
  INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) 
    VALUES ( 2097168,  9, 0, N'Custom Alarm for HighRoller', N'Custom Alarm for HighRoller', 1)
  
  INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES ( 2097168, 24, 0, GETDATE() )
END   
GO

IF  NOT EXISTS (SELECT * FROM  [dbo].[alarm_catalog]  WHERE [alcg_alarm_code] = 2097169)
BEGIN
  INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) 
    VALUES ( 2097169, 10, 0, N'Alarma personalizada por HighRoller An�nimo', N'Alarma personalizada por HighRoller An�nimo', 1)
    
  INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) 
    VALUES ( 2097169,  9, 0, N'Custom Alarm for Anonymous HighRoller', N'Custom Alarm for Anonymous HighRoller', 1)
  
  INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES ( 2097169, 24, 0, GETDATE() )
END
GO

IF (SELECT gp_key_value FROM general_params where gp_group_key =  'AntiMoneyLaundering' AND gp_subject_key = 'Enabled') = '0' 
 BEGIN
      UPDATE   GENERAL_PARAMS  
         SET   GP_KEY_VALUE   = '0'  
       WHERE   GP_GROUP_KEY   = 'AntiMoneyLaundering' 
         AND   GP_SUBJECT_KEY = 'HasBeneficiary' 
         AND   GP_KEY_VALUE   <> '0'  
 END
GO

/******* PROCEDURES *******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetCashierTransactionsTaxes]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[GetCashierTransactionsTaxes]
GO
CREATE PROCEDURE [dbo].[GetCashierTransactionsTaxes] 
(     @pStartDatetime DATETIME,
      @pEndDatetime DATETIME,
      @pTerminalId INT,
      @pCmMovements NVARCHAR(MAX),
      @pHpMovements NVARCHAR(MAX),
      @pPaymentUserId INT,
      @pAuthorizeUserId INT
)
AS 
BEGIN
  DECLARE @_PAID int
  DECLARE @_SQL nVarChar(max)
	
	SET @_PAID = 32768
	
	SET @_SQL = '
    SELECT   AO_OPERATION_ID
           , MAX(CM_DATE)                  AS CM_DATE
           , MAX(CM_TYPE)                  AS CM_TYPE
           , PAY.GU_USERNAME               AS PAYMENT_USER 
           , CM_USER_NAME                  AS AUTHORIZATION_USER
           , SUM(CASE CM_TYPE 
                 WHEN   6 THEN CM_ADD_AMOUNT
                 WHEN  14 THEN CM_ADD_AMOUNT
                 ELSE 0 END )             AS TOTAL_TAX
           , SUM(CASE CM_TYPE 
                 WHEN 103 THEN CM_SUB_AMOUNT
                 WHEN 124 THEN CM_SUB_AMOUNT
                 WHEN 125 THEN CM_SUB_AMOUNT
                 WHEN 126 THEN CM_SUB_AMOUNT
                 ELSE 0 END )             AS TITO_TICKET_PAID
           , SUM(CASE CM_TYPE 
                 WHEN 304 THEN CM_ADD_AMOUNT
                 ELSE 0 END )             AS CHIPS_PURCHASE_TOTAL
           , SUM(CASE CM_TYPE 
                 WHEN  30 THEN CM_SUB_AMOUNT
                 WHEN  32 THEN CM_SUB_AMOUNT
                 ELSE 0 END )             AS HANDPAY
           , CAI_NAME                      AS REASON
           , CS_CASHIER_ID
           , CM_USER_ID
           , AO_COMMENT
           , CM_CASHIER_NAME
      INTO   #TEMP_CASHIER_SESSIONS     
      FROM   CASHIER_MOVEMENTS  WITH(INDEX(IX_cm_date_type))
INNER JOIN   CASHIER_SESSIONS   ON CS_SESSION_ID   = CM_SESSION_ID 
INNER JOIN   ACCOUNT_OPERATIONS ON AO_OPERATION_ID = CM_OPERATION_ID
INNER JOIN   GUI_USERS AS PAY   ON PAY.GU_USER_ID  = CS_USER_ID
 LEFT JOIN   CATALOG_ITEMS      ON CAI_ID          = AO_REASON_ID
     WHERE   1 = 1 '
    + CASE WHEN @pStartDatetime IS NOT NULL THEN ' AND CM_DATE >= ''' + CONVERT(VARCHAR, @pStartDatetime, 21) + '''' ELSE '' END + CHAR(10) 
    + CASE WHEN @pEndDatetime IS NOT NULL THEN ' AND CM_DATE < ''' + CONVERT(VARCHAR, @pEndDatetime, 21) + '''' ELSE '' END + CHAR(10) 
+ '    AND   CM_TYPE IN (6, 14, 103, 124, 125, 126, 304, 30, 32) '
    + CASE WHEN @pTerminalId IS NOT NULL THEN ' AND CS_CASHIER_ID = ' + RTRIM(LTRIM(CONVERT(VARCHAR, @pTerminalId, 21))) + '' ELSE '' END + CHAR(10) 
    + CASE WHEN @pPaymentUserId IS NOT NULL THEN ' AND PAY.GU_USER_ID = ' + RTRIM(LTRIM(CONVERT(VARCHAR, @pPaymentUserId, 21))) + '' ELSE '' END + CHAR(10) 
    + CASE WHEN @pAuthorizeUserId IS NOT NULL THEN ' AND CM_USER_ID =' + RTRIM(LTRIM(CONVERT(VARCHAR, @pAuthorizeUserId, 21))) + '' ELSE '' END + CHAR(10) 
+'GROUP BY   AO_OPERATION_ID
           , PAY.GU_USERNAME
           , CM_USER_NAME
           , CAI_NAME
           , CS_CASHIER_ID
           , CM_USER_ID
           , AO_COMMENT
           , CM_CASHIER_NAME

  ----******************

    SELECT   AO_OPERATION_ID
           , HP_DATETIME               AS DATETIME
           , HP_TE_NAME                AS TERMINAL_NAME
           , HP_TYPE                   AS TRANSACTION_TYPE
           , PAYMENT_USER              AS PAYMENT_USER 
           , AUTHORIZATION_USER        AS AUTHORIZATION_USER
           , ISNULL(HP_AMOUNT,0) 
             - ISNULL(HP_TAX_AMOUNT,0) AS BEFORE_TAX_AMOUNT
           , HP_AMOUNT                 AS AFTER_TAX_AMOUNT
           , REASON                    AS REASON
           , AO_COMMENT                AS COMMENT      
           , 1                         AS ORIGEN
           , HP_TYPE                   AS TYPE1
      FROM   #TEMP_CASHIER_SESSIONS
INNER JOIN   ACCOUNT_MOVEMENTS ON AM_OPERATION_ID = AO_OPERATION_ID
INNER JOIN   HANDPAYS          ON HP_MOVEMENT_ID  = AM_MOVEMENT_ID
     WHERE   1 = 1 '
    + CASE WHEN @pHpMovements IS NOT NULL THEN ' AND HP_TYPE IN (' + @pHpMovements + ') ' ELSE '' END + CHAR(10) 
+ '   AND   HP_STATUS = ' + CAST(@_PAID AS NVARCHAR) + ' 
    UNION
    SELECT   AO_OPERATION_ID
           , CM_DATE              AS DATETIME
           , CM_CASHIER_NAME      AS TERMINAL_NAME
           , CM_TYPE              AS TRANSACTION_TYPE
           , PAYMENT_USER         AS PAYMENT_USER 
           , AUTHORIZATION_USER   AS AUTHORIZATION_USER
           , CHIPS_PURCHASE_TOTAL
           + TITO_TICKET_PAID
           + TOTAL_TAX            AS BEFORE_TAX_AMOUNT
           , CHIPS_PURCHASE_TOTAL
           + TITO_TICKET_PAID
           + TOTAL_TAX            AS AFTER_TAX_AMOUNT
           , REASON               AS REASON
           , AO_COMMENT           AS COMMENT      
           , 2                    AS ORIGEN
           , CM_TYPE              AS TYPE1
      FROM   #TEMP_CASHIER_SESSIONS
     WHERE   1 = 1 '
    + CASE WHEN @pCmMovements IS NOT NULL THEN ' AND CM_TYPE IN (' + ISNULL(@pCmMovements, '0') + ')  ' ELSE '' END + CHAR(10) 
+ '  ORDER   BY DATETIME DESC '
 
+ 'DROP TABLE #TEMP_CASHIER_SESSIONS '

--PRINT @_SQL     
EXEC sp_executesql @_SQL	
   
END  
GO

GRANT EXECUTE ON [dbo].[GetCashierTransactionsTaxes] TO [wggui] WITH GRANT OPTION 
GO

ALTER PROCEDURE [dbo].[PR_collection_by_machine_and_date] 
 ( @pFromDt DATETIME   
  ,@pToDt DATETIME    
  ,@pClosingTime INTEGER
  ,@pMaskStatus INTEGER
  ,@pStatusPaid INTEGER
  ,@pAll		INTEGER
  ,@pTerminalWhere NVARCHAR(MAX) 
  ,@pBillDetail BIT
  ,@pShowMetters BIT
  ,@pOnlyGroupByTerminal BIT
)
AS
BEGIN

  -- DECLARATIONS
   DECLARE @Sql AS VARCHAR(MAX)
   DECLARE @nRows AS INT                              
   DECLARE @index AS INT                              
   DECLARE @tDays AS TABLE(NumDay INT)                       
   DECLARE @Columns AS VARCHAR(MAX)
   DECLARE @ColumnChk AS VARCHAR(MAX)
   DECLARE @p2007Opening AS VARCHAR(MAX)

   SET @p2007Opening  = CAST('2007-01-01T00:00:00' AS DATETIME);
   SET @p2007Opening  = DATEADD(HOUR, @pClosingTime, @p2007Opening)

   SET @nRows  = DATEDIFF(DAY, @pFromDt, @pToDt)+1 
   SET @index = 0                                  

   WHILE @index < @nRows                           
   BEGIN	                                          
     INSERT INTO @tDays VALUES(@index)	                
     SET @index = @index + 1                       
   END 
   
    SET @Columns = ''
    SET @ColumnChk = ''

   SELECT NumDay INTO #TempTable_Days FROM @tDays 
   
   SELECT @Columns = COALESCE(@Columns + '[' + CAST(CGC_DENOMINATION AS NVARCHAR(20)) + '],', '')
     FROM (SELECT   DISTINCT CGC_DENOMINATION 
             FROM   CAGE_CURRENCIES 
            WHERE   CGC_ISO_CODE IN (SELECT   GP_KEY_VALUE 
                                       FROM   GENERAL_PARAMS 
                                      WHERE   GP_GROUP_KEY = 'RegionalOptions' 
                                        AND   GP_SUBJECT_KEY = 'CurrencyISOCode')
              AND   CGC_DENOMINATION >= 1 
              AND   CGC_ALLOWED = 1
                    UNION  
           SELECT   DISTINCT MCD_FACE_VALUE 
             FROM   MONEY_COLLECTION_DETAILS 
            INNER   JOIN MONEY_COLLECTIONS ON MCD_COLLECTION_ID = MC_COLLECTION_ID
            WHERE   MC_COLLECTION_DATETIME >= @pFromDt
              AND   MC_COLLECTION_DATETIME <  @pToDt
              AND   MC_TERMINAL_ID IS NOT NULL 
              AND   MCD_FACE_VALUE >= 1
          ) AS DTM
  ORDER BY CGC_DENOMINATION

  SET @ColumnChk = REPLACE( @Columns, ',', ' IS NOT NULL OR ')

  SET @Columns = LEFT(@Columns,LEN(@Columns)-1)
  SET @ColumnChk = LEFT(@ColumnChk,LEN(@ColumnChk)-3)
     
  SET @Sql = '
   SELECT   TE_PROVIDER_ID 
          , TE_TERMINAL_ID
          , TE_NAME 
          , ISNULL(TE_MULTI_DENOMINATION, ''--'') AS DENOMINATION '
  + CASE WHEN @pOnlyGroupByTerminal = 0 THEN '         
          , ORDER_DATE 
          , DATEADD(DAY, 1, ORDER_DATE) AS ORDER_DATE_FIN
          , COLLECTED_COIN_AMOUNT
          , TICKET_IN_COUNT
          , TI_IN_AMOUNT_RE
          , TI_IN_AMOUNT_NO_RE
          , (ISNULL(TI_IN_AMOUNT_RE,0) + ISNULL(TI_IN_AMOUNT_NO_RE, 0)) AS TI_IN_AMOUNT
          , TICKET_OUT_COUNT
          , TI_OUT_AMOUNT_RE
          , TI_OUT_AMOUNT_NO_RE
          , (ISNULL(TI_OUT_AMOUNT_RE, 0) + ISNULL(TI_OUT_AMOUNT_NO_RE, 0)) AS TI_OUT_AMOUNT
          , ( (ISNULL(TI_OUT_AMOUNT_RE, 0) + ISNULL(TI_OUT_AMOUNT_NO_RE, 0)) - (ISNULL(TI_IN_AMOUNT_RE, 0) + ISNULL(TI_IN_AMOUNT_NO_RE, 0)) ) AS NET_TITO	
          , HAND_PAYS.MANUAL
          , HAND_PAYS.CREDIT_CANCEL
          , HAND_PAYS.JACKPOT_DE_SALA
          , HAND_PAYS.PROGRESIVES
          , HAND_PAYS.NO_PROGRESIVES
          , (ISNULL(HAND_PAYS.MANUAL, 0) + ISNULL(HAND_PAYS.CREDIT_CANCEL, 0) + ISNULL(HAND_PAYS.JACKPOT_DE_SALA, 0) + ISNULL(HAND_PAYS.NO_PROGRESIVES, 0) + ISNULL(PROVISIONS.PROGRESIVE_PROVISIONS, 0)) AS HAND_PAYS_TOTAL
          , PROVISIONS.PROGRESIVE_PROVISIONS
          , (ISNULL(TI_OUT_AMOUNT_RE, 0) - ISNULL(TI_IN_AMOUNT_RE, 0) +
             ISNULL(HAND_PAYS.MANUAL, 0) + ISNULL(HAND_PAYS.CREDIT_CANCEL, 0) + ISNULL(HAND_PAYS.JACKPOT_DE_SALA, 0) + ISNULL(HAND_PAYS.NO_PROGRESIVES, 0) + ISNULL(PROVISIONS.PROGRESIVE_PROVISIONS, 0)) 
              AS TOTAL_WIN_WITHOUT_BILLS
          , 0 AS GAP_BILL
          ' + CASE WHEN @pShowMetters = 1 THEN ', METERS.TOTAL_COIN_IN_CREDITS' ELSE ', 0  AS GAP_METER_0' END + '
          ' + CASE WHEN @pShowMetters = 1 THEN ', METERS.TOTAL_COIN_OUT_CREDITS' ELSE ', 0 AS GAP_METER_1' END + '
          ' + CASE WHEN @pShowMetters = 1 THEN ', METERS.TOTAL_JACKPOT_CREDITS' ELSE ', 0 AS GAP_METER_2' END + '
          ' + CASE WHEN @pShowMetters = 1 THEN ', METERS.TOTAL_HAND_PAID_CANCELLED_CREDITS' ELSE ', 0 AS GAP_METER_3' END + '
          , BILLS.* '
     ELSE '
		  , NULL AS GAP_ORDER_DATE 
          , NULL AS GAP_ORDER_DATE_FIN
          , SUM(COLLECTED_COIN_AMOUNT) AS COLLECTED_COIN_AMOUNT
          , SUM(TICKET_IN_COUNT) AS TICKET_IN_COUNT
          , SUM(TI_IN_AMOUNT_RE) AS TI_IN_AMOUNT_RE
          , SUM(TI_IN_AMOUNT_NO_RE) AS TI_IN_AMOUNT_NO_RE
          , SUM((ISNULL(TI_IN_AMOUNT_RE,0) + ISNULL(TI_IN_AMOUNT_NO_RE, 0))) AS TI_IN_AMOUNT
          , SUM(TICKET_OUT_COUNT) AS TICKET_OUT_COUNT
          , SUM(TI_OUT_AMOUNT_RE) AS TI_OUT_AMOUNT_RE
          , SUM(TI_OUT_AMOUNT_NO_RE) AS TI_OUT_AMOUNT_NO_RE
          , SUM((ISNULL(TI_OUT_AMOUNT_RE, 0) + ISNULL(TI_OUT_AMOUNT_NO_RE, 0))) AS TI_OUT_AMOUNT
          , SUM(( (ISNULL(TI_OUT_AMOUNT_RE, 0) + ISNULL(TI_OUT_AMOUNT_NO_RE, 0)) - (ISNULL(TI_IN_AMOUNT_RE, 0) + ISNULL(TI_IN_AMOUNT_NO_RE, 0))) ) AS NET_TITO	
          , SUM(HAND_PAYS.MANUAL) AS MANUAL
          , SUM(HAND_PAYS.CREDIT_CANCEL) AS CREDIT_CANCEL
          , SUM(HAND_PAYS.JACKPOT_DE_SALA) AS JACKPOT_DE_SALA
          , SUM(HAND_PAYS.PROGRESIVES) AS PROGRESIVES
          , SUM(HAND_PAYS.NO_PROGRESIVES) AS NO_PROGRESIVES
          , SUM((ISNULL(HAND_PAYS.MANUAL, 0) + ISNULL(HAND_PAYS.CREDIT_CANCEL, 0) + ISNULL(HAND_PAYS.JACKPOT_DE_SALA, 0) + ISNULL(HAND_PAYS.NO_PROGRESIVES, 0) + ISNULL(PROVISIONS.PROGRESIVE_PROVISIONS, 0))) AS HAND_PAYS_TOTAL
          , SUM(PROVISIONS.PROGRESIVE_PROVISIONS) AS PROGRESIVE_PROVISIONS
          , SUM((ISNULL(TI_OUT_AMOUNT_RE, 0) - ISNULL(TI_IN_AMOUNT_RE, 0) +
             ISNULL(HAND_PAYS.MANUAL, 0) + ISNULL(HAND_PAYS.CREDIT_CANCEL, 0) + ISNULL(HAND_PAYS.JACKPOT_DE_SALA, 0) + ISNULL(HAND_PAYS.NO_PROGRESIVES, 0) + ISNULL(PROVISIONS.PROGRESIVE_PROVISIONS, 0))) 
              AS TOTAL_WIN_WITHOUT_BILLS
          , 0 AS GAP_BILL
          ' + CASE WHEN @pShowMetters = 1 THEN ', SUM(METERS.TOTAL_COIN_IN_CREDITS) AS TOTAL_COIN_IN_CREDITS' ELSE ', 0  AS GAP_METER_0' END + '
          ' + CASE WHEN @pShowMetters = 1 THEN ', SUM(METERS.TOTAL_COIN_OUT_CREDITS) AS TOTAL_COIN_OUT_CREDITS' ELSE ', 0 AS GAP_METER_1' END + '
          ' + CASE WHEN @pShowMetters = 1 THEN ', SUM(METERS.TOTAL_JACKPOT_CREDITS) AS TOTAL_JACKPOT_CREDITS' ELSE ', 0 AS GAP_METER_2' END + '
          ' + CASE WHEN @pShowMetters = 1 THEN ', SUM(METERS.TOTAL_HAND_PAID_CANCELLED_CREDITS) AS TOTAL_HAND_PAID_CANCELLED_CREDITS' ELSE ', 0 AS GAP_METER_3' END + '
          , NULL AS GAP_BILL_MC_TERMINAL_ID
          , NULL AS GAP_BILL_MC_COLLECTION_DATETIME
          , SUM(BILLS.MCD_FACE_VALUE)
          , SUM(BILLS.COLLECTED) '
     END + '
     FROM   TERMINALS 
 LEFT JOIN (SELECT   DATEADD(DAY, NumDay, CAST(''' + CAST(@pFromDt AS VARCHAR(50)) + ''' AS DATETIME)) AS ORDER_DATE FROM #TempTable_Days ) DIA ON ORDER_DATE <= GETDATE() 
 LEFT JOIN (SELECT   TC_TERMINAL_ID 
                   , DATEADD(HOUR, ' + CAST(@pClosingTime AS VARCHAR(50)) + ', TC_DATE) AS TC_DATE
              FROM   TERMINALS_CONNECTED 
             WHERE   TC_DATE >= dateadd(day, datediff(day, 0, CAST(''' + CAST(@pFromDt AS VARCHAR(50)) + ''' AS DATETIME)), 0)
               AND   TC_DATE <  dateadd(day, datediff(day, 0, CAST(''' + CAST(@pToDt AS VARCHAR(50)) + ''' AS DATETIME)), 0)
               AND   TC_STATUS = 0
               AND   TC_CONNECTED = 1) TC ON TC_TERMINAL_ID = TE_TERMINAL_ID AND TC_DATE = DIA.ORDER_DATE 
 LEFT JOIN (SELECT   MC_TERMINAL_ID AS MC_TERMINAL_ID_C
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), CGS_WORKING_DAY)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS MC_COLLECTION_DATETIME_C
                   , SUM(MC_COLLECTED_COIN_AMOUNT) AS COLLECTED_COIN_AMOUNT
              FROM   MONEY_COLLECTIONS
        INNER JOIN   CAGE_MOVEMENTS ON CGM_MC_COLLECTION_ID = MC_COLLECTION_ID
        INNER JOIN   CAGE_SESSIONS ON CGS_CAGE_SESSION_ID = CGM_CAGE_SESSION_ID
             WHERE   CGS_WORKING_DAY >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
               AND   CGS_WORKING_DAY <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
               AND   MC_TERMINAL_ID IS NOT NULL
             GROUP   BY MC_TERMINAL_ID
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), CGS_WORKING_DAY)/24, CAST(''' + @p2007Opening + ''' AS DATETIME))
           ) CASH_IN_COINS ON TE_TERMINAL_ID = MC_TERMINAL_ID_C AND MC_COLLECTION_DATETIME_C = DIA.ORDER_DATE 
 LEFT JOIN (SELECT   MC_TERMINAL_ID AS MC_TERMINAL_ID_T
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), CGS_WORKING_DAY)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS MC_COLLECTION_DATETIME_T
                   , SUM(MC_EXPECTED_TICKET_COUNT) AS TICKET_IN_COUNT
                   , SUM(ISNULL(MC_EXPECTED_RE_TICKET_AMOUNT, 0)+ISNULL(MC_EXPECTED_PROMO_RE_TICKET_AMOUNT, 0)) AS TI_IN_AMOUNT_RE
                   , SUM(ISNULL(MC_EXPECTED_PROMO_NR_TICKET_AMOUNT, 0)) AS TI_IN_AMOUNT_NO_RE
              FROM   MONEY_COLLECTIONS
        INNER JOIN   CAGE_MOVEMENTS ON CGM_MC_COLLECTION_ID = MC_COLLECTION_ID
        INNER JOIN   CAGE_SESSIONS ON CGS_CAGE_SESSION_ID = CGM_CAGE_SESSION_ID
             WHERE   CGS_WORKING_DAY >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
               AND   CGS_WORKING_DAY <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
               AND   MC_TERMINAL_ID IS NOT NULL
             GROUP   BY MC_TERMINAL_ID
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), CGS_WORKING_DAY)/24, CAST(''' + @p2007Opening + ''' AS DATETIME))
           ) TICKETS_IN ON TE_TERMINAL_ID = MC_TERMINAL_ID_T AND MC_COLLECTION_DATETIME_T = DIA.ORDER_DATE 
LEFT JOIN ( SELECT   TI_CREATED_TERMINAL_ID
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), TI_CREATED_DATETIME)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS TI_CREATED_DATETIME
                   , COUNT(1) AS TICKET_OUT_COUNT
                   , SUM(CASE WHEN TI_TYPE_ID = 2 THEN 0 ELSE TI_AMOUNT END) AS TI_OUT_AMOUNT_RE
                   , SUM(CASE WHEN TI_TYPE_ID = 2 THEN TI_AMOUNT ELSE 0 END) AS TI_OUT_AMOUNT_NO_RE
              FROM   TICKETS 
             WHERE   TI_CREATED_DATETIME >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
               AND   TI_CREATED_DATETIME <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
               AND   TI_CREATED_TERMINAL_TYPE = 1     -- TITO_TERMINAL_TYPE.TERMINAL = 1
               AND   TI_TYPE_ID IN (0,1,2,5) -- TITO_TICKET_TYPE
                                                --CASHABLE = 0,
                                                --PROMO_REDEEM = 1,
                                                --PROMO_NONREDEEM = 2,  // only playable
                                                --HANDPAY = 3,
                                                --JACKPOT = 4,
                                                --OFFLINE = 5
             GROUP   BY TI_CREATED_TERMINAL_ID
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), TI_CREATED_DATETIME)/24, CAST(''' + @p2007Opening + ''' AS DATETIME))
          ) TICKETS_OUT ON TE_TERMINAL_ID = TI_CREATED_TERMINAL_ID AND TI_CREATED_DATETIME = DIA.ORDER_DATE
 LEFT JOIN (SELECT   (CASE WHEN HP_TYPE = 20 THEN HP_SITE_JACKPOT_AWARDED_ON_TERMINAL_ID ELSE HP_TERMINAL_ID END) AS HP_TERMINAL_ID 
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), ISNULL(HP_STATUS_CHANGED, CASE WHEN (HP_TYPE IN ( 20)) THEN HP_DATETIME ELSE NULL END))/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS HP_DATETIME
                   
                   , SUM(CASE WHEN (HP_TYPE IN (10, 1010)) THEN ISNULL(HP_AMOUNT, 0) ELSE 0 END) AS MANUAL 
                   
                   , SUM(CASE WHEN (HP_TYPE IN ( 0, 1000 )) THEN ISNULL(HP_AMOUNT, 0) ELSE 0 END) +
                     SUM(CASE WHEN (HP_TYPE IN ( 1, 1001 ) AND (HP_LEVEL>=1 AND HP_LEVEL<=32) ) THEN 
                         CASE WHEN (HP_TAX_BASE_AMOUNT IS NULL) THEN 0 ELSE ISNULL(HP_AMOUNT, 0) - ISNULL(HP_TAX_BASE_AMOUNT, 0) END ELSE 0 END) AS CREDIT_CANCEL
                         
                   , SUM(CASE WHEN (HP_TYPE IN ( 20)) THEN ISNULL(HP_AMOUNT, 0) ELSE 0 END) AS JACKPOT_DE_SALA 
                   
                   , SUM(CASE WHEN (HP_TYPE IN ( 1, 1001 ) AND (HP_LEVEL>=1 AND HP_LEVEL<=32) AND HP_PROGRESSIVE_ID IS NOT NULL) THEN 
                         CASE WHEN (HP_TAX_BASE_AMOUNT IS NULL) THEN ISNULL(HP_AMOUNT, 0) ELSE ISNULL(HP_TAX_BASE_AMOUNT, 0) END ELSE 0 END) AS PROGRESIVES 
                         
                   , SUM(CASE WHEN (HP_TYPE IN ( 1, 1001 ) AND (HP_LEVEL <1 OR  HP_LEVEL >32 OR HP_LEVEL IS NULL)) THEN ISNULL(HP_AMOUNT, 0) ELSE 0 END) + 
                     SUM(CASE WHEN (HP_TYPE IN ( 1, 1001 ) AND (HP_LEVEL>=1 AND HP_LEVEL <=32 AND HP_PROGRESSIVE_ID IS NULL)) THEN 
                         CASE WHEN (HP_TAX_BASE_AMOUNT IS NULL) THEN ISNULL(HP_AMOUNT, 0) ELSE ISNULL(HP_TAX_BASE_AMOUNT, 0) END ELSE 0 END) AS NO_PROGRESIVES
                   
              FROM   HANDPAYS WITH (INDEX (IX_hp_status_changed))
             WHERE   ' + CASE WHEN @pAll = 0 THEN 'HP_STATUS & ' + CAST(@pMaskStatus AS VARCHAR(10)) + ' = ' + CAST(@pStatusPaid AS VARCHAR(10)) + ' AND ' ELSE + ' ' END +
            '  ISNULL(HP_STATUS_CHANGED, CASE WHEN (HP_TYPE IN ( 20)) THEN HP_DATETIME ELSE NULL END) >= CAST(''' + CAST(@pFromDt AS VARCHAR(50)) + ''' AS DATETIME) 
               AND   ISNULL(HP_STATUS_CHANGED, CASE WHEN (HP_TYPE IN ( 20)) THEN HP_DATETIME ELSE NULL END) < CAST(''' + CAST(@pToDt AS VARCHAR(50)) + ''' AS DATETIME) 
             GROUP   BY (CASE WHEN HP_TYPE = 20 THEN HP_SITE_JACKPOT_AWARDED_ON_TERMINAL_ID ELSE HP_TERMINAL_ID END) 
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), ISNULL(HP_STATUS_CHANGED, CASE WHEN (HP_TYPE IN ( 20)) THEN HP_DATETIME ELSE NULL END))/24, CAST(''' + @p2007Opening + ''' AS DATETIME))
                   ) HAND_PAYS ON HP_TERMINAL_ID = TE_TERMINAL_ID AND HP_DATETIME = DIA.ORDER_DATE
 LEFT JOIN (SELECT   PPT_TERMINAL_ID 
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), PGP_HOUR_FROM)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS PGP_HOUR_FROM
                   , SUM(ISNULL(PPT_AMOUNT, 0)) AS PROGRESIVE_PROVISIONS 
             FROM    PROGRESSIVES_PROVISIONS_TERMINALS 
        LEFT JOIN    PROGRESSIVES_PROVISIONS ON PGP_PROVISION_ID = PPT_PROVISION_ID 
                                            AND PGP_PROGRESSIVE_ID = PPT_PROGRESSIVE_ID 
                                            AND PGP_HOUR_FROM >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
                                            AND PGP_HOUR_FROM <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
                                            AND PGP_STATUS = 0
             GROUP   BY PPT_TERMINAL_ID 
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), PGP_HOUR_FROM)/24, CAST(''' + @p2007Opening + ''' AS DATETIME))
                   ) PROVISIONS ON PPT_TERMINAL_ID = TE_TERMINAL_ID AND PGP_HOUR_FROM = DIA.ORDER_DATE ' 
IF @pBillDetail = 1
BEGIN
  SET @Sql =  @Sql +
'LEFT JOIN (SELECT   *  
              FROM ( SELECT   MC_TERMINAL_ID
                            , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), CGS_WORKING_DAY)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS MC_COLLECTION_DATETIME
                            , MCD_FACE_VALUE
                            , (MCD_NUM_COLLECTED * MCD_FACE_VALUE) AS COLLECTED
                       FROM   MONEY_COLLECTION_DETAILS 
                 INNER JOIN   MONEY_COLLECTIONS ON MCD_COLLECTION_ID = MC_COLLECTION_ID
                 INNER JOIN   CAGE_MOVEMENTS ON CGM_MC_COLLECTION_ID = MC_COLLECTION_ID
                 INNER JOIN   CAGE_SESSIONS ON CGS_CAGE_SESSION_ID = CGM_CAGE_SESSION_ID
                      WHERE   CGS_WORKING_DAY >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
                        AND   CGS_WORKING_DAY <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
                        AND   MC_TERMINAL_ID IS NOT NULL
                   ) PIV
             PIVOT (  SUM(COLLECTED) FOR MCD_FACE_VALUE IN ('+ @Columns  + ')) AS CHILD
            ) BILLS ON TE_TERMINAL_ID = MC_TERMINAL_ID AND MC_COLLECTION_DATETIME = DIA.ORDER_DATE '
END
ELSE
BEGIN
  SET @Sql =  @Sql +
'LEFT JOIN (SELECT   MC_TERMINAL_ID
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), CGS_WORKING_DAY)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS MC_COLLECTION_DATETIME
                   , 0 AS MCD_FACE_VALUE
                   , SUM(ISNULL(MC_COLLECTED_BILL_AMOUNT, 0)) AS COLLECTED
              FROM   MONEY_COLLECTIONS
        INNER JOIN   CAGE_MOVEMENTS ON CGM_MC_COLLECTION_ID = MC_COLLECTION_ID
        INNER JOIN   CAGE_SESSIONS ON CGS_CAGE_SESSION_ID = CGM_CAGE_SESSION_ID
             WHERE   CGS_WORKING_DAY >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
               AND   CGS_WORKING_DAY <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
               AND   MC_TERMINAL_ID IS NOT NULL
             GROUP   BY MC_TERMINAL_ID
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), CGS_WORKING_DAY)/24, CAST(''' + @p2007Opening + ''' AS DATETIME))
          ) BILLS ON TE_TERMINAL_ID = MC_TERMINAL_ID AND MC_COLLECTION_DATETIME = DIA.ORDER_DATE '
END


IF @pShowMetters = 1
BEGIN
SET @Sql =  @Sql +
'LEFT JOIN (SELECT TSMH_TERMINAL_ID
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), TSMH_DATETIME)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS TSMH_DATE
                   , SUM(CASE WHEN TSMH_METER_CODE = 0 THEN ISNULL(CAST(TSMH_METER_INCREMENT AS MONEY)/100, 0) END) AS TOTAL_COIN_IN_CREDITS
                   , SUM(CASE WHEN TSMH_METER_CODE = 1 THEN ISNULL(CAST(TSMH_METER_INCREMENT AS MONEY)/100, 0) END) AS TOTAL_COIN_OUT_CREDITS
                   , SUM(CASE WHEN TSMH_METER_CODE = 2 THEN ISNULL(CAST(TSMH_METER_INCREMENT AS MONEY)/100, 0) END) AS TOTAL_JACKPOT_CREDITS
                   , SUM(CASE WHEN TSMH_METER_CODE = 3 THEN ISNULL(CAST(TSMH_METER_INCREMENT AS MONEY)/100, 0) END) AS TOTAL_HAND_PAID_CANCELLED_CREDITS
			 FROM    TERMINAL_SAS_METERS_HISTORY
			WHERE    TSMH_TYPE = 20
			  AND    TSMH_METER_CODE IN (0,1,2,3)
			  AND    TSMH_METER_INCREMENT > 0 
			  AND    TSMH_DATETIME >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
              AND    TSMH_DATETIME <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
            GROUP    BY TSMH_TERMINAL_ID
				   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), TSMH_DATETIME)/24, CAST(''' + @p2007Opening + ''' AS DATETIME))
	   ) METERS ON TE_TERMINAL_ID = TSMH_TERMINAL_ID AND TSMH_DATE = DIA.ORDER_DATE '
END

SET @Sql =  @Sql + 'WHERE ( ' 

IF @pBillDetail = 1
BEGIN
  SET @Sql =  @Sql + @ColumnChk 
END
ELSE
BEGIN
  SET @Sql =  @Sql + ' BILLS.COLLECTED IS NOT NULL ' 
END

SET @Sql =  @Sql +
  ' OR TICKET_IN_COUNT IS NOT NULL
    OR TI_IN_AMOUNT_RE IS NOT NULL
    OR TI_IN_AMOUNT_NO_RE IS NOT NULL
    OR TICKET_OUT_COUNT IS NOT NULL
    OR TI_OUT_AMOUNT_RE IS NOT NULL
    OR TI_OUT_AMOUNT_NO_RE IS NOT NULL
    OR HAND_PAYS.MANUAL IS NOT NULL
    OR HAND_PAYS.CREDIT_CANCEL IS NOT NULL
    OR HAND_PAYS.JACKPOT_DE_SALA IS NOT NULL
    OR HAND_PAYS.PROGRESIVES IS NOT NULL
    OR HAND_PAYS.NO_PROGRESIVES IS NOT NULL
    OR PROVISIONS.PROGRESIVE_PROVISIONS IS NOT NULL' +
	CASE WHEN @pShowMetters = 1 THEN '
		OR METERS.TOTAL_COIN_IN_CREDITS IS NOT NULL
		OR METERS.TOTAL_COIN_OUT_CREDITS IS NOT NULL
		OR METERS.TOTAL_JACKPOT_CREDITS IS NOT NULL
		OR METERS.TOTAL_HAND_PAID_CANCELLED_CREDITS IS NOT NULL'
    ELSE '' END + '
    OR TC_DATE IS NOT NULL
    )' + CAST(@pTerminalWhere AS Varchar(max)) +
CASE WHEN @pOnlyGroupByTerminal = 1 THEN 
' GROUP   BY TE_PROVIDER_ID 
        , TE_TERMINAL_ID
        , TE_NAME 
        , ISNULL(TE_MULTI_DENOMINATION, ''--'') '
ELSE '' END +
' ORDER   BY TE_PROVIDER_ID 
        , TE_TERMINAL_ID 
        , TE_NAME 
        , ISNULL(TE_MULTI_DENOMINATION, ''--'') ' +
        
CASE WHEN @pOnlyGroupByTerminal = 0 THEN 
        ', ORDER_DATE ' 
ELSE '' END

 -- PRINT @Sql

  EXECUTE (@Sql)

  DROP TABLE #TempTable_Days

END -- END PROCEDURE
GO

--****************
-- Historia 5277 HighRollers
--****************
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CustomersPlaying]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[CustomersPlaying]
GO

CREATE PROCEDURE [dbo].[CustomersPlaying]
AS
BEGIN
  -- SET NOCOUNT ON added to prevent extra result sets from
  -- interfering with SELECT statements.
  SET NOCOUNT ON;

  -- TERMINALES
SELECT   TE_TERMINAL_ID
       , TE_PROV_ID
       , ISNULL(PV_NAME,'') AS PV_NAME
       , ISNULL(TE_NAME,'') AS TE_NAME
       , ISNULL(TE_FLOOR_ID,'') AS TE_FLOOR_ID
       , ISNULL(BK_AREA_ID, -1) AS BK_AREA_ID --'AREA'
       , ISNULL(AR_NAME,'') AS AR_NAME
       , ISNULL(TE_BANK_ID, -1) AS TE_BANK_ID --'ISLA'
       , ISNULL(BK_NAME,'') AS BK_NAME
       , ISNULL(TE_POSITION, -1) AS TE_POSITION --'POSICION'
         -- JUGADORES
       , AC_ACCOUNT_ID
       , ISNULL(AC_HOLDER_NAME,'') AS AC_HOLDER_NAME
       , ISNULL(AC_TRACK_DATA,'') AS AC_TRACK_DATA
       , AC_HOLDER_LEVEL AS AC_HOLDER_LEVEL
       , (SELECT   GP_KEY_VALUE     
            FROM   GENERAL_PARAMS   
           WHERE   GP_GROUP_KEY   = 'PlayerTracking'  
             AND   GP_SUBJECT_KEY = 'Level' + RIGHT('0' + CAST(AC_HOLDER_LEVEL AS NVARCHAR), 2) + '.Name' 
            ) AS AC_LEVEL             		
       , AC_HOLDER_IS_VIP	 
         -- SESION DE JUEGO
       , PS1.PS_PLAY_SESSION_ID
       , PS1.PS_STARTED
       , DATEDIFF(SECOND, PS1.PS_STARTED, ISNULL(PS1.PS_FINISHED, GETDATE())) AS PS1PS_DURATION
       , PS1.PS_PLAYED_COUNT
       , PS1.PS_PLAYED_AMOUNT
       , CASE WHEN  PS1.PS_PLAYED_COUNT = 0 THEN ''
              ELSE ROUND((PS1.PS_PLAYED_AMOUNT / PS1.PS_PLAYED_COUNT), 2) END AS PS_AVERAGE_BET -- 'APUESTA MEDIA'             
         -- CLIENTE JORNADA
       , WD.DURATION AS WD_DURATION
       , WD.PLAYED_COUNT AS WD_PLAYED_COUNT
       , WD.PLAYED_AMOUNT AS WD_PLAYED_AMOUNT
       , WD.PS_AVERAGE_BET AS WD_AVERAGE_BET
       , AC_TYPE
       , AC_USER_TYPE -- GMV 27-10-2015 52755 - HighRollers
       , PS1.PS_FINISHED -- GMV 27-10-2015 52755 - HighRollers
       , ISNULL(TS_PLAYED_WON_FLAGS,0) AS PLAYED_WON_FLAGS -- GMV 27-10-2015 52755 - HighRollers
FROM
(SELECT   PS.PS_ACCOUNT_ID
        , PS.DURATION
        , PS.PLAYED_COUNT
        , PS.PLAYED_AMOUNT
        , CASE WHEN  PS.PLAYED_COUNT = 0 THEN NULL
               ELSE ROUND((PS.PLAYED_AMOUNT / PS.PLAYED_COUNT), 2) END AS PS_AVERAGE_BET -- 'APUESTA MEDIA'             
   FROM
  (SELECT   PS_ACCOUNT_ID
          , SUM(DATEDIFF(SECOND, PS_STARTED, ISNULL(PS_FINISHED, GETDATE()))) AS DURATION
          , SUM(PS_PLAYED_COUNT) AS PLAYED_COUNT
          , SUM(PS_PLAYED_AMOUNT) AS PLAYED_AMOUNT
     FROM PLAY_SESSIONS
    WHERE PS_STARTED >= dbo.TodayOpening(0) 
    GROUP BY PS_ACCOUNT_ID) AS PS) AS WD, 
 TERMINALS
INNER JOIN PROVIDERS ON TE_PROV_ID = PV_ID
INNER JOIN BANKS ON TE_BANK_ID = BK_BANK_ID
INNER JOIN AREAS ON BK_AREA_ID = AR_AREA_ID
INNER JOIN PLAY_SESSIONS PS1 ON TE_CURRENT_PLAY_SESSION_ID = PS_PLAY_SESSION_ID
INNER JOIN ACCOUNTS ON PS_ACCOUNT_ID = AC_ACCOUNT_ID
LEFT JOIN  TERMINAL_STATUS ON   TE_TERMINAL_ID = TS_TERMINAL_ID 
WHERE   TE_CURRENT_PLAY_SESSION_ID IS NOT NULL
  AND   WD.PS_ACCOUNT_ID = AC_ACCOUNT_ID
  
END
GO

GRANT EXECUTE ON [dbo].[CustomersPlaying] TO [wggui] WITH GRANT OPTION
GO

ALTER PROCEDURE [dbo].[GT_Chips_Operations] 
(
	@pDateFrom					DATETIME
	,@pDateTo						DATETIME
	,@pStatus						INTEGER
	,@pArea							VARCHAR(50)
	,@pBank							VARCHAR(50)
	,@pOnlyTables				INTEGER
	,@pCashierGroupName VARCHAR(50)
	,@pValidTypes				VARCHAR(4096)
)
AS
BEGIN
----------------------------------------------------------------------------------------------------------------
DECLARE @_DATE_FROM									AS DATETIME
DECLARE @_DATE_TO										AS DATETIME
DECLARE @_STATUS										AS INTEGER
DECLARE @_AREA											AS VARCHAR(4096)
DECLARE @_BANK											AS VARCHAR(4096)
DECLARE @_DELIMITER									AS CHAR(1)
DECLARE @_ONLY_TABLES								AS INTEGER
DECLARE @_CASHIERS_NAME							AS VARCHAR(50)
DECLARE @_TYPES_TABLE								TABLE(SST_ID INT, SST_VALUE VARCHAR(50)) -- TO STORE THE VALID GAMING TABLE TYPES 
DECLARE @_TYPE_CHIPS_SALE_TOTAL			AS INT
DECLARE @_TYPE_CHIPS_PURCHASE_TOTAL	AS INT
----------------------------------------------------------------------------------------------------------------

-- Initialzitation --
SET @_TYPE_CHIPS_SALE_TOTAL				= 303
SET @_TYPE_CHIPS_PURCHASE_TOTAL		= 304

SET @_DELIMITER   =   ','
SET @_DATE_FROM   =   @pDateFrom
SET @_DATE_TO     =   @pDateTo
SET @_STATUS      =   ISNULL(@pStatus, -1)
SET @_AREA        =   ISNULL(@pArea, '')
SET @_BANK        =   ISNULL(@pBank, '')
SET @_ONLY_TABLES =   ISNULL(@pOnlyTables, 1)

IF ISNULL(@_CASHIERS_NAME,'') = '' BEGIN
	SET @_CASHIERS_NAME  = '---CASHIER---'
END

----------------------------------------------------------------------------------------------------------------
-- CHECK DATE PARAMETERS
IF @_DATE_FROM IS NULL
BEGIN
   -- IF DATE FROM IS NULL, USER FIRST POSIBLE DATE
   SET @_DATE_FROM = CAST('' AS DATETIME)
END
IF @_DATE_TO IS NULL
BEGIN
   -- IF DATE TO IS NULL, QUERY UNTIL CURRENT DATE
   SET @_DATE_TO = CAST(GETDATE() AS DATETIME)
END

-- ASSIGN TYPES PARAMETER INTO TABLE
INSERT INTO @_TYPES_TABLE SELECT * FROM dbo.SplitStringIntoTable(@pValidTypes, @_DELIMITER, DEFAULT)

----------------------------------------------------------------------------------------------------------------
-- MAIN QUERY

SELECT 0 AS TYPE_SESSION
	, CS_SESSION_ID AS SESSION_ID
	, GT_NAME AS GT_NAME
	, CS_OPENING_DATE AS SESSION_DATE
	, GTT_NAME AS GTT_NAME
	, SUM(CM_SUB_AMOUNT) AS GTS_TOTAL_SALES_AMOUNT
	, SUM(CM_ADD_AMOUNT) AS GTS_TOTAL_PURCHASE_AMOUNT
	, SUM(GTS_INITIAL_CHIPS_AMOUNT) + SUM(CM_SUB_AMOUNT) - SUM(CM_ADD_AMOUNT) + SUM(GTS_FINAL_CHIPS_AMOUNT) AS DELTA
INTO   #CHIPS_OPERATIONS_TABLE           
FROM   CASHIER_MOVEMENTS
	INNER JOIN CASHIER_SESSIONS       ON CS_SESSION_ID            = CM_SESSION_ID
	INNER JOIN CASHIER_TERMINALS      ON CM_CASHIER_ID            = CT_CASHIER_ID
	LEFT JOIN GAMING_TABLES_SESSIONS  ON GTS_CASHIER_SESSION_ID   = CM_SESSION_ID
	INNER JOIN GAMING_TABLES GT       ON GTS_GAMING_TABLE_ID      = GT_GAMING_TABLE_ID
	INNER JOIN GAMING_TABLES_TYPES    ON GTT_GAMING_TABLE_TYPE_ID = GT_TYPE_ID 
WHERE   GT_ENABLED = CASE WHEN @_STATUS = -1 THEN GT_ENABLED ELSE @_STATUS END
	AND GT_AREA_ID = CASE WHEN @_AREA   = '' THEN GT_AREA_ID ELSE @_AREA END
	AND GT_BANK_ID = CASE WHEN @_BANK   = '' THEN GT_BANK_ID ELSE @_BANK END
	AND GT_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
	AND CM_DATE >= @_DATE_FROM
	AND CM_DATE <  @_DATE_TO
	AND GT_HAS_INTEGRATED_CASHIER = 1
	AND CM_TYPE IN (@_TYPE_CHIPS_SALE_TOTAL, @_TYPE_CHIPS_PURCHASE_TOTAL)  -- 303 = CHIPS_SALE_TOTAL; 304 = CHIPS_PURCHASE_TOTAL
GROUP BY GT_NAME, GTT_NAME, CS_SESSION_ID, CS_OPENING_DATE
ORDER BY GTT_NAME
  
-- Check if cashiers must be visible  
IF @_ONLY_TABLES = 0
BEGIN

-- Select and join data to show cashiers
-- Adding cashiers without gaming tables

INSERT INTO #CHIPS_OPERATIONS_TABLE
	SELECT 1 AS TYPE_SESSION
		, CM_SESSION_ID AS SESSION_ID
		, CT_NAME as GT_NAME
		, CS_OPENING_DATE AS SESSION_DATE
		, @_CASHIERS_NAME as GTT_NAME
		, SUM(CM_SUB_AMOUNT) AS GTS_TOTAL_SALES_AMOUNT
		, SUM(CM_ADD_AMOUNT) AS GTS_TOTAL_PURCHASE_AMOUNT
		, SUM(ISNULL(GTS_INITIAL_CHIPS_AMOUNT,0)) + SUM(ISNULL(CM_SUB_AMOUNT,0)) - SUM(ISNULL(CM_ADD_AMOUNT,0)) + SUM(ISNULL(GTS_FINAL_CHIPS_AMOUNT,0)) AS DELTA
	FROM   CASHIER_MOVEMENTS
		INNER JOIN CASHIER_SESSIONS       ON CS_SESSION_ID          = CM_SESSION_ID
		INNER JOIN CASHIER_TERMINALS      ON CM_CASHIER_ID          = CT_CASHIER_ID
		LEFT JOIN  GAMING_TABLES_SESSIONS ON GTS_CASHIER_SESSION_ID = CM_SESSION_ID
    WHERE GTS_CASHIER_SESSION_ID IS NULL -- We want the chips operations that are from cashiers that are not gambling tables
		AND CM_TYPE IN (@_TYPE_CHIPS_SALE_TOTAL, @_TYPE_CHIPS_PURCHASE_TOTAL)  -- 303 = CHIPS_SALE_TOTAL; 304 = CHIPS_PURCHASE_TOTAL
		AND CM_DATE >= @_DATE_FROM
		AND CM_DATE <  @_DATE_TO
	GROUP BY CT_NAME, CM_SESSION_ID, CS_OPENING_DATE

END
-- Select results
SELECT * FROM #CHIPS_OPERATIONS_TABLE ORDER BY GTT_NAME,GT_NAME
  
-- DROP TEMPORARY TABLE  
DROP TABLE #CHIPS_OPERATIONS_TABLE
  
END  -- END PROCEDURE

GO

/************** LottoRace ******************/

-- GENERAL PARAM
IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='GameGateway' AND GP_SUBJECT_KEY = 'Enabled')
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('GameGateway', 'Enabled', '0')

IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='GameGateway' AND GP_SUBJECT_KEY = 'Provider')
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('GameGateway', 'Provider', '001')

IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='GameGateway' AND GP_SUBJECT_KEY = 'Log.Mode')
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('GameGateway', 'Log.Mode', '0')

IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='GameGateway' AND GP_SUBJECT_KEY = 'Name')
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('GameGateway', 'Name', 'BonoPlay')
   
IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='GameGateway' AND GP_SUBJECT_KEY = 'Uri')
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('GameGateway', 'Uri', 'http://+:7777/')

IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='GameGateway' AND GP_SUBJECT_KEY = 'Reserved.Enabled')
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('GameGateway', 'Reserved.Enabled', '0')
            
IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='GameGateway' AND GP_SUBJECT_KEY = 'Provider.001.Name')
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('GameGateway', 'Provider.001.Name', 'BonoPLAY')
   
IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='GameGateway' AND GP_SUBJECT_KEY = 'Provider.001.Enabled')
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('GameGateway', 'Provider.001.Enabled', '0')   
   
IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='GameGateway' AND GP_SUBJECT_KEY = 'Provider.001.Uri')
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('GameGateway', 'Provider.001.Uri', 'http://www.xxxxx.xxx')
   
IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='GameGateway' AND GP_SUBJECT_KEY = 'Provider.001.PartnerId')
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('GameGateway', 'Provider.001.PartnerId', '001')
   
IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='GameGateway' AND GP_SUBJECT_KEY = 'Provider.001.Password')
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('GameGateway', 'Provider.001.Password', 'password')
   
IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='GameGateway' AND GP_SUBJECT_KEY = 'Provider.001.AccessMethod')
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('GameGateway', 'Provider.001.AccessMethod', '1')
   
IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='GameGateway' AND GP_SUBJECT_KEY = 'Provider.001.CreditTimeOut')
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('GameGateway', 'Provider.001.CreditTimeOut', '10')               

IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='GameGateway' AND GP_SUBJECT_KEY = 'Provider.001.ShowNewGameMessage')
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('GameGateway', 'Provider.001.ShowNewGameMessage', '1')  
            
-- GAMEGATEWAY_GAMES
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[gamegateway_games]') AND type in (N'U'))
DROP TABLE gamegateway_games
GO

CREATE TABLE [dbo].[gamegateway_games](
	[gg_game_id]          [bigint] NOT NULL,
	[gg_partner_id]       [int] NOT NULL,
	[gg_name]             [nvarchar](50) NULL,
	[gg_name_translated]  [nvarchar](50) NULL,
	[gg_logo]             [nvarchar] (200) NULL,
	[gg_show_lcd_message] [bit]   DEFAULT 1 NOT NULL,
  [gg_played]           [money] DEFAULT 0 NOT NULL,
  [gg_won]              [money] DEFAULT 0 NOT NULL,
  [gg_won_jackpot]      [money] DEFAULT 0 NOT NULL,
  [gg_num_bets]         [int]   DEFAULT 0 NOT NULL,
  [gg_num_prizes]       [int]   DEFAULT 0 NOT NULL,
  [gg_created]          [datetime]  NOT NULL,
  [gg_last_updated]     [timestamp] NOT NULL,
 CONSTRAINT [PK_gamegateway_games] PRIMARY KEY CLUSTERED 
(
	[gg_game_id] ASC, [gg_partner_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

-- GAMEGATEWAY_GAME_INSTANCES
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[gamegateway_game_instances]') AND type in (N'U'))
DROP TABLE gamegateway_game_instances
GO

CREATE TABLE [dbo].[gamegateway_game_instances](
	[ggi_game_instance_id] [bigint] NOT NULL,
	[ggi_partner_id]       [int] NOT NULL,	
	[ggi_game_id]          [bigint] NOT NULL,	
	[ggi_first_prize]      [money] NULL,
	[ggi_entry_cost]       [money] NULL,
	[ggi_jackpot]          [money] NULL,	
  [ggi_played]           [money] DEFAULT 0 NOT NULL,
  [ggi_won]              [money] DEFAULT 0 NOT NULL,
  [ggi_won_jackpot]      [money] DEFAULT 0 NOT NULL,
  [ggi_num_bets]         [int]   DEFAULT 0 NOT NULL,
  [ggi_num_prizes]       [int]   DEFAULT 0 NOT NULL,
  [ggi_created]          [datetime]  NOT NULL,
  [ggi_starts]           [datetime]  NULL,
  [ggi_finished]         [datetime]  NULL,
  [ggi_last_updated]     [timestamp] NOT NULL
 CONSTRAINT [PK_gamegateway_game_instances] PRIMARY KEY CLUSTERED 
(
	[ggi_game_instance_id] ASC, [ggi_partner_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

-- GAMEGATEWAY_LOG
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[gamegateway_log]') AND type in (N'U'))
DROP TABLE gamegateway_log
GO

CREATE TABLE [dbo].[gamegateway_log](
	[gl_id]       [bigint] IDENTITY(1,1) NOT NULL,
	[gl_request]  [xml] NULL,
	[gl_response] [xml] NULL,
	[gl_datetime] [datetime] DEFAULT GETDATE() NOT NULL
 CONSTRAINT [PK_gamegateway_log] PRIMARY KEY CLUSTERED 
(
	[gl_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

-- GAMEGATEWAY_BULK
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[gamegateway_bulk]') AND type in (N'U'))
DROP TABLE gamegateway_bulk
GO

--CREATE TABLE [dbo].[gamegateway_bulk](
--	[gb_id]               [bigint] IDENTITY(1,1) NOT NULL,
--	[gb_user_id]          [bigint]   NOT NULL,
--	[gb_amount]           [money]    NOT NULL,
--	[gb_jackpot]          [money]    NOT NULL,
--	[gb_num_credits]      [int]      NOT NULL,
--	[gb_type]             [int]      NOT NULL,
--	[gb_game_id]          [bigint]   NOT NULL,
--	[gb_game_instance_id] [bigint]   NOT NULL,
--	[gb_partner_id]       [int]      NOT NULL,
--	[gb_created]          [datetime] NOT NULL,
--	[gb_execution]        [datetime] NULL
-- CONSTRAINT [PK_gamegateway_bulk] PRIMARY KEY CLUSTERED 
--(
--	[gb_id] ASC
--)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
--) ON [PRIMARY]
--GO

-- TERMINAL BONOPLUS
IF NOT EXISTS (SELECT * FROM terminals WHERE te_terminal_type = 109)
BEGIN
INSERT INTO [dbo].[terminals]
           ([te_type]
           ,[te_base_name]
           ,[te_external_id]
           ,[te_blocked]
           ,[te_active]
           ,[te_provider_id]
           ,[te_terminal_type]
           ,[te_status]
           ,[te_master_id])
     VALUES (1
            ,'BONOPLAY'
            ,'BONOPLAY'
            ,0
            ,1
            ,'CASINO'
            ,109
            ,0
            ,1);
 
UPDATE [dbo].[terminals]
   SET [te_master_id] = [te_terminal_id]
 WHERE [te_terminal_type] = 109;
END
ELSE            
SELECT '***** Record terminals BONOPLAY already exists *****';

IF NOT EXISTS (SELECT * FROM TERMINAL_TYPES WHERE TT_TYPE = 109)
BEGIN
  INSERT INTO   TERMINAL_TYPES 
              ( TT_TYPE, TT_NAME)
       VALUES ( 109    , 'BONOPLAY')
END
GO

-- NEW FIELD RESERVED IN ACCOUNTS
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[accounts]') and name = 'ac_re_reserved')
BEGIN
  ALTER TABLE dbo.accounts ADD ac_re_reserved money NOT NULL DEFAULT ((0))
END
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_accounts_ac_re_balance1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[accounts] DROP CONSTRAINT [DF_accounts_ac_re_balance1]
END

--IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_accounts_ac_re_balance]') AND type = 'D')
--BEGIN
--ALTER TABLE [dbo].[accounts] DROP CONSTRAINT [DF_accounts_ac_re_balance]
--END

IF  NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_accounts_ac_re_balance]') AND type = 'D')
ALTER TABLE [dbo].[accounts] ADD  CONSTRAINT [DF_accounts_ac_re_balance]  DEFAULT ((0)) FOR [ac_re_balance]
GO

-- NEW FIELD MODE_RESERVED IN ACCOUNTS
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[accounts]') and name = 'ac_mode_reserved')
BEGIN
  ALTER TABLE dbo.accounts ADD ac_mode_reserved bit NOT NULL DEFAULT ((0))
END
GO

-- GAMEGATEWAY_TERMINAL_LIST
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[gamegateway_terminal_list]') AND type in (N'U'))
DROP TABLE gamegateway_terminal_list
GO

CREATE TABLE [dbo].[gamegateway_terminal_list]
(gtl_terminal_list XML)
GO

INSERT INTO [dbo].[gamegateway_terminal_list] (gtl_terminal_list)
VALUES (null);

-- NEW FIELD FINISHED IN GAME INSTANCES
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[gamegateway_game_instances]') and name = 'ggi_finished')
BEGIN
  ALTER TABLE dbo.gamegateway_game_instances ADD	ggi_finished datetime NULL
END
GO

-- GAME_GATEWAY_BETS
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[gamegateway_bets]') AND type in (N'U'))
DROP TABLE gamegateway_bets
GO

CREATE TABLE [dbo].[gamegateway_bets](
	[gb_game_id] [bigint] NOT NULL,
	[gb_game_instance_id] [bigint] NOT NULL,
	[gb_transaction_type] [int] NOT NULL,
	[gb_transaction_id] [nvarchar](40) NOT NULL,
	[gb_account_id] [bigint] NOT NULL,
	[gb_partner_id] [int] NOT NULL,
	[gb_created] [datetime] NOT NULL,
	[gb_num_bets] [int] NOT NULL,
	[gb_total_bet] [money] NOT NULL,
	[gb_num_prizes] [int] NOT NULL,
	[gb_total_prize] [money] NOT NULL,
	[gb_jackpot_prize] [money] NOT NULL,
	[gb_related_ps_id] [bigint] NULL,
	[gb_related_mv_id] [bigint] NULL,
	[gb_last_updated]  [timestamp] NOT NULL,
 CONSTRAINT [PK_gamegateway_bets_1] PRIMARY KEY CLUSTERED 
(
	[gb_game_id] ASC,
	[gb_game_instance_id] ASC,
	[gb_partner_id] ASC,
	[gb_transaction_type] ASC,
	[gb_transaction_id] ASC,
	[gb_account_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

/********************************/

/*************** SmartFloor ****************/

/* ---------------- GENERAL PARAMS ---------------- */

IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='HistoricalData' AND GP_SUBJECT_KEY = 'HistorificationTasks.DateStart')
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('HistoricalData', 'HistorificationTasks.DateStart', '20150101')
   
IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='HistoricalData' AND GP_SUBJECT_KEY = 'HistorificationTasks.Enabled')
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('HistoricalData', 'HistorificationTasks.Enabled', '0')
   
IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='HistoricalData' AND GP_SUBJECT_KEY = 'HistorificationTasks.NumDaysProcessed')
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('HistoricalData', 'HistorificationTasks.NumDaysProcessed', '30')
GO

/* ----------------   PROCEDURES   ---------------- */

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'SmartFloor_PVH') AND type IN (N'P', N'PC' ))
  DROP PROCEDURE [dbo].[SmartFloor_PVH]
GO
  
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'SmartFloor_TVH') AND type IN (N'P', N'PC' ))
  DROP PROCEDURE [dbo].[SmartFloor_TVH]
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'SP_Update_Meter_Data_Site_Calc') AND type IN (N'P', N'PC' ))
  DROP PROCEDURE [dbo].[SP_Update_Meter_Data_Site_Calc]
GO
  
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'SP_Update_Meter_Data_Site') AND type IN (N'P', N'PC' ))
  DROP PROCEDURE [dbo].[SP_Update_Meter_Data_Site]
GO
 
/* */
  
CREATE PROCEDURE [dbo].[SmartFloor_PVH]
  @DateStart       DATETIME,
  @DateEnd         DATETIME,
  @IsTitoMode      BIT
  
  -- JRC 21-SEP-2015 Reads and Inserts or Updates as needed into H_PVH

AS
BEGIN
  
  DECLARE @ROW_COUNT INT 
  SET @ROW_COUNT= 0
  
  -- ACCOUNTS
     SELECT   ACCOUNT_INFO.AC_ACCOUNT_ID                                            AS PVH_ACCOUNT_ID 
            , CAST(CONVERT(NVARCHAR(8),@DateEnd,112) AS INTEGER)                    AS PVH_DATE
            , DATEPART(DW,@DateEnd)                                                 AS PVH_WEEKDAY
            , 1                                                                     AS PVH_VISIT
            
            -- INFO CHECK-IN, GAME TIME, ROOM TIME, etc... BY PLAYER.
            , CASE WHEN ACCOUNT_MOV.[FIRST MOV] < PLAY_SES.STARTSESSION THEN
                ACCOUNT_MOV.[FIRST MOV]
              ELSE
                PLAY_SES.STARTSESSION
              END                                                                   AS PVH_CHECK_IN               -- Fecha en la que ha entrado el player al casino
            , CASE WHEN ACCOUNT_MOV.[LAST MOV] > PLAY_SES.LASTSESSION THEN
                ACCOUNT_MOV.[LAST MOV]
              ELSE
                PLAY_SES.LASTSESSION
              END                                                                   AS PVH_CHECK_OUT              -- Fecha en la que ha ha salido el player del casino
            , DATEDIFF(MINUTE, 
                CASE WHEN ACCOUNT_MOV.[FIRST MOV] < PLAY_SES.STARTSESSION THEN
                  ACCOUNT_MOV.[FIRST MOV]
                ELSE
                  PLAY_SES.STARTSESSION
                END,
                CASE WHEN ACCOUNT_MOV.[LAST MOV] > PLAY_SES.LASTSESSION THEN
                  ACCOUNT_MOV.[LAST MOV]
                ELSE
                  PLAY_SES.LASTSESSION
                END)                                                                AS PVH_ROOM_TIME              -- Tiempo (minutos) que est� el "player" dentro del Casino
            
            -- INFO TOTAL PLAYED, JACKPOTS WON, etc...    
            , PLAY_SES.GAME_TIME                                                    AS PVH_GAME_TIME              -- Tiempo de Juego (minutos) que lleva el player jugando en la m�quina
            , PLAY_SES.TOTAL_PLAYED_COUNT                                           AS PVH_TOTAL_PLAYED_COUNT     -- Num Jugadas (count)
            , PLAY_SES.TOTAL_WON_COUNT                                              AS PVH_PLAYED_WON_COUNT       -- Num Jugadas Ganadas (count)
            , PLAY_SES.TOTAL_PLAYED                                                 AS PVH_TOTAL_PLAYED           -- Total Monto Jugado
            , CASE WHEN PLAY_SES.TOTAL_PLAYED_COUNT > 0 THEN 
                ROUND(PLAY_SES.TOTAL_PLAYED / PLAY_SES.TOTAL_PLAYED_COUNT,2) 
              ELSE 
                0 
              END                                                                   AS PVH_TOTAL_BET_AVG          -- Apuesta Media (Total Jugado / Num Jugadas)
            , JACKPOTS.HP_AMOUNT                                                    AS PVH_JACKPOTS_WON           -- JACKPOTS Won (amount)
            
            , ISNULL(ACCOUNT_MOV.REFUNDS,0) + 
              ISNULL(ACCOUNT_MOV.PRIZE,0) +                                                                     
              ISNULL(JACKPOTS.HP_AMOUNT,0)                                          AS PVH_TOTAL_WON              -- MONEY_OUT + JACKPOTS
              
            -- INFO TOTAL PLAYED (REEDEMABLE AND NOT REEDEMABLE)...   
            , PLAY_SES.TOTAL_PLAYED_REDEEM                                          AS PVH_RE_PLAYED              -- Total Jugado Redimible
            , PLAY_SES.TOTAL_PLAYED_NON_REDEEM                                      AS PVH_NR_PLAYED              -- Total Jugado NO Redimible
            
            -- INFO THEORICAL WON
            , POS.PO_THEO_WON                                                       AS PVH_THEORICAL_WON          -- Te�rico Ganado (Jugado * PayOut Te�rico de la m�quina)
            
            -- INFO MONEY IN, MONEY OUT, JACKPOTS, TAX, etc...
            , CASE WHEN (@IsTitoMode = 1) THEN 
                ISNULL(ACCOUNT_MOV.[DEPOSIT A],0) + 
                ISNULL(ACCOUNT_MOV.[PRIZE COUPON],0) + 
                ISNULL(PLAY_SES.BILL_IN,0)
              ELSE 
                ISNULL(ACCOUNT_MOV.[DEPOSIT A],0) + 
                ISNULL(ACCOUNT_MOV.[PRIZE COUPON],0)
              END                                                                   AS PVH_MONEY_IN               -- CASH IN (Recargas / Bill In)
              
            , ISNULL(ACCOUNT_MOV.[MONEY IN TAX],0)                                  AS PVH_MONEY_IN_TAX           -- CASH IN TAX
            
            , ISNULL(ACCOUNT_MOV.REFUNDS,0) + 
              ISNULL(ACCOUNT_MOV.PRIZE,0)                                           AS PVH_MONEY_OUT              -- TOTAL DINERO RETIRADO (BRUTO)
            
            , PLAY_SES.TOTAL_RE_WON_AMOUNT                                          AS PVH_RE_WON
            , PLAY_SES.TOTAL_NR_WON_AMOUNT                                          AS PVH_NR_WON
            , ACCOUNT_MOV.REFUNDS                                                   AS PVH_DEVOLUTION             -- DEVOLUCI�N
            , ACCOUNT_MOV.PRIZE                                                     AS PVH_PRIZE                  -- PREMIO
            , ACCOUNT_MOV.[TAX ON PRIZE 1]                                          AS PVH_TAX1                   -- TAX 1 (PREMIO)
            , ACCOUNT_MOV.[TAX ON PRIZE 2]                                          AS PVH_TAX2                   -- TAX 2 (PREMIO)
            , ACCOUNT_MOV.NUM_RECHARGES                                             AS PVH_MONEY_IN_COUNT         -- CAHSLESS (NUM RECARGAS) | TITO (NUM BILL IN) 
            , ACCOUNT_MOV.NUM_REFUNDS                                               AS PVH_MONEY_OUT_COUNT        -- NUM RETIROS DE CAJA O COBRO DE TICKETS
            
            -- INFO PLAYER (AGE, LEVEL, NUM REGISTERED DAYS, etc...)
            , ISNULL(DATEDIFF(YEAR,AC_HOLDER_BIRTH_DATE, GETDATE()),0)              AS PVH_AGE                    
            , DATEDIFF(DAY,AC_CREATED, GETDATE())                                    AS PVH_REGISTERED_NUM_DAYS
            , ACCOUNT_INFO.AC_HOLDER_LEVEL                                          AS PVH_LEVEL
       INTO #TEMP_ACCOUNT_INFO
     FROM   ACCOUNTS AS ACCOUNT_INFO
      
  -- PLAY SESSIONS
    INNER JOIN 
        (                                                     
        SELECT    PS_ACCOUNT_ID                                
              , MIN(PS_STARTED)                                               AS   STARTSESSION               -- Primera play sessi�n
              , MAX(PS_FINISHED)                                              AS   LASTSESSION                -- �ltima play sessi�n
              , DATEDIFF (MINUTE,   MIN(PS_STARTED), MAX(PS_FINISHED))        AS   GAME_TIME                  -- Tiempo que lleva el player jugando
              , SUM(PS_PLAYED_AMOUNT)                                         AS   TOTAL_PLAYED               -- Total monto jugado (redimible y no redimible)
              , SUM(PS_PLAYED_COUNT)                                          AS   TOTAL_PLAYED_COUNT         -- Total jugadas 
              , SUM(PS_WON_COUNT)                                             AS   TOTAL_WON_COUNT            -- Total numero de jugadas ganadas  
              , SUM(PS_REDEEMABLE_PLAYED)                                     AS   TOTAL_PLAYED_REDEEM        -- Total jugado redimible
              , SUM(PS_NON_REDEEMABLE_PLAYED)                                 AS   TOTAL_PLAYED_NON_REDEEM    -- Total jugado no redimible
              , SUM(PS_WON_AMOUNT)                                            AS   AMOUNT_WON                 -- Total ganado (redimible y no redimible)
              , SUM(PS_CASH_IN)                                               AS   BILL_IN                    -- Bill In (ONLY FOR TITO)
              , SUM(PS_REDEEMABLE_WON)                                        AS   TOTAL_RE_WON_AMOUNT        -- Total ganado (redimible)
              , SUM(PS_NON_REDEEMABLE_WON)                                    AS   TOTAL_NR_WON_AMOUNT        -- Total ganado (no redimible)
        FROM    PLAY_SESSIONS                                   
         WHERE    PS_FINISHED >= @DateStart                       
           AND    PS_FINISHED <  @DateEnd                       
           AND    PS_STATUS   <> 0                               
     GROUP  BY    PS_ACCOUNT_ID) AS PLAY_SES 
    ON    PLAY_SES.PS_ACCOUNT_ID = ACCOUNT_INFO.AC_ACCOUNT_ID
     
  -- ACCOUNT_MOVEMENTS
    LEFT JOIN
          ( 
        SELECT  AM_ACCOUNT_ID 
            , SUM(CASE AM_TYPE WHEN  1  THEN AM_ADD_AMOUNT ELSE 0 END)        AS  [DEPOSIT A]         -- Recarga / Cash In
            , SUM(CASE AM_TYPE WHEN  1  THEN 1 ELSE 0 END)                    AS  [NUM_RECHARGES]     -- N�mero de Recargas / Num de Billetes Introducidos
            
            , SUM(CASE AM_TYPE WHEN  3  THEN AM_SUB_AMOUNT ELSE 0 END)        AS  [REFUNDS]           -- Reembolso (Devoluci�n)
            , SUM(CASE AM_TYPE WHEN  3  THEN 1 ELSE 0 END)                    AS  [NUM_REFUNDS]       -- N�mero de Retiros (o cobro de tickets)
            
            , SUM(CASE AM_TYPE WHEN  2  THEN AM_SUB_AMOUNT ELSE 0 END)        AS  [PRIZE]             -- Premio
            , SUM(CASE AM_TYPE WHEN 44  THEN AM_ADD_AMOUNT ELSE 0 END)        AS  [PRIZE COUPON]      -- Cup�n Premio
            
            , SUM(CASE AM_TYPE WHEN 82  THEN AM_SUB_AMOUNT ELSE 0 END)        AS  [MONEY IN TAX]        
            , SUM(CASE AM_TYPE WHEN 4   THEN AM_SUB_AMOUNT ELSE 0 END)        AS  [TAX ON PRIZE 1]        
            , SUM(CASE AM_TYPE WHEN 13  THEN AM_SUB_AMOUNT ELSE 0 END)        AS  [TAX ON PRIZE 2]        
            , MIN(AM_DATETIME)                                                AS  [FIRST MOV]         -- Primer movimiento de cuenta 
            , MAX(AM_DATETIME)                                                AS  [LAST MOV]          -- �ltimo movimiento de cuenta 
          FROM  ACCOUNT_MOVEMENTS WITH (INDEX (IX_AM_DATETIME) )                       
         WHERE  AM_DATETIME >= @DateStart                                                   
             AND  AM_DATETIME <  @DateEnd 
      GROUP BY  AM_ACCOUNT_ID) ACCOUNT_MOV
    ON  ACCOUNT_MOV.AM_ACCOUNT_ID = ACCOUNT_INFO.AC_ACCOUNT_ID
        
  -- THEO WON
    LEFT JOIN 
      (
      SELECT    PO_RESULT.PS_ID                                               AS PS_ID
          , SUM(PO_RESULT.PS_RESULT)                                      AS PO_THEO_WON 
        FROM  
        (    
        SELECT    PS_ACCOUNT_ID                                                         AS PS_ID
            , SUM(TE_THEORETICAL_PAYOUT)                                            AS PS_PO
            , SUM(PLAY_SESSIONS.PS_PLAYED_AMOUNT)                                   AS PS_PA
            , SUM(TE_THEORETICAL_PAYOUT) * SUM(PLAY_SESSIONS.PS_PLAYED_AMOUNT)      AS PS_RESULT
          FROM    TERMINALS 
      INNER JOIN    PLAY_SESSIONS
          ON    PLAY_SESSIONS.PS_TERMINAL_ID = TERMINALS.TE_TERMINAL_ID
         WHERE    PS_FINISHED >= @DateStart                       
             AND    PS_FINISHED <  @DateEnd                       
             AND    PS_STATUS   <> 0                      
        GROUP BY    PS_ACCOUNT_ID
            , PS_PLAY_SESSION_ID
        ) PO_RESULT
      GROUP BY PO_RESULT.PS_ID
    ) POS       
    ON POS.PS_ID = ACCOUNT_INFO.AC_ACCOUNT_ID  
    
  -- HANDPAYS (JACKPOTS)
    LEFT JOIN
      (
      SELECT    PS_ACCOUNT_ID             AS PS_ACCOUNT_ID
          , SUM(HP_AMOUNT)            AS HP_AMOUNT
        FROM    HANDPAYS  
    INNER JOIN    PLAY_SESSIONS
        ON    PS_PLAY_SESSION_ID = HP_PLAY_SESSION_ID
       WHERE    HP_DATETIME >= @DateStart   
         AND    HP_DATETIME <  @DateEnd 
         AND    HP_TYPE IN(1, 20, 1001)   --  Jackpots(Progressive, Non Progressive, Major Prize), Site Jackpots and Manual Jackpots
         AND    HP_STATUS & 61440 = 32768 --  Paid
      GROUP BY    PS_ACCOUNT_ID
    ) JACKPOTS
    ON ACCOUNT_INFO.AC_ACCOUNT_ID = JACKPOTS.PS_ACCOUNT_ID
 
 
  CREATE INDEX IDX_TEMP_ACCOUNT_INFO ON #TEMP_ACCOUNT_INFO (PVH_ACCOUNT_ID, PVH_DATE)
  
  INSERT INTO H_PVH
    (
        PVH_ACCOUNT_ID
      , PVH_DATE
      , PVH_WEEKDAY             
      , PVH_VISIT               
      , PVH_CHECK_IN            
      , PVH_CHECK_OUT           
      , PVH_ROOM_TIME           
      , PVH_GAME_TIME           
      , PVH_TOTAL_PLAYED_COUNT  
      , PVH_PLAYED_WON_COUNT    
      , PVH_TOTAL_PLAYED        
      , PVH_TOTAL_BET_AVG       
      , PVH_JACKPOTS_WON        
      , PVH_TOTAL_WON           
      , PVH_RE_PLAYED           
      , PVH_NR_PLAYED           
      , PVH_THEORICAL_WON       
      , PVH_MONEY_IN            
      , PVH_MONEY_IN_TAX        
      , PVH_MONEY_OUT           
      , PVH_RE_WON              
      , PVH_NR_WON              
      , PVH_DEVOLUTION          
      , PVH_PRIZE               
      , PVH_TAX1                
      , PVH_TAX2                
      , PVH_MONEY_IN_COUNT      
      , PVH_MONEY_OUT_COUNT     
      , PVH_AGE                 
      , PVH_REGISTERED_NUM_DAYS 
      , PVH_LEVEL                 
    )
  SELECT
      TEMP.PVH_ACCOUNT_ID
    , TEMP.PVH_DATE
    , TEMP.PVH_WEEKDAY             
    , TEMP.PVH_VISIT              
    , TEMP.PVH_CHECK_IN           
    , TEMP.PVH_CHECK_OUT          
    , TEMP.PVH_ROOM_TIME          
    , TEMP.PVH_GAME_TIME          
    , TEMP.PVH_TOTAL_PLAYED_COUNT 
    , TEMP.PVH_PLAYED_WON_COUNT   
    , TEMP.PVH_TOTAL_PLAYED       
    , TEMP.PVH_TOTAL_BET_AVG      
    , TEMP.PVH_JACKPOTS_WON       
    , TEMP.PVH_TOTAL_WON          
    , TEMP.PVH_RE_PLAYED          
    , TEMP.PVH_NR_PLAYED          
    , TEMP.PVH_THEORICAL_WON      
    , TEMP.PVH_MONEY_IN           
    , TEMP.PVH_MONEY_IN_TAX       
    , TEMP.PVH_MONEY_OUT          
    , TEMP.PVH_RE_WON             
    , TEMP.PVH_NR_WON             
    , TEMP.PVH_DEVOLUTION         
    , TEMP.PVH_PRIZE              
    , TEMP.PVH_TAX1               
    , TEMP.PVH_TAX2               
    , TEMP.PVH_MONEY_IN_COUNT     
    , TEMP.PVH_MONEY_OUT_COUNT    
    , TEMP.PVH_AGE                
    , TEMP.PVH_REGISTERED_NUM_DAYS
    , TEMP.PVH_LEVEL                
  FROM #TEMP_ACCOUNT_INFO TEMP
  WHERE NOT EXISTS (SELECT 1 FROM H_PVH WHERE TEMP.PVH_ACCOUNT_ID = H_PVH.PVH_ACCOUNT_ID AND TEMP.PVH_DATE = H_PVH.PVH_DATE)  
  SET @ROW_COUNT = @@ROWCOUNT
  
  UPDATE H_PVH
  SET
      H_PVH.PVH_WEEKDAY             =  TEMP.PVH_WEEKDAY             
    , H_PVH.PVH_VISIT               =  TEMP.PVH_VISIT              
    , H_PVH.PVH_CHECK_IN            =  TEMP.PVH_CHECK_IN           
    , H_PVH.PVH_CHECK_OUT           =  TEMP.PVH_CHECK_OUT          
    , H_PVH.PVH_ROOM_TIME           =  TEMP.PVH_ROOM_TIME          
    , H_PVH.PVH_GAME_TIME           =  TEMP.PVH_GAME_TIME          
    , H_PVH.PVH_TOTAL_PLAYED_COUNT  =  TEMP.PVH_TOTAL_PLAYED_COUNT 
    , H_PVH.PVH_PLAYED_WON_COUNT    =  TEMP.PVH_PLAYED_WON_COUNT   
    , H_PVH.PVH_TOTAL_PLAYED        =  TEMP.PVH_TOTAL_PLAYED       
    , H_PVH.PVH_TOTAL_BET_AVG       =  TEMP.PVH_TOTAL_BET_AVG      
    , H_PVH.PVH_JACKPOTS_WON        =  TEMP.PVH_JACKPOTS_WON       
    , H_PVH.PVH_TOTAL_WON           =  TEMP.PVH_TOTAL_WON          
    , H_PVH.PVH_RE_PLAYED           =  TEMP.PVH_RE_PLAYED          
    , H_PVH.PVH_NR_PLAYED           =  TEMP.PVH_NR_PLAYED          
    , H_PVH.PVH_THEORICAL_WON       =  TEMP.PVH_THEORICAL_WON      
    , H_PVH.PVH_MONEY_IN            =  TEMP.PVH_MONEY_IN           
    , H_PVH.PVH_MONEY_IN_TAX        =  TEMP.PVH_MONEY_IN_TAX       
    , H_PVH.PVH_MONEY_OUT           =  TEMP.PVH_MONEY_OUT          
    , H_PVH.PVH_RE_WON              =  TEMP.PVH_RE_WON             
    , H_PVH.PVH_NR_WON              =  TEMP.PVH_NR_WON             
    , H_PVH.PVH_DEVOLUTION          =  TEMP.PVH_DEVOLUTION         
    , H_PVH.PVH_PRIZE               =  TEMP.PVH_PRIZE              
    , H_PVH.PVH_TAX1                =  TEMP.PVH_TAX1               
    , H_PVH.PVH_TAX2                =  TEMP.PVH_TAX2               
    , H_PVH.PVH_MONEY_IN_COUNT      =  TEMP.PVH_MONEY_IN_COUNT     
    , H_PVH.PVH_MONEY_OUT_COUNT     =  TEMP.PVH_MONEY_OUT_COUNT    
    , H_PVH.PVH_AGE                 =  TEMP.PVH_AGE                
    , H_PVH.PVH_REGISTERED_NUM_DAYS =  TEMP.PVH_REGISTERED_NUM_DAYS
    , H_PVH.PVH_LEVEL               =  TEMP.PVH_LEVEL                
  FROM H_PVH
  INNER JOIN #TEMP_ACCOUNT_INFO TEMP ON TEMP.PVH_ACCOUNT_ID = H_PVH.PVH_ACCOUNT_ID AND TEMP.PVH_DATE = H_PVH.PVH_DATE
  
  SET @ROW_COUNT = @ROW_COUNT + @@ROWCOUNT
  SELECT @ROW_COUNT
  
END
GO

/* */

CREATE PROCEDURE [dbo].[SmartFloor_TVH]
  @DateStart       DATETIME,
  @DateEnd         DATETIME,
  @IsTitoMode      BIT
AS
BEGIN
  DECLARE @DateStartZero AS DATETIME  
  DECLARE @DateEndZero AS DATETIME  
  DECLARE @BetAvgMinHighRoller AS MONEY
  SET @DateStartZero = (SELECT DATEADD(DAY,DATEDIFF(DAY,0,@DateStart),0)) 
  SET @DateEndZero = (SELECT DATEADD(DAY,DATEDIFF(DAY,0,@DateEnd),0)) 
  
  --SET @BetAvgMinHighRoller = 1.6    --HARDCODED. This value must be obtained from a General Param.

  SELECT    TERMINALS.TE_TERMINAL_ID
          , CAST(CONVERT(NVARCHAR(8),@DateEnd,112) AS INTEGER)                                AS T_DATE
          , DATEPART(DW,@DateEnd)                                                             AS T_WEEKDAY
          , SPH.[APUESTA MEDIA]                                                               AS BET_AVG
          , SPH.JUGADO                                                                        AS PLAYED
          , SPH.PREMIOS                                                                       AS PRIZE
          , CASE WHEN (@IsTitoMode = 1) THEN 
              ACCOUNT_MOV.[DEPOSIT A] + ACCOUNT_MOV.[PRIZE COUPON] + PS2.PS_CASH_IN
            ELSE 
              ACCOUNT_MOV.[DEPOSIT A] + ACCOUNT_MOV.[PRIZE COUPON]
            END                                                                               AS COIN_IN          -- CASH IN (Recargas / Bill In)
            
          , SPH.[NET WIN]                                                                     AS NET_WIN
          , SPH.[THEORICAL WON]                                                               AS THEORETICAL_WIN
          , SPH.[NUM JUGADAS]                                                                 AS PLAYED_COUNT
          , TE_DENOMINATION                                                                   AS DENOMINATION     --�TE_MULTI_DENOMINATION?
          , SPH.LOSS                                                                          AS LOSS
          , CAST(CAST(OCUP.OCCUPIED * 100 AS DECIMAL) / 
            (CAST( (OCUP.TERMINAL_COUNT_MASTER * 24 * 60 * 60) AS DECIMAL)) AS DECIMAL(10,3)) AS MACHINE_OCUPATTION
          --, PS2.GENDER_MALE
          --, PS2.GENDER_FEMALE
          --, PS2.GENDER_UNKNOWN
          --, PS2.ANONYMOUSS                                                                    AS ANONYMOUSS
          --, PS2.REGISTERED_LEVEL_SILVER                                                       AS REGISTERED_LEVEL_SILVER
          --, PS2.REGISTERED_LEVEL_GOLD                                                         AS REGISTERED_LEVEL_GOLD
          --, PS2.REGISTERED_LEVEL_PLATINUM                                                     AS REGISTERED_LEVEL_PLATINUM
          --, PS2.REGISTERED_LEVEL_WIN                                                          AS REGISTERED_LEVEL_WIN
          --, PS2.NEW_CLIENTS                                                                   AS NEW_CLIENTS
     FROM   TERMINALS

-- SALES PER HOUR
INNER JOIN  
        (
          SELECT  SPH_TERMINAL_ID  
                , SUM(CASE WHEN SALES_PER_HOUR_V2.SPH_PLAYED_COUNT > 0 THEN 
                    ROUND( SALES_PER_HOUR_V2.SPH_PLAYED_AMOUNT / 
                           SALES_PER_HOUR_V2.SPH_PLAYED_COUNT, 2)
                  ELSE
                    0                                                               
                  END)                                                              AS [APUESTA MEDIA]
                , SUM(SALES_PER_HOUR_V2.SPH_PLAYED_AMOUNT)                          AS [JUGADO]
                , SUM(SALES_PER_HOUR_V2.SPH_WON_AMOUNT)                             AS [PREMIOS]
                , SUM(ISNULL(SALES_PER_HOUR_V2.SPH_PLAYED_AMOUNT,0) - 
                  (ISNULL(SALES_PER_HOUR_V2.SPH_WON_AMOUNT,0) + 
                   ISNULL(SALES_PER_HOUR_V2.SPH_PROGRESSIVE_PROVISION_AMOUNT,0) - 
                   ISNULL(SALES_PER_HOUR_V2.SPH_PROGRESSIVE_JACKPOT_AMOUNT_0,0)))   AS [NET WIN]
                , SUM(SALES_PER_HOUR_V2.SPH_THEORETICAL_WON_AMOUNT)                 AS [THEORICAL WON]
                , SUM(SALES_PER_HOUR_V2.SPH_PLAYED_COUNT)                           AS [NUM JUGADAS]
                , SUM(ISNULL(SALES_PER_HOUR_V2.SPH_PLAYED_AMOUNT,0) - 
                  (ISNULL(SALES_PER_HOUR_V2.SPH_WON_AMOUNT,0) + 
                   ISNULL(SALES_PER_HOUR_V2.SPH_PROGRESSIVE_PROVISION_AMOUNT,0) - 
                   ISNULL(SALES_PER_HOUR_V2.SPH_PROGRESSIVE_JACKPOT_AMOUNT_0,0)))   AS [LOSS]
            FROM  SALES_PER_HOUR_V2
           WHERE  SPH_BASE_HOUR >= @DateStart 
             AND  SPH_BASE_HOUR < @DateEnd
        GROUP BY  SPH_TERMINAL_ID) SPH
              ON  TERMINALS.TE_TERMINAL_ID = SPH.SPH_TERMINAL_ID

 -- ACCOUNT_MOVEMENTS (PDTE REVISAR)
  LEFT JOIN
         ( 
        SELECT  AM_TERMINAL_ID 
              , SUM(CASE AM_TYPE WHEN  1  THEN AM_ADD_AMOUNT ELSE 0 END)  AS  [DEPOSIT A]         -- Recarga / Cash In
              , SUM(CASE AM_TYPE WHEN 44  THEN AM_ADD_AMOUNT ELSE 0 END)  AS  [PRIZE COUPON]      -- Cup�n Premio
          FROM  ACCOUNT_MOVEMENTS WITH (INDEX (IX_AM_DATETIME) )                       
         WHERE  AM_DATETIME >= @DateStart                                                   
            AND  AM_DATETIME <  @DateEnd 
      GROUP BY  AM_TERMINAL_ID) 
ACCOUNT_MOV ON  ACCOUNT_MOV.AM_TERMINAL_ID = TERMINALS.TE_TERMINAL_ID

-- MACHINE OCUPATTION % (PDTE REVISAR)
LEFT JOIN 
        (
        SELECT  TE_TERMINAL_ID
              , TE_MASTER_ID
              , SUM(DATEDIFF (SECOND, PS_STARTED, PS_FINISHED))     AS OCCUPIED 
              , COUNT(DISTINCT TC_DATE)                             AS CONNECTED
              , CASE WHEN COUNT(DISTINCT TC_DATE) = 0 THEN 
                  CASE WHEN TERMINALS.TE_MASTER_ID IS NULL THEN 0
                  ELSE 1
                  END 
                  ELSE
                    COUNT(DISTINCT TC_DATE)
                  END                                               AS TERMINAL_COUNT_MASTER
          FROM  TERMINALS
     LEFT JOIN  PLAY_SESSIONS 
            ON  TE_TERMINAL_ID = PS_TERMINAL_ID 
           AND  PS_FINISHED >= @DateStart 
           AND  PS_FINISHED <  @DateEnd 
     LEFT JOIN  TERMINALS_CONNECTED  
            ON  TC_TERMINAL_ID = TE_TERMINAL_ID 
           AND  TC_DATE >= @DateStartZero
           AND  TC_DATE <  @DateEndZero
      GROUP BY  TE_TERMINAL_ID, TE_MASTER_ID
      ) OCUP
    ON OCUP.TE_TERMINAL_ID = TERMINALS.TE_TERMINAL_ID
    
-- ACCOUNTS (GENDER: MALE, FEMALE; USER TYPE: REGISTERED(level), ANONYMOUS, UNKNOWN)
LEFT JOIN 
  (
  SELECT 
        PS.PS_TERMINAL_ID AS PSI
      --, COUNT(PS.GENDER_MALE)               AS GENDER_MALE 
      --, COUNT(PS.GENDER_FEMALE)             AS GENDER_FEMALE
      --, COUNT(PS.GENDER_UNKNOWN)            AS GENDER_UNKNOWN
      --, COUNT(PS.ANONYMOUSS)                AS ANONYMOUSS
      --, COUNT(PS.REGISTERED_LEVEL_SILVER)   AS REGISTERED_LEVEL_SILVER
      --, COUNT(PS.REGISTERED_LEVEL_GOLD)     AS REGISTERED_LEVEL_GOLD
      --, COUNT(PS.REGISTERED_LEVEL_PLATINUM) AS REGISTERED_LEVEL_PLATINUM
      --, COUNT(PS.REGISTERED_LEVEL_WIN)      AS REGISTERED_LEVEL_WIN
      --, COUNT(PS.NEW_CLIENTS)               AS NEW_CLIENTS 
      , SUM(PS.PS_CASH_IN)                  AS PS_CASH_IN
  FROM 
    (
      SELECT  PS_TERMINAL_ID
            , AC_ACCOUNT_ID
            --, SUM(CASE WHEN AC_HOLDER_GENDER = 1 THEN 1 END)                    AS GENDER_MALE
         --   , SUM(CASE WHEN AC_HOLDER_GENDER = 2 THEN 1 END)                    AS GENDER_FEMALE
         --   , SUM(CASE WHEN AC_HOLDER_GENDER NOT IN (1, 2) THEN 1 END)          AS GENDER_UNKNOWN
         --   , SUM(CASE WHEN AC_TYPE = 2 
         --               AND AC_HOLDER_LEVEL = 0 THEN 1 
         --         END)                                                          AS ANONYMOUSS
         --   , SUM(CASE WHEN AC_TYPE = 2 
         --               AND AC_HOLDER_LEVEL = 1 THEN 1 
         --         END)                                                          AS REGISTERED_LEVEL_SILVER
         --   , SUM(CASE WHEN AC_TYPE = 2 
         --               AND AC_HOLDER_LEVEL = 2 THEN 1 
         --         END)                                                          AS REGISTERED_LEVEL_GOLD
         --   , SUM(CASE WHEN AC_TYPE = 2 
         --               AND AC_HOLDER_LEVEL = 3 THEN 1 
         --         END)                                                          AS REGISTERED_LEVEL_PLATINUM
         --   , SUM(CASE WHEN AC_TYPE = 2 
         --               AND AC_HOLDER_LEVEL = 4 THEN 1 
         --         END)                                                          AS REGISTERED_LEVEL_WIN
            --, SUM(CASE WHEN DATEDIFF(DAY,AC_CREATED, GETDATE())<=1 THEN 1 END)  AS NEW_CLIENTS
            , SUM(PS_CASH_IN)                                                   AS PS_CASH_IN
        FROM  PLAY_SESSIONS
  INNER JOIN  ACCOUNTS
          ON  PS_ACCOUNT_ID = ACCOUNTS.AC_ACCOUNT_ID
       WHERE  PS_FINISHED >= @DATESTART                       
         AND  PS_FINISHED <  @DATEEND                       
         AND  PS_STATUS   <> 0
    GROUP BY  PS_TERMINAL_ID
            , AC_ACCOUNT_ID) PS
    GROUP BY PS_TERMINAL_ID
      ) PS2
   ON PS2.PSI = TERMINALS.TE_TERMINAL_ID

-- PROMOTIONS (ASSIGNED & GRANTED)
   SELECT   PM.PROMOTIONS_TOTAL
          , PMG.PROMOTIONS_GRANTED
          , PLAYERS2.GENDER_MALE
          , PLAYERS2.GENDER_FEMALE
          , PLAYERS2.GENDER_UNKNOWN
          , PLAYERS2.ANONYMOUSS
          , PLAYERS2.REGISTERED_LEVEL_SILVER
          , PLAYERS2.REGISTERED_LEVEL_GOLD
          , PLAYERS2.REGISTERED_LEVEL_PLATINUM
          , PLAYERS2.REGISTERED_LEVEL_WIN
          , PLAYERS2.NEW_CLIENTS
     FROM   
            (SELECT COUNT(*) AS PROMOTIONS_TOTAL
               FROM PROMOTIONS) PM
          , (SELECT COUNT(*) AS PROMOTIONS_GRANTED
               FROM ACCOUNT_PROMOTIONS) PMG
          
          , (SELECT   COUNT(PLAYERS.GENDER_MALE)               AS GENDER_MALE 
                    , COUNT(PLAYERS.GENDER_FEMALE)             AS GENDER_FEMALE
                    , COUNT(PLAYERS.GENDER_UNKNOWN)            AS GENDER_UNKNOWN
                    , COUNT(PLAYERS.ANONYMOUSS)                AS ANONYMOUSS
                    , COUNT(PLAYERS.REGISTERED_LEVEL_SILVER)   AS REGISTERED_LEVEL_SILVER
                    , COUNT(PLAYERS.REGISTERED_LEVEL_GOLD)     AS REGISTERED_LEVEL_GOLD
                    , COUNT(PLAYERS.REGISTERED_LEVEL_PLATINUM) AS REGISTERED_LEVEL_PLATINUM
                    , COUNT(PLAYERS.REGISTERED_LEVEL_WIN)      AS REGISTERED_LEVEL_WIN
                    , COUNT(PLAYERS.NEW_CLIENTS)               AS NEW_CLIENTS 
                FROM 
                  (
                    SELECT  AC_ACCOUNT_ID
                          , SUM(CASE WHEN AC_HOLDER_GENDER = 1 THEN 1 END)                    AS GENDER_MALE
                          , SUM(CASE WHEN AC_HOLDER_GENDER = 2 THEN 1 END)                    AS GENDER_FEMALE
                          , SUM(CASE WHEN AC_HOLDER_GENDER NOT IN (1, 2) THEN 1 END)          AS GENDER_UNKNOWN
                          , SUM(CASE WHEN AC_TYPE = 2 
                                      AND AC_HOLDER_LEVEL = 0 THEN 1 
                                END)                                                          AS ANONYMOUSS
                          , SUM(CASE WHEN AC_TYPE = 2 
                                      AND AC_HOLDER_LEVEL = 1 THEN 1 
                                END)                                                          AS REGISTERED_LEVEL_SILVER
                          , SUM(CASE WHEN AC_TYPE = 2 
                                      AND AC_HOLDER_LEVEL = 2 THEN 1 
                                END)                                                          AS REGISTERED_LEVEL_GOLD
                          , SUM(CASE WHEN AC_TYPE = 2 
                                      AND AC_HOLDER_LEVEL = 3 THEN 1 
                                END)                                                          AS REGISTERED_LEVEL_PLATINUM
                          , SUM(CASE WHEN AC_TYPE = 2 
                                      AND AC_HOLDER_LEVEL = 4 THEN 1 
                                END)                                                          AS REGISTERED_LEVEL_WIN
                          , SUM(CASE WHEN DATEDIFF(DAY,AC_CREATED, GETDATE())<=1 THEN 1 END)  AS NEW_CLIENTS
                          , SUM(PS_CASH_IN)                                                   AS PS_CASH_IN
                      FROM  PLAY_SESSIONS
                INNER JOIN  ACCOUNTS
                        ON  PS_ACCOUNT_ID = ACCOUNTS.AC_ACCOUNT_ID
                     WHERE  PS_FINISHED >= @DATESTART                       
                       AND  PS_FINISHED <  @DATEEND                       
                       AND  PS_STATUS   <> 0
                  GROUP BY  AC_ACCOUNT_ID) PLAYERS
                 ) PLAYERS2
               
END
GO

/* */

CREATE PROCEDURE [dbo].[SP_Update_Meter_Data_Site_Calc] (
  @pTableId CHAR(1) -- L (Life), Y (Year), M (Month), W (Week), T (Today)
  ,@pEntityId CHAR(1) -- T (Terminales), S (Site), B(Both)
  ,@pDate DATETIME
  ,@pId INT -- Terminal ID or Site Id
  ,@pMeterId INT -- 1=CoinIn, 2= NetWin, 3=BetAvg, 4=Gender...
  ,@pMeterItem INT -- 0=Simple meter data, 1=Gender-Male, 2=Gender-female...
  ,@pValue DECIMAL(12, 3) -- Value to aggregate
  /*   for debug
DECLARE 
  @pTableId CHAR(1) = 'T'
  ,@pEntityId CHAR(1) = 'T'
  ,@pDate DATETIME = '2015-10-07 22:01:00.000'
  ,@pValue DECIMAL(12,3)     = 12
  ,@pId INT = 10006
  ,@pMeterId INT = 1
  ,@pMeterItem INT = 0
*/
  )
AS
BEGIN

/*
  JRC 20150928

  Version 1: sql server pivot
  Version 2: manual pivot
  Version 3: direct update
  Version 4: CAST & Insert Else update
  This sp stores the information in the corresponding table based on the @pTableID and @pEntityId parameters
  ie HT_2D_SMH for T(oday) and S(ite).
  
  on which column is based on the time of the item @pDate (one set of column for each 24 hours)
  the stored info on each set of columns is
  Max value
  Min Value
  Measurements Count
  Average Value
  Total Value
  
  and one special set of columns sumarizinf the information on the row
  Max value from the max fields of the row
  Min value from the min fields of the row
  Measurements Count from the Count fields of the row
  Total value from the total fields of the row.
  Average value from this group of fields.
  
  Average recalc formula explained:
  In order to calculate an average value we need : total and count.
  total can be replace by average * count
  so the new average can be expressed as: ((average * count) + new value) / (count + 1)
  and this is how it is done in this SP.
  
*/

  DECLARE @_date AS DATETIME
  DECLARE @_date_val AS INT
  DECLARE @_wk AS INT
  DECLARE @_h AS VARCHAR(2)
  DECLARE @_get_sql NVARCHAR(MAX)
  DECLARE @_get_sql_terminals NVARCHAR(MAX)
  DECLARE @_insert_sql NVARCHAR(MAX)
  DECLARE @_update_sql NVARCHAR(MAX)
  DECLARE @_row_count_terminals NVARCHAR(MAX)
  DECLARE @_row_count NVARCHAR(MAX)
  DECLARE @pCurrentMin DECIMAL(12, 3)
  DECLARE @pCurrentMax DECIMAL(12, 3)
  DECLARE @pCurrentAcc DECIMAL(12, 3)
  DECLARE @pCurrentAvg DECIMAL(12, 3)
  DECLARE @pCurrentNum BIGINT

  SET @_date = dbo.SP_Get_Range_Date(@pDate, @pTableId, DEFAULT)
  SET @_date_val = CAST(CONVERT(VARCHAR(8), @_date, 112) AS INT)
  SET @_wk = DATEPART(WEEKDAY, @_date)
  SET @_h = DATEPART(HOUR, @pDate)
  SET @_h = REPLICATE('0', 2 - LEN(RTRIM(@_h))) + RTRIM(@_h)

  DECLARE @_date_val_V VARCHAR(20) 
  SET @_date_val_V = CAST(@_date_val AS VARCHAR)
  DECLARE @pId_V VARCHAR(20) 
  SET @pId_V = CAST(@pId AS VARCHAR)
  DECLARE @pMeterId_V VARCHAR(20) 
  SET @pMeterId_V = CAST(@pMeterId AS VARCHAR)
  DECLARE @pMeterItem_V VARCHAR(20) 
  SET @pMeterItem_V = CAST(@pMeterItem AS VARCHAR)
  DECLARE @_wk_V VARCHAR(20) 
  SET @_wk_V = CAST(@_wk AS VARCHAR)
  DECLARE @pValue_V VARCHAR(20) 
  SET @pValue_V = CAST(@pValue AS VARCHAR)



  SET @pCurrentMin = NULL
  SET @pCurrentMax = NULL
  SET @pCurrentAcc = 0
  SET @pCurrentAvg = 0
  SET @pCurrentNum = 0

  DECLARE @DailyMin Decimal(12,3) 
  SET @DailyMin = NULL
  DECLARE @DailyMax Decimal(12,3) 
  SET @DailyMax = NULL
  DECLARE @DailyAcc Decimal(12,3) 
  SET @DailyAcc = 0
  DECLARE @DailyNum BIGINT 
  SET @DailyNum = 0
  DECLARE @DailyAvg Decimal(12,3) 
  SET @DailyAvg = 0

  ------------------------------------------------------------------------------------------------
  -- Obtain current meter data
  -- Use the sentence as EntityId
  SET @_get_sql = ' SELECT @pCurrentMin = (x2d_' + @_h + '_min)
               , @pCurrentMax = (x2d_' + @_h + '_max)
               , @pCurrentAcc = ISNULL(x2d_' + @_h + '_acc, 0)
               , @pCurrentNum = ISNULL(x2d_' + @_h + '_num, 0)
               , @pCurrentAvg = ISNULL(x2d_' + @_h + '_avg, 0)
               , @DailyMin    = x2d_dd_min 
               , @DailyMax    = x2d_dd_max
               , @DailyAcc    =  x2d_dd_acc
               , @DailyNum    =  x2d_dd_num
               , @DailyAvg    =  x2d_dd_avg
            FROM   H_' + @pTableId + '2D_' + @pEntityId + 'MH
            WHERE     x2d_date       = ' + @_date_val_V  + '
              AND   x2d_id         = ' + @pId_V        + '
              AND   x2d_meter_id   = ' + @pMeterId_V   + '
              AND   x2d_meter_item = ' + @pMeterItem_V + '
              AND   x2d_weekday    = ' + @_wk_V

  EXEC sp_executesql @_get_sql
              ,N'@pCurrentMin DECIMAL(12,3) OUTPUT, 
                 @pCurrentMax DECIMAL(12,3) OUTPUT, 
                 @pCurrentAcc DECIMAL(12,3) OUTPUT, 
                 @pCurrentNum BIGINT OUTPUT,
                 @pCurrentAvg DECIMAL(12,3) OUTPUT,
                 @DailyMin    DECIMAL(12,3) OUTPUT,
                 @DailyMax  DECIMAL(12,3) OUTPUT,
                 @DailyAcc  DECIMAL(12,3) OUTPUT,
                 @DailyNum  BIGINT OUTPUT,
                 @DailyAvg  DECIMAL(12,3) OUTPUT
                 '
    ,@pCurrentMin OUTPUT
    ,@pCurrentMax OUTPUT
    ,@pCurrentAcc OUTPUT
    ,@pCurrentNum OUTPUT
    ,@pCurrentAvg OUTPUT
    ,@DailyMin  OUTPUT
    ,@DailyMax  OUTPUT
    ,@DailyAcc  OUTPUT
    ,@DailyNum  OUTPUT
    ,@DailyAvg  OUTPUT

  SET @_row_count = @@ROWCOUNT

  ------------------------------------------------------------------------------------------------
  -- Prepare data for update
  -- Increment number
  SET @pCurrentNum = @pCurrentNum + 1
  -- Average calculation. Must be calculated first JRC
  SET @pCurrentAvg = (@pCurrentAvg * (@pCurrentNum - 1) + @pValue) / CONVERT(DECIMAL(38, 4), @pCurrentNum)

  -- Detect Min value
  IF (@pCurrentMin IS NULL)
    OR (@pCurrentMin > @pValue)
  BEGIN
    SET @pCurrentMin = @pValue
  END

  -- Detect Max value
  IF (@pCurrentMax IS NULL)
    OR (@pCurrentMax < @pValue)
  BEGIN
    SET @pCurrentMax = @pValue
  END

  -- Accumulated calculation
  SET @pCurrentAcc = @pCurrentAcc + @pValue
  ------------------------------------------------------------------------------------------------
  
  IF (@DailyMin IS NULL) OR (@DailyMin > @pValue)
    SET  @DailyMin = @pValue

  IF (@DailyMax IS NULL) OR (@DailyMax < @pValue) 
    SET @DailyMax = @pValue
  


  IF (@_row_count = 0  )
  BEGIN
    -- Create Meter Data
    SET @_insert_sql = 'INSERT INTO H_' + @pTableId + '2D_' + @pEntityId + 'MH
                                     ( x2d_date
                                     , x2d_weekday
                                     , x2d_id
                                     , x2d_meter_id
                                     , x2d_meter_item 
                   , x2d_' + @_h + '_min  
                   , x2d_' + @_h + '_max  
                   , x2d_' + @_h + '_acc  
                   , x2d_' + @_h + '_avg  
                   , x2d_' + @_h + '_num  
                   , x2d_dd_min    
                   , x2d_dd_max    
                   , x2d_dd_acc 
                   , x2d_dd_avg 
                   , x2d_dd_num 
                                   )
                             VALUES
                                     (' + @_date_val_V   + '
                                     ,' + @_wk_V         + '
                                     ,' + @pId_V         + '
                                     ,' + @pMeterId_V    + '                                          
                                     ,' + @pMeterItem_V  + '
                   ,' + @pValue_V      + '
                   ,' + @pValue_V      + '
                   ,' + @pValue_V      + ' 
                   ,' + @pValue_V      + ' 
                   , 1
                   ,' + @pValue_V      + ' 
                   ,' + @pValue_V      + ' 
                   ,' + @pValue_V      + ' 
                   ,' + @pValue_V      + ' 
                   , 1
                                    )'

    EXEC sys.sp_executesql @_insert_sql
  END ELSE BEGIN
    -- Update Meter data
    SET @_update_sql = 'UPDATE   H_' + @pTableId + '2D_' + @pEntityId + 'MH
               SET   x2d_' + @_h + '_min = ' + CAST(@pCurrentMin AS VARCHAR(20)) + '
                 , x2d_' + @_h + '_max = ' + CAST(@pCurrentMax AS VARCHAR(20)) + '
                 , x2d_' + @_h + '_acc = ' + CAST(@pCurrentAcc AS VARCHAR(20)) + '
                 , x2d_' + @_h + '_avg = ' + CAST(@pCurrentAvg AS VARCHAR(20)) + '
                 , x2d_' + @_h + '_num = ' + CAST(@pCurrentNum AS VARCHAR(20)) + '
                 , x2d_dd_min =   ' + CAST(@DailyMin AS VARCHAR(20)) + '
                 , x2d_dd_max =   ' + CAST(@DailyMax AS VARCHAR(20)) + '
                 , x2d_dd_acc = ISNULL([x2d_dd_acc],0) +  ' + @pValue_V + '
                 , x2d_dd_num = ISNULL([x2d_dd_num],0) +  1
                 , x2d_dd_avg = (ISNULL(x2d_dd_acc,0) + ' + @pValue_V +  ')/ (ISNULL(NULLIF(x2d_dd_num,0),1)+1) 
                 WHERE   x2d_date       = ' + @_date_val_V + '
                 AND   x2d_id         = ' + @pId_V + '
                 AND   x2d_meter_id   = ' + @pMeterId_V + '
                 AND   x2d_meter_item = ' + @pMeteritem_V + '
                 AND   x2d_weekday    = ' + @_wk_V

    EXEC sys.sp_executesql @_update_sql
  END

END
GO

/* */

CREATE PROCEDURE [dbo].[SP_Update_Meter_Data_Site] (
  @pTableId CHAR(1) -- L (Life), Y (Year), M (Month), W (Week), T (Today)
  ,@pEntityId CHAR(1) -- T (Terminales), S (Site), X (Terminal - Site)
  ,@pDate DATETIME
  ,@pId INT -- Terminal ID or Site Id
  ,@pMeterId INT -- 1=CoinIn, 2= NetWin, 3=BetAvg, 4=Gender...
  ,@pMeterItem INT -- 0=Simple meter data, 1=Gender-Male, 2=Gender-female...
  ,@pValue DECIMAL(12,3) -- Value to aggregate
  )
AS
BEGIN
/*
  JRC - 20150925
  This SP receives the information from the thread and the calls the _CAL sp maybe many times based on:
  the value in the @pTableId parameter, creating a waterfall efect. If it is T(oday) it will also be called for 
  W(eek), M(onth) and Y. Each of these accumulate the information on several tables (by Day, by Week, by Month and by Year).
  But if the parameter is for example M(onth), then it should be accumulated only for M(onth) and Y(year).
  If the @pEntityId is X, the information goes for T(terminal) and also for S(ite), so it should be processed twice by the _CALC SP

*/

  DECLARE @_EntityId CHAR(1) 
  SET @_EntityId = @pEntityId
  DECLARE @_accion VARCHAR(10)
  SET @_accion = CASE 
            WHEN @pTableId = 'L' THEN 'L'
            WHEN @pTableId = 'Y' THEN 'LY'
            WHEN @pTableId = 'M' THEN 'LYM'
            WHEN @pTableId = 'W' THEN 'LYMW'
            ELSE 'LYMWT' END

  IF @pEntityId = 'X' BEGIN
    -- first execution for Terminal
    SET @_EntityId = 'T'
  --  IF CHARINDEX('L', @_accion) > 0 Entiendo que el L no se utiliza
  --    EXECUTE SP_Update_Meter_Data_Site_Calc @pTableId,@_EntityId, @pDate, @pId ,@pMeterId, @pMeterItem ,@pValue
    IF CHARINDEX('Y', @_accion) > 0 
      EXECUTE SP_Update_Meter_Data_Site_Calc 'Y',@_EntityId, @pDate, @pId ,@pMeterId, @pMeterItem ,@pValue
    IF CHARINDEX('M', @_accion) > 0 
      EXECUTE SP_Update_Meter_Data_Site_Calc 'M',@_EntityId, @pDate, @pId ,@pMeterId, @pMeterItem ,@pValue
    IF CHARINDEX('W', @_accion) > 0 
      EXECUTE SP_Update_Meter_Data_Site_Calc 'W',@_EntityId, @pDate, @pId ,@pMeterId, @pMeterItem ,@pValue
    IF CHARINDEX('T', @_accion) > 0 
      EXECUTE SP_Update_Meter_Data_Site_Calc 'T',@_EntityId, @pDate, @pId ,@pMeterId, @pMeterItem ,@pValue
    SET @_EntityId = 'S'
    --next execution for Site
  END        

--  IF CHARINDEX('L', @_accion) > 0 Entiendo que el L no se utiliza
--    EXECUTE SP_Update_Meter_Data_Site_Calc @pTableId,@_EntityId, @pDate, @pId ,@pMeterId, @pMeterItem ,@pValue
  IF CHARINDEX('Y', @_accion) > 0 
    EXECUTE SP_Update_Meter_Data_Site_Calc 'Y',@_EntityId, @pDate, @pId ,@pMeterId, @pMeterItem ,@pValue
  IF CHARINDEX('M', @_accion) > 0 
    EXECUTE SP_Update_Meter_Data_Site_Calc 'M',@_EntityId, @pDate, @pId ,@pMeterId, @pMeterItem ,@pValue
  IF CHARINDEX('W', @_accion) > 0 
    EXECUTE SP_Update_Meter_Data_Site_Calc 'W',@_EntityId, @pDate, @pId ,@pMeterId, @pMeterItem ,@pValue
  IF CHARINDEX('T', @_accion) > 0 
    EXECUTE SP_Update_Meter_Data_Site_Calc 'T',@_EntityId, @pDate, @pId ,@pMeterId, @pMeterItem ,@pValue
      
END
GO

/* ----------------     TABLES     ---------------- */

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[H_M2D_SMH]') AND type in (N'U'))
  DROP TABLE [dbo].[H_M2D_SMH]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[H_M2D_TMH]') AND type in (N'U'))
  DROP TABLE [dbo].[H_M2D_TMH]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[H_T2D_SMH]') AND type in (N'U'))
  DROP TABLE [dbo].[H_T2D_SMH]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[H_T2D_TMH]') AND type in (N'U'))
  DROP TABLE [dbo].[H_T2D_TMH]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[H_W2D_SMH]') AND type in (N'U'))
  DROP TABLE [dbo].[H_W2D_SMH]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[H_W2D_TMH]') AND type in (N'U'))
  DROP TABLE [dbo].[H_W2D_TMH]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[H_X2D_CONTROL]') AND type in (N'U'))
  DROP TABLE [dbo].[H_X2D_CONTROL]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[H_Y2D_SMH]') AND type in (N'U'))
  DROP TABLE [dbo].[H_Y2D_SMH]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[H_Y2D_TMH]') AND type in (N'U'))
  DROP TABLE [dbo].H_Y2D_TMH
GO

/* */

CREATE TABLE [dbo].[h_m2d_smh](
  [x2d_date] [int] NOT NULL,
  [x2d_weekday] [tinyint] NOT NULL,
  [x2d_id] [int] NOT NULL,
  [x2d_meter_id] [int] NOT NULL,
  [x2d_meter_item] [int] NOT NULL,
  [x2d_00_min] decimal(12,3) NULL,
  [x2d_00_max] decimal(12,3) NULL,
  [x2d_00_acc] decimal(12,3) NULL,
  [x2d_00_avg] decimal(12,3) NULL,
  [x2d_00_num] [bigint] NULL,
  [x2d_01_min] decimal(12,3) NULL,
  [x2d_01_max] decimal(12,3) NULL,
  [x2d_01_acc] decimal(12,3) NULL,
  [x2d_01_avg] decimal(12,3) NULL,
  [x2d_01_num] [bigint] NULL,
  [x2d_02_min] decimal(12,3) NULL,
  [x2d_02_max] decimal(12,3) NULL,
  [x2d_02_acc] decimal(12,3) NULL,
  [x2d_02_avg] decimal(12,3) NULL,
  [x2d_02_num] [bigint] NULL,
  [x2d_03_min] decimal(12,3) NULL,
  [x2d_03_max] decimal(12,3) NULL,
  [x2d_03_acc] decimal(12,3) NULL,
  [x2d_03_avg] decimal(12,3) NULL,
  [x2d_03_num] [bigint] NULL,
  [x2d_04_min] decimal(12,3) NULL,
  [x2d_04_max] decimal(12,3) NULL,
  [x2d_04_acc] decimal(12,3) NULL,
  [x2d_04_avg] decimal(12,3) NULL,
  [x2d_04_num] [bigint] NULL,
  [x2d_05_min] decimal(12,3) NULL,
  [x2d_05_max] decimal(12,3) NULL,
  [x2d_05_acc] decimal(12,3) NULL,
  [x2d_05_avg] decimal(12,3) NULL,
  [x2d_05_num] [bigint] NULL,
  [x2d_06_min] decimal(12,3) NULL,
  [x2d_06_max] decimal(12,3) NULL,
  [x2d_06_acc] decimal(12,3) NULL,
  [x2d_06_avg] decimal(12,3) NULL,
  [x2d_06_num] [bigint] NULL,
  [x2d_07_min] decimal(12,3) NULL,
  [x2d_07_max] decimal(12,3) NULL,
  [x2d_07_acc] decimal(12,3) NULL,
  [x2d_07_avg] decimal(12,3) NULL,
  [x2d_07_num] [bigint] NULL,
  [x2d_08_min] decimal(12,3) NULL,
  [x2d_08_max] decimal(12,3) NULL,
  [x2d_08_acc] decimal(12,3) NULL,
  [x2d_08_avg] decimal(12,3) NULL,
  [x2d_08_num] [bigint] NULL,
  [x2d_09_min] decimal(12,3) NULL,
  [x2d_09_max] decimal(12,3) NULL,
  [x2d_09_acc] decimal(12,3) NULL,
  [x2d_09_avg] decimal(12,3) NULL,
  [x2d_09_num] [bigint] NULL,
  [x2d_10_min] decimal(12,3) NULL,
  [x2d_10_max] decimal(12,3) NULL,
  [x2d_10_acc] decimal(12,3) NULL,
  [x2d_10_avg] decimal(12,3) NULL,
  [x2d_10_num] [bigint] NULL,
  [x2d_11_min] decimal(12,3) NULL,
  [x2d_11_max] decimal(12,3) NULL,
  [x2d_11_acc] decimal(12,3) NULL,
  [x2d_11_avg] decimal(12,3) NULL,
  [x2d_11_num] [bigint] NULL,
  [x2d_12_min] decimal(12,3) NULL,
  [x2d_12_max] decimal(12,3) NULL,
  [x2d_12_acc] decimal(12,3) NULL,
  [x2d_12_avg] decimal(12,3) NULL,
  [x2d_12_num] [bigint] NULL,
  [x2d_13_min] decimal(12,3) NULL,
  [x2d_13_max] decimal(12,3) NULL,
  [x2d_13_acc] decimal(12,3) NULL,
  [x2d_13_avg] decimal(12,3) NULL,
  [x2d_13_num] [bigint] NULL,
  [x2d_14_min] decimal(12,3) NULL,
  [x2d_14_max] decimal(12,3) NULL,
  [x2d_14_acc] decimal(12,3) NULL,
  [x2d_14_avg] decimal(12,3) NULL,
  [x2d_14_num] [bigint] NULL,
  [x2d_15_min] decimal(12,3) NULL,
  [x2d_15_max] decimal(12,3) NULL,
  [x2d_15_acc] decimal(12,3) NULL,
  [x2d_15_avg] decimal(12,3) NULL,
  [x2d_15_num] [bigint] NULL,
  [x2d_16_min] decimal(12,3) NULL,
  [x2d_16_max] decimal(12,3) NULL,
  [x2d_16_acc] decimal(12,3) NULL,
  [x2d_16_avg] decimal(12,3) NULL,
  [x2d_16_num] [bigint] NULL,
  [x2d_17_min] decimal(12,3) NULL,
  [x2d_17_max] decimal(12,3) NULL,
  [x2d_17_acc] decimal(12,3) NULL,
  [x2d_17_avg] decimal(12,3) NULL,
  [x2d_17_num] [bigint] NULL,
  [x2d_18_min] decimal(12,3) NULL,
  [x2d_18_max] decimal(12,3) NULL,
  [x2d_18_acc] decimal(12,3) NULL,
  [x2d_18_avg] decimal(12,3) NULL,
  [x2d_18_num] [bigint] NULL,
  [x2d_19_min] decimal(12,3) NULL,
  [x2d_19_max] decimal(12,3) NULL,
  [x2d_19_acc] decimal(12,3) NULL,
  [x2d_19_avg] decimal(12,3) NULL,
  [x2d_19_num] [bigint] NULL,
  [x2d_20_min] decimal(12,3) NULL,
  [x2d_20_max] decimal(12,3) NULL,
  [x2d_20_acc] decimal(12,3) NULL,
  [x2d_20_avg] decimal(12,3) NULL,
  [x2d_20_num] [bigint] NULL,
  [x2d_21_min] decimal(12,3) NULL,
  [x2d_21_max] decimal(12,3) NULL,
  [x2d_21_acc] decimal(12,3) NULL,
  [x2d_21_avg] decimal(12,3) NULL,
  [x2d_21_num] [bigint] NULL,
  [x2d_22_min] decimal(12,3) NULL,
  [x2d_22_max] decimal(12,3) NULL,
  [x2d_22_acc] decimal(12,3) NULL,
  [x2d_22_avg] decimal(12,3) NULL,
  [x2d_22_num] [bigint] NULL,
  [x2d_23_min] decimal(12,3) NULL,
  [x2d_23_max] decimal(12,3) NULL,
  [x2d_23_acc] decimal(12,3) NULL,
  [x2d_23_avg] decimal(12,3) NULL,
  [x2d_23_num] [bigint] NULL,
  [x2d_dd_min] decimal(12,3) NULL,
  [x2d_dd_max] decimal(12,3) NULL,
  [x2d_dd_acc] decimal(12,3) NULL,
  [x2d_dd_avg] decimal(12,3) NULL,
  [x2d_dd_num] [bigint] NULL,
 CONSTRAINT [PK_M2D_SMH] PRIMARY KEY CLUSTERED 
(
  [x2d_date] ASC,
  [x2d_weekday] ASC,
  [x2d_id] ASC,
  [x2d_meter_id] ASC,
  [x2d_meter_item] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

/* */

CREATE TABLE [dbo].[h_m2d_tmh](
  [x2d_date] [int] NOT NULL,
  [x2d_weekday] [tinyint] NOT NULL,
  [x2d_id] [int] NOT NULL,
  [x2d_meter_id] [int] NOT NULL,
  [x2d_meter_item] [int] NOT NULL,
  [x2d_00_min] decimal(12,3) NULL,
  [x2d_00_max] decimal(12,3) NULL,
  [x2d_00_acc] decimal(12,3) NULL,
  [x2d_00_avg] decimal(12,3) NULL,
  [x2d_00_num] [bigint] NULL,
  [x2d_01_min] decimal(12,3) NULL,
  [x2d_01_max] decimal(12,3) NULL,
  [x2d_01_acc] decimal(12,3) NULL,
  [x2d_01_avg] decimal(12,3) NULL,
  [x2d_01_num] [bigint] NULL,
  [x2d_02_min] decimal(12,3) NULL,
  [x2d_02_max] decimal(12,3) NULL,
  [x2d_02_acc] decimal(12,3) NULL,
  [x2d_02_avg] decimal(12,3) NULL,
  [x2d_02_num] [bigint] NULL,
  [x2d_03_min] decimal(12,3) NULL,
  [x2d_03_max] decimal(12,3) NULL,
  [x2d_03_acc] decimal(12,3) NULL,
  [x2d_03_avg] decimal(12,3) NULL,
  [x2d_03_num] [bigint] NULL,
  [x2d_04_min] decimal(12,3) NULL,
  [x2d_04_max] decimal(12,3) NULL,
  [x2d_04_acc] decimal(12,3) NULL,
  [x2d_04_avg] decimal(12,3) NULL,
  [x2d_04_num] [bigint] NULL,
  [x2d_05_min] decimal(12,3) NULL,
  [x2d_05_max] decimal(12,3) NULL,
  [x2d_05_acc] decimal(12,3) NULL,
  [x2d_05_avg] decimal(12,3) NULL,
  [x2d_05_num] [bigint] NULL,
  [x2d_06_min] decimal(12,3) NULL,
  [x2d_06_max] decimal(12,3) NULL,
  [x2d_06_acc] decimal(12,3) NULL,
  [x2d_06_avg] decimal(12,3) NULL,
  [x2d_06_num] [bigint] NULL,
  [x2d_07_min] decimal(12,3) NULL,
  [x2d_07_max] decimal(12,3) NULL,
  [x2d_07_acc] decimal(12,3) NULL,
  [x2d_07_avg] decimal(12,3) NULL,
  [x2d_07_num] [bigint] NULL,
  [x2d_08_min] decimal(12,3) NULL,
  [x2d_08_max] decimal(12,3) NULL,
  [x2d_08_acc] decimal(12,3) NULL,
  [x2d_08_avg] decimal(12,3) NULL,
  [x2d_08_num] [bigint] NULL,
  [x2d_09_min] decimal(12,3) NULL,
  [x2d_09_max] decimal(12,3) NULL,
  [x2d_09_acc] decimal(12,3) NULL,
  [x2d_09_avg] decimal(12,3) NULL,
  [x2d_09_num] [bigint] NULL,
  [x2d_10_min] decimal(12,3) NULL,
  [x2d_10_max] decimal(12,3) NULL,
  [x2d_10_acc] decimal(12,3) NULL,
  [x2d_10_avg] decimal(12,3) NULL,
  [x2d_10_num] [bigint] NULL,
  [x2d_11_min] decimal(12,3) NULL,
  [x2d_11_max] decimal(12,3) NULL,
  [x2d_11_acc] decimal(12,3) NULL,
  [x2d_11_avg] decimal(12,3) NULL,
  [x2d_11_num] [bigint] NULL,
  [x2d_12_min] decimal(12,3) NULL,
  [x2d_12_max] decimal(12,3) NULL,
  [x2d_12_acc] decimal(12,3) NULL,
  [x2d_12_avg] decimal(12,3) NULL,
  [x2d_12_num] [bigint] NULL,
  [x2d_13_min] decimal(12,3) NULL,
  [x2d_13_max] decimal(12,3) NULL,
  [x2d_13_acc] decimal(12,3) NULL,
  [x2d_13_avg] decimal(12,3) NULL,
  [x2d_13_num] [bigint] NULL,
  [x2d_14_min] decimal(12,3) NULL,
  [x2d_14_max] decimal(12,3) NULL,
  [x2d_14_acc] decimal(12,3) NULL,
  [x2d_14_avg] decimal(12,3) NULL,
  [x2d_14_num] [bigint] NULL,
  [x2d_15_min] decimal(12,3) NULL,
  [x2d_15_max] decimal(12,3) NULL,
  [x2d_15_acc] decimal(12,3) NULL,
  [x2d_15_avg] decimal(12,3) NULL,
  [x2d_15_num] [bigint] NULL,
  [x2d_16_min] decimal(12,3) NULL,
  [x2d_16_max] decimal(12,3) NULL,
  [x2d_16_acc] decimal(12,3) NULL,
  [x2d_16_avg] decimal(12,3) NULL,
  [x2d_16_num] [bigint] NULL,
  [x2d_17_min] decimal(12,3) NULL,
  [x2d_17_max] decimal(12,3) NULL,
  [x2d_17_acc] decimal(12,3) NULL,
  [x2d_17_avg] decimal(12,3) NULL,
  [x2d_17_num] [bigint] NULL,
  [x2d_18_min] decimal(12,3) NULL,
  [x2d_18_max] decimal(12,3) NULL,
  [x2d_18_acc] decimal(12,3) NULL,
  [x2d_18_avg] decimal(12,3) NULL,
  [x2d_18_num] [bigint] NULL,
  [x2d_19_min] decimal(12,3) NULL,
  [x2d_19_max] decimal(12,3) NULL,
  [x2d_19_acc] decimal(12,3) NULL,
  [x2d_19_avg] decimal(12,3) NULL,
  [x2d_19_num] [bigint] NULL,
  [x2d_20_min] decimal(12,3) NULL,
  [x2d_20_max] decimal(12,3) NULL,
  [x2d_20_acc] decimal(12,3) NULL,
  [x2d_20_avg] decimal(12,3) NULL,
  [x2d_20_num] [bigint] NULL,
  [x2d_21_min] decimal(12,3) NULL,
  [x2d_21_max] decimal(12,3) NULL,
  [x2d_21_acc] decimal(12,3) NULL,
  [x2d_21_avg] decimal(12,3) NULL,
  [x2d_21_num] [bigint] NULL,
  [x2d_22_min] decimal(12,3) NULL,
  [x2d_22_max] decimal(12,3) NULL,
  [x2d_22_acc] decimal(12,3) NULL,
  [x2d_22_avg] decimal(12,3) NULL,
  [x2d_22_num] [bigint] NULL,
  [x2d_23_min] decimal(12,3) NULL,
  [x2d_23_max] decimal(12,3) NULL,
  [x2d_23_acc] decimal(12,3) NULL,
  [x2d_23_avg] decimal(12,3) NULL,
  [x2d_23_num] [bigint] NULL,
  [x2d_dd_min] decimal(12,3) NULL,
  [x2d_dd_max] decimal(12,3) NULL,
  [x2d_dd_acc] decimal(12,3) NULL,
  [x2d_dd_avg] decimal(12,3) NULL,
  [x2d_dd_num] [bigint] NULL,
 CONSTRAINT [PK_M2D_TMH] PRIMARY KEY CLUSTERED 
(
  [x2d_date] ASC,
  [x2d_id] ASC,
  [x2d_meter_id] ASC,
  [x2d_meter_item] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/* */

CREATE TABLE [dbo].[h_t2d_smh](
  [x2d_date] [int] NOT NULL,
  [x2d_weekday] [tinyint] NOT NULL,
  [x2d_id] [int] NOT NULL,
  [x2d_meter_id] [int] NOT NULL,
  [x2d_meter_item] [int] NOT NULL,
  [x2d_00_min] decimal(12,3) NULL,
  [x2d_00_max] decimal(12,3) NULL,
  [x2d_00_acc] decimal(12,3) NULL,
  [x2d_00_avg] decimal(12,3) NULL,
  [x2d_00_num] [bigint] NULL,
  [x2d_01_min] decimal(12,3) NULL,
  [x2d_01_max] decimal(12,3) NULL,
  [x2d_01_acc] decimal(12,3) NULL,
  [x2d_01_avg] decimal(12,3) NULL,
  [x2d_01_num] [bigint] NULL,
  [x2d_02_min] decimal(12,3) NULL,
  [x2d_02_max] decimal(12,3) NULL,
  [x2d_02_acc] decimal(12,3) NULL,
  [x2d_02_avg] decimal(12,3) NULL,
  [x2d_02_num] [bigint] NULL,
  [x2d_03_min] decimal(12,3) NULL,
  [x2d_03_max] decimal(12,3) NULL,
  [x2d_03_acc] decimal(12,3) NULL,
  [x2d_03_avg] decimal(12,3) NULL,
  [x2d_03_num] [bigint] NULL,
  [x2d_04_min] decimal(12,3) NULL,
  [x2d_04_max] decimal(12,3) NULL,
  [x2d_04_acc] decimal(12,3) NULL,
  [x2d_04_avg] decimal(12,3) NULL,
  [x2d_04_num] [bigint] NULL,
  [x2d_05_min] decimal(12,3) NULL,
  [x2d_05_max] decimal(12,3) NULL,
  [x2d_05_acc] decimal(12,3) NULL,
  [x2d_05_avg] decimal(12,3) NULL,
  [x2d_05_num] [bigint] NULL,
  [x2d_06_min] decimal(12,3) NULL,
  [x2d_06_max] decimal(12,3) NULL,
  [x2d_06_acc] decimal(12,3) NULL,
  [x2d_06_avg] decimal(12,3) NULL,
  [x2d_06_num] [bigint] NULL,
  [x2d_07_min] decimal(12,3) NULL,
  [x2d_07_max] decimal(12,3) NULL,
  [x2d_07_acc] decimal(12,3) NULL,
  [x2d_07_avg] decimal(12,3) NULL,
  [x2d_07_num] [bigint] NULL,
  [x2d_08_min] decimal(12,3) NULL,
  [x2d_08_max] decimal(12,3) NULL,
  [x2d_08_acc] decimal(12,3) NULL,
  [x2d_08_avg] decimal(12,3) NULL,
  [x2d_08_num] [bigint] NULL,
  [x2d_09_min] decimal(12,3) NULL,
  [x2d_09_max] decimal(12,3) NULL,
  [x2d_09_acc] decimal(12,3) NULL,
  [x2d_09_avg] decimal(12,3) NULL,
  [x2d_09_num] [bigint] NULL,
  [x2d_10_min] decimal(12,3) NULL,
  [x2d_10_max] decimal(12,3) NULL,
  [x2d_10_acc] decimal(12,3) NULL,
  [x2d_10_avg] decimal(12,3) NULL,
  [x2d_10_num] [bigint] NULL,
  [x2d_11_min] decimal(12,3) NULL,
  [x2d_11_max] decimal(12,3) NULL,
  [x2d_11_acc] decimal(12,3) NULL,
  [x2d_11_avg] decimal(12,3) NULL,
  [x2d_11_num] [bigint] NULL,
  [x2d_12_min] decimal(12,3) NULL,
  [x2d_12_max] decimal(12,3) NULL,
  [x2d_12_acc] decimal(12,3) NULL,
  [x2d_12_avg] decimal(12,3) NULL,
  [x2d_12_num] [bigint] NULL,
  [x2d_13_min] decimal(12,3) NULL,
  [x2d_13_max] decimal(12,3) NULL,
  [x2d_13_acc] decimal(12,3) NULL,
  [x2d_13_avg] decimal(12,3) NULL,
  [x2d_13_num] [bigint] NULL,
  [x2d_14_min] decimal(12,3) NULL,
  [x2d_14_max] decimal(12,3) NULL,
  [x2d_14_acc] decimal(12,3) NULL,
  [x2d_14_avg] decimal(12,3) NULL,
  [x2d_14_num] [bigint] NULL,
  [x2d_15_min] decimal(12,3) NULL,
  [x2d_15_max] decimal(12,3) NULL,
  [x2d_15_acc] decimal(12,3) NULL,
  [x2d_15_avg] decimal(12,3) NULL,
  [x2d_15_num] [bigint] NULL,
  [x2d_16_min] decimal(12,3) NULL,
  [x2d_16_max] decimal(12,3) NULL,
  [x2d_16_acc] decimal(12,3) NULL,
  [x2d_16_avg] decimal(12,3) NULL,
  [x2d_16_num] [bigint] NULL,
  [x2d_17_min] decimal(12,3) NULL,
  [x2d_17_max] decimal(12,3) NULL,
  [x2d_17_acc] decimal(12,3) NULL,
  [x2d_17_avg] decimal(12,3) NULL,
  [x2d_17_num] [bigint] NULL,
  [x2d_18_min] decimal(12,3) NULL,
  [x2d_18_max] decimal(12,3) NULL,
  [x2d_18_acc] decimal(12,3) NULL,
  [x2d_18_avg] decimal(12,3) NULL,
  [x2d_18_num] [bigint] NULL,
  [x2d_19_min] decimal(12,3) NULL,
  [x2d_19_max] decimal(12,3) NULL,
  [x2d_19_acc] decimal(12,3) NULL,
  [x2d_19_avg] decimal(12,3) NULL,
  [x2d_19_num] [bigint] NULL,
  [x2d_20_min] decimal(12,3) NULL,
  [x2d_20_max] decimal(12,3) NULL,
  [x2d_20_acc] decimal(12,3) NULL,
  [x2d_20_avg] decimal(12,3) NULL,
  [x2d_20_num] [bigint] NULL,
  [x2d_21_min] decimal(12,3) NULL,
  [x2d_21_max] decimal(12,3) NULL,
  [x2d_21_acc] decimal(12,3) NULL,
  [x2d_21_avg] decimal(12,3) NULL,
  [x2d_21_num] [bigint] NULL,
  [x2d_22_min] decimal(12,3) NULL,
  [x2d_22_max] decimal(12,3) NULL,
  [x2d_22_acc] decimal(12,3) NULL,
  [x2d_22_avg] decimal(12,3) NULL,
  [x2d_22_num] [bigint] NULL,
  [x2d_23_min] decimal(12,3) NULL,
  [x2d_23_max] decimal(12,3) NULL,
  [x2d_23_acc] decimal(12,3) NULL,
  [x2d_23_avg] decimal(12,3) NULL,
  [x2d_23_num] [bigint] NULL,
  [x2d_dd_min] decimal(12,3) NULL,
  [x2d_dd_max] decimal(12,3) NULL,
  [x2d_dd_acc] decimal(12,3) NULL,
  [x2d_dd_avg] decimal(12,3) NULL,
  [x2d_dd_num] [bigint] NULL,
 CONSTRAINT [PK_T2D_SMH] PRIMARY KEY CLUSTERED 
(
  [x2d_date] ASC,
  [x2d_id] ASC,
  [x2d_meter_id] ASC,
  [x2d_meter_item] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/* */

CREATE TABLE [dbo].[h_t2d_tmh](
  [x2d_date] [int] NOT NULL,
  [x2d_weekday] [tinyint] NOT NULL,
  [x2d_id] [int] NOT NULL,
  [x2d_meter_id] [int] NOT NULL,
  [x2d_meter_item] [int] NOT NULL,
  [x2d_00_min] decimal(12,3) NULL,
  [x2d_00_max] decimal(12,3) NULL,
  [x2d_00_acc] decimal(12,3) NULL,
  [x2d_00_avg] decimal(12,3) NULL,
  [x2d_00_num] [bigint] NULL,
  [x2d_01_min] decimal(12,3) NULL,
  [x2d_01_max] decimal(12,3) NULL,
  [x2d_01_acc] decimal(12,3) NULL,
  [x2d_01_avg] decimal(12,3) NULL,
  [x2d_01_num] [bigint] NULL,
  [x2d_02_min] decimal(12,3) NULL,
  [x2d_02_max] decimal(12,3) NULL,
  [x2d_02_acc] decimal(12,3) NULL,
  [x2d_02_avg] decimal(12,3) NULL,
  [x2d_02_num] [bigint] NULL,
  [x2d_03_min] decimal(12,3) NULL,
  [x2d_03_max] decimal(12,3) NULL,
  [x2d_03_acc] decimal(12,3) NULL,
  [x2d_03_avg] decimal(12,3) NULL,
  [x2d_03_num] [bigint] NULL,
  [x2d_04_min] decimal(12,3) NULL,
  [x2d_04_max] decimal(12,3) NULL,
  [x2d_04_acc] decimal(12,3) NULL,
  [x2d_04_avg] decimal(12,3) NULL,
  [x2d_04_num] [bigint] NULL,
  [x2d_05_min] decimal(12,3) NULL,
  [x2d_05_max] decimal(12,3) NULL,
  [x2d_05_acc] decimal(12,3) NULL,
  [x2d_05_avg] decimal(12,3) NULL,
  [x2d_05_num] [bigint] NULL,
  [x2d_06_min] decimal(12,3) NULL,
  [x2d_06_max] decimal(12,3) NULL,
  [x2d_06_acc] decimal(12,3) NULL,
  [x2d_06_avg] decimal(12,3) NULL,
  [x2d_06_num] [bigint] NULL,
  [x2d_07_min] decimal(12,3) NULL,
  [x2d_07_max] decimal(12,3) NULL,
  [x2d_07_acc] decimal(12,3) NULL,
  [x2d_07_avg] decimal(12,3) NULL,
  [x2d_07_num] [bigint] NULL,
  [x2d_08_min] decimal(12,3) NULL,
  [x2d_08_max] decimal(12,3) NULL,
  [x2d_08_acc] decimal(12,3) NULL,
  [x2d_08_avg] decimal(12,3) NULL,
  [x2d_08_num] [bigint] NULL,
  [x2d_09_min] decimal(12,3) NULL,
  [x2d_09_max] decimal(12,3) NULL,
  [x2d_09_acc] decimal(12,3) NULL,
  [x2d_09_avg] decimal(12,3) NULL,
  [x2d_09_num] [bigint] NULL,
  [x2d_10_min] decimal(12,3) NULL,
  [x2d_10_max] decimal(12,3) NULL,
  [x2d_10_acc] decimal(12,3) NULL,
  [x2d_10_avg] decimal(12,3) NULL,
  [x2d_10_num] [bigint] NULL,
  [x2d_11_min] decimal(12,3) NULL,
  [x2d_11_max] decimal(12,3) NULL,
  [x2d_11_acc] decimal(12,3) NULL,
  [x2d_11_avg] decimal(12,3) NULL,
  [x2d_11_num] [bigint] NULL,
  [x2d_12_min] decimal(12,3) NULL,
  [x2d_12_max] decimal(12,3) NULL,
  [x2d_12_acc] decimal(12,3) NULL,
  [x2d_12_avg] decimal(12,3) NULL,
  [x2d_12_num] [bigint] NULL,
  [x2d_13_min] decimal(12,3) NULL,
  [x2d_13_max] decimal(12,3) NULL,
  [x2d_13_acc] decimal(12,3) NULL,
  [x2d_13_avg] decimal(12,3) NULL,
  [x2d_13_num] [bigint] NULL,
  [x2d_14_min] decimal(12,3) NULL,
  [x2d_14_max] decimal(12,3) NULL,
  [x2d_14_acc] decimal(12,3) NULL,
  [x2d_14_avg] decimal(12,3) NULL,
  [x2d_14_num] [bigint] NULL,
  [x2d_15_min] decimal(12,3) NULL,
  [x2d_15_max] decimal(12,3) NULL,
  [x2d_15_acc] decimal(12,3) NULL,
  [x2d_15_avg] decimal(12,3) NULL,
  [x2d_15_num] [bigint] NULL,
  [x2d_16_min] decimal(12,3) NULL,
  [x2d_16_max] decimal(12,3) NULL,
  [x2d_16_acc] decimal(12,3) NULL,
  [x2d_16_avg] decimal(12,3) NULL,
  [x2d_16_num] [bigint] NULL,
  [x2d_17_min] decimal(12,3) NULL,
  [x2d_17_max] decimal(12,3) NULL,
  [x2d_17_acc] decimal(12,3) NULL,
  [x2d_17_avg] decimal(12,3) NULL,
  [x2d_17_num] [bigint] NULL,
  [x2d_18_min] decimal(12,3) NULL,
  [x2d_18_max] decimal(12,3) NULL,
  [x2d_18_acc] decimal(12,3) NULL,
  [x2d_18_avg] decimal(12,3) NULL,
  [x2d_18_num] [bigint] NULL,
  [x2d_19_min] decimal(12,3) NULL,
  [x2d_19_max] decimal(12,3) NULL,
  [x2d_19_acc] decimal(12,3) NULL,
  [x2d_19_avg] decimal(12,3) NULL,
  [x2d_19_num] [bigint] NULL,
  [x2d_20_min] decimal(12,3) NULL,
  [x2d_20_max] decimal(12,3) NULL,
  [x2d_20_acc] decimal(12,3) NULL,
  [x2d_20_avg] decimal(12,3) NULL,
  [x2d_20_num] [bigint] NULL,
  [x2d_21_min] decimal(12,3) NULL,
  [x2d_21_max] decimal(12,3) NULL,
  [x2d_21_acc] decimal(12,3) NULL,
  [x2d_21_avg] decimal(12,3) NULL,
  [x2d_21_num] [bigint] NULL,
  [x2d_22_min] decimal(12,3) NULL,
  [x2d_22_max] decimal(12,3) NULL,
  [x2d_22_acc] decimal(12,3) NULL,
  [x2d_22_avg] decimal(12,3) NULL,
  [x2d_22_num] [bigint] NULL,
  [x2d_23_min] decimal(12,3) NULL,
  [x2d_23_max] decimal(12,3) NULL,
  [x2d_23_acc] decimal(12,3) NULL,
  [x2d_23_avg] decimal(12,3) NULL,
  [x2d_23_num] [bigint] NULL,
  [x2d_dd_min] decimal(12,3) NULL,
  [x2d_dd_max] decimal(12,3) NULL,
  [x2d_dd_acc] decimal(12,3) NULL,
  [x2d_dd_avg] decimal(12,3) NULL,
  [x2d_dd_num] [bigint] NULL,
 CONSTRAINT [PK_T2D_TMH] PRIMARY KEY CLUSTERED 
(
  [x2d_date] ASC,
  [x2d_id] ASC,
  [x2d_meter_id] ASC,
  [x2d_meter_item] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

/* */

CREATE TABLE [dbo].[h_w2d_smh](
  [x2d_date] [int] NOT NULL,
  [x2d_weekday] [tinyint] NOT NULL,
  [x2d_id] [int] NOT NULL,
  [x2d_meter_id] [int] NOT NULL,
  [x2d_meter_item] [int] NOT NULL,
  [x2d_00_min] decimal(12,3) NULL,
  [x2d_00_max] decimal(12,3) NULL,
  [x2d_00_acc] decimal(12,3) NULL,
  [x2d_00_avg] decimal(12,3) NULL,
  [x2d_00_num] [bigint] NULL,
  [x2d_01_min] decimal(12,3) NULL,
  [x2d_01_max] decimal(12,3) NULL,
  [x2d_01_acc] decimal(12,3) NULL,
  [x2d_01_avg] decimal(12,3) NULL,
  [x2d_01_num] [bigint] NULL,
  [x2d_02_min] decimal(12,3) NULL,
  [x2d_02_max] decimal(12,3) NULL,
  [x2d_02_acc] decimal(12,3) NULL,
  [x2d_02_avg] decimal(12,3) NULL,
  [x2d_02_num] [bigint] NULL,
  [x2d_03_min] decimal(12,3) NULL,
  [x2d_03_max] decimal(12,3) NULL,
  [x2d_03_acc] decimal(12,3) NULL,
  [x2d_03_avg] decimal(12,3) NULL,
  [x2d_03_num] [bigint] NULL,
  [x2d_04_min] decimal(12,3) NULL,
  [x2d_04_max] decimal(12,3) NULL,
  [x2d_04_acc] decimal(12,3) NULL,
  [x2d_04_avg] decimal(12,3) NULL,
  [x2d_04_num] [bigint] NULL,
  [x2d_05_min] decimal(12,3) NULL,
  [x2d_05_max] decimal(12,3) NULL,
  [x2d_05_acc] decimal(12,3) NULL,
  [x2d_05_avg] decimal(12,3) NULL,
  [x2d_05_num] [bigint] NULL,
  [x2d_06_min] decimal(12,3) NULL,
  [x2d_06_max] decimal(12,3) NULL,
  [x2d_06_acc] decimal(12,3) NULL,
  [x2d_06_avg] decimal(12,3) NULL,
  [x2d_06_num] [bigint] NULL,
  [x2d_07_min] decimal(12,3) NULL,
  [x2d_07_max] decimal(12,3) NULL,
  [x2d_07_acc] decimal(12,3) NULL,
  [x2d_07_avg] decimal(12,3) NULL,
  [x2d_07_num] [bigint] NULL,
  [x2d_08_min] decimal(12,3) NULL,
  [x2d_08_max] decimal(12,3) NULL,
  [x2d_08_acc] decimal(12,3) NULL,
  [x2d_08_avg] decimal(12,3) NULL,
  [x2d_08_num] [bigint] NULL,
  [x2d_09_min] decimal(12,3) NULL,
  [x2d_09_max] decimal(12,3) NULL,
  [x2d_09_acc] decimal(12,3) NULL,
  [x2d_09_avg] decimal(12,3) NULL,
  [x2d_09_num] [bigint] NULL,
  [x2d_10_min] decimal(12,3) NULL,
  [x2d_10_max] decimal(12,3) NULL,
  [x2d_10_acc] decimal(12,3) NULL,
  [x2d_10_avg] decimal(12,3) NULL,
  [x2d_10_num] [bigint] NULL,
  [x2d_11_min] decimal(12,3) NULL,
  [x2d_11_max] decimal(12,3) NULL,
  [x2d_11_acc] decimal(12,3) NULL,
  [x2d_11_avg] decimal(12,3) NULL,
  [x2d_11_num] [bigint] NULL,
  [x2d_12_min] decimal(12,3) NULL,
  [x2d_12_max] decimal(12,3) NULL,
  [x2d_12_acc] decimal(12,3) NULL,
  [x2d_12_avg] decimal(12,3) NULL,
  [x2d_12_num] [bigint] NULL,
  [x2d_13_min] decimal(12,3) NULL,
  [x2d_13_max] decimal(12,3) NULL,
  [x2d_13_acc] decimal(12,3) NULL,
  [x2d_13_avg] decimal(12,3) NULL,
  [x2d_13_num] [bigint] NULL,
  [x2d_14_min] decimal(12,3) NULL,
  [x2d_14_max] decimal(12,3) NULL,
  [x2d_14_acc] decimal(12,3) NULL,
  [x2d_14_avg] decimal(12,3) NULL,
  [x2d_14_num] [bigint] NULL,
  [x2d_15_min] decimal(12,3) NULL,
  [x2d_15_max] decimal(12,3) NULL,
  [x2d_15_acc] decimal(12,3) NULL,
  [x2d_15_avg] decimal(12,3) NULL,
  [x2d_15_num] [bigint] NULL,
  [x2d_16_min] decimal(12,3) NULL,
  [x2d_16_max] decimal(12,3) NULL,
  [x2d_16_acc] decimal(12,3) NULL,
  [x2d_16_avg] decimal(12,3) NULL,
  [x2d_16_num] [bigint] NULL,
  [x2d_17_min] decimal(12,3) NULL,
  [x2d_17_max] decimal(12,3) NULL,
  [x2d_17_acc] decimal(12,3) NULL,
  [x2d_17_avg] decimal(12,3) NULL,
  [x2d_17_num] [bigint] NULL,
  [x2d_18_min] decimal(12,3) NULL,
  [x2d_18_max] decimal(12,3) NULL,
  [x2d_18_acc] decimal(12,3) NULL,
  [x2d_18_avg] decimal(12,3) NULL,
  [x2d_18_num] [bigint] NULL,
  [x2d_19_min] decimal(12,3) NULL,
  [x2d_19_max] decimal(12,3) NULL,
  [x2d_19_acc] decimal(12,3) NULL,
  [x2d_19_avg] decimal(12,3) NULL,
  [x2d_19_num] [bigint] NULL,
  [x2d_20_min] decimal(12,3) NULL,
  [x2d_20_max] decimal(12,3) NULL,
  [x2d_20_acc] decimal(12,3) NULL,
  [x2d_20_avg] decimal(12,3) NULL,
  [x2d_20_num] [bigint] NULL,
  [x2d_21_min] decimal(12,3) NULL,
  [x2d_21_max] decimal(12,3) NULL,
  [x2d_21_acc] decimal(12,3) NULL,
  [x2d_21_avg] decimal(12,3) NULL,
  [x2d_21_num] [bigint] NULL,
  [x2d_22_min] decimal(12,3) NULL,
  [x2d_22_max] decimal(12,3) NULL,
  [x2d_22_acc] decimal(12,3) NULL,
  [x2d_22_avg] decimal(12,3) NULL,
  [x2d_22_num] [bigint] NULL,
  [x2d_23_min] decimal(12,3) NULL,
  [x2d_23_max] decimal(12,3) NULL,
  [x2d_23_acc] decimal(12,3) NULL,
  [x2d_23_avg] decimal(12,3) NULL,
  [x2d_23_num] [bigint] NULL,
  [x2d_dd_min] decimal(12,3) NULL,
  [x2d_dd_max] decimal(12,3) NULL,
  [x2d_dd_acc] decimal(12,3) NULL,
  [x2d_dd_avg] decimal(12,3) NULL,
  [x2d_dd_num] [bigint] NULL,
 CONSTRAINT [PK_W2D_SMH] PRIMARY KEY CLUSTERED 
(
  [x2d_date] ASC,
  [x2d_weekday] ASC,
  [x2d_id] ASC,
  [x2d_meter_id] ASC,
  [x2d_meter_item] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/* */

CREATE TABLE [dbo].[h_w2d_tmh](
  [x2d_date] [int] NOT NULL,
  [x2d_weekday] [tinyint] NOT NULL,
  [x2d_id] [int] NOT NULL,
  [x2d_meter_id] [int] NOT NULL,
  [x2d_meter_item] [int] NOT NULL,
  [x2d_00_min] decimal(12,3) NULL,
  [x2d_00_max] decimal(12,3) NULL,
  [x2d_00_acc] decimal(12,3) NULL,
  [x2d_00_avg] decimal(12,3) NULL,
  [x2d_00_num] [bigint] NULL,
  [x2d_01_min] decimal(12,3) NULL,
  [x2d_01_max] decimal(12,3) NULL,
  [x2d_01_acc] decimal(12,3) NULL,
  [x2d_01_avg] decimal(12,3) NULL,
  [x2d_01_num] [bigint] NULL,
  [x2d_02_min] decimal(12,3) NULL,
  [x2d_02_max] decimal(12,3) NULL,
  [x2d_02_acc] decimal(12,3) NULL,
  [x2d_02_avg] decimal(12,3) NULL,
  [x2d_02_num] [bigint] NULL,
  [x2d_03_min] decimal(12,3) NULL,
  [x2d_03_max] decimal(12,3) NULL,
  [x2d_03_acc] decimal(12,3) NULL,
  [x2d_03_avg] decimal(12,3) NULL,
  [x2d_03_num] [bigint] NULL,
  [x2d_04_min] decimal(12,3) NULL,
  [x2d_04_max] decimal(12,3) NULL,
  [x2d_04_acc] decimal(12,3) NULL,
  [x2d_04_avg] decimal(12,3) NULL,
  [x2d_04_num] [bigint] NULL,
  [x2d_05_min] decimal(12,3) NULL,
  [x2d_05_max] decimal(12,3) NULL,
  [x2d_05_acc] decimal(12,3) NULL,
  [x2d_05_avg] decimal(12,3) NULL,
  [x2d_05_num] [bigint] NULL,
  [x2d_06_min] decimal(12,3) NULL,
  [x2d_06_max] decimal(12,3) NULL,
  [x2d_06_acc] decimal(12,3) NULL,
  [x2d_06_avg] decimal(12,3) NULL,
  [x2d_06_num] [bigint] NULL,
  [x2d_07_min] decimal(12,3) NULL,
  [x2d_07_max] decimal(12,3) NULL,
  [x2d_07_acc] decimal(12,3) NULL,
  [x2d_07_avg] decimal(12,3) NULL,
  [x2d_07_num] [bigint] NULL,
  [x2d_08_min] decimal(12,3) NULL,
  [x2d_08_max] decimal(12,3) NULL,
  [x2d_08_acc] decimal(12,3) NULL,
  [x2d_08_avg] decimal(12,3) NULL,
  [x2d_08_num] [bigint] NULL,
  [x2d_09_min] decimal(12,3) NULL,
  [x2d_09_max] decimal(12,3) NULL,
  [x2d_09_acc] decimal(12,3) NULL,
  [x2d_09_avg] decimal(12,3) NULL,
  [x2d_09_num] [bigint] NULL,
  [x2d_10_min] decimal(12,3) NULL,
  [x2d_10_max] decimal(12,3) NULL,
  [x2d_10_acc] decimal(12,3) NULL,
  [x2d_10_avg] decimal(12,3) NULL,
  [x2d_10_num] [bigint] NULL,
  [x2d_11_min] decimal(12,3) NULL,
  [x2d_11_max] decimal(12,3) NULL,
  [x2d_11_acc] decimal(12,3) NULL,
  [x2d_11_avg] decimal(12,3) NULL,
  [x2d_11_num] [bigint] NULL,
  [x2d_12_min] decimal(12,3) NULL,
  [x2d_12_max] decimal(12,3) NULL,
  [x2d_12_acc] decimal(12,3) NULL,
  [x2d_12_avg] decimal(12,3) NULL,
  [x2d_12_num] [bigint] NULL,
  [x2d_13_min] decimal(12,3) NULL,
  [x2d_13_max] decimal(12,3) NULL,
  [x2d_13_acc] decimal(12,3) NULL,
  [x2d_13_avg] decimal(12,3) NULL,
  [x2d_13_num] [bigint] NULL,
  [x2d_14_min] decimal(12,3) NULL,
  [x2d_14_max] decimal(12,3) NULL,
  [x2d_14_acc] decimal(12,3) NULL,
  [x2d_14_avg] decimal(12,3) NULL,
  [x2d_14_num] [bigint] NULL,
  [x2d_15_min] decimal(12,3) NULL,
  [x2d_15_max] decimal(12,3) NULL,
  [x2d_15_acc] decimal(12,3) NULL,
  [x2d_15_avg] decimal(12,3) NULL,
  [x2d_15_num] [bigint] NULL,
  [x2d_16_min] decimal(12,3) NULL,
  [x2d_16_max] decimal(12,3) NULL,
  [x2d_16_acc] decimal(12,3) NULL,
  [x2d_16_avg] decimal(12,3) NULL,
  [x2d_16_num] [bigint] NULL,
  [x2d_17_min] decimal(12,3) NULL,
  [x2d_17_max] decimal(12,3) NULL,
  [x2d_17_acc] decimal(12,3) NULL,
  [x2d_17_avg] decimal(12,3) NULL,
  [x2d_17_num] [bigint] NULL,
  [x2d_18_min] decimal(12,3) NULL,
  [x2d_18_max] decimal(12,3) NULL,
  [x2d_18_acc] decimal(12,3) NULL,
  [x2d_18_avg] decimal(12,3) NULL,
  [x2d_18_num] [bigint] NULL,
  [x2d_19_min] decimal(12,3) NULL,
  [x2d_19_max] decimal(12,3) NULL,
  [x2d_19_acc] decimal(12,3) NULL,
  [x2d_19_avg] decimal(12,3) NULL,
  [x2d_19_num] [bigint] NULL,
  [x2d_20_min] decimal(12,3) NULL,
  [x2d_20_max] decimal(12,3) NULL,
  [x2d_20_acc] decimal(12,3) NULL,
  [x2d_20_avg] decimal(12,3) NULL,
  [x2d_20_num] [bigint] NULL,
  [x2d_21_min] decimal(12,3) NULL,
  [x2d_21_max] decimal(12,3) NULL,
  [x2d_21_acc] decimal(12,3) NULL,
  [x2d_21_avg] decimal(12,3) NULL,
  [x2d_21_num] [bigint] NULL,
  [x2d_22_min] decimal(12,3) NULL,
  [x2d_22_max] decimal(12,3) NULL,
  [x2d_22_acc] decimal(12,3) NULL,
  [x2d_22_avg] decimal(12,3) NULL,
  [x2d_22_num] [bigint] NULL,
  [x2d_23_min] decimal(12,3) NULL,
  [x2d_23_max] decimal(12,3) NULL,
  [x2d_23_acc] decimal(12,3) NULL,
  [x2d_23_avg] decimal(12,3) NULL,
  [x2d_23_num] [bigint] NULL,
  [x2d_dd_min] decimal(12,3) NULL,
  [x2d_dd_max] decimal(12,3) NULL,
  [x2d_dd_acc] decimal(12,3) NULL,
  [x2d_dd_avg] decimal(12,3) NULL,
  [x2d_dd_num] [bigint] NULL,
 CONSTRAINT [PK_W2D_TMH] PRIMARY KEY CLUSTERED 
(
  [x2d_date] ASC,
  [x2d_id] ASC,
  [x2d_meter_id] ASC,
  [x2d_meter_item] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/* */

CREATE TABLE [dbo].[h_x2d_control](
  [x2d_date] [datetime] NOT NULL,
  [x2d_meter_type] [int] NOT NULL,
  [x2d_execution] [datetime] NULL,
 CONSTRAINT [PK_H_X2D_CONTROL] PRIMARY KEY CLUSTERED 
(
  [x2d_date] DESC,
  [x2d_meter_type] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/* */

CREATE TABLE [dbo].[h_y2d_smh](
  [x2d_date] [int] NOT NULL,
  [x2d_weekday] [tinyint] NOT NULL,
  [x2d_id] [int] NOT NULL,
  [x2d_meter_id] [int] NOT NULL,
  [x2d_meter_item] [int] NOT NULL,
  [x2d_00_min] decimal(12,3) NULL,
  [x2d_00_max] decimal(12,3) NULL,
  [x2d_00_acc] decimal(12,3) NULL,
  [x2d_00_avg] decimal(12,3) NULL,
  [x2d_00_num] [bigint] NULL,
  [x2d_01_min] decimal(12,3) NULL,
  [x2d_01_max] decimal(12,3) NULL,
  [x2d_01_acc] decimal(12,3) NULL,
  [x2d_01_avg] decimal(12,3) NULL,
  [x2d_01_num] [bigint] NULL,
  [x2d_02_min] decimal(12,3) NULL,
  [x2d_02_max] decimal(12,3) NULL,
  [x2d_02_acc] decimal(12,3) NULL,
  [x2d_02_avg] decimal(12,3) NULL,
  [x2d_02_num] [bigint] NULL,
  [x2d_03_min] decimal(12,3) NULL,
  [x2d_03_max] decimal(12,3) NULL,
  [x2d_03_acc] decimal(12,3) NULL,
  [x2d_03_avg] decimal(12,3) NULL,
  [x2d_03_num] [bigint] NULL,
  [x2d_04_min] decimal(12,3) NULL,
  [x2d_04_max] decimal(12,3) NULL,
  [x2d_04_acc] decimal(12,3) NULL,
  [x2d_04_avg] decimal(12,3) NULL,
  [x2d_04_num] [bigint] NULL,
  [x2d_05_min] decimal(12,3) NULL,
  [x2d_05_max] decimal(12,3) NULL,
  [x2d_05_acc] decimal(12,3) NULL,
  [x2d_05_avg] decimal(12,3) NULL,
  [x2d_05_num] [bigint] NULL,
  [x2d_06_min] decimal(12,3) NULL,
  [x2d_06_max] decimal(12,3) NULL,
  [x2d_06_acc] decimal(12,3) NULL,
  [x2d_06_avg] decimal(12,3) NULL,
  [x2d_06_num] [bigint] NULL,
  [x2d_07_min] decimal(12,3) NULL,
  [x2d_07_max] decimal(12,3) NULL,
  [x2d_07_acc] decimal(12,3) NULL,
  [x2d_07_avg] decimal(12,3) NULL,
  [x2d_07_num] [bigint] NULL,
  [x2d_08_min] decimal(12,3) NULL,
  [x2d_08_max] decimal(12,3) NULL,
  [x2d_08_acc] decimal(12,3) NULL,
  [x2d_08_avg] decimal(12,3) NULL,
  [x2d_08_num] [bigint] NULL,
  [x2d_09_min] decimal(12,3) NULL,
  [x2d_09_max] decimal(12,3) NULL,
  [x2d_09_acc] decimal(12,3) NULL,
  [x2d_09_avg] decimal(12,3) NULL,
  [x2d_09_num] [bigint] NULL,
  [x2d_10_min] decimal(12,3) NULL,
  [x2d_10_max] decimal(12,3) NULL,
  [x2d_10_acc] decimal(12,3) NULL,
  [x2d_10_avg] decimal(12,3) NULL,
  [x2d_10_num] [bigint] NULL,
  [x2d_11_min] decimal(12,3) NULL,
  [x2d_11_max] decimal(12,3) NULL,
  [x2d_11_acc] decimal(12,3) NULL,
  [x2d_11_avg] decimal(12,3) NULL,
  [x2d_11_num] [bigint] NULL,
  [x2d_12_min] decimal(12,3) NULL,
  [x2d_12_max] decimal(12,3) NULL,
  [x2d_12_acc] decimal(12,3) NULL,
  [x2d_12_avg] decimal(12,3) NULL,
  [x2d_12_num] [bigint] NULL,
  [x2d_13_min] decimal(12,3) NULL,
  [x2d_13_max] decimal(12,3) NULL,
  [x2d_13_acc] decimal(12,3) NULL,
  [x2d_13_avg] decimal(12,3) NULL,
  [x2d_13_num] [bigint] NULL,
  [x2d_14_min] decimal(12,3) NULL,
  [x2d_14_max] decimal(12,3) NULL,
  [x2d_14_acc] decimal(12,3) NULL,
  [x2d_14_avg] decimal(12,3) NULL,
  [x2d_14_num] [bigint] NULL,
  [x2d_15_min] decimal(12,3) NULL,
  [x2d_15_max] decimal(12,3) NULL,
  [x2d_15_acc] decimal(12,3) NULL,
  [x2d_15_avg] decimal(12,3) NULL,
  [x2d_15_num] [bigint] NULL,
  [x2d_16_min] decimal(12,3) NULL,
  [x2d_16_max] decimal(12,3) NULL,
  [x2d_16_acc] decimal(12,3) NULL,
  [x2d_16_avg] decimal(12,3) NULL,
  [x2d_16_num] [bigint] NULL,
  [x2d_17_min] decimal(12,3) NULL,
  [x2d_17_max] decimal(12,3) NULL,
  [x2d_17_acc] decimal(12,3) NULL,
  [x2d_17_avg] decimal(12,3) NULL,
  [x2d_17_num] [bigint] NULL,
  [x2d_18_min] decimal(12,3) NULL,
  [x2d_18_max] decimal(12,3) NULL,
  [x2d_18_acc] decimal(12,3) NULL,
  [x2d_18_avg] decimal(12,3) NULL,
  [x2d_18_num] [bigint] NULL,
  [x2d_19_min] decimal(12,3) NULL,
  [x2d_19_max] decimal(12,3) NULL,
  [x2d_19_acc] decimal(12,3) NULL,
  [x2d_19_avg] decimal(12,3) NULL,
  [x2d_19_num] [bigint] NULL,
  [x2d_20_min] decimal(12,3) NULL,
  [x2d_20_max] decimal(12,3) NULL,
  [x2d_20_acc] decimal(12,3) NULL,
  [x2d_20_avg] decimal(12,3) NULL,
  [x2d_20_num] [bigint] NULL,
  [x2d_21_min] decimal(12,3) NULL,
  [x2d_21_max] decimal(12,3) NULL,
  [x2d_21_acc] decimal(12,3) NULL,
  [x2d_21_avg] decimal(12,3) NULL,
  [x2d_21_num] [bigint] NULL,
  [x2d_22_min] decimal(12,3) NULL,
  [x2d_22_max] decimal(12,3) NULL,
  [x2d_22_acc] decimal(12,3) NULL,
  [x2d_22_avg] decimal(12,3) NULL,
  [x2d_22_num] [bigint] NULL,
  [x2d_23_min] decimal(12,3) NULL,
  [x2d_23_max] decimal(12,3) NULL,
  [x2d_23_acc] decimal(12,3) NULL,
  [x2d_23_avg] decimal(12,3) NULL,
  [x2d_23_num] [bigint] NULL,
  [x2d_dd_min] decimal(12,3) NULL,
  [x2d_dd_max] decimal(12,3) NULL,
  [x2d_dd_acc] decimal(12,3) NULL,
  [x2d_dd_avg] decimal(12,3) NULL,
  [x2d_dd_num] [bigint] NULL,
 CONSTRAINT [PK_Y2D_SMH] PRIMARY KEY CLUSTERED 
(
  [x2d_date] ASC,
  [x2d_weekday] ASC,
  [x2d_id] ASC,
  [x2d_meter_id] ASC,
  [x2d_meter_item] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/* */

CREATE TABLE [dbo].[h_y2d_tmh](
  [x2d_date] [int] NOT NULL,
  [x2d_weekday] [tinyint] NOT NULL,
  [x2d_id] [int] NOT NULL,
  [x2d_meter_id] [int] NOT NULL,
  [x2d_meter_item] [int] NOT NULL,
  [x2d_00_min] decimal(12,3) NULL,
  [x2d_00_max] decimal(12,3) NULL,
  [x2d_00_acc] decimal(12,3) NULL,
  [x2d_00_avg] decimal(12,3) NULL,
  [x2d_00_num] [bigint] NULL,
  [x2d_01_min] decimal(12,3) NULL,
  [x2d_01_max] decimal(12,3) NULL,
  [x2d_01_acc] decimal(12,3) NULL,
  [x2d_01_avg] decimal(12,3) NULL,
  [x2d_01_num] [bigint] NULL,
  [x2d_02_min] decimal(12,3) NULL,
  [x2d_02_max] decimal(12,3) NULL,
  [x2d_02_acc] decimal(12,3) NULL,
  [x2d_02_avg] decimal(12,3) NULL,
  [x2d_02_num] [bigint] NULL,
  [x2d_03_min] decimal(12,3) NULL,
  [x2d_03_max] decimal(12,3) NULL,
  [x2d_03_acc] decimal(12,3) NULL,
  [x2d_03_avg] decimal(12,3) NULL,
  [x2d_03_num] [bigint] NULL,
  [x2d_04_min] decimal(12,3) NULL,
  [x2d_04_max] decimal(12,3) NULL,
  [x2d_04_acc] decimal(12,3) NULL,
  [x2d_04_avg] decimal(12,3) NULL,
  [x2d_04_num] [bigint] NULL,
  [x2d_05_min] decimal(12,3) NULL,
  [x2d_05_max] decimal(12,3) NULL,
  [x2d_05_acc] decimal(12,3) NULL,
  [x2d_05_avg] decimal(12,3) NULL,
  [x2d_05_num] [bigint] NULL,
  [x2d_06_min] decimal(12,3) NULL,
  [x2d_06_max] decimal(12,3) NULL,
  [x2d_06_acc] decimal(12,3) NULL,
  [x2d_06_avg] decimal(12,3) NULL,
  [x2d_06_num] [bigint] NULL,
  [x2d_07_min] decimal(12,3) NULL,
  [x2d_07_max] decimal(12,3) NULL,
  [x2d_07_acc] decimal(12,3) NULL,
  [x2d_07_avg] decimal(12,3) NULL,
  [x2d_07_num] [bigint] NULL,
  [x2d_08_min] decimal(12,3) NULL,
  [x2d_08_max] decimal(12,3) NULL,
  [x2d_08_acc] decimal(12,3) NULL,
  [x2d_08_avg] decimal(12,3) NULL,
  [x2d_08_num] [bigint] NULL,
  [x2d_09_min] decimal(12,3) NULL,
  [x2d_09_max] decimal(12,3) NULL,
  [x2d_09_acc] decimal(12,3) NULL,
  [x2d_09_avg] decimal(12,3) NULL,
  [x2d_09_num] [bigint] NULL,
  [x2d_10_min] decimal(12,3) NULL,
  [x2d_10_max] decimal(12,3) NULL,
  [x2d_10_acc] decimal(12,3) NULL,
  [x2d_10_avg] decimal(12,3) NULL,
  [x2d_10_num] [bigint] NULL,
  [x2d_11_min] decimal(12,3) NULL,
  [x2d_11_max] decimal(12,3) NULL,
  [x2d_11_acc] decimal(12,3) NULL,
  [x2d_11_avg] decimal(12,3) NULL,
  [x2d_11_num] [bigint] NULL,
  [x2d_12_min] decimal(12,3) NULL,
  [x2d_12_max] decimal(12,3) NULL,
  [x2d_12_acc] decimal(12,3) NULL,
  [x2d_12_avg] decimal(12,3) NULL,
  [x2d_12_num] [bigint] NULL,
  [x2d_13_min] decimal(12,3) NULL,
  [x2d_13_max] decimal(12,3) NULL,
  [x2d_13_acc] decimal(12,3) NULL,
  [x2d_13_avg] decimal(12,3) NULL,
  [x2d_13_num] [bigint] NULL,
  [x2d_14_min] decimal(12,3) NULL,
  [x2d_14_max] decimal(12,3) NULL,
  [x2d_14_acc] decimal(12,3) NULL,
  [x2d_14_avg] decimal(12,3) NULL,
  [x2d_14_num] [bigint] NULL,
  [x2d_15_min] decimal(12,3) NULL,
  [x2d_15_max] decimal(12,3) NULL,
  [x2d_15_acc] decimal(12,3) NULL,
  [x2d_15_avg] decimal(12,3) NULL,
  [x2d_15_num] [bigint] NULL,
  [x2d_16_min] decimal(12,3) NULL,
  [x2d_16_max] decimal(12,3) NULL,
  [x2d_16_acc] decimal(12,3) NULL,
  [x2d_16_avg] decimal(12,3) NULL,
  [x2d_16_num] [bigint] NULL,
  [x2d_17_min] decimal(12,3) NULL,
  [x2d_17_max] decimal(12,3) NULL,
  [x2d_17_acc] decimal(12,3) NULL,
  [x2d_17_avg] decimal(12,3) NULL,
  [x2d_17_num] [bigint] NULL,
  [x2d_18_min] decimal(12,3) NULL,
  [x2d_18_max] decimal(12,3) NULL,
  [x2d_18_acc] decimal(12,3) NULL,
  [x2d_18_avg] decimal(12,3) NULL,
  [x2d_18_num] [bigint] NULL,
  [x2d_19_min] decimal(12,3) NULL,
  [x2d_19_max] decimal(12,3) NULL,
  [x2d_19_acc] decimal(12,3) NULL,
  [x2d_19_avg] decimal(12,3) NULL,
  [x2d_19_num] [bigint] NULL,
  [x2d_20_min] decimal(12,3) NULL,
  [x2d_20_max] decimal(12,3) NULL,
  [x2d_20_acc] decimal(12,3) NULL,
  [x2d_20_avg] decimal(12,3) NULL,
  [x2d_20_num] [bigint] NULL,
  [x2d_21_min] decimal(12,3) NULL,
  [x2d_21_max] decimal(12,3) NULL,
  [x2d_21_acc] decimal(12,3) NULL,
  [x2d_21_avg] decimal(12,3) NULL,
  [x2d_21_num] [bigint] NULL,
  [x2d_22_min] decimal(12,3) NULL,
  [x2d_22_max] decimal(12,3) NULL,
  [x2d_22_acc] decimal(12,3) NULL,
  [x2d_22_avg] decimal(12,3) NULL,
  [x2d_22_num] [bigint] NULL,
  [x2d_23_min] decimal(12,3) NULL,
  [x2d_23_max] decimal(12,3) NULL,
  [x2d_23_acc] decimal(12,3) NULL,
  [x2d_23_avg] decimal(12,3) NULL,
  [x2d_23_num] [bigint] NULL,
  [x2d_dd_min] decimal(12,3) NULL,
  [x2d_dd_max] decimal(12,3) NULL,
  [x2d_dd_acc] decimal(12,3) NULL,
  [x2d_dd_avg] decimal(12,3) NULL,
  [x2d_dd_num] [bigint] NULL,
 CONSTRAINT [PK_Y2D_TMH] PRIMARY KEY CLUSTERED 
(
  [x2d_date] ASC,
  [x2d_id] ASC,
  [x2d_meter_id] ASC,
  [x2d_meter_item] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/* ---------------- GENERAL PARAMS ---------------- */

IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'IX_hp_play_session_id' AND object_id = OBJECT_ID('handpays'))
    BEGIN
      CREATE NONCLUSTERED INDEX [IX_hp_play_session_id] ON [dbo].[handpays]
      ([hp_play_session_id] ASC) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
  END
GO

/********************************************/