/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 141;

SET @New_ReleaseId = 142;
SET @New_ScriptName = N'UpdateTo_18.142.027.sql';
SET @New_Description = N'Protect TPL_GAME_ID ISNOTNULL in InsertGamePlaySessions';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END
 
/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO


/****** STORED PROCEDURES ******/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.InsertGamePlaySessions') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.InsertGamePlaySessions
GO

CREATE PROCEDURE [dbo].[InsertGamePlaySessions] 
  @pPlaySessionId BIGINT
AS
BEGIN
  DECLARE @PlaysCount                   AS INT
  DECLARE @StatusOpen                   AS INT
  
  DECLARE @RedeemablePlayed             AS MONEY
  DECLARE @NonRedeemablePlayed          AS MONEY
  DECLARE @AccountId                    AS BIGINT
  DECLARE @TerminalId                   AS INT
  DECLARE @PlayedCount                  AS INT
  DECLARE @PlayedAmount                 AS MONEY
  DECLARE @WonCount                     AS INT
  DECLARE @WonAmount                    AS MONEY
  DECLARE @RedeemableWon                AS MONEY
  DECLARE @RedeemableCashIn             AS MONEY
  DECLARE @RedeemableCashOut            AS MONEY
  DECLARE @NonRedeemableWon             AS MONEY
  DECLARE @NonRedeemableCashIn          AS MONEY
  DECLARE @NonRedeemableCashOut         AS MONEY
  DECLARE @GameId                       AS INT
  DECLARE @PsStarted                    AS DATETIME
  DECLARE @PsFinished                   AS DATETIME

  SET @StatusOpen = 0  --- OPEN


  SELECT   PL_PLAY_SESSION_ID                                 AS GFP_PLAY_SESSION_ID
         , ISNULL(TGT_TRANSLATED_GAME_ID,0)                   AS GFP_GAME_ID
         , MIN (CASE WHEN tgt_payout_idx =  1 THEN pg_payout_1
                     WHEN tgt_payout_idx =  2 THEN pg_payout_2
                     WHEN tgt_payout_idx =  3 THEN pg_payout_3
                     WHEN tgt_payout_idx =  4 THEN pg_payout_4
                     WHEN tgt_payout_idx =  5 THEN pg_payout_5
                     WHEN tgt_payout_idx =  6 THEN pg_payout_6
                     WHEN tgt_payout_idx =  7 THEN pg_payout_7
                     WHEN tgt_payout_idx =  8 THEN pg_payout_8
                     WHEN tgt_payout_idx =  9 THEN pg_payout_9
                     WHEN tgt_payout_idx = 10 THEN pg_payout_10
                     ELSE CAST(0.00 AS MONEY)
                     END)                                     AS GFP_PAYOUT
         , SUM(1)                                             AS GFP_PLAYED_COUNT
         , SUM(PL_PLAYED_AMOUNT)                              AS GFP_PLAYED_AMOUNT
         , SUM(CASE WHEN PL_WON_AMOUNT > 0 THEN 1 ELSE 0 END) AS GFP_WON_COUNT
         , SUM(PL_WON_AMOUNT)                                 AS GFP_WON_AMOUNT     
    INTO   #GAMES_FOR_PLAY_SESSION
    FROM   PLAYS
    LEFT   OUTER JOIN TERMINAL_GAME_TRANSLATION ON   TGT_SOURCE_GAME_ID = PL_GAME_ID 
                                               AND   TGT_TERMINAL_ID    = PL_TERMINAL_ID
    LEFT   OUTER JOIN PROVIDERS_GAMES           ON   PG_GAME_ID =  ISNULL(TGT_TRANSLATED_GAME_ID,0)   
   WHERE   PL_PLAY_SESSION_ID = @pPlaySessionId
     AND   TGT_SOURCE_GAME_ID = PL_GAME_ID
     AND   TGT_TERMINAL_ID    = PL_TERMINAL_ID
     AND   PG_GAME_ID         = ISNULL(TGT_TRANSLATED_GAME_ID,0)
   GROUP   BY PL_PLAY_SESSION_ID
         , ISNULL(TGT_TRANSLATED_GAME_ID,0)
   ORDER   BY GFP_PLAYED_AMOUNT DESC, GFP_PLAYED_COUNT DESC

  SELECT   PS_PLAY_SESSION_ID          AS TPS_PLAY_SESSION_ID
         , PS_ACCOUNT_ID               AS TPS_ACCOUNT_ID
         , PS_TERMINAL_ID              AS TPS_TERMINAL_ID
         , PS_PLAYED_COUNT             AS TPS_PLAYED_COUNT
         , PS_PLAYED_AMOUNT            AS TPS_PLAYED_AMOUNT
         , PS_WON_COUNT                AS TPS_WON_COUNT
         , PS_WON_AMOUNT               AS TPS_WON_AMOUNT
         , ISNULL(GFP_GAME_ID,0)       AS TPL_GAME_ID  --Can be NULL when no plays!!
         , GFP_PAYOUT                  as TPL_PAYOUT
         , GFP_PLAYED_COUNT            AS TPL_PLAYED_COUNT
         , GFP_PLAYED_AMOUNT           AS TPL_PLAYED_AMOUNT
         , GFP_WON_COUNT               AS TPL_WON_COUNT
         , GFP_WON_AMOUNT              AS TPL_WON_AMOUNT
         , PS_REDEEMABLE_PLAYED        AS TPS_REDEEMABLE_PLAYED
         , PS_REDEEMABLE_WON           AS TPS_REDEEMABLE_WON
         , PS_REDEEMABLE_CASH_IN       AS TPS_REDEEMABLE_CASH_IN
         , PS_REDEEMABLE_CASH_OUT      AS TPS_REDEEMABLE_CASH_OUT
         , PS_NON_REDEEMABLE_PLAYED    AS TPS_NON_REDEEMABLE_PLAYED  
         , PS_NON_REDEEMABLE_WON       AS TPS_NON_REDEEMABLE_WON
         , PS_NON_REDEEMABLE_CASH_IN   AS TPS_NON_REDEEMABLE_CASH_IN
         , PS_NON_REDEEMABLE_CASH_OUT  AS TPS_NON_REDEEMABLE_CASH_OUT
         , PS_STARTED                  AS TPS_STARTED
         , PS_FINISHED                 AS TPS_FINISHED 
    INTO   #PLAYS_SESSIONS_PLAYS_TEMP 
    FROM   PLAY_SESSIONS
    LEFT   OUTER JOIN #GAMES_FOR_PLAY_SESSION         ON GFP_PLAY_SESSION_ID  = PS_PLAY_SESSION_ID 
   WHERE   PS_PLAY_SESSION_ID = @pPlaySessionId
   ORDER   BY TPS_PLAYED_AMOUNT DESC, TPS_PLAYED_COUNT  DESC
 
   DROP TABLE #GAMES_FOR_PLAY_SESSION
 
   DECLARE  GamesCountPerPlaySession CURSOR FOR 
   SELECT   COUNT(*)
     FROM   #PLAYS_SESSIONS_PLAYS_TEMP          

  OPEN GamesCountPerPlaySession

  FETCH NEXT FROM GamesCountPerPlaySession INTO @PlaysCount 

  WHILE @@FETCH_STATUS = 0
  BEGIN
    
    INSERT INTO   GAME_PLAY_SESSIONS
                ( GPS_PLAY_SESSION_ID
                , GPS_ACCOUNT_ID
                , GPS_TERMINAL_ID
                , GPS_GAME_ID
                , GPS_PLAYED_COUNT
                , GPS_PLAYED_AMOUNT
                , GPS_WON_COUNT
                , GPS_WON_AMOUNT
                , GPS_PAYOUT )
         SELECT   TPS_PLAY_SESSION_ID   -- From PLAY_SESSIONS
                , TPS_ACCOUNT_ID        -- From PLAY_SESSIONS
                , TPS_TERMINAL_ID       -- From PLAY_SESSIONS
                , ISNULL(TPL_GAME_ID,0) -- GAME_ID translated!!!
                , CASE WHEN (@PlaysCount=1)
                       THEN TPS_PLAYED_COUNT
                       ELSE TPL_PLAYED_COUNT 
                       END 
                , CASE WHEN (@PlaysCount=1)
                       THEN TPS_PLAYED_AMOUNT
                       ELSE TPL_PLAYED_AMOUNT 
                       END 
                , CASE WHEN (@PlaysCount=1)
                       THEN TPS_WON_COUNT
                       ELSE TPL_WON_COUNT 
                       END 
                , CASE WHEN (@PlaysCount=1)
                       THEN TPS_WON_AMOUNT
                       ELSE TPL_WON_AMOUNT 
                       END 
                , TPL_PAYOUT
           FROM   #PLAYS_SESSIONS_PLAYS_TEMP
          WHERE   TPS_PLAY_SESSION_ID = @pPlaySessionId
            AND   CASE WHEN (@PlaysCount=1) THEN TPS_PLAYED_COUNT ELSE TPL_PLAYED_COUNT END > 0 -- One record, should select play session!
          ORDER   BY TPS_PLAY_SESSION_ID
                , TPS_ACCOUNT_ID
                , TPS_TERMINAL_ID
                , TPL_GAME_ID
                   
          
       DELETE  FROM MS_PENDING_GAME_PLAY_SESSIONS WHERE MPS_PLAY_SESSION_ID = @pPlaySessionId
          
    FETCH NEXT FROM GamesCountPerPlaySession INTO @PlaysCount
  END

  CLOSE GamesCountPerPlaySession
  DEALLOCATE GamesCountPerPlaySession
  
  IF (ISNULL((SELECT gp_key_value FROM general_params WHERE gp_group_key = 'PlayerTracking.ExternalLoyaltyProgram' AND gp_subject_key = 'Mode'),0) = 1)
  BEGIN
 
   SELECT 
         TPS_PLAY_SESSION_ID
        ,TPS_ACCOUNT_ID
        ,TPS_TERMINAL_ID
        ,TPS_PLAYED_COUNT
        ,TPS_PLAYED_AMOUNT
        ,TPS_WON_COUNT
        ,TPS_WON_AMOUNT
        ,TPS_REDEEMABLE_PLAYED
        ,TPS_REDEEMABLE_WON
        ,TPS_REDEEMABLE_CASH_IN
        ,TPS_REDEEMABLE_CASH_OUT
        ,TPS_NON_REDEEMABLE_PLAYED  
        ,TPS_NON_REDEEMABLE_WON
        ,TPS_NON_REDEEMABLE_CASH_IN
        ,TPS_NON_REDEEMABLE_CASH_OUT
        ,(SELECT TOP 1 TPL_GAME_ID FROM #PLAYS_SESSIONS_PLAYS_TEMP A WHERE A.TPS_PLAY_SESSION_ID = B.TPS_PLAY_SESSION_ID ORDER BY TPL_PLAYED_AMOUNT DESC) AS TPL_GAME_ID
        ,TPS_STARTED
        ,TPS_FINISHED
        INTO #ELP01_PLAY_SESSIONS
        FROM #PLAYS_SESSIONS_PLAYS_TEMP  B
        WHERE TPL_GAME_ID IS NOT NULL   
         GROUP   BY 
           TPS_PLAY_SESSION_ID
         , TPS_ACCOUNT_ID
         , TPS_TERMINAL_ID
         , TPS_PLAYED_COUNT
         , TPS_PLAYED_AMOUNT
         , TPS_WON_COUNT
         , TPS_WON_AMOUNT
         , TPS_REDEEMABLE_PLAYED 
         , TPS_REDEEMABLE_WON 
         , TPS_REDEEMABLE_CASH_IN
         , TPS_REDEEMABLE_CASH_OUT
         , TPS_NON_REDEEMABLE_PLAYED
         , TPS_NON_REDEEMABLE_WON
         , TPS_NON_REDEEMABLE_CASH_IN
         , TPS_NON_REDEEMABLE_CASH_OUT
         , TPS_STARTED
         , TPS_FINISHED  
    
    DECLARE    Elp01_Play_Sessions_Cursor CURSOR FOR 
     SELECT    TPS_PLAY_SESSION_ID
        ,TPS_REDEEMABLE_PLAYED
        ,TPS_NON_REDEEMABLE_PLAYED  
        ,TPS_ACCOUNT_ID
        ,TPS_TERMINAL_ID
        ,TPS_PLAYED_COUNT
        ,TPS_PLAYED_AMOUNT
        ,TPS_WON_COUNT
        ,TPS_WON_AMOUNT      
        ,TPS_REDEEMABLE_WON
        ,TPS_REDEEMABLE_CASH_IN
        ,TPS_REDEEMABLE_CASH_OUT    
        ,TPS_NON_REDEEMABLE_WON
        ,TPS_NON_REDEEMABLE_CASH_IN
        ,TPS_NON_REDEEMABLE_CASH_OUT
        ,TPL_GAME_ID    
        ,TPS_STARTED
        ,TPS_FINISHED
       FROM   #ELP01_PLAY_SESSIONS
       WHERE  TPL_GAME_ID IS NOT NULL

    SET NOCOUNT ON;

    OPEN Elp01_Play_Sessions_Cursor

    FETCH NEXT FROM Elp01_Play_Sessions_Cursor INTO @pPlaySessionId, @RedeemablePlayed, @NonRedeemablePlayed, @AccountId, @TerminalId, @PlayedCount, @PlayedAmount, @WonCount, @WonAmount, @RedeemableWon, @RedeemableCashIn, @RedeemableCashOut, @NonRedeemableWon, @NonRedeemableCashIn, @NonRedeemableCashOut, @GameId, @PsStarted, @PsFinished

    WHILE @@FETCH_STATUS = 0
    BEGIN  
        IF ((@RedeemablePlayed > 0 OR @RedeemableWon > 0) OR (@RedeemableCashIn <> @RedeemableCashOut))
        BEGIN
            EXEC dbo.elp01_sp_TransactionInterface   
                                @pElpPlaySessionId = @pPlaySessionId
                                ,@pElpAccountId = @AccountId
                                ,@pElpTerminalId = @TerminalId 
                                ,@pElpStarted= @PsStarted
                                ,@pElpFinished = @PsFinished
                                ,@pElpPlayed = @RedeemablePlayed
                                ,@pElpWon = @RedeemableWon
                                ,@pElpPlayedCount = @PlayedCount
                                ,@pElpCashin = @RedeemableCashIn
                                ,@pElpCashout = @RedeemableCashOut
                                ,@pKindof_ticket = 0
                                ,@pGameCode = @GameId;
                                                                  
        END
        IF ((@NonRedeemablePlayed > 0 OR @NonRedeemableWon > 0) OR (@NonRedeemableCashIn <> @NonRedeemableCashOut))
        BEGIN
              EXEC dbo.elp01_sp_TransactionInterface   
                                 @pElpPlaySessionId = @pPlaySessionId
                                ,@pElpAccountId = @AccountId
                                ,@pElpTerminalId = @TerminalId 
                                ,@pElpStarted= @PsStarted
                                ,@pElpFinished = @PsFinished
                                ,@pElpPlayed = @NonRedeemablePlayed
                                ,@pElpWon = @NonRedeemableWon
                                ,@pElpPlayedCount = @PlayedCount
                                ,@pElpCashin = @NonRedeemableCashIn
                                ,@pElpCashout = @NonRedeemableCashOut
                                ,@pKindof_ticket = 1
                                ,@pGameCode = @GameId;                                            
        END        
    
    FETCH NEXT FROM Elp01_Play_Sessions_Cursor INTO @pPlaySessionId, @RedeemablePlayed, @NonRedeemablePlayed, @AccountId, @TerminalId, @PlayedCount, @PlayedAmount, @WonCount, @WonAmount, @RedeemableWon, @RedeemableCashIn, @RedeemableCashOut, @NonRedeemableWon, @NonRedeemableCashIn, @NonRedeemableCashOut, @GameId, @PsStarted, @PsFinished
    END

    CLOSE Elp01_Play_Sessions_Cursor
    DEALLOCATE Elp01_Play_Sessions_Cursor
    DROP TABLE #ELP01_PLAY_SESSIONS
    
  
  END
  
  DROP TABLE #PLAYS_SESSIONS_PLAYS_TEMP
  
END -- InsertGamePlaySessions
GO