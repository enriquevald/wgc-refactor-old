/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 298;

SET @New_ReleaseId = 299;
SET @New_ScriptName = N'UpdateTo_18.299.038.sql';
SET @New_Description = N'';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/******* TABLES  *******/
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[handpays]') and name = 'hp_operation_id')
  ALTER TABLE dbo.handpays ADD hp_operation_id BIGINT NULL;
GO

IF NOT EXISTS( SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'MONEY_COLLECTIONS' AND COLUMN_NAME = 'mc_last_updated')
  ALTER TABLE MONEY_COLLECTIONS ADD mc_last_updated DATETIME NULL;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[gamegateway_bets]') AND type in (N'U'))
  DROP TABLE gamegateway_bets
GO

CREATE TABLE [dbo].[gamegateway_bets](
  [gb_game_id] [bigint] NOT NULL,
  [gb_game_instance_id] [bigint] NOT NULL,
  [gb_transaction_type] [int] NOT NULL,
  [gb_transaction_id] [nvarchar](40) NOT NULL,
  [gb_account_id] [bigint] NOT NULL,
  [gb_partner_id] [int] NOT NULL,
  [gb_egm_terminal_id] [int] NULL,
  [gb_created] [datetime] NOT NULL,
  [gb_bets] [xml] NULL,
  [gb_num_bets] [int] NOT NULL,
  [gb_total_bet] [money] NOT NULL,
  [gb_num_prizes] [int] NOT NULL,
  [gb_total_prize] [money] NOT NULL,
  [gb_jackpot_prize] [money] NOT NULL,
  [gb_related_ps_id] [bigint] NULL,
  [gb_related_mv_id] [bigint] NULL,
  [gb_last_updated]  [timestamp] NOT NULL,
 CONSTRAINT [PK_gamegateway_bets_1] PRIMARY KEY CLUSTERED 
(
  [gb_game_id] ASC,
  [gb_game_instance_id] ASC,
  [gb_partner_id] ASC,
  [gb_transaction_type] ASC,
  [gb_transaction_id] ASC,
  [gb_account_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
 GO

/******* INDEXES *******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[wcp_commands]') AND name = N'IX_wcp_cmd_created')
  CREATE NONCLUSTERED INDEX [IX_wcp_cmd_created] ON [dbo].[wcp_commands] 
  ( [cmd_created] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
GO

/******* RECORDS *******/
IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='GameGateway' AND GP_SUBJECT_KEY = 'DebitRequest.CheckSession')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('GameGateway', 'DebitRequest.CheckSession', '1')
GO

IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='GameGateway' AND GP_SUBJECT_KEY = 'NumWorkers')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('GameGateway', 'NumWorkers', '4')
GO

IF NOT EXISTS(SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'MobileBank' AND GP_SUBJECT_KEY = 'TimeGapSeconds')
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE)  VALUES ('MobileBank', 'TimeGapSeconds', '0')
GO

/******* PROCEDURES *******/
