/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 436;

SET @New_ReleaseId = 437;

SET @New_ScriptName = N'UpdateTo_18.437.041.sql';
SET @New_Description = N'New release v03.007.0004'; 

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO



/**** GENERAL PARAM *****/


/**** TABLES *****/
IF EXISTS(SELECT * FROM sys.objects WHERE object_id  = OBJECT_ID('reserved_terminal_configuration') AND type IN ('U'))
	DROP TABLE reserved_terminal_configuration

CREATE TABLE reserved_terminal_configuration
  (
	  rtc_holder_level  		INT NOT NULL,
	  rtc_minutes       		INT NOT NULL,
	  rtc_terminal_list 		XML NULL,
	  rtc_selected_terminals 	XML NULL,
	  rtc_create_date   		DATETIME NOT NULL,
	  rtc_update_date   		DATETIME NOT NULL,
	  rtc_status        		INT NOT NULL
	  CONSTRAINT pk_reserved_terminal_configuration PRIMARY KEY (rtc_holder_level)
  )     
GO

/**** RECORDS *****/


/**** STORED PROCEDURES *****/


/******* TRIGGERS *******/
