/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 116;

SET @New_ReleaseId = 117;
SET @New_ScriptName = N'UpdateTo_18.117.025.sql';
SET @New_Description = N'Update view sales_per_hour_v, added 2 indexes in draw_ticket, changed GP Cashier.Voucher - VoucherOnPromotion.HidePrizeCoupon.';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/

ALTER VIEW [dbo].[sales_per_hour_v]
AS
SELECT     dbo.sales_per_hour.sph_base_hour, dbo.sales_per_hour.sph_terminal_id, dbo.sales_per_hour.sph_terminal_name, dbo.sales_per_hour.sph_played_count, 
                      dbo.sales_per_hour.sph_played_amount, dbo.sales_per_hour.sph_won_count, dbo.sales_per_hour.sph_won_amount, 
                      dbo.sales_per_hour.sph_num_active_terminals, dbo.sales_per_hour.sph_last_play_id, dbo.sales_per_hour.sph_theoretical_won_amount, 
                      dbo.terminal_game_translation.tgt_target_game_id AS SPH_GAME_ID, dbo.games.gm_name AS SPH_GAME_NAME
FROM         dbo.sales_per_hour INNER JOIN
                      dbo.terminal_game_translation ON dbo.sales_per_hour.sph_terminal_id = dbo.terminal_game_translation.tgt_terminal_id AND 
                      dbo.sales_per_hour.sph_game_id = dbo.terminal_game_translation.tgt_source_game_id INNER JOIN
                      dbo.games ON dbo.terminal_game_translation.tgt_target_game_id = dbo.games.gm_game_id

GO

/****** INDEXES ******/

CREATE NONCLUSTERED INDEX [IX_draw_ticket_account_created] ON [dbo].[draw_tickets] 
(
	[dt_account_id] ASC,
	[dt_created] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [IX_draw_ticket_created] ON [dbo].[draw_tickets] 
(
	[dt_created] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

/****** RECORDS ******/

UPDATE   GENERAL_PARAMS
   SET   GP_SUBJECT_KEY = 'VoucherOnPromotion.HidePrizeCoupon'
 WHERE   GP_GROUP_KEY   = 'Cashier.Voucher'
   AND   GP_SUBJECT_KEY = 'VoucherOnPromotionAwarded.HidePrizeCoupon'

GO
