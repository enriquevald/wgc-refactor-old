/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 274;

SET @New_ReleaseId = 275;
SET @New_ScriptName = N'UpdateTo_18.275.038.sql';
SET @New_Description = N'GP:Update Account.Default values for PA and US';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/******* TABLES *******/

/******* RECORDS *******/

-- PANAMA STATE NAME
DECLARE @CountryISOCode2 AS VARCHAR(10)
SELECT @CountryISOCode2 = GP_KEY_VALUE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'RegionalOptions' AND GP_SUBJECT_KEY = 'CountryISOCode2'
IF (@CountryISOCode2 = 'PA')
BEGIN
	UPDATE GENERAL_PARAMS SET GP_KEY_VALUE = 'Provincia' WHERE GP_GROUP_KEY = 'Account.Fields'        AND GP_SUBJECT_KEY = 'State.Name'
	UPDATE GENERAL_PARAMS SET GP_KEY_VALUE = 'PA'        WHERE GP_GROUP_KEY = 'Account.DefaultValues' AND GP_SUBJECT_KEY = 'Country'
	UPDATE GENERAL_PARAMS SET GP_KEY_VALUE = '030'       WHERE GP_GROUP_KEY = 'Account.DefaultValues' AND GP_SUBJECT_KEY = 'DocumentType' -- Others
END
GO

-- USA STATE NAME
DECLARE @CountryISOCode2 AS VARCHAR(10)
SELECT @CountryISOCode2 = GP_KEY_VALUE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'RegionalOptions' AND GP_SUBJECT_KEY = 'CountryISOCode2'
IF (@CountryISOCode2 = 'US')
BEGIN
	UPDATE GENERAL_PARAMS SET GP_KEY_VALUE = 'State' WHERE GP_GROUP_KEY = 'Account.Fields'        AND GP_SUBJECT_KEY = 'State.Name'
	UPDATE GENERAL_PARAMS SET GP_KEY_VALUE = 'US'    WHERE GP_GROUP_KEY = 'Account.DefaultValues' AND GP_SUBJECT_KEY = 'Country'
	UPDATE GENERAL_PARAMS SET GP_KEY_VALUE = '022'   WHERE GP_GROUP_KEY = 'Account.DefaultValues' AND GP_SUBJECT_KEY = 'DocumentType' -- Driver's license
END
GO

/******* PROCEDURES *******/
