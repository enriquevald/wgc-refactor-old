/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 227;

SET @New_ReleaseId = 228;
SET @New_ScriptName = N'UpdateTo_18.228.037.sql';
SET @New_Description = N'Changes for: Chips, Mailing, Terminals, Note counter, Slot attendant, Progressives, GT player tracking, Progressives, Bank transaction, Cash advance, Credit card / debit';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/******* TABLES *******/

--
-- BEGIN TRACK DATA FOR USERS 
--

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[gui_users]') and name = 'gu_card_technician')
BEGIN
  ALTER TABLE dbo.gui_users ADD
              gu_card_technician nvarchar(50) NULL,
              gu_card_change_stacker nvarchar(50) NULL
END
GO

--
-- END TRACK DATA FOR USERS
--

--
-- BEGIN CREDIT CARD / DEBIT
--

CREATE TABLE [dbo].[bank_transactions](
      [bt_operation_id] [bigint] NOT NULL,
      [bt_type] [int] NOT NULL,
      [bt_document_number] [nvarchar](50) NOT NULL,
      [bt_bank_name] [nvarchar](50) NULL,
      [bt_bank_country] [bigint] NULL,
      [bt_holder_name] [nvarchar](200) NULL,
      [bt_card_expiration_date] [nvarchar](5) NULL,
      [bt_card_track_data] [nvarchar](100) NULL,
      [bt_amount] [money] NOT NULL,
      [bt_iso_code] [nvarchar](3) NOT NULL,
      [bt_account_id] [bigint] NOT NULL,
      [bt_cashier_id] [bigint] NOT NULL,
      [bt_transaction_type] [int] NOT NULL,
      [bt_edited] [bit] NOT NULL,
      [bt_check_routing_number] [nvarchar](50) NULL,
      [bt_check_account_number] [nvarchar](50) NULL,
      [bt_check_date] [datetime] NULL,
      [bt_comments] [nvarchar](256) NULL,
      [bt_undo_operation] [bit] NOT NULL,
CONSTRAINT [PK_bank_transactions] PRIMARY KEY CLUSTERED 
(
      [bt_operation_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

--
-- END CREDIT CARD / DEBIT
--

--
-- BEGIN OF CHIPS CHANGES
--

---- chips
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[chips]') AND type in (N'U'))
BEGIN

CREATE TABLE dbo.chips
(
  ch_chip_id BIGINT IDENTITY(1,1) NOT NULL,
  ch_chip_set_id BIGINT NOT NULL,
  ch_iso_code nvarchar(3) NOT NULL,
  ch_denomination Money NULL,
  ch_allowed bit NOT NULL,
  ch_color int NULL,
  ch_name nvarchar(50) NULL,
  ch_drawing nvarchar(50) NULL,
  CONSTRAINT [PK_chips] PRIMARY KEY CLUSTERED 
  (
    [ch_chip_id] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
)  ON [PRIMARY] 
END

GO

---- chips_sets
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[chips_sets]') AND type in (N'U'))
BEGIN

CREATE TABLE dbo.chips_sets
(
    chs_chip_set_id BIGINT IDENTITY(1,1) NOT NULL,
    chs_iso_code nvarchar(3) NOT NULL,
    chs_name nvarchar(50) NOT NULL,
    chs_allowed bit NOT NULL,
    CONSTRAINT [PK_chips_sets] PRIMARY KEY CLUSTERED 
    (
      [chs_chip_set_id] ASC
    )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
)  ON [PRIMARY]
END

GO

---- chips_stocks
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[chips_stock]') AND type in (N'U'))
BEGIN

CREATE TABLE [dbo].[chips_stock](
       [chsk_chip_id] [bigint] NOT NULL,
       [chsk_quantity] [int] NULL,
  CONSTRAINT [PK_chips_stocks] PRIMARY KEY CLUSTERED 
(
  [chsk_chip_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END

GO

---- gaming_table_chips_sets
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[gaming_table_chips_sets]') )
BEGIN
  CREATE TABLE [dbo].[gaming_table_chips_sets](
    [gtcs_gaming_table_id] [bigint] NOT NULL,
    [gtcs_chips_set_id] [bigint] NOT NULL,
   CONSTRAINT [PK_gaming_table_chips_sets] PRIMARY KEY CLUSTERED 
  (
    [gtcs_gaming_table_id] ASC,
    [gtcs_chips_set_id] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
  ) ON [PRIMARY]
END
GO

-- Insert chips and chips stock in new tables
IF NOT EXISTS (SELECT 1 FROM chips_sets WHERE chs_iso_code = 'X01' )
BEGIN
  DECLARE @chips_sets_id AS BIGINT
  
    INSERT INTO chips_sets (chs_iso_code, chs_name        , chs_allowed) 
          VALUES ('X01', 'Fichas de Valor', 1)

  SET @chips_sets_id = SCOPE_IDENTITY();
  
  INSERT INTO   CHIPS (ch_chip_set_id, ch_iso_code, ch_denomination, ch_allowed, ch_color)
       SELECT   @chips_sets_id, caa_iso_code, caa_denomination, caa_allowed, caa_color 
         FROM   cage_amounts
        WHERE   caa_iso_code = 'X01'

    DELETE FROM cage_amounts WHERE caa_iso_code = 'X01'
END

--- Insert de chips_stocks
INSERT   INTO CHIPS_STOCK
SELECT   CH_CHIP_ID, CGS_QUANTITY 
  FROM   CAGE_STOCK
 INNER   JOIN CHIPS ON CGS_DENOMINATION = CH_DENOMINATION
 WHERE   CGS_ISO_CODE = 'X01'

DELETE   CAGE_STOCK  
 where   CGS_ISO_CODE = 'X01' 

UPDATE   currency_exchange 
   SET   ce_type  = 3
 WHERE   ce_currency_iso_code = 'X01'

UPDATE   cashier_sessions_by_currency 
   SET   csc_type = 3
 WHERE   csc_iso_code = 'X01'

--- Add chip_id in Cashier_Movements
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[cashier_movements]') and name = 'cm_chip_id')
BEGIN
      ALTER TABLE dbo.cashier_movements ADD
            cm_chip_id BIGINT NULL
END
GO

--- Add chip_id in cage_movement_details
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[cage_movement_details]') and name = 'cmd_chip_id')
BEGIN
      ALTER TABLE dbo.cage_movement_details ADD
            cmd_chip_id BIGINT  NULL
END
GO

--- Update chip_id in Cashier_Movements
IF EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[TMP_CASHIER_MOVEMENTS]') )
BEGIN
      DROP TABLE TMP_CASHIER_MOVEMENTS
END
GO
     SELECT CM_MOVEMENT_ID, CH_CHIP_ID
       INTO TMP_CASHIER_MOVEMENTS
       FROM CASHIER_MOVEMENTS with(index(IX_cm_date_type))
 INNER JOIN CHIPS ON CM_CURRENCY_DENOMINATION = CH_DENOMINATION
      WHERE CM_DATE > '2014-02-18T00:00:00'
        AND CM_CURRENCY_ISO_CODE = 'X01'
        AND CM_CURRENCY_DENOMINATION IS NOT NULL

DECLARE @movement_id bigint, @chip_id bigint

DECLARE _cashier_movements CURSOR FOR 
 SELECT CM_MOVEMENT_ID, CH_CHIP_ID
   FROM TMP_CASHIER_MOVEMENTS

OPEN _cashier_movements
   
FETCH NEXT FROM _cashier_movements INTO @movement_id, @chip_id
WHILE @@FETCH_STATUS = 0
BEGIN
   UPDATE CASHIER_MOVEMENTS 
      SET CM_CHIP_ID = @chip_id
    WHERE CM_MOVEMENT_ID = @movement_id      
   
   FETCH NEXT FROM _cashier_movements INTO @movement_id, @chip_id
END 
CLOSE _cashier_movements
DEALLOCATE _cashier_movements

IF EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[TMP_CASHIER_MOVEMENTS]') )
BEGIN
      Drop TABLE TMP_CASHIER_MOVEMENTS
END
GO

--Update chip_id in Cage_movement_detail
IF EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[TMP_CAGE_MOVEMENT_DETAIL]') )
BEGIN
      DROP TABLE TMP_CAGE_MOVEMENT_DETAIL
END
GO
     SELECT CMD_CAGE_MOVEMENT_DETAIL_ID, CH_CHIP_ID
       INTO TMP_CAGE_MOVEMENT_DETAIL
       FROM CAGE_MOVEMENT_DETAILS
 INNER JOIN CHIPS ON CMD_DENOMINATION = CH_DENOMINATION
      WHERE CMD_ISO_CODE = 'X01'
        AND CMD_DENOMINATION IS NOT NULL
        
DECLARE @cage_id bigint, @chip_cage_id bigint

DECLARE _cage_movement_detail CURSOR FOR 
 SELECT CMD_CAGE_MOVEMENT_DETAIL_ID, CH_CHIP_ID
   FROM TMP_CAGE_MOVEMENT_DETAIL

OPEN _cage_movement_detail
   
FETCH NEXT FROM _cage_movement_detail INTO @cage_id, @chip_cage_id
WHILE @@FETCH_STATUS = 0
BEGIN
   UPDATE CAGE_MOVEMENT_DETAILS 
      SET CMD_CHIP_ID = @chip_cage_id
    WHERE CMD_CAGE_MOVEMENT_DETAIL_ID = @cage_id      

   FETCH NEXT FROM _cage_movement_detail INTO @cage_id, @chip_cage_id
END
CLOSE _cage_movement_detail;
DEALLOCATE _cage_movement_detail;

IF EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[TMP_CAGE_MOVEMENT_DETAIL]') )
BEGIN
      Drop TABLE TMP_CAGE_MOVEMENT_DETAIL
END
GO

---
---Rename cage_amounts = cage_currencies

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[cage_amounts]') AND type in (N'U'))
BEGIN
  EXECUTE sys.sp_rename N'cage_amounts', N'cage_currencies', Object
END
GO
-- Rename cage_amounts colums caa.. --> cgc..
IF  EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[cage_currencies]') and name = 'caa_iso_code')  
BEGIN 
  EXECUTE sp_rename N'cage_currencies.caa_iso_code', N'cgc_iso_code', 'COLUMN'
END
GO

IF  EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[cage_currencies]') and name = 'caa_denomination')  
BEGIN 
  EXECUTE sp_rename N'cage_currencies.caa_denomination', N'cgc_denomination', 'COLUMN'
END
GO

IF  EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[cage_currencies]') and name = 'caa_allowed')  
BEGIN 
  EXECUTE sp_rename N'cage_currencies.caa_allowed', N'cgc_allowed', 'COLUMN'
END
GO

IF  EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[cage_currencies]') and name = 'caa_color')  
BEGIN 
  ALTER TABLE [dbo].[cage_currencies] DROP COLUMN caa_color;
END
GO
IF  NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[cage_currencies]') and name = 'cgc_cage_currency_type')
BEGIN
      ALTER TABLE dbo.cage_currencies ADD
            cgc_cage_currency_type INT NOT NULL  DEFAULT (0)
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PK_CAGE_AMOUNTS]') AND type in (N'PK'))
BEGIN
      ALTER TABLE dbo.cage_currencies
            DROP CONSTRAINT PK_CAGE_AMOUNTS
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PK_cage_currencies]') AND type in (N'PK'))
BEGIN
      ALTER TABLE dbo.cage_currencies ADD CONSTRAINT
            PK_cage_currencies PRIMARY KEY CLUSTERED 
            (
            cgc_iso_code,
            cgc_denomination,
            cgc_cage_currency_type
            ) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
END
GO

--- Add column cgs_cage_currency_type(PK) in cage_stock 
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[cage_stock]') and name = 'cgs_cage_currency_type')
BEGIN
      ALTER TABLE dbo.cage_stock ADD
           cgs_cage_currency_type INT NOT NULL  DEFAULT (0)
END
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PK_CAGE_STOCK]') AND type in (N'PK'))
BEGIN
      ALTER TABLE dbo.cage_stock
            DROP CONSTRAINT PK_CAGE_STOCK
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PK_cage_stock]') AND type in (N'PK'))
BEGIN
      ALTER TABLE dbo.cage_stock ADD CONSTRAINT
            PK_cage_stock PRIMARY KEY CLUSTERED 
            (
            cgs_iso_code,
            cgs_denomination,
            cgs_cage_currency_type
            ) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
END
GO
--- Add column cmd_cage_currency_type in Cage_movement_detail
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[cage_movement_details]') and name = 'cmd_cage_currency_type')
BEGIN
      ALTER TABLE dbo.Cage_movement_details ADD
            cmd_cage_currency_type INT NULL
END
GO

--- Add column cm_cage_currency_type in Cashier_movements
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[cashier_movements]') and name = 'cm_cage_currency_type')
BEGIN
      ALTER TABLE dbo.cashier_movements ADD
            cm_cage_currency_type INT NULL,
            cm_chips_sale_denomination  INT NULL
END
GO

--
-- END OF CHIPS CHANGES
--

--
-- BEGIN OF MAILING
--

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[PATTERNS]') and name = 'pt_active_mailing')
  ALTER TABLE [dbo].[PATTERNS] ADD [pt_active_mailing] BIT NOT NULL DEFAULT (0)
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[PATTERNS]') and name = 'pt_address_list')
  ALTER TABLE [dbo].[PATTERNS] ADD [pt_address_list] NVARCHAR(500) NULL
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[PATTERNS]') and name = 'pt_subject')
  ALTER TABLE [dbo].[PATTERNS] ADD [pt_subject] NVARCHAR(200) NULL
GO

--
-- END OF MAILING
--

--
-- BEGIN OF TERMINALS CHANGES
--

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[terminals]') and name = 'te_cuim')
  ALTER TABLE [dbo].[terminals] ADD
    [te_cuim]         [nvarchar](50) NULL,
    [te_position]     [int]          NULL,
    [te_top_award]    [bigint]       NULL,
    [te_max_bet]      [int]          NULL,
    [te_number_lines] [nvarchar](50) NULL
GO

IF NOT EXISTS (SELECT * FROM [dbo].[general_params] WHERE gp_group_key LIKE 'EGM' AND gp_subject_key LIKE 'Denominations')
      INSERT INTO [dbo].[general_params]
      VALUES ('EGM','Denominations','0.0005;0.0010;0.0020;0.0025;0.0050;0.01;0.02;0.03;0.05;0.10;0.15;0.20;0.25;0.40;0.50;1;10;100;1000;2;2.50;20;200;2000;25;250;2500;5;50;500;5000') 
GO

IF NOT EXISTS (SELECT * FROM [dbo].[general_params] WHERE gp_group_key LIKE 'EGM' AND gp_subject_key LIKE 'MultiDenominations')
      INSERT INTO [dbo].[general_params]
      VALUES ('EGM','MultiDenominations','') 
GO

IF NOT EXISTS (SELECT * FROM [dbo].[general_params] WHERE gp_group_key LIKE 'EGM' AND gp_subject_key LIKE 'SingleDenominations')
      INSERT INTO [dbo].[general_params]
      VALUES ('EGM','SingleDenominations','') 
GO

CREATE FUNCTION dbo.TMP_WIN_SplitString 
(
    @str NVARCHAR(max), 
    @separator CHAR(1)
)
RETURNS TABLE
AS
RETURN (
WITH tokens (p, a, b)
AS (SELECT
      CAST(1 AS BIGINT),
      CAST(1 AS BIGINT),
      CHARINDEX(@separator, @str) 
      UNION ALL 
      SELECT
      p + 1,
      b + 1,
      CHARINDEX(@separator, @str, b + 1)
FROM tokens
WHERE b > 0)
SELECT
      CONVERT(FLOAT, SUBSTRING(
      @str,
      a,
      CASE WHEN b > 0 THEN b - a ELSE LEN(@str) END
      ), 0) AS Item
FROM tokens
)
GO

CREATE FUNCTION dbo.TMP_WIN_ParseMultiDenomination
(@src_multi NVARCHAR(40),
@sep CHAR)
RETURNS NVARCHAR(50)
AS
BEGIN
      declare @multi nvarchar(50)

      if isnull(@src_multi,'') <> ''
      begin
            SELECT
                  @multi = COALESCE(@multi + '-', '') + CONVERT(nvarchar(10), item)
      FROM (
            SELECT TOP 1000 *
                  FROM  dbo.TMP_WIN_SplitString(@src_multi, @sep)
            ORDER BY 1
       ) X
      END

      RETURN(@multi)
END
GO

DECLARE @gp_multi AS NVARCHAR(1024)
DECLARE @gp_single AS NVARCHAR(1024)
DECLARE @multi AS NVARCHAR(50)
DECLARE @separator AS CHAR
DECLARE @separator_list AS NVARCHAR(10)
DECLARE @value AS NVARCHAR(40)
DECLARE @orig_value AS NVARCHAR(40)

DECLARE db_cursor CURSOR FOR 
  SELECT   DISTINCT te_multi_denomination
    FROM   terminals
   WHERE   te_multi_denomination IS NOT NULL
UNION 
  SELECT   DISTINCT CAST(te_denomination AS NVARCHAR)
    FROM   terminals
   WHERE   te_denomination IS NOT NULL  
ORDER BY   1
   
OPEN db_cursor

FETCH NEXT FROM db_cursor INTO @orig_value
WHILE @@FETCH_STATUS = 0   
BEGIN   
      SET @multi = ''
      SET @separator_list = ',;:-'
      SELECT @separator = CHAR(15) --space
      
      WHILE @multi = '' AND @separator <> ''
      BEGIN
            BEGIN TRY
                SELECT @value = REPLACE(REPLACE(@orig_value,' ',''),'$','')
                
                  SET @multi = dbo.TMP_WIN_ParseMultiDenomination(@value, @separator)
                  
                  IF CHARINDEX('-',@multi,0) > 0
                  BEGIN
                        SELECT @gp_multi = COALESCE(@gp_multi + ';', '') + @multi
                        
                        UPDATE terminals
                         SET te_multi_denomination = @multi
                       WHERE te_multi_denomination = @orig_value
                  END
                  ELSE
                  BEGIN
                        SELECT @gp_single = COALESCE(@gp_single + ';', '') + @multi
                        
                        UPDATE terminals
                         SET te_multi_denomination = @multi
                       WHERE te_denomination = CONVERT(MONEY, @multi, 0)
                  END               
      
            END TRY
            BEGIN CATCH
                  SET @multi = ''
                  SELECT @separator = SUBSTRING(@separator_list, 1, 1)
                  SELECT @separator_list = SUBSTRING(@separator_list, 2, LEN(@separator_list))
            END CATCH
      END
      
      FETCH NEXT FROM db_cursor INTO @value
END

CLOSE db_cursor   
DEALLOCATE db_cursor

SELECT @gp_multi = ISNULL(@gp_multi, '')
SELECT @gp_single = ISNULL(@gp_single, '')

IF EXISTS (SELECT gp_subject_key FROM general_params 
           WHERE gp_group_key LIKE 'EGM' AND gp_subject_key LIKE 'MultiDenominations')
      UPDATE general_params
         SET gp_key_value = @gp_multi
      WHERE gp_group_key LIKE 'EGM'
         AND gp_subject_key LIKE 'MultiDenominations' 
ELSE
      INSERT INTO general_params(gp_group_key, gp_subject_key, gp_key_value)
           VALUES('EGM', 'MultiDenominations', @gp_multi)
IF EXISTS (SELECT gp_subject_key FROM general_params 
           WHERE gp_group_key LIKE 'EGM' AND gp_subject_key LIKE 'SingleDenominations')
      UPDATE general_params
         SET gp_key_value = @gp_single
      WHERE gp_group_key LIKE 'EGM'
         AND gp_subject_key LIKE 'SingleDenominations'      
ELSE
      INSERT INTO general_params(gp_group_key, gp_subject_key, gp_key_value)
           VALUES('EGM', 'SingleDenominations', @gp_single)
GO

DROP FUNCTION dbo.TMP_WIN_SplitString
DROP FUNCTION dbo.TMP_WIN_ParseMultiDenomination
GO

--
-- END OF TERMINALS CHANGES
--

--
-- BEGIN OF NOTECOUNTER
--

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[note_counters]') AND type in (N'U'))
  CREATE TABLE [dbo].[note_counters](
        [nc_application_id] [bigint] NOT NULL,
        [nc_model] [int] NOT NULL,
        [nc_communication_type] [int] NOT NULL,
        [nc_port] [int] NULL,
        [nc_parity] [int] NULL,
        [nc_data_bits] [int] NULL,
        [nc_stop_bits] [int] NULL,
        [nc_baud_rate] [int] NULL,
        [nc_denominations] [xml] NULL,
        [nc_currency_iso_code] [nvarchar](3) NOT NULL,
        CONSTRAINT [PK_note_counters] PRIMARY KEY CLUSTERED 
        (
              [nc_application_id] ASC
        )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
  ) ON [PRIMARY]
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[money_collections]') and name = 'mc_note_counter_events')
	ALTER TABLE dbo.money_collections ADD mc_note_counter_events xml NULL
GO

--
-- END OF NOTECOUNTER
--

--
-- BEGIN OF SLOT ATTENDANT
--

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].handpays') and name = 'hp_id')
  ALTER TABLE dbo.handpays ADD hp_id bigint NOT NULL IDENTITY (1, 1)
GO

DECLARE @_hp_id AS BIGINT

DECLARE   _handpays CURSOR FOR 
 SELECT   HP_ID 
   FROM   HANDPAYS 
  WHERE   ISNULL(HP_MOVEMENT_ID,0) > 0 AND ISNULL(HP_PLAY_SESSION_ID,0) > 0

OPEN _handpays

FETCH NEXT FROM _handpays INTO @_hp_id

WHILE @@FETCH_STATUS = 0
BEGIN
  
  UPDATE HANDPAYS SET HP_STATUS = cast(0x8000 as INT) WHERE HP_ID = @_hp_id -- Status Paid
  
  FETCH NEXT FROM _handpays INTO @_hp_id
END

CLOSE _handpays
DEALLOCATE _handpays
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].cashier_movements') and name = 'cm_related_id')
  ALTER TABLE dbo.cashier_movements ADD cm_related_id bigint NULL
GO

--
-- END OF SLOT ATTENDANT
--

--
-- BEGIN OF PROGRESSIVES CHANGES
--
/*                             */ 
/*   PROGRESSIVES              */
/*                             */ 
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[progressives]') AND type in (N'U'))
CREATE TABLE [dbo].[progressives](
      [pgs_progressive_id] [bigint] IDENTITY(1,1) NOT NULL,
      [pgs_name] [nvarchar](50) NOT NULL,
      [pgs_created] [datetime] NOT NULL,
      [pgs_contribution_pct] [numeric](7, 4) NOT NULL,
      [pgs_num_levels] [int] NOT NULL,
      [pgs_amount] [money] NOT NULL,
      [pgs_terminal_list] [xml] NOT NULL,
      [pgs_status] [int] NOT NULL,
	    [pgs_last_provisioned_hour] datetime NULL,
      [pgs_status_changed] [datetime] NOT NULL CONSTRAINT [DF_progressives_pgs_status_changed]  DEFAULT (getdate()),
CONSTRAINT [PK_progressives] PRIMARY KEY CLUSTERED 
(
      [pgs_progressive_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/*                             */ 
/*   PROGRESSIVES LEVELS       */
/*                             */ 
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[progressives_levels]') AND type in (N'U'))
CREATE TABLE [dbo].[progressives_levels](
      [pgl_progressive_id] [bigint] NOT NULL,
      [pgl_level_id] [int] NOT NULL,
      [pgl_name] [nvarchar](50) NULL,
      [pgl_contribution_pct] [numeric](5, 2) NOT NULL,
      [pgl_amount] [money] NOT NULL,
CONSTRAINT [PK_progressives_levels] PRIMARY KEY CLUSTERED 
(
      [pgl_progressive_id] ASC,
      [pgl_level_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/*                             */ 
/*   PROGRESSIVES PROVISIONS   */
/*                             */ 
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[progressives_provisions]') AND type in (N'U'))
CREATE TABLE [dbo].[progressives_provisions](
      [pgp_provision_id] [bigint] IDENTITY(1,1) NOT NULL,
      [pgp_progressive_id] [bigint] NOT NULL,
      [pgp_created] [datetime] NOT NULL,
      [pgp_hour_from] [datetime] NOT NULL,
      [pgp_hour_to] [datetime] NOT NULL,
      [pgp_amount] [money] NOT NULL,
      [pgp_theoretical_amount] [money] NOT NULL,
      [pgp_cage_session_id] [bigint] NULL,
      [pgp_cage_amount] [money] NULL,
CONSTRAINT [PK_progressives_provisions] PRIMARY KEY CLUSTERED 
(
      [pgp_provision_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/*                                    */ 
/*   PROGRESSIVES PROVISIONS LEVELS   */
/*                                    */ 
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[progressives_provisions_levels]') AND type in (N'U'))
CREATE TABLE [dbo].[progressives_provisions_levels](
      [ppl_provision_id] [bigint] NOT NULL,
      [ppl_level_id] [int] NOT NULL,
      [ppl_amount] [money] NOT NULL,
      [ppl_theoretical_amount] [money] NOT NULL,
CONSTRAINT [PK_progressives_provisions_levels] PRIMARY KEY CLUSTERED 
(
      [ppl_provision_id] ASC,
      [ppl_level_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/*                                      */ 
/*   PROGRESSIVES PROVISIONS TERMINALS  */
/*                                      */ 
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[progressives_provisions_terminals]') AND type in (N'U'))
CREATE TABLE [dbo].[progressives_provisions_terminals](
      [ppt_provision_id] [bigint] NOT NULL,
      [ppt_terminal_id] [int] NOT NULL,
      [ppt_progressive_id] [bigint] NOT NULL,
      [ppt_amount] [money] NOT NULL,
CONSTRAINT [PK_progressives_provisions_terminals] PRIMARY KEY CLUSTERED 
(
      [ppt_provision_id] ASC,
      [ppt_terminal_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/*                             */ 
/*   HANDPAYS                  */
/*                             */ 
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[handpays]') and name = 'hp_progressive_id')
ALTER TABLE dbo.handpays ADD
      hp_progressive_id bigint NULL,
      hp_level int NULL
GO

/*                             */ 
/*   GAME_METERS               */
/*                             */ 
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[game_meters]') and name = 'gm_progressive_jackpot_amount')
ALTER TABLE dbo.game_meters ADD
      gm_progressive_jackpot_amount money NULL,
      gm_delta_progressive_jackpot_amount money NULL,
      gm_max_progressive_jackpot_amount money NULL
GO

/*                             */ 
/*   MACHINE_METERS            */
/*                             */ 
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[machine_meters]') and name = 'mm_progressive_jackpot_amount')
ALTER TABLE dbo.machine_meters ADD
      mm_progressive_jackpot_amount money NULL,
      mm_delta_progressive_jackpot_amount money NULL,
      mm_max_progressive_jackpot_amount money NULL
GO

/*                             */ 
/*   SALES_PER_HOUR            */
/*                             */ 
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[sales_per_hour]') and name = 'sph_jackpot_amount')
ALTER TABLE dbo.sales_per_hour ADD
      sph_jackpot_amount money NULL,
      sph_progressive_jackpot_amount money NULL,
      sph_progressive_jackpot_amount_0 money NULL,
      sph_progressive_provision_amount money NULL
GO

/*                             */ 
/*   MACHINE_STATS_PER_HOUR    */
/*                             */ 
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[machine_stats_per_hour]') and name = 'msh_jackpot_amount')
ALTER TABLE dbo.machine_stats_per_hour ADD
      msh_jackpot_amount money NULL,
      msh_progressive_jackpot_amount money NULL,
      msh_progressive_jackpot_amount_0 money NULL,
      msh_progressive_provision_amount money NULL
GO

/*                             */ 
/*   TERMINALS                 */
/*                             */ 
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[terminals]') and name = 'te_progressive_paid_meter')
ALTER TABLE dbo.terminals ADD
      te_progressive_paid_meter int NULL CONSTRAINT [DF_terminals_te_progressive_paid_meter]  DEFAULT (32)
GO

/*                                             */ 
/*  VIEW SALES_PER_HOUR_V --> DROP AND CREATE  */
/*                                             */ 
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[sales_per_hour_v]'))
DROP VIEW [dbo].[sales_per_hour_v]
GO

CREATE VIEW [dbo].[sales_per_hour_v]
AS
SELECT     dbo.sales_per_hour.sph_base_hour, dbo.sales_per_hour.sph_terminal_id, dbo.sales_per_hour.sph_terminal_name, dbo.sales_per_hour.sph_played_count, 
                      dbo.sales_per_hour.sph_played_amount, dbo.sales_per_hour.sph_won_count, dbo.sales_per_hour.sph_won_amount, 
                      dbo.sales_per_hour.sph_num_active_terminals, dbo.sales_per_hour.sph_last_play_id, dbo.sales_per_hour.sph_theoretical_won_amount, 
                      dbo.terminal_game_translation.tgt_target_game_id AS SPH_GAME_ID, dbo.games.gm_name AS SPH_GAME_NAME, dbo.sales_per_hour.sph_jackpot_amount, 
                      dbo.sales_per_hour.sph_progressive_jackpot_amount, dbo.sales_per_hour.sph_progressive_jackpot_amount_0, 
                      dbo.sales_per_hour.sph_progressive_provision_amount
FROM         dbo.sales_per_hour INNER JOIN
                      dbo.terminal_game_translation ON dbo.sales_per_hour.sph_terminal_id = dbo.terminal_game_translation.tgt_terminal_id AND 
                      dbo.sales_per_hour.sph_game_id = dbo.terminal_game_translation.tgt_source_game_id INNER JOIN
                      dbo.games ON dbo.terminal_game_translation.tgt_target_game_id = dbo.games.gm_game_id
GO

/*                                              */ 
/*  VIEW SALES_PER_HOUR_V2 --> DROP AND CREATE  */
/*                                              */ 
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[sales_per_hour_v2]'))
DROP VIEW [dbo].[sales_per_hour_v2]
GO

CREATE VIEW [dbo].[sales_per_hour_v2]
AS
SELECT     dbo.sales_per_hour.sph_base_hour, dbo.sales_per_hour.sph_terminal_id, dbo.sales_per_hour.sph_terminal_name, dbo.sales_per_hour.sph_played_count, 
                      dbo.sales_per_hour.sph_played_amount, dbo.sales_per_hour.sph_won_count, dbo.sales_per_hour.sph_won_amount, 
                      dbo.sales_per_hour.sph_num_active_terminals, dbo.sales_per_hour.sph_last_play_id, dbo.sales_per_hour.sph_theoretical_won_amount, 
                      ISNULL(dbo.terminal_game_translation.tgt_translated_game_id, 0) AS SPH_GAME_ID, ISNULL(dbo.providers_games.pg_game_name, 'UNKNOWN') 
                      AS SPH_GAME_NAME, dbo.sales_per_hour.sph_jackpot_amount, dbo.sales_per_hour.sph_progressive_jackpot_amount, 
                      dbo.sales_per_hour.sph_progressive_jackpot_amount_0, dbo.sales_per_hour.sph_progressive_provision_amount
FROM         dbo.sales_per_hour INNER JOIN
                      dbo.terminal_game_translation ON dbo.sales_per_hour.sph_terminal_id = dbo.terminal_game_translation.tgt_terminal_id AND 
                      dbo.sales_per_hour.sph_game_id = dbo.terminal_game_translation.tgt_source_game_id LEFT OUTER JOIN
                      dbo.providers_games ON dbo.terminal_game_translation.tgt_translated_game_id = dbo.providers_games.pg_game_id
GO

--
-- END OF PROGRESSIVES CHANGES
--

--
-- BEGIN GT PLAYER TRACKING
--

--
-- Delete table gt_playertracking_session
--
IF EXISTS (SELECT * FROM SYS.OBJECTS WHERE TYPE = 'U' AND NAME = 'gt_playertracking_session')
BEGIN
  DROP TABLE GT_PLAYERTRACKING_SESSION
END
GO

--
-- Delete table gt_play_movements
--
IF EXISTS (SELECT * FROM SYS.OBJECTS WHERE TYPE = 'U' AND NAME = 'gt_play_movements')
BEGIN
  DROP TABLE gt_play_movements
END
GO

--
-- Create tables
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[gt_playertracking_movements]') AND type in (N'U'))
      DROP TABLE [dbo].[gt_playertracking_movements]
GO

CREATE TABLE [dbo].[gt_playertracking_movements](
      [gtpm_movement_id] [bigint] IDENTITY(1,1) not null,
      [gtpm_type] [int] NOT NULL,
      [gtpm_datetime] [datetime] NOT NULL,
      [gtpm_user_id] [int] NOT NULL,
      [gtpm_cashier_id] [bigint] NOT NULL,
      [gtpm_gaming_table_session_id] [bigint] NOT NULL,
      [gtpm_gaming_table_id] [int] NOT NULL,
      [gtpm_seat_id] [bigint] NOT NULL,
      [gtpm_play_session_id] [bigint] NOT NULL,
      [gtpm_terminal_id] [bigint] NULL,
      [gtpm_account_id] [bigint] NULL,
      [gtpm_old_value] [money] NULL,
      [gtpm_value] [money] NOT NULL,     
CONSTRAINT [PK_gt_playertracking_movements] PRIMARY KEY CLUSTERED 
(
      [gtpm_movement_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

--
-- Alter Tables
--
-- GAMING_TABLES
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[gaming_tables]') and name = 'gt_provider_id')
      ALTER TABLE dbo.gaming_tables ADD gt_provider_id INT  NULL
GO

IF  EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[gaming_tables]') and name = 'gt_pt_session_id')  
    ALTER TABLE dbo.gaming_tables DROP COLUMN gt_pt_session_id 
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[gaming_tables]') and name = 'gt_table_speed_slow')
      ALTER TABLE dbo.gaming_tables ADD gt_table_speed_slow INT  NULL
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[gaming_tables]') and name = 'gt_table_speed_fast')
      ALTER TABLE dbo.gaming_tables ADD gt_table_speed_fast INT  NULL
GO

IF  EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[gaming_tables]') and name = 'gt_table_speed')  
BEGIN 
    ALTER TABLE dbo.gaming_tables DROP COLUMN gt_table_speed 
    ALTER TABLE dbo.gaming_tables ADD gt_table_speed_normal INT NULL
END
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[gaming_tables]') and name = 'gt_table_speed_selected')
      ALTER TABLE dbo.gaming_tables ADD gt_table_speed_selected INT  NULL
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[gaming_tables]') and name = 'gt_plays_count')
      ALTER TABLE dbo.gaming_tables ADD gt_plays_count int NULL
GO 

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[gaming_tables]') and name = 'gt_last_tick_plays_count')
      ALTER TABLE dbo.gaming_tables ADD gt_last_tick_plays_count int NULL    
GO 

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[gaming_tables]') and name = 'gt_player_tracking_pause')
      ALTER TABLE dbo.gaming_tables ADD gt_player_tracking_pause bit not NULL     default 0
GO 

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[gaming_tables]') and name = 'gt_timestamp')
      ALTER TABLE dbo.gaming_tables ADD gt_timestamp timestamp NOT NULL
GO

--
-- Create GT_PLAY_SESSIONS
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[gt_play_sessions]') AND type in (N'U'))
  DROP TABLE [dbo].[gt_play_sessions]
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[gt_play_sessions]') AND type in (N'U'))
BEGIN
      CREATE TABLE [dbo].[gt_play_sessions](
              [gtps_play_session_id] [bigint] IDENTITY(1,1) NOT NULL,
              [gtps_gaming_table_id] [int] NOT NULL,
              [gtps_gaming_table_session_id] [bigint] NOT NULL,
              [gtps_started] [datetime] NOT NULL,
              [gtps_finished] [datetime] NULL,
              [gtps_account_id] [bigint] NOT NULL,
              [gtps_seat_id] [bigint] NOT NULL,
              [gtps_status] [int] NOT NULL,
              [gtps_plays] [int] NOT NULL DEFAULT ((0)),
              [gtps_current_bet] [money] NOT NULL DEFAULT ((0)),
              [gtps_bet_min] [money] NOT NULL,
              [gtps_bet_max] [money] NOT NULL,
              [gtps_played_amount] [money] NOT NULL,         
              [gtps_played_average] [money] NOT NULL,              
              [gtps_points] [money] NULL,
              [gtps_computed_points] [money] NULL,           
              [gtps_game_time] [bigint] NULL,
              [gtps_start_walk] [datetime] NULL,
              [gtps_walk] [bigint] NULL,         
              [gtps_player_skill] [int] NULL,
              [gtps_player_speed] [int] NULL,          
              [gtps_finished_user_id] [int] NULL,
              [gtps_updated_user_id] [int] NULL,             
              [gtps_total_sell_chips] [money] NULL,
              [gtps_chips_out] [money] NULL,
              [gtps_chips_in] [money] NULL,
              [gtps_netwin] [money] NULL,        
              [gtps_in_session_plays_table] [int] NULL,
              [gtps_in_session_last_played_amount] [money] NULL,
              [gtps_in_session_last_play] [int] NULL,
              [gtps_updated_played_current] [datetime] NULL DEFAULT (getdate()),
              [gtps_updated] [datetime] NULL DEFAULT (getdate()),              
              [gtps_timestamp] [timestamp] NOT NULL,
      CONSTRAINT [PK_gt_play_sessions] PRIMARY KEY CLUSTERED 
      (
              [gtps_play_session_id] ASC
      )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
      ) ON [PRIMARY]
END
GO

--
-- END GT PLAYER TRACKING
--

/******* INDEXES *******/

--
-- BEGIN OF PROGRESSIVES CHANGES
--

-- TABLE PROGRESSIVES: Add Index
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[progressives]') AND name = N'IX_pgs_status_name')
CREATE NONCLUSTERED INDEX IX_pgs_status_name ON dbo.progressives
      (
      pgs_status,
      pgs_name
      ) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

-- TABLE PROGRESSIVES_PROVISIONS: Add Indexes
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[progressives_provisions]') AND name = N'IX_pgp_created')
CREATE NONCLUSTERED INDEX IX_pgp_created ON dbo.progressives_provisions
      (
      pgp_created
      ) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[progressives_provisions]') AND name = N'IX_pgp_to_progressive_from')
CREATE NONCLUSTERED INDEX IX_pgp_to_progressive_from ON dbo.progressives_provisions
      (
      pgp_hour_to,
      pgp_progressive_id,
      pgp_hour_from
      ) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

--
-- END OF PROGRESSIVES CHANGES
--

--
-- BEGIN GT PLAYER TRACKING
--

IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[gt_playertracking_movements]') AND name = N'IX_gtpm_type_datetime')
CREATE NONCLUSTERED INDEX [IX_gtpm_type_datetime] ON [dbo].[gt_playertracking_movements] 
(
      [gtpm_type] ASC,
      [gtpm_datetime] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[gt_play_sessions]') AND name = N'IX_gtps_account_id_finished')
CREATE NONCLUSTERED INDEX [IX_gtps_account_id_finished] ON [dbo].[gt_play_sessions] 
(
        [gtps_finished] ASC,
        [gtps_account_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
	  
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[gt_play_sessions]') AND name = N'IX_gtps_account_id_started')
CREATE NONCLUSTERED INDEX [IX_gtps_account_id_started] ON [dbo].[gt_play_sessions] 
(
        [gtps_started] ASC,
        [gtps_account_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
	  
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[gt_play_sessions]') AND name = N'IX_gtps_finished_status')
CREATE NONCLUSTERED INDEX [IX_gtps_finished_status] ON [dbo].[gt_play_sessions] 
(
        [gtps_finished] ASC,
        [gtps_status] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
	  
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[gt_play_sessions]') AND name = N'IX_gtps_started_status')
CREATE NONCLUSTERED INDEX [IX_gtps_started_status] ON [dbo].[gt_play_sessions] 
(
        [gtps_started] ASC,
        [gtps_status] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
	  
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[gt_play_sessions]') AND name = N'IX_gtps_timestamp')
CREATE NONCLUSTERED INDEX [IX_gtps_timestamp] ON [dbo].[gt_play_sessions] 
(
        [gtps_timestamp] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
	  
--
-- END GT PLAYER TRACKING
--

/******* RECORDS *******/

--
-- BEGIN OF NOTECOUNTER
--

INSERT INTO general_params (gp_group_key ,gp_subject_key ,gp_key_value) VALUES ('Cage','BillCounter.Enabled',0)
INSERT INTO general_params (gp_group_key ,gp_subject_key ,gp_key_value) VALUES ('Cage','Collection.AutoClose',0)
GO

--
-- END OF NOTECOUNTER
--

--
-- BEGIN OF PROGRESSIVES CHANGES
--

-- SAS METER CATALOG
IF NOT EXISTS (SELECT * FROM SAS_METERS_CATALOG WHERE SMC_METER_CODE = 0x1000)
  INSERT INTO SAS_METERS_CATALOG (SMC_METER_CODE, SMC_DESCRIPTION, SMC_RECOMENDED) VALUES(0x1000, 'Total progressive jackpot credits', 7)  -- 0x1000
GO

-- SAS METER CATALOG PER GROUP
IF NOT EXISTS (SELECT * FROM SAS_METERS_CATALOG_PER_GROUP WHERE SMCG_GROUP_ID = 10004 AND SMCG_METER_CODE = 0x1000)
  INSERT INTO SAS_METERS_CATALOG_PER_GROUP (SMCG_GROUP_ID, SMCG_METER_CODE) VALUES (10004, 0x1000)
GO

UPDATE alarm_catalog SET alcg_name = 'Entrada al men� de operador (SAS host)', alcg_description = 'Entrada al men� de operador (SAS host)' 
  WHERE alcg_alarm_code = 393254 AND alcg_language_id = 10;
UPDATE alarm_catalog SET alcg_name = 'Salida del men� de operador (SAS host)', alcg_description = 'Salida del men� de operador (SAS host)' 
  WHERE alcg_alarm_code = 393255 AND alcg_language_id = 10;
UPDATE alarm_catalog SET alcg_name = 'SAS host operator menu accessed', alcg_description = 'SAS host operator menu accessed' 
  WHERE alcg_alarm_code = 393254 AND alcg_language_id = 9;
UPDATE alarm_catalog SET alcg_name = 'SAS host operator menu exited', alcg_description = 'SAS host operator menu exited' 
  WHERE alcg_alarm_code = 393255 AND alcg_language_id = 9;
GO

-- Progressives
--
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Progressives' AND GP_SUBJECT_KEY ='Enabled')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('Progressives','Enabled','0')

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Progressives' AND GP_SUBJECT_KEY ='AutoRegister.OnSpecialMeterIncrement')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('Progressives','AutoRegister.OnSpecialMeterIncrement','0')

-- SiteJackpot
--
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'SiteJackpot' AND GP_SUBJECT_KEY ='Range.ContributionPct')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('SiteJackpot', 'Range.ContributionPct', '0%;0.5%')

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'SiteJackpot' AND GP_SUBJECT_KEY ='Range.Jackpot01.ContributionPct')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('SiteJackpot', 'Range.Jackpot01.ContributionPct', '0%;100%')

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'SiteJackpot' AND GP_SUBJECT_KEY ='Range.Jackpot02.ContributionPct')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('SiteJackpot', 'Range.Jackpot02.ContributionPct', '0%;100%')

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'SiteJackpot' AND GP_SUBJECT_KEY ='Range.Jackpot03.ContributionPct')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('SiteJackpot', 'Range.Jackpot03.ContributionPct', '0%;100%')

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'SiteJackpot' AND GP_SUBJECT_KEY ='Range.Jackpot01.Minimum')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('SiteJackpot', 'Range.Jackpot01.Minimum', '0;5000000')

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'SiteJackpot' AND GP_SUBJECT_KEY ='Range.Jackpot02.Minimum')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('SiteJackpot', 'Range.Jackpot02.Minimum', '0;5000000')

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'SiteJackpot' AND GP_SUBJECT_KEY ='Range.Jackpot03.Minimum')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('SiteJackpot', 'Range.Jackpot03.Minimum', '0;5000000')

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'SiteJackpot' AND GP_SUBJECT_KEY ='Range.Occupation')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('SiteJackpot', 'Range.Occupation', '0%;100%')

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'SiteJackpot' AND GP_SUBJECT_KEY ='Range.BlockOnOutOfRange')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('SiteJackpot', 'Range.BlockOnOutOfRange', '1')

GO

--
-- END OF PROGRESSIVES CHANGES
--

--
-- BEGIN BANK TRANSACTION
--

-- Differentiate between credit and debit card
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.PaymentMethod' AND GP_SUBJECT_KEY = 'BankCard.DifferentiateType')
      INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('Cashier.PaymentMethod','BankCard.DifferentiateType','0')
GO

-- Show bank card details entry screen
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.PaymentMethod' AND GP_SUBJECT_KEY = 'BankCard.EnterTransactionDetails')
      INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('Cashier.PaymentMethod','BankCard.EnterTransactionDetails','0')
GO

-- Show check details entry screen
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.PaymentMethod' AND GP_SUBJECT_KEY = 'Check.EnterTransactionDetails')
      INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('Cashier.PaymentMethod','Check.EnterTransactionDetails','0')
GO

--
-- END BANK TRANSACTION
--

--
-- BEGIN OF GT PLAYER TRACKING
--

IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.AddAmount' AND GP_SUBJECT_KEY='GamblingTable.CustomButton1')
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY       , GP_SUBJECT_KEY,                GP_KEY_VALUE) 
                         VALUES ('Cashier.AddAmount', 'GamblingTable.CustomButton1', '50')
GO
IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.AddAmount' AND GP_SUBJECT_KEY='GamblingTable.CustomButton2')
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY       , GP_SUBJECT_KEY,                GP_KEY_VALUE) 
                         VALUES ('Cashier.AddAmount', 'GamblingTable.CustomButton2', '100')
GO
IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.AddAmount' AND GP_SUBJECT_KEY='GamblingTable.CustomButton3')
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY       , GP_SUBJECT_KEY,                GP_KEY_VALUE) 
                         VALUES ('Cashier.AddAmount', 'GamblingTable.CustomButton3', '200')
GO
IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.AddAmount' AND GP_SUBJECT_KEY='GamblingTable.CustomButton4')
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY       , GP_SUBJECT_KEY,                GP_KEY_VALUE) 
                         VALUES ('Cashier.AddAmount', 'GamblingTable.CustomButton4', '300')
GO

--
-- Insert General params
--
IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='GamingTable.PlayerTracking' AND GP_SUBJECT_KEY='CurrentBetWarningElapsedPlays')
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('GamingTable.PlayerTracking', 'CurrentBetWarningElapsedPlays' ,'5')
GO

IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='GamingTable.PlayerTracking' AND GP_SUBJECT_KEY='RestartPlayerTrackingInSwapSeats')
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('GamingTable.PlayerTracking', 'RestartPlayerTrackingInSwapSeats','0')
GO

IF NOT EXISTS (SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'GamingTable.PlayerTracking' AND GP_SUBJECT_KEY ='PlayerTracking.SellChips.Text')
     INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('GamingTable.PlayerTracking', 'PlayerTracking.SellChips.Text', '')
GO

IF NOT EXISTS (SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'GamingTable.PlayerTracking' AND GP_SUBJECT_KEY ='ShowInformationMessages')
     INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('GamingTable.PlayerTracking', 'ShowInformationMessages', 2)
GO

--
-- Delete General params
--
IF EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='GamingTable.PlayerTracking' AND GP_SUBJECT_KEY='PlayerTrackingSaveAutomatic')
      DELETE GENERAL_PARAMS WHERE GP_GROUP_KEY   = 'GamingTable.PlayerTracking' AND  GP_SUBJECT_KEY = 'PlayerTrackingSaveAutomatic'
GO

IF EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='GamingTable.PlayerTracking' AND GP_SUBJECT_KEY='ShowConfirmationExitMsg')
      DELETE GENERAL_PARAMS WHERE GP_GROUP_KEY   = 'GamingTable.PlayerTracking' AND  GP_SUBJECT_KEY = 'ShowConfirmationExitMsg'
GO

IF EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='GamingTable.PlayerTracking' AND GP_SUBJECT_KEY='ShowConfirmationChangePlayerMsg')
      DELETE GENERAL_PARAMS WHERE GP_GROUP_KEY   = 'GamingTable.PlayerTracking' AND  GP_SUBJECT_KEY = 'ShowConfirmationChangePlayerMsg'
GO

IF EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='GamingTable.PlayerTracking' AND GP_SUBJECT_KEY='ShowMsgWhenValidateValues')
      DELETE GENERAL_PARAMS WHERE GP_GROUP_KEY   = 'GamingTable.PlayerTracking' AND  GP_SUBJECT_KEY = 'ShowMsgWhenValidateValues'
GO

--
-- END OF GT PLAYER TRACKING
--

--
-- BEGIN OF CASHADVANCE
--

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.Voucher' AND GP_SUBJECT_KEY = 'PaymentMethod.CashAdvance.BankCard.Title')
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE)  VALUES ('Cashier.Voucher', 'PaymentMethod.CashAdvance.BankCard.Title', 'Adelanto de efectivo con tarjeta bancaria')
GO
  
IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.Voucher' AND GP_SUBJECT_KEY = 'PaymentMethod.CashAdvance.BankCard.HideVoucher')
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE)  VALUES ('Cashier.Voucher', 'PaymentMethod.CashAdvance.BankCard.HideVoucher', '0')
GO
  
IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.Voucher' AND GP_SUBJECT_KEY = 'PaymentMethod.CashAdvance.Check.Title')
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE)  VALUES ('Cashier.Voucher', 'PaymentMethod.CashAdvance.Check.Title', 'Adelanto de efectivo con cheque')
GO
  
IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.Voucher' AND GP_SUBJECT_KEY = 'PaymentMethod.CashAdvance.Check.HideVoucher')
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE)  VALUES ('Cashier.Voucher', 'PaymentMethod.CashAdvance.Check.HideVoucher', '0')
GO

--
-- END OF CASHADVANCE
--

--
-- BEGIN OF MAILING
--

IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Pattern' AND GP_SUBJECT_KEY = 'MailingAlarm.Enabled')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Pattern', 'MailingAlarm.Enabled', '1')
GO

--
-- END OF MAILING
--

--
-- NEW ALARMS
--

INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES ( 262152,  9, 0, N'Cash cage reopening', N'Cash cage reopening', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES ( 262152, 10, 0, N'Reapertura de b�veda', N'Reapertura de b�veda', 1)
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES ( 262152, 1, 0, GETDATE() )
GO

/******* STORED PROCEDURES *******/

--
-- BEGIN OF GT PLAYER TRACKING
--

--------------------------------------------------------------------------------
-- Copyright � 2013 Win Systems International
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: ProgresiveReports.sql
-- 
--   DESCRIPTION: Progresive reports reports procedures and functions
-- 
--        AUTHOR: Jos� Mart�nez
-- 
-- CREATION DATE: 04-SEP-2014
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 04-SEP-2014 JML    First release.
-- 04-SEP-2014 JML    PR_collection_by_machine_and_denomination
-- 09-SEP-2014 JML    PR_collection_by_machine_and_date
-------------------------------------------------------------------------------- 

--------------------------------------------------------------------------------
--   DESCRIPTION: Collection by machine and denomination
-- 
--        AUTHOR: Jos� Mart�nez
-- 
-- CREATION DATE: 04-SEP-2014
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 04-SEP-2014 JML    First release.
-- 04-SEP-2014 JML    PR_collection_by_machine_and_denomination
-------------------------------------------------------------------------------- 

IF OBJECT_ID (N'dbo.PR_collection_by_machine_and_denomination', N'P') IS NOT NULL
    DROP PROCEDURE dbo.PR_collection_by_machine_and_denomination;                 
GO    

CREATE PROCEDURE [dbo].[PR_collection_by_machine_and_denomination] 
 ( @pFromDt DATETIME   
  ,@pToDt DATETIME    
  ,@pClosingTime INTEGER
  ,@pTerminalWhere NVARCHAR(MAX) 
)
AS
BEGIN

-- DECLARATIONS
 DECLARE @Sql AS VARCHAR(MAX)
 DECLARE @Nrows AS int                              
 DECLARE @index AS int                              
 DECLARE @q AS table(i int)                       

 SET @pClosingTime = @pClosingTime * -1

 SET @Nrows  = DATEDIFF(DAY, @pFromDt, @pToDt)+1 
 SET @index = 0    

 WHILE @index < @Nrows                           
 BEGIN	                                          
   INSERT INTO @q VALUES(@index)	                
   SET @index = @index + 1                       
 END        

 SELECT I INTO #RP_TEMPORARY FROM @q 
 
 SET @Sql = CAST('
 SELECT   ORDER_DATE 
        , TE_TERMINAL_ID 
        , TE_NAME 
        , TE_PROVIDER_ID 
        , ISNULL(TE_MULTI_DENOMINATION, ''--'') AS DENOMINATION 
        , ISNULL(TTL_IN - TTL_OUT, 0) AS COLLECTION 
        , ISNULL(BILL_IN - BILL_OUT, 0) AS BILL 
        , ISNULL(TICKET_IN - TICKET_OUT, 0) AS TICKET 
        , ISNULL(MANUAL, 0) AS MANUAL 
        , ISNULL(CREDIT_CANCEL, 0) AS CREDIT_CANCEL 
        , ISNULL(JACKPOT_DE_SALA, 0) AS JACKPOT_DE_SALA 
        , ISNULL(PROGRESIVES, 0) AS PROGRESIVES 
        , ISNULL(NO_PROGRESIVES, 0) AS NO_PROGRESIVES 
        , ISNULL(PROGRESIVE_PROVISIONS, 0) AS PROGRESIVE_PROVISIONS 
   FROM   TERMINALS 
 LEFT JOIN (SELECT DATEADD(DAY, I, CAST(''' + CAST(@pFromDt AS VARCHAR(50)) + ''' AS DATE)) AS ORDER_DATE FROM #RP_TEMPORARY ) DIA ON ORDER_DATE <= GETDATE() 
 LEFT JOIN (SELECT   PS_TERMINAL_ID 
                   , CAST(DATEADD(HOUR, ' + CAST(@pClosingTime AS VARCHAR(5)) + ', PS_FINISHED) AS DATE) AS PS_FINISHED 
                   , SUM(PS_INITIAL_BALANCE+PS_CASH_IN)                                                                   AS BILL_IN 
                   , SUM(ISNULL(PS_FINAL_BALANCE,(0))+PS_CASH_OUT)                                                        AS BILL_OUT 
                   , SUM(ISNULL(PS_RE_TICKET_IN,(0))+ISNULL(PS_PROMO_RE_TICKET_IN,(0))+ISNULL(PS_PROMO_NR_TICKET_IN,(0))) AS TICKET_IN 
                   , SUM(ISNULL(PS_RE_TICKET_OUT,(0))+ISNULL(PS_PROMO_NR_TICKET_OUT,(0)))                                 AS TICKET_OUT 
                   , SUM(PS_TOTAL_CASH_IN)                                                                                AS TTL_IN 
                   , SUM(PS_TOTAL_CASH_OUT)                                                                               AS TTL_OUT 
              FROM   PLAY_SESSIONS   WITH (INDEX(IX_ps_finished_status)) 
             WHERE   PS_STATUS <> 0 AND PS_PROMO = 0 
               AND   PS_FINISHED >= CAST(''' + CAST(@pFromDt AS VARCHAR(50)) + ''' AS DATETIME) 
               AND   PS_FINISHED  < CAST(''' + CAST(@pToDt AS VARCHAR(50)) + ''' AS DATETIME) 
          GROUP BY   PS_TERMINAL_ID 
                   , CAST(DATEADD(HOUR, ' + CAST(@pClosingTime AS VARCHAR(5)) + ', PS_FINISHED) AS DATE) 
                   ) A ON PS_TERMINAL_ID = TE_TERMINAL_ID AND PS_FINISHED = DIA.ORDER_DATE 
 LEFT JOIN (SELECT   HP_TERMINAL_ID 
                   , CAST(DATEADD(HOUR, ' + CAST(@pClosingTime AS VARCHAR(5)) + ', HP_DATETIME) AS DATE) AS HP_DATETIME 
                   , SUM(CASE HP_TYPE WHEN 10 THEN ISNULL(HP_AMOUNT, 0) ELSE 0 END) AS MANUAL 
                   , SUM(CASE HP_TYPE WHEN  0 THEN ISNULL(HP_AMOUNT, 0) ELSE 0 END) AS CREDIT_CANCEL 
                   , SUM(CASE HP_TYPE WHEN 20 THEN ISNULL(HP_AMOUNT, 0) ELSE 0 END) AS JACKPOT_DE_SALA 
                   , SUM(CASE HP_TYPE WHEN  1 THEN CASE WHEN (HP_LEVEL>=1 AND HP_LEVEL<=32) THEN ISNULL(HP_AMOUNT, 0) ELSE 0 END END) AS PROGRESIVES 
                   , SUM(CASE HP_TYPE WHEN  1 THEN CASE WHEN (HP_LEVEL <1 OR  HP_LEVEL >32) THEN ISNULL(HP_AMOUNT, 0) ELSE 0 END END) AS NO_PROGRESIVES 
              FROM   HANDPAYS 
             WHERE   HP_DATETIME >= CAST(''' + CAST(@pFromDt AS VARCHAR(50)) + ''' AS DATETIME) 
               AND   HP_DATETIME < CAST(''' + CAST(@pToDt AS VARCHAR(50)) + ''' AS DATETIME) 
             GROUP   BY HP_TERMINAL_ID 
                   , CAST(DATEADD(HOUR, ' + CAST(@pClosingTime AS VARCHAR(5)) + ', HP_DATETIME) AS DATE)
                   ) B ON HP_TERMINAL_ID = TE_TERMINAL_ID AND HP_DATETIME = DIA.ORDER_DATE 
 LEFT JOIN (SELECT   PPT_TERMINAL_ID 
                   , CAST(DATEADD(HOUR, ' + CAST(@pClosingTime AS VARCHAR(5)) + ', PGP_HOUR_TO) AS DATE) AS PGP_HOUR_TO 
                   , SUM(ISNULL(PPT_AMOUNT, 0)) AS PROGRESIVE_PROVISIONS 
             FROM    PROGRESSIVES_PROVISIONS_TERMINALS 
        LEFT JOIN    PROGRESSIVES_PROVISIONS ON PGP_PROVISION_ID = PPT_PROVISION_ID 
                                            AND PGP_PROGRESSIVE_ID = PPT_PROGRESSIVE_ID 
                                            AND PGP_HOUR_TO >= CAST(''' + CAST(@pFromDt AS VARCHAR(50)) + ''' AS DATETIME) 
                                            AND PGP_HOUR_TO < CAST(''' + CAST(@pToDt AS VARCHAR(50)) + ''' AS DATETIME) 
             GROUP   BY PPT_TERMINAL_ID 
                   , CAST(DATEADD(HOUR, ' + CAST(@pClosingTime AS VARCHAR(5)) + ', PGP_HOUR_TO) AS DATE)
                   ) C ON PPT_TERMINAL_ID = TE_TERMINAL_ID AND PGP_HOUR_TO = DIA.ORDER_DATE 
  WHERE ( BILL_IN IS NOT NULL 
     OR   BILL_OUT IS NOT NULL 
     OR   TICKET_IN IS NOT NULL 
     OR   TICKET_OUT IS NOT NULL 
     OR   TTL_IN IS NOT NULL 
     OR   TTL_OUT IS NOT NULL 
     OR   MANUAL IS NOT NULL 
     OR   CREDIT_CANCEL IS NOT NULL 
     OR   JACKPOT_DE_SALA IS NOT NULL 
     OR   PROGRESIVES IS NOT NULL 
     OR   NO_PROGRESIVES IS NOT NULL 
     OR   PROGRESIVE_PROVISIONS IS NOT NULL ) ' + 
     CAST(@pTerminalWhere AS Varchar(max)) +
     ' ORDER   BY ISNULL(TE_MULTI_DENOMINATION, ''--''), TE_PROVIDER_ID, ORDER_DATE, TE_NAME '
      AS varchar(max))

EXECUTE (@Sql)

DROP TABLE #RP_TEMPORARY

END -- END PROCEDURE
GO

-- PERMISSIONS

GRANT EXECUTE ON [dbo].[PR_collection_by_machine_and_denomination] TO [wggui] WITH GRANT OPTION
GO


--------------------------------------------------------------------------------
--   DESCRIPTION: Collection by machine and date
-- 
--        AUTHOR: Jos� Mart�nez
-- 
-- CREATION DATE: 09-SEP-2014
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 09-SEP-2014 JML    First release.
-- 09-SEP-2014 JML    PR_collection_by_machine_and_date
-------------------------------------------------------------------------------- 

IF OBJECT_ID (N'dbo.PR_collection_by_machine_and_date', N'P') IS NOT NULL
    DROP PROCEDURE dbo.PR_collection_by_machine_and_date;                 
GO    

CREATE PROCEDURE [dbo].[PR_collection_by_machine_and_date] 
 ( @pFromDt DATETIME   
  ,@pToDt DATETIME    
  ,@pClosingTime INTEGER
  ,@pTerminalWhere NVARCHAR(MAX) 
)
AS
BEGIN

  -- DECLARATIONS
   DECLARE @Sql AS VARCHAR(MAX)
   DECLARE @Nrows AS int                              
   DECLARE @index AS int                              
   DECLARE @q AS table(i int)                       
   DECLARE @Columns VARCHAR(MAX)
   DECLARE @ColumnChk varchar(max)

   SET @pClosingTime = @pClosingTime * -1

   SET @Nrows  = DATEDIFF(DAY, @pFromDt, @pToDt)+1 
   SET @index = 0                                  

   WHILE @index < @Nrows                           
   BEGIN	                                          
     INSERT INTO @q VALUES(@index)	                
     SET @index = @index + 1                       
   END 
   
    SET @Columns = ''
    SET @ColumnChk = ''

   SELECT I INTO #TempTable_Days FROM @q 

  SELECT @Columns =  COALESCE(@Columns + '[' + CAST(CUD_DENOMINATION AS NVARCHAR(20)) + '],', '')
    FROM (SELECT DISTINCT CUD_DENOMINATION FROM CURRENCY_DENOMINATIONS 
                 UNION  
          SELECT DISTINCT TM_AMOUNT FROM TERMINAL_MONEY 
           WHERE TM_REPORTED >= GETDATE()-60
             AND TM_REPORTED <  GETDATE()
  ) AS DTM
  ORDER BY CUD_DENOMINATION

  SET @ColumnChk = REPLACE( @Columns, ',', ' IS NOT NULL or ')

  SET @Columns = LEFT(@Columns,LEN(@Columns)-1)
  SET @ColumnChk = LEFT(@ColumnChk,LEN(@ColumnChk)-3)
     
  SET @Sql = '
     SELECT   TE_PROVIDER_ID 
            , TE_NAME 
            , ISNULL(TE_MULTI_DENOMINATION, ''--'') AS DENOMINATION 
            , DATEADD(HOUR, ' + CAST(@pClosingTime * -1 AS VARCHAR(5)) + ', CAST(ORDER_DATE AS DATETIME)) AS ORDER_DATE
            , DATEADD(HOUR, ' + CAST((@pClosingTime * -1) + 24 AS VARCHAR(5)) + ', CAST(ORDER_DATE AS DATETIME)) AS ORDER_DATE_FIN
            , TICKET_IN_COUNT
            , TI_IN_AMOUNT_RE
            , TI_IN_AMOUNT_NO_RE
            , (TI_IN_AMOUNT_RE + TI_IN_AMOUNT_NO_RE) as TI_IN_AMOUNT
            , TICKET_OUT_COUNT
            , TI_OUT_AMOUNT_RE
            , TI_OUT_AMOUNT_NO_RE
            , (TI_OUT_AMOUNT_RE + TI_OUT_AMOUNT_NO_RE) as TI_OUT_AMOUNT
            , HAND_PAYS.MANUAL
            , HAND_PAYS.CREDIT_CANCEL
            , HAND_PAYS.JACKPOT_DE_SALA
            , HAND_PAYS.PROGRESIVES
            , HAND_PAYS.NO_PROGRESIVES
            , PROVISIONS.PROGRESIVE_PROVISIONS
            , 0 AS GAP_BILL
            , BILLS.*
       FROM   TERMINALS 
   LEFT JOIN ( SELECT   DATEADD(DAY, I, CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATE)) AS ORDER_DATE FROM #TempTable_Days ) DIA ON ORDER_DATE <= GETDATE() 
   LEFT JOIN ( SELECT   TI_LAST_ACTION_TERMINAL_ID
                      , CAST(DATEADD(HOUR, ' + CAST(@pClosingTime AS VARCHAR(5)) + ', TI_LAST_ACTION_DATETIME) AS DATE ) AS TI_LAST_ACTION_DATETIME
                      , COUNT(1) AS TICKET_IN_COUNT
                      , SUM(CASE WHEN TI_TYPE_ID = 2 THEN 0 ELSE TI_AMOUNT END) AS TI_IN_AMOUNT_RE
                      , SUM(CASE WHEN TI_TYPE_ID = 2 THEN TI_AMOUNT ELSE 0 END) AS TI_IN_AMOUNT_NO_RE
                 FROM   TICKETS 
                WHERE   TI_LAST_ACTION_DATETIME >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
                  AND   TI_LAST_ACTION_DATETIME <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
                GROUP   BY TI_LAST_ACTION_TERMINAL_ID, CAST(DATEADD(HOUR, ' + CAST(@pClosingTime AS VARCHAR(5)) + ', TI_LAST_ACTION_DATETIME) AS DATE )
             ) TICKETS_IN ON TE_TERMINAL_ID = TI_LAST_ACTION_TERMINAL_ID AND TI_LAST_ACTION_DATETIME = DIA.ORDER_DATE 
   LEFT JOIN ( SELECT   TI_CREATED_TERMINAL_ID
                      , CAST(DATEADD(HOUR, ' + CAST(@pClosingTime AS VARCHAR(5)) + ', TI_CREATED_DATETIME) AS DATE) AS TI_CREATED_DATETIME
                      , COUNT(1) AS TICKET_OUT_COUNT
                      , SUM(CASE WHEN TI_TYPE_ID = 2 THEN 0 ELSE TI_AMOUNT END) AS TI_OUT_AMOUNT_RE
                      , SUM(CASE WHEN TI_TYPE_ID = 2 THEN TI_AMOUNT ELSE 0 END) AS TI_OUT_AMOUNT_NO_RE
                 FROM   TICKETS 
                WHERE   TI_CREATED_DATETIME >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
                  AND   TI_CREATED_DATETIME <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
                  AND   TI_CREATED_TERMINAL_TYPE = 1     -- TITO_TERMINAL_TYPE.TERMINAL = 1
                  AND   TI_TYPE_ID IN (0,1,2,5) -- TITO_TICKET_TYPE
                                                             --CASHABLE = 0,
                                                             --PROMO_REDEEM = 1,
                                                             --PROMO_NONREDEEM = 2,  // only playable
                                                             --HANDPAY = 3,
                                                             --JACKPOT = 4,
                                                             --OFFLINE = 5
                GROUP   BY TI_CREATED_TERMINAL_ID, CAST(DATEADD(HOUR, ' + CAST(@pClosingTime AS VARCHAR(5)) + ', TI_CREATED_DATETIME) AS DATE) 
             ) TICKETS_OUT ON TE_TERMINAL_ID = TI_CREATED_TERMINAL_ID AND TI_CREATED_DATETIME = DIA.ORDER_DATE 
   LEFT JOIN (SELECT   HP_TERMINAL_ID 
                     , CAST(DATEADD(HOUR, ' + CAST(@pClosingTime AS VARCHAR(5)) + ', HP_DATETIME) AS DATE) AS HP_DATETIME 
                     , SUM(CASE HP_TYPE WHEN 10 THEN ISNULL(HP_AMOUNT, 0) ELSE 0 END) AS MANUAL 
                     , SUM(CASE HP_TYPE WHEN  0 THEN ISNULL(HP_AMOUNT, 0) ELSE 0 END) AS CREDIT_CANCEL 
                     , SUM(CASE HP_TYPE WHEN 20 THEN ISNULL(HP_AMOUNT, 0) ELSE 0 END) AS JACKPOT_DE_SALA 
                     , SUM(CASE HP_TYPE WHEN  1 THEN CASE WHEN (HP_LEVEL>=1 AND HP_LEVEL<=32) THEN ISNULL(HP_AMOUNT, 0) ELSE 0 END END) AS PROGRESIVES 
                     , SUM(CASE HP_TYPE WHEN  1 THEN CASE WHEN (HP_LEVEL <1 OR  HP_LEVEL >32) THEN ISNULL(HP_AMOUNT, 0) ELSE 0 END END) AS NO_PROGRESIVES 
                FROM   HANDPAYS 
               WHERE   HP_DATETIME >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
                 AND   HP_DATETIME <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
               GROUP   BY HP_TERMINAL_ID 
                     , CAST(DATEADD(HOUR, ' + CAST(@pClosingTime AS VARCHAR(5)) + ', HP_DATETIME) AS DATE)
                     ) HAND_PAYS ON HP_TERMINAL_ID = TE_TERMINAL_ID AND HP_DATETIME = DIA.ORDER_DATE 
   LEFT JOIN (SELECT   PPT_TERMINAL_ID 
                     , CAST(DATEADD(HOUR, ' + CAST(@pClosingTime AS VARCHAR(5)) + ', PGP_HOUR_TO) AS DATE) AS PGP_HOUR_TO 
                     , SUM(ISNULL(PPT_AMOUNT, 0)) AS PROGRESIVE_PROVISIONS 
               FROM    PROGRESSIVES_PROVISIONS_TERMINALS 
          LEFT JOIN    PROGRESSIVES_PROVISIONS ON PGP_PROVISION_ID = PPT_PROVISION_ID 
                                              AND PGP_PROGRESSIVE_ID = PPT_PROGRESSIVE_ID 
                                              AND PGP_HOUR_TO >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
                                              AND PGP_HOUR_TO <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
               GROUP   BY PPT_TERMINAL_ID 
                     , CAST(DATEADD(HOUR, ' + CAST(@pClosingTime AS VARCHAR(5)) + ', PGP_HOUR_TO) AS DATE)
                     ) PROVISIONS ON PPT_TERMINAL_ID = TE_TERMINAL_ID AND PGP_HOUR_TO = DIA.ORDER_DATE 
   LEFT JOIN (SELECT *  
                FROM ( SELECT   TM_TERMINAL_ID
                              , CAST(DATEADD(HOUR, ' + CAST(@pClosingTime AS VARCHAR(5)) + ', TM_REPORTED) AS DATE) AS TM_REPORTED
                              , TM_AMOUNT
                         FROM   TERMINAL_MONEY
                        where   TM_REPORTED >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
                          AND   TM_REPORTED <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
                     ) PIV
              PIVOT (  SUM(TM_AMOUNT) FOR TM_AMOUNT IN ('+ @Columns  + ')) AS Child
             )  BILLS ON TE_TERMINAL_ID = TM_TERMINAL_ID AND TM_REPORTED = DIA.ORDER_DATE 
   WHERE (  ' + @ColumnChk + '
         OR TICKET_IN_COUNT IS NOT NULL
         OR TI_IN_AMOUNT_RE IS NOT NULL
         OR TI_IN_AMOUNT_NO_RE IS NOT NULL
         OR TICKET_OUT_COUNT IS NOT NULL
         OR TI_OUT_AMOUNT_RE IS NOT NULL
         OR TI_OUT_AMOUNT_NO_RE IS NOT NULL
         OR HAND_PAYS.MANUAL IS NOT NULL
         OR HAND_PAYS.CREDIT_CANCEL IS NOT NULL
         OR HAND_PAYS.JACKPOT_DE_SALA IS NOT NULL
         OR HAND_PAYS.PROGRESIVES IS NOT NULL
         OR HAND_PAYS.NO_PROGRESIVES IS NOT NULL
         OR PROVISIONS.PROGRESIVE_PROVISIONS IS NOT NULL
          )' + 
         CAST(@pTerminalWhere AS Varchar(max)) +
'  ORDER BY   TE_PROVIDER_ID 
            , TE_TERMINAL_ID 
            , TE_NAME 
            , ISNULL(TE_MULTI_DENOMINATION, ''--'')
            , ORDER_DATE '

 -- PRINT @Sql

  EXECUTE (@Sql)

  DROP TABLE #TempTable_Days

END -- END PROCEDURE
GO

-- PERMISSIONS

GRANT EXECUTE ON [dbo].[PR_collection_by_machine_and_date] TO [wggui] WITH GRANT OPTION
GO

--------------------------------------------------------------------------------
-- Copyright � 2013 Win Systems International
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: GamingTables.sql
-- 
--   DESCRIPTION: Gaming Tables reports procedures and functions
-- 
--        AUTHOR: Dani Dom�nguez
-- 
-- CREATION DATE: 05-SEP-2014
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 05-SEP-2014 DDM    First release.
-- 19-SEP-2014 DCS    Edit Procedures Player Tracking


----------------------------------------------------------------------------------------------------------------
-------------------------- STORED PROCEDURE
----------------------------------------------------------------------------------------------------------------

/*
----------------------------------------------------------------------------------------------------------------
BASIC REPORT QUERY FOR GAMING TABLES

Version  Date           User     Description
----------------------------------------------------------------------------------------------------------------

Parameters:
   -- BASE TYPE:      Indicates if data is based on table types (0) or the tables (1).
   -- TIME INTERVAL:  Range of time to group data between days (0), months (1) and years (2).
   -- DATE FROM:      Start date for data collection. (If NULL then use first available date).
   -- DATE TO:        End date for data collection. (If NULL then use current date).
   -- ONLY ACTIVITY:  If true (1) results will only show tables or table types with activity inside the dates range,
                      else (0) results will include all tables or table types.
   -- ORDER BY:       (0) to sort by date (interval) descendant, (1) to sort by table type or table identifier.   
   -- VALID TYPES:    A string that contains the valid table types to list.
                      
Process: (From inside to outside)
   
   1- Core Query:
         Query all game table session data linked to the cashier sessions that where linked with a gaming_table and/or gaming table type.
   
   2- Specific report query:
         Here we group by base type and time interval and includes the session information for the interval, generating a temporary
         table.
         
   3- Filter activity:
         Join the temporary table with the time intervals table generated at first doing a left join to show all data, else an inner
         join to filter tables or table types without activity.

   4- Drop the temporary table.
   
 Results:
   
   We can create 6 different report, based on table types or tables and grouped by days, months and years.
----------------------------------------------------------------------------------------------------------------   
*/

IF OBJECT_ID (N'dbo.GT_Base_Report_Data_Player_Tracking', N'P') IS NOT NULL
    DROP PROCEDURE GT_Base_Report_Data_Player_Tracking;                 
GO 

CREATE PROCEDURE [dbo].[GT_Base_Report_Data_Player_Tracking] 
 ( @pBaseType INTEGER
  ,@pTimeInterval INTEGER
  ,@pDateFrom DATETIME 
  ,@pDateTo DATETIME
  ,@pOnlyActivity INTEGER
  ,@pOrderBy INTEGER
  ,@pValidTypes VARCHAR(4096)
)
AS
BEGIN

-- DECLARATIONS
   -- @_DAYS_AND_TABLES:    A temporary table with all intervals and all base type selected (used to show base types without activity).
   DECLARE @_DAYS_AND_TABLES TABLE(DATE_TIME DATETIME, TABLE_IDENTIFIER_VALUE BIGINT, TABLE_IDENT_NAME VARCHAR(50), TABLE_TYPE_IDENT BIGINT, TABLE_TYPE_NAME VARCHAR(50))
   -- @_DAY_VAR:            Variable to create the intervals
   DECLARE @_DAY_VAR DATETIME

-- PARAMETERS
   DECLARE @_BASE_TYPE        AS   INTEGER -- 0 BY TABLE TYPE, 1 BY TABLE 
   DECLARE @_TIME_INTERVAL    AS   INTEGER -- -1, 0 DAYS, 1 MONTH, 2 YEAR
   DECLARE @_DATE_FROM        AS   DATETIME
   DECLARE @_DATE_TO          AS   DATETIME
   DECLARE @_ONLY_ACTIVITY    AS   INTEGER -- 0 SHOW ALL, 1 SHOW ONLY WITH ACTIVITY
   DECLARE @_ORDER_BY         AS   INTEGER -- 0 BY INTERVAL DESC, 1 BY IDENTIFIER ASC
   DECLARE @_DELIMITER        AS   CHAR(1)
   DECLARE @_TYPES_TABLE      TABLE(SST_ID INT, SST_VALUE VARCHAR(50)) -- TO STORE THE VALID GAMING TABLE TYPES 
   
----------------------------------------------------------------------------------------------------------------

-- Initialzitation --
SET @_BASE_TYPE        =   @pBaseType
SET @_TIME_INTERVAL    =   @pTimeInterval
SET @_DATE_FROM        =   @pDateFrom
SET @_DATE_TO          =   @pDateTo
SET @_ONLY_ACTIVITY    =   @pOnlyActivity
SET @_ORDER_BY         =   @pOrderBy
SET @_DELIMITER        =   ','

----------------------------------------------------------------------------------------------------------------

-- CHECK DATE PARAMETERS
IF @_DATE_FROM IS NULL
BEGIN
   -- IF DATE FROM IS NULL, USER FIRST POSIBLE DATE
   SET @_DATE_FROM = CAST('' AS DATETIME)
   SET @pDateFrom  = CAST('' AS DATETIME)
END
IF @_DATE_TO IS NULL
BEGIN
   -- IF DATE TO IS NULL, QUERY UNTIL CURRENT DATE
   SET @_DATE_TO = CAST(GETDATE() AS DATETIME)
   SET @pDateTo  = CAST(GETDATE() AS DATETIME)
END

-- ASSIGN TYPES PARAMETER INTO TABLE
INSERT INTO @_TYPES_TABLE SELECT * FROM dbo.SplitStringIntoTable(@pValidTypes, @_DELIMITER, DEFAULT)

IF @_TIME_INTERVAL <> -1 
 BEGIN
   -- INTERVALS DATES AND TABLE PREPARATION
   -- PREPARE DATES RANGE DEPENDING ON TIME INTERVAL
   SET @_DATE_FROM = CASE 
                        WHEN @_TIME_INTERVAL = 0 THEN -- BY DAY
                           DATEADD(dd, 0, DATEDIFF(dd, 0, @_DATE_FROM))
                        WHEN @_TIME_INTERVAL = 1 THEN -- BY MONTH
                           CAST(CAST(YEAR(@_DATE_FROM) AS VARCHAR(4)) + RIGHT('0' + CAST(MONTH(@_DATE_FROM) AS VARCHAR(2)), 2) + '01' AS DATETIME)
                        WHEN @_TIME_INTERVAL = 2 THEN -- BY YEAR
                           CAST(CAST(YEAR(@_DATE_FROM) AS VARCHAR(4)) + '0101' AS DATETIME)
                     END

   SET @_DATE_TO = CASE 
                        WHEN @_TIME_INTERVAL = 0 THEN -- BY DAY
                           DATEADD(DAY, 1, CAST(@_DATE_TO AS DATETIME))
                        WHEN @_TIME_INTERVAL = 1 THEN -- BY MONTH
                           DATEADD(MONTH, 1, CAST(CAST(YEAR(@_DATE_TO) AS VARCHAR(4)) + RIGHT('0' + CAST(MONTH(@_DATE_TO) AS VARCHAR(2)), 2) + '01' AS DATETIME))
                        WHEN @_TIME_INTERVAL = 2 THEN -- BY YEAR
                           DATEADD(YEAR, 1, CAST(CAST(YEAR(@_DATE_TO) AS VARCHAR(4)) + '0101' AS DATETIME))
                     END
              
   -- PREPARE THE TIME INTERVALS TABLE
   SET @_DAY_VAR = @_DATE_FROM
   WHILE @_DAY_VAR < @_DATE_TO
   BEGIN

         -- SET THE LINK FIELD FOR THE RESULTS 
         IF @_BASE_TYPE = 0
          BEGIN
            -- LINK WITH TABLE TYPES
            INSERT INTO @_DAYS_AND_TABLES 
                 SELECT   CAST(@_DAY_VAR AS DATETIME), X.GTT_GAMING_TABLE_TYPE_ID, X.GTT_NAME AS TABLE_IDENT, NULL, NULL
                   FROM   GAMING_TABLES_TYPES AS X
                  WHERE   X.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )  
          END 
         ELSE
          BEGIN
            -- LINK WITH TABLES
            INSERT INTO @_DAYS_AND_TABLES 
                 SELECT   CAST(@_DAY_VAR AS DATETIME), X.GT_GAMING_TABLE_ID, X.GT_NAME AS TABLE_IDENT, Z.GTT_GAMING_TABLE_TYPE_ID, Z.GTT_NAME 
                   FROM   GAMING_TABLES AS X
              LEFT JOIN   GAMING_TABLES_TYPES AS Z
                     ON   Z.GTT_GAMING_TABLE_TYPE_ID = X.GT_TYPE_ID
                  WHERE   Z.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
          END
          
          -- SET INCREMENT
          SET @_DAY_VAR = CASE 
                               WHEN @_TIME_INTERVAL = 0 THEN DATEADD(DAY,1,@_DAY_VAR)     -- DAY
                               WHEN @_TIME_INTERVAL = 1 THEN DATEADD(MONTH,1,@_DAY_VAR)   -- MONTH
                               WHEN @_TIME_INTERVAL = 2 THEN DATEADD(YEAR,1,@_DAY_VAR)    -- YEAR
                          END
   END

END -- IF INTERVALS <> - 1
ELSE
BEGIN
   
   -- SET THE LINK FIELD FOR THE RESULTS WITHOUT INTERVALS
   IF @_BASE_TYPE = 0
    BEGIN
      -- LINK WITH TABLE TYPES
      INSERT INTO   @_DAYS_AND_TABLES 
           SELECT   NULL, X.GTT_GAMING_TABLE_TYPE_ID, X.GTT_NAME AS TABLE_IDENT, NULL, NULL
             FROM   GAMING_TABLES_TYPES AS X
            WHERE   X.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE ) 
    END 
   ELSE
    BEGIN
      -- LINK WITH TABLES
      INSERT INTO   @_DAYS_AND_TABLES 
           SELECT   NULL, X.GT_GAMING_TABLE_ID, X.GT_NAME AS TABLE_IDENT, Z.GTT_GAMING_TABLE_TYPE_ID, Z.GTT_NAME 
             FROM   GAMING_TABLES AS X
        LEFT JOIN   GAMING_TABLES_TYPES AS Z
               ON   Z.GTT_GAMING_TABLE_TYPE_ID = X.GT_TYPE_ID
            WHERE   Z.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )   
    END
   
 END
  
-- SELECT INTO TEMPORARY TABLE WITH ALL DATA
-- SPECIFIC REPORT QUERY 
  SELECT   X.TABLE_IDENTIFIER   
         , X.TABLE_NAME
         , X.TABLE_TYPE
         , X.TABLE_TYPE_NAME
         , X.CM_DATE_ONLY    
         , MIN(X.OPEN_HOUR) AS OPEN_HOUR
         , MAX(X.CLOSE_HOUR) AS CLOSE_HOUR
         , X.SESSION_HOURS AS SESSION_HOURS
         , X.BUY_IN
         , X.CHIPS_IN
         , X.CHIPS_OUT
         , X.TOTAL_PLAYED
         , X.AVERAGE_BET
         , X.CHIPS_IN + X.BUY_IN AS TOTAL_DROP
         , CASE WHEN (X.CHIPS_IN + X.BUY_IN) =0
		   		   THEN 0
		   		   ELSE ((X.CHIPS_OUT/(X.CHIPS_IN + X.BUY_IN))*100) 
		       END AS HOLD
         , CASE WHEN X.TOTAL_PLAYED =0
		   		   THEN 0
		   		   ELSE (X.NETWIN/X.TOTAL_PLAYED) 
		       END AS PAYOUT
		     , X.NETWIN AS NETWIN
         
   INTO #GT_TEMPORARY_REPORT_DATA
   
   FROM (  
            -- CORE QUERY
					  SELECT   CASE 
													WHEN @_TIME_INTERVAL = 0 THEN     -- TO FILTER BY DAY
											 DATEADD(dd, 0, DATEDIFF(dd, 0, CS_OPENING_DATE))
													WHEN @_TIME_INTERVAL = 1 THEN     -- TO FILTER BY MONTH
											 CAST(CAST(YEAR(CS_OPENING_DATE) AS VARCHAR(4)) + RIGHT('0' + CAST(MONTH(CS_OPENING_DATE) AS VARCHAR(2)), 2) + '01' AS DATETIME)
													WHEN @_TIME_INTERVAL = 2 THEN     -- TO FILTER BY YEAR
													CAST(CAST(YEAR(CS_OPENING_DATE) AS VARCHAR(4)) + '01'+ '01' AS DATETIME)
											END AS CM_DATE_ONLY 
									 , (CASE WHEN @_BASE_TYPE = 0 THEN GTT_GAMING_TABLE_TYPE_ID ELSE GTPS_GAMING_TABLE_ID END)  AS TABLE_IDENTIFIER  -- GET THE BASE TYPE IDENTIFIER
									 , (CASE WHEN @_BASE_TYPE = 0 THEN GTT_NAME ELSE GT_NAME END) AS TABLE_NAME                  -- GET THE BASE TYPE IDENTIFIER NAME
									 , (CASE WHEN @_BASE_TYPE = 0 THEN NULL ELSE GTT_GAMING_TABLE_TYPE_ID END) AS TABLE_TYPE                -- TYPE 
									 , (CASE WHEN @_BASE_TYPE = 0 THEN NULL ELSE GTT_NAME END) AS TABLE_TYPE_NAME      -- TYPE NAME
									 , SUM(GTPS_TOTAL_SELL_CHIPS) AS BUY_IN  
									 , SUM(GTPS_CHIPS_IN) AS CHIPS_IN
									 , SUM(GTPS_CHIPS_OUT)  AS CHIPS_OUT
									 , SUM(GTPS_PLAYED_AMOUNT) AS TOTAL_PLAYED
									 , SUM(GTPS_PLAYED_AVERAGE)  AS AVERAGE_BET
									 , SUM(GTPS_NETWIN) AS NETWIN                                                 
									 , CS_OPENING_DATE AS OPEN_HOUR
									 , CS_CLOSING_DATE AS CLOSE_HOUR
									 , DATEDIFF(MINUTE, CS_OPENING_DATE, ISNULL(CS_CLOSING_DATE, GETDATE())) AS SESSION_HOURS
						  FROM   GT_PLAY_SESSIONS   
						 INNER   JOIN GAMING_TABLES ON GT_GAMING_TABLE_ID = GTPS_GAMING_TABLE_ID
						 INNER   JOIN GAMING_TABLES_TYPES ON GT_TYPE_ID = GTT_GAMING_TABLE_TYPE_ID
						 INNER   JOIN GAMING_TABLES_SESSIONS ON GTPS_GAMING_TABLE_SESSION_ID = GTS_GAMING_TABLE_SESSION_ID
						 INNER   JOIN CASHIER_SESSIONS ON GTS_CASHIER_SESSION_ID = CS_SESSION_ID
						   AND   CS_OPENING_DATE >= @pDateFrom AND CS_OPENING_DATE < @pDateTo
						 WHERE   CS_STATUS = 1   
						  AND    GT_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
						 GROUP   BY   GTPS_GAMING_TABLE_ID
									, GT_NAME
									, CS_OPENING_DATE 
									, CS_CLOSING_DATE 
									, GTT_GAMING_TABLE_TYPE_ID
									, GTT_NAME  
             -- END CORE QUERY      
        ) AS X          
 
 GROUP BY  X.TABLE_IDENTIFIER   
         , X.TABLE_NAME
         , X.TABLE_TYPE
         , X.TABLE_TYPE_NAME
         , X.CM_DATE_ONLY    
         , X.SESSION_HOURS
         , X.BUY_IN
         , X.CHIPS_IN
         , X.CHIPS_OUT
         , X.TOTAL_PLAYED
         , X.AVERAGE_BET
         , X.NETWIN
  -- Group by indentifier and time interval 

IF @_TIME_INTERVAL <> -1
BEGIN
   -- INTERVALS DATES AND TABLE FINAL PREPARATION

   -- FILTER ACTIVITY
   IF @_ONLY_ACTIVITY = 0
    BEGIN
      -- JOIN THE SELECT WITH ALL DATA
      SELECT  
              DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER,
              DT.TABLE_IDENT_NAME AS TABLE_NAME,
              DT.TABLE_TYPE_IDENT AS TABLE_TYPE, 
              DT.TABLE_TYPE_NAME AS TABLE_TYPE_NAME,
              ISNULL(ZZ.BUY_IN, 0) AS BUY_IN, 
			        ISNULL(ZZ.CHIPS_IN, 0) AS CHIPS_IN, 
              ISNULL(ZZ.CHIPS_OUT, 0) AS CHIPS_OUT, 
              ISNULL(ZZ.TOTAL_PLAYED, 0) AS TOTAL_PLAYED, 
              ISNULL(ZZ.AVERAGE_BET, 0) AS AVERAGE_BET, 
              ISNULL(ZZ.TOTAL_DROP, 0) AS TOTAL_DROP, 
              ISNULL(ZZ.HOLD, 0) AS HOLD, 
              ISNULL(ZZ.PAYOUT, 0) AS PAYOUT, 
              ISNULL(ZZ.NETWIN, 0) AS NETWIN, 
              ZZ.OPEN_HOUR,
              ZZ.CLOSE_HOUR,
              CASE WHEN ZZ.CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED,
              CASE WHEN ZZ.SESSION_HOURS = 0 THEN DATEDIFF(MINUTE,ZZ.OPEN_HOUR,GETDATE()) ELSE ISNULL(ZZ.SESSION_HOURS, 0) END AS SESSION_HOURS,
              DATE_TIME

      FROM @_DAYS_AND_TABLES DT
        
        LEFT JOIN #GT_TEMPORARY_REPORT_DATA ZZ 
                     ON   
                          (
                              (@_TIME_INTERVAL = 0 AND DATE_TIME = ZZ.CM_DATE_ONLY)
                           OR (@_TIME_INTERVAL = 1 AND MONTH(DATE_TIME) = MONTH(ZZ.CM_DATE_ONLY) AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
                           OR (@_TIME_INTERVAL = 2 AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
                          )
                    AND   TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER

       -- SET ORDER 
       ORDER BY   DT.TABLE_TYPE_IDENT ASC,
                  CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC,
                  DATE_TIME DESC;                                             
                    
    END
   ELSE
    BEGIN
      -- JOIN THE SELECT HIDDING WITHOUT ACTIVITY
      SELECT  
              DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER,
              DT.TABLE_IDENT_NAME AS TABLE_NAME,
              DT.TABLE_TYPE_IDENT AS TABLE_TYPE, 
              DT.TABLE_TYPE_NAME AS TABLE_TYPE_NAME,
              ISNULL(ZZ.BUY_IN, 0) AS BUY_IN, 
			  ISNULL(ZZ.CHIPS_IN, 0) AS CHIPS_IN, 
              ISNULL(ZZ.CHIPS_OUT, 0) AS CHIPS_OUT, 
              ISNULL(ZZ.TOTAL_PLAYED, 0) AS TOTAL_PLAYED, 
              ISNULL(ZZ.AVERAGE_BET, 0) AS AVERAGE_BET, 
              ISNULL(ZZ.TOTAL_DROP, 0) AS TOTAL_DROP, 
              ISNULL(ZZ.HOLD, 0) AS HOLD, 
              ISNULL(ZZ.PAYOUT, 0) AS PAYOUT, 
              ISNULL(ZZ.NETWIN, 0) AS NETWIN, 
              ZZ.OPEN_HOUR,
              ZZ.CLOSE_HOUR,
              CASE WHEN ZZ.CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED,
              CASE WHEN ZZ.SESSION_HOURS = 0 THEN DATEDIFF(MINUTE,ZZ.OPEN_HOUR,GETDATE()) ELSE ISNULL(ZZ.SESSION_HOURS, 0) END AS SESSION_HOURS,
              DATE_TIME 

      FROM @_DAYS_AND_TABLES DT
        
       INNER JOIN #GT_TEMPORARY_REPORT_DATA ZZ 
                     ON   
                          (
                              (@_TIME_INTERVAL = 0 AND DATE_TIME = ZZ.CM_DATE_ONLY)
                           OR (@_TIME_INTERVAL = 1 AND MONTH(DATE_TIME) = MONTH(ZZ.CM_DATE_ONLY) AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
                           OR (@_TIME_INTERVAL = 2 AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
                          )
                    AND   TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER
       
       -- SET ORDER             
       ORDER BY   DT.TABLE_TYPE_IDENT ASC,
                  CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC,
                  DATE_TIME DESC;
                  
    END -- IF ONLY_ACTIVITY

END
ELSE  -- ELSE WITHOUT INTERVALS
BEGIN
   
   -- FINAL WITHOUT INTERVALS 
   
      -- FILTER ACTIVITY
   IF @_ONLY_ACTIVITY = 0
    BEGIN
            -- JOIN DATA INCLUDING WITHOUT ACTIVITY
            SELECT   DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER,
                     DT.TABLE_IDENT_NAME AS TABLE_NAME,
                     DT.TABLE_TYPE_IDENT AS TABLE_TYPE, 
                     DT.TABLE_TYPE_NAME,
										 ISNULL(ZZ.BUY_IN, 0) AS BUY_IN, 
										 ISNULL(ZZ.CHIPS_IN, 0) AS CHIPS_IN, 
										 ISNULL(ZZ.CHIPS_OUT, 0) AS CHIPS_OUT, 
										 ISNULL(ZZ.TOTAL_PLAYED, 0) AS TOTAL_PLAYED, 
										 ISNULL(ZZ.AVERAGE_BET, 0) AS AVERAGE_BET, 
										 ISNULL(ZZ.TOTAL_DROP, 0) AS TOTAL_DROP, 
										 ISNULL(ZZ.HOLD, 0) AS HOLD, 
										 ISNULL(ZZ.PAYOUT, 0) AS PAYOUT, 
										 ISNULL(ZZ.NETWIN, 0) AS NETWIN, 
                     OPEN_HOUR,
                     CLOSE_HOUR,
                     CASE WHEN CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED,
                     CASE WHEN SESSION_HOURS = 0 THEN DATEDIFF(MINUTE, OPEN_HOUR, GETDATE()) ELSE ISNULL(SESSION_HOURS, 0) END AS SESSION_HOURS
              FROM   @_DAYS_AND_TABLES DT
                 
         LEFT JOIN   #GT_TEMPORARY_REPORT_DATA ZZ
                ON   DT.TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER
                
          ORDER BY   DT.TABLE_TYPE_IDENT ASC,
                     CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC,
                     DATE_TIME DESC;                            

     END
    ELSE
     BEGIN
            -- JOIN DATA WITH ONLY ACTIVITY
            SELECT   DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER,
                     DT.TABLE_IDENT_NAME AS TABLE_NAME,
                     DT.TABLE_TYPE_IDENT AS TABLE_TYPE, 
                     DT.TABLE_TYPE_NAME,
                     ISNULL(ZZ.BUY_IN, 0) AS BUY_IN, 
										 ISNULL(ZZ.CHIPS_IN, 0) AS CHIPS_IN, 
										 ISNULL(ZZ.CHIPS_OUT, 0) AS CHIPS_OUT, 
										 ISNULL(ZZ.TOTAL_PLAYED, 0) AS TOTAL_PLAYED, 
										 ISNULL(ZZ.AVERAGE_BET, 0) AS AVERAGE_BET, 
										 ISNULL(ZZ.TOTAL_DROP, 0) AS TOTAL_DROP, 
										 ISNULL(ZZ.HOLD, 0) AS HOLD, 
										 ISNULL(ZZ.PAYOUT, 0) AS PAYOUT,
										 ISNULL(ZZ.NETWIN, 0) AS NETWIN,  
                     OPEN_HOUR,
                     CLOSE_HOUR,
                     CASE WHEN CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED,
                     CASE WHEN SESSION_HOURS = 0 THEN DATEDIFF(MINUTE, OPEN_HOUR, GETDATE()) ELSE ISNULL(SESSION_HOURS, 0) END AS SESSION_HOURS
              FROM   @_DAYS_AND_TABLES DT
                 
         INNER JOIN   #GT_TEMPORARY_REPORT_DATA ZZ
                ON   DT.TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER
                
           ORDER BY   DT.TABLE_TYPE_IDENT ASC,
                      CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC,
                      DATE_TIME DESC;                            
                      
     END  
 END

-- ERASE THE TEMPORARY DATA 
DROP TABLE #GT_TEMPORARY_REPORT_DATA

END -- END PROCEDURE
GO

-- PERMISSIONS

GRANT EXECUTE ON [dbo].[GT_Base_Report_Data_Player_Tracking] TO [wggui] WITH GRANT OPTION
GO

--
-- END OF GT PLAYER TRACKING
--

---
--- #WIG-1244
---

  --------------------------------------------------------------------------------
  -- Copyright � 2013 Win Systems International
  --------------------------------------------------------------------------------
  --
  --   MODULE NAME: Insert_Providers.sql
  --
  --   DESCRIPTION: Insert_Providers
  --
  --        AUTHOR: Jos� Mart�nez
  --
  -- CREATION DATE: 21-MAY-2013
  --
  -- REVISION HISTORY:
  --
  -- Date        Author Description
  -- ----------- ------ ----------------------------------------------------------
  -- 21-MAY-2013 JML    First release. Different from the center.
  -- 17-SEP-2014 JML    Fixed Bug #WIG-1244
  -------------------------------------------------------------------------------- 
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Update_Providers]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Update_Providers]
GO

CREATE PROCEDURE [dbo].[Update_Providers]
                   @pProviderId INT
                 , @pName NVARCHAR(50)
                 , @pHide BIT
                 , @pPointsMultiplier MONEY
                 , @p3gs BIT
                 , @p3gsVendorId NVARCHAR(50)
                 , @p3gsVendorIp NVARCHAR(50)
                 , @pSiteJackpot BIT
                 , @pOnlyRedeemable BIT 
                 , @pMsSequenceId BIGINT		   
AS
BEGIN 

  DECLARE @pOtherProviderId as BIGINT

  SET @pOtherProviderId = (SELECT PV_ID FROM PROVIDERS WHERE PV_NAME = @pName)
    
  IF (@pOtherProviderId IS NOT NULL AND @pOtherProviderId <> @pProviderId)
  BEGIN  
    UPDATE   PROVIDERS
       SET   PV_NAME =  'CHANGING-' + CAST( @pOtherProviderId AS NVARCHAR)
     WHERE   PV_ID   = @pOtherProviderId	     
  END
   
  IF EXISTS (SELECT 1 FROM PROVIDERS WHERE PV_ID = @pProviderId)
  BEGIN
    UPDATE   PROVIDERS
       SET   PV_NAME           = @pName				
           , PV_HIDE           = @pHide		  
           , PV_MS_SEQUENCE_ID = @pMsSequenceId
     WHERE   PV_ID             = @pProviderId        
  END
  ELSE
  BEGIN
    SET IDENTITY_INSERT PROVIDERS ON
    
    INSERT INTO   PROVIDERS
                ( PV_ID
                , PV_NAME
                , PV_HIDE
                , PV_MS_SEQUENCE_ID)
         VALUES ( @pProviderId			  
                , @pName				  
                , @pHide		  
                , @pMsSequenceId)
    
    SET IDENTITY_INSERT PROVIDERS OFF
  END
END 
GO

---
---
---


---
--- BEGIN MAILING
---

--------------------------------------------------------------------------------
-- Copyright � 2014 Win Systems International
--------------------------------------------------------------------------------
--
--   MODULE NAME: Update_AlarmPatterns.sql
--
--   DESCRIPTION: Update Patterns in the Site
--
--        AUTHOR: Jos� Mart�nez
--
-- CREATION DATE: 16-MAY-2014
--
-- REVISION HISTORY:
--
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 16-MAY-2014 JML    First release.  
-- 08-JUL-2014 JPJ    Added multilanguage support
-- 06-AUG-2014 DCS    Fixed Bug WIG-1162: Alarm catalog without update
-- 18-SEP-2014 JPJ    Mailing alarm functionality
-------------------------------------------------------------------------------- 

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Update_AlarmPatterns]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Update_AlarmPatterns]
GO 

CREATE PROCEDURE   [dbo].[Update_AlarmPatterns]
                    @pId                 BIGINT
                  , @pName               NVARCHAR(50)
                  , @pDescription        NVARCHAR(350)
                  , @pActive             BIT
                  , @pPattern            XML
                  , @pAlCode             INT
                  , @pAlName             NVARCHAR(50)
                  , @pAlDescription      NVARCHAR(350)
                  , @pAlSeverity         INT
                  , @pType               INT
                  , @pSource             INT
                  , @pLifeTime           INT
                  , @pLastFind           DATETIME
                  , @pDetections         INT
                  , @pScheduleTimeFrom   INT
                  , @pScheduleTimeTo     INT
                  , @pTimestamp          BIGINT

AS
BEGIN

  IF NOT EXISTS (SELECT 1 FROM PATTERNS WHERE PT_ID = @pId)
  
  BEGIN
    SET IDENTITY_INSERT PATTERNS ON

    INSERT INTO   PATTERNS ( PT_ID
                           , PT_NAME
                           , PT_DESCRIPTION
                           , PT_ACTIVE
                           , PT_PATTERN
                           , PT_AL_CODE
                           , PT_AL_NAME
                           , PT_AL_DESCRIPTION
                           , PT_AL_SEVERITY
                           , PT_TYPE
                           , PT_SOURCE
                           , PT_LIFE_TIME
                           , PT_LAST_FIND
                           , PT_DETECTIONS
                           , PT_SCHEDULE_TIME_FROM
                           , PT_SCHEDULE_TIME_TO
                           , PT_TIMESTAMP 
                           , PT_ACTIVE_MAILING )
                    VALUES ( @pId 
                           , @pName
                           , @pDescription
                           , @pActive
                           , @pPattern
                           , @pAlCode
                           , @pAlName
                           , @pAlDescription
                           , @pAlSeverity
                           , @pType
                           , @pSource
                           , @pLifeTime
                           , NULL
                           , 0
                           , @pScheduleTimeFrom
                           , @pScheduleTimeTo
                           , @pTimestamp 
                           , 0 ) 

    SET IDENTITY_INSERT PATTERNS OFF
    
		-- English language
	  INSERT INTO ALARM_CATALOG (ALCG_ALARM_CODE,  ALCG_LANGUAGE_ID, ALCG_TYPE, ALCG_NAME, ALCG_DESCRIPTION, ALCG_VISIBLE)
												 VALUES (@pAlCode,         9,                1,         @pAlName,  @pAlDescription,  1           )
	  
		-- Spanish language
		INSERT INTO ALARM_CATALOG (ALCG_ALARM_CODE, ALCG_LANGUAGE_ID, ALCG_TYPE, ALCG_NAME, ALCG_DESCRIPTION, ALCG_VISIBLE)
												 VALUES (@pAlCode,        10,               1,         @pAlName,  @pAlDescription,  1           )
    
    -- Insert alarm in his category 
		INSERT INTO ALARM_CATALOG_PER_CATEGORY (ALCC_ALARM_CODE, ALCC_CATEGORY, ALCC_TYPE, ALCC_DATETIME)
																			VALUES (@pAlCode,        44,            1,         GetDate()    )
  
  END
  ELSE
  BEGIN
    UPDATE   PATTERNS
       SET   PT_NAME               = @pName
           , PT_DESCRIPTION        = @pDescription
           , PT_ACTIVE             = @pActive
           , PT_PATTERN            = @pPattern
           , PT_AL_CODE            = @pAlCode
           , PT_AL_NAME            = @pAlName
           , PT_AL_DESCRIPTION     = @pAlDescription
           , PT_AL_SEVERITY        = @pAlSeverity
           , PT_TYPE               = @pType
           , PT_SOURCE             = @pSource
           , PT_LIFE_TIME          = @pLifeTime
           , PT_SCHEDULE_TIME_FROM = @pScheduleTimeFrom
           , PT_SCHEDULE_TIME_TO   = @pScheduleTimeTo
           , PT_TIMESTAMP          = @pTimestamp
     WHERE   PT_ID  = @pId
     
     UPDATE ALARM_CATALOG 
			 SET   ALCG_DESCRIPTION			= @pAlDescription
					 , ALCG_NAME						= @pAlName
		 WHERE   ALCG_ALARM_CODE			= @pAlCode   
		 
  END

END
GO 

---
--- END MAILING
---