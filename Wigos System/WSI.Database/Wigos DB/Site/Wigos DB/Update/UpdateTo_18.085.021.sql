/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 84;

SET @New_ReleaseId = 85;
SET @New_ScriptName = N'UpdateTo_18.085.021.sql';
SET @New_Description = N'Tables and Indexes for Flags & GP for WelcomeMessage.';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/

CREATE TABLE [dbo].[flags](
	[fl_flag_id] [bigint] IDENTITY(1,1) NOT NULL,
	[fl_type] [int] NOT NULL,
	[fl_status] [int] NOT NULL,
	[fl_expiration_type] [int] NOT NULL,
	[fl_expiration_date] [datetime] NULL,
	[fl_expiration_interval] [int] NULL,
	[fl_created] [datetime] NOT NULL,
	[fl_name] [nvarchar](50) NOT NULL,
	[fl_description] [nvarchar](250) NULL,
	[fl_color] [int] NOT NULL,
 CONSTRAINT [PK_flags] PRIMARY KEY CLUSTERED 
(
	[fl_flag_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[account_flags](
	[af_unique_id] [bigint] IDENTITY(1,1) NOT NULL,
	[af_account_id] [bigint] NOT NULL,
	[af_flag_id] [bigint] NOT NULL,
	[af_status] [int] NOT NULL,
	[af_valid_from] [datetime] NOT NULL,
	[af_valid_to] [datetime] NULL,
	[af_added_datetime] [datetime] NOT NULL,
	[af_added_by_user_id] [int] NULL,
	[af_added_by_promotion_id] [bigint] NULL,
	[af_cancelled_datetime] [datetime] NULL,
	[af_cancelled_user_id] [int] NULL,
 CONSTRAINT [PK_account_flags] PRIMARY KEY CLUSTERED 
(
	[af_unique_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[account_flags]  WITH CHECK ADD  CONSTRAINT [FK_account_flags_accounts] FOREIGN KEY([af_account_id])
REFERENCES [dbo].[accounts] ([ac_account_id])
GO

ALTER TABLE [dbo].[account_flags] CHECK CONSTRAINT [FK_account_flags_accounts]
GO

ALTER TABLE [dbo].[account_flags]  WITH CHECK ADD  CONSTRAINT [FK_account_flags_flags] FOREIGN KEY([af_flag_id])
REFERENCES [dbo].[flags] ([fl_flag_id])
GO

ALTER TABLE [dbo].[account_flags] CHECK CONSTRAINT [FK_account_flags_flags]
GO

CREATE TABLE [dbo].[promotion_flags](
	[pf_promotion_id] [bigint] NOT NULL,
	[pf_type] [int] NOT NULL,
	[pf_flag_id] [bigint] NOT NULL,
	[pf_flag_count] [int] NOT NULL,
 CONSTRAINT [PK_promotion_flags] PRIMARY KEY CLUSTERED 
(
	[pf_promotion_id] ASC,
	[pf_type] ASC,
	[pf_flag_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

/****** INDEXES ******/

CREATE NONCLUSTERED INDEX [IX_af_flag_status] ON [dbo].[account_flags] 
(
	[af_flag_id] ASC,
	[af_status] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

CREATE NONCLUSTERED INDEX [IX_af_status_account] ON [dbo].[account_flags] 
(
	[af_status] ASC,
	[af_account_id] ASC,
	[af_flag_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

CREATE NONCLUSTERED INDEX [IX_fl_type_status] ON [dbo].[flags] 
(
	[fl_type] ASC,
	[fl_status] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

GO

/****** RECORDS ******/

INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('WelcomeMessage', 'Line1', 'Bienvenido');
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('WelcomeMessage', 'Line2', '');

