/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 387;

SET @New_ReleaseId = 388;
SET @New_ScriptName = N'UpdateTo_18.388.041.sql';
SET @New_Description = N'zsp_CardInfo'; 

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/**** GENERAL PARAM *****/

/******* TABLES  *******/

/******* RECORDS *******/

/******* PROCEDURES *******/

IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'zsp_CardInfo')
       DROP PROCEDURE [zsp_CardInfo]
GO

create PROCEDURE [dbo].[zsp_CardInfo]
    @TrackData       varchar(24),
@CardIndex  int output,
@_account_id bigint output,
@CustumerId Bigint output,  
@AccountType int output,
@CardStatus int output,
@Owner      nvarchar(250) output

WITH EXECUTE AS OWNER
AS

BEGIN
                  
BEGIN TRANSACTION

      
      SET @CardIndex = 1
      set @CustumerId = 1
      set @AccountType = 1
      set @CardStatus =1
      
      IF @TrackData IS NOT NULL  
        BEGIN
         SELECT  @_account_id = AC_ACCOUNT_ID , @Owner = ac_holder_name
                                      FROM   ACCOUNTS
                                     WHERE   AC_TRACK_DATA = dbo.TrackDataToInternal(@TrackData)
        END
        
        
        IF @_account_id > 0
              begin
                  SET @CardIndex = 1
                  set @CustumerId = @_account_id
                  set @AccountType = 1
                  set @AccountType = 1
                  
                  set @Owner = (select ac_holder_name from accounts where ac_account_id=@_account_id)
                  if @Owner is null set @AccountType=0
              END
            Else

            begin
                  SET @CardIndex = 0
                  set @CustumerId = 0
                  set @AccountType = 0
                  set @CardStatus =0
            end
      
      commit transaction 
     
end --Zsp_GetCardInfo

GO

IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'S2S_CardInfo')
       DROP PROCEDURE [S2S_CardInfo]
GO

create PROCEDURE [dbo].[S2S_CardInfo]
      @TrackData       varchar(24)
WITH EXECUTE AS OWNER
AS
BEGIN

declare     @CardIndex                         int ,
                  @CustumerId                 bigint ,
                  @AccountType                       int ,
                  @CardStatus                        int

  DECLARE @status_code  int
  DECLARE @status_text  varchar (254)
  DECLARE @error_text   nvarchar (MAX)
  DECLARE @input        nvarchar(MAX)
  DECLARE @output       nvarchar(MAX)
  DECLARE @_try         int
  DECLARE @_max_tries   int
  DECLARE @_exception   bit
  DECLARE @_completed   bit
  DECLARE @_account_id BIGINT
  declare @Owner        varchar(250)
  SET @_try       = 0
  SET @_max_tries = 6        -- AJQ 22-DES-2014, The number of retries has been incremented to 6
  SET @_completed = 0
print 'good'
  BEGIN TRANSACTION

  WHILE (@_completed = 0)
  BEGIN

    SET @_exception   = 0
    SET @status_code  = 4
    SET @status_text  = 'Access Denied'
    SET @error_text   = ''

    BEGIN TRY


      
      EXECUTE dbo.[zsp_CardInfo] @TrackData ,  
                                            @CardIndex     output ,
                                            @_account_id output ,
                                            @CustumerId  output ,
                                            @AccountType output ,
                                            @CardStatus    output ,
                                            @Owner   output 
      
      SET @_completed = 1          
	  set @status_code =0
	  set @status_text ='OK'
    END TRY

    BEGIN CATCH
    
      ROLLBACK TRANSACTION

  
      IF (@_try >= @_max_tries) 
      BEGIN
        SET @_completed  = 1;
        SET @_exception  = 1;
        SET @status_code = 4;
        SET @status_text = 'Access Denied';
        SET @error_text  = ' ERROR_NUMBER: '    + CAST(ERROR_NUMBER()               AS NVARCHAR)
                         + ' ERROR_SEVERITY: '  + CAST(ERROR_SEVERITY()             AS NVARCHAR)
                         + ' ERROR_STATE: '     + CAST(ERROR_STATE()                AS NVARCHAR)
                         + ' ERROR_PROCEDURE: ' + CAST(ISNULL(ERROR_PROCEDURE(),'') AS NVARCHAR)
                         + ' ERROR_LINE: '      + CAST(ERROR_LINE()                 AS NVARCHAR)
                         + ' ERROR_MESSAGE: '   + CAST(ERROR_MESSAGE()              AS NVARCHAR(MAX))
      END
      ELSE
      BEGIN
        WAITFOR DELAY '00:00:02'
      END

      BEGIN TRANSACTION

    END CATCH
  END

  SET @input = '@TrackData='       + @TrackData

  IF @error_text <> ''
    SET @error_text = ';Details='      + @error_text

  SET @output = 'CardIndex='      + CAST( @CardIndex as nvarchar)
              + ';CustumerId='     +     CAST(@CustumerId as nvarchar)
              + ';AccountType='    +     cast(@AccountType as nvarchar)
              + ';CardStatus='     +     cast(@CardStatus as nvarchar)




              + @error_text
              + '; TryIndex='      + CAST (@_try AS NVARCHAR)


  COMMIT TRANSACTION

  SELECT 
   @status_code							as statuscode,
   @status_text							as statustext,
   @CardIndex							as CardIndex,
   CAST(@CustumerId  as bigint)			as CustomerId,
   @AccountType							as AccountType,
   @CardStatus							as CardStatus
END 

go


GRANT EXECUTE ON [dbo].[zsp_CardInfo] TO [3GS] WITH GRANT OPTION 
GO
GRANT EXECUTE ON [dbo].[S2S_CardInfo] TO [3GS] WITH GRANT OPTION 
GO

-- [EIBE]
GRANT EXECUTE ON [dbo].[zsp_CardInfo] TO [EIBE] WITH GRANT OPTION 
GO
GRANT EXECUTE ON [dbo].[S2S_CardInfo] TO [EIBE] WITH GRANT OPTION 
GO

-- [wg_interface]
GRANT EXECUTE ON [dbo].[zsp_CardInfo] TO [wg_interface] WITH GRANT OPTION 
GO
GRANT EXECUTE ON [dbo].[S2S_CardInfo] TO [wg_interface] WITH GRANT OPTION 
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetMonthlyNetwin]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetMonthlyNetwin]
GO

CREATE PROCEDURE [dbo].[GetMonthlyNetwin]
@pMonth int,
@pYear  int
AS
BEGIN

DECLARE @_aux_date AS DATETIME
DECLARE @_aux_date_init AS DATETIME
DECLARE @_aux_date_final AS DATETIME
DECLARE @_aux_site_name AS NVARCHAR(200)

-- Create temp table for whole report
CREATE TABLE #TT_MONTHLYNETWIN (
TSMH_MONTH				NVARCHAR(5), 
SITE_NAME				NVARCHAR(200), 
TE_NAME					NVARCHAR(200), 
TE_SERIAL_NUMBER		NVARCHAR(200),
BILL_IN					MONEY, 
TICKET_IN_REDIMIBLE		MONEY,
TICKET_OUT_REDIMIBLE	MONEY,
TOTAL_IN				MONEY,
TOTAL_OUT				MONEY,
REDEEM_TICKETS			MONEY,
TICKET_IN_PROMOTIONAL	MONEY,
CANCELLED_CREDITS		MONEY,
JACKPOTS				MONEY,
LGA_NETWIN				MONEY,
TOTAL_COIN_IN			MONEY,
TOTAL_WON				MONEY,
TOTAL_COIN_IN_TOTAL_WON	MONEY)

--Obtain the INITIAL and FINAL date
set @_aux_date = CONVERT(DATETIME,CONVERT(NVARCHAR(4),@pYear) + '-' + CONVERT(NVARCHAR(2),@pMonth) + '-' +  '1')
set @_aux_date_init = CONVERT(NVARCHAR(25),DATEADD(dd,-(DAY(@_aux_date)-1),@_aux_date),101)
set @_aux_date_final = DATEADD(dd,-(DAY(DATEADD(mm,1,@_aux_date))),DATEADD(hh,23,DATEADD(mi,59,DATEADD(ss,59,DATEADD(mm,1,@_aux_date)))))

--Obtain site ID and site name
set @_aux_site_name = (SELECT GP_KEY_VALUE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Site' AND GP_SUBJECT_KEY = 'Identifier') + ' - ' + (SELECT GP_KEY_VALUE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Site' AND GP_SUBJECT_KEY = 'Name')

INSERT INTO #TT_MONTHLYNETWIN
SELECT    (CASE WHEN TSMH_GROUP_ID IS NULL THEN DATEPART(MM,TSMH_DATETIME) ELSE DATEPART(MM,TSMH_CREATED_DATETIME)END) AS TSMH_MONTH
        , @_aux_site_name AS SITE_NAME
        , TE_NAME
        , TE_SERIAL_NUMBER
        /*BILL IN*/
        , SUM( CASE WHEN TSMH_TYPE <> 20 THEN
                  - CASE WHEN (TSMH_METER_CODE = 11)                           THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               ELSE
                    CASE WHEN (TSMH_METER_CODE = 11)                           THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               END ) AS BILL_IN
        /*TICKET IN*/
        , SUM( CASE WHEN TSMH_TYPE <> 20 THEN
                  - CASE WHEN (TSMH_METER_CODE = 128)                          THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               ELSE
                    CASE WHEN (TSMH_METER_CODE = 128)                          THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               END ) AS TICKET_IN_REDIMIBLE
        /*TICKET OUT*/
        , SUM( CASE WHEN TSMH_TYPE <> 20 THEN
                  - CASE WHEN (TSMH_METER_CODE = 134)                          THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               ELSE
                    CASE WHEN (TSMH_METER_CODE = 134)                          THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               END ) AS TICKET_OUT_REDIMIBLE
        /*TOTAL IN = BILL IN + TICKET IN*/
        , SUM( CASE WHEN TSMH_TYPE <> 20 THEN
                  - CASE WHEN (TSMH_METER_CODE IN (11, 128))                   THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               ELSE
                    CASE WHEN (TSMH_METER_CODE IN (11, 128))                   THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               END ) AS TOTAL_IN
        /*TOTAL OUT = JACKPOTS + CANCELLED CREDITS + TICKET OUT*/
        , SUM( CASE WHEN TSMH_TYPE <> 20 THEN
                  - CASE WHEN (TSMH_METER_CODE IN (2, 3, 134))                 THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               ELSE
                    CASE WHEN (TSMH_METER_CODE IN (2, 3, 134))                 THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               END ) AS  TOTAL_OUT
        /*REDEEM TICKETS*/
        , SUM( CASE WHEN TSMH_TYPE <> 20 THEN 
                  - CASE WHEN (TSMH_METER_CODE = 128)                          THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
                  + CASE WHEN (TSMH_METER_CODE = 134)                          THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END 
               ELSE 
                    CASE WHEN (TSMH_METER_CODE = 128)                          THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
                  - CASE WHEN (TSMH_METER_CODE = 134)                          THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END 
               END ) AS  REDEEM_TICKETS
        /*TICKET IN PROMOTIONAL (redeem, no redeem)*/
        , SUM( CASE WHEN TSMH_TYPE <> 20 THEN
                  - CASE WHEN (TSMH_METER_CODE IN (130, 132))                  THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               ELSE
                    CASE WHEN (TSMH_METER_CODE IN (130, 132))                  THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               END ) AS TICKET_IN_PROMOTIONAL
        /*CANCELLED CREDITS*/
        , SUM( CASE WHEN TSMH_TYPE <> 20 THEN
                  - CASE WHEN (TSMH_METER_CODE = 3)                            THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               ELSE
                    CASE WHEN (TSMH_METER_CODE = 3)                            THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               END ) AS CANCELLED_CREDITS
        /*JACKPOTS*/
        , SUM( CASE WHEN TSMH_TYPE <> 20 THEN
                  - CASE WHEN (TSMH_METER_CODE = 2)                            THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               ELSE
                    CASE WHEN (TSMH_METER_CODE = 2)                            THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               END ) AS JACKPOTS
        /*LGA NETWIN*/
        , SUM( CASE WHEN TSMH_TYPE <> 20 THEN
                  - CASE WHEN (TSMH_METER_CODE IN (11, 128))                   THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
                  + CASE WHEN (TSMH_METER_CODE IN (3, 130, 132, 134))          THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               ELSE
                    CASE WHEN (TSMH_METER_CODE IN (11, 128))                   THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
                  - CASE WHEN (TSMH_METER_CODE IN (3, 130, 132, 134))          THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               END ) AS LGA_NETWIN
        /*TOTAL COIN IN*/
        , SUM( CASE WHEN TSMH_TYPE <> 20 THEN
                  - CASE WHEN (TSMH_METER_CODE = 0)								THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               ELSE
                    CASE WHEN (TSMH_METER_CODE = 0)								THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               END ) AS TOTAL_COIN_IN

        /*TOTAL WON = COIN OUT + JACKPOTS*/
        , SUM( CASE WHEN TSMH_TYPE <> 20 THEN
                  - CASE WHEN (TSMH_METER_CODE IN (1, 2))						THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               ELSE
                    CASE WHEN (TSMH_METER_CODE IN (1, 2))						THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               END ) AS  TOTAL_WON
        /*TOTAL COIN IN - TOTAL WON*/
        , SUM( CASE WHEN TSMH_TYPE <> 20 THEN
                  - CASE WHEN (TSMH_METER_CODE = 0)								THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
                  + CASE WHEN (TSMH_METER_CODE IN (1, 2))						THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               ELSE
                    CASE WHEN (TSMH_METER_CODE = 0)								THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
                  - CASE WHEN (TSMH_METER_CODE IN (1, 2))						THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               END ) AS TOTAL_COIN_IN_TOTAL_WON
FROM TERMINALS
INNER JOIN TERMINAL_SAS_METERS_HISTORY WITH(INDEX(PK_terminal_sas_meters_history)) ON (TE_TERMINAL_ID = TSMH_TERMINAL_ID)
WHERE TSMH_DATETIME >= @_aux_date_init AND TSMH_DATETIME < @_aux_date_final
      AND TSMH_TYPE IN ( 20   /*MINCETUR_DAILY_REPORT*/
                       , 10   /*METER_RESET*/
                       , 11   /*METER_ROLLOVER*/
                       , 15   /*METER_SERVICE_RAM_CLEAR*/
                       , 16   /*METER_MACHINE_RAM_CLEAR*/
                         )
      AND TSMH_METER_CODE IN  ( 0   /*Total IN*/
                              , 1   /*Total OUT*/
                              , 2   /*Jackpots*/
                              , 3   /*Cancelled credits*/
                              , 11  /*Bill IN*/
                              , 128 /*Redimible Ticket IN*/
                              , 130 /*Ticket IN redeem promotional*/
                              , 132 /*Ticket IN no redeem promotional*/
                              , 134 /*Ticket OUT */
                              ) 
GROUP BY  TE_TERMINAL_ID, TE_NAME, TE_SERIAL_NUMBER, TE_MACHINE_SERIAL_NUMBER,
          (CASE WHEN TSMH_GROUP_ID IS NULL THEN DATEPART(MM,TSMH_DATETIME) ELSE DATEPART(MM,TSMH_CREATED_DATETIME) END)
ORDER BY TE_NAME
          
/*Add the TOTAL ROW*/
SELECT TSMH_MONTH,SITE_NAME, TE_NAME, TE_SERIAL_NUMBER, BILL_IN, TICKET_IN_REDIMIBLE, TICKET_OUT_REDIMIBLE, TOTAL_IN, TOTAL_OUT, REDEEM_TICKETS, TICKET_IN_PROMOTIONAL, CANCELLED_CREDITS, JACKPOTS, LGA_NETWIN, TOTAL_COIN_IN, TOTAL_WON, TOTAL_COIN_IN_TOTAL_WON
FROM #TT_MONTHLYNETWIN
UNION ALL
SELECT 'TOTAL' AS TSMH_MONTH, '' AS SITE_NAME, '' AS TE_NAME, '' AS TE_SERIAL_NUMBER, SUM(BILL_IN), SUM(TICKET_IN_REDIMIBLE), SUM(TICKET_OUT_REDIMIBLE), SUM(TOTAL_IN), SUM(TOTAL_OUT), SUM(REDEEM_TICKETS), SUM(TICKET_IN_PROMOTIONAL), SUM(CANCELLED_CREDITS), SUM(JACKPOTS), SUM(LGA_NETWIN), SUM(TOTAL_COIN_IN), SUM(TOTAL_WON), SUM(TOTAL_COIN_IN_TOTAL_WON)
FROM #TT_MONTHLYNETWIN
          
DROP TABLE #TT_MONTHLYNETWIN

END
GO

GRANT EXECUTE ON [dbo].[GetMonthlyNetwin] TO wggui WITH GRANT OPTION
GO

BEGIN
  DECLARE @rtc_form_id INT
  DECLARE @rtc_design_sheet VARCHAR(MAX)

  SET @rtc_design_sheet =                                   '<ArrayOfReportToolDesignSheetsDTO>
                                                                <ReportToolDesignSheetsDTO>
                                                                  <LanguageResources>
                                                                    <NLS09>
                                                                      <Label>Monthly Report NetWin</Label>
                                                                    </NLS09>
                                                                    <NLS10>
                                                                      <Label>Reporte mensual de NetWin</Label>
                                                                    </NLS10>
                                                                  </LanguageResources>
                                                                  <Columns>
                                                                    <ReportToolDesignColumn>
                                                                      <Code>TSMH_MONTH</Code>
                                                                      <Width>150</Width>
                                                                      <EquityMatchType>Equality</EquityMatchType>
                                                                      <LanguageResources>
                                                                        <NLS09>
                                                                          <Label>Month</Label>
                                                                        </NLS09>
                                                                        <NLS10>
                                                                          <Label>Mes</Label>
                                                                        </NLS10>
                                                                      </LanguageResources>
                                                                    </ReportToolDesignColumn>
                                                                    <ReportToolDesignColumn>
                                                                      <Code>TSMH_DAY</Code>
                                                                      <Width>300</Width>
                                                                      <EquityMatchType>Equality</EquityMatchType>
                                                                      <LanguageResources>
                                                                        <NLS09>
                                                                          <Label>Day</Label>
                                                                        </NLS09>
                                                                        <NLS10>
                                                                          <Label>D�a</Label>
                                                                        </NLS10>
                                                                      </LanguageResources>
                                                                    </ReportToolDesignColumn>
                                                                    <ReportToolDesignColumn>
                                                                      <Code>SITE_NAME</Code>
                                                                      <Width>300</Width>
                                                                      <EquityMatchType>Equality</EquityMatchType>
                                                                      <LanguageResources>
                                                                        <NLS09>
                                                                          <Label>Site Name</Label>
                                                                        </NLS09>
                                                                        <NLS10>
                                                                          <Label>Nombre de la sala</Label>
                                                                        </NLS10>
                                                                      </LanguageResources>
                                                                    </ReportToolDesignColumn>
                                                                    <ReportToolDesignColumn>
                                                                      <Code>TE_NAME</Code>
                                                                      <Width>300</Width>
                                                                      <EquityMatchType>Equality</EquityMatchType>
                                                                      <LanguageResources>
                                                                        <NLS09>
                                                                          <Label>Terminal name</Label>
                                                                        </NLS09>
                                                                        <NLS10>
                                                                          <Label>Nombre de la terminal</Label>
                                                                        </NLS10>
                                                                      </LanguageResources>
                                                                    </ReportToolDesignColumn>
                                                                    <ReportToolDesignColumn>
                                                                      <Code>TE_SERIAL_NUMBER</Code>
                                                                      <Width>300</Width>
                                                                      <EquityMatchType>Equality</EquityMatchType>
                                                                      <LanguageResources>
                                                                        <NLS09>
                                                                          <Label>Serial number</Label>
                                                                        </NLS09>
                                                                        <NLS10>
                                                                          <Label>N�mero de serie</Label>
                                                                        </NLS10>
                                                                      </LanguageResources>
                                                                    </ReportToolDesignColumn>
                                                                    <!--Bill IN-->
                                                                    <ReportToolDesignColumn>
                                                                      <Code>BILL_IN</Code>
                                                                      <Width>300</Width>
                                                                      <EquityMatchType>Equality</EquityMatchType>
                                                                      <LanguageResources>
                                                                        <NLS09>
                                                                          <Label>Bill IN</Label>
                                                                        </NLS09>
                                                                        <NLS10>
                                                                          <Label>Entrada de billetes</Label>
                                                                        </NLS10>
                                                                      </LanguageResources>
                                                                    </ReportToolDesignColumn>
                                                                    <!--Ticket IN-->
                                                                    <ReportToolDesignColumn>
                                                                      <Code>TICKET_IN_REDIMIBLE</Code>
                                                                      <Width>300</Width>
                                                                      <EquityMatchType>Equality</EquityMatchType>
                                                                      <LanguageResources>
                                                                        <NLS09>
                                                                          <Label>Ticket IN redimible</Label>
                                                                        </NLS09>
                                                                        <NLS10>
                                                                          <Label>Entrada ticket redimible</Label>
                                                                        </NLS10>
                                                                      </LanguageResources>
                                                                    </ReportToolDesignColumn>
                                                                    <!--Ticket OUT-->
                                                                    <ReportToolDesignColumn>
                                                                      <Code>TICKET_OUT_REDIMIBLE</Code>
                                                                      <Width>300</Width>
                                                                      <EquityMatchType>Equality</EquityMatchType>
                                                                      <LanguageResources>
                                                                        <NLS09>
                                                                          <Label>Ticket OUT redimible</Label>
                                                                        </NLS09>
                                                                        <NLS10>
                                                                          <Label>Salida ticket redimible</Label>
                                                                        </NLS10>
                                                                      </LanguageResources>
                                                                    </ReportToolDesignColumn>
                                                                    <!--Total IN-->
                                                                    <ReportToolDesignColumn>
                                                                      <Code>TOTAL_IN</Code>
                                                                      <Width>300</Width>
                                                                      <EquityMatchType>Equality</EquityMatchType>
                                                                      <LanguageResources>
                                                                        <NLS09>
                                                                          <Label>Total IN</Label>
                                                                        </NLS09>
                                                                        <NLS10>
                                                                          <Label>Total entradas</Label>
                                                                        </NLS10>
                                                                      </LanguageResources>
                                                                    </ReportToolDesignColumn>
                                                                    <!--Total OUT-->
                                                                    <ReportToolDesignColumn>
                                                                      <Code>TOTAL_OUT</Code>
                                                                      <Width>300</Width>
                                                                      <EquityMatchType>Equality</EquityMatchType>
                                                                      <LanguageResources>
                                                                        <NLS09>
                                                                          <Label>Total OUT</Label>
                                                                        </NLS09>
                                                                        <NLS10>
                                                                          <Label>Total salidas</Label>
                                                                        </NLS10>
                                                                      </LanguageResources>
                                                                    </ReportToolDesignColumn>
                                                                    <!--Redeem tickets-->
                                                                    <ReportToolDesignColumn>
                                                                      <Code>REDEEM_TICKETS</Code>
                                                                      <Width>300</Width>
                                                                      <EquityMatchType>Equality</EquityMatchType>
                                                                      <LanguageResources>
                                                                        <NLS09>
                                                                          <Label>Redeem tickets</Label>
                                                                        </NLS09>
                                                                        <NLS10>
                                                                          <Label>Tickets redimibles</Label>
                                                                        </NLS10>
                                                                      </LanguageResources>
                                                                    </ReportToolDesignColumn>
                                                                    <!--Promotional ticket IN-->
                                                                    <ReportToolDesignColumn>
                                                                      <Code>TICKET_IN_PROMOTIONAL</Code>
                                                                      <Width>300</Width>
                                                                      <EquityMatchType>Equality</EquityMatchType>
                                                                      <LanguageResources>
                                                                        <NLS09>
                                                                          <Label>Promotional ticket IN</Label>
                                                                        </NLS09>
                                                                        <NLS10>
                                                                          <Label>Entrada ticket promocional</Label>
                                                                        </NLS10>
                                                                      </LanguageResources>
                                                                    </ReportToolDesignColumn>
                                                                    <!--Cancelled credits-->
                                                                    <ReportToolDesignColumn>
                                                                      <Code>CANCELLED_CREDITS</Code>
                                                                      <Width>300</Width>
                                                                      <EquityMatchType>Equality</EquityMatchType>
                                                                      <LanguageResources>
                                                                        <NLS09>
                                                                          <Label>Cancelled credits</Label>
                                                                        </NLS09>
                                                                        <NLS10>
                                                                          <Label>Cr�ditos cancelados</Label>
                                                                        </NLS10>
                                                                      </LanguageResources>
                                                                    </ReportToolDesignColumn>
                                                                    <!--Jackpots-->
                                                                    <ReportToolDesignColumn>
                                                                      <Code>JACKPOTS</Code>
                                                                      <Width>300</Width>
                                                                      <EquityMatchType>Equality</EquityMatchType>
                                                                      <LanguageResources>
                                                                        <NLS09>
                                                                          <Label>Jackpots</Label>
                                                                        </NLS09>
                                                                        <NLS10>
                                                                          <Label>Jackpots</Label>
                                                                        </NLS10>
                                                                      </LanguageResources>
                                                                    </ReportToolDesignColumn>
                                                                    <!--LGA Netwin-->
                                                                    <ReportToolDesignColumn>
                                                                      <Code>LGA_NETWIN</Code>
                                                                      <Width>300</Width>
                                                                      <EquityMatchType>Equality</EquityMatchType>
                                                                      <LanguageResources>
                                                                        <NLS09>
                                                                          <Label>LGA Netwin</Label>
                                                                        </NLS09>
                                                                        <NLS10>
                                                                          <Label>LGA Netwin</Label>
                                                                        </NLS10>
                                                                      </LanguageResources>
                                                                    </ReportToolDesignColumn>
                                                                    <!--TOTAL COIN IN-->
                                                                    <ReportToolDesignColumn>
                                                                      <Code>TOTAL_COIN_IN</Code>
                                                                      <Width>300</Width>
                                                                      <EquityMatchType>Equality</EquityMatchType>
                                                                      <LanguageResources>
                                                                        <NLS09>
                                                                          <Label>Total Coin In</Label>
                                                                        </NLS09>
                                                                        <NLS10>
                                                                          <Label>Total Jugado</Label>
                                                                        </NLS10>
                                                                      </LanguageResources>
                                                                    </ReportToolDesignColumn>
                                                                    <!--TOTAL WON-->
                                                                    <ReportToolDesignColumn>
                                                                      <Code>TOTAL_WON</Code>
                                                                      <Width>300</Width>
                                                                      <EquityMatchType>Equality</EquityMatchType>
                                                                      <LanguageResources>
                                                                        <NLS09>
                                                                          <Label>Total Won</Label>
                                                                        </NLS09>
                                                                        <NLS10>
                                                                          <Label>Total Ganado</Label>
                                                                        </NLS10>
                                                                      </LanguageResources>
                                                                    </ReportToolDesignColumn>
                                                                    <!--TOTAL COIN IN - TOTAL WON-->
                                                                    <ReportToolDesignColumn>
                                                                      <Code>TOTAL_COIN_IN_TOTAL_WON</Code>
                                                                      <Width>300</Width>
                                                                      <EquityMatchType>Equality</EquityMatchType>
                                                                      <LanguageResources>
                                                                        <NLS09>
                                                                          <Label>Total Coin In - Total Won</Label>
                                                                        </NLS09>
                                                                        <NLS10>
                                                                          <Label>Total Jugado - Total Ganado</Label>
                                                                        </NLS10>
                                                                      </LanguageResources>
                                                                    </ReportToolDesignColumn>
                                                                  </Columns>
                                                                </ReportToolDesignSheetsDTO>
                                                              </ArrayOfReportToolDesignSheetsDTO>'
  
  IF NOT EXISTS(SELECT 1 FROM report_tool_config WHERE rtc_store_name = 'GetMonthlyNetwin')
  BEGIN
    SELECT @rtc_form_id = ISNULL(MAX(rtc_form_id),10999) + 1
      FROM report_tool_config
    
    INSERT INTO [dbo].[REPORT_TOOL_CONFIG] VALUES /*RTC_REPORT_TOOL_ID     4*/
                                      /*RTC_FORM_ID*/        (11003,
                                      /*RTC_LOCATION_MENU*/   2, 
                                      /*RTC_REPORT_NAME*/     '<LanguageResources><NLS09><Label>Monthly Report NetWin</Label></NLS09><NLS10><Label>Reporte mensual de NetWin</Label></NLS10></LanguageResources>',
                                      /*RTC_STORE_NAME*/      'GetMonthlyNetwin',
                                      /*RTC_DESIGN_FILTER*/   '<ReportToolDesignFilterDTO><FilterType>MonthYear</FilterType></ReportToolDesignFilterDTO>',
                                      /*RTC_DESIGN_SHEETS*/   @rtc_design_sheet,
                                      /*RTC_MAILING*/         0,
                                      /*RTC_STATUS*/          1,
                                      /*RTC_MODE*/            1)
                                      
  END
  ELSE
  BEGIN
      UPDATE [dbo].[REPORT_TOOL_CONFIG] 
      SET RTC_DESIGN_SHEETS =           /*RTC_DESIGN_SHEETS*/   @rtc_design_sheet
      WHERE RTC_STORE_NAME = 'GetMonthlyNetwin'
  END
END
GO


IF OBJECT_ID('sp_CheckCashDeskDrawConfig', 'P') IS NOT NULL
DROP PROC sp_CheckCashDeskDrawConfig
GO
create PROCEDURE [dbo].[sp_CheckCashDeskDrawConfig]
      -- Add the parameters for the stored procedure here
      @pID INT = 0,
      @pName VARCHAR(20) = ''
AS
BEGIN
SET NOCOUNT ON;

DELETE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CashDesk.Draw.00'
DELETE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration.00'
   
DECLARE @POSTFIJ AS VARCHAR(5)

IF @pID = 0
BEGIN
  SET @POSTFIJ = ''
END
ELSE 
BEGIN 
  SET @POSTFIJ = '.' + RIGHT( '00' + CONVERT(VARCHAR(2),@pID),2)
END


  --'CashDesk.Draw', 'Enabled', 0
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ
                        AND GP_SUBJECT_KEY = 'Enabled'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'Enabled', '0')
END


--'CashDesk.Draw', 'ShowCashDeskDraws', 0
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ
                        AND GP_SUBJECT_KEY = 'ShowCashDeskDraws'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'ShowCashDeskDraws', '0')
END


--'CashDesk.Draw', 'VoucherOnCashDeskDraws.Winner', 0
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'VoucherOnCashDeskDraws.Winner'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'VoucherOnCashDeskDraws.Winner', '0')
END


--'CashDesk.Draw', 'WinnerPrizeVoucherTitle', 'Sorteo en XXXX'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrizeVoucherTitle'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'WinnerPrizeVoucherTitle', 'Sorteo en ' + @pName)
END


--'CashDesk.Draw', 'VoucherOnCashDeskDraws.Loser', 0
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'VoucherOnCashDeskDraws.Loser'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'VoucherOnCashDeskDraws.Loser', '0')
END


--'CashDesk.Draw', 'AskForParticipation', 0
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'AskForParticipation'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'AskForParticipation', '0')
END


--'CashDesk.Draw', 'LoserPrizeVoucherTitle','Cortes�a sorteo en XXXX'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'LoserPrizeVoucherTitle'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'LoserPrizeVoucherTitle', 'Cortes�a sorteo en ' + @pName)
END


--'CashDesk.Draw', 'ActionOnServerDown','0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'ActionOnServerDown'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'ActionOnServerDown', '0')
END


--'CashDesk.Draw', 'LocalServer', '1'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'LocalServer'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'LocalServer', '1')
END


--'CashDesk.Draw', 'ServerBDConnectionString',''
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'ServerBDConnectionString'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'ServerBDConnectionString', '')
END


--'CashDesk.Draw', 'ServerAddress1',''
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'ServerAddress1'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'ServerAddress1', '')
END


--'CashDesk.Draw', 'ServerAddress2',''
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'ServerAddress2'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'ServerAddress2', '')
END


--'CashDesk.DrawConfiguration', 'BallsExtracted','0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'BallsExtracted'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'BallsExtracted', '0')
END


--'CashDesk.DrawConfiguration', 'BallsOfParticipant', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'BallsOfParticipant'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'BallsOfParticipant', '0')
END


--'CashDesk.DrawConfiguration', 'TotalsBallsNumber', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'TotalsBallsNumber'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'TotalsBallsNumber', '0')
END


--'CashDesk.DrawConfiguration', 'LoserPrize1.Fixed', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'LoserPrize1.Fixed'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'LoserPrize1.Fixed', '0')
END


--'CashDesk.DrawConfiguration', 'LoserPrize1.Percentage', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'LoserPrize1.Percentage'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'LoserPrize1.Percentage', '0')
END


--'CashDesk.DrawConfiguration', 'LoserPrize1.Enabled', 0
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'LoserPrize1.Enabled'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'LoserPrize1.Enabled', '0')
END


--'CashDesk.DrawConfiguration', 'NumberOfParticipants', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'NumberOfParticipants'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'NumberOfParticipants', '0')
END


--'CashDesk.DrawConfiguration', 'NumberOfWinners', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'NumberOfWinners'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'NumberOfWinners', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize1.Fixed', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize1.Fixed'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize1.Fixed', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize1.Percentage', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize1.Percentage'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize1.Percentage', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize1.Enabled', 0
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize1.Enabled'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize1.Enabled', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize2.Fixed', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize2.Fixed'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize2.Fixed', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize2.Percentage', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize2.Percentage'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize2.Percentage', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize2.Enabled', 0
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize2.Enabled'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize2.Enabled', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize3.Fixed', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize3.Fixed'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize3.Fixed', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize3.Percentage', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize3.Percentage'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize3.Percentage', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize3.Enabled', 0
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize3.Enabled'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize3.Enabled', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize4.Fixed', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize4.Fixed'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize4.Fixed', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize4.Percentage', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize4.Percentage'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize4.Percentage', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize4.Enabled', 0
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize4.Enabled'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize4.Enabled', '0')
END



--'CashDesk.DrawConfiguration', 'WinnerPrize1.PrizeType', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize1.PrizeType'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize1.PrizeType', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize2.PrizeType', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize2.PrizeType'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize2.PrizeType','0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize3.PrizeType', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize3.PrizeType'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize3.PrizeType', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize4.PrizeType', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize4.PrizeType'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize4.PrizeType', '0')
END


--'CashDesk.DrawConfiguration', 'LoserPrize1.PrizeType', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'LoserPrize1.PrizeType'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'LoserPrize1.PrizeType', '0')
END


-- 'CashDesk.Draw', 'AccountingMode', ''
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'AccountingMode'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'AccountingMode', '0')
END


-- 'CashDesk.DrawConfiguration', 'ParticipationPrice', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'ParticipationPrice'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'ParticipationPrice', '0')
END   
      
      
-- 'CashDesk.Draw', 'IsCashDeskDraw', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'IsCashDeskDraw'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'IsCashDeskDraw', '0')
END         


-- 'CashDesk.Draw', 'ReportUNRFromSP_SalesAndPayment', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'ReportUNRFromSP_SalesAndPayment'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'ReportUNRFromSP_SalesAndPayment', '0')
END   


-- 'CashDesk.Draw', 'AccountingMode.PromoNameMovement', 'PROMOCI�N'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'AccountingMode.PromoNameMovement'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'AccountingMode.PromoNameMovement', 'Promoci�n')
END   

-- 'CashDesk.Draw', 'AccountingMode.KindOfNameMovement', 'Premio en especie'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'AccountingMode.KindOfNameMovement'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'AccountingMode.KindOfNameMovement', 'Premio en especie')
END   

-- 'CashDesk.Draw', 'AccountingMode.RE.KindOfNameMovement', 'Premio en especie (RE)'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'AccountingMode.RE.KindOfNameMovement'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'AccountingMode.RE.KindOfNameMovement', 'Premio en especie (RE)')
END   

-- 'CashDesk.Draw', 'AccountingMode.RE.PromoNameMovement', 'Promoci�n (RE)'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'AccountingMode.RE.PromoNameMovement'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'AccountingMode.RE.PromoNameMovement', 'Promoci�n (RE)')
END   


-- 'CashDesk.Draw', 'Voucher.HideCurrencySymbol', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'Voucher.HideCurrencySymbol'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'Voucher.HideCurrencySymbol', '0')
END   
      
-- 'CashDesk.Draw', 'Voucher.LoserPrizeLabel', 'Cortesia'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'Voucher.LoserPrizeLabel'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'Voucher.LoserPrizeLabel', 'Cortesia')
END   
      
-- 'CashDesk.Draw', 'Voucher.UNRLabel', 'FD'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'Voucher.UNRLabel'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'Voucher.UNRLabel', 'FD')
END   

-- 'CashDesk.Draw', 'Voucher.WinnerPrizeLabel', 'Premio'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'Voucher.WinnerPrizeLabel'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'Voucher.WinnerPrizeLabel', 'Premio')
END   



--/// Probability


IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'LoserPrize1.Probability'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'LoserPrize1.Probability', '0')
END

IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize1.Probability'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize1.Probability', '0')
END

IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize2.Probability'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize2.Probability', '0')
END

IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize3.Probability'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize3.Probability', '0')
END

IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'WinnerPrize4.Probability'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize4.Probability', '0')
END


IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'ParticipationMaxPrice'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'ParticipationMaxPrice', '1')
END



IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameUrl'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameUrl', '')
END



IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameTimeout'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameTimeout', '30')
END

IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameName'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameName', 'Terminal Draw')
END

IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameFirstDrawScreenMessageLine0'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameFirstDrawScreenMessageLine0', '')
END

IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameFirstDrawScreenMessageLine1'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameFirstDrawScreenMessageLine1', 'Pulse [1] para jugar')
END

IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameFirstDrawScreenMessageLine2'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameFirstDrawScreenMessageLine2', '')
END

IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameFirstDrawScreenTimeOut'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameFirstDrawScreenTimeOut', '10')
END


IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'IfTimeOutExpiresParticipateInDraw'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'IfTimeOutExpiresParticipateInDraw', '0')
END

IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'CloseOpenedSessionsBeforeNewGame'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'CloseOpenedSessionsBeforeNewGame', '0')
END
      
END
GO

GRANT EXECUTE ON [dbo].[sp_CheckCashDeskDrawConfig] TO [wggui]
GO

/******* TRIGGERS *******/
