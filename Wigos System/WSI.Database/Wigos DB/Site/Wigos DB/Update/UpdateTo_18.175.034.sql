/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 174;

SET @New_ReleaseId = 175;
SET @New_ScriptName = N'UpdateTo_18.175.034.sql';
SET @New_Description = N'GamingTables stored GT_Chips_Operations and GPs; TITO GP';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/******* TABLES  *******/

IF OBJECT_ID (N'dbo.bonuses', N'U') IS NOT NULL
    DROP TABLE dbo.bonuses;                 
GO  

CREATE TABLE [dbo].[bonuses](
      [bns_bonus_id] [bigint] IDENTITY(1,1) NOT NULL,
      [bns_inserted] [datetime] NOT NULL,
      [bns_to_transfer_re] [money] NOT NULL,
      [bns_to_transfer_promo_re] [money] NOT NULL,
      [bns_to_transfer_promo_nr] [money] NOT NULL,
      [bns_source_type] [int] NOT NULL,
      [bns_source_bigint1] [bigint] NULL,
      [bns_source_bigint2] [bigint] NULL,
      [bns_source_int1] [int] NULL,
      [bns_source_int2] [int] NULL,
      [bns_target_type] [int] NOT NULL,
      [bns_target_terminal_id] [int] NULL,
      [bns_target_account_id] [bigint] NULL,
      [bns_transfer_status] [int] NOT NULL,
      [bns_transfer_status_changed] [datetime] NOT NULL,
      [bns_transfer_status_text] [nvarchar](max) NULL,
      [bns_wcp_cmd_id_award] [bigint] NOT NULL,
      [bns_wcp_cmd_id_status] [bigint] NOT NULL,
      [bns_wcp_cmd_datetime] [datetime] NULL,
      [bns_transferred_datetime] [datetime] NULL,
      [bns_transferred_re] [money] NULL,
      [bns_trasnferred_promo_re] [money] NULL,
      [bns_transferred_promo_nr] [money] NULL,
      [bns_transferred_terminal_id] [int] NULL,
      [bns_transferred_play_session_id] [bigint] NULL,
      [bns_transferred_account_id] [bigint] NULL,
      [bns_sas_host_status] [int] NULL,
CONSTRAINT [PK_bonuses] PRIMARY KEY CLUSTERED 
(
      [bns_bonus_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[bonuses] ADD  CONSTRAINT [DF_bonusing_bns_datetime]  DEFAULT (getdate()) FOR [bns_inserted]
GO
ALTER TABLE [dbo].[bonuses] ADD  CONSTRAINT [DF_bonusing_bns_to_transfer_re]  DEFAULT ((0)) FOR [bns_to_transfer_re]
GO
ALTER TABLE [dbo].[bonuses] ADD  CONSTRAINT [DF_bonusing_bns_to_transfer_promo_re]  DEFAULT ((0)) FOR [bns_to_transfer_promo_re]
GO
ALTER TABLE [dbo].[bonuses] ADD  CONSTRAINT [DF_bonusing_bns_to_transfer_promo_nr]  DEFAULT ((0)) FOR [bns_to_transfer_promo_nr]
GO
ALTER TABLE [dbo].[bonuses] ADD  CONSTRAINT [DF_bonuses_bns_transfer_status]  DEFAULT ((0)) FOR [bns_transfer_status]
GO
ALTER TABLE [dbo].[bonuses] ADD  CONSTRAINT [DF_bonuses_bns_transfer_status_changed]  DEFAULT (getdate()) FOR [bns_transfer_status_changed]
GO

/******* INDEXES  *******/

/******* STORED  *******/

IF OBJECT_ID (N'dbo.GT_Chips_Operations', N'P') IS NOT NULL
    DROP PROCEDURE dbo.GT_Chips_Operations;                 
GO  

/*
----------------------------------------------------------------------------------------------------------------
BASIC REPORT QUERY FOR CHIPS OPERATIONS

Version  Date           User     Description
----------------------------------------------------------------------------------------------------------------
1.0.0    09-JAN-2014    RMS      First Release

Requeriments:
   -- Functions:
         
Parameters:
   -- DATE FROM:      Start date for data collection. (If NULL then use first available date).
   -- DATE TO:        End date for data collection. (If NULL then use current date).
   -- STATUS:         The gaming table status tu filter (enabled or disabled) (-1 to show all).
   -- AREA:           Location Area of the gaming table.
   -- BANK:           Area bank of location.
   -- FLOOR:          Floor of location.
   -- CASHIERS:       Indicates if cashiers without gaming table must be included 
   -- CASHIER GROUP NAME: Sets the name of the gaming table type column for cashiers.
   -- VALID TYPES:    A string that contains the valid table types to list.
*/    

 CREATE PROCEDURE [dbo].[GT_Chips_Operations] 
 ( @pDateFrom DATETIME
  ,@pDateTo DATETIME
  ,@pStatus INTEGER
  ,@pArea VARCHAR(50)
  ,@pBank VARCHAR(50)
  ,@pCashiers INTEGER
  ,@pCashierGroupName VARCHAR(50)
  ,@pValidTypes VARCHAR(4096)
 )
AS
BEGIN

----------------------------------------------------------------------------------------------------------------
DECLARE @_DATE_FROM     AS   DATETIME
DECLARE @_DATE_TO       AS   DATETIME
DECLARE @_STATUS        AS   INTEGER
DECLARE @_AREA          AS   VARCHAR(4096)
DECLARE @_BANK          AS   VARCHAR(4096)
DECLARE @_DELIMITER     AS   CHAR(1)
DECLARE @_CASHIERS      AS   INTEGER
DECLARE @_CASHIERS_NAME AS   VARCHAR(50)
DECLARE @_TYPES_TABLE        TABLE(SST_ID INT, SST_VALUE VARCHAR(50)) -- TO STORE THE VALID GAMING TABLE TYPES 

----------------------------------------------------------------------------------------------------------------

-- Initialzitation --
SET @_DELIMITER   =   ','
SET @_DATE_FROM   =   @pDateFrom
SET @_DATE_TO     =   @pDateTo
SET @_STATUS      =   ISNULL(@pStatus, -1)
SET @_AREA        =   ISNULL(@pArea, '')
SET @_BANK        =   ISNULL(@pBank, '')
SET @_CASHIERS    =   ISNULL(@pCashiers, 1)
SET @_CASHIERS_NAME = ISNULL(@pCashierGroupName, '---')

----------------------------------------------------------------------------------------------------------------
-- CHECK DATE PARAMETERS
IF @_DATE_FROM IS NULL
BEGIN
   -- IF DATE FROM IS NULL, USER FIRST POSIBLE DATE
   SET @_DATE_FROM = CAST('' AS DATETIME)
END
IF @_DATE_TO IS NULL
BEGIN
   -- IF DATE TO IS NULL, QUERY UNTIL CURRENT DATE
   SET @_DATE_TO = CAST(GETDATE() AS DATETIME)
END

-- ASSIGN TYPES PARAMETER INTO TABLE
INSERT INTO @_TYPES_TABLE SELECT * FROM dbo.SplitStringIntoTable(@pValidTypes, @_DELIMITER, DEFAULT)

----------------------------------------------------------------------------------------------------------------
-- MAIN QUERY

    SELECT   GT.GT_NAME AS GT_NAME
           , GTT.GTT_NAME AS GTT_NAME
           , SUM(ISNULL(GTS.GTS_TOTAL_SALES_AMOUNT,0)) AS GTS_TOTAL_SALES_AMOUNT
           , SUM(ISNULL(GTS.GTS_TOTAL_PURCHASE_AMOUNT,0)) AS GTS_TOTAL_PURCHASE_AMOUNT
      INTO   #CHIPS_OPERATIONS_TABLE           
      FROM   GAMING_TABLES GT
 LEFT JOIN   GAMING_TABLES_SESSIONS GTS
        ON   GTS.GTS_GAMING_TABLE_ID = GT.GT_GAMING_TABLE_ID
 LEFT JOIN   CASHIER_SESSIONS CS
        ON   CS.CS_SESSION_ID = GTS.GTS_CASHIER_SESSION_ID
INNER JOIN   GAMING_TABLES_TYPES GTT
        ON   GTT.GTT_GAMING_TABLE_TYPE_ID = GT.GT_TYPE_ID 
     WHERE   GT.GT_ENABLED  = CASE WHEN @_STATUS = -1 THEN GT.GT_ENABLED ELSE @_STATUS END
       AND   GT.GT_AREA_ID  = CASE WHEN @_AREA   = '' THEN GT.GT_AREA_ID ELSE @_AREA END
       AND   GT.GT_BANK_ID  = CASE WHEN @_BANK   = '' THEN GT.GT_BANK_ID ELSE @_BANK END
       AND   GT.GT_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
       AND   (CS.CS_OPENING_DATE >= @_DATE_FROM AND (CS.CS_CLOSING_DATE IS NULL OR CS.CS_CLOSING_DATE <= @_DATE_TO))  
       AND	 GT.GT_HAS_INTEGRATED_CASHIER = 1
  GROUP BY   GT.GT_NAME, GTT.GTT_NAME
  ORDER BY   GTT.GTT_NAME
  
-- Check if cashiers must be visible  
IF @_CASHIERS = 1 
BEGIN

-- Select and join data to show cashiers
SELECT * FROM (
   -- Select default gaming tables
   SELECT * FROM #CHIPS_OPERATIONS_TABLE
   
   UNION ALL
   
   -- Adding cashiers without gaming tables
    SELECT   CT_NAME as GT_NAME
           , @_CASHIERS_NAME as GTT_NAME
           , SUM(CM_SUB_AMOUNT) AS GTS_TOTAL_SALES_AMOUNT
           , SUM(CM_ADD_AMOUNT) AS GTS_TOTAL_PURCHASE_AMOUNT
      FROM   CASHIER_MOVEMENTS
INNER JOIN   CASHIER_TERMINALS
        ON   CM_CASHIER_ID = CT_CASHIER_ID
     WHERE   CM_CASHIER_ID NOT IN (SELECT DISTINCT(GT_CASHIER_ID) FROM GAMING_TABLES)
       AND   CM_TYPE IN (303,304)
       AND   (CM_DATE >= @_DATE_FROM AND CM_DATE <= @_DATE_TO)
  GROUP BY   CM_CASHIER_ID, CT_NAME  

) AS X

END
ELSE
BEGIN
 -- Data without cashiers

 -- Select default gaming tables
 SELECT * FROM #CHIPS_OPERATIONS_TABLE  
  
END
 
-- DROP TEMPORARY TABLE  
DROP TABLE #CHIPS_OPERATIONS_TABLE
  
END  -- END PROCEDURE
GO

-- PERMISSIONS

GRANT EXECUTE ON [dbo].[GT_Chips_Operations] TO [wggui] WITH GRANT OPTION
GO

--
--
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AML_AccountCurrentMonth]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[AML_AccountCurrentMonth]
GO
CREATE PROCEDURE [dbo].[AML_AccountCurrentMonth]
  @AccountId  BIGINT,
  @SplitA     MONEY OUTPUT,
  @Prizes     MONEY OUTPUT
AS
BEGIN
  SET NOCOUNT ON;

  DECLARE @_today as DATETIME
  DECLARE @_day_1 as DATETIME
  DECLARE @_date1  as DATETIME
  DECLARE @_track_data as NVARCHAR(50)
  DECLARE @_track_data_external as NVARCHAR(50)
  DECLARE @_group_by_trackdata as INT
  DECLARE @_level as INT
  DECLARE @_group_by_mode as INT

  SET @SplitA = 0
  SET @Prizes = 0

  SET @_track_data = NULL

  SET @_group_by_trackdata  = 0

  SELECT   @_group_by_mode = CAST (GP_KEY_VALUE AS INT)
    FROM   GENERAL_PARAMS 
   WHERE   GP_GROUP_KEY   = 'AntiMoneyLaundering' 
     AND   GP_SUBJECT_KEY = 'GroupByMode'
     AND   ISNUMERIC(GP_KEY_VALUE) = 1
  SET @_group_by_mode = ISNULL (@_group_by_mode, 0)

  IF @_group_by_mode <> 0 
  BEGIN
    -- Trackdata is needed
    SET @_group_by_trackdata = 1

    SELECT   @_track_data  = AC_TRACK_DATA
           , @_level       = ISNULL(AC_HOLDER_LEVEL, 0)
      FROM   ACCOUNTS
     WHERE   AC_ACCOUNT_ID = @AccountId

    IF @_track_data IS NULL RETURN

    SET @_track_data_external = dbo.TrackDataToExternal(@_track_data)
    IF @_track_data_external = ''
      SET @_track_data_external = @_track_data

    IF @_group_by_mode = 1 AND @_level > 0 SET @_group_by_trackdata = 0  -- Personal account, don't group by 'trackdata'
    IF @_group_by_mode = 2 AND @_level = 0 SET @_group_by_trackdata = 0  -- Anonymous account, don't group by 'trackdata'
  END

  SET @_today = dbo.TodayOpening(0)
  SET @_day_1 = DATEADD (DAY, 1 - DATEPART(DAY, @_today), @_today)

  SET @_date1 = NULL

  IF @_group_by_trackdata = 0 
  BEGIN
    SELECT   @_date1 = MAX(AMD_DAY), @SplitA = SUM(AMD_SPLIT_A), @Prizes = SUM(AMD_PRIZE)
      FROM   AML_DAILY WITH (INDEX (IX_amd_account_day))
     WHERE   AMD_ACCOUNT_ID = @AccountId
       AND   AMD_DAY       >= @_day_1 
  END
  ELSE
  BEGIN
    SELECT   @_date1 = MAX(AMD_DAY), @SplitA = SUM(AMD_SPLIT_A), @Prizes = SUM(AMD_PRIZE)
      FROM   AML_DAILY WITH (INDEX (IX_amd_account_day))
     WHERE   AMD_ACCOUNT_ID = @AccountId
       AND   AMD_DAY       >= @_day_1 
       AND   AMD_TRACK_DATA = @_track_data_external
  END

  IF @_date1 IS NULL 
  BEGIN 
    SET @_date1  = @_day_1
    SET @SplitA = 0
    SET @Prizes = 0
  END
  ELSE
  BEGIN
    SET @_date1  = DATEADD(DAY, 1, @_date1)
    SET @SplitA = ISNULL(@SplitA, 0)
    SET @Prizes = ISNULL(@Prizes, 0)
  END

  IF @_group_by_trackdata = 0 
  BEGIN
    SELECT   @SplitA = @SplitA
                     + isnull(sum(case when (am_type =  1) then am_add_amount else 0 end ), 0)
                     - isnull(sum(case when (am_type in (77, 1000)) then am_sub_amount + am_add_amount else 0 end ), 0),
             @Prizes = @Prizes + isnull(sum(case when (am_type =  2) then am_sub_amount else 0 end ), 0)
      FROM   ACCOUNT_MOVEMENTS WITH (INDEX (IX_MOVEMENTS_ACCOUNT_DATE))
     WHERE   AM_DATETIME >= @_date1
       AND   AM_TYPE IN (1, 2, 77, 1000)
       AND   AM_ACCOUNT_ID = @AccountId
  END
  ELSE
  BEGIN
    SELECT   @SplitA = @SplitA
                     + isnull(sum(case when (am_type =  1) then am_add_amount else 0 end ), 0)
                     - isnull(sum(case when (am_type in (77, 1000)) then am_sub_amount + am_add_amount else 0 end ), 0),
             @Prizes = @Prizes + isnull(sum(case when (am_type =  2) then am_sub_amount else 0 end ), 0)
      FROM   ACCOUNT_MOVEMENTS WITH (INDEX (IX_MOVEMENTS_ACCOUNT_DATE))
     WHERE   AM_DATETIME >= @_date1
       AND   AM_TYPE IN (1, 2, 77, 1000)
       AND   AM_ACCOUNT_ID = @AccountId
       AND   AM_TRACK_DATA = @_track_data_external
  END
END
GO

GRANT EXECUTE ON [dbo].[AML_AccountCurrentMonth] TO [wggui] WITH GRANT OPTION
GO

--
--
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AML_CreateDailyMonthly]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[AML_CreateDailyMonthly]
GO
CREATE PROCEDURE [dbo].[AML_CreateDailyMonthly] 
  @Day DATETIME
AS
BEGIN
  DECLARE @Date1 DATETIME
  DECLARE @Date2 DATETIME
  DECLARE @Count DATETIME

  SET NOCOUNT ON;

  -- Delete any previous row of the given day  
  SET @Date1 = dbo.Opening (0, @Day)
  SET @Date1 = DATEADD (HOUR, -DATEPART(HOUR, @Date1), @Date1)
  SET @Date2 = DATEADD (DAY, 1, @Date1)

  SELECT @Count = COUNT(*) FROM AML_DAILY WHERE AMD_DAY >= @Date1 AND AMD_DAY < @Date2
  IF @Count > 0 
  BEGIN
    DELETE AML_DAILY WHERE AMD_DAY >= @Date1 AND AMD_DAY < @Date2
  END

    -- Generate 'day'
  SET @Date1 = dbo.Opening (0, @Day)
  SET @Date2 = DATEADD (DAY, 1, @Date1)

  INSERT INTO AML_DAILY (AMD_DAY, AMD_ACCOUNT_ID, AMD_TRACK_DATA, AMD_SPLIT_A, AMD_PRIZE)
  (
    SELECT   DATEADD(DAY, DATEDIFF(HOUR, @Date1, AM_DATETIME)/24, @Date1)      
           , AM_ACCOUNT_ID
           , ISNULL(AM_TRACK_DATA, '')                            
           , SUM(CASE WHEN (AM_TYPE =  1) THEN AM_ADD_AMOUNT ELSE 0 END )
           - SUM(CASE WHEN (AM_TYPE IN (77, 1000)) THEN AM_SUB_AMOUNT + AM_ADD_AMOUNT ELSE 0 END )           
           , SUM(CASE WHEN (AM_TYPE =  2) THEN AM_SUB_AMOUNT ELSE 0 END )      
      FROM   ACCOUNT_MOVEMENTS  WITH (INDEX (IX_am_datetime))
     WHERE   AM_DATETIME >= @Date1
       AND   AM_DATETIME <  @Date2
       AND   AM_TYPE IN (1, 2, 77, 1000)
    GROUP BY AM_ACCOUNT_ID
           , ISNULL(AM_TRACK_DATA, '')
           , DATEADD(DAY, DATEDIFF(HOUR, @Date1, AM_DATETIME)/24, @Date1)
  )

  -- First day of the month at '00:00:00'
  SET @Date1 = DATEADD (DAY, 1 - DATEPART(DAY, @Date1), @Date1)
  SET @Date1 = DATEADD (HOUR, -DATEPART(HOUR, @Date1), @Date1)
  SELECT @Count = COUNT(*) FROM AML_MONTHLY WHERE AMM_MONTH >= @Date1 AND AMM_MONTH < DATEADD(MONTH, 1, @Date1)
  IF @Count > 0
  BEGIN
    DELETE AML_MONTHLY WHERE AMM_MONTH >= @Date1 AND AMM_MONTH < DATEADD(MONTH, 1, @Date1)
  END

  -- First day of the month at 'Opening'
  SET @Date1 = dbo.Opening (0, @Day)
  SET @Date1 = DATEADD(DAY, 1 - DATEPART(DAY, @Date1), @Date1)
  INSERT INTO AML_MONTHLY (AMM_MONTH, AMM_ACCOUNT_ID, AMM_TRACK_DATA, AMM_SPLIT_A, AMM_PRIZE)
  (
    SELECT   @Date1
           , AMD_ACCOUNT_ID
           , AMD_TRACK_DATA
           , SUM(AMD_SPLIT_A)
           , SUM(AMD_PRIZE)
      FROM   AML_DAILY WITH (INDEX (PK_aml_daily))
     WHERE   AMD_DAY >= @Date1
       AND   AMD_DAY <  DATEADD(MONTH, 1, @Date1)
    GROUP BY AMD_ACCOUNT_ID, AMD_TRACK_DATA
  )
END
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TITO_SetTicketPendingCancel]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[TITO_SetTicketPendingCancel]
GO

Create PROCEDURE [dbo].[TITO_SetTicketPendingCancel] 
	@pTerminalId                  INT,
	@pTicketValidationNumber      BIGINT,
	@pDefaultAllowRedemption      BIT,
	@pDefaultMaxAllowedTicketIn  	MONEY,
	--- OUTPUT
	@pTicketId						        BIGINT	 OUTPUT,
	@pTicketType					        INT      OUTPUT,
	@pTicketAmount					      MONEY    OUTPUT,
	@pTicketExpiration				    DATETIME OUTPUT

AS
BEGIN
	DECLARE  @_ticket_status_valid			    INT
	DECLARE  @_ticket_status_pending_cancel	INT
	DECLARE  @_min_ticket_id				        BIGINT
	DECLARE  @_max_ticket_id				        BIGINT
	DECLARE	 @_max_ticket_amount			      MONEY
	DECLARE  @_redeem_allowed				        BIT

	SET NOCOUNT ON

	SET @pTicketId = NULL
	SET @pTicketType = NULL
	SET @pTicketAmount = NULL
	SET @pTicketExpiration = NULL

	SELECT   @_redeem_allowed    = ISNULL(TE_ALLOWED_REDEMPTION, @pDefaultAllowRedemption)
		     , @_max_ticket_amount = ISNULL(TE_MAX_ALLOWED_TI,     @pDefaultMaxAllowedTicketIn)
	  FROM   TERMINALS                     
	 WHERE   TE_TERMINAL_ID = @pTerminalId 

	IF   @_max_ticket_amount IS NULL 
	  OR @_redeem_allowed    IS NULL 
	  OR @_redeem_allowed    = 0 
	  RETURN

	SET @_ticket_status_valid          = 0 -- TITO_TICKET_STATUS.VALID
	SET @_ticket_status_pending_cancel = 5 -- TITO_TICKET_STATUS.PENDING_CANCEL

	SELECT   @_min_ticket_id = MIN (TI_TICKET_ID)
		     , @_max_ticket_id = MAX (TI_TICKET_ID)
	  FROM   TICKETS WITH (INDEX (IX_ti_validation_number_status))
	 WHERE   TI_VALIDATION_NUMBER  = @pTicketValidationNumber
	   AND   TI_STATUS             = @_ticket_status_valid
	   AND   TI_AMOUNT  > 0
	   AND ( TI_EXPIRATION_DATETIME IS NULL OR TI_EXPIRATION_DATETIME > GETDATE () )
	   AND ( @_max_ticket_amount = 0 OR TI_AMOUNT <= @_max_ticket_amount )
	   
	IF @_min_ticket_id =  @_max_ticket_id
	BEGIN
		-- One ticket found, change its status
		UPDATE   TICKETS 
		   SET   TI_STATUS                  = @_ticket_status_pending_cancel 
			     , TI_LAST_ACTION_TERMINAL_ID = @pTerminalId
			     , TI_LAST_ACTION_TERMINAL_TYPE = 1  --Always 1 from terminal (WCP)
			     , TI_LAST_ACTION_DATETIME    = GETDATE() 
		 WHERE   TI_TICKET_ID               = @_min_ticket_id 
		   AND   TI_STATUS                  = @_ticket_status_valid
		   AND   TI_AMOUNT  > 0
		   AND ( TI_EXPIRATION_DATETIME IS NULL OR TI_EXPIRATION_DATETIME > GETDATE () )
		   AND ( @_max_ticket_amount = 0 OR TI_AMOUNT <= @_max_ticket_amount )

		 IF @@ROWCOUNT = 1
			SELECT   @pTicketId         = TI_TICKET_ID
				     , @pTicketType       = TI_TYPE_ID
				     , @pTicketAmount     = TI_AMOUNT
				     , @pTicketExpiration = TI_EXPIRATION_DATETIME
			  FROM   TICKETS 
			 WHERE   TI_TICKET_ID  = @_min_ticket_id 

	END

END

GO


/******* RECORDS *******/

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'TITO' AND GP_SUBJECT_KEY ='MassivePromotionsValidation.LastTicket')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key] ,[gp_subject_key],[gp_key_value])
       VALUES
             ('TITO' ,'MassivePromotionsValidation.LastTicket' ,1);
GO

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'TITO' AND GP_SUBJECT_KEY ='MassivePromotions.MaxPromos')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key] ,[gp_subject_key],[gp_key_value])
       VALUES
             ('TITO' ,'MassivePromotions.MaxPromos' ,400);
GO

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'GamingTables' AND GP_SUBJECT_KEY = 'Purchases.AskForSourceTable')
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE)  VALUES ('GamingTables', 'Purchases.AskForSourceTable', 0)
GO

IF NOT EXISTS ( SELECT * FROM GENERAL_PARAMS WHERE gp_group_key = 'Cashier' AND gp_subject_key = 'CashInTax.Enabled')
  INSERT INTO GENERAL_PARAMS (gp_group_key, gp_subject_key, gp_key_value) VALUES ('Cashier', 'CashInTax.Enabled', '0')
GO

IF NOT EXISTS ( SELECT * FROM GENERAL_PARAMS WHERE gp_group_key = 'Cashier' AND gp_subject_key = 'CashInTax.Name')
  INSERT INTO GENERAL_PARAMS (gp_group_key, gp_subject_key, gp_key_value) VALUES ('Cashier', 'CashInTax.Name', 'Impuesto IEJC')
GO

IF NOT EXISTS ( SELECT * FROM GENERAL_PARAMS WHERE gp_group_key = 'Cashier' AND gp_subject_key = 'CashInTax.VoucherText')
  INSERT INTO GENERAL_PARAMS (gp_group_key, gp_subject_key, gp_key_value) VALUES ('Cashier', 'CashInTax.VoucherText', 'IEJC 10%')
GO

IF NOT EXISTS ( SELECT * FROM GENERAL_PARAMS WHERE gp_group_key = 'Cashier' AND gp_subject_key = 'CashInTax.Pct')
  INSERT INTO GENERAL_PARAMS (gp_group_key, gp_subject_key, gp_key_value) VALUES ('Cashier', 'CashInTax.Pct', '10')
GO

IF NOT EXISTS ( SELECT * FROM GENERAL_PARAMS WHERE gp_group_key = 'Cashier' AND gp_subject_key = 'CashInTax.BasePct')
  INSERT INTO GENERAL_PARAMS (gp_group_key, gp_subject_key, gp_key_value) VALUES ('Cashier', 'CashInTax.BasePct', '71.42857142857143')
GO


-- GENERAL PARAMS
IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Alerts' AND GP_SUBJECT_KEY = 'CheckLastHours')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Alerts', 'CheckLastHours', '12')
GO

--
-- Alerts
--

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Alerts' AND GP_SUBJECT_KEY = 'Plays.Activated')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Alerts', 'Plays.Activated', '1')
GO
IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Alerts' AND GP_SUBJECT_KEY = 'Plays.PlaysPerSecond.Value')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Alerts', 'Plays.PlaysPerSecond.Value', '1')
GO


IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Alerts' AND GP_SUBJECT_KEY = 'Plays.CentsPerSecond.Value')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Alerts', 'Plays.CentsPerSecond.Value', '100')
GO

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Alerts' AND GP_SUBJECT_KEY = 'Wonamount.Activated')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Alerts', 'Wonamount.Activated', '1')
GO
IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Alerts' AND GP_SUBJECT_KEY = 'Wonamount.Value')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Alerts', 'Wonamount.Value', '100000')
GO


IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Alerts' AND GP_SUBJECT_KEY = 'TheoreticalPayout.Activated')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Alerts', 'TheoreticalPayout.Activated', '1')
GO
IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Alerts' AND GP_SUBJECT_KEY = 'TheoreticalPayout.RangeBefore')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Alerts', 'TheoreticalPayout.RangeBefore', '10')
GO
IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Alerts' AND GP_SUBJECT_KEY = 'TheoreticalPayout.RangeAfter')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Alerts', 'TheoreticalPayout.RangeAfter', '5')
GO


IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Alerts' AND GP_SUBJECT_KEY = 'Counterfeit.Activated')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Alerts', 'Counterfeit.Activated', '1')
GO
IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Alerts' AND GP_SUBJECT_KEY = 'Counterfeit.QuantityValue')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Alerts', 'Counterfeit.QuantityValue', '10')
GO

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Alerts' AND GP_SUBJECT_KEY = 'Jackpot.Activated')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Alerts', 'Jackpot.Activated', '1')
GO