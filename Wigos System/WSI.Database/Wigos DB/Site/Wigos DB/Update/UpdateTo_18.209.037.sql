/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 208;

SET @New_ReleaseId = 209;
SET @New_ScriptName = N'UpdateTo_18.209.037.sql';
SET @New_Description = N'Added columns in cage_movements; cancellation_user_id, cancellation_datetime ';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/******* TABLES *******/

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[cage_movements]') and name = 'cgm_cancellation_user_id')
		ALTER TABLE dbo.cage_movements ADD cgm_cancellation_user_id int NULL
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[cage_movements]') and name = 'cgm_cancellation_datetime')
		ALTER TABLE dbo.cage_movements ADD cgm_cancellation_datetime datetime NULL
GO

/******* STORED *******/

IF OBJECT_ID (N'dbo.GT_Chips_Operations', N'P') IS NOT NULL
    DROP PROCEDURE dbo.GT_Chips_Operations;                 
GO  

CREATE PROCEDURE [dbo].[GT_Chips_Operations] 
 ( @pDateFrom DATETIME
  ,@pDateTo DATETIME
  ,@pStatus INTEGER
  ,@pArea VARCHAR(50)
  ,@pBank VARCHAR(50)
  ,@pOnlyTables INTEGER
  ,@pCashierGroupName VARCHAR(50)
  ,@pValidTypes VARCHAR(4096)
)
AS
BEGIN

----------------------------------------------------------------------------------------------------------------
DECLARE @_DATE_FROM     AS   DATETIME
DECLARE @_DATE_TO       AS   DATETIME
DECLARE @_STATUS        AS   INTEGER
DECLARE @_AREA          AS   VARCHAR(4096)
DECLARE @_BANK          AS   VARCHAR(4096)
DECLARE @_DELIMITER     AS   CHAR(1)
DECLARE @_ONLY_TABLES      AS   INTEGER
DECLARE @_CASHIERS_NAME AS   VARCHAR(50)
DECLARE @_TYPES_TABLE        TABLE(SST_ID INT, SST_VALUE VARCHAR(50)) -- TO STORE THE VALID GAMING TABLE TYPES 

----------------------------------------------------------------------------------------------------------------

-- Initialzitation --
SET @_DELIMITER   =   ','
SET @_DATE_FROM   =   @pDateFrom
SET @_DATE_TO     =   @pDateTo
SET @_STATUS      =   ISNULL(@pStatus, -1)
SET @_AREA        =   ISNULL(@pArea, '')
SET @_BANK        =   ISNULL(@pBank, '')
SET @_ONLY_TABLES =   ISNULL(@pOnlyTables, 1)

IF ISNULL(@_CASHIERS_NAME,'') = '' BEGIN
      SET @_CASHIERS_NAME  = '---CASHIER---'
END

----------------------------------------------------------------------------------------------------------------
-- CHECK DATE PARAMETERS
IF @_DATE_FROM IS NULL
BEGIN
   -- IF DATE FROM IS NULL, USER FIRST POSIBLE DATE
   SET @_DATE_FROM = CAST('' AS DATETIME)
END
IF @_DATE_TO IS NULL
BEGIN
   -- IF DATE TO IS NULL, QUERY UNTIL CURRENT DATE
   SET @_DATE_TO = CAST(GETDATE() AS DATETIME)
END

-- ASSIGN TYPES PARAMETER INTO TABLE
INSERT INTO @_TYPES_TABLE SELECT * FROM dbo.SplitStringIntoTable(@pValidTypes, @_DELIMITER, DEFAULT)

----------------------------------------------------------------------------------------------------------------
-- MAIN QUERY

    SELECT 0 AS TYPE_SESSION
           , CS_SESSION_ID AS SESSION_ID
           , GT_NAME AS GT_NAME
           , CS_OPENING_DATE AS SESSION_DATE
           , GTT_NAME AS GTT_NAME
           , SUM(CM_SUB_AMOUNT) AS GTS_TOTAL_SALES_AMOUNT
           , SUM(CM_ADD_AMOUNT) AS GTS_TOTAL_PURCHASE_AMOUNT
           
      INTO   #CHIPS_OPERATIONS_TABLE           
      FROM   CASHIER_MOVEMENTS
         INNER JOIN   CASHIER_SESSIONS       ON CS_SESSION_ID            = CM_SESSION_ID
         INNER JOIN   CASHIER_TERMINALS      ON CM_CASHIER_ID            = CT_CASHIER_ID
              LEFT JOIN   GAMING_TABLES_SESSIONS ON GTS_CASHIER_SESSION_ID   = CM_SESSION_ID
         INNER JOIN   GAMING_TABLES GT       ON GTS_GAMING_TABLE_ID      = GT_GAMING_TABLE_ID
         INNER JOIN   GAMING_TABLES_TYPES    ON GTT_GAMING_TABLE_TYPE_ID = GT_TYPE_ID 
     WHERE   GT_ENABLED  = CASE WHEN @_STATUS = -1 THEN GT_ENABLED ELSE @_STATUS END
       AND   GT_AREA_ID  = CASE WHEN @_AREA   = '' THEN GT_AREA_ID ELSE @_AREA END
       AND   GT_BANK_ID  = CASE WHEN @_BANK   = '' THEN GT_BANK_ID ELSE @_BANK END
       AND   GT_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
       AND   CM_DATE >= @_DATE_FROM
       AND   CM_DATE <  @_DATE_TO
       AND  GT_HAS_INTEGRATED_CASHIER = 1
       AND   CM_TYPE IN (303, 304)  -- 303 = CHIPS_SALE_TOTAL; 304 = CHIPS_PURCHASE_TOTAL
  GROUP BY   GT_NAME, GTT_NAME, CS_SESSION_ID, CS_OPENING_DATE
  ORDER BY   GTT_NAME
  
-- Check if cashiers must be visible  
IF @_ONLY_TABLES = 0
BEGIN

-- Select and join data to show cashiers
   -- Adding cashiers without gaming tables

  INSERT INTO #CHIPS_OPERATIONS_TABLE
       SELECT 1 AS TYPE_SESSION
              , CM_SESSION_ID AS SESSION_ID
              , CT_NAME as GT_NAME
              , CS_OPENING_DATE AS SESSION_DATE
              , @_CASHIERS_NAME as GTT_NAME
              , SUM(CM_SUB_AMOUNT) AS GTS_TOTAL_SALES_AMOUNT
              , SUM(CM_ADD_AMOUNT) AS GTS_TOTAL_PURCHASE_AMOUNT
         FROM   CASHIER_MOVEMENTS
   INNER JOIN   CASHIER_SESSIONS       ON CS_SESSION_ID          = CM_SESSION_ID
   INNER JOIN   CASHIER_TERMINALS      ON CM_CASHIER_ID          = CT_CASHIER_ID
    LEFT JOIN   GAMING_TABLES_SESSIONS ON GTS_CASHIER_SESSION_ID = CM_SESSION_ID
        WHERE   GTS_CASHIER_SESSION_ID IS NULL -- We want the chips operations that are from cashiers that are not gambling tables
          AND   CM_TYPE IN (303, 304)  -- 303 = CHIPS_SALE_TOTAL; 304 = CHIPS_PURCHASE_TOTAL
          AND   CM_DATE >= @_DATE_FROM
          AND   CM_DATE <  @_DATE_TO
     GROUP BY   CT_NAME, CM_SESSION_ID, CS_OPENING_DATE

END
-- Select results
SELECT * FROM #CHIPS_OPERATIONS_TABLE ORDER BY GTT_NAME,GT_NAME
  
-- DROP TEMPORARY TABLE  
DROP TABLE #CHIPS_OPERATIONS_TABLE
  
END  -- END PROCEDURE
GO

-- PERMISSIONS

GRANT EXECUTE ON [dbo].[GT_Chips_Operations] TO [wggui] WITH GRANT OPTION
GO
