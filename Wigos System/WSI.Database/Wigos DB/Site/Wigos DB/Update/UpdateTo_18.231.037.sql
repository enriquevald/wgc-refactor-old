/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 230;

SET @New_ReleaseId = 231;
SET @New_ScriptName = N'UpdateTo_18.231.037.sql';
SET @New_Description = N'Changes for cards';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/******* TABLES *******/

IF EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[bank_transactions]') and name = 'bt_card_track_data')
BEGIN 
      ALTER TABLE BANK_TRANSACTIONS ALTER COLUMN BT_CARD_TRACK_DATA VARCHAR(256)
END
GO

CREATE TABLE [dbo].[cards](
      [ca_trackdata] [char](20) NOT NULL,
      [ca_linked_type] [int] NOT NULL,
      [ca_linked_id] [bigint] NULL,
      [ca_block_reason] [int] NOT NULL,
      [ca_block_datetime] [datetime] NULL,
      [ca_pin_flags] [int] NOT NULL,
      [ca_pin] [char](20) NULL,
      [ca_pin_changed] [datetime] NULL,
      [ca_pin_expires] [datetime] NULL,
      [ca_pin_errors] [int] NOT NULL,
      [ca_payment_datetime] [datetime] NULL,
      [ca_total_paid] [money] NULL,
      [ca_refundable_deposit] [money] NULL,
CONSTRAINT [PK_cards] PRIMARY KEY CLUSTERED 
(
      [ca_trackdata] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING ON
GO

ALTER TABLE [dbo].[cards] ADD  CONSTRAINT [DF_cards_ca_block_reason]  DEFAULT ((0)) FOR [ca_block_reason]
GO

ALTER TABLE [dbo].[cards] ADD  CONSTRAINT [DF_cards_ca_pin_flags]  DEFAULT ((0)) FOR [ca_pin_flags]
GO

ALTER TABLE [dbo].[cards] ADD  CONSTRAINT [DF_cards_ca_pin_errors]  DEFAULT ((0)) FOR [ca_pin_errors]
GO

ALTER TABLE [dbo].[cards] ADD  CONSTRAINT [DF_cards_ca_refundable_deposit]  DEFAULT ((0)) FOR [ca_refundable_deposit]
GO

IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[cards]') AND name = N'IX_ca_linked_type_id_trackdata')
  CREATE NONCLUSTERED INDEX [IX_ca_linked_type_id_trackdata] ON [dbo].[cards] 
  (
        [ca_linked_type] ASC,
        [ca_linked_id] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LinkCard]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[LinkCard]
GO
CREATE PROCEDURE [dbo].[LinkCard]
    @pExternalTrackData as CHAR(20)
  , @pLinkedType        as INT    = NULL
  , @pLinkedId          as BIGINT = NULL
AS
BEGIN
  -- SET NOCOUNT ON added to prevent extra result sets from
  -- interfering with SELECT statements.
  SET NOCOUNT ON;

  --
  -- Unlink the 'old card' from the linked 'user'
  --    
  IF @pLinkedType IS NOT NULL
    DELETE cards WHERE ca_linked_type = @pLinkedType AND ca_linked_id = @pLinkedId

  IF @pExternalTrackData IS NULL OR LEN(@pExternalTrackData) <= 0 RETURN 

  --
  -- Unlink the 'card' from any other 'user'
  --    
  DELETE cards WHERE ca_trackdata = @pExternalTrackData

  IF @pLinkedType   IS NULL OR @pLinkedId  IS NULL RETURN
                
  INSERT INTO   CARDS 
              ( CA_TRACKDATA, CA_LINKED_TYPE, CA_LINKED_ID )
       VALUES ( @pExternalTrackData, @pLinkedType, @pLinkedId )
END
GO

GRANT EXECUTE ON [dbo].[LinkCard] TO [wggui] WITH GRANT OPTION
GO

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[TRIGGER_MB]'))
DROP TRIGGER [dbo].[TRIGGER_MB]
GO

CREATE TRIGGER [dbo].[TRIGGER_MB] 
   ON  [dbo].[mobile_banks] 
   AFTER INSERT,DELETE,UPDATE
AS 
BEGIN
      -- SET NOCOUNT ON added to prevent extra result sets from
      -- interfering with SELECT statements.
      SET NOCOUNT ON;
      
      IF NOT UPDATE (MB_TRACK_DATA) RETURN
        
    declare @_int as NVARCHAR(50)
    declare @_ext as CHAR(20)
    declare @_id  as BIGINT
    
    DECLARE _CURSOR CURSOR FOR SELECT MB_ACCOUNT_ID, MB_TRACK_DATA FROM DELETED 

    OPEN    _CURSOR
      FETCH NEXT FROM _CURSOR INTO @_id, @_int
    
    WHILE @@FETCH_STATUS = 0
    BEGIN
        IF ISNUMERIC (@_int) = 1
      BEGIN
          SET  @_ext = dbo.TrackDataToExternal (@_int)
            EXEC dbo.LinkCard @_ext, NULL, NULL
      END
        FETCH NEXT FROM _CURSOR INTO @_id, @_int
    END
    
      CLOSE      _CURSOR
      DEALLOCATE _CURSOR
      
      DECLARE _CURSOR CURSOR FOR SELECT MB_ACCOUNT_ID, MB_TRACK_DATA FROM INSERTED 

    OPEN    _CURSOR
      FETCH NEXT FROM _CURSOR INTO @_id, @_int
    
    WHILE @@FETCH_STATUS = 0
    BEGIN
        IF ISNUMERIC (@_int) = 1
      BEGIN
          SET  @_ext = dbo.TrackDataToExternal (@_int)
            EXEC dbo.LinkCard @_ext, 2, @_id
      END
        FETCH NEXT FROM _CURSOR INTO @_id, @_int
    END
    
      CLOSE      _CURSOR
      DEALLOCATE _CURSOR
END
GO

UPDATE MOBILE_BANKS SET MB_TRACK_DATA = MB_TRACK_DATA 
GO

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[TRIGGER_MB]'))
  DROP TRIGGER [dbo].[TRIGGER_MB]
GO

ALTER TABLE dbo.gui_users
DROP COLUMN gu_card_technician, gu_card_change_stacker
GO

/******* INDEXES *******/

/******* RECORDS *******/

/******* STORED PROCEDURES *******/
