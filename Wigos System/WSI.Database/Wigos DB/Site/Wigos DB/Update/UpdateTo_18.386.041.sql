/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 385;

SET @New_ReleaseId = 386;
SET @New_ScriptName = N'UpdateTo_18.386.041.sql';
SET @New_Description = N'CASH DESK CONFIG,STATIC WEB HOST, GT_Cashier_Movements_And_Summary_By_Session'; 

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/**** GENERAL PARAM *****/

/******* TABLES  *******/

/******* RECORDS *******/

/******* PROCEDURES *******/

IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'zsp_CardInfo')
       DROP PROCEDURE [zsp_CardInfo]
GO

create PROCEDURE [dbo].[zsp_CardInfo]
    @TrackData       varchar(24),
@CardIndex  int output,
@_account_id bigint output,
@CustumerId varchar(24)output,  
@AccountType int output,
@CardStatus int output,
@Owner      nvarchar(250) output

WITH EXECUTE AS OWNER
AS

BEGIN
                  
BEGIN TRANSACTION

      
      SET @CardIndex = 1
      set @CustumerId = 1
      set @AccountType = 1
      set @CardStatus =1
      
      IF @TrackData IS NOT NULL  
        BEGIN
         SELECT  @_account_id = AC_ACCOUNT_ID , @Owner = ac_holder_name
                                      FROM   ACCOUNTS
                                     WHERE   AC_TRACK_DATA = dbo.TrackDataToInternal(@TrackData)
        END
        
        
        IF @_account_id > 0
              begin
                  SET @CardIndex = 1
                  set @CustumerId = @_account_id
                  set @AccountType = 1
                  set @AccountType = 1
                  
                  set @Owner = (select ac_holder_name from accounts where ac_account_id=@_account_id)
                  if @Owner is null set @AccountType=0
              END
            Else

            begin
                  SET @CardIndex = 0
                  set @CustumerId = 0
                  set @AccountType = 0
                  set @CardStatus =0
            end
      
      commit transaction 
     
end --Zsp_GetCardInfo

GO

IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'S2S_CardInfo')
       DROP PROCEDURE [S2S_CardInfo]
GO

create PROCEDURE [dbo].[S2S_CardInfo]
      @TrackData       varchar(24)
WITH EXECUTE AS OWNER
AS
BEGIN

declare     @CardIndex                         int ,
                  @CustumerId                 varchar(24) ,
                  @AccountType                       int ,
                  @CardStatus                        int

  DECLARE @status_code  int
  DECLARE @status_text  varchar (254)
  DECLARE @error_text   nvarchar (MAX)
  DECLARE @input        nvarchar(MAX)
  DECLARE @output       nvarchar(MAX)
  DECLARE @_try         int
  DECLARE @_max_tries   int
  DECLARE @_exception   bit
  DECLARE @_completed   bit
  DECLARE @_account_id BIGINT
  declare @Owner        varchar(250)
  SET @_try       = 0
  SET @_max_tries = 6        -- AJQ 22-DES-2014, The number of retries has been incremented to 6
  SET @_completed = 0
print 'good'
  BEGIN TRANSACTION

  WHILE (@_completed = 0)
  BEGIN

    SET @_exception   = 0
    SET @status_code  = 4
    SET @status_text  = 'Access Denied'
    SET @error_text   = ''

    BEGIN TRY


      
      EXECUTE dbo.[zsp_CardInfo] @TrackData ,  
                                            @CardIndex     output ,
                                            @_account_id output ,
                                            @CustumerId  output ,
                                            @AccountType output ,
                                            @CardStatus    output ,
                                            @Owner   output 
      
      SET @_completed = 1          

    END TRY

    BEGIN CATCH
    
      ROLLBACK TRANSACTION

  
      IF (@_try >= @_max_tries) 
      BEGIN
        SET @_completed  = 1;
        SET @_exception  = 1;
        SET @status_code = 4;
        SET @status_text = 'Access Denied';
        SET @error_text  = ' ERROR_NUMBER: '    + CAST(ERROR_NUMBER()               AS NVARCHAR)
                         + ' ERROR_SEVERITY: '  + CAST(ERROR_SEVERITY()             AS NVARCHAR)
                         + ' ERROR_STATE: '     + CAST(ERROR_STATE()                AS NVARCHAR)
                         + ' ERROR_PROCEDURE: ' + CAST(ISNULL(ERROR_PROCEDURE(),'') AS NVARCHAR)
                         + ' ERROR_LINE: '      + CAST(ERROR_LINE()                 AS NVARCHAR)
                         + ' ERROR_MESSAGE: '   + CAST(ERROR_MESSAGE()              AS NVARCHAR(MAX))
      END
      ELSE
      BEGIN
        WAITFOR DELAY '00:00:02'
      END

      BEGIN TRANSACTION

    END CATCH
  END

  SET @input = '@TrackData='       + @TrackData

  IF @error_text <> ''
    SET @error_text = ';Details='      + @error_text

  SET @output = 'CardIndex='      + CAST( @CardIndex as nvarchar)
              + ';CustumerId='     +     @CustumerId 
              + ';AccountType='    +     cast(@AccountType as nvarchar)
              + ';CardStatus='     +     cast(@CardStatus as nvarchar)




              + @error_text
              + '; TryIndex='      + CAST (@_try AS NVARCHAR)


  COMMIT TRANSACTION

  SELECT @CardIndex     as                   CardIndex      ,
            @CustumerId      as             CustomerId  ,
            @AccountType     as             AccountType,
            @CardStatus      as             CardStatus
END 

go


GRANT EXECUTE ON [dbo].[zsp_CardInfo] TO [3GS] WITH GRANT OPTION 
GO
GRANT EXECUTE ON [dbo].[S2S_CardInfo] TO [3GS] WITH GRANT OPTION 
GO

-- [EIBE]
GRANT EXECUTE ON [dbo].[zsp_CardInfo] TO [EIBE] WITH GRANT OPTION 
GO
GRANT EXECUTE ON [dbo].[S2S_CardInfo] TO [EIBE] WITH GRANT OPTION 
GO

-- [wg_interface]
GRANT EXECUTE ON [dbo].[zsp_CardInfo] TO [wg_interface] WITH GRANT OPTION 
GO
GRANT EXECUTE ON [dbo].[S2S_CardInfo] TO [wg_interface] WITH GRANT OPTION 
GO
/******* TRIGGERS *******/
