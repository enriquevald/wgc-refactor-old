/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 204;

SET @New_ReleaseId = 205;
SET @New_ScriptName = N'UpdateTo_18.205.037.sql';
SET @New_Description = N'GP Alarms..Amount and Qty update to -1';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/******* TABLES  *******/

/******* INDEXES *******/

/******* RECORDS *******/

UPDATE   GENERAL_PARAMS 
   SET   GP_KEY_VALUE   = '-1' 
 WHERE   GP_GROUP_KEY   = 'Alarms' 
   AND   GP_SUBJECT_KEY = 'Played.Limit.Amount'  
   AND   GP_KEY_VALUE   = '0'
GO 
        
UPDATE   GENERAL_PARAMS 
   SET   GP_KEY_VALUE   = '-1' 
 WHERE   GP_GROUP_KEY   = 'Alarms' 
   AND   GP_SUBJECT_KEY = 'Played.Limit.Quantity' 
   AND   GP_KEY_VALUE   = '0'
GO 

UPDATE   GENERAL_PARAMS 
   SET   GP_KEY_VALUE   = '-1' 
 WHERE   GP_GROUP_KEY   = 'Alarms' 
   AND   GP_SUBJECT_KEY = 'Won.Limit.Amount' 
   AND   GP_KEY_VALUE   = '0'
GO 
        
UPDATE   GENERAL_PARAMS 
   SET   GP_KEY_VALUE   = '-1'
 WHERE   GP_GROUP_KEY   = 'Alarms' 
   AND   GP_SUBJECT_KEY = 'Won.Limit.Quantity' 
   AND   GP_KEY_VALUE   = '0'
GO 

UPDATE   GENERAL_PARAMS 
   SET   GP_KEY_VALUE   = '-1'
 WHERE   GP_GROUP_KEY   = 'Alarms' 
   AND   GP_SUBJECT_KEY = 'Counterfeit.Limit.Quantity' 
   AND   GP_KEY_VALUE   = '0'
GO 
        
UPDATE   GENERAL_PARAMS 
   SET   GP_KEY_VALUE   = '-1'
 WHERE   GP_GROUP_KEY   = 'Alarms' 
   AND   GP_SUBJECT_KEY = 'Jackpot.Limit.Amount' 
   AND   GP_KEY_VALUE   = '0'
GO 
        
UPDATE   GENERAL_PARAMS 
   SET   GP_KEY_VALUE   = '-1'
 WHERE   GP_GROUP_KEY   = 'Alarms' 
   AND   GP_SUBJECT_KEY = 'Jackpot.Limit.Quantity' 
   AND   GP_KEY_VALUE   = '0'
GO 
                        
UPDATE   GENERAL_PARAMS 
   SET   GP_KEY_VALUE   = '-1'
 WHERE   GP_GROUP_KEY   = 'Alarms' 
   AND   GP_SUBJECT_KEY = 'CanceledCredit.Limit.Amount' 
   AND   GP_KEY_VALUE   = '0'
GO 
        
UPDATE   GENERAL_PARAMS 
   SET   GP_KEY_VALUE   = '-1'
 WHERE   GP_GROUP_KEY   = 'Alarms' 
   AND   GP_SUBJECT_KEY = 'CanceledCredit.Limit.Quantity' 
   AND   GP_KEY_VALUE   = '0'
GO 
