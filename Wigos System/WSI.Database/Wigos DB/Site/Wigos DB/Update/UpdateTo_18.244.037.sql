/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 243;

SET @New_ReleaseId = 244;
SET @New_ScriptName = N'UpdateTo_18.244.037.sql';
SET @New_Description = N'New index IX_cmd_movement_id for cage_movement_details; Update SP for multiple account block reason; Update SP CageGetGlobalReportData.';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/******* TABLES *******/

/******* INDEXES *******/

IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[cage_movement_details]') AND name = N'IX_cmd_movement_id')
  CREATE NONCLUSTERED INDEX [IX_cmd_movement_id] ON [dbo].[cage_movement_details] 
  (
        [cmd_movement_id] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

/******* RECORDS *******/

IF NOT EXISTS ( SELECT 1 FROM   GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY = 'Tax.OnPrizeInKind.1.Name' )
      INSERT INTO   GENERAL_PARAMS ( GP_GROUP_KEY, GP_SUBJECT_KEY           , GP_KEY_VALUE )
                               SELECT   'Cashier','Tax.OnPrizeInKind.1.Name', GP_KEY_VALUE 
                                 FROM   GENERAL_PARAMS 
                                WHERE   GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY = 'Tax.OnPrize.1.Name'
GO

IF NOT EXISTS ( SELECT 1 FROM   GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY = 'Tax.OnPrizeInKind.2.Name' )
      INSERT INTO   GENERAL_PARAMS ( GP_GROUP_KEY, GP_SUBJECT_KEY           , GP_KEY_VALUE )
                               SELECT   'Cashier','Tax.OnPrizeInKind.2.Name', GP_KEY_VALUE 
                                 FROM   GENERAL_PARAMS
                                WHERE   GP_GROUP_KEY   = 'Cashier' AND   GP_SUBJECT_KEY = 'Tax.OnPrize.2.Name'
GO

IF NOT EXISTS ( SELECT 1 FROM   GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY = 'Tax.OnPrizeInKind.SetNegative' )
      INSERT INTO   GENERAL_PARAMS ( GP_GROUP_KEY, GP_SUBJECT_KEY           , GP_KEY_VALUE )
                            values ( 'Cashier','Tax.OnPrizeInKind.SetNegative', '0')
GO


IF EXISTS ( SELECT * FROM general_params WHERE gp_group_key = 'Cage' AND gp_subject_key = 'Terminal.DefaultCageSession')
  DELETE GENERAL_PARAMS WHERE gp_group_key = 'Cage' AND gp_subject_key = 'Terminal.DefaultCageSession'

IF NOT EXISTS (SELECT * FROM lcd_functionalities WHERE fun_function_id = 6)
  INSERT INTO lcd_functionalities (fun_function_id, fun_name, fun_enabled) VALUES ( 6, 'EXCHANGE CREDITS', '0')

IF NOT EXISTS (SELECT * FROM lcd_functionalities WHERE fun_function_id = 7)
  INSERT INTO lcd_functionalities (fun_function_id, fun_name, fun_enabled) VALUES ( 7, 'SHOW JACKPOT AWARDED', '0')

GO

--
-- Fix CageCount movements for Cage Report
--
DECLARE @_id         BIGINT
DECLARE @_related_id BIGINT
DECLARE @_denom      MONEY
DECLARE @_isocode    NVARCHAR(3)
DECLARE @_qty        INT

DECLARE db_cursor CURSOR FOR 
          SELECT
             CMD_CAGE_MOVEMENT_DETAIL_ID,
             CGM_RELATED_MOVEMENT_ID,
             CMD_ISO_CODE,
             CMD_DENOMINATION,
             CMD_QUANTITY
        FROM CAGE_MOVEMENT_DETAILS
  INNER JOIN CAGE_MOVEMENTS
          ON CMD_MOVEMENT_ID = CGM_MOVEMENT_ID
       WHERE CGM_TYPE IN (99, 199)
        
OPEN db_cursor

FETCH NEXT FROM db_cursor INTO @_id, @_related_id, @_isocode, @_denom, @_qty

WHILE @@FETCH_STATUS = 0
BEGIN
  IF @_qty < 0 
  BEGIN
    IF NOT EXISTS (SELECT CGM_MOVEMENT_ID
                     FROM CAGE_MOVEMENT_DETAILS
               INNER JOIN CAGE_MOVEMENTS
                       ON CMD_MOVEMENT_ID = CGM_MOVEMENT_ID
                    WHERE CGM_TYPE = 3
                      AND CMD_QUANTITY = @_qty
                      AND CMD_ISO_CODE = @_isocode
                      AND CMD_MOVEMENT_ID = @_related_id) 
                      
      DELETE CAGE_MOVEMENT_DETAILS WHERE CMD_CAGE_MOVEMENT_DETAIL_ID = @_id      
  END 
  ELSE 
  BEGIN
    IF NOT EXISTS (SELECT CGM_MOVEMENT_ID
                     FROM CAGE_MOVEMENT_DETAILS
               INNER JOIN CAGE_MOVEMENTS
                       ON CMD_MOVEMENT_ID = CGM_MOVEMENT_ID
                    WHERE CGM_TYPE = 3
                      AND CMD_DENOMINATION = @_denom
                      AND CMD_QUANTITY >= 0
                      AND CMD_ISO_CODE = @_isocode
                      AND CMD_MOVEMENT_ID = @_related_id) 
                      
    DELETE CAGE_MOVEMENT_DETAILS WHERE CMD_CAGE_MOVEMENT_DETAIL_ID = @_id
  END

  FETCH NEXT FROM db_cursor INTO @_id, @_related_id, @_isocode, @_denom, @_qty
END

CLOSE db_cursor;
DEALLOCATE db_cursor;

GO

/******* STORED PROCEDURES *******/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[BlockReason_ToNewEnumerate]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
  DROP FUNCTION [dbo].[BlockReason_ToNewEnumerate]
GO
CREATE FUNCTION BlockReason_ToNewEnumerate
(@pBlockReason INT) returns INT
AS
BEGIN
      DECLARE @Result INT
      
    IF @pBlockReason > 0 AND @pBlockReason < 256
              SET @Result  = POWER(2, @pBlockReason + 7) -- 2^(AC_BLOCK_REASON-1) x 256
    ELSE 
              SET @Result = @pBlockReason
              
      RETURN @Result
END -- BlockReason_ToNewEnumerate
GO

GRANT EXECUTE ON [dbo].[BlockReason_ToNewEnumerate] TO [wggui] WITH GRANT OPTION 
GO

--
-- Update_PersonalInfo
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Update_PersonalInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Update_PersonalInfo]
GO
CREATE PROCEDURE [dbo].[Update_PersonalInfo]
  @pAccountId                  bigint
, @pTrackData                  nvarchar(50)
,	@pHolderName                 nvarchar(200)
,	@pHolderId                   nvarchar(20)
, @pHolderIdType               int
, @pHolderAddress01            nvarchar(50)
, @pHolderAddress02            nvarchar(50)
, @pHolderAddress03            nvarchar(50)
, @pHolderCity                 nvarchar(50)
, @pHolderZip                  nvarchar(10) 
, @pHolderEmail01              nvarchar(50)
,	@pHolderEmail02              nvarchar(50)
,	@pHolderTwitter              nvarchar(50)
,	@pHolderPhoneNumber01        nvarchar(20)
, @pHolderPhoneNumber02        nvarchar(20)
, @pHolderComments             nvarchar(100)
, @pHolderId1                  nvarchar(20)
, @pHolderId2                  nvarchar(20)
, @pHolderDocumentId1          bigint
, @pHolderDocumentId2          bigint
, @pHolderName1                nvarchar(50)
,	@pHolderName2                nvarchar(50)
,	@pHolderName3                nvarchar(50)
, @pHolderGender               int
, @pHolderMaritalStatus        int
, @pHolderBirthDate            datetime
, @pHolderWeddingDate          datetime
, @pHolderLevel                int
, @pHolderLevelNotify          int
, @pHolderLevelEntered         datetime
,	@pHolderLevelExpiration      datetime
,	@pPin                        nvarchar(12)
, @pPinFailures                int
, @pPinLastModified            datetime
, @pBlocked                    bit
, @pActivated                  bit
, @pBlockReason                int
, @pHolderIsVip                int
, @pHolderTitle                nvarchar(15)                       
, @pHolderName4                nvarchar(50)                       
, @pHolderPhoneType01          int
, @pHolderPhoneType02          int
, @pHolderState                nvarchar(50)                    
, @pHolderCountry              nvarchar(50)                
, @pPersonalInfoSequenceId     bigint                     
, @pUserType                   int
, @pPointsStatus               int
, @pDeposit                    money
, @pCardPay                    bit
, @pBlockDescription           nvarchar(256) 
, @pMSHash                     varbinary(20) 
, @pMSCreatedOnSiteId          int
, @pCreated                    datetime
, @pExternalReference          nvarchar(50) 
, @pHolderOccupation           nvarchar(50) 	
, @pHolderExtNum               nvarchar(10) 
, @pHolderNationality          Int 
, @pHolderBirthCountry         Int 
, @pHolderFedEntity            Int 
, @pHolderId1Type              Int -- RFC
, @pHolderId2Type              Int -- CURP
, @pHolderId3Type              nvarchar(50) 
, @pHolderId3                  nvarchar(20) 
, @pHolderHasBeneficiary       bit  
, @pBeneficiaryName            nvarchar(200) 
, @pBeneficiaryName1           nvarchar(50) 
, @pBeneficiaryName2           nvarchar(50) 
, @pBeneficiaryName3           nvarchar(50) 
, @pBeneficiaryBirthDate       Datetime 
, @pBeneficiaryGender          int 
, @pBeneficiaryOccupation      nvarchar(50) 
, @pBeneficiaryId1Type         int   -- RFC
, @pBeneficiaryId1             nvarchar(20) 
, @pBeneficiaryId2Type         int   -- CURP
, @pBeneficiaryId2             nvarchar(20) 
, @pBeneficiaryId3Type         nvarchar(50) 
, @pBeneficiaryId3             nvarchar(20)
, @pHolderOccupationId         int
, @pBeneficiaryOccupationId    int
AS
BEGIN

DECLARE @pOtherAccountId as BIGINT
DECLARE @UserTypeSiteAccount as BIT

SELECT
       @pOtherAccountId = AC_ACCOUNT_ID 
      ,@UserTypeSiteAccount = AC_USER_TYPE
FROM ACCOUNTS WHERE AC_TRACK_DATA = @pTrackData

SET @pOtherAccountId = ISNULL(@pOtherAccountId, 0)
SET @UserTypeSiteAccount = ISNULL(@UserTypeSiteAccount, 1)

SET @pUserType = ISNULL(@pUserType,CASE WHEN ISNULL(@pHolderLevel,0) > 0  THEN 1 ELSE 0 END)

IF @pOtherAccountId <> 0 AND @pOtherAccountId <> @pAccountId
	   IF (ISNULL((SELECT gp_key_value FROM general_params WHERE gp_group_key = 'PlayerTracking.ExternalLoyaltyProgram' AND gp_subject_key = 'Mode'),0) = 1)
			AND @UserTypeSiteAccount = 0 -- IS ANONYMOUS
       BEGIN
			SET @pTrackData = '-RECYCLED-DUP-' + CAST (NEWID() AS NVARCHAR(50))
       END
       ELSE 
       BEGIN -- NOT IS ANONYMOUS OR ELP IS NOT ENABLED
			  UPDATE   ACCOUNTS   
				 SET   AC_TRACK_DATA =  '-RECYCLED-DUP-' + CAST (NEWID() AS NVARCHAR(50))
			   WHERE   AC_ACCOUNT_ID = @pOtherAccountId
       END


IF NOT EXISTS (SELECT 1 FROM ACCOUNTS WHERE ac_account_id = @pAccountId )  
BEGIN
  INSERT INTO   ACCOUNTS 
              ( AC_ACCOUNT_ID, AC_TYPE, AC_BLOCKED, AC_TRACK_DATA) 
       VALUES ( @pAccountId,         2,  @pBlocked, @pTrackData )
END  

 
  UPDATE   ACCOUNTS                   
     SET   AC_TRACK_DATA                = @pTrackData 
         , AC_HOLDER_NAME               = @pHolderName 
         , AC_HOLDER_ID                 = @pHolderId 
         , AC_HOLDER_ID_TYPE            = @pHolderIdType 
         , AC_HOLDER_ADDRESS_01         = @pHolderAddress01 
         , AC_HOLDER_ADDRESS_02         = @pHolderAddress02 
         , AC_HOLDER_ADDRESS_03         = @pHolderAddress03 
         , AC_HOLDER_CITY               = @pHolderCity 
         , AC_HOLDER_ZIP                = @pHolderZip  
         , AC_HOLDER_EMAIL_01           = @pHolderEmail01 
         , AC_HOLDER_EMAIL_02           = @pHolderEmail02 
         , AC_HOLDER_TWITTER_ACCOUNT    = @pHolderTwitter 
         , AC_HOLDER_PHONE_NUMBER_01    = @pHolderPhoneNumber01 
         , AC_HOLDER_PHONE_NUMBER_02    = @pHolderPhoneNumber02 
         , AC_HOLDER_COMMENTS           = @pHolderComments 
         , AC_HOLDER_ID1                = @pHolderId1 
         , AC_HOLDER_ID2                = @pHolderId2 
         , AC_HOLDER_DOCUMENT_ID1       = @pHolderDocumentId1 
         , AC_HOLDER_DOCUMENT_ID2       = @pHolderDocumentId2 
         , AC_HOLDER_NAME1              = @pHolderName1 
         , AC_HOLDER_NAME2              = @pHolderName2 
         , AC_HOLDER_NAME3              = @pHolderName3 
         , AC_HOLDER_GENDER             = @pHolderGender  
         , AC_HOLDER_MARITAL_STATUS     = @pHolderMaritalStatus 
         , AC_HOLDER_BIRTH_DATE         = @pHolderBirthDate 
         , AC_HOLDER_WEDDING_DATE       = @pHolderWeddingDate 
         , AC_HOLDER_LEVEL              = @pHolderLevel 
         , AC_HOLDER_LEVEL_NOTIFY       = @pHolderLevelNotify 
         , AC_HOLDER_LEVEL_ENTERED      = @pHolderLevelEntered 
         , AC_HOLDER_LEVEL_EXPIRATION   = @pHolderLevelExpiration 
         , AC_PIN                       = @pPin 
         , AC_PIN_FAILURES              = @pPinFailures 
         , AC_PIN_LAST_MODIFIED         = @pPinLastModified 
         , AC_BLOCKED                   = @pBlocked 
         , AC_ACTIVATED                 = @pActivated 
         , AC_BLOCK_REASON              = CASE WHEN @pBlocked = 0 THEN 0 ELSE (dbo.BlockReason_ToNewEnumerate(AC_BLOCK_REASON) | @pBlockReason) END 
         , AC_HOLDER_IS_VIP             = @pHolderIsVip 
         , AC_HOLDER_TITLE              = @pHolderTitle 
         , AC_HOLDER_NAME4              = @pHolderName4         
         , AC_HOLDER_PHONE_TYPE_01      = @pHolderPhoneType01  
         , AC_HOLDER_PHONE_TYPE_02      = @pHolderPhoneType02  
         , AC_HOLDER_STATE              = @pHolderState    
         , AC_HOLDER_COUNTRY            = @pHolderCountry  					
         , AC_MS_PERSONAL_INFO_SEQ_ID   = @pPersonalInfoSequenceId           
         , AC_USER_TYPE				          = @pUserType
         , AC_POINTS_STATUS             = @pPointsStatus
         , AC_MS_CHANGE_GUID            = AC_MS_CHANGE_GUID -- avoid trigger on update
         , AC_DEPOSIT                   = @pDeposit 
         , AC_CARD_PAID                 = @pCardPay 
         , AC_BLOCK_DESCRIPTION         = @pBlockDescription                
         , AC_CREATED                   = @pCreated
         , AC_MS_CREATED_ON_SITE_ID     = @pMSCreatedOnSiteId
         , AC_MS_HASH                   = @pMSHash
         , AC_EXTERNAL_REFERENCE        = ISNULL(@pExternalReference,AC_EXTERNAL_REFERENCE)
         , AC_HOLDER_OCCUPATION         = @pHolderOccupation
         , AC_HOLDER_EXT_NUM            = @pHolderExtNum
         , AC_HOLDER_NATIONALITY        = @pHolderNationality 
         , AC_HOLDER_BIRTH_COUNTRY      = @pHolderBirthCountry 
         , AC_HOLDER_FED_ENTITY         = @pHolderFedEntity
         , AC_HOLDER_ID1_TYPE           = @pHolderId1Type 
         , AC_HOLDER_ID2_TYPE           = @pHolderId2Type 
         , AC_HOLDER_ID3_TYPE           = @pHolderId3Type 
         , AC_HOLDER_ID3                = @pHolderId3
         , AC_HOLDER_HAS_BENEFICIARY    = @pHolderHasBeneficiary 
         , AC_BENEFICIARY_NAME          = @pBeneficiaryName 
         , AC_BENEFICIARY_NAME1         = @pBeneficiaryName1
         , AC_BENEFICIARY_NAME2         = @pBeneficiaryName2
         , AC_BENEFICIARY_NAME3         = @pBeneficiaryName3
         , AC_BENEFICIARY_BIRTH_DATE    = @pBeneficiaryBirthDate 
         , AC_BENEFICIARY_GENDER        = @pBeneficiaryGender 
         , AC_BENEFICIARY_OCCUPATION    = @pBeneficiaryOccupation
         , AC_BENEFICIARY_ID1_TYPE      = @pBeneficiaryId1Type 
         , AC_BENEFICIARY_ID1           = @pBeneficiaryId1 
         , AC_BENEFICIARY_ID2_TYPE      = @pBeneficiaryId2Type 
         , AC_BENEFICIARY_ID2           = @pBeneficiaryId2 
         , AC_BENEFICIARY_ID3_TYPE      = @pBeneficiaryId3Type 
         , AC_BENEFICIARY_ID3           = @pBeneficiaryId3 
         , AC_HOLDER_OCCUPATION_ID      = ISNULL(@pHolderOccupationId, AC_HOLDER_OCCUPATION_ID)
         , AC_BENEFICIARY_OCCUPATION_ID = ISNULL(@pBeneficiaryOccupationId, AC_BENEFICIARY_OCCUPATION_ID)
   WHERE   AC_ACCOUNT_ID                = @pAccountId 

END
GO

--
-- ReportShortfallExcess
--

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ReportShortfallExcess]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ReportShortfallExcess]

GO

CREATE PROCEDURE [dbo].[ReportShortfallExcess]
  @pDateFrom DATETIME,  
  @pDateTo   DATETIME
    
AS 
BEGIN
    --GUI USERS
    SELECT   GUI_USERS.GU_USER_ID        as  USERID 
           , 0                           as  TYPE_USER
           , GUI_USERS.GU_USERNAME       as  USERNAME 
           , GUI_USERS.GU_FULL_NAME      as  FULLNAME 
           , GUI_USERS.GU_EMPLOYEE_CODE  as  EMPLOYEECODE 
           , X.CM_CURRENCY_ISO_CODE      as  CURRENCY 
           , X.FALTANTE                  as  FALTANTE 
           , X.SOBRANTE                  as  SOBRANTE 
           , (X.SOBRANTE - X.FALTANTE)   as  DIFERENCIA
      FROM
      (
        SELECT   CM_USER_ID
               , CM_CURRENCY_ISO_CODE
               , SUM (CASE WHEN CM_TYPE = 158 THEN CM_ADD_AMOUNT ELSE 0 END) as FALTANTE
               , SUM (CASE WHEN CM_TYPE = 159 THEN CM_ADD_AMOUNT ELSE 0 END) as SOBRANTE
          FROM   CASHIER_MOVEMENTS WITH (INDEX (IX_CM_DATE_TYPE))
         WHERE   CM_DATE    >= @pDateFrom  
           AND   CM_DATE    <  @pDateTo
           AND   CM_TYPE    IN (158, 159) 
      GROUP BY   CM_USER_ID
               , CM_CURRENCY_ISO_CODE
      ) X
           
    INNER JOIN   GUI_USERS
            ON   GU_USER_ID = X.CM_USER_ID
           AND  (X.FALTANTE<>0 OR X.SOBRANTE<>0)

    UNION ALL
    
    --BANK MOBILE USERS (this kind of users have national currency only)
    SELECT   MOBILE_BANKS.MB_ACCOUNT_ID                       as  USERID 
           , 1                                                as  TYPE_USER 
           , 'MB-' + CAST(MOBILE_BANKS.mb_account_id as CHAR) as  USERNAME
           , MOBILE_BANKS.MB_HOLDER_NAME                      as  FULLNAME
           , ''                                               as  EMPLOYEECODE
           , ''                                               as  CURRENCY
           , X2.FALTANTE                                      as  FALTANTE
           , X2.SOBRANTE                                      as  SOBRANTE 
           , (X2.SOBRANTE - X2.FALTANTE)                      as  DIFERENCIA
     FROM
     (
          SELECT   MBM_MB_ID       
                 , SUM(CASE WHEN  MBM_TYPE = 8 THEN MBM_SUB_AMOUNT ELSE 0 END) AS FALTANTE
                 , SUM(CASE WHEN  MBM_TYPE = 9 THEN MBM_ADD_AMOUNT ELSE 0 END) AS SOBRANTE
            FROM   MB_MOVEMENTS WITH (INDEX (IX_MBM_DATETIME_TYPE))    
           WHERE   MBM_DATETIME  >= @pDateFrom  
             AND   MBM_DATETIME  <  @pDateTo
             AND   MBM_TYPE      IN (8,9) 
        GROUP BY   MBM_MB_ID
     ) X2
    
      INNER JOIN   MOBILE_BANKS
              ON   X2.MBM_MB_ID = MOBILE_BANKS.MB_ACCOUNT_ID
             AND   (X2.FALTANTE<>0 OR X2.SOBRANTE<>0)
        ORDER BY   USERNAME
END
GO

GRANT EXECUTE ON [dbo].[ReportShortfallExcess] TO [wggui] WITH GRANT OPTION
GO

--
-- CageGetGlobalReportData
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CageGetGlobalReportData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CageGetGlobalReportData]
GO

CREATE PROCEDURE [dbo].[CageGetGlobalReportData]
        @pCageSessionIds NVARCHAR(MAX) = NULL
AS
BEGIN

  DECLARE @NationalIsoCode NVARCHAR(3)
  DECLARE @CageOperationType_FromTerminal INT
  DECLARE @TicketsId INT
  DECLARE @BillsId INT
  DECLARE @Currencies AS NVARCHAR(200)

  SELECT @NationalIsoCode = GP_KEY_VALUE 
    FROM GENERAL_PARAMS 
   WHERE GP_GROUP_KEY = 'RegionalOptions' 
     AND GP_SUBJECT_KEY = 'CurrencyISOCode'

  SELECT @Currencies = GP_KEY_VALUE 
    FROM GENERAL_PARAMS 
   WHERE GP_GROUP_KEY = 'RegionalOptions' 
     AND GP_SUBJECT_KEY = 'CurrenciesAccepted'

  -- Split currencies ISO codes
  SELECT SST_VALUE AS CURRENCY_ISO_CODE INTO #TMP_CURRENCIES_ISO_CODES FROM [SplitStringIntoTable] (@Currencies, ';', 1)
               
  SET @CageOperationType_FromTerminal = 104
  SET @TicketsId = -200
  SET @BillsId = 0
      
  -- TABLE[0]: Get cage stock
  SELECT CGS_ISO_CODE                                          
       , CGS_DENOMINATION                                      
       , CGS_QUANTITY 
    FROM (SELECT CGS_ISO_CODE                                          
               , CGS_DENOMINATION                                      
               , CGS_QUANTITY                                          
            FROM CAGE_STOCK
                  
          UNION ALL
                  
          SELECT C.CH_ISO_CODE AS CGS_ISO_CODE
               , C.CH_DENOMINATION AS CGS_DENOMINATION
               , CST.CHSK_QUANTITY AS CGS_QUANTITY
            FROM CHIPS_STOCK AS CST
                 INNER JOIN CHIPS AS C ON CST.CHSK_CHIP_ID = C.CH_CHIP_ID
          ) AS _tbl 
    WHERE CGS_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)                                                          
 ORDER BY CGS_ISO_CODE                                          
        , CASE WHEN CGS_DENOMINATION >= 0 THEN CGS_DENOMINATION 
               ELSE CGS_DENOMINATION * (-100000) END

  IF @pCageSessionIds IS NOT NULL
  BEGIN
  
    -- Session IDs Split
    SELECT SST_VALUE AS CAGE_SESSION_ID INTO #TMP_CAGE_SESSIONS FROM [SplitStringIntoTable] (@pCageSessionIds,','  , 1)
    
    -- TABLE[1]: Get cage sessions information
    SELECT CSM.CSM_SOURCE_TARGET_ID AS CM_SOURCE_TARGET_ID
         , CSM.CSM_CONCEPT_ID AS CM_CONCEPT_ID
         , CSM.CSM_ISO_CODE AS CM_ISO_CODE
         , SUM(CSM.CSM_VALUE_IN) AS CM_VALUE_IN
         , SUM(CSM.CSM_VALUE_OUT) AS CM_VALUE_OUT
         , CST.CST_SOURCE_TARGET_NAME
         , CC.CC_DESCRIPTION
      FROM CAGE_SESSION_METERS AS CSM
           INNER JOIN CAGE_SOURCE_TARGET_CONCEPTS AS CSTC ON CSTC.CSTC_CONCEPT_ID = CSM.CSM_CONCEPT_ID AND CSTC.CSTC_SOURCE_TARGET_ID = CSM.CSM_SOURCE_TARGET_ID
           INNER JOIN CAGE_CONCEPTS AS CC ON CC.CC_CONCEPT_ID = CSTC.CSTC_CONCEPT_ID
           INNER JOIN CAGE_SOURCE_TARGET AS CST ON CST.CST_SOURCE_TARGET_ID = CSTC.CSTC_SOURCE_TARGET_ID
     WHERE CSM.CSM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
       AND CC.CC_SHOW_IN_REPORT = 1
       AND CC.CC_ENABLED = 1
       AND (CC.CC_TYPE = 1 OR CC_CONCEPT_ID IN (9, 10))
       AND CSTC.CSTC_ENABLED = 1
       AND CSM_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)
  GROUP BY CSM.CSM_SOURCE_TARGET_ID, CSM.CSM_CONCEPT_ID, CSM.CSM_ISO_CODE, CST.CST_SOURCE_TARGET_NAME, CC.CC_DESCRIPTION
  ORDER BY CSM.CSM_SOURCE_TARGET_ID
         , CASE WHEN CSM.CSM_CONCEPT_ID = 0 THEN '000' ELSE CC.CC_DESCRIPTION END
         , CASE WHEN CSM.CSM_ISO_CODE = @NationalIsoCode THEN '000' ELSE CSM.CSM_ISO_CODE END                    

    -- TABLE[2]: Get dynamic liabilities
    SELECT CC.CC_CONCEPT_ID AS LIABILITY_CONCEPT_ID
         , CC.CC_DESCRIPTION AS LIABILITY_NAME 
         , CSM.CSM_ISO_CODE AS LIABILITY_ISO_CODE
         , SUM(CSM.CSM_VALUE) AS LIABILITY_VALUE
      FROM CAGE_SESSION_METERS AS CSM
           INNER JOIN CAGE_CONCEPTS AS CC ON CC.CC_CONCEPT_ID = CSM.CSM_CONCEPT_ID
     WHERE CC.CC_ENABLED = 1
       AND CC.CC_SHOW_IN_REPORT = 1
       AND CC.CC_IS_PROVISION = 1
       AND CSM.CSM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
       AND CSM_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)
 GROUP BY CC.CC_CONCEPT_ID, CC.CC_DESCRIPTION, CSM.CSM_ISO_CODE
  ORDER BY CC.CC_CONCEPT_ID ASC
         , CASE WHEN CSM.CSM_ISO_CODE = @NationalIsoCode THEN '000' ELSE CSM.CSM_ISO_CODE END 
      
    -- TABLE[3]: Get session sumary
    SELECT CMD_ISO_CODE AS ISO_CODE
         , ISNULL(ORIGIN, 0) AS ORIGIN
         , SUM(CMD_DEPOSITS) AS DEPOSITS
         , SUM(CMD_WITHDRAWALS) AS WITHDRAWALS
      FROM (SELECT CMD_ISO_CODE
                 , ORIGIN
                 , SUM(CMD_DEPOSITS) CMD_DEPOSITS
                 , SUM(CMD_WITHDRAWALS) CMD_WITHDRAWALS      
              FROM (SELECT CASE WHEN CMD_QUANTITY >= 0 THEN @BillsId
                                ELSE CMD_QUANTITY END  AS ORIGIN
                         , CMD_ISO_CODE
                         , CMD_DENOMINATION
                         , CASE WHEN CGM_TYPE IN (100, 101, 103, 104) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY < 0 THEN CMD_DENOMINATION
                                                                                            ELSE CMD_QUANTITY * CMD_DENOMINATION END), 0)
                                ELSE 0 END AS CMD_DEPOSITS
                         , CASE WHEN CGM_TYPE IN (0, 1, 4, 5) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY < 0 THEN CMD_DENOMINATION             
                                                                                    ELSE CMD_QUANTITY * CMD_DENOMINATION END), 0)
                                ELSE 0 END AS CMD_WITHDRAWALS
                      FROM CAGE_MOVEMENTS 
                           INNER JOIN   CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          
                     WHERE CGM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
                       AND (CGM_STATUS IN (2, 3, 4, 8, 9, 10) 
                            OR (CGM_STATUS = 0 AND CGM_TYPE = 0))
                  GROUP BY CMD_ISO_CODE, CMD_DENOMINATION, CGM_TYPE, CMD_QUANTITY
                   ) AS T1        
          GROUP BY ORIGIN, CMD_ISO_CODE

            -- GET Terminal value-in amounts
              
              UNION ALL 
              
              SELECT @NationalIsoCode AS CMD_ISO_CODE
                    , @TicketsId AS ORIGIN
                    , MC_COLLECTED_TICKET_AMOUNT AS DEPOSITS
                    , 0 AS WITHDRAWS
              FROM MONEY_COLLECTIONS
                  INNER JOIN CAGE_MOVEMENTS ON CGM_MC_COLLECTION_ID = MC_COLLECTION_ID 
             WHERE CAGE_MOVEMENTS.CGM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
               AND CAGE_MOVEMENTS.CGM_TYPE = @CAGEOPERATIONTYPE_FROMTERMINAL
               AND (CGM_STATUS IN (2, 3, 4, 8, 9, 10) 
                    OR (CGM_STATUS = 0 AND CGM_TYPE = 0))           
             
              UNION ALL
              
                SELECT @NationalIsoCode AS CMD_ISO_CODE
                    , @BillsId AS ORIGIN
                    , MC_COLLECTED_BILL_AMOUNT  AS DEPOSITS
                    , 0 AS WITHDRAWS
              FROM MONEY_COLLECTIONS
                  INNER JOIN CAGE_MOVEMENTS ON CGM_MC_COLLECTION_ID = MC_COLLECTION_ID 
             WHERE CAGE_MOVEMENTS.CGM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
               AND CAGE_MOVEMENTS.CGM_TYPE = @CageOperationType_FromTerminal
               AND (CGM_STATUS IN (2, 3, 4, 8, 9, 10) 
                    OR (CGM_STATUS = 0 AND CGM_TYPE = 0))
                                      
          ) AS _TBL_GLB
      WHERE CMD_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)
   GROUP BY _TBL_GLB.CMD_ISO_CODE, _TBL_GLB.ORIGIN
   ORDER BY CASE WHEN _TBL_GLB.CMD_ISO_CODE = @NationalIsoCode THEN '000' ELSE _TBL_GLB.CMD_ISO_CODE END 
          , _TBL_GLB.ORIGIN DESC
         
    -- TABLE[4]: Other System Concepts           
    SELECT CSM_CONCEPT_ID AS CM_CONCEPT_ID
         , CC.CC_DESCRIPTION AS CM_DESCRIPTION
         , CSM_ISO_CODE AS CM_ISO_CODE
         , SUM(CSM_VALUE) AS CM_VALUE
      FROM CAGE_SESSION_METERS AS CSM
           INNER JOIN CAGE_CONCEPTS AS CC ON CSM.CSM_CONCEPT_ID = CC.CC_CONCEPT_ID
     WHERE CSM_SOURCE_TARGET_ID = 0
       AND CSM_CONCEPT_ID IN (7, 8)
       AND CSM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
       AND CSM_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)
  GROUP BY CSM_CONCEPT_ID, CSM_ISO_CODE, CSM_VALUE, CC.CC_DESCRIPTION 
  ORDER BY CM_CONCEPT_ID
         , CASE WHEN CSM_ISO_CODE = @NationalIsoCode THEN '000' ELSE CSM_ISO_CODE END 
         
     -- TABLE[5]: Cage Counts
    SELECT ORIGIN AS ORIGIN
         , CMD_ISO_CODE AS ISO_CODE
         , ISNULL(SUM(CMD_DEPOSITS), 0) AS DEPOSITS
      FROM (SELECT CASE WHEN CMD_QUANTITY >= 0 THEN 0
                        ELSE CMD_QUANTITY END  AS ORIGIN
                 , CMD_ISO_CODE
                 , ISNULL(SUM (CASE WHEN CMD_QUANTITY >= 0 THEN CMD_QUANTITY 
                                    ELSE 1 END * CASE WHEN CGM_TYPE = 99 THEN -1 * CMD_DENOMINATION 
												                              ELSE CMD_DENOMINATION END), 0) AS CMD_DEPOSITS
              FROM CAGE_MOVEMENTS 
                   INNER JOIN CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          
--             WHERE CGM_TYPE = 3
				WHERE CGM_TYPE IN (99, 199)
               AND CGM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
          GROUP BY CMD_ISO_CODE, CMD_DENOMINATION, CGM_TYPE, CMD_QUANTITY
            ) AS T1           
   GROUP BY ORIGIN, CMD_ISO_CODE
           
    UNION ALL
              
    SELECT NULL AS ORIGIN
         , CMD_ISO_CODE AS ISO_CODE
         , ISNULL(SUM(CMD_DEPOSITS), 0) AS DEPOSITS
      FROM (SELECT CMD_ISO_CODE
                 , ISNULL(SUM (CASE WHEN CMD_QUANTITY >= 0 THEN CMD_QUANTITY 
                                    ELSE 1 END * CASE WHEN CGM_TYPE = 99 THEN -1 * CMD_DENOMINATION 
												                              ELSE CMD_DENOMINATION END), 0) AS CMD_DEPOSITS
              FROM CAGE_MOVEMENTS 
                   INNER JOIN CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          
             WHERE CGM_TYPE IN (99, 199)
               AND CGM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
          GROUP BY CMD_ISO_CODE, CMD_DENOMINATION, CGM_TYPE, CMD_QUANTITY
           ) AS T1           
  GROUP BY CMD_ISO_CODE

    DROP TABLE #TMP_CAGE_SESSIONS 

  END -- IF @pCageSessionIds IS NOT NULL THEN
      
  ELSE -- IF @pCageSessionIds IS NULL
  BEGIN
      
    -- TABLE[1]: Get cage sessions information
    SELECT CM.CM_SOURCE_TARGET_ID
         , CM.CM_CONCEPT_ID
         , CM.CM_ISO_CODE
         , CM.CM_VALUE_IN
         , CM.CM_VALUE_OUT
         , CST.CST_SOURCE_TARGET_NAME
         , CC.CC_DESCRIPTION
      FROM CAGE_METERS AS CM
           INNER JOIN CAGE_SOURCE_TARGET_CONCEPTS AS CSTC ON CSTC.CSTC_CONCEPT_ID = CM.CM_CONCEPT_ID AND CSTC.CSTC_SOURCE_TARGET_ID = CM.CM_SOURCE_TARGET_ID
           INNER JOIN CAGE_CONCEPTS AS CC ON CC.CC_CONCEPT_ID = CSTC.CSTC_CONCEPT_ID
           INNER JOIN CAGE_SOURCE_TARGET AS CST ON CST.CST_SOURCE_TARGET_ID = CSTC.CSTC_SOURCE_TARGET_ID
     WHERE (CC.CC_TYPE = 1 OR CC_CONCEPT_ID IN (9, 10))
       AND CC.CC_ENABLED = 1
       AND CSTC.CSTC_ENABLED = 1
       AND CC.CC_SHOW_IN_REPORT = 1
       AND CM_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)
  ORDER BY CM.CM_SOURCE_TARGET_ID
         , CASE WHEN CM.CM_CONCEPT_ID = 0 THEN '000' ELSE CC.CC_DESCRIPTION END
         , CASE WHEN CM.CM_ISO_CODE = @NationalIsoCode THEN '000' ELSE CM.CM_ISO_CODE END   

    -- TABLE[2]: Get dynamic liabilities
    SELECT CC.CC_CONCEPT_ID AS LIABILITY_CONCEPT_ID
         , CC.CC_DESCRIPTION AS LIABILITY_NAME 
         , CM.CM_ISO_CODE AS LIABILITY_ISO_CODE
         , SUM(CM.CM_VALUE) AS LIABILITY_VALUE
      FROM CAGE_METERS AS CM
           INNER JOIN CAGE_CONCEPTS AS CC ON CC.CC_CONCEPT_ID = CM.CM_CONCEPT_ID
     WHERE CC.CC_ENABLED = 1
       AND CC.CC_SHOW_IN_REPORT = 1
       AND CC.CC_IS_PROVISION = 1
       AND CM_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)
  GROUP BY CC.CC_CONCEPT_ID, CC.CC_DESCRIPTION, CM.CM_ISO_CODE
  ORDER BY CC.CC_CONCEPT_ID ASC 
         , CASE WHEN CM.CM_ISO_CODE = @NationalIsoCode THEN '000' ELSE CM.CM_ISO_CODE END 
      
    -- TABLE[3]: Get session sumary
    SELECT CMD_ISO_CODE AS ISO_CODE
         , ISNULL(ORIGIN, 0) AS ORIGIN
         , SUM(CMD_DEPOSITS) AS DEPOSITS
         , SUM(CMD_WITHDRAWALS) AS WITHDRAWALS     
      FROM (SELECT CMD_ISO_CODE
                 , ORIGIN
                 , SUM(CMD_DEPOSITS) CMD_DEPOSITS
                 , SUM(CMD_WITHDRAWALS) CMD_WITHDRAWALS      
              FROM (SELECT CASE WHEN CMD_QUANTITY >= 0 THEN @BillsId
                                ELSE CMD_QUANTITY END AS ORIGIN
                         , CMD_ISO_CODE
                         , CMD_DENOMINATION
                         , CASE WHEN CGM_TYPE IN (100, 101, 103, 104) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY < 0 THEN CMD_DENOMINATION
                                                                                            ELSE CMD_QUANTITY * CMD_DENOMINATION END), 0)
                                ELSE 0 END AS CMD_DEPOSITS
                         , CASE WHEN CGM_TYPE IN (0, 1, 4, 5) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY < 0 THEN CMD_DENOMINATION             
                                                                                    ELSE CMD_QUANTITY * CMD_DENOMINATION END), 0)
                                ELSE 0 END AS CMD_WITHDRAWALS
                      FROM CAGE_MOVEMENTS 
                           INNER JOIN   CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          
                     WHERE CGM_STATUS IN (2, 3, 4, 8, 9, 10) 
                        OR (CGM_STATUS = 0 AND CGM_TYPE = 0)
                  GROUP BY CMD_ISO_CODE, CMD_DENOMINATION, CGM_TYPE, CMD_QUANTITY
                    ) AS T1
           GROUP BY ORIGIN,CMD_ISO_CODE

              -- GET Terminal value-in amounts
              
              UNION ALL 
              
              SELECT @NationalIsoCode AS CMD_ISO_CODE
                    , @TicketsId AS ORIGIN
                    , MC_COLLECTED_TICKET_AMOUNT AS DEPOSITS
                    , 0 AS WITHDRAWS
              FROM MONEY_COLLECTIONS
                  INNER JOIN CAGE_MOVEMENTS ON CGM_MC_COLLECTION_ID = MC_COLLECTION_ID 
             WHERE CAGE_MOVEMENTS.CGM_TYPE = @CAGEOPERATIONTYPE_FROMTERMINAL
               AND (CGM_STATUS IN (2, 3, 4, 8, 9, 10) 
                    OR (CGM_STATUS = 0 AND CGM_TYPE = 0))           
             
              UNION ALL
              
                SELECT @NationalIsoCode AS CMD_ISO_CODE
                    , @BillsId AS ORIGIN
                    , MC_COLLECTED_BILL_AMOUNT  AS DEPOSITS
                    , 0 AS WITHDRAWS
              FROM MONEY_COLLECTIONS
                  INNER JOIN CAGE_MOVEMENTS ON CGM_MC_COLLECTION_ID = MC_COLLECTION_ID 
             WHERE CAGE_MOVEMENTS.CGM_TYPE = @CageOperationType_FromTerminal
               AND (CGM_STATUS IN (2, 3, 4, 8, 9, 10) 
                    OR (CGM_STATUS = 0 AND CGM_TYPE = 0))
                          
          ) AS _TBL_GLB 
      WHERE CMD_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)             
   GROUP BY _TBL_GLB.CMD_ISO_CODE, _TBL_GLB.ORIGIN
   ORDER BY CASE WHEN _TBL_GLB.CMD_ISO_CODE = @NationalIsoCode THEN '000' ELSE _TBL_GLB.CMD_ISO_CODE END 
          , _TBL_GLB.ORIGIN DESC 
         
    -- TABLE[4]: Other System Concepts           
    SELECT CM_CONCEPT_ID
         , CC.CC_DESCRIPTION AS CM_DESCRIPTION
         , CM_ISO_CODE
         , SUM(CM_VALUE) AS CM_VALUE
      FROM CAGE_METERS AS CM
           INNER JOIN CAGE_CONCEPTS AS CC ON CM.CM_CONCEPT_ID = CC.CC_CONCEPT_ID
     WHERE CM_SOURCE_TARGET_ID = 0
       AND CM_CONCEPT_ID IN (7, 8)
       AND CM_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)
  GROUP BY CM_CONCEPT_ID, CM_ISO_CODE, CM_VALUE, CC.CC_DESCRIPTION
  ORDER BY CM_CONCEPT_ID
         , CASE WHEN CM_ISO_CODE = @NationalIsoCode THEN '000' ELSE CM_ISO_CODE END
    
    -- TABLE[5]: Cage Counts
    SELECT ORIGIN AS ORIGIN
         , CMD_ISO_CODE AS ISO_CODE
         , ISNULL(SUM(CMD_DEPOSITS), 0) AS DEPOSITS
      FROM (SELECT CASE WHEN CMD_QUANTITY >= 0 THEN 0
                        ELSE CMD_QUANTITY END  AS ORIGIN
                 , CMD_ISO_CODE
                 , ISNULL(SUM (CASE WHEN CMD_QUANTITY >= 0 THEN CMD_QUANTITY 
                                    ELSE 1 END * CASE WHEN CGM_TYPE = 99 THEN -1 * CMD_DENOMINATION 
												                              ELSE CMD_DENOMINATION END), 0) AS CMD_DEPOSITS
              FROM CAGE_MOVEMENTS 
                   INNER JOIN CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          
             WHERE CGM_TYPE IN (99, 199)
          GROUP BY CMD_ISO_CODE, CMD_DENOMINATION, CGM_TYPE, CMD_QUANTITY
            ) AS T1           
   GROUP BY ORIGIN, CMD_ISO_CODE
           
    UNION ALL
              
    SELECT NULL AS ORIGIN
         , CMD_ISO_CODE AS ISO_CODE
         , ISNULL(SUM(CMD_DEPOSITS), 0) AS DEPOSITS
      FROM (SELECT CMD_ISO_CODE
                 , ISNULL(SUM (CASE WHEN CMD_QUANTITY >= 0 THEN CMD_QUANTITY 
                                    ELSE 1 END * CASE WHEN CGM_TYPE = 99 THEN -1 * CMD_DENOMINATION 
												                              ELSE CMD_DENOMINATION END), 0) AS CMD_DEPOSITS
              FROM CAGE_MOVEMENTS 
                   INNER JOIN CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          
             WHERE CGM_TYPE IN (99, 199)
          GROUP BY CMD_ISO_CODE, CMD_DENOMINATION, CGM_TYPE, CMD_QUANTITY
           ) AS T1           
  GROUP BY CMD_ISO_CODE


  END -- ELSE
  
  DROP TABLE #TMP_CURRENCIES_ISO_CODES
                   
END -- CageGetGlobalReportData

GO
GRANT EXECUTE ON [dbo].[CageGetGlobalReportData] TO [wggui] WITH GRANT OPTION 
GO

--
-- Migrate BlockReasons enumerate (only if not a member of MultiSite)
--
DECLARE @ACCOUNT_ID BIGINT
DECLARE @ACCOUNTS_TABLE TABLE(AC_ACCOUNT_ID BIGINT)
DECLARE @DATETIME_INIT DATETIME
DECLARE @DATETIME_FINISH DATETIME

IF NOT EXISTS (SELECT   1
             FROM   GENERAL_PARAMS 
            WHERE   GP_GROUP_KEY   = N'Site' 
              AND   GP_SUBJECT_KEY = N'MultiSiteMember' 
              AND   GP_KEY_VALUE   = N'1')
BEGIN
  INSERT INTO @ACCOUNTS_TABLE(AC_ACCOUNT_ID)SELECT AC_ACCOUNT_ID FROM ACCOUNTS WHERE AC_BLOCK_REASON > 0 AND AC_BLOCK_REASON <= 10

  SET @DATETIME_INIT = GETDATE()
  SELECT 'BEGIN' _STATE, @DATETIME_INIT _DATETIME

  DECLARE Curs CURSOR FOR SELECT AC_ACCOUNT_ID FROM @ACCOUNTS_TABLE

  OPEN Curs
	  FETCH NEXT FROM Curs INTO @ACCOUNT_ID

	  WHILE @@FETCH_STATUS = 0
	  BEGIN
		    UPDATE  ACCOUNTS  
		       SET  AC_BLOCK_REASON = POWER(2, AC_BLOCK_REASON + 7) -- 2^(AC_BLOCK_REASON-1) x 256
		     WHERE  AC_ACCOUNT_ID = @ACCOUNT_ID
  		
		  FETCH NEXT FROM Curs INTO @ACCOUNT_ID
	  END -- WHILE
  	
  CLOSE Curs
  DEALLOCATE Curs
  SET @DATETIME_FINISH = GETDATE()

  SELECT 'END' _STATE, @DATETIME_FINISH _DATETIME
  SELECT DATEDIFF(MINUTE, @DATETIME_INIT, @DATETIME_FINISH) 'TOTAL TIME (minutes)'
END

GO
