/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 330;

SET @New_ReleaseId = 331;
SET @New_ScriptName = N'UpdateTo_18.331.041.sql';
SET @New_Description = N'Exped';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/**** GENERAL PARAM *****/
IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='ExternalPaymentAndSaleValidation' AND GP_SUBJECT_KEY = 'TimeOutInMilliseconds')
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('ExternalPaymentAndSaleValidation', 'TimeOutInMilliseconds', 10000)
GO

/******* TABLES  *******/

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EXTERNAL_VALIDATION_OPERATION]') AND type in (N'U'))
BEGIN
  CREATE TABLE EXTERNAL_VALIDATION_OPERATION
  (
      evo_id                    BIGINT IDENTITY(1, 1),
      evo_operation_id          BIGINT NOT NULL,
      evo_account_id            BIGINT NOT NULL,
      evo_net_amount            MONEY NOT NULL,
      evo_gross_amount          MONEY NOT NULL,

      evo_operation_type        INTEGER,
      evo_business_line_type    INTEGER,
      evo_site_id               INTEGER,
      
      evo_authorization_code    NVARCHAR(200),
      evo_transaction_id        BIGINT,

      evo_created_date          DATETIME NOT NULL DEFAULT GETDATE(),
      evo_status                INTEGER NOT NULL DEFAULT 0,
      evo_updated_date          DATETIME,

      CONSTRAINT PK_EXTERNAL_VALIDATION_OPERATION PRIMARY KEY
      (
        evo_id ASC
      )
  )
END
GO

/******* INDEXES *******/

/******* RECORDS *******/


/******* PROCEDURES *******/

IF OBJECT_ID (N'dbo.GT_Session_Information', N'P') IS NOT NULL
    DROP PROCEDURE dbo.GT_Session_Information;
GO

/*
----------------------------------------------------------------------------------------------------------------
BASIC REPORT QUERY FOR GAMING TABLE SESSION INFORMATION

Version   Date            User      Description
----------------------------------------------------------------------------------------------------------------
1.0.0     31-JAN-2014     RMS       First Release
1.0.1     27-APR-2016     RAB       Include differents chip types in a operations.
1.0.2     25-AUG-2016     FGB       GT_Session_Information: Optimization.
1.0.3     07-SEP-2016     FGB       GT_Session_Information: Optimization.

Requeriments:
   -- Functions:

Parameters:
   -- BASE TYPE:        To select if report data source is session or cashier movements.
                          0 - By Session
                          1 - By Cashier Movements
   -- DATE FROM:        Start date for data collection. (If NULL then use first available date).
   -- DATE TO:          End date for data collection. (If NULL then use current date).
   -- STATUS:           The gaming table session status (Opened or Closed) (-1 to show all).
   -- ENABLED:          The gaming table status (Enabled or Disabled) (-1 to show all).
   -- AREA:             Location Area of the gaming table.
   -- BANK:             Area bank of location.
   -- CHIPS ISO CODE:   ISO Code for chips in cage.
   -- CHIPS COINS CODE: Coins code for chips in cage.
   -- VALID TYPES:      A string that contains the valid table types to list.
*/

CREATE PROCEDURE [dbo].[GT_Session_Information]
(
    @pBaseType            INTEGER
  , @pDateFrom            DATETIME
  , @pDateTo              DATETIME
  , @pStatus              INTEGER
  , @pEnabled             INTEGER
  , @pAreaId              INTEGER
  , @pBankId              INTEGER
  , @pChipsISOCode        VARCHAR(50)
  , @pChipsCoinsCode      INTEGER
  , @pValidTypes          VARCHAR(4096)
)
AS
BEGIN

----------------------------------------------------------------------------------------------------------------
DECLARE @_BASE_TYPE        AS   INTEGER
DECLARE @_DATE_FROM        AS   DATETIME
DECLARE @_DATE_TO          AS   DATETIME
DECLARE @_STATUS           AS   INTEGER
DECLARE @_ENABLED          AS   INTEGER
DECLARE @_AREA             AS   INTEGER
DECLARE @_BANK             AS   INTEGER
DECLARE @_CHIPS_ISO_CODE   AS   VARCHAR(50)
DECLARE @_CHIPS_COINS_CODE AS   VARCHAR(50)
DECLARE @_DELIMITER        AS   CHAR(1)
DECLARE @_TYPES_TABLE           TABLE(SST_ID INT, SST_VALUE VARCHAR(50)) -- TO STORE THE VALID GAMING TABLE TYPES

-- CHIP TYPES
DECLARE @_CHIPS_RE    AS INTEGER
DECLARE @_CHIPS_NR    AS INTEGER
DECLARE @_CHIPS_COLOR AS INTEGER

-- MOVEMENTS
DECLARE @_CHIPS_MOVEMENT_PURCHASE_TOTAL          AS   INTEGER
DECLARE @_CHIPS_MOVEMENT_SALES_TOTAL             AS   INTEGER
DECLARE @_CHIPS_MOVEMENT_PURCHASE_TOTAL_EXCHANGE AS   INTEGER
DECLARE @_CHIPS_MOVEMENT_SALES_TOTAL_EXCHANGE    AS   INTEGER
DECLARE @_MOVEMENT_FILLER_IN                     AS   INTEGER
DECLARE @_MOVEMENT_FILLER_OUT                    AS   INTEGER
DECLARE @_MOVEMENT_CAGE_FILLER_OUT               AS   INTEGER
DECLARE @_MOVEMENT_CAGE_CLOSE_SESSION            AS   INTEGER
DECLARE @_MOVEMENT_CLOSE_SESSION                 AS   INTEGER

----------------------------------------------------------------------------------------------------------------
-- Initialization --
SET @_DELIMITER        =   ','
SET @_BASE_TYPE        =   @pBaseType
SET @_DATE_FROM        =   @pDateFrom
SET @_DATE_TO          =   @pDateTo
SET @_STATUS           =   ISNULL(@pStatus, -1)
SET @_ENABLED          =   ISNULL(@pEnabled, -1)
SET @_AREA             =   ISNULL(@pAreaId, 0)
SET @_BANK             =   ISNULL(@pBankId, 0)
SET @_CHIPS_ISO_CODE   =   ISNULL(@pChipsISOCode, 'X01')
SET @_CHIPS_COINS_CODE =   ISNULL(@pChipsCoinsCode, -100)
SET @_CHIPS_RE         =   1001
SET @_CHIPS_NR         =   1002
SET @_CHIPS_COLOR      =   1003

SET @_CHIPS_MOVEMENT_PURCHASE_TOTAL          =   304
SET @_CHIPS_MOVEMENT_SALES_TOTAL             =   303
SET @_CHIPS_MOVEMENT_PURCHASE_TOTAL_EXCHANGE =   310
SET @_CHIPS_MOVEMENT_SALES_TOTAL_EXCHANGE    =   309
SET @_MOVEMENT_FILLER_IN                     =   2
SET @_MOVEMENT_FILLER_OUT                    =   3
SET @_MOVEMENT_CAGE_FILLER_OUT               =   203
SET @_MOVEMENT_CAGE_CLOSE_SESSION            =   201
SET @_MOVEMENT_CLOSE_SESSION                 =   1

----------------------------------------------------------------------------------------------------------------
-- CHECK DATE PARAMETERS
IF @_DATE_FROM IS NULL
BEGIN
   -- IF DATE FROM IS NULL, USER FIRST POSIBLE DATE
   SET @_DATE_FROM = CAST('' AS DATETIME)
END

IF @_DATE_TO IS NULL
BEGIN
   -- IF DATE TO IS NULL, QUERY UNTIL CURRENT DATE
   SET @_DATE_TO = CAST(GETDATE() AS DATETIME)
END

-- ASSIGN TYPES PARAMETER INTO TABLE
INSERT INTO @_TYPES_TABLE SELECT * FROM dbo.SplitStringIntoTable(@pValidTypes, @_DELIMITER, DEFAULT)

----------------------------------------------------------------------------------------------------------------

IF @_BASE_TYPE = 0
BEGIN
  -- REPORT BY GAMING TABLE SESSION
  SELECT    GTS_CASHIER_SESSION_ID
          , CS_NAME
          , CS_STATUS
          , GT_NAME
          , GTT_NAME
          , GT_HAS_INTEGRATED_CASHIER
          , CT_NAME
          , GU_USERNAME
          , CS_OPENING_DATE
          , CS_CLOSING_DATE
          , SUM(ISNULL(GTSC_TOTAL_PURCHASE_AMOUNT, 0) )   AS GTS_TOTAL_PURCHASE_AMOUNT
          , SUM(ISNULL(GTSC_TOTAL_SALES_AMOUNT, 0)    )   AS GTS_TOTAL_SALES_AMOUNT
          , SUM(ISNULL(GTSC_INITIAL_CHIPS_AMOUNT, 0)  )   AS GTS_INITIAL_CHIPS_AMOUNT
          , SUM(ISNULL(GTSC_FILLS_CHIPS_AMOUNT, 0)    )   AS GTS_FILLS_CHIPS_AMOUNT
          , SUM(ISNULL(GTSC_CREDITS_CHIPS_AMOUNT, 0)  )   AS GTS_CREDITS_CHIPS_AMOUNT
          , SUM(ISNULL(GTSC_FINAL_CHIPS_AMOUNT, 0)    )   AS GTS_FINAL_CHIPS_AMOUNT
          , SUM(ISNULL(GTSC_TIPS, 0)                  )   AS GTS_TIPS
          , SUM(ISNULL(GTSC_COLLECTED_AMOUNT, 0)      )   AS GTS_COLLECTED_AMOUNT
          , GTS_CLIENT_VISITS
          , AR_NAME
          , BK_NAME
          , GTS_GAMING_TABLE_SESSION_ID
          , dbo.GT_Calculate_DROP(SUM(ISNULL(GTSC_OWN_SALES_AMOUNT, 0)), SUM(ISNULL(GTSC_EXTERNAL_SALES_AMOUNT, 0)), SUM(ISNULL(GTSC_COLLECTED_AMOUNT, 0)), GT_HAS_INTEGRATED_CASHIER) AS DROP_BY_CURRENCY
          , GTSC_ISO_CODE
          , dbo.GT_Calculate_DROP(ISNULL(GTS_OWN_SALES_AMOUNT, 0), ISNULL(GTS_EXTERNAL_SALES_AMOUNT, 0), ISNULL(GTS_COLLECTED_AMOUNT, 0), GT_HAS_INTEGRATED_CASHIER) AS DROP_LOCAL_CURRENCY
   FROM         GAMING_TABLES_SESSIONS
   LEFT    JOIN GAMING_TABLES_SESSIONS_BY_CURRENCY ON GTS_GAMING_TABLE_SESSION_ID = GTSC_GAMING_TABLE_SESSION_ID AND GTSC_TYPE <> @_CHIPS_COLOR
   INNER   JOIN CASHIER_SESSIONS                   ON CS_SESSION_ID               = GTS_CASHIER_SESSION_ID
   LEFT    JOIN GUI_USERS                           ON GU_USER_ID                  = CS_USER_ID
   LEFT    JOIN GAMING_TABLES                       ON GT_GAMING_TABLE_ID          = GTS_GAMING_TABLE_ID
   LEFT    JOIN GAMING_TABLES_TYPES                 ON GTT_GAMING_TABLE_TYPE_ID    = GT_TYPE_ID
   LEFT    JOIN AREAS                               ON GT_AREA_ID                  = AR_AREA_ID
   LEFT    JOIN BANKS                               ON GT_BANK_ID                  = BK_BANK_ID
   LEFT    JOIN CASHIER_TERMINALS                   ON GT_CASHIER_ID               = CT_CASHIER_ID
 WHERE   CS_OPENING_DATE >= @_DATE_FROM
   AND   CS_OPENING_DATE < @_DATE_TO
   AND   (GT_ENABLED = CASE WHEN @_ENABLED = -1 THEN GT_ENABLED ELSE @_ENABLED END)
   AND   (CS_STATUS  = CASE WHEN @_STATUS  = -1 THEN CS_STATUS  ELSE @_STATUS  END)
   AND   (GT_AREA_ID = CASE WHEN @_AREA    = 0  THEN GT_AREA_ID ELSE @_AREA    END)
   AND   (GT_BANK_ID = CASE WHEN @_BANK    = 0  THEN GT_BANK_ID ELSE @_BANK    END)
   AND   (GT_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE ))
   GROUP BY GTS_CASHIER_SESSION_ID
         , CS_NAME
         , CS_STATUS
         , GT_NAME
         , GTT_NAME
         , GT_HAS_INTEGRATED_CASHIER
         , CT_NAME
         , GU_USERNAME
         , CS_OPENING_DATE
         , CS_CLOSING_DATE
         , GTS_CLIENT_VISITS
         , AR_NAME
         , BK_NAME
         , GTS_GAMING_TABLE_SESSION_ID
         , GTS_OWN_SALES_AMOUNT
         , GTS_EXTERNAL_SALES_AMOUNT
         , GTS_COLLECTED_AMOUNT
         , GTSC_ISO_CODE
     ORDER BY CS_OPENING_DATE
;

END
ELSE
BEGIN
  -- REPORT BY CASHIER MOVEMENTS

  SELECT   CM_SESSION_ID
         , CS_NAME
         , CS_STATUS
         , GT_NAME
         , GTT_NAME
         , GT_HAS_INTEGRATED_CASHIER
         , CT_NAME
         , GU_USERNAME
         , CS_OPENING_DATE
         , CS_CLOSING_DATE
         , SUM(CASE WHEN CM_TYPE                  = @_CHIPS_MOVEMENT_PURCHASE_TOTAL          THEN CM_ADD_AMOUNT
                    WHEN CM_TYPE                  = @_CHIPS_MOVEMENT_PURCHASE_TOTAL_EXCHANGE THEN CM_INITIAL_BALANCE
                    ELSE 0
               END
              )  AS CM_TOTAL_PURCHASE_AMOUNT
         , SUM(CASE WHEN CM_TYPE                  = @_CHIPS_MOVEMENT_SALES_TOTAL             THEN CM_SUB_AMOUNT
                    WHEN CM_TYPE                  = @_CHIPS_MOVEMENT_SALES_TOTAL_EXCHANGE    THEN CM_INITIAL_BALANCE
                    ELSE 0
               END
              )  AS CM_TOTAL_SALES_AMOUNT
         , SUM(CASE WHEN (CM_CURRENCY_ISO_CODE    = @_CHIPS_ISO_CODE
                     OR  CM_CAGE_CURRENCY_TYPE  IN (@_CHIPS_RE, @_CHIPS_NR))
                     AND CM_TYPE                  = @_MOVEMENT_FILLER_IN
                    THEN CM_ADD_AMOUNT ELSE 0 END
              )  AS CM_INITIAL_CHIPS_AMOUNT
         , SUM(CASE WHEN (CM_CURRENCY_ISO_CODE    = @_CHIPS_ISO_CODE
                     OR  CM_CAGE_CURRENCY_TYPE  IN (@_CHIPS_RE, @_CHIPS_NR))
                     AND CM_TYPE                  = @_MOVEMENT_FILLER_IN
                    THEN CM_ADD_AMOUNT
                    ELSE 0
               END
              )  AS CM_FILLS_CHIPS_AMOUNT
         , SUM(CASE WHEN (CM_CURRENCY_ISO_CODE    = @_CHIPS_ISO_CODE
                       OR  CM_CAGE_CURRENCY_TYPE   IN (@_CHIPS_RE, @_CHIPS_NR))
                       AND CM_TYPE                  = @_MOVEMENT_FILLER_OUT
                      THEN CM_SUB_AMOUNT
                    WHEN (CM_CURRENCY_ISO_CODE    = @_CHIPS_ISO_CODE
                       OR  CM_CAGE_CURRENCY_TYPE  IN (@_CHIPS_RE, @_CHIPS_NR))
                       AND CM_TYPE                  = @_MOVEMENT_CAGE_CLOSE_SESSION
                     THEN CM_INITIAL_BALANCE
                    ELSE 0
               END
              )  AS CM_CREDITS_CHIPS_AMOUNT
         , SUM(CASE WHEN (CM_CURRENCY_ISO_CODE    = @_CHIPS_ISO_CODE
                     OR  CM_CAGE_CURRENCY_TYPE   IN (@_CHIPS_RE, @_CHIPS_NR))
                     AND CM_TYPE                  = @_MOVEMENT_FILLER_OUT
                    THEN CM_SUB_AMOUNT
                    WHEN (CM_CURRENCY_ISO_CODE    = @_CHIPS_ISO_CODE
                     OR  CM_CAGE_CURRENCY_TYPE  IN (@_CHIPS_RE, @_CHIPS_NR))
                     AND CM_TYPE                  = @_MOVEMENT_CAGE_CLOSE_SESSION
                    THEN CM_INITIAL_BALANCE
                    ELSE 0 END
              )  AS CM_FINAL_CHIPS_AMOUNT
         , SUM(CASE WHEN CM_TYPE                  = @_MOVEMENT_CAGE_FILLER_OUT
                     AND ((CM_CURRENCY_ISO_CODE   = @_CHIPS_ISO_CODE
                     AND CM_CURRENCY_DENOMINATION = @_CHIPS_COINS_CODE)
                      OR CM_CAGE_CURRENCY_TYPE  IN (@_CHIPS_RE, @_CHIPS_NR))
                    THEN CM_SUB_AMOUNT
                    WHEN CM_TYPE                  = @_MOVEMENT_CAGE_CLOSE_SESSION
                     AND ((CM_CURRENCY_ISO_CODE   = @_CHIPS_ISO_CODE
                     AND CM_CURRENCY_DENOMINATION = @_CHIPS_COINS_CODE)
                      OR CM_CAGE_CURRENCY_TYPE  IN (@_CHIPS_RE, @_CHIPS_NR))
                    THEN CM_INITIAL_BALANCE
                    ELSE 0
               END
              )  AS CM_TIPS
         , SUM(CASE WHEN GT_HAS_INTEGRATED_CASHIER = 0 THEN --Only gaming table
                        CASE WHEN CM_CURRENCY_ISO_CODE IS NULL
                               OR (CM_CURRENCY_ISO_CODE <> @_CHIPS_ISO_CODE
                              AND  CM_CAGE_CURRENCY_TYPE NOT IN (@_CHIPS_RE, @_CHIPS_NR, @_CHIPS_COLOR) ) THEN
                             CASE WHEN CM_TYPE  = @_MOVEMENT_FILLER_IN            THEN -1 * ISNULL(CM_AUX_AMOUNT, CM_ADD_AMOUNT)
                                  WHEN CM_TYPE  = @_MOVEMENT_FILLER_OUT           THEN      ISNULL(CM_AUX_AMOUNT, CM_SUB_AMOUNT)
                                  WHEN CM_TYPE  = @_MOVEMENT_CAGE_CLOSE_SESSION   THEN      ISNULL(CM_AUX_AMOUNT, CM_INITIAL_BALANCE)
                                  ELSE 0
                             END
                             ELSE 0
                        END
                    WHEN GT_HAS_INTEGRATED_CASHIER = 1 THEN --Gaming table with cashier
                        CASE WHEN CM_TYPE   = @_CHIPS_MOVEMENT_PURCHASE_TOTAL THEN -1 * CM_ADD_AMOUNT
                             WHEN CM_TYPE   = @_CHIPS_MOVEMENT_SALES_TOTAL    THEN      CM_SUB_AMOUNT
                             ELSE 0
                        END
                    ELSE 0
               END
              )  AS CM_COLLECTED_AMOUNT
         , GTS_CLIENT_VISITS
         , AR_NAME
         , BK_NAME
         , CM_GAMING_TABLE_SESSION_ID
         , dbo.GT_Calculate_DROP(SUM(ISNULL(GTSC_OWN_SALES_AMOUNT, 0)), SUM(ISNULL(GTSC_EXTERNAL_SALES_AMOUNT, 0)), SUM(ISNULL(GTSC_COLLECTED_AMOUNT, 0)), GT_HAS_INTEGRATED_CASHIER) AS DROP_BY_CURRENCY
         , CM_CURRENCY_ISO_CODE
         , dbo.GT_Calculate_DROP(SUM(ISNULL(GTS_OWN_SALES_AMOUNT, 0)), SUM(ISNULL(GTS_EXTERNAL_SALES_AMOUNT, 0)), SUM(ISNULL(GTS_COLLECTED_AMOUNT, 0)), GT_HAS_INTEGRATED_CASHIER) AS DROP_LOCAL_CURRENCY
   FROM         CASHIER_MOVEMENTS
  INNER   JOIN  CASHIER_SESSIONS                   ON CS_SESSION_ID               = CM_SESSION_ID
  INNER   JOIN  GAMING_TABLES_SESSIONS             ON CS_SESSION_ID               = GTS_CASHIER_SESSION_ID
   LEFT   JOIN  GAMING_TABLES_SESSIONS_BY_CURRENCY ON GTS_GAMING_TABLE_SESSION_ID = GTSC_GAMING_TABLE_SESSION_ID AND GTSC_TYPE <> @_CHIPS_COLOR
   LEFT   JOIN  GUI_USERS                          ON GU_USER_ID                  = CS_USER_ID
   LEFT   JOIN  GAMING_TABLES                      ON GT_GAMING_TABLE_ID          = GTS_GAMING_TABLE_ID
   LEFT   JOIN  GAMING_TABLES_TYPES                ON GTT_GAMING_TABLE_TYPE_ID    = GT_TYPE_ID
   LEFT   JOIN  AREAS                              ON GT_AREA_ID                  = AR_AREA_ID
   LEFT   JOIN  BANKS                              ON GT_BANK_ID                  = BK_BANK_ID
   LEFT   JOIN  CASHIER_TERMINALS                  ON GT_CASHIER_ID               = CT_CASHIER_ID
 WHERE   CM_DATE >= @_DATE_FROM
   AND   CM_DATE <= @_DATE_TO
   AND   (GT_ENABLED = CASE WHEN @_ENABLED = -1 THEN GT_ENABLED ELSE @_ENABLED END)
   AND   (CS_STATUS  = CASE WHEN @_STATUS  = -1 THEN CS_STATUS  ELSE @_STATUS  END)
   AND   (GT_AREA_ID = CASE WHEN @_AREA    = 0  THEN GT_AREA_ID ELSE @_AREA    END)
   AND   (GT_BANK_ID = CASE WHEN @_BANK    = 0  THEN GT_BANK_ID ELSE @_BANK    END)
   AND   (GT_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE ))
  GROUP BY  CM_SESSION_ID
      , CS_NAME
      , CS_STATUS
      , GT_NAME
      , GTT_NAME
      , GT_HAS_INTEGRATED_CASHIER
      , CT_NAME
      , GU_USERNAME
      , CS_OPENING_DATE
      , CS_CLOSING_DATE
      , GTS_CLIENT_VISITS
      , AR_NAME
      , BK_NAME
      , CM_GAMING_TABLE_SESSION_ID
      , CM_CURRENCY_ISO_CODE
     ORDER BY CS_OPENING_DATE
END

END -- END PROCEDURE GT_Session_Information
GO

-- PERMISSIONS
GRANT EXECUTE ON [dbo].[GT_Session_Information] TO [wggui] WITH GRANT OPTION
GO
