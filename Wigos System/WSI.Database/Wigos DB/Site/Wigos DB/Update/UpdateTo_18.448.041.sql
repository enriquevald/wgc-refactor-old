﻿/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 447;

SET @New_ReleaseId = 448;

SET @New_ScriptName = N'2018-06-19 - UpdateTo_18.448.041.sql';
SET @New_Description = N'New release v03.008.0014'; 

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO


/*********************************************************************************************************/

/**** GENERAL PARAM *****/


/**** VIEW *****/


/******* TABLES  *******/


/******* INDEXES *******/


/******* TRIGERS *******/


/******* RECORDS *******/


/******* PROCEDURES *******/
-- 19-JUN-2018 FJC    Bug: WIGOS-13040 When pressing Reset button on Cash movements coming from Chip purchase/sale report, two points appear

IF OBJECT_ID (N'dbo.GT_Chips_Operations', N'P') IS NOT NULL
    DROP PROCEDURE dbo.GT_Chips_Operations;
GO

CREATE PROCEDURE [dbo].[GT_Chips_Operations]
(
    @pDateFrom              DATETIME
  , @pDateTo                DATETIME
  , @pStatus                INTEGER
  , @pArea                  NVARCHAR(50)
  , @pBank                  NVARCHAR(50)
  , @pOnlyTables            INTEGER
  , @pCashierGroupName      NVARCHAR(50)
  , @pValidTypes            NVARCHAR(50)
  , @pSelectedCurrency      NVARCHAR(3)
  , @pSelectedOptionForm    INTEGER
)
AS
BEGIN
  ----------------------------------------------------------------------------------------------------------------
  DECLARE @_DATE_FROM                  AS DATETIME
  DECLARE @_DATE_TO                    AS DATETIME
  DECLARE @_STATUS                     AS INTEGER
  DECLARE @_AREA                       AS NVARCHAR(50)
  DECLARE @_BANK                       AS NVARCHAR(50)
  DECLARE @_DELIMITER                  AS CHAR(1)
  DECLARE @_ONLY_TABLES                AS INTEGER
  DECLARE @_CASHIERS_NAME              AS NVARCHAR(50)
  DECLARE @_TYPES_TABLE                TABLE(SST_ID INT, SST_VALUE NVARCHAR(50)) -- TO STORE THE VALID GAMING TABLE TYPES
  DECLARE @_TYPE_CHIPS_SALE_TOTAL      AS INTEGER
  DECLARE @_TYPE_CHIPS_PURCHASE_TOTAL  AS INTEGER
  DECLARE @_CHIP_RE                    AS INTEGER
  DECLARE @_CHIP_NR                    AS INTEGER
  DECLARE @_SELECTED_CURRENCY          AS NVARCHAR(3)
  DECLARE @_SELECTED_OPTION_FORM       AS INTEGER
  DECLARE @_TYPE_CHIPS_SALE_TOTAL_EXCHANGE            AS INTEGER
  DECLARE @_TYPE_CHIPS_PURCHASE_TOTAL_EXCHANGE        AS INTEGER
  DECLARE @_TYPE_CHIPS_SALE_REMAINING_AMOUNT          AS INTEGER
  DECLARE @_TYPE_CHIPS_SALE_CONSUMED_REMAINING_AMOUNT AS INTEGER
  ----------------------------------------------------------------------------------------------------------------

  -- Initialization --
  SET @_TYPE_CHIPS_SALE_TOTAL              = 303
  SET @_TYPE_CHIPS_PURCHASE_TOTAL          = 304
  SET @_TYPE_CHIPS_SALE_TOTAL_EXCHANGE     = 309
  SET @_TYPE_CHIPS_PURCHASE_TOTAL_EXCHANGE = 310
  SET @_TYPE_CHIPS_SALE_REMAINING_AMOUNT          = 312
  SET @_TYPE_CHIPS_SALE_CONSUMED_REMAINING_AMOUNT = 313

  SET @_DELIMITER            =   ','
  SET @_DATE_FROM            =   @pDateFrom
  SET @_DATE_TO              =   @pDateTo
  SET @_STATUS               =   ISNULL(@pStatus, -1)
  SET @_AREA                 =   ISNULL(@pArea, '')
  SET @_BANK                 =   ISNULL(@pBank, '')
  SET @_ONLY_TABLES          =   ISNULL(@pOnlyTables, 1)
  SET @_CHIP_RE              =   1001
  SET @_CHIP_NR              =   1002
  SET @_SELECTED_CURRENCY    = @pSelectedCurrency
  SET @_SELECTED_OPTION_FORM = @pSelectedOptionForm

  IF ISNULL(@_CASHIERS_NAME,'') = '' BEGIN
    SET @_CASHIERS_NAME  = '---CASHIER---'
  END

  ----------------------------------------------------------------------------------------------------------------
  -- CHECK DATE PARAMETERS
  IF @_DATE_FROM IS NULL
  BEGIN
     -- IF DATE FROM IS NULL, USER FIRST POSIBLE DATE
     SET @_DATE_FROM = CAST('' AS DATETIME)
  END

  IF @_DATE_TO IS NULL
  BEGIN
     -- IF DATE TO IS NULL, QUERY UNTIL CURRENT DATE
     SET @_DATE_TO = CAST(GETDATE() AS DATETIME)
  END

  -- ASSIGN TYPES PARAMETER INTO TABLE
  INSERT INTO @_TYPES_TABLE SELECT * FROM dbo.SplitStringIntoTable(@pValidTypes, @_DELIMITER, DEFAULT)

  ----------------------------------------------------------------------------------------------------------------
  -- MAIN QUERY
  -- Total to local isocode
  IF @_SELECTED_OPTION_FORM = 0
  BEGIN
     SELECT   0 AS TYPE_SESSION
            , CS_SESSION_ID AS SESSION_ID
            , GT_NAME AS GT_NAME
            , CS_OPENING_DATE AS SESSION_DATE
            , GTT_NAME AS GTT_NAME
            , ISNULL(SUM(GTS_TOTAL_SALES_AMOUNT)   , 0) AS GTS_TOTAL_SALES_AMOUNT
            , ISNULL(SUM(GTS_TOTAL_PURCHASE_AMOUNT), 0) AS GTS_TOTAL_PURCHASE_AMOUNT
            , ISNULL(SUM(GTS_TOTAL_SALES_AMOUNT)   , 0) - ISNULL(SUM(GTS_TOTAL_PURCHASE_AMOUNT), 0) AS DELTA
            , (SELECT ISNULL(SUM(CASE WHEN (CM_TYPE = @_TYPE_CHIPS_SALE_REMAINING_AMOUNT) THEN CM_SUB_AMOUNT-CM_ADD_AMOUNT
                                    WHEN (CM_TYPE = @_TYPE_CHIPS_SALE_CONSUMED_REMAINING_AMOUNT) THEN CM_SUB_AMOUNT-CM_ADD_AMOUNT
                                    ELSE 0 END ), 0)
               FROM cashier_movements WHERE cm_session_id = cs_session_id ) AS REMINING
						, ''					 AS CURRENCY_ISO_CODE
						, MAX(CS_NAME) AS SESSION_NAME
       INTO   #CHIPS_OPERATIONS_TABLE_OPTION_1
       FROM   CASHIER_SESSIONS
       LEFT   JOIN GAMING_TABLES_SESSIONS ON GTS_CASHIER_SESSION_ID   = CS_SESSION_ID
      INNER   JOIN GAMING_TABLES GT       ON GTS_GAMING_TABLE_ID      = GT_GAMING_TABLE_ID
      INNER   JOIN GAMING_TABLES_TYPES    ON GTT_GAMING_TABLE_TYPE_ID = GT_TYPE_ID
      WHERE   GT_ENABLED = CASE WHEN @_STATUS = -1 THEN GT_ENABLED ELSE @_STATUS END
        AND   GT_AREA_ID   = CASE WHEN @_AREA   = '' THEN GT_AREA_ID ELSE @_AREA END
        AND   GT_BANK_ID   = CASE WHEN @_BANK   = '' THEN GT_BANK_ID ELSE @_BANK END
        AND   GT_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
        AND   CS_OPENING_DATE >= @_DATE_FROM
        AND   CS_OPENING_DATE <  @_DATE_TO
        AND   GT_HAS_INTEGRATED_CASHIER = 1
        AND   (ISNULL(GTS_TOTAL_SALES_AMOUNT, 0) + ISNULL(GTS_TOTAL_PURCHASE_AMOUNT, 0)) <> 0
    GROUP BY GT_NAME, GTT_NAME, CS_SESSION_ID, CS_OPENING_DATE
    ORDER BY GTT_NAME

    -- Check if cashiers must be visible
    IF @_ONLY_TABLES = 0
    BEGIN
      -- Select and join data to show cashiers
      -- Adding cashiers without gaming tables
      INSERT INTO   #CHIPS_OPERATIONS_TABLE_OPTION_1
           SELECT   1 AS TYPE_SESSION
                  , CM_SESSION_ID AS SESSION_ID
                  , CT_NAME as GT_NAME
                  , CS_OPENING_DATE AS SESSION_DATE
                  , @_CASHIERS_NAME as GTT_NAME
                  , ISNULL(SUM(CASE WHEN (CM_TYPE = @_TYPE_CHIPS_SALE_TOTAL) THEN CM_SUB_AMOUNT
                                    WHEN (CM_TYPE = @_TYPE_CHIPS_PURCHASE_TOTAL) THEN CM_SUB_AMOUNT
                                    WHEN (CM_TYPE = @_TYPE_CHIPS_SALE_TOTAL_EXCHANGE) THEN CM_SUB_AMOUNT
                                    WHEN (CM_TYPE = @_TYPE_CHIPS_PURCHASE_TOTAL_EXCHANGE) THEN CM_SUB_AMOUNT
                                    ELSE 0 END), 0) AS GTS_TOTAL_SALES_AMOUNT
                  , ISNULL(SUM(CASE WHEN (CM_TYPE = @_TYPE_CHIPS_SALE_TOTAL) THEN CM_ADD_AMOUNT
                                    WHEN (CM_TYPE = @_TYPE_CHIPS_PURCHASE_TOTAL) THEN CM_ADD_AMOUNT
                                    WHEN (CM_TYPE = @_TYPE_CHIPS_SALE_TOTAL_EXCHANGE) THEN CM_ADD_AMOUNT
                                    WHEN (CM_TYPE = @_TYPE_CHIPS_PURCHASE_TOTAL_EXCHANGE) THEN CM_ADD_AMOUNT
                                    ELSE 0 END), 0) AS GTS_TOTAL_PURCHASE_AMOUNT
                  , ISNULL(SUM(CASE WHEN (CM_TYPE = @_TYPE_CHIPS_SALE_TOTAL) THEN CM_SUB_AMOUNT
                                    WHEN (CM_TYPE = @_TYPE_CHIPS_PURCHASE_TOTAL) THEN CM_SUB_AMOUNT
                                    WHEN (CM_TYPE = @_TYPE_CHIPS_SALE_TOTAL_EXCHANGE) THEN CM_SUB_AMOUNT
                                    WHEN (CM_TYPE = @_TYPE_CHIPS_PURCHASE_TOTAL_EXCHANGE) THEN CM_SUB_AMOUNT
                                    ELSE 0 END ), 0) 
                  - ISNULL(SUM(CASE WHEN (CM_TYPE = @_TYPE_CHIPS_SALE_TOTAL) THEN CM_ADD_AMOUNT
                                    WHEN (CM_TYPE = @_TYPE_CHIPS_PURCHASE_TOTAL) THEN CM_ADD_AMOUNT
                                    WHEN (CM_TYPE = @_TYPE_CHIPS_SALE_TOTAL_EXCHANGE) THEN CM_ADD_AMOUNT
                                    WHEN (CM_TYPE = @_TYPE_CHIPS_PURCHASE_TOTAL_EXCHANGE) THEN CM_ADD_AMOUNT
                                    ELSE 0 END ), 0) AS DELTA
                  , ISNULL(SUM(CASE WHEN (CM_TYPE = @_TYPE_CHIPS_SALE_REMAINING_AMOUNT) THEN CM_SUB_AMOUNT-CM_ADD_AMOUNT
                                    WHEN (CM_TYPE = @_TYPE_CHIPS_SALE_CONSUMED_REMAINING_AMOUNT) THEN CM_SUB_AMOUNT-CM_ADD_AMOUNT
                                    ELSE 0 END ), 0) AS REMINING
									, ''					 AS CURRENCY_ISO_CODE
									, MAX(CS_NAME) AS SESSION_NAME
             FROM   CASHIER_MOVEMENTS
            INNER   JOIN CASHIER_SESSIONS        ON CS_SESSION_ID          = CM_SESSION_ID
            INNER   JOIN CASHIER_TERMINALS       ON CM_CASHIER_ID          = CT_CASHIER_ID
             LEFT   JOIN  GAMING_TABLES_SESSIONS ON GTS_CASHIER_SESSION_ID = CM_SESSION_ID
            WHERE   GTS_CASHIER_SESSION_ID IS NULL -- We want the chips operations that are from cashiers that are not gambling tables
								 -- 303 = CHIPS_SALE_TOTAL; 304 = CHIPS_PURCHASE_TOTAL; 309 = CHIPS_SALE_TOTAL_EXCHANGE; 310 = CHIPS_PURCHASE_TOTAL_EXCHANGE
                 -- 312 = CHIPS_SALE_REMAINING_AMOUNT;  313 = CHIPS_SALE_REMAINING_AMOUNT
              AND   CM_TYPE IN (@_TYPE_CHIPS_SALE_TOTAL, @_TYPE_CHIPS_PURCHASE_TOTAL, @_TYPE_CHIPS_SALE_TOTAL_EXCHANGE, @_TYPE_CHIPS_PURCHASE_TOTAL_EXCHANGE,
                                @_TYPE_CHIPS_SALE_REMAINING_AMOUNT, @_TYPE_CHIPS_SALE_CONSUMED_REMAINING_AMOUNT)
              AND   CM_DATE >= @_DATE_FROM
              AND   CM_DATE <  @_DATE_TO
            GROUP   BY CT_NAME, CM_SESSION_ID, CS_OPENING_DATE
    END
    -- Select results
    SELECT * FROM #CHIPS_OPERATIONS_TABLE_OPTION_1 ORDER BY GTT_NAME,GT_NAME

    -- DROP TEMPORARY TABLE
    DROP TABLE #CHIPS_OPERATIONS_TABLE_OPTION_1
  END

  -- By selected currency
  IF @_SELECTED_OPTION_FORM = 1
  BEGIN
   SELECT   0 AS TYPE_SESSION
          , CS_SESSION_ID AS SESSION_ID
          , GT_NAME AS GT_NAME
          , CS_OPENING_DATE AS SESSION_DATE
          , GTT_NAME AS GTT_NAME
          , ISNULL(SUM(GTSC_TOTAL_SALES_AMOUNT)   , 0) AS GTS_TOTAL_SALES_AMOUNT
          , ISNULL(SUM(GTSC_TOTAL_PURCHASE_AMOUNT), 0) AS GTS_TOTAL_PURCHASE_AMOUNT
          , ISNULL(SUM(GTSC_TOTAL_SALES_AMOUNT)   , 0) - ISNULL(SUM(GTSC_TOTAL_PURCHASE_AMOUNT), 0) AS DELTA
          , (SELECT ISNULL(SUM(CASE WHEN (CM_TYPE = @_TYPE_CHIPS_SALE_REMAINING_AMOUNT) THEN CM_SUB_AMOUNT-CM_ADD_AMOUNT
                                    WHEN (CM_TYPE = @_TYPE_CHIPS_SALE_CONSUMED_REMAINING_AMOUNT) THEN CM_SUB_AMOUNT-CM_ADD_AMOUNT
                                    ELSE 0 END ), 0)
               FROM cashier_movements WHERE cm_session_id = cs_session_id ) AS REMINING
          , GTSC_ISO_CODE AS CURRENCY_ISO_CODE
					, MAX(CS_NAME) AS SESSION_NAME
     INTO   #CHIPS_OPERATIONS_TABLE_OPTION_2
     FROM   CASHIER_SESSIONS
     LEFT   JOIN GAMING_TABLES_SESSIONS             ON GTS_CASHIER_SESSION_ID   = CS_SESSION_ID
     LEFT   JOIN GAMING_TABLES_SESSIONS_BY_CURRENCY ON GTS_GAMING_TABLE_SESSION_ID = GTSC_GAMING_TABLE_SESSION_ID
    INNER   JOIN GAMING_TABLES GT                   ON GTS_GAMING_TABLE_ID      = GT_GAMING_TABLE_ID
    INNER   JOIN GAMING_TABLES_TYPES                ON GTT_GAMING_TABLE_TYPE_ID = GT_TYPE_ID
    WHERE   GT_ENABLED = CASE WHEN @_STATUS = -1 THEN GT_ENABLED ELSE @_STATUS END
      AND   GT_AREA_ID = CASE WHEN @_AREA   = '' THEN GT_AREA_ID ELSE @_AREA END
      AND   GT_BANK_ID = CASE WHEN @_BANK   = '' THEN GT_BANK_ID ELSE @_BANK END
      AND   GT_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
      AND   CS_OPENING_DATE >= @_DATE_FROM
      AND   CS_OPENING_DATE <  @_DATE_TO
      AND   GT_HAS_INTEGRATED_CASHIER = 1
      AND   GTSC_TYPE IN (@_CHIP_RE, @_CHIP_NR)
      AND   GTSC_ISO_CODE = @_SELECTED_CURRENCY
      AND   (ISNULL(GTS_TOTAL_SALES_AMOUNT, 0) + ISNULL(GTS_TOTAL_PURCHASE_AMOUNT, 0)) <> 0
    GROUP   BY GT_NAME, GTT_NAME, CS_SESSION_ID, CS_OPENING_DATE, GTSC_ISO_CODE
    ORDER   BY GTT_NAME

  -- Check if cashiers must be visible
  IF @_ONLY_TABLES = 0
  BEGIN
    -- Select and join data to show cashiers
    -- Adding cashiers without gaming tables

    INSERT INTO #CHIPS_OPERATIONS_TABLE_OPTION_2
      SELECT   1 AS TYPE_SESSION
             , CM_SESSION_ID AS SESSION_ID
             , CT_NAME as GT_NAME
             , CS_OPENING_DATE AS SESSION_DATE
             , @_CASHIERS_NAME as GTT_NAME
             , ISNULL(SUM(CASE WHEN (CM_TYPE = @_TYPE_CHIPS_SALE_TOTAL) THEN CM_SUB_AMOUNT
                               WHEN (CM_TYPE = @_TYPE_CHIPS_SALE_TOTAL_EXCHANGE) THEN CM_INITIAL_BALANCE 
                               ELSE 0 END), 0) AS GTS_TOTAL_SALES_AMOUNT
             , ISNULL(SUM(CASE WHEN (CM_TYPE = @_TYPE_CHIPS_PURCHASE_TOTAL) THEN CM_ADD_AMOUNT
                               WHEN (CM_TYPE = @_TYPE_CHIPS_PURCHASE_TOTAL_EXCHANGE) THEN CM_INITIAL_BALANCE 
                               ELSE 0 END), 0) AS GTS_TOTAL_PURCHASE_AMOUNT

             -- CALCULATE DELTA
             , ISNULL(SUM(CASE WHEN (CM_TYPE = @_TYPE_CHIPS_SALE_TOTAL) THEN CM_SUB_AMOUNT
                               WHEN (CM_TYPE = @_TYPE_CHIPS_SALE_TOTAL_EXCHANGE) THEN CM_INITIAL_BALANCE 
                               ELSE 0 END), 0)
             - ISNULL(SUM(CASE WHEN (CM_TYPE = @_TYPE_CHIPS_PURCHASE_TOTAL) THEN CM_ADD_AMOUNT
                               WHEN (CM_TYPE = @_TYPE_CHIPS_PURCHASE_TOTAL_EXCHANGE) THEN CM_INITIAL_BALANCE 
                               ELSE 0 END), 0) AS DELTA

             , ISNULL(SUM(CASE WHEN (CM_TYPE = @_TYPE_CHIPS_SALE_REMAINING_AMOUNT) THEN CM_SUB_AMOUNT-CM_ADD_AMOUNT
                               WHEN (CM_TYPE = @_TYPE_CHIPS_SALE_CONSUMED_REMAINING_AMOUNT) THEN CM_SUB_AMOUNT-CM_ADD_AMOUNT
                               ELSE 0 END ), 0) AS REMINING

             , CM_CURRENCY_ISO_CODE AS CURRENCY_ISO_CODE
						 , MAX(CS_NAME) AS SESSION_NAME
        FROM   CASHIER_MOVEMENTS
       INNER   JOIN CASHIER_SESSIONS                   ON CS_SESSION_ID               = CM_SESSION_ID
       INNER   JOIN CASHIER_TERMINALS                  ON CM_CASHIER_ID               = CT_CASHIER_ID
        LEFT   JOIN GAMING_TABLES_SESSIONS             ON GTS_CASHIER_SESSION_ID      = CM_SESSION_ID
        LEFT   JOIN GAMING_TABLES_SESSIONS_BY_CURRENCY ON GTS_GAMING_TABLE_SESSION_ID = GTSC_GAMING_TABLE_SESSION_ID
       WHERE   GTS_CASHIER_SESSION_ID IS NULL -- We want the chips operations that are from cashiers that are not gambling tables
              -- 303 = CHIPS_SALE_TOTAL; 304 = CHIPS_PURCHASE_TOTAL; 309 = CHIPS_SALE_TOTAL_EXCHANGE; 310 = CHIPS_PURCHASE_TOTAL_EXCHANGE
							-- 312 = CHIPS_SALE_REMAINING_AMOUNT;  313 = CHIPS_SALE_REMAINING_AMOUNT
        AND   CM_TYPE IN (@_TYPE_CHIPS_SALE_TOTAL, @_TYPE_CHIPS_PURCHASE_TOTAL, @_TYPE_CHIPS_SALE_TOTAL_EXCHANGE, @_TYPE_CHIPS_PURCHASE_TOTAL_EXCHANGE,
                          @_TYPE_CHIPS_SALE_REMAINING_AMOUNT, @_TYPE_CHIPS_SALE_CONSUMED_REMAINING_AMOUNT)
        AND   CM_DATE >= @_DATE_FROM
        AND   CM_DATE <  @_DATE_TO
        AND   CM_CURRENCY_ISO_CODE = @_SELECTED_CURRENCY
      GROUP   BY CT_NAME, CM_SESSION_ID, CS_OPENING_DATE, CM_CURRENCY_ISO_CODE
    END

    -- Select results
    SELECT * FROM #CHIPS_OPERATIONS_TABLE_OPTION_2 ORDER BY GTT_NAME,GT_NAME

    -- DROP TEMPORARY TABLE
    DROP TABLE #CHIPS_OPERATIONS_TABLE_OPTION_2
 END

END  -- END PROCEDURE
GO

-- PERMISSIONS
GRANT EXECUTE ON [dbo].[GT_Chips_Operations] TO [wggui] WITH GRANT OPTION
GO

/******* TRIGGERS *******/



