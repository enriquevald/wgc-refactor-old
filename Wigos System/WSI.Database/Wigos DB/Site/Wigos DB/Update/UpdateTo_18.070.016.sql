/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 69;

SET @New_ReleaseId = 70;
SET @New_ScriptName = N'UpdateTo_18.070.016.sql';
SET @New_Description = N'New Index for ACCOUNT_OPERATIONS. General Params for Peru Frames.';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** INDEXES ******/

IF  NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[account_operations]') AND name = N'IX_ao_code_date_account')
  CREATE NONCLUSTERED INDEX [IX_ao_code_date_account] ON [dbo].[account_operations] 
  (
	  [ao_code] ASC,
	  [ao_datetime] ASC,
	  [ao_account_id] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]


/****** RECORDS ******/

--
-- More GeneralParams for Peru (ExternalProtocol)
--

IF EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'ExternalProtocol' AND GP_SUBJECT_KEY ='RemoteURL')
  DELETE
    FROM [dbo].[general_params]
   WHERE [gp_group_key]   = 'ExternalProtocol'
     AND [gp_subject_key] = 'RemoteURL'

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'ExternalProtocol' AND GP_SUBJECT_KEY ='E-Frame.RemoteURL')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key]
             ,[gp_subject_key]
             ,[gp_key_value])
       VALUES
             ('ExternalProtocol'
             ,'E-Frame.RemoteURL'
             ,'');

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'ExternalProtocol' AND GP_SUBJECT_KEY ='T-Frame.RemoteURL')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key]
             ,[gp_subject_key]
             ,[gp_key_value])
       VALUES
             ('ExternalProtocol'
             ,'T-Frame.RemoteURL'
             ,'');

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'ExternalProtocol' AND GP_SUBJECT_KEY ='Login.Username')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key]
             ,[gp_subject_key]
             ,[gp_key_value])
       VALUES
             ('ExternalProtocol'
             ,'Login.Username'
             ,'');

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'ExternalProtocol' AND GP_SUBJECT_KEY ='Login.Password')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key]
             ,[gp_subject_key]
             ,[gp_key_value])
       VALUES
             ('ExternalProtocol'
             ,'Login.Password'
             ,'');

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'ExternalProtocol' AND GP_SUBJECT_KEY ='Login.Key')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key]
             ,[gp_subject_key]
             ,[gp_key_value])
       VALUES
             ('ExternalProtocol'
             ,'Login.Key'
             ,'');

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'ExternalProtocol' AND GP_SUBJECT_KEY ='Login.IV')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key]
             ,[gp_subject_key]
             ,[gp_key_value])
       VALUES
             ('ExternalProtocol'
             ,'Login.IV'
             ,'');

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'ExternalProtocol' AND GP_SUBJECT_KEY ='Data.Key')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key]
             ,[gp_subject_key]
             ,[gp_key_value])
       VALUES
             ('ExternalProtocol'
             ,'Data.Key'
             ,'');

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'ExternalProtocol' AND GP_SUBJECT_KEY ='Data.IV')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key]
             ,[gp_subject_key]
             ,[gp_key_value])
       VALUES
             ('ExternalProtocol'
             ,'Data.IV'
             ,'');

GO
