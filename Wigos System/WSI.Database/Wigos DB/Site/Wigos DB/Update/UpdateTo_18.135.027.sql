/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 134;

SET @New_ReleaseId = 135;
SET @New_ScriptName = N'UpdateTo_18.135.027.sql';
SET @New_Description = N'Update Update_AccountDocuments';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END
 
/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

--------------------------------------------------------------------------------
-- Copyright � 2013 Win Systems International
--------------------------------------------------------------------------------
--
--   MODULE NAME: Update_AccountDocuments.sql
--
--   DESCRIPTION: Update Account Documents in Site
--
--        AUTHOR: Jos� Mart�nez L�pez
--
-- CREATION DATE: 02-JUL-2013
--
-- REVISION HISTORY:
--
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 02-JUL-2013 JML    First release.  
-------------------------------------------------------------------------------- 

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Update_AccountDocuments]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Update_AccountDocuments]
GO

CREATE PROCEDURE Update_AccountDocuments
                 @pAccountId BIGINT
               , @pCreated DATETIME
               , @pModified DATETIME
               , @pData VARBINARY(MAX)
               , @pSequenceId BIGINT
AS
BEGIN   
IF EXISTS (SELECT 1 FROM ACCOUNT_DOCUMENTS WHERE AD_ACCOUNT_ID = @pAccountId)
  BEGIN   
	  UPDATE   ACCOUNT_DOCUMENTS
       SET   AD_CREATED        = @pCreated				
           , AD_MODIFIED       = @pModified		  
           , AD_DATA           = @pData  
           , AD_MS_SEQUENCE_ID = @pSequenceId	  
     WHERE   AD_ACCOUNT_ID     = @pAccountId 

  END
ELSE
  BEGIN   
  
    INSERT INTO   ACCOUNT_DOCUMENTS
                ( AD_ACCOUNT_ID
                , AD_CREATED
                , AD_MODIFIED
                , AD_DATA
                , AD_MS_SEQUENCE_ID)
         VALUES ( @pAccountId 
                , @pCreated
                , @pModified
                , @pData
                , @pSequenceId )

  END
END -- Update_AccountDocuments
GO
  