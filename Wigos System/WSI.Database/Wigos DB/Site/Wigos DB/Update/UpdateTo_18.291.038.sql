/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 290;

SET @New_ReleaseId = 291;
SET @New_ScriptName = N'UpdateTo_18.291.038.sql';
SET @New_Description = N'New tables for space; New alarms';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/******* TABLES  *******/

-- ELP01_SPACE_REQUESTS
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[elp01_space_requests]') AND type in (N'U'))
DROP TABLE elp01_space_requests
GO

CREATE TABLE [dbo].[elp01_space_requests](
	[es_account_id]       [bigint] NOT NULL,
	[es_transaction_id]   [nvarchar](50) NOT NULL,
  [es_credit_type]      [int]	NOT NULL,
  [es_amount]           [money] NOT NULL,
	[es_group_id]         [int] NOT NULL,
	[es_timestamp]        [timestamp] NOT NULL,
 CONSTRAINT [PK_elp01_space_requests] PRIMARY KEY CLUSTERED 
(
	[es_account_id] ASC, [es_transaction_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/******* INDEXES *******/

/******* RECORDS *******/

IF NOT EXISTS (SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'TITO' AND GP_SUBJECT_KEY = 'Tickets.MaxMinutesAllowedToValidate')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('TITO', 'Tickets.MaxMinutesAllowedToValidate', '2880')
GO

IF NOT EXISTS (SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.DrawTicket' AND GP_SUBJECT_KEY = 'ShowDocumentId')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Cashier.DrawTicket', 'ShowDocumentId', '0')
GO

IF EXISTS(SELECT * FROM GENERAL_PARAMS WHERE gp_group_key = 'SystemMode' AND GP_SUBJECT_KEY = 'Site')
  UPDATE GENERAL_PARAMS SET gp_group_key = 'Site', GP_SUBJECT_KEY = 'SystemMode' WHERE gp_group_key = 'SystemMode' AND GP_SUBJECT_KEY = 'Site'
GO

-- TerminalSystem_GameMeterBigIncrement_ByMeter = 0x000A0000,     // Warn => 0x000A0000 + MeterCode
-- TerminalSystem_MachineMeterBigIncrement_ByMeter = 0x000B0000,  // Warn => 0x000B0000 + MeterCode

-- Meters:
-- 0x0005 Games played / Jugadas
-- 0x0000 Total coin in credits / Monto jugado
-- 0x0006 Games won / Jugadas ganadas
-- 0x0001 Total coin out credits / Monto ganado
-- 0x0002 Total jackpot credits / Total cr�ditos jackpot
-- 0x1000 Total progressive jackpot credits / Total cr�ditos jackpot progresivo


CREATE TABLE #ALARMS (ALARM_CODE INT NOT NULL, EN NVARCHAR(350), SP NVARCHAR(350))
 INSERT INTO #ALARMS (ALARM_CODE,EN,SP) VALUES (CONVERT(INT, 0x000A0000) + CONVERT(INT, 0x0005), 'Big increment in MM: Games played'                      , 'Salto de contador en MM: Jugadas')
 INSERT INTO #ALARMS (ALARM_CODE,EN,SP) VALUES (CONVERT(INT, 0x000A0000) + CONVERT(INT, 0x0000), 'Big increment in MM: Total coin in credits'             , 'Salto de contador en MM: Monto jugado')
 INSERT INTO #ALARMS (ALARM_CODE,EN,SP) VALUES (CONVERT(INT, 0x000A0000) + CONVERT(INT, 0x0006), 'Big increment in MM: Games won'                         , 'Salto de contador en MM: Jugadas ganadas')
 INSERT INTO #ALARMS (ALARM_CODE,EN,SP) VALUES (CONVERT(INT, 0x000A0000) + CONVERT(INT, 0x0001), 'Big increment in MM: Total coin out credits'            , 'Salto de contador en MM: Monto ganado')
 INSERT INTO #ALARMS (ALARM_CODE,EN,SP) VALUES (CONVERT(INT, 0x000A0000) + CONVERT(INT, 0x0002), 'Big increment in MM: Total jackpot credits'             , 'Salto de contador en MM: Total cr�ditos jackpot')
 INSERT INTO #ALARMS (ALARM_CODE,EN,SP) VALUES (CONVERT(INT, 0x000A0000) + CONVERT(INT, 0x1000), 'Big increment in MM: Total prog. jackpot credits'       , 'Salto de contador en MM: Total cr�d. jackpot prog.')
                                                                                                       
 INSERT INTO #ALARMS (ALARM_CODE,EN,SP) VALUES (CONVERT(INT, 0x000B0000) + CONVERT(INT, 0x0005), 'Big increment in GM: Games played'                      , 'Salto de contador en GM: Jugadas')
 INSERT INTO #ALARMS (ALARM_CODE,EN,SP) VALUES (CONVERT(INT, 0x000B0000) + CONVERT(INT, 0x0000), 'Big increment in GM: Total coin in credits'             , 'Salto de contador en GM: Monto jugado')
 INSERT INTO #ALARMS (ALARM_CODE,EN,SP) VALUES (CONVERT(INT, 0x000B0000) + CONVERT(INT, 0x0006), 'Big increment in GM: Games won'                         , 'Salto de contador en GM: Jugadas ganadas')
 INSERT INTO #ALARMS (ALARM_CODE,EN,SP) VALUES (CONVERT(INT, 0x000B0000) + CONVERT(INT, 0x0001), 'Big increment in GM: Total coin out credits'            , 'Salto de contador en GM: Monto ganado')
 INSERT INTO #ALARMS (ALARM_CODE,EN,SP) VALUES (CONVERT(INT, 0x000B0000) + CONVERT(INT, 0x0002), 'Big increment in GM: Total jackpot credits'             , 'Salto de contador en GM: Total cr�ditos jackpot')
 INSERT INTO #ALARMS (ALARM_CODE,EN,SP) VALUES (CONVERT(INT, 0x000B0000) + CONVERT(INT, 0x1000), 'Big increment in GM: Total prog. jackpot credits'       , 'Salto de contador en GM: Total cr�d. jackpot prog.')

--DELETE from [alarm_catalog] where [alcg_alarm_code] IN (select alarm_code from #ALARMS)
--DELETE from [alarm_catalog_per_category] where [alcc_alarm_code] IN (select alarm_code from #ALARMS)

IF NOT EXISTS (SELECT [alcg_alarm_code] from [dbo].[alarm_catalog] WHERE [alcg_alarm_code] = CONVERT(INT, 0x000A0000) + CONVERT(INT, 0x0005) AND [alcg_language_id] = 10)
  INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) 
                         SELECT          ALARM_CODE,                 10,           0,          SP,                 SP,              1 FROM #ALARMS ORDER BY ALARM_CODE DESC
GO

IF NOT EXISTS (SELECT [alcg_alarm_code] from [dbo].[alarm_catalog] WHERE [alcg_alarm_code] = CONVERT(INT, 0x000A0000) + CONVERT(INT, 0x0005) AND [alcg_language_id] = 9)
  INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) 
                         SELECT          ALARM_CODE,                  9,           0,          EN,                 EN,              1 FROM #ALARMS ORDER BY ALARM_CODE DESC
GO

IF NOT EXISTS (SELECT [alcc_alarm_code] from [dbo].[alarm_catalog_per_category] WHERE [alcc_alarm_code] = CONVERT(INT, 0x000A0000) + CONVERT(INT, 0x0005) AND [alcc_category] = 1) 
  INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) 
                                       SELECT       ALARM_CODE,               1,           0,         GETDATE()  FROM #ALARMS
GO

DROP TABLE #ALARMS
GO

IF NOT EXISTS (SELECT [alcg_alarm_code] from [dbo].[alarm_catalog] WHERE [alcg_alarm_code] = 393261 AND [alcg_language_id] = 10)
BEGIN
  INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) 
    VALUES ( 393261, 10, 0, N'Ticket ID no coincide', N'Ticket ID no coincide', 1)
END
GO

IF NOT EXISTS (SELECT [alcg_alarm_code] from [dbo].[alarm_catalog] WHERE [alcg_alarm_code] = 393261 AND [alcg_language_id] = 9)
BEGIN
  INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) 
    VALUES ( 393261,  9, 0, N'Ticket ID mismatch', N'Ticket ID mismatch', 1)
END 
GO

IF NOT EXISTS (SELECT [alcg_alarm_code] from [dbo].[alarm_catalog] WHERE [alcg_alarm_code] = 393262 AND [alcg_language_id] = 10)
BEGIN
  INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) 
    VALUES ( 393262, 10, 0, N'No se puedo leer Ticket Info', N'No se puedo leer Ticket Info', 1)
END
GO

IF NOT EXISTS (SELECT [alcg_alarm_code] from [dbo].[alarm_catalog] WHERE [alcg_alarm_code] = 393262 AND [alcg_language_id] = 9)
BEGIN
  INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) 
    VALUES ( 393262,  9, 0, N'Can''t read Ticket Info', N'Can''t read Ticket Info', 1)
END 
GO

IF NOT EXISTS (SELECT [alcc_alarm_code] from [dbo].[alarm_catalog_per_category] WHERE [alcc_alarm_code] = 393261 AND [alcc_category] = 50) 
BEGIN
  INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) 
    VALUES ( 393261, 24, 0, GETDATE() )
END
GO


IF NOT EXISTS (SELECT [alcc_alarm_code] from [dbo].[alarm_catalog_per_category] WHERE [alcc_alarm_code] = 393262 AND [alcc_category] = 50) 
BEGIN
  INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) 
    VALUES ( 393262, 24, 0, GETDATE() )
END
GO

IF NOT EXISTS (SELECT [alcg_alarm_code] from [dbo].[alarm_catalog] WHERE [alcg_alarm_code] = 393263 AND [alcg_language_id] = 10)
BEGIN
  INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) 
    VALUES ( 393263, 10, 0, N'AFT No autorizada', N'AFT No autorizada', 1)
END
GO

IF NOT EXISTS (SELECT [alcg_alarm_code] from [dbo].[alarm_catalog] WHERE [alcg_alarm_code] = 393263 AND [alcg_language_id] = 9)
BEGIN
  INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) 
    VALUES ( 393263,  9, 0, N'AFT Not authorized', N'AFT Not authorized', 1)
END 
GO

IF NOT EXISTS (SELECT [alcc_alarm_code] from [dbo].[alarm_catalog_per_category] WHERE [alcc_alarm_code] = 393263 AND [alcc_category] = 24) 
BEGIN
  INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) 
    VALUES ( 393263, 24, 0, GETDATE() )
END
GO

IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='Interface.Coljuegos' AND GP_SUBJECT_KEY = 'RegisterEndOfTheDay')
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Interface.Coljuegos', 'RegisterEndOfTheDay', '0')
GO

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Intrusion' AND GP_SUBJECT_KEY ='DisableAFT')
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Intrusion', 'DisableAFT', 0); 

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Intrusion' AND GP_SUBJECT_KEY ='BlockMachine')
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Intrusion', 'BlockMachine', 0); 

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Intrusion' AND GP_SUBJECT_KEY ='BlockAccount')
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Intrusion', 'BlockAccount', 0); 
GO

IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cage' AND GP_SUBJECT_KEY = 'GamingTables.TransferCurrency.Enabled')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Cage', 'GamingTables.TransferCurrency.Enabled', '0')
GO

IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'TITO' AND GP_SUBJECT_KEY = 'Tickets.MaxDaysAllowedToVoid')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('TITO', 'Tickets.MaxDaysAllowedToVoid', '15')
GO

/******* PROCEDURES *******/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_Coljuegos_ReporteDiario]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_Coljuegos_ReporteDiario]
GO

CREATE PROCEDURE [dbo].[SP_Coljuegos_ReporteDiario]
  (@Date DATETIME) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  DECLARE @FechaReporte nvarchar(1024)
  DECLARE @NIT          nvarchar(1024)
  DECLARE @Contrato     nvarchar(1024)
  DECLARE @Codigo       nvarchar(1024)
      
  -- FechaReporte
  SELECT   @FechaReporte = CONVERT(VARCHAR(25), @Date, 112)

  -- NIT
  SELECT   @NIT = gp_key_value
    FROM   general_params
   WHERE   gp_group_key = 'Interface.Coljuegos'
     AND   gp_subject_key = 'Operador.NIT'
 
  -- Contrato
  SELECT   @Contrato = gp_key_value
    FROM   general_params
   WHERE   gp_group_key = 'Interface.Coljuegos'
     AND   gp_subject_key = 'Operador.Contrato.Numero'
     
  -- Codigo
  SELECT   @Codigo = gp_key_value
    FROM   general_params
   WHERE   gp_group_key = 'Interface.Coljuegos'
     AND   gp_subject_key = 'Establecimiento.Codigo'     

  -------------------
  -- ReporteDiario --
  -------------------
  SELECT   4             AS NumColumns
         , 'RI'          AS Indicador
         , @FechaReporte AS Col1
         , @NIT          AS Col2
         , (SELECT   gp_key_value
              FROM   general_params
             WHERE   gp_group_key = 'Interface.Coljuegos'
               AND   gp_subject_key = 'Fabricante.Clave') AS Col3
         , NULL          AS Col4
         , NULL          AS Col5
         , NULL          AS Col6
         , NULL          AS Col7
         , NULL          AS Col8
         , NULL          AS Col9
         , NULL          AS Col10
         , NULL          AS Col11
         , NULL          AS Col12
   UNION
  SELECT   3         AS NumColumns
         , 'RC'      AS Indicador
         , @Contrato AS Col1
         , @Codigo   AS Col2
         , NULL      AS Col3
         , NULL      AS Col4
         , NULL      AS Col5
         , NULL      AS Col6
         , NULL      AS Col7
         , NULL      AS Col8
         , NULL      AS Col9
         , NULL      AS Col10
         , NULL      AS Col11
         , NULL      AS Col12
   UNION
  SELECT   13                         AS NumColumns
         , 'RD'                       AS Indicador
         , WXM_NUC                    AS Col1
         , WXM_NUID                   AS Col2
         , WXM_SERIAL                 AS Col3
         , ROUND(WXM_COIN_IN, 0, 1)   AS Col4
         , ROUND(WXM_COIN_OUT, 0, 1)  AS Col5
         , ROUND(WXM_JACKPOT, 0, 1)   AS Col6
         , ROUND(WXM_HAND_PAID, 0, 1) AS Col7
         , ROUND(WXM_BILL_IN, 0, 1)   AS Col8
         , WXM_GAMES_PLAYED           AS Col9
         , WXM_PAYOUT                 AS Col10
         , RIGHT('00' + CAST(WXM_EVENT_ID AS VARCHAR), 2) AS Col11
         , CASE WHEN WXM_EVENT_ID > 0 THEN REPLACE(
            REPLACE(
             REPLACE(CONVERT(VARCHAR(19), CONVERT(DATETIME, wxm_datetime, 112), 126)
             , '-', '')
            , 'T', '')
           , ':', '')     ELSE '' END AS Col12
    FROM   WXP_002_MESSAGES
   WHERE   WXM_DATETIME >= @Date
     AND   WXM_DATETIME <  DATEADD(DAY,1,@Date)
   UNION
  SELECT   3         AS NumColumns
         , 'RE'      AS Indicador
         , @Contrato AS Col1
         , @Codigo   AS Col2
         , NUll      AS Col3
         , NULL      AS Col4
         , NULL      AS Col5
         , NULL      AS Col6
         , NULL      AS Col7
         , NULL      AS Col8
         , NULL      AS Col9
         , NULL      AS Col10
         , NULL      AS Col11
         , NULL      AS Col12
  UNION  
  SELECT   3             AS NumColumns
         , 'RF'          AS Indicador
         , @FechaReporte AS Col1
         , @NIT          AS Col2
         , NULL          AS Col3
         , NULL          AS Col4
         , NULL          AS Col5
         , NULL          AS Col6
         , NULL          AS Col7
         , NULL          AS Col8
         , NULL          AS Col9
         , NULL          AS Col10
         , NULL          AS Col11
         , NULL          AS Col12
                    
END

GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_GenerateColombiaEventRegister]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_GenerateColombiaEventRegister]
GO

CREATE PROCEDURE [dbo].[SP_GenerateColombiaEventRegister]
(
   @pTerminal Integer, 
   @pEventId Integer,
   @pEndDayRegisterDatetime DateTime = NULL
) 
AS

BEGIN

DECLARE @DefaultPayout AS INTEGER
DECLARE @Now as Datetime

set @Now = GETDATE()

--If Terminal payout is not defined, it uses the GP default payout
SELECT   @DefaultPayout = GP_KEY_VALUE
  FROM   GENERAL_PARAMS
 WHERE   GP_SUBJECT_KEY = 'TerminalDefaultPayout' AND GP_GROUP_KEY = 'PlayerTracking'

--Gets and pivots the specified terminal meters
    SELECT   TE_TERMINAL_ID
           , TSM_METER_CODE
           , CASE WHEN TSM_METER_CODE IN (0,1,2,3,11) THEN TSM_METER_VALUE/100.0 ELSE TSM_METER_VALUE END AS METER_VALUE
           , CASE WHEN TE_THEORETICAL_PAYOUT IS NOT NULL THEN TE_THEORETICAL_PAYOUT * 100 ELSE @DefaultPayout END AS TE_THEORETICAL_PAYOUT
           , TE_REGISTRATION_CODE-- AS NUC
           , TE_EXTERNAL_ID --AS NUID
           , TE_SERIAL_NUMBER --AS SERIAL
      INTO   #TEMP 
      FROM   TERMINALS
 LEFT JOIN   TERMINAL_SAS_METERS ON TSM_TERMINAL_ID = TE_TERMINAL_ID
     WHERE   TE_TERMINAL_ID = CASE WHEN @pTerminal <> 0 THEN  @pTerminal ELSE TE_TERMINAL_ID END 
       AND   TE_STATUS = CASE WHEN @pEventId <> 9 THEN 0 ELSE TE_STATUS END 
       AND   TE_TERMINAL_TYPE = 5

IF @pEndDayRegisterDatetime IS NOT NULL and @pEventId = 0
BEGIN
INSERT INTO   WXP_002_MESSAGES
            ( WXM_TERMINAL_ID
            , wxm_datetime
            , wxm_event_id
            , wxm_nuc
            , wxm_nuid
            , wxm_serial
            , wxm_coin_in
            , wxm_coin_out
            , wxm_jackpot
            , wxm_hand_paid
            , wxm_games_played
            , wxm_bill_in
            , wxm_payout
            )
            SELECT   TE_TERMINAL_ID 
                   , @pEndDayRegisterDatetime
                   , @pEventId
                   , TE_REGISTRATION_CODE
                   , TE_EXTERNAL_ID
                   , TE_SERIAL_NUMBER
                   , ISNULL([0],0)  AS COIN_IN
                   , ISNULL([1],0)  AS COIN_OUT
                   , ISNULL([2],0)  AS JACKPOT
                   , ISNULL([3],0)  AS HANDPAY
                   , ISNULL([5],0)  AS GAMES_PLAYED
                   , ISNULL([11],0) AS BILL_IN
                   , ISNULL(TE_THEORETICAL_PAYOUT, @DefaultPayout)
              FROM   #TEMP 
             PIVOT   (SUM(METER_VALUE) 
               FOR   TSM_METER_CODE IN ([0],[1],[2],[3],[5],[11])) AS METERS
END
--Inserts the row
INSERT INTO   WXP_002_MESSAGES
            ( WXM_TERMINAL_ID
            , wxm_datetime
            , wxm_event_id
            , wxm_nuc
            , wxm_nuid
            , wxm_serial
            , wxm_coin_in
            , wxm_coin_out
            , wxm_jackpot
            , wxm_hand_paid
            , wxm_games_played
            , wxm_bill_in
            , wxm_payout
            )
            SELECT   TE_TERMINAL_ID 
                   , @Now
                   , @pEventId
                   , TE_REGISTRATION_CODE
                   , TE_EXTERNAL_ID
                   , TE_SERIAL_NUMBER
                   , ISNULL([0],0)  AS COIN_IN
                   , ISNULL([1],0)  AS COIN_OUT
                   , ISNULL([2],0)  AS JACKPOT
                   , ISNULL([3],0)  AS HANDPAY
                   , ISNULL([5],0)  AS GAMES_PLAYED
                   , ISNULL([11],0) AS BILL_IN
                   , ISNULL(TE_THEORETICAL_PAYOUT, @DefaultPayout)
              FROM   #TEMP 
             PIVOT   (SUM(METER_VALUE) 
               FOR   TSM_METER_CODE IN ([0],[1],[2],[3],[5],[11])) AS METERS

--Inserts the row
     DROP TABLE #TEMP
END 
GO

GRANT EXECUTE ON [dbo].[SP_GenerateColombiaEventRegister] TO [wggui] WITH GRANT OPTION
GO

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[TRIGGER_PIN_FAILED]'))
DROP TRIGGER [dbo].[TRIGGER_PIN_FAILED]
GO

CREATE TRIGGER [dbo].TRIGGER_PIN_FAILED ON [dbo].ACCOUNTS AFTER UPDATE
AS 
BEGIN
      SET NOCOUNT ON;

    IF NOT UPDATE(AC_BLOCK_REASON) RETURN
    
    DECLARE @_old_br int
    DECLARE @_new_br int
    DECLARE @_id     bigint
    
    SELECT @_old_br = DELETED.AC_BLOCK_REASON, @_new_br = INSERTED.AC_BLOCK_REASON, @_id = INSERTED.AC_ACCOUNT_ID 
      FROM INSERTED INNER JOIN DELETED ON INSERTED.AC_ACCOUNT_ID = DELETED.AC_ACCOUNT_ID
      
    IF @_new_br = @_old_br + 4096
    BEGIN
        UPDATE ACCOUNTS 
             SET AC_BLOCK_REASON = AC_BLOCK_REASON - 4096
               , AC_BLOCKED = CASE WHEN ac_block_reason = 4096 THEN 0 ELSE 1 END
         WHERE AC_ACCOUNT_ID   = @_ID 
             AND AC_BLOCK_REASON & 4096 = 4096
    END
    
END

GO

ALTER PROCEDURE [dbo].[PR_collection_by_machine_and_date] 
 ( @pFromDt DATETIME   
  ,@pToDt DATETIME    
  ,@pClosingTime INTEGER
  ,@pMaskStatus INTEGER
  ,@pStatusPaid INTEGER
  ,@pTerminalWhere NVARCHAR(MAX) 
  ,@pBillDetail BIT
  ,@pShowMetters BIT
  ,@pOnlyGroupByTerminal BIT
  ,@pIncludeJackpotRoom BIT
)
AS
BEGIN

  -- DECLARATIONS
   DECLARE @Sql AS VARCHAR(MAX)
   DECLARE @nRows AS INT                              
   DECLARE @index AS INT                              
   DECLARE @tDays AS TABLE(NumDay INT)                       
   DECLARE @Columns AS VARCHAR(MAX)
   DECLARE @ColumnChk AS VARCHAR(MAX)
   DECLARE @p2007Opening AS VARCHAR(MAX)

   SET @p2007Opening  = CAST('2007-01-01T00:00:00' AS DATETIME);
   SET @p2007Opening  = DATEADD(HOUR, @pClosingTime, @p2007Opening)

   SET @nRows  = DATEDIFF(DAY, @pFromDt, @pToDt)+1 
   SET @index = 0                                  

   WHILE @index < @nRows                           
   BEGIN                                               
     INSERT INTO @tDays VALUES(@index)                  
     SET @index = @index + 1                       
   END 
   
    SET @Columns = ''
    SET @ColumnChk = ''

   SELECT NumDay INTO #TempTable_Days FROM @tDays 
   
   SELECT @Columns = COALESCE(@Columns + '[' + CAST(CGC_DENOMINATION AS NVARCHAR(20)) + '],', '')
     FROM (SELECT   DISTINCT CGC_DENOMINATION 
             FROM   CAGE_CURRENCIES 
            WHERE   CGC_ISO_CODE IN (SELECT   GP_KEY_VALUE 
                                       FROM   GENERAL_PARAMS 
                                      WHERE   GP_GROUP_KEY = 'RegionalOptions' 
                                        AND   GP_SUBJECT_KEY = 'CurrencyISOCode')
              AND   CGC_DENOMINATION >= 1 
              AND   CGC_ALLOWED = 1
                    UNION  
           SELECT   DISTINCT MCD_FACE_VALUE 
             FROM   MONEY_COLLECTION_DETAILS 
            INNER   JOIN MONEY_COLLECTIONS ON MCD_COLLECTION_ID = MC_COLLECTION_ID
            WHERE   MC_COLLECTION_DATETIME >= @pFromDt
              AND   MC_COLLECTION_DATETIME <  @pToDt
              AND   MC_TERMINAL_ID IS NOT NULL 
              AND   MCD_FACE_VALUE >= 1
          ) AS DTM
  ORDER BY CGC_DENOMINATION

  SET @ColumnChk = REPLACE( @Columns, ',', ' IS NOT NULL OR ')

  SET @Columns = LEFT(@Columns,LEN(@Columns)-1)
  SET @ColumnChk = LEFT(@ColumnChk,LEN(@ColumnChk)-3)
     
  SET @Sql = '
   SELECT   TE_PROVIDER_ID 
          , TE_TERMINAL_ID
          , TE_NAME 
          , ISNULL(TE_MULTI_DENOMINATION, ''--'') AS DENOMINATION '
  + CASE WHEN @pOnlyGroupByTerminal = 0 THEN '         
          , ORDER_DATE 
          , DATEADD(DAY, 1, ORDER_DATE) AS ORDER_DATE_FIN
          , COLLECTED_COIN_AMOUNT
          , TICKET_IN_COUNT
          , TI_IN_AMOUNT_RE
          , TI_IN_AMOUNT_NO_RE
          , (ISNULL(TI_IN_AMOUNT_RE,0) + ISNULL(TI_IN_AMOUNT_NO_RE, 0)) AS TI_IN_AMOUNT
          , TICKET_OUT_COUNT
          , TI_OUT_AMOUNT_RE
          , TI_OUT_AMOUNT_NO_RE
          , (ISNULL(TI_OUT_AMOUNT_RE, 0) + ISNULL(TI_OUT_AMOUNT_NO_RE, 0)) AS TI_OUT_AMOUNT
          , ( (ISNULL(TI_OUT_AMOUNT_RE, 0) + ISNULL(TI_OUT_AMOUNT_NO_RE, 0)) - (ISNULL(TI_IN_AMOUNT_RE, 0) + ISNULL(TI_IN_AMOUNT_NO_RE, 0)) ) AS NET_TITO 
          , HAND_PAYS.MANUAL
          , HAND_PAYS.CREDIT_CANCEL
          , HAND_PAYS.JACKPOT_DE_SALA
          , HAND_PAYS.PROGRESIVES
          , HAND_PAYS.NO_PROGRESIVES
          , (ISNULL(HAND_PAYS.MANUAL, 0) + ISNULL(HAND_PAYS.CREDIT_CANCEL, 0) + ISNULL(HAND_PAYS.JACKPOT_DE_SALA, 0) + ISNULL(HAND_PAYS.NO_PROGRESIVES, 0) + ISNULL(PROVISIONS.PROGRESIVE_PROVISIONS, 0)) AS HAND_PAYS_TOTAL
          , PROVISIONS.PROGRESIVE_PROVISIONS
          , (ISNULL(TI_OUT_AMOUNT_RE, 0) - ISNULL(TI_IN_AMOUNT_RE, 0) +
             ISNULL(HAND_PAYS.MANUAL, 0) + ISNULL(HAND_PAYS.CREDIT_CANCEL, 0) + ISNULL(HAND_PAYS.JACKPOT_DE_SALA, 0) + ISNULL(HAND_PAYS.NO_PROGRESIVES, 0) + ISNULL(PROVISIONS.PROGRESIVE_PROVISIONS, 0)) 
              AS TOTAL_WIN_WITHOUT_BILLS
          , 0 AS GAP_BILL
          ' + CASE WHEN @pShowMetters = 1 THEN ', METERS.TOTAL_COIN_IN_CREDITS' ELSE ', 0  AS GAP_METER_0' END + '
          ' + CASE WHEN @pShowMetters = 1 THEN ', METERS.TOTAL_COIN_OUT_CREDITS' ELSE ', 0 AS GAP_METER_1' END + '
          ' + CASE WHEN @pShowMetters = 1 THEN ', METERS.TOTAL_JACKPOT_CREDITS' ELSE ', 0 AS GAP_METER_2' END + '
          ' + CASE WHEN @pShowMetters = 1 THEN ', METERS.TOTAL_HAND_PAID_CANCELLED_CREDITS' ELSE ', 0 AS GAP_METER_3' END + '
          , BILLS.* '
     ELSE '
               , NULL AS GAP_ORDER_DATE 
          , NULL AS GAP_ORDER_DATE_FIN
          , SUM(COLLECTED_COIN_AMOUNT) AS COLLECTED_COIN_AMOUNT
          , SUM(TICKET_IN_COUNT) AS TICKET_IN_COUNT
          , SUM(TI_IN_AMOUNT_RE) AS TI_IN_AMOUNT_RE
          , SUM(TI_IN_AMOUNT_NO_RE) AS TI_IN_AMOUNT_NO_RE
          , SUM((ISNULL(TI_IN_AMOUNT_RE,0) + ISNULL(TI_IN_AMOUNT_NO_RE, 0))) AS TI_IN_AMOUNT
          , SUM(TICKET_OUT_COUNT) AS TICKET_OUT_COUNT
          , SUM(TI_OUT_AMOUNT_RE) AS TI_OUT_AMOUNT_RE
          , SUM(TI_OUT_AMOUNT_NO_RE) AS TI_OUT_AMOUNT_NO_RE
          , SUM((ISNULL(TI_OUT_AMOUNT_RE, 0) + ISNULL(TI_OUT_AMOUNT_NO_RE, 0))) AS TI_OUT_AMOUNT
          , SUM(( (ISNULL(TI_OUT_AMOUNT_RE, 0) + ISNULL(TI_OUT_AMOUNT_NO_RE, 0)) - (ISNULL(TI_IN_AMOUNT_RE, 0) + ISNULL(TI_IN_AMOUNT_NO_RE, 0))) ) AS NET_TITO      
          , SUM(HAND_PAYS.MANUAL) AS MANUAL
          , SUM(HAND_PAYS.CREDIT_CANCEL) AS CREDIT_CANCEL
          , SUM(HAND_PAYS.JACKPOT_DE_SALA) AS JACKPOT_DE_SALA
          , SUM(HAND_PAYS.PROGRESIVES) AS PROGRESIVES
          , SUM(HAND_PAYS.NO_PROGRESIVES) AS NO_PROGRESIVES
          , SUM((ISNULL(HAND_PAYS.MANUAL, 0) + ISNULL(HAND_PAYS.CREDIT_CANCEL, 0) + ISNULL(HAND_PAYS.JACKPOT_DE_SALA, 0) + ISNULL(HAND_PAYS.NO_PROGRESIVES, 0) + ISNULL(PROVISIONS.PROGRESIVE_PROVISIONS, 0))) AS HAND_PAYS_TOTAL
          , SUM(PROVISIONS.PROGRESIVE_PROVISIONS) AS PROGRESIVE_PROVISIONS
          , SUM((ISNULL(TI_OUT_AMOUNT_RE, 0) - ISNULL(TI_IN_AMOUNT_RE, 0) +
             ISNULL(HAND_PAYS.MANUAL, 0) + ISNULL(HAND_PAYS.CREDIT_CANCEL, 0) + ISNULL(HAND_PAYS.JACKPOT_DE_SALA, 0) + ISNULL(HAND_PAYS.NO_PROGRESIVES, 0) + ISNULL(PROVISIONS.PROGRESIVE_PROVISIONS, 0))) 
              AS TOTAL_WIN_WITHOUT_BILLS
          , 0 AS GAP_BILL
          ' + CASE WHEN @pShowMetters = 1 THEN ', SUM(METERS.TOTAL_COIN_IN_CREDITS) AS TOTAL_COIN_IN_CREDITS' ELSE ', 0  AS GAP_METER_0' END + '
          ' + CASE WHEN @pShowMetters = 1 THEN ', SUM(METERS.TOTAL_COIN_OUT_CREDITS) AS TOTAL_COIN_OUT_CREDITS' ELSE ', 0 AS GAP_METER_1' END + '
          ' + CASE WHEN @pShowMetters = 1 THEN ', SUM(METERS.TOTAL_JACKPOT_CREDITS) AS TOTAL_JACKPOT_CREDITS' ELSE ', 0 AS GAP_METER_2' END + '
          ' + CASE WHEN @pShowMetters = 1 THEN ', SUM(METERS.TOTAL_HAND_PAID_CANCELLED_CREDITS) AS TOTAL_HAND_PAID_CANCELLED_CREDITS' ELSE ', 0 AS GAP_METER_3' END + '
          , NULL AS GAP_BILL_MC_TERMINAL_ID
          , NULL AS GAP_BILL_MC_COLLECTION_DATETIME
          , SUM(BILLS.MCD_FACE_VALUE)
          , SUM(BILLS.COLLECTED) '
     END + '
     FROM   TERMINALS 
 LEFT JOIN (SELECT   DATEADD(DAY, NumDay, CAST(''' + CAST(@pFromDt AS VARCHAR(50)) + ''' AS DATETIME)) AS ORDER_DATE FROM #TempTable_Days ) DIA ON ORDER_DATE <= GETDATE() 
 LEFT JOIN (SELECT   TC_TERMINAL_ID 
                   , DATEADD(HOUR, ' + CAST(@pClosingTime AS VARCHAR(50)) + ', TC_DATE) AS TC_DATE
              FROM   TERMINALS_CONNECTED 
             WHERE   TC_DATE >= dateadd(day, datediff(day, 0, CAST(''' + CAST(@pFromDt AS VARCHAR(50)) + ''' AS DATETIME)), 0)
               AND   TC_DATE <  dateadd(day, datediff(day, 0, CAST(''' + CAST(@pToDt AS VARCHAR(50)) + ''' AS DATETIME)), 0)
               AND   TC_STATUS = 0
               AND   TC_CONNECTED = 1) TC ON TC_TERMINAL_ID = TE_TERMINAL_ID AND TC_DATE = DIA.ORDER_DATE 
 LEFT JOIN (SELECT   MC_TERMINAL_ID AS MC_TERMINAL_ID_C
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), CGS_WORKING_DAY)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS MC_COLLECTION_DATETIME_C
                  , SUM(MC_COLLECTED_COIN_AMOUNT) AS COLLECTED_COIN_AMOUNT
              FROM   MONEY_COLLECTIONS
        INNER JOIN   CAGE_MOVEMENTS ON CGM_MC_COLLECTION_ID = MC_COLLECTION_ID
        INNER JOIN   CAGE_SESSIONS ON CGS_CAGE_SESSION_ID = CGM_CAGE_SESSION_ID
             WHERE   CGS_WORKING_DAY >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
               AND   CGS_WORKING_DAY <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
               AND   MC_TERMINAL_ID IS NOT NULL
             GROUP   BY MC_TERMINAL_ID
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), CGS_WORKING_DAY)/24, CAST(''' + @p2007Opening + ''' AS DATETIME))
           ) CASH_IN_COINS ON TE_TERMINAL_ID = MC_TERMINAL_ID_C AND MC_COLLECTION_DATETIME_C = DIA.ORDER_DATE 
 LEFT JOIN (SELECT   MC_TERMINAL_ID AS MC_TERMINAL_ID_T
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), CGS_WORKING_DAY)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS MC_COLLECTION_DATETIME_T
                   , SUM(MC_EXPECTED_TICKET_COUNT) AS TICKET_IN_COUNT
                   , SUM(ISNULL(MC_EXPECTED_RE_TICKET_AMOUNT, 0)+ISNULL(MC_EXPECTED_PROMO_RE_TICKET_AMOUNT, 0)) AS TI_IN_AMOUNT_RE
                   , SUM(ISNULL(MC_EXPECTED_PROMO_NR_TICKET_AMOUNT, 0)) AS TI_IN_AMOUNT_NO_RE
              FROM   MONEY_COLLECTIONS
        INNER JOIN   CAGE_MOVEMENTS ON CGM_MC_COLLECTION_ID = MC_COLLECTION_ID
        INNER JOIN   CAGE_SESSIONS ON CGS_CAGE_SESSION_ID = CGM_CAGE_SESSION_ID
             WHERE   CGS_WORKING_DAY >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
               AND   CGS_WORKING_DAY <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
               AND   MC_TERMINAL_ID IS NOT NULL
             GROUP   BY MC_TERMINAL_ID
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), CGS_WORKING_DAY)/24, CAST(''' + @p2007Opening + ''' AS DATETIME))
           ) TICKETS_IN ON TE_TERMINAL_ID = MC_TERMINAL_ID_T AND MC_COLLECTION_DATETIME_T = DIA.ORDER_DATE 
LEFT JOIN ( SELECT   TI_CREATED_TERMINAL_ID
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), TI_CREATED_DATETIME)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS TI_CREATED_DATETIME
                   , COUNT(1) AS TICKET_OUT_COUNT
                   , SUM(CASE WHEN TI_TYPE_ID = 2 THEN 0 ELSE TI_AMOUNT END) AS TI_OUT_AMOUNT_RE
                   , SUM(CASE WHEN TI_TYPE_ID = 2 THEN TI_AMOUNT ELSE 0 END) AS TI_OUT_AMOUNT_NO_RE
              FROM   TICKETS 
             WHERE   TI_CREATED_DATETIME >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
               AND   TI_CREATED_DATETIME <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
               AND   TI_CREATED_TERMINAL_TYPE = 1     -- TITO_TERMINAL_TYPE.TERMINAL = 1
               AND   TI_TYPE_ID IN (0,1,2,5) -- TITO_TICKET_TYPE
                                                --CASHABLE = 0,
                                                --PROMO_REDEEM = 1,
                                                --PROMO_NONREDEEM = 2,  // only playable
                                                --HANDPAY = 3,
                                                --JACKPOT = 4,
                                                --OFFLINE = 5
             GROUP   BY TI_CREATED_TERMINAL_ID
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), TI_CREATED_DATETIME)/24, CAST(''' + @p2007Opening + ''' AS DATETIME))
          ) TICKETS_OUT ON TE_TERMINAL_ID = TI_CREATED_TERMINAL_ID AND TI_CREATED_DATETIME = DIA.ORDER_DATE
LEFT JOIN (SELECT   (CASE WHEN HP_TYPE = 20 THEN HP_SITE_JACKPOT_AWARDED_ON_TERMINAL_ID ELSE HP_TERMINAL_ID END) AS HP_TERMINAL_ID 
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), ISNULL(HP_STATUS_CHANGED, CASE WHEN (HP_TYPE IN ( 20)) THEN HP_DATETIME ELSE NULL END))/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS HP_DATETIME
                   
                   , SUM(CASE WHEN (HP_TYPE IN (10, 1010)) THEN ISNULL(HP_AMOUNT, 0) ELSE 0 END) AS MANUAL 
                   
                   , SUM(CASE WHEN (HP_TYPE IN ( 0, 1000 )) THEN ISNULL(HP_AMOUNT, 0) ELSE 0 END) +
                     SUM(CASE WHEN (HP_TYPE IN ( 1, 1001 ) AND (HP_LEVEL>=1 AND HP_LEVEL<=32) ) THEN 
                         CASE WHEN (HP_TAX_BASE_AMOUNT IS NULL) THEN 0 ELSE ISNULL(HP_AMOUNT, 0) - ISNULL(HP_TAX_BASE_AMOUNT, 0) END ELSE 0 END) AS CREDIT_CANCEL
'                          
  IF @pIncludeJackpotRoom = 1 
  BEGIN
    SET @Sql =  @Sql + ', SUM(CASE WHEN (HP_TYPE IN ( 20)) THEN ISNULL(HP_AMOUNT, 0) ELSE 0 END) AS JACKPOT_DE_SALA '
  END
  ELSE
  BEGIN
    SET @Sql =  @Sql + ', 0 AS JACKPOT_DE_SALA '
  END
                   
  SET @Sql =  @Sql +
  '                , SUM(CASE WHEN (HP_TYPE IN ( 1, 1001 ) AND (HP_LEVEL>=1 AND HP_LEVEL<=32) AND HP_PROGRESSIVE_ID IS NOT NULL) THEN 
                         CASE WHEN (HP_TAX_BASE_AMOUNT IS NULL) THEN ISNULL(HP_AMOUNT, 0) ELSE ISNULL(HP_TAX_BASE_AMOUNT, 0) END ELSE 0 END) AS PROGRESIVES 
                         
                   , SUM(CASE WHEN (HP_TYPE IN ( 1, 1001 ) AND (HP_LEVEL <1 OR  HP_LEVEL >32 OR HP_LEVEL IS NULL)) THEN ISNULL(HP_AMOUNT, 0) ELSE 0 END) + 
                     SUM(CASE WHEN (HP_TYPE IN ( 1, 1001 ) AND (HP_LEVEL>=1 AND HP_LEVEL <=32 AND HP_PROGRESSIVE_ID IS NULL)) THEN 
                         CASE WHEN (HP_TAX_BASE_AMOUNT IS NULL) THEN ISNULL(HP_AMOUNT, 0) ELSE ISNULL(HP_TAX_BASE_AMOUNT, 0) END ELSE 0 END) AS NO_PROGRESIVES
                   
              FROM   HANDPAYS WITH (INDEX (IX_hp_status_changed))
             WHERE   HP_STATUS & ' + CAST(@pMaskStatus AS VARCHAR(10)) + ' = ' + CAST(@pStatusPaid AS VARCHAR(10)) + 
            '  AND   ISNULL(HP_STATUS_CHANGED, CASE WHEN (HP_TYPE IN ( 20)) THEN HP_DATETIME ELSE NULL END) >= CAST(''' + CAST(@pFromDt AS VARCHAR(50)) + ''' AS DATETIME) 
               AND   ISNULL(HP_STATUS_CHANGED, CASE WHEN (HP_TYPE IN ( 20)) THEN HP_DATETIME ELSE NULL END) < CAST(''' + CAST(@pToDt AS VARCHAR(50)) + ''' AS DATETIME) 
             GROUP   BY (CASE WHEN HP_TYPE = 20 THEN HP_SITE_JACKPOT_AWARDED_ON_TERMINAL_ID ELSE HP_TERMINAL_ID END) 
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), ISNULL(HP_STATUS_CHANGED, CASE WHEN (HP_TYPE IN ( 20)) THEN HP_DATETIME ELSE NULL END))/24, CAST(''' + @p2007Opening + ''' AS DATETIME))
                   ) HAND_PAYS ON HP_TERMINAL_ID = TE_TERMINAL_ID AND HP_DATETIME = DIA.ORDER_DATE
LEFT JOIN (SELECT   PPT_TERMINAL_ID 
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), PGP_HOUR_FROM)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS PGP_HOUR_FROM
                   , SUM(ISNULL(PPT_AMOUNT, 0)) AS PROGRESIVE_PROVISIONS 
             FROM    PROGRESSIVES_PROVISIONS_TERMINALS 
        LEFT JOIN    PROGRESSIVES_PROVISIONS ON PGP_PROVISION_ID = PPT_PROVISION_ID 
                                            AND PGP_PROGRESSIVE_ID = PPT_PROGRESSIVE_ID 
                                            AND PGP_HOUR_FROM >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
                                            AND PGP_HOUR_FROM <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
                                            AND PGP_STATUS = 0
             GROUP   BY PPT_TERMINAL_ID 
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), PGP_HOUR_FROM)/24, CAST(''' + @p2007Opening + ''' AS DATETIME))
                   ) PROVISIONS ON PPT_TERMINAL_ID = TE_TERMINAL_ID AND PGP_HOUR_FROM = DIA.ORDER_DATE ' 
IF @pBillDetail = 1
BEGIN
  SET @Sql =  @Sql +
'LEFT JOIN (SELECT   *  
              FROM ( SELECT   MC_TERMINAL_ID
                            , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), CGS_WORKING_DAY)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS MC_COLLECTION_DATETIME
                            , MCD_FACE_VALUE
                            , (MCD_NUM_COLLECTED * MCD_FACE_VALUE) AS COLLECTED
                       FROM   MONEY_COLLECTION_DETAILS 
                 INNER JOIN   MONEY_COLLECTIONS ON MCD_COLLECTION_ID = MC_COLLECTION_ID
                 INNER JOIN   CAGE_MOVEMENTS ON CGM_MC_COLLECTION_ID = MC_COLLECTION_ID
                 INNER JOIN   CAGE_SESSIONS ON CGS_CAGE_SESSION_ID = CGM_CAGE_SESSION_ID
                      WHERE   CGS_WORKING_DAY >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
                        AND   CGS_WORKING_DAY <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
                        AND   MC_TERMINAL_ID IS NOT NULL 
                        AND   MCD_CAGE_CURRENCY_TYPE = 0
                        AND   MCD_FACE_VALUE > 0
                   ) PIV
             PIVOT (  SUM(COLLECTED) FOR MCD_FACE_VALUE IN ('+ @Columns  + ')) AS CHILD
            ) BILLS ON TE_TERMINAL_ID = MC_TERMINAL_ID AND MC_COLLECTION_DATETIME = DIA.ORDER_DATE '
END
ELSE
BEGIN
  SET @Sql =  @Sql +
'LEFT JOIN (SELECT   MC_TERMINAL_ID
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), CGS_WORKING_DAY)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS MC_COLLECTION_DATETIME
                   , 0 AS MCD_FACE_VALUE
                   , SUM(MCD_NUM_COLLECTED * MCD_FACE_VALUE) AS COLLECTED
              FROM   MONEY_COLLECTION_DETAILS 
        INNER JOIN   MONEY_COLLECTIONS ON MCD_COLLECTION_ID = MC_COLLECTION_ID
        INNER JOIN   CAGE_MOVEMENTS ON CGM_MC_COLLECTION_ID = MC_COLLECTION_ID
        INNER JOIN   CAGE_SESSIONS ON CGS_CAGE_SESSION_ID = CGM_CAGE_SESSION_ID
             WHERE   CGS_WORKING_DAY >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
               AND   CGS_WORKING_DAY <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
               AND   MC_TERMINAL_ID IS NOT NULL
               AND   MCD_CAGE_CURRENCY_TYPE = 0
               AND   MCD_FACE_VALUE > 0
             GROUP   BY MC_TERMINAL_ID
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), CGS_WORKING_DAY)/24, CAST(''' + @p2007Opening + ''' AS DATETIME))
          ) BILLS ON TE_TERMINAL_ID = MC_TERMINAL_ID AND MC_COLLECTION_DATETIME = DIA.ORDER_DATE '
END


IF @pShowMetters = 1
BEGIN
SET @Sql =  @Sql +
'LEFT JOIN (SELECT TSMH_TERMINAL_ID
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), TSMH_DATETIME)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS TSMH_DATE
                   , SUM(CASE WHEN TSMH_METER_CODE = 0 THEN ISNULL(CAST(TSMH_METER_INCREMENT AS MONEY)/100, 0) END) AS TOTAL_COIN_IN_CREDITS
                   , SUM(CASE WHEN TSMH_METER_CODE = 1 THEN ISNULL(CAST(TSMH_METER_INCREMENT AS MONEY)/100, 0) END) AS TOTAL_COIN_OUT_CREDITS
                   , SUM(CASE WHEN TSMH_METER_CODE = 2 THEN ISNULL(CAST(TSMH_METER_INCREMENT AS MONEY)/100, 0) END) AS TOTAL_JACKPOT_CREDITS
                   , SUM(CASE WHEN TSMH_METER_CODE = 3 THEN ISNULL(CAST(TSMH_METER_INCREMENT AS MONEY)/100, 0) END) AS TOTAL_HAND_PAID_CANCELLED_CREDITS
                    FROM    TERMINAL_SAS_METERS_HISTORY
                    WHERE    TSMH_TYPE = 20
                      AND    TSMH_METER_CODE IN (0,1,2,3)
                      AND    TSMH_METER_INCREMENT > 0 
                      AND    TSMH_DATETIME >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
              AND    TSMH_DATETIME <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
            GROUP    BY TSMH_TERMINAL_ID
                              , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), TSMH_DATETIME)/24, CAST(''' + @p2007Opening + ''' AS DATETIME))
          ) METERS ON TE_TERMINAL_ID = TSMH_TERMINAL_ID AND TSMH_DATE = DIA.ORDER_DATE '
END

SET @Sql =  @Sql + 'WHERE ( ' 

IF @pBillDetail = 1
BEGIN
  SET @Sql =  @Sql + @ColumnChk 
END
ELSE
BEGIN
  SET @Sql =  @Sql + ' BILLS.COLLECTED IS NOT NULL ' 
END

SET @Sql =  @Sql +
  ' OR TICKET_IN_COUNT IS NOT NULL
    OR TI_IN_AMOUNT_RE IS NOT NULL
    OR TI_IN_AMOUNT_NO_RE IS NOT NULL
    OR TICKET_OUT_COUNT IS NOT NULL
    OR TI_OUT_AMOUNT_RE IS NOT NULL
    OR TI_OUT_AMOUNT_NO_RE IS NOT NULL
    OR HAND_PAYS.MANUAL IS NOT NULL
    OR HAND_PAYS.CREDIT_CANCEL IS NOT NULL
    OR HAND_PAYS.JACKPOT_DE_SALA IS NOT NULL
    OR HAND_PAYS.PROGRESIVES IS NOT NULL
    OR HAND_PAYS.NO_PROGRESIVES IS NOT NULL
    OR PROVISIONS.PROGRESIVE_PROVISIONS IS NOT NULL' +
       CASE WHEN @pShowMetters = 1 THEN '
             OR METERS.TOTAL_COIN_IN_CREDITS IS NOT NULL
             OR METERS.TOTAL_COIN_OUT_CREDITS IS NOT NULL
             OR METERS.TOTAL_JACKPOT_CREDITS IS NOT NULL
             OR METERS.TOTAL_HAND_PAID_CANCELLED_CREDITS IS NOT NULL'
    ELSE '' END + '
    OR TC_DATE IS NOT NULL
    )' + CAST(@pTerminalWhere AS Varchar(max)) +
CASE WHEN @pOnlyGroupByTerminal = 1 THEN 
' GROUP   BY TE_PROVIDER_ID 
        , TE_TERMINAL_ID
        , TE_NAME 
        , ISNULL(TE_MULTI_DENOMINATION, ''--'') '
ELSE '' END +
' ORDER   BY TE_PROVIDER_ID 
        , TE_TERMINAL_ID 
        , TE_NAME 
        , ISNULL(TE_MULTI_DENOMINATION, ''--'') ' +
        
CASE WHEN @pOnlyGroupByTerminal = 0 THEN 
        ', ORDER_DATE ' 
ELSE '' END

-- PRINT @Sql

  EXECUTE (@Sql)

  DROP TABLE #TempTable_Days

END -- END PROCEDURE
GO
