/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 351;

SET @New_ReleaseId = 352;
SET @New_ScriptName = N'UpdateTo_18.352.041.sql';
SET @New_Description = N'ReportCashDeskDraws'; 

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/**** GENERAL PARAM *****/

/******* TABLES  *******/

/******* INDEXES *******/

/******* RECORDS *******/

/******* PROCEDURES *******/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ReportCashDeskDraws]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ReportCashDeskDraws]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[ReportCashDeskDraws]  
  @pDrawFrom	    DATETIME      = NULL,
  @pDrawTo		    DATETIME      = NULL,
  @pDrawId		    BIGINT		  =	NULL,
  @pCashierId       INT			  = NULL,
  @pUserId          INT			  = NULL,
  @pSqlAccount      NVARCHAR(MAX) = NULL,
  @pCashierMovement INT	          = NULL,
  @pCashDeskDraw    INT           = 1,
  @pGamingTableDraw INT           = 1,
  @pLooserPrizeId   INT           = 5

AS
BEGIN

	SET NOCOUNT ON;
	DECLARE @_Terminal_CashDesk_draw_type AS INT;
	DECLARE @_Terminal_GamingTable_draw_type AS INT;
	DECLARE @_Sorter_terminal_ID AS INT;
	
	SET @_Terminal_CashDesk_draw_type = 106
	SET @_Terminal_GamingTable_draw_type = 111
	SET @_Sorter_terminal_ID = (SELECT  TOP 1 te_terminal_id FROM terminals WHERE te_terminal_type = @_Terminal_CashDesk_draw_type)
	

    CREATE TABLE #ACCOUNTS_TEMP (AC_ACCOUNT_ID BIGINT NOT NULL, AC_HOLDER_NAME NVARCHAR(200), AC_TRACK_DATA NVARCHAR(50))
	IF @pSqlAccount IS NOT NULL
	BEGIN
	  INSERT INTO #ACCOUNTS_TEMP EXEC ('SELECT AC_ACCOUNT_ID, AC_HOLDER_NAME, AC_TRACK_DATA  FROM ACCOUNTS ' + @pSqlAccount)
		IF @@ROWCOUNT > 500
			ALTER TABLE #ACCOUNTS_TEMP ADD PRIMARY KEY NONCLUSTERED (AC_ACCOUNT_ID)
	END

	IF 	@pDrawId IS NOT NULL 
	--
	-- Filtering by Draw ID
	--
	BEGIN	
	  IF @pDrawFrom IS NULL SET @pDrawFrom = '2007-01-01T00:00:00'
		IF @pDrawTo   IS NULL SET @pDrawTo   = GETDATE ()
		
    --No filter by draw type
	  IF (@pCashDeskDraw = 1 AND @pGamingTableDraw = 1) OR (@pCashDeskDraw = 0 AND @pGamingTableDraw = 0)
	  BEGIN
		    SELECT DISTINCT
		        CD_DRAW_ID
		      , CD_DRAW_DATETIME
		      , TE_TERMINAL_TYPE
		      , CM_CASHIER_NAME		  
		      , CM_USER_NAME
		      , CD_OPERATION_ID		
		      , CM_DATE
		      , CD_ACCOUNT_ID
		      , AC_HOLDER_NAME
		      , CD_COMBINATION_BET
		      , CD_COMBINATION_WON
		      , CD_RE_BET
          , CASE WHEN CD_PRIZE_ID = @pLooserPrizeId THEN AC.AO_PROMO_REDEEMABLE ELSE CD_RE_WON END CD_RE_WON
		      , CASE WHEN CD_PRIZE_ID = @pLooserPrizeId THEN AC.AO_PROMO_NOT_REDEEMABLE ELSE CD_NR_WON END CD_NR_WON
		      , CD_PRIZE_ID
		    FROM
		       CASHDESK_DRAWS
		       WITH(INDEX(IX_CD_DRAW_DATETIME_ACCOUNT))
		        INNER JOIN CASHIER_MOVEMENTS WITH(INDEX(IX_CM_OPERATION_ID)) ON CD_OPERATION_ID = CM_OPERATION_ID
		        INNER JOIN ACCOUNTS          ON CD_ACCOUNT_ID   = AC_ACCOUNT_ID
		        INNER JOIN TERMINALS   ON (CASE WHEN CD_TERMINAL IS NOT NULL THEN CD_TERMINAL ELSE @_Sorter_terminal_ID END) = TE_TERMINAL_ID
		        LEFT JOIN ACCOUNT_OPERATIONS AC ON AC.AO_OPERATION_ID = CD_OPERATION_ID
		        		        
		    WHERE    
		        CD_DRAW_ID = @pDrawId	
		        AND CD_DRAW_DATETIME >= @pDrawFrom
		        AND CD_DRAW_DATETIME < @pDrawTo	
		        AND ((@pCashierId IS NULL) OR (CM_CASHIER_ID = @pCashierId))
		        AND ((@pUserId IS NULL) OR (CM_USER_ID = @pUserId))
		        AND ( (@pSqlAccount     IS NULL) OR (CD_ACCOUNT_ID IN ( SELECT AC_ACCOUNT_ID FROM #ACCOUNTS_TEMP ) ) )
		        AND ( CM_TYPE = @pCashierMovement )
		        AND    te_terminal_type IN (@_Terminal_CashDesk_draw_type, @_Terminal_GamingTable_draw_type)
		        
		    ORDER BY CD_DRAW_ID
		    DROP TABLE #ACCOUNTS_TEMP

		    RETURN
		END
    --Filter by CASH DESK draw
	  ELSE IF  @pCashDeskDraw = 1 AND @pGamingTableDraw = 0
	  BEGIN
		    SELECT DISTINCT
		        CD_DRAW_ID
		      , CD_DRAW_DATETIME
		      , TE_TERMINAL_TYPE
		      , CM_CASHIER_NAME		  
		      , CM_USER_NAME
		      , CD_OPERATION_ID		
		      , CM_DATE
		      , CD_ACCOUNT_ID
		      , AC_HOLDER_NAME
		      , CD_COMBINATION_BET
		      , CD_COMBINATION_WON
		      , CD_RE_BET
          , CASE WHEN CD_PRIZE_ID = @pLooserPrizeId THEN AC.AO_PROMO_REDEEMABLE ELSE CD_RE_WON END CD_RE_WON
		      , CASE WHEN CD_PRIZE_ID = @pLooserPrizeId THEN AC.AO_PROMO_NOT_REDEEMABLE ELSE CD_NR_WON END CD_NR_WON
		      , CD_PRIZE_ID
		    FROM
		       CASHDESK_DRAWS
		       WITH(INDEX(IX_CD_DRAW_DATETIME_ACCOUNT))
		        INNER JOIN CASHIER_MOVEMENTS WITH(INDEX(IX_CM_OPERATION_ID)) ON CD_OPERATION_ID = CM_OPERATION_ID  
		        INNER JOIN ACCOUNTS   ON CD_ACCOUNT_ID   = AC_ACCOUNT_ID
		        INNER JOIN TERMINALS   ON (CASE WHEN CD_TERMINAL IS NOT NULL THEN CD_TERMINAL ELSE @_Sorter_terminal_ID END) = TE_TERMINAL_ID
		        LEFT JOIN ACCOUNT_OPERATIONS AC ON AC.AO_OPERATION_ID = CD_OPERATION_ID
		        
		    WHERE    
		        CD_DRAW_ID = @pDrawId	
		        AND CD_DRAW_DATETIME >= @pDrawFrom
		        AND CD_DRAW_DATETIME < @pDrawTo	
		        AND ((@pCashierId IS NULL) OR (CM_CASHIER_ID = @pCashierId))
		        AND ((@pUserId IS NULL) OR (CM_USER_ID = @pUserId))
		        AND ( (@pSqlAccount     IS NULL) OR (CD_ACCOUNT_ID IN ( SELECT AC_ACCOUNT_ID FROM #ACCOUNTS_TEMP ) ) )
		        AND ( CM_TYPE = @pCashierMovement )
		        AND te_terminal_type = @_Terminal_CashDesk_draw_type
		    ORDER BY CD_DRAW_ID
		    DROP TABLE #ACCOUNTS_TEMP

		    RETURN		    	  
	  END
--Filter by GAMING TABLE draw
	  ELSE IF  @pCashDeskDraw = 0 AND @pGamingTableDraw = 1
	  BEGIN
		    SELECT DISTINCT
		        CD_DRAW_ID
		      , CD_DRAW_DATETIME
		      , TE_TERMINAL_TYPE
		      , CM_CASHIER_NAME		  
		      , CM_USER_NAME
		      , CD_OPERATION_ID		
		      , CM_DATE
		      , CD_ACCOUNT_ID
		      , AC_HOLDER_NAME
		      , CD_COMBINATION_BET
		      , CD_COMBINATION_WON
		      , CD_RE_BET
          , CASE WHEN CD_PRIZE_ID = @pLooserPrizeId THEN AC.AO_PROMO_REDEEMABLE ELSE CD_RE_WON END CD_RE_WON
		      , CASE WHEN CD_PRIZE_ID = @pLooserPrizeId THEN AC.AO_PROMO_NOT_REDEEMABLE ELSE CD_NR_WON END CD_NR_WON
		      , CD_PRIZE_ID
		    FROM
		       CASHDESK_DRAWS
		       WITH(INDEX(IX_CD_DRAW_DATETIME_ACCOUNT))
		        INNER JOIN CASHIER_MOVEMENTS WITH(INDEX(IX_CM_OPERATION_ID)) ON CD_OPERATION_ID = CM_OPERATION_ID  
		        INNER JOIN ACCOUNTS   ON CD_ACCOUNT_ID   = AC_ACCOUNT_ID
		        INNER JOIN TERMINALS   ON (CASE WHEN CD_TERMINAL IS NOT NULL THEN CD_TERMINAL ELSE @_Sorter_terminal_ID END) = TE_TERMINAL_ID
		        LEFT JOIN ACCOUNT_OPERATIONS AC ON AC.AO_OPERATION_ID = CD_OPERATION_ID
		        
		    WHERE    
		        CD_DRAW_ID = @pDrawId	
		        AND CD_DRAW_DATETIME >= @pDrawFrom
		        AND CD_DRAW_DATETIME < @pDrawTo	
		        AND ((@pCashierId IS NULL) OR (CM_CASHIER_ID = @pCashierId))
		        AND ((@pUserId IS NULL) OR (CM_USER_ID = @pUserId))
		        AND ( (@pSqlAccount     IS NULL) OR (CD_ACCOUNT_ID IN ( SELECT AC_ACCOUNT_ID FROM #ACCOUNTS_TEMP ) ) )
		        AND ( CM_TYPE = @pCashierMovement )
		        AND te_terminal_type = @_Terminal_GamingTable_draw_type
		    ORDER BY CD_DRAW_ID
		    DROP TABLE #ACCOUNTS_TEMP

		    RETURN		    	  
	  END	  
		
	END 	

	
	IF 	@pSqlAccount IS NOT NULL 
	--
	-- Filtering by account, but not by draw ID
	--
	BEGIN				
		IF @pDrawFrom IS NULL SET @pDrawFrom = '2007-01-01T00:00:00'
		IF @pDrawTo   IS NULL SET @pDrawTo   = GETDATE ()

	  --No filter by draw type
	  IF (@pCashDeskDraw = 1 AND @pGamingTableDraw = 1) OR (@pCashDeskDraw = 0 AND @pGamingTableDraw = 0)
	  BEGIN
		    SELECT DISTINCT
		        CD_DRAW_ID
		      , CD_DRAW_DATETIME
		      , TE_TERMINAL_TYPE
		      , CM_CASHIER_NAME		  
		      , CM_USER_NAME
		      , CD_OPERATION_ID		
		      , CM_DATE
		      , CD_ACCOUNT_ID
		      , AC_HOLDER_NAME
		      , CD_COMBINATION_BET
		      , CD_COMBINATION_WON
		      , CD_RE_BET
          , CASE WHEN CD_PRIZE_ID = @pLooserPrizeId THEN AC.AO_PROMO_REDEEMABLE ELSE CD_RE_WON END CD_RE_WON
		      , CASE WHEN CD_PRIZE_ID = @pLooserPrizeId THEN AC.AO_PROMO_NOT_REDEEMABLE ELSE CD_NR_WON END CD_NR_WON
		      , CD_PRIZE_ID
		    FROM
		       CASHDESK_DRAWS
		       WITH(INDEX(IX_CD_ACCOUNT_DATETIME))
		        INNER JOIN CASHIER_MOVEMENTS WITH(INDEX(IX_CM_OPERATION_ID)) ON CD_OPERATION_ID = CM_OPERATION_ID 
		        INNER JOIN #ACCOUNTS_TEMP    ON CD_ACCOUNT_ID   = AC_ACCOUNT_ID
		        INNER JOIN TERMINALS   ON (CASE WHEN CD_TERMINAL IS NOT NULL THEN CD_TERMINAL ELSE @_Sorter_terminal_ID END) = TE_TERMINAL_ID
		        LEFT JOIN ACCOUNT_OPERATIONS AC ON AC.AO_OPERATION_ID = CD_OPERATION_ID
		        
		    WHERE    
			    CD_DRAW_DATETIME >= @pDrawFrom
		       AND CD_DRAW_DATETIME < @pDrawTo 		   
		       AND ((@pCashierId IS NULL) OR (CM_CASHIER_ID = @pCashierId))
		       AND ((@pUserId IS NULL) OR (CM_USER_ID = @pUserId))
		       AND ( CM_TYPE = @pCashierMovement ) 
		       AND    te_terminal_type IN (@_Terminal_CashDesk_draw_type, @_Terminal_GamingTable_draw_type)
		       
		    ORDER BY CD_DRAW_ID
		    DROP TABLE #ACCOUNTS_TEMP

		    RETURN
		END
	  --Filter by CASH DESK draw
	  ELSE IF  @pCashDeskDraw = 1 AND @pGamingTableDraw = 0
	  BEGIN
          SELECT DISTINCT
		        CD_DRAW_ID
		      , CD_DRAW_DATETIME
		      , TE_TERMINAL_TYPE
		      , CM_CASHIER_NAME		  
		      , CM_USER_NAME
		      , CD_OPERATION_ID		
		      , CM_DATE
		      , CD_ACCOUNT_ID
		      , AC_HOLDER_NAME
		      , CD_COMBINATION_BET
		      , CD_COMBINATION_WON
		      , CD_RE_BET
          , CASE WHEN CD_PRIZE_ID = @pLooserPrizeId THEN AC.AO_PROMO_REDEEMABLE ELSE CD_RE_WON END CD_RE_WON
		      , CASE WHEN CD_PRIZE_ID = @pLooserPrizeId THEN AC.AO_PROMO_NOT_REDEEMABLE ELSE CD_NR_WON END CD_NR_WON
		      , CD_PRIZE_ID
		    FROM
		       CASHDESK_DRAWS
		       WITH(INDEX(IX_CD_ACCOUNT_DATETIME))
		        INNER JOIN CASHIER_MOVEMENTS WITH(INDEX(IX_CM_OPERATION_ID)) ON CD_OPERATION_ID = CM_OPERATION_ID 
		        INNER JOIN #ACCOUNTS_TEMP    ON CD_ACCOUNT_ID   = AC_ACCOUNT_ID
		        INNER JOIN TERMINALS   ON (CASE WHEN CD_TERMINAL IS NOT NULL THEN CD_TERMINAL ELSE @_Sorter_terminal_ID END) = TE_TERMINAL_ID
		        LEFT JOIN ACCOUNT_OPERATIONS AC ON AC.AO_OPERATION_ID = CD_OPERATION_ID
		        
		    WHERE    
			    CD_DRAW_DATETIME >= @pDrawFrom
		       AND CD_DRAW_DATETIME < @pDrawTo 		   
		       AND((@pCashierId IS NULL) OR (CM_CASHIER_ID = @pCashierId))
		       AND ((@pUserId IS NULL) OR (CM_USER_ID = @pUserId))
		       AND ( CM_TYPE = @pCashierMovement )
		       AND    te_terminal_type = @_Terminal_CashDesk_draw_type
		    ORDER BY CD_DRAW_ID
		    DROP TABLE #ACCOUNTS_TEMP
		    
		    RETURN
	  END
  --Filter by GAMING TABLE draw
	  ELSE IF  @pCashDeskDraw = 0 AND @pGamingTableDraw = 1
	  BEGIN
          SELECT DISTINCT
		        CD_DRAW_ID
		      , CD_DRAW_DATETIME
		      , TE_TERMINAL_TYPE
		      , CM_CASHIER_NAME		  
		      , CM_USER_NAME
		      , CD_OPERATION_ID		
		      , CM_DATE
		      , CD_ACCOUNT_ID
		      , AC_HOLDER_NAME
		      , CD_COMBINATION_BET
		      , CD_COMBINATION_WON
		      , CD_RE_BET
          , CASE WHEN CD_PRIZE_ID = @pLooserPrizeId THEN AC.AO_PROMO_REDEEMABLE ELSE CD_RE_WON END CD_RE_WON
		      , CASE WHEN CD_PRIZE_ID = @pLooserPrizeId THEN AC.AO_PROMO_NOT_REDEEMABLE ELSE CD_NR_WON END CD_NR_WON
		      , CD_PRIZE_ID
		    FROM
		       CASHDESK_DRAWS
		       WITH(INDEX(IX_CD_ACCOUNT_DATETIME))
		        INNER JOIN CASHIER_MOVEMENTS WITH(INDEX(IX_CM_OPERATION_ID)) ON CD_OPERATION_ID = CM_OPERATION_ID 
		        INNER JOIN #ACCOUNTS_TEMP    ON CD_ACCOUNT_ID   = AC_ACCOUNT_ID
		        INNER JOIN TERMINALS   ON (CASE WHEN CD_TERMINAL IS NOT NULL THEN CD_TERMINAL ELSE @_Sorter_terminal_ID END) = TE_TERMINAL_ID
		        LEFT JOIN ACCOUNT_OPERATIONS AC ON AC.AO_OPERATION_ID = CD_OPERATION_ID
		        
		    WHERE    
			    CD_DRAW_DATETIME >= @pDrawFrom
		       AND CD_DRAW_DATETIME < @pDrawTo 		   
		       AND((@pCashierId IS NULL) OR (CM_CASHIER_ID = @pCashierId))
		       AND ((@pUserId IS NULL) OR (CM_USER_ID = @pUserId))
		       AND ( CM_TYPE = @pCashierMovement )
		       AND    te_terminal_type = @_Terminal_GamingTable_draw_type
		    ORDER BY CD_DRAW_ID
		    DROP TABLE #ACCOUNTS_TEMP
		    
		    RETURN
	  END	  
	END 
		
	--
	-- Filter by Date, but not by account neither Draw ID
	--
	IF @pDrawFrom IS NULL SET @pDrawFrom = '2007-01-01T00:00:00'
	IF @pDrawTo   IS NULL SET @pDrawTo   = GETDATE ()
	
	--No filter by draw type
	IF (@pCashDeskDraw = 1 AND @pGamingTableDraw = 1) OR (@pCashDeskDraw = 0 AND @pGamingTableDraw = 0)
	BEGIN
	  SELECT DISTINCT
	      CD_DRAW_ID
	    , CD_DRAW_DATETIME
	    , TE_TERMINAL_TYPE
	    , CM_CASHIER_NAME		  
	    , CM_USER_NAME
	    , CD_OPERATION_ID		
	    , CM_DATE
	    , CD_ACCOUNT_ID
	    , AC_HOLDER_NAME
	    , CD_COMBINATION_BET
	    , CD_COMBINATION_WON
	    , CD_RE_BET
      , CASE WHEN CD_PRIZE_ID = @pLooserPrizeId THEN AC.AO_PROMO_REDEEMABLE ELSE CD_RE_WON END CD_RE_WON
		  , CASE WHEN CD_PRIZE_ID = @pLooserPrizeId THEN AC.AO_PROMO_NOT_REDEEMABLE ELSE CD_NR_WON END CD_NR_WON
	    , CD_PRIZE_ID
	  FROM
	     CASHDESK_DRAWS WITH ( INDEX(IX_CD_DATETIME))	   
	      INNER JOIN CASHIER_MOVEMENTS WITH(INDEX(IX_CM_OPERATION_ID)) ON CD_OPERATION_ID = CM_OPERATION_ID
	      INNER JOIN ACCOUNTS    ON CD_ACCOUNT_ID   = AC_ACCOUNT_ID
	      INNER JOIN TERMINALS   ON (CASE WHEN CD_TERMINAL IS NOT NULL THEN CD_TERMINAL ELSE @_Sorter_terminal_ID END) = TE_TERMINAL_ID
	      LEFT JOIN ACCOUNT_OPERATIONS AC ON AC.AO_OPERATION_ID = CD_OPERATION_ID
  	  
	  WHERE     
		   CD_DRAW_DATETIME >= @pDrawFrom
	     AND 		CD_DRAW_DATETIME < @pDrawTo	     
	     AND 		((@pCashierId IS NULL) OR (CM_CASHIER_ID = @pCashierId))
	     AND 		((@pUserId IS NULL) OR (CM_USER_ID = @pUserId))
	     AND    ( CM_TYPE = @pCashierMovement )
	     AND    te_terminal_type IN (@_Terminal_CashDesk_draw_type, @_Terminal_GamingTable_draw_type)
	  ORDER BY CD_DRAW_ID
	  DROP TABLE #ACCOUNTS_TEMP
	  
	  RETURN
	END
	--Filter by CASH DESK draw
	ELSE IF  @pCashDeskDraw = 1 AND @pGamingTableDraw = 0
	BEGIN
      SELECT DISTINCT
	      CD_DRAW_ID
	    , CD_DRAW_DATETIME
	    , TE_TERMINAL_TYPE
	    , CM_CASHIER_NAME		  
	    , CM_USER_NAME
	    , CD_OPERATION_ID		
	    , CM_DATE
	    , CD_ACCOUNT_ID
	    , AC_HOLDER_NAME
	    , CD_COMBINATION_BET
	    , CD_COMBINATION_WON
	    , CD_RE_BET
      , CASE WHEN CD_PRIZE_ID = @pLooserPrizeId THEN AC.AO_PROMO_REDEEMABLE ELSE CD_RE_WON END CD_RE_WON
		  , CASE WHEN CD_PRIZE_ID = @pLooserPrizeId THEN AC.AO_PROMO_NOT_REDEEMABLE ELSE CD_NR_WON END CD_NR_WON
	    , CD_PRIZE_ID
	  FROM
	     CASHDESK_DRAWS WITH ( INDEX(IX_CD_DATETIME))	   
	      INNER JOIN CASHIER_MOVEMENTS WITH(INDEX(IX_CM_OPERATION_ID)) ON CD_OPERATION_ID = CM_OPERATION_ID
	      INNER JOIN ACCOUNTS          ON CD_ACCOUNT_ID   = AC_ACCOUNT_ID
	      INNER JOIN TERMINALS   ON (CASE WHEN CD_TERMINAL IS NOT NULL THEN CD_TERMINAL ELSE @_Sorter_terminal_ID END) = TE_TERMINAL_ID
	      LEFT JOIN ACCOUNT_OPERATIONS AC ON AC.AO_OPERATION_ID = CD_OPERATION_ID
  	  
	  WHERE     
		  CD_DRAW_DATETIME >= @pDrawFrom
	     AND 		CD_DRAW_DATETIME < @pDrawTo	     
	     AND 		((@pCashierId IS NULL) OR (CM_CASHIER_ID = @pCashierId))
	     AND 		((@pUserId IS NULL) OR (CM_USER_ID = @pUserId))
	     AND    ( CM_TYPE = @pCashierMovement ) 
	     AND    te_terminal_type = @_Terminal_CashDesk_draw_type
	  ORDER BY CD_DRAW_ID
	  DROP TABLE #ACCOUNTS_TEMP
	  
	  RETURN
	END
--Filter by GAMING TABLE draw
	ELSE IF  @pCashDeskDraw = 0 AND @pGamingTableDraw = 1
	BEGIN
      SELECT DISTINCT
	      CD_DRAW_ID
	    , CD_DRAW_DATETIME
	    , TE_TERMINAL_TYPE
	    , CM_CASHIER_NAME		  
	    , CM_USER_NAME
	    , CD_OPERATION_ID		
	    , CM_DATE
	    , CD_ACCOUNT_ID
	    , AC_HOLDER_NAME
	    , CD_COMBINATION_BET
	    , CD_COMBINATION_WON
	    , CD_RE_BET
      , CASE WHEN CD_PRIZE_ID = @pLooserPrizeId THEN AC.AO_PROMO_REDEEMABLE ELSE CD_RE_WON END CD_RE_WON
		  , CASE WHEN CD_PRIZE_ID = @pLooserPrizeId THEN AC.AO_PROMO_NOT_REDEEMABLE ELSE CD_NR_WON END CD_NR_WON
	    , CD_PRIZE_ID
	  FROM
	     CASHDESK_DRAWS with ( INDEX(IX_CD_DATETIME))	   
	      INNER JOIN CASHIER_MOVEMENTS WITH(INDEX(IX_CM_OPERATION_ID)) ON CD_OPERATION_ID = CM_OPERATION_ID 
	      INNER JOIN ACCOUNTS          ON CD_ACCOUNT_ID   = AC_ACCOUNT_ID
	      INNER JOIN TERMINALS   ON (CASE WHEN CD_TERMINAL IS NOT NULL THEN CD_TERMINAL ELSE @_Sorter_terminal_ID END) = TE_TERMINAL_ID
	      LEFT JOIN ACCOUNT_OPERATIONS AC ON AC.AO_OPERATION_ID = CD_OPERATION_ID
  	  
	  WHERE     
		  CD_DRAW_DATETIME >= @pDrawFrom
	     AND 		CD_DRAW_DATETIME < @pDrawTo	     
	     AND 		((@pCashierId IS NULL) OR (CM_CASHIER_ID = @pCashierId))
	     AND 		((@pUserId IS NULL) OR (CM_USER_ID = @pUserId))
	     AND    ( CM_TYPE = @pCashierMovement ) 
	     AND    te_terminal_type = @_Terminal_GamingTable_draw_type
	  ORDER BY CD_DRAW_ID
	  DROP TABLE #ACCOUNTS_TEMP	
	END	

END
GO

GRANT EXECUTE ON ReportCashDeskDraws TO wggui WITH GRANT OPTION