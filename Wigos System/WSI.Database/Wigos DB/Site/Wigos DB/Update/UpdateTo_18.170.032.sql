/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 169;

SET @New_ReleaseId = 170;
SET @New_ScriptName = N'UpdateTo_18.170.032.sql';
SET @New_Description = N'SASFlags, GamingTables an TITO updates ';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO


/******* TABLES  *******/

/* SASFlags */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[terminals]') and name = 'te_sas_flags_use_site_default')
  ALTER TABLE [dbo].terminals ADD te_sas_flags_use_site_default int NOT NULL DEFAULT (CONVERT(INT, 0xFFFFFFFF));
ELSE
  SELECT '***** Field terminals.te_sas_flags_use_site_default already exists *****';
GO

/******* RECORDS *******/

IF (NOT EXISTS (SELECT TOP 1 * FROM CAGE_AMOUNTS WHERE CAA_ISO_CODE = 'MXN' AND CAA_DENOMINATION = -200))
	INSERT INTO cage_amounts VALUES(	'MXN',	-200,	1,	0)
GO
IF (NOT EXISTS (SELECT TOP 1 * FROM CAGE_AMOUNTS WHERE CAA_ISO_CODE = 'USD' AND CAA_DENOMINATION = -200))
	INSERT INTO cage_amounts VALUES(	'USD',	-200,	1,	0)
GO
IF (NOT EXISTS (SELECT TOP 1 * FROM CAGE_AMOUNTS WHERE CAA_ISO_CODE = 'PEN' AND CAA_DENOMINATION = -200))
	INSERT INTO cage_amounts VALUES(	'PEN',	-200,	0,	0)
GO

DECLARE @MXNEnabled AS BIT
SET @MXNEnabled = 0
IF (EXISTS (SELECT CE_STATUS FROM CURRENCY_EXCHANGE WHERE CE_CURRENCY_ISO_CODE = 'MXN' AND CE_TYPE = 0))
  SET @MXNEnabled = (SELECT CE_STATUS FROM CURRENCY_EXCHANGE WHERE CE_CURRENCY_ISO_CODE = 'MXN' AND CE_TYPE = 0)

UPDATE CAGE_AMOUNTS SET CAA_ALLOWED = @MXNEnabled WHERE CAA_ISO_CODE = 'MXN'
GO

DECLARE @USDEnabled AS BIT
SET @USDEnabled = 0
IF (EXISTS (SELECT CE_STATUS FROM CURRENCY_EXCHANGE WHERE CE_CURRENCY_ISO_CODE = 'USD' AND CE_TYPE = 0))
  SET @USDEnabled = (SELECT CE_STATUS FROM CURRENCY_EXCHANGE WHERE CE_CURRENCY_ISO_CODE = 'USD' AND CE_TYPE = 0)

UPDATE CAGE_AMOUNTS SET CAA_ALLOWED = @USDEnabled WHERE CAA_ISO_CODE = 'USD'
GO

DECLARE @PENEnabled AS BIT
SET @PENEnabled = 0
IF (EXISTS (SELECT CE_STATUS FROM CURRENCY_EXCHANGE WHERE CE_CURRENCY_ISO_CODE = 'PEN' AND CE_TYPE = 0))
  SET @PENEnabled = (SELECT CE_STATUS FROM CURRENCY_EXCHANGE WHERE CE_CURRENCY_ISO_CODE = 'PEN' AND CE_TYPE = 0)

UPDATE CAGE_AMOUNTS SET CAA_ALLOWED = @PENEnabled WHERE CAA_ISO_CODE = 'PEN'
GO

DECLARE @X01Enabled AS BIT
SET @X01Enabled = 0
IF (EXISTS (SELECT CE_STATUS FROM CURRENCY_EXCHANGE WHERE CE_CURRENCY_ISO_CODE = 'X01' AND CE_TYPE = 0))
  SET @X01Enabled = (SELECT CE_STATUS FROM CURRENCY_EXCHANGE WHERE CE_CURRENCY_ISO_CODE = 'X01' AND CE_TYPE = 0)

UPDATE CAGE_AMOUNTS SET CAA_ALLOWED = @X01Enabled WHERE CAA_ISO_CODE = 'X01'
GO

-- GP NOT USED
DELETE FROM general_params WHERE gp_group_key = 'SasHost' AND gp_subject_key = 'SASFlags'
-- 
IF NOT EXISTS ( SELECT 1 FROM general_params WHERE gp_group_key = 'SasHost' AND gp_subject_key = 'SystemDefaultSasFlags')
      INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('SasHost', 'SystemDefaultSasFlags', '0')
GO

IF NOT EXISTS ( SELECT 1 FROM general_params WHERE gp_group_key = 'TITO' AND gp_subject_key = 'StackerCapacity')
			INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('TITO', 'StackerCapacity', '0')
GO

IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='PSAClient' AND GP_SUBJECT_KEY='ReportExpiredPrize')
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('PSAClient', 'ReportExpiredPrize','1')
GO

UPDATE TERMINALS SET TE_SAS_FLAGS_USE_SITE_DEFAULT = ((-1) & ~ TE_SAS_FLAGS)
GO
