/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 148;

SET @New_ReleaseId = 149;
SET @New_ScriptName = N'UpdateTo_18.149.027.sql';
SET @New_Description = N'Mailing type for WXP - Nationalities - Docs Types For scan - Address for required fields';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END
 
/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/**** INDEXES *****/


IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[cashdesk_draws]') AND name = N'IX_ACCOUNT_DATETIME')
DROP INDEX [IX_ACCOUNT_DATETIME] ON [dbo].[cashdesk_draws] WITH ( ONLINE = OFF )
GO
IF not EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[cashdesk_draws]') AND name = N'IX_cd_account_datetime')
  CREATE NONCLUSTERED INDEX [IX_cd_account_datetime] ON [dbo].[cashdesk_draws] 
  (
        [cd_account_id] ASC,
        [cd_draw_datetime] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

GO

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[cashdesk_draws]') AND name = N'IX_DRAW_DATETIME')
  DROP INDEX [IX_DRAW_DATETIME] ON [dbo].[cashdesk_draws] WITH ( ONLINE = OFF )
GO
IF  NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[cashdesk_draws]') AND name = N'IX_cd_datetime')
  CREATE NONCLUSTERED INDEX [IX_cd_datetime] ON [dbo].[cashdesk_draws] 
  (
        [cd_draw_datetime] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

GO

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[cashdesk_draws]') AND name = N'IX_ID_DATETIME')
  DROP INDEX [IX_ID_DATETIME] ON [dbo].[cashdesk_draws] WITH ( ONLINE = OFF )
GO
IF  NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[cashdesk_draws]') AND name = N'IX_cd_draw_datetime_account')
  CREATE NONCLUSTERED INDEX [IX_cd_draw_datetime_account] ON [dbo].[cashdesk_draws] 
  (
        [cd_draw_id] ASC,
        [cd_draw_datetime] ASC,
        [cd_account_id] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

GO

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[cashdesk_draws]') AND name = N'IX_UPLOAD_TO_EXT_DB')
  DROP INDEX [IX_UPLOAD_TO_EXT_DB] ON [dbo].[cashdesk_draws] WITH ( ONLINE = OFF )
GO
IF  NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[cashdesk_draws]') AND name = N'IX_cd_upload')
  CREATE NONCLUSTERED INDEX [IX_cd_upload] ON [dbo].[cashdesk_draws] 
  (
        [cd_upload_to_ext_db] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

GO


/**** PROCEDURES *****/


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ReportCashDeskDraws]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ReportCashDeskDraws]
GO
CREATE PROCEDURE [dbo].[ReportCashDeskDraws]  
  @pDrawFrom          DATETIME      = NULL,
  @pDrawTo            DATETIME      = NULL,
  @pDrawId            BIGINT          =   NULL,
  @pCashierId       INT              = NULL,
  @pUserId          INT              = NULL,
  @pSqlAccount      NVARCHAR(MAX) = NULL,
  @pCashierMovement INT           = NULL

AS
BEGIN

      SET NOCOUNT ON;

    CREATE TABLE #ACCOUNTS_TEMP (AC_ACCOUNT_ID BIGINT NOT NULL, AC_HOLDER_NAME NVARCHAR(200), AC_TRACK_DATA NVARCHAR(50))
      IF @pSqlAccount IS NOT NULL
      BEGIN
        INSERT INTO #ACCOUNTS_TEMP EXEC ('SELECT AC_ACCOUNT_ID, AC_HOLDER_NAME, AC_TRACK_DATA  FROM ACCOUNTS ' + @pSqlAccount)
            IF @@ROWCOUNT > 500
                  ALTER TABLE #ACCOUNTS_TEMP ADD PRIMARY KEY NONCLUSTERED (AC_ACCOUNT_ID)
      END

      IF    @pDrawId IS NOT NULL 
      --
      -- Filtering by Draw ID
      --
      BEGIN       

            IF @pDrawFrom IS NULL SET @pDrawFrom = '2007-01-01T00:00:00'
            IF @pDrawTo   IS NULL SET @pDrawTo   = GETDATE ()

            SELECT 
                CD_DRAW_ID
              , CD_DRAW_DATETIME
              , CM_CASHIER_NAME            
              , CM_USER_NAME
              , CD_OPERATION_ID          
              , CM_DATE
              , CD_ACCOUNT_ID
              , AC_HOLDER_NAME
              , CD_COMBINATION_BET
              , CD_COMBINATION_WON
              , CD_RE_BET
              , CD_RE_WON
              , CD_NR_WON
            FROM
               CASHDESK_DRAWS
               WITH(INDEX(IX_CD_DRAW_DATETIME_ACCOUNT))
                INNER JOIN CASHIER_MOVEMENTS WITH(INDEX(IX_CM_DATE_TYPE)) ON CD_OPERATION_ID = CM_OPERATION_ID  
                INNER JOIN ACCOUNTS          ON CD_ACCOUNT_ID   = AC_ACCOUNT_ID    
            WHERE    
                CD_DRAW_ID = @pDrawId    
               AND 
                  CD_DRAW_DATETIME >= @pDrawFrom
               AND
                  CD_DRAW_DATETIME < @pDrawTo  
               AND
                  ((@pCashierId IS NULL) OR (CM_CASHIER_ID = @pCashierId))
               AND
                  ((@pUserId IS NULL) OR (CM_USER_ID = @pUserId))
               AND   
                  ( (@pSqlAccount     IS NULL) OR (CD_ACCOUNT_ID IN ( SELECT AC_ACCOUNT_ID FROM #ACCOUNTS_TEMP ) ) )
               AND ( CM_TYPE = @pCashierMovement )            
            ORDER BY CD_DRAW_ID

            DROP TABLE #ACCOUNTS_TEMP

            RETURN
            
      END   

      IF    @pSqlAccount IS NOT NULL 
      --
      -- Filtering by account, but not by draw ID
      --
      BEGIN                   
            IF @pDrawFrom IS NULL SET @pDrawFrom = '2007-01-01T00:00:00'
            IF @pDrawTo   IS NULL SET @pDrawTo   = GETDATE ()

            SELECT 
                CD_DRAW_ID
              , CD_DRAW_DATETIME
              , CM_CASHIER_NAME            
              , CM_USER_NAME
              , CD_OPERATION_ID          
              , CM_DATE
              , CD_ACCOUNT_ID
              , AC_HOLDER_NAME
              , CD_COMBINATION_BET
              , CD_COMBINATION_WON
              , CD_RE_BET
              , CD_RE_WON
              , CD_NR_WON
            FROM
               CASHDESK_DRAWS
               WITH(INDEX(IX_CD_ACCOUNT_DATETIME))
                INNER JOIN CASHIER_MOVEMENTS WITH(INDEX(IX_CM_DATE_TYPE)) ON CD_OPERATION_ID = CM_OPERATION_ID  
                INNER JOIN #ACCOUNTS_TEMP    ON CD_ACCOUNT_ID   = AC_ACCOUNT_ID
            WHERE    
                  CD_DRAW_DATETIME >= @pDrawFrom
               AND
                  CD_DRAW_DATETIME < @pDrawTo           
               AND
                  ((@pCashierId IS NULL) OR (CM_CASHIER_ID = @pCashierId))
               AND
                  ((@pUserId IS NULL) OR (CM_USER_ID = @pUserId))
               AND ( CM_TYPE = @pCashierMovement )
            ORDER BY CD_DRAW_ID

            DROP TABLE #ACCOUNTS_TEMP

            RETURN
            
      END 

      --
      -- Filter by Date, but not by account neither Draw ID
      --
      IF @pDrawFrom IS NULL SET @pDrawFrom = '2007-01-01T00:00:00'
      IF @pDrawTo   IS NULL SET @pDrawTo   = GETDATE ()
      
      SELECT 
          CD_DRAW_ID
        , CD_DRAW_DATETIME
        , CM_CASHIER_NAME            
        , CM_USER_NAME
        , CD_OPERATION_ID          
        , CM_DATE
        , CD_ACCOUNT_ID
        , AC_HOLDER_NAME
        , CD_COMBINATION_BET
        , CD_COMBINATION_WON
        , CD_RE_BET
        , CD_RE_WON
        , CD_NR_WON
      FROM
         CASHDESK_DRAWS with ( INDEX(IX_CD_DATETIME))         
          INNER JOIN CASHIER_MOVEMENTS with(INDEX(IX_CM_DATE_TYPE)) ON CD_OPERATION_ID = CM_OPERATION_ID  
          INNER JOIN ACCOUNTS          ON CD_ACCOUNT_ID   = AC_ACCOUNT_ID    
        
      WHERE     
            CD_DRAW_DATETIME >= @pDrawFrom
         AND            CD_DRAW_DATETIME < @pDrawTo       
         AND            ((@pCashierId IS NULL) OR (CM_CASHIER_ID = @pCashierId))
         AND            ((@pUserId IS NULL) OR (CM_USER_ID = @pUserId))
         AND      ( CM_TYPE = @pCashierMovement )                
      ORDER BY CD_DRAW_ID

      DROP TABLE #ACCOUNTS_TEMP

return

END
GO

GRANT EXECUTE ON [dbo].[ReportCashDeskDraws] TO [wggui] WITH GRANT OPTION
GO

/**** RECORDS ****/

---
---
---
IF NOT EXISTS (SELECT * FROM mailing_programming WHERE mp_type = 8)
BEGIN
  INSERT INTO mailing_programming
           ([mp_name]
           ,[mp_enabled]
           ,[mp_type]
           ,[mp_address_list]
           ,[mp_subject]
           ,[mp_schedule_weekday]
           ,[mp_schedule_time_from]
           ,[mp_schedule_time_to]
           ,[mp_schedule_time_step])
     VALUES
           ('WXP - Contadores SAS'
           ,0
           ,8
           ,''
           ,''
           ,127
           ,0
           ,0
           ,0 )
END

--
-- Docs types: Licencia de conducir - 008, c�dula profesional - 009
--
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.Scan' AND GP_SUBJECT_KEY = 'Document.008.XYWH')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Cashier.Scan', 'Document.008.XYWH', '');

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.Scan' AND GP_SUBJECT_KEY = 'Document.009.XYWH')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Cashier.Scan', 'Document.009.XYWH', '');

--
--
--
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('23','en','GERMANY','GERMAN');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('23','es','ALEMANIA','ALEMANA');

INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('24','en','AUSTRIA','AUSTRIAN');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('24','es','AUSTRIA','AUSTR�ACA');

INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('25','en','BELGIUM','BELGIAN');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('25','es','B�LGICA','BELGA');

INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('26','en','BULGARIA','BULGARIAN');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('26','es','BULGARIA','B�LGARA');

INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('27','en','CYPRUS','CYPRIOT');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('27','es','CHIPRE','CHIPRIOTA');

INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('28','en','CROATIA','CROATIAN');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('28','es','CROACIA','CROATA');

INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('29','en','DENMARK','DANISH');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('29','es','DINAMARCA','DANESA');

INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('30','en','SLOVAKIA','SLOVAK');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('30','es','ESLOVAQUIA','ESLOVACA');

INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('31','en','SLOVENIA','SLOVENIAN');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('31','es','ESLOVENIA','ESLOVENA');

INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('32','en','ESTONIA','ESTONIAN');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('32','es','ESTONIA','ESTONIA');

INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('33','en','FINLAND','FINNISH');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('33','es','FINLANDIA','FINLANDESA');

INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('34','en','FRANCE','FRENCH');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('34','es','FRANCIA','FRANCESA');

INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('35','en','GREECE','GREEK');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('35','es','GRECIA','GRIEGA');

INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('36','en','HUNGARY','HUNGARIAN');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('36','es','HUNGR�A','H�NGARA');

INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('37','en','IRLAND','IRISH');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('37','es','IRLANDA','IRLANDESA');

INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('38','en','ITALY','ITALIAN');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('38','es','ITALIA','ITALIANA');

INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('39','en','LATVIA','LATVIAN');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('39','es','LETONIA','LETONA');

INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('40','en','LITHUANIA','LITHUANIAN');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('40','es','LITUANIA','LITUANA');

INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('41','en','LUXEMBOURG','LOUXEMBOURGER');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('41','es','LUXEMBURGO','LUXEMBURGUESA');

INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('42','en','MALTA','MALTESE');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('42','es','MALTA','MALTESA');

INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('43','en','NETHERLANDS','DUTCH');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('43','es','HOLANDA','HOLANDESA');

INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('44','en','POLAND','POLISH');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('44','es','POLONIA','POLACA');

INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('45','en','PORTUGAL','PORTUGUESE');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('45','es','PORTUGAL','PORTUGUESA');

INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('46','en','UNITED KINGDOM','BRITISH');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('46','es','REINO UNIDO','BRIT�NICA');

INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('47','en','CZECH REPUBLIC','CZECH');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('47','es','REPUBLICA CHECA','CHECA');

INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('48','en','ROMANIA','ROMANIAN');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('48','es','RUMANIA','RUMANA');

INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('49','en','SWEDEN','SWEDISH');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('49','es','SUECIA','SUECA');

INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('50','en','INDIA','INDIAN');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('50','es','INDIA','INDIA');

INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('51','en','CHINA','CHINESE');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('51','es','CHINA','CHINA');

INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('52','en','NORTH KOREA','NORTH KOREAN');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('52','es','COREA DEL NORTE','NORCOREANA');

INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('53','en','SOUTH KOREA','SOUTH KOREAN');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('53','es','COREA DEL SUR','SURCOREANA');

INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('54','en','JAPAN','JAPANESE');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('54','es','JAP�N','JAPONESA');
GO

--
--
--
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Account.RequestedField' AND GP_SUBJECT_KEY = 'Address')
    INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('Account.RequestedField','Address','0'); 

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Account.RequestedField' AND GP_SUBJECT_KEY = 'AntiMoneyLaundering.Address')
    INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('Account.RequestedField','AntiMoneyLaundering.Address','0'); 
GO

--
--
--
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'AntiMoneyLaundering' AND GP_SUBJECT_KEY = 'Recharge.Report.ShowMessage')
    INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('AntiMoneyLaundering','Recharge.Report.ShowMessage','1')

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'AntiMoneyLaundering' AND GP_SUBJECT_KEY = 'Prize.Report.ShowMessage')
    INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('AntiMoneyLaundering','Prize.Report.ShowMessage','1')
GO

--
--
--
IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='Cashier' AND GP_SUBJECT_KEY='Promotion.HideUNR')
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('Cashier','Promotion.HideUNR','1')
