/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 361;

SET @New_ReleaseId = 362;
SET @New_ScriptName = N'UpdateTo_18.362.041.sql';
SET @New_Description = N'Tickets Audit - HandPays AutoPay'; 

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GT_Base_Report_Data]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[GT_Base_Report_Data]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GT_Calculate_DROP_GAMBLING_TABLES]') AND type = N'FN')
    DROP FUNCTION [dbo].[GT_Calculate_DROP_GAMBLING_TABLES]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GT_Calculate_WIN]') AND type = N'FN')
    DROP FUNCTION [dbo].[GT_Calculate_WIN]
GO

/*
   PURPOSE: Calculates the DROP field in a gaming table or table type

   PARAMS:  @pCollectedAmount : total cash
			@pCollectedAmountDropBox : total cash on dropbox
            @pCollectedChipsAmount: Chips on dropbox
            @pCollectedTicketsAmount: tickets on dropbox


   RETURN:  Money: Drop
*/
CREATE FUNCTION [dbo].[GT_Calculate_DROP_GAMBLING_TABLES] (@pCollectedAmount AS MONEY, @pCollectedAmountDropBox AS MONEY, @pCollectedChipsAmountDropBox as MONEY, @pCollectedTicketsAmountDropBox as MONEY, @pIsIntegratedCashier AS BIT) RETURNS MONEY
  BEGIN
	DECLARE @drop as Money
    
    SET @drop = 0  
    
    IF @pIsIntegratedCashier = 1
    BEGIN
		RETURN @drop
	END
	        
    SET @drop = @pCollectedAmount + @pCollectedAmountDropBox + @pCollectedChipsAmountDropBox + @pCollectedTicketsAmountDropBox
    
    IF @drop < 0
      set @drop = 0

   RETURN @drop
  END
GO

-- PERMISSIONS  GT_Calculate_DROP_GAMBLING_TABLES
GRANT EXECUTE ON [dbo].[GT_Calculate_DROP_GAMBLING_TABLES] TO [wggui] WITH GRANT OPTION
GO

/*
   PURPOSE: Calculates the WIN field in a gaming table or table type

   PARAMS:  @pChipsFinalAmount: Closing chips amounts, sended from CAGE
			@pChipsInitialAmount: Opening chips amounts, sended from CAGE
			@pChipsFillAmount: Chips sended from CAGE, excluding opening
			@pChipsCreditAmount: Chips sended to CAGE, excluding closing
			@pTipsAmount: Tips
			@pOwnSales: Chips own sales
			@pExternalSales: Chips external sales
			@pCollectedAmount: Money collected
			@pCollectedAmountDropBox : total cash on dropbox
            @pCollectedChipsAmount: Chips on dropbox
            @pCollectedTicketsAmount: tickets on dropbox

   RETURN:  Money: Win
*/
CREATE FUNCTION [dbo].[GT_Calculate_WIN] ( @pChipsFinalAmount AS MONEY, @pChipsInitialAmount AS MONEY, @pChipsFillAmount AS MONEY, @pChipsCreditAmount AS MONEY, @pTipsAmount AS MONEY , @pOwnSales AS MONEY, @pExternalSales AS MONEY, @pCollectedAmount AS MONEY, @pCollectedAmountDropBox AS MONEY, @pCollectedChipsAmountDropBox as MONEY, @pCollectedTicketsAmountDropBox as MONEY, @pIsIntegratedCashier AS BIT) RETURNS MONEY
  BEGIN

  DECLARE @win as Money
  
    SET @win = (@pChipsFinalAmount - @pChipsInitialAmount) + (@pChipsCreditAmount - @pChipsFillAmount) - @pTipsAmount
    
    -- Add Drop CashDesk    
    SET @win = @win + @pOwnSales + @pExternalSales
    
    IF @pIsIntegratedCashier = 1
    BEGIN
		RETURN @win
	END
    
    -- Add Drop CollectedAmount + DropBox
    SET @win = @win + @pCollectedAmount + @pCollectedAmountDropBox + @pCollectedChipsAmountDropBox + @pCollectedTicketsAmountDropBox
    
    RETURN @win
    
  END
GO

-- PERMISSIONS  GT_Calculate_WIN]
GRANT EXECUTE ON [dbo].[GT_Calculate_WIN] TO [wggui] WITH GRANT OPTION
GO



CREATE PROCEDURE [dbo].[GT_Base_Report_Data]
(
      @pBaseType                   INTEGER
    , @pTimeInterval               INTEGER
    , @pDateFrom                   DATETIME
    , @pDateTo                     DATETIME
    , @pOnlyActivity               INTEGER
    , @pOrderBy                    INTEGER
    , @pValidTypes                 VARCHAR(4096)
    , @pSelectedCurrency           NVARCHAR(100)
    , @pTotalToSelectedCurrency    INTEGER
)
AS
BEGIN

-- DECLARATIONS
   -- @_DAYS_AND_TABLES:    A temporary table with all intervals and all base type selected (used to show base types without activity).
   DECLARE @_DAYS_AND_TABLES TABLE(DATE_TIME DATETIME, TABLE_IDENTIFIER_VALUE BIGINT, TABLE_IDENT_NAME VARCHAR(50), TABLE_TYPE_IDENT BIGINT, TABLE_TYPE_NAME VARCHAR(50))
   -- @_DAY_VAR:            Variable to create the intervals
   DECLARE @_DAY_VAR DATETIME

-- PARAMETERS
     DECLARE @_BASE_TYPE                  AS   INTEGER -- 0 BY TABLE TYPE, 1 BY TABLE
     DECLARE @_TIME_INTERVAL              AS   INTEGER -- -1, 0 DAYS, 1 MONTH, 2 YEAR
     DECLARE @_DATE_FROM                  AS   DATETIME
     DECLARE @_DATE_TO                    AS   DATETIME
     DECLARE @_ONLY_ACTIVITY              AS   INTEGER -- 0 SHOW ALL, 1 SHOW ONLY WITH ACTIVITY
     DECLARE @_ORDER_BY                   AS   INTEGER -- 0 BY INTERVAL DESC, 1 BY IDENTIFIER ASC
     DECLARE @_DELIMITER                  AS   CHAR(1)
     DECLARE @_TYPES_TABLE                TABLE(SST_ID INT, SST_VALUE VARCHAR(50)) -- TO STORE THE VALID GAMING TABLE TYPES
     DECLARE @_SELECTED_CURRENCY          AS   NVARCHAR(3)
     DECLARE @_TOTAL_TO_SELECTED_CURRENCY AS   INTEGER
     DECLARE @_CHIP_RE                    AS   INTEGER
     DECLARE @_CHIP_NRE                   AS   INTEGER
     DECLARE @_CHIP_COLOR                 AS   INTEGER
----------------------------------------------------------------------------------------------------------------

-- Initialization --
  SET @_BASE_TYPE                  =   @pBaseType
  SET @_TIME_INTERVAL              =   @pTimeInterval
  SET @_DATE_FROM                  =   @pDateFrom
  SET @_DATE_TO                    =   @pDateTo
  SET @_ONLY_ACTIVITY              =   @pOnlyActivity
  SET @_ORDER_BY                   =   @pOrderBy
  SET @_DELIMITER                  =   ','
  SET @_SELECTED_CURRENCY          =   @pSelectedCurrency
  SET @_TOTAL_TO_SELECTED_CURRENCY =   @pTotalToSelectedCurrency
  SET @_CHIP_RE                    =   1001
  SET @_CHIP_NRE                   =   1002
  SET @_CHIP_COLOR                 =   1003
----------------------------------------------------------------------------------------------------------------

-- CHECK DATE PARAMETERS
IF @_DATE_FROM IS NULL
BEGIN
   -- IF DATE FROM IS NULL, USER FIRST POSIBLE DATE
   SET @_DATE_FROM = CAST('' AS DATETIME)
   SET @pDateFrom  = CAST('' AS DATETIME)
END

IF @_DATE_TO IS NULL
BEGIN
   -- IF DATE TO IS NULL, QUERY UNTIL CURRENT DATE
   SET @_DATE_TO = CAST(GETDATE() AS DATETIME)
   SET @pDateTo  = CAST(GETDATE() AS DATETIME)
END

-- ASSIGN TYPES PARAMETER INTO TABLE
INSERT INTO @_TYPES_TABLE SELECT * FROM dbo.SplitStringIntoTable(@pValidTypes, @_DELIMITER, DEFAULT)

IF @_TIME_INTERVAL <> -1
 BEGIN
   -- INTERVALS DATES AND TABLE PREPARATION

   -- PREPARE DATES RANGE DEPENDING ON TIME INTERVAL
   SET @_DATE_FROM = CASE
                        WHEN @_TIME_INTERVAL = 0 THEN -- BY DAY
                           DATEADD(dd, 0, DATEDIFF(dd, 0, @_DATE_FROM))
                        WHEN @_TIME_INTERVAL = 1 THEN -- BY MONTH
                           CAST(CAST(YEAR(@_DATE_FROM) AS VARCHAR(4)) + RIGHT('0' + CAST(MONTH(@_DATE_FROM) AS VARCHAR(2)), 2) + '01' AS DATETIME)
                        WHEN @_TIME_INTERVAL = 2 THEN -- BY YEAR
                           CAST(CAST(YEAR(@_DATE_FROM) AS VARCHAR(4)) + '0101' AS DATETIME)
                     END

   SET @_DATE_TO = CASE
          WHEN @_TIME_INTERVAL = 0 THEN -- BY DAY
                           DATEADD(DAY, 1, CAST(@_DATE_TO AS DATETIME))
                        WHEN @_TIME_INTERVAL = 1 THEN -- BY MONTH
                           DATEADD(MONTH, 1, CAST(CAST(YEAR(@_DATE_TO) AS VARCHAR(4)) + RIGHT('0' + CAST(MONTH(@_DATE_TO) AS VARCHAR(2)), 2) + '01' AS DATETIME))
                        WHEN @_TIME_INTERVAL = 2 THEN -- BY YEAR
                           DATEADD(YEAR, 1, CAST(CAST(YEAR(@_DATE_TO) AS VARCHAR(4)) + '0101' AS DATETIME))
                     END

   -- PREPARE THE TIME INTERVALS TABLE
   SET @_DAY_VAR = @_DATE_FROM
   WHILE @_DAY_VAR < @_DATE_TO
   BEGIN
         -- SET THE LINK FIELD FOR THE RESULTS
         IF @_BASE_TYPE = 0
          BEGIN
            -- LINK WITH TABLE TYPES
            INSERT INTO @_DAYS_AND_TABLES
                 SELECT   CAST(@_DAY_VAR AS DATETIME), X.GTT_GAMING_TABLE_TYPE_ID, X.GTT_NAME AS TABLE_IDENT, NULL, NULL
                   FROM   GAMING_TABLES_TYPES AS X
                  WHERE   X.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
                          AND     X.GTT_ENABLED = 1
          END
         ELSE
          BEGIN
            -- LINK WITH TABLES
            INSERT INTO @_DAYS_AND_TABLES
                 SELECT   CAST(@_DAY_VAR AS DATETIME), X.GT_GAMING_TABLE_ID, X.GT_NAME AS TABLE_IDENT, Z.GTT_GAMING_TABLE_TYPE_ID, Z.GTT_NAME
                   FROM   GAMING_TABLES AS X
              LEFT JOIN   GAMING_TABLES_TYPES AS Z
                     ON   Z.GTT_GAMING_TABLE_TYPE_ID = X.GT_TYPE_ID
                  WHERE   Z.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
                          AND X.GT_ENABLED = 1
          END

          -- SET INCREMENT
          SET @_DAY_VAR = CASE
                               WHEN @_TIME_INTERVAL = 0 THEN DATEADD(DAY,1,@_DAY_VAR)     -- DAY
                               WHEN @_TIME_INTERVAL = 1 THEN DATEADD(MONTH,1,@_DAY_VAR)   -- MONTH
                               WHEN @_TIME_INTERVAL = 2 THEN DATEADD(YEAR,1,@_DAY_VAR)    -- YEAR
                          END
   END

END -- IF INTERVALS <> - 1
ELSE
BEGIN

   -- SET THE LINK FIELD FOR THE RESULTS WITHOUT INTERVALS
   IF @_BASE_TYPE = 0
    BEGIN
      -- LINK WITH TABLE TYPES
      INSERT INTO @_DAYS_AND_TABLES
           SELECT   NULL, X.GTT_GAMING_TABLE_TYPE_ID, X.GTT_NAME AS TABLE_IDENT, NULL, NULL
             FROM   GAMING_TABLES_TYPES AS X
            WHERE   X.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
                    AND X.GTT_ENABLED = 1
    END
   ELSE
    BEGIN
      -- LINK WITH TABLES
      INSERT INTO @_DAYS_AND_TABLES
           SELECT   NULL, X.GT_GAMING_TABLE_ID, X.GT_NAME AS TABLE_IDENT, Z.GTT_GAMING_TABLE_TYPE_ID, Z.GTT_NAME
             FROM   GAMING_TABLES AS X
        LEFT JOIN   GAMING_TABLES_TYPES AS Z
               ON   Z.GTT_GAMING_TABLE_TYPE_ID = X.GT_TYPE_ID
            WHERE   Z.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
                    AND X.GT_ENABLED = 1
    END

 END


-- SELECT INTO TEMPORARY TABLE WITH ALL DATA
-- SPECIFIC REPORT QUERY
  SELECT   X.TABLE_IDENTIFIER
         , X.TABLE_NAME
         , X.TABLE_TYPE
         , X.TABLE_TYPE_NAME
         , SUM(X.S_DROP_GAMBLING_TABLE) + SUM (X.S_DROP_CASHIER) TOTAL_DROP
         , SUM(X.S_DROP_GAMBLING_TABLE) TOTAL_DROP_GAMBLING_TABLE
         , SUM(X.S_DROP_CASHIER) TOTAL_DROP_CASHIER
         , SUM(X.S_WIN) TOTAL_WIN
         , ISNULL(SUM(X.S_TIP),0)  TOTAL_TIP
         , CASE WHEN SUM(X.S_DROP_GAMBLING_TABLE) + SUM (X.S_DROP_CASHIER) = 0 THEN 0 ELSE CAST(SUM(X.S_WIN) AS DECIMAL(18,2)) / CAST(SUM(X.S_DROP_GAMBLING_TABLE) + SUM (X.S_DROP_CASHIER) AS DECIMAL(18,2)) * 100 END AS WIN_DROP
         , MIN(X.THEORIC_HOLD) AS THEORIC_HOLD
         , CASE WHEN SUM(X.S_DROP_GAMBLING_TABLE) + SUM (X.S_DROP_CASHIER) = 0 THEN 0 ELSE SUM(CAST(X.S_TIP AS DECIMAL(18,2))) / CAST(SUM(X.S_DROP_GAMBLING_TABLE) + SUM (X.S_DROP_CASHIER) AS DECIMAL(18,2)) * 100 END AS TIP_DROP
         , SUM(X.S_WIN) + SUM(X.S_TIP) AS WIN_TIPS
         , X.CM_DATE_ONLY
         , MIN(X.OPEN_HOUR) AS OPEN_HOUR
         , MAX(X.CLOSE_HOUR) AS CLOSE_HOUR
         , SUM(X.SESSION_SECONDS) AS SESSION_SECONDS
         , COUNT(X.SESSION_SECONDS) AS SESSION_SUM         

   INTO #GT_TEMPORARY_REPORT_DATA

   FROM (
           -- CORE QUERY
            SELECT CASE WHEN @_TIME_INTERVAL = 0 THEN     -- TO FILTER BY DAY
                        DATEADD(dd, 0, DATEDIFF(dd, 0, CS_OPENING_DATE))
                      WHEN @_TIME_INTERVAL = 1 THEN     -- TO FILTER BY MONTH
                        CAST(CAST(YEAR(CS_OPENING_DATE) AS VARCHAR(4)) + RIGHT('0' + CAST(MONTH(CS_OPENING_DATE) AS VARCHAR(2)), 2) + '01' AS DATETIME)
                      WHEN @_TIME_INTERVAL = 2 THEN     -- TO FILTER BY YEAR
                        CAST(CAST(YEAR(CS_OPENING_DATE) AS VARCHAR(4)) + '01'+ '01' AS DATETIME)
                        END                                                                                       AS CM_DATE_ONLY
                     , (CASE WHEN @_BASE_TYPE = 0 THEN GTT_GAMING_TABLE_TYPE_ID ELSE GT.GT_GAMING_TABLE_ID END)   AS TABLE_IDENTIFIER -- GET THE BASE TYPE IDENTIFIER
                     , (CASE WHEN @_BASE_TYPE = 0 THEN GTT_NAME ELSE GT.GT_NAME END)                              AS TABLE_NAME       -- GET THE BASE TYPE IDENTIFIER NAME
                     , (CASE WHEN @_BASE_TYPE = 0 THEN NULL ELSE GTT_GAMING_TABLE_TYPE_ID END)                    AS TABLE_TYPE       -- TYPE
                     , (CASE WHEN @_BASE_TYPE = 0 THEN NULL ELSE GTT_NAME END)                                    AS TABLE_TYPE_NAME  -- TYPE NAME

                     ,  CASE WHEN @_TOTAL_TO_SELECTED_CURRENCY = 0 THEN
                                     DBO.GT_CALCULATE_DROP_GAMBLING_TABLES(ISNULL(GTS_COLLECTED_AMOUNT                , 0)
                                                                         , ISNULL(GTS_COLLECTED_DROPBOX_AMOUNT        , 0)
                                                                         , ISNULL(GTS_COLLECTED_DROPBOX_CHIPS_AMOUNT  , 0)
                                                                         , 0 -- It does not allow takings of dropbox with tickets
                                                                         , GT_HAS_INTEGRATED_CASHIER)
                             WHEN @_TOTAL_TO_SELECTED_CURRENCY = 1 THEN
                                     DBO.GT_CALCULATE_DROP_GAMBLING_TABLES(SUM(ISNULL(GTSC_COLLECTED_AMOUNT                , 0))
                                                                         , SUM(ISNULL(CASE WHEN GTSC_TYPE = 0            THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                                           WHEN GTSC_TYPE = 1            THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                                      ELSE 0 END,  0))
                                                                         , SUM(ISNULL(CASE WHEN GTSC_TYPE = @_CHIP_RE    THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                                           WHEN GTSC_TYPE = @_CHIP_NRE   THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                                           WHEN GTSC_TYPE = @_CHIP_COLOR THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                             ELSE 0 END,  0))
                                                                         , 0 -- It does not allow takings of dropbox with tickets
                                                                         , GT_HAS_INTEGRATED_CASHIER)
                             END                                                                                  AS S_DROP_GAMBLING_TABLE

                     ,  CASE WHEN @_TOTAL_TO_SELECTED_CURRENCY = 0 THEN
                                     DBO.GT_CALCULATE_DROP_CASHIER(ISNULL(GTS_OWN_SALES_AMOUNT, 0)
                                                                 , ISNULL(GTS_EXTERNAL_SALES_AMOUNT, 0))
                             WHEN @_TOTAL_TO_SELECTED_CURRENCY = 1 THEN
                                     DBO.GT_CALCULATE_DROP_CASHIER(SUM(ISNULL(GTSC_OWN_SALES_AMOUNT, 0))
                                                                 , SUM(ISNULL(GTSC_EXTERNAL_SALES_AMOUNT, 0)))
                             END                                                                                  AS S_DROP_CASHIER

                     , GT_THEORIC_HOLD AS THEORIC_HOLD
                     ,  CASE WHEN @_TOTAL_TO_SELECTED_CURRENCY = 0 THEN
                                     DBO.GT_CALCULATE_WIN(ISNULL(GTS_FINAL_CHIPS_AMOUNT      , 0)
                                                        , ISNULL(GTS_INITIAL_CHIPS_AMOUNT    , 0)
                                                        , ISNULL(GTS_FILLS_CHIPS_AMOUNT      , 0)
                                                        , ISNULL(GTS_CREDITS_CHIPS_AMOUNT    , 0)                                                        
                                                        , ISNULL(GTS_TIPS                    , 0)
                                                        , ISNULL(GTS_OWN_SALES_AMOUNT        , 0)
                                                        , ISNULL(GTS_EXTERNAL_SALES_AMOUNT   , 0)
                                                        , ISNULL(GTS_COLLECTED_AMOUNT        , 0)
                                                        , ISNULL(GTS_COLLECTED_DROPBOX_AMOUNT        , 0)
                                                        , ISNULL(GTS_COLLECTED_DROPBOX_CHIPS_AMOUNT  , 0)
                                                        , 0 -- It does not allow takings of dropbox with tickets)
                                                        , GT_HAS_INTEGRATED_CASHIER)
                             WHEN @_TOTAL_TO_SELECTED_CURRENCY = 1 THEN
                                     DBO.GT_CALCULATE_WIN(SUM(ISNULL(GTSC_FINAL_CHIPS_AMOUNT   , 0))
                                                        , SUM(ISNULL(GTSC_INITIAL_CHIPS_AMOUNT , 0))
                                                        , SUM(ISNULL(GTSC_FILLS_CHIPS_AMOUNT   , 0))
                                                        , SUM(ISNULL(GTSC_CREDITS_CHIPS_AMOUNT  , 0))
                                                        , SUM(ISNULL(GTSC_TIPS                 , 0))
                                                        , SUM(ISNULL(GTSC_OWN_SALES_AMOUNT     , 0))
                                                        , SUM(ISNULL(GTSC_EXTERNAL_SALES_AMOUNT, 0))
                                                        , SUM(ISNULL(GTSC_COLLECTED_AMOUNT     , 0))
                                                        , SUM(ISNULL(CASE WHEN GTSC_TYPE = 0            THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                          WHEN GTSC_TYPE = 1            THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                          ELSE 0 END,  0))
                                                        , SUM(ISNULL(CASE WHEN GTSC_TYPE = @_CHIP_RE    THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                          WHEN GTSC_TYPE = @_CHIP_NRE   THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                          WHEN GTSC_TYPE = @_CHIP_COLOR THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                          ELSE 0 END,  0))
                                                        , 0 -- It does not allow takings of dropbox with tickets
                                                        , GT_HAS_INTEGRATED_CASHIER)
                             END AS S_WIN

                     ,  CASE WHEN @_TOTAL_TO_SELECTED_CURRENCY = 0 THEN
                                     ISNULL(GTS_TIPS, 0)
                             WHEN @_TOTAL_TO_SELECTED_CURRENCY = 1 THEN
                                     ISNULL(GTSC_TIPS, 0)
                             END AS S_TIP

                     , CS.CS_OPENING_DATE                                                                         AS OPEN_HOUR
                     , CS.CS_CLOSING_DATE                                                                         AS CLOSE_HOUR
                     , DATEDIFF(SECOND, CS_OPENING_DATE, ISNULL(CS_CLOSING_DATE, GETDATE()))                      AS SESSION_SECONDS
                                   FROM   GAMING_TABLES_SESSIONS GTS
                    LEFT  JOIN   GAMING_TABLES_SESSIONS_BY_CURRENCY GTSC
                           ON    GTSC.GTSC_GAMING_TABLE_SESSION_ID = GTS.GTS_GAMING_TABLE_SESSION_ID
                          AND    GTSC_ISO_CODE IN (@_SELECTED_CURRENCY)
                  INNER JOIN   CASHIER_SESSIONS CS ON GTS.GTS_CASHIER_SESSION_ID = CS.CS_SESSION_ID
                                         AND   CS_OPENING_DATE >= @pDateFrom AND CS_OPENING_DATE < @pDateTo
                  INNER JOIN   GAMING_TABLES GT ON GTS.GTS_GAMING_TABLE_ID = GT.GT_GAMING_TABLE_ID
                           AND   GT.GT_TYPE_ID IN (SELECT SST_VALUE FROM @_TYPES_TABLE)
                  INNER JOIN   GAMING_TABLES_TYPES GTT ON GT.GT_TYPE_ID = GTT.GTT_GAMING_TABLE_TYPE_ID
               WHERE   CS_STATUS = 1  -- Only closed sessions
               GROUP BY CS_OPENING_DATE, GTT_GAMING_TABLE_TYPE_ID, GT_GAMING_TABLE_ID, GTT_NAME, GT_NAME, GTS_OWN_SALES_AMOUNT, GTS_EXTERNAL_SALES_AMOUNT, GTS_COLLECTED_AMOUNT, GT_HAS_INTEGRATED_CASHIER
                      , GTS_COLLECTED_DROPBOX_AMOUNT, GTS_COLLECTED_DROPBOX_AMOUNT, GTS_DROPBOX_ENABLED, GTS_COLLECTED_DROPBOX_CHIPS_AMOUNT, GT_THEORIC_HOLD, GTS_FINAL_CHIPS_AMOUNT, GTS_INITIAL_CHIPS_AMOUNT
                      , GTS_FILLS_CHIPS_AMOUNT, GTS_CREDITS_CHIPS_AMOUNT, GTS_TIPS, GTSC_TIPS, CS_CLOSING_DATE
          -- END CORE QUERY

        ) AS X

  GROUP BY   X.TABLE_TYPE, X.TABLE_IDENTIFIER, X.TABLE_TYPE_NAME, X.TABLE_NAME, X.CM_DATE_ONLY  -- Group by indentifier and time interval

IF @_TIME_INTERVAL <> -1
BEGIN
   -- INTERVALS DATES AND TABLE FINAL PREPARATION

   -- FILTER ACTIVITY
   IF @_ONLY_ACTIVITY = 0
    BEGIN
      -- JOIN THE SELECT WITH ALL DATA
      SELECT
              DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER,
              DT.TABLE_IDENT_NAME AS TABLE_NAME,
              DT.TABLE_TYPE_IDENT AS TABLE_TYPE,
              DT.TABLE_TYPE_NAME AS TABLE_TYPE_NAME,
              ISNULL(ZZ.TOTAL_DROP_GAMBLING_TABLE,0) AS TOTAL_DROP_GAMBLING_TABLE,
              ISNULL(ZZ.TOTAL_DROP_CASHIER,0) AS  TOTAL_DROP_CASHIER,
              ISNULL(ZZ.TOTAL_DROP, 0) AS TOTAL_DROP,
              ISNULL(ZZ.TOTAL_WIN, 0) AS TOTAL_WIN,
              ISNULL(ZZ.TOTAL_TIP, 0) AS TOTAL_TIP,
              ISNULL(ZZ.WIN_DROP, 0) AS WIN_DROP,
              ISNULL(ZZ.THEORIC_HOLD, 0) AS THEORIC_HOLD,
              ISNULL(ZZ.TIP_DROP, 0) AS TIP_DROP,
              ISNULL(ZZ.WIN_TIPS, 0) AS WIN_TIPS,
              ZZ.OPEN_HOUR,
              ZZ.CLOSE_HOUR,
              CASE WHEN ZZ.CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED,
              CASE WHEN ZZ.SESSION_SECONDS = 0 THEN DATEDIFF(SECOND,ZZ.OPEN_HOUR,GETDATE()) ELSE ISNULL(ZZ.SESSION_SECONDS, 0) END AS SESSION_SECONDS,
              ISNULL(ZZ.SESSION_SUM, 0) AS SESSION_SUM,
              DATE_TIME              

      FROM @_DAYS_AND_TABLES DT

        LEFT JOIN #GT_TEMPORARY_REPORT_DATA ZZ
                     ON
                          (
                              (@_TIME_INTERVAL = 0 AND DATE_TIME = ZZ.CM_DATE_ONLY)
                           OR (@_TIME_INTERVAL = 1 AND MONTH(DATE_TIME) = MONTH(ZZ.CM_DATE_ONLY) AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
                           OR (@_TIME_INTERVAL = 2 AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
                          )
                    AND   TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER

       -- SET ORDER
       ORDER BY   DT.TABLE_TYPE_IDENT ASC,
                  CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC,
                  DATE_TIME DESC;

    END
   ELSE
    BEGIN
      -- JOIN THE SELECT HIDDING WITHOUT ACTIVITY
      SELECT
              DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER,
              DT.TABLE_IDENT_NAME AS TABLE_NAME,
              DT.TABLE_TYPE_IDENT AS TABLE_TYPE,
              DT.TABLE_TYPE_NAME AS TABLE_TYPE_NAME,
              ISNULL(ZZ.TOTAL_DROP_GAMBLING_TABLE,0) AS TOTAL_DROP_GAMBLING_TABLE,
              ISNULL(ZZ.TOTAL_DROP_CASHIER,0) AS  TOTAL_DROP_CASHIER,
              ISNULL(ZZ.TOTAL_DROP, 0) AS TOTAL_DROP,
              ISNULL(ZZ.TOTAL_WIN, 0) AS TOTAL_WIN,
              ISNULL(ZZ.TOTAL_TIP, 0) AS TOTAL_TIP,
              ISNULL(ZZ.WIN_DROP, 0) AS WIN_DROP,
              ISNULL(ZZ.THEORIC_HOLD, 0) AS THEORIC_HOLD,
              ISNULL(ZZ.TIP_DROP, 0) AS TIP_DROP,
              ISNULL(ZZ.WIN_TIPS, 0) AS WIN_TIPS,
              ZZ.OPEN_HOUR,
              ZZ.CLOSE_HOUR,
              CASE WHEN ZZ.CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED,
              CASE WHEN ZZ.SESSION_SECONDS = 0 THEN DATEDIFF(SECOND,ZZ.OPEN_HOUR,GETDATE()) ELSE ISNULL(ZZ.SESSION_SECONDS, 0) END AS SESSION_SECONDS,
              ISNULL(ZZ.SESSION_SUM, 0) AS SESSION_SUM,
              DATE_TIME

      FROM @_DAYS_AND_TABLES DT

       INNER JOIN #GT_TEMPORARY_REPORT_DATA ZZ
                     ON
                          (
                              (@_TIME_INTERVAL = 0 AND DATE_TIME = ZZ.CM_DATE_ONLY)
                           OR (@_TIME_INTERVAL = 1 AND MONTH(DATE_TIME) = MONTH(ZZ.CM_DATE_ONLY) AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
                           OR (@_TIME_INTERVAL = 2 AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
                          )
                    AND   TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER

       -- SET ORDER
       ORDER BY   DT.TABLE_TYPE_IDENT ASC,
                  CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC,
                  DATE_TIME DESC;

    END -- IF ONLY_ACTIVITY

END
ELSE  -- ELSE WITHOUT INTERVALS
BEGIN

   -- FINAL WITHOUT INTERVALS

      -- FILTER ACTIVITY
   IF @_ONLY_ACTIVITY = 0
    BEGIN
            -- JOIN DATA INCLUDING WITHOUT ACTIVITY
            SELECT   DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER,
                     DT.TABLE_IDENT_NAME AS TABLE_NAME,
                     DT.TABLE_TYPE_IDENT AS TABLE_TYPE,
                     DT.TABLE_TYPE_NAME,
                     ISNULL(ZZ.TOTAL_DROP_GAMBLING_TABLE,0) AS TOTAL_DROP_GAMBLING_TABLE,
                     ISNULL(ZZ.TOTAL_DROP_CASHIER,0) AS  TOTAL_DROP_CASHIER,
                     ISNULL(TOTAL_DROP, 0) AS TOTAL_DROP,
                     ISNULL(TOTAL_WIN, 0) AS TOTAL_WIN,
                     ISNULL(TOTAL_TIP, 0) AS TOTAL_TIP,
                     ISNULL(WIN_DROP, 0) AS WIN_DROP,
                     ISNULL(ZZ.THEORIC_HOLD, 0) AS THEORIC_HOLD,
                     ISNULL(TIP_DROP, 0) AS TIP_DROP,
                     ISNULL(WIN_TIPS, 0) AS WIN_TIPS,
                     OPEN_HOUR,
                     CLOSE_HOUR,
                     CASE WHEN CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED,
                     CASE WHEN SESSION_SECONDS = 0 THEN DATEDIFF(SECOND, OPEN_HOUR, GETDATE()) ELSE ISNULL(SESSION_SECONDS, 0) END AS SESSION_SECONDS,
                   ISNULL(SESSION_SUM, 0) AS SESSION_SUM
              FROM   @_DAYS_AND_TABLES DT

         LEFT JOIN   #GT_TEMPORARY_REPORT_DATA ZZ
                ON   DT.TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER

                          ORDER BY   DT.TABLE_TYPE_IDENT ASC,
                     CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC,
                     DATE_TIME DESC;

     END
    ELSE
     BEGIN
            -- JOIN DATA WITH ONLY ACTIVITY
          SELECT     DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER,
                     DT.TABLE_IDENT_NAME AS TABLE_NAME,
                     DT.TABLE_TYPE_IDENT AS TABLE_TYPE,
                     DT.TABLE_TYPE_NAME,
                     ISNULL(ZZ.TOTAL_DROP_GAMBLING_TABLE,0) AS TOTAL_DROP_GAMBLING_TABLE,
                     ISNULL(ZZ.TOTAL_DROP_CASHIER,0) AS  TOTAL_DROP_CASHIER,
                     ISNULL(TOTAL_DROP, 0) AS TOTAL_DROP,
                     ISNULL(TOTAL_WIN, 0) AS TOTAL_WIN,
                     ISNULL(TOTAL_TIP, 0) AS TOTAL_TIP,
                     ISNULL(WIN_DROP, 0) AS WIN_DROP,
                     ISNULL(ZZ.THEORIC_HOLD, 0) AS THEORIC_HOLD,
                     ISNULL(TIP_DROP, 0) AS TIP_DROP,
                     ISNULL(WIN_TIPS, 0) AS WIN_TIPS,
                     OPEN_HOUR,
                     CLOSE_HOUR,
                     CASE WHEN CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED,
                     CASE WHEN SESSION_SECONDS = 0 THEN DATEDIFF(SECOND, OPEN_HOUR, GETDATE()) ELSE ISNULL(SESSION_SECONDS, 0) END AS SESSION_SECONDS,
                     ISNULL(SESSION_SUM, 0) AS SESSION_SUM
              FROM   @_DAYS_AND_TABLES DT
                     
         INNER JOIN   #GT_TEMPORARY_REPORT_DATA ZZ
                ON   DT.TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER

           ORDER BY   DT.TABLE_TYPE_IDENT ASC,
                      CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC,
                      DATE_TIME DESC;

     END
 END

-- ERASE THE TEMPORARY DATA
DROP TABLE #GT_TEMPORARY_REPORT_DATA

END -- END PROCEDURE

GO

-- PERMISSIONS
GRANT EXECUTE ON [dbo].[GT_Base_Report_Data] TO [wggui] WITH GRANT OPTION
GO
