/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 399;

SET @New_ReleaseId = 400;
SET @New_ScriptName = N'UpdateTo_18.400.041.sql';
SET @New_Description = N'S2S-Account Information'; 

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/**** GENERAL PARAM *****/

IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'Features'
                        AND GP_SUBJECT_KEY = 'CashDesk.Draw.02'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('Features','CashDesk.Draw.02','0')
END

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY = 'IsEnableParallelSession')
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Cashier', 'IsEnableParallelSession','0')
GO


/******* TABLES  *******/

/******* RECORDS *******/

/******* PROCEDURES *******/

IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'zsp_AccountInfo')
       DROP PROCEDURE zsp_AccountInfo
GO

CREATE PROCEDURE [dbo].[zsp_AccountInfo]
    @VendorId        varchar(16),
    @TrackData       varchar(50)

WITH EXECUTE AS OWNER
AS
BEGIN
  DECLARE @status_code  int
  DECLARE @status_text  varchar (254)
  DECLARE @error_text   nvarchar (MAX)
  DECLARE @input        nvarchar(MAX)
  DECLARE @output       nvarchar(MAX)
  DECLARE @_try         int
  DECLARE @_max_tries   int
  DECLARE @_exception   bit
  DECLARE @_completed   bit

  declare 
  @AccountNumber              bigint       ,
  @Name                       varchar(255) ,
  @FirstSurname               varchar(255) ,
  @SecondSurname              varchar(255) ,
  @Gender                     varchar(255) ,
  @BirthDate                  varchar(255) ,
  @Email                      varchar(255) ,
  @AuxEmail                   varchar(255) ,
  @MobileNumber               varchar(255) ,
  @FixedPhone                 varchar(255) ,
  @ClientTypeCode             varchar(255) ,
  @ClientType                 varchar(255) ,
  @FiscalCode                 varchar(255) ,
  @Nationality                varchar(255) ,
  @Street                     varchar(255) ,
  @HouseNumber                varchar(255) ,
  @Colony                     varchar(255) ,
  @State                      varchar(255) ,
  @City                       varchar(255) ,
  @DocumentType               varchar(255) ,
  @DocumentNumber             varchar(255) ,
  @LevelCode                  varchar(255) ,
  @Level                      varchar(255) ,
  @Points                     varchar(255) ,
  @Document2Type              varchar(255) ,
  @Document2Number            varchar(255) ,
  @Document3Type              varchar(255) ,
  @Document3Number            varchar(255) ,
  @CityCode					  varchar(255) ,
  @DocumentTypeCode			  varchar(255) 		

  SET @_try       = 0
  SET @_max_tries = 6        -- AJQ 22-DES-2014, The number of retries has been incremented to 6
  SET @_completed = 0

  BEGIN TRANSACTION

  WHILE (@_completed = 0)
  BEGIN

    SET @_exception   = 0
    SET @status_code  = 4
    SET @status_text  = 'Ok'
    SET @error_text   = ''
    BEGIN TRY
      SET @_try = @_try + 1

      DECLARE @_account_id BIGINT

      SET @_account_id = 0
      IF @TrackData IS NOT NULL  
      BEGIN 
        SET @_account_id = ( SELECT   AC_ACCOUNT_ID
                             FROM   ACCOUNTS
                             WHERE   AC_TRACK_DATA = dbo.TrackDataToInternal(@TrackData)
                           )
      END
      
        IF @_account_id > 0
        begin
            SELECT   @status_code  = 0,
					 @AccountNumber = isnull(ac_account_id ,0),
                     @Name = isnull(ac_holder_name3 ,''),
                     @FirstSurname = isnull(ac_holder_name1 ,''),
                     @SecondSurname = isnull(ac_holder_name2,''),
                     @Gender = isnull(ac_holder_gender ,''),
                     @BirthDate = isnull(ac_holder_birth_date,''),
                     @Email = isnull(ac_holder_email_01 ,''),
                     @AuxEmail = isnull(ac_holder_email_02 ,''),
                     @MobileNumber = isnull(ac_holder_phone_number_01 ,''),
                     @FixedPhone = isnull(ac_holder_phone_number_02 ,''),
                     @ClientTypeCode = 'F' ,
                     @ClientType = 'FISICA',
                     @FiscalCode = isnull(oc_code,'') ,
                     @Nationality = isnull(co_name,''),
                     @Street = isnull(ac_holder_address_01 ,''),
                     @HouseNumber = isnull(ac_holder_ext_num,''),
                     @Colony = isnull(ac_holder_address_02 ,''),
                     @State = isnull(fs_name ,''),
                     @City = isnull(ac_holder_city ,''),
                     @DocumentType = isnull(identification_types.idt_name,'') ,
                     @DocumentNumber = isnull(ac_holder_id,''),
                     @LevelCode = isnull(ac_holder_level,''),
                     @Level = isnull((select top 1 gp_key_value from general_params where gp_group_key='PlayerTracking' and gp_subject_key='Level0'+ cast(ac_holder_level as varchar(2)) +'.Name'),''),
                     @Points = isnull(ac_points,''),
                     @Document2Type = isnull(idt2.idt_name,'') ,
                     @Document2Number = isnull(ac_holder_id2,''),
                     @Document3Type = isnull(idt3.idt_name,'') ,
                     @Document3Number = isnull(ac_holder_id3,''),
                     @CityCode = ISNULL(ac_holder_zip, ''),
                     @DocumentTypeCode = ISNULL(identification_types.idt_id, '')

              FROM   accounts
              LEFT JOIN countries on ac_holder_nationality = co_country_id and co_language_id = 10
              LEFT JOIN federal_states on ac_holder_fed_entity = fs_state_id
              LEFT JOIN identification_types on ac_holder_id_type = identification_types.idt_id
              LEFT JOIN identification_types idt2 on ac_holder_id2_type = idt2.idt_id
              LEFT JOIN identification_types idt3 on ac_holder_id3_type = idt3.idt_id
              LEFT JOIN occupations on oc_id = ac_holder_occupation_id
            WHERE   ac_account_id = @_account_id

        end

      SET @_completed = 1

    END TRY
    BEGIN CATCH
    
      ROLLBACK TRANSACTION
      set @AccountNumber =0
      IF (@_try >= @_max_tries) 
      BEGIN
        SET @_completed  = 1;
        SET @_exception  = 1;
        SET @status_code = 4;
        SET @status_text = 'Access Denied';
        SET @error_text  = ' ERROR_NUMBER: '    + CAST(ERROR_NUMBER()               AS NVARCHAR)
                         + ' ERROR_SEVERITY: '  + CAST(ERROR_SEVERITY()             AS NVARCHAR)
                         + ' ERROR_STATE: '     + CAST(ERROR_STATE()                AS NVARCHAR)
                         + ' ERROR_PROCEDURE: ' + CAST(ISNULL(ERROR_PROCEDURE(),'') AS NVARCHAR)
                         + ' ERROR_LINE: '      + CAST(ERROR_LINE()                 AS NVARCHAR)
                         + ' ERROR_MESSAGE: '   + CAST(ERROR_MESSAGE()              AS NVARCHAR(MAX))
      END
      ELSE
      BEGIN
        WAITFOR DELAY '00:00:02'
      END

      BEGIN TRANSACTION

    END CATCH
  END

  SET @input = '@TrackData='       + @TrackData 
               +';VenbdorId='      + @VendorId
			   
  IF @error_text <> ''
    SET @error_text = ';Details='      + @error_text


  SET @output = 'StatusCode='      + CAST (@status_code     AS NVARCHAR)
              +';StatusText='      + @status_text
              + @error_text
              + '; TryIndex='      + CAST (@_try AS NVARCHAR)

  EXECUTE dbo.zsp_Audit 'zsp_GetAccountInfo', @AccountNumber, 0, 0, 0, 0, @status_code, 0, 1, @input, @output

  COMMIT TRANSACTION
      SELECT 
      isnull(      @status_code      ,1)       as StatusCode,
      isnull(      @status_text      ,'')      as StatusText,
      isnull(      @AccountNumber    ,0)       as  AccountNumber     ,
      isnull(      @Name             ,'')      as  Name             , 
      isnull(      @FirstSurname     ,'')      as  FirstSurname     , 
      isnull(      @SecondSurname    ,'')      as  SecondSurname    , 
      isnull(      @Gender           ,'')      as  Gender           , 
      isnull(      @BirthDate        ,'')      as  BirthDate        , 
      isnull(      @Email            ,'')      as  Email            , 
      isnull(      @AuxEmail         ,'')      as  AuxEmail         , 
      isnull(      @MobileNumber     ,'')      as  MobileNumber     , 
      isnull(      @FixedPhone       ,'')      as  FixedPhone       , 
      isnull(      @ClientTypeCode   ,'')      as  ClientTypeCode   , 
      isnull(      @ClientType       ,'')      as  ClientType       , 
      isnull(      @FiscalCode       ,'')      as  FiscalCode       , 
      isnull(      @Nationality      ,'')      as  Nationality      , 
      isnull(      @Street           ,'')      as  Street           , 
      isnull(      @HouseNumber      ,'')      as  HouseNumber      , 
      isnull(      @Colony           ,'')      as  Colony           , 
      isnull(      @State            ,'')      as  [State]          , 
      isnull(      @City             ,'')      as  City             , 
      isnull(      @DocumentType     ,'')      as  DocumentType     , 
      isnull(      @DocumentNumber   ,'')      as  DocumentNumber   ,
      isnull(      @LevelCode        ,'')      as  LevelCode		 ,
      isnull(      @Level            ,'')      as  [Level]			 ,
      isnull(      @Points           ,'')      as  Points			 ,
      isnull(      @Document2Type    ,'')      as  Document2Type     , 
      isnull(      @Document2Number  ,'')      as  Document2Number   ,
      isnull(      @Document3Type    ,'')      as  Document3Type     , 
      isnull(      @Document3Number  ,'')      as  Document3Number   ,
      isnull(      @CityCode         ,'')      as  CityCode          ,
      isnull(      @DocumentTypeCode ,'')      as  DocumentTypeCode

END -- zsp_SessionStart

GO

-- [3GS]
GRANT EXECUTE ON [dbo].[zsp_AccountInfo] TO [3GS] WITH GRANT OPTION 
GO

-- [EIBE]
GRANT EXECUTE ON [dbo].[zsp_AccountInfo] TO [EIBE] WITH GRANT OPTION 
GO

-- [wg_interface]
GRANT EXECUTE ON [dbo].[zsp_AccountInfo] TO [wg_interface] WITH GRANT OPTION 
GO
/******* TRIGGERS *******/