/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 33;

SET @New_ReleaseId = 34;
SET @New_ScriptName = N'UpdateTo_18.034.006.sql';
SET @New_Description = N'Tickets y Reports Palacio de los N�meros.';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/

/* Cashier Vouchers */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[cashier_vouchers]') and name = 'cv_sequence')
  ALTER TABLE [dbo].[cashier_vouchers]
          ADD [cv_sequence] [bigint] NULL;
ELSE
  SELECT '***** Field cashier_vouchers.cv_sequence already exists *****';

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[cashier_vouchers]') and name = 'cv_mode')
  ALTER TABLE [dbo].[cashier_vouchers]
          ADD [cv_mode] [int] NULL;
ELSE
  SELECT '***** Field cashier_vouchers.cv_mode already exists *****';

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[cashier_vouchers]') and name = 'cv_m01_dev')
  ALTER TABLE [dbo].[cashier_vouchers]
          ADD [cv_m01_dev] [money] NULL;
ELSE
  SELECT '***** Field cashier_vouchers.cv_m01_dev already exists *****';

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[cashier_vouchers]') and name = 'cv_m01_base')
  ALTER TABLE [dbo].[cashier_vouchers]
          ADD [cv_m01_base] [money] NULL;
ELSE
  SELECT '***** Field cashier_vouchers.cv_m01_base already exists *****';

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[cashier_vouchers]') and name = 'cv_m01_tax1')
  ALTER TABLE [dbo].[cashier_vouchers]
          ADD [cv_m01_tax1] [money] NULL;
ELSE
  SELECT '***** Field cashier_vouchers.cv_m01_tax1 already exists *****';

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[cashier_vouchers]') and name = 'cv_m01_tax2')
  ALTER TABLE [dbo].[cashier_vouchers]
          ADD [cv_m01_tax2] [money] NULL;
ELSE
  SELECT '***** Field cashier_vouchers.cv_m01_tax2 already exists *****';

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[cashier_vouchers]') and name = 'cv_m01_final')
  ALTER TABLE [dbo].[cashier_vouchers]
          ADD [cv_m01_final] [money] NULL;
ELSE
  SELECT '***** Field cashier_vouchers.cv_m01_final already exists *****';

/****** Object:  Table [dbo].[sequences]    Script Date: 05/25/2011 12:53:51 ******/
IF NOT EXISTS (SELECT * FROM SYS.OBJECTS WHERE TYPE = 'U' AND NAME = 'sequences')
  CREATE TABLE [dbo].[sequences](
      [seq_id] [int] NOT NULL,
      [seq_next_value] [bigint] NOT NULL,
  CONSTRAINT [PK_sequences] PRIMARY KEY CLUSTERED 
  (
      [seq_id] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
  ) ON [PRIMARY];

/****** RECORDS ******/

/* Cashier.Voucher */
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.Voucher.Mode.01' AND GP_SUBJECT_KEY ='Branch')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key]
             ,[gp_subject_key]
             ,[gp_key_value])
       VALUES
             ('Cashier.Voucher.Mode.01'
             ,'Branch'
             ,'');
ELSE
  SELECT '***** Record Cashier.Voucher.Mode.01.Branch already exists *****';

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.Voucher.Mode.01' AND GP_SUBJECT_KEY ='Series.A')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key]
             ,[gp_subject_key]
             ,[gp_key_value])
       VALUES
             ('Cashier.Voucher.Mode.01'
             ,'Series.A'
             ,'');
ELSE
  SELECT '***** Record Cashier.Voucher.Mode.01.Series.A already exists *****';

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.Voucher.Mode.01' AND GP_SUBJECT_KEY ='Series.B')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key]
             ,[gp_subject_key]
             ,[gp_key_value])
       VALUES
             ('Cashier.Voucher.Mode.01'
             ,'Series.B'
             ,'');
ELSE
  SELECT '***** Record Cashier.Voucher.Mode.01.Series.B already exists *****';

