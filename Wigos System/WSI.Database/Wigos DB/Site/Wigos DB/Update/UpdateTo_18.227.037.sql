/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 226;

SET @New_ReleaseId = 227;
SET @New_ScriptName = N'UpdateTo_18.227.037.sql';
SET @New_Description = N'GP Added: GamingTables - Cashier.ChipsSaleEnabled, zsp_SessionEnd update.';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/******* TABLES *******/

/******* INDEXES *******/

/******* RECORDS *******/

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'GamingTables' AND GP_SUBJECT_KEY = 'Cashier.ChipsSaleEnabled')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('GamingTables', 'Cashier.ChipsSaleEnabled', '1')
GO

/******* STORED PROCEDURES *******/

--------------------------------------------------------------------------------
-- PURPOSE: End play session
--
--  PARAMS:
--      - INPUT:
--          @AccountId      varchar(24)
--          @VendorId       varchar(16)
--          @SerialNumber   varchar(30)
--          @MachineNumber  int
--          @SessionId      bigint
--          @AmountPlayed   money
--          @AmountWon      money
--          @GamesPlayed    int
--          @GamesWon       int
--          @CreditBalance  money,
--          @CurrentJackpot money = 0
--
--      - OUTPUT:
--
-- RETURNS:
--      StatusCode
--      StatusText
--
--   NOTES:
--
ALTER PROCEDURE [dbo].[zsp_SessionEnd]
  @AccountId      varchar(24),
  @VendorId       varchar(16),
  @SerialNumber   varchar(30),
  @MachineNumber  int,
  @SessionId      bigint,
  @AmountPlayed   money,
  @AmountWon      money,
  @GamesPlayed    int,
  @GamesWon       int,
  @CreditBalance  money,
  @CurrentJackpot money = 0
WITH EXECUTE AS OWNER
AS
BEGIN

  BEGIN TRANSACTION

  DECLARE @status_code  int
  DECLARE @status_text  varchar (254)
  DECLARE @error_text   nvarchar (MAX)

  SET @status_code  = 4
  SET @status_text  = 'Access Denied'
  SET @error_text   = ''

  BEGIN TRY

    EXECUTE dbo.Trx_3GS_EndCardSession @VendorId, @SerialNumber, @MachineNumber,
                                       @AccountID,
                                       @SessionId,
                                       @AmountPlayed, @AmountWon, @GamesPlayed, @GamesWon,
                                       @CreditBalance,
                                       @status_code OUTPUT, @status_text OUTPUT, @error_text OUTPUT

    GOTO LABEL_AUDIT

  END TRY

  BEGIN CATCH

    SET @status_code = 4;
    SET @status_text = 'Access Denied';
    SET @error_text  = ' ERROR_NUMBER: '    + CAST(ERROR_NUMBER()               AS NVARCHAR)
                     + ' ERROR_SEVERITY: '  + CAST(ERROR_SEVERITY()             AS NVARCHAR)
                     + ' ERROR_STATE: '     + CAST(ERROR_STATE()                AS NVARCHAR)
                     + ' ERROR_PROCEDURE: ' + CAST(ISNULL(ERROR_PROCEDURE(),'') AS NVARCHAR)
                     + ' ERROR_LINE: '      + CAST(ERROR_LINE()                 AS NVARCHAR)
                     + ' ERROR_MESSAGE: '   + CAST(ERROR_MESSAGE()              AS NVARCHAR(MAX))

  END CATCH

LABEL_ERROR:
  ROLLBACK TRANSACTION
  BEGIN TRANSACTION

LABEL_AUDIT:

  DECLARE @input  AS nvarchar(MAX)
  DECLARE @output AS nvarchar(MAX)

  SET @input = '@AccountID='      + @AccountID
             +';@VendorId='       + @VendorId
             +';@SerialNumber='   + CAST (@SerialNumber   AS NVARCHAR)
             +';@MachineNumber='  + CAST (@MachineNumber  AS NVARCHAR)
             +';@SessionId='      + CAST (@SessionId      AS NVARCHAR)
             +';@AmountPlayed='   + CAST (@AmountPlayed   AS NVARCHAR)
             +';@AmountWon='      + CAST (@AmountWon      AS NVARCHAR)
             +';@GamesPlayed='    + CAST (@GamesPlayed    AS NVARCHAR)
             +';@GamesWon='       + CAST (@GamesWon       AS NVARCHAR)
             +';@CreditBalance='  + CAST (@CreditBalance  AS NVARCHAR)
             +';@CurrentJackpot=' + CAST (@CurrentJackpot AS NVARCHAR)

  IF @error_text <> ''
    SET @error_text = ';Details='     + @error_text

  SET @output = 'StatusCode='     + CAST (@status_code    AS NVARCHAR)
              +';StatusText='     + @status_text
              + @error_text

  EXECUTE dbo.zsp_Audit 'zsp_SessionEnd', @AccountID, @VendorId, @SerialNumber, @MachineNumber,
                                          @SessionId, @status_code, @CreditBalance, 1,
                                          @input, @output

  COMMIT TRANSACTION

  -- AJQ 21-AUG-2014, Always return 0 "Success" to the caller.
  --                  This is to avoid retries from the client side.
  SET  @status_code = 0

  SELECT @status_code AS StatusCode, @status_text AS StatusText

END -- zsp_SessionEnd

GO
