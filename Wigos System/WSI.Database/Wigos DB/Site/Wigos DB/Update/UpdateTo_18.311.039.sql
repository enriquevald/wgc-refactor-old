/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 310;

SET @New_ReleaseId = 311;
SET @New_ScriptName = N'UpdateTo_18.311.039.sql';
SET @New_Description = N'Create Gamegateway tables';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/******* TABLES  *******/

-- GAMEGATEWAY_COMMAND_MESSAGES
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[gamegateway_command_messages]') AND type in (N'U'))
  DROP TABLE gamegateway_command_messages
GO

CREATE TABLE gamegateway_command_messages(
	gcm_id             BIGINT         NOT NULL IDENTITY (1, 1),
	gcm_terminal_id    INT            NOT NULL,	
	gcm_code           INT            NOT NULL,	
	gcm_type           INT            NOT NULL,	
	gcm_parameter      NVARCHAR(MAX)  NULL,	
	gcm_status         INT            NOT NULL,	
	gcm_created        DATETIME       NOT NULL DEFAULT GETDATE(),	
	gcm_status_changed DATETIME       NULL,	
	gcm_response       NVARCHAR(MAX)  NULL
	
 CONSTRAINT pk_gamegateway_command_messages PRIMARY KEY CLUSTERED 
(
	gcm_id ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO 

/******* INDEXES *******/

--INDEX 
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[gamegateway_command_messages]') AND type in (N'U'))
BEGIN
  IF  NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[gamegateway_command_messages]') AND name = N'IX_gcm_type_gcm_status_gcm_status_changed')
    CREATE NONCLUSTERED INDEX [IX_gcm_type_gcm_status_gcm_status_changed]
    ON [dbo].[gamegateway_command_messages] ([GCM_TYPE],[GCM_STATUS],[GCM_STATUS_CHANGED])
END 

GO

/******* RECORDS *******/

DELETE FROM CUSTOMER_BUCKET_BY_GAMING_DAY WHERE CBUD_VALUE = 0 AND CBUD_VALUE_ADDED = 0 AND CBUD_VALUE_SUBSTRACTED = 0
GO

/******* PROCEDURES *******/

ALTER PROCEDURE [dbo].[WSP_AccountPendingHandpays]
  @AccountId  BigInt
AS
BEGIN
	SET NOCOUNT ON;

  DECLARE @TypeCancelledCredits AS Int
  DECLARE @TypeJackpot AS Int
  DECLARE @TypeSiteJackpot AS Int
  DECLARE @TypeOrphanCredits AS Int
  DECLARE @oldest_handpay  AS Datetime
  DECLARE @candidate_exists as bit

  DECLARE @TimePeriod as int
  DECLARE @oldest_session_start as Datetime
  DECLARE @newest_session_end   as Datetime
  DECLARE @count                as int
  DECLARE @now   as Datetime

  DECLARE @cph_terminal_id as int
  DECLARE @cph_datetime as Datetime
  DECLARE @cph_amount as Money
  DECLARE @expired as int

  --
  -- Handpay types
  --
  SET @TypeCancelledCredits = 0
  SET @TypeJackpot          = 1
  SET @TypeSiteJackpot      = 20
  SET @TypeOrphanCredits    = 2

  SET @expired              = 16384 -- 0x4000 Expired

  --
  -- Oldest Handpay 'Not expired'
  --
  SET @now = GETDATE()

  SELECT   @TimePeriod              = CAST (GP_KEY_VALUE AS INT)
    FROM   GENERAL_PARAMS
   WHERE   GP_GROUP_KEY             = 'Cashier.HandPays'
     AND   GP_SUBJECT_KEY           = 'TimePeriod'
     AND   ISNUMERIC (GP_KEY_VALUE) = 1 --- Ensure is a numeric value

  SET @TimePeriod = ISNULL (@TimePeriod, 60)      -- Default value: 1 hour
  IF ( @TimePeriod <   5 ) SET @TimePeriod =   5  -- Minimum: 5 minutes
  IF ( @TimePeriod > 2500 ) SET @TimePeriod = 2500  -- Maximum: 40 h x 60 min/h = 2500 min approximately

  SET @oldest_handpay = DATEADD (MINUTE, -@TimePeriod, @now)

  --
  -- Pending Handpays
  --
  SELECT   *
    INTO   #PENDING_HANDPAYS
    FROM   HANDPAYS 
   WHERE   HP_MOVEMENT_ID IS NULL
     AND   HP_DATETIME >= @oldest_handpay
     AND   HP_STATUS_CALCULATED <> @expired
     AND   ( HP_TYPE IN (@TypeCancelledCredits, @TypeJackpot, @TypeOrphanCredits)
        OR ( HP_TYPE = @TypeSiteJackpot AND HP_SITE_JACKPOT_AWARDED_TO_ACCOUNT_ID = @AccountId ) )

  ---
  --- Loop on pending Handpays (excluding 'SiteJackpot')
  ---
  DECLARE   cursor_pending_handpays CURSOR FOR
   SELECT   HP_TERMINAL_ID, HP_DATETIME, HP_AMOUNT
      FROM  #PENDING_HANDPAYS
    WHERE   HP_TYPE <> @TypeSiteJackpot
    
  OPEN cursor_pending_handpays
  FETCH NEXT FROM cursor_pending_handpays INTO @cph_terminal_id, @cph_datetime, @cph_amount

  WHILE @@Fetch_Status = 0
  BEGIN
    
    SET @oldest_session_start = DATEADD (DAY, -1, @cph_datetime)
    SET @newest_session_end   = DATEADD (MINUTE, @TimePeriod, @cph_datetime)

    SELECT   @count = COUNT(*)
      FROM   PLAY_SESSIONS
     WHERE   PS_TERMINAL_ID = @cph_terminal_id
       AND   PS_ACCOUNT_ID  = @AccountId
       AND   PS_STARTED     >= @oldest_session_start
       AND   PS_STARTED     <  @cph_datetime
       AND   PS_STARTED     < PS_FINISHED -- Exclude 'paid handpays' sessions 
       AND   PS_FINISHED    >= @oldest_session_start
       AND   PS_FINISHED    <  @newest_session_end

    IF ( @count = 0 )
    BEGIN
      ---
      --- Not a candidate: delete it!
      ---
      DELETE   #PENDING_HANDPAYS
       WHERE   HP_TERMINAL_ID = @cph_terminal_id
         AND   HP_DATETIME    = @cph_datetime
         AND   HP_AMOUNT      = @cph_amount
    END

    --
    -- Next
    --
    FETCH NEXT FROM cursor_pending_handpays INTO @cph_terminal_id, @cph_datetime, @cph_amount

  END

  CLOSE cursor_pending_handpays
  DEALLOCATE cursor_pending_handpays

  SELECT   HP_TERMINAL_ID 
         , HP_DATETIME 
         , HP_TE_PROVIDER_ID 
         , HP_TE_NAME 
         , HP_TYPE 
         , ' '  HP_DUMMY
         , ISNULL (HP_SITE_JACKPOT_NAME, ' ')  HP_SITE_JACKPOT_NAME
         , HP_AMOUNT
         , ISNULL (HP_SITE_JACKPOT_AWARDED_ON_TERMINAL_ID, 0)  HP_SITE_JACKPOT_AWARDED_ON_TERMINAL_ID  
         , HP_AMT0 -- NEW       
    FROM   #PENDING_HANDPAYS
   ORDER BY HP_DATETIME DESC

  DROP TABLE #PENDING_HANDPAYS

END -- WSP_AccountPendingHandpays
GO
