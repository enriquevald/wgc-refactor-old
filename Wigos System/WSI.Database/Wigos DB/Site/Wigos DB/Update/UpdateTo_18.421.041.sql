/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 420;

SET @New_ReleaseId = 421;

SET @New_ScriptName = N'UpdateTo_18.421.041.sql';
SET @New_Description = N'New release v03.006.0022'; 

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO



/**** GENERAL PARAM *****/


/******* TABLES  *******/


/******* RECORDS  *******/

BEGIN
  DECLARE @rtc_form_id INT
  DECLARE @rtc_design_sheet VARCHAR(MAX)
  DECLARE @rtc_report_name VARCHAR(MAX)
  
  SET @rtc_report_name =      '<LanguageResources><NLS09><Label>Cage movements for currency</Label></NLS09><NLS10><Label>Movimientos de b�veda por divisa</Label></NLS10></LanguageResources>'                                                              
  SET @rtc_design_sheet =     '<ArrayOfReportToolDesignSheetsDTO>
                                 <ReportToolDesignSheetsDTO>
                                   <LanguageResources>
                                     <NLS09>
                                       <Label>Cage movements for currency</Label>
                                     </NLS09>
                                     <NLS10>
                                       <Label>Movimientos por divisa</Label>
                                     </NLS10>
                                   </LanguageResources>
                                   <Columns>
                                     <ReportToolDesignColumn>
                                       <Code>Fecha</Code>
                                       <Width>300</Width>
                                       <EquityMatchType>Equality</EquityMatchType>
                                       <LanguageResources>
                                         <NLS09>
                                           <Label>Date</Label>
                                         </NLS09>
                                         <NLS10>
                                           <Label>Fecha</Label>
                                         </NLS10>
                                      </LanguageResources>
                                    </ReportToolDesignColumn>
                                    <ReportToolDesignColumn>
                                     <Code>Usuario de B�veda</Code>
                                     <Width>300</Width>
                                     <EquityMatchType>Equality</EquityMatchType>
                                     <LanguageResources>
                                       <NLS09>
                                         <Label>Cash cage user</Label>
                                       </NLS09>
                                       <NLS10>
                                         <Label>Usuario de B�veda</Label>
                                       </NLS10>
                                     </LanguageResources>
                                   </ReportToolDesignColumn>
                                   <ReportToolDesignColumn>
                                     <Code>Origen/Destino</Code>
                                     <Width>300</Width>
                                     <EquityMatchType>Equality</EquityMatchType>
                                     <LanguageResources>
                                       <NLS09>
                                         <Label>Source/Destination</Label>
                                       </NLS09>
                                       <NLS10>
                                         <Label>Origen/Destino</Label>
                                       </NLS10>
                                     </LanguageResources>
                                   </ReportToolDesignColumn>
                                   <ReportToolDesignColumn>
                                     <Code>Tipo de movimiento</Code>
                                     <Width>300</Width>
                                     <EquityMatchType>Equality</EquityMatchType>
                                     <LanguageResources>
                                       <NLS09>
                                         <Label>Type</Label>
                                       </NLS09>
                                       <NLS10>
                                         <Label>Tipo de movimiento</Label>
                                       </NLS10>
                                     </LanguageResources>
                                   </ReportToolDesignColumn>
                                   <ReportToolDesignColumn>
                                     <Code>Estado</Code>
                                     <Width>300</Width>
                                     <EquityMatchType>Equality</EquityMatchType>
                                     <LanguageResources>
                                       <NLS09>
                                         <Label>Status</Label>
                                       </NLS09>
                                       <NLS10>
                                         <Label>Estado</Label>
                                       </NLS10>
                                     </LanguageResources>
                                   </ReportToolDesignColumn>
                                   <ReportToolDesignColumn>
                                     <Code>Otros</Code>
                                     <Width>300</Width>
                                     <EquityMatchType>Contains</EquityMatchType>
                                     <LanguageResources>
                                       <NLS09>
                                         <Label>Other</Label>
                                       </NLS09>
                                       <NLS10>
                                         <Label>Otros</Label>
                                       </NLS10>
                                     </LanguageResources>
                                   </ReportToolDesignColumn>
                                   <ReportToolDesignColumn>
                                     <Code>Fichas</Code>
                                     <Width>300</Width>
                                     <EquityMatchType>Equality</EquityMatchType>
                                     <LanguageResources>
                                       <NLS09>
                                         <Label>Chips</Label>
                                       </NLS09>
                                     <NLS10>
                                       <Label>Fichas</Label>
                                     </NLS10>
                                   </LanguageResources>
                                 </ReportToolDesignColumn>
                               </Columns>
                             </ReportToolDesignSheetsDTO>
                           </ArrayOfReportToolDesignSheetsDTO>'
  
  IF NOT EXISTS(SELECT 1 FROM report_tool_config WHERE rtc_store_name = 'GetCageMovementType_GR')
  BEGIN
    SELECT @rtc_form_id = 11000
      FROM report_tool_config
    
    INSERT INTO [dbo].[REPORT_TOOL_CONFIG] 
			(RTC_FORM_ID,
			RTC_LOCATION_MENU,
			RTC_REPORT_NAME,
			RTC_STORE_NAME,
			RTC_DESIGN_FILTER,
			RTC_DESIGN_SHEETS,
			RTC_MAILING,
			RTC_STATUS,
			RTC_MODE_TYPE,
      RTC_HTML_HEADER,
      RTC_HTML_FOOTER)
		VALUES
      (@rtc_form_id,
      7, 
      @rtc_report_name,
      'GetCageMovementType_GR',
      '<ReportToolDesignFilterDTO><FilterType>FromToDate</FilterType></ReportToolDesignFilterDTO>',
      @rtc_design_sheet,
      1,
      1,
      1,
      NULL,
      NULL)
                                      
  END 
  ELSE
  BEGIN
      UPDATE [dbo].[REPORT_TOOL_CONFIG] 
      SET RTC_REPORT_NAME =         /*RTC_REPORT_NAME*/     @rtc_report_name,
          RTC_DESIGN_SHEETS =       /*RTC_DESIGN_SHEETS*/   @rtc_design_sheet
      WHERE RTC_STORE_NAME = 'GetCageMovementType_GR'
  END
END
GO


/******* STORED PROCEDURES  *******/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GenerateTablesActivity]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[sp_GenerateTablesActivity]
GO

CREATE PROCEDURE [dbo].[sp_GenerateTablesActivity]
AS
BEGIN

      DECLARE @CurrentDay AS DATETIME
      SELECT @CurrentDay = [dbo].[TodayOpening] (1)

      SET NOCOUNT ON;


      -- Insert all gaming tables to terminals connected with default values to 0
      INSERT INTO   GAMING_TABLES_CONNECTED
            SELECT   GT_GAMING_TABLE_ID, CONVERT(INT,CONVERT(VARCHAR(8), @CurrentDay, 112)), 0, 0
               FROM   GAMING_TABLES
        LEFT JOIN   GAMING_TABLES_CONNECTED ON GMC_GAMINGTABLE_ID = GT_GAMING_TABLE_ID AND GMC_GAMING_DAY = CONVERT(INT,CONVERT(VARCHAR(8), @CurrentDay, 112))
              WHERE   GMC_GAMING_DAY IS NULL

    -- Update Enabled
       UPDATE   GAMING_TABLES_CONNECTED
              SET   GMC_ENABLED = (CASE WHEN S.S_ENABLED = 1 OR GMC_ENABLED = 1 THEN 1 ELSE 0 END)
             FROM 
                  (
                          SELECT   GT_GAMING_TABLE_ID AS S_GAMING_TABLE_ID
                                    , CONVERT(INT,CONVERT(VARCHAR(8), @CurrentDay, 112)) AS S_GAMING_DAY
                                    , GT_ENABLED AS S_ENABLED
                            FROM   GAMING_TABLES
                    INNER JOIN   GAMING_TABLES_CONNECTED ON GMC_GAMINGTABLE_ID = GT_GAMING_TABLE_ID AND GMC_GAMING_DAY = CONVERT(INT,CONVERT(VARCHAR(8), @CurrentDay, 112))                  
                      GROUP BY   GT_GAMING_TABLE_ID, GT_ENABLED  
                  )S
            WHERE   S.S_GAMING_TABLE_ID = GAMING_TABLES_CONNECTED.GMC_GAMINGTABLE_ID AND GMC_GAMING_DAY = S_GAMING_DAY
    
    -- Update Used    
             UPDATE   GAMING_TABLES_CONNECTED
                SET   GMC_USED = CASE WHEN S.S_USED = 1 OR GMC_USED = 1 THEN 1 ELSE 0 END
               FROM 
                  (
                          SELECT   GT_GAMING_TABLE_ID AS S_GAMING_TABLE_ID
                                    , CONVERT(INT,CONVERT(VARCHAR(8), @CurrentDay, 112)) AS S_GAMING_DAY
                                    , CASE WHEN MAX(GTS.GTS_CASHIER_SESSION_ID) > 0 THEN 1 ELSE 0 END AS S_USED
                            FROM   GAMING_TABLES
                  INNER JOIN   GAMING_TABLES_CONNECTED ON GMC_GAMINGTABLE_ID = GT_GAMING_TABLE_ID AND GMC_GAMING_DAY = CONVERT(INT,CONVERT(VARCHAR(8), @CurrentDay, 112))
                  LEFT JOIN   GAMING_TABLES_SESSIONS GTS ON GMC_GAMINGTABLE_ID = GTS_GAMING_TABLE_ID  AND GMC_GAMING_DAY = CONVERT(INT,CONVERT(VARCHAR(8), @CurrentDay, 112))
                  INNER JOIN   CASHIER_SESSIONS CS ON CS.CS_SESSION_ID = GTS.GTS_CASHIER_SESSION_ID
                                   AND (
                                            (@CurrentDay >= CONVERT(DATETIME,CS.CS_OPENING_DATE) AND @CurrentDay <= CONVERT(DATETIME,CS.CS_CLOSING_DATE)) 
                                         OR (@CurrentDay >= CONVERT(DATETIME,CS.CS_OPENING_DATE) AND CONVERT(DATETIME,CS.CS_CLOSING_DATE) IS NULL)
                                         OR (CONVERT(DATETIME,CS.CS_OPENING_DATE) > @CurrentDay AND CONVERT(DATETIME,CS.CS_OPENING_DATE) <= DATEADD(D,1,@CurrentDay))
                                         OR (CONVERT(DATETIME,CS.CS_CLOSING_DATE) > @CurrentDay AND CONVERT(DATETIME,CS.CS_CLOSING_DATE) <= DATEADD(D,1,@CurrentDay))
                                         )
                    GROUP BY   GT_GAMING_TABLE_ID, GT_ENABLED  
                  )S
            WHERE   S.S_GAMING_TABLE_ID = GAMING_TABLES_CONNECTED.GMC_GAMINGTABLE_ID AND GMC_GAMING_DAY = S_GAMING_DAY

END

GO

GRANT EXECUTE ON [sp_GenerateTablesActivity] TO [WGGUI] WITH GRANT OPTION
GO


