/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 317;

SET @New_ReleaseId = 318;
SET @New_ScriptName = N'UpdateTo_18.318.039.sql';
SET @New_Description = N'';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/******* TABLES  *******/

IF NOT EXISTS(SELECT * FROM sys.columns WHERE Name = N'cuep_valid_gaming_days' AND Object_ID = Object_ID(N'customer_entrances_prices'))
	ALTER TABLE dbo.customer_entrances_prices ADD cuep_valid_gaming_days int NOT NULL DEFAULT 1
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[blacklist_file_imported_type]') AND type in (N'U'))
  DROP TABLE dbo.blacklist_file_imported_type
GO

CREATE TABLE [dbo].[blacklist_file_imported_type](
	[bklt_id_type] [int] IDENTITY(1,1) NOT NULL,
	[bklt_name] [nvarchar](50) NOT NULL,
	[bklt_message] [nvarchar](50) NOT NULL,
	[bklt_entrance_allowed] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[bklt_id_type] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

IF NOT EXISTS(SELECT * FROM sys.columns  WHERE Name = N'blkf_id_type' AND Object_ID = Object_ID(N'blacklist_file_imported'))
  ALTER TABLE [dbo].[blacklist_file_imported] ADD [blkf_id_type] INT DEFAULT -1 NOT NULL;
GO

IF NOT EXISTS(SELECT * FROM sys.columns  WHERE Name = N'cut_included_in_black_list' AND Object_ID = Object_ID(N'customer_entrances'))
  ALTER TABLE [dbo].[customer_entrances] ADD [cut_included_in_black_list] INT DEFAULT 0 NOT NULL;
GO

IF NOT EXISTS(SELECT * FROM sys.columns  WHERE Name = N'cut_op_acknowledged_black_list' AND Object_ID = Object_ID(N'customer_entrances'))
  ALTER TABLE [dbo].[customer_entrances] ADD [cut_op_acknowledged_black_list] BIT DEFAULT 0 NOT NULL;
GO

/******* INDEXES *******/

/******* RECORDS *******/

/* Reception Agreement.CashierTex */
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Reception' AND GP_SUBJECT_KEY ='Agreement.CashierTex')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key]
             ,[gp_subject_key]
             ,[gp_key_value])
       VALUES
             ('Reception'
             ,'Agreement.CashierTex'
             ,'Acepto las condiciones');
GO

/* Delete obsolete general param */
IF EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Reception' AND GP_SUBJECT_KEY ='Entrance.ExpirationDays')
  DELETE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Reception' AND GP_SUBJECT_KEY ='Entrance.ExpirationDays';
GO

/******* PROCEDURES *******/

