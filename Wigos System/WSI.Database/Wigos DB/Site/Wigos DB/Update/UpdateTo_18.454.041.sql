﻿/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 453;

SET @New_ReleaseId = 454;

SET @New_ScriptName = N'2018-07-03 - UpdateTo_18.454.041.sql';
SET @New_Description = N'New release v03.008.0025'; 

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO


/*********************************************************************************************************/

/**** GENERAL PARAM *****/


/**** VIEW *****/


/******* TABLES  *******/


/******* INDEXES *******/


/******* TRIGERS *******/


/******* RECORDS *******/

  IF (EXISTS (SELECT * 
                FROM INFORMATION_SCHEMA.TABLES 
                WHERE TABLE_NAME = 'gaming_tables'))
  BEGIN
    -- Update only RelativeLocationX
    UPDATE gaming_tables
        SET gt_design_xml.modify('replace value of (/TableDesign/GamingSeatProperties/RelativeLocationX/text())[1] with "-325"')
    WHERE [gt_design_xml].value('(/TableDesign/GamingSeatProperties/RelativeLocationX/node())[1]', 'nvarchar(max)') = '-321'
      and [gt_design_xml].value('(/TableDesign/GamingSeatProperties/RelativeLocationY/node())[1]', 'nvarchar(max)') = '-78'
      and [gt_design_xml].value('(/TableDesign/GamingSeatProperties/SeatPosition/node())[1]', 'nvarchar(max)') = '1' 
      and gt_num_seats = 1

    -- Update only RelativeLocationY
    UPDATE gaming_tables
        SET gt_design_xml.modify('replace value of (/TableDesign/GamingSeatProperties/RelativeLocationY/text())[1] with "-28"')
    WHERE [gt_design_xml].value('(/TableDesign/GamingSeatProperties/RelativeLocationY/node())[1]', 'nvarchar(max)') = '-78'
      and [gt_design_xml].value('(/TableDesign/GamingSeatProperties/SeatPosition/node())[1]', 'nvarchar(max)') = '1' 
      and [gt_design_xml].value('(/TableDesign/GamingSeatProperties/RelativeLocationX/node())[1]', 'nvarchar(max)') = '-325'
      and gt_num_seats = 1

  END

  GO

  
  UPDATE IDENTIFICATION_TYPES SET IDT_ENABLED = 0 WHERE IDT_ID = 146
IF NOT EXISTS (SELECT 1 FROM IDENTIFICATION_TYPES WHERE IDT_ID = 152)
BEGIN
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (152,1,12,'Overseas Workers Welfare Administration (OWWA)ID','PH')
END
IF NOT EXISTS (SELECT 1 FROM IDENTIFICATION_TYPES WHERE IDT_ID = 153)
BEGIN
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (153,1,13,'Overseas Filipino Worker (OFW) ID','PH')
END
IF NOT EXISTS (SELECT 1 FROM IDENTIFICATION_TYPES WHERE IDT_ID = 154)
BEGIN
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (154,1,14,'National Bureau Investigation (NBI) Clearance','PH')
END
IF NOT EXISTS (SELECT 1 FROM IDENTIFICATION_TYPES WHERE IDT_ID = 155)
BEGIN
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (155,1,15,'Police Clearance Certificate','PH')
END
IF NOT EXISTS (SELECT 1 FROM IDENTIFICATION_TYPES WHERE IDT_ID = 156)
BEGIN
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (156,1,16,'Philhealth ID','PH')
END
IF NOT EXISTS (SELECT 1 FROM IDENTIFICATION_TYPES WHERE IDT_ID = 157)
BEGIN
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (157,1,17,'Seaman’s Book','PH')
END
IF NOT EXISTS (SELECT 1 FROM IDENTIFICATION_TYPES WHERE IDT_ID = 158)
BEGIN
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (158,1,18,'Firearms License issued by the Philip. Nat. Police','PH')
END
IF NOT EXISTS (SELECT 1 FROM IDENTIFICATION_TYPES WHERE IDT_ID = 159)
BEGIN
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (159,1,19,'Integrated Bar of the Philippines ID','PH')
END
 

/******* PROCEDURES *******/


/******* TRIGGERS *******/

