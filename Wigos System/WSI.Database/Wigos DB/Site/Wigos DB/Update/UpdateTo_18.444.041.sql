/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 443;

SET @New_ReleaseId = 444;

SET @New_ScriptName = N'2018-06-12 - UpdateTo_18.444.041.sql';
SET @New_Description = N'New release v03.008.0008'; 

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO


/*********************************************************************************************************/

/**** GENERAL PARAM *****/

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'WigosGUI' AND GP_SUBJECT_KEY = 'CageMovements.MonitorRefreshTime')
  INSERT INTO [dbo].[general_params] ([gp_group_key] ,[gp_subject_key],[gp_key_value]) VALUES ('WigosGUI', 'CageMovements.MonitorRefreshTime', '5');
GO


IF NOT EXISTS	(	SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'WCP' AND	GP_SUBJECT_KEY = 'SasMetersInterval')
BEGIN
	INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES	('WCP','SasMetersInterval','60')
END
ELSE
BEGIN
	UPDATE GENERAL_PARAMS SET GP_KEY_VALUE = '60' WHERE GP_GROUP_KEY = 'WCP' AND	GP_SUBJECT_KEY = 'SasMetersInterval'
END

GO

/**** VIEW *****/


/******* TABLES  *******/


/******* RECORDS *******/


DECLARE @_sheets AS XML
SET @_sheets = '
<ArrayOfReportToolDesignSheetsDTO>
  <ReportToolDesignSheetsDTO>
    <LanguageResources>
      <NLS09>
        <Label>Cage movements for currency</Label>
      </NLS09>
      <NLS10>
        <Label>Movimientos por divisa</Label>
      </NLS10>
    </LanguageResources>
    <Columns>
      <ReportToolDesignColumn>
        <Code>Fecha</Code>
        <Width>300</Width>
        <EquityMatchType>Equality</EquityMatchType>
        <LanguageResources>
          <NLS09>
            <Label>Date</Label>
          </NLS09>
          <NLS10>
            <Label>Fecha</Label>
          </NLS10>
        </LanguageResources>
      </ReportToolDesignColumn>
      <ReportToolDesignColumn>
        <Code>Usuario de B�veda</Code>
        <Width>300</Width>
        <EquityMatchType>Equality</EquityMatchType>
        <LanguageResources>
          <NLS09>
            <Label>Cash cage user</Label>
          </NLS09>
          <NLS10>
            <Label>Usuario de B�veda</Label>
          </NLS10>
        </LanguageResources>
      </ReportToolDesignColumn>
      <ReportToolDesignColumn>
        <Code>Origen/Destino</Code>
        <Width>300</Width>
        <EquityMatchType>Equality</EquityMatchType>
        <LanguageResources>
          <NLS09>
            <Label>Source/Destination</Label>
          </NLS09>
          <NLS10>
            <Label>Origen/Destino</Label>
          </NLS10>
        </LanguageResources>
      </ReportToolDesignColumn>
      <ReportToolDesignColumn>
        <Code>Tipo de movimiento</Code>
        <Width>300</Width>
        <EquityMatchType>Equality</EquityMatchType>
        <LanguageResources>
          <NLS09>
            <Label>Type</Label>
          </NLS09>
          <NLS10>
            <Label>Tipo de movimiento</Label>
          </NLS10>
        </LanguageResources>
      </ReportToolDesignColumn>
      <ReportToolDesignColumn>
        <Code>Estado</Code>
        <Width>300</Width>
        <EquityMatchType>Equality</EquityMatchType>
        <LanguageResources>
          <NLS09>
            <Label>Status</Label>
          </NLS09>
          <NLS10>
            <Label>Estado</Label>
          </NLS10>
        </LanguageResources>
      </ReportToolDesignColumn>  
	  <ReportToolDesignColumn>
        <Code>_0</Code>
        <Width>300</Width>
        <EquityMatchType>Contains</EquityMatchType>
        <LanguageResources>
          <NLS09>
            <Label></Label>
          </NLS09>
          <NLS10>
            <Label></Label>
          </NLS10>
        </LanguageResources>
      </ReportToolDesignColumn>
	  <ReportToolDesignColumn>
        <Code>_1001</Code>
        <Width>300</Width>
        <EquityMatchType>Contains</EquityMatchType>
        <LanguageResources>
          <NLS09>
            <Label>-Chips RE</Label>
          </NLS09>
          <NLS10>
            <Label>-Fichas RE</Label>
          </NLS10>
        </LanguageResources>
      </ReportToolDesignColumn>
	  <ReportToolDesignColumn>
        <Code>_1002</Code>
        <Width>300</Width>
        <EquityMatchType>Contains</EquityMatchType>
        <LanguageResources>
          <NLS09>
            <Label>-Chips NR</Label>
          </NLS09>
          <NLS10>
            <Label>-Fichas NR</Label>
          </NLS10>
        </LanguageResources>
      </ReportToolDesignColumn>
	  <ReportToolDesignColumn>
        <Code>X02_1003</Code>
        <Width>300</Width>
        <EquityMatchType>Equality</EquityMatchType>
        <LanguageResources>
          <NLS09>
            <Label>Color chips</Label>
          </NLS09>
          <NLS10>
            <Label>Fichas de color</Label>
          </NLS10>
        </LanguageResources>
      </ReportToolDesignColumn>
	  <ReportToolDesignColumn>
        <Code>_2001</Code>
        <Width>300</Width>
        <EquityMatchType>Contains</EquityMatchType>
        <LanguageResources>
          <NLS09>
            <Label>-Tickets TITO</Label>
          </NLS09>
          <NLS10>
            <Label>-Tickets TITO</Label>
          </NLS10>
        </LanguageResources>
      </ReportToolDesignColumn>                    
    </Columns>
  </ReportToolDesignSheetsDTO>
</ArrayOfReportToolDesignSheetsDTO>
'

IF EXISTS (SELECT * FROM report_tool_config WHERE rtc_store_name = 'GetCageMovementType_GR')
	BEGIN
		UPDATE report_tool_config
		 SET rtc_design_sheets = @_sheets 
		WHERE rtc_store_name = 'GetCageMovementType_GR'
	END
ELSE
	BEGIN
	  DECLARE @_name AS XML
  
	  SET @_name = '
		<LanguageResources>
		  <NLS09>
			<Label>Cage movements for currency</Label>
		  </NLS09>
		  <NLS10>
			<Label>Movimientos de b�veda por divisa</Label>
		  </NLS10>
		</LanguageResources>
	  '
	   DECLARE @_filter AS XML

	   SET @_filter = '
		<ReportToolDesignFilterDTO>
			<FilterType>FromToDate</FilterType>
		</ReportToolDesignFilterDTO>
	   '

	  INSERT INTO report_tool_config
		(rtc_form_id
		,rtc_location_menu
		,rtc_report_name
		,rtc_store_name
		,rtc_design_filter
		,rtc_design_sheets
		,rtc_mailing
		,rtc_status
		,rtc_mode_type
		,rtc_html_header
		,rtc_html_footer)
	  VALUES (11000, 7, @_name, 'GetCageMovementType_GR', @_filter, @_sheets , 1, 0, 1, NULL, NULL)
	END
GO


/******* PROCEDURES *******/


IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID('GetCageMovementType') AND type in ('P', 'PC'))
  DROP PROCEDURE GetCageMovementType
GO

CREATE PROCEDURE GetCageMovementType
 @pStarDateTime DATETIME,
 @pEndDateTime  DATETIME
AS
BEGIN

	DECLARE @_isoCodeRegional AS VARCHAR(5)

	CREATE TABLE #isoCodeTypes
	( 
		movement_id BIGINT,
		iso_code VARCHAR(3),  
		currency_type INTEGER,
		amount MONEY,
		position INTEGER
	) 

	CREATE INDEX idx_isoCode
	ON #isoCodeTypes (movement_id);

	SET @_isoCodeRegional = (SELECT GP.GP_KEY_VALUE FROM GENERAL_PARAMS AS GP WHERE GP.GP_GROUP_KEY = 'RegionalOptions' AND GP.GP_SUBJECT_KEY = 'CurrencyISOCode')     
	INSERT INTO #isoCodeTypes
	SELECT CMD_MOVEMENT_ID, ISNULL(CASE WHEN CMD_ISO_CODE = 'X01' THEN @_isoCodeRegional ELSE CMD_ISO_CODE END,@_isoCodeRegional) AS CMD_ISO_CODE
		  ,CASE WHEN CMD_CAGE_CURRENCY_TYPE > 1000 THEN CMD_CAGE_CURRENCY_TYPE 
		   ELSE CASE WHEN CMD_QUANTITY IN(-200) AND CMD_CAGE_CURRENCY_TYPE = 99 THEN 2001
		   ELSE 0 END END AS CMD_CAGE_CURRENCY_TYPE
		  ,SUM(CASE WHEN CMD_CHIP_ID IS NULL THEN 0 
					ELSE CASE WHEN CMD_QUANTITY IN(-1,-2,-7,-100,-200) AND CMD_CAGE_CURRENCY_TYPE = 99 THEN CMD_DENOMINATION 
					ELSE CASE WHEN CMD_ISO_CODE IN ('X02') AND CGM_TYPE IN(0, 1, 4, 5) THEN CMD_QUANTITY * -1
					ELSE CASE WHEN CMD_ISO_CODE IN ('X02') AND CGM_TYPE NOT IN(0, 1, 4, 5) THEN CMD_QUANTITY 
					ELSE CASE WHEN CGM_TYPE IN(0, 1, 4, 5) THEN CMD_QUANTITY * CMD_DENOMINATION * -1
					ELSE CMD_QUANTITY * CMD_DENOMINATION END END END END END) AS CMD_AMOUNT
		  ,CASE WHEN CMD_QUANTITY IN(-200) AND CMD_CAGE_CURRENCY_TYPE = 99 THEN 2001
				ELSE CASE WHEN CE_CURRENCY_ORDER IS NULL THEN 999 
				ELSE CE_CURRENCY_ORDER END END AS CMD_POSITION  
	FROM CAGE_MOVEMENTS AS A
	LEFT JOIN CAGE_MOVEMENT_DETAILS AS B ON CGM_MOVEMENT_ID = CMD_MOVEMENT_ID 
	LEFT JOIN CURRENCY_EXCHANGE ON CE_CURRENCY_ISO_CODE =  CMD_ISO_CODE AND CE_TYPE = 0  
	WHERE  CGM_MOVEMENT_DATETIME >= @pStarDateTime
			AND CGM_MOVEMENT_DATETIME < @pEndDateTime
			AND CGM_TYPE NOT IN(200) AND CGM_STATUS NOT IN(0, 1) AND CMD_MOVEMENT_ID IS NOT NULL
	GROUP BY CMD_MOVEMENT_ID
			,ISNULL(CASE WHEN CMD_ISO_CODE = 'X01' THEN @_isoCodeRegional ELSE CMD_ISO_CODE END,@_isoCodeRegional)		
			,CASE WHEN CMD_CAGE_CURRENCY_TYPE > 1000 THEN CMD_CAGE_CURRENCY_TYPE 
			 ELSE CASE WHEN CMD_QUANTITY IN(-200) AND CMD_CAGE_CURRENCY_TYPE = 99 THEN 2001
			 ELSE 0 END END
			,CASE WHEN CMD_QUANTITY IN(-200) AND CMD_CAGE_CURRENCY_TYPE = 99 THEN 2001
			 ELSE CASE WHEN CE_CURRENCY_ORDER IS NULL THEN 999 
			 ELSE CE_CURRENCY_ORDER END END 
		
	ORDER BY CMD_MOVEMENT_ID, MAX(ISNULL(CE_CURRENCY_ORDER,999))
			,CASE WHEN CMD_CAGE_CURRENCY_TYPE > 1000 THEN CMD_CAGE_CURRENCY_TYPE 
			 ELSE CASE WHEN CMD_QUANTITY IN(-200) AND CMD_CAGE_CURRENCY_TYPE = 99 THEN 2001
			 ELSE 0 END END

	CREATE TABLE #cageData (        
			 movement_id BIGINT,
			 date_insert DATETIME, 
			 movement_type INTEGER,
			 movement_type_name NVARCHAR(MAX),       
			 status_name NVARCHAR(MAX),
			 t_user_name NVARCHAR(MAX),
			 c_user_name NVARCHAR(MAX),
			 machine_name NVARCHAR(MAX),  
			 collection_id BIGINT,
			 terminal_id INTEGER
	)   

	INSERT INTO #cageData
	SELECT
		 CM.CGM_MOVEMENT_ID   
		,CM.CGM_MOVEMENT_DATETIME
		,CM.CGM_TYPE
		,CASE 
			WHEN CM.CGM_TYPE = 0   THEN 'Env�o a Cajero'                             WHEN CM.CGM_TYPE = 1   THEN 'Env�o a destino personalizado'
			WHEN CM.CGM_TYPE = 2   THEN 'Cierre de b�veda'                           WHEN CM.CGM_TYPE = 3   THEN 'Arqueo de b�veda'
			WHEN CM.CGM_TYPE = 4   THEN 'Env�o a mesa de juego'                      WHEN CM.CGM_TYPE = 5   THEN 'Env�o a terminal'
			WHEN CM.CGM_TYPE = 99  THEN 'P�rdida de cuadratura'                      WHEN CM.CGM_TYPE = 100 THEN 'Recaudaci�n de Cajero' 
			WHEN CM.CGM_TYPE = 101 THEN 'Recaudaci�n de origen personalizado'        WHEN CM.CGM_TYPE = 102 THEN 'Apertura de b�veda'
			WHEN CM.CGM_TYPE = 103 THEN 'Recaudaci�n de mesa de juego'               WHEN CM.CGM_TYPE = 104 THEN 'Recaudaci�n de terminal'
			WHEN CM.CGM_TYPE = 105 THEN 'Reapertura de b�veda'                       WHEN CM.CGM_TYPE = 106 THEN 'Recaudaci�n de drop box de mesa de juego'
			WHEN CM.CGM_TYPE = 107 THEN 'Recaudaci�n de drop box de mesa de juego'   WHEN CM.CGM_TYPE = 108 THEN 'Recaudaci�n de cierre de Cajero'
			WHEN CM.CGM_TYPE = 109 THEN 'Recaudaci�n de cierre de Mesa de Juego'	 WHEN CM.CGM_TYPE = 199 THEN 'Ganancia de cuadratura'
			WHEN CM.CGM_TYPE = 200 THEN 'Solicitud desde Cajero'                     WHEN CM.CGM_TYPE = 300 THEN 'Provisi�n de progresivo'
			WHEN CM.CGM_TYPE = 301 THEN 'Progresivo otorgado'                        WHEN CM.CGM_TYPE = 302 THEN '(Anulaci�n)Provisi�n de progresivo'
			WHEN CM.CGM_TYPE = 303 THEN '(Anulaci�n)Progresivo otorgado'             ELSE CAST(CM.CGM_TYPE AS NVARCHAR(MAX))
			END AS CGM_TYPE_NAME
		,CASE
			WHEN CM.CGM_STATUS = 0 AND CM.CGM_TYPE IN (0, 4)                         THEN 'Enviado'
			WHEN CM.CGM_STATUS = 0 AND CM.CGM_TYPE IN (200)                          THEN 'Solicitud pendiente'
			WHEN CM.CGM_STATUS = 0 AND CM.CGM_TYPE IN (5, 100, 103, 104)             THEN 'Pendiente recaudar'            
			WHEN CM.CGM_STATUS = 2 AND CM.CGM_TYPE IN (0, 4, 5, 300, 301, 302, 303)  THEN 'Finalizado'
			WHEN CM.CGM_STATUS = 2 AND CM.CGM_TYPE IN (100, 103, 104)                THEN 'Recaudado'
			WHEN CM.CGM_STATUS = 2 AND CM.CGM_TYPE IN (200)                          THEN 'Solicitud tramitada'
			WHEN CM.CGM_STATUS = 1 AND CM.CGM_TYPE IN (200)                          THEN 'Solicitud cancelada'
			WHEN CM.CGM_STATUS = 1 AND CM.CGM_TYPE NOT IN (200)                      THEN 'Cancelado'   
			WHEN CM.CGM_STATUS = 3 AND CM.CGM_TYPE IN (0)                            THEN 'Finalizado con descuadre denominaciones'
			WHEN CM.CGM_STATUS = 3 AND CM.CGM_TYPE NOT IN (0)                        THEN 'Recaudado con descuadre denominaciones'            
			WHEN CM.CGM_STATUS = 4 AND CM.CGM_TYPE IN (0)                            THEN 'Finalizado con descuadre del monto'
			WHEN CM.CGM_STATUS = 4 AND CM.CGM_TYPE NOT IN (0)                        THEN 'Recaudado con descuadre total'        
			WHEN CM.CGM_STATUS IN (9, 11, 12)                                        THEN 'Finalizado'
			WHEN CM.CGM_STATUS = 10                                                  THEN 'Recaudado'  
			WHEN CM.CGM_STATUS = 2 AND CM.CGM_TYPE IN (108)                          THEN 'Recaudado'            
			WHEN CM.CGM_STATUS = 13                                                  THEN '(Cancelado) Pendiente recaudar'
			ELSE '---'
			END AS CGM_STATUS_NAME  
		  ,ISNULL(GU.GU_FULL_NAME, '') AS GU_FULL_NAME
		  ,ISNULL(GC.GU_FULL_NAME,'') AS GU_CAGE_FULL_NAME
		  ,ISNULL(ISNULL(CA.CT_NAME, CT.CT_NAME),TE.TE_NAME) AS CGM_NAME   
		  ,CM.CGM_MC_COLLECTION_ID
		  ,CM.CGM_TERMINAL_CASHIER_ID	 
	 FROM CAGE_MOVEMENTS AS CM
	 LEFT JOIN GUI_USERS AS GU ON GU.GU_USER_ID = CM.CGM_USER_CASHIER_ID  
	 LEFT JOIN GUI_USERS AS GC ON GC.GU_USER_ID = CM.CGM_USER_CAGE_ID 
	 LEFT JOIN CASHIER_TERMINALS AS CA ON CA.CT_CASHIER_ID = CM.CGM_TERMINAL_CASHIER_ID
	 LEFT JOIN CASHIER_SESSIONS AS CS ON CS.CS_SESSION_ID = CM.CGM_CASHIER_SESSION_ID  
	 LEFT JOIN CASHIER_TERMINALS AS CT ON CT.CT_CASHIER_ID = CS.CS_CASHIER_ID
	 LEFT JOIN TERMINALS AS TE ON TE.TE_TERMINAL_ID = CM.CGM_TERMINAL_CASHIER_ID
	 WHERE CM.CGM_TYPE NOT IN(200) AND CM.CGM_STATUS NOT IN(0, 1) AND CM.CGM_MOVEMENT_DATETIME >= @pStarDateTime
			AND CM.CGM_MOVEMENT_DATETIME < @pEndDateTime  
			ORDER BY CM.CGM_MOVEMENT_ID

	DECLARE @_isoCode VARCHAR(3)                                                                                                                       
	DECLARE @_currencyType INTEGER  
	DECLARE @_query AS NVARCHAR(MAX)
	DECLARE @_update AS NVARCHAR(MAX)
	DECLARE @_select AS NVARCHAR(MAX)

	SET @_select = ''
    
	DECLARE currency_cursor CURSOR FOR 
	SELECT cc.iso_code, cc.currency_type FROM(SELECT iso_code, currency_type , MIN(position) position FROM #isoCodeTypes
	GROUP BY iso_code, currency_type) cc
	ORDER BY cc.position, cc.currency_type

	OPEN currency_cursor
	FETCH NEXT FROM currency_cursor INTO @_isoCode, @_currencyType                                                                 
	WHILE @@FETCH_STATUS = 0 
	BEGIN
  
	  IF @_select = ''
		 SET @_select = 'ISNULL(' + @_isoCode + '_' + CAST(@_currencyType AS VARCHAR(MAX)) + ', 0) ' + @_isoCode + '_' + CAST(@_currencyType AS VARCHAR(MAX))
	  ELSE
		 SET @_select = @_select + ',' + 'ISNULL(' + @_isoCode + '_' + CAST(@_currencyType AS VARCHAR(MAX)) + ', 0) ' + @_isoCode + '_' + CAST(@_currencyType AS VARCHAR(MAX))
 
	  SET @_query = 'ALTER TABLE #cageData ADD [' + @_isoCode + '_' + CAST(@_currencyType AS VARCHAR(MAX)) + '] MONEY '
	  EXEC SP_EXECUTESQL @_query

	  FETCH NEXT FROM currency_cursor INTO @_isoCode, @_currencyType 
	END
	CLOSE currency_cursor                                                                                                                          
	DEALLOCATE currency_cursor  

	DECLARE @_movementID AS BIGINT
	DECLARE @_amount AS MONEY

	DECLARE cage_cursor CURSOR FOR
	SELECT movement_id FROM #cageData 

	OPEN cage_cursor
	FETCH NEXT FROM cage_cursor INTO @_movementID                                                                 
	WHILE @@FETCH_STATUS = 0 
	BEGIN
  
	  SET @_update = ''
	  DECLARE update_cursor CURSOR FOR
	  SELECT iso_code, currency_type, amount FROM #isoCodeTypes WHERE movement_id = @_movementID
	  OPEN update_cursor
	  FETCH NEXT FROM update_cursor INTO @_isoCode, @_currencyType, @_amount               
	  WHILE @@FETCH_STATUS = 0
	  BEGIN    
		IF @_update = ''
		  SET @_update = 'UPDATE #cageData SET ' + @_isoCode + '_' + CAST(@_currencyType AS VARCHAR(MAX)) + ' = ' + CAST(@_amount AS VARCHAR(MAX))
		ELSE
		  SET @_update =  @_update + ', ' + @_isoCode + '_' + CAST(@_currencyType AS VARCHAR(MAX)) + ' = ' + CAST(@_amount AS VARCHAR(MAX))

		FETCH NEXT FROM update_cursor INTO @_isoCode, @_currencyType, @_amount  
	  END
	  IF @_update <> ''
	  BEGIN
		SET @_update =  @_update + ' WHERE movement_id = ' + CAST(@_movementID AS VARCHAR(MAX))  
		EXEC SP_EXECUTESQL @_update
	  END

	  CLOSE update_cursor		                                                                                                     
	  DEALLOCATE update_cursor 
	  FETCH NEXT FROM cage_cursor INTO @_movementID 

	END
	CLOSE cage_cursor                                                                                                                          
	DEALLOCATE cage_cursor 

	CREATE TABLE #isoCodeMachineGame
	( 
		movement_id BIGINT,
		iso_code VARCHAR(3),    
		money_amount MONEY,
		tickets_count INTEGER,
		position INTEGER
	) 

	CREATE INDEX idx_isoCodeMachineGame
	ON #isoCodeMachineGame (movement_id);

	INSERT INTO #isoCodeMachineGame
	SELECT  A.movement_id, ISNULL(C.te_iso_code, @_isoCodeRegional)   
		   ,CASE WHEN A.movement_type = 104 THEN ISNULL(B.mc_collected_bill_amount,0) + ISNULL( B.mc_collected_coin_amount, 0) 
			ELSE ISNULL(B.mc_expected_bill_amount, 0) + ISNULL( B.mc_expected_coin_amount, 0)  END
		   ,CASE WHEN A.movement_type = 104 THEN ISNULL(B.mc_collected_ticket_count,0) 
			ELSE ISNULL(B.mc_expected_ticket_count,0) END
		   ,ISNULL(D.ce_currency_order,1000) 
	FROM #cageData A
	INNER JOIN MONEY_COLLECTIONS B ON A.collection_id = B.mc_collection_id AND A.movement_type IN(5,104)
	LEFT JOIN TERMINALS C ON A.terminal_id = C.TE_TERMINAL_ID 
	LEFT JOIN CURRENCY_EXCHANGE D ON D.ce_currency_iso_code =  ISNULL(C.te_iso_code, @_isoCodeRegional)  AND D.ce_type = 0  

	DECLARE @_countISO AS INTEGER
	DECLARE machineGame_cursor CURSOR FOR
	SELECT A.iso_code  FROM(SELECT iso_code, MIN(position) AS position FROM #isoCodeMachineGame WHERE money_amount > 0
	GROUP BY iso_code) A
	ORDER BY A.position

	OPEN machineGame_cursor
	FETCH NEXT FROM machineGame_cursor INTO @_isoCode
	WHILE @@FETCH_STATUS = 0 
	BEGIN
  
	  SET @_countISO = (SELECT COUNT(*) FROM #isoCodeTypes WHERE iso_code = @_isoCode AND currency_type = 0 )

	  IF @_countISO = 0
	  BEGIN
		  IF @_select = ''
		   SET @_select = 'ISNULL(' + @_isoCode + '_0, 0) ' + @_isoCode + '_0'
		 ELSE
		   SET @_select = @_select + ',' + 'ISNULL(' + @_isoCode + '_0, 0) ' + @_isoCode + '_0'

		 SET @_query = 'ALTER TABLE #cageData ADD [' + @_isoCode + '_0] MONEY '
		 EXEC SP_EXECUTESQL @_query
	  END

	  FETCH NEXT FROM machineGame_cursor INTO @_isoCode
	END
	CLOSE machineGame_cursor                                                                                                                          
	DEALLOCATE machineGame_cursor 


	DECLARE machineTicket_cursor CURSOR FOR
	SELECT A.iso_code  FROM(SELECT iso_code, MIN(position) AS position FROM #isoCodeMachineGame WHERE tickets_count > 0
	GROUP BY iso_code) A
	ORDER BY A.position

	OPEN machineTicket_cursor
	FETCH NEXT FROM machineTicket_cursor INTO @_isoCode
	WHILE @@FETCH_STATUS = 0 
	BEGIN
  
	  SET @_countISO = (SELECT COUNT(*) FROM #isoCodeTypes WHERE iso_code = @_isoCode AND currency_type = 2001 )

	  IF @_countISO = 0
	  BEGIN
		  IF @_select = ''
		   SET @_select = 'ISNULL(' + @_isoCode + '_2001, 0) ' + @_isoCode + '_2001'
		 ELSE
		   SET @_select = @_select + ',' + 'ISNULL(' + @_isoCode + '_2001, 0) ' + @_isoCode + '_2001'

		 SET @_query = 'ALTER TABLE #cageData ADD [' + @_isoCode + '_2001] MONEY '
		 EXEC SP_EXECUTESQL @_query
	  END

	  FETCH NEXT FROM machineTicket_cursor INTO @_isoCode
	END
	CLOSE machineTicket_cursor                                                                                                                          
	DEALLOCATE machineTicket_cursor 

	DECLARE @_countTicket AS MONEY

	DECLARE update_cursor CURSOR FOR
	SELECT movement_id, iso_code, money_amount, tickets_count  AS position FROM #isoCodeMachineGame WHERE tickets_count > 0 OR money_amount > 0

	OPEN update_cursor
	FETCH NEXT FROM update_cursor INTO @_movementID, @_isoCode, @_amount, @_countTicket
	WHILE @@FETCH_STATUS = 0 
	BEGIN
  
	  SET @_update = ''

	  IF @_amount > 0
	  BEGIN
		 SET @_update =' UPDATE #cageData SET ' + @_isoCode + '_0 = ' + CAST(@_amount AS VARCHAR(MAX)) + ' WHERE movement_id = ' + CAST(@_movementID AS VARCHAR(MAX))
	  END
  
	  IF @_countTicket > 0
	  BEGIN
		SET @_update = @_update + ' UPDATE #cageData SET ' + @_isoCode + '_2001 = ' + CAST(@_countTicket AS VARCHAR(MAX)) + ' WHERE movement_id = ' + CAST(@_movementID AS VARCHAR(MAX))
	  END

	  IF @_update <> ''
	  BEGIN
		EXEC SP_EXECUTESQL @_update
	  END

	  FETCH NEXT FROM update_cursor INTO @_movementID, @_isoCode, @_amount, @_countTicket
	END
	CLOSE update_cursor                                                                                                                          
	DEALLOCATE update_cursor 

	SET @_update = '
		  SELECT 
			 CONVERT(char(11), date_insert, 103) + CONVERT(char(8), date_insert, 108) AS ''Fecha''
			,c_user_name AS ''Usuario de B�veda''  
		    ,CASE WHEN t_user_name LIKE ''SYS-%'' THEN '''' ELSE ISNULL(t_user_name,'''') END  AS ''Origen/Destino''
			,ISNULL(machine_name,'''') AS ''Terminal'' 
			,movement_type_name AS ''Tipo de movimiento'' 
			,status_name ''Estado'''
	
	IF @_select <> ''
	BEGIN
	  SET @_update = @_update + ',' + @_select
	END 
	
	SET @_update = @_update + ' FROM #cageData '		
								  
	EXEC SP_EXECUTESQL @_update

	DROP TABLE #cageData
	DROP TABLE #isoCodeMachineGame
	DROP TABLE #isoCodeTypes
END
GO

GRANT EXECUTE ON GetCageMovementType TO wggui WITH GRANT OPTION 
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID('GetCageMovementType_GR') AND type in ('P', 'PC'))
  DROP PROCEDURE GetCageMovementType_GR
GO

CREATE PROCEDURE GetCageMovementType_GR
 @pFrom DATETIME,
 @pTo  DATETIME
AS
BEGIN
	EXEC GetCageMovementType @pFrom, @pTo
END
GO

GRANT EXECUTE ON GetCageMovementType_GR TO wggui WITH GRANT OPTION 
GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CageGetGlobalReportData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CageGetGlobalReportData]
GO

CREATE PROCEDURE [dbo].[CageGetGlobalReportData]
        @pCageSessionIds NVARCHAR(MAX) = NULL
AS
BEGIN  
  
  DECLARE @NationalIsoCode NVARCHAR(3)
  DECLARE @CageOperationType_FromTerminal INT
  DECLARE @TicketsId INT
  DECLARE @BillsId INT
  DECLARE @Currencies AS NVARCHAR(200)
  DECLARE @CountSessions INT
  DECLARE @List VARCHAR(MAX)
  DECLARE @StockIsStored BIT
  DECLARE @CageCurrencyType_ColorChips INT

  SELECT @NationalIsoCode = GP_KEY_VALUE 
    FROM GENERAL_PARAMS 
   WHERE GP_GROUP_KEY = 'RegionalOptions' 
     AND GP_SUBJECT_KEY = 'CurrencyISOCode'

  SELECT @Currencies = GP_KEY_VALUE 
    FROM GENERAL_PARAMS 
   WHERE GP_GROUP_KEY = 'RegionalOptions' 
     AND GP_SUBJECT_KEY = 'CurrenciesAccepted'

  -- Split currencies ISO codes
  SELECT   CURRENCY_ISO_CODE INTO #TMP_CURRENCIES_ISO_CODES 
    FROM ( SELECT   SST_VALUE AS CURRENCY_ISO_CODE FROM [SplitStringIntoTable] (@Currencies, ';', 1)
            UNION ALL 
           SELECT   'X01' AS CURRENCY_ISO_CODE    -- for retrocompatibility
            UNION ALL 
           SELECT   'X02' AS CURRENCY_ISO_CODE ) AS CURRENCIES
               
  SET @CageOperationType_FromTerminal = 104
  SET @TicketsId = -200
  SET @BillsId = 0
  SET @CageCurrencyType_ColorChips = 1003
   
  -- Session IDs Split
  SELECT SST_VALUE AS CAGE_SESSION_ID INTO #TMP_CAGE_SESSIONS FROM [SplitStringIntoTable] (@pCageSessionIds,','  , 1)
  SET @CountSessions = (SELECT COUNT(*) FROM #TMP_CAGE_SESSIONS )
  
  IF @CountSessions = 1 
    SELECT @List = CGS_CAGE_STOCK FROM CAGE_SESSIONS 
     WHERE CGS_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)

  -- TABLE[0]: Get cage stock 
  IF (@CountSessions = 1 AND @List IS NOT NULL) 
  BEGIN
    --Get stock on close session
    SET @StockIsStored =  1
    SELECT SST_VALUE AS STOCK_ROW INTO #TMP_STOCK_ROWS FROM [SplitStringIntoTable] (@List, '|', 1)
    
    -- LTC 2017-JUN-20
    UPDATE #TMP_STOCK_ROWS 
    SET STOCK_ROW = CASE WHEN (LEN(STOCK_ROW) - LEN(REPLACE(STOCK_ROW, ';',''))) = 3 THEN STOCK_ROW + ';' + '-1' ELSE STOCK_ROW END
    FROM #TMP_STOCK_ROWS

    SELECT left(STOCK_ROW, 3) AS CGS_ISO_CODE,                        
       CAST(REPLACE(PARSENAME(REPLACE(REPLACE(RIGHT(STOCK_ROW, LEN(STOCK_ROW)-4),'.', '|'), ';', '.'), 4), '|', '.') AS MONEY) AS CGS_DENOMINATION,   
       CAST(REPLACE(PARSENAME(REPLACE(REPLACE(RIGHT(STOCK_ROW, LEN(STOCK_ROW)-4), '.', '|'), ';', '.'), 3), '|', '.') AS DECIMAL(15,2)) AS CGS_QUANTITY,       
       CAST(REPLACE(PARSENAME(REPLACE(REPLACE(RIGHT(STOCK_ROW, LEN(STOCK_ROW)-4), '.', '|'), ';', '.'), 2), '|', '.') AS INT) AS CGS_CAGE_CURRENCY_TYPE,  
       CAST(REPLACE(PARSENAME(REPLACE(REPLACE(RIGHT(STOCK_ROW, LEN(STOCK_ROW)-4), '.', '|'), ';', '.'), 1), '|', '.') AS INT) AS CGS_CHIP_ID, 
       CH_NAME AS NAME, CH_DRAWING AS DRAWING
       INTO #TMP_STOCK                                                                                                               
    FROM #TMP_STOCK_ROWS 
    LEFT JOIN chips ON ch_chip_id = CAST(REPLACE(PARSENAME(REPLACE(REPLACE(RIGHT(STOCK_ROW, LEN(STOCK_ROW)-4), '.', '|'), ';', '.'), 1), '|', '.') AS INT)

    SELECT   A.*
           , CE_CURRENCY_ORDER 
      FROM   #TMP_STOCK AS A
      LEFT   JOIN CURRENCY_EXCHANGE ON CGS_ISO_CODE = CE_CURRENCY_ISO_CODE AND CE_TYPE = 0
  ORDER BY   CE_CURRENCY_ORDER
           , CGS_ISO_CODE
           , CASE WHEN CGS_CAGE_CURRENCY_TYPE < 1000 THEN 0
                  WHEN CGS_CAGE_CURRENCY_TYPE = 1003 THEN 9999999
                  ELSE 1 END
           , CGS_DENOMINATION DESC
           , CASE WHEN CGS_DENOMINATION >= 0 THEN CGS_DENOMINATION 
                  ELSE CGS_DENOMINATION * (-100000) END                                                                    

    DROP TABLE #TMP_STOCK_ROWS                                                                                                      
    DROP TABLE #TMP_STOCK     
    
  END --IF (@CountSessions = 1 AND @List IS NOT NULL)    
  ELSE --IF (@CountSessions = 1 AND @List IS NOT NULL) 
  BEGIN
    -- Get current stock
    SET @StockIsStored =  0
    EXEC CageCurrentStock
  END --ELSE
  
  IF @pCageSessionIds IS NOT NULL
  BEGIN
      
    -- TABLE[1]: Get cage sessions information
    SELECT  a.*
          , (ROW_NUMBER() OVER(ORDER BY CM_SOURCE_TARGET_ID
          , CASE CM_CONCEPT_ID WHEN 0 THEN '000' ELSE CC_DESCRIPTION END
          , CASE WHEN CAGE_CURRENCY_TYPE < 1000 THEN 0
                 WHEN CAGE_CURRENCY_TYPE = 1003 THEN 9999999
                 ELSE 1 END
          , CE_CURRENCY_ORDER
          , CM_ISO_CODE
          , CAGE_CURRENCY_TYPE)) as ORDEN
    FROM (
    SELECT CSM.CSM_SOURCE_TARGET_ID AS CM_SOURCE_TARGET_ID
         , CSM.CSM_CONCEPT_ID AS CM_CONCEPT_ID
         , CASE CSM.CSM_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CSM.CSM_ISO_CODE END AS CM_ISO_CODE
         , CASE CSM.CSM_ISO_CODE WHEN 'X01' THEN 1001 ELSE CASE WHEN CSM_CAGE_CURRENCY_TYPE < 1000 THEN 0 ELSE CSM_CAGE_CURRENCY_TYPE END END AS CAGE_CURRENCY_TYPE
         , SUM(CSM.CSM_VALUE_IN) AS CM_VALUE_IN
         , SUM(CSM.CSM_VALUE_OUT) AS CM_VALUE_OUT
         , CST.CST_SOURCE_TARGET_NAME
         , CC.CC_DESCRIPTION
      FROM CAGE_SESSION_METERS AS CSM
           INNER JOIN CAGE_SOURCE_TARGET_CONCEPTS AS CSTC ON CSTC.CSTC_CONCEPT_ID = CSM.CSM_CONCEPT_ID AND CSTC.CSTC_SOURCE_TARGET_ID = CSM.CSM_SOURCE_TARGET_ID
           INNER JOIN CAGE_CONCEPTS AS CC ON CC.CC_CONCEPT_ID = CSTC.CSTC_CONCEPT_ID
           INNER JOIN CAGE_SOURCE_TARGET AS CST ON CST.CST_SOURCE_TARGET_ID = CSTC.CSTC_SOURCE_TARGET_ID
     WHERE CSM.CSM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
       AND CC.CC_SHOW_IN_REPORT = 1
       AND CC.CC_ENABLED = 1
       AND (CC.CC_TYPE = 1 OR CC_CONCEPT_ID IN (9, 10,11))
       AND CSTC.CSTC_ENABLED = 1
       AND CSM_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)
  GROUP BY CSM.CSM_SOURCE_TARGET_ID
         , CSM.CSM_CONCEPT_ID
         , CASE CSM.CSM_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CSM.CSM_ISO_CODE END 
         , CASE CSM.CSM_ISO_CODE WHEN 'X01' THEN 1001 ELSE CASE WHEN CSM_CAGE_CURRENCY_TYPE < 1000 THEN 0 ELSE CSM_CAGE_CURRENCY_TYPE END END  
         , CST.CST_SOURCE_TARGET_NAME
         , CC.CC_DESCRIPTION
         ) as a
    LEFT  JOIN CURRENCY_EXCHANGE ON CM_ISO_CODE = CE_CURRENCY_ISO_CODE AND CE_TYPE = 0
  ORDER BY CM_SOURCE_TARGET_ID
         , CASE CM_CONCEPT_ID WHEN 0 THEN '000' ELSE CC_DESCRIPTION END
         , CASE WHEN CAGE_CURRENCY_TYPE < 1000 THEN 0
                WHEN CAGE_CURRENCY_TYPE = 1003 THEN 9999999
                ELSE 1 END
         , CE_CURRENCY_ORDER
         , CM_ISO_CODE
         , CAGE_CURRENCY_TYPE 
         
     -- TABLE[2]: Get dynamic liabilities
    SELECT CC.CC_CONCEPT_ID AS LIABILITY_CONCEPT_ID
         , CASE CC_CONCEPT_ID WHEN 0 THEN '000' ELSE CC_DESCRIPTION END AS LIABILITY_NAME 
         , CASE CSM_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CSM_ISO_CODE END AS LIABILITY_ISO_CODE
         , CASE WHEN CASE CSM_ISO_CODE WHEN 'X01' THEN 1001 ELSE CSM_CAGE_CURRENCY_TYPE END < 1000 THEN 0
                ELSE CSM_CAGE_CURRENCY_TYPE END   AS LIABILITY_CAGE_CURRENCY_TYPE
         , SUM(CSM.CSM_VALUE) AS LIABILITY_VALUE
      FROM CAGE_SESSION_METERS AS CSM
     INNER JOIN CAGE_CONCEPTS AS CC ON CC.CC_CONCEPT_ID = CSM.CSM_CONCEPT_ID
      LEFT JOIN CURRENCY_EXCHANGE ON CSM_ISO_CODE = CE_CURRENCY_ISO_CODE AND CE_TYPE = 0
     WHERE CC.CC_ENABLED = 1
       AND CC.CC_SHOW_IN_REPORT = 1
       AND CC.CC_IS_PROVISION = 1
       AND CSM.CSM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
       AND CSM_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)
  GROUP BY CC.CC_CONCEPT_ID  
         , CASE CC_CONCEPT_ID WHEN 0 THEN '000' ELSE CC_DESCRIPTION END
         , CASE WHEN CASE CSM_ISO_CODE WHEN 'X01' THEN 1001 ELSE CSM_CAGE_CURRENCY_TYPE END < 1000 THEN 0
                ELSE CSM_CAGE_CURRENCY_TYPE END
         , CE_CURRENCY_ORDER
         , CASE CSM_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CSM_ISO_CODE END
  ORDER BY CC.CC_CONCEPT_ID  
         , CASE CC_CONCEPT_ID WHEN 0 THEN '000' ELSE CC_DESCRIPTION END
         , CASE WHEN CASE CSM_ISO_CODE WHEN 'X01' THEN 1001 ELSE CSM_CAGE_CURRENCY_TYPE END < 1000 THEN 0
                ELSE CSM_CAGE_CURRENCY_TYPE END
         , CE_CURRENCY_ORDER
         , CASE CSM_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CSM_ISO_CODE END
      
    -- TEMP TABLE[3]: Get session sumary
    SELECT CMD_ISO_CODE AS ISO_CODE
         , CASE WHEN CAGE_CURRENCY_TYPE = 99 THEN 0 ELSE CAGE_CURRENCY_TYPE END AS CAGE_CURRENCY_TYPE
         , ISNULL(ORIGIN, 0) AS ORIGIN
         , SUM(CMD_DEPOSITS) AS DEPOSITS
         , SUM(CMD_WITHDRAWALS) AS WITHDRAWALS
		 INTO #TABLE_TEMP_3
      FROM (SELECT CASE CMD_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CMD_ISO_CODE END AS CMD_ISO_CODE
                 , CASE WHEN CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE CAGE_CURRENCY_TYPE END < 1000 THEN 0
                        ELSE CAGE_CURRENCY_TYPE END AS CAGE_CURRENCY_TYPE
                 , ORIGIN
                 , SUM(CMD_DEPOSITS) CMD_DEPOSITS
                 , SUM(CMD_WITHDRAWALS) CMD_WITHDRAWALS      
              FROM (SELECT CASE WHEN CMD_QUANTITY >= 0 THEN @BillsId
                                ELSE CMD_QUANTITY END  AS ORIGIN
                         , CASE CMD_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CMD_ISO_CODE END AS CMD_ISO_CODE
                         , CASE WHEN CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE CMD_CAGE_CURRENCY_TYPE END < 1000 THEN 0
                                ELSE CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE ISNULL(CMD_CAGE_CURRENCY_TYPE, 0) END END  AS CAGE_CURRENCY_TYPE
                         , CMD_DENOMINATION
                         , CASE WHEN CGM_TYPE IN (100, 101, 103, 104, 108) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY < 0 AND CMD_QUANTITY <> @TicketsId THEN CMD_DENOMINATION
                                                                                            WHEN CMD_QUANTITY = @TicketsId THEN 0
                                                                                            WHEN CMD_CAGE_CURRENCY_TYPE = @CageCurrencyType_ColorChips THEN CMD_QUANTITY
                                                                                       ELSE CMD_QUANTITY * CMD_DENOMINATION END), 0)
                                ELSE 0 END AS CMD_DEPOSITS
                         , CASE WHEN CGM_TYPE IN (0, 1, 4, 5) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY < 0 AND CMD_QUANTITY <> @TicketsId THEN CMD_DENOMINATION
                                                                                    WHEN CMD_QUANTITY = @TicketsId THEN 0
                                                                                    WHEN CMD_CAGE_CURRENCY_TYPE = @CageCurrencyType_ColorChips THEN CMD_QUANTITY
                                                                               ELSE CMD_QUANTITY * CMD_DENOMINATION END), 0)
                                ELSE 0 END AS CMD_WITHDRAWALS
                      FROM CAGE_MOVEMENTS 
                INNER JOIN CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          
                     WHERE CGM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
                       AND (CGM_STATUS IN (2, 3, 4, 8, 9, 10) 
                            OR (CGM_STATUS = 0 AND CGM_TYPE = 0))
                  GROUP BY CASE WHEN CMD_QUANTITY >= 0 THEN @BillsId
                                ELSE CMD_QUANTITY END  
                         , CASE CMD_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CMD_ISO_CODE END 
                         , CASE WHEN CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE CMD_CAGE_CURRENCY_TYPE END < 1000 THEN 0
                                ELSE CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE ISNULL(CMD_CAGE_CURRENCY_TYPE, 0) END END 
                         , CMD_DENOMINATION
                         , CGM_TYPE 
                  -- Tickets 
                  UNION ALL
                  
                    SELECT @TicketsId AS ORIGIN
                         , ISNULL(TI_CUR0, @NationalIsoCode) AS CMD_ISO_CODE 
                         , ISNULL(CMD_CAGE_CURRENCY_TYPE, 0) AS CAGE_CURRENCY_TYPE                                    
                         , 1 AS CMD_DENOMINATION
                         , CASE WHEN CGM_TYPE IN (100, 101, 103, 104, 108) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY = @TicketsId THEN 
                                                                                                 CASE WHEN TI_AMT0 IS NULL THEN TI_AMOUNT 
                                                                                                      ELSE TI_AMT0 END
                                                                                       ELSE 0 END), 0)
                                ELSE 0 END AS CMD_DEPOSITS
                         , CASE WHEN CGM_TYPE IN (0, 1, 4, 5) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY = @TicketsId THEN 
                                                                                         CASE WHEN TI_AMT0 IS NULL THEN TI_AMOUNT 
                                                                                              ELSE TI_AMT0 END
                                                                                    ELSE 0 END), 0)
                                ELSE 0 END AS CMD_WITHDRAWALS
                      FROM CAGE_MOVEMENTS 
                           INNER JOIN   CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          
                           INNER JOIN   TICKETS ON TICKETS.TI_COLLECTED_MONEY_COLLECTION = CAGE_MOVEMENTS.CGM_MC_COLLECTION_ID     
                     WHERE  CGM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
                  AND (CGM_STATUS IN (2, 3, 4, 8, 9, 10) 
                            OR (CGM_STATUS = 0 AND CGM_TYPE = 0)) AND CMD_QUANTITY = @TicketsId  
              AND CAGE_MOVEMENTS.CGM_TYPE <> @CageOperationType_FromTerminal
                     GROUP BY ISNULL(TI_CUR0, @NationalIsoCode) 
                         , ISNULL(CMD_CAGE_CURRENCY_TYPE, 0)                                 
                         , CGM_TYPE
                   ) AS T1        
          GROUP BY ORIGIN
                  ,CASE CMD_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CMD_ISO_CODE END 
                 , CASE WHEN CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE CAGE_CURRENCY_TYPE END < 1000 THEN 0
                        ELSE CAGE_CURRENCY_TYPE END 
                 

            -- GET Terminal value-in amounts
              
              UNION ALL 
              
              SELECT CASE TI_CUR0 WHEN 'X01' THEN @NationalIsoCode ELSE ISNULL(TI_CUR0, @NationalIsoCode) END AS CMD_ISO_CODE
                    , 0 AS CAGE_CURRENCY_TYPE
                    , @TicketsId AS ORIGIN
                    , ISNULL(SUM (CASE WHEN TI_AMT0 IS NULL THEN TI_AMOUNT 
                                        ELSE TI_AMT0 END),0)  AS DEPOSITS
                    , 0 AS WITHDRAWS
              FROM MONEY_COLLECTIONS
                  INNER JOIN CAGE_MOVEMENTS ON CGM_MC_COLLECTION_ID = MC_COLLECTION_ID 
          INNER JOIN TICKETS ON TICKETS.TI_COLLECTED_MONEY_COLLECTION = CAGE_MOVEMENTS.CGM_MC_COLLECTION_ID
             WHERE CAGE_MOVEMENTS.CGM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
               AND CAGE_MOVEMENTS.CGM_TYPE = @CAGEOPERATIONTYPE_FROMTERMINAL
               AND (CGM_STATUS IN (2, 3, 4, 8, 9, 10) 
                    OR (CGM_STATUS = 0 AND CGM_TYPE = 0))           
             GROUP BY CASE TI_CUR0 WHEN 'X01' THEN @NationalIsoCode ELSE ISNULL(TI_CUR0, @NationalIsoCode) END, CGM_TYPE

              UNION ALL
              
               SELECT CASE TE_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE ISNULL(TE_ISO_CODE, @NationalIsoCode) END AS CMD_ISO_CODE
                    , 0 AS CAGE_CURRENCY_TYPE
                    , @BillsId AS ORIGIN
                    , MC_COLLECTED_BILL_AMOUNT + MC_COLLECTED_COIN_AMOUNT AS DEPOSITS
                    , 0 AS WITHDRAWS
              FROM MONEY_COLLECTIONS
                  INNER JOIN CAGE_MOVEMENTS ON CGM_MC_COLLECTION_ID = MC_COLLECTION_ID 
                  INNER JOIN TERMINALS ON TE_TERMINAL_ID = MC_TERMINAL_ID
             WHERE CAGE_MOVEMENTS.CGM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
               AND CAGE_MOVEMENTS.CGM_TYPE = @CageOperationType_FromTerminal
               AND (CGM_STATUS IN (2, 3, 4, 8, 9, 10) 
                    OR (CGM_STATUS = 0 AND CGM_TYPE = 0))
                                      
          ) AS _TBL_GLB
  LEFT JOIN CURRENCY_EXCHANGE ON CMD_ISO_CODE = CE_CURRENCY_ISO_CODE AND CE_TYPE = 0
      WHERE CMD_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)
   GROUP BY CAGE_CURRENCY_TYPE 
          , CE_CURRENCY_ORDER
          , CMD_ISO_CODE 
          , ORIGIN  
   ORDER BY CAGE_CURRENCY_TYPE 
          , CE_CURRENCY_ORDER
          , CMD_ISO_CODE 
          , ORIGIN DESC 
    
    -- TABLE[3]    
        SELECT * FROM #TABLE_TEMP_3
			    WHERE ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)   
        DROP TABLE #TABLE_TEMP_3
        
    -- TABLE[4]: Other System Concepts    
    SELECT * FROM (       
    SELECT CSM_CONCEPT_ID AS CM_CONCEPT_ID
         , CC.CC_DESCRIPTION AS CM_DESCRIPTION
         , CASE CSM_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CSM.CSM_ISO_CODE END AS CM_ISO_CODE
         , CASE CSM_ISO_CODE WHEN 'X01' THEN 1001 ELSE CASE WHEN CSM_CAGE_CURRENCY_TYPE < 1000 THEN 0 ELSE CSM_CAGE_CURRENCY_TYPE END END AS CAGE_CURRENCY_TYPE
         , SUM(CSM_VALUE) AS CM_VALUE
      FROM CAGE_SESSION_METERS AS CSM
           INNER JOIN CAGE_CONCEPTS AS CC ON CSM.CSM_CONCEPT_ID = CC.CC_CONCEPT_ID
     WHERE CSM_SOURCE_TARGET_ID = 0
       AND CSM_CONCEPT_ID IN (7, 8)
       AND CSM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
       AND CSM_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)
  GROUP BY CSM_CONCEPT_ID
         , CC.CC_DESCRIPTION
         , CASE CSM_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CSM.CSM_ISO_CODE END 
         , CASE CSM_ISO_CODE WHEN 'X01' THEN 1001 ELSE CASE WHEN CSM_CAGE_CURRENCY_TYPE < 1000 THEN 0 ELSE CSM_CAGE_CURRENCY_TYPE END END
         ) AS A
  ORDER BY CM_CONCEPT_ID
         , CASE CM_ISO_CODE WHEN @NationalIsoCode THEN '000' ELSE CM_ISO_CODE END
         , CAGE_CURRENCY_TYPE
         
     -- TABLE[5]: Cage Counts
    SELECT ORIGIN AS ORIGIN
         , CASE CMD_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CMD_ISO_CODE END AS ISO_CODE
         , CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE CASE WHEN CAGE_CURRENCY_TYPE = 1 THEN 0 ELSE CAGE_CURRENCY_TYPE END END AS CAGE_CURRENCY_TYPE
         , ISNULL(SUM(CMD_DEPOSITS), 0) AS DEPOSITS
      FROM (SELECT CASE WHEN CMD_QUANTITY >= 0 THEN 0
                        ELSE CMD_QUANTITY END  AS ORIGIN
                 , CMD_ISO_CODE
                 , ISNULL(CMD_CAGE_CURRENCY_TYPE, 0) AS CAGE_CURRENCY_TYPE
                 , ISNULL(SUM (  CASE WHEN CMD_QUANTITY >= 0 THEN CMD_QUANTITY ELSE 1 END 
                               * CASE WHEN CGM_TYPE = 99 THEN -1 ELSE 1 END
                               * CASE WHEN CMD_CAGE_CURRENCY_TYPE = @CageCurrencyType_ColorChips THEN 1 ELSE CMD_DENOMINATION END), 0) AS CMD_DEPOSITS
              FROM CAGE_MOVEMENTS 
                   INNER JOIN CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          
             WHERE CGM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS) and CGM_TYPE IN (99, 199)
          GROUP BY CMD_ISO_CODE, ISNULL(CMD_CAGE_CURRENCY_TYPE, 0), CMD_DENOMINATION, CGM_TYPE, CMD_QUANTITY
            ) AS T1           
   GROUP BY ORIGIN, CASE CMD_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CMD_ISO_CODE END 
         , CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE CASE WHEN CAGE_CURRENCY_TYPE = 1 THEN 0 ELSE CAGE_CURRENCY_TYPE END END 
           
    UNION ALL
              
    SELECT NULL AS ORIGIN
         , CASE CMD_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CMD_ISO_CODE END AS ISO_CODE
         , CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE CASE WHEN CAGE_CURRENCY_TYPE = 1 THEN 0 ELSE CAGE_CURRENCY_TYPE END END AS CAGE_CURRENCY_TYPE
         , ISNULL(SUM(CMD_DEPOSITS), 0) AS DEPOSITS
      FROM (SELECT CMD_ISO_CODE
                 , ISNULL(CMD_CAGE_CURRENCY_TYPE, 0) AS CAGE_CURRENCY_TYPE
                 , ISNULL(SUM (  CASE WHEN CMD_QUANTITY >= 0 THEN CMD_QUANTITY ELSE 1 END 
                               * CASE WHEN CGM_TYPE = 99 THEN -1 ELSE 1 END
                               * CASE WHEN CMD_CAGE_CURRENCY_TYPE = @CageCurrencyType_ColorChips THEN 1 ELSE CMD_DENOMINATION END), 0) AS CMD_DEPOSITS
              FROM CAGE_MOVEMENTS 
                   INNER JOIN CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          
             WHERE CGM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS) and CGM_TYPE IN (99, 199)
          GROUP BY CMD_ISO_CODE, ISNULL(CMD_CAGE_CURRENCY_TYPE, 0), CMD_DENOMINATION, CGM_TYPE, CMD_QUANTITY
           ) AS T1           
  GROUP BY CASE CMD_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CMD_ISO_CODE END 
         , CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE CASE WHEN CAGE_CURRENCY_TYPE = 1 THEN 0 ELSE CAGE_CURRENCY_TYPE END END 
 
    --DROP TABLE #TMP_CAGE_SESSIONS 

  END -- IF @pCageSessionIds IS NOT NULL THEN
      
  ELSE -- IF @pCageSessionIds IS NULL
  BEGIN
      
    -- TABLE[1]: Get cage sessions information
    SELECT a.* 
          , (ROW_NUMBER() OVER(ORDER BY CM_SOURCE_TARGET_ID
          , CASE CM_CONCEPT_ID WHEN 0 THEN '000' ELSE CC_DESCRIPTION END
          , CASE WHEN CAGE_CURRENCY_TYPE < 1000 THEN 0
                 WHEN CAGE_CURRENCY_TYPE = 1003 THEN 9999999
                 ELSE 1 END
          , CE_CURRENCY_ORDER
          , CM_ISO_CODE
          , CAGE_CURRENCY_TYPE)) as ORDEN
    FROM (
    SELECT CM.CM_SOURCE_TARGET_ID
         , CM.CM_CONCEPT_ID
         , CASE CM.CM_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CM.CM_ISO_CODE END AS CM_ISO_CODE
         , CASE CM.CM_ISO_CODE WHEN 'X01' THEN 1001 ELSE CASE WHEN CM_CAGE_CURRENCY_TYPE < 1000 THEN 0 ELSE CM_CAGE_CURRENCY_TYPE END END AS CAGE_CURRENCY_TYPE
         , SUM(CM.CM_VALUE_IN) AS CM_VALUE_IN
         , SUM(CM.CM_VALUE_OUT) AS CM_VALUE_OUT
         , CST.CST_SOURCE_TARGET_NAME
         , CC.CC_DESCRIPTION
      FROM CAGE_METERS AS CM
           INNER JOIN CAGE_SOURCE_TARGET_CONCEPTS AS CSTC ON CSTC.CSTC_CONCEPT_ID = CM.CM_CONCEPT_ID AND CSTC.CSTC_SOURCE_TARGET_ID = CM.CM_SOURCE_TARGET_ID
           INNER JOIN CAGE_CONCEPTS AS CC ON CC.CC_CONCEPT_ID = CSTC.CSTC_CONCEPT_ID
           INNER JOIN CAGE_SOURCE_TARGET AS CST ON CST.CST_SOURCE_TARGET_ID = CSTC.CSTC_SOURCE_TARGET_ID
     WHERE (CC.CC_TYPE = 1 OR CC_CONCEPT_ID IN (9, 10, 11))
       AND CC.CC_ENABLED = 1
       AND CSTC.CSTC_ENABLED = 1
       AND CC.CC_SHOW_IN_REPORT = 1
       AND CM_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)
  GROUP BY CM.CM_SOURCE_TARGET_ID
         , CM.CM_CONCEPT_ID
         , CASE CM.CM_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CM.CM_ISO_CODE END 
         , CASE CM.CM_ISO_CODE WHEN 'X01' THEN 1001 ELSE CASE WHEN CM_CAGE_CURRENCY_TYPE < 1000 THEN 0 ELSE CM_CAGE_CURRENCY_TYPE END END 
         , CST.CST_SOURCE_TARGET_NAME
         , CC.CC_DESCRIPTION
         ) as a
    LEFT  JOIN CURRENCY_EXCHANGE ON CM_ISO_CODE = CE_CURRENCY_ISO_CODE AND CE_TYPE = 0
  ORDER BY CM_SOURCE_TARGET_ID
         , CASE CM_CONCEPT_ID WHEN 0 THEN '000' ELSE CC_DESCRIPTION END
         , CASE WHEN CAGE_CURRENCY_TYPE < 1000 THEN 0
                WHEN CAGE_CURRENCY_TYPE = 1003 THEN 9999999
                ELSE 1 END
         , CE_CURRENCY_ORDER
         , CM_ISO_CODE
         , CAGE_CURRENCY_TYPE

    -- TABLE[2]: Get dynamic liabilities
    SELECT CC_CONCEPT_ID AS LIABILITY_CONCEPT_ID
         , CC_DESCRIPTION AS LIABILITY_NAME 
         , CASE CM.CM_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CM.CM_ISO_CODE END AS LIABILITY_ISO_CODE
         , CASE WHEN CASE CM.CM_ISO_CODE WHEN 'X01' THEN 1001 ELSE CM_CAGE_CURRENCY_TYPE END < 1000 THEN 0
                ELSE CM_CAGE_CURRENCY_TYPE END AS LIABILITY_CAGE_CURRENCY_TYPE
         , SUM(CM.CM_VALUE) AS LIABILITY_VALUE
      FROM CAGE_METERS AS CM
     INNER JOIN CAGE_CONCEPTS AS CC ON CC.CC_CONCEPT_ID = CM.CM_CONCEPT_ID
      LEFT JOIN CURRENCY_EXCHANGE ON CM_ISO_CODE = CE_CURRENCY_ISO_CODE AND CE_TYPE = 0
     WHERE CC.CC_ENABLED = 1
       AND CC.CC_SHOW_IN_REPORT = 1
       AND CC.CC_IS_PROVISION = 1
       AND CM_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)
  GROUP BY CC.CC_CONCEPT_ID  
         , CC_DESCRIPTION
         , CASE WHEN CASE CM.CM_ISO_CODE WHEN 'X01' THEN 1001 ELSE CM_CAGE_CURRENCY_TYPE END < 1000 THEN 0
                ELSE CM_CAGE_CURRENCY_TYPE END
         , CE_CURRENCY_ORDER
         , CASE CM.CM_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CM.CM_ISO_CODE END
  ORDER BY CC.CC_CONCEPT_ID  
         , CASE WHEN CASE CM.CM_ISO_CODE WHEN 'X01' THEN 1001 ELSE CM_CAGE_CURRENCY_TYPE END < 1000 THEN 0
                ELSE CM_CAGE_CURRENCY_TYPE END
         , CE_CURRENCY_ORDER
         , CASE CM.CM_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CM.CM_ISO_CODE END

      
   -- TEMP TABLE[3]: Get session sumary
   SELECT   CMD_ISO_CODE
          , CAGE_CURRENCY_TYPE
		      , ORIGIN
		      , CMD_DEPOSITS
		      , CMD_WITHDRAWALS 
	   INTO   #TEMP_TABLE_3    
     FROM (
			      SELECT   CASE CMD_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CMD_ISO_CODE END AS CMD_ISO_CODE
                   , CASE WHEN CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE CAGE_CURRENCY_TYPE END < 1000 THEN 0
                          ELSE CAGE_CURRENCY_TYPE END AS CAGE_CURRENCY_TYPE
                   , ORIGIN
                   , SUM(CMD_DEPOSITS) CMD_DEPOSITS
                   , SUM(CMD_WITHDRAWALS) CMD_WITHDRAWALS      
              FROM ( SELECT   CASE WHEN CMD_QUANTITY >= 0 THEN @BillsId
                                   ELSE CMD_QUANTITY END AS ORIGIN
                            , CASE CMD_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CMD_ISO_CODE END AS CMD_ISO_CODE
                            , CASE WHEN CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE CMD_CAGE_CURRENCY_TYPE END < 1000 THEN 0
                                   ELSE CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE ISNULL(CMD_CAGE_CURRENCY_TYPE, 0) END END  AS CAGE_CURRENCY_TYPE
                            , CMD_DENOMINATION
                            , CASE WHEN CGM_TYPE IN (100, 101, 103, 104, 108) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY < 0 AND CMD_QUANTITY <> @TicketsId THEN CMD_DENOMINATION
                                                                                               WHEN CMD_QUANTITY = @TicketsId THEN 0
                                                                                               WHEN CMD_CAGE_CURRENCY_TYPE = @CageCurrencyType_ColorChips THEN CMD_QUANTITY
                                                                                               ELSE CMD_QUANTITY * CMD_DENOMINATION END), 0)
                                   ELSE 0 END AS CMD_DEPOSITS
                            , CASE WHEN CGM_TYPE IN (0, 1, 4, 5) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY < 0 AND CMD_QUANTITY <> @TicketsId THEN CMD_DENOMINATION
                                                                                    WHEN CMD_QUANTITY = @TicketsId THEN 0
                                                                                    WHEN CMD_CAGE_CURRENCY_TYPE = @CageCurrencyType_ColorChips THEN CMD_QUANTITY
                                                                                    ELSE CMD_QUANTITY * CMD_DENOMINATION END), 0)
                                ELSE 0 END AS CMD_WITHDRAWALS
                      FROM CAGE_MOVEMENTS 
                INNER JOIN CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          
                     WHERE CGM_STATUS IN (2, 3, 4, 8, 9, 10) 
                        OR (CGM_STATUS = 0 AND CGM_TYPE = 0)
                  GROUP BY CASE WHEN CMD_QUANTITY >= 0 THEN @BillsId
                                ELSE CMD_QUANTITY END 
                         , CASE CMD_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CMD_ISO_CODE END 
                         , CASE WHEN CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE CMD_CAGE_CURRENCY_TYPE END < 1000 THEN 0
                                ELSE CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE ISNULL(CMD_CAGE_CURRENCY_TYPE, 0) END END 
                         , CMD_DENOMINATION
                         , CGM_TYPE

          -- Tickets 
                  UNION ALL
                  
                    SELECT @TicketsId AS ORIGIN
                         , ISNULL(TI_CUR0, @NationalIsoCode) AS CMD_ISO_CODE 
                         , ISNULL(CMD_CAGE_CURRENCY_TYPE, 0) AS CAGE_CURRENCY_TYPE                                   
                         , 1 AS CMD_DENOMINATION
                         , CASE WHEN CGM_TYPE IN (100, 101, 103, 104, 108) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY = @TicketsId THEN 
                                                                                                 CASE WHEN TI_AMT0 IS NULL THEN TI_AMOUNT 
                                                                                                      ELSE TI_AMT0 END
                                                                                       ELSE 0 END), 0)
                                ELSE 0 END AS CMD_DEPOSITS
                         , CASE WHEN CGM_TYPE IN (0, 1, 4, 5) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY = @TicketsId THEN 
                                                                                         CASE WHEN TI_AMT0 IS NULL THEN TI_AMOUNT 
                                                                                              ELSE TI_AMT0 END
                                                                                    ELSE 0 END), 0)
                                ELSE 0 END AS CMD_WITHDRAWALS
                      FROM CAGE_MOVEMENTS 
                           INNER JOIN   CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          
                           INNER JOIN   TICKETS ON TICKETS.TI_COLLECTED_MONEY_COLLECTION = CAGE_MOVEMENTS.CGM_MC_COLLECTION_ID     
                     WHERE  (CGM_STATUS IN (2, 3, 4, 8, 9, 10) 
                            OR (CGM_STATUS = 0 AND CGM_TYPE = 0)) AND CMD_QUANTITY = @TicketsId  
                            AND CAGE_MOVEMENTS.CGM_TYPE <> @CageOperationType_FromTerminal
                     GROUP BY ISNULL(TI_CUR0, @NationalIsoCode) 
                         , ISNULL(CMD_CAGE_CURRENCY_TYPE, 0)                             
                         , CGM_TYPE 
                    ) AS T1
           GROUP BY ORIGIN
                  ,CASE CMD_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CMD_ISO_CODE END 
                 , CASE WHEN CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE CAGE_CURRENCY_TYPE END < 1000 THEN 0
                        ELSE CAGE_CURRENCY_TYPE END 
                 

              -- GET Terminal value-in amounts
              
              UNION ALL 
              
              SELECT ISNULL(TI_CUR0, @NationalIsoCode) AS CMD_ISO_CODE
                    , 0 AS CAGE_CURRENCY_TYPE
                    , @TicketsId AS ORIGIN
                    , ISNULL(SUM (CASE WHEN TI_AMT0 IS NULL THEN TI_AMOUNT 
                                       ELSE TI_AMT0 END),0)  AS DEPOSITS
                    , 0 AS WITHDRAWS
              FROM MONEY_COLLECTIONS
        INNER JOIN CAGE_MOVEMENTS ON CGM_MC_COLLECTION_ID = MC_COLLECTION_ID 
        INNER JOIN TICKETS ON TICKETS.TI_COLLECTED_MONEY_COLLECTION = CAGE_MOVEMENTS.CGM_MC_COLLECTION_ID            
             WHERE CAGE_MOVEMENTS.CGM_TYPE = @CAGEOPERATIONTYPE_FROMTERMINAL
               AND (CGM_STATUS IN (2, 3, 4, 8, 9, 10) 
                    OR (CGM_STATUS = 0 AND CGM_TYPE = 0))
             GROUP BY ISNULL(TI_CUR0, @NationalIsoCode) 

              UNION ALL
              
                SELECT ISNULL(TE_ISO_CODE, @NationalIsoCode) AS CMD_ISO_CODE
                    , 0 AS CAGE_CURRENCY_TYPE
                    , @BillsId AS ORIGIN
                    , MC_COLLECTED_BILL_AMOUNT + MC_COLLECTED_COIN_AMOUNT  AS DEPOSITS
                    , 0 AS WITHDRAWS
              FROM MONEY_COLLECTIONS
                  INNER JOIN CAGE_MOVEMENTS ON CGM_MC_COLLECTION_ID = MC_COLLECTION_ID 
          INNER JOIN TERMINALS ON TE_TERMINAL_ID = MC_TERMINAL_ID
             WHERE CAGE_MOVEMENTS.CGM_TYPE = @CageOperationType_FromTerminal
               AND (CGM_STATUS IN (2, 3, 4, 8, 9, 10) 
                    OR (CGM_STATUS = 0 AND CGM_TYPE = 0))
                          
          ) AS _TBL_GLB



	SELECT   CMD_ISO_CODE AS ISO_CODE
           , CASE WHEN CAGE_CURRENCY_TYPE = 99 THEN 0 ELSE CAGE_CURRENCY_TYPE END AS CAGE_CURRENCY_TYPE
           , ISNULL(ORIGIN, 0) AS ORIGIN
           , SUM(CMD_DEPOSITS) AS DEPOSITS
           , SUM(CMD_WITHDRAWALS) AS WITHDRAWALS  
	  INTO   #TEMP_TABLE_5
	  FROM   #TEMP_TABLE_3 TEMP_3   		
 LEFT JOIN   CURRENCY_EXCHANGE CE ON TEMP_3.CMD_ISO_CODE = CE.CE_CURRENCY_ISO_CODE AND CE.CE_TYPE = 0
  GROUP BY   CAGE_CURRENCY_TYPE 
           , CE_CURRENCY_ORDER
           , CMD_ISO_CODE 
           , ORIGIN  
  ORDER BY   CAGE_CURRENCY_TYPE 
           , CE_CURRENCY_ORDER
           , CMD_ISO_CODE 
           , ORIGIN DESC 
		   
	-- TABLE[3]:
	  SELECT * FROM #TEMP_TABLE_5
	           WHERE ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)   
      DROP TABLE #TEMP_TABLE_3
			DROP TABLE #TEMP_TABLE_5
         
    -- TABLE[4]: Other System Concepts    
    SELECT * FROM (       
    SELECT CM_CONCEPT_ID
         , CC.CC_DESCRIPTION AS CM_DESCRIPTION
         , CASE CM_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CM.CM_ISO_CODE END AS CM_ISO_CODE
         , CASE CM_ISO_CODE WHEN 'X01' THEN 1001 ELSE CASE WHEN CM_CAGE_CURRENCY_TYPE < 1000 THEN 0 ELSE CM_CAGE_CURRENCY_TYPE END END AS CAGE_CURRENCY_TYPE
         , SUM(CM_VALUE) AS CM_VALUE
      FROM CAGE_METERS AS CM
           INNER JOIN CAGE_CONCEPTS AS CC ON CM.CM_CONCEPT_ID = CC.CC_CONCEPT_ID
     WHERE CM_SOURCE_TARGET_ID = 0
       AND CM_CONCEPT_ID IN (7, 8)
       AND CM_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)
  GROUP BY CM_CONCEPT_ID
         , CASE CM_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CM.CM_ISO_CODE END
         , CASE CM_ISO_CODE WHEN 'X01' THEN 1001 ELSE CASE WHEN CM_CAGE_CURRENCY_TYPE < 1000 THEN 0 ELSE CM_CAGE_CURRENCY_TYPE END END
         , CC.CC_DESCRIPTION
         ) AS A
  ORDER BY CM_CONCEPT_ID
         , CASE CM_ISO_CODE WHEN @NationalIsoCode THEN '000' ELSE CM_ISO_CODE END
         , CAGE_CURRENCY_TYPE
    
    -- TABLE[5]: Cage Counts
    SELECT ORIGIN AS ORIGIN
         , CASE CMD_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CMD_ISO_CODE END AS ISO_CODE
         , CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE CASE WHEN CAGE_CURRENCY_TYPE = 1 THEN 0 ELSE CAGE_CURRENCY_TYPE END END AS CAGE_CURRENCY_TYPE
         , ISNULL(SUM(CMD_DEPOSITS), 0) AS DEPOSITS
      FROM (SELECT CASE WHEN CMD_QUANTITY >= 0 THEN 0
                        ELSE CMD_QUANTITY END  AS ORIGIN
                 , CMD_ISO_CODE
                 , ISNULL(CMD_CAGE_CURRENCY_TYPE, 0) AS CAGE_CURRENCY_TYPE
                 , ISNULL(SUM (  CASE WHEN CMD_QUANTITY >= 0 THEN CMD_QUANTITY ELSE 1 END 
                               * CASE WHEN CGM_TYPE = 99 THEN -1 ELSE 1 END
                               * CASE WHEN CMD_CAGE_CURRENCY_TYPE = @CageCurrencyType_ColorChips THEN 1 ELSE CMD_DENOMINATION END), 0) AS CMD_DEPOSITS
              FROM CAGE_MOVEMENTS 
                   INNER JOIN CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          
             WHERE CGM_TYPE IN (99, 199)
          GROUP BY CMD_ISO_CODE, ISNULL(CMD_CAGE_CURRENCY_TYPE, 0), CMD_DENOMINATION, CGM_TYPE, CMD_QUANTITY
            ) AS T1           
   GROUP BY ORIGIN, CASE CMD_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CMD_ISO_CODE END 
         , CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE CASE WHEN CAGE_CURRENCY_TYPE = 1 THEN 0 ELSE CAGE_CURRENCY_TYPE END END 
           
    UNION ALL
              
    SELECT NULL AS ORIGIN
         , CASE CMD_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CMD_ISO_CODE END AS ISO_CODE
         , CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE CASE WHEN CAGE_CURRENCY_TYPE = 1 THEN 0 ELSE CAGE_CURRENCY_TYPE END END AS CAGE_CURRENCY_TYPE
         , ISNULL(SUM(CMD_DEPOSITS), 0) AS DEPOSITS
      FROM (SELECT CMD_ISO_CODE
                 , ISNULL(CMD_CAGE_CURRENCY_TYPE, 0) AS CAGE_CURRENCY_TYPE
                 , ISNULL(SUM (  CASE WHEN CMD_QUANTITY >= 0 THEN CMD_QUANTITY ELSE 1 END 
                               * CASE WHEN CGM_TYPE = 99 THEN -1 ELSE 1 END
                               * CASE WHEN CMD_CAGE_CURRENCY_TYPE = @CageCurrencyType_ColorChips THEN 1 ELSE CMD_DENOMINATION END), 0) AS CMD_DEPOSITS
              FROM CAGE_MOVEMENTS 
                   INNER JOIN CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          
             WHERE CGM_TYPE IN (99, 199)
          GROUP BY CMD_ISO_CODE, ISNULL(CMD_CAGE_CURRENCY_TYPE, 0), CMD_DENOMINATION, CGM_TYPE, CMD_QUANTITY
           ) AS T1           
  GROUP BY CASE CMD_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CMD_ISO_CODE END 
         , CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE CASE WHEN CAGE_CURRENCY_TYPE = 1 THEN 0 ELSE CAGE_CURRENCY_TYPE END END 
 

  END -- ELSE
  
  -- TABLE[6]: Stock Stored 
  SELECT @StockIsStored

  DROP TABLE #TMP_CURRENCIES_ISO_CODES
  DROP TABLE #TMP_CAGE_SESSIONS
  
                   
                   
END -- CageGetGlobalReportData

GO

GRANT EXECUTE ON [dbo].[CageGetGlobalReportData] TO [wggui] WITH GRANT OPTION
GO



/****** Object:  StoredProcedure [dbo].[CageUpdateSystemMeters]    Script Date: 7/6/2018 11:53:52 ******/
IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('dbo.CageUpdateSystemMeters'))
   DROP PROCEDURE [dbo].[CageUpdateSystemMeters]
GO

CREATE PROCEDURE [dbo].[CageUpdateSystemMeters]
    @pCashierSessionId BIGINT
  AS
BEGIN             

  DECLARE @CageSessionId BIGINT
  DECLARE @NationalCurrency VARCHAR(3)
  DECLARE @Currencies AS VARCHAR(200)
  DECLARE @CurrentCurrency AS VARCHAR(3)
  DECLARE @CageCurrencyType INT
      
  DECLARE @Tax1 MONEY
  DECLARE @Tax2 MONEY
  DECLARE @TaxIEJC MONEY

  DECLARE @ClosingShort MONEY
  DECLARE @ClosingOver MONEY
  
  DECLARE @CardRefundable BIT
  DECLARE @CardDeposit MONEY
  
  SELECT @NationalCurrency = GP_KEY_VALUE FROM GENERAL_PARAMS 
   WHERE GP_GROUP_KEY = 'RegionalOptions' 
     AND GP_SUBJECT_KEY = 'CurrencyISOCode'
  
  
  ---- GET CAGE SESSION ID ----
  
  SET @CageSessionId = (SELECT TOP 1 CGM_CAGE_SESSION_ID 
              FROM CAGE_MOVEMENTS
              WHERE CGM_CASHIER_SESSION_ID = @pCashierSessionId 
              AND CGM_TYPE IN (0, 1, 4, 5)
              ORDER BY CGM_MOVEMENT_DATETIME DESC)

  IF @CageSessionId IS NULL
    SET @CageSessionId = (SELECT TOP 1 CGM_CAGE_SESSION_ID 
                FROM CAGE_MOVEMENTS
                WHERE CGM_CASHIER_SESSION_ID = @pCashierSessionId 
                AND CGM_TYPE IN (100, 101, 103, 104)
                ORDER BY CGM_MOVEMENT_DATETIME DESC)
      
  IF @CageSessionId IS NULL
      SET @CageSessionId = (SELECT TOP 1 CGS_CAGE_SESSION_ID 
                  FROM CAGE_SESSIONS
                  WHERE CGS_CLOSE_DATETIME IS NULL
                  ORDER BY CGS_CAGE_SESSION_ID DESC)
   
   IF @CageSessionId IS NULL
      SET @CageSessionId = (SELECT TOP 1 CGS_CAGE_SESSION_ID 
                  FROM CAGE_SESSIONS
                  ORDER BY CGS_CAGE_SESSION_ID DESC)
   
      IF @CageSessionId IS NOT NULL
      BEGIN
  
        -- FEDERAL TAX --
  
        SELECT @Tax1 = SUM(CM_ADD_AMOUNT) 
        FROM CASHIER_MOVEMENTS_GROUPED_BY_SESSION_ID  
           WHERE CM_TYPE IN (6) 
          AND CM_SESSION_ID = @pCashierSessionId

        SET @Tax1 = ISNULL(@Tax1, 0)
        
        EXEC [dbo].[CageUpdateMeter]
        @pValueIn = @Tax1,
        @pValueOut = 0,
        @pSourceTagetId = 0,
        @pConceptId = 3,
        @pIsoCode = @NationalCurrency,
        @pCageCurrencyType = 0,
        @pOperation = 1,
        @pSessionId = @CageSessionId,
        @pSessiongetMode = 0          
     
        -- STATE TAX --
   
        SELECT @Tax2 = SUM(CM_ADD_AMOUNT) 
        FROM CASHIER_MOVEMENTS_GROUPED_BY_SESSION_ID  
        WHERE CM_TYPE IN (14) 
          AND CM_SESSION_ID = @pCashierSessionId 
        
        SET @Tax2 = ISNULL(@Tax2, 0)
         
        EXEC [dbo].[CageUpdateMeter]
        @pValueIn = @Tax2,
        @pValueOut = 0,
        @pSourceTagetId = 0,
        @pConceptId = 2,
        @pIsoCode = @NationalCurrency,
        @pCageCurrencyType = 0,
        @pOperation = 1,
        @pSessionId = @CageSessionId,
        @pSessiongetMode = 0 
        
        -- IEJC TAX --
  
        SELECT @TaxIEJC = SUM(CM_ADD_AMOUNT) 
        FROM CASHIER_MOVEMENTS_GROUPED_BY_SESSION_ID  
        WHERE CM_TYPE IN (142,146) 
          AND CM_SESSION_ID = @pCashierSessionId
 
        SET @TaxIEJC = ISNULL(@TaxIEJC, 0)
        
        EXEC [dbo].[CageUpdateMeter]
        @pValueIn = @TaxIEJC,
        @pValueOut = 0,
        @pSourceTagetId = 0,
        @pConceptId = 12,
        @pIsoCode = @NationalCurrency,
        @pCageCurrencyType = @CageCurrencyType,
        @pOperation = 1,
        @pSessionId = @CageSessionId,
        @pSessiongetMode = 0 
        
        ---- CARD_PAYMENT ---- (IF @CardRefundable IS FALSE: WE DON'T NEED TO RETURN VALUE FOR LIABILITIES)                                                                    

        SELECT @CardRefundable = GP_KEY_VALUE 
        FROM GENERAL_PARAMS  
        WHERE GP_GROUP_KEY = 'Cashier'                             
          AND GP_SUBJECT_KEY = 'CardRefundable'                    

        IF ISNULL(@CardRefundable, 0) = 1                                                                        
        BEGIN                                                                                                   
          SELECT @CardDeposit = SUM(CASE WHEN CM_TYPE = 10 THEN -1 * CM_SUB_AMOUNT   
        ELSE CM_ADD_AMOUNT END)                                                  
                FROM CASHIER_MOVEMENTS_GROUPED_BY_SESSION_ID                                                    
                WHERE CM_TYPE IN (9, 10, 532, 533, 534, 535, 536, 544, 545, 546, 547, 548)                                             
                  AND CM_SESSION_ID IN(SELECT DISTINCT(CGM_CASHIER_SESSION_ID)                                   
                             FROM CAGE_MOVEMENTS                                                                     
                             WHERE CGM_CASHIER_SESSION_ID = @pCashierSessionId)
                                                    
          SET @CardDeposit = ISNULL(@CardDeposit, 0)
              
          EXEC [dbo].[CageUpdateMeter]
            @pValueIn = @CardDeposit,
            @pValueOut = 0,
            @pSourceTagetId = 0,
            @pConceptId = 5,
            @pIsoCode = @NationalCurrency,
            @pCageCurrencyType = @CageCurrencyType,
            @pOperation = 1,
            @pSessionId = @CageSessionId,
            @pSessiongetMode = 0
                     
        END                                                                                                     

      SELECT @Currencies = GP_KEY_VALUE 
      FROM GENERAL_PARAMS 
      WHERE GP_GROUP_KEY = 'RegionalOptions' 
        AND GP_SUBJECT_KEY = 'CurrenciesAccepted'
    
      -- Split currencies ISO codes      
       SELECT * INTO #TMP_CURRENCIES_ISO_CODES FROM 
       (  SELECT SST_VALUE AS CURRENCY_ISO_CODE, 0 AS CAGE_CURRENCY_TYPE FROM [SplitStringIntoTable] (@Currencies, ';', 1)
        UNION ALL
        SELECT [chs_iso_code] AS CURRENCY_ISO_CODE, [chs_chip_type] AS CAGE_CURRENCY_TYPE  FROM [chips_sets] 
       ) AS A
           
      DECLARE Curs_CageUpdateSystemMeters CURSOR FOR 
      SELECT CURRENCY_ISO_CODE, CAGE_CURRENCY_TYPE
        FROM #TMP_CURRENCIES_ISO_CODES 
      
      SET NOCOUNT ON;

      OPEN Curs_CageUpdateSystemMeters

      FETCH NEXT FROM Curs_CageUpdateSystemMeters INTO @CurrentCurrency, @CageCurrencyType
    
      WHILE @@FETCH_STATUS = 0
      BEGIN
  
        -- CLOSING SHORT --
        
        SELECT @ClosingShort = SUM(CM_ADD_AMOUNT) 
        FROM CASHIER_MOVEMENTS_GROUPED_BY_SESSION_ID  
        WHERE CM_TYPE IN (160) 
          AND CM_SESSION_ID = @pCashierSessionId 
          AND CM_CURRENCY_ISO_CODE = @CurrentCurrency
          AND CM_CAGE_CURRENCY_TYPE = @CageCurrencyType
            
        SET @ClosingShort = ISNULL(@ClosingShort, 0)
        
        EXEC [dbo].[CageUpdateMeter]
            @pValueIn = @ClosingShort,
            @pValueOut = 0,
            @pSourceTagetId = 0,
            @pConceptId = 7,
            @pIsoCode = @CurrentCurrency,
            @pCageCurrencyType = @CageCurrencyType, 
            @pOperation = 1,
            @pSessionId = @CageSessionId,
            @pSessiongetMode = 0 
        
        -- CLOSING OVER --
        
        SELECT @ClosingOver = SUM(CM_ADD_AMOUNT) 
        FROM CASHIER_MOVEMENTS_GROUPED_BY_SESSION_ID  
        WHERE CM_TYPE IN (161) 
          AND CM_SESSION_ID = @pCashierSessionId 
          AND CM_CURRENCY_ISO_CODE = @CurrentCurrency
          AND CM_CAGE_CURRENCY_TYPE = @CageCurrencyType
        
        SET @ClosingOver = ISNULL(@ClosingOver, 0)
        
        EXEC [dbo].[CageUpdateMeter]
            @pValueIn = @ClosingOver,
            @pValueOut = 0,
            @pSourceTagetId = 0,
            @pConceptId = 8,
            @pIsoCode = @CurrentCurrency,
            @pCageCurrencyType = @CageCurrencyType,
            @pOperation = 1,
            @pSessionId = @CageSessionId,
            @pSessiongetMode = 0 
          
        FETCH NEXT FROM Curs_CageUpdateSystemMeters INTO @CurrentCurrency, @CageCurrencyType
                
      END

      CLOSE Curs_CageUpdateSystemMeters
      DEALLOCATE Curs_CageUpdateSystemMeters

      END                                      

END -- CageUpdateSystemMeters
GO

GRANT EXECUTE ON [dbo].[CageUpdateSystemMeters] TO [wggui] WITH GRANT OPTION 
GO


IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetMonthlyNetwin]') AND type in (N'P', N'PC'))
   DROP PROCEDURE [dbo].[GetMonthlyNetwin]
GO

CREATE PROCEDURE [dbo].[GetMonthlyNetwin]
@pMonth int,
@pYear  int
AS
BEGIN

DECLARE @_aux_date AS DATETIME
DECLARE @_aux_date_init AS DATETIME
DECLARE @_aux_date_final AS DATETIME
DECLARE @_aux_site_name AS NVARCHAR(200)

-- Create temp table for whole report
CREATE TABLE #TT_MONTHLYNETWIN (
TSMH_MONTH				        NVARCHAR(5), 
SITE_NAME				          NVARCHAR(200), 
TE_NAME					          NVARCHAR(200), 
TE_SERIAL_NUMBER		      NVARCHAR(200),
BILL_IN					          MONEY, 
TICKET_IN_REDIMIBLE		    MONEY,
TICKET_OUT_REDIMIBLE	    MONEY,
TOTAL_IN				          MONEY,
TOTAL_OUT				          MONEY,
REDEEM_TICKETS			      MONEY,
TICKET_IN_PROMOTIONAL	    MONEY,
CANCELLED_CREDITS		      MONEY,
JACKPOTS				          MONEY,
LGA_NETWIN				        MONEY,
TOTAL_COIN_IN			        MONEY,
TOTAL_WON				          MONEY,
TOTAL_COIN_IN_TOTAL_WON	  MONEY)

--Obtain the INITIAL and FINAL date
set @_aux_date = CONVERT(DATETIME,CONVERT(NVARCHAR(4),@pYear) + '-' + CONVERT(NVARCHAR(2),@pMonth) + '-' +  '1')
set @_aux_date_init = CONVERT(NVARCHAR(25),DATEADD(dd,-(DAY(@_aux_date)-1),@_aux_date),101)
set @_aux_date_final = DATEADD(dd,-(DAY(DATEADD(mm,1,@_aux_date))),DATEADD(hh,23,DATEADD(mi,59,DATEADD(ss,59,DATEADD(mm,1,@_aux_date)))))

--Obtain site ID and site name
set @_aux_site_name = (SELECT GP_KEY_VALUE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Site' AND GP_SUBJECT_KEY = 'Identifier') + ' - ' + (SELECT GP_KEY_VALUE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Site' AND GP_SUBJECT_KEY = 'Name')

INSERT INTO #TT_MONTHLYNETWIN
SELECT    (CASE WHEN TSMH_GROUP_ID IS NULL THEN DATEPART(MM,TSMH_DATETIME) ELSE DATEPART(MM,TSMH_CREATED_DATETIME)END) AS TSMH_MONTH
        , @_aux_site_name AS SITE_NAME
        , TE_NAME
        , TE_SERIAL_NUMBER
        /*BILL IN*/
        , SUM( CASE WHEN TSMH_TYPE <> 20 THEN
                  - CASE WHEN (TSMH_METER_CODE = 11)                           THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               ELSE
                    CASE WHEN (TSMH_METER_CODE = 11)                           THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               END ) AS BILL_IN
        /*TICKET IN*/
        , SUM( CASE WHEN TSMH_TYPE <> 20 THEN
                  - CASE WHEN (TSMH_METER_CODE = 128)                          THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               ELSE
                    CASE WHEN (TSMH_METER_CODE = 128)                          THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               END ) AS TICKET_IN_REDIMIBLE
        /*TICKET OUT*/
        , SUM( CASE WHEN TSMH_TYPE <> 20 THEN
                  - CASE WHEN (TSMH_METER_CODE = 134)                          THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               ELSE
                    CASE WHEN (TSMH_METER_CODE = 134)                          THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               END ) AS TICKET_OUT_REDIMIBLE
        /*TOTAL IN = BILL IN + TICKET IN*/
        , SUM( CASE WHEN TSMH_TYPE <> 20 THEN
                  - CASE WHEN (TSMH_METER_CODE IN (11, 128))                   THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               ELSE
                    CASE WHEN (TSMH_METER_CODE IN (11, 128))                   THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               END ) AS TOTAL_IN
        /*TOTAL OUT = JACKPOTS + CANCELLED CREDITS + TICKET OUT*/
        , SUM( CASE WHEN TSMH_TYPE <> 20 THEN
                  - CASE WHEN (TSMH_METER_CODE IN (2, 3, 134))                 THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               ELSE
                    CASE WHEN (TSMH_METER_CODE IN (2, 3, 134))                 THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               END ) AS  TOTAL_OUT
        /*REDEEM TICKETS*/
        , SUM( CASE WHEN TSMH_TYPE <> 20 THEN 
                  - CASE WHEN (TSMH_METER_CODE = 128)                          THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
                  + CASE WHEN (TSMH_METER_CODE = 134)                          THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END 
               ELSE 
                    CASE WHEN (TSMH_METER_CODE = 128)                          THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
                  - CASE WHEN (TSMH_METER_CODE = 134)                          THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END 
               END ) AS  REDEEM_TICKETS
        /*TICKET IN PROMOTIONAL (redeem, no redeem)*/
        , SUM( CASE WHEN TSMH_TYPE <> 20 THEN
                  - CASE WHEN (TSMH_METER_CODE IN (130, 132))                  THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               ELSE
                    CASE WHEN (TSMH_METER_CODE IN (130, 132))                  THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               END ) AS TICKET_IN_PROMOTIONAL
        /*CANCELLED CREDITS*/
        , SUM( CASE WHEN TSMH_TYPE <> 20 THEN
                  - CASE WHEN (TSMH_METER_CODE = 3)                            THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               ELSE
                    CASE WHEN (TSMH_METER_CODE = 3)                            THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               END ) AS CANCELLED_CREDITS
        /*JACKPOTS*/
        , SUM( CASE WHEN TSMH_TYPE <> 20 THEN
                  - CASE WHEN (TSMH_METER_CODE = 2)                            THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               ELSE
                    CASE WHEN (TSMH_METER_CODE = 2)                            THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               END ) AS JACKPOTS
        /*LGA NETWIN*/
        , SUM( CASE WHEN TSMH_TYPE <> 20 THEN
                  - CASE WHEN (TSMH_METER_CODE IN (11, 128))                   THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
                  + CASE WHEN (TSMH_METER_CODE IN (3, 130, 132, 134))          THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               ELSE
                    CASE WHEN (TSMH_METER_CODE IN (11, 128))                   THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
                  - CASE WHEN (TSMH_METER_CODE IN (3, 130, 132, 134))          THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               END ) AS LGA_NETWIN
        /*TOTAL COIN IN*/
        , SUM( CASE WHEN TSMH_TYPE <> 20 THEN
                  - CASE WHEN (TSMH_METER_CODE = 0)								THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               ELSE
                    CASE WHEN (TSMH_METER_CODE = 0)								THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               END ) AS TOTAL_COIN_IN

        /*TOTAL WON = COIN OUT + JACKPOTS*/
        , SUM( CASE WHEN TSMH_TYPE <> 20 THEN
                  - CASE WHEN (TSMH_METER_CODE IN (1, 2))						THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               ELSE
                    CASE WHEN (TSMH_METER_CODE IN (1, 2))						THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               END ) AS  TOTAL_WON
        /*TOTAL COIN IN - TOTAL WON*/
        , SUM( CASE WHEN TSMH_TYPE <> 20 THEN
                  - CASE WHEN (TSMH_METER_CODE = 0)								THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
                  + CASE WHEN (TSMH_METER_CODE IN (1, 2))						THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               ELSE
                    CASE WHEN (TSMH_METER_CODE = 0)								THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
                  - CASE WHEN (TSMH_METER_CODE IN (1, 2))						THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               END ) AS TOTAL_COIN_IN_TOTAL_WON
FROM TERMINALS
INNER JOIN TERMINAL_SAS_METERS_HISTORY ON (TE_TERMINAL_ID = TSMH_TERMINAL_ID)
WHERE TSMH_DATETIME >= @_aux_date_init AND TSMH_DATETIME < @_aux_date_final
      AND TSMH_TYPE IN ( 20   /*MINCETUR_DAILY_REPORT*/
                       , 10   /*METER_RESET*/
                       , 11   /*METER_ROLLOVER*/
                       , 15   /*METER_SERVICE_RAM_CLEAR*/
                       , 16   /*METER_MACHINE_RAM_CLEAR*/
                         )
      AND TSMH_METER_CODE IN  ( 0   /*Total IN*/
                              , 1   /*Total OUT*/
                              , 2   /*Jackpots*/
                              , 3   /*Cancelled credits*/
                              , 11  /*Bill IN*/
                              , 128 /*Redimible Ticket IN*/
                              , 130 /*Ticket IN redeem promotional*/
                              , 132 /*Ticket IN no redeem promotional*/
                              , 134 /*Ticket OUT */
                              ) 
GROUP BY  TE_TERMINAL_ID, TE_NAME, TE_SERIAL_NUMBER, TE_MACHINE_SERIAL_NUMBER,
          (CASE WHEN TSMH_GROUP_ID IS NULL THEN DATEPART(MM,TSMH_DATETIME) ELSE DATEPART(MM,TSMH_CREATED_DATETIME) END)
ORDER BY TE_NAME
          
/*Add the TOTAL ROW*/
SELECT TSMH_MONTH,SITE_NAME, TE_NAME, TE_SERIAL_NUMBER, BILL_IN, TICKET_IN_REDIMIBLE, TICKET_OUT_REDIMIBLE, TOTAL_IN, TOTAL_OUT, REDEEM_TICKETS, TICKET_IN_PROMOTIONAL, CANCELLED_CREDITS, JACKPOTS, LGA_NETWIN, TOTAL_COIN_IN, TOTAL_WON, TOTAL_COIN_IN_TOTAL_WON
FROM #TT_MONTHLYNETWIN
UNION ALL
SELECT 'TOTAL' AS TSMH_MONTH, '' AS SITE_NAME, '' AS TE_NAME, '' AS TE_SERIAL_NUMBER, SUM(BILL_IN), SUM(TICKET_IN_REDIMIBLE), SUM(TICKET_OUT_REDIMIBLE), SUM(TOTAL_IN), SUM(TOTAL_OUT), SUM(REDEEM_TICKETS), SUM(TICKET_IN_PROMOTIONAL), SUM(CANCELLED_CREDITS), SUM(JACKPOTS), SUM(LGA_NETWIN), SUM(TOTAL_COIN_IN), SUM(TOTAL_WON), SUM(TOTAL_COIN_IN_TOTAL_WON)
FROM #TT_MONTHLYNETWIN
          
DROP TABLE #TT_MONTHLYNETWIN

END
GO

GRANT EXECUTE ON [dbo].[GetMonthlyNetwin] TO wggui WITH GRANT OPTION
GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Tax_Report_Per_Day]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Tax_Report_Per_Day]
GO

CREATE PROCEDURE [dbo].[Tax_Report_Per_Day]        
                 @pDateFrom DATETIME
               , @pDateTo   DATETIME       
  AS
BEGIN 

  DECLARE @CARD_DEPOSIT_IN                              INT
  DECLARE @CARD_REPLACEMENT                             INT
  DECLARE @CARD_DEPOSIT_IN_CHECK                        INT
  DECLARE @CARD_DEPOSIT_IN_CURRENCY_EXCHANGE            INT
  DECLARE @CARD_DEPOSIT_IN_CARD_CREDIT                  INT
  DECLARE @CARD_DEPOSIT_IN_CARD_DEBIT                   INT
  DECLARE @CARD_DEPOSIT_IN_CARD_GENERIC                 INT
  DECLARE @CARD_REPLACEMENT_CHECK                       INT
  DECLARE @CARD_REPLACEMENT_CARD_CREDIT                 INT
  DECLARE @CARD_REPLACEMENT_CARD_DEBIT                  INT
  DECLARE @CARD_REPLACEMENT_CARD_GENERIC                INT
  DECLARE @COMPANY_B_CARD_DEPOSIT_IN                    INT
  DECLARE @COMPANY_B_CARD_REPLACEMENT                   INT
  DECLARE @COMPANY_B_CARD_DEPOSIT_IN_CHECK              INT
  DECLARE @COMPANY_B_CARD_DEPOSIT_IN_CURRENCY_EXCHANGE  INT
  DECLARE @COMPANY_B_CARD_DEPOSIT_IN_CARD_CREDIT        INT
  DECLARE @COMPANY_B_CARD_DEPOSIT_IN_CARD_DEBIT         INT
  DECLARE @COMPANY_B_CARD_DEPOSIT_IN_CARD_GENERIC       INT
  DECLARE @COMPANY_B_CARD_REPLACEMENT_CHECK             INT
  DECLARE @COMPANY_B_CARD_REPLACEMENT_CARD_CREDIT       INT
  DECLARE @COMPANY_B_CARD_REPLACEMENT_CARD_DEBIT        INT
  DECLARE @COMPANY_B_CARD_REPLACEMENT_CARD_GENERIC      INT

  DECLARE @CASH_IN_SPLIT2                               INT
  DECLARE @MB_CASH_IN_SPLIT2                            INT
  DECLARE @NA_CASH_IN_SPLIT2                            INT
  DECLARE @_DAY_VAR                                     DATETIME 

  SET @CARD_DEPOSIT_IN                                  =   9
  SET @CARD_REPLACEMENT                                 =   28

  SET @CARD_DEPOSIT_IN_CHECK                            =   532
  SET @CARD_DEPOSIT_IN_CURRENCY_EXCHANGE                =   533
  SET @CARD_DEPOSIT_IN_CARD_CREDIT                      =   534
  SET @CARD_DEPOSIT_IN_CARD_DEBIT                       =   535
  SET @CARD_DEPOSIT_IN_CARD_GENERIC                     =   536
  SET @CARD_REPLACEMENT_CHECK                           =   537
  SET @CARD_REPLACEMENT_CARD_CREDIT                     =   538
  SET @CARD_REPLACEMENT_CARD_DEBIT                      =   539
  SET @CARD_REPLACEMENT_CARD_GENERIC                    =   540
  SET @COMPANY_B_CARD_DEPOSIT_IN                        =   541
  SET @COMPANY_B_CARD_REPLACEMENT                       =   543
  SET @COMPANY_B_CARD_DEPOSIT_IN_CHECK                  =   544
  SET @COMPANY_B_CARD_DEPOSIT_IN_CURRENCY_EXCHANGE      =   545
  SET @COMPANY_B_CARD_DEPOSIT_IN_CARD_CREDIT            =   546
  SET @COMPANY_B_CARD_DEPOSIT_IN_CARD_DEBIT             =   547
  SET @COMPANY_B_CARD_DEPOSIT_IN_CARD_GENERIC           =   548
  SET @COMPANY_B_CARD_REPLACEMENT_CHECK                 =   549
  SET @COMPANY_B_CARD_REPLACEMENT_CARD_CREDIT           =   550
  SET @COMPANY_B_CARD_REPLACEMENT_CARD_DEBIT            =   551
  SET @COMPANY_B_CARD_REPLACEMENT_CARD_GENERIC          =   552

  SET @CASH_IN_SPLIT2                                   =   34
  SET @MB_CASH_IN_SPLIT2                                =   35
  SET @NA_CASH_IN_SPLIT2                                =   55

  IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID('#TEMP_TABLE') AND type in (N'U'))
  BEGIN                                
    DROP TABLE #TEMP_TABLE  
  END       

  CREATE  TABLE #TEMP_TABLE ( TODAY   DATETIME PRIMARY KEY ) 
  
  IF @pDateTo IS NULL
  BEGIN
     -- IF DATE TO IS NULL, QUERY UNTIL CURRENT DATE
     SET @pDateTo = CAST(GETDATE() AS DATETIME)
  END

  -- TEMP TABLE IS FILLED WITH THE RANGE OF DATES
  SET @_DAY_VAR = @pDateFrom

  WHILE @_DAY_VAR < @pDateTo 
  BEGIN 
     INSERT INTO   #TEMP_TABLE (Today) VALUES (@_DAY_VAR)
     SET @_DAY_VAR =  DATEADD(Day,1,@_DAY_VAR)
  END 
  
  ;
  -- CASHIER MOVEMENTS FILTERED BY DATE AND MOVEMENT TYPES
  WITH MovementsPerWorkingday  AS 
   (
     SELECT   dbo.Opening(0, CM_DATE) 'WorkingDate'
            , CM_TYPE
            , CM_ADD_AMOUNT
            , CM_AUX_AMOUNT
       FROM   CASHIER_MOVEMENTS_GROUPED_BY_HOUR
      WHERE   (CM_DATE >= @pDateFrom AND (CM_DATE < @pDateTo))
        AND   CM_SUB_TYPE = 0
              AND  CM_TYPE IN (  @MB_CASH_IN_SPLIT2
                               , @CASH_IN_SPLIT2
                               , @NA_CASH_IN_SPLIT2
                               , @CARD_DEPOSIT_IN
                               , @CARD_REPLACEMENT 
                               , @CARD_DEPOSIT_IN_CHECK                       
                               , @CARD_DEPOSIT_IN_CURRENCY_EXCHANGE           
                               , @CARD_DEPOSIT_IN_CARD_CREDIT                 
                               , @CARD_DEPOSIT_IN_CARD_DEBIT                  
                               , @CARD_DEPOSIT_IN_CARD_GENERIC                
                               , @CARD_REPLACEMENT_CHECK                      
                               , @CARD_REPLACEMENT_CARD_CREDIT                
                               , @CARD_REPLACEMENT_CARD_DEBIT                 
                               , @CARD_REPLACEMENT_CARD_GENERIC               
                               , @COMPANY_B_CARD_DEPOSIT_IN                   
                               , @COMPANY_B_CARD_REPLACEMENT                  
                               , @COMPANY_B_CARD_DEPOSIT_IN_CHECK             
                               , @COMPANY_B_CARD_DEPOSIT_IN_CURRENCY_EXCHANGE 
                               , @COMPANY_B_CARD_DEPOSIT_IN_CARD_CREDIT       
                               , @COMPANY_B_CARD_DEPOSIT_IN_CARD_DEBIT        
                               , @COMPANY_B_CARD_DEPOSIT_IN_CARD_GENERIC      
                               , @COMPANY_B_CARD_REPLACEMENT_CHECK            
                               , @COMPANY_B_CARD_REPLACEMENT_CARD_CREDIT      
                               , @COMPANY_B_CARD_REPLACEMENT_CARD_DEBIT       
                               , @COMPANY_B_CARD_REPLACEMENT_CARD_GENERIC     
                            )
  )

  SELECT   TimeRange.Today WorkingDate
         , ISNULL(((GrossCard - TaxCard) + (GrossCompanyB - TaxCompanyB)),0) BaseTotal
         , ISNULL((TaxCard + TaxCompanyB),0)       TaxTotal
         , ISNULL((GrossCard + GrossCompanyB),0)   GrossTotal
         , ISNULL((GrossCard - TaxCard),0)         BaseCard
         , ISNULL(TaxCard,0)                       TaxCard
         , ISNULL(GrossCard,0)                     GrossCard
         , ISNULL((GrossCompanyB - TaxCompanyB),0) BaseCompanyB         
         , ISNULL(TaxCompanyB,0)                   TaxCompanyB
         , ISNULL(GrossCompanyB,0)                 GrossCompanyB
  FROM (
           SELECT  WorkingDate
           ,
                   SUM(ISNULL(CASE WHEN CM_TYPE IN (  @CARD_DEPOSIT_IN
                                                    , @CARD_REPLACEMENT 
                                                    , @CARD_DEPOSIT_IN_CHECK                       
                                                    , @CARD_DEPOSIT_IN_CURRENCY_EXCHANGE           
                                                    , @CARD_DEPOSIT_IN_CARD_CREDIT                 
                                                    , @CARD_DEPOSIT_IN_CARD_DEBIT                  
                                                    , @CARD_DEPOSIT_IN_CARD_GENERIC                
                                                    , @CARD_REPLACEMENT_CHECK                      
                                                    , @CARD_REPLACEMENT_CARD_CREDIT                
                                                    , @CARD_REPLACEMENT_CARD_DEBIT                 
                                                    , @CARD_REPLACEMENT_CARD_GENERIC               
                                                    , @COMPANY_B_CARD_DEPOSIT_IN                   
                                                    , @COMPANY_B_CARD_REPLACEMENT                  
                                                    , @COMPANY_B_CARD_DEPOSIT_IN_CHECK             
                                                    , @COMPANY_B_CARD_DEPOSIT_IN_CURRENCY_EXCHANGE 
                                                    , @COMPANY_B_CARD_DEPOSIT_IN_CARD_CREDIT       
                                                    , @COMPANY_B_CARD_DEPOSIT_IN_CARD_DEBIT        
                                                    , @COMPANY_B_CARD_DEPOSIT_IN_CARD_GENERIC      
                                                    , @COMPANY_B_CARD_REPLACEMENT_CHECK            
                                                    , @COMPANY_B_CARD_REPLACEMENT_CARD_CREDIT      
                                                    , @COMPANY_B_CARD_REPLACEMENT_CARD_DEBIT       
                                                    , @COMPANY_B_CARD_REPLACEMENT_CARD_GENERIC     
                                                   )  THEN CM_ADD_AMOUNT
                        ELSE 0
                        END, 0)) 'GrossCard'
                  , SUM(ISNULL(CASE WHEN CM_TYPE IN (   @CARD_DEPOSIT_IN
                                                      , @CARD_REPLACEMENT 
                                                      , @CARD_DEPOSIT_IN_CHECK                       
                                                      , @CARD_DEPOSIT_IN_CURRENCY_EXCHANGE           
                                                      , @CARD_DEPOSIT_IN_CARD_CREDIT                 
                                                      , @CARD_DEPOSIT_IN_CARD_DEBIT                  
                                                      , @CARD_DEPOSIT_IN_CARD_GENERIC                
                                                      , @CARD_REPLACEMENT_CHECK                      
                                                      , @CARD_REPLACEMENT_CARD_CREDIT                
                                                      , @CARD_REPLACEMENT_CARD_DEBIT                 
                                                      , @CARD_REPLACEMENT_CARD_GENERIC               
                                                      , @COMPANY_B_CARD_DEPOSIT_IN                   
                                                      , @COMPANY_B_CARD_REPLACEMENT                  
                                                      , @COMPANY_B_CARD_DEPOSIT_IN_CHECK             
                                                      , @COMPANY_B_CARD_DEPOSIT_IN_CURRENCY_EXCHANGE 
                                                      , @COMPANY_B_CARD_DEPOSIT_IN_CARD_CREDIT       
                                                      , @COMPANY_B_CARD_DEPOSIT_IN_CARD_DEBIT        
                                                      , @COMPANY_B_CARD_DEPOSIT_IN_CARD_GENERIC      
                                                      , @COMPANY_B_CARD_REPLACEMENT_CHECK            
                                                      , @COMPANY_B_CARD_REPLACEMENT_CARD_CREDIT      
                                                      , @COMPANY_B_CARD_REPLACEMENT_CARD_DEBIT       
                                                      , @COMPANY_B_CARD_REPLACEMENT_CARD_GENERIC  
                                                    )  THEN CM_AUX_AMOUNT
                        ELSE 0
                        END, 0)) 'TaxCard'
                  , SUM(ISNULL(CASE WHEN CM_TYPE IN ( @MB_CASH_IN_SPLIT2,
                                                      @CASH_IN_SPLIT2, 
                                                      @NA_CASH_IN_SPLIT2 ) THEN CM_ADD_AMOUNT
                        ELSE 0
                        END, 0)) 'GrossCompanyB'
                  , SUM(ISNULL(CASE WHEN CM_TYPE IN ( @MB_CASH_IN_SPLIT2,
                                                      @CASH_IN_SPLIT2, 
                                                      @NA_CASH_IN_SPLIT2 ) THEN CM_AUX_AMOUNT
                        ELSE 0
                        END, 0)) 'TaxCompanyB'
           FROM MovementsPerWorkingday 
           GROUP BY WorkingDate
         ) MOVEMENTS RIGHT JOIN #TEMP_TABLE TimeRange on TimeRange.Today = MOVEMENTS.WorkingDate
   ORDER BY TimeRange.Today ASC       
   
   DROP TABLE #TEMP_TABLE
END
GO

GRANT EXECUTE ON [dbo].[Tax_Report_Per_Day] TO [wggui] WITH GRANT OPTION
GO




/******* TRIGGERS *******/



