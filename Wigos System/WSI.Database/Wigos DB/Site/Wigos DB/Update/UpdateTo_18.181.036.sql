/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 180;

SET @New_ReleaseId = 181;
SET @New_ScriptName = N'UpdateTo_18.181.036.sql';
SET @New_Description = N'New GP: CashDesk.Draw and BigIncrements,Update Trigger MultiSite,Update GAMING TABLE stored';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/******* TABLES  *******/

-- TABLE TERMINAL_STATUS
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[terminal_status]') AND type in (N'U'))
  DROP TABLE terminal_status
GO

CREATE TABLE [dbo].[terminal_status](
	[ts_terminal_id] [int] NOT NULL,
	[ts_sas_host_error] [bit] NOT NULL DEFAULT 0,
	[ts_current_payout] [money] NULL,	
	[ts_stacker_counter] [int] NOT NULL DEFAULT 0,	
	[ts_stacker_status] [int] NOT NULL DEFAULT 0,
	[ts_egm_flags] [int] NOT NULL DEFAULT 0,	
	[ts_door_flags] [int] NOT NULL DEFAULT 0,
	[ts_bill_flags] [int] NOT NULL DEFAULT 0,
	[ts_printer_flags] [int] NOT NULL DEFAULT 0,	
	[ts_played_won_flags] [int] NOT NULL DEFAULT 0,	
	[ts_won_amount] [money] NULL,	
	[ts_plays_per_second] decimal(8,4) NULL,
	[ts_cents_per_second] decimal(8,4) NULL,
	[ts_jackpot_flags] [int] NOT NULL DEFAULT 0,
	[ts_jackpot_amount] [money] NULL,
	[ts_call_attendant_flags] [bit] NOT NULL DEFAULT 0,
 CONSTRAINT [PK_terminal_status] PRIMARY KEY CLUSTERED 
(
	[ts_terminal_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[gui_users]') and name = 'gu_employee_code')
  ALTER TABLE gui_users ADD gu_employee_code nvarchar(40) NULL
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[tickets]') and name = 'ti_collected_money_collection')
  ALTER TABLE tickets ADD ti_collected_money_collection BIGINT NULL
GO

/******* INDEXES  *******/

/******* STORED  *******/

IF OBJECT_ID (N'dbo.GT_Session_Information', N'P') IS NOT NULL
    DROP PROCEDURE dbo.GT_Session_Information;                 
GO  

/*
----------------------------------------------------------------------------------------------------------------
BASIC REPORT QUERY FOR GAMING TABLE SESSION INFORMATION

Version  Date           User     Description
----------------------------------------------------------------------------------------------------------------
1.0.0    31-JAN-2014    RMS      First Release

Requeriments:
   -- Functions:
         
Parameters:
   -- BASE TYPE:        To select if report data source is session or cashier movements. 
                          0 - By Session
                          1 - By Cashier Movements
   -- DATE FROM:        Start date for data collection. (If NULL then use first available date).
   -- DATE TO:          End date for data collection. (If NULL then use current date).
   -- STATUS:           The gaming table session status (Opened or Closed) (-1 to show all).
   -- ENABLED:          The gaming table status (Enabled or Disabled) (-1 to show all).
   -- AREA:             Location Area of the gaming table.
   -- BANK:             Area bank of location.
   -- CHIPS ISO CODE:   ISO Code for chips in cage.
   -- CHIPS COINS CODE: Coins code for chips in cage.
   -- VALID TYPES:      A string that contains the valid table types to list.
*/  

CREATE PROCEDURE [dbo].[GT_Session_Information] 
 ( @pBaseType INTEGER
  ,@pDateFrom DATETIME 
  ,@pDateTo DATETIME
  ,@pStatus INTEGER
  ,@pEnabled INTEGER
  ,@pAreaId INTEGER
  ,@pBankId INTEGER
  ,@pChipsISOCode VARCHAR(50)
  ,@pChipsCoinsCode INTEGER
  ,@pValidTypes VARCHAR(4096)
 )
AS
BEGIN

----------------------------------------------------------------------------------------------------------------
DECLARE @_BASE_TYPE        AS   INTEGER
DECLARE @_DATE_FROM        AS   DATETIME
DECLARE @_DATE_TO          AS   DATETIME
DECLARE @_STATUS           AS   INTEGER
DECLARE @_ENABLED          AS   INTEGER
DECLARE @_AREA             AS   INTEGER
DECLARE @_BANK             AS   INTEGER
DECLARE @_CHIPS_ISO_CODE   AS   VARCHAR(50)
DECLARE @_CHIPS_COINS_CODE AS   VARCHAR(50)
DECLARE @_DELIMITER        AS   CHAR(1)
DECLARE @_TYPES_TABLE           TABLE(SST_ID INT, SST_VALUE VARCHAR(50)) -- TO STORE THE VALID GAMING TABLE TYPES 

-- MOVEMENTS
DECLARE @_CHIPS_MOVEMENT_PURCHASE_TOTAL AS   INTEGER
DECLARE @_CHIPS_MOVEMENT_SALES_TOTAL    AS   INTEGER
DECLARE @_MOVEMENT_FILLER_IN            AS   INTEGER
DECLARE @_MOVEMENT_FILLER_OUT           AS   INTEGER
DECLARE @_MOVEMENT_CAGE_FILLER_OUT      AS   INTEGER
DECLARE @_MOVEMENT_CAGE_CLOSE_SESSION   AS   INTEGER
DECLARE @_MOVEMENT_CLOSE_SESSION	    AS   INTEGER

----------------------------------------------------------------------------------------------------------------

-- Initialzitation --
SET @_DELIMITER        =   ','
SET @_BASE_TYPE        =   @pBaseType
SET @_DATE_FROM        =   @pDateFrom
SET @_DATE_TO          =   @pDateTo
SET @_STATUS           =   ISNULL(@pStatus, -1)
SET @_ENABLED          =   ISNULL(@pEnabled, -1)
SET @_AREA             =   ISNULL(@pAreaId, 0)
SET @_BANK             =   ISNULL(@pBankId, 0)
SET @_CHIPS_ISO_CODE   =   ISNULL(@pChipsISOCode, 'X01')
SET @_CHIPS_COINS_CODE =   ISNULL(@pChipsCoinsCode, -100)

SET @_CHIPS_MOVEMENT_PURCHASE_TOTAL =   304
SET @_CHIPS_MOVEMENT_SALES_TOTAL    =   303
SET @_MOVEMENT_FILLER_IN            =   2
SET @_MOVEMENT_FILLER_OUT           =   3
SET @_MOVEMENT_CAGE_FILLER_OUT      =   203
SET @_MOVEMENT_CAGE_CLOSE_SESSION   =   201
SET @_MOVEMENT_CLOSE_SESSION        =   1

----------------------------------------------------------------------------------------------------------------
-- CHECK DATE PARAMETERS
IF @_DATE_FROM IS NULL
BEGIN
   -- IF DATE FROM IS NULL, USER FIRST POSIBLE DATE
   SET @_DATE_FROM = CAST('' AS DATETIME)
END
IF @_DATE_TO IS NULL
BEGIN
   -- IF DATE TO IS NULL, QUERY UNTIL CURRENT DATE
   SET @_DATE_TO = CAST(GETDATE() AS DATETIME)
END

-- ASSIGN TYPES PARAMETER INTO TABLE
INSERT INTO @_TYPES_TABLE SELECT * FROM dbo.SplitStringIntoTable(@pValidTypes, @_DELIMITER, DEFAULT)

----------------------------------------------------------------------------------------------------------------

IF @_BASE_TYPE = 0 
BEGIN
   -- REPORT BY GAMING TABLE SESSION
 SELECT    GTS_CASHIER_SESSION_ID                                                           
         , CS_NAME                                                                          
         , CS_STATUS                                                                        
         , GT_NAME                                                                          
         , GTT_NAME                                                                         
         , GT_HAS_INTEGRATED_CASHIER                                                        
         , CT_NAME                                                                          
         , GU_USERNAME                                                                      
         , CS_OPENING_DATE                                                                  
         , CS_CLOSING_DATE                                                                  
         , GTS_TOTAL_PURCHASE_AMOUNT                                                        
         , GTS_TOTAL_SALES_AMOUNT                                                           
         , GTS_INITIAL_CHIPS_AMOUNT                                                         
         , GTS_FINAL_CHIPS_AMOUNT                                                           
         , GTS_TIPS                                                                         
         , GTS_COLLECTED_AMOUNT                                                             
         , GTS_CLIENT_VISITS                                                                
         , AR_NAME          		                                                            
         , BK_NAME          		                                                            
   FROM         GAMING_TABLES_SESSIONS                                                      
  INNER    JOIN CASHIER_SESSIONS      ON CS_SESSION_ID            = GTS_CASHIER_SESSION_ID  
   LEFT    JOIN GUI_USERS             ON GU_USER_ID               = CS_USER_ID              
   LEFT    JOIN GAMING_TABLES         ON GT_GAMING_TABLE_ID       = GTS_GAMING_TABLE_ID     
   LEFT    JOIN GAMING_TABLES_TYPES   ON GTT_GAMING_TABLE_TYPE_ID = GT_TYPE_ID              
   LEFT    JOIN AREAS                 ON GT_AREA_ID               = AR_AREA_ID              
   LEFT    JOIN BANKS                 ON GT_BANK_ID               = BK_BANK_ID              
   LEFT    JOIN CASHIER_TERMINALS     ON GT_CASHIER_ID            = CT_CASHIER_ID           
 WHERE   (CS_OPENING_DATE >= @_DATE_FROM AND (CS_OPENING_DATE < @_DATE_TO))
   AND   (GT_ENABLED = CASE WHEN @_ENABLED = -1 THEN GT_ENABLED ELSE @_ENABLED END)  
   AND   (CS_STATUS  = CASE WHEN @_STATUS  = -1 THEN CS_STATUS  ELSE @_STATUS  END) 
   AND   (GT_AREA_ID = CASE WHEN @_AREA    = 0  THEN GT_AREA_ID ELSE @_AREA    END) 
   AND   (GT_BANK_ID = CASE WHEN @_BANK    = 0  THEN GT_BANK_ID ELSE @_BANK    END) 
   AND   (GT_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE ))
   ;
   
END
ELSE
BEGIN
   -- REPORT BY CASHIER MOVEMENTS

                      
 SELECT    CM_SESSION_ID                                                                      
         , CS_NAME                                                                            
         , CS_STATUS                                                                          
         , GT_NAME                                                                            
         , GTT_NAME                                                                           
         , GT_HAS_INTEGRATED_CASHIER                                                          
         , CT_NAME                                                                            
         , GU_USERNAME                                                                        
         , CS_OPENING_DATE                                                                    
         , CS_CLOSING_DATE                                                                    
         , SUM(CASE WHEN CM_TYPE                  = @_CHIPS_MOVEMENT_PURCHASE_TOTAL
                    THEN CM_ADD_AMOUNT ELSE 0 END
              )  AS CM_TOTAL_PURCHASE_AMOUNT           
         , SUM(CASE WHEN CM_TYPE                  = @_CHIPS_MOVEMENT_SALES_TOTAL
                    THEN CM_SUB_AMOUNT ELSE 0 END
              )  AS CM_TOTAL_SALES_AMOUNT	            
         , SUM(CASE WHEN CM_CURRENCY_ISO_CODE     = @_CHIPS_ISO_CODE             
                     AND CM_TYPE                  = @_MOVEMENT_FILLER_IN
                    THEN CM_ADD_AMOUNT ELSE 0 END
              )  AS CM_INITIAL_CHIPS_AMOUNT	          
         , SUM(CASE WHEN CM_CURRENCY_ISO_CODE     = @_CHIPS_ISO_CODE             
                     AND CM_TYPE                  = @_MOVEMENT_FILLER_OUT
                    THEN CM_SUB_AMOUNT
					--WHEN CM_CURRENCY_ISO_CODE     = @_CHIPS_ISO_CODE
					 --AND CM_TYPE				  = @_MOVEMENT_CLOSE_SESSION
                    --THEN CM_INITIAL_BALANCE 
					WHEN CM_CURRENCY_ISO_CODE     = @_CHIPS_ISO_CODE
					 AND CM_TYPE				  = @_MOVEMENT_CAGE_CLOSE_SESSION
                    THEN CM_INITIAL_BALANCE
                    ELSE 0 END
              )  AS CM_FINAL_CHIPS_AMOUNT	                          
         , SUM(CASE WHEN CM_TYPE                  = @_MOVEMENT_CAGE_FILLER_OUT
                     AND CM_CURRENCY_ISO_CODE     = @_CHIPS_ISO_CODE             
                     AND CM_CURRENCY_DENOMINATION = @_CHIPS_COINS_CODE
                    THEN CM_SUB_AMOUNT
                    WHEN CM_TYPE                  = @_MOVEMENT_CAGE_CLOSE_SESSION
                     AND CM_CURRENCY_ISO_CODE     = @_CHIPS_ISO_CODE             
                     AND CM_CURRENCY_DENOMINATION = @_CHIPS_COINS_CODE                    
                    THEN CM_INITIAL_BALANCE
                    ELSE 0 END
              )  AS CM_TIPS                       
         , SUM(CASE WHEN CM_CURRENCY_ISO_CODE IS NULL OR CM_CURRENCY_ISO_CODE <> @_CHIPS_ISO_CODE THEN 
                    CASE WHEN CM_TYPE   = @_MOVEMENT_FILLER_IN            THEN -1 * DBO.ApplyExchange(CM_ADD_AMOUNT, CM_CURRENCY_ISO_CODE) 
                         WHEN CM_TYPE   = @_MOVEMENT_FILLER_OUT           THEN      DBO.ApplyExchange(CM_SUB_AMOUNT, CM_CURRENCY_ISO_CODE) 
                         WHEN CM_TYPE   = @_MOVEMENT_CAGE_CLOSE_SESSION   THEN      DBO.ApplyExchange(CM_INITIAL_BALANCE, CM_CURRENCY_ISO_CODE) 
                         ELSE 0 END 
               ELSE 0 END
              )  AS CM_COLLECTED_AMOUNT 
         , GTS_CLIENT_VISITS                                                                  
         , AR_NAME          		                                                              
         , BK_NAME          		                                                              
   FROM        CASHIER_MOVEMENTS                                                             
  INNER   JOIN	CASHIER_SESSIONS        ON CS_SESSION_ID            = CM_SESSION_ID           
  INNER   JOIN	GAMING_TABLES_SESSIONS  ON CS_SESSION_ID            = GTS_CASHIER_SESSION_ID  
   LEFT   JOIN	GUI_USERS               ON GU_USER_ID               = CS_USER_ID              
   LEFT   JOIN	GAMING_TABLES           ON GT_GAMING_TABLE_ID       = GTS_GAMING_TABLE_ID     
   LEFT   JOIN	GAMING_TABLES_TYPES     ON GTT_GAMING_TABLE_TYPE_ID = GT_TYPE_ID              
   LEFT   JOIN	AREAS                   ON GT_AREA_ID               = AR_AREA_ID              
   LEFT   JOIN	BANKS                   ON GT_BANK_ID               = BK_BANK_ID              
   LEFT   JOIN	CASHIER_TERMINALS       ON GT_CASHIER_ID            = CT_CASHIER_ID           
 WHERE   CM_MOVEMENT_ID IN 
            (
               SELECT   CM_MOVEMENT_ID 
                 FROM   CASHIER_MOVEMENTS  
                WHERE   (CM_DATE >= @_DATE_FROM AND CM_DATE <= @_DATE_TO) 
                  AND   CM_SESSION_ID = CS_SESSION_ID 
            ) 
   AND   (GT_ENABLED = CASE WHEN @_ENABLED = -1 THEN GT_ENABLED ELSE @_ENABLED END)  
   AND   (CS_STATUS  = CASE WHEN @_STATUS  = -1 THEN CS_STATUS  ELSE @_STATUS  END)             
   AND   (GT_AREA_ID = CASE WHEN @_AREA    = 0  THEN GT_AREA_ID ELSE @_AREA    END) 
   AND   (GT_BANK_ID = CASE WHEN @_BANK    = 0  THEN GT_BANK_ID ELSE @_BANK    END) 
   AND   (GT_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE ))
                  
  GROUP BY  CM_SESSION_ID         
      , CS_NAME                   
      , CS_STATUS                 
      , GT_NAME                   
      , GTT_NAME                  
      , GT_HAS_INTEGRATED_CASHIER 
      , CT_NAME                   
      , GU_USERNAME               
      , CS_OPENING_DATE           
      , CS_CLOSING_DATE           
      , GTS_CLIENT_VISITS         
      , AR_NAME                   
      , BK_NAME
  
END

END -- END PROCEDURE
GO

-- PERMISSIONS
GRANT EXECUTE ON [dbo].[GT_Session_Information] TO [wggui] WITH GRANT OPTION
GO


IF OBJECT_ID (N'dbo.GT_Chips_Operations', N'P') IS NOT NULL
    DROP PROCEDURE dbo.GT_Chips_Operations;                 
GO  


CREATE PROCEDURE [dbo].[GT_Chips_Operations] 
 ( @pDateFrom DATETIME
  ,@pDateTo DATETIME
  ,@pStatus INTEGER
  ,@pArea VARCHAR(50)
  ,@pBank VARCHAR(50)
  ,@pCashiers INTEGER
  ,@pCashierGroupName VARCHAR(50)
  ,@pValidTypes VARCHAR(4096)
)
AS
BEGIN

----------------------------------------------------------------------------------------------------------------
DECLARE @_DATE_FROM     AS   DATETIME
DECLARE @_DATE_TO       AS   DATETIME
DECLARE @_STATUS        AS   INTEGER
DECLARE @_AREA          AS   VARCHAR(4096)
DECLARE @_BANK          AS   VARCHAR(4096)
DECLARE @_DELIMITER     AS   CHAR(1)
DECLARE @_CASHIERS      AS   INTEGER
DECLARE @_CASHIERS_NAME AS   VARCHAR(50)
DECLARE @_TYPES_TABLE        TABLE(SST_ID INT, SST_VALUE VARCHAR(50)) -- TO STORE THE VALID GAMING TABLE TYPES 

----------------------------------------------------------------------------------------------------------------

-- Initialzitation --
SET @_DELIMITER   =   ','
SET @_DATE_FROM   =   @pDateFrom
SET @_DATE_TO     =   @pDateTo
SET @_STATUS      =   ISNULL(@pStatus, -1)
SET @_AREA        =   ISNULL(@pArea, '')
SET @_BANK        =   ISNULL(@pBank, '')
SET @_CASHIERS    =   ISNULL(@pCashiers, 1)
SET @_CASHIERS_NAME = ISNULL(@pCashierGroupName, '---')

----------------------------------------------------------------------------------------------------------------
-- CHECK DATE PARAMETERS
IF @_DATE_FROM IS NULL
BEGIN
   -- IF DATE FROM IS NULL, USER FIRST POSIBLE DATE
   SET @_DATE_FROM = CAST('' AS DATETIME)
END
IF @_DATE_TO IS NULL
BEGIN
   -- IF DATE TO IS NULL, QUERY UNTIL CURRENT DATE
   SET @_DATE_TO = CAST(GETDATE() AS DATETIME)
END

-- ASSIGN TYPES PARAMETER INTO TABLE
INSERT INTO @_TYPES_TABLE SELECT * FROM dbo.SplitStringIntoTable(@pValidTypes, @_DELIMITER, DEFAULT)

----------------------------------------------------------------------------------------------------------------
-- MAIN QUERY

    SELECT 0 AS TYPE_SESSION
           ,  CS.CS_SESSION_ID AS SESSION_ID
           ,  GT.GT_NAME AS GT_NAME
           , GTT.GTT_NAME AS GTT_NAME
           , SUM(ISNULL(GTS.GTS_TOTAL_SALES_AMOUNT,0))    AS GTS_TOTAL_SALES_AMOUNT
           , SUM(ISNULL(GTS.GTS_TOTAL_PURCHASE_AMOUNT,0)) AS GTS_TOTAL_PURCHASE_AMOUNT
           
      INTO   #CHIPS_OPERATIONS_TABLE           
      FROM   GAMING_TABLES GT
LEFT JOIN   GAMING_TABLES_SESSIONS GTS ON GTS.GTS_GAMING_TABLE_ID = GT.GT_GAMING_TABLE_ID
LEFT JOIN   CASHIER_SESSIONS CS        ON CS.CS_SESSION_ID = GTS.GTS_CASHIER_SESSION_ID
INNER JOIN   GAMING_TABLES_TYPES GTT    ON GTT.GTT_GAMING_TABLE_TYPE_ID = GT.GT_TYPE_ID 
     WHERE   GT.GT_ENABLED  = CASE WHEN @_STATUS = -1 THEN GT.GT_ENABLED ELSE @_STATUS END
       AND   GT.GT_AREA_ID  = CASE WHEN @_AREA   = '' THEN GT.GT_AREA_ID ELSE @_AREA END
       AND   GT.GT_BANK_ID  = CASE WHEN @_BANK   = '' THEN GT.GT_BANK_ID ELSE @_BANK END
       AND   GT.GT_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
       AND   CS.CS_OPENING_DATE >= @_DATE_FROM
       AND   CS.CS_OPENING_DATE <  @_DATE_TO
       AND  GT.GT_HAS_INTEGRATED_CASHIER = 1
  GROUP BY   GT.GT_NAME, GTT.GTT_NAME, CS.CS_SESSION_ID
  ORDER BY   GTT.GTT_NAME
  
-- Check if cashiers must be visible  
IF @_CASHIERS = 1 
BEGIN

-- Select and join data to show cashiers
   -- Adding cashiers without gaming tables

  INSERT INTO #CHIPS_OPERATIONS_TABLE
       SELECT 1 AS TYPE_SESSION
              , CM_SESSION_ID AS SESSION_ID
              , CT_NAME as GT_NAME
              , @_CASHIERS_NAME as GTT_NAME
              , SUM(CM_SUB_AMOUNT) AS GTS_TOTAL_SALES_AMOUNT
              , SUM(CM_ADD_AMOUNT) AS GTS_TOTAL_PURCHASE_AMOUNT
         FROM   CASHIER_MOVEMENTS
   INNER JOIN   CASHIER_SESSIONS       ON CS_SESSION_ID          = CM_SESSION_ID
   INNER JOIN   CASHIER_TERMINALS      ON CM_CASHIER_ID          = CT_CASHIER_ID
    LEFT JOIN   GAMING_TABLES_SESSIONS ON GTS_CASHIER_SESSION_ID = CM_SESSION_ID
        WHERE   GTS_CASHIER_SESSION_ID IS NULL -- We want the chips operations that are from cashiers that are not gambling tables
          AND   CM_TYPE IN (303, 304)  -- 303 = CHIPS_SALE_TOTAL; 304 = CHIPS_PURCHASE_TOTAL
          AND   CS_OPENING_DATE >= @_DATE_FROM
          AND   CS_OPENING_DATE <  @_DATE_TO
     GROUP BY   CT_NAME, CM_SESSION_ID

END
-- Select results
SELECT * FROM #CHIPS_OPERATIONS_TABLE ORDER BY GT_NAME
  
-- DROP TEMPORARY TABLE  
DROP TABLE #CHIPS_OPERATIONS_TABLE
  
END  -- END PROCEDURE
GO

-- PERMISSIONS

GRANT EXECUTE ON [dbo].[GT_Chips_Operations] TO [wggui] WITH GRANT OPTION
GO



--------------------------------------------------------------------------------
-- Copyright � 2013 Win Systems International
--------------------------------------------------------------------------------
--
--   MODULE NAME: update_MasterUsers.sql
--
--   DESCRIPTION: Update Corporate Users in the Site
--
--        AUTHOR: Jos� Mart�nez
--
-- CREATION DATE: 15-JUL-2013
--
-- REVISION HISTORY:
--
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 15-JUL-2013 JML    First release.  
-------------------------------------------------------------------------------- 

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[update_MasterUsers]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[update_MasterUsers]
GO 

CREATE PROCEDURE   [dbo].[update_MasterUsers]
                   @ProfileMasterId       BIGINT
                 , @UserName              NVARCHAR(50)
                 , @UserEnabled           BIT
                 , @UserPassword          BINARY(40)
                 , @UserNotValidBefore    DATETIME
                 , @UserNotValidAfter     DATETIME
                 , @UserLastChanged       DATETIME
                 , @UserPasswordExp       DATETIME
                 , @UserPwdChgReq         BIT
                 , @UserLoginFailures     INT
                 , @UserFullName          NVARCHAR(50)
                 , @UserTimestamp         BIGINT
                 , @UserType              SMALLINT
                 , @UserSalesLimit        MONEY
                 , @UserMbSalesLimit      MONEY
                 , @UserBlockReason       INT
                 , @UserMasterId          INT
                 , @UserEmployeeCode      NVARCHAR(40)
                 
AS
BEGIN

  DECLARE @UserId             AS BIGINT
  DECLARE @ProfileId          AS BIGINT
  

  SET @ProfileId = NULL
  SET @ProfileId = ( SELECT GUP_PROFILE_ID FROM GUI_USER_PROFILES WHERE GUP_MASTER_ID = @ProfileMasterId )
 
  SET @UserId = NULL
  SET @UserId = ( SELECT GU_USER_ID FROM GUI_USERS WHERE GU_USERNAME = @UserName )

  IF @UserId IS NULL AND @ProfileId IS NOT NULL
  BEGIN
    -- is new User
    SET @UserId = ISNULL((SELECT MAX(GU_USER_ID) FROM GUI_USERS), 0) + 1
    
    INSERT INTO GUI_USERS (GU_USER_ID, GU_PROFILE_ID, GU_USERNAME, GU_ENABLED,   GU_PASSWORD,   GU_NOT_VALID_BEFORE, GU_PWD_CHG_REQ, GU_USER_TYPE,  GU_BLOCK_REASON) 
                   VALUES (@UserId,    @ProfileId,    @UserName,   @UserEnabled, @UserPassword, @UserNotValidBefore, @UserPwdChgReq, @UserType, @UserBlockReason)
  END

  IF @UserId IS NOT NULL AND @ProfileId IS NOT NULL
  BEGIN
    UPDATE   GUI_USERS                    
       SET   GU_PROFILE_ID          = @ProfileId  
           , GU_USERNAME            = @UserName            
           , GU_ENABLED             = @UserEnabled         
           , GU_PASSWORD            = @UserPassword        
           , GU_NOT_VALID_BEFORE    = @UserNotValidBefore  
           , GU_NOT_VALID_AFTER     = @UserNotValidAfter   
           , GU_LAST_CHANGED        = @UserLastChanged     
           , GU_PASSWORD_EXP        = @UserPasswordExp     
           , GU_PWD_CHG_REQ         = @UserPwdChgReq
           , GU_LOGIN_FAILURES      = @UserLoginFailures
           , GU_FULL_NAME           = @UserFullName 
           , GU_USER_TYPE           = @UserType        
           , GU_SALES_LIMIT         = @UserSalesLimit      
           , GU_MB_SALES_LIMIT      = @UserMbSalesLimit    
           , GU_BLOCK_REASON        = @UserBlockReason     
           , GU_MASTER_ID           = @UserMasterId        
           , GU_MASTER_SEQUENCE_ID  = @UserTimestamp
           , GU_EMPLOYEE_CODE       = @UserEmployeeCode       
     WHERE   GU_USER_ID   = @UserId
  END
END
GO 

/******* TRIGGERS *******/

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[MultiSiteTrigger_SiteAccountUpdate]') AND type in (N'TR'))
  DROP TRIGGER [dbo].[MultiSiteTrigger_SiteAccountUpdate]
GO

CREATE TRIGGER [dbo].[MultiSiteTrigger_SiteAccountUpdate] ON [dbo].[accounts]
   AFTER UPDATE
NOT FOR REPLICATION
AS 
BEGIN
    DECLARE @AccountId          AS BIGINT
    DECLARE @hash0              AS varbinary(20)
    DECLARE @hash1              AS varbinary(20)
    DECLARE @value              AS nvarchar(max)
    DECLARE @changed            AS bit
    DECLARE @updated            AS BIT    
-- For Last Activity 
    DECLARE @LastActyvity0          AS DATETIME
    DECLARE @ReBalance0             AS MONEY
    DECLARE @PromoREBalance0        AS MONEY
    DECLARE @PromoNRBalance0        AS MONEY
    DECLARE @SessionReBalance0      AS MONEY
    DECLARE @SessionPromoREBalance0 AS MONEY
    DECLARE @SessionPromoNRBalance0 AS MONEY
    DECLARE @LastActyvity1          AS DATETIME
    DECLARE @ReBalance1             AS MONEY
    DECLARE @PromoREBalance1        AS MONEY
    DECLARE @PromoNRBalance1        AS MONEY
    DECLARE @SessionReBalance1      AS MONEY
    DECLARE @SessionPromoREBalance1 AS MONEY
    DECLARE @SessionPromoNRBalance1 AS MONEY
    
    IF (UPDATE(AC_MS_CHANGE_GUID))  
         return

  IF NOT EXISTS (SELECT   1 
                   FROM   GENERAL_PARAMS 
                  WHERE   GP_GROUP_KEY   = N'Site' 
                    AND   GP_SUBJECT_KEY = N'MultiSiteMember' 
                    AND   GP_KEY_VALUE   = N'1')
  BEGIN
        RETURN
  END

  SET @updated = 0;  

  IF   UPDATE (AC_TRACK_DATA)
    OR UPDATE (AC_HOLDER_NAME)
    OR UPDATE (AC_HOLDER_ID)
    OR UPDATE (AC_HOLDER_ID_TYPE)
    OR UPDATE (AC_HOLDER_ADDRESS_01)
    OR UPDATE (AC_HOLDER_ADDRESS_02)
    OR UPDATE (AC_HOLDER_ADDRESS_03)
    OR UPDATE (AC_HOLDER_CITY)
    OR UPDATE (AC_HOLDER_ZIP)
    OR UPDATE (AC_HOLDER_EMAIL_01)
    OR UPDATE (AC_HOLDER_EMAIL_02)
    OR UPDATE (AC_HOLDER_TWITTER_ACCOUNT)
    OR UPDATE (AC_HOLDER_PHONE_NUMBER_01)
    OR UPDATE (AC_HOLDER_PHONE_NUMBER_02)
    OR UPDATE (AC_HOLDER_COMMENTS)
    OR UPDATE (AC_HOLDER_ID1)
    OR UPDATE (AC_HOLDER_ID2)
    OR UPDATE (AC_HOLDER_DOCUMENT_ID1)
    OR UPDATE (AC_HOLDER_DOCUMENT_ID2)
    OR UPDATE (AC_HOLDER_NAME1)
    OR UPDATE (AC_HOLDER_NAME2)
    OR UPDATE (AC_HOLDER_NAME3)
    OR UPDATE (AC_HOLDER_GENDER)
    OR UPDATE (AC_HOLDER_MARITAL_STATUS)
    OR UPDATE (AC_HOLDER_BIRTH_DATE)
    OR UPDATE (AC_HOLDER_WEDDING_DATE)
    OR UPDATE (AC_HOLDER_LEVEL)
    OR UPDATE (AC_HOLDER_LEVEL_NOTIFY)
    OR UPDATE (AC_HOLDER_LEVEL_ENTERED)
    OR UPDATE (AC_HOLDER_LEVEL_EXPIRATION)
    OR UPDATE (AC_PIN)
    OR UPDATE (AC_PIN_FAILURES)
    OR UPDATE (AC_PIN_LAST_MODIFIED)
    OR UPDATE (AC_BLOCKED)
    OR UPDATE (AC_ACTIVATED)
    OR UPDATE (AC_BLOCK_REASON)
    OR UPDATE (AC_HOLDER_IS_VIP)
    OR UPDATE (AC_HOLDER_TITLE)
    OR UPDATE (AC_HOLDER_NAME4)
    OR UPDATE (AC_HOLDER_PHONE_TYPE_01)
    OR UPDATE (AC_HOLDER_PHONE_TYPE_02)
    OR UPDATE (AC_HOLDER_STATE)
    OR UPDATE (AC_HOLDER_COUNTRY)
    OR UPDATE (AC_USER_TYPE)
    OR UPDATE (AC_POINTS_STATUS)
    OR UPDATE (AC_DEPOSIT)
    OR UPDATE (AC_CARD_PAID)
    OR UPDATE (AC_BLOCK_DESCRIPTION)    
    OR UPDATE (AC_MS_CREATED_ON_SITE_ID)
    OR UPDATE (AC_EXTERNAL_REFERENCE)
    OR UPDATE (AC_HOLDER_OCCUPATION)     
    OR UPDATE (AC_HOLDER_EXT_NUM)       
    OR UPDATE (AC_HOLDER_NATIONALITY)
    OR UPDATE (AC_HOLDER_BIRTH_COUNTRY)
    OR UPDATE (AC_HOLDER_FED_ENTITY)
    OR UPDATE (AC_HOLDER_ID1_TYPE)
    OR UPDATE (AC_HOLDER_ID2_TYPE)
    OR UPDATE (AC_HOLDER_ID3_TYPE)
    OR UPDATE (AC_HOLDER_ID3)
    OR UPDATE (AC_HOLDER_HAS_BENEFICIARY)
    OR UPDATE (AC_BENEFICIARY_NAME)
    OR UPDATE (AC_BENEFICIARY_NAME1)
    OR UPDATE (AC_BENEFICIARY_NAME2)
    OR UPDATE (AC_BENEFICIARY_NAME3)
    OR UPDATE (AC_BENEFICIARY_BIRTH_DATE)
    OR UPDATE (AC_BENEFICIARY_GENDER)
    OR UPDATE (AC_BENEFICIARY_OCCUPATION)
    OR UPDATE (AC_BENEFICIARY_ID1_TYPE)
    OR UPDATE (AC_BENEFICIARY_ID1)
    OR UPDATE (AC_BENEFICIARY_ID2_TYPE)
    OR UPDATE (AC_BENEFICIARY_ID2)
    OR UPDATE (AC_BENEFICIARY_ID3_TYPE)
    OR UPDATE (AC_BENEFICIARY_ID3)
    OR UPDATE (AC_HOLDER_OCCUPATION_ID)
    OR UPDATE (AC_BENEFICIARY_OCCUPATION_ID)
        SET @updated = 1;      
  -- For Last Activity  
  IF   UPDATE (AC_LAST_ACTIVITY)
    OR UPDATE (AC_RE_BALANCE)
    OR UPDATE (AC_PROMO_RE_BALANCE)
    OR UPDATE (AC_PROMO_NR_BALANCE)
    OR UPDATE (AC_IN_SESSION_RE_BALANCE)
    OR UPDATE (AC_IN_SESSION_PROMO_RE_BALANCE)
    OR UPDATE (AC_IN_SESSION_PROMO_NR_BALANCE)
        SET @updated = 2;
        
  IF (@updated = 0) RETURN

  DECLARE PersonalInfoCursor CURSOR FOR 
   SELECT   AC_ACCOUNT_ID
          , HASHBYTES ('SHA1',  ISNULL(AC_TRACK_DATA,        '')
                              + ISNULL(AC_HOLDER_NAME,       '')
                              + ISNULL(AC_HOLDER_ID,         '')
                              + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_ID_TYPE), '')
                              + ISNULL(AC_HOLDER_ADDRESS_01, '')
                              + ISNULL(AC_HOLDER_ADDRESS_02, '')
                              + ISNULL(AC_HOLDER_ADDRESS_03, '')
                              + ISNULL(AC_HOLDER_CITY,       '')
                              + ISNULL(AC_HOLDER_ZIP,        '')
                              + ISNULL(AC_HOLDER_EMAIL_01,   '')
                              + ISNULL(AC_HOLDER_EMAIL_02,   '')
                              + ISNULL(AC_HOLDER_TWITTER_ACCOUNT, '')
                              + ISNULL(AC_HOLDER_PHONE_NUMBER_01, '')
                              + ISNULL(AC_HOLDER_PHONE_NUMBER_02, '')
                              + ISNULL(AC_HOLDER_COMMENTS,        '')
                              + ISNULL(AC_HOLDER_ID1, '')
                              + ISNULL(AC_HOLDER_ID2, '')
                              + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_DOCUMENT_ID1), '')
                              + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_DOCUMENT_ID2), '')
                              + ISNULL(AC_HOLDER_NAME1, '')
                              + ISNULL(AC_HOLDER_NAME2, '')
                              + ISNULL(AC_HOLDER_NAME3, '')
                              + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_GENDER),         '')
                              + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_MARITAL_STATUS), '')
                              + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_BIRTH_DATE,   21), '')
                              + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_WEDDING_DATE, 21), '')
                              + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_LEVEL),                '')
                              + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_LEVEL_NOTIFY),         '')
                              + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_LEVEL_ENTERED,    21), '')
                              + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_LEVEL_EXPIRATION, 21), '')
                              + ISNULL(CONVERT(NVARCHAR, AC_PIN),          '')
                              + ISNULL(CONVERT(NVARCHAR, AC_PIN_FAILURES), '')
                              + ISNULL(CONVERT(NVARCHAR, AC_PIN_LAST_MODIFIED, 21), '')
                              + ISNULL(CONVERT(NVARCHAR, AC_BLOCKED),           '')
                              + ISNULL(CONVERT(NVARCHAR, AC_ACTIVATED),         '')
                              + ISNULL(CONVERT(NVARCHAR, AC_BLOCK_REASON),         '')
                              + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_IS_VIP),        '')
                              + ISNULL(AC_HOLDER_TITLE                        , '')
                              + ISNULL(AC_HOLDER_NAME4                        , '')
                              + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_PHONE_TYPE_01),        '')
                              + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_PHONE_TYPE_02),        '')
                              + ISNULL(AC_HOLDER_STATE                        , '')
                              + ISNULL(AC_HOLDER_COUNTRY                      , '') 
                              + ISNULL(CONVERT(NVARCHAR,AC_USER_TYPE)         , '')
                              + ISNULL(CONVERT(NVARCHAR,AC_POINTS_STATUS)     , '') 
                              + ISNULL(CONVERT(NVARCHAR,AC_DEPOSIT)           , '')
                              + ISNULL(CONVERT(NVARCHAR,AC_CARD_PAID)         , '')
                              + ISNULL(AC_BLOCK_DESCRIPTION, '')
                              + ISNULL(CONVERT(NVARCHAR,AC_MS_CREATED_ON_SITE_ID), '')
                              + ISNULL(AC_EXTERNAL_REFERENCE, '')
                              + ISNULL(AC_HOLDER_OCCUPATION, '')
                              + ISNULL(AC_HOLDER_EXT_NUM, '')
                              + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_NATIONALITY),         '')
                              + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_BIRTH_COUNTRY), '')
                              + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_ID1_TYPE), '')
                              + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_ID2_TYPE), '')
                              + ISNULL(AC_HOLDER_ID3_TYPE, '')
                              + ISNULL(AC_HOLDER_ID3, '')
                              + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_HAS_BENEFICIARY), '')
                              + ISNULL(AC_BENEFICIARY_NAME, '')
                              + ISNULL(AC_BENEFICIARY_NAME1, '')
                              + ISNULL(AC_BENEFICIARY_NAME2, '')
                              + ISNULL(AC_BENEFICIARY_NAME3, '')
                              + ISNULL(CONVERT(NVARCHAR, AC_BENEFICIARY_BIRTH_DATE,   21), '')
                              + ISNULL(CONVERT(NVARCHAR, AC_BENEFICIARY_GENDER), '')  
                              + ISNULL(AC_BENEFICIARY_OCCUPATION, '')
                              + ISNULL(CONVERT(NVARCHAR, AC_BENEFICIARY_ID1_TYPE), '')        
                              + ISNULL(AC_BENEFICIARY_ID1, '')
                              + ISNULL(CONVERT(NVARCHAR, AC_BENEFICIARY_ID2_TYPE), '')        
                              + ISNULL(AC_BENEFICIARY_ID2, '')
                              + ISNULL(AC_BENEFICIARY_ID3_TYPE, '')        
                              + ISNULL(AC_BENEFICIARY_ID3, '') 
                              + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_OCCUPATION_ID), '')
                              + ISNULL(CONVERT(NVARCHAR, AC_BENEFICIARY_OCCUPATION_ID), '')) 
          , ISNULL(AC_LAST_ACTIVITY, GETDATE())
          , ISNULL(AC_RE_BALANCE, 0)
          , ISNULL(AC_PROMO_RE_BALANCE, 0)
          , ISNULL(AC_PROMO_NR_BALANCE, 0)
          , ISNULL(AC_IN_SESSION_RE_BALANCE, 0)
          , ISNULL(AC_IN_SESSION_PROMO_RE_BALANCE, 0)
          , ISNULL(AC_IN_SESSION_PROMO_NR_BALANCE, 0)
     FROM   INSERTED
    WHERE   AC_TRACK_DATA not like '%-NEW-%'
      AND   AC_TYPE NOT IN (4, 5) -- Virtual accounts don't have to be sent to the Center
      
  SET NOCOUNT ON;

  OPEN PersonalInfoCursor

  FETCH NEXT FROM PersonalInfoCursor INTO   @AccountId
                                          , @hash1
                                          , @LastActyvity1
                                          , @ReBalance1
                                          , @PromoREBalance1
                                          , @PromoNRBalance1
                                          , @SessionReBalance1
                                          , @SessionPromoREBalance1
                                          , @SessionPromoNRBalance1

  WHILE @@FETCH_STATUS = 0
  BEGIN

    SELECT   @hash0                  = AC_MS_HASH          
           , @LastActyvity0          = AC_LAST_ACTIVITY
           , @ReBalance0             = AC_RE_BALANCE
           , @PromoREBalance0        = AC_PROMO_RE_BALANCE
           , @PromoNRBalance0        = AC_PROMO_NR_BALANCE
           , @SessionReBalance0      = AC_IN_SESSION_RE_BALANCE
           , @SessionPromoREBalance0 = AC_IN_SESSION_PROMO_RE_BALANCE
           , @SessionPromoNRBalance0 = AC_IN_SESSION_PROMO_NR_BALANCE
      FROM   DELETED 
     WHERE   AC_ACCOUNT_ID = @AccountId
		 
    SELECT   @changed = CASE WHEN ( @hash0 = @hash1 ) THEN 0 ELSE 1 END

    IF @changed = 1
    BEGIN
      DECLARE @new_id as uniqueidentifier
           
      SET @new_id = NEWID()
           
      -- Personal Info
      UPDATE   ACCOUNTS
         SET   AC_MS_HASH         = @hash1
             , AC_MS_CHANGE_GUID  = @new_id
       WHERE   AC_ACCOUNT_ID      = @AccountId
            
      IF NOT EXISTS (SELECT 1 FROM MS_SITE_PENDING_ACCOUNTS WHERE SPA_ACCOUNT_ID = @AccountId)
        INSERT INTO   MS_SITE_PENDING_ACCOUNTS (SPA_ACCOUNT_ID, SPA_GUID) VALUES (@AccountId, @new_id )
      ELSE 
        UPDATE MS_SITE_PENDING_ACCOUNTS SET SPA_GUID = @new_id WHERE SPA_ACCOUNT_ID = @AccountId

    END

    SELECT @changed = CASE WHEN (    @LastActyvity0          = @LastActyvity1          
                                 AND @ReBalance0             = @ReBalance1             
                                 AND @PromoREBalance0        = @PromoREBalance1        
                                 AND @PromoNRBalance0        = @PromoNRBalance1        
                                 AND @SessionReBalance0      = @SessionReBalance1      
                                 AND @SessionPromoREBalance0 = @SessionPromoREBalance1 
                                 AND @SessionPromoNRBalance0 = @SessionPromoNRBalance1 ) THEN 0
                           ELSE 1 END

    IF (@changed = 1)
    BEGIN

      IF NOT EXISTS (SELECT 1 FROM MS_SITE_PENDING_LAST_ACTIVITY WHERE LAA_ACCOUNT_ID = @AccountId)
        INSERT INTO  MS_SITE_PENDING_LAST_ACTIVITY (LAA_ACCOUNT_ID) VALUES (@AccountId )
      ELSE 
        UPDATE MS_SITE_PENDING_LAST_ACTIVITY SET LAA_ACCOUNT_ID = @AccountId WHERE LAA_ACCOUNT_ID = @AccountId

    END

    FETCH NEXT FROM PersonalInfoCursor INTO   @AccountId
                                            , @hash1
                                            , @LastActyvity1
                                            , @ReBalance1
                                            , @PromoREBalance1
                                            , @PromoNRBalance1
                                            , @SessionReBalance1
                                            , @SessionPromoREBalance1
                                            , @SessionPromoNRBalance1

  END

  CLOSE PersonalInfoCursor
  DEALLOCATE PersonalInfoCursor

END
GO

--
-- Disable/enable triggers for site members
--

IF EXISTS (SELECT   1
             FROM   GENERAL_PARAMS 
            WHERE   GP_GROUP_KEY   = N'Site' 
              AND   GP_SUBJECT_KEY = N'MultiSiteMember' 
              AND   GP_KEY_VALUE   = N'1')
BEGIN
      EXEC MultiSiteTriggersEnable 1
END
ELSE
BEGIN
      EXEC MultiSiteTriggersEnable 0
END
GO

/******* RECORDS *******/
UPDATE GUI_USERS SET GU_USERNAME  = 'SYS-Validator' , GU_FULL_NAME = 'VALID. BILLETES' WHERE   GU_USER_TYPE = 1
GO

UPDATE MOBILE_BANKS SET MB_HOLDER_NAME = 'SYS-Validator' WHERE MB_ACCOUNT_TYPE = 2
GO

UPDATE TICKETS SET TI_COLLECTED_MONEY_COLLECTION = TI_MONEY_COLLECTION_ID WHERE TI_COLLECTED = 1
GO

IF EXISTS (SELECT 1 FROM general_params WHERE gp_group_key = 'SasMeter.BigIncrement' AND gp_subject_key = 'Cents.BillIn')
	DELETE FROM GENERAL_PARAMS WHERE gp_group_key = 'SasMeter.BigIncrement' AND gp_subject_key = 'Cents.BillIn'
	
INSERT INTO general_params (gp_group_key, gp_subject_key, gp_key_value) VALUES ('SasMeter.BigIncrement','Cents.BillIn','500000000')
GO

IF EXISTS (SELECT 1 FROM general_params WHERE gp_group_key = 'SasMeter.BigIncrement' AND gp_subject_key = 'Cents.FTFromEgm')
  DELETE FROM GENERAL_PARAMS WHERE gp_group_key = 'SasMeter.BigIncrement' AND gp_subject_key = 'Cents.FTFromEgm'

INSERT INTO general_params (gp_group_key, gp_subject_key, gp_key_value) VALUES ('SasMeter.BigIncrement','Cents.FTFromEgm','500000000')
GO

IF EXISTS (SELECT 1 FROM general_params WHERE gp_group_key = 'SasMeter.BigIncrement' AND gp_subject_key = 'Cents.FTToEgm')
  DELETE FROM GENERAL_PARAMS WHERE gp_group_key = 'SasMeter.BigIncrement' AND gp_subject_key = 'Cents.FTToEgm'

INSERT INTO general_params (gp_group_key, gp_subject_key, gp_key_value) VALUES ('SasMeter.BigIncrement','Cents.FTToEgm','500000000')
GO

IF EXISTS (SELECT 1 FROM general_params WHERE gp_group_key = 'SasMeter.BigIncrement' AND gp_subject_key = 'Cents.HandPays')
  DELETE FROM GENERAL_PARAMS WHERE gp_group_key = 'SasMeter.BigIncrement' AND gp_subject_key = 'Cents.HandPays'

INSERT INTO general_params (gp_group_key, gp_subject_key, gp_key_value) VALUES ('SasMeter.BigIncrement','Cents.HandPays','500000000')
GO

IF EXISTS (SELECT 1 FROM general_params WHERE gp_group_key = 'SasMeter.BigIncrement' AND gp_subject_key = 'Cents.Jackpot')
  DELETE FROM GENERAL_PARAMS WHERE gp_group_key = 'SasMeter.BigIncrement' AND gp_subject_key = 'Cents.Jackpot' 
  
INSERT INTO general_params (gp_group_key, gp_subject_key, gp_key_value) VALUES ('SasMeter.BigIncrement','Cents.Jackpot','500000000')
GO

IF EXISTS (SELECT 1 FROM general_params WHERE gp_group_key = 'SasMeter.BigIncrement' AND gp_subject_key = 'Cents.Played')
  DELETE FROM GENERAL_PARAMS WHERE gp_group_key = 'SasMeter.BigIncrement' AND gp_subject_key = 'Cents.Played'

INSERT INTO general_params (gp_group_key, gp_subject_key, gp_key_value) VALUES ('SasMeter.BigIncrement','Cents.Played','10000000')
GO

IF EXISTS (SELECT 1 FROM general_params WHERE gp_group_key = 'SasMeter.BigIncrement' AND gp_subject_key = 'Cents.TicketIn')
  DELETE FROM GENERAL_PARAMS WHERE gp_group_key = 'SasMeter.BigIncrement' AND gp_subject_key = 'Cents.TicketIn'

INSERT INTO general_params (gp_group_key, gp_subject_key, gp_key_value) VALUES ('SasMeter.BigIncrement','Cents.TicketIn','100000000')
GO

IF EXISTS (SELECT 1 FROM general_params WHERE gp_group_key = 'SasMeter.BigIncrement' AND gp_subject_key = 'Cents.TicketOut')
  DELETE FROM GENERAL_PARAMS WHERE gp_group_key = 'SasMeter.BigIncrement' AND gp_subject_key = 'Cents.TicketOut'

INSERT INTO general_params (gp_group_key, gp_subject_key, gp_key_value) VALUES ('SasMeter.BigIncrement','Cents.TicketOut','100000000')
GO

IF EXISTS (SELECT 1 FROM general_params WHERE gp_group_key = 'SasMeter.BigIncrement' AND gp_subject_key = 'Cents.Won')
  DELETE FROM GENERAL_PARAMS WHERE gp_group_key = 'SasMeter.BigIncrement' AND gp_subject_key = 'Cents.Won'
  
INSERT INTO general_params (gp_group_key, gp_subject_key, gp_key_value) VALUES ('SasMeter.BigIncrement','Cents.Won','500000000')
GO

IF EXISTS (SELECT 1 FROM general_params WHERE gp_group_key = 'SasMeter.BigIncrement' AND gp_subject_key = 'Quantity.BillIn')
  DELETE FROM GENERAL_PARAMS WHERE gp_group_key = 'SasMeter.BigIncrement' AND gp_subject_key = 'Quantity.BillIn'

INSERT INTO general_params (gp_group_key, gp_subject_key, gp_key_value) VALUES ('SasMeter.BigIncrement','Quantity.BillIn','50')
GO

IF EXISTS (SELECT 1 FROM general_params WHERE gp_group_key = 'SasMeter.BigIncrement' AND gp_subject_key = 'Quantity.FTFromEgm')
  DELETE FROM GENERAL_PARAMS WHERE gp_group_key = 'SasMeter.BigIncrement' AND gp_subject_key = 'Quantity.FTFromEgm'

INSERT INTO general_params (gp_group_key, gp_subject_key, gp_key_value) VALUES ('SasMeter.BigIncrement','Quantity.FTFromEgm','50')
GO

IF EXISTS (SELECT 1 FROM general_params WHERE gp_group_key = 'SasMeter.BigIncrement' AND gp_subject_key = 'Quantity.FTToEgm')
  DELETE FROM GENERAL_PARAMS WHERE gp_group_key = 'SasMeter.BigIncrement' AND gp_subject_key = 'Quantity.FTToEgm'

INSERT INTO general_params (gp_group_key, gp_subject_key, gp_key_value) VALUES ('SasMeter.BigIncrement','Quantity.FTToEgm','50')
GO

IF EXISTS (SELECT 1 FROM general_params WHERE gp_group_key = 'SasMeter.BigIncrement' AND gp_subject_key = 'Quantity.GamesPlayed')
  DELETE FROM GENERAL_PARAMS WHERE gp_group_key = 'SasMeter.BigIncrement' AND gp_subject_key = 'Quantity.GamesPlayed'

INSERT INTO general_params (gp_group_key, gp_subject_key, gp_key_value) VALUES ('SasMeter.BigIncrement','Quantity.GamesPlayed','5000')
GO

IF EXISTS (SELECT 1 FROM general_params WHERE gp_group_key = 'SasMeter.BigIncrement' AND gp_subject_key = 'Quantity.GamesWon')
  DELETE FROM GENERAL_PARAMS WHERE gp_group_key = 'SasMeter.BigIncrement' AND gp_subject_key = 'Quantity.GamesWon'

INSERT INTO general_params (gp_group_key, gp_subject_key, gp_key_value) VALUES ('SasMeter.BigIncrement','Quantity.GamesWon','5000')
GO

IF EXISTS (SELECT 1 FROM general_params WHERE gp_group_key = 'SasMeter.BigIncrement' AND gp_subject_key = 'Quantity.TicketIn')
  DELETE FROM GENERAL_PARAMS WHERE gp_group_key = 'SasMeter.BigIncrement' AND gp_subject_key = 'Quantity.TicketIn'

INSERT INTO general_params (gp_group_key, gp_subject_key, gp_key_value) VALUES ('SasMeter.BigIncrement','Quantity.TicketIn','50')
GO

IF EXISTS (SELECT 1 FROM general_params WHERE gp_group_key = 'SasMeter.BigIncrement' AND gp_subject_key = 'Quantity.TicketOut')
  DELETE FROM GENERAL_PARAMS WHERE gp_group_key = 'SasMeter.BigIncrement' AND gp_subject_key = 'Quantity.TicketOut'
  
INSERT INTO general_params (gp_group_key, gp_subject_key, gp_key_value) VALUES ('SasMeter.BigIncrement','Quantity.TicketOut','50')
GO

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CashDesk.Draw' AND GP_SUBJECT_KEY ='Voucher.LoserPrizeLabel')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key] ,[gp_subject_key],[gp_key_value])
       VALUES
             ('CashDesk.Draw' ,'Voucher.LoserPrizeLabel' ,'Cortes�a');
GO

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CashDesk.Draw' AND GP_SUBJECT_KEY ='Voucher.WinnerPrizeLabel')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key] ,[gp_subject_key],[gp_key_value])
       VALUES
             ('CashDesk.Draw' ,'Voucher.WinnerPrizeLabel' ,'Premio');
GO

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CashDesk.Draw' AND GP_SUBJECT_KEY ='Voucher.HideCurrencySymbol')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key] ,[gp_subject_key],[gp_key_value])
       VALUES
             ('CashDesk.Draw' ,'Voucher.HideCurrencySymbol' , '0');
GO

