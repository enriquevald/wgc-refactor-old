/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 346;

SET @New_ReleaseId = 347;
SET @New_ScriptName = N'UpdateTo_18.347.041.sql';
SET @New_Description = N'Malta Regionalization'; 

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/**** GENERAL PARAM *****/

/******* TABLES  *******/

/******* INDEXES *******/

/******* RECORDS *******/
-- MALTA OCCUPATIONS
IF (NOT EXISTS (SELECT * FROM occupations WHERE oc_country_iso_code2 = 'MT' ))
BEGIN
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('NOT AVAILABLE','0000001',1,1000,'MT')
END
GO

-- MALTA IDENTIFICATION_TYPES
IF (NOT EXISTS (SELECT * FROM identification_types WHERE idt_country_iso_code2 = 'MT' ))
BEGIN
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (180,1,100,'Other','MT')
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (181,1,1,'Passport','MT')                              -- Relacionado con el GP BlackListService.PRMalta - IdPassportTypeId
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (182,1,2,'ID Card','MT')                                         -- Relacionado con el GP BlackListService.PRMalta - IdNumberTypeId
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (183,1,3,'Driving licence','MT')   -- Relacionado con el GP BlackListService.PRMalta - LicenceNumberTypeId
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (184,1,4,'Citizenship card','MT')
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (185,1,5,'Employment licence','MT')
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (186,1,6,'Residency permit','MT')
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (187,1,7,'Student visa','MT')
END
GO

-- MALTA REGIONS
IF (NOT EXISTS (SELECT * FROM federal_states WHERE fs_country_iso_code2 = 'MT' ))
BEGIN
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Malta','MT'); --????
END
GO

DECLARE @CountryISOCode2 AS VARCHAR(10)
SELECT @CountryISOCode2 = GP_KEY_VALUE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'RegionalOptions' AND GP_SUBJECT_KEY = 'CountryISOCode2'
IF (@CountryISOCode2 = 'MT')
BEGIN
  UPDATE GENERAL_PARAMS SET GP_KEY_VALUE = 'Regions' WHERE GP_GROUP_KEY = 'Account.Fields'  AND GP_SUBJECT_KEY = 'State.Name'
  UPDATE GENERAL_PARAMS SET GP_KEY_VALUE = 'MT'     WHERE GP_GROUP_KEY = 'Account.DefaultValues' AND GP_SUBJECT_KEY = 'Country'
  UPDATE GENERAL_PARAMS SET GP_KEY_VALUE = '181'    WHERE GP_GROUP_KEY = 'Account.DefaultValues' AND GP_SUBJECT_KEY = 'DocumentType' -- Passport
END
GO

/******* PROCEDURES *******/

GO

GRANT EXECUTE ON GetReportBanking TO WGGUI WITH GRANT OPTION

GO