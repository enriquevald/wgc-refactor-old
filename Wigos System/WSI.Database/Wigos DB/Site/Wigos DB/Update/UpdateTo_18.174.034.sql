/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 173;

SET @New_ReleaseId = 174;
SET @New_ScriptName = N'UpdateTo_18.174.034.sql';
SET @New_Description = N'GamingTables updates and storeds';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/******* TABLES  *******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pending_tickets_action]') AND type in (N'U'))
 DROP TABLE [dbo].[pending_tickets_action]
GO

CREATE TABLE [dbo].[pending_tickets_action](
      [pta_unique_id] [bigint] IDENTITY(1,1) NOT NULL,
      [pta_action_type] [int] NOT NULL,
      [pta_terminal_id] [int] NOT NULL,
      [pta_sequence_id] [bigint] NOT NULL,
      [pta_ticket_id] [bigint] NOT NULL,
      [pta_status] [int] NOT NULL,
      [pta_validation_number] [bigint] NOT NULL,
      [pta_register_date] [datetime] NOT NULL,
      [pta_amount] [money] NOT NULL,
CONSTRAINT [PK_pending_tickets_action] PRIMARY KEY CLUSTERED 
(
      [pta_unique_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/******* INDEXES  *******/

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[gaming_tables]') AND name = N'IX_gt_cashier_id')
            DROP INDEX [IX_gt_cashier_id] ON [dbo].[gaming_tables] WITH ( ONLINE = OFF )
GO
CREATE NONCLUSTERED INDEX [IX_gt_cashier_id] ON [dbo].[gaming_tables] 
(
      [gt_cashier_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO


IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[gaming_tables]') AND name = N'IX_gamning_tables_combos')
            DROP INDEX [IX_gamning_tables_combos] ON [dbo].[gaming_tables] WITH ( ONLINE = OFF )
GO
CREATE NONCLUSTERED INDEX [IX_gamning_tables_combos] ON [dbo].[gaming_tables] 
(     
  [gt_enabled],
  [gt_name] ASC,
  [gt_cashier_id],      
  [gt_gaming_table_id] ASC,
  [gt_code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO


IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[gaming_tables]') AND name = N'IX_gt_type_id_gt_name')
DROP INDEX [IX_gt_type_id_gt_name] ON [dbo].[gaming_tables] WITH ( ONLINE = OFF )
GO
CREATE NONCLUSTERED INDEX [IX_gt_type_id_gt_name] ON [dbo].[gaming_tables] 
(
      [gt_type_id] ASC,
      [gt_name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO


IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[gaming_tables]') AND name = N'IX_gt_name')
DROP INDEX [IX_gt_name] ON [dbo].[gaming_tables] WITH ( ONLINE = OFF )
GO
CREATE NONCLUSTERED INDEX [IX_gt_name] ON [dbo].[gaming_tables] 
(
      [gt_name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO


IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[gaming_tables_types]') AND name = N'IX_gtt_enabled')
DROP INDEX [IX_gtt_enabled] ON [dbo].[gaming_tables_types] WITH ( ONLINE = OFF )
GO

CREATE NONCLUSTERED INDEX [IX_gtt_enabled] ON [dbo].[gaming_tables_types] 
(
      [gtt_enabled] ASC,
      [gtt_gaming_table_type_id] ASC,
      [gtt_name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO


/******* STORED  *******/

IF OBJECT_ID (N'dbo.ApplyExchange', N'FN') IS NOT NULL
    DROP FUNCTION DBO.ApplyExchange;                 
GO

    -- PURPOSE: Apply exchange to national currency
    --
    -- PARAMS:
    --     - INPUT:
    --           - @pAmount
    --           - @pIsoCode
    --
    -- RETURNS:
    --     - DECIMAL(16,8): Operation result
    --

CREATE FUNCTION ApplyExchange
(@pAmount  DECIMAL(16,8),
  @pIsoCode VARCHAR(3))
RETURNS DECIMAL(16,8)
AS
BEGIN

      DECLARE @Exchanged      DECIMAL(18,8)
      DECLARE @Change         DECIMAL(18,8)
      DECLARE @NumDecimals INT
      
      IF(@pIsoCode IS NULL)
            SET @Exchanged = @pAmount;
      ELSE
      BEGIN
            SELECT 
                  @Change = CE_CHANGE,
                  @NumDecimals = CE_NUM_DECIMALS
            FROM CURRENCY_EXCHANGE
            WHERE CE_TYPE = 0
                    AND CE_CURRENCY_ISO_CODE = @pIsoCode

            SET @NumDecimals = ISNULL(@NumDecimals, 0)     
            SET @Exchanged = ROUND(ISNULL(@Change * @pAmount, 0), @NumDecimals, 1)   

      END
      
      
      RETURN @Exchanged
END -- ApplyExchange
GO

GRANT EXECUTE ON [dbo].[ApplyExchange] TO [wggui]
GO

  /****** Object:  StoredProcedure [dbo].[TITO_SetTicketCanceled]    Script Date: 01/21/2014 11:07:40 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TITO_SetTicketCanceled]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[TITO_SetTicketCanceled]
GO

CREATE PROCEDURE [dbo].[TITO_SetTicketCanceled]
      @pTerminalId                 INT,
      @pTicketValidationNumber     BIGINT,
      @MsgSequenceId               BIGINT,
      --- OUTPUT
      @pTicketId                   BIGINT   OUTPUT,
      @pTicketAmount               MONEY    OUTPUT
AS
BEGIN
      DECLARE  @_ticket_status_canceled              INT
      DECLARE  @_ticket_status_pending_cancel        INT
      DECLARE  @_min_ticket_id                       BIGINT
      DECLARE  @_max_ticket_id                       BIGINT
      DECLARE  @_ticket_amount                       MONEY

      SET NOCOUNT ON

      SET @pTicketId = NULL
      SET @pTicketAmount = NULL

      SET @_ticket_status_canceled       = 1 -- TITO_TICKET_STATUS.CANCELED
      SET @_ticket_status_pending_cancel = 5 -- TITO_TICKET_STATUS.PENDING_CANCEL

      SELECT   @_min_ticket_id = MIN (TI_TICKET_ID)
             , @_max_ticket_id = MAX (TI_TICKET_ID)
             , @_ticket_amount = MAX (TI_AMOUNT)
        FROM   TICKETS WITH (INDEX (IX_ti_validation_number_status))
      WHERE    TI_VALIDATION_NUMBER       = @pTicketValidationNumber
         AND   TI_STATUS                  = @_ticket_status_pending_cancel
         AND   TI_LAST_ACTION_TERMINAL_ID = @pTerminalId
         
      IF @_min_ticket_id =  @_max_ticket_id
      BEGIN
            -- One ticket found, change its status
            UPDATE   TICKETS 
               SET   TI_STATUS                  = @_ticket_status_canceled 
                   , TI_LAST_ACTION_DATETIME    = GETDATE() 
             WHERE   TI_TICKET_ID               = @_min_ticket_id 
               AND   TI_STATUS                  = @_ticket_status_pending_cancel
               AND   TI_LAST_ACTION_TERMINAL_ID = @pTerminalId
         
            IF @@ROWCOUNT = 1
            BEGIN
                  SET   @pTicketId     = @_min_ticket_id
                  SET   @pTicketAmount = @_ticket_amount

                  INSERT INTO   PENDING_TICKETS_ACTION
                              ( PTA_ACTION_TYPE
                              , PTA_TERMINAL_ID
                              , PTA_SEQUENCE_ID
                              , PTA_TICKET_ID
                              , PTA_STATUS
                              , PTA_VALIDATION_NUMBER
                              , PTA_REGISTER_DATE
                              , PTA_AMOUNT )
                       VALUES ( 1 -- TITO_TICKET_STATUS.CANCELED
                              , @pTerminalId
                              , @MsgSequenceId
                              , @pTicketId
                              , @_ticket_status_canceled
                              , @pTicketValidationNumber
                              , GETDATE()
                              , @pTicketAmount ) 
            END

            RETURN
      END
END
GO

  /****** Object:  StoredProcedure [dbo].[TITO_SetTicketValid]    Script Date: 01/21/2014 11:07:40 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TITO_SetTicketValid]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[TITO_SetTicketValid]
GO

CREATE PROCEDURE [dbo].[TITO_SetTicketValid] 
      @pTerminalId                 INT,
      @pTicketValidationNumber     BIGINT,
      --- OUTPUT
      @pTicketId                               BIGINT   OUTPUT
AS
BEGIN
      DECLARE  @_ticket_status_valid                 INT
      DECLARE  @_ticket_status_pending_cancel        INT
      DECLARE  @_min_ticket_id                       BIGINT
      DECLARE  @_max_ticket_id                       BIGINT

      SET NOCOUNT ON

      SET @pTicketId = NULL

      SET @_ticket_status_valid          = 0 -- TITO_TICKET_STATUS.VALID
      SET @_ticket_status_pending_cancel = 5 -- TITO_TICKET_STATUS.PENDING_CANCEL

      SELECT   @_min_ticket_id = MIN (TI_TICKET_ID)
             , @_max_ticket_id = MAX (TI_TICKET_ID)
        FROM   TICKETS WITH (INDEX (IX_ti_validation_number_status))
      WHERE    TI_VALIDATION_NUMBER       = @pTicketValidationNumber
         AND   TI_STATUS                  = @_ticket_status_pending_cancel
         AND   TI_LAST_ACTION_TERMINAL_ID = @pTerminalId
         
      IF @_min_ticket_id =  @_max_ticket_id
      BEGIN
            -- One ticket found, change its status
            UPDATE   TICKETS 
               SET   TI_STATUS                  = @_ticket_status_valid 
                   , TI_LAST_ACTION_TERMINAL_ID = @pTerminalId 
                   , TI_LAST_ACTION_DATETIME    = GETDATE() 
             WHERE   TI_TICKET_ID               = @_min_ticket_id 
               AND   TI_STATUS                  = @_ticket_status_pending_cancel
               AND   TI_LAST_ACTION_TERMINAL_ID = @pTerminalId
         
            IF @@ROWCOUNT = 1
                  SET   @pTicketId = @_min_ticket_id
      END
END

GO

/****** Object:  StoredProcedure [dbo].[TITO_ChangeTicketStatus]    Script Date: 01/21/2014 11:07:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TITO_ChangeTicketStatus]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[TITO_ChangeTicketStatus]
GO

CREATE PROCEDURE [dbo].[TITO_ChangeTicketStatus] 
       @pTerminalId                   INT,
       @pTicketValidationNumber       BIGINT,
       @pTicketNewStatus              INT,
       @MsgSequenceId                 BIGINT,
       @pDefaultAllowRedemption       BIT,
       @pDefaultMaxAllowedTicketIn    MONEY,
       --- OUTPUT
       @pTicketId                     BIGINT    OUTPUT,
       @pTicketType                   INT       OUTPUT,
       @pTicketAmount                 MONEY     OUTPUT,
       @pTicketExpiration             DATETIME  OUTPUT

AS
BEGIN
       IF @pTicketNewStatus = 5 -- TITO_TICKET_STATUS.PENDING_CANCEL
       BEGIN
           exec TITO_SetTicketPendingCancel   @pTerminalId, @pTicketValidationNumber, @pDefaultAllowRedemption, @pDefaultMaxAllowedTicketIn
                                            , @pTicketId output, @pTicketType output, @pTicketAmount output, @pTicketExpiration output 
           RETURN
       END

       IF @pTicketNewStatus = 0 -- TITO_TICKET_STATUS.VALID
       BEGIN
           exec TITO_SetTicketValid @pTerminalId, @pTicketValidationNumber, @pTicketId output 
           
           RETURN
       END
       
       IF @pTicketNewStatus = 1 -- TITO_TICKET_STATUS.CANCELED
       BEGIN       
           exec TITO_SetTicketCanceled @pTerminalId, @pTicketValidationNumber, @MsgSequenceId, @pTicketId output, @pTicketAmount output 
           
           RETURN
       END   
END
GO

/******* RECORDS *******/

