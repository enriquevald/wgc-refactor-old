/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 239;

SET @New_ReleaseId = 240;
SET @New_ScriptName = N'UpdateTo_18.240.037.sql';
SET @New_Description = N'SP Updated and new system concept for Efectivo in Stackers.';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/******* TABLES *******/

/******* INDEXES *******/

IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[money_collections]') AND name = N'IX_mc_collection_datetime')
  CREATE NONCLUSTERED INDEX [IX_mc_collection_datetime] ON [dbo].[money_collections] 
  (
        [mc_collection_datetime] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
GO

/******* RECORDS *******/

IF NOT EXISTS (SELECT TOP 1 cc_concept_id FROM CAGE_CONCEPTS WHERE cc_concept_id = 10)
INSERT INTO CAGE_CONCEPTS( cc_concept_id, cc_description, cc_is_provision, cc_show_in_report, cc_name, cc_type, cc_enabled )
     VALUES ( 10, 'Efectivo', 0, 1, 'Efectivo', 0, 1)
GO

IF NOT EXISTS (SELECT TOP 1 cstc_concept_id FROM cage_source_target_concepts WHERE cstc_concept_id = 10)
INSERT INTO cage_source_target_concepts ( cstc_concept_id, cstc_source_target_id, cstc_type, cstc_only_national_currency, cstc_enabled, cstc_price_factor )
     VALUES (10, 12, 2, 1, 1, 1)
GO

DECLARE @NationalIsoCode AS VARCHAR(3)

 SELECT @NationalIsoCode = GP_KEY_VALUE 
        FROM GENERAL_PARAMS 
       WHERE GP_GROUP_KEY = 'RegionalOptions' 
         AND GP_SUBJECT_KEY = 'CurrencyISOCode'

IF NOT EXISTS (SELECT TOP 1 cm_concept_id FROM CAGE_METERS WHERE cm_concept_id = 10)
INSERT INTO CAGE_METERS
           ( cm_source_target_id
           , cm_concept_id
           , cm_iso_code
           , cm_value
           , cm_value_in
           , cm_value_out )
     VALUES
           ( 12
           , 10
           , @NationalIsoCode
           , 0
           , 0
           , 0 )


 DECLARE @CurrentSessionId AS BIGINT
      
   DECLARE Curs CURSOR FOR 
    SELECT cgs_cage_session_id
        FROM cage_sessions 
      
    SET NOCOUNT ON;

    OPEN Curs

    FETCH NEXT FROM Curs INTO @CurrentSessionId
    
    WHILE @@FETCH_STATUS = 0
    BEGIN
    
		 IF NOT EXISTS (SELECT TOP 1 csm_concept_id FROM CAGE_SESSION_METERS WHERE csm_concept_id = 9 AND csm_cage_session_id = @CurrentSessionId)
		 INSERT INTO CAGE_SESSION_METERS
           ( csm_cage_session_id
           , csm_source_target_id
           , csm_concept_id
           , csm_iso_code
           , csm_value
           , csm_value_in
           , csm_value_out)
		 VALUES
           ( @CurrentSessionId
           , 12
           , 9
           , @NationalIsoCode
           , 0
           , 0
           , 0 )
    
		 IF NOT EXISTS (SELECT TOP 1 csm_concept_id FROM CAGE_SESSION_METERS WHERE csm_concept_id = 10 AND csm_cage_session_id = @CurrentSessionId)
		 INSERT INTO CAGE_SESSION_METERS
           ( csm_cage_session_id
           , csm_source_target_id
           , csm_concept_id
           , csm_iso_code
           , csm_value
           , csm_value_in
           , csm_value_out)
		 VALUES
           ( @CurrentSessionId
           , 12
           , 10
           , @NationalIsoCode
           , 0
           , 0
           , 0 )
    
		 FETCH NEXT FROM Curs INTO @CurrentSessionId
    
    END
    
    CLOSE Curs
    DEALLOCATE Curs    
    
GO

UPDATE CAGE_SOURCE_TARGET
   SET CST_SOURCE_TARGET_NAME = 'Stackers'
 WHERE CST_SOURCE_TARGET_ID = 12 
   AND CST_SOURCE_TARGET_NAME = 'Terminales'

GO

/******* STORED PROCEDURES *******/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CageGetGlobalReportData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CageGetGlobalReportData]
GO

CREATE PROCEDURE [dbo].[CageGetGlobalReportData]
        @pCageSessionIds NVARCHAR(MAX) = NULL
AS
BEGIN

    DECLARE @NationalIsoCode NVARCHAR(3)
    DECLARE @CageOperationType_FromTerminal INT

      SELECT @NationalIsoCode = GP_KEY_VALUE 
        FROM GENERAL_PARAMS 
       WHERE GP_GROUP_KEY = 'RegionalOptions' 
         AND GP_SUBJECT_KEY = 'CurrencyISOCode'
               
      SET @CageOperationType_FromTerminal = 104
      
      -- TABLE[0]: Get cage stock
    SELECT CGS_ISO_CODE                                          
         , CGS_DENOMINATION                                      
         , CGS_QUANTITY 
        FROM (SELECT CGS_ISO_CODE                                          
									 , CGS_DENOMINATION                                      
									 , CGS_QUANTITY                                          
								FROM CAGE_STOCK
                  
                  UNION ALL
                  
                  SELECT C.CH_ISO_CODE AS CGS_ISO_CODE
											 , C.CH_DENOMINATION AS CGS_DENOMINATION
											 , CST.CHSK_QUANTITY AS CGS_QUANTITY
										FROM CHIPS_STOCK AS CST
												 INNER JOIN CHIPS AS C ON CST.CHSK_CHIP_ID = C.CH_CHIP_ID
							) AS _tbl         
                                                   
  ORDER BY CGS_ISO_CODE                                          
         , CASE WHEN CGS_DENOMINATION >= 0 THEN CGS_DENOMINATION 
                ELSE CGS_DENOMINATION * (-100000) END -- show negative denominations at the end (coins, cheks, etc.)


      IF @pCageSessionIds IS NOT NULL
      BEGIN
            -- Session IDs Split
            SELECT SST_VALUE AS CAGE_SESSION_ID INTO #TMP_CAGE_SESSIONS FROM [SplitStringIntoTable] (@pCageSessionIds,','  , 1)
    
            -- TABLE[1]: Get cage sessions information
            SELECT CSM.CSM_SOURCE_TARGET_ID AS CM_SOURCE_TARGET_ID
                  , CSM.CSM_CONCEPT_ID AS CM_CONCEPT_ID
             , CSM.CSM_ISO_CODE AS CM_ISO_CODE
             , SUM(CSM.CSM_VALUE_IN) AS CM_VALUE_IN
             , SUM(CSM.CSM_VALUE_OUT) AS CM_VALUE_OUT
             , CST.CST_SOURCE_TARGET_NAME
             , CC.CC_DESCRIPTION
              FROM CAGE_SESSION_METERS AS CSM
               INNER JOIN CAGE_SOURCE_TARGET_CONCEPTS AS CSTC ON CSTC.CSTC_CONCEPT_ID = CSM.CSM_CONCEPT_ID AND CSTC.CSTC_SOURCE_TARGET_ID = CSM.CSM_SOURCE_TARGET_ID
               INNER JOIN CAGE_CONCEPTS AS CC ON CC.CC_CONCEPT_ID = CSTC.CSTC_CONCEPT_ID
               INNER JOIN CAGE_SOURCE_TARGET AS CST ON CST.CST_SOURCE_TARGET_ID = CSTC.CSTC_SOURCE_TARGET_ID
            WHERE CSM.CSM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
           AND CC.CC_SHOW_IN_REPORT = 1
           AND CC.CC_ENABLED = 1
           AND (CC.CC_TYPE = 1 OR CC_CONCEPT_ID IN (9, 10))
           AND CSTC.CSTC_ENABLED = 1
      GROUP BY CSM.CSM_SOURCE_TARGET_ID, CSM.CSM_CONCEPT_ID, CSM.CSM_ISO_CODE, CST.CST_SOURCE_TARGET_NAME, CC.CC_DESCRIPTION
      ORDER BY CSM.CSM_SOURCE_TARGET_ID
                  , CSM.CSM_CONCEPT_ID
                  , CASE WHEN CSM.CSM_ISO_CODE = @NationalIsoCode THEN '000' ELSE CSM.CSM_ISO_CODE END                    

            -- TABLE[2]: Get dynamic liabilities
            SELECT CC.CC_CONCEPT_ID AS LIABILITY_CONCEPT_ID
             , CC.CC_DESCRIPTION AS LIABILITY_NAME 
             , CSM.CSM_ISO_CODE AS LIABILITY_ISO_CODE
             , SUM(CSM.CSM_VALUE) AS LIABILITY_VALUE
          FROM CAGE_SESSION_METERS AS CSM
               INNER JOIN CAGE_CONCEPTS AS CC ON CC.CC_CONCEPT_ID = CSM.CSM_CONCEPT_ID
            WHERE CC.CC_ENABLED = 1
                    AND CC.CC_SHOW_IN_REPORT = 1
              AND CC.CC_IS_PROVISION = 1
              AND CSM.CSM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS) 
      GROUP BY CC.CC_CONCEPT_ID, CC.CC_DESCRIPTION, CSM.CSM_ISO_CODE
      ORDER BY CC.CC_CONCEPT_ID ASC
                  , CASE WHEN CSM.CSM_ISO_CODE = @NationalIsoCode THEN '000' ELSE CSM.CSM_ISO_CODE END 
      
            -- TABLE[3]: Get session sumary
            SELECT CMD_ISO_CODE AS ISO_CODE
             , ISNULL(ORIGIN, 0) AS ORIGIN
             , SUM(CMD_DEPOSITS) AS DEPOSITS
             , SUM(CMD_WITHDRAWALS) AS WITHDRAWALS
              FROM (SELECT CMD_ISO_CODE
                     , ORIGIN
                     , SUM(CMD_DEPOSITS) CMD_DEPOSITS
                     , SUM(CMD_WITHDRAWALS) CMD_WITHDRAWALS      
                  FROM (SELECT CASE WHEN CMD_QUANTITY >= 0 THEN 0
                                    ELSE CMD_QUANTITY END  AS ORIGIN
                             , CMD_ISO_CODE
                             , CMD_DENOMINATION
                             , CASE WHEN CGM_TYPE >= 100 AND CGM_TYPE NOT IN (2, 102, 99, 199) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY < 0 THEN CMD_DENOMINATION
                                                                                                                     ELSE CMD_QUANTITY * CMD_DENOMINATION END), 0)
                                    ELSE 0 END AS CMD_DEPOSITS
                             , CASE WHEN CGM_TYPE < 100 AND CGM_TYPE NOT IN (2, 102, 99, 199) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY < 0 THEN CMD_DENOMINATION             
                                                                                                                    ELSE CMD_QUANTITY * CMD_DENOMINATION END), 0)
                                                     ELSE 0 END AS CMD_WITHDRAWALS

                          FROM CAGE_MOVEMENTS 
                               INNER JOIN   CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          
                         WHERE CGM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
                               GROUP BY CMD_ISO_CODE, CMD_DENOMINATION, CGM_TYPE, CMD_QUANTITY
                       ) AS T1        
                        GROUP BY ORIGIN, CMD_ISO_CODE

                        UNION ALL 

                        --The next one is for the Stackers / Terminals collection
                        SELECT @NationalIsoCode AS CMD_ISO_CODE                                                                                     
                             , CASE WHEN ISNULL(MCD_FACE_VALUE, 0) < 0 THEN CAST(MCD_FACE_VALUE AS INT) 
                                    ELSE 0 END AS ORIGIN
                             , CASE WHEN ISNULL(MCD_FACE_VALUE,0) = -200 THEN MC_COLLECTED_TICKET_AMOUNT
                                    ELSE (CASE WHEN ISNULL(MCD_FACE_VALUE,0) < 0 THEN MCD_NUM_COLLECTED    
                                               ELSE (ISNULL(MCD_FACE_VALUE,0) * ISNULL(MCD_NUM_COLLECTED,0)) END) 
                                                      END AS CMD_DEPOSITS       
                             , 0 AS CMD_WITHDRAWALS                                                                                     
                          FROM MONEY_COLLECTION_DETAILS                                                                                    
                               INNER JOIN MONEY_COLLECTIONS ON MONEY_COLLECTIONS.MC_COLLECTION_ID = MONEY_COLLECTION_DETAILS.MCD_COLLECTION_ID  
                               INNER JOIN CAGE_MOVEMENTS ON MC_COLLECTION_ID = CAGE_MOVEMENTS.CGM_MC_COLLECTION_ID                              
                         WHERE CAGE_MOVEMENTS.CGM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
                           AND CAGE_MOVEMENTS.CGM_TYPE = @CAGEOPERATIONTYPE_FROMTERMINAL
                           AND ISNULL(MCD_NUM_COLLECTED,0) > 0
                      
               ) AS _TBL_GLB
      GROUP BY _TBL_GLB.CMD_ISO_CODE, _TBL_GLB.ORIGIN
      ORDER BY CASE WHEN _TBL_GLB.CMD_ISO_CODE = @NationalIsoCode THEN '000' ELSE _TBL_GLB.CMD_ISO_CODE END 
                   , _TBL_GLB.ORIGIN DESC
         
         
            -- TABLE[4]: Other System Concepts           
        SELECT CSM_CONCEPT_ID AS CM_CONCEPT_ID
             , CC.CC_DESCRIPTION AS CM_DESCRIPTION
             , CSM_ISO_CODE AS CM_ISO_CODE
             , SUM(CSM_VALUE) AS CM_VALUE
              FROM CAGE_SESSION_METERS AS CSM
               INNER JOIN CAGE_CONCEPTS AS CC ON CSM.CSM_CONCEPT_ID = CC.CC_CONCEPT_ID
         WHERE CSM_SOURCE_TARGET_ID = 0
           AND CSM_CONCEPT_ID IN (7, 8)
           AND CSM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
      GROUP BY CSM_CONCEPT_ID, CSM_ISO_CODE, CSM_VALUE, CC.CC_DESCRIPTION 
      ORDER BY CM_CONCEPT_ID
                 , CASE WHEN CSM_ISO_CODE = @NationalIsoCode THEN '000' ELSE CSM_ISO_CODE END 

            DROP TABLE #TMP_CAGE_SESSIONS 

      END -- IF @pCageSessionIds IS NOT NULL THEN
      
      ELSE -- IF @pCageSessionIds IS NULL
      BEGIN
      
            -- TABLE[1]: Get cage sessions information
        SELECT CM.CM_SOURCE_TARGET_ID
             , CM.CM_CONCEPT_ID
             , CM.CM_ISO_CODE
             , CM.CM_VALUE_IN
             , CM.CM_VALUE_OUT
             , CST.CST_SOURCE_TARGET_NAME
             , CC.CC_DESCRIPTION
              FROM CAGE_METERS AS CM
               INNER JOIN CAGE_SOURCE_TARGET_CONCEPTS AS CSTC ON CSTC.CSTC_CONCEPT_ID = CM.CM_CONCEPT_ID AND CSTC.CSTC_SOURCE_TARGET_ID = CM.CM_SOURCE_TARGET_ID
               INNER JOIN CAGE_CONCEPTS AS CC ON CC.CC_CONCEPT_ID = CSTC.CSTC_CONCEPT_ID
               INNER JOIN CAGE_SOURCE_TARGET AS CST ON CST.CST_SOURCE_TARGET_ID = CSTC.CSTC_SOURCE_TARGET_ID
            WHERE (CC.CC_TYPE = 1 OR CC_CONCEPT_ID IN (9, 10))
           AND CC.CC_ENABLED = 1
           AND CSTC.CSTC_ENABLED = 1
           AND CC.CC_SHOW_IN_REPORT = 1
      ORDER BY CM.CM_SOURCE_TARGET_ID
                 , CM.CM_CONCEPT_ID
                  , CASE WHEN CM.CM_ISO_CODE = @NationalIsoCode THEN '000' ELSE CM.CM_ISO_CODE END   


            -- TABLE[2]: Get dynamic liabilities
        SELECT CC.CC_CONCEPT_ID AS LIABILITY_CONCEPT_ID
             , CC.CC_DESCRIPTION AS LIABILITY_NAME 
             , CM.CM_ISO_CODE AS LIABILITY_ISO_CODE
             , SUM(CM.CM_VALUE) AS LIABILITY_VALUE
              FROM CAGE_METERS AS CM
               INNER JOIN CAGE_CONCEPTS AS CC ON CC.CC_CONCEPT_ID = CM.CM_CONCEPT_ID
            WHERE CC.CC_ENABLED = 1
           AND CC.CC_SHOW_IN_REPORT = 1
           AND CC.CC_IS_PROVISION = 1
      GROUP BY CC.CC_CONCEPT_ID, CC.CC_DESCRIPTION, CM.CM_ISO_CODE
      ORDER BY CC.CC_CONCEPT_ID ASC 
                   , CASE WHEN CM.CM_ISO_CODE = @NationalIsoCode THEN '000' ELSE CM.CM_ISO_CODE END 
      
            -- TABLE[3]: Get session sumary
        SELECT CMD_ISO_CODE AS ISO_CODE
             , ISNULL(ORIGIN, 0) AS ORIGIN
             , SUM(CMD_DEPOSITS) AS DEPOSITS
             , SUM(CMD_WITHDRAWALS) AS WITHDRAWALS     
              FROM (SELECT CMD_ISO_CODE
                     , ORIGIN
                     , SUM(CMD_DEPOSITS) CMD_DEPOSITS
                     , SUM(CMD_WITHDRAWALS) CMD_WITHDRAWALS      
                  FROM (SELECT CASE WHEN CMD_QUANTITY >= 0 THEN 0
                                    ELSE CMD_QUANTITY END AS ORIGIN
                             , CMD_ISO_CODE
                             , CMD_DENOMINATION
                             , CASE WHEN CGM_TYPE >= 100 AND CGM_TYPE NOT IN (2, 102, 99, 199) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY < 0 THEN CMD_DENOMINATION
                                                                                                                     ELSE CMD_QUANTITY * CMD_DENOMINATION END), 0)
                                    ELSE 0 END AS CMD_DEPOSITS
                             , CASE WHEN CGM_TYPE < 100 AND CGM_TYPE NOT IN (2, 102, 99, 199) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY < 0 THEN CMD_DENOMINATION             
                                                                                                                    ELSE CMD_QUANTITY * CMD_DENOMINATION END), 0)
                                    ELSE 0 END AS CMD_WITHDRAWALS
                                     FROM CAGE_MOVEMENTS 
                               INNER JOIN   CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          
                      GROUP BY CMD_ISO_CODE, CMD_DENOMINATION, CGM_TYPE, CMD_QUANTITY
                       ) AS T1
              GROUP BY ORIGIN,CMD_ISO_CODE

                UNION ALL 

                --The next one is for the Stackers / Terminals collection
                SELECT @NationalIsoCode AS CMD_ISO_CODE                                                                                     
                     , CASE WHEN ISNULL(MCD_FACE_VALUE, 0) < 0 THEN CAST(MCD_FACE_VALUE AS INT) 
                            ELSE 0 END AS ORIGIN
                     , CASE WHEN ISNULL(MCD_FACE_VALUE,0) = -200 THEN MC_COLLECTED_TICKET_AMOUNT
                            ELSE (CASE WHEN ISNULL(MCD_FACE_VALUE,0) < 0 THEN MCD_NUM_COLLECTED    
                                       ELSE (ISNULL(MCD_FACE_VALUE,0) * ISNULL(MCD_NUM_COLLECTED,0)) END) 
                                         END AS CMD_DEPOSITS       
                     , 0 AS CMD_WITHDRAWALS                                                                                     
                  FROM MONEY_COLLECTION_DETAILS                                                                                    
                       INNER JOIN MONEY_COLLECTIONS ON MONEY_COLLECTIONS.MC_COLLECTION_ID = MONEY_COLLECTION_DETAILS.MCD_COLLECTION_ID  
                       INNER JOIN CAGE_MOVEMENTS ON MC_COLLECTION_ID = CAGE_MOVEMENTS.CGM_MC_COLLECTION_ID                              
                 WHERE CAGE_MOVEMENTS.CGM_TYPE = @CAGEOPERATIONTYPE_FROMTERMINAL
                   AND ISNULL(MCD_NUM_COLLECTED, 0) > 0
                           
               ) AS _TBL_GLB              
      GROUP BY _TBL_GLB.CMD_ISO_CODE, _TBL_GLB.ORIGIN
      ORDER BY CASE WHEN _TBL_GLB.CMD_ISO_CODE = @NationalIsoCode THEN '000' ELSE _TBL_GLB.CMD_ISO_CODE END 
             , _TBL_GLB.ORIGIN DESC 
         
            -- TABLE[4]: Other System Concepts           
        SELECT CM_CONCEPT_ID
             , CC.CC_DESCRIPTION AS CM_DESCRIPTION
             , CM_ISO_CODE
             , SUM(CM_VALUE) AS CM_VALUE
          FROM CAGE_METERS AS CM
               INNER JOIN CAGE_CONCEPTS AS CC ON CM.CM_CONCEPT_ID = CC.CC_CONCEPT_ID
         WHERE CM_SOURCE_TARGET_ID = 0
           AND CM_CONCEPT_ID IN (7, 8)
      GROUP BY CM_CONCEPT_ID, CM_ISO_CODE, CM_VALUE, CC.CC_DESCRIPTION
      ORDER BY CM_CONCEPT_ID
             , CASE WHEN CM_ISO_CODE = @NationalIsoCode THEN '000' ELSE CM_ISO_CODE END 

      END -- ELSE
                   
END -- CageGetGlobalReportData
GO

GRANT EXECUTE ON [dbo].[CageGetGlobalReportData] TO [wggui] WITH GRANT OPTION 
GO


IF OBJECT_ID (N'dbo.PR_collection_by_machine_and_denomination', N'P') IS NOT NULL
    DROP PROCEDURE dbo.PR_collection_by_machine_and_denomination;                 
GO    

CREATE PROCEDURE [dbo].[PR_collection_by_machine_and_denomination] 
 ( @pFromDt DATETIME   
  ,@pToDt DATETIME    
  ,@pClosingTime INTEGER
  ,@pTerminalWhere NVARCHAR(MAX) 
)
AS
BEGIN

-- DECLARATIONS
 DECLARE @Sql AS VARCHAR(MAX)
 DECLARE @Nrows AS int                              
 DECLARE @index AS int                              
 DECLARE @q AS table(i int)                       

 SET @pClosingTime = @pClosingTime * -1

 SET @Nrows  = DATEDIFF(DAY, @pFromDt, @pToDt)+1 
 SET @index = 0    

 WHILE @index < @Nrows                           
 BEGIN	                                          
   INSERT INTO @q VALUES(@index)	                
   SET @index = @index + 1                       
 END        

 SELECT I INTO #RP_TEMPORARY FROM @q 
 
 SET @Sql = CAST('
 SELECT   ORDER_DATE 
        , TE_TERMINAL_ID 
        , TE_PROVIDER_ID 
        , ISNULL(TE_MULTI_DENOMINATION, ''--'') AS DENOMINATION 
        , ISNULL(TTL_IN - TTL_OUT, 0) AS COLLECTION 
        , ISNULL(BILL_IN - BILL_OUT, 0) AS BILL 
        , ISNULL(TICKET_IN - TICKET_OUT, 0) AS TICKET 
        , ISNULL(MANUAL, 0) AS MANUAL 
        , ISNULL(CREDIT_CANCEL, 0) AS CREDIT_CANCEL 
        , ISNULL(JACKPOT_DE_SALA, 0) AS JACKPOT_DE_SALA 
        , ISNULL(PROGRESIVES, 0) AS PROGRESIVES 
        , ISNULL(NO_PROGRESIVES, 0) AS NO_PROGRESIVES 
        , ISNULL(PROGRESIVE_PROVISIONS, 0) AS PROGRESIVE_PROVISIONS 
   FROM   TERMINALS 
 LEFT JOIN (SELECT DATEADD(DAY, I, CAST(''' + CAST(@pFromDt AS VARCHAR(50)) + ''' AS DATE)) AS ORDER_DATE FROM #RP_TEMPORARY ) DIA ON ORDER_DATE <= GETDATE() 
 LEFT JOIN (SELECT   PS_TERMINAL_ID 
                   , CAST(DATEADD(HOUR, ' + CAST(@pClosingTime AS VARCHAR(5)) + ', PS_FINISHED) AS DATE) AS PS_FINISHED 
                   , SUM(PS_INITIAL_BALANCE+PS_CASH_IN)                                                                   AS BILL_IN 
                   , SUM(ISNULL(PS_FINAL_BALANCE,(0))+PS_CASH_OUT)                                                        AS BILL_OUT 
                   , SUM(ISNULL(PS_RE_TICKET_IN,(0))+ISNULL(PS_PROMO_RE_TICKET_IN,(0))+ISNULL(PS_PROMO_NR_TICKET_IN,(0))) AS TICKET_IN 
                   , SUM(ISNULL(PS_RE_TICKET_OUT,(0))+ISNULL(PS_PROMO_NR_TICKET_OUT,(0)))                                 AS TICKET_OUT 
                   , SUM(PS_TOTAL_CASH_IN)                                                                                AS TTL_IN 
                   , SUM(PS_TOTAL_CASH_OUT)                                                                               AS TTL_OUT 
              FROM   PLAY_SESSIONS   WITH (INDEX(IX_ps_finished_status)) 
             WHERE   PS_STATUS <> 0 AND PS_PROMO = 0 
               AND   PS_FINISHED >= CAST(''' + CAST(@pFromDt AS VARCHAR(50)) + ''' AS DATETIME) 
               AND   PS_FINISHED  < CAST(''' + CAST(@pToDt AS VARCHAR(50)) + ''' AS DATETIME) 
          GROUP BY   PS_TERMINAL_ID 
                   , CAST(DATEADD(HOUR, ' + CAST(@pClosingTime AS VARCHAR(5)) + ', PS_FINISHED) AS DATE) 
                   ) A ON PS_TERMINAL_ID = TE_TERMINAL_ID AND PS_FINISHED = DIA.ORDER_DATE 
 LEFT JOIN (SELECT   HP_TERMINAL_ID 
                     , CAST(DATEADD(HOUR, ' + CAST(@pClosingTime AS VARCHAR(5)) + ', HP_DATETIME) AS DATE) AS HP_DATETIME 
                     , SUM(CASE WHEN (HP_TYPE IN (10, 1000, 1001, 1010)) THEN ISNULL(HP_AMOUNT, 0) ELSE 0 END) AS MANUAL 
                     , SUM(CASE HP_TYPE WHEN  0 THEN ISNULL(HP_AMOUNT, 0) ELSE 0 END) AS CREDIT_CANCEL 
                     , SUM(CASE HP_TYPE WHEN 20 THEN ISNULL(HP_AMOUNT, 0) ELSE 0 END) AS JACKPOT_DE_SALA 
                     , SUM(CASE HP_TYPE WHEN  1 THEN CASE WHEN (HP_LEVEL>=1 AND HP_LEVEL<=32) THEN ISNULL(HP_AMOUNT, 0) ELSE 0 END END) AS PROGRESIVES 
                     , SUM(CASE HP_TYPE WHEN  1 THEN CASE WHEN (HP_LEVEL <1 OR  HP_LEVEL >32) THEN ISNULL(HP_AMOUNT, 0) ELSE 0 END END) AS NO_PROGRESIVES 
              FROM   HANDPAYS 
             WHERE   HP_DATETIME >= CAST(''' + CAST(@pFromDt AS VARCHAR(50)) + ''' AS DATETIME) 
               AND   HP_DATETIME < CAST(''' + CAST(@pToDt AS VARCHAR(50)) + ''' AS DATETIME) 
             GROUP   BY HP_TERMINAL_ID 
                   , CAST(DATEADD(HOUR, ' + CAST(@pClosingTime AS VARCHAR(5)) + ', HP_DATETIME) AS DATE)
                   ) B ON HP_TERMINAL_ID = TE_TERMINAL_ID AND HP_DATETIME = DIA.ORDER_DATE 
 LEFT JOIN (SELECT   PPT_TERMINAL_ID 
                   , CAST(DATEADD(HOUR, ' + CAST(@pClosingTime AS VARCHAR(5)) + ', PGP_HOUR_TO) AS DATE) AS PGP_HOUR_TO 
                   , SUM(ISNULL(PPT_AMOUNT, 0)) AS PROGRESIVE_PROVISIONS 
             FROM    PROGRESSIVES_PROVISIONS_TERMINALS 
        LEFT JOIN    PROGRESSIVES_PROVISIONS ON PGP_PROVISION_ID = PPT_PROVISION_ID 
                                            AND PGP_PROGRESSIVE_ID = PPT_PROGRESSIVE_ID 
                                            AND PGP_HOUR_TO >= CAST(''' + CAST(@pFromDt AS VARCHAR(50)) + ''' AS DATETIME) 
                                            AND PGP_HOUR_TO < CAST(''' + CAST(@pToDt AS VARCHAR(50)) + ''' AS DATETIME) 
             GROUP   BY PPT_TERMINAL_ID 
                   , CAST(DATEADD(HOUR, ' + CAST(@pClosingTime AS VARCHAR(5)) + ', PGP_HOUR_TO) AS DATE)
                   ) C ON PPT_TERMINAL_ID = TE_TERMINAL_ID AND PGP_HOUR_TO = DIA.ORDER_DATE 
  WHERE ( BILL_IN IS NOT NULL 
     OR   BILL_OUT IS NOT NULL 
     OR   TICKET_IN IS NOT NULL 
     OR   TICKET_OUT IS NOT NULL 
     OR   TTL_IN IS NOT NULL 
     OR   TTL_OUT IS NOT NULL 
     OR   MANUAL IS NOT NULL 
     OR   CREDIT_CANCEL IS NOT NULL 
     OR   JACKPOT_DE_SALA IS NOT NULL 
     OR   PROGRESIVES IS NOT NULL 
     OR   NO_PROGRESIVES IS NOT NULL 
     OR   PROGRESIVE_PROVISIONS IS NOT NULL ) ' + 
     CAST(@pTerminalWhere AS Varchar(max)) +
     ' ORDER   BY ISNULL(TE_MULTI_DENOMINATION, ''--''), TE_PROVIDER_ID, ORDER_DATE, TE_NAME '
      AS varchar(max))

EXECUTE (@Sql)

DROP TABLE #RP_TEMPORARY

END -- END PROCEDURE
GO

-- PERMISSIONS

GRANT EXECUTE ON [dbo].[PR_collection_by_machine_and_denomination] TO [wggui] WITH GRANT OPTION
GO


IF OBJECT_ID (N'dbo.PR_collection_by_machine_and_date', N'P') IS NOT NULL
    DROP PROCEDURE dbo.PR_collection_by_machine_and_date;                 
GO    

CREATE PROCEDURE [dbo].[PR_collection_by_machine_and_date] 
 ( @pFromDt DATETIME   
  ,@pToDt DATETIME    
  ,@pClosingTime INTEGER
  ,@pTerminalWhere NVARCHAR(MAX) 
  ,@pBillDetail BIT
)
AS
BEGIN

  -- DECLARATIONS
   DECLARE @Sql AS VARCHAR(MAX)
   DECLARE @Nrows AS int                              
   DECLARE @index AS int                              
   DECLARE @q AS table(i int)                       
   DECLARE @Columns VARCHAR(MAX)
   DECLARE @ColumnChk varchar(max)

   SET @pClosingTime = @pClosingTime * -1

   SET @Nrows  = DATEDIFF(DAY, @pFromDt, @pToDt)+1 
   SET @index = 0                                  

   WHILE @index < @Nrows                           
   BEGIN	                                          
     INSERT INTO @q VALUES(@index)	                
     SET @index = @index + 1                       
   END 
   
    SET @Columns = ''
    SET @ColumnChk = ''

   SELECT I INTO #TempTable_Days FROM @q 
   
   SELECT @Columns = COALESCE(@Columns + '[' + CAST(CGC_DENOMINATION AS NVARCHAR(20)) + '],', '')
     FROM (SELECT   DISTINCT CGC_DENOMINATION 
             FROM   CAGE_CURRENCIES 
            WHERE   CGC_ISO_CODE IN (SELECT   GP_KEY_VALUE 
                                       FROM   GENERAL_PARAMS 
                                      WHERE   GP_GROUP_KEY = 'RegionalOptions' 
                                        AND   GP_SUBJECT_KEY = 'CurrencyISOCode')
              AND   CGC_DENOMINATION >= 1 
              AND   CGC_ALLOWED = 1
                    UNION  
           SELECT   DISTINCT MCD_FACE_VALUE 
             FROM   MONEY_COLLECTION_DETAILS 
            INNER   JOIN MONEY_COLLECTIONS ON MCD_COLLECTION_ID = MC_COLLECTION_ID
            WHERE   MC_COLLECTION_DATETIME >= @pFromDt
              AND   MC_COLLECTION_DATETIME <  @pToDt
              AND   MC_TERMINAL_ID IS NOT NULL 
              AND   MCD_FACE_VALUE >= 1
          ) AS DTM
  ORDER BY CGC_DENOMINATION

  SET @ColumnChk = REPLACE( @Columns, ',', ' IS NOT NULL OR ')

  SET @Columns = LEFT(@Columns,LEN(@Columns)-1)
  SET @ColumnChk = LEFT(@ColumnChk,LEN(@ColumnChk)-3)
     
  SET @Sql = '
   SELECT   TE_PROVIDER_ID 
          , TE_TERMINAL_ID 
          , ISNULL(TE_MULTI_DENOMINATION, ''--'') AS DENOMINATION 
          , DATEADD(HOUR, ' + CAST(@pClosingTime * -1 AS VARCHAR(5)) + ', CAST(ORDER_DATE AS DATETIME)) AS ORDER_DATE
          , DATEADD(HOUR, ' + CAST((@pClosingTime * -1) + 24 AS VARCHAR(5)) + ', CAST(ORDER_DATE AS DATETIME)) AS ORDER_DATE_FIN
          , TICKET_IN_COUNT
          , TI_IN_AMOUNT_RE
          , TI_IN_AMOUNT_NO_RE
          , (TI_IN_AMOUNT_RE + TI_IN_AMOUNT_NO_RE) as TI_IN_AMOUNT
          , TICKET_OUT_COUNT
          , TI_OUT_AMOUNT_RE
          , TI_OUT_AMOUNT_NO_RE
          , (TI_OUT_AMOUNT_RE + TI_OUT_AMOUNT_NO_RE) as TI_OUT_AMOUNT
          , HAND_PAYS.MANUAL
          , HAND_PAYS.CREDIT_CANCEL
          , HAND_PAYS.JACKPOT_DE_SALA
          , HAND_PAYS.PROGRESIVES
          , HAND_PAYS.NO_PROGRESIVES
          , PROVISIONS.PROGRESIVE_PROVISIONS
          , 0 AS GAP_BILL
          , BILLS.*
     FROM   TERMINALS 
 LEFT JOIN ( SELECT   DATEADD(DAY, I, CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATE)) AS ORDER_DATE FROM #TempTable_Days ) DIA ON ORDER_DATE <= GETDATE() 
 
 LEFT JOIN (SELECT   MC_TERMINAL_ID AS MC_TERMINAL_ID_T
                   , CAST(DATEADD(HOUR, ' + CAST(@pClosingTime AS VARCHAR(5)) + ', MC_COLLECTION_DATETIME) AS DATE) AS MC_COLLECTION_DATETIME_T
                   , SUM(MC_COLLECTED_TICKET_COUNT) AS TICKET_IN_COUNT
                   , SUM(ISNULL(MC_COLLECTED_RE_TICKET_AMOUNT, 0)+ISNULL(MC_COLLECTED_PROMO_RE_TICKET_AMOUNT, 0)) AS TI_IN_AMOUNT_RE
                   , SUM(ISNULL(MC_COLLECTED_PROMO_NR_TICKET_AMOUNT, 0)) AS TI_IN_AMOUNT_NO_RE
              FROM   MONEY_COLLECTIONS 
             WHERE   MC_COLLECTION_DATETIME >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
               AND   MC_COLLECTION_DATETIME <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
               AND   MC_TERMINAL_ID IS NOT NULL
             GROUP   BY MC_TERMINAL_ID
                   , CAST(DATEADD(HOUR, ' + CAST(@pClosingTime AS VARCHAR(5)) + ', MC_COLLECTION_DATETIME) AS DATE) 
           ) TICKETS_IN ON TE_TERMINAL_ID = MC_TERMINAL_ID_T AND MC_COLLECTION_DATETIME_T = DIA.ORDER_DATE 
 LEFT JOIN ( SELECT   TI_CREATED_TERMINAL_ID
                    , CAST(DATEADD(HOUR, ' + CAST(@pClosingTime AS VARCHAR(5)) + ', TI_CREATED_DATETIME) AS DATE) AS TI_CREATED_DATETIME
                    , COUNT(1) AS TICKET_OUT_COUNT
                    , SUM(CASE WHEN TI_TYPE_ID = 2 THEN 0 ELSE TI_AMOUNT END) AS TI_OUT_AMOUNT_RE
                    , SUM(CASE WHEN TI_TYPE_ID = 2 THEN TI_AMOUNT ELSE 0 END) AS TI_OUT_AMOUNT_NO_RE
               FROM   TICKETS 
              WHERE   TI_CREATED_DATETIME >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
                AND   TI_CREATED_DATETIME <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
                AND   TI_CREATED_TERMINAL_TYPE = 1     -- TITO_TERMINAL_TYPE.TERMINAL = 1
                AND   TI_TYPE_ID IN (0,1,2,5) -- TITO_TICKET_TYPE
                                                 --CASHABLE = 0,
                                                 --PROMO_REDEEM = 1,
                                                 --PROMO_NONREDEEM = 2,  // only playable
                                                 --HANDPAY = 3,
                                                 --JACKPOT = 4,
                                                 --OFFLINE = 5
              GROUP   BY TI_CREATED_TERMINAL_ID, CAST(DATEADD(HOUR, ' + CAST(@pClosingTime AS VARCHAR(5)) + ', TI_CREATED_DATETIME) AS DATE) 
           ) TICKETS_OUT ON TE_TERMINAL_ID = TI_CREATED_TERMINAL_ID AND TI_CREATED_DATETIME = DIA.ORDER_DATE 
 LEFT JOIN (SELECT   HP_TERMINAL_ID 
                   , CAST(DATEADD(HOUR, ' + CAST(@pClosingTime AS VARCHAR(5)) + ', HP_DATETIME) AS DATE) AS HP_DATETIME 
                   , SUM(CASE WHEN (HP_TYPE IN (10, 1000, 1001, 1010)) THEN ISNULL(HP_AMOUNT, 0) ELSE 0 END) AS MANUAL 
                   , SUM(CASE HP_TYPE WHEN  0 THEN ISNULL(HP_AMOUNT, 0) ELSE 0 END) AS CREDIT_CANCEL 
                   , SUM(CASE HP_TYPE WHEN 20 THEN ISNULL(HP_AMOUNT, 0) ELSE 0 END) AS JACKPOT_DE_SALA 
                   , SUM(CASE HP_TYPE WHEN  1 THEN CASE WHEN (HP_LEVEL>=1 AND HP_LEVEL<=32) THEN ISNULL(HP_AMOUNT, 0) ELSE 0 END END) AS PROGRESIVES 
                   , SUM(CASE HP_TYPE WHEN  1 THEN CASE WHEN (HP_LEVEL <1 OR  HP_LEVEL >32) THEN ISNULL(HP_AMOUNT, 0) ELSE 0 END END) AS NO_PROGRESIVES 
              FROM   HANDPAYS 
             WHERE   HP_DATETIME >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
               AND   HP_DATETIME <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
             GROUP   BY HP_TERMINAL_ID 
                   , CAST(DATEADD(HOUR, ' + CAST(@pClosingTime AS VARCHAR(5)) + ', HP_DATETIME) AS DATE)
                   ) HAND_PAYS ON HP_TERMINAL_ID = TE_TERMINAL_ID AND HP_DATETIME = DIA.ORDER_DATE 
 LEFT JOIN (SELECT   PPT_TERMINAL_ID 
                   , CAST(DATEADD(HOUR, ' + CAST(@pClosingTime AS VARCHAR(5)) + ', PGP_HOUR_TO) AS DATE) AS PGP_HOUR_TO 
                   , SUM(ISNULL(PPT_AMOUNT, 0)) AS PROGRESIVE_PROVISIONS 
             FROM    PROGRESSIVES_PROVISIONS_TERMINALS 
        LEFT JOIN    PROGRESSIVES_PROVISIONS ON PGP_PROVISION_ID = PPT_PROVISION_ID 
                                            AND PGP_PROGRESSIVE_ID = PPT_PROGRESSIVE_ID 
                                            AND PGP_HOUR_TO >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
                                            AND PGP_HOUR_TO <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
             GROUP   BY PPT_TERMINAL_ID 
                   , CAST(DATEADD(HOUR, ' + CAST(@pClosingTime AS VARCHAR(5)) + ', PGP_HOUR_TO) AS DATE)
                   ) PROVISIONS ON PPT_TERMINAL_ID = TE_TERMINAL_ID AND PGP_HOUR_TO = DIA.ORDER_DATE '
IF @pBillDetail = 1
BEGIN
  SET @Sql =  @Sql +
'LEFT JOIN (SELECT   *  
              FROM ( SELECT   MC_TERMINAL_ID
                            , CAST(DATEADD(HOUR, ' + CAST(@pClosingTime AS VARCHAR(5)) + ', MC_COLLECTION_DATETIME) AS DATE) AS MC_COLLECTION_DATETIME
                            , MCD_FACE_VALUE
                            , (MCD_NUM_COLLECTED * MCD_FACE_VALUE) AS COLLECTED
                       FROM   MONEY_COLLECTION_DETAILS 
                 INNER JOIN   MONEY_COLLECTIONS ON MCD_COLLECTION_ID = MC_COLLECTION_ID
                      WHERE   MC_COLLECTION_DATETIME >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
                        AND   MC_COLLECTION_DATETIME <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
                        AND   MC_TERMINAL_ID IS NOT NULL
                   ) PIV
             PIVOT (  SUM(COLLECTED) FOR MCD_FACE_VALUE IN ('+ @Columns  + ')) AS CHILD
            ) BILLS ON TE_TERMINAL_ID = MC_TERMINAL_ID AND MC_COLLECTION_DATETIME = DIA.ORDER_DATE '
END
ELSE
BEGIN
  SET @Sql =  @Sql +
'LEFT JOIN (SELECT   MC_TERMINAL_ID
                  , CAST(DATEADD(HOUR, ' + CAST(@pClosingTime AS VARCHAR(5)) + ', MC_COLLECTION_DATETIME) AS DATE) AS MC_COLLECTION_DATETIME
                  , 0 AS MCD_FACE_VALUE
                  , SUM(ISNULL(MC_COLLECTED_BILL_AMOUNT, 0)) AS COLLECTED
             FROM   MONEY_COLLECTIONS 
            WHERE   MC_COLLECTION_DATETIME >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
              AND   MC_COLLECTION_DATETIME <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
              AND   MC_TERMINAL_ID IS NOT NULL
            GROUP   BY MC_TERMINAL_ID
                  , CAST(DATEADD(HOUR, ' + CAST(@pClosingTime AS VARCHAR(5)) + ', MC_COLLECTION_DATETIME) AS DATE) 
          ) BILLS ON TE_TERMINAL_ID = MC_TERMINAL_ID AND MC_COLLECTION_DATETIME = DIA.ORDER_DATE '
END

SET @Sql =  @Sql + 'WHERE ( ' 

IF @pBillDetail = 1
BEGIN
  SET @Sql =  @Sql + @ColumnChk 
END
ELSE
BEGIN
  SET @Sql =  @Sql + ' BILLS.COLLECTED IS NOT NULL ' 
END

SET @Sql =  @Sql +
  ' OR TICKET_IN_COUNT IS NOT NULL
    OR TI_IN_AMOUNT_RE IS NOT NULL
    OR TI_IN_AMOUNT_NO_RE IS NOT NULL
    OR TICKET_OUT_COUNT IS NOT NULL
    OR TI_OUT_AMOUNT_RE IS NOT NULL
    OR TI_OUT_AMOUNT_NO_RE IS NOT NULL
    OR HAND_PAYS.MANUAL IS NOT NULL
    OR HAND_PAYS.CREDIT_CANCEL IS NOT NULL
    OR HAND_PAYS.JACKPOT_DE_SALA IS NOT NULL
    OR HAND_PAYS.PROGRESIVES IS NOT NULL
    OR HAND_PAYS.NO_PROGRESIVES IS NOT NULL
    OR PROVISIONS.PROGRESIVE_PROVISIONS IS NOT NULL
    )' + CAST(@pTerminalWhere AS Varchar(max)) +
' ORDER   BY TE_PROVIDER_ID 
        , TE_TERMINAL_ID 
        , TE_NAME 
        , ISNULL(TE_MULTI_DENOMINATION, ''--'')
        , ORDER_DATE '

 -- PRINT @Sql

  EXECUTE (@Sql)

  DROP TABLE #TempTable_Days

END -- END PROCEDURE
GO

-- PERMISSIONS

GRANT EXECUTE ON [dbo].[PR_collection_by_machine_and_date] TO [wggui] WITH GRANT OPTION
GO
