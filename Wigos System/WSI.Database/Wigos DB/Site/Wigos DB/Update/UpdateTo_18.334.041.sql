/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 333;

SET @New_ReleaseId = 334;
SET @New_ScriptName = N'UpdateTo_18.334.041.sql';
SET @New_Description = N'';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/**** GENERAL PARAM *****/


/******* TABLES  *******/

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EXTERNAL_VALIDATION_OPERATION]') AND type in (N'U'))
BEGIN
  ALTER TABLE EXTERNAL_VALIDATION_OPERATION
  ALTER COLUMN evo_transaction_id   NVARCHAR(200) NULL;   
END
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[EXTERNAL_VALIDATION_OPERATION]') and name = 'evo_register_error_code')
BEGIN
  ALTER TABLE EXTERNAL_VALIDATION_OPERATION
  ADD evo_register_error_code   NVARCHAR(10) NULL;   
END
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[EXTERNAL_VALIDATION_OPERATION]') and name = 'evo_register_error_description')
BEGIN
  ALTER TABLE EXTERNAL_VALIDATION_OPERATION
  ADD evo_register_error_description   NVARCHAR(300) NULL;   
END
GO

IF NOT EXISTS(SELECT * FROM sys.columns 
            WHERE Name = N'PT_RETRY' AND Object_ID = Object_ID(N'pinpad_transactions'))
BEGIN

	ALTER TABLE pinpad_transactions add
				PT_RETRY BIGINT NOT NULL     
END


/******* INDEXES *******/

/******* RECORDS *******/

UPDATE REPORT_TOOL_CONFIG 
   SET RTC_MODE_TYPE = 3
WHERE RTC_LOCATION_MENU = 2


IF NOT EXISTS (SELECT [alc_category_id] from [dbo].[alarm_categories] WHERE [alc_category_id] = 57 AND [alc_language_id] = 9) 
BEGIN
  INSERT [dbo].alarm_categories ([alc_category_id], [alc_language_id], [alc_alarm_group_id], [alc_type], [alc_name], [alc_description], [alc_visible]) 
    VALUES (57, 9, 1, 0, N'Output transfer', N'', 1)
END
GO

IF NOT EXISTS (SELECT [alc_category_id] from [dbo].[alarm_categories] WHERE [alc_category_id] = 57 AND [alc_language_id] = 10) 
BEGIN
  INSERT [dbo].alarm_categories ([alc_category_id], [alc_language_id], [alc_alarm_group_id], [alc_type], [alc_name], [alc_description], [alc_visible]) 
    VALUES (57, 10, 1, 0, N'Transferencia de salida', N'', 1)
END
GO

IF NOT EXISTS (SELECT [alcg_alarm_code] from [dbo].[alarm_catalog] WHERE [alcg_alarm_code] = 393282 AND [alcg_language_id] = 9)
BEGIN
  INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) 
    VALUES ( 393282,  9, 0, N'LOCK and AFT mismatch', N'LOCK and AFT mismatch', 1)
END 
GO

IF NOT EXISTS (SELECT [alcg_alarm_code] from [dbo].[alarm_catalog] WHERE [alcg_alarm_code] = 393282 AND [alcg_language_id] = 10)
BEGIN
  INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) 
    VALUES ( 393282, 10, 0, N'LOCK and AFT mismatch', N'LOCK and AFT mismatch', 1)
END
GO

IF NOT EXISTS (SELECT [alcc_alarm_code] from [dbo].[alarm_catalog_per_category] WHERE [alcc_alarm_code] = 393282 AND [alcc_category] = 57) 
BEGIN
  INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) 
    VALUES ( 393282, 57, 0, GETDATE() )
END
GO

/******* PROCEDURES *******/
