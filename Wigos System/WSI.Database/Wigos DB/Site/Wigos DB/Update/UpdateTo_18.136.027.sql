/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 135;

SET @New_ReleaseId = 136;
SET @New_ScriptName = N'UpdateTo_18.136.027.sql';
SET @New_Description = N'AntiMoneyLaundering, currency exchange and credit card recharges ';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END
 
/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/


--
-- MultiSite: DownloadCommonUsers
--

ALTER TABLE GUI_USERS ADD gu_master_id INT NULL
GO
ALTER TABLE GUI_USERS ADD gu_master_sequence_id BIGINT NULL
GO

--
-- Jackpot
--

ALTER TABLE dbo.handpays ADD hp_site_jackpot_notified bit NOT NULL CONSTRAINT DF_handpays_hp_site_jackpot_notified DEFAULT ((0))
GO

UPDATE HANDPAYS SET HP_SITE_JACKPOT_NOTIFIED = 1 WHERE HP_MOVEMENT_ID IS NOT NULL AND HP_TYPE = 20
GO


--
-- AntiMoneyLaundering
--

ALTER TABLE dbo.accounts ADD ac_holder_has_beneficiary bit NOT NULL CONSTRAINT DF_accounts_ac_holder_has_beneficiary DEFAULT ((0))
GO

UPDATE dbo.accounts SET ac_holder_has_beneficiary = 1 WHERE ac_holder_as_beneficiary  = 0
GO

ALTER TABLE dbo.accounts DROP CONSTRAINT DF_ac_holder_as_beneficiary
GO

ALTER TABLE dbo.accounts DROP COLUMN ac_holder_as_beneficiary
GO

--
-- NEW TYPE OF DOCUMENT SCANNED
--
ALTER TABLE accounts ALTER COLUMN ac_holder_id3_type nvarchar(50)
ALTER TABLE accounts ALTER COLUMN ac_beneficiary_id3_type nvarchar(50)
GO

--
--  CURRENCY EXCHANGE AND CREDIT CARD RECHARGES
--

CREATE TABLE [dbo].[currency_exchange](
      [ce_type] [int] NOT NULL,
      [ce_currency_iso_code] [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
      [ce_description] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
      [ce_change] [decimal](16, 8) NOT NULL,
      [ce_num_decimals] [int] NULL,
      [ce_variable_commission] [money] NULL,
      [ce_fixed_commission] [money] NULL,
      [ce_variable_nr2] [money] NULL,
      [ce_fixed_nr2] [money] NULL,
      [ce_status] [bit] NOT NULL,
CONSTRAINT [PK_currency_exchange] PRIMARY KEY CLUSTERED 
(
      [ce_type] ASC,
      [ce_currency_iso_code] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[currency_exchange_audit](
      [cea_audit_id] [bigint] IDENTITY(1,1) NOT NULL,
      [cea_type] [int] NOT NULL,
      [cea_currency_iso_code] [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
      [cea_datetime] [datetime] NOT NULL,
      [cea_old_change] [decimal](16, 8) NOT NULL,
      [cea_new_change] [decimal](16, 8) NOT NULL,
CONSTRAINT [PK_currency_exchange_audit] PRIMARY KEY CLUSTERED 
(
      [cea_audit_id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE dbo.cashier_movements ADD
      cm_currency_iso_code nvarchar(3) NULL,
      cm_aux_amount money NULL
GO

CREATE TABLE dbo.cashier_sessions_by_currency
      (
      csc_session_id bigint NOT NULL,
      csc_iso_code nvarchar(3) NOT NULL,
      csc_type int NOT NULL,
      csc_balance money NOT NULL,
      csc_collected money NOT NULL
      )  ON [PRIMARY]
GO

ALTER TABLE dbo.cashier_sessions_by_currency ADD CONSTRAINT
      DF_cashier_sessions_by_currency_csc_balance DEFAULT 0 FOR csc_balance
GO

ALTER TABLE dbo.cashier_sessions_by_currency ADD CONSTRAINT
      DF_cashier_sessions_by_currency_csc_collected DEFAULT 0 FOR csc_collected
GO

ALTER TABLE dbo.cashier_sessions_by_currency ADD CONSTRAINT
      PK_cashier_sessions_by_currency PRIMARY KEY CLUSTERED 
      (
      csc_session_id,
      csc_iso_code,
      csc_type
      ) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

/****** VIEWS ******/

ALTER VIEW [dbo].[plays_v]
AS
SELECT     dbo.plays.pl_play_id, dbo.plays.pl_play_session_id, dbo.plays.pl_account_id, dbo.plays.pl_terminal_id, dbo.plays.pl_wcp_sequence_id, 
                      dbo.plays.pl_wcp_transaction_id, dbo.plays.pl_datetime, dbo.plays.pl_initial_balance, dbo.plays.pl_played_amount, dbo.plays.pl_won_amount, 
                      dbo.plays.pl_final_balance, dbo.plays.pl_transferred, dbo.plays.pl_game_id

FROM         dbo.plays

GO

/****** CONSTRAINTS ******/

/****** FUNCTIONS ******/

/****** INDEXES ******/

/****** STORED PROCEDURES ******/

CREATE PROCEDURE [dbo].[AML_ThisMonthRechargesPrizes] 
  @AccountId  BIGINT,
  @SplitA     MONEY OUTPUT,
  @Prizes     MONEY OUTPUT
AS
BEGIN
  SET NOCOUNT ON;

  DECLARE @_today as DATETIME
  DECLARE @_day_1 as DATETIME

  SET  @_today = dbo.TodayOpening(0)
  SET  @_day_1 = DATEADD (DAY, 1 - DATEPART(DAY, @_today), @_today)

  SELECT   @SplitA = ISNULL(SUM(CASE WHEN (AM_TYPE = 1) THEN AM_ADD_AMOUNT ELSE 0 END ), 0) 
         , @Prizes = ISNULL(SUM(CASE WHEN (AM_TYPE = 2) THEN AM_SUB_AMOUNT ELSE 0 END ), 0) 
    FROM   ACCOUNT_MOVEMENTS WITH (INDEX (IX_MOVEMENTS_ACCOUNT_DATE))
   WHERE   AM_DATETIME >= @_day_1
     AND   AM_TYPE IN (1, 2)
     AND   AM_ACCOUNT_ID = @AccountId
END
GO

GRANT EXECUTE ON [dbo].[AML_ThisMonthRechargesPrizes] TO [wggui] WITH GRANT OPTION
GO

--------------------------------------------------------------------------------
-- Copyright � 2013 Win Systems International
--------------------------------------------------------------------------------
--
--   MODULE NAME: Update_PersonalInfo.sql.sql
--
--   DESCRIPTION: Update personal Information 
--
--        AUTHOR: Dani Dom�nguez
--
-- CREATION DATE: 08-MAR-2013
--
-- REVISION HISTORY:
--
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 08-MAR-2013 DDM    First release.  
-- 22-MAY-2013 DDM    Fixed bugs 783,793 and 693
--                    Added field AC_BLOCK_DESCRIPTION
-- 28-MAY-2013 DDM    Fixed bug #803
--                    Added field AC_EXTERNAL_REFERENCE
-- 03-JUL-2013 DDM    Added new fields about Money Laundering
--------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Update_PersonalInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Update_PersonalInfo]
GO
CREATE PROCEDURE [dbo].[Update_PersonalInfo]
  @pAccountId bigint
, @pTrackData nvarchar(50)
, @pHolderName nvarchar(200)
, @pHolderId nvarchar(20)
, @pHolderIdType int
, @pHolderAddress01 nvarchar(50)
, @pHolderAddress02 nvarchar(50)
, @pHolderAddress03 nvarchar(50)
, @pHolderCity nvarchar(50)
, @pHolderZip  nvarchar(10) 
, @pHolderEmail01 nvarchar(50)
, @pHolderEmail02 nvarchar(50)
, @pHolderTwitter nvarchar(50)
, @pHolderPhoneNumber01 nvarchar(20)
, @pHolderPhoneNumber02 nvarchar(20)
, @pHolderComments nvarchar(100)
, @pHolderId1 nvarchar(20)
, @pHolderId2 nvarchar(20)
, @pHolderDocumentId1 bigint
, @pHolderDocumentId2 bigint
, @pHolderName1 nvarchar(50)
, @pHolderName2 nvarchar(50)
, @pHolderName3 nvarchar(50)
, @pHolderGender  int
, @pHolderMaritalStatus int
, @pHolderBirthDate datetime
, @pHolderWeddingDate datetime
, @pHolderLevel int
, @pHolderLevelNotify int
, @pHolderLevelEntered datetime
, @pHolderLevelExpiration datetime
, @pPin nvarchar(12)
, @pPinFailures int
, @pPinLastModified datetime
, @pBlocked bit
, @pActivated bit
, @pBlockReason int
, @pHolderIsVip int
, @pHolderTitle nvarchar(15)                       
, @pHolderName4 nvarchar(50)                       
, @pHolderPhoneType01  int
, @pHolderPhoneType02  int
, @pHolderState    nvarchar(50)                    
, @pHolderCountry  nvarchar(50)                
, @pPersonalInfoSequenceId  bigint                     
, @pUserType int
, @pPointsStatus int
, @pDeposit money
, @pCardPay bit
, @pBlockDescription nvarchar(256) 
, @pMSHash varbinary(20) 
, @pMSCreatedOnSiteId int
, @pCreated datetime
, @pExternalReference nvarchar(50) 
, @pHolderOccupation  nvarchar(50)   
, @pHolderExtNum      nvarchar(10) 
, @pHolderNationality  Int 
, @pHolderBirthCountry Int 
, @pHolderFedEntity    Int 
, @pHolderId1Type      Int -- RFC
, @pHolderId2Type      Int -- CURP
, @pHolderId3Type      nvarchar(50) 
, @pHolderId3           nvarchar(20) 
, @pHolderHasBeneficiary bit  
, @pBeneficiaryName     nvarchar(200) 
, @pBeneficiaryName1    nvarchar(50) 
, @pBeneficiaryName2    nvarchar(50) 
, @pBeneficiaryName3    nvarchar(50) 
, @pBeneficiaryBirthDate Datetime 
, @pBeneficiaryGender    int 
, @pBeneficiaryOccupation nvarchar(50) 
, @pBeneficiaryId1Type    int   -- RFC
, @pBeneficiaryId1        nvarchar(20) 
, @pBeneficiaryId2Type    int   -- CURP
, @pBeneficiaryId2        nvarchar(20) 
, @pBeneficiaryId3Type    nvarchar(50) 
, @pBeneficiaryId3        nvarchar(20)
AS
BEGIN

DECLARE @pOtherAccountId as BIGINT

SET @pOtherAccountId = ISNULL ((SELECT AC_ACCOUNT_ID FROM ACCOUNTS WHERE AC_TRACK_DATA = @pTrackData), 0)

SET @pUserType = ISNULL(@pUserType,CASE WHEN ISNULL(@pHolderLevel,0) > 0  THEN 1 ELSE 0 END)

IF @pOtherAccountId <> 0 AND @pOtherAccountId <> @pAccountId
  UPDATE   ACCOUNTS   
     SET   AC_TRACK_DATA =  '-RECYCLED-NEW-' + CAST (NEWID() AS NVARCHAR(50))
   WHERE   AC_ACCOUNT_ID = @pOtherAccountId


IF NOT EXISTS (SELECT 1 FROM ACCOUNTS WHERE ac_account_id = @pAccountId )  
BEGIN
  INSERT INTO   ACCOUNTS 
              ( AC_ACCOUNT_ID, AC_TYPE, AC_BLOCKED, AC_TRACK_DATA) 
       VALUES ( @pAccountId,         2,  @pBlocked, @pTrackData )
END  

 
  UPDATE   ACCOUNTS                   
     SET   AC_TRACK_DATA              = @pTrackData 
         , AC_HOLDER_NAME             = @pHolderName 
         , AC_HOLDER_ID               = @pHolderId 
         , AC_HOLDER_ID_TYPE          = @pHolderIdType 
         , AC_HOLDER_ADDRESS_01       = @pHolderAddress01 
         , AC_HOLDER_ADDRESS_02       = @pHolderAddress02 
         , AC_HOLDER_ADDRESS_03       = @pHolderAddress03 
         , AC_HOLDER_CITY             = @pHolderCity 
         , AC_HOLDER_ZIP              = @pHolderZip  
         , AC_HOLDER_EMAIL_01         = @pHolderEmail01 
         , AC_HOLDER_EMAIL_02         = @pHolderEmail02 
         , AC_HOLDER_TWITTER_ACCOUNT  = @pHolderTwitter 
         , AC_HOLDER_PHONE_NUMBER_01  = @pHolderPhoneNumber01 
         , AC_HOLDER_PHONE_NUMBER_02  = @pHolderPhoneNumber02 
         , AC_HOLDER_COMMENTS         = @pHolderComments 
         , AC_HOLDER_ID1              = @pHolderId1 
         , AC_HOLDER_ID2              = @pHolderId2 
         , AC_HOLDER_DOCUMENT_ID1     = @pHolderDocumentId1 
         , AC_HOLDER_DOCUMENT_ID2     = @pHolderDocumentId2 
         , AC_HOLDER_NAME1            = @pHolderName1 
         , AC_HOLDER_NAME2            = @pHolderName2 
         , AC_HOLDER_NAME3            = @pHolderName3 
         , AC_HOLDER_GENDER           = @pHolderGender  
         , AC_HOLDER_MARITAL_STATUS   = @pHolderMaritalStatus 
         , AC_HOLDER_BIRTH_DATE       = @pHolderBirthDate 
         , AC_HOLDER_WEDDING_DATE     = @pHolderWeddingDate 
         , AC_HOLDER_LEVEL            = @pHolderLevel 
         , AC_HOLDER_LEVEL_NOTIFY     = @pHolderLevelNotify 
         , AC_HOLDER_LEVEL_ENTERED    = @pHolderLevelEntered 
         , AC_HOLDER_LEVEL_EXPIRATION = @pHolderLevelExpiration 
         , AC_PIN                     = @pPin 
         , AC_PIN_FAILURES            = @pPinFailures 
         , AC_PIN_LAST_MODIFIED       = @pPinLastModified 
         , AC_BLOCKED                 = @pBlocked 
         , AC_ACTIVATED               = @pActivated 
         , AC_BLOCK_REASON            = @pBlockReason 
         , AC_HOLDER_IS_VIP           = @pHolderIsVip 
         , AC_HOLDER_TITLE            = @pHolderTitle 
         , AC_HOLDER_NAME4            = @pHolderName4         
         , AC_HOLDER_PHONE_TYPE_01    = @pHolderPhoneType01  
         , AC_HOLDER_PHONE_TYPE_02    = @pHolderPhoneType02  
         , AC_HOLDER_STATE            = @pHolderState    
         , AC_HOLDER_COUNTRY          = @pHolderCountry            
         , AC_MS_PERSONAL_INFO_SEQ_ID = @pPersonalInfoSequenceId           
         , AC_USER_TYPE                = @pUserType
         , AC_POINTS_STATUS           = @pPointsStatus
         , AC_MS_CHANGE_GUID          = AC_MS_CHANGE_GUID -- avoid trigger on update
         , AC_DEPOSIT                 = @pDeposit 
         , AC_CARD_PAID               = @pCardPay 
         , AC_BLOCK_DESCRIPTION       = @pBlockDescription                
         , AC_CREATED                 = @pCreated
         , AC_MS_CREATED_ON_SITE_ID   = @pMSCreatedOnSiteId
         , AC_MS_HASH                 = @pMSHash
         , AC_EXTERNAL_REFERENCE      = ISNULL(@pExternalReference,AC_EXTERNAL_REFERENCE)
         , AC_HOLDER_OCCUPATION       = @pHolderOccupation
         , AC_HOLDER_EXT_NUM          = @pHolderExtNum
         , AC_HOLDER_NATIONALITY      = @pHolderNationality 
         , AC_HOLDER_BIRTH_COUNTRY    = @pHolderBirthCountry 
         , AC_HOLDER_FED_ENTITY       = @pHolderFedEntity
         , AC_HOLDER_ID1_TYPE         = @pHolderId1Type 
         , AC_HOLDER_ID2_TYPE         = @pHolderId2Type 
         , AC_HOLDER_ID3_TYPE         = @pHolderId3Type 
         , AC_HOLDER_ID3              = @pHolderId3
         , AC_HOLDER_HAS_BENEFICIARY  = @pHolderHasBeneficiary 
         , AC_BENEFICIARY_NAME        = @pBeneficiaryName 
         , AC_BENEFICIARY_NAME1       = @pBeneficiaryName1
         , AC_BENEFICIARY_NAME2       = @pBeneficiaryName2
         , AC_BENEFICIARY_NAME3       = @pBeneficiaryName3
         , AC_BENEFICIARY_BIRTH_DATE  = @pBeneficiaryBirthDate 
         , AC_BENEFICIARY_GENDER      = @pBeneficiaryGender 
         , AC_BENEFICIARY_OCCUPATION  = @pBeneficiaryOccupation
         , AC_BENEFICIARY_ID1_TYPE    = @pBeneficiaryId1Type 
         , AC_BENEFICIARY_ID1         = @pBeneficiaryId1 
         , AC_BENEFICIARY_ID2_TYPE    = @pBeneficiaryId2Type 
         , AC_BENEFICIARY_ID2         = @pBeneficiaryId2 
         , AC_BENEFICIARY_ID3_TYPE    = @pBeneficiaryId3Type 
         , AC_BENEFICIARY_ID3         = @pBeneficiaryId3 
   WHERE   AC_ACCOUNT_ID              = @pAccountId 

END
GO

/****** TRIGGERS ******/

--------------------------------------------------------------------------------
-- Copyright � 2013 Win Systems International 
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: MultiSiteTrigger_SiteAccountUpdate.sql
-- 
--   DESCRIPTION: Procedures for trigger MultiSiteTrigger_SiteAccountUpdate and related issues
-- 
--        AUTHOR: Jos� Mart�nez
-- 
-- CREATION DATE: 07-MAR-2013
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 07-MAR-2013 JML    First release.
-- 22-MAY-2013 DDM    Fixed bug #793
--------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[MultiSiteTrigger_SiteAccountUpdate]') AND type in (N'TR'))
DROP TRIGGER [dbo].[MultiSiteTrigger_SiteAccountUpdate]
GO


CREATE TRIGGER [dbo].[MultiSiteTrigger_SiteAccountUpdate] ON [dbo].[accounts]
   AFTER UPDATE
NOT FOR REPLICATION
AS 
BEGIN
    DECLARE @AccountId          AS BIGINT
    DECLARE @hash0              as varbinary(20)
    DECLARE @hash1              as varbinary(20)
    DECLARE @value              as nvarchar(max)
    DECLARE @changed            as bit
    DECLARE @updated            AS BIT    
    
    IF (UPDATE(AC_MS_CHANGE_GUID))  
         return

  IF NOT EXISTS (SELECT   1 
                   FROM   GENERAL_PARAMS 
                  WHERE   GP_GROUP_KEY   = N'Site' 
                    AND   GP_SUBJECT_KEY = N'MultiSiteMember' 
                    AND   GP_KEY_VALUE   = N'1')
  BEGIN
        RETURN
  END

  SET @updated = 0;  

IF UPDATE (AC_TRACK_DATA)
    OR UPDATE (AC_HOLDER_NAME)
    OR UPDATE (AC_HOLDER_ID)
    OR UPDATE (AC_HOLDER_ID_TYPE)
    OR UPDATE (AC_HOLDER_ADDRESS_01)
    OR UPDATE (AC_HOLDER_ADDRESS_02)
    OR UPDATE (AC_HOLDER_ADDRESS_03)
    OR UPDATE (AC_HOLDER_CITY)
    OR UPDATE (AC_HOLDER_ZIP)
    OR UPDATE (AC_HOLDER_EMAIL_01)
    OR UPDATE (AC_HOLDER_EMAIL_02)
    OR UPDATE (AC_HOLDER_TWITTER_ACCOUNT)
    OR UPDATE (AC_HOLDER_PHONE_NUMBER_01)
    OR UPDATE (AC_HOLDER_PHONE_NUMBER_02)
    OR UPDATE (AC_HOLDER_COMMENTS)
    OR UPDATE (AC_HOLDER_ID1)
    OR UPDATE (AC_HOLDER_ID2)
    OR UPDATE (AC_HOLDER_DOCUMENT_ID1)
    OR UPDATE (AC_HOLDER_DOCUMENT_ID2)
    OR UPDATE (AC_HOLDER_NAME1)
    OR UPDATE (AC_HOLDER_NAME2)
    OR UPDATE (AC_HOLDER_NAME3)
    OR UPDATE (AC_HOLDER_GENDER)
    OR UPDATE (AC_HOLDER_MARITAL_STATUS)
    OR UPDATE (AC_HOLDER_BIRTH_DATE)
    OR UPDATE (AC_HOLDER_WEDDING_DATE)
    OR UPDATE (AC_HOLDER_LEVEL)
    OR UPDATE (AC_HOLDER_LEVEL_NOTIFY)
    OR UPDATE (AC_HOLDER_LEVEL_ENTERED)
    OR UPDATE (AC_HOLDER_LEVEL_EXPIRATION)
    OR UPDATE (AC_PIN)
    OR UPDATE (AC_PIN_FAILURES)
    OR UPDATE (AC_PIN_LAST_MODIFIED)
    OR UPDATE (AC_BLOCKED)
    OR UPDATE (AC_ACTIVATED)
    OR UPDATE (AC_BLOCK_REASON)
    OR UPDATE (AC_HOLDER_IS_VIP)
    OR UPDATE (AC_HOLDER_TITLE)
    OR UPDATE (AC_HOLDER_NAME4)
    OR UPDATE (AC_HOLDER_PHONE_TYPE_01)
    OR UPDATE (AC_HOLDER_PHONE_TYPE_02)
    OR UPDATE (AC_HOLDER_STATE)
    OR UPDATE (AC_HOLDER_COUNTRY)
    OR UPDATE (AC_USER_TYPE)
    OR UPDATE (AC_POINTS_STATUS)
    OR UPDATE (AC_DEPOSIT)
    OR UPDATE (AC_CARD_PAID)
    OR UPDATE (AC_BLOCK_DESCRIPTION)    
    OR UPDATE (AC_MS_CREATED_ON_SITE_ID)
    OR UPDATE (AC_EXTERNAL_REFERENCE)
    OR UPDATE (AC_HOLDER_OCCUPATION)     
    OR UPDATE (AC_HOLDER_EXT_NUM)       
    OR UPDATE (AC_HOLDER_NATIONALITY)
    OR UPDATE (AC_HOLDER_BIRTH_COUNTRY)
    OR UPDATE (AC_HOLDER_FED_ENTITY)
    OR UPDATE (AC_HOLDER_ID1_TYPE)
    OR UPDATE (AC_HOLDER_ID2_TYPE)
    OR UPDATE (AC_HOLDER_ID3_TYPE)
    OR UPDATE (AC_HOLDER_ID3)
    OR UPDATE (AC_HOLDER_HAS_BENEFICIARY)
    OR UPDATE (AC_BENEFICIARY_NAME)
    OR UPDATE (AC_BENEFICIARY_NAME1)
    OR UPDATE (AC_BENEFICIARY_NAME2)
    OR UPDATE (AC_BENEFICIARY_NAME3)
    OR UPDATE (AC_BENEFICIARY_BIRTH_DATE)
    OR UPDATE (AC_BENEFICIARY_GENDER)
    OR UPDATE (AC_BENEFICIARY_OCCUPATION)
    OR UPDATE (AC_BENEFICIARY_ID1_TYPE)
    OR UPDATE (AC_BENEFICIARY_ID1)
    OR UPDATE (AC_BENEFICIARY_ID2_TYPE)
    OR UPDATE (AC_BENEFICIARY_ID2)
    OR UPDATE (AC_BENEFICIARY_ID3_TYPE)
    OR UPDATE (AC_BENEFICIARY_ID3)
        SET @updated = 1;
        
    IF (@updated = 0) RETURN

    DECLARE PersonalInfoCursor CURSOR FOR 
     SELECT   AC_ACCOUNT_ID
            , HASHBYTES ('SHA1',  ISNULL(AC_TRACK_DATA,        '')
                                + ISNULL(AC_HOLDER_NAME,       '')
                                + ISNULL(AC_HOLDER_ID,         '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_ID_TYPE), '')
                                + ISNULL(AC_HOLDER_ADDRESS_01, '')
                                + ISNULL(AC_HOLDER_ADDRESS_02, '')
                                + ISNULL(AC_HOLDER_ADDRESS_03, '')
                                + ISNULL(AC_HOLDER_CITY,       '')
                                + ISNULL(AC_HOLDER_ZIP,        '')
                                + ISNULL(AC_HOLDER_EMAIL_01,   '')
                                + ISNULL(AC_HOLDER_EMAIL_02,   '')
                                + ISNULL(AC_HOLDER_TWITTER_ACCOUNT, '')
                                + ISNULL(AC_HOLDER_PHONE_NUMBER_01, '')
                                + ISNULL(AC_HOLDER_PHONE_NUMBER_02, '')
                                + ISNULL(AC_HOLDER_COMMENTS,        '')
                                + ISNULL(AC_HOLDER_ID1, '')
                                + ISNULL(AC_HOLDER_ID2, '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_DOCUMENT_ID1), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_DOCUMENT_ID2), '')
                                + ISNULL(AC_HOLDER_NAME1, '')
                                + ISNULL(AC_HOLDER_NAME2, '')
                                + ISNULL(AC_HOLDER_NAME3, '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_GENDER),         '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_MARITAL_STATUS), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_BIRTH_DATE,   21), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_WEDDING_DATE, 21), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_LEVEL),                '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_LEVEL_NOTIFY),         '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_LEVEL_ENTERED,    21), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_LEVEL_EXPIRATION, 21), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_PIN),          '')
                                + ISNULL(CONVERT(NVARCHAR, AC_PIN_FAILURES), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_PIN_LAST_MODIFIED, 21), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_BLOCKED),           '')
                                + ISNULL(CONVERT(NVARCHAR, AC_ACTIVATED),         '')
                                + ISNULL(CONVERT(NVARCHAR, AC_BLOCK_REASON),         '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_IS_VIP),        '')
                                + ISNULL(AC_HOLDER_TITLE                        , '')
                                + ISNULL(AC_HOLDER_NAME4                        , '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_PHONE_TYPE_01),        '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_PHONE_TYPE_02),        '')
                                + ISNULL(AC_HOLDER_STATE                        , '')
                                + ISNULL(AC_HOLDER_COUNTRY                      , '') 
                                + ISNULL(CONVERT(NVARCHAR,AC_USER_TYPE)         , '')
                                + ISNULL(CONVERT(NVARCHAR,AC_POINTS_STATUS)     , '') 
                                + ISNULL(CONVERT(NVARCHAR,AC_DEPOSIT)           , '')
                                + ISNULL(CONVERT(NVARCHAR,AC_CARD_PAID)         , '')
                                + ISNULL(AC_BLOCK_DESCRIPTION, '')
                                + ISNULL(CONVERT(NVARCHAR,AC_MS_CREATED_ON_SITE_ID), '')
                                + ISNULL(AC_EXTERNAL_REFERENCE, '')
                                + ISNULL(AC_HOLDER_OCCUPATION, '')
                                + ISNULL(AC_HOLDER_EXT_NUM, '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_NATIONALITY),         '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_BIRTH_COUNTRY), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_ID1_TYPE), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_ID2_TYPE), '')
                                + ISNULL(AC_HOLDER_ID3_TYPE, '')
                                + ISNULL(AC_HOLDER_ID3, '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_HAS_BENEFICIARY), '')
                                + ISNULL(AC_BENEFICIARY_NAME, '')
                                + ISNULL(AC_BENEFICIARY_NAME1, '')
                                + ISNULL(AC_BENEFICIARY_NAME2, '')
                                + ISNULL(AC_BENEFICIARY_NAME3, '')
                                + ISNULL(CONVERT(NVARCHAR, AC_BENEFICIARY_BIRTH_DATE,   21), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_BENEFICIARY_GENDER), '')  
                                + ISNULL(AC_BENEFICIARY_OCCUPATION, '')
                                + ISNULL(CONVERT(NVARCHAR, AC_BENEFICIARY_ID1_TYPE), '')        
                                + ISNULL(AC_BENEFICIARY_ID1, '')
                                + ISNULL(CONVERT(NVARCHAR, AC_BENEFICIARY_ID2_TYPE), '')        
                                + ISNULL(AC_BENEFICIARY_ID2, '')
                                + ISNULL(AC_BENEFICIARY_ID3_TYPE, '')        
                                + ISNULL(AC_BENEFICIARY_ID3, '') )
       FROM   INSERTED
      WHERE   AC_TRACK_DATA not like '%-NEW-%'

    SET NOCOUNT ON;

    OPEN PersonalInfoCursor

    FETCH NEXT FROM PersonalInfoCursor INTO @AccountId, @hash1

    WHILE @@FETCH_STATUS = 0
    BEGIN

        SELECT @hash0 = AC_MS_HASH FROM DELETED WHERE AC_ACCOUNT_ID = @AccountId

        SELECT @changed = CASE WHEN ( @hash0 = @hash1 ) THEN 0 ELSE 1 END

        IF @changed = 1
        BEGIN
           DECLARE @new_id as uniqueidentifier
           
           SET @new_id = NEWID()
           
            -- Personal Info
            UPDATE   ACCOUNTS
               SET   AC_MS_HASH                 = @hash1
                   , AC_MS_CHANGE_GUID          = @new_id
             WHERE   AC_ACCOUNT_ID              = @AccountId
            
            
            IF NOT EXISTS (SELECT 1 FROM MS_SITE_PENDING_ACCOUNTS WHERE SPA_ACCOUNT_ID = @AccountId)
              INSERT INTO   MS_SITE_PENDING_ACCOUNTS (SPA_ACCOUNT_ID, SPA_GUID) VALUES (@AccountId, @new_id )
            ELSE 
              UPDATE MS_SITE_PENDING_ACCOUNTS SET SPA_GUID = @new_id WHERE SPA_ACCOUNT_ID = @AccountId
            
            
        END

        FETCH NEXT FROM PersonalInfoCursor INTO @AccountId, @hash1
    END

    CLOSE PersonalInfoCursor
    DEALLOCATE PersonalInfoCursor

END
GO

--
-- Disable/enable triggers for site members
--

IF EXISTS (SELECT   1
             FROM   GENERAL_PARAMS 
            WHERE   GP_GROUP_KEY   = N'Site' 
              AND   GP_SUBJECT_KEY = N'MultiSiteMember' 
              AND   GP_KEY_VALUE   = N'1')
BEGIN
      EXEC MultiSiteTriggersEnable 1
END
ELSE
BEGIN
      EXEC MultiSiteTriggersEnable 0
END
GO

/****** RECORDS ******/

IF NOT EXISTS (SELECT * FROM currency_exchange WHERE ce_type = 0 AND ce_currency_iso_code = 'MXN')
BEGIN
  INSERT INTO [currency_exchange]
             ([ce_type]
             ,[ce_currency_iso_code]
             ,[ce_description]
             ,[ce_change]
             ,[ce_num_decimals]
             ,[ce_variable_commission]
             ,[ce_fixed_commission]
             ,[ce_variable_nr2]
             ,[ce_fixed_nr2]
             ,[ce_status])
       VALUES
             ( 0
             , 'MXN'
             , 'Peso Mexicano'
             , 1
             , 2
             , 0
             , 0
             , 0
             , 0
             , 1 )
END
GO

IF NOT EXISTS (SELECT * FROM currency_exchange WHERE ce_type = 1 AND ce_currency_iso_code = 'MXN')
BEGIN
  INSERT INTO [currency_exchange]
             ([ce_type]
             ,[ce_currency_iso_code]
             ,[ce_description]
             ,[ce_change]
             ,[ce_num_decimals]
             ,[ce_variable_commission]
             ,[ce_fixed_commission]
             ,[ce_variable_nr2]
             ,[ce_fixed_nr2]
             ,[ce_status])
       VALUES
             ( 1
             , 'MXN'
             , 'Tarjeta Bancaria'
             , 1
             , 2
             , 0
             , 0
             , 0
             , 0
             , 0 )
END
GO

--
-- MultiSite: DownloadCommonUsers
-- 

INSERT INTO MS_SITE_TASKS (ST_TASK_ID, ST_ENABLED, ST_INTERVAL_SECONDS, ST_MAX_ROWS_TO_UPLOAD) values (61, 1, 600,200); -- DownloadCommonUsers
GO

UPDATE MS_SITE_TASKS SET ST_MAX_ROWS_TO_UPLOAD = 1 WHERE ST_TASK_ID = 60 -- DownloadMasterProfiles
GO

--
-- 
-- 

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_plays_play_sessions]') AND parent_object_id = OBJECT_ID(N'[dbo].[plays]'))
  ALTER TABLE [dbo].[plays] DROP CONSTRAINT [FK_plays_play_sessions]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_play_sessions_accounts]') AND parent_object_id = OBJECT_ID(N'[dbo].[play_sessions]'))
  ALTER TABLE [dbo].[play_sessions] DROP CONSTRAINT [FK_play_sessions_accounts]
GO

--
-- GENERAL PARAMS
--

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PSAClient' AND GP_SUBJECT_KEY ='ProxyAddress')
  INSERT INTO [dbo].[general_params] ([gp_group_key] ,[gp_subject_key] ,[gp_key_value]) VALUES ('PSAClient' ,'ProxyAddress' ,'');
GO

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'SiteJackpot' AND GP_SUBJECT_KEY = 'PaymentMode')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('SiteJackpot', 'PaymentMode', '0');

--
-- GENERAL PARAMS: AntiMoneyLaundering
--

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Account.RequestedField' AND GP_SUBJECT_KEY = 'DocumentTypeList')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Account.RequestedField', 'DocumentTypeList', '');
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Account.RequestedField' AND GP_SUBJECT_KEY = 'DocScan')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Account.RequestedField', 'DocScan', '0');
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Account.RequestedField' AND GP_SUBJECT_KEY = 'DocScanTypeList')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Account.RequestedField', 'DocScanTypeList', '');
GO

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Account.RequestedField' AND GP_SUBJECT_KEY = 'BirthCountry')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Account.RequestedField', 'BirthCountry', '0');
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Account.RequestedField' AND GP_SUBJECT_KEY = 'Nationality')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Account.RequestedField', 'Nationality', '0');
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Account.RequestedField' AND GP_SUBJECT_KEY = 'Occupation')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Account.RequestedField', 'Occupation', '0');
GO

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Account.RequestedField' AND GP_SUBJECT_KEY = 'AntiMoneyLaundering.BirthCountry')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Account.RequestedField', 'AntiMoneyLaundering.BirthCountry', '0');
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Account.RequestedField' AND GP_SUBJECT_KEY = 'AntiMoneyLaundering.BirthDate')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Account.RequestedField', 'AntiMoneyLaundering.BirthDate', '1');
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Account.RequestedField' AND GP_SUBJECT_KEY = 'AntiMoneyLaundering.Document')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Account.RequestedField', 'AntiMoneyLaundering.Document', '1');
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Account.RequestedField' AND GP_SUBJECT_KEY = 'AntiMoneyLaundering.DocumentTypeList')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Account.RequestedField', 'AntiMoneyLaundering.DocumentTypeList', '');
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Account.RequestedField' AND GP_SUBJECT_KEY = 'AntiMoneyLaundering.DocScan')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Account.RequestedField', 'AntiMoneyLaundering.DocScan', '0');
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Account.RequestedField' AND GP_SUBJECT_KEY = 'AntiMoneyLaundering.DocScanTypeList')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Account.RequestedField', 'AntiMoneyLaundering.DocScanTypeList', '');
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Account.RequestedField' AND GP_SUBJECT_KEY = 'AntiMoneyLaundering.Email1')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Account.RequestedField', 'AntiMoneyLaundering.Email1', '0');
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Account.RequestedField' AND GP_SUBJECT_KEY = 'AntiMoneyLaundering.Gender')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Account.RequestedField', 'AntiMoneyLaundering.Gender', '1');
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Account.RequestedField' AND GP_SUBJECT_KEY = 'AntiMoneyLaundering.Name1')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Account.RequestedField', 'AntiMoneyLaundering.Name1', '1');
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Account.RequestedField' AND GP_SUBJECT_KEY = 'AntiMoneyLaundering.Name2')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Account.RequestedField', 'AntiMoneyLaundering.Name2', '0');
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Account.RequestedField' AND GP_SUBJECT_KEY = 'AntiMoneyLaundering.Name3')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Account.RequestedField', 'AntiMoneyLaundering.Name3', '1');
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Account.RequestedField' AND GP_SUBJECT_KEY = 'AntiMoneyLaundering.Nationality')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Account.RequestedField', 'AntiMoneyLaundering.Nationality', '0');
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Account.RequestedField' AND GP_SUBJECT_KEY = 'AntiMoneyLaundering.Occupation')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Account.RequestedField', 'AntiMoneyLaundering.Occupation', '0');
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Account.RequestedField' AND GP_SUBJECT_KEY = 'AntiMoneyLaundering.Phone1')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Account.RequestedField', 'AntiMoneyLaundering.Phone1', '0');
GO

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Beneficiary.RequestedField' AND GP_SUBJECT_KEY = 'Document')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Beneficiary.RequestedField', 'Document', '1');
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Beneficiary.RequestedField' AND GP_SUBJECT_KEY = 'DocumentTypeList')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Beneficiary.RequestedField', 'DocumentTypeList', '');
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Beneficiary.RequestedField' AND GP_SUBJECT_KEY = 'Name1')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Beneficiary.RequestedField', 'Name1', '1');
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Beneficiary.RequestedField' AND GP_SUBJECT_KEY = 'Name2')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Beneficiary.RequestedField', 'Name2', '0');
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Beneficiary.RequestedField' AND GP_SUBJECT_KEY = 'Name3')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Beneficiary.RequestedField', 'Name3', '1');
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Beneficiary.RequestedField' AND GP_SUBJECT_KEY = 'BirthDate')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Beneficiary.RequestedField', 'BirthDate', '1');
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Beneficiary.RequestedField' AND GP_SUBJECT_KEY = 'Gender')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Beneficiary.RequestedField', 'Gender', '1');
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Beneficiary.RequestedField' AND GP_SUBJECT_KEY = 'Occupation')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Beneficiary.RequestedField', 'Occupation', '0');
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Beneficiary.RequestedField' AND GP_SUBJECT_KEY = 'DocScan')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Beneficiary.RequestedField', 'DocScan', '0');
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Beneficiary.RequestedField' AND GP_SUBJECT_KEY = 'DocScanTypeList')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Beneficiary.RequestedField', 'DocScanTypeList', '');
GO

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Beneficiary.RequestedField' AND GP_SUBJECT_KEY = 'AntiMoneyLaundering.Document')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Beneficiary.RequestedField', 'AntiMoneyLaundering.Document', '1');
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Beneficiary.RequestedField' AND GP_SUBJECT_KEY = 'AntiMoneyLaundering.DocumentTypeList')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Beneficiary.RequestedField', 'AntiMoneyLaundering.DocumentTypeList', '');
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Beneficiary.RequestedField' AND GP_SUBJECT_KEY = 'AntiMoneyLaundering.Name1')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Beneficiary.RequestedField', 'AntiMoneyLaundering.Name1', '1');
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Beneficiary.RequestedField' AND GP_SUBJECT_KEY = 'AntiMoneyLaundering.Name2')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Beneficiary.RequestedField', 'AntiMoneyLaundering.Name2', '0');
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Beneficiary.RequestedField' AND GP_SUBJECT_KEY = 'AntiMoneyLaundering.Name3')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Beneficiary.RequestedField', 'AntiMoneyLaundering.Name3', '1');
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Beneficiary.RequestedField' AND GP_SUBJECT_KEY = 'AntiMoneyLaundering.BirthDate')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Beneficiary.RequestedField', 'AntiMoneyLaundering.BirthDate', '1');
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Beneficiary.RequestedField' AND GP_SUBJECT_KEY = 'AntiMoneyLaundering.Gender')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Beneficiary.RequestedField', 'AntiMoneyLaundering.Gender', '1');
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Beneficiary.RequestedField' AND GP_SUBJECT_KEY = 'AntiMoneyLaundering.Occupation')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Beneficiary.RequestedField', 'AntiMoneyLaundering.Occupation', '0');
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Beneficiary.RequestedField' AND GP_SUBJECT_KEY = 'AntiMoneyLaundering.DocScan')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Beneficiary.RequestedField', 'AntiMoneyLaundering.DocScan', '0');
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Beneficiary.RequestedField' AND GP_SUBJECT_KEY = 'AntiMoneyLaundering.DocScanTypeList')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Beneficiary.RequestedField', 'AntiMoneyLaundering.DocScanTypeList', '');
GO

DECLARE @AntiMoneyLaunderingEnabled AS INT
SELECT @AntiMoneyLaunderingEnabled = GP_KEY_VALUE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'MoneyLaundering' AND GP_SUBJECT_KEY = 'Enabled'
SET @AntiMoneyLaunderingEnabled = ISNULL(@AntiMoneyLaunderingEnabled, 0)

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'AntiMoneyLaundering' AND GP_SUBJECT_KEY = 'Enabled')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('AntiMoneyLaundering', 'Enabled', @AntiMoneyLaunderingEnabled);
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'AntiMoneyLaundering' AND GP_SUBJECT_KEY = 'BaseAmount')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('AntiMoneyLaundering', 'BaseAmount', '64.76');
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'AntiMoneyLaundering' AND GP_SUBJECT_KEY = 'BaseName')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('AntiMoneyLaundering', 'BaseName', 'SMVGDF');
GO

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'AntiMoneyLaundering' AND GP_SUBJECT_KEY = 'Recharge.Identification.Limit')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('AntiMoneyLaundering', 'Recharge.Identification.Limit', '325');
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'AntiMoneyLaundering' AND GP_SUBJECT_KEY = 'Recharge.Identification.Message')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('AntiMoneyLaundering', 'Recharge.Identification.Message', 'El cliente ha superado el l�mite 325 SMVGFD. Por la ley antilavado de dinero, debe identificarse.');
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'AntiMoneyLaundering' AND GP_SUBJECT_KEY = 'Recharge.Identification.Warning.Limit')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('AntiMoneyLaundering', 'Recharge.Identification.Warning.Limit', '310');
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'AntiMoneyLaundering' AND GP_SUBJECT_KEY = 'Recharge.Identification.Warning.Message')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('AntiMoneyLaundering', 'Recharge.Identification.Warning.Message', 'Por la ley antilavado de dinero, el cliente deber� identificarse en pr�ximas recargas.');
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'AntiMoneyLaundering' AND GP_SUBJECT_KEY = 'Recharge.Identification.DontAllowMB')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('AntiMoneyLaundering', 'Recharge.Identification.DontAllowMB', '0'); 
GO

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'AntiMoneyLaundering' AND GP_SUBJECT_KEY = 'Recharge.Report.Limit')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('AntiMoneyLaundering', 'Recharge.Report.Limit', '645');
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'AntiMoneyLaundering' AND GP_SUBJECT_KEY = 'Recharge.Report.Message')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('AntiMoneyLaundering', 'Recharge.Report.Message', 'Por la ley antilavado de dinero, el cliente va a ser inclu�do en un reporte para el SAT.');
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'AntiMoneyLaundering' AND GP_SUBJECT_KEY = 'Recharge.Report.Warning.Limit')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('AntiMoneyLaundering', 'Recharge.Report.Warning.Limit', '625');
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'AntiMoneyLaundering' AND GP_SUBJECT_KEY = 'Recharge.Report.Warning.Message')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('AntiMoneyLaundering', 'Recharge.Report.Warning.Message', 'Por la ley antilavado de dinero, el cliente ser� reportado al SAT en pr�ximas recargas.');
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'AntiMoneyLaundering' AND GP_SUBJECT_KEY = 'Recharge.Report.DontAllowMB')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('AntiMoneyLaundering', 'Recharge.Report.DontAllowMB', '0'); 
GO

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'AntiMoneyLaundering' AND GP_SUBJECT_KEY = 'Prize.Identification.Limit')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('AntiMoneyLaundering', 'Prize.Identification.Limit', '325');
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'AntiMoneyLaundering' AND GP_SUBJECT_KEY = 'Prize.Identification.Message')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('AntiMoneyLaundering', 'Prize.Identification.Message', 'El cliente ha superado el l�mite 325 SMVGFD. Por la ley antilavado de dinero, debe identificarse.');
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'AntiMoneyLaundering' AND GP_SUBJECT_KEY = 'Prize.Identification.Warning.Limit')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('AntiMoneyLaundering', 'Prize.Identification.Warning.Limit', '310');
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'AntiMoneyLaundering' AND GP_SUBJECT_KEY = 'Prize.Identification.Warning.Message')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('AntiMoneyLaundering', 'Prize.Identification.Warning.Message', 'Por la ley antilavado de dinero, el cliente deber� identificarse en pr�ximos retiros.');
GO

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'AntiMoneyLaundering' AND GP_SUBJECT_KEY = 'Prize.Report.Limit')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('AntiMoneyLaundering', 'Prize.Report.Limit', '645');
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'AntiMoneyLaundering' AND GP_SUBJECT_KEY = 'Prize.Report.Message')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('AntiMoneyLaundering', 'Prize.Report.Message', 'Por la ley antilavado de dinero, el cliente va a ser inclu�do en un reporte para el SAT.');
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'AntiMoneyLaundering' AND GP_SUBJECT_KEY = 'Prize.Report.Warning.Limit')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('AntiMoneyLaundering', 'Prize.Report.Warning.Limit', '625');
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'AntiMoneyLaundering' AND GP_SUBJECT_KEY = 'Prize.Report.Warning.Message')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('AntiMoneyLaundering', 'Prize.Report.Warning.Message', 'Por la ley antilavado de dinero, el cliente ser� reportado al SAT en pr�ximos retiros.');
GO

DECLARE @ScanCountDown AS INT
SELECT @ScanCountDown = GP_KEY_VALUE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'MoneyLaundering' AND GP_SUBJECT_KEY = 'ScanCountDown'
SET @ScanCountDown = ISNULL(@ScanCountDown, 30)

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY = 'ScanCountDown')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Cashier', 'ScanCountDown', @ScanCountDown);
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY = 'MaxAllowedScanDocs')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Cashier', 'MaxAllowedScanDocs', '4');
GO

---
--- DELETE OLD GP
---

DELETE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'MoneyLaundering'
GO

---
---
---
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PlayerTracking' AND GP_SUBJECT_KEY ='MaxDaysWithoutActivity')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key]
             ,[gp_subject_key]
             ,[gp_key_value])
       VALUES
             ('PlayerTracking'
             ,'MaxDaysWithoutActivity'
             ,'90');
GO

--
--  CURRENCY EXCHANGE AND CREDIT CARD RECHARGES
--
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'RegionalOptions' AND GP_SUBJECT_KEY = 'CurrenciesAccepted')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('RegionalOptions', 'CurrenciesAccepted', 'MXN;')
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'RegionalOptions' AND GP_SUBJECT_KEY = 'CurrencyISOCode')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('RegionalOptions', 'CurrencyISOCode', 'MXN')
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.Voucher' AND GP_SUBJECT_KEY = 'CurrencyExchange.HideVoucher')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Cashier.Voucher', 'CurrencyExchange.HideVoucher', '0')

DECLARE @expiration_value AS INT

SET @expiration_value = 30

IF NOT EXISTS (SELECT * FROM promotions WHERE pm_type = 11)
BEGIN
  INSERT INTO [dbo].[promotions]
         ([pm_name]
         ,[pm_enabled]
         ,[pm_type]
         ,[pm_date_start]
         ,[pm_date_finish]
         ,[pm_schedule_weekday]
         ,[pm_schedule1_time_from]
         ,[pm_schedule1_time_to]
         ,[pm_schedule2_enabled]
         ,[pm_schedule2_time_from]
         ,[pm_schedule2_time_to]
         ,[pm_gender_filter]
         ,[pm_birthday_filter]
         ,[pm_expiration_type]
         ,[pm_expiration_value]
         ,[pm_min_cash_in]
         ,[pm_min_cash_in_reward]
         ,[pm_cash_in]
         ,[pm_cash_in_reward]
         ,[pm_num_tokens]
         ,[pm_token_name]
         ,[pm_token_reward]
         ,[pm_level_filter]
         ,[pm_permission]
         ,[pm_freq_filter_last_days]
         ,[pm_freq_filter_min_days]
         ,[pm_freq_filter_min_cash_in]
         ,[pm_min_spent]
         ,[pm_min_spent_reward]
         ,[pm_spent]
         ,[pm_spent_reward]
         ,[pm_min_played]
         ,[pm_min_played_reward]
         ,[pm_played]
         ,[pm_played_reward]
         ,[pm_next_execution]
         ,[pm_credit_type]
         ,[pm_ticket_footer]
         ,[pm_category_id]
         ,[pm_visible_on_promobox]
         ,[pm_expiration_limit])
     VALUES
         ('Promoci�n Divisa / Tarjeta'
         , 1
         , 11
         , CAST('01-01-2010 00:00:00' as DATETIME)
         , CAST('01-01-2100 00:00:00' as DATETIME)
         , 127
         , 0
         , 0
         , 0
         , 0
         , 0
         , 0
         , 0
         , 1
         , @expiration_value
         , 0
         , 0
         , 0
         , 0
         , 0
         , ''
         , 0
         , 0
         , 0
         , 0
         , 0
         , 0
         , 0
         , 0
         , 0
         , 0
         , 0
         , 0
         , 0
         , 0
         , 0
         , 2
         , ''
         , 0
         , 0
         , CAST('01-01-2100 00:00:00' as DATETIME)
         )
END
GO

--
-- NEW TYPE OF DOCUMENT SCANNED
--
UPDATE ACCOUNTS SET ac_holder_id3_type      = CASE WHEN cast(ac_holder_id3_type as INT)      < 0 THEN NULL ELSE REPLACE(STR(ac_holder_id3_type, 3), SPACE(1), '0')      END WHERE ac_holder_id3_type      IS NOT NULL
UPDATE ACCOUNTS SET ac_beneficiary_id3_type = CASE WHEN cast(ac_beneficiary_id3_type as INT) < 0 THEN NULL ELSE REPLACE(STR(ac_beneficiary_id3_type, 3), SPACE(1), '0') END WHERE ac_beneficiary_id3_type IS NOT NULL 
GO

---
--- SAVE ACCOUNT_DOCUMENTS 
---
SELECT * INTO account_documents_backup FROM ACCOUNT_DOCUMENTS 
GO

SELECT   AC_ACCOUNT_ID
       , ISNULL(AC_HOLDER_ID3_TYPE,      '') AC_HOLDER_ID3_TYPE
       , ISNULL(AC_BENEFICIARY_ID3_TYPE, '') AC_BENEFICIARY_ID3_TYPE
  INTO   account_documents_to_upgrade
  FROM   ACCOUNTS
 INNER   JOIN ACCOUNT_DOCUMENTS ON AD_ACCOUNT_ID = AC_ACCOUNT_ID
GO