/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 247;

SET @New_ReleaseId = 248;
SET @New_ScriptName = N'UpdateTo_18.248.037.sql';
SET @New_Description = N'Added new fields for handpays,currency_exchange,gui_audit,terminals;Added new alarms catalog;Added new SP iStatsCashierMovements, update SP GT_Cancellations';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/******* TABLES *******/

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[currency_exchange]') and name = 'ce_formatted_decimals')
	ALTER TABLE [dbo].[currency_exchange] ADD [ce_formatted_decimals] INT NULL
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[gui_audit]') and name = 'ga_related_type')
	ALTER TABLE [dbo].[gui_audit] ADD [ga_related_type] INT NULL, [ga_related_id] BIGINT NULL
GO

IF  NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[terminals]') and name = 'te_game_theme')
	ALTER TABLE [dbo].[terminals] ADD te_game_theme NVARCHAR(50) NULL
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[tickets]') and name = 'ti_account_promotion')
	ALTER TABLE [dbo].[tickets] ADD [ti_account_promotion] [bigint] NULL
GO

--
--
--
IF NOT EXISTS(SELECT * FROM sys.columns WHERE NAME = N'hp_tax_base_amount' AND OBJECT_ID = OBJECT_ID(N'handpays'))
  ALTER TABLE [dbo].[handpays] ADD hp_tax_base_amount MONEY NULL
GO

IF NOT EXISTS(SELECT * FROM sys.columns WHERE NAME = N'hp_tax_amount' AND OBJECT_ID = OBJECT_ID(N'handpays'))
  ALTER TABLE [dbo].[handpays] ADD hp_tax_amount MONEY NULL
GO

IF NOT EXISTS(SELECT * FROM sys.columns WHERE NAME = N'hp_tax_pct' AND OBJECT_ID = OBJECT_ID(N'handpays'))
  ALTER TABLE [dbo].[handpays] ADD hp_tax_pct DECIMAL NULL
GO

/****** INSERT TABLE LCD_MESSAGES ******/
IF NOT EXISTS (SELECT * FROM SYS.OBJECTS WHERE TYPE = 'U' AND NAME = 'lcd_messages')
BEGIN
       CREATE TABLE [dbo].[lcd_messages](
             [msg_unique_id] [bigint] NOT NULL,
             [msg_type] [int] NOT NULL,
             [msg_site_list] [xml] NULL,
             [msg_terminal_list] [xml] NULL,
             [msg_account_list] [xml] NULL,
             [msg_enabled] [bit] NOT NULL,
             [msg_order] [int] NOT NULL,
             [msg_schedule_start] [datetime] NOT NULL,
             [msg_schedule_end] [datetime] NULL,
             [msg_schedule_weekday] [int] NOT NULL,
             [msg_schedule1_time_from] [int] NOT NULL,
             [msg_schedule1_time_to] [int] NOT NULL,
             [msg_schedule2_enabled] [bit] NOT NULL,
             [msg_schedule2_time_from] [int] NULL,
             [msg_schedule2_time_to] [int] NULL,
             [msg_message] [xml] NOT NULL,
             [msg_resource_id] [bigint] NOT NULL,
             [msg_timestamp] [timestamp] NULL,
             [msg_master_sequence_id] [bigint] NULL,
             [msg_computed_order]  AS (case when isnull([MSG_MASTER_SEQUENCE_ID],(0))=(0) then [MSG_ORDER] else (1000000000)+[MSG_ORDER] end),
       CONSTRAINT [PK_lcd_messages] PRIMARY KEY CLUSTERED 
        (
             [msg_unique_id] ASC
       )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
       ) ON [PRIMARY];
       ALTER TABLE [dbo].[lcd_messages] ADD  CONSTRAINT [DF_lcd_messages_msg_order]  DEFAULT ((0)) FOR [msg_order];
END
ELSE
SELECT '***** Table lcd_messages already exists *****';
GO

/******* INDEXES *******/

IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[gui_audit]') AND name = N'IX_gui_related_type_related_id')
BEGIN
CREATE NONCLUSTERED INDEX [IX_gui_related_type_related_id] ON [dbo].[gui_audit] 
(
      [ga_related_type] ASC, [ga_related_id] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT name FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[tickets]') AND name = N'IX_ti_canceled_play_session_id_type_id')
CREATE  NONCLUSTERED  INDEX [IX_ti_canceled_play_session_id_type_id] ON [dbo].[tickets] 
(
      TI_CANCELED_PLAY_SESSION_ID ASC,
      TI_TYPE_ID ASC
) WITH ( PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

/****** INSERT INDEX IX_msg_enabled_start ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[lcd_messages]') AND name = N'IX_msg_enabled_start')
DROP INDEX [IX_msg_enabled_start] ON [dbo].[lcd_messages] WITH ( ONLINE = OFF )
GO
CREATE NONCLUSTERED INDEX [IX_msg_enabled_start] ON [dbo].[lcd_messages] 
(
       [msg_enabled] ASC,
       [msg_schedule_start] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

/******* RECORDS *******/

-- 
-- COVER COUPON
--
UPDATE   GENERAL_PARAMS SET GP_GROUP_KEY = 'Cashier.CoverCoupon', GP_SUBJECT_KEY = 'RangePercentage' 
 WHERE   GP_GROUP_KEY      = 'Cashier' 
   AND   GP_SUBJECT_KEY    = 'CoverCouponPct'
GO

DELETE FROM   GENERAL_PARAMS 
      WHERE   GP_GROUP_KEY = 'Cashier' 
        AND   GP_SUBJECT_KEY = 'CoverCouponExpireAfterDays'
GO

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.CoverCoupon' AND GP_SUBJECT_KEY = 'Enabled')
	INSERT INTO   [dbo].[general_params] ([gp_group_key],[gp_subject_key],[gp_key_value])
         SELECT   'Cashier.CoverCoupon'
	            , 'Enabled' 
	            ,  CASE WHEN ISNUMERIC(GP_KEY_VALUE) = 1 THEN 
                       CASE WHEN CAST(GP_KEY_VALUE AS DECIMAL) > 0 AND CAST(GP_KEY_VALUE AS DECIMAL) <= 100 THEN 1
                       ELSE 0 END 
                   ELSE 0
                   END
           FROM  GENERAL_PARAMS 
          WHERE  GP_GROUP_KEY    = 'Cashier.CoverCoupon' 
            AND  GP_SUBJECT_KEY = 'RangePercentage'
GO

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.CoverCoupon' AND GP_SUBJECT_KEY = 'OnlyFirstDailyRecharge')
	INSERT INTO [dbo].[general_params] ([gp_group_key],[gp_subject_key],[gp_key_value])
                                VALUES ('Cashier.CoverCoupon' ,'OnlyFirstDailyRecharge','0')
GO

IF NOT EXISTS(SELECT 1 FROM sas_meters_groups WHERE smg_group_id = 3)
  INSERT INTO sas_meters_groups VALUES (3, 'Meters reported', 'Meters reported', 0)
GO
  
IF NOT EXISTS(SELECT 1 FROM sas_meters_catalog_per_group WHERE smcg_group_id = 10002 AND smcg_meter_code = 11)
  INSERT INTO sas_meters_catalog_per_group VALUES (10002, 11)
GO

IF NOT EXISTS(SELECT 1 FROM sas_meters_catalog_per_group WHERE smcg_group_id = 3 AND smcg_meter_code = 0)
  INSERT INTO sas_meters_catalog_per_group VALUES (3, 0)
GO
  
IF NOT EXISTS(SELECT 1 FROM sas_meters_catalog_per_group WHERE smcg_group_id = 3 AND smcg_meter_code = 1)
  INSERT INTO sas_meters_catalog_per_group VALUES (3, 1)
GO

IF NOT EXISTS(SELECT 1 FROM sas_meters_catalog_per_group WHERE smcg_group_id = 3 AND smcg_meter_code = 2)
  INSERT INTO sas_meters_catalog_per_group VALUES (3, 2)
GO

IF NOT EXISTS(SELECT 1 FROM sas_meters_catalog_per_group WHERE smcg_group_id = 3 AND smcg_meter_code = 3)
  INSERT INTO sas_meters_catalog_per_group VALUES (3, 3)
GO

IF NOT EXISTS(SELECT 1 FROM sas_meters_catalog_per_group WHERE smcg_group_id = 3 AND smcg_meter_code = 5)
  INSERT INTO sas_meters_catalog_per_group VALUES (3, 5)
GO

IF NOT EXISTS(SELECT 1 FROM sas_meters_catalog_per_group WHERE smcg_group_id = 3 AND smcg_meter_code = 6)
  INSERT INTO sas_meters_catalog_per_group VALUES (3, 6)
GO

IF NOT EXISTS(SELECT 1 FROM sas_meters_catalog_per_group WHERE smcg_group_id = 3 AND smcg_meter_code = 11)
  INSERT INTO sas_meters_catalog_per_group VALUES (3, 11)
GO

IF NOT EXISTS(SELECT 1 FROM sas_meters_catalog_per_group WHERE smcg_group_id = 3 AND smcg_meter_code = 12)
  INSERT INTO sas_meters_catalog_per_group VALUES (3, 12)
GO

IF NOT EXISTS(SELECT 1 FROM sas_meters_catalog_per_group WHERE smcg_group_id = 3 AND smcg_meter_code = 128)
  INSERT INTO sas_meters_catalog_per_group VALUES (3, 128)
GO

IF NOT EXISTS(SELECT 1 FROM sas_meters_catalog_per_group WHERE smcg_group_id = 3 AND smcg_meter_code = 129)
  INSERT INTO sas_meters_catalog_per_group VALUES (3, 129)
GO

IF NOT EXISTS(SELECT 1 FROM sas_meters_catalog_per_group WHERE smcg_group_id = 3 AND smcg_meter_code = 130)
  INSERT INTO sas_meters_catalog_per_group VALUES (3, 130)
GO

IF NOT EXISTS(SELECT 1 FROM sas_meters_catalog_per_group WHERE smcg_group_id = 3 AND smcg_meter_code = 131)
  INSERT INTO sas_meters_catalog_per_group VALUES (3, 131)
GO

IF NOT EXISTS(SELECT 1 FROM sas_meters_catalog_per_group WHERE smcg_group_id = 3 AND smcg_meter_code = 132)
  INSERT INTO sas_meters_catalog_per_group VALUES (3, 132)
GO
  
IF NOT EXISTS(SELECT 1 FROM sas_meters_catalog_per_group WHERE smcg_group_id = 3 AND smcg_meter_code = 133)
  INSERT INTO sas_meters_catalog_per_group VALUES (3, 133)
GO
  
IF NOT EXISTS(SELECT 1 FROM sas_meters_catalog_per_group WHERE smcg_group_id = 3 AND smcg_meter_code = 134)
  INSERT INTO sas_meters_catalog_per_group VALUES (3, 134)
GO

IF NOT EXISTS(SELECT 1 FROM sas_meters_catalog_per_group WHERE smcg_group_id = 3 AND smcg_meter_code = 135)
  INSERT INTO sas_meters_catalog_per_group VALUES (3, 135)
GO

IF NOT EXISTS(SELECT 1 FROM sas_meters_catalog_per_group WHERE smcg_group_id = 3 AND smcg_meter_code = 136)
  INSERT INTO sas_meters_catalog_per_group VALUES (3, 136)
GO

IF NOT EXISTS(SELECT 1 FROM sas_meters_catalog_per_group WHERE smcg_group_id = 3 AND smcg_meter_code = 137)
  INSERT INTO sas_meters_catalog_per_group VALUES (3, 137)
GO

IF NOT EXISTS(SELECT 1 FROM sas_meters_catalog_per_group WHERE smcg_group_id = 3 AND smcg_meter_code = 138)
  INSERT INTO sas_meters_catalog_per_group VALUES (3, 138)
GO
  
IF NOT EXISTS(SELECT 1 FROM sas_meters_catalog_per_group WHERE smcg_group_id = 3 AND smcg_meter_code = 139)
  INSERT INTO sas_meters_catalog_per_group VALUES (3, 139)
GO
  
IF NOT EXISTS(SELECT 1 FROM sas_meters_catalog_per_group WHERE smcg_group_id = 3 AND smcg_meter_code = 160)
  INSERT INTO sas_meters_catalog_per_group VALUES (3, 160)
GO

IF NOT EXISTS(SELECT 1 FROM sas_meters_catalog_per_group WHERE smcg_group_id = 3 AND smcg_meter_code = 161)
  INSERT INTO sas_meters_catalog_per_group VALUES (3, 161)
GO
  
IF NOT EXISTS(SELECT 1 FROM sas_meters_catalog_per_group WHERE smcg_group_id = 3 AND smcg_meter_code = 184)
  INSERT INTO sas_meters_catalog_per_group VALUES (3, 184)
GO
  
IF NOT EXISTS(SELECT 1 FROM sas_meters_catalog_per_group WHERE smcg_group_id = 3 AND smcg_meter_code = 185)
  INSERT INTO sas_meters_catalog_per_group VALUES (3, 185)
GO
  
IF NOT EXISTS(SELECT 1 FROM sas_meters_catalog_per_group WHERE smcg_group_id = 3 AND smcg_meter_code = 4096)
  INSERT INTO sas_meters_catalog_per_group VALUES (3, 4096)
GO

DECLARE @meter_code as INT
DECLARE bill_cursor CURSOR FOR SELECT smcg_meter_code FROM sas_meters_catalog_per_group where smcg_group_id = 10002
OPEN bill_cursor
FETCH NEXT FROM bill_cursor INTO @meter_code

WHILE @@FETCH_STATUS = 0
  BEGIN
    IF NOT EXISTS(SELECT 1 FROM sas_meters_catalog_per_group WHERE smcg_group_id = 3 AND smcg_meter_code = @meter_code)
      INSERT INTO sas_meters_catalog_per_group VALUES (3, @meter_code)

      FETCH NEXT FROM bill_cursor INTO @meter_code
  END

CLOSE bill_cursor
DEALLOCATE bill_cursor
GO

-- Enable Commission to Company B functionality
IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.PaymentMethod' AND GP_SUBJECT_KEY = 'CommissionToCompanyB.Enabled')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Cashier.PaymentMethod', 'CommissionToCompanyB.Enabled', '0')
GO

-- Commission bank card
IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.PaymentMethod' AND GP_SUBJECT_KEY = 'CommissionToCompanyB.BankCard.VoucherTitle')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Cashier.PaymentMethod', 'CommissionToCompanyB.BankCard.VoucherTitle', 'Comisi�n uso de tarjeta')
GO

IF NOT EXISTS (SELECT * FROM SAS_METERS_CATALOG_PER_GROUP WHERE smcg_group_id = 10001 AND smcg_meter_code = 25)
	INSERT INTO [dbo].[sas_meters_catalog_per_group] ([smcg_group_id],[smcg_meter_code]) VALUES (10001,25);
GO

IF NOT EXISTS (SELECT * FROM SAS_METERS_CATALOG_PER_GROUP WHERE smcg_group_id = 10001 AND smcg_meter_code = 26)
	INSERT INTO [dbo].[sas_meters_catalog_per_group] ([smcg_group_id],[smcg_meter_code]) VALUES (10001,26);
GO

IF NOT EXISTS (SELECT * FROM SAS_METERS_CATALOG_PER_GROUP WHERE smcg_group_id = 3 AND smcg_meter_code = 25)
	INSERT INTO [dbo].[sas_meters_catalog_per_group] ([smcg_group_id],[smcg_meter_code]) VALUES (3,25);
GO

IF NOT EXISTS (SELECT * FROM SAS_METERS_CATALOG_PER_GROUP WHERE smcg_group_id = 3 AND smcg_meter_code = 26)
	INSERT INTO [dbo].[sas_meters_catalog_per_group] ([smcg_group_id],[smcg_meter_code]) VALUES (3,26);
GO

IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.PaymentMethod' AND GP_SUBJECT_KEY = 'BankCard.VisibleChars')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Cashier.PaymentMethod', 'BankCard.VisibleChars', '0')
GO

INSERT INTO   BANK_TRANSACTIONS
     SELECT   AO_UNDO_OPERATION_ID
	        , BT_TYPE
			, BT_DOCUMENT_NUMBER
			, BT_BANK_NAME
			, BT_BANK_COUNTRY
			, BT_HOLDER_NAME
			, BT_CARD_EXPIRATION_DATE
			, BT_CARD_TRACK_DATA
			, -BT_AMOUNT
			, BT_ISO_CODE
			, CM_USER_ID
			, CM_CASHIER_ID
			, CM_USER_NAME
			, CM_CASHIER_NAME
			, BT_TRANSACTION_TYPE
			, BT_EDITED
			, BT_CHECK_ROUTING_NUMBER
			, BT_CHECK_ACCOUNT_NUMBER
			, BT_CHECK_DATE
			, BT_COMMENTS
			, BT_ACCOUNT_TRACK_DATA
			, BT_ACCOUNT_HOLDER_NAME
       FROM   BANK_TRANSACTIONS
 INNER JOIN   ACCOUNT_OPERATIONS ON BT_OPERATION_ID=AO_OPERATION_ID
 INNER JOIN   CASHIER_MOVEMENTS ON AO_UNDO_OPERATION_ID=CM_OPERATION_ID
      WHERE   AO_UNDO_OPERATION_ID IS NOT  NULL 
        AND   CM_TYPE IN (4,153)
GO

--
--
--

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Account' AND GP_SUBJECT_KEY ='Level00.Global.MaxAllowedCashIn.Count')
INSERT INTO general_params (gp_group_key, gp_subject_key,                           gp_key_value)
                    SELECT  'Account',    'Level00.Global.MaxAllowedCashIn.Count',  case when gp_key_value = '1' then gp_key_value else 0 end
                      FROM  general_params WHERE  gp_group_key = 'Account' AND  gp_subject_key = 'Anonymous.OnlyOneRecharge'
GO

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Account' AND GP_SUBJECT_KEY ='Level00.Global.MaxAllowedCashIn.Amount')
 INSERT INTO general_params (gp_group_key, gp_subject_key,                           gp_key_value)
                     VALUES ('Account',    'Level00.Global.MaxAllowedCashIn.Amount', '0'         );
GO

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Account' AND GP_SUBJECT_KEY ='Level00.Daily.MaxAllowedCashIn.Count')
  INSERT INTO general_params (gp_group_key, gp_subject_key,                           gp_key_value)
                      VALUES ('Account',    'Level00.Daily.MaxAllowedCashIn.Count',   '0'         );
GO

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Account' AND GP_SUBJECT_KEY ='Level00.Daily.MaxAllowedCashIn.Amount')
  INSERT INTO general_params (gp_group_key, gp_subject_key,                           gp_key_value)
                      VALUES ('Account',    'Level00.Daily.MaxAllowedCashIn.Amount',  '0'         );
GO

IF EXISTS ( SELECT * FROM general_params WHERE gp_group_key = 'Account' AND gp_subject_key = 'Anonymous.OnlyOneRecharge')
  DELETE GENERAL_PARAMS WHERE gp_group_key = 'Account' AND gp_subject_key = 'Anonymous.OnlyOneRecharge'
GO

--
--
--
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (212994, 10, 1, 'Sas Meter Rollover.', 'Sas Meter Rollover.', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (212994, 9, 1, 'Sas Meter Rollover.', 'Sas Meter Rollover.', 1)
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (212994, 1, 1, GETDATE());
GO

IF NOT EXISTS (SELECT * FROM [dbo].[alarm_catalog] WHERE [alcg_alarm_code] = 393257)
BEGIN
	INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (196616, 9, 0, ' Application disconnected from DB', ' Application disconnected from DB', 1);
	INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (196616, 10, 0, 'Aplicaci�n desconectada de BD', 'Aplicaci�n desconectada de BD', 1);
	INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (196616, 48, 0, GETDATE());
END
ELSE
BEGIN
	UPDATE [dbo].[alarm_catalog_per_category] SET [alcc_alarm_code] = 196616 WHERE [alcc_alarm_code] = 393257
	UPDATE [dbo].[alarm_catalog] SET [alcg_alarm_code] = 196616 WHERE [alcg_alarm_code] = 393257
	UPDATE [dbo].[alarms] SET [al_alarm_code] = 196616 WHERE [al_alarm_code] = 393257
END
GO

INSERT INTO dbo.alarm_catalog (alcg_alarm_code, alcg_language_id, alcg_type, alcg_name, alcg_description, alcg_visible) VALUES (393256 , 9, 0, 'Memory corruption in one of the copies of the DB', 'Memory corruption in one of the copies of the database (data recovered from the other copy)', 1);
INSERT INTO dbo.alarm_catalog (alcg_alarm_code, alcg_language_id, alcg_type, alcg_name, alcg_description, alcg_visible) VALUES (393256 , 10, 0, 'Corrupci�n de memoria en una de las bases de datos', 'Corrupci�n de memoria en una de las copias de la base de datos (datos recuperados desde la otra copia)', 1);
INSERT INTO dbo.alarm_catalog (alcg_alarm_code, alcg_language_id, alcg_type, alcg_name, alcg_description, alcg_visible) VALUES (393257 , 9, 0, 'Memory corruption in both copies of the DB', 'Memory corruption in both copies of the database (data loss occurred)', 1);
INSERT INTO dbo.alarm_catalog (alcg_alarm_code, alcg_language_id, alcg_type, alcg_name, alcg_description, alcg_visible) VALUES (393257 , 10, 0, 'Corrupci�n de memoria en ambas bases de datos', 'Corrupci�n de memoria en ambas copias de la base de datos (se produjo perdida de informaci�n)', 1);
GO

INSERT INTO dbo.alarm_catalog_per_category (alcc_alarm_code, alcc_category, alcc_type, alcc_datetime) VALUES (393256 , 2, 0, GETDATE());
INSERT INTO dbo.alarm_catalog_per_category (alcc_alarm_code, alcc_category, alcc_type, alcc_datetime) VALUES (393257 , 2, 0, GETDATE());
GO

--
--
--
-- Stacker is only target (not source)
UPDATE CAGE_SOURCE_TARGET SET CST_TARGET = 0 WHERE CST_SOURCE_TARGET_ID = 12 
UPDATE CAGE_SOURCE_TARGET_CONCEPTS SET CSTC_TYPE  =0 WHERE CSTC_SOURCE_TARGET_ID = 12
GO

--
--
--
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY = 'CreditsExpireDailyOnAnonymousAccounts')
  INSERT INTO [general_params] ([gp_group_key],[gp_subject_key],[gp_key_value])
       VALUES                  ('Cashier', 'CreditsExpireDailyOnAnonymousAccounts','0')
GO

--
--
--
--- Add column
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[currency_exchange]') and name = 'ce_language_resources')
BEGIN
      ALTER TABLE dbo.currency_exchange ADD
            ce_language_resources XML NULL
END
GO

-- Peso mexicano
UPDATE	currency_exchange SET ce_language_resources = '
<LanguageResources>
  <NLS09>
   <PluralCurrency>pesos</PluralCurrency>
   <SingularCurrency>peso</SingularCurrency>
   <PluralCents>cents</PluralCents>
   <SingularCents>cent</SingularCents>
 </NLS09>
  <NLS10>
   <PluralCurrency>pesos</PluralCurrency>
   <SingularCurrency>peso</SingularCurrency>
   <PluralCents>centavos</PluralCents>
   <SingularCents>centavo</SingularCents>
 </NLS10>
</LanguageResources>'
where ce_currency_iso_code = 'MXN' AND ce_type = 0
GO

-- Dolar USA
UPDATE	currency_exchange SET ce_language_resources = '
<LanguageResources>
  <NLS09>
   <PluralCurrency>dollars</PluralCurrency>
   <SingularCurrency>dollar</SingularCurrency>
   <PluralCents>cents</PluralCents>
   <SingularCents>cent</SingularCents>
 </NLS09>
  <NLS10>
   <PluralCurrency>d�lares</PluralCurrency>
   <SingularCurrency>d�lar</SingularCurrency>
   <PluralCents>c�ntimos</PluralCents>
   <SingularCents>c�ntimo</SingularCents>
 </NLS10>
</LanguageResources>'
where ce_currency_iso_code = 'USD' AND ce_type = 0
GO

-- Nuevo sol peruano
UPDATE	currency_exchange SET ce_language_resources = '
<LanguageResources>
  <NLS09>
   <PluralCurrency>soles</PluralCurrency>
   <SingularCurrency>sol</SingularCurrency>
   <PluralCents>cents</PluralCents>
   <SingularCents>cent</SingularCents>
 </NLS09>
  <NLS10>
   <PluralCurrency>soles</PluralCurrency>
   <SingularCurrency>sol</SingularCurrency>
   <PluralCents>c�ntimos</PluralCents>
   <SingularCents>c�ntimo</SingularCents>
 </NLS10>
</LanguageResources>'
where ce_currency_iso_code = 'PEN' AND ce_type = 0
GO

-- Corona noruega
UPDATE	currency_exchange SET ce_language_resources = '
<LanguageResources>
  <NLS09>
   <PluralCurrency>kroner</PluralCurrency>
   <SingularCurrency>krone</SingularCurrency>
   <PluralCents>�re</PluralCents>
   <SingularCents>�re</SingularCents>
 </NLS09>
  <NLS10>
   <PluralCurrency>coronas</PluralCurrency>
   <SingularCurrency>corona</SingularCurrency>
   <PluralCents>�re</PluralCents>
   <SingularCents>�re</SingularCents>
 </NLS10>
</LanguageResources>'
where ce_currency_iso_code = 'NOK' AND ce_type = 0
GO

/****** INSERT TASK (66 - MESSAGE LCD) IN SITE ******/
IF NOT EXISTS(SELECT ST_TASK_ID FROM MS_SITE_TASKS WHERE ST_TASK_ID = 66)
BEGIN
  INSERT   INTO MS_SITE_TASKS
         ( ST_TASK_ID
         , ST_ENABLED
         , ST_INTERVAL_SECONDS
         , ST_MAX_ROWS_TO_UPLOAD
         ) 
  VALUES ( 66
         , 1
         , 3600
         , 50
         )
END
GO

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'WCP' AND GP_SUBJECT_KEY ='LCDMessages.MaintenanceChangesAfterMinutes')
  INSERT INTO general_params (gp_group_key, gp_subject_key, gp_key_value)
                      VALUES ('WCP', 'LCDMessages.MaintenanceChangesAfterMinutes', '5');
GO

/****** INSERT SEQUENCES ******/
IF NOT EXISTS(SELECT SEQ_ID FROM SEQUENCES WHERE SEQ_ID = 42)
BEGIN
    INSERT INTO SEQUENCES (SEQ_ID, SEQ_NEXT_VALUE) VALUES (42, 1);
END
GO

/******* STORED PROCEDURES *******/

--------------------------------------------------------------------------------
-- Copyright � 2013 Win Systems International
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: CashierMovementsGrouped_Read.sql
-- 
--   DESCRIPTION: Read historied cashier movements per working day
-- 
--        AUTHOR: JPJ
-- 
-- CREATION DATE: 02-DEC-2014
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 02-DEC-2014 JPJ    First release.
--------------------------------------------------------------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Tax_Report_Per_Day]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Tax_Report_Per_Day]
GO

CREATE PROCEDURE [dbo].[Tax_Report_Per_Day]        
                 @pDateFrom DATETIME
               , @pDateTo   DATETIME       
  AS
BEGIN 

  DECLARE @CARD_DEPOSIT_IN                      INT
  DECLARE @CARD_REPLACEMENT                     INT
  DECLARE @CASH_IN_SPLIT2                       INT
  DECLARE @MB_CASH_IN_SPLIT2                    INT
  DECLARE @NA_CASH_IN_SPLIT2                    INT
  DECLARE @_DAY_VAR                             DATETIME 

  SET @CARD_DEPOSIT_IN                        =   9
  SET @CARD_REPLACEMENT                       =  28
  SET @CASH_IN_SPLIT2                         =  34
  SET @MB_CASH_IN_SPLIT2                      =  35
  SET @NA_CASH_IN_SPLIT2                      =  55

  IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID('#TEMP_TABLE') AND type in (N'U'))
  BEGIN                                
    DROP TABLE #TEMP_TABLE  
  END       

  CREATE  TABLE #TEMP_TABLE ( TODAY   DATETIME PRIMARY KEY ) 
  
  IF @pDateTo IS NULL
  BEGIN
     -- IF DATE TO IS NULL, QUERY UNTIL CURRENT DATE
     SET @pDateTo = CAST(GETDATE() AS DATETIME)
  END

  -- TEMP TABLE IS FILLED WITH THE RANGE OF DATES
  SET @_DAY_VAR = @pDateFrom

  WHILE @_DAY_VAR < @pDateTo 
  BEGIN 
     INSERT INTO   #TEMP_TABLE (Today) VALUES (@_DAY_VAR)
     SET @_DAY_VAR =  DATEADD(Day,1,@_DAY_VAR)
  END 
  
  ;
  -- CASHIER MOVEMENTS FILTERED BY DATE AND MOVEMENT TYPES
  WITH MovementsPerWorkingday  AS 
   (
     SELECT   dbo.Opening(0, CM_DATE) 'WorkingDate'
            , CM_TYPE
            , CM_ADD_AMOUNT
            , CM_AUX_AMOUNT
       FROM   CASHIER_MOVEMENTS_GROUPED_BY_HOUR
      WHERE   (CM_DATE >= @pDateFrom AND (CM_DATE < @pDateTo))
              AND  CM_TYPE IN ( @MB_CASH_IN_SPLIT2,
                                @CASH_IN_SPLIT2, 
                                @NA_CASH_IN_SPLIT2,
                                @CARD_DEPOSIT_IN,
                                @CARD_REPLACEMENT )
  )

  SELECT   TimeRange.Today WorkingDate
         , ISNULL(((GrossCard - TaxCard) + (GrossCompanyB - TaxCompanyB)),0) BaseTotal
         , ISNULL((TaxCard + TaxCompanyB),0)       TaxTotal
         , ISNULL((GrossCard + GrossCompanyB),0)   GrossTotal
         , ISNULL((GrossCard - TaxCard),0)         BaseCard
         , ISNULL(TaxCard,0)                       TaxCard
         , ISNULL(GrossCard,0)                     GrossCard
         , ISNULL((GrossCompanyB - TaxCompanyB),0) BaseCompanyB         
         , ISNULL(TaxCompanyB,0)                   TaxCompanyB
         , ISNULL(GrossCompanyB,0)                 GrossCompanyB
  FROM (
           SELECT  WorkingDate
           ,
                   SUM(ISNULL(CASE WHEN CM_TYPE IN ( @CARD_DEPOSIT_IN,
                                                      @CARD_REPLACEMENT )  THEN CM_ADD_AMOUNT
                        ELSE 0
                        END, 0)) 'GrossCard'
                  , SUM(ISNULL(CASE WHEN CM_TYPE IN ( @CARD_DEPOSIT_IN,
                                                      @CARD_REPLACEMENT )  THEN CM_AUX_AMOUNT
                        ELSE 0
                        END, 0)) 'TaxCard'
                  , SUM(ISNULL(CASE WHEN CM_TYPE IN ( @MB_CASH_IN_SPLIT2,
                                                      @CASH_IN_SPLIT2, 
                                                      @NA_CASH_IN_SPLIT2 ) THEN CM_ADD_AMOUNT
                        ELSE 0
                        END, 0)) 'GrossCompanyB'
                  , SUM(ISNULL(CASE WHEN CM_TYPE IN ( @MB_CASH_IN_SPLIT2,
                                                      @CASH_IN_SPLIT2, 
                                                      @NA_CASH_IN_SPLIT2 ) THEN CM_AUX_AMOUNT
                        ELSE 0
                        END, 0)) 'TaxCompanyB'
           FROM MovementsPerWorkingday 
           GROUP BY WorkingDate
         ) MOVEMENTS RIGHT JOIN #TEMP_TABLE TimeRange on TimeRange.Today = MOVEMENTS.WorkingDate
   ORDER BY TimeRange.Today ASC       
   
   DROP TABLE #TEMP_TABLE
END
GO

GRANT EXECUTE ON [dbo].[Tax_Report_Per_Day] TO [wggui] WITH GRANT OPTION
GO

 
--------------------------------------------------------------------------------
-- Copyright � 2014 Win Systems International
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: iStatsCashierMovements.sql
-- 
--   DESCRIPTION: Procedure for iStat Cashier Movements 
-- 
--        AUTHOR: Jordi Masachs 
-- 
-- CREATION DATE: 24-NOV-2014
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 24-NOV-2014 JMV    First release
--------------------------------------------------------------------------------


--------------------------------------------------------------------------------
-- PURPOSE: Show cashier movements grouped by day, week or month
-- 
--  PARAMS:
--      - INPUT:
--   @pDateFrom	   DATETIME     
--   @pDateTo	   DATETIME   
--   @pGrouped     NVARCHAR ('D'/'W'/'M')  
--
--      - OUTPUT:
--
-- RETURNS:
--                  
--
--   NOTES:
--

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[iStatsCashierMovements]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[iStatsCashierMovements]
GO  

CREATE PROCEDURE [dbo].[iStatsCashierMovements]
  @pDateFrom DATETIME,  
  @pDateTo   DATETIME,
  @pGrouped NVARCHAR
  
AS
  
BEGIN  

	DECLARE @pConcatDateFrom DateTime
	DECLARE @pConcatDateTo DateTime
	DECLARE @pConcatDate2007 DateTime
	DECLARE @pOH INT
	DECLARE @NationalCurrency VARCHAR(3)
	
	SET @pConcatDateFrom = dbo.ConcatOpeningTime(0, @pDateFrom)
	SET @pConcatDateTo = dbo.ConcatOpeningTime(0, @pDateTo)
	SET @pConcatDate2007 = dbo.ConcatOpeningTime(0, '2007/01/01')
	SET @pOH = DATEDIFF (HOUR, '2007/01/01', @pConcatDate2007)
	
	-- Get national currency
	SELECT @NationalCurrency = GP_KEY_VALUE FROM GENERAL_PARAMS 
     WHERE GP_GROUP_KEY = 'RegionalOptions' 
       AND GP_SUBJECT_KEY = 'CurrencyISOCode'	
	
	
	SELECT   BASE_DATE
		   , TOTAL_CASH_IN
		   , TOTAL_OUTPUTS - PRIZE_TAX AS TOTAL_CASH_OUT
		   , PRIZE_TAX + TAXES AS PRIZE_TAXES
		   , TOTAL_CASH_IN - TOTAL_OUTPUTS - TAXES - CASH_IN_TAXES AS RESULT_CASHIER
	  FROM   (
			  SELECT  
				CASE @pGrouped
					WHEN 'D' THEN DATEADD (DAY, DATEDIFF (HOUR, @pConcatDate2007, CM_DATE) / 24, '01/01/2007')
					WHEN 'W' THEN DATEADD (DAY, DATEDIFF (HOUR, @pConcatDate2007, CM_DATE) / 24, '01/01/2007')  
					WHEN 'M' THEN DATEADD (MONTH, DATEDIFF (MONTH, @pConcatDate2007, DATEADD(HOUR, -@pOH, CM_DATE)), '01/01/2007') 
				END AS BASE_DATE			   
				 , ( SUM(CASE WHEN CM_TYPE IN (9, 28, 34, 35, 37, 39, 54, 55, 71, 85, 86, 142, 146) THEN CM_SUB_AMOUNT + CM_ADD_AMOUNT ELSE 0 END) 
                   + SUM(CASE WHEN CM_TYPE IN (78, 79, 92,  147, 148, 152, 153, 154, 155) THEN CM_SUB_AMOUNT ELSE 0 END)
                   + SUM(CASE WHEN (CM_TYPE = 1000000 AND (ISNULL(cm_currency_iso_code, '') = '' OR cm_currency_iso_code = @NationalCurrency)) THEN CM_SUB_AMOUNT + CM_ADD_AMOUNT ELSE 0 END) ) AS TOTAL_CASH_IN
                 , ( SUM(CASE WHEN CM_TYPE IN (8, 10, 36, 38, 40, 41, 69, 70, 143, 302) THEN CM_SUB_AMOUNT + CM_ADD_AMOUNT ELSE 0 END) 
                   + SUM(CASE WHEN (CM_TYPE = 2000000 AND (ISNULL(cm_currency_iso_code, '') = '' OR cm_currency_iso_code = @NationalCurrency)) THEN CM_SUB_AMOUNT + CM_ADD_AMOUNT ELSE 0 END)) AS TOTAL_OUTPUTS
                 , SUM(CASE WHEN CM_TYPE IN (6, 14) THEN CM_SUB_AMOUNT + CM_ADD_AMOUNT ELSE 0 END) AS PRIZE_TAX
                 , SUM(CASE WHEN CM_TYPE IN (95, 96, 101, 102) THEN CM_SUB_AMOUNT + CM_ADD_AMOUNT ELSE 0 END) AS TAXES
                 , SUM(CASE WHEN CM_TYPE IN (142, 146) THEN CM_SUB_AMOUNT + CM_ADD_AMOUNT ELSE 0 END) AS CASH_IN_TAXES
				FROM   CASHIER_MOVEMENTS_GROUPED_BY_HOUR
			   WHERE   CM_DATE <  @pConcatDateTo
				 AND   CM_DATE >= @pConcatDateFrom
				 AND   CM_TYPE in (6, 8, 9, 10, 14, 28, 34, 35, 36, 37, 38, 39, 40, 41, 54, 55, 69, 70, 71, 78, 79, 92, 85, 86, 95, 96, 101, 102, 142, 143, 146, 147, 148, 152, 153, 154, 155, 302, 1000000, 2000000)
				 AND   CM_SUB_TYPE = 0
			  GROUP BY 
			  	CASE @pGrouped
					WHEN 'D' THEN DATEADD (DAY, DATEDIFF (HOUR, @pConcatDate2007, CM_DATE) / 24, '01/01/2007')
					WHEN 'W' THEN DATEADD (DAY, DATEDIFF (HOUR, @pConcatDate2007, CM_DATE) / 24, '01/01/2007')  
					WHEN 'M' THEN DATEADD (MONTH, DATEDIFF (MONTH, @pConcatDate2007, DATEADD(HOUR, -@pOH, CM_DATE)), '01/01/2007') 
				END 
			 
			 ) X
	 WHERE   TOTAL_CASH_IN > 0
		OR   TOTAL_OUTPUTS > 0
		OR   PRIZE_TAX   > 0
		OR   TAXES > 0
	ORDER BY BASE_DATE

END
GO

ALTER PROCEDURE GT_Cancellations
(
	  @pCashierId BIGINT,
    @pDateFrom DATETIME,
    @pDateTo DATETIME,
    @pType INT, -- 0: only Sales ; 1: only Purchases ; -1: all
    @pUserId INT
)
AS
BEGIN

      DECLARE @TYPE_CHIPS_SALE AS INT
      DECLARE @TYPE_CHIPS_PURCHASE AS INT
      DECLARE @TYPE_CHIPS_SALE_TOTAL AS INT
      DECLARE @TYPE_CHIPS_PURCHASE_TOTAL AS INT
      DECLARE @UNDO_STATUS AS INT

      SET @TYPE_CHIPS_SALE = 300
      SET @TYPE_CHIPS_PURCHASE = 301
      SET @TYPE_CHIPS_SALE_TOTAL = 303
      SET @TYPE_CHIPS_PURCHASE_TOTAL = 304
      SET @UNDO_STATUS = 2
      
      SET @pType = ISNULL(@pType, -1)

      SELECT
            CASE WHEN CM_TYPE IN (@TYPE_CHIPS_SALE_TOTAL, @TYPE_CHIPS_PURCHASE_TOTAL) THEN 
                        CM_CASHIER_NAME
                  ELSE NULL END AS ORIGEN,
            CASE WHEN CM_TYPE IN (@TYPE_CHIPS_SALE_TOTAL, @TYPE_CHIPS_PURCHASE_TOTAL) THEN 
                        CM_DATE
                  ELSE NULL END AS CM_DATE,
            CASE WHEN CM_TYPE IN (@TYPE_CHIPS_SALE, @TYPE_CHIPS_SALE_TOTAL) THEN ABS(CM_SUB_AMOUNT)
                  ELSE ABS(CM_ADD_AMOUNT) END AS AMOUNT,
                  
            ABS(CASE WHEN CM_CURRENCY_DENOMINATION IS NULL OR CM_CURRENCY_DENOMINATION =0 THEN 0 
                         ELSE 
                             CASE WHEN CM_TYPE IN (@TYPE_CHIPS_SALE, @TYPE_CHIPS_SALE_TOTAL) THEN CM_SUB_AMOUNT
                                   ELSE CM_ADD_AMOUNT 
                                    END / CM_CURRENCY_DENOMINATION 
                             END) AS QUANTITY,
            CM_CURRENCY_DENOMINATION,
            CM.CM_MOVEMENT_ID,
            CM.CM_TYPE,
            CM.CM_USER_NAME,
            CM.CM_OPERATION_ID,
            CASE WHEN CM_TYPE IN (@TYPE_CHIPS_SALE_TOTAL, @TYPE_CHIPS_PURCHASE_TOTAL) THEN 
                        AO.AO_DATETIME
                  ELSE NULL END AS AO_DATETIME
      FROM CASHIER_MOVEMENTS CM
            LEFT JOIN ACCOUNT_OPERATIONS CANCEL ON CM.CM_OPERATION_ID = CANCEL.AO_OPERATION_ID
            LEFT JOIN ACCOUNT_OPERATIONS AO ON CANCEL.AO_UNDO_OPERATION_ID = AO.AO_OPERATION_ID
      WHERE (@pCashierId IS NULL OR @pCashierId = CM_CASHIER_ID)
              AND ( @pUserId IS NULL OR @pUserId = CM_USER_ID)
              AND CM_UNDO_STATUS = @UNDO_STATUS 
              AND CM_TYPE IN (@TYPE_CHIPS_SALE, @TYPE_CHIPS_PURCHASE, @TYPE_CHIPS_SALE_TOTAL, @TYPE_CHIPS_PURCHASE_TOTAL)
              AND CM_DATE >= @pDateFrom AND CM_DATE < @pDateTo
              AND (@pType = -1
                        OR (@pType = 1 AND CM.CM_TYPE IN (@TYPE_CHIPS_PURCHASE, @TYPE_CHIPS_PURCHASE_TOTAL))
                        OR (@pType = 0 AND CM.CM_TYPE IN (@TYPE_CHIPS_SALE, @TYPE_CHIPS_SALE_TOTAL)))
      ORDER BY CM.CM_MOVEMENT_ID
		
END --GT_CANCELLATIONS
GO

--------------------------------------------------------------------------------
-- Copyright � 2014 Win Systems International
--------------------------------------------------------------------------------
--
--   MODULE NAME: Update_LcdMessages.sql
--
--   DESCRIPTION: Update Lcd Messages in the Site
--
--        AUTHOR: Didac Campanals Subirats
--
-- CREATION DATE: 03-DIC-2014
--
-- REVISION HISTORY:
--
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 03-DIC-2014 DCS    First release.  
-------------------------------------------------------------------------------- 

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Update_LcdMessages]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Update_LcdMessages]
GO 

CREATE PROCEDURE   [dbo].[Update_LcdMessages]
										 @pUniqueId							BIGINT        
									 , @pType									INT
									 , @pSiteList             XML
									 , @pTerminalList         XML
									 , @pAccountList          XML
									 , @pEnabled              BIT
									 , @pOrder								INT
									 , @pScheduleStart        DATETIME
									 , @pScheduleEnd          DATETIME
									 , @pScheduleWeekday      INT
									 , @pSchedule1TimeFrom    INT
									 , @pSchedule1TimeTo      INT
									 , @pSchedule2Enabled     BIT
									 , @pSchedule2TimeFrom    INT
									 , @pSchedule2TimeTo      INT
									 , @pMessage              XML
									 , @pResourceId           BIGINT
									 , @pMasterSequenceId     BIGINT									 
                   
AS
BEGIN

  IF NOT EXISTS (SELECT 1 FROM LCD_MESSAGES WHERE MSG_UNIQUE_ID = @pUniqueId)
  
  BEGIN
    
    INSERT INTO   LCD_MESSAGES ( 
																 MSG_UNIQUE_ID 
															 , MSG_TYPE
															 , MSG_SITE_LIST
															 , MSG_TERMINAL_LIST
															 , MSG_ACCOUNT_LIST
															 , MSG_ENABLED
															 , MSG_ORDER
															 , MSG_SCHEDULE_START
															 , MSG_SCHEDULE_END
															 , MSG_SCHEDULE_WEEKDAY
															 , MSG_SCHEDULE1_TIME_FROM
															 , MSG_SCHEDULE1_TIME_TO
															 , MSG_SCHEDULE2_ENABLED
															 , MSG_SCHEDULE2_TIME_FROM
															 , MSG_SCHEDULE2_TIME_TO
															 , MSG_MESSAGE
															 , MSG_RESOURCE_ID
															 , MSG_MASTER_SEQUENCE_ID
															 )
                       VALUES  (
                                 @pUniqueId            
															 , @pType                
															 , @pSiteList            
															 , @pTerminalList        
															 , @pAccountList         
															 , @pEnabled             
															 , @pOrder               
															 , @pScheduleStart       
															 , @pScheduleEnd         
															 , @pScheduleWeekday     
															 , @pSchedule1TimeFrom   
															 , @pSchedule1TimeTo     
															 , @pSchedule2Enabled    
															 , @pSchedule2TimeFrom   
															 , @pSchedule2TimeTo      
															 , @pMessage             
															 , @pResourceId          
															 , @pMasterSequenceId    
															 ) 

  END
  ELSE
  BEGIN
    UPDATE   LCD_MESSAGES
      SET 
					 MSG_TYPE										= @pType
				 , MSG_SITE_LIST							= @pSiteList
				 , MSG_TERMINAL_LIST					= @pTerminalList
				 , MSG_ACCOUNT_LIST						= @pAccountList
				 , MSG_ENABLED								= @pEnabled
				 , MSG_ORDER									= @pOrder
				 , MSG_SCHEDULE_START					= @pScheduleStart
				 , MSG_SCHEDULE_END						= @pScheduleEnd
				 , MSG_SCHEDULE_WEEKDAY				= @pScheduleWeekday
				 , MSG_SCHEDULE1_TIME_FROM		= @pSchedule1TimeFrom
				 , MSG_SCHEDULE1_TIME_TO			= @pSchedule1TimeTo
				 , MSG_SCHEDULE2_ENABLED			= @pSchedule2Enabled
				 , MSG_SCHEDULE2_TIME_FROM		= @pSchedule2TimeFrom
				 , MSG_SCHEDULE2_TIME_TO			= @pSchedule2TimeTo
				 , MSG_MESSAGE								= @pMessage
				 , MSG_RESOURCE_ID						= @pResourceId
				 , MSG_MASTER_SEQUENCE_ID			= @pMasterSequenceId
   WHERE   MSG_UNIQUE_ID							= @pUniqueId
		 
  END
END
GO 