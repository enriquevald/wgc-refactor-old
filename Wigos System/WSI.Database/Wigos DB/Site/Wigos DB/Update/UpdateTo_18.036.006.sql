/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 35;

SET @New_ReleaseId = 36;
SET @New_ScriptName = N'UpdateTo_18.036.006.sql';
SET @New_Description = N'New field dr_gender_filter in table Draws, and new records in general params.';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/
/* Draws */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[draws]') and name = 'dr_gender_filter')
  ALTER TABLE [dbo].[draws]
          ADD [dr_gender_filter] [int] NOT NULL CONSTRAINT DF_draws_dr_gender_filter DEFAULT 0;
ELSE
  SELECT '***** Field draws.dr_gender_filter already exists *****';

/****** INDEXES ******/

/****** RECORDS ******/
/* Cashier.CashIn.Mode */
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY ='CashIn.Mode')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key]
             ,[gp_subject_key]
             ,[gp_key_value])
       VALUES
             ('Cashier'
             ,'CashIn.Mode'
             ,'0');
ELSE
  SELECT '***** Record Cashier.CashIn.Mode already exists *****';

/* Cashier.Promotions.HoursBeforeTodayOpening */
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY ='Promotions.HoursBeforeTodayOpening')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key]
             ,[gp_subject_key]
             ,[gp_key_value])
       VALUES
             ('Cashier'
             ,'Promotions.HoursBeforeTodayOpening'
             ,'0');
ELSE
  SELECT '***** Record Cashier.Promotions.HoursBeforeTodayOpening already exists *****';

/* Cashier.Draws.HoursBeforeTodayOpening */
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY ='Draws.HoursBeforeTodayOpening')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key]
             ,[gp_subject_key]
             ,[gp_key_value])
       VALUES
             ('Cashier'
             ,'Draws.HoursBeforeTodayOpening'
             ,'0');
ELSE
  SELECT '***** Record Cashier.Draws.HoursBeforeTodayOpening already exists *****';
