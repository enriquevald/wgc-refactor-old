/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 194;

SET @New_ReleaseId = 195;
SET @New_ScriptName = N'UpdateTo_18.195.037.sql';
SET @New_Description = N'Changes for bonusing and historical data';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/******* TABLES  *******/
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[site_jackpot_parameters]') and name = 'sjp_terminal_list')
  ALTER TABLE [dbo].[site_jackpot_parameters] ADD [sjp_terminal_list] [xml] NULL

/******* INDEXES  *******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[cashier_terminals]') AND name = N'IX_ct_terminal_id')
  CREATE NONCLUSTERED INDEX [IX_ct_terminal_id] ON [dbo].[cashier_terminals] 
  (
        [ct_terminal_id] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

/******* STORED  *******/

IF OBJECT_ID (N'dbo.GT_Cancellations', N'P') IS NOT NULL
    DROP PROCEDURE dbo.GT_Cancellations;                 
GO

CREATE PROCEDURE GT_Cancellations
(
        @pCashierId BIGINT,
    @pDateFrom DATETIME,
    @pDateTo DATETIME,
    @pType INT -- 0: only Sales ; 1: only Purchases ; -1: all
)
AS
BEGIN

      DECLARE @TYPE_CHIPS_SALE AS INT
      DECLARE @TYPE_CHIPS_PURCHASE AS INT
      DECLARE @TYPE_CHIPS_SALE_TOTAL AS INT
      DECLARE @TYPE_CHIPS_PURCHASE_TOTAL AS INT
      DECLARE @UNDO_STATUS AS INT

      SET @TYPE_CHIPS_SALE = 300
      SET @TYPE_CHIPS_PURCHASE = 301
      SET @TYPE_CHIPS_SALE_TOTAL = 303
      SET @TYPE_CHIPS_PURCHASE_TOTAL = 304
      SET @UNDO_STATUS = 2
      
      SET @pType = ISNULL(@pType, -1)

      SELECT
            CASE WHEN CM_TYPE IN (@TYPE_CHIPS_SALE_TOTAL, @TYPE_CHIPS_PURCHASE_TOTAL) THEN 
                        CT.CT_NAME
                  ELSE NULL END AS ORIGEN,
            CM_DATE,
            CASE WHEN CM_TYPE IN (@TYPE_CHIPS_SALE, @TYPE_CHIPS_SALE_TOTAL) THEN ABS(CM_SUB_AMOUNT)
                  ELSE ABS(CM_ADD_AMOUNT) END AS AMOUNT,
                  
            ABS(CASE WHEN CM_CURRENCY_DENOMINATION IS NULL OR CM_CURRENCY_DENOMINATION =0 THEN 0 
                         ELSE 
                             CASE WHEN CM_TYPE IN (@TYPE_CHIPS_SALE, @TYPE_CHIPS_SALE_TOTAL) THEN CM_SUB_AMOUNT
                                   ELSE CM_ADD_AMOUNT 
                                    END / CM_CURRENCY_DENOMINATION 
                             END) AS QUANTITY,
            CM_CURRENCY_DENOMINATION,
            CM.CM_MOVEMENT_ID,
            CM.CM_TYPE,
            GU.GU_USERNAME,
            CM.CM_OPERATION_ID
      FROM CASHIER_MOVEMENTS CM
            LEFT JOIN GAMING_TABLES_SESSIONS GTS ON GTS.GTS_GAMING_TABLE_SESSION_ID = CM.CM_GAMING_TABLE_SESSION_ID
            LEFT JOIN GAMING_TABLES GT ON GT.GT_GAMING_TABLE_ID = GTS.GTS_GAMING_TABLE_ID
            LEFT JOIN CASHIER_TERMINALS CT ON CT.CT_CASHIER_ID = CM.CM_CASHIER_ID
            LEFT JOIN GUI_USERS GU ON CM.CM_USER_ID = GU.GU_USER_ID
      WHERE (@pCashierId IS NULL OR @pCashierId = CT.CT_CASHIER_ID)
              AND CM_UNDO_STATUS = @UNDO_STATUS 
              AND CM_TYPE IN (@TYPE_CHIPS_SALE, @TYPE_CHIPS_PURCHASE, @TYPE_CHIPS_SALE_TOTAL, @TYPE_CHIPS_PURCHASE_TOTAL)
              AND CM_DATE >= @pDateFrom AND CM_DATE < @pDateTo
              AND (@pType = -1
                        OR (@pType = 1 AND CM.CM_TYPE IN (@TYPE_CHIPS_PURCHASE, @TYPE_CHIPS_PURCHASE_TOTAL))
                        OR (@pType = 0 AND CM.CM_TYPE IN (@TYPE_CHIPS_SALE, @TYPE_CHIPS_SALE_TOTAL)))

END --GT_CANCELLATIONS

GO

-- PERMISSIONS

GRANT EXECUTE ON [dbo].[GT_Cancellations] TO [wggui] WITH GRANT OPTION
GO

/*
----------------------------------------------------------------------------------------------------------------
CASHIER SESSIONS REPORTS

Version  Date           User     Description
----------------------------------------------------------------------------------------------------------------
1.0.0    28-FEB-2014    RMS      First Release

Parameters:
   -- :
  
 Results:
   
----------------------------------------------------------------------------------------------------------------   
*/

IF OBJECT_ID (N'dbo.SP_Cashier_Sessions_Report', N'P') IS NOT NULL
    DROP PROCEDURE dbo.SP_Cashier_Sessions_Report;                 
GO 

CREATE PROCEDURE [dbo].[SP_Cashier_Sessions_Report] 
 (  @pDateFrom DateTime
  , @pDateTo DateTime
  , @pUseCashierMovements as Bit
  , @pShowSystemSessions as Bit
  , @pSystemUsersTypes as VarChar(4096)
  , @pSessionStatus as VarChar(4096)
  , @pUserId as Integer
  , @pCashierId as Integer
  , @pCurrencyISOCode as VarChar(3)
)
AS
BEGIN

-- Members --
DECLARE @_delimiter AS CHAR(1)
SET @_delimiter = ','
DECLARE @pSystemUsersTypesTable AS TABLE (SUT_ID INT, SUT_VALUE VARCHAR(50))
DECLARE @pSessionsStatusTable AS TABLE (SUT_ID INT, SUT_VALUE VARCHAR(50))
DECLARE @_table_sessions_ AS TABLE ( sess_id                      BIGINT 
 								           , sess_name                    NVARCHAR(50) 
 								           , sess_user_id                 INT	
 								           , sess_full_name               NVARCHAR(50) 
 								           , sess_opening_date            DATETIME 
 								           , sess_closing_date            DATETIME  
 								           , sess_cashier_id              INT 
 								           , sess_ct_name                 NVARCHAR(50) 
 								           , sess_status                  INT 
 								           , sess_cs_limit_sale           MONEY
 								           , sess_mb_limit_sale           MONEY
 								           , sess_collected_amount        MONEY
 								           ) 
 								           
-- SYSTEM USER TYPES --
IF @pShowSystemSessions = 0
BEGIN
 INSERT INTO @pSystemUsersTypesTable SELECT * FROM dbo.SplitStringIntoTable(@pSystemUsersTypes, @_delimiter, DEFAULT)
END

-- SESSIONS STATUS
INSERT INTO @pSessionsStatusTable SELECT * FROM dbo.SplitStringIntoTable(@pSessionStatus, @_delimiter, DEFAULT)

IF @pUseCashierMovements = 0
   BEGIN -- Filter Dates on cashier session opening date  

      -- SESSIONS --
      INSERT   INTO @_table_sessions_
      SELECT   DISTINCT(CS_SESSION_ID) AS SESS_ID
             , CS_NAME AS SESS_NAME
             , CS_USER_ID AS SESS_USER_ID
             , GU_FULL_NAME AS SESS_FULL_NAME
             , CS_OPENING_DATE AS SESS_OPENING_DATE
             , CS_CLOSING_DATE AS SESS_CLOSING_DATE
             , CS_CASHIER_ID AS SESS_CASHIER_ID
             , CT_NAME AS SESS_CT_NAME
             , CS_STATUS AS SESS_STATUS
             , CS_SALES_LIMIT AS SESS_CS_LIMIT_SALE
             , CS_MB_SALES_LIMIT AS SESS_MB_LIMIT_SALE
             , CS_COLLECTED_AMOUNT AS SESS_COLLECTED_AMOUNT
        FROM   CASHIER_SESSIONS --WITH (INDEX (IX_cs_opening_date)) /* TODO: REVIEW INDEXES */
       INNER   JOIN GUI_USERS         ON CS_USER_ID    = GU_USER_ID
       INNER   JOIN CASHIER_TERMINALS ON CS_CASHIER_ID = CT_CASHIER_ID
       WHERE   (CS_OPENING_DATE >= @pDateFrom AND CS_OPENING_DATE <= @pDateTo)
          --   usuarios sistema s/n & user 
         AND   (      GU_USER_TYPE NOT IN ( SELECT SUT_VALUE FROM @pSystemUsersTypesTable )
                AND   CS_USER_ID = (CASE WHEN @pUserId IS NULL THEN CS_USER_ID ELSE @pUserId END) )
          --   cashier id
         AND   CS_CASHIER_ID = (CASE WHEN @pCashierId IS NULL THEN CS_CASHIER_ID ELSE @pCashierId END) 
          --   session status
         AND   CS_STATUS IN ( SELECT SUT_VALUE FROM @pSessionsStatusTable )
       ORDER   BY CS_OPENING_DATE DESC  

      -- MOVEMENTS
      -- By Movements in Historical Data --    
      SELECT   CM_SESSION_ID
             , CM_TYPE
             , CM_SUB_TYPE
             , ISNULL(CM_CURRENCY_ISO_CODE, @pCurrencyISOCode) AS CM_CURRENCY_ISO_CODE
             , CM_CURRENCY_DENOMINATION
             , CM_TYPE_COUNT
             , CM_SUB_AMOUNT
             , CM_ADD_AMOUNT
             , CM_AUX_AMOUNT
             , CM_INITIAL_BALANCE
             , CM_FINAL_BALANCE
        FROM   CASHIER_MOVEMENTS_GROUPED_BY_SESSION_ID
       WHERE   CM_SESSION_ID IN (SELECT SESS_ID FROM @_table_sessions_)

   END
ELSE
   BEGIN -- Filter Dates on cashier movements date


      -- SESSIONS --
      INSERT   INTO @_table_sessions_
      SELECT   DISTINCT(CS_SESSION_ID) AS SESS_ID
             , CS_NAME AS SESS_NAME
             , CS_USER_ID AS SESS_USER_ID
             , GU_FULL_NAME AS SESS_FULL_NAME
             , CS_OPENING_DATE AS SESS_OPENING_DATE
             , CS_CLOSING_DATE AS SESS_CLOSING_DATE
             , CS_CASHIER_ID AS SESS_CASHIER_ID
             , CT_NAME AS SESS_CT_NAME
             , CS_STATUS AS SESS_STATUS
             , CS_SALES_LIMIT AS SESS_CS_LIMIT_SALE
             , CS_MB_SALES_LIMIT AS SESS_MB_LIMIT_SALE
             , CS_COLLECTED_AMOUNT AS SESS_COLLECTED_AMOUNT
        FROM   CASHIER_SESSIONS --WITH (INDEX (IX_cs_opening_date)) /* TODO: REVIEW INDEXES */
       INNER   JOIN GUI_USERS         ON CS_USER_ID    = GU_USER_ID
       INNER   JOIN CASHIER_TERMINALS ON CS_CASHIER_ID = CT_CASHIER_ID
       WHERE   (CS_OPENING_DATE <= @pDateTo)
         AND   (CS_CLOSING_DATE IS NULL OR CS_CLOSING_DATE >= @pDateFrom)
          --   usuarios sistema s/n & user 
         AND   (      GU_USER_TYPE NOT IN ( SELECT SUT_VALUE FROM @pSystemUsersTypesTable )
                AND   CS_USER_ID = (CASE WHEN @pUserId IS NULL THEN CS_USER_ID ELSE @pUserId END) )
          --   cashier id
         AND   CS_CASHIER_ID = (CASE WHEN @pCashierId IS NULL THEN CS_CASHIER_ID ELSE @pCashierId END) 
          --   session status
         AND   CS_STATUS IN ( SELECT SUT_VALUE FROM @pSessionsStatusTable )         
       ORDER   BY CS_OPENING_DATE DESC  
       
      -- MOVEMENTS
      -- By Cashier_Movements --    
      SELECT   CM_DATE
             , CM_SESSION_ID
             , CM_TYPE
             , 0 as CM_SUB_TYPE
             , ISNULL(CM_CURRENCY_ISO_CODE, @pCurrencyISOCode) AS CM_CURRENCY_ISO_CODE
             , CM_CURRENCY_DENOMINATION
             , 1 as CM_TYPE_COUNT
             , CM_SUB_AMOUNT
             , CM_ADD_AMOUNT
             , CM_AUX_AMOUNT
             , CM_INITIAL_BALANCE
             , CM_FINAL_BALANCE
        FROM   CASHIER_MOVEMENTS 
       WHERE   CM_DATE >= @pDateFrom AND CM_DATE < @pDateTo
         AND   CM_SESSION_ID IN (SELECT SESS_ID FROM @_table_sessions_)
       UNION   ALL
      SELECT   MBM_DATETIME AS CM_DATE
             , MBM_CASHIER_SESSION_ID AS CM_SESSION_ID
             , MBM_TYPE AS CM_TYPE
             , 1 AS CM_SUB_TYPE
             , @pCurrencyISOCode AS CM_CURRENCY_ISO_CODE
             , 0 AS CM_CURRENCY_DENOMINATION
             , 1 AS CM_TYPE_COUNT
             , ISNULL(MBM_SUB_AMOUNT, 0) AS CM_SUB_AMOUNT
             , ISNULL(MBM_ADD_AMOUNT, 0) AS CM_ADD_AMOUNT
             , 0 AS CM_AUX_AMOUNT
             , 0 AS CM_INITIAL_BALANCE
             , 0 AS CM_FINAL_BALANCE
        FROM   MB_MOVEMENTS 
       WHERE   MBM_CASHIER_SESSION_ID IN (SELECT SESS_ID FROM @_table_sessions_)
         AND   MBM_DATETIME >= @pDateFrom AND MBM_DATETIME < @pDateTo
   END 

-- SESSIONS with CAGE INFO
SELECT   SESS_ID
       , SESS_NAME
       , SESS_USER_ID
       , SESS_FULL_NAME
       , SESS_OPENING_DATE
       , SESS_CLOSING_DATE
       , SESS_CASHIER_ID
       , SESS_CT_NAME
       , SESS_STATUS
       , SESS_CS_LIMIT_SALE
       , SESS_MB_LIMIT_SALE
       , SESS_COLLECTED_AMOUNT 
       , X.CGS_CAGE_SESSION_ID AS CGS_CAGE_SESSION_ID
       , X.CGS_SESSION_NAME AS CGS_SESSION_NAME 
  FROM   @_table_sessions_
  LEFT   JOIN ( SELECT   CGS_SESSION_NAME
                       , CGS_CLOSE_DATETIME
                       , CGM_CASHIER_SESSION_ID
                       , CGS_CAGE_SESSION_ID
                       , CGM_MOVEMENT_ID     
                  FROM   CAGE_MOVEMENTS AS CM
                 INNER   JOIN CAGE_SESSIONS ON CM.CGM_CAGE_SESSION_ID = CGS_CAGE_SESSION_ID
                 WHERE   CM.CGM_MOVEMENT_ID = (SELECT    MAX(CGM.CGM_MOVEMENT_ID) 
                                                 FROM    CAGE_MOVEMENTS AS CGM 
                                                WHERE    CGM.CGM_CASHIER_SESSION_ID = CM.CGM_CASHIER_SESSION_ID)
               ) AS X ON X.CGM_CASHIER_SESSION_ID = [@_table_sessions_].SESS_ID

-- SESSIONS CURRENCIES INFORMATION
SELECT   CSC_SESSION_ID
       , CSC_ISO_CODE
       , CSC_TYPE
       , CSC_BALANCE
       , CSC_COLLECTED 
  FROM   CASHIER_SESSIONS_BY_CURRENCY 
 WHERE   CSC_SESSION_ID IN (SELECT SESS_ID FROM @_table_sessions_)

END -- End Procedure
GO

-- PERMISSIONS

GRANT EXECUTE ON [dbo].[SP_Cashier_Sessions_Report] TO [wggui] WITH GRANT OPTION
GO

--
-- CreateCashierMovementsHistoryByHour
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CashierMovementsOldHistoryByHour]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[CashierMovementsOldHistoryByHour]
GO

CREATE PROCEDURE [dbo].[CashierMovementsOldHistoryByHour]
      @HoursToProcess INT OUTPUT
AS
BEGIN
      DECLARE @Date0   DATETIME
    DECLARE @Date1   DATETIME
      DECLARE @Date2   DATETIME

      DECLARE @MaxHoursPerIteration INT
      
      DECLARE @HoursDiff INT
      DECLARE @Counter INT

      DECLARE @LastHourHistorified DATETIME
      DECLARE @CMLastHourNotHistorified DATETIME
      DECLARE @CMFirstHour DATETIME

      DECLARE @Cs_session_id AS BIGINT

      SET NOCOUNT ON;

      -- Read @MaxHoursPerIteration from General Params
      SELECT  @MaxHoursPerIteration = GP_KEY_VALUE 
                        FROM  GENERAL_PARAMS 
                   WHERE  GP_GROUP_KEY   = 'HistoricalData' 
                         AND  GP_SUBJECT_KEY = 'OldCashierSessions.MaxHoursPerIteration'
            
                  SET @MaxHoursPerIteration = ISNULL(@MaxHoursPerIteration, 24)
      SET @HoursToProcess = -1

      -- Default CURRENCY_ISO_CODE
      DECLARE @_CM_CURRENCY_ISO_CODE NVARCHAR(20)
      SELECT @_CM_CURRENCY_ISO_CODE = gp_key_value FROM general_params 
      WHERE gp_group_key = 'RegionalOptions' and gp_subject_key = 'CurrencyISOCode'

      SELECT @LastHourHistorified = MIN(CM_DATE) FROM CASHIER_MOVEMENTS_GROUPED_BY_HOUR

      IF @LastHourHistorified  IS NULL
            RETURN

      SELECT @CMFirstHour = MIN(CM_DATE) FROM CASHIER_MOVEMENTS
      SELECT @CMLastHourNotHistorified = MAX(CM_DATE) FROM CASHIER_MOVEMENTS WHERE CM_DATE < @LastHourHistorified

      SET @HoursDiff = ISNULL(DATEDIFF(HOUR, @CMFirstHour, @CMLastHourNotHistorified),0)
      SET @HoursToProcess = @HoursDiff

      IF @HoursDiff <= 0 
      BEGIN
            IF @CMLastHourNotHistorified IS NOT NULL
            BEGIN
                  SET @HoursDiff = 1
                  SET @HoursToProcess = @HoursDiff
            END
            ELSE
            BEGIN
                  RETURN
            END
      END
      
      -- Gets hour with no minutes or seconds
      SET @Date0 = DATEADD(HOUR, DATEDIFF(HOUR, 0, @CMLastHourNotHistorified), 0)

      IF @HoursDiff > @MaxHoursPerIteration 
            SET @HoursDiff = @MaxHoursPerIteration
      
      SET @Counter = @HoursDiff

      WHILE @Counter >= -1
      BEGIN

            SET @Date1 = DATEADD (HOUR, -@Counter, @Date0)
            SET @Date2 = DATEADD (HOUR, 1, @Date1)
            
            DELETE FROM CASHIER_MOVEMENTS_GROUPED_BY_HOUR WHERE   CM_DATE >= @DATE1 AND CM_DATE <  @DATE2
      
            INSERT INTO   CASHIER_MOVEMENTS_GROUPED_BY_HOUR 
                             ( CM_DATE
                             , CM_TYPE
                             , CM_SUB_TYPE
                             , CM_TYPE_COUNT
                             , CM_CURRENCY_ISO_CODE
                             , CM_CURRENCY_DENOMINATION
                             , CM_SUB_AMOUNT
                             , CM_ADD_AMOUNT
                             , CM_AUX_AMOUNT
                             , CM_INITIAL_BALANCE
                             , CM_FINAL_BALANCE
                             )     
                  SELECT   @Date1 CM_DATE
                             , CM_TYPE
                             , 0                                            AS CM_SUB_TYPE                  --CAIXER MOVEMENT
                             , COUNT(CM_TYPE)                AS CM_TYPE_COUNT
                             , ISNULL(CM_CURRENCY_ISO_CODE, @_CM_CURRENCY_ISO_CODE) AS CM_CURRENCY_ISO_CODE
                             , ISNULL(CM_CURRENCY_DENOMINATION, 0) AS CM_CURRENCY_DENOMINATION
                              , SUM(ISNULL(CM_SUB_AMOUNT,      0)) CM_SUB_AMOUNT
                             , SUM(ISNULL(CM_ADD_AMOUNT,      0)) CM_ADD_AMOUNT
                             , SUM(ISNULL(CM_AUX_AMOUNT,      0)) CM_AUX_AMOUNT
                             , SUM(ISNULL(CM_INITIAL_BALANCE, 0)) CM_INITIAL_BALANCE
                             , SUM(ISNULL(CM_FINAL_BALANCE,   0)) CM_FINAL_BALANCE
                      
                        FROM    CASHIER_MOVEMENTS WITH (INDEX (IX_CM_DATE_TYPE)) 
                        WHERE   CM_DATE >= @Date1
                             AND   CM_DATE <  @Date2
                        GROUP BY   CM_TYPE, ISNULL(CM_CURRENCY_ISO_CODE, @_CM_CURRENCY_ISO_CODE), ISNULL(CM_CURRENCY_DENOMINATION, 0)
           UNION ALL 
                  SELECT  @Date1                                        AS CM_DATE
                     , mbm_type                              AS CM_TYPE 
                     , 1                                                        AS CM_SUB_TYPE         --MBANK MOVEMENT
                     , COUNT(mbm_type)                              AS CM_TYPE_COUNT
                     , @_CM_CURRENCY_ISO_CODE                AS CM_CURRENCY_ISO_CODE
                     , 0                                                        AS CM_CURRENCY_DENOMINATION
                     , SUM(ISNULL(mbm_sub_amount,0))          AS CM_SUB_AMOUNT
                     , SUM(ISNULL(mbm_add_amount,0))          AS CM_ADD_AMOUNT        
                     , 0                                                        AS CM_AUX_AMOUNT 
                     , 0                                                        AS CM_INITIAL_BALANCE
                     , 0                                     AS CM_FINAL_BALANCE
                      
             FROM    MB_MOVEMENTS WITH (INDEX (IX_MBM_DATETIME_TYPE))
            WHERE   mbm_datetime >= @Date1
                        AND   mbm_datetime <  @Date2
            GROUP BY    mbm_type 

      SET @Counter = @Counter -1
      END

RETURN

END--CreateCashierMovementsHistoryByHour
GO


-- Drops
IF OBJECT_ID (N'dbo.SP_GenerateHistoricalMovements_ByHour', N'P') IS NOT NULL
    DROP PROCEDURE dbo.SP_GenerateHistoricalMovements_ByHour;                 
GO  

--
-- SP_GenerateHistoricalMovements_ByHour
--
/*
----------------------------------------------------------------------------------------------------------------
PERFOM HISTORY OF CASHIER AND MOBILE BANK MOVEMENTS PER HOUR

Version  Date           User     Description
----------------------------------------------------------------------------------------------------------------
1.0.0    12-FEB-2014    RMS      First Release
1.0.1    17-FEB-2014    RMS      Added parameter for Currency ISO Code

Parameters:
   -- MAXITEMSPERLOOP:    Maximum number of items to process.
                      
 Results:
   Return the number of items processed and the total of pending items to process before start.
----------------------------------------------------------------------------------------------------------------   
*/

CREATE PROCEDURE [dbo].[SP_GenerateHistoricalMovements_ByHour]
(
@pMaxItemsPerLoop Integer, 
@pCurrecyISOCode VarChar(20)
) 
AS
BEGIN

-- Declarations
DECLARE @_max_movements_per_loop Integer
DECLARE @_total_movements_count   Integer                                   -- To get the total number of items pending to historize
DECLARE @_updated_movements_count Integer                                  -- To get the number of historized movements
DECLARE @_tmp_updating_movements  TABLE  (CMPH_MOVEMENT_ID BigInt,         -- To get the movements id's of items to historize 
                                          CMPH_SUB_TYPE Integer)   
DECLARE @_tmp_updated_movements   TABLE  (UM_DATE DateTime,                -- To get the updated data
                                          UM_TYPE Integer, 
                                          UM_SUBTYPE Integer, 
                                          UM_CURRENCY_ISO_CODE NVarChar(3), 
                                          UM_CURRENCY_DENOMINATION Money)

-- Initialize max items per loop
SET @_max_movements_per_loop = ISNULL(@pMaxItemsPerLoop, 2000) 

-- Get total movements pending of historize
SELECT   @_total_movements_count = COUNT(*) 
  FROM   CASHIER_MOVEMENTS_PENDING_HISTORY 

-- If there are no items in pending movements table
IF @_total_movements_count = 0
BEGIN
   -- Return number of updated movements, and total number of movements
   SELECT CAST(0 AS Integer), CAST(0 AS Integer)
   
   RETURN
END

-- Get the movements id's to be updated
DELETE   TOP(@_max_movements_per_loop) 
  FROM   CASHIER_MOVEMENTS_PENDING_HISTORY 
OUTPUT   DELETED.CMPH_MOVEMENT_ID
       , DELETED.CMPH_SUB_TYPE 
  INTO   @_tmp_updating_movements(CMPH_MOVEMENT_ID, CMPH_SUB_TYPE)   

-- Obtain the number of movements to update
SELECT   @_updated_movements_count = COUNT(*) 
  FROM   @_tmp_updating_movements 

-- If there are items to update process it
IF @_updated_movements_count = 0
BEGIN
   -- Return number of updated movements, and total number of movements
   SELECT CAST(0 AS Integer), @_total_movements_count 
   
   RETURN
END

-- Create a temporary table with grouped items by PK for the CASHIER_MOVEMENTS (SUB_TYPE = 0)
SELECT   X.CM_DATE
       , X.CM_TYPE
       , X.CM_SUB_TYPE
       , X.CM_CURRENCY_ISO_CODE
       , X.CM_CURRENCY_DENOMINATION
       , X.CM_TYPE_COUNT
       , X.CM_SUB_AMOUNT
       , X.CM_ADD_AMOUNT
       , X.CM_AUX_AMOUNT
       , X.CM_INITIAL_BALANCE
       , X.CM_FINAL_BALANCE
  INTO   #TMP_MOVEMENTS_HISTORIZE_PENDING
  FROM
       (
         SELECT   DATEADD(HOUR, DATEDIFF(HOUR, 0, CM_DATE), 0) AS CM_DATE
                , CM_TYPE
                , 0 AS CM_SUB_TYPE
                , ISNULL(CM_CURRENCY_ISO_CODE, @pCurrecyISOCode) AS CM_CURRENCY_ISO_CODE
                , ISNULL(CM_CURRENCY_DENOMINATION, 0)            AS CM_CURRENCY_DENOMINATION
                , COUNT(CM_TYPE)          AS CM_TYPE_COUNT
                , SUM(CM_SUB_AMOUNT)      AS CM_SUB_AMOUNT
                , SUM(CM_ADD_AMOUNT)      AS CM_ADD_AMOUNT
                , SUM(CM_AUX_AMOUNT)      AS CM_AUX_AMOUNT
                , SUM(CM_INITIAL_BALANCE) AS CM_INITIAL_BALANCE
                , SUM(CM_FINAL_BALANCE)   AS CM_FINAL_BALANCE
           FROM   @_tmp_updating_movements
          INNER   JOIN CASHIER_MOVEMENTS ON CMPH_MOVEMENT_ID = CM_MOVEMENT_ID
          WHERE   CMPH_SUB_TYPE = 0      -- Cashier movements
       GROUP BY   DATEADD(HOUR, DATEDIFF(HOUR, 0, CM_DATE), 0)
                , CM_TYPE
                , ISNULL(CM_CURRENCY_ISO_CODE, @pCurrecyISOCode)
                , ISNULL(CM_CURRENCY_DENOMINATION, 0)
       ) AS X 

---- Add to the temporary table items by PK for the MOBILE_BANK_MOVEMENTS (SUB_TYPE = 1)
INSERT INTO   #TMP_MOVEMENTS_HISTORIZE_PENDING
     SELECT   DATEADD(HOUR, DATEDIFF(HOUR, 0, MBM_DATETIME), 0) AS CM_DATE
            , MBM_TYPE   AS CM_TYPE
            , 1          AS CM_SUB_TYPE
            , @pCurrecyISOCode AS CM_CURRENCY_ISO_CODE
            , 0                AS CM_CURRENCY_DENOMINATION
            , COUNT(MBM_TYPE)  AS CM_TYPE_COUNT
            , SUM(MBM_SUB_AMOUNT) AS CM_SUB_AMOUNT
            , SUM(MBM_ADD_AMOUNT) AS CM_ADD_AMOUNT
            , 0 AS CM_AUX_AMOUNT
            , 0 AS CM_INITIAL_BALANCE
            , 0 AS CM_FINAL_BALANCE
       FROM   @_tmp_updating_movements
      INNER   JOIN MB_MOVEMENTS ON CMPH_MOVEMENT_ID = MBM_MOVEMENT_ID
      WHERE   CMPH_SUB_TYPE = 1       -- Mobile Bank movements
   GROUP BY   DATEADD(HOUR, DATEDIFF(HOUR, 0, MBM_DATETIME), 0)
            , MBM_TYPE 

-- Update the items already in database triggering the updates into a temporary table of updated elements
UPDATE   CASHIER_MOVEMENTS_GROUPED_BY_HOUR
   SET   CM_TYPE_COUNT      = T.CM_TYPE_COUNT       + S.CM_TYPE_COUNT
       , CM_SUB_AMOUNT      = T.CM_SUB_AMOUNT       + ISNULL(S.CM_SUB_AMOUNT, 0)
       , CM_ADD_AMOUNT      = T.CM_ADD_AMOUNT       + ISNULL(S.CM_ADD_AMOUNT, 0)
       , CM_AUX_AMOUNT      = T.CM_AUX_AMOUNT       + ISNULL(S.CM_AUX_AMOUNT, 0)
       , CM_INITIAL_BALANCE = T.CM_INITIAL_BALANCE  + ISNULL(S.CM_INITIAL_BALANCE, 0)
       , CM_FINAL_BALANCE   = T.CM_FINAL_BALANCE    + ISNULL(S.CM_FINAL_BALANCE, 0)
OUTPUT   INSERTED.CM_DATE
       , INSERTED.CM_TYPE
       , INSERTED.CM_SUB_TYPE
       , INSERTED.CM_CURRENCY_ISO_CODE
       , INSERTED.CM_CURRENCY_DENOMINATION 
  INTO   @_tmp_updated_movements(UM_DATE, UM_TYPE, UM_SUBTYPE, UM_CURRENCY_ISO_CODE, UM_CURRENCY_DENOMINATION)
  FROM   CASHIER_MOVEMENTS_GROUPED_BY_HOUR T
 INNER   JOIN #TMP_MOVEMENTS_HISTORIZE_PENDING S ON T.CM_DATE                  = S.CM_DATE
                                                AND T.CM_TYPE                  = S.CM_TYPE
                                                AND T.CM_SUB_TYPE              = S.CM_SUB_TYPE
                                                AND T.CM_CURRENCY_ISO_CODE     = S.CM_CURRENCY_ISO_CODE
                                                AND T.CM_CURRENCY_DENOMINATION = S.CM_CURRENCY_DENOMINATION 

-- Insert the elements not previously updated (update items not present in temporary updated items)
INSERT INTO   CASHIER_MOVEMENTS_GROUPED_BY_HOUR
           (   CM_DATE
             , CM_TYPE
             , CM_SUB_TYPE
             , CM_CURRENCY_ISO_CODE
             , CM_CURRENCY_DENOMINATION
             , CM_TYPE_COUNT
             , CM_SUB_AMOUNT     
             , CM_ADD_AMOUNT      
             , CM_AUX_AMOUNT     
             , CM_INITIAL_BALANCE 
             , CM_FINAL_BALANCE  )
     SELECT   CM_DATE
            , CM_TYPE
            , CM_SUB_TYPE
            , CM_CURRENCY_ISO_CODE
            , CM_CURRENCY_DENOMINATION
            , CM_TYPE_COUNT
            , CM_SUB_AMOUNT     
            , CM_ADD_AMOUNT      
            , CM_AUX_AMOUNT     
            , CM_INITIAL_BALANCE 
            , CM_FINAL_BALANCE 
       FROM   #TMP_MOVEMENTS_HISTORIZE_PENDING 
      WHERE   NOT EXISTS(SELECT   1
                           FROM   @_tmp_updated_movements NTI
                          WHERE   NTI.UM_DATE                  = CM_DATE
                            AND   NTI.UM_TYPE                  = CM_TYPE
                            AND   NTI.UM_SUBTYPE               = CM_SUB_TYPE
                            AND   NTI.UM_CURRENCY_ISO_CODE     = CM_CURRENCY_ISO_CODE
                            AND   NTI.UM_CURRENCY_DENOMINATION = CM_CURRENCY_DENOMINATION) 

-- Get total movements pending of historize
SELECT   @_total_movements_count = COUNT(*) 
  FROM   CASHIER_MOVEMENTS_PENDING_HISTORY 
  
-- Return values
SELECT @_updated_movements_count, @_total_movements_count

-- Erase the temporary data
DROP TABLE #TMP_MOVEMENTS_HISTORIZE_PENDING 
--

END  -- End Procedure
GO

/******* RECORDS *******/
IF NOT EXISTS ( SELECT 1 FROM general_params WHERE gp_group_key = 'SiteJackpot' AND gp_subject_key = 'Bonusing.Enabled')
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('SiteJackpot', 'Bonusing.Enabled', 0)
GO

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Account' AND GP_SUBJECT_KEY = 'ZipCode.ValidationRegex')
   INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE)  VALUES ('Account', 'ZipCode.ValidationRegex', '')
GO

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'HistoricalData' AND GP_SUBJECT_KEY = 'OldCashierSessions.Step.WaitSeconds')
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE)  VALUES ('HistoricalData', 'OldCashierSessions.Step.WaitSeconds', '30')
GO
IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'HistoricalData' AND GP_SUBJECT_KEY = 'OldCashierSessions.DateRange')
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE)  VALUES ('HistoricalData', 'OldCashierSessions.DateRange', '0-24')
GO
IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'HistoricalData' AND GP_SUBJECT_KEY = 'OldCashierSessions.MaxHoursPerIteration')
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE)  VALUES ('HistoricalData', 'OldCashierSessions.MaxHoursPerIteration', '24')
GO

IF EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'HistoricalData' AND GP_SUBJECT_KEY = 'Step.WaitSeconds')
      UPDATE GENERAL_PARAMS 
             SET GP_SUBJECT_KEY = 'PendingMovementsByHour.Step.WaitSeconds' 
       WHERE GP_GROUP_KEY    = 'HistoricalData'
            AND GP_SUBJECT_KEY = 'Step.WaitSeconds'
GO          
IF EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'HistoricalData' AND GP_SUBJECT_KEY = 'Step.MaxItems')
      UPDATE GENERAL_PARAMS 
             SET GP_SUBJECT_KEY = 'PendingMovementsByHour.Step.MaxItems' 
       WHERE GP_GROUP_KEY    = 'HistoricalData'
            AND GP_SUBJECT_KEY = 'Step.MaxItems'
GO
