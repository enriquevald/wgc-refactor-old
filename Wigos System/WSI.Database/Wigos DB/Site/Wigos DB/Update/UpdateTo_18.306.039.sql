/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 305;

SET @New_ReleaseId = 306;
SET @New_ScriptName = N'UpdateTo_18.306.039.sql';
SET @New_Description = N'';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/******* TABLES  *******/

--- RECEPCION
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[customers]') AND type in (N'U'))
BEGIN
  CREATE TABLE [dbo].[customers](
	  [cus_customer_id] [bigint] NOT NULL,
   CONSTRAINT [PK_customers] PRIMARY KEY CLUSTERED 
  (
	  [cus_customer_id] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
  ) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[customer_records]') AND type in (N'U'))
BEGIN
  CREATE TABLE [dbo].[customer_records](
	  [cur_record_id] [bigint] IDENTITY(1,1) NOT NULL,
	  [cur_customer_id] [bigint] NOT NULL,
	  [cur_deleted] [bit] NOT NULL,
	  [cur_created] [datetime] NOT NULL,
	  [cur_expiration] [datetime] NULL,
   CONSTRAINT [PK_customer_records] PRIMARY KEY CLUSTERED 
  (
	  [cur_record_id] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
  ) ON [PRIMARY]
END
GO

IF  NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[customer_record_details]') AND type in (N'U'))
BEGIN
  CREATE TABLE [dbo].[customer_record_details](
	  [curd_detail_id] [bigint] IDENTITY(1,1) NOT NULL,
	  [curd_record_id] [bigint] NOT NULL,
	  [curd_deleted] [bit] NOT NULL,
	  [curd_type] [int] NOT NULL,
	  [curd_data] [nvarchar](max) NULL,
	  [curd_created] [datetime] NOT NULL,
	  [curd_expiration] [datetime] NULL,
	  [curd_image] [varbinary](max) NULL,
   CONSTRAINT [PK_customer_record_details] PRIMARY KEY CLUSTERED 
  (
	  [curd_detail_id] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
  ) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[customer_visit_gt_stats]') AND type in (N'U'))
BEGIN
  CREATE TABLE [dbo].[customer_visit_gt_stats](
	  [cvgt_visit_id] [bigint] NOT NULL,
	  [cvgt_playing_time] [int] NOT NULL
  ) ON [PRIMARY]
END
GO

IF  NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[customer_visit_egm_stats]') AND type in (N'U'))
BEGIN
  CREATE TABLE [dbo].[customer_visit_egm_stats](
	  [cve_visit_id] [bigint] NOT NULL,
	  [cve_credit_type] [int] NOT NULL,
	  [cve_total_in] [money] NOT NULL,
	  [cve_total_out] [money] NOT NULL,
	  [cve_coin_in] [money] NOT NULL,
	  [cve_coin_out] [money] NOT NULL,
	  [cve_jackpot] [money] NOT NULL,
	  [cve_theoretical_coin_out] [money] NOT NULL,
	  [cve_playing_time] [int] NULL
  ) ON [PRIMARY]
END
GO

IF  NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[blacklist_internal_block_list]') AND type in (N'U'))
BEGIN
  CREATE TABLE [dbo].[blacklist_internal_block_list](
	  [bkl_id] [bigint] IDENTITY(1,1) NOT NULL,
	  [bkl_account_id] [bigint] NOT NULL,
	  [bkl_inclusion_date] [datetime] NOT NULL,
	  [bkl_reason] [smallint] NOT NULL,
	  [bkl_reason_description] [varchar](max) NULL,
   CONSTRAINT [PK_blacklist_internal_block_list] PRIMARY KEY CLUSTERED 
  (
	  [bkl_id] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
  ) ON [PRIMARY]
END
GO

IF  NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[customer_entrances_prices]') AND type in (N'U'))
BEGIN
  CREATE TABLE [dbo].[customer_entrances_prices](
	  [cuep_price_id] [bigint] IDENTITY(1,1) NOT NULL,
	  [cuep_description] [nvarchar](max) NOT NULL,
	  [cuep_price] [money] NOT NULL,
	  [cuep_customer_level] [int] NULL,
	  [cuep_default] [bit] NOT NULL,
   CONSTRAINT [PK_Customer_Entrances_Prices] PRIMARY KEY CLUSTERED 
  (
	  [cuep_price_id] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
  ) ON [PRIMARY]
END
GO

IF  NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[customer_entrances]') AND type in (N'U'))
BEGIN
  CREATE TABLE [dbo].[customer_entrances](
	[cue_entrance_datetime] [datetime] NOT NULL,
	[cue_visit_id] [bigint] NOT NULL,
	[cue_cashier_session_id] [bigint] NULL,
	[cue_cashier_user_id] [int] NULL,
	[cue_entrance_block_reason] [int] NULL,
	[cue_entrance_block_reason2] [int] NULL,
	[cue_entrance_block_description] [nvarchar](256) NULL,
	[cue_remarks] [nvarchar](256) NULL,
	[cue_ticket_entry_id] [bigint] NULL,
	[cue_ticket_entry_price_real] [money] NULL,
	[cue_document_type] [int] NULL,
	[cue_document_number] [nvarchar](50) NULL,
	[cue_voucher_id] [bigint] NULL,
	[cue_cashier_terminal_id] [bigint] NULL,
	[cue_coupon] [nchar](250) NULL,
	[cue_voucher_sequence] [bigint] NULL,
	[cue_entrance_expiration] [datetime] NULL,
	[cue_ticket_entry_price_paid] [money] NULL,
	[cue_ticket_entry_price_difference] [money] NULL,
 CONSTRAINT [PK_customer_entrances] PRIMARY KEY CLUSTERED 
(
	[cue_entrance_datetime] ASC,
	[cue_visit_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[customer_entrances]  WITH CHECK ADD  CONSTRAINT [FK_customer_entrances_customer_visits] FOREIGN KEY([cue_visit_id])
REFERENCES [dbo].[customer_visits] ([cut_visit_id])

ALTER TABLE [dbo].[customer_entrances] CHECK CONSTRAINT [FK_customer_entrances_customer_visits]

ALTER TABLE [dbo].[customer_entrances] ADD  CONSTRAINT [DF_customer_entrances_ce_entrance_datetime]  DEFAULT (getdate()) FOR [cue_entrance_datetime]

END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[blacklist_internal_block_list]') AND type in (N'U'))
BEGIN
  CREATE TABLE [dbo].[blacklist_internal_block_list](
	  [bkl_id] [bigint] IDENTITY(1,1) NOT NULL,
	  [bkl_account_id] [bigint] NOT NULL,
	  [bkl_inclusion_date] [datetime] NOT NULL,
	  [bkl_reason] [smallint] NOT NULL,
	  [bkl_reason_description] [varchar](max) NULL,
   CONSTRAINT [PK_blacklist_internal_block_list] PRIMARY KEY CLUSTERED 
  (
	  [bkl_id] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
  ) ON [PRIMARY]
END
GO

IF  NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[blacklist_file_imported]') AND type in (N'U'))
BEGIN
  CREATE TABLE [dbo].[blacklist_file_imported](
	  [blkf_id] [int] IDENTITY(1,1) NOT NULL,
	  [blkf_name] [nvarchar](50) NOT NULL,
	  [blkf_middle_name] [nvarchar](50) NULL,
	  [blkf_lastname_1] [nvarchar](50) NOT NULL,
	  [blkf_lastname_2] [nvarchar](50) NULL,
	  [blkf_document_type] [smallint] NOT NULL,
	  [blkf_document] [nvarchar](20) NOT NULL,
	  [bklf_exclusion_date] [datetime] NOT NULL,
	  [bklf_reason_type] [smallint] NULL,
	  [bklf_reason_description] [nvarchar](max) NOT NULL,
	  [bklf_exclusion_duration] [int] NULL,
	  [bklf_origin] [smallint] NULL,
	  [blkf_reference] [nvarchar](50) NULL
  ) ON [PRIMARY]
END
GO

/** DELETE CONSTRAINTS **/

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_customer_entrances_customer_visits]') AND parent_object_id = OBJECT_ID(N'[dbo].[customer_entrances]'))
ALTER TABLE [dbo].[customer_entrances] DROP CONSTRAINT [FK_customer_entrances_customer_visits]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_customer_record_details_customer_records]') AND parent_object_id = OBJECT_ID(N'[dbo].[customer_record_details]'))
ALTER TABLE [dbo].[customer_record_details] DROP CONSTRAINT [FK_customer_record_details_customer_records]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_customer_record_details_history_customer_records_history]') AND parent_object_id = OBJECT_ID(N'[dbo].[customer_record_details_history]'))
ALTER TABLE [dbo].[customer_record_details_history] DROP CONSTRAINT [FK_customer_record_details_history_customer_records_history]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_customer_records_customers]') AND parent_object_id = OBJECT_ID(N'[dbo].[customer_records]'))
ALTER TABLE [dbo].[customer_records] DROP CONSTRAINT [FK_customer_records_customers]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_customer_records_history_customers]') AND parent_object_id = OBJECT_ID(N'[dbo].[customer_records_history]'))
ALTER TABLE [dbo].[customer_records_history] DROP CONSTRAINT [FK_customer_records_history_customers]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_customer_visit_egm_stats_customer_visits]') AND parent_object_id = OBJECT_ID(N'[dbo].[customer_visit_egm_stats]'))
ALTER TABLE [dbo].[customer_visit_egm_stats] DROP CONSTRAINT [FK_customer_visit_egm_stats_customer_visits]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_customer_visit_gt_stats_customer_visits]') AND parent_object_id = OBJECT_ID(N'[dbo].[customer_visit_gt_stats]'))
ALTER TABLE [dbo].[customer_visit_gt_stats] DROP CONSTRAINT [FK_customer_visit_gt_stats_customer_visits]
GO

/******* INDEXES *******/

/** INDICES **/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[customer_entrances]') AND name = N'IX_cue_visit_id')
BEGIN
  CREATE NONCLUSTERED INDEX [IX_cue_visit_id] ON [dbo].[customer_entrances] 
  (
	  [cue_visit_id] ASC
  )
  INCLUDE ( [cue_cashier_user_id],
  [cue_ticket_entry_price_real],
  [cue_ticket_entry_price_paid]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
END
GO

IF  NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[customer_entrances]') AND name = N'IX_dta_index_customer_entrances_cue_visit_id')
BEGIN
  CREATE NONCLUSTERED INDEX [IX_dta_index_customer_entrances_cue_visit_id] ON [dbo].[customer_entrances] 
  (
	  [cue_visit_id] ASC
  )
  INCLUDE ( [cue_ticket_entry_price_real],
  [cue_ticket_entry_price_paid]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
END
GO

IF  NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[customer_visits]') AND name = N'IX_cut_customer_id__cut_gaming_day__cut_visit_id')
BEGIN
  CREATE NONCLUSTERED INDEX [IX_cut_customer_id__cut_gaming_day__cut_visit_id] ON [dbo].[customer_visits] 
  (
	  [cut_customer_id] ASC,
	  [cut_gaming_day] ASC,
	  [cut_visit_id] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[customer_visits]') AND name = N'IX_cut_gaming_day')
BEGIN
  CREATE NONCLUSTERED INDEX [IX_cut_gaming_day] ON [dbo].[customer_visits] 
  (
	  [cut_gaming_day] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
END
GO

IF  NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[customer_visits]') AND name = N'IX_cut_gaming_day_cut_visit_id')
BEGIN
  CREATE NONCLUSTERED INDEX [IX_cut_gaming_day_cut_visit_id] ON [dbo].[customer_visits] 
  (
	  [cut_gaming_day] ASC,
	  [cut_visit_id] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
END
GO

IF  NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[customer_visits]') AND name = N'IX_cut_visit_id_cut_gaming_day')
BEGIN
  CREATE NONCLUSTERED INDEX [IX_cut_visit_id_cut_gaming_day] ON [dbo].[customer_visits] 
  (
	  [cut_visit_id] ASC,
	  [cut_gaming_day] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
END
GO

/******* RECORDS *******/

/** GENERAL PARAMS RECEPTION/VISITS MODULE **/

IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='Reception' AND GP_SUBJECT_KEY = 'Enabled')
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Reception', 'Enabled', '0')
GO   
IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='Reception' AND GP_SUBJECT_KEY = 'RequireNewCard')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Reception', 'RequireNewCard',1)
GO
IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='Reception' AND GP_SUBJECT_KEY = 'CardFunctionsEnabled') 	
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Reception', 'CardFunctionsEnabled',1)
GO
IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='Account.VisibleField' AND GP_SUBJECT_KEY = 'Photo') 	
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Account.VisibleField','Photo',1)
GO
IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='Account.RequestedField' AND GP_SUBJECT_KEY = 'Photo') 	
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Account.RequestedField','Photo',0)
GO
IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='Account.RequestedField' AND GP_SUBJECT_KEY = 'AntiMoneyLaundering.Photo') 	
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Account.RequestedField','AntiMoneyLaundering.Photo',0)
GO
IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Reception' AND GP_SUBJECT_KEY ='Agreement.Text')
	INSERT INTO general_params (gp_group_key, gp_subject_key, gp_key_value)VALUES ('Reception', 'Agreement.Text', '');
GO
IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Reception' AND GP_SUBJECT_KEY ='Agreement.ShowMode')
	INSERT INTO general_params (gp_group_key, gp_subject_key, gp_key_value) VALUES ('Reception', 'Agreement.ShowMode', '0');
GO
IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.Voucher' AND GP_SUBJECT_KEY ='Reception.Footer')
	INSERT INTO general_params (gp_group_key, gp_subject_key, gp_key_value) VALUES ('Cashier.Voucher', 'Reception.Footer', '');
GO
IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Reception' AND GP_SUBJECT_KEY ='Record.ExpirationDays')
	INSERT INTO general_params (gp_group_key, gp_subject_key, gp_key_value) VALUES ('Reception', 'Record.ExpirationDays', '90');
GO
IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Reception' AND GP_SUBJECT_KEY ='Mode')
	INSERT INTO general_params (gp_group_key, gp_subject_key, gp_key_value) VALUES ('Reception', 'Mode', '0');
GO
IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Reception' AND GP_SUBJECT_KEY ='TimeBeforeEntry')
	INSERT INTO general_params (gp_group_key, gp_subject_key, gp_key_value) VALUES ('Reception', 'TimeBeforeEntry', '0');
GO

/******* PROCEDURES *******/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Trigger_SiteToMultiSite_Points]') AND type in (N'TR'))
DROP TRIGGER [dbo].[Trigger_SiteToMultiSite_Points]
GO

CREATE TRIGGER [dbo].[Trigger_SiteToMultiSite_Points]
ON [dbo].[ACCOUNT_MOVEMENTS]
AFTER INSERT
NOT FOR REPLICATION
AS
  BEGIN
  
    IF NOT EXISTS (SELECT   1 
                     FROM   GENERAL_PARAMS 
                    WHERE   GP_GROUP_KEY   = N'Site' 
                      AND   GP_SUBJECT_KEY = N'MultiSiteMember' 
                      AND   GP_KEY_VALUE   = N'1')
    BEGIN
          RETURN
    END
  
    SET NOCOUNT ON

    -- Insert movement to synchronize
    INSERT INTO   MS_SITE_PENDING_ACCOUNT_MOVEMENTS 
                ( SPM_MOVEMENT_ID )
         SELECT   AM_MOVEMENT_ID 
           FROM   INSERTED 
          WHERE   AM_TYPE IN ( 36, 37, 38, 39, 40, 41, 42, 46, 50, 60, 61, 62, 66, 67, 68, 71, 72, 73, 84, 86, 101, 1101, 1201, 1301, 1401, 1501)
							   
    SET NOCOUNT OFF

  END -- [Trigger_SiteToMultiSite_Points]
GO
 
--
-- Disable/enable triggers for site members
--

IF EXISTS (SELECT   1
             FROM   GENERAL_PARAMS 
            WHERE   GP_GROUP_KEY   = N'Site' 
              AND   GP_SUBJECT_KEY = N'MultiSiteMember' 
              AND   GP_KEY_VALUE   = N'1')
BEGIN
      EXEC MultiSiteTriggersEnable 1
END
ELSE
BEGIN
      EXEC MultiSiteTriggersEnable 0
END
GO
 
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Update_PointsInAccount]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Update_PointsInAccount]
GO

CREATE PROCEDURE [dbo].[Update_PointsInAccount]
  @pMovementId                 BIGINT
, @pAccountId                  BIGINT
, @pErrorCode                  INT         
, @pPointsSequenceId           BIGINT      
, @pReceivedPoints             MONEY  
, @pPlayerTrackingMode         INT         

AS
BEGIN   
  DECLARE @LocalDelta       AS MONEY
  DECLARE @ActualPoints     AS MONEY
  DECLARE @ReceivedAddLocal AS MONEY
  DECLARE @NewPoints        AS MONEY
  DECLARE @bucket_puntos_canje AS INT
  DECLARE @TableTemp table ( T_VALUE DECIMAL);
	
  SET @bucket_puntos_canje = 1

  IF NOT EXISTS (SELECT 1 FROM ACCOUNTS WHERE ac_account_id = @pAccountId )   
    INSERT INTO   ACCOUNTS 
                ( AC_ACCOUNT_ID, AC_TYPE, AC_BLOCKED, AC_TRACK_DATA) 
         VALUES ( @pAccountId,         2,          1, '-RECYCLED-NEW-' + CAST (NEWID() AS NVARCHAR(50)) )
         
  IF ((@pMovementId <> 0)AND(@pErrorCode = 0 OR @pErrorCode = 2))
    DELETE   MS_SITE_PENDING_ACCOUNT_MOVEMENTS
     WHERE   SPM_MOVEMENT_ID = @pMovementId
  
  IF (@pErrorCode <> 0) RETURN

  IF (@pPlayerTrackingMode = 1) RETURN

  DECLARE @PrevSequence as BIGINT
  
  -- Lock the account and later add the local points
  UPDATE ACCOUNTS SET AC_TYPE = AC_TYPE + 0 WHERE AC_ACCOUNT_ID = @pAccountId
  UPDATE CUSTOMER_BUCKET SET CBU_VALUE = CBU_VALUE + 0 OUTPUT INSERTED.CBU_VALUE INTO @TableTemp WHERE CBU_CUSTOMER_ID = @pAccountId AND CBU_BUCKET_ID = @bucket_puntos_canje

  -- SET @PrevSequence = (SELECT ISNULL(AC_MS_POINTS_SEQ_ID, 0) FROM ACCOUNTS WHERE AC_ACCOUNT_ID = @pAccountId)
  SELECT   @PrevSequence = ISNULL(AC_MS_POINTS_SEQ_ID, 0) 
    FROM   ACCOUNTS 
   WHERE   AC_ACCOUNT_ID = @pAccountId

   SELECT @ActualPoints = T_VALUE FROM @TableTemp
   SET @ActualPoints = ISNULL(@ActualPoints, 0)
   
  IF ( @PrevSequence > @pPointsSequenceId ) RETURN
  
-- Local Points
  SELECT   @LocalDelta = SUM(am_add_amount - am_sub_amount) 
    FROM   ACCOUNT_MOVEMENTS, MS_SITE_PENDING_ACCOUNT_MOVEMENTS
   WHERE   AM_MOVEMENT_ID = SPM_MOVEMENT_ID
     AND   AM_ACCOUNT_ID  = @pAccountId
     AND   AM_TYPE IN (36,37,38,40,41,46,50,60,61,66,1101,1201,1301,1401,1501 ) -- Not included level movements,(39)PointsGiftDelivery, 
                                                      -- (42)PointsGiftServices and (67)PointsStatusChanged

--  36, 'PointsAwarded'
--  37, 'PointsToGiftRequest'
--  38, 'PointsToNotRedeemable'
--  40, 'PointsExpired'
--  41, 'PointsToDrawTicketPrint'
--  46, 'PointsToRedeemable'
--  50, 'CardAdjustment'
--  60, 'PromotionPoint'
--  61, 'CancelPromotionPoint'
--  66, 'CancelGiftInstance' 
-- 1101, 'MULTIPLE_BUCKETS_END_SESSION + PuntosNivel'
-- 1201, 'MULTIPLE_BUCKETS_Expired    RedemptionPoints --PuntosCanje
-- 1301, 'MULTIPLE_BUCKETS_Manual_Add RedemptionPoints --PuntosCanje
-- 1401, 'MULTIPLE_BUCKETS_Manual_Sub RedemptionPoints --PuntosCanje
-- 1501, 'MULTIPLE_BUCKETS_Manual_Set RedemptionPoints --PuntosCanje





  
  SET @ReceivedAddLocal = @pReceivedPoints + ISNULL(@LocalDelta , 0) 
  
  SET @NewPoints = CASE WHEN (ISNULL(@LocalDelta , 0) <> 0 AND @ActualPoints    <= @pReceivedPoints AND @ActualPoints    <= @ReceivedAddLocal)
                             THEN @ActualPoints
                        WHEN (ISNULL(@LocalDelta , 0) <> 0 AND @pReceivedPoints <= @ActualPoints    AND @pReceivedPoints <= @ReceivedAddLocal)
                             THEN @pReceivedPoints
                        ELSE @ReceivedAddLocal 
                        END 
/*
  UPDATE   ACCOUNTS
     SET   AC_POINTS                  = @NewPoints
         , AC_MS_POINTS_SEQ_ID        = @pPointsSequenceId
         , AC_MS_POINTS_SYNCHRONIZED  = GETDATE ()
   WHERE   AC_ACCOUNT_ID                   = @pAccountId
     AND   ISNULL(AC_MS_POINTS_SEQ_ID, 0) <= @pPointsSequenceId
*/
  UPDATE   ACCOUNTS
     SET   AC_MS_POINTS_SEQ_ID        = @pPointsSequenceId
         , AC_MS_POINTS_SYNCHRONIZED  = GETDATE ()
   WHERE   AC_ACCOUNT_ID                   = @pAccountId
     AND   ISNULL(AC_MS_POINTS_SEQ_ID, 0) <= @pPointsSequenceId

	 IF @@ROWCOUNT = 1
		UPDATE CUSTOMER_BUCKET SET CBU_VALUE = @NewPoints 
		 WHERE CBU_CUSTOMER_ID = @pAccountId AND CBU_BUCKET_ID = @bucket_puntos_canje 

END
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AccountPointsCache_CalculatePoints]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[AccountPointsCache_CalculatePoints]
GO

CREATE PROCEDURE [dbo].[AccountPointsCache_CalculatePoints]
       @AccountId                        BIGINT,
       @DateFrom                         DATETIME,
       @DateTo                           DATETIME,
       @MovementId                       BIGINT, 
       @LastMovementId                   BIGINT OUTPUT, 
       @PointsGeneratedForLevel          MONEY  OUTPUT,
       @PointsDiscretionalForLevel       MONEY  OUTPUT,
       @PointsDiscretionalOnlyForRedeem  MONEY  OUTPUT,
       @PointsPromotionOnlyForRedeem     MONEY  OUTPUT
AS
BEGIN
  DECLARE @points_awarded                        AS INT;
  DECLARE @manually_added_points_for_level       AS INT;
  DECLARE @imported_points_for_level             AS INT;
  DECLARE @imported_points_history               AS INT;
  DECLARE @manually_added_points_only_for_redeem AS INT;
  DECLARE @imported_points_only_for_redeem       AS INT;
  DECLARE @promotion_point                       AS INT;
  DECLARE @cancel_promotion_point                AS INT;
  -- 11-FEB-2016 JRC    PBI 7909: Multiple Buckets. 
  DECLARE @EndSession_RankingLevelPoints		AS INT;	
  DECLARE @Expired_RankingLevelPoints		    AS INT
  DECLARE @Manual_Add_RankingLevelPoints		AS INT
  DECLARE @Manual_Sub_RankingLevelPoints		AS INT
  DECLARE @Manual_Set_RankingLevelPoints		AS INT
  DECLARE @EndSession_RedemptionPoints			AS INT
  DECLARE @Expired_RedemptionPoints				AS INT
  DECLARE @Manual_Add_RedemptionPoints			AS INT
  DECLARE @Manual_Sub_RedemptionPoints			AS INT
  DECLARE @Manual_Set_RedemptionPoints			AS INT
  


  DECLARE @Sql             AS NVARCHAR(MAX);
  DECLARE @ParamDefinition AS NVARCHAR(MAX);
  DECLARE @Index           AS NVARCHAR(MAX);
  DECLARE @Where           AS NVARCHAR(MAX); 
  
  -- Points for level
     -- Generated
  SET @points_awarded                        = 36
  --PointsAwarded
  
     -- Discretional
  
  SET @manually_added_points_for_level       = 68
  --ManuallyAddedPointsForLevel
  
  SET @imported_points_for_level             = 72
  --ImportedPointsForLevel

  SET @imported_points_history               = 73
  --ImportedPointsHistory

  -- Points only for redeem
     -- Discretional 
  SET @manually_added_points_only_for_redeem = 50
  --ManuallyAddedPointsOnlyForRedeem

  SET @imported_points_only_for_redeem       = 71
  --ImportedPointsOnlyForRedeem

     -- Promotion 
  SET @promotion_point                       = 60
  --PromotionPoint
  
  SET @cancel_promotion_point                = 61
  --CancelPromotionPoint 

  SET @EndSession_RankingLevelPoints		     = 1102
  --'MULTIPLE_BUCKETS_END_SESSION + RankingLevelPoints PuntosNivel'
  SET @Expired_RankingLevelPoints		         = 1202
  --'MULTIPLE_BUCKETS_Expired + RankingLevelPoints PuntosNivel'
  SET @Manual_Add_RankingLevelPoints		     = 1302
  --'MULTIPLE_BUCKETS_Manual_Add + RankingLevelPoints PuntosNivel'
  SET @Manual_Sub_RankingLevelPoints		     = 1402
  --'MULTIPLE_BUCKETS_Manual_Sub + RankingLevelPoints PuntosNivel'
  SET @Manual_Set_RankingLevelPoints		     = 1502
  --'MULTIPLE_BUCKETS_Manual_Set + RankingLevelPoints PuntosNivel'


  SET @EndSession_RedemptionPoints		     = 1101
  --'MULTIPLE_BUCKETS_END_SESSION + RedemptionPoints PuntosCanje'
  SET @Expired_RedemptionPoints		         = 1201
  --'MULTIPLE_BUCKETS_Expired + RedemptionPoints PuntosCanje'
  SET @Manual_Add_RedemptionPoints		     = 1301
  --'MULTIPLE_BUCKETS_Manual_Add + RedemptionPoints PuntosCanje'
  SET @Manual_Sub_RedemptionPoints		     = 1401
  --'MULTIPLE_BUCKETS_Manual_Sub + RedemptionPoints PuntosCanje'
  SET @Manual_Set_RedemptionPoints		     = 1501
  --'MULTIPLE_BUCKETS_Manual_Set + RedemptionPoints PuntosCanje'




  
  SET @LastMovementId = @MovementId

    SET @Index = 'IX_am_account_id_type_datetime'
    SET @Where = ' WHERE AM_ACCOUNT_ID  = @AccountId ' +
                 '   AND AM_TYPE IN (@points_awarded, @manually_added_points_for_level, @imported_points_for_level, @imported_points_history, ' +
				 '                   @EndSession_RankingLevelPoints, @Expired_RankingLevelPoints, @Manual_Add_RankingLevelPoints, @Manual_Sub_RankingLevelPoints, @Manual_Set_RankingLevelPoints,' +
				 '                   @EndSession_RedemptionPoints, @Expired_RedemptionPoints, @Manual_Add_RedemptionPoints, @Manual_Sub_RedemptionPoints, @Manual_Set_RedemptionPoints,' +
                 '                   @manually_added_points_only_for_redeem, @imported_points_only_for_redeem, @promotion_point, @cancel_promotion_point) ' + 
                 '   AND AM_DATETIME >= CAST(''' + CAST(@DateFrom AS VARCHAR(50)) + ''' AS DATETIME) ' +
                 '   AND AM_DATETIME  < CAST(''' + CAST(@DateTo AS VARCHAR(50)) + ''' AS DATETIME) ' +
                 '   AND AM_MOVEMENT_ID > ' + CAST(@MovementId AS NVARCHAR(MAX)) 

  SET @Sql = '
  SELECT   @PointsGeneratedForLevel_out          = ISNULL(SUM (CASE WHEN AM_TYPE IN (@points_awarded,@EndSession_RankingLevelPoints, @Expired_RankingLevelPoints)
                                                               THEN AM_ADD_AMOUNT-AM_SUB_AMOUNT 
                                                               ELSE 0 
                                                               END ), 0) 
         , @PointsDiscretionalForLevel_out       = ISNULL(SUM (CASE WHEN AM_TYPE IN (@manually_added_points_for_level, @imported_points_for_level, @imported_points_history, @Manual_Add_RankingLevelPoints, @Manual_Sub_RankingLevelPoints, @Manual_Set_RankingLevelPoints)
                                                               THEN AM_ADD_AMOUNT-AM_SUB_AMOUNT 
                                                               ELSE 0 
                                                               END ), 0) 
         , @PointsDiscretionalOnlyForRedeem_out  = ISNULL(SUM (CASE WHEN AM_TYPE IN (@manually_added_points_only_for_redeem, @imported_points_only_for_redeem, @Manual_Add_RedemptionPoints, @Manual_Sub_RedemptionPoints, @Manual_Set_RedemptionPoints)
                                                               THEN AM_ADD_AMOUNT-AM_SUB_AMOUNT 
                                                               ELSE 0 
                                                               END ), 0)
         , @PointsPromotionOnlyForRedeem_out     = ISNULL(SUM (CASE WHEN AM_TYPE IN (@promotion_point, @cancel_promotion_point,@EndSession_RedemptionPoints, @Expired_RedemptionPoints)
                                                               THEN AM_ADD_AMOUNT-AM_SUB_AMOUNT
                                                               ELSE 0 
                                                               END ), 0) 
         , @LastMovementId_out                   = ISNULL(MAX (AM_MOVEMENT_ID), @LastMovementId_out)
    FROM   ACCOUNT_MOVEMENTS WITH (INDEX (' + @Index + ')) ' 
  + @Where
 
SET @ParamDefinition = N'@AccountId                             BIGINT, 
                        @points_awarded                         INT, 
                        @manually_added_points_for_level        INT, 
                        @imported_points_for_level              INT, 
                        @imported_points_history                INT, 
                        @manually_added_points_only_for_redeem  INT, 
                        @imported_points_only_for_redeem        INT, 
                        @promotion_point                        INT, 
                        @cancel_promotion_point                 INT, 
						@EndSession_RankingLevelPoints			INT,
						@Expired_RankingLevelPoints				INT,	
						@Manual_Add_RankingLevelPoints			INT,
						@Manual_Sub_RankingLevelPoints			INT,
						@Manual_Set_RankingLevelPoints			INT,
						@EndSession_RedemptionPoints			INT,	
						@Expired_RedemptionPoints				INT,	
						@Manual_Add_RedemptionPoints			INT,	
						@Manual_Sub_RedemptionPoints			INT,	
						@Manual_Set_RedemptionPoints			INT,	
                        @LastMovementId_out                     BIGINT OUTPUT, 
                        @PointsGeneratedForLevel_out            MONEY  OUTPUT, 
                        @PointsDiscretionalForLevel_out         MONEY  OUTPUT, 
                        @PointsDiscretionalOnlyForRedeem_out    MONEY  OUTPUT, 
                        @PointsPromotionOnlyForRedeem_out       MONEY  OUTPUT' 

EXEC sp_executesql @Sql, 
					@ParamDefinition,
					@AccountId                             = @AccountId, 
					@points_awarded                        = @points_awarded,                        
					@manually_added_points_for_level       = @manually_added_points_for_level,
					@imported_points_for_level             = @imported_points_for_level,
					@imported_points_history               = @imported_points_history, 
					@manually_added_points_only_for_redeem = @manually_added_points_only_for_redeem, 
					@imported_points_only_for_redeem       = @imported_points_only_for_redeem, 
					@promotion_point                       = @promotion_point,
					@cancel_promotion_point                = @cancel_promotion_point,
					@EndSession_RankingLevelPoints    =   @EndSession_RankingLevelPoints, 
					@Expired_RankingLevelPoints	  =   @Expired_RankingLevelPoints,
					@Manual_Add_RankingLevelPoints    =   @Manual_Add_RankingLevelPoints ,
					@Manual_Sub_RankingLevelPoints    =   @Manual_Sub_RankingLevelPoints ,
					@Manual_Set_RankingLevelPoints    =   @Manual_Set_RankingLevelPoints ,
					@EndSession_RedemptionPoints	  =   @EndSession_RedemptionPoints	,
					@Expired_RedemptionPoints	  =   @Expired_RedemptionPoints		,
					@Manual_Add_RedemptionPoints	  =   @Manual_Add_RedemptionPoints	,
					@Manual_Sub_RedemptionPoints	  =   @Manual_Sub_RedemptionPoints	,
					@Manual_Set_RedemptionPoints	  =   @Manual_Set_RedemptionPoints	,
					@LastMovementId_out                    = @LastMovementId                   OUTPUT, 
					@PointsGeneratedForLevel_out           = @PointsGeneratedForLevel          OUTPUT,
					@PointsDiscretionalForLevel_out        = @PointsDiscretionalForLevel       OUTPUT,
					@PointsDiscretionalOnlyForRedeem_out   = @PointsDiscretionalOnlyForRedeem  OUTPUT,
					@PointsPromotionOnlyForRedeem_out      = @PointsPromotionOnlyForRedeem     OUTPUT 

---------------------------------------------------------------------------------------------------------------------------------
-- Note:
-- @LastMovementId_out : Is the variable�s name of the output parameter of '@Sql execute'
-- When run 'EXEC sp_executesql' is finished, the value of @LastMovementId_out is assigned to @LastMovementId
---------------------------------------------------------------------------------------------------------------------------------

END  -- PROCEDURE [dbo].[AccountPointsCache_CalculatePoints]
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[v_TotalVisitsByDay]'))
DROP VIEW [dbo].[v_TotalVisitsByDay]
GO
CREATE VIEW [dbo].[v_TotalVisitsByDay]
AS
SELECT     TOP (100) PERCENT dbo.customer_visits.cut_gaming_day AS SQL_COLUMN_GAMING_DAY, COUNT(DISTINCT dbo.customer_visits.cut_visit_id) AS TotalByDay, 
                      '' AS Colum1, '' AS Colum2, 0 AS Column3, SUM(ISNULL(dbo.customer_entrances.cue_ticket_entry_price_real, 0)) AS TotalTeoricByDay, 
                      SUM(ISNULL(dbo.customer_entrances.cue_ticket_entry_price_paid, 0)) AS TotalRealByDay, 
                      SUM(ISNULL(ISNULL(dbo.customer_entrances.cue_ticket_entry_price_real, 0) - ISNULL(dbo.customer_entrances.cue_ticket_entry_price_paid, 0), 0)) 
                      AS SQL_COLUMN_DIFFERENCE_ACCUMULATE, COUNT(dbo.customer_visits.cut_visit_id) AS TotalEntries
FROM         dbo.customer_visits INNER JOIN
                      dbo.customer_entrances ON dbo.customer_visits.cut_visit_id = dbo.customer_entrances.cue_visit_id
GROUP BY dbo.customer_visits.cut_gaming_day
ORDER BY SQL_COLUMN_GAMING_DAY

GO

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[32] 4[38] 2[27] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1[50] 4[25] 3) )"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1[50] 2[25] 3) )"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = -364
      End
      Begin Tables = 
         Begin Table = "customer_visits"
            Begin Extent = 
               Top = 21
               Left = 506
               Bottom = 237
               Right = 741
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "customer_entrances"
            Begin Extent = 
               Top = 68
               Left = 839
               Bottom = 274
               Right = 1250
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 10
         Width = 284
         Width = 2280
         Width = 2970
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1455
         Width = 1500
         Width = 3315
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 13860
         Alias = 3345
         Table = 1335
         Output = 705
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'v_TotalVisitsByDay'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'v_TotalVisitsByDay'
GO


/****** Object:  View [dbo].[v_TotalEntrancesByVisits]    Script Date: 02/18/2016 13:12:11 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[v_TotalEntrancesByVisits]'))
  DROP VIEW [dbo].[v_TotalEntrancesByVisits]
GO

CREATE VIEW [dbo].[v_TotalEntrancesByVisits]
AS
SELECT     TOP (100) PERCENT dbo.v_TotalVisitsByDay.SQL_COLUMN_GAMING_DAY, dbo.v_TotalVisitsByDay.TotalByDay AS SQL_COLUMN_VISIT_NUMBER, 
                      dbo.accounts.ac_holder_name AS SQL_COLUMN_HOLDER_NAME, dbo.TrackDataToExternal(dbo.accounts.ac_track_data) AS SQL_COLUMN_CARD_TRACK, 
                      COUNT(dbo.customer_entrances.cue_visit_id) AS SQL_COLUMN_ENTRIES, dbo.v_TotalVisitsByDay.TotalTeoricByDay AS SQL_COLUMN_THEORICAL_BY_DAY, 
                      dbo.v_TotalVisitsByDay.TotalRealByDay AS SQL_COLUMN_REAL_BY_DAY, 
                      dbo.v_TotalVisitsByDay.TotalRealByDay - dbo.v_TotalVisitsByDay.TotalTeoricByDay AS SQL_COLUMN_DIFFERENCE_BY_DAY, 
                      SUM(ISNULL(dbo.customer_entrances.cue_ticket_entry_price_real, 0)) AS SQL_COLUMN_THEORICAL_COLLECTION, 
                      SUM(ISNULL(dbo.customer_entrances.cue_ticket_entry_price_paid, 0)) AS SQL_COLUMN_REAL_COLLECTION, 
                      SUM(ISNULL(dbo.customer_entrances.cue_ticket_entry_price_paid, 0) - ISNULL(dbo.customer_entrances.cue_ticket_entry_price_real, 0)) 
                      AS SQL_COLUMN_DIFFERENCE
FROM         dbo.accounts INNER JOIN
                      dbo.customer_visits ON dbo.accounts.ac_account_id = dbo.customer_visits.cut_customer_id INNER JOIN
                      dbo.v_TotalVisitsByDay ON dbo.customer_visits.cut_gaming_day = dbo.v_TotalVisitsByDay.SQL_COLUMN_GAMING_DAY INNER JOIN
                      dbo.customer_entrances ON dbo.customer_visits.cut_visit_id = dbo.customer_entrances.cue_visit_id
GROUP BY dbo.v_TotalVisitsByDay.TotalByDay, dbo.accounts.ac_holder_name, dbo.TrackDataToExternal(dbo.accounts.ac_track_data), 
                      dbo.v_TotalVisitsByDay.TotalTeoricByDay, dbo.v_TotalVisitsByDay.TotalRealByDay, 
                      dbo.v_TotalVisitsByDay.TotalRealByDay - dbo.v_TotalVisitsByDay.TotalTeoricByDay, dbo.v_TotalVisitsByDay.SQL_COLUMN_GAMING_DAY
ORDER BY dbo.v_TotalVisitsByDay.SQL_COLUMN_GAMING_DAY DESC

GO

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[33] 4[24] 2[22] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1[45] 4[30] 3) )"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1[56] 3) )"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1[70] 4) )"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "customer_visits"
            Begin Extent = 
               Top = 9
               Left = 765
               Bottom = 236
               Right = 1071
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "accounts"
            Begin Extent = 
               Top = 218
               Left = 277
               Bottom = 385
               Right = 564
            End
            DisplayFlags = 280
            TopColumn = 3
         End
         Begin Table = "customer_entrances"
            Begin Extent = 
               Top = 1
               Left = 1179
               Bottom = 290
               Right = 1433
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "v_TotalVisitsByDay"
            Begin Extent = 
               Top = 5
               Left = 254
               Bottom = 200
               Right = 553
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 12
         Width = 284
         Width = 2385
         Width = 2385
         Width = 3060
         Width = 2805
         Width = 2805
         Width = 2805
         Width = 2805
         Width = 3420
         Width = 3180
         Width = 2700
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 10620
         Alias = 3210
         Table = 3330
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'v_TotalEntrancesByVisits'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'= 1350
         Filter = 4125
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'v_TotalEntrancesByVisits'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'v_TotalEntrancesByVisits'
GO


/****** Object:  View [dbo].[v_TotalEntriesByMonth]    Script Date: 02/18/2016 13:06:01 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[v_TotalEntriesByMonth]'))
  DROP VIEW [dbo].[v_TotalEntriesByMonth]
GO

CREATE VIEW [dbo].[v_TotalEntriesByMonth]
AS
SELECT     TOP (100) PERCENT dbo.customer_visits.cut_gaming_day AS SQL_COLUMN_GAMING_DAY, COUNT(DISTINCT dbo.customer_visits.cut_visit_id) 
                      AS SQL_COLUMN_VISIT_NUMBER, '""' AS Col1, '""' AS Col2, 0 AS Col3, SUM(ISNULL(dbo.customer_entrances.cue_ticket_entry_price_real, 0)) 
                      AS SQL_COLUMN_THEORICAL_BY_DAY, SUM(ISNULL(dbo.customer_entrances.cue_ticket_entry_price_paid, 0)) AS SQL_COLUMN_REAL_BY_DAY, 
                      SUM(ISNULL(dbo.customer_entrances.cue_ticket_entry_price_paid, 0) - ISNULL(dbo.customer_entrances.cue_ticket_entry_price_real, 0)) 
                      AS SQL_COLUMN_DIFFERENCE_BY_DAY, COUNT(dbo.customer_entrances.cue_cashier_user_id) AS SQL_COLUMN_TOTAL_ENTRIES
FROM         dbo.customer_visits INNER JOIN
                      dbo.customer_entrances ON dbo.customer_visits.cut_visit_id = dbo.customer_entrances.cue_visit_id
GROUP BY dbo.customer_visits.cut_gaming_day
ORDER BY SQL_COLUMN_GAMING_DAY DESC

GO

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[36] 4[26] 2[17] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[35] 4[26] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "customer_visits"
            Begin Extent = 
               Top = 18
               Left = 343
               Bottom = 202
               Right = 527
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "customer_entrances"
            Begin Extent = 
               Top = 44
               Left = 709
               Bottom = 246
               Right = 979
            End
            DisplayFlags = 280
            TopColumn = 7
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 10
         Width = 284
         Width = 2280
         Width = 2385
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 2805
         Width = 1500
         Width = 1500
         Width = 2475
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 13860
         Alias = 2910
         Table = 1695
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'v_TotalEntriesByMonth'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'v_TotalEntriesByMonth'
GO


/******* TABLES  *******/


/******* INDEXES *******/

/******* RECORDS *******/

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CashDesk.Draw' AND GP_SUBJECT_KEY ='AccountingMode.RE.KindOfNameMovement')
	INSERT INTO general_params (gp_group_key, gp_subject_key, gp_key_value)
		VALUES ('CashDesk.Draw', 'AccountingMode.RE.KindOfNameMovement', 'Premio en especie (RE)');

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CashDesk.Draw' AND GP_SUBJECT_KEY ='AccountingMode.RE.PromoNameMovement')
	INSERT INTO general_params (gp_group_key, gp_subject_key, gp_key_value)
		VALUES ('CashDesk.Draw', 'AccountingMode.RE.PromoNameMovement', 'Promoci�n (RE)');

-- GENERAL PARAM
IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='GameGateway' AND GP_SUBJECT_KEY = 'AwardPrizes')
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('GameGateway', 'AwardPrizes', '1')
   
   
-- INDICES
IF  NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[gamegateway_bets]') AND name = N'IX_gb_transaction_type_gb_account_id')
CREATE NONCLUSTERED INDEX [IX_gb_transaction_type_gb_account_id]
ON [dbo].[gamegateway_bets] ([gb_transaction_type],[gb_account_id])
INCLUDE ([gb_total_prize])
GO

IF  NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[gamegateway_bets]') AND name = N'IX_gb_account_id_gb_transaction_type')
CREATE NONCLUSTERED INDEX [IX_gb_account_id_gb_transaction_type]
ON [dbo].[gamegateway_bets] ([gb_account_id],[gb_transaction_type])
INCLUDE ([gb_game_id],[gb_game_instance_id],[gb_transaction_id],[gb_partner_id],[gb_created],[gb_num_bets],[gb_total_bet],[gb_num_prizes],[gb_total_prize],[gb_jackpot_prize])
GO

IF  NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[gamegateway_bets]') AND name = N'IX_gb_transaction_id_gb_transaction_type')
CREATE NONCLUSTERED INDEX [IX_gb_transaction_id_gb_transaction_type]
ON [dbo].[gamegateway_bets] ([gb_transaction_id],[gb_transaction_type])
GO


-- GAMEGATEWAY_GAMES
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[gamegateway_games]') and name = 'gg_pending')
  ALTER TABLE dbo.gamegateway_games ADD gg_pending     [money] DEFAULT 0 NOT NULL,
                                        gg_num_pending [int]   DEFAULT 0 NOT NULL
GO


-- GAMEGATEWAY_GAME_INSTANCES
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[gamegateway_game_instances]') and name = 'ggi_pending')
  ALTER TABLE dbo.gamegateway_game_instances ADD ggi_pending     [money] DEFAULT 0 NOT NULL,
                                                 ggi_num_pending [int]   DEFAULT 0 NOT NULL
GO

DECLARE @bu_pm_name AS NVARCHAR(30)
SELECT @bu_pm_name = BU_NAME FROM buckets WHERE bu_bucket_id = 4

IF NOT EXISTS (SELECT * FROM promotions WHERE pm_type = 24) -- PROMOTION: AUTOMATIC NR CREDIT
BEGIN
  INSERT INTO [dbo].[promotions]  
             ([pm_name]
             ,[pm_enabled]
             ,[pm_type]
             ,[pm_date_start]
             ,[pm_date_finish]
             ,[pm_schedule_weekday]
             ,[pm_schedule1_time_from]
             ,[pm_schedule1_time_to]
             ,[pm_schedule2_enabled]
             ,[pm_schedule2_time_from]
             ,[pm_schedule2_time_to]
             ,[pm_gender_filter]
             ,[pm_birthday_filter]
             ,[pm_expiration_type]
             ,[pm_expiration_value]
             ,[pm_min_cash_in]
             ,[pm_min_cash_in_reward]
             ,[pm_cash_in]
             ,[pm_cash_in_reward]
             ,[pm_won_lock]
             ,[pm_num_tokens]
             ,[pm_token_name]
             ,[pm_token_reward]
             ,[pm_daily_limit]
             ,[pm_monthly_limit]
             ,[pm_level_filter]
             ,[pm_permission]
             ,[pm_freq_filter_last_days]
             ,[pm_freq_filter_min_days]
             ,[pm_freq_filter_min_cash_in]
             ,[pm_min_spent]
             ,[pm_min_spent_reward]
             ,[pm_spent]
             ,[pm_spent_reward]
             ,[pm_provider_list]
             ,[pm_offer_list]
             ,[pm_global_daily_limit]
             ,[pm_global_monthly_limit]
             ,[pm_small_resource_id]
             ,[pm_large_resource_id]
             ,[pm_min_played]
             ,[pm_min_played_reward]
             ,[pm_played]
             ,[pm_played_reward]
             ,[pm_play_restricted_to_provider_list]
             ,[pm_last_executed]
             ,[pm_next_execution]
             ,[pm_global_limit]
             ,[pm_credit_type]
             ,[pm_category_id]
             ,[pm_ticket_footer]
             ,[pm_visible_on_promobox]
             ,[pm_award_on_promobox]
             ,[pm_expiration_limit])
       VALUES
              (  @bu_pm_name --[pm_name]
             , 1
             , 24                         --[pm_type]
             , CAST('01-01-2010 00:00:00' as DATETIME)
             , CAST('01-01-2100 00:00:00' as DATETIME)
             , 127
             , 0
             , 0
             , 0
             , 0
             , 0
             , 0
             , 0
             , 1 
             , 999                        --[pm_expiration_value]
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , NULL
             , 0
             , ''
             , 0.00
             , NULL
             , NULL
             , 0             
             , 0
             , 0
             , 0
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , NULL
             , NULL
             , NULL
             , NULL
             , NULL
             , NULL
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , 0
             , NULL
             , NULL
             , NULL
             , 3
             , 0
             , ''
             , 0
             , 0
             , NULL)
END

GO

-- GENERAL PARAM
IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='GameGateway' AND GP_SUBJECT_KEY = 'AwardPrizes')
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('GameGateway', 'AwardPrizes', '1')
   
   
-- INDICES
IF  NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[gamegateway_bets]') AND name = N'IX_gb_transaction_type_gb_account_id')
CREATE NONCLUSTERED INDEX [IX_gb_transaction_type_gb_account_id]
ON [dbo].[gamegateway_bets] ([gb_transaction_type],[gb_account_id])
INCLUDE ([gb_total_prize])
GO

IF  NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[gamegateway_bets]') AND name = N'IX_gb_account_id_gb_transaction_type')
CREATE NONCLUSTERED INDEX [IX_gb_account_id_gb_transaction_type]
ON [dbo].[gamegateway_bets] ([gb_account_id],[gb_transaction_type])
INCLUDE ([gb_game_id],[gb_game_instance_id],[gb_transaction_id],[gb_partner_id],[gb_created],[gb_num_bets],[gb_total_bet],[gb_num_prizes],[gb_total_prize],[gb_jackpot_prize])
GO

IF  NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[gamegateway_bets]') AND name = N'IX_gb_transaction_id_gb_transaction_type')
CREATE NONCLUSTERED INDEX [IX_gb_transaction_id_gb_transaction_type]
ON [dbo].[gamegateway_bets] ([gb_transaction_id],[gb_transaction_type])
GO


-- GAMEGATEWAY_GAMES
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[gamegateway_games]') and name = 'gg_pending')
  ALTER TABLE dbo.gamegateway_games ADD gg_pending     [money] DEFAULT 0 NOT NULL,
                                        gg_num_pending [int]   DEFAULT 0 NOT NULL
GO


-- GAMEGATEWAY_GAME_INSTANCES
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[gamegateway_game_instances]') and name = 'ggi_pending')
  ALTER TABLE dbo.gamegateway_game_instances ADD ggi_pending     [money] DEFAULT 0 NOT NULL,
                                                 ggi_num_pending [int]   DEFAULT 0 NOT NULL
GO

/******* PROCEDURES *******/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetCashierTransactionsTaxes]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[GetCashierTransactionsTaxes]
GO

--EXEC	[dbo].[GetCashierTransactionsTaxes_Prueba3]
--		@pStartDatetime = N'2015-09-01 00:00:00.000',
--		@pEndDatetime = N'2016-01-01 00:00:00.000',
--		@pTerminalId = 13,
--		@pCmMovements = N'103, 304',
--		@pHpMovements = N'0,1,2,30, 1000, 1001, 11001, 11002, 1010',
--		@pPaymentUserId = 16,
--		@pAuthorizeUserId = null,  --18,

/*

----------------------------------------------------------------------------------------------------------------
BASIC REPORT QUERY FOR CASHIER TRANSACTIONS TAXES

Version           Date                    User                    Description
----------------------------------------------------------------------------------------------------------------
1.0.0             22-OCT-2015             CPC                     New procedure
1.0.1             24-DEC-2015             JML                     New version

Requirements:
   -- Functions:
         
Parameters:
   -- START DATE:            Start date for data collection. (If NULL then use first available date).
   -- END DATE:              End date for data collection. (If NULL then use current date).
   -- TERMINAL ID:           The terminal id for data collection (If NULL then all terminals)
   -- CASHIER MOVEMENTS:     Cashier movements in format '1,2,3,4'.  CM_TYPE IN (6, 14, 103, 124, 125, 126, 304, 30, 32) 
   --                         TAX_ON_PRIZE1 = 6,
   --                         TAX_ON_PRIZE2 = 14,
   --                                                                                      
   --                         TITO_TICKET_CASHIER_PAID_CASHABLE = 103,
   --                         TITO_TICKET_CASHIER_PAID_PROMO_REDEEMABLE = 124,
   --                         TITO_TICKET_CASHIER_EXPIRED_PAID_CASHABLE = 125,
   --                         TITO_TICKET_CASHIER_EXPIRED_PAID_PROMO_REDEEMABLE = 126,
   --                                                                                      
   --                         CHIPS_PURCHASE_TOTAL = 304,
   --
   --                         HANDPAY = 30,
   --                         MANUAL_HANDPAY = 32,
   --
   -- HANDPAYS MOVEMENTS:    Handpays movements in format '1,2,3,4'. 
   -- PAYMENT USER ID:       The payment user id for data collection (If NULL then all payment users)
   -- AUTHORIZE USER ID:     The authorize user id for data collection (If NULL then all authorize users)
   
*/  
CREATE PROCEDURE [dbo].[GetCashierTransactionsTaxes] 
(     @pStartDatetime DATETIME,
      @pEndDatetime DATETIME,
      @pTerminalId INT,
      @pCmMovements NVARCHAR(MAX),
      @pHpMovements NVARCHAR(MAX),
      @pPaymentUserId INT,
      @pAuthorizeUserId INT
)
AS 
BEGIN
  DECLARE @_PAID int
  DECLARE @_SQL nVarChar(max)
  DECLARE @SelectPartToExecute INT -- 1:HP,  2:CM,  3:Both
      
  SET @_PAID = 32768
  
  SET @SelectPartToExecute = 3 

  IF @pHpMovements IS NOT NULL AND @pCmMovements IS NULL
  BEGIN
     SET @SelectPartToExecute = 2
  END
  ELSE IF @pHpMovements IS NULL AND @pCmMovements IS NOT NULL
  BEGIN
     SET @SelectPartToExecute = 1
  END
     
  
  SET @_SQL = '
    SELECT   AO_OPERATION_ID
           , MAX(CM_DATE)                  AS CM_DATE
           , MAX(CM_TYPE)                  AS CM_TYPE
           , PAY.GU_USERNAME               AS PAYMENT_USER 
           , CM_USER_NAME                  AS AUTHORIZATION_USER
           , SUM(CASE CM_TYPE 
                 WHEN   6 THEN CM_ADD_AMOUNT
                 WHEN  14 THEN CM_ADD_AMOUNT
                 ELSE 0 END )             AS TOTAL_TAX
           , SUM(CASE CM_TYPE 
                 WHEN 103 THEN CM_SUB_AMOUNT
                 WHEN 124 THEN CM_SUB_AMOUNT
                 WHEN 125 THEN CM_SUB_AMOUNT
                 WHEN 126 THEN CM_SUB_AMOUNT
                 ELSE 0 END )             AS TITO_TICKET_PAID
           , SUM(CASE CM_TYPE 
                 WHEN 304 THEN CM_ADD_AMOUNT
                 ELSE 0 END )             AS CHIPS_PURCHASE_TOTAL
           , SUM(CASE CM_TYPE 
                 WHEN  30 THEN CM_SUB_AMOUNT
                 WHEN  32 THEN CM_SUB_AMOUNT
                 ELSE 0 END )             AS HANDPAY
           , CAI_NAME                      AS REASON
           , CS_CASHIER_ID
           , CM_USER_ID
           , AO_COMMENT
           , CM_CASHIER_NAME
      INTO   #TEMP_CASHIER_SESSIONS     
      FROM   CASHIER_MOVEMENTS  WITH(INDEX(IX_cm_date_type))
INNER JOIN   CASHIER_SESSIONS   ON CS_SESSION_ID   = CM_SESSION_ID 
INNER JOIN   ACCOUNT_OPERATIONS ON AO_OPERATION_ID = CM_OPERATION_ID
INNER JOIN   GUI_USERS AS PAY   ON PAY.GU_USER_ID  = CS_USER_ID
 LEFT JOIN   CATALOG_ITEMS      ON CAI_ID          = AO_REASON_ID
     WHERE   1 = 1 '
    + CASE WHEN @pStartDatetime IS NOT NULL THEN ' AND CM_DATE >= ''' + CONVERT(VARCHAR, @pStartDatetime, 21) + '''' ELSE '' END + CHAR(10) 
    + CASE WHEN @pEndDatetime IS NOT NULL THEN ' AND CM_DATE < ''' + CONVERT(VARCHAR, @pEndDatetime, 21) + '''' ELSE '' END + CHAR(10) 
+ '    AND   CM_TYPE IN (6, 14, 103, 124, 125, 126, 304, 30, 32) '
    + CASE WHEN @pTerminalId IS NOT NULL THEN ' AND CS_CASHIER_ID = ' + RTRIM(LTRIM(CONVERT(VARCHAR, @pTerminalId, 21))) + '' ELSE '' END + CHAR(10) 
    + CASE WHEN @pPaymentUserId IS NOT NULL THEN ' AND PAY.GU_USER_ID = ' + RTRIM(LTRIM(CONVERT(VARCHAR, @pPaymentUserId, 21))) + '' ELSE '' END + CHAR(10) 
    + CASE WHEN @pAuthorizeUserId IS NOT NULL THEN ' AND CM_USER_ID =' + RTRIM(LTRIM(CONVERT(VARCHAR, @pAuthorizeUserId, 21))) + '' ELSE '' END + CHAR(10) 
+'GROUP BY   AO_OPERATION_ID
           , PAY.GU_USERNAME
           , CM_USER_NAME
           , CAI_NAME
           , CS_CASHIER_ID
           , CM_USER_ID
           , AO_COMMENT
           , CM_CASHIER_NAME
' + CHAR(10) 

IF @SelectPartToExecute = 3 OR @SelectPartToExecute = 2
BEGIN
  SET @_SQL = @_SQL + '
    SELECT   AO_OPERATION_ID
           , HP_DATETIME               AS DATETIME
           , HP_TE_NAME                AS TERMINAL_NAME
           , HP_TYPE                   AS TRANSACTION_TYPE
           , PAYMENT_USER              AS PAYMENT_USER 
           , AUTHORIZATION_USER        AS AUTHORIZATION_USER
           , ISNULL(HP_AMOUNT,0) 
             - ISNULL(HP_TAX_AMOUNT,0) AS BEFORE_TAX_AMOUNT
           , HP_AMOUNT                 AS AFTER_TAX_AMOUNT
           , REASON                    AS REASON
           , AO_COMMENT                AS COMMENT      
           , 1                         AS ORIGEN
           , HP_TYPE                   AS TYPE1
      FROM   #TEMP_CASHIER_SESSIONS
INNER JOIN   ACCOUNT_MOVEMENTS WITH(INDEX( IX_am_operation_id)) ON AM_OPERATION_ID = AO_OPERATION_ID
INNER JOIN   HANDPAYS          WITH(INDEX( IX_HP_MOVEMENT_ID))       ON HP_MOVEMENT_ID  = AM_MOVEMENT_ID
     WHERE   1 = 1 '
    + CASE WHEN @pHpMovements IS NOT NULL THEN ' AND HP_TYPE IN (' + @pHpMovements + ') ' ELSE '' END + CHAR(10) 
--------      + CASE WHEN @pWithTaxes = 1 THEN ' AND HP_TAX_AMOUNT > 0' ELSE ' AND HP_TAX_AMOUNT = 0' END + CHAR(10) 
+ '   AND   HP_STATUS = ' + CAST(@_PAID AS NVARCHAR) + CHAR(10) 
END


IF @SelectPartToExecute = 3
BEGIN
  SET @_SQL = @_SQL + ' UNION ' + CHAR(10)
END

IF @SelectPartToExecute = 3 OR @SelectPartToExecute = 1
BEGIN
  SET @_SQL = @_SQL + '
    SELECT   AO_OPERATION_ID
           , CM_DATE              AS DATETIME
           , CM_CASHIER_NAME      AS TERMINAL_NAME
           , CM_TYPE              AS TRANSACTION_TYPE
           , PAYMENT_USER         AS PAYMENT_USER 
           , AUTHORIZATION_USER   AS AUTHORIZATION_USER
           , CHIPS_PURCHASE_TOTAL
           + TITO_TICKET_PAID
           + TOTAL_TAX            AS BEFORE_TAX_AMOUNT
           , CHIPS_PURCHASE_TOTAL
           + TITO_TICKET_PAID
           + TOTAL_TAX            AS AFTER_TAX_AMOUNT
           , REASON               AS REASON
           , AO_COMMENT           AS COMMENT      
           , 2                    AS ORIGEN
           , CM_TYPE              AS TYPE1
      FROM   #TEMP_CASHIER_SESSIONS
     WHERE   1 = 1 '
    + CASE WHEN @pCmMovements IS NOT NULL THEN ' AND CM_TYPE IN (' + ISNULL(@pCmMovements, '0') + ')  ' ELSE '' END + CHAR(10) 
----     + CASE WHEN @pWithTaxes = 1 THEN ' AND CM_ADD_AMOUNT > 0' ELSE ' AND CM_ADD_AMOUNT = 0' END + CHAR(10) 
END
 
SET @_SQL = @_SQL + ' ORDER   BY DATETIME DESC ' + CHAR(10) 
+ 'DROP TABLE #TEMP_CASHIER_SESSIONS '

--PRINT @_SQL     
EXEC sp_executesql @_SQL  
   
END  

GO
GRANT EXECUTE ON [dbo].[GetCashierTransactionsTaxes] TO [wggui] WITH GRANT OPTION 
GO