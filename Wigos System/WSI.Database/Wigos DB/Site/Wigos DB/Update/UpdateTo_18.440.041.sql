/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@DoubleCheck_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 439;
SET @DoubleCheck_ReleaseId= 471;

SET @New_ReleaseId = 440;

SET @New_ScriptName = N'UpdateTo_18.440.041.sql';
SET @New_Description = N'New release v03.007.0009'; 

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id BETWEEN @Exp_ReleaseId and @DoubleCheck_ReleaseId ))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id BETWEEN @Exp_ReleaseId AND @DoubleCheck_ReleaseId;
GO



/**** GENERAL PARAM *****/
IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.Voucher' AND GP_SUBJECT_KEY = 'VoucherOnPromotionAwarded.Title.RE')
	  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Cashier.Voucher', 'VoucherOnPromotionAwarded.Title.RE', '')
GO

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.Voucher' AND GP_SUBJECT_KEY = 'VoucherOnPromotionAwarded.Title.NR')
	  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Cashier.Voucher', 'VoucherOnPromotionAwarded.Title.NR', '')
GO

IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='PinPad' AND GP_SUBJECT_KEY = 'RequiredInternetConnection')
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('PinPad', 'RequiredInternetConnection', '1')
GO


IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'FBM' AND GP_SUBJECT_KEY = 'Enabled')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('FBM', 'Enabled', '0')
GO  

IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'FBM' AND GP_SUBJECT_KEY = 'Uri')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('FBM', 'Uri', 'http://25.71.170.138/WebService/FBMWService/FBMWebService.asmx?wsdl')
GO

IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'FBM' AND GP_SUBJECT_KEY = 'EstablishmentCode')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('FBM', 'EstablishmentCode', 'tws')
GO

IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'FBM' AND GP_SUBJECT_KEY = 'Provider')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('FBM', 'Provider', '')
GO

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Client.Registration' AND GP_SUBJECT_KEY = 'HideThresholdMovementsReport')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Client.Registration', 'HideThresholdMovementsReport', '1');
GO

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CountR' AND GP_SUBJECT_KEY = 'Protocols.Port')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('CountR', 'Protocols.Port', '2600');
GO

IF EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CountR' AND GP_SUBJECT_KEY = 'Server.Port')
  DELETE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CountR' AND GP_SUBJECT_KEY = 'Server.Port';
GO

IF NOT EXISTS (SELECT 1 FROM dbo.general_params
							WHERE gp_group_key = 'Account.Fields'
								AND	gp_subject_key = 'Delegation.Name')
BEGIN
	INSERT INTO dbo.general_params
	        ( gp_group_key ,
	          gp_subject_key ,
	          gp_key_value
	        )
	VALUES  ( N'Account.Fields' , -- gp_group_key - nvarchar(50)
	          N'Delegation.Name' , -- gp_subject_key - nvarchar(50)
	          N''  -- gp_key_value - nvarchar(1024)
	        )
END				
GO

IF NOT EXISTS (SELECT 1 FROM dbo.general_params
							WHERE gp_group_key = 'Account.Fields'
								AND	gp_subject_key = 'Township.Name')
BEGIN
	INSERT INTO dbo.general_params
	        ( gp_group_key ,
	          gp_subject_key ,
	          gp_key_value
	        )
	VALUES  ( N'Account.Fields' , -- gp_group_key - nvarchar(50)
	          N'Township.Name' , -- gp_subject_key - nvarchar(50)
	          N''  -- gp_key_value - nvarchar(1024)
	        )
END		
GO

IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Account.Fields' AND GP_SUBJECT_KEY = 'Name')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Account.Fields', 'Name', '')
GO  

IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Account.Fields' AND GP_SUBJECT_KEY = 'MiddleName')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Account.Fields', 'MiddleName', '')
GO  

IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Account.Fields' AND GP_SUBJECT_KEY = 'Surname1')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Account.Fields', 'Surname1', '')
GO  

IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Account.Fields' AND GP_SUBJECT_KEY = 'Surname2')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Account.Fields', 'Surname2', '')
GO  


IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='GamingTables' AND GP_SUBJECT_KEY='DealerCopyTickets.ExpirationDays')                     
                  INSERT INTO GENERAL_PARAMS  (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES      ('GamingTables','DealerCopyTickets.ExpirationDays','0')
GO

IF EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'AGG' AND GP_SUBJECT_KEY = 'MaxValuesOfBigIncrement' AND GP_KEY_VALUE = '0.01:500000;0.1:50000;1:5000')
  UPDATE GENERAL_PARAMS SET GP_KEY_VALUE = '0.01:500;0.02:1000;0.05:2500;0.1:5000;0.2:10000;0.25:12500;0.5:25000;1:50000;2:100000;5:250000;10:500000' WHERE GP_GROUP_KEY = 'AGG' AND GP_SUBJECT_KEY = 'MaxValuesOfBigIncrement'
GO

IF EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'User' AND GP_SUBJECT_KEY = 'Password.OldRules')
  DELETE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'User' AND GP_SUBJECT_KEY = 'Password.OldRules'
GO

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PSAClient' AND GP_SUBJECT_KEY ='Salidas.ISR3')
  INSERT INTO [dbo].[GENERAL_PARAMS]
             ([GP_group_key] ,[GP_subject_key] ,[GP_key_value])
       VALUES
             ('PSAClient', 'Salidas.ISR3', '0');
GO


IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'WCP' AND GP_SUBJECT_KEY = 'SasMetersInterval')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('WCP', 'SasMetersInterval', '30');  
GO

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'WCP' AND GP_SUBJECT_KEY = 'IntervalBilling')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('WCP', 'IntervalBilling', '30');
GO

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'WCP' AND GP_SUBJECT_KEY = 'IntervalBilling.CorretionTime')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('WCP', 'IntervalBilling.CorretionTime', '1');
GO

/**** TABLES *****/

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[terminal_draws_recharges]') and name = 'tdr_unique_id')
  ALTER TABLE [dbo].[terminal_draws_recharges] ADD [tdr_unique_id] BigInt NOT NULL DEFAULT '0'
GO

IF NOT EXISTS( SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'gifts' AND COLUMN_NAME = 'gi_pyramidal_dist')
BEGIN
  ALTER TABLE dbo.gifts ADD gi_pyramidal_dist XML NULL CONSTRAINT DF_gifts_gi_pyramidal_dist DEFAULT NULL
END
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[account_promotions]') and name = 'acp_pyramid_prize')
  ALTER TABLE [dbo].[account_promotions] ADD [acp_pyramid_prize] Money NULL DEFAULT '0'
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[account_promotions]') and name = 'acp_draw_price')
  ALTER TABLE [dbo].[account_promotions] ADD [acp_draw_price] Money NULL DEFAULT '0'
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[account_promotions]') and name = 'acp_total_prize')
  ALTER TABLE [dbo].[account_promotions] ADD [acp_total_prize] Money NULL DEFAULT '0'
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.service_internet_connection') AND type in (N'U'))
BEGIN

CREATE TABLE [dbo].[service_internet_connection](
	[sic_protocol] [nchar](50) NOT NULL,
	[sic_has_internet_connection] [bit] NOT NULL,
	[sic_last_update] [datetime] NOT NULL,
 CONSTRAINT [PK_service_internet_connection] PRIMARY KEY CLUSTERED 
(
	[sic_protocol] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

END
GO

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'GamingTables.ServiceCharge' AND GP_SUBJECT_KEY = 'AttributedToCompany')
BEGIN
  DECLARE @_attributed as NVARCHAR(50)
  SELECT @_attributed = GP_KEY_VALUE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.ServiceCharge' AND GP_SUBJECT_KEY = 'AttributedToCompany'
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES('GamingTables.ServiceCharge', 'AttributedToCompany', @_attributed)
END
GO

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'GamingTables.ServiceCharge' AND GP_SUBJECT_KEY = 'Enabled')
BEGIN
  DECLARE @_enabled as NVARCHAR(50)
  SELECT @_enabled = GP_KEY_VALUE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.ServiceCharge' AND GP_SUBJECT_KEY = 'Enabled'  
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES('GamingTables.ServiceCharge', 'Enabled', @_enabled)
END
GO

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'GamingTables.ServiceCharge' AND GP_SUBJECT_KEY = 'Mode')
BEGIN
  DECLARE @_mode as NVARCHAR(50)
  SELECT @_mode = GP_KEY_VALUE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.ServiceCharge' AND GP_SUBJECT_KEY = 'Mode'
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES('GamingTables.ServiceCharge', 'Mode', @_mode)
END
GO

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'GamingTables.ServiceCharge' AND GP_SUBJECT_KEY = 'OnLoser.RefundPct')
BEGIN
  DECLARE @_onloser_refound as NVARCHAR(50)
  SELECT @_onloser_refound = GP_KEY_VALUE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.ServiceCharge' AND GP_SUBJECT_KEY = 'OnLoser.RefundPct'
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES('GamingTables.ServiceCharge', 'OnLoser.RefundPct', @_onloser_refound)
END
GO

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'GamingTables.ServiceCharge' AND GP_SUBJECT_KEY = 'OnRecharge.BasePct')
BEGIN
  DECLARE @_onrecharge_pct as NVARCHAR(50)
  SELECT @_onrecharge_pct = GP_KEY_VALUE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.ServiceCharge' AND GP_SUBJECT_KEY = 'OnRecharge.BasePct'
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES('GamingTables.ServiceCharge', 'OnRecharge.BasePct', @_onrecharge_pct)
END
GO

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'GamingTables.ServiceCharge' AND GP_SUBJECT_KEY = 'OnWinner.PrizePct')
BEGIN
  DECLARE @_onwinner_prize_pct as NVARCHAR(50)
  SELECT @_onwinner_prize_pct = GP_KEY_VALUE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.ServiceCharge' AND GP_SUBJECT_KEY = 'OnWinner.PrizePct'
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES('GamingTables.ServiceCharge', 'OnWinner.PrizePct', @_onwinner_prize_pct)
END
GO

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'GamingTables.ServiceCharge' AND GP_SUBJECT_KEY = 'OnWinner.RefundPct')
BEGIN
  DECLARE @_onwinner_refound_pct as NVARCHAR(50)
  SELECT @_onwinner_refound_pct = GP_KEY_VALUE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.ServiceCharge' AND GP_SUBJECT_KEY = 'OnWinner.RefundPct'
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES('GamingTables.ServiceCharge', 'OnWinner.RefundPct', @_onwinner_refound_pct)
END
GO

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'GamingTables.ServiceCharge' AND GP_SUBJECT_KEY = 'Report.ShowPrize')
BEGIN
  DECLARE @_report_show_prize as NVARCHAR(50)
  SELECT @_report_show_prize = GP_KEY_VALUE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.ServiceCharge' AND GP_SUBJECT_KEY = 'Report.ShowPrize'
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES('GamingTables.ServiceCharge', 'Report.ShowPrize', @_report_show_prize)
END
GO

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'GamingTables.ServiceCharge' AND GP_SUBJECT_KEY = 'Report.ShowRefund')
BEGIN
  DECLARE @_report_show_refund as NVARCHAR(50)
  SELECT @_report_show_refund = GP_KEY_VALUE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.ServiceCharge' AND GP_SUBJECT_KEY = 'Report.ShowRefund'
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES('GamingTables.ServiceCharge', 'Report.ShowRefund', @_report_show_refund)
END
GO

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'GamingTables.ServiceCharge' AND GP_SUBJECT_KEY = 'Text')
BEGIN
  DECLARE @_text as NVARCHAR(50)
  SELECT @_text = GP_KEY_VALUE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.ServiceCharge' AND GP_SUBJECT_KEY = 'Text'
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES('GamingTables.ServiceCharge', 'Text', @_text)
END
GO


IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[customer_bucket_by_gaming_day]') AND name = N'IX_cbud_customer_id_cbud_bucket_id_cbud_gaming_day')
  DROP INDEX [IX_cbud_customer_id_cbud_bucket_id_cbud_gaming_day] ON [dbo].[customer_bucket_by_gaming_day] WITH ( ONLINE = OFF )
GO

CREATE NONCLUSTERED INDEX [IX_cbud_customer_id_cbud_bucket_id_cbud_gaming_day] ON [dbo].[customer_bucket_by_gaming_day] 
(
	[cbud_customer_id] ASC,
	[cbud_bucket_id] ASC,
	[cbud_gaming_day] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO


IF NOT EXISTS (SELECT * FROM sys.columns  WHERE object_id = object_id(N'[dbo].[terminals]') AND name = 'te_manufacture_month') 
  ALTER TABLE dbo.terminals ADD te_manufacture_month INT NULL
GO

IF NOT EXISTS (SELECT * FROM sys.columns  WHERE object_id = object_id(N'[dbo].[terminals]') AND name = 'te_manufacture_day') 
  ALTER TABLE dbo.terminals ADD te_manufacture_day INT NULL
GO

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'WCP' AND GP_SUBJECT_KEY = 'SasMetersCleanDenomEmpty')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('WCP', 'SasMetersCleanDenomEmpty', '0');
GO

IF NOT EXISTS(SELECT   1 
                FROM   GENERAL_PARAMS 
               WHERE   GP_GROUP_KEY = 'SasHost'
                 AND   GP_SUBJECT_KEY = 'SasMeterInterval'
             )
BEGIN
    INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
    VALUES ('SasHost', 'SasMeterInterval', '60')
END
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[customer_entrances]') and name = 'cue_exit_datetime')
  ALTER TABLE [dbo].[customer_entrances] ADD [cue_exit_datetime] datetime NULL,
                                             [cue_exit_reason] int NULL
GO


IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[egm_control_mark]') AND TYPE IN (N'U'))
    DROP TABLE [dbo].[egm_control_mark]
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[egm_control_mark]') AND TYPE IN (N'U'))
              CREATE TABLE [dbo].[egm_control_mark](
                              [ecm_site_id] INT NOT NULL, 
                              [ecm_control_mark] [DATETIME] NOT NULL
              CONSTRAINT [PK_egm_control_mark] PRIMARY KEY CLUSTERED 
                (
                            [ecm_site_id]    ASC
                ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
              ) ON [PRIMARY]
GO

IF NOT EXISTS( SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'machine_meters' AND COLUMN_NAME = 'mm_sas_accounting_denom') 
BEGIN
	ALTER TABLE [dbo].[machine_meters] ADD mm_sas_accounting_denom MONEY NULL;
END
GO




/**** RECORDS *****/

IF (NOT EXISTS (SELECT * FROM occupations WHERE oc_country_iso_code2 = 'CL' AND oc_description = 'Abogado/a'))
BEGIN
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Abogado/a', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Actor', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Actriz', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Administrador/ra de Empresas', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Administrador/ra en Gastron�mica Internacional', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Administrador/ra en Hoteler�a y Servicios', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Administrador/ra en Turismo', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Administrativo/a', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Agricultor/a', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Agronomo/a', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Alba�il', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Analista De Sistema', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Analista Programador', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Animador/a', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Arquitecto/a', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Artesano/a', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Asesora de Hogar', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Asistente De Educaci�n', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Asistente Social', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Bailarin/a', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Biologo/a', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Biologo/a Marino', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Botones', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Cajero/a', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Camarografo', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Carabinero', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Carpintero', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Cesante', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Chef', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Chofer / Conductor', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Climatizaci�n', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Cocinero/a', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Colectivero', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Comerciante', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Conserje ', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Constructor', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Constructor/ra Civil', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Contador/a', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Contador/a Auditor', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Contador/a General', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Contador/a P�blico', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Contratista', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Contructor Civil', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Corredor de propiedades', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Corredor de seguro', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Cosmetologo/a', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Costurero/a', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Dibujante', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Director/a Colegio', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Dise�ador/a de Moda', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Dise�ador/a Gr�fico', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Dise�ador/a Web', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Dise�adora de vestuario', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Docente Universitario', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Due�o/a de Casa', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Economista', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Educadora Diferencial', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Ejecutivo/a de Ventas', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Electrico', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Electrico/a Industrial', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Electromec�nico/a', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Electr�nico/a Industrial', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Empleado Judicial', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Empresario/a', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Enfermero/a', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Estelista', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Estudiante', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Farmaceutico', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('FFAA', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Florista', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Fonoaudiologo/a', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Fotografo', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Funcionario/a Publica', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Futbolista', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Garzon/a', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Gasfiter', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Geologo/a', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Gerente de Asuntos Corporativos', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Gerente de Asuntos Publicos', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Gerente de Finanzas', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Gerente de Informatica', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Gerente de Marketing', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Gerente de Operaciones', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Gerente de Personas', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Gerente de RRHH', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Gerente General', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Guardia', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Independiente', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Productor Grafico', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Productor de Eventos', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Ingeniero/a', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Ingeniero/a  Agr�cola', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Ingeniero/a  Comercial', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Ingeniero/a  Electr�nica', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Ingeniero/a  en Administraci�n de Empresas ', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Ingeniero/a  en Automatizaci�n y Control Industrial', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Ingeniero/a  en Ciberseguridad', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Ingeniero/a  en Climatizaci�n', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Ingeniero/a  en Comercio Exterior', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Ingeniero/a  en Construcci�n', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Ingeniero/a  en Electricidad', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Ingeniero/a  en Geomensura', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Ingeniero/a  en Inform�tica', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Ingeniero/a  en Maquinaria', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Ingeniero/a  en Metalurgia', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Ingeniero/a  en Minas', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Ingeniero/a  en Prevenci�n de Riesgos', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Ingeniero/a  en Producci�n Ganadera', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Ingeniero/a  en Qu�mica Industrial', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Ingeniero/a  en Refrigeraci�n', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Ingeniero/a  en Sonido', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Ingeniero/a  en Telecomunicaciones, Conectividad y Redes', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Ingeniero/a  Industrial', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Ingeniero/a  Mec�nica en Mantenimiento Industrial', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Ingeniero/a  Mec�nica en Producci�n Industrial', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Ingeniero/a Agricola', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Ingeniero/a Civil', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Ingeniero/a Civil Electrico', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Ingeniero/a Comercial', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Ingeniero/a Electrico', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Ingeniero/a en Ejecuci�n ', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Ingeniero/a en Minas', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Ingeniero/a en Prevenci�n de Riesgos', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Ingeniero/a Industrial', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Ingeniero/a Mecanico', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Ingeniero/a Metalurgia', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Inspector/a', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Instructor Deportivo', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Instructor Zumba', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Instrumentista', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Intructor Maquinas', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Jardinero/a', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Jefe administrativo', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Jubilado/a', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Kinesiologo/a', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Laboratorista Cl�nico, Banco de Sangre e Imagenolog�a', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Maestro Minero', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Manicurista', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Mantenedor Equipos Industriales', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Mantenedor/a Industrial', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Mantenedor/a Mec�nico de Plantas Mineras', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Marino', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Marketing Publicidad', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Masajista', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Matron/a', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Mecanico', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Mec�nico Automotriz en Maquinaria Pesada', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Mec�nico Automotriz en Sistemas Electr�nicos', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Mec�nico en Producci�n Industrial', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Medico/a', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Medico/a Veterinario', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Militar', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Minero', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Mueblista', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Nutricionista', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Odontologo/a', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Operador Maquinaria', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Operador Planta', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Paramedico', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Particular', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Parvulario/a', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Pastelero/a', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Pedicurista', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Peluquera', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Periodista', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Piloto', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Pintor', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Profesor/ra B�sica o Media', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Profesor/ra Universitario', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Psicologo/a', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Psicopedagogo/a', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Quimico farmac�utico', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Quiropractivo', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Recepcionista', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Relacionador/a P�blico', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Secretario/a', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Servicio Publico', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Sicologo/a', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Soldador', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Sonidista', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Steward', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Supervisora', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Taxista', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Tecnico Agricola', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('T�cnico en Enfermer�a', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('T�cnico en Farmacia', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('T�cnico en Metalurgia Extractiva', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Tecnico en Minas', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('T�cnico en Miner�a', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('T�cnico en Odontolog�a', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Tecnico en plantas', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Tecnico en Telecomunicaciones', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Tecnico Enfermeria', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Tecnico Financiero', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Tecnico Mecanico', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Tecnico Universitario', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Tecnolog�a Agr�cola', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Tecnolog�a en An�lisis Qu�mico', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Tecnolog�a en Producci�n Ganadera', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Tecnolog�a en Sonido', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Tecnologo Medico', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Tecnologo/a Medico Imagenologia', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Tecnologo/a Medico Laboratorio', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Tecnologo/a Medico Oftalmologia', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Telefonista', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Terapeuta', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Terapeuta Ocupacional', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Topografo/a', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Trabajador Sector Privado', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Trabajador Sector Publico', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Trabajador/a Social', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Transportista', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Turismo', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Vendedor/a', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Vigilante', '0000', 1, 1000, 'CL')

END
GO

--English
IF NOT EXISTS (SELECT 1 
							FROM ALARM_CATALOG
							WHERE ALCG_ALARM_CODE = 393286
								AND ALCG_LANGUAGE_ID = 9)
	BEGIN
		INSERT INTO ALARM_CATALOG (ALCG_ALARM_CODE, ALCG_LANGUAGE_ID, ALCG_TYPE, ALCG_NAME, ALCG_DESCRIPTION, ALCG_VISIBLE)
		VALUES  ( 393286 , 9, 0, 'Bill-in exceeded limit', 'Bill-in exceeded limit', 1)
	END
	GO
	
--Spanish
IF NOT EXISTS (SELECT 1 
							FROM ALARM_CATALOG
							WHERE ALCG_ALARM_CODE = 393286
								AND ALCG_LANGUAGE_ID = 10)
	BEGIN
		INSERT INTO ALARM_CATALOG (ALCG_ALARM_CODE, ALCG_LANGUAGE_ID, ALCG_TYPE, ALCG_NAME, ALCG_DESCRIPTION, ALCG_VISIBLE)
		VALUES  ( 393286 , 10, 0, 'Bill-in l�mite superado', 'Bill-in l�mite superado', 1)
	END	
	GO
	
--French
IF NOT EXISTS (SELECT 1 
							FROM ALARM_CATALOG
							WHERE ALCG_ALARM_CODE = 393286
								AND ALCG_LANGUAGE_ID = 12)
	BEGIN
		INSERT INTO ALARM_CATALOG (ALCG_ALARM_CODE, ALCG_LANGUAGE_ID, ALCG_TYPE, ALCG_NAME, ALCG_DESCRIPTION, ALCG_VISIBLE)
		VALUES  ( 393286 , 12, 0, 'Bill-in limite d�pass�e', 'Bill-in limite d�pass�e', 1)
	END	
GO

--CATEGORY	
IF NOT EXISTS(SELECT 1 
							FROM ALARM_CATALOG_PER_CATEGORY
							WHERE ALCC_ALARM_CODE = 393286)
	BEGIN
		INSERT INTO	ALARM_CATALOG_PER_CATEGORY (ALCC_ALARM_CODE, ALCC_CATEGORY, ALCC_TYPE, ALCC_DATETIME)
		VALUES  ( 393286, 9, 0, GETDATE())
	END			
GO

IF NOT EXISTS(SELECT 1 FROM SAS_METERS_GROUPS WHERE smg_group_id = 10006)
BEGIN
      INSERT INTO SAS_METERS_GROUPS (smg_group_id,smg_name,smg_description,smg_required) VALUES (10006,'IPLYC','IPLYC',0)
      INSERT INTO SAS_METERS_CATALOG_PER_GROUP (smcg_group_id, smcg_meter_code)          VALUES (10006,0)
      INSERT INTO SAS_METERS_CATALOG_PER_GROUP (smcg_group_id, smcg_meter_code)          VALUES (10006,1)
      INSERT INTO SAS_METERS_CATALOG_PER_GROUP (smcg_group_id, smcg_meter_code)          VALUES (10006,2)
      INSERT INTO SAS_METERS_CATALOG_PER_GROUP (smcg_group_id, smcg_meter_code)          VALUES (10006,5)
      INSERT INTO SAS_METERS_CATALOG_PER_GROUP (smcg_group_id, smcg_meter_code)          VALUES (10006,6)
      INSERT INTO SAS_METERS_CATALOG_PER_GROUP (smcg_group_id, smcg_meter_code)          VALUES (10006,36)
END 
GO

/**** STORED PROCEDURES *****/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetSpentGroupByTerminal]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[GetSpentGroupByTerminal]
GO

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[GetSpentGroupByTerminal]  
       @pAccountId as BigInt
     , @pLowerLimitDatetime as DateTime
     , @pPlaySessionOpened as Int = 0
AS
BEGIN

  DECLARE @MaxUnbalanceInCents as decimal(19,3)
  DECLARE @MaxBetAmountAverageInCents as decimal(19,3)
  SET NOCOUNT ON;

  SELECT @MaxUnbalanceInCents = CAST(GP_KEY_VALUE AS decimal)/100
    FROM GENERAL_PARAMS 
   WHERE GP_GROUP_KEY = 'Cashier.DrawTicket' 
     AND GP_SUBJECT_KEY = 'ToGiveDrawNumbers.MaxUnbalanceInCents'

  SELECT @MaxBetAmountAverageInCents = CAST(GP_KEY_VALUE AS decimal)/100
    FROM GENERAL_PARAMS 
   WHERE GP_GROUP_KEY = 'Cashier.DrawTicket' 
     AND GP_SUBJECT_KEY = 'ToGiveDrawNumbers.MaxBetAmountAverageInCents'
                                                         
      SELECT   TE_PROVIDER_ID  
             , TE_TERMINAL_ID
             , DATEADD (HOUR, DATEDIFF (HOUR, @pLowerLimitDatetime, PS_STARTED),  @pLowerLimitDatetime) AS STARTED 
             , DATEADD (HOUR, DATEDIFF (HOUR, @pLowerLimitDatetime, PS_FINISHED), @pLowerLimitDatetime) AS FINISHED 
             , SUM (PS_PLAYED_AMOUNT * CASE ISNULL(ce_change, 0) WHEN 0 THEN 1 ELSE ce_change END ) AS TOTAL_PLAYED                                      
             , SUM (PS_REDEEMABLE_PLAYED * CASE ISNULL(ce_change, 0) WHEN 0 THEN 1 ELSE ce_change END ) AS REDEEMABLE_PLAYED                                 
             , SUM (CASE WHEN PS_PLAYED_AMOUNT     > PS_WON_AMOUNT     THEN PS_PLAYED_AMOUNT     - PS_WON_AMOUNT     ELSE 0 END * CASE ISNULL(CE_CHANGE, 0) WHEN 0 THEN 1 ELSE CE_CHANGE END ) AS TOTAL_SPENT                                       
             , SUM (CASE WHEN PS_REDEEMABLE_PLAYED > PS_REDEEMABLE_WON THEN PS_REDEEMABLE_PLAYED - PS_REDEEMABLE_WON ELSE 0 END * CASE ISNULL(CE_CHANGE, 0) WHEN 0 THEN 1 ELSE CE_CHANGE END ) AS REDEEMABLE_SPENT                                  
             , SUM (PS_INITIAL_BALANCE) AS BAL_INI
             , SUM (PS_FINAL_BALANCE) AS BAL_FIN
             , SUM (PS_WON_AMOUNT) AS WON_AMOUNT
             , SUM (PS_PLAYED_COUNT) AS NUM_PLAYS
             , ABS(PS_TOTAL_CASH_OUT - (PS_TOTAL_CASH_IN - PS_TOTAL_PLAYED + PS_TOTAL_WON))          AS UNBALANCE
             , CASE WHEN PS_PLAYED_COUNT = 0 THEN 0 ELSE ABS(PS_PLAYED_AMOUNT / PS_PLAYED_COUNT) END AS AVG_BET
             , @MaxUnbalanceInCents as a
        FROM   PLAY_SESSIONS                                         
  INNER JOIN   TERMINALS ON PS_TERMINAL_ID = TE_TERMINAL_ID 
   LEFT JOIN   CURRENCY_EXCHANGE ON TE_ISO_CODE = CE_CURRENCY_ISO_CODE AND CE_TYPE = 0 
       WHERE   PS_FINISHED   >= @pLowerLimitDatetime                 
         AND   PS_STATUS     <> @pPlaySessionOpened                  
         AND   PS_ACCOUNT_ID  = @pAccountId    
         AND   (ABS(PS_TOTAL_CASH_OUT - (PS_TOTAL_CASH_IN - PS_TOTAL_PLAYED + PS_TOTAL_WON))       < @MaxUnbalanceInCents OR @MaxUnbalanceInCents = 0)
         AND   (CASE WHEN PS_PLAYED_COUNT = 0 THEN 0 ELSE ABS(PS_PLAYED_AMOUNT / PS_PLAYED_COUNT) END < @MaxBetAmountAverageInCents OR @MaxBetAmountAverageInCents = 0)
    GROUP BY   TE_PROVIDER_ID
             , TE_TERMINAL_ID                           
             , DATEADD (HOUR, DATEDIFF (HOUR, @pLowerLimitDatetime, PS_STARTED),  @pLowerLimitDatetime)   
             , DATEADD (HOUR, DATEDIFF (HOUR, @pLowerLimitDatetime, PS_FINISHED), @pLowerLimitDatetime)   
             , ABS(PS_TOTAL_CASH_OUT - (PS_TOTAL_CASH_IN - PS_TOTAL_PLAYED + PS_TOTAL_WON))      
             , CASE WHEN PS_PLAYED_COUNT = 0 THEN 0 ELSE ABS(PS_PLAYED_AMOUNT / PS_PLAYED_COUNT) END


      SELECT   PS_PLAY_SESSION_ID
             , PS_ACCOUNT_ID
             , TE_PROVIDER_ID  
             , TE_TERMINAL_ID
             , PS_STARTED
             , PS_FINISHED
             , PS_PLAYED_AMOUNT * CASE ISNULL(ce_change, 0) WHEN 0 THEN 1 ELSE ce_change END      AS TOTAL_PLAYED                                      
             , PS_REDEEMABLE_PLAYED * CASE ISNULL(ce_change, 0) WHEN 0 THEN 1 ELSE ce_change END  AS REDEEMABLE_PLAYED                                 
             , CASE WHEN PS_PLAYED_AMOUNT     > PS_WON_AMOUNT     THEN PS_PLAYED_AMOUNT     - PS_WON_AMOUNT     ELSE 0 END * CASE ISNULL(CE_CHANGE, 0) WHEN 0 THEN 1 ELSE CE_CHANGE END  AS TOTAL_SPENT                                       
             , CASE WHEN PS_REDEEMABLE_PLAYED > PS_REDEEMABLE_WON THEN PS_REDEEMABLE_PLAYED - PS_REDEEMABLE_WON ELSE 0 END * CASE ISNULL(CE_CHANGE, 0) WHEN 0 THEN 1 ELSE CE_CHANGE END  AS REDEEMABLE_SPENT                                  
             , PS_INITIAL_BALANCE
             , PS_FINAL_BALANCE
             , PS_WON_AMOUNT
             , PS_PLAYED_COUNT
             , ABS(PS_TOTAL_CASH_OUT - (PS_TOTAL_CASH_IN - PS_TOTAL_PLAYED + PS_TOTAL_WON))          AS UNBALANCE
             , CASE WHEN PS_PLAYED_COUNT = 0 THEN 0 ELSE ABS(PS_PLAYED_AMOUNT / PS_PLAYED_COUNT) END AS AVG_BET
        FROM   PLAY_SESSIONS                                         
  INNER JOIN   TERMINALS ON PS_TERMINAL_ID = TE_TERMINAL_ID 
   LEFT JOIN   CURRENCY_EXCHANGE ON TE_ISO_CODE = CE_CURRENCY_ISO_CODE AND CE_TYPE = 0 
       WHERE   PS_FINISHED   >= @pLowerLimitDatetime                 
         AND   PS_STATUS     <> @pPlaySessionOpened                  
         AND   PS_ACCOUNT_ID  = @pAccountId    
         AND   (  (ABS(PS_TOTAL_CASH_OUT - (PS_TOTAL_CASH_IN - PS_TOTAL_PLAYED + PS_TOTAL_WON))       >= @MaxUnbalanceInCents AND @MaxUnbalanceInCents > 0)
               OR (CASE WHEN PS_PLAYED_COUNT = 0 THEN 0 ELSE ABS(PS_PLAYED_AMOUNT / PS_PLAYED_COUNT) END >= @MaxBetAmountAverageInCents AND @MaxBetAmountAverageInCents > 0))
    ORDER BY   TE_PROVIDER_ID
             , TE_TERMINAL_ID                           
             , DATEADD (HOUR, DATEDIFF (HOUR, @pLowerLimitDatetime, PS_STARTED),  @pLowerLimitDatetime)   
             , DATEADD (HOUR, DATEDIFF (HOUR, @pLowerLimitDatetime, PS_FINISHED), @pLowerLimitDatetime)   

END
GO

GRANT EXECUTE ON [dbo].[GetSpentGroupByTerminal] TO [wggui] WITH GRANT OPTION
GO

IF EXISTS (SELECT * FROM 	sys.objects WHERE 	OBJECT_ID = OBJECT_ID(N'[dbo].[GT_Calculate_DROP_CASHIER]'))
BEGIN
 GRANT EXECUTE ON GT_Calculate_DROP_CASHIER TO [wggui] WITH GRANT OPTION
END
GO

------------------------------------------------------------------------------------------------------------------------------
-- Copyright � 2017 Win Systems International
------------------------------------------------------------------------------------------------------------------------------
--
--   MODULE NAME: GetTicketAmounts
--
--   DESCRIPTION: Calculate ticket Amount from when ticket change from PENDING_PRINT to VALID
--        ALARMS: 0 - No alarm
--                1 - New amount is null or zero
--                2 - New amount greater of old amount
--                3 - New amount smaller of old amount
--                4 - New currency is diferent of older
--                5 - New amount greater of old amount with diferent currency
--                6 - New amount smaller of old amount with diferent currency
--
--        AUTHOR: Jos� Mart�nez L�pez
--
-- CREATION DATE: 13-NOV-2017
--
-- REVISION HISTORY:
--
--
-- Date        Author Description
-- ----------- ------ --------------------------------------------------------------------------------------------------------
-- 13-NOV-2017 JML    First release.
------------------------------------------------------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetTicketAmounts]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[GetTicketAmounts]
GO


CREATE PROCEDURE [dbo].[GetTicketAmounts]
                  @TerminalId           AS BigInt
                  
                , @OriginalTicketAmount AS Money
                , @OriginalTicketAmt0   AS Money
                , @OriginalTicketCur0   AS NVARCHAR(3)
                
                , @NewTicketAmount      AS Money
                , @NewTicketAmt0        AS Money
                , @NewTicketCur0        AS NVARCHAR(3)
                
                , @UpdateTicketAmt0     AS Money       OUTPUT
                , @UpdateTicketCur0     AS NVARCHAR(3) OUTPUT
                , @UpdateTicketAmount   AS Money       OUTPUT
                
                , @AlarmId              AS Int         OUTPUT

AS
BEGIN
  
  -- Set default values
  SET @UpdateTicketAmount = @NewTicketAmount
  SET @UpdateTicketAmt0   = @NewTicketAmt0
  SET @UpdateTicketCur0   = @NewTicketCur0

  -- ALARMS --
  
  SET @AlarmId = 0

  -- Set original amout if new amount is null or zero an set alarm
  IF @NewTicketAmt0 = 0
  BEGIN
    SET @AlarmId = 1 
    SET @UpdateTicketAmt0 = @OriginalTicketAmt0
  END

  -- Compare amounts and set alarm if it is necesary
  IF @OriginalTicketCur0 = @NewTicketCur0
  BEGIN
    IF @OriginalTicketAmt0 < @NewTicketAmt0
    BEGIN
      SET @AlarmId = 2
    END
    IF @OriginalTicketAmt0 > @NewTicketAmt0
    BEGIN
      SET @AlarmId = 3
    END
  END

  IF @OriginalTicketCur0 <> @NewTicketCur0
  BEGIN
    SET @AlarmId = 4
    IF @OriginalTicketAmt0 < @NewTicketAmt0
    BEGIN
      SET @AlarmId = 5
    END
    IF @OriginalTicketAmt0 > @NewTicketAmt0
    BEGIN
      SET @AlarmId = 6
    END
  END
  
END
GO

GRANT EXECUTE ON [GetTicketAmounts] TO wggui WITH GRANT OPTION
GO

--****************************************************************************************************************************
--****************************************************************************************************************************
--****************************************************************************************************************************

------------------------------------------------------------------------------------------------------------------------------
-- Copyright � 2017 Win Systems International
------------------------------------------------------------------------------------------------------------------------------
--
--   MODULE NAME: TITO_UpdateTicketStatusToValid
--
--   DESCRIPTION: Change tickets status from PENDING_PRINT to VALID
--
--        AUTHOR: Jos� Mart�nez L�pez
--
-- CREATION DATE: 7-NOV-2017
--
-- REVISION HISTORY:
--
--
-- Date        Author Description
-- ----------- ------ --------------------------------------------------------------------------------------------------------
-- 07-NOV-2017 JML    First release.
------------------------------------------------------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TITO_UpdateTicketStatusToValid]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[TITO_UpdateTicketStatusToValid]
GO


CREATE PROCEDURE [dbo].[TITO_UpdateTicketStatusToValid]
                  @pStatus              AS Int
                , @pMachineNumber       AS BigInt
                , @pValidationType      AS Int                                                       
                        
                , @pCreatedTerminalId   AS Int
                , @pValidationNumber    AS BigInt
                , @pTransactionId       AS BigInt
                , @pStatusPendingPrint  AS Int
                        
                , @pAmountCents         AS BigInt                                                       
                , @pCreatedAccountID    AS BigInt
                , @pPlaySessionId       AS BigInt
                
                , @OriginalTicketAmt0   AS Money       OUTPUT
                , @OriginalTicketCur0   AS NVARCHAR(3) OUTPUT
                , @OriginalTicketAmount AS Money       OUTPUT

                , @UpdateTicketAmt0     AS Money       OUTPUT
                , @UpdateTicketCur0     AS NVARCHAR(3) OUTPUT
                , @UpdateTicketAmount   AS Money       OUTPUT
                
                , @AlarmId              AS Int         OUTPUT

AS
BEGIN
  DECLARE @LastTicketId         AS BigInt
  
  DECLARE @SysCur               AS NVARCHAR(3)
  
  DECLARE @NewTicketAmount      AS Money
  DECLARE @NewTicketAmt0        AS Money
  DECLARE @NewTicketCur0        AS NVARCHAR(3)
  
  SET NOCOUNT OFF
  
  SET @LastTicketId = 0
  
  SET @AlarmId = 0
  
  -- SELECT LAST TICKET ID
  SELECT   @LastTicketId = MAX(TI_TICKET_ID)
    FROM   TICKETS 
   WHERE   TI_CREATED_TERMINAL_ID = @pCreatedTerminalId        
     AND   TI_VALIDATION_NUMBER   = @pValidationNumber         
     AND   TI_STATUS              = @pStatusPendingPrint 
     AND   DATEDIFF(DAY, TI_CREATED_DATETIME, GETDATE()) <= 30  --FJC 18-10-2016 (PBI 18258) ValidationNumber could be repeated in one or more years. (ACC)  

  IF IsNull(@LastTicketId, 0) > 0
  BEGIN
    -- REGISTER TICKET BLOKED
    UPDATE   TICKETS 
       SET   TI_VALIDATION_NUMBER = TI_VALIDATION_NUMBER
     WHERE   TI_TICKET_ID = @LastTicketId
    
    -- GET SYSTEM CURRENCY CODE
    SELECT @SysCur = GP_KEY_VALUE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'RegionalOptions' AND GP_SUBJECT_KEY = 'CurrencyISOCode'

    -- Change amount in cents to amount in money with decimals
    SET @NewTicketAmt0 = isNull(@pAmountCents, 0) / 100.00

    -- SELECT TICKET/TERMINAL DATA
         SELECT   @OriginalTicketAmount = TI_AMOUNT
                , @OriginalTicketAmt0   = TI_AMT0
                , @OriginalTicketCur0  = TI_CUR0
                , @NewTicketCur0       = ISNULL(TE_ISO_CODE, @SysCur)
           FROM   TICKETS
      LEFT JOIN   TERMINALS ON TE_TERMINAL_ID = TI_CREATED_TERMINAL_ID
          WHERE   TI_TICKET_ID = @LastTicketId
    
    -- SET DEFAULT VALUE FOR NEW TICKET AMOUNT
    SET @NewTicketAmount = @OriginalTicketAmount
    IF @NewTicketCur0 = @SysCur and @NewTicketAmt0 > 0
    BEGIN
      SET @NewTicketAmount = @NewTicketAmt0
    END

    -- GET AMOUNTS FOR UPDATE TICKET
    EXEC dbo.GetTicketAmounts   @pCreatedTerminalId
    
                              , @OriginalTicketAmount 
                              , @OriginalTicketAmt0
                              , @OriginalTicketCur0
                                 
                              , @NewTicketAmount
                              , @NewTicketAmt0
                              , @NewTicketCur0
                                 
                              , @UpdateTicketAmt0   OUTPUT
                              , @UpdateTicketCur0   OUTPUT
                              , @UpdateTicketAmount OUTPUT
                                 
                              , @AlarmId            OUTPUT
                                 
    -- If change amount amount or currency, calculate new amount in local currency (floor Dual Currency)
    IF @OriginalTicketAmt0 <> @UpdateTicketAmt0 OR @OriginalTicketCur0 <> @UpdateTicketCur0 OR @OriginalTicketAmount <> @UpdateTicketAmount
    BEGIN
      SELECT @UpdateTicketAmount = dbo.ApplyExchange2(@UpdateTicketAmt0, @UpdateTicketCur0, @SysCur)
    END


    SET NOCOUNT ON
     
    -- UPDATE TICKET AND SET STATUS VALID
    UPDATE   TICKETS                                             
       SET   TI_STATUS              = @pStatus                   
           , TI_MACHINE_NUMBER      = @pMachineNumber            
           , TI_VALIDATION_TYPE     = @pValidationType           
           , TI_TRANSACTION_ID      = @pTransactionId            
           , TI_AMOUNT              = @UpdateTicketAmount
           , TI_AMT0                = @UpdateTicketAmt0
           , TI_CUR0                = @UpdateTicketCur0
     WHERE   TI_TICKET_ID = @LastTicketId
       
  END --   IF IsNull(@LastTicketId, 0) > 0
  
END
GO

GRANT EXECUTE ON [TITO_UpdateTicketStatusToValid] TO wggui WITH GRANT OPTION
GO

--****************************************************************************************************************************
--****************************************************************************************************************************
--****************************************************************************************************************************

------------------------------------------------------------------------------------------------------------------------------
-- Copyright � 2017 Win Systems International
------------------------------------------------------------------------------------------------------------------------------
--
--   MODULE NAME: TITO_CreateTicket
--
--   DESCRIPTION: Insert TITO Ticket in Database
--
--        AUTHOR: Jos� Mart�nez L�pez
--
-- CREATION DATE: 10-NOV-2017
--
-- REVISION HISTORY:
--
--
-- Date        Author Description
-- ----------- ------ --------------------------------------------------------------------------------------------------------
-- 10-NOV-2017 JML    First release.
------------------------------------------------------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TITO_CreateTicket]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[TITO_CreateTicket]
GO

CREATE PROCEDURE [dbo].[TITO_CreateTicket]
                 @pTicketValidationNumber        AS BigInt           
               , @pTicketAmount                  AS Money
               , @pTicketStatus                  AS Int              
               , @pTicketStatusCheck             AS Int              
               , @pTicketType                    AS Int              
               , @pTicketCreatedDateTime         AS DateTime         
               , @pCreatedTerminalId             AS Int              
               , @pTerminalType                  AS Int              
               , @pTicketExpirationDateTime      AS DateTime       
               , @pTicketCreatedAccountID        AS BigInt           
               , @pPromotionId                   AS BigInt           
               , @pValidationType                AS Int              
               , @pMachineTicketNumber           AS Int              
               , @pTransactionId                 AS BigInt           
               , @pCreatedPlaySessionId          AS BigInt           
               , @pAccountPromotion              AS BigInt           
               , @pAmt0                          AS Money            
               , @pCur0                          AS NVarChar(3)
               , @pPendingPrintEnabled           AS Bit
               , @pTimeToCheckDuplicityInCashout AS Int

               --- OUTPUT
               , @pTicketId                      AS BigInt OUTPUT
               , @pValidationNumberOutput        AS BigInt OUTPUT
               , @pTransactionIdOutput           AS BigInt OUTPUT

                                
AS
BEGIN

  IF @pPendingPrintEnabled = 1 
  BEGIN

    SET @pValidationNumberOutput = 0  
    SET @pTransactionIdOutput    = 0  

    DECLARE @TempTableTicket TABLE ( TI_TICKET_ID            BIGINT                                                          
                                   , TI_VALIDATION_NUMBER    BIGINT                                                          
                                   , TI_TRANSACTION_ID       BIGINT                                                          
                                   )  

    -- PBI 18258:TITO: Nuevo estado "Pending Print" en tickets - Creaci�n nuevo estado

    IF (@pTimeToCheckDuplicityInCashout > 0)
    BEGIN
      -- FJC PBI 18258:TITO: We try to avoid "duplicated tickets"
      -- If there's an equality between ticket in DB and requested creation ticket with: (the same Amount, terminalId and is been created in less than N seconds) and this .
      -- Probably is a duplicated ticket.
      INSERT INTO @TempTableTicket    
      SELECT   TI_TICKET_ID                                                                                  
             , TI_VALIDATION_NUMBER                                                                          
             , TI_TRANSACTION_ID                                                                             
        FROM   TICKETS                                                                                       
       WHERE   TI_STATUS = @pTicketStatusCheck                                                               
         AND   TI_CREATED_TERMINAL_ID = @pCreatedTerminalId                                                  
         AND   TI_AMOUNT = @pTicketAmount                                                                    
         AND   DATEDIFF(SECOND, TI_CREATED_DATETIME, GETDATE()) < @pTimeToCheckDuplicityInCashout
      -- FJC PBI 18258:TITO: We try to avoid "duplicated tickets"
    END

    IF NOT EXISTS ( SELECT   TI_TICKET_ID FROM @TempTableTicket)                                                           
    BEGIN

       INSERT   INTO TICKETS              
              ( TI_VALIDATION_NUMBER      
              , TI_AMOUNT                 
              , TI_STATUS                 
              , TI_TYPE_ID                
              , TI_CREATED_DATETIME       
              , TI_CREATED_TERMINAL_ID    
              , TI_CREATED_TERMINAL_TYPE  
              , TI_EXPIRATION_DATETIME    
              , TI_CREATED_ACCOUNT_ID     
              , TI_PROMOTION_ID           
              , TI_VALIDATION_TYPE        
              , TI_MACHINE_NUMBER         
              , TI_TRANSACTION_ID         
              , TI_CREATED_PLAY_SESSION_ID                                                                                      
              , TI_ACCOUNT_PROMOTION      
              , TI_AMT0                   
              , TI_CUR0                   
              )                           

       OUTPUT   INSERTED.TI_TICKET_ID     
              , INSERTED.TI_VALIDATION_NUMBER                                                                                   
              , INSERTED.TI_TRANSACTION_ID                                                                                      
         INTO   @TempTableTicket          
       VALUES ( @pTicketValidationNumber  
              , @pTicketAmount            
              , @pTicketStatus            
              , @pTicketType              
              , @pTicketCreatedDateTime   
              , @pCreatedTerminalId       
              , @pTerminalType            
              , @pTicketExpirationDateTime                                                                                      
              , @pTicketCreatedAccountID  
              , @pPromotionId             
              , @pValidationType          
              , @pMachineTicketNumber     
              , @pTransactionId           
              , @pCreatedPlaySessionId    
              , @pAccountPromotion        
              , @pAmt0                    
              , @pCur0)                   

    END                               

    SELECT   @pTicketId = TI_TICKET_ID                                                                                       
           , @pValidationNumberOutput = TI_VALIDATION_NUMBER                                                                 
           , @pTransactionIdOutput = ISNULL(TI_TRANSACTION_ID, 0)                                                            
      FROM   @TempTableTicket          
  END
  ELSE        --  IF @pPendingPrintEnabled = 1 
  BEGIN
       INSERT   INTO TICKETS                  
              ( TI_VALIDATION_NUMBER          
              , TI_AMOUNT                     
              , TI_STATUS                     
              , TI_TYPE_ID                    
              , TI_CREATED_DATETIME           
              , TI_CREATED_TERMINAL_ID        
              , TI_CREATED_TERMINAL_TYPE      
              , TI_EXPIRATION_DATETIME        
              , TI_CREATED_ACCOUNT_ID         
              , TI_PROMOTION_ID               
              , TI_VALIDATION_TYPE            
              , TI_MACHINE_NUMBER             
              , TI_TRANSACTION_ID             
              , TI_CREATED_PLAY_SESSION_ID    
              , TI_ACCOUNT_PROMOTION          
              , TI_AMT0                       
              , TI_CUR0                       
              )                               
       VALUES ( @pTicketValidationNumber      
              , @pTicketAmount                
              , @pTicketStatus                
              , @pTicketType                  
              , @pTicketCreatedDateTime       
              , @pCreatedTerminalId           
              , @pTerminalType                
              , @pTicketExpirationDateTime    
              , @pTicketCreatedAccountID      
              , @pPromotionId                 
              , @pValidationType              
              , @pMachineTicketNumber         
              , @pTransactionId               
              , @pCreatedPlaySessionId        
              , @pAccountPromotion            
              , @pAmt0                        
              , @pCur0)                       

    SET   @pTicketId = SCOPE_IDENTITY() 
  END

END --- TITO_CreateTicket
GO

GRANT EXECUTE ON [TITO_CreateTicket] TO wggui WITH GRANT OPTION
GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_CheckCashDeskDrawConfig]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_CheckCashDeskDrawConfig]
GO


CREATE PROCEDURE [dbo].[sp_CheckCashDeskDrawConfig]
      -- Add the parameters for the stored procedure here
      @pID INT = 0,
      @pName VARCHAR(20) = ''
AS
BEGIN
SET NOCOUNT ON;

DELETE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CashDesk.Draw.00'
DELETE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration.00'
   
DECLARE @POSTFIJ AS VARCHAR(5)

IF @pID = 0
BEGIN
  SET @POSTFIJ = ''
END
ELSE 
BEGIN 
  SET @POSTFIJ = '.' + RIGHT( '00' + CONVERT(VARCHAR(2),@pID),2)
END


  --'CashDesk.Draw', 'Enabled', 0
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ
                        AND GP_SUBJECT_KEY = 'Enabled'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'Enabled', '0')
END


--'CashDesk.Draw', 'ShowCashDeskDraws', 0
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ
                        AND GP_SUBJECT_KEY = 'ShowCashDeskDraws'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'ShowCashDeskDraws', '0')
END


--'CashDesk.Draw', 'VoucherOnCashDeskDraws.Winner', 0
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'VoucherOnCashDeskDraws.Winner'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'VoucherOnCashDeskDraws.Winner', '0')
END


--'CashDesk.Draw', 'WinnerPrizeVoucherTitle', 'Sorteo en XXXX'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrizeVoucherTitle'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'WinnerPrizeVoucherTitle', 'Sorteo en ' + @pName)
END


--'CashDesk.Draw', 'VoucherOnCashDeskDraws.Loser', 0
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'VoucherOnCashDeskDraws.Loser'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'VoucherOnCashDeskDraws.Loser', '0')
END


--'CashDesk.Draw', 'AskForParticipation', 0
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'AskForParticipation'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'AskForParticipation', '0')
END


--'CashDesk.Draw', 'LoserPrizeVoucherTitle','Cortes�a sorteo en XXXX'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'LoserPrizeVoucherTitle'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'LoserPrizeVoucherTitle', 'Cortes�a sorteo en ' + @pName)
END


--'CashDesk.Draw', 'ActionOnServerDown','0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'ActionOnServerDown'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'ActionOnServerDown', '0')
END


--'CashDesk.Draw', 'LocalServer', '1'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'LocalServer'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'LocalServer', '1')
END


--'CashDesk.Draw', 'ServerBDConnectionString',''
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'ServerBDConnectionString'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'ServerBDConnectionString', '')
END


--'CashDesk.Draw', 'ServerAddress1',''
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'ServerAddress1'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'ServerAddress1', '')
END


--'CashDesk.Draw', 'ServerAddress2',''
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'ServerAddress2'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'ServerAddress2', '')
END

--'CashDesk.Draw', 'GroupDraws',''
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'GroupDraws'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'GroupDraws', '1')
END

--'CashDesk.Draw', 'LetWithdrawalWithPendingDraw',''
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'LetWithdrawalWithPendingDraw'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'LetWithdrawalWithPendingDraw', '1')
END

--'CashDesk.Draw', 'LetCancelRechargeWithPendingDraw',''
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'LetCancelRechargeWithPendingDraw'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'LetCancelRechargeWithPendingDraw', '1')
END

--'CashDesk.DrawConfiguration', 'BallsExtracted','0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'BallsExtracted'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'BallsExtracted', '0')
END


--'CashDesk.DrawConfiguration', 'BallsOfParticipant', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'BallsOfParticipant'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'BallsOfParticipant', '0')
END


--'CashDesk.DrawConfiguration', 'TotalsBallsNumber', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'TotalsBallsNumber'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'TotalsBallsNumber', '0')
END


--'CashDesk.DrawConfiguration', 'LoserPrize1.Fixed', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'LoserPrize1.Fixed'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'LoserPrize1.Fixed', '0')
END


--'CashDesk.DrawConfiguration', 'LoserPrize1.Percentage', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'LoserPrize1.Percentage'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'LoserPrize1.Percentage', '0')
END


--'CashDesk.DrawConfiguration', 'LoserPrize1.Enabled', 0
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'LoserPrize1.Enabled'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'LoserPrize1.Enabled', '0')
END


--'CashDesk.DrawConfiguration', 'NumberOfParticipants', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'NumberOfParticipants'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'NumberOfParticipants', '0')
END


--'CashDesk.DrawConfiguration', 'NumberOfWinners', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'NumberOfWinners'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'NumberOfWinners', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize1.Fixed', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize1.Fixed'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize1.Fixed', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize1.Percentage', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize1.Percentage'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize1.Percentage', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize1.Enabled', 0
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize1.Enabled'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize1.Enabled', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize2.Fixed', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize2.Fixed'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize2.Fixed', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize2.Percentage', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize2.Percentage'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize2.Percentage', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize2.Enabled', 0
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize2.Enabled'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize2.Enabled', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize3.Fixed', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize3.Fixed'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize3.Fixed', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize3.Percentage', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize3.Percentage'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize3.Percentage', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize3.Enabled', 0
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize3.Enabled'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize3.Enabled', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize4.Fixed', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize4.Fixed'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize4.Fixed', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize4.Percentage', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize4.Percentage'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize4.Percentage', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize4.Enabled', 0
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize4.Enabled'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize4.Enabled', '0')
END



--'CashDesk.DrawConfiguration', 'WinnerPrize1.PrizeType', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize1.PrizeType'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize1.PrizeType', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize2.PrizeType', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize2.PrizeType'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize2.PrizeType','0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize3.PrizeType', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize3.PrizeType'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize3.PrizeType', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize4.PrizeType', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize4.PrizeType'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize4.PrizeType', '0')
END


--'CashDesk.DrawConfiguration', 'LoserPrize1.PrizeType', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'LoserPrize1.PrizeType'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'LoserPrize1.PrizeType', '0')
END


-- 'CashDesk.Draw', 'AccountingMode', ''
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'AccountingMode'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'AccountingMode', '0')
END


-- 'CashDesk.DrawConfiguration', 'ParticipationPrice', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'ParticipationPrice'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'ParticipationPrice', '0')
END   
      
      
-- 'CashDesk.Draw', 'IsCashDeskDraw', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'IsCashDeskDraw'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'IsCashDeskDraw', '0')
END         


-- 'CashDesk.Draw', 'ReportUNRFromSP_SalesAndPayment', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'ReportUNRFromSP_SalesAndPayment'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'ReportUNRFromSP_SalesAndPayment', '0')
END   


-- 'CashDesk.Draw', 'AccountingMode.PromoNameMovement', 'PROMOCI�N'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'AccountingMode.PromoNameMovement'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'AccountingMode.PromoNameMovement', 'Promoci�n')
END   

-- 'CashDesk.Draw', 'AccountingMode.KindOfNameMovement', 'Premio en especie'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'AccountingMode.KindOfNameMovement'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'AccountingMode.KindOfNameMovement', 'Premio en especie')
END   

-- 'CashDesk.Draw', 'AccountingMode.RE.KindOfNameMovement', 'Premio en especie (RE)'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'AccountingMode.RE.KindOfNameMovement'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'AccountingMode.RE.KindOfNameMovement', 'Premio en especie (RE)')
END   

-- 'CashDesk.Draw', 'AccountingMode.RE.PromoNameMovement', 'Promoci�n (RE)'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'AccountingMode.RE.PromoNameMovement'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'AccountingMode.RE.PromoNameMovement', 'Promoci�n (RE)')
END   


-- 'CashDesk.Draw', 'Voucher.HideCurrencySymbol', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'Voucher.HideCurrencySymbol'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'Voucher.HideCurrencySymbol', '0')
END   
      
-- 'CashDesk.Draw', 'Voucher.LoserPrizeLabel', 'Cortesia'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'Voucher.LoserPrizeLabel'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'Voucher.LoserPrizeLabel', 'Cortesia')
END   
      
-- 'CashDesk.Draw', 'Voucher.UNRLabel', 'FD'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'Voucher.UNRLabel'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'Voucher.UNRLabel', 'FD')
END   

-- 'CashDesk.Draw', 'Voucher.WinnerPrizeLabel', 'Premio'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'Voucher.WinnerPrizeLabel'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'Voucher.WinnerPrizeLabel', 'Premio')
END   


--/// Probability


IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'LoserPrize1.Probability'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'LoserPrize1.Probability', '0')
END

IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize1.Probability'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize1.Probability', '0')
END

IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize2.Probability'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize2.Probability', '0')
END

IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize3.Probability'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize3.Probability', '0')
END

IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'WinnerPrize4.Probability'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize4.Probability', '0')
END


IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'ParticipationMaxPrice'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'ParticipationMaxPrice', '1')
END


IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameUrl'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameUrl', '')
END


IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameTimeout'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameTimeout', '30')
END

IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameName'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameName', 'Terminal Draw')
END

IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameSecondDrawScreenMessageLine0'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameSecondDrawScreenMessageLine0', 'Bienvenido')
END

IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameSecondDrawScreenMessageLine1'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameSecondDrawScreenMessageLine1', 'Pulse[1][2][3][4][5]')
END

IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameSecondDrawScreenMessageLine2'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameSecondDrawScreenMessageLine2', 'Saldo: @BALANCE')
END


IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameSecondDrawScreenMessageLine3'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameSecondDrawScreenMessageLine3', 'Apuesta: @BET')
END

--Message Result Winner
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameResultWinnerDrawScreenMessageLine0'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameResultWinnerDrawScreenMessageLine0', 'Felicidades!!!')
END


IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameResultWinnerDrawScreenMessageLine1'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameResultWinnerDrawScreenMessageLine1', 'Ganaste: @WON')
END


IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameResultWinnerDrawScreenMessageLine2'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameResultWinnerDrawScreenMessageLine2', 'Saldo: @BALANCE')
END

IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameResultWinnerDrawScreenMessageLine3'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameResultWinnerDrawScreenMessageLine3', '')
END

--Message Result Looser
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameResultLooserDrawScreenMessageLine0'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameResultLooserDrawScreenMessageLine0', 'Perdiste!!!')
END


IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameResultLooserDrawScreenMessageLine1'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameResultLooserDrawScreenMessageLine1', 'Premio cortes�a: @WON')
END


IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameResultLooserDrawScreenMessageLine2'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameResultLooserDrawScreenMessageLine2', 'Saldo: @BALANCE')
END

IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameResultLooserDrawScreenMessageLine3'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameResultLooserDrawScreenMessageLine3', '')
END

-- First Screen
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameFirstDrawScreenMessageLine0'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameFirstDrawScreenMessageLine0', '')
END


IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameFirstDrawScreenMessageLine1'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameFirstDrawScreenMessageLine1', 'Pulse [1] para jugar')
END


IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameFirstDrawScreenMessageLine2'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameFirstDrawScreenMessageLine2', '')
END


IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameFirstDrawScreenTimeOut'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameFirstDrawScreenTimeOut', '10')
END


IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameFirsIfTimeOutExpiresParticipateInDraw'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameFirsIfTimeOutExpiresParticipateInDraw', '0')
END


IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'CloseOpenedSessionsBeforeNewGame'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'CloseOpenedSessionsBeforeNewGame', '0')
END
      
END
GO

GRANT EXECUTE ON sp_CheckCashDeskDrawConfig TO wggui WITH GRANT OPTION
GO

 IF OBJECT_ID (N'dbo.SP_Cashier_Sessions_Report', N'P') IS NOT NULL
    DROP PROCEDURE dbo.SP_Cashier_Sessions_Report;                 
GO  

/*
----------------------------------------------------------------------------------------------------------------
CASHIER SESSIONS REPORTS

Version  Date           User     Description
----------------------------------------------------------------------------------------------------------------
1.0.0    28-FEB-2014    RMS      First Release
1.0.1    14-APR-2015    RMS      Gaming Day
1.0.2    01-FEB-2016    FOS      Floor Dual Currency

Parameters:
   -- :
  
 Results:
   
----------------------------------------------------------------------------------------------------------------   
*/


CREATE PROCEDURE [dbo].[SP_Cashier_Sessions_Report]
(  @pDateFrom DateTime
  , @pDateTo DateTime
  , @pReportMode as Int
  , @pShowSystemSessions as Bit
  , @pSystemUsersTypes as VarChar(4096)
  , @pSessionStatus as VarChar(4096)
  , @pUserId as Integer
  , @pCashierId as Integer
  , @pCurrencyISOCode as VarChar(3)
)
AS
BEGIN
-- Members --
DECLARE @_delimiter AS CHAR(1)
SET @_delimiter = ','
DECLARE @pSystemUsersTypesTable AS TABLE (SUT_ID INT, SUT_VALUE VARCHAR(50))
DECLARE @pSessionsStatusTable AS TABLE (SUT_ID INT, SUT_VALUE VARCHAR(50))
DECLARE @_table_sessions_ AS TABLE (  sess_id                      BIGINT 
                                    , sess_name                    NVARCHAR(50) 
                                    , sess_user_id                 INT      
                                    , sess_full_name               NVARCHAR(50) 
                                    , sess_opening_date            DATETIME 
                                    , sess_closing_date            DATETIME  
                                    , sess_cashier_id              INT 
                                    , sess_ct_name                 NVARCHAR(50) 
                                    , sess_status                  INT 
                                    , sess_cs_limit_sale           MONEY
                                    , sess_mb_limit_sale           MONEY
                                    , sess_collected_amount        MONEY
                                    , sess_gaming_day              DATETIME
                                    , sess_te_iso_code             NVARCHAR(3)
                                   ) 
                                                           
-- SYSTEM USER TYPES --
IF @pShowSystemSessions = 0
BEGIN
INSERT INTO @pSystemUsersTypesTable SELECT * FROM dbo.SplitStringIntoTable(@pSystemUsersTypes, @_delimiter, DEFAULT)
END

-- SESSIONS STATUS
INSERT INTO @pSessionsStatusTable SELECT * FROM dbo.SplitStringIntoTable(@pSessionStatus, @_delimiter, DEFAULT)

IF @pReportMode = 0 OR @pReportMode = 2
   BEGIN -- Filter Dates on cashier session opening date  

    IF @pReportMode = 0
      BEGIN
        -- SESSIONS BY OPENING DATE --
        INSERT   INTO @_table_sessions_
        SELECT   DISTINCT(CS_SESSION_ID) AS SESS_ID
               , CS_NAME AS SESS_NAME
               , CS_USER_ID AS SESS_USER_ID
               , GU_FULL_NAME AS SESS_FULL_NAME
               , CS_OPENING_DATE AS SESS_OPENING_DATE
               , CS_CLOSING_DATE AS SESS_CLOSING_DATE
               , CS_CASHIER_ID AS SESS_CASHIER_ID
               , CT_NAME AS SESS_CT_NAME
               , CS_STATUS AS SESS_STATUS
               , CS_SALES_LIMIT AS SESS_CS_LIMIT_SALE
               , CS_MB_SALES_LIMIT AS SESS_MB_LIMIT_SALE
               , CS_COLLECTED_AMOUNT AS SESS_COLLECTED_AMOUNT
               , CS_GAMING_DAY AS SESS_GAMING_DAY
               , ISNULL(TE_ISO_CODE, @pCurrencyISOCode) AS SESS_TE_ISO_CODE 
          FROM   CASHIER_SESSIONS --WITH (INDEX (IX_cs_opening_date)) /* TODO: REVIEW INDEXES */
         INNER   JOIN GUI_USERS         ON CS_USER_ID    = GU_USER_ID
         INNER   JOIN CASHIER_TERMINALS ON CS_CASHIER_ID = CT_CASHIER_ID
          LEFT   JOIN TERMINALS         ON CT_TERMINAL_ID = TE_TERMINAL_ID   
         WHERE   (CS_OPENING_DATE >= @pDateFrom AND CS_OPENING_DATE < @pDateTo)
            --   usuarios sistema s/n & user 
           AND   (      GU_USER_TYPE NOT IN ( SELECT SUT_VALUE FROM @pSystemUsersTypesTable )
                  AND   CS_USER_ID = (CASE WHEN @pUserId IS NULL THEN CS_USER_ID ELSE @pUserId END) )
            --   cashier id
           AND   CS_CASHIER_ID = (CASE WHEN @pCashierId IS NULL THEN CS_CASHIER_ID ELSE @pCashierId END) 
            --   session status
           AND   CS_STATUS IN ( SELECT SUT_VALUE FROM @pSessionsStatusTable )
         ORDER   BY CS_OPENING_DATE DESC  
      END
    ELSE
      BEGIN
        -- @pReportMode = 2
        -- SESSIONS BY GAMING DAY --
        INSERT   INTO @_table_sessions_
        SELECT   DISTINCT(CS_SESSION_ID) AS SESS_ID
               , CS_NAME AS SESS_NAME
               , CS_USER_ID AS SESS_USER_ID
               , GU_FULL_NAME AS SESS_FULL_NAME
               , CS_OPENING_DATE AS SESS_OPENING_DATE
               , CS_CLOSING_DATE AS SESS_CLOSING_DATE
               , CS_CASHIER_ID AS SESS_CASHIER_ID
               , CT_NAME AS SESS_CT_NAME
               , CS_STATUS AS SESS_STATUS
               , CS_SALES_LIMIT AS SESS_CS_LIMIT_SALE
               , CS_MB_SALES_LIMIT AS SESS_MB_LIMIT_SALE
               , CS_COLLECTED_AMOUNT AS SESS_COLLECTED_AMOUNT
               , CS_GAMING_DAY AS SESS_GAMING_DAY
               , ISNULL(TE_ISO_CODE, @pCurrencyISOCode) AS SESS_TE_ISO_CODE 
          FROM   CASHIER_SESSIONS --WITH (INDEX (IX_cs_opening_date)) /* TODO: REVIEW INDEXES */
         INNER   JOIN GUI_USERS         ON CS_USER_ID    = GU_USER_ID
         INNER   JOIN CASHIER_TERMINALS ON CS_CASHIER_ID = CT_CASHIER_ID
          LEFT   JOIN TERMINALS         ON CT_TERMINAL_ID = TE_TERMINAL_ID   
         WHERE   (CS_GAMING_DAY >= @pDateFrom AND CS_GAMING_DAY < @pDateTo)
            --   usuarios sistema s/n & user 
           AND   (      GU_USER_TYPE NOT IN ( SELECT SUT_VALUE FROM @pSystemUsersTypesTable )
                  AND   CS_USER_ID = (CASE WHEN @pUserId IS NULL THEN CS_USER_ID ELSE @pUserId END) )
            --   cashier id
           AND   CS_CASHIER_ID = (CASE WHEN @pCashierId IS NULL THEN CS_CASHIER_ID ELSE @pCashierId END) 
            --   session status
           AND   CS_STATUS IN ( SELECT SUT_VALUE FROM @pSessionsStatusTable )
         ORDER   BY CS_GAMING_DAY DESC  
      END
      
    -- MOVEMENTS
    -- By Movements in Historical Data --    
    SELECT   CM_SESSION_ID
           , CM_TYPE
           , CM_SUB_TYPE
           , ISNULL(CM_CURRENCY_ISO_CODE, @pCurrencyISOCode) AS CM_CURRENCY_ISO_CODE
           , CM_CURRENCY_DENOMINATION
           , CM_TYPE_COUNT
           , CM_SUB_AMOUNT
           , CM_ADD_AMOUNT
           , CM_AUX_AMOUNT
           , CM_INITIAL_BALANCE
           , CM_FINAL_BALANCE
           , CM_CAGE_CURRENCY_TYPE
      FROM   CASHIER_MOVEMENTS_GROUPED_BY_SESSION_ID
     WHERE   CM_SESSION_ID IN (SELECT SESS_ID FROM @_table_sessions_)

   END
ELSE
  BEGIN -- Filter Dates on cashier movements date (@pReportMode = 1)
       
      -- MOVEMENTS
      -- By Cashier_Movements --   
      SELECT * 
      INTO #CASHIER_MOVEMENTS
      FROM ( 
            SELECT   CM_DATE
                   , CM_SESSION_ID
                   , CM_TYPE
                   , 0 as CM_SUB_TYPE
                   , ISNULL(CM_CURRENCY_ISO_CODE, @pCurrencyISOCode) AS CM_CURRENCY_ISO_CODE
                   , CM_CURRENCY_DENOMINATION
                   , CASE WHEN CM_SUB_AMOUNT < 0 OR CM_ADD_AMOUNT < 0 THEN -1 ELSE 1 END    AS CM_TYPE_COUNT
                   , CM_SUB_AMOUNT
                   , CM_ADD_AMOUNT
                   , CM_AUX_AMOUNT
                   , CM_INITIAL_BALANCE
                   , CM_FINAL_BALANCE
                   , ISNULL(CM_CAGE_CURRENCY_TYPE, 0) AS CM_CAGE_CURRENCY_TYPE
              FROM   CASHIER_MOVEMENTS 
             WHERE   CM_DATE >= @pDateFrom AND CM_DATE < @pDateTo
             UNION   ALL
            SELECT   MBM_DATETIME AS CM_DATE
                   , MBM_CASHIER_SESSION_ID AS CM_SESSION_ID
                   , MBM_TYPE AS CM_TYPE
                   , 1 AS CM_SUB_TYPE
                   , @pCurrencyISOCode AS CM_CURRENCY_ISO_CODE
                   , 0 AS CM_CURRENCY_DENOMINATION
                   , 1 AS CM_TYPE_COUNT
                   , ISNULL(MBM_SUB_AMOUNT, 0) AS CM_SUB_AMOUNT
                   , ISNULL(MBM_ADD_AMOUNT, 0) AS CM_ADD_AMOUNT
                   , 0 AS CM_AUX_AMOUNT
                   , 0 AS CM_INITIAL_BALANCE
                   , 0 AS CM_FINAL_BALANCE
                   , 0 AS CM_CAGE_CURRENCY_TYPE
              FROM   MB_MOVEMENTS 
             WHERE   MBM_DATETIME >= @pDateFrom AND MBM_DATETIME < @pDateTo
       ) AS MOVEMENTS
       
      CREATE NONCLUSTERED INDEX IX_CM_SESSION_ID ON #CASHIER_MOVEMENTS(CM_SESSION_ID)

      -- SESSIONS --  
      INSERT   INTO @_table_sessions_  
      SELECT   DISTINCT(CS_SESSION_ID) AS SESS_ID  
             , CS_NAME AS SESS_NAME  
             , CS_USER_ID AS SESS_USER_ID  
             , GU_FULL_NAME AS SESS_FULL_NAME  
             , CS_OPENING_DATE AS SESS_OPENING_DATE  
			       , CS_CLOSING_DATE AS SESS_CLOSING_DATE  
             , CS_CASHIER_ID AS SESS_CASHIER_ID  
             , CT_NAME AS SESS_CT_NAME  
             , CS_STATUS AS SESS_STATUS  
             , CS_SALES_LIMIT AS SESS_CS_LIMIT_SALE  
             , CS_MB_SALES_LIMIT AS SESS_MB_LIMIT_SALE  
             , CS_COLLECTED_AMOUNT AS SESS_COLLECTED_AMOUNT  
             , CS_GAMING_DAY AS SESS_GAMING_DAY  
			       , ISNULL(TE_ISO_CODE, @pCurrencyISOCode) AS SESS_TE_ISO_CODE  
        FROM   CASHIER_SESSIONS --WITH (INDEX (IX_cs_opening_date)) /* TODO: REVIEW INDEXES */  
       INNER   JOIN GUI_USERS         ON CS_USER_ID    = GU_USER_ID  
       INNER   JOIN CASHIER_TERMINALS ON CS_CASHIER_ID = CT_CASHIER_ID  
	      LEFT   JOIN TERMINALS         ON CT_TERMINAL_ID = TE_TERMINAL_ID   
       WHERE   CS_SESSION_ID IN (SELECT CM_SESSION_ID FROM #CASHIER_MOVEMENTS)  
          --   usuarios sistema s/n & user   
         AND   (      GU_USER_TYPE NOT IN ( SELECT SUT_VALUE FROM @pSystemUsersTypesTable )  
                AND   CS_USER_ID = (CASE WHEN @pUserId IS NULL THEN CS_USER_ID ELSE @pUserId END) )  
          --   cashier id  
         AND   CS_CASHIER_ID = (CASE WHEN @pCashierId IS NULL THEN CS_CASHIER_ID ELSE @pCashierId END)   
          --   session status  
         AND   CS_STATUS IN ( SELECT SUT_VALUE FROM @pSessionsStatusTable )  
       ORDER   BY CS_OPENING_DATE DESC    
        
       SELECT * FROM #CASHIER_MOVEMENTS INNER JOIN @_table_sessions_ ON SESS_ID = CM_SESSION_ID  
                
       DROP TABLE #CASHIER_MOVEMENTS        
         
   END   
  
-- SESSIONS with CAGE INFO  
SELECT   SESS_ID  
       , SESS_NAME  
       , SESS_USER_ID  
       , SESS_FULL_NAME  
       , SESS_OPENING_DATE  
       , SESS_CLOSING_DATE  
       , SESS_CASHIER_ID  
       , SESS_CT_NAME  
       , SESS_STATUS  
       , SESS_CS_LIMIT_SALE  
       , SESS_MB_LIMIT_SALE  
       , SESS_COLLECTED_AMOUNT   
       , X.CGS_CAGE_SESSION_ID AS CGS_CAGE_SESSION_ID  
       , X.CGS_SESSION_NAME AS CGS_SESSION_NAME   
       , SESS_GAMING_DAY  
	   , SESS_TE_ISO_CODE 
       , (SELECT SUM(AO_AMOUNT) FROM  account_operations A1 WHERE A1.ao_code IN (1, 3, 17) AND NOT EXISTS(SELECT A.ao_operation_id FROM pinpad_transactions T INNER JOIN gui_users U ON U.gu_user_id = T.pt_user_id INNER JOIN account_operations A ON A.ao_operation_id = T.pt_operation_id WHERE T.pt_status = 1 AND A.ao_code IN (1, 3, 17) AND A.ao_operation_id = A1.ao_operation_id AND A.ao_cashier_session_id = SESS_ID) AND A1.ao_cashier_session_id = SESS_ID) AS SESS_MONEY_RECHARGE_AMOUNT
       , (SELECT SUM(T.pt_total_amount) FROM pinpad_transactions T INNER JOIN gui_users U ON U.gu_user_id = T.pt_user_id INNER JOIN account_operations A ON A.ao_operation_id = T.pt_operation_id WHERE T.pt_status = 1 AND A.ao_code IN (1, 3, 17) AND A.ao_cashier_session_id = SESS_ID) AS SESS_BANK_CARD_RECHARGE_AMOUNT
       , (SELECT Count(*) FROM pinpad_transactions T INNER JOIN gui_users U ON U.gu_user_id = T.pt_user_id INNER JOIN account_operations A ON A.ao_operation_id = T.pt_operation_id WHERE T.pt_status = 1 AND A.ao_code IN (1, 3, 17, 125) AND A.ao_cashier_session_id = SESS_ID) AS SESS_TOTAL_VOUCHERS
       , (SELECT Count(*) FROM pinpad_transactions T INNER JOIN gui_users U ON U.gu_user_id = T.pt_user_id INNER JOIN account_operations A ON A.ao_operation_id = T.pt_operation_id WHERE T.pt_status = 1 AND A.ao_code IN (1, 3, 17, 125) AND A.ao_cashier_session_id = SESS_ID AND EXISTS(SELECT 1 FROM pinpad_transactions_reconciliation WHERE ptc_id = T.pt_id AND ptc_reconciliate = 1)) AS SESS_DELIVERED_VOUCHERS
       , (SELECT ISNULL(SUM(T.pt_total_amount),0) FROM pinpad_transactions T INNER JOIN gui_users U ON U.gu_user_id = T.pt_user_id INNER JOIN account_operations A ON A.ao_operation_id = T.pt_operation_id WHERE T.pt_status = 1 AND A.ao_code IN (1, 3, 17, 125) AND A.ao_cashier_session_id = SESS_ID AND EXISTS(SELECT 1 FROM pinpad_transactions_reconciliation WHERE ptc_id = T.pt_id AND ptc_reconciliate = 1)) AS SESS_TOTAL_VOUCHERS_AMOUNT
  FROM   @_table_sessions_  
  LEFT   JOIN ( SELECT   CGS_SESSION_NAME  
                       , CGM_CASHIER_SESSION_ID  
                       , CGS_CAGE_SESSION_ID   
                  FROM   CAGE_MOVEMENTS AS CM  
                 INNER   JOIN CAGE_SESSIONS ON CM.CGM_CAGE_SESSION_ID = CGS_CAGE_SESSION_ID  
                 WHERE   CM.CGM_MOVEMENT_ID = (SELECT    TOP 1 CGM.CGM_MOVEMENT_ID  
                                                 FROM    CAGE_MOVEMENTS AS CGM   
                                                WHERE    CGM.CGM_CASHIER_SESSION_ID = CM.CGM_CASHIER_SESSION_ID)  
               ) AS X ON X.CGM_CASHIER_SESSION_ID = [@_table_sessions_].SESS_ID 
                                              
-- SESSIONS CURRENCIES INFORMATION
IF @pReportMode = 0 OR @pReportMode = 2
    SELECT   CSC_SESSION_ID
           , CSC_ISO_CODE
           , CSC_TYPE
           , CSC_BALANCE
           , CSC_COLLECTED 
      FROM   CASHIER_SESSIONS_BY_CURRENCY 
     INNER JOIN @_table_sessions_ ON SESS_ID = CSC_SESSION_ID

END -- End Procedure
GO

-- PERMISSIONS

GRANT EXECUTE ON [dbo].[SP_Cashier_Sessions_Report] TO [wggui] WITH GRANT OPTION
GO

------------------------------------------------------------------------------------------------------------------------------------------------
-- Copyright � 2017 Win Systems International
------------------------------------------------------------------------------------------------------------------------------------------------
-- 
--   MODULE NAME: ScoreReport.sql
-- 
--   DESCRIPTION: 
-- 
--        AUTHOR: 
-- 
-- CREATION DATE: 
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
------------------------------------------------------------------------------------------------------------------------------------------------
-- 19-JUL-2017 JML    PBI 28626: WIGOS-3190 Score report - Report in GUI
-- 09-AUG-2017 RAB    Bug 29293: WIGOS-4303 'Hourly score report' reports an incorrect Drop
-- 17-AUG-2017 RAB    Bug 29297: WIGOS-4304 'Hourly score report' reports the Win/Loss for the hour of the fill, and the next ones which does not have fills
-- 17-AUG-2017 RAB    Bug 29384: WIGOS-4391 Cashiers 'Hourly drop' does not match with Drop in WigosGUI 'Hourly score report'
-- 17-AUG-2017 RAB    Bug 29332: WIGOS-4150 Score report - Close gaming table amounts are not considered on WinLoss "Final" column
------------------------------------------------------------------------------------------------------------------------------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ScoreReport]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[ScoreReport]
GO

CREATE  PROCEDURE [dbo].[ScoreReport]
(
    @pDateFrom        AS DATETIME
  , @pCageFillerIn    AS INT
  , @pCageFillerOut   AS INT
  , @pChipRE          AS INT
  , @pSaleChips       AS INT
 )
AS
BEGIN
  DECLARE @old_time AS DATETIME
  DECLARE @DateTo   AS DATETIME
  DECLARE @Horas    AS NVARCHAR(1000)
  
  DECLARE @cols             AS NVARCHAR(MAX)
  DECLARE @query            AS NVARCHAR(MAX)
  DECLARE @NationalCurrency AS NVARCHAR(3)

  SET @old_time = '2007-01-01T07:00:00'
  SET @DateTo = DATEADD(DAY, 1, @pDateFrom)  
  SET @Horas = N'[0],[1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12],[13],[14],[15],[16],[17],[18],[19],[20],[21],[22],[23],[9999]'
  SET @NationalCurrency = (SELECT GP_KEY_VALUE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'RegionalOptions' AND GP_SUBJECT_KEY = 'CurrencyIsoCode')
  
  SELECT * 
    INTO #GROUPS_BY_SESSION 
    FROM SplitStringIntoTable(REPLACE(REPLACE(@Horas, '[', ''), ']', ''), ',', 1)

      SELECT   GTT_NAME                             AS 'TYPE_NAME'
             , GT_NAME                              AS 'TABLE_NAME'
             , CS_OPENING_DATE                      AS 'OPENING_DATE'
             , CS_CLOSING_DATE                      AS 'CLOSING_DATE'
             , GTS_GAMING_TABLE_SESSION_ID          AS 'GAMING_TABLE_SESSION_ID'
             , GTS_CASHIER_SESSION_ID               AS 'CASHIER_SESSION_ID'
             , CS_NAME                              AS 'SESION_NAME'
             , ISNULL(GTSC_INITIAL_CHIPS_AMOUNT, 0) AS 'INITIAL_BALANCE'
             , ISNULL(GTSC_FINAL_CHIPS_AMOUNT, 0)   AS 'FINAL_BALANCE'
             , CAST(GT_NUM_SEATS AS DECIMAL(18,2))  AS 'NUM_SEATS_IN_TABLE'
             , CAST(SST_VALUE AS INT)               AS 'HORA'
             , SST_VALUE                            AS 'HORA_STR'
             , CAST(0 AS DECIMAL(18,2))             AS 'GT_DROP'
             , CAST(0 AS DECIMAL(18,2))             AS 'GT_WIN_LOSS'
             , CAST(0 AS DECIMAL(18,2))             AS 'GT_HOLD'
             , CAST(0 AS DECIMAL(18,2))             AS 'GT_OCCUPATION_NUMBER'
        INTO   #VALUE_TABLE
        FROM   GAMING_TABLES
  INNER JOIN   GAMING_TABLES_TYPES                ON GT_TYPE_ID                    = GTT_GAMING_TABLE_TYPE_ID
  INNER JOIN   GAMING_TABLES_SESSIONS             ON GT_GAMING_TABLE_ID            = GTS_GAMING_TABLE_ID
   LEFT JOIN   GAMING_TABLES_SESSIONS_BY_CURRENCY ON GTSC_GAMING_TABLE_SESSION_ID  = GTS_GAMING_TABLE_SESSION_ID AND GTSC_TYPE = @pChipRE AND GTSC_ISO_CODE = @NATIONALCURRENCY
  INNER JOIN   CASHIER_SESSIONS                   ON GTS_CASHIER_SESSION_ID        = CS_SESSION_ID 
 CROSS APPLY   #GROUPS_BY_SESSION
       WHERE   CS_OPENING_DATE >= @pDateFrom
         AND   CS_OPENING_DATE <  @DateTo         
    ORDER BY   GTT_NAME                 
             , GT_NAME                  
             , CS_OPENING_DATE          
             , CAST(SST_VALUE AS INT)                                            

     --  SET DROP BY HOUR - TICKETS  
      UPDATE   #VALUE_TABLE
         SET   GT_DROP = GT_DROP + ISNULL((SELECT   SUM(TI_AMOUNT)
                                             FROM   GT_COPY_DEALER_VALIDATED 
                                       INNER JOIN   TICKETS  ON GTCD_TICKET_ID = TI_TICKET_ID
                                            WHERE   GAMING_TABLE_SESSION_ID = GTCD_GAMING_TABLE_SESSION_ID 
                                              AND   DATEADD(HOUR, DATEDIFF(HOUR, @old_time, GTCD_VALIDATION_DATETIME), @old_time) = DATEADD(HOUR, HORA,  @pDateFrom)), 0)
 
     --  SET DROP BY HOUR - AMOUNTS
      UPDATE   #VALUE_TABLE
         SET   GT_DROP = GT_DROP + ISNULL((SELECT   SUM(CASE WHEN GTPM_ISO_CODE <> @NationalCurrency THEN dbo.ApplyExchange2(GTPM_VALUE, GTPM_ISO_CODE, @NationalCurrency) ELSE  GTPM_VALUE END)
                                             FROM   GT_PLAYERTRACKING_MOVEMENTS 
                                            WHERE   GAMING_TABLE_SESSION_ID = GTPM_GAMING_TABLE_SESSION_ID 
                                              AND   DATEADD(HOUR, DATEDIFF(HOUR, @old_time, GTPM_DATETIME), @old_time) = DATEADD(HOUR, HORA,  @pDateFrom)
                                              AND   GTPM_TYPE = @pSaleChips), 0)                                                                                           
                                              
      --  SET WIN/LOSS WITH CURSORS      
      DECLARE @GT_SESSION_ID BIGINT
      DECLARE GT_SESSION_WIN_LOSS_CURSOR CURSOR GLOBAL
      FOR SELECT GAMING_TABLE_SESSION_ID
            FROM #VALUE_TABLE 
        GROUP BY GAMING_TABLE_SESSION_ID
                                                            
      OPEN GT_SESSION_WIN_LOSS_CURSOR
      FETCH NEXT FROM GT_SESSION_WIN_LOSS_CURSOR INTO @GT_SESSION_ID
      
      -- FIRST CURSOR: ITERANCE THROUGH ALL SESSIONS OF #VALUE_TABLE
      WHILE(@@FETCH_STATUS = 0)
        BEGIN        
        
          DECLARE @WIN_LOSS_HOUR INT
          DECLARE @WIN_LOSS_LAST_AMOUNT MONEY
          DECLARE @WIN_LOSS_CURRENT_AMOUNT MONEY
          DECLARE @WIN_LOSS_FINAL_AMOUNT MONEY
          DECLARE @WIN_LOSS_CLOSING_HOUR AS INT
          DECLARE @WIN_LOSS_CURRENT_HOUR AS INT
          DECLARE @WIN_LOSS_CLOSING_TABLE_AMOUNT MONEY
          DECLARE @CREDITS_SUB_FILLS MONEY     
          DECLARE @DROP_CURRENT_AMOUNT MONEY		
            
          DECLARE HOUR_WIN_LOSS_CURSOR CURSOR GLOBAL
          FOR SELECT HORA
                FROM #VALUE_TABLE
               WHERE GAMING_TABLE_SESSION_ID = @GT_SESSION_ID              
                                                            
          OPEN HOUR_WIN_LOSS_CURSOR
          FETCH NEXT FROM HOUR_WIN_LOSS_CURSOR INTO @WIN_LOSS_HOUR     
                                      
          -- WE HAVE THE HOUR THE TABLE WAS CLOSED                                                            
           SELECT   @WIN_LOSS_CURRENT_HOUR = DATEPART(HOUR, ISNULL(CLOSING_DATE, GETDATE()))
                  , @WIN_LOSS_CLOSING_HOUR = DATEPART(HOUR, CLOSING_DATE)
             FROM   #VALUE_TABLE
            WHERE   GAMING_TABLE_SESSION_ID = @GT_SESSION_ID
         GROUP BY   CLOSING_DATE
                                      
          SET @WIN_LOSS_FINAL_AMOUNT = 0
          SET @WIN_LOSS_CLOSING_TABLE_AMOUNT = (SELECT FINAL_BALANCE
                                                  FROM #VALUE_TABLE
                                                 WHERE GAMING_TABLE_SESSION_ID = @GT_SESSION_ID
                                              GROUP BY FINAL_BALANCE)                 
                                                          
          -- SECOND CURSOR: ITERANCE THE 24 HOURS OF THE PREVIOUS CURSOR SESSION
          WHILE(@@FETCH_STATUS = 0)
            BEGIN
              
              IF @WIN_LOSS_HOUR <> 9999
              BEGIN
                -- GET CURRENT WIN LOSS AMOUNT BY THE CURRENT TIME ITERANCE              
                SET @WIN_LOSS_CURRENT_AMOUNT = (SELECT ISNULL(GTWL_WIN_LOSS_AMOUNT, 0)
                                                  FROM GAMING_TABLES_WIN_LOSS
                                                 WHERE GTWL_GAMING_TABLE_SESSION_ID = @GT_SESSION_ID AND GTWL_DATETIME_HOUR = DATEADD(HOUR, @WIN_LOSS_HOUR, @pDateFrom))
                
                SET @CREDITS_SUB_FILLS = (SELECT SUM(CASE WHEN CM_CURRENCY_ISO_CODE <> @NationalCurrency THEN dbo.ApplyExchange2(CM_SUB_AMOUNT, CM_CURRENCY_ISO_CODE, @NationalCurrency) ELSE  CM_SUB_AMOUNT END -
                                                     CASE WHEN CM_CURRENCY_ISO_CODE <> @NationalCurrency THEN dbo.ApplyExchange2(CM_ADD_AMOUNT, CM_CURRENCY_ISO_CODE, @NationalCurrency) ELSE  CM_ADD_AMOUNT END)
                                            FROM CASHIER_MOVEMENTS
                                           WHERE CM_DATE BETWEEN DATEADD(HOUR, @WIN_LOSS_HOUR, @pDateFrom) AND DATEADD(HOUR, @WIN_LOSS_HOUR + 1, @pDateFrom)
                                             AND CM_TYPE IN (@pCageFillerIn, @pCageFillerOut)
                                             AND CM_CAGE_CURRENCY_TYPE = @pChipRE
                                             AND CM_SESSION_ID = (SELECT CASHIER_SESSION_ID
                                                                    FROM #VALUE_TABLE
                                                                   WHERE GAMING_TABLE_SESSION_ID = @GT_SESSION_ID
                                                                GROUP BY CASHIER_SESSION_ID))                                           
                
                IF DATEPART(HOUR, DATEADD(HOUR, @WIN_LOSS_HOUR,  @pDateFrom)) <= @WIN_LOSS_CURRENT_HOUR
                BEGIN
                UPDATE #VALUE_TABLE
                   SET GT_WIN_LOSS = ISNULL(@WIN_LOSS_CURRENT_AMOUNT, 0) - ISNULL(@WIN_LOSS_LAST_AMOUNT, 0) + ISNULL(@CREDITS_SUB_FILLS, 0) + GT_DROP, @DROP_CURRENT_AMOUNT = GT_DROP
                 WHERE GAMING_TABLE_SESSION_ID = @GT_SESSION_ID AND HORA = @WIN_LOSS_HOUR                              
                END                
                
                -- IF THE ITERATED TIME IS EQUAL TO THE CLOSURE OF THE GAMBLING TABLE WE SET THE VALUE OF WIN LOSS
                IF DATEPART(HOUR, DATEADD(HOUR, @WIN_LOSS_HOUR,  @pDateFrom)) = @WIN_LOSS_CLOSING_HOUR
                BEGIN
                  SET @WIN_LOSS_FINAL_AMOUNT = ISNULL(@WIN_LOSS_CURRENT_AMOUNT, 0) - ISNULL(@WIN_LOSS_LAST_AMOUNT, 0) + ISNULL(@CREDITS_SUB_FILLS, 0) + ISNULL(@WIN_LOSS_CLOSING_TABLE_AMOUNT, 0)
                END
                
                -- UPDATE WIN LOSS LAST AMOUNT WITH CURRENT WIN LOSS AMOUNT
                IF DATEADD(HOUR, @WIN_LOSS_HOUR,  @pDateFrom) <= GETDATE()
                BEGIN
                  SET @WIN_LOSS_LAST_AMOUNT = ISNULL(@WIN_LOSS_CURRENT_AMOUNT, 0)
                END
                ELSE
                BEGIN
                  SET @WIN_LOSS_LAST_AMOUNT = 0
                END
              END
                           
              FETCH NEXT FROM HOUR_WIN_LOSS_CURSOR INTO @WIN_LOSS_HOUR                         
            
            END       
            CLOSE HOUR_WIN_LOSS_CURSOR
            DEALLOCATE HOUR_WIN_LOSS_CURSOR
  
        FETCH NEXT FROM GT_SESSION_WIN_LOSS_CURSOR INTO @GT_SESSION_ID
      END
      CLOSE GT_SESSION_WIN_LOSS_CURSOR
      DEALLOCATE GT_SESSION_WIN_LOSS_CURSOR         

      UPDATE #VALUE_TABLE
          SET GT_WIN_LOSS = (SELECT ISNULL(SUM(CASE WHEN CM_TYPE = 191 THEN ISNULL(CM_SUB_AMOUNT, 0)
                                               WHEN CM_TYPE = 201 THEN ISNULL(CM_FINAL_BALANCE, 0)
                                               END ), 0)
                               FROM CASHIER_MOVEMENTS
                              WHERE CM_SESSION_ID = CASHIER_SESSION_ID
                                AND cm_type IN (191, 201)
                                AND cm_cage_currency_type = 1001)
        WHERE HORA = 9999

      --  SET HOLD BY HOUR    
      UPDATE   #VALUE_TABLE
         SET   GT_HOLD = A.GT_DROP / A.GT_WIN_LOSS * 100
        FROM   #VALUE_TABLE AS A
       WHERE   GAMING_TABLE_SESSION_ID = A.GAMING_TABLE_SESSION_ID
         AND   HORA = A.HORA
         AND   A.GT_WIN_LOSS <> 0
 
      --  SET OCCUPATION BY HOUR 
      UPDATE   #VALUE_TABLE
         SET   GT_OCCUPATION_NUMBER = (SELECT   CAST(COUNT(DISTINCT GTPS_SEAT_ID) AS DECIMAL(18,2))
                                  FROM   GT_PLAY_SESSIONS 
                                 WHERE   GAMING_TABLE_SESSION_ID = GTPS_GAMING_TABLE_SESSION_ID 
                                   AND ( GTPS_STARTED <= DATEADD(HOUR, HORA,  @pDateFrom) 
                                   AND   ISNULL(GTPS_FINISHED, GETDATE()) > DATEADD(HOUR, HORA,  @pDateFrom)
                                    OR   GTPS_STARTED >= DATEADD(HOUR, HORA,  @pDateFrom)
                                   AND   GTPS_STARTED < DATEADD(HOUR, 1, DATEADD(HOUR, HORA,  @pDateFrom))))
       WHERE  NUM_SEATS_IN_TABLE <> 0
 
      --  SET OCCUPATION BY HOUR -- TOTAL --
      UPDATE   #VALUE_TABLE
         SET   GT_OCCUPATION_NUMBER = ISNULL((SELECT   SUM(A.GT_OCCUPATION_NUMBER) / CAST(SUM(CASE WHEN A.GT_OCCUPATION_NUMBER > 0 THEN 1 ELSE 0 END)  AS DECIMAL(18,2))
                                  FROM   #VALUE_TABLE AS A
                                 WHERE   #VALUE_TABLE.GAMING_TABLE_SESSION_ID = A.GAMING_TABLE_SESSION_ID 
                                   AND   A.GT_OCCUPATION_NUMBER > 0), 0)
       WHERE  HORA = 9999
       
      -- SET HOUR = 0 WHEN WIN/LOSS IS NOT ENTER 
      DECLARE @Table_Name AS NVARCHAR(50)
      DECLARE @Hour       AS DATETIME
      
      DECLARE BLANK_HOUR_CURSOR CURSOR GLOBAL
        FOR SELECT   GT_NAME
                   , GTWL_DATETIME_HOUR
              FROM   GAMING_TABLES
        INNER JOIN   GAMING_TABLES_SESSIONS  ON GT_GAMING_TABLE_ID = GTS_GAMING_TABLE_ID
        INNER JOIN ( SELECT   GTWL_GAMING_TABLE_SESSION_ID, GTWL_DATETIME_HOUR, GTWL_WIN_LOSS_AMOUNT 
                       FROM   GAMING_TABLES_WIN_LOSS 
                      WHERE   GTWL_DATETIME_HOUR >= @pDateFrom
                        AND   GTWL_DATETIME_HOUR <= DATEADD(DAY, 1, @pDateFrom)
                      UNION
                     SELECT   GTWL_GAMING_TABLE_SESSION_ID, CONVERT(DATETIME, LEFT(CONVERT(NVARCHAR(20), GETDATE(), 120), 14)+'00:00', 120), NULL AS GTWL_WIN_LOSS_AMOUNT 
                       FROM   GAMING_TABLES_WIN_LOSS 
                      WHERE   GTWL_DATETIME_HOUR >= (SELECT MAX(GTWL_DATETIME_HOUR) FROM  GAMING_TABLES_WIN_LOSS WHERE   GTWL_DATETIME_HOUR >= @pDateFrom )
                        AND   GTWL_DATETIME_HOUR <= DATEADD(HOUR, -1, GETDATE())  
                    ) AS A    ON GTS_GAMING_TABLE_SESSION_ID = GTWL_GAMING_TABLE_SESSION_ID
          GROUP BY   GT_NAME, GTWL_DATETIME_HOUR
            HAVING   MAX(GTWL_WIN_LOSS_AMOUNT) IS NULL
                                                            
      OPEN BLANK_HOUR_CURSOR
      FETCH NEXT FROM BLANK_HOUR_CURSOR INTO @Table_Name, @Hour     
      WHILE(@@FETCH_STATUS = 0)
        BEGIN
          UPDATE   #VALUE_TABLE
             SET   GT_DROP = 0
                 , GT_WIN_LOSS = 0
                 , GT_HOLD = 0
                 , GT_OCCUPATION_NUMBER = 0
           WHERE   TABLE_NAME = @Table_Name
             AND   DATEADD(HOUR, HORA,  @pDateFrom) = @Hour
        
          FETCH NEXT FROM BLANK_HOUR_CURSOR INTO @Table_Name, @Hour                          
        END       
      CLOSE BLANK_HOUR_CURSOR
      DEALLOCATE BLANK_HOUR_CURSOR           
            
            
      --PIVOT COLUMS FOR REPORT
      SELECT   @cols = STUFF((SELECT ',' + QUOTENAME( CASE WHEN HORA < 25 
                                                           THEN REPLACE(LEFT(RIGHT(CONVERT(NVARCHAR(20), DATEADD(HOUR, HORA, @pDateFrom), 120), 8), 5), ':', '') 
                                                           ELSE '9999'
                                                           END   +'-'+COL) 
        FROM   #VALUE_TABLE t
 CROSS APPLY ( SELECT 'GT_DROP', 1 UNION ALL
               SELECT 'GT_WIN_LOSS', 2 UNION ALL
               SELECT 'GT_HOLD', 3 UNION ALL
               SELECT 'GT_OCCUPATION_NUMBER', 4
             ) C (COL, SO)
    GROUP BY   COL
             , SO
             , HORA
    ORDER BY   HORA
             , SO FOR XML PATH('')
             , TYPE ).value('.', 'NVARCHAR(MAX)'),1,1,'')


set @query = 'SELECT   TYPE_NAME
                     , TABLE_NAME
                     , OPENING_DATE
                     , CLOSING_DATE
                     , GAMING_TABLE_SESSION_ID
                     , SESION_NAME
                     , INITIAL_BALANCE
                     , FINAL_BALANCE
                     , NUM_SEATS_IN_TABLE
                     , ' + @cols + ' 
                FROM ( SELECT   TYPE_NAME
                              , TABLE_NAME
                              , OPENING_DATE
                              , CLOSING_DATE
                              , GAMING_TABLE_SESSION_ID
                              , SESION_NAME
                              , INITIAL_BALANCE
                              , FINAL_BALANCE
                              , NUM_SEATS_IN_TABLE
                              , COL = CASE WHEN HORA < 25 
                                           THEN REPLACE( LEFT(  RIGHT(CONVERT(NVARCHAR(20), DATEADD(HOUR, HORA, ''' + CONVERT(NVARCHAR(20), @pDateFrom, 120) + '''), 120), 8), 5), '':'', '''' )
                                           ELSE ''9999'' 
                                           END  +''-''+COL
                              , VALUE
                         FROM   #VALUE_TABLE T
                  CROSS APPLY ( SELECT   ''GT_DROP''
                                       , GT_DROP UNION ALL
                                SELECT   ''GT_WIN_LOSS''
                                       , GT_WIN_LOSS UNION ALL
                                SELECT   ''GT_HOLD''
                                       , GT_HOLD UNION ALL
                                SELECT   ''GT_OCCUPATION_NUMBER''
                                       , GT_OCCUPATION_NUMBER
                              ) C (COL, VALUE)
                     ) X
               PIVOT ( MAX(VALUE) FOR COL IN (' + @cols + ') ) PVT '


  EXECUTE SP_EXECUTESQL @query

  DROP TABLE #GROUPS_BY_SESSION
  
END --ScoreReport
GO

GRANT EXECUTE ON [dbo].[ScoreReport] TO [wggui] WITH GRANT OPTION
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Update_Meters_SalesPerHour]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Update_Meters_SalesPerHour]
GO

CREATE PROCEDURE [dbo].[Update_Meters_SalesPerHour]
      @pBaseHour                  DATETIME 
,     @pTerminalId                INT
,     @pGameId                    INT
,     @pTerminalDefaultPayout     DECIMAL
,     @pSPH_Played_Count          BIGINT
,     @pSPH_Played_Amount         MONEY
,     @pSPH_Won_Count             BIGINT
,     @pSPH_Won_Amount            MONEY
,     @pSPH_Jackpot_Amount        MONEY

AS
BEGIN 

  DECLARE @pTerminalName AS NVARCHAR(50)
  DECLARE @pGameName AS NVARCHAR(50)
  DECLARE @payout AS MONEY 

  IF @pGameId = 0
  BEGIN
    -- Get game id from sales per hour
    SELECT   @pGameId = ISNULL(MIN(SPH_GAME_ID), 0)
      FROM   SALES_PER_HOUR
     WHERE   SPH_BASE_HOUR >= dbo.Opening(0, @pBaseHour)
       AND   SPH_BASE_HOUR < DATEADD(DAY, 1, dbo.Opening(0, @pBaseHour))
       AND   SPH_TERMINAL_ID = @pTerminalId
     
    IF @pGameId = 0
    BEGIN
      -- Get game id from terminals
      SELECT   @pGameId = ISNULL(TE_LAST_GAME_PLAYED_ID, 0)
        FROM   TERMINALS
       WHERE   TE_TERMINAL_ID = @pTerminalId
    END
    
    IF @pGameId = 0
    BEGIN
      -- Get game id from terminal game translation
      SELECT   @pGameId = ISNULL(TGT_SOURCE_GAME_ID, 0)
        FROM   TERMINAL_GAME_TRANSLATION
       WHERE   TGT_TERMINAL_ID = @pTerminalId
    END
  END
  
  IF @pGameId = 0
  BEGIN
    RAISERROR (N'Terminal without activity can not be adjusted', 16, 10)
    RETURN
  END

  -- Get terminal name 
  SELECT   @pTerminalName = TE_NAME
         , @payout = ISNULL(TE_THEORETICAL_PAYOUT, @pTerminalDefaultPayout) 
    FROM   TERMINALS
   WHERE   TE_TERMINAL_ID = @pTerminalId

  IF NOT EXISTS ( SELECT   *
                    FROM   SALES_PER_HOUR
                   WHERE   SPH_BASE_HOUR = @pBaseHour
                     AND   SPH_TERMINAL_ID = @pTerminalId
                     AND   SPH_GAME_ID = @pGameId)
  BEGIN

    -- Get game name
    SELECT   @pGameName = GM_NAME
      FROM   GAMES
     WHERE   GM_GAME_ID = @pGameId   
     
    INSERT INTO SALES_PER_HOUR
               ( SPH_BASE_HOUR
               , SPH_TERMINAL_ID
               , SPH_TERMINAL_NAME
               , SPH_GAME_ID
               , SPH_GAME_NAME
               , SPH_PLAYED_COUNT
               , SPH_PLAYED_AMOUNT
               , SPH_WON_COUNT
               , SPH_WON_AMOUNT
               , SPH_NUM_ACTIVE_TERMINALS
               , SPH_LAST_PLAY_ID
               , SPH_THEORETICAL_WON_AMOUNT
               , SPH_JACKPOT_AMOUNT
               , SPH_PROGRESSIVE_JACKPOT_AMOUNT
               , SPH_PROGRESSIVE_JACKPOT_AMOUNT_0
               , SPH_PROGRESSIVE_PROVISION_AMOUNT)
         VALUES
               ( @pBaseHour
               , @pTerminalId
               , @pTerminalName
               , @pGameId
               , @pGameName
               , ISNULL(@pSPH_Played_Count, 0)
               , ISNULL(@pSPH_Played_Amount, 0)
               , ISNULL(@pSPH_Won_Count, 0)
               , ISNULL(@pSPH_Won_Amount, 0)
               , 0
               , 0
               , ISNULL(@pSPH_Played_Amount, 0) * @payout
               , ISNULL(@pSPH_Jackpot_Amount, 0)
               , 0
               , 0
               , 0)

  END
  ELSE
  BEGIN

    UPDATE   SALES_PER_HOUR
       SET   SPH_PLAYED_COUNT   = ISNULL(@pSPH_Played_Count, SPH_PLAYED_COUNT)
           , SPH_PLAYED_AMOUNT  = ISNULL(@pSPH_Played_Amount, SPH_PLAYED_AMOUNT)
           , SPH_WON_COUNT      = ISNULL(@pSPH_Won_Count, SPH_WON_COUNT)
           , SPH_WON_AMOUNT     = ISNULL(@pSPH_Won_Amount, SPH_WON_AMOUNT)
           , SPH_JACKPOT_AMOUNT = ISNULL(@pSPH_Jackpot_Amount, SPH_JACKPOT_AMOUNT)
           , SPH_THEORETICAL_WON_AMOUNT = CASE WHEN ISNULL(SPH_PLAYED_AMOUNT, 0) = 0
                                               THEN ISNULL(@pSPH_Played_Amount, SPH_PLAYED_AMOUNT) * @payout
                                               ELSE ISNULL(@pSPH_Played_Amount, SPH_PLAYED_AMOUNT) * (SPH_THEORETICAL_WON_AMOUNT / SPH_PLAYED_AMOUNT)
                                                END
     WHERE   SPH_BASE_HOUR = @pBaseHour
       AND   SPH_TERMINAL_ID = @pTerminalId
       AND   SPH_GAME_ID = @pGameId

  END
            
END 
GO

GRANT EXECUTE ON [dbo].[Update_Meters_SalesPerHour] TO [wggui] WITH GRANT OPTION
GO

---- 000 
-------------------------------------------
-- Drop and create table ErrorHandling
-------------------------------------------

IF NOT EXISTS (SELECT * FROM sys.objects WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[ErrorHandling]') AND TYPE IN (N'U'))
	CREATE TABLE [dbo].[ErrorHandling](
		[pkErrorHandlingID] [INT] 			IDENTITY(1,1) 		NOT NULL,
		[Error_Number] 		[INT] 								NOT NULL,
		[Error_Message] 	[VARCHAR](4000) 						NULL,
		[Error_Severity] 	[SMALLINT] 							NOT NULL,
		[Error_State] 		[SMALLINT] 							NOT NULL	DEFAULT 1,
		[Error_Procedure] 	[VARCHAR](200) 						NOT NULL,
		[Error_Line] 		[INT] 								NOT NULL	DEFAULT 0,
		[UserName] 			[VARCHAR](128) 						NOT NULL 	DEFAULT '',
		[HostName] 			[VARCHAR](128) 						NOT NULL 	DEFAULT '',
		[Time_Stamp] 		[DATETIME] 							NOT NULL,
	PRIMARY KEY CLUSTERED 
	(
		[pkErrorHandlingID] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]
	GO
	
	----000
	/*
----------------------------------------------------------------------------------------------------------------
Stored Procedure: 'spErrorHandling'.
Is implemented to detect last error that has occurred and insert related information into ErrorHandling table.

Version     Date          User      Description
----------------------------------------------------------------------------------------------------------------
1.0.0       02-FEB-2018   OMC       New procedure

Requeriments:

Parameters:

*/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spErrorHandling]') AND TYPE IN (N'P'))
	DROP PROCEDURE [dbo].[spErrorHandling]
GO

CREATE PROCEDURE [dbo].[spErrorHandling] 
AS 
	-- Declaration statements
	DECLARE @Error_Number 		INT
	DECLARE @Error_Message 		VARCHAR(4000)
	DECLARE @Error_Severity 	INT
	DECLARE @Error_State 		INT
	DECLARE @Error_Procedure 	VARCHAR(200)
	DECLARE @Error_Line 		INT
	DECLARE @UserName 			VARCHAR(200)
	DECLARE @HostName 			VARCHAR(200)
	DECLARE @Time_Stamp 		DATETIME

-- Initialize variables
SELECT
	@Error_Number 		= ISNULL(ERROR_NUMBER(), 0)
   ,@Error_Message 		= ISNULL(ERROR_MESSAGE(), 'NULL Message')
   ,@Error_Severity 	= ISNULL(ERROR_SEVERITY(), 0)
   ,@Error_State 		= ISNULL(ERROR_STATE(), 1)
   ,@Error_Line 		= ISNULL(ERROR_LINE(), 0)
   ,@Error_Procedure 	= ISNULL(ERROR_PROCEDURE(), '')
   ,@UserName 			= SUSER_SNAME()
   ,@HostName 			= HOST_NAME()
   ,@Time_Stamp 		= GETDATE();

-- Insert into the dbo.ErrorHandling table (protecting if table is in the database to avoid errors).
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE = 'BASE TABLE' AND TABLE_NAME = 'ErrorHandling')
	INSERT INTO dbo.ErrorHandling (Error_Number, Error_Message, Error_Severity, Error_State, Error_Line, Error_Procedure, UserName, HostName, Time_Stamp)
		SELECT
			@Error_Number
		   ,@Error_Message
		   ,@Error_Severity
		   ,@Error_State
		   ,@Error_Line
		   ,@Error_Procedure
		   ,@UserName
		   ,@HostName
		   ,@Time_Stamp
GO

---- 001

/*
----------------------------------------------------------------------------------------------------------------
Stored Procedure: 'EnableTriggerByNameAndTable'.
Is implemented to Enable or Disable a Trigger based on received parameters.

Version     Date          User      Description
----------------------------------------------------------------------------------------------------------------
1.0.0       08-FEB-2018   OMC       New procedure

Requeriments:

Parameters:
   @TableName: Table Affected Name.
 , @ProcName: Trigger to be Enabled/Disabled Name.
 , @Enable: Bit to set Enabled/Disabled The Trigger.
*/
/* Stored Procedure EnableTriggerByNameAndTable */
IF  EXISTS ( SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EnableTriggerByNameAndTable]') AND type IN (N'P', N'PC'))
	DROP PROCEDURE [dbo].[EnableTriggerByNameAndTable]
GO

CREATE PROCEDURE [dbo].[EnableTriggerByNameAndTable] 
   @TableName AS VARCHAR(150)
 , @ProcName AS VARCHAR(200)
 , @Enable AS BIT
AS
	BEGIN
		BEGIN TRY
			SET NOCOUNT ON;
			DECLARE @sql NVARCHAR(MAX);
			IF @Enable = 1 
				BEGIN
					SET @sql = 'ENABLE  TRIGGER ' + @ProcName + '    ON ' + @TableName + ';'
				END
			ELSE 
				BEGIN
					SET @sql = 'DISABLE  TRIGGER ' + @ProcName + '    ON ' + @TableName + ';'
				END
			IF EXISTS (SELECT * FROM sys.objects WHERE type = 'TR' AND name = @ProcName)
			EXEC sp_executesql @sql;
		END TRY
		BEGIN CATCH
			EXEC dbo.spErrorHandling
		END CATCH
	END
GO


--- 002
/*
----------------------------------------------------------------------------------------------------------------
Stored Procedure: 'EnableTriggers_By_Service'.
Is implemented to set Enabled or Disabled the group of Triggers linked to the service name passed.

Current Supported Values for @ServiceName: 
	- InHouseAPI.
	- AGG.
	- The rest of values are controlled to print advertisement with a 'not found message'.

Version     Date          User      Description
----------------------------------------------------------------------------------------------------------------
1.0.0       08-FEB-2018   OMC       New procedure

Requeriments:

Parameters:
   @ServiceName: Service to affect its own triggers.
*/
/* Stored Procedure EnableTriggers_By_Service */
IF  EXISTS ( SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EnableTriggers_By_Service]') AND type IN (N'P', N'PC'))
	DROP PROCEDURE [dbo].[EnableTriggers_By_Service]
GO

CREATE PROCEDURE [dbo].[EnableTriggers_By_Service] 
 @ServiceName VARCHAR(100)
AS
	BEGIN
	DECLARE @trigger_status_text VARCHAR(100);
	DECLARE @service_exists BIT = 0;
		BEGIN TRY
			SET NOCOUNT ON;
			/* InHouseAPI */
			IF(@ServiceName = 'InHouseAPI')
				BEGIN
					DECLARE @INHOUSEAPI BIT = ISNULL(( SELECT gp_key_value FROM general_params WHERE gp_group_key = 'InHouseAPI' AND gp_subject_key = 'Enabled'), 0);
					SET @service_exists = 1;
					EXEC EnableTriggerByNameAndTable @TableName = N'PLAY_SESSIONS', @ProcName = N'InHouseAPI_Play_Sessions_Insert', @Enable = @INHOUSEAPI
					EXEC EnableTriggerByNameAndTable @TableName = N'CUSTOMER_VISITS', @ProcName = N'InHouseAPI_Customer_Visits_Insert', @Enable = @INHOUSEAPI
					/* Action Performed Message  */
					IF @INHOUSEAPI = 1
						SET @trigger_status_text = ' :: Triggers Enabled';
					ELSE
						SET @trigger_status_text = ' :: Triggers Disabled';
				END
			/* AGG */
			/*ELSE IF(@ServiceName = 'AGG')
				BEGIN
					DECLARE @AGG BIT = ISNULL((SELECT gp_key_value FROM general_params WHERE gp_group_key = 'AGG' AND gp_subject_key = 'Buffering.Enabled'), 0);
					SET @service_exists = 1;
					EXEC EnableTriggerByNameAndTable @TableName = N'ACCOUNTS', @ProcName = N'MultiSiteTrigger_SiteAccountUpdate', @Enable = @AGG
					EXEC EnableTriggerByNameAndTable @TableName = N'ACCOUNT_MOVEMENTS', @ProcName = N'Trigger_SiteToMultiSite_Points', @Enable = @AGG
					EXEC EnableTriggerByNameAndTable @TableName = N'ACCOUNT_DOCUMENTS', @ProcName = N'MultiSiteTrigger_SiteAccountDocuments', @Enable = @AGG
					/* Action Performed Message  */
					IF @AGG = 1
						SET @trigger_status_text = ' :: Triggers Enabled';
					ELSE
						SET @trigger_status_text = ' :: Triggers Disabled';
				END*/
			/* Future Services Added Section ...*/
			/*
			----------------------------------------------------------------------------------------
			ELSE IF(@ServiceName = 'XXX')
				BEGIN
					IF @XXX = 1
						SET @trigger_status_text = ' :: Triggers Enabled';
					ELSE
						SET @trigger_status_text = ' :: Triggers Disabled';				
				END
			----------------------------------------------------------------------------------------
			*/
			IF (@service_exists = 1)
				SELECT @ServiceName + @trigger_status_text;
			ELSE
				SELECT 'ServiceName "' + @ServiceName + '" not found!';
		END TRY
		BEGIN CATCH
			EXEC dbo.spErrorHandling
		END CATCH
	END
GO


---- 999
/*
----------------------------------------------------------------------------------------------------------------
Call to Set Enabled or Disabled the group of Triggers linked to the service name passed.

Current Supported Values for @ServiceName: 
	- InHouseAPI.
	- AGG.
	- The rest of values are controlled to print advertisement with a 'not found message'.
	


Version     Date          User      Description
----------------------------------------------------------------------------------------------------------------
1.0.0       08-FEB-2018   OMC       Help Call.

Requeriments:

Parameters:
   @ServiceName: Service to affect its own triggers.
*/
/* Stored Procedure Call EnableTriggers_By_Service */
EXEC	[dbo].[EnableTriggers_By_Service] @ServiceName = N'InHouseAPI'


/****** Object:  StoredProcedure [dbo].[ReportCashDeskDraws]    Script Date: 02/14/2018 14:34:11 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ReportCashDeskDraws]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ReportCashDeskDraws]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[ReportCashDeskDraws]  
  @pDrawFrom	    DATETIME      = NULL,
  @pDrawTo		    DATETIME      = NULL,
  @pDrawId		    BIGINT		  =	NULL,
  @pCashierId       INT			  = NULL,
  @pUserId          INT			  = NULL,
  @pSqlAccount      NVARCHAR(MAX) = NULL,
  @pCashierMovement INT	          = NULL,
  @pCashDeskDraw    INT           = 1,
  @pGamingTableDraw INT           = 1,
  @pLooserPrizeId   INT           = 5

AS
BEGIN

	SET NOCOUNT ON;
	DECLARE @_Terminal_CashDesk_draw_type AS INT;
	DECLARE @_Terminal_GamingTable_draw_type AS INT;
	DECLARE @_Sorter_terminal_ID AS INT;
	DECLARE @_Promotion_Redeemeable AS INT;
	DECLARE @_Promotion_NoRedeemeable AS INT;
	
	SET @_Terminal_CashDesk_draw_type = 106
	SET @_Terminal_GamingTable_draw_type = 111
	SET @_Sorter_terminal_ID = (SELECT  TOP 1 te_terminal_id FROM terminals WHERE te_terminal_type = @_Terminal_CashDesk_draw_type)
	SET @_Promotion_Redeemeable = 14
	SET @_Promotion_NoRedeemeable = 13

    CREATE TABLE #ACCOUNTS_TEMP (AC_ACCOUNT_ID BIGINT NOT NULL, AC_HOLDER_NAME NVARCHAR(200), AC_TRACK_DATA NVARCHAR(50))
	IF @pSqlAccount IS NOT NULL
	BEGIN
	  INSERT INTO #ACCOUNTS_TEMP EXEC ('SELECT AC_ACCOUNT_ID, AC_HOLDER_NAME, AC_TRACK_DATA  FROM ACCOUNTS ' + @pSqlAccount)
		IF @@ROWCOUNT > 500
			ALTER TABLE #ACCOUNTS_TEMP ADD PRIMARY KEY NONCLUSTERED (AC_ACCOUNT_ID)
	END

	IF 	@pDrawId IS NOT NULL 
	--
	-- Filtering by Draw ID
	--
	BEGIN	
	  IF @pDrawFrom IS NULL SET @pDrawFrom = '2007-01-01T00:00:00'
		IF @pDrawTo   IS NULL SET @pDrawTo   = GETDATE ()
		
    --No filter by draw type
	  IF (@pCashDeskDraw = 1 AND @pGamingTableDraw = 1) OR (@pCashDeskDraw = 0 AND @pGamingTableDraw = 0)
	  BEGIN
		    SELECT DISTINCT
		        CD_DRAW_ID
		      , CD_DRAW_DATETIME
		      , TE_TERMINAL_TYPE
		      , CM_CASHIER_NAME		  
		      , CM_USER_NAME
		      , CD_OPERATION_ID		
		      , CM_DATE
		      , CD_ACCOUNT_ID
		      , AC_HOLDER_NAME
		      , CD_COMBINATION_BET
		      , CD_COMBINATION_WON
		      , CD_RE_BET
          , CASE WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_CashDesk_draw_type THEN AC.AO_PROMO_REDEEMABLE 
                 WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_GamingTable_draw_type AND AP.ACP_PROMO_TYPE = @_Promotion_Redeemeable THEN AP.ACP_BALANCE
                 WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_GamingTable_draw_type AND AP.ACP_PROMO_TYPE = @_Promotion_NoRedeemeable THEN 0
                 ELSE CD_RE_WON END CD_RE_WON
		      , CASE WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_CashDesk_draw_type  THEN AC.AO_PROMO_NOT_REDEEMABLE 
		             WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_GamingTable_draw_type AND AP.ACP_PROMO_TYPE = @_Promotion_Redeemeable THEN 0
		             WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_GamingTable_draw_type AND AP.ACP_PROMO_TYPE = @_Promotion_NoRedeemeable THEN AP.ACP_BALANCE
		        ELSE CD_NR_WON END CD_NR_WON
		      , CD_PRIZE_ID
		    FROM
		       CASHDESK_DRAWS
		       WITH(INDEX(IX_CD_DRAW_DATETIME_ACCOUNT))
		        INNER JOIN CASHIER_MOVEMENTS WITH(INDEX(IX_CM_OPERATION_ID)) ON CD_OPERATION_ID = CM_OPERATION_ID
		        INNER JOIN ACCOUNTS          ON CD_ACCOUNT_ID   = AC_ACCOUNT_ID
		        INNER JOIN TERMINALS   ON (CASE WHEN CD_TERMINAL IS NOT NULL THEN CD_TERMINAL ELSE @_Sorter_terminal_ID END) = TE_TERMINAL_ID
		        LEFT JOIN ACCOUNT_OPERATIONS AC ON AC.AO_OPERATION_ID = CD_OPERATION_ID
		        LEFT  JOIN ACCOUNT_PROMOTIONS AP ON AP.ACP_OPERATION_ID = CD_OPERATION_ID AND AP.ACP_PROMO_TYPE IN (@_Promotion_Redeemeable, @_Promotion_NoRedeemeable)
		        		        
		    WHERE    
		        CD_DRAW_ID = @pDrawId	
		        AND CD_DRAW_DATETIME >= @pDrawFrom
		        AND CD_DRAW_DATETIME < @pDrawTo	
		        AND ((@pCashierId IS NULL) OR (CM_CASHIER_ID = @pCashierId))
		        AND ((@pUserId IS NULL) OR (CM_USER_ID = @pUserId))
		        AND ( (@pSqlAccount     IS NULL) OR (CD_ACCOUNT_ID IN ( SELECT AC_ACCOUNT_ID FROM #ACCOUNTS_TEMP ) ) )
		        AND ( CM_TYPE = @pCashierMovement )
		        AND    te_terminal_type IN (@_Terminal_CashDesk_draw_type, @_Terminal_GamingTable_draw_type)
		        AND AC.ao_undo_status IS NULL
		    ORDER BY CD_DRAW_ID
		    DROP TABLE #ACCOUNTS_TEMP

		    RETURN
		END
    --Filter by CASH DESK draw
	  ELSE IF  @pCashDeskDraw = 1 AND @pGamingTableDraw = 0
	  BEGIN
		    SELECT DISTINCT
		        CD_DRAW_ID
		      , CD_DRAW_DATETIME
		      , TE_TERMINAL_TYPE
		      , CM_CASHIER_NAME		  
		      , CM_USER_NAME
		      , CD_OPERATION_ID		
		      , CM_DATE
		      , CD_ACCOUNT_ID
		      , AC_HOLDER_NAME
		      , CD_COMBINATION_BET
		      , CD_COMBINATION_WON
		      , CD_RE_BET
          , CASE WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_CashDesk_draw_type THEN AC.AO_PROMO_REDEEMABLE 
                 WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_GamingTable_draw_type AND AP.ACP_PROMO_TYPE = @_Promotion_Redeemeable THEN AP.ACP_BALANCE
                 WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_GamingTable_draw_type AND AP.ACP_PROMO_TYPE = @_Promotion_NoRedeemeable THEN 0
                 ELSE CD_RE_WON END CD_RE_WON
		      , CASE WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_CashDesk_draw_type  THEN AC.AO_PROMO_NOT_REDEEMABLE 
		             WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_GamingTable_draw_type AND AP.ACP_PROMO_TYPE = @_Promotion_Redeemeable THEN 0
		             WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_GamingTable_draw_type AND AP.ACP_PROMO_TYPE = @_Promotion_NoRedeemeable THEN AP.ACP_BALANCE
		             ELSE CD_NR_WON END CD_NR_WON
		      , CD_PRIZE_ID
		    FROM
		       CASHDESK_DRAWS
		       WITH(INDEX(IX_CD_DRAW_DATETIME_ACCOUNT))
		        INNER JOIN CASHIER_MOVEMENTS WITH(INDEX(IX_CM_OPERATION_ID)) ON CD_OPERATION_ID = CM_OPERATION_ID  
		        INNER JOIN ACCOUNTS   ON CD_ACCOUNT_ID   = AC_ACCOUNT_ID
		        INNER JOIN TERMINALS   ON (CASE WHEN CD_TERMINAL IS NOT NULL THEN CD_TERMINAL ELSE @_Sorter_terminal_ID END) = TE_TERMINAL_ID
		        LEFT JOIN ACCOUNT_OPERATIONS AC ON AC.AO_OPERATION_ID = CD_OPERATION_ID 
		        LEFT  JOIN ACCOUNT_PROMOTIONS AP ON AP.ACP_OPERATION_ID = CD_OPERATION_ID AND AP.ACP_PROMO_TYPE IN (@_Promotion_Redeemeable, @_Promotion_NoRedeemeable)
		        
		    WHERE    
		        CD_DRAW_ID = @pDrawId	
		        AND CD_DRAW_DATETIME >= @pDrawFrom
		        AND CD_DRAW_DATETIME < @pDrawTo	
		        AND ((@pCashierId IS NULL) OR (CM_CASHIER_ID = @pCashierId))
		        AND ((@pUserId IS NULL) OR (CM_USER_ID = @pUserId))
		        AND ( (@pSqlAccount     IS NULL) OR (CD_ACCOUNT_ID IN ( SELECT AC_ACCOUNT_ID FROM #ACCOUNTS_TEMP ) ) )
		        AND ( CM_TYPE = @pCashierMovement )
		        AND te_terminal_type = @_Terminal_CashDesk_draw_type
		        AND AC.ao_undo_status IS NULL
		    ORDER BY CD_DRAW_ID
		    DROP TABLE #ACCOUNTS_TEMP

		    RETURN		    	  
	  END
--Filter by GAMING TABLE draw
	  ELSE IF  @pCashDeskDraw = 0 AND @pGamingTableDraw = 1
	  BEGIN
		    SELECT DISTINCT
		        CD_DRAW_ID
		      , CD_DRAW_DATETIME
		      , TE_TERMINAL_TYPE
		      , CM_CASHIER_NAME		  
		      , CM_USER_NAME
		      , CD_OPERATION_ID		
		      , CM_DATE
		      , CD_ACCOUNT_ID
		      , AC_HOLDER_NAME
		      , CD_COMBINATION_BET
		      , CD_COMBINATION_WON
		      , CD_RE_BET
          , CASE WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_CashDesk_draw_type THEN AC.AO_PROMO_REDEEMABLE 
                 WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_GamingTable_draw_type AND AP.ACP_PROMO_TYPE = @_Promotion_Redeemeable THEN AP.ACP_BALANCE
                 WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_GamingTable_draw_type AND AP.ACP_PROMO_TYPE = @_Promotion_NoRedeemeable THEN 0
                 ELSE CD_RE_WON END CD_RE_WON
		      , CASE WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_CashDesk_draw_type  THEN AC.AO_PROMO_NOT_REDEEMABLE 
		             WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_GamingTable_draw_type AND AP.ACP_PROMO_TYPE = @_Promotion_Redeemeable THEN 0
		             WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_GamingTable_draw_type AND AP.ACP_PROMO_TYPE = @_Promotion_NoRedeemeable THEN AP.ACP_BALANCE
		             ELSE CD_NR_WON END CD_NR_WON
		      , CD_PRIZE_ID
		    FROM
		       CASHDESK_DRAWS
		       WITH(INDEX(IX_CD_DRAW_DATETIME_ACCOUNT))
		        INNER JOIN CASHIER_MOVEMENTS WITH(INDEX(IX_CM_OPERATION_ID)) ON CD_OPERATION_ID = CM_OPERATION_ID  
		        INNER JOIN ACCOUNTS   ON CD_ACCOUNT_ID   = AC_ACCOUNT_ID
		        INNER JOIN TERMINALS   ON (CASE WHEN CD_TERMINAL IS NOT NULL THEN CD_TERMINAL ELSE @_Sorter_terminal_ID END) = TE_TERMINAL_ID
		        LEFT JOIN ACCOUNT_OPERATIONS AC ON AC.AO_OPERATION_ID = CD_OPERATION_ID
		        LEFT JOIN ACCOUNT_PROMOTIONS AP ON AP.ACP_OPERATION_ID = CD_OPERATION_ID AND AP.ACP_PROMO_TYPE IN (@_Promotion_Redeemeable, @_Promotion_NoRedeemeable)
		        
		    WHERE    
		        CD_DRAW_ID = @pDrawId	
		        AND CD_DRAW_DATETIME >= @pDrawFrom
		        AND CD_DRAW_DATETIME < @pDrawTo	
		        AND ((@pCashierId IS NULL) OR (CM_CASHIER_ID = @pCashierId))
		        AND ((@pUserId IS NULL) OR (CM_USER_ID = @pUserId))
		        AND ( (@pSqlAccount     IS NULL) OR (CD_ACCOUNT_ID IN ( SELECT AC_ACCOUNT_ID FROM #ACCOUNTS_TEMP ) ) )
		        AND ( CM_TYPE = @pCashierMovement )
		        AND te_terminal_type = @_Terminal_GamingTable_draw_type
		        AND AC.ao_undo_status IS NULL
		    ORDER BY CD_DRAW_ID
		    DROP TABLE #ACCOUNTS_TEMP

		    RETURN		    	  
	  END	  
		
	END 	

	
	IF 	@pSqlAccount IS NOT NULL 
	--
	-- Filtering by account, but not by draw ID
	--
	BEGIN				
		IF @pDrawFrom IS NULL SET @pDrawFrom = '2007-01-01T00:00:00'
		IF @pDrawTo   IS NULL SET @pDrawTo   = GETDATE ()

	  --No filter by draw type
	  IF (@pCashDeskDraw = 1 AND @pGamingTableDraw = 1) OR (@pCashDeskDraw = 0 AND @pGamingTableDraw = 0)
	  BEGIN
		    SELECT DISTINCT
		        CD_DRAW_ID
		      , CD_DRAW_DATETIME
		      , TE_TERMINAL_TYPE
		      , CM_CASHIER_NAME		  
		      , CM_USER_NAME
		      , CD_OPERATION_ID		
		      , CM_DATE
		      , CD_ACCOUNT_ID
		      , AC_HOLDER_NAME
		      , CD_COMBINATION_BET
		      , CD_COMBINATION_WON
		      , CD_RE_BET
          , CASE WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_CashDesk_draw_type THEN AC.AO_PROMO_REDEEMABLE 
                 WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_GamingTable_draw_type AND AP.ACP_PROMO_TYPE = @_Promotion_Redeemeable THEN AP.ACP_BALANCE
                 WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_GamingTable_draw_type AND AP.ACP_PROMO_TYPE = @_Promotion_NoRedeemeable THEN 0
                 ELSE CD_RE_WON END CD_RE_WON
		      , CASE WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_CashDesk_draw_type  THEN AC.AO_PROMO_NOT_REDEEMABLE 
		             WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_GamingTable_draw_type AND AP.ACP_PROMO_TYPE = @_Promotion_Redeemeable THEN 0
		             WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_GamingTable_draw_type AND AP.ACP_PROMO_TYPE = @_Promotion_NoRedeemeable THEN AP.ACP_BALANCE
		             ELSE CD_NR_WON END CD_NR_WON
		      , CD_PRIZE_ID
		    FROM
		       CASHDESK_DRAWS
		       WITH(INDEX(IX_CD_ACCOUNT_DATETIME))
		        INNER JOIN CASHIER_MOVEMENTS WITH(INDEX(IX_CM_OPERATION_ID)) ON CD_OPERATION_ID = CM_OPERATION_ID 
		        INNER JOIN #ACCOUNTS_TEMP    ON CD_ACCOUNT_ID   = AC_ACCOUNT_ID
		        INNER JOIN TERMINALS   ON (CASE WHEN CD_TERMINAL IS NOT NULL THEN CD_TERMINAL ELSE @_Sorter_terminal_ID END) = TE_TERMINAL_ID
		        LEFT JOIN ACCOUNT_OPERATIONS AC ON AC.AO_OPERATION_ID = CD_OPERATION_ID
		        LEFT JOIN ACCOUNT_PROMOTIONS AP ON AP.ACP_OPERATION_ID = CD_OPERATION_ID AND AP.ACP_PROMO_TYPE IN (@_Promotion_Redeemeable, @_Promotion_NoRedeemeable)
		        
		    WHERE    
			    CD_DRAW_DATETIME >= @pDrawFrom
		       AND CD_DRAW_DATETIME < @pDrawTo 		   
		       AND ((@pCashierId IS NULL) OR (CM_CASHIER_ID = @pCashierId))
		       AND ((@pUserId IS NULL) OR (CM_USER_ID = @pUserId))
		       AND ( CM_TYPE = @pCashierMovement ) 
		       AND    te_terminal_type IN (@_Terminal_CashDesk_draw_type, @_Terminal_GamingTable_draw_type)
		       AND AC.ao_undo_status IS NULL
		    ORDER BY CD_DRAW_ID
		    DROP TABLE #ACCOUNTS_TEMP

		    RETURN
		END
	  --Filter by CASH DESK draw
	  ELSE IF  @pCashDeskDraw = 1 AND @pGamingTableDraw = 0
	  BEGIN
          SELECT DISTINCT
		        CD_DRAW_ID
		      , CD_DRAW_DATETIME
		      , TE_TERMINAL_TYPE
		      , CM_CASHIER_NAME		  
		      , CM_USER_NAME
		      , CD_OPERATION_ID		
		      , CM_DATE
		      , CD_ACCOUNT_ID
		      , AC_HOLDER_NAME
		      , CD_COMBINATION_BET
		      , CD_COMBINATION_WON
		      , CD_RE_BET
          , CASE WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_CashDesk_draw_type THEN AC.AO_PROMO_REDEEMABLE 
                 WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_GamingTable_draw_type AND AP.ACP_PROMO_TYPE = @_Promotion_Redeemeable THEN AP.ACP_BALANCE
                 WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_GamingTable_draw_type AND AP.ACP_PROMO_TYPE = @_Promotion_NoRedeemeable THEN 0
                 ELSE CD_RE_WON END CD_RE_WON
		      , CASE WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_CashDesk_draw_type  THEN AC.AO_PROMO_NOT_REDEEMABLE 
		             WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_GamingTable_draw_type AND AP.ACP_PROMO_TYPE = @_Promotion_Redeemeable THEN 0
		             WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_GamingTable_draw_type AND AP.ACP_PROMO_TYPE = @_Promotion_NoRedeemeable THEN AP.ACP_BALANCE
		             ELSE CD_NR_WON END CD_NR_WON
		      , CD_PRIZE_ID
		    FROM
		       CASHDESK_DRAWS
		       WITH(INDEX(IX_CD_ACCOUNT_DATETIME))
		        INNER JOIN CASHIER_MOVEMENTS WITH(INDEX(IX_CM_OPERATION_ID)) ON CD_OPERATION_ID = CM_OPERATION_ID 
		        INNER JOIN #ACCOUNTS_TEMP    ON CD_ACCOUNT_ID   = AC_ACCOUNT_ID
		        INNER JOIN TERMINALS   ON (CASE WHEN CD_TERMINAL IS NOT NULL THEN CD_TERMINAL ELSE @_Sorter_terminal_ID END) = TE_TERMINAL_ID
		        LEFT JOIN ACCOUNT_OPERATIONS AC ON AC.AO_OPERATION_ID = CD_OPERATION_ID
		        LEFT JOIN ACCOUNT_PROMOTIONS AP ON AP.ACP_OPERATION_ID = CD_OPERATION_ID AND AP.ACP_PROMO_TYPE IN (@_Promotion_Redeemeable, @_Promotion_NoRedeemeable)
		        
		    WHERE    
			    CD_DRAW_DATETIME >= @pDrawFrom
		       AND CD_DRAW_DATETIME < @pDrawTo 		   
		       AND((@pCashierId IS NULL) OR (CM_CASHIER_ID = @pCashierId))
		       AND ((@pUserId IS NULL) OR (CM_USER_ID = @pUserId))
		       AND ( CM_TYPE = @pCashierMovement )
		       AND    te_terminal_type = @_Terminal_CashDesk_draw_type
		       AND AC.ao_undo_status IS NULL
		    ORDER BY CD_DRAW_ID
		    DROP TABLE #ACCOUNTS_TEMP
		    
		    RETURN
	  END
  --Filter by GAMING TABLE draw
	  ELSE IF  @pCashDeskDraw = 0 AND @pGamingTableDraw = 1
	  BEGIN
          SELECT DISTINCT
		        CD_DRAW_ID
		      , CD_DRAW_DATETIME
		      , TE_TERMINAL_TYPE
		      , CM_CASHIER_NAME		  
		      , CM_USER_NAME
		      , CD_OPERATION_ID		
		      , CM_DATE
		      , CD_ACCOUNT_ID
		      , AC_HOLDER_NAME
		      , CD_COMBINATION_BET
		      , CD_COMBINATION_WON
		      , CD_RE_BET
          , CASE WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_CashDesk_draw_type THEN AC.AO_PROMO_REDEEMABLE 
                 WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_GamingTable_draw_type AND AP.ACP_PROMO_TYPE = @_Promotion_Redeemeable THEN AP.ACP_BALANCE
                 WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_GamingTable_draw_type AND AP.ACP_PROMO_TYPE = @_Promotion_NoRedeemeable THEN 0
                 ELSE CD_RE_WON END CD_RE_WON
		      , CASE WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_CashDesk_draw_type  THEN AC.AO_PROMO_NOT_REDEEMABLE 
		             WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_GamingTable_draw_type AND AP.ACP_PROMO_TYPE = @_Promotion_Redeemeable THEN 0
		             WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_GamingTable_draw_type AND AP.ACP_PROMO_TYPE = @_Promotion_NoRedeemeable THEN AP.ACP_BALANCE
		             ELSE CD_NR_WON END CD_NR_WON
		      , CD_PRIZE_ID
		    FROM
		       CASHDESK_DRAWS
		       WITH(INDEX(IX_CD_ACCOUNT_DATETIME))
		        INNER JOIN CASHIER_MOVEMENTS WITH(INDEX(IX_CM_OPERATION_ID)) ON CD_OPERATION_ID = CM_OPERATION_ID 
		        INNER JOIN #ACCOUNTS_TEMP    ON CD_ACCOUNT_ID   = AC_ACCOUNT_ID
		        INNER JOIN TERMINALS   ON (CASE WHEN CD_TERMINAL IS NOT NULL THEN CD_TERMINAL ELSE @_Sorter_terminal_ID END) = TE_TERMINAL_ID
		        LEFT JOIN ACCOUNT_OPERATIONS AC ON AC.AO_OPERATION_ID = CD_OPERATION_ID
		        LEFT JOIN ACCOUNT_PROMOTIONS AP ON AP.ACP_OPERATION_ID = CD_OPERATION_ID AND AP.ACP_PROMO_TYPE IN (@_Promotion_Redeemeable, @_Promotion_NoRedeemeable)
		        
		    WHERE    
			    CD_DRAW_DATETIME >= @pDrawFrom
		       AND CD_DRAW_DATETIME < @pDrawTo 		   
		       AND((@pCashierId IS NULL) OR (CM_CASHIER_ID = @pCashierId))
		       AND ((@pUserId IS NULL) OR (CM_USER_ID = @pUserId))
		       AND ( CM_TYPE = @pCashierMovement )
		       AND    te_terminal_type = @_Terminal_GamingTable_draw_type
		       AND AC.ao_undo_status IS NULL
		    ORDER BY CD_DRAW_ID
		    DROP TABLE #ACCOUNTS_TEMP
		    
		    RETURN
	  END	  
	END 
		
	--
	-- Filter by Date, but not by account neither Draw ID
	--
	IF @pDrawFrom IS NULL SET @pDrawFrom = '2007-01-01T00:00:00'
	IF @pDrawTo   IS NULL SET @pDrawTo   = GETDATE ()
	
	--No filter by draw type
	IF (@pCashDeskDraw = 1 AND @pGamingTableDraw = 1) OR (@pCashDeskDraw = 0 AND @pGamingTableDraw = 0)
	BEGIN
	  SELECT DISTINCT
	      CD_DRAW_ID
	    , CD_DRAW_DATETIME
	    , TE_TERMINAL_TYPE
	    , CM_CASHIER_NAME		  
	    , CM_USER_NAME
	    , CD_OPERATION_ID		
	    , CM_DATE
	    , CD_ACCOUNT_ID
	    , AC_HOLDER_NAME
	    , CD_COMBINATION_BET
	    , CD_COMBINATION_WON
	    , CD_RE_BET
      , CASE WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_CashDesk_draw_type THEN AC.AO_PROMO_REDEEMABLE 
             WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_GamingTable_draw_type AND AP.ACP_PROMO_TYPE = @_Promotion_Redeemeable THEN AP.ACP_BALANCE
             WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_GamingTable_draw_type AND AP.ACP_PROMO_TYPE = @_Promotion_NoRedeemeable THEN 0
             ELSE CD_RE_WON END CD_RE_WON
      , CASE WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_CashDesk_draw_type  THEN AC.AO_PROMO_NOT_REDEEMABLE 
             WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_GamingTable_draw_type AND AP.ACP_PROMO_TYPE = @_Promotion_Redeemeable THEN 0
             WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_GamingTable_draw_type AND AP.ACP_PROMO_TYPE = @_Promotion_NoRedeemeable THEN AP.ACP_BALANCE
             ELSE CD_NR_WON END CD_NR_WON
	    , CD_PRIZE_ID
	  FROM
	     CASHDESK_DRAWS WITH ( INDEX(IX_CD_DATETIME))	   
	      INNER JOIN CASHIER_MOVEMENTS WITH(INDEX(IX_CM_OPERATION_ID)) ON CD_OPERATION_ID = CM_OPERATION_ID
	      INNER JOIN ACCOUNTS    ON CD_ACCOUNT_ID   = AC_ACCOUNT_ID
	      INNER JOIN TERMINALS   ON (CASE WHEN CD_TERMINAL IS NOT NULL THEN CD_TERMINAL ELSE @_Sorter_terminal_ID END) = TE_TERMINAL_ID
	      LEFT JOIN ACCOUNT_OPERATIONS AC ON AC.AO_OPERATION_ID = CD_OPERATION_ID
  	    LEFT JOIN ACCOUNT_PROMOTIONS AP ON AP.ACP_OPERATION_ID = CD_OPERATION_ID AND AP.ACP_PROMO_TYPE IN (@_Promotion_Redeemeable, @_Promotion_NoRedeemeable)
  	  
	  WHERE     
		   CD_DRAW_DATETIME >= @pDrawFrom
	     AND 		CD_DRAW_DATETIME < @pDrawTo	     
	     AND 		((@pCashierId IS NULL) OR (CM_CASHIER_ID = @pCashierId))
	     AND 		((@pUserId IS NULL) OR (CM_USER_ID = @pUserId))
	     AND    ( CM_TYPE = @pCashierMovement )
	     AND    te_terminal_type IN (@_Terminal_CashDesk_draw_type, @_Terminal_GamingTable_draw_type)
	     AND AC.ao_undo_status IS NULL
	  ORDER BY CD_DRAW_ID
	  DROP TABLE #ACCOUNTS_TEMP
	  
	  RETURN
	END
	--Filter by CASH DESK draw
	ELSE IF  @pCashDeskDraw = 1 AND @pGamingTableDraw = 0
	BEGIN
      SELECT DISTINCT
	      CD_DRAW_ID
	    , CD_DRAW_DATETIME
	    , TE_TERMINAL_TYPE
	    , CM_CASHIER_NAME		  
	    , CM_USER_NAME
	    , CD_OPERATION_ID		
	    , CM_DATE
	    , CD_ACCOUNT_ID
	    , AC_HOLDER_NAME
	    , CD_COMBINATION_BET
	    , CD_COMBINATION_WON
	    , CD_RE_BET
      , CASE WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_CashDesk_draw_type THEN AC.AO_PROMO_REDEEMABLE 
             WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_GamingTable_draw_type AND AP.ACP_PROMO_TYPE = @_Promotion_Redeemeable THEN AP.ACP_BALANCE
             WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_GamingTable_draw_type AND AP.ACP_PROMO_TYPE = @_Promotion_NoRedeemeable THEN 0
             ELSE CD_RE_WON END CD_RE_WON
      , CASE WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_CashDesk_draw_type  THEN AC.AO_PROMO_NOT_REDEEMABLE 
             WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_GamingTable_draw_type AND AP.ACP_PROMO_TYPE = @_Promotion_Redeemeable THEN 0
             WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_GamingTable_draw_type AND AP.ACP_PROMO_TYPE = @_Promotion_NoRedeemeable THEN AP.ACP_BALANCE
             ELSE CD_NR_WON END CD_NR_WON
	    , CD_PRIZE_ID
	  FROM
	     CASHDESK_DRAWS WITH ( INDEX(IX_CD_DATETIME))	   
	      INNER JOIN CASHIER_MOVEMENTS WITH(INDEX(IX_CM_OPERATION_ID)) ON CD_OPERATION_ID = CM_OPERATION_ID
	      INNER JOIN ACCOUNTS          ON CD_ACCOUNT_ID   = AC_ACCOUNT_ID
	      INNER JOIN TERMINALS   ON (CASE WHEN CD_TERMINAL IS NOT NULL THEN CD_TERMINAL ELSE @_Sorter_terminal_ID END) = TE_TERMINAL_ID
	      LEFT JOIN ACCOUNT_OPERATIONS AC ON AC.AO_OPERATION_ID = CD_OPERATION_ID
  	    LEFT JOIN ACCOUNT_PROMOTIONS AP ON AP.ACP_OPERATION_ID = CD_OPERATION_ID AND AP.ACP_PROMO_TYPE IN (@_Promotion_Redeemeable, @_Promotion_NoRedeemeable)
  	  
	  WHERE     
		  CD_DRAW_DATETIME >= @pDrawFrom
	     AND 		CD_DRAW_DATETIME < @pDrawTo	     
	     AND 		((@pCashierId IS NULL) OR (CM_CASHIER_ID = @pCashierId))
	     AND 		((@pUserId IS NULL) OR (CM_USER_ID = @pUserId))
	     AND    ( CM_TYPE = @pCashierMovement ) 
	     AND    te_terminal_type = @_Terminal_CashDesk_draw_type
	     AND AC.ao_undo_status IS NULL
	  ORDER BY CD_DRAW_ID
	  DROP TABLE #ACCOUNTS_TEMP
	  
	  RETURN
	END
--Filter by GAMING TABLE draw
	ELSE IF  @pCashDeskDraw = 0 AND @pGamingTableDraw = 1
	BEGIN
      SELECT DISTINCT
	      CD_DRAW_ID
	    , CD_DRAW_DATETIME
	    , TE_TERMINAL_TYPE
	    , CM_CASHIER_NAME		  
	    , CM_USER_NAME
	    , CD_OPERATION_ID		
	    , CM_DATE
	    , CD_ACCOUNT_ID
	    , AC_HOLDER_NAME
	    , CD_COMBINATION_BET
	    , CD_COMBINATION_WON
	    , CD_RE_BET
      , CASE WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_CashDesk_draw_type THEN AC.AO_PROMO_REDEEMABLE 
             WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_GamingTable_draw_type AND AP.ACP_PROMO_TYPE = @_Promotion_Redeemeable THEN AP.ACP_BALANCE
             WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_GamingTable_draw_type AND AP.ACP_PROMO_TYPE = @_Promotion_NoRedeemeable THEN 0
             ELSE CD_RE_WON END CD_RE_WON
      , CASE WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_CashDesk_draw_type  THEN AC.AO_PROMO_NOT_REDEEMABLE 
             WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_GamingTable_draw_type AND AP.ACP_PROMO_TYPE = @_Promotion_Redeemeable THEN 0
             WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_GamingTable_draw_type AND AP.ACP_PROMO_TYPE = @_Promotion_NoRedeemeable THEN AP.ACP_BALANCE
             ELSE CD_NR_WON END CD_NR_WON
	    , CD_PRIZE_ID
	  FROM
	     CASHDESK_DRAWS with ( INDEX(IX_CD_DATETIME))	   
	      INNER JOIN CASHIER_MOVEMENTS WITH(INDEX(IX_CM_OPERATION_ID)) ON CD_OPERATION_ID = CM_OPERATION_ID 
	      INNER JOIN ACCOUNTS          ON CD_ACCOUNT_ID   = AC_ACCOUNT_ID
	      INNER JOIN TERMINALS   ON (CASE WHEN CD_TERMINAL IS NOT NULL THEN CD_TERMINAL ELSE @_Sorter_terminal_ID END) = TE_TERMINAL_ID
	      LEFT JOIN ACCOUNT_OPERATIONS AC ON AC.AO_OPERATION_ID = CD_OPERATION_ID
	      LEFT JOIN ACCOUNT_PROMOTIONS AP ON AP.ACP_OPERATION_ID = CD_OPERATION_ID AND AP.ACP_PROMO_TYPE IN (@_Promotion_Redeemeable, @_Promotion_NoRedeemeable)
  	  
	  WHERE     
		  CD_DRAW_DATETIME >= @pDrawFrom
	     AND 		CD_DRAW_DATETIME < @pDrawTo	     
	     AND 		((@pCashierId IS NULL) OR (CM_CASHIER_ID = @pCashierId))
	     AND 		((@pUserId IS NULL) OR (CM_USER_ID = @pUserId))
	     AND    ( CM_TYPE = @pCashierMovement ) 
	     AND    te_terminal_type = @_Terminal_GamingTable_draw_type
	     AND AC.ao_undo_status IS NULL
	  ORDER BY CD_DRAW_ID
	  DROP TABLE #ACCOUNTS_TEMP	
	END	

END


GO

GRANT EXECUTE ON [dbo].[ReportCashDeskDraws] TO [wggui] WITH GRANT OPTION
GO


/****** Object:  StoredProcedure [dbo].[sp_CheckCashDeskDrawConfig]    Script Date: 09/11/2017 17:08:32 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_CheckCashDeskDrawConfig]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_CheckCashDeskDrawConfig]
GO

 
CREATE PROCEDURE [dbo].[sp_CheckCashDeskDrawConfig]
      -- Add the parameters for the stored procedure here
      @pID INT = 0,
      @pName VARCHAR(20) = ''
AS
BEGIN
SET NOCOUNT ON;

DELETE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CashDesk.Draw.00'
DELETE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration.00'
   
DECLARE @POSTFIJ AS VARCHAR(5)

IF @pID = 0
BEGIN
  SET @POSTFIJ = ''
END
ELSE 
BEGIN 
  SET @POSTFIJ = '.' + RIGHT( '00' + CONVERT(VARCHAR(2),@pID),2)
END

 
  --'CashDesk.Draw', 'Enabled', 0
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ
                        AND GP_SUBJECT_KEY = 'Enabled'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'Enabled', '0')
END

 
--'CashDesk.Draw', 'ShowCashDeskDraws', 0
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ
                        AND GP_SUBJECT_KEY = 'ShowCashDeskDraws'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'ShowCashDeskDraws', '0')
END

 
--'CashDesk.Draw', 'VoucherOnCashDeskDraws.Winner', 0
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'VoucherOnCashDeskDraws.Winner'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'VoucherOnCashDeskDraws.Winner', '0')
END

 
--'CashDesk.Draw', 'WinnerPrizeVoucherTitle', 'Sorteo en XXXX'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrizeVoucherTitle'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'WinnerPrizeVoucherTitle', 'Sorteo en ' + @pName)
END

 
--'CashDesk.Draw', 'VoucherOnCashDeskDraws.Loser', 0
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'VoucherOnCashDeskDraws.Loser'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'VoucherOnCashDeskDraws.Loser', '0')
END

 
--'CashDesk.Draw', 'AskForParticipation', 0
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'AskForParticipation'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'AskForParticipation', '0')
END

 
--'CashDesk.Draw', 'LoserPrizeVoucherTitle','Cortesía sorteo en XXXX'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'LoserPrizeVoucherTitle'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'LoserPrizeVoucherTitle', 'Cortesía sorteo en ' + @pName)
END

 
--'CashDesk.Draw', 'ActionOnServerDown','0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'ActionOnServerDown'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'ActionOnServerDown', '0')
END

 
--'CashDesk.Draw', 'LocalServer', '1'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'LocalServer'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'LocalServer', '1')
END

 
--'CashDesk.Draw', 'ServerBDConnectionString',''
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'ServerBDConnectionString'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'ServerBDConnectionString', '')
END

 
--'CashDesk.Draw', 'ServerAddress1',''
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'ServerAddress1'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'ServerAddress1', '')
END

 
--'CashDesk.Draw', 'ServerAddress2',''
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'ServerAddress2'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'ServerAddress2', '')
END

--'CashDesk.Draw', 'GroupDraws',''
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'GroupDraws'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'GroupDraws', '1')
END

--'CashDesk.Draw', 'LetWithdrawalWithPendingDraw',''
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'LetWithdrawalWithPendingDraw'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'LetWithdrawalWithPendingDraw', '1')
END

--'CashDesk.Draw', 'LetCancelRechargeWithPendingDraw',''
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'LetCancelRechargeWithPendingDraw'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'LetCancelRechargeWithPendingDraw', '1')
END

--'CashDesk.DrawConfiguration', 'BallsExtracted','0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'BallsExtracted'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'BallsExtracted', '0')
END

 
--'CashDesk.DrawConfiguration', 'BallsOfParticipant', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'BallsOfParticipant'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'BallsOfParticipant', '0')
END

 
--'CashDesk.DrawConfiguration', 'TotalsBallsNumber', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'TotalsBallsNumber'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'TotalsBallsNumber', '0')
END

 
--'CashDesk.DrawConfiguration', 'LoserPrize1.Fixed', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'LoserPrize1.Fixed'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'LoserPrize1.Fixed', '0')
END

 
--'CashDesk.DrawConfiguration', 'LoserPrize1.Percentage', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'LoserPrize1.Percentage'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'LoserPrize1.Percentage', '0')
END

 
--'CashDesk.DrawConfiguration', 'LoserPrize1.Enabled', 0
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'LoserPrize1.Enabled'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'LoserPrize1.Enabled', '0')
END

 
--'CashDesk.DrawConfiguration', 'NumberOfParticipants', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'NumberOfParticipants'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'NumberOfParticipants', '0')
END

 
--'CashDesk.DrawConfiguration', 'NumberOfWinners', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'NumberOfWinners'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'NumberOfWinners', '0')
END

 
--'CashDesk.DrawConfiguration', 'WinnerPrize1.Fixed', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize1.Fixed'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize1.Fixed', '0')
END

 
--'CashDesk.DrawConfiguration', 'WinnerPrize1.Percentage', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize1.Percentage'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize1.Percentage', '0')
END

 
--'CashDesk.DrawConfiguration', 'WinnerPrize1.Enabled', 0
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize1.Enabled'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize1.Enabled', '0')
END

 
--'CashDesk.DrawConfiguration', 'WinnerPrize2.Fixed', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize2.Fixed'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize2.Fixed', '0')
END

 
--'CashDesk.DrawConfiguration', 'WinnerPrize2.Percentage', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize2.Percentage'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize2.Percentage', '0')
END

 
--'CashDesk.DrawConfiguration', 'WinnerPrize2.Enabled', 0
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize2.Enabled'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize2.Enabled', '0')
END

 
--'CashDesk.DrawConfiguration', 'WinnerPrize3.Fixed', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize3.Fixed'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize3.Fixed', '0')
END

 
--'CashDesk.DrawConfiguration', 'WinnerPrize3.Percentage', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize3.Percentage'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize3.Percentage', '0')
END

 
--'CashDesk.DrawConfiguration', 'WinnerPrize3.Enabled', 0
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize3.Enabled'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize3.Enabled', '0')
END

 
--'CashDesk.DrawConfiguration', 'WinnerPrize4.Fixed', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize4.Fixed'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize4.Fixed', '0')
END

 
--'CashDesk.DrawConfiguration', 'WinnerPrize4.Percentage', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize4.Percentage'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize4.Percentage', '0')
END

 
--'CashDesk.DrawConfiguration', 'WinnerPrize4.Enabled', 0
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize4.Enabled'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize4.Enabled', '0')
END

 
 
--'CashDesk.DrawConfiguration', 'WinnerPrize1.PrizeType', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize1.PrizeType'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize1.PrizeType', '0')
END

 
--'CashDesk.DrawConfiguration', 'WinnerPrize2.PrizeType', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize2.PrizeType'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize2.PrizeType','0')
END

 
--'CashDesk.DrawConfiguration', 'WinnerPrize3.PrizeType', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize3.PrizeType'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize3.PrizeType', '0')
END

 
--'CashDesk.DrawConfiguration', 'WinnerPrize4.PrizeType', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize4.PrizeType'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize4.PrizeType', '0')
END

 
--'CashDesk.DrawConfiguration', 'LoserPrize1.PrizeType', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'LoserPrize1.PrizeType'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'LoserPrize1.PrizeType', '0')
END

 
-- 'CashDesk.Draw', 'AccountingMode', ''
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'AccountingMode'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'AccountingMode', '0')
END

 
-- 'CashDesk.DrawConfiguration', 'ParticipationPrice', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'ParticipationPrice'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'ParticipationPrice', '0')
END   
      
      
-- 'CashDesk.Draw', 'IsCashDeskDraw', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'IsCashDeskDraw'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'IsCashDeskDraw', '0')
END         

 
-- 'CashDesk.Draw', 'ReportUNRFromSP_SalesAndPayment', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'ReportUNRFromSP_SalesAndPayment'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'ReportUNRFromSP_SalesAndPayment', '0')
END   

 
-- 'CashDesk.Draw', 'AccountingMode.PromoNameMovement', 'PROMOCIÓN'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'AccountingMode.PromoNameMovement'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'AccountingMode.PromoNameMovement', 'Promoción')
END   

-- 'CashDesk.Draw', 'AccountingMode.KindOfNameMovement', 'Premio en especie'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'AccountingMode.KindOfNameMovement'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'AccountingMode.KindOfNameMovement', 'Premio en especie')
END   

-- 'CashDesk.Draw', 'AccountingMode.RE.KindOfNameMovement', 'Premio en especie (RE)'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'AccountingMode.RE.KindOfNameMovement'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'AccountingMode.RE.KindOfNameMovement', 'Premio en especie (RE)')
END   

-- 'CashDesk.Draw', 'AccountingMode.RE.PromoNameMovement', 'Promoción (RE)'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'AccountingMode.RE.PromoNameMovement'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'AccountingMode.RE.PromoNameMovement', 'Promoción (RE)')
END   

 
-- 'CashDesk.Draw', 'Voucher.HideCurrencySymbol', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'Voucher.HideCurrencySymbol'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'Voucher.HideCurrencySymbol', '0')
END   
      
-- 'CashDesk.Draw', 'Voucher.LoserPrizeLabel', 'Cortesia'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'Voucher.LoserPrizeLabel'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'Voucher.LoserPrizeLabel', 'Cortesia')
END   
      
-- 'CashDesk.Draw', 'Voucher.UNRLabel', 'FD'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'Voucher.UNRLabel'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'Voucher.UNRLabel', 'FD')
END   

-- 'CashDesk.Draw', 'Voucher.WinnerPrizeLabel', 'Premio'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'Voucher.WinnerPrizeLabel'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'Voucher.WinnerPrizeLabel', 'Premio')
END   

 
--/// Probability

 
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'LoserPrize1.Probability'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'LoserPrize1.Probability', '0')
END

IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize1.Probability'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize1.Probability', '0')
END

IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize2.Probability'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize2.Probability', '0')
END

IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize3.Probability'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize3.Probability', '0')
END

IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'WinnerPrize4.Probability'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize4.Probability', '0')
END

 
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'ParticipationMaxPrice'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'ParticipationMaxPrice', '1')
END

 
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameUrl'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameUrl', '')
END

 
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameTimeout'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameTimeout', '30')
END

IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameName'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameName', 'Terminal Draw')
END

IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameSecondDrawScreenMessageLine0'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameSecondDrawScreenMessageLine0', 'Bienvenido')
END

IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameSecondDrawScreenMessageLine1'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameSecondDrawScreenMessageLine1', 'Pulse[1][2][3][4][5]')
END

IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameSecondDrawScreenMessageLine2'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameSecondDrawScreenMessageLine2', 'Saldo: @BALANCE')
END

 
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameSecondDrawScreenMessageLine3'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameSecondDrawScreenMessageLine3', 'Apuesta: @BET')
END

--Message Result Winner
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameResultWinnerDrawScreenMessageLine0'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameResultWinnerDrawScreenMessageLine0', 'Felicidades!!!')
END

 
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameResultWinnerDrawScreenMessageLine1'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameResultWinnerDrawScreenMessageLine1', 'Ganaste: @WON')
END

 
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameResultWinnerDrawScreenMessageLine2'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameResultWinnerDrawScreenMessageLine2', 'Saldo: @BALANCE')
END

IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameResultWinnerDrawScreenMessageLine3'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameResultWinnerDrawScreenMessageLine3', '')
END

--Message Result Looser
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameResultLooserDrawScreenMessageLine0'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameResultLooserDrawScreenMessageLine0', 'Perdiste!!!')
END

 
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameResultLooserDrawScreenMessageLine1'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameResultLooserDrawScreenMessageLine1', 'Premio cortesía: @WON')
END

 
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameResultLooserDrawScreenMessageLine2'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameResultLooserDrawScreenMessageLine2', 'Saldo: @BALANCE')
END

IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameResultLooserDrawScreenMessageLine3'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameResultLooserDrawScreenMessageLine3', '')
END

-- First Screen
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameFirstDrawScreenMessageLine0'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameFirstDrawScreenMessageLine0', '')
END

 
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameFirstDrawScreenMessageLine1'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameFirstDrawScreenMessageLine1', 'Pulse [1] para jugar')
END

 
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameFirstDrawScreenMessageLine2'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameFirstDrawScreenMessageLine2', '')
END

 
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameFirstDrawScreenTimeOut'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameFirstDrawScreenTimeOut', '10')
END

 
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameFirsIfTimeOutExpiresParticipateInDraw'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameFirsIfTimeOutExpiresParticipateInDraw', '0')
END

 
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'CloseOpenedSessionsBeforeNewGame'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'CloseOpenedSessionsBeforeNewGame', '0')
END
   
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameFirstDrawScreenForceParticipateInDraw'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameFirstDrawScreenForceParticipateInDraw', '0')
END

IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameSecondDrawScreenForceExecuteDraw'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameSecondDrawScreenForceExecuteDraw', '0')
END
          
             
END
GO

GRANT EXECUTE ON sp_CheckCashDeskDrawConfig TO wggui WITH GRANT OPTION
GO

/*----------------------------------------------------------------------------------------------------------------
BASIC REPORT QUERY FOR GAMING TABLES

Version  Date           User     Description
----------------------------------------------------------------------------------------------------------------
1.0.1    24-DEC-2013    RMS      Using GAMING_TABLE_SESSION
1.0.2    20-FEB-2014    RMS      Only get data of closed gaming table sessions
1.0.3    26-FEB-2015    ANM      Add sum in SESSION_HOURS to get average time later in code
1.0.4    06-JUN-2016    FOS      Calculate drop in gamblingtables and cashier
1.0.5    20-JUL-2016    RAB      Add MultiCurrency to report
1.0.6    05-APR-2017    JML      Add validated tickets 
1.0.7    23-FEB-2018    RGR      Bug 31477:WIGOS-8152 [Ticket #12340] Fallo: Reporte Control de Ingresos Agrupado por Fecha Version V03.06.0035

Requeriments:
   -- Functions:
         dbo.GT_Calculate_DROP( Amount )
         dbo.GT_Calculate_WIN ( AmountAdded, AmountSubtracted, CollectedAmount, TipsAmount )

Parameters:
     -- BASE TYPE:                  Indicates if data is based on table types (0) or the tables (1).
     -- TIME INTERVAL:              Range of time to group data between days (0), months (1) and years (2).
     -- DATE FROM:                  Start date for data collection. (If NULL then use first available date).
     -- DATE TO:                    End date for data collection. (If NULL then use current date).
     -- ONLY ACTIVITY:              If true (1) results will only show tables or table types with activity inside the dates range,
                      else (0) results will include all tables or table types.
     -- ORDER BY:                   0) to sort by date (interval) descendant, (1) to sort by table type or table identifier.
     -- VALID TYPES:                A string that contains the valid table types to list.
     -- SELECTED CURRENCY:          Selected currency in a frm_gaming_tables_income_report
     -- TOTAL TO SELECTED CURRENCY: 0 --> Option: Total to local iso code
                                    1 --> Option: By selected currency

Process: (From inside to outside)

   1- Core Query:
         Query all game table session data linked to the cashier sessions that where linked with a gaming_table and/or gaming table type.

   2- Specific report query:
         Here we group by base type and time interval and includes the session information for the interval, generating a temporary
         table.

   3- Filter activity:
         Join the temporary table with the time intervals table generated at first doing a left join to show all data, else an inner
         join to filter tables or table types without activity.

   4- Drop the temporary table.

 Results:

   We can create 6 different report, based on table types or tables and grouped by days, months and years.
----------------------------------------------------------------------------------------------------------------
*/

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID('GT_Base_Report_Data') AND type in ('P', 'PC'))
DROP PROCEDURE GT_Base_Report_Data
GO

CREATE PROCEDURE [dbo].[GT_Base_Report_Data]
(
      @pBaseType                   INTEGER
    , @pTimeInterval               INTEGER
    , @pDateFrom                   DATETIME
    , @pDateTo                     DATETIME
    , @pOnlyActivity               INTEGER
    , @pOrderBy                    INTEGER
    , @pValidTypes                 VARCHAR(4096)
    , @pSelectedCurrency           NVARCHAR(100)
    , @pTotalToSelectedCurrency    INTEGER
)
AS
BEGIN

-- DECLARATIONS
   -- @_DAYS_AND_TABLES:    A temporary table with all intervals and all base type selected (used to show base types without activity).
   DECLARE @_DAYS_AND_TABLES TABLE(DATE_TIME DATETIME, TABLE_IDENTIFIER_VALUE BIGINT, TABLE_IDENT_NAME VARCHAR(50), TABLE_TYPE_IDENT BIGINT, TABLE_TYPE_NAME VARCHAR(50))
   -- @_DAY_VAR:            Variable to create the intervals
   DECLARE @_DAY_VAR DATETIME

-- PARAMETERS
     DECLARE @_BASE_TYPE                  AS   INTEGER -- 0 BY TABLE TYPE, 1 BY TABLE
     DECLARE @_TIME_INTERVAL              AS   INTEGER -- -1, 0 DAYS, 1 MONTH, 2 YEAR
     DECLARE @_DATE_FROM                  AS   DATETIME
     DECLARE @_DATE_TO                    AS   DATETIME
     DECLARE @_ONLY_ACTIVITY              AS   INTEGER -- 0 SHOW ALL, 1 SHOW ONLY WITH ACTIVITY
     DECLARE @_ORDER_BY                   AS   INTEGER -- 0 BY INTERVAL DESC, 1 BY IDENTIFIER ASC
     DECLARE @_DELIMITER                  AS   CHAR(1)
     DECLARE @_TYPES_TABLE                TABLE(SST_ID INT, SST_VALUE VARCHAR(50)) -- TO STORE THE VALID GAMING TABLE TYPES
     DECLARE @_SELECTED_CURRENCY          TABLE(SST_ID INT, SST_VALUE VARCHAR(50)) -- TO STORE SELECTED CATEGORIES
     DECLARE @_TOTAL_TO_SELECTED_CURRENCY AS   INTEGER
     DECLARE @_CHIP_RE                    AS   INTEGER
     DECLARE @_CHIP_NRE                   AS   INTEGER
     DECLARE @_CHIP_COLOR                 AS   INTEGER
----------------------------------------------------------------------------------------------------------------

-- Initialization --
  SET @_BASE_TYPE                  =   @pBaseType
  SET @_TIME_INTERVAL              =   @pTimeInterval
  SET @_DATE_FROM                  =   @pDateFrom
  SET @_DATE_TO                    =   @pDateTo
  SET @_ONLY_ACTIVITY              =   @pOnlyActivity
  SET @_ORDER_BY                   =   @pOrderBy
  SET @_DELIMITER                  =   ','
  SET @_TOTAL_TO_SELECTED_CURRENCY =   @pTotalToSelectedCurrency
  SET @_CHIP_RE                    =   1001
  SET @_CHIP_NRE                   =   1002
  SET @_CHIP_COLOR                 =   1003
----------------------------------------------------------------------------------------------------------------

-- CHECK DATE PARAMETERS
IF @_DATE_FROM IS NULL
BEGIN
   -- IF DATE FROM IS NULL, USER FIRST POSIBLE DATE
   SET @_DATE_FROM = CAST('' AS DATETIME)
   SET @pDateFrom  = CAST('' AS DATETIME)
END

IF @_DATE_TO IS NULL
BEGIN
   -- IF DATE TO IS NULL, QUERY UNTIL CURRENT DATE
   SET @_DATE_TO = CAST(GETDATE() AS DATETIME)
   SET @pDateTo  = CAST(GETDATE() AS DATETIME)
END

-- ASSIGN TYPES PARAMETER INTO TABLE
INSERT INTO @_TYPES_TABLE SELECT * FROM dbo.SplitStringIntoTable(@pValidTypes, @_DELIMITER, DEFAULT)

INSERT INTO @_SELECTED_CURRENCY SELECT * FROM dbo.SplitStringIntoTable(@pSelectedCurrency, @_DELIMITER, DEFAULT)

IF @_TIME_INTERVAL <> -1
 BEGIN
   -- INTERVALS DATES AND TABLE PREPARATION

   -- PREPARE DATES RANGE DEPENDING ON TIME INTERVAL
   SET @_DATE_FROM = CASE
                        WHEN @_TIME_INTERVAL = 0 THEN -- BY DAY
                           DATEADD(dd, 0, DATEDIFF(dd, 0, @_DATE_FROM))
                        WHEN @_TIME_INTERVAL = 1 THEN -- BY MONTH
                           CAST(CAST(YEAR(@_DATE_FROM) AS VARCHAR(4)) + RIGHT('0' + CAST(MONTH(@_DATE_FROM) AS VARCHAR(2)), 2) + '01' AS DATETIME)
                        WHEN @_TIME_INTERVAL = 2 THEN -- BY YEAR
                           CAST(CAST(YEAR(@_DATE_FROM) AS VARCHAR(4)) + '0101' AS DATETIME)
                     END

   SET @_DATE_TO = CASE
          WHEN @_TIME_INTERVAL = 0 THEN -- BY DAY
                           CAST(@_DATE_TO AS DATETIME)
                        WHEN @_TIME_INTERVAL = 1 THEN -- BY MONTH
                           DATEADD(MONTH, 1, CAST(CAST(YEAR(@_DATE_TO) AS VARCHAR(4)) + RIGHT('0' + CAST(MONTH(@_DATE_TO) AS VARCHAR(2)), 2) + '01' AS DATETIME))
                        WHEN @_TIME_INTERVAL = 2 THEN -- BY YEAR
                           DATEADD(YEAR, 1, CAST(CAST(YEAR(@_DATE_TO) AS VARCHAR(4)) + '0101' AS DATETIME))
                     END

   -- PREPARE THE TIME INTERVALS TABLE
   SET @_DAY_VAR = @_DATE_FROM
   WHILE @_DAY_VAR < @_DATE_TO
   BEGIN
         -- SET THE LINK FIELD FOR THE RESULTS
         IF @_BASE_TYPE = 0
          BEGIN
            -- LINK WITH TABLE TYPES
            INSERT INTO @_DAYS_AND_TABLES
                 SELECT   CAST(@_DAY_VAR AS DATETIME), X.GTT_GAMING_TABLE_TYPE_ID, X.GTT_NAME AS TABLE_IDENT, NULL, NULL
                   FROM   GAMING_TABLES_TYPES AS X
                  WHERE   X.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
                          AND     X.GTT_ENABLED = 1
          END
         ELSE
          BEGIN
            -- LINK WITH TABLES
            INSERT INTO @_DAYS_AND_TABLES
                 SELECT   CAST(@_DAY_VAR AS DATETIME), X.GT_GAMING_TABLE_ID, X.GT_NAME AS TABLE_IDENT, Z.GTT_GAMING_TABLE_TYPE_ID, Z.GTT_NAME
                   FROM   GAMING_TABLES AS X
              LEFT JOIN   GAMING_TABLES_TYPES AS Z
                     ON   Z.GTT_GAMING_TABLE_TYPE_ID = X.GT_TYPE_ID
                  WHERE   Z.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
                          AND X.GT_ENABLED = 1
          END

          -- SET INCREMENT
          SET @_DAY_VAR = CASE
                               WHEN @_TIME_INTERVAL = 0 THEN DATEADD(DAY,1,@_DAY_VAR)     -- DAY
                               WHEN @_TIME_INTERVAL = 1 THEN DATEADD(MONTH,1,@_DAY_VAR)   -- MONTH
                               WHEN @_TIME_INTERVAL = 2 THEN DATEADD(YEAR,1,@_DAY_VAR)    -- YEAR
                          END
   END

END -- IF INTERVALS <> - 1
ELSE
BEGIN

   -- SET THE LINK FIELD FOR THE RESULTS WITHOUT INTERVALS
   IF @_BASE_TYPE = 0
    BEGIN
      -- LINK WITH TABLE TYPES
      INSERT INTO @_DAYS_AND_TABLES
           SELECT   NULL, X.GTT_GAMING_TABLE_TYPE_ID, X.GTT_NAME AS TABLE_IDENT, NULL, NULL
             FROM   GAMING_TABLES_TYPES AS X
            WHERE   X.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
                    AND X.GTT_ENABLED = 1
    END
   ELSE
    BEGIN
      -- LINK WITH TABLES
      INSERT INTO @_DAYS_AND_TABLES
           SELECT   NULL, X.GT_GAMING_TABLE_ID, X.GT_NAME AS TABLE_IDENT, Z.GTT_GAMING_TABLE_TYPE_ID, Z.GTT_NAME
             FROM   GAMING_TABLES AS X
        LEFT JOIN   GAMING_TABLES_TYPES AS Z
               ON   Z.GTT_GAMING_TABLE_TYPE_ID = X.GT_TYPE_ID
            WHERE   Z.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
                    AND X.GT_ENABLED = 1
    END

 END


-- SELECT INTO TEMPORARY TABLE WITH ALL DATA
-- SPECIFIC REPORT QUERY
  SELECT   X.TABLE_IDENTIFIER
         , X.TABLE_NAME
         , X.TABLE_TYPE
         , X.TABLE_TYPE_NAME
         , SUM(X.COPY_DEALER_VALIDATED_AMOUNT) AS COPY_DEALER_VALIDATED_AMOUNT         
         , SUM(X.S_DROP_GAMBLING_TABLE) + SUM (X.S_DROP_CASHIER) TOTAL_DROP
         , SUM(X.S_DROP_GAMBLING_TABLE) TOTAL_DROP_GAMBLING_TABLE
         , SUM(X.S_DROP_CASHIER) TOTAL_DROP_CASHIER
         , SUM(X.S_WIN) TOTAL_WIN
         , ISNULL(SUM(X.S_TIP),0)  TOTAL_TIP
         , CASE WHEN SUM(X.S_DROP_GAMBLING_TABLE) + SUM (X.S_DROP_CASHIER) = 0 THEN 0 ELSE CAST(SUM(X.S_WIN) AS DECIMAL(18,2)) / CAST(SUM(X.S_DROP_GAMBLING_TABLE) + SUM (X.S_DROP_CASHIER) AS DECIMAL(18,2)) * 100 END AS WIN_DROP
         , MIN(X.THEORIC_HOLD) AS THEORIC_HOLD
         , CASE WHEN SUM(X.S_DROP_GAMBLING_TABLE) + SUM (X.S_DROP_CASHIER) = 0 THEN 0 ELSE SUM(CAST(X.S_TIP AS DECIMAL(18,2))) / CAST(SUM(X.S_DROP_GAMBLING_TABLE) + SUM (X.S_DROP_CASHIER) AS DECIMAL(18,2)) * 100 END AS TIP_DROP
         , SUM(X.S_WIN) + SUM(X.S_TIP) AS WIN_TIPS
         , X.CM_DATE_ONLY
         , MIN(X.OPEN_HOUR) AS OPEN_HOUR
         , MAX(X.CLOSE_HOUR) AS CLOSE_HOUR
         , SUM(X.SESSION_SECONDS) AS SESSION_SECONDS
         , COUNT(X.SESSION_SECONDS) AS SESSION_SUM         
    INTO   #GT_TEMPORARY_REPORT_DATA
   FROM (
           -- CORE QUERY
         SELECT CASE WHEN @_TIME_INTERVAL = 0      -- TO FILTER BY DAY
                     THEN DATEADD(dd, 0, DATEDIFF(dd, 0, CS_OPENING_DATE))
                     WHEN @_TIME_INTERVAL = 1      -- TO FILTER BY MONTH
                     THEN CAST(CAST(YEAR(CS_OPENING_DATE) AS VARCHAR(4)) + RIGHT('0' + CAST(MONTH(CS_OPENING_DATE) AS VARCHAR(2)), 2) + '01' AS DATETIME)
                     WHEN @_TIME_INTERVAL = 2      -- TO FILTER BY YEAR
                     THEN CAST(CAST(YEAR(CS_OPENING_DATE) AS VARCHAR(4)) + '01'+ '01' AS DATETIME)
                        END                                                                                       AS CM_DATE_ONLY
                     , (CASE WHEN @_BASE_TYPE = 0 THEN GTT_GAMING_TABLE_TYPE_ID ELSE GT.GT_GAMING_TABLE_ID END)   AS TABLE_IDENTIFIER -- GET THE BASE TYPE IDENTIFIER
                     , (CASE WHEN @_BASE_TYPE = 0 THEN GTT_NAME ELSE GT.GT_NAME END)                              AS TABLE_NAME       -- GET THE BASE TYPE IDENTIFIER NAME
                     , (CASE WHEN @_BASE_TYPE = 0 THEN NULL ELSE GTT_GAMING_TABLE_TYPE_ID END)                    AS TABLE_TYPE       -- TYPE
                     , (CASE WHEN @_BASE_TYPE = 0 THEN NULL ELSE GTT_NAME END)                                    AS TABLE_TYPE_NAME  -- TYPE NAME

                ,  CASE WHEN @_TOTAL_TO_SELECTED_CURRENCY = 0 
                        THEN DBO.GT_CALCULATE_DROP_GAMBLING_TABLES(ISNULL(GTS_COLLECTED_AMOUNT                , 0)
                                                                         , ISNULL(GTS_COLLECTED_DROPBOX_AMOUNT        , 0)
                                                                         , ISNULL(GTS_COLLECTED_DROPBOX_CHIPS_AMOUNT  , 0)
                                                                         , 0 -- It does not allow takings of dropbox with tickets
                                                                         , GT_HAS_INTEGRATED_CASHIER)
                        WHEN @_TOTAL_TO_SELECTED_CURRENCY = 1 
                        THEN DBO.GT_CALCULATE_DROP_GAMBLING_TABLES(SUM(ISNULL(GTSC_COLLECTED_AMOUNT                , 0))
                                                                         , SUM(ISNULL(CASE WHEN GTSC_TYPE = 0            THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                                           WHEN GTSC_TYPE = 1            THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                                      ELSE 0 END,  0))
                                                                         , SUM(ISNULL(CASE WHEN GTSC_TYPE = @_CHIP_RE    THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                                           WHEN GTSC_TYPE = @_CHIP_NRE   THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                                           --WHEN GTSC_TYPE = @_CHIP_COLOR THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                             ELSE 0 END,  0))
                                                                         , 0 -- It does not allow takings of dropbox with tickets
                                                                         , GT_HAS_INTEGRATED_CASHIER)
                             END                                                                                  AS S_DROP_GAMBLING_TABLE

                ,  CASE WHEN @_TOTAL_TO_SELECTED_CURRENCY = 0 
                        THEN DBO.GT_CALCULATE_DROP_CASHIER(ISNULL(GTS_OWN_SALES_AMOUNT, 0)
                                                                 , ISNULL(GTS_EXTERNAL_SALES_AMOUNT, 0))
                        WHEN @_TOTAL_TO_SELECTED_CURRENCY = 1 
                        THEN DBO.GT_CALCULATE_DROP_CASHIER(SUM(ISNULL(GTSC_OWN_SALES_AMOUNT, 0))
                                                                 , SUM(ISNULL(GTSC_EXTERNAL_SALES_AMOUNT, 0)))
                        END                                                                              AS S_DROP_CASHIER

                ,  GT_THEORIC_HOLD AS THEORIC_HOLD
                ,  CASE WHEN @_TOTAL_TO_SELECTED_CURRENCY = 0 
                        THEN DBO.GT_CALCULATE_WIN(ISNULL(GTS_FINAL_CHIPS_AMOUNT      , 0)
                                                        , ISNULL(GTS_INITIAL_CHIPS_AMOUNT    , 0)
                                                        , ISNULL(GTS_FILLS_CHIPS_AMOUNT      , 0)
                                                        , ISNULL(GTS_CREDITS_CHIPS_AMOUNT    , 0)                                                        
                                                        , ISNULL(GTS_TIPS                    , 0)
                                                        , ISNULL(GTS_OWN_SALES_AMOUNT        , 0)
                                                        , ISNULL(GTS_EXTERNAL_SALES_AMOUNT   , 0)
                                                        , ISNULL(GTS_COLLECTED_AMOUNT        , 0)
                                                        , ISNULL(GTS_COLLECTED_DROPBOX_AMOUNT        , 0)
                                                        , ISNULL(GTS_COLLECTED_DROPBOX_CHIPS_AMOUNT  , 0)
                                                        , 0 -- It does not allow takings of dropbox with tickets)
                                                        , GT_HAS_INTEGRATED_CASHIER)
                        WHEN @_TOTAL_TO_SELECTED_CURRENCY = 1 
                        THEN DBO.GT_CALCULATE_WIN(SUM(ISNULL(GTSC_FINAL_CHIPS_AMOUNT   , 0))
                                                        , SUM(ISNULL(GTSC_INITIAL_CHIPS_AMOUNT , 0))
                                                        , SUM(ISNULL(GTSC_FILLS_CHIPS_AMOUNT   , 0))
                                                , SUM(ISNULL(GTSC_CREDITS_CHIPS_AMOUNT , 0))
                                                        , SUM(ISNULL(GTSC_TIPS                 , 0))
                                                        , SUM(ISNULL(GTSC_OWN_SALES_AMOUNT     , 0))
                                                        , SUM(ISNULL(GTSC_EXTERNAL_SALES_AMOUNT, 0))
                                                        , SUM(ISNULL(GTSC_COLLECTED_AMOUNT     , 0))
                                                        , SUM(ISNULL(CASE WHEN GTSC_TYPE = 0            THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                          WHEN GTSC_TYPE = 1            THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                          ELSE 0 END,  0))
                                                        , SUM(ISNULL(CASE WHEN GTSC_TYPE = @_CHIP_RE    THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                          WHEN GTSC_TYPE = @_CHIP_NRE   THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                          --WHEN GTSC_TYPE = @_CHIP_COLOR THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                          ELSE 0 END,  0))
                                                        , 0 -- It does not allow takings of dropbox with tickets
                                                        , GT_HAS_INTEGRATED_CASHIER)
                             END AS S_WIN

                     ,  CASE WHEN @_TOTAL_TO_SELECTED_CURRENCY = 0 THEN
                                     SUM(ISNULL(GTS_TIPS, 0))
                             WHEN @_TOTAL_TO_SELECTED_CURRENCY = 1 THEN
                                     SUM(ISNULL(GTSC_TIPS, 0))
                             ELSE 0
                             END                                                                       AS S_TIP
                     , CS.CS_OPENING_DATE                                                              AS OPEN_HOUR
                     , CS.CS_CLOSING_DATE                                                              AS CLOSE_HOUR
                     , DATEDIFF(SECOND, CS_OPENING_DATE, ISNULL(CS_CLOSING_DATE, GETDATE()))           AS SESSION_SECONDS
                     , CASE WHEN @_TOTAL_TO_SELECTED_CURRENCY = 0
                            THEN ISNULL(GTS_COPY_DEALER_VALIDATED_AMOUNT, 0)
                            WHEN @_TOTAL_TO_SELECTED_CURRENCY = 1 
                            THEN SUM(ISNULL(GTSC_COPY_DEALER_VALIDATED_AMOUNT, 0))
                            END                                                           AS COPY_DEALER_VALIDATED_AMOUNT
                FROM   GAMING_TABLES_SESSIONS GTS
           LEFT JOIN   GAMING_TABLES_SESSIONS_BY_CURRENCY GTSC
                  ON   GTSC.GTSC_GAMING_TABLE_SESSION_ID = GTS.GTS_GAMING_TABLE_SESSION_ID
                 AND   GTSC_ISO_CODE IN ( SELECT SST_VALUE FROM @_SELECTED_CURRENCY) 
                 AND   GTSC_TYPE <> @_CHIP_COLOR
          INNER JOIN   CASHIER_SESSIONS CS ON GTS.GTS_CASHIER_SESSION_ID = CS.CS_SESSION_ID
                 AND   CS_OPENING_DATE >= @pDateFrom AND CS_OPENING_DATE < @pDateTo
          INNER JOIN   GAMING_TABLES GT ON GTS.GTS_GAMING_TABLE_ID = GT.GT_GAMING_TABLE_ID
                 AND   GT.GT_TYPE_ID IN (SELECT SST_VALUE FROM @_TYPES_TABLE)
          INNER JOIN   GAMING_TABLES_TYPES GTT ON GT.GT_TYPE_ID = GTT.GTT_GAMING_TABLE_TYPE_ID
               WHERE   CS_STATUS = 1 -- Only closed sessions
                 AND   (@_TOTAL_TO_SELECTED_CURRENCY = 0 AND GTSC_TYPE = 0 OR @_TOTAL_TO_SELECTED_CURRENCY = 1) 
            GROUP BY   CS_OPENING_DATE, GTT_GAMING_TABLE_TYPE_ID, GT_GAMING_TABLE_ID, GTT_NAME, GT_NAME
                     , GTS_OWN_SALES_AMOUNT, GTS_EXTERNAL_SALES_AMOUNT, GTS_COLLECTED_AMOUNT, GT_HAS_INTEGRATED_CASHIER
                     , GTS_COLLECTED_DROPBOX_AMOUNT, GTS_COLLECTED_DROPBOX_AMOUNT, GTS_DROPBOX_ENABLED
                     , GTS_COLLECTED_DROPBOX_CHIPS_AMOUNT, GT_THEORIC_HOLD, GTS_FINAL_CHIPS_AMOUNT, GTS_INITIAL_CHIPS_AMOUNT
                     , GTS_FILLS_CHIPS_AMOUNT, GTS_CREDITS_CHIPS_AMOUNT, GTSC_TYPE, GTS_TIPS, GTSC_TIPS, CS_CLOSING_DATE
                     , GTS_COPY_DEALER_VALIDATED_AMOUNT
          -- END CORE QUERY
        ) AS X
  GROUP BY   X.TABLE_TYPE, X.TABLE_IDENTIFIER, X.TABLE_TYPE_NAME, X.TABLE_NAME, X.CM_DATE_ONLY  -- Group by indentifier and time interval

IF @_TIME_INTERVAL <> -1
BEGIN
   -- INTERVALS DATES AND TABLE FINAL PREPARATION
   -- FILTER ACTIVITY
   IF @_ONLY_ACTIVITY = 0
    BEGIN
      -- JOIN THE SELECT WITH ALL DATA
      SELECT   DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER
             , DT.TABLE_IDENT_NAME AS TABLE_NAME
             , DT.TABLE_TYPE_IDENT AS TABLE_TYPE
             , DT.TABLE_TYPE_NAME AS TABLE_TYPE_NAME
             , ISNULL(zz.COPY_DEALER_VALIDATED_AMOUNT, 0) AS COPY_DEALER_AMOUNT
             , ISNULL(ZZ.TOTAL_DROP_GAMBLING_TABLE,0) AS TOTAL_DROP_GAMBLING_TABLE
             , ISNULL(ZZ.TOTAL_DROP_CASHIER,0) AS  TOTAL_DROP_CASHIER
             , ISNULL(ZZ.TOTAL_DROP, 0) AS TOTAL_DROP
             , ISNULL(ZZ.TOTAL_WIN, 0) AS TOTAL_WIN
             , ISNULL(ZZ.TOTAL_TIP, 0) AS TOTAL_TIP
             , ISNULL(ZZ.WIN_DROP, 0) AS WIN_DROP
             , ISNULL(ZZ.THEORIC_HOLD, 0) AS THEORIC_HOLD
             , ISNULL(ZZ.TIP_DROP, 0) AS TIP_DROP
             , ISNULL(ZZ.WIN_TIPS, 0) AS WIN_TIPS
             , ZZ.OPEN_HOUR
             , ZZ.CLOSE_HOUR
             , CASE WHEN ZZ.CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED
             , CASE WHEN ZZ.SESSION_SECONDS = 0 THEN DATEDIFF(SECOND,ZZ.OPEN_HOUR,GETDATE()) ELSE ISNULL(ZZ.SESSION_SECONDS, 0) END AS SESSION_SECONDS
             , ISNULL(ZZ.SESSION_SUM, 0) AS SESSION_SUM
             , DATE_TIME              
        FROM   @_DAYS_AND_TABLES DT
   LEFT JOIN   #GT_TEMPORARY_REPORT_DATA ZZ
          ON   (  (@_TIME_INTERVAL = 0 AND DATE_TIME = ZZ.CM_DATE_ONLY)
                           OR (@_TIME_INTERVAL = 1 AND MONTH(DATE_TIME) = MONTH(ZZ.CM_DATE_ONLY) AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
                           OR (@_TIME_INTERVAL = 2 AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
               ) AND TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER
       -- SET ORDER
    ORDER BY   DT.TABLE_TYPE_IDENT ASC
             , CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC
             , DATE_TIME DESC;

    END
   ELSE
    BEGIN
      -- JOIN THE SELECT HIDDING WITHOUT ACTIVITY
      SELECT   DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER
             , DT.TABLE_IDENT_NAME AS TABLE_NAME
             , DT.TABLE_TYPE_IDENT AS TABLE_TYPE
             , DT.TABLE_TYPE_NAME AS TABLE_TYPE_NAME
             , ISNULL(zz.COPY_DEALER_VALIDATED_AMOUNT, 0) AS COPY_DEALER_AMOUNT
             , ISNULL(ZZ.TOTAL_DROP_GAMBLING_TABLE,0) AS TOTAL_DROP_GAMBLING_TABLE 
             , ISNULL(ZZ.TOTAL_DROP_CASHIER,0) AS  TOTAL_DROP_CASHIER
             , ISNULL(ZZ.TOTAL_DROP, 0) AS TOTAL_DROP 
             , ISNULL(ZZ.TOTAL_WIN, 0) AS TOTAL_WIN
             , ISNULL(ZZ.TOTAL_TIP, 0) AS TOTAL_TIP
             , ISNULL(ZZ.WIN_DROP, 0) AS WIN_DROP
             , ISNULL(ZZ.THEORIC_HOLD, 0) AS THEORIC_HOLD
             , ISNULL(ZZ.TIP_DROP, 0) AS TIP_DROP
             , ISNULL(ZZ.WIN_TIPS, 0) AS WIN_TIPS
             , ZZ.OPEN_HOUR
             , ZZ.CLOSE_HOUR
             , CASE WHEN ZZ.CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED
             , CASE WHEN ZZ.SESSION_SECONDS = 0 THEN DATEDIFF(SECOND,ZZ.OPEN_HOUR,GETDATE()) ELSE ISNULL(ZZ.SESSION_SECONDS, 0) END AS SESSION_SECONDS
             , ISNULL(ZZ.SESSION_SUM, 0) AS SESSION_SUM
             , DATE_TIME
        FROM   @_DAYS_AND_TABLES DT
  INNER JOIN   #GT_TEMPORARY_REPORT_DATA ZZ
          ON   (  (@_TIME_INTERVAL = 0 AND DATE_TIME = ZZ.CM_DATE_ONLY)
                           OR (@_TIME_INTERVAL = 1 AND MONTH(DATE_TIME) = MONTH(ZZ.CM_DATE_ONLY) AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
                           OR (@_TIME_INTERVAL = 2 AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
               ) AND TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER
       -- SET ORDER
    ORDER BY   DT.TABLE_TYPE_IDENT ASC
             , CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC
             , DATE_TIME DESC;
    END -- IF ONLY_ACTIVITY
END
ELSE  -- ELSE WITHOUT INTERVALS
BEGIN
   -- FINAL WITHOUT INTERVALS
      -- FILTER ACTIVITY
   IF @_ONLY_ACTIVITY = 0
    BEGIN
            -- JOIN DATA INCLUDING WITHOUT ACTIVITY
      SELECT   DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER
             , DT.TABLE_IDENT_NAME AS TABLE_NAME
             , DT.TABLE_TYPE_IDENT AS TABLE_TYPE
             , DT.TABLE_TYPE_NAME
             , ISNULL(zz.COPY_DEALER_VALIDATED_AMOUNT, 0) AS COPY_DEALER_AMOUNT
             , ISNULL(ZZ.TOTAL_DROP_GAMBLING_TABLE,0) AS TOTAL_DROP_GAMBLING_TABLE
             , ISNULL(ZZ.TOTAL_DROP_CASHIER,0) AS  TOTAL_DROP_CASHIER
             , ISNULL(TOTAL_DROP, 0) AS TOTAL_DROP
             , ISNULL(TOTAL_WIN, 0) AS TOTAL_WIN
             , ISNULL(TOTAL_TIP, 0) AS TOTAL_TIP
             , ISNULL(WIN_DROP, 0) AS WIN_DROP
             , ISNULL(ZZ.THEORIC_HOLD, 0) AS THEORIC_HOLD
             , ISNULL(TIP_DROP, 0) AS TIP_DROP
             , ISNULL(WIN_TIPS, 0) AS WIN_TIPS
             , OPEN_HOUR
             , CLOSE_HOUR
             , CASE WHEN CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED
             , CASE WHEN SESSION_SECONDS = 0 THEN DATEDIFF(SECOND, OPEN_HOUR, GETDATE()) ELSE ISNULL(SESSION_SECONDS, 0) END AS SESSION_SECONDS
             , ISNULL(SESSION_SUM, 0) AS SESSION_SUM
              FROM   @_DAYS_AND_TABLES DT
   LEFT JOIN   #GT_TEMPORARY_REPORT_DATA ZZ ON DT.TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER
    ORDER BY   DT.TABLE_TYPE_IDENT ASC
             , CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC
             , DATE_TIME DESC;

     END
    ELSE
     BEGIN
            -- JOIN DATA WITH ONLY ACTIVITY
       SELECT   DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER
              , DT.TABLE_IDENT_NAME AS TABLE_NAME
              , DT.TABLE_TYPE_IDENT AS TABLE_TYPE
              , DT.TABLE_TYPE_NAME
              , ISNULL(zz.COPY_DEALER_VALIDATED_AMOUNT, 0) AS COPY_DEALER_AMOUNT
              , ISNULL(ZZ.TOTAL_DROP_GAMBLING_TABLE,0) AS TOTAL_DROP_GAMBLING_TABLE
              , ISNULL(ZZ.TOTAL_DROP_CASHIER,0) AS  TOTAL_DROP_CASHIER
              , ISNULL(TOTAL_DROP, 0) AS TOTAL_DROP
              , ISNULL(TOTAL_WIN, 0) AS TOTAL_WIN
              , ISNULL(TOTAL_TIP, 0) AS TOTAL_TIP
              , ISNULL(WIN_DROP, 0) AS WIN_DROP
              , ISNULL(ZZ.THEORIC_HOLD, 0) AS THEORIC_HOLD
              , ISNULL(TIP_DROP, 0) AS TIP_DROP
              , ISNULL(WIN_TIPS, 0) AS WIN_TIPS
              , OPEN_HOUR
              , CLOSE_HOUR
              , CASE WHEN CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED
              , CASE WHEN SESSION_SECONDS = 0 THEN DATEDIFF(SECOND, OPEN_HOUR, GETDATE()) ELSE ISNULL(SESSION_SECONDS, 0) END AS SESSION_SECONDS
              , ISNULL(SESSION_SUM, 0) AS SESSION_SUM
              FROM   @_DAYS_AND_TABLES DT
   INNER JOIN   #GT_TEMPORARY_REPORT_DATA ZZ ON DT.TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER
     ORDER BY   DT.TABLE_TYPE_IDENT ASC
              , CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC
              , DATE_TIME DESC;
     END
 END

-- ERASE THE TEMPORARY DATA
DROP TABLE #GT_TEMPORARY_REPORT_DATA

END -- END PROCEDURE
GO

-- PERMISSIONS
GRANT EXECUTE ON [dbo].[GT_Base_Report_Data] TO [wggui] WITH GRANT OPTION
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ReceptionCustomerVisits]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[ReceptionCustomerVisits]
GO

CREATE PROCEDURE [dbo].[ReceptionCustomerVisits] @pStartDate BIGINT, @pPlayersOnSite BIT
AS BEGIN

DECLARE @_QUERY   NVARCHAR(MAX)

SET @_QUERY = 
  '         SELECT                                                                                                                          
                ACCOUNT_PHOTO.APH_PHOTO                                                                                                
               --,ACCOUNTS.AC_HOLDER_NAME   
               ,ACCOUNTS.AC_HOLDER_NAME

               ,NULL AS AC_HOLDER_NAME12
               
               ,CAST(CUSTOMER_VISITS.CUT_GENDER AS VARCHAR) CUT_GENDER                                                                 
               ,ACCOUNTS.AC_HOLDER_BIRTH_DATE                                                                                          
               ,ADIC_DATA.CUE_DOCUMENT_TYPE                                                                                            
               ,ADIC_DATA.CUE_DOCUMENT_NUMBER                                                                                          
               ,CAST(ACCOUNTS.AC_HOLDER_LEVEL AS VARCHAR)   AC_HOLDER_LEVEL                                                            
               ,ADIC_DATA.ENTRANCES_COUNT                                                                                              
               ,ADIC_DATA.SUM_CUE_TICKET_ENTRY_PRICE_PAID                                                                              
               ,ADIC_DATA.CUE_ENTRANCE_DATETIME
               ,ACCOUNTS.AC_ACCOUNT_ID   
			   ,CUE_EXIT_DATETIME                                                                                                                              
         FROM   CUSTOMER_VISITS                                                                                                        
   INNER JOIN  ACCOUNTS ON     CUSTOMER_VISITS.CUT_CUSTOMER_ID = ACCOUNTS.AC_ACCOUNT_ID                                                
    LEFT JOIN  ACCOUNT_PHOTO ON    CUSTOMER_VISITS.CUT_CUSTOMER_ID = ACCOUNT_PHOTO.APH_ACCOUNT_ID                                      
                                                                                                                                       
                                                                                                                                       
   INNER JOIN                                                                                                                          
               (                                          
                  SELECT                                                                                                               
                           CUSTOMER_ENTRANCES.CUE_VISIT_ID                                                                             
                          ,CAST(CUSTOMER_ENTRANCES.CUE_DOCUMENT_TYPE AS VARCHAR) CUE_DOCUMENT_TYPE                                     
                          ,CUSTOMER_ENTRANCES.CUE_DOCUMENT_NUMBER                                                                      
                          ,CUSTOMER_ENTRANCES.CUE_TICKET_ENTRY_PRICE_PAID                                                              
                          ,Z.ENTRANCES_COUNT                                                                                           
                          ,Z.CUE_ENTRANCE_DATETIME                                                                                     
                          ,Z.SUM_CUE_TICKET_ENTRY_PRICE_PAID  
						  ,CUSTOMER_ENTRANCES.CUE_EXIT_DATETIME 				                                                                      
                    FROM                                                                                                               
                          CUSTOMER_ENTRANCES                                                                                           
              INNER JOIN (                                                                                                             
                           SELECT                                                                                                      
                                    MAX(CUE_ENTRANCE_DATETIME) AS CUE_ENTRANCE_DATETIME                                                
                                   ,CUE_VISIT_ID                                                                                       
                                   ,COUNT(*) AS ENTRANCES_COUNT                                                                        
                                   ,SUM(CUE_TICKET_ENTRY_PRICE_PAID) AS SUM_CUE_TICKET_ENTRY_PRICE_PAID
                             FROM                                                                                                      
                                   CUSTOMER_ENTRANCES                                                                                  
                            WHERE                                                                                                      
                                   CUSTOMER_ENTRANCES.CUE_VISIT_ID IN (                                                                
                                                                       SELECT                                                          
                                                                               CUT_VISIT_ID                                            
                                                                         FROM                                                          
                                                                               CUSTOMER_VISITS                                         
                                                                        WHERE                                                          
                                                                               CUT_GAMING_DAY = ' + CAST(@pStartDate AS VARCHAR(50)) + ' 
                                                          )                                                                
                           GROUP BY CUSTOMER_ENTRANCES.CUE_VISIT_ID                  
                           ) Z                                                                                                         
                      ON CUSTOMER_ENTRANCES.CUE_ENTRANCE_DATETIME = Z.CUE_ENTRANCE_DATETIME                                            
                         AND CUSTOMER_ENTRANCES.CUE_VISIT_ID = Z.CUE_VISIT_ID                                                          
               ) ADIC_DATA                                                                                                             
                                                                                                                                       
           ON CUSTOMER_VISITS.CUT_VISIT_ID = ADIC_DATA.CUE_VISIT_ID                                                                    
                                                                                                                                       
        WHERE  CUT_GAMING_DAY = ' + CAST(@pStartDate AS VARCHAR(50))

		IF (@pPlayersOnSite = 1)
		BEGIN
			SET @_QUERY = @_QUERY + ' AND CUE_EXIT_DATETIME IS NULL '
		END
                                                                                       
        SET @_QUERY = @_QUERY + ' ORDER BY ADIC_DATA.CUE_ENTRANCE_DATETIME '

		EXEC (@_QUERY)

END

GO

GRANT EXECUTE ON RECEPTIONCUSTOMERVISITS TO WGGUI

GO

/****** Object:  StoredProcedure [dbo].[EnableTriggers_By_Service]    Script Date: 02/27/2018 12:57:39 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EnableTriggers_By_Service]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[EnableTriggers_By_Service]
GO

CREATE PROCEDURE [dbo].[EnableTriggers_By_Service] 
 @ServiceName VARCHAR(100)
AS
	BEGIN
	DECLARE @trigger_status_text VARCHAR(100);
	DECLARE @service_exists BIT = 0;
		BEGIN TRY
			SET NOCOUNT ON;
			/* InHouseAPI */
			IF(@ServiceName = 'InHouseAPI')
				BEGIN
					DECLARE @INHOUSEAPI BIT = ISNULL(( SELECT gp_key_value FROM general_params WHERE gp_group_key = 'InHouseAPI' AND gp_subject_key = 'Enabled'), 0);
					SET @service_exists = 1;
					EXEC EnableTriggerByNameAndTable @TableName = N'PLAY_SESSIONS', @ProcName = N'InHouseAPI_Play_Sessions_Insert', @Enable = @INHOUSEAPI
					EXEC EnableTriggerByNameAndTable @TableName = N'CUSTOMER_VISITS', @ProcName = N'InHouseAPI_Customer_Visits_Insert', @Enable = @INHOUSEAPI
					/* Action Performed Message  */
					IF @INHOUSEAPI = 1
						SET @trigger_status_text = ' :: Triggers Enabled';
					ELSE
						SET @trigger_status_text = ' :: Triggers Disabled';
				END
			/* Future Services Added Section ...*/
			/*
			----------------------------------------------------------------------------------------
			ELSE IF(@ServiceName = 'XXX')
				BEGIN
					IF @XXX = 1
						SET @trigger_status_text = ' :: Triggers Enabled';
					ELSE
						SET @trigger_status_text = ' :: Triggers Disabled';				
				END
			----------------------------------------------------------------------------------------
			*/
			IF (@service_exists = 1)
				SELECT @ServiceName + @trigger_status_text;
			ELSE
				SELECT 'ServiceName "' + @ServiceName + '" not found!';
		END TRY
		BEGIN CATCH
			EXEC dbo.spErrorHandling
		END CATCH
	END
GO


-------------  Update_Meters_SalesPerHour.sql


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Update_Meters_SalesPerHour]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Update_Meters_SalesPerHour]
GO

CREATE PROCEDURE [dbo].[Update_Meters_SalesPerHour]
      @pBaseHour                  DATETIME 
,     @pTerminalId                INT
,     @pGameId                    INT
,     @pTerminalDefaultPayout     MONEY
,     @pSPH_Played_Count          BIGINT
,     @pSPH_Played_Amount         MONEY
,     @pSPH_Won_Count             BIGINT
,     @pSPH_Won_Amount            MONEY
,     @pSPH_Jackpot_Amount        MONEY

AS
BEGIN 

  DECLARE @pTerminalName AS NVARCHAR(50)
  DECLARE @pGameName AS NVARCHAR(50)
  DECLARE @payout AS MONEY 

  IF @pGameId = 0
  BEGIN
    -- Get game id from sales per hour
    SELECT   @pGameId = ISNULL(MIN(SPH_GAME_ID), 0)
      FROM   SALES_PER_HOUR
     WHERE   SPH_BASE_HOUR >= dbo.Opening(0, @pBaseHour)
       AND   SPH_BASE_HOUR < DATEADD(DAY, 1, dbo.Opening(0, @pBaseHour))
       AND   SPH_TERMINAL_ID = @pTerminalId
     
    IF @pGameId = 0
    BEGIN
      -- Get game id from terminals
      SELECT   @pGameId = ISNULL(TE_LAST_GAME_PLAYED_ID, 0)
        FROM   TERMINALS
       WHERE   TE_TERMINAL_ID = @pTerminalId
    END
    
    IF @pGameId = 0
    BEGIN
      -- Get game id from terminal game translation
      SELECT   @pGameId = ISNULL(TGT_SOURCE_GAME_ID, 0)
        FROM   TERMINAL_GAME_TRANSLATION
       WHERE   TGT_TERMINAL_ID = @pTerminalId
    END
  END
  
  IF @pGameId = 0
  BEGIN
    RAISERROR (N'Terminal without activity can not be adjusted', 16, 10)
    RETURN
  END

  -- Get terminal name 
  SELECT   @pTerminalName = TE_NAME
         , @payout = ISNULL(TE_THEORETICAL_PAYOUT, @pTerminalDefaultPayout) 
    FROM   TERMINALS
   WHERE   TE_TERMINAL_ID = @pTerminalId

  IF NOT EXISTS ( SELECT   *
                    FROM   SALES_PER_HOUR
                   WHERE   SPH_BASE_HOUR = @pBaseHour
                     AND   SPH_TERMINAL_ID = @pTerminalId
                     AND   SPH_GAME_ID = @pGameId)
  BEGIN

    -- Get game name
    SELECT   @pGameName = GM_NAME
      FROM   GAMES
     WHERE   GM_GAME_ID = @pGameId   
     
    INSERT INTO SALES_PER_HOUR
               ( SPH_BASE_HOUR
               , SPH_TERMINAL_ID
               , SPH_TERMINAL_NAME
               , SPH_GAME_ID
               , SPH_GAME_NAME
               , SPH_PLAYED_COUNT
               , SPH_PLAYED_AMOUNT
               , SPH_WON_COUNT
               , SPH_WON_AMOUNT
               , SPH_NUM_ACTIVE_TERMINALS
               , SPH_LAST_PLAY_ID
               , SPH_THEORETICAL_WON_AMOUNT
               , SPH_JACKPOT_AMOUNT
               , SPH_PROGRESSIVE_JACKPOT_AMOUNT
               , SPH_PROGRESSIVE_JACKPOT_AMOUNT_0
               , SPH_PROGRESSIVE_PROVISION_AMOUNT)
         VALUES
               ( @pBaseHour
               , @pTerminalId
               , @pTerminalName
               , @pGameId
               , @pGameName
               , ISNULL(@pSPH_Played_Count, 0)
               , ISNULL(@pSPH_Played_Amount, 0)
               , ISNULL(@pSPH_Won_Count, 0)
               , ISNULL(@pSPH_Won_Amount, 0)
               , 0
               , 0
               , ISNULL(@pSPH_Played_Amount, 0) * @payout
               , ISNULL(@pSPH_Jackpot_Amount, 0)
               , 0
               , 0
               , 0)

  END
  ELSE
  BEGIN

    UPDATE   SALES_PER_HOUR
       SET   SPH_PLAYED_COUNT   = ISNULL(@pSPH_Played_Count, SPH_PLAYED_COUNT)
           , SPH_PLAYED_AMOUNT  = ISNULL(@pSPH_Played_Amount, SPH_PLAYED_AMOUNT)
           , SPH_WON_COUNT      = ISNULL(@pSPH_Won_Count, SPH_WON_COUNT)
           , SPH_WON_AMOUNT     = ISNULL(@pSPH_Won_Amount, SPH_WON_AMOUNT)
           , SPH_JACKPOT_AMOUNT = ISNULL(@pSPH_Jackpot_Amount, SPH_JACKPOT_AMOUNT)
           , SPH_THEORETICAL_WON_AMOUNT = CASE WHEN ISNULL(SPH_PLAYED_AMOUNT, 0) = 0
                                               THEN ISNULL(@pSPH_Played_Amount, SPH_PLAYED_AMOUNT) * @payout
                                               ELSE ISNULL(@pSPH_Played_Amount, SPH_PLAYED_AMOUNT) * (SPH_THEORETICAL_WON_AMOUNT / SPH_PLAYED_AMOUNT)
                                                END
     WHERE   SPH_BASE_HOUR = @pBaseHour
       AND   SPH_TERMINAL_ID = @pTerminalId
       AND   SPH_GAME_ID = @pGameId

  END
            
END 
GO

GRANT EXECUTE ON [dbo].[Update_Meters_SalesPerHour] TO [wggui] WITH GRANT OPTION
GO


/******* TRIGGERS *******/


/**** GENERAL PARAM *****/
IF NOT EXISTS(SELECT   1 
                FROM   GENERAL_PARAMS 
               WHERE   GP_GROUP_KEY = 'InHouseAPI'
                 AND   GP_SUBJECT_KEY = 'Enabled'
             )
BEGIN
    INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
    VALUES ('InHouseAPI', 'Enabled', '0')
END
GO

-- Get if Reception is enabled
DECLARE @ReceptionVisitEnabledValue  as nvarchar(50)

SELECT   @ReceptionVisitEnabledValue = MIN(gp_key_value)
  FROM   GENERAL_PARAMS 
 WHERE   1 = 1 
   AND   GP_GROUP_KEY = 'Reception'
   AND   GP_SUBJECT_KEY = 'Enabled'

--ReceptionVisit is enabled if Reception is enabled.
set @ReceptionVisitEnabledValue = ISNULL(@ReceptionVisitEnabledValue, '0');

--PlaySession is enabled if Reception is NOT enabled.
DECLARE @PlaySessionEnabledValue    AS nvarchar(50)
IF (@ReceptionVisitEnabledValue = '0')
    SET @PlaySessionEnabledValue = '1'
ELSE
    SET @PlaySessionEnabledValue = '0'

--Reception
IF NOT EXISTS(SELECT   1 
                FROM   GENERAL_PARAMS 
               WHERE   GP_GROUP_KEY = 'InHouseAPI'
                 AND   GP_SUBJECT_KEY = 'ReceptionVisitEnabled'
             )
BEGIN
    INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
    VALUES ('InHouseAPI', 'ReceptionVisitEnabled', @ReceptionVisitEnabledValue)
END

--Play Session
IF NOT EXISTS(SELECT   1 
                FROM   GENERAL_PARAMS 
               WHERE   GP_GROUP_KEY = 'InHouseAPI'
                 AND   GP_SUBJECT_KEY = 'PlaySessionEnabled'
             )
BEGIN
    INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
    VALUES ('InHouseAPI', 'PlaySessionEnabled', @PlaySessionEnabledValue)
END

--AskChildsInterval (in seconds)
IF NOT EXISTS(SELECT   1 
                FROM   GENERAL_PARAMS 
               WHERE   GP_GROUP_KEY = 'InHouseAPI'
                 AND   GP_SUBJECT_KEY = 'AskChildsInterval'
             )
BEGIN
    INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
    VALUES ('InHouseAPI', 'AskChildsInterval', '300')
END
GO

-- DeleteThreshold (in hours)
IF NOT EXISTS(SELECT   1 
                FROM   GENERAL_PARAMS 
               WHERE   GP_GROUP_KEY = 'InHouseAPI'
                 AND   GP_SUBJECT_KEY = 'DeleteThreshold'
             )
BEGIN
    INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
    VALUES ('InHouseAPI', 'DeleteThreshold', '48')
END
GO


/**** TABLES *****/
-- CREATE TABLE TODAY_VISITS
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[today_visits]') AND type in (N'U'))
BEGIN
    CREATE TABLE [dbo].[today_visits]
    (
        tv_site_id                  INT                     NOT NULL,
        tv_gaming_day               INT                     NOT NULL,
        tv_customer_id              BIGINT                  NOT NULL,
        CONSTRAINT [PK_today_visits] PRIMARY KEY CLUSTERED
        (
            tv_site_id, tv_gaming_day, tv_customer_id
        ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
    ) ON [PRIMARY]
END
GO

-- ALTER TABLE ACCOUNTS_PHOTO
IF NOT EXISTS( SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'account_photo' AND COLUMN_NAME = 'aph_last_update') 
BEGIN
	ALTER TABLE		[dbo].[account_photo] 
	ADD 			aph_last_update 	DATETIME DEFAULT GETDATE() NOT NULL
END
GO

-- CREATE TABLE IN_HOUSE_EVENTS
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[in_house_events]') AND type in (N'U'))
BEGIN
    CREATE TABLE [dbo].[in_house_events]
    (
        ihe_event_id                        BIGINT  IDENTITY(1, 1)  NOT NULL,
        ihe_site_id                         INT                     NOT NULL,
        ihe_event_inserted_in_local_time    DATETIME                NOT NULL,
        ihe_event_inserted_in_utc_time      DATETIME                NOT NULL,
        ihe_event_datetime_in_local_time    DATETIME                NOT NULL,
        ihe_event_datetime_in_utc_time      DATETIME                NOT NULL,
        ihe_event_type                      INT                     NOT NULL,
        ihe_customer_id                     BIGINT                  NOT NULL,
        ihe_egm_id                          INT                     NULL,
        CONSTRAINT [PK_in_house_events] PRIMARY KEY CLUSTERED
        (
            ihe_event_id
        ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
    ) ON [PRIMARY]
END
GO

-- ALTER TABLE SITE
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'site' AND COLUMN_NAME = 'st_inhouse_api_url') 
BEGIN
	  ALTER TABLE		[dbo].[site] 
	  ADD				st_inhouse_api_url		NVARCHAR(255)		NULL
END
GO

IF NOT EXISTS( SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'accounts' AND COLUMN_NAME = 'ac_last_update_in_local_time')
BEGIN
    ALTER TABLE     [dbo].[accounts]    ADD     ac_last_update_in_local_time    DATETIME    DEFAULT GETDATE()   NOT NULL
END
GO

IF NOT EXISTS( SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'accounts' AND COLUMN_NAME = 'ac_last_update_in_utc_time')
BEGIN
    ALTER TABLE     [dbo].[accounts]    ADD     ac_last_update_in_utc_time  DATETIME    DEFAULT GETUTCDATE()   NOT NULL
END
GO

/**** RECORDS *****/



/**** STORED PROCEDURES *****/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AccountPointsCache_CalculatePoints]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[AccountPointsCache_CalculatePoints]
GO

CREATE PROCEDURE [dbo].[AccountPointsCache_CalculatePoints]
       @AccountId                        BIGINT,
       @DateFrom                         DATETIME,
       @DateTo                           DATETIME,
       @MovementId                       BIGINT, 
       @LastMovementId                   BIGINT OUTPUT, 
       @PointsGeneratedForLevel          MONEY  OUTPUT,
       @PointsDiscretionalForLevel       MONEY  OUTPUT,
       @PointsDiscretionalOnlyForRedeem  MONEY  OUTPUT,
       @PointsPromotionOnlyForRedeem     MONEY  OUTPUT
AS
BEGIN
  DECLARE @points_awarded                        AS INT;
  DECLARE @manually_added_points_for_level       AS INT;
  DECLARE @imported_points_for_level             AS INT;
  DECLARE @imported_points_history               AS INT;
  DECLARE @manually_added_points_only_for_redeem AS INT;
  DECLARE @imported_points_only_for_redeem       AS INT;
  DECLARE @promotion_point                       AS INT;
  DECLARE @cancel_promotion_point                AS INT;
  -- 11-FEB-2016 JRC    PBI 7909: Multiple Buckets. 
  DECLARE @EndSession_RankingLevelPoints		AS INT;	
  DECLARE @Expired_RankingLevelPoints		    AS INT
  DECLARE @Manual_Add_RankingLevelPoints		AS INT
  DECLARE @Manual_Sub_RankingLevelPoints		AS INT
  DECLARE @Manual_Set_RankingLevelPoints		AS INT
  DECLARE @EndSession_RedemptionPoints			AS INT
  DECLARE @Expired_RedemptionPoints				AS INT
  DECLARE @Manual_Add_RedemptionPoints			AS INT
  DECLARE @Manual_Sub_RedemptionPoints			AS INT
  DECLARE @Manual_Set_RedemptionPoints			AS INT
  


  DECLARE @Sql             AS NVARCHAR(MAX);
  DECLARE @ParamDefinition AS NVARCHAR(MAX);
  DECLARE @Index           AS NVARCHAR(MAX);
  DECLARE @Where           AS NVARCHAR(MAX); 
  
  -- Points for level
     -- Generated
  SET @points_awarded                        = 36
  --PointsAwarded
  
     -- Discretional
  
  SET @manually_added_points_for_level       = 68
  --ManuallyAddedPointsForLevel
  
  SET @imported_points_for_level             = 72
  --ImportedPointsForLevel

  SET @imported_points_history               = 73
  --ImportedPointsHistory

  -- Points only for redeem
     -- Discretional 
  SET @manually_added_points_only_for_redeem = 50
  --ManuallyAddedPointsOnlyForRedeem

  SET @imported_points_only_for_redeem       = 71
  --ImportedPointsOnlyForRedeem

     -- Promotion 
  SET @promotion_point                       = 60
  --PromotionPoint
  
  SET @cancel_promotion_point                = 61
  --CancelPromotionPoint 

  SET @EndSession_RankingLevelPoints		     = 1102
  --'MULTIPLE_BUCKETS_END_SESSION + RankingLevelPoints PuntosNivel'
  SET @Expired_RankingLevelPoints		         = 1202
  --'MULTIPLE_BUCKETS_Expired + RankingLevelPoints PuntosNivel'
  SET @Manual_Add_RankingLevelPoints		     = 1302
  --'MULTIPLE_BUCKETS_Manual_Add + RankingLevelPoints PuntosNivel'
  SET @Manual_Sub_RankingLevelPoints		     = 1402
  --'MULTIPLE_BUCKETS_Manual_Sub + RankingLevelPoints PuntosNivel'
  SET @Manual_Set_RankingLevelPoints		     = 1502
  --'MULTIPLE_BUCKETS_Manual_Set + RankingLevelPoints PuntosNivel'


  SET @EndSession_RedemptionPoints		     = 1101
  --'MULTIPLE_BUCKETS_END_SESSION + RedemptionPoints PuntosCanje'
  SET @Expired_RedemptionPoints		         = 1201
  --'MULTIPLE_BUCKETS_Expired + RedemptionPoints PuntosCanje'
  SET @Manual_Add_RedemptionPoints		     = 1301
  --'MULTIPLE_BUCKETS_Manual_Add + RedemptionPoints PuntosCanje'
  SET @Manual_Sub_RedemptionPoints		     = 1401
  --'MULTIPLE_BUCKETS_Manual_Sub + RedemptionPoints PuntosCanje'
  SET @Manual_Set_RedemptionPoints		     = 1501
  --'MULTIPLE_BUCKETS_Manual_Set + RedemptionPoints PuntosCanje'




  
  SET @LastMovementId = @MovementId

    SET @Index = 'IX_am_account_id_type_datetime'
    SET @Where = ' WHERE AM_ACCOUNT_ID  = @AccountId ' +
                 '   AND AM_TYPE IN (@points_awarded, @manually_added_points_for_level, @imported_points_for_level, @imported_points_history, ' +
				 '                   @EndSession_RankingLevelPoints, @Expired_RankingLevelPoints, @Manual_Add_RankingLevelPoints, @Manual_Sub_RankingLevelPoints, @Manual_Set_RankingLevelPoints,' +
				 '                   @EndSession_RedemptionPoints, @Expired_RedemptionPoints, @Manual_Add_RedemptionPoints, @Manual_Sub_RedemptionPoints, @Manual_Set_RedemptionPoints,' +
                 '                   @manually_added_points_only_for_redeem, @imported_points_only_for_redeem, @promotion_point, @cancel_promotion_point) ' + 
                 '   AND AM_DATETIME >= CAST(''' + CAST(@DateFrom AS VARCHAR(50)) + ''' AS DATETIME) ' +
                 '   AND AM_DATETIME  < CAST(''' + CAST(@DateTo AS VARCHAR(50)) + ''' AS DATETIME) ' +
                 '   AND AM_MOVEMENT_ID > ' + CAST(@MovementId AS NVARCHAR(MAX)) 

  SET @Sql = '
  SELECT   @PointsGeneratedForLevel_out          = ISNULL(SUM (CASE WHEN AM_TYPE IN (@points_awarded,@EndSession_RankingLevelPoints, @Expired_RankingLevelPoints)
                                                               THEN AM_ADD_AMOUNT-AM_SUB_AMOUNT 
                                                               ELSE 0 
                                                               END ), 0) 
         , @PointsDiscretionalForLevel_out       = ISNULL(SUM (CASE WHEN AM_TYPE IN (@manually_added_points_for_level, @imported_points_for_level, @imported_points_history, @Manual_Add_RankingLevelPoints, @Manual_Sub_RankingLevelPoints, @Manual_Set_RankingLevelPoints)
                                                               THEN AM_ADD_AMOUNT-AM_SUB_AMOUNT 
                                                               ELSE 0 
                                                               END ), 0) 
         , @PointsDiscretionalOnlyForRedeem_out  = ISNULL(SUM (CASE WHEN AM_TYPE IN (@manually_added_points_only_for_redeem, @imported_points_only_for_redeem, @Manual_Add_RedemptionPoints, @Manual_Sub_RedemptionPoints, @Manual_Set_RedemptionPoints)
                                                               THEN AM_ADD_AMOUNT-AM_SUB_AMOUNT 
                                                               ELSE 0 
                                                               END ), 0)
         , @PointsPromotionOnlyForRedeem_out     = ISNULL(SUM (CASE WHEN AM_TYPE IN (@promotion_point, @cancel_promotion_point,@EndSession_RedemptionPoints, @Expired_RedemptionPoints)
                                                               THEN AM_ADD_AMOUNT-AM_SUB_AMOUNT
                                                               ELSE 0 
                                                               END ), 0) 
         , @LastMovementId_out                   = ISNULL(MAX (AM_MOVEMENT_ID), @LastMovementId_out)
    FROM   ACCOUNT_MOVEMENTS WITH (INDEX (' + @Index + ')) ' 
  + @Where
 
SET @ParamDefinition = N'@AccountId                             BIGINT, 
                        @points_awarded                         INT, 
                        @manually_added_points_for_level        INT, 
                        @imported_points_for_level              INT, 
                        @imported_points_history                INT, 
                        @manually_added_points_only_for_redeem  INT, 
                        @imported_points_only_for_redeem        INT, 
                        @promotion_point                        INT, 
                        @cancel_promotion_point                 INT, 
						@EndSession_RankingLevelPoints			INT,
						@Expired_RankingLevelPoints				INT,	
						@Manual_Add_RankingLevelPoints			INT,
						@Manual_Sub_RankingLevelPoints			INT,
						@Manual_Set_RankingLevelPoints			INT,
						@EndSession_RedemptionPoints			INT,	
						@Expired_RedemptionPoints				INT,	
						@Manual_Add_RedemptionPoints			INT,	
						@Manual_Sub_RedemptionPoints			INT,	
						@Manual_Set_RedemptionPoints			INT,	
                        @LastMovementId_out                     BIGINT OUTPUT, 
                        @PointsGeneratedForLevel_out            MONEY  OUTPUT, 
                        @PointsDiscretionalForLevel_out         MONEY  OUTPUT, 
                        @PointsDiscretionalOnlyForRedeem_out    MONEY  OUTPUT, 
                        @PointsPromotionOnlyForRedeem_out       MONEY  OUTPUT' 

EXEC sp_executesql @Sql, 
					@ParamDefinition,
					@AccountId                             = @AccountId, 
					@points_awarded                        = @points_awarded,                        
					@manually_added_points_for_level       = @manually_added_points_for_level,
					@imported_points_for_level             = @imported_points_for_level,
					@imported_points_history               = @imported_points_history, 
					@manually_added_points_only_for_redeem = @manually_added_points_only_for_redeem, 
					@imported_points_only_for_redeem       = @imported_points_only_for_redeem, 
					@promotion_point                       = @promotion_point,
					@cancel_promotion_point                = @cancel_promotion_point,
					@EndSession_RankingLevelPoints    =   @EndSession_RankingLevelPoints, 
					@Expired_RankingLevelPoints		  =   @Expired_RankingLevelPoints		,
					@Manual_Add_RankingLevelPoints    =   @Manual_Add_RankingLevelPoints ,
					@Manual_Sub_RankingLevelPoints    =   @Manual_Sub_RankingLevelPoints ,
					@Manual_Set_RankingLevelPoints    =   @Manual_Set_RankingLevelPoints ,
					@EndSession_RedemptionPoints	  =   @EndSession_RedemptionPoints	,
					@Expired_RedemptionPoints		  =   @Expired_RedemptionPoints		,
					@Manual_Add_RedemptionPoints	  =   @Manual_Add_RedemptionPoints	,
					@Manual_Sub_RedemptionPoints	  =   @Manual_Sub_RedemptionPoints	,
					@Manual_Set_RedemptionPoints	  =   @Manual_Set_RedemptionPoints	,
					@LastMovementId_out                    = @LastMovementId                   OUTPUT, 
					@PointsGeneratedForLevel_out           = @PointsGeneratedForLevel          OUTPUT,
					@PointsDiscretionalForLevel_out        = @PointsDiscretionalForLevel       OUTPUT,
					@PointsDiscretionalOnlyForRedeem_out   = @PointsDiscretionalOnlyForRedeem  OUTPUT,
					@PointsPromotionOnlyForRedeem_out      = @PointsPromotionOnlyForRedeem     OUTPUT 

---------------------------------------------------------------------------------------------------------------------------------
-- Note:
-- @LastMovementId_out : Is the variable�s name of the output parameter of '@Sql execute'
-- When run 'EXEC sp_executesql' is finished, the value of @LastMovementId_out is assigned to @LastMovementId
---------------------------------------------------------------------------------------------------------------------------------

END  -- PROCEDURE [dbo].[AccountPointsCache_CalculatePoints]
GO

--------------------------------------------------------------------------------
-- PURPOSE: Save AccountPoints for level in DB (for simulate a cache)
-- 
--  PARAMS:
--      - INPUT:
--           @AccountId       BIGINT       
--
--      - OUTPUT:
--
-- RETURNS:
--
--   NOTES:
--------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AccountPointsCache_CalculateToday]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[AccountPointsCache_CalculateToday]
GO

CREATE PROCEDURE [dbo].[AccountPointsCache_CalculateToday]
       @AccountId     BIGINT
AS
BEGIN

  DECLARE @update_history                                  AS BIT;
  
  DECLARE @apc_today                                       AS DATETIME;
  DECLARE @apc_days                                        AS INT;
	DECLARE @apc_history_points_generated_for_level          AS MONEY;
	DECLARE @apc_history_points_discretional_for_level       AS MONEY;
	DECLARE @apc_history_points_discretional_only_for_redeem AS MONEY;
	DECLARE @apc_history_points_promotion_only_for_redeem    AS MONEY;
	DECLARE @apc_today_points_generated_for_level            AS MONEY;
	DECLARE @apc_today_points_discretional_for_level         AS MONEY;
	DECLARE @apc_today_points_discretional_only_for_redeem   AS MONEY;
	DECLARE @apc_today_points_promotion_only_for_redeem      AS MONEY;
  DECLARE @apc_today_last_movement_id                      AS BIGINT;
  
  DECLARE @estudy_period                                   AS INT;
  DECLARE @date_from                                       AS DATETIME;
  DECLARE @now                                             AS DATETIME;
  DECLARE @today_opening                                   AS DATETIME;
  
  SET @update_history = 0;
  
  -- Get register from ACCOUNT_POINTS_CACHE
  SELECT   @apc_today                  = APC_TODAY 
         , @apc_days                   = APC_DAYS
         , @apc_today_last_movement_id = APC_TODAY_LAST_MOVEMENT_ID
    FROM   ACCOUNT_POINTS_CACHE 
   WHERE   APC_ACCOUNT_ID = @AccountId 
     
  -- GET General param: "PlayerTracking", "Levels.DaysCountingPoints"
  SET @estudy_period = (SELECT CAST(GP_KEY_VALUE AS INT) FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PlayerTracking' AND GP_SUBJECT_KEY = 'Levels.DaysCountingPoints')
     
  --_day_start_counting_points = Misc.TodayOpening().AddDays(-(_days_counting_points - 1));
  SET @now = GETDATE();
  SET @today_opening = dbo.Opening(0, @now)
  SET @date_from = DATEADD(DAY, -1*(@estudy_period-1), @today_opening);
    
  IF (   @today_opening <> ISNULL(@apc_today, DATEADD(d, -1, GETDATE()))   --> no hay registro o cambio de jornada.
      OR @estudy_period <> ISNULL(@apc_days, -1) )                         --> no hay registro o cambio en el periodo de estudio.
  BEGIN
    SET @update_history = 1;
  END   
    
  IF (@update_history = 1)
  BEGIN
    SET @apc_today_last_movement_id = 0;
    
    -- Get history data 
    EXEC AccountPointsCache_CalculatePoints @AccountId, @date_from, @today_opening, 
                                            @apc_today_last_movement_id, 
                                            @apc_today_last_movement_id                      OUTPUT, 
                                            @apc_history_points_generated_for_level          OUTPUT, 
                                            @apc_history_points_discretional_for_level       OUTPUT, 
                                            @apc_history_points_discretional_only_for_redeem OUTPUT, 
                                            @apc_history_points_promotion_only_for_redeem    OUTPUT
    --> Get today data
    EXEC AccountPointsCache_CalculatePoints @AccountId, @today_opening, @now, 
                                            @apc_today_last_movement_id, 
                                            @apc_today_last_movement_id                    OUTPUT, 
                                            @apc_today_points_generated_for_level          OUTPUT, 
                                            @apc_today_points_discretional_for_level       OUTPUT, 
                                            @apc_today_points_discretional_only_for_redeem OUTPUT, 
                                            @apc_today_points_promotion_only_for_redeem    OUTPUT
  
    DELETE  FROM ACCOUNT_POINTS_CACHE 
     WHERE  APC_ACCOUNT_ID = @AccountId 
   
    INSERT INTO ACCOUNT_POINTS_CACHE ( APC_ACCOUNT_ID, 
                                       APC_DAYS,       
                                       APC_HISTORY_POINTS_GENERATED_FOR_LEVEL,          
                                       APC_HISTORY_POINTS_DISCRETIONAL_FOR_LEVEL,
                                       APC_HISTORY_POINTS_DISCRETIONAL_ONLY_FOR_REDEEM,
                                       APC_HISTORY_POINTS_PROMOTION_ONLY_FOR_REDEEM,
                                       APC_TODAY, 
                                       APC_TODAY_POINTS_GENERATED_FOR_LEVEL,
                                       APC_TODAY_POINTS_DISCRETIONAL_FOR_LEVEL, 
                                       APC_TODAY_POINTS_DISCRETIONAL_ONLY_FOR_REDEEM, 
                                       APC_TODAY_POINTS_PROMOTION_ONLY_FOR_REDEEM,
                                       APC_TODAY_LAST_MOVEMENT_ID, 
                                       APC_TODAY_LAST_UPDATED ) 
                              VALUES ( @AccountId,     
                                       @estudy_period, 
                                       @apc_history_points_generated_for_level,              
                                       @apc_history_points_discretional_for_level, 
                                       @apc_history_points_discretional_only_for_redeem,     
                                       @apc_history_points_promotion_only_for_redeem,
                                       @today_opening, 
                                       @apc_today_points_generated_for_level, 
                                       @apc_today_points_discretional_for_level, 
                                       @apc_today_points_discretional_only_for_redeem, 
                                       @apc_today_points_promotion_only_for_redeem, 
                                       @apc_today_last_movement_id, 
                                       @now )  
  END
  ELSE
  BEGIN
    --> Get today data
    EXEC AccountPointsCache_CalculatePoints @AccountId, @today_opening, @now, 
                                            @apc_today_last_movement_id, 
                                            @apc_today_last_movement_id                    OUTPUT, 
                                            @apc_today_points_generated_for_level          OUTPUT, 
                                            @apc_today_points_discretional_for_level       OUTPUT, 
                                            @apc_today_points_discretional_only_for_redeem OUTPUT, 
                                            @apc_today_points_promotion_only_for_redeem    OUTPUT
    
    --update today
    UPDATE   ACCOUNT_POINTS_CACHE  
       SET   APC_TODAY                                     = @today_opening
           , APC_TODAY_POINTS_GENERATED_FOR_LEVEL          = APC_TODAY_POINTS_GENERATED_FOR_LEVEL          + @apc_today_points_generated_for_level         
           , APC_TODAY_POINTS_DISCRETIONAL_FOR_LEVEL       = APC_TODAY_POINTS_DISCRETIONAL_FOR_LEVEL       + @apc_today_points_discretional_for_level      
           , APC_TODAY_POINTS_DISCRETIONAL_ONLY_FOR_REDEEM = APC_TODAY_POINTS_DISCRETIONAL_ONLY_FOR_REDEEM + @apc_today_points_discretional_only_for_redeem
           , APC_TODAY_POINTS_PROMOTION_ONLY_FOR_REDEEM    = APC_TODAY_POINTS_PROMOTION_ONLY_FOR_REDEEM    + @apc_today_points_promotion_only_for_redeem   
           , APC_TODAY_LAST_MOVEMENT_ID                    = @apc_today_last_movement_id 
           , APC_TODAY_LAST_UPDATED                        = @now
     WHERE   APC_ACCOUNT_ID = @AccountId
     
  END
    
END  -- PROCEDURE [dbo].[AccountPointsCache_CalculateToday]
GO  

--------------------------------------------------------------------------------
-- PURPOSE: Return Accounts & AccountPointsCache data for account
-- 
--  PARAMS:
--      - INPUT:
--           @AccountId       BIGINT 
--
--      - OUTPUT:
--           AC_HOLDER_NAME 
--           AC_HOLDER_GENDER 
--           AC_HOLDER_BIRTH_DATE 
--           AC_BALANCE 
--           AC_POINTS 
--           AC_CURRENT_HOLDER_LEVEL 
--           AC_HOLDER_LEVEL_EXPIRATION 
--           AC_HOLDER_LEVEL_ENTERED 
--           AC_RE_BALANCE 
--           AC_PROMO_RE_BALANCE 
--           AC_PROMO_NR_BALANCE 
--           AC_CURRENT_PLAY_SESSION_ID 
--           AC_CURRENT_TERMINAL_NAME 
--           AM_POINTS_GENERATED 
--           AM_POINTS_DISCRETIONARIES 
--           AM_POINTS_PROMO_DISCRETIONARIES 
--           AM_POINTS_PROMO_ONLY_FOR_REDEEM 
--           APC_TODAY_LAST_MOVEMENT_ID 
--           BU_NR_BALANCE
--           BU_RE_BALANCE
-- RETURNS:
--
--   NOTES:
--------------------------------------------------------------------------------


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetAccountPointsCache]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[GetAccountPointsCache]
GO

CREATE PROCEDURE [dbo].[GetAccountPointsCache] @pAccountId bigint
AS
BEGIN

  --EXEC AccountPointsCache_CalculateToday @pAccountId

  DECLARE @BucketPuntosCanje Int
  DECLARE @BucketNR Int 
  DECLARE @BucketRE Int
  DECLARE @BucketPuntosCanjeGenerados Int
  DECLARE @BucketPuntosCanjeDiscrecionales Int

    
  SET @BucketPuntosCanje = 1
  SET @BucketPuntosCanjeGenerados = 8
  SET @BucketPuntosCanjeDiscrecionales = 9
  
  --Realizar select ACCOUNT_POINTS_CACHE con inner join a ACCOUNTS. -- select similar al que se hace en WKT_Player.cs funci�n Read(..)
  SELECT   ISNULL(AC_HOLDER_NAME, '')             AS AC_HOLDER_NAME 
         , ISNULL(AC_HOLDER_GENDER, 0)            AS AC_HOLDER_GENDER 
         , AC_HOLDER_BIRTH_DATE 
         , AC_BALANCE 
         , AC_HOLDER_LEVEL                        AS AC_CURRENT_HOLDER_LEVEL
         , AC_HOLDER_LEVEL_EXPIRATION 
         , AC_HOLDER_LEVEL_ENTERED 
         , AC_RE_BALANCE 
         , AC_PROMO_RE_BALANCE 
         , AC_PROMO_NR_BALANCE 
				 , AC_IN_SESSION_RE_BALANCE   
				 , AC_PIN
				 , AC_IN_SESSION_PROMO_RE_BALANCE
				 , AC_IN_SESSION_PROMO_NR_BALANCE
         , ISNULL (AC_CURRENT_PLAY_SESSION_ID, 0) AS AC_CURRENT_PLAY_SESSION_ID 
         , AC_CURRENT_TERMINAL_NAME 
		     , ISNULL(AC_POINTS.CBU_VALUE                      ,0) AS  AC_POINTS                 
		     , ISNULL(AM_POINTS_GENERATED.CBU_VALUE      	  ,0) AS  AM_POINTS_GENERATED       
		     , ISNULL(AM_POINTS_DISCRETIONARIES.CBU_VALUE	  ,0) AS  AM_POINTS_DISCRETIONARIES 
         , AC_PIN_FAILURES
         , AC_TRACK_DATA 
	 , AC_HOLDER_NAME3
    FROM   ACCOUNTS 
      LEFT JOIN CUSTOMER_BUCKET AC_POINTS ON AC_POINTS.CBU_CUSTOMER_ID = AC_ACCOUNT_ID AND AC_POINTS.CBU_BUCKET_ID = 1 /* REDEMPTIONPOINTS */
			LEFT JOIN CUSTOMER_BUCKET AC_LEVEL_POINTS ON AC_LEVEL_POINTS.CBU_CUSTOMER_ID = AC_ACCOUNT_ID AND AC_LEVEL_POINTS.CBU_BUCKET_ID = 2 /* LEVELPOINTS */  
      LEFT JOIN CUSTOMER_BUCKET AM_POINTS_GENERATED ON AM_POINTS_GENERATED.CBU_CUSTOMER_ID = AC_ACCOUNT_ID AND AM_POINTS_GENERATED.CBU_BUCKET_ID = 8 /* RANKINGLEVELPOINTS_GENERATED */
      LEFT JOIN CUSTOMER_BUCKET AM_POINTS_DISCRETIONARIES ON AM_POINTS_DISCRETIONARIES.CBU_CUSTOMER_ID = AC_ACCOUNT_ID AND AM_POINTS_DISCRETIONARIES.CBU_BUCKET_ID = 9 /* RANKINGLEVELPOINTS_DISCRETIONAL */
   WHERE   AC_ACCOUNT_ID = @pAccountId; 
END

GO

GRANT EXECUTE ON [dbo].[GetAccountPointsCache] TO [wggui] WITH GRANT OPTION
GO                          

-- Alter PROMOTIONS Table
IF NOT EXISTS( SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'promotions' AND COLUMN_NAME = 'pm_journey_limit')
BEGIN
  ALTER TABLE [dbo].[promotions] ADD [pm_journey_limit] bit NOT NULL DEFAULT(0)
END
GO  

-- Alter PROMOTIONS Table
IF NOT EXISTS( SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'promotions' AND COLUMN_NAME = 'pm_pyramidal_dist')
BEGIN
  ALTER TABLE dbo.promotions ADD pm_pyramidal_dist XML NULL CONSTRAINT DF_promotions_pm_pyramidal_dist DEFAULT NULL
END
GO   

-- Add PyramidalDistributionReward General Param
IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='Features' AND GP_SUBJECT_KEY = 'PyramidalDistributionReward')
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Features', 'PyramidalDistributionReward', '0')
GO

-- Create table PROMOGAME_TYPE_PLAY_SESSION
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS( SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'promogame_type_play_session')
BEGIN
CREATE TABLE [dbo].[promogame_type_play_session](
	[ptp_playsession_id] [bigint] NOT NULL,
	[ptp_gametype] [bigint] NOT NULL,
 CONSTRAINT [PK_promogametype_playsession] PRIMARY KEY CLUSTERED 
(
	[ptp_playsession_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

IF NOT EXISTS( SELECT * FROM sys.indexes WHERE name='IDX_Play_Session_Id' AND object_id = OBJECT_ID('promogame_type_play_session'))
BEGIN
CREATE NONCLUSTERED INDEX [IDX_Play_Session_Id] ON [dbo].[promogame_type_play_session] 
(
  [ptp_playsession_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
END
GO

IF NOT EXISTS( SELECT * FROM sys.indexes WHERE name='IDX_Game_Type' AND object_id = OBJECT_ID('promogame_type_play_session'))
BEGIN
CREATE NONCLUSTERED INDEX [IDX_Game_Type] ON [dbo].[promogame_type_play_session] 
(
  [ptp_gametype] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
END
GO

-- Rename INTOUCH_PALETTE Table
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'intouch_palette') 
BEGIN
	EXEC sp_rename	'intouch_palette.itp_id',
					'pli_id',
					'COLUMN'
	EXEC sp_rename	'intouch_palette.itp_index',
					'pli_index',
					'COLUMN'
	EXEC sp_rename	'intouch_palette.itp_name',
					'pli_name',
					'COLUMN'
	EXEC sp_rename	'intouch_palette.itp_level',
					'pli_level',
					'COLUMN'
	EXEC sp_rename	'intouch_palette.itp_NLS_id',
					'pli_NLS_id',
					'COLUMN'
	EXEC sp_rename	'intouch_palette.itp_icon',
					'pli_icon',
					'COLUMN'
	EXEC sp_rename	'intouch_palette.itp_pending_download',
					'pli_pending_download',
					'COLUMN'
	EXEC sp_rename	'intouch_palette',
					'player_level_icons'
END
GO
  
-- Rename INTOUCH_PROMOGAME Table
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'intouch_promogame') 
BEGIN
	EXEC sp_rename	'intouch_promogame.ip_id',
					'pg_id',
					'COLUMN'
	EXEC sp_rename	'intouch_promogame.ip_name',
					'pg_name',
					'COLUMN'
	EXEC sp_rename	'intouch_promogame.ip_type',
					'pg_type',
					'COLUMN'
	EXEC sp_rename	'intouch_promogame.ip_price',
					'pg_price',
					'COLUMN'
	EXEC sp_rename	'intouch_promogame.ip_price_units',
					'pg_price_units',
					'COLUMN'
	EXEC sp_rename	'intouch_promogame.ip_return_price',
					'pg_return_price',
					'COLUMN'
	EXEC sp_rename	'intouch_promogame.ip_return_units',
					'pg_return_units',
					'COLUMN'
	EXEC sp_rename	'intouch_promogame.ip_percentatge_cost_return',
					'pg_percentatge_cost_return',
					'COLUMN'
	EXEC sp_rename	'intouch_promogame.ip_game_url',
					'pg_game_url',
					'COLUMN'
	EXEC sp_rename	'intouch_promogame.ip_mandatory_ic',
					'pg_mandatory_ic',
					'COLUMN'
	EXEC sp_rename	'intouch_promogame.ip_show_buy_dialog',
					'pg_show_buy_dialog',
					'COLUMN'
	EXEC sp_rename	'intouch_promogame.ip_player_can_cancel',
					'pg_player_can_cancel',
					'COLUMN'
	EXEC sp_rename	'intouch_promogame.ip_autocancel',
					'pg_autocancel',
					'COLUMN'
	EXEC sp_rename	'intouch_promogame.ip_transfer_screen',
					'pg_transfer_screen',
					'COLUMN'
	EXEC sp_rename	'intouch_promogame.ip_transfer_timeout',
					'pg_transfer_timeout',
					'COLUMN'
	EXEC sp_rename	'intouch_promogame.ip_pay_table',
					'pg_pay_table',
					'COLUMN'
	EXEC sp_rename	'intouch_promogame.ip_last_update',
					'pg_last_update',
					'COLUMN'
	EXEC sp_rename	'intouch_promogame',
					'promogames'
END
GO

 -- Rename PROMOTIONS.PM_INTOUCH_PROMOGAME
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'promotions' AND COLUMN_NAME = 'PM_INTOUCH_PROMOGAME') 
BEGIN 
	EXEC sp_rename	'promotions.pm_intouch_promogame',
					'pm_promogame_id',
					'COLUMN'
END
GO

 -- Rename PROMOTIONS.PM_PYRAMIDAL_DIST
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'promotions' AND COLUMN_NAME = 'PM_INTOUCH_PYRAMIDAL_DIST') 
BEGIN 					
	EXEC sp_rename	'promotions.PM_INTOUCH_PYRAMIDAL_DIST',
					'pm_pyramidal_dist',
					'COLUMN'					
END
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_InHouseAPI_Insert_Today_Visits]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[SP_InHouseAPI_Insert_Today_Visits]
GO

CREATE PROCEDURE [dbo].[SP_InHouseAPI_Insert_Today_Visits]  
    @pGamingDay	        INT,
    @pCustomerId		    BIGINT
AS
BEGIN
    DECLARE @SiteId            as INT
    
    SET NOCOUNT ON
    
    --Get site id
    SELECT   @SiteId = CAST(GP_KEY_VALUE AS INT) 
      FROM   GENERAL_PARAMS 
     WHERE   GP_GROUP_KEY   = 'Site' 
       AND   GP_SUBJECT_KEY = 'Identifier'
       AND   ISNUMERIC(GP_KEY_VALUE) = 1       
       
    --It not already exists TODAY_VISITS?, insert in TODAY_VISITS and IN_HOUSE_EVENTS 
    IF NOT EXISTS (SELECT  *
                     FROM  TODAY_VISITS
                    WHERE  tv_site_id      = @SiteId
                      AND  tv_gaming_day   = @pGamingDay
                      AND  tv_customer_id  = @pCustomerId)
    BEGIN
        INSERT INTO TODAY_VISITS
        (
            tv_site_id, tv_gaming_day, tv_customer_id
        )
        VALUES
        (
            @SiteId, @pGamingDay, @pCustomerId                              
        )
        
        INSERT INTO IN_HOUSE_EVENTS
        (
            ihe_site_id
          , ihe_event_inserted_in_local_time, ihe_event_inserted_in_utc_time
          , ihe_event_datetime_in_local_time, ihe_event_datetime_in_utc_time
          , ihe_event_type, ihe_customer_id, ihe_egm_id
        )
        VALUES
        (
            @SiteId
          , GETDATE(), GETUTCDATE()
          , GETDATE(), GETUTCDATE()
          , 1, @pCustomerId, NULL                              
        )        
    END
END
GO

GRANT EXECUTE ON [dbo].[SP_InHouseAPI_Insert_Today_Visits] TO [wggui] WITH GRANT OPTION 
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GT_Base_Report_Data]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[GT_Base_Report_Data]
GO

CREATE PROCEDURE [dbo].[GT_Base_Report_Data]
(
      @pBaseType                   INTEGER
    , @pTimeInterval               INTEGER
    , @pDateFrom                   DATETIME
    , @pDateTo                     DATETIME
    , @pOnlyActivity               INTEGER
    , @pOrderBy                    INTEGER
    , @pValidTypes                 VARCHAR(4096)
    , @pSelectedCurrency           NVARCHAR(100)
    , @pTotalToSelectedCurrency    INTEGER
)
AS
BEGIN

-- DECLARATIONS
   -- @_DAYS_AND_TABLES:    A temporary table with all intervals and all base type selected (used to show base types without activity).
   DECLARE @_DAYS_AND_TABLES TABLE(DATE_TIME DATETIME, TABLE_IDENTIFIER_VALUE BIGINT, TABLE_IDENT_NAME VARCHAR(50), TABLE_TYPE_IDENT BIGINT, TABLE_TYPE_NAME VARCHAR(50))
   -- @_DAY_VAR:            Variable to create the intervals
   DECLARE @_DAY_VAR DATETIME

-- PARAMETERS
     DECLARE @_BASE_TYPE                  AS   INTEGER -- 0 BY TABLE TYPE, 1 BY TABLE
     DECLARE @_TIME_INTERVAL              AS   INTEGER -- -1, 0 DAYS, 1 MONTH, 2 YEAR
     DECLARE @_DATE_FROM                  AS   DATETIME
     DECLARE @_DATE_TO                    AS   DATETIME
     DECLARE @_ONLY_ACTIVITY              AS   INTEGER -- 0 SHOW ALL, 1 SHOW ONLY WITH ACTIVITY
     DECLARE @_ORDER_BY                   AS   INTEGER -- 0 BY INTERVAL DESC, 1 BY IDENTIFIER ASC
     DECLARE @_DELIMITER                  AS   CHAR(1)
     DECLARE @_TYPES_TABLE                TABLE(SST_ID INT, SST_VALUE VARCHAR(50)) -- TO STORE THE VALID GAMING TABLE TYPES
     DECLARE @_SELECTED_CURRENCY          TABLE(SST_ID INT, SST_VALUE VARCHAR(50)) -- TO STORE SELECTED CATEGORIES
     DECLARE @_TOTAL_TO_SELECTED_CURRENCY AS   INTEGER
     DECLARE @_CHIP_RE                    AS   INTEGER
     DECLARE @_CHIP_NRE                   AS   INTEGER
     DECLARE @_CHIP_COLOR                 AS   INTEGER
----------------------------------------------------------------------------------------------------------------

-- Initialization --
  SET @_BASE_TYPE                  =   @pBaseType
  SET @_TIME_INTERVAL              =   @pTimeInterval
  SET @_DATE_FROM                  =   @pDateFrom
  SET @_DATE_TO                    =   @pDateTo
  SET @_ONLY_ACTIVITY              =   @pOnlyActivity
  SET @_ORDER_BY                   =   @pOrderBy
  SET @_DELIMITER                  =   ','
  SET @_TOTAL_TO_SELECTED_CURRENCY =   @pTotalToSelectedCurrency
  SET @_CHIP_RE                    =   1001
  SET @_CHIP_NRE                   =   1002
  SET @_CHIP_COLOR                 =   1003
----------------------------------------------------------------------------------------------------------------

-- CHECK DATE PARAMETERS
IF @_DATE_FROM IS NULL
BEGIN
   -- IF DATE FROM IS NULL, USER FIRST POSIBLE DATE
   SET @_DATE_FROM = CAST('' AS DATETIME)
   SET @pDateFrom  = CAST('' AS DATETIME)
END

IF @_DATE_TO IS NULL
BEGIN
   -- IF DATE TO IS NULL, QUERY UNTIL CURRENT DATE
   SET @_DATE_TO = CAST(GETDATE() AS DATETIME)
   SET @pDateTo  = CAST(GETDATE() AS DATETIME)
END

-- ASSIGN TYPES PARAMETER INTO TABLE
INSERT INTO @_TYPES_TABLE SELECT * FROM dbo.SplitStringIntoTable(@pValidTypes, @_DELIMITER, DEFAULT)

INSERT INTO @_SELECTED_CURRENCY SELECT * FROM dbo.SplitStringIntoTable(@pSelectedCurrency, @_DELIMITER, DEFAULT)

IF @_TIME_INTERVAL <> -1
 BEGIN
   -- INTERVALS DATES AND TABLE PREPARATION

   -- PREPARE DATES RANGE DEPENDING ON TIME INTERVAL
   SET @_DATE_FROM = CASE
                        WHEN @_TIME_INTERVAL = 0 THEN -- BY DAY
                           DATEADD(dd, 0, DATEDIFF(dd, 0, @_DATE_FROM))
                        WHEN @_TIME_INTERVAL = 1 THEN -- BY MONTH
                           CAST(CAST(YEAR(@_DATE_FROM) AS VARCHAR(4)) + RIGHT('0' + CAST(MONTH(@_DATE_FROM) AS VARCHAR(2)), 2) + '01' AS DATETIME)
                        WHEN @_TIME_INTERVAL = 2 THEN -- BY YEAR
                           CAST(CAST(YEAR(@_DATE_FROM) AS VARCHAR(4)) + '0101' AS DATETIME)
                     END

   SET @_DATE_TO = CASE
          WHEN @_TIME_INTERVAL = 0 THEN -- BY DAY
                           DATEADD(DAY, 1, CAST(@_DATE_TO AS DATETIME))
                        WHEN @_TIME_INTERVAL = 1 THEN -- BY MONTH
                           DATEADD(MONTH, 1, CAST(CAST(YEAR(@_DATE_TO) AS VARCHAR(4)) + RIGHT('0' + CAST(MONTH(@_DATE_TO) AS VARCHAR(2)), 2) + '01' AS DATETIME))
                        WHEN @_TIME_INTERVAL = 2 THEN -- BY YEAR
                           DATEADD(YEAR, 1, CAST(CAST(YEAR(@_DATE_TO) AS VARCHAR(4)) + '0101' AS DATETIME))
                     END

   -- PREPARE THE TIME INTERVALS TABLE
   SET @_DAY_VAR = @_DATE_FROM
   WHILE @_DAY_VAR < @_DATE_TO
   BEGIN
         -- SET THE LINK FIELD FOR THE RESULTS
         IF @_BASE_TYPE = 0
          BEGIN
            -- LINK WITH TABLE TYPES
            INSERT INTO @_DAYS_AND_TABLES
                 SELECT   CAST(@_DAY_VAR AS DATETIME), X.GTT_GAMING_TABLE_TYPE_ID, X.GTT_NAME AS TABLE_IDENT, NULL, NULL
                   FROM   GAMING_TABLES_TYPES AS X
                  WHERE   X.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
                          AND     X.GTT_ENABLED = 1
          END
         ELSE
          BEGIN
            -- LINK WITH TABLES
            INSERT INTO @_DAYS_AND_TABLES
                 SELECT   CAST(@_DAY_VAR AS DATETIME), X.GT_GAMING_TABLE_ID, X.GT_NAME AS TABLE_IDENT, Z.GTT_GAMING_TABLE_TYPE_ID, Z.GTT_NAME
                   FROM   GAMING_TABLES AS X
              LEFT JOIN   GAMING_TABLES_TYPES AS Z
                     ON   Z.GTT_GAMING_TABLE_TYPE_ID = X.GT_TYPE_ID
                  WHERE   Z.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
                          AND X.GT_ENABLED = 1
          END

          -- SET INCREMENT
          SET @_DAY_VAR = CASE
                               WHEN @_TIME_INTERVAL = 0 THEN DATEADD(DAY,1,@_DAY_VAR)     -- DAY
                               WHEN @_TIME_INTERVAL = 1 THEN DATEADD(MONTH,1,@_DAY_VAR)   -- MONTH
                               WHEN @_TIME_INTERVAL = 2 THEN DATEADD(YEAR,1,@_DAY_VAR)    -- YEAR
                          END
   END

END -- IF INTERVALS <> - 1
ELSE
BEGIN

   -- SET THE LINK FIELD FOR THE RESULTS WITHOUT INTERVALS
   IF @_BASE_TYPE = 0
    BEGIN
      -- LINK WITH TABLE TYPES
      INSERT INTO @_DAYS_AND_TABLES
           SELECT   NULL, X.GTT_GAMING_TABLE_TYPE_ID, X.GTT_NAME AS TABLE_IDENT, NULL, NULL
             FROM   GAMING_TABLES_TYPES AS X
            WHERE   X.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
                    AND X.GTT_ENABLED = 1
    END
   ELSE
    BEGIN
      -- LINK WITH TABLES
      INSERT INTO @_DAYS_AND_TABLES
           SELECT   NULL, X.GT_GAMING_TABLE_ID, X.GT_NAME AS TABLE_IDENT, Z.GTT_GAMING_TABLE_TYPE_ID, Z.GTT_NAME
             FROM   GAMING_TABLES AS X
        LEFT JOIN   GAMING_TABLES_TYPES AS Z
               ON   Z.GTT_GAMING_TABLE_TYPE_ID = X.GT_TYPE_ID
            WHERE   Z.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
                    AND X.GT_ENABLED = 1
    END

 END


-- SELECT INTO TEMPORARY TABLE WITH ALL DATA
-- SPECIFIC REPORT QUERY
  SELECT   X.TABLE_IDENTIFIER
         , X.TABLE_NAME
         , X.TABLE_TYPE
         , X.TABLE_TYPE_NAME
         , SUM(X.COPY_DEALER_VALIDATED_AMOUNT) AS COPY_DEALER_VALIDATED_AMOUNT         
         , SUM(X.S_DROP_GAMBLING_TABLE) + SUM (X.S_DROP_CASHIER) TOTAL_DROP
         , SUM(X.S_DROP_GAMBLING_TABLE) TOTAL_DROP_GAMBLING_TABLE
         , SUM(X.S_DROP_CASHIER) TOTAL_DROP_CASHIER
         , SUM(X.S_WIN) TOTAL_WIN
         , ISNULL(SUM(X.S_TIP),0)  TOTAL_TIP
         , CASE WHEN SUM(X.S_DROP_GAMBLING_TABLE) + SUM (X.S_DROP_CASHIER) = 0 THEN 0 ELSE CAST(SUM(X.S_WIN) AS DECIMAL(18,2)) / CAST(SUM(X.S_DROP_GAMBLING_TABLE) + SUM (X.S_DROP_CASHIER) AS DECIMAL(18,2)) * 100 END AS WIN_DROP
         , MIN(X.THEORIC_HOLD) AS THEORIC_HOLD
         , CASE WHEN SUM(X.S_DROP_GAMBLING_TABLE) + SUM (X.S_DROP_CASHIER) = 0 THEN 0 ELSE SUM(CAST(X.S_TIP AS DECIMAL(18,2))) / CAST(SUM(X.S_DROP_GAMBLING_TABLE) + SUM (X.S_DROP_CASHIER) AS DECIMAL(18,2)) * 100 END AS TIP_DROP
         , SUM(X.S_WIN) + SUM(X.S_TIP) AS WIN_TIPS
         , X.CM_DATE_ONLY
         , MIN(X.OPEN_HOUR) AS OPEN_HOUR
         , MAX(X.CLOSE_HOUR) AS CLOSE_HOUR
         , SUM(X.SESSION_SECONDS) AS SESSION_SECONDS
         , COUNT(X.SESSION_SECONDS) AS SESSION_SUM         
    INTO   #GT_TEMPORARY_REPORT_DATA
   FROM (
           -- CORE QUERY
         SELECT CASE WHEN @_TIME_INTERVAL = 0      -- TO FILTER BY DAY
                     THEN DATEADD(dd, 0, DATEDIFF(dd, 0, CS_OPENING_DATE))
                     WHEN @_TIME_INTERVAL = 1      -- TO FILTER BY MONTH
                     THEN CAST(CAST(YEAR(CS_OPENING_DATE) AS VARCHAR(4)) + RIGHT('0' + CAST(MONTH(CS_OPENING_DATE) AS VARCHAR(2)), 2) + '01' AS DATETIME)
                     WHEN @_TIME_INTERVAL = 2      -- TO FILTER BY YEAR
                     THEN CAST(CAST(YEAR(CS_OPENING_DATE) AS VARCHAR(4)) + '01'+ '01' AS DATETIME)
                        END                                                                                       AS CM_DATE_ONLY
                     , (CASE WHEN @_BASE_TYPE = 0 THEN GTT_GAMING_TABLE_TYPE_ID ELSE GT.GT_GAMING_TABLE_ID END)   AS TABLE_IDENTIFIER -- GET THE BASE TYPE IDENTIFIER
                     , (CASE WHEN @_BASE_TYPE = 0 THEN GTT_NAME ELSE GT.GT_NAME END)                              AS TABLE_NAME       -- GET THE BASE TYPE IDENTIFIER NAME
                     , (CASE WHEN @_BASE_TYPE = 0 THEN NULL ELSE GTT_GAMING_TABLE_TYPE_ID END)                    AS TABLE_TYPE       -- TYPE
                     , (CASE WHEN @_BASE_TYPE = 0 THEN NULL ELSE GTT_NAME END)                                    AS TABLE_TYPE_NAME  -- TYPE NAME

                ,  CASE WHEN @_TOTAL_TO_SELECTED_CURRENCY = 0 
                        THEN DBO.GT_CALCULATE_DROP_GAMBLING_TABLES(ISNULL(GTS_COLLECTED_AMOUNT                , 0)
                                                                         , ISNULL(GTS_COLLECTED_DROPBOX_AMOUNT        , 0)
                                                                         , ISNULL(GTS_COLLECTED_DROPBOX_CHIPS_AMOUNT  , 0)
                                                                         , 0 -- It does not allow takings of dropbox with tickets
                                                                         , GT_HAS_INTEGRATED_CASHIER)
                        WHEN @_TOTAL_TO_SELECTED_CURRENCY = 1 
                        THEN DBO.GT_CALCULATE_DROP_GAMBLING_TABLES(SUM(ISNULL(GTSC_COLLECTED_AMOUNT                , 0))
                                                                         , SUM(ISNULL(CASE WHEN GTSC_TYPE = 0            THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                                           WHEN GTSC_TYPE = 1            THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                                      ELSE 0 END,  0))
                                                                         , SUM(ISNULL(CASE WHEN GTSC_TYPE = @_CHIP_RE    THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                                           WHEN GTSC_TYPE = @_CHIP_NRE   THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                                           --WHEN GTSC_TYPE = @_CHIP_COLOR THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                             ELSE 0 END,  0))
                                                                         , 0 -- It does not allow takings of dropbox with tickets
                                                                         , GT_HAS_INTEGRATED_CASHIER)
                             END                                                                                  AS S_DROP_GAMBLING_TABLE

                ,  CASE WHEN @_TOTAL_TO_SELECTED_CURRENCY = 0 
                        THEN DBO.GT_CALCULATE_DROP_CASHIER(ISNULL(GTS_OWN_SALES_AMOUNT, 0)
                                                                 , ISNULL(GTS_EXTERNAL_SALES_AMOUNT, 0))
                        WHEN @_TOTAL_TO_SELECTED_CURRENCY = 1 
                        THEN DBO.GT_CALCULATE_DROP_CASHIER(SUM(ISNULL(GTSC_OWN_SALES_AMOUNT, 0))
                                                                 , SUM(ISNULL(GTSC_EXTERNAL_SALES_AMOUNT, 0)))
                        END                                                                              AS S_DROP_CASHIER

                ,  GT_THEORIC_HOLD AS THEORIC_HOLD
                ,  CASE WHEN @_TOTAL_TO_SELECTED_CURRENCY = 0 
                        THEN DBO.GT_CALCULATE_WIN(ISNULL(GTS_FINAL_CHIPS_AMOUNT      , 0)
                                                        , ISNULL(GTS_INITIAL_CHIPS_AMOUNT    , 0)
                                                        , ISNULL(GTS_FILLS_CHIPS_AMOUNT      , 0)
                                                        , ISNULL(GTS_CREDITS_CHIPS_AMOUNT    , 0)                                                        
                                                        , ISNULL(GTS_TIPS                    , 0)
                                                        , ISNULL(GTS_OWN_SALES_AMOUNT        , 0)
                                                        , ISNULL(GTS_EXTERNAL_SALES_AMOUNT   , 0)
                                                        , ISNULL(GTS_COLLECTED_AMOUNT        , 0)
                                                        , ISNULL(GTS_COLLECTED_DROPBOX_AMOUNT        , 0)
                                                        , ISNULL(GTS_COLLECTED_DROPBOX_CHIPS_AMOUNT  , 0)
                                                        , 0 -- It does not allow takings of dropbox with tickets)
                                                        , GT_HAS_INTEGRATED_CASHIER)
                        WHEN @_TOTAL_TO_SELECTED_CURRENCY = 1 
                        THEN DBO.GT_CALCULATE_WIN(SUM(ISNULL(GTSC_FINAL_CHIPS_AMOUNT   , 0))
                                                        , SUM(ISNULL(GTSC_INITIAL_CHIPS_AMOUNT , 0))
                                                        , SUM(ISNULL(GTSC_FILLS_CHIPS_AMOUNT   , 0))
                                                , SUM(ISNULL(GTSC_CREDITS_CHIPS_AMOUNT , 0))
                                                        , SUM(ISNULL(GTSC_TIPS                 , 0))
                                                        , SUM(ISNULL(GTSC_OWN_SALES_AMOUNT     , 0))
                                                        , SUM(ISNULL(GTSC_EXTERNAL_SALES_AMOUNT, 0))
                                                        , SUM(ISNULL(GTSC_COLLECTED_AMOUNT     , 0))
                                                        , SUM(ISNULL(CASE WHEN GTSC_TYPE = 0            THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                          WHEN GTSC_TYPE = 1            THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                          ELSE 0 END,  0))
                                                        , SUM(ISNULL(CASE WHEN GTSC_TYPE = @_CHIP_RE    THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                          WHEN GTSC_TYPE = @_CHIP_NRE   THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                          --WHEN GTSC_TYPE = @_CHIP_COLOR THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                          ELSE 0 END,  0))
                                                        , 0 -- It does not allow takings of dropbox with tickets
                                                        , GT_HAS_INTEGRATED_CASHIER)
                             END AS S_WIN

                     ,  CASE WHEN @_TOTAL_TO_SELECTED_CURRENCY = 0 THEN
                                     SUM(ISNULL(GTS_TIPS, 0))
                             WHEN @_TOTAL_TO_SELECTED_CURRENCY = 1 THEN
                                     SUM(ISNULL(GTSC_TIPS, 0))
                             ELSE 0
                             END                                                                       AS S_TIP
                     , CS.CS_OPENING_DATE                                                              AS OPEN_HOUR
                     , CS.CS_CLOSING_DATE                                                              AS CLOSE_HOUR
                     , DATEDIFF(SECOND, CS_OPENING_DATE, ISNULL(CS_CLOSING_DATE, GETDATE()))           AS SESSION_SECONDS
                     , CASE WHEN @_TOTAL_TO_SELECTED_CURRENCY = 0
                            THEN ISNULL(GTS_COPY_DEALER_VALIDATED_AMOUNT, 0)
                            WHEN @_TOTAL_TO_SELECTED_CURRENCY = 1 
                            THEN SUM(ISNULL(GTSC_COPY_DEALER_VALIDATED_AMOUNT, 0))
                            END                                                           AS COPY_DEALER_VALIDATED_AMOUNT
                FROM   GAMING_TABLES_SESSIONS GTS
           LEFT JOIN   GAMING_TABLES_SESSIONS_BY_CURRENCY GTSC
                  ON   GTSC.GTSC_GAMING_TABLE_SESSION_ID = GTS.GTS_GAMING_TABLE_SESSION_ID
                 AND   GTSC_ISO_CODE IN ( SELECT SST_VALUE FROM @_SELECTED_CURRENCY) 
                 AND   GTSC_TYPE <> @_CHIP_COLOR
          INNER JOIN   CASHIER_SESSIONS CS ON GTS.GTS_CASHIER_SESSION_ID = CS.CS_SESSION_ID
                 AND   CS_OPENING_DATE >= @pDateFrom AND CS_OPENING_DATE < @pDateTo
          INNER JOIN   GAMING_TABLES GT ON GTS.GTS_GAMING_TABLE_ID = GT.GT_GAMING_TABLE_ID
                 AND   GT.GT_TYPE_ID IN (SELECT SST_VALUE FROM @_TYPES_TABLE)
          INNER JOIN   GAMING_TABLES_TYPES GTT ON GT.GT_TYPE_ID = GTT.GTT_GAMING_TABLE_TYPE_ID
               WHERE   CS_STATUS = 1 -- Only closed sessions
                 AND   (@_TOTAL_TO_SELECTED_CURRENCY = 0 AND GTSC_TYPE = 0 OR @_TOTAL_TO_SELECTED_CURRENCY = 1) 
            GROUP BY   CS_OPENING_DATE, GTT_GAMING_TABLE_TYPE_ID, GT_GAMING_TABLE_ID, GTT_NAME, GT_NAME
                     , GTS_OWN_SALES_AMOUNT, GTS_EXTERNAL_SALES_AMOUNT, GTS_COLLECTED_AMOUNT, GT_HAS_INTEGRATED_CASHIER
                     , GTS_COLLECTED_DROPBOX_AMOUNT, GTS_COLLECTED_DROPBOX_AMOUNT, GTS_DROPBOX_ENABLED
                     , GTS_COLLECTED_DROPBOX_CHIPS_AMOUNT, GT_THEORIC_HOLD, GTS_FINAL_CHIPS_AMOUNT, GTS_INITIAL_CHIPS_AMOUNT
                     , GTS_FILLS_CHIPS_AMOUNT, GTS_CREDITS_CHIPS_AMOUNT, GTSC_TYPE, GTS_TIPS, GTSC_TIPS, CS_CLOSING_DATE
                     , GTS_COPY_DEALER_VALIDATED_AMOUNT
          -- END CORE QUERY
        ) AS X
  GROUP BY   X.TABLE_TYPE, X.TABLE_IDENTIFIER, X.TABLE_TYPE_NAME, X.TABLE_NAME, X.CM_DATE_ONLY  -- Group by indentifier and time interval

IF @_TIME_INTERVAL <> -1
BEGIN
   -- INTERVALS DATES AND TABLE FINAL PREPARATION
   -- FILTER ACTIVITY
   IF @_ONLY_ACTIVITY = 0
    BEGIN
      -- JOIN THE SELECT WITH ALL DATA
      SELECT   DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER
             , DT.TABLE_IDENT_NAME AS TABLE_NAME
             , DT.TABLE_TYPE_IDENT AS TABLE_TYPE
             , DT.TABLE_TYPE_NAME AS TABLE_TYPE_NAME
             , ISNULL(zz.COPY_DEALER_VALIDATED_AMOUNT, 0) AS COPY_DEALER_AMOUNT
             , ISNULL(ZZ.TOTAL_DROP_GAMBLING_TABLE,0) AS TOTAL_DROP_GAMBLING_TABLE
             , ISNULL(ZZ.TOTAL_DROP_CASHIER,0) AS  TOTAL_DROP_CASHIER
             , ISNULL(ZZ.TOTAL_DROP, 0) AS TOTAL_DROP
             , ISNULL(ZZ.TOTAL_WIN, 0) AS TOTAL_WIN
             , ISNULL(ZZ.TOTAL_TIP, 0) AS TOTAL_TIP
             , ISNULL(ZZ.WIN_DROP, 0) AS WIN_DROP
             , ISNULL(ZZ.THEORIC_HOLD, 0) AS THEORIC_HOLD
             , ISNULL(ZZ.TIP_DROP, 0) AS TIP_DROP
             , ISNULL(ZZ.WIN_TIPS, 0) AS WIN_TIPS
             , ZZ.OPEN_HOUR
             , ZZ.CLOSE_HOUR
             , CASE WHEN ZZ.CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED
             , CASE WHEN ZZ.SESSION_SECONDS = 0 THEN DATEDIFF(SECOND,ZZ.OPEN_HOUR,GETDATE()) ELSE ISNULL(ZZ.SESSION_SECONDS, 0) END AS SESSION_SECONDS
             , ISNULL(ZZ.SESSION_SUM, 0) AS SESSION_SUM
             , DATE_TIME              
        FROM   @_DAYS_AND_TABLES DT
   LEFT JOIN   #GT_TEMPORARY_REPORT_DATA ZZ
          ON   (  (@_TIME_INTERVAL = 0 AND DATE_TIME = ZZ.CM_DATE_ONLY)
                           OR (@_TIME_INTERVAL = 1 AND MONTH(DATE_TIME) = MONTH(ZZ.CM_DATE_ONLY) AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
                           OR (@_TIME_INTERVAL = 2 AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
               ) AND TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER
       -- SET ORDER
    ORDER BY   DT.TABLE_TYPE_IDENT ASC
             , CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC
             , DATE_TIME DESC;

    END
   ELSE
    BEGIN
      -- JOIN THE SELECT HIDDING WITHOUT ACTIVITY
      SELECT   DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER
             , DT.TABLE_IDENT_NAME AS TABLE_NAME
             , DT.TABLE_TYPE_IDENT AS TABLE_TYPE
             , DT.TABLE_TYPE_NAME AS TABLE_TYPE_NAME
             , ISNULL(zz.COPY_DEALER_VALIDATED_AMOUNT, 0) AS COPY_DEALER_AMOUNT
             , ISNULL(ZZ.TOTAL_DROP_GAMBLING_TABLE,0) AS TOTAL_DROP_GAMBLING_TABLE 
             , ISNULL(ZZ.TOTAL_DROP_CASHIER,0) AS  TOTAL_DROP_CASHIER
             , ISNULL(ZZ.TOTAL_DROP, 0) AS TOTAL_DROP 
             , ISNULL(ZZ.TOTAL_WIN, 0) AS TOTAL_WIN
             , ISNULL(ZZ.TOTAL_TIP, 0) AS TOTAL_TIP
             , ISNULL(ZZ.WIN_DROP, 0) AS WIN_DROP
             , ISNULL(ZZ.THEORIC_HOLD, 0) AS THEORIC_HOLD
             , ISNULL(ZZ.TIP_DROP, 0) AS TIP_DROP
             , ISNULL(ZZ.WIN_TIPS, 0) AS WIN_TIPS
             , ZZ.OPEN_HOUR
             , ZZ.CLOSE_HOUR
             , CASE WHEN ZZ.CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED
             , CASE WHEN ZZ.SESSION_SECONDS = 0 THEN DATEDIFF(SECOND,ZZ.OPEN_HOUR,GETDATE()) ELSE ISNULL(ZZ.SESSION_SECONDS, 0) END AS SESSION_SECONDS
             , ISNULL(ZZ.SESSION_SUM, 0) AS SESSION_SUM
             , DATE_TIME
        FROM   @_DAYS_AND_TABLES DT
  INNER JOIN   #GT_TEMPORARY_REPORT_DATA ZZ
          ON   (  (@_TIME_INTERVAL = 0 AND DATE_TIME = ZZ.CM_DATE_ONLY)
                           OR (@_TIME_INTERVAL = 1 AND MONTH(DATE_TIME) = MONTH(ZZ.CM_DATE_ONLY) AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
                           OR (@_TIME_INTERVAL = 2 AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
               ) AND TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER
       -- SET ORDER
    ORDER BY   DT.TABLE_TYPE_IDENT ASC
             , CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC
             , DATE_TIME DESC;
    END -- IF ONLY_ACTIVITY
END
ELSE  -- ELSE WITHOUT INTERVALS
BEGIN
   -- FINAL WITHOUT INTERVALS
      -- FILTER ACTIVITY
   IF @_ONLY_ACTIVITY = 0
    BEGIN
            -- JOIN DATA INCLUDING WITHOUT ACTIVITY
      SELECT   DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER
             , DT.TABLE_IDENT_NAME AS TABLE_NAME
             , DT.TABLE_TYPE_IDENT AS TABLE_TYPE
             , DT.TABLE_TYPE_NAME
             , ISNULL(zz.COPY_DEALER_VALIDATED_AMOUNT, 0) AS COPY_DEALER_AMOUNT
             , ISNULL(ZZ.TOTAL_DROP_GAMBLING_TABLE,0) AS TOTAL_DROP_GAMBLING_TABLE
             , ISNULL(ZZ.TOTAL_DROP_CASHIER,0) AS  TOTAL_DROP_CASHIER
             , ISNULL(TOTAL_DROP, 0) AS TOTAL_DROP
             , ISNULL(TOTAL_WIN, 0) AS TOTAL_WIN
             , ISNULL(TOTAL_TIP, 0) AS TOTAL_TIP
             , ISNULL(WIN_DROP, 0) AS WIN_DROP
             , ISNULL(ZZ.THEORIC_HOLD, 0) AS THEORIC_HOLD
             , ISNULL(TIP_DROP, 0) AS TIP_DROP
             , ISNULL(WIN_TIPS, 0) AS WIN_TIPS
             , OPEN_HOUR
             , CLOSE_HOUR
             , CASE WHEN CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED
             , CASE WHEN SESSION_SECONDS = 0 THEN DATEDIFF(SECOND, OPEN_HOUR, GETDATE()) ELSE ISNULL(SESSION_SECONDS, 0) END AS SESSION_SECONDS
             , ISNULL(SESSION_SUM, 0) AS SESSION_SUM
              FROM   @_DAYS_AND_TABLES DT
   LEFT JOIN   #GT_TEMPORARY_REPORT_DATA ZZ ON DT.TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER
    ORDER BY   DT.TABLE_TYPE_IDENT ASC
             , CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC
             , DATE_TIME DESC;

     END
    ELSE
     BEGIN
            -- JOIN DATA WITH ONLY ACTIVITY
       SELECT   DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER
              , DT.TABLE_IDENT_NAME AS TABLE_NAME
              , DT.TABLE_TYPE_IDENT AS TABLE_TYPE
              , DT.TABLE_TYPE_NAME
              , ISNULL(zz.COPY_DEALER_VALIDATED_AMOUNT, 0) AS COPY_DEALER_AMOUNT
              , ISNULL(ZZ.TOTAL_DROP_GAMBLING_TABLE,0) AS TOTAL_DROP_GAMBLING_TABLE
              , ISNULL(ZZ.TOTAL_DROP_CASHIER,0) AS  TOTAL_DROP_CASHIER
              , ISNULL(TOTAL_DROP, 0) AS TOTAL_DROP
              , ISNULL(TOTAL_WIN, 0) AS TOTAL_WIN
              , ISNULL(TOTAL_TIP, 0) AS TOTAL_TIP
              , ISNULL(WIN_DROP, 0) AS WIN_DROP
              , ISNULL(ZZ.THEORIC_HOLD, 0) AS THEORIC_HOLD
              , ISNULL(TIP_DROP, 0) AS TIP_DROP
              , ISNULL(WIN_TIPS, 0) AS WIN_TIPS
              , OPEN_HOUR
              , CLOSE_HOUR
              , CASE WHEN CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED
              , CASE WHEN SESSION_SECONDS = 0 THEN DATEDIFF(SECOND, OPEN_HOUR, GETDATE()) ELSE ISNULL(SESSION_SECONDS, 0) END AS SESSION_SECONDS
              , ISNULL(SESSION_SUM, 0) AS SESSION_SUM
              FROM   @_DAYS_AND_TABLES DT
   INNER JOIN   #GT_TEMPORARY_REPORT_DATA ZZ ON DT.TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER
     ORDER BY   DT.TABLE_TYPE_IDENT ASC
              , CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC
              , DATE_TIME DESC;
     END
 END

-- ERASE THE TEMPORARY DATA
DROP TABLE #GT_TEMPORARY_REPORT_DATA

END -- END PROCEDURE
GO

-- PERMISSIONS
GRANT EXECUTE ON [dbo].[GT_Base_Report_Data] TO [wggui] WITH GRANT OPTION
GO

IF EXISTS (SELECT *
			FROM 	sys.objects 
			WHERE 	OBJECT_ID = OBJECT_ID(N'[dbo].[GetTablePlayersReport]') 
				AND type in (N'P', N'PC'))
BEGIN
  DROP PROCEDURE GetTablePlayersReport
END
GO

CREATE PROCEDURE GetTablePlayersReport
	 @pStart VARCHAR(MAX) 
	,@pEnd VARCHAR(MAX)
	,@pIsoCode VARCHAR(5) 
	,@pTableTypes VARCHAR(MAX) 
	,@pAccountNumber VARCHAR(MAX) 
	,@pTrackData VARCHAR(MAX)
	,@pHolderName VARCHAR(MAX)
	,@pIsVip BIT 
AS
BEGIN

DECLARE @SqlCmdA AS NVARCHAR(MAX)
DECLARE @SqlCmdB AS NVARCHAR(MAX)
DECLARE @SqlCmdC AS NVARCHAR(MAX)

DECLARE @ChipRE AS NVARCHAR(4)
DECLARE @ChipNRE AS NVARCHAR(4)
DECLARE @ChipColor AS NVARCHAR(4)

SET @ChipRE = '1001'
SET @ChipNRE = '1002'
SET @ChipColor = '1003'

if( @pEnd = NULL)
  SET @pEnd = ''

if( @pAccountNumber = NULL)
  SET @pAccountNumber = ''

if( @pTrackData = NULL)
  SET @pTrackData = ''

if( @pHolderName = NULL)
  SET @pHolderName = ''

SET @SqlCmdA = '
    SELECT 	   
	    ISNULL(SUM(GTPS_TOTAL_SELL_CHIPS),0) AS GTPS_BUY_IN  
	   ,ISNULL(SUM(GTPS_CHIPS_IN),0) AS GTPS_CHIPS_IN   
	   ,ISNULL(SUM(GTPS_NETWIN),0) AS GTPS_NETWIN 	  
	FROM  GT_PLAY_SESSIONS WITH( INDEX(IX_gtps_account_id_started))	  
	WHERE GTPS_FINISHED >= CAST('''+ @pStart +''' AS DATETIME)
	  AND GTPS_ISO_CODE IN ('''+ @pIsoCode +''')
'
IF(@pEnd <> '')
  SET @SqlCmdA += ' AND GTPS_FINISHED < CAST('''+@pEnd+''' AS DATETIME) '

SET @SqlCmdB = '
    SELECT 
	        ISNULL(SUM(GTSC_DROP_TABLES),0) + ISNULL(SUM(GTSC_DROP_CASHIER),0) GTSC_DROP
	       ,ISNULL(SUM(GTSC_WIN),0)                                            GTSC_WIN
	       
	
	FROM(
	SELECT 
	dbo.GT_Calculate_DROP_GAMBLING_TABLES(ISNULL(SUM(GTSC_COLLECTED_AMOUNT),0) 
										,SUM(ISNULL(CASE WHEN GTSC_TYPE = 0 THEN GTSC_COLLECTED_DROPBOX_AMOUNT
														WHEN GTSC_TYPE = 1 THEN GTSC_COLLECTED_DROPBOX_AMOUNT
											ELSE 0 END,  0))
										,SUM(ISNULL(CASE WHEN GTSC_TYPE = '+ @ChipRE +'    THEN GTSC_COLLECTED_DROPBOX_AMOUNT
														WHEN GTSC_TYPE =  '+ @ChipNRE +'   THEN GTSC_COLLECTED_DROPBOX_AMOUNT
														WHEN GTSC_TYPE =  '+ @ChipColor +' THEN GTSC_COLLECTED_DROPBOX_AMOUNT
																					ELSE 0 END,  0))
										,0
										,GT_HAS_INTEGRATED_CASHIER) AS GTSC_DROP_TABLES
    
	,DBO.GT_Calculate_DROP_CASHIER(SUM(ISNULL(GTSC_OWN_SALES_AMOUNT, 0))
                                  ,SUM(ISNULL(GTSC_EXTERNAL_SALES_AMOUNT, 0))) AS GTSC_DROP_CASHIER

	,dbo.GT_Calculate_WIN(SUM(ISNULL(GTSC_FINAL_CHIPS_AMOUNT   , 0))
										, SUM(ISNULL(GTSC_INITIAL_CHIPS_AMOUNT , 0))
										, SUM(ISNULL(GTSC_FILLS_CHIPS_AMOUNT   , 0))
										, SUM(ISNULL(GTSC_CREDITS_CHIPS_AMOUNT , 0))
										, SUM(ISNULL(GTSC_TIPS                 , 0))
										, SUM(ISNULL(GTSC_OWN_SALES_AMOUNT     , 0))
										, SUM(ISNULL(GTSC_EXTERNAL_SALES_AMOUNT, 0))
										, SUM(ISNULL(GTSC_COLLECTED_AMOUNT     , 0))
										, SUM(ISNULL(CASE WHEN GTSC_TYPE = 0 THEN GTSC_COLLECTED_DROPBOX_AMOUNT
														WHEN GTSC_TYPE = 1 THEN GTSC_COLLECTED_DROPBOX_AMOUNT
														ELSE 0 END,  0))
										, SUM(ISNULL(CASE WHEN GTSC_TYPE = '+ @ChipRE +'    THEN GTSC_COLLECTED_DROPBOX_AMOUNT
														  WHEN GTSC_TYPE = '+ @ChipNRE +'   THEN GTSC_COLLECTED_DROPBOX_AMOUNT
														  WHEN GTSC_TYPE = '+ @ChipColor +' THEN GTSC_COLLECTED_DROPBOX_AMOUNT
														ELSE 0 END,  0))
										, 0
										, GT_HAS_INTEGRATED_CASHIER) AS GTSC_WIN
	FROM   GAMING_TABLES_SESSIONS GTS
	LEFT   JOIN GAMING_TABLES_SESSIONS_BY_CURRENCY GTSC ON GTSC.GTSC_GAMING_TABLE_SESSION_ID = GTS.GTS_GAMING_TABLE_SESSION_ID
														AND GTSC_ISO_CODE IN ('''+ @pIsoCode +''')
	INNER   JOIN CASHIER_SESSIONS CS  ON GTS.GTS_CASHIER_SESSION_ID = CS.CS_SESSION_ID
									  AND CS_OPENING_DATE >=  CAST('''+ @pStart +''' AS DATETIME) '
IF(@pEnd <> '')
  SET @SqlCmdB +=	'							  AND CS_OPENING_DATE < CAST('''+@pEnd+''' AS DATETIME) '

SET @SqlCmdB +=	'INNER   JOIN GAMING_TABLES GT ON GTS.GTS_GAMING_TABLE_ID = GT.GT_GAMING_TABLE_ID									
	INNER   JOIN GAMING_TABLES_TYPES GTT ON GT.GT_TYPE_ID = GTT.GTT_GAMING_TABLE_TYPE_ID
	WHERE   CS_STATUS = 1  
	 GROUP BY CS_OPENING_DATE, GTT_GAMING_TABLE_TYPE_ID, GT_GAMING_TABLE_ID, GTT_NAME, GT_NAME
                      , GTS_OWN_SALES_AMOUNT, GTS_EXTERNAL_SALES_AMOUNT, GTS_COLLECTED_AMOUNT, GT_HAS_INTEGRATED_CASHIER
                      , GTS_COLLECTED_DROPBOX_AMOUNT, GTS_COLLECTED_DROPBOX_AMOUNT, GTS_DROPBOX_ENABLED
                      , GTS_COLLECTED_DROPBOX_CHIPS_AMOUNT, GT_THEORIC_HOLD, GTS_FINAL_CHIPS_AMOUNT, GTS_INITIAL_CHIPS_AMOUNT
                      , GTS_FILLS_CHIPS_AMOUNT, GTS_CREDITS_CHIPS_AMOUNT, GTSC_TYPE, GTS_TIPS, GTSC_TIPS, CS_CLOSING_DATE
                      , GTS_COPY_DEALER_VALIDATED_AMOUNT
    ) A
'

SET @SqlCmdC = '
  SELECT * FROM(
	SELECT 
		AC_ACCOUNT_ID
	   ,AC_HOLDER_NAME 
	   ,GT_NAME
	   ,GTPS_STARTED
	   ,GTPS_FINISHED
	   ,ISNULL(GTPS_TOTAL_SELL_CHIPS, 0) AS GTPS_BUY_IN  
	   ,ISNULL(GTPS_CHIPS_IN,0) AS GTPS_CHIPS_IN   
	   ,ISNULL(GTPS_NETWIN,0) AS GTPS_NETWIN   
	   ,CASE WHEN AC_HOLDER_NAME IS NOT NULL THEN 0 ELSE 1 END AC_IS_ANONYMOUS	
	   ,GTPS_PLAY_SESSION_ID
	FROM  GT_PLAY_SESSIONS WITH( INDEX(IX_gtps_account_id_started))
	INNER JOIN ACCOUNTS                                                         
	ON   GTPS_ACCOUNT_ID = AC_ACCOUNT_ID
	INNER JOIN   GAMING_TABLES                                                    
	ON   GTPS_GAMING_TABLE_ID = GT_GAMING_TABLE_ID  
	WHERE GTPS_FINISHED >= CAST('''+ @pStart +''' AS DATETIME)
	  AND GTPS_ISO_CODE IN ('''+ @pIsoCode +''')
	  AND GT_TYPE_ID IN ('+ @pTableTypes +')
	 
'
IF(@pEnd <> '')
  SET @SqlCmdC += ' AND GTPS_FINISHED < CAST('''+@pEnd+''' AS DATETIME) '

IF(@pAccountNumber <> '' )
  SET @SqlCmdC += ' AND AC_ACCOUNT_ID = '+ @pAccountNumber +' ' 

IF(@pTrackData <> '')
  SET @SqlCmdC += ' AND AC_TRACK_DATA = '''+ @pTrackData +''' '

IF(@pHolderName <> '')
  SET @SqlCmdC += ' AND AC_HOLDER_NAME LIKE ''%'+@pHolderName +'%'' '

IF(@pIsVip = 1)
  SET @SqlCmdC += ' AND AC_HOLDER_IS_VIP = 1 '

SET @SqlCmdC += ' ) A ORDER BY AC_IS_ANONYMOUS, AC_HOLDER_NAME, GTPS_PLAY_SESSION_ID, AC_ACCOUNT_ID '

EXEC SP_EXECUTESQL @SqlCmdA
EXEC SP_EXECUTESQL @SqlCmdB
EXEC SP_EXECUTESQL @SqlCmdC 

END
GO

GRANT EXECUTE ON GetTablePlayersReport TO [wggui] WITH GRANT OPTION
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_AFIP_GetTerminalMetersHistory]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[SP_AFIP_GetTerminalMetersHistory]
GO

CREATE PROCEDURE [dbo].[SP_AFIP_GetTerminalMetersHistory]
(
    @pStartDateTime           DATETIME
  , @pEndDateTime             DATETIME
  , @pPendingType             INT
  , @pTerminalsAFIP           NVARCHAR(4000)
  , @pTerminalStatusActive    INT
  , @pTerminalType            NVARCHAR(4000)
)
AS
BEGIN
  --Pending Types CONSTANTS
  DECLARE @PENDING_TYPE_REQUEST                   INT;

  --Meter Types CONSTANTS
  DECLARE @TYPE_DAILY                             INT;

  DECLARE @TYPE_ROLLOVER                          INT;
  DECLARE @TYPE_SAS_ACCOUNT_DENOM_CHANGE          INT;
  DECLARE @TYPE_SERVICE_RAM_CLEAR                 INT;

  DECLARE @TYPE_GROUP_SERVICE_RAM_CLEAR           INT;
  DECLARE @TYPE_GROUP_ROLLOVER                    INT;
  DECLARE @TYPE_GROUP_SAS_ACCOUNT_DENOM_CHANGE    INT;

  --Meter Codes CONSTANTS
  DECLARE @METER_COIN_IN                          INT;
  DECLARE @METER_COIN_OUT                         INT;
  DECLARE @METER_JACKPOTS                         INT;
  DECLARE @METER_GAMES_PLAYED                     INT;
  DECLARE @METER_GAMES_WON                        INT;

  --Internal variables
  DECLARE @LAST_TERMINAL_METER_DATE               DATETIME;
  DECLARE @INTERVAL_FROM                          DATETIME;
  DECLARE @INTERVAL_TO                            DATETIME;

  DECLARE @TERMINAL_ID                            INT;
  DECLARE @REGISTRATION_CODE                      NVARCHAR(50);

  --Pending Types
  SET @PENDING_TYPE_REQUEST = 4;                  --Request

  --Meter Types
  SET @TYPE_DAILY = 1                             --Daily

  SET @TYPE_ROLLOVER = 11                         --Rollover
  SET @TYPE_SAS_ACCOUNT_DENOM_CHANGE = 14         --Denom change
  SET @TYPE_SERVICE_RAM_CLEAR = 15                --Service RAM Clear

  SET @TYPE_GROUP_ROLLOVER = 110                  --Group Rollover
  SET @TYPE_GROUP_SAS_ACCOUNT_DENOM_CHANGE = 140  --Group Denom change
  SET @TYPE_GROUP_SERVICE_RAM_CLEAR = 150         --Group Service RAM Clear

  --Meter Codes
  SET @METER_COIN_IN = 0                          --Coin In
  SET @METER_COIN_OUT = 1                         --Coin Out
  SET @METER_JACKPOTS = 2                         --Jackpots
  SET @METER_GAMES_PLAYED = 5                     --Games Played
  SET @METER_GAMES_WON = 6                        --Games Won (NOT an AFIP meter, but affects resets & rollovers)

  --Temporary table for terminals connected
  CREATE TABLE #TEMP_TERMINALS
  (
    TERMINAL_ID         INT
  , REGISTRATION_CODE   NVARCHAR(50)
  );

  --Temporary table for meters
  CREATE TABLE #TEMP_METERS
  (
    TERMINAL_ID           INT
  , REGISTRATION_CODE     NVARCHAR(50)
  , SAS_ACCOUNTING_DENOM  MONEY
  , METER_DATE            DATETIME
  , METER_CODE            INT
  , METER_TYPE            INT
  , INI_VALUE             BIGINT
  , FIN_VALUE             BIGINT
  , GROUP_ID              BIGINT
  , METER_ORIGIN          INT
  , METER_MAX_VALUE       BIGINT
  , INTERVAL_FROM         DATETIME
  , INTERVAL_TO           DATETIME
  );

  SET @INTERVAL_FROM = @pStartDateTime;
  SET @INTERVAL_TO   = @pEndDateTime;

  IF (@pPendingType = @PENDING_TYPE_REQUEST)
  BEGIN
    --Request type
    --Get Terminal ID
    SET @TERMINAL_ID = ( SELECT  MIN(te.TE_TERMINAL_ID)
                           FROM  TERMINALS te
                          WHERE  (te.TE_REGISTRATION_CODE = @pTerminalsAFIP)
                            -- Only show terminal with the terminal type
                            AND  ((ISNULL(@pTerminalType, '') = '') OR (te.TE_TERMINAL_TYPE IN (SELECT tt.SST_VALUE
                                                                                                FROM SplitStringIntoTable(@pTerminalType, ',', 1) AS tt)))
                        );

    --Search terminal last meter date previous
    INSERT INTO #TEMP_METERS
           EXEC SP_AFIP_GetTerminalLastPreviousMeters
                     @TERMINAL_ID
                   , @pTerminalsAFIP
                   , @pEndDateTime
                   ;
  END --@PENDING_TYPE_REQUEST
  ELSE
  BEGIN
    --Terminals Type
    --Get terminals connected
    INSERT INTO #TEMP_TERMINALS
    (
      TERMINAL_ID, REGISTRATION_CODE
    )
    SELECT  te.TE_TERMINAL_ID
          , te.TE_REGISTRATION_CODE
      FROM  TERMINALS te
        --Was the terminal active?
     WHERE  EXISTS ( SELECT   1
                       FROM   TERMINALS_CONNECTED tc
                      WHERE   tc.TC_DATE >= @INTERVAL_FROM
                        AND   tc.TC_DATE <  @INTERVAL_TO
                        AND   tc.TC_MASTER_ID = te.TE_MASTER_ID
                        AND   tc.TC_STATUS = @pTerminalStatusActive
                   )
       --Are there any terminal to filter with?
       AND  ((ISNULL(@pTerminalsAFIP, '') = '') OR (te.TE_REGISTRATION_CODE IN (SELECT tp.SST_VALUE
                                                                                FROM SplitStringIntoTable(@pTerminalsAFIP, ',', 1) AS tp)))
       -- Only show terminal with the terminal type
       AND  ((ISNULL(@pTerminalType, '') = '') OR (te.TE_TERMINAL_TYPE IN (SELECT tt.SST_VALUE
                                                                           FROM SplitStringIntoTable(@pTerminalType, ',', 1) AS tt)))

    --Get meters for the terminals connected
    INSERT INTO #TEMP_METERS
    (
      TERMINAL_ID
    , REGISTRATION_CODE
    , SAS_ACCOUNTING_DENOM
    , METER_DATE
    , METER_CODE
    , METER_TYPE
    , INI_VALUE
    , FIN_VALUE
    , GROUP_ID
    , METER_ORIGIN
    , METER_MAX_VALUE
    , INTERVAL_FROM
    , INTERVAL_TO
    )
    SELECT  te.TERMINAL_ID
          , te.REGISTRATION_CODE
          , th.TSMH_SAS_ACCOUNTING_DENOM
          , th.TSMH_DATETIME
          , th.TSMH_METER_CODE
          , th.TSMH_TYPE
          , ISNULL(th.TSMH_METER_INI_VALUE, 0)
          , ISNULL(th.TSMH_METER_FIN_VALUE, 0)
          , th.TSMH_GROUP_ID
          , th.TSMH_METER_ORIGIN
          , th.TSMH_METER_MAX_VALUE
          , @INTERVAL_FROM
          , @INTERVAL_TO
     FROM TERMINAL_SAS_METERS_HISTORY AS th
    INNER JOIN #TEMP_TERMINALS AS te ON th.TSMH_TERMINAL_ID = te.TERMINAL_ID
    WHERE th.TSMH_DATETIME >= @INTERVAL_FROM
      AND th.TSMH_DATETIME <  @INTERVAL_TO
      AND th.TSMH_TYPE IN (
                             @TYPE_DAILY,
                             @TYPE_ROLLOVER, @TYPE_SAS_ACCOUNT_DENOM_CHANGE, @TYPE_SERVICE_RAM_CLEAR,
                             @TYPE_GROUP_ROLLOVER, @TYPE_GROUP_SAS_ACCOUNT_DENOM_CHANGE, @TYPE_GROUP_SERVICE_RAM_CLEAR
                          )
      AND th.TSMH_METER_CODE IN (@METER_COIN_IN, @METER_COIN_OUT, @METER_JACKPOTS, @METER_GAMES_PLAYED, @METER_GAMES_WON);

    -- Get terminals connected with no meters between the dates
    DECLARE curTerminalsWithNoMeters CURSOR FOR
      SELECT te.TERMINAL_ID
           , te.REGISTRATION_CODE
      FROM #TEMP_TERMINALS te
      WHERE NOT EXISTS (SELECT *
                        FROM #TEMP_METERS tm
                        WHERE tm.TERMINAL_ID = te.TERMINAL_ID);

    --Open cursor
    OPEN curTerminalsWithNoMeters

    FETCH NEXT FROM curTerminalsWithNoMeters
      INTO @TERMINAL_ID
         , @REGISTRATION_CODE;

    --Loop the cursor
    WHILE @@FETCH_STATUS = 0
    BEGIN
      -- For each terminal connected with no meters between the dates:
      --  * Search terminal last meter date previous
      INSERT INTO #TEMP_METERS
             EXEC SP_AFIP_GetTerminalLastPreviousMeters
                       @TERMINAL_ID
                     , @REGISTRATION_CODE
                     , @pEndDateTime
                     ;
      --Get next record
      FETCH NEXT FROM curTerminalsWithNoMeters
      INTO @TERMINAL_ID
         , @REGISTRATION_CODE;
    END --end while CURSOR

    --Free memory
    CLOSE curTerminalsWithNoMeters
    DEALLOCATE curTerminalsWithNoMeters
  END

  --Return meters data
  SELECT *
  FROM #TEMP_METERS
  ORDER BY TERMINAL_ID, METER_DATE, METER_TYPE, METER_CODE;

  --Drop temporary tables
  DROP TABLE #TEMP_TERMINALS;

  DROP TABLE #TEMP_METERS;
END
GO

GRANT EXECUTE ON [dbo].[SP_AFIP_GetTerminalMetersHistory] TO [WGGUI] WITH GRANT OPTION
GO


/******* TRIGGERS *******/
-- TRIGGER UPDATE PHOTO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InHouseAPI_Photo_Updated]') AND TYPE IN (N'TR'))
    DROP TRIGGER [dbo].[InHouseAPI_Photo_Updated]
GO

CREATE TRIGGER [dbo].[InHouseAPI_Photo_Updated] ON [dbo].[ACCOUNT_PHOTO]
    AFTER INSERT
AS
BEGIN
    SET NOCOUNT ON
    
	  UPDATE   ACCOUNT_PHOTO
		   SET   APH_LAST_UPDATE = GETDATE()
		  FROM   INSERTED
		 WHERE   ACCOUNT_PHOTO.APH_ACCOUNT_ID = INSERTED.APH_ACCOUNT_ID
END
GO

-- CREATE TRIGGER PLAY_SESSIONS

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InHouseAPI_Play_Sessions_Insert]') AND TYPE IN (N'TR'))
    DROP TRIGGER [dbo].[InHouseAPI_Play_Sessions_Insert]
GO

CREATE TRIGGER [dbo].[InHouseAPI_Play_Sessions_Insert] ON [dbo].[play_sessions]
    AFTER INSERT
AS
BEGIN
    SET NOCOUNT ON

    DECLARE @IsEnabledInHouseAPI          AS  NVARCHAR(50)
    DECLARE @IsEnabledPlaySessionsVisit   AS  NVARCHAR(50)

    SET @IsEnabledInHouseAPI = (SELECT  GP_KEY_VALUE
                                  FROM  GENERAL_PARAMS
                                 WHERE  GP_GROUP_KEY   = 'InHouseAPI'
                                   AND  GP_SUBJECT_KEY = 'Enabled')

    SET @IsEnabledInHouseAPI = ISNULL(@IsEnabledInHouseAPI, '0')

    --InHouseAPI is enabled?
    IF (@IsEnabledInHouseAPI = '1')
    BEGIN
        SET @IsEnabledPlaySessionsVisit = (SELECT  GP_KEY_VALUE
                                             FROM  GENERAL_PARAMS
                                            WHERE  GP_GROUP_KEY   = 'InHouseAPI'
                                              AND  GP_SUBJECT_KEY = 'PlaySessionEnabled')

        SET @IsEnabledPlaySessionsVisit = ISNULL(@IsEnabledPlaySessionsVisit, '0')

        --InHouseAPI PlaySession is enabled?
        IF (@IsEnabledPlaySessionsVisit = '1')
        BEGIN
            --Insert (if needed) in TODAY_VISITS
            DECLARE @Started        AS  DATETIME
            DECLARE @GamingDay      AS  INT
            DECLARE @CustomerId     AS  BIGINT

            DECLARE curPlaySessions CURSOR
            FOR
                SELECT ps_started
                     , ps_account_id
                FROM INSERTED

            OPEN curPlaySessions

            FETCH NEXT FROM curPlaySessions
             INTO   @Started
                ,   @CustomerId

            WHILE (@@FETCH_STATUS = 0)
            BEGIN
                IF ((@Started IS NOT NULL) AND (@CustomerId IS NOT NULL))
                BEGIN
                    --Get gamingday from datetime
                    SET @GamingDay = dbo.GamingDayFromDateTime(@Started)

                    EXEC dbo.SP_InHouseAPI_Insert_Today_Visits
                      @GamingDay
                    , @CustomerId
                END

                FETCH NEXT FROM curPlaySessions
                 INTO   @Started
                    ,   @CustomerId
            END

            CLOSE curPlaySessions
            DEALLOCATE curPlaySessions
        END
    END
END
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InHouseAPI_Customer_Visits_Insert]') AND TYPE IN (N'TR'))
    DROP TRIGGER [dbo].[InHouseAPI_Customer_Visits_Insert]
GO

CREATE TRIGGER [dbo].[InHouseAPI_Customer_Visits_Insert] ON [dbo].[customer_visits]
    AFTER INSERT
AS
BEGIN
    SET NOCOUNT ON

    DECLARE @IsEnabledInHouseAPI          AS  NVARCHAR(50)
    DECLARE @IsEnabledReceptionVisit      AS  NVARCHAR(50)

    SET @IsEnabledInHouseAPI = (SELECT  GP_KEY_VALUE
                                  FROM  GENERAL_PARAMS
                                 WHERE  GP_GROUP_KEY   = 'InHouseAPI'
                                   AND  GP_SUBJECT_KEY = 'Enabled')

    SET @IsEnabledInHouseAPI = ISNULL(@IsEnabledInHouseAPI, '0')

    --InHouseAPI is enabled?
    IF (@IsEnabledInHouseAPI = '1')
    BEGIN
        SET @IsEnabledReceptionVisit = (SELECT  GP_KEY_VALUE
                                          FROM  GENERAL_PARAMS
                                         WHERE  GP_GROUP_KEY   = 'InHouseAPI'
                                           AND  GP_SUBJECT_KEY = 'ReceptionVisitEnabled')

        SET @IsEnabledReceptionVisit = ISNULL(@IsEnabledReceptionVisit, '0')

        --InHouseAPI ReceptionVisit is enabled?
        IF (@IsEnabledReceptionVisit = '1')
        BEGIN
            --Insert (if needed) in TODAY_VISITS
            DECLARE @GamingDay      AS  INT
            DECLARE @CustomerId     AS  BIGINT

            DECLARE curCustomerVisits   CURSOR
            FOR
                SELECT  cut_gaming_day
                      , cut_customer_id
                FROM INSERTED

            OPEN curCustomerVisits

            FETCH NEXT FROM curCustomerVisits
             INTO   @GamingDay
                ,   @CustomerId

            WHILE (@@FETCH_STATUS = 0)
            BEGIN
                EXEC dbo.SP_InHouseAPI_Insert_Today_Visits
                    @GamingDay
                  , @CustomerId

                FETCH NEXT FROM curCustomerVisits
                 INTO   @GamingDay
                    ,   @CustomerId
            END

            CLOSE curCustomerVisits
            DEALLOCATE curCustomerVisits
        END
    END
END
GO


-- CREATE TRIGGER ACCOUNTS_UPDATE
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InHouseAPI_Accounts_Updated]') AND TYPE IN (N'TR'))
    DROP TRIGGER [dbo].[InHouseAPI_Accounts_Updated]
GO

CREATE TRIGGER [dbo].[InHouseAPI_Accounts_Updated] ON [dbo].[ACCOUNTS]
    AFTER INSERT, UPDATE
AS
BEGIN
    SET NOCOUNT ON

    UPDATE   ACCOUNTS
       SET   AC_LAST_UPDATE_IN_LOCAL_TIME = GETDATE()
         ,   AC_LAST_UPDATE_IN_UTC_TIME   = GETUTCDATE()
      FROM   INSERTED
     WHERE   ACCOUNTS.AC_ACCOUNT_ID = INSERTED.AC_ACCOUNT_ID
END
GO