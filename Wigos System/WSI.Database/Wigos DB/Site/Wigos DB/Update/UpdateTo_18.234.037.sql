/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 233;

SET @New_ReleaseId = 234;
SET @New_ScriptName = N'UpdateTo_18.234.037.sql';
SET @New_Description = N'Added SP for CONCEPTS, Update alarm_catalog, Added fields for terminals';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/******* TABLES *******/

IF EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[terminals]') AND name = 'te_top_award')
      ALTER TABLE dbo.terminals alter column te_top_award MONEY NULL
GO
IF EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[terminals]') AND name = 'te_max_bet')
      ALTER TABLE dbo.terminals alter column te_max_bet MONEY NULL
GO

--RENAME TE_CUIM--
IF EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[terminals]') AND name = 'te_cuim')
      EXECUTE sp_rename N'terminals.te_cuim', N'te_machine_id', 'COLUMN'
GO
--CHANGE SIZE DATA--
IF EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[terminals]') AND name = 'te_machine_id')
      ALTER TABLE dbo.terminals alter column te_machine_id NVARCHAR(50) NULL
GO
		
/******* INDEXES *******/

/******* RECORDS *******/

--CREATE GP--
IF NOT EXISTS (SELECT * FROM general_params WHERE gp_group_key LIKE 'EGM' AND gp_subject_key LIKE 'MachineId.Name' )
  INSERT INTO general_params(gp_group_key,gp_subject_key,gp_key_value) VALUES('EGM','MachineId.Name','')
GO

IF NOT EXISTS (SELECT * FROM alarm_catalog WHERE alcg_alarm_code = 262153 AND alcg_language_id = 9)
  INSERT INTO [dbo].[alarm_catalog] ([alcg_alarm_code],[alcg_language_id],[alcg_type],[alcg_name],[alcg_description],[alcg_visible])
       VALUES (262153,9,0,'Collection 0','A collection with a value of 0 was performed',1);

GO	   
	   
IF NOT EXISTS (SELECT * FROM alarm_catalog WHERE alcg_alarm_code = 262153 AND alcg_language_id = 10)
  INSERT INTO [dbo].[alarm_catalog] ([alcg_alarm_code],[alcg_language_id],[alcg_type],[alcg_name],[alcg_description],[alcg_visible])
       VALUES (262153,10,0,'Recaudaci�n a 0','Se ha realizado una recaudaci�n con valor 0',1);
GO
	   
IF NOT EXISTS (SELECT * FROM alarm_catalog_per_category WHERE alcc_alarm_code = 262153 AND alcc_category = 40)
  INSERT INTO [dbo].[alarm_catalog_per_category] ([alcc_alarm_code],[alcc_category],[alcc_type],[alcc_datetime])
       VALUES (262153,40,0,GETDATE());
GO

-- DELETE GP--
DELETE FROM general_params WHERE gp_group_key = 'Cage' AND gp_subject_key = 'Meters.Enabled'
GO

/******* STORED PROCEDURES *******/

/******* CONCEPTS *******/
/*    CREACI�N DEL CONCEPTO PROPINAS PRE-DEFINIDO EN EL SISTEMA
      SE CREAN USOS DE ESTE CONCEPTO PARA LOS CAJEROS Y MESAS DE JUEGO */      
        -- Propinas ([1000])
      IF NOT EXISTS (SELECT TOP 1 CC_CONCEPT_ID FROM CAGE_CONCEPTS WHERE CC_CONCEPT_ID = 1000)
      INSERT INTO CAGE_CONCEPTS
                  ( CC_CONCEPT_ID, CC_DESCRIPTION, CC_NAME
                  , CC_IS_PROVISION, CC_SHOW_IN_REPORT
                  , CC_TYPE, CC_ENABLED )
            VALUES
                  ( 1000, 'Propinas', 'Propinas'
                  , 1, 1
                  , 1, 1)
                  
      -- PROPINAS-CAJAS ([1000]-[10])
      -- Create Relation
      IF NOT EXISTS (SELECT TOP 1 * FROM CAGE_SOURCE_TARGET_CONCEPTS WHERE CSTC_CONCEPT_ID = 1000 
                                                                       AND CSTC_SOURCE_TARGET_ID = 10)
      INSERT INTO CAGE_SOURCE_TARGET_CONCEPTS
           ( CSTC_CONCEPT_ID, CSTC_SOURCE_TARGET_ID
           , CSTC_TYPE, CSTC_ONLY_NATIONAL_CURRENCY
           , CSTC_ENABLED
           , CSTC_CASHIER_ACTION
               , CSTC_CASHIER_BTN_GROUP
               , CSTC_CASHIER_CONTAINER)
     VALUES
           ( 1000, 10
           , 0, 0
           , 1
           , 0 
               , 'STR_CASHIER_CONCEPTS_IN'
               , 0)
    -- Create Meters
    EXEC CageCreateMeters
                  @pSourceTargetId = 10
              , @pConceptId = 1000
              , @pCageSessionId = NULL
              , @CreateMetersOption = 1
              , @CurrencyForced = NULL
              
              
      -- PROPINAS-MESAS ([1000]-[11])
      -- Create Relation
      IF NOT EXISTS (SELECT TOP 1 * FROM CAGE_SOURCE_TARGET_CONCEPTS WHERE CSTC_CONCEPT_ID = 1000 
                                                                       AND CSTC_SOURCE_TARGET_ID = 11)
      INSERT INTO CAGE_SOURCE_TARGET_CONCEPTS
           ( CSTC_CONCEPT_ID, CSTC_SOURCE_TARGET_ID
           , CSTC_TYPE, CSTC_ONLY_NATIONAL_CURRENCY
           , CSTC_ENABLED )
     VALUES
           ( 1000, 11
           , 2, 0
           , 1 )
    -- Create Meters
    EXEC CageCreateMeters
                  @pSourceTargetId = 11
              , @pConceptId = 1000
              , @pCageSessionId = NULL
              , @CreateMetersOption = 1
              , @CurrencyForced = NULL

                    
 /* STORED PROCEDURES  */

  /* 
CageGetGlobalReportData  
Se ha incluido la condici�n �AND   CC.CC_SHOW_IN_REPORT = 1� para los meters globales
*/

 IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CageGetGlobalReportData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CageGetGlobalReportData]
GO
CREATE PROCEDURE [dbo].[CageGetGlobalReportData]
        @pCageSessionIds NVARCHAR(MAX) = NULL
  AS
BEGIN

      DECLARE @NationalIsoCode NVARCHAR(3)
      DECLARE @CageOperationType_FromTerminal INT

      SET @CageOperationType_FromTerminal = 104
      
      -- TABLE[0]: Get cage stock
    SELECT CGS_ISO_CODE                                          
         , CGS_DENOMINATION                                      
             , CGS_QUANTITY 
      FROM (SELECT CGS_ISO_CODE                                          
                   , CGS_DENOMINATION                                      
                         , CGS_QUANTITY                                          
                    FROM CAGE_STOCK
                  
                  UNION ALL
                  
                  SELECT C.CH_ISO_CODE AS CGS_ISO_CODE
                        , C.CH_DENOMINATION AS CGS_DENOMINATION
                        , CST.CHSK_QUANTITY AS CGS_QUANTITY
                    FROM CHIPS_STOCK AS CST
                           INNER JOIN CHIPS AS C ON CST.CHSK_CHIP_ID = C.CH_CHIP_ID
                  ) AS _tbl         
                                                   
  ORDER BY CGS_ISO_CODE                                          
              ,CASE WHEN CGS_DENOMINATION >= 0 THEN CGS_DENOMINATION 
                      ELSE CGS_DENOMINATION * (-100000) END -- show negative denominations at the end (coins, cheks, etc.)
                
                                            

      IF @pCageSessionIds IS NOT NULL
      BEGIN
            -- Session IDs Split
            SELECT SST_VALUE AS CAGE_SESSION_ID INTO #TMP_CAGE_SESSIONS FROM [SplitStringIntoTable] (@pCageSessionIds,','  , 1)
      
          -- TABLE[1]: Get cage sessions information
            SELECT      CSM.CSM_SOURCE_TARGET_ID AS CM_SOURCE_TARGET_ID
                    , CSM.CSM_CONCEPT_ID AS CM_CONCEPT_ID
                    , CSM.CSM_ISO_CODE AS CM_ISO_CODE
                    , SUM(CSM.CSM_VALUE_IN) AS CM_VALUE_IN
                    , SUM(CSM.CSM_VALUE_OUT) AS CM_VALUE_OUT
                    , CST.CST_SOURCE_TARGET_NAME
                    , CC.CC_DESCRIPTION
              FROM      CAGE_SESSION_METERS AS CSM
                        INNER JOIN CAGE_SOURCE_TARGET_CONCEPTS AS CSTC ON CSTC.CSTC_CONCEPT_ID = CSM.CSM_CONCEPT_ID AND CSTC.CSTC_SOURCE_TARGET_ID = CSM.CSM_SOURCE_TARGET_ID
                        INNER JOIN CAGE_CONCEPTS AS CC ON CC.CC_CONCEPT_ID = CSTC.CSTC_CONCEPT_ID
                        INNER JOIN CAGE_SOURCE_TARGET AS CST ON CST.CST_SOURCE_TARGET_ID = CSTC.CSTC_SOURCE_TARGET_ID
            WHERE      CSM.CSM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
               AND      CC.CC_SHOW_IN_REPORT = 1
               AND      CC.CC_ENABLED = 1
               AND      CC.CC_TYPE = 1
             AND  CSTC.CSTC_ENABLED = 1
        GROUP BY  CSM.CSM_SOURCE_TARGET_ID, CSM.CSM_CONCEPT_ID, CSM.CSM_ISO_CODE, CST.CST_SOURCE_TARGET_NAME, CC.CC_DESCRIPTION
        ORDER BY  CSM.CSM_SOURCE_TARGET_ID                   

            -- TABLE[2]: Get dynamic liabilities
            SELECT      CC.CC_CONCEPT_ID AS LIABILITY_CONCEPT_ID
                    , CC.CC_DESCRIPTION AS LIABILITY_NAME 
                    , CSM.CSM_ISO_CODE AS LIABILITY_ISO_CODE
                    , SUM(CSM.CSM_VALUE) AS LIABILITY_VALUE
              FROM      CAGE_SESSION_METERS AS CSM
                        INNER JOIN CAGE_CONCEPTS AS CC ON CC.CC_CONCEPT_ID = CSM.CSM_CONCEPT_ID
            WHERE      CC.CC_IS_PROVISION = 1
               AND      CSM.CSM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS) 
      GROUP BY    CC.CC_CONCEPT_ID, CC.CC_DESCRIPTION, CSM.CSM_ISO_CODE
      ORDER BY CC.CC_CONCEPT_ID ASC
      
            -- TABLE[3]: Get session sumary
            SELECT @NationalIsoCode = GP_KEY_VALUE 
              FROM GENERAL_PARAMS 
             WHERE GP_GROUP_KEY = 'RegionalOptions' 
               AND GP_SUBJECT_KEY = 'CurrencyISOCode'

            SELECT CMD_ISO_CODE AS ISO_CODE
               , ISNULL(ORIGIN, 0) AS ORIGIN
               , SUM(CMD_DEPOSITS) AS DEPOSITS
               , SUM(CMD_WITHDRAWALS) AS WITHDRAWALS
                  
              FROM      (SELECT CMD_ISO_CODE
                               , ORIGIN
                               , SUM(CMD_DEPOSITS) CMD_DEPOSITS
                               , SUM(CMD_WITHDRAWALS) CMD_WITHDRAWALS
                           
                           FROM     (SELECT CASE WHEN CMD_QUANTITY >= 0 THEN NULL
                                                     ELSE CMD_QUANTITY END  AS ORIGIN
                                           , CMD_ISO_CODE
                                           , CMD_DENOMINATION
                                           , CASE WHEN CGM_TYPE >= 100 AND CGM_TYPE NOT IN (2, 102, 99, 199) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY < 0 THEN CMD_DENOMINATION
                                                                                                                                                                          ELSE CMD_QUANTITY * CMD_DENOMINATION END), 0)
                                                     ELSE 0 END AS CMD_DEPOSITS
                                           , CASE WHEN CGM_TYPE < 100 AND CGM_TYPE NOT IN (2, 102, 99, 199) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY < 0 THEN CMD_DENOMINATION             
                                                                                                                                                                           ELSE CMD_QUANTITY * CMD_DENOMINATION END), 0)                              
                                                      ELSE 0 END AS CMD_WITHDRAWALS

                                      FROM CAGE_MOVEMENTS 
                                               INNER JOIN   CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          
                                     WHERE CGM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)

                                GROUP BY CMD_ISO_CODE, CMD_DENOMINATION, CGM_TYPE, CMD_QUANTITY
                                ) AS T1
                                
                        GROUP BY ORIGIN,CMD_ISO_CODE

                        UNION ALL 

                        --The next one is for the Stackers / Terminals collection
                        SELECT @NationalIsoCode AS CMD_ISO_CODE                                                                                     
                              , 0 AS ORIGIN
                             , CASE WHEN ISNULL(MCD_FACE_VALUE,0) < 0 THEN MCD_NUM_COLLECTED    
                                         ELSE (ISNULL(MCD_FACE_VALUE,0) * ISNULL(MCD_NUM_COLLECTED,0)) END AS CMD_DEPOSITS     
                              , 0 AS CMD_WITHDRAWALS                                                                                     
                          FROM MONEY_COLLECTION_DETAILS                                                                                    
                                INNER JOIN MONEY_COLLECTIONS ON MONEY_COLLECTIONS.MC_COLLECTION_ID = MONEY_COLLECTION_DETAILS.MCD_COLLECTION_ID  
                                INNER JOIN CAGE_MOVEMENTS ON MC_COLLECTION_ID = CAGE_MOVEMENTS.CGM_MC_COLLECTION_ID                              
                         WHERE CAGE_MOVEMENTS.CGM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
                           AND CAGE_MOVEMENTS.CGM_TYPE = @CAGEOPERATIONTYPE_FROMTERMINAL
                           AND ISNULL(MCD_NUM_COLLECTED,0) > 0
                           
                        ) AS _TBL_GLB

         GROUP BY _TBL_GLB.CMD_ISO_CODE, _TBL_GLB.ORIGIN
         ORDER BY _TBL_GLB.CMD_ISO_CODE


            DROP TABLE #TMP_CAGE_SESSIONS 

      END -- IF @pCageSessionIds IS NOT NULL THEN
      
      ELSE
      BEGIN
      
            -- TABLE[1]: Get cage sessions information
            SELECT CM.CM_SOURCE_TARGET_ID
                  , CM.CM_CONCEPT_ID
                  , CM.CM_ISO_CODE
                  , CM.CM_VALUE_IN
                  , CM.CM_VALUE_OUT
                  , CST.CST_SOURCE_TARGET_NAME
                  , CC.CC_DESCRIPTION
              FROM CAGE_METERS AS CM
                     INNER JOIN CAGE_SOURCE_TARGET_CONCEPTS AS CSTC ON CSTC.CSTC_CONCEPT_ID = CM.CM_CONCEPT_ID AND CSTC.CSTC_SOURCE_TARGET_ID = CM.CM_SOURCE_TARGET_ID
                     INNER JOIN CAGE_CONCEPTS AS CC ON CC.CC_CONCEPT_ID = CSTC.CSTC_CONCEPT_ID
                     INNER JOIN CAGE_SOURCE_TARGET AS CST ON CST.CST_SOURCE_TARGET_ID = CSTC.CSTC_SOURCE_TARGET_ID
            WHERE CC.CC_TYPE = 1
               AND CC.CC_ENABLED = 1
             AND CSTC.CSTC_ENABLED = 1
             AND  CC.CC_SHOW_IN_REPORT = 1
        ORDER BY CM.CM_SOURCE_TARGET_ID 

            
            -- TABLE[2]: Get dynamic liabilities
            SELECT CC.CC_CONCEPT_ID AS LIABILITY_CONCEPT_ID
                  , CC.CC_DESCRIPTION AS LIABILITY_NAME 
                   , CM.CM_ISO_CODE AS LIABILITY_ISO_CODE
                  , SUM(CM.CM_VALUE) AS LIABILITY_VALUE
              FROM CAGE_METERS AS CM
                     INNER JOIN CAGE_CONCEPTS AS CC ON CC.CC_CONCEPT_ID = CM.CM_CONCEPT_ID
            WHERE CC.CC_IS_PROVISION = 1
      GROUP BY CC.CC_CONCEPT_ID, CC.CC_DESCRIPTION, CM.CM_ISO_CODE
      ORDER BY CC.CC_CONCEPT_ID ASC 
      
                  -- TABLE[3]: Get session sumary

            SELECT @NationalIsoCode = GP_KEY_VALUE 
              FROM GENERAL_PARAMS 
             WHERE GP_GROUP_KEY = 'RegionalOptions' 
               AND GP_SUBJECT_KEY = 'CurrencyISOCode'

            SELECT CMD_ISO_CODE AS ISO_CODE
               , ISNULL(ORIGIN, 0) AS ORIGIN
               , SUM(CMD_DEPOSITS) AS DEPOSITS
               , SUM(CMD_WITHDRAWALS) AS WITHDRAWALS
                  
              FROM      (SELECT CMD_ISO_CODE
                               , ORIGIN
                               , SUM(CMD_DEPOSITS) CMD_DEPOSITS
                               , SUM(CMD_WITHDRAWALS) CMD_WITHDRAWALS
                           
                           FROM     (SELECT CASE WHEN CMD_QUANTITY >= 0 THEN NULL
                                                     ELSE CMD_QUANTITY END  AS ORIGIN
                                           , CMD_ISO_CODE
                                           , CMD_DENOMINATION
                                           , CASE WHEN CGM_TYPE >= 100 AND CGM_TYPE NOT IN (2, 102, 99, 199) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY < 0 THEN CMD_DENOMINATION
                                                                                                                                                                          ELSE CMD_QUANTITY * CMD_DENOMINATION END), 0)
                                                     ELSE 0 END AS CMD_DEPOSITS
                                           , CASE WHEN CGM_TYPE < 100 AND CGM_TYPE NOT IN (2, 102, 99, 199) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY < 0 THEN CMD_DENOMINATION             
                                                                                                                                                                           ELSE CMD_QUANTITY * CMD_DENOMINATION END), 0)                              
                                                      ELSE 0 END AS CMD_WITHDRAWALS

                                      FROM CAGE_MOVEMENTS 
                                               INNER JOIN   CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          

                                GROUP BY CMD_ISO_CODE, CMD_DENOMINATION, CGM_TYPE, CMD_QUANTITY
                                ) AS T1
                                
                        GROUP BY ORIGIN,CMD_ISO_CODE

                        UNION ALL 

                        --The next one is for the Stackers / Terminals collection
                        SELECT @NationalIsoCode AS CMD_ISO_CODE                                                                                     
                              , 0 AS ORIGIN
                             , CASE WHEN ISNULL(MCD_FACE_VALUE,0) < 0 THEN MCD_NUM_COLLECTED    
                                         ELSE (ISNULL(MCD_FACE_VALUE,0) * ISNULL(MCD_NUM_COLLECTED,0)) END AS CMD_DEPOSITS     
                              , 0 AS CMD_WITHDRAWALS                                                                                     
                          FROM MONEY_COLLECTION_DETAILS                                                                                    
                                INNER JOIN MONEY_COLLECTIONS ON MONEY_COLLECTIONS.MC_COLLECTION_ID = MONEY_COLLECTION_DETAILS.MCD_COLLECTION_ID  
                                INNER JOIN CAGE_MOVEMENTS ON MC_COLLECTION_ID = CAGE_MOVEMENTS.CGM_MC_COLLECTION_ID                              
                         WHERE CAGE_MOVEMENTS.CGM_TYPE = @CAGEOPERATIONTYPE_FROMTERMINAL
                           AND ISNULL(MCD_NUM_COLLECTED,0) > 0
                           
                        ) AS _TBL_GLB

         GROUP BY _TBL_GLB.CMD_ISO_CODE, _TBL_GLB.ORIGIN
         ORDER BY _TBL_GLB.CMD_ISO_CODE

      
      END --ELSE
                   
END -- CageGetGlobalReportData
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CageCreateMeters]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CageCreateMeters]
GO
CREATE PROCEDURE [dbo].[CageCreateMeters]
        @pSourceTargetId BIGINT
      , @pConceptId BIGINT
      , @pCageSessionId BIGINT = NULL
      , @CreateMetersOption INT
      , @CurrencyForced VARCHAR(3) =  NULL
  AS
BEGIN   

                -- @@CreateMetersOption  =    0 - Create in cage_meters and cage_session_meters
                --                       =    1 - Create in cage_meters only
                --                       =    2 - Create in cage_session_meters only 
                --                       =    3 - Create in cage_meters and all opened cage sessions 


                DECLARE @Currencies VARCHAR(100)
                DECLARE @OnlyNationalCurrency BIT
                DECLARE @CageSessionId BIGINT
                
                SET @OnlyNationalCurrency = 0
                
                SELECT @OnlyNationalCurrency = CSTC_ONLY_NATIONAL_CURRENCY 
                  FROM CAGE_SOURCE_TARGET_CONCEPTS 
                 WHERE CSTC_SOURCE_TARGET_ID = @pSourceTargetId
                   AND  CSTC_CONCEPT_ID = @pConceptId  

                -- Get national currency
                IF @OnlyNationalCurrency = 1 AND @CurrencyForced IS NULL
                               SELECT @Currencies = GP_KEY_VALUE FROM GENERAL_PARAMS 
         WHERE GP_GROUP_KEY = 'RegionalOptions' 
           AND GP_SUBJECT_KEY = 'CurrencyISOCode'
    
    -- Get accepted currencies
    ELSE
    BEGIN
                               IF @CurrencyForced IS NOT NULL
                                               SET @Currencies = @CurrencyForced
                               ELSE
                                               SELECT @Currencies = GP_KEY_VALUE FROM GENERAL_PARAMS 
                                                WHERE GP_GROUP_KEY = 'RegionalOptions' 
                                                  AND GP_SUBJECT_KEY = 'CurrenciesAccepted'
    END
    
    -- Split currencies ISO codes
   SELECT SST_VALUE AS CURRENCY_ISO_CODE INTO #TMP_CURRENCIES_ISO_CODES FROM [SplitStringIntoTable] (@Currencies, ';', 1)
                
                
                -- Create a new meters for each currency
                DECLARE @CurrentCurrency VARCHAR(3)
                
                DECLARE Curs CURSOR FOR 
    SELECT CURRENCY_ISO_CODE
      FROM #TMP_CURRENCIES_ISO_CODES 
      
                SET NOCOUNT ON;

    OPEN Curs

    FETCH NEXT FROM Curs INTO @CurrentCurrency
    
    WHILE @@FETCH_STATUS = 0
    BEGIN
                
                               -- CAGE METERS --
                               IF @CreateMetersOption = 0 OR @CreateMetersOption = 1 OR @CreateMetersOption = 3
                               BEGIN
                                               IF NOT EXISTS (SELECT TOP 1 CM_VALUE 
                                                                    FROM CAGE_METERS 
                                                                   WHERE CM_SOURCE_TARGET_ID = @pSourceTargetId
                                                                     AND CM_CONCEPT_ID = @pConceptId
                                                                     AND CM_ISO_CODE = @CurrentCurrency)
                                               INSERT INTO CAGE_METERS
                                                                                ( CM_SOURCE_TARGET_ID
                                                                                , CM_CONCEPT_ID
                                                                                , CM_ISO_CODE
                                                                               , CM_VALUE
                                                                                , CM_VALUE_IN
                                                                                , CM_VALUE_OUT )
                                                               VALUES
                                                                                ( @pSourceTargetId
                                                                                , @pConceptId
                                                                                , @CurrentCurrency
                                                                                , 0
                                                                                , 0
                                                                                , 0 )
                               END

                               -- CAGE SESSION METERS --
                               
                               IF (@CreateMetersOption = 0 OR @CreateMetersOption = 2) AND @pCageSessionId IS NOT NULL
                               BEGIN
                                                     IF NOT EXISTS (SELECT TOP 1 CSM_VALUE 
                                                                             FROM CAGE_SESSION_METERS 
                                                                            WHERE CSM_SOURCE_TARGET_ID = @pSourceTargetId
                                                                              AND CSM_CONCEPT_ID = @pConceptId
                                                                              AND CSM_ISO_CODE = @CurrentCurrency
                                                                              AND CSM_CAGE_SESSION_ID = @pCageSessionId)
                                                           INSERT INTO CAGE_SESSION_METERS( CSM_CAGE_SESSION_ID
                                                                       , CSM_SOURCE_TARGET_ID
                                                                       , CSM_CONCEPT_ID
                                                                       , CSM_ISO_CODE
                                                                      , CSM_VALUE
                                                                       , CSM_VALUE_IN
                                                                       , CSM_VALUE_OUT )
                                                                                             VALUES( @pCageSessionId
                                                                                                       , @pSourceTargetId
                                                                       , @pConceptId
                                                                       , @CurrentCurrency
                                                                       , 0
                                                                       , 0
                                                                       , 0 )
                               END
                               
                               IF (@CreateMetersOption = 3)
                               BEGIN
                               
                                                     DECLARE Cur2 CURSOR FOR SELECT cgs_cage_session_id  
                                                                                   FROM cage_sessions 
                                                                                  WHERE (cgs_close_datetime IS NULL)
                                                           
                                                     OPEN Cur2
                                                     FETCH NEXT FROM Cur2 INTO @CageSessionId
                                                     WHILE @@FETCH_STATUS = 0
                                                     BEGIN
                               
                                                           
                                                           IF NOT EXISTS (SELECT TOP 1 CSM_VALUE 
                                                         FROM CAGE_SESSION_METERS 
                                                        WHERE CSM_SOURCE_TARGET_ID = @pSourceTargetId
                                                          AND CSM_CONCEPT_ID = @pConceptId
                                                          AND CSM_ISO_CODE = @CurrentCurrency
                                                          AND CSM_CAGE_SESSION_ID = @CageSessionId)
                                               INSERT INTO CAGE_SESSION_METERS( CSM_CAGE_SESSION_ID
                                                                               , CSM_SOURCE_TARGET_ID
                                                                               , CSM_CONCEPT_ID
                                                                               , CSM_ISO_CODE
                                                                               , CSM_VALUE
                                                                               , CSM_VALUE_IN
                                                                               , CSM_VALUE_OUT )
                                                                                                          VALUES( @CageSessionId
                                                                               , @pSourceTargetId
                                                                               , @pConceptId
                                                                               , @CurrentCurrency
                                                                               , 0
                                                                               , 0
                                                                               , 0 )
                                                                               
                                                           FETCH NEXT FROM Cur2 INTO @CageSessionId
                                                     END
                                                           
                                                     CLOSE Cur2
                                                     DEALLOCATE Cur2                       
                               END
                               
                               FETCH NEXT FROM Curs INTO @CurrentCurrency
                
                END

    CLOSE Curs
    DEALLOCATE Curs
              
END -- CageCreateMeters
GO

DELETE FROM CHIPS WHERE CH_DENOMINATION < 0
GO

GRANT EXECUTE ON [dbo].[CageGetGlobalReportData] TO [wggui] WITH GRANT OPTION
GO

GRANT EXECUTE ON [dbo].[CageCreateMeters] TO [wggui] WITH GRANT OPTION 
GO
