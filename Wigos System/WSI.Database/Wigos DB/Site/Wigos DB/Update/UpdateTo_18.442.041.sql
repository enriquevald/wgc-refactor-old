/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 441;

SET @New_ReleaseId = 442;

SET @New_ScriptName = N'2018-05-08 - UpdateTo_18.442.041.sql';
SET @New_Description = N'New release v03.008.0001.01'; 

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO


/*********************************************************************************************************/

/**** GENERAL PARAM *****/

IF NOT EXISTS(SELECT   1 
                FROM   GENERAL_PARAMS 
               WHERE   GP_GROUP_KEY = 'Movibank'
                 AND   GP_SUBJECT_KEY = 'Device'
             )
BEGIN
    INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
    VALUES ('Movibank', 'Device', '0')
END
GO

/**** TABLES *****/

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[mobibank_areas]') AND type in (N'U'))
BEGIN
	CREATE TABLE [dbo].[mobibank_areas](
		[mba_id] [int] IDENTITY(1,1) NOT NULL,
		[mba_account_id] [int] NOT NULL,
		[mba_bank_id] [int] NOT NULL,
		[mba_name] [varchar](1000) NULL
    CONSTRAINT [PK_mobibank_areas] PRIMARY KEY CLUSTERED 
    (
      [mba_id] ASC
    ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]

END

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'mobile_banks' AND COLUMN_NAME = 'mb_user_id')
BEGIN
      ALTER TABLE       [dbo].[mobile_banks]
      ADD               mb_user_id      int     NULL
END
GO

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'mobile_banks' AND COLUMN_NAME = 'mb_lock')
BEGIN
      ALTER TABLE       [dbo].[mobile_banks]
      ADD               mb_lock      bit     NOT NULL DEFAULT 0
END
GO

/**** RECORDS *****/


/**** STORED PROCEDURES *****/


/******* GENERIC REPORT *******/

