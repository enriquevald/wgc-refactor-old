/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 95;

SET @New_ReleaseId = 96;
SET @New_ScriptName = N'UpdateTo_18.096.022.sql';
SET @New_Description = N'New column for site jackpots, new index for play sessions and new GPs for PromoBOX.';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/

/* Site Jackpot */
ALTER TABLE dbo.site_jackpot_instances ADD
                sji_minimum_bet money NOT NULL CONSTRAINT DF_site_jackpot_instances_sji_minimum_bet DEFAULT 0

GO

/****** INDEXES ******/

/* play_sessions */
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[play_sessions]') AND name = N'IX_ps_account_id_finished')
BEGIN
  CREATE NONCLUSTERED INDEX [IX_ps_account_id_finished] ON [dbo].[play_sessions] 
  (
	  [ps_account_id] ASC,
	  [ps_finished] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
END

GO

/****** RECORDS ******/

--
-- GENERAL PARAMS FOR PromoBOX

-- Points Level 01 Image
INSERT INTO [dbo].[wkt_images]
           (   [cim_image_id]
             , [cim_name]
             , [cim_resource_id]
             , [cim_expected_w]
             , [cim_expected_h] )
     VALUES
           (   2
             , 'IMG02'
             , 0
             , 0
             , 0 )
             
-- Points Level 02 Image
INSERT INTO [dbo].[wkt_images]
           (   [cim_image_id]
             , [cim_name]
             , [cim_resource_id]
             , [cim_expected_w]
             , [cim_expected_h] )
     VALUES
           (   3
             , 'IMG03'
             , 0
             , 0
             , 0 )
             
-- Points Level 03 Image
INSERT INTO [dbo].[wkt_images]
           (   [cim_image_id]
             , [cim_name]
             , [cim_resource_id]
             , [cim_expected_w]
             , [cim_expected_h] )
     VALUES
           (   4
             , 'IMG04'
             , 0
             , 0
             , 0 )
             
-- Points Level 04 Image
INSERT INTO [dbo].[wkt_images]
           (   [cim_image_id]
             , [cim_name]
             , [cim_resource_id]
             , [cim_expected_w]
             , [cim_expected_h] )
     VALUES
           (   5
             , 'IMG05'
             , 0
             , 0
             , 0 )

-- PromoBOX Split Gifts
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'WigosKiosk' AND GP_SUBJECT_KEY ='Gifts.SplitCategories')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('WigosKiosk', 'Gifts.SplitCategories', '0');

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'WigosKiosk' AND GP_SUBJECT_KEY ='Gifts.CategoryName01')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('WigosKiosk', 'Gifts.CategoryName01', '');

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'WigosKiosk' AND GP_SUBJECT_KEY ='Gifts.CategoryName02')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('WigosKiosk', 'Gifts.CategoryName02', '');

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'WigosKiosk' AND GP_SUBJECT_KEY ='Gifts.CategoryName03')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('WigosKiosk', 'Gifts.CategoryName03', '');

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'WigosKiosk' AND GP_SUBJECT_KEY ='Gifts.CategoryShort01')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('WigosKiosk', 'Gifts.CategoryShort01', '');

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'WigosKiosk' AND GP_SUBJECT_KEY ='Gifts.CategoryShort02')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('WigosKiosk', 'Gifts.CategoryShort02', '');

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'WigosKiosk' AND GP_SUBJECT_KEY ='Gifts.CategoryShort03')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('WigosKiosk', 'Gifts.CategoryShort03', '');


--
-- GENERAL PARAMS FOR PSAClient ( Classic Wigos )

DELETE FROM GENERAL_PARAMS WHERE GP_SUBJECT_KEY = 'HourStartOfSentToPSA' AND GP_GROUP_KEY = 'PSAClient'

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PSAClient' AND GP_SUBJECT_KEY ='BeforeSendWaitHours')
INSERT INTO general_params
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('PSAClient'
           ,'BeforeSendWaitHours'
           ,'4')

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PSAClient' AND GP_SUBJECT_KEY ='NumberOfMonthsToReport')
INSERT INTO general_params
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('PSAClient'
           ,'NumberOfMonthsToReport'
           ,'2')

