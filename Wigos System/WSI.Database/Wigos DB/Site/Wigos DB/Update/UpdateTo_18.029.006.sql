/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 28;

SET @New_ReleaseId = 29;
SET @New_ScriptName = N'UpdateTo_18.029.006.sql';
SET @New_Description = N'Reset te_external_id field for retired terminals; added ConcatOpeningTime function.';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/


/****** INDEXES ******/


/****** STORED PROCEDURES ******/
--------------------------------------------------------------------------------
-- PURPOSE: Concat the Opening Time to the parameter Date.
-- 
--  PARAMS:
--      - INPUT:
--        @SiteId int
--        @Date   datetime
--
--      - OUTPUT:
--
-- RETURNS:
--      datetime
--
--   NOTES:
--   - SiteId is not used for now.
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ConcatOpeningTime]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[ConcatOpeningTime]
GO
CREATE FUNCTION dbo.ConcatOpeningTime
  (@SiteId int,
   @Date   datetime)
RETURNS datetime
AS
BEGIN
  DECLARE @closing_hour    int
  DECLARE @closing_minutes int
  DECLARE @today_opening  datetime

  SET @closing_hour = ISNULL((SELECT   gp_key_value
                                FROM   general_params
                               WHERE   gp_group_key   = 'WigosGUI'
                                 AND   gp_subject_key = 'ClosingTime'), 6)

  SET @closing_minutes = ISNULL((SELECT   gp_key_value
                                   FROM   general_params
                                  WHERE   gp_group_key   = 'WigosGUI'
                                    AND   gp_subject_key = 'ClosingTimeMinutes'), 0)

  -- Trunc date to start of the day (00:00h).
  SET @today_opening = DATEADD(DAY, DATEDIFF(day, 0, @Date), 0)
  
  SET @today_opening = DATEADD(HOUR, @closing_hour, @today_opening)
  SET @today_opening = DATEADD(MINUTE, @closing_minutes, @today_opening)

  RETURN @today_opening
END -- ConcatOpeningTime

GO

--------------------------------------------------------------------------------
-- PURPOSE: Returns the Opening DateTime according to the DateTime parameter
-- 
--  PARAMS:
--      - INPUT:
--        @SiteId int
--        @Date   datetime
--
--      - OUTPUT:
--
-- RETURNS:
--      datetime
--
--   NOTES:
--   - SiteId is not used for now.
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Opening]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[Opening]
GO
CREATE FUNCTION dbo.Opening
  (@SiteId int,
   @Date   datetime)
RETURNS datetime
AS
BEGIN
  DECLARE @now           datetime
  DECLARE @today_opening datetime

  SET @now = @Date
  SET @today_opening = dbo.ConcatOpeningTime(@SiteId, @now)

  IF @today_opening > @now
  BEGIN
    SET @today_opening = DATEADD(DAY, -1, @today_opening)
  END

  RETURN @today_opening
END -- Opening

GO

--------------------------------------------------------------------------------
-- PURPOSE: Returns the Opening DateTime
-- 
--  PARAMS:
--      - INPUT:
--        @SiteId int
--
--      - OUTPUT:
--
-- RETURNS:
--      datetime
--
--   NOTES:
--   - SiteId is not used for now.
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TodayOpening]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[TodayOpening]
GO
CREATE FUNCTION dbo.TodayOpening
  (@SiteId int)
RETURNS datetime
AS
BEGIN
  RETURN dbo.Opening(@SiteId, GETDATE())
END -- TodayOpening

GO


/****** TRIGGERS ******/


/****** RECORDS ******/
   
/****** Reset protocol terminal id of all previously retired terminals ******/
UPDATE   TERMINALS
   SET   TE_EXTERNAL_ID = NULL
  FROM   TERMINALS 
 WHERE   TE_STATUS = 2
   AND   TE_EXTERNAL_ID IS NOT NULL;
GO
