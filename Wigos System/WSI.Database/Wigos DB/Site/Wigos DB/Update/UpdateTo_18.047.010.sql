/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 46;

SET @New_ReleaseId = 47;
SET @New_ScriptName = N'UpdateTo_18.047.010.sql';
SET @New_Description = N'Constancias: New columns in accounts and new tables account_major_prizes and documents.
 New table providers, new column te_prov_id in terminals and new triggers associated.
 New records in general params.';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/
/* accounts */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[accounts]') and name = 'ac_holder_name1')
  ALTER TABLE [dbo].[accounts]
          ADD [ac_holder_name1] [nvarchar](50) NULL
ELSE
  SELECT '***** Field accounts.ac_holder_name1 already exists *****';

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[accounts]') and name = 'ac_holder_name2')
  ALTER TABLE [dbo].[accounts]
          ADD [ac_holder_name2] [nvarchar](50) NULL
ELSE
  SELECT '***** Field accounts.ac_holder_name2 already exists *****';

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[accounts]') and name = 'ac_holder_name3')
  ALTER TABLE [dbo].[accounts]
          ADD [ac_holder_name3] [nvarchar](50) NULL
ELSE
  SELECT '***** Field accounts.ac_holder_name3 already exists *****';

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[accounts]') and name = 'ac_holder_id1')
  ALTER TABLE [dbo].[accounts]
          ADD ac_holder_id1 [nvarchar](20) NULL
ELSE
  SELECT '***** Field accounts.ac_holder_id1 already exists *****';

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[accounts]') and name = 'ac_holder_id2')
  ALTER TABLE [dbo].[accounts]
          ADD ac_holder_id2 [nvarchar](20) NULL
ELSE
  SELECT '***** Field accounts.ac_holder_id2 already exists *****';

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[accounts]') and name = 'ac_holder_document_id1')
  ALTER TABLE [dbo].[accounts]
          ADD ac_holder_document_id1 [bigint] NULL
ELSE
  SELECT '***** Field accounts.ac_holder_document_id1 already exists *****';

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[accounts]') and name = 'ac_holder_document_id2')
  ALTER TABLE [dbo].[accounts]
          ADD ac_holder_document_id2 [bigint] NULL
ELSE
  SELECT '***** Field accounts.ac_holder_document_id2 already exists *****';

/* terminals */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[terminals]') and name = 'te_prov_id')
  ALTER TABLE [dbo].[terminals]
          ADD te_prov_id [int] NULL
ELSE
  SELECT '***** Field terminals.te_prov_id already exists *****';

GO


--
-- Play sessions
--
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[play_sessions]') and name = 'ps_spent_used')
  BEGIN
    ALTER TABLE dbo.play_sessions ADD
                ps_spent_used money NULL,
                ps_spent_remaining  AS CASE WHEN (ps_spent_used >=0 AND ps_redeemable_played-ps_redeemable_won>0) THEN ps_redeemable_played-ps_redeemable_won-ps_spent_used ELSE 0 END
  
    ALTER TABLE dbo.play_sessions ADD CONSTRAINT
                DF_play_sessions_ps_spent_used DEFAULT 0 FOR ps_spent_used
  END
ELSE
  SELECT '***** Field play_sessions.ps_spent_used already exists *****';

GO

--
-- New table: PROVIDERS
--
IF  NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[providers]') AND type in (N'U'))
BEGIN
  CREATE TABLE [dbo].[providers](
        [pv_id] [int] IDENTITY(0,1) NOT NULL,
        [pv_name] [nvarchar](50) NOT NULL,
        [pv_hide] [bit] NOT NULL CONSTRAINT [DF_providers_pv_hide]  DEFAULT ((0)),
        [pv_points_multiplier] [money] NOT NULL CONSTRAINT [DF_providers_pv_points_multiplier]  DEFAULT ((1)),
        [pv_3gs] [bit] NOT NULL CONSTRAINT [DF_providers_pv_3gs]  DEFAULT ((0)),
        [pv_3gs_vendor_id] [nvarchar](50) NULL,
        [pv_3gs_vendor_ip] [nvarchar](50) NULL,
  CONSTRAINT [PK_providers] PRIMARY KEY CLUSTERED 
  (
        [pv_id] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
  ) ON [PRIMARY]
END
ELSE
  SELECT '***** Table providers already exists *****';

GO

--
-- New table: DOCUMENTS
--
IF  NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[documents]') AND type in (N'U'))
BEGIN
  CREATE TABLE [dbo].[documents](
        [doc_document_id] [bigint] IDENTITY(1,1) NOT NULL,
        [doc_type] [int] NOT NULL,
        [doc_created] [datetime] NOT NULL CONSTRAINT [DF_documents_doc_created]  DEFAULT (getdate()),
        [doc_modified] [datetime] NOT NULL CONSTRAINT [DF_documents_doc_modified]  DEFAULT (getdate()),
        [doc_printed] [datetime] NULL,
        [doc_data] [varbinary](max) NULL,
  CONSTRAINT [PK_documents] PRIMARY KEY CLUSTERED 
  (
        [doc_document_id] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
  ) ON [PRIMARY]
END
ELSE
  SELECT '***** Table documents already exists *****';

GO

--
-- New table: ACCOUNT_MAJOR_PRIZES
--
IF  NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[account_major_prizes]') AND type in (N'U'))
BEGIN
  CREATE TABLE [dbo].[account_major_prizes](
        [amp_operation_id] [bigint] NOT NULL,
        [amp_account_id] [bigint] NOT NULL,
        [amp_datetime] [datetime] NOT NULL CONSTRAINT [DF_account_major_prizes_amp_datetime]  DEFAULT (getdate()),

        [amp_prize] [money] NOT NULL,
        [amp_witholding_tax1] [money] NULL,
        [amp_witholding_tax2] [money] NULL,
        [amp_witholding_tax3] [money] NULL,

        [amp_player_name] [nvarchar](150) NOT NULL,
        [amp_player_id1] [nvarchar](20) NOT NULL,
        [amp_player_id2] [nvarchar](20) NOT NULL,

        [amp_player_address01] [nvarchar](50) NULL,
        [amp_player_address02] [nvarchar](50) NULL,
        [amp_player_address03] [nvarchar](50) NULL,
        [amp_player_address04] [nvarchar](50) NULL,
        [amp_player_address05] [nvarchar](50) NULL,
        [amp_player_address06] [nvarchar](50) NULL,
        [amp_player_address07] [nvarchar](50) NULL,
        [amp_player_address08] [nvarchar](50) NULL,
        [amp_player_address09] [nvarchar](50) NULL,
        [amp_player_address10] [nvarchar](50) NULL,

        [amp_business_name] [nvarchar](150)   NULL,
        [amp_business_id1] [nvarchar](20)     NULL,
        [amp_business_id2] [nvarchar](20)     NULL,

        [amp_representative_name] [nvarchar](150) NULL,
        [amp_representative_id1] [nvarchar](20)   NULL,
        [amp_representative_id2] [nvarchar](20)   NULL,

        [amp_document_id1] [bigint] NULL,
        [amp_document_id2] [bigint] NULL,
        [amp_document_id3] [bigint] NULL,
  CONSTRAINT [PK_account_major_prizes] PRIMARY KEY CLUSTERED 
  (
        [amp_operation_id] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
  ) ON [PRIMARY]

  ALTER TABLE [dbo].[account_major_prizes]  WITH CHECK ADD  CONSTRAINT [FK_account_major_prizes_account_operations] FOREIGN KEY([amp_operation_id])
  REFERENCES [dbo].[account_operations] ([ao_operation_id]);

  ALTER TABLE [dbo].[account_major_prizes] CHECK CONSTRAINT [FK_account_major_prizes_account_operations];

  ALTER TABLE [dbo].[account_major_prizes]  WITH CHECK ADD  CONSTRAINT [FK_account_major_prizes_accounts] FOREIGN KEY([amp_account_id])
  REFERENCES [dbo].[accounts] ([ac_account_id]);

  ALTER TABLE [dbo].[account_major_prizes] CHECK CONSTRAINT [FK_account_major_prizes_accounts];
END
ELSE
  SELECT '***** Table account_major_prizes already exists *****';


/****** TRIGGERS ******/
--
-- New trigger: TERMINALS.TerminalProviderTrigger
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TerminalProviderTrigger]') AND type in (N'TR'))
  DROP TRIGGER [dbo].[TerminalProviderTrigger];
GO
CREATE TRIGGER [dbo].[TerminalProviderTrigger] 
   ON  [dbo].[terminals] 
   AFTER INSERT, UPDATE
AS
BEGIN
  DECLARE @old_provider_name as NVARCHAR(50)
  DECLARE @new_provider_name as NVARCHAR(50)
  DECLARE @terminal_id as INT

  -- SET NOCOUNT ON added to prevent extra result sets from
  -- interfering with SELECT statements.
  SET NOCOUNT ON;

  IF NOT UPDATE (TE_PROVIDER_ID) RETURN

  DECLARE _terminals CURSOR FOR SELECT TE_TERMINAL_ID, TE_PROVIDER_ID FROM INSERTED

  OPEN _terminals

  FETCH NEXT FROM _terminals INTO @terminal_id, @new_provider_name

  WHILE @@Fetch_Status = 0
  BEGIN
    SET @old_provider_name = (SELECT TE_PROVIDER_ID FROM DELETED WHERE TE_TERMINAL_ID = @terminal_id)

    IF @new_provider_name <> @old_provider_name OR @old_provider_name IS NULL
    BEGIN
      IF NOT EXISTS (SELECT PV_ID FROM PROVIDERS WHERE PV_NAME = @new_provider_name) INSERT INTO PROVIDERS (PV_NAME) VALUES (@new_provider_name)
      UPDATE TERMINALS SET TE_PROV_ID = (SELECT PV_ID FROM PROVIDERS WHERE PV_NAME = @new_provider_name) WHERE TERMINALS.TE_TERMINAL_ID = @terminal_id
    END

    FETCH NEXT FROM _terminals INTO @terminal_id, @new_provider_name
  END

  CLOSE _terminals
  DEALLOCATE _terminals
END

GO

--
-- New trigger: PROVIDERS.ProviderTerminalTrigger
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ProviderTerminalTrigger]') AND type in (N'TR'))
  DROP TRIGGER [dbo].[ProviderTerminalTrigger];
GO
CREATE TRIGGER [dbo].[ProviderTerminalTrigger] 
   ON  [dbo].[providers] 
   AFTER INSERT, UPDATE
AS
BEGIN
  DECLARE @old_provider_name as NVARCHAR(50)
  DECLARE @new_provider_name as NVARCHAR(50)
  DECLARE @provider_id as INT

  -- SET NOCOUNT ON added to prevent extra result sets from
  -- interfering with SELECT statements.
  SET NOCOUNT ON;

  IF NOT UPDATE (PV_NAME) RETURN

  DECLARE _providers CURSOR FOR SELECT PV_ID, PV_NAME FROM INSERTED

  OPEN _providers

  FETCH NEXT FROM _providers INTO @provider_id, @new_provider_name

  WHILE @@Fetch_Status = 0
  BEGIN
    SET @old_provider_name = (SELECT PV_NAME FROM DELETED WHERE PV_ID = @provider_id)

    IF @old_provider_name IS NULL 
    BEGIN
      -- INSERT
      UPDATE TERMINALS SET TE_PROV_ID = @provider_id WHERE TE_PROVIDER_ID = @new_provider_name
    END
    ELSE
    BEGIN
      -- UPDATE
      UPDATE TERMINALS SET TE_PROVIDER_ID = @new_provider_name WHERE TE_PROV_ID = @provider_id 
    END

    FETCH NEXT FROM _providers INTO @provider_id, @new_provider_name
  END

  CLOSE _providers
  DEALLOCATE _providers
END

GO

/****** INDEXES ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[providers]') AND name = N'IX_pv_name')
  CREATE UNIQUE NONCLUSTERED INDEX [IX_pv_name] ON [dbo].[providers] 
  (
    [pv_name] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
ELSE
    SELECT '***** Index providers.IX_pv_name already exists *****';

GO

/****** RECORDS ******/
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Witholding.Document' AND GP_SUBJECT_KEY ='Enabled')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key]
             ,[gp_subject_key]
             ,[gp_key_value])
       VALUES
             ('Witholding.Document'
             ,'Enabled'
             ,'0');

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Witholding.Document' AND GP_SUBJECT_KEY ='Business.ID1')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key]
             ,[gp_subject_key]
             ,[gp_key_value])
       VALUES
             ('Witholding.Document'
             ,'Business.ID1'
             ,'');

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Witholding.Document' AND GP_SUBJECT_KEY ='Business.ID2')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key]
             ,[gp_subject_key]
             ,[gp_key_value])
       VALUES
             ('Witholding.Document'
             ,'Business.ID2'
             ,'');

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Witholding.Document' AND GP_SUBJECT_KEY ='Business.Name')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key]
             ,[gp_subject_key]
             ,[gp_key_value])
       VALUES
             ('Witholding.Document'
             ,'Business.Name'
             ,'');

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Witholding.Document' AND GP_SUBJECT_KEY ='Representative.Name')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key]
             ,[gp_subject_key]
             ,[gp_key_value])
       VALUES
             ('Witholding.Document'
             ,'Representative.Name'
             ,'');

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Witholding.Document' AND GP_SUBJECT_KEY ='Representative.ID1')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key]
             ,[gp_subject_key]
             ,[gp_key_value])
       VALUES
             ('Witholding.Document'
             ,'Representative.ID1'
             ,'');

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Witholding.Document' AND GP_SUBJECT_KEY ='Representative.ID2')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key]
             ,[gp_subject_key]
             ,[gp_key_value])
       VALUES
             ('Witholding.Document'
             ,'Representative.ID2'
             ,'');

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Witholding.Document' AND GP_SUBJECT_KEY ='OnPrizeGreaterThan')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key]
             ,[gp_subject_key]
             ,[gp_key_value])
       VALUES
             ('Witholding.Document'
             ,'OnPrizeGreaterThan'
             ,'10000');

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PlayerTracking' AND GP_SUBJECT_KEY ='TerminalDefaultPayout')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key]
             ,[gp_subject_key]
             ,[gp_key_value])
       VALUES
             ('PlayerTracking'
             ,'TerminalDefaultPayout'
             ,'95');

GO

/****** STORED PROCEDURES ******/
ALTER PROCEDURE [dbo].[PT_PlaySessionFinished]
  @PlaySessionId      bigint
AS
BEGIN

      -- Only for test
  DECLARE @status_code                       int
  DECLARE @status_text                       nvarchar (500)
  -- Only for test
  
  DECLARE @terminal_id                       int
  DECLARE @account_id                        bigint
  DECLARE @total_cash_in                     money
  DECLARE @played_amount                     money
  DECLARE @won_amount                        money
  DECLARE @total_cash_out                    money
  DECLARE @initial_non_redeemable            money
  DECLARE @initial_cash_in                   money
  DECLARE @prize_lock                        money
  DECLARE @account_balance                   money
  DECLARE @holder_name                       nvarchar (50)
  DECLARE @holder_level                      int
  
  DECLARE @non_redeemable_cash_in            money
  DECLARE @redeemable_cash_in                money
  DECLARE @non_redeemable_cash_out           money
  DECLARE @redeemable_cash_out               money
  DECLARE @non_redeemable_played             money
  DECLARE @redeemable_played                 money
  DECLARE @non_redeemable_won                money
  DECLARE @redeemable_won                    money
   
  DECLARE @spent_no_redeemable               money
  DECLARE @spent_redeemable                  money

  DECLARE @profit_no_redeemable              money
  DECLARE @profit_redeemable                 money
  
  DECLARE @total_sum_redimible               numeric (20,6)
  DECLARE @total_sum                         numeric (20,6)
  DECLARE @percent_redeemable                numeric (8,6)

  DECLARE @rest_played_and_no_spent          money
  DECLARE @rest_won_and_no_profit            money
  
  DECLARE @total_played_to_1_point           numeric (20,6)
  DECLARE @redeemable_played_to_1_point      numeric (20,6)
  DECLARE @redeemable_spent_to_1_point       numeric (20,6)

  DECLARE @points_balance_before             money
  DECLARE @points_balance_after              money
  DECLARE @points_total_played               money
  DECLARE @points_played_redeemable          money
  DECLARE @points_spent_redeemable           money
  DECLARE @won_points                        money
  DECLARE @points_multiplier                 money
  DECLARE @max_allowed_acc_balance           numeric (20,6)

  -- SET NOCOUNT ON added to prevent extra result sets from
  -- interfering with SELECT statements.
  SET NOCOUNT ON;
      
  SET @status_code  = 0
  SET @status_text  = 'PT_PlaySessionFinished: Init'

  SET @points_total_played      = 0
  SET @points_played_redeemable = 0
  SET @points_spent_redeemable  = 0
  SET @won_points               = 0  
  SET @points_balance_before    = 0
  SET @points_balance_after     = 0


  IF ( @PlaySessionId = 0 ) 
  BEGIN
    SET @status_text = 'PT_PlaySessionFinished: Invalid PlaySessionId'
    GOTO ERROR_PROCEDURE
  END

  --
  -- Read Session & Account & PlayerTracking Data
  --
  EXECUTE dbo.PT_ReadData @PlaySessionId, @terminal_id OUTPUT, @account_id OUTPUT,
                          @total_cash_in OUTPUT, @played_amount OUTPUT, @won_amount OUTPUT, @total_cash_out OUTPUT, 
                          @initial_non_redeemable OUTPUT, @initial_cash_in OUTPUT, @prize_lock OUTPUT, @account_balance OUTPUT, 
                          @points_balance_before OUTPUT, @holder_name OUTPUT, @holder_level OUTPUT, @max_allowed_acc_balance OUTPUT,
                          @status_code OUTPUT, @status_text OUTPUT

  IF ( @status_code <> 0 )
    GOTO ERROR_PROCEDURE
    
  --
  -- Compute PlayedCredits, PlayedRedeemableCredits, SpentRedeemableCredits
  --
  EXECUTE dbo.PT_BalancePartsWhenPlaying @total_cash_in, @initial_cash_in, @initial_non_redeemable, @prize_lock, 
                                         @non_redeemable_cash_in OUTPUT, @redeemable_cash_in OUTPUT

  EXECUTE dbo.PT_BalancePartsWhenPlaying @total_cash_out, @initial_cash_in, @initial_non_redeemable, @prize_lock, 
                                         @non_redeemable_cash_out OUTPUT, @redeemable_cash_out OUTPUT

  SET @spent_no_redeemable      = dbo.Maximum_Money (0, @non_redeemable_cash_in - @non_redeemable_cash_out)
  SET @spent_redeemable         = dbo.Maximum_Money (0, @redeemable_cash_in - @redeemable_cash_out)

  SET @profit_no_redeemable     = dbo.Maximum_Money (0, @non_redeemable_cash_out - @non_redeemable_cash_in)
  SET @profit_redeemable        = dbo.Maximum_Money (0, @redeemable_cash_out - @redeemable_cash_in)

  SET @total_sum_redimible      = @redeemable_cash_in + @redeemable_cash_out 
  SET @total_sum                = @non_redeemable_cash_in + @non_redeemable_cash_out + @redeemable_cash_in + @redeemable_cash_out
  
  IF @total_sum = 0 GOTO ERROR_PROCEDURE   
  SET @percent_redeemable       = @total_sum_redimible / @total_sum

  SET @rest_played_and_no_spent = @played_amount - (@spent_no_redeemable + @spent_redeemable)
  SET @redeemable_played        = ROUND(@spent_redeemable + (@rest_played_and_no_spent * @percent_redeemable), 4)
  SET @non_redeemable_played    = @played_amount - @redeemable_played

  SET @rest_won_and_no_profit   = @won_amount - (@profit_no_redeemable + @profit_redeemable)
  SET @redeemable_won           = ROUND(@profit_redeemable + (@rest_won_and_no_profit * @percent_redeemable), 4)
  SET @non_redeemable_won       = @won_amount - @redeemable_won

  --
  -- Update PlaySession table (Redeemable & NonRedeemable amounts)
  --
  UPDATE PLAY_SESSIONS
     SET PS_NON_REDEEMABLE_CASH_IN    = @non_redeemable_cash_in
       , PS_NON_REDEEMABLE_CASH_OUT   = @non_redeemable_cash_out
       , PS_NON_REDEEMABLE_PLAYED     = @non_redeemable_played
       , PS_NON_REDEEMABLE_WON        = @non_redeemable_won
       , PS_REDEEMABLE_CASH_IN        = @redeemable_cash_in
       , PS_REDEEMABLE_CASH_OUT       = @redeemable_cash_out
       , PS_REDEEMABLE_PLAYED         = @redeemable_played
       , PS_REDEEMABLE_WON            = @redeemable_won
   WHERE PS_PLAY_SESSION_ID = @PlaySessionId 

  --
  -- RCI & ACC & AJQ 30/09/2010: Reset the Cancellable Operation when PlayedAmount greater than 0.
  --
  UPDATE   ACCOUNTS
     SET   AC_CANCELLABLE_OPERATION_ID = CASE WHEN (@played_amount > 0) THEN NULL ELSE AC_CANCELLABLE_OPERATION_ID END
   WHERE   AC_ACCOUNT_ID               = @account_id  

  --
  -- ACC 22/10/2010: Check maximum allowed account balance
  --
  IF ( @max_allowed_acc_balance > 0 AND @total_cash_out > @max_allowed_acc_balance )
  BEGIN
    UPDATE ACCOUNTS
       SET AC_BLOCKED       = 1
         , AC_BLOCK_REASON  = 2  -- MaxBalance
     WHERE AC_ACCOUNT_ID    = @account_id  
  END

  --- Anonymous accounts have Level=0
  IF ( @holder_level = 0 )
    GOTO EXIT_PROCEDURE

  --
  -- Read PlayerTracking Point Conversion Factors according to the holder's level
  --
  EXECUTE dbo.PT_ReadPointFactors @holder_level, 
                                  @redeemable_spent_to_1_point  OUTPUT,
                                  @redeemable_played_to_1_point OUTPUT,
                                  @total_played_to_1_point      OUTPUT,
                                  @status_code                  OUTPUT, 
                                  @status_text                  OUTPUT

  IF ( @status_code <> 0 )
    GOTO ERROR_PROCEDURE

  --
  -- Compute Points (it is possible to accummulate points per each type of credits at the same time)
  --  
  IF @redeemable_spent_to_1_point  > 0 SET @points_spent_redeemable  = @spent_redeemable  / @redeemable_spent_to_1_point
  IF @redeemable_played_to_1_point > 0 SET @points_played_redeemable = @redeemable_played / @redeemable_played_to_1_point
  IF @total_played_to_1_point      > 0 SET @points_total_played      = @played_amount     / @total_played_to_1_point
  
  SET @won_points = ROUND(@points_total_played + @points_played_redeemable + @points_spent_redeemable, 4)

  -- 
  -- AJQ 20-JAN-2012, Provider's points multiplier
  -- Get the "Provider" multiplier
  --
  SET @points_multiplier = ISNULL ( ( SELECT PV_POINTS_MULTIPLIER FROM PROVIDERS     WHERE PV_ID = (
                                      SELECT TE_PROV_ID           FROM TERMINALS     WHERE TE_TERMINAL_ID = ( 
                                      SELECT PS_TERMINAL_ID       FROM PLAY_SESSIONS WHERE PS_PLAY_SESSION_ID = @PlaySessionId ) ) ), 0 )
  IF ( @points_multiplier <= 0 )
    GOTO EXIT_PROCEDURE

  -- Apply multiplier
  SET @won_points = ROUND (@won_points * @points_multiplier, 4)

  --
  -- Accumulate points
  -- 
  UPDATE   ACCOUNTS
     SET   AC_POINTS                   = AC_POINTS  + @won_points
   WHERE   AC_ACCOUNT_ID               = @account_id  
  
  --  
  -- Create CardMovement.ObtainedPoints
  --
  -- PointsAwarded   = 36,
  SET @points_balance_after     = @points_balance_before + @won_points
  
  EXECUTE dbo.InsertMovement @PlaySessionId, @account_id, @terminal_id, 36, @points_balance_before, 0, @won_points, @points_balance_after

EXIT_PROCEDURE:

  --
  -- ACC 25/11/2010 Accumulate played amount into site jackpot
  --
  EXECUTE dbo.SiteJackpot_AccumulatePlayed @played_amount, @redeemable_played

  SET @status_code  = 0
  SET @status_text  = 'PT_PlaySessionFinished: Successful points accumulated.' 
                    + ' *** '
                    + ' Points.PerRedeemableSpent  = ' + CAST (@points_spent_redeemable AS nvarchar)  + ' (' + CAST (@spent_redeemable AS nvarchar)  + ' credits)'
                    + ' Points.PerRedeemablePlayed = ' + CAST (@points_played_redeemable AS nvarchar) + ' (' + CAST (@redeemable_played AS nvarchar) + ' credits)'
                    + ' Points.PerTotalPlayed      = ' + CAST (@points_total_played AS nvarchar)      + ' (' + CAST (@played_amount AS nvarchar)     + ' credits)'
                    + ' Points.TotalAwarded        = ' + CAST (@won_points AS nvarchar) 
                    + ' *** '

ERROR_PROCEDURE:
--
-- ACC 18-OCT-2010 DO NOT SELECT (3GS not works with this select)
--
--  SELECT @status_text AS StatusText

END -- PT_PlaySessionFinished

GO

/****** SPECIAL CODE TO FILL NEW TABLE PROVIDERS ******/
TRUNCATE TABLE PROVIDERS;
GO
INSERT INTO   PROVIDERS (PV_NAME) VALUES ('UNKNOWN');
GO
INSERT INTO   PROVIDERS (PV_NAME) 
     SELECT   DISTINCT TE_PROVIDER_ID FROM TERMINALS 
      WHERE   TE_PROVIDER_ID IS NOT NULL 
        AND   TE_PROVIDER_ID <> 'UNKNOWN'
   ORDER BY   1 ASC

GO

/**************************************** 3GS Section ****************************************/
