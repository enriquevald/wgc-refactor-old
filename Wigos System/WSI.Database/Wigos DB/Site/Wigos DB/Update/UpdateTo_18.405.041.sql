/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 404;

SET @New_ReleaseId = 405;
SET @New_ScriptName = N'UpdateTo_18.405.041.sql';
SET @New_Description = N'New release v03.006'; 

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO



/**** GENERAL PARAM *****/

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY = 'Tax.OnPrize.3.Name' )
BEGIN
      INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Cashier', 'Tax.OnPrize.3.Name', '' )
END

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY = 'Tax.OnPrize.3.Pct' )
BEGIN
      INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Cashier', 'Tax.OnPrize.3.Pct', 0 )
END

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY = 'Tax.OnPrize.3.Threshold' )
BEGIN
      INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Cashier', 'Tax.OnPrize.3.Threshold', 0 )
END

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY = 'Promotions.RE.Tax.3.Pct' )
BEGIN
      INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Cashier', 'Promotions.RE.Tax.3.Pct', 0 )
END

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY = 'Promotions.RE.Tax.3.Threshold' )
BEGIN
      INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Cashier', 'Promotions.RE.Tax.3.Threshold', 0 )
END

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY = 'Tax.OnPrizeInKind.3.Name' )
BEGIN
      INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Cashier', 'Tax.OnPrizeInKind.3.Name', '' )
END

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY = 'Tax.OnPrizeInKind.3.Pct' )
BEGIN
      INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Cashier', 'Tax.OnPrizeInKind.3.Pct', 0 )
END
      
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[CASHIER_VOUCHERS]') and name = 'cv_m01_tax3')
BEGIN
  ALTER TABLE dbo.CASHIER_VOUCHERS ADD cv_m01_tax3 MONEY NULL
END
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[ACCOUNT_PROMOTIONS]') and name = 'acp_prize_tax3')
BEGIN
  ALTER TABLE dbo.ACCOUNT_PROMOTIONS ADD acp_prize_tax3 MONEY NULL
END
GO

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'GamingTables' AND GP_SUBJECT_KEY = 'Tax.OnPrize.3.Pct' )
BEGIN
    INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('GamingTables', 'Tax.OnPrize.3.Pct', 0 )
END
GO

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY = 'Promotion.NR3AsUNR' )
BEGIN
    INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Cashier', 'Promotion.NR3AsUNR', 0 )
END
GO

IF (NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'SiteJackpot' AND GP_SUBJECT_KEY = 'Buffering.Enabled'))
BEGIN 
    INSERT INTO general_params (gp_group_key, gp_subject_key, gp_key_value)
    VALUES ('SiteJackpot','Buffering.Enabled', 1)
END 
GO

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'GamingTables.PlayerTracking' AND GP_SUBJECT_KEY = 'BuyIn.Mode')
BEGIN
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('GamingTables.PlayerTracking', 'BuyIn.Mode', '0')
END
GO

IF (NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'SiteJackpot' AND GP_SUBJECT_KEY = 'Bonusing.ShowOnNotificationEGM'))
BEGIN 
    INSERT INTO general_params (gp_group_key, gp_subject_key, gp_key_value)
    VALUES ('SiteJackpot','Bonusing.ShowOnNotificationEGM', 0)
END 
GO

IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CreditLine' AND GP_SUBJECT_KEY = 'Enabled')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('CreditLine', 'Enabled', '0')
GO  

IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CreditLine' AND GP_SUBJECT_KEY = 'NumSigners')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('CreditLine', 'NumSigners', '2')
GO  

IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Junkets' AND GP_SUBJECT_KEY = 'Enabled')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Junkets', 'Enabled', '0')
GO  

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'GamingTables' AND GP_SUBJECT_KEY = 'Conciliation.SuggestTickets')
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('GamingTables','Conciliation.SuggestTickets','1')
GO

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'GamingTable.PlayerTracking' AND GP_SUBJECT_KEY = 'BuyIn.Mode')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) 
	SELECT 'GamingTable.PlayerTracking', 'BuyIn.Mode', GP_KEY_VALUE FROM GENERAL_PARAMS 
	WHERE GP_GROUP_KEY = 'GamingTables.PlayerTracking' AND GP_SUBJECT_KEY = 'BuyIn.Mode'
GO

IF EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'GamingTables.PlayerTracking' AND GP_SUBJECT_KEY = 'BuyIn.Mode')
  DELETE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'GamingTables.PlayerTracking' AND GP_SUBJECT_KEY = 'BuyIn.Mode'
GO



/******* TABLES  *******/
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[terminals]') and name = 'te_creation_date')
BEGIN
  ALTER TABLE dbo.terminals ADD te_creation_date DATETIME NULL
END
GO

--Replacement date for terminals
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[terminals]') and name = 'te_replacement_date')
BEGIN
  ALTER TABLE dbo.terminals ADD te_replacement_date DATETIME NULL
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.credit_lines') AND type in (N'U'))
BEGIN
CREATE TABLE dbo.credit_lines(
	cl_id bigint IDENTITY(1,1) NOT NULL,
	cl_account_id bigint NOT NULL,
	cl_limit_amount money NOT NULL,
	cl_tto_amount money NULL,
	cl_spent_amount money NOT NULL,
	cl_iso_code nvarchar(3) NOT NULL,
	cl_status int NOT NULL,
	cl_iban nvarchar(34) NULL,
	cl_xml_signers xml NULL,
	cl_creation datetime NOT NULL,
	cl_update datetime NOT NULL,
	cl_expired datetime NULL,
	cl_last_payback datetime NULL,
CONSTRAINT [PK_cl_id] PRIMARY KEY CLUSTERED 
(
  cl_id ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

-- CREATE CREDIT_LINE_MOVEMENTS
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.credit_line_movements') AND type in (N'U'))
BEGIN
CREATE TABLE dbo.credit_line_movements(
	clm_id bigint IDENTITY(1,1) NOT NULL,
	clm_credit_line_id bigint NOT NULL,
	clm_operation_id bigint NOT NULL,
	clm_type int NOT NULL,
	clm_old_value xml NULL,
	clm_new_value xml NOT NULL,	
	clm_creation datetime NOT NULL,
	clm_creation_user nvarchar(50) NOT NULL,
CONSTRAINT [PK_clm_id] PRIMARY KEY CLUSTERED 
(
  clm_id ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

-- MODIFY CAGE_CURRENCIES TABLE
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[cage_currencies]') and name = 'cgc_cage_visible')
  ALTER TABLE [dbo].[cage_currencies] ADD [cgc_cage_visible] bit NULL DEFAULT(1)
GO

-- UPDATE CAGE_CURRENCIES TABLE
UPDATE   CAGE_CURRENCIES 
   SET   CGC_CAGE_VISIBLE = 1
 WHERE   CGC_CAGE_VISIBLE IS NULL

GO

IF NOT EXISTS( SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'gaming_tables_sessions' AND COLUMN_NAME = 'gts_copy_dealer_validated_amount')
BEGIN
  ALTER TABLE dbo.gaming_tables_sessions ADD
    gts_copy_dealer_validated_amount money NOT NULL CONSTRAINT DF_gaming_tables_sessions_gts_copy_dealer_validated_amount DEFAULT 0
END
GO 

-- ADD COLUMN gaming_tables_sessions IN gtsc_copy_dealer_validated_amount
IF NOT EXISTS( SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'gaming_tables_sessions_by_currency' AND COLUMN_NAME = 'gtsc_copy_dealer_validated_amount')
BEGIN
  ALTER TABLE dbo.gaming_tables_sessions_by_currency ADD
    gtsc_copy_dealer_validated_amount money NOT NULL CONSTRAINT DF_gaming_tables_sessions_by_currency_gtsc_copy_dealer_validated_amount DEFAULT 0
END
GO 

-- ADD TABLE gt_copy_dealer_validated
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[gt_copy_dealer_validated]') AND type in (N'U'))
BEGIN
  CREATE TABLE dbo.gt_copy_dealer_validated
  (
    gtcd_gaming_table_session_id bigint NOT NULL,
    gtcd_ticket_id bigint NOT NULL,
    gtcd_validation_origin int NOT NULL,
    gtcd_validation_datetime datetime NOT NULL
    
    CONSTRAINT PK_gt_copy_dealer_validated PRIMARY KEY CLUSTERED 
    (
      gtcd_gaming_table_session_id ASC,
      gtcd_ticket_id ASC
    ) WITH( PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
  ) ON [PRIMARY]
END
GO

-- ADD INDEX IX_unique_ticket_id TO AVOID DUPLICATE TICKETS 
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[gt_copy_dealer_validated]') AND name = N'IX_gtcdv_unique_ticket_id')
BEGIN
  CREATE UNIQUE NONCLUSTERED INDEX IX_gtcdv_unique_ticket_id ON dbo.gt_copy_dealer_validated 
  (
	  gtcd_ticket_id ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
END
GO


IF not EXISTS (SELECT 1 FROM SYS.COLUMNS WHERE NAME = N'cl_timestamp' AND Object_ID = Object_ID(N'credit_lines'))
BEGIN
   ALTER TABLE [dbo].[credit_lines]
   ADD cl_timestamp timestamp 
END
GO

IF not EXISTS (SELECT 1 FROM SYS.COLUMNS WHERE NAME = N'clm_timestamp' AND Object_ID = Object_ID(N'credit_line_movements'))
BEGIN
   ALTER TABLE [dbo].[credit_line_movements]
   ADD clm_timestamp timestamp 
END
GO

IF not EXISTS (SELECT 1 FROM SYS.INDEXES WHERE NAME = 'IDX_cl_account_id')
CREATE NONCLUSTERED INDEX IDX_cl_account_id ON [dbo].[credit_lines] 
(
	[cl_account_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

IF not EXISTS (SELECT 1 FROM SYS.INDEXES WHERE NAME = 'IDX_clm_credit_line_id')
CREATE NONCLUSTERED INDEX [IDX_clm_credit_line_id] ON [dbo].[credit_line_movements] 
(
	[clm_credit_line_id] ASC,
	[clm_type] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO


IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.junkets_representatives') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[junkets_representatives](
	[jr_id] [bigint] IDENTITY(1,1) NOT NULL,
	[jr_code] [nvarchar](10) NOT NULL,
	[jr_name] [nvarchar](50) NOT NULL,
	[jr_internal] [bit] NULL,
	[jr_status] [bit] NOT NULL,
	[jr_deposit_amount] [money] NOT NULL,
	[jr_comment] [nvarchar](512) NULL,	
	[jr_creation] [datetime] NOT NULL,
	[jr_update] [datetime] NOT NULL,
 CONSTRAINT [PK_jr_id] PRIMARY KEY CLUSTERED 
(
	[jr_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

-- TABLE JUNKETS
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.junkets') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[junkets](
	ju_id bigint IDENTITY(1,1) NOT NULL,
	ju_representative_id bigint NOT NULL,
	ju_code nvarchar(10) NOT NULL,
	ju_name nvarchar(50) NOT NULL,
	ju_comment nvarchar(512) NULL,	
	ju_date_from datetime NOT NULL,
	ju_date_to datetime NOT NULL,	
	ju_creation datetime NOT NULL,
	ju_update datetime NOT NULL,
 CONSTRAINT [PK_ju_id] PRIMARY KEY CLUSTERED 
(
	[ju_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

-- TABLE JUNKETS_COMMISSIONS
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.junkets_commissions') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[junkets_commissions](
	jc_id bigint IDENTITY(1,1) NOT NULL,
	jc_junket_id bigint NOT NULL,
	jc_type int NOT NULL,
	jc_fixed_value money NOT NULL,
	jc_variable_value decimal(5, 2) NOT NULL,
	jc_date_from datetime NOT NULL,
	jc_date_to datetime NOT NULL,		
	jc_creation datetime NOT NULL,
	jc_update datetime NOT NULL,
 CONSTRAINT [PK_jc_id] PRIMARY KEY CLUSTERED 
(
	[jc_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

-- TABLE JUNKETS FLYERS
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.junkets_flyers') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[junkets_flyers](
	[jf_id] [bigint] IDENTITY(1,1) NOT NULL,
	[jf_junket_id] [bigint] NOT NULL,
	[jf_promotion_id] [bigint] NULL,
	[jf_code] [nvarchar](6) NOT NULL,
	[jf_num_created] [int] NULL,
	[jf_allow_reuse] [bit] NOT NULL,
	[jf_comment] [nvarchar](512) NULL,
	[jf_show_pop_up] [bit] NOT NULL,
	[jf_text_pop_up] [nvarchar](256) NULL,	
	[jf_print_voucher] [bit] NOT NULL,
	[jf_title_voucher] [nvarchar](50) NULL,	
	[jf_text_voucher] [nvarchar](50) NULL,	
        [jf_print_text_promotion] [bit] NOT NULL,
	[jf_text_promotion] [nvarchar](50) NULL,
	[jf_creation] [datetime] NOT NULL,
	[jf_update] [datetime] NOT NULL,	
 CONSTRAINT [PK_jf_id] PRIMARY KEY CLUSTERED 
(
	[jf_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

-- TABLE JUNKETS FLYERS FLAGS
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.junkets_flyers_flags') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[junkets_flyers_flags](
	[jff_flyer_id] [bigint] NOT NULL,
	[jff_flag_id] [bigint] NOT NULL,
	[jff_flag_count] [int] NOT NULL,
 CONSTRAINT [PK_junkets_flyers_flags] PRIMARY KEY CLUSTERED 
(
	[jff_flyer_id] ASC,
	[jff_flag_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[account_flags]') and name = 'af_added_by_operation_id')
ALTER TABLE [dbo].[account_flags] 
	ADD af_added_by_operation_id [bigint] NULL;
GO

-- TABLE JUNKETS COMMISSIONS MOVEMENTS
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.junkets_commissions_movements') AND type in (N'U'))
BEGIN
CREATE TABLE dbo.junkets_commissions_movements
	(
	jcm_id bigint IDENTITY(1,1) NOT NULL,
	jcm_account_id bigint NOT NULL,
	jcm_flyer_id bigint NOT NULL,
  jcm_operation_id bigint NOT NULL,
	jcm_type int NOT NULL,
	jcm_status int NOT NULL,
	jcm_amount money NOT NULL,
	jcm_creation datetime NOT NULL,
	jcm_paid datetime NULL,
 CONSTRAINT [PK_jcm_id] PRIMARY KEY CLUSTERED 
(
	[jcm_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

-- TABLE JUNKETS COMMISSIONS PENDING
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.junkets_commissions_pending') AND type in (N'U'))
BEGIN
CREATE TABLE dbo.junkets_commissions_pending
	(
	jcp_id bigint IDENTITY(1,1) NOT NULL,
	jcp_account_id bigint NOT NULL,
	jcp_datetime datetime NOT NULL,
 CONSTRAINT [PK_jcp_id] PRIMARY KEY CLUSTERED 
(
	[jcp_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

-- New columns in tabla flags
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[flags]') and name = 'fl_related_id')  
BEGIN
ALTER TABLE flags ADD fl_related_id bigint
END
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[flags]') and name = 'fl_related_type')  
BEGIN
ALTER TABLE flags ADD fl_related_type int
END
GO



/******* RECORDS *******/
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[gt_copy_dealer_validated]') and name = 'gtcd_validation_source')  
BEGIN 
  EXECUTE sp_rename N'dbo.gt_copy_dealer_validated.gtcd_validation_origin', N'gtcd_validation_source', 'COLUMN' 
END
GO

-- Update creation date
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[terminals]') 
               and name = 'te_creation_date' AND is_nullable = 0)
BEGIN
  UPDATE dbo.terminals 
  SET te_creation_date = t2.te_activation_date
  FROM terminals t inner join
       (
         select MIN(te_activation_date) as te_activation_date
              , te_master_id as te_master_id
           from terminals
          group by (te_master_id)
       ) t2 
  ON t.te_master_id = t2.te_master_id
END
GO
-- Set creation date to NOT NULL (Always after the previous Update)
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[terminals]') 
               and name = 'te_creation_date' AND is_nullable = 0)
BEGIN
  ALTER TABLE dbo.terminals ALTER COLUMN te_creation_date DATETIME NOT NULL
END
GO

DELETE FROM MS_SITE_TASKS 
WHERE ST_TASK_ID IN (76,77)
GO

/****** INSERT TASK (76 - CREDITLINES) IN SITE ******/
IF NOT EXISTS(SELECT ST_TASK_ID FROM MS_SITE_TASKS WHERE ST_TASK_ID = 76)
BEGIN
 INSERT   INTO MS_SITE_TASKS 
			 ( ST_TASK_ID
			, ST_ENABLED
			, ST_INTERVAL_SECONDS
			, ST_MAX_ROWS_TO_UPLOAD
			) 
	  SELECT  76
			, 1
			, 180
			, 200
END
GO

/****** INSERT TASK (77 - CREDITLINES) IN MULTISITE ******/
IF NOT EXISTS(SELECT ST_TASK_ID FROM MS_SITE_TASKS WHERE ST_TASK_ID = 77)
BEGIN
 INSERT   INTO MS_SITE_TASKS 
			 ( ST_TASK_ID
			, ST_ENABLED
			, ST_INTERVAL_SECONDS
			, ST_MAX_ROWS_TO_UPLOAD
			) 
	  SELECT  77
			, 1
			, 180
			, 200
END
GO

IF NOT EXISTS (SELECT [alcg_alarm_code] from [dbo].[alarm_catalog] WHERE [alcg_alarm_code] = 262164 AND [alcg_language_id] = 10)
BEGIN
  INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) 
    VALUES ( 262164, 10, 0, N'Conciliado ticket no esperado', N'Conciliado ticket no esperado', 1)
END
GO

IF NOT EXISTS (SELECT [alcg_alarm_code] from [dbo].[alarm_catalog] WHERE [alcg_alarm_code] = 262164 AND [alcg_language_id] = 9)
BEGIN
  INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) 
    VALUES ( 262164,  9, 0, N'Unexpected ticket conciliated', N'Unexpected ticket conciliated', 1)
END 
GO

IF NOT EXISTS (SELECT [alcc_alarm_code] from [dbo].[alarm_catalog_per_category] WHERE [alcc_alarm_code] = 262164 AND [alcc_category] = 40) 
BEGIN
  INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) 
    VALUES ( 262164, 40, 0, GETDATE() )
END
GO

IF NOT EXISTS (SELECT [alcg_alarm_code] from [dbo].[alarm_catalog] WHERE [alcg_alarm_code] = 262165 AND [alcg_language_id] = 10)
BEGIN
  INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) 
    VALUES ( 262165, 10, 0, N'Ticket conciliado no valido', N'Ticket conciliado no valido', 1)
END
GO

IF NOT EXISTS (SELECT [alcg_alarm_code] from [dbo].[alarm_catalog] WHERE [alcg_alarm_code] = 262165 AND [alcg_language_id] = 9)
BEGIN
  INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) 
    VALUES ( 262165,  9, 0, N'Invalid ticket conciliated', N'Invalid ticket conciliated', 1)
END 
GO

IF NOT EXISTS (SELECT [alcc_alarm_code] from [dbo].[alarm_catalog_per_category] WHERE [alcc_alarm_code] = 262165 AND [alcc_category] = 40) 
BEGIN
  INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) 
    VALUES ( 262165, 40, 0, GETDATE() )
END
GO


/******* PROCEDURES *******/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GT_Cashier_Movements_And_Summary_By_Session]') AND type in (N'P', N'PC'))
   DROP PROCEDURE [dbo].[GT_Cashier_Movements_And_Summary_By_Session]
GO

CREATE PROCEDURE [dbo].[GT_Cashier_Movements_And_Summary_By_Session]
(
  @pGtSessionId                  BigInt,

  @pCageChipColor                VARCHAR(3),-- X02
  @pOpeningCash                  INT,  
  @pCageCloseSession             INT,
  @pCageFillerIn                 INT,
  @pCageFillerOut                INT,
  @pChipsSale                    INT,
  @pChipsPurchase                INT,
  @pChipsSaleDevolutionForTito   INT,
  @pChipsSaleWithCashIn          INT,
  @pChipsSaleRegisterTotal       INT,
  @pFillerInClosingStock         INT, 
  @pFillerOutClosingStock        INT,  
  @pReopenCashier                INT,
  
  @pOwnSalesAmount       Money,
  @pExternalSalesAmount  Money,
  @pCollectedAmount      Money,
  @pIsIntegratedCashier  Bit,

  @pDrop                 Money        OUTPUT
)
AS
BEGIN
  DECLARE @Columns           AS VARCHAR(MAX)
  DECLARE @NationalCurrency  AS VARCHAR(5)
           
  SET @NationalCurrency = (SELECT GP_KEY_VALUE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'RegionalOptions' AND GP_SUBJECT_KEY = 'CurrencyISOCode')
          
      SELECT   CS_SESSION_ID
             , MAX(CM_DATE)                    AS DATE_MOV
             , CM_TYPE                         AS TYPE_MOV
             , ISNULL(CCMR.CGM_MOVEMENT_ID, 0) AS ID_MOV_22
             , REPLICATE('0', 50 - LEN(CAST(ISNULL(CCMR.CGM_MOVEMENT_ID, 0) AS NVARCHAR(50)))) + CAST(ISNULL(CCMR.CGM_MOVEMENT_ID, 0) AS NVARCHAR(50)) 
               + REPLICATE('0', 50 - LEN(CAST(ISNULL(CM.CM_OPERATION_ID, 0) AS NVARCHAR(50)))) + CAST(ISNULL(CM.CM_OPERATION_ID, 0) AS NVARCHAR(50)) AS ID_MOV
             , SUM(CM_SUB_AMOUNT)              AS SUB_AMOUNT
             , SUM(CM_ADD_AMOUNT)              AS ADD_AMOUNT
             , SUM(CM_ADD_AMOUNT) - SUM(CM_SUB_AMOUNT) AS CALCULATED_AMOUNT
             ,  CASE ISNULL(CM_CURRENCY_ISO_CODE,'') WHEN '' THEN @NationalCurrency
                WHEN @pCageChipColor THEN ''
                ELSE CM.CM_CURRENCY_ISO_CODE END AS CURRENCY_ISO_CODE
             ,  CM.CM_CAGE_CURRENCY_TYPE AS CURRENCY_TYPE
             ,  0 OPENER_MOV
             , CM.CM_OPERATION_ID
             , CM_UNDO_STATUS
        INTO   #TABLE_MOVEMENTS
        FROM   GAMING_TABLES_SESSIONS
  INNER JOIN   CASHIER_SESSIONS  ON GTS_CASHIER_SESSION_ID = CS_SESSION_ID
  INNER JOIN   CASHIER_MOVEMENTS AS CM WITH(INDEX(IX_CM_SESSION_ID)) ON CM_SESSION_ID = CS_SESSION_ID
   LEFT JOIN   CAGE_CASHIER_MOVEMENT_RELATION AS CCMR ON CCMR.CM_MOVEMENT_ID = CM.CM_MOVEMENT_ID
  INNER JOIN   GUI_USERS         ON CS_USER_ID    = GU_USER_ID
       WHERE   GTS_GAMING_TABLE_SESSION_ID = @pGtSessionId
         AND   CM_TYPE IN (@pOpeningCash,
                           @pFillerInClosingStock,
                           @pCageFillerIn,
                           @pCageFillerOut,
                           @pCageCloseSession,
                           @pFillerOutClosingStock,
                           @pChipsSale,
                           @pChipsPurchase,
                           @pChipsSaleDevolutionForTito,
                           @pChipsSaleWithCashIn,
                           @pChipsSaleRegisterTotal, 
                           @pReopenCashier)
    GROUP BY   CS_SESSION_ID
             , CM_TYPE
             , CCMR.CGM_MOVEMENT_ID
             , REPLICATE('0', 50 - LEN(cast(ISNULL(CCMR.CGM_MOVEMENT_ID, 0) as nvarchar(50)))) + cast(ISNULL(CCMR.CGM_MOVEMENT_ID, 0) as nvarchar(50)) 
               + REPLICATE('0', 50 - LEN(cast(ISNULL(CM.CM_OPERATION_ID, 0) as nvarchar(50)))) + cast(ISNULL(CM.CM_OPERATION_ID, 0) as nvarchar(50))
             , CM.CM_CURRENCY_ISO_CODE
             , CM.CM_CAGE_CURRENCY_TYPE
             , CM.CM_OPERATION_ID
             , CM_UNDO_STATUS
    ORDER BY   MAX(CM_DATE) DESC
          
  DECLARE @cursor_id_mov AS nvarchar(255)

  DECLARE _cursor CURSOR FOR SELECT MIN(ID_MOV) 
                               FROM #TABLE_MOVEMENTS 
                              WHERE TYPE_MOV IN (@pCageFillerIn , @pFillerInClosingStock) 
                              GROUP BY CS_SESSION_ID
  
  OPEN _cursor  
  FETCH NEXT FROM _cursor INTO @cursor_id_mov
  
  WHILE @@FETCH_STATUS = 0  
  BEGIN  
    UPDATE #TABLE_MOVEMENTS
       SET OPENER_MOV = 1
     WHERE ID_MOV = @cursor_id_mov

    FETCH NEXT FROM _cursor INTO @cursor_id_mov 
  END   
  CLOSE _cursor;  
  DEALLOCATE _cursor;  

  SET @NationalCurrency = (SELECT GP_KEY_VALUE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'RegionalOptions' AND GP_SUBJECT_KEY = 'CurrencyISOCode')

  SELECT @Columns = COALESCE(@Columns + ',', '') +  '[' + CURRENCY_TYPE + ']'                             
    FROM   (SELECT   DISTINCT ISNULL(CE_CURRENCY_ORDER, -1) CE_CURRENCY_ORDER
                   , CASE WHEN CURRENCY_TYPE > 1000 
                          THEN 0
                          ELSE 1 END AS ORDER_2
                   , CASE WHEN CURRENCY_ISO_CODE = '' THEN 'X02' ELSE CURRENCY_ISO_CODE END 
                   + ' ' + CONVERT(NVARCHAR(4), CURRENCY_TYPE) AS CURRENCY_TYPE      
              FROM   #TABLE_MOVEMENTS 
              LEFT   JOIN CURRENCY_EXCHANGE ON CURRENCY_ISO_CODE = CE_CURRENCY_ISO_CODE AND CE_TYPE = 0
             WHERE   CURRENCY_ISO_CODE IS NOT NULL
           ) AS COLS
  ORDER   BY CE_CURRENCY_ORDER ASC
        , ORDER_2 ASC
        , CURRENCY_TYPE ASC

  SET @Columns = ISNULL(@Columns,COALESCE('[' + @NationalCurrency + ']',''))                   

  SELECT * FROM   #TABLE_MOVEMENTS   

   EXEC ('-- INITIAL AMOUNT
        SELECT   ''INITIAL_AMOUNT''  AS NAME_MOV
               , *
          FROM ( SELECT   ADD_AMOUNT AS AMOUNT
                        , CASE WHEN CURRENCY_ISO_CODE = '''' THEN ''X02'' ELSE CURRENCY_ISO_CODE END
                          + '' '' + CONVERT(NVARCHAR(4), CURRENCY_TYPE) AS CURRENCY_ISO_CODE
                   FROM   #TABLE_MOVEMENTS
                  WHERE   OPENER_MOV = 1
               ) AS T1
        PIVOT (
                SUM (AMOUNT) FOR CURRENCY_ISO_CODE IN (' + @Columns+ ')
              ) AS PVT
        UNION   ALL
         -- TOTAL FILL IN
        SELECT   ''TOTAL_FILL_IN''  AS NAME_MOV
               , *
          FROM ( SELECT   ADD_AMOUNT AS AMOUNT
                        , CASE WHEN CURRENCY_ISO_CODE = '''' THEN ''X02'' ELSE CURRENCY_ISO_CODE END 
                          + '' '' + CONVERT(NVARCHAR(4), CURRENCY_TYPE) AS CURRENCY_ISO_CODE
                   FROM   #TABLE_MOVEMENTS
                  WHERE   OPENER_MOV <> 1
               ) AS T1
         PIVOT (
                 SUM (AMOUNT) FOR CURRENCY_ISO_CODE IN (' + @Columns+ ')
               ) AS PVT
         UNION   ALL
          -- TOTAL WITHDRAW
        SELECT   ''TOTAL_WITHDRAW'' AS NAME_MOV
               , *
          FROM (  SELECT   SUB_AMOUNT AS AMOUNT
                         , CASE WHEN CURRENCY_ISO_CODE = '''' THEN ''X02'' ELSE CURRENCY_ISO_CODE END
                           + '' '' + CONVERT(NVARCHAR(4), CURRENCY_TYPE) AS CURRENCY_ISO_CODE
                    FROM   #TABLE_MOVEMENTS
               ) AS T1
         PIVOT (
                 SUM (AMOUNT) FOR CURRENCY_ISO_CODE IN (' + @Columns+ ')
               ) AS PVT
   ')
  
   DROP TABLE #TABLE_MOVEMENTS      

  SELECT @pDrop = dbo.GT_Calculate_DROP(@pOwnSalesAmount, @pExternalSalesAmount, @pCollectedAmount, @pIsIntegratedCashier) 
 
END
GO
  
GRANT EXECUTE ON [dbo].[GT_Cashier_Movements_And_Summary_By_Session] TO [WGGUI] WITH GRANT OPTION
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CashierMovementsGrouped_Select]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[CashierMovementsGrouped_Select]
GO

CREATE PROCEDURE [dbo].[CashierMovementsGrouped_Select]
        @pCashierSessionId NVARCHAR(MAX)
       ,@pDateFrom DATETIME
       ,@pDateTo DATETIME 
  AS
BEGIN

DECLARE @_sql NVARCHAR(MAX)

DECLARE @_CM_CURRENCY_ISO_CODE NVARCHAR(20)
	SELECT @_CM_CURRENCY_ISO_CODE = gp_key_value FROM general_params 
	WHERE gp_group_key = 'RegionalOptions' and gp_subject_key = 'CurrencyISOCode'
	SET @_CM_CURRENCY_ISO_CODE = ISNULL(@_CM_CURRENCY_ISO_CODE, 'NULL')

	IF (@pCashierSessionId IS NOT NULL AND @pDateFrom IS NULL AND @pDateTo IS NULL)  --ByID
	BEGIN     
			SET @_sql =	'
			SELECT	 CM_SESSION_ID
					,CM_TYPE
					,0							  AS CM_SUB_TYPE
					,SUM(CASE WHEN CM_SUB_AMOUNT < 0 OR CM_ADD_AMOUNT < 0 THEN -1 ELSE 1 END)   AS CM_TYPE_COUNT
					,ISNULL(CM_CURRENCY_ISO_CODE ,'''+@_CM_CURRENCY_ISO_CODE+''')  AS CM_CURRENCY_ISO_CODE
					,ISNULL(CM_CURRENCY_DENOMINATION, 0) AS CM_CURRENCY_DENOMINATION
					,SUM(ISNULL(CM_SUB_AMOUNT,0)) CM_SUB_AMOUNT
					,SUM(ISNULL(CM_ADD_AMOUNT,0)) CM_ADD_AMOUNT
					,SUM(ISNULL(CM_AUX_AMOUNT,0)) CM_AUX_AMOUNT
					,SUM(ISNULL(CM_INITIAL_BALANCE,0)) CM_INITIAL_BALANCE
					,SUM(ISNULL(CM_FINAL_BALANCE,0)) CM_FINAL_BALANCE
          ,CM_CAGE_CURRENCY_TYPE
					
			FROM   CASHIER_MOVEMENTS WITH (INDEX (IX_CM_SESSION_ID)) 
			WHERE   CM_SESSION_ID IN ('+@pCashierSessionId+')
			GROUP BY CM_SESSION_ID, CM_TYPE, ISNULL(CM_CURRENCY_ISO_CODE ,'''+@_CM_CURRENCY_ISO_CODE+'''), ISNULL(CM_CURRENCY_DENOMINATION, 0), CM_CAGE_CURRENCY_TYPE
		 	  
 			  UNION ALL 
			   SELECT  mbm_cashier_session_id			       AS CM_SESSION_ID
					   , mbm_type                              AS CM_TYPE  
					   , 1									   AS CM_SUB_TYPE				  
					   , COUNT(mbm_type)			           AS CM_TYPE_COUNT     
					   ,'''+@_CM_CURRENCY_ISO_CODE+'''         AS CM_CURRENCY_ISO_CODE
					   , 0									   AS CM_CURRENCY_DENOMINATION
					   , SUM(ISNULL(mbm_sub_amount,0))		   AS CM_SUB_AMOUNT
					   , SUM(ISNULL(mbm_add_amount,0))		   AS CM_ADD_AMOUNT        
					   , 0									   AS CM_AUX_AMOUNT 
					   , 0									   AS CM_INITIAL_BALANCE
					   , 0                                     AS CM_FINAL_BALANCE
						 , 0										 AS CM_CAGE_CURRENCY_TYPE			   
					    
				 FROM    MB_MOVEMENTS WITH (INDEX (IX_MB_MOVEMENTS))          
				WHERE   mbm_cashier_session_id IN ('+@pCashierSessionId+')
				GROUP BY    mbm_cashier_session_id, mbm_type '
	END --ByID
	ELSE--ByDate
	BEGIN
	SET @_sql =	'
				SELECT   
					  NULL
		 			, CM_TYPE
		 			, 0								AS CM_SUB_TYPE			
		 			, SUM(CASE WHEN CM_SUB_AMOUNT < 0 OR CM_ADD_AMOUNT < 0 THEN -1 ELSE 1 END)   AS CM_TYPE_COUNT
		 			, ISNULL(CM_CURRENCY_ISO_CODE ,'''+@_CM_CURRENCY_ISO_CODE+''') AS CM_CURRENCY_ISO_CODE
		 			, ISNULL(CM_CURRENCY_DENOMINATION, 0) AS CM_CURRENCY_DENOMINATION
		  			, SUM(ISNULL(CM_SUB_AMOUNT,      0)) CM_SUB_AMOUNT
					, SUM(ISNULL(CM_ADD_AMOUNT,      0)) CM_ADD_AMOUNT
					, SUM(ISNULL(CM_AUX_AMOUNT,      0)) CM_AUX_AMOUNT
					, SUM(ISNULL(CM_INITIAL_BALANCE, 0)) CM_INITIAL_BALANCE
					, SUM(ISNULL(CM_FINAL_BALANCE,   0)) CM_FINAL_BALANCE
          , CM_CAGE_CURRENCY_TYPE
				    
			   FROM    CASHIER_MOVEMENTS WITH (INDEX (IX_cm_date_type)) 
			  WHERE  ' +
			  CASE WHEN @pDateFrom IS NULL THEN '' ELSE ' CM_DATE >=  '''+CONVERT(VARCHAR, @pDateFrom, 21) +''' AND '	END +
			  ' CM_DATE <  '''+ISNULL(CONVERT(VARCHAR, @pDateTo, 21),CONVERT(VARCHAR, GETDATE(), 21)) +''''+
			    
			 CASE WHEN @pCashierSessionId IS NULL THEN '' ELSE
			 'AND CM_SESSION_ID IN ('+@pCashierSessionId +')'END +'

		   GROUP BY   CM_TYPE, ISNULL(CM_CURRENCY_ISO_CODE ,'''+@_CM_CURRENCY_ISO_CODE+'''),ISNULL(CM_CURRENCY_DENOMINATION, 0), CM_CAGE_CURRENCY_TYPE
 		  UNION ALL 
		   SELECT
					 NULL   
				   , mbm_type                              AS CM_TYPE 
				   , 1									   AS CM_SUB_TYPE         
				   , COUNT(mbm_type)					   AS CM_TYPE_COUNT
				   ,'''+@_CM_CURRENCY_ISO_CODE+'''         AS CM_CURRENCY_ISO_CODE
				   , 0									   AS CM_CURRENCY_DENOMINATION
				   , SUM(ISNULL(mbm_sub_amount,0))		   AS CM_SUB_AMOUNT
				   , SUM(ISNULL(mbm_add_amount,0))		   AS CM_ADD_AMOUNT        
				   , 0									   AS CM_AUX_AMOUNT 
				   , 0									   AS CM_INITIAL_BALANCE
				   , 0                                     AS CM_FINAL_BALANCE		
				   , 0										 AS CM_CAGE_CURRENCY_TYPE			   
			 FROM    MB_MOVEMENTS WITH (INDEX (IX_MBM_DATETIME_TYPE))             
			WHERE   ' +
			  CASE WHEN @pDateFrom IS NULL THEN '' ELSE ' MBM_DATETIME >=  '''+CONVERT(VARCHAR, @pDateFrom, 21) +''' AND '	END +
			  ' MBM_DATETIME <  '''+ISNULL(CONVERT(VARCHAR, @pDateTo, 21),CONVERT(VARCHAR, GETDATE(), 21)) +''''+
			
					CASE WHEN @pCashierSessionId IS NULL THEN '' ELSE
					'AND mbm_cashier_session_id IN ('+@pCashierSessionId +')'END +'
			GROUP BY    mbm_type '
		
	END--ByDate
 	
	EXEC sp_executesql @_sql
	
 END --CashierMovementsGrouped_Select
 GO
 
 GRANT EXECUTE ON [dbo].[CashierMovementsGrouped_Select] TO [wggui] WITH GRANT OPTION
 GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CashierMovementsHistory]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[CashierMovementsHistory]
GO

CREATE PROCEDURE [dbo].[CashierMovementsHistory]        
                        @pSessionId             BIGINT
                       ,@MovementId             BIGINT
                       ,@pType                  INT    
                       ,@pSubType               INT
                       ,@pInitialBalance        MONEY 
                       ,@pAddAmount             MONEY
                       ,@pSubAmmount            MONEY
                       ,@pFinalBalance          MONEY
                       ,@pCurrencyCode          NVARCHAR(3)
                       ,@pAuxAmount             MONEY
                       ,@pCurrencyDenomination  MONEY
                       ,@pCurrencyCageType      INT
AS
BEGIN 
  DECLARE @_idx_try int
  DECLARE @_row_count int

  SET @pCurrencyDenomination = ISNULL(@pCurrencyDenomination, 0)
  SET @pCurrencyCageType     = ISNULL(@pCurrencyCageType, 0)
  
/******** Grouped by ID ***************/

  SET @_idx_try = 0

  WHILE @_idx_try < 2
  BEGIN
    UPDATE   CASHIER_MOVEMENTS_GROUPED_BY_SESSION_ID
       SET   CM_TYPE_COUNT            = CM_TYPE_COUNT      + CASE WHEN @pSubAmmount < 0 OR @pAddAmount < 0 THEN -1 ELSE 1 END 
           , CM_SUB_AMOUNT            = CM_SUB_AMOUNT      + ISNULL(@pSubAmmount, 0)
           , CM_ADD_AMOUNT            = CM_ADD_AMOUNT      + ISNULL(@pAddAmount, 0)
           , CM_AUX_AMOUNT            = CM_AUX_AMOUNT      + ISNULL(@pAuxAmount, 0)
           , CM_INITIAL_BALANCE       = CM_INITIAL_BALANCE + ISNULL(@pInitialBalance, 0)
           , CM_FINAL_BALANCE         = CM_FINAL_BALANCE   + ISNULL(@pFinalBalance, 0)
     WHERE   CM_SESSION_ID            = @pSessionId 
       AND   CM_TYPE                  = @pType 
       AND   CM_SUB_TYPE              = @pSubType 
       AND   CM_CURRENCY_ISO_CODE     = @pCurrencyCode
       AND   CM_CURRENCY_DENOMINATION = @pCurrencyDenomination
       AND   CM_CAGE_CURRENCY_TYPE    = @pCurrencyCageType

    SET @_row_count = @@ROWCOUNT
   
    IF @_row_count = 1 BREAK

    IF @_idx_try = 1
    BEGIN
      RAISERROR ('Update into historicized table failed. Table = [cashier_movements_grouped_by_session_id]', 11, 0) 
    END

    BEGIN TRY
      -- Entry doesn't exists, lets insert that
      INSERT INTO   CASHIER_MOVEMENTS_GROUPED_BY_SESSION_ID
                  ( CM_SESSION_ID
                  , CM_TYPE
                  , CM_SUB_TYPE
                  , CM_TYPE_COUNT
                  , CM_CURRENCY_ISO_CODE
                  , CM_CURRENCY_DENOMINATION
                  , CM_SUB_AMOUNT
                  , CM_ADD_AMOUNT
                  , CM_AUX_AMOUNT
                  , CM_INITIAL_BALANCE
                  , CM_FINAL_BALANCE
                  , CM_CAGE_CURRENCY_TYPE)
           VALUES
                  ( @pSessionId
                  , @pType        
                  , @pSubType
                  , 1  
                  , @pCurrencyCode
                  , @pCurrencyDenomination
                  , ISNULL(@pSubAmmount, 0)
                  , ISNULL(@pAddAmount, 0)
                  , ISNULL(@pAuxAmount, 0)             
                  , ISNULL(@pInitialBalance, 0)
                  , ISNULL(@pFinalBalance, 0)
                  , ISNULL(@pCurrencyCageType, 0))

			SET @_row_count = @@ROWCOUNT

      -- Update history flag in CASHIER_SESSIONS
      -- UPDATE CASHIER_SESSIONS SET CS_HISTORY = 1 WHERE CS_SESSION_ID = @pSessionId

      -- All ok, exit WHILE      
      BREAK

    END TRY
    BEGIN CATCH
    END CATCH

    SET @_idx_try = @_idx_try + 1
  END
  
  IF @_row_count = 0
  BEGIN
    RAISERROR ('Insert into historicized table failed. Table = [cashier_movements_grouped_by_session_id]', 11, 0) 
  END

  -- AJQ & RCI & RMS 10-FEB-2014: Movements historicized by hour are not inserted here. They serialize all workers and reduce performance.
  --                              A pending cashier movement to historify will be inserted in table CASHIER_MOVEMENTS_PENDING_HISTORY.
  --                              Later a thread will historify these pending movements.
  INSERT INTO CASHIER_MOVEMENTS_PENDING_HISTORY (CMPH_MOVEMENT_ID, CMPH_SUB_TYPE) VALUES (@MovementId, @pSubType) 

END --[CashierMovementsHistory]
GO

GRANT EXECUTE ON [dbo].[CashierMovementsHistory] TO [wggui] WITH GRANT OPTION
GO


IF  EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_GenerateHistoricalMovements_ByHour]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[SP_GenerateHistoricalMovements_ByHour]
GO
 
CREATE PROCEDURE [dbo].[SP_GenerateHistoricalMovements_ByHour]
(
  @pMaxItemsPerLoop Integer, 
  @pCurrecyISOCode VarChar(20)
) 
AS
BEGIN

-- Declarations
DECLARE @_max_movements_per_loop Integer
DECLARE @_total_movements_count   Integer                                   -- To get the total number of items pending to historize
DECLARE @_updated_movements_count Integer                                  -- To get the number of historized movements
DECLARE @_tmp_updating_movements  TABLE  (CMPH_MOVEMENT_ID BigInt,         -- To get the movements id's of items to historize 
                                          CMPH_SUB_TYPE Integer)   
DECLARE @_tmp_updated_movements   TABLE  (UM_DATE DateTime,                -- To get the updated data
                                          UM_TYPE Integer, 
                                          UM_SUBTYPE Integer, 
                                          UM_CURRENCY_ISO_CODE NVarChar(3), 
                                          UM_CURRENCY_DENOMINATION Money,
                                          UM_CAGE_CURRENCY_TYPE Integer )

-- Initialize max items per loop
SET @_max_movements_per_loop = ISNULL(@pMaxItemsPerLoop, 2000) 

-- Get total movements pending of historize
SELECT   @_total_movements_count = COUNT(*) 
  FROM   CASHIER_MOVEMENTS_PENDING_HISTORY 

-- If there are no items in pending movements table
IF @_total_movements_count = 0
BEGIN
   -- Return number of updated movements, and total number of movements
   SELECT CAST(0 AS Integer), CAST(0 AS Integer)
   
   RETURN
END

-- Get the movements id's to be updated
INSERT   INTO @_tmp_updating_movements
SELECT   TOP(@_max_movements_per_loop) 
         CMPH_MOVEMENT_ID
       , CMPH_SUB_TYPE
  FROM   CASHIER_MOVEMENTS_PENDING_HISTORY 

-- Obtain the number of movements to update
SELECT   @_updated_movements_count = COUNT(*) 
  FROM   @_tmp_updating_movements 

-- If there are items to update process it
IF @_updated_movements_count = 0
BEGIN
   -- Return number of updated movements, and total number of movements
   SELECT CAST(0 AS Integer), @_total_movements_count 
   
   RETURN
END

-- DELETE THE TO BE UPDATED REGS FROM PENDING
DELETE   X
  FROM   CASHIER_MOVEMENTS_PENDING_HISTORY AS X
 INNER   JOIN @_tmp_updating_movements AS Z ON Z.CMPH_MOVEMENT_ID = X.CMPH_MOVEMENT_ID AND Z.CMPH_SUB_TYPE = X.CMPH_SUB_TYPE

-- Create a temporary table with grouped items by PK for the CASHIER_MOVEMENTS (SUB_TYPE = 0)
SELECT   X.CM_DATE
       , X.CM_TYPE
       , X.CM_SUB_TYPE
       , X.CM_CURRENCY_ISO_CODE
       , X.CM_CURRENCY_DENOMINATION
       , X.CM_TYPE_COUNT
       , X.CM_SUB_AMOUNT
       , X.CM_ADD_AMOUNT
       , X.CM_AUX_AMOUNT
       , X.CM_INITIAL_BALANCE
       , X.CM_FINAL_BALANCE
       , X.CM_CAGE_CURRENCY_TYPE
  INTO   #TMP_MOVEMENTS_HISTORIZE_PENDING
  FROM
       (
         SELECT   DATEADD(HOUR, DATEDIFF(HOUR, 0, CM_DATE), 0) AS CM_DATE
                , CM_TYPE
                , 0 AS CM_SUB_TYPE
                , ISNULL(CM_CURRENCY_ISO_CODE, @pCurrecyISOCode) AS CM_CURRENCY_ISO_CODE
                , ISNULL(CM_CURRENCY_DENOMINATION, 0)            AS CM_CURRENCY_DENOMINATION
                , SUM(CASE WHEN CM_SUB_AMOUNT < 0 OR CM_ADD_AMOUNT < 0 THEN -1 ELSE 1 END)    AS CM_TYPE_COUNT
                , SUM(CM_SUB_AMOUNT)      AS CM_SUB_AMOUNT
                , SUM(CM_ADD_AMOUNT)      AS CM_ADD_AMOUNT
                , SUM(CM_AUX_AMOUNT)      AS CM_AUX_AMOUNT
                , SUM(CM_INITIAL_BALANCE) AS CM_INITIAL_BALANCE
                , SUM(CM_FINAL_BALANCE)   AS CM_FINAL_BALANCE
                , ISNULL(CM_CAGE_CURRENCY_TYPE, 0)               AS CM_CAGE_CURRENCY_TYPE
           FROM   @_tmp_updating_movements
          INNER   JOIN CASHIER_MOVEMENTS ON CMPH_MOVEMENT_ID = CM_MOVEMENT_ID
          WHERE   CMPH_SUB_TYPE = 0      -- Cashier movements
       GROUP BY   DATEADD(HOUR, DATEDIFF(HOUR, 0, CM_DATE), 0)
                , CM_TYPE
                , ISNULL(CM_CURRENCY_ISO_CODE, @pCurrecyISOCode)
                , ISNULL(CM_CURRENCY_DENOMINATION, 0)
                , ISNULL(CM_CAGE_CURRENCY_TYPE, 0)
       ) AS X 

---- Add to the temporary table items by PK for the MOBILE_BANK_MOVEMENTS (SUB_TYPE = 1)
INSERT INTO   #TMP_MOVEMENTS_HISTORIZE_PENDING
     SELECT   DATEADD(HOUR, DATEDIFF(HOUR, 0, MBM_DATETIME), 0) AS CM_DATE
            , MBM_TYPE   AS CM_TYPE
            , 1          AS CM_SUB_TYPE
            , @pCurrecyISOCode AS CM_CURRENCY_ISO_CODE
            , 0                AS CM_CURRENCY_DENOMINATION
            , COUNT(MBM_TYPE)  AS CM_TYPE_COUNT
            , SUM(MBM_SUB_AMOUNT) AS CM_SUB_AMOUNT
            , SUM(MBM_ADD_AMOUNT) AS CM_ADD_AMOUNT
            , 0 AS CM_AUX_AMOUNT
            , 0 AS CM_INITIAL_BALANCE
            , 0 AS CM_FINAL_BALANCE
            , 0 AS CM_CAGE_CURRENCY_TYPE
       FROM   @_tmp_updating_movements
      INNER   JOIN MB_MOVEMENTS ON CMPH_MOVEMENT_ID = MBM_MOVEMENT_ID
      WHERE   CMPH_SUB_TYPE = 1       -- Mobile Bank movements
   GROUP BY   DATEADD(HOUR, DATEDIFF(HOUR, 0, MBM_DATETIME), 0)
            , MBM_TYPE 

-- Update the items already in database triggering the updates into a temporary table of updated elements
UPDATE   CASHIER_MOVEMENTS_GROUPED_BY_HOUR
   SET   CM_TYPE_COUNT      = T.CM_TYPE_COUNT       + S.CM_TYPE_COUNT
       , CM_SUB_AMOUNT      = T.CM_SUB_AMOUNT       + ISNULL(S.CM_SUB_AMOUNT, 0)
       , CM_ADD_AMOUNT      = T.CM_ADD_AMOUNT       + ISNULL(S.CM_ADD_AMOUNT, 0)
       , CM_AUX_AMOUNT      = T.CM_AUX_AMOUNT       + ISNULL(S.CM_AUX_AMOUNT, 0)
       , CM_INITIAL_BALANCE = T.CM_INITIAL_BALANCE  + ISNULL(S.CM_INITIAL_BALANCE, 0)
       , CM_FINAL_BALANCE   = T.CM_FINAL_BALANCE    + ISNULL(S.CM_FINAL_BALANCE, 0)
OUTPUT   INSERTED.CM_DATE
       , INSERTED.CM_TYPE
       , INSERTED.CM_SUB_TYPE
       , INSERTED.CM_CURRENCY_ISO_CODE
       , INSERTED.CM_CURRENCY_DENOMINATION
       , INSERTED.CM_CAGE_CURRENCY_TYPE
  INTO   @_tmp_updated_movements(UM_DATE, UM_TYPE, UM_SUBTYPE, UM_CURRENCY_ISO_CODE, UM_CURRENCY_DENOMINATION, UM_CAGE_CURRENCY_TYPE)
  FROM   CASHIER_MOVEMENTS_GROUPED_BY_HOUR T
 INNER   JOIN #TMP_MOVEMENTS_HISTORIZE_PENDING S ON T.CM_DATE                  = S.CM_DATE
                                                AND T.CM_TYPE                  = S.CM_TYPE
                                                AND T.CM_SUB_TYPE              = S.CM_SUB_TYPE
                                                AND T.CM_CURRENCY_ISO_CODE     = S.CM_CURRENCY_ISO_CODE
                                                AND T.CM_CURRENCY_DENOMINATION = S.CM_CURRENCY_DENOMINATION 
                                                AND T.CM_CAGE_CURRENCY_TYPE    = S.CM_CAGE_CURRENCY_TYPE

-- Insert the elements not previously updated (update items not present in temporary updated items)
INSERT INTO   CASHIER_MOVEMENTS_GROUPED_BY_HOUR
           (   CM_DATE
             , CM_TYPE
             , CM_SUB_TYPE
             , CM_CURRENCY_ISO_CODE
             , CM_CURRENCY_DENOMINATION
             , CM_TYPE_COUNT
             , CM_SUB_AMOUNT     
             , CM_ADD_AMOUNT      
             , CM_AUX_AMOUNT     
             , CM_INITIAL_BALANCE 
             , CM_FINAL_BALANCE  
             , CM_CAGE_CURRENCY_TYPE )
     SELECT   CM_DATE
            , CM_TYPE
            , CM_SUB_TYPE
            , CM_CURRENCY_ISO_CODE
            , CM_CURRENCY_DENOMINATION
            , CM_TYPE_COUNT
            , CM_SUB_AMOUNT     
            , CM_ADD_AMOUNT      
            , CM_AUX_AMOUNT     
            , CM_INITIAL_BALANCE 
            , CM_FINAL_BALANCE
            , CM_CAGE_CURRENCY_TYPE 
       FROM   #TMP_MOVEMENTS_HISTORIZE_PENDING 
      WHERE   NOT EXISTS(SELECT   1
                           FROM   @_tmp_updated_movements NTI
                          WHERE   NTI.UM_DATE                  = CM_DATE
                            AND   NTI.UM_TYPE                  = CM_TYPE
                            AND   NTI.UM_SUBTYPE               = CM_SUB_TYPE
                            AND   NTI.UM_CURRENCY_ISO_CODE     = CM_CURRENCY_ISO_CODE
                            AND   NTI.UM_CURRENCY_DENOMINATION = CM_CURRENCY_DENOMINATION
                            AND   NTI.UM_CAGE_CURRENCY_TYPE    = CM_CAGE_CURRENCY_TYPE) 

-- Get total movements pending of historize
SELECT   @_total_movements_count = COUNT(*) 
  FROM   CASHIER_MOVEMENTS_PENDING_HISTORY 
  
-- Return values
SELECT @_updated_movements_count, @_total_movements_count

-- Erase the temporary data
DROP TABLE #TMP_MOVEMENTS_HISTORIZE_PENDING 

END  -- End Procedure
GO

GRANT EXECUTE ON [dbo].[SP_GenerateHistoricalMovements_ByHour] TO [wggui] WITH GRANT OPTION
GO

IF  EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CM_OldHistoryByHour]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[CM_OldHistoryByHour]
GO

CREATE PROCEDURE [dbo].[CM_OldHistoryByHour]
      @Date1 DATETIME,
      @CM_CURRENCY_ISO_CODE NVARCHAR(20) = NULL -- If @CM_CURRENCY_ISO_CODE == NULL Then (Select CurrencyISOCode From general_params) EndIf
      
AS
BEGIN
            DECLARE @Date2   DATETIME

            IF @CM_CURRENCY_ISO_CODE IS NULL BEGIN
                  SELECT @CM_CURRENCY_ISO_CODE = gp_key_value FROM general_params 
                  WHERE gp_group_key = 'RegionalOptions' and gp_subject_key = 'CurrencyISOCode'
            END

            SET @Date1 = DATEADD(HOUR, DATEDIFF(HOUR, 0, @Date1), 0)
            SET @Date2 = DATEADD (HOUR, 1, @Date1)

            DELETE FROM CASHIER_MOVEMENTS_GROUPED_BY_HOUR WHERE   CM_DATE >= @Date1 AND CM_DATE <  @Date2 AND CM_SUB_TYPE <> 2 -- 0=CASHIER_MOVEMENTS; 1=MB_MOVEMENTS; 2=LIABILITIES

            INSERT INTO   CASHIER_MOVEMENTS_GROUPED_BY_HOUR 
                                    ( CM_DATE
                                   , CM_TYPE
                                   , CM_SUB_TYPE
                                   , CM_TYPE_COUNT
                                   , CM_CURRENCY_ISO_CODE
                                   , CM_CURRENCY_DENOMINATION
                                   , CM_SUB_AMOUNT
                                   , CM_ADD_AMOUNT
                                   , CM_AUX_AMOUNT
                                   , CM_INITIAL_BALANCE
                                   , CM_FINAL_BALANCE
                                   )     
                    SELECT   @Date1 CM_DATE
                                   , CM_TYPE
                                   , 0                             AS CM_SUB_TYPE                  --CAIXER MOVEMENT
                                   , SUM(CASE WHEN CM_SUB_AMOUNT < 0 OR CM_ADD_AMOUNT < 0 THEN -1 ELSE 1 END)    AS CM_TYPE_COUNT
                                   , ISNULL(CM_CURRENCY_ISO_CODE, @CM_CURRENCY_ISO_CODE) AS CM_CURRENCY_ISO_CODE
                                   , ISNULL(CM_CURRENCY_DENOMINATION, 0) AS CM_CURRENCY_DENOMINATION
                                     , SUM(ISNULL(CM_SUB_AMOUNT,       0)) CM_SUB_AMOUNT
                                   , SUM(ISNULL(CM_ADD_AMOUNT,        0)) CM_ADD_AMOUNT
                                   , SUM(ISNULL(CM_AUX_AMOUNT,        0)) CM_AUX_AMOUNT
                                   , SUM(ISNULL(CM_INITIAL_BALANCE,   0)) CM_INITIAL_BALANCE
                                   , SUM(ISNULL(CM_FINAL_BALANCE,     0)) CM_FINAL_BALANCE
                      
                             FROM    CASHIER_MOVEMENTS WITH (INDEX (IX_CM_DATE_TYPE)) 
                             WHERE   CM_DATE >= @Date1
                                   AND   CM_DATE <  @Date2
                             GROUP BY   CM_TYPE, ISNULL(CM_CURRENCY_ISO_CODE, @CM_CURRENCY_ISO_CODE), ISNULL(CM_CURRENCY_DENOMINATION, 0)
            UNION ALL 
                    SELECT  @Date1                      AS CM_DATE
                        , mbm_type                      AS CM_TYPE 
                         , 1                             AS CM_SUB_TYPE         --MBANK MOVEMENT
                        , COUNT(mbm_type)               AS CM_TYPE_COUNT
                        , @CM_CURRENCY_ISO_CODE         AS CM_CURRENCY_ISO_CODE
                        , 0                             AS CM_CURRENCY_DENOMINATION
                        , SUM(ISNULL(mbm_sub_amount,0)) AS CM_SUB_AMOUNT
                        , SUM(ISNULL(mbm_add_amount,0)) AS CM_ADD_AMOUNT        
                         , 0                             AS CM_AUX_AMOUNT 
                         , 0                             AS CM_INITIAL_BALANCE
                        , 0                             AS CM_FINAL_BALANCE
                      
             FROM    MB_MOVEMENTS WITH (INDEX (IX_MBM_DATETIME_TYPE))
            WHERE   mbm_datetime >= @Date1
                             AND   mbm_datetime <  @Date2
            GROUP BY    mbm_type 
            
RETURN

END--[CM_OldHistoryByHour]
GO

GRANT EXECUTE ON [dbo].CM_OldHistoryByHour TO [wggui] WITH GRANT OPTION
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_Cashier_Sessions_Report]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[SP_Cashier_Sessions_Report]
GO

CREATE PROCEDURE [dbo].[SP_Cashier_Sessions_Report]
(  @pDateFrom DateTime
  , @pDateTo DateTime
  , @pReportMode as Int
  , @pShowSystemSessions as Bit
  , @pSystemUsersTypes as VarChar(4096)
  , @pSessionStatus as VarChar(4096)
  , @pUserId as Integer
  , @pCashierId as Integer
  , @pCurrencyISOCode as VarChar(3)
)
AS
BEGIN

-- Members --
DECLARE @_delimiter AS CHAR(1)
SET @_delimiter = ','
DECLARE @pSystemUsersTypesTable AS TABLE (SUT_ID INT, SUT_VALUE VARCHAR(50))
DECLARE @pSessionsStatusTable AS TABLE (SUT_ID INT, SUT_VALUE VARCHAR(50))
DECLARE @_table_sessions_ AS TABLE (  sess_id                      BIGINT 
                                    , sess_name                    NVARCHAR(50) 
                                    , sess_user_id                 INT      
                                    , sess_full_name               NVARCHAR(50) 
                                    , sess_opening_date            DATETIME 
                                    , sess_closing_date            DATETIME  
                                    , sess_cashier_id              INT 
                                    , sess_ct_name                 NVARCHAR(50) 
                                    , sess_status                  INT 
                                    , sess_cs_limit_sale           MONEY
                                    , sess_mb_limit_sale           MONEY
                                    , sess_collected_amount        MONEY
                                    , sess_gaming_day              DATETIME
                                    , sess_te_iso_code             NVARCHAR(3)
                                   ) 
                                                           
-- SYSTEM USER TYPES --
IF @pShowSystemSessions = 0
BEGIN
INSERT INTO @pSystemUsersTypesTable SELECT * FROM dbo.SplitStringIntoTable(@pSystemUsersTypes, @_delimiter, DEFAULT)
END

-- SESSIONS STATUS
INSERT INTO @pSessionsStatusTable SELECT * FROM dbo.SplitStringIntoTable(@pSessionStatus, @_delimiter, DEFAULT)

IF @pReportMode = 0 OR @pReportMode = 2
   BEGIN -- Filter Dates on cashier session opening date  

    IF @pReportMode = 0
      BEGIN
        -- SESSIONS BY OPENING DATE --
        INSERT   INTO @_table_sessions_
        SELECT   DISTINCT(CS_SESSION_ID) AS SESS_ID
               , CS_NAME AS SESS_NAME
               , CS_USER_ID AS SESS_USER_ID
               , GU_FULL_NAME AS SESS_FULL_NAME
               , CS_OPENING_DATE AS SESS_OPENING_DATE
               , CS_CLOSING_DATE AS SESS_CLOSING_DATE
               , CS_CASHIER_ID AS SESS_CASHIER_ID
               , CT_NAME AS SESS_CT_NAME
               , CS_STATUS AS SESS_STATUS
               , CS_SALES_LIMIT AS SESS_CS_LIMIT_SALE
               , CS_MB_SALES_LIMIT AS SESS_MB_LIMIT_SALE
               , CS_COLLECTED_AMOUNT AS SESS_COLLECTED_AMOUNT
               , CS_GAMING_DAY AS SESS_GAMING_DAY
               , ISNULL(TE_ISO_CODE, @pCurrencyISOCode) AS SESS_TE_ISO_CODE 
          FROM   CASHIER_SESSIONS --WITH (INDEX (IX_cs_opening_date)) /* TODO: REVIEW INDEXES */
         INNER   JOIN GUI_USERS         ON CS_USER_ID    = GU_USER_ID
         INNER   JOIN CASHIER_TERMINALS ON CS_CASHIER_ID = CT_CASHIER_ID
          LEFT   JOIN TERMINALS         ON CT_TERMINAL_ID = TE_TERMINAL_ID   
         WHERE   (CS_OPENING_DATE >= @pDateFrom AND CS_OPENING_DATE < @pDateTo)
            --   usuarios sistema s/n & user 
           AND   (      GU_USER_TYPE NOT IN ( SELECT SUT_VALUE FROM @pSystemUsersTypesTable )
                  AND   CS_USER_ID = (CASE WHEN @pUserId IS NULL THEN CS_USER_ID ELSE @pUserId END) )
            --   cashier id
           AND   CS_CASHIER_ID = (CASE WHEN @pCashierId IS NULL THEN CS_CASHIER_ID ELSE @pCashierId END) 
            --   session status
           AND   CS_STATUS IN ( SELECT SUT_VALUE FROM @pSessionsStatusTable )
         ORDER   BY CS_OPENING_DATE DESC  
      END
    ELSE
      BEGIN
        -- @pReportMode = 2
        -- SESSIONS BY GAMING DAY --
        INSERT   INTO @_table_sessions_
        SELECT   DISTINCT(CS_SESSION_ID) AS SESS_ID
               , CS_NAME AS SESS_NAME
               , CS_USER_ID AS SESS_USER_ID
               , GU_FULL_NAME AS SESS_FULL_NAME
               , CS_OPENING_DATE AS SESS_OPENING_DATE
               , CS_CLOSING_DATE AS SESS_CLOSING_DATE
               , CS_CASHIER_ID AS SESS_CASHIER_ID
               , CT_NAME AS SESS_CT_NAME
               , CS_STATUS AS SESS_STATUS
               , CS_SALES_LIMIT AS SESS_CS_LIMIT_SALE
               , CS_MB_SALES_LIMIT AS SESS_MB_LIMIT_SALE
               , CS_COLLECTED_AMOUNT AS SESS_COLLECTED_AMOUNT
               , CS_GAMING_DAY AS SESS_GAMING_DAY
               , ISNULL(TE_ISO_CODE, @pCurrencyISOCode) AS SESS_TE_ISO_CODE 
          FROM   CASHIER_SESSIONS --WITH (INDEX (IX_cs_opening_date)) /* TODO: REVIEW INDEXES */
         INNER   JOIN GUI_USERS         ON CS_USER_ID    = GU_USER_ID
         INNER   JOIN CASHIER_TERMINALS ON CS_CASHIER_ID = CT_CASHIER_ID
          LEFT   JOIN TERMINALS         ON CT_TERMINAL_ID = TE_TERMINAL_ID   
         WHERE   (CS_GAMING_DAY >= @pDateFrom AND CS_GAMING_DAY < @pDateTo)
            --   usuarios sistema s/n & user 
           AND   (      GU_USER_TYPE NOT IN ( SELECT SUT_VALUE FROM @pSystemUsersTypesTable )
                  AND   CS_USER_ID = (CASE WHEN @pUserId IS NULL THEN CS_USER_ID ELSE @pUserId END) )
            --   cashier id
           AND   CS_CASHIER_ID = (CASE WHEN @pCashierId IS NULL THEN CS_CASHIER_ID ELSE @pCashierId END) 
            --   session status
           AND   CS_STATUS IN ( SELECT SUT_VALUE FROM @pSessionsStatusTable )
         ORDER   BY CS_GAMING_DAY DESC  
      END

    -- MOVEMENTS
    -- By Movements in Historical Data --    
    SELECT   CM_SESSION_ID
           , CM_TYPE
           , CM_SUB_TYPE
           , ISNULL(CM_CURRENCY_ISO_CODE, @pCurrencyISOCode) AS CM_CURRENCY_ISO_CODE
           , CM_CURRENCY_DENOMINATION
           , CM_TYPE_COUNT
           , CM_SUB_AMOUNT
           , CM_ADD_AMOUNT
           , CM_AUX_AMOUNT
           , CM_INITIAL_BALANCE
           , CM_FINAL_BALANCE
           , CM_CAGE_CURRENCY_TYPE
      FROM   CASHIER_MOVEMENTS_GROUPED_BY_SESSION_ID
     WHERE   CM_SESSION_ID IN (SELECT SESS_ID FROM @_table_sessions_)

   END
ELSE
  BEGIN -- Filter Dates on cashier movements date (@pReportMode = 1)
       
      -- MOVEMENTS
      -- By Cashier_Movements --   
      SELECT * 
      INTO #CASHIER_MOVEMENTS
      FROM ( 
            SELECT   CM_DATE
                   , CM_SESSION_ID
                   , CM_TYPE
                   , 0 as CM_SUB_TYPE
                   , ISNULL(CM_CURRENCY_ISO_CODE, @pCurrencyISOCode) AS CM_CURRENCY_ISO_CODE
                   , CM_CURRENCY_DENOMINATION
                   , CASE WHEN CM_SUB_AMOUNT < 0 OR CM_ADD_AMOUNT < 0 THEN -1 ELSE 1 END    AS CM_TYPE_COUNT
                   , CM_SUB_AMOUNT
                   , CM_ADD_AMOUNT
                   , CM_AUX_AMOUNT
                   , CM_INITIAL_BALANCE
                   , CM_FINAL_BALANCE
                   , ISNULL(CM_CAGE_CURRENCY_TYPE, 0) AS CM_CAGE_CURRENCY_TYPE
              FROM   CASHIER_MOVEMENTS 
             WHERE   CM_DATE >= @pDateFrom AND CM_DATE < @pDateTo
             UNION   ALL
            SELECT   MBM_DATETIME AS CM_DATE
                   , MBM_CASHIER_SESSION_ID AS CM_SESSION_ID
                   , MBM_TYPE AS CM_TYPE
                   , 1 AS CM_SUB_TYPE
                   , @pCurrencyISOCode AS CM_CURRENCY_ISO_CODE
                   , 0 AS CM_CURRENCY_DENOMINATION
                   , 1 AS CM_TYPE_COUNT
                   , ISNULL(MBM_SUB_AMOUNT, 0) AS CM_SUB_AMOUNT
                   , ISNULL(MBM_ADD_AMOUNT, 0) AS CM_ADD_AMOUNT
                   , 0 AS CM_AUX_AMOUNT
                   , 0 AS CM_INITIAL_BALANCE
                   , 0 AS CM_FINAL_BALANCE
                   , 0 AS CM_CAGE_CURRENCY_TYPE
              FROM   MB_MOVEMENTS 
             WHERE   MBM_DATETIME >= @pDateFrom AND MBM_DATETIME < @pDateTo
       ) AS MOVEMENTS
       
      CREATE NONCLUSTERED INDEX IX_CM_SESSION_ID ON #CASHIER_MOVEMENTS(CM_SESSION_ID)

      -- SESSIONS --  
      INSERT   INTO @_table_sessions_  
      SELECT   DISTINCT(CS_SESSION_ID) AS SESS_ID  
             , CS_NAME AS SESS_NAME  
             , CS_USER_ID AS SESS_USER_ID  
             , GU_FULL_NAME AS SESS_FULL_NAME  
             , CS_OPENING_DATE AS SESS_OPENING_DATE  
			       , CS_CLOSING_DATE AS SESS_CLOSING_DATE  
             , CS_CASHIER_ID AS SESS_CASHIER_ID  
             , CT_NAME AS SESS_CT_NAME  
             , CS_STATUS AS SESS_STATUS  
             , CS_SALES_LIMIT AS SESS_CS_LIMIT_SALE  
             , CS_MB_SALES_LIMIT AS SESS_MB_LIMIT_SALE  
             , CS_COLLECTED_AMOUNT AS SESS_COLLECTED_AMOUNT  
             , CS_GAMING_DAY AS SESS_GAMING_DAY  
			       , ISNULL(TE_ISO_CODE, @pCurrencyISOCode) AS SESS_TE_ISO_CODE  
        FROM   CASHIER_SESSIONS --WITH (INDEX (IX_cs_opening_date)) /* TODO: REVIEW INDEXES */  
       INNER   JOIN GUI_USERS         ON CS_USER_ID    = GU_USER_ID  
       INNER   JOIN CASHIER_TERMINALS ON CS_CASHIER_ID = CT_CASHIER_ID  
	      LEFT   JOIN TERMINALS         ON CT_TERMINAL_ID = TE_TERMINAL_ID   
       WHERE   CS_SESSION_ID IN (SELECT CM_SESSION_ID FROM #CASHIER_MOVEMENTS)  
          --   usuarios sistema s/n & user   
         AND   (      GU_USER_TYPE NOT IN ( SELECT SUT_VALUE FROM @pSystemUsersTypesTable )  
                AND   CS_USER_ID = (CASE WHEN @pUserId IS NULL THEN CS_USER_ID ELSE @pUserId END) )  
          --   cashier id  
         AND   CS_CASHIER_ID = (CASE WHEN @pCashierId IS NULL THEN CS_CASHIER_ID ELSE @pCashierId END)   
          --   session status  
         AND   CS_STATUS IN ( SELECT SUT_VALUE FROM @pSessionsStatusTable )  
       ORDER   BY CS_OPENING_DATE DESC    
         
       SELECT * FROM #CASHIER_MOVEMENTS INNER JOIN @_table_sessions_ ON SESS_ID = CM_SESSION_ID  
         
       DROP TABLE #CASHIER_MOVEMENTS        
  
   END   
  
-- SESSIONS with CAGE INFO  
SELECT   SESS_ID  
       , SESS_NAME  
       , SESS_USER_ID  
       , SESS_FULL_NAME  
       , SESS_OPENING_DATE  
       , SESS_CLOSING_DATE  
       , SESS_CASHIER_ID  
       , SESS_CT_NAME  
       , SESS_STATUS  
       , SESS_CS_LIMIT_SALE  
       , SESS_MB_LIMIT_SALE  
       , SESS_COLLECTED_AMOUNT   
       , X.CGS_CAGE_SESSION_ID AS CGS_CAGE_SESSION_ID  
       , X.CGS_SESSION_NAME AS CGS_SESSION_NAME   
       , SESS_GAMING_DAY  
	     , SESS_TE_ISO_CODE 
  FROM   @_table_sessions_  
  LEFT   JOIN ( SELECT   CGS_SESSION_NAME  
                       , CGM_CASHIER_SESSION_ID  
                       , CGS_CAGE_SESSION_ID   
                  FROM   CAGE_MOVEMENTS AS CM  
                 INNER   JOIN CAGE_SESSIONS ON CM.CGM_CAGE_SESSION_ID = CGS_CAGE_SESSION_ID  
                 WHERE   CM.CGM_MOVEMENT_ID = (SELECT    TOP 1 CGM.CGM_MOVEMENT_ID  
                                                 FROM    CAGE_MOVEMENTS AS CGM   
                                                WHERE    CGM.CGM_CASHIER_SESSION_ID = CM.CGM_CASHIER_SESSION_ID)  
               ) AS X ON X.CGM_CASHIER_SESSION_ID = [@_table_sessions_].SESS_ID 
                               
-- SESSIONS CURRENCIES INFORMATION
IF @pReportMode = 0 OR @pReportMode = 2
    SELECT   CSC_SESSION_ID
           , CSC_ISO_CODE
           , CSC_TYPE
           , CSC_BALANCE
           , CSC_COLLECTED 
      FROM   CASHIER_SESSIONS_BY_CURRENCY 
     INNER JOIN @_table_sessions_ ON SESS_ID = CSC_SESSION_ID

END -- End Procedure
GO

GRANT EXECUTE ON [dbo].[SP_Cashier_Sessions_Report] TO [wggui] WITH GRANT OPTION
GO

IF OBJECT_ID (N'dbo.GT_Base_Report_Data_Player_Tracking', N'P') IS NOT NULL
    DROP PROCEDURE dbo.GT_Base_Report_Data_Player_Tracking;
GO

CREATE PROCEDURE [dbo].[GT_Base_Report_Data_Player_Tracking]
(
    @pBaseType            INTEGER
  , @pTimeInterval        INTEGER
  , @pDateFrom            DATETIME
  , @pDateTo              DATETIME
  , @pOnlyActivity        INTEGER
  , @pOrderBy             INTEGER
  , @pValidTypes          VARCHAR(4096)
  , @pSelectedCurrency    NVARCHAR(3)
)
AS
BEGIN
-- DECLARATIONS
   -- @_DAYS_AND_TABLES:    A temporary table with all intervals and all base type selected (used to show base types without activity).
   DECLARE @_DAYS_AND_TABLES TABLE(DATE_TIME DATETIME, TABLE_IDENTIFIER_VALUE BIGINT, TABLE_IDENT_NAME VARCHAR(50), TABLE_TYPE_IDENT BIGINT, TABLE_TYPE_NAME VARCHAR(50))
   -- @_DAY_VAR:            Variable to create the intervals
   DECLARE @_DAY_VAR DATETIME

-- PARAMETERS
   DECLARE @_BASE_TYPE        AS   INTEGER -- 0 BY TABLE TYPE, 1 BY TABLE
   DECLARE @_TIME_INTERVAL    AS   INTEGER -- -1, 0 DAYS, 1 MONTH, 2 YEAR
   DECLARE @_DATE_FROM        AS   DATETIME
   DECLARE @_DATE_TO          AS   DATETIME
   DECLARE @_ONLY_ACTIVITY    AS   INTEGER -- 0 SHOW ALL, 1 SHOW ONLY WITH ACTIVITY
   DECLARE @_ORDER_BY         AS   INTEGER -- 0 BY INTERVAL DESC, 1 BY IDENTIFIER ASC
   DECLARE @_DELIMITER        AS   CHAR(1)
   DECLARE @_TYPES_TABLE      TABLE(SST_ID INT, SST_VALUE VARCHAR(50)) -- TO STORE THE VALID GAMING TABLE TYPES
   DECLARE @_SELECTED_CURRENCY AS  NVARCHAR(3)

----------------------------------------------------------------------------------------------------------------

-- Initialization --
SET @_BASE_TYPE        =   @pBaseType
SET @_TIME_INTERVAL    =   @pTimeInterval
SET @_DATE_FROM        =   @pDateFrom
SET @_DATE_TO          =   @pDateTo
SET @_ONLY_ACTIVITY    =   @pOnlyActivity
SET @_ORDER_BY         =   @pOrderBy
SET @_DELIMITER        =   ','
SET @_SELECTED_CURRENCY =  @pSelectedCurrency

----------------------------------------------------------------------------------------------------------------

-- CHECK DATE PARAMETERS
IF @_DATE_FROM IS NULL
BEGIN
   -- IF DATE FROM IS NULL, USER FIRST POSIBLE DATE
   SET @_DATE_FROM = CAST('' AS DATETIME)
   SET @pDateFrom  = CAST('' AS DATETIME)
END

IF @_DATE_TO IS NULL
BEGIN
   -- IF DATE TO IS NULL, QUERY UNTIL CURRENT DATE
   SET @_DATE_TO = CAST(GETDATE() AS DATETIME)
   SET @pDateTo  = CAST(GETDATE() AS DATETIME)
END

-- ASSIGN TYPES PARAMETER INTO TABLE
INSERT INTO @_TYPES_TABLE SELECT * FROM dbo.SplitStringIntoTable(@pValidTypes, @_DELIMITER, DEFAULT)

IF @_TIME_INTERVAL <> -1
 BEGIN
   -- INTERVALS DATES AND TABLE PREPARATION
   -- PREPARE DATES RANGE DEPENDING ON TIME INTERVAL
   SET @_DATE_FROM = CASE
                        WHEN @_TIME_INTERVAL = 0 THEN -- BY DAY
                           DATEADD(dd, 0, DATEDIFF(dd, 0, @_DATE_FROM))
                        WHEN @_TIME_INTERVAL = 1 THEN -- BY MONTH
                           CAST(CAST(YEAR(@_DATE_FROM) AS VARCHAR(4)) + RIGHT('0' + CAST(MONTH(@_DATE_FROM) AS VARCHAR(2)), 2) + '01' AS DATETIME)
                        WHEN @_TIME_INTERVAL = 2 THEN -- BY YEAR
                           CAST(CAST(YEAR(@_DATE_FROM) AS VARCHAR(4)) + '0101' AS DATETIME)
                     END

   SET @_DATE_TO = CASE
                        WHEN @_TIME_INTERVAL = 0 THEN -- BY DAY
                           DATEADD(DAY, 1, CAST(@_DATE_TO AS DATETIME))
                        WHEN @_TIME_INTERVAL = 1 THEN -- BY MONTH
                           DATEADD(MONTH, 1, CAST(CAST(YEAR(@_DATE_TO) AS VARCHAR(4)) + RIGHT('0' + CAST(MONTH(@_DATE_TO) AS VARCHAR(2)), 2) + '01' AS DATETIME))
                        WHEN @_TIME_INTERVAL = 2 THEN -- BY YEAR
                           DATEADD(YEAR, 1, CAST(CAST(YEAR(@_DATE_TO) AS VARCHAR(4)) + '0101' AS DATETIME))
                     END

   -- PREPARE THE TIME INTERVALS TABLE
   SET @_DAY_VAR = @_DATE_FROM
   WHILE @_DAY_VAR < @_DATE_TO AND @_DAY_VAR < GETDATE()
   BEGIN
         -- SET THE LINK FIELD FOR THE RESULTS
         IF @_BASE_TYPE = 0
          BEGIN
            -- LINK WITH TABLE TYPES
            INSERT INTO @_DAYS_AND_TABLES
                 SELECT   CAST(@_DAY_VAR AS DATETIME), X.GTT_GAMING_TABLE_TYPE_ID, X.GTT_NAME AS TABLE_IDENT, NULL, NULL
                   FROM   GAMING_TABLES_TYPES AS X
                  WHERE   X.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
          END
         ELSE
          BEGIN
            -- LINK WITH TABLES
            INSERT INTO @_DAYS_AND_TABLES
                 SELECT   CAST(@_DAY_VAR AS DATETIME), X.GT_GAMING_TABLE_ID, X.GT_NAME AS TABLE_IDENT, Z.GTT_GAMING_TABLE_TYPE_ID, Z.GTT_NAME
                   FROM   GAMING_TABLES AS X
              LEFT JOIN   GAMING_TABLES_TYPES AS Z
                     ON   Z.GTT_GAMING_TABLE_TYPE_ID = X.GT_TYPE_ID
                  WHERE   Z.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
          END

          -- SET INCREMENT
          SET @_DAY_VAR = CASE
                               WHEN @_TIME_INTERVAL = 0 THEN DATEADD(DAY,1,@_DAY_VAR)     -- DAY
                               WHEN @_TIME_INTERVAL = 1 THEN DATEADD(MONTH,1,@_DAY_VAR)   -- MONTH
                               WHEN @_TIME_INTERVAL = 2 THEN DATEADD(YEAR,1,@_DAY_VAR)    -- YEAR
                          END
   END

END -- IF INTERVALS <> - 1
ELSE
BEGIN

   -- SET THE LINK FIELD FOR THE RESULTS WITHOUT INTERVALS
   IF @_BASE_TYPE = 0
    BEGIN
      -- LINK WITH TABLE TYPES
      INSERT INTO   @_DAYS_AND_TABLES
           SELECT   NULL, X.GTT_GAMING_TABLE_TYPE_ID, X.GTT_NAME AS TABLE_IDENT, NULL, NULL
             FROM   GAMING_TABLES_TYPES AS X
            WHERE   X.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
    END
   ELSE
    BEGIN
      -- LINK WITH TABLES
      INSERT INTO   @_DAYS_AND_TABLES
           SELECT   NULL, X.GT_GAMING_TABLE_ID, X.GT_NAME AS TABLE_IDENT, Z.GTT_GAMING_TABLE_TYPE_ID, Z.GTT_NAME
             FROM   GAMING_TABLES AS X
        LEFT JOIN   GAMING_TABLES_TYPES AS Z
               ON   Z.GTT_GAMING_TABLE_TYPE_ID = X.GT_TYPE_ID
            WHERE   Z.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
    END

END

-- SELECT INTO TEMPORARY TABLE WITH ALL DATA
-- SPECIFIC REPORT QUERY
  SELECT   X.TABLE_IDENTIFIER
         , X.TABLE_NAME
         , X.TABLE_TYPE
         , X.TABLE_TYPE_NAME
         , X.CM_DATE_ONLY
         , MIN(X.OPEN_HOUR) AS OPEN_HOUR
         , MAX(X.CLOSE_HOUR) AS CLOSE_HOUR
         , SUM(X.SESSION_HOURS) AS SESSION_HOURS
         , SUM(X.BUY_IN) AS BUY_IN
         , SUM(X.COPY_DEALER_VALIDATED_AMOUNT) AS COPY_DEALER_VALIDATED_AMOUNT
         , SUM(X.CHIPS_IN) AS CHIPS_IN
         , SUM(X.CHIPS_OUT) AS CHIPS_OUT
         , SUM(X.TOTAL_PLAYED) AS TOTAL_PLAYED
         , SUM(X.AVERAGE_BET) AS AVERAGE_BET
         , SUM(X.CHIPS_IN) + SUM(X.BUY_IN) AS TOTAL_DROP
         , CASE WHEN (SUM(X.CHIPS_IN) + SUM(X.BUY_IN)) =0
              THEN 0
              ELSE ((SUM(X.CHIPS_OUT)/(SUM(X.CHIPS_IN) + SUM(X.BUY_IN)))*100)
           END AS HOLD
         , MIN(X.THEORIC_HOLD) AS THEORIC_HOLD
         , CASE WHEN SUM(X.TOTAL_PLAYED) =0
              THEN 0
              ELSE ((SUM(X.NETWIN)/SUM(X.TOTAL_PLAYED)) * 100)
           END AS PAYOUT
         , SUM(X.NETWIN) AS NETWIN
         , X.ISO_CODE
   INTO #GT_TEMPORARY_REPORT_DATA

   FROM (
            -- CORE QUERY
            SELECT   CASE
                        WHEN @_TIME_INTERVAL = 0 THEN     -- TO FILTER BY DAY
                            DATEADD(dd, 0, DATEDIFF(dd, 0, CS_OPENING_DATE))
                        WHEN @_TIME_INTERVAL = 1 THEN     -- TO FILTER BY MONTH
                            CAST(CAST(YEAR(CS_OPENING_DATE) AS VARCHAR(4)) + RIGHT('0' + CAST(MONTH(CS_OPENING_DATE) AS VARCHAR(2)), 2) + '01' AS DATETIME)
                        WHEN @_TIME_INTERVAL = 2 THEN     -- TO FILTER BY YEAR
                            CAST(CAST(YEAR(CS_OPENING_DATE) AS VARCHAR(4)) + '01'+ '01' AS DATETIME)
                     END AS CM_DATE_ONLY
                   , (CASE WHEN @_BASE_TYPE = 0 THEN GTT_GAMING_TABLE_TYPE_ID ELSE GTPS_GAMING_TABLE_ID END)  AS TABLE_IDENTIFIER     -- GET THE BASE TYPE IDENTIFIER
                   , (CASE WHEN @_BASE_TYPE = 0 THEN GTT_NAME ELSE GT_NAME END) AS TABLE_NAME                                         -- GET THE BASE TYPE IDENTIFIER NAME
                   , (CASE WHEN @_BASE_TYPE = 0 THEN NULL ELSE GTT_GAMING_TABLE_TYPE_ID END) AS TABLE_TYPE                            -- TYPE
                   , (CASE WHEN @_BASE_TYPE = 0 THEN NULL ELSE GTT_NAME END) AS TABLE_TYPE_NAME                                       -- TYPE NAME
                   , SUM(GTPS_TOTAL_SELL_CHIPS) AS BUY_IN
                   , SUM(ISNULL(GTSC_COPY_DEALER_VALIDATED_AMOUNT, 0)) AS COPY_DEALER_VALIDATED_AMOUNT
                   , SUM(GTPS_CHIPS_IN) AS CHIPS_IN
                   , SUM(GTPS_CHIPS_OUT)  AS CHIPS_OUT
                   , SUM(GTPS_PLAYED_AMOUNT) AS TOTAL_PLAYED
                   , SUM(GTPS_PLAYED_AVERAGE)  AS AVERAGE_BET
                   , SUM(GTPS_NETWIN) AS NETWIN
                   , GTPS_ISO_CODE AS ISO_CODE
                   , GT_THEORIC_HOLD  AS THEORIC_HOLD
                   , CS_OPENING_DATE AS OPEN_HOUR
                   , CS_CLOSING_DATE AS CLOSE_HOUR
                   , DATEDIFF(MINUTE, CS_OPENING_DATE, ISNULL(CS_CLOSING_DATE, GETDATE())) AS SESSION_HOURS
              FROM   GT_PLAY_SESSIONS
             INNER   JOIN GAMING_TABLES ON GT_GAMING_TABLE_ID = GTPS_GAMING_TABLE_ID
             INNER   JOIN GAMING_TABLES_TYPES ON GT_TYPE_ID = GTT_GAMING_TABLE_TYPE_ID
             INNER   JOIN GAMING_TABLES_SESSIONS ON GTPS_GAMING_TABLE_SESSION_ID = GTS_GAMING_TABLE_SESSION_ID
              LEFT   JOIN GAMING_TABLES_SESSIONS_BY_CURRENCY ON GTPS_GAMING_TABLE_SESSION_ID = GTSC_GAMING_TABLE_SESSION_ID AND GTPS_ISO_CODE = GTSC_ISO_CODE 
             INNER   JOIN CASHIER_SESSIONS ON GTS_CASHIER_SESSION_ID = CS_SESSION_ID
               AND   CS_OPENING_DATE >= @pDateFrom
               AND   CS_OPENING_DATE < @pDateTo
             WHERE   CS_STATUS = 1
              AND    GT_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
              AND    GTPS_ISO_CODE IN (@pSelectedCurrency)
             GROUP   BY   GTPS_GAMING_TABLE_ID
                   , GT_NAME
                   , CS_OPENING_DATE
                   , CS_CLOSING_DATE
                   , GTT_GAMING_TABLE_TYPE_ID
                   , GTT_NAME
                   , GT_THEORIC_HOLD
                   , GTPS_ISO_CODE
             -- END CORE QUERY
        ) AS X
 GROUP BY  X.TABLE_IDENTIFIER
         , X.TABLE_NAME
         , X.TABLE_TYPE
         , X.TABLE_TYPE_NAME
         , X.CM_DATE_ONLY
         , X.ISO_CODE

  -- Group by indentifier and time interval

IF @_TIME_INTERVAL <> -1
BEGIN
   -- INTERVALS DATES AND TABLE FINAL PREPARATION

   -- FILTER ACTIVITY
   IF @_ONLY_ACTIVITY = 0
    BEGIN
      -- JOIN THE SELECT WITH ALL DATA
      SELECT   DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER,
               DT.TABLE_IDENT_NAME AS TABLE_NAME,
               DT.TABLE_TYPE_IDENT AS TABLE_TYPE,
               DT.TABLE_TYPE_NAME AS TABLE_TYPE_NAME,
               ISNULL(ZZ.BUY_IN, 0) AS BUY_IN,
               ISNULL(ZZ.COPY_DEALER_VALIDATED_AMOUNT, 0) AS COPY_DEALER_VALIDATED_AMOUNT,
               ISNULL(ZZ.CHIPS_IN, 0) AS CHIPS_IN,
               ISNULL(ZZ.CHIPS_OUT, 0) AS CHIPS_OUT,
               ISNULL(ZZ.TOTAL_PLAYED, 0) AS TOTAL_PLAYED,
               ISNULL(ZZ.AVERAGE_BET, 0) AS AVERAGE_BET,
               ISNULL(ZZ.TOTAL_DROP, 0) AS TOTAL_DROP,
               ISNULL(ZZ.HOLD, 0) AS HOLD,
               ISNULL(ZZ.THEORIC_HOLD, 0) AS THEORIC_HOLD,
               ISNULL(ZZ.PAYOUT, 0) AS PAYOUT,
               ISNULL(ZZ.NETWIN, 0) AS NETWIN,
               ISO_CODE,
               ZZ.OPEN_HOUR,
               ZZ.CLOSE_HOUR,
               CASE WHEN ZZ.CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED,
               CASE WHEN ZZ.SESSION_HOURS = 0 THEN DATEDIFF(MINUTE,ZZ.OPEN_HOUR,GETDATE()) ELSE ISNULL(ZZ.SESSION_HOURS, 0) END AS SESSION_HOURS,
               DATE_TIME
        FROM   @_DAYS_AND_TABLES DT
   LEFT JOIN   #GT_TEMPORARY_REPORT_DATA ZZ
               ON (
                  (@_TIME_INTERVAL = 0 AND DATE_TIME = ZZ.CM_DATE_ONLY)
                  OR (@_TIME_INTERVAL = 1 AND MONTH(DATE_TIME) = MONTH(ZZ.CM_DATE_ONLY) AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
                  OR (@_TIME_INTERVAL = 2 AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
                )
         AND   TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER
       -- SET ORDER
    ORDER BY   DT.TABLE_TYPE_IDENT ASC,
               CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC,
               DATE_TIME DESC;

    END
   ELSE
    BEGIN
      -- JOIN THE SELECT HIDDING WITHOUT ACTIVITY
      SELECT  DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER,
              DT.TABLE_IDENT_NAME AS TABLE_NAME,
              DT.TABLE_TYPE_IDENT AS TABLE_TYPE,
              DT.TABLE_TYPE_NAME AS TABLE_TYPE_NAME,
              ISNULL(ZZ.BUY_IN, 0) AS BUY_IN,
              ISNULL(ZZ.COPY_DEALER_VALIDATED_AMOUNT, 0) AS COPY_DEALER_VALIDATED_AMOUNT, 
              ISNULL(ZZ.CHIPS_IN, 0) AS CHIPS_IN,
              ISNULL(ZZ.CHIPS_OUT, 0) AS CHIPS_OUT,
              ISNULL(ZZ.TOTAL_PLAYED, 0) AS TOTAL_PLAYED,
              ISNULL(ZZ.AVERAGE_BET, 0) AS AVERAGE_BET,
              ISNULL(ZZ.TOTAL_DROP, 0) AS TOTAL_DROP,
              ISNULL(ZZ.HOLD, 0) AS HOLD,
              ISNULL(ZZ.THEORIC_HOLD, 0) AS THEORIC_HOLD,
              ISNULL(ZZ.PAYOUT, 0) AS PAYOUT,
              ISNULL(ZZ.NETWIN, 0) AS NETWIN,
              ISO_CODE,
              ZZ.OPEN_HOUR,
              ZZ.CLOSE_HOUR,
              CASE WHEN ZZ.CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED,
              CASE WHEN ZZ.SESSION_HOURS = 0 THEN DATEDIFF(MINUTE,ZZ.OPEN_HOUR,GETDATE()) ELSE ISNULL(ZZ.SESSION_HOURS, 0) END AS SESSION_HOURS,
              DATE_TIME
        FROM  @_DAYS_AND_TABLES DT
  INNER JOIN  #GT_TEMPORARY_REPORT_DATA ZZ
              ON(
                 (@_TIME_INTERVAL = 0 AND DATE_TIME = ZZ.CM_DATE_ONLY)
                 OR (@_TIME_INTERVAL = 1 AND MONTH(DATE_TIME) = MONTH(ZZ.CM_DATE_ONLY) AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
                 OR (@_TIME_INTERVAL = 2 AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
              )
              AND   TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER
       -- SET ORDER
   ORDER BY   DT.TABLE_TYPE_IDENT ASC,
              CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC,
              DATE_TIME DESC;

    END -- IF ONLY_ACTIVITY

END
ELSE  -- ELSE WITHOUT INTERVALS
BEGIN

   -- FINAL WITHOUT INTERVALS

      -- FILTER ACTIVITY
   IF @_ONLY_ACTIVITY = 0
    BEGIN
            -- JOIN DATA INCLUDING WITHOUT ACTIVITY
            SELECT   DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER,
                     DT.TABLE_IDENT_NAME AS TABLE_NAME,
                     DT.TABLE_TYPE_IDENT AS TABLE_TYPE,
                     DT.TABLE_TYPE_NAME,
                     ISNULL(ZZ.BUY_IN, 0) AS BUY_IN,
                     ISNULL(ZZ.COPY_DEALER_VALIDATED_AMOUNT, 0) AS COPY_DEALER_VALIDATED_AMOUNT, 
                     ISNULL(ZZ.CHIPS_IN, 0) AS CHIPS_IN,
                     ISNULL(ZZ.CHIPS_OUT, 0) AS CHIPS_OUT,
                     ISNULL(ZZ.TOTAL_PLAYED, 0) AS TOTAL_PLAYED,
                     ISNULL(ZZ.AVERAGE_BET, 0) AS AVERAGE_BET,
                     ISNULL(ZZ.TOTAL_DROP, 0) AS TOTAL_DROP,
                     ISNULL(ZZ.HOLD, 0) AS HOLD,
                     ISNULL(ZZ.THEORIC_HOLD, 0) AS THEORIC_HOLD,
                     ISNULL(ZZ.PAYOUT, 0) AS PAYOUT,
                     ISNULL(ZZ.NETWIN, 0) AS NETWIN,
                     ISO_CODE,
                     OPEN_HOUR,
                     CLOSE_HOUR,
                     CASE WHEN CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED,
                     CASE WHEN SESSION_HOURS = 0 THEN DATEDIFF(MINUTE, OPEN_HOUR, GETDATE()) ELSE ISNULL(SESSION_HOURS, 0) END AS SESSION_HOURS
              FROM   @_DAYS_AND_TABLES DT
         LEFT JOIN   #GT_TEMPORARY_REPORT_DATA ZZ
                ON   DT.TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER
          ORDER BY   DT.TABLE_TYPE_IDENT ASC,
                     CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC,
                     DATE_TIME DESC;

     END
    ELSE
     BEGIN
            -- JOIN DATA WITH ONLY ACTIVITY
            SELECT   DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER,
                     DT.TABLE_IDENT_NAME AS TABLE_NAME,
                     DT.TABLE_TYPE_IDENT AS TABLE_TYPE,
                     DT.TABLE_TYPE_NAME,
                     ISNULL(ZZ.BUY_IN, 0) AS BUY_IN,
                     ISNULL(ZZ.COPY_DEALER_VALIDATED_AMOUNT, 0) AS COPY_DEALER_VALIDATED_AMOUNT,
                     ISNULL(ZZ.CHIPS_IN, 0) AS CHIPS_IN,
                     ISNULL(ZZ.CHIPS_OUT, 0) AS CHIPS_OUT,
                     ISNULL(ZZ.TOTAL_PLAYED, 0) AS TOTAL_PLAYED,
                     ISNULL(ZZ.AVERAGE_BET, 0) AS AVERAGE_BET,
                     ISNULL(ZZ.TOTAL_DROP, 0) AS TOTAL_DROP,
                     ISNULL(ZZ.HOLD, 0) AS HOLD,
                     ISNULL(ZZ.THEORIC_HOLD, 0) AS THEORIC_HOLD,
                     ISNULL(ZZ.PAYOUT, 0) AS PAYOUT,
                     ISNULL(ZZ.NETWIN, 0) AS NETWIN,
                     ISO_CODE,
                     OPEN_HOUR,
                     CLOSE_HOUR,
                     CASE WHEN CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED,
                     CASE WHEN SESSION_HOURS = 0 THEN DATEDIFF(MINUTE, OPEN_HOUR, GETDATE()) ELSE ISNULL(SESSION_HOURS, 0) END AS SESSION_HOURS
              FROM   @_DAYS_AND_TABLES DT
        INNER JOIN   #GT_TEMPORARY_REPORT_DATA ZZ
                ON   DT.TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER
          ORDER BY   DT.TABLE_TYPE_IDENT ASC,
                     CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC,
                     DATE_TIME DESC;

     END
 END

-- ERASE THE TEMPORARY DATA
DROP TABLE #GT_TEMPORARY_REPORT_DATA

END -- END PROCEDURE
GO

-- PERMISSIONS
GRANT EXECUTE ON [dbo].[GT_Base_Report_Data_Player_Tracking] TO [wggui] WITH GRANT OPTION
GO


IF OBJECT_ID (N'dbo.GT_Base_Report_Data', N'P') IS NOT NULL
    DROP PROCEDURE dbo.GT_Base_Report_Data;
GO

CREATE PROCEDURE [dbo].[GT_Base_Report_Data]
(
      @pBaseType                   INTEGER
    , @pTimeInterval               INTEGER
    , @pDateFrom                   DATETIME
    , @pDateTo                     DATETIME
    , @pOnlyActivity               INTEGER
    , @pOrderBy                    INTEGER
    , @pValidTypes                 VARCHAR(4096)
    , @pSelectedCurrency           NVARCHAR(100)
    , @pTotalToSelectedCurrency    INTEGER
)
AS
BEGIN

-- DECLARATIONS
   -- @_DAYS_AND_TABLES:    A temporary table with all intervals and all base type selected (used to show base types without activity).
   DECLARE @_DAYS_AND_TABLES TABLE(DATE_TIME DATETIME, TABLE_IDENTIFIER_VALUE BIGINT, TABLE_IDENT_NAME VARCHAR(50), TABLE_TYPE_IDENT BIGINT, TABLE_TYPE_NAME VARCHAR(50))
   -- @_DAY_VAR:            Variable to create the intervals
   DECLARE @_DAY_VAR DATETIME

-- PARAMETERS
     DECLARE @_BASE_TYPE                  AS   INTEGER -- 0 BY TABLE TYPE, 1 BY TABLE
     DECLARE @_TIME_INTERVAL              AS   INTEGER -- -1, 0 DAYS, 1 MONTH, 2 YEAR
     DECLARE @_DATE_FROM                  AS   DATETIME
     DECLARE @_DATE_TO                    AS   DATETIME
     DECLARE @_ONLY_ACTIVITY              AS   INTEGER -- 0 SHOW ALL, 1 SHOW ONLY WITH ACTIVITY
     DECLARE @_ORDER_BY                   AS   INTEGER -- 0 BY INTERVAL DESC, 1 BY IDENTIFIER ASC
     DECLARE @_DELIMITER                  AS   CHAR(1)
     DECLARE @_TYPES_TABLE                TABLE(SST_ID INT, SST_VALUE VARCHAR(50)) -- TO STORE THE VALID GAMING TABLE TYPES
     DECLARE @_SELECTED_CURRENCY          AS   NVARCHAR(3)
     DECLARE @_TOTAL_TO_SELECTED_CURRENCY AS   INTEGER
     DECLARE @_CHIP_RE                    AS   INTEGER
     DECLARE @_CHIP_NRE                   AS   INTEGER
     DECLARE @_CHIP_COLOR                 AS   INTEGER
----------------------------------------------------------------------------------------------------------------

-- Initialization --
  SET @_BASE_TYPE                  =   @pBaseType
  SET @_TIME_INTERVAL              =   @pTimeInterval
  SET @_DATE_FROM                  =   @pDateFrom
  SET @_DATE_TO                    =   @pDateTo
  SET @_ONLY_ACTIVITY              =   @pOnlyActivity
  SET @_ORDER_BY                   =   @pOrderBy
  SET @_DELIMITER                  =   ','
  SET @_SELECTED_CURRENCY          =   @pSelectedCurrency
  SET @_TOTAL_TO_SELECTED_CURRENCY =   @pTotalToSelectedCurrency
  SET @_CHIP_RE                    =   1001
  SET @_CHIP_NRE                   =   1002
  SET @_CHIP_COLOR                 =   1003
----------------------------------------------------------------------------------------------------------------

-- CHECK DATE PARAMETERS
IF @_DATE_FROM IS NULL
BEGIN
   -- IF DATE FROM IS NULL, USER FIRST POSIBLE DATE
   SET @_DATE_FROM = CAST('' AS DATETIME)
   SET @pDateFrom  = CAST('' AS DATETIME)
END

IF @_DATE_TO IS NULL
BEGIN
   -- IF DATE TO IS NULL, QUERY UNTIL CURRENT DATE
   SET @_DATE_TO = CAST(GETDATE() AS DATETIME)
   SET @pDateTo  = CAST(GETDATE() AS DATETIME)
END

-- ASSIGN TYPES PARAMETER INTO TABLE
INSERT INTO @_TYPES_TABLE SELECT * FROM dbo.SplitStringIntoTable(@pValidTypes, @_DELIMITER, DEFAULT)

IF @_TIME_INTERVAL <> -1
 BEGIN
   -- INTERVALS DATES AND TABLE PREPARATION

   -- PREPARE DATES RANGE DEPENDING ON TIME INTERVAL
   SET @_DATE_FROM = CASE
                        WHEN @_TIME_INTERVAL = 0 THEN -- BY DAY
                           DATEADD(dd, 0, DATEDIFF(dd, 0, @_DATE_FROM))
                        WHEN @_TIME_INTERVAL = 1 THEN -- BY MONTH
                           CAST(CAST(YEAR(@_DATE_FROM) AS VARCHAR(4)) + RIGHT('0' + CAST(MONTH(@_DATE_FROM) AS VARCHAR(2)), 2) + '01' AS DATETIME)
                        WHEN @_TIME_INTERVAL = 2 THEN -- BY YEAR
                           CAST(CAST(YEAR(@_DATE_FROM) AS VARCHAR(4)) + '0101' AS DATETIME)
                     END

   SET @_DATE_TO = CASE
          WHEN @_TIME_INTERVAL = 0 THEN -- BY DAY
                           DATEADD(DAY, 1, CAST(@_DATE_TO AS DATETIME))
                        WHEN @_TIME_INTERVAL = 1 THEN -- BY MONTH
                           DATEADD(MONTH, 1, CAST(CAST(YEAR(@_DATE_TO) AS VARCHAR(4)) + RIGHT('0' + CAST(MONTH(@_DATE_TO) AS VARCHAR(2)), 2) + '01' AS DATETIME))
                        WHEN @_TIME_INTERVAL = 2 THEN -- BY YEAR
                           DATEADD(YEAR, 1, CAST(CAST(YEAR(@_DATE_TO) AS VARCHAR(4)) + '0101' AS DATETIME))
                     END

   -- PREPARE THE TIME INTERVALS TABLE
   SET @_DAY_VAR = @_DATE_FROM
   WHILE @_DAY_VAR < @_DATE_TO
   BEGIN
         -- SET THE LINK FIELD FOR THE RESULTS
         IF @_BASE_TYPE = 0
          BEGIN
            -- LINK WITH TABLE TYPES
            INSERT INTO @_DAYS_AND_TABLES
                 SELECT   CAST(@_DAY_VAR AS DATETIME), X.GTT_GAMING_TABLE_TYPE_ID, X.GTT_NAME AS TABLE_IDENT, NULL, NULL
                   FROM   GAMING_TABLES_TYPES AS X
                  WHERE   X.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
                          AND     X.GTT_ENABLED = 1
          END
         ELSE
          BEGIN
            -- LINK WITH TABLES
            INSERT INTO @_DAYS_AND_TABLES
                 SELECT   CAST(@_DAY_VAR AS DATETIME), X.GT_GAMING_TABLE_ID, X.GT_NAME AS TABLE_IDENT, Z.GTT_GAMING_TABLE_TYPE_ID, Z.GTT_NAME
                   FROM   GAMING_TABLES AS X
              LEFT JOIN   GAMING_TABLES_TYPES AS Z
                     ON   Z.GTT_GAMING_TABLE_TYPE_ID = X.GT_TYPE_ID
                  WHERE   Z.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
                          AND X.GT_ENABLED = 1
          END

          -- SET INCREMENT
          SET @_DAY_VAR = CASE
                               WHEN @_TIME_INTERVAL = 0 THEN DATEADD(DAY,1,@_DAY_VAR)     -- DAY
                               WHEN @_TIME_INTERVAL = 1 THEN DATEADD(MONTH,1,@_DAY_VAR)   -- MONTH
                               WHEN @_TIME_INTERVAL = 2 THEN DATEADD(YEAR,1,@_DAY_VAR)    -- YEAR
                          END
   END

END -- IF INTERVALS <> - 1
ELSE
BEGIN

   -- SET THE LINK FIELD FOR THE RESULTS WITHOUT INTERVALS
   IF @_BASE_TYPE = 0
    BEGIN
      -- LINK WITH TABLE TYPES
      INSERT INTO @_DAYS_AND_TABLES
           SELECT   NULL, X.GTT_GAMING_TABLE_TYPE_ID, X.GTT_NAME AS TABLE_IDENT, NULL, NULL
             FROM   GAMING_TABLES_TYPES AS X
            WHERE   X.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
                    AND X.GTT_ENABLED = 1
    END
   ELSE
    BEGIN
      -- LINK WITH TABLES
      INSERT INTO @_DAYS_AND_TABLES
           SELECT   NULL, X.GT_GAMING_TABLE_ID, X.GT_NAME AS TABLE_IDENT, Z.GTT_GAMING_TABLE_TYPE_ID, Z.GTT_NAME
             FROM   GAMING_TABLES AS X
        LEFT JOIN   GAMING_TABLES_TYPES AS Z
               ON   Z.GTT_GAMING_TABLE_TYPE_ID = X.GT_TYPE_ID
            WHERE   Z.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
                    AND X.GT_ENABLED = 1
    END

 END


-- SELECT INTO TEMPORARY TABLE WITH ALL DATA
-- SPECIFIC REPORT QUERY
  SELECT   X.TABLE_IDENTIFIER
         , X.TABLE_NAME
         , X.TABLE_TYPE
         , X.TABLE_TYPE_NAME
         , SUM(X.COPY_DEALER_VALIDATED_AMOUNT) AS COPY_DEALER_VALIDATED_AMOUNT         
         , SUM(X.S_DROP_GAMBLING_TABLE) + SUM (X.S_DROP_CASHIER) TOTAL_DROP
         , SUM(X.S_DROP_GAMBLING_TABLE) TOTAL_DROP_GAMBLING_TABLE
         , SUM(X.S_DROP_CASHIER) TOTAL_DROP_CASHIER
         , SUM(X.S_WIN) TOTAL_WIN
         , ISNULL(SUM(X.S_TIP),0)  TOTAL_TIP
         , CASE WHEN SUM(X.S_DROP_GAMBLING_TABLE) + SUM (X.S_DROP_CASHIER) = 0 THEN 0 ELSE CAST(SUM(X.S_WIN) AS DECIMAL(18,2)) / CAST(SUM(X.S_DROP_GAMBLING_TABLE) + SUM (X.S_DROP_CASHIER) AS DECIMAL(18,2)) * 100 END AS WIN_DROP
         , MIN(X.THEORIC_HOLD) AS THEORIC_HOLD
         , CASE WHEN SUM(X.S_DROP_GAMBLING_TABLE) + SUM (X.S_DROP_CASHIER) = 0 THEN 0 ELSE SUM(CAST(X.S_TIP AS DECIMAL(18,2))) / CAST(SUM(X.S_DROP_GAMBLING_TABLE) + SUM (X.S_DROP_CASHIER) AS DECIMAL(18,2)) * 100 END AS TIP_DROP
         , SUM(X.S_WIN) + SUM(X.S_TIP) AS WIN_TIPS
         , X.CM_DATE_ONLY
         , MIN(X.OPEN_HOUR) AS OPEN_HOUR
         , MAX(X.CLOSE_HOUR) AS CLOSE_HOUR
         , SUM(X.SESSION_SECONDS) AS SESSION_SECONDS
         , COUNT(X.SESSION_SECONDS) AS SESSION_SUM
    INTO   #GT_TEMPORARY_REPORT_DATA
    FROM (
         -- CORE QUERY
         SELECT CASE WHEN @_TIME_INTERVAL = 0      -- TO FILTER BY DAY
                     THEN DATEADD(dd, 0, DATEDIFF(dd, 0, CS_OPENING_DATE))
                     WHEN @_TIME_INTERVAL = 1      -- TO FILTER BY MONTH
                     THEN CAST(CAST(YEAR(CS_OPENING_DATE) AS VARCHAR(4)) + RIGHT('0' + CAST(MONTH(CS_OPENING_DATE) AS VARCHAR(2)), 2) + '01' AS DATETIME)
                     WHEN @_TIME_INTERVAL = 2      -- TO FILTER BY YEAR
                     THEN CAST(CAST(YEAR(CS_OPENING_DATE) AS VARCHAR(4)) + '01'+ '01' AS DATETIME)
                     END                                                                                       AS CM_DATE_ONLY
                , (CASE WHEN @_BASE_TYPE = 0 THEN GTT_GAMING_TABLE_TYPE_ID ELSE GT.GT_GAMING_TABLE_ID END)   AS TABLE_IDENTIFIER -- GET THE BASE TYPE IDENTIFIER
                , (CASE WHEN @_BASE_TYPE = 0 THEN GTT_NAME ELSE GT.GT_NAME END)                              AS TABLE_NAME       -- GET THE BASE TYPE IDENTIFIER NAME
                , (CASE WHEN @_BASE_TYPE = 0 THEN NULL ELSE GTT_GAMING_TABLE_TYPE_ID END)                    AS TABLE_TYPE       -- TYPE
                , (CASE WHEN @_BASE_TYPE = 0 THEN NULL ELSE GTT_NAME END)                                    AS TABLE_TYPE_NAME  -- TYPE NAME

                ,  CASE WHEN @_TOTAL_TO_SELECTED_CURRENCY = 0 
                        THEN DBO.GT_CALCULATE_DROP_GAMBLING_TABLES(ISNULL(GTS_COLLECTED_AMOUNT                , 0)
                                                                 , ISNULL(GTS_COLLECTED_DROPBOX_AMOUNT        , 0)
                                                                 , ISNULL(GTS_COLLECTED_DROPBOX_CHIPS_AMOUNT  , 0)
                                                                 , 0 -- It does not allow takings of dropbox with tickets
                                                                 , GT_HAS_INTEGRATED_CASHIER)
                        WHEN @_TOTAL_TO_SELECTED_CURRENCY = 1 
                        THEN DBO.GT_CALCULATE_DROP_GAMBLING_TABLES(SUM(ISNULL(GTSC_COLLECTED_AMOUNT                , 0))
                                                                 , SUM(ISNULL(CASE WHEN GTSC_TYPE = 0            THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                                   WHEN GTSC_TYPE = 1            THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                                   ELSE 0 END,  0))
                                                                 , SUM(ISNULL(CASE WHEN GTSC_TYPE = @_CHIP_RE    THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                                   WHEN GTSC_TYPE = @_CHIP_NRE   THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                                   WHEN GTSC_TYPE = @_CHIP_COLOR THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                                   ELSE 0 END,  0))
                                                                 , 0 -- It does not allow takings of dropbox with tickets
                                                                 , GT_HAS_INTEGRATED_CASHIER)
                        END                                                                                  AS S_DROP_GAMBLING_TABLE

                ,  CASE WHEN @_TOTAL_TO_SELECTED_CURRENCY = 0 
                        THEN DBO.GT_CALCULATE_DROP_CASHIER(ISNULL(GTS_OWN_SALES_AMOUNT, 0)
                                                         , ISNULL(GTS_EXTERNAL_SALES_AMOUNT, 0))
                        WHEN @_TOTAL_TO_SELECTED_CURRENCY = 1 
                        THEN DBO.GT_CALCULATE_DROP_CASHIER(SUM(ISNULL(GTSC_OWN_SALES_AMOUNT, 0))
                                                         , SUM(ISNULL(GTSC_EXTERNAL_SALES_AMOUNT, 0)))
                        END                                                                              AS S_DROP_CASHIER

                ,  GT_THEORIC_HOLD AS THEORIC_HOLD
                ,  CASE WHEN @_TOTAL_TO_SELECTED_CURRENCY = 0 
                        THEN DBO.GT_CALCULATE_WIN(ISNULL(GTS_FINAL_CHIPS_AMOUNT      , 0)
                                                , ISNULL(GTS_INITIAL_CHIPS_AMOUNT    , 0)
                                                , ISNULL(GTS_FILLS_CHIPS_AMOUNT      , 0)
                                                , ISNULL(GTS_CREDITS_CHIPS_AMOUNT    , 0)                                                        
                                                , ISNULL(GTS_TIPS                    , 0)
                                                , ISNULL(GTS_OWN_SALES_AMOUNT        , 0)
                                                , ISNULL(GTS_EXTERNAL_SALES_AMOUNT   , 0)
                                                , ISNULL(GTS_COLLECTED_AMOUNT        , 0)
                                                , ISNULL(GTS_COLLECTED_DROPBOX_AMOUNT        , 0)
                                                , ISNULL(GTS_COLLECTED_DROPBOX_CHIPS_AMOUNT  , 0)
                                                , 0 -- It does not allow takings of dropbox with tickets)
                                                , GT_HAS_INTEGRATED_CASHIER)
                        WHEN @_TOTAL_TO_SELECTED_CURRENCY = 1 
                        THEN DBO.GT_CALCULATE_WIN(SUM(ISNULL(GTSC_FINAL_CHIPS_AMOUNT   , 0))
                                                , SUM(ISNULL(GTSC_INITIAL_CHIPS_AMOUNT , 0))
                                                , SUM(ISNULL(GTSC_FILLS_CHIPS_AMOUNT   , 0))
                                                , SUM(ISNULL(GTSC_CREDITS_CHIPS_AMOUNT , 0))
                                                , SUM(ISNULL(GTSC_TIPS                 , 0))
                                                , SUM(ISNULL(GTSC_OWN_SALES_AMOUNT     , 0))
                                                , SUM(ISNULL(GTSC_EXTERNAL_SALES_AMOUNT, 0))
                                                , SUM(ISNULL(GTSC_COLLECTED_AMOUNT     , 0))
                                                , SUM(ISNULL(CASE WHEN GTSC_TYPE = 0            THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                  WHEN GTSC_TYPE = 1            THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                  ELSE 0 END,  0))
                                                , SUM(ISNULL(CASE WHEN GTSC_TYPE = @_CHIP_RE    THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                  WHEN GTSC_TYPE = @_CHIP_NRE   THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                  WHEN GTSC_TYPE = @_CHIP_COLOR THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                  ELSE 0 END,  0))
                                                , 0 -- It does not allow takings of dropbox with tickets
                                                , GT_HAS_INTEGRATED_CASHIER)
                        END AS S_WIN

                     ,  CASE WHEN @_TOTAL_TO_SELECTED_CURRENCY = 0 THEN ISNULL(GTS_TIPS, 0)
                             WHEN @_TOTAL_TO_SELECTED_CURRENCY = 1 THEN ISNULL(GTSC_TIPS, 0)
                             END                                                                       AS S_TIP
                     , CS.CS_OPENING_DATE                                                              AS OPEN_HOUR
                     , CS.CS_CLOSING_DATE                                                              AS CLOSE_HOUR
                     , DATEDIFF(SECOND, CS_OPENING_DATE, ISNULL(CS_CLOSING_DATE, GETDATE()))           AS SESSION_SECONDS
                     , SUM(ISNULL(GTSC_COPY_DEALER_VALIDATED_AMOUNT, 0))                               AS COPY_DEALER_VALIDATED_AMOUNT
                FROM   GAMING_TABLES_SESSIONS GTS
                LEFT   JOIN GAMING_TABLES_SESSIONS_BY_CURRENCY GTSC ON GTSC.GTSC_GAMING_TABLE_SESSION_ID = GTS.GTS_GAMING_TABLE_SESSION_ID
                                                                   AND GTSC_ISO_CODE IN (@_SELECTED_CURRENCY)
               INNER   JOIN CASHIER_SESSIONS CS  ON GTS.GTS_CASHIER_SESSION_ID = CS.CS_SESSION_ID
                                                AND CS_OPENING_DATE >= @pDateFrom AND CS_OPENING_DATE < @pDateTo
               INNER   JOIN GAMING_TABLES GT ON GTS.GTS_GAMING_TABLE_ID = GT.GT_GAMING_TABLE_ID
                                            AND GT.GT_TYPE_ID IN (SELECT SST_VALUE FROM @_TYPES_TABLE)
               INNER   JOIN GAMING_TABLES_TYPES GTT ON GT.GT_TYPE_ID = GTT.GTT_GAMING_TABLE_TYPE_ID
               WHERE   CS_STATUS = 1  -- Only closed sessions
               GROUP BY CS_OPENING_DATE, GTT_GAMING_TABLE_TYPE_ID, GT_GAMING_TABLE_ID, GTT_NAME, GT_NAME
                      , GTS_OWN_SALES_AMOUNT, GTS_EXTERNAL_SALES_AMOUNT, GTS_COLLECTED_AMOUNT, GT_HAS_INTEGRATED_CASHIER
                      , GTS_COLLECTED_DROPBOX_AMOUNT, GTS_COLLECTED_DROPBOX_AMOUNT, GTS_DROPBOX_ENABLED
                      , GTS_COLLECTED_DROPBOX_CHIPS_AMOUNT, GT_THEORIC_HOLD, GTS_FINAL_CHIPS_AMOUNT, GTS_INITIAL_CHIPS_AMOUNT
                      , GTS_FILLS_CHIPS_AMOUNT, GTS_CREDITS_CHIPS_AMOUNT, GTS_TIPS, GTSC_TIPS, CS_CLOSING_DATE
          -- END CORE QUERY
        ) AS X
  GROUP BY   X.TABLE_TYPE, X.TABLE_IDENTIFIER, X.TABLE_TYPE_NAME, X.TABLE_NAME, X.CM_DATE_ONLY  -- Group by indentifier and time interval

IF @_TIME_INTERVAL <> -1
BEGIN
  -- INTERVALS DATES AND TABLE FINAL PREPARATION
  -- FILTER ACTIVITY
  IF @_ONLY_ACTIVITY = 0
    BEGIN
      -- JOIN THE SELECT WITH ALL DATA
      SELECT   DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER
             , DT.TABLE_IDENT_NAME AS TABLE_NAME
             , DT.TABLE_TYPE_IDENT AS TABLE_TYPE
             , DT.TABLE_TYPE_NAME AS TABLE_TYPE_NAME
             , ISNULL(zz.COPY_DEALER_VALIDATED_AMOUNT, 0) AS COPY_DEALER_AMOUNT
             , ISNULL(ZZ.TOTAL_DROP_GAMBLING_TABLE,0) AS TOTAL_DROP_GAMBLING_TABLE
             , ISNULL(ZZ.TOTAL_DROP_CASHIER,0) AS  TOTAL_DROP_CASHIER
             , ISNULL(ZZ.TOTAL_DROP, 0) AS TOTAL_DROP
             , ISNULL(ZZ.TOTAL_WIN, 0) AS TOTAL_WIN
             , ISNULL(ZZ.TOTAL_TIP, 0) AS TOTAL_TIP
             , ISNULL(ZZ.WIN_DROP, 0) AS WIN_DROP
             , ISNULL(ZZ.THEORIC_HOLD, 0) AS THEORIC_HOLD
             , ISNULL(ZZ.TIP_DROP, 0) AS TIP_DROP
             , ISNULL(ZZ.WIN_TIPS, 0) AS WIN_TIPS
             , ZZ.OPEN_HOUR
             , ZZ.CLOSE_HOUR
             , CASE WHEN ZZ.CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED
             , CASE WHEN ZZ.SESSION_SECONDS = 0 THEN DATEDIFF(SECOND,ZZ.OPEN_HOUR,GETDATE()) ELSE ISNULL(ZZ.SESSION_SECONDS, 0) END AS SESSION_SECONDS
             , ISNULL(ZZ.SESSION_SUM, 0) AS SESSION_SUM
             , DATE_TIME              
        FROM   @_DAYS_AND_TABLES DT
   LEFT JOIN   #GT_TEMPORARY_REPORT_DATA ZZ
          ON   (  (@_TIME_INTERVAL = 0 AND DATE_TIME = ZZ.CM_DATE_ONLY)
               OR (@_TIME_INTERVAL = 1 AND MONTH(DATE_TIME) = MONTH(ZZ.CM_DATE_ONLY) AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
               OR (@_TIME_INTERVAL = 2 AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
               ) AND TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER
       -- SET ORDER
    ORDER BY   DT.TABLE_TYPE_IDENT ASC
             , CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC
             , DATE_TIME DESC;

    END
  ELSE
    BEGIN
      -- JOIN THE SELECT HIDDING WITHOUT ACTIVITY
      SELECT   DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER
             , DT.TABLE_IDENT_NAME AS TABLE_NAME
             , DT.TABLE_TYPE_IDENT AS TABLE_TYPE
             , DT.TABLE_TYPE_NAME AS TABLE_TYPE_NAME
             , ISNULL(zz.COPY_DEALER_VALIDATED_AMOUNT, 0) AS COPY_DEALER_AMOUNT
             , ISNULL(ZZ.TOTAL_DROP_GAMBLING_TABLE,0) AS TOTAL_DROP_GAMBLING_TABLE 
             , ISNULL(ZZ.TOTAL_DROP_CASHIER,0) AS  TOTAL_DROP_CASHIER
             , ISNULL(ZZ.TOTAL_DROP, 0) AS TOTAL_DROP 
             , ISNULL(ZZ.TOTAL_WIN, 0) AS TOTAL_WIN
             , ISNULL(ZZ.TOTAL_TIP, 0) AS TOTAL_TIP
             , ISNULL(ZZ.WIN_DROP, 0) AS WIN_DROP
             , ISNULL(ZZ.THEORIC_HOLD, 0) AS THEORIC_HOLD
             , ISNULL(ZZ.TIP_DROP, 0) AS TIP_DROP
             , ISNULL(ZZ.WIN_TIPS, 0) AS WIN_TIPS
             , ZZ.OPEN_HOUR
             , ZZ.CLOSE_HOUR
             , CASE WHEN ZZ.CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED
             , CASE WHEN ZZ.SESSION_SECONDS = 0 THEN DATEDIFF(SECOND,ZZ.OPEN_HOUR,GETDATE()) ELSE ISNULL(ZZ.SESSION_SECONDS, 0) END AS SESSION_SECONDS
             , ISNULL(ZZ.SESSION_SUM, 0) AS SESSION_SUM
             , DATE_TIME
        FROM   @_DAYS_AND_TABLES DT
  INNER JOIN   #GT_TEMPORARY_REPORT_DATA ZZ
          ON   (  (@_TIME_INTERVAL = 0 AND DATE_TIME = ZZ.CM_DATE_ONLY)
               OR (@_TIME_INTERVAL = 1 AND MONTH(DATE_TIME) = MONTH(ZZ.CM_DATE_ONLY) AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
               OR (@_TIME_INTERVAL = 2 AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
               ) AND TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER
       -- SET ORDER
    ORDER BY   DT.TABLE_TYPE_IDENT ASC
             , CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC
             , DATE_TIME DESC;
    END -- IF ONLY_ACTIVITY
  END
ELSE  -- ELSE WITHOUT INTERVALS
  BEGIN
   -- FINAL WITHOUT INTERVALS
      -- FILTER ACTIVITY
   IF @_ONLY_ACTIVITY = 0
     BEGIN
            -- JOIN DATA INCLUDING WITHOUT ACTIVITY
      SELECT   DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER
             , DT.TABLE_IDENT_NAME AS TABLE_NAME
             , DT.TABLE_TYPE_IDENT AS TABLE_TYPE
             , DT.TABLE_TYPE_NAME
             , ISNULL(zz.COPY_DEALER_VALIDATED_AMOUNT, 0) AS COPY_DEALER_AMOUNT
             , ISNULL(ZZ.TOTAL_DROP_GAMBLING_TABLE,0) AS TOTAL_DROP_GAMBLING_TABLE
             , ISNULL(ZZ.TOTAL_DROP_CASHIER,0) AS  TOTAL_DROP_CASHIER
             , ISNULL(TOTAL_DROP, 0) AS TOTAL_DROP
             , ISNULL(TOTAL_WIN, 0) AS TOTAL_WIN
             , ISNULL(TOTAL_TIP, 0) AS TOTAL_TIP
             , ISNULL(WIN_DROP, 0) AS WIN_DROP
             , ISNULL(ZZ.THEORIC_HOLD, 0) AS THEORIC_HOLD
             , ISNULL(TIP_DROP, 0) AS TIP_DROP
             , ISNULL(WIN_TIPS, 0) AS WIN_TIPS
             , OPEN_HOUR
             , CLOSE_HOUR
             , CASE WHEN CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED
             , CASE WHEN SESSION_SECONDS = 0 THEN DATEDIFF(SECOND, OPEN_HOUR, GETDATE()) ELSE ISNULL(SESSION_SECONDS, 0) END AS SESSION_SECONDS
             , ISNULL(SESSION_SUM, 0) AS SESSION_SUM
        FROM   @_DAYS_AND_TABLES DT
   LEFT JOIN   #GT_TEMPORARY_REPORT_DATA ZZ ON DT.TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER
    ORDER BY   DT.TABLE_TYPE_IDENT ASC
             , CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC
             , DATE_TIME DESC;

     END
   ELSE
     BEGIN
       -- JOIN DATA WITH ONLY ACTIVITY
       SELECT   DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER
              , DT.TABLE_IDENT_NAME AS TABLE_NAME
              , DT.TABLE_TYPE_IDENT AS TABLE_TYPE
              , DT.TABLE_TYPE_NAME
              , ISNULL(zz.COPY_DEALER_VALIDATED_AMOUNT, 0) AS COPY_DEALER_AMOUNT
              , ISNULL(ZZ.TOTAL_DROP_GAMBLING_TABLE,0) AS TOTAL_DROP_GAMBLING_TABLE
              , ISNULL(ZZ.TOTAL_DROP_CASHIER,0) AS  TOTAL_DROP_CASHIER
              , ISNULL(TOTAL_DROP, 0) AS TOTAL_DROP
              , ISNULL(TOTAL_WIN, 0) AS TOTAL_WIN
              , ISNULL(TOTAL_TIP, 0) AS TOTAL_TIP
              , ISNULL(WIN_DROP, 0) AS WIN_DROP
              , ISNULL(ZZ.THEORIC_HOLD, 0) AS THEORIC_HOLD
              , ISNULL(TIP_DROP, 0) AS TIP_DROP
              , ISNULL(WIN_TIPS, 0) AS WIN_TIPS
              , OPEN_HOUR
              , CLOSE_HOUR
              , CASE WHEN CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED
              , CASE WHEN SESSION_SECONDS = 0 THEN DATEDIFF(SECOND, OPEN_HOUR, GETDATE()) ELSE ISNULL(SESSION_SECONDS, 0) END AS SESSION_SECONDS
              , ISNULL(SESSION_SUM, 0) AS SESSION_SUM
         FROM   @_DAYS_AND_TABLES DT
   INNER JOIN   #GT_TEMPORARY_REPORT_DATA ZZ ON DT.TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER
     ORDER BY   DT.TABLE_TYPE_IDENT ASC
              , CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC
              , DATE_TIME DESC;
  END
END

-- ERASE THE TEMPORARY DATA
DROP TABLE #GT_TEMPORARY_REPORT_DATA

END -- END PROCEDURE
GO

-- PERMISSIONS
GRANT EXECUTE ON [dbo].[GT_Base_Report_Data] TO [wggui] WITH GRANT OPTION
GO


IF OBJECT_ID (N'dbo.GT_Session_Information', N'P') IS NOT NULL
    DROP PROCEDURE dbo.GT_Session_Information;
GO

CREATE PROCEDURE [dbo].[GT_Session_Information]
(
    @pBaseType            INTEGER
  , @pDateFrom            DATETIME
  , @pDateTo              DATETIME
  , @pStatus              INTEGER
  , @pEnabled             INTEGER
  , @pAreaId              INTEGER
  , @pBankId              INTEGER
  , @pChipsISOCode        VARCHAR(50)
  , @pChipsCoinsCode      INTEGER
  , @pValidTypes          VARCHAR(4096)
)
AS
BEGIN

----------------------------------------------------------------------------------------------------------------
DECLARE @_BASE_TYPE        AS   INTEGER
DECLARE @_DATE_FROM        AS   DATETIME
DECLARE @_DATE_TO          AS   DATETIME
DECLARE @_STATUS           AS   INTEGER
DECLARE @_ENABLED          AS   INTEGER
DECLARE @_AREA             AS   INTEGER
DECLARE @_BANK             AS   INTEGER
DECLARE @_CHIPS_ISO_CODE   AS   VARCHAR(50)
DECLARE @_CHIPS_COINS_CODE AS   VARCHAR(50)
DECLARE @_DELIMITER        AS   CHAR(1)
DECLARE @_TYPES_TABLE           TABLE(SST_ID INT, SST_VALUE VARCHAR(50)) -- TO STORE THE VALID GAMING TABLE TYPES
DECLARE @_NATIONAL_CURRENCY AS  VARCHAR(3)

-- CHIP TYPES
DECLARE @_CHIPS_RE    AS INTEGER
DECLARE @_CHIPS_NR    AS INTEGER
DECLARE @_CHIPS_COLOR AS INTEGER

-- MOVEMENTS
DECLARE @_CHIPS_MOVEMENT_PURCHASE_TOTAL          AS   INTEGER
DECLARE @_CHIPS_MOVEMENT_SALES_TOTAL             AS   INTEGER
DECLARE @_CHIPS_MOVEMENT_PURCHASE_TOTAL_EXCHANGE AS   INTEGER
DECLARE @_CHIPS_MOVEMENT_SALES_TOTAL_EXCHANGE    AS   INTEGER
DECLARE @_MOVEMENT_FILLER_IN                     AS   INTEGER
DECLARE @_MOVEMENT_FILLER_OUT                    AS   INTEGER
DECLARE @_MOVEMENT_CAGE_FILLER_OUT               AS   INTEGER
DECLARE @_MOVEMENT_CAGE_CLOSE_SESSION            AS   INTEGER
DECLARE @_MOVEMENT_CASHIER_BY_CONCEPT_IN_TIPS    AS   INTEGER

DECLARE @_MOVEMENT_CLOSE_SESSION                 AS   INTEGER

----------------------------------------------------------------------------------------------------------------
-- Initialization --
SET @_DELIMITER        =   ','
SET @_BASE_TYPE        =   @pBaseType
SET @_DATE_FROM        =   @pDateFrom
SET @_DATE_TO          =   @pDateTo
SET @_STATUS           =   ISNULL(@pStatus, -1)
SET @_ENABLED          =   ISNULL(@pEnabled, -1)
SET @_AREA             =   ISNULL(@pAreaId, 0)
SET @_BANK             =   ISNULL(@pBankId, 0)
SET @_CHIPS_ISO_CODE   =   ISNULL(@pChipsISOCode, 'X01')
SET @_CHIPS_COINS_CODE =   ISNULL(@pChipsCoinsCode, -100)
SET @_CHIPS_RE         =   1001
SET @_CHIPS_NR         =   1002
SET @_CHIPS_COLOR      =   1003

SET @_CHIPS_MOVEMENT_PURCHASE_TOTAL          =   304
SET @_CHIPS_MOVEMENT_SALES_TOTAL             =   303
SET @_CHIPS_MOVEMENT_PURCHASE_TOTAL_EXCHANGE =   310
SET @_CHIPS_MOVEMENT_SALES_TOTAL_EXCHANGE    =   309
SET @_MOVEMENT_FILLER_IN                     =   2
SET @_MOVEMENT_FILLER_OUT                    =   3
SET @_MOVEMENT_CAGE_FILLER_OUT               =   203
SET @_MOVEMENT_CAGE_CLOSE_SESSION            =   201
SET @_MOVEMENT_CASHIER_BY_CONCEPT_IN_TIPS    =   1000006 --- CASHIER_MOVEMENT.CASHIER_BY_CONCEPT_IN + CageMeters.CageConceptId.Tips;

SET @_MOVEMENT_CLOSE_SESSION                 =   1

----------------------------------------------------------------------------------------------------------------
-- CHECK DATE PARAMETERS
IF @_DATE_FROM IS NULL
BEGIN
   -- IF DATE FROM IS NULL, USER FIRST POSIBLE DATE
   SET @_DATE_FROM = CAST('' AS DATETIME)
END

IF @_DATE_TO IS NULL
BEGIN
   -- IF DATE TO IS NULL, QUERY UNTIL CURRENT DATE
   SET @_DATE_TO = CAST(GETDATE() AS DATETIME)
END

-- ASSIGN TYPES PARAMETER INTO TABLE
INSERT INTO @_TYPES_TABLE SELECT * FROM dbo.SplitStringIntoTable(@pValidTypes, @_DELIMITER, DEFAULT)

-- GET NATIONAL CURRENCY
SELECT @_NATIONAL_CURRENCY = GP_KEY_VALUE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'RegionalOptions' AND GP_SUBJECT_KEY = 'CurrencyISOCode' 

----------------------------------------------------------------------------------------------------------------

IF @_BASE_TYPE = 0
BEGIN
  -- REPORT BY GAMING TABLE SESSION
  SELECT    GTS_CASHIER_SESSION_ID
          , CS_NAME
          , CS_STATUS
          , GT_NAME
          , GTT_NAME
          , GT_HAS_INTEGRATED_CASHIER
          , CT_NAME
          , GU_USERNAME
          , CS_OPENING_DATE
          , CS_CLOSING_DATE
          , SUM(ISNULL(CASE WHEN GTSC_ISO_CODE IS NULL   THEN GTS_TOTAL_PURCHASE_AMOUNT     ELSE GTSC_TOTAL_PURCHASE_AMOUNT   END, 0)  )  AS GTS_TOTAL_PURCHASE_AMOUNT
          , SUM(ISNULL(CASE WHEN GTSC_ISO_CODE IS NULL   THEN GTS_TOTAL_SALES_AMOUNT        ELSE GTSC_TOTAL_SALES_AMOUNT      END, 0)  )  AS GTS_TOTAL_SALES_AMOUNT 
          , SUM(ISNULL(CASE WHEN GTSC_ISO_CODE IS NULL   THEN GTS_INITIAL_CHIPS_AMOUNT      ELSE GTSC_INITIAL_CHIPS_AMOUNT    END, 0)  )  AS GTS_INITIAL_CHIPS_AMOUNT
          , SUM(ISNULL(CASE WHEN GTSC_ISO_CODE IS NULL   THEN GTS_FILLS_CHIPS_AMOUNT        ELSE GTSC_FILLS_CHIPS_AMOUNT      END, 0)  )  AS GTS_FILLS_CHIPS_AMOUNT 
          , SUM(ISNULL(CASE WHEN GTSC_ISO_CODE IS NULL   THEN GTS_CREDITS_CHIPS_AMOUNT      ELSE GTSC_CREDITS_CHIPS_AMOUNT    END, 0)  )  AS GTS_CREDITS_CHIPS_AMOUNT
          , SUM(ISNULL(CASE WHEN GTSC_ISO_CODE IS NULL   THEN GTS_FINAL_CHIPS_AMOUNT        ELSE GTSC_FINAL_CHIPS_AMOUNT      END, 0)  )  AS GTS_FINAL_CHIPS_AMOUNT 
          , SUM(ISNULL(CASE WHEN GTSC_ISO_CODE IS NULL   THEN GTS_TIPS                      ELSE GTSC_TIPS                    END, 0)  )  AS GTS_TIPS               
          , SUM(ISNULL(CASE WHEN GTSC_ISO_CODE IS NULL   THEN GTS_COLLECTED_AMOUNT          ELSE GTSC_COLLECTED_AMOUNT        END, 0)  )  AS GTS_COLLECTED_AMOUNT   
          , GTS_CLIENT_VISITS
          , AR_NAME
          , BK_NAME
          , GTS_GAMING_TABLE_SESSION_ID
          , dbo.GT_Calculate_DROP(SUM(ISNULL(GTSC_OWN_SALES_AMOUNT, 0)), SUM(ISNULL(GTSC_EXTERNAL_SALES_AMOUNT, 0)), SUM(ISNULL(GTSC_COLLECTED_AMOUNT, 0)), GT_HAS_INTEGRATED_CASHIER) AS DROP_BY_CURRENCY
          , CASE WHEN GTSC_ISO_CODE IS NULL   THEN @_NATIONAL_CURRENCY ELSE GTSC_ISO_CODE END AS GTSC_ISO_CODE
          , dbo.GT_Calculate_DROP(ISNULL(GTS_OWN_SALES_AMOUNT, 0), ISNULL(GTS_EXTERNAL_SALES_AMOUNT, 0), ISNULL(GTS_COLLECTED_AMOUNT, 0), GT_HAS_INTEGRATED_CASHIER) AS DROP_LOCAL_CURRENCY
          , SUM(ISNULL(CASE WHEN GTSC_ISO_CODE IS NULL   THEN GTS_COPY_DEALER_VALIDATED_AMOUNT ELSE GTSC_COPY_DEALER_VALIDATED_AMOUNT END, 0)  ) AS GTS_COPY_DEALER_VALIDATED_AMOUNT
   FROM         GAMING_TABLES_SESSIONS
   LEFT    JOIN GAMING_TABLES_SESSIONS_BY_CURRENCY ON GTS_GAMING_TABLE_SESSION_ID = GTSC_GAMING_TABLE_SESSION_ID AND GTSC_TYPE <> @_CHIPS_COLOR
   INNER   JOIN CASHIER_SESSIONS                   ON CS_SESSION_ID               = GTS_CASHIER_SESSION_ID
   LEFT    JOIN GUI_USERS                           ON GU_USER_ID                  = CS_USER_ID
   LEFT    JOIN GAMING_TABLES                       ON GT_GAMING_TABLE_ID          = GTS_GAMING_TABLE_ID
   LEFT    JOIN GAMING_TABLES_TYPES                 ON GTT_GAMING_TABLE_TYPE_ID    = GT_TYPE_ID
   LEFT    JOIN AREAS                               ON GT_AREA_ID                  = AR_AREA_ID
   LEFT    JOIN BANKS                               ON GT_BANK_ID                  = BK_BANK_ID
   LEFT    JOIN CASHIER_TERMINALS                   ON GT_CASHIER_ID               = CT_CASHIER_ID
 WHERE   CS_OPENING_DATE >= @_DATE_FROM
   AND   CS_OPENING_DATE < @_DATE_TO
   AND   (GT_ENABLED = CASE WHEN @_ENABLED = -1 THEN GT_ENABLED ELSE @_ENABLED END)
   AND   (CS_STATUS  = CASE WHEN @_STATUS  = -1 THEN CS_STATUS  ELSE @_STATUS  END)
   AND   (GT_AREA_ID = CASE WHEN @_AREA    = 0  THEN GT_AREA_ID ELSE @_AREA    END)
   AND   (GT_BANK_ID = CASE WHEN @_BANK    = 0  THEN GT_BANK_ID ELSE @_BANK    END)
   AND   (GT_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE ))
   GROUP BY GTS_CASHIER_SESSION_ID
         , CS_NAME
         , CS_STATUS
         , GT_NAME
         , GTT_NAME
         , GT_HAS_INTEGRATED_CASHIER
         , CT_NAME
         , GU_USERNAME
         , CS_OPENING_DATE
         , CS_CLOSING_DATE
         , GTS_CLIENT_VISITS
         , AR_NAME
         , BK_NAME
         , GTS_GAMING_TABLE_SESSION_ID
         , GTS_OWN_SALES_AMOUNT
         , GTS_EXTERNAL_SALES_AMOUNT
         , GTS_COLLECTED_AMOUNT
         , CASE WHEN GTSC_ISO_CODE IS NULL   THEN @_NATIONAL_CURRENCY ELSE GTSC_ISO_CODE END 
     ORDER BY CS_OPENING_DATE

END
ELSE
BEGIN
    SELECT   CM_SESSION_ID
           , CS_NAME
           , CS_STATUS
           , GT_NAME
           , GTT_NAME
           , GT_HAS_INTEGRATED_CASHIER
           , CT_NAME
           , GU_USERNAME
           , CS_OPENING_DATE
           , CS_CLOSING_DATE
           , SUM(CASE WHEN CM_TYPE                  = @_CHIPS_MOVEMENT_PURCHASE_TOTAL          THEN CM_ADD_AMOUNT
                      WHEN CM_TYPE                  = @_CHIPS_MOVEMENT_PURCHASE_TOTAL_EXCHANGE THEN CM_INITIAL_BALANCE
                      ELSE 0
                 END
                )  AS CM_TOTAL_PURCHASE_AMOUNT
           , SUM(CASE WHEN CM_TYPE                  = @_CHIPS_MOVEMENT_SALES_TOTAL             THEN CM_SUB_AMOUNT
                      WHEN CM_TYPE                  = @_CHIPS_MOVEMENT_SALES_TOTAL_EXCHANGE    THEN CM_INITIAL_BALANCE
                      ELSE 0
                 END
                )  AS CM_TOTAL_SALES_AMOUNT
           , 0     AS CM_INITIAL_CHIPS_AMOUNT                                         --  Don't show initial chips amount for movements inquiry
           , SUM(CASE WHEN (CM_CURRENCY_ISO_CODE    = @_CHIPS_ISO_CODE
                       OR  CM_CAGE_CURRENCY_TYPE  IN (@_CHIPS_RE, @_CHIPS_NR))
                       AND CM_TYPE                  = @_MOVEMENT_FILLER_IN
                      THEN CM_ADD_AMOUNT ELSE 0 END
                )  AS CM_FILLS_CHIPS_AMOUNT
           , SUM(CASE WHEN (CM_CURRENCY_ISO_CODE   = @_CHIPS_ISO_CODE
                        OR CM_CAGE_CURRENCY_TYPE  IN (@_CHIPS_RE, @_CHIPS_NR))
                       AND CM_TYPE                 = @_MOVEMENT_FILLER_OUT
                      THEN CM_SUB_AMOUNT
                      WHEN (CM_CURRENCY_ISO_CODE    = @_CHIPS_ISO_CODE
                        OR CM_CAGE_CURRENCY_TYPE  IN (@_CHIPS_RE, @_CHIPS_NR))
                       AND CM_TYPE                  = @_MOVEMENT_CAGE_CLOSE_SESSION
                       THEN CM_INITIAL_BALANCE
                       ELSE 0
                       END
                )  AS CM_CREDITS_CHIPS_AMOUNT
           , 0     AS CM_FINAL_CHIPS_AMOUNT                                           --  Don't show final chips amount for movements inquiry
           , SUM(CASE WHEN CM_TYPE                  = @_MOVEMENT_CAGE_FILLER_OUT
                       AND ((CM_CURRENCY_ISO_CODE   = @_CHIPS_ISO_CODE
                       AND CM_CURRENCY_DENOMINATION = @_CHIPS_COINS_CODE)
                        OR CM_CAGE_CURRENCY_TYPE  IN (@_CHIPS_RE, @_CHIPS_NR))
                      THEN CM_SUB_AMOUNT
                      WHEN CM_TYPE                  = @_MOVEMENT_CAGE_CLOSE_SESSION
                       AND ((CM_CURRENCY_ISO_CODE   = @_CHIPS_ISO_CODE
                       AND CM_CURRENCY_DENOMINATION = @_CHIPS_COINS_CODE)
                        OR CM_CAGE_CURRENCY_TYPE  IN (@_CHIPS_RE, @_CHIPS_NR))
                      THEN CM_INITIAL_BALANCE
                      
                      WHEN CM_TYPE                  = @_MOVEMENT_CASHIER_BY_CONCEPT_IN_TIPS  
                      THEN CM_ADD_AMOUNT    
                      ELSE 0    
                 END
                )  AS CM_TIPS
           , SUM(CASE WHEN GT_HAS_INTEGRATED_CASHIER = 0 THEN --Only gaming table
                          CASE WHEN CM_CURRENCY_ISO_CODE IS NULL
                                 OR (CM_CURRENCY_ISO_CODE <> @_CHIPS_ISO_CODE
                                AND  CM_CAGE_CURRENCY_TYPE NOT IN (@_CHIPS_RE, @_CHIPS_NR, @_CHIPS_COLOR) ) THEN
                               CASE WHEN CM_TYPE  = @_MOVEMENT_FILLER_IN            THEN -1 * ISNULL(CM_AUX_AMOUNT, CM_ADD_AMOUNT)
                                    WHEN CM_TYPE  = @_MOVEMENT_FILLER_OUT           THEN      ISNULL(CM_AUX_AMOUNT, CM_SUB_AMOUNT)
                                    WHEN CM_TYPE  = @_MOVEMENT_CAGE_CLOSE_SESSION   THEN      ISNULL(CM_AUX_AMOUNT, CM_INITIAL_BALANCE)
                                    ELSE 0
                               END
                               ELSE 0
                          END
                      WHEN GT_HAS_INTEGRATED_CASHIER = 1 THEN --Gaming table with cashier
                          CASE WHEN CM_TYPE   = @_CHIPS_MOVEMENT_PURCHASE_TOTAL THEN -1 * CM_ADD_AMOUNT
                               WHEN CM_TYPE   = @_CHIPS_MOVEMENT_SALES_TOTAL    THEN      CM_SUB_AMOUNT
                               ELSE 0
                          END
                      ELSE 0
                 END
                )  AS CM_COLLECTED_AMOUNT
           , GTS_CLIENT_VISITS
           , AR_NAME
           , BK_NAME
           , CM_GAMING_TABLE_SESSION_ID
           , dbo.GT_Calculate_DROP(SUM(ISNULL(GTSC_OWN_SALES_AMOUNT, 0)), SUM(ISNULL(GTSC_EXTERNAL_SALES_AMOUNT, 0)), SUM(ISNULL(GTSC_COLLECTED_AMOUNT, 0)), GT_HAS_INTEGRATED_CASHIER) AS DROP_BY_CURRENCY
           , CASE WHEN ISNULL(CM_CURRENCY_ISO_CODE, 'X01') = 'X01' THEN @_NATIONAL_CURRENCY ELSE CM_CURRENCY_ISO_CODE END AS CM_CURRENCY_ISO_CODE
           , dbo.GT_Calculate_DROP(SUM(ISNULL(GTS_OWN_SALES_AMOUNT, 0)), SUM(ISNULL(GTS_EXTERNAL_SALES_AMOUNT, 0)), SUM(ISNULL(GTS_COLLECTED_AMOUNT, 0)), GT_HAS_INTEGRATED_CASHIER) AS DROP_LOCAL_CURRENCY
           , 0 AS GTS_COPY_DEALER_VALIDATED_AMOUNT
      FROM         CASHIER_MOVEMENTS
     INNER   JOIN  CASHIER_SESSIONS                   ON CS_SESSION_ID               = CM_SESSION_ID
     INNER   JOIN  GAMING_TABLES_SESSIONS             ON CS_SESSION_ID               = GTS_CASHIER_SESSION_ID
      LEFT   JOIN  GAMING_TABLES_SESSIONS_BY_CURRENCY ON GTS_GAMING_TABLE_SESSION_ID = GTSC_GAMING_TABLE_SESSION_ID 
                                                     AND GTSC_TYPE <> @_CHIPS_COLOR
                                                     AND gtsc_iso_code = cm_currency_iso_code 
                                                     AND gtsc_type = cm_cage_currency_type
      LEFT   JOIN  GUI_USERS                          ON GU_USER_ID                  = CS_USER_ID
      LEFT   JOIN  GAMING_TABLES                      ON GT_GAMING_TABLE_ID          = GTS_GAMING_TABLE_ID
      LEFT   JOIN  GAMING_TABLES_TYPES                ON GTT_GAMING_TABLE_TYPE_ID    = GT_TYPE_ID
      LEFT   JOIN  AREAS                              ON GT_AREA_ID                  = AR_AREA_ID
      LEFT   JOIN  BANKS                              ON GT_BANK_ID                  = BK_BANK_ID
      LEFT   JOIN  CASHIER_TERMINALS                  ON GT_CASHIER_ID               = CT_CASHIER_ID
     WHERE   CM_DATE >= @_DATE_FROM
       AND   CM_DATE <= @_DATE_TO
       AND   (GT_ENABLED = CASE WHEN @_ENABLED = -1 THEN GT_ENABLED ELSE @_ENABLED END)
       AND   (CS_STATUS  = CASE WHEN @_STATUS  = -1 THEN CS_STATUS  ELSE @_STATUS  END)
       AND   (GT_AREA_ID = CASE WHEN @_AREA    = 0  THEN GT_AREA_ID ELSE @_AREA    END)
       AND   (GT_BANK_ID = CASE WHEN @_BANK    = 0  THEN GT_BANK_ID ELSE @_BANK    END)
       AND   (GT_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE ))
  GROUP BY   CM_SESSION_ID
           , CS_NAME
           , CS_STATUS
           , GT_NAME
           , GTT_NAME
           , GT_HAS_INTEGRATED_CASHIER
           , CT_NAME
           , GU_USERNAME
           , CS_OPENING_DATE
           , CS_CLOSING_DATE
           , GTS_CLIENT_VISITS
           , AR_NAME
           , BK_NAME
           , CM_GAMING_TABLE_SESSION_ID
           , CASE WHEN ISNULL(CM_CURRENCY_ISO_CODE, 'X01') = 'X01' THEN @_NATIONAL_CURRENCY ELSE CM_CURRENCY_ISO_CODE END
  ORDER BY   CS_OPENING_DATE

END

END -- END PROCEDURE GT_Session_Information
GO

-- PERMISSIONS
GRANT EXECUTE ON [dbo].[GT_Session_Information] TO [wggui] WITH GRANT OPTION
GO


IF OBJECT_ID (N'dbo.GT_Base_Report_Data', N'P') IS NOT NULL
    DROP PROCEDURE dbo.GT_Base_Report_Data;
GO

CREATE PROCEDURE [dbo].[GT_Base_Report_Data]
(
      @pBaseType                   INTEGER
    , @pTimeInterval               INTEGER
    , @pDateFrom                   DATETIME
    , @pDateTo                     DATETIME
    , @pOnlyActivity               INTEGER
    , @pOrderBy                    INTEGER
    , @pValidTypes                 VARCHAR(4096)
    , @pSelectedCurrency           NVARCHAR(100)
    , @pTotalToSelectedCurrency    INTEGER
)
AS
BEGIN

-- DECLARATIONS
   -- @_DAYS_AND_TABLES:    A temporary table with all intervals and all base type selected (used to show base types without activity).
   DECLARE @_DAYS_AND_TABLES TABLE(DATE_TIME DATETIME, TABLE_IDENTIFIER_VALUE BIGINT, TABLE_IDENT_NAME VARCHAR(50), TABLE_TYPE_IDENT BIGINT, TABLE_TYPE_NAME VARCHAR(50))
   -- @_DAY_VAR:            Variable to create the intervals
   DECLARE @_DAY_VAR DATETIME

-- PARAMETERS
     DECLARE @_BASE_TYPE                  AS   INTEGER -- 0 BY TABLE TYPE, 1 BY TABLE
     DECLARE @_TIME_INTERVAL              AS   INTEGER -- -1, 0 DAYS, 1 MONTH, 2 YEAR
     DECLARE @_DATE_FROM                  AS   DATETIME
     DECLARE @_DATE_TO                    AS   DATETIME
     DECLARE @_ONLY_ACTIVITY              AS   INTEGER -- 0 SHOW ALL, 1 SHOW ONLY WITH ACTIVITY
     DECLARE @_ORDER_BY                   AS   INTEGER -- 0 BY INTERVAL DESC, 1 BY IDENTIFIER ASC
     DECLARE @_DELIMITER                  AS   CHAR(1)
     DECLARE @_TYPES_TABLE                TABLE(SST_ID INT, SST_VALUE VARCHAR(50)) -- TO STORE THE VALID GAMING TABLE TYPES
     DECLARE @_SELECTED_CURRENCY          AS   NVARCHAR(3)
     DECLARE @_TOTAL_TO_SELECTED_CURRENCY AS   INTEGER
     DECLARE @_CHIP_RE                    AS   INTEGER
     DECLARE @_CHIP_NRE                   AS   INTEGER
     DECLARE @_CHIP_COLOR                 AS   INTEGER
----------------------------------------------------------------------------------------------------------------

-- Initialization --
  SET @_BASE_TYPE                  =   @pBaseType
  SET @_TIME_INTERVAL              =   @pTimeInterval
  SET @_DATE_FROM                  =   @pDateFrom
  SET @_DATE_TO                    =   @pDateTo
  SET @_ONLY_ACTIVITY              =   @pOnlyActivity
  SET @_ORDER_BY                   =   @pOrderBy
  SET @_DELIMITER                  =   ','
  SET @_SELECTED_CURRENCY          =   @pSelectedCurrency
  SET @_TOTAL_TO_SELECTED_CURRENCY =   @pTotalToSelectedCurrency
  SET @_CHIP_RE                    =   1001
  SET @_CHIP_NRE                   =   1002
  SET @_CHIP_COLOR                 =   1003
----------------------------------------------------------------------------------------------------------------

-- CHECK DATE PARAMETERS
IF @_DATE_FROM IS NULL
BEGIN
   -- IF DATE FROM IS NULL, USER FIRST POSIBLE DATE
   SET @_DATE_FROM = CAST('' AS DATETIME)
   SET @pDateFrom  = CAST('' AS DATETIME)
END

IF @_DATE_TO IS NULL
BEGIN
   -- IF DATE TO IS NULL, QUERY UNTIL CURRENT DATE
   SET @_DATE_TO = CAST(GETDATE() AS DATETIME)
   SET @pDateTo  = CAST(GETDATE() AS DATETIME)
END

-- ASSIGN TYPES PARAMETER INTO TABLE
INSERT INTO @_TYPES_TABLE SELECT * FROM dbo.SplitStringIntoTable(@pValidTypes, @_DELIMITER, DEFAULT)

IF @_TIME_INTERVAL <> -1
 BEGIN
   -- INTERVALS DATES AND TABLE PREPARATION

   -- PREPARE DATES RANGE DEPENDING ON TIME INTERVAL
   SET @_DATE_FROM = CASE
                        WHEN @_TIME_INTERVAL = 0 THEN -- BY DAY
                           DATEADD(dd, 0, DATEDIFF(dd, 0, @_DATE_FROM))
                        WHEN @_TIME_INTERVAL = 1 THEN -- BY MONTH
                           CAST(CAST(YEAR(@_DATE_FROM) AS VARCHAR(4)) + RIGHT('0' + CAST(MONTH(@_DATE_FROM) AS VARCHAR(2)), 2) + '01' AS DATETIME)
                        WHEN @_TIME_INTERVAL = 2 THEN -- BY YEAR
                           CAST(CAST(YEAR(@_DATE_FROM) AS VARCHAR(4)) + '0101' AS DATETIME)
                     END

   SET @_DATE_TO = CASE
          WHEN @_TIME_INTERVAL = 0 THEN -- BY DAY
                           DATEADD(DAY, 1, CAST(@_DATE_TO AS DATETIME))
                        WHEN @_TIME_INTERVAL = 1 THEN -- BY MONTH
                           DATEADD(MONTH, 1, CAST(CAST(YEAR(@_DATE_TO) AS VARCHAR(4)) + RIGHT('0' + CAST(MONTH(@_DATE_TO) AS VARCHAR(2)), 2) + '01' AS DATETIME))
                        WHEN @_TIME_INTERVAL = 2 THEN -- BY YEAR
                           DATEADD(YEAR, 1, CAST(CAST(YEAR(@_DATE_TO) AS VARCHAR(4)) + '0101' AS DATETIME))
                     END

   -- PREPARE THE TIME INTERVALS TABLE
   SET @_DAY_VAR = @_DATE_FROM
   WHILE @_DAY_VAR < @_DATE_TO
   BEGIN
         -- SET THE LINK FIELD FOR THE RESULTS
         IF @_BASE_TYPE = 0
          BEGIN
            -- LINK WITH TABLE TYPES
            INSERT INTO @_DAYS_AND_TABLES
                 SELECT   CAST(@_DAY_VAR AS DATETIME), X.GTT_GAMING_TABLE_TYPE_ID, X.GTT_NAME AS TABLE_IDENT, NULL, NULL
                   FROM   GAMING_TABLES_TYPES AS X
                  WHERE   X.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
                          AND     X.GTT_ENABLED = 1
          END
         ELSE
          BEGIN
            -- LINK WITH TABLES
            INSERT INTO @_DAYS_AND_TABLES
                 SELECT   CAST(@_DAY_VAR AS DATETIME), X.GT_GAMING_TABLE_ID, X.GT_NAME AS TABLE_IDENT, Z.GTT_GAMING_TABLE_TYPE_ID, Z.GTT_NAME
                   FROM   GAMING_TABLES AS X
              LEFT JOIN   GAMING_TABLES_TYPES AS Z
                     ON   Z.GTT_GAMING_TABLE_TYPE_ID = X.GT_TYPE_ID
                  WHERE   Z.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
                          AND X.GT_ENABLED = 1
          END

          -- SET INCREMENT
          SET @_DAY_VAR = CASE
                               WHEN @_TIME_INTERVAL = 0 THEN DATEADD(DAY,1,@_DAY_VAR)     -- DAY
                               WHEN @_TIME_INTERVAL = 1 THEN DATEADD(MONTH,1,@_DAY_VAR)   -- MONTH
                               WHEN @_TIME_INTERVAL = 2 THEN DATEADD(YEAR,1,@_DAY_VAR)    -- YEAR
                          END
   END

END -- IF INTERVALS <> - 1
ELSE
BEGIN

   -- SET THE LINK FIELD FOR THE RESULTS WITHOUT INTERVALS
   IF @_BASE_TYPE = 0
    BEGIN
      -- LINK WITH TABLE TYPES
      INSERT INTO @_DAYS_AND_TABLES
           SELECT   NULL, X.GTT_GAMING_TABLE_TYPE_ID, X.GTT_NAME AS TABLE_IDENT, NULL, NULL
             FROM   GAMING_TABLES_TYPES AS X
            WHERE   X.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
                    AND X.GTT_ENABLED = 1
    END
   ELSE
    BEGIN
      -- LINK WITH TABLES
      INSERT INTO @_DAYS_AND_TABLES
           SELECT   NULL, X.GT_GAMING_TABLE_ID, X.GT_NAME AS TABLE_IDENT, Z.GTT_GAMING_TABLE_TYPE_ID, Z.GTT_NAME
             FROM   GAMING_TABLES AS X
        LEFT JOIN   GAMING_TABLES_TYPES AS Z
               ON   Z.GTT_GAMING_TABLE_TYPE_ID = X.GT_TYPE_ID
            WHERE   Z.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
                    AND X.GT_ENABLED = 1
    END

 END


-- SELECT INTO TEMPORARY TABLE WITH ALL DATA
-- SPECIFIC REPORT QUERY
  SELECT   X.TABLE_IDENTIFIER
         , X.TABLE_NAME
         , X.TABLE_TYPE
         , X.TABLE_TYPE_NAME
         , SUM(X.COPY_DEALER_VALIDATED_AMOUNT) AS COPY_DEALER_VALIDATED_AMOUNT         
         , SUM(X.S_DROP_GAMBLING_TABLE) + SUM (X.S_DROP_CASHIER) TOTAL_DROP
         , SUM(X.S_DROP_GAMBLING_TABLE) TOTAL_DROP_GAMBLING_TABLE
         , SUM(X.S_DROP_CASHIER) TOTAL_DROP_CASHIER
         , SUM(X.S_WIN) TOTAL_WIN
         , ISNULL(SUM(X.S_TIP),0)  TOTAL_TIP
         , CASE WHEN SUM(X.S_DROP_GAMBLING_TABLE) + SUM (X.S_DROP_CASHIER) = 0 THEN 0 ELSE CAST(SUM(X.S_WIN) AS DECIMAL(18,2)) / CAST(SUM(X.S_DROP_GAMBLING_TABLE) + SUM (X.S_DROP_CASHIER) AS DECIMAL(18,2)) * 100 END AS WIN_DROP
         , MIN(X.THEORIC_HOLD) AS THEORIC_HOLD
         , CASE WHEN SUM(X.S_DROP_GAMBLING_TABLE) + SUM (X.S_DROP_CASHIER) = 0 THEN 0 ELSE SUM(CAST(X.S_TIP AS DECIMAL(18,2))) / CAST(SUM(X.S_DROP_GAMBLING_TABLE) + SUM (X.S_DROP_CASHIER) AS DECIMAL(18,2)) * 100 END AS TIP_DROP
         , SUM(X.S_WIN) + SUM(X.S_TIP) AS WIN_TIPS
         , X.CM_DATE_ONLY
         , MIN(X.OPEN_HOUR) AS OPEN_HOUR
         , MAX(X.CLOSE_HOUR) AS CLOSE_HOUR
         , SUM(X.SESSION_SECONDS) AS SESSION_SECONDS
         , COUNT(X.SESSION_SECONDS) AS SESSION_SUM
    INTO   #GT_TEMPORARY_REPORT_DATA
    FROM (
         -- CORE QUERY
         SELECT CASE WHEN @_TIME_INTERVAL = 0      -- TO FILTER BY DAY
                     THEN DATEADD(dd, 0, DATEDIFF(dd, 0, CS_OPENING_DATE))
                     WHEN @_TIME_INTERVAL = 1      -- TO FILTER BY MONTH
                     THEN CAST(CAST(YEAR(CS_OPENING_DATE) AS VARCHAR(4)) + RIGHT('0' + CAST(MONTH(CS_OPENING_DATE) AS VARCHAR(2)), 2) + '01' AS DATETIME)
                     WHEN @_TIME_INTERVAL = 2      -- TO FILTER BY YEAR
                     THEN CAST(CAST(YEAR(CS_OPENING_DATE) AS VARCHAR(4)) + '01'+ '01' AS DATETIME)
                     END                                                                                       AS CM_DATE_ONLY
                , (CASE WHEN @_BASE_TYPE = 0 THEN GTT_GAMING_TABLE_TYPE_ID ELSE GT.GT_GAMING_TABLE_ID END)   AS TABLE_IDENTIFIER -- GET THE BASE TYPE IDENTIFIER
                , (CASE WHEN @_BASE_TYPE = 0 THEN GTT_NAME ELSE GT.GT_NAME END)                              AS TABLE_NAME       -- GET THE BASE TYPE IDENTIFIER NAME
                , (CASE WHEN @_BASE_TYPE = 0 THEN NULL ELSE GTT_GAMING_TABLE_TYPE_ID END)                    AS TABLE_TYPE       -- TYPE
                , (CASE WHEN @_BASE_TYPE = 0 THEN NULL ELSE GTT_NAME END)                                    AS TABLE_TYPE_NAME  -- TYPE NAME

                ,  CASE WHEN @_TOTAL_TO_SELECTED_CURRENCY = 0 
                        THEN DBO.GT_CALCULATE_DROP_GAMBLING_TABLES(ISNULL(GTS_COLLECTED_AMOUNT                , 0)
                                                                 , ISNULL(GTS_COLLECTED_DROPBOX_AMOUNT        , 0)
                                                                 , ISNULL(GTS_COLLECTED_DROPBOX_CHIPS_AMOUNT  , 0)
                                                                 , 0 -- It does not allow takings of dropbox with tickets
                                                                 , GT_HAS_INTEGRATED_CASHIER)
                        WHEN @_TOTAL_TO_SELECTED_CURRENCY = 1 
                        THEN DBO.GT_CALCULATE_DROP_GAMBLING_TABLES(SUM(ISNULL(GTSC_COLLECTED_AMOUNT                , 0))
                                                                 , SUM(ISNULL(CASE WHEN GTSC_TYPE = 0            THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                                   WHEN GTSC_TYPE = 1            THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                                   ELSE 0 END,  0))
                                                                 , SUM(ISNULL(CASE WHEN GTSC_TYPE = @_CHIP_RE    THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                                   WHEN GTSC_TYPE = @_CHIP_NRE   THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                                   WHEN GTSC_TYPE = @_CHIP_COLOR THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                                   ELSE 0 END,  0))
                                                                 , 0 -- It does not allow takings of dropbox with tickets
                                                                 , GT_HAS_INTEGRATED_CASHIER)
                        END                                                                                  AS S_DROP_GAMBLING_TABLE

                ,  CASE WHEN @_TOTAL_TO_SELECTED_CURRENCY = 0 
                        THEN DBO.GT_CALCULATE_DROP_CASHIER(ISNULL(GTS_OWN_SALES_AMOUNT, 0)
                                                         , ISNULL(GTS_EXTERNAL_SALES_AMOUNT, 0))
                        WHEN @_TOTAL_TO_SELECTED_CURRENCY = 1 
                        THEN DBO.GT_CALCULATE_DROP_CASHIER(SUM(ISNULL(GTSC_OWN_SALES_AMOUNT, 0))
                                                         , SUM(ISNULL(GTSC_EXTERNAL_SALES_AMOUNT, 0)))
                        END                                                                              AS S_DROP_CASHIER

                ,  GT_THEORIC_HOLD AS THEORIC_HOLD
                ,  CASE WHEN @_TOTAL_TO_SELECTED_CURRENCY = 0 
                        THEN DBO.GT_CALCULATE_WIN(ISNULL(GTS_FINAL_CHIPS_AMOUNT      , 0)
                                                , ISNULL(GTS_INITIAL_CHIPS_AMOUNT    , 0)
                                                , ISNULL(GTS_FILLS_CHIPS_AMOUNT      , 0)
                                                , ISNULL(GTS_CREDITS_CHIPS_AMOUNT    , 0)                                                        
                                                , ISNULL(GTS_TIPS                    , 0)
                                                , ISNULL(GTS_OWN_SALES_AMOUNT        , 0)
                                                , ISNULL(GTS_EXTERNAL_SALES_AMOUNT   , 0)
                                                , ISNULL(GTS_COLLECTED_AMOUNT        , 0)
                                                , ISNULL(GTS_COLLECTED_DROPBOX_AMOUNT        , 0)
                                                , ISNULL(GTS_COLLECTED_DROPBOX_CHIPS_AMOUNT  , 0)
                                                , 0 -- It does not allow takings of dropbox with tickets)
                                                , GT_HAS_INTEGRATED_CASHIER)
                        WHEN @_TOTAL_TO_SELECTED_CURRENCY = 1 
                        THEN DBO.GT_CALCULATE_WIN(SUM(ISNULL(GTSC_FINAL_CHIPS_AMOUNT   , 0))
                                                , SUM(ISNULL(GTSC_INITIAL_CHIPS_AMOUNT , 0))
                                                , SUM(ISNULL(GTSC_FILLS_CHIPS_AMOUNT   , 0))
                                                , SUM(ISNULL(GTSC_CREDITS_CHIPS_AMOUNT , 0))
                                                , SUM(ISNULL(GTSC_TIPS                 , 0))
                                                , SUM(ISNULL(GTSC_OWN_SALES_AMOUNT     , 0))
                                                , SUM(ISNULL(GTSC_EXTERNAL_SALES_AMOUNT, 0))
                                                , SUM(ISNULL(GTSC_COLLECTED_AMOUNT     , 0))
                                                , SUM(ISNULL(CASE WHEN GTSC_TYPE = 0            THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                  WHEN GTSC_TYPE = 1            THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                  ELSE 0 END,  0))
                                                , SUM(ISNULL(CASE WHEN GTSC_TYPE = @_CHIP_RE    THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                  WHEN GTSC_TYPE = @_CHIP_NRE   THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                  WHEN GTSC_TYPE = @_CHIP_COLOR THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                  ELSE 0 END,  0))
                                                , 0 -- It does not allow takings of dropbox with tickets
                                                , GT_HAS_INTEGRATED_CASHIER)
                        END AS S_WIN

                     ,  CASE WHEN @_TOTAL_TO_SELECTED_CURRENCY = 0 THEN ISNULL(GTS_TIPS, 0)
                             WHEN @_TOTAL_TO_SELECTED_CURRENCY = 1 THEN ISNULL(GTSC_TIPS, 0)
                             END                                                                       AS S_TIP
                     , CS.CS_OPENING_DATE                                                              AS OPEN_HOUR
                     , CS.CS_CLOSING_DATE                                                              AS CLOSE_HOUR
                     , DATEDIFF(SECOND, CS_OPENING_DATE, ISNULL(CS_CLOSING_DATE, GETDATE()))           AS SESSION_SECONDS
                     , CASE WHEN @_TOTAL_TO_SELECTED_CURRENCY = 0 
                            THEN ISNULL(GTS_COPY_DEALER_VALIDATED_AMOUNT, 0)
                            WHEN @_TOTAL_TO_SELECTED_CURRENCY = 1 
                            THEN SUM(ISNULL(GTSC_COPY_DEALER_VALIDATED_AMOUNT, 0))
                            END                                                           AS COPY_DEALER_VALIDATED_AMOUNT
                FROM   GAMING_TABLES_SESSIONS GTS
                LEFT   JOIN GAMING_TABLES_SESSIONS_BY_CURRENCY GTSC ON GTSC.GTSC_GAMING_TABLE_SESSION_ID = GTS.GTS_GAMING_TABLE_SESSION_ID
                                                                   AND GTSC_ISO_CODE IN (@_SELECTED_CURRENCY)
               INNER   JOIN CASHIER_SESSIONS CS  ON GTS.GTS_CASHIER_SESSION_ID = CS.CS_SESSION_ID
                                                AND CS_OPENING_DATE >= @pDateFrom AND CS_OPENING_DATE < @pDateTo
               INNER   JOIN GAMING_TABLES GT ON GTS.GTS_GAMING_TABLE_ID = GT.GT_GAMING_TABLE_ID
                                            AND GT.GT_TYPE_ID IN (SELECT SST_VALUE FROM @_TYPES_TABLE)
               INNER   JOIN GAMING_TABLES_TYPES GTT ON GT.GT_TYPE_ID = GTT.GTT_GAMING_TABLE_TYPE_ID
               WHERE   CS_STATUS = 1  -- Only closed sessions
               GROUP BY CS_OPENING_DATE, GTT_GAMING_TABLE_TYPE_ID, GT_GAMING_TABLE_ID, GTT_NAME, GT_NAME
                      , GTS_OWN_SALES_AMOUNT, GTS_EXTERNAL_SALES_AMOUNT, GTS_COLLECTED_AMOUNT, GT_HAS_INTEGRATED_CASHIER
                      , GTS_COLLECTED_DROPBOX_AMOUNT, GTS_COLLECTED_DROPBOX_AMOUNT, GTS_DROPBOX_ENABLED
                      , GTS_COLLECTED_DROPBOX_CHIPS_AMOUNT, GT_THEORIC_HOLD, GTS_FINAL_CHIPS_AMOUNT, GTS_INITIAL_CHIPS_AMOUNT
                      , GTS_FILLS_CHIPS_AMOUNT, GTS_CREDITS_CHIPS_AMOUNT, GTS_TIPS, GTSC_TIPS, CS_CLOSING_DATE
                      , GTS_COPY_DEALER_VALIDATED_AMOUNT
          -- END CORE QUERY
        ) AS X
  GROUP BY   X.TABLE_TYPE, X.TABLE_IDENTIFIER, X.TABLE_TYPE_NAME, X.TABLE_NAME, X.CM_DATE_ONLY  -- Group by indentifier and time interval

IF @_TIME_INTERVAL <> -1
BEGIN
  -- INTERVALS DATES AND TABLE FINAL PREPARATION
  -- FILTER ACTIVITY
  IF @_ONLY_ACTIVITY = 0
    BEGIN
      -- JOIN THE SELECT WITH ALL DATA
      SELECT   DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER
             , DT.TABLE_IDENT_NAME AS TABLE_NAME
             , DT.TABLE_TYPE_IDENT AS TABLE_TYPE
             , DT.TABLE_TYPE_NAME AS TABLE_TYPE_NAME
             , ISNULL(zz.COPY_DEALER_VALIDATED_AMOUNT, 0) AS COPY_DEALER_AMOUNT
             , ISNULL(ZZ.TOTAL_DROP_GAMBLING_TABLE,0) AS TOTAL_DROP_GAMBLING_TABLE
             , ISNULL(ZZ.TOTAL_DROP_CASHIER,0) AS  TOTAL_DROP_CASHIER
             , ISNULL(ZZ.TOTAL_DROP, 0) AS TOTAL_DROP
             , ISNULL(ZZ.TOTAL_WIN, 0) AS TOTAL_WIN
             , ISNULL(ZZ.TOTAL_TIP, 0) AS TOTAL_TIP
             , ISNULL(ZZ.WIN_DROP, 0) AS WIN_DROP
             , ISNULL(ZZ.THEORIC_HOLD, 0) AS THEORIC_HOLD
             , ISNULL(ZZ.TIP_DROP, 0) AS TIP_DROP
             , ISNULL(ZZ.WIN_TIPS, 0) AS WIN_TIPS
             , ZZ.OPEN_HOUR
             , ZZ.CLOSE_HOUR
             , CASE WHEN ZZ.CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED
             , CASE WHEN ZZ.SESSION_SECONDS = 0 THEN DATEDIFF(SECOND,ZZ.OPEN_HOUR,GETDATE()) ELSE ISNULL(ZZ.SESSION_SECONDS, 0) END AS SESSION_SECONDS
             , ISNULL(ZZ.SESSION_SUM, 0) AS SESSION_SUM
             , DATE_TIME              
        FROM   @_DAYS_AND_TABLES DT
   LEFT JOIN   #GT_TEMPORARY_REPORT_DATA ZZ
          ON   (  (@_TIME_INTERVAL = 0 AND DATE_TIME = ZZ.CM_DATE_ONLY)
               OR (@_TIME_INTERVAL = 1 AND MONTH(DATE_TIME) = MONTH(ZZ.CM_DATE_ONLY) AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
               OR (@_TIME_INTERVAL = 2 AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
               ) AND TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER
       -- SET ORDER
    ORDER BY   DT.TABLE_TYPE_IDENT ASC
             , CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC
             , DATE_TIME DESC;

    END
  ELSE
    BEGIN
      -- JOIN THE SELECT HIDDING WITHOUT ACTIVITY
      SELECT   DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER
             , DT.TABLE_IDENT_NAME AS TABLE_NAME
             , DT.TABLE_TYPE_IDENT AS TABLE_TYPE
             , DT.TABLE_TYPE_NAME AS TABLE_TYPE_NAME
             , ISNULL(zz.COPY_DEALER_VALIDATED_AMOUNT, 0) AS COPY_DEALER_AMOUNT
             , ISNULL(ZZ.TOTAL_DROP_GAMBLING_TABLE,0) AS TOTAL_DROP_GAMBLING_TABLE 
             , ISNULL(ZZ.TOTAL_DROP_CASHIER,0) AS  TOTAL_DROP_CASHIER
             , ISNULL(ZZ.TOTAL_DROP, 0) AS TOTAL_DROP 
             , ISNULL(ZZ.TOTAL_WIN, 0) AS TOTAL_WIN
             , ISNULL(ZZ.TOTAL_TIP, 0) AS TOTAL_TIP
             , ISNULL(ZZ.WIN_DROP, 0) AS WIN_DROP
             , ISNULL(ZZ.THEORIC_HOLD, 0) AS THEORIC_HOLD
             , ISNULL(ZZ.TIP_DROP, 0) AS TIP_DROP
             , ISNULL(ZZ.WIN_TIPS, 0) AS WIN_TIPS
             , ZZ.OPEN_HOUR
             , ZZ.CLOSE_HOUR
             , CASE WHEN ZZ.CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED
             , CASE WHEN ZZ.SESSION_SECONDS = 0 THEN DATEDIFF(SECOND,ZZ.OPEN_HOUR,GETDATE()) ELSE ISNULL(ZZ.SESSION_SECONDS, 0) END AS SESSION_SECONDS
             , ISNULL(ZZ.SESSION_SUM, 0) AS SESSION_SUM
             , DATE_TIME
        FROM   @_DAYS_AND_TABLES DT
  INNER JOIN   #GT_TEMPORARY_REPORT_DATA ZZ
          ON   (  (@_TIME_INTERVAL = 0 AND DATE_TIME = ZZ.CM_DATE_ONLY)
               OR (@_TIME_INTERVAL = 1 AND MONTH(DATE_TIME) = MONTH(ZZ.CM_DATE_ONLY) AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
               OR (@_TIME_INTERVAL = 2 AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
               ) AND TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER
       -- SET ORDER
    ORDER BY   DT.TABLE_TYPE_IDENT ASC
             , CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC
             , DATE_TIME DESC;
    END -- IF ONLY_ACTIVITY
  END
ELSE  -- ELSE WITHOUT INTERVALS
  BEGIN
   -- FINAL WITHOUT INTERVALS
      -- FILTER ACTIVITY
   IF @_ONLY_ACTIVITY = 0
     BEGIN
            -- JOIN DATA INCLUDING WITHOUT ACTIVITY
      SELECT   DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER
             , DT.TABLE_IDENT_NAME AS TABLE_NAME
             , DT.TABLE_TYPE_IDENT AS TABLE_TYPE
             , DT.TABLE_TYPE_NAME
             , ISNULL(zz.COPY_DEALER_VALIDATED_AMOUNT, 0) AS COPY_DEALER_AMOUNT
             , ISNULL(ZZ.TOTAL_DROP_GAMBLING_TABLE,0) AS TOTAL_DROP_GAMBLING_TABLE
             , ISNULL(ZZ.TOTAL_DROP_CASHIER,0) AS  TOTAL_DROP_CASHIER
             , ISNULL(TOTAL_DROP, 0) AS TOTAL_DROP
             , ISNULL(TOTAL_WIN, 0) AS TOTAL_WIN
             , ISNULL(TOTAL_TIP, 0) AS TOTAL_TIP
             , ISNULL(WIN_DROP, 0) AS WIN_DROP
             , ISNULL(ZZ.THEORIC_HOLD, 0) AS THEORIC_HOLD
             , ISNULL(TIP_DROP, 0) AS TIP_DROP
             , ISNULL(WIN_TIPS, 0) AS WIN_TIPS
             , OPEN_HOUR
             , CLOSE_HOUR
             , CASE WHEN CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED
             , CASE WHEN SESSION_SECONDS = 0 THEN DATEDIFF(SECOND, OPEN_HOUR, GETDATE()) ELSE ISNULL(SESSION_SECONDS, 0) END AS SESSION_SECONDS
             , ISNULL(SESSION_SUM, 0) AS SESSION_SUM
        FROM   @_DAYS_AND_TABLES DT
   LEFT JOIN   #GT_TEMPORARY_REPORT_DATA ZZ ON DT.TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER
    ORDER BY   DT.TABLE_TYPE_IDENT ASC
             , CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC
             , DATE_TIME DESC;

     END
   ELSE
     BEGIN
       -- JOIN DATA WITH ONLY ACTIVITY
       SELECT   DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER
              , DT.TABLE_IDENT_NAME AS TABLE_NAME
              , DT.TABLE_TYPE_IDENT AS TABLE_TYPE
              , DT.TABLE_TYPE_NAME
              , ISNULL(zz.COPY_DEALER_VALIDATED_AMOUNT, 0) AS COPY_DEALER_AMOUNT
              , ISNULL(ZZ.TOTAL_DROP_GAMBLING_TABLE,0) AS TOTAL_DROP_GAMBLING_TABLE
              , ISNULL(ZZ.TOTAL_DROP_CASHIER,0) AS  TOTAL_DROP_CASHIER
              , ISNULL(TOTAL_DROP, 0) AS TOTAL_DROP
              , ISNULL(TOTAL_WIN, 0) AS TOTAL_WIN
              , ISNULL(TOTAL_TIP, 0) AS TOTAL_TIP
              , ISNULL(WIN_DROP, 0) AS WIN_DROP
              , ISNULL(ZZ.THEORIC_HOLD, 0) AS THEORIC_HOLD
              , ISNULL(TIP_DROP, 0) AS TIP_DROP
              , ISNULL(WIN_TIPS, 0) AS WIN_TIPS
              , OPEN_HOUR
              , CLOSE_HOUR
              , CASE WHEN CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED
              , CASE WHEN SESSION_SECONDS = 0 THEN DATEDIFF(SECOND, OPEN_HOUR, GETDATE()) ELSE ISNULL(SESSION_SECONDS, 0) END AS SESSION_SECONDS
              , ISNULL(SESSION_SUM, 0) AS SESSION_SUM
         FROM   @_DAYS_AND_TABLES DT
   INNER JOIN   #GT_TEMPORARY_REPORT_DATA ZZ ON DT.TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER
     ORDER BY   DT.TABLE_TYPE_IDENT ASC
              , CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC
              , DATE_TIME DESC;
  END
END

-- ERASE THE TEMPORARY DATA
DROP TABLE #GT_TEMPORARY_REPORT_DATA

END -- END PROCEDURE
GO

-- PERMISSIONS
GRANT EXECUTE ON [dbo].[GT_Base_Report_Data] TO [wggui] WITH GRANT OPTION
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GT_HourlyDrop]') AND type in (N'P', N'PC'))
   DROP PROCEDURE [dbo].[GT_HourlyDrop]
GO

CREATE PROCEDURE [dbo].[GT_HourlyDrop]
 (
    @pGamingTableSessionId	BIGINT,
    @pDateFrom				DATETIME,
    @pDateTo				DATETIME,
    @pIsoCode				NVARCHAR(3),
    @pSaleChipsMov			INT,
    @pValCopyDealerMov		INT,
    @pRedeemableChipsType   INT
 )
AS
BEGIN

DECLARE @NationalCurrency AS NVARCHAR(3)

SELECT @NationalCurrency = GP_KEY_VALUE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'RegionalOptions' AND  GP_SUBJECT_KEY = 'CurrencyISOCode'

DECLARE  @t_Hour_Iso 
  TABLE( TEMP_SESSION_ID BIGINT,
             TEMP_DATETIME DATETIME, 
         TEMP_ISO_CODE VARCHAR(3));  

IF @pGamingTableSessionId IS NOT NULL
BEGIN
            SELECT   @pDateFrom = CONVERT(DATETIME, CONVERT(NVARCHAR(13), CS_OPENING_DATE, 21) + ':00:00', 21)
                     , @pDateTo = ISNULL(CS_CLOSING_DATE, GETDATE())
              FROM   GAMING_TABLES_SESSIONS 
INNER JOIN   CASHIER_SESSIONS ON CS_SESSION_ID = GTS_CASHIER_SESSION_ID
            WHERE   GTS_GAMING_TABLE_SESSION_ID = @pGamingTableSessionId
END
ELSE
BEGIN
      IF @pDateFrom IS NULL
            SET @pDateFrom = CONVERT(DATETIME, 0)
            
      IF @pDateTo IS NULL
            SET @pDateTo = GETDATE()
END

DECLARE @Date AS DATETIME
DECLARE @GTSessionId AS BIGINT
DECLARE @IsoCode AS NVARCHAR(3)
DECLARE @OpeningDate AS DATETIME
DECLARE @ClosingDate AS DATETIME

DECLARE ISO_CODES_CURSOR CURSOR FOR
                        SELECT    DISTINCT GTS_GAMING_TABLE_SESSION_ID
                                , ISNULL(GTSC_ISO_CODE, @NationalCurrency)
                                , CS_OPENING_DATE
                                , ISNULL(CS_CLOSING_DATE, GETDATE())
                          FROM GAMING_TABLES_SESSIONS 
                          INNER  JOIN CASHIER_SESSIONS ON CS_SESSION_ID = GTS_CASHIER_SESSION_ID
                          LEFT   JOIN GAMING_TABLES_SESSIONS_BY_CURRENCY ON GTSC_GAMING_TABLE_SESSION_ID = GTS_GAMING_TABLE_SESSION_ID
                          WHERE  ((@pGamingTableSessionId IS NOT NULL AND GTS_GAMING_TABLE_SESSION_ID = @pGamingTableSessionId)
                                                             OR  (@pGamingTableSessionId IS NULL AND ISNULL(CS_CLOSING_DATE, CS_OPENING_DATE) < @pDateTo))
                                               AND (ISNULL(@pIsoCode,'') = '' OR @pIsoCode = GTSC_ISO_CODE) 
                                               AND ISNULL(GTSC_TYPE, @pRedeemableChipsType) = @pRedeemableChipsType
                                               
						UNION
						
						SELECT    DISTINCT GTPM_GAMING_TABLE_SESSION_ID
                                , GTPM_ISO_CODE
                                , CS_OPENING_DATE
                                , ISNULL(CS_CLOSING_DATE, GETDATE())
                           FROM  GT_PLAYERTRACKING_MOVEMENTS 
                          INNER  JOIN GAMING_TABLES_SESSIONS ON GTPM_GAMING_TABLE_SESSION_ID = GTS_GAMING_TABLE_SESSION_ID
                          INNER  JOIN CASHIER_SESSIONS ON CS_SESSION_ID = GTS_CASHIER_SESSION_ID
                          WHERE  ((@pGamingTableSessionId IS NOT NULL AND GTPM_GAMING_TABLE_SESSION_ID = @pGamingTableSessionId OR @pGamingTableSessionId IS NULL)
                                                             OR  (@pGamingTableSessionId IS NULL AND CS_OPENING_DATE >= @pDateFrom AND ISNULL(CS_CLOSING_DATE, CS_OPENING_DATE) < @pDateTo))
                                                       AND  GTPM_TYPE IN (@pSaleChipsMov, @pValCopyDealerMov)
                                               AND (ISNULL(@pIsoCode,'') = '' OR @pIsoCode = GTPM_ISO_CODE)


OPEN ISO_CODES_CURSOR   
  FETCH NEXT FROM ISO_CODES_CURSOR INTO @GTSessionId, @IsoCode, @OpeningDate, @ClosingDate

  WHILE @@FETCH_STATUS = 0   
  BEGIN   
    
    SET @Date = @pDateFrom

    WHILE @Date < @pDateTo
    BEGIN
    IF @Date >= CONVERT(NVARCHAR(13), @OpeningDate, 21) + ':00:00' AND @Date <= @ClosingDate
            BEGIN
              INSERT INTO @t_Hour_Iso
                     VALUES(@GTSessionId, CONVERT(NVARCHAR(13), @Date, 21) + ':00:00', @IsoCode)
            END
      SET @Date = DATEADD(HH, 1, @Date)
    END
      
    FETCH NEXT FROM ISO_CODES_CURSOR INTO @GTSessionId, @IsoCode, @OpeningDate, @ClosingDate  
  END  

  CLOSE ISO_CODES_CURSOR   
  DEALLOCATE ISO_CODES_CURSOR
  
   SELECT   TEMP_SESSION_ID
          , TEMP_DATETIME
          , TEMP_ISO_CODE
          , ISNULL(SUM(GTPM_VALUE), 0) AS DROP_AMOUNT
          , CS_NAME
          , GT_NAME
     FROM   @t_Hour_Iso
LEFT JOIN   GT_PLAYERTRACKING_MOVEMENTS ON TEMP_ISO_CODE = GTPM_ISO_CODE
                            AND TEMP_DATETIME = CONVERT(NVARCHAR(13), GTPM_DATETIME, 21) + ':00:00'
                            AND GTPM_GAMING_TABLE_SESSION_ID = TEMP_SESSION_ID
                            AND GTPM_TYPE IN (@pSaleChipsMov, @pValCopyDealerMov)
INNER JOIN  GAMING_TABLES_SESSIONS ON TEMP_SESSION_ID = GTS_GAMING_TABLE_SESSION_ID
INNER JOIN  CASHIER_SESSIONS ON GTS_CASHIER_SESSION_ID = CS_SESSION_ID
INNER JOIN  GAMING_TABLES ON GTS_GAMING_TABLE_ID = GT_GAMING_TABLE_ID
GROUP BY    TEMP_SESSION_ID
          , TEMP_DATETIME
          , TEMP_ISO_CODE                            
          , CS_NAME
          , GT_NAME
ORDER BY    TEMP_SESSION_ID
          , TEMP_DATETIME
          , TEMP_ISO_CODE
               
END -- END PROCEDURE
GO

-- PERMISSIONS
GRANT EXECUTE ON [dbo].[GT_HourlyDrop] TO [wggui] WITH GRANT OPTION
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GT_HourlyDrop]') AND type in (N'P', N'PC'))
   DROP PROCEDURE [dbo].[GT_HourlyDrop]
GO

/*
----------------------------------------------------------------------------------------------------------------
REPORT QUERY FOR CASHIER & GUI DROP HOURLY FOR GAMBLING TABLES

Version     Date          User              Description
----------------------------------------------------------------------------------------------------------------
1.0.0       26-APR-2017   JML&RAB&DHA       New procedure
1.0.1       05-MAY-2017   DHA               PBI 27175:Drop gaming table information - Global drop hourly report (Filter)
----------------------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[GT_HourlyDrop]
(
    @pGamingTableSessionId	BIGINT,
    @pDateFrom				      DATETIME,
    @pDateTo				        DATETIME,
    @pIsoCode				        NVARCHAR(3),
    @pSaleChipsMov			    INT,
    @pValCopyDealerMov		  INT,
    @pRedeemableChipsType   INT,
    @pGamingTablesId				NVARCHAR(MAX)
)
AS
BEGIN

DECLARE @NationalCurrency NVARCHAR(3)
DECLARE @_DELIMITER       NVARCHAR(1)
DECLARE @_GAMING_TABLE    TABLE(SST_ID INT, SST_VALUE VARCHAR(50))

SET     @_DELIMITER = ','

INSERT INTO @_GAMING_TABLE SELECT * FROM dbo.SplitStringIntoTable(@pGamingTablesId, @_DELIMITER, DEFAULT)

SELECT @NationalCurrency = GP_KEY_VALUE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'RegionalOptions' AND  GP_SUBJECT_KEY = 'CurrencyISOCode'

DECLARE  @t_Hour_Iso 
  TABLE( TEMP_SESSION_ID BIGINT,
             TEMP_DATETIME DATETIME, 
         TEMP_ISO_CODE VARCHAR(3));  

IF @pGamingTableSessionId IS NOT NULL
BEGIN
            SELECT   @pDateFrom = CONVERT(DATETIME, CONVERT(NVARCHAR(13), CS_OPENING_DATE, 21) + ':00:00', 21)
                     , @pDateTo = ISNULL(CS_CLOSING_DATE, GETDATE())
              FROM   GAMING_TABLES_SESSIONS 
INNER JOIN   CASHIER_SESSIONS ON CS_SESSION_ID = GTS_CASHIER_SESSION_ID
            WHERE   GTS_GAMING_TABLE_SESSION_ID = @pGamingTableSessionId
END
ELSE
BEGIN
      IF @pDateFrom IS NULL
            SET @pDateFrom = CONVERT(DATETIME, 0)
            
      IF @pDateTo IS NULL
            SET @pDateTo = GETDATE()
END

DECLARE @Date AS DATETIME
DECLARE @GTSessionId AS BIGINT
DECLARE @IsoCode AS NVARCHAR(3)
DECLARE @OpeningDate AS DATETIME
DECLARE @ClosingDate AS DATETIME

DECLARE ISO_CODES_CURSOR CURSOR FOR
                        SELECT    DISTINCT GTS_GAMING_TABLE_SESSION_ID
                                , ISNULL(GTSC_ISO_CODE, @NationalCurrency)
                                , CS_OPENING_DATE
                                , ISNULL(CS_CLOSING_DATE, GETDATE())
                          FROM GAMING_TABLES_SESSIONS 
                          INNER  JOIN CASHIER_SESSIONS ON CS_SESSION_ID = GTS_CASHIER_SESSION_ID
                          LEFT   JOIN GAMING_TABLES_SESSIONS_BY_CURRENCY ON GTSC_GAMING_TABLE_SESSION_ID = GTS_GAMING_TABLE_SESSION_ID
                          WHERE  ((@pGamingTableSessionId IS NOT NULL AND GTS_GAMING_TABLE_SESSION_ID = @pGamingTableSessionId)
                                                             OR  (@pGamingTableSessionId IS NULL AND ISNULL(CS_CLOSING_DATE, CS_OPENING_DATE) < @pDateTo))
                            AND  (ISNULL(@pIsoCode,'') = '' OR @pIsoCode = GTSC_ISO_CODE) 
                            AND  ISNULL(GTSC_TYPE, @pRedeemableChipsType) = @pRedeemableChipsType
                            AND  ( GTS_GAMING_TABLE_ID IN ( SELECT SST_VALUE FROM @_GAMING_TABLE ) OR LEN(@pGamingTablesId) = 0 )
                                               
                                   UNION
                                   
                                   SELECT    DISTINCT GTPM_GAMING_TABLE_SESSION_ID
                                , GTPM_ISO_CODE
                                , CS_OPENING_DATE
                                , ISNULL(CS_CLOSING_DATE, GETDATE())
                           FROM  GT_PLAYERTRACKING_MOVEMENTS 
                          INNER  JOIN GAMING_TABLES_SESSIONS ON GTPM_GAMING_TABLE_SESSION_ID = GTS_GAMING_TABLE_SESSION_ID
                          INNER  JOIN CASHIER_SESSIONS ON CS_SESSION_ID = GTS_CASHIER_SESSION_ID
                          WHERE  ((@pGamingTableSessionId IS NOT NULL AND GTPM_GAMING_TABLE_SESSION_ID = @pGamingTableSessionId OR @pGamingTableSessionId IS NULL)
                                                             OR  (@pGamingTableSessionId IS NULL AND CS_OPENING_DATE >= @pDateFrom AND ISNULL(CS_CLOSING_DATE, CS_OPENING_DATE) < @pDateTo))
                            AND  GTPM_TYPE IN (@pSaleChipsMov, @pValCopyDealerMov)
                            AND  (ISNULL(@pIsoCode,'') = '' OR @pIsoCode = GTPM_ISO_CODE)
                            AND  ( GTS_GAMING_TABLE_ID IN ( SELECT SST_VALUE FROM @_GAMING_TABLE ) OR LEN(ISNULL(@pGamingTablesId, '')) = 0 )


OPEN ISO_CODES_CURSOR   
  FETCH NEXT FROM ISO_CODES_CURSOR INTO @GTSessionId, @IsoCode, @OpeningDate, @ClosingDate

  WHILE @@FETCH_STATUS = 0   
  BEGIN   
    
    SET @Date = @pDateFrom

    WHILE @Date < @pDateTo
    BEGIN
    IF @Date >= CONVERT(NVARCHAR(13), @OpeningDate, 21) + ':00:00' AND @Date <= @ClosingDate
            BEGIN
              INSERT INTO @t_Hour_Iso
                     VALUES(@GTSessionId, CONVERT(NVARCHAR(13), @Date, 21) + ':00:00', @IsoCode)
            END
      SET @Date = DATEADD(HH, 1, @Date)
    END
      
    FETCH NEXT FROM ISO_CODES_CURSOR INTO @GTSessionId, @IsoCode, @OpeningDate, @ClosingDate  
  END  

  CLOSE ISO_CODES_CURSOR   
  DEALLOCATE ISO_CODES_CURSOR
  
   SELECT   TEMP_SESSION_ID
          , TEMP_DATETIME
          , TEMP_ISO_CODE
          , ISNULL(SUM(GTPM_VALUE), 0) AS DROP_AMOUNT
          , CS_NAME
          , GT_NAME
     FROM   @t_Hour_Iso
LEFT JOIN   GT_PLAYERTRACKING_MOVEMENTS ON TEMP_ISO_CODE = GTPM_ISO_CODE
                            AND TEMP_DATETIME = CONVERT(NVARCHAR(13), GTPM_DATETIME, 21) + ':00:00'
                            AND GTPM_GAMING_TABLE_SESSION_ID = TEMP_SESSION_ID
                            AND GTPM_TYPE IN (@pSaleChipsMov, @pValCopyDealerMov)
INNER JOIN  GAMING_TABLES_SESSIONS ON TEMP_SESSION_ID = GTS_GAMING_TABLE_SESSION_ID
INNER JOIN  CASHIER_SESSIONS ON GTS_CASHIER_SESSION_ID = CS_SESSION_ID
INNER JOIN  GAMING_TABLES ON GTS_GAMING_TABLE_ID = GT_GAMING_TABLE_ID
GROUP BY    TEMP_SESSION_ID
          , TEMP_DATETIME
          , TEMP_ISO_CODE                            
          , CS_NAME
          , GT_NAME
ORDER BY    TEMP_SESSION_ID
          , TEMP_DATETIME
          , TEMP_ISO_CODE
               
END -- END PROCEDURE
GO

-- PERMISSIONS
GRANT EXECUTE ON [dbo].[GT_HourlyDrop] TO [wggui] WITH GRANT OPTION
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GT_HourlyDrop]') AND type in (N'P', N'PC'))
   DROP PROCEDURE [dbo].[GT_HourlyDrop]
GO

/*
----------------------------------------------------------------------------------------------------------------
REPORT QUERY FOR CASHIER & GUI DROP HOURLY FOR GAMBLING TABLES

Version     Date          User              Description
----------------------------------------------------------------------------------------------------------------
1.0.0       26-APR-2017   JML&RAB&DHA       New procedure
1.0.1       05-MAY-2017   DHA               PBI 27175:Drop gaming table information - Global drop hourly report (Filter)
----------------------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[GT_HourlyDrop]
(
    @pGamingTableSessionId			BIGINT,
    @pDateFrom						DATETIME,
    @pDateTo				        DATETIME,
    @pIsoCode				        NVARCHAR(3),
    @pSaleChipsMov					INT,
    @pValCopyDealerMov				INT,
    @pRedeemableChipsType			INT,
    @pGamingTablesId				NVARCHAR(MAX)
)
AS
BEGIN

DECLARE @NationalCurrency NVARCHAR(3)
DECLARE @_DELIMITER       NVARCHAR(1)
DECLARE @_GAMING_TABLE    TABLE(SST_ID INT, SST_VALUE VARCHAR(50))

SET     @_DELIMITER = ','

INSERT INTO @_GAMING_TABLE SELECT * FROM dbo.SplitStringIntoTable(@pGamingTablesId, @_DELIMITER, DEFAULT)

SELECT @NationalCurrency = GP_KEY_VALUE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'RegionalOptions' AND  GP_SUBJECT_KEY = 'CurrencyISOCode'

DECLARE  @t_Hour_Iso 
  TABLE( TEMP_SESSION_ID BIGINT,
             TEMP_DATETIME DATETIME, 
         TEMP_ISO_CODE VARCHAR(3));  

IF @pGamingTableSessionId IS NOT NULL
BEGIN
            SELECT   @pDateFrom = CONVERT(DATETIME, CONVERT(NVARCHAR(13), CS_OPENING_DATE, 21) + ':00:00', 21)
                     , @pDateTo = ISNULL(CS_CLOSING_DATE, GETDATE())
              FROM   GAMING_TABLES_SESSIONS 
INNER JOIN   CASHIER_SESSIONS ON CS_SESSION_ID = GTS_CASHIER_SESSION_ID
            WHERE   GTS_GAMING_TABLE_SESSION_ID = @pGamingTableSessionId
END
ELSE
BEGIN
      IF @pDateFrom IS NULL
            SET @pDateFrom = CONVERT(DATETIME, 0)
            
      IF @pDateTo IS NULL
            SET @pDateTo = GETDATE()
END

DECLARE @Date AS DATETIME
DECLARE @GTSessionId AS BIGINT
DECLARE @IsoCode AS NVARCHAR(3)
DECLARE @OpeningDate AS DATETIME
DECLARE @ClosingDate AS DATETIME

DECLARE ISO_CODES_CURSOR CURSOR FOR
                        SELECT    DISTINCT GTS_GAMING_TABLE_SESSION_ID
                                , ISNULL(GTSC_ISO_CODE, @NationalCurrency)
                                , CS_OPENING_DATE
                                , ISNULL(CS_CLOSING_DATE, GETDATE())
                          FROM GAMING_TABLES_SESSIONS 
                          INNER  JOIN CASHIER_SESSIONS ON CS_SESSION_ID = GTS_CASHIER_SESSION_ID
                          LEFT   JOIN GAMING_TABLES_SESSIONS_BY_CURRENCY ON GTSC_GAMING_TABLE_SESSION_ID = GTS_GAMING_TABLE_SESSION_ID
                          WHERE  ((@pGamingTableSessionId IS NOT NULL AND GTS_GAMING_TABLE_SESSION_ID = @pGamingTableSessionId)
                                                             OR  (@pGamingTableSessionId IS NULL AND ISNULL(CS_CLOSING_DATE, CS_OPENING_DATE) < @pDateTo))
                            AND  (ISNULL(@pIsoCode,'') = '' OR @pIsoCode = GTSC_ISO_CODE) 
                            AND  ISNULL(GTSC_TYPE, @pRedeemableChipsType) = @pRedeemableChipsType
                            AND  ( GTS_GAMING_TABLE_ID IN ( SELECT SST_VALUE FROM @_GAMING_TABLE ) OR LEN(@pGamingTablesId) = 0 )
                                               
                                   UNION
                                   
                                   SELECT    DISTINCT GTPM_GAMING_TABLE_SESSION_ID
                                , GTPM_ISO_CODE
                                , CS_OPENING_DATE
                                , ISNULL(CS_CLOSING_DATE, GETDATE())
                           FROM  GT_PLAYERTRACKING_MOVEMENTS 
                          INNER  JOIN GAMING_TABLES_SESSIONS ON GTPM_GAMING_TABLE_SESSION_ID = GTS_GAMING_TABLE_SESSION_ID
                          INNER  JOIN CASHIER_SESSIONS ON CS_SESSION_ID = GTS_CASHIER_SESSION_ID
                          WHERE  ((@pGamingTableSessionId IS NOT NULL AND GTPM_GAMING_TABLE_SESSION_ID = @pGamingTableSessionId OR @pGamingTableSessionId IS NULL)
                                                             OR  (@pGamingTableSessionId IS NULL AND CS_OPENING_DATE >= @pDateFrom AND ISNULL(CS_CLOSING_DATE, CS_OPENING_DATE) < @pDateTo))
                            AND  GTPM_TYPE IN (@pSaleChipsMov, @pValCopyDealerMov)
                            AND  (ISNULL(@pIsoCode,'') = '' OR @pIsoCode = GTPM_ISO_CODE)
                            AND  ( GTS_GAMING_TABLE_ID IN ( SELECT SST_VALUE FROM @_GAMING_TABLE ) OR LEN(ISNULL(@pGamingTablesId, '')) = 0 )


OPEN ISO_CODES_CURSOR   
  FETCH NEXT FROM ISO_CODES_CURSOR INTO @GTSessionId, @IsoCode, @OpeningDate, @ClosingDate

  WHILE @@FETCH_STATUS = 0   
  BEGIN   
    
    SET @Date = @pDateFrom
    
    IF (@OpeningDate > @pDateFrom)
		SET @Date = @OpeningDate
    
    WHILE @Date < @ClosingDate AND @Date < @pDateTo
    BEGIN
    IF @Date >= CONVERT(NVARCHAR(13), @OpeningDate, 21) + ':00:00' AND @Date <= @ClosingDate
            BEGIN
              INSERT INTO @t_Hour_Iso
                     VALUES(@GTSessionId, CONVERT(NVARCHAR(13), @Date, 21) + ':00:00', @IsoCode)
            END
      SET @Date = DATEADD(HH, 1, @Date)
    END
      
    FETCH NEXT FROM ISO_CODES_CURSOR INTO @GTSessionId, @IsoCode, @OpeningDate, @ClosingDate  
  END  

  CLOSE ISO_CODES_CURSOR   
  DEALLOCATE ISO_CODES_CURSOR
  
   SELECT   TEMP_SESSION_ID
          , TEMP_DATETIME
          , TEMP_ISO_CODE
          , ISNULL(SUM(GTPM_VALUE), 0) AS DROP_AMOUNT
          , CS_NAME
          , GT_NAME
     FROM   @t_Hour_Iso
LEFT JOIN   GT_PLAYERTRACKING_MOVEMENTS ON TEMP_ISO_CODE = GTPM_ISO_CODE
                            AND TEMP_DATETIME = CONVERT(NVARCHAR(13), GTPM_DATETIME, 21) + ':00:00'
                            AND GTPM_GAMING_TABLE_SESSION_ID = TEMP_SESSION_ID
                            AND GTPM_TYPE IN (@pSaleChipsMov, @pValCopyDealerMov)
INNER JOIN  GAMING_TABLES_SESSIONS ON TEMP_SESSION_ID = GTS_GAMING_TABLE_SESSION_ID
INNER JOIN  CASHIER_SESSIONS ON GTS_CASHIER_SESSION_ID = CS_SESSION_ID
INNER JOIN  GAMING_TABLES ON GTS_GAMING_TABLE_ID = GT_GAMING_TABLE_ID
GROUP BY    TEMP_SESSION_ID
          , TEMP_DATETIME
          , TEMP_ISO_CODE                            
          , CS_NAME
          , GT_NAME
ORDER BY    TEMP_SESSION_ID
          , TEMP_DATETIME
          , TEMP_ISO_CODE
               
END -- END PROCEDURE
GO

-- PERMISSIONS
GRANT EXECUTE ON [dbo].[GT_HourlyDrop] TO [wggui] WITH GRANT OPTION
GO

/******* TRIGGERS *******/




DECLARE @CurrentCurrency AS VARCHAR(3)
DECLARE @NationalCurrency AS VARCHAR(3)

DECLARE @Allowed AS BIT
DECLARE Curs_Currencies CURSOR FOR SELECT CGC_ISO_CODE FROM CAGE_CURRENCIES GROUP BY CGC_ISO_CODE,CGC_ALLOWED

SELECT @NationalCurrency = gp_key_value FROM general_params 
WHERE gp_group_key = 'RegionalOptions' and gp_subject_key = 'CurrencyISOCode'
SET @NationalCurrency = ISNULL(@NationalCurrency, 'NULL')
SET @Allowed = 0
OPEN Curs_Currencies
FETCH NEXT FROM Curs_Currencies INTO @CurrentCurrency
WHILE @@FETCH_STATUS = 0
   BEGIN
      IF NOT EXISTS (SELECT * FROM CAGE_CURRENCIES WHERE CGC_DENOMINATION = -7 AND CGC_ISO_CODE = @CurrentCurrency) 
      BEGIN
		 IF EXISTS (select 1 where @CurrentCurrency = @NationalCurrency)
			SET @Allowed = 1
        INSERT INTO CAGE_CURRENCIES (CGC_ISO_CODE , CGC_DENOMINATION , CGC_ALLOWED, CGC_CAGE_CURRENCY_TYPE,CGC_CAGE_VISIBLE) VALUES (@CurrentCurrency , -7 , @Allowed, 0,0) -- -7 reserved for Credit Lines
        
        INSERT INTO CURRENCY_EXCHANGE(CE_TYPE,CE_CURRENCY_ISO_CODE,CE_DESCRIPTION,CE_CHANGE,CE_NUM_DECIMALS,CE_VARIABLE_COMMISSION,CE_FIXED_COMMISSION,CE_VARIABLE_NR2,CE_FIXED_NR2,CE_STATUS,CE_FORMATTED_DECIMALS,CE_LANGUAGE_RESOURCES,CE_CONFIGURATION,CE_CURRENCY_ORDER)
			VALUES
							(7,@CurrentCurrency,'Credit Line',1.00000000,2,0.00,0.00,0.00,0.00,@Allowed,NULL,NULL,'<configuration><data><type>0</type><scope>0</scope><fixed_commission>0.00</fixed_commission><variable_commission>0.00</variable_commission><fixed_nr2>0.00</fixed_nr2><variable_nr2>0.00</variable_nr2>  </data></configuration>',NULL)
							
	  END
	  set @Allowed = 0
      FETCH NEXT FROM Curs_Currencies INTO @CurrentCurrency
   END
CLOSE Curs_Currencies
DEALLOCATE Curs_Currencies
GO
