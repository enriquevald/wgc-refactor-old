/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 249;

SET @New_ReleaseId = 250;
SET @New_ScriptName = N'UpdateTo_18.250.037.sql';
SET @New_Description = N'New GP for EGM-GameTheme.Name, updated SP CompareMeterBonus.';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/******* TABLES *******/

/******* INDEXES *******/

/******* RECORDS *******/

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'EGM' AND GP_SUBJECT_KEY = 'GameTheme.Name')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('EGM', 'GameTheme.Name', '')

IF NOT EXISTS ( SELECT 1 FROM general_params WHERE gp_group_key = 'ManualPromotions' AND gp_subject_key = 'LastHoursToGetRecharges')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('ManualPromotions', 'LastHoursToGetRecharges', '24')

GO  

/******* STORED PROCEDURES *******/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CompareMeterBonus]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].CompareMeterBonus
GO

CREATE PROCEDURE [dbo].CompareMeterBonus (
  @pFrom                DATETIME,
  @pTo                  DATETIME,
  @pExcludeNoActivity   BIT = 0,
  @pOnlyMismatch        BIT = 0,
  @pSqlProviderIdList   NVARCHAR(4000) = NULL, 
  @pOrderBy             NVARCHAR(4000) = NULL )
AS
BEGIN

DECLARE @_QUERY                  NVARCHAR(MAX)
DECLARE @_transferred_datetime   NVARCHAR(256)
DECLARE @_tsmh_datetime          NVARCHAR(256)
DECLARE @_declare_from           NVARCHAR(256)
DECLARE @_declare_to             NVARCHAR(256)

SET @_transferred_datetime = ''
SET @_tsmh_datetime = ''
SET @_declare_from = ''
SET @_declare_to = ''

IF @pFrom IS NOT NULL
BEGIN
  SET @_transferred_datetime = @_transferred_datetime + ' AND   BNS_TRANSFERRED_DATETIME >= @_FROM '
  SET @_tsmh_datetime        = @_tsmh_datetime        + ' AND   TSMH_DATETIME            >= @_FROM '
  SET @_declare_from         = ' SET @_FROM = ''' + CAST(@pFrom AS NVARCHAR) + ''' '
END
IF @pTo IS NOT NULL
BEGIN
  SET @_transferred_datetime = @_transferred_datetime + ' AND   BNS_TRANSFERRED_DATETIME <  @_TO '
  SET @_tsmh_datetime        = @_tsmh_datetime        + ' AND   TSMH_DATETIME            <  @_TO '
  SET @_declare_to           = ' SET @_TO = ''' + CAST(@pTo AS NVARCHAR) + ''' '
END

SET @_QUERY = '
DECLARE @_FROM                   DATETIME
DECLARE @_TO                     DATETIME
DECLARE @_EXCLUDE_NO_ACTIVITY    BIT
DECLARE @_ONLY_MISMATCH          BIT
DECLARE @_SQL_PROVIDER_ID_LIST   NVARCHAR(4000)

' + @_declare_from + @_declare_to + '
SET @_EXCLUDE_NO_ACTIVITY  = '   + CAST(@pExcludeNoActivity AS NVARCHAR) + '
SET @_ONLY_MISMATCH        = '   + CAST(@pOnlyMismatch      AS NVARCHAR) + '

SELECT   TE_TERMINAL_ID
       , TE_PROVIDER_ID
       , TE_NAME
       , BNS_AWARDED
       , BNS_IN_DOUBT
       , BNS_TOTAL
       , METER_1E
       , METER_21
       , METER_TOTAL
       , DIFF
       , CASE WHEN BNS_TOTAL <> 0 THEN ROUND(100 * DIFF/BNS_TOTAL, 2) ELSE CASE WHEN DIFF <> 0 THEN -100 ELSE NULL END END DIFF_PCT  
       , BNS_TRANSFERRED_DATETIME
 FROM
      (
         SELECT   TE_TERMINAL_ID
                , TE_PROVIDER_ID
                , TE_NAME
                , BNS_AWARDED
                , BNS_IN_DOUBT
                , ISNULL(BNS_AWARDED,0)+ISNULL(BNS_IN_DOUBT,0) BNS_TOTAL
                , METER_1E
                , METER_21
                , ISNULL(METER_1E,0)+ISNULL(METER_21,0) METER_TOTAL  
                , ISNULL(BNS_AWARDED,0)+ISNULL(BNS_IN_DOUBT,0) - (ISNULL(METER_1E,0)+ISNULL(METER_21,0)) DIFF  
                , CASE WHEN (ISNULL(BNS_AWARDED,0) + ISNULL(BNS_IN_DOUBT,0) + ISNULL(METER_1E,0) + ISNULL(METER_21,0)) <> 0 THEN 1 ELSE 0 END ACTIVITY
                , BNS_TRANSFERRED_DATETIME  
           FROM   TERMINALS
      LEFT JOIN 
                (
                  SELECT   ISNULL(BNS_TERMINAL_ID, METER_TERMINAL_ID) TERMINAL_ID
                         , BNS_AWARDED
                         , BNS_IN_DOUBT
                         , METER_1E / 100.0 AS METER_1E
                         , METER_21 / 100.0 AS METER_21
                         , BNS_TRANSFERRED_DATETIME
                    FROM
                         (
                           SELECT   BNS_TARGET_TERMINAL_ID		BNS_TERMINAL_ID
	                               , SUM ( CASE WHEN BNS_TRANSFER_STATUS = 10 THEN (ISNULL(BNS_TRANSFERRED_RE,0) + ISNULL(BNS_TRANSFERRED_PROMO_RE,0) + ISNULL(BNS_TRANSFERRED_PROMO_NR,0)) ELSE 0 END ) BNS_AWARDED
	                               , SUM ( CASE WHEN BNS_TRANSFER_STATUS =  3 THEN (ISNULL(BNS_TRANSFERRED_RE,0) + ISNULL(BNS_TRANSFERRED_PROMO_RE,0) + ISNULL(BNS_TRANSFERRED_PROMO_NR,0)) ELSE 0 END ) BNS_IN_DOUBT
	                               , BNS_TRANSFERRED_DATETIME
                             FROM   BONUSES
                            WHERE   BNS_TRANSFER_STATUS      IN (3, 10) ' + @_transferred_datetime + '
                         GROUP BY   BNS_TARGET_TERMINAL_ID, BNS_TRANSFERRED_DATETIME
                         ) BONUS
         FULL OUTER JOIN 
                         (
                           SELECT   TSMH_TERMINAL_ID														  METER_TERMINAL_ID	
                                  , SUM(CASE WHEN TSMH_METER_CODE = 30 THEN ISNULL(TSMH_METER_INCREMENT, 0) ELSE 0 END) METER_1E
                                  , SUM(CASE WHEN TSMH_METER_CODE = 33 THEN ISNULL(TSMH_METER_INCREMENT, 0) ELSE 0 END) METER_21
                             FROM   TERMINAL_SAS_METERS_HISTORY 
                            WHERE   TSMH_METER_CODE IN (30, 33) AND TSMH_TYPE = 1 ' + @_tsmh_datetime + '
                         GROUP BY   TSMH_TERMINAL_ID
                         ) METER ON BONUS.BNS_TERMINAL_ID = METER.METER_TERMINAL_ID 
                ) BONUS_CMP ON TE_TERMINAL_ID = BONUS_CMP.TERMINAL_ID 
                           AND TE_TERMINAL_TYPE = 5

) XXX

WHERE (@_EXCLUDE_NO_ACTIVITY  = 0      OR (@_EXCLUDE_NO_ACTIVITY = 1 AND ACTIVITY = 1) )
  AND (@_ONLY_MISMATCH        = 0      OR (@_ONLY_MISMATCH       = 1 AND DIFF <>    0) ) 
'

IF @pSqlProviderIdList IS NOT NULL
 BEGIN
   SET @_QUERY = @_QUERY + '
   AND (TE_TERMINAL_ID IN ' + @pSqlProviderIdList + ' )
   '
 END
IF @pOrderBy IS NOT NULL
	BEGIN
		SET @_QUERY = @_QUERY + ' ORDER BY ' + @pOrderBy
	END
ELSE
	SET @_QUERY = @_QUERY + '  
	   ORDER BY TE_PROVIDER_ID ASC, TE_NAME ASC
	'

EXEC (@_QUERY)

END
GO

GRANT EXECUTE ON [dbo].CompareMeterBonus TO [wggui] WITH GRANT OPTION
GO
