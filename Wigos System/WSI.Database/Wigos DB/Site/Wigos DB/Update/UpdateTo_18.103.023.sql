/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 102;

SET @New_ReleaseId = 103;
SET @New_ScriptName = N'UpdateTo_18.103.023.sql';
SET @New_Description = N'Extended search, player redeem permission, inactivity user lock, new terminal properties, liabilities.';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/

ALTER TABLE dbo.terminals ADD
	te_serial_number nvarchar(50) NULL,
	te_cabinet_type nvarchar(50) NULL,
	te_jackpot_contribution_pct numeric(7, 4) NULL
GO

CREATE TABLE [dbo].[hourly_liabilities](
	[hlb_datetime] [datetime] NOT NULL,
	[hlb_re_balance] [money] NOT NULL,
	[hlb_promo_re_balance] [money] NOT NULL,
	[hlb_promo_nr_balance] [money] NOT NULL,
	[hlb_in_session_re_to_gm] [money] NOT NULL,
	[hlb_in_session_promo_re_to_gm] [money] NOT NULL,
	[hlb_in_session_promo_nr_to_gm] [money] NOT NULL,
	[hlb_points] [money] NOT NULL,
	[hlb_num_accounts] [int] NOT NULL,
	[hlb_num_accounts_in_session] [int] NOT NULL,
 CONSTRAINT [PK_hourly_liabilities] PRIMARY KEY CLUSTERED 
(
	[hlb_datetime] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE dbo.accounts ADD
	ac_points_status int NULL
GO

-- For personal accounts, set POINTS_STATUS = 0. 
UPDATE   ACCOUNTS
   SET   AC_POINTS_STATUS = 0
 WHERE   AC_USER_TYPE     = 1

-- For inactivity process, it is needed to have a date in last activity for all users.
UPDATE GUI_USERS
   SET GU_LAST_ACTIVITY  = GETDATE()
 WHERE GU_USER_TYPE	     = 0
   AND GU_LAST_ACTIVITY IS NULL
   AND GU_BLOCK_REASON	 = 0   

/****** RECORDS ******/

INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Account.ExtendedSearch', 'BirthDate', '0')
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Account.ExtendedSearch', 'WeddingDate', '0')
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Account.ExtendedSearch', 'PhoneNumber', '0')

