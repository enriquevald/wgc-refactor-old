/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 220;

SET @New_ReleaseId = 221;
SET @New_ScriptName = N'UpdateTo_18.221.037.sql';
SET @New_Description = N'Added column: account payment order - name4; Migrate payment order ids from doc description to doc id';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/******* TABLES *******/

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[account_payment_orders]') and name = 'apo_player_name4')
  ALTER TABLE [dbo].[account_payment_orders] ADD [apo_player_name4] [nvarchar](50) NULL 
GO

/******* INDEXES *******/

/******* RECORDS *******/

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'GamingTable.PlayerTracking' AND GP_SUBJECT_KEY ='ShowMsgWhenValidateValues')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key]
             ,[gp_subject_key]
             ,[gp_key_value])
       VALUES
             ('GamingTable.PlayerTracking'
             ,'ShowMsgWhenValidateValues'
             ,'1');
GO

/******* STORED PROCEDURES *******/


/* Migrate payment order ids from doc description to doc id */

/*
DOCUMENTS 1
*/

DECLARE @_general_param_value AS VARCHAR(100)
DECLARE @_document_name AS VARCHAR(100)
DECLARE @_document_id AS VARCHAR(MAX)
DECLARE @_result AS VARCHAR(MAX)
DECLARE @_language AS VARCHAR(3)

SET @_result = ''

SELECT
  @_language = GP_KEY_VALUE
FROM GENERAL_PARAMS
WHERE GP_GROUP_KEY = 'RegionalOptions' AND GP_SUBJECT_KEY = 'CountryISOCode2'

IF @_language IS NULL OR RTRIM(@_language)=''
SET @_language = 'MX'

SELECT
  @_general_param_value = GP_KEY_VALUE
FROM GENERAL_PARAMS
WHERE GP_GROUP_KEY = 'PaymentOrder' AND GP_SUBJECT_KEY = 'AllowedDocuments1'

DECLARE _document_cursor CURSOR
FOR SELECT
  SST_VALUE
FROM DBO.SPLITSTRINGINTOTABLE(@_general_param_value, ';', DEFAULT)

OPEN _document_cursor

FETCH NEXT FROM _document_cursor
INTO @_document_name

WHILE @@FETCH_STATUS = 0
BEGIN
    SELECT @_document_id = (
      SELECT RIGHT('000' + CONVERT(varchar, IDT_ID), 3)
      FROM IDENTIFICATION_TYPES
      WHERE IDT_NAME = @_document_name AND IDT_COUNTRY_ISO_CODE2 = @_language)
      
        IF @_document_id IS NOT NULL
      SET @_result = @_result + ',' + @_document_id

        FETCH NEXT FROM _document_cursor
        INTO @_document_name
END

CLOSE _document_cursor
DEALLOCATE _document_cursor

IF @_result IS NOT NULL
  UPDATE GENERAL_PARAMS
  SET GP_KEY_VALUE = SUBSTRING(@_result, 2, LEN(@_result))
  WHERE GP_GROUP_KEY = 'PaymentOrder' AND GP_SUBJECT_KEY = 'AllowedDocuments1'

/*
DOCUMENTS 2
*/

SET @_general_param_value = ''
SET @_document_name = ''
SET @_document_id = ''
SET @_result = ''

SELECT
  @_general_param_value = GP_KEY_VALUE
FROM GENERAL_PARAMS
WHERE GP_GROUP_KEY = 'PaymentOrder' AND GP_SUBJECT_KEY = 'AllowedDocuments2'

DECLARE _document_cursor CURSOR
FOR SELECT
  SST_VALUE
FROM DBO.SPLITSTRINGINTOTABLE(@_general_param_value, ';', DEFAULT)

OPEN _document_cursor

FETCH NEXT FROM _document_cursor
INTO @_document_name

WHILE @@FETCH_STATUS = 0
BEGIN
  SELECT
    @_document_id = (
    SELECT RIGHT('000' + CONVERT(varchar, IDT_ID), 3)
    FROM IDENTIFICATION_TYPES
    WHERE IDT_NAME = @_document_name AND IDT_COUNTRY_ISO_CODE2 = @_language)
      
      IF @_document_id IS NOT NULL
    SET @_result = @_result + ',' + @_document_id

      FETCH NEXT FROM _document_cursor
      INTO @_document_name
END

CLOSE _document_cursor
DEALLOCATE _document_cursor

IF @_result IS NOT NULL
  UPDATE GENERAL_PARAMS
  SET GP_KEY_VALUE = SUBSTRING(@_result, 2, LEN(@_result))
  WHERE GP_GROUP_KEY = 'PaymentOrder' AND GP_SUBJECT_KEY = 'AllowedDocuments2'

GO
