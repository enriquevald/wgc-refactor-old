/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 341;

SET @New_ReleaseId = 342;
SET @New_ScriptName = N'UpdateTo_18.342.041.sql';
SET @New_Description = N'RedeemWithoutPlay'; 

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/**** GENERAL PARAM *****/

IF  EXISTS(SELECT 1 
				FROM GENERAL_PARAMS 
				WHERE GP_GROUP_KEY = 'Cashier.TotalRedeem' 
				AND GP_SUBJECT_KEY = 'RedeemWthoutPlay.Enabled'
			 )
BEGIN
				DELETE 
				FROM GENERAL_PARAMS 
				WHERE GP_GROUP_KEY = 'Cashier.TotalRedeem' 
				AND GP_SUBJECT_KEY = 'RedeemWthoutPlay.Enabled'
END

IF NOT EXISTS(SELECT 1 
				FROM GENERAL_PARAMS 
				WHERE GP_GROUP_KEY = 'Cashier.TotalRedeem' 
				AND GP_SUBJECT_KEY = 'RedeemWithoutPlay.Enabled'
			 )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('Cashier.TotalRedeem', 'RedeemWithoutPlay.Enabled', '0')
END

/******* TABLES  *******/


/******* INDEXES *******/


/******* RECORDS *******/

/*** Terminals ***/
IF NOT EXISTS(SELECT * FROM TERMINALS WHERE te_base_name = 'SORTEADOR MESAS')
BEGIN
	INSERT INTO TERMINALS
	(
	TE_TYPE,
	TE_SERVER_ID,
	TE_EXTERNAL_ID,
	TE_BLOCKED,
	TE_ACTIVE,
	TE_PROVIDER_ID,
	TE_CLIENT_ID,
	TE_BUILD_ID,
	TE_TERMINAL_TYPE,
	TE_VENDOR_ID,
	TE_STATUS,
	TE_RETIREMENT_DATE,
	TE_RETIREMENT_REQUESTED,
	TE_DENOMINATION,
	TE_MULTI_DENOMINATION,
	TE_PROGRAM,
	TE_THEORETICAL_PAYOUT,
	TE_PROV_ID,
	TE_BANK_ID,
	TE_FLOOR_ID,
	TE_GAME_TYPE,
	TE_ACTIVATION_DATE,
	TE_CURRENT_ACCOUNT_ID,
	TE_CURRENT_PLAY_SESSION_ID,
	TE_REGISTRATION_CODE,
	TE_SAS_FLAGS,
	TE_SERIAL_NUMBER,
	TE_CABINET_TYPE,
	TE_JACKPOT_CONTRIBUTION_PCT,
	TE_CONTRACT_TYPE,
	TE_CONTRACT_ID,
	TE_ORDER_NUMBER,
	TE_WXP_REPORTED,
	TE_WXP_REPORTED_STATUS_DATETIME,
	TE_WXP_REPORTED_STATUS,
	TE_SEQUENCE_ID,
	TE_VALIDATION_TYPE,
	TE_ALLOWED_CASHABLE_EMISSION,
	TE_ALLOWED_PROMO_EMISSION,
	TE_ALLOWED_REDEMPTION,
	TE_MAX_ALLOWED_TI,
	TE_MAX_ALLOWED_TO,
	TE_SAS_VERSION,
	TE_SAS_MACHINE_NAME,
	TE_BONUS_FLAGS,
	TE_FEATURES_BYTES,
	TE_VIRTUAL_ACCOUNT_ID,
	TE_SAS_FLAGS_USE_SITE_DEFAULT,
	TE_AUTHENTICATION_METHOD,
	TE_AUTHENTICATION_SEED,
	TE_AUTHENTICATION_SIGNATURE,
	TE_AUTHENTICATION_STATUS,
	TE_AUTHENTICATION_LAST_CHECKED,
	TE_MACHINE_ID,
	TE_POSITION,
	TE_TOP_AWARD,
	TE_MAX_BET,
	TE_NUMBER_LINES,
	TE_GAME_THEME,
	TE_ACCOUNT_PROMOTION_ID,
	TE_MASTER_ID,
	TE_CHANGE_ID,
	TE_BASE_NAME,
	TE_MACHINE_ASSET_NUMBER,
	TE_ASSET_NUMBER,
	TE_MACHINE_SERIAL_NUMBER,
	TE_METER_DELTA_ID,
	TE_LAST_GAME_PLAYED_ID,
	TE_TRANSFER_STATUS,
	TE_SMIB2EGM_COMM_TYPE,
	TE_SMIB2EGM_CONF_ID,
	TE_BRAND_CODE,
	TE_MODEL,
	TE_MANUFACTURE_YEAR,
	TE_MET_HOMOLOGATED,
	TE_BET_CODE,
	TE_COIN_COLLECTION,
	TE_EQUITY_PERCENTAGE,
	TE_TERMINAL_CURRENCY_ID,
	TE_ISO_CODE,
	TE_SAS_ACCOUNTING_DENOM,
	TE_TITO_HOST_ID)
		SELECT TOP  1 
								TE_TYPE,
								TE_SERVER_ID,
								TE_EXTERNAL_ID,
								TE_BLOCKED,
								TE_ACTIVE,
								TE_PROVIDER_ID,
								TE_CLIENT_ID,
								TE_BUILD_ID,
								111,
								TE_VENDOR_ID,
								TE_STATUS,
								TE_RETIREMENT_DATE,
								TE_RETIREMENT_REQUESTED,
								TE_DENOMINATION,
								TE_MULTI_DENOMINATION,
								TE_PROGRAM,
								TE_THEORETICAL_PAYOUT,
								TE_PROV_ID,
								TE_BANK_ID,
								TE_FLOOR_ID,
								TE_GAME_TYPE,
								TE_ACTIVATION_DATE,
								TE_CURRENT_ACCOUNT_ID,
								TE_CURRENT_PLAY_SESSION_ID,
								TE_REGISTRATION_CODE,
								TE_SAS_FLAGS,
								TE_SERIAL_NUMBER,
								TE_CABINET_TYPE,
								TE_JACKPOT_CONTRIBUTION_PCT,
								TE_CONTRACT_TYPE,
								TE_CONTRACT_ID,
								TE_ORDER_NUMBER,
								TE_WXP_REPORTED,
								TE_WXP_REPORTED_STATUS_DATETIME,
								TE_WXP_REPORTED_STATUS,
								TE_SEQUENCE_ID,
								TE_VALIDATION_TYPE,
								TE_ALLOWED_CASHABLE_EMISSION,
								TE_ALLOWED_PROMO_EMISSION,
								TE_ALLOWED_REDEMPTION,
								TE_MAX_ALLOWED_TI,
								TE_MAX_ALLOWED_TO,
								TE_SAS_VERSION,
								TE_SAS_MACHINE_NAME,
								TE_BONUS_FLAGS,
								TE_FEATURES_BYTES,
								TE_VIRTUAL_ACCOUNT_ID,
								TE_SAS_FLAGS_USE_SITE_DEFAULT,
								TE_AUTHENTICATION_METHOD,
								TE_AUTHENTICATION_SEED,
								TE_AUTHENTICATION_SIGNATURE,
								TE_AUTHENTICATION_STATUS,
								TE_AUTHENTICATION_LAST_CHECKED,
								TE_MACHINE_ID,
								TE_POSITION,
								TE_TOP_AWARD,
								TE_MAX_BET,
								TE_NUMBER_LINES,
								TE_GAME_THEME,
								TE_ACCOUNT_PROMOTION_ID,
								TE_MASTER_ID,
								TE_CHANGE_ID,
								'SORTEADOR MESAS',
								TE_MACHINE_ASSET_NUMBER,
								TE_ASSET_NUMBER,
								TE_MACHINE_SERIAL_NUMBER,
								TE_METER_DELTA_ID,
								TE_LAST_GAME_PLAYED_ID,
								TE_TRANSFER_STATUS,
								TE_SMIB2EGM_COMM_TYPE,
								TE_SMIB2EGM_CONF_ID,
								TE_BRAND_CODE,
								TE_MODEL,
								TE_MANUFACTURE_YEAR,
								TE_MET_HOMOLOGATED,
								TE_BET_CODE,
								TE_COIN_COLLECTION,
								TE_EQUITY_PERCENTAGE,
								TE_TERMINAL_CURRENCY_ID,
								TE_ISO_CODE,
								TE_SAS_ACCOUNTING_DENOM,
								TE_TITO_HOST_ID
		FROM TERMINALS 
		WHERE TE_TERMINAL_TYPE = 106

END
GO
/******* PROCEDURES *******/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ReportCashDeskDraws]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ReportCashDeskDraws]
GO

/****** Object:  StoredProcedure [dbo].[ReportCashDeskDraws]    Script Date: 10/10/2016 16:36:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[ReportCashDeskDraws]  
  @pDrawFrom	    DATETIME      = NULL,
  @pDrawTo		    DATETIME      = NULL,
  @pDrawId		    BIGINT		  =	NULL,
  @pCashierId       INT			  = NULL,
  @pUserId          INT			  = NULL,
  @pSqlAccount      NVARCHAR(MAX) = NULL,
  @pCashierMovement INT	          = NULL,
  @pCashDeskDraw    INT           = 1,
  @pGamingTableDraw INT           = 1

AS
BEGIN

	SET NOCOUNT ON;
	DECLARE @_Terminal_CashDesk_draw_type AS INT;
	DECLARE @_Terminal_GamingTable_draw_type AS INT;
	
	SET @_Terminal_CashDesk_draw_type = 106
	SET @_Terminal_GamingTable_draw_type = 111
	

    CREATE TABLE #ACCOUNTS_TEMP (AC_ACCOUNT_ID BIGINT NOT NULL, AC_HOLDER_NAME NVARCHAR(200), AC_TRACK_DATA NVARCHAR(50))
	IF @pSqlAccount IS NOT NULL
	BEGIN
	  INSERT INTO #ACCOUNTS_TEMP EXEC ('SELECT AC_ACCOUNT_ID, AC_HOLDER_NAME, AC_TRACK_DATA  FROM ACCOUNTS ' + @pSqlAccount)
		IF @@ROWCOUNT > 500
			ALTER TABLE #ACCOUNTS_TEMP ADD PRIMARY KEY NONCLUSTERED (AC_ACCOUNT_ID)
	END

	IF 	@pDrawId IS NOT NULL 
	--
	-- Filtering by Draw ID
	--
	BEGIN	
	  IF @pDrawFrom IS NULL SET @pDrawFrom = '2007-01-01T00:00:00'
		IF @pDrawTo   IS NULL SET @pDrawTo   = GETDATE ()
		
    --No filter by draw type
	  IF (@pCashDeskDraw = 1 AND @pGamingTableDraw = 1) OR (@pCashDeskDraw = 0 AND @pGamingTableDraw = 0)
	  BEGIN
		    SELECT DISTINCT
		        CD_DRAW_ID
		      , CD_DRAW_DATETIME
		      , TE_TERMINAL_TYPE
		      , CM_CASHIER_NAME		  
		      , CM_USER_NAME
		      , CD_OPERATION_ID		
		      , CM_DATE
		      , CD_ACCOUNT_ID
		      , AC_HOLDER_NAME
		      , CD_COMBINATION_BET
		      , CD_COMBINATION_WON
		      , CD_RE_BET
		      , CD_RE_WON
		      , CD_NR_WON
		      , CD_PRIZE_ID
		    FROM
		       CASHDESK_DRAWS
		       WITH(INDEX(IX_CD_DRAW_DATETIME_ACCOUNT))
		        INNER JOIN CASHIER_MOVEMENTS WITH(INDEX(IX_CM_OPERATION_ID)) ON CD_OPERATION_ID = CM_OPERATION_ID
		        INNER JOIN ACCOUNTS          ON CD_ACCOUNT_ID   = AC_ACCOUNT_ID
		        INNER JOIN TERMINALS         ON (CD_TERMINAL IS NULL) OR (CD_TERMINAL = TE_TERMINAL_ID)
		        
		    WHERE    
		        CD_DRAW_ID = @pDrawId	
		        AND CD_DRAW_DATETIME >= @pDrawFrom
		        AND CD_DRAW_DATETIME < @pDrawTo	
		        AND ((@pCashierId IS NULL) OR (CM_CASHIER_ID = @pCashierId))
		        AND ((@pUserId IS NULL) OR (CM_USER_ID = @pUserId))
		        AND ( (@pSqlAccount     IS NULL) OR (CD_ACCOUNT_ID IN ( SELECT AC_ACCOUNT_ID FROM #ACCOUNTS_TEMP ) ) )
		        AND ( CM_TYPE = @pCashierMovement )
		        AND    te_terminal_type IN (@_Terminal_CashDesk_draw_type, @_Terminal_GamingTable_draw_type)
		        
		    ORDER BY CD_DRAW_ID
		    DROP TABLE #ACCOUNTS_TEMP

		    RETURN
		END
    --Filter by CASH DESK draw
	  ELSE IF  @pCashDeskDraw = 1 AND @pGamingTableDraw = 0
	  BEGIN
		    SELECT DISTINCT
		        CD_DRAW_ID
		      , CD_DRAW_DATETIME
		      , TE_TERMINAL_TYPE
		      , CM_CASHIER_NAME		  
		      , CM_USER_NAME
		      , CD_OPERATION_ID		
		      , CM_DATE
		      , CD_ACCOUNT_ID
		      , AC_HOLDER_NAME
		      , CD_COMBINATION_BET
		      , CD_COMBINATION_WON
		      , CD_RE_BET
		      , CD_RE_WON
		      , CD_NR_WON
		      , CD_PRIZE_ID
		    FROM
		       CASHDESK_DRAWS
		       WITH(INDEX(IX_CD_DRAW_DATETIME_ACCOUNT))
		        INNER JOIN CASHIER_MOVEMENTS WITH(INDEX(IX_CM_OPERATION_ID)) ON CD_OPERATION_ID = CM_OPERATION_ID  
		        INNER JOIN ACCOUNTS   ON CD_ACCOUNT_ID   = AC_ACCOUNT_ID
		        INNER JOIN TERMINALS  ON CD_TERMINAL IS NULL OR CD_TERMINAL = TE_TERMINAL_ID
		        
		    WHERE    
		        CD_DRAW_ID = @pDrawId	
		        AND CD_DRAW_DATETIME >= @pDrawFrom
		        AND CD_DRAW_DATETIME < @pDrawTo	
		        AND ((@pCashierId IS NULL) OR (CM_CASHIER_ID = @pCashierId))
		        AND ((@pUserId IS NULL) OR (CM_USER_ID = @pUserId))
		        AND ( (@pSqlAccount     IS NULL) OR (CD_ACCOUNT_ID IN ( SELECT AC_ACCOUNT_ID FROM #ACCOUNTS_TEMP ) ) )
		        AND ( CM_TYPE = @pCashierMovement )
		        AND te_terminal_type = @_Terminal_CashDesk_draw_type
		    ORDER BY CD_DRAW_ID
		    DROP TABLE #ACCOUNTS_TEMP

		    RETURN		    	  
	  END
--Filter by GAMING TABLE draw
	  ELSE IF  @pCashDeskDraw = 0 AND @pGamingTableDraw = 1
	  BEGIN
		    SELECT DISTINCT
		        CD_DRAW_ID
		      , CD_DRAW_DATETIME
		      , TE_TERMINAL_TYPE
		      , CM_CASHIER_NAME		  
		      , CM_USER_NAME
		      , CD_OPERATION_ID		
		      , CM_DATE
		      , CD_ACCOUNT_ID
		      , AC_HOLDER_NAME
		      , CD_COMBINATION_BET
		      , CD_COMBINATION_WON
		      , CD_RE_BET
		      , CD_RE_WON
		      , CD_NR_WON
		      , CD_PRIZE_ID
		    FROM
		       CASHDESK_DRAWS
		       WITH(INDEX(IX_CD_DRAW_DATETIME_ACCOUNT))
		        INNER JOIN CASHIER_MOVEMENTS WITH(INDEX(IX_CM_OPERATION_ID)) ON CD_OPERATION_ID = CM_OPERATION_ID  
		        INNER JOIN ACCOUNTS   ON CD_ACCOUNT_ID   = AC_ACCOUNT_ID
		        INNER JOIN TERMINALS  ON CD_TERMINAL = TE_TERMINAL_ID
		        
		    WHERE    
		        CD_DRAW_ID = @pDrawId	
		        AND CD_DRAW_DATETIME >= @pDrawFrom
		        AND CD_DRAW_DATETIME < @pDrawTo	
		        AND ((@pCashierId IS NULL) OR (CM_CASHIER_ID = @pCashierId))
		        AND ((@pUserId IS NULL) OR (CM_USER_ID = @pUserId))
		        AND ( (@pSqlAccount     IS NULL) OR (CD_ACCOUNT_ID IN ( SELECT AC_ACCOUNT_ID FROM #ACCOUNTS_TEMP ) ) )
		        AND ( CM_TYPE = @pCashierMovement )
		        AND te_terminal_type = @_Terminal_GamingTable_draw_type
		    ORDER BY CD_DRAW_ID
		    DROP TABLE #ACCOUNTS_TEMP

		    RETURN		    	  
	  END	  
		
	END 	

	
	IF 	@pSqlAccount IS NOT NULL 
	--
	-- Filtering by account, but not by draw ID
	--
	BEGIN				
		IF @pDrawFrom IS NULL SET @pDrawFrom = '2007-01-01T00:00:00'
		IF @pDrawTo   IS NULL SET @pDrawTo   = GETDATE ()

	  --No filter by draw type
	  IF (@pCashDeskDraw = 1 AND @pGamingTableDraw = 1) OR (@pCashDeskDraw = 0 AND @pGamingTableDraw = 0)
	  BEGIN
		    SELECT DISTINCT
		        CD_DRAW_ID
		      , CD_DRAW_DATETIME
		      , TE_TERMINAL_TYPE
		      , CM_CASHIER_NAME		  
		      , CM_USER_NAME
		      , CD_OPERATION_ID		
		      , CM_DATE
		      , CD_ACCOUNT_ID
		      , AC_HOLDER_NAME
		      , CD_COMBINATION_BET
		      , CD_COMBINATION_WON
		      , CD_RE_BET
		      , CD_RE_WON
		      , CD_NR_WON
		      , CD_PRIZE_ID
		    FROM
		       CASHDESK_DRAWS
		       WITH(INDEX(IX_CD_ACCOUNT_DATETIME))
		        INNER JOIN CASHIER_MOVEMENTS WITH(INDEX(IX_CM_OPERATION_ID)) ON CD_OPERATION_ID = CM_OPERATION_ID 
		        INNER JOIN #ACCOUNTS_TEMP    ON CD_ACCOUNT_ID   = AC_ACCOUNT_ID
		        INNER JOIN TERMINALS         ON (CD_TERMINAL IS NULL) OR (CD_TERMINAL = TE_TERMINAL_ID)
		        
		    WHERE    
			    CD_DRAW_DATETIME >= @pDrawFrom
		       AND CD_DRAW_DATETIME < @pDrawTo 		   
		       AND ((@pCashierId IS NULL) OR (CM_CASHIER_ID = @pCashierId))
		       AND ((@pUserId IS NULL) OR (CM_USER_ID = @pUserId))
		       AND ( CM_TYPE = @pCashierMovement ) 
		       AND    te_terminal_type IN (@_Terminal_CashDesk_draw_type, @_Terminal_GamingTable_draw_type)
		       
		    ORDER BY CD_DRAW_ID
		    DROP TABLE #ACCOUNTS_TEMP

		    RETURN
		END
	  --Filter by CASH DESK draw
	  ELSE IF  @pCashDeskDraw = 1 AND @pGamingTableDraw = 0
	  BEGIN
          SELECT DISTINCT
		        CD_DRAW_ID
		      , CD_DRAW_DATETIME
		      , TE_TERMINAL_TYPE
		      , CM_CASHIER_NAME		  
		      , CM_USER_NAME
		      , CD_OPERATION_ID		
		      , CM_DATE
		      , CD_ACCOUNT_ID
		      , AC_HOLDER_NAME
		      , CD_COMBINATION_BET
		      , CD_COMBINATION_WON
		      , CD_RE_BET
		      , CD_RE_WON
		      , CD_NR_WON
		      , CD_PRIZE_ID
		    FROM
		       CASHDESK_DRAWS
		       WITH(INDEX(IX_CD_ACCOUNT_DATETIME))
		        INNER JOIN CASHIER_MOVEMENTS WITH(INDEX(IX_CM_OPERATION_ID)) ON CD_OPERATION_ID = CM_OPERATION_ID 
		        INNER JOIN #ACCOUNTS_TEMP    ON CD_ACCOUNT_ID   = AC_ACCOUNT_ID
		        INNER JOIN TERMINALS       ON CD_TERMINAL IS NULL OR CD_TERMINAL = TE_TERMINAL_ID
		        
		    WHERE    
			    CD_DRAW_DATETIME >= @pDrawFrom
		       AND CD_DRAW_DATETIME < @pDrawTo 		   
		       AND((@pCashierId IS NULL) OR (CM_CASHIER_ID = @pCashierId))
		       AND ((@pUserId IS NULL) OR (CM_USER_ID = @pUserId))
		       AND ( CM_TYPE = @pCashierMovement )
		       AND    te_terminal_type = @_Terminal_CashDesk_draw_type
		    ORDER BY CD_DRAW_ID
		    DROP TABLE #ACCOUNTS_TEMP
		    
		    RETURN
	  END
  --Filter by GAMING TABLE draw
	  ELSE IF  @pCashDeskDraw = 0 AND @pGamingTableDraw = 1
	  BEGIN
          SELECT DISTINCT
		        CD_DRAW_ID
		      , CD_DRAW_DATETIME
		      , TE_TERMINAL_TYPE
		      , CM_CASHIER_NAME		  
		      , CM_USER_NAME
		      , CD_OPERATION_ID		
		      , CM_DATE
		      , CD_ACCOUNT_ID
		      , AC_HOLDER_NAME
		      , CD_COMBINATION_BET
		      , CD_COMBINATION_WON
		      , CD_RE_BET
		      , CD_RE_WON
		      , CD_NR_WON
		      , CD_PRIZE_ID
		    FROM
		       CASHDESK_DRAWS
		       WITH(INDEX(IX_CD_ACCOUNT_DATETIME))
		        INNER JOIN CASHIER_MOVEMENTS WITH(INDEX(IX_CM_OPERATION_ID)) ON CD_OPERATION_ID = CM_OPERATION_ID 
		        INNER JOIN #ACCOUNTS_TEMP    ON CD_ACCOUNT_ID   = AC_ACCOUNT_ID
		        INNER JOIN TERMINALS       ON CD_TERMINAL = TE_TERMINAL_ID
		        
		    WHERE    
			    CD_DRAW_DATETIME >= @pDrawFrom
		       AND CD_DRAW_DATETIME < @pDrawTo 		   
		       AND((@pCashierId IS NULL) OR (CM_CASHIER_ID = @pCashierId))
		       AND ((@pUserId IS NULL) OR (CM_USER_ID = @pUserId))
		       AND ( CM_TYPE = @pCashierMovement )
		       AND    te_terminal_type = @_Terminal_GamingTable_draw_type
		    ORDER BY CD_DRAW_ID
		    DROP TABLE #ACCOUNTS_TEMP
		    
		    RETURN
	  END	  
	END 
		
	--
	-- Filter by Date, but not by account neither Draw ID
	--
	IF @pDrawFrom IS NULL SET @pDrawFrom = '2007-01-01T00:00:00'
	IF @pDrawTo   IS NULL SET @pDrawTo   = GETDATE ()
	
	--No filter by draw type
	IF (@pCashDeskDraw = 1 AND @pGamingTableDraw = 1) OR (@pCashDeskDraw = 0 AND @pGamingTableDraw = 0)
	BEGIN
	  SELECT DISTINCT
	      CD_DRAW_ID
	    , CD_DRAW_DATETIME
	    , TE_TERMINAL_TYPE
	    , CM_CASHIER_NAME		  
	    , CM_USER_NAME
	    , CD_OPERATION_ID		
	    , CM_DATE
	    , CD_ACCOUNT_ID
	    , AC_HOLDER_NAME
	    , CD_COMBINATION_BET
	    , CD_COMBINATION_WON
	    , CD_RE_BET
	    , CD_RE_WON
	    , CD_NR_WON
	    , CD_PRIZE_ID
	  FROM
	     CASHDESK_DRAWS WITH ( INDEX(IX_CD_DATETIME))	   
	      INNER JOIN CASHIER_MOVEMENTS WITH(INDEX(IX_CM_OPERATION_ID)) ON CD_OPERATION_ID = CM_OPERATION_ID 
	      INNER JOIN ACCOUNTS          ON CD_ACCOUNT_ID   = AC_ACCOUNT_ID
	      INNER JOIN TERMINALS         ON (CD_TERMINAL IS NULL) OR (CD_TERMINAL = TE_TERMINAL_ID)
  	  
	  WHERE     
		   CD_DRAW_DATETIME >= @pDrawFrom
	     AND 		CD_DRAW_DATETIME < @pDrawTo	     
	     AND 		((@pCashierId IS NULL) OR (CM_CASHIER_ID = @pCashierId))
	     AND 		((@pUserId IS NULL) OR (CM_USER_ID = @pUserId))
	     AND    ( CM_TYPE = @pCashierMovement )
	     AND    te_terminal_type IN (@_Terminal_CashDesk_draw_type, @_Terminal_GamingTable_draw_type)
	  ORDER BY CD_DRAW_ID
	  DROP TABLE #ACCOUNTS_TEMP
	  
	  RETURN
	END
	--Filter by CASH DESK draw
	ELSE IF  @pCashDeskDraw = 1 AND @pGamingTableDraw = 0
	BEGIN
      SELECT DISTINCT
	      CD_DRAW_ID
	    , CD_DRAW_DATETIME
	    , TE_TERMINAL_TYPE
	    , CM_CASHIER_NAME		  
	    , CM_USER_NAME
	    , CD_OPERATION_ID		
	    , CM_DATE
	    , CD_ACCOUNT_ID
	    , AC_HOLDER_NAME
	    , CD_COMBINATION_BET
	    , CD_COMBINATION_WON
	    , CD_RE_BET
	    , CD_RE_WON
	    , CD_NR_WON
	    , CD_PRIZE_ID
	  FROM
	     CASHDESK_DRAWS WITH ( INDEX(IX_CD_DATETIME))	   
	      INNER JOIN CASHIER_MOVEMENTS WITH(INDEX(IX_CM_OPERATION_ID)) ON CD_OPERATION_ID = CM_OPERATION_ID
	      INNER JOIN ACCOUNTS          ON CD_ACCOUNT_ID   = AC_ACCOUNT_ID
	      INNER JOIN TERMINALS       ON (CD_TERMINAL IS NULL) OR (CD_TERMINAL = TE_TERMINAL_ID)
  	  
	  WHERE     
		  CD_DRAW_DATETIME >= @pDrawFrom
	     AND 		CD_DRAW_DATETIME < @pDrawTo	     
	     AND 		((@pCashierId IS NULL) OR (CM_CASHIER_ID = @pCashierId))
	     AND 		((@pUserId IS NULL) OR (CM_USER_ID = @pUserId))
	     AND    ( CM_TYPE = @pCashierMovement ) 
	     AND    te_terminal_type = @_Terminal_CashDesk_draw_type
	  ORDER BY CD_DRAW_ID
	  DROP TABLE #ACCOUNTS_TEMP
	  
	  RETURN
	END
--Filter by GAMING TABLE draw
	ELSE IF  @pCashDeskDraw = 0 AND @pGamingTableDraw = 1
	BEGIN
      SELECT DISTINCT
	      CD_DRAW_ID
	    , CD_DRAW_DATETIME
	    , TE_TERMINAL_TYPE
	    , CM_CASHIER_NAME		  
	    , CM_USER_NAME
	    , CD_OPERATION_ID		
	    , CM_DATE
	    , CD_ACCOUNT_ID
	    , AC_HOLDER_NAME
	    , CD_COMBINATION_BET
	    , CD_COMBINATION_WON
	    , CD_RE_BET
	    , CD_RE_WON
	    , CD_NR_WON
	    , CD_PRIZE_ID
	  FROM
	     CASHDESK_DRAWS with ( INDEX(IX_CD_DATETIME))	   
	      INNER JOIN CASHIER_MOVEMENTS WITH(INDEX(IX_CM_OPERATION_ID)) ON CD_OPERATION_ID = CM_OPERATION_ID 
	      INNER JOIN ACCOUNTS          ON CD_ACCOUNT_ID   = AC_ACCOUNT_ID
	      INNER JOIN TERMINALS       ON CD_TERMINAL = TE_TERMINAL_ID
  	  
	  WHERE     
		  CD_DRAW_DATETIME >= @pDrawFrom
	     AND 		CD_DRAW_DATETIME < @pDrawTo	     
	     AND 		((@pCashierId IS NULL) OR (CM_CASHIER_ID = @pCashierId))
	     AND 		((@pUserId IS NULL) OR (CM_USER_ID = @pUserId))
	     AND    ( CM_TYPE = @pCashierMovement ) 
	     AND    te_terminal_type = @_Terminal_GamingTable_draw_type
	  ORDER BY CD_DRAW_ID
	  DROP TABLE #ACCOUNTS_TEMP	
	END	

RETURN
END
GO

/****** Object:  StoredProcedure [dbo].[sp_CheckCashDeskDrawConfig]    Script Date: 09/14/2016 11:34:23 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_CheckCashDeskDrawConfig]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_CheckCashDeskDrawConfig]
GO

/****** Object:  StoredProcedure [dbo].[sp_CheckCashDeskDrawConfig]    Script Date: 09/14/2016 11:34:23 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/*
----------------------------------------------------------------------------------------------------------------
BASIC REPORT QUERY FOR Bank Transactions Reconciliation

Version		Date						User			Description
----------------------------------------------------------------------------------------------------------------
1.0.0	    14-AGO-2016			LTC				Initial version - Product Backlog Item 17115:Mesas 46 - Sorteo de caja para mesas: GUI - Configuraci�n
1.1.0	    10-OCT-2016			LTC				Bug 18843:Cajero - Cuentas: Reintegro de pago de premio en "Efectivo en cuenta" muestra un error y no permite la operaci�n

Requeriments:
   -- Functions: 
         
Parameters:
   -- @pID:			CashDeskDraw Id
   -- @pName:   CashDeskDraw Name
*/  
CREATE PROCEDURE [dbo].[sp_CheckCashDeskDrawConfig]
	-- Add the parameters for the stored procedure here
	@pID INT = 0,
	@pName VARCHAR(20) = ''
AS
BEGIN
SET NOCOUNT ON;

DELETE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CashDesk.Draw.00'
DELETE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration.00'
   
DECLARE @POSTFIJ AS VARCHAR(5)

IF @pID = 0
BEGIN
  SET @POSTFIJ = ''
END
ELSE 
BEGIN 
  SET @POSTFIJ = '.' + RIGHT( '00' + CONVERT(VARCHAR(2),@pID),2)
END


  --'CashDesk.Draw', 'Enabled', 0
IF NOT EXISTS(SELECT 1 
				FROM GENERAL_PARAMS 
				WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ
				AND GP_SUBJECT_KEY = 'Enabled'
			 )
BEGIN
	INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
	VALUES ('CashDesk.Draw' + @POSTFIJ, 'Enabled', '0')
END


--'CashDesk.Draw', 'ShowCashDeskDraws', 0
IF NOT EXISTS(SELECT 1 
				FROM GENERAL_PARAMS 
				WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ
				AND GP_SUBJECT_KEY = 'ShowCashDeskDraws'
			 )
BEGIN
	INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
	VALUES ('CashDesk.Draw' + @POSTFIJ, 'ShowCashDeskDraws', '0')
END


--'CashDesk.Draw', 'VoucherOnCashDeskDraws.Winner', 0
IF NOT EXISTS(SELECT 1 
				FROM GENERAL_PARAMS 
				WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
				AND GP_SUBJECT_KEY = 'VoucherOnCashDeskDraws.Winner'
			 )
BEGIN
	INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
	VALUES ('CashDesk.Draw' + @POSTFIJ, 'VoucherOnCashDeskDraws.Winner', '0')
END


--'CashDesk.Draw', 'WinnerPrizeVoucherTitle', 'Sorteo en XXXX'
IF NOT EXISTS(SELECT 1 
				FROM GENERAL_PARAMS 
				WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
				AND GP_SUBJECT_KEY = 'WinnerPrizeVoucherTitle'
			 )
BEGIN
	INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
	VALUES ('CashDesk.Draw' + @POSTFIJ, 'WinnerPrizeVoucherTitle', 'Sorteo en ' + @pName)
END


--'CashDesk.Draw', 'VoucherOnCashDeskDraws.Loser', 0
IF NOT EXISTS(SELECT 1 
				FROM GENERAL_PARAMS 
				WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
				AND GP_SUBJECT_KEY = 'VoucherOnCashDeskDraws.Loser'
			 )
BEGIN
	INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
	VALUES ('CashDesk.Draw' + @POSTFIJ, 'VoucherOnCashDeskDraws.Loser', '0')
END


--'CashDesk.Draw', 'AskForParticipation', 0
IF NOT EXISTS(SELECT 1 
				FROM GENERAL_PARAMS 
				WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
				AND GP_SUBJECT_KEY = 'AskForParticipation'
			 )
BEGIN
	INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
	VALUES ('CashDesk.Draw' + @POSTFIJ, 'AskForParticipation', '0')
END


--'CashDesk.Draw', 'LoserPrizeVoucherTitle','Cortes�a sorteo en XXXX'
IF NOT EXISTS(SELECT 1 
				FROM GENERAL_PARAMS 
				WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
				AND GP_SUBJECT_KEY = 'LoserPrizeVoucherTitle'
			 )
BEGIN
	INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
	VALUES ('CashDesk.Draw' + @POSTFIJ, 'LoserPrizeVoucherTitle', 'Cortes�a sorteo en ' + @pName)
END


--'CashDesk.Draw', 'ActionOnServerDown','0'
IF NOT EXISTS(SELECT 1 
				FROM GENERAL_PARAMS 
				WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
				AND GP_SUBJECT_KEY = 'ActionOnServerDown'
			 )
BEGIN
	INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
	VALUES ('CashDesk.Draw' + @POSTFIJ, 'ActionOnServerDown', '0')
END


--'CashDesk.Draw', 'LocalServer', '1'
IF NOT EXISTS(SELECT 1 
				FROM GENERAL_PARAMS 
				WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
				AND GP_SUBJECT_KEY = 'LocalServer'
			 )
BEGIN
	INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
	VALUES ('CashDesk.Draw' + @POSTFIJ, 'LocalServer', '1')
END


--'CashDesk.Draw', 'ServerBDConnectionString',''
IF NOT EXISTS(SELECT 1 
				FROM GENERAL_PARAMS 
				WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
				AND GP_SUBJECT_KEY = 'ServerBDConnectionString'
			 )
BEGIN
	INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
	VALUES ('CashDesk.Draw' + @POSTFIJ, 'ServerBDConnectionString', '')
END


--'CashDesk.Draw', 'ServerAddress1',''
IF NOT EXISTS(SELECT 1 
				FROM GENERAL_PARAMS 
				WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
				AND GP_SUBJECT_KEY = 'ServerAddress1'
			 )
BEGIN
	INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
	VALUES ('CashDesk.Draw' + @POSTFIJ, 'ServerAddress1', '')
END


--'CashDesk.Draw', 'ServerAddress2',''
IF NOT EXISTS(SELECT 1 
				FROM GENERAL_PARAMS 
				WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
				AND GP_SUBJECT_KEY = 'ServerAddress2'
			 )
BEGIN
	INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
	VALUES ('CashDesk.Draw' + @POSTFIJ, 'ServerAddress2', '')
END


--'CashDesk.DrawConfiguration', 'BallsExtracted','0'
IF NOT EXISTS(SELECT 1 
				FROM GENERAL_PARAMS 
				WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
				AND GP_SUBJECT_KEY = 'BallsExtracted'
			 )
BEGIN
	INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
	VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'BallsExtracted', '0')
END


--'CashDesk.DrawConfiguration', 'BallsOfParticipant', '0'
IF NOT EXISTS(SELECT 1 
				FROM GENERAL_PARAMS 
				WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
				AND GP_SUBJECT_KEY = 'BallsOfParticipant'
			 )
BEGIN
	INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
	VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'BallsOfParticipant', '0')
END


--'CashDesk.DrawConfiguration', 'TotalsBallsNumber', '0'
IF NOT EXISTS(SELECT 1 
				FROM GENERAL_PARAMS 
				WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
				AND GP_SUBJECT_KEY = 'TotalsBallsNumber'
			 )
BEGIN
	INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
	VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'TotalsBallsNumber', '0')
END


--'CashDesk.DrawConfiguration', 'LoserPrize1.Fixed', '0'
IF NOT EXISTS(SELECT 1 
				FROM GENERAL_PARAMS 
				WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
				AND GP_SUBJECT_KEY = 'LoserPrize1.Fixed'
			 )
BEGIN
	INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
	VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'LoserPrize1.Fixed', '0')
END


--'CashDesk.DrawConfiguration', 'LoserPrize1.Percentage', '0'
IF NOT EXISTS(SELECT 1 
				FROM GENERAL_PARAMS 
				WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
				AND GP_SUBJECT_KEY = 'LoserPrize1.Percentage'
			 )
BEGIN
	INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
	VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'LoserPrize1.Percentage', '0')
END


--'CashDesk.DrawConfiguration', 'LoserPrize1.Enabled', 0
IF NOT EXISTS(SELECT 1 
				FROM GENERAL_PARAMS 
				WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
				AND GP_SUBJECT_KEY = 'LoserPrize1.Enabled'
			 )
BEGIN
	INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
	VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'LoserPrize1.Enabled', '0')
END


--'CashDesk.DrawConfiguration', 'NumberOfParticipants', '0'
IF NOT EXISTS(SELECT 1 
				FROM GENERAL_PARAMS 
				WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
				AND GP_SUBJECT_KEY = 'NumberOfParticipants'
			 )
BEGIN
	INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
	VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'NumberOfParticipants', '0')
END


--'CashDesk.DrawConfiguration', 'NumberOfWinners', '0'
IF NOT EXISTS(SELECT 1 
				FROM GENERAL_PARAMS 
				WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
				AND GP_SUBJECT_KEY = 'NumberOfWinners'
			 )
BEGIN
	INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
	VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'NumberOfWinners', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize1.Fixed', '0'
IF NOT EXISTS(SELECT 1 
				FROM GENERAL_PARAMS 
				WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
				AND GP_SUBJECT_KEY = 'WinnerPrize1.Fixed'
			 )
BEGIN
	INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
	VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize1.Fixed', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize1.Percentage', '0'
IF NOT EXISTS(SELECT 1 
				FROM GENERAL_PARAMS 
				WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
				AND GP_SUBJECT_KEY = 'WinnerPrize1.Percentage'
			 )
BEGIN
	INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
	VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize1.Percentage', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize1.Enabled', 0
IF NOT EXISTS(SELECT 1 
				FROM GENERAL_PARAMS 
				WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
				AND GP_SUBJECT_KEY = 'WinnerPrize1.Enabled'
			 )
BEGIN
	INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
	VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize1.Enabled', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize2.Fixed', '0'
IF NOT EXISTS(SELECT 1 
				FROM GENERAL_PARAMS 
				WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
				AND GP_SUBJECT_KEY = 'WinnerPrize2.Fixed'
			 )
BEGIN
	INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
	VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize2.Fixed', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize2.Percentage', '0'
IF NOT EXISTS(SELECT 1 
				FROM GENERAL_PARAMS 
				WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
				AND GP_SUBJECT_KEY = 'WinnerPrize2.Percentage'
			 )
BEGIN
	INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
	VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize2.Percentage', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize2.Enabled', 0
IF NOT EXISTS(SELECT 1 
				FROM GENERAL_PARAMS 
				WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
				AND GP_SUBJECT_KEY = 'WinnerPrize2.Enabled'
			 )
BEGIN
	INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
	VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize2.Enabled', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize3.Fixed', '0'
IF NOT EXISTS(SELECT 1 
				FROM GENERAL_PARAMS 
				WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
				AND GP_SUBJECT_KEY = 'WinnerPrize3.Fixed'
			 )
BEGIN
	INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
	VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize3.Fixed', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize3.Percentage', '0'
IF NOT EXISTS(SELECT 1 
				FROM GENERAL_PARAMS 
				WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
				AND GP_SUBJECT_KEY = 'WinnerPrize3.Percentage'
			 )
BEGIN
	INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
	VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize3.Percentage', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize3.Enabled', 0
IF NOT EXISTS(SELECT 1 
				FROM GENERAL_PARAMS 
				WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
				AND GP_SUBJECT_KEY = 'WinnerPrize3.Enabled'
			 )
BEGIN
	INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
	VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize3.Enabled', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize4.Fixed', '0'
IF NOT EXISTS(SELECT 1 
				FROM GENERAL_PARAMS 
				WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
				AND GP_SUBJECT_KEY = 'WinnerPrize4.Fixed'
			 )
BEGIN
	INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
	VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize4.Fixed', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize4.Percentage', '0'
IF NOT EXISTS(SELECT 1 
				FROM GENERAL_PARAMS 
				WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
				AND GP_SUBJECT_KEY = 'WinnerPrize4.Percentage'
			 )
BEGIN
	INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
	VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize4.Percentage', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize4.Enabled', 0
IF NOT EXISTS(SELECT 1 
				FROM GENERAL_PARAMS 
				WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
				AND GP_SUBJECT_KEY = 'WinnerPrize4.Enabled'
			 )
BEGIN
	INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
	VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize4.Enabled', '0')
END



--'CashDesk.DrawConfiguration', 'WinnerPrize1.PrizeType', '0'
IF NOT EXISTS(SELECT 1 
				FROM GENERAL_PARAMS 
				WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
				AND GP_SUBJECT_KEY = 'WinnerPrize1.PrizeType'
			 )
BEGIN
	INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
	VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize1.PrizeType', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize2.PrizeType', '0'
IF NOT EXISTS(SELECT 1 
				FROM GENERAL_PARAMS 
				WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
				AND GP_SUBJECT_KEY = 'WinnerPrize2.PrizeType'
			 )
BEGIN
	INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
	VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize2.PrizeType','0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize3.PrizeType', '0'
IF NOT EXISTS(SELECT 1 
				FROM GENERAL_PARAMS 
				WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
				AND GP_SUBJECT_KEY = 'WinnerPrize3.PrizeType'
			 )
BEGIN
	INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
	VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize3.PrizeType', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize4.PrizeType', '0'
IF NOT EXISTS(SELECT 1 
				FROM GENERAL_PARAMS 
				WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
				AND GP_SUBJECT_KEY = 'WinnerPrize4.PrizeType'
			 )
BEGIN
	INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
	VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize4.PrizeType', '0')
END


--'CashDesk.DrawConfiguration', 'LoserPrize1.PrizeType', '0'
IF NOT EXISTS(SELECT 1 
				FROM GENERAL_PARAMS 
				WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
				AND GP_SUBJECT_KEY = 'LoserPrize1.PrizeType'
			 )
BEGIN
	INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
	VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'LoserPrize1.PrizeType', '0')
END


-- 'CashDesk.Draw', 'AccountingMode', ''
IF NOT EXISTS(SELECT 1 
				FROM GENERAL_PARAMS 
				WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
				AND GP_SUBJECT_KEY = 'AccountingMode'
			 )
BEGIN
	INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
	VALUES ('CashDesk.Draw' + @POSTFIJ, 'AccountingMode', '0')
END


-- 'CashDesk.DrawConfiguration', 'ParticipationPrice', '0'
IF NOT EXISTS(SELECT 1 
				FROM GENERAL_PARAMS 
				WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
				AND GP_SUBJECT_KEY = 'ParticipationPrice'
			 )
BEGIN
	INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
	VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'ParticipationPrice', '0')
END	
	
	
-- 'CashDesk.Draw', 'IsCashDeskDraw', '0'
IF NOT EXISTS(SELECT 1 
				FROM GENERAL_PARAMS 
				WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
				AND GP_SUBJECT_KEY = 'IsCashDeskDraw'
			 )
BEGIN
	INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
	VALUES ('CashDesk.Draw' + @POSTFIJ, 'IsCashDeskDraw', '0')
END		


-- 'CashDesk.Draw', 'ReportUNRFromSP_SalesAndPayment', '0'
IF NOT EXISTS(SELECT 1 
				FROM GENERAL_PARAMS 
				WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
				AND GP_SUBJECT_KEY = 'ReportUNRFromSP_SalesAndPayment'
			 )
BEGIN
	INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
	VALUES ('CashDesk.Draw' + @POSTFIJ, 'ReportUNRFromSP_SalesAndPayment', '0')
END	


-- 'CashDesk.Draw', 'AccountingMode.PromoNameMovement', 'PROMOCI�N'
IF NOT EXISTS(SELECT 1 
				FROM GENERAL_PARAMS 
				WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
				AND GP_SUBJECT_KEY = 'AccountingMode.PromoNameMovement'
			 )
BEGIN
	INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
	VALUES ('CashDesk.Draw' + @POSTFIJ, 'AccountingMode.PromoNameMovement', 'Promoci�n')
END	

-- 'CashDesk.Draw', 'AccountingMode.KindOfNameMovement', 'Premio en especie'
IF NOT EXISTS(SELECT 1 
				FROM GENERAL_PARAMS 
				WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
				AND GP_SUBJECT_KEY = 'AccountingMode.KindOfNameMovement'
			 )
BEGIN
	INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
	VALUES ('CashDesk.Draw' + @POSTFIJ, 'AccountingMode.KindOfNameMovement', 'Premio en especie')
END	

-- 'CashDesk.Draw', 'AccountingMode.RE.KindOfNameMovement', 'Premio en especie (RE)'
IF NOT EXISTS(SELECT 1 
				FROM GENERAL_PARAMS 
				WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
				AND GP_SUBJECT_KEY = 'AccountingMode.RE.KindOfNameMovement'
			 )
BEGIN
	INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
	VALUES ('CashDesk.Draw' + @POSTFIJ, 'AccountingMode.RE.KindOfNameMovement', 'Premio en especie (RE)')
END	

-- 'CashDesk.Draw', 'AccountingMode.RE.PromoNameMovement', 'Promoci�n (RE)'
IF NOT EXISTS(SELECT 1 
				FROM GENERAL_PARAMS 
				WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
				AND GP_SUBJECT_KEY = 'AccountingMode.RE.PromoNameMovement'
			 )
BEGIN
	INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
	VALUES ('CashDesk.Draw' + @POSTFIJ, 'AccountingMode.RE.PromoNameMovement', 'Promoci�n (RE)')
END	


-- 'CashDesk.Draw', 'Voucher.HideCurrencySymbol', '0'
IF NOT EXISTS(SELECT 1 
				FROM GENERAL_PARAMS 
				WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
				AND GP_SUBJECT_KEY = 'Voucher.HideCurrencySymbol'
			 )
BEGIN
	INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
	VALUES ('CashDesk.Draw' + @POSTFIJ, 'Voucher.HideCurrencySymbol', '0')
END	
	
-- 'CashDesk.Draw', 'Voucher.LoserPrizeLabel', 'Cortesia'
IF NOT EXISTS(SELECT 1 
				FROM GENERAL_PARAMS 
				WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
				AND GP_SUBJECT_KEY = 'Voucher.LoserPrizeLabel'
			 )
BEGIN
	INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
	VALUES ('CashDesk.Draw' + @POSTFIJ, 'Voucher.LoserPrizeLabel', 'Cortesia')
END	
	
-- 'CashDesk.Draw', 'Voucher.UNRLabel', 'FD'
IF NOT EXISTS(SELECT 1 
				FROM GENERAL_PARAMS 
				WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
				AND GP_SUBJECT_KEY = 'Voucher.UNRLabel'
			 )
BEGIN
	INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
	VALUES ('CashDesk.Draw' + @POSTFIJ, 'Voucher.UNRLabel', 'FD')
END	

-- 'CashDesk.Draw', 'Voucher.WinnerPrizeLabel', 'Premio'
IF NOT EXISTS(SELECT 1 
				FROM GENERAL_PARAMS 
				WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
				AND GP_SUBJECT_KEY = 'Voucher.WinnerPrizeLabel'
			 )
BEGIN
	INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
	VALUES ('CashDesk.Draw' + @POSTFIJ, 'Voucher.WinnerPrizeLabel', 'Premio')
END	



--/// Probability


IF NOT EXISTS(SELECT 1 
				FROM GENERAL_PARAMS 
				WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
				AND GP_SUBJECT_KEY = 'LoserPrize1.Probability'
			 )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'LoserPrize1.Probability', '0')
END

IF NOT EXISTS(SELECT 1 
				FROM GENERAL_PARAMS 
				WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
				AND GP_SUBJECT_KEY = 'WinnerPrize1.Probability'
			 )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize1.Probability', '0')
END

IF NOT EXISTS(SELECT 1 
				FROM GENERAL_PARAMS 
				WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
				AND GP_SUBJECT_KEY = 'WinnerPrize2.Probability'
			 )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize2.Probability', '0')
END

IF NOT EXISTS(SELECT 1 
				FROM GENERAL_PARAMS 
				WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
				AND GP_SUBJECT_KEY = 'WinnerPrize3.Probability'
			 )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize3.Probability', '0')
END

IF NOT EXISTS(SELECT 1 
				FROM GENERAL_PARAMS 
				WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
				AND GP_SUBJECT_KEY = 'WinnerPrize4.Probability'
			 )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize4.Probability', '0')
END

	
END

GO

GRANT EXECUTE ON sp_CheckCashDeskDrawConfig TO wggui WITH GRANT OPTION
GO
EXEC sp_CheckCashDeskDrawConfig
GO

/****** Object:  StoredProcedure [dbo].[GetBankTransacctionsToReconciliate]    Script Date: 08/05/2016 12:38:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetBankTransacctionsToReconciliate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetBankTransacctionsToReconciliate]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/*
----------------------------------------------------------------------------------------------------------------
BASIC REPORT QUERY FOR Bank Transactions Reconciliation

Version		Date						User			Description
----------------------------------------------------------------------------------------------------------------
1.0.0	    05-AGO-2016			LTC				Initial version - Product Backlog Item 15199: TPV Televisa: GUI, bank card reconciliation
1.0.1     25-AGO-2016     LTC       Product Backlog Item 16742:TPV Televisa: Reconciliation modification of cashier vouchers
1.0.2     10-OCT-2016     LTC       Bug 18897:Televisa: Conciliacion de tarjetas bancarias no esta mostrando el numero correcto de autorizaci�n

Requeriments:
   -- Functions: 
         
Parameters:
   -- @pUser:					User Id
   -- @pWithdrawal:   Withdrawal session
*/  

CREATE PROCEDURE [dbo].[GetBankTransacctionsToReconciliate]
		@pUser INT = 0,
		@pWithdrawal VARCHAR(20)= ''
AS
BEGIN
			SELECT 
							T.PT_CREATED, 
							T.PT_ID, 
							ISNULL(T.PT_AUTH_CODE,'') PT_OPERATION_ID, 
							T.PT_TOTAL_AMOUNT, 
							T.PT_CARD_NUMBER,
							R.PTC_RECONCILIATE,
							U.GU_FULL_NAME  
				FROM  PINPAD_TRANSACTIONS T
  INNER JOIN  GUI_USERS U
					ON  U.GU_USER_ID = T.PT_USER_ID
  INNER JOIN ACCOUNT_OPERATIONS A
					ON A.AO_OPERATION_ID = T.PT_OPERATION_ID
  INNER JOIN PINPAD_TRANSACTIONS_RECONCILIATION R
					ON R.PTC_ID = T.PT_ID
			 WHERE T.PT_STATUS = 1
				 AND A.AO_CODE IN (1,17)
				 AND U.GU_USER_ID = @pUser
				 AND CONVERT(VARCHAR(20),PTC_DRAWAL,112) + ' ' + CONVERT(VARCHAR(20),PTC_DRAWAL,108) = @pWithdrawal
				 AND R.PTC_RECONCILIATE = 0
	  ORDER BY T.PT_CREATED

END

GO 

GRANT EXECUTE ON GetBankTransacctionsToReconciliate TO wggui WITH GRANT OPTION 