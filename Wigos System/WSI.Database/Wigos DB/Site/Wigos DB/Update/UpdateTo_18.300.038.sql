/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 299;

SET @New_ReleaseId = 300;
SET @New_ScriptName = N'UpdateTo_18.300.038.sql';
SET @New_Description = N'New feature: Buckets';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/******* TABLES  *******/

IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE  TABLE_NAME = 'money_collections' AND COLUMN_NAME = 'mc_refilled_hopper') 
      ALTER TABLE money_collections ADD mc_refilled_hopper XML NULL 
GO

IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE  TABLE_NAME = 'money_collection_meters' AND COLUMN_NAME = 'mcm_out_bills') 
  ALTER TABLE money_collection_meters ADD mcm_out_bills money NULL 
GO

IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE  TABLE_NAME = 'money_collection_meters' AND COLUMN_NAME = 'mcm_out_coins') 
  ALTER TABLE money_collection_meters ADD mcm_out_coins money NULL 
GO

IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE  TABLE_NAME = 'money_collection_meters' AND COLUMN_NAME = 'mcm_out_cents') 
  ALTER TABLE money_collection_meters ADD mcm_out_cents money NULL 
GO

IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE  TABLE_NAME = 'money_collections' AND COLUMN_NAME = 'mc_out_bills') 
  ALTER TABLE money_collections ADD mc_out_bills money NULL 
GO

IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE  TABLE_NAME = 'money_collections' AND COLUMN_NAME = 'mc_out_coins') 
  ALTER TABLE money_collections  ADD mc_out_coins money NULL 
GO
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE  TABLE_NAME = 'money_collections' AND COLUMN_NAME = 'mc_out_cents') 
  ALTER TABLE money_collections ADD mc_out_cents money NULL 
GO

IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE  TABLE_NAME = 'money_collections' AND COLUMN_NAME = 'mc_initial_meters') 
  ALTER TABLE money_collections  ADD mc_initial_meters XML NULL 
GO

IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE  TABLE_NAME = 'money_collections' AND COLUMN_NAME = 'mc_final_meters') 
  ALTER TABLE money_collections ADD mc_final_meters XML NULL 
GO

-- CREATE CUSTOMER TABLE
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[customers]') AND type in (N'U'))
BEGIN
  CREATE TABLE [dbo].[customers](
    [cus_customer_id] [bigint] NOT NULL,
     CONSTRAINT [PK_customers] PRIMARY KEY CLUSTERED 
    (
      [cus_customer_id] ASC
    )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
  ) ON [PRIMARY]
END
GO

-- CREATE CUSTOMER_RECORDS TABLE
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[customer_records]') AND type in (N'U'))
BEGIN
 CREATE TABLE [dbo].[customer_records](
 [cur_record_id] [bigint] IDENTITY(1,1) NOT NULL,
 [cur_customer_id] [bigint] NOT NULL,
 [cur_deleted] [bit] NOT NULL,
 [cur_created] [datetime] NOT NULL,
 [cur_expiration] [datetime] NULL,
 CONSTRAINT [PK_customer_records] PRIMARY KEY CLUSTERED 
 (
 	[cur_record_id] ASC
 )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  =
 OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
 ) ON [PRIMARY]
 
 ALTER TABLE [dbo].[customer_records]  WITH CHECK ADD  CONSTRAINT [FK_customer_records_customers] FOREIGN KEY([cur_customer_id])
 REFERENCES [dbo].[customers] ([cus_customer_id])
 
 ALTER TABLE [dbo].[customer_records] CHECK CONSTRAINT [FK_customer_records_customers]
END
GO

-- CREATE CUSTOMER_RECORDS_HISTORY
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[customer_records_history]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[customer_records_history](
	[curh_record_history_id] [bigint] IDENTITY(1,1) NOT NULL,
	[curh_record_id] [bigint] NOT NULL,
	[curh_customer_id] [bigint] NOT NULL,
	[curh_deleted] [bit] NOT NULL,
	[curh_created] [datetime] NOT NULL,
	[curh_expiration] [datetime] NULL,
	[curh_logdate] [datetime] NOT NULL,
 CONSTRAINT [PK_customer_records_history] PRIMARY KEY CLUSTERED 
(
	[curh_record_history_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[customer_records_history]  WITH CHECK ADD  CONSTRAINT [FK_customer_records_history_customers] FOREIGN KEY([curh_customer_id])
REFERENCES [dbo].[customers] ([cus_customer_id])

ALTER TABLE [dbo].[customer_records_history] CHECK CONSTRAINT [FK_customer_records_history_customers]
END
GO

-- CREATE CUSTOMER_RECORD_DETAILS
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[customer_record_details]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[customer_record_details](
	[curd_detail_id] [bigint] IDENTITY(1,1) NOT NULL,
	[curd_record_id] [bigint] NOT NULL,
	[curd_deleted] [bit] NOT NULL,
	[curd_type] [int] NOT NULL,
	[curd_data] [nvarchar](max) NULL,
	[curd_created] [datetime] NOT NULL,
	[curd_expiration] [datetime] NULL,
	[curd_image] [varbinary](max) NULL,
 CONSTRAINT [PK_customer_record_details] PRIMARY KEY CLUSTERED 
  (
	  [curd_detail_id] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING ON

ALTER TABLE [dbo].[customer_record_details]  WITH CHECK ADD  CONSTRAINT [FK_customer_record_details_customer_records] FOREIGN KEY([curd_record_id])
REFERENCES [dbo].[customer_records] ([cur_record_id])

ALTER TABLE [dbo].[customer_record_details] CHECK CONSTRAINT [FK_customer_record_details_customer_records]
END
GO

-- CREATE CUSTOMER_RECORD_DETAILS_HISTORY
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[customer_record_details_history]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[customer_record_details_history](
	[curdh_detail_history_id] [bigint] IDENTITY(1,1) NOT NULL,
	[curdh_record_id] [bigint] NOT NULL,
	[curdh_deleted] [bit] NOT NULL,
	[curdh_type] [int] NOT NULL,
	[curdh_data] [nvarchar](max) NULL,
	[curdh_created] [datetime] NOT NULL,
	[curdh_expiration] [datetime] NULL,
	[curdh_logdate] [datetime] NOT NULL,
	[curdh_image] [varbinary](max) NULL,
 CONSTRAINT [PK_customer_record_details_history] PRIMARY KEY CLUSTERED 
(
	[curdh_detail_history_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING ON

ALTER TABLE [dbo].[customer_record_details_history]  WITH CHECK ADD  CONSTRAINT [FK_customer_record_details_history_customer_records_history] FOREIGN KEY([curdh_record_id])
REFERENCES [dbo].[customer_records_history] ([curh_record_history_id])

ALTER TABLE [dbo].[customer_record_details_history] CHECK CONSTRAINT [FK_customer_record_details_history_customer_records_history]
END
GO

-- ALTER TABLE CUSTOMER_ENTRANCE
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[customer_entrances]') AND type in (N'U'))
BEGIN
EXECUTE sp_rename N'dbo.customer_entrances.cue_ticket_entry_price', N'Tmp_cue_ticket_entry_price_real_1', 'COLUMN' 
EXECUTE sp_rename N'dbo.customer_entrances.Tmp_cue_ticket_entry_price_real_1', N'cue_ticket_entry_price_real', 'COLUMN' 

ALTER TABLE dbo.customer_entrances ADD
   cue_cashier_terminal_id bigint NULL
  ,cue_coupon nchar(250) NULL
  ,cue_voucher_sequence bigint NULL
  ,cue_entrance_expiration datetime NULL
  ,cue_ticket_entry_price_paid money NULL
  ,cue_ticket_entry_price_difference money NULL

--ALTER TABLE dbo.customer_entrances SET (LOCK_ESCALATION = TABLE)
END
GO


-- CREATE CUSTOMER_ENTRANCES_PRICES
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[customer_entrances_prices]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[customer_entrances_prices](
	[cuep_price_id] [bigint] IDENTITY(1,1) NOT NULL,
	[cuep_description] [nvarchar](max) NOT NULL,
	[cuep_price] [money] NOT NULL,
	[cuep_customer_level] [int] NULL,
	[cuep_default] [bit] NOT NULL,
 CONSTRAINT [PK_Customer_Entrances_Prices] PRIMARY KEY CLUSTERED 
(
	[cuep_price_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END 
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[buckets]') AND type in (N'U'))
  DROP TABLE buckets
GO

CREATE TABLE [dbo].[buckets](
	[bu_bucket_id] [bigint] NOT NULL,
	[bu_name] nvarchar(100) NOT NULL,
	[bu_enabled] [bit] NULL,
	[bu_system_type] [int] NULL,
	[bu_bucket_type] [int] NULL,
	[bu_visible_flags] [bigint] NULL,
	[bu_order_on_reports] [int] NULL,
	[bu_expiration_days] [int] NULL,
	[bu_expiration_date] varchar(10) NULL,
	[bu_level_flags] [bit] NOT NULL,
	[bu_k_factor] [bit] NULL,
 CONSTRAINT [PK_buckets] PRIMARY KEY CLUSTERED 
(
	[bu_bucket_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[customer_bucket_by_gaming_day]') AND type in (N'U'))
  DROP TABLE customer_bucket_by_gaming_day
GO

CREATE TABLE [dbo].[customer_bucket_by_gaming_day](
	[cbud_gaming_day] [int] NOT NULL,
	[cbud_customer_id] [bigint] NOT NULL,
	[cbud_bucket_id] [bigint] NOT NULL,
	[cbud_value] [money] NULL,
	[cbud_value_added] [money] NULL,
	[cbud_value_substracted] [money] NULL,
 CONSTRAINT [PK_customer_bucket_by_gaming_day] PRIMARY KEY CLUSTERED 
(
	[cbud_gaming_day] ASC,
	[cbud_customer_id] ASC,
	[cbud_bucket_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[customer_bucket]') AND type in (N'U'))
DROP TABLE customer_bucket
GO

CREATE TABLE [dbo].[customer_bucket](
	[cbu_customer_id] [bigint] NOT NULL,
	[cbu_bucket_id] [bigint] NOT NULL,
	[cbu_value] [money] NULL,
	[cbu_updated] [datetime] NOT NULL,
	[cbu_timestamp] [timestamp] NULL,
 CONSTRAINT [PK_customer_bucket] PRIMARY KEY CLUSTERED 
(
	[cbu_customer_id] ASC,
	[cbu_bucket_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[bucket_levels]') AND type in (N'U'))
DROP TABLE bucket_levels
GO

CREATE TABLE [dbo].[bucket_levels](
	[bul_bucket_id] [bigint] NOT NULL,
	[bul_level_id] [bigint] NOT NULL,
	[bul_a_factor] [decimal] NOT NULL,
	[bul_b_factor] [decimal] NOT NULL,
 CONSTRAINT [PK_bucket_levels] PRIMARY KEY CLUSTERED 
(
	[bul_bucket_id] ASC,
	[bul_level_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pending_play_sessions_to_player_tracking]') AND type in (N'U'))
DROP TABLE pending_play_sessions_to_player_tracking
GO

CREATE TABLE [dbo].[pending_play_sessions_to_player_tracking](
	[pps_session_id] [bigint] NOT NULL,
	[pps_account_id] [bigint] NOT NULL,
	[pps_coin_in] decimal(12,3) NOT NULL,
	[pps_datetimecreated] [datetime] NOT NULL,
	[pps_terminal_id] [int] NOT NULL,
	[pps_duration_total_minutes] [int] NOT NULL,
	[pps_params_numplayed] [bigint] NOT NULL, 
	[pps_params_balance_mismatch] [bit] NOT NULL,
	[pps_terminal_type] [int] NOT NULL
 CONSTRAINT [PK_pending_play_sessions_to_player_tracking] PRIMARY KEY CLUSTERED 
(
	[pps_session_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[pending_play_sessions_to_player_tracking] ADD CONSTRAINT [DF_pending_play_sessions_to_player_tracking_pps_datetimecreated] DEFAULT getdate() FOR [pps_datetimecreated]

GO

-- TABLE account_buckets_expired_list -------------------------------------------------------------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[account_buckets_expired_list]') AND type in (N'U'))
	DROP TABLE [account_buckets_expired_list]
GO

CREATE TABLE account_buckets_expired_list
(
	abel_bucket_id bigint NOT NULL,
	abel_account_id bigint NOT NULL,
	abel_value_to_expire money NOT NULL,
	abel_datetime datetime NOT NULL,
	CONSTRAINT PK_account_buckets_expired_list PRIMARY KEY CLUSTERED 
	(
		abel_bucket_id ASC, abel_account_id ASC
	)
	WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[account_buckets_expired_control]') AND type in (N'U'))
DROP TABLE [account_buckets_expired_control]
GO

CREATE TABLE account_buckets_expired_control
(
	abec_bucket_id bigint NOT NULL,
	abec_day_month nvarchar(5) NOT NULL,
	abec_year int NOT NULL,
	abec_execution datetime NULL,
	CONSTRAINT PK_account_buckets_expired_control PRIMARY KEY CLUSTERED 
	(
		abec_bucket_id ASC, abec_day_month ASC, abec_year ASC
	)
	WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]	

GO

/******* INDEXES *******/

/******* RECORDS *******/


IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='Reception' AND GP_SUBJECT_KEY = 'RequireNewCard')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Reception', 'RequireNewCard',1)
GO
IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='Reception' AND GP_SUBJECT_KEY = 'CardFunctionsEnabled') 	
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Reception', 'CardFunctionsEnabled',1)
GO
IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='Account.VisibleField' AND GP_SUBJECT_KEY = 'Photo') 	
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Account.VisibleField','Photo',1)
GO
IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Reception' AND GP_SUBJECT_KEY ='Agreement.Text')
	INSERT INTO general_params (gp_group_key, gp_subject_key, gp_key_value)VALUES ('Reception', 'Agreement.Text', '');
GO
IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Reception' AND GP_SUBJECT_KEY ='Agreement.ShowMode')
	INSERT INTO general_params (gp_group_key, gp_subject_key, gp_key_value) VALUES ('Reception', 'Agreement.ShowMode', '0');
GO
IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.Voucher' AND GP_SUBJECT_KEY ='Reception.Footer')
	INSERT INTO general_params (gp_group_key, gp_subject_key, gp_key_value) VALUES ('Cashier.Voucher', 'Reception.Footer', '');
GO
IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Reception' AND GP_SUBJECT_KEY ='Record.ExpirationDays')
	INSERT INTO general_params (gp_group_key, gp_subject_key, gp_key_value) VALUES ('Reception', 'Record.ExpirationDays', '90');
GO
IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Reception' AND GP_SUBJECT_KEY ='Mode')
	INSERT INTO general_params (gp_group_key, gp_subject_key, gp_key_value) VALUES ('Reception', 'Mode', '0');
GO

DELETE FROM BUCKETS
GO

DECLARE @bu_name_bucket_1 NVARCHAR(100)
DECLARE @bu_name_bucket_2 NVARCHAR(100)
DECLARE @bu_name_bucket_3 NVARCHAR(100)
DECLARE @bu_name_bucket_4 NVARCHAR(100)
DECLARE @bu_name_bucket_5 NVARCHAR(100)
DECLARE @bu_name_bucket_6 NVARCHAR(100)
DECLARE @bu_name_bucket_7 NVARCHAR(100)

if exists(select 1 from GENERAL_PARAMS where gp_group_key='WigosGUI' and gp_subject_key = 'Language' and gp_key_value = 'es') begin
            set @bu_name_bucket_1 = 'Puntos de canje'
            set @bu_name_bucket_2 = 'Puntos de nivel'
            set @bu_name_bucket_3 = 'Cr�dito NR'
            set @bu_name_bucket_4 = 'Cr�dito RE'
            set @bu_name_bucket_5 = 'Bucket oculto 1: Puntos de Canje'
            set @bu_name_bucket_6 = 'Bucket oculto 2: Cr�dito NR o free plays'
            set @bu_name_bucket_7 = 'Bucket oculto 3: Cr�dito RE o Comps'

end else 
begin
            set @bu_name_bucket_1 = 'Redemption points'
            set @bu_name_bucket_2 = 'Ranking level points'
            set @bu_name_bucket_3 = 'Non-redeemable credit'
            set @bu_name_bucket_4 = 'Redeemable credit'
            set @bu_name_bucket_5 = 'Hidden Bucket 1: Redemption points'
            set @bu_name_bucket_6 = 'Hidden Bucket 2: Non-redeemable credit or free plays'
            set @bu_name_bucket_7 = 'Hidden Bucket 3: Redeemable credit or Comps'
end

INSERT INTO BUCKETS
		(
		BU_BUCKET_ID,
		BU_NAME,
		BU_ENABLED, 
		BU_SYSTEM_TYPE, 
		BU_BUCKET_TYPE, 
		BU_VISIBLE_FLAGS, 
		BU_ORDER_ON_REPORTS, 
		BU_EXPIRATION_DAYS, 
		BU_EXPIRATION_DATE, 
		BU_LEVEL_FLAGS, -- Por nivel / General
		BU_K_FACTOR
		)

SELECT 1, @bu_name_bucket_1, 0, 1, 1, 4294967291, 1, 0, NULL, 1, 0
UNION ALL                                        
SELECT 2, @bu_name_bucket_2, 0, 1, 2, 4294967291, 2, 0, NULL, 1, 0
UNION ALL                                        
SELECT 3, @bu_name_bucket_3, 0, 1, 3, 4294967291, 3, 0, NULL, 1, 0
UNION ALL                                        
SELECT 4, @bu_name_bucket_4, 0, 1, 4, 4294967291, 4, 0, NULL, 1, 0
UNION ALL                                        
SELECT 5, @bu_name_bucket_5, 0, 1, 5, 4294967291, 5, 0, NULL, 1, 0
UNION ALL                                        
SELECT 6, @bu_name_bucket_6, 0, 1, 6, 4294967291, 6, 0, NULL, 1, 0
UNION ALL                                        
SELECT 7, @bu_name_bucket_7, 0, 1, 7, 4294967291, 7, 0, NULL, 1, 0

GO

ALTER TABLE bucket_levels ALTER COLUMN bul_a_factor decimal(18, 2) NOT NULL
GO

ALTER TABLE bucket_levels ALTER COLUMN bul_b_factor decimal(18, 2) NOT NULL
GO

DELETE FROM BUCKET_LEVELS


DECLARE @BucketId_PuntosCanje INTEGER
DECLARE @BucketId_PuntosNivel INTEGER
DECLARE @Level01_Points decimal(10,2)
DECLARE @Level02_Points decimal(10,2)
DECLARE @Level03_Points decimal(10,2)
DECLARE @Level04_Points decimal(10,2)
DECLARE @Level05_Points decimal(10,2)

SET @Level01_Points = 0
SET @Level02_Points = 0
SET @Level03_Points = 0
SET @Level04_Points = 0
SET @Level05_Points = 0

SELECT @Level01_Points = ISNULL(convert(decimal(10,2),replace(GP_KEY_VALUE,',','.')),0) FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PlayerTracking' AND GP_SUBJECT_KEY = 'Level01.RedeemablePlayedTo1Point'
SELECT @Level02_Points = ISNULL(convert(decimal(10,2),replace(GP_KEY_VALUE,',','.')),0) FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PlayerTracking' AND GP_SUBJECT_KEY = 'Level02.RedeemablePlayedTo1Point'
SELECT @Level03_Points = ISNULL(convert(decimal(10,2),replace(GP_KEY_VALUE,',','.')),0) FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PlayerTracking' AND GP_SUBJECT_KEY = 'Level03.RedeemablePlayedTo1Point'
SELECT @Level04_Points = ISNULL(convert(decimal(10,2),replace(GP_KEY_VALUE,',','.')),0) FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PlayerTracking' AND GP_SUBJECT_KEY = 'Level04.RedeemablePlayedTo1Point'
SELECT @Level05_Points = ISNULL(convert(decimal(10,2),replace(GP_KEY_VALUE,',','.')),0) FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PlayerTracking' AND GP_SUBJECT_KEY = 'Level05.RedeemablePlayedTo1Point'


-- BUCKET: Puntos de canje
SET @BucketId_PuntosCanje = 1
INSERT INTO  BUCKET_LEVELS (BUL_BUCKET_ID, BUL_LEVEL_ID, BUL_A_FACTOR, BUL_B_FACTOR)
     SELECT  @BucketId_PuntosCanje, 1, 1, @Level01_Points WHERE @Level01_Points <> 0
      UNION
     SELECT  @BucketId_PuntosCanje, 2, 1, @Level02_Points WHERE @Level02_Points <> 0
      UNION
     SELECT  @BucketId_PuntosCanje, 3, 1, @Level03_Points WHERE @Level03_Points <> 0
      UNION
     SELECT  @BucketId_PuntosCanje, 4, 1, @Level04_Points WHERE @Level04_Points <> 0
      UNION
     SELECT  @BucketId_PuntosCanje, 5, 1, @Level05_Points WHERE @Level05_Points <> 0
     
		-- Disable Buckets with RedeemablePlayedTo1Point = 0
		IF (   @Level01_Points <> 0
			OR @Level02_Points <> 0
			OR @Level03_Points <> 0
			OR @Level04_Points <> 0
			OR @Level05_Points <> 0)
			
			UPDATE BUCKETS SET bu_enabled = 1 WHERE bu_bucket_id = @BucketId_PuntosCanje
											

-- BUCKET: Puntos de nivel
SET @BucketId_PuntosNivel = 2
INSERT INTO  BUCKET_LEVELS (BUL_BUCKET_ID, BUL_LEVEL_ID, BUL_A_FACTOR, BUL_B_FACTOR)
     SELECT  @BucketId_PuntosNivel, 1, 1, @Level01_Points WHERE @Level01_Points <> 0
      UNION												 
     SELECT  @BucketId_PuntosNivel, 2, 1, @Level02_Points WHERE @Level02_Points <> 0
      UNION												 
     SELECT  @BucketId_PuntosNivel, 3, 1, @Level03_Points WHERE @Level03_Points <> 0
      UNION												 
     SELECT  @BucketId_PuntosNivel, 4, 1, @Level04_Points WHERE @Level04_Points <> 0
      UNION												 
     SELECT  @BucketId_PuntosNivel, 5, 1, @Level05_Points WHERE @Level05_Points <> 0



		-- Disable Buckets with RedeemablePlayedTo1Point = 0
		IF (   @Level01_Points <> 0
			OR @Level02_Points <> 0
			OR @Level03_Points <> 0
			OR @Level04_Points <> 0
			OR @Level05_Points <> 0)
			
			UPDATE BUCKETS SET bu_enabled = 1 WHERE bu_bucket_id = @BucketId_PuntosNivel
												
											
											
GO

DECLARE @bu_pm_name AS NVARCHAR(30)
SELECT @bu_pm_name = BU_NAME FROM buckets WHERE bu_bucket_id = 3

IF NOT EXISTS (SELECT * FROM promotions WHERE pm_type = 23) -- PROMOTION: AUTOMATIC NR CREDIT
BEGIN
  INSERT INTO [dbo].[promotions]  
             ([pm_name]
             ,[pm_enabled]
             ,[pm_type]
             ,[pm_date_start]
             ,[pm_date_finish]
             ,[pm_schedule_weekday]
             ,[pm_schedule1_time_from]
             ,[pm_schedule1_time_to]
             ,[pm_schedule2_enabled]
             ,[pm_schedule2_time_from]
             ,[pm_schedule2_time_to]
             ,[pm_gender_filter]
             ,[pm_birthday_filter]
             ,[pm_expiration_type]
             ,[pm_expiration_value]
             ,[pm_min_cash_in]
             ,[pm_min_cash_in_reward]
             ,[pm_cash_in]
             ,[pm_cash_in_reward]
             ,[pm_won_lock]
             ,[pm_num_tokens]
             ,[pm_token_name]
             ,[pm_token_reward]
             ,[pm_daily_limit]
             ,[pm_monthly_limit]
             ,[pm_level_filter]
             ,[pm_permission]
             ,[pm_freq_filter_last_days]
             ,[pm_freq_filter_min_days]
             ,[pm_freq_filter_min_cash_in]
             ,[pm_min_spent]
             ,[pm_min_spent_reward]
             ,[pm_spent]
             ,[pm_spent_reward]
             ,[pm_provider_list]
             ,[pm_offer_list]
             ,[pm_global_daily_limit]
             ,[pm_global_monthly_limit]
             ,[pm_small_resource_id]
             ,[pm_large_resource_id]
             ,[pm_min_played]
             ,[pm_min_played_reward]
             ,[pm_played]
             ,[pm_played_reward]
             ,[pm_play_restricted_to_provider_list]
             ,[pm_last_executed]
             ,[pm_next_execution]
             ,[pm_global_limit]
             ,[pm_credit_type]
             ,[pm_category_id]
             ,[pm_ticket_footer]
             ,[pm_visible_on_promobox]
             ,[pm_award_on_promobox]
             ,[pm_expiration_limit])
       VALUES
              ( @bu_pm_name --[pm_name]
             , 1
             , 23                         --[pm_type]
             , CAST('01-01-2010 00:00:00' as DATETIME)
             , CAST('01-01-2100 00:00:00' as DATETIME)
             , 127
             , 0
             , 0
             , 0
             , 0
             , 0
             , 0
             , 0
             , 1 
             , 999                        --[pm_expiration_value]
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , NULL
             , 0
             , ''
             , 0.00
             , NULL
             , NULL
             , 0             
             , 0
             , 0
             , 0
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , NULL
             , NULL
             , NULL
             , NULL
             , NULL
             , NULL
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , 0
             , NULL
             , NULL
             , NULL
             , 1
             , 0
             , ''
             , 0
             , 0
             , NULL)
END
GO

/******* PROCEDURES *******/

IF EXISTS(SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetBucketValue]'))
    DROP FUNCTION [dbo].[GetBucketValue];
GO

CREATE FUNCTION [dbo].[GetBucketValue]  (@BucketId bigint, @AccountId bigint) 
RETURNS DECIMAL(12,2)
AS
BEGIN
  DECLARE @Result DECIMAL(12,2)
  
  
  SELECT @Result  = CBU_VALUE 
  FROM CUSTOMER_BUCKET 
  WHERE CBU_BUCKET_ID = @BucketId
  AND CBU_CUSTOMER_ID = @AccountId
  
  RETURN ISNULL(@Result ,0)
END -- GetBucketValue
GO

GRANT EXECUTE ON [dbo].[GetBucketValue] TO [wggui] WITH GRANT OPTION 
GO

GRANT EXEC ON dbo.GetBucketValue TO PUBLIC
GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetAccountPointsCache]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[GetAccountPointsCache]
GO

CREATE PROCEDURE [dbo].[GetAccountPointsCache]
       @pAccountId    bigint
AS
BEGIN
  
  EXEC AccountPointsCache_CalculateToday @pAccountId

  DECLARE @BucketPuntosCanje Integer 
  SET @BucketPuntosCanje = 1
  
  --Realizar select ACCOUNT_POINTS_CACHE con inner join a ACCOUNTS. -- select similar al que se hace en WKT_Player.cs funci�n Read(..)
  SELECT   ISNULL(AC_HOLDER_NAME, '')             AS AC_HOLDER_NAME 
         , ISNULL(AC_HOLDER_GENDER, 0)            AS AC_HOLDER_GENDER 
         , AC_HOLDER_BIRTH_DATE 
         , AC_BALANCE 
         , ISNULL (DBO.GETBUCKETVALUE(@BucketPuntosCanje, @pAccountId), 0)  AS AC_POINTS
         , AC_HOLDER_LEVEL                        AS AC_CURRENT_HOLDER_LEVEL
         , AC_HOLDER_LEVEL_EXPIRATION 
         , AC_HOLDER_LEVEL_ENTERED 
         , AC_RE_BALANCE 
         , AC_PROMO_RE_BALANCE 
         , AC_PROMO_NR_BALANCE 
         , ISNULL (AC_CURRENT_PLAY_SESSION_ID, 0) AS AC_CURRENT_PLAY_SESSION_ID 
         , AC_CURRENT_TERMINAL_NAME 
         , ISNULL(APC_HISTORY_POINTS_GENERATED_FOR_LEVEL         , 0) + ISNULL(APC_TODAY_POINTS_GENERATED_FOR_LEVEL         , 0)  AS AM_POINTS_GENERATED 
         , ISNULL(APC_HISTORY_POINTS_DISCRETIONAL_FOR_LEVEL      , 0) + ISNULL(APC_TODAY_POINTS_DISCRETIONAL_FOR_LEVEL      , 0)  AS AM_POINTS_DISCRETIONARIES
         , ISNULL(APC_HISTORY_POINTS_DISCRETIONAL_ONLY_FOR_REDEEM, 0) + ISNULL(APC_TODAY_POINTS_DISCRETIONAL_ONLY_FOR_REDEEM, 0)  AS AM_POINTS_PROMO_DISCRETIONARIES 
         , ISNULL(APC_HISTORY_POINTS_PROMOTION_ONLY_FOR_REDEEM   , 0) + ISNULL(APC_TODAY_POINTS_PROMOTION_ONLY_FOR_REDEEM   , 0)  AS AM_POINTS_PROMO_ONLY_FOR_REDEEM
         , APC_TODAY_LAST_MOVEMENT_ID 
    FROM   ACCOUNTS 
   INNER   JOIN ACCOUNT_POINTS_CACHE ON APC_ACCOUNT_ID = AC_ACCOUNT_ID
   WHERE   AC_ACCOUNT_ID = @pAccountId; 
  
END  -- PROCEDURE [dbo].[GetAccountPointsCache]
GO

GRANT EXECUTE ON [dbo].[GetAccountPointsCache] TO [wggui] WITH GRANT OPTION
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WSP_PlayerInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[WSP_PlayerInfo]
GO
CREATE PROCEDURE [dbo].[WSP_PlayerInfo]
    @pVendorID   NVARCHAR(16),
    @pTrackData  NVARCHAR(24)
AS
BEGIN
  -- SET NOCOUNT ON added to prevent extra result sets from
  -- interfering with SELECT statements.
  SET NOCOUNT ON;

  DECLARE @pStatusCode    AS INT
  DECLARE @pStatusText    AS NVARCHAR(256)

  DECLARE @pBlocked       AS BIT
  DECLARE @pPlayed        AS MONEY
  DECLARE @pWon           AS MONEY
  DECLARE @pToday         AS DATETIME
  DECLARE @pSiteId        AS INT
  DECLARE @pAccountId     AS BIGINT
  DECLARE @pHolderLevel   AS INT
  DECLARE @pLevelPrefix   AS NVARCHAR (50)
  DECLARE @pLevelName     AS NVARCHAR (50)
  DECLARE @auth_vendor    AS BIT
  DECLARE @pPointsToday   AS MONEY
  DECLARE @bucket_puntos_canje AS INT

  SET @pStatusCode = 4
  SET @pStatusText = N'Error'
  SET @bucket_puntos_canje = 1

  BEGIN TRY
  
    -- Check if vendor is authorized
    --
    SELECT   @auth_vendor  = WAV_AUTHORIZED
      FROM   WSP_AUTHORIZED_VENDORS
     WHERE   WAV_VENDOR_ID = @pVendorID

    SET @auth_vendor = ISNULL(@auth_vendor, 0)
    IF @auth_vendor = 0
    BEGIN
      SET @pStatusCode = 1
      SET @pStatusText = N'Vendor Not Authorized'

      GOTO END_PROCEDURE
    END

    SET @pAccountId = ( SELECT AC_ACCOUNT_ID From ACCOUNTS where ac_track_data  = dbo.TrackDataToInternal(@pTrackData))
    SET @pAccountId = ISNULL (@pAccountId, 0)

    SET @pStatusCode = 2
    SET @pStatusText = N'Account Not Found'

    IF ( @pAccountId > 0 )
    BEGIN

      SET @pStatusCode = 3
      SET @pStatusText = N'Account Blocked'

      SELECT @pBlocked = AC_BLOCKED, @pHolderLevel = AC_HOLDER_LEVEL FROM ACCOUNTS WHERE ac_account_id = @pAccountId

      IF ( @pHolderLevel = 1 )      SET @pLevelPrefix = 'Level01'
      ELSE IF ( @pHolderLevel = 2 ) SET @pLevelPrefix = 'Level02'
      ELSE IF ( @pHolderLevel = 3 ) SET @pLevelPrefix = 'Level03'
      ELSE IF ( @pHolderLevel = 4 ) SET @pLevelPrefix = 'Level04'
      ELSE SET @pLevelPrefix = NULL

      IF ( @pLevelPrefix IS NOT NULL )
      BEGIN
        SELECT @pLevelName = GP_KEY_VALUE
          FROM GENERAL_PARAMS
         WHERE GP_GROUP_KEY = 'PlayerTracking'
           AND GP_SUBJECT_KEY = @pLevelPrefix + '.Name'
      END

      SET @pSiteId = ISNULL ( ( SELECT   CAST (GP_KEY_VALUE AS INT)
                         FROM   GENERAL_PARAMS
                        WHERE   GP_GROUP_KEY   = 'Site'
                          AND   GP_SUBJECT_KEY = 'Identifier' ), 0 )

      SET @pToday = ( SELECT dbo.TodayOpening (@pSiteId) )

      SELECT   @pPlayed       = SUM(PS_PLAYED_AMOUNT)
             , @pWon          = SUM(PS_WON_AMOUNT)
             , @pPointsToday  = SUM(ISNULL(PS_AWARDED_POINTS, 0))
        FROM   PLAY_SESSIONS WITH(INDEX(IX_ps_account_id_started))
       WHERE   PS_ACCOUNT_ID  = @pAccountId
         AND   PS_STARTED    >= @pToday

      IF ( @pBlocked = 0 )
      BEGIN
        -- Insert statements for procedure here
        SELECT   0                                                                                                         StatusCode
               , 'Success'                                                                                                 StatusText
               , CASE WHEN @pLevelPrefix IS NULL THEN NULL ELSE AC_HOLDER_NAME END                                         PlayerName
               , CASE WHEN @pLevelPrefix IS NULL THEN NULL ELSE AC_HOLDER_GENDER END                                       Gender
               , CASE WHEN @pLevelPrefix IS NULL THEN NULL ELSE AC_HOLDER_BIRTH_DATE END                                   Birthdate
               , AC_HOLDER_LEVEL                                                                                           Level
               , CASE WHEN @pLevelPrefix IS NULL THEN NULL ELSE @pLevelName END                                            LevelName
               , CASE WHEN @pLevelPrefix IS NULL THEN NULL ELSE CAST (ROUND(DBO.GETBUCKETVALUE(@bucket_puntos_canje, AC_ACCOUNT_ID) , 0, 1) AS INT) END Points
               , AC_LAST_ACTIVITY                                                                                          LastActivity
               , ISNULL (@pPlayed, 0)                                                                                      PlayedToday
               , ISNULL (@pWon,    0)                                                                                      WonToday
               , AC_ACCOUNT_ID                                                                                             AccountId
               , CASE WHEN @pLevelPrefix IS NULL THEN NULL ELSE CAST (ROUND(ISNULL (@pPointsToday, 0) , 0, 1) AS INT) END  PointsToday

        From ACCOUNTS
        where ac_account_id = @pAccountId

        SET @pStatusCode = 0
        SET @pStatusText = N'Success'

      END
    END

  END TRY
  BEGIN CATCH
    SET @pStatusCode = 4
    SET @pStatusText  = N'Error:'
                      + N' ERROR_NUMBER: '    + CAST(ERROR_NUMBER()               AS NVARCHAR)
                      + N' ERROR_SEVERITY: '  + CAST(ERROR_SEVERITY()             AS NVARCHAR)
                      + N' ERROR_STATE: '     + CAST(ERROR_STATE()                AS NVARCHAR)
                      + N' ERROR_PROCEDURE: ' + CAST(ISNULL(ERROR_PROCEDURE(),'') AS NVARCHAR)
                      + N' ERROR_LINE: '      + CAST(ERROR_LINE()                 AS NVARCHAR)
                      + N' ERROR_MESSAGE: '   + CAST(ERROR_MESSAGE()              AS NVARCHAR(MAX))
  END CATCH

END_PROCEDURE:
  IF ( @pStatusCode <> 0 )
    SELECT  @pStatusCode   StatusCode
          , @pStatusText  StatusText
          , NULL PlayerName
          , NULL Gender
          , NULL Birthdate
          , NULL Level
          , NULL LevelName
          , NULL Points
          , NULL LastActivity
          , NULL PlayedToday
          , NULL WonToday
          , NULL AccountId
          , NULL PointsToday

END -- WSP_PlayerInfo

-- Only if exists user wg_wsp, then 'grant execute' to procedure
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'wg_wsp')
  GRANT EXECUTE ON OBJECT::dbo.WSP_PlayerInfo TO wg_wsp;
GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Update_PointsInAccount]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Update_PointsInAccount]
GO
CREATE PROCEDURE [dbo].[Update_PointsInAccount]
  @pMovementId                 BIGINT
, @pAccountId                  BIGINT
, @pErrorCode                  INT         
, @pPointsSequenceId           BIGINT      
, @pReceivedPoints             MONEY  
, @pPlayerTrackingMode         INT         

AS
BEGIN   
  DECLARE @LocalDelta       AS MONEY
  DECLARE @ActualPoints     AS MONEY
  DECLARE @ReceivedAddLocal AS MONEY
  DECLARE @NewPoints        AS MONEY
  DECLARE @bucket_puntos_canje AS INT
  DECLARE @TableTemp table ( T_VALUE DECIMAL);
	
  SET @bucket_puntos_canje = 1

  IF NOT EXISTS (SELECT 1 FROM ACCOUNTS WHERE ac_account_id = @pAccountId )   
    INSERT INTO   ACCOUNTS 
                ( AC_ACCOUNT_ID, AC_TYPE, AC_BLOCKED, AC_TRACK_DATA) 
         VALUES ( @pAccountId,         2,          1, '-RECYCLED-NEW-' + CAST (NEWID() AS NVARCHAR(50)) )
         
  IF ((@pMovementId <> 0)AND(@pErrorCode = 0 OR @pErrorCode = 2))
    DELETE   MS_SITE_PENDING_ACCOUNT_MOVEMENTS
     WHERE   SPM_MOVEMENT_ID = @pMovementId
  
  IF (@pErrorCode <> 0) RETURN

  IF (@pPlayerTrackingMode = 1) RETURN

  DECLARE @PrevSequence as BIGINT
  
  -- Lock the account and later add the local points
  UPDATE ACCOUNTS SET AC_TYPE = AC_TYPE + 0 WHERE AC_ACCOUNT_ID = @pAccountId
  UPDATE CUSTOMER_BUCKET SET CBU_VALUE = CBU_VALUE + 0 OUTPUT INSERTED.CBU_VALUE INTO @TableTemp WHERE CBU_CUSTOMER_ID = @pAccountId AND CBU_BUCKET_ID = @bucket_puntos_canje

  -- SET @PrevSequence = (SELECT ISNULL(AC_MS_POINTS_SEQ_ID, 0) FROM ACCOUNTS WHERE AC_ACCOUNT_ID = @pAccountId)
  SELECT   @PrevSequence = ISNULL(AC_MS_POINTS_SEQ_ID, 0) 
    FROM   ACCOUNTS 
   WHERE   AC_ACCOUNT_ID = @pAccountId

   SELECT @ActualPoints = T_VALUE FROM @TableTemp
   SET @ActualPoints = ISNULL(@ActualPoints, 0)
   
  IF ( @PrevSequence > @pPointsSequenceId ) RETURN
  
-- Local Points
  SELECT   @LocalDelta = SUM(am_add_amount - am_sub_amount) 
    FROM   ACCOUNT_MOVEMENTS, MS_SITE_PENDING_ACCOUNT_MOVEMENTS
   WHERE   AM_MOVEMENT_ID = SPM_MOVEMENT_ID
     AND   AM_ACCOUNT_ID  = @pAccountId
     AND   AM_TYPE IN (36,37,38,40,41,46,50,60,61,66) -- Not included level movements,(39)PointsGiftDelivery, 
                                                      -- (42)PointsGiftServices and (67)PointsStatusChanged
  
  SET @ReceivedAddLocal = @pReceivedPoints + ISNULL(@LocalDelta , 0) 
  
  SET @NewPoints = CASE WHEN (ISNULL(@LocalDelta , 0) <> 0 AND @ActualPoints    <= @pReceivedPoints AND @ActualPoints    <= @ReceivedAddLocal)
                             THEN @ActualPoints
                        WHEN (ISNULL(@LocalDelta , 0) <> 0 AND @pReceivedPoints <= @ActualPoints    AND @pReceivedPoints <= @ReceivedAddLocal)
                             THEN @pReceivedPoints
                        ELSE @ReceivedAddLocal 
                        END 
/*
  UPDATE   ACCOUNTS
     SET   AC_POINTS                  = @NewPoints
         , AC_MS_POINTS_SEQ_ID        = @pPointsSequenceId
         , AC_MS_POINTS_SYNCHRONIZED  = GETDATE ()
   WHERE   AC_ACCOUNT_ID                   = @pAccountId
     AND   ISNULL(AC_MS_POINTS_SEQ_ID, 0) <= @pPointsSequenceId
*/
  UPDATE   ACCOUNTS
     SET   AC_MS_POINTS_SEQ_ID        = @pPointsSequenceId
         , AC_MS_POINTS_SYNCHRONIZED  = GETDATE ()
   WHERE   AC_ACCOUNT_ID                   = @pAccountId
     AND   ISNULL(AC_MS_POINTS_SEQ_ID, 0) <= @pPointsSequenceId

  IF @@ROWCOUNT = 1
		UPDATE CUSTOMER_BUCKET SET CBU_VALUE = @NewPoints 
		 WHERE CBU_CUSTOMER_ID = @pAccountId AND CBU_BUCKET_ID = @bucket_puntos_canje 

END
GO


IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'terminals_view')
   DROP VIEW terminals_view
GO

CREATE VIEW [dbo].[terminals_view]
AS
SELECT     dbo.terminals.te_terminal_id, dbo.terminals.te_type, dbo.terminals.te_server_id, dbo.terminals.te_name, dbo.terminals.te_external_id, dbo.terminals.te_blocked, 
                      dbo.terminals.te_timestamp, dbo.terminals.te_active, dbo.terminals.te_provider_id, dbo.terminals.te_client_id, dbo.terminals.te_build_id, 
                      dbo.play_sessions.ps_play_session_id, dbo.play_sessions.ps_account_id, dbo.play_sessions.ps_terminal_id, dbo.play_sessions.ps_type, 
                      dbo.play_sessions.ps_type_data, dbo.play_sessions.ps_status, dbo.play_sessions.ps_started, dbo.play_sessions.ps_initial_balance, 
                      dbo.play_sessions.ps_played_count, dbo.play_sessions.ps_played_amount, dbo.play_sessions.ps_won_count, dbo.play_sessions.ps_won_amount, 
                      dbo.play_sessions.ps_cash_in, dbo.play_sessions.ps_cash_out, dbo.play_sessions.ps_finished, dbo.play_sessions.ps_final_balance, 
                      dbo.play_sessions.ps_timestamp, dbo.play_sessions.ps_locked, dbo.accounts.ac_account_id, dbo.accounts.ac_type, dbo.accounts.ac_holder_name, 
                      dbo.accounts.ac_blocked, dbo.accounts.ac_not_valid_before, dbo.accounts.ac_not_valid_after, dbo.accounts.ac_balance, dbo.accounts.ac_cash_in, 
                      dbo.accounts.ac_cash_won, dbo.accounts.ac_not_redeemable, dbo.accounts.ac_timestamp, dbo.accounts.ac_track_data, dbo.accounts.ac_total_cash_in, 
                      dbo.accounts.ac_total_cash_out, dbo.accounts.ac_initial_cash_in, dbo.accounts.ac_activated, dbo.accounts.ac_deposit, dbo.accounts.ac_current_terminal_id, 
                      dbo.accounts.ac_current_terminal_name, dbo.accounts.ac_current_play_session_id, dbo.accounts.ac_last_terminal_id, dbo.accounts.ac_last_terminal_name, 
                      dbo.accounts.ac_last_play_session_id, dbo.accounts.ac_user_type, ISNULL(CBU_VALUE, 0) AS ac_points, 
                      dbo.accounts.ac_initial_not_redeemable, dbo.play_sessions.ps_stand_alone
FROM         dbo.accounts RIGHT OUTER JOIN
                      dbo.terminals_last_play_session INNER JOIN
                      dbo.play_sessions ON dbo.terminals_last_play_session.lp_play_session_id = dbo.play_sessions.ps_play_session_id ON 
                      dbo.accounts.ac_account_id = dbo.play_sessions.ps_account_id AND 
                      dbo.accounts.ac_current_play_session_id = dbo.play_sessions.ps_play_session_id RIGHT OUTER JOIN
                      dbo.terminals ON dbo.terminals_last_play_session.lp_terminal_id = dbo.terminals.te_terminal_id
                      LEFT JOIN CUSTOMER_BUCKET ON CBU_CUSTOMER_ID = AC_ACCOUNT_ID AND CBU_BUCKET_ID = 1 -- Puntos de canje
GO
