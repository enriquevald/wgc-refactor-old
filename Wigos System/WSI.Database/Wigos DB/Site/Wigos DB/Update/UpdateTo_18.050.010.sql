/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 49;

SET @New_ReleaseId = 50;
SET @New_ScriptName = N'UpdateTo_18.050.010.sql';
SET @New_Description = N'Fixed NR1 amount when exists a WonLock. New implementation of list of pending Handpays using SP.';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** STORED PROCEDURES ******/
ALTER PROCEDURE [dbo].[WSP_BalanceParts]
  @Balance        money
, @InitialCashIn  money
, @NonRedeemable1 money
, @NonRedeemable2 money
, @WonLock        money
AS
BEGIN
  DECLARE @tmp_balance          money
  DECLARE @part_nr1             money
  DECLARE @part_nr2             money
  DECLARE @part_nr_won          money
  DECLARE @lock_broken          bit
  DECLARE @part_dev             money
  DECLARE @part_won             money
  DECLARE @total_redeemable     money
  DECLARE @total_non_redeemable money
  DECLARE @part_nr1_total       money
  
  SET NOCOUNT ON;

  SET @tmp_balance = CASE WHEN ( @Balance < 0 ) THEN 0 ELSE @Balance END

  -- Non Redeemable
  --   NR Withold
  SET @part_nr1 = CASE WHEN (@tmp_balance < @NonRedeemable1) THEN @tmp_balance ELSE @NonRedeemable1 END
  SET @tmp_balance = @tmp_balance - @part_nr1
  --   NR Cover Coupon
  SET @part_nr2 = CASE WHEN (@tmp_balance < @NonRedeemable2) THEN @tmp_balance ELSE @NonRedeemable2 END
  SET @tmp_balance = @tmp_balance - @part_nr2
  --   NR Won
  SET @part_nr_won = 0
  SET @lock_broken = 0

  -- Redeemable
  --   Devolution
  SET @part_dev = CASE WHEN (@tmp_balance < @InitialCashIn) THEN @tmp_balance ELSE @InitialCashIn END
  SET @tmp_balance = @tmp_balance - @part_dev
  --   Won
  SET @part_won = @tmp_balance
   
  IF ( @WonLock <> 0 )
  BEGIN
    -- WonLock -> Won is Non Redeemable 
    SET @part_nr_won = @part_won
    SET @part_won    = 0
      IF ( @part_nr_won >= @WonLock ) SET @lock_broken = 1
  END

  SET @total_redeemable     = @part_dev + @part_won
  SET @part_nr1_total       = @part_nr1 + @part_nr_won
  SET @total_non_redeemable = @part_nr1_total + @part_nr2

  SELECT @Balance as TOTAL_BALANCE, @total_redeemable as TOTAL_REDEEMABLE, @total_non_redeemable as TOTAL_NON_REDEEMABLE, 
         @part_dev as PART_REFUND, @part_won as PART_WON, @part_nr1_total as PART_NR1, @part_nr2 as PART_NR2,
         @part_nr_won as PART_NR_WON, @lock_broken as LOCK_BROKEN

END -- WSP_BalanceParts
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WSP_AccountPendingHandpays]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[WSP_AccountPendingHandpays]
GO
CREATE PROCEDURE [dbo].[WSP_AccountPendingHandpays]
  @AccountId  BigInt
AS
BEGIN
	SET NOCOUNT ON;

  DECLARE @TypeCancelledCredits AS Int
  DECLARE @TypeJackpot AS Int
  DECLARE @TypeSiteJackpot AS Int
  DECLARE @TypeOrphanCredits AS Int
  DECLARE @oldest_handpay  AS Datetime
  DECLARE @candidate_exists as bit

  DECLARE @TimePeriod as int
  DECLARE @oldest_session_start as Datetime
  DECLARE @newest_session_end   as Datetime
  DECLARE @count                as int
  DECLARE @now   as Datetime

  DECLARE @cph_terminal_id as int
  DECLARE @cph_datetime as Datetime
  DECLARE @cph_amount as Money

  --
  -- Handpay types
  --
  SET @TypeCancelledCredits = 0
  SET @TypeJackpot          = 1
  SET @TypeSiteJackpot      = 20
  SET @TypeOrphanCredits    = 2

  --
  -- Oldest Handpay 'Not expired'
  --
  SET @now = GETDATE()

  SELECT   @TimePeriod              = CAST (GP_KEY_VALUE AS INT)
    FROM   GENERAL_PARAMS
   WHERE   GP_GROUP_KEY             = 'Cashier.HandPays'
     AND   GP_SUBJECT_KEY           = 'TimePeriod'
     AND   ISNUMERIC (GP_KEY_VALUE) = 1 --- Ensure is a numeric value

  SET @TimePeriod = ISNULL (@TimePeriod, 60)      -- Default value: 1 hour
  IF ( @TimePeriod <   5 ) SET @TimePeriod =   5  -- Minimum: 5 minutes
  IF ( @TimePeriod > 720 ) SET @TimePeriod = 720  -- Maximum: 12 h x 60 min/h = 720 min

  SET @oldest_handpay = DATEADD (MINUTE, -@TimePeriod, @now)

  --
  -- Pending Handpays
  --
  SELECT   *
    INTO   #PENDING_HANDPAYS
    FROM   HANDPAYS 
   WHERE   HP_MOVEMENT_ID IS NULL
     AND   HP_DATETIME >= @oldest_handpay
     AND   ( HP_TYPE IN (@TypeCancelledCredits, @TypeJackpot, @TypeOrphanCredits)
        OR ( HP_TYPE = @TypeSiteJackpot AND HP_SITE_JACKPOT_AWARDED_TO_ACCOUNT_ID = @AccountId ) )

  ---
  --- Loop on pending Handpays (excluding 'SiteJackpot')
  ---
  DECLARE   cursor_pending_handpays CURSOR FOR
   SELECT   HP_TERMINAL_ID, HP_DATETIME, HP_AMOUNT
      FROM  #PENDING_HANDPAYS
    WHERE   HP_TYPE <> @TypeSiteJackpot
    
  OPEN cursor_pending_handpays
  FETCH NEXT FROM cursor_pending_handpays INTO @cph_terminal_id, @cph_datetime, @cph_amount

  WHILE @@Fetch_Status = 0
  BEGIN
    
    SET @oldest_session_start = DATEADD (DAY, -1, @cph_datetime)
    SET @newest_session_end   = DATEADD (MINUTE, @TimePeriod, @cph_datetime)

    SELECT   @count = COUNT(*)
      FROM   PLAY_SESSIONS
     WHERE   PS_TERMINAL_ID = @cph_terminal_id
       AND   PS_ACCOUNT_ID  = @AccountId
       AND   PS_STARTED     >= @oldest_session_start
       AND   PS_STARTED     <  @cph_datetime
       AND   PS_STARTED     < PS_FINISHED -- Exclude 'paid handpays' sessions 
       AND   PS_FINISHED    >= @oldest_session_start
       AND   PS_FINISHED    <  @newest_session_end

    IF ( @count = 0 )
    BEGIN
      ---
      --- Not a candidate: delete it!
      ---
      DELETE   #PENDING_HANDPAYS
       WHERE   HP_TERMINAL_ID = @cph_terminal_id
         AND   HP_DATETIME    = @cph_datetime
         AND   HP_AMOUNT      = @cph_amount
    END

    --
    -- Next
    --
    FETCH NEXT FROM cursor_pending_handpays INTO @cph_terminal_id, @cph_datetime, @cph_amount

  END

  CLOSE cursor_pending_handpays
  DEALLOCATE cursor_pending_handpays

  SELECT   HP_TERMINAL_ID 
         , HP_DATETIME 
         , HP_TE_PROVIDER_ID 
         , HP_TE_NAME 
         , HP_TYPE 
         , ' '  HP_DUMMY
         , ISNULL (HP_SITE_JACKPOT_NAME, ' ')  HP_SITE_JACKPOT_NAME
         , HP_AMOUNT 
         , ISNULL (HP_SITE_JACKPOT_AWARDED_ON_TERMINAL_ID, 0)  HP_SITE_JACKPOT_AWARDED_ON_TERMINAL_ID
    FROM   #PENDING_HANDPAYS
   ORDER BY HP_DATETIME DESC

  DROP TABLE #PENDING_HANDPAYS

END -- WSP_AccountPendingHandpays
GO

/****** PERMISSIONS ******/
GRANT EXECUTE ON OBJECT::dbo.WSP_AccountPendingHandpays TO wggui;
GO
