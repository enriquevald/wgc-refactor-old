/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 285;

SET @New_ReleaseId = 286;
SET @New_ScriptName = N'UpdateTo_18.286.038.sql';
SET @New_Description = N'SP/Tables/Records for Zip Code';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/******* TABLES *******/

-- add new column in table accounts
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[accounts]') and name = 'ac_holder_address_country')
BEGIN
  ALTER TABLE dbo.accounts ADD ac_holder_address_country INT NULL 
END
GO

-- add new column in table accounts
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[accounts]') and name = 'ac_holder_address_validation')
BEGIN
  ALTER TABLE dbo.accounts ADD ac_holder_address_validation INT NOT NULL DEFAULT 0
END
GO

--TABLE COUNTRY_ZIP_CODE_SETTINGS
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[country_zip_code_settings]') AND type in (N'U'))
DROP TABLE country_zip_code_settings
GO

CREATE TABLE [dbo].[country_zip_code_settings](
	[czs_country_iso2] [nvarchar](2) NOT NULL,
	[czs_mode] [int] NOT NULL,
	[czs_format] [nvarchar](50) NULL,
	[czs_regex] [nvarchar](50) NULL,
	[czs_addr_01] [int] NULL,
	[czs_addr_02] [int] NULL,
	[czs_addr_03] [int] NULL,
	[czs_addr_04] [int] NULL,
	[czs_addr_05] [int] NULL,
 CONSTRAINT [PK_country_zip_code_settings] PRIMARY KEY CLUSTERED 
(
	[czs_country_iso2] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

--TABLE COUNTRY_ZIP_CODE_LIST
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[country_zip_code_list]') AND type in (N'U'))
DROP TABLE country_zip_code_list
GO

CREATE TABLE [dbo].[country_zip_code_list](
	[czl_country_iso2] [nvarchar](2) NOT NULL,
	[czl_zip_code] [nvarchar](10) NOT NULL,
	[czl_addr_01] [nvarchar](50) NULL,
	[czl_addr_02] [nvarchar](50) NULL,
	[czl_addr_03] [nvarchar](50) NULL,
	[czl_addr_04] [nvarchar](50) NULL,
	[czl_addr_05] [nvarchar](50) NULL
) ON [PRIMARY]
GO

/******* INDEXES *******/


IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[country_zip_code_list]') AND name = N'IX_czl_country_iso2_czl_zip_code')
  DROP INDEX [IX_czl_country_iso2_czl_zip_code] ON [dbo].[country_zip_code_list] WITH ( ONLINE = OFF )
GO
CREATE NONCLUSTERED INDEX [IX_czl_country_iso2_czl_zip_code] ON [dbo].[country_zip_code_list] 
(
	[czl_country_iso2] ASC,
	[czl_zip_code] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

/******* RECORDS *******/

-- add new general params
IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='Account.RequestedField' AND GP_SUBJECT_KEY = 'AntiMoneyLaundering.AddressCountry')
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Account.RequestedField', 'AntiMoneyLaundering.AddressCountry', '0')
IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='Account.RequestedField' AND GP_SUBJECT_KEY = 'AddressCountry')
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Account.RequestedField', 'AddressCountry', '0')
IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='Account.VisibleField' AND GP_SUBJECT_KEY = 'AddressCountry')
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Account.VisibleField', 'AddressCountry', '1')
GO

/******* PROCEDURES *******/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ZipCode_Delete]') AND type in (N'P', N'PC'))
   DROP PROCEDURE [dbo].[ZipCode_Delete]
GO

CREATE PROCEDURE [dbo].[ZipCode_Delete]
			   @country_iso2 nvarchar(2)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DELETE [country_zip_code_list]
	 WHERE czl_country_iso2 = @country_iso2 

END

GO

GRANT EXECUTE ON [dbo].[ZipCode_Delete] TO [wggui] WITH GRANT OPTION

GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ZipCode_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ZipCode_Insert]
GO

CREATE PROCEDURE [dbo].[ZipCode_Insert]
			    @country_iso2 nvarchar(2)
			   ,@zip_code nvarchar(10)
			   ,@addr_01  nvarchar(50)
			   ,@addr_02  nvarchar(50)
			   ,@addr_03  nvarchar(50)
			   ,@addr_04  nvarchar(50)
			   ,@addr_05  nvarchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO [country_zip_code_list]
			   ([czl_country_iso2]
			   ,[czl_zip_code]
			   ,[czl_addr_01]
			   ,[czl_addr_02]
			   ,[czl_addr_03]
			   ,[czl_addr_04]
			   ,[czl_addr_05]
			   )
		 VALUES
			   (@country_iso2 
			   ,@zip_code 
			   ,@addr_01 
			   ,@addr_02 
			   ,@addr_03 
			   ,@addr_04 
			   ,@addr_05 
			   )
END
GO

GRANT EXECUTE ON [dbo].[ZipCode_Insert] TO [wggui] WITH GRANT OPTION
GO
