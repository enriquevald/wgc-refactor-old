/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 413;

SET @New_ReleaseId = 414;

SET @New_ScriptName = N'UpdateTo_18.414.041.sql';
SET @New_Description = N'New release v03.006.0012'; 

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO



/**** GENERAL PARAM *****/


/******* TABLES  *******/


/******* RECORDS *******/

BEGIN
  DECLARE @rtc_form_id INT
  DECLARE @rtc_design_sheet VARCHAR(MAX)
  DECLARE @rtc_report_name VARCHAR(MAX)
  
  SET @rtc_report_name =                                    '<LanguageResources><NLS09><Label>Gambling table activity</Label></NLS09><NLS10><Label>Actividad en mesa de juego</Label></NLS10></LanguageResources>'
  SET @rtc_design_sheet =                                   '<ArrayOfReportToolDesignSheetsDTO>
                                                                <ReportToolDesignSheetsDTO>
                                                                  <LanguageResources>
                                                                    <NLS09>
                                                                      <Label>Gambling table activity</Label>
                                                                    </NLS09>
                                                                    <NLS10>
                                                                      <Label>Actividad en mesa de juego</Label>
                                                                    </NLS10>
                                                                  </LanguageResources>
                                                                  <Columns>
                                                                    <ReportToolDesignColumn>
                                                                      <Code>TIPO</Code>
                                                                      <Width>300</Width>
                                                                      <EquityMatchType>Equality</EquityMatchType>
                                                                      <LanguageResources>
                                                                        <NLS09>
                                                                          <Label>Type</Label>
                                                                        </NLS09>
                                                                        <NLS10>
                                                                          <Label>Tipo</Label>
                                                                        </NLS10>
                                                                      </LanguageResources>
                                                                    </ReportToolDesignColumn>
                                                                    <ReportToolDesignColumn>
                                                                      <Code>MESA</Code>
                                                                      <Width>300</Width>
                                                                      <EquityMatchType>Equality</EquityMatchType>
                                                                      <LanguageResources>
                                                                        <NLS09>
                                                                          <Label>Table</Label>
                                                                        </NLS09>
                                                                        <NLS10>
                                                                          <Label>Mesa</Label>
                                                                        </NLS10>
                                                                      </LanguageResources>
                                                                    </ReportToolDesignColumn>
                                                                    <ReportToolDesignColumn>
                                                                      <Code>MES</Code>
                                                                      <Width>300</Width>
                                                                      <EquityMatchType>Equality</EquityMatchType>
                                                                      <LanguageResources>
                                                                        <NLS09>
                                                                          <Label>Month</Label>
                                                                        </NLS09>
                                                                        <NLS10>
                                                                          <Label>Mes</Label>
                                                                        </NLS10>
                                                                      </LanguageResources>
                                                                    </ReportToolDesignColumn>
                                                                    <ReportToolDesignColumn>
                                                                      <Code>DIAS HABILITADA</Code>
                                                                      <Width>300</Width>
                                                                      <EquityMatchType>Equality</EquityMatchType>
                                                                      <LanguageResources>
                                                                        <NLS09>
                                                                          <Label>Enabled days</Label>
                                                                        </NLS09>
                                                                        <NLS10>
                                                                          <Label>Dias habilitada</Label>
                                                                        </NLS10>
                                                                      </LanguageResources>
                                                                    </ReportToolDesignColumn>
                                                                    <ReportToolDesignColumn>
                                                                      <Code>DIAS USADO</Code>
                                                                      <Width>300</Width>
                                                                      <EquityMatchType>Equality</EquityMatchType>
                                                                      <LanguageResources>
                                                                        <NLS09>
                                                                          <Label>Used days</Label>
                                                                        </NLS09>
                                                                        <NLS10>
                                                                          <Label>Dias usado</Label>
                                                                        </NLS10>
                                                                      </LanguageResources>
                                                                    </ReportToolDesignColumn>
                                                                  </Columns>
                                                                </ReportToolDesignSheetsDTO>
                                                              </ArrayOfReportToolDesignSheetsDTO>'
  
  IF NOT EXISTS(SELECT 1 FROM report_tool_config WHERE rtc_store_name = 'sp_GetTablesActivityFormatted')
  BEGIN
    SELECT @rtc_form_id = 11001
      FROM report_tool_config
    
    INSERT INTO [dbo].[REPORT_TOOL_CONFIG] 
			(RTC_FORM_ID,
			RTC_LOCATION_MENU,
			RTC_REPORT_NAME,
			RTC_STORE_NAME,
			RTC_DESIGN_FILTER,
			RTC_DESIGN_SHEETS,
			RTC_MAILING,
			RTC_STATUS,
			RTC_MODE_TYPE,
      RTC_HTML_HEADER,
      RTC_HTML_FOOTER)
			 
	  VALUES /*RTC_REPORT_TOOL_ID*/
      (@rtc_form_id,
			12, 
			@rtc_report_name,
			'sp_GetTablesActivityFormatted',
			'<ReportToolDesignFilterDTO><FilterType>MonthYear</FilterType></ReportToolDesignFilterDTO>',
      @rtc_design_sheet,
      1,
			1,
      11,
      NULL,
      NULL)
                                      
  END 
  ELSE
  BEGIN
      UPDATE [dbo].[REPORT_TOOL_CONFIG] 
      SET RTC_REPORT_NAME =         /*RTC_REPORT_NAME*/     @rtc_report_name,
          RTC_DESIGN_SHEETS =       /*RTC_DESIGN_SHEETS*/   @rtc_design_sheet
      WHERE RTC_STORE_NAME = 'sp_GetTablesActivityFormatted'
  END
END
GO


/******* PROCEDURES *******/

 
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ScoreReport]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[ScoreReport]
GO

CREATE  PROCEDURE [dbo].[ScoreReport]
(
    @pDateFrom        AS DATETIME
  , @pCageFillerIn    AS INT
  , @pCageFillerOut   AS INT
)
AS
BEGIN
  DECLARE @old_time AS DATETIME
  DECLARE @DateTo   AS DATETIME
  DECLARE @Horas    AS NVARCHAR(1000)
  
  DECLARE @cols  AS NVARCHAR(MAX)
  DECLARE @query AS NVARCHAR(MAX)

  SET @old_time = '2007-01-01T07:00:00'
  SET @DateTo = DATEADD(DAY, 1, @pDateFrom)  
  SET @Horas = N'[0],[1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12],[13],[14],[15],[16],[17],[18],[19],[20],[21],[22],[23],[9999]'
  
  SELECT * 
    INTO #GROUPS_BY_SESSION 
    FROM SplitStringIntoTable(REPLACE(REPLACE(@Horas, '[', ''), ']', ''), ',', 1)
  

      SELECT   GTT_NAME                             AS 'TYPE_NAME'
             , GT_NAME                              AS 'TABLE_NAME'
             , CS_OPENING_DATE                      AS 'OPENING_DATE'
             , CS_CLOSING_DATE                      AS 'CLOSING_DATE'
             , GTS_GAMING_TABLE_SESSION_ID          AS 'GAMING_TABLE_SESSION_ID'
             , CS_NAME                              AS 'SESION_NAME'
             , GTS_INITIAL_CHIPS_AMOUNT             AS 'INITIAL_BALANCE'
             , GTS_FILLS_CHIPS_AMOUNT               AS 'FINAL_BALANCE'
             , CAST(GT_NUM_SEATS AS DECIMAL(18,2))  AS 'NUM_SEATS_IN_TABLE'
             , CAST(SST_VALUE AS INT)               AS 'HORA'
             , SST_VALUE                            AS 'HORA_STR'
             , CAST(0 AS DECIMAL(18,2))             AS 'GT_DROP'
             , CAST(0 AS DECIMAL(18,2))             AS 'GT_WIN_LOSS'
             , CAST(0 AS DECIMAL(18,2))             AS 'GT_HOLD'
             , CAST(0 AS DECIMAL(18,2))             AS 'GT_OCCUPATION_NUMBER'
        INTO   #VALUE_TABLE
        FROM   GAMING_TABLES
  INNER JOIN   GAMING_TABLES_TYPES    ON GT_TYPE_ID                  = GTT_GAMING_TABLE_TYPE_ID
  INNER JOIN   GAMING_TABLES_SESSIONS ON GT_GAMING_TABLE_ID          = GTS_GAMING_TABLE_ID
  INNER JOIN   CASHIER_SESSIONS       ON GTS_CASHIER_SESSION_ID      = CS_SESSION_ID 
 CROSS APPLY   #GROUPS_BY_SESSION
       WHERE   CS_OPENING_DATE >= @pDateFrom
         AND   CS_OPENING_DATE <  @DateTo
    ORDER BY   GTT_NAME                 
             , GT_NAME                  
             , CS_OPENING_DATE          
             , CAST(SST_VALUE AS INT)

 
     --  SET DROP BY HOUR    
      UPDATE   #VALUE_TABLE
         SET   GT_DROP = GT_DROP + ISNULL((SELECT   SUM(TI_AMOUNT)
                                             FROM   GT_COPY_DEALER_VALIDATED 
                                       INNER JOIN   TICKETS  ON GTCD_TICKET_ID = TI_TICKET_ID
                                            WHERE   GAMING_TABLE_SESSION_ID = GTCD_GAMING_TABLE_SESSION_ID 
                                              AND ( DATEADD(HOUR, DATEDIFF(HOUR, @old_time, GTCD_VALIDATION_DATETIME), @old_time) = DATEADD(HOUR, HORA,  @pDateFrom)
                                               OR   HORA = 9999)), 0)
 
      --  SET WIN/LOSS WITH INITIAL BALANCE 
      UPDATE   #VALUE_TABLE
         SET   GT_WIN_LOSS = INITIAL_BALANCE
       WHERE   DATEADD(HOUR, DATEDIFF(HOUR, @old_time, OPENING_DATE), @old_time) <= DATEADD(HOUR, HORA,  @pDateFrom)
         AND   DATEADD(HOUR, DATEDIFF(HOUR, @old_time, CLOSING_DATE), @old_time) >= DATEADD(HOUR, HORA,  @pDateFrom)
          OR   HORA = 9999

      --  ADD SCORE BY HOUR TO WIN/LOSS
      UPDATE   #VALUE_TABLE
         SET   GT_WIN_LOSS = GT_WIN_LOSS + ISNULL((SELECT   GTWL_WIN_LOSS_AMOUNT
                                                     FROM   GAMING_TABLES_WIN_LOSS 
                                                    WHERE   GAMING_TABLE_SESSION_ID = GTWL_GAMING_TABLE_SESSION_ID 
                                                      AND ( DATEADD(HOUR, DATEDIFF(HOUR, @old_time, GTWL_DATETIME_HOUR), @old_time) = DATEADD(HOUR, HORA,  @pDateFrom)
                                                       OR   HORA = 9999 
                                                      AND   GTWL_DATETIME_HOUR = (SELECT  MAX(GTWL_DATETIME_HOUR) 
                                                                                    FROM  GAMING_TABLES_WIN_LOSS 
                                                                                   WHERE  GAMING_TABLE_SESSION_ID = GTWL_GAMING_TABLE_SESSION_ID 
                                                                                     AND  GTWL_DATETIME_HOUR < ISNULL(CLOSING_DATE, GETDATE())))), 0)
        
 
      --  ADD FILLS-CREDITS CHIPS BY HOUR TO WIN/LOSS
      UPDATE   #VALUE_TABLE
         SET   GT_WIN_LOSS = GT_WIN_LOSS + (CM_ADD_AMOUNT - CM_SUB_AMOUNT)
        FROM   CASHIER_MOVEMENTS 
       WHERE   GAMING_TABLE_SESSION_ID = CM_GAMING_TABLE_SESSION_ID
         AND   (DATEADD(HOUR, DATEDIFF(HOUR, @old_time, CM_DATE), @old_time) >= DATEADD(HOUR, HORA,  @pDateFrom)
          OR   HORA = 9999 )
         AND   CM_TYPE IN (@pCageFillerIn, @pCageFillerOut)
 
      --  SET HOLD BY HOUR    
      UPDATE   #VALUE_TABLE
         SET   GT_HOLD = A.GT_WIN_LOSS / A.GT_DROP * 100
        FROM   #VALUE_TABLE AS A
       WHERE   GAMING_TABLE_SESSION_ID = A.GAMING_TABLE_SESSION_ID
         AND   HORA = A.HORA
         AND   A.GT_DROP <> 0
 
      --  SET OCCUPATION BY HOUR    
      UPDATE   #VALUE_TABLE
         SET   GT_OCCUPATION_NUMBER = (SELECT   CAST(COUNT(DISTINCT GTPS_SEAT_ID) AS DECIMAL(18,2))
                                  FROM   GT_PLAY_SESSIONS 
                                 WHERE   GAMING_TABLE_SESSION_ID = GTPS_GAMING_TABLE_SESSION_ID 
                                   AND ( GTPS_STARTED <= DATEADD(HOUR, HORA,  @pDateFrom) 
                                   AND   ISNULL(GTPS_FINISHED, GETDATE()) > DATEADD(HOUR, HORA,  @pDateFrom)
                                    OR   GTPS_STARTED >= DATEADD(HOUR, HORA,  @pDateFrom)
                                   AND   GTPS_STARTED < DATEADD(HOUR, 1, DATEADD(HOUR, HORA,  @pDateFrom))))
       WHERE  NUM_SEATS_IN_TABLE <> 0
 
      --  SET OCCUPATION BY HOUR -- TOTAL --
      UPDATE   #VALUE_TABLE
         SET   GT_OCCUPATION_NUMBER = ISNULL((SELECT   SUM(A.GT_OCCUPATION_NUMBER) / CAST(SUM(CASE WHEN A.GT_OCCUPATION_NUMBER > 0 THEN 1 ELSE 0 END)  AS DECIMAL(18,2))
                                  FROM   #VALUE_TABLE AS A
                                 WHERE   #VALUE_TABLE.GAMING_TABLE_SESSION_ID = A.GAMING_TABLE_SESSION_ID 
                                   AND   A.GT_OCCUPATION_NUMBER > 0), 0)
       WHERE  HORA = 9999
  
 --PIVOT COLUMS FOR REPORT
      SELECT   @cols = STUFF((SELECT ',' + QUOTENAME( CASE WHEN HORA < 25 
                                                           THEN REPLACE(LEFT(RIGHT(CONVERT(NVARCHAR(20), DATEADD(HOUR, HORA, @pDateFrom), 120), 8), 5), ':', '') 
                                                           ELSE '9999'
                                                           END   +'-'+COL) 
        FROM   #VALUE_TABLE t
 CROSS APPLY ( SELECT 'GT_DROP', 1 UNION ALL
               SELECT 'GT_WIN_LOSS', 2 UNION ALL
               SELECT 'GT_HOLD', 3 UNION ALL
               SELECT 'GT_OCCUPATION_NUMBER', 4
             ) C (COL, SO)
    GROUP BY   COL
             , SO
             , HORA
    ORDER BY   HORA
             , SO FOR XML PATH('')
             , TYPE ).value('.', 'NVARCHAR(MAX)'),1,1,'')

set @query = 'SELECT   TYPE_NAME
                     , TABLE_NAME
                     , OPENING_DATE
                     , CLOSING_DATE
                     , GAMING_TABLE_SESSION_ID
                     , SESION_NAME
                     , INITIAL_BALANCE
                     , FINAL_BALANCE
                     , NUM_SEATS_IN_TABLE
                     , ' + @cols + ' 
                FROM ( SELECT   TYPE_NAME
                              , TABLE_NAME
                              , OPENING_DATE
                              , CLOSING_DATE
                              , GAMING_TABLE_SESSION_ID
                              , SESION_NAME
                              , INITIAL_BALANCE
                              , FINAL_BALANCE
                              , NUM_SEATS_IN_TABLE
                              , COL = CASE WHEN HORA < 25 
                                           THEN REPLACE( LEFT(  RIGHT(CONVERT(NVARCHAR(20), DATEADD(HOUR, HORA, ''' + CONVERT(NVARCHAR(20), @pDateFrom, 120) + '''), 120), 8), 5), '':'', '''' )
                                           ELSE ''9999'' 
                                           END  +''-''+COL
                              , VALUE
                         FROM   #VALUE_TABLE T
                  CROSS APPLY ( SELECT   ''GT_DROP''
                                       , GT_DROP UNION ALL
                                SELECT   ''GT_WIN_LOSS''
                                       , GT_WIN_LOSS UNION ALL
                                SELECT   ''GT_HOLD''
                                       , GT_HOLD UNION ALL
                                SELECT   ''GT_OCCUPATION_NUMBER''
                                       , GT_OCCUPATION_NUMBER
                              ) C (COL, VALUE)
                     ) X
               PIVOT ( MAX(VALUE) FOR COL IN (' + @cols + ') ) PVT '

  EXECUTE SP_EXECUTESQL @QUERY

  DROP TABLE #GROUPS_BY_SESSION
  
END --ScoreReport
GO

GRANT EXECUTE ON [dbo].[ScoreReport] TO [wggui] WITH GRANT OPTION
GO




