/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 255;

SET @New_ReleaseId = 256;
SET @New_ScriptName = N'UpdateTo_18.256.037.sql';
SET @New_Description = N'Updated alarms catalog';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/******* TABLES *******/

/******* RECORDS *******/

INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES ( 2097160, 10, 0, N'Cumplea�os del jugador hoy', N'Cumplea�os del jugador hoy', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES ( 2097160,  9, 0, N'Player birthday today', N'Player birthday today', 1)

INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES ( 2097161, 10, 0, N'Cumplea�os del jugador pr�ximamente', N'Cumplea�os del jugador pr�ximamente', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES ( 2097161,  9, 0, N'Player birthday coming soon', N'Player birthday coming soon', 1)

INSERT [dbo].[alarm_categories] ([alc_category_id], [alc_language_id], [alc_alarm_group_id], [alc_type], [alc_name], [alc_description], [alc_visible]) VALUES (49,  9, 3, 0, N'Players', N'', 1)
INSERT [dbo].[alarm_categories] ([alc_category_id], [alc_language_id], [alc_alarm_group_id], [alc_type], [alc_name], [alc_description], [alc_visible]) VALUES (49, 10, 3, 0, N'Jugadores', N'', 1)

INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES ( 2097160, 49, 0, GETDATE() )
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES ( 2097161, 49, 0, GETDATE() )
GO

UPDATE ALARM_CATALOG SET ALCG_NAME = 'Meter Big Increment', ALCG_DESCRIPTION = 'Meter Big Increment' WHERE ALCG_ALARM_CODE = 131081
UPDATE ALARM_CATALOG SET ALCG_NAME = 'Meter Rollover',      ALCG_DESCRIPTION = 'Meter Rollover'      WHERE ALCG_ALARM_CODE = 212994
GO
