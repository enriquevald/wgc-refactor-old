/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 164;

SET @New_ReleaseId = 165;
SET @New_ScriptName = N'UpdateTo_18.165.032.sql';
SET @New_Description = N'Update to TITO mode';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/

/************************  tickets ****************/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tickets](	
	[ti_ticket_id] [bigint] IDENTITY(1,1) NOT NULL,
	[ti_validation_number] [bigint] NOT NULL,
	[ti_amount] [money] NOT NULL,
	[ti_status] [int] NOT NULL,	
	[ti_created_datetime] [datetime] NOT NULL,
	[ti_created_terminal_id] [int] NOT NULL,	
	[ti_created_terminal_type] [int] NOT NULL,	
	[ti_expiration_datetime] [datetime] NULL,
	[ti_money_collection_id] [bigint] NULL,
	[ti_created_account_id] [bigint] NULL,		
	[ti_promotion_id] [bigint] NULL,
	[ti_collected] [bit] NULL,		
	[ti_validation_type] [int] NULL,
	[ti_type_id] [int] NOT NULL,
	[ti_last_action_terminal_id] [int] NULL,
	[ti_last_action_datetime] [datetime] NULL,
	[ti_last_action_account_id] [bigint] NULL,
	[ti_last_action_terminal_type] [int] NULL,
	[ti_machine_number] [int] NULL,
	[ti_transaction_id] [bigint] NULL,
	[ti_created_play_session_id] [bigint] NULL,
  [ti_canceled_play_session_id] [bigint] NULL,
  [ti_operation_id] [bigint] NULL

 CONSTRAINT [PK_tickets] PRIMARY KEY CLUSTERED 
(
	[ti_ticket_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
                                                                      
/************************  stackers ****************/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[stackers](
	[st_stacker_id] [bigint] NOT NULL,
	[st_model] [nvarchar](50) NOT NULL,
	[st_inserted] [int] NULL,
	[st_notes] [nvarchar](200) NULL,	
	[st_terminal_preassigned] [int] NULL,
	[st_provider_preassigned] [int] NULL,
	[st_status] [int] NOT NULL     

 CONSTRAINT [PK_stackers] PRIMARY KEY CLUSTERED 
(
	[st_stacker_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


/************************  money_collection_meters ****************/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[money_collection_meters](
	[mcm_money_collection_id] [bigint] NOT NULL,
	[mcm_session_id] [bigint] NOT NULL,
	[mcm_bills_in_stacker_num] [bigint] NULL,
	[mcm_terminal_id] [int] NOT NULL,
	[mcm_started] [datetime] NULL,
	[mcm_finished] [datetime] NULL,
	[mcm_last_reported] [datetime] NULL,
	[mcm_error_reporting] [bit] NULL,
	[mcm_cashable_num] [bigint] NULL,
	[mcm_cashable_amount] [money] NULL,
	[mcm_promo_nr_num] [bigint] NULL,
	[mcm_promo_nr_amount] [money] NULL,
	[mcm_promo_re_num] [int] NULL,
	[mcm_promo_re_amount] [money] NULL,
	[mcm_1_bill_num] [int] NULL,
	[mcm_2_bill_num] [int] NULL,
	[mcm_5_bill_num] [int] NULL,
	[mcm_10_bill_num] [int] NULL,
	[mcm_20_bill_num] [int] NULL,
	[mcm_25_bill_num] [int] NULL,
	[mcm_50_bill_num] [int] NULL,
	[mcm_100_bill_num] [int] NULL,
	[mcm_200_bill_num] [int] NULL,
	[mcm_250_bill_num] [int] NULL,
	[mcm_500_bill_num] [int] NULL,
	[mcm_1000_bill_num] [int] NULL,
	[mcm_2000_bill_num] [int] NULL,
	[mcm_2500_bill_num] [int] NULL,
	[mcm_5000_bill_num] [int] NULL,
	[mcm_10000_bill_num] [int] NULL,
	[mcm_20000_bill_num] [int] NULL,
	[mcm_25000_bill_num] [int] NULL,
	[mcm_50000_bill_num] [int] NULL,
	[mcm_100000_bill_num] [int] NULL,
	[mcm_200000_bill_num] [int] NULL,
	[mcm_250000_bill_num] [int] NULL,
	[mcm_500000_bill_num] [int] NULL,
	[mcm_1000000_bill_num] [int] NULL,
 CONSTRAINT [PK_money_collection_meters] PRIMARY KEY CLUSTERED 
(
	[mcm_session_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


/************************  cashier_terminal_money ****************/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cashier_terminal_money](
	[ctm_session_id] [bigint] NOT NULL,  
	[ctm_terminal_id] [int] NOT NULL,
	[ctm_denomination] [money] NOT NULL,
	[ctm_quantity] [int] NOT NULL,	
	[ctm_money_collection_id] [bigint] NOT NULL

 CONSTRAINT [PK_cashier_terminal_money] PRIMARY KEY CLUSTERED 
(
	[ctm_session_id] ASC, [ctm_terminal_id] ASC, [ctm_denomination] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


/************************  terminal_sas_meters ****************/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[terminal_sas_meters](
	[tsm_terminal_id] [int] NOT NULL,
	[tsm_meter_code] [int] NOT NULL,
	[tsm_game_id] [int] NOT NULL,
	[tsm_denomination] [money] NOT NULL,
	[tsm_wcp_sequence_id] [bigint] NOT NULL,
	[tsm_last_reported] [datetime] NOT NULL,
	[tsm_last_modified] [datetime] NULL,
	[tsm_meter_value] [bigint] NOT NULL,
	[tsm_meter_max_value] [bigint] NOT NULL,
	[tsm_delta_value] [bigint] NOT NULL,
	[tsm_raw_delta_value] [bigint] NOT NULL,
	[tsm_delta_updating] [bit] NOT NULL,
 CONSTRAINT [PK_terminal_sas_meters] PRIMARY KEY CLUSTERED 
(
	[tsm_terminal_id] ASC,
	[tsm_meter_code] ASC,
	[tsm_game_id] ASC,
	[tsm_denomination] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


/************************  terminal_sas_meters_history ****************/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[terminal_sas_meters_history](
	[tsmh_terminal_id] [int] NOT NULL,
	[tsmh_meter_code] [int] NOT NULL,
	[tsmh_game_id] [int] NOT NULL,
	[tsmh_denomination] [money] NOT NULL,
	[tsmh_type] [int] NOT NULL,
	[tsmh_datetime] [datetime] NOT NULL,
	[tsmh_meter_ini_value] [bigint] NULL,
	[tsmh_meter_fin_value] [bigint] NOT NULL,
	[tsmh_meter_increment] [bigint] NOT NULL,
	[tsmh_raw_meter_increment] [bigint] NOT NULL,
	[tsmh_last_reported] [datetime] NULL,
 CONSTRAINT [PK_terminal_sas_meters_history] PRIMARY KEY CLUSTERED 
(
	[tsmh_terminal_id] ASC,
	[tsmh_meter_code] ASC,
	[tsmh_game_id] ASC,
	[tsmh_denomination] ASC,
	[tsmh_type] ASC,
	[tsmh_datetime] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


/************************  sas_meters_catalog ****************/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sas_meters_catalog](
	[smc_meter_code] [int] NOT NULL,
	[smc_description] [nvarchar](max) NOT NULL,
	[smc_recomended] [int] NOT NULL,
	[smc_required] [int] NULL,
 CONSTRAINT [PK_sas_meters_catalog] PRIMARY KEY CLUSTERED 
(
	[smc_meter_code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


/************************  sas_meters_groups ****************/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sas_meters_groups](
	[smg_group_id] [int] NOT NULL,
	[smg_name] [nvarchar](50) NOT NULL,
	[smg_description] [nvarchar](200) NULL,
	[smg_required] [bit] NOT NULL,
 CONSTRAINT [PK_sas_meters_groups] PRIMARY KEY CLUSTERED 
(
	[smg_group_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


/************************  sas_meters_catalog_per_group ****************/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sas_meters_catalog_per_group](
	[smcg_group_id] [int] NOT NULL,
	[smcg_meter_code] [int] NOT NULL,
 CONSTRAINT [PK_sas_meters_catalog_per_group] PRIMARY KEY CLUSTERED 
(
	[smcg_group_id] ASC,
	[smcg_meter_code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[sas_meters_catalog_per_group]  WITH CHECK ADD  CONSTRAINT [FK_sas_meters_catalog_per_group_sas_meters_catalog] FOREIGN KEY([smcg_meter_code])
REFERENCES [dbo].[sas_meters_catalog] ([smc_meter_code])
GO
ALTER TABLE [dbo].[sas_meters_catalog_per_group] CHECK CONSTRAINT [FK_sas_meters_catalog_per_group_sas_meters_catalog]
GO
ALTER TABLE [dbo].[sas_meters_catalog_per_group]  WITH CHECK ADD  CONSTRAINT [FK_sas_meters_catalog_per_group_sas_meters_groups] FOREIGN KEY([smcg_group_id])
REFERENCES [dbo].[sas_meters_groups] ([smg_group_id])
GO
ALTER TABLE [dbo].[sas_meters_catalog_per_group] CHECK CONSTRAINT [FK_sas_meters_catalog_per_group_sas_meters_groups]
GO
	  

/************************  terminals ****************/

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[terminals]') and name = 'te_sequence_id')
		ALTER TABLE [dbo].[terminals]		       ADD [te_sequence_id] [bigint] NULL DEFAULT((0))
GO
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[terminals]') and name = 'te_validation_type')
		ALTER TABLE [dbo].[terminals]				   ADD [te_validation_type] [int] NULL
GO
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[terminals]') and name = 'te_allowed_cashable_emission')
		ALTER TABLE [dbo].[terminals] 	  	   ADD [te_allowed_cashable_emission] [bit] NULL
GO
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[terminals]') and name = 'te_allowed_promo_emission')
		ALTER TABLE [dbo].[terminals] 	  	   ADD [te_allowed_promo_emission] [bit] NULL
GO
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[terminals]') and name = 'te_allowed_redemption')
		ALTER TABLE [dbo].[terminals] 	  	   ADD [te_allowed_redemption] [bit] NULL	  
GO
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[terminals]') and name = 'te_max_allowed_ti')
		ALTER TABLE [dbo].[terminals] 	  	   ADD [te_max_allowed_ti] [money] NULL	  
GO
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[terminals]') and name = 'te_max_allowed_to')
		ALTER TABLE [dbo].[terminals] 	  		 ADD [te_max_allowed_to] [money] NULL	  
GO
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[terminals]') and name = 'te_sas_version')
		ALTER TABLE [dbo].[terminals] 	  		 ADD [te_sas_version] nvarchar(10) NULL	  
GO
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[terminals]') and name = 'te_sas_machine_name')
		ALTER TABLE [dbo].[terminals] 	  		 ADD [te_sas_machine_name] nvarchar (10) NULL	  
GO
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[terminals]') and name = 'te_bonus_flags')
		ALTER TABLE [dbo].[terminals] 	  		 ADD [te_bonus_flags] [int] NULL	  
GO
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[terminals]') and name = 'te_features_bytes')
		ALTER TABLE [dbo].[terminals] 	  		 ADD [te_features_bytes] [int] NULL	  
GO	
	
	
/************************  cashier_terminals ****************/

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[cashier_terminals]') and name = 'ct_sequence_id')
		ALTER TABLE [dbo].[cashier_terminals]    ADD [ct_sequence_id] [bigint] NULL
GO
	  
		
/************************  money_collections ****************/

IF EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[money_collections]') and name = 'mc_user_id')
		ALTER TABLE [dbo].[money_collections]	   ALTER COLUMN [mc_user_id] [int] NULL
GO
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[money_collections]') and name = 'mc_notes')
		ALTER TABLE [dbo].[money_collections]	   ADD [mc_notes] [nvarchar](200) NULL
GO
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[money_collections]') and name = 'mc_stacker_id')
		ALTER TABLE [dbo].[money_collections]	   ADD [mc_stacker_id] [bigint] NULL
GO
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[money_collections]') and name = 'mc_inserted_user_id')
		ALTER TABLE [dbo].[money_collections]	   ADD [mc_inserted_user_id] [int] NULL
GO
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[money_collections]') and name = 'mc_collection_datetime')
		ALTER TABLE [dbo].[money_collections]	   ADD [mc_collection_datetime] [datetime] NULL
GO
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[money_collections]') and name = 'mc_extraction_datetime')
		ALTER TABLE [dbo].[money_collections]	   ADD [mc_extraction_datetime] [datetime] NULL
GO
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[money_collections]') and name = 'mc_status')
		ALTER TABLE [dbo].[money_collections]	   ADD [mc_status] [int] NULL
GO
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[money_collections]') and name = 'mc_cashier_id')
		ALTER TABLE [dbo].[money_collections]		 ADD  [mc_cashier_id] [int] NULL
GO


/************************  terminal_money ****************/

IF EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[terminal_money]') and name = 'tm_into_acceptor')
		ALTER TABLE [dbo].[terminal_money]		   ALTER COLUMN [tm_into_acceptor] [bit] NULL
GO	  
  
  
/************************  promotions ****************/

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[promotions]') and name = 'pm_ticket_quantity')
		ALTER TABLE [dbo].[promotions]	   ADD [pm_ticket_quantity] [int] NULL
GO
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[promotions]') and name = 'pm_generated_tickets')
		ALTER TABLE [dbo].[promotions]	   ADD [pm_generated_tickets] [int] NULL
GO

/************************  terminals ****************/

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[terminals]') and name = 'te_virtual_account_id')
		ALTER TABLE [dbo].[terminals]
				ADD [te_virtual_account_id] [bigint]
GO

/************************  cashier_terminals ****************/

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[cashier_terminals]') and name = 'ct_virtual_account_id')
		ALTER TABLE [dbo].[cashier_terminals]
				ADD [ct_virtual_account_id] [bigint]
GO

/************************  play_sessions ****************/

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[play_sessions]') and name = 'ps_re_ticket_in')
		ALTER TABLE [dbo].[play_sessions]	   ADD [ps_re_ticket_in] [money] NULL
GO
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[play_sessions]') and name = 'ps_promo_re_ticket_in')
		ALTER TABLE [dbo].[play_sessions]	   ADD [ps_promo_re_ticket_in] [money] NULL
GO
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[play_sessions]') and name = 'ps_promo_nr_ticket_in')
		ALTER TABLE [dbo].[play_sessions]	   ADD [ps_promo_nr_ticket_in] [money] NULL
GO		
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[play_sessions]') and name = 'ps_re_ticket_out')
		ALTER TABLE [dbo].[play_sessions]	   ADD [ps_re_ticket_out] [money] NULL
GO
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[play_sessions]') and name = 'ps_promo_nr_ticket_out')
		ALTER TABLE [dbo].[play_sessions]	   ADD [ps_promo_nr_ticket_out] [money] NULL
GO
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[play_sessions]') and name = 'ps_bills_in_amount')
		ALTER TABLE [dbo].[play_sessions]	   ADD [ps_bills_in_amount] [money] NULL
GO	  
IF EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[play_sessions]') and name = 'ps_total_cash_in')
		ALTER TABLE [dbo].[play_sessions]	   DROP COLUMN [ps_total_cash_in] 	
GO	  
		ALTER TABLE [dbo].[play_sessions]	   ADD [ps_total_cash_in] AS 	((((([ps_initial_balance]+[ps_cash_in])+isnull([ps_re_ticket_in],(0)))+isnull([ps_promo_re_ticket_in],(0)))+isnull([ps_promo_nr_ticket_in],(0)))+isnull([ps_bills_in_amount],(0)))
GO
IF EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[play_sessions]') and name = 'ps_total_cash_out')
		ALTER TABLE [dbo].[play_sessions]	   DROP COLUMN [ps_total_cash_out] 		
GO	  
		ALTER TABLE [dbo].[play_sessions]	   ADD [ps_total_cash_out] AS (((isnull([ps_final_balance],(0))+[ps_cash_out])+isnull([ps_re_ticket_out],(0)))+isnull([ps_promo_nr_ticket_out],(0)))	
GO


/************************  handpays ****************/

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[handpays]') and name = 'hp_ticket_id')
		ALTER TABLE [dbo].[handpays]	   ADD [hp_ticket_id] [bigint]  NULL
GO
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[handpays]') and name = 'hp_transaction_id')
		ALTER TABLE [dbo].[handpays]	   ADD [hp_transaction_id] [bigint]  NULL
GO
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[handpays]') and name = 'hp_candidate_play_session_id')
		ALTER TABLE [dbo].[handpays]	   ADD [hp_candidate_play_session_id] [bigint] NULL
GO
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[handpays]') and name = 'hp_candidate_prev_play_session_id')
		ALTER TABLE [dbo].[handpays]	   ADD [hp_candidate_prev_play_session_id] [bigint] NULL
GO


/****** INDEXES ******/

/************************  tickets ****************/

IF  NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[tickets]') AND name = N'IX_ti_validation_number_status')
  CREATE NONCLUSTERED INDEX [IX_ti_validation_number_status] ON [dbo].[tickets] 
  (
        [ti_validation_number] ASC,
        [ti_status] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
  
  
/****** RECORDS ******/

/****** CREATE USER 'SYS TITO user' used for promotions and promo expiration. ******/
DECLARE @user_id AS INT
IF NOT EXISTS (
               SELECT   1
                 FROM   GUI_USERS
                WHERE   GU_USER_TYPE = 4
              )
BEGIN
  SELECT   @user_id = MAX(ISNULL(GU_USER_ID, 0)) + 1 
    FROM   GUI_USERS                      

  INSERT INTO GUI_USERS ( GU_USER_ID, GU_PROFILE_ID, GU_USERNAME, GU_ENABLED, GU_PASSWORD               , GU_NOT_VALID_BEFORE, GU_NOT_VALID_AFTER, GU_LAST_CHANGED, GU_PASSWORD_EXP, GU_PWD_CHG_REQ, GU_FULL_NAME   , GU_LOGIN_FAILURES, GU_USER_TYPE)
                 VALUES ( @user_id  , 1            , 'SysTITO'  , 1         , CAST('0000' AS BINARY(40)), GETDATE() - 3      , NULL              , GETDATE()      , NULL           , 0             ,'SYS TITO user' , 0                , 4)
END

/************************  general_params ****************/
IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='TITO' AND GP_SUBJECT_KEY='TITOMode')
			INSERT INTO GENERAL_PARAMS  (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('TITO','TITOMode','0')
GO
IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='TITO' AND GP_SUBJECT_KEY='Tickets.MaxAllowedTicketIn')           
			INSERT INTO GENERAL_PARAMS  (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('TITO','Tickets.MaxAllowedTicketIn','4000')
GO
IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='TITO' AND GP_SUBJECT_KEY='Tickets.MaxAllowedTicketOut')  
			INSERT INTO GENERAL_PARAMS  (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('TITO','Tickets.MaxAllowedTicketOut','6500')
GO
IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='TITO' AND GP_SUBJECT_KEY='NoteAcCollectionType')             
			INSERT INTO GENERAL_PARAMS  (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('TITO','NoteAcCollectionType','2')
GO
IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='TITO' AND GP_SUBJECT_KEY='CashableTickets.AllowEmission')  
			INSERT INTO GENERAL_PARAMS  (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES	('TITO','CashableTickets.AllowEmission','1')
GO
IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='TITO' AND GP_SUBJECT_KEY='PromotionalTickets.AllowEmission')  					
			INSERT INTO GENERAL_PARAMS  (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES	('TITO','PromotionalTickets.AllowEmission','1')
GO					
IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='TITO' AND GP_SUBJECT_KEY='TicketsCollection')  
			INSERT INTO GENERAL_PARAMS  (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('TITO','TicketsCollection','0')
GO
IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='TITO' AND GP_SUBJECT_KEY='TicketsSystemId')  					 
			INSERT INTO GENERAL_PARAMS  (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES	('TITO','TicketsSystemId','5')
GO
IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='TITO' AND GP_SUBJECT_KEY='Tickets.LimitBeforePrize')  					
			INSERT INTO GENERAL_PARAMS  (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES	('TITO','Tickets.LimitBeforePrize','300')
GO	
IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='TITO' AND GP_SUBJECT_KEY='Tickets.Location')  					
			INSERT INTO GENERAL_PARAMS  (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES	('TITO','Tickets.Location','Win-Systems')
GO
IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='TITO' AND GP_SUBJECT_KEY='Tickets.Address1')  	 				
			INSERT INTO GENERAL_PARAMS  (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES	('TITO','Tickets.Address1','Address1')
GO
IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='TITO' AND GP_SUBJECT_KEY='Tickets.Address2')  					
			INSERT INTO GENERAL_PARAMS  (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES	('TITO','Tickets.Address2','Address2')
GO
IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='TITO' AND GP_SUBJECT_KEY='DebitTickets.Title')  					
			INSERT INTO GENERAL_PARAMS  (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES	('TITO','DebitTickets.Title','DebitTickets.Title')
GO					
IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='TITO' AND GP_SUBJECT_KEY='Tickets.ShowLastNNumbers')  	
			INSERT INTO GENERAL_PARAMS  (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('TITO','Tickets.ShowLastNNumbers','0')		
GO			
IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='TITO' AND GP_SUBJECT_KEY='CashableTickets.ExpirationDays')          
			INSERT INTO GENERAL_PARAMS  (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES	('TITO','CashableTickets.ExpirationDays','200')
GO
IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='TITO' AND GP_SUBJECT_KEY='PromotionalTickets.Redeemable.ExpirationDays')  					
			INSERT INTO GENERAL_PARAMS  (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES	('TITO','PromotionalTickets.Redeemable.ExpirationDays','200')
GO
IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='TITO' AND GP_SUBJECT_KEY='PromotionalTickets.NonRedeemable.ExpirationDays')  				
			INSERT INTO GENERAL_PARAMS  (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES	('TITO','PromotionalTickets.NonRedeemable.ExpirationDays','200')
GO					
IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='TITO' AND GP_SUBJECT_KEY='CashableTickets.Title')
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('TITO','CashableTickets.Title','CASHOUT TICKET')
GO
IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='TITO' AND GP_SUBJECT_KEY='PromotionalTickets.NonRedeemable.Title')
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('TITO','PromotionalTickets.NonRedeemable.Title','PLAYABLE ONLY')
GO
IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='TITO' AND GP_SUBJECT_KEY='PromotionalTickets.Redeemable.Title')
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('TITO','PromotionalTickets.Redeemable.Title','CASHABLE PROMO')
GO
IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='TITO' AND GP_SUBJECT_KEY='TicketInfo.LblSequence')
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('TITO','TicketInfo.LblSequence','TICKET# ')
GO
IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='TITO' AND GP_SUBJECT_KEY='TicketInfo.LblMachine')
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('TITO','TicketInfo.LblMachine','MACHINE# ')
GO
IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='TITO' AND GP_SUBJECT_KEY='TicketInfo.LblExpiration')
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('TITO','TicketInfo.LblExpiration','Ticket Void after ')
GO
IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='TITO' AND GP_SUBJECT_KEY='TicketInfo.LblValidation')
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('TITO','TicketInfo.LblValidation','VALIDATION ')
GO
IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='TITO' AND GP_SUBJECT_KEY='TicketInfo.FormatDate')
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('TITO','TicketInfo.FormatDate','dd/MM/yyyy')
GO
IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='TITO' AND GP_SUBJECT_KEY='TicketInfo.FormatTime')
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('TITO','TicketInfo.FormatTime','HH:mm:ss')
GO
IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='TITO' AND GP_SUBJECT_KEY='Cashier.TicketsCollection')
			INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('TITO','Cashier.TicketsCollection','0')
GO
IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='TITO' AND GP_SUBJECT_KEY='AllowRedemption')
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('TITO','AllowRedemption','1')
GO
IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='TITO' AND GP_SUBJECT_KEY='DailySasMetersMinutes')
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('TITO','DailySasMetersMinutes','')
GO
IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='TITO' AND GP_SUBJECT_KEY='DailySasMetersHour')
			INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('TITO','DailySasMetersHour','')
GO

--IF EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='Cage' AND GP_SUBJECT_KEY='Enabled')
--			UPDATE GENERAL_PARAMS SET GP_KEY_VALUE='1' WHERE GP_GROUP_KEY ='Cage' AND GP_SUBJECT_KEY='Enabled'
--GO

/************************  sas_meters_catalog ****************/
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (0, N'Total coin in credits', 7, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (1, N'Total coin out credits', 7, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (2, N'Total jackpot credits', 7, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (3, N'Total hand paid cancelled credits', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (4, N'Total cancelled credits', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (5, N'Games played', 7, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (6, N'Games won', 7, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (7, N'Games lost', 7, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (8, N'Total credits from coin acceptor', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (9, N'Total credits paid from hopper', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (10, N'Total credits from coins to drop', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (11, N'Total credits from bills accepted', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (12, N'Current credits', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (13, N'Total SAS cashable ticket in, including nonrestricted tickets (cents) [same as meter 0080 + 0084]', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (14, N'Total SAS cashable ticket out, including debit tickets (cents) [same as meter 0086 + 008A]', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (15, N'Total SAS restricted ticket in (cents) [same as meter 0082]', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (16, N'Total SAS restricted ticket out (cents) [same as meter 0088]', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (17, N'Total SAS cashable ticket in, including nonrestricted tickets (quantity) [same as meter 0081 + 0085]', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (18, N'Total SAS cashable ticket out, including debit tickets (quantity) [same as meter 0087 + 008B]', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (19, N'Total SAS restricted ticket in (quantity) [same as meter 0083]', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (20, N'Total SAS restricted ticket out (quantity)  [same as meter 0089]', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (21, N'Total ticket in, including cashable, nonrestricted and restricted tickets (credits)', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (22, N'Total ticket out, including cashable, nonrestricted, restricted and debit tickets (credits)', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (23, N'Total electronic transfers to gaming machine, including cashable, nonrestricted, restricted and debit, whether transfer is to credit meter or to ticket (credits)', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (24, N'Total electronic transfers to host, including cashable, nonrestricted, restricted and win amounts (credits)', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (25, N'Total restricted amount played (credits)', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (26, N'Total nonrestricted amount played (credits)', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (27, N'Current restricted credits', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (28, N'Total machine paid paytable win, not including progressive or external bonus amounts (credits)', 7, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (29, N'Total machine paid progressive win (credits)', 7, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (30, N'Total machine paid external bonus win (credits)', 7, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (31, N'Total attendant paid paytable win, not including progressive or external bonus amounts (credits)', 7, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (32, N'Total attendant paid progressive win (credits)', 7, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (33, N'Total attendant paid external bonus win (credits)', 7, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (34, N'Total won credits (sum of total coin out and total jackpot)', 7, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (35, N'Total hand paid credits (sum of total hand paid cancelled credits and total jackpot)', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (36, N'Total drop, including but not limited to coins to drop, bills to drop, tickets to drop, and electronic in (credits)', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (37, N'Games since last power reset', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (38, N'Games since slot door closure', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (39, N'Total credits from external coin acceptor', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (40, N'Total cashable ticket in, including nonrestricted promotional tickets (credits)', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (41, N'Total regular cashable ticket in (credits)', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (42, N'Total restricted promotional ticket in (credits)', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (43, N'Total nonrestricted promotional ticket in (credits)', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (44, N'Total cashable ticket out, including debit tickets (credits)', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (45, N'Total restricted promotional ticket out (credits)', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (46, N'Electronic regular cashable transfers to gaming machine, not including external bonus awards (credits)', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (47, N'Electronic restricted promotional transfers to gaming machine, not including external bonus awards (credits)', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (48, N'Electronic nonrestricted promotional transfers to gaming machine, not including external bonus awards (credits)', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (49, N'Electronic debit transfers to gaming machine (credits)', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (50, N'Electronic regular cashable transfers to host (credits)', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (51, N'Electronic restricted promotional transfers to host (credits)', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (52, N'Electronic nonrestricted promotional transfers to host (credits)', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (53, N'Total regular cashable ticket in (quantity)', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (54, N'Total restricted promotional ticket in (quantity)', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (55, N'Total nonrestricted promotional ticket in (quantity)', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (56, N'Total cashable ticket out, including debit tickets (quantity)', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (57, N'Total restricted promotional ticket out (quantity)', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (62, N'Number of bills currently in the stacker (Issue exception 7B when this meter is reset)', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (63, N'Total value of bills currently in the stacker (credits) (Issue exception 7B when this meter is reset)', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (64, N'Total number of $1.00 bills accepted', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (65, N'Total number of $2.00 bills accepted', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (66, N'Total number of $5.00 bills accepted', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (67, N'Total number of $10.00 bills accepted', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (68, N'Total number of $20.00 bills accepted', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (69, N'Total number of $25.00 bills accepted', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (70, N'Total number of $50.00 bills accepted', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (71, N'Total number of $100.00 bills accepted', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (72, N'Total number of $200.00 bills accepted', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (73, N'Total number of $250.00 bills accepted', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (74, N'Total number of $500.00 bills accepted', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (75, N'Total number of $1,000.00 bills accepted', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (76, N'Total number of $2,000.00 bills accepted', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (77, N'Total number of $2,500.00 bills accepted', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (78, N'Total number of $5,000.00 bills accepted', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (79, N'Total number of $10,000.00 bills accepted', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (80, N'Total number of $20,000.00 bills accepted', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (81, N'Total number of $25,000.00 bills accepted', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (82, N'Total number of $50,000.00 bills accepted', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (83, N'Total number of $100,000.00 bills accepted', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (84, N'Total number of $200,000.00 bills accepted', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (85, N'Total number of $250,000.00 bills accepted', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (86, N'Total number of $500,000.00 bills accepted', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (87, N'Total number of $1,000,000.00 bills accepted', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (88, N'Total credits from bills to drop', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (89, N'Total number of $1.00 bills to drop', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (90, N'Total number of $2.00 bills to drop', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (91, N'Total number of $5.00 bills to drop', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (92, N'Total number of $10.00 bills to drop', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (93, N'Total number of $20.00 bills to drop', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (94, N'Total number of $50.00 bills to drop', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (95, N'Total number of $100.00 bills to drop', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (96, N'Total number of $200.00 bills to drop', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (97, N'Total number of $500.00 bills to drop', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (98, N'Total number of $1000.00 bills to drop', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (99, N'Total credits from bills diverted to hopper', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (100, N'Total number of $1.00 bills diverted to hopper', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (101, N'Total number of $2.00 bills diverted to hopper', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (102, N'Total number of $5.00 bills diverted to hopper', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (103, N'Total number of $10.00 bills diverted to hopper', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (104, N'Total number of $20.00 bills diverted to hopper', 1, NULL)
GO

INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (105, N'Total number of $50.00 bills diverted to hopper', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (106, N'Total number of $100.00 bills diverted to hopper', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (107, N'Total number of $200.00 bills diverted to hopper', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (108, N'Total number of $500.00 bills diverted to hopper', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (109, N'Total number of $1000.00 bills diverted to hopper', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (110, N'Total credits from bills dispensed from hopper', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (111, N'Total number of $1.00 bills dispensed from hopper', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (112, N'Total number of $2.00 bills dispensed from hopper', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (113, N'Total number of $5.00 bills dispensed from hopper', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (114, N'Total number of $10.00 bills dispensed from hopper', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (115, N'Total number of $20.00 bills dispensed from hopper', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (116, N'Total number of $50.00 bills dispensed from hopper', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (117, N'Total number of $100.00 bills dispensed from hopper', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (118, N'Total number of $200.00 bills dispensed from hopper', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (119, N'Total number of $500.00 bills dispensed from hopper', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (120, N'Total number of $1000.00 bills dispensed from hopper', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (127, N'Weighted average theoretical payback percentage in hundredths of a percent', 3, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (128, N'Regular cashable ticket in (cents)', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (129, N'Regular cashable ticket in (quantity)', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (130, N'Restricted ticket in (cents)', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (131, N'Restricted ticket in (quantity)', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (132, N'Nonrestricted ticket in (cents)', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (133, N'Nonrestricted ticket in (quantity)', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (134, N'Regular cashable ticket out (cents)', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (135, N'Regular cashable ticket out (quantity)', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (136, N'Restricted ticket out (cents)', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (137, N'Restricted ticket out (quantity)', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (138, N'Debit ticket out (cents)', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (139, N'Debit ticket out (quantity)', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (140, N'Validated cancelled credit handpay, receipt printed (cents)', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (141, N'Validated cancelled credit handpay, receipt printed (quantity)', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (142, N'Validated jackpot handpay, receipt printed (cents)', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (143, N'Validated jackpot handpay, receipt printed (quantity)', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (144, N'Validated cancelled credit handpay, no receipt (cents)', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (145, N'Validated cancelled credit handpay, no receipt (quantity)', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (146, N'Validated jackpot handpay, no receipt (cents)', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (147, N'Validated jackpot handpay, no receipt (quantity)', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (160, N'In-house cashable transfers to gaming machine (cents)', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (161, N'In-House transfers to gaming machine that included cashable amounts (quantity)', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (162, N'In-house restricted transfers to gaming machine (cents)', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (163, N'In-house transfers to gaming machine that included restricted amounts (quantity)', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (164, N'In-house nonrestricted transfers to gaming machine (cents)', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (165, N'In-house transfers to gaming machine that included nonrestricted amounts (quantity)', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (166, N'Debit transfers to gaming machine (cents)', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (167, N'Debit transfers to gaming machine (quantity)', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (168, N'In-house cashable transfers to ticket (cents)', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (169, N'In-house cashable transfers to ticket (quantity)', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (170, N'In-house restricted transfers to ticket (cents)', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (171, N'In-house restricted transfers to ticket (quantity)', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (172, N'Debit transfers to ticket (cents)', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (173, N'Debit transfers to ticket (quantity)', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (174, N'Bonus cashable transfers to gaming machine (cents)', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (175, N'Bonus transfers to gaming machine that included cashable amounts (quantity)', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (176, N'Bonus nonrestricted transfers to gaming machine (cents)', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (177, N'Bonus transfers to gaming machine that included nonrestricted amounts (quantity)', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (184, N'In-house cashable transfers to host (cents)', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (185, N'In-house transfers to host that included cashable amounts (quantity)', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (186, N'In-house restricted transfers to host (cents)', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (187, N'In-house transfers to host that included restricted amounts (quantity)', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (188, N'In-house nonrestricted transfers to host (cents)', 1, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required]) VALUES (189, N'In-house transfers to host that included nonrestricted amounts (quantity)', 1, NULL)
GO
	

/************************  sas_meters_groups ****************/
INSERT INTO sas_meters_groups (smg_group_id,smg_name,smg_description,smg_required) VALUES (1,'System Bills','',0)
INSERT INTO sas_meters_groups (smg_group_id,smg_name,smg_description,smg_required) VALUES (2,'Stackers','',0)
INSERT INTO sas_meters_groups (smg_group_id,smg_name,smg_description,smg_required) VALUES (10001,'Tickets','',0)
INSERT INTO sas_meters_groups (smg_group_id,smg_name,smg_description,smg_required) VALUES (10002,'Bills','',0)
GO


/************************  sas_meters_catalog_per_group ****************/
INSERT INTO sas_meters_catalog_per_group (smcg_group_id,smcg_meter_code) VALUES (1,0x40)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id,smcg_meter_code) VALUES (1,0x41)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id,smcg_meter_code) VALUES (1,0x42)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id,smcg_meter_code) VALUES (1,0x43)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id,smcg_meter_code) VALUES (1,0x44)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id,smcg_meter_code) VALUES (1,0x45)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id,smcg_meter_code) VALUES (1,0x46)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id,smcg_meter_code) VALUES (1,0x47)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id,smcg_meter_code) VALUES (1,0x48)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id,smcg_meter_code) VALUES (1,0x49)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id,smcg_meter_code) VALUES (1,0x4A)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id,smcg_meter_code) VALUES (1,0x4B)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id,smcg_meter_code) VALUES (1,0x4C)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id,smcg_meter_code) VALUES (1,0x4D)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id,smcg_meter_code) VALUES (1,0x4E)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id,smcg_meter_code) VALUES (1,0x4F)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id,smcg_meter_code) VALUES (1,0x50)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id,smcg_meter_code) VALUES (1,0x51)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id,smcg_meter_code) VALUES (1,0x52)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id,smcg_meter_code) VALUES (1,0x53)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id,smcg_meter_code) VALUES (1,0x54)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id,smcg_meter_code) VALUES (1,0x55)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id,smcg_meter_code) VALUES (1,0x56)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id,smcg_meter_code) VALUES (1,0x57)
GO

INSERT INTO sas_meters_catalog_per_group (smcg_group_id,smcg_meter_code) VALUES (2,0x0B)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id,smcg_meter_code) VALUES (2,0x40)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id,smcg_meter_code) VALUES (2,0x41)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id,smcg_meter_code) VALUES (2,0x42)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id,smcg_meter_code) VALUES (2,0x43)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id,smcg_meter_code) VALUES (2,0x44)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id,smcg_meter_code) VALUES (2,0x45)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id,smcg_meter_code) VALUES (2,0x46)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id,smcg_meter_code) VALUES (2,0x47)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id,smcg_meter_code) VALUES (2,0x48)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id,smcg_meter_code) VALUES (2,0x49)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id,smcg_meter_code) VALUES (2,0x4A)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id,smcg_meter_code) VALUES (2,0x4B)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id,smcg_meter_code) VALUES (2,0x4C)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id,smcg_meter_code) VALUES (2,0x4D)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id,smcg_meter_code) VALUES (2,0x4E)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id,smcg_meter_code) VALUES (2,0x4F)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id,smcg_meter_code) VALUES (2,0x50)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id,smcg_meter_code) VALUES (2,0x51)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id,smcg_meter_code) VALUES (2,0x52)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id,smcg_meter_code) VALUES (2,0x53)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id,smcg_meter_code) VALUES (2,0x54)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id,smcg_meter_code) VALUES (2,0x55)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id,smcg_meter_code) VALUES (2,0x56)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id,smcg_meter_code) VALUES (2,0x57)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id,smcg_meter_code) VALUES (2,0x80)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id,smcg_meter_code) VALUES (2,0x81)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id,smcg_meter_code) VALUES (2,0x82)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id,smcg_meter_code) VALUES (2,0x83)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id,smcg_meter_code) VALUES (2,0x84)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id,smcg_meter_code) VALUES (2,0x85)
GO

INSERT INTO sas_meters_catalog_per_group (smcg_group_id,smcg_meter_code) VALUES (10001,0x80)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id,smcg_meter_code) VALUES (10001,0x81)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id,smcg_meter_code) VALUES (10001,0x82)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id,smcg_meter_code) VALUES (10001,0x83)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id,smcg_meter_code) VALUES (10001,0x84)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id,smcg_meter_code) VALUES (10001,0x85)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id,smcg_meter_code) VALUES (10001,0x86)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id,smcg_meter_code) VALUES (10001,0x87)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id,smcg_meter_code) VALUES (10001,0x88)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id,smcg_meter_code) VALUES (10001,0x89)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id,smcg_meter_code) VALUES (10001,0x8A)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id,smcg_meter_code) VALUES (10001,0x8B)
GO	
	
INSERT INTO sas_meters_catalog_per_group (smcg_group_id,smcg_meter_code) VALUES (10002,0x40)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id,smcg_meter_code) VALUES (10002,0x42)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id,smcg_meter_code) VALUES (10002,0x43)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id,smcg_meter_code) VALUES (10002,0x44)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id,smcg_meter_code) VALUES (10002,0x46)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id,smcg_meter_code) VALUES (10002,0x47)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id,smcg_meter_code) VALUES (10002,0x48)
INSERT INTO sas_meters_catalog_per_group (smcg_group_id,smcg_meter_code) VALUES (10002,0x4A)
GO	

