/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 56;

SET @New_ReleaseId = 57;
SET @New_ScriptName = N'UpdateTo_18.057.011.sql';
SET @New_Description = N'Balance Mismatch for SAS-HOST and adapted for 3GS. New tables, indexes and parameters for NoteAcceptor.';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/

/* play_sessions.ps_reported_balance_mismatch */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[play_sessions]') and name = 'ps_reported_balance_mismatch')
  ALTER TABLE [dbo].[play_sessions]
          ADD [ps_reported_balance_mismatch] [money] NULL
ELSE
  SELECT '***** Field play_sessions.ps_reported_balance_mismatch already exists *****';
GO

/* NoteAcceptor */
CREATE TABLE [dbo].[terminal_money](
	[tm_terminal_id] [int] NOT NULL,
	[tm_transaction_id] [bigint] NOT NULL,
	[tm_stacker_id] [bigint] NULL,
	[tm_accepted] [datetime] NOT NULL,
	[tm_amount] [money] NOT NULL,
	[tm_trackdata] [nvarchar](20) NOT NULL,
	[tm_play_session_id] [bigint] NULL,
	[tm_account_id] [bigint] NULL,
	[tm_transferred_to_account_id] [bigint] NULL,
	[tm_reported] [datetime] NOT NULL CONSTRAINT [DF_terminal_money_tm_reported]  DEFAULT (getdate()),
	[tm_transferred] [datetime] NULL,
	[tm_collection_id] [bigint] NULL,
	[tm_into_acceptor] [bit] NOT NULL CONSTRAINT [DF_terminal_money_tm_into_acceptor]  DEFAULT ((1)),
 CONSTRAINT [PK_terminal_money] PRIMARY KEY CLUSTERED 
(
	[tm_terminal_id] ASC,
	[tm_transaction_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
CREATE TABLE [dbo].[money_collections](
	[mc_collection_id] [bigint] IDENTITY(1,1) NOT NULL,
	[mc_datetime] [datetime] NOT NULL CONSTRAINT [DF_money_collections_tmc_datetime]  DEFAULT (getdate()),
	[mc_terminal_id] [int] NULL,
	[mc_user_id] [int] NOT NULL,
	[mc_undo_collect] [bit] NOT NULL CONSTRAINT [DF_money_collections_mc_undo_collect]  DEFAULT ((0)),
	[mc_money_lost] [bit] NOT NULL CONSTRAINT [DF_money_collections_mc_collection_type]  DEFAULT ((0)),
 CONSTRAINT [PK_money_collections] PRIMARY KEY CLUSTERED 
(
	[mc_collection_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
CREATE TABLE [dbo].[money_collection_details](
	[mcd_collection_id] [bigint] NOT NULL,
	[mcd_face_value] [money] NOT NULL,
	[mcd_num_expected] [int] NOT NULL,
	[mcd_num_collected] [int] NOT NULL,
 CONSTRAINT [PK_money_collection_details] PRIMARY KEY CLUSTERED 
(
	[mcd_collection_id] ASC,
	[mcd_face_value] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

/****** INDEXES ******/

IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[terminal_money]') AND name = N'IX_tm_terminal_amount_collection')
  CREATE NONCLUSTERED INDEX [IX_tm_terminal_amount_collection] ON [dbo].[terminal_money] 
  (
	  [tm_terminal_id] ASC,
	  [tm_amount] ASC,
	  [tm_collection_id] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
ELSE
  SELECT '***** Index terminal_money.IX_tm_terminal_amount_collection already exists *****';

GO

IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[terminal_money]') AND name = N'IX_tm_reported')
  CREATE NONCLUSTERED INDEX [IX_tm_reported] ON [dbo].[terminal_money] 
  (
	  [tm_reported] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
ELSE
  SELECT '***** Index terminal_money.IX_tm_reported already exists *****';

GO

IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[terminal_money]') AND name = N'IX_tm_trackdata')
  CREATE NONCLUSTERED INDEX [IX_tm_trackdata] ON [dbo].[terminal_money] 
  (
	  [tm_trackdata] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
ELSE
  SELECT '***** Index terminal_money.IX_tm_trackdata already exists *****';

GO

IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[money_collections]') AND name = N'IX_mc_datetime')
  CREATE NONCLUSTERED INDEX [IX_mc_datetime] ON [dbo].[money_collections] 
  (
	  [mc_datetime] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
ELSE
  SELECT '***** Index money_collections.IX_mc_datetime already exists *****';

GO

/****** RECORDS ******/

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'NoteAcceptor' AND GP_SUBJECT_KEY ='Enabled')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key]
             ,[gp_subject_key]
             ,[gp_key_value])
       VALUES
             ('NoteAcceptor'
             ,'Enabled'
             ,'0');

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'NoteAcceptor' AND GP_SUBJECT_KEY ='BankNotes')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key]
             ,[gp_subject_key]
             ,[gp_key_value])
       VALUES
             ('NoteAcceptor'
             ,'BankNotes'
             ,'20;50;100;200;500;1000');

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'NoteAcceptor' AND GP_SUBJECT_KEY ='CollectionByTerminal')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key]
             ,[gp_subject_key]
             ,[gp_key_value])
       VALUES
             ('NoteAcceptor'
             ,'CollectionByTerminal'
             ,'0');

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'SasHost' AND GP_SUBJECT_KEY ='MaximumBalanceErrorCents')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key]
             ,[gp_subject_key]
             ,[gp_key_value])
       SELECT 'SasHost'
             ,'MaximumBalanceErrorCents'
             ,[gp_key_value]
         FROM [dbo].[general_params]
        WHERE [gp_group_key]   = 'Interface3GS'
          AND [gp_subject_key] = 'MaximumBalanceErrorCents';

GO

/****** CREATE USER 'ACEPT. BILLETES' AND SYSTEM MOBILE BANK. ******/

DECLARE @user_id AS INT
DECLARE @pin AS INT
DECLARE @mb_id AS BIGINT
DECLARE @track_data AS NVARCHAR(20)

IF NOT EXISTS (
               SELECT   1
                 FROM   GUI_USERS
                WHERE   GU_USER_TYPE = 1
              )
BEGIN
  SELECT   @user_id = MAX(ISNULL(GU_USER_ID, 0)) + 1 
    FROM   GUI_USERS                      

  INSERT INTO GUI_USERS ( GU_USER_ID
                        , GU_PROFILE_ID
                        , GU_USERNAME
                        , GU_ENABLED
                        , GU_PASSWORD
                        , GU_NOT_VALID_BEFORE
                        , GU_NOT_VALID_AFTER
                        , GU_LAST_CHANGED
                        , GU_PASSWORD_EXP
                        , GU_PWD_CHG_REQ
                        , GU_FULL_NAME
                        , GU_LOGIN_FAILURES
                        , GU_USER_TYPE
                        )
                 VALUES ( @user_id
                        , 0
                        , 'ACEPT. BILLETES'
                        , 1
                        , CAST('0000' AS BINARY(40))
                        , GETDATE() - 3
                        , NULL
                        , GETDATE()
                        , NULL
                        , 0
                        , 'ACEPT. BILLETES'
                        , 0
                        , 1
                        )
END

IF NOT EXISTS
            (
              SELECT   1
                FROM   MOBILE_BANKS
               WHERE   MB_ACCOUNT_TYPE = 2
            )
BEGIN
  SET @pin = ROUND((9999 - 1000) * RAND() + 1000, 0)

  INSERT INTO   MOBILE_BANKS
              ( MB_BLOCKED
              , MB_BALANCE
              , MB_ACCOUNT_TYPE
              , MB_PIN
              , MB_HOLDER_NAME
              )
       VALUES ( 0
              , 0
              , 2
              , CAST(@pin AS NVARCHAR)
              , 'SISTEMA'
              )
  SET @mb_id = SCOPE_IDENTITY()

  SET @track_data = RIGHT('0000000000000' + CAST(@mb_id AS NVARCHAR), 13)

  UPDATE   MOBILE_BANKS
     SET   MB_TRACK_DATA = @track_data
   WHERE   MB_ACCOUNT_ID = @mb_id

END


/**************************************** 3GS Section ****************************************/

/**** CHECK DATABASE EXISTENCE SECTION *****/
GO
IF (NOT EXISTS (SELECT * FROM sys.databases WHERE name = 'sPOS'))
BEGIN
  SELECT 'No 3GS interface to update.';

  raiserror('No 3GS interface to update', 20, -1) with log;
END
ELSE 
  SELECT 'Updating 3GS interface...';


/**** 3GS DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);
    
/**** 3GS INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 10;

SET @New_ReleaseId = 11;
SET @New_ScriptName = N'UpdateTo_18.057.011.sql';
SET @New_Description = N'Balance Mismatch for SAS-HOST and adapted for 3GS.';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version_interface_3gs WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
  /**** VERSION FAILURE SECTION *****/
  SELECT 'Wrong 3GS DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
  FROM DB_VERSION_INTERFACE_3GS

  raiserror('Not updated', 20, -1) with log
END

/**** UPDATE VERSION SECTION *****/
UPDATE db_version_interface_3gs
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;


/**** UPDATE BODY SECTION *****/
GO


ALTER PROCEDURE dbo.SetPlaySessionBalanceMismatch
 (@SessionId                bigint,
  @BalanceMismatch          bit,
  @CreditBalance            Money)
AS
BEGIN

   --DDM 09-MAY-2012: Modified, for save the PS_BALANCE_REPORTED_MISMATCH     
  UPDATE   PLAY_SESSIONS 
     SET   PS_BALANCE_MISMATCH          = @BalanceMismatch 
         , PS_REPORTED_BALANCE_MISMATCH = CASE WHEN (@BalanceMismatch = 1) THEN @CreditBalance ELSE NULL END
   WHERE   PS_PLAY_SESSION_ID           = @SessionId

END -- SetPlaySessionBalanceMismatch
GO


ALTER PROCEDURE [dbo].[zsp_SessionUpdate]
  @AccountId      varchar(24),
  @VendorId       varchar(16),
  @SerialNumber   varchar(30),
  @MachineNumber  int,
  @SessionId      bigint,
  @AmountPlayed   money,
  @AmountWon      money,
  @GamesPlayed    int,
  @GamesWon       int,
  @CreditBalance  money,
  @CurrentJackpot money = 0
AS
BEGIN

  BEGIN TRAN

  -- SET NOCOUNT ON added to prevent extra result sets from
  -- interfering with SELECT statements.
  SET NOCOUNT ON;
	
  DECLARE @status_code         int
  DECLARE @status_text         varchar (254)
  DECLARE @terminal_id         int
  DECLARE @account_id          bigint
  DECLARE @previous_balance    money
  DECLARE @delta_amount_played money
  DECLARE @delta_amount_won    money
  DECLARE @ignore_session_id   int
  DECLARE @dummy               int
  DECLARE @ps_initial_balance        money
  DECLARE @calculated_final_balance  money
  DECLARE @balance_mismatch          bit
  DECLARE @big_mismatch              bit
  DECLARE @balance_mismatch_text     varchar (254)
  DECLARE @final_balance_to_save     money
  DECLARE @force_audit               bit

  SET @status_code         = 0
  SET @status_text         = 'Successful Session Update'
  SET @delta_amount_played = 0
  SET @delta_amount_won    = 0
  SET @dummy               = 0
  SET @balance_mismatch    = 0
  SET @big_mismatch        = 0  
  SET @balance_mismatch_text = ' '

  SELECT @terminal_id = dbo.GetTerminalID(@VendorId, @SerialNumber, @MachineNumber) 
		
  IF (@terminal_id = 0)
  BEGIN
    SET @status_code = 2
    SET @status_text = 'Invalid Machine Information'
    GOTO ERROR_PROCEDURE
  END

  SELECT @account_id = dbo.GetAccountID(@AccountId)

  IF (@account_id = 0)
  BEGIN
    SET @status_code = 1
    SET @status_text = 'Invalid Account Number'
    GOTO ERROR_PROCEDURE
  END
  
  SELECT @ignore_session_id = CAST(GP_KEY_VALUE AS int)
    FROM GENERAL_PARAMS 
   WHERE GP_GROUP_KEY = 'Interface3GS' AND GP_SUBJECT_KEY ='IgnoreSessionID'
  
  IF ( @ignore_session_id = 1 )
  BEGIN
    SELECT @SessionId = ISNULL(AC_CURRENT_PLAY_SESSION_ID , AC_LAST_PLAY_SESSION_ID ) 
      FROM   ACCOUNTS                   
     WHERE ( AC_ACCOUNT_ID = @account_id )
  END

  --
  -- ACC 13-OCT-2011: Check reported CreditBalance.
  --
  
  -- Set FinalBalanceToSave to the Reported Balance
  SET @final_balance_to_save = @CreditBalance
  
  EXECUTE dbo.CheckFinalBalanceMismatch @SessionId, @AmountPlayed, @AmountWon, @CreditBalance, 
                                        @calculated_final_balance OUTPUT, @big_mismatch OUTPUT, @balance_mismatch OUTPUT
  IF (@big_mismatch = 1)
  BEGIN                                
    -- Set FinalBalanceToSave to the Calculated Balance
    if (@calculated_final_balance < @final_balance_to_save)
    BEGIN
      SET @final_balance_to_save = @calculated_final_balance
    END
  END

  EXECUTE dbo.SessionUpdate_Internal @account_id, @terminal_id, @SerialNumber, @SessionId,
                                     @AmountPlayed, @AmountWon, @GamesPlayed, @GamesWon,
                                     @final_balance_to_save, @CurrentJackpot, @dummy, 
                                     @delta_amount_played OUTPUT, @delta_amount_won OUTPUT,
                                     @status_code OUTPUT, @status_text OUTPUT
  IF ( @status_code <> 0 )
    GOTO ERROR_PROCEDURE

  IF (@big_mismatch = 1)
  BEGIN
    SET @status_code = 5
    SET @status_text = 'Final balance does not match validation. '
    SET @balance_mismatch_text = 'Reported='+CAST(@CreditBalance AS nvarchar)
                                +'; Calculated='+CAST(@calculated_final_balance AS nvarchar)    
  
    COMMIT TRAN
	  GOTO OK    
  END
  
  -- Insert Movement
  
  -- Use delta values to calculate initial balance
  --SET @previous_balance = @CreditBalance + @AmountPlayed - @AmountWon
  SET @previous_balance = @CreditBalance + @delta_amount_played - @delta_amount_won
  
  EXECUTE dbo.InsertMovement @SessionId, @account_id, @terminal_id, 0, @previous_balance, @delta_amount_played, @delta_amount_won, @CreditBalance

  SET @status_text = 'Successful Session Update'
	
  COMMIT TRAN
	GOTO OK  
	  
ERROR_PROCEDURE:
  ROLLBACK TRAN
	  
OK: 

	-- Audit MBF 17-JUN-2010
	BEGIN TRAN
	
    DECLARE @input AS nvarchar(MAX)
    DECLARE @output AS nvarchar(MAX)	
  		  	
	  SET @input = '@AccountID='+@AccountID
	             +';@VendorId='+@VendorId
	             +';@SerialNumber='+CAST (@SerialNumber AS nvarchar)
	             +';@MachineNumber='+CAST (@MachineNumber AS nvarchar)
	             +';@SessionId='+CAST (@SessionId AS nvarchar)
	             +';@AmountPlayed='+CAST (@AmountPlayed AS nvarchar)
	             +';@AmountWon='+CAST (@AmountWon AS nvarchar)
	             +';@GamesPlayed='+CAST (@GamesPlayed AS nvarchar)
	             +';@GamesWon='+CAST (@GamesWon AS nvarchar)
	             +';@CreditBalance='+CAST (@CreditBalance AS nvarchar)
	             +';@CurrentJackpot='+CAST (@CurrentJackpot AS nvarchar)
  	    
    SET @output = 'StatusCode='+CAST (@status_code AS nvarchar)
                 +';StatusText='+@status_text+@balance_mismatch_text
    
    SET @force_audit = 0    
    IF @balance_mismatch = 1
    BEGIN
      SET @force_audit = 1
    END
     	
	  EXECUTE dbo.zsp_Audit 'zsp_SessionUpdate'
                        , @AccountID
                        , @VendorId 
                        , @SerialNumber
                        , @MachineNumber
                        , @SessionId
                        , @status_code
                        , @CreditBalance
                        , @force_audit
                        , @input
                        , @output
                      
  -- ACC 13-OCT-2011: Set balance mismatch flag if status is OK or ERROR_BALANCE_MISMATCH
  IF @status_code = 0 OR @status_code = 5
  BEGIN
    EXECUTE dbo.SetPlaySessionBalanceMismatch @SessionId, @balance_mismatch, @CreditBalance 
  END

  COMMIT TRAN
	-- End Audit

  SELECT @status_code AS StatusCode, @status_text AS StatusText

END -- zsp_SessionUpdate
GO


ALTER PROCEDURE [dbo].[zsp_SessionEnd]
  @AccountId      varchar(24),
  @VendorId       varchar(16),
  @SerialNumber   varchar(30),
  @MachineNumber  int,
  @SessionId 	    bigint,
  @AmountPlayed   money,
  @AmountWon      money,
  @GamesPlayed    int,
  @GamesWon       int,
  @CreditBalance  money,
  @CurrentJackpot money = 0
AS
BEGIN

  BEGIN TRAN

  -- SET NOCOUNT ON added to prevent extra result sets from
  -- interfering with SELECT statements.
  SET NOCOUNT ON;

  DECLARE @status_code               int
  DECLARE @status_text               varchar (254)
  DECLARE @terminal_id               int
  DECLARE @account_id                bigint
  DECLARE @previous_balance          money
  DECLARE @delta_amount_played       money
  DECLARE @delta_amount_won          money
  DECLARE @ignore_session_id         int
  DECLARE @is_end_session            int
  DECLARE @ps_initial_balance        money
  DECLARE @calculated_final_balance  money
  DECLARE @balance_mismatch          bit
  DECLARE @big_mismatch              bit
  DECLARE @balance_mismatch_text     varchar (254)
  DECLARE @final_balance_to_save     money
  DECLARE @force_audit               bit

  SET @status_code         = 0
  SET @status_text         = 'Successful End Session'
  SET @delta_amount_played = 0
  SET @delta_amount_won    = 0
  SET @is_end_session      = 1
  SET @balance_mismatch    = 0
  SET @big_mismatch        = 0
  SET @balance_mismatch_text = ' '

  SELECT @terminal_id = dbo.GetTerminalID(@VendorId, @SerialNumber, @MachineNumber) 

  IF (@terminal_id = 0)
  BEGIN
    SET @status_code = 2
    SET @status_text = 'Invalid Machine Information'
    GOTO ERROR_PROCEDURE
  END

  SELECT @account_id = dbo.GetAccountID(@AccountId)

  IF (@account_id = 0)
  BEGIN
    SET @status_code = 1
    SET @status_text = 'Invalid Account Number'
    GOTO ERROR_PROCEDURE
  END
  
  SELECT @ignore_session_id = CAST(GP_KEY_VALUE AS int)
    FROM GENERAL_PARAMS 
   WHERE GP_GROUP_KEY = 'Interface3GS' AND GP_SUBJECT_KEY ='IgnoreSessionID'
  
  IF ( @ignore_session_id = 1 )
  BEGIN
    SELECT @SessionId = ISNULL(AC_CURRENT_PLAY_SESSION_ID , 0 ) 
      FROM   ACCOUNTS                   
     WHERE ( AC_ACCOUNT_ID = @account_id )
  END

  --
  -- ACC 13-OCT-2011: Check reported CreditBalance.
  --
  
  -- Set FinalBalanceToSave to the Reported Balance
  SET @final_balance_to_save = @CreditBalance

  EXECUTE dbo.CheckFinalBalanceMismatch @SessionId, @AmountPlayed, @AmountWon, @CreditBalance, 
                                        @calculated_final_balance OUTPUT, @big_mismatch OUTPUT, @balance_mismatch OUTPUT
  IF (@big_mismatch = 1)
  BEGIN                                
    -- Set FinalBalanceToSave to the Calculated Balance
    if (@calculated_final_balance < @final_balance_to_save)
    BEGIN
      SET @final_balance_to_save = @calculated_final_balance
    END
  END

  EXECUTE dbo.SessionUpdate_Internal @account_id, @terminal_id, @SerialNumber, @SessionId,
                                     @AmountPlayed, @AmountWon, @GamesPlayed, @GamesWon,
                                     @final_balance_to_save, @CurrentJackpot, @is_end_session, 
                                     @delta_amount_played OUTPUT, @delta_amount_won OUTPUT,
                                     @status_code OUTPUT, @status_text OUTPUT
  IF ( @status_code <> 0 )
    GOTO ERROR_PROCEDURE

  IF (@big_mismatch = 1)
  BEGIN
    SET @status_code = 5
    SET @status_text = 'Final balance does not match validation. '
    SET @balance_mismatch_text = 'Reported='+CAST(@CreditBalance AS nvarchar)
                                +'; Calculated='+CAST(@calculated_final_balance AS nvarchar)    
  
    COMMIT TRAN
	  GOTO OK    
  END
  
  -- close play session
  EXECUTE dbo.CloseOpenedPlaySession @SessionId, @status_code OUTPUT, @status_text OUTPUT

  IF ( @status_code <> 0 )
    GOTO ERROR_PROCEDURE
    
  -- Close account session
  EXECUTE dbo.CloseAccountSession @account_id

  -- Insert Movement
  
  -- Use delta values to calculate initial balance
  --SET @previous_balance = @CreditBalance + @AmountPlayed - @AmountWon
  SET @previous_balance = @CreditBalance + @delta_amount_played - @delta_amount_won

  IF (1 = (SELECT   CASE WHEN ( (AC_PROMO_CREATION <= GETDATE()) AND (GETDATE() < AC_PROMO_EXPIRATION) AND (AC_PROMO_BALANCE >0) ) THEN 1 ELSE 0 END
            FROM   ACCOUNTS                   
           WHERE ( AC_ACCOUNT_ID = @account_id )))
  BEGIN
    -- PromoEndSession   = 25,
    EXECUTE dbo.InsertMovement @SessionId, @account_id, @terminal_id, 25, @previous_balance, @delta_amount_played, @delta_amount_won, @CreditBalance

  END
  ELSE
  BEGIN
    -- EndCardSession = 6,
    EXECUTE dbo.InsertMovement @SessionId, @account_id, @terminal_id,  6, @previous_balance, @delta_amount_played, @delta_amount_won, @CreditBalance
  END
	  
  SET @status_text = 'Successful End Session'

  COMMIT TRAN
	GOTO OK  
	  
	ERROR_PROCEDURE:
	  ROLLBACK TRAN
	  
	OK:  
	
	-- Audit MBF 17-JUN-2010
	BEGIN TRAN
	
    DECLARE @input AS nvarchar(MAX)
    DECLARE @output AS nvarchar(MAX)	
  		  	
	  SET @input = '@AccountID='+@AccountID
	             +';@VendorId='+@VendorId
	             +';@SerialNumber='+CAST (@SerialNumber AS nvarchar)
	             +';@MachineNumber='+CAST (@MachineNumber AS nvarchar)
	             +';@SessionId='+CAST (@SessionId AS nvarchar)
	             +';@AmountPlayed='+CAST (@AmountPlayed AS nvarchar)
	             +';@AmountWon='+CAST (@AmountWon AS nvarchar)
	             +';@GamesPlayed='+CAST (@GamesPlayed AS nvarchar)
	             +';@GamesWon='+CAST (@GamesWon AS nvarchar)
	             +';@CreditBalance='+CAST (@CreditBalance AS nvarchar)
	             +';@CurrentJackpot='+CAST (@CurrentJackpot AS nvarchar)
  	           
    SET @output = 'StatusCode='+CAST (@status_code AS nvarchar)
                 +';StatusText='+@status_text+@balance_mismatch_text
     	
    SET @force_audit = 0
    IF @balance_mismatch = 1
    BEGIN
      SET @force_audit = 1
    END
     	
	  EXECUTE dbo.zsp_Audit 'zsp_SessionEnd'
                        , @AccountID
                        , @VendorId 
                        , @SerialNumber
                        , @MachineNumber
                        , @SessionId
                        , @status_code
                        , @CreditBalance
                        , @force_audit
                        , @input
                        , @output

  -- ACC 13-OCT-2011: Set balance mismatch flag if status is OK or ERROR_BALANCE_MISMATCH
  IF @status_code = 0 OR @status_code = 5
  BEGIN
    EXECUTE dbo.SetPlaySessionBalanceMismatch @SessionId, @balance_mismatch, @CreditBalance 
  END

  COMMIT TRAN
	-- End Audit
	
  SELECT @status_code AS StatusCode, @status_text AS StatusText

END -- zsp_SessionEnd
GO


/**** FINAL SECTION *****/
SELECT '3GS Update OK.';
