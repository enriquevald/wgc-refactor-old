/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 442;

SET @New_ReleaseId = 443;

SET @New_ScriptName = N'2018-06-06 - UpdateTo_18.443.041.sql';
SET @New_Description = N'New release v03.008.0001.05'; 

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO


/*********************************************************************************************************/

/**** GENERAL PARAM *****/

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[mobibank_request_status]') AND type in (N'U'))
BEGIN

	CREATE TABLE [dbo].[mobibank_request_status](
		[mbrs_id] [int] IDENTITY(1,1) NOT NULL,
		[mbrs_name] [varchar](1000) NULL,
	 CONSTRAINT [PK_mobibank_request_status] PRIMARY KEY CLUSTERED 
	(
		[mbrs_id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]

END 

GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[mobibank_request_status_expired_reason]') AND type in (N'U'))
BEGIN

	CREATE TABLE [dbo].[mobibank_request_status_expired_reason](
		[mbrser_id] [int] IDENTITY(1,1) NOT NULL,
		[mbrser_name] [varchar](1000) NULL,
	 CONSTRAINT [PK_mobibank_request_status_expired_reason] PRIMARY KEY CLUSTERED 
	(
		[mbrser_id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]

END

GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[mobibank_request]') AND type in (N'U'))
BEGIN

	CREATE TABLE [dbo].[mobibank_request](
		[mbr_id] [bigint] IDENTITY(1,1) NOT NULL,
		[mbr_terminal_id] [int] NOT NULL,
		[mbr_account_id] [bigint] NOT NULL,
		[mbr_time_request] [datetime] NULL,
		[mbr_elapsed_time_request] [datetime] NULL,
		[mbr_area_id] [int] NULL,
		[mbr_bank_id] [int] NULL,
		[mbr_status_id] [int] NULL,
		[mbr_amount] [money] NULL,
		[mbr_user_id_in_progress] [int] NULL,
		[mbr_reason] [int] NULL,
		[mbr_finished_time_request] [datetime] NULL,
	 CONSTRAINT [PK_mobibank] PRIMARY KEY CLUSTERED 
	(
		[mbr_id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]

END
GO
/**** VIEW *****/

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[mobibank_request_view]'))
DROP VIEW [dbo].[mobibank_request_view]
GO

CREATE VIEW [dbo].[mobibank_request_view]
AS
SELECT        MR.mbr_id AS Id, MR.mbr_terminal_id AS TerminalId, T.te_base_name AS TerminalName, MR.mbr_account_id AS AccountId, A.ac_holder_name AS AccountName, A.ac_holder_level AS AccountLevel, 
                         A.ac_holder_is_vip AS AccountIsVip, MR.mbr_time_request AS TimeRequest, MR.mbr_finished_time_request as TransactionTimeRequest, MR.mbr_status_id AS StatusId, MR.mbr_bank_id AS BankId, B.bk_name AS BankName, AR.ar_area_id AS AreaId, AR.ar_name AS AreaName, MR.mbr_user_id_in_progress as UserId, MR.mbr_amount as TransactionAmount
FROM            dbo.mobibank_request AS MR LEFT OUTER JOIN
                         dbo.terminals AS T ON MR.mbr_terminal_id = T.te_terminal_id LEFT OUTER JOIN
                         dbo.accounts AS A ON MR.mbr_account_id = A.ac_account_id LEFT OUTER JOIN
                         dbo.banks AS B ON MR.mbr_bank_id = B.bk_bank_id LEFT OUTER JOIN
                         dbo.areas AS AR ON B.bk_area_id = AR.ar_area_id

GO
						 
/**** GENERAL PARAMS *****/


BEGIN
if not exists (select * from general_params where gp_group_key = 'MobileBank' and gp_subject_key = 'ExpirationRequestTimeDefault')
	INSERT INTO [dbo].[general_params]
			   ([gp_group_key]
			   ,[gp_subject_key]
			   ,[gp_key_value])
		 VALUES
			   ('MobileBank'
			   ,'ExpirationRequestTimeDefault'
			   ,0)

	END
GO

BEGIN
if not exists (select * from general_params where gp_group_key = 'MobileBank' and gp_subject_key = 'Cashier.AddAmount.CustomButton1')
	INSERT INTO [dbo].[general_params]
			   ([gp_group_key]
			   ,[gp_subject_key]
			   ,[gp_key_value])
		 VALUES
			   ('MobileBank'
			   ,'Cashier.AddAmount.CustomButton1'
			   ,50)

	END
GO

BEGIN
if not exists (select * from general_params where gp_group_key = 'MobileBank' and gp_subject_key = 'Cashier.AddAmount.CustomButton2')
	INSERT INTO [dbo].[general_params]
			   ([gp_group_key]
			   ,[gp_subject_key]
			   ,[gp_key_value])
		 VALUES
			   ('MobileBank'
			   ,'Cashier.AddAmount.CustomButton2'
			   ,75)

	END
GO

BEGIN
if not exists (select * from general_params where gp_group_key = 'MobileBank' and gp_subject_key = 'Cashier.AddAmount.CustomButton3')
	INSERT INTO [dbo].[general_params]
			   ([gp_group_key]
			   ,[gp_subject_key]
			   ,[gp_key_value])
		 VALUES
			   ('MobileBank'
			   ,'Cashier.AddAmount.CustomButton3'
			   ,100)

	END
GO

BEGIN
if not exists (select * from general_params where gp_group_key = 'MobileBank' and gp_subject_key = 'Cashier.AddAmount.CustomButton4')
	INSERT INTO [dbo].[general_params]
			   ([gp_group_key]
			   ,[gp_subject_key]
			   ,[gp_key_value])
		 VALUES
			   ('MobileBank'
			   ,'Cashier.AddAmount.CustomButton4'
			   ,500)

	END
GO

IF NOT EXISTS(SELECT 1  FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'MobileBank'
                        AND GP_SUBJECT_KEY = 'NumberOfRetriesConnection'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('MobileBank', 'NumberOfRetriesConnection', '3')
END
GO
 IF NOT EXISTS(SELECT 1  FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'MobileBank'
                        AND GP_SUBJECT_KEY = 'TimeForTestConnection'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('MobileBank', 'TimeForTestConnection', '15000')
END
GO
IF NOT EXISTS(SELECT 1  FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'MobileBank'
                        AND GP_SUBJECT_KEY = 'BlinkingTimeRequest'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('MobileBank', 'BlinkingTimeRequest', '0')
END
GO
IF NOT EXISTS(SELECT 1  FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'Terminal'
                        AND GP_SUBJECT_KEY = 'QR'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('Terminal', 'QR', '{@AssetNumber}')
END
GO

IF NOT EXISTS(SELECT 1  FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'Terminal'
                        AND GP_SUBJECT_KEY = 'QR.Footer'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('Terminal', 'QR.Footer', '{@AssetNumber}')
END

GO
if not exists (select * from general_params where gp_group_key = 'MobileBank' and gp_subject_key = 'RequestRefreshTime')

BEGIN
	INSERT INTO [dbo].[general_params]
			   ([gp_group_key]
			   ,[gp_subject_key]
			   ,[gp_key_value])
		 VALUES
			   ('MobileBank'
			   ,'RequestRefreshTime'
			   ,30)

END
GO

if not exists (select * from general_params where gp_group_key = 'MobileBank' and gp_subject_key = 'DeviceLimit.Visible')
BEGIN
	INSERT INTO [dbo].[general_params]
			   ([gp_group_key]
			   ,[gp_subject_key]
			   ,[gp_key_value])
		 VALUES
			   ('MobileBank'
			   ,'DeviceLimit.Visible'
			   ,1)

	END
GO
GO
IF NOT EXISTS(SELECT 1  FROM GENERAL_PARAMS
                       WHERE GP_GROUP_KEY = 'User'
                       AND GP_SUBJECT_KEY = 'SessionTimeOut.MoviBank'
                 )
BEGIN
     INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
     VALUES ('User', 'SessionTimeOut.MoviBank', '0')
END

GO

/**** INSERTS *****/

TRUNCATE TABLE mobibank_request_status

SET IDENTITY_INSERT [dbo].[mobibank_request_status] ON 
INSERT [dbo].[mobibank_request_status] ([mbrs_id], [mbrs_name]) VALUES (1, N'New')
INSERT [dbo].[mobibank_request_status] ([mbrs_id], [mbrs_name]) VALUES (2, N'In Progress')
INSERT [dbo].[mobibank_request_status] ([mbrs_id], [mbrs_name]) VALUES (3, N'Void')
INSERT [dbo].[mobibank_request_status] ([mbrs_id], [mbrs_name]) VALUES (4, N'Finished')
INSERT [dbo].[mobibank_request_status] ([mbrs_id], [mbrs_name]) VALUES (5, N'FinishedInEgm')
SET IDENTITY_INSERT [dbo].[mobibank_request_status] OFF

if not exists (select * from [mobibank_request_status_expired_reason] where [mbrser_name] = 'Expired')
INSERT INTO [dbo].[mobibank_request_status_expired_reason]
           ([mbrser_name])
     VALUES
           ('Expired')
GO


