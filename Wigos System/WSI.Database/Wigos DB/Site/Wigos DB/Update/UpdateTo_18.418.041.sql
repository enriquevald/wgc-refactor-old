/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 417;

SET @New_ReleaseId = 418;

SET @New_ScriptName = N'UpdateTo_18.418.041.sql';
SET @New_Description = N'New release v03.006.0017'; 

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO



/**** GENERAL PARAM *****/


/******* TABLES  *******/

IF NOT EXISTS (SELECT 1 
                FROM sys.columns 
                WHERE object_id = object_id(N'[dbo].[pinpad_transactions_reconciliation]') 
                  AND name = 'ptc_code'
              )
BEGIN              
  ALTER TABLE [dbo].[pinpad_transactions_reconciliation]
  ADD [ptc_code] [int] NOT NULL DEFAULT 20
END        
ELSE
BEGIN 
  SELECT '***** Field pinpad_transactions_reconciliation.ptc_code already exists *****';
END
  

/******* RECORDS *******/


/******* PROCEDURES *******/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GT_Cashier_Movements_And_Summary_By_Session]') AND type in (N'P', N'PC'))
   DROP PROCEDURE [dbo].[GT_Cashier_Movements_And_Summary_By_Session]
GO


CREATE PROCEDURE [dbo].[GT_Cashier_Movements_And_Summary_By_Session]
(
  @pGtSessionId                  BigInt,

  @pCageChipColor                VARCHAR(3),-- X02
  @pOpeningCash                  INT,  
  @pCageCloseSession             INT,
  @pCageFillerIn                 INT,
  @pCageFillerOut                INT,
  @pChipsSale                    INT,
  @pChipsPurchase                INT,
  @pChipsSaleDevolutionForTito   INT,
  @pChipsSaleWithCashIn          INT,
  @pChipsSaleRegisterTotal       INT,
  @pFillerInClosingStock         INT, 
  @pFillerOutClosingStock        INT,  
  @pReopenCashier                INT,
  
  @pOwnSalesAmount       Money,
  @pExternalSalesAmount  Money,
  @pCollectedAmount      Money,
  @pIsIntegratedCashier  Bit,

  @pDrop                 Money        OUTPUT
)
AS
BEGIN
  DECLARE @Columns           AS VARCHAR(MAX)
  DECLARE @NationalCurrency  AS VARCHAR(5)
  DECLARE @HasInitial        AS INT
           
  SET @HasInitial = 0
  SET @NationalCurrency = (SELECT GP_KEY_VALUE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'RegionalOptions' AND GP_SUBJECT_KEY = 'CurrencyISOCode')
          
      SELECT   CS_SESSION_ID
             , MAX(CM_DATE)                    AS DATE_MOV
             , CM_TYPE                         AS TYPE_MOV
             , ISNULL(CCMR.CGM_MOVEMENT_ID, 0) AS ID_MOV_22
             , REPLICATE('0', 50 - LEN(CAST(ISNULL(CCMR.CGM_MOVEMENT_ID, 0) AS NVARCHAR(50)))) + CAST(ISNULL(CCMR.CGM_MOVEMENT_ID, 0) AS NVARCHAR(50)) 
               + REPLICATE('0', 50 - LEN(CAST(ISNULL(CM.CM_OPERATION_ID, 0) AS NVARCHAR(50)))) + CAST(ISNULL(CM.CM_OPERATION_ID, 0) AS NVARCHAR(50)) AS ID_MOV
             , SUM(CM_SUB_AMOUNT)              AS SUB_AMOUNT
             , SUM(CM_ADD_AMOUNT)              AS ADD_AMOUNT
             , SUM(CM_ADD_AMOUNT) - SUM(CM_SUB_AMOUNT) AS CALCULATED_AMOUNT
             ,  CASE ISNULL(CM_CURRENCY_ISO_CODE,'') WHEN '' THEN @NationalCurrency
                WHEN @pCageChipColor THEN ''
                ELSE CM.CM_CURRENCY_ISO_CODE END AS CURRENCY_ISO_CODE
             ,  CM.CM_CAGE_CURRENCY_TYPE AS CURRENCY_TYPE
             ,  0 OPENER_MOV
             , CM.CM_OPERATION_ID
             , CM_UNDO_STATUS
             , GTS_INITIAL_CHIPS_AMOUNT AS INITIAL_CHIPS_AMOUNT
        INTO   #TABLE_MOVEMENTS
        FROM   GAMING_TABLES_SESSIONS
  INNER JOIN   CASHIER_SESSIONS  ON GTS_CASHIER_SESSION_ID = CS_SESSION_ID
  INNER JOIN   CASHIER_MOVEMENTS AS CM WITH(INDEX(IX_CM_SESSION_ID)) ON CM_SESSION_ID = CS_SESSION_ID
   LEFT JOIN   CAGE_CASHIER_MOVEMENT_RELATION AS CCMR ON CCMR.CM_MOVEMENT_ID = CM.CM_MOVEMENT_ID
  INNER JOIN   GUI_USERS         ON CS_USER_ID    = GU_USER_ID
       WHERE   GTS_GAMING_TABLE_SESSION_ID = @pGtSessionId
         AND   CM_TYPE IN (@pOpeningCash,
                           ---@pFillerInClosingStock,
                           @pCageFillerIn,
                           @pCageFillerOut,
                           @pCageCloseSession,
                           @pFillerOutClosingStock,
                           @pChipsSale,
                           @pChipsPurchase,
                           @pChipsSaleDevolutionForTito,
                           @pChipsSaleWithCashIn,
                           @pChipsSaleRegisterTotal, 
                           @pReopenCashier)
    GROUP BY   CS_SESSION_ID
             , CM_TYPE
             , CCMR.CGM_MOVEMENT_ID
             , REPLICATE('0', 50 - LEN(cast(ISNULL(CCMR.CGM_MOVEMENT_ID, 0) as nvarchar(50)))) + cast(ISNULL(CCMR.CGM_MOVEMENT_ID, 0) as nvarchar(50)) 
               + REPLICATE('0', 50 - LEN(cast(ISNULL(CM.CM_OPERATION_ID, 0) as nvarchar(50)))) + cast(ISNULL(CM.CM_OPERATION_ID, 0) as nvarchar(50))
             , CM.CM_CURRENCY_ISO_CODE
             , CM.CM_CAGE_CURRENCY_TYPE
             , CM.CM_OPERATION_ID
             , CM_UNDO_STATUS
             , GTS_INITIAL_CHIPS_AMOUNT
    ORDER BY   MAX(CM_DATE) DESC
          
  DECLARE @cursor_id_mov AS nvarchar(255)

  DECLARE _cursor CURSOR FOR SELECT MIN(ID_MOV) 
                               FROM #TABLE_MOVEMENTS 
                              WHERE TYPE_MOV IN (@pCageFillerIn , @pFillerInClosingStock) 
                                AND INITIAL_CHIPS_AMOUNT > 0
                              GROUP BY CS_SESSION_ID
  
  OPEN _cursor  
  FETCH NEXT FROM _cursor INTO @cursor_id_mov
  
  WHILE @@FETCH_STATUS = 0  
  BEGIN  
    UPDATE #TABLE_MOVEMENTS
       SET OPENER_MOV = 1
     WHERE ID_MOV = @cursor_id_mov
     
     SET @HasInitial = @HasInitial + 1

    FETCH NEXT FROM _cursor INTO @cursor_id_mov 
  END   
  CLOSE _cursor;  
  DEALLOCATE _cursor;  

  SET @NationalCurrency = (SELECT GP_KEY_VALUE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'RegionalOptions' AND GP_SUBJECT_KEY = 'CurrencyISOCode')

  SELECT @Columns = COALESCE(@Columns + ',', '') +  '[' + CURRENCY_TYPE + ']'                             
    FROM   (SELECT   DISTINCT ISNULL(CE_CURRENCY_ORDER, -1) CE_CURRENCY_ORDER
                   , CASE WHEN CURRENCY_TYPE > 1000 
                          THEN 0
                          ELSE 1 END AS ORDER_2
                   , CASE WHEN CURRENCY_ISO_CODE = '' THEN 'X02' ELSE CURRENCY_ISO_CODE END 
                   + ' ' + CONVERT(NVARCHAR(4), CURRENCY_TYPE) AS CURRENCY_TYPE      
              FROM   #TABLE_MOVEMENTS 
              LEFT   JOIN CURRENCY_EXCHANGE ON CURRENCY_ISO_CODE = CE_CURRENCY_ISO_CODE AND CE_TYPE = 0
             WHERE   CURRENCY_ISO_CODE IS NOT NULL
           ) AS COLS
  ORDER   BY CE_CURRENCY_ORDER ASC
        , ORDER_2 ASC
        , CURRENCY_TYPE ASC

  SET @Columns = ISNULL(@Columns,COALESCE('[' + @NationalCurrency + ']',''))                   

  SELECT * FROM   #TABLE_MOVEMENTS   

  IF (@HasInitial = 0) 
  BEGIN
        INSERT INTO #TABLE_MOVEMENTS
          SELECT   CS_SESSION_ID
                 , cs_opening_date
                 , @pCageFillerIn 
                 , 0
                 , REPLICATE('0', 100) 
                 , 0
                 , 0
                 , 0
                 , @NationalCurrency 
                 , 0
                 , 1 OPENER_MOV
                 , 0
                 , 0
                 , 0
            FROM   GAMING_TABLES_SESSIONS
      INNER JOIN   CASHIER_SESSIONS  ON GTS_CASHIER_SESSION_ID = CS_SESSION_ID
           WHERE   GTS_GAMING_TABLE_SESSION_ID = @pGtSessionId
  END

   EXEC ('-- INITIAL AMOUNT
        SELECT   ''INITIAL_AMOUNT''  AS NAME_MOV
               , *
          FROM ( SELECT   ADD_AMOUNT AS AMOUNT
                        , CASE WHEN CURRENCY_ISO_CODE = '''' THEN ''X02'' ELSE CURRENCY_ISO_CODE END
                          + '' '' + CONVERT(NVARCHAR(4), CURRENCY_TYPE) AS CURRENCY_ISO_CODE
                   FROM   #TABLE_MOVEMENTS
                  WHERE   OPENER_MOV = 1
               ) AS T1
        PIVOT (
                SUM (AMOUNT) FOR CURRENCY_ISO_CODE IN (' + @Columns+ ')
              ) AS PVT
        UNION   ALL
         -- TOTAL FILL IN
        SELECT   ''TOTAL_FILL_IN''  AS NAME_MOV
               , *
          FROM ( SELECT   ADD_AMOUNT AS AMOUNT
                        , CASE WHEN CURRENCY_ISO_CODE = '''' THEN ''X02'' ELSE CURRENCY_ISO_CODE END 
                          + '' '' + CONVERT(NVARCHAR(4), CURRENCY_TYPE) AS CURRENCY_ISO_CODE
                   FROM   #TABLE_MOVEMENTS
                  WHERE   OPENER_MOV <> 1
               ) AS T1
         PIVOT (
                 SUM (AMOUNT) FOR CURRENCY_ISO_CODE IN (' + @Columns+ ')
               ) AS PVT
         UNION   ALL
          -- TOTAL WITHDRAW
        SELECT   ''TOTAL_WITHDRAW'' AS NAME_MOV
               , *
          FROM (  SELECT   SUB_AMOUNT AS AMOUNT
                         , CASE WHEN CURRENCY_ISO_CODE = '''' THEN ''X02'' ELSE CURRENCY_ISO_CODE END
                           + '' '' + CONVERT(NVARCHAR(4), CURRENCY_TYPE) AS CURRENCY_ISO_CODE
                    FROM   #TABLE_MOVEMENTS
               ) AS T1
         PIVOT (
                 SUM (AMOUNT) FOR CURRENCY_ISO_CODE IN (' + @Columns+ ')
               ) AS PVT
   ')
  
   DROP TABLE #TABLE_MOVEMENTS      

  SELECT @pDrop = dbo.GT_Calculate_DROP(@pOwnSalesAmount, @pExternalSalesAmount, @pCollectedAmount, @pIsIntegratedCashier) 
 
END
GO
  
GRANT EXECUTE ON [dbo].[GT_Cashier_Movements_And_Summary_By_Session] TO [WGGUI] WITH GRANT OPTION
GO


IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GT_HourlyDrop]') AND type in (N'P', N'PC'))
   DROP PROCEDURE [dbo].[GT_HourlyDrop]
GO


CREATE PROCEDURE [dbo].[GT_HourlyDrop]
(
    @pGamingTableSessionId BIGINT,
    @pDateFrom                                DATETIME,
    @pDateTo                              DATETIME,
    @pIsoCode                             NVARCHAR(3),
    @pSaleChipsMov                    INT,
    @pValCopyDealerMov              INT,
    @pRedeemableChipsType   INT,
    @pGamingTablesId                    NVARCHAR(MAX)
)
AS
BEGIN

  DECLARE @NationalCurrency NVARCHAR(3)
  DECLARE @_DELIMITER       NVARCHAR(1)
  DECLARE @_GAMING_TABLE    TABLE(SST_ID INT, SST_VALUE VARCHAR(50))
  DECLARE @old_time AS DATETIME

  DECLARE @Date AS DATETIME
  DECLARE @GTSessionId AS BIGINT
  DECLARE @IsoCode AS NVARCHAR(3)
  DECLARE @OpeningDate AS DATETIME
  DECLARE @ClosingDate AS DATETIME



    
  SET @old_time = '2007-01-01T07:00:00'
  SET     @_DELIMITER = ','

  INSERT INTO @_GAMING_TABLE SELECT * FROM dbo.SplitStringIntoTable(@pGamingTablesId, @_DELIMITER, DEFAULT)

  SELECT @NationalCurrency = GP_KEY_VALUE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'RegionalOptions' AND  GP_SUBJECT_KEY = 'CurrencyISOCode'

  DECLARE   @t_Hour_Iso 
    TABLE ( TEMP_SESSION_ID  BIGINT,
            TEMP_DATETIME    DATETIME, 
            TEMP_ISO_CODE    VARCHAR(3));  

  IF @pGamingTableSessionId IS NOT NULL
  BEGIN
        SELECT   @pDateFrom = CONVERT(DATETIME, CONVERT(NVARCHAR(13), CS_OPENING_DATE, 21) + ':00:00', 21)
               , @pDateTo = ISNULL(CS_CLOSING_DATE, GETDATE())
          FROM   GAMING_TABLES_SESSIONS 
    INNER JOIN   CASHIER_SESSIONS ON CS_SESSION_ID = GTS_CASHIER_SESSION_ID
         WHERE   GTS_GAMING_TABLE_SESSION_ID = @pGamingTableSessionId
  END
  ELSE
  BEGIN
    IF @pDateFrom IS NULL
    BEGIN
      SET @pDateFrom = CONVERT(DATETIME, 0)
    END
            
    IF @pDateTo IS NULL
    BEGIN
      SET @pDateTo = GETDATE()
    END
  END


  DECLARE ISO_CODES_CURSOR CURSOR FOR
           SELECT   DISTINCT GTS_GAMING_TABLE_SESSION_ID
                  , ISNULL(GTSC_ISO_CODE, @NationalCurrency)
                  , CS_OPENING_DATE
                  , ISNULL(CS_CLOSING_DATE, GETDATE())
             FROM   GAMING_TABLES_SESSIONS 
       INNER JOIN   CASHIER_SESSIONS ON CS_SESSION_ID = GTS_CASHIER_SESSION_ID
        LEFT JOIN   GAMING_TABLES_SESSIONS_BY_CURRENCY ON GTSC_GAMING_TABLE_SESSION_ID = GTS_GAMING_TABLE_SESSION_ID
            WHERE  ((@pGamingTableSessionId IS NOT NULL AND GTS_GAMING_TABLE_SESSION_ID = @pGamingTableSessionId)
                                                OR  (@pGamingTableSessionId IS NULL AND ISNULL(CS_CLOSING_DATE, CS_OPENING_DATE) < @pDateTo))
              AND  (ISNULL(@pIsoCode,'') = '' OR @pIsoCode = GTSC_ISO_CODE) 
              AND  ISNULL(GTSC_TYPE, @pRedeemableChipsType) = @pRedeemableChipsType
              AND  ( GTS_GAMING_TABLE_ID IN ( SELECT SST_VALUE FROM @_GAMING_TABLE ) OR LEN(@pGamingTablesId) = 0 )
                                               
            UNION
                                   
           SELECT   DISTINCT GTPM_GAMING_TABLE_SESSION_ID
                  , GTPM_ISO_CODE
                  , CS_OPENING_DATE
                  , ISNULL(CS_CLOSING_DATE, GETDATE())
             FROM   GT_PLAYERTRACKING_MOVEMENTS 
       INNER JOIN   GAMING_TABLES_SESSIONS ON GTPM_GAMING_TABLE_SESSION_ID = GTS_GAMING_TABLE_SESSION_ID
       INNER JOIN   CASHIER_SESSIONS ON CS_SESSION_ID = GTS_CASHIER_SESSION_ID
            WHERE   ((@pGamingTableSessionId IS NOT NULL AND GTPM_GAMING_TABLE_SESSION_ID = @pGamingTableSessionId OR @pGamingTableSessionId IS NULL)
                                                OR  (@pGamingTableSessionId IS NULL AND CS_OPENING_DATE >= @pDateFrom AND ISNULL(CS_CLOSING_DATE, CS_OPENING_DATE) < @pDateTo))
              AND   GTPM_TYPE IN (@pSaleChipsMov, @pValCopyDealerMov)
              AND   (ISNULL(@pIsoCode,'') = '' OR @pIsoCode = GTPM_ISO_CODE)
              AND   ( GTS_GAMING_TABLE_ID IN ( SELECT SST_VALUE FROM @_GAMING_TABLE ) OR LEN(ISNULL(@pGamingTablesId, '')) = 0 )


  OPEN ISO_CODES_CURSOR   
    FETCH NEXT FROM ISO_CODES_CURSOR INTO @GTSessionId, @IsoCode, @OpeningDate, @ClosingDate

  WHILE @@FETCH_STATUS = 0   
  BEGIN   
    
    SET @Date = @pDateFrom
    
    IF (@OpeningDate > @pDateFrom)
    BEGIN
               SET @Date = DATEADD(HOUR, DATEDIFF(HOUR, @old_time, @OpeningDate), @old_time) 
    END
    
    WHILE @Date < @ClosingDate AND @Date < @pDateTo
    BEGIN
      IF @Date >= CONVERT(NVARCHAR(13), @OpeningDate, 21) + ':00:00' AND @Date <= @ClosingDate
      BEGIN
        INSERT INTO @t_Hour_Iso
               VALUES(@GTSessionId, CONVERT(NVARCHAR(13), @Date, 21) + ':00:00', @IsoCode)
      END
      SET @Date = DATEADD(HH, 1, @Date)
    END
      
    FETCH NEXT FROM ISO_CODES_CURSOR INTO @GTSessionId, @IsoCode, @OpeningDate, @ClosingDate  
  END  

  CLOSE ISO_CODES_CURSOR   
  DEALLOCATE ISO_CODES_CURSOR
  
     SELECT   TEMP_SESSION_ID
            , TEMP_DATETIME
            , TEMP_ISO_CODE
            , ISNULL(SUM(GTPM_VALUE), 0) AS DROP_AMOUNT
            , CS_NAME
            , GT_NAME
       FROM   @t_Hour_Iso
  LEFT JOIN   GT_PLAYERTRACKING_MOVEMENTS ON TEMP_ISO_CODE = GTPM_ISO_CODE
                                         AND TEMP_DATETIME = CONVERT(NVARCHAR(13), GTPM_DATETIME, 21) + ':00:00'
                                         AND GTPM_GAMING_TABLE_SESSION_ID = TEMP_SESSION_ID
                                         AND GTPM_TYPE IN (@pSaleChipsMov, @pValCopyDealerMov)
INNER JOIN   GAMING_TABLES_SESSIONS      ON TEMP_SESSION_ID = GTS_GAMING_TABLE_SESSION_ID
INNER JOIN   CASHIER_SESSIONS            ON GTS_CASHIER_SESSION_ID = CS_SESSION_ID
INNER JOIN   GAMING_TABLES               ON GTS_GAMING_TABLE_ID = GT_GAMING_TABLE_ID
   GROUP BY   TEMP_SESSION_ID
            , TEMP_DATETIME
            , TEMP_ISO_CODE                            
            , CS_NAME
            , GT_NAME
   ORDER BY   TEMP_SESSION_ID
            , TEMP_DATETIME
            , TEMP_ISO_CODE
               
END -- END PROCEDURE
GO

-- PERMISSIONS
GRANT EXECUTE ON [dbo].[GT_HourlyDrop] TO [wggui] WITH GRANT OPTION
GO
