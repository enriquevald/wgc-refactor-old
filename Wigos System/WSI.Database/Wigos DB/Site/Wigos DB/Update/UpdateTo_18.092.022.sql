/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 91;

SET @New_ReleaseId = 92;
SET @New_ScriptName = N'UpdateTo_18.092.022.sql';
SET @New_Description = N'.';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** INDEXES ******/
BEGIN TRANSACTION
GO


ALTER PROCEDURE [dbo].[WSP_AccountPendingHandpays]
  @AccountId  BigInt
AS
BEGIN
    SET NOCOUNT ON;

  DECLARE @TypeCancelledCredits AS Int
  DECLARE @TypeJackpot AS Int
  DECLARE @TypeSiteJackpot AS Int
  DECLARE @TypeOrphanCredits AS Int
  DECLARE @oldest_handpay  AS Datetime
  DECLARE @candidate_exists as bit

  DECLARE @TimePeriod as int
  DECLARE @oldest_session_start as Datetime
  DECLARE @newest_session_end   as Datetime
  DECLARE @count                as int
  DECLARE @now   as Datetime

  DECLARE @cph_terminal_id as int
  DECLARE @cph_datetime as Datetime
  DECLARE @cph_amount as Money

  --
  -- Handpay types
  --
  SET @TypeCancelledCredits = 0
  SET @TypeJackpot          = 1
  SET @TypeSiteJackpot      = 20
  SET @TypeOrphanCredits    = 2

  --
  -- Oldest Handpay 'Not expired'
  --
  SET @now = GETDATE()

  SELECT   @TimePeriod              = CAST (GP_KEY_VALUE AS INT)
    FROM   GENERAL_PARAMS
   WHERE   GP_GROUP_KEY             = 'Cashier.HandPays'
     AND   GP_SUBJECT_KEY           = 'TimePeriod'
     AND   ISNUMERIC (GP_KEY_VALUE) = 1 --- Ensure is a numeric value

  SET @TimePeriod = ISNULL (@TimePeriod, 60)      -- Default value: 1 hour
  IF ( @TimePeriod <   5 ) SET @TimePeriod =    5  -- Minimum: 5 minutes
  IF ( @TimePeriod > 2500 ) SET @TimePeriod = 2500  -- Desired: (24 + 12) h x 60 min/h = 2160 min, so we use 2500

  SET @oldest_handpay = DATEADD (MINUTE, -@TimePeriod, @now)

  --
  -- Pending Handpays
  --
  SELECT   *
    INTO   #PENDING_HANDPAYS
    FROM   HANDPAYS 
   WHERE   HP_MOVEMENT_ID IS NULL
     AND   HP_DATETIME >= @oldest_handpay
     AND   ( HP_TYPE IN (@TypeCancelledCredits, @TypeJackpot, @TypeOrphanCredits)
        OR ( HP_TYPE = @TypeSiteJackpot AND HP_SITE_JACKPOT_AWARDED_TO_ACCOUNT_ID = @AccountId ) )

  ---
  --- Loop on pending Handpays (excluding 'SiteJackpot')
  ---
  DECLARE   cursor_pending_handpays CURSOR FOR
   SELECT   HP_TERMINAL_ID, HP_DATETIME, HP_AMOUNT
      FROM  #PENDING_HANDPAYS
    WHERE   HP_TYPE <> @TypeSiteJackpot
    
  OPEN cursor_pending_handpays
  FETCH NEXT FROM cursor_pending_handpays INTO @cph_terminal_id, @cph_datetime, @cph_amount

  WHILE @@Fetch_Status = 0
  BEGIN
    
    SET @oldest_session_start = DATEADD (DAY, -1, @cph_datetime)
    SET @newest_session_end   = DATEADD (MINUTE, @TimePeriod, @cph_datetime)

    SELECT   @count = COUNT(*)
      FROM   PLAY_SESSIONS
     WHERE   PS_TERMINAL_ID = @cph_terminal_id
       AND   PS_ACCOUNT_ID  = @AccountId
       AND   PS_STARTED     >= @oldest_session_start
       AND   PS_STARTED     <  @cph_datetime
       AND   PS_STARTED     < PS_FINISHED -- Exclude 'paid handpays' sessions 
       AND   PS_FINISHED    >= @oldest_session_start
       AND   PS_FINISHED    <  @newest_session_end

    IF ( @count = 0 )
    BEGIN
      ---
      --- Not a candidate: delete it!
      ---
      DELETE   #PENDING_HANDPAYS
       WHERE   HP_TERMINAL_ID = @cph_terminal_id
         AND   HP_DATETIME    = @cph_datetime
         AND   HP_AMOUNT      = @cph_amount
    END

    --
    -- Next
    --
    FETCH NEXT FROM cursor_pending_handpays INTO @cph_terminal_id, @cph_datetime, @cph_amount

  END

  CLOSE cursor_pending_handpays
  DEALLOCATE cursor_pending_handpays

  SELECT   HP_TERMINAL_ID 
         , HP_DATETIME 
         , HP_TE_PROVIDER_ID 
         , HP_TE_NAME 
         , HP_TYPE 
         , ' '  HP_DUMMY
         , ISNULL (HP_SITE_JACKPOT_NAME, ' ')  HP_SITE_JACKPOT_NAME
         , HP_AMOUNT 
         , ISNULL (HP_SITE_JACKPOT_AWARDED_ON_TERMINAL_ID, 0)  HP_SITE_JACKPOT_AWARDED_ON_TERMINAL_ID
    FROM   #PENDING_HANDPAYS
   ORDER BY HP_DATETIME DESC

  DROP TABLE #PENDING_HANDPAYS

END -- WSP_AccountPendingHandpays

GO


GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WSP_PlayerInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[WSP_PlayerInfo]

GO

CREATE PROCEDURE [dbo].[WSP_PlayerInfo]
    -- Add the parameters for the stored procedure here
    @TrackData  NVARCHAR(20)

AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;


--
-- 0: Success
-- 1: Account Not Found
-- 2: Account Blocked
-- 3: Error


DECLARE @pStatusCode AS INT
DECLARE @pStatusText AS NVARCHAR(256)

DECLARE @pBlocked   AS BIT
DECLARE @pPlayed    AS MONEY
DECLARE @pWon       AS MONEY
DECLARE @pToday     AS DATETIME
DECLARE @pSiteId    AS INT
DECLARE @pAccountId AS BIGINT

SET @pStatusCode = 3
SET @pStatusText = N'Error'


SET @pAccountId = ( SELECT AC_ACCOUNT_ID From ACCOUNTS where ac_track_data  = dbo.TrackDataToInternal(@TrackData))
SET @pAccountId = ISNULL (@pAccountId, 0)


IF ( @pAccountId > 0 )
BEGIN

    SET @pStatusCode = 2
    SET @pStatusText = N'Account Blocked'

    SELECT @pBlocked = AC_BLOCKED From ACCOUNTS where ac_track_data  = dbo.TrackDataToInternal(@TrackData)

    SET @pSiteId = ISNULL ( ( SELECT   CAST (GP_KEY_VALUE AS INT) 
                       FROM   GENERAL_PARAMS
                      WHERE   GP_GROUP_KEY   = 'Site' 
                        AND   GP_SUBJECT_KEY = 'Identifier' ), 0 )

    SET @pToday = ( SELECT dbo.TodayOpening (@pSiteId) )


    SET @pPlayed = ( SELECT SUM(PS_PLAYED_AMOUNT) FROM PLAY_SESSIONS WHERE PS_ACCOUNT_ID = @pAccountId AND PS_STARTED >=  @pToday )
    SET @pWon    = ( SELECT SUM(PS_WON_AMOUNT)    FROM PLAY_SESSIONS WHERE PS_ACCOUNT_ID = @pAccountId AND PS_STARTED >=  @pToday )

    IF ( @pBlocked = 0 )
    BEGIN
        -- Insert statements for procedure here
        SELECT   0                       StatusCode
               , 'Success'               StatusText
               , AC_HOLDER_NAME          PlayerName
               , AC_HOLDER_GENDER        Gender
               , AC_HOLDER_BIRTH_DATE    Birthdate
               , AC_HOLDER_LEVEL         Level
               , 'PLATINO'               LevelName
               , CAST (AC_POINTS AS INT) Points
               , AC_LAST_ACTIVITY        LastActivity
               , ISNULL (@pPlayed, 0)    PlayedToday
               , ISNULL (@pWon,    0)    WonToday

        From ACCOUNTS
        where ac_account_id = @pAccountId
    
        SET @pStatusCode = 0
        SET @pStatusText = N'Success'

    END
END


IF ( @pStatusCode <> 0 )
    SELECT  @pStatusCode            StatusCode
           , @pStatusText  StatusText
           , NULL PlayerName
           , NULL Gender
           , NULL Birthdate
           , NULL Level
           , NULL LevelName
           , NULL Points
           , NULL LastActivity
           , NULL PlayedToday
           , NULL WonToday

END

GO

---
--- Checkbox to control the visible promotions on PromoBox
---
ALTER TABLE dbo.promotions ADD pm_visible_on_promobox   bit NOT NULL DEFAULT (1)

---
--- New Level system: ExpirationDayMonth, PointsToKeep, NoActivity
---
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('PlayerTracking', 'Level02.PointsToKeep', '');
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('PlayerTracking', 'Level03.PointsToKeep', '');
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('PlayerTracking', 'Level04.PointsToKeep', '');

INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('PlayerTracking', 'Level02.MaxDaysNoActivity', '');
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('PlayerTracking', 'Level03.MaxDaysNoActivity', '');
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('PlayerTracking', 'Level04.MaxDaysNoActivity', '');

INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('PlayerTracking', 'Levels.ExpirationDayMonth', '');


GO


---
--- Index for DrawTickets:
---

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[draw_tickets]') AND name = N'IX_draw_id')
DROP INDEX [IX_draw_id] ON [dbo].[draw_tickets] WITH ( ONLINE = OFF )

GO

CREATE NONCLUSTERED INDEX [IX_draw_id] ON [dbo].[draw_tickets] 
(
    [dt_draw_id] ASC,
    [dt_account_id] ASC,
    [dt_created] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]



GO
COMMIT
