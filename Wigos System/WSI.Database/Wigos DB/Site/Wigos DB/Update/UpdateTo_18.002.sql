/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 1;

SET @New_ReleaseId = 2;
SET @New_ScriptName = N'UpdateTo_18.002.sql';
SET @New_Description = N'Player Tracking & Draws; audit_3gs; db_version_3gs; FK c2_jackpot_history.';

/**** CHECK VERSION SECTION *****/
IF (EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/****** TABLES ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[audit_3gs]') AND type in (N'U'))
DROP TABLE [dbo].[audit_3gs];

CREATE TABLE [dbo].[audit_3gs](
	[a3gs_execution] [datetime] NOT NULL CONSTRAINT [DF_audit_procedure_a3gs_exec_time]  DEFAULT (getdate()),
	[a3gs_id] [bigint] IDENTITY(1,1) NOT NULL,
	[a3gs_procedure] [nvarchar](50) NOT NULL,
	[a3gs_account_id] [nvarchar](24) NOT NULL,
	[a3gs_vendor_id] [nvarchar](16) NOT NULL,
	[a3gs_machine_number] [int] NOT NULL,
	[a3gs_serial_number] [nvarchar](30) NULL,
	[a3gs_session_id] [bigint] NULL,
	[a3gs_balance] [money] NULL,
	[a3gs_status_code] [int] NOT NULL,
	[a3gs_input] [nvarchar](max) NULL,
	[a3gs_output] [nvarchar](max) NULL
) ON [PRIMARY];

CREATE NONCLUSTERED INDEX [IX_audit_3gs] ON [dbo].[audit_3gs] 
(
	[a3gs_execution] ASC,
	[a3gs_vendor_id] ASC,
	[a3gs_machine_number] ASC,
	[a3gs_status_code] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY];

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[accounts]') and name = 'ac_holder_id')
ALTER TABLE [dbo].[accounts]   ADD
    [ac_holder_id] [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [ac_holder_address_01] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [ac_holder_address_02] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [ac_holder_address_03] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [ac_holder_city] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [ac_holder_zip] [nvarchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [ac_holder_email_01] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [ac_holder_email_02] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [ac_holder_phone_number_01] [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [ac_holder_phone_number_02] [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [ac_holder_comments] [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [ac_holder_gender] [int] NULL,
    [ac_holder_marital_status] [int] NULL,
    [ac_holder_birth_date] [datetime] NULL,
    [ac_draw_last_play_session_id] [bigint] NOT NULL CONSTRAINT [DF_accounts_ac_draw_last_session_id]  DEFAULT ((0)),
    [ac_draw_last_play_session_remainder] [money] NOT NULL CONSTRAINT [DF_accounts_ac_draw_last_remainder]  DEFAULT ((0)),
    [ac_holder_id_indexed]  AS (isnull([ac_holder_id],'<NULL>'+CONVERT([varchar],[ac_account_id],(0))));

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[accounts]') AND name = N'IX_ac_holder_id_indexed')
DROP INDEX [IX_ac_holder_id_indexed] ON [dbo].[accounts];

CREATE UNIQUE NONCLUSTERED INDEX [IX_ac_holder_id_indexed] ON [dbo].[accounts] 
(
	[ac_holder_id_indexed] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY];

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[draws]') AND type in (N'U'))
DROP TABLE [dbo].[draws];

CREATE TABLE [dbo].[draws](
	[dr_id] [bigint] IDENTITY(1,1) NOT NULL,
	[dr_name] [varchar](50) NOT NULL,
	[dr_starting_date] [datetime] NOT NULL,
	[dr_ending_date] [datetime] NOT NULL,
	[dr_last_number] [bigint] NOT NULL CONSTRAINT [DF_draws_dr_last_number]  DEFAULT ((0)),
	[dr_number_price] [money] NOT NULL,
	[dr_status] [int] NOT NULL CONSTRAINT [DF_draws_dr_status]  DEFAULT ((0))
) ON [PRIMARY];

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[draw_tickets]') AND type in (N'U'))
DROP TABLE [dbo].[draw_tickets];

CREATE TABLE [dbo].[draw_tickets](
	[dt_id] [bigint] IDENTITY(1,1) NOT NULL,
	[dt_created] [datetime] NOT NULL CONSTRAINT [DF_draw_tickets_dt_created]  DEFAULT (getdate()),
	[dt_draw_id] [bigint] NOT NULL,
	[dt_account_id] [bigint] NOT NULL,
	[dt_first_number] [bigint] NOT NULL,
	[dt_last_number] [bigint] NOT NULL,
	[dt_voucher_id] [bigint] NULL
) ON [PRIMARY];

CREATE NONCLUSTERED INDEX [IX_draw_id] ON [dbo].[draw_tickets] 
(
	[dt_draw_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY];    

CREATE NONCLUSTERED INDEX [IX_first_number] ON [dbo].[draw_tickets] 
(
	[dt_first_number] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY];

CREATE NONCLUSTERED INDEX [IX_last_number] ON [dbo].[draw_tickets] 
(
	[dt_last_number] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY];

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[db_version_interface_3gs]') AND type in (N'U'))
DROP TABLE [dbo].[db_version_interface_3gs];

CREATE TABLE [dbo].[db_version_interface_3gs](
	[db_client_id] [int] NOT NULL,
	[db_common_build_id] [int] NOT NULL,
	[db_client_build_id] [int] NOT NULL,
	[db_release_id] [int] NOT NULL,
	[db_updated_script] [nvarchar](50) NULL,
	[db_updated] [datetime] NULL,
	[db_description] [nvarchar](1000) NULL,
 CONSTRAINT [PK_db_version_interface_3gs] PRIMARY KEY CLUSTERED 
(
	[db_client_id] ASC,
	[db_common_build_id] ASC,
	[db_client_build_id] ASC,
	[db_release_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY];

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FK_Jackpot_History_Draw_Audit_Plays]') AND type in (N'F'))
ALTER TABLE [dbo].[c2_jackpot_history] DROP CONSTRAINT fk_jackpot_history_draw_audit_plays;

ALTER TABLE [dbo].[c2_jackpot_history]  WITH CHECK ADD  CONSTRAINT [FK_Jackpot_History_Draw_Audit_Plays] FOREIGN KEY([c2jh_play_id])
REFERENCES [dbo].[c2_draw_audit_plays] ([dap_play_id]);
ALTER TABLE [dbo].[c2_jackpot_history] CHECK CONSTRAINT [FK_Jackpot_History_Draw_Audit_Plays];

/****** RECORDS ******/
IF EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Interface3GS' AND GP_SUBJECT_KEY ='AuditEnabled')
DELETE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Interface3GS' AND GP_SUBJECT_KEY ='AuditEnabled';

INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('Interface3GS'
           ,'AuditEnabled'
           ,'0');

IF EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Interface3GS' AND GP_SUBJECT_KEY ='IgnoreMachineNumber')
DELETE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Interface3GS' AND GP_SUBJECT_KEY ='IgnoreMachineNumber';

INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('Interface3GS'
           ,'IgnoreMachineNumber'
           ,'0');

IF EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Interface3GS' AND GP_SUBJECT_KEY ='IgnoreSessionID')
DELETE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Interface3GS' AND GP_SUBJECT_KEY ='IgnoreSessionID';

INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('Interface3GS'
           ,'IgnoreSessionID'
           ,'0');

IF EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.DrawTicket' AND GP_SUBJECT_KEY ='MaxNumbersPerTicket')
DELETE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.DrawTicket' AND GP_SUBJECT_KEY ='MaxNumbersPerTicket';

INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('Cashier.DrawTicket'
           ,'MaxNumbersPerTicket'
           ,'300');

IF EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.DrawTicket' AND GP_SUBJECT_KEY ='SalesIntervalInHours')
DELETE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.DrawTicket' AND GP_SUBJECT_KEY ='SalesIntervalInHours';

INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('Cashier.DrawTicket'
           ,'SalesIntervalInHours'
           ,'24');

IF EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.AddAmount' AND GP_SUBJECT_KEY ='CustomButton1')
DELETE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.AddAmount' AND GP_SUBJECT_KEY ='CustomButton1';

INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('Cashier.AddAmount'
           ,'CustomButton1'
           ,'50');

IF EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.AddAmount' AND GP_SUBJECT_KEY ='CustomButton2')
DELETE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.AddAmount' AND GP_SUBJECT_KEY ='CustomButton2';

INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('Cashier.AddAmount'
           ,'CustomButton2'
           ,'100');

IF EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.AddAmount' AND GP_SUBJECT_KEY ='CustomButton3')
DELETE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.AddAmount' AND GP_SUBJECT_KEY ='CustomButton3';

INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('Cashier.AddAmount'
           ,'CustomButton3'
           ,'200');

IF EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.AddAmount' AND GP_SUBJECT_KEY ='CustomButton4')
DELETE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.AddAmount' AND GP_SUBJECT_KEY ='CustomButton4';

INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('Cashier.AddAmount'
           ,'CustomButton4'
           ,'300');

IF EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.AddAmount' AND GP_SUBJECT_KEY ='CustomButton4')
DELETE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.AddAmount' AND GP_SUBJECT_KEY ='CustomButton4';

INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('Cashier.AddAmount'
           ,'CustomButton4'
           ,'300');

INSERT INTO [dbo].[db_version_interface_3gs]
           ([db_client_id]
           ,[db_common_build_id]
           ,[db_client_build_id]
           ,[db_release_id]
           ,[db_updated_script]
           ,[db_updated]
           ,[db_description])
     VALUES
           (18
           ,100
           ,1
           ,1
           ,@New_ScriptName
           ,Getdate()
           ,'Initial Creation for Alesis');

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;

END
ELSE
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION
GO
