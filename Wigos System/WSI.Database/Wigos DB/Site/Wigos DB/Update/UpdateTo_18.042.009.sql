/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 41;

SET @New_ReleaseId = 42;
SET @New_ScriptName = N'UpdateTo_18.042.009.sql';
SET @New_Description = N'New index IX_datetime in table Cashier_Vouchers; new records in general params; updated 3GS procedures; new table alarms.';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/
/* Play_Sessions */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[Play_Sessions]') and name = 'ps_balance_mismatch')
  ALTER TABLE [dbo].[Play_Sessions]
          ADD [ps_balance_mismatch] [bit] NULL;
ELSE
  SELECT '***** Field Play_Sessions.ps_balance_mismatch already exists *****';
           
/* Draws */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[Draws]') and name = 'dr_limit_per_operation')
  ALTER TABLE [dbo].[Draws]
          ADD [dr_limit_per_operation] [int] NOT NULL CONSTRAINT DF_draws_dr_limit_per_operation DEFAULT (0);
ELSE
  SELECT '***** Field Draws.dr_limit_per_operation already exists *****';
  
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[Draws]') and name = 'dr_first_cash_in_constrained')
  ALTER TABLE [dbo].[Draws]
          ADD [dr_first_cash_in_constrained] [bit] NOT NULL CONSTRAINT DF_draws_dr_first_cash_in_not_constrained DEFAULT (0);
ELSE
  SELECT '***** Field Draws.dr_first_cash_in_constrained already exists *****';

/* Promotions */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[Promotions]') and name = 'pm_global_daily_limit')
  ALTER TABLE [dbo].[Promotions]
          ADD [pm_global_daily_limit] [money] NULL;
ELSE
  SELECT '***** Field Promotions.pm_global_daily_limit already exists *****';
  
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[Promotions]') and name = 'pm_global_monthly_limit')
  ALTER TABLE [dbo].[Promotions]
          ADD [pm_global_monthly_limit] [money] NULL;
ELSE
  SELECT '***** Field Promotions.pm_global_monthly_limit already exists *****';

/****** Object:  Table [dbo].[alarms]    Script Date: 10/28/2011 18:32:38 ******/
IF NOT EXISTS (SELECT * FROM SYS.OBJECTS WHERE TYPE = 'U' AND NAME = 'alarms')
  BEGIN
    CREATE TABLE [dbo].[alarms](
	[al_alarm_id] [bigint] IDENTITY(1,1) NOT NULL,
	[al_source_code] [int] NOT NULL,
	[al_source_id] [bigint] NOT NULL,
	[al_source_name] [nvarchar](100) NOT NULL,
	[al_alarm_code] [int] NOT NULL,
	[al_alarm_name] [nvarchar](50) NOT NULL,
	[al_alarm_description] [nvarchar](max) NULL,
	[al_severity] [int] NOT NULL,
	[al_reported] [datetime] NOT NULL CONSTRAINT [DF_alarms_al_reported]  DEFAULT (getdate()),
	[al_datetime] [datetime] NOT NULL,
	[al_ack_datetime] [datetime] NULL,
	[al_ack_user_id] [int] NULL,
	[al_ack_user_name] [nvarchar](50) NULL,
	[al_timestamp] [timestamp] NOT NULL,
    CONSTRAINT [PK_alarms] PRIMARY KEY CLUSTERED 
    (
	[al_alarm_id] ASC
    )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
    ) ON [PRIMARY]
  END
ELSE
    SELECT '***** Table alarms already exists *****';


/****** INDEXES ******/
/****** Object:  Index [IX_audit_3gs_session_id_3gs_id]    Script Date: 10/14/2011 12:29:59 ******/
IF  NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[audit_3gs]') AND name = N'IX_audit_3gs_session_id_3gs_id')
CREATE NONCLUSTERED INDEX [IX_audit_3gs_session_id_3gs_id] ON [dbo].[audit_3gs] 
(
	[a3gs_session_id] ASC,
	[a3gs_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY];
ELSE
    SELECT '***** Index audit_3gs.IX_audit_3gs_session_id_3gs_id already exists *****';

/****** Object:  Index [IX_alarms_datetime_severity_source]    Script Date: 10/28/2011 18:33:20 ******/
IF  NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[alarms]') AND name = N'IX_alarms_datetime_severity_source')
  CREATE NONCLUSTERED INDEX [IX_alarms_datetime_severity_source] ON [dbo].[alarms] 
  (
	[al_datetime] DESC,
	[al_severity] DESC,
	[al_source_code] ASC,
	[al_source_id] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
ELSE
    SELECT '***** Index alarms.IX_alarms_datetime_severity_source already exists *****';


/****** RECORDS ******/
/* Interface3GS.MaximumBalanceErrorCents */
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Interface3GS' AND GP_SUBJECT_KEY ='MaximumBalanceErrorCents')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key]
             ,[gp_subject_key]
             ,[gp_key_value])
       VALUES
             ('Interface3GS'
             ,'MaximumBalanceErrorCents'
             ,'40000');
ELSE
  SELECT '***** Record Interface3GS.MaximumBalanceErrorCents already exists *****';

/* Site.DisableNewAlarms */
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Site' AND GP_SUBJECT_KEY ='DisableNewAlarms')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key]
             ,[gp_subject_key]
             ,[gp_key_value])
       VALUES
             ('Site'
             ,'DisableNewAlarms'
             ,'0');
ELSE
  SELECT '***** Record Site.DisableNewAlarms already exists *****';


/**************************************** 3GS Section ****************************************/

/**** CHECK DATABASE EXISTENCE SECTION *****/
GO
IF (NOT EXISTS (SELECT * FROM sys.databases WHERE name = 'sPOS'))
BEGIN
  SELECT 'No 3GS interface to update.';

  raiserror('No 3GS interface to update', 20, -1) with log;
END
ELSE 
  SELECT 'Updating 3GS interface...';


/**** 3GS DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);
    
/**** 3GS INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 8;

SET @New_ReleaseId = 9;
SET @New_ScriptName = N'UpdateTo_18.042.009.sql';
SET @New_Description = N'New control on play session mismatches.';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version_interface_3gs WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
  /**** VERSION FAILURE SECTION *****/
  SELECT 'Wrong 3GS DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
  FROM DB_VERSION_INTERFACE_3GS

  raiserror('Not updated', 20, -1) with log
END

/**** UPDATE VERSION SECTION *****/
UPDATE db_version_interface_3gs
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;


/**** UPDATE BODY SECTION *****/
GO


/**** zsp_Audit *****/
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[zsp_Audit]') AND type in (N'P', N'PC'))
BEGIN
  SELECT 'Procedure zsp_Audit not found!';
  
  raiserror('Not updated', 20, -1) with log;
END
GO

ALTER PROCEDURE [dbo].[zsp_Audit]
  @ProcedureName nvarchar(50),
  @AccountID     nvarchar(24), 
  @VendorID      nvarchar(16), 
  @SerialNumber  nvarchar(30), 
  @MachineNumber int,
  @SessionId     bigint,
  @StatusCode    int,
  @Balance       Money,
  @ForceAudit    bit,
  @Input         nvarchar(MAX),
  @Output        nvarchar(MAX) 
AS
BEGIN 

  DECLARE @active int
  
  -- Check general params
  SELECT @active = CAST(GP_KEY_VALUE AS int)
    FROM GENERAL_PARAMS 
   WHERE GP_GROUP_KEY = 'Interface3GS' AND GP_SUBJECT_KEY ='AuditEnabled'

  IF ( @active = 1 OR (@StatusCode <> 0 AND @StatusCode < 100) OR @ForceAudit = 1 )
  BEGIN

    INSERT INTO AUDIT_3GS ( A3GS_PROCEDURE
                          , A3GS_ACCOUNT_ID
                          , A3GS_VENDOR_ID
                          , A3GS_SERIAL_NUMBER
                          , A3GS_MACHINE_NUMBER
                          , A3GS_SESSION_ID
                          , A3GS_STATUS_CODE
                          , A3GS_BALANCE
                          , A3GS_INPUT
                          , A3GS_OUTPUT )
                   VALUES ( @ProcedureName
                          , @AccountID
                          , @VendorID
                          , @SerialNumber
                          , @MachineNumber
                          , @SessionId
                          , @StatusCode
                          , @Balance
                          , @Input
                          , @Output )
                          
  END      
                      
END -- [zsp_Audit]

GO   


/**** zsp_SessionUpdate *****/
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[zsp_SessionUpdate]') AND type in (N'P', N'PC'))
BEGIN
  SELECT 'Procedure zsp_SessionUpdate not found!';
  
  raiserror('Not updated', 20, -1) with log;
END
GO

ALTER PROCEDURE [dbo].[zsp_SessionUpdate]
  @AccountId      varchar(24),
  @VendorId       varchar(16),
  @SerialNumber   varchar(30),
  @MachineNumber  int,
  @SessionId      bigint,
  @AmountPlayed   money,
  @AmountWon      money,
  @GamesPlayed    int,
  @GamesWon       int,
  @CreditBalance  money,
  @CurrentJackpot money = 0
AS
BEGIN

  BEGIN TRAN

  -- SET NOCOUNT ON added to prevent extra result sets from
  -- interfering with SELECT statements.
  SET NOCOUNT ON;
	
  DECLARE @status_code         int
  DECLARE @status_text         varchar (254)
  DECLARE @terminal_id         int
  DECLARE @account_id          bigint
  DECLARE @previous_balance    money
  DECLARE @delta_amount_played money
  DECLARE @delta_amount_won    money
  DECLARE @ignore_session_id   int
  DECLARE @dummy               int
  DECLARE @ps_initial_balance        money
  DECLARE @calculated_final_balance  money
  DECLARE @balance_mismatch          bit
  DECLARE @big_mismatch              bit
  DECLARE @balance_mismatch_text     varchar (254)
  DECLARE @final_balance_to_save     money
  DECLARE @force_audit               bit

  SET @status_code         = 0
  SET @status_text         = 'Successful Session Update'
  SET @delta_amount_played = 0
  SET @delta_amount_won    = 0
  SET @dummy               = 0
  SET @balance_mismatch    = 0
  SET @big_mismatch        = 0  
  SET @balance_mismatch_text = ' '

  SELECT @terminal_id = dbo.GetTerminalID(@VendorId, @SerialNumber, @MachineNumber) 
		
  IF (@terminal_id = 0)
  BEGIN
    SET @status_code = 2
    SET @status_text = 'Invalid Machine Information'
    GOTO ERROR_PROCEDURE
  END

  SELECT @account_id = dbo.GetAccountID(@AccountId)

  IF (@account_id = 0)
  BEGIN
    SET @status_code = 1
    SET @status_text = 'Invalid Account Number'
    GOTO ERROR_PROCEDURE
  END
  
  SELECT @ignore_session_id = CAST(GP_KEY_VALUE AS int)
    FROM GENERAL_PARAMS 
   WHERE GP_GROUP_KEY = 'Interface3GS' AND GP_SUBJECT_KEY ='IgnoreSessionID'
  
  IF ( @ignore_session_id = 1 )
  BEGIN
    SELECT @SessionId = ISNULL(AC_CURRENT_PLAY_SESSION_ID , AC_LAST_PLAY_SESSION_ID ) 
      FROM   ACCOUNTS                   
     WHERE ( AC_ACCOUNT_ID = @account_id )
  END

  --
  -- ACC 13-OCT-2011: Check reported CreditBalance.
  --
  
  -- Set FinalBalanceToSave to the Reported Balance
  SET @final_balance_to_save = @CreditBalance
  
  EXECUTE dbo.CheckFinalBalanceMismatch @SessionId, @AmountPlayed, @AmountWon, @CreditBalance, 
                                        @calculated_final_balance OUTPUT, @big_mismatch OUTPUT, @balance_mismatch OUTPUT
  IF (@big_mismatch = 1)
  BEGIN                                
    -- Set FinalBalanceToSave to the Calculated Balance
    if (@calculated_final_balance < @final_balance_to_save)
    BEGIN
      SET @final_balance_to_save = @calculated_final_balance
    END
  END

  EXECUTE dbo.SessionUpdate_Internal @account_id, @terminal_id, @SerialNumber, @SessionId,
                                     @AmountPlayed, @AmountWon, @GamesPlayed, @GamesWon,
                                     @final_balance_to_save, @CurrentJackpot, @dummy, 
                                     @delta_amount_played OUTPUT, @delta_amount_won OUTPUT,
                                     @status_code OUTPUT, @status_text OUTPUT
  IF ( @status_code <> 0 )
    GOTO ERROR_PROCEDURE

  IF (@big_mismatch = 1)
  BEGIN
    SET @status_code = 5
    SET @status_text = 'Final balance does not match validation. '
    SET @balance_mismatch_text = 'Reported='+CAST(@CreditBalance AS nvarchar)
                                +'; Calculated='+CAST(@calculated_final_balance AS nvarchar)    
  
    COMMIT TRAN
	  GOTO OK    
  END
  
  -- Insert Movement
  
  -- Use delta values to calculate initial balance
  --SET @previous_balance = @CreditBalance + @AmountPlayed - @AmountWon
  SET @previous_balance = @CreditBalance + @delta_amount_played - @delta_amount_won
  
  EXECUTE dbo.InsertMovement @SessionId, @account_id, @terminal_id, 0, @previous_balance, @delta_amount_played, @delta_amount_won, @CreditBalance

  SET @status_text = 'Successful Session Update'
	
  COMMIT TRAN
	GOTO OK  
	  
ERROR_PROCEDURE:
  ROLLBACK TRAN
	  
OK: 

	-- Audit MBF 17-JUN-2010
	BEGIN TRAN
	
    DECLARE @input AS nvarchar(MAX)
    DECLARE @output AS nvarchar(MAX)	
  		  	
	  SET @input = '@AccountID='+@AccountID
	             +';@VendorId='+@VendorId
	             +';@SerialNumber='+CAST (@SerialNumber AS nvarchar)
	             +';@MachineNumber='+CAST (@MachineNumber AS nvarchar)
	             +';@SessionId='+CAST (@SessionId AS nvarchar)
	             +';@AmountPlayed='+CAST (@AmountPlayed AS nvarchar)
	             +';@AmountWon='+CAST (@AmountWon AS nvarchar)
	             +';@GamesPlayed='+CAST (@GamesPlayed AS nvarchar)
	             +';@GamesWon='+CAST (@GamesWon AS nvarchar)
	             +';@CreditBalance='+CAST (@CreditBalance AS nvarchar)
	             +';@CurrentJackpot='+CAST (@CurrentJackpot AS nvarchar)
  	           
  	           
    SET @output = 'StatusCode='+CAST (@status_code AS nvarchar)
                 +';StatusText='+@status_text+@balance_mismatch_text

    SET @force_audit = 0
    IF @balance_mismatch = 1
    BEGIN
      SET @force_audit = 1
    END
     	
	  EXECUTE dbo.zsp_Audit 'zsp_SessionUpdate'
                        , @AccountID
                        , @VendorId 
                        , @SerialNumber
                        , @MachineNumber
                        , @SessionId
                        , @status_code
                        , @CreditBalance
                        , @force_audit
                        , @input
                        , @output
                      
  -- ACC 13-OCT-2011: Set balance mismatch flag if status is OK or ERROR_BALANCE_MISMATCH
  IF @status_code = 0 OR @status_code = 5
  BEGIN
    EXECUTE dbo.SetPlaySessionBalanceMismatch @SessionId, @balance_mismatch
  END

  COMMIT TRAN
	-- End Audit

  SELECT @status_code AS StatusCode, @status_text AS StatusText

END -- zsp_SessionUpdate

GO   


/**** zsp_SessionEnd *****/
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[zsp_SessionEnd]') AND type in (N'P', N'PC'))
BEGIN
  SELECT 'Procedure zsp_SessionEnd not found!';
  
  raiserror('Not updated', 20, -1) with log;
END
GO

ALTER PROCEDURE [dbo].[zsp_SessionEnd]
  @AccountId      varchar(24),
  @VendorId       varchar(16),
  @SerialNumber   varchar(30),
  @MachineNumber  int,
  @SessionId 	    bigint,
  @AmountPlayed   money,
  @AmountWon      money,
  @GamesPlayed    int,
  @GamesWon       int,
  @CreditBalance  money,
  @CurrentJackpot money = 0
AS
BEGIN

  BEGIN TRAN

  -- SET NOCOUNT ON added to prevent extra result sets from
  -- interfering with SELECT statements.
  SET NOCOUNT ON;

  DECLARE @status_code               int
  DECLARE @status_text               varchar (254)
  DECLARE @terminal_id               int
  DECLARE @account_id                bigint
  DECLARE @previous_balance          money
  DECLARE @delta_amount_played       money
  DECLARE @delta_amount_won          money
  DECLARE @ignore_session_id         int
  DECLARE @is_end_session            int
  DECLARE @ps_initial_balance        money
  DECLARE @calculated_final_balance  money
  DECLARE @balance_mismatch          bit
  DECLARE @big_mismatch              bit
  DECLARE @balance_mismatch_text     varchar (254)
  DECLARE @final_balance_to_save     money
  DECLARE @force_audit               bit

  SET @status_code         = 0
  SET @status_text         = 'Successful End Session'
  SET @delta_amount_played = 0
  SET @delta_amount_won    = 0
  SET @is_end_session      = 1
  SET @balance_mismatch    = 0
  SET @big_mismatch        = 0
  SET @balance_mismatch_text = ' '

  SELECT @terminal_id = dbo.GetTerminalID(@VendorId, @SerialNumber, @MachineNumber) 

  IF (@terminal_id = 0)
  BEGIN
    SET @status_code = 2
    SET @status_text = 'Invalid Machine Information'
    GOTO ERROR_PROCEDURE
  END

  SELECT @account_id = dbo.GetAccountID(@AccountId)

  IF (@account_id = 0)
  BEGIN
    SET @status_code = 1
    SET @status_text = 'Invalid Account Number'
    GOTO ERROR_PROCEDURE
  END
  
  SELECT @ignore_session_id = CAST(GP_KEY_VALUE AS int)
    FROM GENERAL_PARAMS 
   WHERE GP_GROUP_KEY = 'Interface3GS' AND GP_SUBJECT_KEY ='IgnoreSessionID'
  
  IF ( @ignore_session_id = 1 )
  BEGIN
    SELECT @SessionId = ISNULL(AC_CURRENT_PLAY_SESSION_ID , 0 ) 
      FROM   ACCOUNTS                   
     WHERE ( AC_ACCOUNT_ID = @account_id )
  END

  --
  -- ACC 13-OCT-2011: Check reported CreditBalance.
  --
  
  -- Set FinalBalanceToSave to the Reported Balance
  SET @final_balance_to_save = @CreditBalance

  EXECUTE dbo.CheckFinalBalanceMismatch @SessionId, @AmountPlayed, @AmountWon, @CreditBalance, 
                                        @calculated_final_balance OUTPUT, @big_mismatch OUTPUT, @balance_mismatch OUTPUT
  IF (@big_mismatch = 1)
  BEGIN                                
    -- Set FinalBalanceToSave to the Calculated Balance
    if (@calculated_final_balance < @final_balance_to_save)
    BEGIN
      SET @final_balance_to_save = @calculated_final_balance
    END
  END

  EXECUTE dbo.SessionUpdate_Internal @account_id, @terminal_id, @SerialNumber, @SessionId,
                                     @AmountPlayed, @AmountWon, @GamesPlayed, @GamesWon,
                                     @final_balance_to_save, @CurrentJackpot, @is_end_session, 
                                     @delta_amount_played OUTPUT, @delta_amount_won OUTPUT,
                                     @status_code OUTPUT, @status_text OUTPUT
  IF ( @status_code <> 0 )
    GOTO ERROR_PROCEDURE

  IF (@big_mismatch = 1)
  BEGIN
    SET @status_code = 5
    SET @status_text = 'Final balance does not match validation. '
    SET @balance_mismatch_text = 'Reported='+CAST(@CreditBalance AS nvarchar)
                                +'; Calculated='+CAST(@calculated_final_balance AS nvarchar)    
  
    COMMIT TRAN
	  GOTO OK    
  END
  
  -- close play session
  EXECUTE dbo.CloseOpenedPlaySession @SessionId, @status_code OUTPUT, @status_text OUTPUT

  IF ( @status_code <> 0 )
    GOTO ERROR_PROCEDURE
    
  -- Close account session
  EXECUTE dbo.CloseAccountSession @account_id

  -- Insert Movement
  
  -- Use delta values to calculate initial balance
  --SET @previous_balance = @CreditBalance + @AmountPlayed - @AmountWon
  SET @previous_balance = @CreditBalance + @delta_amount_played - @delta_amount_won

  IF (1 = (SELECT   CASE WHEN ( (AC_PROMO_CREATION <= GETDATE()) AND (GETDATE() < AC_PROMO_EXPIRATION) AND (AC_PROMO_BALANCE >0) ) THEN 1 ELSE 0 END
            FROM   ACCOUNTS                   
           WHERE ( AC_ACCOUNT_ID = @account_id )))
  BEGIN
    -- PromoEndSession   = 25,
    EXECUTE dbo.InsertMovement @SessionId, @account_id, @terminal_id, 25, @previous_balance, @delta_amount_played, @delta_amount_won, @CreditBalance

  END
  ELSE
  BEGIN
    -- EndCardSession = 6,
    EXECUTE dbo.InsertMovement @SessionId, @account_id, @terminal_id,  6, @previous_balance, @delta_amount_played, @delta_amount_won, @CreditBalance
  END
	  
  SET @status_text = 'Successful End Session'

  COMMIT TRAN
	GOTO OK  
	  
	ERROR_PROCEDURE:
	  ROLLBACK TRAN
	  
	OK:  
	
	-- Audit MBF 17-JUN-2010
	BEGIN TRAN
	
    DECLARE @input AS nvarchar(MAX)
    DECLARE @output AS nvarchar(MAX)	
  		  	
	  SET @input = '@AccountID='+@AccountID
	             +';@VendorId='+@VendorId
	             +';@SerialNumber='+CAST (@SerialNumber AS nvarchar)
	             +';@MachineNumber='+CAST (@MachineNumber AS nvarchar)
	             +';@SessionId='+CAST (@SessionId AS nvarchar)
	             +';@AmountPlayed='+CAST (@AmountPlayed AS nvarchar)
	             +';@AmountWon='+CAST (@AmountWon AS nvarchar)
	             +';@GamesPlayed='+CAST (@GamesPlayed AS nvarchar)
	             +';@GamesWon='+CAST (@GamesWon AS nvarchar)
	             +';@CreditBalance='+CAST (@CreditBalance AS nvarchar)
	             +';@CurrentJackpot='+CAST (@CurrentJackpot AS nvarchar)
  	           
    SET @output = 'StatusCode='+CAST (@status_code AS nvarchar)
                 +';StatusText='+@status_text+@balance_mismatch_text
     	
    SET @force_audit = 0
    IF @balance_mismatch = 1
    BEGIN
      SET @force_audit = 1
    END
     	
	  EXECUTE dbo.zsp_Audit 'zsp_SessionEnd'
                        , @AccountID
                        , @VendorId 
                        , @SerialNumber
                        , @MachineNumber
                        , @SessionId
                        , @status_code
                        , @CreditBalance
                        , @force_audit
                        , @input
                        , @output

  -- ACC 13-OCT-2011: Set balance mismatch flag if status is OK or ERROR_BALANCE_MISMATCH
  IF @status_code = 0 OR @status_code = 5
  BEGIN
    EXECUTE dbo.SetPlaySessionBalanceMismatch @SessionId, @balance_mismatch
  END

  COMMIT TRAN
	-- End Audit
	
  SELECT @status_code AS StatusCode, @status_text AS StatusText

END -- zsp_SessionEnd

GO   


/**** zsp_SessionStart *****/
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[zsp_SessionStart]') AND type in (N'P', N'PC'))
BEGIN
  SELECT 'Procedure zsp_SessionStart not found!';
  
  raiserror('Not updated', 20, -1) with log;
END
GO

ALTER PROCEDURE [dbo].[zsp_SessionStart]
	@AccountID 	    varchar(24),
	@VendorId 	      varchar(16),
	@SerialNumber    varchar(30),
	@MachineNumber   int,
	@CurrentJackpot  money = 0,
	@VendorSessionID bigint = 0
AS
BEGIN

  BEGIN TRAN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
		
  DECLARE @status_code         int
	DECLARE @status_text         varchar (254)
	DECLARE @session_id          bigint
	DECLARE @balance             money
	DECLARE @terminal_id         bigint
	DECLARE @account_id          bigint
	DECLARE @is_promotion        int
	DECLARE @promo_balance       money
	DECLARE @account_terminal_id bigint
	DECLARE @current_session_id  bigint
	DECLARE @movement_type       int
	DECLARE @cash_in_while_playing money
    -- 09-AUG-2010, Return the welcome message with the customer name
    DECLARE @holder_name         nvarchar (50)
	
    SET @status_code        = 0
	SET @status_text        = 'Bienvenido'
	SET @session_id         = 0
	SET @balance            = 0
	SET @current_session_id = 0

	SELECT @terminal_id = dbo.GetTerminalID(@VendorId, @SerialNumber, @MachineNumber) 
		
  IF (@terminal_id = 0)
  BEGIN
    EXECUTE dbo.InsertTerminal 0, @VendorId, @SerialNumber, @MachineNumber
    SET @status_code = 3
    SET @status_text = 'Invalid Machine Information'
	  COMMIT TRAN
    BEGIN TRAN
    GOTO ERROR_PROCEDURE
  END
  
	IF ((SELECT dbo.CheckTerminalBlocked(@terminal_ID)) = 1)
  BEGIN
    SET @status_code = 3
    SET @status_text = 'Invalid Machine Information'
    GOTO ERROR_PROCEDURE
  END

	SELECT @account_id = dbo.GetAccountID(@AccountID)
  
  IF (@account_ID = 0)
  BEGIN
    SET @status_code = 1
    SET @status_text = 'Invalid Account Number'
    GOTO ERROR_PROCEDURE
  END
	
	IF ((SELECT dbo.CheckAccountBlocked(@account_ID)) = 1)
  BEGIN
    SET @status_code = 4
    SET @status_text = 'Access Denied'
    GOTO ERROR_PROCEDURE
  END
	
	SELECT @is_promotion        = CASE WHEN ( (AC_PROMO_CREATION <= GETDATE()) AND (GETDATE() < AC_PROMO_EXPIRATION) AND (AC_PROMO_BALANCE >0) ) THEN 1 ELSE 0 END
       , @promo_balance       = AC_PROMO_BALANCE 
       , @balance             = AC_BALANCE 
       , @current_session_id  = ISNULL(AC_CURRENT_PLAY_SESSION_ID , 0 ) 
       , @account_terminal_id = ISNULL(AC_CURRENT_TERMINAL_ID , 0 )
       -- 09-AUG-2010, Return the welcome message with the customer name
       , @holder_name         = ISNULL(AC_HOLDER_NAME, ' ')
       , @cash_in_while_playing = ISNULL(AC_CASHIN_WHILE_PLAYING, 0)
  FROM   ACCOUNTS                   
  WHERE ( AC_ACCOUNT_ID = @account_ID )
	
	IF (@is_promotion = 1)
  BEGIN
    SET @balance = @promo_balance
  END
	
	
	IF (@current_session_id <> 0)
	BEGIN
	  IF ( @account_terminal_id <> @terminal_id )
	  BEGIN
	    SET @status_code = 2
      SET @status_text = 'Account Locked (In Session)' 
      GOTO ERROR_PROCEDURE
	  END

    IF (@cash_in_while_playing = 0)
    BEGIN
	    SET @status_code = 2
      SET @status_text = 'Account Locked (In Session)' 
      GOTO ERROR_PROCEDURE
	  END

    -- Close account session
    EXECUTE dbo.CloseAccountSession @account_id
  
	END





  DECLARE @insert_ps_table TABLE (StatusCode int, StatusText nvarchar(254), SessionID bigint)
  INSERT INTO @insert_ps_table EXECUTE  dbo.InsertPlaySession @terminal_id, @account_id, @balance, @is_promotion    
  SELECT @status_code = StatusCode
       , @status_text = StatusText
       , @session_id  = SessionID
    FROM @insert_ps_table
  
  IF ( @status_code <> 0 )
    GOTO ERROR_PROCEDURE
   
  EXECUTE dbo.InsertGameMeters @terminal_id
	
	SET @movement_type = CASE WHEN (@is_promotion = 1) THEN 24 ELSE 5 END
	
	EXECUTE dbo.InsertMovement @session_id
                           , @account_id
                           , @terminal_id
                           , @movement_type
                           , @balance
                           , 0
                           , 0
                           , @balance
	
	

	
    -- 09-AUG-2010, Return the welcome message with the customer name
    SET @status_text = 'Bienvenido ' + @holder_name
	--    IF (@is_promotion = 1)
	--    BEGIN
	--       SET @status_text = @status_text + ' --- NO REDIMIBLE ---'
	--    END

    -- AJQ 17-SEP-2010, Reset CashinWhilePlaying
    UPDATE ACCOUNTS
       SET AC_CASHIN_WHILE_PLAYING    = NULL
     WHERE AC_ACCOUNT_ID              = @account_id
       AND AC_CURRENT_TERMINAL_ID     = @terminal_id
       AND AC_CURRENT_PLAY_SESSION_ID = @session_id 
	 
	COMMIT TRAN
	GOTO OK  
	  
	ERROR_PROCEDURE:
	  ROLLBACK TRAN
	  
	OK:  
	
	-- Audit MBF 17-JUN-2010
	BEGIN TRAN
	
    DECLARE @input AS nvarchar(MAX)
    DECLARE @output AS nvarchar(MAX)	
  	
	  SET @input = '@AccountID='+@AccountID
	             +';@VendorId='+@VendorId
	             +';@SerialNumber='+CAST (@SerialNumber AS nvarchar)
	             +';@MachineNumber='+CAST (@MachineNumber AS nvarchar)
	             +';@CurrentJackpot='+CAST (@CurrentJackpot AS nvarchar)
	             +';@VendorSessionID='+CAST (@VendorSessionID AS nvarchar)
  	           
  	           
    SET @output = 'StatusCode='+CAST (@status_code AS nvarchar)
                 +';StatusText='+@status_text
                 +';SessionID='+CAST(@session_id AS nvarchar)
                 +';Balance='+CAST(@balance AS nvarchar)
     	
	  EXECUTE dbo.zsp_Audit 'zsp_SessionStart'
                        , @AccountID
                        , @VendorId 
                        , @SerialNumber
                        , @MachineNumber
                        , @session_id
                        , @status_code
                        , @balance
                        , 0
                        , @input
                        , @output        
  COMMIT TRAN
	-- End Audit
	
	SELECT @status_code AS StatusCode, @status_text AS StatusText, @session_id AS SessionId, @balance AS Balance

END -- zsp_SessionStart

GO   


/**** zsp_SendEvent *****/
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[zsp_SendEvent]') AND type in (N'P', N'PC'))
BEGIN
  SELECT 'Procedure zsp_SendEvent not found!';
  
  raiserror('Not updated', 20, -1) with log;
END
GO

ALTER PROCEDURE [dbo].[zsp_SendEvent]
	@AccountID 	     varchar(24),
	@VendorId 	     varchar(16),
	@SerialNumber    varchar(30),
	@MachineNumber   int,
	@EventID         bigint, 
	@Amount          money
AS
BEGIN

  BEGIN TRAN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
  DECLARE @status_code         int
	DECLARE @status_text         varchar (254)
	DECLARE @operation_code      int
	DECLARE @play_session_id     bigint
	DECLARE @terminal_id         bigint
	DECLARE @account_id          bigint
	DECLARE @account_terminal_id bigint
	
  SET @status_code        = @EventID + 100
	SET @status_text        = ''
		
	SELECT @terminal_id = dbo.GetTerminalID(@VendorId, @SerialNumber, @MachineNumber) 
	  
  IF (@terminal_id = 0)
  BEGIN
    EXECUTE dbo.InsertTerminal 0, @VendorId, @SerialNumber, @MachineNumber
    SET @status_code = 3
    SET @status_text = 'Invalid Machine Information'
	  COMMIT TRAN
    BEGIN TRAN
    GOTO ERROR_PROCEDURE
  END
		
	SELECT @account_id = dbo.GetAccountID(@AccountID)
	
	IF (@account_id = 0)
  BEGIN
    SET @status_code = 1
    SET @status_text = 'Invalid Account Number'
    GOTO ERROR_PROCEDURE
  END
	
	SET @operation_code = CASE WHEN (@EventID = 1) THEN 16
	                           WHEN (@EventID = 2) THEN 17
	                           WHEN (@EventID = 3) THEN 18
	                                               ELSE  0 END
	
	-- Unknown codes, do nothing
	IF (@operation_code = 0)
	BEGIN
    GOTO ERROR_PROCEDURE
	END
	
	SELECT @play_session_id     = ISNULL(AC_CURRENT_PLAY_SESSION_ID , 0 ) 
       , @account_terminal_id = ISNULL(AC_CURRENT_TERMINAL_ID , 0 )
  FROM   ACCOUNTS                   
  WHERE ( AC_ACCOUNT_ID = @account_id )
	
	-- Check if account has a play session
	IF (@play_session_id = 0)
	BEGIN
	  SET @status_code = 4
    SET @status_text = 'Access Denied'
    GOTO ERROR_PROCEDURE
	END
	
	-- Check if the terminal is the same as the playsession
	IF (@account_terminal_id <> @terminal_id)
	BEGIN
	  SET @status_code = 4
    SET @status_text = 'Access Denied'
    GOTO ERROR_PROCEDURE
	END
		
	INSERT INTO EVENT_HISTORY (EH_TERMINAL_ID 
                           , EH_SESSION_ID 
                           , EH_DATETIME 
                           , EH_EVENT_TYPE 
                           , EH_OPERATION_CODE 
                           , EH_OPERATION_DATA)
                     VALUES (@terminal_id
                           , @play_session_id
                           , GETDATE()
                           , 2 
                           , @operation_code
                           , @Amount)
	
	COMMIT TRAN
	GOTO OK  
	  
	ERROR_PROCEDURE:
	ROLLBACK TRAN
	  
	OK: 
	
	-- Audit MBF 17-JUN-2010
	BEGIN TRAN
	
    DECLARE @input AS nvarchar(MAX)
    DECLARE @output AS nvarchar(MAX)
  		  	
	  SET @input = '@AccountID='+@AccountID
	             +';@VendorId='+@VendorId
	             +';@SerialNumber='+CAST (@SerialNumber AS nvarchar)
	             +';@MachineNumber='+CAST (@MachineNumber AS nvarchar)
	             +';@EventID='+CAST (@EventID AS nvarchar)
	             +';@Amount='+CAST (@Amount AS nvarchar)  	           
  	           
    SET @output = 'StatusCode='+CAST (@status_code AS nvarchar)
                 +';StatusText='+@status_text
     	
	  EXECUTE dbo.zsp_Audit 'zsp_SendEvent'
                        , @AccountID
                        , @VendorId 
                        , @SerialNumber
                        , @MachineNumber
                        , NULL
                        , @status_code
                        , NULL
                        , 0
                        , @input
                        , @output
                      
  COMMIT TRAN
	-- End Audit
	 
	SELECT @status_code AS StatusCode, @status_text AS StatusText

	
END -- zsp_SendEvent

GO   


/**** zsp_AccountStatus *****/
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[zsp_AccountStatus]') AND type in (N'P', N'PC'))
BEGIN
  SELECT 'Procedure zsp_AccountStatus not found!';
  
  raiserror('Not updated', 20, -1) with log;
END
GO

ALTER PROCEDURE [dbo].[zsp_AccountStatus]
	@AccountID 	     varchar(24),
	@VendorId 	     varchar(16),
	@MachineNumber   int
AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
  DECLARE @status_code           int
	DECLARE @status_text           varchar (254)
	DECLARE @terminal_id           bigint
	DECLARE @account_id            bigint
	DECLARE @balance               money
	DECLARE @is_promotion          int
	DECLARE @promo_balance         money
	DECLARE @play_session_id       bigint
  DECLARE @ignore_machine_number int
	
  SET @status_code        = 0
	SET @status_text        = 'Available Account'
	SET @balance            = 0
	
	SELECT @ignore_machine_number = CAST(GP_KEY_VALUE AS int)
    FROM GENERAL_PARAMS 
   WHERE GP_GROUP_KEY = 'Interface3GS' AND GP_SUBJECT_KEY ='IgnoreMachineNumber'
	
	IF ( @ignore_machine_number <> 1 )
	BEGIN
    IF (1 <> (SELECT COUNT(*) 
                FROM TERMINALS_3GS
               WHERE T3GS_MACHINE_NUMBER = @MachineNumber
                 AND T3GS_VENDOR_ID      = @VendorId))
    BEGIN
      SET @status_code = 3
      SET @status_text = 'Invalid Machine Number'
      GOTO ERROR_PROCEDURE
    END
  END
		
	SELECT @account_id = dbo.GetAccountID(@AccountID)
	
	IF (@account_id = 0)
  BEGIN
    SET @status_code = 1
    SET @status_text = 'Invalid Account'
    GOTO ERROR_PROCEDURE
  END

  
  SELECT @play_session_id     = ISNULL(AC_CURRENT_PLAY_SESSION_ID , 0 ) 
       , @is_promotion        = CASE WHEN ( (AC_PROMO_CREATION <= GETDATE()) AND (GETDATE() < AC_PROMO_EXPIRATION) AND (AC_PROMO_BALANCE >0) ) THEN 1 ELSE 0 END
       , @promo_balance       = AC_PROMO_BALANCE 
       , @balance             = AC_BALANCE 
    FROM   ACCOUNTS                   
   WHERE ( AC_ACCOUNT_ID = @account_id )
   
 	-- Check if account has a play session
	IF (@play_session_id <> 0)
	BEGIN
	  SET @status_code = 2
    SET @status_text = 'Account In Session'
    GOTO ERROR_PROCEDURE
	END  
	
	IF (@is_promotion = 1)
  BEGIN
    SET @balance = @promo_balance
  END 
     
	ERROR_PROCEDURE:
	IF (@status_code <> 0)
	  SET @balance = 0
	
	-- Audit MBF 17-JUN-2010
	BEGIN TRAN
	
    DECLARE @input AS nvarchar(MAX)
    DECLARE @output AS nvarchar(MAX)	
  		  	  		  	
	  SET @input = '@AccountID='+@AccountID
	             +';@VendorId='+@VendorId
	             +';@MachineNumber='+CAST (@MachineNumber AS nvarchar)  	           
  	           
    SET @output = 'StatusCode='+CAST (@status_code AS nvarchar)
                 +';StatusText='+@status_text
                 +';AcctBalance='+CAST (@balance AS nvarchar)
                 +';VID=0'
     	
	  EXECUTE dbo.zsp_Audit 'zsp_AccountStatus'
                        , @AccountID
                        , @VendorId 
                        , NULL
                        , @MachineNumber
                        , NULL
                        , @status_code
                        , NULL
                        , 0
                        , @input
                        , @output
                      
  COMMIT TRAN
	-- End Audit
	
	SELECT @status_code AS StatusCode, @status_text AS StatusText, @balance AS Balance, '0' AS VID

END -- zsp_AccountStatus

GO


/**** CheckFinalBalanceMismatch *****/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CheckFinalBalanceMismatch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CheckFinalBalanceMismatch]
GO
CREATE PROCEDURE dbo.CheckFinalBalanceMismatch
 (@SessionId                bigint,
  @ReportedAmountPlayed     money,
  @ReportedAmountWon        money,
  @ReportedFinalBalance     money,
  @CalculatedFinalBalance   money OUTPUT,
  @BigMismatch              bit OUTPUT,
  @BalanceMismatch          bit OUTPUT)
AS
BEGIN
  DECLARE @ps_initial_balance       money
  DECLARE @maximum_balance_error    money
  DECLARE @diff_balance             money

  SELECT   @ps_initial_balance = PS_INITIAL_BALANCE
    FROM   PLAY_SESSIONS
   WHERE   PS_PLAY_SESSION_ID  = @SessionId

  SELECT @maximum_balance_error = CAST(GP_KEY_VALUE AS int)
    FROM GENERAL_PARAMS 
   WHERE GP_GROUP_KEY = 'Interface3GS' AND GP_SUBJECT_KEY ='MaximumBalanceErrorCents'
   
  -- Convert Cents to money
  SET @maximum_balance_error = @maximum_balance_error / 100 
  
  -- Calculate Final Balance
  SET @CalculatedFinalBalance = @ps_initial_balance - @ReportedAmountPlayed + @ReportedAmountWon

  -- Calculate difference  
  SET @diff_balance = ABS(@ReportedFinalBalance - @CalculatedFinalBalance)

  SET @BigMismatch = 0
  IF ( @diff_balance > @maximum_balance_error )
  BEGIN
    SET @BigMismatch = 1
  END
  
  SET @BalanceMismatch = 0
  IF ( @diff_balance > 0 )
  BEGIN
    SET @BalanceMismatch = 1
  END

END -- CheckFinalBalanceMismatch

GO


/**** SetPlaySessionBalanceMismatch *****/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SetPlaySessionBalanceMismatch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SetPlaySessionBalanceMismatch]
GO
CREATE PROCEDURE dbo.SetPlaySessionBalanceMismatch
 (@SessionId                bigint,
  @BalanceMismatch          bit)
AS
BEGIN

  UPDATE PLAY_SESSIONS 
     SET PS_BALANCE_MISMATCH = @BalanceMismatch 
   WHERE PS_PLAY_SESSION_ID = @SessionId

END -- SetPlaySessionBalanceMismatch

GO


/**** FINAL SECTION *****/
SELECT '3GS Update OK.';
