/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 403;

SET @New_ReleaseId = 404;
SET @New_ScriptName = N'UpdateTo_18.404.041.sql';
SET @New_Description = N'Ticket Out Report; Account Parameters'; 

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/**** GENERAL PARAM *****/

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Account.Fields' AND GP_SUBJECT_KEY = 'State.Name')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('Account.Fields','State.Name','')

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Account.Fields' AND GP_SUBJECT_KEY = 'Colony.Name')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('Account.Fields','Colony.Name','')

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Account.Fields' AND GP_SUBJECT_KEY = 'Zip.Name')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('Account.Fields','Zip.Name','')

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Account.Fields' AND GP_SUBJECT_KEY = 'Address.Name')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('Account.Fields','Address.Name','')

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Account.Fields' AND GP_SUBJECT_KEY = 'ExtNumber.Name')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('Account.Fields','ExtNumber.Name','')

/******* TABLES  *******/

/******* RECORDS *******/

/******* PROCEDURES *******/

IF OBJECT_ID (N'dbo.PR_collection_by_machine_and_denomination', N'P') IS NOT NULL
    DROP PROCEDURE dbo.PR_collection_by_machine_and_denomination;                 
GO    

CREATE PROCEDURE [dbo].[PR_collection_by_machine_and_denomination] 
 ( @pFromDt DATETIME   
  ,@pToDt DATETIME    
  ,@pClosingTime INTEGER
  ,@pTerminalWhere NVARCHAR(MAX) 
)
AS
BEGIN

-- DECLARATIONS
 DECLARE @Sql AS VARCHAR(MAX)
 DECLARE @nRows AS INT                              
 DECLARE @index AS INT                              
 DECLARE @tDays AS TABLE(NumDay INT)                       
 DECLARE @p2007Opening AS VARCHAR(MAX)

 SET @p2007Opening  = CAST('2007-01-01T00:00:00' AS DATETIME);
 SET @p2007Opening  = DATEADD(HOUR, @pClosingTime, @p2007Opening)
 
 SET @nRows  = DATEDIFF(DAY, @pFromDt, @pToDt)+1 
 SET @index = 0    

 WHILE @index < @nRows                           
 BEGIN	                                          
   INSERT INTO @tDays VALUES(@index)	                
   SET @index = @index + 1                       
 END        

 SELECT NumDay INTO #TempTable_Days FROM @tDays 
 
 SET @Sql = '
 SELECT   ORDER_DATE 
        , TE_TERMINAL_ID 
        , TE_PROVIDER_ID 
        , ISNULL(TE_MULTI_DENOMINATION, ''--'') AS DENOMINATION 
        , ISNULL(TTL_IN - TTL_OUT, 0) AS COLLECTION 
        , ISNULL(BILL_IN - BILL_OUT, 0) AS BILL 
        , ISNULL(TICKET_IN - TICKET_OUT, 0) AS TICKET 
        , ISNULL(MANUAL, 0) AS MANUAL 
        , ISNULL(CREDIT_CANCEL, 0) AS CREDIT_CANCEL 
        , ISNULL(JACKPOT_DE_SALA, 0) AS JACKPOT_DE_SALA 
        , ISNULL(PROGRESIVES, 0) AS PROGRESIVES 
        , ISNULL(NO_PROGRESIVES, 0) AS NO_PROGRESIVES 
        , ISNULL(PROGRESIVE_PROVISIONS, 0) AS PROGRESIVE_PROVISIONS 
   FROM   TERMINALS 
 LEFT JOIN (SELECT   DATEADD(DAY, NumDay, CAST(''' + CAST(@pFromDt AS VARCHAR(50)) + ''' AS DATETIME)) AS ORDER_DATE FROM #TempTable_Days ) DIA ON ORDER_DATE <= GETDATE() 

 LEFT JOIN (SELECT   PS_TERMINAL_ID 
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), PS_FINISHED)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS PS_FINISHED
                   , SUM(PS_INITIAL_BALANCE+PS_CASH_IN)                                                                   AS BILL_IN 
                   , SUM(ISNULL(PS_FINAL_BALANCE,(0))+PS_CASH_OUT)                                                        AS BILL_OUT 
                   , SUM(ISNULL(PS_RE_TICKET_IN,(0))+ISNULL(PS_PROMO_RE_TICKET_IN,(0))+ISNULL(PS_PROMO_NR_TICKET_IN,(0))) AS TICKET_IN 
                   , SUM(ISNULL(PS_RE_TICKET_OUT,(0))+ISNULL(PS_PROMO_NR_TICKET_OUT,(0)))                                 AS TICKET_OUT 
                   , SUM(PS_TOTAL_CASH_IN)                                                                                AS TTL_IN 
                   , SUM(PS_TOTAL_CASH_OUT)                                                                               AS TTL_OUT 
              FROM   PLAY_SESSIONS   WITH (INDEX(IX_ps_finished_status)) 
             WHERE   PS_STATUS <> 0 AND PS_PROMO = 0 
               AND   PS_FINISHED >= CAST(''' + CAST(@pFromDt AS VARCHAR(50)) + ''' AS DATETIME) 
               AND   PS_FINISHED  < CAST(''' + CAST(@pToDt AS VARCHAR(50)) + ''' AS DATETIME) 
          GROUP BY   PS_TERMINAL_ID 
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), PS_FINISHED)/24, CAST(''' + @p2007Opening + ''' AS DATETIME))
                   ) A ON PS_TERMINAL_ID = TE_TERMINAL_ID AND PS_FINISHED = DIA.ORDER_DATE 
 LEFT JOIN (SELECT   (CASE WHEN HP_TYPE = 20 THEN HP_SITE_JACKPOT_AWARDED_ON_TERMINAL_ID ELSE HP_TERMINAL_ID END) AS HP_TERMINAL_ID  
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), HP_DATETIME)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS HP_DATETIME
                   
                   , SUM(CASE WHEN (HP_TYPE IN (10, 1010)) THEN ISNULL(HP_AMOUNT, 0) ELSE 0 END) AS MANUAL 
                   
                   , SUM(CASE WHEN (HP_TYPE IN ( 0, 1000 )) THEN ISNULL(HP_AMOUNT, 0) ELSE 0 END) +
                     SUM(CASE WHEN (HP_TYPE IN ( 1, 1001 ) AND (HP_LEVEL>=1 AND HP_LEVEL<=32) ) THEN 
                         CASE WHEN (HP_TAX_BASE_AMOUNT IS NULL) THEN 0 ELSE ISNULL(HP_AMOUNT, 0) - ISNULL(HP_TAX_BASE_AMOUNT, 0) END ELSE 0 END) AS CREDIT_CANCEL
                         
                   , SUM(CASE WHEN (HP_TYPE IN ( 20)) THEN ISNULL(HP_AMOUNT, 0) ELSE 0 END) AS JACKPOT_DE_SALA 
                   
                   , SUM(CASE WHEN (HP_TYPE IN ( 1, 1001 ) AND (HP_LEVEL>=1 AND HP_LEVEL<=32) AND HP_PROGRESSIVE_ID IS NOT NULL) THEN 
                         CASE WHEN (HP_TAX_BASE_AMOUNT IS NULL) THEN ISNULL(HP_AMOUNT, 0) ELSE ISNULL(HP_TAX_BASE_AMOUNT, 0) END ELSE 0 END) AS PROGRESIVES 
                         
                   , SUM(CASE WHEN (HP_TYPE IN ( 1, 1001 ) AND (HP_LEVEL <1 OR  HP_LEVEL >32 OR HP_LEVEL IS NULL)) THEN ISNULL(HP_AMOUNT, 0) ELSE 0 END) + 
                     SUM(CASE WHEN (HP_TYPE IN ( 1, 1001 ) AND (HP_LEVEL>=1 AND HP_LEVEL <=32 AND HP_PROGRESSIVE_ID IS NULL)) THEN 
                         CASE WHEN (HP_TAX_BASE_AMOUNT IS NULL) THEN ISNULL(HP_AMOUNT, 0) ELSE ISNULL(HP_TAX_BASE_AMOUNT, 0) END ELSE 0 END) AS NO_PROGRESIVES
                   
              FROM   HANDPAYS 
             WHERE   HP_DATETIME >= CAST(''' + CAST(@pFromDt AS VARCHAR(50)) + ''' AS DATETIME) 
               AND   HP_DATETIME < CAST(''' + CAST(@pToDt AS VARCHAR(50)) + ''' AS DATETIME) 
             GROUP   BY (CASE WHEN HP_TYPE = 20 THEN HP_SITE_JACKPOT_AWARDED_ON_TERMINAL_ID ELSE HP_TERMINAL_ID END) 
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), HP_DATETIME)/24, CAST(''' + @p2007Opening + ''' AS DATETIME))
                   ) B ON HP_TERMINAL_ID = TE_TERMINAL_ID AND HP_DATETIME = DIA.ORDER_DATE 
 LEFT JOIN (SELECT   PPT_TERMINAL_ID 
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), PGP_HOUR_FROM)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS PGP_HOUR_FROM
                   , SUM(ISNULL(PPT_AMOUNT, 0)) AS PROGRESIVE_PROVISIONS 
             FROM    PROGRESSIVES_PROVISIONS_TERMINALS 
        LEFT JOIN    PROGRESSIVES_PROVISIONS ON PGP_PROVISION_ID = PPT_PROVISION_ID 
                                            AND PGP_PROGRESSIVE_ID = PPT_PROGRESSIVE_ID 
                                            AND PGP_HOUR_FROM >= CAST(''' + CAST(@pFromDt AS VARCHAR(50)) + ''' AS DATETIME) 
                                            AND PGP_HOUR_FROM < CAST(''' + CAST(@pToDt AS VARCHAR(50)) + ''' AS DATETIME) 
                                            AND PGP_STATUS = 0
             GROUP   BY PPT_TERMINAL_ID 
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), PGP_HOUR_FROM)/24, CAST(''' + @p2007Opening + ''' AS DATETIME))
                   ) C ON PPT_TERMINAL_ID = TE_TERMINAL_ID AND PGP_HOUR_FROM = DIA.ORDER_DATE 
  WHERE ( BILL_IN IS NOT NULL 
     OR   BILL_OUT IS NOT NULL 
     OR   TICKET_IN IS NOT NULL 
     OR   TICKET_OUT IS NOT NULL 
     OR   TTL_IN IS NOT NULL 
     OR   TTL_OUT IS NOT NULL 
     OR   MANUAL IS NOT NULL 
     OR   CREDIT_CANCEL IS NOT NULL 
     OR   JACKPOT_DE_SALA IS NOT NULL 
     OR   PROGRESIVES IS NOT NULL 
     OR   NO_PROGRESIVES IS NOT NULL 
     OR   PROGRESIVE_PROVISIONS IS NOT NULL ) ' + 
     CAST(@pTerminalWhere AS Varchar(max)) +
' ORDER   BY ISNULL(TE_MULTI_DENOMINATION, ''--''), TE_PROVIDER_ID, ORDER_DATE, TE_NAME '

EXECUTE (@Sql)

DROP TABLE #TempTable_Days

END -- END PROCEDURE
GO

-- PERMISSIONS

GRANT EXECUTE ON [dbo].[PR_collection_by_machine_and_denomination] TO [wggui] WITH GRANT OPTION
GO


--------------------------------------------------------------------------------
--   DESCRIPTION: Collection by machine and date
-- 
--        AUTHOR: José Martínez
-- 
-- CREATION DATE: 09-SEP-2014
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 09-FEB-2016 JML    First release.
-- 09-FEB-2016 JML    GetDualCurrencyAmount
--------------------------------------------------------------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetDualCurrencyAmount]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[GetDualCurrencyAmount]
GO

CREATE FUNCTION [dbo].[GetDualCurrencyAmount]
(@pAmount  MONEY,
 @pAmt0    MONEY,
 @pPreferAmt INT)
RETURNS MONEY
AS
BEGIN

  DECLARE @Amount MONEY

  SET @Amount = @pAmount;
      
  IF (@pAmt0 IS NOT NULL AND @pPreferAmt = 1)
  BEGIN
     SET @Amount = @pAmt0;
  END
     
  RETURN @Amount
  
END -- GetDualCurrencyAmount
GO

-- PERMISSIONS
GRANT EXECUTE ON [dbo].[GetDualCurrencyAmount] TO [wggui] WITH GRANT OPTION
GO




--------------------------------------------------------------------------------
--   DESCRIPTION: Collection by machine and date
-- 
--        AUTHOR: José Martínez
-- 
-- CREATION DATE: 09-SEP-2014
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 09-SEP-2014 JML    First release.
-- 09-SEP-2014 JML    PR_collection_by_machine_and_date
-- 26-JAN-2015 FJC    Fixed Bug WIG-1966: change filter dates for provisions.
-- 29-JAN-2015 FJC    Fixed Bug WIG-1981 
-- 02-FEB-2015 FJC    Fixed bug: WIG-1994 - Annulled provisions are showed in the report.
-- 03-FEB-2015 FJC    Fixed bug: WIG-1998 & 2004 - Changes in handpays placement 
--                      Column Manual:                   Others                     (MANUAL & MANUAL_OTHERS)
--                      Column Cancelled Credits:        Créditos cancelados        
--                      Column No-Progressive Jackpots:  Jackpot + Jackpot Top Win  
--                      Column Progressive Jackpots:     Progressive Jackpots
-- 09-FEB-2015 JML & FJC Fixed bug: WIG-2012: Site Jackpot is not displayed in some cases
-- 10-FEB-2015 FJC    WIG-2003. 
-- 09-MAR-2015 FJC    Fixed bug: WIG-2144
-- 30-MAR-2015 FJC    BacklogItem 542: BV-75: Reporte JCJ – Cambio en Net TITO
-- 28-MAY-2015 FAV    BacklogItem 1722: @pShowMetters and @pOnlyGroupByTerminal parameters added. 
--                    For the 'Netwin control report' form.
-- 14-JUL-2015 FAV    Fixed Bug WIG-2516 and 2536, check and rebuild 'PR_collection_by_machine_and_date' stored procedure
-- 21-SEP-2015 JML    Add cash-in coins
-- 26-JUN-2017 RGR    Fixed Bug 28489: WIGOS-3112 Ticket in status Pending to Print are considered in Ticket Out Report
-------------------------------------------------------------------------------- 


IF OBJECT_ID (N'dbo.PR_collection_by_machine_and_date', N'P') IS NOT NULL
    DROP PROCEDURE dbo.PR_collection_by_machine_and_date;                 
GO    

CREATE PROCEDURE [dbo].[PR_collection_by_machine_and_date] 
 ( @pFromDt DATETIME   
  ,@pToDt DATETIME    
  ,@pClosingTime INTEGER
  ,@pMaskStatus INTEGER
  ,@pStatusPaid INTEGER
  ,@pAll		INTEGER
  ,@pTerminalWhere NVARCHAR(MAX) 
  ,@pBillDetail BIT
  ,@pShowMetters BIT
  ,@pOnlyGroupByTerminal BIT
  ,@pIncludeJackpotRoom BIT
)
AS
BEGIN

  -- DECLARATIONS
   DECLARE @Sql AS VARCHAR(MAX)
   DECLARE @nRows AS INT                              
   DECLARE @index AS INT                              
   DECLARE @tDays AS TABLE(NumDay INT)                       
   DECLARE @Columns AS VARCHAR(MAX)
   DECLARE @ColumnChk AS VARCHAR(MAX)
   DECLARE @p2007Opening AS VARCHAR(MAX)

   SET @p2007Opening  = CAST('2007-01-01T00:00:00' AS DATETIME);
   SET @p2007Opening  = DATEADD(HOUR, @pClosingTime, @p2007Opening)

   SET @nRows  = DATEDIFF(DAY, @pFromDt, @pToDt)+1 
   SET @index = 0                                  

   WHILE @index < @nRows                           
   BEGIN	                                          
     INSERT INTO @tDays VALUES(@index)	                
     SET @index = @index + 1                       
   END 
   
    SET @Columns = ''
    SET @ColumnChk = ''

   SELECT NumDay INTO #TempTable_Days FROM @tDays 
   
   SELECT @Columns = COALESCE(@Columns + '[' + CAST(CGC_DENOMINATION AS NVARCHAR(20)) + '],', '')
     FROM (SELECT   DISTINCT CGC_DENOMINATION 
             FROM   CAGE_CURRENCIES 
            WHERE   CGC_ISO_CODE IN (SELECT   GP_KEY_VALUE 
                                       FROM   GENERAL_PARAMS 
                                      WHERE   GP_GROUP_KEY = 'RegionalOptions' 
                                        AND   GP_SUBJECT_KEY = 'CurrencyISOCode')
              AND   CGC_DENOMINATION >= 1 
              AND   CGC_ALLOWED = 1
                    UNION  
           SELECT   DISTINCT MCD_FACE_VALUE 
             FROM   MONEY_COLLECTION_DETAILS 
            INNER   JOIN MONEY_COLLECTIONS ON MCD_COLLECTION_ID = MC_COLLECTION_ID
            WHERE   MC_COLLECTION_DATETIME >= @pFromDt
              AND   MC_COLLECTION_DATETIME <  @pToDt
              AND   MC_TERMINAL_ID IS NOT NULL 
              AND   MCD_FACE_VALUE >= 1
          ) AS DTM
  ORDER BY CGC_DENOMINATION

  SET @ColumnChk = REPLACE( @Columns, ',', ' IS NOT NULL OR ')

  SET @Columns = LEFT(@Columns,LEN(@Columns)-1)
  SET @ColumnChk = LEFT(@ColumnChk,LEN(@ColumnChk)-3)
     
  SET @Sql = '
   SELECT   TE_PROVIDER_ID 
          , TE_TERMINAL_ID
          , TE_NAME 
          , ISNULL(TE_MULTI_DENOMINATION, ''--'') AS DENOMINATION '
  + CASE WHEN @pOnlyGroupByTerminal = 0 THEN '         
          , ORDER_DATE 
          , DATEADD(DAY, 1, ORDER_DATE) AS ORDER_DATE_FIN
          , COLLECTED_COIN_AMOUNT
          , TICKET_IN_COUNT
          , TI_IN_AMOUNT_RE
          , TI_IN_AMOUNT_NO_RE
          , (ISNULL(TI_IN_AMOUNT_RE,0) + ISNULL(TI_IN_AMOUNT_NO_RE, 0)) AS TI_IN_AMOUNT
          , TICKET_OUT_COUNT
          , TI_OUT_AMOUNT_RE
          , TI_OUT_AMOUNT_NO_RE
          , (ISNULL(TI_OUT_AMOUNT_RE, 0) + ISNULL(TI_OUT_AMOUNT_NO_RE, 0)) AS TI_OUT_AMOUNT
          , ( (ISNULL(TI_OUT_AMOUNT_RE, 0) + ISNULL(TI_OUT_AMOUNT_NO_RE, 0)) - (ISNULL(TI_IN_AMOUNT_RE, 0) + ISNULL(TI_IN_AMOUNT_NO_RE, 0)) ) AS NET_TITO	
          , HAND_PAYS.MANUAL
          , HAND_PAYS.CREDIT_CANCEL
          , HAND_PAYS.JACKPOT_DE_SALA
          , HAND_PAYS.PROGRESIVES
          , HAND_PAYS.NO_PROGRESIVES
          , (ISNULL(HAND_PAYS.MANUAL, 0) + ISNULL(HAND_PAYS.CREDIT_CANCEL, 0) + ISNULL(HAND_PAYS.JACKPOT_DE_SALA, 0) + ISNULL(HAND_PAYS.NO_PROGRESIVES, 0) + ISNULL(PROVISIONS.PROGRESIVE_PROVISIONS, 0)) AS HAND_PAYS_TOTAL
          , PROVISIONS.PROGRESIVE_PROVISIONS
          , (ISNULL(TI_OUT_AMOUNT_RE, 0) - ISNULL(TI_IN_AMOUNT_RE, 0) +
             ISNULL(HAND_PAYS.MANUAL, 0) + ISNULL(HAND_PAYS.CREDIT_CANCEL, 0) + ISNULL(HAND_PAYS.JACKPOT_DE_SALA, 0) + ISNULL(HAND_PAYS.NO_PROGRESIVES, 0) + ISNULL(PROVISIONS.PROGRESIVE_PROVISIONS, 0)) 
              AS TOTAL_WIN_WITHOUT_BILLS
          , 0 AS GAP_BILL
          ' + CASE WHEN @pShowMetters = 1 THEN ', METERS.TOTAL_COIN_IN_CREDITS' ELSE ', 0  AS GAP_METER_0' END + '
          ' + CASE WHEN @pShowMetters = 1 THEN ', METERS.TOTAL_COIN_OUT_CREDITS' ELSE ', 0 AS GAP_METER_1' END + '
          ' + CASE WHEN @pShowMetters = 1 THEN ', METERS.TOTAL_JACKPOT_CREDITS' ELSE ', 0 AS GAP_METER_2' END + '
          ' + CASE WHEN @pShowMetters = 1 THEN ', METERS.TOTAL_HAND_PAID_CANCELLED_CREDITS' ELSE ', 0 AS GAP_METER_3' END + '
          , BILLS.* '
     ELSE '
		  , NULL AS GAP_ORDER_DATE 
          , NULL AS GAP_ORDER_DATE_FIN
          , SUM(COLLECTED_COIN_AMOUNT) AS COLLECTED_COIN_AMOUNT
          , SUM(TICKET_IN_COUNT) AS TICKET_IN_COUNT
          , SUM(TI_IN_AMOUNT_RE) AS TI_IN_AMOUNT_RE
          , SUM(TI_IN_AMOUNT_NO_RE) AS TI_IN_AMOUNT_NO_RE
          , SUM((ISNULL(TI_IN_AMOUNT_RE,0) + ISNULL(TI_IN_AMOUNT_NO_RE, 0))) AS TI_IN_AMOUNT
          , SUM(TICKET_OUT_COUNT) AS TICKET_OUT_COUNT
          , SUM(TI_OUT_AMOUNT_RE) AS TI_OUT_AMOUNT_RE
          , SUM(TI_OUT_AMOUNT_NO_RE) AS TI_OUT_AMOUNT_NO_RE
          , SUM((ISNULL(TI_OUT_AMOUNT_RE, 0) + ISNULL(TI_OUT_AMOUNT_NO_RE, 0))) AS TI_OUT_AMOUNT
          , SUM(( (ISNULL(TI_OUT_AMOUNT_RE, 0) + ISNULL(TI_OUT_AMOUNT_NO_RE, 0)) - (ISNULL(TI_IN_AMOUNT_RE, 0) + ISNULL(TI_IN_AMOUNT_NO_RE, 0))) ) AS NET_TITO	
          , SUM(HAND_PAYS.MANUAL) AS MANUAL
          , SUM(HAND_PAYS.CREDIT_CANCEL) AS CREDIT_CANCEL
          , SUM(HAND_PAYS.JACKPOT_DE_SALA) AS JACKPOT_DE_SALA
          , SUM(HAND_PAYS.PROGRESIVES) AS PROGRESIVES
          , SUM(HAND_PAYS.NO_PROGRESIVES) AS NO_PROGRESIVES
          , SUM((ISNULL(HAND_PAYS.MANUAL, 0) + ISNULL(HAND_PAYS.CREDIT_CANCEL, 0) + ISNULL(HAND_PAYS.JACKPOT_DE_SALA, 0) + ISNULL(HAND_PAYS.NO_PROGRESIVES, 0) + ISNULL(PROVISIONS.PROGRESIVE_PROVISIONS, 0))) AS HAND_PAYS_TOTAL
          , SUM(PROVISIONS.PROGRESIVE_PROVISIONS) AS PROGRESIVE_PROVISIONS
          , SUM((ISNULL(TI_OUT_AMOUNT_RE, 0) - ISNULL(TI_IN_AMOUNT_RE, 0) +
             ISNULL(HAND_PAYS.MANUAL, 0) + ISNULL(HAND_PAYS.CREDIT_CANCEL, 0) + ISNULL(HAND_PAYS.JACKPOT_DE_SALA, 0) + ISNULL(HAND_PAYS.NO_PROGRESIVES, 0) + ISNULL(PROVISIONS.PROGRESIVE_PROVISIONS, 0))) 
              AS TOTAL_WIN_WITHOUT_BILLS
          , 0 AS GAP_BILL
          ' + CASE WHEN @pShowMetters = 1 THEN ', SUM(METERS.TOTAL_COIN_IN_CREDITS) AS TOTAL_COIN_IN_CREDITS' ELSE ', 0  AS GAP_METER_0' END + '
          ' + CASE WHEN @pShowMetters = 1 THEN ', SUM(METERS.TOTAL_COIN_OUT_CREDITS) AS TOTAL_COIN_OUT_CREDITS' ELSE ', 0 AS GAP_METER_1' END + '
          ' + CASE WHEN @pShowMetters = 1 THEN ', SUM(METERS.TOTAL_JACKPOT_CREDITS) AS TOTAL_JACKPOT_CREDITS' ELSE ', 0 AS GAP_METER_2' END + '
          ' + CASE WHEN @pShowMetters = 1 THEN ', SUM(METERS.TOTAL_HAND_PAID_CANCELLED_CREDITS) AS TOTAL_HAND_PAID_CANCELLED_CREDITS' ELSE ', 0 AS GAP_METER_3' END + '
          , NULL AS GAP_BILL_MC_TERMINAL_ID
          , NULL AS GAP_BILL_MC_COLLECTION_DATETIME
          , SUM(BILLS.MCD_FACE_VALUE)
          , SUM(BILLS.COLLECTED) '
     END + '
     FROM   TERMINALS 
 LEFT JOIN (SELECT   DATEADD(DAY, NumDay, CAST(''' + CAST(@pFromDt AS VARCHAR(50)) + ''' AS DATETIME)) AS ORDER_DATE FROM #TempTable_Days ) DIA ON ORDER_DATE <= GETDATE() 
 LEFT JOIN (SELECT   TC_TERMINAL_ID 
                   , DATEADD(HOUR, ' + CAST(@pClosingTime AS VARCHAR(50)) + ', TC_DATE) AS TC_DATE
              FROM   TERMINALS_CONNECTED 
             WHERE   TC_DATE >= dateadd(day, datediff(day, 0, CAST(''' + CAST(@pFromDt AS VARCHAR(50)) + ''' AS DATETIME)), 0)
               AND   TC_DATE <  dateadd(day, datediff(day, 0, CAST(''' + CAST(@pToDt AS VARCHAR(50)) + ''' AS DATETIME)), 0)
               AND   TC_STATUS = 0
               AND   TC_CONNECTED = 1) TC ON TC_TERMINAL_ID = TE_TERMINAL_ID AND TC_DATE = DIA.ORDER_DATE 
 LEFT JOIN (SELECT   MC_TERMINAL_ID AS MC_TERMINAL_ID_C
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), CGS_WORKING_DAY)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS MC_COLLECTION_DATETIME_C
                   , SUM(MC_COLLECTED_COIN_AMOUNT) AS COLLECTED_COIN_AMOUNT
              FROM   MONEY_COLLECTIONS
        INNER JOIN   CAGE_MOVEMENTS ON CGM_MC_COLLECTION_ID = MC_COLLECTION_ID
        INNER JOIN   CAGE_SESSIONS ON CGS_CAGE_SESSION_ID = CGM_CAGE_SESSION_ID
             WHERE   CGS_WORKING_DAY >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
               AND   CGS_WORKING_DAY <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
               AND   MC_TERMINAL_ID IS NOT NULL
             GROUP   BY MC_TERMINAL_ID
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), CGS_WORKING_DAY)/24, CAST(''' + @p2007Opening + ''' AS DATETIME))
           ) CASH_IN_COINS ON TE_TERMINAL_ID = MC_TERMINAL_ID_C AND MC_COLLECTION_DATETIME_C = DIA.ORDER_DATE 
 LEFT JOIN (SELECT   MC_TERMINAL_ID AS MC_TERMINAL_ID_T
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), CGS_WORKING_DAY)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS MC_COLLECTION_DATETIME_T
                   , SUM(MC_EXPECTED_TICKET_COUNT) AS TICKET_IN_COUNT
                   , SUM(ISNULL(MC_EXPECTED_RE_TICKET_AMOUNT, 0)+ISNULL(MC_EXPECTED_PROMO_RE_TICKET_AMOUNT, 0)) AS TI_IN_AMOUNT_RE
                   , SUM(ISNULL(MC_EXPECTED_PROMO_NR_TICKET_AMOUNT, 0)) AS TI_IN_AMOUNT_NO_RE
              FROM   MONEY_COLLECTIONS
        INNER JOIN   CAGE_MOVEMENTS ON CGM_MC_COLLECTION_ID = MC_COLLECTION_ID
        INNER JOIN   CAGE_SESSIONS ON CGS_CAGE_SESSION_ID = CGM_CAGE_SESSION_ID
             WHERE   CGS_WORKING_DAY >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
               AND   CGS_WORKING_DAY <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
               AND   MC_TERMINAL_ID IS NOT NULL
             GROUP   BY MC_TERMINAL_ID
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), CGS_WORKING_DAY)/24, CAST(''' + @p2007Opening + ''' AS DATETIME))
           ) TICKETS_IN ON TE_TERMINAL_ID = MC_TERMINAL_ID_T AND MC_COLLECTION_DATETIME_T = DIA.ORDER_DATE 
LEFT JOIN ( SELECT   TI_CREATED_TERMINAL_ID
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), TI_CREATED_DATETIME)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS TI_CREATED_DATETIME
                   , COUNT(1) AS TICKET_OUT_COUNT
                   , SUM(CASE WHEN TI_TYPE_ID = 2 AND TI_STATUS NOT IN (6) THEN 0 ELSE TI_AMOUNT END) AS TI_OUT_AMOUNT_RE
                   , SUM(CASE WHEN TI_TYPE_ID = 2 THEN TI_AMOUNT ELSE 0 END) AS TI_OUT_AMOUNT_NO_RE
              FROM   TICKETS 
             WHERE   TI_CREATED_DATETIME >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
               AND   TI_CREATED_DATETIME <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
               AND   TI_CREATED_TERMINAL_TYPE = 1     -- TITO_TERMINAL_TYPE.TERMINAL = 1              
               AND   TI_TYPE_ID IN (0,1,2,5)    --TITO_TICKET_TYPE
                                                --CASHABLE = 0,
                                                --PROMO_REDEEM = 1,
                                                --PROMO_NONREDEEM = 2,  // only playable
                                                --HANDPAY = 3,
                                                --JACKPOT = 4,
                                                --OFFLINE = 5
             GROUP   BY TI_CREATED_TERMINAL_ID
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), TI_CREATED_DATETIME)/24, CAST(''' + @p2007Opening + ''' AS DATETIME))
          ) TICKETS_OUT ON TE_TERMINAL_ID = TI_CREATED_TERMINAL_ID AND TI_CREATED_DATETIME = DIA.ORDER_DATE
 LEFT JOIN (SELECT   (CASE WHEN HP_TYPE = 20 THEN HP_SITE_JACKPOT_AWARDED_ON_TERMINAL_ID ELSE HP_TERMINAL_ID END) AS HP_TERMINAL_ID 
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), ISNULL(HP_STATUS_CHANGED, CASE WHEN (HP_TYPE IN ( 20)) THEN HP_DATETIME ELSE NULL END))/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS HP_DATETIME
                   
                   , SUM(CASE WHEN (HP_TYPE IN (10, 1010)) THEN ISNULL(dbo.GetDualCurrencyAmount(HP_AMOUNT, HP_AMT0, ' + CAST(@pShowMetters AS varchar(1) ) + '), 0) ELSE 0 END) AS MANUAL 
                   
                   , SUM(CASE WHEN (HP_TYPE IN ( 0, 1000 )) THEN ISNULL(dbo.GetDualCurrencyAmount(HP_AMOUNT, HP_AMT0, ' + CAST(@pShowMetters AS varchar(1) ) + '), 0) ELSE 0 END) +
                     SUM(CASE WHEN (HP_TYPE IN ( 1, 1001 ) AND (HP_LEVEL>=1 AND HP_LEVEL<=32) ) THEN 
                         CASE WHEN (HP_TAX_BASE_AMOUNT IS NULL) THEN 0 ELSE ISNULL(dbo.GetDualCurrencyAmount(HP_AMOUNT, HP_AMT0, ' + CAST(@pShowMetters AS varchar(1) ) + '), 0) - ISNULL(HP_TAX_BASE_AMOUNT, 0) END ELSE 0 END) AS CREDIT_CANCEL
'                          
  IF @pIncludeJackpotRoom = 1 
  BEGIN
    SET @Sql =  @Sql + ', SUM(CASE WHEN (HP_TYPE IN ( 20)) THEN ISNULL(dbo.GetDualCurrencyAmount(HP_AMOUNT, HP_AMT0, ' + CAST(@pShowMetters AS varchar(1) ) + '), 0) ELSE 0 END) AS JACKPOT_DE_SALA  '
  END
  ELSE
  BEGIN
    SET @Sql =  @Sql + ', 0 AS JACKPOT_DE_SALA '
  END
  SET @Sql =  @Sql +
  '                 , SUM(CASE WHEN (HP_TYPE IN ( 1, 1001 ) AND (HP_LEVEL>=1 AND HP_LEVEL<=32) AND HP_PROGRESSIVE_ID IS NOT NULL) THEN 
                         CASE WHEN (HP_TAX_BASE_AMOUNT IS NULL) THEN ISNULL(dbo.GetDualCurrencyAmount(HP_AMOUNT, HP_AMT0, ' + CAST(@pShowMetters AS varchar(1) ) + '), 0) ELSE ISNULL(HP_TAX_BASE_AMOUNT, 0) END ELSE 0 END) AS PROGRESIVES 
                         
                   , SUM(CASE WHEN (HP_TYPE IN ( 1, 1001 ) AND (HP_LEVEL <1 OR  HP_LEVEL >32 OR HP_LEVEL IS NULL)) THEN ISNULL(dbo.GetDualCurrencyAmount(HP_AMOUNT, HP_AMT0, ' + CAST(@pShowMetters AS varchar(1) ) + '), 0) ELSE 0 END) + 
                     SUM(CASE WHEN (HP_TYPE IN ( 1, 1001 ) AND (HP_LEVEL>=1 AND HP_LEVEL <=32 AND HP_PROGRESSIVE_ID IS NULL)) THEN 
                         CASE WHEN (HP_TAX_BASE_AMOUNT IS NULL) THEN ISNULL(dbo.GetDualCurrencyAmount(HP_AMOUNT, HP_AMT0, ' + CAST(@pShowMetters AS varchar(1) ) + '), 0) ELSE ISNULL(HP_TAX_BASE_AMOUNT, 0) END ELSE 0 END) AS NO_PROGRESIVES
                   
              FROM   HANDPAYS WITH (INDEX (IX_hp_status_changed))
             WHERE   ' + CASE WHEN @pAll = 0 THEN 'HP_STATUS & ' + CAST(@pMaskStatus AS VARCHAR(10)) + ' = ' + CAST(@pStatusPaid AS VARCHAR(10)) + ' AND ' ELSE + ' ' END +
            '  ISNULL(HP_STATUS_CHANGED, CASE WHEN (HP_TYPE IN ( 20)) THEN HP_DATETIME ELSE NULL END) >= CAST(''' + CAST(@pFromDt AS VARCHAR(50)) + ''' AS DATETIME) 
               AND   ISNULL(HP_STATUS_CHANGED, CASE WHEN (HP_TYPE IN ( 20)) THEN HP_DATETIME ELSE NULL END) < CAST(''' + CAST(@pToDt AS VARCHAR(50)) + ''' AS DATETIME) 
             GROUP   BY (CASE WHEN HP_TYPE = 20 THEN HP_SITE_JACKPOT_AWARDED_ON_TERMINAL_ID ELSE HP_TERMINAL_ID END) 
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), ISNULL(HP_STATUS_CHANGED, CASE WHEN (HP_TYPE IN ( 20)) THEN HP_DATETIME ELSE NULL END))/24, CAST(''' + @p2007Opening + ''' AS DATETIME))
                   ) HAND_PAYS ON HP_TERMINAL_ID = TE_TERMINAL_ID AND HP_DATETIME = DIA.ORDER_DATE
 LEFT JOIN (SELECT   PPT_TERMINAL_ID 
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), PGP_HOUR_FROM)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS PGP_HOUR_FROM
                   , SUM(ISNULL(PPT_AMOUNT, 0)) AS PROGRESIVE_PROVISIONS 
             FROM    PROGRESSIVES_PROVISIONS_TERMINALS 
        LEFT JOIN    PROGRESSIVES_PROVISIONS ON PGP_PROVISION_ID = PPT_PROVISION_ID 
                                            AND PGP_PROGRESSIVE_ID = PPT_PROGRESSIVE_ID 
                                            AND PGP_HOUR_FROM >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
                                            AND PGP_HOUR_FROM <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
                                            AND PGP_STATUS = 0
             GROUP   BY PPT_TERMINAL_ID 
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), PGP_HOUR_FROM)/24, CAST(''' + @p2007Opening + ''' AS DATETIME))
                   ) PROVISIONS ON PPT_TERMINAL_ID = TE_TERMINAL_ID AND PGP_HOUR_FROM = DIA.ORDER_DATE ' 
IF @pBillDetail = 1
BEGIN
  SET @Sql =  @Sql +
'LEFT JOIN (SELECT   *  
              FROM ( SELECT   MC_TERMINAL_ID
                            , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), CGS_WORKING_DAY)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS MC_COLLECTION_DATETIME
                            , MCD_FACE_VALUE
                            , (MCD_NUM_COLLECTED * MCD_FACE_VALUE) AS COLLECTED
                       FROM   MONEY_COLLECTION_DETAILS 
                 INNER JOIN   MONEY_COLLECTIONS ON MCD_COLLECTION_ID = MC_COLLECTION_ID
                 INNER JOIN   CAGE_MOVEMENTS ON CGM_MC_COLLECTION_ID = MC_COLLECTION_ID
                 INNER JOIN   CAGE_SESSIONS ON CGS_CAGE_SESSION_ID = CGM_CAGE_SESSION_ID
                      WHERE   CGS_WORKING_DAY >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
                        AND   CGS_WORKING_DAY <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
                        AND   MC_TERMINAL_ID IS NOT NULL 
                        AND   MCD_CAGE_CURRENCY_TYPE = 0
                        AND   MCD_FACE_VALUE > 0
                   ) PIV
             PIVOT (  SUM(COLLECTED) FOR MCD_FACE_VALUE IN ('+ @Columns  + ')) AS CHILD
            ) BILLS ON TE_TERMINAL_ID = MC_TERMINAL_ID AND MC_COLLECTION_DATETIME = DIA.ORDER_DATE '
END
ELSE
BEGIN
  SET @Sql =  @Sql +
'LEFT JOIN (SELECT   MC_TERMINAL_ID
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), CGS_WORKING_DAY)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS MC_COLLECTION_DATETIME
                   , 0 AS MCD_FACE_VALUE
                   , SUM(MCD_NUM_COLLECTED * MCD_FACE_VALUE) AS COLLECTED
              FROM   MONEY_COLLECTION_DETAILS 
        INNER JOIN   MONEY_COLLECTIONS ON MCD_COLLECTION_ID = MC_COLLECTION_ID
        INNER JOIN   CAGE_MOVEMENTS ON CGM_MC_COLLECTION_ID = MC_COLLECTION_ID
        INNER JOIN   CAGE_SESSIONS ON CGS_CAGE_SESSION_ID = CGM_CAGE_SESSION_ID
             WHERE   CGS_WORKING_DAY >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
               AND   CGS_WORKING_DAY <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
               AND   MC_TERMINAL_ID IS NOT NULL
               AND   MCD_CAGE_CURRENCY_TYPE = 0
               AND   MCD_FACE_VALUE > 0
             GROUP   BY MC_TERMINAL_ID
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), CGS_WORKING_DAY)/24, CAST(''' + @p2007Opening + ''' AS DATETIME))
          ) BILLS ON TE_TERMINAL_ID = MC_TERMINAL_ID AND MC_COLLECTION_DATETIME = DIA.ORDER_DATE '
END


IF @pShowMetters = 1
BEGIN
SET @Sql =  @Sql +
'LEFT JOIN (SELECT TSMH_TERMINAL_ID
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), TSMH_DATETIME)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS TSMH_DATE
                   , SUM(CASE WHEN TSMH_METER_CODE = 0 THEN ISNULL(CAST(TSMH_METER_INCREMENT AS MONEY)/100, 0) END) AS TOTAL_COIN_IN_CREDITS
                   , SUM(CASE WHEN TSMH_METER_CODE = 1 THEN ISNULL(CAST(TSMH_METER_INCREMENT AS MONEY)/100, 0) END) AS TOTAL_COIN_OUT_CREDITS
                   , SUM(CASE WHEN TSMH_METER_CODE = 2 THEN ISNULL(CAST(TSMH_METER_INCREMENT AS MONEY)/100, 0) END) AS TOTAL_JACKPOT_CREDITS
                   , SUM(CASE WHEN TSMH_METER_CODE = 3 THEN ISNULL(CAST(TSMH_METER_INCREMENT AS MONEY)/100, 0) END) AS TOTAL_HAND_PAID_CANCELLED_CREDITS
			 FROM    TERMINAL_SAS_METERS_HISTORY
			WHERE    TSMH_TYPE = 1
			  AND    TSMH_METER_CODE IN (0,1,2,3)
			  AND    TSMH_METER_INCREMENT > 0 
			  AND    TSMH_DATETIME >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
              AND    TSMH_DATETIME <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
            GROUP    BY TSMH_TERMINAL_ID
				   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), TSMH_DATETIME)/24, CAST(''' + @p2007Opening + ''' AS DATETIME))
	   ) METERS ON TE_TERMINAL_ID = TSMH_TERMINAL_ID AND TSMH_DATE = DIA.ORDER_DATE '
END

SET @Sql =  @Sql + 'WHERE ( ' 

IF @pBillDetail = 1
BEGIN
  SET @Sql =  @Sql + @ColumnChk 
END
ELSE
BEGIN
  SET @Sql =  @Sql + ' BILLS.COLLECTED IS NOT NULL ' 
END

SET @Sql =  @Sql +
  ' OR TICKET_IN_COUNT IS NOT NULL
    OR TI_IN_AMOUNT_RE IS NOT NULL
    OR TI_IN_AMOUNT_NO_RE IS NOT NULL
    OR TICKET_OUT_COUNT IS NOT NULL
    OR TI_OUT_AMOUNT_RE IS NOT NULL
    OR TI_OUT_AMOUNT_NO_RE IS NOT NULL
    OR HAND_PAYS.MANUAL IS NOT NULL
    OR HAND_PAYS.CREDIT_CANCEL IS NOT NULL
    OR HAND_PAYS.JACKPOT_DE_SALA IS NOT NULL
    OR HAND_PAYS.PROGRESIVES IS NOT NULL
    OR HAND_PAYS.NO_PROGRESIVES IS NOT NULL
    OR PROVISIONS.PROGRESIVE_PROVISIONS IS NOT NULL' +
	CASE WHEN @pShowMetters = 1 THEN '
		OR METERS.TOTAL_COIN_IN_CREDITS IS NOT NULL
		OR METERS.TOTAL_COIN_OUT_CREDITS IS NOT NULL
		OR METERS.TOTAL_JACKPOT_CREDITS IS NOT NULL
		OR METERS.TOTAL_HAND_PAID_CANCELLED_CREDITS IS NOT NULL'
    ELSE '' END + '
    OR TC_DATE IS NOT NULL
    )' + CAST(@pTerminalWhere AS Varchar(max)) +
CASE WHEN @pOnlyGroupByTerminal = 1 THEN 
' GROUP   BY TE_PROVIDER_ID 
        , TE_TERMINAL_ID
        , TE_NAME 
        , ISNULL(TE_MULTI_DENOMINATION, ''--'') '
ELSE '' END +
' ORDER   BY TE_PROVIDER_ID 
        , TE_TERMINAL_ID 
        , TE_NAME 
        , ISNULL(TE_MULTI_DENOMINATION, ''--'') ' +
        
CASE WHEN @pOnlyGroupByTerminal = 0 THEN 
        ', ORDER_DATE ' 
ELSE '' END

 -- PRINT @Sql

  EXECUTE (@Sql)

  DROP TABLE #TempTable_Days

END -- END PROCEDURE
GO

-- PERMISSIONS

GRANT EXECUTE ON [dbo].[PR_collection_by_machine_and_date] TO [wggui] WITH GRANT OPTION
GO



--------------------------------------------------------------------------------
--   DESCRIPTION: Balance Handpays Report
-- 
--        AUTHOR: Ervin Olvera
-- 
-- CREATION DATE: 21-JUN-2016
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 21-JUN-2016 EOR    First release.
-- 04-JUL-2016 EOR    Bug 14939: Handpays Balance: Only Once Search and Window exit
-------------------------------------------------------------------------------- 



IF OBJECT_ID (N'dbo.GetHandpaysBalanceReport', N'P') IS NOT NULL
    DROP PROCEDURE dbo.GetHandpaysBalanceReport                 
GO    
CREATE PROCEDURE [dbo].[GetHandpaysBalanceReport]
( @pFromDt            DATETIME   
  ,@pToDt             DATETIME    
  ,@pClosingTime      INTEGER
  ,@pTerminalWhere    NVARCHAR(MAX)
  ,@pWithOutActivity  BIT
  ,@pOnlyUnbalance    BIT
)
AS
BEGIN
  DECLARE @index        AS INT
  DECLARE @nRows        AS INT
  DECLARE @p2007Opening AS DATETIME
  
  DECLARE @c2007Opening AS CHAR(20)
  DECLARE @cFromDt      AS CHAR(20)
  DECLARE @cToDt        AS CHAR(20)
  DECLARE @query        AS VARCHAR(MAX)
  DECLARE @apos         AS CHAR(1)
  DECLARE @query_day    AS VARCHAR(MAX)
  DECLARE @query_where  AS VARCHAR(MAX)
  
  SET @index = 0   
  SET @query_day = ''
  SET @query_where = ''
  
  SET @p2007Opening  = CAST('2007-01-01 00:00:00' AS DATETIME)
  SET @p2007Opening  = DATEADD(HOUR, @pClosingTime, @p2007Opening)
  
  SET @c2007Opening = CONVERT(CHAR(20),@p2007Opening,120)
  SET @cFromDt = CONVERT(CHAR(20),@pFromDt,120)
  SET @cToDt = CONVERT(CHAR(20),@pToDt,120)
  
  SET @apos = CHAR(39)
  SET @nRows  = DATEDIFF(DAY, @pFromDt, @pToDt) + 1 
  
  
  WHILE @index < @nRows                           
  BEGIN
    SET @query_day = @query_day + ' UNION SELECT ' + LTRIM(RTRIM(CAST(@index AS CHAR(5)))) + ' AS ORDER_DATE '                                         
    SET @index = @index + 1                       
  END

  SET @query_day = ' SELECT DATEADD(DAY, ORDER_DATE, CAST('+ @apos + @cFromDt + @apos +' AS DATETIME)) AS ORDER_DATE 
                     FROM (' + SUBSTRING(@query_day,8, LEN(@query_day)) + ') TABLE_DAYS '
           
           
  IF @pWithOutActivity = 1
    BEGIN
      SET @query_where = ' AND (BALANCE_HANDPAYS.SYSTEM_HANDPAYS_TOTAL <> 0 OR BALANCE_HANDPAYS.COUNTERS_HANDPAYS_TOTAL <> 0) '
    END          
         
  IF @pOnlyUnbalance = 1
    BEGIN
      SET @query_where = @query_where + ' AND BALANCE_HANDPAYS.DIFFERENCE <> 0 '
    END
    
    
  IF @query_where <> ''
    BEGIN
      SET @query_where = ' WHERE ' + SUBSTRING(@query_where, 6, LEN(@query_where))
    END 


  SET @query = ' SELECT * FROM (
                                SELECT  TE_PROVIDER_ID, 
                                        TE_TERMINAL_ID, 
                                        TE_NAME, 
                                        ISNULL(TE_MULTI_DENOMINATION, ' + @apos + '--' + @apos + ') AS DENOMINATION, 
                                        
                                        ISNULL(SUM(HAND_PAYS.CREDIT_CANCEL),0) AS SYSTEM_CREDIT_CANCEL, 
                                        ISNULL(SUM(HAND_PAYS.PROGRESIVES),0) + ISNULL(SUM(HAND_PAYS.NO_PROGRESIVES),0) AS SYSTEM_JACKPOTS,
                                        ISNULL(SUM(HAND_PAYS.CREDIT_CANCEL),0) + ISNULL(SUM(HAND_PAYS.PROGRESIVES),0) + ISNULL(SUM(HAND_PAYS.NO_PROGRESIVES),0) AS SYSTEM_HANDPAYS_TOTAL,

                                        ISNULL(SUM(METERS.TOTAL_HAND_PAID_CANCELLED_CREDITS),0) AS COUNTERS_CREDIT_CANCEL,
                                        ISNULL(SUM(METERS.TOTAL_JACKPOT_CREDITS),0) AS COUNTERS_JACKPOTS,
                                        ISNULL(SUM(METERS.TOTAL_HAND_PAID_CANCELLED_CREDITS),0) + ISNULL(SUM(METERS.TOTAL_JACKPOT_CREDITS),0) AS COUNTERS_HANDPAYS_TOTAL,
                                        
                                        (ISNULL(SUM(HAND_PAYS.CREDIT_CANCEL),0) + ISNULL(SUM(HAND_PAYS.PROGRESIVES),0) + ISNULL(SUM(HAND_PAYS.NO_PROGRESIVES),0)) -
                                        (ISNULL(SUM(METERS.TOTAL_HAND_PAID_CANCELLED_CREDITS),0) + ISNULL(SUM(METERS.TOTAL_JACKPOT_CREDITS),0)) AS DIFFERENCE
                                        
                                FROM   TERMINALS 
                                LEFT JOIN (' + LTRIM(RTRIM(@query_day)) + '
                                          ) DIA ON ORDER_DATE <= GETDATE() 
                                LEFT JOIN (SELECT TC_TERMINAL_ID, 
                                                  DATEADD(HOUR, 10, TC_DATE) AS TC_DATE 
                                           FROM   TERMINALS_CONNECTED 
                                           WHERE  TC_DATE >= DATEADD(DAY, DATEDIFF(DAY, 0, CAST('+ @apos + @cFromDt + @apos +' AS DATETIME)), 0) 
                                                  AND TC_DATE < DATEADD(DAY, DATEDIFF(DAY, 0, CAST('+ @apos + @cToDt + @apos +' AS DATETIME)), 0) 
                                                  AND TC_STATUS = 0 
                                                  AND TC_CONNECTED = 1
                                          ) TC ON TC_TERMINAL_ID = TE_TERMINAL_ID AND TC_DATE = DIA.ORDER_DATE 
                                LEFT JOIN (SELECT HP_TERMINAL_ID, 
                                                  DATEADD(DAY, DATEDIFF(HOUR, CAST('+ @apos + @c2007Opening + @apos +' AS DATETIME), HP_DATETIME) / 24, CAST('+ @apos + @c2007Opening + @apos +' AS DATETIME)) AS HP_DATETIME, 
                                                  SUM(CASE WHEN HP_TYPE IN (0, 1000) 
                                                           THEN ISNULL(dbo.GetDualCurrencyAmount(HP_AMOUNT, HP_AMT0, 1), 0) ELSE 0 END) + 
                                                  SUM(CASE WHEN (HP_TYPE IN (1, 1001) AND (HP_LEVEL >= 1 AND HP_LEVEL <= 32)) 
                                                           THEN CASE WHEN HP_TAX_BASE_AMOUNT IS NULL 
                                                                     THEN 0 
                                                                     ELSE ISNULL(dbo.GetDualCurrencyAmount(HP_AMOUNT, HP_AMT0, 1), 0) - 
                                                                          ISNULL(HP_TAX_BASE_AMOUNT, 0) END 
                                                           ELSE 0 END) AS CREDIT_CANCEL, 
                                                  SUM(CASE WHEN (HP_TYPE IN (1, 1001) AND (HP_LEVEL >= 1 AND HP_LEVEL <= 32) AND HP_PROGRESSIVE_ID IS NOT NULL) 
                                                           THEN CASE WHEN HP_TAX_BASE_AMOUNT IS NULL 
                                                                     THEN ISNULL(dbo.GetDualCurrencyAmount(HP_AMOUNT, HP_AMT0, 1), 0) 
                                                                     ELSE ISNULL(HP_TAX_BASE_AMOUNT, 0) END 
                                                           ELSE 0 END) AS PROGRESIVES, 
                                                  SUM(CASE WHEN (HP_TYPE IN (1, 1001) AND (HP_LEVEL < 1 OR HP_LEVEL > 32 OR HP_LEVEL IS NULL)) 
                                                           THEN ISNULL(dbo.GetDualCurrencyAmount(HP_AMOUNT, HP_AMT0, 1), 0) 
                                                           ELSE 0 END) + 
                                                  SUM(CASE WHEN (HP_TYPE IN (1, 1001) AND (HP_LEVEL >= 1 AND HP_LEVEL <= 32 AND HP_PROGRESSIVE_ID IS NULL)) 
                                                           THEN CASE WHEN (HP_TAX_BASE_AMOUNT IS NULL) 
                                                                     THEN ISNULL(dbo.GetDualCurrencyAmount(HP_AMOUNT, HP_AMT0, 1), 0)
                                                                     ELSE ISNULL(HP_TAX_BASE_AMOUNT, 0) END 
                                                           ELSE 0 END) AS NO_PROGRESIVES 
                                           FROM HANDPAYS WITH (INDEX (IX_HP_STATUS_CHANGED)) 
                                           WHERE  HP_DATETIME >= CAST('+ @apos + @cFromDt + @apos +' AS DATETIME) 
                                                  AND HP_DATETIME < CAST('+ @apos + @cToDt + @apos +' AS DATETIME) 
                                                  AND HP_TYPE <> 20 
                                          GROUP BY HP_TERMINAL_ID, 
                                                    DATEADD(DAY, DATEDIFF(HOUR, CAST('+ @apos + @c2007Opening + @apos +' AS DATETIME), HP_DATETIME) / 24, CAST('+ @apos + @c2007Opening + @apos +' AS DATETIME))
                                          ) HAND_PAYS ON HP_TERMINAL_ID = TE_TERMINAL_ID AND HP_DATETIME = DIA.ORDER_DATE 
                                LEFT JOIN (SELECT TSMH_TERMINAL_ID, 
                                                  DATEADD(DAY, DATEDIFF(HOUR, CAST('+ @apos + @c2007Opening + @apos +' AS DATETIME), TSMH_DATETIME) / 24, CAST('+ @apos + @c2007Opening + @apos +' AS DATETIME)) AS TSMH_DATE, 
                                                  SUM(CASE WHEN TSMH_METER_CODE = 2 
                                                          THEN ISNULL(CAST(TSMH_METER_INCREMENT AS MONEY) / 100, 0) END) AS TOTAL_JACKPOT_CREDITS, 
                                                  SUM(CASE WHEN TSMH_METER_CODE = 3 
                                                          THEN ISNULL(CAST(TSMH_METER_INCREMENT AS MONEY) / 100, 0) END) AS TOTAL_HAND_PAID_CANCELLED_CREDITS 
                                           FROM   TERMINAL_SAS_METERS_HISTORY 
                                           WHERE  TSMH_TYPE = 1 
                                                  AND TSMH_METER_CODE IN (0, 1, 2, 3 ) 
                                                  AND TSMH_METER_INCREMENT > 0 
                                                  AND TSMH_DATETIME >= CAST('+ @apos + @cFromDt + @apos +' AS DATETIME) 
                                                  AND TSMH_DATETIME < CAST('+ @apos + @cToDt + @apos +' AS DATETIME) 
                                           GROUP BY TSMH_TERMINAL_ID, 
                                                    DATEADD(DAY, DATEDIFF(HOUR, CAST('+ @apos + @c2007Opening + @apos +' AS DATETIME), TSMH_DATETIME) / 24, CAST('+ @apos + @c2007Opening + @apos +' AS DATETIME))
                                          ) METERS ON TE_TERMINAL_ID = TSMH_TERMINAL_ID AND TSMH_DATE = DIA.ORDER_DATE 
                                WHERE (HAND_PAYS.CREDIT_CANCEL IS NOT NULL 
                                        OR HAND_PAYS.PROGRESIVES IS NOT NULL 
                                        OR HAND_PAYS.NO_PROGRESIVES IS NOT NULL 
                                        OR METERS.TOTAL_JACKPOT_CREDITS IS NOT NULL 
                                        OR METERS.TOTAL_HAND_PAID_CANCELLED_CREDITS IS NOT NULL 
                                        OR TC_DATE IS NOT NULL ) ' + LTRIM(RTRIM(@pTerminalWhere)) + '
                                GROUP BY  TE_PROVIDER_ID, 
                                          TE_TERMINAL_ID, 
                                          TE_NAME, 
                                          ISNULL(TE_MULTI_DENOMINATION, ' + @apos + '--' + @apos + ') 
                               )  BALANCE_HANDPAYS ' + @query_where + '                                                                
                 ORDER BY BALANCE_HANDPAYS.TE_PROVIDER_ID, 
                          BALANCE_HANDPAYS.TE_TERMINAL_ID, 
                          BALANCE_HANDPAYS.TE_NAME, 
                          BALANCE_HANDPAYS.DENOMINATION '

  EXECUTE (@query)                          
END 
GO
-- PERMISSIONS
GRANT EXECUTE ON [dbo].[GetHandpaysBalanceReport] TO [wggui] WITH GRANT OPTION
GO


/******* TRIGGERS *******/

-- Special Updates

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.Voucher' AND GP_SUBJECT_KEY ='Redeem.SignaturesRoom')
   INSERT INTO general_params (gp_group_key, gp_subject_key, gp_key_value)
      VALUES ('Cashier.Voucher', 'Redeem.SignaturesRoom', '0');
GO

-- TABLE CREDIT_LINES
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.credit_lines') AND type in (N'U'))
BEGIN
CREATE TABLE dbo.credit_lines(
	cl_id bigint IDENTITY(1,1) NOT NULL,
	cl_account_id bigint NOT NULL,
	cl_limit_amount money NOT NULL,
	cl_tto_amount money NULL,
	cl_spent_amount money NOT NULL,
	cl_iso_code nvarchar(3) NOT NULL,
	cl_status int NOT NULL,
	cl_iban nvarchar(34) NULL,
	cl_xml_signers xml NULL,
	cl_creation datetime NOT NULL,
	cl_update datetime NOT NULL,
	cl_expired datetime NULL,
	cl_last_payback datetime NULL,
CONSTRAINT [PK_cl_id] PRIMARY KEY CLUSTERED 
(
  cl_id ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

-- CREATE CREDIT_LINE_MOVEMENTS
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.credit_line_movements') AND type in (N'U'))
BEGIN
CREATE TABLE dbo.credit_line_movements(
	clm_id bigint IDENTITY(1,1) NOT NULL,
	clm_credit_line_id bigint NOT NULL,
	clm_operation_id bigint NOT NULL,
	clm_type int NOT NULL,
	clm_old_value xml NULL,
	clm_new_value xml NOT NULL,	
	clm_creation datetime NOT NULL,
	clm_creation_user nvarchar(50) NOT NULL,
CONSTRAINT [PK_clm_id] PRIMARY KEY CLUSTERED 
(
  clm_id ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

-- MODIFY CAGE_CURRENCIES TABLE
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[cage_currencies]') and name = 'cgc_cage_visible')
  ALTER TABLE [dbo].[cage_currencies] ADD [cgc_cage_visible] bit NULL DEFAULT(1)
GO

-- UPDATE CAGE_CURRENCIES TABLE
UPDATE   CAGE_CURRENCIES 
   SET   CGC_CAGE_VISIBLE = 1
 WHERE   CGC_CAGE_VISIBLE IS NULL

GO

-- CREATE GENERALS PARAMS
IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CreditLine' AND GP_SUBJECT_KEY = 'Enabled')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('CreditLine', 'Enabled', '0')
GO  

IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CreditLine' AND GP_SUBJECT_KEY = 'NumSigners')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('CreditLine', 'NumSigners', '2')
GO  

DECLARE @CurrentCurrency AS VARCHAR(3)
DECLARE @NationalCurrency AS VARCHAR(3)

DECLARE @Allowed AS BIT
DECLARE Curs_Currencies CURSOR FOR SELECT CGC_ISO_CODE FROM CAGE_CURRENCIES GROUP BY CGC_ISO_CODE,CGC_ALLOWED

SELECT @NationalCurrency = gp_key_value FROM general_params 
WHERE gp_group_key = 'RegionalOptions' and gp_subject_key = 'CurrencyISOCode'
SET @NationalCurrency = ISNULL(@NationalCurrency, 'NULL')
SET @Allowed = 0
OPEN Curs_Currencies
FETCH NEXT FROM Curs_Currencies INTO @CurrentCurrency
WHILE @@FETCH_STATUS = 0
   BEGIN
      IF NOT EXISTS (SELECT * FROM CAGE_CURRENCIES WHERE CGC_DENOMINATION = -7 AND CGC_ISO_CODE = @CurrentCurrency) 
      BEGIN
		 IF EXISTS (select 1 where @CurrentCurrency = @NationalCurrency)
			SET @Allowed = 1
        INSERT INTO CAGE_CURRENCIES (CGC_ISO_CODE , CGC_DENOMINATION , CGC_ALLOWED, CGC_CAGE_CURRENCY_TYPE,CGC_CAGE_VISIBLE) VALUES (@CurrentCurrency , -7 , @Allowed, 0,0) -- -50 reserved for Coins in money collections
        
        INSERT INTO CURRENCY_EXCHANGE(CE_TYPE,CE_CURRENCY_ISO_CODE,CE_DESCRIPTION,CE_CHANGE,CE_NUM_DECIMALS,CE_VARIABLE_COMMISSION,CE_FIXED_COMMISSION,CE_VARIABLE_NR2,CE_FIXED_NR2,CE_STATUS,CE_FORMATTED_DECIMALS,CE_LANGUAGE_RESOURCES,CE_CONFIGURATION,CE_CURRENCY_ORDER)
			VALUES
							(7,@CurrentCurrency,'Credit Line',1.00000000,2,0.00,0.00,0.00,0.00,@Allowed,NULL,NULL,'<configuration><data><type>0</type><scope>0</scope><fixed_commission>0.00</fixed_commission><variable_commission>0.00</variable_commission><fixed_nr2>0.00</fixed_nr2><variable_nr2>0.00</variable_nr2>  </data></configuration>',NULL)
	  END
	  set @Allowed = 0
      FETCH NEXT FROM Curs_Currencies INTO @CurrentCurrency
   END
CLOSE Curs_Currencies
DEALLOCATE Curs_Currencies
GO

/****** INSERT TASK (76 - CREDITLINES) IN SITE ******/
  
DELETE FROM MS_SITE_TASKS 
WHERE ST_TASK_ID IN (76,77)
GO

/****** INSERT TASK (76 - CREDITLINES) IN SITE ******/
  
IF NOT EXISTS(SELECT ST_TASK_ID FROM MS_SITE_TASKS WHERE ST_TASK_ID = 76)
BEGIN
 INSERT   INTO MS_SITE_TASKS 
			 ( ST_TASK_ID
			, ST_ENABLED
			, ST_INTERVAL_SECONDS
			, ST_MAX_ROWS_TO_UPLOAD
			) 
	  SELECT  76
			, 1
			, 180
			, 200
END
GO

/****** INSERT TASK (77 - CREDITLINES) IN MULTISITE ******/
  
IF NOT EXISTS(SELECT ST_TASK_ID FROM MS_SITE_TASKS WHERE ST_TASK_ID = 77)
BEGIN
 INSERT   INTO MS_SITE_TASKS 
			 ( ST_TASK_ID
			, ST_ENABLED
			, ST_INTERVAL_SECONDS
			, ST_MAX_ROWS_TO_UPLOAD
			) 
	  SELECT  77
			, 1
			, 180
			, 200
END
GO

IF not EXISTS (SELECT 1 FROM SYS.COLUMNS WHERE NAME = N'cl_timestamp' AND Object_ID = Object_ID(N'credit_lines'))
BEGIN
   ALTER TABLE [dbo].[credit_lines]
   ADD cl_timestamp timestamp 
END
GO

IF not EXISTS (SELECT 1 FROM SYS.COLUMNS WHERE NAME = N'clm_timestamp' AND Object_ID = Object_ID(N'credit_line_movements'))
BEGIN
   ALTER TABLE [dbo].[credit_line_movements]
   ADD clm_timestamp timestamp 
END
GO

IF not EXISTS (SELECT 1 FROM SYS.INDEXES WHERE NAME = 'IDX_cl_account_id')
CREATE NONCLUSTERED INDEX IDX_cl_account_id ON [dbo].[credit_lines] 
(
	[cl_account_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

IF not EXISTS (SELECT 1 FROM SYS.INDEXES WHERE NAME = 'IDX_clm_credit_line_id')
CREATE NONCLUSTERED INDEX [IDX_clm_credit_line_id] ON [dbo].[credit_line_movements] 
(
	[clm_credit_line_id] ASC,
	[clm_type] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO


IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Account' AND GP_SUBJECT_KEY = 'PlayerPicture.AllowZoom')
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Account','PlayerPicture.AllowZoom','0')
GO


BEGIN
  DECLARE @rtc_form_id INT
  DECLARE @rtc_design_sheet VARCHAR(MAX)
  DECLARE @rtc_report_name VARCHAR(MAX)
  
  SET @rtc_report_name ='<LanguageResources><NLS09><Label>Gambling table activity</Label></NLS09><NLS10><Label>Actividad en mesa de juego</Label></NLS10></LanguageResources>'
  SET @rtc_design_sheet ='<ArrayOfReportToolDesignSheetsDTO>
                             <ReportToolDesignSheetsDTO>
                               <LanguageResources>
                                 <NLS09>
                                   <Label>Gambling table activity</Label>
                                 </NLS09>
                                 <NLS10>
                                   <Label>Actividad en mesa de juego</Label>
                                 </NLS10>
                               </LanguageResources>
                               <Columns>
                                 <ReportToolDesignColumn>
                                   <Code>TIPO</Code>
                                   <Width>300</Width>
                                   <EquityMatchType>Equality</EquityMatchType>
                                   <LanguageResources>
                                     <NLS09>
                                       <Label>Type</Label>
                                     </NLS09>
                                     <NLS10>
                                       <Label>Tipo</Label>
                                     </NLS10>
                                   </LanguageResources>
                                 </ReportToolDesignColumn>
                                 <ReportToolDesignColumn>
                                   <Code>MESA</Code>
                                   <Width>300</Width>
                                   <EquityMatchType>Equality</EquityMatchType>
                                   <LanguageResources>
                                     <NLS09>
                                       <Label>Table</Label>
                                     </NLS09>
                                     <NLS10>
                                       <Label>Mesa</Label>
                                     </NLS10>
                                   </LanguageResources>
                                 </ReportToolDesignColumn>
                                 <ReportToolDesignColumn>
                                   <Code>MES</Code>
                                   <Width>300</Width>
                                   <EquityMatchType>Equality</EquityMatchType>
                                   <LanguageResources>
                                     <NLS09>
                                       <Label>Month</Label>
                                     </NLS09>
                                     <NLS10>
                                       <Label>Mes</Label>
                                     </NLS10>
                                   </LanguageResources>
                                 </ReportToolDesignColumn>
                                 <ReportToolDesignColumn>
                                   <Code>DIAS HABILITADA</Code>
                                   <Width>300</Width>
                                   <EquityMatchType>Equality</EquityMatchType>
                                   <LanguageResources>
                                     <NLS09>
                                       <Label>Enabled days</Label>
                                     </NLS09>
                                     <NLS10>
                                       <Label>Dias habilitada</Label>
                                     </NLS10>
                                   </LanguageResources>
                                 </ReportToolDesignColumn>
                                 <ReportToolDesignColumn>
                                   <Code>DIAS USADO</Code>
                                   <Width>300</Width>
                                   <EquityMatchType>Equality</EquityMatchType>
                                   <LanguageResources>
                                     <NLS09>
                                       <Label>Used days</Label>
                                     </NLS09>
                                     <NLS10>
                                       <Label>Dias usado</Label>
                                     </NLS10>
                                   </LanguageResources>
                                 </ReportToolDesignColumn>
                               </Columns>
                             </ReportToolDesignSheetsDTO>
                           </ArrayOfReportToolDesignSheetsDTO>'
  
  IF NOT EXISTS(SELECT 1 FROM report_tool_config WHERE rtc_store_name = 'sp_GetTablesActivityFormatted')
  BEGIN
    SELECT @rtc_form_id = 11001
      FROM report_tool_config
    
    INSERT INTO [dbo].[REPORT_TOOL_CONFIG] VALUES /*RTC_REPORT_TOOL_ID*/
                                      /*RTC_FORM_ID*/        (@rtc_form_id,
                                      /*RTC_LOCATION_MENU*/   12, 
                                      /*RTC_REPORT_NAME*/     @rtc_report_name,
                                      /*RTC_STORE_NAME*/      'sp_GetTablesActivityFormatted',
                                      /*RTC_DESIGN_FILTER*/   '<ReportToolDesignFilterDTO><FilterType>MonthYear</FilterType></ReportToolDesignFilterDTO>',
                                      /*RTC_DESIGN_SHEETS*/   @rtc_design_sheet,
                                      /*RTC_MAILING*/         1,
                                      /*RTC_STATUS*/          0,
                                      /*RTC_MODE_TYPE*/       11)
                                      
  END 
  ELSE
  BEGIN
      UPDATE [dbo].[REPORT_TOOL_CONFIG] 
      SET RTC_REPORT_NAME =         /*RTC_REPORT_NAME*/     @rtc_report_name,
          RTC_DESIGN_SHEETS =       /*RTC_DESIGN_SHEETS*/   @rtc_design_sheet
      WHERE RTC_STORE_NAME = 'sp_GetTablesActivityFormatted'
  END
END
GO


IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Witholding.Document' AND GP_SUBJECT_KEY = 'NumCopies')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Witholding.Document', 'NumCopies', '2')
GO  

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MontlyReportPhilippines]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[MontlyReportPhilippines]
GO
CREATE PROCEDURE MontlyReportPhilippines
(
@pMonthFrom AS DATETIME,
@pMonthTo AS DATETIME
)
AS
BEGIN 

  DECLARE @TypeUnclaimed  INT
  DECLARE @AuxDate        DATETIME
  DECLARE @NowMonth       INT
  DECLARE @Language       NVARCHAR(1024)
  DECLARE @Message01      VARCHAR(100)
  DECLARE @Message02      VARCHAR(100)
  DECLARE @Winnings       MONEY
  
  SELECT @Language = gp_key_value  
  FROM general_params
  WHERE gp_group_key = 'WigosGUI'
    AND gp_subject_key = 'Language'
   
  SET @pMonthFrom = dbo.Opening(0, DATEADD(DAY, (DAY(@pMonthFrom) - 1) * -1, @pMonthFrom))
  SET @pMonthTo = dbo.Opening(0, DATEADD(DAY, -1, DATEADD(MONTH, 1, DATEADD(DAY, (DAY(@pMonthTo) - 1) * -1, @pMonthTo))))
  SET @TypeUnclaimed = 1000021
      
  IF ISNULL(@Language,'en-US') = 'es'
  BEGIN
    SET @Message01 = 'La fecha final debe ser mayor que la fecha inicial'
    SET @Message02 = 'No hay datos que cumplan las condiciones de búsqueda'
  END
  ELSE
  BEGIN
    SET @Message01 = 'Final date must be greater than end date'
    SET @Message02 = 'There is no data that meets the search criteria'
  END
  
  IF @pMonthFrom > @pMonthTo
  BEGIN
    RAISERROR(@Message01, 1, 1)  
    RETURN 
  END
  

  -- Create temp table Meters
  CREATE TABLE #TT_METERS
      ( DAY_WEEK        DATETIME, 
        TOTAL_BET       MONEY, 
        TOTAL_JACKPOTS  MONEY,
        TOTAL_PAYOUTS   MONEY 
      ) 

  -- Create temp table Unclaimed
  CREATE TABLE #TT_UNCLAIMED
      ( DAY_WEEK        DATETIME, 
        TOTAL_UNCLAIMED MONEY
      ) 
      
  -- Create temp table Result
  CREATE TABLE #TT_RESULT
      ( TYPE_ROW        INT,
        DAY_WEEK        DATETIME,
        TOTAL_BET       MONEY,
        TOTAL_PAYOUTS   MONEY,
        TOTAL_JACKPOTS  MONEY,
        TOTAL_UNCLAIMED MONEY
      )
        
  INSERT INTO #TT_METERS       
  SELECT  /*DAY*/
        dbo.Opening(0,TSMH_DATETIME)  AS DATE
        /*BET*/
        , SUM( CASE WHEN TSMH_TYPE <> 20 THEN
                  - CASE WHEN (TSMH_METER_CODE = 0)                           THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               ELSE
                    CASE WHEN (TSMH_METER_CODE = 0)                           THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               END ) AS TOTAL_BET
        /*JACKPOTS*/
        , SUM( CASE WHEN TSMH_TYPE <> 20 THEN
                  - CASE WHEN (TSMH_METER_CODE = 2)                            THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               ELSE
                    CASE WHEN (TSMH_METER_CODE = 2)                            THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               END ) AS TOTAL_JACKPOTS
        /*PAYOUTS*/
        , SUM( CASE WHEN TSMH_TYPE <> 20 THEN
                  - CASE WHEN (TSMH_METER_CODE = 1)                            THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               ELSE
                    CASE WHEN (TSMH_METER_CODE = 1)                            THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               END ) AS TOTAL_PAYOUTS              
  FROM TERMINAL_SAS_METERS_HISTORY  WITH(INDEX(PK_terminal_sas_meters_history))
  WHERE TSMH_DATETIME >= @pMonthFrom 
    AND TSMH_DATETIME < @pMonthTo
    AND TSMH_TYPE IN ( 20   /*MINCETUR_DAILY_REPORT*/
                       , 10   /*METER_RESET*/
                       , 11   /*METER_ROLLOVER*/
                       , 15   /*METER_SERVICE_RAM_CLEAR*/
                       , 16   /*METER_MACHINE_RAM_CLEAR*/
                         )
    AND TSMH_METER_CODE IN  ( 0   /*Total IN*/
                            , 1   /*Total OUT*/
                            , 2   /*Jackpots*/
                            )
  GROUP BY TSMH_DATETIME
  
  INSERT INTO #TT_UNCLAIMED
  SELECT dbo.Opening(0, CM_DATE), SUM(CM_ADD_AMOUNT)
  FROM CASHIER_MOVEMENTS WITH (INDEX (IX_cm_date_type))
  WHERE CM_DATE >= @pMonthFrom 
    AND CM_DATE < @pMonthTo
    AND CM_TYPE = @TypeUnclaimed
  GROUP BY dbo.Opening(0, CM_DATE)
  
  
  SET @AuxDate = @pMonthFrom
  SET @NowMonth = MONTH(@AuxDate)
  
  WHILE(@AuxDate <= @pMonthTo)
  BEGIN
    
    INSERT INTO #TT_RESULT (TYPE_ROW, DAY_WEEK, TOTAL_BET, TOTAL_PAYOUTS, TOTAL_JACKPOTS, TOTAL_UNCLAIMED) 
    VALUES (1, @AuxDate, 0 , 0, 0 ,0)
    
    UPDATE #TT_RESULT
    SET #TT_RESULT.TOTAL_BET = ISNULL(#TT_METERS.TOTAL_BET, 0)
      , #TT_RESULT.TOTAL_PAYOUTS = ISNULL(#TT_METERS.TOTAL_PAYOUTS, 0)
      , #TT_RESULT.TOTAL_JACKPOTS = ISNULL(#TT_METERS.TOTAL_JACKPOTS, 0)
    FROM  #TT_RESULT
    INNER JOIN #TT_METERS ON #TT_METERS.DAY_WEEK = #TT_RESULT.DAY_WEEK
    WHERE #TT_RESULT.DAY_WEEK =  @AuxDate
    
    UPDATE #TT_RESULT
    SET #TT_RESULT.TOTAL_UNCLAIMED = ISNULL(#TT_UNCLAIMED.TOTAL_UNCLAIMED, 0)
    FROM  #TT_RESULT
    INNER JOIN #TT_UNCLAIMED ON #TT_UNCLAIMED.DAY_WEEK = #TT_RESULT.DAY_WEEK
    WHERE #TT_RESULT.DAY_WEEK =  @AuxDate    
    
    SET @AuxDate = DATEADD(DAY, 1, @AuxDate)
    SET @NowMonth = MONTH(@AuxDate)
    
    IF MONTH(@AuxDate)  <> MONTH(DATEADD(DAY, -1, @AuxDate))
    BEGIN 
      INSERT INTO #TT_RESULT 
                ( TYPE_ROW
                , DAY_WEEK
                , TOTAL_BET
                , TOTAL_PAYOUTS
                , TOTAL_JACKPOTS
                , TOTAL_UNCLAIMED
                ) 
      SELECT  2
              , DATEADD(DAY, -1, @AuxDate)
              , SUM(TOTAL_BET)
              , SUM(TOTAL_PAYOUTS)
              , SUM(TOTAL_JACKPOTS)
              , SUM(TOTAL_UNCLAIMED)
      FROM  #TT_RESULT
      WHERE MONTH(DAY_WEEK) = MONTH(DATEADD(DAY, -1, @AuxDate))
    END 
    
  END 
    
  INSERT INTO #TT_RESULT 
            ( TYPE_ROW
            , DAY_WEEK
            , TOTAL_BET
            , TOTAL_PAYOUTS
            , TOTAL_JACKPOTS
            , TOTAL_UNCLAIMED
            ) 
  SELECT  3
        , DATEADD(DAY, -1, @AuxDate)
        , SUM(TOTAL_BET)
        , SUM(TOTAL_PAYOUTS)
        , SUM(TOTAL_JACKPOTS)
        , SUM(TOTAL_UNCLAIMED)
  FROM  #TT_RESULT
  WHERE TYPE_ROW = 2
  
  SELECT @Winnings = TOTAL_BET - (TOTAL_PAYOUTS + TOTAL_JACKPOTS - TOTAL_UNCLAIMED)
  FROM #TT_RESULT
  WHERE TYPE_ROW = 3
  
  IF ISNULL(@Winnings,0) = 0
  BEGIN
    RAISERROR(@Message02, 1, 1)  
    RETURN 
  END
 
  SELECT RTRIM(CASE TYPE_ROW
        WHEN 1 THEN SUBSTRING(DATENAME(MONTH, DAY_WEEK), 1, 3) + '-' + CAST(DAY(DAY_WEEK) AS CHAR)
        WHEN 2 THEN SUBSTRING(DATENAME(MONTH, DAY_WEEK), 1, 3) + ' Total' 
        ELSE 'TOTAL' END) DAY_MONTH
      , CASE TYPE_ROW
        WHEN 1 THEN SUBSTRING(DATENAME(dw, DAY_WEEK), 1, 3) END DAY_WEEK
      , TOTAL_BET
      , TOTAL_PAYOUTS AS TOTAL_PAYOUTS_1
      , TOTAL_UNCLAIMED
      , TOTAL_JACKPOTS
      , TOTAL_PAYOUTS + TOTAL_JACKPOTS - TOTAL_UNCLAIMED AS TOTAL_PAYOUTS_2
      , TOTAL_BET - (TOTAL_PAYOUTS + TOTAL_JACKPOTS - TOTAL_UNCLAIMED) AS WINNINGS
  FROM #TT_RESULT
    
  DROP TABLE #TT_METERS    
  DROP TABLE #TT_UNCLAIMED    
  DROP TABLE #TT_RESULT    
    
END

GO

GRANT EXECUTE ON [dbo].[MontlyReportPhilippines] TO wggui WITH GRANT OPTION
GO

BEGIN
  DECLARE @rtc_form_id INT
  DECLARE @rtc_design_sheet VARCHAR(MAX)
  DECLARE @rtc_design_filter VARCHAR(MAX)

  SET @rtc_design_sheet = '<ArrayOfReportToolDesignSheetsDTO>
   <ReportToolDesignSheetsDTO>
      <LanguageResources>
         <NLS09>
            <Label>Monthly Report</Label>
         </NLS09>
         <NLS10>
            <Label>Reporte mensual</Label>
         </NLS10>
      </LanguageResources>
      <Columns>
         <!--Date-->	  
         <ReportToolDesignColumn>
            <Code>DAY_MONTH</Code>
            <Width>300</Width>
            <EquityMatchType>Equality</EquityMatchType>
            <LanguageResources>
               <NLS09>
                  <Label>Date</Label>
               </NLS09>
               <NLS10>
                  <Label>Fecha</Label>
               </NLS10>
            </LanguageResources>
         </ReportToolDesignColumn>
         <!--Days-->	           
		 <ReportToolDesignColumn>
            <Code>DAY_WEEK</Code>
            <Width>300</Width>
            <EquityMatchType>Equality</EquityMatchType>
            <LanguageResources>
               <NLS09>
                  <Label>Days</Label>
               </NLS09>
               <NLS10>
                  <Label>Días</Label>
               </NLS10>
            </LanguageResources>
         </ReportToolDesignColumn>
         <!--Total Bet-->	  
         <ReportToolDesignColumn>
            <Code>TOTAL_BET</Code>
            <Width>300</Width>
            <EquityMatchType>Equality</EquityMatchType>
            <LanguageResources>
               <NLS09>
                  <Label>Total Bet</Label>
               </NLS09>
               <NLS10>
                  <Label>Total Apuestas</Label>
               </NLS10>
            </LanguageResources>
         </ReportToolDesignColumn>
         <!--Total Payouts-->	  
         <ReportToolDesignColumn>
            <Code>TOTAL_PAYOUTS_1</Code>
            <Width>300</Width>
            <EquityMatchType>Equality</EquityMatchType>
            <LanguageResources>
               <NLS09>
                  <Label>Total - Payouts</Label>
               </NLS09>
               <NLS10>
                  <Label>Total - Pagos</Label>
               </NLS10>
            </LanguageResources>
         </ReportToolDesignColumn>
         <!--Unclaimed-->	  
         <ReportToolDesignColumn>
            <Code>TOTAL_UNCLAIMED</Code>
            <Width>300</Width>
            <EquityMatchType>Equality</EquityMatchType>
            <LanguageResources>
               <NLS09>
                  <Label>Unclaimed</Label>
               </NLS09>
               <NLS10>
                  <Label>No Reclamos</Label>
               </NLS10>
            </LanguageResources>
         </ReportToolDesignColumn>
         <!--Jackpot 20%-->	  
         <ReportToolDesignColumn>
            <Code>TOTAL_JACKPOTS</Code>
            <Width>300</Width>
            <EquityMatchType>Equality</EquityMatchType>
            <LanguageResources>
               <NLS09>
                  <Label>Jackpot 20%</Label>
               </NLS09>
               <NLS10>
                  <Label>Jackpot 20%</Label>
               </NLS10>
            </LanguageResources>
         </ReportToolDesignColumn>
         <!--Total Payouts-->	           
		 <ReportToolDesignColumn>
            <Code>TOTAL_PAYOUTS_2</Code>
            <Width>300</Width>
            <EquityMatchType>Equality</EquityMatchType>
            <LanguageResources>
               <NLS09>
                  <Label>Total Payouts</Label>
               </NLS09>
               <NLS10>
                  <Label>Total Pagos</Label>
               </NLS10>
            </LanguageResources>
         </ReportToolDesignColumn>
         <!--Winnings-->	                    
		 <ReportToolDesignColumn>
            <Code>WINNINGS</Code>
            <Width>300</Width>
            <EquityMatchType>Equality</EquityMatchType>
            <LanguageResources>
               <NLS09>
                  <Label>Winnings</Label>
               </NLS09>
               <NLS10>
                  <Label>Ganancias</Label>
               </NLS10>
            </LanguageResources>
         </ReportToolDesignColumn>
      </Columns>
   </ReportToolDesignSheetsDTO>
</ArrayOfReportToolDesignSheetsDTO>'
  
  SET @rtc_design_filter = '<ReportToolDesignFilterDTO>
   <FilterType>CustomFilters</FilterType>
   <Filters>
      <ReportToolDesignFilter>
         <TypeControl>uc_date_picker</TypeControl>
         <TextControl>
            <LanguageResources>
               <NLS09>
                  <Label>From</Label>
               </NLS09>
               <NLS10>
                  <Label>Desde</Label>
               </NLS10>
            </LanguageResources>
         </TextControl>
         <ParameterStoredProcedure>
            <Name>@pMonthFrom</Name>
            <Type>DateTime</Type>
         </ParameterStoredProcedure>
         <Methods>
            <Method>
               <Name>SetFormat</Name>
               <Parameters>14,0</Parameters>
            </Method>
         </Methods>
         <Propertys>
            <Property>
               <Name>ShowUpDown</Name>
               <Value>True</Value>
            </Property>
         </Propertys>
         <Value>TodayOpening()-30</Value>
      </ReportToolDesignFilter>
      <ReportToolDesignFilter>
         <TypeControl>uc_date_picker</TypeControl>
         <TextControl>
            <LanguageResources>
               <NLS09>
                  <Label>To</Label>
               </NLS09>
               <NLS10>
                  <Label>Hasta</Label>
               </NLS10>
            </LanguageResources>
         </TextControl>
         <ParameterStoredProcedure>
            <Name>@pMonthTo</Name>
            <Type>DateTime</Type>
         </ParameterStoredProcedure>
         <Methods>
            <Method>
               <Name>SetFormat</Name>
               <Parameters>14,0</Parameters>
            </Method>
         </Methods>
         <Propertys>
            <Property>
               <Name>ShowUpDown</Name>
               <Value>True</Value>
            </Property>
         </Propertys>         
         <Value>TodayOpening()</Value>
      </ReportToolDesignFilter>
   </Filters>
</ReportToolDesignFilterDTO>'
  
  
  IF NOT EXISTS(SELECT 1 
				FROM report_tool_config 
				WHERE rtc_store_name = 'MontlyReportPhilippines')
  BEGIN
    SELECT @rtc_form_id = ISNULL(MAX(rtc_form_id),10999) + 1
    FROM report_tool_config
    
    INSERT INTO [dbo].[REPORT_TOOL_CONFIG] VALUES /*RTC_REPORT_TOOL_ID */
                                      /*RTC_FORM_ID*/        (@rtc_form_id,
                                      /*RTC_LOCATION_MENU*/   8, 
                                      /*RTC_REPORT_NAME*/     '<LanguageResources><NLS09><Label>Monthly Report</Label></NLS09><NLS10><Label>Reporte mensual</Label></NLS10></LanguageResources>',
                                      /*RTC_STORE_NAME*/      'MontlyReportPhilippines',
                                      /*RTC_DESIGN_FILTER*/   @rtc_design_filter,
                                      /*RTC_DESIGN_SHEETS*/   @rtc_design_sheet,
                                      /*RTC_MAILING*/         0,
                                      /*RTC_STATUS*/          0,
                                      /*RTC_MODE*/            1)
                                      
  END
  ELSE
  BEGIN
      UPDATE [dbo].[REPORT_TOOL_CONFIG] 
      SET RTC_DESIGN_SHEETS = /*RTC_DESIGN_SHEETS*/   @rtc_design_sheet
        , RTC_DESIGN_FILTER = /*RTC_DESIGN_FILTER*/   @rtc_design_filter
      WHERE RTC_STORE_NAME = 'MontlyReportPhilippines'
  END
END
GO


/*New Concept of System (Unclaimed)*/
IF NOT EXISTS ( SELECT 1 FROM CAGE_CONCEPTS WHERE CC_CONCEPT_ID = 21)
BEGIN
	INSERT INTO cage_concepts 
			      ( cc_concept_id
			      , cc_description
			      , cc_is_provision
			      , cc_show_in_report
			      , cc_unit_price
			      , cc_name
			      , cc_type
			      , cc_enabled
			      , cc_sequence_id
			      , cc_operation_code
			      , cc_voucher_type
            )
    VALUES  ( 21          -- cc_concept_id - bigint
            , 'Unclaimed' -- cc_description - nvarchar(50)
            , 0           -- cc_is_provision - bit
            , 1           -- cc_show_in_report - bit
            , NULL        -- cc_unit_price - money
            , 'Unclaimed' -- cc_name - nvarchar(20)
            , 1           -- cc_type - int
            , 1           -- cc_enabled - bit
            , NULL        -- cc_sequence_id - int
            , NULL        -- cc_operation_code - int
            , NULL        -- cc_voucher_type - int
            )
END
GO

IF NOT EXISTS ( SELECT 1 FROM CAGE_SOURCE_TARGET_CONCEPTS WHERE CSTC_CONCEPT_ID = 21)
BEGIN
  INSERT INTO cage_source_target_concepts
            ( cstc_concept_id 
            , cstc_source_target_id 
            , cstc_type 
            , cstc_only_national_currency 
            , cstc_cashier_action 
            , cstc_cashier_container 
            , cstc_cashier_btn_group 
            , cstc_enabled 
            , cstc_price_factor
            )
    VALUES  ( 21                        -- cstc_concept_id - bigint
            , 10                        -- cstc_source_target_id - bigint
            , 0                         -- cstc_type - int
            , 0                         -- cstc_only_national_currency - bit
            , 0                         -- cstc_cashier_action - int
            , 0                         -- cstc_cashier_container - int
            , 'STR_CASHIER_CONCEPTS_IN' -- cstc_cashier_btn_group - varchar(50)
            , 1                         -- cstc_enabled - bit
            , 1                         -- cstc_price_factor - money
            )          
END  
GO

IF OBJECT_ID('sp_CheckCashDeskDrawConfig', 'P') IS NOT NULL
DROP PROC sp_CheckCashDeskDrawConfig
GO
create PROCEDURE [dbo].[sp_CheckCashDeskDrawConfig]
      -- Add the parameters for the stored procedure here
      @pID INT = 0,
      @pName VARCHAR(20) = ''
AS
BEGIN
SET NOCOUNT ON;

DELETE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CashDesk.Draw.00'
DELETE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration.00'
   
DECLARE @POSTFIJ AS VARCHAR(5)

IF @pID = 0
BEGIN
  SET @POSTFIJ = ''
END
ELSE 
BEGIN 
  SET @POSTFIJ = '.' + RIGHT( '00' + CONVERT(VARCHAR(2),@pID),2)
END


  --'CashDesk.Draw', 'Enabled', 0
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ
                        AND GP_SUBJECT_KEY = 'Enabled'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'Enabled', '0')
END


--'CashDesk.Draw', 'ShowCashDeskDraws', 0
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ
                        AND GP_SUBJECT_KEY = 'ShowCashDeskDraws'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'ShowCashDeskDraws', '0')
END


--'CashDesk.Draw', 'VoucherOnCashDeskDraws.Winner', 0
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'VoucherOnCashDeskDraws.Winner'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'VoucherOnCashDeskDraws.Winner', '0')
END


--'CashDesk.Draw', 'WinnerPrizeVoucherTitle', 'Sorteo en XXXX'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrizeVoucherTitle'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'WinnerPrizeVoucherTitle', 'Sorteo en ' + @pName)
END


--'CashDesk.Draw', 'VoucherOnCashDeskDraws.Loser', 0
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'VoucherOnCashDeskDraws.Loser'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'VoucherOnCashDeskDraws.Loser', '0')
END


--'CashDesk.Draw', 'AskForParticipation', 0
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'AskForParticipation'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'AskForParticipation', '0')
END


--'CashDesk.Draw', 'LoserPrizeVoucherTitle','Cortesía sorteo en XXXX'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'LoserPrizeVoucherTitle'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'LoserPrizeVoucherTitle', 'Cortesía sorteo en ' + @pName)
END


--'CashDesk.Draw', 'ActionOnServerDown','0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'ActionOnServerDown'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'ActionOnServerDown', '0')
END


--'CashDesk.Draw', 'LocalServer', '1'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'LocalServer'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'LocalServer', '1')
END


--'CashDesk.Draw', 'ServerBDConnectionString',''
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'ServerBDConnectionString'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'ServerBDConnectionString', '')
END


--'CashDesk.Draw', 'ServerAddress1',''
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'ServerAddress1'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'ServerAddress1', '')
END


--'CashDesk.Draw', 'ServerAddress2',''
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'ServerAddress2'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'ServerAddress2', '')
END


--'CashDesk.DrawConfiguration', 'BallsExtracted','0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'BallsExtracted'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'BallsExtracted', '0')
END


--'CashDesk.DrawConfiguration', 'BallsOfParticipant', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'BallsOfParticipant'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'BallsOfParticipant', '0')
END


--'CashDesk.DrawConfiguration', 'TotalsBallsNumber', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'TotalsBallsNumber'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'TotalsBallsNumber', '0')
END


--'CashDesk.DrawConfiguration', 'LoserPrize1.Fixed', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'LoserPrize1.Fixed'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'LoserPrize1.Fixed', '0')
END


--'CashDesk.DrawConfiguration', 'LoserPrize1.Percentage', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'LoserPrize1.Percentage'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'LoserPrize1.Percentage', '0')
END


--'CashDesk.DrawConfiguration', 'LoserPrize1.Enabled', 0
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'LoserPrize1.Enabled'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'LoserPrize1.Enabled', '0')
END


--'CashDesk.DrawConfiguration', 'NumberOfParticipants', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'NumberOfParticipants'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'NumberOfParticipants', '0')
END


--'CashDesk.DrawConfiguration', 'NumberOfWinners', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'NumberOfWinners'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'NumberOfWinners', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize1.Fixed', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize1.Fixed'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize1.Fixed', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize1.Percentage', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize1.Percentage'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize1.Percentage', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize1.Enabled', 0
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize1.Enabled'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize1.Enabled', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize2.Fixed', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize2.Fixed'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize2.Fixed', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize2.Percentage', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize2.Percentage'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize2.Percentage', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize2.Enabled', 0
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize2.Enabled'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize2.Enabled', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize3.Fixed', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize3.Fixed'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize3.Fixed', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize3.Percentage', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize3.Percentage'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize3.Percentage', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize3.Enabled', 0
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize3.Enabled'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize3.Enabled', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize4.Fixed', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize4.Fixed'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize4.Fixed', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize4.Percentage', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize4.Percentage'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize4.Percentage', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize4.Enabled', 0
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize4.Enabled'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize4.Enabled', '0')
END



--'CashDesk.DrawConfiguration', 'WinnerPrize1.PrizeType', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize1.PrizeType'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize1.PrizeType', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize2.PrizeType', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize2.PrizeType'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize2.PrizeType','0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize3.PrizeType', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize3.PrizeType'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize3.PrizeType', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize4.PrizeType', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize4.PrizeType'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize4.PrizeType', '0')
END


--'CashDesk.DrawConfiguration', 'LoserPrize1.PrizeType', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'LoserPrize1.PrizeType'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'LoserPrize1.PrizeType', '0')
END


-- 'CashDesk.Draw', 'AccountingMode', ''
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'AccountingMode'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'AccountingMode', '0')
END


-- 'CashDesk.DrawConfiguration', 'ParticipationPrice', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'ParticipationPrice'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'ParticipationPrice', '0')
END   
      
      
-- 'CashDesk.Draw', 'IsCashDeskDraw', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'IsCashDeskDraw'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'IsCashDeskDraw', '0')
END         


-- 'CashDesk.Draw', 'ReportUNRFromSP_SalesAndPayment', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'ReportUNRFromSP_SalesAndPayment'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'ReportUNRFromSP_SalesAndPayment', '0')
END   


-- 'CashDesk.Draw', 'AccountingMode.PromoNameMovement', 'PROMOCIÓN'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'AccountingMode.PromoNameMovement'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'AccountingMode.PromoNameMovement', 'Promoción')
END   

-- 'CashDesk.Draw', 'AccountingMode.KindOfNameMovement', 'Premio en especie'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'AccountingMode.KindOfNameMovement'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'AccountingMode.KindOfNameMovement', 'Premio en especie')
END   

-- 'CashDesk.Draw', 'AccountingMode.RE.KindOfNameMovement', 'Premio en especie (RE)'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'AccountingMode.RE.KindOfNameMovement'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'AccountingMode.RE.KindOfNameMovement', 'Premio en especie (RE)')
END   

-- 'CashDesk.Draw', 'AccountingMode.RE.PromoNameMovement', 'Promoción (RE)'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'AccountingMode.RE.PromoNameMovement'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'AccountingMode.RE.PromoNameMovement', 'Promoción (RE)')
END   


-- 'CashDesk.Draw', 'Voucher.HideCurrencySymbol', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'Voucher.HideCurrencySymbol'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'Voucher.HideCurrencySymbol', '0')
END   
      
-- 'CashDesk.Draw', 'Voucher.LoserPrizeLabel', 'Cortesia'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'Voucher.LoserPrizeLabel'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'Voucher.LoserPrizeLabel', 'Cortesia')
END   
      
-- 'CashDesk.Draw', 'Voucher.UNRLabel', 'FD'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'Voucher.UNRLabel'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'Voucher.UNRLabel', 'FD')
END   

-- 'CashDesk.Draw', 'Voucher.WinnerPrizeLabel', 'Premio'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'Voucher.WinnerPrizeLabel'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'Voucher.WinnerPrizeLabel', 'Premio')
END   


--/// Probability


IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'LoserPrize1.Probability'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'LoserPrize1.Probability', '0')
END

IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize1.Probability'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize1.Probability', '0')
END

IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize2.Probability'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize2.Probability', '0')
END

IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize3.Probability'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize3.Probability', '0')
END

IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'WinnerPrize4.Probability'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize4.Probability', '0')
END


IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'ParticipationMaxPrice'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'ParticipationMaxPrice', '1')
END


IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameUrl'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameUrl', '')
END


IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameTimeout'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameTimeout', '30')
END

IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameName'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameName', 'Terminal Draw')
END

IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameSecondDrawScreenMessageLine0'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameSecondDrawScreenMessageLine0', 'Bienvenido')
END

IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameSecondDrawScreenMessageLine1'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameSecondDrawScreenMessageLine1', 'Pulse[1][2][3][4][5]')
END

IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameSecondDrawScreenMessageLine2'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameSecondDrawScreenMessageLine2', 'Saldo: @BALANCE')
END


IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameSecondDrawScreenMessageLine3'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameSecondDrawScreenMessageLine3', 'Apuesta: @BET')
END

--Message Result Winner
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameResultWinnerDrawScreenMessageLine0'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameResultWinnerDrawScreenMessageLine0', 'Felicidades!!!')
END


IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameResultWinnerDrawScreenMessageLine1'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameResultWinnerDrawScreenMessageLine1', 'Ganaste: @WON')
END


IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameResultWinnerDrawScreenMessageLine2'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameResultWinnerDrawScreenMessageLine2', 'Saldo: @BALANCE')
END

IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameResultWinnerDrawScreenMessageLine3'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameResultWinnerDrawScreenMessageLine3', '')
END

--Message Result Looser
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameResultLooserDrawScreenMessageLine0'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameResultLooserDrawScreenMessageLine0', 'Perdiste!!!')
END


IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameResultLooserDrawScreenMessageLine1'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameResultLooserDrawScreenMessageLine1', 'Premio cortesía: @WON')
END


IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameResultLooserDrawScreenMessageLine2'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameResultLooserDrawScreenMessageLine2', 'Saldo: @BALANCE')
END

IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameResultLooserDrawScreenMessageLine3'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameResultLooserDrawScreenMessageLine3', '')
END

-- First Screen
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameFirstDrawScreenMessageLine0'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameFirstDrawScreenMessageLine0', '')
END


IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameFirstDrawScreenMessageLine1'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameFirstDrawScreenMessageLine1', 'Pulse [1] para jugar')
END


IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameFirstDrawScreenMessageLine2'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameFirstDrawScreenMessageLine2', '')
END


IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameFirstDrawScreenTimeOut'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameFirstDrawScreenTimeOut', '10')
END


IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameFirsIfTimeOutExpiresParticipateInDraw'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameFirsIfTimeOutExpiresParticipateInDraw', '0')
END


IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'CloseOpenedSessionsBeforeNewGame'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'CloseOpenedSessionsBeforeNewGame', '0')
END


IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ
                        AND GP_SUBJECT_KEY = 'TerminalGameFirstDrawScreenForceParticipateInDraw'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameFirstDrawScreenForceParticipateInDraw', '0')
END

      
END
GO

GRANT EXECUTE ON [dbo].[sp_CheckCashDeskDrawConfig] TO [wggui]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ReportTerminalDraws]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ReportTerminalDraws]
GO

SET QUOTED_IDENTIFIER ON
GO

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[ReportTerminalDraws]  
  @pDrawFrom	      DATETIME      = NULL,
  @pDrawTo		      DATETIME      = NULL,
  @pDrawId		      BIGINT		    =	NULL,
  @pTerminalId      INT			      = NULL,
  @pUserId          INT			      = NULL,
  @pSqlAccount      NVARCHAR(MAX) = NULL,
  @pCashierMovement INT	          = NULL,
  @pTerminalDraw	  INT				    = 1,
  @pLooserPrizeId   INT           = 5

AS
BEGIN

	SET NOCOUNT ON;
	DECLARE @_Terminal_draw_type AS INT;
	DECLARE @_Sorter_terminal_ID AS INT;
	DECLARE @_Promotion_Redeemeable AS INT;
	DECLARE @_Promotion_NoRedeemeable AS INT;
	SET @_Terminal_draw_type = 5
	SET @_Sorter_terminal_ID = (SELECT  TOP 1 te_terminal_id FROM terminals WHERE te_terminal_type = @_Terminal_draw_type)
	SET @_Promotion_Redeemeable = 14
	SET @_Promotion_NoRedeemeable = 13

  CREATE TABLE #ACCOUNTS_TEMP (AC_ACCOUNT_ID BIGINT NOT NULL, AC_HOLDER_NAME NVARCHAR(200), AC_TRACK_DATA NVARCHAR(50))
	IF @pSqlAccount IS NOT NULL
	BEGIN
	  INSERT INTO #ACCOUNTS_TEMP EXEC ('SELECT AC_ACCOUNT_ID, AC_HOLDER_NAME, AC_TRACK_DATA  FROM ACCOUNTS ' + @pSqlAccount)
		IF @@ROWCOUNT > 500
			ALTER TABLE #ACCOUNTS_TEMP ADD PRIMARY KEY NONCLUSTERED (AC_ACCOUNT_ID)
	END

	IF 	@pDrawId IS NOT NULL 
	--
	-- Filtering by Draw ID
	--
	BEGIN	
	  IF @pDrawFrom IS NULL SET @pDrawFrom = '2007-01-01T00:00:00'
		IF @pDrawTo   IS NULL SET @pDrawTo   = GETDATE ()
		
    --Filter by terminalDraw 
	  IF  @pTerminalDraw = 1
	  BEGIN
		      SELECT    DISTINCT
		                CD_DRAW_ID
		              , CD_DRAW_DATETIME
		              , TE_TERMINAL_TYPE
		              , CM_CASHIER_NAME		  
		              , CM_USER_NAME
		              , CD_OPERATION_ID		
		              , CD_ACCOUNT_ID
		              , AC_HOLDER_NAME
		              , CD_COMBINATION_BET
		              , CD_COMBINATION_WON
		              , CD_RE_BET
                  , CASE WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_draw_type THEN AC.AO_PROMO_REDEEMABLE 
                         ELSE CD_RE_WON END CD_RE_WON
		              , CASE WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_draw_type  THEN AC.AO_PROMO_NOT_REDEEMABLE 
		                     ELSE CD_NR_WON END CD_NR_WON
		              , CD_PRIZE_ID
		        FROM    CASHDESK_DRAWS WITH(INDEX(IX_CD_DRAW_DATETIME_ACCOUNT))
		  INNER JOIN    CASHIER_MOVEMENTS WITH(INDEX(IX_CM_OPERATION_ID)) 
		          ON    CD_OPERATION_ID = CM_OPERATION_ID  
		  INNER JOIN    ACCOUNTS   
		          ON    CD_ACCOUNT_ID   = AC_ACCOUNT_ID
		  INNER JOIN    TERMINALS 
		          ON    CD_TERMINAL =  TE_TERMINAL_ID
		   LEFT JOIN    ACCOUNT_OPERATIONS AC 
		          ON    AC.AO_OPERATION_ID = CD_OPERATION_ID
		   LEFT JOIN    ACCOUNT_PROMOTIONS AP 
		          ON    AP.ACP_OPERATION_ID = CD_OPERATION_ID AND AP.ACP_PROMO_TYPE IN (@_Promotion_Redeemeable, @_Promotion_NoRedeemeable)
  		        
		       WHERE    CD_DRAW_ID = @pDrawId	
             AND    CD_DRAW_DATETIME >= @pDrawFrom
	           AND    CD_DRAW_DATETIME < @pDrawTo	
	           AND    ((@pTerminalId IS NULL) OR (TE_TERMINAL_ID = @pTerminalId))
	           AND    ((@pUserId IS NULL) OR (CM_USER_ID = @pUserId))
	           AND    ( (@pSqlAccount     IS NULL) OR (CD_ACCOUNT_ID IN ( SELECT AC_ACCOUNT_ID FROM #ACCOUNTS_TEMP ) ) )
	           AND    te_terminal_type = @_Terminal_draw_type
		    ORDER BY    CD_DRAW_ID
	
	  DROP TABLE #ACCOUNTS_TEMP

		    RETURN		    	  
	  END	--IF  @pTerminalDraw = 1

	  IF 	@pSqlAccount IS NOT NULL 
	  --
	  -- Filtering by account, but not by draw ID
	  --
	  BEGIN		
		  IF @pDrawFrom IS NULL SET @pDrawFrom = '2007-01-01T00:00:00'
		  IF @pDrawTo   IS NULL SET @pDrawTo   = GETDATE ()

	    --Filter by TERMINAL draw
	    IF  @pTerminalDraw = 1
	    BEGIN
            SELECT DISTINCT
		                CD_DRAW_ID
		              , CD_DRAW_DATETIME
		              , TE_TERMINAL_TYPE
		              , CM_CASHIER_NAME		  
		              , CM_USER_NAME
		              , CD_OPERATION_ID		
		              , CD_ACCOUNT_ID
		              , CD_COMBINATION_BET
		              , CD_COMBINATION_WON
		              , CD_RE_BET
                  , CASE WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_draw_type THEN AC.AO_PROMO_REDEEMABLE 
                         ELSE CD_RE_WON END CD_RE_WON
		              , CASE WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_draw_type  THEN AC.AO_PROMO_NOT_REDEEMABLE 
		                     ELSE CD_NR_WON END CD_NR_WON
		              , CD_PRIZE_ID
		          FROM  CASHDESK_DRAWS WITH(INDEX(IX_CD_ACCOUNT_DATETIME))
		    INNER JOIN  CASHIER_MOVEMENTS WITH(INDEX(IX_CM_OPERATION_ID)) 
		            ON  CD_OPERATION_ID = CM_OPERATION_ID 
		    INNER JOIN  #ACCOUNTS_TEMP    
		            ON  CD_ACCOUNT_ID   = AC_ACCOUNT_ID
		    INNER JOIN  TERMINALS 
		            ON  CD_TERMINAL =  TE_TERMINAL_ID
		     LEFT JOIN  ACCOUNT_OPERATIONS AC 
		            ON  AC.AO_OPERATION_ID = CD_OPERATION_ID
		     LEFT JOIN  ACCOUNT_PROMOTIONS AP 
		            ON  AP.ACP_OPERATION_ID = CD_OPERATION_ID 
		           AND  AP.ACP_PROMO_TYPE IN (@_Promotion_Redeemeable, @_Promotion_NoRedeemeable)
  		        
		         WHERE  CD_DRAW_DATETIME >= @pDrawFrom
		           AND  CD_DRAW_DATETIME < @pDrawTo 		   
		           AND  ((@pTerminalId IS NULL) OR (TE_TERMINAL_ID = @pTerminalId))
		           AND  ((@pUserId IS NULL) OR (CM_USER_ID = @pUserId))
		           AND  ( (@pSqlAccount     IS NULL) OR (CD_ACCOUNT_ID IN ( SELECT AC_ACCOUNT_ID FROM #ACCOUNTS_TEMP ) ) )
		           AND  te_terminal_type = @_Terminal_draw_type
		      ORDER BY  CD_DRAW_ID
		 
		    DROP TABLE #ACCOUNTS_TEMP
  		    
		      RETURN
	    END
	  END --IF 	@pSqlAccount IS NOT NULL 
	
	END --IF	@pDrawId IS NOT NULL 
	--
	-- Filter by Date, but not by account neither Draw ID
	--
	IF @pDrawFrom IS NULL SET @pDrawFrom = '2007-01-01T00:00:00'
	IF @pDrawTo   IS NULL SET @pDrawTo   = GETDATE ()
	

	--Filter by Terminal draw
	IF  @pTerminalDraw = 1
	BEGIN
      SELECT  DISTINCT
	            CD_DRAW_ID
	          , CD_DRAW_DATETIME
	          , TE_TERMINAL_TYPE
	          , CM_CASHIER_NAME		  
	          , CM_USER_NAME
	          , CD_OPERATION_ID		
	          , CD_ACCOUNT_ID
	          , AC_HOLDER_NAME
	          , CD_COMBINATION_BET
	          , CD_COMBINATION_WON
	          , CD_RE_BET
            , CASE WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_draw_type THEN AC.AO_PROMO_REDEEMABLE 
                   ELSE CD_RE_WON END CD_RE_WON
            , CASE WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_draw_type  THEN AC.AO_PROMO_NOT_REDEEMABLE 
                   ELSE CD_NR_WON END CD_NR_WON
	          , CD_PRIZE_ID
	      FROM  CASHDESK_DRAWS WITH ( INDEX(IX_CD_DATETIME))	   
  INNER JOIN  CASHIER_MOVEMENTS WITH(INDEX(IX_CM_OPERATION_ID)) 
          ON  CD_OPERATION_ID = CM_OPERATION_ID
  INNER JOIN  ACCOUNTS          
          ON  CD_ACCOUNT_ID = AC_ACCOUNT_ID
  INNER JOIN  TERMINALS 
          ON  CD_TERMINAL =  TE_TERMINAL_ID
   LEFT JOIN  ACCOUNT_OPERATIONS AC 
          ON  AC.AO_OPERATION_ID = CD_OPERATION_ID
   LEFT JOIN  ACCOUNT_PROMOTIONS AP 
          ON  AP.ACP_OPERATION_ID = CD_OPERATION_ID 
         AND  AP.ACP_PROMO_TYPE IN (@_Promotion_Redeemeable, @_Promotion_NoRedeemeable)
	     WHERE  CD_DRAW_DATETIME >= @pDrawFrom
	       AND 	CD_DRAW_DATETIME < @pDrawTo	     
	       AND  ((@pTerminalId IS NULL) OR (TE_TERMINAL_ID = @pTerminalId))
	       AND 	((@pUserId IS NULL) OR (CM_USER_ID = @pUserId))
	       AND  ( (@pSqlAccount     IS NULL) OR (CD_ACCOUNT_ID IN ( SELECT AC_ACCOUNT_ID FROM #ACCOUNTS_TEMP ) ) )
         AND  te_terminal_type = @_Terminal_draw_type
    ORDER BY  CD_DRAW_ID
  
  DROP TABLE #ACCOUNTS_TEMP
	
	  RETURN
	END

END --AS BEGIN

GO

GRANT EXECUTE ON [dbo].[ReportTerminalDraws] TO [wggui] WITH GRANT OPTION 
GO


IF OBJECT_ID (N'dbo.PR_collection_by_machine_and_date', N'P') IS NOT NULL
    DROP PROCEDURE dbo.PR_collection_by_machine_and_date;                 
GO    

CREATE PROCEDURE [dbo].[PR_collection_by_machine_and_date] 
 ( @pFromDt DATETIME   
  ,@pToDt DATETIME    
  ,@pClosingTime INTEGER
  ,@pMaskStatus INTEGER
  ,@pStatusPaid INTEGER
  ,@pAll		INTEGER
  ,@pTerminalWhere NVARCHAR(MAX) 
  ,@pBillDetail BIT
  ,@pShowMetters BIT
  ,@pOnlyGroupByTerminal BIT
  ,@pIncludeJackpotRoom BIT
)
AS
BEGIN

  -- DECLARATIONS
   DECLARE @Sql AS VARCHAR(MAX)
   DECLARE @nRows AS INT                              
   DECLARE @index AS INT                              
   DECLARE @tDays AS TABLE(NumDay INT)                       
   DECLARE @Columns AS VARCHAR(MAX)
   DECLARE @ColumnChk AS VARCHAR(MAX)
   DECLARE @p2007Opening AS VARCHAR(MAX)

   SET @p2007Opening  = CAST('2007-01-01T00:00:00' AS DATETIME);
   SET @p2007Opening  = DATEADD(HOUR, @pClosingTime, @p2007Opening)

   SET @nRows  = DATEDIFF(DAY, @pFromDt, @pToDt)+1 
   SET @index = 0                                  

   WHILE @index < @nRows                           
   BEGIN	                                          
     INSERT INTO @tDays VALUES(@index)	                
     SET @index = @index + 1                       
   END 
   
    SET @Columns = ''
    SET @ColumnChk = ''

   SELECT NumDay INTO #TempTable_Days FROM @tDays 
   
   SELECT @Columns = COALESCE(@Columns + '[' + CAST(CGC_DENOMINATION AS NVARCHAR(20)) + '],', '')
     FROM (SELECT   DISTINCT CGC_DENOMINATION 
             FROM   CAGE_CURRENCIES 
            WHERE   CGC_ISO_CODE IN (SELECT   GP_KEY_VALUE 
                                       FROM   GENERAL_PARAMS 
                                      WHERE   GP_GROUP_KEY = 'RegionalOptions' 
                                        AND   GP_SUBJECT_KEY = 'CurrencyISOCode')
              AND   CGC_DENOMINATION >= 1 
              AND   CGC_ALLOWED = 1
                    UNION  
           SELECT   DISTINCT MCD_FACE_VALUE 
             FROM   MONEY_COLLECTION_DETAILS 
            INNER   JOIN MONEY_COLLECTIONS ON MCD_COLLECTION_ID = MC_COLLECTION_ID
            WHERE   MC_COLLECTION_DATETIME >= @pFromDt
              AND   MC_COLLECTION_DATETIME <  @pToDt
              AND   MC_TERMINAL_ID IS NOT NULL 
              AND   MCD_FACE_VALUE >= 1
          ) AS DTM
  ORDER BY CGC_DENOMINATION

  SET @ColumnChk = REPLACE( @Columns, ',', ' IS NOT NULL OR ')

  SET @Columns = LEFT(@Columns,LEN(@Columns)-1)
  SET @ColumnChk = LEFT(@ColumnChk,LEN(@ColumnChk)-3)
     
  SET @Sql = '
   SELECT   TE_PROVIDER_ID 
          , TE_TERMINAL_ID
          , TE_NAME 
          , ISNULL(TE_MULTI_DENOMINATION, ''--'') AS DENOMINATION '
  + CASE WHEN @pOnlyGroupByTerminal = 0 THEN '         
          , ORDER_DATE 
          , DATEADD(DAY, 1, ORDER_DATE) AS ORDER_DATE_FIN
          , COLLECTED_COIN_AMOUNT
          , TICKET_IN_COUNT
          , TI_IN_AMOUNT_RE
          , TI_IN_AMOUNT_NO_RE
          , (ISNULL(TI_IN_AMOUNT_RE,0) + ISNULL(TI_IN_AMOUNT_NO_RE, 0)) AS TI_IN_AMOUNT
          , TICKET_OUT_COUNT
          , TI_OUT_AMOUNT_RE
          , TI_OUT_AMOUNT_NO_RE
          , (ISNULL(TI_OUT_AMOUNT_RE, 0) + ISNULL(TI_OUT_AMOUNT_NO_RE, 0)) AS TI_OUT_AMOUNT
          , ( (ISNULL(TI_OUT_AMOUNT_RE, 0) + ISNULL(TI_OUT_AMOUNT_NO_RE, 0)) - (ISNULL(TI_IN_AMOUNT_RE, 0) + ISNULL(TI_IN_AMOUNT_NO_RE, 0)) ) AS NET_TITO	
          , HAND_PAYS.MANUAL
          , HAND_PAYS.CREDIT_CANCEL
          , HAND_PAYS.JACKPOT_DE_SALA
          , HAND_PAYS.PROGRESIVES
          , HAND_PAYS.NO_PROGRESIVES
          , (ISNULL(HAND_PAYS.MANUAL, 0) + ISNULL(HAND_PAYS.CREDIT_CANCEL, 0) + ISNULL(HAND_PAYS.JACKPOT_DE_SALA, 0) + ISNULL(HAND_PAYS.NO_PROGRESIVES, 0) + ISNULL(PROVISIONS.PROGRESIVE_PROVISIONS, 0)) AS HAND_PAYS_TOTAL
          , PROVISIONS.PROGRESIVE_PROVISIONS
          , (ISNULL(TI_OUT_AMOUNT_RE, 0) - ISNULL(TI_IN_AMOUNT_RE, 0) +
             ISNULL(HAND_PAYS.MANUAL, 0) + ISNULL(HAND_PAYS.CREDIT_CANCEL, 0) + ISNULL(HAND_PAYS.JACKPOT_DE_SALA, 0) + ISNULL(HAND_PAYS.NO_PROGRESIVES, 0) + ISNULL(PROVISIONS.PROGRESIVE_PROVISIONS, 0)) 
              AS TOTAL_WIN_WITHOUT_BILLS
          , 0 AS GAP_BILL
          ' + CASE WHEN @pShowMetters = 1 THEN ', METERS.TOTAL_COIN_IN_CREDITS' ELSE ', 0  AS GAP_METER_0' END + '
          ' + CASE WHEN @pShowMetters = 1 THEN ', METERS.TOTAL_COIN_OUT_CREDITS' ELSE ', 0 AS GAP_METER_1' END + '
          ' + CASE WHEN @pShowMetters = 1 THEN ', METERS.TOTAL_JACKPOT_CREDITS' ELSE ', 0 AS GAP_METER_2' END + '
          ' + CASE WHEN @pShowMetters = 1 THEN ', METERS.TOTAL_HAND_PAID_CANCELLED_CREDITS' ELSE ', 0 AS GAP_METER_3' END + '
          , BILLS.* '
     ELSE '
		  , NULL AS GAP_ORDER_DATE 
          , NULL AS GAP_ORDER_DATE_FIN
          , SUM(COLLECTED_COIN_AMOUNT) AS COLLECTED_COIN_AMOUNT
          , SUM(TICKET_IN_COUNT) AS TICKET_IN_COUNT
          , SUM(TI_IN_AMOUNT_RE) AS TI_IN_AMOUNT_RE
          , SUM(TI_IN_AMOUNT_NO_RE) AS TI_IN_AMOUNT_NO_RE
          , SUM((ISNULL(TI_IN_AMOUNT_RE,0) + ISNULL(TI_IN_AMOUNT_NO_RE, 0))) AS TI_IN_AMOUNT
          , SUM(TICKET_OUT_COUNT) AS TICKET_OUT_COUNT
          , SUM(TI_OUT_AMOUNT_RE) AS TI_OUT_AMOUNT_RE
          , SUM(TI_OUT_AMOUNT_NO_RE) AS TI_OUT_AMOUNT_NO_RE
          , SUM((ISNULL(TI_OUT_AMOUNT_RE, 0) + ISNULL(TI_OUT_AMOUNT_NO_RE, 0))) AS TI_OUT_AMOUNT
          , SUM(( (ISNULL(TI_OUT_AMOUNT_RE, 0) + ISNULL(TI_OUT_AMOUNT_NO_RE, 0)) - (ISNULL(TI_IN_AMOUNT_RE, 0) + ISNULL(TI_IN_AMOUNT_NO_RE, 0))) ) AS NET_TITO	
          , SUM(HAND_PAYS.MANUAL) AS MANUAL
          , SUM(HAND_PAYS.CREDIT_CANCEL) AS CREDIT_CANCEL
          , SUM(HAND_PAYS.JACKPOT_DE_SALA) AS JACKPOT_DE_SALA
          , SUM(HAND_PAYS.PROGRESIVES) AS PROGRESIVES
          , SUM(HAND_PAYS.NO_PROGRESIVES) AS NO_PROGRESIVES
          , SUM((ISNULL(HAND_PAYS.MANUAL, 0) + ISNULL(HAND_PAYS.CREDIT_CANCEL, 0) + ISNULL(HAND_PAYS.JACKPOT_DE_SALA, 0) + ISNULL(HAND_PAYS.NO_PROGRESIVES, 0) + ISNULL(PROVISIONS.PROGRESIVE_PROVISIONS, 0))) AS HAND_PAYS_TOTAL
          , SUM(PROVISIONS.PROGRESIVE_PROVISIONS) AS PROGRESIVE_PROVISIONS
          , SUM((ISNULL(TI_OUT_AMOUNT_RE, 0) - ISNULL(TI_IN_AMOUNT_RE, 0) +
             ISNULL(HAND_PAYS.MANUAL, 0) + ISNULL(HAND_PAYS.CREDIT_CANCEL, 0) + ISNULL(HAND_PAYS.JACKPOT_DE_SALA, 0) + ISNULL(HAND_PAYS.NO_PROGRESIVES, 0) + ISNULL(PROVISIONS.PROGRESIVE_PROVISIONS, 0))) 
              AS TOTAL_WIN_WITHOUT_BILLS
          , 0 AS GAP_BILL
          ' + CASE WHEN @pShowMetters = 1 THEN ', SUM(METERS.TOTAL_COIN_IN_CREDITS) AS TOTAL_COIN_IN_CREDITS' ELSE ', 0  AS GAP_METER_0' END + '
          ' + CASE WHEN @pShowMetters = 1 THEN ', SUM(METERS.TOTAL_COIN_OUT_CREDITS) AS TOTAL_COIN_OUT_CREDITS' ELSE ', 0 AS GAP_METER_1' END + '
          ' + CASE WHEN @pShowMetters = 1 THEN ', SUM(METERS.TOTAL_JACKPOT_CREDITS) AS TOTAL_JACKPOT_CREDITS' ELSE ', 0 AS GAP_METER_2' END + '
          ' + CASE WHEN @pShowMetters = 1 THEN ', SUM(METERS.TOTAL_HAND_PAID_CANCELLED_CREDITS) AS TOTAL_HAND_PAID_CANCELLED_CREDITS' ELSE ', 0 AS GAP_METER_3' END + '
          , NULL AS GAP_BILL_MC_TERMINAL_ID
          , NULL AS GAP_BILL_MC_COLLECTION_DATETIME
          , SUM(BILLS.MCD_FACE_VALUE)
          , SUM(BILLS.COLLECTED) '
     END + '
     FROM   TERMINALS 
 LEFT JOIN (SELECT   DATEADD(DAY, NumDay, CAST(''' + CAST(@pFromDt AS VARCHAR(50)) + ''' AS DATETIME)) AS ORDER_DATE FROM #TempTable_Days ) DIA ON ORDER_DATE <= GETDATE() 
 LEFT JOIN (SELECT   TC_TERMINAL_ID 
                   , DATEADD(HOUR, ' + CAST(@pClosingTime AS VARCHAR(50)) + ', TC_DATE) AS TC_DATE
              FROM   TERMINALS_CONNECTED 
             WHERE   TC_DATE >= dateadd(day, datediff(day, 0, CAST(''' + CAST(@pFromDt AS VARCHAR(50)) + ''' AS DATETIME)), 0)
               AND   TC_DATE <  dateadd(day, datediff(day, 0, CAST(''' + CAST(@pToDt AS VARCHAR(50)) + ''' AS DATETIME)), 0)
               AND   TC_STATUS = 0
               AND   TC_CONNECTED = 1) TC ON TC_TERMINAL_ID = TE_TERMINAL_ID AND TC_DATE = DIA.ORDER_DATE 
 LEFT JOIN (SELECT   MC_TERMINAL_ID AS MC_TERMINAL_ID_C
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), CGS_WORKING_DAY)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS MC_COLLECTION_DATETIME_C
                   , SUM(MC_COLLECTED_COIN_AMOUNT) AS COLLECTED_COIN_AMOUNT
              FROM   MONEY_COLLECTIONS
        INNER JOIN   CAGE_MOVEMENTS ON CGM_MC_COLLECTION_ID = MC_COLLECTION_ID
        INNER JOIN   CAGE_SESSIONS ON CGS_CAGE_SESSION_ID = CGM_CAGE_SESSION_ID
             WHERE   CGS_WORKING_DAY >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
               AND   CGS_WORKING_DAY <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
               AND   MC_TERMINAL_ID IS NOT NULL
             GROUP   BY MC_TERMINAL_ID
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), CGS_WORKING_DAY)/24, CAST(''' + @p2007Opening + ''' AS DATETIME))
           ) CASH_IN_COINS ON TE_TERMINAL_ID = MC_TERMINAL_ID_C AND MC_COLLECTION_DATETIME_C = DIA.ORDER_DATE 
 LEFT JOIN (SELECT   MC_TERMINAL_ID AS MC_TERMINAL_ID_T
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), CGS_WORKING_DAY)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS MC_COLLECTION_DATETIME_T
                   , SUM(MC_EXPECTED_TICKET_COUNT) AS TICKET_IN_COUNT
                   , SUM(ISNULL(MC_EXPECTED_RE_TICKET_AMOUNT, 0)+ISNULL(MC_EXPECTED_PROMO_RE_TICKET_AMOUNT, 0)) AS TI_IN_AMOUNT_RE
                   , SUM(ISNULL(MC_EXPECTED_PROMO_NR_TICKET_AMOUNT, 0)) AS TI_IN_AMOUNT_NO_RE
              FROM   MONEY_COLLECTIONS
        INNER JOIN   CAGE_MOVEMENTS ON CGM_MC_COLLECTION_ID = MC_COLLECTION_ID
        INNER JOIN   CAGE_SESSIONS ON CGS_CAGE_SESSION_ID = CGM_CAGE_SESSION_ID
             WHERE   CGS_WORKING_DAY >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
               AND   CGS_WORKING_DAY <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
               AND   MC_TERMINAL_ID IS NOT NULL
             GROUP   BY MC_TERMINAL_ID
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), CGS_WORKING_DAY)/24, CAST(''' + @p2007Opening + ''' AS DATETIME))
           ) TICKETS_IN ON TE_TERMINAL_ID = MC_TERMINAL_ID_T AND MC_COLLECTION_DATETIME_T = DIA.ORDER_DATE 
LEFT JOIN ( SELECT   TI_CREATED_TERMINAL_ID
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), TI_CREATED_DATETIME)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS TI_CREATED_DATETIME
                   , COUNT(1) AS TICKET_OUT_COUNT
                   , SUM(CASE WHEN TI_TYPE_ID = 2 OR TI_STATUS = 6 THEN 0 ELSE TI_AMOUNT END) AS TI_OUT_AMOUNT_RE
                   , SUM(CASE WHEN TI_TYPE_ID = 2 THEN TI_AMOUNT ELSE 0 END) AS TI_OUT_AMOUNT_NO_RE
              FROM   TICKETS 
             WHERE   TI_CREATED_DATETIME >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
               AND   TI_CREATED_DATETIME <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
               AND   TI_CREATED_TERMINAL_TYPE = 1     -- TITO_TERMINAL_TYPE.TERMINAL = 1              
               AND   TI_TYPE_ID IN (0,1,2,5)    --TITO_TICKET_TYPE
                                                --CASHABLE = 0,
                                                --PROMO_REDEEM = 1,
                                                --PROMO_NONREDEEM = 2,  // only playable
                                                --HANDPAY = 3,
                                                --JACKPOT = 4,
                                                --OFFLINE = 5
             GROUP   BY TI_CREATED_TERMINAL_ID
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), TI_CREATED_DATETIME)/24, CAST(''' + @p2007Opening + ''' AS DATETIME))
          ) TICKETS_OUT ON TE_TERMINAL_ID = TI_CREATED_TERMINAL_ID AND TI_CREATED_DATETIME = DIA.ORDER_DATE
 LEFT JOIN (SELECT   (CASE WHEN HP_TYPE = 20 THEN HP_SITE_JACKPOT_AWARDED_ON_TERMINAL_ID ELSE HP_TERMINAL_ID END) AS HP_TERMINAL_ID 
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), ISNULL(HP_STATUS_CHANGED, CASE WHEN (HP_TYPE IN ( 20)) THEN HP_DATETIME ELSE NULL END))/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS HP_DATETIME
                   
                   , SUM(CASE WHEN (HP_TYPE IN (10, 1010)) THEN ISNULL(dbo.GetDualCurrencyAmount(HP_AMOUNT, HP_AMT0, ' + CAST(@pShowMetters AS varchar(1) ) + '), 0) ELSE 0 END) AS MANUAL 
                   
                   , SUM(CASE WHEN (HP_TYPE IN ( 0, 1000 )) THEN ISNULL(dbo.GetDualCurrencyAmount(HP_AMOUNT, HP_AMT0, ' + CAST(@pShowMetters AS varchar(1) ) + '), 0) ELSE 0 END) +
                     SUM(CASE WHEN (HP_TYPE IN ( 1, 1001 ) AND (HP_LEVEL>=1 AND HP_LEVEL<=32) ) THEN 
                         CASE WHEN (HP_TAX_BASE_AMOUNT IS NULL) THEN 0 ELSE ISNULL(dbo.GetDualCurrencyAmount(HP_AMOUNT, HP_AMT0, ' + CAST(@pShowMetters AS varchar(1) ) + '), 0) - ISNULL(HP_TAX_BASE_AMOUNT, 0) END ELSE 0 END) AS CREDIT_CANCEL
'                          
  IF @pIncludeJackpotRoom = 1 
  BEGIN
    SET @Sql =  @Sql + ', SUM(CASE WHEN (HP_TYPE IN ( 20)) THEN ISNULL(dbo.GetDualCurrencyAmount(HP_AMOUNT, HP_AMT0, ' + CAST(@pShowMetters AS varchar(1) ) + '), 0) ELSE 0 END) AS JACKPOT_DE_SALA  '
  END
  ELSE
  BEGIN
    SET @Sql =  @Sql + ', 0 AS JACKPOT_DE_SALA '
  END
  SET @Sql =  @Sql +
  '                 , SUM(CASE WHEN (HP_TYPE IN ( 1, 1001 ) AND (HP_LEVEL>=1 AND HP_LEVEL<=32) AND HP_PROGRESSIVE_ID IS NOT NULL) THEN 
                         CASE WHEN (HP_TAX_BASE_AMOUNT IS NULL) THEN ISNULL(dbo.GetDualCurrencyAmount(HP_AMOUNT, HP_AMT0, ' + CAST(@pShowMetters AS varchar(1) ) + '), 0) ELSE ISNULL(HP_TAX_BASE_AMOUNT, 0) END ELSE 0 END) AS PROGRESIVES 
                         
                   , SUM(CASE WHEN (HP_TYPE IN ( 1, 1001 ) AND (HP_LEVEL <1 OR  HP_LEVEL >32 OR HP_LEVEL IS NULL)) THEN ISNULL(dbo.GetDualCurrencyAmount(HP_AMOUNT, HP_AMT0, ' + CAST(@pShowMetters AS varchar(1) ) + '), 0) ELSE 0 END) + 
                     SUM(CASE WHEN (HP_TYPE IN ( 1, 1001 ) AND (HP_LEVEL>=1 AND HP_LEVEL <=32 AND HP_PROGRESSIVE_ID IS NULL)) THEN 
                         CASE WHEN (HP_TAX_BASE_AMOUNT IS NULL) THEN ISNULL(dbo.GetDualCurrencyAmount(HP_AMOUNT, HP_AMT0, ' + CAST(@pShowMetters AS varchar(1) ) + '), 0) ELSE ISNULL(HP_TAX_BASE_AMOUNT, 0) END ELSE 0 END) AS NO_PROGRESIVES
                   
              FROM   HANDPAYS WITH (INDEX (IX_hp_status_changed))
             WHERE   ' + CASE WHEN @pAll = 0 THEN 'HP_STATUS & ' + CAST(@pMaskStatus AS VARCHAR(10)) + ' = ' + CAST(@pStatusPaid AS VARCHAR(10)) + ' AND ' ELSE + ' ' END +
            '  ISNULL(HP_STATUS_CHANGED, CASE WHEN (HP_TYPE IN ( 20)) THEN HP_DATETIME ELSE NULL END) >= CAST(''' + CAST(@pFromDt AS VARCHAR(50)) + ''' AS DATETIME) 
               AND   ISNULL(HP_STATUS_CHANGED, CASE WHEN (HP_TYPE IN ( 20)) THEN HP_DATETIME ELSE NULL END) < CAST(''' + CAST(@pToDt AS VARCHAR(50)) + ''' AS DATETIME) 
             GROUP   BY (CASE WHEN HP_TYPE = 20 THEN HP_SITE_JACKPOT_AWARDED_ON_TERMINAL_ID ELSE HP_TERMINAL_ID END) 
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), ISNULL(HP_STATUS_CHANGED, CASE WHEN (HP_TYPE IN ( 20)) THEN HP_DATETIME ELSE NULL END))/24, CAST(''' + @p2007Opening + ''' AS DATETIME))
                   ) HAND_PAYS ON HP_TERMINAL_ID = TE_TERMINAL_ID AND HP_DATETIME = DIA.ORDER_DATE
 LEFT JOIN (SELECT   PPT_TERMINAL_ID 
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), PGP_HOUR_FROM)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS PGP_HOUR_FROM
                   , SUM(ISNULL(PPT_AMOUNT, 0)) AS PROGRESIVE_PROVISIONS 
             FROM    PROGRESSIVES_PROVISIONS_TERMINALS 
        LEFT JOIN    PROGRESSIVES_PROVISIONS ON PGP_PROVISION_ID = PPT_PROVISION_ID 
                                            AND PGP_PROGRESSIVE_ID = PPT_PROGRESSIVE_ID 
                                            AND PGP_HOUR_FROM >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
                                            AND PGP_HOUR_FROM <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
                                            AND PGP_STATUS = 0
             GROUP   BY PPT_TERMINAL_ID 
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), PGP_HOUR_FROM)/24, CAST(''' + @p2007Opening + ''' AS DATETIME))
                   ) PROVISIONS ON PPT_TERMINAL_ID = TE_TERMINAL_ID AND PGP_HOUR_FROM = DIA.ORDER_DATE ' 
IF @pBillDetail = 1
BEGIN
  SET @Sql =  @Sql +
'LEFT JOIN (SELECT   *  
              FROM ( SELECT   MC_TERMINAL_ID
                            , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), CGS_WORKING_DAY)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS MC_COLLECTION_DATETIME
                            , MCD_FACE_VALUE
                            , (MCD_NUM_COLLECTED * MCD_FACE_VALUE) AS COLLECTED
                       FROM   MONEY_COLLECTION_DETAILS 
                 INNER JOIN   MONEY_COLLECTIONS ON MCD_COLLECTION_ID = MC_COLLECTION_ID
                 INNER JOIN   CAGE_MOVEMENTS ON CGM_MC_COLLECTION_ID = MC_COLLECTION_ID
                 INNER JOIN   CAGE_SESSIONS ON CGS_CAGE_SESSION_ID = CGM_CAGE_SESSION_ID
                      WHERE   CGS_WORKING_DAY >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
                        AND   CGS_WORKING_DAY <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
                        AND   MC_TERMINAL_ID IS NOT NULL 
                        AND   MCD_CAGE_CURRENCY_TYPE = 0
                        AND   MCD_FACE_VALUE > 0
                   ) PIV
             PIVOT (  SUM(COLLECTED) FOR MCD_FACE_VALUE IN ('+ @Columns  + ')) AS CHILD
            ) BILLS ON TE_TERMINAL_ID = MC_TERMINAL_ID AND MC_COLLECTION_DATETIME = DIA.ORDER_DATE '
END
ELSE
BEGIN
  SET @Sql =  @Sql +
'LEFT JOIN (SELECT   MC_TERMINAL_ID
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), CGS_WORKING_DAY)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS MC_COLLECTION_DATETIME
                   , 0 AS MCD_FACE_VALUE
                   , SUM(MCD_NUM_COLLECTED * MCD_FACE_VALUE) AS COLLECTED
              FROM   MONEY_COLLECTION_DETAILS 
        INNER JOIN   MONEY_COLLECTIONS ON MCD_COLLECTION_ID = MC_COLLECTION_ID
        INNER JOIN   CAGE_MOVEMENTS ON CGM_MC_COLLECTION_ID = MC_COLLECTION_ID
        INNER JOIN   CAGE_SESSIONS ON CGS_CAGE_SESSION_ID = CGM_CAGE_SESSION_ID
             WHERE   CGS_WORKING_DAY >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
               AND   CGS_WORKING_DAY <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
               AND   MC_TERMINAL_ID IS NOT NULL
               AND   MCD_CAGE_CURRENCY_TYPE = 0
               AND   MCD_FACE_VALUE > 0
             GROUP   BY MC_TERMINAL_ID
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), CGS_WORKING_DAY)/24, CAST(''' + @p2007Opening + ''' AS DATETIME))
          ) BILLS ON TE_TERMINAL_ID = MC_TERMINAL_ID AND MC_COLLECTION_DATETIME = DIA.ORDER_DATE '
END


IF @pShowMetters = 1
BEGIN
SET @Sql =  @Sql +
'LEFT JOIN (SELECT TSMH_TERMINAL_ID
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), TSMH_DATETIME)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS TSMH_DATE
                   , SUM(CASE WHEN TSMH_METER_CODE = 0 THEN ISNULL(CAST(TSMH_METER_INCREMENT AS MONEY)/100, 0) END) AS TOTAL_COIN_IN_CREDITS
                   , SUM(CASE WHEN TSMH_METER_CODE = 1 THEN ISNULL(CAST(TSMH_METER_INCREMENT AS MONEY)/100, 0) END) AS TOTAL_COIN_OUT_CREDITS
                   , SUM(CASE WHEN TSMH_METER_CODE = 2 THEN ISNULL(CAST(TSMH_METER_INCREMENT AS MONEY)/100, 0) END) AS TOTAL_JACKPOT_CREDITS
                   , SUM(CASE WHEN TSMH_METER_CODE = 3 THEN ISNULL(CAST(TSMH_METER_INCREMENT AS MONEY)/100, 0) END) AS TOTAL_HAND_PAID_CANCELLED_CREDITS
			 FROM    TERMINAL_SAS_METERS_HISTORY
			WHERE    TSMH_TYPE = 1
			  AND    TSMH_METER_CODE IN (0,1,2,3)
			  AND    TSMH_METER_INCREMENT > 0 
			  AND    TSMH_DATETIME >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
              AND    TSMH_DATETIME <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
            GROUP    BY TSMH_TERMINAL_ID
				   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), TSMH_DATETIME)/24, CAST(''' + @p2007Opening + ''' AS DATETIME))
	   ) METERS ON TE_TERMINAL_ID = TSMH_TERMINAL_ID AND TSMH_DATE = DIA.ORDER_DATE '
END

SET @Sql =  @Sql + 'WHERE ( ' 

IF @pBillDetail = 1
BEGIN
  SET @Sql =  @Sql + @ColumnChk 
END
ELSE
BEGIN
  SET @Sql =  @Sql + ' BILLS.COLLECTED IS NOT NULL ' 
END

SET @Sql =  @Sql +
  ' OR TICKET_IN_COUNT IS NOT NULL
    OR TI_IN_AMOUNT_RE IS NOT NULL
    OR TI_IN_AMOUNT_NO_RE IS NOT NULL
    OR TICKET_OUT_COUNT IS NOT NULL
    OR TI_OUT_AMOUNT_RE IS NOT NULL
    OR TI_OUT_AMOUNT_NO_RE IS NOT NULL
    OR HAND_PAYS.MANUAL IS NOT NULL
    OR HAND_PAYS.CREDIT_CANCEL IS NOT NULL
    OR HAND_PAYS.JACKPOT_DE_SALA IS NOT NULL
    OR HAND_PAYS.PROGRESIVES IS NOT NULL
    OR HAND_PAYS.NO_PROGRESIVES IS NOT NULL
    OR PROVISIONS.PROGRESIVE_PROVISIONS IS NOT NULL' +
	CASE WHEN @pShowMetters = 1 THEN '
		OR METERS.TOTAL_COIN_IN_CREDITS IS NOT NULL
		OR METERS.TOTAL_COIN_OUT_CREDITS IS NOT NULL
		OR METERS.TOTAL_JACKPOT_CREDITS IS NOT NULL
		OR METERS.TOTAL_HAND_PAID_CANCELLED_CREDITS IS NOT NULL'
    ELSE '' END + '
    OR TC_DATE IS NOT NULL
    )' + CAST(@pTerminalWhere AS Varchar(max)) +
CASE WHEN @pOnlyGroupByTerminal = 1 THEN 
' GROUP   BY TE_PROVIDER_ID 
        , TE_TERMINAL_ID
        , TE_NAME 
        , ISNULL(TE_MULTI_DENOMINATION, ''--'') '
ELSE '' END +
' ORDER   BY TE_PROVIDER_ID 
        , TE_TERMINAL_ID 
        , TE_NAME 
        , ISNULL(TE_MULTI_DENOMINATION, ''--'') ' +
        
CASE WHEN @pOnlyGroupByTerminal = 0 THEN 
        ', ORDER_DATE ' 
ELSE '' END

 -- PRINT @Sql

  EXECUTE (@Sql)

  DROP TABLE #TempTable_Days

END -- END PROCEDURE
GO

-- PERMISSIONS

GRANT EXECUTE ON [dbo].[PR_collection_by_machine_and_date] TO [wggui] WITH GRANT OPTION
GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CageGetGlobalReportData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CageGetGlobalReportData]
GO

CREATE PROCEDURE [dbo].[CageGetGlobalReportData]
        @pCageSessionIds NVARCHAR(MAX) = NULL
AS
BEGIN
  
  DECLARE @NationalIsoCode NVARCHAR(3)
  DECLARE @CageOperationType_FromTerminal INT
  DECLARE @TicketsId INT
  DECLARE @BillsId INT
  DECLARE @Currencies AS NVARCHAR(200)
  DECLARE @CountSessions INT
  DECLARE @List VARCHAR(MAX)
  DECLARE @StockIsStored BIT
  DECLARE @CageCurrencyType_ColorChips INT

  SELECT @NationalIsoCode = GP_KEY_VALUE 
    FROM GENERAL_PARAMS 
   WHERE GP_GROUP_KEY = 'RegionalOptions' 
     AND GP_SUBJECT_KEY = 'CurrencyISOCode'

  SELECT @Currencies = GP_KEY_VALUE 
    FROM GENERAL_PARAMS 
   WHERE GP_GROUP_KEY = 'RegionalOptions' 
     AND GP_SUBJECT_KEY = 'CurrenciesAccepted'

  -- Split currencies ISO codes
  SELECT   CURRENCY_ISO_CODE INTO #TMP_CURRENCIES_ISO_CODES 
    FROM ( SELECT   SST_VALUE AS CURRENCY_ISO_CODE FROM [SplitStringIntoTable] (@Currencies, ';', 1)
            UNION ALL 
           SELECT   'X01' AS CURRENCY_ISO_CODE    -- for retrocompatibility
            UNION ALL 
           SELECT   'X02' AS CURRENCY_ISO_CODE ) AS CURRENCIES
               
  SET @CageOperationType_FromTerminal = 104
  SET @TicketsId = -200
  SET @BillsId = 0
  SET @CageCurrencyType_ColorChips = 1003
   
  -- Session IDs Split
  SELECT SST_VALUE AS CAGE_SESSION_ID INTO #TMP_CAGE_SESSIONS FROM [SplitStringIntoTable] (@pCageSessionIds,','  , 1)
  SET @CountSessions = (SELECT COUNT(*) FROM #TMP_CAGE_SESSIONS )
  
  IF @CountSessions = 1 
    SELECT @List = CGS_CAGE_STOCK FROM CAGE_SESSIONS 
     WHERE CGS_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)

  -- TABLE[0]: Get cage stock 
  IF (@CountSessions = 1 AND @List IS NOT NULL) 
  BEGIN
    --Get stock on close session
    SET @StockIsStored =  1
    SELECT SST_VALUE AS STOCK_ROW INTO #TMP_STOCK_ROWS FROM [SplitStringIntoTable] (@List, '|', 1)
    
    -- LTC 2017-JUN-20
    UPDATE #TMP_STOCK_ROWS 
    SET STOCK_ROW = CASE WHEN (LEN(STOCK_ROW) - LEN(REPLACE(STOCK_ROW, ';',''))) = 3 THEN STOCK_ROW + ';' + '-1' ELSE STOCK_ROW END
    FROM #TMP_STOCK_ROWS

    SELECT left(STOCK_ROW, 3) AS CGS_ISO_CODE,                        
       CAST(REPLACE(PARSENAME(REPLACE(REPLACE(RIGHT(STOCK_ROW, LEN(STOCK_ROW)-4),'.', '|'), ';', '.'), 4), '|', '.') AS MONEY) AS CGS_DENOMINATION,   
       CAST(REPLACE(PARSENAME(REPLACE(REPLACE(RIGHT(STOCK_ROW, LEN(STOCK_ROW)-4), '.', '|'), ';', '.'), 3), '|', '.') AS DECIMAL(15,2)) AS CGS_QUANTITY,       
       CAST(REPLACE(PARSENAME(REPLACE(REPLACE(RIGHT(STOCK_ROW, LEN(STOCK_ROW)-4), '.', '|'), ';', '.'), 2), '|', '.') AS INT) AS CGS_CAGE_CURRENCY_TYPE,  
       CAST(REPLACE(PARSENAME(REPLACE(REPLACE(RIGHT(STOCK_ROW, LEN(STOCK_ROW)-4), '.', '|'), ';', '.'), 1), '|', '.') AS INT) AS CGS_CHIP_ID, 
       CH_NAME AS NAME, CH_DRAWING AS DRAWING
       INTO #TMP_STOCK                                                                                                               
    FROM #TMP_STOCK_ROWS 
    LEFT JOIN chips ON ch_chip_id = CAST(REPLACE(PARSENAME(REPLACE(REPLACE(RIGHT(STOCK_ROW, LEN(STOCK_ROW)-4), '.', '|'), ';', '.'), 1), '|', '.') AS INT)

    SELECT   A.*
           , CE_CURRENCY_ORDER 
      FROM   #TMP_STOCK AS A
      LEFT   JOIN CURRENCY_EXCHANGE ON CGS_ISO_CODE = CE_CURRENCY_ISO_CODE AND CE_TYPE = 0
  ORDER BY   CE_CURRENCY_ORDER
           , CGS_ISO_CODE
           , CASE WHEN CGS_CAGE_CURRENCY_TYPE < 1000 THEN 0
                  WHEN CGS_CAGE_CURRENCY_TYPE = 1003 THEN 9999999
                  ELSE 1 END
           , CGS_DENOMINATION DESC
           , CASE WHEN CGS_DENOMINATION >= 0 THEN CGS_DENOMINATION 
                  ELSE CGS_DENOMINATION * (-100000) END                                                                    

    DROP TABLE #TMP_STOCK_ROWS                                                                                                      
    DROP TABLE #TMP_STOCK     
    
  END --IF (@CountSessions = 1 AND @List IS NOT NULL)    
  ELSE --IF (@CountSessions = 1 AND @List IS NOT NULL) 
  BEGIN
    -- Get current stock
    SET @StockIsStored =  0
    EXEC CageCurrentStock
  END --ELSE
  
  IF @pCageSessionIds IS NOT NULL
  BEGIN
      
    -- TABLE[1]: Get cage sessions information
    SELECT  a.*
          , (ROW_NUMBER() OVER(ORDER BY CM_SOURCE_TARGET_ID
          , CASE CM_CONCEPT_ID WHEN 0 THEN '000' ELSE CC_DESCRIPTION END
          , CASE WHEN CAGE_CURRENCY_TYPE < 1000 THEN 0
                 WHEN CAGE_CURRENCY_TYPE = 1003 THEN 9999999
                 ELSE 1 END
          , CE_CURRENCY_ORDER
          , CM_ISO_CODE
          , CAGE_CURRENCY_TYPE)) as ORDEN
    FROM (
    SELECT CSM.CSM_SOURCE_TARGET_ID AS CM_SOURCE_TARGET_ID
         , CSM.CSM_CONCEPT_ID AS CM_CONCEPT_ID
         , CASE CSM.CSM_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CSM.CSM_ISO_CODE END AS CM_ISO_CODE
         , CASE CSM.CSM_ISO_CODE WHEN 'X01' THEN 1001 ELSE CASE WHEN CSM_CAGE_CURRENCY_TYPE < 1000 THEN 0 ELSE CSM_CAGE_CURRENCY_TYPE END END AS CAGE_CURRENCY_TYPE
         , SUM(CSM.CSM_VALUE_IN) AS CM_VALUE_IN
         , SUM(CSM.CSM_VALUE_OUT) AS CM_VALUE_OUT
         , CST.CST_SOURCE_TARGET_NAME
         , CC.CC_DESCRIPTION
      FROM CAGE_SESSION_METERS AS CSM
           INNER JOIN CAGE_SOURCE_TARGET_CONCEPTS AS CSTC ON CSTC.CSTC_CONCEPT_ID = CSM.CSM_CONCEPT_ID AND CSTC.CSTC_SOURCE_TARGET_ID = CSM.CSM_SOURCE_TARGET_ID
           INNER JOIN CAGE_CONCEPTS AS CC ON CC.CC_CONCEPT_ID = CSTC.CSTC_CONCEPT_ID
           INNER JOIN CAGE_SOURCE_TARGET AS CST ON CST.CST_SOURCE_TARGET_ID = CSTC.CSTC_SOURCE_TARGET_ID
     WHERE CSM.CSM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
       AND CC.CC_SHOW_IN_REPORT = 1
       AND CC.CC_ENABLED = 1
       AND (CC.CC_TYPE = 1 OR CC_CONCEPT_ID IN (9, 10,11))
       AND CSTC.CSTC_ENABLED = 1
       AND CSM_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)
  GROUP BY CSM.CSM_SOURCE_TARGET_ID
         , CSM.CSM_CONCEPT_ID
         , CASE CSM.CSM_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CSM.CSM_ISO_CODE END 
         , CASE CSM.CSM_ISO_CODE WHEN 'X01' THEN 1001 ELSE CASE WHEN CSM_CAGE_CURRENCY_TYPE < 1000 THEN 0 ELSE CSM_CAGE_CURRENCY_TYPE END END  
         , CST.CST_SOURCE_TARGET_NAME
         , CC.CC_DESCRIPTION
         ) as a
    LEFT  JOIN CURRENCY_EXCHANGE ON CM_ISO_CODE = CE_CURRENCY_ISO_CODE AND CE_TYPE = 0
  ORDER BY CM_SOURCE_TARGET_ID
         , CASE CM_CONCEPT_ID WHEN 0 THEN '000' ELSE CC_DESCRIPTION END
         , CASE WHEN CAGE_CURRENCY_TYPE < 1000 THEN 0
                WHEN CAGE_CURRENCY_TYPE = 1003 THEN 9999999
                ELSE 1 END
         , CE_CURRENCY_ORDER
         , CM_ISO_CODE
         , CAGE_CURRENCY_TYPE 
         
     -- TABLE[2]: Get dynamic liabilities
    SELECT CC.CC_CONCEPT_ID AS LIABILITY_CONCEPT_ID
         , CASE CC_CONCEPT_ID WHEN 0 THEN '000' ELSE CC_DESCRIPTION END AS LIABILITY_NAME 
         , CASE CSM_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CSM_ISO_CODE END AS LIABILITY_ISO_CODE
         , CASE WHEN CASE CSM_ISO_CODE WHEN 'X01' THEN 1001 ELSE CSM_CAGE_CURRENCY_TYPE END < 1000 THEN 0
                ELSE CSM_CAGE_CURRENCY_TYPE END   AS LIABILITY_CAGE_CURRENCY_TYPE
         , SUM(CSM.CSM_VALUE) AS LIABILITY_VALUE
      FROM CAGE_SESSION_METERS AS CSM
     INNER JOIN CAGE_CONCEPTS AS CC ON CC.CC_CONCEPT_ID = CSM.CSM_CONCEPT_ID
      LEFT JOIN CURRENCY_EXCHANGE ON CSM_ISO_CODE = CE_CURRENCY_ISO_CODE AND CE_TYPE = 0
     WHERE CC.CC_ENABLED = 1
       AND CC.CC_SHOW_IN_REPORT = 1
       AND CC.CC_IS_PROVISION = 1
       AND CSM.CSM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
       AND CSM_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)
  GROUP BY CC.CC_CONCEPT_ID  
         , CASE CC_CONCEPT_ID WHEN 0 THEN '000' ELSE CC_DESCRIPTION END
         , CASE WHEN CASE CSM_ISO_CODE WHEN 'X01' THEN 1001 ELSE CSM_CAGE_CURRENCY_TYPE END < 1000 THEN 0
                ELSE CSM_CAGE_CURRENCY_TYPE END
         , CE_CURRENCY_ORDER
         , CASE CSM_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CSM_ISO_CODE END
  ORDER BY CC.CC_CONCEPT_ID  
         , CASE CC_CONCEPT_ID WHEN 0 THEN '000' ELSE CC_DESCRIPTION END
         , CASE WHEN CASE CSM_ISO_CODE WHEN 'X01' THEN 1001 ELSE CSM_CAGE_CURRENCY_TYPE END < 1000 THEN 0
                ELSE CSM_CAGE_CURRENCY_TYPE END
         , CE_CURRENCY_ORDER
         , CASE CSM_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CSM_ISO_CODE END
      
    -- TEMP TABLE[3]: Get session sumary
    SELECT CMD_ISO_CODE AS ISO_CODE
         , CASE WHEN CAGE_CURRENCY_TYPE = 99 THEN 0 ELSE CAGE_CURRENCY_TYPE END AS CAGE_CURRENCY_TYPE
         , ISNULL(ORIGIN, 0) AS ORIGIN
         , SUM(CMD_DEPOSITS) AS DEPOSITS
         , SUM(CMD_WITHDRAWALS) AS WITHDRAWALS
		 INTO #TABLE_TEMP_3
      FROM (SELECT CASE CMD_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CMD_ISO_CODE END AS CMD_ISO_CODE
                 , CASE WHEN CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE CAGE_CURRENCY_TYPE END < 1000 THEN 0
                        ELSE CAGE_CURRENCY_TYPE END AS CAGE_CURRENCY_TYPE
                 , ORIGIN
                 , SUM(CMD_DEPOSITS) CMD_DEPOSITS
                 , SUM(CMD_WITHDRAWALS) CMD_WITHDRAWALS      
              FROM (SELECT CASE WHEN CMD_QUANTITY >= 0 THEN @BillsId
                                ELSE CMD_QUANTITY END  AS ORIGIN
                         , CASE CMD_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CMD_ISO_CODE END AS CMD_ISO_CODE
                         , CASE WHEN CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE CMD_CAGE_CURRENCY_TYPE END < 1000 THEN 0
                                ELSE CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE ISNULL(CMD_CAGE_CURRENCY_TYPE, 0) END END  AS CAGE_CURRENCY_TYPE
                         , CMD_DENOMINATION
                         , CASE WHEN CGM_TYPE IN (100, 101, 103, 104) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY < 0 AND CMD_QUANTITY <> @TicketsId THEN CMD_DENOMINATION
                                                                                            WHEN CMD_QUANTITY = @TicketsId THEN 0
                                                                                            WHEN CMD_CAGE_CURRENCY_TYPE = @CageCurrencyType_ColorChips THEN CMD_QUANTITY
                                                                                       ELSE CMD_QUANTITY * CMD_DENOMINATION END), 0)
                                ELSE 0 END AS CMD_DEPOSITS
                         , CASE WHEN CGM_TYPE IN (0, 1, 4, 5) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY < 0 AND CMD_QUANTITY <> @TicketsId THEN CMD_DENOMINATION
                                                                                    WHEN CMD_QUANTITY = @TicketsId THEN 0
                                                                                    WHEN CMD_CAGE_CURRENCY_TYPE = @CageCurrencyType_ColorChips THEN CMD_QUANTITY
                                                                               ELSE CMD_QUANTITY * CMD_DENOMINATION END), 0)
                                ELSE 0 END AS CMD_WITHDRAWALS
                      FROM CAGE_MOVEMENTS 
                INNER JOIN CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          
                     WHERE CGM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
                       AND (CGM_STATUS IN (2, 3, 4, 8, 9, 10) 
                            OR (CGM_STATUS = 0 AND CGM_TYPE = 0))
                  GROUP BY CASE WHEN CMD_QUANTITY >= 0 THEN @BillsId
                                ELSE CMD_QUANTITY END  
                         , CASE CMD_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CMD_ISO_CODE END 
                         , CASE WHEN CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE CMD_CAGE_CURRENCY_TYPE END < 1000 THEN 0
                                ELSE CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE ISNULL(CMD_CAGE_CURRENCY_TYPE, 0) END END 
                         , CMD_DENOMINATION
                         , CGM_TYPE 
                  -- Tickets 
                  UNION ALL
                  
                    SELECT @TicketsId AS ORIGIN
                         , ISNULL(TI_CUR0, @NationalIsoCode) AS CMD_ISO_CODE 
                         , ISNULL(CMD_CAGE_CURRENCY_TYPE, 0) AS CAGE_CURRENCY_TYPE                                    
                         , 1 AS CMD_DENOMINATION
                         , CASE WHEN CGM_TYPE IN (100, 101, 103, 104) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY = @TicketsId THEN 
                                                                                                 CASE WHEN TI_AMT0 IS NULL THEN TI_AMOUNT 
                                                                                                      ELSE TI_AMT0 END
                                                                                       ELSE 0 END), 0)
                                ELSE 0 END AS CMD_DEPOSITS
                         , CASE WHEN CGM_TYPE IN (0, 1, 4, 5) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY = @TicketsId THEN 
                                                                                         CASE WHEN TI_AMT0 IS NULL THEN TI_AMOUNT 
                                                                                              ELSE TI_AMT0 END
                                                                                    ELSE 0 END), 0)
                                ELSE 0 END AS CMD_WITHDRAWALS
                      FROM CAGE_MOVEMENTS 
                           INNER JOIN   CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          
                           INNER JOIN   TICKETS ON TICKETS.TI_COLLECTED_MONEY_COLLECTION = CAGE_MOVEMENTS.CGM_MC_COLLECTION_ID     
                     WHERE  CGM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
                  AND (CGM_STATUS IN (2, 3, 4, 8, 9, 10) 
                            OR (CGM_STATUS = 0 AND CGM_TYPE = 0)) AND CMD_QUANTITY = @TicketsId  
              AND CAGE_MOVEMENTS.CGM_TYPE <> @CageOperationType_FromTerminal
                     GROUP BY ISNULL(TI_CUR0, @NationalIsoCode) 
                         , ISNULL(CMD_CAGE_CURRENCY_TYPE, 0)                                 
                         , CGM_TYPE
                   ) AS T1        
          GROUP BY ORIGIN
                  ,CASE CMD_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CMD_ISO_CODE END 
                 , CASE WHEN CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE CAGE_CURRENCY_TYPE END < 1000 THEN 0
                        ELSE CAGE_CURRENCY_TYPE END 
                 

            -- GET Terminal value-in amounts
              
              UNION ALL 
              
              SELECT CASE TI_CUR0 WHEN 'X01' THEN @NationalIsoCode ELSE ISNULL(TI_CUR0, @NationalIsoCode) END AS CMD_ISO_CODE
                    , 0 AS CAGE_CURRENCY_TYPE
                    , @TicketsId AS ORIGIN
                    , ISNULL(SUM (CASE WHEN TI_AMT0 IS NULL THEN TI_AMOUNT 
                                        ELSE TI_AMT0 END),0)  AS DEPOSITS
                    , 0 AS WITHDRAWS
              FROM MONEY_COLLECTIONS
                  INNER JOIN CAGE_MOVEMENTS ON CGM_MC_COLLECTION_ID = MC_COLLECTION_ID 
          INNER JOIN TICKETS ON TICKETS.TI_COLLECTED_MONEY_COLLECTION = CAGE_MOVEMENTS.CGM_MC_COLLECTION_ID
             WHERE CAGE_MOVEMENTS.CGM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
               AND CAGE_MOVEMENTS.CGM_TYPE = @CAGEOPERATIONTYPE_FROMTERMINAL
               AND (CGM_STATUS IN (2, 3, 4, 8, 9, 10) 
                    OR (CGM_STATUS = 0 AND CGM_TYPE = 0))           
             GROUP BY CASE TI_CUR0 WHEN 'X01' THEN @NationalIsoCode ELSE ISNULL(TI_CUR0, @NationalIsoCode) END, CGM_TYPE

              UNION ALL
              
               SELECT CASE TE_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE ISNULL(TE_ISO_CODE, @NationalIsoCode) END AS CMD_ISO_CODE
                    , 0 AS CAGE_CURRENCY_TYPE
                    , @BillsId AS ORIGIN
                    , MC_COLLECTED_BILL_AMOUNT + MC_COLLECTED_COIN_AMOUNT AS DEPOSITS
                    , 0 AS WITHDRAWS
              FROM MONEY_COLLECTIONS
                  INNER JOIN CAGE_MOVEMENTS ON CGM_MC_COLLECTION_ID = MC_COLLECTION_ID 
                  INNER JOIN TERMINALS ON TE_TERMINAL_ID = MC_TERMINAL_ID
             WHERE CAGE_MOVEMENTS.CGM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
               AND CAGE_MOVEMENTS.CGM_TYPE = @CageOperationType_FromTerminal
               AND (CGM_STATUS IN (2, 3, 4, 8, 9, 10) 
                    OR (CGM_STATUS = 0 AND CGM_TYPE = 0))
                                      
          ) AS _TBL_GLB
  LEFT JOIN CURRENCY_EXCHANGE ON CMD_ISO_CODE = CE_CURRENCY_ISO_CODE AND CE_TYPE = 0
      WHERE CMD_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)
   GROUP BY CAGE_CURRENCY_TYPE 
          , CE_CURRENCY_ORDER
          , CMD_ISO_CODE 
          , ORIGIN  
   ORDER BY CAGE_CURRENCY_TYPE 
          , CE_CURRENCY_ORDER
          , CMD_ISO_CODE 
          , ORIGIN DESC 
    
    -- TABLE[3]    
        SELECT * FROM #TABLE_TEMP_3
			    WHERE ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)   
        DROP TABLE #TABLE_TEMP_3
        
    -- TABLE[4]: Other System Concepts    
    SELECT * FROM (       
    SELECT CSM_CONCEPT_ID AS CM_CONCEPT_ID
         , CC.CC_DESCRIPTION AS CM_DESCRIPTION
         , CASE CSM_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CSM.CSM_ISO_CODE END AS CM_ISO_CODE
         , CASE CSM_ISO_CODE WHEN 'X01' THEN 1001 ELSE CASE WHEN CSM_CAGE_CURRENCY_TYPE < 1000 THEN 0 ELSE CSM_CAGE_CURRENCY_TYPE END END AS CAGE_CURRENCY_TYPE
         , SUM(CSM_VALUE) AS CM_VALUE
      FROM CAGE_SESSION_METERS AS CSM
           INNER JOIN CAGE_CONCEPTS AS CC ON CSM.CSM_CONCEPT_ID = CC.CC_CONCEPT_ID
     WHERE CSM_SOURCE_TARGET_ID = 0
       AND CSM_CONCEPT_ID IN (7, 8)
       AND CSM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
       AND CSM_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)
  GROUP BY CSM_CONCEPT_ID
         , CC.CC_DESCRIPTION
         , CASE CSM_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CSM.CSM_ISO_CODE END 
         , CASE CSM_ISO_CODE WHEN 'X01' THEN 1001 ELSE CASE WHEN CSM_CAGE_CURRENCY_TYPE < 1000 THEN 0 ELSE CSM_CAGE_CURRENCY_TYPE END END
         ) AS A
  ORDER BY CM_CONCEPT_ID
         , CASE CM_ISO_CODE WHEN @NationalIsoCode THEN '000' ELSE CM_ISO_CODE END
         , CAGE_CURRENCY_TYPE
         
     -- TABLE[5]: Cage Counts
    SELECT ORIGIN AS ORIGIN
         , CASE CMD_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CMD_ISO_CODE END AS ISO_CODE
         , CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE CASE WHEN CAGE_CURRENCY_TYPE = 1 THEN 0 ELSE CAGE_CURRENCY_TYPE END END AS CAGE_CURRENCY_TYPE
         , ISNULL(SUM(CMD_DEPOSITS), 0) AS DEPOSITS
      FROM (SELECT CASE WHEN CMD_QUANTITY >= 0 THEN 0
                        ELSE CMD_QUANTITY END  AS ORIGIN
                 , CMD_ISO_CODE
                 , ISNULL(CMD_CAGE_CURRENCY_TYPE, 0) AS CAGE_CURRENCY_TYPE
                 , ISNULL(SUM (  CASE WHEN CMD_QUANTITY >= 0 THEN CMD_QUANTITY ELSE 1 END 
                               * CASE WHEN CGM_TYPE = 99 THEN -1 ELSE 1 END
                               * CASE WHEN CMD_CAGE_CURRENCY_TYPE = @CageCurrencyType_ColorChips THEN 1 ELSE CMD_DENOMINATION END), 0) AS CMD_DEPOSITS
              FROM CAGE_MOVEMENTS 
                   INNER JOIN CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          
             WHERE CGM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS) and CGM_TYPE IN (99, 199)
          GROUP BY CMD_ISO_CODE, ISNULL(CMD_CAGE_CURRENCY_TYPE, 0), CMD_DENOMINATION, CGM_TYPE, CMD_QUANTITY
            ) AS T1           
   GROUP BY ORIGIN, CASE CMD_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CMD_ISO_CODE END 
         , CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE CASE WHEN CAGE_CURRENCY_TYPE = 1 THEN 0 ELSE CAGE_CURRENCY_TYPE END END 
           
    UNION ALL
              
    SELECT NULL AS ORIGIN
         , CASE CMD_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CMD_ISO_CODE END AS ISO_CODE
         , CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE CASE WHEN CAGE_CURRENCY_TYPE = 1 THEN 0 ELSE CAGE_CURRENCY_TYPE END END AS CAGE_CURRENCY_TYPE
         , ISNULL(SUM(CMD_DEPOSITS), 0) AS DEPOSITS
      FROM (SELECT CMD_ISO_CODE
                 , ISNULL(CMD_CAGE_CURRENCY_TYPE, 0) AS CAGE_CURRENCY_TYPE
                 , ISNULL(SUM (  CASE WHEN CMD_QUANTITY >= 0 THEN CMD_QUANTITY ELSE 1 END 
                               * CASE WHEN CGM_TYPE = 99 THEN -1 ELSE 1 END
                               * CASE WHEN CMD_CAGE_CURRENCY_TYPE = @CageCurrencyType_ColorChips THEN 1 ELSE CMD_DENOMINATION END), 0) AS CMD_DEPOSITS
              FROM CAGE_MOVEMENTS 
                   INNER JOIN CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          
             WHERE CGM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS) and CGM_TYPE IN (99, 199)
          GROUP BY CMD_ISO_CODE, ISNULL(CMD_CAGE_CURRENCY_TYPE, 0), CMD_DENOMINATION, CGM_TYPE, CMD_QUANTITY
           ) AS T1           
  GROUP BY CASE CMD_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CMD_ISO_CODE END 
         , CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE CASE WHEN CAGE_CURRENCY_TYPE = 1 THEN 0 ELSE CAGE_CURRENCY_TYPE END END 
 
    DROP TABLE #TMP_CAGE_SESSIONS 

  END -- IF @pCageSessionIds IS NOT NULL THEN
      
  ELSE -- IF @pCageSessionIds IS NULL
  BEGIN
      
    -- TABLE[1]: Get cage sessions information
    SELECT a.* 
          , (ROW_NUMBER() OVER(ORDER BY CM_SOURCE_TARGET_ID
          , CASE CM_CONCEPT_ID WHEN 0 THEN '000' ELSE CC_DESCRIPTION END
          , CASE WHEN CAGE_CURRENCY_TYPE < 1000 THEN 0
                 WHEN CAGE_CURRENCY_TYPE = 1003 THEN 9999999
                 ELSE 1 END
          , CE_CURRENCY_ORDER
          , CM_ISO_CODE
          , CAGE_CURRENCY_TYPE)) as ORDEN
    FROM (
    SELECT CM.CM_SOURCE_TARGET_ID
         , CM.CM_CONCEPT_ID
         , CASE CM.CM_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CM.CM_ISO_CODE END AS CM_ISO_CODE
         , CASE CM.CM_ISO_CODE WHEN 'X01' THEN 1001 ELSE CASE WHEN CM_CAGE_CURRENCY_TYPE < 1000 THEN 0 ELSE CM_CAGE_CURRENCY_TYPE END END AS CAGE_CURRENCY_TYPE
         , SUM(CM.CM_VALUE_IN) AS CM_VALUE_IN
         , SUM(CM.CM_VALUE_OUT) AS CM_VALUE_OUT
         , CST.CST_SOURCE_TARGET_NAME
         , CC.CC_DESCRIPTION
      FROM CAGE_METERS AS CM
           INNER JOIN CAGE_SOURCE_TARGET_CONCEPTS AS CSTC ON CSTC.CSTC_CONCEPT_ID = CM.CM_CONCEPT_ID AND CSTC.CSTC_SOURCE_TARGET_ID = CM.CM_SOURCE_TARGET_ID
           INNER JOIN CAGE_CONCEPTS AS CC ON CC.CC_CONCEPT_ID = CSTC.CSTC_CONCEPT_ID
           INNER JOIN CAGE_SOURCE_TARGET AS CST ON CST.CST_SOURCE_TARGET_ID = CSTC.CSTC_SOURCE_TARGET_ID
     WHERE (CC.CC_TYPE = 1 OR CC_CONCEPT_ID IN (9, 10, 11))
       AND CC.CC_ENABLED = 1
       AND CSTC.CSTC_ENABLED = 1
       AND CC.CC_SHOW_IN_REPORT = 1
       AND CM_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)
  GROUP BY CM.CM_SOURCE_TARGET_ID
         , CM.CM_CONCEPT_ID
         , CASE CM.CM_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CM.CM_ISO_CODE END 
         , CASE CM.CM_ISO_CODE WHEN 'X01' THEN 1001 ELSE CASE WHEN CM_CAGE_CURRENCY_TYPE < 1000 THEN 0 ELSE CM_CAGE_CURRENCY_TYPE END END 
         , CST.CST_SOURCE_TARGET_NAME
         , CC.CC_DESCRIPTION
         ) as a
    LEFT  JOIN CURRENCY_EXCHANGE ON CM_ISO_CODE = CE_CURRENCY_ISO_CODE AND CE_TYPE = 0
  ORDER BY CM_SOURCE_TARGET_ID
         , CASE CM_CONCEPT_ID WHEN 0 THEN '000' ELSE CC_DESCRIPTION END
         , CASE WHEN CAGE_CURRENCY_TYPE < 1000 THEN 0
                WHEN CAGE_CURRENCY_TYPE = 1003 THEN 9999999
                ELSE 1 END
         , CE_CURRENCY_ORDER
         , CM_ISO_CODE
         , CAGE_CURRENCY_TYPE

    -- TABLE[2]: Get dynamic liabilities
    SELECT CC_CONCEPT_ID AS LIABILITY_CONCEPT_ID
         , CC_DESCRIPTION AS LIABILITY_NAME 
         , CASE CM.CM_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CM.CM_ISO_CODE END AS LIABILITY_ISO_CODE
         , CASE WHEN CASE CM.CM_ISO_CODE WHEN 'X01' THEN 1001 ELSE CM_CAGE_CURRENCY_TYPE END < 1000 THEN 0
                ELSE CM_CAGE_CURRENCY_TYPE END AS LIABILITY_CAGE_CURRENCY_TYPE
         , SUM(CM.CM_VALUE) AS LIABILITY_VALUE
      FROM CAGE_METERS AS CM
     INNER JOIN CAGE_CONCEPTS AS CC ON CC.CC_CONCEPT_ID = CM.CM_CONCEPT_ID
      LEFT JOIN CURRENCY_EXCHANGE ON CM_ISO_CODE = CE_CURRENCY_ISO_CODE AND CE_TYPE = 0
     WHERE CC.CC_ENABLED = 1
       AND CC.CC_SHOW_IN_REPORT = 1
       AND CC.CC_IS_PROVISION = 1
       AND CM_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)
  GROUP BY CC.CC_CONCEPT_ID  
         , CC_DESCRIPTION
         , CASE WHEN CASE CM.CM_ISO_CODE WHEN 'X01' THEN 1001 ELSE CM_CAGE_CURRENCY_TYPE END < 1000 THEN 0
                ELSE CM_CAGE_CURRENCY_TYPE END
         , CE_CURRENCY_ORDER
         , CASE CM.CM_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CM.CM_ISO_CODE END
  ORDER BY CC.CC_CONCEPT_ID  
         , CASE WHEN CASE CM.CM_ISO_CODE WHEN 'X01' THEN 1001 ELSE CM_CAGE_CURRENCY_TYPE END < 1000 THEN 0
                ELSE CM_CAGE_CURRENCY_TYPE END
         , CE_CURRENCY_ORDER
         , CASE CM.CM_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CM.CM_ISO_CODE END

      
    -- TEMP TABLE[3]: Get session sumary
    SELECT CMD_ISO_CODE AS ISO_CODE
         , CASE WHEN CAGE_CURRENCY_TYPE = 99 THEN 0 ELSE CAGE_CURRENCY_TYPE END AS CAGE_CURRENCY_TYPE
         , ISNULL(ORIGIN, 0) AS ORIGIN
         , SUM(CMD_DEPOSITS) AS DEPOSITS
         , SUM(CMD_WITHDRAWALS) AS WITHDRAWALS 
         INTO #TEMP_TABLE_3    
      FROM (SELECT CASE CMD_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CMD_ISO_CODE END AS CMD_ISO_CODE
                 , CASE WHEN CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE CAGE_CURRENCY_TYPE END < 1000 THEN 0
                        ELSE CAGE_CURRENCY_TYPE END AS CAGE_CURRENCY_TYPE
                 , ORIGIN
                 , SUM(CMD_DEPOSITS) CMD_DEPOSITS
                 , SUM(CMD_WITHDRAWALS) CMD_WITHDRAWALS      
              FROM (SELECT CASE WHEN CMD_QUANTITY >= 0 THEN @BillsId
                                ELSE CMD_QUANTITY END AS ORIGIN
                         , CASE CMD_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CMD_ISO_CODE END AS CMD_ISO_CODE
                         , CASE WHEN CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE CMD_CAGE_CURRENCY_TYPE END < 1000 THEN 0
                                ELSE CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE ISNULL(CMD_CAGE_CURRENCY_TYPE, 0) END END  AS CAGE_CURRENCY_TYPE
                         , CMD_DENOMINATION
                         , CASE WHEN CGM_TYPE IN (100, 101, 103, 104) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY < 0 AND CMD_QUANTITY <> @TicketsId THEN CMD_DENOMINATION
                                                                                            WHEN CMD_QUANTITY = @TicketsId THEN 0
                                                                                            WHEN CMD_CAGE_CURRENCY_TYPE = @CageCurrencyType_ColorChips THEN CMD_QUANTITY
                                                                                       ELSE CMD_QUANTITY * CMD_DENOMINATION END), 0)
                                ELSE 0 END AS CMD_DEPOSITS
                         , CASE WHEN CGM_TYPE IN (0, 1, 4, 5) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY < 0 AND CMD_QUANTITY <> @TicketsId THEN CMD_DENOMINATION
                                                                                    WHEN CMD_QUANTITY = @TicketsId THEN 0
                                                                                    WHEN CMD_CAGE_CURRENCY_TYPE = @CageCurrencyType_ColorChips THEN CMD_QUANTITY
                                                                                    ELSE CMD_QUANTITY * CMD_DENOMINATION END), 0)
                                ELSE 0 END AS CMD_WITHDRAWALS
                      FROM CAGE_MOVEMENTS 
                INNER JOIN CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          
                     WHERE CGM_STATUS IN (2, 3, 4, 8, 9, 10) 
                        OR (CGM_STATUS = 0 AND CGM_TYPE = 0)
                  GROUP BY CASE WHEN CMD_QUANTITY >= 0 THEN @BillsId
                                ELSE CMD_QUANTITY END 
                         , CASE CMD_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CMD_ISO_CODE END 
                         , CASE WHEN CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE CMD_CAGE_CURRENCY_TYPE END < 1000 THEN 0
                                ELSE CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE ISNULL(CMD_CAGE_CURRENCY_TYPE, 0) END END 
                         , CMD_DENOMINATION
                         , CGM_TYPE

          -- Tickets 
                  UNION ALL
                  
                    SELECT @TicketsId AS ORIGIN
                         , ISNULL(TI_CUR0, @NationalIsoCode) AS CMD_ISO_CODE 
                         , ISNULL(CMD_CAGE_CURRENCY_TYPE, 0) AS CAGE_CURRENCY_TYPE                                   
                         , 1 AS CMD_DENOMINATION
                         , CASE WHEN CGM_TYPE IN (100, 101, 103, 104) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY = @TicketsId THEN 
                                                                                                 CASE WHEN TI_AMT0 IS NULL THEN TI_AMOUNT 
                                                                                                      ELSE TI_AMT0 END
                                                                                       ELSE 0 END), 0)
                                ELSE 0 END AS CMD_DEPOSITS
                         , CASE WHEN CGM_TYPE IN (0, 1, 4, 5) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY = @TicketsId THEN 
                                                                                         CASE WHEN TI_AMT0 IS NULL THEN TI_AMOUNT 
                                                                                              ELSE TI_AMT0 END
                                                                                    ELSE 0 END), 0)
                                ELSE 0 END AS CMD_WITHDRAWALS
                      FROM CAGE_MOVEMENTS 
                           INNER JOIN   CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          
                           INNER JOIN   TICKETS ON TICKETS.TI_COLLECTED_MONEY_COLLECTION = CAGE_MOVEMENTS.CGM_MC_COLLECTION_ID     
                     WHERE  (CGM_STATUS IN (2, 3, 4, 8, 9, 10) 
                            OR (CGM_STATUS = 0 AND CGM_TYPE = 0)) AND CMD_QUANTITY = @TicketsId  
                            AND CAGE_MOVEMENTS.CGM_TYPE <> @CageOperationType_FromTerminal
                     GROUP BY ISNULL(TI_CUR0, @NationalIsoCode) 
                         , ISNULL(CMD_CAGE_CURRENCY_TYPE, 0)                             
                         , CGM_TYPE 
                    ) AS T1
           GROUP BY ORIGIN
                  ,CASE CMD_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CMD_ISO_CODE END 
                 , CASE WHEN CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE CAGE_CURRENCY_TYPE END < 1000 THEN 0
                        ELSE CAGE_CURRENCY_TYPE END 
                 

              -- GET Terminal value-in amounts
              
              UNION ALL 
              
              SELECT ISNULL(TI_CUR0, @NationalIsoCode) AS CMD_ISO_CODE
                    , 0 AS CAGE_CURRENCY_TYPE
                    , @TicketsId AS ORIGIN
                    , ISNULL(SUM (CASE WHEN TI_AMT0 IS NULL THEN TI_AMOUNT 
                                       ELSE TI_AMT0 END),0)  AS DEPOSITS
                    , 0 AS WITHDRAWS
              FROM MONEY_COLLECTIONS
        INNER JOIN CAGE_MOVEMENTS ON CGM_MC_COLLECTION_ID = MC_COLLECTION_ID 
        INNER JOIN TICKETS ON TICKETS.TI_COLLECTED_MONEY_COLLECTION = CAGE_MOVEMENTS.CGM_MC_COLLECTION_ID            
             WHERE CAGE_MOVEMENTS.CGM_TYPE = @CAGEOPERATIONTYPE_FROMTERMINAL
               AND (CGM_STATUS IN (2, 3, 4, 8, 9, 10) 
                    OR (CGM_STATUS = 0 AND CGM_TYPE = 0))
             GROUP BY ISNULL(TI_CUR0, @NationalIsoCode) 

              UNION ALL
              
                SELECT ISNULL(TE_ISO_CODE, @NationalIsoCode) AS CMD_ISO_CODE
                    , 0 AS CAGE_CURRENCY_TYPE
                    , @BillsId AS ORIGIN
                    , MC_COLLECTED_BILL_AMOUNT + MC_COLLECTED_COIN_AMOUNT  AS DEPOSITS
                    , 0 AS WITHDRAWS
              FROM MONEY_COLLECTIONS
                  INNER JOIN CAGE_MOVEMENTS ON CGM_MC_COLLECTION_ID = MC_COLLECTION_ID 
          INNER JOIN TERMINALS ON TE_TERMINAL_ID = MC_TERMINAL_ID
             WHERE CAGE_MOVEMENTS.CGM_TYPE = @CageOperationType_FromTerminal
               AND (CGM_STATUS IN (2, 3, 4, 8, 9, 10) 
                    OR (CGM_STATUS = 0 AND CGM_TYPE = 0))
                          
          ) AS _TBL_GLB
  LEFT JOIN CURRENCY_EXCHANGE ON CMD_ISO_CODE = CE_CURRENCY_ISO_CODE AND CE_TYPE = 0
   GROUP BY CAGE_CURRENCY_TYPE 
          , CE_CURRENCY_ORDER
          , CMD_ISO_CODE 
          , ORIGIN  
   ORDER BY CAGE_CURRENCY_TYPE 
          , CE_CURRENCY_ORDER
          , CMD_ISO_CODE 
          , ORIGIN DESC 
	-- TABLE[3]:
	  SELECT * FROM #TEMP_TABLE_3
	           WHERE ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)   
      DROP TABLE #TEMP_TABLE_3
         
    -- TABLE[4]: Other System Concepts    
    SELECT * FROM (       
    SELECT CM_CONCEPT_ID
         , CC.CC_DESCRIPTION AS CM_DESCRIPTION
         , CASE CM_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CM.CM_ISO_CODE END AS CM_ISO_CODE
         , CASE CM_ISO_CODE WHEN 'X01' THEN 1001 ELSE CASE WHEN CM_CAGE_CURRENCY_TYPE < 1000 THEN 0 ELSE CM_CAGE_CURRENCY_TYPE END END AS CAGE_CURRENCY_TYPE
         , SUM(CM_VALUE) AS CM_VALUE
      FROM CAGE_METERS AS CM
           INNER JOIN CAGE_CONCEPTS AS CC ON CM.CM_CONCEPT_ID = CC.CC_CONCEPT_ID
     WHERE CM_SOURCE_TARGET_ID = 0
       AND CM_CONCEPT_ID IN (7, 8)
       AND CM_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)
  GROUP BY CM_CONCEPT_ID
         , CASE CM_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CM.CM_ISO_CODE END
         , CASE CM_ISO_CODE WHEN 'X01' THEN 1001 ELSE CASE WHEN CM_CAGE_CURRENCY_TYPE < 1000 THEN 0 ELSE CM_CAGE_CURRENCY_TYPE END END
         , CC.CC_DESCRIPTION
         ) AS A
  ORDER BY CM_CONCEPT_ID
         , CASE CM_ISO_CODE WHEN @NationalIsoCode THEN '000' ELSE CM_ISO_CODE END
         , CAGE_CURRENCY_TYPE
    
    -- TABLE[5]: Cage Counts
    SELECT ORIGIN AS ORIGIN
         , CASE CMD_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CMD_ISO_CODE END AS ISO_CODE
         , CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE CASE WHEN CAGE_CURRENCY_TYPE = 1 THEN 0 ELSE CAGE_CURRENCY_TYPE END END AS CAGE_CURRENCY_TYPE
         , ISNULL(SUM(CMD_DEPOSITS), 0) AS DEPOSITS
      FROM (SELECT CASE WHEN CMD_QUANTITY >= 0 THEN 0
                        ELSE CMD_QUANTITY END  AS ORIGIN
                 , CMD_ISO_CODE
                 , ISNULL(CMD_CAGE_CURRENCY_TYPE, 0) AS CAGE_CURRENCY_TYPE
                 , ISNULL(SUM (  CASE WHEN CMD_QUANTITY >= 0 THEN CMD_QUANTITY ELSE 1 END 
                               * CASE WHEN CGM_TYPE = 99 THEN -1 ELSE 1 END
                               * CASE WHEN CMD_CAGE_CURRENCY_TYPE = @CageCurrencyType_ColorChips THEN 1 ELSE CMD_DENOMINATION END), 0) AS CMD_DEPOSITS
              FROM CAGE_MOVEMENTS 
                   INNER JOIN CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          
             WHERE CGM_TYPE IN (99, 199)
          GROUP BY CMD_ISO_CODE, ISNULL(CMD_CAGE_CURRENCY_TYPE, 0), CMD_DENOMINATION, CGM_TYPE, CMD_QUANTITY
            ) AS T1           
   GROUP BY ORIGIN, CASE CMD_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CMD_ISO_CODE END 
         , CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE CASE WHEN CAGE_CURRENCY_TYPE = 1 THEN 0 ELSE CAGE_CURRENCY_TYPE END END 
           
    UNION ALL
              
    SELECT NULL AS ORIGIN
         , CASE CMD_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CMD_ISO_CODE END AS ISO_CODE
         , CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE CASE WHEN CAGE_CURRENCY_TYPE = 1 THEN 0 ELSE CAGE_CURRENCY_TYPE END END AS CAGE_CURRENCY_TYPE
         , ISNULL(SUM(CMD_DEPOSITS), 0) AS DEPOSITS
      FROM (SELECT CMD_ISO_CODE
                 , ISNULL(CMD_CAGE_CURRENCY_TYPE, 0) AS CAGE_CURRENCY_TYPE
                 , ISNULL(SUM (  CASE WHEN CMD_QUANTITY >= 0 THEN CMD_QUANTITY ELSE 1 END 
                               * CASE WHEN CGM_TYPE = 99 THEN -1 ELSE 1 END
                               * CASE WHEN CMD_CAGE_CURRENCY_TYPE = @CageCurrencyType_ColorChips THEN 1 ELSE CMD_DENOMINATION END), 0) AS CMD_DEPOSITS
              FROM CAGE_MOVEMENTS 
                   INNER JOIN CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          
             WHERE CGM_TYPE IN (99, 199)
          GROUP BY CMD_ISO_CODE, ISNULL(CMD_CAGE_CURRENCY_TYPE, 0), CMD_DENOMINATION, CGM_TYPE, CMD_QUANTITY
           ) AS T1           
  GROUP BY CASE CMD_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CMD_ISO_CODE END 
         , CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE CASE WHEN CAGE_CURRENCY_TYPE = 1 THEN 0 ELSE CAGE_CURRENCY_TYPE END END 
 

  END -- ELSE
  
  -- TABLE[6]: Stock Stored 
  SELECT @StockIsStored
  
  DROP TABLE #TMP_CURRENCIES_ISO_CODES
                   
                   
END -- CageGetGlobalReportData

GO

GRANT EXECUTE ON [dbo].[CageGetGlobalReportData] TO [wggui] WITH GRANT OPTION
GO

------------------------------------------------------------------------------------------------------------------------------------------------
-- Copyright © 2014 Win Systems International
------------------------------------------------------------------------------------------------------------------------------------------------
-- 
--   MODULE NAME: CageCurrentStock.sql
-- 
--   DESCRIPTION: 
-- 
--        AUTHOR: 
-- 
-- CREATION DATE: 
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
------------------------------------------------------------------------------------------------------------------------------------------------
-- 14-JUL-2017 JML    Bug 28673:WIGOS-3575 Cage - There are different fields and data between GUI and The excel report.
------------------------------------------------------------------------------------------------------------------------------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CageCurrentStock]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[CageCurrentStock]
GO

CREATE  PROCEDURE [dbo].[CageCurrentStock]
AS
BEGIN
  --Get current stock     
  SELECT   CGS_ISO_CODE                                          
         , CGS_DENOMINATION                                      
         , CGS_QUANTITY 
         , CGS_CAGE_CURRENCY_TYPE
         , SET_ID 
         , CGS_CHIP_ID
         , COLOR
         , NAME
         , DRAWING
         , CE_CURRENCY_ORDER 
    FROM ( SELECT   CGS_ISO_CODE                                          
                  , CGS_DENOMINATION                                      
                  , CGS_QUANTITY                     
                  , CASE WHEN CGS_DENOMINATION > 0 THEN ISNULL(CGS_CAGE_CURRENCY_TYPE,0) ELSE 99 END AS CGS_CAGE_CURRENCY_TYPE   
                  , -1 AS CGS_CHIP_ID  
                  , -1 AS SET_ID                
                  , '' AS COLOR
                  , '' AS NAME
                  , '' AS DRAWING
             FROM   CAGE_STOCK
        UNION ALL
           SELECT   C.CH_ISO_CODE AS CGS_ISO_CODE
                  , C.CH_DENOMINATION AS CGS_DENOMINATION
                  , CST.CHSK_QUANTITY AS CGS_QUANTITY
                  , C.CH_CHIP_TYPE AS CGS_CAGE_CURRENCY_TYPE
                  , C.CH_CHIP_ID AS CGS_CHIP_ID
                  , CSC_SET_ID AS SET_ID
                  , C.CH_COLOR AS COLOR
                  , C.CH_NAME AS NAME
                  , C.CH_DRAWING AS DRAWING
             FROM   CHIPS_STOCK AS CST
       INNER JOIN   CHIPS AS C ON CST.CHSK_CHIP_ID = C.CH_CHIP_ID
       INNER JOIN   CHIPS_SETS_CHIPS ON C.CH_CHIP_ID = CSC_CHIP_ID
                  ) AS _TBL 
    LEFT   JOIN CURRENCY_EXCHANGE ON CGS_ISO_CODE = CE_CURRENCY_ISO_CODE AND CE_TYPE = 0
   WHERE   CGS_QUANTITY > 0  OR CGS_DENOMINATION = -100  
ORDER BY   CE_CURRENCY_ORDER
         , CGS_ISO_CODE
         , CASE WHEN CGS_CAGE_CURRENCY_TYPE < 1000 THEN 0
                   WHEN CGS_CAGE_CURRENCY_TYPE = 1003 THEN 9999999
                   ELSE 1 END
         , SET_ID ASC
         , CGS_DENOMINATION DESC
         , CASE WHEN CGS_DENOMINATION >= 0 THEN CGS_DENOMINATION 
                ELSE CGS_DENOMINATION * (-100000) END
END --CageCurrentStock

GO

GRANT EXECUTE ON [dbo].[CageCurrentStock] TO [wggui] WITH GRANT OPTION
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CageStockOnCloseSession]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[CageStockOnCloseSession]
GO

CREATE PROCEDURE [dbo].[CageStockOnCloseSession]
        @pCageSessionId NVARCHAR(MAX) = NULL
AS
BEGIN
  
  DECLARE @List VARCHAR(MAX) 

  DECLARE @TMP_STOCK_GEN TABLE
  (
     CGS_ISO_CODE NVARCHAR(MAX)                                        
     , CGS_DENOMINATION MONEY                                      
     , CGS_QUANTITY MONEY  
     , CGS_CAGE_CURRENCY_TYPE INT
     , SET_ID BIGINT
     , CGS_CHIP_ID BIGINT
     , COLOR INT
     , NAME NVARCHAR(50)
     , DRAWING NVARCHAR(50)
     , CE_CURRENCY_ORDER BIGINT
  )
  --Get Current Stock
  INSERT INTO @TMP_STOCK_GEN
  EXEC CageCurrentStock
  
  --Parse current Stock                                                
  SELECT  @List = COALESCE(@List + '', '') + RTRIM(LTRIM(CGS_ISO_CODE))      
      + ';'+ RTRIM(LTRIM(CAST(CGS_DENOMINATION AS VARCHAR)))       
      + ';'+ RTRIM(LTRIM(CAST(CGS_QUANTITY AS VARCHAR)))           
      + ';'+ RTRIM(LTRIM(CAST(CGS_CAGE_CURRENCY_TYPE AS VARCHAR))) 
      + ';'+ RTRIM(LTRIM(CAST(CGS_CHIP_ID AS VARCHAR))) 
      + '|'                                                        
   FROM @TMP_STOCK_GEN
   
   --Save current stock on session    
   UPDATE CAGE_SESSIONS                                                       
    SET CGS_CAGE_STOCK = @List                                              
   WHERE CGS_CAGE_SESSION_ID = @pCageSessionId                                     

END --CageStockOnCloseSession

GO
GRANT EXECUTE ON [dbo].[CageStockOnCloseSession] TO [wggui] WITH GRANT OPTION 
GO


/****** Object:  StoredProcedure [dbo].[TITO_SetTicketValid]    Script Date: 01/21/2014 11:07:40 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TITO_SetTicketValid]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[TITO_SetTicketValid]
GO


CREATE PROCEDURE [dbo].[TITO_SetTicketValid] 
      @pTerminalId                 INT,
      @pTicketValidationNumber     BIGINT,
      @pTicketRejectReasonEgm      BIGINT,
      @pTicketRejectReasonWcp      BIGINT,
      --- OUTPUT
      @pTicketId                               BIGINT   OUTPUT
AS
BEGIN
      DECLARE  @_ticket_status_valid                 INT
      DECLARE  @_ticket_status_pending_cancel        INT
      DECLARE  @_min_ticket_id                       BIGINT
      DECLARE  @_max_ticket_id                       BIGINT
      DECLARE  @_virtual_account_id                  BIGINT
      DECLARE  @_last_action_account_id              BIGINT
      DECLARE  @_player_account_id                   BIGINT

      SET NOCOUNT ON

      SET @pTicketId = NULL

      SET @_ticket_status_valid          = 0 -- TITO_TICKET_STATUS.VALID
      SET @_ticket_status_pending_cancel = 5 -- TITO_TICKET_STATUS.PENDING_CANCEL      
            
      --SELECT   @_virtual_account_id = ISNULL(TE_VIRTUAL_ACCOUNT_ID, 0)
      --  FROM   TERMINALS
      -- WHERE   TE_TERMINAL_ID = @pTerminalId 

      --Get AccountID (From indentified Player or not)
      SELECT TOP 1  @_virtual_account_id = ISNULL(TE_VIRTUAL_ACCOUNT_ID, 0)                                                                       
                  , @_player_account_id  = ISNULL(PS_ACCOUNT_ID, 0)                                                                       
            FROM    TERMINALS                                                                                               
       LEFT JOIN    PLAY_SESSIONS      
              ON    PS_TERMINAL_ID  = TE_TERMINAL_ID 
             AND    PS_STATUS       = 0               --PlaySession OPENED     
           WHERE    TE_TERMINAL_ID  = @pTerminalId                                                
        ORDER BY    PS_PLAY_SESSION_ID  DESC                     

      SET @_last_action_account_id = @_virtual_account_id --Without Card (VirtualAccountId) "anonymous"

      IF (@_player_account_id) > 0 
      BEGIN
        SET @_last_action_account_id = @_player_account_id --Player identified
      END


      SELECT   @_min_ticket_id = MIN (TI_TICKET_ID)
             , @_max_ticket_id = MAX (TI_TICKET_ID)
        FROM   TICKETS WITH (INDEX (IX_ti_validation_number_status))
      WHERE    TI_VALIDATION_NUMBER       = @pTicketValidationNumber
         AND   TI_STATUS                  = @_ticket_status_pending_cancel
         AND   TI_LAST_ACTION_TERMINAL_ID = @pTerminalId
         
      IF @_min_ticket_id =  @_max_ticket_id
      BEGIN
            -- One ticket found, change its status
            UPDATE   TICKETS 
               SET   TI_STATUS                  = @_ticket_status_valid 
                   , TI_LAST_ACTION_TERMINAL_ID = @pTerminalId 
                   , TI_LAST_ACTION_DATETIME    = GETDATE() 
                   , TI_LAST_ACTION_ACCOUNT_ID  = @_last_action_account_id 
                   , TI_REJECT_REASON_EGM       = @pTicketRejectReasonEgm
                   , TI_REJECT_REASON_WCP       = @pTicketRejectReasonWcp
             WHERE   TI_TICKET_ID               = @_min_ticket_id 
               AND   TI_STATUS                  = @_ticket_status_pending_cancel
               AND   TI_LAST_ACTION_TERMINAL_ID = @pTerminalId
         
            IF @@ROWCOUNT = 1
                  SET   @pTicketId = @_min_ticket_id
      END
END

GO


 /****** Object:  StoredProcedure [dbo].[TITO_SetTicketCanceled]    Script Date: 01/21/2014 11:07:40 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TITO_SetTicketCanceled]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[TITO_SetTicketCanceled]
GO

CREATE PROCEDURE [dbo].[TITO_SetTicketCanceled]
      @pTerminalId                 INT,
      @pTicketValidationNumber     BIGINT,
      @MsgSequenceId               BIGINT,
      @pTicketRejectReasonEgm      BIGINT,
      @pTicketRejectReasonWcp      BIGINT,
      --- OUTPUT
      @pTicketId                   BIGINT   OUTPUT,
      @pTicketAmount               MONEY    OUTPUT
AS
BEGIN
      DECLARE  @_ticket_status_canceled              INT
      DECLARE  @_ticket_status_pending_cancel        INT
      DECLARE  @_min_ticket_id                       BIGINT
      DECLARE  @_max_ticket_id                       BIGINT
      DECLARE  @_ticket_amount                       MONEY
      DECLARE  @_virtual_account_id                  BIGINT
      DECLARE  @_player_account_id                   BIGINT
      DECLARE  @_last_action_account_id              BIGINT

      SET NOCOUNT ON

      SET @pTicketId = NULL
      SET @pTicketAmount = NULL

      SET @_ticket_status_canceled       = 1 -- TITO_TICKET_STATUS.CANCELED
      SET @_ticket_status_pending_cancel = 5 -- TITO_TICKET_STATUS.PENDING_CANCEL
      
      --SELECT   @_virtual_account_id = ISNULL(TE_VIRTUAL_ACCOUNT_ID, 0)
      --  FROM   TERMINALS
      -- WHERE   TE_TERMINAL_ID = @pTerminalId 
      
      --Get AccountID (From indentified Player or not)
      SELECT TOP 1  @_virtual_account_id = ISNULL(TE_VIRTUAL_ACCOUNT_ID, 0)                                                                       
                  , @_player_account_id  = ISNULL(PS_ACCOUNT_ID, 0)                                                                       
            FROM    TERMINALS                                                                                               
       LEFT JOIN    PLAY_SESSIONS      
              ON    PS_TERMINAL_ID  = TE_TERMINAL_ID 
             AND    PS_STATUS       = 0               --PlaySession OPENED     
           WHERE    TE_TERMINAL_ID  = @pTerminalId                                                
        ORDER BY    PS_PLAY_SESSION_ID  DESC                     

      SET @_last_action_account_id = @_virtual_account_id --Without Card (VirtualAccountId) "anonymous"

      IF (@_player_account_id) > 0 
      BEGIN
        SET @_last_action_account_id = @_player_account_id --Player identified
      END


      SELECT   @_min_ticket_id = MIN (TI_TICKET_ID)
             , @_max_ticket_id = MAX (TI_TICKET_ID)
             , @_ticket_amount = MAX (TI_AMOUNT)
        FROM   TICKETS WITH (INDEX (IX_ti_validation_number_status))
      WHERE    TI_VALIDATION_NUMBER       = @pTicketValidationNumber
         AND   TI_STATUS                  = @_ticket_status_pending_cancel
         AND   TI_LAST_ACTION_TERMINAL_ID = @pTerminalId
         
      IF @_min_ticket_id =  @_max_ticket_id
      BEGIN
            -- One ticket found, change its status
            UPDATE   TICKETS 
               SET   TI_STATUS                  = @_ticket_status_canceled 
                   , TI_LAST_ACTION_DATETIME    = GETDATE() 
                   , TI_LAST_ACTION_ACCOUNT_ID  = @_last_action_account_id 
                   , TI_REJECT_REASON_EGM       = @pTicketRejectReasonEgm
                   , TI_REJECT_REASON_WCP       = @pTicketRejectReasonWcp
             WHERE   TI_TICKET_ID               = @_min_ticket_id 
               AND   TI_STATUS                  = @_ticket_status_pending_cancel
               AND   TI_LAST_ACTION_TERMINAL_ID = @pTerminalId
         
            IF @@ROWCOUNT = 1
            BEGIN
                  SET   @pTicketId     = @_min_ticket_id
                  SET   @pTicketAmount = @_ticket_amount

                  INSERT INTO   TITO_TASKS 
                              ( TT_TASK_TYPE
                              , TT_TICKET_ID )
                       VALUES ( 1 -- TITO_TICKET_STATUS.CANCELED
                              , @pTicketId   ) 
            END

            RETURN
      END
END

GO


-- TITO_SetTicketPendingCancel
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TITO_SetTicketPendingCancel]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[TITO_SetTicketPendingCancel]
GO

CREATE PROCEDURE [dbo].[TITO_SetTicketPendingCancel] 
  @pTerminalId                  INT,
  @pTicketValidationNumber      BIGINT,
  @pDefaultAllowRedemption      BIT,
  @pDefaultMaxAllowedTicketIn   MONEY,
  @pDefaultMinAllowedTicketIn   MONEY,
  @pTicketRejectReasonEgm       BIGINT,
  @pTicketRejectReasonWcp       BIGINT,
  --- OUTPUT
  @pTicketId                    BIGINT   OUTPUT,
  @pTicketType                  INT      OUTPUT,
  @pTicketAmount                MONEY    OUTPUT,
  @pTicketExpiration            DATETIME OUTPUT

AS
BEGIN
  DECLARE @_ticket_status_valid            INT
  DECLARE @_ticket_status_pending_printing INT
  DECLARE @_ticket_status_pending_cancel   INT
  DECLARE @_min_ticket_id                  BIGINT
  DECLARE @_max_ticket_id                  BIGINT
  DECLARE @_max_ticket_amount              MONEY
  DECLARE @_min_ticket_amount              MONEY
  DECLARE @_min_denom_amount               MONEY
  DECLARE @_redeem_allowed                 BIT
  DECLARE @_promotion_id                   BIGINT
  DECLARE @_restricted                     INT
  DECLARE @_prov_id                        INT
  DECLARE @_element_type                   INT
  DECLARE @_ticket_type                    INT
  DECLARE @_only_redeemable                INT
  DECLARE @_virtual_account_id             BIGINT
  DECLARE @_player_account_id              BIGINT
  DECLARE @_last_action_account_id         BIGINT

  
  
  DECLARE @_amt0                           MONEY
  DECLARE @_cur0                           NVARCHAR(3)
  DECLARE @_sys_amt                        MONEY
  DECLARE @_egm_amt                        MONEY
  DECLARE @_egm_iso                        NVARCHAR(3)   
  DECLARE @_dual_currency_enabled          BIT
  DECLARE @_sys_iso_national_cur           AS NVARCHAR(3)   
  DECLARE @_allow_to_play_on_egm           BIT
  DECLARE @_allow_to_pay_pending_printing  BIT  
  DECLARE @_terminal_allow_truncate        BIT
  DECLARE @_gp_allow_truncate              BIT
  DECLARE @_is_truncate                    BIT
  DECLARE @_gp_min_denom_amount            MONEY
  
  SET NOCOUNT ON

  SET @pTicketId = NULL
  SET @pTicketType = NULL
  SET @pTicketAmount = NULL
  SET @pTicketExpiration = NULL
  SET @_element_type = 3  -- EXPLOIT_ELEMENT_TYPE.PROMOTION

  SELECT @_allow_to_play_on_egm = CAST(ISNULL(GP_KEY_VALUE,'0') AS BIT) FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'TITO' AND GP_SUBJECT_KEY = 'PendingCancel.AllowToPlayOnEGM'
  SET @_allow_to_play_on_egm = ISNULL(@_allow_to_play_on_egm,0)
  
  SELECT @_allow_to_pay_pending_printing = CAST(ISNULL(GP_KEY_VALUE,'0') AS BIT) FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'TITO' AND GP_SUBJECT_KEY = 'PendingPrint.AllowToPlayOnEGM'
  SET @_allow_to_pay_pending_printing = ISNULL(@_allow_to_pay_pending_printing,0)
     
  --SELECT   @_redeem_allowed           = ISNULL(TE_ALLOWED_REDEMPTION, @pDefaultAllowRedemption)
  --       , @_max_ticket_amount        = TE_MAX_ALLOWED_TI
  --       , @_min_ticket_amount        = TE_MIN_ALLOWED_TI
  --       , @_min_denom_amount         = TE_MIN_DENOMINATION
  --       , @_terminal_allow_truncate  = TE_ALLOW_TRUNCATE
  --       , @_prov_id                  = ISNULL(TE_PROV_ID, -1)
  --       , @_virtual_account_id       = ISNULL(TE_VIRTUAL_ACCOUNT_ID, 0)
  --  FROM  TERMINALS                     
  -- WHERE  TE_TERMINAL_ID = @pTerminalId 

  --Get AccountID (From indentified Player or not)
  SELECT TOP 1  @_redeem_allowed           = ISNULL(TE_ALLOWED_REDEMPTION, @pDefaultAllowRedemption)
              , @_max_ticket_amount        = TE_MAX_ALLOWED_TI
              , @_min_ticket_amount        = TE_MIN_ALLOWED_TI
              , @_min_denom_amount         = ISNULL(TE_MIN_DENOMINATION, 0)
              , @_terminal_allow_truncate  = ISNULL(TE_ALLOW_TRUNCATE, 0)
              , @_prov_id                  = ISNULL(TE_PROV_ID, -1)
              , @_virtual_account_id       = ISNULL(TE_VIRTUAL_ACCOUNT_ID, 0)
              , @_player_account_id        = ISNULL(PS_ACCOUNT_ID, 0)                                                                       
        FROM    TERMINALS   
   LEFT JOIN    PLAY_SESSIONS      
          ON    PS_TERMINAL_ID  = TE_TERMINAL_ID 
         AND    PS_STATUS       = 0               --PlaySession OPENED     
       WHERE    TE_TERMINAL_ID  = @pTerminalId                                                
	ORDER BY    PS_PLAY_SESSION_ID  DESC   
      
   SET @_last_action_account_id = @_virtual_account_id --Without Card (VirtualAccountId) "anonymous"

   IF (@_player_account_id) > 0 
   BEGIN
     SET @_last_action_account_id = @_player_account_id --Player identified
   END


   SET @_egm_amt = NULL
   SET @_egm_iso = NULL

   SELECT @_dual_currency_enabled = CAST(ISNULL(GP_KEY_VALUE,'0') AS BIT) FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'FloorDualCurrency' AND GP_SUBJECT_KEY = 'Enabled'
   SET @_dual_currency_enabled = ISNULL(@_dual_currency_enabled,0)   
   
   IF (@_dual_currency_enabled = 1)
   BEGIN
     SET @_sys_iso_national_cur = (SELECT ISNULL(GP_KEY_VALUE,'') FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'RegionalOptions' AND GP_SUBJECT_KEY = 'CurrencyISOCode')
     SET @_egm_iso  = (SELECT ISNULL(TE_ISO_CODE, @_sys_iso_national_cur) FROM TERMINALS WHERE TE_TERMINAL_ID = @pTerminalId)
     -- Convert max allowed ticket In when terminal iso code is different National Currency
     IF (@_max_ticket_amount IS NOT NULL AND  @_sys_iso_national_cur <> @_egm_iso)
     BEGIN
      SET @_max_ticket_amount = dbo.ApplyExchange2(@_max_ticket_amount, @_egm_iso, @_sys_iso_national_cur)
     END
     IF (@_min_ticket_amount IS NOT NULL AND  @_sys_iso_national_cur <> @_egm_iso)
     BEGIN
      SET @_min_ticket_amount = dbo.ApplyExchange2(@_min_ticket_amount, @_egm_iso, @_sys_iso_national_cur)
     END
   END

   SET @_max_ticket_amount = ISNULL(@_max_ticket_amount, @pDefaultMaxAllowedTicketIn)   
   SET @_min_ticket_amount = ISNULL(@_min_ticket_amount, @pDefaultMinAllowedTicketIn)   

  IF   @_max_ticket_amount IS NULL 
    OR @_min_ticket_amount IS NULL 
    OR @_redeem_allowed    IS NULL 
    OR @_redeem_allowed    = 0 
    RETURN
      
  SET @_ticket_status_valid          = 0 -- TITO_TICKET_STATUS.VALID
  SET @_ticket_status_pending_cancel = 5 -- TITO_TICKET_STATUS.PENDING_CANCEL
  set @_ticket_status_pending_printing = 6

  if @_allow_to_pay_pending_printing = 0 begin
	-- Not allow to pay tickets pending priting
	set @_ticket_status_pending_printing = -9
  end

	  SELECT   @_min_ticket_id = MIN (TI_TICKET_ID)
			 , @_max_ticket_id = MAX (TI_TICKET_ID)
			 , @_promotion_id  = MAX (ISNULL(TI_PROMOTION_ID,0))
			 , @_ticket_type   = MAX (TI_TYPE_ID)
			 , @_amt0          = ISNULL (MAX(TI_AMT0),MAX(TI_AMOUNT))
			 , @_cur0          = MAX(TI_CUR0)
			 , @_sys_amt       = MAX(TI_AMOUNT)
		FROM   TICKETS WITH (INDEX (IX_ti_validation_number_status))
	   WHERE   TI_VALIDATION_NUMBER  = @pTicketValidationNumber
		 AND   1 = CASE 
					 WHEN @_allow_to_play_on_egm = 0 AND (TI_STATUS = @_ticket_status_valid or ti_status = @_ticket_status_pending_printing) THEN 1
					 WHEN @_allow_to_play_on_egm = 1 AND (TI_STATUS = @_ticket_status_valid OR TI_STATUS = @_ticket_status_pending_cancel or ti_status = @_ticket_status_pending_printing) THEN 1
				   ELSE 0 END
		 AND   TI_AMOUNT  > 0
		 AND ( TI_EXPIRATION_DATETIME IS NULL OR TI_EXPIRATION_DATETIME > GETDATE () )
		 AND ( @_max_ticket_amount = 0 OR TI_AMOUNT <= @_max_ticket_amount )
     AND ( @_min_ticket_amount = 0 OR TI_AMOUNT >= @_min_ticket_amount )

 IF @_min_ticket_id =  @_max_ticket_id
  BEGIN
  
    -- We verify if the provider is restricted with no-redeemable
    SET @_only_redeemable = (SELECT ISNULL(PV_ONLY_REDEEMABLE,-1) FROM PROVIDERS WHERE PV_ID = @_prov_id)
      
    -- TITO_TICKET_TYPE.PROMO_NONREDEEM
    IF ((@_only_redeemable = 1 AND @_ticket_type = 2) or (@_only_redeemable = -1))
      RETURN

    -- We verify if the ticket can be played in this terminal due to promotion restriction
    -- @_restricted could be:
    --                -  null : Reedemable ticket
    --                -  1    : Terminal isn't restricted, ticket can be played
    --                -  0    : Terminal is restricted, ticket can't be played
    SELECT   @_restricted = CASE WHEN PM_RESTRICTED_TO_TERMINAL_LIST IS NOT NULL 
                                 THEN ISNULL( (SELECT   1  FROM   TERMINAL_GROUPS  WHERE   TG_TERMINAL_ID  = @pTerminalId 
                                                                                     AND   TG_ELEMENT_ID   = @_promotion_id 
                                                                                     AND   TG_ELEMENT_TYPE = @_element_type), 0) 
                                 ELSE 1 END
      FROM   PROMOTIONS 
     WHERE   PM_PROMOTION_ID = @_promotion_id
    
    IF @_restricted = 0
      RETURN

    
    IF (@_dual_currency_enabled = 1) 
    BEGIN  
        SET    @_cur0     =  ISNULL(@_cur0,@_sys_iso_national_cur)                      
        IF  @_egm_iso = @_cur0 
            SET @_egm_amt = @_amt0
        ELSE
            SET @_egm_amt = dbo.ApplyExchange2(@_amt0, @_cur0, @_egm_iso)
                      
        SET @_sys_amt = dbo.ApplyExchange2(@_amt0, @_cur0, @_sys_iso_national_cur)
    END
      
    -- One ticket found, change its status
		UPDATE    TICKETS 
		   SET    TI_STATUS                    = @_ticket_status_pending_cancel 
				, TI_LAST_ACTION_TERMINAL_ID   = @pTerminalId
				, TI_LAST_ACTION_TERMINAL_TYPE = 1  --Always 1 from terminal (WCP)
				, TI_LAST_ACTION_DATETIME      = GETDATE() 
				, TI_AMOUNT                    = @_sys_amt
				, TI_AMT1                      = @_egm_amt
				, TI_CUR1                      = @_egm_iso 
				, TI_LAST_ACTION_ACCOUNT_ID    = @_last_action_account_id 
        , TI_REJECT_REASON_EGM         = @pTicketRejectReasonEgm
        , TI_REJECT_REASON_WCP         = @pTicketRejectReasonWcp
		   WHERE  TI_TICKET_ID                 = @_min_ticket_id 
			AND   TI_AMOUNT  > 0
			AND ( TI_EXPIRATION_DATETIME IS NULL OR TI_EXPIRATION_DATETIME > GETDATE () )
			AND ( @_max_ticket_amount = 0 OR TI_AMOUNT <= @_max_ticket_amount )
      AND ( @_min_ticket_amount = 0 OR TI_AMOUNT >= @_min_ticket_amount )
			AND   (TI_STATUS = @_ticket_status_valid or TI_STATUS = @_ticket_status_pending_cancel or TI_STATUS = @_ticket_status_pending_printing)
			
			   IF @@ROWCOUNT = 1
				SELECT    @pTicketId         = TI_TICKET_ID
						, @pTicketType       = TI_TYPE_ID
						, @pTicketAmount     = CASE WHEN @_dual_currency_enabled = 1 THEN TI_AMT1
                ELSE TI_AMOUNT
                END
					, @pTicketExpiration = TI_EXPIRATION_DATETIME
					FROM    TICKETS 
				WHERE    TI_TICKET_ID  = @_min_ticket_id 


      -- Alow truncate
      SET @_gp_allow_truncate       = (SELECT ISNULL(GP_KEY_VALUE, 0) FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'TITO' AND GP_SUBJECT_KEY = 'Tickets.AllowTruncate')
      SET @_terminal_allow_truncate = ISNULL(@_terminal_allow_truncate, @_gp_allow_truncate)
      SET @_is_truncate             = 0

      -- Min denomination
      SET @_gp_min_denom_amount     = (SELECT ISNULL(GP_KEY_VALUE, 0) FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'TITO' AND GP_SUBJECT_KEY = 'Tickets.MinDenominationMultiple')
      SET @_min_denom_amount        = ISNULL(@_min_denom_amount, @_gp_min_denom_amount)

      if (@_terminal_allow_truncate = 0 AND @_min_denom_amount > 0)
      BEGIN
		      SET @_is_truncate = @pTicketAmount % @_min_denom_amount
      END

      IF    @pTicketAmount < @_min_denom_amount                     -- TicketAmount is lower than Machine Min Denomination
        OR (@_terminal_allow_truncate = 0 AND @_is_truncate > 0)    -- Is Truncate and Not Alow Truncate
	    BEGIN

	      SET @pTicketId         = NULL
	      SET @pTicketType       = NULL
	      SET @pTicketAmount     = NULL
	      SET @pTicketExpiration = NULL

      END

  END

END

GO


IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'WS2S' AND GP_SUBJECT_KEY = 'CloseSessionToZero')
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('WS2S','CloseSessionToZero','0')
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Trx_3GS_UpdateCardSession]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Trx_3GS_UpdateCardSession]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Trx_3GS_StartCardSession]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Trx_3GS_StartCardSession]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Trx_3GS_SendEvent]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Trx_3GS_SendEvent]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Trx_3GS_EndCardSession]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Trx_3GS_EndCardSession]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Trx_3GS_AccountStatus]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Trx_3GS_AccountStatus]
GO

DROP ASSEMBLY [SQLBusinessLogic]
GO

CREATE ASSEMBLY [SQLBusinessLogic]
AUTHORIZATION [dbo]
FROM 0x4D5A90000300000004000000FFFF0000B800000000000000400000000000000000000000000000000000000000000000000000000000000000000000800000000E1FBA0E00B409CD21B8014CCD21546869732070726F6772616D2063616E6E6F742062652072756E20696E20444F53206D6F64652E0D0D0A2400000000000000504500006486020085A768590000000000000000F00022200B020B000098040000040000000000000000000000200000000000800100000000200000000200000400000000000000040000000000000000E00400000200000000000003004085000040000000000000400000000000000000100000000000002000000000000000000000100000000000000000000000000000000000000000C00400A00300000000000000000000000000000000000000000000000000001CB504001C0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000002000004800000000000000000000002E7465787400000054960400002000000098040000020000000000000000000000000000200000602E72737263000000A003000000C0040000040000009A0400000000000000000000000000400000402E72656C6F6300000000000000E0040000000000009E040000000000000000000000000040000042480000000200050038A50100E40F03000100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000013300700D0000000010000110073FF0000060A166A05281D01000616731100000A1612020E0428D6000006130411042D1700061F0A087BC50400046FFB00000600060D389200000002030412010E042843010006130411042D1300061F1472010000706FFB00000600060D2B6D0E0472030000706F1200000A00166A070816020E0428A10000060A067B5104000416FE01130411042D0F000E0472030000706F1300000A0000087BB3040004087BB8040004077BFF0400048C280000017219000070077BF2040004281400000A190E0428E000000626060D2B00092A1B300700DC0000000200001100730F0100060A166A0C160D0E0772210000706F1200000A0000020E07280A000006130511052D1600061872010000706F0B01000600061304DD9A0000000E04120212030E07280B000006130511052D1300061872010000706F0B01000600061304DE740805281D01000616731100000A1612010E0728D6000006130511052D1500061F0A077BC50400046F0B01000600061304DE4209070E040E050E060E0728070000060A061304DE2D00067B820400042C0B067B820400041AFE012B011700130511052D0F000E0772210000706F1300000A000000DC0011042A011000000200190092AB002D000000001B300700200200000300001100730F0100060A166A13041613050E0772390000706F1200000A00000E04120412050E07280B000006130811082D1600061872010000706F0B01000600061307DDD7010000110405281D01000616731100000A1612020E0728D6000006130811082D1800061F0A087BC50400046F0B01000600061307DDA1010000087BBD0400041733120E0616731100000A281500000A16FE012B011700130811082D47000E040E071206280C000006130811082D1000061F0D72010000706F0B0100060000110619FE0116FE01130811082D16000616724B0000706F0B01000600061307DD3801000000087BBC0400040E072842010006130811082D1700061F1472010000706F0B01000600061307DD0D010000087BBC04000412010E072845010006130811082D1700061F1472010000706F0B01000600061307DDE1000000087BBC040004080E040E050E060E0728070000060A067B820400042C0B067B820400041AFE012B011700130811082D0900061307DDA800000073000100060D09087BB30400047D55040004090E067D5904000409177D5A040004090E057D5B040004091C7D53040004090E047D5604000409166A7D57040004077BEF040004077BF1040004077BF2040004090212000E0728B0000006130811082D1300061872990000706F0B01000600061307DE32061307DE2D00067B820400042C0B067B820400041AFE012B011700130811082D0F000E0772390000706F1300000A000000DC0011072A411C0000020000001B000000D4010000EF0100002D000000000000001B30070019010000040000110073FF0000060A0572D30000706F1200000A00000305280A0000060D092D1500061772010000706FFB00000600060CDDE2000000166A02281D01000616731100000A1612010528D60000060D092D1700061F0A077BC50400046FFB00000600060CDDB0000000072C0F077BB3040004166AFE0116FE012B0116000D092D1600061F0A72010000706FFB00000600060CDD82000000077BB404000416FE010D092D1300061F0B72010000706FFB00000600060CDE62077BBB040004166AFE010D092D1F0006077BBB0400047D49040004061F0C72010000706FFB00000600060CDE3506077BC30400047D4D040004066FFD00000600060CDE1E00067B5104000416FE010D092D0E000572D30000706F1300000A000000DC00082A0000000110000002001300E5F8001E000000001B30070032030000050000110073FF0000060A0E0672F90000706F1200000A000003040512010E062843010006130811082D1700061F1472010000706FFB00000600061307DDF0020000072C0E077BEF04000416FE0116FE012B011600130811082D1700061F1472010000706FFB00000600061307DDC00200000E04130911091F0B6A30411109166A3F98000000110969450C0000003F0000002B00000030000000350000003A0000006000000060000000600000006000000060000000560000005B00000011091F126A301B11091F116A325211091F116A596945020000002C0000003000000011091F1D6A2E162B351F100D2B451F110D2B401F120D2B3B1F440D2B36066FFD00000600061307DD26020000170D2B23180D2B1F1F440D2B1A1F450D2B15061772170100706FFB00000600061307DDFF010000166A02281D01000616731100000A1612020E0628D600000626166A1305082C0C087BBB040004166AFE012B011700130811082D0A00087BBB040004130500731600000A1304110472390100706F1700000A26110472930100706F1700000A26110472EB0100706F1700000A261104723F0200706F1700000A26110472970200706F1700000A26110472F70200706F1700000A26110472590300706F1700000A26110472AF0300706F1700000A261104720B0400706F1700000A261104725B0400706F1700000A26110472AF0400706F1700000A261104720B0500706F1700000A2611046F1800000A0E066F1900000A0E06731A00000A13060011066F1B00000A725B0500701E6F1C00000A077BEF0400048C2D0000016F1D00000A0011066F1B00000A72750500701E6F1C00000A11058C280000016F1D00000A0011066F1B00000A72950500701E6F1C00000A188C2D0000016F1D00000A0011066F1B00000A72AD0500701E6F1C00000A098C2D0000016F1D00000A0011066F1B00000A72CD0500701F096F1C00000A0E058C080000016F1D00000A0011066F1E00000A17FE01130811082D1300061772DF0500706FFB00000600061307DE4500DE14110614FE01130811082D0811066F1F00000A00DC00066FFD00000600061307DE2100067B5104000416FE01130811082D0F000E0672F90000706F1300000A000000DC0011072A0000413400000200000027020000C5000000EC02000014000000000000000200000014000000F90200000D03000021000000000000001B300500B401000006000011000E041F0A6A2E03172B0116000D73FF0000060A02030412020E052843010006130611062D1700061F1472010000706FFB000006000613053873010000731600000A0B07721D0600706F1700000A2607729B0600706F1700000A260772190700706F1700000A260772970700706F1700000A260772150800706F1700000A260772930800706F1700000A260772110900706F1700000A2607728F0900706F1700000A2607720D0A00706F1700000A2607728B0A00706F1700000A260772090B00706F1700000A260772870B00706F1700000A26076F1800000A0E056F1900000A0E05731A00000A13040011046F1B00000A725B0500701E6F1C00000A087BEF0400048C2D0000016F1D00000A0011046F1B00000A72CD0500701F096F1C00000A058C080000016F1D00000A0011046F1B00000A72050C00701F0C6F1C00000A087BF20400046F1D00000A0011046F1B00000A72230C00701E6F1C00000A098C2D0000016F1D00000A0011046F1E00000A17FE01130611062D1300061772350C00706FFB00000600061305DE2400DE14110414FE01130611062D0811046F1F00000A00DC00066FFD000006000613052B000011052A011000000200E900A68F0114000000001B300600380400000700001100730F0100060A032C0F037BB3040004166AFE0116FE012B011600130A110A2D1700061F0A72010000706F0B0100060006130938FC030000037BBB04000404330B037BBD04000416FE012B011600130A110A2D3E00061F0D72670C00700F02282000000A037CBB040004282000000A037BBD0400048C110000026F1800000A282100000A6F0B0100060006130938A20300000E0416731100000A282200000A2D50057B6604000416731100000A282200000A2D3D057B65040004166A3233057B6804000416731100000A282200000A2D20057B67040004166A3216057B6A04000416731100000A282200000A16FE012B011600130A110A2D6D00061F1572E70C00701B8D01000001130B110B160F04282300000AA2110B17057C66040004282300000AA2110B18057C65040004282000000AA2110B19057C68040004282300000AA2110B1A057C67040004282000000AA2110B282400000A6F0B0100060006130938CE02000016731100000A0E04282500000A0D16731100000A037BBF040004057B66040004282600000A057B68040004282700000A057B6A040004282700000A282500000A0C0E0408282600000A1305110516731100000A282800000A1306161307110616FE01130A110A2D5B0072650D0070727F0D00700E0528F90000061204282900000A2611041F64731100000A282A00000A13041105282B00000A1104282C00000A16FE01130A110A2D190017130716731100000A080E04282D00000A282500000A0D000019040512010E0528AE000006130A110A2D1600061872B10D00706F0B0100060006130938E301000004090E0411061C0E0528F1000006130A110A2D1600061872FF0D00706F0B0100060006130938B90100000E05284C01000613080011086F1B00000A725B0500701E6F1C00000A028C2D0000016F1D00000A0011086F1B00000A724D0E00701F0C1F326F2E00000A726B0E00706F1D00000A0011086F1B00000A727D0E00701F096F1C00000A237B14AE47E17A843F8C310000016F1D00000A0011086F1B00000A729B0E0070166F1C00000A168C2D0000016F1D00000A0011086F1B00000A72BB0E0070166F1C00000A077B650400048C280000016F1D00000A0011086F1B00000A72E10E00701F096F1C00000A077B660400048C080000016F1D00000A0011086F1B00000A72090F0070166F1C00000A077B670400048C280000016F1D00000A0011086F1B00000A72290F00701F096F1C00000A077B680400048C080000016F1D00000A0011086F1B00000A724B0F00701F096F1C00000A168C2D0000016F1D00000A0011086F1E00000A17FE01130A110A2D1300061872750F00706F0B01000600061309DE5C00DE14110814FE01130A110A2D0811086F1F00000A00DC00110716FE01130A110A2D2D00061A72A70F00700F04282300000A1202282300000A1203282300000A282100000A6F0B010006000613092B0C066F0D010006000613092B000011092A411C0000020000008402000057010000DB0300001400000000000000133002008A000000080000110005026FFE00000651027B510400040A064502000000280000005F000000061F0A594503000000260000004A00000032000000061F145945020000002D0000002D0000002B3703165404721B10007051057201000070512B3003175404722B100070512B24031854047259100070512B1803195404727F100070512B0C031A540472B7100070512B002A000013300200CE000000090000110005026F0E01000651027B820400040A06450E000000130000006700000067000000670000007300000067000000670000006700000067000000670000004300000067000000670000005B000000061F145945020000003E0000003E0000002B5403165405507E2F00000A283000000A16FE010B072D0B0004721B10007051002B06000405505100057201000070512B3C03175404722B100070512B3003185404727F100070512B240319540472D3100070512B18031A540472B7100070512B0C031B54047207110070512B002A00001B300300A20000000A00001100140B731600000A0A000672591100706F1700000A260672A71100706F1700000A260672F51100706F1700000A26066F1800000A036F1900000A03731A00000A0C00086F1B00000A72431200701F0C6F1C00000A026F1D00000A00086F3100000A0B072C0A077E3200000AFE012B011700130411042D0500170DDE2300DE120814FE01130411042D07086F1F00000A00DC0000DE05260000DE0000160D2B0000092A0000011C0000020041003E7F001200000000000009008C950005010000011B300300CE0000000B00001100731600000A0A03166A55041654000672591200706F1700000A260672B71200706F1700000A260672151300706F1700000A26066F1800000A056F1900000A05731A00000A0B00076F1B00000A7273130070166F1C00000A028C280000016F1D00000A00076F3300000A0C00086F3400000A16FE01130411042D17000308166F3500000A550408176F3600000A54170DDE3900DE120814FE01130411042D07086F1F00000A00DC0000DE120714FE01130411042D07076F1F00000A00DC0000DE05260000DE0000160D2B0000092A00000128000002006B002A950012000000000200460065AB00120000000000000E00B3C10005010000011B300300960000000A0000110000731600000A0A0672931300706F1700000A260672EB1300706F1700000A260672431400706F1700000A26066F1800000A036F1900000A03731A00000A0C00086F1B00000A729B140070166F1C00000A028C280000016F1D00000A00086F3100000A0B0407A5110000025400DE120814FE01130411042D07086F1F00000A00DC00170DDE0D260000DE0000041754160D2B0000092A0000011C000002003F00306F001200000000000001008586000501000001133008006E0000000C000011000373130000060A1F310B06176A20930300006A071F65731100000A16731100000A16731100000A1F65731100000A6F170000062606186A20930300006A0720CA000000731100000A20CA000000731100000A16731100000A16731100000A6F170000062606046F21000006262A000013300800760000000C0000110003040573150000060A1B0B06196A20930300006A07202F010000731100000A16731100000A16731100000A202F010000731100000A6F1700000626061A6A20930300006A072094010000731100000A2094010000731100000A16731100000A16731100000A6F1700000626060E046F21000006262A1E02283700000A2A0000133002001A0000000D00001100027B05000004166AFE010A062D03002B0702037D050000042A0000133002002E0000000D0000110004283800000A16FE010A062D0F000272010000707D06000004002B100002047D0600000402037D07000004002A0000133005005B0200000E0000110002733900000A7D01000004027B010000046F3A00000A72B314007072D1140070283B00000A6F3C00000A0A06176F3D00000A0006156A6F3E00000A0006156A6F3F00000A00027B010000046F3A00000A72EB14007072D1140070283B00000A6F3C00000A26027B010000046F3A00000A720B15007072D1140070283B00000A6F3C00000A166F4000000A00027B010000046F3A00000A72271500707237150070283B00000A6F3C00000A166F4000000A00027B010000046F3A00000A72511500707277150070283B00000A6F3C00000A166F4000000A00027B010000046F3A00000A72951500707277150070283B00000A6F3C00000A166F4000000A00027B010000046F3A00000A72B11500707277150070283B00000A6F3C00000A166F4000000A00027B010000046F3A00000A72CD1500707277150070283B00000A6F3C00000A166F4000000A00027B010000046F3A00000A72EF1500707205160070283B00000A6F3C00000A176F4000000A00027B010000046F3A00000A72211600707205160070283B00000A6F3C00000A176F4000000A00027B010000046F3A00000A72371600707205160070283B00000A6F3C00000A176F4000000A00027B010000046F3A00000A72531600707237150070283B00000A6F3C00000A176F4000000A00027B010000046F3A00000A72711600707205160070283B00000A6F3C00000A176F4000000A00027B010000046F3A00000A72931600707205160070283B00000A6F3C00000A176F4000000A00027B01000004178D370000010B0716027B010000046F3A00000A72B31400706F4100000AA2076F4200000A002A0003300200840000000000000002147D0100000402147D0200000402167D030000040272010000707D0400000402166A7D0500000402147D0600000402166A7D070000040272010000707D0800000402283700000A00000228120000060002036F240000067D0200000402167D030000040272010000707D0400000402166A7D050000040272010000707D08000004002A133004008D0000000D00001102032813000006000005283800000A0A062D0E00027B02000004057D0C00000400027B020000047B1200000418FE010A062D580002047D0300000402027B020000047B0C0000047D04000004027B020000047B0F000004283800000A0A062D230002027B020000047B0F00000472B9160070027B04000004284300000A7D040000040002147D0200000400002A4A02030405720100007028160000060000002A03300200770000000000000002147D0100000402147D0200000402167D030000040272010000707D0400000402166A7D0500000402147D0600000402166A7D070000040272010000707D0800000402283700000A00000228120000060002147D0200000402037D0300000402047D0400000402057D05000004020E047D08000004002A0013300B00220000000D00001100020304050E040E050E060E077201000070157201000070281A0000060A2B00062A000013300D00290000000D00001100020304050E040E050E060E07720100007015720100007072010000700E08281C0000060A2B00062A00000013300B001F0000000D00001100020304050E040E050E060E070E08157201000070281A0000060A2B00062A0013300D00280000000D00001100020304050E040E050E060E070E080E090E0A7201000070027B05000004281C0000060A2B00062A13300D00250000000D00001100020304050E040E050E060E070E080E090E0A0E0B027B05000004281C0000060A2B00062A0000001B300300B90100000F0000110000027B010000046F4400000A0A0672EB140070038C280000016F4500000A0006720B150070048C280000016F4500000A00067227150070058C0D0000026F4500000A000672511500700E048C080000016F4500000A000672951500700E058C080000016F4500000A000672B11500700E068C080000016F4500000A000672CD1500700E078C080000016F4500000A000E08283800000A16FE010C082D15000672EF1500707E3200000A6F4500000A00002B10000672EF1500700E086F4500000A00000E0B283800000A16FE010C082D15000672211600707E3200000A6F4500000A00002B10000672211600700E0B6F4500000A00000E0915FE010C082D17000672531600700E098C2D0000016F4500000A00002B13000672531600707E3200000A6F4500000A00000E0A283800000A16FE010C082D15000672711600707E3200000A6F4500000A00002B10000672711600700E0A6F4500000A00000E0C166AFE010C082D17000672931600700E0C8C280000016F4500000A00002B1900067293160070027B050000048C280000016F4500000A0000027B010000046F4600000A066F4700000A00170BDE0A260000DE0000160B2B0000072A000000411C00000000000001000000AB010000AC01000005000000010000011B30030086000000100000110072010000700C0072BD1600700A06036F1900000A03731A00000A0D00096F1B00000A727E170070166F1C00000A028C280000016F1D00000A00096F3100000A0B00DE120914FE01130511052D07096F1F00000A00DC00072C0A077E3200000AFE012B011700130511052D09000774290000010C0000DE05260000DE00000813042B0011042A0000011C000002001C00284400120000000000000700717800053900000113300300560000001100001100027B010000046F4600000A6F4800000A16FE0216FE010B072D34027B010000046F4600000A027B010000046F4600000A6F4800000A17596F4900000A72B31400706F4A00000AA5280000010A2B05156A0A2B00062A3A00027B010000046F4B00000A002A00000013300200610000001100001100027B010000046F4600000A6F4800000A16311C0316321803027B010000046F4600000A6F4800000AFE0416FE012B0117000B072D2400027B010000046F4600000A036F4900000A72B31400706F4A00000AA5280000010A2B05156A0A2B00062A0000001B300400F50800001200001100729617007072AA1700700328F90000061203284C00000A260917FE010C00027B010000046F4600000A6F4800000A16FE0116FE01130B110B2D090017130ADDAD08000002281E000006166AFE0216FE01130B110B2D090017130ADD91080000027B0600000414FE0116FE01130B110B2D3000027B010000046F4600000A166F4900000A720B1500706F4A00000AA5280000011305110503281D0000061306002B1200027B070000041305027B0600000413060000027B010000046F4600000A6F4D00000A130C2B3F110C6F4E00000A740B00000113070011051107720B1500706F4A00000AA528000001FE0116FE01130B110B2D11001107723716007011066F4500000A000000110C6F4F00000A130B110B2DB4DE1D110C7505000001130D110D14FE01130B110B2D08110D6F1F00000A00DC00731600000A0A0672BC1700706F1700000A2606720C1800706F1700000A260672461800706F1700000A260672761800706F1700000A260672591900706F1700000A260672851900706F1700000A260816FE01130B110B2D62000672CF1900706F1700000A2606721D1A00706F1700000A260672211A00706F1700000A260672491A00706F1700000A260672A51A00706F1700000A2606721D1A00706F1700000A260672F11A00706F1700000A2606721D1A00706F1700000A26000672A61B00706F1700000A260672081C00706F1700000A2606726A1C00706F1700000A260672CC1C00706F1700000A2606722E1D00706F1700000A260672901D00706F1700000A260672F21D00706F1700000A260672541E00706F1700000A260672B61E00706F1700000A260672181F00706F1700000A2606727A1F00706F1700000A260672DC1F00706F1700000A2606723E2000706F1700000A260672A02000706F1700000A260672022100706F1700000A260672642100706F1700000A260672C62100706F1700000A260816FE01130B110B2D0E000672282200706F1700000A26000672862200706F1700000A2606728C2200706F1700000A260672EE2200706F1700000A260672502300706F1700000A260672B22300706F1700000A260672142400706F1700000A260672762400706F1700000A260672D82400706F1700000A2606723A2500706F1700000A2606729C2500706F1700000A260672FE2500706F1700000A260672602600706F1700000A260672172700706F1700000A260672792700706F1700000A260672DB2700706F1700000A2606723D2800706F1700000A2606729F2800706F1700000A260816FE01130B110B2D0E000672012900706F1700000A26000672862200706F1700000A260816FE01130B110B2D100006725F2900706F1700000A26002B0E000672AB2900706F1700000A2600066F1800000A036F1900000A03731A00000A13080011086F1B00000A72F5290070166F1C00000A72EB1400706F5000000A0011086F1B00000A727E170070166F1C00000A720B1500706F5000000A0011086F1B00000A72112A00701E6F1C00000A72271500706F5000000A0011086F1B00000A721F2A00701F096F1C00000A72511500706F5000000A0011086F1B00000A72412A00701F096F1C00000A72951500706F5000000A0011086F1B00000A72592A00701F096F1C00000A72B11500706F5000000A0011086F1B00000A72712A00701F096F1C00000A72CD1500706F5000000A0011086F1B00000A728F2A00701F0C20000100006F2E00000A72EF1500706F5000000A0011086F1B00000A72A32A00701F0C1F406F2E00000A72211600706F5000000A0011086F1B00000A72B72A00701F0C1F326F2E00000A72371600706F5000000A00027B0200000414FE01130B110B3A8201000000027B020000047B0A00000416FE0216FE01130B110B2D2C0011086F1B00000A72CD2A00701E6F1C00000A027B020000047B0A0000048C2D0000016F1D00000A00002B1F0011086F1B00000A72CD2A00701E6F1C00000A7E3200000A6F1D00000A0000027B020000047B0C000004283800000A130B110B3A8400000000027B020000047B0F000004283800000A16FE01130B110B2D2A0011086F1B00000A72E52A00701F0C1F326F2E00000A027B020000047B0C0000046F1D00000A00002B3D0011086F1B00000A72E52A00701F0C1F326F2E00000A027B020000047B0F00000472B9160070027B020000047B0C000004284300000A6F1D00000A0000002B1F0011086F1B00000A72E52A00701E6F1C00000A7E3200000A6F1D00000A000011086F1B00000A725B0500701E6F1C00000A72531600706F5000000A0011086F1B00000A72050C00701F0C1F326F2E00000A72711600706F5000000A0011086F1B00000A72750500701F0C1F326F2E00000A72931600706F5000000A0000384D0100000011086F1B00000A72CD2A00701E6F1C00000A7E3200000A6F1D00000A00027B08000004283800000A16FE01130B110B2D240011086F1B00000A72E52A00701F0C1F326F2E00000A7E3200000A6F1D00000A00002B230011086F1B00000A72E52A00701F0C1F326F2E00000A027B080000046F1D00000A0000027B0300000416FE0116FE01130B110B2D210011086F1B00000A725B0500701E6F1C00000A7E3200000A6F1D00000A00002B250011086F1B00000A725B0500701E6F1C00000A027B030000048C2D0000016F1D00000A0000027B04000004283800000A16FE01130B110B2D240011086F1B00000A72050C00701F0C1F326F2E00000A7E3200000A6F1D00000A00002B230011086F1B00000A72050C00701F0C1F326F2E00000A027B040000046F1D00000A000011086F1B00000A72750500701F0C1F326F2E00000A7E3200000A6F1D00000A00000816FE01130B110B2D210011086F1B00000A72012B00701E6F1C00000A1F0C8C2D0000016F1D00000A000011086F1B00000A72152B0070166F1C00000A1304110472B31400706F5000000A001104186F5100000A001108176F5200000A00735300000A130900110911086F5400000A001109027B010000046F5500000A0B07027B010000046F4600000A6F4800000AFE0116FE01130B110B2D060017130ADE3E00DE14110914FE01130B110B2D0811096F1F00000A00DC0000DE14110814FE01130B110B2D0811086F1F00000A00DC0000DE05260000DE000016130A2B0000110A2A0000004164000002000000C700000050000000170100001D0000000000000002000000780800003E000000B6080000140000000000000002000000FF030000CF040000CE0800001400000000000000000000001E000000C8080000E608000005000000010000011330060038000000130000110020842900006A0A1F32731100000A0B20030900006A0C0373290000060D09061F2B070872010000706F2B0000062609046F43000006262A1E02283700000A2A1330010011000000140000110002285600000A74060000020A2B00062A1E02283700000A2A000000133001000C0000001400001100027B150000040A2B00062A260002037D130000042A0000033003000F030000000000000002733900000A7D13000004027B130000046F3A00000A722F2B007072D1140070283B00000A6F3C00000A26027B130000046F3A00000A724F2B007072D1140070283B00000A6F3C00000A26027B130000046F3A00000A726B2B00707205160070283B00000A6F3C00000A26027B130000046F3A00000A72912B00707237150070283B00000A6F3C00000A166F4000000A00027B130000046F3A00000A72A12B00707277150070283B00000A6F3C00000A166F4000000A00027B130000046F3A00000A72C72B00707277150070283B00000A6F3C00000A166F4000000A00027B130000046F3A00000A72E32B00707277150070283B00000A6F3C00000A166F4000000A00027B130000046F3A00000A72FF2B00707277150070283B00000A6F3C00000A166F4000000A00027B130000046F3A00000A72212C00707277150070283B00000A6F3C00000A166F4000000A00027B130000046F3A00000A724B2C00707205160070283B00000A6F3C00000A176F4000000A00027B130000046F3A00000A72612C00707205160070283B00000A6F3C00000A176F4000000A00027B130000046F3A00000A728B2C00707277150070283B00000A6F3C00000A176F4000000A00027B130000046F3A00000A728B2C00706F4100000A168C2D0000016F5700000A00027B130000046F3A00000A72A72C00707277150070283B00000A6F3C00000A176F4000000A00027B130000046F3A00000A72A72C00706F4100000A168C2D0000016F5700000A00027B130000046F3A00000A72D92C007072D1140070283B00000A6F3C00000A176F4000000A00027B130000046F3A00000A720F2D007072D1140070283B00000A6F3C00000A176F4000000A00027B130000046F3A00000A72252D00707237150070283B00000A6F3C00000A176F4000000A00027B130000046F3A00000A72512D00707237150070283B00000A6F3C00000A176F4000000A00027B130000046F3A00000A726D2D007072D1140070283B00000A6F3C00000A176F4000000A00027B130000046F3A00000A72892D007072992D0070283B00000A6F3C00000A176F4000000A002ACA02147D1300000402147D1400000402147D1500000402283700000A00000228280000060002036F240000067D15000004002A000013300200230000000D00001102032829000006000004283800000A0A062D0E00027B15000004047D0C00000400002A0013300700180000000D00001100020304050E040E057201000070282C0000060A2B00062A133008001A0000000D00001100020304050E040E050E067201000070282E0000060A2B00062A0000133009001C0000000D00001100020304050E040E050E0672010000700E07282F0000060A2B00062A133009001D0000000D00001100020304050E040E050E060E0716731100000A28310000060A2B00062A00000013300A001F0000000D00001100020304050E040E050E060E0716731100000A0E0828300000060A2B00062A0013300C001F0000000D00001100020304050E040E050E060E070E08166A166A0E0928330000060A2B00062A0013300B001D0000000D00001100020304050E040E050E060E070E08166A166A28320000060A2B00062A00000013300C00230000000D00001100020304050E040E050E060E070E080E090E0A15731100000A28340000060A2B00062A0013300D00250000000D00001100020304050E040E050E060E070E080E090E0A15731100000A0E0B28350000060A2B00062A00000013301000350000001500001100020304050E040E050E060E070E080E090E0A0E0B16735800000A16735900000A1201FE150300001B071628360000060A2B00062A00000013301000360000001500001100020304050E040E050E060E070E080E090E0A0E0B16735800000A16735900000A1201FE150300001B070E0C28360000060A2B00062A00001B30060066030000160000110000020E097D160000040405120112020F050E0F2845000006000407080228260000067B120000041CFE0128460000060A02027B130000046F4400000A7D14000004027B14000004722F2B0070038C280000016F4500000A00027B1400000472912B0070048C0C0000026F4500000A00027B1400000472A12B0070168C2D0000016F4500000A00027B1400000472C72B0070078C080000016F4500000A00027B1400000472E32B0070088C080000016F4500000A00027B1400000472FF2B0070168C2D0000016F4500000A00027B1400000472212C0070068C080000016F4500000A00027B1400000472A72C00700E088C080000016F4500000A00027B1400000472D92C00700E0A8C280000016F4500000A000E06283800000A16FE01130411042D1A00027B14000004724B2C00707E3200000A6F4500000A00002B1500027B14000004724B2C00700E066F4500000A00000E05283800000A130411042D1700027B14000004726B2B00700E056F4500000A00002B1800027B14000004726B2B00707E3200000A6F4500000A00000E04166AFE01130411042D1C00027B14000004724F2B00700E048C280000016F4500000A00002B1800027B14000004724F2B00707E3200000A6F4500000A00000E077201000070285A00000A16FE01130411042D1700027B1400000472612C00700E076F4500000A00002B1800027B1400000472612C00707E3200000A6F4500000A00000E0B16731100000A285B00000A16FE01130411042D1C00027B14000004728B2C00700E0B8C080000016F4500000A00002B1800027B14000004728B2C00707E3200000A6F4500000A00000F0D285C00000A16FE01130411042D1C00027B1400000472252D00700E0D8C0200001B6F4500000A00002B1800027B1400000472252D00707E3200000A6F4500000A00000F0E285D00000A16FE01130411042D1C00027B14000004720F2D00700E0E8C0300001B6F4500000A00002B1800027B14000004720F2D00707E3200000A6F4500000A00000F0C285E00000A16FE01130411042D1C00027B1400000472512D00700E0C8C0100001B6F4500000A00002B1800027B1400000472512D00707E3200000A6F4500000A0000027B1400000472892D00707E3200000A6F4500000A00027B130000046F4600000A027B140000046F4700000A00170DDE0A260000DE0000160D2B0000092A0000411C000000000000010000005803000059030000050000000100000113300600170000000D00001100020304050E0416731100000A28380000060A2B00062A00133007002F0100001700001100160C0E046F660000061304110445030000009200000005000000B300000038CF0000000E046F68000006130511051959450200000002000000290000002B4E08130611062D100020930000000A20950000000B002B0E0020050200000A20060200000B002B4808130611062D100020940000000A20960000000B002B0E0020070200000A20080200000B002B2108130611062D0A001F4F0A1F580B002B0E0020030200000A20040200000B002B002B4608130611062D0A001F4E0A1F570B002B0E00200A0200000A200B0200000B002B2508130611062D0A001F5C0A1F5D0B002B0E00200C0200000A200D0200000B002B04160D2B36020304050E04060E05283B000006130611062D0500160D2B1D020304050E04070E05283B000006130611062D0500160D2B04170D2B00092A00133007009D00000018000011000E046F6600000616FE010C082D07160B38850000000E046F4B0000060E05283000000A2C130E046F4D0000060E05285A00000A16FE012B0117000C082D08204D0200000A2B350E046F4B0000060E05285A00000A2C130E046F4D0000060E05283000000A16FE012B0117000C082D08204B0200000A2B04160B2B1F020304050E040616731100000A283B0000060C082D0500160B2B04170B2B00072A00000013300700FF0000001900001100160B0E046F660000060D0945030000000500000023000000A000000038B600000007130411042D0A0020970000000A002B0800200E0200000A00389C0000000E046F68000006130511054505000000020000005300000002000000380000001D0000002B5107130411042D0A0020980000000A002B0800200F0200000A002B3A07130411042D0A00209A0000000A002B080020110200000A002B1F07130411042D0A0020990000000A002B080020100200000A002B04160C2B422B1F07130411042D0A00209B0000000A002B080020120200000A002B04160C2B21020304050E040616731100000A283B000006130411042D0500160C2B04170C2B00082A0013300300CD0400001A0000110002027B130000046F4400000A7D14000004027B14000004722F2B0070038C280000016F4500000A00027B1400000472912B00700E058C0C0000026F4500000A0004166AFE010C082D1B00027B14000004724F2B0070048C280000016F4500000A00002B1800027B14000004724F2B00707E3200000A6F4500000A000005283800000A0C082D1600027B14000004726B2B0070056F4500000A00002B1800027B14000004726B2B00707E3200000A6F4500000A0000027B14000004724B2C00707E3200000A6F4500000A00027B1400000472212C0070168C2D0000016F4500000A00027B1400000472A12B00700E046F470000068C080000016F4500000A00027B1400000472C72B00700E046F550000068C080000016F4500000A00027B1400000472612C00700E046F4B0000066F4500000A000E051F4F3BB90000000E0520930000003BAD0000000E0520940000003BA10000000E051F4E3B980000000E051F5C3B8F0000000E0520970000003B830000000E0520980000002E7A0E0520990000002E710E05209A0000002E680E05209B0000002E5F0E0520030200002E560E0520050200002E4D0E0520070200002E440E05200A0200002E3B0E05200C0200002E320E05200E0200002E290E05200F0200002E200E0520100200002E170E0520110200002E0E0E052012020000FE0116FE012B0116000C083AE200000000027B1400000472E32B00700E046F570000068C080000016F4500000A00027B1400000472FF2B00700E046F5A0000068C080000016F4500000A00027B14000004728B2C00700E046F5E0000068C080000016F4500000A000E0520970000002E560E0520980000002E4D0E0520990000002E440E05209A0000002E3B0E05209B0000002E320E05200E0200002E290E05200F0200002E200E0520100200002E170E0520110200002E0E0E052012020000FE0116FE012B0116000C082D1F00027B1400000472FF2B00700E046F590000068C080000016F4500000A00000038830100000E05204B020000FE0116FE010C082D5E00027B1400000472E32B00700E046F570000068C080000016F4500000A00027B1400000472FF2B00700E046F590000068C080000016F4500000A00027B14000004728B2C00700E046F5E0000068C080000016F4500000A000038150100000E05204D020000FE0116FE010C083AB5000000000E046F470000060E046F6C000006282600000A0A027B1400000472A12B00700E046F590000068C080000016F4500000A00027B1400000472E32B00700E046F570000068C080000016F4500000A00027B1400000472C72B0070068C080000016F4500000A00027B1400000472FF2B0070068C080000016F4500000A00027B14000004728B2C00700E046F5E0000068C080000016F4500000A00027B1400000472612C00700E046F4D0000066F4500000A00002B4D00027B1400000472E32B0070168C2D0000016F4500000A00027B1400000472FF2B00700E046F5C0000068C080000016F4500000A00027B14000004728B2C0070168C2D0000016F4500000A0000027B1400000472A72C00700E068C080000016F4500000A00027B1400000472D92C0070168C2D0000016F4500000A00027B1400000472512D00700E046F660000068C240000026F4500000A00027B130000046F4600000A027B140000046F4700000A00170B2B00072A00000013300300280000000D00001100027B1400000414FE010A062D1900027B14000004726D2D0070038C280000016F4500000A00002A13300300280000000D00001100027B1400000414FE010A062D1900027B1400000472E32B0070038C080000016F4500000A00002A13300300280000000D00001100027B1400000414FE010A062D1900027B1400000472C72B0070038C080000016F4500000A00002A13300300280000000D00001100027B1400000414FE010A062D1900027B1400000472A12B0070038C080000016F4500000A00002A13300300280000000D00001100027B1400000414FE010A062D1900027B1400000472892D0070038C0A0000016F4500000A00002A133003003F0000000D00001100027B1400000414FE010A062D3000027B1400000472A12B0070038C080000016F4500000A00027B1400000472FF2B0070048C080000016F4500000A00002A0013300200260000001B00001100150A027B1400000414FE010C082D1000027B140000046F5F00000A8E690A00060B2B00072A00001B300E00B70900001C0000110017130E00027B130000046F4600000A6F4800000A16FE0116FE01131511152D0900171314DD89090000166A130A7201000070130B00027B130000046F4600000A6F4D00000A131638B000000011166F4E00000A740B000001130F00110F724F2B00706F6000000A16FE01131511152D06003886000000110F726B2B00706F6000000A131511152D03002B71110A166AFE0116FE01131511152D3000110F724F2B00706F4A00000AA528000001130A110A03281D000006130B110F726B2B0070110B6F4500000A00002B3100110A110F724F2B00706F4A00000AA528000001FE0116FE01131511152D1100110F726B2B0070110B6F4500000A0000000011166F4F00000A131511153A40FFFFFFDE1D111675050000011317111714FE01131511152D0811176F1F00000A00DC000203120012021203120412051206120712081209120C120D120E284400000600110E131511152D0900161314DD5508000006166F6100000A000672B92D00706F1700000A260672BC1700706F1700000A2606720C1800706F1700000A260672052E00706F1700000A2606728C2E00706F1700000A260672761800706F1700000A260672591900706F1700000A260672851900706F1700000A260672F42E00706F1700000A2606724C2F00706F1700000A260672A42F00706F1700000A260672FC2F00706F1700000A260672543000706F1700000A260672AC3000706F1700000A260672043100706F1700000A2606725C3100706F1700000A260672B43100706F1700000A2606720C3200706F1700000A260672643200706F1700000A260672BC3200706F1700000A260672143300706F1700000A2606726C3300706F1700000A260672C43300706F1700000A2606721C3400706F1700000A260672743400706F1700000A260672CC3400706F1700000A260672243500706F1700000A2606727C3500706F1700000A260672D43500706F1700000A2606722C3600706F1700000A260672843600706F1700000A260672DC3600706F1700000A260672343700706F1700000A2606728C3700706F1700000A260672E43700706F1700000A2606723C3800706F1700000A260672943800706F1700000A260672EC3800706F1700000A260672443900706F1700000A2606729C3900706F1700000A260672F43900706F1700000A2606724C3A00706F1700000A260672A43A00706F1700000A260672FC3A00706F1700000A260672543B00706F1700000A260672AC3B00706F1700000A260672043C00706F1700000A2606725C3C00706F1700000A260672B43C00706F1700000A2606720C3D00706F1700000A260672643D00706F1700000A260672BC3D00706F1700000A260672143E00706F1700000A2606726C3E00706F1700000A260672DC3600706F1700000A260672C43E00706F1700000A2606721C3F00706F1700000A260672CB3F00706F1700000A260672D73F00706F1700000A260672374000706F1700000A260672594000706F1700000A2606729F4000706F1700000A260672E14000706F1700000A260672034100706F1700000A2606720B4100706F1700000A260672214100706F1700000A260672814100706F1700000A260672E14100706F1700000A260672964200706F1700000A2606725F4300706F1700000A260672424400706F1700000A260672564400706F1700000A260672724400706F1700000A260672CC4400706F1700000A260672124500706F1700000A260672524500706F1700000A2606726C4500706F1700000A260672C04500706F1700000A260672184600706F1700000A260672644600706F1700000A260672A24600706F1700000A260672E24600706F1700000A2606721C4700706F1700000A26066F1800000A036F1900000A03731A00000A13100011106F1B00000A72F5290070166F1C00000A722F2B00706F5000000A0011106F1B00000A727E170070166F1C00000A724F2B00706F5000000A0011106F1B00000A72B72A00701F0C1F326F2E00000A726B2B00706F5000000A0011106F1B00000A72112A00701E6F1C00000A72912B00706F5000000A0011106F1B00000A721F2A00701F096F1C00000A72A12B00706F5000000A0011106F1B00000A72592A00701F096F1C00000A72C72B00706F5000000A0011106F1B00000A72412A00701F096F1C00000A72E32B00706F5000000A0011106F1B00000A72712A00701F096F1C00000A72FF2B00706F5000000A0011106F1B00000A72324700701F096F1C00000A72212C00706F5000000A0011106F1B00000A728F2A00701F0C20000100006F2E00000A724B2C00706F5000000A0011106F1B00000A72584700701F0C6F1C00000A72612C00706F5000000A0011106F1B00000A72764700701F096F1C00000A728B2C00706F5000000A0011106F1B00000A728E4700701F0C6F1C00000A11086F1D00000A00027B150000047B09000004166AFE01131511152D2C0011106F1B00000A72BC470070166F1C00000A027B150000047B090000048C280000016F1D00000A00002B1F0011106F1B00000A72BC470070166F1C00000A72D44700706F5000000A000011106F1B00000A72CD2A0070166F1C00000A027B150000047B0A0000048C2D0000016F1D00000A0011106F1B00000A72E52A00701F0C1F326F2E00000A027B150000047B0C0000046F1D00000A0011106F1B00000A72F0470070166F1C00000A027B150000047B0E0000048C2D0000016F1D00000A0011106F1B00000A72024800701F0C1F326F2E00000A027B150000047B0F0000046F1D00000A0011106F1B00000A72184800701E6F1C00000A168C2D0000016F1D00000A0011106F1B00000A722C4800701F096F1C00000A72A72C00706F5000000A0011106F1B00000A725A480070166F1C00000A72D92C00706F5000000A0011106F1B00000A72884800701E6F1C00000A20C80000008C0C0000026F1D00000A0011106F1B00000A72AA4800701E6F1C00000A202B0100008C0C0000026F1D00000A0011106F1B00000A72CA480070166F1C00000A027B160000048C280000016F1D00000A0011106F1B00000A72F0480070166F1C00000A720F2D00706F5000000A0011106F1B00000A72024900701E6F1C00000A72252D00706F5000000A0011106F1B00000A722C490070166F1C00000A726D2D00706F5000000A0011106F1B00000A72444900701A6F1C00000A72892D00706F5000000A001110166F5200000A00735300000A131100111111106F5400000A00111120F40100006F6200000A001111027B130000046F5500000A0B07027B130000046F4600000A6F4800000AFE0116FE01131511152D0600171314DE4500DE14111114FE01131511152D0811116F1F00000A00DC0000DE14111014FE01131511152D0811106F1F00000A00DC0000DE0C13120011127A13130011137A001613142B000011142A00417C00000200000048000000C70000000F0100001D0000000000000002000000260900004B000000710900001400000000000000020000005E0500002B04000089090000140000000000000000000000040000009D090000A1090000060000004000000100000000040000009D090000A709000006000000390000011B300400132200001D000011000E0D17520E0A155404731600000A51045072524900706F1700000A260450728C4900706F1700000A260450729C4900706F1700000A26045072DA4900706F1700000A260450725A4A00706F1700000A260450729A4A00706F1700000A26045072DE4A00706F1700000A26045072364B00706F1700000A26045072904B00706F1700000A26045072F84B00706F1700000A260450725C4C00706F1700000A26045072704C00706F1700000A26045072BC4C00706F1700000A26045072CE4C00706F1700000A26045072064D00706F1700000A26045072724D00706F1700000A26045072DE4D00706F1700000A26045072464E00706F1700000A26045072AA4E00706F1700000A26045072064F00706F1700000A26045072A34F00706F1700000A26045072ED4F00706F1700000A26045072395000706F1700000A26045072A55000706F1700000A260450720D5100706F1700000A260450726F5100706F1700000A26045072A35100706F1700000A26045072FF5100706F1700000A26045072615200706F1700000A26045072DF5200706F1700000A260450725F5300706F1700000A26045072EE5300706F1700000A26045072795400706F1700000A26045072B15400706F1700000A26045072C15400706F1700000A26045072235500706F1700000A26045072695500706F1700000A26045072AB5500706F1700000A26045072E55500706F1700000A26045072255600706F1700000A260450726B5600706F1700000A26045072935600706F1700000A26045072C35600706F1700000A26045072155700706F1700000A260450723F5700706F1700000A260450727F5700706F1700000A26045072BB5700706F1700000A26045072F15700706F1700000A260450724F5800706F1700000A260450726B5600706F1700000A260450727F5800706F1700000A2604506F1800000A036F1900000A03731A00000A0A00066F1B00000A72BC470070166F1C00000A027B150000047B090000048C280000016F1D00000A00066F1B00000A728F5800701E6F1C00000A168C220000026F1D00000A00066F1B00000A72A95800701E6F1C00000A188C220000026F1D00000A00066F1B00000A72D15800701E6F1C00000A198C220000026F1D00000A0005066F1B00000A72F15800701F166F1C00000A510E04066F1B00000A72112A00701E6F1C00000A510E05066F1B00000A72324700701F096F1C00000A510E06066F1B00000A72055900701F096F1C00000A510E07066F1B00000A723B590070186F1C00000A510E08066F1B00000A7261590070186F1C00000A510E09729B59007072BB5900700328F9000006510E0B72DB59007072F55900700328F9000006510E0C0E0B50720F5A0070283000000A5200027B130000046F4600000A6F4D00000A0D38CD1D0000096F4E00000A740B0000010B000E0D4616FE01130411043AB01D00000005500772612C00706F4A00000A6F1D00000A0005506F6300000A6F1800000A283800000A16FE01130411042D0D0005500E09506F1D00000A00000E04500772512D00706F4A00000A6F1D00000A000E0750178C2D0000016F1D00000A000E0850168C2D0000016F1D00000A000E0650168C2D0000016F1D00000A000E0A0772912B00706F4A00000AA50C000002540E0A4A1305110520A10000003D0601000011051F23302E110545040000004B060000DB060000DB060000DB06000011051F0D3B0F0E000011051F233B2918000038A318000011051F58304411051F273B1518000011051F4E59450B00000055030000550300005E1800005E1800005E1800005E1800005E1800002D0300002D0300005503000055030000385918000011051F5C59450800000026030000260300002F1800002F1800002F1800003C080000910800006B0500001105208F00000059451300000091020000D6170000D6170000D6170000CD020000CD020000CD020000CD020000E70A0000E70A0000E70A0000E70A0000E70A0000370B0000D6170000D6170000D6170000BF060000BF06000038D1170000110520CE000000303F110520BE000000594502000000D40500004D0A0000110520C90000003B49070000110520CC000000594503000000690F00008E170000690F000038891700001105206A0200003D1B0200001105202F01000059450800000007080000030900000B150000E915000050170000501700006708000053090000110520F40100005945770000008803000001080000671500006715000067150000671500006715000067150000671500006715000067150000671500006715000067150000671500005E0000005E0000005E0000005E0000005E0000005E000000671500005E0000005E0000005E0000005E000000671500007808000078080000780800007808000067150000060B0000060B0000060B0000060B0000060B0000060B0000060B0000060B0000060B0000671500006715000067150000060B0000060B0000060B0000060B0000060B0000060B0000060B0000060B0000060B00008803000088030000A40E000067150000D50D00006715000067150000D50D0000671500006715000067150000420D0000420D0000671500006715000067150000671500006715000067150000890C00005E0000005E0000005E000000671500006715000067150000671500006715000067150000671500006715000067150000671500006715000020110000780F0000A8110000511000006715000035120000060B0000060B0000060B0000060B00000E1300000E130000671500006715000067150000671500006715000067150000671500006715000067150000671500006715000067150000210200002102000062020000620200006202000062020000100C0000100C0000386215000011052040420F003BEE08000011052080841E003BC009000038451500000E0550168C2D0000016F1D00000A0038151600000E05500772C72B00706F4A00000AA508000001286400000A8C080000016F1D00000A0038ED1500000E05500772FF2B00706F4A00000AA508000001286400000A8C080000016F1D00000A000E0A4A1F4E2E770E0A4A1F4F2E700E0A4A20930000002E660E0A4A20940000002E5C0E0A4A1F5C2E550E0A4A20030200002E4B0E0A4A20050200002E410E0A4A20070200002E370E0A4A200A0200002E2D0E0A4A200C0200002E230E0A4A203F0200002E190E0A4A204B0200002E0F0E0A4A204D020000FE0116FE012B011600130411042D16000E06500772A12B00706F4A00000A6F1D00000A00000E0850178C2D0000016F1D00000A000772612C00706F6000000A2D1D0772612C00706F4A00000A74290000010E0950283000000A16FE012B011700130411043ABB000000000E0A4A130511051F62303711051F4F3B8300000011051F582E7D11051F5C594507000000680000006800000079000000790000007900000057000000680000002B7711052093000000594504000000380000003800000038000000380000001105200302000059450500000017000000390000001700000039000000170000001105200C02000059450200000013000000130000002B220E0450178C2D0000016F1D00000A002B110E0450188C2D0000016F1D00000A002B0000382A1400000E0850168C2D0000016F1D00000A000E0750178C2D0000016F1D00000A000E05500772E32B00706F4A00000AA5080000018C080000016F1D00000A0038E91300000E0850168C2D0000016F1D00000A000E0750178C2D0000016F1D00000A000E05500772C72B00706F4A00000AA5080000018C080000016F1D00000A0038A81300000E0850178C2D0000016F1D00000A000E0750168C2D0000016F1D00000A000E0550168C2D0000016F1D00000A000E06500772E32B00706F4A00000AA508000001286400000A8C080000016F1D00000A0038531300000E05500772212C00706F4A00000A6F1D00000A000772612C00706F6000000A2D1F0772612C00706F4A00000A74290000017201000070285A00000A16FE012B011700130411042D43000E0550168C2D0000016F1D00000A000E06500772212C00706F4A00000A6F1D00000A000E0850178C2D0000016F1D00000A000E0750168C2D0000016F1D00000A000038C31200000E06500772212C00706F4A00000A6F1D00000A000E05500772212C00706F4A00000A6F1D00000A000772612C00706F6000000A2D1A0772612C00706F4A00000A74290000010E0950285A00000A2D3D0772612C00706F6000000A2D2C0772612C00706F4A00000A74290000010E0950283000000A2C120E04506F6300000AA52400000216FE012B0117002B011600130411042D2F000E0550168C2D0000016F1D00000A000E0850178C2D0000016F1D00000A000E0750168C2D0000016F1D00000A000038FB1100000E06500772212C00706F4A00000A6F1D00000A000E05500772212C00706F4A00000A6F1D00000A000772612C00706F6000000A2D480772612C00706F4A00000A74290000017201000070285A00000A2C2C0772612C00706F4A00000A74290000010E0950283000000A2C120E04506F6300000AA52400000216FE012B011700130411042D2F000E0550168C2D0000016F1D00000A000E0850178C2D0000016F1D00000A000E0750168C2D0000016F1D00000A000038421100000772A12B00700772C72B00706F4A00000AA5080000018C080000016F4500000A000772FF2B00700772C72B00706F4A00000AA5080000018C080000016F4500000A000772212C0070168C2D0000016F4500000A000772C72B0070168C2D0000016F4500000A0038D71000000E06500772212C00706F4A00000A6F1D00000A000E0550168C2D0000016F1D00000A000E0850178C2D0000016F1D00000A000E0750168C2D0000016F1D00000A000E0450178C2D0000016F1D00000A0038821000000E06500772212C00706F4A00000A6F1D00000A000E0550168C2D0000016F1D00000A000E0850178C2D0000016F1D00000A000E0750168C2D0000016F1D00000A000E0450188C2D0000016F1D00000A00382D1000000E0850178C2D0000016F1D00000A000E0750178C2D0000016F1D00000A000E0550168C2D0000016F1D00000A000E0C46130411042D25000E06500772E32B00706F4A00000AA508000001286400000A8C080000016F1D00000A000038CD0F00000772612C00706F6000000A2D1A0772612C00706F4A00000A74290000010E0950285A00000A2D120E04506F6300000AA52400000216FE012B011600130411042D54000E0750168C2D0000016F1D00000A000E0850178C2D0000016F1D00000A000E0550168C2D0000016F1D00000A000E06500772A12B00706F4A00000AA508000001286400000A8C080000016F1D00000A00002B02000038310F00000E0850178C2D0000016F1D00000A000E0750178C2D0000016F1D00000A000E0550168C2D0000016F1D00000A000E06500772C72B00706F4A00000AA5080000018C080000016F1D00000A0038E10E00000772612C00706F6000000A2D1A0772612C00706F4A00000A74290000010E0950285A00000A2D120E04506F6300000AA52400000216FE012B011600130411042D4F000E0750168C2D0000016F1D00000A000E0850178C2D0000016F1D00000A000E0550168C2D0000016F1D00000A000E06500772A12B00706F4A00000AA5080000018C080000016F1D00000A00002B020000384A0E00000E0650168C2D0000016F1D00000A000E0550168C2D0000016F1D00000A000772612C00706F6000000A2D1F0772612C00706F4A00000A74290000017201000070285A00000A16FE012B011700130411042D20000E0850178C2D0000016F1D00000A000E0750168C2D0000016F1D00000A000038D30D00000E0750168C2D0000016F1D00000A000E0550168C2D0000016F1D00000A000E0850178C2D0000016F1D00000A000E06500772A12B00706F4A00000AA5080000018C080000016F1D00000A0038830D00000E0750178C2D0000016F1D00000A000E05500772E32B00706F4A00000AA508000001286400000A8C080000016F1D00000A00384C0D00000E0750178C2D0000016F1D00000A000E0850168C2D0000016F1D00000A000E05500772C72B00706F4A00000AA5080000018C080000016F1D00000A000E0650168C2D0000016F1D00000A000772612C00706F6000000A2D1A0772612C00706F4A00000A74290000010E0950285A00000A2D120E04506F6300000AA52400000216FE012B011600130411042D4D000E0750168C2D0000016F1D00000A000E0850178C2D0000016F1D00000A000E0550168C2D0000016F1D00000A000E06500772C72B00706F4A00000AA5080000018C080000016F1D00000A0000386E0C00000E0750178C2D0000016F1D00000A000E0850168C2D0000016F1D00000A000E05500772E32B00706F4A00000AA508000001286400000A8C080000016F1D00000A000E0650168C2D0000016F1D00000A000772612C00706F6000000A2D1A0772612C00706F4A00000A74290000010E0950285A00000A2D120E04506F6300000AA52400000216FE012B011600130411042D52000E0750168C2D0000016F1D00000A000E0850178C2D0000016F1D00000A000E0550168C2D0000016F1D00000A000E06500772E32B00706F4A00000AA508000001286400000A8C080000016F1D00000A000038860B00000E06500772C72B00706F4A00000AA5080000018C080000016F1D00000A000E05500772C72B00706F4A00000AA5080000018C080000016F1D00000A0038450B00000E0750168C2D0000016F1D00000A000E0550168C2D0000016F1D00000A000E0850178C2D0000016F1D00000A000E0450178C2D0000016F1D00000A000772612C00706F6000000A16FE01130411042D24000772612C00700E09506F4500000A0005500772612C00706F4A00000A6F1D00000A00000E0A4A20150200002E0F0E0A4A2021020000FE0116FE012B011600130411042D20000E0450168C2D0000016F1D00000A000E0650168C2D0000016F1D00000A00000E0A4A20140200002E2D0E0A4A20200200002E230E0A4A20190200002E190E0A4A20250200002E0F0E0A4A2054020000FE0116FE012B011600130411042D11000E0450188C2D0000016F1D00000A0000383B0A00000E0750168C2D0000016F1D00000A000E0550168C2D0000016F1D00000A000E0850178C2D0000016F1D00000A000E0450178C2D0000016F1D00000A000772612C00706F6000000A16FE01130411042D24000772612C00700E09506F4500000A0005500772612C00706F4A00000A6F1D00000A000038C20900000E0450168C2D0000016F1D00000A000E0750168C2D0000016F1D00000A000E0550168C2D0000016F1D00000A000E0850168C2D0000016F1D00000A000E0650168C2D0000016F1D00000A0005506F6300000A6F1800000A0E0950283000000A16FE01130411042D27000E0750178C2D0000016F1D00000A000E05500772212C00706F4A00000A6F1D00000A00002B25000E0850178C2D0000016F1D00000A000E06500772212C00706F4A00000A6F1D00000A000038090900000E0750178C2D0000016F1D00000A000E0850168C2D0000016F1D00000A000E0550168C2D0000016F1D00000A000E0650168C2D0000016F1D00000A000772612C00706F6000000A2D1D0772612C00706F4A00000A74290000010E0950285A00000A16FE012B011700130411042D20000E0750168C2D0000016F1D00000A000E0850178C2D0000016F1D00000A000038760800000E0750178C2D0000016F1D00000A000E0850168C2D0000016F1D00000A000E05500772C72B00706F4A00000AA5080000018C080000016F1D00000A000E0650168C2D0000016F1D00000A000772612C00706F6000000A2D1D0772612C00706F4A00000A74290000010E0950285A00000A16FE012B011700130411042D4D000E0750168C2D0000016F1D00000A000E0850178C2D0000016F1D00000A000E0550168C2D0000016F1D00000A000E06500772C72B00706F4A00000AA5080000018C080000016F1D00000A000038A70700000E0750178C2D0000016F1D00000A000E0850168C2D0000016F1D00000A000E05500772E32B00706F4A00000AA508000001286400000A8C080000016F1D00000A000E0650168C2D0000016F1D00000A000772612C00706F6000000A2D1D0772612C00706F4A00000A74290000010E0950285A00000A16FE012B011700130411042D4D000E0750168C2D0000016F1D00000A000E0850178C2D0000016F1D00000A000E0550168C2D0000016F1D00000A000E06500772E32B00706F4A00000AA5080000018C080000016F1D00000A000038D30600000E0750178C2D0000016F1D00000A000E0850168C2D0000016F1D00000A000E05500772E32B00706F4A00000AA508000001286400000A8C080000016F1D00000A000E0650168C2D0000016F1D00000A000772612C00706F6000000A2D1D0772612C00706F4A00000A74290000010E0950285A00000A16FE012B011700130411042D52000E0750168C2D0000016F1D00000A000E0850178C2D0000016F1D00000A000E0550168C2D0000016F1D00000A000E06500772E32B00706F4A00000AA508000001286400000A8C080000016F1D00000A000038FA0500000E0750178C2D0000016F1D00000A000E0850168C2D0000016F1D00000A000E05500772C72B00706F4A00000AA5080000018C080000016F1D00000A000E0650168C2D0000016F1D00000A000772612C00706F6000000A2D1D0772612C00706F4A00000A74290000010E0950285A00000A16FE012B011700130411042D4D000E0750168C2D0000016F1D00000A000E0850178C2D0000016F1D00000A000E0550168C2D0000016F1D00000A000E06500772C72B00706F4A00000AA5080000018C080000016F1D00000A0000382B0500000772612C00706F6000000A2D1D0772612C00706F4A00000A74290000010E0950285A00000A16FE012B011700130411042D4F000E0750168C2D0000016F1D00000A000E0850178C2D0000016F1D00000A000E0550168C2D0000016F1D00000A000E06500772A12B00706F4A00000AA5080000018C080000016F1D00000A00002B02000038A30400000772612C00706F6000000A2D1D0772612C00706F4A00000A74290000010E0950285A00000A16FE012B011700130411042D54000E0750168C2D0000016F1D00000A000E0850178C2D0000016F1D00000A000E0550168C2D0000016F1D00000A000E06500772A12B00706F4A00000AA508000001286400000A8C080000016F1D00000A00002B02000038160400000E0750178C2D0000016F1D00000A000E0850168C2D0000016F1D00000A000E05500772E32B00706F4A00000AA508000001286400000A8C080000016F1D00000A000E0650168C2D0000016F1D00000A000772612C00706F6000000A2D1D0772612C00706F4A00000A74290000010E0950285A00000A16FE012B011700130411042D52000E0750168C2D0000016F1D00000A000E0850178C2D0000016F1D00000A000E0550168C2D0000016F1D00000A000E06500772E32B00706F4A00000AA508000001286400000A8C080000016F1D00000A0000383D0300000E0550168C2D0000016F1D00000A0038290300000E0750178C2D0000016F1D00000A000E0850168C2D0000016F1D00000A000E05500772C72B00706F4A00000AA5080000018C080000016F1D00000A000E0650168C2D0000016F1D00000A000772612C00706F6000000A2D1A0772612C00706F4A00000A74290000010E0950285A00000A2D120E04506F6300000AA52400000216FE012B011600130411042D4D000E0750168C2D0000016F1D00000A000E0850178C2D0000016F1D00000A000E0550168C2D0000016F1D00000A000E06500772C72B00706F4A00000AA5080000018C080000016F1D00000A0000384B0200000E0750178C2D0000016F1D00000A000E0850168C2D0000016F1D00000A000E05500772E32B00706F4A00000AA508000001286400000A8C080000016F1D00000A000E0650168C2D0000016F1D00000A000772612C00706F6000000A2D1A0772612C00706F4A00000A74290000010E0950285A00000A2D120E04506F6300000AA52400000216FE012B011600130411042D52000E0750168C2D0000016F1D00000A000E0850178C2D0000016F1D00000A000E0550168C2D0000016F1D00000A000E06500772E32B00706F4A00000AA508000001286400000A8C080000016F1D00000A000038630100000772A12B00706F4A00000AA5080000010772FF2B00706F4A00000AA508000001281500000A16FE01130411042D22000E0650168C2D0000016F1D00000A000E0550168C2D0000016F1D00000A00002B2A000E06500772212C00706F4A00000A6F1D00000A000E05500772212C00706F4A00000A6F1D00000A000038E40000000E06500772212C00706F4A00000A6F1D00000A000E05500772212C00706F4A00000A6F1D00000A000E0A4A2040420F00310A0E0A4A2080841E00327E0E0A4A2080841E00310A0E0A4A20BFC62D00316A0E0A4A204C040000310A0E0A4A20AF04000031560E0A4A20B0040000310A0E0A4A201305000031420E0A4A2014050000310A0E0A4A2077050000312E0E0A4A2078050000310A0E0A4A20DB050000311A0E0A4A20DC050000310C0E0A4A203F060000FE022B0117002B011600130411042D20000E0650168C2D0000016F1D00000A000E0550168C2D0000016F1D00000A00002B000E0A4A20CA0000002E3E0E0A4A20CB0000002E340E0A4A20C90000002E2A0E0A4A20CC0000002E200E0A4A20CE0000002E160E0A4A20D00000002E0C0E0A4A20CF000000FE012B011700130411043A0103000000066F3300000A0C00086F3400000A130411042D0A000E0D16520E0A1554000E0A4A1305110520360100003D9F00000011051F5D30451105153B9E01000011051F4E594502000000910100009101000011051F575945070000006B0100006B010000A5010000A5010000A50100006B0100006B01000038A001000011051F633B5D010000110520930000005945090000002C0100002C0100002C0100002C0100002C0100002C0100002C0100002C0100002C010000110520350100005945020000001701000017010000384C0100001105203F0200003DB8000000110520F501000059451E00000081000000BB000000BB000000BB000000BB000000BB000000BB000000BB000000BB000000BB000000BB000000BB000000BB000000BB000000810000008100000081000000810000008100000081000000BB0000008100000081000000810000008100000081000000810000008100000081000000810000001105203402000059450200000071000000710000001105203D02000059450300000053000000530000005300000038880000001105204B020000594503000000350000006F0000003500000011052063020000594506000000100000001000000010000000100000001000000010000000110520710200002E072B3F380901000038040100000772A12B007008166F6500000A8C080000016F4500000A000772FF2B007008166F6500000A8C080000016F4500000A0038CF0000000E0A4A2040420F00310A0E0A4A2080841E00327E0E0A4A2080841E00310A0E0A4A20BFC62D00316A0E0A4A204C040000310A0E0A4A20AF04000031560E0A4A20B0040000310A0E0A4A201305000031420E0A4A2014050000310A0E0A4A2077050000312E0E0A4A2078050000310A0E0A4A20DB050000311A0E0A4A20DC050000310C0E0A4A203F060000FE022B0117002B011600130411042D03002B320772A12B007008166F6500000A8C080000016F4500000A000772FF2B007008176F6500000A8C080000016F4500000A002B0000DE120814FE01130411042D07086F1F00000A00DC00000000096F4F00000A130411043A24E2FFFFDE1C0975050000011306110614FE01130411042D0811066F1F00000A00DC0000DE120614FE01130411042D07066F1F00000A00DC002A00414C000002000000D31E0000E5020000B8210000120000000000000002000000FC030000E31D0000DF2100001C0000000000000002000000BB020000441F0000FF210000120000000000000013300200EA0F00001E000011000E04500A0416731100000A81080000010516731100000A81080000010E04720100007051020B0720370100003D8B0300000745D10000004C020000640200002B030000730300008B030000DF030000A9040000710400008D040000E1040000FD04000035050000510500006D050000A9040000640B0000640B0000640B0000640B0000640B0000B6020000EE0200000A030000D20200009902000099020000640B0000640B000019050000DD050000FE0500001A0600003606000052060000A6060000A5050000DE0600008A060000C206000089050000FA06000016070000DD050000320700004E0700006F070000B1070000D207000071040000640B0000640B0000640B0000640B0000640B000089050000A5050000640B0000640B0000640B000090070000C1050000C1050000D2020000D2020000D20200007104000035050000F30700000F080000C5040000C5040000A9040000A70300002B080000430800005F080000640B0000640B0000640B0000640B0000640B0000640B0000640B0000640B0000FB0300008A060000A6060000640B0000640B0000640B00002B0800007C080000640B0000640B000098080000B4080000B4080000640B0000640B00007303000098080000D0080000D008000055040000390400005504000039040000390400003904000039040000100A0000100A0000100A000039040000390400003904000039040000550400005504000039040000390400003904000055040000550400005504000055040000550400005504000055040000640B0000640B0000640B0000640B0000640B0000640B0000640B0000640B0000640B0000640B0000640B0000F8090000E0090000780900006E060000640B0000640B000078090000640B0000640B0000640B0000640B0000280A0000280A0000280A0000280A0000280A0000400A0000880A0000640B0000640B00008102000081020000640B0000640B0000640B0000640B0000640B0000640B0000640B0000640B0000640B0000640B0000640B0000640B0000640B0000640B0000640B0000640B0000640B0000640B0000640B0000640B0000640B0000640B0000640B0000640B0000640B0000640B0000640B0000640B00002B03000073030000640B0000640B0000640B0000640B0000640B0000640B0000640B0000640B00004C0200004C0200002B0300007303000043030000640B00005B030000640B00007303000007202C01000059450C0000002009000004090000200900002009000004090000740900008C090000280B0000280B0000F80A0000100B00002009000038230B00000720F4010000594579000000FA000000FA000000330900003309000033090000330900003309000033090000330900003309000033090000330900003309000033090000920100003309000033090000330900003309000033090000330900007308000033090000330900003309000033090000F7070000F7070000F7070000F7070000F707000033090000B0020000B0020000B0020000B0020000B0020000E8020000E8020000E8020000E8020000B0020000CC020000E8020000B0020000B0020000B0020000B0020000B0020000E8020000E8020000E8020000E802000042010000FA0000002A0100003309000012010000330900003309000012010000330900000802000008020000120100002A010000BB060000BB060000D7060000D7060000F3060000F3060000120100003309000033090000330900008B0800008B0800008B0800008B0800008B0800006307000040020000630700009402000033090000E802000033090000BB08000033090000A308000008020000D308000012010000120100001201000012010000EB080000EB0800003309000033090000330900003309000033090000330900003309000033090000330900003309000033090000330900003309000033090000330900003309000033090000330900003309000033090000780200005C020000072040420F003B1C080000072080841E003B290800003818090000040381080000010516731100000A810800000138090A00000416731100000A81080000010516731100000A810800000138EC090000040381080000010516731100000A810800000138D40900000416731100000A81080000010516731100000A810800000138B7090000040381080000010516731100000A81080000010E040651389B0900000416731100000A8108000001050381080000010E040651387F090000040381080000010516731100000A81080000010E04065138630900000416731100000A81080000010516731100000A81080000010E0406513842090000040381080000010516731100000A8108000001382A090000040381080000010516731100000A810800000138120900000416731100000A81080000010503810800000138FA0800000416731100000A81080000010503810800000138E2080000040381080000010516731100000A81080000010E04065138C60800000416731100000A8108000001050381080000010E04065138AA080000040381080000010516731100000A81080000010E040651388E0800000416731100000A8108000001050381080000010E04065138720800000E0516FE010C082D17000416731100000A810800000105038108000001002B1500040381080000010516731100000A8108000001000E0406513834080000040381080000010516731100000A81080000010E04065138180800000416731100000A8108000001050381080000010E04065138FC070000040381080000010516731100000A81080000010E04065138E00700000416731100000A8108000001050381080000010E04065138C4070000040381080000010516731100000A81080000010E04065138A80700000416731100000A8108000001050381080000010E040651388C070000040381080000010516731100000A81080000010E04065138700700000416731100000A8108000001050381080000010E0406513854070000040381080000010516731100000A81080000010E04065138380700000416731100000A8108000001050381080000010E040651381C070000040381080000010516731100000A81080000010E0406513800070000040381080000010516731100000A81080000010E04065138E4060000040381080000010516731100000A81080000010E04065138C8060000040381080000010516731100000A81080000010E04065138AC060000040381080000010516731100000A81080000010E04065138900600000416731100000A81080000010516731100000A81080000010E040651386F0600000416731100000A8108000001050381080000010E0406513853060000040381080000010516731100000A81080000010E04065138370600000416731100000A8108000001050381080000010E040651381B060000040381080000010516731100000A81080000010E04065138FF0500000416731100000A8108000001050381080000010E04065138E3050000040381080000010516731100000A81080000010E04065138C7050000040381080000010516731100000A81080000010E04065138AB0500000416731100000A8108000001050381080000010E040651388F0500000416731100000A8108000001050381080000010E04065138730500000416731100000A8108000001050381080000010E04065138570500000416731100000A8108000001050381080000010E040651383B050000040381080000010516731100000A81080000010E040651381F0500000416731100000A81080000010516731100000A81080000010E04065138FE0400000416731100000A81080000010516731100000A81080000010E04065138DD0400000416731100000A81080000010516731100000A81080000010E04065138BC0400000416731100000A81080000010516731100000A81080000010E040651389B0400000416731100000A81080000010516731100000A81080000010E040651387A040000040381080000010516731100000A81080000010E040651385E0400000416731100000A8108000001050381080000010E0406513842040000040381080000010516731100000A8108000001382A040000040381080000010516731100000A81080000010E040651380E0400000416731100000A81080000010516731100000A810800000138F1030000040381080000010516731100000A81080000010E04065138D50300000416731100000A8108000001050381080000010E04065138B9030000040381080000010516731100000A81080000010E040651389D0300000416731100000A8108000001050381080000010E04065138810300000416731100000A8108000001050381080000010E0406513865030000040381080000010516731100000A81080000010E04065138490300000416731100000A8108000001050381080000010E040651382D030000040381080000010516731100000A81080000010E04065138110300000416731100000A8108000001050381080000010E04065138F5020000040381080000010516731100000A81080000010E04065138D9020000040381080000010516731100000A81080000010E04065138BD020000040381080000010516731100000A810800000138A50200000416731100000A810800000105038108000001388D020000040381080000010516731100000A810800000138750200000416731100000A810800000105038108000001385D0200000416731100000A8108000001050381080000013845020000040381080000010516731100000A8108000001382D0200000416731100000A8108000001050381080000013815020000040381080000010516731100000A810800000138FD0100000416731100000A81080000010503810800000138E50100000416731100000A8108000001050381080000010E04065138C9010000040381080000010516731100000A810800000138B1010000040381080000010516731100000A81080000013899010000040381080000010516731100000A810800000138810100000416731100000A81080000010503810800000138690100000416731100000A8108000001050381080000013851010000040381080000010516731100000A810800000138390100000416731100000A8108000001050381080000013821010000040381080000010516731100000A81080000013809010000022040420F00310D022080841E00FE0416FE012B0117000C082D1900040381080000010516731100000A810800000138D5000000022080841E00310A0220BFC62D00FE022B0117000C082D19000416731100000A81080000010503810800000138A4000000022014050000310A022077050000FE022B0117000C082D1600040381080000010516731100000A81080000012B76022078050000310A0220DB050000FE022B0117000C082D16000416731100000A8108000001050381080000012B4802204C040000310A0220AF040000FE022B0117000C082D1600040381080000010516731100000A81080000012B1A0416731100000A81080000010516731100000A81080000012B002A000013300200480400001F00001100020C0820370100003D040300000820BF0000003D88020000081C59459C0000005701000057010000570100008201000082010000570100005701000082010000570100008201000082010000820100008201000082010000570100005701000082010000570100008201000082010000820100008201000082010000820100005701000057010000570100005701000057010000570100005701000057010000570100005701000057010000570100008201000057010000820100008201000082010000820100005701000082010000820100008201000082010000820100005701000057010000820100008201000082010000820100005701000057010000570100005701000057010000570100005701000057010000570100005701000057010000570100005701000057010000820100008201000082010000820100008201000082010000820100008201000082010000820100005701000057010000570100008201000082010000820100005701000057010000820100008201000057010000570100005701000082010000820100005701000057010000570100005701000064010000570100005701000057010000570100005701000057010000570100005701000057010000820100008201000057010000570100006401000064010000570100005701000057010000570100005701000064010000640100006401000064010000640100008201000082010000820100008201000082010000820100008201000082010000820100008201000082010000570100005701000057010000570100008201000082010000570100008201000082010000820100008201000082010000820100008201000082010000820100008201000057010000820100008201000057010000570100000820BF0000003B4C01000038720100000820C800000059450900000017010000170100001701000017010000170100004201000017010000170100001701000008202C01000059450C000000DB000000DB00000006010000DB000000DB00000006010000060100000601000006010000DB000000DB000000DB000000380101000008200902000030300820FE010000594505000000AE000000AE000000AE000000AE000000AE0000000820090200003BA300000038C900000008203202000059451F00000016000000160000001600000016000000160000001600000016000000160000001600000016000000410000004100000041000000410000001600000016000000160000001600000016000000160000004100000016000000160000004100000041000000410000001600000041000000160000001F0000001600000008206B02000059450200000002000000020000002B2B16731100000A0A2B51030A2B4D0516FE010D092D0B0004286400000A0A002B090016731100000A0A002B2F02204C040000310A02203F060000FE022B0117000D092D0B0016731100000A0A002B0A000304282600000A0A002B00060B2B00072A133001000C0000002000001100027B840300040A2B00062A260002037D840300042A0000133001000C0000002100001100027B930300040A2B00062A260002037D930300042A0000133001000C0000002200001100027B850300040A2B00062A260002037D850300042A0000133001000C0000002200001100027B860300040A2B00062A260002037D860300042A0000133001000C0000002000001100027B8E0300040A2B00062A260002037D8E0300042A0000133001000C0000002300001100027B8F0300040A2B00062A260002037D8F0300042A0000133001000C0000000D00001100027B900300040A2B00062A260002037D900300042A0000133001000C0000002000001100027B870300040A2B00062A260002037D870300042A0000133001000C0000002000001100027B880300040A2B00062A260002037D880300042A000013300200170000002000001100027B89030004027B8A030004282700000A0A2B00062A00133001000C0000002000001100027B890300040A2B00062A260002037D890300042A0000133001000C0000002000001100027B8A0300040A2B00062A260002037D8A0300042A0000133001000C0000002000001100027B8B0300040A2B00062A260002037D8B0300042A0000133001000C0000002000001100027B8C0300040A2B00062A260002037D8C0300042A0000133001000C0000002000001100027B8D0300040A2B00062A260002037D8D0300042A0000133001000C0000000D00001100027B910300040A2B00062A260002037D910300042A0000133001000C0000002400001100027B920300040A2B00062A260002037D920300042A0000133001000C0000002500001100027B960300040A2B00062A260002037D960300042A0000133001000C0000002600001100027B940300040A2B00062A260002037D940300042A0000133001000C0000002000001100027B950300040A2B00062A260002037D950300042A8602167D910300040273910000067D9403000402167D9603000402283700000A002A133001000C0000002100001100027B970300040A2B00062A260002037D970300042A0000133001000C0000002500001100027B980300040A2B00062A260002037D980300042A0000133001000C0000002200001100027B990300040A2B00062A260002037D990300042A0000133001000C0000002200001100027B9A0300040A2B00062A260002037D9A0300042A0000133001000C0000002200001100027B9B0300040A2B00062A260002037D9B0300042A0000133001000C0000002300001100027B9C0300040A2B00062A260002037D9C0300042A0000133001000C0000002200001100027B9D0300040A2B00062A260002037D9D0300042A0000133001000C0000002000001100027B9E0300040A2B00062A260002037D9E0300042A0000133001000C0000002200001100027B9F0300040A2B00062A260002037D9F0300042A0000133001000C0000002200001100027BA00300040A2B00062A260002037DA00300042A0000133001000C0000002200001100027BA20300040A2B00062A260002037DA20300042A0000133001000C0000002200001100027BA30300040A2B00062A260002037DA30300042A0000133001000C0000000D00001100027BA10300040A2B00062A260002037DA10300042A0000133001000C0000002200001100027BA40300040A2B00062A260002037DA40300042A0000133001000C0000002700001100027BA50300040A2B00062A260002037DA50300042A0000133001000C0000002800001100027BA60300040A2B00062A260002037DA60300042A0000133001000C0000002200001100027BA70300040A2B00062A260002037DA70300042A2A02283700000A0000002A000000133001000C0000002300001100027BF70300040A2B00062A260002037DF70300042A0000133001000C0000002000001100027BF80300040A2B00062A260002037DF80300042A0000133001000C0000002200001100027BF90300040A2B00062A260002037DF90300042A0000133001000C0000002400001100027BFA0300040A2B00062A260002037DFA0300042A0000133001000C0000002100001100027BF60300040A2B00062A260002037DF60300042A0000133001000C0000002200001100027BFB0300040A2B00062A260002037DFB0300042A1E02283700000A2A00001B3005006B050000290000110004281D010006510516540003281D010006120212050E0428D1000006130C110C2D090016130BDD3B0500001105166AFE01130C110C2D090016130BDD26050000731600000A0A02130D110D1759450200000005000000D700000038600100001F0A130406166F6100000A000672135A00706F1700000A2606726F5A00706F1700000A260672D15A00706F1700000A260672625B00706F1700000A260672CE5B00706F1700000A26066F1800000A0D06166F6100000A000672185C00700972075D0070284300000A6F1700000A260672155D007009286800000A6F1700000A260672045E00706F1700000A260672105E00700972075D0070284300000A6F1700000A260672FB5E007009286800000A6F1700000A260672B15400706F1700000A260672E65F00706F1700000A2638960000001B130406166F6100000A000672135A00706F1700000A2606726F5A00706F1700000A2606722A6000706F1700000A260672786000706F1700000A260672DC6000706F1700000A260672386100706F1700000A26066F1800000A0D06166F6100000A0006728E6100706F1700000A260672C06100706F1700000A2606724062007009286800000A6F1700000A262B0816130BDD9F030000066F1800000A0E046F1900000A0E04731A00000A13060011066F1B00000A727E170070166F1C00000A038C280000016F1D00000A0011066F1B00000A72566200701E6F1C00000A188C0A0000026F1D00000A0011066F1B00000A72746200701E6F1C00000A178C090000026F1D00000A0011066F1B00000A72946200701E6F1C00000A188C090000026F1D00000A0011066F1B00000A72B46200701E6F1C00000A188C2D0000016F1D00000A001106736900000A130700733900000A0B1107076F6A00000A2600DE14110714FE01130C110C2D0811076F1F00000A00DC0000DE14110614FE01130C110C2D0811066F1F00000A00DC00076F4600000A6F4800000A16FE02130C110C2D090017130BDD8C02000006166F6100000A000672DA6200706F1700000A260672166300706F1700000A260672726300706F1700000A260672B06300706F1700000A260672336400706F1700000A2606728D64007009286800000A6F1700000A26066F1800000A0E046F1900000A0E04731A00000A13060011066F1B00000A72A3640070166F1C00000A130811066F1B00000A72B96400701E6F1C00000A11048C0A0000026F1D00000A0011066F1B00000A727E170070166F1C00000A038C280000016F1D00000A0011066F1B00000A72566200701E6F1C00000A188C0A0000026F1D00000A0011066F1B00000A72746200701E6F1C00000A178C090000026F1D00000A0011066F1B00000A72946200701E6F1C00000A188C090000026F1D00000A0011066F1B00000A72B46200701E6F1C00000A188C2D0000016F1D00000A0000076F4600000A6F4D00000A130E389F000000110E6F4E00000A740B00000113090011081109166F6B00000A6F1D00000A001109166F6B00000AA528000001156AFE0116FE01130C110C2D13000516540450087BA80400047DA80400042B6411066F3300000A130A00110A6F3400000A130C110C2D0300DE3905254A1758540450257BA8040004110A166F6500000A282700000A7DA804000400DE14110A14FE01130C110C2D08110A6F1F00000A00DC000000110E6F4F00000A130C110C3A50FFFFFFDE1D110E7505000001130F110F14FE01130C110C2D08110F6F1F00000A00DC0000DE14110614FE01130C110C2D0811066F1F00000A00DC00045004507BA6040004087BA6040004282D00000A7DA6040004045004507BA7040004087BA7040004282D00000A7DA7040004045004507BA8040004087BA8040004282D00000A7DA804000417130BDE0B260000DE000016130B2B0000110B2A0041940000020000007E0200001300000091020000140000000000000002000000DE010000CB000000A90200001400000000000000020000007904000034000000AD0400001400000000000000020000001F040000B7000000D60400001D000000000000000200000047030000B0010000F70400001400000000000000000000000B000000510500005C050000050000000100000113300800B4000000010000110073FF0000060A027B4504000416FE01130411042D1500061772D16400706FFB00000600060D388700000000166A027B44040004281D01000616731100000A1612020328D6000006130411042D1300061F0A72010000706FFB00000600060D2B51027B430400041201032845010006130411042D1300061F1472010000706FFB00000600060D2B2A00027B420400040708027B45040004027B46040004027B47040004027B480400040328A50000060D2B00092A133009001A0000002A000011000203040516281D010006160E040E0528A20000060A2B00062A000013300C008A0100002B0000110073FF0000060A722B65007072356500700E0828F90000061201284C00000A260716FE010D092D1600061F15725B6500706FFB00000600060C384A010000032C0E037BEF04000416FE0116FE012B0116000D092D1600061F1472010000706FFB00000600060C381D010000037BF40400042D0B037BF504000416FE012B0116000D092D1600061F1572010000706FFB00000600060C38EE000000042C0F047BB3040004166AFE0116FE012B0116000D092D1600061F0A72010000706FFB00000600060C38C0000000047BB404000416FE010D092D1600061F0B72010000706FFB00000600060C389D000000050D092D6100047BBB040004166AFE010D092D5100037BF10400041B3321047BBC040004037BEF0400043313047BBD0400042D0B047BBE04000416FE012B0116000D092D1F0006047BBB0400047D49040004061F0C72010000706FFB00000600060C2B3900000402037BEF040004037BF1040004037BF2040004050E040E050E060E0712000E0828A30000062606047BB80400047D50040004060C2B00082A00001B300900800900002C000011001613170E0A73FF000006510E0A501772010000706FFB00000600027BB3040004130F001613111613121613190E05131C722B65007072A76500700E0B28F900000672BD6500706F6C00000A131D111C16FE01132911292D37000E06182E1D0E061733140E076F1C01000616731100000A282800000A2B0116002B01170013110E061A3304111D2B0116001319002B06000E0813120011192D0F111D2C07111116FE012B0117002B011600132911292D050016131C0011112D0411122B0117001313111116FE01132911292D05001A131700111216FE01132911292D0E0011171A60131711171E60131700166A131B729B59007072BB5900700E0B28F9000006131A72C16500700E0B6F1900000A0E0B731A00000A131E00111E6F1B00000A725B0500701F0C1F326F2E00000A048C2D0000016F1D00000A00111E6F3300000A131F00111F6F3400000A132911292D18000E0A5017727C6600706FFB00000600161328DD14080000111F166F6D00000A131A111F176F6E00000A2D0A111F176F3500000A2B02166A00131B00DE14111F14FE01132911292D08111F6F1F00000A00DC0000DE14111E14FE01132911292D08111E6F1F00000A00DC00111116FE01132911293A3C010000001B1310166A130F731600000A130E110E72E06600706F1700000A26110E723C6700706F1700000A26110E72986700706F1700000A26110E72F46700706F1700000A26110E72506800706F1700000A26110E72AC6800706F1700000A26110E72086900706F1700000A26110E6F1800000A0E0B6F1900000A0E0B731A00000A13200011206F1B00000A725B0500701E6F1C00000A048C2D0000016F1D00000A0011206F1B00000A72646900701E6F1C00000A168C110000026F1D00000A0011206F3300000A13210011216F3400000A16FE01132911292D33001121166F6E00000A2D0A1121166F3500000A2B02166A00130F1121176F6E00000A2D0A1121176F3600000A2B01160013100000DE14112114FE01132911292D0811216F1F00000A00DC0000DE14112014FE01132911292D0811206F1F00000A00DC0000110F7E2F00000A281D01000616731100000A16111712140E0B28D7000006132911292D18000E0A501772766900706FFB00000600161328DD3B06000011147BC30400040B11147BC4040004130811147BBB040004130B11147BC20400041305111216FE01132911292D2A0016731100000A077BAE040004282500000A131516731100000A077BAF040004282500000A1316002B340016731100000A131616731100000A0E077BA8040004077BA8040004077BAE040004282600000A282600000A282500000A13150011151F64731100000A286F00000A287000000A1F64731100000A282A00000A131511161F64731100000A286F00000A287000000A1F64731100000A282A00000A13160E0617FE0116FE01132911292D4A0007077BA80400040E077BA8040004282D00000A7DA804000407077BA70400040E077BA7040004282D00000A7DA704000407077BA60400040E077BA6040004282D00000A7DA604000400027BB30400041322111916FE01132911292D0600111B13220011220405111C0E060E07120012070E0B28AA000006132911292D18000E0A501772D26900706FFB00000600161328DDC9040000111916FE01132911292D5900111B04060E0B28A8000006132911292D18000E0A5017720C6A00706FFB00000600161328DD94040000027BB30400040406170E0B28A7000006132911292D18000E0A501772506A00706FFB00000600161328DD6604000000111316FE01132911292D18000E0A501772906A00706FFB00000600161328DD42040000111116FE01132911293AFB0000000016130A110F046A060307111D16FE01120312230E0B28C2000006132911292D18000E0A501772D86A00706FFB00000600161328DDFB030000096F1C01000616731100000A287100000A16FE01132911292D6F000E0A50171C8D29000001132A112A1672166B0070A2112A17096F1C010006132B122B282300000AA2112A1872866B0070A2112A190E076F1C010006132B122B282300000AA2112A1A72B46B0070A2112A1B0F02287200000AA2112A287300000A6FFB00000600161328DD72030000111D132911292D2E00110F09046A11230E0B28E2000006132911292D18000E0A501772D46B00706FFB00000600161328DD3D03000000002B7E00110B166AFE01130A110A2D07111C16FE012B011600132911292D3100110F166A12241225120C0E0B28BA000006132911292D18000E0A501772306C00706FFB00000600161328DDED02000000110F046A0603070E0912030E0B28C1000006132911292D18000E0A501772D86A00706FFB00000600161328DDBC0200000009097BA70400041F64731100000A286F00000A287000000A1F64731100000A282A00000A7DA704000409097BA80400041F64731100000A286F00000A287000000A1F64731100000A282A00000A7DA8040004110F09097BAD040004282701000616731100000A12021209120B12050E0B28D4000006132911292D23000E0A501772746C0070096F1800000A286800000A6FFB00000600161328DD1D020000040E0406731500000613061106166A110F1B076F1C010006096F1C01000616731100000A086F1C0100066F17000006132911292D18000E0A501772DA6C00706FFB00000600161328DDD001000011060E0B6F21000006132911292D18000E0A501772426D00706FFB00000600161328DDA9010000110F0E0B28D8000006132911292D18000E0A501772886D00706FFB00000600161328DD82010000729B59007072BB5900700E0B28F90000061326091126111A12180E0B28A6000006132911292D2F000E0A501772B86D00701200282000000A72D86D0070096F1800000A287400000A6FFB00000600161328DD2C010000110A061118111C111D11190E0B28DD000006132911292D5D000E0A50171C8D29000001132A112A1672006E0070A2112A17120A287500000AA2112A18725C6E0070A2112A191200282000000AA2112A1A72D86D0070A2112A1B096F1800000AA2112A287300000A6FFB00000600161328DDB700000073130100061304111C132911292D3B001107110F03091204120C120D0E0B28DB000006132911292D20000E0A501772806E0070096F1800000A286800000A6FFB00000600161328DE6E000E0A50067D490400040E0A50110A7D4A0400040E0A50077D4B0400040E0A50097D4C0400040E0A5011047D4E0400040E0A50087D4D0400040E0A5011097D4F0400040E0A506FFD00000600171328DE1D1327000E0A501711276F7600000A6FFB0000060000DE00001613282B000011282A417C000002000000420100004C0000008E010000140000000000000002000000170100008F000000A60100001400000000000000020000009002000047000000D70200001400000000000000020000004A020000A5000000EF020000140000000000000000000000230000003C0900005F09000017000000390000011B3004002D0100000B000011000502810800000103046F6C00000A16FE01130411042D0800170D380A01000000731600000A0A0672EA6E00706F1700000A2606724A6F00706F1700000A260672AA6F00706F1700000A2606720A7000706F1700000A26066F1800000A0E046F1900000A0E04731A00000A0B00076F1B00000A72CD0500701F096F1C00000A028C080000016F1D00000A00076F1B00000A726A7000701F0C6F1C00000A036F1D00000A00076F1B00000A727E7000701F0C6F1C00000A046F1D00000A00076F3300000A0C00086F3400000A16FE01130411042D1C00050808728E7000706F7700000A6F6500000A8108000001170DDE3A00DE120814FE01130411042D07086F1F00000A00DC0000DE120714FE01130411042D07076F1F00000A00DC0000DE062600160DDE0500160D2B0000092A000000012800000200C4002FF300120000000002006C009D0901120000000000002000FF1F010639000001133009001C0000002A00001100020304050E040E050E067E2F00000A0E0728A20000060A2B00062A13300500AF0000002D0000110005025103046F6C00000A16FE010C082D0800170B3893000000027BA6040004030405507CA60400040E0428A40000060A062C18027BA7040004030405507CA70400040E0428A40000062B0116000A062C18027BA8040004030405507CA80400040E0428A40000062B0116000A062C18027BAE040004030405507CAE0400040E0428A40000062B0116000A062C18027BAF040004030405507CAF0400040E0428A40000062B0116000A060B2B00072A001B3003002C0100002E0000110000731600000A0A0672A27000706F1700000A260672257100706F1700000A260672167200706F1700000A2606729F7200706F1700000A260516FE010D092D260006721F7300706F1700000A260672937300706F1700000A260672077400706F1700000A260006727B7400706F1700000A260672837400706F1700000A260672037500706F1700000A2606728C7500706F1700000A26066F1800000A0E046F1900000A0E04731A00000A0B00076F1B00000A727E170070166F1C00000A028C280000016F1D00000A00076F1B00000A725B0500701E6F1C00000A038C2D0000016F1D00000A00076F1B00000A7275050070166F1C00000A048C280000016F1D00000A00076F1E00000A18FE010CDE1A0714FE010D092D07076F1F00000A00DC260000DE0000160C2B0000082A4134000002000000AB000000640000000F010000100000000000000000000000010000001E0100001F01000005000000010000011B3003004F0100002E0000110000731600000A0A06720F7600706F1700000A260672A87600706F1700000A260672457700706F1700000A260672E67700706F1700000A2606725A7800706F1700000A260672CE7800706F1700000A260672427900706F1700000A260672C57900706F1700000A2606724A7A00706F1700000A2606727B7400706F1700000A260672D57A00706F1700000A260672497B00706F1700000A260672BD7B00706F1700000A260672427C00706F1700000A260672C57C00706F1700000A26066F1800000A056F1900000A05731A00000A0B00076F1B00000A727E170070166F1C00000A028C280000016F1D00000A00076F1B00000A725B0500701E6F1C00000A038C2D0000016F1D00000A00076F1B00000A7275050070166F1C00000A048C280000016F1D00000A00076F1E00000A26170CDE1A0714FE010D092D07076F1F00000A00DC260000DE0000160C2B0000082A004134000002000000CF000000630000003201000010000000000000000000000001000000410100004201000005000000010000011B300300FA0000002F0000110000731600000A0A0672507D00706F1700000A260672DF7D00706F1700000A2606726E7E00706F1700000A260672FD7E00706F1700000A2606728C7F00706F1700000A2606721B8000706F1700000A260672AA8000706F1700000A260672398100706F1700000A26066F1800000A046F1900000A04731A00000A0C00086F1B00000A727E170070166F1C00000A028C280000016F1D00000A00086F1B00000A725B0500701E6F1C00000A038C2D0000016F1D00000A00086F3100000AA52D0000010B0716FE0116FE01130411042D0500170DDE2300DE120814FE01130411042D07086F1F00000A00DC0000DE05260000DE0000160D2B0000092A0000011C000002007B005CD700120000000000000100ECED0005010000011B3007004704000030000011000E06166A550E071654160B166A0C160D1613047201000070130516130C16130D16731100000A130E16731100000A130F16731100000A131016731100000A131116731100000A1312731600000A0A000672C88100706F1700000A2606720C8200706F1700000A260672588200706F1700000A2606727E8200706F1700000A260672A88200706F1700000A260672DA8200706F1700000A260672188300706F1700000A2606725C8300706F1700000A260672A68300706F1700000A260672F08300706F1700000A260672468400706F1700000A2606729C8400706F1700000A260672E88400706F1700000A260672408500706F1700000A260672FF8500706F1700000A26066F1800000A0E086F1900000A0E08731A00000A13130011136F1B00000A727E170070166F1C00000A028C280000016F1D00000A0011136F3300000A13140011146F3400000A131611162D0900161315DDEA0200001114166F6E00000A2D0A1114166F3600000A2B0116000B1114176F6E00000A2D0A1114176F3500000A2B02166A000C1114186F6E00000A2D0A1114186F3600000A2B0116000D1114196F6E00000A2D0A1114196F3600000A2B011700130405131611162D350011141A6F6E00000A131611162D2500120611141A6F6D00000A1628550100061307110716FE01131611162D06001106130500000011141B6F6E00000A2D0A11141B6F7800000A2B011600130811141C6F3600000A130C11141D6F3600000A130D11141E6F6500000A130E11141F096F6500000A130F11141F0A6F6500000A131011141F0B6F6500000A131111141F0C6F6500000A131200DE14111414FE01131611162D0811146F1F00000A00DC0000DE14111314FE01131611162D0811136F1F00000A00DC00052C070E0416FE022B011700131611163AF800000000110C163053110D16304E110E16731100000A282C00000A2D3F110F16731100000A282C00000A2D30111016731100000A282C00000A2D21111116731100000A282C00000A2D12111216731100000A282C00000A16FE012B011600131611163A930000000018081209120A120B0E0828F0000006131611162D0900161315DD2601000011092D07110A16FE012B011700131611162D1900080E0828F6000006131611162D0900161315DDFB0000000002091105030412020E0828AB000006131611162D0900161315DDDC000000020308160E0828A7000006131611162D0900161315DDC20000000E0717540E060855171315DDB20000000008166A2E2B722B65007072A76500700E0828F900000672BD6500706F6C00000A2C0A110417FE0116FE012B0117002B011600131611162D400002091105030412020E0828AB000006131611162D0600161315DE5D020308160E0828A7000006131611162D0600161315DE460E0717540E060855171315DE390307330F11042D0B041B3307110816FE012B011600131611162D0600161315DE180E0718540E060855171315DE0B260000DE00001613152B000011152A00414C0000020000004201000016010000580200001400000000000000020000001A01000056010000700200001400000000000000000000004F000000E90300003804000005000000010000011B3003000903000031000011000E05166A55731600000A0B160A03130511051759450500000002000000060000000A0000000E000000060000002B0C170A2B0C180A2B08190A2B04160A2B000007166F6100000A0007724D8600706F1700000A2607721D1A00706F1700000A260772A18600706F1700000A2607721B8700706F1700000A260772958700706F1700000A2607720F8800706F1700000A2607721D1A00706F1700000A260772898800706F1700000A260772D58800706F1700000A260772218900706F1700000A2607726D8900706F1700000A2607721D1A00706F1700000A260772B98900706F1700000A2607721B8A00706F1700000A2607727D8A00706F1700000A260772DF8A00706F1700000A260772418B00706F1700000A260772A38B00706F1700000A260772058C00706F1700000A260772678C00706F1700000A260772C98C00706F1700000A2607722B8D00706F1700000A2607728D8D00706F1700000A260772EF8D00706F1700000A260772518E00706F1700000A260772518E00706F1700000A260772B38E00706F1700000A260772728F00706F1700000A2607721D1A00706F1700000A260772D68F00706F1700000A26076F1800000A0E066F1900000A0E06731A00000A0D00096F1B00000A727E170070166F1C00000A028C280000016F1D00000A00096F1B00000A72289000701E6F1C00000A058C2D0000016F1D00000A00096F1B00000A72449000701E6F1C00000A068C0E0000026F1D00000A00096F1B00000A72589000701F096F1C00000A168C2D0000016F1D00000A00096F1B00000A727A9000701F196F1C00000A04283800000A2D03042B057E3200000A006F1D00000A00096F1B00000A7290900070186F1C00000A0E041BFE018C430000016F1D00000A00096F1B00000A7275050070166F1C00000A0C08186F5100000A00096F1E00000A17FE01130611062D0600161304DE50086F6300000A7E3200000AFE0116FE01130611062D0600161304DE340E05086F6300000AA5280000015500DE120914FE01130611062D07096F1F00000A00DC00171304DE0B260000DE00001613042B000011042A0000004134000002000000C60100001C010000E202000012000000000000000000000040000000BA020000FA02000005000000010000011B3003008600000032000011000072AA900070046F1900000A04731A00000A0A00066F1B00000A727E170070166F1C00000A038C280000016F1D00000A00066F1B00000A726B9100701E6F1C00000A028C280000016F1D00000A00066F1E00000A17FE010C082D0500160BDE2200DE100614FE010C082D07066F1F00000A00DC00170BDE0A260000DE0000160B2B0000072A0000011C00000200140050640010000000000000010078790005010000011B300300F203000033000011000E06156A55731600000A0A0E056F1C01000616731100000A282C00000A16FE01130711072D06001B0D002B04001C0D0016731100000A130402130811081F14302111084503000000380000003D0000004B00000011081F0A2E4A11081F142E492B5511081F1E2E3C110820E7030000594503000000360000002300000023000000110820F20300002E1A2B2B1F640C2B2E1F650C0E056F1C01000613042B201F660C2B1B1F6E0C2B161F780C0E056F1C01000613042B08161306382E0300000006728D9100706F1700000A260672E39100706F1700000A2606723B9200706F1700000A260672859200706F1700000A260672E59200706F1700000A260672419300706F1700000A2606729B9300706F1700000A260672F79300706F1700000A2606724B9400706F1700000A260672A19400706F1700000A260672F19400706F1700000A260672439500706F1700000A260672919500706F1700000A260672E19500706F1700000A260672339600706F1700000A2606728B9600706F1700000A260672D79600706F1700000A260672119700706F1700000A260672639700706F1700000A260672B79700706F1700000A260672FF9700706F1700000A2606725B9800706F1700000A260672B39800706F1700000A260672B39800706F1700000A260672B39800706F1700000A260672F19800706F1700000A260672B39800706F1700000A260672B39800706F1700000A260672439900706F1700000A2606728F9900706F1700000A2606728F9900706F1700000A260672DD9900706F1700000A260672B39800706F1700000A260672D79600706F1700000A260672319A00706F1700000A26066F1800000A0E076F1900000A0E07731A00000A13050011056F1B00000A727E170070166F1C00000A038C280000016F1D00000A0011056F1B00000A725B0500701E6F1C00000A048C2D0000016F1D00000A0011056F1B00000A72112A00701E6F1C00000A088C0E0000026F1D00000A0011056F1B00000A72646900701E6F1C00000A098C110000026F1D00000A0011056F1B00000A721F2A00701F096F1C00000A0E046F1C0100068C080000016F1D00000A0011056F1B00000A72B89A00701F096F1C00000A11048C080000016F1D00000A0011056F1B00000A72712A00701F096F1C00000A0E046F1C0100060E056F1C010006282700000A8C080000016F1D00000A0011056F1B00000A7290900070186F1C00000A051BFE018C430000016F1D00000A0011056F1B00000A7275050070166F1C00000A0B07186F5100000A0011056F1E00000A17FE01130711072D0600161306DE320E06076F6300000AA5280000015500DE14110514FE01130711072D0811056F1F00000A00DC00171306DE072600161306DE000011062A000041340000020000007B02000052010000CD030000140000000000000000000000C000000027030000E703000007000000010000011B300300E00200000B000011000573010100065100731600000A0A0672D09A00706F1700000A260219FE0116FE01130411042D40000672009B00706F1700000A260672F79B00706F1700000A260672559C00706F1700000A2606724C9D00706F1700000A260672AA9D00706F1700000A26002B32000672009B00706F1700000A260672B79E00706F1700000A260672559C00706F1700000A260672AE9F00706F1700000A26000672A5A000706F1700000A260672EDA000706F1700000A2606723FA100706F1700000A260672BDA100706F1700000A2606723BA200706F1700000A260672B9A200706F1700000A260219FE0116FE01130411042D0E00067237A300706F1700000A26000672D2A300706F1700000A26066F1800000A0E046F1900000A0E04731A00000A0B00076F1B00000A7275050070166F1C00000A038C280000016F1D00000A00076F1B00000A7230A40070166F1C00000A166A047B65040004287900000A8C280000016F1D00000A00076F1B00000A724CA400701F096F1C00000A16731100000A047B66040004282500000A8C080000016F1D00000A00076F1B00000A726AA40070166F1C00000A166A047B67040004287900000A8C280000016F1D00000A00076F1B00000A72B89A00701F096F1C00000A16731100000A047B68040004282500000A8C080000016F1D00000A000219FE0116FE01130411042D3000076F1B00000A7280A400701F096F1C00000A16731100000A047B6A040004282500000A8C080000016F1D00000A0000076F3300000A0C00086F3400000A130411042D0800160DDD9A000000055008166F3600000A6A7D65040004055008176F6500000A7D66040004055008186F3600000A6A7D67040004055008196F6500000A7D68040004055016731100000A7D690400040219FE0116FE01130411042D10000550081A6F6500000A7D6A0400040000DE120814FE01130411042D07086F1F00000A00DC00170DDE1C0714FE01130411042D07076F1F00000A00DC260000DE0000160D2B0000092A414C0000020000002E0200007C000000AA0200001200000000000000020000001E010000A3010000C102000012000000000000000000000008000000CB020000D3020000050000000100000113300700180000000D00001100020304057E2F00000A0E040E0528B00000060A2B00062A1B300800DD09000034000011000E05730F010006510E05501772010000706F0B01000600057B5C0400041308057B60040004130911092C0C057B55040004166AFE012B011700131511152D05001613080000057B56040004166A310F057B57040004166AFE0416FE012B011600131511152D38000E0550187294A40070057C56040004282000000A72E6A40070057C57040004282000000A287400000A6F0B01000600161314DD3A090000110816FE01131511152D0C00057B590400041305002B150016731100000A057B59040004282500000A1305001105057B59040004282800000A16FE01131511152D2C000E055072ECA40070057C59040004282300000A7224A500701205282300000A287400000A6F0C010006000003131611161759450500000002000000670000006500000067000000070000002B653887000000057B57040004166A2E08057B5A0400042B011600131511152D4200057B530400041F20FE01131511152D30000E05507230A50070057C57040004282000000A728AA50070057C5A040004287500000A287400000A6F0C0100060000002B292B270E05501872AAA50070038C190000026F1800000A286800000A6F0B01000600161314DD1E080000057B530400041CFE0116FE01131511152D360003057B56040004057B5B040004120C0E0628AE000006131511152D18000E05501772DEA500706F0B01000600161314DDD7070000000E06722CA600706F1200000A00110816FE01131511152D0F000528CB00000613060038F6000000000319FE0116FE01131511152D3100057B5604000412060E0628CA000006131511152D18000E0550177258A600706F0B01000600161314DD73070000002B2F00057B5604000412060E0628C9000006131511152D18000E0550177258A600706F0B01000600161314DD4207000000110916FE01131511152D4D001106177D900400041106057B610400047D910400041106057B5B0400047B660400047D870400041106057B5B0400047B680400047D880400041106057B6204000473140100067D920400040011060E05507B830400040E0628C7000006131511152D18000E0550177294A600706F0B01000600161314DDBA060000001106130711067B8D04000473140100060A0673140100060B031316111617594505000000020000006F000000140000006F0000000F0000002B6D066F1C010006130538870000003882000000057B530400041F20FE0116FE01131511152D4600057B58040004131511152D0A00066F1C01000613050072D2A6007072DCA600700E0628F9000006120B284C00000A26110B16FE01131511152D0A0016731100000A130500002B270E05501872AAA50070038C190000026F1800000A286800000A6F0B01000600161314DDEB050000066F1C0100061105282800000A0D090C0916FE01131511153A4B020000000673140100060B1105066F1C010006282600000A130D160C031B2E090317FE0116FE012B011600131511152D41007202A70070727F0D00700E0628F9000006120E282900000A26110E1F64731100000A282A00000A130E110D110E282C00000A16FE01131511152D0400170C00000319FE0116FE01131511152D460072650D0070727F0D00700E0628F9000006120E282900000A26110E1F64731100000A282A00000A130E110D282B00000A110E282C00000A16FE01131511152D0400170C00007310010006130F110F057B560400047D84040004110F11067B850400047D85040004110F0673140100067D8D040004110D16731100000A282C00000A16FE01131511152D1A00110F16731100000A7D87040004110F110D7D88040004002B1D00110F110D282B00000A7D87040004110F16731100000A7D8804000400110816FE01131511152D110073130100060B73130100061310002B4200110F0E05507B830400040E0628C7000006131511152D18000E0550177294A600706F0B01000600161314DD44040000110F7B8D0400041310111073140100060B00031B2E090317FE0116FE012B011600131511152D430002057B560400040E0628F7000006131511152D2D000E0550177212A700700F00287200000A7260A70070057C56040004282000000A287400000A6F0B010006000000057B530400041F20FE0116FE01131511152D0600160D160C000816FE01131511152D330011106F1C010006066F1C010006282C00000A16FE01131511152D09000673140100060B000E06722CA600706F1300000A0000001108131511152D3C00057B56040004076F1C010006110509057B530400040E0628F1000006131511152D18000E0550177282A700706F0B01000600161314DD400300000011092C37057B640400046F1C01000616731100000A282C00000A2D1B057B630400046F1C01000616731100000A282C00000A16FE012B0116002B011700131511152D3900057B56040004057B64040004057B630400040E0628B1000006131511152D18000E05501772D0A700706F0B01000600161314DDC4020000000816FE01131511152D4C000E05501A72A70F0070057B590400048C08000001066F1800000A076F1800000A282100000A6F0B010006000E05500773140100067D810400041108131511152D0900171314DD6E02000000731101000613041104057B530400047D930400041104057B540400047D940400041104057B560400047D950400041104057B550400047D960400041104027D970400041104047D980400041104097D9904000411040773140100067D9A04000411040E05507B830400047D9B0400041104057B5B0400047B650400047D9C040004110411077B8E0400047D9D0400041104057B5B0400047B670400047D9E040004110411077B8F0400047D9F0400041104057B5F0400047DA30400041104057B600400047DA40400041104057B5B0400047B6B0400047DA504000411082D07110916FE012B011600131511153A96000000001104177DA00400041104057B5D0400047DA10400041104057B5E0400047DA2040004057B56040004057B5E0400047B6D040004057B5E0400047B6E040004057B5E0400047B6C040004057B5E0400047B6F040004057B5E0400047B70040004057B5E0400047B710400047B740400040E0628B8000006131511152D18000E0550177222A800706F0B01000600161314DDE70000000011040E05500E0628B3000006131511152D18000E055017726CA800706F0B01000600161314DDBC0000000E05500773140100067D81040004031B2E090319FE0116FE012B011600131511152D6E00120A0E0628E800000616FE01131511152D5A00110A0E04283000000A16FE01131511152D4600057B55040004057B590400041817121112120E0628E7000006131511152D25000E05501772A6A80070057B550400048C28000001287A00000A6F0B01000600161314DE2E0000000E05506F0D01000600171314DE1D1313000E05501711136F7600000A6F0B0100060000DE00001613142B000011142A000000411C0000000000004500000077090000BC09000017000000390000011B300300620100002E00001100731600000A0A06721AA900706F1700000A26067248A900706F1700000A260672AAA900706F1700000A2606720EAA00706F1700000A2606727AAA00706F1700000A260672E6AA00706F1700000A2600066F1800000A056F1900000A05731A00000A0B00076F1B00000A724CAB0070166F1C00000A028C280000016F1D00000A00076F1B00000A726CAB00701F096F1C00000A037BA604000416731100000A282500000A8C080000016F1D00000A00076F1B00000A728AAB00701F096F1C00000A036F1B01000616731100000A282500000A8C080000016F1D00000A00076F1B00000A72A8AB00701F096F1C00000A047BA604000416731100000A282500000A8C080000016F1D00000A00076F1B00000A72CEAB00701F096F1C00000A046F1B01000616731100000A282500000A8C080000016F1D00000A00076F1E00000A17FE010CDE1A0714FE010D092D07076F1F00000A00DC260000DE0000160C2B0000082A0000413400000200000063000000E2000000450100001000000000000000000000004F000000060100005501000005000000010000011B300300F90000003500001100031652160B00731600000A0A0672F4AB00706F1700000A26067264AC00706F1700000A260672D4AC00706F1700000A26067244AD00706F1700000A260672B4AD00706F1700000A26067224AE00706F1700000A26066F1800000A046F1900000A04731A00000A0D00096F1B00000A727E170070166F1C00000A028C280000016F1D00000A00096F1B00000A7294AE00701E6F1C00000A1B8C0F0000026F1D00000A00096F3100000A0C00DE120914FE01130611062D07096F1F00000A00DC00082C0A087E3200000AFE012B011700130611062D090008A52D0000010B00030716FE015200DE08130400161305DE06001713052B000011052A000000011C00000200680045AD00120000000000000600E1E700083900000113300900400600003600001100027BA0040004130C027BA40400042C0C027B96040004166AFE012B011700131311132D050016130C00110C131311132D1400027B96040004130D027B96040004130E002B2500027B96040004166A3308027BA10400042B06027B9604000400130D027BA1040004130E0073130100060A73130100060B027B97040004120A042845010006131311132D1600031772C6AE00706F0B01000600161312389C050000110C131311132D3F00027B96040004027B9A04000412010428D0000006131311132D1600031772FEAE00706F0B01000600161312386305000007027B9A04000428210100060A00110D0428D8000006131311132D1600031772886D00706F0B010006001613123831050000110C131311133A170100000019027B96040004166A027B9A0400041202120312040428DB000006131311132D160003177240AF00706F0B0100060016131238EE040000027B96040004027B95040004166A0428BB000006131311132D160003177286AF00706F0B0100060016131238BE040000027BA404000416FE01131311133A9A0000000008257BA6040004027BA20400046F02010006282700000A7DA604000408257BA7040004027BA20400046F03010006282700000A7DA704000408257BA8040004027BA20400046F04010006282700000A7DA804000409257BA6040004027BA20400046F06010006282700000A7DA604000409257BA70400047DA704000409257BA8040004027BA20400046F07010006282700000A7DA804000400002B4B00027BA20400046F02010006027BA20400046F03010006027BA20400046F0401000673150100060C027BA20400046F0601000616731100000A027BA20400046F0701000673150100060D00027B93040004131411141C2E0811141F202E0F2B15171306027B9704000413092B321913061613092B2A031872D2AF0070027B930400048C0D0000026F1800000A286800000A6F0B010006001613123870030000027B9504000408027B9D040004027B9F04000409027BA50400040428B7000006131311132D160003177206B000706F0B0100060016131238340300001106027B9504000412071208120B0428F0000006131311132D160003177248B000706F0B01000600161312380403000011072D07110816FE012B011700131311132D2A00027B950400040428F6000006131311132D160003177290B000706F0B0100060016131238C802000000110E027B97040004027B950400040428A8000006131311132D16000317720C6A00706F0B010006001613123897020000110C16FE01131311132D2C00027B95040004110D0428AC000006131311132D1600031772E0B000706F0B01000600161312386102000000027B9D0400046F1C01000616731100000A282C00000A16FE01131311133AE200000000110D0428B9000006131311132D16000317721CB100706F0B010006001613123819020000110A027B9D0400040428B6000006131311132D160003177268B100706F0B0100060016131238EF010000027B930400041C3308027B990400042B011700131311132D7900110C16FE01131311132D4200027B95040004027BA20400047B6F040004027BA20400046F020100060428BE000006131311132D1600031772B0B100706F0B01000600161312388B010000002B2A00027B960400040428BD000006131311132D1600031772F8B100706F0B01000600161312385F0100000000001109027B98040004027B95040004027B9404000473160000061305027B960400041311110C16FE01131311132D0A00027BA10400041311001105166A1111027B93040004066F1C01000616731100000A027B9A0400046F1C010006076F1C010006027BA30400046F19000006131311132D30000317723AB20070027B930400048C0D0000026F1800000A7270B20070284300000A6F0B0100060016131238BA0000001105046F21000006131311132D1600031772426D00706F0B010006001613123896000000120F0428DF000006131311132D050016130F00110F16FE0116FE01131311132D7000027B9D0400046F1A01000616731100000A282C00000A16FE01131311132D4F00027B9604000412100428B2000006131311132D0500161310001110131311132D2C00110B0203110A7BF10400040428B5000006131311132D130003177284B200706F0B010006001613122B080000001713122B0011122A13300800BB0100003700001100731600000A0D0312020E0428C9000006130C110C2D090016130B389801000008090E0428C7000006130C110C2D090016130B3880010000087B8D0400047314010006130411047314010006130511046F1C010006056F1C010006282800000A1306110616FE01130C110C2D0200000311056F1C010006056F1C01000611061C0E0428F1000006130C110C2D090016130B382201000002110512000E0428D0000006130C110C2D090016130B380701000006110528210100060B1902166A110512081209120A0E0428DB000006130C110C2D090016130B38DC0000000203166A0E0428BB000006130C110C2D090016130B38C2000000031108087B8E040004087B8F040004110916731100000A0E0428B7000006130C110C2D090016130B38950000000204030E0428A8000006130C110C2D060016130B2B7F087B8E0400046F1C01000616731100000A282C00000A16FE01130C110C2D5B00020E0428B9000006130C110C2D060016130B2B4B0412070E042845010006130C110C2D060016130B2B351107087B8E0400040E0428B6000006130C110C2D060016130B2B1A020E0428BD000006130C110C2D060016130B2B060017130B2B00110B2A001B300300640200002E0000110000731600000A0A0672C6B200706F1700000A26067236B300706F1700000A260672A6B300706F1700000A26067216B400706F1700000A26067286B400706F1700000A260672F6B400706F1700000A26067266B500706F1700000A260672D6B500706F1700000A26067246B600706F1700000A260672B6B600706F1700000A26067226B700706F1700000A26067296B700706F1700000A26067206B800706F1700000A26067276B800706F1700000A260672E6B800706F1700000A26067256B900706F1700000A260672C6B900706F1700000A26067236BA00706F1700000A260672A6BA00706F1700000A26067216BB00706F1700000A26067286BB00706F1700000A260672F6BB00706F1700000A26066F1800000A0E046F1900000A0E04731A00000A0B00076F1B00000A7266BC0070166F1C00000A037B950400048C280000016F1D00000A00076F1B00000A7286BC0070166F1C00000A037B960400048C280000016F1D00000A00076F1B00000A72A6BC00701F096F1C00000A037B9D0400046F1A0100068C080000016F1D00000A00076F1B00000A72C0BC00701E6F1C00000A037B970400048C2D0000016F1D00000A00076F1B00000A72E2BC00701E6F1C00000A0F00287B00000A8C310000016F1D00000A00076F1B00000A721ABD0070166F1C00000A037B9C0400048C280000016F1D00000A00076F1B00000A7246BD0070186F1C00000A037B990400048C430000016F1D00000A00076F1B00000A7280BD00701E6F1C00000A058C190000026F1D00000A00076F1E00000A2600DE100714FE010D092D07076F1F00000A00DC00170CDE0A260000DE0000160C2B0000082A4134000002000000250100001D0100004202000010000000000000000000000001000000560200005702000005000000010000011B300300C40000002E0000110000027BFB0400040D092D0800170CDDAD000000731600000A0A0672A6BD00706F1700000A260672E8BD00706F1700000A260672E1BE00706F1700000A26066F1800000A046F1900000A04731A00000A0B00076F1B00000A7213BF00701F096F1C00000A036F1A0100068C080000016F1D00000A00076F1B00000A7243BF00701F096F1C00000A036F1C0100068C080000016F1D00000A00076F1E00000A2600DE100714FE010D092D07076F1F00000A00DC00170CDE0A260000DE0000160C2B0000082A011C00000200510051A200100000000000000100B6B70005010000011B300300450300002E0000110000731600000A0A06166F6100000A0006725FBF00706F1700000A26067291BF00706F1700000A26067214C000706F1700000A26067288C000706F1700000A260672F2C000706F1700000A26067266C100706F1700000A260672E9C100706F1700000A2606725FC200706F1700000A260672CBC200706F1700000A26067241C300706F1700000A260672C1C300706F1700000A2606723BC400706F1700000A260672ABC400706F1700000A26067225C500706F1700000A26067299C500706F1700000A26067203C600706F1700000A26067277C600706F1700000A26066F1800000A0E066F1900000A0E06731A00000A0B00076F1B00000A72EDC600701F096F1C00000A036F1B0100068C080000016F1D00000A00076F1B00000A7219C700701F096F1C00000A036F1A0100068C080000016F1D00000A00076F1B00000A7239C700701F096F1C00000A037BA60400048C080000016F1D00000A00076F1B00000A724FC700701F096F1C00000A037BA70400048C080000016F1D00000A00076F1B00000A726FC700701F096F1C00000A0E046F1B0100068C080000016F1D00000A00076F1B00000A729DC700701F096F1C00000A0E046F1A0100068C080000016F1D00000A00076F1B00000A72BFC700701F096F1C00000A0E047BA60400048C080000016F1D00000A00076F1B00000A72D7C700701F096F1C00000A0E047BA70400048C080000016F1D00000A00076F1B00000A72F9C700701F096F1C00000A046F1B0100068C080000016F1D00000A00076F1B00000A7225C800701F096F1C00000A046F1A0100068C080000016F1D00000A00076F1B00000A7243BF00701F096F1C00000A046F1C0100068C080000016F1D00000A00076F1B00000A724BC800701F096F1C00000A056F1B0100068C080000016F1D00000A00076F1B00000A7271C800701F096F1C00000A056F1A0100068C080000016F1D00000A00076F1B00000A7291C800701F096F1C00000A056F1C0100068C080000016F1D00000A00076F1B00000A72A7C800701F096F1C00000A0E058C080000016F1D00000A00076F1B00000A726B910070166F1C00000A028C280000016F1D00000A00076F1E00000A17FE010CDE1A0714FE010D092D07076F1F00000A00DC260000DE0000160C2B0000082A0000004134000002000000F1000000370200002803000010000000000000000000000001000000370300003803000005000000010000011B300300830100002E0000110000731600000A0A06166F6100000A0006725FBF00706F1700000A260672C9C800706F1700000A26067235C900706F1700000A260672A1C900706F1700000A26067203CA00706F1700000A26067271CA00706F1700000A260672D5CA00706F1700000A2606723BCB00706F1700000A26066F1800000A0E076F1900000A0E07731A00000A0B00076F1B00000A72A5CB00701F096F1C00000A038C080000016F1D00000A00076F1B00000A72C9CB00701F096F1C00000A048C080000016F1D00000A00076F1B00000A72EDCB00701F096F1C00000A058C080000016F1D00000A00076F1B00000A7207CC00701F096F1C00000A0E058C080000016F1D00000A00076F1B00000A722DCC00701F096F1C00000A0E048C080000016F1D00000A00076F1B00000A7249CC00701F096F1C00000A0E068C080000016F1D00000A00076F1B00000A726B910070166F1C00000A028C280000016F1D00000A00076F1E00000A17FE010CDE1A0714FE010D092D07076F1F00000A00DC260000DE0000160C2B0000082A00413400000200000085000000E10000006601000010000000000000000000000001000000750100007601000005000000010000011B300300860000002E0000110000731600000A0A067267CC00706F1700000A2606728FCC00706F1700000A260672EBCC00706F1700000A26066F1800000A036F1900000A03731A00000A0B00076F1B00000A727E170070166F1C00000A028C280000016F1D00000A00076F1E00000A17FE010CDE1A0714FE010D092D07076F1F00000A00DC260000DE0000160C2B0000082A0000011C000002003F002A690010000000000000010078790005010000011B300500D60200000B000011000416731100000A81080000010516731100000A81080000010E04281D0100065100731600000A0A067259CD00706F1700000A260672DECD00706F1700000A26067263CE00706F1700000A260672E8CE00706F1700000A2606726DCF00706F1700000A26066F7C00000A26067267CC00706F1700000A260672F6CF00706F1700000A26067246D000706F1700000A26067296D000706F1700000A260672E6D000706F1700000A26067236D100706F1700000A26067286D100706F1700000A260672D6D100706F1700000A26067232D200706F1700000A2606728ED200706F1700000A260672EAD200706F1700000A26067246D300706F1700000A260672A2D300706F1700000A260672FED300706F1700000A2606725AD400706F1700000A260672B6D400706F1700000A26067212D500706F1700000A26067282D500706F1700000A260672F2D500706F1700000A26067262D600706F1700000A260672D2D600706F1700000A26067222D700706F1700000A2606726CD700706F1700000A260672C4D700706F1700000A26067228D800706F1700000A2606728CD800706F1700000A260672FF8500706F1700000A2603166AFE01130411042D0E000672BCD800706F1700000A260006722CD900706F1700000A26067258D900706F1700000A26066F1800000A0E056F1900000A0E05731A00000A0B00076F1B00000A727E170070166F1C00000A028C280000016F1D00000A00076F1B00000A7275050070166F1C00000A038C280000016F1D00000A00076F3300000A0C00086F3400000A130411042D0800160DDD870000000408166F6500000A81080000010508176F6500000A81080000010E0408186F6500000A08196F6500000A081A6F6500000A73150100065100DE120814FE01130411042D07086F1F00000A00DC0000DE120714FE01130411042D07076F1F00000A00DC000203166A0E0528BB000006130411042D0500160DDE0E170DDE0A260000DE0000160D2B0000092A0000414C000002000000370200004F00000086020000120000000000000002000000F5010000A70000009C02000012000000000000000000000021000000A8020000C902000005000000010000011B3003001B030000380000110000731600000A0A06728E6100706F1700000A26067294D900706F1700000A26067217DA00706F1700000A2606726F5A00706F1700000A2603166AFE01130711072D2900067273DA00706F1700000A2604166AFE01130711072D0E000672D5DA00706F1700000A2600002B0E00067237DB00706F1700000A2600066F1800000A056F1900000A05731A00000A0C00086F1B00000A727E170070166F1C00000A028C280000016F1D00000A00086F1B00000A72566200701E6F1C00000A188C0A0000026F1D00000A00086F1B00000A7275050070166F1C00000A038C280000016F1D00000A00086F1B00000A728DDB0070166F1C00000A048C280000016F1D00000A0008736900000A0D00733900000A0B09076F6A00000A2600DE120914FE01130711072D07096F1F00000A00DC0000DE120814FE01130711072D07086F1F00000A00DC00076F4600000A6F4800000A16FE0116FE01130711072D0900171306DDB301000006166F6100000A000672DA6200706F1700000A260672ADDB00706F1700000A260672F9DB00706F1700000A260672B06300706F1700000A26067245DC00706F1700000A2606729DDC00706F1700000A260672F7DC00706F1700000A2603166AFE01130711072D2700067273DA00706F1700000A2604166AFE01130711072D0E000672D5DA00706F1700000A260000066F1800000A056F1900000A05731A00000A0C00086F1B00000A72A3640070166F1C00000A1304086F1B00000A727E170070166F1C00000A028C280000016F1D00000A00086F1B00000A72566200701E6F1C00000A188C0A0000026F1D00000A00086F1B00000A7275050070166F1C00000A038C280000016F1D00000A00086F1B00000A728DDB0070166F1C00000A048C280000016F1D00000A0000076F4600000A6F4D00000A13082B2711086F4E00000A740B00000113050011041105166F6B00000A6F1D00000A00086F1E00000A260011086F4F00000A130711072DCCDE1D110875050000011309110914FE01130711072D0811096F1F00000A00DC0000DE120814FE01130711072D07086F1F00000A00DC00171306DE0B260000DE00001613062B000011062A00417C00000200000009010000120000001B0100001200000000000000020000008D000000A4000000310100001200000000000000020000009B02000038000000D30200001D000000000000000200000005020000EF000000F4020000120000000000000000000000010000000B0300000C03000005000000010000011B3003003F020000380000110000731600000A0A06728E6100706F1700000A26067294D900706F1700000A26067257DD00706F1700000A260672A9DD00706F1700000A26067201DE00706F1700000A26066F1800000A036F1900000A03731A00000A0C00086F1B00000A727E170070166F1C00000A028C280000016F1D00000A00086F1B00000A72566200701E6F1C00000A188C0A0000026F1D00000A0008736900000A0D00733900000A0B09076F6A00000A2600DE120914FE01130711072D07096F1F00000A00DC0000DE120814FE01130711072D07086F1F00000A00DC00076F4600000A6F4800000A16FE0116FE01130711072D0900171306DD4701000006166F6100000A000672DA6200706F1700000A2606723FDE00706F1700000A260672B06300706F1700000A26067212DF00706F1700000A26067262DF00706F1700000A260672A9DD00706F1700000A26067201DE00706F1700000A26066F1800000A036F1900000A03731A00000A0C00086F1B00000A72B4DF0070166F1C00000A1304086F1B00000A727E170070166F1C00000A028C280000016F1D00000A00086F1B00000A72566200701E6F1C00000A188C0A0000026F1D00000A0000076F4600000A6F4D00000A13082B2711086F4E00000A740B00000113050011041105166F6B00000A6F1D00000A00086F1E00000A260011086F4F00000A130711072DCCDE1D110875050000011309110914FE01130711072D0811096F1F00000A00DC0000DE120814FE01130711072D07086F1F00000A00DC00171306DE0B260000DE00001613062B000011062A00417C0000020000009900000012000000AB000000120000000000000002000000570000006A000000C1000000120000000000000002000000BF01000038000000F70100001D000000000000000200000063010000B500000018020000120000000000000000000000010000002F0200003002000005000000010000011B300400670300003900001100160C733900000A0B150D00731600000A0A06728E6100706F1700000A26067294D900706F1700000A26067257DD00706F1700000A260672A9DD00706F1700000A2606722A6000706F1700000A260672C8DF00706F1700000A26066F1800000A036F1900000A03731A00000A13040011046F1B00000A727E170070166F1C00000A028C280000016F1D00000A0011046F1B00000A72746200701E6F1C00000A178C090000026F1D00000A0011046F1B00000A72946200701E6F1C00000A188C090000026F1D00000A0011046F1B00000A72566200701E6F1C00000A188C0A0000026F1D00000A0011046F1B00000A724BE000701E6F1C00000A198C0A0000026F1D00000A001104736900000A1305001105076F6A00000A2600DE14110514FE01130911092D0811056F1F00000A00DC0000DE14110414FE01130911092D0811046F1F00000A00DC00160D076F4600000A6F4800000A16FE0116FE01130911092D0900171308DDF901000006166F6100000A000672DA6200706F1700000A2606726FE000706F1700000A260672B06300706F1700000A26067212DF00706F1700000A26067262DF00706F1700000A260672A9DD00706F1700000A260672CBE000706F1700000A2606722A6000706F1700000A260672C8DF00706F1700000A26066F1800000A036F1900000A03731A00000A13040011046F1B00000A72A3640070166F1C00000A130611046F1B00000A727E170070166F1C00000A028C280000016F1D00000A0011046F1B00000A72746200701E6F1C00000A178C090000026F1D00000A0011046F1B00000A72946200701E6F1C00000A188C090000026F1D00000A0011046F1B00000A72566200701E6F1C00000A188C0A0000026F1D00000A0011046F1B00000A724BE000701E6F1C00000A198C0A0000026F1D00000A0000076F4600000A6F4D00000A130A2B2A110A6F4E00000A740B00000113070011061107166F6B00000A6F1D00000A000911046F1E00000A580D00110A6F4F00000A130911092DC9DE1D110A7505000001130B110B14FE01130911092D08110B6F1F00000A00DC0000DE14110414FE01130911092D0811046F1F00000A00DC00171308DE422600170C00DE0000DE3200082D13076F4600000A6F4800000A09FE0116FE012B011700130911092D1100021616731100000A0328F5000006260000DC001613082B000011082A0041940000020000000E0100000D0000001B0100001400000000000000020000006E000000C500000033010000140000000000000002000000AB0200003B000000E60200001D0000000000000002000000F201000015010000070300001400000000000000000000000B00000016030000210300000700000001000001020000000B000000200300002B03000032000000000000001B300300050100003A00001100166A0C00731600000A0A067209E100706F1700000A26067259E100706F1700000A26067283E100706F1700000A260672F9E100706F1700000A26067269E200706F1700000A26066F1800000A056F1900000A05731A00000A0D00096F1B00000A7275050070166F1C00000A028C280000016F1D00000A00096F1B00000A72BDE200701E6F1C00000A188C2D0000016F1D00000A00096F3100000A0B072C0A077E3200000AFE012B011700130511052D090007A5280000010C0000DE120914FE01130511052D07096F1F00000A00DC0008166AFE0116FE01130511052D0600171304DE1D0304282600000A080528BF0000061304DE0B260000DE00001613042B000011042A000000011C000002005A0063BD00120000000000000400F2F60005010000011B300300CC0000002E00001100731600000A0A0672D7E200706F1700000A26067215E300706F1700000A260672AAE300706F1700000A26067218E400706F1700000A260672BBE400706F1700000A2600066F1800000A046F1900000A04731A00000A0B00076F1B00000A721DE50070166F1C00000A038C280000016F1D00000A00076F1B00000A72CD0500701F096F1C00000A028C080000016F1D00000A00076F1E00000A17FE0116FE010D092D0500170CDE2100DE100714FE010D092D07076F1F00000A00DC0000DE05260000DE0000160C2B0000082A011C00000200570054AB001000000000000043007CBF00050100000113300A001F0000003B000011007E2F00000A0B020304050E0416070E0512000E0628C40000060C2B00082A0013300A001A0000003C00001100020304050E04160E050E0612000E0728C40000060B2B00072A000013300A001F0000003D000011007E2F00000A0A020304050E0416060E060E070E0828C40000060B2B00072A001B300300760100002E0000110002165400731600000A0A067247E500706F1700000A260672EEE500706F1700000A26067295E600706F1700000A2606723CE700706F1700000A260672E3E700706F1700000A2606728AE800706F1700000A26067231E900706F1700000A260672EEE500706F1700000A260672D8E900706F1700000A26067295E600706F1700000A2606723CE700706F1700000A260672E3E700706F1700000A2606727FEA00706F1700000A26067226EB00706F1700000A260672CDEB00706F1700000A260672EEE500706F1700000A260672D8E900706F1700000A26067274EC00706F1700000A2606721BED00706F1700000A260672C2ED00706F1700000A260672EEE500706F1700000A26067269EE00706F1700000A26066F1800000A046F1900000A04731A00000A0B00076F1B00000A727E170070166F1C00000A038C280000016F1D00000A0002076F3100000AA52D0000015400DE100714FE010D092D07076F1F00000A00DC00170CDE0A260000DE0000160C2B0000082A00004134000002000000260100002E0000005401000010000000000000000000000004000000650100006901000005000000010000011B300400960700003E000011000E08733900000A517E2F00000A13070E0716731100000A0E047BA6040004282500000A16731100000A0E047BA7040004282500000A16731100000A73150100065112070E0928E8000006261206020E0928C30000062C0A1106172E291106182E241107283800000A2D170E06283800000A2D0E11070E06285A00000A16FE012B0117002B011600130D110D2D2F000E070E046F1E0100067314010006510E075016731100000A7DA80400040E075016731100000A7DB0040004002B40001107283800000A2D0E11070E06283000000A16FE012B011700130D110D2D1F000E070E046F1F0100067314010006510E075016731100000A7DB0040004000004210000000000000080FE01130D110D2D26000E047BA804000416731100000A287100000A16FE01130D110D2D090017130C385E0600000072010000701304160D731600000A0A0006166F6100000A00067210EF00706F1700000A26067234EF00706F1700000A2606726EEF00706F1700000A260672ECEF00706F1700000A26066F1800000A0E096F1900000A0E09731A00000A13080011086F1B00000A725B050070166F1C00000A038C280000016F1D00000A0011086F3300000A13090011096F3400000A130D110D2D090016130CDDC00500001109166F6D00000A13041109176F7800000A0D00DE14110914FE01130D110D2D0811096F1F00000A00DC0000DE14110814FE01130D110D2D0811086F1F00000A00DC000916FE01130D110D2D090017130CDD6A0500000E08506F3A00000A723CF0007072D1140070283B00000A6F3C00000A260E08506F3A00000A7258F000707237150070283B00000A6F3C00000A260E08506F3A00000A7278F000707277150070283B00000A6F3C00000A2606166F6100000A0006728E6100706F1700000A26067290F000706F1700000A2606721BF100706F1700000A2606727AF200706F1700000A260672B0F200706F1700000A26067294D900706F1700000A260672DEF200706F1700000A26067217DA00706F1700000A2606726F5A00706F1700000A26067261F300706F1700000A260672CFF300706F1700000A2606722A6000706F1700000A26067268F400706F1700000A26066F1800000A0E096F1900000A0E09731A00000A13080011086F1B00000A727E170070166F1C00000A028C280000016F1D00000A0011086F1B00000A72566200701E6F1C00000A188C0A0000026F1D00000A0011086F1B00000A72A4F400701E6F1C00000A178C090000026F1D00000A0011086F1B00000A72B8F400701E6F1C00000A188C090000026F1D00000A0011086F1B00000A725B0500701E6F1C00000A038C280000016F1D00000A0011086F1B00000A72CCF400701E6F1C00000A198C2E0000026F1D00000A0011086F3300000A1309002B5F001109176F3600000A16FE0116FE01130D110D2D03002B470E08506F4400000A13051105161109166F3500000A8C280000016F7D00000A001105181109186F6500000A8C080000016F7D00000A000E08506F4600000A11056F4700000A000011096F3400000A130D110D2D9400DE14110914FE01130D110D2D0811096F1F00000A00DC0000DE14110814FE01130D110D2D0811086F1F00000A00DC0006166F6100000A000672FAF400706F1700000A26067234F500706F1700000A26067294F500706F1700000A260672F4F500706F1700000A26067238F600706F1700000A2606727CF600706F1700000A260672FCF600706F1700000A26067252F700706F1700000A260672AAF700706F1700000A26067208F800706F1700000A26067274F800706F1700000A2606720BF900706F1700000A26066F1800000A0E096F1900000A0E09731A00000A13080011086F1B00000A72A3640070166F1C00000A130A11086F1B00000A727E170070166F1C00000A028C280000016F1D00000A0011086F1B00000A72566200701E6F1C00000A188C0A0000026F1D00000A0011086F1B00000A72A4F400701E6F1C00000A178C090000026F1D00000A0011086F1B00000A72B8F400701E6F1C00000A188C090000026F1D00000A000E0516FE01130D110D2D3E0011086F1B00000A7275050070166F1C00000A7E3200000A6F1D00000A0011086F1B00000A728DDB0070166F1C00000A7E3200000A6F1D00000A00002B3E0011086F1B00000A7275050070166F1C00000A048C280000016F1D00000A0011086F1B00000A728DDB0070166F1C00000A058C280000016F1D00000A0000000E08506F4600000A6F4D00000A130E38A5000000110E6F4E00000A740B000001130B00110A110B166F6B00000A6F1D00000A0011086F3300000A13090011096F3400000A130D110D2D0300DE6C1109166F3600000A0B1109176F6500000A0C16731100000A08282500000A0C07130F110F4504000000180000000200000002000000180000002B160E0750257BA804000408282700000A7DA80400042B022B0000DE14110914FE01130D110D2D0811096F1F00000A00DC000000110E6F4F00000A130D110D3A4AFFFFFFDE1D110E75050000011310111014FE01130D110D2D0811106F1F00000A00DC0000DE14110814FE01130D110D2D0811086F1F00000A00DC000E075016731100000A0E07507BA80400040E047BA8040004282D00000A282500000A7DA804000417130CDE0B260000DE000016130C2B0000110C2A000041C4000002000000BB0100002D000000E8010000140000000000000002000000930100006D00000000020000140000000000000002000000F703000072000000690400001400000000000000020000003903000048010000810400001400000000000000020000009506000067000000FC06000014000000000000000200000068060000BD000000250700001D0000000000000002000000440500000202000046070000140000000000000000000000440100004306000087070000050000000100000113300300500000003F00001100731600000A0B03281D0100065102166AFE0116FE010D092D0500160C2B2F0212000428C90000060D092D0500160C2B1D06070428C70000060D092D0500160C2B0C03067B8D04000451170C2B00082A1B30030081000000400000110002165400731600000A0A06166F6100000A00067257F900706F1700000A26066F1800000A036F1900000A03731A00000A0B0002076F3100000AA52D0000015400DE100714FE010D092D07076F1F00000A00DC00024A13041104450200000002000000020000002B022B050216542B00170CDE0A260000DE0000160C2B0000082A000000011C0000020032001143001000000000000004007074000501000001133004000F0000000D000011000203160428C80000060A2B00062A001B300600840B00004100001100027B8704000416731100000A282800000A2D13027B8804000416731100000A282800000A2B0117000D027B9004000416FE01131011102D3E00092D18027B920400047BA804000416731100000A282C00000A2B0117000D092D18027B920400047BA704000416731100000A282C00000A2B0117000D0000731600000A0A0916FE01131011103A290700000012020528C6000006131011102D15000372B6FA00706F1700000A2616130FDDD10A0000027B9004000416FE01131011102D0400170C0006166F6100000A000672F4FA00706F1700000A26067226FB00706F1700000A2606725AFB00706F1700000A2606728CFB00706F1700000A260672BCFB00706F1700000A260672E6FB00706F1700000A26067220FC00706F1700000A26067256FC00706F1700000A260672DFFC00706F1700000A2606723DFD00706F1700000A260672A1FD00706F1700000A26067207FE00706F1700000A26066F1800000A056F1900000A05731A00000A13040011046F1B00000A7275050070166F1C00000A027B840400048C280000016F1D00000A0011046F1B00000A727E170070166F1C00000A027B850400048C280000016F1D00000A0011046F1B00000A72566200701E6F1C00000A188C2D0000016F1D00000A0002733900000A7D8604000411046F3300000A130500027B8604000411056F7E00000A0000DE14110514FE01131011102D0811056F1F00000A00DC0000DE14110414FE01131011102D0811046F1F00000A00DC0008131111114502000000050000005A010000389E01000016731100000A1308027B8D0400046F1C01000616731100000A282C00000A2C1B027B8D0400046F1B01000616731100000A282C00000A16FE012B011700131011102D1F00027B8D0400046F1B010006027B8D0400046F1C010006282A00000A130800170228EE00000600180228EB000006000228EF0000060016731100000A027B87040004027B88040004282D00000A282500000A130911091108286F00000A1817287F00000A1309110916731100000A282C00000A16FE01131011102D6600027B870400041109282600000A1306027B880400041109282600000A13070211097D870400040211097D88040004021728E900000600170228EB0000060002027B870400041106282700000A7D8704000402027B880400041107282700000A7D8804000400021628E9000006000228EA00000600170228EE00000600180228EB000006000228EF00000600170228EB000006002B680228EA00000600180228EB00000600170228EB000006000228EF00000600027B9004000416FE01131011102D1A00180228EC00000600170228EC00000600190228ED00000600002B1F037245FE0070088C5E000002287A00000A6F1700000A2616130FDD860700000216731100000A027B87040004282500000A7D870400040216731100000A027B88040004282500000A7D88040004027B86040004720100007072010000701F106F8000000A0B078E6916FE0216FE01131011103A610300000006166F6100000A000672FAF400706F1700000A2606727DFE00706F1700000A260672D1FE00706F1700000A2606721DFF00706F1700000A2606727CF600706F1700000A260672FCF600706F1700000A26067252F700706F1700000A260672AAF700706F1700000A2606726FFF00706F1700000A26735300000A130A00110A066F1800000A056F1900000A05731A00000A6F8100000A00110A6F8200000A6F1B00000A72CFFF00701F096F1C00000A7278F000706F5000000A00110A6F8200000A6F1B00000A72E3FF00701F096F1C00000A72EFFF00706F5000000A00110A6F8200000A6F1B00000A72FFFF00701F096F1C00000A72110001706F5000000A00110A6F8200000A6F1B00000A72A3640070166F1C00000A723CF000706F5000000A00110A6F8200000A6F1B00000A727E170070166F1C00000A027B850400048C280000016F1D00000A00110A6F8200000A6F1B00000A72566200701E6F1C00000A188C2D0000016F1D00000A00110A6F8200000A6F1B00000A7275050070166F1C00000A027B840400048C280000016F1D00000A00110A6F8200000A166F5200000A00110A176F6200000A00110A176F8300000A00110A076F8400000A130B110B078E69FE01131011103A7901000000037227000170110B8C2D000001078E698C2D000001288500000A6F1700000A260007131216131338A4000000111211139A130C0003728F0001701B8D010000011314111416110C723CF000706F4A00000AA2111417110C7258F000706F4A00000AA2111418110C7278F000706F4A00000AA2111419110C72110001706F4A00000AA211141A110C72EFFF00706F4A00000AA21114282400000A6F1700000A26110C6F8600000A16FE01131011102D1A0003722C010170110C6F8700000A286800000A6F1700000A260000111317581313111311128E69FE04131011103A4BFFFFFF03723C010170110A6F8200000A6F8800000A286800000A6F1700000A2600110A6F8200000A6F1B00000A6F8900000A13152B2F11156F4E00000A740E000001130D00037264010170110D6F8A00000A110D6F6300000A288500000A6F1700000A260011156F4F00000A131011102DC4DE1D111575050000011316111614FE01131011102D0811166F1F00000A00DC0016130FDDE603000000DE14110A14FE01131011102D08110A6F1F00000A00DC000000027B9004000416FE01131011102D0E0002027B910400047D8D0400040006166F6100000A0006728C0101706F1700000A260672020201706F1700000A260672780201706F1700000A260672EE0201706F1700000A26066F7C00000A26067267CC00706F1700000A260416FE01131011102D280006726A0301706F1700000A260672390401706F1700000A260672080501706F1700000A26002B26000672D70501706F1700000A260672640601706F1700000A260672F10601706F1700000A260006727E0701706F1700000A260672330801706F1700000A260672E80801706F1700000A260672970901706F1700000A260672460A01706F1700000A260672F70A01706F1700000A260672A20B01706F1700000A260672FC0B01706F1700000A260672560C01706F1700000A260672B00C01706F1700000A2606728CD800706F1700000A2606720A0D01706F1700000A260672FF8500706F1700000A2606722CD900706F1700000A26067258D900706F1700000A26066F1800000A056F1900000A05731A00000A13040011046F1B00000A727E170070166F1C00000A027B850400048C280000016F1D00000A0011046F1B00000A72320D01701F096F1C00000A027B8D0400047BA60400048C080000016F1D00000A0011046F1B00000A725C0D01701F096F1C00000A027B8D0400047BA70400048C080000016F1D00000A0011046F1B00000A72900D01701F096F1C00000A027B8D0400047BA80400048C080000016F1D00000A0011046F1B00000A72C40D01701F096F1C00000A027B890400048C080000016F1D00000A0011046F1B00000A72EC0D01701F096F1C00000A027B8A0400048C080000016F1D00000A0011046F1B00000A72140E01701F096F1C00000A027B8B0400048C080000016F1D00000A0011046F1B00000A72360E01701F096F1C00000A027B8C0400048C080000016F1D00000A0011046F1B00000A72580E01701F096F1C00000A027B870400048C080000016F1D00000A0011046F1B00000A727C0E01701F096F1C00000A027B880400048C080000016F1D00000A0011046F3300000A13050011056F3400000A131011102D250003729A0E0170027B850400048C28000001287A00000A6F1700000A2616130FDD91000000021105166F6500000A16731100000A1105176F6500000A73150100067D8E040004021105186F6500000A16731100000A1105196F6500000A73150100067D8F04000400DE14110514FE01131011102D0811056F1F00000A00DC0000DE14110414FE01131011102D0811046F1F00000A00DC0017130FDE1A130E0003110E6F7600000A6F1700000A2600DE000016130F2B0000110F2A41AC000002000000E701000012000000F90100001400000000000000020000006E010000A3000000110200001400000000000000020000003407000040000000740700001D0000000000000002000000CE040000CF0200009D070000140000000000000002000000BC0A000078000000340B00001400000000000000020000003C090000100200004C0B000014000000000000000000000077000000EF0A0000660B000014000000390000011B300500E90100000B0000110003731001000651731600000A0A0672F00E01706F1700000A260672750F01706F1700000A26067263CE00706F1700000A260672E8CE00706F1700000A260672FA0F01706F1700000A2606727F1001706F1700000A26066F7C00000A26067267CC00706F1700000A2606720A1101706F1700000A2606721F1201706F1700000A260672341301706F1700000A260672E51301706F1700000A2606726CD700706F1700000A260672C4D700706F1700000A26067228D800706F1700000A260672961401706F1700000A2606728CD800706F1700000A260672D81401706F1700000A2606729D1501706F1700000A26066F7C00000A2606722CD900706F1700000A26067258D900706F1700000A2600066F1800000A046F1900000A04731A00000A0B00076F1B00000A7275050070166F1C00000A028C280000016F1D00000A00076F3300000A0C00086F3400000A130411042D0800160DDD8C00000003731001000651035008166F6500000A7D87040004035008176F6500000A7D88040004035008186F6500000A08196F6500000A081A6F6500000A73150100067D8D0400040350081B6F3500000A7D850400040350027D84040004170DDE2E0814FE01130411042D07086F1F00000A00DC0714FE01130411042D07076F1F00000A00DC260000DE0000160D2B0000092A000000012800000200450173B801120000000002002001AACA01120000000000000C01D0DC0105010000011B3005000D0200000B0000110003731001000651731600000A0A0672F00E01706F1700000A260672750F01706F1700000A26067263CE00706F1700000A260672E8CE00706F1700000A260672FA0F01706F1700000A260672031601706F1700000A2606727F1001706F1700000A26066F7C00000A26067267CC00706F1700000A2606720A1101706F1700000A2606721F1201706F1700000A260672341301706F1700000A260672E51301706F1700000A2606726CD700706F1700000A260672C4D700706F1700000A26067228D800706F1700000A260672881601706F1700000A260672961401706F1700000A2606728CD800706F1700000A260672D81401706F1700000A2606729D1501706F1700000A26066F7C00000A2606722CD900706F1700000A26067258D900706F1700000A2600066F1800000A046F1900000A04731A00000A0B00076F1B00000A7275050070166F1C00000A028C280000016F1D00000A00076F3300000A0C00086F3400000A130411042D0800160DDD9800000003731001000651035008166F6500000A7D87040004035008176F6500000A7D88040004035008186F6500000A081B6F6500000A282700000A08196F6500000A081A6F6500000A73150100067D8D0400040350081C6F3500000A7D850400040350027D84040004170DDE2E0814FE01130411042D07086F1F00000A00DC0714FE01130411042D07076F1F00000A00DC260000DE0000160D2B0000092A0000000128000002005D017FDC01120000000002003801B6EE01120000000000002401DC0002050100000113300400170100004200001100027B5E0400046F020100060B027B5E0400046F030100060C027B5E0400046F040100060D027B5E0400046F07010006130473100100060A06027B5B0400047B660400047D8704000406027B5B0400047B680400047D8804000406027B550400047D8504000406067B87040004091104282600000A16731100000A282500000A282D00000A7D8A04000406067B87040004067B8A040004282600000A7D8904000406027B5B0400047B680400047D8B0400040616731100000A7D8C04000406067B8904000416731100000A067B8A04000473150100067D8E04000406067B8B04000416731100000A067B8C04000473150100067D8F04000406067B8E040004067B8F04000428210100067D8D0400040613052B0011052A00133008001A000000430000110002281D01000603120004120212010528D40000060D2B00092A0000133009001C000000430000110002281D0100060304120005120212010E0428D50000060D2B00092A13300800170000004400001100020304050E04120112000E0528D40000060C2B00082A00133008004D00000045000011000E0414510E0516731100000A8108000001027E2F00000A0304160512000E0628D700000616FE010C082D1B000E04067BC3040004510E05067BC40400048108000001170B2B04160B2B00072A000000133008001B0000004600001100020316731100000A041201120212000528D40000060D2B00092A00133008001B0000004700001100020316731100000A0412000512010E0428D40000060C2B00082A00133008001B0000004800001100020316731100000A04050E0412000E0528D40000060B2B00072A00133008001B0000004900001100020316731100000A041200050E040E0528D40000060B2B00072A00133007006A000000450000110005281D010006510E0416731100000A81080000010E05166A550E06281D01000651027E2F00000A03041612000E0728D60000060C082D0500160B2B2B05067BC3040004510E04067BC404000481080000010E05067BBB040004550E06067BC204000451170B2B00072A0000133007006C00000045000011000E04281D010006510E0516731100000A81080000010E06166A550E07281D01000651027E2F00000A03040512000E0828D60000060C082D0500160B2B2C0E04067BC3040004510E05067BC404000481080000010E06067BBB040004550E07067BC204000451170B2B00072A13300800160000000D00001100020304050E04160E050E0628D70000060A2B00062A00001B300A00DE0B00004A00001100170C190D1A130472010000700B0E067334010006510E051A5F16FE0116FE0113070E051E5F16FE0116FE011308729617007072AA1700700E0728F90000061205284C00000A26110517FE011306731600000A0A0672BA1601706F1700000A260672021701706F1700000A260672661701706F1700000A260672AE1701706F1700000A260672F61701706F1700000A2606723E1801706F1700000A260672861801706F1700000A260672E21801706F1700000A2606722E1901706F1700000A2606727A1901706F1700000A260672C61901706F1700000A260672121A01706F1700000A260672701A01706F1700000A260672C81A01706F1700000A260672421B01706F1700000A260672BA1B01706F1700000A260672321C01706F1700000A260672AA1C01706F1700000A2606720E1D01706F1700000A2606726E1D01706F1700000A260672CE1D01706F1700000A2606722E1E01706F1700000A2606728E1E01706F1700000A260672EE1E01706F1700000A2606724E1F01706F1700000A26066F7C00000A260672661F01706F1700000A260672A41F01706F1700000A260672FA1F01706F1700000A260516731100000A282200000A2C040E042B011700130D110D2D0E000672162001706F1700000A260002166AFE01130D110D2D10000672DF2001706F1700000A26002B1A000672312101706F1700000A260672C22101706F1700000A26000672852201706F1700000A260672F92201706F1700000A2606726D2301706F1700000A260516731100000A282200000A2C040E042B011700130D110D2D0E000672E12301706F1700000A26000672552401706F1700000A260672CD2401706F1700000A260672452501706F1700000A260672C12501706F1700000A260672192601706F1700000A2606728F2601706F1700000A260672F12601706F1700000A260672532701706F1700000A260672B52701706F1700000A260672172801706F1700000A260672792801706F1700000A260672DB2801706F1700000A2606723D2901706F1700000A2606729F2901706F1700000A260672012A01706F1700000A260672792801706F1700000A260672632A01706F1700000A26067267CC00706F1700000A260672C52A01706F1700000A260672E42B01706F1700000A2606726B2C01706F1700000A260672FC2C01706F1700000A2606728D2D01706F1700000A261106130D110D2D5F000672222E01706F1700000A2606723D2F01706F1700000A260672583001706F1700000A260672733101706F1700000A26047BAC04000416FE01130D110D2D100006728E3201706F1700000A26002B0E000672F03201706F1700000A2600000672563301706F1700000A2606729A3301706F1700000A260672E43301706F1700000A2606722E3401706F1700000A260672783401706F1700000A260672D83401706F1700000A260672383501706F1700000A260672883501706F1700000A260672D83501706F1700000A260672283601706F1700000A260672723601706F1700000A260672D03601706F1700000A2606722E3701706F1700000A260672A83701706F1700000A2606720A3801706F1700000A260672783801706F1700000A260672E63801706F1700000A261106130D110D2D1C0006725C3901706F1700000A260672BC3901706F1700000A26002B1A0006721C3A01706F1700000A2606721C3A01706F1700000A2600110716FE01130D110D2D10000672363A01706F1700000A26002B0E0006721C3A01706F1700000A2600110816FE01130D110D2D10000672803A01706F1700000A26002B0E000672C63A01706F1700000A26000672DC3A01706F1700000A260672143B01706F1700000A2606728CD800706F1700000A260672563B01706F1700000A260672823B01706F1700000A26110716FE01130D110D2D0E000672713C01706F1700000A2600110816FE01130D110D2D0E000672603D01706F1700000A26000672533E01706F1700000A26066F7C00000A2606722CD900706F1700000A26067258D900706F1700000A2600066F1800000A0E076F1900000A0E07731A00000A13090002166AFE0116FE01130D110D2D4C00036F8B00000A1F141F306F8C00000A0B11096F1B00000A72CB3E01701F0C1F326F2E00000A036F1D00000A0011096F1B00000A72EF3E01701F0C1F326F2E00000A076F1D00000A00002B200011096F1B00000A727E170070166F1C00000A028C280000016F1D00000A000011096F1B00000A720D3F01701F096F1C00000A047BA60400048C080000016F1D00000A0011096F1B00000A72253F01701F096F1C00000A047BA70400048C080000016F1D00000A0011096F1B00000A72473F01701F096F1C00000A047BA80400048C080000016F1D00000A0011096F1B00000A72693F01701F096F1C00000A047BB00400048C080000016F1D00000A0011096F1B00000A728F3F01701F096F1C00000A058C080000016F1D00000A001106130D110D2D490011096F1B00000A72A13F01701F096F1C00000A047BA90400048C080000016F1D00000A0011096F1B00000A72B73F0170186F1C00000A047BAB0400048C430000016F1D00000A000011096F1B00000A72D53F01701E6F1C00000A088C2D0000016F1D00000A00110716FE01130D110D2D200011096F1B00000A72F13F01701E6F1C00000A098C2D0000016F1D00000A0000110816FE01130D110D2D210011096F1B00000A720D4001701E6F1C00000A11048C2D0000016F1D00000A000011096F3300000A130A00110A6F3400000A130D110D2D580002166AFE0116FE01130D110D2D21000E06507229400170036F8B00000A7253400170284300000A7DC5040004002B20000E065072854001700F00282000000A729D400170284300000A7DC50400040016130CDDA00300000E0650110A166F3500000A7DB30400040E0650110A176F7800000A7DB40400040E0650110A186F3600000A7DB50400040E0650110A196F3600000A7DB60400040E06507BB604000416FE0216FE01130D110D2D22000E0650110A1A6F3600000A7DB70400040E0650110A1B6F6D00000A7DB8040004000E0650110A1C6F6500000A110A1D6F6500000A110A1E6F6500000A110A1F116F6500000A110A1F126F7800000A110A1F136F6500000A110A1F146F6500000A110A1F166F6500000A73190100067DC30400040E0650110A1F096F6500000A7DC40400040E0650110A1F0A6F6E00000A2D0B110A1F0A6F3500000A2B02166A007DBB0400040E0650110A1F0B6F6E00000A2D0B110A1F0B6F3600000A2B0116007DBC0400040E06507BBB040004166AFE01130D110D2D71000E0650110A1F0C6F6E00000A2D0B110A1F0C6F3500000A2B02166A007DC10400040E06507BC1040004166AFE01130D110D2D3C000E0650110A1F0D6F6500000A110A1F0E6F6500000A110A1F0F6F6500000A110A1F116F6500000A110A1F126F7800000A73170100067DC204000400000E0650110A1F106F6E00000A2D0B110A1F106F3500000A2B02166A007DBA0400040E0650110A1F156F3600000A7DB904000400DE14110A14FE01130D110D2D08110A6F1F00000A00DC0000DE14110914FE01130D110D2D0811096F1F00000A00DC000E06507BBB040004166AFE01130D110D3A1E0100000006166F6100000A000672CD4001706F1700000A260672F74001706F1700000A2606724B4101706F1700000A260672874101706F1700000A260672B34101706F1700000A260672E54101706F1700000A26066F1800000A0E076F1900000A0E07731A00000A13090011096F1B00000A7275050070166F1C00000A0E06507BBB0400048C280000016F1D00000A0011096F3300000A130A00110A6F3400000A130D110D2D090016130CDDDF0000000E0650110A166F3600000A7DBD0400040E0650110A176F7800000A7DBE0400040E0650110A186F6500000A7DBF0400040E0650110A196F6500000A7DC004000400DE14110A14FE01130D110D2D08110A6F1F00000A00DC0000DE14110914FE01130D110D2D0811096F1F00000A00DC000017130CDE69130B0002166AFE0116FE01130D110D2D28000E06507229400170036F8B00000A7245420170110B6F7600000A287400000A7DC5040004002B27000E065072854001700F00282000000A7269420170110B6F7600000A287400000A7DC50400040000DE000016130C2B0000110C2A0000417C000002000000D4070000380200000C0A0000140000000000000002000000DB05000049040000240A0000140000000000000002000000E40A00005A0000003E0B0000140000000000000002000000B50A0000A1000000560B0000140000000000000000000000C4050000AD050000710B000063000000390000011B300300860000002E00001100731600000A0A06728B4201706F1700000A260672B14201706F1700000A260672014301706F1700000A2600066F1800000A036F1900000A03731A00000A0B00076F1B00000A727E170070166F1C00000A028C280000016F1D00000A00076F1E00000A17FE010CDE1A0714FE010D092D07076F1F00000A00DC260000DE0000160C2B0000082A0000011C000002003F002A6900100000000000002B004E790005010000011B300300F90000002E00001100731600000A0A06728B4201706F1700000A2606724F4301706F1700000A2606728B4301706F1700000A260672444401706F1700000A2606728E4401706F1700000A260672DE4401706F1700000A26066F1800000A046F1900000A04731A00000A0B00076F1B00000A72794501701E6F1C00000A038C170000026F1D00000A00076F1B00000A727E170070166F1C00000A028C280000016F1D00000A00076F1B00000A72954501701E6F1C00000A1A8C0F0000026F1D00000A00076F1B00000A7294AE00701E6F1C00000A1B8C0F0000026F1D00000A00076F1E00000A17FE0216FE010CDE100714FE010D092D07076F1F00000A00DC00082A000000011000000200620084E60010000000001B300800140500004B0000110004165205166A5517130E16731100000A131016731100000A131117130B000E0472C54501706F1200000A00731600000A0A0672F34501706F1700000A2606722F4601706F1700000A260672974601706F1700000A260672FF4601706F1700000A260672654701706F1700000A260672CB4701706F1700000A260672314801706F1700000A260672974801706F1700000A260672FD4801706F1700000A260672634901706F1700000A260672C94901706F1700000A2606722F4A01706F1700000A26066F7C00000A26067267CC00706F1700000A260672A74A01706F1700000A26067282D500706F1700000A260672F2D500706F1700000A26067262D600706F1700000A260672174B01706F1700000A260672834B01706F1700000A260672EF4B01706F1700000A2606725B4C01706F1700000A260672C74C01706F1700000A260672334D01706F1700000A260672874D01706F1700000A260672E74D01706F1700000A260672474E01706F1700000A260672894E01706F1700000A260672BB4E01706F1700000A2606728CD800706F1700000A2606720A0D01706F1700000A260672E74E01706F1700000A260672AC4F01706F1700000A260672715001706F1700000A260672DF5001706F1700000A260672725101706F1700000A260672055201706F1700000A260672985201706F1700000A2606722B5301706F1700000A260672BE5301706F1700000A260672515401706F1700000A260672DE5401706F1700000A260672425501706F1700000A2606722CD900706F1700000A26067258D900706F1700000A26066F1800000A0E046F1900000A0E04731A00000A13120011126F1B00000A725B0500701E6F1C00000A028C2D0000016F1D00000A0011126F1B00000A728DDB0070166F1C00000A038C280000016F1D00000A0011126F1B00000A72945501701E6F1C00000A168C110000026F1D00000A0011126F3300000A13130011136F3400000A131611162D0900171315DD360200001113166F3500000A0B1113176F3500000A0C1113186F6500000A1113196F6500000A11131A6F6500000A7315010006130411131B6F6500000A11131C6F6500000A11131D6F6500000A7315010006130511131E6F6500000A131011131F096F6500000A131111131F0A6F6D00000A0D00DE14111314FE01131611162D0811136F1F00000A00DC0000DE14111214FE01131611162D0811126F1F00000A00DC00110411052822010006130E110E2C21111016731100000A281500000A2C12111116731100000A282C00000A16FE012B011700131611162D050016130E001A070311041206120712080E0428DB000006131611162D0900161315DD39010000071104120A0E0428D0000006131611162D0900161315DD1E010000110A110428210100061309110E0811042826010006160E0428DC000006131611162D0900161315DDF2000000110E16FE01131611162D3C001A08120C120D120F0E0428F0000006131611162D0900161315DDC80000000508550702080E0428A8000006131611162D0900161315DDAC000000000708030E0428BB000006131611162D0900161315DD92000000020908731500000613141114166A071F3811096F1C01000616731100000A11046F1C010006110A6F1C0100066F17000006131611162D0600161315DE5511140E046F21000006131611162D0600161315DE4004175216130B171315DE35260000DE0000DE2700110B16FE01131611162D1900000E0472C54501706F1300000A0000DE05260000DE00000000DC001613152B000011152A417C000002000000C3020000890000004C0300001400000000000000020000005F02000005010000640300001400000000000000000000001E000000BD040000DB040000050000000100000100000000F004000011000000010500000500000001000001020000001E000000C5040000E304000027000000000000001B3005006F0500004C000011000E04281D010006510E05281D010006510E06281D01000651057BA604000416731100000A282200000A2D29057BA704000416731100000A282200000A2D16057BA804000416731100000A282200000A16FE012B011600130911092D090016130838050500000002130A110A17594504000000020000000200000014000000300000002B4F051304050C281D0100060D050B0413052B450528260100061304281D0100060C050D281D0100060B166A13052B2905282601000613040528260100060C281D0100060D281D0100060B166A13052B08161308DD8F040000731600000A0A0672F34501706F1700000A260672B25501706F1700000A260672105601706F1700000A2606726E5601706F1700000A260672CC5601706F1700000A2606722A5701706F1700000A260672885701706F1700000A260672E65701706F1700000A260672445801706F1700000A260672A25801706F1700000A26066F7C00000A26067267CC00706F1700000A260672045901706F1700000A260672B95901706F1700000A260672685A01706F1700000A260672175B01706F1700000A260672D45B01706F1700000A2606728B5C01706F1700000A260672425D01706F1700000A260672035E01706F1700000A260672BE5E01706F1700000A260672795F01706F1700000A260672126001706F1700000A260672A56001706F1700000A260672326101706F1700000A260672BF6101706F1700000A260672156201706F1700000A260672776201706F1700000A260672D96201706F1700000A260672336301706F1700000A260672996301706F1700000A260672FF6301706F1700000A260672596401706F1700000A260672BF6401706F1700000A2606728CD800706F1700000A2606720A0D01706F1700000A260672FF8500706F1700000A26066F7C00000A2606722CD900706F1700000A26067258D900706F1700000A26066F1800000A0E076F1900000A0E07731A00000A13060011066F1B00000A727E170070166F1C00000A038C280000016F1D00000A0011066F1B00000A72256501701B6F1C00000A11047BA60400048C080000016F1D00000A0011066F1B00000A723F6501701B6F1C00000A11047BA70400048C080000016F1D00000A0011066F1B00000A72536501701B6F1C00000A11047BA80400048C080000016F1D00000A0011066F1B00000A72676501701B6F1C00000A087BA60400048C080000016F1D00000A0011066F1B00000A72896501701B6F1C00000A087BA70400048C080000016F1D00000A0011066F1B00000A72A56501701B6F1C00000A087BA80400048C080000016F1D00000A0011066F1B00000A72C16501701B6F1C00000A097BA60400048C080000016F1D00000A0011066F1B00000A72E76501701B6F1C00000A097BA70400048C080000016F1D00000A0011066F1B00000A72076601701B6F1C00000A097BA80400048C080000016F1D00000A0011066F1B00000A7227660170166F1C00000A11058C280000016F1D00000A0011066F1B00000A72536601701B6F1C00000A077BA60400048C080000016F1D00000A0011066F1B00000A72796601701B6F1C00000A077BA70400048C080000016F1D00000A0011066F1B00000A72996601701B6F1C00000A077BA80400048C080000016F1D00000A0011066F3300000A13070011076F3400000A130911092D0900161308DD980000000E041107166F6500000A1107176F6500000A1107186F6500000A7315010006510E051107196F6500000A11071A6F6500000A11071B6F6500000A7315010006510E0611071C6F6500000A11071D6F6500000A11071E6F6500000A731501000651171308DE33110714FE01130911092D0811076F1F00000A00DC110614FE01130911092D0811066F1F00000A00DC260000DE00001613082B000011082A00414C000002000000BC0400007C00000038050000140000000000000002000000CE0200007E0200004C05000014000000000000000000000066000000FA04000060050000050000000100000113300700130000000D000011000203040516160E0428DD0000060A2B00062A001B300300720100002E0000110000731600000A0A0672B96601706F1700000A260672ED6601706F1700000A260516FE010D092D1C000672046801706F1700000A260672A76801706F1700000A26002B0E0006724A6901706F1700000A26000E0416FE010D092D0E000672896A01706F1700000A26000672F76A01706F1700000A26066F1800000A0E066F1900000A0E06731A00000A0B00076F1B00000A725B6B0170186F1C00000A028C430000016F1D00000A00076F1B00000A727F6B0170186F1C00000A0E058C430000016F1D00000A00076F1B00000A7275050070166F1C00000A038C280000016F1D00000A00076F1B00000A72B16B01701B6F1C00000A046F1C0100068C080000016F1D00000A00076F1B00000A72D56B01701B6F1C00000A046F1B0100068C080000016F1D00000A00076F1B00000A72ED6B01701B6F1C00000A046F1A0100068C080000016F1D00000A00076F1E00000A17FE010CDE1A0714FE010D092D07076F1F00000A00DC260000DE0000160C2B0000082A000041340000020000008A000000CB0000005501000010000000000000000000000001000000640100006501000005000000010000011B3004000C0400004D000011000E0616520E0516731100000A810800000112050E0728DF00000616FE01130911092D1600110516FE01130911092D090017130838CF03000000731600000A0D0972056C01706F1700000A2609720A0D01706F1700000A260972FF8500706F1700000A2600096F1800000A0E076F1900000A0E07731A00000A13060011066F1B00000A727E170070166F1C00000A028C280000016F1D00000A0011066F1B00000A7294AE00701E6F1C00000A1B8C0F0000026F1D00000A0011066F3100000AA52D000001130400DE14110614FE01130911092D0811066F1F00000A00DC00110416FE0116FE01130911092D0900171308DD1303000009166F6100000A000972CE6C01706F1700000A260972FE6C01706F1700000A260972326D01706F1700000A2609728A6D01706F1700000A260972066E01706F1700000A260972CE6C01706F1700000A260972FE6C01706F1700000A260972326D01706F1700000A2609720C6E01706F1700000A260972066E01706F1700000A260972CE6C01706F1700000A260972FE6C01706F1700000A260972326D01706F1700000A260972866E01706F1700000A260972066E01706F1700000A260972FA6E0170120472006F0170288D00000A6F8E00000A26096F1800000A0E076F1900000A0E07731A00000A13060011066F3300000A13070011076F3400000A130911092D0900161308DD080200001107166F6D00000A288F00000A0B11076F9000000A130911092D0900161308DDE401000011076F3400000A130911092D0900161308DDCE0100001107166F6D00000A288F00000A0A11076F9000000A130911092D0900161308DDAA01000011076F3400000A130911092D0900161308DD940100001107166F6D00000A288F00000A0C00DE14110714FE01130911092D0811076F1F00000A00DC0000DE14110614FE01130911092D0811066F1F00000A00DC00037BFA04000416731100000A281500000A16FE01130911092D1A000E0617520E0516731100000A8108000001171308DD220100000716731100000A282500000A0B0616731100000A282500000A0A0816731100000A282500000A0C0716731100000A282C00000A16FE01130911092D2A000E0617520E0525710800000116731100000A05282500000A07282A00000A282700000A8108000001000616731100000A282C00000A16FE01130911092D2A000E0617520E0525710800000116731100000A04282500000A06282A00000A282700000A8108000001000816731100000A282C00000A16FE01130911092D2B000E0617520E0525710800000116731100000A0E04282500000A08282A00000A282700000A8108000001000E050E06462D0816731100000A2B180E057108000001037BFA040004286F00000A1A289100000A008108000001171308DE0B260000DE00001613082B000011082A41640000020000007B0000004E000000C9000000140000000000000002000000E90100009C00000085020000140000000000000002000000DF010000BE0000009D0200001400000000000000000000006400000099030000FD03000005000000010000011B300300950000002E0000110002165400731600000A0A06166F6100000A000672066F01706F1700000A260672746F01706F1700000A260672CA6F01706F1700000A260672757001706F1700000A260672DD7001706F1700000A26066F1800000A036F1900000A03731A00000A0B0002076F3100000AA52D0000015400DE100714FE010D092D07076F1F00000A00DC00170CDE0A260000DE0000160C2B0000082A000000011C00000200620011730010000000000000040084880005010000011B300700280100004E00001100160A166A0B7E2F00000A0C00725B7101700E046F1900000A0E04731A00000A0D00091A6F9200000A00727E17007016739300000A13041104028C280000016F1D00000A001104176F5100000A00096F1B00000A11046F9400000A2672777101701E739300000A13051105186F5100000A00096F1B00000A11056F9400000A26096F1E00000A26096F1B00000A72777101706F9500000A6F6300000AA5550000020A00DE120914FE01130711072D07096F1F00000A00DC00728F7101700C0616FE01130711072D4C0006130811081759450200000002000000130000002B2220080020006A0B0804286800000A0C2B1320090020006A0B0804286800000A0C2B022B00196A0203076D08170E0428E10000062600171306DE0B260000DE00001613062B000011062A41340000020000002100000084000000A50000001200000000000000000000000C0000000D0100001901000005000000010000011B300300FB0100002E0000110000731600000A0A0672A57101706F1700000A260672FD7101706F1700000A260672557201706F1700000A260672AD7201706F1700000A260672057301706F1700000A2606725D7301706F1700000A260672B57301706F1700000A2606720D7401706F1700000A260672657401706F1700000A260672BD7401706F1700000A260672157501706F1700000A2606726D7501706F1700000A260672C57501706F1700000A2606721D7601706F1700000A260672757601706F1700000A260672CD7601706F1700000A260672257701706F1700000A2606727D7701706F1700000A2606727D7701706F1700000A260672BD7401706F1700000A26066F1800000A0E066F1900000A0E06731A00000A0B00076F1B00000A72D57701701E6F1C00000A028C280000016F1D00000A00076F1B00000A72EF770170166F1C00000A038C280000016F1D00000A00076F1B00000A72057801701F0C6F1C00000A046F1D00000A00076F1B00000A721F7801701E6F1C00000A058C4B0000016F1D00000A00076F1B00000A72377801701F0C6F1C00000A724F7801706F1D00000A00076F1B00000A72597801701F0C6F1C00000A0E046F1D00000A00076F1B00000A727F7801701E6F1C00000A0E058C2D0000016F1D00000A00076F1E00000A26170CDE1A0714FE010D092D07076F1F00000A00DC260000DE0000160C2B0000082A0041340000020000000D010000D1000000DE01000010000000000000000000000001000000ED010000EE01000005000000010000011B3003009B0200004F0000110000166A0B056F4600000A6F4800000A2C16036F1B01000616731100000A281500000A16FE012B011600130A110A2D0900171309DD5E020000731600000A0A0672DA6200706F1700000A260672957801706F1700000A260672E37801706F1700000A260672B06300706F1700000A26067245DC00706F1700000A2606729DDC00706F1700000A260672F7DC00706F1700000A26066F1800000A0E046F1900000A0E04731A00000A13040011046F1B00000A72A3640070166F1C00000A130511046F1B00000A722D7901701F096F1C00000A130611046F1B00000A72B96400701E6F1C00000A130711046F1B00000A727E170070166F1C00000A028C280000016F1D00000A0011046F1B00000A72566200701E6F1C00000A188C0A0000026F1D00000A00036F1B0100060C00056F4600000A6F4D00000A130B38E8000000110B6F4E00000A740B0000011308001108166F6B00000AA5280000010B1105078C280000016F1D00000A001108186F6B00000AA5080000010D0908285B00000A16FE01130A110A2D620011060908282600000A8C080000016F1D00000A000908281500000A16FE01130A110A2D12001107198C0A0000026F1D00000A00002B10001107188C0A0000026F1D00000A000011046F1E00000A17FE01130A110A2D0900161309DDB10000002B4D000809282600000A0C1106168C2D0000016F1D00000A001107198C0A0000026F1D00000A0011046F1E00000A17FE01130A110A2D0600161309DE740000110B6F4F00000A130A110A3A08FFFFFFDE1D110B7505000001130C110C14FE01130A110A2D08110C6F1F00000A00DC0000DE14110414FE01130A110A2D0811046F1F00000A00DC0007166AFE0216FE01130A110A2D0C0004070E0428E30000062600171309DE0B260000DE00001613092B000011092A00414C00000200000038010000FF000000370200001D0000000000000002000000A9000000AF01000058020000140000000000000000000000010000008B0200008C02000005000000010000011B300300860000003200001100007249790170046F1900000A04731A00000A0A00066F1B00000A72047A0170166F1C00000A038C280000016F1D00000A00066F1B00000A725B0500701E6F1C00000A028C280000016F1D00000A00066F1E00000A17FE010C082D0500160BDE2200DE100614FE010C082D07066F1F00000A00DC00170BDE0A260000DE0000160B2B0000072A0000011C00000200140050640010000000000000010078790005010000011B300800C803000050000011001713060516520E04166A550073130100060B731600000A0A0672187A01706F1700000A260672B77A01706F1700000A260672567B01706F1700000A260672F57B01706F1700000A260672947C01706F1700000A260672337D01706F1700000A260672D27D01706F1700000A260672717E01706F1700000A260672107F01706F1700000A260672AF7F01706F1700000A2606724E8001706F1700000A26066F1800000A0E056F1900000A0E05731A00000A13090011096F1B00000A725B0500701E6F1C00000A028C2D0000016F1D00000A0011096F1B00000A72ED8001701E6F1C00000A188C0F0000026F1D00000A0011096F1B00000A72098101701E6F1C00000A1B8C0F0000026F1D00000A0011096F1B00000A72646900701E6F1C00000A168C110000026F1D00000A0011096F3300000A130A00110A6F3400000A16FE01130C110C2D2A00110A166F3500000A0C110A176F3500000A0D110A186F3500000A1304110A196F6D00000A1305002B090017130BDD4B02000000DE14110A14FE01130C110C2D08110A6F1F00000A00DC0000DE14110914FE01130C110C2D0811096F1F00000A00DC000E0572C54501706F1200000A00080412010E0528D0000006130C110C2D090016130BDDF4010000161104042826010006170E0528DC000006130C110C2D090016130BDDD4010000047BA7040004047BA8040004282700000A16731100000A282C00000A16FE01130C110C3A0C0100000006166F6100000A000672DA6200706F1700000A260672338101706F1700000A2606729F8101706F1700000A260672F58101706F1700000A260672578201706F1700000A26066F1800000A0E056F1900000A0E05731A00000A13090011096F1B00000A72D56B01701F096F1C00000A046F1C0100068C080000016F1D00000A0011096F1B00000A72566200701E6F1C00000A188C0A0000026F1D00000A0011096F1B00000A721DE50070166F1C00000A098C280000016F1D00000A0011096F1B00000A727E170070166F1C00000A088C280000016F1D00000A0011096F1E00000A17FE01130C110C2D090016130BDDB900000000DE14110914FE01130C110C2D0811096F1F00000A00DC0000076F1C010006046F1C010006282600000A13070211051104731500000613081108166A081F38110716731100000A046F1C010006076F1C0100066F17000006130C110C2D060016130BDE5511080E056F21000006130C110C2D060016130BDE4016130605175217130BDE35260000DE0000DE2700110616FE01130C110C2D1900000E0572C54501706F1300000A0000DE05260000DE00000000DC0016130B2B0000110B2A419400000200000035010000470000007C010000140000000000000002000000B3000000E100000094010000140000000000000002000000730200009B0000000E0300001400000000000000000000000C000000830300008F030000050000000100000100000000A403000011000000B50300000500000001000001020000000C0000008B03000097030000270000000000000013300500100000000D00001100020304170528E60000060A2B00062A133002000B0000000D000011000E041754160A2B00062A001B3005006302000051000011000E0516731100000A81080000010E041754000316731100000A282800000A2D090418FE0116FE012B011600130511053A1C02000000731600000A0C02281D010006120012010E0628D100000616FE01130511053AF7010000000E05067BA90400048108000001041306110645030000000500000059000000A200000038C10000000316731100000A282200000A16FE01130511052D2200066F1A010006067BA9040004282600000A066F1A010006282D00000A1001002B1B00066F1A010006067BA9040004282600000A03282D00000A1001002B750316731100000A282200000A16FE01130511052D1700067BA904000415731100000A286F00000A1001002B1B00067BA904000403282D00000A15731100000A286F00000A1001002B2C03066F1A010006282C00000A16FE01130511052D0D000E041854161304DD1E0100002B08161304DD14010000731600000A0C0872A78201706F1700000A260418FE0116FE01130511052D1C000872138301706F1700000A2608727F8301706F1700000A26002B0E000872EB8301706F1700000A26000872578401706F1700000A26086F1800000A0E066F1900000A0E06731A00000A0D00096F1B00000A72CD0500701F096F1C00000A038C080000016F1D00000A00096F1B00000A727E170070166F1C00000A028C280000016F1D00000A000418FE0116FE01130511052D1F00096F1B00000A72B73F0170186F1C00000A058C430000016F1D00000A0000096F1E00000A17FE0116FE01130511052D0A000E041654171304DE2600DE120914FE01130511052D07096F1F00000A00DC00000000DE05260000DE00001613042B000011042A004134000002000000B5010000870000003C02000012000000000000000000000012000000420200005402000005000000010000011B300400330000000D00001100027E2F00000A51000272D2A6007072C38401700328F90000065100DE05260000DE00000250283800000A16FE010A2B00062A000110000000000800161E0005010000011B3004009C010000520000110000027B860400046F4600000A6F4D00000A0C3859010000086F4E00000A740B0000010B00027B8804000416731100000A287100000A16FE010D092D0600383B010000077258F000706F4A00000AA50900000217FE010D092D060038110100000772D58401706F6000000A2C060316FE012B0116000D092D0B00027B880400040A002B4A000772ED8401706F4A00000AA508000001077278F000706F4A00000AA5080000010772ED8401706F4A00000AA508000001282D00000A282600000A0A06027B88040004282D00000A0A000616731100000A287100000A16FE010D092D0600388A000000077278F00070077278F000706F4A00000AA50800000106282700000A8C080000016F4500000A000772EFFF00700772EFFF00706F4A00000AA50800000106282700000A8C080000016F4500000A0002257B8804000406282600000A7D8804000402257B8C04000406282700000A7D8C040004027B8D040004257BA804000406282700000A7DA804000400086F4F00000A0D093A9AFEFFFFDE1A0875050000011304110414FE010D092D0811046F1F00000A00DC002A411C000002000000130000006D010000800100001A00000000000000133003005F0000004900001100027B8804000416731100000A287100000A16FE010B072D03002B42027B880400040A027B8D040004257BA604000406282700000A7DA604000402257B8B04000406282700000A7D8B04000402257B8804000406282600000A7D880400042A001B3004005C010000520000110000037B860400046F4600000A6F4D00000A0C3819010000086F4E00000A740B0000010B00037B8704000416731100000A287100000A16FE010D092D060038FB000000077258F000706F4A00000AA50900000202FE010D092D060038D1000000077278F000706F4A00000AA508000001037B87040004282D00000A0A06037B8D0400047BA8040004282D00000A0A0616731100000A287100000A16FE010D092D0600388A000000077278F00070077278F000706F4A00000AA50800000106282600000A8C080000016F4500000A000772110001700772110001706F4A00000AA50800000106282700000A8C080000016F4500000A0003257B8A04000406282700000A7D8A04000403257B8704000406282600000A7D87040004037B8D040004257BA804000406282600000A7DA804000400086F4F00000A0D093ADAFEFFFFDE1A0875050000011304110414FE010D092D0811046F1F00000A00DC002A411C000002000000130000002D010000400100001A000000000000001B3004002D010000520000110000037B860400046F4600000A6F4D00000A0C38EA000000086F4E00000A740B0000010B00037B920400047BA804000416731100000A287100000A16FE010D092D060038C7000000077258F000706F4A00000AA50900000202FE010D092D0600389D000000077278F000706F4A00000AA508000001037B920400047BA8040004282D00000A0A0616731100000A287100000A16FE010D092D03002B66077278F00070077278F000706F4A00000AA50800000106282600000A8C080000016F4500000A000772110001700772110001706F4A00000AA50800000106282700000A8C080000016F4500000A00037B92040004257BA804000406282600000A7DA804000400086F4F00000A0D093A09FFFFFFDE1A0875050000011304110414FE010D092D0811046F1F00000A00DC002A0000000110000002001300FE11011A000000001B3004002D010000520000110000037B860400046F4600000A6F4D00000A0C38EA000000086F4E00000A740B0000010B00037B920400047BA704000416731100000A287100000A16FE010D092D060038C7000000077258F000706F4A00000AA50900000202FE010D092D0600389D000000077278F000706F4A00000AA508000001037B920400047BA7040004282D00000A0A0616731100000A287100000A16FE010D092D03002B66077278F00070077278F000706F4A00000AA50800000106282600000A8C080000016F4500000A000772110001700772110001706F4A00000AA50800000106282700000A8C080000016F4500000A00037B92040004257BA704000406282600000A7DA704000400086F4F00000A0D093A09FFFFFFDE1A0875050000011304110414FE010D092D0811046F1F00000A00DC002A0000000110000002001300FE11011A000000001B300400A2010000530000110000037B860400046F4600000A6F4D00000A0D385B010000096F4E00000A740B0000010C00037B8704000416731100000A287100000A16FE01130411042D0600383D010000087258F000706F4A00000AA50900000202FE01130411042D0600380F0100000872D58401706F6000000A16FE01130411042D060038F500000016731100000A087278F000706F4A00000AA5080000010872ED8401706F4A00000AA508000001282600000A282500000A0B037B8704000407282D00000A0A037B8D0400047BA804000406282D00000A0A0616731100000A287100000A16FE01130411042D0600388A000000087278F00070087278F000706F4A00000AA50800000106282600000A8C080000016F4500000A000872110001700872110001706F4A00000AA50800000106282700000A8C080000016F4500000A0003257B8A04000406282700000A7D8A04000403257B8704000406282600000A7D87040004037B8D040004257BA804000406282600000A7DA804000400096F4F00000A130411043A96FEFFFFDE1C0975050000011305110514FE01130411042D0811056F1F00000A00DC002A0000411C0000020000001300000071010000840100001C0000000000000013300300D00000004900001100027B8D0400047BA7040004027B87040004282D00000A0A0616731100000A282C00000A16FE010B072D3D00027B8D040004257BA704000406282600000A7DA704000402257B8904000406282700000A7D8904000402257B8704000406282600000A7D8704000400027B8D0400047BA6040004027B87040004282D00000A0A0616731100000A282C00000A16FE010B072D3D00027B8D040004257BA604000406282600000A7DA604000402257B8904000406282700000A7D8904000402257B8704000406282600000A7D87040004002A1B3003008A01000054000011000416540516541201FE150A0000011202FE150A0000010E04FE150F00000100731600000A0A0672D09A00706F1700000A260672078501706F1700000A260672E08501706F1700000A260672B98601706F1700000A260672928701706F1700000A260672CA8701706F1700000A260672048801706F1700000A2606723E8801706F1700000A260672A08801706F1700000A26066F1800000A0E056F1900000A0E05731A00000A0D00096F1B00000A7275050070166F1C00000A038C280000016F1D00000A00096F1B00000A72945501701E6F1C00000A168C110000026F1D00000A00096F1B00000A72B96400701E6F1C00000A028C110000026F1D00000A00096F3300000A13040011046F3400000A130611062D0600161305DE6B041104166F3600000A54051104176F3600000A541104186F9600000A0B1104196F9600000A0C0E04120207289700000A810F000001171305DE31110414FE01130611062D0811046F1F00000A00DC0914FE01130611062D07096F1F00000A00DC260000DE00001613052B000011052A0000414C000002000000070100004E00000055010000140000000000000002000000A7000000C2000000690100001200000000000000000000001F0000005C0100007B01000005000000010000011B30030069010000550000110016731100000A03282500000A100116731100000A04282500000A100272FC8801700A06722C890170286800000A0A06723D8A0170286800000A0A0672AD8A0170286800000A0A0672238B0170286800000A0A0E041F20FE0116FE010D092D10000672958B0170286800000A0A002B0E000672368C0170286800000A0A0000060E056F1900000A0E05731A00000A0B00076F1B00000A724CAB0070166F1C00000A028C280000016F1D00000A00076F1B00000A72712A00701F096F1C00000A038C080000016F1D00000A00076F1B00000A72A68C01701F096F1C00000A048C080000016F1D00000A00076F1B00000A72CA8C0170186F1C00000A058C430000016F1D00000A00076F1B00000A72945501701E6F1C00000A168C2D0000016F1D00000A00076F1B00000A72EE8C01701E6F1C00000A188C2D0000016F1D00000A00076F1E00000A17FE010CDE1A0714FE010D092D07076F1F00000A00DC260000DE0000160C2B0000082A000000011C000002008F00BD4C01100000000000007E00DE5C0105010000011B300300BE000000560000110000731600000A0A0672128D01706F1700000A2606727A8D01706F1700000A260672E28D01706F1700000A2606724A8E01706F1700000A26066F1800000A046F1900000A04731A00000A0B00076F1B00000A72EE8C01701E6F1C00000A188C110000026F1D00000A00076F1B00000A7275050070166F1C00000A028C280000016F1D00000A00076F1E00000A17FE010DDE290714FE01130411042D07076F1F00000A00DC0C0003086F7600000A6F1700000A2600DE0000160D2B0000092A0000011C000002004B00479200120000000000000100A3A40012390000011B3009002E01000057000011000E07731600000A510073000100060A061F207D5304000406047D55040004060E067D5404000406037D5904000406177D5A04000406027D5604000406166A7D57040004050E040E050612010E0828AF000006130511052D26000E0750076F0E0100066F1700000A260E075072B28E01706F1700000A26161304DDAB000000020E07500E0828F2000006130511052D17000E075072EC8E01706F1700000A26161304DD830000000420000010000E0828D9000006130511052D14000E0750723A8F01706F1700000A26161304DE5C050E050273150000060C08166A041F4B0316731100000A16731100000A037E2F00000A6F1900000626080E086F21000006130511052D0600161304DE1F171304DE1A0D000E0750096F7600000A6F1700000A2600DE00001613042B000011042A0000411C00000000000009000000070100001001000014000000390000011B300300200100000B00001100731600000A0A0672768F01706F1700000A260672AC8F01706F1700000A260672E48F01706F1700000A260672149001706F1700000A260672B34101706F1700000A260672E54101706F1700000A2600066F1800000A046F1900000A04731A00000A0B00076F1B00000A7275050070166F1C00000A028C280000016F1D00000A00076F3300000A0C00086F3400000A130411042D0800160DDD80000000037B5B04000408166F3600000A6A7D65040004037B5B04000408176F6500000A7D66040004037B5B04000408186F3600000A6A7D67040004037B5B04000408196F6500000A7D6804000400DE120814FE01130411042D07086F1F00000A00DC00170DDE1C0714FE01130411042D07076F1F00000A00DC260000DE0000160D2B0000092A012800000200880062EA001200000000020063009E0101120000000000004F00C4130105010000011B30030085030000580000110000731600000A0A160B03130511054502000000050000000500000038F000000006166F6100000A000672469001706F1700000A2606726E9001706F1700000A260672AA9001706F1700000A260672FA9001706F1700000A260672509101706F1700000A26066F1800000A056F1900000A05731A00000A0C00086F1B00000A727E170070166F1C00000A028C280000016F1D00000A00086F1B00000A72A69101701E6F1C00000A178C090000026F1D00000A00086F1B00000A72B29101701E6F1C00000A188C090000026F1D00000A00086F1B00000A72566200701E6F1C00000A188C0A0000026F1D00000A00086F3100000AA52D0000010B00DE120814FE01130611062D07086F1F00000A00DC002B0003130511054505000000F1000000F10000000500000005000000F1000000383D0200000416731100000A281500000A16FE01130611062D0900171304DD2F02000006166F6100000A0006728B4201706F1700000A260672BE9101706F1700000A2606726B9201706F1700000A2606721E9301706F1700000A260672AD9301706F1700000A2606721D9401706F1700000A26066F1800000A056F1900000A05731A00000A0D00096F1B00000A727E170070166F1C00000A028C280000016F1D00000A00096F1B00000A72CD0500701F096F1C00000A03182E0804286400000A2B0104008C080000016F1D00000A00096F1E00000A17FE011304DD730100000914FE01130611062D07096F1F00000A00DC06166F6100000A0006728B4201706F1700000A260672699401706F1700000A2606721D9401706F1700000A26066F1800000A056F1900000A05731A00000A0D00096F1B00000A727E170070166F1C00000A028C280000016F1D00000A00096F1B00000A72CD0500701F096F1C00000A260316FE0116FE01130611062D4E000716FE0116FE01130611062D1F00096F1B00000A72CD0500706F9500000A7E3200000A6F1D00000A00002B1E00096F1B00000A72CD0500706F9500000A048C080000016F1D00000A0000002B67071733090317FE0116FE012B011700130611062D2000096F1B00000A72CD0500706F9500000A048C080000016F1D00000A00002B32031AFE0116FE01130611062D1F00096F1B00000A72CD0500706F9500000A7E3200000A6F1D00000A00002B0600171304DE2F096F1E00000A17FE011304DE220914FE01130611062D07096F1F00000A00DC161304DE0B260000DE00001613042B000011042A00000041640000020000007800000084000000FC000000120000000000000002000000B5010000590000000E0200001200000000000000020000005F020000000100005F03000012000000000000000000000001000000750300007603000005000000010000011B300300C10000002F00001100722B65007072C79401700328F90000061201284C00000A260716FE0116FE01130411042D0800170D389000000000731600000A0A0672E79401706F1700000A2606723F9501706F1700000A260672879501706F1700000A26066F1800000A036F1900000A03731A00000A0C00086F1B00000A7275050070166F1C00000A028C280000016F1D00000A00086F1E00000A17FE01130411042D0500160DDE20170DDE1C0814FE01130411042D07086F1F00000A00DC260000DE0000160D2B0000092A000000011C000002006C0036A200120000000000002E0086B40005010000011B300300E90000002E0000110000731600000A0A0672C99501706F1700000A260672FD9501706F1700000A260672379601706F1700000A260672639601706F1700000A260672939601706F1700000A260672C59601706F1700000A260672059701706F1700000A260672359701706F1700000A26066F1800000A046F1900000A04731A00000A0B00076F1B00000A72CE9701701E6F1C00000A028C2D0000016F1D00000A00076F1B00000A72E6970170166F1C00000A038C280000016F1D00000A00076F1E00000A17FE010D092D0500160CDE1E170CDE1A0714FE010D092D07076F1F00000A00DC260000DE0000160C2B0000082A000000011C000002007B0051CC00100000000000000100DBDC0005010000011B300400E205000059000011000E051752031305731600000A1306731600000A1307160A160B160C160D731600000A13081108166F6100000A00110872CE6C01706F1700000A26110872FE6C01706F1700000A26110872049801706F1700000A26110872809801706F1700000A26110872FC9801706F1700000A26110872CE6C01706F1700000A26110872FE6C01706F1700000A26110872049801706F1700000A26110872169901706F1700000A26110872FC9801706F1700000A26110872CE6C01706F1700000A26110872FE6C01706F1700000A26110872049801706F1700000A26110872A59901706F1700000A26110872FC9801706F1700000A26110872CE6C01706F1700000A26110872FE6C01706F1700000A26110872049801706F1700000A261108720F9A01706F1700000A26110872066E01706F1700000A260011086F1800000A0E066F1900000A0E06731A00000A13090011096F3300000A130A00110A6F3400000A16FE01130C110C2D1B00110A166F6D00000A1200284C00000A130C110C2D0400160A0000110A6F3400000A16FE01130C110C2D1B00110A166F6D00000A1201284C00000A130C110C2D0400160B0000110A6F3400000A16FE01130C110C2D1B00110A166F6D00000A1202289800000A130C110C2D0400160C0000110A6F3400000A16FE01130C110C2D1B00110A166F6D00000A1203289800000A130C110C2D0400160D000000DE14110A14FE01130C110C2D08110A6F1F00000A00DC0000DE14110914FE01130C110C2D0811096F1F00000A00DC000E054616FE01130C110C2D4D000616311106289900000A03282200000A16FE012B011700130C110C2D2E000E051652110672899A01700F01282300000A6F9A00000A26110672B39A01701200287200000A6F9A00000A2600000E054616FE01130C110C3ACF000000000F02287B00000A230000000000000000FE0216FE01130C110C3AAF000000000716312307289900000A030F02287B00000A6C289B00000A282A00000A282200000A16FE012B011700130C110C2D7E000E051652110672899A01700F01282300000A6F9A00000A26110672F19A01700F02287B00000A130D120D721D9B0170289C00000A6F9A00000A26110672279B0170030F02287B00000A6C289B00000A282A00000A130E120E721D9B0170289D00000A6F9A00000A26110672599B01701201287200000A6F9A00000A260000000E054616FE01130C110C2D66000817330F057B9C040004166AFE0116FE012B011700130C110C2D4900027BF104000417FE0116FE01130C110C2D0400002B31000E051652110672AD9B0170057C9C040004282000000A6F9A00000A26110672CF9B0170168D010000016F9E00000A260000000E054616FE01130C110C2D4A000917330B057B9904000416FE012B011700130C110C2D31000E051652110672F99B0170057C99040004287500000A6F9A00000A26110672259C0170168D010000016F9E00000A2600001713040E0546130C110C2D760016731100000A13051813041107725F9C01706F9F00000A26110772859C0170057B960400048C280000016F9A00000A26110772A39C0170057B970400048C2D0000016F9A00000A26110772C79C01706F9F00000A2611061611076F1800000A6FA000000A260E0411066F1800000A6F0C0100060000731600000A1308110872E39C01706F1700000A26110872199D01706F1700000A26110872249E01706F1700000A26110872079F01706F1700000A26110872EA9F01706F1700000A2611086F1800000A0E066F1900000A0E06731A00000A13090011096F1B00000A725AA001701E6F1C00000A168C2D0000016F1D00000A0011096F1B00000A7276A001701E6F1C00000A11048C2D0000016F1D00000A0011096F1B00000A7294A001701F096F1C00000A038C080000016F1D00000A0011096F1B00000A72B6A001701F096F1C00000A11058C080000016F1D00000A0011096F1B00000A7275050070166F1C00000A057B950400048C280000016F1D00000A0011096F1E00000A17FE01130BDE1F110914FE01130C110C2D0811096F1F00000A00DC260000DE000016130B2B0000110B2A0000416400000200000054010000B0000000040200001400000000000000020000004A010000D20000001C02000014000000000000000200000011050000AE000000BF05000014000000000000000000000032010000A1040000D305000005000000010000011B300400D40000005A0000110072010000700C00731600000A0A0672CE6C01706F1700000A260672FE6C01706F1700000A260672D6A001706F1700000A26067220A101706F1700000A26066F1800000A046F1900000A04731A00000A0D00096F1B00000A726EA101701F0C1F326F2E00000A026F1D00000A00096F1B00000A727EA101701F0C1F326F2E00000A036F1D00000A00096F3100000A0B072C0A077E3200000AFE012B011700130511052D09000774290000010C0000DE120914FE01130511052D07096F1F00000A00DC0000DE05260000DE00000813042B0011042A011C0000020051005FB000120000000000000700BFC60005010000018602167D4604000402281D0100067D4704000402167D4804000402283700000A002A000013300200220000000D0000110002037D5104000404283800000A0A062D0F00027B52040004046F1700000A26002A000013300300250000000D0000110003283800000A0A062D1900027B520400047292A1017003286800000A6F1700000A26002A260002167D510400042A0013300100110000002200001100027B520400046F1800000A0A2B00062A00000003300200610000000000000002166A7D4904000402167D4A04000402281D0100067D4B04000402281D0100067D4C04000402281D0100067D4D04000402281D0100067D4E0400040216731100000A7D4F04000402177D5104000402731600000A7D5204000402283700000A002A00000003300200AF00000000000000021C7D530400040272010000707D5404000402156A7D5504000402156A7D5604000402166A7D5704000402167D580400040216731100000A7D5904000402167D5A0400040273010100067D5B04000402167D5C04000402166A7D5D0400040273090100067D5E0400040272010000707D5F04000402167D6004000402281D0100067D6104000402281D0100067D6204000402281D0100067D6304000402281D0100067D6404000402283700000A002A0003300200540000000000000002166A7D650400040216731100000A7D6604000402166A7D670400040216731100000A7D680400040216731100000A7D690400040216731100000A7D6A0400040216731100000A7D6B04000402283700000A002A133002001C0000002000001100027B6C040004027B710400047B74040004282700000A0A2B00062A133001000C0000002000001100027B6E0400040A2B00062A133001000C0000002000001100027B6D0400040A2B00062A13300200220000002000001100022802010006022803010006282700000A022804010006282700000A0A2B00062A0000133001000C0000002000001100027B6F0400040A2B00062A133001000C0000002000001100027B700400040A2B00062A13300200170000002000001100022806010006022807010006282700000A0A2B00062A4E02730A0100067D7104000402283700000A002A1E02283700000A2A0013300200220000000D0000110002037D8204000404283800000A0A062D0F00027B83040004046F1700000A26002A000013300300250000000D0000110003283800000A0A062D1900027B830400047292A1017003286800000A6F1700000A26002A260002167D820400042A0013300100110000002200001100027B830400046F1800000A0A2B00062A9602281D0100067D8104000402177D8204000402731600000A7D8304000402283700000A002A0003300200A50000000000000002166A7D8404000402166A7D8504000402147D860400040216731100000A7D870400040216731100000A7D880400040216731100000A7D890400040216731100000A7D8A0400040216731100000A7D8B0400040216731100000A7D8C04000402281D0100067D8D04000402281D0100067D8E04000402281D0100067D8F04000402167D9004000402281D0100067D9104000402281D0100067D9204000402283700000A002A00000003300200B700000000000000021C7D930400040272010000707D9404000402166A7D9504000402166A7D9604000402167D970400040272010000707D9804000402177D9904000402281D0100067D9A04000402731600000A7D9B04000402166A7D9C04000402281D0100067D9D04000402166A7D9E04000402281D0100067D9F04000402167DA004000402166A7DA10400040273090100067DA20400040272010000707DA304000402167DA40400040216731100000A7DA504000402283700000A002A0013300400530000005B0000110072A6A101701A8D010000010B071602281C0100060C1202282300000AA20717027CA6040004282300000AA20718027CA7040004282300000AA20719027CA8040004282300000AA207282400000A0A2B00062A6202283700000A000002167DAA04000402167DAD040004002A033002008E0000000000000002283700000A000002037BA60400047DA604000402037BA70400047DA704000402037BA80400047DA804000402037BA90400047DA904000402037BAA0400047DAA04000402037BAB0400047DAB04000402037BAC0400047DAC04000402037BAD0400047DAD04000402037BAE0400047DAE04000402037BAF0400047DAF04000402037BB00400047DB0040004002A0000033002006B0000000000000002283700000A000002037DA604000402047DA704000402057DA80400040216731100000A7DB00400040216731100000A7DA904000402167DAA04000402167DAB04000402167DAC04000402167DAD0400040216731100000A7DAE0400040216731100000A7DAF040004002A560203040528150100060000020E047DB0040004002A920203040528150100060000020E047DA9040004020E057DAB04000402177DAC040004002A0000033006004700000000000000020304050E040E052817010006000002257BA80400040E06282700000A7DA8040004020E067DAE04000402257BA70400040E07282700000A7DA7040004020E077DAF040004002AA2020304050E040E050E060E072818010006000002257BB00400040E08282700000A7DB0040004002A13300200170000002000001100027BA6040004027BA7040004282700000A0A2B00062A00133001000C0000002000001100027BA80400040A2B00062A13300200220000002000001100027BA6040004027BA7040004282700000A027BA8040004282700000A0A2B00062A0000133001000B0000005C0000110073130100060A2B00062A0013300300100100004700001100027BAA0400042D16027BA904000416731100000A281500000A16FE012B0116000C082D0800020B38E100000002281A010006027BA9040004282200000A16FE010C082D20000216731100000A7DA60400040216731100000A7DA7040004020B38A9000000027BA90400040A027BA604000406285B00000A16FE010C082D240002257BA604000406282600000A7DA604000416731100000A0A02177DAA040004002B1B0006027BA6040004282600000A0A0216731100000A7DA6040004000616731100000A282C00000A2C11027BA704000406285B00000A16FE012B0117000C082D1B0002257BA704000406282600000A7DA704000402177DAA040004000216731100000A7DAE040004020B2B00072A13300200EA000000470000110002177DAD0400040216731100000A7DA804000402281A010006027BA9040004282200000A2D16027BA904000416731100000A281500000A16FE012B0116000C082D20000216731100000A7DA60400040216731100000A7DA7040004020B3885000000027BA90400040A027BA604000406285B00000A16FE010C082D1E0002067DA604000416731100000A0A0216731100000A7DA7040004002B0F0006027BA6040004282600000A0A000616731100000A282C00000A2C11027BA704000406285B00000A16FE012B0117000C082D090002067DA7040004000216731100000A7DAE040004020B2B00072A0000133004003E0000005C00001100027BA6040004037BA6040004282700000A027BA7040004037BA7040004282700000A027BA8040004037BA8040004282700000A73150100060A2B00062A0000133005004F0000005C00001100027BA6040004037BA6040004282600000A027BA7040004037BA7040004282600000A027BA8040004037BA8040004282600000A027BB0040004037BB0040004282600000A73160100060A2B00062A0013300200410000000D00001100027BA6040004037BA6040004281500000A2C26027BA7040004037BA7040004281500000A2C13027BA8040004037BA8040004281500000A2B0116000A2B00062A00000013300200100000000D000011000203282201000616FE010A2B00062A13300200120000000D000011000203745100000228220100060A2B00062A0000133001000C00000023000011000228A100000A0A2B00062A133002000D0000005C00001100021628270100060A2B00062A000000133005007B00000048000011000316FE010B072D3B00027BA6040004286400000A027BA7040004286400000A027BA8040004286400000A027BA9040004286400000A027BAB04000473170100060A2B3500027BA6040004286400000A027BA7040004286400000A027BA8040004286400000A027BB0040004286400000A73160100060A2B00062A00133002006D0000005D0000110073130100060A06027BA60400047DA604000406027BA70400047DA704000406027BA80400047DA804000406027BA90400047DA904000406027BAA0400047DAA04000406027BAB0400047DAB04000406027BAC0400047DAC04000406027BAD0400047DAD040004060B2B00072A0000001330030026000000220000110072E6A10170027BB10400046F1800000A027CB2040004282300000A288500000A0A2B00062A8602283700000A000002281D0100067DB10400040216731100000A7DB2040004002A9E02283700000A000002037BB104000473140100067DB104000402037BB20400047DB2040004002A7602283700000A0000020373140100067DB104000402047DB2040004002A0000133001000B0000005E00001100732A0100060A2B00062A00133003002D0000005E00001100027BB1040004037BB10400042820010006027BB2040004037BB2040004282700000A732C0100060A2B00062A000000133003002D0000005E00001100027BB1040004037BB10400042821010006027BB2040004037BB2040004282600000A732C0100060A2B00062A000000133002002E0000000D00001100027BB1040004037BB104000428220100062C13027BB2040004037BB2040004281500000A2B0116000A2B00062A000013300200100000000D000011000203283001000616FE010A2B00062A13300200120000000D000011000203745200000228300100060A2B00062A0000133001000C00000023000011000228A100000A0A2B00062A03300200A90000000000000002166A7DB304000402177DB404000402167DB504000402167DB604000402167DB70400040272010000707DB804000402166A7DBA04000402166A7DBB04000402167DBC04000402177DBD04000402177DBE0400040216731100000A7DBF0400040216731100000A7DC004000402166A7DC104000402281D0100067DC204000402281D0100067DC30400040216731100000A7DC40400040272010000707DC504000402283700000A002A9A02147DE404000402147DE504000402147DE604000402283700000A000002283E01000600002A13300200220000005F00001100027BE50400046F4600000A166F4900000A166F6B00000AA55C0000020A2B00062AB200027BE50400046F4600000A166F4900000A16038C5C0000026F7D00000A00027BE40400046F4B00000A002A001B300400300100006000001100027BE50400046F4600000A166F4900000A166F6B00000AA55C00000213041104450300000002000000090000000D0000002B0F030D38F3000000040A2B35050A2B317200A20170027BE50400046F4600000A166F4900000A166F6B00000AA52D0000018C2D00000128A200000A73A300000A7A731600000A0B00027BE40400046F4600000A6F4D00000A13052B4711056F4E00000A740B0000010C00076FA400000A16FE0216FE01130611062D0E000772E6A400706F9F00000A26000708166F6B00000A74290000016FA500000A6F9F00000A260011056F4F00000A130611062DACDE1D110575050000011307110714FE01130611062D0811076F1F00000A00DC00067236A20170076F1800000A7E2F00000A285A00000A2D040E042B06076F1800000A00284300000A0D2B00092A0110000002008D0058E5001D0000000013300300630000006100001100027BE50400046F4600000A166F4900000A166F6B00000AA55C00000216FE0116FE010B072D03002B380203283F01000614FE010B072D03002B27027BE40400046F4400000A0A0616036F7D00000A00027BE40400046F4600000A066F4700000A002A00133002002700000061000011000203283F0100060A0614FE010B072D1500066FA600000A00027BE40400046FA700000A00002A0013300200590000006200001100027BE50400046F4600000A166F4900000A166F6B00000AA55C0000020B0745030000000200000006000000160000002B21170A2B210203283F01000614FE0116FE010A2B110203283F01000614FE010A2B04160A2B00062A000000133003003E0000003D00001100027BE50400046F4600000A166F4900000A166F6B00000AA55C00000216FE0116FE010B072D0500140A2B1002027BE60400041728400100060A2B00062A0000133004003F0000000D000011000314FE0116FE010A062D0B0002162837010006002B27027BE50400046F4B00000A00027BE40400046F4B00000A0002027BE604000403182841010006002A0013300500F70000006300001100160B02723CA2017073A800000A7DE6040004027256A2017073A900000A7DE5040004027BE50400046F3A00000A7268A20170078C5C00000228AA00000A6F3C00000A26027BE50400046F4400000A0A0616168C5C0000026F7D00000A00027BE50400046F4600000A066F4700000A00027272A2017073A900000A7DE4040004027BE40400046F3A00000A7284A201707205160070283B00000A6F3C00000A26027BE4040004178D370000010C0816027BE40400046F3A00000A166FAB00000AA2086F4200000A00027BE60400046FAC00000A027BE50400046FAD00000A00027BE60400046FAC00000A027BE40400046FAD00000A002A001B30030070000000640000110000027BE40400046F4600000A6F4D00000A0C2B2D086F4E00000A740B0000010A000306166F6B00000A7429000001196FAE00000A16FE010D092D0500060BDE2C00086F4F00000A0D092DC9DE1A0875050000011304110414FE010D092D0811046F1F00000A00DC00140B2B0000072A01100000020013003B4E001A000000001B30030038000000650000110072010000700A73AF00000A0B000307046FB000000A00076F1800000A0A00DE100714FE010D092D07076F1F00000A00DC00060C2B00082A0110000002000D0014210010000000001B3003002700000066000011000473B100000A0A000306056FB200000A2600DE100614FE010B072D07066F1F00000A00DC002A0001100000020008000D150010000000001B300300D1000000670000110000731600000A0B07166F6100000A0007728EA201706F1700000A260772B6A201706F1700000A260772E8A201706F1700000A26077266A301706F1700000A26077278A301706F1700000A26076F1800000A036F1900000A03731A00000A0C00086F1B00000A72CE9701701E6F1C00000A028C2D0000016F1D00000A00086F3100000A0A062C0A067E3200000AFE012B011700130511052D100006A52D0000010D0916FE021304DE2400DE120814FE01130511052D07086F1F00000A00DC0000DE05260000DE00001613042B000011042A000000011C000002005F004DAC00120000000000000100C1C20005010000011B3005009F01000068000011000573500100065100731600000A0A06166F6100000A000672BEA301706F1700000A260672FE6C01706F1700000A26067236A401706F1700000A260672DDA401706F1700000A26067211A501706F1700000A26067259A501706F1700000A260672BDA501706F1700000A26067268A601706F1700000A26067213A701706F1700000A26066F1800000A0E046F1900000A0E04731A00000A0C00086F1B00000A72431200701F0C6F1C00000A026F1D00000A00086F1B00000A727FA701701F0C6F1C00000A032C03032B057201000070006F1D00000A00086F1B00000A729DA701701E6F1C00000A0314FE018C430000016F1D00000A00086F1B00000A72C5A701701E6F1C00000A048C2D0000016F1D00000A00086F1B00000A72E5A701701E6F1C00000A168C2D0000016F1D00000A00086F3100000A0B072C0A077E3200000AFE012B011700130511052D150007A52D0000010D09050E0428450100061304DE3A0314FE01130511052D0E00160203040E0428440100062600171304DE1D0814FE01130511052D07086F1F00000A00DC260000DE00001613042B000011042A00413400000200000098000000E60000007E01000012000000000000000000000008000000880100009001000005000000010000011B300300CD0100002E0000110000731600000A0A06166F6100000A0006720FA801706F1700000A2606722FA801706F1700000A26067237A801706F1700000A26067251A801706F1700000A2606728BA801706F1700000A260672DBA801706F1700000A2606722FA901706F1700000A2606728BA901706F1700000A26067293A901706F1700000A260672EFA901706F1700000A26067251AA01706F1700000A260672BBAA01706F1700000A2606722BAB01706F1700000A26067285AB01706F1700000A260672E3AB01706F1700000A26067249AC01706F1700000A260672B5AC01706F1700000A260672C3AC01706F1700000A26067201AD01706F1700000A26067263AD01706F1700000A260672B7AD01706F1700000A2606720FAE01706F1700000A26066F1800000A0E046F1900000A0E04731A00000A0B00076F1B00000A726FAE01701E6F1C00000A028C2D0000016F1D00000A00076F1B00000A72431200701F0C6F1C00000A036F1D00000A00076F1B00000A727FA701701F0C6F1C00000A046F1D00000A00076F1B00000A72C5A701701E6F1C00000A058C2D0000016F1D00000A00076F1E00000A17FE010D092D0500160CDE1E170CDE1A0714FE010D092D07076F1F00000A00DC260000DE0000160C2B0000082A00000041340000020000002D01000083000000B001000010000000000000000000000001000000BF010000C0010000050000000100000113300500180000000D000011000272010000707201000070030428480100060A2B00062A13300500140000000D0000110016027201000070030428480100060A2B00062A13300500140000000D0000110016720100007002030428480100060A2B00062A1B3004005705000069000011007202A700707281AE01700E0428F90000061203284C00000A130711072D0400160D0005735001000651731600000A0A000672ADAE01706F1700000A26067236AF01706F1700000A260672BFAF01706F1700000A26067248B001706F1700000A260672D1B001706F1700000A2606725AB101706F1700000A260672E3B101706F1700000A2606726CB201706F1700000A260672F5B201706F1700000A2606727EB301706F1700000A26067207B401706F1700000A26067290B401706F1700000A26067219B501706F1700000A260672A2B501706F1700000A2606722BB601706F1700000A260672B4B601706F1700000A2606723BB701706F1700000A260672C4B701706F1700000A2606724DB801706F1700000A260672D6B801706F1700000A2606725FB901706F1700000A260672E8B901706F1700000A26067271BA01706F1700000A260672FABA01706F1700000A26067281BB01706F1700000A26067208BC01706F1700000A2606728FBC01706F1700000A26067216BD01706F1700000A2606729FBD01706F1700000A26067228BE01706F1700000A260672B1BE01706F1700000A2606723ABF01706F1700000A260672C3BF01706F1700000A2606724CC001706F1700000A260672D5C001706F1700000A2603283800000A130711072D100006725EC101706F1700000A26002B2C0004283800000A130711072D10000672A6C101706F1700000A26002B0E00067208C201706F1700000A260000066F1800000A0E046F1900000A0E04731A00000A13040011046F1B00000A725AC201701E6F1C00000A1A8C2E0000026F1D00000A0003283800000A130711072D200011046F1B00000A72050C00701F0C1F326F2E00000A036F1D00000A00002B4E0004283800000A130711072D200011046F1B00000A7276C201701F0C1F326F2E00000A046F1D00000A00002B200011046F1B00000A725B0500701E6F1C00000A028C2D0000016F1D00000A00000011046F3300000A13050011056F3400000A130711072D0900161306DD6702000005501105166F3600000A7DEF0400041105176F6E00000A130711072D110005501105176F3600000A7DF00400040005501105186FB300000A7DF104000405501105196F6D00000A7DF2040004055011051A6F6D00000A7DF3040004055011051B6F7800000A7DF4040004055011051C6F3600000A7DF5040004055011051D6F6E00000A2D0A11051D6F3500000A2B02166A007DF7040004055011051E6F6E00000A2D0A11051E6F3500000A2B02166A007DF8040004055016731100000A11051F096F6500000A282500000A7DFA040004055011051F0A6F7800000A7DFB040004055011051F0B6F7800000A7DFC04000411051F0C6F3600000A0B11051F0D6F3600000A0C055009085F0708665F607DF9040004055011051F0E6F3600000A7DF6040004055011051F0F6F6E00000A2D0B11051F0F6F6D00000A2B057E2F00000A007D0B050004055011051F106F6E00000A2D0B11051F106F6D00000A2B057E2F00000A007D0C050004055011051F116F6D00000A7D0D050004055011051F126F6D00000A7D0E050004055011051F136F3600000A7D0F050004055011051F146F6E00000A2D0B11051F146F6D00000A2B057E2F00000A007D1005000405507BF604000413081108450200000002000000020000002B022B0A0550167DF60400042B0005507BF604000417FE0116FE01130711072D320005507BF10400041BFE0116FE01130711072D1D000550186F4F01000616FE01130711072D0A000550187DF604000400000000DE14110514FE01130711072D0811056F1F00000A00DC0000DE14110414FE01130711072D0811046F1F00000A00DC00171306DE0B260000DE00001613062B000011062A00414C000002000000D5020000410200001605000014000000000000000200000033020000FB0200002E05000014000000000000000000000030000000180500004805000005000000010000011B300300740100006A000011000313047E2F00000A0B7E2F00000A0C7E2F00000A0D047E2F00000A51731600000A0A0672A0C201706F1700000A260672DCC201706F1700000A26067218C301706F1700000A26067254C301706F1700000A26067290C301706F1700000A2600066F1800000A056F1900000A05731A00000A13050011056F1B00000A72E6C30170166F1C00000A028C2D0000016F1D00000A0011056F3300000A1306002B53001106166F6E00000A2D0A1106166F6D00000A2B057201000070000B1106176F6E00000A2D0A1106176F6D00000A2B057201000070000C1106186F6E00000A2D0A1106186F6D00000A2B057201000070000D0011066F3400000A130811082DA000DE14110614FE01130811082D0811066F1F00000A00DC0000DE14110514FE01130811082D0811056F1F00000A00DC00110472FEC30170076FB400000A13041104720AC40170086FB400000A13041104721EC40170096FB400000A130404110451171307DE0B260000DE00001613072B000011072A414C0000020000009C000000660000000201000014000000000000000200000074000000A60000001A0100001400000000000000000000005F000000060100006501000005000000010000011B3003002E0100006B00001100731600000A0A067230C401706F1700000A26067286C401706F1700000A260672DCC401706F1700000A26067232C501706F1700000A26066F1800000A046F1900000A04731A00000A13040011046F1B00000A725B0500701E6F1C00000A038C2D0000016F1D00000A0011046F3300000A13050011056F3400000A130711072D0900161306DDA0000000110511057288C501706F7700000A6F3600000A0B1105110572A2C501706F7700000A6F3600000A0C00DE14110514FE01130711072D0811056F1F00000A00DC0000DE14110414FE01130711072D0811046F1F00000A00DC000802284B01000616FE01130711072D2D007202A700707281AE01700428F90000061203284C00000A130711072D0400160D000902284B01000613062B0B0702284B01000613062B000011062A0000011C00000200730042B500140000000002004B0082CD001400000000133002000C0000000D0000110002035F03FE010A2B00062A13300300D80200006C00001100731600000A0A0672DEC501706F1700000A26067200C601706F1700000A26067224C601706F1700000A2606725CC601706F1700000A260672BEC601706F1700000A26067224C701706F1700000A26067236C701706F1700000A26067272C701706F1700000A260672B4C701706F1700000A260672FCC701706F1700000A26067246C801706F1700000A2606728AC801706F1700000A260672CEC801706F1700000A26067214C901706F1700000A26067252C901706F1700000A26067292C901706F1700000A260672DAC901706F1700000A26067224CA01706F1700000A26067274CA01706F1700000A260672C6CA01706F1700000A26067210CB01706F1700000A2606725CCB01706F1700000A260672A2CB01706F1700000A260672C6CB01706F1700000A260672E6CB01706F1700000A26067224CC01706F1700000A26067266CC01706F1700000A260672AACC01706F1700000A260672ECCC01706F1700000A26067236CD01706F1700000A26067282CD01706F1700000A260672C6CD01706F1700000A2606720CCE01706F1700000A26067224CC01706F1700000A260672ECCC01706F1700000A26067236CD01706F1700000A26067282CD01706F1700000A260672C6CD01706F1700000A2606725ACE01706F1700000A260672A2CB01706F1700000A260672045E00706F1700000A26067292CE01706F1700000A260672B2CE01706F1700000A260672E4CE01706F1700000A26067250CF01706F1700000A260672BACF01706F1700000A26067261D001706F1700000A2606720AD101706F1700000A260672ABD101706F1700000A2606724ED201706F1700000A260672B8D201706F1700000A2606725FD301706F1700000A26067208D401706F1700000A260672A9D401706F1700000A2606724CD501706F1700000A260672F7D501706F1700000A26067257D601706F1700000A260672BDD601706F1700000A26066F1800000A026F1900000A02731A00000A0B070C2B00082A1B300300890100006D00001100160A731600000A0B077227D701706F1700000A260772EAD701706F1700000A260772ADD801706F1700000A26077270D901706F1700000A26077233DA01706F1700000A260772F6DA01706F1700000A260772B9DB01706F1700000A2607727CDC01706F1700000A2607723FDD01706F1700000A26077202DE01706F1700000A260772C5DE01706F1700000A26077288DF01706F1700000A2607724BE001706F1700000A2607720EE101706F1700000A260772D1E101706F1700000A26077294E201706F1700000A26077257E301706F1700000A2607721AE401706F1700000A260772DDE401706F1700000A2600076F1800000A036F1900000A03731A00000A0C00086F1B00000A72A0E501701E6F1C00000A198C220000026F1D00000A00086F1B00000A725B0500701E6F1C00000A028C2D0000016F1D00000A00086F1B00000A72CEE501701E6F1C00000A168C220000026F1D00000A00086F1E00000A26170A00DE120814FE01130411042D07086F1F00000A00DC0000DE072600160A00DE0000060D2B00092A000000011C00000200010164650112000000000000ED008E7B0107390000011B3003006300000032000011000415540072E8E50170036F1900000A03731A00000A0A00066F1B00000A725B0500701E6F1C00000A028C2D0000016F1D00000A0004066F3100000AA54C00000154170BDE1A0614FE010C082D07066F1F00000A00DC260000DE0000160B2B0000072A00011C0000020017002F4600100000000000000400525600050100000113300200120000000D00001100027BF904000403284B0100060A2B00062A000003300200790000000000000002167DEF04000402167DF004000402157DF10400040272010000707DF20400040272010000707DF304000402177DF404000402187DF504000402167DF604000402156A7DF704000402166A7DF804000402167DF90400040216731100000A7DFA04000402177DFB04000402167DFC04000402283700000A002A000000133003002D0000006E00001100022C0B026FB600000A16FE022B0116000D092D0872010000700C2B0E1200021628550100060B060C2B00082A00000013300300390000006F00001100022C0B026FB600000A16FE022B0116000D092D0872010000700C2B1A021200120128540100060D092D0872010000700C2B04060C2B00082A1E02283700000A2A0000001B300400A001000070000011001E8D54000001130A0372010000705104165400026FB600000A1F0DFE0416FE01131311132D0900161312DD6C010000737A0100060A061F406FB700000A00067E120500047E130500046FB800000A130D156A1314121428B900000A130C02110C28BA00000A16FE0216FE01131311132D0900161312DD210100000228BB00000A1307110728BC00000A130E110E73BD00000A130F110F110D1673BE00000A13101110110A161E6FBF00000A26110A1628C000000A1308120828B900000A1000021F141F306F8C00000A10000216176FC100000A0B02171A6FC100000A0C021B1F0D6FC100000A0D021F12186FC100000A130407727FE60170285A00000A16FE01131311132D0900161312DD8C000000161305091104286800000A28BB00000A1309110928BC00000A130B1613112B12001105110B111191581305001111175813111111110B8E69FE04131311132DE0110520F52600005E13057283E6017011058C4B000001287A00000A1306081106285A00000A16FE01131311132D0600161312DE1803095104110428C200000A54171312DE072600161312DE000011122A411C00000000000013000000820100009501000007000000010000011B3004004601000071000011000272010000705100036FB600000A1F0DFE01130D110D2D090016130CDD200100000416FE01130D110D2D090016130CDD0D010000737A0100060A061F406FB700000A00067E120500047E130500046FC300000A130673C400000A1307110711061773BE00000A1308037291E60170048C2D000001287A00000A286800000A28BB00000A1304110428BC00000A1305160D1613092B1000091105110991580D00110917581309110911058E69FE04130D110D2DE20920F52600005E0D7283E60170098C4B000001287A00000A0C08037291E60170048C2D000001287A00000A284300000A0B0728BB00000A1304110428BC00000A130A1108110A16110A8E696FC500000A0011086FC600000A0011076FC700000A130B02110B1628C000000A130E120E28B900000A510202501F141F306F8C00000A5117130CDE07260016130CDE0000110C2A0000411C00000000000008000000330100003B0100000700000001000001133003002B00000022000011007283E60170028C2D000001287A00000A729FE60170038C28000001287A00000A286800000A0A2B00062A7E000302161A6FC100000A28C800000A5404021A6FC900000A28CA00000A552A00133003003D000000720000110003120212012854010006130511052D06001613042B230716FE01130511052D06001613042B1308120012032857010006000206FE0113042B0011042A000000133003002200000073000011000403510217FE0116FE010C082D0D000304120028540100060B2B04170B2B00072A00000000000023215E48475E2423464453484A76622D1330030029000000740000111F108D5400000125D02005000428CC00000A8012050004178D540000010A0616179C0680130500042A1E02283700000A2AEE02283700000A000002036FCD00000A740400001B7D1705000402038E697D180500040220000100008D540000017D1905000402286301000600002A00000013300100070000000D00001100170A2B00062A0013300100070000000D00001100170A2B00062A0013300100070000002300001100170A2B00062A0013300100070000002300001100170A2B00062A00133005005D0100007500001100027B1C05000416FE01130411042D12000228AA00000A6FCE00000A73CF00000A7A0314FE0116FE01130411042D110072ADE6017072C5E6017073D000000A7A0E0414FE0116FE01130411042D1100720FE701707229E7017073D000000A7A0416321C0E05163217040558038E69300F0E0505580E048E69FE0216FE012B011600130411042D0C007275E7017073D100000A7A0405580C38AE0000000002027B1A050004175820000100005DD27D1A05000402027B1B050004027B19050004027B1A050004915820000100005DD27D1B050004027B19050004027B1A050004910B027B19050004027B1A050004027B19050004027B1B050004919C027B19050004027B1B050004079C027B19050004027B1A05000491027B19050004027B1B050004915820000100005DD20A0E040E05030491027B19050004069161D29C0004175810020E05175810050408FE04130411043A45FFFFFF050D2B00092A00000013300600400000007600001100027B1C05000416FE010C082D12000228AA00000A6FCE00000A73CF00000A7A058D540000010A02030405061628610100062602286301000600060B2B00072A13300400910000007700001100160B2B1000027B190500040707D29C000717580B07027B190500048E69FE040D092DE102167D1A05000402167D1B050004160C160B2B490008027B19050004079158027B1705000407027B180500045D915820000100005D0C027B1905000407910A027B1905000407027B1905000408919C027B1905000408069C000717580B07027B190500048E69FE040D092DA82A00000013300300540000000D00001100027B1C0500040A062D4800027B1705000416027B170500048E6928D200000A00027B1905000416027B190500048E6928D200000A0002167D1A05000402167D1B05000402177D1C0500040228D300000A00002A4A0228D400000A0000021F407DD500000A002A00133001000700000023000011001E0A2B00062A0013300200160000000D00001100031EFE010A062D0C0072C5E7017073D600000A7A2A1E0073D700000A7A1E0073D700000A7A0000133001000C0000007400001100178D540000010A2B00062A13300200220000000D00001100032C0B038E6917FE0216FE012B0117000A062D0C00720DE8017073D600000A7A2A000013300500190000007800001100178D140000010B07161E1E1673D800000AA2070A2B00062A000000133005001D0000007800001100178D140000010B07161E20000800001E73D800000AA2070A2B00062A00000013300100070000007900001100190A2B00062A0013300200160000000D000011000319FE010A062D0C007241E8017073D600000A7A2A000013300100070000007A00001100170A2B00062A0013300200160000000D000011000317FE010A062D0C007281E8017073D600000A7A2A0A002A00000013300200260000007B0000110073D900000A0A026FDA00000A1E5B8D540000010B06076FDB00000A0002076FDC00000A002A000013300100100000007C0000110072C9E8017028750100060A2B00062A133003005B0000007D000011000214FE0116FE010B072D110072D9E8017072E9E8017073D000000A7A027219E901701928DD00000A16FE010B072D0900737A0100060A2B200272C9E801701928DD00000A16FE010B072D090073760100060A2B04140A2B00062A2A0228650100060000002A0000133002008D0000007E00001100027B1D05000416FE010B072D12000228AA00000A6FCE00000A73CF00000A7A0314FE0116FE010B072D11007221E90170722FE9017073D000000A7A038E692C0F038E692000010000FE0216FE012B0116000B072D0C00726FE9017073D600000A7A042C0B048E6917FE0216FE012B0117000B072D0C0072B3E9017073D600000A7A03735C0100060A2B00062A000000133003000E0000007F000011000203046FB800000A0A2B00062A4600020328DE00000A0002177D1D0500042A5602286501000600000273760100067D1E050004002A000013300100110000002300001100027B1E0500046FDF00000A0A2B00062A00000013300200230000000D00001100031EFE010A062D0C0072F5E9017073D600000A7A027B1E050004036FE000000A002A0013300100110000002300001100027B1E0500046FE100000A0A2B00062A3E00027B1E050004036FE200000A002A00000013300100110000007400001100027B1E0500046FE300000A0A2B00062A3E00027B1E050004036FE400000A002A00000013300100110000007400001100027B1E0500046FE500000A0A2B00062A3E00027B1E050004036FDC00000A002A00000013300100110000002300001100027B1E0500046FDA00000A0A2B00062A3E00027B1E050004036FB700000A002A00000013300100110000008000001100027B1E0500046FE600000A0A2B00062A00000013300100110000008000001100027B1E0500046FE700000A0A2B00062A00000013300100110000007900001100027B1E0500046FE800000A0A2B00062A3E00027B1E050004036FE900000A002A00000013300100110000007A00001100027B1E0500046FEA00000A0A2B00062A3E00027B1E050004036FEB00000A002A3A00027B1E0500046FEC00000A002A3A00027B1E0500046FED00000A002A0013300300990000007E00001100027B1F05000416FE010B072D17000228AA00000A6FCE00000A7231EA017073EE00000A7A0314FE0116FE010B072D11007263EA01707271EA017073D000000A7A038E692C0F038E692000010000FE0216FE012B0116000B072D0C0072B1EA017073D600000A7A042C0B048E6917FE0216FE012B0117000B072D0C0072F5EA017073D600000A7A027B1E05000403046FB800000A0A2B00062A000000133003000E0000007F000011000203046FB800000A0A2B00062A0000133002003E0000000D00001100027B1F0500040A062D320002177D1F050004027B1E05000414FE010A062D1500027B1E0500046FEF00000A0002147D1E050004000228D300000A00002A00001B3005007801000081000011000E061A540E0772B7100070510E087201000070510E04166A550E0516731100000A8108000001007237EB017073F100000A0B00076FF200000A00076FF300000A0C00087267EB01706F1200000A0000020304050828010000060A060E060E070E08280800000600067B510400041F0CFE0116FE01130411042D0B000E04067B490400045500067B5104000416FE0116FE01130411042D60000E04067B4904000455067B4C0400047BAD04000416FE01130411042D16000E05067B4C0400047BA90400048108000001002B14000E05067B4C0400046F1C0100068108000001000E0616540E077299EB0170067B50040004286800000A510000DE1A0D0000087267EB01706F1300000A0000DE05260000DE0000097A00DE0A00086FF400000A0000DC0000DE120814FE01130411042D07086F1F00000A00DC0000DE120714FE01130411042D07076F1F00000A00DC0000DE240D000E061A540E0772B7100070510E0872B1EB0170096F7600000A286800000A5100DE00002A4194000000000000FD000000100000000D0100000500000001000001000000004F000000AC000000FB0000001A00000039000001020000004F000000C9000000180100000A000000000000000200000042000000E40000002601000012000000000000000200000033000000090100003C010000120000000000000000000000270000002B0100005201000024000000390000011B3008006501000082000011000E0B1A540E0C72B7100070510E0D720100007051007237EB017073F100000A13040011046FF200000A0011046FF300000A1305001105720DEC01706F1200000A000073010100060B070E076A7D65040004070E057D66040004070E086A7D67040004070E067D680400040716731100000A7D69040004070E0A7D6A0400041202110528E800000616FE01130711072D28000802283000000A16FE01130711072D0B0016731100000A0D002B090016731100000A0D00002B05000E090D00020304050E040709110528020000060A060E0B0E0C0E0D28090000060000DE1D130600001105720DEC01706F1300000A0000DE05260000DE000011067A00DE0B0011056FF400000A0000DC0000DE14110514FE01130711072D0811056F1F00000A00DC0000DE14110414FE01130711072D0811046F1F00000A00DC0000DE261306000E0B1A540E0C72B7100070510E0D7241EC017011066F7600000A286800000A5100DE00002A0000004194000000000000E100000011000000F2000000050000000100000100000000420000009C000000DE0000001D000000390000010200000042000000BC000000FE0000000B000000000000000200000034000000D90000000D01000014000000000000000200000022000000030100002501000014000000000000000000000015000000280100003D01000026000000390000011B3008001B01000083000011000E0B1A540E0C72B7100070510E0D720100007051007237EB017073F100000A0C00086FF200000A00086FF300000A0D0009729FEC01706F1200000A000073010100060B070E076A7D65040004070E057D66040004070E086A7D67040004070E067D680400040716731100000A7D69040004070E0A7D6A040004020304050E04070E090928030000060A060E0B0E0C0E0D28090000060000DE1C1304000009729FEC01706F1300000A0000DE05260000DE000011047A00DE0A00096FF400000A0000DC0000DE120914FE01130511052D07096F1F00000A00DC0000DE120814FE01130511052D07086F1F00000A00DC0000DE261304000E0B1A540E0C72B7100070510E0D72CDEC017011046F7600000A286800000A5100DE00002A00014C000000009D0010AD00050100000100003D005D9A001C3900000102003D007CB9000A000000000200300097C700120000000002002100BCDD00120000000000001500DEF30026390000011B3004006001000084000011000516731100000A81080000010E041A540E0572B7100070510E06720100007051007237EB017073F100000A0C00086FF200000A00086FF300000A0D00097225ED01706F1200000A00000203040928040000060A12010928E800000616FE01130511052D57000703283000000A16FE01130511052D150005067B4D0400047BA90400048108000001002B2E0005067B4D0400046F1C010006067B4D0400047BA9040004282600000A16731100000A282500000A810800000100002B130005067B4D0400046F1C010006810800000100060E040E050E0628080000060000DE1C13040000097225ED01706F1300000A0000DE05260000DE000011047A00DE0A00096FF400000A0000DC0000DE120914FE01130511052D07096F1F00000A00DC0000DE120814FE01130511052D07086F1F00000A00DC0000DE261304000E041A540E0572B7100070510E067251ED017011046F7600000A286800000A5100DE00002A4194000000000000E200000010000000F20000000500000001000001000000004900000096000000DF0000001C000000390000010200000049000000B5000000FE0000000A00000000000000020000003C000000D00000000C0100001200000000000000020000002D000000F50000002201000012000000000000000000000021000000170100003801000026000000390000011B3007002301000081000011000E061A540E0772B7100070510E08720100007051007237EB017073F100000A0B00076FF200000A00076FF300000A0C000872A7ED01706F1200000A0000020304050E040E050828050000060A0E041F0A6A2E0C0E041F0B6AFE0116FE012B011600130411042D10000304050E050E040828060000060A00060E060E070E08280800000600067B5104000416FE0116FE01130411042D0C000E060E041F646A5869540000DE1A0D00000872A7ED01706F1300000A0000DE05260000DE0000097A00DE0A00086FF400000A0000DC0000DE120814FE01130411042D07086F1F00000A00DC0000DE120714FE01130411042D07076F1F00000A00DC0000DE240D000E061A540E0772B7100070510E0872CBED0170096F7600000A286800000A5100DE00002A00014C00000000A80010B800050100000100003D0069A6001A3900000102003D0086C3000A0000000002003000A1D100120000000002002100C6E700120000000000001500E8FD00243900000142534A4201000100000000000C00000076322E302E35303732370000000005006C000000807A0000237E0000EC7A0000A088000023537472696E6773000000008C0301001CEE010023555300A8F10200100000002347554944000000B8F102002C1E000023426C6F620000000000000002000010579FA2290902000000FA253300160000010000006A000000690000002005000094010000F503000003000000F4000000F40300001800000001000000840000000E000000530000008A000000040000000100000001000000020000001B00000000000A000100000000000600790772070600800772070600850772070600AD0790070600BE0772070600CA0790070A00FF07E9070600140872070A00D208DD070600500A72070A00710ADD070600B80A72070600BC0BB00B0A00CA0BE9070600EA5D72070A00926DDD070A00096EDD070A00236EDD070A00DE6EE9070600677390070600967390070600B37390070600C875A9750600617D4F7D0600787D4F7D0600957D4F7D0600B47D4F7D0600CD7D4F7D0600E67D4F7D0600017E4F7D06001C7E4F7D0600357EA9750600497EA9750600577E4F7D0600707E4F7D0600A07E8D7E9300B47E00000600E37EC37E0600037FC37E06003B7F72070600417F72070A005A7FE9070A00777FE9070A009D7FDD070600A77F72070A00C07FAD7F0A00D67FAD7F0600038072070600388072070600538072070A005A80E9070A007680AD7F0A00A880DD070600825672070A00D180DD070A005281DD0706006D8172070A007781DD070600AF819C810A00EF81DD070A001082DD070A003582E9070A005682AD7F0A00DC82E90706000F83720706001E8372070600748372070A00A383DD070600B48372070A00CB83DD070A000784AD7F0A005884AD7F0600958472070A00B284DD070600CE8472070600EA8472070A004B85DD0706006A857207060085857B85060092857B850600A6857B850600B3857B850A00EA85CF850600FF85720706000486720706001986720706002F867B8506003C869007060049867B850600508690070600EE86C37E06000987720706004487C37E0600538772070600598772070600898772070600A18772070600B78772070600D38772070600F487900706000B88720706002188900706003A8890070A005088CF850A006688AD7F0A008988AD7F0000000001000000000001000100810110001F002400050001000100010010002F002400050001000D000100100044002400050001001000010010005A002400050009002200010010006F0024000500090024000100100082002400050013002600010100009800240009001700470001010000A200240009002C00470001010000BC00240009003600470001010000D100240009004300470001010000DE00240009004E00470001010000EF00240009007101470001010000FC002400090010024700010100000C0124000900200247000101000018012400090027024700010100002501240009002B024700010100003701240009003D024700010100004B0124000900410247000101000061012400090046024700010100007A01240009004B024700010100009701240009004E02470001010000B901240009005102470001010000CC01240009006302470001010000E601240009006802470001010000F401240009007B0247000101000001022400090086024700010100001002240009008A02470001010000240224000900900247000101000038022400090094024700010100004E02240009009802470001010000680224000900BF02470001010000730224000900DB02470001010000890224000900E002470001010000A00224000900E502470001210000B80224000900EB02470001010000CD0224000900F602470001010000DD0224000900FB02470001010000EB02240009004F03470001010000FE02240009007A034700010100000903240009007E034700010100001803240009008103470001001000290324000500840347000100100040032400050097036F0001010000540324000900A803920001010000670324000900B1039200010100007C0324000900BC03920001010000960324000900C003920001010000AD0324000900C503920001010000BF0324000900CB03920001010000D20324000900D403920001010000DE0324000900DA03920001010000EB0324000900E003920001010000F80324000900E503920001010000090424000900EE03920001010000160424000900F1039200010010002F0424000500F6039200010100004D0424000900FC039F0001010000560424000900FF039F000101000073042400090004049F000101000083042400090008049F00010100009904240009000C049F0001010000A6042400090015049F0001010000C6042400090019049F0001010000D104240009001D049F0001010000E1042400090023049F0001010000EA04240009002D049F0081011000F8042400050033049F00020100000405000009003404FA00020100001705000009003C04FA00020010003005000005004204FA00020010004205000005004904FB0002001000550500000500530400010200100065050000050065040101020010007505000005006C0402010200100087050000050072040A01020100009F050000090075040B0102001000B0050000050081040B0102001000C105000005008404100102001000D505000005009304110102001000F00500000500A604120102001000FF0500000500B1042901020010000C0600000500B304340102010000180600000900C604350102010000300600000900C9043501020100003F0600000900CD043501020100004C0600000900D204350102010000620600000900D6043501020100007D0600000900DA04350102010000A40600000900DE04350101001000B40624000500E404350102010000C10600000900E704420181011000D20624000500EB04420102010000DB0600000900EB044F0102001000EB0600000500EF044F0101001000F80600000500110551010100100002070000050011055401020100000D070000090014055C010001100017070000050017055C01810010002F07000019001D056501010110003307000090011D057601010110004207000090011E057A01810110005B07240005002005900100000000A986000005002005950113010000138700007101210595010100DC08AD000100E408B1000100FB08B50001000909B80001001909BB0001002B09B80001003809BB0001004509B8000600E209BB000600F309B5000600FE09B5000600050AB8000600120AB80006001B0AB50006002E0AB8000600430A6E010600590A71010600630A75010100DC08AD000100790A7E010100E408B10001007F0ABB000606370CB50056803F0CF2025680440CF2025680580CF2025680760CF20256808E0CF2025680A80CF2025680C90CF2025680EA0CF20256800B0DF20256802C0DF20256804D0DF20256806E0DF20256808F0DF2025680B00DF2025680C70DF2025680D90DF2025680F70DF20256801A0EF20256803B0EF2025680620EF2020606370CB5005680860E5A0356808E0E5A035680920E5A035680960E5A035680A10E5A035680A70E5A035680AC0E5A035680B10E5A035680B50E5A030606370CB5005680860E72035680C80E72035680D00E72035680D70E72035680E10E72035680E90E72035680F20E72035680FE0E72035680040F72035680180F72035680220F72035680360F72030606370CB5005680420F750156804F0F75015680540F75015680610F750156806E0F75015680790F75015680820F75015680930F75015680A20F75015680AB0F75010606370CB5005680420F8A035680B50F8A035680C20F8A035680D00F8A035680DA0F8A035680E50F8A035680ED0F8A035680F60F8A03568004108A0356801D108A03568024108A03568034108A03568045108A0356805B108A03568072108A0356807D108A0356808B108A03568099108A035680AD108A035680BB108A035680C8108A035680DC108A035680EE108A035680FF108A03568011118A03568019118A0356802E118A0356803D118A03568059118A03568068118A0356807A118A03568085118A03568094118A0356809F118A035680B1118A035680BF118A035680CD118A035680E9118A035680F6118A03568004128A03568019128A0356802C128A0356803F128A03568054128A0356806C128A03568086128A0356809E128A035680B9128A035680C9128A035680DB128A035680ED128A035680FD128A03568017138A0356802F138A0356803D138A03568056138A0356806B138A03568082138A0356809B138A035680B2138A035680C7138A035680E3138A035680F3138A0356800A148A03568028148A03568039148A03568048148A03568055148A03568070148A0356807E148A03568093148A035680A3148A035680B5148A035680CE148A035680EC148A03568009158A0356802E158A03568054158A0356807A158A03568093158A035680B1158A035680CE158A035680E8158A03568007168A0356801D168A03568033168A03568048168A0356805C168A03568070168A03568088168A0356809C168A035680AD168A035680C2168A035680D6168A035680EA168A0356800C178A03568039178A0356805D178A0356808E178A035680A2178A035680C6178A035680EA178A0356800F188A03568038188A03568057188A0356807D188A035680A3188A035680C7188A035680EB188A0356800C198A0356802D198A03568042198A03568067198A03568098198A035680C4198A035680F4198A0356801E1A8A035680481A8A0356807A1A8A035680A31A8A035680CC1A8A035680DF1A8A035680F31A8A035680061B8A035680191B8A0356803E1B8A035680581B8A035680731B8A035680861B8A035680AB1B8A035680CF1B8A035680F41B8A035680181C8A035680371C8A035680561C8A035680741C8A035680911C8A035680A41C8A035680B61C8A035680D31C8A035680F11C8A0356800E1D8A035680211D8A035680331D8A0356804B1D8A035680641D8A035680761D8A035680891D8A035680981D8A035680A81D8A035680CE1D8A035680F21D8A0356801B1E8A0356804B1E8A0356805E1E8A035680691E8A035680781E8A035680971E8A035680A81E8A035680BD1E8A035680CD1E8A035680DE1E8A035680F61E8A035680131F8A0356802D1F8A0356804B1F8A035680651F8A035680791F8A035680921F8A035680B11F8A035680C41F8A035680D91F8A035680E81F8A035680FE1F8A03568008208A03568030208A03568058208A03568087208A035680B6208A035680E4208A03568012218A03568026218A03568049218A0356806C218A0356808C218A035680AC218A035680D5218A035680FE218A03568026228A0356804D228A0356806A228A03568085228A0356809B228A035680BD228A035680D9228A035680F4228A03568011238A03568028238A03568045238A03568061238A0356807F238A03568099238A035680B4238A035680CF238A035680EF238A0356801B248A03568041248A03568066248A0356808D248A035680AE248A035680D5248A035680FB248A03568023258A0356803A258A03568054258A03568073258A0356808A258A035680AA258A035680CB258A035680FD258A0356802B268A03568051268A03568075268A0356808D268A035680A5268A035680BC268A035680D3268A035680EA268A03568001278A03568013278A0356803E278A03568069278A03568088278A035680AA278A035680D4278A03568002288A03568024288A03568052288A03568061288A03568084288A03568090288A035680A6288A035680B5288A035680DC288A035680F9288A0356801F298A0356803B298A0356805F298A03568082298A03568099298A035680B6298A035680D4298A035680EC298A0356800D2A8A0356802C2A8A035680472A8A035680622A8A035680782A8A0356808E2A8A035680A52A8A035680BC2A8A035680DB2A8A035680FC2A8A0356801F2B8A035680422B8A035680582B8A035680712B8A035680942B8A035680B72B8A035680DB2B8A035680FF2B8A035680222C8A035680442C8A0356805E2C8A035680822C8A0356809E2C8A035680B52C8A035680C42C8A035680E12C8A035680032D8A0356801C2D8A0356803A2D8A035680562D8A035680772D8A035680932D8A035680B42D8A035680D02D8A035680F12D8A035680072E8A0356801E2E8A030606370CB5005680362EE30856803B2EE3085680422EE30856804A2EE3085680552EE3085680612EE3085680722EE3085680812EE30856808A2EE3085680952EE3085680AC2EE3085680B62EE3085680C12EE3085680D52EE3085680E12EE3085680EE2EE3085680002FE30856800D2FE3085680192FE30856802B2FE30856803B2FE30856804A2FE3085680662FE3085680762FE3085680862FE30856808E2FE3085680A22FE3085680B32FE3085680C92FE3085680D72FE3085680F12FE3085680FF2FE30856801330E30856802930E30856803C30E30856804A30E30856806230E30856807530E30856808830E30856809430E3085680BF30E3085680D230E3085680DF30E3085680EB30E30856800231E30856802331E30856803431E30856804531E30856806231E30856807431E30856808931E30856809C31E3085680A931E3085680BD31E3085680D731E3085680E631E3085680FB31E30856801432E30856802E32E30856803E32E30856804C32E30856805F32E30856807332E30856808F32E30856809F32E3085680BB32E3085680D232E3085680E832E3085680F732E30856800833E30856801533E30856802733E30856803833E30856804233E30856805933E30856807133E30856807D33E30856809B33E3085680B733E3085680D933E3085680F833E30856802134E30856804234E30856805534E30856807234E30856808F34E3085680A234E3085680CE34E3085680EF34E30856801035E30856802E35E30856804C35E30856806E35E30856809A35E3085680C035E3085680E835E30856801336E30856803936E30856806636E30856808B36E3085680B036E3085680CA36E3085680E236E3085680FD36E30856800C37E30856801F37E30856803737E30856804A37E30856806837E30856808037E30856809737E3085680B037E3085680C537E3085680DF37E3085680F837E30856801338E30856802238E30856803338E30856806038E30856808A38E3085680A338E3085680BA38E3085680C838E3085680D338E3085680E438E3085680FF38E30856802039E30856802E39E30856804839E30856806639E30856808239E30856809539E3085680A839E3085680BC39E3085680D039E3085680EC39E30856800A3AE3085680223AE30856803A3AE30856805A3AE30856807A3AE30856808D3AE3085680A33AE3085680AF3AE3085680BD3AE3085680D13AE3085680DB3AE3085680FA3AE3085680163BE3085680363BE30856804F3BE30856806C3BE3085680883BE3085680A93BE3085680C53BE3085680E63BE3085680023CE3085680233CE3080606370CB50056803F0CC3095680373CC3095680453CC3095680493CC3095680503CC30956806A3CC30956807A3CC3095680913CC3095680A03CC3095680B53CC3095680BA3CC3095680C83CC3095680DE3CC3095680F23CC3095680FE3CC3090606370CB5005680073DCC095680173DCC0956802D3DCC095680393DCC095680483DCC095680603DCC090606370CB50056803F0CD0095680453CD0095680493CD0090606370CB5005680793DD4095680803DD4095680873DD4095680913DD40956809E3DD4095680A83DD4095680B73DD4095680CC3DD4095680D73DD4095680E13DD4095680F43DD40956801338D40956802238D4095680A338D4095680A33AD4095680AF3AD4095680BD3AD4090606370CB5005680793DD8095680803DD8095680093ED8090606370CB50056800E3EDC095680163EDC0956801B3EDC095680223EDC090606370CB50056800E3EE0095680273EE00956802C3EE0095680343EE0090606370CB5005680393EE4095680413EE4090606370CB5005680603EE8095680653EE8090606370CB50056803F0CEC0956806D3EEC0956807A3EEC095680863EEC095680953EEC0956809E3EEC095680AF3EEC095680B63EEC095680C63EEC095680DD3EEC095680F23EEC0956800B3FEC095680183FEC095680223FEC095680383FEC095680463FEC095680543FEC090606370CB50056805E3F090A5680623F090A56806A3F090A56806F3F090A0606370CB5005680860E0D0A5680453C0D0A56801F000D0A5680833F0D0A56808C3F0D0A5680913F0D0A56809E3F0D0A5680AA3F0D0A5680BA3F0D0A5680C13F0D0A5680CA3F0D0A5680D83F0D0A5680EA3F0D0A5680F23C0D0A5680FE3C0D0A5680F23F0D0A568006400D0A5680BA3C0D0A0606370CB50056800D40110A56801F40110A56802740110A56803640110A5680913F110A56803D40110A5680860E110A56805140110A56806A40110A56807940110A0606370CB5005680D00E240A56808740240A56809640240A0606370CB50056809E40280A5680AE40280A5680C840280A5680DB40280A5680EC40280A0606370CB5005680860E2C0A5680FE402C0A568008412C0A0606370CB5005680860E300A56801441300A56801E41300A0606370CB50056800E3E340A56802B41340A56804141340A56805741340A56806A41340A56808141340A56809541340A5680A841340A5680C241340A5680DC41340A5680F541340A56800C42340A56802142340A56803342340A56804C42340A56805F42340A56807242340A56808242340A56808E42340A56809A42340A5680AA42340A5680C342340A5680DA42340A5680F142340A56800843340A56801B43340A56803E43340A56804B43340A56806143340A56807543340A56808443340A56809743340A5680A643340A5680BC43340A5680D943340A5680F743340A56801644340A56803A44340A0606370CB50056805D44380A56806444380A56807344380A56808244380A56809744380A5680A144380A5680B544380A5680C744380A5680D844380A5680E744380A5680F144380A56800245380A56800F45380A56801A45380A56802B45380A56804045380A56805245380A56806C45380A56808345380A56809C45380A5680B245380A5680C845380A5680E445380A5680F645380A5680FE45380A56800746380A56801446380A0606370CB50056802646420A56803246420A56804246420A56806346420A0606370CB50056807F46470A56808446470A56808B46470A56809846470A0606370CB50056803F0C4C0A5680A8464C0A5680AE464C0A5680B8464C0A5680C4464C0A0606370CB5005680CF46510A5680D846510A5680A846510A5680DD46510A5680E846510A5680F746510A56800747510A56801947510A56801E47510A56802547510A0606370CB50056802D47600A56803A47600A56804347600A5680EE10600A0606370CB50056805A47650A5680E50F650A5680ED0F650A56806247650A56807210650A56806C47650A56807947650A56808647650A56809447650A5680A047650A5680B147650A5680F31A650A5680DF1A650A56805E1E650A5680CB47650A5680691E650A5680F61E650A56802D47650A5680E447650A5680EF47650A5680FC47650A56800C48650A56802748650A56804648650A56805548650A56806A48650A56807B48650A56809348650A56802C12650A5680A748650A5680C748650A5680D248650A5680DF48650A5680F448650A56801111650A56801911650A56801349650A56802649650A56803449650A56804149650A56804E49650A56806549650A56808749650A56809C49650A56806A22650A5680EE10650A5680B249650A5680D749650A56807325650A5680F949650A5680124A650A56802D4A650A5680032D650A56804D4A650A5680694A650A56800127650A5680814A650A5680974A650A5680B24A650A5680C94A650A5680E54A650A5680FE4A650A5680A628650A5680094B650A5680164B650A5680304B650A5680494B650A5680664B650A5680622A650A5680782A650A56808E2A650A5680A52A650A5680BC2A650A5680DB2A650A56802C2A650A5680472A650A5680FC2A650A56801F2B650A56807B4B650A56808B4B650A56809C4B650A5680BA4B650A5680C84B650A0606370CB50056805D44B00A5680DB4BB00A5680E44BB00A5680ED4BB00A5680F74BB00A5680004CB00A5680104CB00A5680244CB00A56802E4CB00A5680384CB00A5680414CB00A56804D4CB00A5680614CB00A56806D4CB00A5680794CB00A56808C4CB00A5680964CB00A5680A04CB00A5680AD4CB00A5680BE4CB00A5680D14CB00A5680E54CB00A5680F14CB00A5680004DB00A5680104DB00A5680224DB00A5680662FB00A5680334DB00A56804B4DB00A5680664DB00A5680844DB00A5680974DB00A5680A94DB00A5680BA38B00A5680BA4DB00A5680C838B00A5680C44DB00A5680D74DB00A5680E74DB00A5680F94DB00A56800746B00A56801446B00A0606370CB5005680084EB50A56800D4EB50A5680144EB50A0606370CB5005680224EBA0A56802A4EBA0A0606370CB5005680364EBF0A5680494EBF0A0100584EC40A0100644EB8000100724EB8000100814EC40A0100904EC40A01009C4EC40A0100B04EC40A0100C44EC40A0100D14EC40A0100DE4EC40A0100F04EC40A0100FE4EB50001000D4F6E010100244F6E0101003D4F510A0100474FBB0001005E4FC80A0100764FC40A01008E4F4C0A01004953BB00010058534C0A01005F53B80001007153B80001007D53B80001008B53B50001009A53B8000100584EC40A0100AC53B8000100BC53B8000100D3536E010100E653B8000100FD53B80001001454B80001001E54710101002F54600A01004254B8000606370CB50056802757500B56802B57500B56803157500B56803657500B56803B57500B56804057500B56804557500B56804A57500B0606370CB50056802B57550B56806247550B56805057550B56801F40550B56806557550B56806E57550B56807A57550B56808657550B56809757550B5680A457550B0606370CB5005680B7575A0B5680BF575A0B5680C3575A0B0606370CB5005680084E5F0B5680C6575F0B5680D3575F0B5680E0575F0B0606370CB5005680B757640B5680E557640B5680F957640B56800158640B56800858640B0606370CB50056801958690B56802158690B56802958690B56802F58690B56803758690B56803F58690B56804758690B56804E58690B0606370CB500568056586E0B56805F586E0B568064586E0B568069586E0B568074586E0B0606370CB5005680860E730B56807A58730B56808658730B5680A058730B5680A958730B0606370CB50056800E3E780B5680C258780B5680D458780B5680DF58780B0606370CB5005680F4587D0B5680FA587D0B568003597D0B568008597D0B56800D597D0B568014597D0B568023597D0B568034597D0B0606370CB50056803F59870B56804759870B0606370CB500568050598C0B568058598C0B568064598C0B56807C598C0B01009359BB000100A059B5000100AB59C40A0100B459B8000100BF59510A01001454B8000606370CB5005680975A910B5680493C910B0606370CB5005680084E960B56809E5A960B5680A35A960B56809E3D960B0606370CB5005680084E9B0B5680AC5A9B0B5680B05A9B0B0606370CB5005680E057A00B5680B75AA00B5680BA5AA00B0606370CB50056803F0CA50B5680BE5AA50B5680CA5AA50B5680D65AA50B5680D95AA50B5680DC5AA50B5680E75AA50B5680EF5AA50B0606370CB50056803F0CAA0B5680F75AAA0B56800C5BAA0B0606370CB50056803F0CAF0B5680255BAF0B56802B5BAF0B0606370CB5005680084EB40B5680335BB40B5680355BB40B5680375BB40B5680395BB40B0606370CB50056803B5BB90B56804C5BB90B56805F5BB90B5680695BB90B5680735BB90B56808A5BB90B56809A5BB90B5680AA5BB90B5680C75BB90B0606370CB5005680E75BBE0B5680EF5BBE0B5680F75BBE0B5680FF5BBE0B5680075CBE0B56800F5CBB000606370CB50056804C636D1056804F636D10568055636D105680E8326D10568064636D10568075636D10568085636D100606370CB5005680B757721056809563721056809D6372105680A16372105680B16372100600B763BB000600F309B5000600F806B8000600C5636E010600D06372100600DD6377100600F6636E0106000A64BB00060018646E0106002964771006003464771006004464771006004F64771006006064C40A06009F56B800060067646D10060072647C100600EF00E3080600AB64B80006009744BB0006000A64BB000600B763BB000600B7646E010600DF64C40A0600ED646E010600F76488100600FE646E0106000565BB00060016658D1006002865B80006003C656E0106004465771006005565771006007065771006007F65771006008A65BB0006009665C40A0600A365BB000600AC65C40A0600B665C40A0600C465C40A0600D165C40A0600E065C40A0600F165C40A06000166C40A06001166C40A06002366C40A06003B2E921006001E67C40A06002A67C40A06003667C40A0606370CB50056804C63971056804267971056804D6797105680586797105680626797105680556397105680E8329710568064639710568072679710568075639710568085639710060085677710060067649710060072647C1006000A64BB0006009744BB0006009D67AD000600AE67C40A0600BA67C40A0600C367C40A0600D167C40A0600DF67C40A0600EA67C40A0600F56777100600FD67771006000468771006003C656E010600446577100600556577100600EF00E3080600AB64B80006000A64BB0006009744BB000600F309B5000600050AB800060062676E01060008687710060072647C1006001B68BB000600FD67771006002568BB000600046877100600FE646E0106000565BB00060016658D1006002865B80006003C656E0106002C68C40A06003A68C40A06004568C40A06005568C40A06006868C40A060071686E01060084686E01060091686E010600A5686E010600B568C40A0600C468C40A0600D368C40A0600F567771006006064C40A06009744BB000600066A6E0106000E6AB50006001A6AB5000600266AB50006009F56B8000600336ACC0906003A6ABB000600516ABB000600666AB5000600786AD4090600916A6E010600B36AC40A0600D46AC40A0600F36ABB000600046B77100600176B77100600246BC40A0600306BB8000606370CB50056803D6B6E1156804C6B6E110606370CB5005680666B73115680716B73115680866B73110606370CB50056809D6B78115680B46B78115680CB6B78115680E26B78110606370CB5005680F46B7D1156800D6C7D115680296C7D110606370CB5005680516C821156807C6C82115680A46C82110606370CB5005680C56C87115680EC6C871156801C6D87110606370CB50056800E3E8C1156804A6D8C115680576D8C115680666D8C115680716D8C110100876DAD0001005853AD0001009A6D91110606370CB50056809D63CC115680476ECC1156804E6ECC110606370CB50056803C6F3D1256804E6F3D125680606F3D120600F309B50006006B6FB5000600726F0D0A06007F6FB8000600846FB8000600066A6E010600916F240A0600986F3D120600516ABB000600A16FBB000600B26FB5000600BB6FC40A0600CE6F6E010600DC6F6E010600FE6FB50006006802BB0006000770BB00060011706E01060028706E01060042706E0106005070B50006006A70B50006008770B80006009E70B8000600B270B8000600C670B8000600E070B8000600F570BB0006000E71B80006001971B80006009656B8000600BF56B80006002171B50006002871B80051804771B50031005771521231006C7152120606370CB50056803F0C861256800472861201000872521201001672B500010021725212010032728B12010039728B12010040726E0101003A746E0101006674F412010040726E01130130879B1C50200000000096000E080A0001002C210000000096001C08160006002422000000009600230816000E006C24000000009600270828001600A425000000009600350833001A0018290000000096003F0842002100E82A0000000091004E0850002700482F0000000096005E0862002D00E02F0000000096005E086F003100BC3000000000960068087C00350088310000000096008108830037008C32000000009100AB088E003B004C33000000008600C00898003E00C833000000008600C608A00040004A34000000008618CC08A900440054340000000086005409BE0044007C340000000086006509C3004500B8340000000086007909A90047002037000000008618CC08C9004700B037000000008618CC08CF0048004938000000008618CC08D7004B005C38000000008618CC08DE004E00E0380000000086008409E600520010390000000086008409F600590048390000000086008409070161007439000000008600840918016900A83900000000860084092B017300DC3900000000860084093F017E00C03B000000009600880954018A00703C0000000086089A095B018C00D23C000000008600B209A9008C00E43C000000008600B8095F018C00543D000000008600C90964018D00BC46000000008600C00898008E000047000000008618CC08A900900008470000000086006C0A790190002547000000008618CC08A900900030470000000086088D0A790190004847000000008608A40A8201900054470000000086007909A90091006F4A000000008618CC08C9009100A44A000000008618CC0888019200D44A00000000860084098F019400F84A00000000860084099A019900204B0000000086008409A6019F00484B0000000086008409B301A600744B0000000086008409C001AD00A04B0000000086008409CE01B500CC4B0000000086008409DE01BE00F84B0000000086008409ED01C600284C0000000086008409FE01D0005C4C00000000860084091002DB00A04C00000000860084092302E600E44C00000000860084093702F2007450000000008600C30A5E0201019850000000008600C30A68020501D451000000008600CF0A74020A018052000000008600E30A5E020F018C53000000008100FA0A7F02130168580000000086000F0BBE0019019C58000000008600250B8D021A01D0580000000086003B0B8D021B010459000000008600510B8D021C0138590000000086006C0B93021D016C590000000086007D0B99021E01B859000000008600A60BA1022001EC59000000008600C909640120012C64000000008100D70BA50221019886000000009600E50BCB022E019096000000009600040CDC023401E49A000000008608984FCD0A3801FC9A000000008608A54F8D023801089B000000008608B24F5B013901209B000000008608C94FBE0039012C9B000000008608E04FD20A3A01449B000000008608F34FD60A3A01509B0000000086080650D20A3B01689B0000000086081A50D60A3B01749B0000000086082E50CD0A3C018C9B0000000086083D508D023C01989B0000000086084C50A1023D01B09B0000000086085950DB0A3D01BC9B0000000086086650E00A3E01D49B0000000086087D50E40A3E01E09B0000000086089450CD0A3F01F89B000000008608A4508D023F01049C000000008608B450CD0A40011C9C000000008608C2508D024001289C000000008608D050CD0A41014C9C000000008608DE50CD0A4101649C000000008608F2508D024101709C0000000086080651CD0A4201889C0000000086081A518D024201949C0000000086082E51CD0A4301AC9C0000000086083C518D024301B89C0000000086084A51CD0A4401D09C00000000860858518D024401DC9C0000000086086651CD0A4501F49C00000000860879518D024501009D0000000086088C51E00A4601189D000000008608A551E40A4601249D000000008608BE51E90A47013C9D000000008608C951EF0A4701489D000000008608D451F60A4801609D000000008608E051FC0A48016C9D000000008608EC51030B4901849D0000000086080452090B4901909D0000000086081C52CD0A4A01A89D00000000860834528D024A01B29D000000008618CC08A9004B01D49D00000000860858545B014B01EC9D0000000086086854BE004B01F89D0000000086087854F60A4C01109E0000000086088154FC0A4C011C9E0000000086088A54D20A4D01349E0000000086089D54D60A4D01409E000000008608B054D20A4E01589E000000008608BD54D60A4E01649E000000008608CA54D20A4F017C9E000000008608D954D60A4F01889E000000008608E854A1025001A09E000000008608F854DB0A5001AC9E0000000086080855D20A5101C49E0000000086081655D60A5101D09E0000000086082455CD0A5201E89E00000000860831558D025201F49E0000000086083E55D20A53010C9F0000000086084F55D60A5301189F0000000086086055D20A5401309F0000000086087355D60A54013C9F0000000086088655D20A5501549F0000000086089855D60A5501609F000000008608AA55D20A5601789F000000008608BC55D60A5601849F000000008608CE55E00A57019C9F000000008608DD55E40A5701A89F000000008608EC55D20A5801C09F000000008608F955D60A5801CC9F0000000086080656330B5901E49F000000008608145693025901F09F0000000086082256380B5A0108A000000000860836563E0B5A0114A00000000086084A56D20A5B012CA00000000086086056D60A5B0136A0000000008618CC08A9005C0144A0000000008608CF59A1025C015CA0000000008608DC59DB0A5C0168A0000000008608E959CD0A5D0180A0000000008608F4598D025D018CA0000000008608FF59D20A5E01A4A00000000086080B5AD60A5E01B0A0000000008608175AE90A5F01C8A0000000008608285AEF0A5F01D4A0000000008608395A5B016001ECA0000000008608475ABE006001F8A0000000008608555AD20A610110A1000000008608615AD60A61011AA1000000008618CC08A900620124A10000000096002F5CCC0B620130A70000000096004B5CDC0B6701F0A7000000009600635CE70B690118A8000000009600635CF80B6F01B0A90000000091007E5C100C7801B8B3000000009600955C2B0C84011CB5000000009600635C380C890144B5000000009600955C4F0C910100B6000000009600A55C5E0C96016CB7000000009600BD5C680C9B01FCB8000000009600D75C710C9F0120BA000000009600ED5C790CA201C0BE000000009600025D900CAB010CC2000000009600185D9F0CB201BCC20000000096002E5DA70CB501F0C60000000096004B5DBB0CBD0128CA0000000096006A5DCB0CC2014CCA0000000096006A5DDC0CC80154D40000000091007F5DEE0CCF01F8D5000000009100A05DFB0CD3011CD7000000009100BC5D040DD60168DD000000009600D15D100DD90130DF000000009600F35D1C0DDE01D4E10000000091000C5E2C0DE301C0E2000000009600285E380DE60148E6000000009600415E4D0DED010CE80000000096005E5E600DF501BCE80000000096007C5E670DF701ECEB000000009600965E790DFD0190EF000000009600B45E600D010258F2000000009600D95E600D030260F6000000009600F25E820D050290F70000000096000D5F8D0D090284F80000000096002C5F960D0C02B0F80000000096002C5FA70D1302D8F80000000096002C5FB90D1B0204F9000000009100435FCE0D2402BCFA0000000096002C5FD70D27022403010000009600655FED0D310280030100000091007C5FF80D34022C04010000009600655F020E36024804010000009600655F0D0E39028410010000009600935F190E3D02A412010000009600A95F190E4002E814010000009600C25F240E43020C16010000009600D95F2D0E44023416010000009600D95F390E48025C16010000009600F15F460E4D028016010000009600F15F590E5302DC16010000009600F15F6F0E5A020417010000009600F15F7D0E5E022C17010000009600F15F8D0E63025417010000009600F15FA00E69027C17010000009600F15FB40E6F02F417010000009600F15FCD0E77026C18010000009600F15FE70E80029018010000009600F15FF90E8702F8240100000096000A60600D8F02A8250100000096001A600E0F9102C0260100000096003060170F94025C2C0100000096004B60230F9902243201000000960066603D0FA10244320100000096006660490FA602F8330100000096007F60570FAD0274380100000091009F606C0FB5023439010000009600C160740FB7029C3A010000009600D4607F0FBC02D83C010000009100DD608B0FC302CC3F01000000960004619F0CC8027C400100000096002661980FCB02E4440100000096005A61A70FD10200450100000096005A61B50FD50218450100000096005A61C40FDA02BC470100000096007961D80FE1020C480100000091009961E00FE302D049010000009100AF61E80FE5023C4A010000009100BD61EF0FE602C04B010000009100DB61EF0FE8020C4D010000009100FA61EF0FEA02584E0100000091001962EF0FEC0224500100000091002C62E80FEE0200510100000091003B62F80FEF02E45201000000960057620A10F502785401000000910076621810FB02605501000000960095622110FE02B856010000009600B462331007030C58010000009600D8623D100A03045C010000009100F162600D0E03F05C010000009100116348101003045E01000000910022635010130358640100000096003A6365101A035465010000008618CC08A9001D0378650100000086007B6480101D03A8650100000086008464D60A1F03D9650100000086008C64A9002003E4650100000086089764D20A20030466010000008618CC08A90020037466010000008618CC08A90020033067010000008618CC08A900200390670100000086083466CD0A2003B8670100000086084966CD0A2003D0670100000086085B66CD0A2003E8670100000086086D66CD0A200318680100000086087D66CD0A200330680100000086089366CD0A20034868010000008608A666CD0A20036B68010000008618CC08A90020037F68010000008618CC08A900200388680100000086007B649C102003B8680100000086008464D60A2203E9680100000086008C64A9002303F4680100000086089764D20A23031169010000008618CC08A90023033869010000008618CC08A9002303EC69010000008618CC08A9002303B06A01000000C600E468D20A23030F6B010000008618CC08A9002303286B010000008618CC08A4102303C46B010000008618CC08AB1024033B6C010000008618CC08B5102703516C010000008618CC08C1102B03786C010000008618CC08CE103003CB6C010000008618CC08DF103703F46C010000008608ED68CD0A3F03186D0100000086080169CD0A3F03306D0100000086081869CD0A3F03606D0100000096082969F2103F03786D0100000086083269F8103F03946E0100000086084169F8103F038C6F0100000096085B69FE103F03D86F0100000096086769FE104103347001000000960876690A114303847001000000960882690A114503A07001000000C600906914114703C07001000000C6009769A1024803D870010000009600A36919114803F470010000009600A369221149037C7101000000E601AA692C114B03F87101000000C600E468D20A4B032A72010000008618CC08A9004B034C72010000008618CC083C114B037472010000008618CC0843114C03947201000000960829694C114E03AC720100000096085B6952114E03E872010000009608676952115003247301000000960876695E115203607301000000960882695E1154037C7301000000C6009069141156039C7301000000C6009769A1025703B473010000008618CC08A90057036974010000008618CC08A900570390740100000086089F6D95115703BE74010000008608AC6D9B115703EC74010000008600B96DA21158033876010000008600C56DD60A5C03A876010000008600D16DD60A5D03DC76010000008600E06DAA115E034477010000008600E96DD20A5F039077010000008600EF6DD60A5F03DC77010000008100F76DA9006003E078010000008100FC6DAF1160036C79010000008100166EB5116103C0790100000081002F6EBD116303047A010000009600586ED1116603007B0100000096006A6ED8116803E07C0100000096007E6EE5116D03F07E0100000096006A6EEF117203147F0100000096006A6EFA117503347F010000009600986EFA117803547F0100000091006A6E05127B030485010000009600B86E12128003D086010000009600CF6E1C1284032888010000009600CF6E251287034088010000009600E96E2C128903248B010000009600046FD1118A03D88C010000009600286F33128C03648D010000008600ED6F42128F03848D010000008618CC08A90090030C8E010000009600317148129003488E0100000096003C71481291038D8E010000008618CC08A9009203988E010000009600807156129203609001000000960094715F129503D091010000009600A871671298030792010000009600BE716D129A032892010000009600D57176129D037492010000009600F3717C129F03ED92010000008618CC08A900A203B892010000009118A286971CA203F592010000008618CC088E12A203349301000000E6094872E00AA303489301000000E6095E72E00AA3035C9301000000E6097D72A102A303709301000000E6099072A102A303849301000000E601A4729412A303F09401000000E601B3729F12A8033C95010000008100F76DA900AB03DC9501000000E601C772A900AB033C96010000008418CC08A900AB03509601000000C6081B73A102AB03649601000000C6082973DB0AAB03869601000000C6083773A102AC038E9601000000C6084873DB0AAC03989601000000C6085973A812AD03B09601000000C60860738E12AD03E09601000000C6087073AD12AE03089701000000C6088473AD12AE03349701000000C608A173B312AE03489701000000C608AA73B812AE036C9701000000C608BF73BE12AF03809701000000C608CB73C312AF03A29701000000C600D773A900B003A89701000000C600E273A900B003DC97010000009600EE73C912B003F897010000009600EE73CF12B0035F98010000008618CC08A900B1036C9801000000C6004674EB12B103089901000000C6005674EB12B303229901000000C400C772E40AB5033499010000008618CC08A900B6034C9901000000C6081B73A102B6036C9901000000C6082973DB0AB6039C9901000000C6083773A102B703B99901000000C6084873DB0AB703CC9901000000C6085973A812B803E99901000000C60860738E12B803FC9901000000C6087574A812B903199A01000000C6087D748E12B9032C9A01000000C6088574A102BA03499A01000000C6089174DB0ABA035C9A01000000C6087073AD12BB037C9A01000000C6088473AD12BB039C9A01000000C608A173B312BB03B99A01000000C608AA73B812BB03CC9A01000000C608BF73BE12BC03E99A01000000C608CB73C312BC03F99A01000000C600D773A900BD03089B01000000C600E273A900BD03189B01000000C6004674EB12BD03C09B01000000C6005674EB12BF03DC9B010000008100C772A900C103289C010000009600A974F912C103409E010000009600C2740C13CA0348A0010000009600DC740C13D803BCA1010000009600F3742513E603BCA301000000960009753513ED03000001001B75000002002475000003003175000004008809000005003F75000001001B75000002004375000003005575000004006875000005000A64000006007F7500000700176B000008003F75000001001B75000002004375000003005575000004006875000005000A64000006007F7500000700176B000008003F75000001008809000002001B75000003005575000004003F75000001008809000002001B7500000300247500000400317500000500917500000600765A000007003F75000001001B75000002002475000003003175000004009975000005009175000006003F7500000100F30900000200A175000003000A64000004007F7500000500176B000006003F75000001004A6D02000200676402000300D57502000400E07500000100666D02000200676402000300D57502000400E075000001001B75000002003F75000001000A6402000200974402000300F309000004003F75000001000A64000002003F7502000300916F000001006F0000000200EA7500000100F30900000200050A000003000A6400000400EA75000001000A6400000100974400000200F806000001006F00000001006F0000000200F30900000300050A00000100F30900000200050A000003000A6400000100F30900000200050A000003000A6400000400AB6400000100765600000200974400000300825600000400F175000005000076000006000A7600000700176B00000100765600000200974400000300825600000400F175000005000076000006000A7600000700176B000008000A6400000100765600000200974400000300825600000400F175000005000076000006000A7600000700176B00000800147600000100765600000200974400000300825600000400F175000005000076000006000A7600000700176B00000800147600000900F30900000A00050A00000100765600000200974400000300825600000400F175000005000076000006000A7600000700176B00000800147600000900F30900000A00050A00000B001C7600000100765600000200974400000300825600000400F175000005000076000006000A7600000700176B00000800147600000900F30900000A00050A00000B001C7600000C000A64000001009744000002003F75000001002476000001003F75000001006F0000000200EA75000001002A76000001006F00000001006F0000000200050A00000100765600000200EF0000000300765A00000400974400000500307600000100765600000200EF0000000300765A000004009744000005003076000006003E7600000100765600000200EF0000000300765A00000400974400000500307600000600497600000700537600000100765600000200EF0000000300765A000004009744000005003076000006003E76000007007D5A00000100765600000200EF0000000300765A000004009744000005003076000006004976000007007D5A00000800537600000100765600000200EF0000000300765A000004009744000005003076000006004976000007007D5A000008005A7600000900537600000100765600000200EF0000000300765A000004009744000005003076000006003E76000007007D5A000008005A7600000100765600000200EF0000000300765A000004009744000005003076000006003E76000007007D5A000008005A76000009006F7600000A007A7600000100765600000200EF0000000300765A000004009744000005003076000006004976000007007D5A000008005A76000009006F7600000A007A7600000B00537600000100765600000200EF0000000300765A000004009744000005003076000006003E76000007007D5A000008005A76000009006F7600000A007A7600000B008F7600000100765600000200EF0000000300765A000004009744000005003076000006004976000007007D5A000008005A76000009006F7600000A007A7600000B008F7600000C00537600000100765600000200EF0000000300765A000004009744000005003076000006003E76000007007D5A000008005A76000009006F7600000A007A7600000B008F7600000C00997600000D00090400000E00AA7610100F00537600000100765600000200974400000300307600000400B17600000100765600000200974400000300307600000400B176000005005A7600000100765600000200974400000300307600000400B17600000500C07600000100765600000200974400000300307600000400B17600000100765600000200974400000300307600000400B17600000500D176000006005A7600000100DA76000001000076000001000A76000001000A7600000100500A00000100E47600000200F276000001003F75000001003F7502000200FE76020003000277020004001277020005002777020006003877020007005277020008006D7702000900947702000A00A77702000B00B17702000C00C37702000D00D17700000100EF0000000200765A020003000A7602000400007600000500307610100600537600000100EF00000002000A7600000300007610100400E177000001002A76000001002A76000001002A76000001002A76000001002A76000001002A76000001002A76000001002A76000001002A76000001002A76000001002A76000001002A76000001002A76000001002A76000001002A76000001002A76000001002A76000001002A76000001002A76000001002A76000001002A76000001002A76000001002A76000001002A76000001002A76000001002A76000001002A76000001002A76000001002A76000001002A76000001002A76000001002A76000001002A76000001002A76000001002A76000001002A76000001002A76000001002A76000001002A76000001002A76000001002A76000001002A7600000100EA7700000200974402000300F177020004000178000005003F75000001000B78000002003F7500000100B76300000200D20600000300A175000004001178000005001B75000006003F7500000100B76300000200D20600000300A17500000400117800000500D063000006001A7800000700F663000008001B75000009003F7500000100A17500000200B76300000300F30900000400726F00000500050A00000600117800000700D063000008001A7800000900F66300000A001B7502000B002C7800000C003F7500000100765A000002003378000003003B7802000400417800000500EA7500000100B76300000200D20600000300A17500000400117800000500D063000006001A7800000700F663000008003F7500000100765A000002003378000003003B7802000400417800000500EA7500000100974400000200F309000003000A64000004004B78000005003F7500000100974400000200F309000003000A64000004003F7500000100974400000200F309000003003F7500000100974400000200F30900000300726F00000400117800000500D063000006001A78020007000A6402000800EA77000009003F75000001009744000002000C01000003005E7800000400F30900000500726F020006000A64000007003F75000001000A64000002009744000003003F7500000100825600000200974400000300F30900000400726F000005006778000006008178020007000A64000008003F7500000100726F000002000A64000003007F75020004008F78000005003F7500000100F30900000200726F00000300050A000004000B78020005002C78000006003F7500000100F30900000200726F00000300050A000004000B78000005001B75020006002C78000007003F75000001000A64000002009578000003009B78000004003F7500000100974402000200A578000003003F7500000100BA78000002002C78000003003F75000001009744000002000A6400000300F30900000400C178000005003F7500000100CE7800000200BA78000003002C7800000400726F000005003F7500000100EB0600000200FD67000003003F75000001000A64000002003B2E00000300FD6700000400046800000500422E00000600D165000007003F75000001000A6400000200E47800000300F478000004000479000005000F79000006001B79000007002C79000008003F75000001009744000002003F75000001009744000002000A6402000300397902000400497902000500567900000600EA75000001009744000002000A6400000300B76300000400EA7500000100974400000200EA7500000100974400000200EA75000001000A6400000200677900000300737900000400EA7500000100765A00000200555200000300EA7500000100974400000200F309000003000A6400000400B763000005007C7902000600847900000700EA7500000100974400000200F309000003000A6400000400B763000005007C79000006001B7502000700847900000800EA7500000100974400000200F309000003000A6400000400B763000005007C79000006008D79020007008479020008009C7900000900EA75020001002D74000002009744000003003F7500000100974400000200F309000003000A6400000400B763000005007C79000006008D79000007001B75020008008479020009009C7900000A00EA75000001000A64020002005679000003003F7502000100986F000002003F7500000100C105000002007264000003003F7500000100C10500000200726400000300A979000004003F75000001000A6402000200C105000003003F75000001000A6402000200C105000003003F75000001000B7800000100974400000200B67902000300C279000004003F7500000100974400000200B67900000300DA7902000400C279000005003F7500000100974400000200EE7900000300B67902000400F47902000500246B000006003F7500000100974400000200EE7900000300B67900000400FA7902000500F47902000600246B000007003F7500000100974400000200EE7902000300F479000004003F7500000100974400000200EE7902000300F479020004000A64000005003F7500000100974400000200EE7902000300F47902000400246B020005000A64000006003F7500000100974400000200EE7902000300F479020004000A6402000500087A000006003F7500000100974400000200EE7900000300B67902000400F47902000500246B020006000A6402000700087A000008003F7500000100974400000200EE7900000300B67900000400DA7902000500F47902000600246B020007000A6402000800087A000009003F7500000100974400000200880900000300EE7900000400B67900000500DA79020006000C06000007003F7500000100974400000200880900000300EE7900000400B67900000500DA7900000600FA79020007000C06000008003F75000001009744000002003F75000001009744000002000E6A00000300EA7500000100F30900000200B763020003009E3D02000400147A000005003F7500000100EA7700000200974400000300B763000004003464020005004F6402000600287A020007003B7A000008003F75000001004A7A000002000A64000003003464000004001178000005003F75000001004A7A000002000A64000003003464000004001178000005003C65000006005A7A000007003F7500000100974400000200EB0600000300717A00000400827A00000500397902000600947A020007009E7A000008003F75020001002D74000002003F75000001009744000002009F5600000300050A00000400726F000005003F7500000100B37A00000200BE7A00000300C77A00000400D27A00000500DC7A00000600E87A00000700EA75000001009744000002001A7800000300F30900000400F17A00000500EA7500000100F30900000200017B00000300EA7500000100F30900000200B76300000300097B020004009E3D02000500147A000006003F75000001009744000002001A7B00000300287B02000400916F000001009744000002001A7B00000300287B00000400846802000500916F000001009744000002001A7B00000300287B00000400846802000500916F02000600347B000007003F75020001004B7B00000200EA7500000100BA7800000200577B00000100BA7800000100677B00000200BA7800000100677B00000200BA7800000100677B00000200BA7800000100677B00000200BA7800000100BA7800000100727B000002000A6402000300807B020004008A7B02000500947B000006003F75000001000A6400000200A17B00000300B67B00000400626700000500EF00000006003F75000001000A64000002007264000003003F75000001000A6400000200176B00000300974400000400F30900000500726F00000600050A00000700CB7B020008007264000009003F75000001000A6400000200F764000003003F7500000100974400000200EA7700000300765A00000400EA75000001000A64000002003F7500000100F309000002000A64000003003F7500000100D20600000200DD7B00000300947B00000400BA78000005002C7802000600F12F000007003F7500000100EC7B00000200F27B000003003F75000001004F6300000200A36400000100A364000001004F6300000200A36400000100A36400000100FA7B000001003A68000002004568000003005568000001003A6800000200456800000300556800000400D368000001003A68000002004568000003005568000004006868000005008468000001003A6800000200456800000300556800000400686800000500846800000600B56800000700C468000001003A6800000200456800000300556800000400686800000500846800000600B56800000700C46800000800D36800000100007C00000200057C00000100007C00000200057C00000100007C00000200057C00000100007C00000200057C000001000A7C00000100007C00000100007C000002000E7C00000100FF0500000100F56700000200606400000100007C00000200057C00000100007C00000200057C00000100007C00000200057C00000100007C00000200057C000001000A7C000001002A76000001009D6300000200476E000003001D7C00000400084E00000100267C00000100267C00000100267C000001002F7C00000100267C00000100926D00000200337C00000100926D000002002F7C000003003D7C00000100F309000002003F75000001001B7500000200247500000300317502000400D206000005003F7500000100467C000002001B75000003002475000004003175000005003F7500000100F309020002004D7C000003003F7500000100050A020002004D7C000003003F75000001005D7C020002004D7C000003003F7500000100F30900000200050A000003005D7C020004004D7C000005003F7500000100F30900000200707C02000300797C000004003F7500000100857C00000200F309000003003F75000001008A7C00000200857C000001003F7500000100F309000002003F7500000100F309000002003F7502000300726F00000100857C00000100307600000100307600000100880902000200977C02000300A97C02000100880900000200977C00000300A97C00000100B27C00000200B97C00000100977C02000200B27C02000300B97C00000100B27C000002008809000001002D7400000200C27C02000300D47C000001009D7400000100E07C00000200EC7C00000300F87C00000400037D00000500107D00000100E07C00000200EC7C00000300F87C000001002A76000001002A76000001002A76000001002A76000001002A76000001001D7D00000100257D000002002C7D00000100257D000002002C7D00000100327D000001002A76000001002A76000001002A76000001002A76000001002A76000001002A76000001002A7600000100257D000002002C7D00000100257D000002002C7D000001001B75000002002475000003003175000004008809020005000A6402000600346402000700676402000800D575020009003C7D000001001B75000002004375000003005575000004006875000005000A6400000600966500000700AC65000008008A6500000900A36500000A00176B00000B00487D02000C00676402000D00D57502000E003C7D000001001B75000002004375000003005575000004006875000005000A6400000600966500000700AC65000008008A6500000900A36500000A00176B00000B00487D02000C00676402000D00D57502000E003C7D000001008809000002001B7500000300557502000400F56702000500676402000600D575020007003C7D000001008809000002001B7500000300247500000400317500000500917500000600765A02000700676402000800D575020009003C7D51000D006300110063001500B900CC08A900C100CC08D60AC900CC08D60AD100CC08D60AD900CC08D60AE100CC08D60AE900CC08D60AF100CC08D60AF900CC08D60A0101CC08E40A0901CC08D60A1101CC08D60A1901CC08D60A2101CC0848133101CC08DB0A3901CC08A9004100CC08DB0A3900C909D60A3900327FD60A4901487F4F134100766975136900CC08A90069004F7FA1130900E468D20A3900687FA7139900CC08AD1399008E7FB71359018409BD137101CC7FC6137901E07FA1022900C772A9004101E468D20A4901F07FF4134100F77F75134100E468D20A4901F07FFC1381010880031441006769031441005B69031441008269751341000C800C14410015800314810121801414410025807513810134800314590184091B1449013F80B800490176694614790145802C119101FA7B5314990068806214A1018380E00AA10188805F01A101918068140900CC08A90049019A8084144900CC08A9004900BD808914B101C9808F14A90184099614B901DC80E40AB901EE80BE00B9010481BE00B9011A81E40AA9012A81A01449003381A7144901487FB91449004281C01459004981C51449006481CB14C1018409D114D1019281A102C1012A81E81459002A81EE144900B209A90069010C80F814D101BB81FF14D901C9812C11D901D581E00A7101DE81D60A710102820515790120820C15F101CC08A900F10144821315F9011C081915090064822C11B9017482C6130C00CC084F151400CC084F1549018269461441008582751314009B82E00A1C009B82E00A0C009B82E00A5900A882A3155900B682AA116900BD82DB0AF901C882DB0A7101E9822C114100F3821414A1010483DC150902CC08A9001102CC08A9004901487F3316F101CC081315F9013483191559002A81391649019069AA11A10139837316A1014383781641004C8303148101588314144100618375136901E468D20A4901487F7D164901487F83161902E468D20AC9019764D20AA1017C83E416A101878378168101088053174901F07F591779009283ED1769004F7FF1175900498143184900AF8380188101C58387184900DC839218F101E3831315F101F5839D1839021384E40AF9011C08A2184901F07FA91859002D84E00A59003B84D20A79014884D20A4102BB81FF1471016E84D20A49018084D20A4901858430196901E4689B1969008D84A01949029D84A719A101A784E00A8101C583AD197901BE84C9197100CC08D01959018409D81959012A81DF19A101D584571A5100E1845D1A61020C80A61A4100F084AD1A6900FC84B31A41000985BA1A8901E4689B194100E4689B196900FC84C01A69001585A11369001C85C81A09009769A1024901487F171BC901CC08D60A69002385A10249012E85D20A59003685A90049003D85A9008100CC08D60A4900CC08D60A0900C9803C1BA9012A81421B81005F85491B690284098201490190695B1B7902CC08A90081009D85701B8902CC08D60A8100BE85821BA101C685A91B49018D84C01B9902CC08A90049012385A10231009174DB0A31004674EB12A902E468D20A49010B86061CA90213860C1CB1022686111CB902CC088E12C102CC08171CC9028380231CB10261862B1C49016A86321C69011386381C31005674EB12B902CC08A900C90274865F1CC1027A86A900B9028A86A81249029286381C49016A86731649029A86831CD902CC08A900E9026C87A01CF102AA692C11B1017C87D20A0103CC08D60A0903CC08B21C1103CC08D60AF102B209CF1C1903D687D81C3100CC08A9003100E787B5002103CC08D60A2903CC08A900A100CC08DD1C3103CC08A90031008574A102390326868E1231007D748E1249019069051D3100C772E40A31001B73A10231002973DB0A31003773A10231004873DB0A31005973A812310060738E1231007574A81231007073AD1231008473AD123100A173B3123100AA73B8123100BF73BE123100CB73C3123100D773A9003100E273A9000103CC08B21C3100B209A9004103CC08A9005101CC08D60A49037388A90051017888261D51039788A90008006000F60208006400FB0208006800000308006C000503080070000A03080074000F0308007800140308007C001903080080001E0308008400230308008800280308008C002D03080090003203080094003703080098003C0308009C0041030800A00046030800A4004B030800A80050030800AC0055030800B400F6020800B800FB020800BC0000030800C0005E030800C40005030800C80063030800CC0068030800D0006D030800D4000A030800DC00F6020800E000FB020800E40000030800E8005E030800EC0005030800F00063030800F40068030800F8006D030800FC000A03080000017603080004017B0308000801800308001001850308001401F60208001801FB0208001C010003080020015E0308002401050308002801630308002C016803080030016D03080034017B0308003C01850308004001F60208004401FB0208004801000308004C015E0308005001050308005401630308005801680308005C016D03080060010A03080064017603080068017B0308006C018003080070018E0308007401930308007801980308007C019D0308008001A20308008401A70308008801AC0308008C01B10308009001B60308009401BB0308009801C00308009C01C5030800A001CA030800A40114030800A801CF030800AC01D4030800B001D9030800B401DE030800B801E3030800BC01E8030800C001ED030800C401F2030800C801F7030800CC01FC030800D00101040800D40106040800D8010B040800DC0110040800E00115040800E4011A040800E8011F040800EC0124040800F00129040800F4012E040800F80133040800FC013804080000023D0408000402420408000802470408000C024C04080010025104080014025604080018025B0408001C026004080020026504080024021903080028026A0408002C026F04080030027404080034027904080038027E0408003C028304080040028804080044028D0408004802920408004C029704080050029C0408005402A10408005802A60408005C02AB0408006002B00408006402B50408006802BA0408006C02BF0408007002C40408007402C90408007802CE0408007C02D30408008002D80408008402DD0408008802E20408008C02E70408009002EC0408009402F10408009802F60408009C02FB040800A00200050800A40205050800A8020A050800AC020F050800B00214050800B40219050800B8021E050800BC0223050800C00228050800C4022D050800C80232050800CC0237050800D0023C050800D40241050800D80246050800DC024B050800E00250050800E40255050800E8025A050800EC025F050800F00264050800F40269050800F8026E050800FC027305080000037805080004037D0508000803820508000C038705080010038C05080014039105080018031E0308001C039605080020039B0508002403A00508002803A50508002C03AA0508003003AF0508003403B40508003803B90508003C03BE0508004003C30508004403C80508004803CD0508004C03D20508005003D70508005403DC0508005803E10508005C03E60508006003EB0508006403F00508006803F50508006C03FA0508007003FF0508007403040608007803090608007C030E06080080031306080084031806080088031D0608008C032206080090032706080094032C0608009803310608009C0336060800A0033B060800A40340060800A80345060800AC034A060800B0034F060800B40354060800B80359060800BC035E060800C00363060800C40368060800C8036D060800CC0372060800D00377060800D4037C060800D80381060800DC0386060800E0038B060800E40390060800E80328030800EC0395060800F0039A060800F4039F060800F803A4060800FC03A90608000004AE0602000104461308000404B30608000804B80608000C04BD0608001004C20608001404C70608001804CC0608001C04D10608002004D60608002404DB0608002804E00608002C04E50608003004EA0608003404EF0608003804F40608003C04F90608004004FE0608004404030708004804080708004C040D07080050041207080054041707080058041C0708005C042107080060042607080064042B0708006804300708006C043507080070043A07080074043F0708007804440708007C044907080080044E0708008404530708008804580708008C045D07080090046207080094046707080098046C0708009C0471070800A00476070800A4047B070800A80480070800AC0485070800B0048A070800B4048F070800B80494070800BC0499070800C0049E070800C404A3070800C804A8070800CC04AD070200CD0446130800D004B2070800D404B7070800D804BC070800DC04C1070200DD0446130800E004C6070800E404CB070800E804D0070800EC04D5070800F004DA070800F404DF070800F804E4070800FC04E90708000005EE0708000405F30708000805F80708000C05FD07080010050208080014050708080018050C0808001C051108080020051608080024051B0808002805200808002C052508080030052A08080034052F0808003805340808003C053908080040053E0808004405430808004805480808004C054D08080050055208080054055708080058055C0808005C056108080060056608080064056B0808006805700808006C057508080070057A08080074057F0808007805840808007C058908080080058E0808008405930808008805980808008C059D0808009005A20808009405A70808009805AC0808009C05B1080800A005B6080800A405BB080800A805C0080800AC05C5080800B005CA080800B405CF080800B805D4080800BC05D9080800C005DE080800C805F6020800CC05FB020800D00500030800D4055E030800D80505030800DC0563030800E00568030800E4056D030800E8050A030800EC0576030800F0057B030800F40580030800F8058E030800FC059303080000069D0308000406A20308000806A70308000C06AC0308001006B10308001406B60308001806E70808001C06EC0808002006BB0308002406C00308002806C50308002C06CA0308003006140308003406CF0308003806D40308003C06D90308004006DE0308004406E30308004806E80308004C06ED0308005006F20308005406F70308005806FC0308005C060104080060060604080064060B0408006806100408006C061504080070061A04080074061F0408007806240408007C062904080080062E0408008406330408008806380408008C063D04080090064204080094064704080098064C0408009C0651040800A00656040800A4065B040800A80660040800AC0665040800B00619030800B4066A040800B8066F040800BC0674040800C00679040800C40683040800C80688040800CC068D040800D00692040800D4069C040800D806A1040800DC06A6040800E006AB040800E406B0040800E806F1080800EC06F6080800F006BA040800F406BF040800F806C4040800FC060F05080000072305080004072805080008072D0508000C073205080010073705080014073C0508001807410508001C074605080020074B0508002407500508002807550508002C075A05080030075F0508003407640508003807690508003C076E05080040077305080044077805080048077D0508004C078205080050078705080054078C0508005807130608005C071806080060071D0608006407220608006807270608006C07FB08080070072C0608007407310608007807360608007C070009080080070509080084070A09080088070F0908008C071409080090071909080094071E0908009807230908009C0728090800A0072D090800A40732090800A80737090800AC073C090800B00741090800B40746090800B8074B090800BC0750090800C00755090800C4075A090800C8075F090800CC0764090800D00769090800D4076E090800D80773090800DC0778090800E0077D090800E40782090800E80787090800EC078C090800F00791090800F40796090800F8079B090800FC07A00908000008A50908000408AA0908000808AF0908000C08B40908001008B90908001408A20808001808A70808001C08AC0808002008B10808002408B60808002808BB0808002C08C00808003008C50808003408CA0808003808CF0808003C08BE0908004408F60208004808FB0208004C080003080050085E03080054080A05080058080F0508005C081405080060083C05080064086E0508006808130608006C08180608007008400608007408450608007808C70908007C08810608008408F60208008808FB0208008C080003080090085E030800940805030800980863030800A008F6020800A408FB020800A80800030800B008F6020800B408FB020800B80800030800BC085E030800C00805030800C40863030800C80868030800CC087B030800D00880030800D4089D030800D808A2030800DC08C5030800E008CA030800E40814030800E808CF030800EC08D4030800F008D9030800F808F6020800FC08FB0208000009000308000809F60208000C09FB02080010090003080014095E0308001C09F60208002009FB02080024090003080028095E0308003009F60208003409FB0208003C09F60208004009FB0208004809F60208004C092303080050092803080054092D0308005809320308005C093703080060093C0308006409410308006809460308006C094B0308007009500308007409550308007809F00908007C09F50908008009FA0908008409FF0908008809040A08009009F60208009409FB0208009809000308009C095E030800A40985030800A809FB020800AC095E030800B00963030800B4090A050800B8090F050800BC0914050800C00919050800C4091E050800C80923050800CC0928050800D0092D050800D40932050800D80937050800DC093C050800E00941050800E40946050800E8094B050800F009F6020800F409FB020800F80900030800FC097B030800000A9D030800040AC5030800080A150A08000C0AB9090800100A1A0A0800140A1F0A08001C0AF6020800200AFB020800240A000308002C0AF6020800300AFB020800340A00030800380A5E0308003C0A05030800440AF6020800480AFB0208004C0A00030800540AF6020800580AFB0208005C0A00030800640AF6020800680AFB0208006C0A80030800700A8E030800740A93030800780AA20308007C0AA7030800800ACA030800840A14030800880ACF0308008C0AF2030800900AF7030800940AFC030800980A010408009C0A24040800A00A29040800A40A2E040800A80A33040800AC0A38040800B00A3D040800B40A4C040800B80A51040800BC0A56040800C00A5B040800C40A60040800C80A65040800CC0A19030800D00A6A040800D40A6F040800D80A74040800DC0A79040800E00A7E040800E40A83040800E80A88040800EC0A8D040800F00A92040800F40A97040800F80A9C040800000BF6020800040BFB020800080B000308000C0B5E030800100B05030800140B7B030800180B800308001C0B8E030800200B93030800240B98030800280B3D0A08002C0BF7030800300BFC030800340B01040800380B060408003C0B0B040800400B10040800440B15040800480B1A0408004C0B1F040800500B0A050800540B0F050800580B190508005C0B14050800600B1E050800640BD4080800680BD9080800700BF6020800740BFB020800780B000308007C0B5E030800840BF6020800880BFB0208008C0B00030800900B5E030800980BF60208009C0BFB020800A00B00030800A40B5E030800A80B05030800B00BF6020800B40BFB020800B80B00030800BC0B5E030800C00B1A0A0800C40B560A0800C80B5B0A0800CC0B05030800D00B63030800D40B68030800DC0BF6020800E00BFB020800E40B00030800E80B5E030800F00BF6020800F40BFB020800F80B00030800FC0B5E030800000C05030800040C63030800080C680308000C0C6D030800100C0A030800140C76030800180C7B0308001C0C80030800200C8E030800240C93030800280C980308002C0C3D0A0800300C0F030800340C6A0A0800380C6F0A08003C0C740A0800400C9D030800440C0A050800480C0F0508004C0C14050800500C19050800540C1E050800580C230508005C0C28050800600C2D050800640C32050800680C370508006C0C3C050800700C41050800740C46050800780C4B0508007C0C50050800800C55050800840C5F050800880C640508008C0C69050800900C6E050800940C73050800980C780508009C0C7D050800A00C82050800A40C87050800A80C8C050800AC0C91050800B00C1E030800B40C790A0800B80C7E0A0800BC0C830A0800C00C880A0800C40C8D0A0800C80C920A0800CC0C970A0800D00C9C0A0800D40CA10A0800D80CA60A0800DC0C96050800E00C9B050800E40CA0050800E80CA5050800EC0CAA050800F00CAF050800F40CB4050800F80CB9050800FC0CBE050800000DC3050800040DC8050800080DCD0508000C0DD2050800100DD7050800140DDC050800180DE10508001C0DE6050800200DEB050800240DF0050800280DFF0508002C0D04060800300DAB0A0800340DD4080800380DD9080800400DF6020800440DFB020800480D000308004C0D5E030800500D05030800540D63030800580D680308005C0D6D030800600D0A030800640D76030800680D7B0308006C0D80030800700D8E030800740D93030800780D980308007C0D3D0A0800800D0F030800840D6A0A0800880D6F0A08008C0D740A0800900D9D030800940DA2030800980DA70308009C0DAC030800A00DB1030800A40DB6030800A80DE7080800AC0DEC080800B00DBB030800B40DC0030800B80DC5030800BC0DCA030800C00D14030800C40DCF030800C80DD4030800CC0DD9030800D00DDE030800D40DE3030800D80DE8030800DC0DED030800E00DD4080800E40DD9080800EC0DF6020800F00DFB020800F40D00030800FC0DF6020800000EFB020800080EF60208000C0EFB020800A40EF6020800A80EFB020800AC0E00030800B00E5E030800B40E05030800B80E63030800BC0E68030800C00E6D030800C80EFB020800CC0E00030800D00E5E030800D40E05030800D80E63030800DC0E68030800E00E6D030800E40E0A030800E80E76030800EC0E7B030800F40EF6020800F80EFB020800FC0E00030800040FF6020800080FFB0208000C0F00030800100F5E030800180FF60208001C0FFB020800200F00030800240F68030800280F6D030800300FF6020800340F05030800380F630308003C0F76030800400F7B030800440F0F030800480F6F0A08004C0FB6030800540FF6020800580FFB0208005C0F00030800600F5E030800640F050308006C0FF6020800700FFB020800740F00030800780F5E0308007C0F05030800840F85030800880FF60208008C0FFB020800900F00030800980F820B08009C0F85030800A00FF6020800A40FFB020800A80F05050800AC0F1A0A0800B00F560A0800B40F5B0A0800BC0FF6020800C00FFB020800C80FF6020800CC0FFB020800D00F00030800D40F05030800F40FFB020800F80F000308000010F60208000410FB0208000810000308000C105E0308001410F60208001810FB0208001C10000308002410F60208002810FB0208002C10000308003410F60208003810FB0208003C100003080040100503080044100A03080048100F0308004C10140308005010190308005810F60208005C10FB0208006010000308006810850308006C10F60208007010FB0208007810F60208007C10FB02080080100003080084105E0308008810050308009010FB02080094100003080098105E0308009C1005030800A01063030800A41068030800A8106D030800AC100A030800B01076030800B810FB020800BC1000030800C0105E030800C41005030800C81063030A00CC10C30B0800D410F6020800D810FB020800DC107B030800E01080030800E4108E030800E8109D030800EC10A2030800F410F6020800F810FB020800FC100003080000115E030800041105030800D811F6020800DC11FB020800E01100030800E4115E030800E81105030800EC117B030800F01180030800F4118E030800F81193030800FC119D0308000012A20308001C13FB0208002013000308002813F60208002C13FB0208003013000308003813F60208003C13FB02080040130003080044135E0308004C13F60208005013FB0208005413000308005C13F60208006013FB0208006413000308006C13F60208007013FB0208007413000308007C13F60208008013FB02080084130003080088135E0308008C1305030800A013F6020800A413FB020800A81300030800B013F6020800B413FB020800B8130003080044144D1208005414F60208005814FB022E001300711D2E001B008B1D2E0083000A1E2E0043008B1D2E007B00011E2E002B00911D2E003300711D2E003B00A01D2E0023008B1D2E0053008B1D2E005B00C11D2E006B00EB1D2E007300F81D03013303FB02E3023303FB02C3073303FB02030D5B06FB02202AAB05FB02402AAB05FB0200328307FB0220328307FB0240328307FB0260328307FB0280328307FB020100100000006900561366137D139413CB13E213251440144C1458146D1479148014AF14D714DE14F3141F153915421563156C15771587158E159C15A815AE15E215F415FB1505160A160E16121616161C16221628162D163E16621668168B16E916EF16F81602172A17391740175F179217A017CA17F6170D182618311838183E1849186A187418B018E418F5180019081910191A1923192A1936194D197F19B519E519F819131A2B1A3B1A481A641A741A7C1A881A981ACF1AEB1AF61AFE1A041B0B1B111B1D1B2F1B351B4F1B631B791B8C1B931B9E1BAE1BC61BD61BE51BEE1BF81BFF1B3D1C671C881C911CAA1CB81CC01CC81CE41CED1CF21CF71CFF1C0E1D151D1B1D201D2B1D3A1D4F1D611D04000100070002002B0004002C0018003900290048002F004B0030004E0037005100380052003E005B003F00630040006400440066004B000000CE096A0100006F00E8020000270CED0200004C52100B000055526A0100006852150B00007752150B00008752100B00009252190B00009B521D0B0000AE52100B0000BA52100B0000C452100B0000CE52100B0000DE52100B0000EE52100B0000F852100B00000253100B000011531D0B00002653210B00002D53270B000040032D0B00003553100B000076566A0100008256270B00008756150B00009656150B00009F56150B0000AA56190B0000F806150B0000B656100B0000BF56150B0000CC56150B0000DB56150B0000E956150B0000F7561D0B00000257150B00000B57450B0000CD024A0B00001557150B00006D5A190B0000765A100B00007D5A150B00000904210B0000855A6A0100008F5A150B0000A364150B0000B766100B0000C866100B0000D666100B0000E466100B0000F066100B00000267100B00001167100B0000A364150B0000B069100B0000C069100B0000D369100B0000E06930110000E56936110000F06936110000E069681100003E6EC6110000CF721D0B0000E1721D0B0000FC72190B00000B73190B0000F573190B0000FF73190B00000C74D61200000F74DB1200001F74DB1200002D74E11200003274E6120000F573190B0000FF73190B00000C74D61200009D74D6120000A174190B00000F74DB1200001F74DB1200002D74E11200003274E61202001E000300020026000500010027000700020047000900010048000900020049000B0001004A000B0002004B000D0001004C000D0002004D000F0001004E000F0002004F001100010050001100020051001300010052001300020053001500010054001500010056001700020055001700020057001900010058001900020059001B0001005B001D0002005A001D0002005C001F0001005D001F0001005F00210002005E002100010061002300020060002300020062002500010063002500010065002700020064002700020066002900010067002900020068002B00010069002B0002006A002D0001006B002D0001006D002F0002006C002F0002006F003100010070003100010072003300020071003300010074003500020073003500020075003700010076003700020077003900010078003900020079003B0001007A003B0002007B003D0001007C003D0002007D003F0001007E003F0001008000410002007F004100020081004300010082004300010084004500020083004500010086004700020085004700020087004900010088004900020089004B0001008A004B0002008B004D0001008C004D0002008D004F0001008E004F0002008F00510001009000510002009200530001009300530002009400550001009500550002009600570001009700570002009800590001009900590001009B005B0002009A005B0002009C005D0001009D005D000200FE005F00020002016100020003016300020004016500020005016700020006016900020007016B00020008016D0002000E016F0002001A01710002001B01730002001C01750002001D01770002001E01790002001F017B0002002D017D00020036017F00010037017F0002005D01810002005E01830002005F018500020060018700010067018900020066018900020068018B00010069018B0001006B018D0002006A018D0002006C018F0002006D01910002006E01930001006F01930002007001950001007101950002007B01970001007C01970002007D01990001007E01990002007F019B00010080019B00010082019D00020081019D00020083019F00010084019F0002008501A10002008601A30002008701A50001008801A50002008901A70001008A01A700471555155D15AF1CA89201002005048000000100000000000000000000000000217F000002000000000000000000000001006907000000000200000000000000000000000100DD070000000045004400460044004700440048004400490044004A0044004B0044004C0044004D0044004E0044004F004400500044005100440052004400530044005400440055004400560044005700440058004400590044005A0044005C005B005E005D005F005D00620061006900680000000000003C4D6F64756C653E0053514C427573696E6573734C6F6769632E646C6C0054334753005753492E436F6D6D6F6E004163636F756E744D6F76656D656E747354657374004163636F756E744D6F76656D656E74735461626C6500436173686965724D6F76656D656E747354657374004361736869657253657373696F6E496E666F00436173686965724D6F76656D656E74735461626C65005341535F464C414753004143434F554E545F50524F4D4F5F4352454449545F54595045004143434F554E545F50524F4D4F5F5354415455530047555F555345525F5459504500434153484945525F4D4F56454D454E54004D6F76656D656E745479706500506C617953657373696F6E54797065004163636F756E745479706500436173686C6573734D6F646500506C617953657373696F6E537461747573004754506C617953657373696F6E537461747573004754506C61796572547261636B696E675370656564004754506C617953657373696F6E506C61796572536B696C6C0050656E64696E6747616D65506C617953657373696F6E5374617475730047616D696E675461626C6544726F70426F78436F6C6C656374696F6E436F756E74004163636F756E74426C6F636B526561736F6E004163636F756E74426C6F636B556E626C6F636B536F75726365005465726D696E616C54797065730048414E445041595F54595045005465726D696E616C537461747573004143434F554E54535F50524F4D4F5F434F5354005757505F434F4E4E454354494F4E5F54595045005757505F434F4E4E454354494F4E5F53544154555300545950455F4D554C5449534954455F534954455F5441534B530053657175656E6365496400506172616D617465727354797065466F725369746500434153484945525F53455353494F4E5F5354415455530043757272656E637945786368616E6765537562547970650043757272656E637945786368616E676554797065005472616E73616374696F6E54797065004F7065726174696F6E436F64650043617368696572566F75636865725479706500556E646F5374617475730046554C4C5F4E414D455F54595045005743505F5465726D696E616C547970650043757272656E637945786368616E6765526573756C740042616E6B5472616E73616374696F6E446174610047524F55505F454C454D454E545F54595045004558504C4F49545F454C454D454E545F54595045005061727469636970617465496E436173684465736B447261770055706772616465446F776E6772616465416374696F6E004177617264506F696E7473537461747573004C616E67756167654964656E7469666965720053595354454D5F4D4F4445005041545445524E5F54595045004754506C6179657254797065004361676543757272656E6379547970650043757272656E6379547970650053746174757355706461746543757272656E6379547970650043617368696572436F6E636570744F7065726174696F6E526573756C74005053415F54595045004361736869657253657373696F6E5472616E73666572537461747573005465726D696E616C426F7854797065005465726D696E616C43617368446972656374696F6E00466C61674275636B6574496400486F6C646572486170707942697274684461794E6F74696669636174696F6E0050696E5061645479706500486F6C6465724C6576656C54797065004275636B65744964005072697A6543617465676F7279004D756C746950726F6D6F7300537461727453657373696F6E53746174757300537461727453657373696F6E5472616E736665724D6F646500537461727453657373696F6E496E70757400537461727453657373696F6E4F757470757400456E6453657373696F6E496E70757400506C61796564576F6E4D6574657273005449544F53657373696F6E4D6574657273005449544F53657373696F6E4D657465727343617368496E00456E6453657373696F6E53746174757300456E6453657373696F6E4F757470757400496E53657373696F6E506172616D657465727300506C617953657373696F6E436C6F7365506172616D6574657273004163636F756E7442616C616E63650050726F6D6F42616C616E6365004163636F756E74496E666F004163636F756E7450726F6D6F74696F6E73416374696F6E00414C41524D5F424952544844415900524551554553545F5459504500524551554553545F545950455F524553504F4E53450047414D455F474154455741595F52455345525645445F4D4F444500454E554D5F47414D455F474154455741595F50524F4D4F5F42414C414E43455F414354494F4E00496E53657373696F6E416374696F6E0050726F76696465724C6973740050726F76696465724C69737454797065005465726D696E616C0043726564697473506C61794D6F6465005465726D696E616C496E666F00547261636B4461746100436172644E756D6265720043726970744D6F646500415243466F75724D616E616765645472616E73666F726D0052433400415243466F75724D616E616765640052433443727970746F5365727669636550726F76696465720053716C50726F63656475726573006D73636F726C69620053797374656D004F626A65637400456E756D0049436C6F6E6561626C650053797374656D2E53656375726974792E43727970746F677261706879004943727970746F5472616E73666F726D0049446973706F7361626C650053796D6D6574726963416C676F726974686D0053797374656D2E446174610053797374656D2E446174612E53716C436C69656E740053716C5472616E73616374696F6E00537461727400446563696D616C0055706461746500456E64004163636F756E745374617475730053656E644576656E7400496E736572745F48616E64706179005570646174655F496E7465726E616C005472616E736C6174650044425F436865636B496645786973747356656E646F7249640044425F4765744163636F756E74416E645465726D696E616C49644279506C617953657373696F6E496400676574506C617953657373696F6E537461747573005465737431005465737432002E63746F7200446174615461626C65006D5F7461626C65006D5F636173686965725F73657373696F6E5F696E666F006D5F7465726D696E616C5F6964006D5F7465726D696E616C5F6E616D65006D5F706C61795F73657373696F6E5F6964006D5F747261636B5F64617461006D5F6163636F756E745F6964006D5F636173686965725F6E616D6500536574506C617953657373696F6E4964005365744163636F756E74547261636B4461746100496E6974536368656D61004164640045787465726E616C547261636B44617461006765745F4C61737453617665644D6F76656D656E74496400436C6561720053617665644D6F76656D656E747349640053617665004C61737453617665644D6F76656D656E744964004361736869657253657373696F6E4964005465726D696E616C496400557365724964005465726D696E616C4E616D6500557365724E616D6500417574686F72697A6564427955736572496400417574686F72697A65644279557365724E616D650049734D6F62696C6542616E6B004461746554696D650047616D696E6744617900557365725479706500436F70790044617461526F77006D5F726F77006D5F6D6F76656D656E745F6964006765745F4361736869657253657373696F6E496E666F007365745F446174615461626C65546F53617665004E756C6C61626C6560310041646445786368616E67650041646443757272656E637945786368616E67650041646443617368416476616E636545786368616E67650041646445786368616E67655F496E7465726E616C0041646452656C617465644964496E4C6173744D6F7600416464537562416D6F756E74496E4C6173744D6F7600536574416464416D6F756E74496E4C6173744D6F7600536574496E697469616C42616C616E6365496E4C6173744D6F760053657444617465496E4C6173744D6F7600536574496E697469616C416E6446696E616C42616C616E6365416D6F756E74496E4C6173744D6F7600526F7773436F756E740053797374656D2E5465787400537472696E674275696C6465720053716C506172616D657465720055706461746542616C616E636500476574436173686965724D6F76656D656E74416464537562416D6F756E7400476574436173686965724D6F76656D656E7442616C616E6365496E6372656D656E7400446174615461626C65546F536176650076616C75655F5F004E4F4E45005553455F455854454E4445445F4D455445525300435245444954535F504C41595F4D4F44455F5341535F4D414348494E45005553455F48414E445041595F494E464F524D4154494F4E005350454349414C5F50524F47524553534956455F4D45544552005553455F4D455445525F46495845445F344244435F4C454E4754485F42495430005553455F4D455445525F46495845445F344244435F4C454E4754485F42495431005553455F4D455445525F46495845445F344244435F4C454E4754485F42495432005553455F4D455445525F46495845445F344244435F4C454E4754485F42495433005553455F4D455445525F46495845445F354244435F4C454E4754485F42495430005553455F4D455445525F46495845445F354244435F4C454E4754485F42495431005553455F4D455445525F46495845445F354244435F4C454E4754485F42495432005553455F4D455445525F46495845445F354244435F4C454E4754485F42495433005553455F464C4147535F464F5243455F4E4F5F52544500414C4C4F575F53494E474C455F42595445005341535F464C4147535F49474E4F52455F4E4F5F4245545F504C415953005341535F464C41475F44495341424C45445F4146545F4F4E5F494E54525553494F4E005341535F464C4147535F4146545F414C54484F5547485F4C4F434B5F5A45524F005341535F464C4147535F454E41424C455F44495341424C455F4E4F54455F4143434550544F52005341535F464C4147535F4155544F4D415449435F48414E445041595F5041594D454E5400554E4B4E4F574E004E5231004E52320052454445454D41424C4500504F494E5400554E523100554E5232004E52330052454445454D41424C455F494E5F4B494E440041574152444544004143544956450045584841555354454400455850495245440052454445454D4544004E4F545F41574152444544004552524F520043414E43454C4C45445F4E4F545F41444445440043414E43454C4C45440043414E43454C4C45445F42595F504C415945520050524541535349474E4544004E4F545F41535349474E45440055534552005359535F4143434550544F52005359535F50524F4D4F424F58005359535F53595354454D005359535F5449544F005359535F47414D494E475F5441424C45005359535F524544454D5054494F4E005359535F4332474F00535550455255534552004F50454E5F53455353494F4E00434C4F53455F53455353494F4E0046494C4C45525F494E0046494C4C45525F4F555400434153485F494E00434153485F4F5554005441585F4F4E5F5052495A45310050524F4D4F54494F4E5F4E4F545F52454445454D41424C45005052495A455300434152445F4445504F5349545F494E00434152445F4445504F5349545F4F55540043414E43454C5F4E4F545F52454445454D41424C45004D425F4D414E55414C5F4348414E47455F4C494D4954004D425F434153485F494E005441585F4F4E5F5052495A45320050524F4D4F5F435245444954530050524F4D4F5F544F5F52454445454D41424C450050524F4D4F5F455850495245440050524F4D4F5F43414E43454C0050524F4D4F5F53544152545F53455353494F4E0050524F4D4F5F454E445F53455353494F4E00434152445F5245504C4143454D454E5400445241575F5449434B45545F5052494E540048414E445041590048414E445041595F43414E43454C4C4154494F4E004D414E55414C5F48414E44504159004D414E55414C5F48414E445041595F43414E43454C4C4154494F4E00434153485F494E5F53504C495432004D425F434153485F494E5F53504C495432004445565F53504C49543200434153485F494E5F53504C495431004445565F53504C495431004D425F434153485F494E5F53504C4954310043414E43454C5F53504C4954310043414E43454C5F53504C49543200504F494E54535F544F5F445241575F5449434B45545F5052494E54005052495A455F434F55504F4E00434152445F4352454154494F4E00434152445F504552534F4E414C495A4154494F4E004143434F554E545F50494E5F4348414E4745004143434F554E545F50494E5F52414E444F4D00434153485F494E5F434F5645525F434F55504F4E00434153485F494E5F4E4F545F52454445454D41424C4532004D425F434153485F494E5F4E4F545F52454445454D41424C45004D425F434153485F494E5F434F5645525F434F55504F4E004D425F434153485F494E5F4E4F545F52454445454D41424C4532004D425F5052495A455F434F55504F4E004E415F434153485F494E5F53504C495431004E415F434153485F494E5F53504C495432004E415F5052495A455F434F55504F4E004E415F434153485F494E5F4E4F545F52454445454D41424C45004E415F434153485F494E5F434F5645525F434F55504F4E00434152445F52454359434C454400504F494E54535F544F5F4E4F545F52454445454D41424C4500504F494E54535F544F5F52454445454D41424C450052454445454D41424C455F4445565F455850495245440052454445454D41424C455F5052495A455F45585049524544004E4F545F52454445454D41424C455F455850495245440050524F4D4F54494F4E5F52454445454D41424C450043414E43454C5F50524F4D4F54494F4E5F52454445454D41424C450050524F4D4F54494F4E5F504F494E540043414E43454C5F50524F4D4F54494F4E5F504F494E54005441585F52455455524E494E475F4F4E5F5052495A455F434F55504F4E00444543494D414C5F524F554E44494E4700534552564943455F434841524745004D425F434153485F4C4F535400524551554553545F544F5F564F55434845525F52455052494E5400434845434B5F5041594D454E5400434153485F50454E44494E475F434C4F53494E47004143434F554E545F424C4F434B4544004143434F554E545F554E424C4F434B45440043555252454E43595F45584348414E47455F53504C4954310043555252454E43595F434152445F45584348414E47455F53504C49543100434153494E4F5F43484950535F45584348414E47455F53504C495431005449544F5F5449434B45545F434153484945525F5052494E5445445F4341534841424C4500504F494E54535F544F5F52454445454D41424C455F41535F43415348494E5F53504C49543100504F494E54535F544F5F52454445454D41424C455F41535F43415348494E5F53504C4954320043555252454E43595F45584348414E47455F53504C4954320043555252454E43595F434152445F45584348414E47455F53504C49543200434153494E4F5F43484950535F45584348414E47455F53504C49543200524551554553545F544F5F5449434B45545F52455052494E540050415254494349504154494F4E5F494E5F434153484445534B5F445241570043555252454E43595F434845434B5F53504C4954310043555252454E43595F434845434B5F53504C495432005052495A455F494E5F4B494E44315F47524F5353005052495A455F494E5F4B494E44315F54415831005052495A455F494E5F4B494E44315F5441583200434C4F53455F53455353494F4E5F42414E4B5F4341524400434C4F53455F53455353494F4E5F434845434B0046494C4C45525F4F55545F434845434B005052495A455F494E5F4B494E44325F47524F5353005052495A455F494E5F4B494E44325F54415831005052495A455F494E5F4B494E44325F54415832005449544F5F5449434B45545F434153484945525F504149445F4341534841424C45005449544F5F5449434B45545F434153484945525F5052494E5445445F50524F4D4F5F52454445454D41424C45005449544F5F5449434B45545F4D414348494E455F504C415945445F4341534841424C45005449544F5F5449434B45545F434153484945525F5052494E5445445F50524F4D4F5F4E4F545F52454445454D41424C45005449544F5F5449434B45545F52454953535545005449544F5F5449434B45545F4352454154455F4341534841424C455F4F46464C494E45005449544F5F5449434B45545F4352454154455F504C415941424C455F4F46464C494E45005449544F5F5449434B45545F455850495245445F50524F4D4F5F52454445454D41424C45005449544F5F5449434B45545F455850495245445F50524F4D4F5F4E4F545F52454445454D41424C45005449544F5F5449434B45545F455850495245445F52454445454D41424C45005449544F5F5449434B45545F4341534841424C455F52454445454D45445F4F46464C494E45005449544F5F5449434B45545F504C415941424C455F52454445454D45445F4F46464C494E45005449544F5F5449434B45545F4D414348494E455F5052494E5445445F48414E44504159005449544F5F5449434B45545F4D414348494E455F5052494E5445445F4A41434B504F54005449544F5F5449434B45545F434153484945525F504149445F48414E44504159005449544F5F5449434B45545F434153484945525F504149445F4A41434B504F54005449544F5F56414C49444154455F5449434B4554005449544F5F5449434B45545F4D414348494E455F5052494E5445445F4341534841424C45005449544F5F5449434B45545F4D414348494E455F5052494E5445445F50524F4D4F5F4E4F545F52454445454D41424C45005449544F5F5449434B45545F4D414348494E455F504C415945445F50524F4D4F5F52454445454D41424C45005449544F5F5449434B45545F4D414348494E455F504C415945445F50524F4D4F5F4E4F545F52454445454D41424C45005449544F5F5449434B45545F434153484945525F504149445F50524F4D4F5F52454445454D41424C45005449544F5F5449434B45545F434153484945525F455850495245445F504149445F4341534841424C45005449544F5F5449434B45545F434153484945525F455850495245445F504149445F50524F4D4F5F52454445454D41424C45005449544F5F5449434B45545F434153484945525F455850495245445F504149445F4A41434B504F54005449544F5F5449434B45545F434153484945525F455850495245445F504149445F48414E44504159005449544F5F4C4153545F4D4F56454D454E54005452414E534645525F4352454449545F4F5554005452414E534645525F4352454449545F494E00434153485F494E5F5441585F53504C49543100504F494E54535F544F5F52454445454D41424C455F41535F43415348494E5F5052495A4500434152445F5245504C4143454D454E545F464F525F4652454500434152445F5245504C4143454D454E545F494E5F504F494E545300434153485F494E5F5441585F53504C4954320043555252454E43595F4352454449545F434152445F45584348414E47455F53504C4954310043555252454E43595F44454249545F434152445F45584348414E47455F53504C4954310043555252454E43595F4352454449545F434152445F45584348414E47455F53504C4954320043555252454E43595F44454249545F434152445F45584348414E47455F53504C49543200434153485F414456414E43455F43555252454E43595F45584348414E474500434153485F414456414E43455F47454E455249435F42414E4B5F4341524400434153485F414456414E43455F4352454449545F42414E4B5F4341524400434153485F414456414E43455F44454249545F42414E4B5F4341524400434153485F414456414E43455F434845434B00434153485F414456414E43455F43415348004E4F545F504C415945445F52454348415247455F524546554E44454400434153485F434C4F53494E475F53484F52545F4445505245434154454400434153485F434C4F53494E475F4F5645525F4445505245434154454400434153485F434C4F53494E475F53484F525400434153485F434C4F53494E475F4F5645520046494C4C45525F494E5F434C4F53494E475F53544F434B0046494C4C45525F4F55545F434C4F53494E475F53544F434B00434147455F4F50454E5F53455353494F4E00434147455F434C4F53455F53455353494F4E00434147455F46494C4C45525F494E00434147455F46494C4C45525F4F555400434147455F434153484945525F434F4C4C4543545F44524F50424F585F5445524D494E414C00434147455F434153484945525F524546494C4C5F484F505045525F5445524D494E414C00434147455F434153484945525F434F4C4C4543545F44524F50424F585F47414D494E475441424C4500434147455F434153484945525F434F4C4C4543545F44524F50424F585F47414D494E475441424C455F44455441494C00434147455F4C4153545F4D4F56454D454E540043484950535F53414C450043484950535F50555243484153450043484950535F53414C455F4445564F4C5554494F4E5F464F525F5449544F0043484950535F53414C455F544F54414C0043484950535F50555243484153455F544F54414C0043484950535F4348414E47455F494E0043484950535F4348414E47455F4F55540043484950535F53414C455F574954485F434153485F494E0043484950535F50555243484153455F574954485F434153485F4F55540043484950535F53414C455F544F54414C5F45584348414E47450043484950535F50555243484153455F544F54414C5F45584348414E47450043484950535F53414C455F52454749535445525F544F54414C0043484950535F4C4153545F4D4F56454D454E540047414D494E475F5441424C455F47414D455F4E455457494E0047414D494E475F5441424C455F47414D455F4E455457494E5F434C4F53450048414E445041595F415554484F52495A45440048414E445041595F554E415554484F52495A45440048414E445041595F564F494445440048414E445041595F564F494445445F554E444F4E45004D425F45584345535300434F4D50414E595F425F43555252454E43595F434152445F45584348414E47455F53504C49543100434F4D50414E595F425F43555252454E43595F434152445F45584348414E47455F53504C49543200434F4D50414E595F425F43555252454E43595F4352454449545F434152445F45584348414E47455F53504C49543100434F4D50414E595F425F43555252454E43595F4352454449545F434152445F45584348414E47455F53504C49543200434F4D50414E595F425F43555252454E43595F44454249545F434152445F45584348414E47455F53504C49543100434F4D50414E595F425F43555252454E43595F44454249545F434152445F45584348414E47455F53504C4954320048414E445041595F57495448484F4C44494E4700434F4D50414E595F425F43555252454E43595F45584348414E47455F53504C49543100434F4D50414E595F425F43555252454E43595F45584348414E47455F53504C49543200434F4D50414E595F425F43555252454E43595F434845434B5F53504C49543100434F4D50414E595F425F43555252454E43595F434845434B5F53504C49543200434F4D50414E595F425F434153485F414456414E43455F43555252454E43595F45584348414E474500434F4D50414E595F425F434153485F414456414E43455F47454E455249435F42414E4B5F4341524400434F4D50414E595F425F434153485F414456414E43455F4352454449545F42414E4B5F4341524400434F4D50414E595F425F434153485F414456414E43455F44454249545F42414E4B5F4341524400434F4D50414E595F425F434153485F414456414E43455F434845434B0057494E5F4C4F53535F53544154454D454E545F5245515545535400434152445F4445504F5349545F494E5F434845434B00434152445F4445504F5349545F494E5F43555252454E43595F45584348414E474500434152445F4445504F5349545F494E5F434152445F43524544495400434152445F4445504F5349545F494E5F434152445F444542495400434152445F4445504F5349545F494E5F434152445F47454E4552494300434152445F5245504C4143454D454E545F434845434B00434152445F5245504C4143454D454E545F434152445F43524544495400434152445F5245504C4143454D454E545F434152445F444542495400434152445F5245504C4143454D454E545F434152445F47454E4552494300434F4D50414E595F425F434152445F4445504F5349545F494E00434F4D50414E595F425F434152445F4445504F5349545F4F555400434F4D50414E595F425F434152445F5245504C4143454D454E5400434F4D50414E595F425F434152445F4445504F5349545F494E5F434845434B00434F4D50414E595F425F434152445F4445504F5349545F494E5F43555252454E43595F45584348414E474500434F4D50414E595F425F434152445F4445504F5349545F494E5F434152445F43524544495400434F4D50414E595F425F434152445F4445504F5349545F494E5F434152445F444542495400434F4D50414E595F425F434152445F4445504F5349545F494E5F434152445F47454E4552494300434F4D50414E595F425F434152445F5245504C4143454D454E545F434845434B00434F4D50414E595F425F434152445F5245504C4143454D454E545F434152445F43524544495400434F4D50414E595F425F434152445F5245504C4143454D454E545F434152445F444542495400434F4D50414E595F425F434152445F5245504C4143454D454E545F434152445F47454E45524943005452414E534645525F43415348494552535F53454E44005452414E534645525F43415348494552535F5245434549564500434153484945525F524546494C4C5F484F505045525F5445524D494E414C005445524D494E414C5F524546494C4C5F484F5050455200434153484945525F4348414E47455F535441434B45525F5445524D494E414C00434153484945525F434F4C4C4543545F44524F50424F585F5445524D494E414C005449544F5F5449434B45545F50524F4D4F424F585F5052494E5445445F50524F4D4F5F4E4F545F52454445454D41424C45005449544F5F5449434B45545F50524F4D4F424F585F5052494E5445445F50524F4D4F5F52454445454D41424C4500434153484945525F434F4C4C4543545F44524F50424F585F5445524D494E414C5F4341474500434153484945525F524546494C4C5F484F505045525F5445524D494E414C5F43414745005052495A455F52455F494E5F4B494E44315F47524F5353005052495A455F52455F494E5F4B494E44325F47524F5353005052495A455F52455F494E5F4B494E44315F54415831005052495A455F52455F494E5F4B494E44315F54415832005052495A455F52455F494E5F4B494E44325F54415831005052495A455F52455F494E5F4B494E44325F5441583200435553544F4D45525F454E5452414E4345005449544F5F5449434B4554535F4352454154455F45584348414E47455F4455414C5F43555252454E4359005449544F5F5449434B4554535F504C415945445F45584348414E47455F4455414C5F43555252454E43590042494C4C5F494E5F45584348414E47455F4455414C5F43555252454E4359005449544F5F5449434B45545F434153484945525F564F49445F4341534841424C45005449544F5F5449434B45545F434153484945525F564F49445F50524F4D4F5F52454445454D41424C45005449544F5F5449434B45545F434153484945525F564F49445F50524F4D4F5F4E4F545F52454445454D41424C45005449544F5F5449434B45545F4D414348494E455F564F49445F4341534841424C45005449544F5F5449434B45545F4D414348494E455F564F49445F50524F4D4F5F4E4F545F52454445454D41424C45005441585F50524F564953494F4E53005449544F5F5449434B45545F434153484945525F535741505F464F525F4348495053005441585F435553544F4459005441585F52455455524E494E475F435553544F445900434152445F4153534F43494154450043555252454E43595F45584348414E47455F544F5F4E4154494F4E414C5F43555252454E43590043555252454E43595F45584348414E47455F414D4F554E545F4F55540043555252454E43595F45584348414E47455F544F5F464F524549474E5F43555252454E43590043555252454E43595F45584348414E47455F414D4F554E545F494E005449544F5F5449434B45545F434F554E54525F5052494E5445445F4341534841424C450043555252454E43595F45584348414E47455F414D4F554E545F494E5F4348414E474500435553544F4D45525F454E5452414E43455F4341524400435553544F4D45525F454E5452414E43455F44454249545F4341524400435553544F4D45525F454E5452414E43455F4352454449545F4341524400435553544F4D45525F454E5452414E43455F434845434B0050524F4D4F54494F4E5F52454445454D41424C455F4645444552414C5F5441580050524F4D4F54494F4E5F52454445454D41424C455F53544154455F544158004332474F5F4C4F434B5F4143434F554E545F494E5445524E414C004332474F5F4C4F434B5F4143434F554E545F45585445524E414C004332474F5F434153485F494E5F494E5445524E414C004332474F5F434153485F494E5F45585445524E414C004332474F5F434153485F4F55545F494E5445524E414C004332474F5F434153485F4F55545F45585445524E414C004332474F5F41435449564154455F4143434F554E545F494E5445524E414C004332474F5F444541435449564154455F4143434F554E545F494E5445524E414C004332474F5F52454E4F4D494E4154494F4E5F4143434F554E545F494E5445524E414C004332474F5F52454E4F4D494E4154494F4E5F4143434F554E545F45585445524E414C004143434F554E545F4144445F424C41434B4C495354004143434F554E545F52454D4F56455F424C41434B4C4953540043555252454E43595F434152445F45584348414E47455F53504C4954315F554E444F0043555252454E43595F434152445F45584348414E47455F53504C4954325F554E444F00434153485F414456414E43455F43555252454E43595F45584348414E47455F554E444F00434153485F414456414E43455F47454E455249435F42414E4B5F434152445F554E444F00434153485F414456414E43455F4352454449545F42414E4B5F434152445F554E444F00434153485F414456414E43455F44454249545F42414E4B5F434152445F554E444F00434152445F4445504F5349545F494E5F434152445F554E444F00434F4D50414E595F425F434152445F4445504F5349545F494E5F434152445F554E444F00534552564943455F4348415247455F4F4E5F434F4D50414E595F41005052495A455F445241575F47524F53535F4F544845520052454F50454E5F43415348494552004D554C5449504C455F4255434B4554535F454E445F53455353494F4E004D554C5449504C455F4255434B4554535F454E445F53455353494F4E5F4C415354004D554C5449504C455F4255434B4554535F45585049524544004D554C5449504C455F4255434B4554535F455850495245445F4C415354004D554C5449504C455F4255434B4554535F4D414E55414C5F414444004D554C5449504C455F4255434B4554535F4D414E55414C5F4144445F4C415354004D554C5449504C455F4255434B4554535F4D414E55414C5F535542004D554C5449504C455F4255434B4554535F4D414E55414C5F5355425F4C415354004D554C5449504C455F4255434B4554535F4D414E55414C5F534554004D554C5449504C455F4255434B4554535F4D414E55414C5F5345545F4C41535400434153484945525F42595F434F4E434550545F494E00434153484945525F42595F434F4E434550545F4F555400434153484945525F42595F434F4E434550545F4C41535400506C61790043617368496E00436173684F7574004465766F6C7574696F6E005461784F6E5072697A65310053746172744361726453657373696F6E00456E644361726453657373696F6E00416374697661746500446561637469766174650050726F6D6F74696F6E4E6F7452656465656D61626C65004465706F736974496E004465706F7369744F75740043616E63656C4E6F7452656465656D61626C65005461784F6E5072697A65320050726F6D6F437265646974730050726F6D6F546F52656465656D61626C650050726F6D6F457870697265640050726F6D6F43616E63656C0050726F6D6F537461727453657373696F6E0050726F6D6F456E6453657373696F6E00437265646974734578706972656400437265646974734E6F7452656465656D61626C654578706972656400436172645265706C6163656D656E7400447261775469636B65745072696E740048616E647061790048616E6470617943616E63656C6C6174696F6E004D616E75616C456E6453657373696F6E0050726F6D6F4D616E75616C456E6453657373696F6E004D616E75616C48616E64706179004D616E75616C48616E6470617943616E63656C6C6174696F6E00506F696E74734177617264656400506F696E7473546F476966745265717565737400506F696E7473546F4E6F7452656465656D61626C6500506F696E74734769667444656C697665727900506F696E74734578706972656400506F696E7473546F447261775469636B65745072696E7400506F696E747347696674536572766963657300486F6C6465724C6576656C4368616E676564005072697A65436F75706F6E00444550524543415445445F52656465656D61626C655370656E7455736564496E50726F6D6F74696F6E7300506F696E7473546F52656465656D61626C65005072697A6545787069726564004361726443726561746564004163636F756E74506572736F6E616C697A6174696F6E004D616E75616C6C794164646564506F696E74734F6E6C79466F7252656465656D004163636F756E7450494E4368616E6765004163636F756E7450494E52616E646F6D00437265646974734E6F7452656465656D61626C6532457870697265640043617368496E436F766572436F75706F6E0043617368496E4E6F7452656465656D61626C65320043616E63656C537461727453657373696F6E004361726452656379636C65640050726F6D6F74696F6E52656465656D61626C650043616E63656C50726F6D6F74696F6E52656465656D61626C650050726F6D6F74696F6E506F696E740043616E63656C50726F6D6F74696F6E506F696E74004D616E75616C486F6C6465724C6576656C4368616E6765640054617852657475726E696E674F6E5072697A65436F75706F6E00446563696D616C526F756E64696E6700536572766963654368617267650043616E63656C47696674496E7374616E636500506F696E74735374617475734368616E676564004D616E75616C6C794164646564506F696E7473466F724C6576656C00496D706F727465644163636F756E7400496D706F72746564506F696E74734F6E6C79466F7252656465656D00496D706F72746564506F696E7473466F724C6576656C00496D706F72746564506F696E7473486973746F7279004163636F756E74426C6F636B6564004163636F756E74556E626C6F636B65640043616E63656C6C6174696F6E005472616E736665724372656469744F7574005472616E73666572437265646974496E0043617368496E54617800436172645265706C6163656D656E74466F724672656500436172645265706C6163656D656E74496E506F696E74730043617368416476616E63650045787465726E616C53797374656D506F696E7473537562737472616374004D756C74695369746543757272656E744C6F63616C506F696E7473005449544F5F5469636B6574436173686965725072696E7465644361736861626C65005449544F5F5469636B657443617368696572506169644361736861626C65005449544F5F5469636B6574436173686965725072696E74656450726F6D6F52656465656D61626C65005449544F5F5469636B65744D616368696E65506C617965644361736861626C65005449544F5F5469636B657452656973737565005449544F5F4163636F756E74546F5465726D696E616C437265646974005449544F5F5465726D696E616C546F4163636F756E74437265646974005449544F5F5469636B65744F66666C696E65005449544F5F5469636B6574436173686965725072696E74656450726F6D6F4E6F7452656465656D61626C65005449544F5F5469636B65744D616368696E655072696E74656448616E64706179005449544F5F5469636B65744D616368696E655072696E7465644A61636B706F74005449544F5F5469636B6574436173686965725061696448616E64706179005449544F5F5469636B657443617368696572506169644A61636B706F74005449544F5F5469636B65744D616368696E655072696E7465644361736861626C65005449544F5F5469636B65744D616368696E655072696E74656450726F6D6F4E6F7452656465656D61626C65005449544F5F5469636B6574436173686965725061696450726F6D6F52656465656D61626C65005449544F5F5469636B65744D616368696E65506C6179656450726F6D6F52656465656D61626C65005449544F5F7469636B65744D616368696E65506C6179656450726F6D6F4E6F7452656465656D61626C65005449544F5F5469636B65744361736869657245787069726564506169644361736861626C65005449544F5F5469636B657443617368696572457870697265645061696450726F6D6F52656465656D61626C65005449544F5F5469636B65744361736869657245787069726564506169644A61636B706F74005449544F5F5469636B657443617368696572457870697265645061696448616E6470617900436173686465736B4472617750617274696369706174696F6E00436173686465736B44726177506C617953657373696F6E00436869707353616C654465766F6C7574696F6E466F725469746F00436869707353616C65546F74616C0043686970735075726368617365546F74616C0057696E4C6F737353746174656D656E745265717565737400436172644465706F736974496E436865636B00436172644465706F736974496E43757272656E637945786368616E676500436172644465706F736974496E4361726443726564697400436172644465706F736974496E43617264446562697400436172644465706F736974496E4361726447656E6572696300436172645265706C6163656D656E74436865636B00436172645265706C6163656D656E744361726443726564697400436172645265706C6163656D656E7443617264446562697400436172645265706C6163656D656E744361726447656E657269630047616D65476174657761794265740047616D65476174657761795072697A65005449544F5F5469636B657450726F6D6F426F785072696E74656450726F6D6F4E6F7452656465656D61626C65005449544F5F5469636B657450726F6D6F426F785072696E74656450726F6D6F52656465656D61626C650047616D6547617465776179526573657276654372656469740047616D6547617465776179426574526F6C6C6261636B0054617850726F766973696F6E7300546178437573746F64790054617852657475726E437573746F64790043757272656E637945786368616E6765436173686965724F7574005449544F5F5469636B6574436F756E74525072696E7465644361736861626C6500436172644173736F63696174650043757272656E637945786368616E676543617368696572496E0050726F6D6F74696F6E52656465656D61626C654665646572616C5461780050726F6D6F74696F6E52656465656D61626C655374617465546178004332474F43617368496E496E7465726E616C004332474F43617368496E45787465726E616C004332474F436173684F7574496E7465726E616C004332474F436173684F757445787465726E616C004332474F41637469766174654163636F756E74496E7465726E616C004332474F446561637469766174654163636F756E74496E7465726E616C004332474F4C6F636B4163636F756E74496E7465726E616C004332474F4C6F636B4163636F756E7445787465726E616C004332474F52656E6F6D696E6174696F6E4163636F756E74496E7465726E616C004332474F52656E6F6D696E6174696F6E4163636F756E7445787465726E616C004163636F756E74496E426C61636B6C697374004163636F756E744E6F74496E426C61636B6C6973740050617269506C61794265740050617269506C61795072697A650050617269506C6179426574526F6C6C6261636B0041646D697373696F6E0048494444454E5F52656368617267654E6F74446F6E655769746843617368004D554C5449504C455F4255434B4554535F456E6453657373696F6E004D554C5449504C455F4255434B4554535F456E6453657373696F6E4C617374004D554C5449504C455F4255434B4554535F45787069726564004D554C5449504C455F4255434B4554535F457870697265644C617374004D554C5449504C455F4255434B4554535F4D616E75616C5F416464004D554C5449504C455F4255434B4554535F4D616E75616C5F4164645F4C617374004D554C5449504C455F4255434B4554535F4D616E75616C5F537562004D554C5449504C455F4255434B4554535F4D616E75616C5F5375625F4C617374004D554C5449504C455F4255434B4554535F4D616E75616C5F536574004D554C5449504C455F4255434B4554535F4D616E75616C5F5365745F4C6173740057696E706F7446616B6550726F766973696F6E00434144494C4C41435F4A41434B0057494E00414C455349530048414E445041595F43414E43454C4C45445F435245444954530048414E445041595F4A41434B504F540048414E445041595F4F525048414E5F435245444954530048414E445041595F4D414E55414C0048414E445041595F534954455F4A41434B504F540044524157005445524D494E414C5F445241570050524F47524553534956455F50524F564953494F4E0050524F47524553534956455F524553455256450047414D45474154455741590050415249504C4159004143434F554E545F554E4B4E4F574E004143434F554E545F434144494C4C41435F4A41434B004143434F554E545F57494E004143434F554E545F414C45534953004143434F554E545F5649525455414C5F43415348494552004143434F554E545F5649525455414C5F5445524D494E414C004F70656E656400436C6F736564004162616E646F6E6564004D616E75616C436C6F7365640043616E63656C6C65640048616E647061795061796D656E740048616E6470617943616E63656C5061796D656E74004472617757696E6E657200447261774C6F7365720050726F6772657373697665526573657276650050726F677265737369766550726F766973696F6E004177617900556E6B6E6F776E00536C6F77004D656469756D004661737400536F66740041766572616765004861726400496E697469616C00454C5030315F53706C69745F50535F52656464656D5F4E6F52656465656D004361676500436173686965720046524F4D5F43415348494552004D41585F42414C414E4345004142414E444F4E45445F434152440046524F4D5F475549004D41585F50494E5F4641494C5552455300494D504F52540045585445524E414C5F53595354454D00484F4C4445525F4C4556454C5F444F574E475241444500484F4C4445525F4C4556454C5F555047524144450045585445524E414C5F53595354454D5F43414E43454C45440045585445524E414C5F414D4C00494E54525553494F4E00494E4143544956455F504C41595F53455353494F4E004332474F5F494E5445524E414C004332474F5F45585445524E414C00424C41434B4C495354004755490043415348494552004332474F00524543455054494F4E5F424C41434B4C495354005341535F484F5354005349544500534954455F4A41434B504F54004D4F42494C455F42414E4B004D4F42494C455F42414E4B5F494D42004953544154530050524F4D4F424F5800434153484445534B5F445241570047414D494E475F5441424C455F53454154004F46464C494E4500434153484445534B5F445241575F5441424C450057494E5F55500043414E43454C4C45445F43524544495453004A41434B504F54004F525048414E5F43524544495453004D414E55414C005350454349414C5F50524F4752455353495645004D414E55414C5F43414E43454C4C45445F43524544495453004D414E55414C5F4A41434B504F54004D414E55414C5F4F5448455253004F55545F4F465F5345525649434500524554495245440052455345545F4F4E5F52454445454D005345545F4F4E5F46495253545F4E525F50524F4D4F54494F4E005550444154455F4F4E5F5245434841524745005550444154455F4F4E5F52454445454D0052455345545F4F4E5F5245434841524745004D554C54495349544500535550455243454E54455200434F4E4E454354454400444953434F4E4E454354454400446F776E6C6F6164436F6E66696775726174696F6E0055706C6F6164506F696E74734D6F76656D656E74730055706C6F6164506572736F6E616C496E666F0055706C6F61644163636F756E74446F63756D656E747300446F776E6C6F6164506F696E74735F5369746500446F776E6C6F6164506F696E74735F416C6C00446F776E6C6F6164506572736F6E616C496E666F5F4361726400446F776E6C6F6164506572736F6E616C496E666F5F5369746500446F776E6C6F6164506572736F6E616C496E666F5F416C6C00446F776E6C6F616450726F76696465727347616D65730055706C6F616450726F76696465727347616D657300446F776E6C6F616450726F76696465727300446F776E6C6F61644163636F756E74446F63756D656E74730055706C6F616453616C6573506572486F75720055706C6F6164506C617953657373696F6E730055706C6F616450726F7669646572730055706C6F616442616E6B730055706C6F616441726561730055706C6F61645465726D696E616C730055706C6F61645465726D696E616C73436F6E6E65637465640055706C6F616447616D65506C617953657373696F6E7300446F776E6C6F61644D617374657250726F66696C657300446F776E6C6F6164436F72706F7261746555736572730055706C6F61644C61737441637469766974790055706C6F6164436173686965724D6F76656D656E74734772757065644279486F75720055706C6F6164416C61726D7300446F776E6C6F6164416C61726D5061747465726E7300446F776E6C6F61644C63644D657373616765730055706C6F616448616E64706179730055706C6F616453656C6C73416E644275797300526563657074696F6E456E74727900446F776E6C6F61644275636B657473436F6E6669670055706C6F61644C696E6B656445787465726E616C4163636F756E747300446F776E6C6F616445787465726E616C4163636F756E74735F5369746500446F776E6C6F61644C696E6B656445787465726E616C4163636F756E747300446F776E6C6F61644C696E6B656445787465726E616C4163636F756E74735F5369746500446F776E6C6F61644C696E6B656445787465726E616C4163636F756E74735F416C6C004E6F7453657400566F75636865727353706C69744100566F75636865727353706C69744200566F75636865727353706C69744243616E63656C004163636F756E74496400547261636B4163636F756E744368616E67657300547261636B506F696E744368616E676573004163636F756E744D6F76656D656E74730050726F76696465727347616D65730050726F766964657273004163636F756E74446F63756D656E747300436173684465736B44726177004C43444D65737361676500566F7563686572456E7472616E63657300566F756368657254617850726F766973696F6E7300566F7563686572546178437573746F6479004F70656E436C6F73654C6F6F70536974655265717565737473004C696E6B656445787465726E616C4163636F756E747300566F756368657250617274696369706174696F6E4472617700566F756368657253756D6D61727943726564697473005449544F5F56616C69646174696F6E4E756D626572005465726D696E616C4D6574657273486973746F72794576656E74730050696E5061645472616E73616374696F6E00417564697449640050617279506C61790043616765436F6E63657074730043616765436F6E63657074735F4C617374004E6F74446F776E6C6F616400446F776E6C6F61645F55706461746500446F776E6C6F61645F5570646174655072696F726974794D756C74697369746500446F776E6C6F61645F5570646174655072696F7269747953697465004F50454E00434C4F534544004F50454E5F50454E44494E470050454E44494E475F434C4F53494E4700434845434B0042414E4B5F43415244004352454449545F434152440044454249545F434152440043555252454E4359004341524400434153494E4F4348495000434153494E4F5F434849505F524500434153494E4F5F434849505F4E524500434153494E4F5F434849505F434F4C4F520046524545005449434B4554004341524432474F00434153485F414456414E43450052454348415247450052454348415247455F434152445F4352454154494F4E004E4F545F5345540050524F4D4F54494F4E004D425F50524F4D4F54494F4E00474946545F5245515545535400474946545F44454C495645525900445241575F5449434B455400474946545F445241575F5449434B455400474946545F52454445454D41424C455F41535F43415348494E0043484950535F53414C455F574954485F5245434841524745004D425F4445504F53495400434153485F4445504F53495400434153485F5749544844524157414C0052454445454D41424C455F435245444954535F45585049524544004E4F545F52454445454D41424C455F435245444954535F4558504952454400504F494E54535F4558504952454400484F4C4445525F4C4556454C5F4348414E474544004143434F554E545F4352454154494F4E004143434F554E545F504552534F4E414C495A4154494F4E004143434F554E545F50494E5F4348414E474544004E4F545F52454445454D41424C45325F435245444954535F45585049524544004E415F434153485F494E004E415F50524F4D4F54494F4E0043414E43454C5F474946545F494E5354414E43450050524F4D4F424F585F544F54414C5F5245434841524745535F5052494E540048414E445041595F56414C49444154494F4E00494D504F52545F504F494E5453005449544F5F4F46464C494E45005449544F5F52454953535545005449544F5F5449434B45545F56414C49444154494F4E00494D504F52545F47414D494E475F5441424C455F504C41595F53455353494F4E5300534146455F4B454550494E475F4445504F53495400534146455F4B454550494E475F5749544844524157005452414E534645525F434153484945525F53455353494F4E535F5749544844524157414C005452414E534645525F434153484945525F53455353494F4E535F4445504F534954005445524D494E414C5F434F4C4C4543545F44524F50424F580047414D45474154455741595F524553455256455F435245444954005445524D494E414C5F4348414E47455F535441434B45525F52455155455354004D554C5449504C455F4255434B4554535F454E4453455353494F4E004D554C5449504C455F4255434B4554535F4D414E55414C00434153484945525F524546494C4C5F484F5050455200434153484945525F524546494C4C5F484F505045525F4341474500434153484945525F434F4C4C4543545F524546494C4C00434153484945525F434F4C4C4543545F524546494C4C5F43414745005449544F5F564F49445F4D414348494E455F5449434B45540043484950535F53574150005052495A455F5041594F5554005052495A455F5041594F55545F414E445F52454348415247450043555252454E43595F45584348414E47455F4348414E47450043555252454E43595F45584348414E47455F4445564F4C5554494F4E0050524F4D4F54494F4E5F574954485F544158455300434C4F53455F4F5045524154494F4E0052454F50454E5F4F5045524154494F4E0048414E445041595F4155544F4D41544943414C4C595F5041594D454E5400434147455F434F4E434550545300434147455F434F4E43455054535F4C4153540043617368496E5F410043617368496E5F4200436173684F75745F410043616E63656C5F4200536572766963654368617267655F4200436173684465736B447261775F57696E6E6572005469636B65744F757400436869707353616C650043686970734275790043686970734368616E676500436869707353616C654465616C6572436F707900436173684F70656E696E670043617368436C6F73696E67004D4253616C65734C696D69744368616E6765004D424465706F736974004D42436C6F73696E6700436F6D6D697373696F6E5F4200436865636B436F6D6D697373696F6E4200436173684465736B447261775F4C6F7365720045786368616E6765436F6D6D697373696F6E4200436173684465706F73697400436173685769746864726177616C00436172644465706F736974496E5F4200436172645265706C6163656D656E745F4200436172644465706F7369744F75745F4200436173684465736B436C6F7365556E62616C616E63656400436173686965725472616E73666572436173684465706F73697400436173686965725472616E73666572436173685769746864726177616C005465726D696E616C73546F436F6C6C656374005465726D696E616C73546F526566696C6C00437573746F6D6572456E7472616E63650043686970735377617000436173685769746864726177616C4361726400546F74616C697A696E6753747269700050617274696369706174696F6E447261770053756D6D61727943726564697473004E6F6E6500556E646F6E6500556E646F4F7065726174696F6E004143434F554E540057495448484F4C44494E4700496E7465726D6564696174655365727665720047616D696E675465726D696E616C006D5F696E5F616D6F756E74006D5F696E5F69736F5F636F6465006D5F6F75745F69736F5F636F6465006D5F67726F73735F616D6F756E74006D5F636F6D697373696F6E006D5F6E65745F616D6F756E745F73706C697431006D5F6E65745F616D6F756E745F73706C697432006D5F4E52325F616D6F756E74006D5F636172645F7072696365006D5F636172645F636F6D697373696F6E73006D5F6368616E67655F72617465006D5F6E756D5F646563696D616C73006D5F7761735F666F7265696E675F63757272656E6379006D5F6172655F70726F6D6F74696F6E735F656E61626C6564006D5F696E5F74797065006D5F6163636F756E745F70726F6D6F74696F6E5F6964006D5F62616E6B5F7472616E73616374696F6E5F64617461006D5F6E65745F616D6F756E745F6465766F6C7574696F6E006D5F73756274797065006765745F496E416D6F756E74007365745F496E416D6F756E74006765745F4163636F756E7450726F6D6F74696F6E4964007365745F4163636F756E7450726F6D6F74696F6E4964006765745F496E43757272656E6379436F6465007365745F496E43757272656E6379436F6465006765745F4F757443757272656E6379436F6465007365745F4F757443757272656E6379436F6465006765745F4368616E676552617465007365745F4368616E676552617465006765745F446563696D616C73007365745F446563696D616C73006765745F576173466F7265696E6743757272656E6379007365745F576173466F7265696E6743757272656E6379006765745F47726F7373416D6F756E74007365745F47726F7373416D6F756E74006765745F436F6D697373696F6E007365745F436F6D697373696F6E006765745F4E6574416D6F756E74006765745F4E6574416D6F756E7453706C697431007365745F4E6574416D6F756E7453706C697431006765745F4E6574416D6F756E7453706C697432007365745F4E6574416D6F756E7453706C697432006765745F4E5232416D6F756E74007365745F4E5232416D6F756E74006765745F436172645072696365007365745F436172645072696365006765745F43617264436F6D697373696F6E73007365745F43617264436F6D697373696F6E73006765745F41726550726F6D6F74696F6E73456E61626C6564007365745F41726550726F6D6F74696F6E73456E61626C6564006765745F496E54797065007365745F496E54797065006765745F53756254797065007365745F53756254797065006765745F42616E6B5472616E73616374696F6E44617461007365745F42616E6B5472616E73616374696F6E44617461006765745F4E6574416D6F756E744465766F6C7574696F6E007365745F4E6574416D6F756E744465766F6C7574696F6E00496E416D6F756E74004163636F756E7450726F6D6F74696F6E496400496E43757272656E6379436F6465004F757443757272656E6379436F6465004368616E67655261746500446563696D616C7300576173466F7265696E6743757272656E63790047726F7373416D6F756E7400436F6D697373696F6E004E6574416D6F756E74004E6574416D6F756E7453706C697431004E6574416D6F756E7453706C697432004E5232416D6F756E74004361726450726963650043617264436F6D697373696F6E730041726550726F6D6F74696F6E73456E61626C656400496E547970650053756254797065004E6574416D6F756E744465766F6C7574696F6E006D5F6F7065726174696F6E5F6964006D5F74797065006D5F646F63756D656E745F6E756D626572006D5F62616E6B5F6E616D65006D5F686F6C6465725F6E616D65006D5F62616E6B5F636F756E747279006D5F636172645F747261636B5F64617461006D5F63757272656E63795F636F6465006D5F636172645F65787069726174696F6E5F64617465006D5F636172645F646174615F656469746564006D5F636865636B5F726F7574696E675F6E756D626572006D5F636865636B5F6163636F756E745F6E756D626572006D5F636F6D6D656E74006D5F636865636B5F6461746574696D65006D5F7472616E73616374696F6E5F74797065006D5F6163636F756E745F686F6C6465725F6E616D65006765745F4F7065726174696F6E4964007365745F4F7065726174696F6E4964006765745F54797065007365745F54797065006765745F446F63756D656E744E756D626572007365745F446F63756D656E744E756D626572006765745F42616E6B4E616D65007365745F42616E6B4E616D65006765745F486F6C6465724E616D65007365745F486F6C6465724E616D65006765745F42616E6B436F756E747279007365745F42616E6B436F756E747279006765745F547261636B44617461007365745F547261636B44617461006765745F696E416D6F756E74007365745F696E416D6F756E74006765745F43757272656E6379436F6465007365745F43757272656E6379436F6465006765745F45787069726174696F6E44617465007365745F45787069726174696F6E44617465006765745F526F7574696E674E756D626572007365745F526F7574696E674E756D626572006765745F4163636F756E744E756D626572007365745F4163636F756E744E756D626572006765745F43617264456469746564007365745F43617264456469746564006765745F436F6D6D656E7473007365745F436F6D6D656E7473006765745F436865636B44617465007365745F436865636B44617465006765745F5472616E73616374696F6E54797065007365745F5472616E73616374696F6E54797065006765745F4163636F756E74486F6C6465724E616D65007365745F4163636F756E74486F6C6465724E616D65004F7065726174696F6E4964005479706500446F63756D656E744E756D6265720042616E6B4E616D6500486F6C6465724E616D650042616E6B436F756E74727900696E416D6F756E740043757272656E6379436F64650045787069726174696F6E4461746500526F7574696E674E756D626572004163636F756E744E756D626572004361726445646974656400436F6D6D656E747300436865636B44617465004163636F756E74486F6C6465724E616D6500414C4C0047524F55500050524F56005A4F4E4500415245410042414E4B005445524D0051554552590050524F4D4F54494F4E5F52455354524943544544005041545445524E530050524F4752455353495645004C43445F4D4553534147450050524F4D4F54494F4E5F504C415945440047414D455F47415445574159004255434B4554535F4D554C5449504C4945520044656661756C7400596573004E6F00426C6F636B4F6E456E74657200426C6F636B4F6E4C6561766500426F74680043616C63756C61746564416E645265776172640050656E64696E67004D616E75616C004D616E75616C43616C63756C61746564004E65757472616C004368696E65736500437A65636800456E676C697368005370616E697368004974616C69616E004B6F7265616E005275737369616E00434153484C455353005449544F00574153530047414D494E4748414C4C004D49434F3200434F4E5345435554495645004E4F5F434F4E53454355544956455F574954485F4F52444552004E4F5F4F52444552004E4F5F4F524445525F574954485F52455045544954494F4E00416E6F6E796D6F7573576974684361726400437573746F6D697A656400416E6F6E796D6F7573576974686F75744361726400436865636B0042616E6B436172640042696C6C00436F696E004F7468657273004368697073526564696D69626C650043686970734E6F526564696D69626C65004368697073436F6C6F7200466F726569676E004E6174696F6E616C004E6F7468696E67004E657743757272656E6379004E6577536974654E6174696F6E616C43757272656E6379004E657753697465466F726569676E43757272656E6379006D5F636F6E636570745F6964006D5F7175616E74697479006D5F616D6F756E74006D5F69736F5F636F6465006D5F63757272656E63795F74797065006765745F5175616E74697479007365745F5175616E74697479006765745F416D6F756E74007365745F416D6F756E74006765745F49736F436F6465007365745F49736F436F6465006765745F43757272656E637954797065007365745F43757272656E637954797065006765745F436F6E636570744964007365745F436F6E636570744964006765745F436F6D6D656E74007365745F436F6D6D656E74005175616E7469747900416D6F756E740049736F436F646500436F6E63657074496400436F6D6D656E740057494E5053410053656E7400526563656976656400426F7800486F7070657200496E004F75740050756E746F7343616E6A650050756E746F734E6976656C004E5200524500436F6D703143616E6A6500436F6D70324E5200436F6D703352450048415050595F42495254484441595F544F4441590048415050595F42495254484441595F494E434F4D4D494E470044554D4D590042414E4F525445004100420043004400526564656D7074696F6E506F696E74730052616E6B696E674C6576656C506F696E7473004372656469745F4E52004372656469745F524500436F6D70315F526564656D7074696F6E506F696E747300436F6D70325F4372656469745F4E5200436F6D70335F4372656469745F52450052616E6B696E674C6576656C506F696E74735F47656E6572617465640052616E6B696E674C6576656C506F696E74735F44697363726574696F6E616C005072697A655F31005072697A655F32005072697A655F33005072697A655F34005072697A655F350048414E445041595F5649525455414C5F504C41595F53455353494F4E5F4944005472785F4163636F756E7450726F6D6F74696F6E73416374696F6E005472785F57637053746172744361726453657373696F6E005472785F436F6D6D6F6E53746172744361726453657373696F6E005472785F4F6E53746172744361726453657373696F6E005472785F47657445786368616E6765005472785F4C696E6B4163636F756E745465726D696E616C005472785F556E6C696E6B4163636F756E745465726D696E616C005472785F436865636B5465726D696E616C4C696E6B005472785F476574506C617953657373696F6E4964005472785F496E73657274506C617953657373696F6E005472785F55706461746550734163636F756E744964005472785F48616E64706179496E73657274506C617953657373696F6E005472785F557064617465506C617953657373696F6E506C61796564576F6E005472785F4F6E456E644361726453657373696F6E005472785F557064617465466F756E64416E6452656D61696E696E67496E45676D0049734163636F756E74416E6F6E796D6F75734F725669727475616C005472785F506C617953657373696F6E436C6F7365005472785F41646442616C616E636573546F4163636F756E740054696D655370616E005472785F506C617953657373696F6E546F50656E64696E67005472785F536974654A61636B706F74436F6E747269627574696F6E005472785F506C617953657373696F6E5365744D6574657273005472785F506C617953657373696F6E5449544F5365744D6574657273005472785F526573657443616E63656C6C61626C654F7065726174696F6E005472785F52657365744163636F756E74496E53657373696F6E005472785F556E6C696E6B50726F6D6F74696F6E73496E53657373696F6E005472785F50726F6D6F5265636F6D7075746557697468686F6C644F6E5265636861726765005472785F50726F6D6F4D61726B4173457868617573746564005472785F5449544F5F55706461746550726F6D6F4E52436F7374005472785F5570646174654163636F756E7450726F6D6F74696F6E436F7374005472785F476574506C617961626C6542616C616E6365005472785F47657447616D65476174657761795265736572766564456E61626C6564005472785F557064617465506C61796564416E64576F6E005472785F47657443726564697473506C61794D6F6465005472785F47657444656C7461506C61796564576F6E005472785F47657444656C7461506C61796564576F6E334753005449544F5F47657444656C7461506C61796564576F6E005472785F5570646174654163636F756E74506F696E7473005472785F5570646174654163636F756E7442616C616E6365005472785F5365744163746976697479005472785F5365744163636F756E74426C6F636B6564005472785F43616E63656C53746172744361726453657373696F6E005472785F5570646174654163636F756E74496E53657373696F6E005472785F42616C616E6365546F506C617953657373696F6E00444550524543415445445F5472785F436F6D70757465576F6E506F696E7473005472785F47657445787465726E616C4C6F79616C747950726F6772616D4D6F646500436865636B4269727468646179416C61726D005265676973746572005472785F5449544F5F5570646174654163636F756E7450726F6D6F74696F6E42616C616E63650044425F5570646174655465726D696E616C4163636F756E7450726F6D6F74696F6E005472785F5449544F5F5472616E7366657246726F6D4163636F756E745F43616E63656C53746172744361726453657373696F6E005472785F47616D65476174657761795F526573657276655F437265646974005472785F47657446424D5265736572766564437265646974456E61626C656400576F6E50726F6D6F4E6F7452656465656D61626C6500576F6E52656465656D61626C6500506C617950726F6D6F4E6F7452656465656D61626C6542616C616E6365005370656E7452656D61696E696E67496E53657373696F6E50726F6D6F4E52005370656E7452656D61696E696E67496E53657373696F6E50726F6D6F524500506C617950726F6D6F5072697A654F6E6C7900506C617952656465656D61626C65005472785F506C617953657373696F6E4368616E6765537461747573005472785F506C617953657373696F6E53657446696E616C42616C616E6365005472785F506C617953657373696F6E4D61726B41734162616E646F6E6564005472785F52656C65617365496E616374697665506C617953657373696F6E005472785F476574506C617953657373696F6E5449544F53657373696F6E4D6574657273005472785F4163636F756E7450726F6D6F74696F6E436F7374005472785F496E736572744167726F757047616D65506C617953657373696F6E00496E73657274576370436F6D6D616E64005472785F506C617953657373696F6E546F526576697365005265616447656E6572616C506172616D73004F6B004572726F72004163636F756E74556E6B6E6F776E004163636F756E74496E53657373696F6E005465726D696E616C556E6B6E6F776E005465726D696E616C426C6F636B6564005061727469616C00416C6C00496E636C7564655265736572766564004D69636F32005472616E73616374696F6E49640049735469746F4D6F6465005472616E736665724D6F6465004163636F756E7442616C616E6365546F5472616E73666572005472616E73666572576974684275636B65747300506C617953657373696F6E49640049734E6577506C617953657373696F6E00496E6942616C616E636500506C617961626C6542616C616E63650046696E42616C616E636500546F74616C546F474D42616C616E636500506F696E747300537461747573436F6465004572726F724D7367005365744572726F72005761726E696E670053657453756363657373006765745F4D657373616765004D65737361676500436173686965724E616D650046696E616C42616C616E636553656C656374656442795573657241667465724D69736D617463680042616C616E636546726F6D474D004861734D6574657273004D65746572730049735449544F005669727475616C4163636F756E744964005469746F53657373696F6E4D657465727300436C6F736553657373696F6E436F6D6D656E740049734D69636F320042616C616E6365546F4163636F756E740044656C746142616C616E6365466F756E64496E53657373696F6E0052656D61696E696E67496E45676D00466F756E64496E45676D00506C61796564436F756E7400506C61796564416D6F756E7400576F6E436F756E7400576F6E416D6F756E74004A61636B706F74416D6F756E740042696C6C496E416D6F756E740048616E6470617973416D6F756E74005469636B6574496E4361736861626C65005469636B6574496E50726F6D6F4E72005469636B6574496E50726F6D6F5265005469636B65744F75744361736861626C65005469636B65744F757450726F6D6F4E72006765745F52656465656D61626C6543617368496E006765745F50726F6D6F526543617368496E006765745F50726F6D6F4E7243617368496E006765745F546F74616C43617368496E006765745F52656465656D61626C65436173684F7574006765745F50726F6D6F4E72436173684F7574006765745F546F74616C436173684F75740052656465656D61626C6543617368496E0050726F6D6F526543617368496E0050726F6D6F4E7243617368496E00546F74616C43617368496E0052656465656D61626C65436173684F75740050726F6D6F4E72436173684F757400546F74616C436173684F75740043617368496E436F696E730043617368496E42696C6C730043617368496E546F74616C004572726F72526574727900466174616C4572726F72004E6F744F70656E65640042616C616E63654D69736D6174636800496E76616C6964506C617953657373696F6E00506C617953657373696F6E46696E616C42616C616E6365004163636F756E7450726F6D6F74696F6E0044656C7461506C617965640044656C7461576F6E0044656C7461506C6179656452650044656C7461506C617965644E720044656C7461576F6E52650044656C7461576F6E4E720042616C616E636500506C6179656400576F6E0042616C616E636546726F6D474D546F416464004E756D506C61796564004E756D576F6E0048616E647061797343656E74730052656465656D61626C650050726F6D6F52656465656D61626C650050726F6D6F4E6F7452656465656D61626C65005265736572766564005265736572766564446973636F756E746564004D6F64655265736572766564004D6F646552657365727665644368616E676564004765744F6E6C795265736572766564004275636B65744E52437265646974004275636B6574524543726564697400437573746F647950726F766973696F6E00546F537472696E67006765745F546F74616C52656465656D61626C65006765745F546F74616C4E6F7452656465656D61626C65006765745F546F74616C42616C616E6365006765745F5A65726F006765745F42616C616E636545474D006765745F42616C616E636545474D526573657276656446424D006F705F4164646974696F6E006F705F5375627472616374696F6E006F705F457175616C697479006F705F496E657175616C69747900457175616C730047657448617368436F6465004E656761746500436C6F6E6500546F74616C52656465656D61626C6500546F74616C4E6F7452656465656D61626C6500546F74616C42616C616E6365005A65726F0042616C616E636545474D0042616C616E636545474D526573657276656446424D00426C6F636B656400426C6F636B526561736F6E00486F6C6465724C6576656C00486F6C64657247656E646572004163547970650043616E63656C6C61626C654F7065726174696F6E49640043757272656E74506C617953657373696F6E49640043757272656E745465726D696E616C49640043757272656E74506C617953657373696F6E5374617475730043757272656E74506C617953657373696F6E42616C616E63654D69736D617463680043757272656E74506C617953657373696F6E496E697469616C42616C616E63650043757272656E74506C617953657373696F6E43617368496E416D6F756E740043616E63656C6C61626C6554727849640043616E63656C6C61626C6542616C616E63650046696E616C42616C616E63650046696E616C506F696E7473004572726F724D6573736167650043616E63656C4279506C61796572004E6F7452656465656D61626C65546F52656465656D61626C6500444F5F4E4F5448494E470053454E445F4249525448444154455F414C41524D0053454E445F4249525448444154455F49535F4E454152005245504F52545F52455345525645445F4352454449540043414E43454C5F52455345525645445F435245444954004D4F444946595F52455345525645445F435245444954005245504F52545F4745545F43524544495400524551554553545F545950455F524553504F4E53455F4F4B00524551554553545F545950455F524553504F4E53455F4552524F5200524551554553545F545950455F524553504F4E53455F4E4F545F454E4F5547485F4352454449540047414D455F474154455741595F52455345525645445F4D4F44455F574954484F55545F4255434B4554530047414D455F474154455741595F52455345525645445F4D4F44455F574954485F4255434B4554530047414D455F474154455741595F52455345525645445F4D4F44455F4D495845440047414D455F474154455741595F50524F4D4F5F42414C414E43455F414354494F4E5F4E4F4E450047414D455F474154455741595F50524F4D4F5F42414C414E43455F414354494F4E5F434F4C4C4543545F5052495A450047414D455F474154455741595F50524F4D4F5F42414C414E43455F414354494F4E5F574954484F55545F50494E00537461727453657373696F6E005265537461727453657373696F6E00456E6453657373696F6E00537461727453657373696F6E43616E63656C6C6564006D5F70726F76696465720044617461536574006D5F6473006765745F4C69737454797065007365745F4C69737454797065004175646974537472696E670041646450726F76696465720052656D6F766550726F766964657200436F6E7461696E7300546F586D6C0046726F6D586D6C00496E6974005365656B50726F766964657200586D6C57726974654D6F64650044617461536574546F586D6C00586D6C526561644D6F6465004461746153657446726F6D586D6C004C69737454797065004C6973746564004E6F744C6973746564005472785F49735433535465726D696E616C005472785F4765745465726D696E616C496E666F005472785F496E7365727450656E64696E675465726D696E616C005472785F4765745465726D696E616C496E666F427945787465726E616C4964004765745465726D696E616C446973706C61794E616D6500534153466C6167416374697665640053716C436F6D6D616E640047657447616D654D6574657273557064617465436F6D6D616E6400436C6F7365416C6C4275744E657765724F70656E4361736869657253657373696F6E73005472785F4765745465726D696E616C54797065004E6F7452656465656D61626C654C6173740050726F6D6F74696F6E616C734669727374005361734D616368696E650050726F764964005465726D696E616C54797065004E616D650050726F76696465724E616D650053746174757300506C61794D6F64650043757272656E744163636F756E74496400534153466C616773005076506F696E74734D756C7469706C696572005076536974654A61636B706F740050764F6E6C7952656465656D61626C6500534153466C61674163746976617465640053797374656D4964004D616368696E65496400416C6C6F774361736861626C655469636B65744F757400416C6C6F7750726F6D6F74696F6E616C5469636B65744F757400416C6C6F775469636B6574496E0045787069726174696F6E5469636B6574734361736861626C650045787069726174696F6E5469636B65747350726F6D6F74696F6E616C005469746F5469636B65744C6F63616C697A6174696F6E005469746F5469636B6574416464726573735F31005469746F5469636B6574416464726573735F32005469746F5469636B65745469746C6552657374726963746564005469746F5469636B65745469746C654465626974005469636B6574734D61784372656174696F6E416D6F756E740045787465726E616C496400466C6F6F72496400486F7374496400417265614E616D6500546F45787465726E616C00546F496E7465726E616C00434845434B53554D5F4D4F44554C4500545241434B444154415F43525950544F5F4B455900545241434B444154415F43525950544F5F495600547261636B44617461546F496E7465726E616C00547261636B44617461546F45787465726E616C004D616B65496E7465726E616C547261636B446174610053706C6974496E7465726E616C547261636B446174610045787465726E616C547261636B4461746142656C6F6E67546F5369746500436F6E76657274547261636B4461746100574350007472616E73666F726D5F6B6579006B65795F6C656E67746800646174615F7065726D75746174696F6E00696E6465783100696E6465783200646973706F7365006765745F43616E52657573655472616E73666F726D006765745F43616E5472616E73666F726D4D756C7469706C65426C6F636B73006765745F496E707574426C6F636B53697A65006765745F4F7574707574426C6F636B53697A65005472616E73666F726D426C6F636B005472616E73666F726D46696E616C426C6F636B00446973706F73650043616E52657573655472616E73666F726D0043616E5472616E73666F726D4D756C7469706C65426C6F636B7300496E707574426C6F636B53697A65004F7574707574426C6F636B53697A65006765745F426C6F636B53697A65007365745F426C6F636B53697A65006765745F466565646261636B53697A65007365745F466565646261636B53697A65006765745F4956007365745F4956004B657953697A6573006765745F4C6567616C426C6F636B53697A6573006765745F4C6567616C4B657953697A6573004369706865724D6F6465006765745F4D6F6465007365745F4D6F64650050616464696E674D6F6465006765745F50616464696E67007365745F50616464696E670047656E657261746549560047656E65726174654B65790043726561746500426C6F636B53697A6500466565646261636B53697A65004956004C6567616C426C6F636B53697A6573004C6567616C4B657953697A6573004D6F64650050616464696E670069735F646973706F73656400437265617465446563727970746F7200437265617465456E63727970746F7200617263666F75726D616E61676564006765745F4B6579007365745F4B6579006765745F4B657953697A65007365745F4B657953697A65004B6579004B657953697A65005472785F3347535F53746172744361726453657373696F6E005472785F3347535F5570646174654361726453657373696F6E005472785F3347535F456E644361726453657373696F6E005472785F3347535F4163636F756E74537461747573005472785F3347535F53656E644576656E740056656E646F7249640053657269616C4E756D626572004D616368696E654E756D626572005472780053657269616C4E756D62657244756D6D79004D616368696E654E756D62657244756D6D790045787465726E616C547261636B4461746144756D6D7900506C617953657373696F6E4D6574657273004576656E7449640048616E64506179004163636F756E740053797374656D2E52756E74696D652E496E7465726F705365727669636573004F75744174747269627574650053746174757354657874004572726F72546578740053716C54727800496E697469616C42616C616E636500537562416D6F756E7400416464416D6F756E740044657461696C7300526561736F6E7300496E6465780076616C75650043617264547261636B44617461004D6F7644657461696C730050726F6D6F4E616D65004973537761700043757272656E637944656E6F6D696E6174696F6E004D6F76656D656E7449640047616D696E675461626C6553657373696F6E496400417578416D6F756E74004375727245786368616E676554797065004368697049640045786368616E6765526573756C74004E6174696F6E616C43757272656E6379004D6F76656D656E740052656C61746564496400496E697469616C416D6F756E740046696E616C416D6F756E74005F7362005F706172616D5F69736F5F636F6465005F706172616D5F63757272656E63795F74797065005F706172616D5F696E6372656D656E74005F706172616D5F63757272656E63795F696E6372656D656E74005F706172616D5F696E5F636173686965725F73657373696F6E73005F706172616D5F696E5F636173686965725F73657373696F6E735F62795F63757272656E6379005F6E6174696F6E616C5F63757272656E6379005F6D6F76656D656E74005F636173686965725F6D6F64655F737472005F636173686965725F6D6F6465005F6861735F6E6F745F6572726F7273004973436F756E745200416374696F6E00416666656374656442616C616E6365004E756D50726F6D6F7300496E707574005449544F4D6F64650042616C616E6365546F5472616E73666572004F75747075740046726F6D49534F00546F49534F0045786368616E67656400456E737572654E6F74496E53657373696F6E00547970654461746100506C617953657373696F6E496E697469616C42616C616E63650048616E64706179416D6F756E740044656C746100466F756E640052656D61696E696E67004973416E6F6E796D6F75734F725669727475616C00506172616D730042616C616E6365546F41646400506C61795F53657373696F6E5F6475726174696F6E0050726F6D6F4E725469636B6574496E0050726F6D6F52655469636B6574496E0052655469636B6574496E0052655469636B65744F75740050726F6D6F4E725469636B65744F75740043617368496E416D6F756E7400496E53657373696F6E506C6179656400496E53657373696F6E576F6E00496E53657373696F6E42616C616E6365005469636B65744F757452650043617368496E52650043757272656E7400506C617961626C65005449544F49735472616E736665720041637469766550726F6D6F7300416464496E53657373696F6E00506F696E7473546F41646400506F696E7473546F41646446696E616C42616C616E636500416C6C6F774E65676174697665506F696E747300546F4164640046696E616C00496E636C7564654275636B65740043616E63656C6C61626C6500436C6F736564506C617953657373696F6E496400546F74616C46726F6D474D42616C616E63650053657373696F6E42616C616E636500497346697273745472616E736665720049676E6F7265506C617961626C65496E43617368496E00496E53657373696F6E52655370656E7400496E53657373696F6E5265506C6179656400576F6E506F696E747300506F696E7448617665546F42654177617264656400536F75726365436F646500536F75726365496400536F757263654E616D6500416C61726D436F6465004465736372697074696F6E0053657665726974790050726F6D6F73546F436F6E73756D650050726F6D6F49640042616C616E636543616E63656C6C65640052657175657374416D6F756E74005265717565737454797065005265736572766564496E697469616C42616C616E63650046424D50726F76696465720053696D756C617465576F6E4C6F636B00437265646974547970650044657369726564537461747573004F6C64537461747573004E657753746174757300506C617954696D655370616E00456665637469766546696E616C42616C616E6365005265706F7274656446696E616C42616C616E63650053797374656D436173686965724E616D6500436F6D7075746564506F696E74730047726F7570005375626A6563740056616C75650042616C310042616C32006F626A004E65676174655265736572766564004578636C756465640050726F766964657200586D6C0057726974654D6F646500526561644D6F646500536F75726365004F75745465726D696E616C496E666F005465726D696E616C45787465726E616C4964004D61736B4E616D6500446973706C61794E616D6500466C61670043757272656E74466C61677300496E7465726E616C547261636B44617461004361726454797065005369746549640053657175656E6365005265636569766564547261636B44617461004442547261636B4461746100496E70757442756666657200496E7075744F666673657400496E707574436F756E74004F7574707574427566666572004F75747075744F666673657400416C674E616D65005267624B657900526762495600446973706F73696E67005374617475734572726F720042696C6C496E0053797374656D2E5265666C656374696F6E00417373656D626C795469746C6541747472696275746500417373656D626C794465736372697074696F6E41747472696275746500417373656D626C79436F6E66696775726174696F6E41747472696275746500417373656D626C79436F6D70616E7941747472696275746500417373656D626C7950726F6475637441747472696275746500417373656D626C79436F7079726967687441747472696275746500417373656D626C7954726164656D61726B41747472696275746500417373656D626C7943756C7475726541747472696275746500436F6D56697369626C65417474726962757465004775696441747472696275746500417373656D626C7956657273696F6E41747472696275746500417373656D626C7946696C6556657273696F6E4174747269627574650053797374656D2E446961676E6F73746963730044656275676761626C6541747472696275746500446562756767696E674D6F6465730053797374656D2E52756E74696D652E436F6D70696C6572536572766963657300436F6D70696C6174696F6E52656C61786174696F6E734174747269627574650052756E74696D65436F6D7061746962696C6974794174747269627574650053514C427573696E6573734C6F67696300526F6C6C6261636B00496E74363400537472696E6700436F6E63617400417070656E644C696E650053716C436F6E6E656374696F6E006765745F436F6E6E656374696F6E0053716C506172616D65746572436F6C6C656374696F6E006765745F506172616D65746572730053716C44625479706500496E7433320053797374656D2E446174612E436F6D6D6F6E004462506172616D65746572007365745F56616C7565004462436F6D6D616E6400457865637574654E6F6E517565727900466F726D6174006F705F4C6573735468616E004D617468004D6178005472795061727365006F705F4469766973696F6E00416273006F705F477265617465725468616E004D696E00446F75626C6500456D70747900457865637574655363616C61720044424E756C6C0053716C44617461526561646572004578656375746552656164657200446244617461526561646572005265616400476574496E74363400476574496E7433320049734E756C6C4F72456D7074790044617461436F6C756D6E436F6C6C656374696F6E006765745F436F6C756D6E7300476574547970650044617461436F6C756D6E007365745F4175746F496E6372656D656E74007365745F4175746F496E6372656D656E7453656564007365745F4175746F496E6372656D656E7453746570007365745F416C6C6F7744424E756C6C006765745F4974656D007365745F5072696D6172794B6579004E6577526F77007365745F4974656D0044617461526F77436F6C6C656374696F6E006765745F526F777300457863657074696F6E00496E7465726E616C44617461436F6C6C656374696F6E42617365006765745F436F756E740053797374656D2E436F6C6C656374696F6E730049456E756D657261746F7200476574456E756D657261746F72006765745F43757272656E74004D6F76654E657874007365745F536F75726365436F6C756D6E00506172616D65746572446972656374696F6E007365745F446972656374696F6E00557064617465526F77536F75726365007365745F55706461746564526F77536F757263650053716C4461746141646170746572007365745F496E73657274436F6D6D616E640044624461746141646170746572004D656D62657277697365436C6F6E65007365745F44656661756C7456616C7565006F705F477265617465725468616E4F72457175616C006765745F48617356616C7565006765745F4974656D41727261790049734E756C6C007365745F4C656E677468007365745F557064617465426174636853697A650053716C457863657074696F6E006765745F56616C7565006F705F556E6172794E65676174696F6E00476574446563696D616C00466C6167734174747269627574650053657269616C697A61626C654174747269627574650046696C6C00476574537472696E6700497344424E756C6C006F705F4D756C7469706C79005472756E63617465006F705F4C6573735468616E4F72457175616C00426F6F6C65616E004765744F7264696E616C00476574426F6F6C65616E006765745F546F74616C4D696E75746573004944617461526561646572004C6F6164004D6964706F696E74526F756E64696E6700526F756E64004461746156696577526F7753746174650053656C656374007365745F557064617465436F6D6D616E64006765745F557064617465436F6D6D616E64004461746141646170746572007365745F436F6E74696E75655570646174654F6E4572726F72006765745F4861734572726F7273006765745F526F774572726F72006765745F436F6D6D616E6454657874004462506172616D65746572436F6C6C656374696F6E006765745F506172616D657465724E616D65005472696D005061644C656674005265706C61636500436F6E7665727400546F446563696D616C004E657874526573756C7400436F6D6D616E6454797065007365745F436F6D6D616E64547970650055496E743332004765744461746554696D6500537562747261637400496E743136006F705F496D706C6963697400417070656E64466F726D6174006F705F4578706C6963697400417070656E6400496E73657274006765745F4C656E67746800546F55707065720044656C657465004163636570744368616E67657300446174615461626C65436F6C6C656374696F6E006765745F5461626C657300537472696E67436F6D70617269736F6E0053797374656D2E494F00537472696E675772697465720054657874577269746572005772697465586D6C00537472696E6752656164657200546578745265616465720052656164586D6C00476574496E743136004D6963726F736F66742E53716C5365727665722E5365727665720053716C46756E6374696F6E41747472696275746500427974650055496E74363400436F6D7061726500506172736500426974436F6E766572746572004765744279746573004D656D6F727953747265616D0043727970746F53747265616D0053747265616D0043727970746F53747265616D4D6F646500546F55496E74363400537562737472696E6700577269746500466C75736846696E616C426C6F636B00546F417272617900546F496E74333200546F496E743634002E6363746F72003C50726976617465496D706C656D656E746174696F6E44657461696C733E7B36433836303542302D343738372D344242392D383838302D3043433134453231453644437D00436F6D70696C657247656E6572617465644174747269627574650056616C756554797065005F5F5374617469634172726179496E69745479706553697A653D31360024246D6574686F643078363030303139342D310052756E74696D6548656C706572730041727261790052756E74696D654669656C6448616E646C6500496E697469616C697A654172726179006765745F46756C6C4E616D65004F626A656374446973706F736564457863657074696F6E00417267756D656E744E756C6C457863657074696F6E00417267756D656E744F75744F6652616E6765457863657074696F6E00474300537570707265737346696E616C697A65004B657953697A6556616C75650043727970746F67726170686963457863657074696F6E004E6F74537570706F72746564457863657074696F6E00524E4743727970746F5365727669636550726F76696465720052616E646F6D4E756D62657247656E657261746F720053716C50726F636564757265417474726962757465004462436F6E6E656374696F6E004F70656E00426567696E5472616E73616374696F6E0044625472616E73616374696F6E00436F6D6D69740000000001001554003300470053002E0053007400610072007400000720002D002000011754003300470053002E00550070006400610074006500001154003300470053002E0045006E006400004D54006800690073002000730065007300730069006F006E00200068006100730020006200650065006E00200063006C006F0073006500640020006D0061006E00750061006C006C0079002E0000395400720078005F004F006E0045006E0064004300610072006400530065007300730069006F006E0020006600610069006C00650064002100002554003300470053002E004100630063006F0075006E007400530074006100740075007300001D54003300470053002E00530065006E0064004500760065006E007400002155006E006B006E006F0077006E0020004500760065006E007400490064002E000059200049004E005300450052005400200049004E0054004F0020004500560045004E0054005F0048004900530054004F0052005900200028002000450048005F005400450052004D0049004E0041004C005F0049004400200000572000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000450048005F00530045005300530049004F004E005F0049004400200000532000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000450048005F004400410054004500540049004D004500200000572000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000450048005F004500560045004E0054005F0054005900500045002000005F2000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000450048005F004F005000450052004100540049004F004E005F0043004F0044004500200000612000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000450048005F004F005000450052004100540049004F004E005F00440041005400410029002000005520002000200020002000200020002000200020002000200020002000200020002000200020002000560041004C00550045005300200028002000400070005400650072006D0069006E0061006C00490064002000005B2000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C0020004000700050006C0061007900530065007300730069006F006E00490064002000004F2000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000470045005400440041005400450028002900200000532000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000400070004500760065006E00740054007900700065002000005B2000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000400070004F007000650072006100740069006F006E0043006F00640065002000004F2000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C0020004000700041006D006F0075006E007400290020000019400070005400650072006D0069006E0061006C0049006400001F4000700050006C0061007900530065007300730069006F006E00490064000017400070004500760065006E0074005400790070006500001F400070004F007000650072006100740069006F006E0043006F006400650000114000700041006D006F0075006E007400003D4500720072006F00720020006F006E00200069006E00730065007200740020006500760065006E0074005F0068006900730074006F00720079002100007D49004E005300450052005400200049004E0054004F002000480041004E00440050004100590053002000200028002000480050005F005400450052004D0049004E0041004C005F00490044002C0020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000007D200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000480050005F0041004D004F0055004E0054002C002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000007D200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000480050005F00540045005F004E0041004D0045002C00200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000007D200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000480050005F0054005900500045002C00200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000007D200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000480050005F00540045005F00500052004F00560049004400450052005F0049004400290020002000200020002000200020002000200020002000200020002000200020002000200020002000007D200020002000200020002000200020002000200020002000200020002000560041004C00550045005300200028002000400070005400650072006D0069006E0061006C00490064002C002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000007D2000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020004000700041006D006F0075006E0074002C0020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000007D200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000400070005400650072006D0069006E0061006C004E0061006D0065002C0020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000007D200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000400070004800700054007900700065002C0020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000007D2000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002800530045004C004500430054002000540045005F00500052004F00560049004400450052005F00490044002000200020002000200020002000200020002000200020002000200020002000007D200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000460052004F004D0020005400450052004D0049004E0041004C005300200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000007D200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000570048004500520045002000540045005F005400450052004D0049004E0041004C005F004900440020003D002000400070005400650072006D0069006E0061006C0049006400290029002000001D400070005400650072006D0069006E0061006C004E0061006D00650000114000700048007000540079007000650000314500720072006F00720020006F006E00200069006E0073006500720074002000680061006E0064007000610079002100007F49006E00760061006C0069006400200050006C0061007900530065007300730069006F006E00490064002E0020005200650070006F0072007400650064003A0020007B0030007D002C002000430075007200720065006E0074003A0020007B0031007D002C0020005300740061007400750073003A0020007B0032007D00007D4E00650067006100740069007600650020004D00650074006500720073002E002000460069006E0061006C003A0020007B0030007D003B00200050006C0061007900650064003A0020007B0031007D002C00200023007B0032007D003B00200057006F006E003A0020007B0033007D002C00200023007B0034007D00001949006E00740065007200660061006300650033004700530000314D006100780069006D0075006D00420061006C0061006E00630065004500720072006F007200430065006E0074007300004D5400720078005F0055007000640061007400650050006C0061007900530065007300730069006F006E0050006C00610079006500640057006F006E0020006600610069006C00650064002100004D5400720078005F0050006C0061007900530065007300730069006F006E00530065007400460069006E0061006C00420061006C0061006E006300650020006600610069006C00650064002100001D40007000470061006D00650042006100730065004E0061006D0065000011470041004D0045005F00330047005300001D40007000440065006E006F006D0069006E006100740069006F006E00001F40007000570063007000530065007100750065006E006300650049006400002540007000440065006C007400610050006C00610079006500640043006F0075006E007400002740007000440065006C007400610050006C00610079006500640041006D006F0075006E007400001F40007000440065006C007400610057006F006E0043006F0075006E007400002140007000440065006C007400610057006F006E0041006D006F0075006E007400002940007000440065006C00740061004A00610063006B0070006F00740041006D006F0075006E0074000031470061006D0065004D006500740065007200730055007000640061007400650020006600610069006C006500640021000073420061006C0061006E006300650020004D00690073006D0061007400630068003A0020005200650070006F0072007400650064003D007B0030007D002C002000430061006C00630075006C0061007400650064003D007B0031007D002C002000460069006E0061006C003D007B0032007D00000F5300750063006300650073007300002D49006E00760061006C006900640020004100630063006F0075006E00740020004E0075006D0062006500720000254100630063006F0075006E007400200049006E002000530065007300730069006F006E00003749006E00760061006C006900640020004D0061006300680069006E006500200049006E0066006F0072006D006100740069006F006E00001B4100630063006500730073002000440065006E00690065006400003349006E00760061006C00690064002000530065007300730069006F006E0020004900440020004E0075006D006200650072000051460069006E0061006C002000620061006C0061006E0063006500200064006F006500730020006E006F00740020006D0061007400630068002000760061006C00690064006100740069006F006E002E00004D2000530045004C004500430054002000200054004F00500020003100200054003300470053005F00560045004E0044004F0052005F0049004400200020002000200020002000200020002000004D200020002000460052004F004D00200020005400450052004D0049004E0041004C0053005F003300470053002000200020002000200020002000200020002000200020002000200020002000004D20002000570048004500520045002000200054003300470053005F00560045004E0044004F0052005F004900440020003D00200040007000560065006E0064006F0072004900640020002000001540007000560065006E0064006F00720049006400005D2000530045004C0045004300540020002000500053005F004100430043004F0055004E0054005F00490044002C002000500053005F005400450052004D0049004E0041004C005F004900440020002000200020002000200020002000005D200020002000460052004F004D002000200050004C00410059005F00530045005300530049004F004E005300200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000005D200020005700480045005200450020002000500053005F0050004C00410059005F00530045005300530049004F004E005F004900440020003D0020004000700050006C0061007900730065007300730069006F006E00490064002000001F4000700050006C0061007900730065007300730069006F006E004900640000572000530045004C00450043005400200020002000500053005F005300540041005400550053002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000057200020002000460052004F004D0020002000200050004C00410059005F00530045005300530049004F004E0053002000200020002000200020002000200020002000200020002000200020002000200020002000200000572000200057004800450052004500200020002000500053005F0050004C00410059005F00530045005300530049004F004E005F004900440020003D00200040007000530065007300730069006F006E00490044002000001740007000530065007300730069006F006E0049004400001D41004D005F004D004F00560045004D0045004E0054005F00490044000019530079007300740065006D002E0049006E00740036003400001F41004D005F004F005000450052004100540049004F004E005F0049004400001B41004D005F004100430043004F0055004E0054005F0049004400000F41004D005F0054005900500045000019530079007300740065006D002E0049006E00740033003200002541004D005F0049004E0049005400490041004C005F00420041004C0041004E0043004500001D530079007300740065006D002E0044006500630069006D0061006C00001B41004D005F005300550042005F0041004D004F0055004E005400001B41004D005F004100440044005F0041004D004F0055004E005400002141004D005F00460049004E0041004C005F00420041004C0041004E0043004500001541004D005F00440045005400410049004C005300001B530079007300740065006D002E0053007400720069006E006700001541004D005F0052004500410053004F004E005300001B41004D005F0054005200410043004B005F004400410054004100001D41004D005F005400450052004D0049004E0041004C005F0049004400002141004D005F005400450052004D0049004E0041004C005F004E0041004D004500002541004D005F0050004C00410059005F00530045005300530049004F004E005F0049004400000340000080BF2000530045004C004500430054002000640062006F002E0054007200610063006B00440061007400610054006F00450078007400650072006E0061006C002800410043005F0054005200410043004B005F00440041005400410029002000460052004F004D0020004100430043004F0055004E00540053002000570048004500520045002000410043005F004100430043004F0055004E0054005F004900440020003D002000400070004100630063006F0075006E0074004900640020000017400070004100630063006F0075006E0074004900640000134D0075006C00740069005300690074006500001149007300430065006E00740065007200004F20004400450043004C0041005200450020002000200040005F0074007200610063006B00640061007400610020004100530020004E0056004100520043004800410052002800350030002900200000392000530045005400200020002000200020002000200040005F0074007200610063006B00640061007400610020003D002000270027002000012F2000490046002000400070004300610072006400440061007400610020004900530020004E0055004C004C0020000080E1200020002000530045004C004500430054002000200040005F0074007200610063006B00640061007400610020003D002000640062006F002E0054007200610063006B00440061007400610054006F00450078007400650072006E0061006C002800410043005F0054005200410043004B005F00440041005400410029002000460052004F004D0020004100430043004F0055004E00540053002000570048004500520045002000410043005F004100430043004F0055004E0054005F004900440020003D002000400070004100630063006F0075006E007400490064002000002B200049004600200040005F0074007200610063006B00640061007400610020003D00200027002700200001492000200020005300450054002000200020002000200040005F0074007200610063006B00640061007400610020003D00200040007000430061007200640044006100740061002000004D4400450043004C0041005200450020002000200040007000530065007100750065006E006300650031003200560061006C0075006500200041005300200042004900470049004E0054002000000320000027550050004400410054004500200020002000530045005100550045004E004300450053002000005B2000200020005300450054002000200020005300450051005F004E004500580054005F00560041004C005500450020003D0020005300450051005F004E004500580054005F00560041004C005500450020002B00200031002000004B2000570048004500520045002000200020005300450051005F00490044002000200020002000200020002000200020003D0020004000700053006500710049006400310032003B0020000080B3530045004C0045004300540020002000200040007000530065007100750065006E006300650031003200560061006C007500650020003D0020005300450051005F004E004500580054005F00560041004C005500450020002D00200031002000460052004F004D002000530045005100550045004E0043004500530020005700480045005200450020005300450051005F004900440020003D0020004000700053006500710049006400310032003B002000016149004E005300450052005400200049004E0054004F002000200020004100430043004F0055004E0054005F004D004F00560045004D0045004E00540053002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002800200041004D005F004100430043004F0055004E0054005F004900440020002000200020002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002C00200041004D005F00540059005000450020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002C00200041004D005F0049004E0049005400490041004C005F00420041004C0041004E0043004500200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002C00200041004D005F005300550042005F0041004D004F0055004E00540020002000200020002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002C00200041004D005F004100440044005F0041004D004F0055004E00540020002000200020002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002C00200041004D005F00460049004E0041004C005F00420041004C0041004E004300450020002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002C00200041004D005F004400410054004500540049004D0045002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002C00200041004D005F0043004100530048004900450052005F004900440020002000200020002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002C00200041004D005F0043004100530048004900450052005F004E0041004D004500200020002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002C00200041004D005F004F005000450052004100540049004F004E005F0049004400200020002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002C00200041004D005F0050004C00410059005F00530045005300530049004F004E005F0049004400200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002C00200041004D005F005400450052004D0049004E0041004C005F00490044002000200020002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002C00200041004D005F005400450052004D0049004E0041004C005F004E0041004D00450020002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002C00200041004D005F00440045005400410049004C00530020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002C00200041004D005F0052004500410053004F004E00530020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002C00200041004D005F0054005200410043004B005F004400410054004100200020002000200020002000200020002000200020002000200020002000200020002000200020002000005D2000200020002000200020002000200020002000200020002C00200041004D005F004D004F00560045004D0045004E0054005F0049004400200020002000200020002000200020002000200020002000200020002000200020002000000529002000006120002000200020002000560041004C00550045005300200028002000400070004100630063006F0075006E007400490064002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002C002000400070005400790070006500200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002C0020004000700049006E0069007400690061006C00420061006C0061006E006300650020002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002C0020004000700053007500620041006D006F0075006E0074002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002C0020004000700041006400640041006D006F0075006E0074002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002C00200040007000460069006E0061006C00420061006C0061006E00630065002000200020002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002C002000470045005400440041005400450028002900200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002C002000400070004300610073006800690065007200490064002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002C0020004000700043006100730068006900650072004E0061006D00650020002000200020002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002C002000400070004F007000650072006100740069006F006E00490064002000200020002000200020002000200020002000200020002000200020002000200020002000200020000080B52000200020002000200020002000200020002000200020002C002000430041005300450020005700480045004E002000490053004E0055004C004C0028004000700050006C0061007900530065007300730069006F006E00490064002C003000290020003D002000300020005400480045004E0020004E0055004C004C00200045004C005300450020004000700050006C0061007900530065007300730069006F006E0049006400200045004E0044002000200000612000200020002000200020002000200020002000200020002C002000400070005400650072006D0069006E0061006C0049006400200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002C002000400070005400650072006D0069006E0061006C004E0061006D0065002000200020002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002C00200040007000440065007400610069006C007300200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002C0020004000700052006500610073006F006E007300200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002C00200040005F0074007200610063006B00640061007400610020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000005D2000200020002000200020002000200020002000200020002C00200040007000530065007100750065006E006300650031003200560061006C0075006500200020002000200020002000200020002000200020002000200020002000004B20005300450054002000400070004D006F00760065006D0065006E0074004900640020003D00200040007000530065007100750065006E006300650031003200560061006C0075006500004920005300450054002000400070004D006F00760065006D0065006E0074004900640020003D002000530043004F00500045005F004900440045004E00540049005400590028002900001B400070004F007000650072006100740069006F006E0049006400000D40007000540079007000650000214000700049006E0069007400690061006C00420061006C0061006E006300650000174000700053007500620041006D006F0075006E00740000174000700041006400640041006D006F0075006E007400001D40007000460069006E0061006C00420061006C0061006E0063006500001340007000440065007400610069006C00730000134000700052006500610073006F006E00730000154000700043006100720064004400610074006100001740007000430061007300680069006500720049006400001B4000700043006100730068006900650072004E0061006D00650000134000700053006500710049006400310032000019400070004D006F00760065006D0065006E00740049006400001F43004D005F004F005000450052004100540049004F004E005F0049004400001B43004D005F004100430043004F0055004E0054005F0049004400002543004D005F0043004100520044005F0054005200410043004B005F004400410054004100000F43004D005F005400590050004500002543004D005F0049004E0049005400490041004C005F00420041004C0041004E0043004500001B43004D005F004100440044005F0041004D004F0055004E005400001B43004D005F005300550042005F0041004D004F0055004E005400002143004D005F00460049004E0041004C005F00420041004C0041004E0043004500002943004D005F00420041004C0041004E00430045005F0049004E004300520045004D0045004E005400001543004D005F00440045005400410049004C005300002943004D005F00430055005200520045004E00430059005F00490053004F005F0043004F0044004500001B43004D005F004100550058005F0041004D004F0055004E005400003143004D005F00430055005200520045004E00430059005F00440045004E004F004D0049004E004100540049004F004E00003543004D005F00470041004D0049004E0047005F005400410042004C0045005F00530045005300530049004F004E005F0049004400001543004D005F0043004800490050005F0049004400002B43004D005F0043004100470045005F00430055005200520045004E00430059005F005400590050004500001B430055005200520045004E00430059005F005400590050004500001B43004D005F00520045004C0041005400450044005F0049004400000F43004D005F004400410054004500001F530079007300740065006D002E004400610074006500540069006D006500004B20004400450043004C0041005200450020002000200040005F0063006D005F006D006F00760065006D0065006E0074005F0069006400200041005300200042004900470049004E00540000808520005300450054002000200020002000200020002000400070004300610067006500430075007200720065006E006300690065007300540079007000650020003D002000490053004E0055004C004C002800400070004300610067006500430075007200720065006E00630069006500730054007900700065002C00200030002900200000672000490046002000400070004300610072006400440061007400610020004900530020004E0055004C004C00200041004E0044002000400070004100630063006F0075006E0074004900640020004900530020004E004F00540020004E0055004C004C0020000057200049004E005300450052005400200049004E0054004F0020002000200043004100530048004900450052005F004D004F00560045004D0045004E005400530020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002800200043004D005F00530045005300530049004F004E005F0049004400200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C00200043004D005F0043004100530048004900450052005F0049004400200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C00200043004D005F0055005300450052005F0049004400200020002000200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C00200043004D005F005400590050004500200020002000200020002000200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C00200043004D005F0049004E0049005400490041004C005F00420041004C0041004E00430045002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C00200043004D005F004100440044005F0041004D004F0055004E005400200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C00200043004D005F005300550042005F0041004D004F0055004E005400200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C00200043004D005F00460049004E0041004C005F00420041004C0041004E0043004500200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C00200043004D005F0055005300450052005F004E0041004D0045002000200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C00200043004D005F0043004100530048004900450052005F004E0041004D0045002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C00200043004D005F0043004100520044005F0054005200410043004B005F0044004100540041002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C00200043004D005F004100430043004F0055004E0054005F0049004400200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C00200043004D005F004F005000450052004100540049004F004E005F00490044002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C00200043004D005F00440045005400410049004C005300200020002000200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C00200043004D005F00430055005200520045004E00430059005F00490053004F005F0043004F004400450020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C00200043004D005F004100550058005F0041004D004F0055004E005400200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C00200043004D005F00430055005200520045004E00430059005F00440045004E004F004D0049004E004100540049004F004E002000200020002000005720002000200020002000200020002000200020002000200020002C00200043004D005F00470041004D0049004E0047005F005400410042004C0045005F00530045005300530049004F004E005F004900440020002000005720002000200020002000200020002000200020002000200020002C00200043004D005F0043004800490050005F0049004400200020002000200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C00200043004D005F0043004100470045005F00430055005200520045004E00430059005F0054005900500045002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C00200043004D005F00520045004C0041005400450044005F0049004400200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C00200043004D005F004400410054004500200020002000200020002000200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002900200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000057200020002000560041004C0055004500530020002000200020002800200040007000530065007300730069006F006E004900640020002000200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C0020004000700043006100730068006900650072004900640020002000200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C0020004000700055007300650072004900640020002000200020002000200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C0020004000700054007900700065002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C0020004000700049006E0069007400690061006C00420061006C0061006E0063006500200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C0020004000700041006400640041006D006F0075006E00740020002000200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C0020004000700053007500620041006D006F0075006E00740020002000200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C00200040007000460069006E0061006C00420061006C0061006E006300650020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C0020004000700055007300650072004E0061006D006500200020002000200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C0020004000700043006100730068006900650072004E0061006D006500200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C00200040005F0074007200610063006B00640061007400610020002000200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C002000400070004100630063006F0075006E0074004900640020002000200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C002000400070004F007000650072006100740069006F006E0049006400200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C00200040007000440065007400610069006C0073002000200020002000200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C00200040007000430075007200720065006E006300790043006F006400650020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C0020004000700041007500780041006D006F0075006E00740020002000200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C00200040007000430075007200720065006E0063007900440065006E006F006D0069006E006100740069006F006E00200020002000200020002000005720002000200020002000200020002000200020002000200020002C00200040007000470061006D0069006E0067005400610062006C006500530065007300730069006F006E0049006400200020002000200020002000005720002000200020002000200020002000200020002000200020002C0020004000700043006800690070004900640020002000200020002000200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C002000400070004300610067006500430075007200720065006E006300690065007300540079007000650020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C00200040007000520065006C0061007400650064004900640020002000200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002C002000690073004E0075006C006C0028004000700044006100740065002C00200047004500540044004100540045002800290029002000200020000057200053004500540020002000200040005F0063006D005F006D006F00760065006D0065006E0074005F006900640020003D002000530043004F00500045005F004900440045004E0054004900540059002800290020000080AD49004600200040007000540079007000650020003E003D00200040007000430061006700650046006900720073007400560061006C0075006500200041004E004400200040007000540079007000650020003C003D0020004000700043006100670065004C00610073007400560061006C0075006500200041004E004400200040007000450078007400650072006E004D006F00760065006D0065006E0074004900640020003E0020003000000B42004500470049004E00005F2000200049004E005300450052005400200049004E0054004F0020002000200043004100470045005F0043004100530048004900450052005F004D004F00560045004D0045004E0054005F00520045004C004100540049004F004E00200000212000200020002000200020002000560041004C005500450053002000280020000045200020002000200020002000200020002000200020002000200020002000200040007000450078007400650072006E004D006F00760065006D0065006E007400490064000041200020002000200020002000200020002000200020002000200020002C00200040005F0063006D005F006D006F00760065006D0065006E0074005F006900640000212000200020002000200020002000200020002000200020002000200029002000000745004E0044000015200042004500470049004E002000540052005900005F200049004600200040007000430075007200720065006E006300790043006F006400650020004900530020004E0055004C004C00200020002000200020002000200020002000200020002000200020002000200020002000200020002000005F200020002000530045005400200040007000430075007200720065006E006300790043006F006400650020003D002000400070004E006100740069006F006E0061006C00430075007200720065006E006300790043006F006400650020000080B320002000200020002000200045005800450043002000640062006F002E0043006100730068006900650072004D006F00760065006D0065006E007400730048006900730074006F0072007900200040007000530065007300730069006F006E00490064002C00200040005F0063006D005F006D006F00760065006D0065006E0074005F00690064002C0020004000700054007900700065002C0020004000700053007500620054007900700065002C0020000080C72000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020004000700049006E0069007400690061006C00420061006C0061006E00630065002C0020004000700041006400640041006D006F0075006E0074002C0020004000700053007500620041006D006F0075006E0074002C00200040007000460069006E0061006C00420061006C0061006E00630065002C0020000080E120002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200040007000430075007200720065006E006300790043006F00640065002C0020004000700041007500780041006D006F0075006E0074002C00200040007000430075007200720065006E0063007900440065006E006F006D0069006E006100740069006F006E002C002000400070004300610067006500430075007200720065006E00630069006500730054007900700065000013200045004E00440020005400520059002000001B200042004500470049004E002000430041005400430048002000005920002000200020004400450043004C0041005200450020002000200040004500720072006F0072004D0065007300730061006700650020004E0056004100520043004800410052002800340030003000300029003B002000004520002000200020004400450043004C0041005200450020002000200040004500720072006F00720053006500760065007200690074007900200049004E0054003B002000003F20002000200020004400450043004C0041005200450020002000200040004500720072006F00720053007400610074006500200049004E0054003B00200000192000200020002000530045004C004500430054002000200000532000200020002000200020002000200040004500720072006F0072004D0065007300730061006700650020003D0020004500520052004F0052005F004D00450053005300410047004500280029002C00200000572000200020002000200020002000200040004500720072006F0072005300650076006500720069007400790020003D0020004500520052004F0052005F0053004500560045005200490054005900280029002C002000004B2000200020002000200020002000200040004500720072006F0072005300740061007400650020003D0020004500520052004F0052005F0053005400410054004500280029003B002000003D200020002000200052004100490053004500520052004F0052002000280040004500720072006F0072004D006500730073006100670065002C002000003F20002000200020002000200020002000200020002000200020002000200040004500720072006F007200530065007600650072006900740079002C002000003920002000200020002000200020002000200020002000200020002000200040004500720072006F0072005300740061007400650029003B000015200045004E004400200043004100540043004800002540007000420061006C0061006E006300650049006E006300720065006D0065006E007400001D40007000430075007200720065006E006300790043006F006400650000174000700041007500780041006D006F0075006E007400002D400070004E006100740069006F006E0061006C00430075007200720065006E006300790043006F0064006500001740007000530065007300730069006F006E0049006400001B43004D005F00530045005300530049004F004E005F004900440000114000700055007300650072004900640000154000700055007300650072004E0061006D0065000013400070005300750062005400790070006500002D40007000430075007200720065006E0063007900440065006E006F006D0069006E006100740069006F006E00002D40007000470061006D0069006E0067005400610062006C006500530065007300730069006F006E0049006400002140007000430061006700650046006900720073007400560061006C0075006500001F4000700043006100670065004C00610073007400560061006C0075006500002540007000450078007400650072006E004D006F00760065006D0065006E007400490064000011400070004300680069007000490064000029400070004300610067006500430075007200720065006E0063006900650073005400790070006500001740007000520065006C00610074006500640049006400000D40007000440061007400650000392000490046002000400049006E004300610073006800690065007200530065007300730069006F006E00730020003D002000310020002000000F200042004500470049004E002000003D20002000200055005000440041005400450020002000200043004100530048004900450052005F00530045005300530049004F004E00530020002000007F200020002000200020002000530045005400200020002000430053005F00420041004C0041004E0043004500200020002000200020003D002000430053005F00420041004C0041004E004300450020002B002000280040007000420061006C0061006E006300650049006E006300720065006D0065006E00740029002000003F2000200020004F0055005400500055005400200020002000440045004C0045005400450044002E00430053005F00420041004C0041004E00430045002000004320002000200020002000200020002000200020002C00200049004E005300450052005400450044002E00430053005F00420041004C0041004E0043004500200020000057200020002000200057004800450052004500200020002000430053005F00530045005300530049004F004E005F0049004400200020003D0020002000200040007000530065007300730069006F006E00490064002000005920002000200020002000200041004E004400200020002000430053005F005300540041005400550053002000200020002000200049004E00200028002000400070005300740061007400750073004F00700065006E0020000067200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000400070005300740061007400750073004F00700065006E00500065006E00640069006E00670020000063200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C00200040007000530074006100740075007300500065006E00640069006E00670020002900200000132000200020002000200045004E0044002000004B2000490046002000400049006E004300610073006800690065007200530065007300730069006F006E00730042007900430075007200720065006E006300790020003D002000310020000011200042004500470049004E00200020000037200020002000490046002000450058004900530054005300200028002000530045004C00450043005400200020002000310020002000006B20002000200020002000200020002000200020002000200020002000200020002000460052004F004D0020002000200043004100530048004900450052005F00530045005300530049004F004E0053005F00420059005F00430055005200520045004E00430059002000006B2000200020002000200020002000200020002000200020002000200020002000570048004500520045002000200020004300530043005F00530045005300530049004F004E005F004900440020003D00200040007000530065007300730069006F006E00490064002000006720002000200020002000200020002000200020002000200020002000200020002000200041004E0044002000200020004300530043005F00490053004F005F0043004F00440045002000200020003D00200040007000490073006F0043006F00640065002000006320002000200020002000200020002000200020002000200020002000200020002000200041004E0044002000200020004300530043005F00540059005000450020002000200020002000200020003D00200040007000540079007000650029002000005B200020002000200020002000200055005000440041005400450020002000200043004100530048004900450052005F00530045005300530049004F004E0053005F00420059005F00430055005200520045004E0043005900200000809B20002000200020002000200020002000200020005300450054002000200020004300530043005F00420041004C0041004E0043004500200020002000200020003D0020004300530043005F00420041004C0041004E004300450020002B002000280040007000420061006C0061006E0063006500430075007200720065006E006300790049006E006300720065006D0065006E00740029002000004920002000200020002000200020004F0055005400500055005400200020002000440045004C0045005400450044002E004300530043005F00420041004C0041004E00430045002000004B200020002000200020002000200020002000200020002000200020002C00200049004E005300450052005400450044002E004300530043005F00420041004C0041004E00430045002000006B20002000200020002000200020002000570048004500520045002000200020004300530043005F00530045005300530049004F004E005F00490044002000200020002000200020002000200020003D00200040007000530065007300730069006F006E004900640020000067200020002000200020002000200020002000200041004E0044002000200020004300530043005F00490053004F005F0043004F0044004500200020002000200020002000200020002000200020003D00200040007000490073006F0043006F006400650020000061200020002000200020002000200020002000200041004E0044002000200020004300530043005F0054005900500045002000200020002000200020002000200020002000200020002000200020003D00200040007000540079007000650020000033200020002000200020002000200020002000200041004E004400200020002000450058004900530054005300200028002000005B200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000530045004C004500430054002000430053005F00530045005300530049004F004E005F00490044002000006120002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000460052004F004D00200043004100530048004900450052005F00530045005300530049004F004E0053002000007D2000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000570048004500520045002000430053005F00530045005300530049004F004E005F0049004400200020003D0020002000200040007000530065007300730069006F006E00490064002000007F20002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200041004E0044002000430053005F005300540041005400550053002000200020002000200049004E00200028002000400070005300740061007400750073004F00700065006E00200000808D2000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000400070005300740061007400750073004F00700065006E00500065006E00640069006E00670020000080892000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C00200040007000530074006100740075007300500065006E00640069006E0067002000290020000037200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200029002000000F2000200045004C0053004500200000612000200020002000200049004E005300450052005400200049004E0054004F0020002000200043004100530048004900450052005F00530045005300530049004F004E0053005F00420059005F00430055005200520045004E00430059002000004520002000200020002000200020002000200020002000200020002000200020002000280020004300530043005F00530045005300530049004F004E005F004900440020000041200020002000200020002000200020002000200020002000200020002000200020002C0020004300530043005F00490053004F005F0043004F004400450020000039200020002000200020002000200020002000200020002000200020002000200020002C0020004300530043005F0054005900500045002000003F200020002000200020002000200020002000200020002000200020002000200020002C0020004300530043005F00420041004C0041004E004300450020000045200020002000200020002000200020002000200020002000200020002000200020002C0020004300530043005F0043004F004C004C00450043005400450044002000200000272000200020002000200020002000200020002000200020002000200020002000200029002000002F20002000200020002000200020002000200020004F005500540050005500540020002000200030002E00300020000051200020002000200020002000200020002000200020002000200020002000200020002C00200049004E005300450052005400450044002E004300530043005F00420041004C0041004E0043004500200000292000200020002000200020002000200020002000560041004C005500450053002000200020002000003F200020002000200020002000200020002000200020002000200020002000200020002800200040007000530065007300730069006F006E00490064002000003B200020002000200020002000200020002000200020002000200020002000200020002C00200040007000490073006F0043006F006400650020000035200020002000200020002000200020002000200020002000200020002000200020002C0020004000700054007900700065002000005D200020002000200020002000200020002000200020002000200020002000200020002C00200040007000420061006C0061006E0063006500430075007200720065006E006300790049006E006300720065006D0065006E0074002000002F200020002000200020002000200020002000200020002000200020002000200020002C00200030002E0030002000000F20002000200045004E00440020000019400070005300740061007400750073004F00700065006E000027400070005300740061007400750073004F00700065006E00500065006E00640069006E006700001F40007000530074006100740075007300500065006E00640069006E006700001340007000490073006F0043006F0064006500003540007000420061006C0061006E0063006500430075007200720065006E006300790049006E006300720065006D0065006E0074000025400049006E004300610073006800690065007200530065007300730069006F006E0073000039400049006E004300610073006800690065007200530065007300730069006F006E00730042007900430075007200720065006E0063007900001F52006500670069006F006E0061006C004F007000740069006F006E007300001F430075007200720065006E0063007900490053004F0043006F00640065000019470061006D0069006E0067005400610062006C0065007300001943006100730068006900650072002E004D006F006400650000033100005B20002000200020002000200020002000200020004100430050005F004100430043004F0055004E0054005F004900440020002000200020002000200020003D002000400070004100630063006F0075006E0074004900640020000061200020002000200041004E0044002000200020004100430050005F00530054004100540055005300200020002000200020002000200020002000200020003D00200040007000530074006100740075007300410063007400690076006500200000808F200020002000200041004E0044002000200020004100430050005F004300520045004400490054005F00540059005000450020002000200020002000200049004E002000280020004000700043007200650064006900740054007900700065004E00520031002C0020004000700043007200650064006900740054007900700065004E0052003200200029002000006B200020002000200041004E0044002000200020004100430050005F00500052004F004D004F005F00540059005000450020002000200020002000200020003C003E00200040007000500072006F006D006F0043006F0076006500720043006F00750070006F006E0020000049200020002000200041004E0044002000200020004100430050005F00420041004C0041004E004300450020002000200020002000200020002000200020003E003D002000300020000080ED4900460020002800530045004C00450043005400200043004F0055004E00540028002A002900200020002000200020002000460052004F004D0020004100430043004F0055004E0054005F00500052004F004D004F00540049004F004E00530020005700490054004800280049004E004400450058002800490058005F004100430050005F004100430043004F0055004E0054005F005300540041005400550053002900290020005700480045005200450020004100430050005F0057004F004E004C004F0043004B0020004900530020004E004F00540020004E0055004C004C00200041004E0044002000000D290020003E002000300020000080ED2000200020002000530045004C0045004300540020004100430050005F0055004E0049005100550045005F00490044002000460052004F004D0020004100430043004F0055004E0054005F00500052004F004D004F00540049004F004E00530020005700490054004800280049004E004400450058002800490058005F004100430050005F004100430043004F0055004E0054005F005300540041005400550053002900290020005700480045005200450020004100430050005F0057004F004E004C004F0043004B0020004900530020004E004F00540020004E0055004C004C00200041004E0044002000000B45004C005300450020000080E9200020004900460020002800530045004C00450043005400200043004F0055004E00540028002A002900200020002000200020002000460052004F004D0020004100430043004F0055004E0054005F00500052004F004D004F00540049004F004E00530020005700490054004800280049004E004400450058002800490058005F004100430050005F004100430043004F0055004E0054005F005300540041005400550053002900290020005700480045005200450020004100430050005F0057004F004E004C004F0043004B0020004900530020004E0055004C004C00200041004E00440020000080E9200020002000200020002000530045004C0045004300540020004100430050005F0055004E0049005100550045005F00490044002000460052004F004D0020004100430043004F0055004E0054005F00500052004F004D004F00540049004F004E00530020005700490054004800280049004E004400450058002800490058005F004100430050005F004100430043004F0055004E0054005F005300540041005400550053002900290020005700480045005200450020004100430050005F0057004F004E004C004F0043004B0020004900530020004E0055004C004C00200041004E00440020000043200020002000200020002000530045004C0045004300540020004300410053005400200028002D003100200041005300200042004900470049004E00540029002000014D200020002000200041004E0044002000200020004100430050005F0050004C00410059005F00530045005300530049004F004E005F004900440020004900530020004E0055004C004C0020000063200020002000200041004E0044002000200020004100430050005F004300520045004400490054005F0054005900500045002000200020002000200020003D0020004000700043007200650064006900740054007900700065004E00520031002000005B200020002000200041004E0044002000200020004100430050005F00420041004C0041004E00430045002000200020002000200020002000200020003E003D0020004100430050005F0057004F004E004C004F0043004B0020000055200020002000200041004E0044002000200020004100430050005F0057004F004E004C004F0043004B002000200020002000200020002000200020004900530020004E004F00540020004E0055004C004C00200000312000530045004C004500430054002000200020004100430050005F0055004E0049005100550045005F00490044002000007F200020002000460052004F004D002000200020004100430043004F0055004E0054005F00500052004F004D004F00540049004F004E00530020005700490054004800280049004E004400450058002800490058005F006100630070005F006100630063006F0075006E0074005F007300740061007400750073002900290000152000200057004800450052004500200020002000001D40007000530074006100740075007300410063007400690076006500001F4000700043007200650064006900740054007900700065004E0052003100001F4000700043007200650064006900740054007900700065004E0052003200002540007000500072006F006D006F0043006F0076006500720043006F00750070006F006E00003B20005500500044004100540045002000200020004100430043004F0055004E0054005F00500052004F004D004F00540049004F004E0053002000005B20002000200020005300450054002000200020004100430050005F00530054004100540055005300200020002000200020002000200020002000200020003D002000400070004E00650077005300740061007400750073002000003D20004F0055005400500055005400200020002000440045004C0045005400450044002E004100430050005F00420041004C0041004E00430045002000008081200020002000460052004F004D002000200020004100430043004F0055004E0054005F00500052004F004D004F00540049004F004E00530020005700490054004800280049004E00440045005800280050004B005F006100630063006F0075006E0074005F00700072006F006D006F00740069006F006E007300290029002000005920002000570048004500520045002000200020004100430050005F0055004E0049005100550045005F0049004400200020002000200020002000200020003D0020004000700055006E0069007100750065004900640020000015200020002000200041004E00440020002000200000154000700055006E006900710075006500490064000017400070004E00650077005300740061007400750073000059530051004C005F0042005500530049004E004500530053005F004C004F00470049004300200064006F00650073006E0027007400200073007500700070006F007200740020005400490054004F0020004D006F0064006500010953006900740065000025440069007300610062006C0065004E0065007700530065007300730069006F006E007300004B530079007300740065006D0020006800610073002000640069007300610062006C006500640020006E0065007700200050006C0061007900530065007300730069006F006E0073002E000015530079007300740065006D004D006F0064006500000334000080B9530045004C004500430054002000540045005F00490053004F005F0043004F00440045002C002000540045005F005600490052005400550041004C005F004100430043004F0055004E0054005F00490044002000460052004F004D0020005400450052004D0049004E0041004C0053002000570048004500520045002000540045005F005400450052004D0049004E0041004C005F004900440020003D002000400070005400650072006D0069006E0061006C004900640000635400720078005F005500700064006100740065004100630063006F0075006E007400420061006C0061006E0063006500200047006500740020007400650072006D0069006E0061006C00200069006E0066006F0020006600610069006C0065006400005B2000200020002000530045004C00450043005400200020002000500053005F004100430043004F0055004E0054005F00490044002000200020002000200020002000200020002000200020002000200020002000200020002000005B200020002000200020002000200020002000200020002C002000410043005F0054005900500045002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000005B200020002000200020002000460052004F004D0020002000200050004C00410059005F00530045005300530049004F004E0053002000200020002000200020002000200020002000200020002000200020002000200020002000005B20004C0045004600540020004A004F0049004E002000200020004100430043004F0055004E0054005300200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000005B200020002000200020002000200020004F004E00200020002000410043005F004100430043004F0055004E0054005F0049004400200020003D002000500053005F004100430043004F0055004E0054005F004900440020002000005B2000200020002000200057004800450052004500200020002000500053005F005400450052004D0049004E0041004C005F004900440020003D002000400070005400650072006D0069006E0061006C0049006400200020002000005B200020002000200020002000200041004E004400200020002000500053005F005300540041005400550053002000200020002000200020003D002000400070005300740061007400750073002000200020002000200020002000001140007000530074006100740075007300005B5400720078005F005500700064006100740065004100630063006F0075006E007400420061006C0061006E006300650020004C004F0043004B0020006100630063006F0075006E00740020006600610069006C00650064002E0000395400720078005F0047006500740050006C0061007900530065007300730069006F006E004900640020006600610069006C00650064002E0000435400720078005F0055006E006C0069006E006B004100630063006F0075006E0074005400650072006D0069006E0061006C0020006600610069006C00650064002E00003F5400720078005F004C0069006E006B004100630063006F0075006E0074005400650072006D0069006E0061006C0020006600610069006C00650064002E0000474C004300440020005400720061006E007300660065007200200069006E00200033004700530020006E006F007400200073007500700070006F00720074006500640021002100003D5400720078005F0047006500740050006C0061007900610062006C006500420061006C0061006E006300650020006600610069006C00650064002E00006F57006900740068006F00750074002000700072006F006D006F00740069006F006E007300200074006F0020007400720061006E0073006600650072003A002000420061006C0061006E00630065002000410063007400690076006500500072006F006D006F00730020003D002000002D2C002000420061006C0061006E006300650054006F005400720061006E00730066006500720020003D002000001F2C0020005400650072006D0069006E0061006C004900640020003D002000005B5400720078005F005400690074006F005500700064006100740065004100630063006F0075006E007400500072006F006D006F00740069006F006E00420061006C0061006E006300650020006600610069006C00650064002E0000435400720078005F00520065007300650074004100630063006F0075006E00740049006E00530065007300730069006F006E0020006600610069006C00650064002E0000655400720078005F005500700064006100740065004100630063006F0075006E007400420061006C0061006E006300650020006600610069006C00650064002E00200050006C0061007900610062006C006500420061006C0061006E00630065003A00200000674100630063006F0075006E0074004D006F00760065006D0065006E00740073005400610062006C0065002E004100640064002800530074006100720074004300610072006400530065007300730069006F006E00290020006600610069006C00650064002E0000454100630063006F0075006E0074004D006F00760065006D0065006E00740073005400610062006C0065002E00530061007600650020006600610069006C00650064002E00002F5400720078005F005300650074004100630074006900760069007400790020006600610069006C00650064002E00001F50006C0061007900530065007300730069006F006E00490064003A00200000272C00200050006C0061007900610062006C006500420061006C0061006E00630065003A002000005B5400720078005F00420061006C0061006E006300650054006F0050006C0061007900530065007300730069006F006E0020006600610069006C00650064003A0020004E0065007700530065007300730069006F006E003A00200000232C00200050006C0061007900530065007300730069006F006E00490064003A00200000695400720078005F005500700064006100740065004100630063006F0075006E00740049006E00530065007300730069006F006E0020006600610069006C00650064002E00200050006C0061007900610062006C006500420061006C0061006E00630065003A002000005F200020002000530045004C0045004300540020002000200020002000640062006F002E004100700070006C007900450078006300680061006E006700650032002800200020004000700041006D006F0075006E0074002C00200020002000005F2000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200040007000460072006F006D00490073006F002C0020002000005F200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020004000700054006F00490073006F0020002000200020002000005F20002000200020002000200020002000200020002000200020002000290020002000410053002000450078006300680061006E00670065006400200020002000200020002000200020002000200020002000200020002000200020002000001340007000460072006F006D00490073006F00000F4000700054006F00490073006F000013450078006300680061006E0067006500640000808155005000440041005400450020004100430043004F0055004E00540053002000530045005400200020002000410043005F00430055005200520045004E0054005F005400450052004D0049004E0041004C005F0049004400200020002000200020003D002000400070005400650072006D0069006E0061006C004900640020000080EF200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000410043005F00430055005200520045004E0054005F005400450052004D0049004E0041004C005F004E0041004D0045002000200020003D0020002800530045004C004500430054002000540045005F004E0041004D0045002000460052004F004D0020005400450052004D0049004E0041004C0053002000570048004500520045002000540045005F005400450052004D0049004E0041004C005F004900440020003D002000400070005400650072006D0069006E0061006C004900640029002000008087200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000410043005F00430055005200520045004E0054005F0050004C00410059005F00530045005300530049004F004E005F004900440020003D0020004000700050006C0061007900530065007300730069006F006E00490064002000007F2000200020002000200020002000200020002000200020002000200057004800450052004500200020002000410043005F004100430043004F0055004E0054005F0049004400200020002000200020002000200020002000200020002000200020003D002000400070004100630063006F0075006E0074004900640020000073200020002000200020002000200020002000200020002000200020002000200041004E004400200020002000410043005F00430055005200520045004E0054005F005400450052004D0049004E0041004C005F0049004400200020002000200020004900530020004E0055004C004C0020000073200020002000200020002000200020002000200020002000200020002000200041004E004400200020002000410043005F00430055005200520045004E0054005F005400450052004D0049004E0041004C005F004E0041004D0045002000200020004900530020004E0055004C004C0020000073200020002000200020002000200020002000200020002000200020002000200041004E004400200020002000410043005F00430055005200520045004E0054005F0050004C00410059005F00530045005300530049004F004E005F004900440020004900530020004E0055004C004C002000000720003B002000007F55005000440041005400450020005400450052004D0049004E0041004C005300200053004500540020002000540045005F00430055005200520045004E0054005F004100430043004F0055004E0054005F00490044002000200020002000200020003D002000400070004100630063006F0075006E007400490064002000008087200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000540045005F00430055005200520045004E0054005F0050004C00410059005F00530045005300530049004F004E005F004900440020003D0020004000700050006C0061007900530065007300730069006F006E004900640020000080812000200020002000200020002000200020002000200020002000200057004800450052004500200020002000540045005F005400450052004D0049004E0041004C005F004900440020002000200020002000200020002000200020002000200020003D002000400070005400650072006D0069006E0061006C00490064002000008097200055005000440041005400450020004100430043004F0055004E00540053002000530045005400200020002000410043005F004C004100530054005F005400450052004D0049004E0041004C005F0049004400200020002000200020002000200020003D002000410043005F00430055005200520045004E0054005F005400450052004D0049004E0041004C005F0049004400200000809B2000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000410043005F004C004100530054005F005400450052004D0049004E0041004C005F004E0041004D0045002000200020002000200020003D002000410043005F00430055005200520045004E0054005F005400450052004D0049004E0041004C005F004E0041004D004500200000809F2000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000410043005F004C004100530054005F0050004C00410059005F00530045005300530049004F004E005F004900440020002000200020003D002000410043005F00430055005200520045004E0054005F0050004C00410059005F00530045005300530049004F004E005F0049004400200000732000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000410043005F00430055005200520045004E0054005F005400450052004D0049004E0041004C005F0049004400200020002000200020003D0020004E0055004C004C00200000732000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000410043005F00430055005200520045004E0054005F005400450052004D0049004E0041004C005F004E0041004D0045002000200020003D0020004E0055004C004C00200000732000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000410043005F00430055005200520045004E0054005F0050004C00410059005F00530045005300530049004F004E005F004900440020003D0020004E0055004C004C00200000808120002000200020002000200020002000200020002000200020002000200057004800450052004500200020002000410043005F004100430043004F0055004E0054005F0049004400200020002000200020002000200020002000200020002000200020003D002000400070004100630063006F0075006E0074004900640020000080832000200020002000200020002000200020002000200020002000200020002000200041004E004400200020002000410043005F00430055005200520045004E0054005F005400450052004D0049004E0041004C005F0049004400200020002000200020003D002000400070005400650072006D0069006E0061006C004900640020000080892000200020002000200020002000200020002000200020002000200020002000200041004E004400200020002000410043005F00430055005200520045004E0054005F0050004C00410059005F00530045005300530049004F004E005F004900440020003D0020004000700050006C0061007900530065007300730069006F006E004900640020000073200055005000440041005400450020005400450052004D0049004E0041004C005300200053004500540020002000540045005F00430055005200520045004E0054005F004100430043004F0055004E0054005F00490044002000200020002000200020003D0020004E0055004C004C00200000732000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000540045005F00430055005200520045004E0054005F0050004C00410059005F00530045005300530049004F004E005F004900440020003D0020004E0055004C004C00200000808320002000200020002000200020002000200020002000200020002000200057004800450052004500200020002000540045005F005400450052004D0049004E0041004C005F004900440020002000200020002000200020002000200020002000200020003D002000400070005400650072006D0069006E0061006C004900640020000080812000200020002000200020002000200020002000200020002000200020002000200041004E004400200020002000540045005F00430055005200520045004E0054005F004100430043004F0055004E0054005F00490044002000200020002000200020003D002000400070004100630063006F0075006E0074004900640020000080892000200020002000200020002000200020002000200020002000200020002000200041004E004400200020002000540045005F00430055005200520045004E0054005F0050004C00410059005F00530045005300530049004F004E005F004900440020003D0020004000700050006C0061007900530065007300730069006F006E0049006400200000808D2000530045004C0045004300540020002000200043006F0075006E00740028002A002900200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000808D200020002000460052004F004D002000200020004100430043004F0055004E0054005300200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000808D2000200057004800450052004500200020002000410043005F004100430043004F0055004E0054005F004900440020003D002000400070004100630063006F0075006E0074004900640020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000808D200020002000200041004E004400200020002000410043005F00430055005200520045004E0054005F005400450052004D0049004E0041004C005F0049004400200020002000200020004900530020004E004F00540020004E0055004C004C00200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000808D200020002000200041004E004400200020002000410043005F00430055005200520045004E0054005F0050004C00410059005F00530045005300530049004F004E005F004900440020004900530020004E004F00540020004E0055004C004C00200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000808D200020002000200041004E004400200020002000410043005F004100430043004F0055004E0054005F004900440020003C003E00200028002000530045004C00450043005400200020002000540045005F005600490052005400550041004C005F004100430043004F0055004E0054005F00490044002000200020002000200020002000200020002000200000808D2000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000460052004F004D002000200020005400450052004D0049004E0041004C0053002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000808D20002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200057004800450052004500200020002000540045005F005400450052004D0049004E0041004C005F004900440020003D002000400070005400650072006D0069006E0061006C004900640020002900200000432000530045004C00450043005400200020002000410043005F00430055005200520045004E0054005F005400450052004D0049004E0041004C005F00490044002000004B200020002000200020002000200020002C002000410043005F00430055005200520045004E0054005F0050004C00410059005F00530045005300530049004F004E005F004900440020000025200020002000200020002000200020002C002000410043005F00540059005000450020000029200020002000200020002000200020002C002000500053005F0053005400410054005500530020000031200020002000200020002000200020002C002000410043005F0054005200410043004B005F0044004100540041002000003D200020002000200020002000200020002C002000500053005F00420041004C0041004E00430045005F004D00490053004D00410054004300480020000043200020002000200020002000200020002C002000490053004E0055004C004C002800500053005F0057004F004E005F0043004F0055004E0054002C003000290020000049200020002000200020002000200020002C002000490053004E0055004C004C002800500053005F0050004C0041005900450044005F0043004F0055004E0054002C003000290020000049200020002000200020002000200020002C002000490053004E0055004C004C002800500053005F00520045005F005400490043004B00450054005F0049004E002C003000290020000055200020002000200020002000200020002C002000490053004E0055004C004C002800500053005F00500052004F004D004F005F00520045005F005400490043004B00450054005F0049004E002C003000290020000055200020002000200020002000200020002C002000490053004E0055004C004C002800500053005F00500052004F004D004F005F004E0052005F005400490043004B00450054005F0049004E002C00300029002000004B200020002000200020002000200020002C002000490053004E0055004C004C002800500053005F00520045005F005400490043004B00450054005F004F00550054002C003000290020000057200020002000200020002000200020002C002000490053004E0055004C004C002800500053005F00500052004F004D004F005F004E0052005F005400490043004B00450054005F004F00550054002C003000290020000080BD200020002000460052004F004D002000200020004100430043004F0055004E005400530020004C0045004600540020004A004F0049004E00200050004C00410059005F00530045005300530049004F004E00530020004F004E002000410043005F00430055005200520045004E0054005F0050004C00410059005F00530045005300530049004F004E005F004900440020003D002000500053005F0050004C00410059005F00530045005300530049004F004E005F00490044002000004D2000200057004800450052004500200020002000410043005F004100430043004F0055004E0054005F004900440020003D002000400070004100630063006F0075006E007400490064002000005320004400450043004C00410052004500200040004100630074006900760069007400790044007500650054006F00430061007200640049006E00200041005300200049004E0054004500470045005200200000792000530045004C0045004300540020002000200040004100630074006900760069007400790044007500650054006F00430061007200640049006E0020003D002000430041005300540020002800470050005F004B00450059005F00560041004C0055004500200041005300200049004E005400290020000079200020002000460052004F004D00200020002000470045004E004500520041004C005F0050004100520041004D00530020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000792000200057004800450052004500200020002000470050005F00470052004F00550050005F004B00450059002000200020003D002000270050006C006100790027002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000179200020002000200041004E004400200020002000470050005F005300550042004A004500430054005F004B004500590020003D00200027004100630074006900760069007400790044007500650054006F00430061007200640049006E002700200020002000200020002000200020002000200020002000014B20004900460020002800200040004100630074006900760069007400790044007500650054006F00430061007200640049006E0020004900530020004E0055004C004C00200029002000004B200042004500470049004E002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000004B200020002000530045005400200040004100630074006900760069007400790044007500650054006F00430061007200640049006E0020003D0020003000200020002000200020002000004B200045004E0044002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000061200049004E005300450052005400200049004E0054004F00200050004C00410059005F00530045005300530049004F004E00530020002800500053005F004100430043004F0055004E0054005F004900440020002000200020002000200020000061200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000500053005F005400450052004D0049004E0041004C005F00490044002000200020002000200020000061200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000500053005F00540059005000450020002000200020002000200020002000200020002000200020000061200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000500053005F0054005900500045005F004400410054004100200020002000200020002000200020000061200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000500053005F0049004E0049005400490041004C005F00420041004C0041004E0043004500200020000061200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000500053005F00460049004E0041004C005F00420041004C0041004E004300450020002000200020000061200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000500053005F00460049004E00490053004800450044002000200020002000200020002000200020000061200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000500053005F005300540041004E0044005F0041004C004F004E004500200020002000200020002900006120002000200020002000200020002000200020002000200020002000200020002000200020002000560041004C0055004500530020002800400070004100630063006F0075006E007400490064002000200020002000200020002000200020000061200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000400070005400650072006D0069006E0061006C0049006400200020002000200020002000200020000061200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000400070005400790070006500570069006E00200020002000200020002000200020002000200020000061200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000400070005400790070006500440061007400610020002000200020002000200020002000200020000061200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C00200040007000440065006600610075006C007400420061006C0061006E006300650020002000200020000080BD200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000430041005300450020005700480045004E002000280040004100630074006900760069007400790044007500650054006F00430061007200640049006E0020003D0020003100290020005400480045004E002000470045005400440041005400450028002900200045004C005300450020004E0055004C004C00200045004E00440020000063200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000400070005300740061006E00640041006C006F006E0065002000200020002000200020002000200029000051200053004500540020004000700050006C0061007900530065007300730069006F006E004900640020003D002000530043004F00500045005F004900440045004E005400490054005900280029002000001B400070005400650072006D0069006E0061006C004900640020000013400070005400790070006500570069006E00002140007000440065006600610075006C007400420061006C0061006E0063006500001540007000540079007000650044006100740061000019400070005300740061006E00640041006C006F006E0065000080BF550050004400410054004500200050004C00410059005F00530045005300530049004F004E00530020005300450054002000500053005F004100430043004F0055004E0054005F004900440020003D002000400070004100630063006F0075006E007400490064002000570048004500520045002000500053005F0050004C00410059005F00530045005300530049004F004E005F004900440020003D0020004000700050006C0061007900530065007300730069006F006E004900640000214000700050006C0061007900530065007300730069006F006E00490064002000005549004E005300450052005400200049004E0054004F00200050004C00410059005F00530045005300530049004F004E005300200028002000500053005F004100430043004F0055004E0054005F004900440020000057200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000500053005F005400450052004D0049004E0041004C005F004900440020000049200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000500053005F0054005900500045002000005F200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000500053005F0049004E0049005400490041004C005F00420041004C0041004E00430045002000005B200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000500053005F00460049004E0041004C005F00420041004C0041004E004300450020000059200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000500053005F0050004C0041005900450044005F0043004F0055004E0054002000005B200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000500053005F0050004C0041005900450044005F0041004D004F0055004E00540020000053200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000500053005F0057004F004E005F0043004F0055004E00540020000055200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000500053005F0057004F004E005F0041004D004F0055004E0054002000004F200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000500053005F0043004100530048005F0049004E0020000051200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000500053005F0043004100530048005F004F00550054002000004D200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000500053005F005300540041005400550053002000004F200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000500053005F00530054004100520054004500440020000051200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000500053005F00460049004E004900530048004500440020000057200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000500053005F005300540041004E0044005F0041004C004F004E0045002000004B200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000500053005F00500052004F004D004F0020000039200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002900200000512000200020002000200020002000200020002000200020002000200020002000200020002000560041004C00550045005300200028002000400070004100630063006F0075006E0074004900640020000053200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000400070005400650072006D0069006E0061006C004900640020000047200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C0020004000700054007900700065002000005B200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C0020004000700049006E0069007400690061006C00420061006C0061006E006300650020000057200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C00200040007000460069006E0061006C00420061006C0061006E00630065002000003D200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000300020000051200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C0020004000700057006F006E0041006D006F0075006E0074002000004B200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000400070005300740061007400750073002000004D200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C00200047004500540044004100540045002800290020000053200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000400070005300740061006E00640041006C006F006E006500200000808520002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200053004500540020004000700050006C0061007900530065007300730069006F006E004900640020003D002000530043004F00500045005F004900440045004E00540049005400590028002900200000174000700057006F006E0041006D006F0075006E007400002F55005000440041005400450020002000200050004C00410059005F00530045005300530049004F004E00530020000080F5200020002000530045005400200020002000500053005F0050004C0041005900450044005F0043004F0055004E00540020002000200020003D002000430041005300450020005700480045004E00200028004000700050006C00610079006500640043006F0075006E007400200020003E002000500053005F0050004C0041005900450044005F0043004F0055004E0054002900200020005400480045004E0020004000700050006C00610079006500640043006F0075006E0074002000200045004C00530045002000500053005F0050004C0041005900450044005F0043004F0055004E0054002000200045004E0044002000005D20002000200020002000200020002C002000500053005F0050004C0041005900450044005F0041004D004F0055004E0054002000200020003D0020004000700050006C00610079006500640041006D006F0075006E007400200020000080F520002000200020002000200020002C002000500053005F0057004F004E005F0043004F0055004E00540020002000200020002000200020003D002000430041005300450020005700480045004E00200028004000700057006F006E0043006F0075006E007400200020002000200020003E002000500053005F0057004F004E005F0043004F0055004E0054002900200020002000200020005400480045004E0020004000700057006F006E0043006F0075006E0074002000200020002000200045004C00530045002000500053005F0057004F004E005F0043004F0055004E0054002000200020002000200045004E0044002000005D20002000200020002000200020002C002000500053005F0057004F004E005F0041004D004F0055004E0054002000200020002000200020003D0020004000700057006F006E0041006D006F0075006E0074002000200020002000200000810B20002000200020002000200020002C002000700073005F0063006100730068005F0069006E002000200020002000200020003D002000430041005300450020005700480045004E002000280040007000420069006C006C00730049006E00200020002000200020003E002000690073006E0075006C006C002800700073005F0063006100730068005F0069006E002C00300029002900200020002000200020005400480045004E00200040007000420069006C006C00730049006E002000200020002000200045004C00530045002000690073006E0075006C006C002800700073005F0063006100730068005F0069006E002C00300029002000200020002000200045004E00440020000080F520002000200020002000200020002C002000500053005F0050004C0041005900450044005F0041004D004F0055004E0054002000200020003D002000430041005300450020005700480045004E00200028004000700050006C00610079006500640041006D006F0075006E00740020003E002000500053005F0050004C0041005900450044005F0041004D004F0055004E005400290020005400480045004E0020004000700050006C00610079006500640041006D006F0075006E007400200045004C00530045002000500053005F0050004C0041005900450044005F0041004D004F0055004E005400200045004E00440020000080F520002000200020002000200020002C002000500053005F0057004F004E005F0041004D004F0055004E0054002000200020002000200020003D002000430041005300450020005700480045004E00200028004000700057006F006E0041006D006F0075006E00740020002000200020003E002000500053005F0057004F004E005F0041004D004F0055004E005400290020002000200020005400480045004E0020004000700057006F006E0041006D006F0075006E007400200020002000200045004C00530045002000500053005F0057004F004E005F0041004D004F0055004E005400200020002000200045004E0044002000004720002000200020002000200020002C002000500053005F004C004F0043004B004500440020002000200020002000200020002000200020003D0020004E0055004C004C002000005120002000200020002000200020002C002000500053005F00460049004E0049005300480045004400200020002000200020002000200020003D0020004700450054004400410054004500280029002000007D4F005500540050005500540020002000200049004E005300450052005400450044002E00500053005F0050004C0041005900450044005F0043004F0055004E005400200020002D002000440045004C0045005400450044002E00500053005F0050004C0041005900450044005F0043004F0055004E00540020002000017D20002000200020002000200020002C00200049004E005300450052005400450044002E00500053005F0050004C0041005900450044005F0041004D004F0055004E00540020002D002000440045004C0045005400450044002E00500053005F0050004C0041005900450044005F0041004D004F0055004E0054002000017D20002000200020002000200020002C00200049004E005300450052005400450044002E00500053005F0057004F004E005F0043004F0055004E005400200020002000200020002D002000440045004C0045005400450044002E00500053005F0057004F004E005F0043004F0055004E00540020002000200020002000017D20002000200020002000200020002C00200049004E005300450052005400450044002E00500053005F0057004F004E005F0041004D004F0055004E00540020002000200020002D002000440045004C0045005400450044002E00500053005F0057004F004E005F0041004D004F0055004E005400200020002000200001809920002000200020002000200020002C002000690073006E0075006C006C00280049004E005300450052005400450044002E00700073005F0063006100730068005F0069006E002C003000290020002000200020002D002000690073006E0075006C006C002800440045004C0045005400450044002E00700073005F0063006100730068005F0069006E002C00300029002000200020002000015D200057004800450052004500200020002000500053005F0050004C00410059005F00530045005300530049004F004E005F004900440020003D0020004000700050006C0061007900530065007300730069006F006E00490064002000001B4000700050006C00610079006500640043006F0075006E007400001D4000700050006C00610079006500640041006D006F0075006E00740000154000700057006F006E0043006F0075006E007400001340007000420069006C006C00730049006E00005155006E0065007800700065006300740065006400200050006C0061007900530065007300730069006F006E00490064002F005400720061006E00730061006300740069006F006E00490064003A00200000052C0020000037420061006C0061006E0063006500460072006F006D0047004D0020006300680061006E006700650064002000660072006F006D002000000B2C00200074006F0020000059530065007300730069006F006E0020004D006500740065007200730020006E006F0074002000720065006300650069007600650064002E0020005400720061006E00730061006300740069006F006E00490064003A002000001F2C00200048006100730043006F0075006E0074006500720073003A002000003355006E006500780070006500630074006500640020005400650072006D0069006E0061006C0054007900700065003A002000004D5400720078005F0055007000640061007400650050006C0061007900530065007300730069006F006E0050006C00610079006500640057006F006E0020006600610069006C00650064002E00002B430061006C00630075006C0061007400650050006C00610079006500640041006E00640057006F006E00003B5400720078005F00470065007400440065006C007400610050006C00610079006500640057006F006E0020006600610069006C00650064002E00003D5400720078005F0055007000640061007400650050006C00610079006500640041006E00640057006F006E0020006600610069006C00650064002E0000095700530032005300002543006C006F0073006500530065007300730069006F006E0054006F005A00650072006F00000F53006100730048006F0073007400004D430061006C006C0069006E006700200049006E00730065007200740057006300700043006F006D006D0061006E0064003A0020005400650072006D0069006E0061006C00490064003A0020000021200050006C0061007900530065007300730069006F006E00490064003A002000004D5400720078005F0050006C0061007900530065007300730069006F006E00530065007400460069006E0061006C00420061006C0061006E006300650020006600610069006C00650064002E0000515400720078005F0055007000640061007400650046006F0075006E00640041006E006400520065006D00610069006E0069006E00670049006E00450067006D0020006600610069006C00650064002E0000495400720078005F0050006C0061007900530065007300730069006F006E005400490054004F005300650074004D006500740065007200730020006600610069006C00650064002E0000395400720078005F0050006C0061007900530065007300730069006F006E0043006C006F007300650020006600610069006C00650064002E0000735400720078005F00470061006D00650047006100740065007700610079005F0052006500730065007200760065005F0043007200650064006900740020006600610069006C006500640021002100210020002D0020004100630063006F0075006E007400490064003A0020007B0031007D00012D55005000440041005400450020002000200050004C00410059005F00530045005300530049004F004E0053000061200020002000530045005400200020002000500053005F00520045005F0046004F0055004E0044005F0049004E005F00450047004D00200020002000200020003D002000400070005200450046006F0075006E00640049006E00450067006D00006320002000200020002000200020002C002000500053005F004E0052005F0046004F0055004E0044005F0049004E005F00450047004D00200020002000200020003D002000400070004E00520046006F0075006E00640049006E00450067006D002000006B20002000200020002000200020002C002000500053005F00520045005F00520045004D00410049004E0049004E0047005F0049004E005F00450047004D0020003D0020004000700052004500520065006D00610069006E0069006E00670049006E00450067006D002000006B20002000200020002000200020002C002000500053005F004E0052005F00520045004D00410049004E0049004E0047005F0049004E005F00450047004D0020003D002000400070004E005200520065006D00610069006E0069006E00670049006E00450067006D0020000065200057004800450052004500200020002000500053005F0050004C00410059005F00530045005300530049004F004E005F0049004400200020002000200020003D0020004000700050006C0061007900530065007300730069006F006E00490044002000001F4000700050006C0061007900530065007300730069006F006E0049004400001D400070005200450046006F0075006E00640049006E00450067006D00001D400070004E00520046006F0075006E00640049006E00450067006D0000254000700052004500520065006D00610069006E0069006E00670049006E00450067006D000025400070004E005200520065006D00610069006E0069006E00670049006E00450067006D00006F2000530045004C00450043005400200020002000430041005300450020005700480045004E002000410043005F00540059005000450020003D002000400070004100630063006F0075006E0074005600690072007400750061006C005400650072006D0069006E0061006C002000006F200020002000200020002000200020002000200020002000200020002000200020005400480045004E002000300020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000006F2000200020002000200020002000200020002000200020002000200020002000200045004C00530045002000490053004E0055004C004C002800410043005F0048004F004C004400450052005F004C004500560045004C002C002000300029002000200020002000200020002000006F200020002000200020002000200020002000200045004E004400200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000006F200020002000460052004F004D002000200020004100430043004F0055004E00540053002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000006F2000200057004800450052004500200020002000410043005F004100430043004F0055004E0054005F004900440020003D002000400070004100630063006F0075006E007400490064002000200020002000200020002000200020002000200020002000200020002000200020000031400070004100630063006F0075006E0074005600690072007400750061006C005400650072006D0069006E0061006C0000375400720078005F004700650074005400650072006D0069006E0061006C0049006E0066006F0020006600610069006C00650064002E0000415400720078005F005500700064006100740065004100630063006F0075006E007400420061006C0061006E006300650020006600610069006C00650064002E0000455400720078005F005500700064006100740065004100630063006F0075006E00740049006E00530065007300730069006F006E0020006600610069006C00650064002E00004B5400720078005F0055006E006C0069006E006B00500072006F006D006F00740069006F006E00730049006E00530065007300730069006F006E0020006600610069006C00650064002E00003355006E006500780070006500630074006500640020004D006F00760065006D0065006E00740054007900700065003A00200000415400720078005F0050006C0061007900530065007300730069006F006E005300650074004D006500740065007200730020006600610069006C00650064002E0000475400720078005F0050006C0061007900530065007300730069006F006E004300680061006E006700650053007400610074007500730020006600610069006C00650064002E00004F5400720078005F0049006E0073006500720074004100670072006F0075007000470061006D00650050006C0061007900530065007300730069006F006E0020006600610069006C00650064002E00003B5400720078005F00550070006400610074006500500073004100630063006F0075006E0074004900640020006600610069006C00650064002E00004B5400720078005F0052006500730065007400430061006E00630065006C006C00610062006C0065004F007000650072006100740069006F006E0020006600610069006C00650064002E0000475400720078005F0053006900740065004A00610063006B0070006F00740043006F006E0074007200690062007500740069006F006E0020006600610069006C00650064002E0000475400720078005F005400490054004F005F00430061006C00630075006C00610074006500500072006F006D006F0043006F007300740020006600610069006C00650064002E0000415400720078005F00500072006F006D006F004D00610072006B004100730045007800680061007500730074006500640020006600610069006C00650064002E0000354100630063006F0075006E0074004D006F00760065006D0065006E00740073005400610062006C0065002E0041006400640028000013290020006600610069006C00650064002E0000415400720078005F0050006C0061007900530065007300730069006F006E0054006F00500065006E00640069006E00670020006600610069006C00650064002E00006F2000200049004E005300450052005400200049004E0054004F002000500045004E00440049004E0047005F0050004C00410059005F00530045005300530049004F004E0053005F0054004F005F0050004C0041005900450052005F0054005200410043004B0049004E0047002000006F2000200028002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000006F20002000200020002000200020005000500053005F00530045005300530049004F004E005F00490044002C00200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000006F20002000200020002000200020005000500053005F004100430043004F0055004E0054005F00490044002C00200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000006F20002000200020002000200020005000500053005F0043004F0049004E005F0049004E002C00200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000006F20002000200020002000200020005000500053005F005400450052004D0049004E0041004C005F00490044002C0020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000006F20002000200020002000200020005000500053005F004400550052004100540049004F004E005F0054004F00540041004C005F004D0049004E0055005400450053002C00200020002000200020002000200020002000200020002000200020002000200020002000200020002000006F20002000200020002000200020005000500053005F0050004100520041004D0053005F004E0055004D0050004C0041005900450044002C00200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000006F20002000200020002000200020005000500053005F0050004100520041004D0053005F00420041004C0041004E00430045005F004D00490053004D0041005400430048002C0020002000200020002000200020002000200020002000200020002000200020002000200020002000006F20002000200020002000200020005000500053005F005400450052004D0049004E0041004C005F0054005900500045002C00200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000006F20002000200020002000200020005000500053005F004400410054004500540049004D004500430052004500410054004500440020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000006F2000200029002000560041004C00550045005300200028002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000006F200020002000200020002000200040007000700073005F00730065007300730069006F006E005F00690064002C0020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000006F200020002000200020002000200040007000700073005F006100630063006F0075006E0074005F00690064002C0020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000006F200020002000200020002000200040007000700073005F0063006F0069006E005F0069006E002C0020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000006F200020002000200020002000200040007000700073005F007400650072006D0069006E0061006C005F00690064002C002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000006F200020002000200020002000200040007000700073005F006400750072006100740069006F006E005F0074006F00740061006C005F006D0069006E0075007400650073002C0020002000200020002000200020002000200020002000200020002000200020002000200020002000006F200020002000200020002000200040007000700073005F0070006100720061006D0073005F006E0075006D0070006C0061007900650064002C0020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000006F200020002000200020002000200040007000700073005F0070006100720061006D0073005F00620061006C0061006E00630065005F006D00690073006D0061007400630068002C002000200020002000200020002000200020002000200020002000200020002000200020002000006F200020002000200020002000200040007000700073005F007400650072006D0069006E0061006C005F0074007900700065002C0020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000006F2000200020002000200020002000670065007400640061007400650028002900200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000006F2000200029002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000001F40007000700073005F00730065007300730069006F006E005F0069006400001F40007000700073005F006100630063006F0075006E0074005F0069006400001940007000700073005F0063006F0069006E005F0069006E00002140007000700073005F007400650072006D0069006E0061006C005F0069006400003740007000700073005F006400750072006100740069006F006E005F0074006F00740061006C005F006D0069006E007500740065007300002B40007000700073005F0070006100720061006D0073005F006E0075006D0070006C006100790065006400003940007000700073005F0070006100720061006D0073005F00620061006C0061006E00630065005F006D00690073006D006100740063006800002540007000700073005F007400650072006D0069006E0061006C005F007400790070006500004120002000550050004400410054004500200053004900540045005F004A00410043004B0050004F0054005F0050004100520041004D00450054004500520053000080F720002000200020002000530045005400200053004A0050005F0050004C004100590045004400200020003D00200053004A0050005F0050004C00410059004500440020002B002000430041005300450020005700480045004E002000280053004A0050005F004F004E004C0059005F00520045004400450045004D00410042004C00450020003D0020003100290020005400480045004E0020004000700054006F00740061006C00520065006400650065006D00610062006C00650050006C006100790065006400200045004C005300450020004000700054006F00740061006C0050006C006100790065006400200045004E004400003120002000200057004800450052004500200053004A0050005F0045004E00410042004C004500440020003D0020003100002F4000700054006F00740061006C00520065006400650065006D00610062006C00650050006C006100790065006400001B4000700054006F00740061006C0050006C0061007900650064000031200055005000440041005400450020002000200050004C00410059005F00530045005300530049004F004E00530020000080812000200020002000530045005400200020002000500053005F004E004F004E005F00520045004400450045004D00410042004C0045005F0043004100530048005F0049004E0020002000200020003D002000400070004E006F006E00520065006400650065006D00610062006C006500430061007300680049006E00200020000073200020002000200020002000200020002C002000500053005F00520045004400450045004D00410042004C0045005F0043004100530048005F0049004E00200020002000200020002000200020003D0020004000700054006F00740061006C0052006500430061007300680049006E0020000069200020002000200020002000200020002C002000500053005F00520045005F0043004100530048005F0049004E0020002000200020002000200020002000200020002000200020002000200020003D0020004000700052006500430061007300680049006E0020000073200020002000200020002000200020002C002000500053005F00500052004F004D004F005F00520045005F0043004100530048005F0049004E0020002000200020002000200020002000200020003D00200040007000500072006F006D006F0052006500430061007300680049006E002000008081200020002000200020002000200020002C002000500053005F004E004F004E005F00520045004400450045004D00410042004C0045005F0043004100530048005F004F00550054002000200020003D002000400070004E006F006E00520065006400650065006D00610062006C00650043006100730068004F007500740020000075200020002000200020002000200020002C002000500053005F00520045004400450045004D00410042004C0045005F0043004100530048005F004F005500540020002000200020002000200020003D0020004000700054006F00740061006C005200650043006100730068004F00750074002000006B200020002000200020002000200020002C002000500053005F00520045005F0043004100530048005F004F00550054002000200020002000200020002000200020002000200020002000200020003D002000400070005200650043006100730068004F007500740020000075200020002000200020002000200020002C002000500053005F00500052004F004D004F005F00520045005F0043004100530048005F004F00550054002000200020002000200020002000200020003D00200040007000500072006F006D006F005200650043006100730068004F00750074002000007F200020002000200020002000200020002C002000500053005F004E004F004E005F00520045004400450045004D00410042004C0045005F0050004C004100590045004400200020002000200020003D002000400070004E006F006E00520065006400650065006D00610062006C00650050006C00610079006500640020000079200020002000200020002000200020002C002000500053005F00520045004400450045004D00410042004C0045005F0050004C0041005900450044002000200020002000200020002000200020003D00200040007000520065006400650065006D00610062006C00650050006C0061007900650064002000006F200020002000200020002000200020002C002000500053005F0050004C0041005900450044005F0041004D004F0055004E00540020002000200020002000200020002000200020002000200020003D0020004000700054006F00740061006C0050006C00610079006500640020000079200020002000200020002000200020002C002000500053005F004E004F004E005F00520045004400450045004D00410042004C0045005F0057004F004E00200020002000200020002000200020003D002000400070004E006F006E00520065006400650065006D00610062006C00650057006F006E0020000073200020002000200020002000200020002C002000500053005F00520045004400450045004D00410042004C0045005F0057004F004E002000200020002000200020002000200020002000200020003D00200040007000520065006400650065006D00610062006C00650057006F006E0020000069200020002000200020002000200020002C002000500053005F0057004F004E005F0041004D004F0055004E00540020002000200020002000200020002000200020002000200020002000200020003D0020004000700054006F00740061006C0057006F006E0020000073200020002000200020002000200020002C002000500053005F00480041004E00440050004100590053005F0041004D004F0055004E005400200020002000200020002000200020002000200020003D00200040007000480061006E006400700061007900730041006D006F0075006E00740000752000200057004800450052004500200020002000500053005F0050004C00410059005F00530045005300530049004F004E005F0049004400200020002000200020002000200020002000200020003D0020004000700050006C0061007900530065007300730069006F006E004900640020002000002B400070004E006F006E00520065006400650065006D00610062006C006500430061007300680049006E00001F4000700054006F00740061006C0052006500430061007300680049006E0000154000700052006500430061007300680049006E00001F40007000500072006F006D006F0052006500430061007300680049006E00002D400070004E006F006E00520065006400650065006D00610062006C00650043006100730068004F007500740000214000700054006F00740061006C005200650043006100730068004F00750074000017400070005200650043006100730068004F0075007400002140007000500072006F006D006F005200650043006100730068004F0075007400002B400070004E006F006E00520065006400650065006D00610062006C00650050006C006100790065006400002540007000520065006400650065006D00610062006C00650050006C0061007900650064000025400070004E006F006E00520065006400650065006D00610062006C00650057006F006E00001F40007000520065006400650065006D00610062006C00650057006F006E0000154000700054006F00740061006C0057006F006E00002140007000480061006E006400700061007900730041006D006F0075006E007400006B2000200020002000530045005400200020002000500053005F00500052004F004D004F005F004E0052005F005400490043004B00450054005F0049004E00200020003D00200040007000500072006F006D006F004E0072005400690063006B006500740049006E002000006B200020002000200020002000200020002C002000500053005F00500052004F004D004F005F00520045005F005400490043004B00450054005F0049004E00200020003D00200040007000500072006F006D006F00520065005400690063006B006500740049006E0020000061200020002000200020002000200020002C002000500053005F00520045005F005400490043004B00450054005F0049004E00200020002000200020002000200020003D00200040007000520065005400690063006B006500740049006E002000006D200020002000200020002000200020002C002000500053005F00500052004F004D004F005F004E0052005F005400490043004B00450054005F004F005500540020003D00200040007000500072006F006D006F004E0072005400690063006B00650074004F007500740020000063200020002000200020002000200020002C002000500053005F00520045005F005400490043004B00450054005F004F005500540020002000200020002000200020003D00200040007000520065005400690063006B00650074004F007500740020000065200020002000200020002000200020002C002000500053005F0043004100530048005F0049004E0020002000200020002000200020002000200020002000200020003D00200040007000430061007300680049006E0041006D006F0075006E007400200000692000200057004800450052004500200020002000500053005F0050004C00410059005F00530045005300530049004F004E005F0049004400200020002000200020003D0020004000700050006C0061007900530065007300730069006F006E004900640020002000002340007000500072006F006D006F004E0072005400690063006B006500740049006E00002340007000500072006F006D006F00520065005400690063006B006500740049006E00001940007000520065005400690063006B006500740049006E00002540007000500072006F006D006F004E0072005400690063006B00650074004F0075007400001B40007000520065005400690063006B00650074004F0075007400001D40007000430061007300680049006E0041006D006F0075006E007400002720005500500044004100540045002000200020004100430043004F0055004E00540053002000005B2000200020002000530045005400200020002000410043005F00430041004E00430045004C004C00410042004C0045005F004F005000450052004100540049004F004E005F004900440020003D0020004E0055004C004C002000006D2000200057004800450052004500200020002000410043005F004100430043004F0055004E0054005F00490044002000200020002000200020002000200020002000200020002000200020003D002000400070004100630063006F0075006E007400490064002000200020000080834400450043004C00410052004500200040004F00750074007000750074005400610062006C00650020005400410042004C004500200028002000410043005F0049004E005F00530045005300530049004F004E005F0050004C004100590045004400200020002000200020002000200020002000200020004D004F004E00450059000080832000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F0057004F004E00200020002000200020002000200020002000200020002000200020004D004F004E00450059000080832000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F00420041004C0041004E004300450020002000200020002000200020004D004F004E00450059000080832000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F00520045005F00420041004C0041004E004300450020004D004F004E00450059000080872000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F00420041004C0041004E004300450020004D004F004E004500590029003B00004F2000200020002000530045005400200020002000410043005F0049004E005F00530045005300530049004F004E005F0050004C004100590045004400200020002000200020003D00200030002000004F200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F0057004F004E00200020002000200020002000200020003D00200030002000004F200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F0050004C004100590045004400200020003D00200030002000004F200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F004E0052005F0050004C004100590045004400200020003D00200030002000004F200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F0057004F004E00200020002000200020003D00200030002000004F200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F004E0052005F0057004F004E00200020002000200020003D00200030002000005B200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F00420041004C0041004E004300450020002000200020002000200020003D00200030002000005B200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F00520045005F00420041004C0041004E004300450020003D00200030002000005B200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F00420041004C0041004E004300450020003D00200030002000005B200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F0054004F005F0047004D002000200020002000200020002000200020003D00200030002000005B200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F00520045005F0054004F005F0047004D002000200020003D00200030002000005B200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F0054004F005F0047004D002000200020003D00200030002000005B200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F00460052004F004D005F0047004D0020002000200020002000200020003D00200030002000005B200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F00520045005F00460052004F004D005F0047004D0020003D00200030002000005B200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F00460052004F004D005F0047004D0020003D00200030002000006F200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00430041004E00430045004C004C00410042004C0045005F005400520041004E00530041004300540049004F004E005F004900440020003D00200030002000006F200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F00430041004E00430045004C004C00410042004C00450020002000200020002000200020002000200020002000200020003D00200030002000006F200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F00520045005F00430041004E00430045004C004C00410042004C00450020002000200020002000200020003D00200030002000006F200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F00430041004E00430045004C004C00410042004C00450020002000200020002000200020003D00200030002000004F20004F0055005400500055005400200020002000440045004C0045005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F0050004C00410059004500440020000049200020002000200020002000200020002C002000440045004C0045005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F0057004F004E0020000057200020002000200020002000200020002C002000440045004C0045005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F00520045005F00420041004C0041004E004300450020000063200020002000200020002000200020002C002000440045004C0045005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F00520045005F00420041004C0041004E004300450020000063200020002000200020002000200020002C002000440045004C0045005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F00420041004C0041004E00430045002000002F20002000200049004E0054004F0020002000200040004F00750074007000750074005400610062006C0065002000006F200020002000200041004E004400200020002000410043005F00430055005200520045004E0054005F0050004C00410059005F00530045005300530049004F004E005F004900440020003D0020004000700050006C0061007900530065007300730069006F006E00490064002000002B490046002000280020004000400052004F00570043004F0055004E00540020003D002000310020002900003B200020002000530045004C0045004300540020002A002000460052004F004D00200040004F00750074007000750074005400610062006C006500008081200020002000460052004F004D002000200020004100430043004F0055004E0054005F00500052004F004D004F00540049004F004E00530020005700490054004800280049004E004400450058002800490058005F006100630070005F006100630063006F0075006E0074005F00730074006100740075007300290029002000005B20002000570048004500520045002000200020004100430050005F004100430043004F0055004E0054005F004900440020002000200020002000200020003D002000400070004100630063006F0075006E0074004900640020000061200020002000200041004E0044002000200020004100430050005F0050004C00410059005F00530045005300530049004F004E005F004900440020003D0020004000700050006C0061007900530065007300730069006F006E004900640020000061200020002000200041004E0044002000200020004100430050005F005400520041004E00530041004300540049004F004E005F0049004400200020003D002000400070005400720061006E00730061006300740069006F006E004900640020000055200020002000200041004E0044002000200020004100430050005F0050004C00410059005F00530045005300530049004F004E005F004900440020004900530020004E004F00540020004E0055004C004C002000001F400070005400720061006E00730061006300740069006F006E0049006400004B20002000200020005300450054002000200020004100430050005F0050004C00410059005F00530045005300530049004F004E005F004900440020003D0020004E0055004C004C002000004B200020002000200020002000200020002C0020004100430050005F005400520041004E00530041004300540049004F004E005F0049004400200020003D0020004E0055004C004C002000005720002000570048004500520045002000200020004100430050005F0055004E0049005100550045005F004900440020002000200020002000200020003D0020004000700055006E0069007100750065004900640020000059200020002000200041004E0044002000200020004100430050005F004100430043004F0055004E0054005F00490044002000200020002000200020003D002000400070004100630063006F0075006E007400490064002000005F200020002000200041004E0044002000200020004100430050005F0053005400410054005500530020002000200020002000200020002000200020003D002000400070005300740061007400750073004100630074006900760065002000005120002000570048004500520045002000200020004100430050005F004100430043004F0055004E0054005F0049004400200020003D002000400070004100630063006F0075006E0074004900640020000057200020002000200041004E0044002000200020004100430050005F005300540041005400550053002000200020002000200020003D002000400070005300740061007400750073004100630074006900760065002000003D200020002000200041004E0044002000200020004100430050005F00570049005400480048004F004C00440020002000200020003E002000300020000080D120002000200020005300450054002000200020004100430050005F00570049005400480048004F004C00440020002000200020003D002000430041005300450020005700480045004E0020004100430050005F00420041004C0041004E004300450020003C0020004100430050005F00570049005400480048004F004C00440020005400480045004E0020004100430050005F00420041004C0041004E0043004500200045004C005300450020004100430050005F00570049005400480048004F004C004400200045004E0044002000004F20002000570048004500520045002000200020004100430050005F0055004E0049005100550045005F00490044002000200020003D0020004000700055006E0069007100750065004900640020000051200020002000200041004E0044002000200020004100430050005F004100430043004F0055004E0054005F0049004400200020003D002000400070004100630063006F0075006E0074004900640020000013700055006E00690071007500650049006400008081200020002000200041004E0044002000200020004100430050005F004300520045004400490054005F005400590050004500200049004E00200028004000700043007200650064006900740054007900700065004E00520031002C0020004000700043007200650064006900740054007900700065004E005200320029002000002340007000530074006100740075007300450078006800610075007300740065006400005B20002000200020005300450054002000200020004100430050005F005300540041005400550053002000200020002000200020003D00200040007000530074006100740075007300450078006800610075007300740065006400003D200020002000200041004E0044002000200020004100430050005F00420041004C0041004E0043004500200020002000200020003D00200030002000004F200020002000530045004C0045004300540020002000200054004F005000200031002000540049005F004100430043004F0055004E0054005F00500052004F004D004F00540049004F004E002000002920002000200020002000460052004F004D002000200020005400490043004B0045005400530020000075200020002000200057004800450052004500200020002000540049005F00430041004E00430045004C00450044005F0050004C00410059005F00530045005300530049004F004E005F004900440020003D0020004000700050006C0061007900530065007300730069006F006E00490064002000006F20002000200020002000200041004E004400200020002000540049005F0054005900500045005F00490044002000200020002000200020002000200020002000200020002000200020002000200020003D002000400070005400690063006B006500740054007900700065002000005320004F005200440045005200200042005900200020002000540049005F004C004100530054005F0041004300540049004F004E005F004400410054004500540049004D0045002000440045005300430020000019400070005400690063006B00650074005400790070006500003D200020005500500044004100540045002000200020004100430043004F0055004E0054005F00500052004F004D004F00540049004F004E0053002000008093200020002000200020005300450054002000200020004100430050005F00520045004400450045004D00410042004C0045005F0043004F005300540020003D002000430041005300450020005700480045004E002000200020004100430050005F00520045004400450045004D00410042004C0045005F0043004F005300540020004900530020004E0055004C004C002000006D200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020005400480045004E002000200020004000700041006D006F0075006E00740020000080A12000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200045004C00530045002000200020004100430050005F00520045004400450045004D00410042004C0045005F0043004F005300540020002B0020004000700041006D006F0075006E007400200045004E00440020000061200020002000570048004500520045002000200020004100430050005F0055004E0049005100550045005F004900440020003D002000400070004100630063006F0075006E007400500072006F006D006F00740069006F006E004900640020000029400070004100630063006F0075006E007400500072006F006D006F00740069006F006E00490064000080A520004400450043004C00410052004500200040004D006F006400650052006500730065007200760065006400200041005300200049004E005400200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000080A520002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000080A52000530045005400200040004D006F00640065005200650073006500720076006500640020003D0020002800530045004C004500430054002000490053004E0055004C004C0028002800530045004C00450043005400200020002000430041005300540020002800470050005F004B00450059005F00560041004C0055004500200041005300200049004E005400290020002000200020002000200020002000200020000080A5200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000460052004F004D00200020002000470045004E004500520041004C005F0050004100520041004D00530020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000080A52000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200057004800450052004500200020002000470050005F00470052004F00550050005F004B00450059002000200020003D0020002700470061006D006500470061007400650077006100790027002000200020002000200020000180A5200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200041004E004400200020002000470050005F005300550042004A004500430054005F004B004500590020003D002000270045006E00610062006C0065006400270020002000200020002000200020002000200020000180A5200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200041004E004400200020002000490053004E0055004D0045005200490043002800470050005F004B00450059005F00560041004C0055004500290020003D0020003100200029002C002000300029002900200020000080A52000490046002000280040004D006F00640065005200650073006500720076006500640020003D00200031002900200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000080A5200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200041004E004400200020002000470050005F005300550042004A004500430054005F004B004500590020003D0020002700520065007300650072007600650064002E0045006E00610062006C0065006400270020000180A5200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200041004E0044002000200020002800490053004E0055004D0045005200490043002800470050005F004B00450059005F00560041004C0055004500290020003D0020003100200020002000200020002000200020000080A5200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020004F0052002000490053004E0055004D0045005200490043002800470050005F004B00450059005F00560041004C0055004500290020003D00200032002900200029002C0020003000290029000080A5200020002000530045005400200040004D006F00640065005200650073006500720076006500640020003D0020002800530045004C004500430054002000200043004100530054002800410043005F004D004F00440045005F0052004500530045005200560045004400200041005300200049004E00540029002000200020002000200020002000200020002000200020002000200020002000200020002000200020000080A520002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000460052004F004D002000200020004100430043004F0055004E0054005300200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000080A5200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200057004800450052004500200020002000410043005F004100430043004F0055004E0054005F004900440020003D002000400070004100630063006F0075006E0074004900640029002000200020002000200020002000200020002000200020002000200020002000200020002000200020000080A52000530045004C00450043005400200040004D006F00640065005200650073006500720076006500640020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000023530045004C00450043005400200020002000500056005F004E0041004D0045002000003920002000200020002000200020002C002000500056005F004F004E004C0059005F00520045004400450045004D00410042004C0045002000007D20002000460052004F004D00200020002000500052004F00560049004400450052005300200049004E004E004500520020004A004F0049004E0020005400450052004D0049004E0041004C00530020004F004E002000540045005F00500052004F0056005F004900440020003D002000500056005F00490044002000004F200057004800450052004500200020002000540045005F005400450052004D0049004E0041004C005F004900440020003D002000400070005400650072006D0069006E0061006C00490064002000001B4100430050005F0055004E0049005100550045005F0049004400001F4100430050005F004300520045004400490054005F00540059005000450000174100430050005F00420041004C0041004E0043004500008089200020002000200020002000200020002C002000430041005300450020005700480045004E00200050004D005F0052004500530054005200490043005400450044005F0054004F005F005400450052004D0049004E0041004C005F004C0049005300540020004900530020004E004F00540020004E0055004C004C0020005400480045004E00200000815D20002000200020002000200020002000200020002000200020002000200020002000490053004E0055004C004C0028002800530045004C00450043005400200031002000460052004F004D0020005400450052004D0049004E0041004C005F00470052004F005500500053002000570048004500520045002000540047005F005400450052004D0049004E0041004C005F004900440020003D002000400070005400650072006D0069006E0061006C0049006400200041004E0044002000540047005F0045004C0045004D0045004E0054005F004900440020003D0020004100430050005F00500052004F004D004F005F0049004400200041004E0044002000540047005F0045004C0045004D0045004E0054005F00540059005000450020003D0020004000700045006C0065006D0065006E0074005400790070006500500072006F006D006F00740069006F006E0029002C002000300029002000003520002000200020002000200020002000200020002000200020002000200045004C005300450020003100200045004E0044002000002D200020002000200020002000200020002C0020004100430050005F00420041004C0041004E0043004500200000808120002000200049004E004E0045005200200020004A004F0049004E002000500052004F004D004F00540049004F004E00530020004F004E00200050004D005F00500052004F004D004F00540049004F004E005F004900440020003D0020004100430050005F00500052004F004D004F005F004900440020002000200020002000006D200020002000200041004E0044002000200020004100430050005F004300520045004400490054005F005400590050004500200049004E0020002000200028004000700054007900700065004E00520031002C0020004000700054007900700065004E005200320029002000008097200020002000200041004E004400200028002000280020004100430050005F00420041004C0041004E004300450020003E00200030002000290020004F0052002000280020004100430050005F00420041004C0041004E004300450020003D0020003000200041004E00440020004100430050005F00570049005400480048004F004C00440020003E00200030002000290029002000003B200020004F00520044004500520020004200590020004100430050005F0055004E0049005100550045005F00490044002000410053004300200000134000700054007900700065004E005200310000134000700054007900700065004E0052003200002D4000700045006C0065006D0065006E0074005400790070006500500072006F006D006F00740069006F006E0000395500500044004100540045002000200020004100430043004F0055004E0054005F00500052004F004D004F00540049004F004E0053002000005F2000200020005300450054002000200020004100430050005F0050004C00410059005F00530045005300530049004F004E005F004900440020003D0020004000700050006C0061007900530065007300730069006F006E00490064002000005F20002000200020002000200020002C0020004100430050005F005400520041004E00530041004300540049004F004E005F0049004400200020003D002000400070005400720061006E00730061006300740069006F006E0049006400200000434F0055005400500055005400200020002000440045004C0045005400450044002E004100430050005F004300520045004400490054005F0054005900500045002000004320002000200020002000200020002C002000440045004C0045005400450044002E004100430050005F00420041004C0041004E004300450020002000200020002000007F20002000460052004F004D002000200020004100430043004F0055004E0054005F00500052004F004D004F00540049004F004E00530020005700490054004800280049004E00440045005800280050004B005F006100630063006F0075006E0074005F00700072006F006D006F00740069006F006E00730029002900200000552000570048004500520045002000200020004100430050005F0055004E0049005100550045005F004900440020002000200020002000200020003D0020004000700055006E006900710075006500490064002000005720002000200041004E0044002000200020004100430050005F004100430043004F0055004E0054005F00490044002000200020002000200020003D002000400070004100630063006F0075006E007400490064002000005D20002000200041004E0044002000200020004100430050005F0053005400410054005500530020002000200020002000200020002000200020003D002000400070005300740061007400750073004100630074006900760065002000006B20002000200041004E0044002000200020004100430050005F004300520045004400490054005F005400590050004500200049004E0020002000200028004000700054007900700065004E00520031002C0020004000700054007900700065004E00520032002900200000809520002000200041004E004400200028002000280020004100430050005F00420041004C0041004E004300450020003E00200030002000290020004F0052002000280020004100430050005F00420041004C0041004E004300450020003D0020003000200041004E00440020004100430050005F00570049005400480048004F004C00440020003E00200030002000290029002000004B20002000200041004E0044002000200020004100430050005F0050004C00410059005F00530045005300530049004F004E005F004900440020004900530020004E0055004C004C00200000815D530045004C004500430054002000490053004E0055004C004C0020002800200028002000530045004C004500430054002000430041005300540020002800470050005F004B00450059005F00560041004C0055004500200041005300200049004E00540029002000460052004F004D002000470045004E004500520041004C005F0050004100520041004D0053002000570048004500520045002000470050005F00470052004F00550050005F004B004500590020003D002000270043007200650064006900740073002700200041004E0044002000470050005F005300550042004A004500430054005F004B004500590020003D002000270050006C00610079004D006F00640065002700200041004E0044002000490053004E0055004D0045005200490043002800470050005F004B00450059005F00560041004C0055004500290020003D0020003100200029002C0020003000200029002000013D5400720078005F00470065007400430072006500640069007400730050006C00610079004D006F006400650020006600610069006C00650064002E000031200020002000530045004C004500430054002000200020004100430050005F00420041004C0041004E00430045002000003320002000200020002000200020002000200020002C0020004100430050005F00570049005400480048004F004C0044002000003120002000200020002000200020002000200020002C0020004100430050005F0057004F004E004C004F0043004B002000002F20002000200020002000200020002000200020002C0020004100430050005F0050004C0041005900450044002000002920002000200020002000200020002000200020002C0020004100430050005F0057004F004E002000003920002000200020002000200020002000200020002C0020004100430050005F004300520045004400490054005F0054005900500045002000003520002000200020002000200020002000200020002C0020004100430050005F0055004E0049005100550045005F0049004400200000808720002000200020002000460052004F004D002000200020004100430043004F0055004E0054005F00500052004F004D004F00540049004F004E005300200057004900540048002000280049004E004400450058002800490058005F006100630070005F006100630063006F0075006E0074005F00730074006100740075007300290029002000005D2000200020002000570048004500520045002000200020004100430050005F004100430043004F0055004E0054005F00490044002000200020002000200020003D002000400070004100630063006F0075006E007400490064002000006320002000200020002000200041004E0044002000200020004100430050005F0053005400410054005500530020002000200020002000200020002000200020003D002000400070005300740061007400750073004100630074006900760065002000006520002000200020002000200041004E0044002000200020004100430050005F0050004C00410059005F00530045005300530049004F004E005F004900440020003D0020004000700050006C0061007900530065007300730069006F006E00490064002000003D20004F0052004400450052002000420059002000200020004100430050005F0055004E0049005100550045005F004900440020004100530043002000003755006E006B006E006F0077006E00200043007200650064006900740050006C00610079004D006F00640065003A0020007B0030007D0000532000200020005300450054002000200020004100430050005F00420041004C0041004E00430045002000200020002000200020002000200020003D00200040007000420061006C0061006E00630065002000004B20002000200020002000200020002C0020004100430050005F0057004F004E0020002000200020002000200020002000200020002000200020003D0020004000700057006F006E002000005120002000200020002000200020002C0020004100430050005F0050004C00410059004500440020002000200020002000200020002000200020003D0020004000700050006C0061007900650064002000005F20002000200041004E0044002000200020004100430050005F0050004C00410059005F00530045005300530049004F004E005F004900440020003D0020004000700050006C0061007900530065007300730069006F006E00490064002000001340007000420061006C0061006E0063006500000B4000700057006F006E00000F4100430050005F0057004F004E0000114000700050006C00610079006500640000154100430050005F0050004C0041005900450044000067430061006E0027007400200075007000640061007400650020006100630063006F0075006E0074002000700072006F006D006F00740069006F006E0073002E002000550070006400610074006500640020007B0030007D0020006F00660020007B0031007D0001809B500072006F006D006F00740069006F006E00200055006E006900710075006500490064003A0020007B0030007D002C00200043007200650064006900740054007900700065003A0020007B0031007D002C002000420061006C0061006E00630065003A0020007B0032007D002C00200050006C0061007900650064003A0020007B0033007D002C00200057006F006E003A0020007B0034007D00000F4500720072006F0072003A0020000027510075006500720079002E0043006F006D006D0061006E00640054006500780074003A002000002750006100720061006D00650074006500720020007B0030007D0020003D0020007B0031007D0000754400450043004C00410052004500200040004F00750074007000750074005400610062006C00650020005400410042004C004500200028002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F0050004C00410059004500440020004D004F004E004500590000752000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F004E0052005F0050004C00410059004500440020004D004F004E004500590000752000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F0057004F004E0020002000200020004D004F004E0045005900007B2000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F004E0052005F0057004F004E0020002000200020004D004F004E0045005900200029003B000080CD2000200020002000530045005400200020002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F00420041004C0041004E004300450020002000200020002000200020003D002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F00420041004C0041004E004300450020002000200020002000200020002B0020004000700049006E00530065007300730069006F006E0052006500420061006C0061006E00630065002000200020002000200020000080CD200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F00520045005F00420041004C0041004E004300450020003D002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F00520045005F00420041004C0041004E004300450020002B0020004000700049006E00530065007300730069006F006E00500072006F006D006F0052006500420061006C0061006E006300650020000080CD200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F00420041004C0041004E004300450020003D002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F00420041004C0041004E004300450020002B0020004000700049006E00530065007300730069006F006E00500072006F006D006F004E007200420061006C0061006E0063006500200000808B2000200020002000530045005400200020002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F00420041004C0041004E004300450020002000200020002000200020003D0020004000700049006E00530065007300730069006F006E0052006500420061006C0061006E006300650020002000200020002000200000808B200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F00520045005F00420041004C0041004E004300450020003D0020004000700049006E00530065007300730069006F006E00500072006F006D006F0052006500420061006C0061006E0063006500200000808B200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F00420041004C0041004E004300450020003D0020004000700049006E00530065007300730069006F006E00500072006F006D006F004E007200420061006C0061006E006300650020000080B3200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F0050004C004100590045004400200020002000200020002000200020003D002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F0050004C00410059004500440020002B0020004000700049006E00530065007300730069006F006E005200650050006C00610079006500640020000080B3200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F004E0052005F0050004C004100590045004400200020002000200020002000200020003D002000410043005F0049004E005F00530045005300530049004F004E005F004E0052005F0050004C00410059004500440020002B0020004000700049006E00530065007300730069006F006E004E00720050006C00610079006500640020000080AD200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F0057004F004E00200020002000200020002000200020002000200020003D002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F0057004F004E0020002000200020002B0020004000700049006E00530065007300730069006F006E005200650057006F006E0020000080AD200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F004E0052005F0057004F004E00200020002000200020002000200020002000200020003D002000410043005F0049004E005F00530045005300530049004F004E005F004E0052005F0057004F004E0020002000200020002B0020004000700049006E00530065007300730069006F006E004E00720057006F006E0020000080AF200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F0050004C004100590045004400200020002000200020002000200020002000200020003D002000410043005F0049004E005F00530045005300530049004F004E005F0050004C00410059004500440020002000200020002D0020004000700049006E00530065007300730069006F006E0050006C00610079006500640020000180A9200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F0057004F004E00200020002000200020002000200020002000200020002000200020003D002000410043005F0049004E005F00530045005300530049004F004E005F0057004F004E0020002000200020002000200020002D0020004000700049006E00530065007300730069006F006E0057006F006E002000015920004F005500540050005500540020002000200049004E005300450052005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F00520045005F0050004C004100590045004400200020000059200020002000200020002000200020002C00200049004E005300450052005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F004E0052005F0050004C004100590045004400200020000059200020002000200020002000200020002C00200049004E005300450052005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F00520045005F0057004F004E00200020002000200020000059200020002000200020002000200020002C00200049004E005300450052005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F004E0052005F0057004F004E00200020002000200020000027200020002000460052004F004D002000200020004100430043004F0055004E0054005300200000294000700049006E00530065007300730069006F006E0052006500420061006C0061006E006300650000334000700049006E00530065007300730069006F006E00500072006F006D006F0052006500420061006C0061006E006300650000334000700049006E00530065007300730069006F006E00500072006F006D006F004E007200420061006C0061006E006300650000274000700049006E00530065007300730069006F006E005200650050006C00610079006500640000274000700049006E00530065007300730069006F006E004E00720050006C00610079006500640000214000700049006E00530065007300730069006F006E005200650057006F006E0000214000700049006E00530065007300730069006F006E004E00720057006F006E0000234000700049006E00530065007300730069006F006E0050006C006100790065006400001D4000700049006E00530065007300730069006F006E0057006F006E000055430061006E002700740020007500700064006100740065002000270069006E002000730065007300730069006F006E00270020006100630063006F0075006E0074002C002000490064003A0020007B0030007D000180834400450043004C00410052004500200040004F00750074007000750074005400610062006C00650020005400410042004C004500200028002000440045004C00540041005F0050004C00410059004500440020002000200020002000200020002000200020002000200020002000200020002000200020004D004F004E00450059000080832000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000440045004C00540041005F0057004F004E0020002000200020002000200020002000200020002000200020002000200020002000200020002000200020004D004F004E00450059000080832000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F00420041004C0041004E004300450020004D004F004E00450059000080892000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000410043005F004100430043004F0055004E0054005F004900440020002000200020002000200020002000200020002000200020002000200020002000200042004900470049004E00540029003B000081132000200020002000530045005400200020002000410043005F0049004E005F00530045005300530049004F004E005F0050004C00410059004500440020003D002000430041005300450020005700480045004E0020002800500053005F0050004C0041005900450044005F0041004D004F0055004E00540020003E002000410043005F0049004E005F00530045005300530049004F004E005F0050004C004100590045004400290020005400480045004E002000500053005F0050004C0041005900450044005F0041004D004F0055004E005400200045004C00530045002000410043005F0049004E005F00530045005300530049004F004E005F0050004C004100590045004400200045004E0044002000008113200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F0057004F004E0020002000200020003D002000430041005300450020005700480045004E0020002800500053005F0057004F004E005F0041004D004F0055004E00540020002000200020003E002000410043005F0049004E005F00530045005300530049004F004E005F0057004F004E00290020002000200020005400480045004E002000500053005F0057004F004E005F0041004D004F0055004E005400200020002000200045004C00530045002000410043005F0049004E005F00530045005300530049004F004E005F0057004F004E00200020002000200045004E00440020000080AF20004F005500540050005500540020002000200049004E005300450052005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F0050004C00410059004500440020002D002000440045004C0045005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F0050004C0041005900450044002000410053002000440045004C00540041005F0050004C00410059004500440020000180AF200020002000200020002000200020002C00200049004E005300450052005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F0057004F004E0020002000200020002D002000440045004C0045005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F0057004F004E002000200020002000410053002000440045004C00540041005F0057004F004E0020002000200020000141200020002000200020002000200020002C002000440045004C0045005400450044002E00410043005F004100430043004F0055004E0054005F004900440020000080C3200020002000460052004F004D0020002000200050004C00410059005F00530045005300530049004F004E005300200041005300200050005300200049004E004E004500520020004A004F0049004E0020004100430043004F0055004E0054005300200041005300200041004300430020004F004E002000500053002E00500053005F004100430043004F0055004E0054005F004900440020003D0020004100430043002E00410043005F004100430043004F0055004E0054005F0049004400200000652000200057004800450052004500200020002000500053002E00500053005F0050004C00410059005F00530045005300530049004F004E005F004900440020003D0020004000700050006C0061007900530065007300730069006F006E004900640020000080832000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000420049004C004C005F0049004E002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020004D004F004E00450059000031200020002000200020002000200020002C002000700073002E00700073005F0063006100730068005F0069006E00200000474400450043004C00410052004500200040004100630063006F0075006E007400490064005F0074006F00550070006400610074006500200042004900470049004E005400200000634400450043004C00410052004500200040004F00750074007000750074005400610062006C00650020005400410042004C004500200028002000410043005F004100430043004F0055004E0054005F0049004400200042004900470049004E0054000047200020002000200020002000200020002C002000410043005F0042004C004F0043004B00450044002000200020002000200020002000200020002000200020004200490054000047200020002000200020002000200020002C002000410043005F0042004C004F0043004B005F0052004500410053004F004E00200020002000200020002000200049004E0054000047200020002000200020002000200020002C002000410043005F0048004F004C004400450052005F004C004500560045004C00200020002000200020002000200049004E0054000047200020002000200020002000200020002C002000410043005F0048004F004C004400450052005F00470045004E0044004500520020002000200020002000200049004E005400005B200020002000200020002000200020002C002000410043005F0048004F004C004400450052005F004E0041004D004500200020002000200020002000200020004E00560041005200430048004100520028003200300030002900004B200020002000200020002000200020002C002000410043005F00520045005F00420041004C0041004E00430045002000200020002000200020002000200020004D004F004E0045005900004B200020002000200020002000200020002C002000410043005F00500052004F004D004F005F00520045005F00420041004C0041004E00430045002000200020004D004F004E0045005900004B200020002000200020002000200020002C002000410043005F00500052004F004D004F005F004E0052005F00420041004C0041004E00430045002000200020004D004F004E0045005900004B200020002000200020002000200020002C0020004300420055005F00560041004C005500450020002000200020002000200020002000200020002000200020004D004F004E0045005900005D200020002000200020002000200020002C002000410043005F00430055005200520045004E0054005F0050004C00410059005F00530045005300530049004F004E005F0049004400200020002000200042004900470049004E0054000057200020002000200020002000200020002C002000410043005F00430055005200520045004E0054005F005400450052004D0049004E0041004C005F00490044002000200020002000200020002000200049004E0054000079200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00430041004E00430045004C004C00410042004C0045005F005400520041004E00530041004300540049004F004E005F0049004400200020002000200042004900470049004E0054000077200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F00430041004E00430045004C004C00410042004C00450020002000200020002000200020002000200020002000200020002000200020004D004F004E00450059000077200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F00520045005F00430041004E00430045004C004C00410042004C00450020002000200020002000200020002000200020004D004F004E00450059000077200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F00430041004E00430045004C004C00410042004C00450020002000200020002000200020002000200020004D004F004E00450059000063200020002000200020002000200020002C002000410043005F00430041004E00430045004C004C00410042004C0045005F004F005000450052004100540049004F004E005F004900440020002000200042004900470049004E005400200020002000005F200020002000200020002000200020002C002000410043005F00520045005F005200450053004500520056004500440020002000200020002000200020004D004F004E004500590020002000200020002000200020002000200020002000005F200020002000200020002000200020002C002000410043005F004D004F00440045005F0052004500530045005200560045004400200020002000200020004200490054002000200020002000200020002000200020002000200020002000005F200020002000200020002000200020002C002000420055005F004E0052005F004300520045004400490054002000200020002000200020002000200020004D004F004E004500590020002000200020002000200020002000200020002000005F200020002000200020002000200020002C002000420055005F00520045005F004300520045004400490054002000200020002000200020002000200020004D004F004E004500590020002000200020002000200020002000200020002000005F200020002000200020002000200020002C002000410043005F0054005900500045002000200020002000200020002000200020002000200020002000200049004E0054002000200020002000200020002000200020002000200020002000005F200020002000200020002000200020002C002000410043005F00500052004F0056004900530049004F004E002000200020002000200020002000200020004D004F004E0045005900200020002000200020002000200020002000200020000017200020002000200020002000200029003B0020002000003D530045005400200040004100630063006F0075006E007400490064005F0074006F0055007000640061007400650020003D0020004E0055004C004C000055530045004C00450043005400200040004100630063006F0075006E007400490064005F0074006F0055007000640061007400650020003D002000410043005F004100430043004F0055004E0054005F0049004400001B460052004F004D0020004100430043004F0055004E00540053000080C74C0045004600540020004A004F0049004E00200043005500530054004F004D00450052005F004200550043004B004500540020004F004E0020002800410043005F004100430043004F0055004E0054005F004900440020003D0020004300420055005F0043005500530054004F004D00450052005F00490044002900200041004E004400200028004300420055005F004200550043004B00450054005F004900440020003D002000400070004200750063006B0065007400490064005F00500054002900200000512000200057004800450052004500200020002000410043005F004100430043004F0055004E0054005F00490044002000200020003D002000400070004100630063006F0075006E00740049006400200000808F200020005700480045005200450020002000280020002000200028002000410043005F005400590050004500200020003D00200020003300200041004E0044002000410043005F0054005200410043004B005F0044004100540041002000200020003D0020004000700041006C00650073006900730054007200610063006B0044006100740061002000290020000080C120002000200020002000200020002000200020004F005200200028002000410043005F00540059005000450020003C003E00200020003300200041004E0044002000410043005F0054005200410043004B005F0044004100540041002000200020003D002000640062006F002E0054007200610063006B00440061007400610054006F0049006E007400650072006E0061006C002000280040007000570069006E0054007200610063006B0044006100740061002900200029002000290020000073200020002000200041004E0044002000200020002800410043005F00520045005F00420041004C0041004E0043004500200020002000200020002000200020002B0020004000700052006500420061006C0061006E006300650020002000200020002000290020003E003D002000300020000073200020002000200041004E0044002000200020002800410043005F00500052004F004D004F005F00520045005F00420041004C0041004E0043004500200020002B00200040007000500072006F006D006F0052006500420061006C0061006E0063006500290020003E003D002000300020000073200020002000200041004E0044002000200020002800410043005F00500052004F004D004F005F004E0052005F00420041004C0041004E0043004500200020002B00200040007000500072006F006D006F004E007200420061006C0061006E0063006500290020003E003D002000300020000073200020002000200041004E0044002000200020002800490053004E0055004C004C0028004300420055005F00560041004C00550045002C0020003000290020002B0020004000700050006F0069006E007400730020002000200020002000200020002000290020003E003D002000300020000077200020002000200055005000440041005400450020002000200043005500530054004F004D00450052005F004200550043004B00450054002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000007720002000200020002000200020005300450054002000200020004300420055005F00560041004C005500450020003D002000490053004E0055004C004C0028004300420055005F00560041004C00550045002C0020003000290020002B0020004000700050006F0069006E0074007300200020002000007B20002000200020002000570048004500520045002000200020004300420055005F0043005500530054004F004D00450052005F004900440020003D002000490053004E0055004C004C00280040004100630063006F0075006E007400490064005F0074006F005500700064006100740065002C002000300029000057200020002000200020002000200041004E0044002000200020004300420055005F004200550043004B00450054005F004900440020003D002000400070004200750063006B0065007400490064005F00500054002000007520004900460020004000400052004F00570043004F0055004E00540020003D0020003000200041004E0044002000490053004E0055004C004C00280040004100630063006F0075006E007400490064005F0074006F005500700064006100740065002C0020003000290020003C003E00200030000061200042004500470049004E00200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000006120002000200049004E005300450052005400200049004E0054004F00200043005500530054004F004D00450052005F004200550043004B0045005400200028004300420055005F0043005500530054004F004D00450052005F0049004400200000612000200020002000200020002000200020002000200020002000200020002000200020002C0020004300420055005F004200550043004B00450054005F004900440020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002000200020002000200020002C0020004300420055005F00560041004C0055004500200020002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002000200020002000200020002C0020004300420055005F005500500044004100540045004400200020002000200020002000200020002000200020002000200020002000200020000061200020002000200020002000200020002000200020002000200020002000200020002000290020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000006120002000200020002000200020002000200020002000200020002000560041004C005500450053002000280040004100630063006F0075006E007400490064005F0074006F00550070006400610074006500200020002000200020002000200000612000200020002000200020002000200020002000200020002000200020002000200020002C002000400070004200750063006B0065007400490064005F005000540020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002000200020002000200020002C0020004000700050006F0069006E00740073002000200020002000200020002000200020002000200020002000200020002000200020002000200000612000200020002000200020002000200020002000200020002000200020002000200020002C00200047004500540044004100540045002800290020002000200020002000200020002000200020002000200020002000200020002000200020000061200045004E0044002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000811D2000200020002000530045005400200020002000410043005F00420041004C0041004E004300450020002000200020002000200020002000200020003D002000410043005F00520045005F00420041004C0041004E004300450020002B002000410043005F00500052004F004D004F005F00520045005F00420041004C0041004E004300450020002B002000410043005F00500052004F004D004F005F004E0052005F00420041004C0041004E004300450020002B0020004000700052006500420061006C0061006E006300650020002B00200040007000500072006F006D006F0052006500420061006C0061006E006300650020002B00200040007000500072006F006D006F004E007200420061006C0061006E00630065002000008085200020002000200020002000200020002C002000410043005F00520045005F00420041004C0041004E004300450020002000200020002000200020003D002000410043005F00520045005F00420041004C0041004E004300450020002000200020002000200020002B0020004000700052006500420061006C0061006E0063006500200000808F200020002000200020002000200020002C002000410043005F00500052004F004D004F005F00520045005F00420041004C0041004E004300450020003D002000410043005F00500052004F004D004F005F00520045005F00420041004C0041004E004300450020002B00200040007000500072006F006D006F0052006500420061006C0061006E0063006500200000808F200020002000200020002000200020002C002000410043005F00500052004F004D004F005F004E0052005F00420041004C0041004E004300450020003D002000410043005F00500052004F004D004F005F004E0052005F00420041004C0041004E004300450020002B00200040007000500072006F006D006F004E007200420061006C0061006E00630065002000008093200020002000200020002000200020002C002000410043005F00500052004F0056004900530049004F004E00200020002000200020002000200020003D002000410043005F00500052004F0056004900530049004F004E00200020002000200020002000200020002B0020004000700043007500730074006F0064007900500072006F0076006900730069006F006E002000008119200020002000200020002000200020002C002000410043005F00520045005F005200450053004500520056004500440020002000200020002000200020003D002000430041005300450020005700480045004E002000410043005F00520045005F0052004500530045005200560045004400200020002B002000400070005200650073006500720076006500640020003E0020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000081192000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000410043005F00520045005F00420041004C0041004E00430045002000200020002B002000410043005F00500052004F004D004F005F00520045005F00420041004C0041004E004300450020002B0020004000700052006500420061006C0061006E006300650020002B00200040007000500072006F006D006F0052006500420061006C0061006E00630065002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000081192000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020005400480045004E002000410043005F00520045005F00420041004C0041004E0043004500200020002B002000410043005F00500052004F004D004F005F00520045005F00420041004C0041004E004300450020002B0020004000700052006500420061006C0061006E006300650020002B00200040007000500072006F006D006F0052006500420061006C0061006E006300650020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000811920002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200045004C00530045002000410043005F00520045005F005200450053004500520056004500440020002B0020004000700052006500730065007200760065006400200045004E004400200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000061200020002000200020002000200020002C002000410043005F004D004F00440045005F0052004500530045005200560045004400200020002000200020003D002000400070004D006F00640065005200650073006500720076006500640020000065200020002000200020002000200020002C002000410043005F004D004F00440045005F0052004500530045005200560045004400200020002000200020003D002000410043005F004D004F00440045005F00520045005300450052005600450044002000004320004F005500540050005500540020002000200049004E005300450052005400450044002E00410043005F004100430043004F0055004E0054005F004900440020000049200020002000200020002000200020002C00200049004E005300450052005400450044002E00410043005F0042004C004F0043004B004500440020002000200020002000200020000049200020002000200020002000200020002C00200049004E005300450052005400450044002E00410043005F0042004C004F0043004B005F0052004500410053004F004E00200020000049200020002000200020002000200020002C00200049004E005300450052005400450044002E00410043005F0048004F004C004400450052005F004C004500560045004C0020002000005F200020002000200020002000200020002C002000490053004E0055004C004C00280049004E005300450052005400450044002E00410043005F0048004F004C004400450052005F00470045004E004400450052002C002000300029002000005F200020002000200020002000200020002C002000490053004E0055004C004C00280049004E005300450052005400450044002E00410043005F0048004F004C004400450052005F004E0041004D0045002C00200020002700270029002000014F200020002000200020002000200020002C00200049004E005300450052005400450044002E00410043005F00520045005F00420041004C0041004E00430045002000200020002000200020002000004F200020002000200020002000200020002C00200049004E005300450052005400450044002E00410043005F00500052004F004D004F005F00520045005F00420041004C0041004E00430045002000004F200020002000200020002000200020002C00200049004E005300450052005400450044002E00410043005F00500052004F004D004F005F004E0052005F00420041004C0041004E004300450020000049200020002000200020002000200020002C002000490053004E0055004C004C002800430042005F00500054002E004300420055005F00560041004C00550045002C00200030002900005D200020002000200020002000200020002C00200049004E005300450052005400450044002E00410043005F00430055005200520045004E0054005F0050004C00410059005F00530045005300530049004F004E005F00490044002000005D200020002000200020002000200020002C00200049004E005300450052005400450044002E00410043005F00430055005200520045004E0054005F005400450052004D0049004E0041004C005F0049004400200020002000200020000079200020002000200020002000200020002C00200049004E005300450052005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F00430041004E00430045004C004C00410042004C0045005F005400520041004E00530041004300540049004F004E005F004900440020000061200020002000200020002000200020002C00200049004E005300450052005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F00520045005F00430041004E00430045004C004C00410042004C0045002000006D200020002000200020002000200020002C00200049004E005300450052005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F00520045005F00430041004E00430045004C004C00410042004C0045002000006D200020002000200020002000200020002C00200049004E005300450052005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F00430041004E00430045004C004C00410042004C00450020000075200020002000200020002000200020002C002000490053004E0055004C004C00280049004E005300450052005400450044002E00410043005F00430041004E00430045004C004C00410042004C0045005F004F005000450052004100540049004F004E005F00490044002C002000300029002000005F200020002000200020002000200020002C002000490053004E0055004C004C00280049004E005300450052005400450044002E00410043005F00520045005F00520045005300450052005600450044002C00200030002900200020002000005F200020002000200020002000200020002C00200049004E005300450052005400450044002E00410043005F004D004F00440045005F00520045005300450052005600450044002000200020002000200020002000200020002000200020000019200020002000200020002000200020002C002000300020000049200020002000200020002000200020002C002000490053004E0055004C004C002800430042005F004E0052002E004300420055005F00560041004C00550045002C0020003000290000452000200020002000200020002C002000490053004E0055004C004C002800430042005F00520045002E004300420055005F00560041004C00550045002C0020003000290000152000200020002000200020002C002000300020000037200020002000200020002000200020002C00200049004E005300450052005400450044002E00410043005F00540059005000450020000041200020002000200020002000200020002C00200049004E005300450052005400450044002E00410043005F00500052004F0056004900530049004F004E002000002B200020002000200020002000460052004F004D002000200020004100430043004F0055004E00540053000080ED49004E004E004500520020004A004F0049004E0020002000200043005500530054004F004D00450052005F004200550043004B00450054002000430042005F005000540020004F004E0020002000430042005F00500054002E004300420055005F0043005500530054004F004D00450052005F004900440020003D002000410043005F004100430043004F0055004E0054005F00490044002000200041004E00440020002000430042005F00500054002E004300420055005F004200550043004B00450054005F004900440020003D002000400070004200750063006B0065007400490064005F00500054000080ED4C00450046005400200020004A004F0049004E0020002000200043005500530054004F004D00450052005F004200550043004B00450054002000430042005F004E00520020004F004E0020002000430042005F004E0052002E004300420055005F0043005500530054004F004D00450052005F004900440020003D002000410043005F004100430043004F0055004E0054005F00490044002000200041004E00440020002000430042005F004E0052002E004300420055005F004200550043004B00450054005F004900440020003D002000400070004200750063006B0065007400490064005F004E0052000080F14C00450046005400200020004A004F0049004E0020002000200043005500530054004F004D00450052005F004200550043004B00450054002000430042005F005200450020004F004E00200020002000430042005F00520045002E004300420055005F0043005500530054004F004D00450052005F004900440020003D002000410043005F004100430043004F0055004E0054005F00490044002000200041004E004400200020002000430042005F00520045002E004300420055005F004200550043004B00450054005F004900440020003D002000400070004200750063006B0065007400490064005F005200450000772000200020002000200057004800450052004500200020002000410043005F004100430043004F0055004E0054005F004900440020003D002000490053004E0055004C004C00280040004100630063006F0075006E007400490064005F0074006F005500700064006100740065002C0020003000290000234000700041006C00650073006900730054007200610063006B004400610074006100001D40007000570069006E0054007200610063006B00440061007400610000174000700052006500420061006C0061006E0063006500002140007000500072006F006D006F0052006500420061006C0061006E0063006500002140007000500072006F006D006F004E007200420061006C0061006E006300650000254000700043007500730074006F0064007900500072006F0076006900730069006F006E0000114000700050006F0069006E007400730000154000700052006500730065007200760065006400001D400070004D006F006400650052006500730065007200760065006400001B400070004200750063006B0065007400490064005F0050005400001B400070004200750063006B0065007400490064005F004E005200001B400070004200750063006B0065007400490064005F00520045000029450078007400650072006E0061006C0054007200610063006B0044006100740061003A00200027000131270020002D002D003E0020004100630063006F0075006E00740020004E006F007400200046006F0075006E0064002E0001174100630063006F0075006E007400490064003A002000002F20002D002D003E0020004100630063006F0075006E00740020004E006F007400200046006F0075006E0064002E0001292000530045004C00450043005400200020002000500053005F0053005400410054005500530020000053200020002000200020002000200020002C002000490053004E0055004C004C002800500053005F00420041004C0041004E00430045005F004D00490053004D0041005400430048002C002000300029002000003B200020002000200020002000200020002C002000500053005F0049004E0049005400490041004C005F00420041004C0041004E00430045002000002B200020002000200020002000200020002C002000500053005F0043004100530048005F0049004E0020000031200020002000460052004F004D0020002000200050004C00410059005F00530045005300530049004F004E0053002000005F2000200057004800450052004500200020002000500053005F0050004C00410059005F00530045005300530049004F004E005F004900440020003D0020004000700050006C0061007900530065007300730069006F006E004900640020000023270020002D002D003E00200045007800630065007000740069006F006E003A002000012120002D002D003E00200045007800630065007000740069006F006E003A00200001255500500044004100540045002000200020004100430043004F0055004E00540053002000004F200020002000530045005400200020002000410043005F004C004100530054005F0041004300540049005600490054005900200020003D0020004700450054004400410054004500280029002000004D200057004800450052004500200020002000410043005F004100430043004F0055004E0054005F0049004400200020003D002000400070004100630063006F0075006E007400490064002000003B200020002000530045005400200020002000410043005F0042004C004F0043004B00450044002000200020002000200020003D002000310020000080B720002000200020002000200020002C002000410043005F0042004C004F0043004B005F0052004500410053004F004E0020003D002000640062006F002E0042006C006F0063006B0052006500610073006F006E005F0054006F004E006500770045006E0075006D00650072006100740065002800410043005F0042004C004F0043004B005F0052004500410053004F004E00290020007C0020004000700042006C006F0063006B0052006500610073006F006E002000004920002000200020002000200020002C002000410043005F0042004C004F0043004B005F004400450053004300520049005000540049004F004E0020003D0020004E0055004C004C00004F200057004800450052004500200020002000410043005F004100430043004F0055004E0054005F00490044002000200020003D002000400070004100630063006F0075006E00740049006400200000809920002000200041004E004400200020002000410043005F00540059005000450020004E004F005400200049004E0020002800400070004100630063006F0075006E0074005600690072007400750061006C0043006100730068006900650072002C002000400070004100630063006F0075006E0074005600690072007400750061006C005400650072006D0069006E0061006C0029002000001B4000700042006C006F0063006B0052006500610073006F006E00002F400070004100630063006F0075006E0074005600690072007400750061006C004300610073006800690065007200002D430061006E00630065006C00530074006100720074004300610072006400530065007300730069006F006E00003B4400450043004C00410052004500200040004F00750074007000750074005400610062006C00650020005400410042004C00450020002800200000672000200020002000200020002000200020002000410043005F004100430043004F0055004E0054005F0049004400200020002000200020002000200020002000200020002000200020002000200020002000200020002000200042004900470049004E0054000067200020002000200020002000200020002C002000410043005F00430055005200520045004E0054005F0050004C00410059005F00530045005300530049004F004E005F004900440020002000200020002000200020002000200042004900470049004E0054000065200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F00430041004E00430045004C004C00410042004C00450020002000200020002000200020004D004F004E00450059000065200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F00520045005F00430041004E00430045004C004C00410042004C00450020004D004F004E00450059000065200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F00430041004E00430045004C004C00410042004C00450020004D004F004E00450059000065200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F0054004F005F0047004D0020002000200020002000200020002000200020002000200020004D004F004E00450059000065200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F00520045005F0054004F005F0047004D0020002000200020002000200020004D004F004E00450059000065200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F0054004F005F0047004D0020002000200020002000200020004D004F004E00450059000065200020002000200020002000200020002C002000500053005F0049004E0049005400490041004C005F00420041004C0041004E0043004500200020002000200020002000200020002000200020002000200020002000200020004D004F004E00450059000065200020002000200020002000200020002C002000500053005F0043004100530048005F0049004E0020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020004D004F004E00450059000077200020002000200020002000200020002C002000540045005F004E0041004D00450020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020004E005600410052004300480041005200280035003000290029003B00006F2000200020002000530045005400200020002000410043005F0049004E005F00530045005300530049004F004E005F00430041004E00430045004C004C00410042004C0045005F005400520041004E00530041004300540049004F004E005F004900440020003D00200030002000006B20004F0055005400500055005400200020002000440045004C0045005400450044002E00410043005F004100430043004F0055004E0054005F00490044002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000006B200020002000200020002000200020002C002000440045004C0045005400450044002E00410043005F00430055005200520045004E0054005F0050004C00410059005F00530045005300530049004F004E005F0049004400200020002000200020002000200020002000006B200020002000200020002000200020002C002000440045004C0045005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F00520045005F00430041004E00430045004C004C00410042004C0045002000200020002000200020002000006B200020002000200020002000200020002C002000440045004C0045005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F00520045005F00430041004E00430045004C004C00410042004C0045002000006B200020002000200020002000200020002C002000440045004C0045005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F00430041004E00430045004C004C00410042004C00450020000053200020002000200020002000200020002C002000440045004C0045005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F00520045005F0054004F005F0047004D002000005F200020002000200020002000200020002C002000440045004C0045005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F00520045005F0054004F005F0047004D002000005F200020002000200020002000200020002C002000440045004C0045005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F0054004F005F0047004D0020000041200020002000200020002000200020002C002000500053002E00500053005F0049004E0049005400490041004C005F00420041004C0041004E004300450020000031200020002000200020002000200020002C002000500053002E00500053005F0043004100530048005F0049004E002000002B200020002000200020002000200020002C002000540045002E00540045005F004E0041004D00450020000080C32000200049004E004E00450052002000200020004A004F0049004E0020005400450052004D0049004E0041004C005300200020002000200020004100530020005400450020004F004E002000410043005F00430055005200520045004E0054005F005400450052004D0049004E0041004C005F0049004400200020002000200020003D002000540045002E00540045005F005400450052004D0049004E0041004C005F004900440020002000200020002000200020002000200020002000200020000080C32000200049004E004E00450052002000200020004A004F0049004E00200050004C00410059005F00530045005300530049004F004E00530020004100530020005000530020004F004E002000410043005F00430055005200520045004E0054005F0050004C00410059005F00530045005300530049004F004E005F004900440020003D002000500053002E00500053005F0050004C00410059005F00530045005300530049004F004E005F0049004400200020002000200020002000200020002000006D2000200057004800450052004500200020002000540045002E00540045005F005400450052004D0049004E0041004C005F004900440020002000200020002000200020002000200020002000200020003D002000400070005400650072006D0069006E0061006C0049006400008091200020002000200041004E004400200020002000540045002E00540045005F00430055005200520045004E0054005F0050004C00410059005F00530045005300530049004F004E005F004900440020003D002000410043005F00430055005200520045004E0054005F0050004C00410059005F00530045005300530049004F004E005F00490044002000200020002000008091200020002000200041004E004400200020002000500053002E00500053005F0050004C00410059005F00530045005300530049004F004E005F00490044002000200020002000200020002000200020003D002000540045002E00540045005F00430055005200520045004E0054005F0050004C00410059005F00530045005300530049004F004E005F00490044002000008091200020002000200041004E004400200020002000500053002E00500053005F005400450052004D0049004E0041004C005F004900440020002000200020002000200020002000200020002000200020003D002000400070005400650072006D0069006E0061006C0049006400200020002000200020002000200020002000200020002000200020002000200020002000008091200020002000200041004E004400200020002000410043005F004100430043004F0055004E0054005F0049004400200020002000200020002000200020002000200020002000200020002000200020003D002000540045002E00540045005F00430055005200520045004E0054005F004100430043004F0055004E0054005F0049004400200020002000200020002000008091200020002000200041004E004400200020002000410043005F00430055005200520045004E0054005F005400450052004D0049004E0041004C005F0049004400200020002000200020002000200020003D002000400070005400650072006D0069006E0061006C0049006400200020002000200020002000200020002000200020002000200020002000200020002000008091200020002000200041004E004400200020002000410043005F00430055005200520045004E0054005F0050004C00410059005F00530045005300530049004F004E005F004900440020002000200020003D002000540045002E00540045005F00430055005200520045004E0054005F0050004C00410059005F00530045005300530049004F004E005F0049004400200000808B200020002000200041004E004400200020002000410043005F0049004E005F00530045005300530049004F004E005F00430041004E00430045004C004C00410042004C0045005F005400520041004E00530041004300540049004F004E005F004900440020003D002000400070005400720061006E00730061006300740069006F006E004900640020000063200020002000200041004E004400200020002000410043005F00430055005200520045004E0054005F0050004C00410059005F00530045005300530049004F004E005F004900440020004900530020004E004F00540020004E0055004C004C0020000051200020002000200041004E004400200020002000500053002E00500053005F0053005400410054005500530020003D002000400070005300740061007400750073004F00700065006E00650064002000001D400070005300740061007400750073004F00700065006E0065006400005D2000200020002000200020002000200020002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F0054004F005F0047004D002000200020002000200020002000200020004D004F004E0045005900005D200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F00520045005F0054004F005F0047004D002000200020004D004F004E0045005900005D200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F0054004F005F0047004D002000200020004D004F004E0045005900005D200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F00460052004F004D005F0047004D0020002000200020002000200020004D004F004E0045005900005D200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F00520045005F00460052004F004D005F0047004D0020004D004F004E0045005900005D200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F00460052004F004D005F0047004D0020004D004F004E0045005900005D200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F00420041004C0041004E004300450020002000200020002000200020004D004F004E0045005900005D200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F00520045005F00420041004C0041004E004300450020004D004F004E00450059000061200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F00420041004C0041004E004300450020004D004F004E004500590029003B000080B32000200020002000530045005400200020002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F00420041004C0041004E004300450020002000200020002000200020003D002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F00420041004C0041004E004300450020002000200020002000200020002B00200040007000520065006400650065006D00610062006C00650020000080AD200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F00520045005F00420041004C0041004E004300450020003D002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F00520045005F00420041004C0041004E004300450020002B00200040007000500072006F006D006F005200650020000080AD200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F00420041004C0041004E004300450020003D002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F00420041004C0041004E004300450020002B00200040007000500072006F006D006F004E00720020000080BB200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F0054004F005F0047004D002000200020002000200020002000200020003D002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F0054004F005F0047004D002000200020002000200020002000200020002B0020004000700054006F0047006D00520065006400650065006D00610062006C00650020000080B5200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F00520045005F0054004F005F0047004D002000200020003D002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F00520045005F0054004F005F0047004D002000200020002B0020004000700054006F0047006D00500072006F006D006F005200650020000080B5200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F0054004F005F0047004D002000200020003D002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F0054004F005F0047004D002000200020002B0020004000700054006F0047006D00500072006F006D006F004E00720020000080BF200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F00460052004F004D005F0047004D0020002000200020002000200020003D002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F00460052004F004D005F0047004D0020002000200020002000200020002B00200040007000460072006F006D0047006D00520065006400650065006D00610062006C00650020000080B9200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F00520045005F00460052004F004D005F0047004D0020003D002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F00520045005F00460052004F004D005F0047004D0020002B00200040007000460072006F006D0047006D00500072006F006D006F005200650020000080B9200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F00460052004F004D005F0047004D0020003D002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F00460052004F004D005F0047004D0020002B00200040007000460072006F006D0047006D00500072006F006D006F004E0072002000008097200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00430041004E00430045004C004C00410042004C0045005F005400520041004E00530041004300540049004F004E005F004900440020003D00200040007000430061006E00630065006C005400720061006E00730061006300740069006F006E00490064002000008091200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00520045005F00430041004E00430045004C004C00410042004C00450020002000200020002000200020002000200020002000200020003D00200040007000430061006E00630065006C00520065006400650065006D00610062006C006500200000808B200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F00520045005F00430041004E00430045004C004C00410042004C00450020002000200020002000200020003D00200040007000430061006E00630065006C00500072006F006D006F0052006500200000808B200020002000200020002000200020002C002000410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F00430041004E00430045004C004C00410042004C00450020002000200020002000200020003D00200040007000430061006E00630065006C00500072006F006D006F004E0072002000005520004F005500540050005500540020002000200049004E005300450052005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F00520045005F0054004F005F0047004D0020000061200020002000200020002000200020002C00200049004E005300450052005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F00520045005F0054004F005F0047004D0020000061200020002000200020002000200020002C00200049004E005300450052005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F0054004F005F0047004D0020000059200020002000200020002000200020002C00200049004E005300450052005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F00520045005F00460052004F004D005F0047004D0020000065200020002000200020002000200020002C00200049004E005300450052005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F00520045005F00460052004F004D005F0047004D0020000065200020002000200020002000200020002C00200049004E005300450052005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F00460052004F004D005F0047004D0020000059200020002000200020002000200020002C00200049004E005300450052005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F00520045005F00420041004C0041004E004300450020000065200020002000200020002000200020002C00200049004E005300450052005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F00520045005F00420041004C0041004E004300450020000065200020002000200020002000200020002C00200049004E005300450052005400450044002E00410043005F0049004E005F00530045005300530049004F004E005F00500052004F004D004F005F004E0052005F00420041004C0041004E00430045002000001940007000520065006400650065006D00610062006C006500001340007000500072006F006D006F0052006500001340007000500072006F006D006F004E00720000214000700054006F0047006D00520065006400650065006D00610062006C006500001B4000700054006F0047006D00500072006F006D006F0052006500001B4000700054006F0047006D00500072006F006D006F004E007200002540007000460072006F006D0047006D00520065006400650065006D00610062006C006500001F40007000460072006F006D0047006D00500072006F006D006F0052006500001F40007000460072006F006D0047006D00500072006F006D006F004E007200002B40007000430061006E00630065006C005400720061006E00730061006300740069006F006E0049006400002540007000430061006E00630065006C00520065006400650065006D00610062006C006500001F40007000430061006E00630065006C00500072006F006D006F0052006500001F40007000430061006E00630065006C00500072006F006D006F004E00720000332000200055005000440041005400450020002000200050004C00410059005F00530045005300530049004F004E005300200000811520002000200020002000530045005400200020002000500053005F0049004E0049005400490041004C005F00420041004C0041004E004300450020003D002000430041005300450020005700480045004E00200028004000700049007300460069007200730074005400720061006E00730066006500720020003D0020003100290020005400480045004E002000500053005F0049004E0049005400490041004C005F00420041004C0041004E004300450020002B0020004000700050006C0061007900610062006C006500420061006C0061006E0063006500200045004C00530045002000500053005F0049004E0049005400490041004C005F00420041004C0041004E0043004500200045004E00440020000080A120002000200020002000200020002000200020002C002000500053005F004100550058005F00460054005F004E0052005F0043004100530048005F0049004E0020003D002000490053004E0055004C004C002800500053005F004100550058005F00460054005F004E0052005F0043004100530048005F0049004E002C0020003000290020002B002000400070004E005200420061006C0061006E00630065000080A120002000200020002000200020002000200020002C002000500053005F004100550058005F00460054005F00520045005F0043004100530048005F0049004E0020003D002000490053004E0055004C004C002800500053005F004100550058005F00460054005F00520045005F0043004100530048005F0049004E002C0020003000290020002B0020004000700052004500420061006C0061006E006300650000813D20002000200020002000200020002000200020002C002000500053005F0043004100530048005F0049004E0020003D002000430041005300450020005700480045004E002000280040007000490067006E006F007200650050006C0061007900610062006C00650049006E00430061007300680049006E0020003D0020003100290020005400480045004E002000500053005F0043004100530048005F0049004E0020005700480045004E00200028004000700049007300460069007200730074005400720061006E00730066006500720020003D0020003100290020005400480045004E0020003000200045004C00530045002000500053005F0043004100530048005F0049004E0020002B0020004000700050006C0061007900610062006C006500420061006C0061006E0063006500200045004E0044002000006D20002000200020002000200020002000200020002C002000500053005F00520045004400450045004D00410042004C0045005F0043004100530048005F0049004E0020003D0020004000700050006C0061007900610062006C006500420061006C0061006E00630065002000006320002000200057004800450052004500200020002000500053005F0050004C00410059005F00530045005300530049004F004E005F004900440020003D0020004000700050006C0061007900530065007300730069006F006E00490064002000200000234000700049007300460069007200730074005400720061006E007300660065007200003140007000490067006E006F007200650050006C0061007900610062006C00650049006E00430061007300680049006E0000234000700050006C0061007900610062006C006500420061006C0061006E00630065000017400070004E005200420061006C0061006E006300650000174000700052004500420061006C0061006E00630065000080C72000530045004C00450043005400200020002000430041005300450020005700480045004E002000410043005F00540059005000450020003D002000400070004100630063006F0075006E0074005600690072007400750061006C005400650072006D0069006E0061006C0020005400480045004E0020003000200045004C00530045002000490053004E0055004C004C002800410043005F0048004F004C004400450052005F004C004500560045004C002C00200030002900200045004E00440020002000002F2000530045004C00450043005400200020002000470050005F004B00450059005F00560041004C005500450020000033200020002000460052004F004D00200020002000470045004E004500520041004C005F0050004100520041004D005300200000572000200057004800450052004500200020002000470050005F00470052004F00550050005F004B0045005900200020003D002000270050006C00610079006500720054007200610063006B0069006E00670027002000017B200020002000200041004E004400200020002000470050005F005300550042004A004500430054005F004B004500590020003D00200027004C006500760065006C00580058002E00520065006400650065006D00610062006C00650050006C00610079006500640054006F00310050006F0069006E0074002700010520003B000079200020002000200041004E004400200020002000470050005F005300550042004A004500430054005F004B004500590020003D00200027004C006500760065006C00580058002E00520065006400650065006D00610062006C0065005300700065006E00740054006F00310050006F0069006E00740027000173200020002000200041004E004400200020002000470050005F005300550042004A004500430054005F004B004500590020003D00200027004C006500760065006C00580058002E0054006F00740061006C0050006C00610079006500640054006F00310050006F0069006E00740027002000010558005800000530003000006D530045004C004500430054002000490053004E0055004C004C0020002800200028002000530045004C00450043005400200020002000430041005300540020002800470050005F004B00450059005F00560041004C0055004500200041005300200049004E00540029002000005520002000200020002000200020002000200020002000200020002000200020002000200020002000460052004F004D00200020002000470045004E004500520041004C005F0050004100520041004D00530020000080A9200020002000200020002000200020002000200020002000200020002000200020002000200057004800450052004500200020002000470050005F00470052004F00550050005F004B00450059002000200020003D002000270050006C00610079006500720054007200610063006B0069006E0067002E00450078007400650072006E0061006C004C006F00790061006C0074007900500072006F006700720061006D0027002000016720002000200020002000200020002000200020002000200020002000200020002000200020002000200041004E004400200020002000470050005F005300550042004A004500430054005F004B004500590020003D00200027004D006F006400650027002000017D20002000200020002000200020002000200020002000200020002000200020002000200020002000200041004E004400200020002000490053004E0055004D0045005200490043002800470050005F004B00450059005F00560041004C0055004500290020003D0020003100200029002C0020003000200029002000001B4200690072007400680044006100790041006C00610072006D0000174000700041006C00610072006D00540079007000650000155400650072006D0069006E0061006C003A0020000057200049004E005300450052005400200049004E0054004F002000200041004C00410052004D0053002000280041004C005F0053004F0055005200430045005F0043004F004400450020002000200020002000200020000057200020002000200020002000200020002000200020002000200020002000200020002000200020002C00200041004C005F0053004F0055005200430045005F00490044002000200020002000200020002000200020000057200020002000200020002000200020002000200020002000200020002000200020002000200020002C00200041004C005F0053004F0055005200430045005F004E0041004D00450020002000200020002000200020000057200020002000200020002000200020002000200020002000200020002000200020002000200020002C00200041004C005F0041004C00410052004D005F0043004F0044004500200020002000200020002000200020000057200020002000200020002000200020002000200020002000200020002000200020002000200020002C00200041004C005F0041004C00410052004D005F004E0041004D004500200020002000200020002000200020000057200020002000200020002000200020002000200020002000200020002000200020002000200020002C00200041004C005F0041004C00410052004D005F004400450053004300520049005000540049004F004E0020000057200020002000200020002000200020002000200020002000200020002000200020002000200020002C00200041004C005F005300450056004500520049005400590020002000200020002000200020002000200020000057200020002000200020002000200020002000200020002000200020002000200020002000200020002C00200041004C005F005200450050004F00520054004500440020002000200020002000200020002000200020000057200020002000200020002000200020002000200020002000200020002000200020002000200020002C00200041004C005F004400410054004500540049004D004500200020002000200020002000200020002000200000572000200020002000200020002000200020002000200020002000200020002000200020002000200020002900200020002000200020002000200020002000200020002000200020002000200020002000200020002000005720002000200020002000200020002000200020002000200020002000560041004C00550045005300200028004000700053006F00750072006300650043006F00640065002000200020002000200020002000200020000057200020002000200020002000200020002000200020002000200020002000200020002000200020002C0020004000700053006F00750072006300650049006400200020002000200020002000200020002000200020000057200020002000200020002000200020002000200020002000200020002000200020002000200020002C0020004000700053006F0075007200630065004E0061006D0065002000200020002000200020002000200020000057200020002000200020002000200020002000200020002000200020002000200020002000200020002C0020004000700041006C00610072006D0043006F006400650020002000200020002000200020002000200020000057200020002000200020002000200020002000200020002000200020002000200020002000200020002C0020004000700041006C00610072006D004E0061006D00650020002000200020002000200020002000200020000057200020002000200020002000200020002000200020002000200020002000200020002000200020002C0020004000700041006C00610072006D004400650073006300720069007000740069006F006E002000200020000057200020002000200020002000200020002000200020002000200020002000200020002000200020002C0020004000700053006500760065007200690074007900200020002000200020002000200020002000200020000057200020002000200020002000200020002000200020002000200020002000200020002000200020002C00200047004500540044004100540045002000280029002000200020002000200020002000200020002000200000194000700053006F00750072006300650043006F006400650000154000700053006F0075007200630065004900640000194000700053006F0075007200630065004E0061006D00650000174000700041006C00610072006D0043006F006400650000174000700041006C00610072006D004E0061006D0065000009550073006500720000254000700041006C00610072006D004400650073006300720069007000740069006F006E0000154000700053006500760065007200690074007900004D20002000200020005300450054002000200020004100430050005F00420041004C0041004E004300450020003D00200040007000420061006C0061006E0063006500520065007300740020000049200020002000200020002000200020002C0020004100430050005F00530054004100540055005300200020003D002000400070004E00650077005300740061007400750073002000001B40007000420061006C0061006E006300650052006500730074000080B955005000440041005400450020005400450052004D0049004E0041004C00530020005300450054002000540045005F004100430043004F0055004E0054005F00500052004F004D004F00540049004F004E005F004900440020003D00200040007000500072006F006D006F00490064002000570048004500520045002000540045005F005400450052004D0049004E0041004C005F004900440020003D002000400070005400650072006D0069006E0061006C0049006400001340007000500072006F006D006F004900640000809D2000200020002000530045004C00450043005400200020002000410043005F004100430043004F0055004E0054005F0049004400200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000809D200020002000200020002000200020002000200020002C002000490053004E0055004C004C002800540045005F004100430043004F0055004E0054005F00500052004F004D004F00540049004F004E005F00490044002C00200030002900200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000809D200020002000200020002000200020002000200020002C002000500053005F0050004C00410059005F00530045005300530049004F004E005F00490044002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000809D200020002000200020002000200020002000200020002C002000540045005F004E0041004D004500200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000809D200020002000200020002000460052004F004D002000200020005400450052004D0049004E0041004C0053002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000809D20004C0045004600540020004A004F0049004E0020002000200050004C00410059005F00530045005300530049004F004E00530020004F004E002000500053005F0050004C00410059005F00530045005300530049004F004E005F004900440020003D002000540045005F00430055005200520045004E0054005F0050004C00410059005F00530045005300530049004F004E005F0049004400200000809D20004C0045004600540020004A004F0049004E002000200020004100430043004F0055004E00540053002000200020002000200020004F004E002000410043005F004100430043004F0055004E0054005F00490044002000200020002000200020003D002000500053005F004100430043004F0055004E0054005F00490044002000200020002000200020002000200020002000200020002000200000809D2000200020002000200057004800450052004500200020002000540045005F005400450052004D0049004E0041004C005F004900440020003D002000400070005400650072006D0069006E0061006C004900640020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000809D200020002000200020002000200041004E0044002000200020002800200020002000410043005F005400590050004500200020002000200020002000200020003D002000400070004100630063006F0075006E0074005400790070006500200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000809D200020002000200020002000200020002000200020002000200020004F0052002000410043005F005400590050004500200020002000200020002000200020003D002000400070005600690072007400750061006C004100630063006F0075006E007400540079007000650020002900200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000809D200020002000200020002000200041004E004400200020002000500053005F005300540041005400550053002000200020002000200020003D002000400070005300740061007400750073002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000001B400070004100630063006F0075006E00740054007900700065000029400070005600690072007400750061006C004100630063006F0075006E0074005400790070006500006B20002000200020005300450054002000200020004100430050005F00420041004C0041004E004300450020002000200020003D0020004100430050005F00420041004C0041004E004300450020002B002000400070004E005200420061006C0061006E006300650020000055200020002000200020002000200020002C0020004100430050005F00530054004100540055005300200020002000200020003D002000400070005300740061007400750073004100630074006900760065002000006120002000570048004500520045002000200020004100430050005F0055004E0049005100550045005F0049004400200020003D002000400070004100630063006F0075006E007400500072006F006D006F00740069006F006E00490064002000004F200020002000200041004E0044002000200020004100430050005F004100430043004F0055004E0054005F004900440020003D002000400070004100630063006F0075006E007400490064002000006B20005500500044004100540045002000200020004100430043004F0055004E005400530020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000006B2000200020002000530045005400200020002000410043005F00520045005F005200450053004500520056004500440020003D0020004000700041006D006F0075006E007400200020002000200020002000200020002000200020002000200020002000200020002000006B200020002000200020002000200020002C002000410043005F004D004F00440045005F005200450053004500520056004500440020003D002000400070004D006F0064006500520065007300650072007600650064002000200020002000200020002000200020002000006B2000200020002000530045005400200020002000410043005F00520045005F005200450053004500520056004500440020003D002000410043005F00520045005F005200450053004500520056004500440020002B0020004000700041006D006F0075006E0074002000006B2000200057004800450052004500200020002000410043005F004100430043004F0055004E0054005F0049004400200020003D002000400070004100630063006F0075006E007400490064002000200020002000200020002000200020002000200020002000200020000011560065006E0064006F0072004900640000174100430050005F0057004F004E004C004F0043004B0000194100430050005F00570049005400480048004F004C0044000080D7200020002000530045005400200020002000500053005F0053005400410054005500530020002000200020002000200020002000200020003D002000430041005300450020005700480045004E0020002800500053005F0053005400410054005500530020003D002000400070005300740061007400750073004F00700065006E0065006400290020005400480045004E002000400070004E0065007700530074006100740075007300200045004C00530045002000500053005F0053005400410054005500530020002000200045004E00440020000080D720002000200020002000200020002C002000500053005F004C004F0043004B004500440020002000200020002000200020002000200020003D002000430041005300450020005700480045004E0020002800500053005F0053005400410054005500530020003D002000400070005300740061007400750073004F00700065006E0065006400290020005400480045004E0020004E0055004C004C002000200020002000200020002000200045004C00530045002000500053005F004C004F0043004B004500440020002000200045004E00440020000080D720002000200020002000200020002C002000500053005F00460049004E0049005300480045004400200020002000200020002000200020003D002000430041005300450020005700480045004E0020002800500053005F0053005400410054005500530020003D002000400070005300740061007400750073004F00700065006E0065006400290020005400480045004E00200047004500540044004100540045002800290020002000200045004C00530045002000500053005F00460049004E0049005300480045004400200045004E004400200000374F0055005400500055005400200020002000440045004C0045005400450044002E00500053005F005300540041005400550053002000003920002000200020002000200020002C00200049004E005300450052005400450044002E00500053005F005300540041005400550053002000003920002000200020002000200020002C002000440045004C0045005400450044002E00500053005F0053005400410052005400450044002000006120002000200020002000200020002C002000490053004E0055004C004C002800440045004C0045005400450044002E00500053005F00460049004E00490053004800450044002C00200047004500540044004100540045002800290029002000005B200057004800450052004500200020002000500053005F0050004C00410059005F00530045005300530049004F004E005F004900440020003D0020004000700050006C0061007900530065007300730069006F006E0049006400002F200055005000440041005400450020002000200050004C00410059005F00530045005300530049004F004E00530000810F200020002000530045005400200020002000500053005F005200450050004F0052005400450044005F00420041004C0041004E00430045005F004D00490053004D00410054004300480020003D002000430041005300450020005700480045004E002000280040007000420061006C0061006E00630065004D00690073006D00610074006300680020003D0020003100290020005400480045004E002000400070005200650070006F007200740065006400420061006C0061006E0063006500200045004C00530045002000500053005F005200450050004F0052005400450044005F00420041004C0041004E00430045005F004D00490053004D004100540043004800200045004E0044002000006F20002000200020002000200020002C002000500053005F00460049004E0041004C005F00420041004C0041004E004300450020002000200020002000200020002000200020002000200020003D00200040007000460069006E0061006C00420061006C0061006E00630065002000007520002000200020002000200020002C002000500053005F00420041004C0041004E00430045005F004D00490053004D00410054004300480020002000200020002000200020002000200020003D00200040007000420061006C0061006E00630065004D00690073006D00610074006300680020000071200057004800450052004500200020002000500053005F0050004C00410059005F00530045005300530049004F004E005F0049004400200020002000200020002000200020002000200020003D0020004000700050006C0061007900530065007300730069006F006E0049004400200000809F20002000200041004E004400200020002000500053005F005300540041005400550053002000200020002000200020002000200020002000200020002000200020002000200020002000200049004E00200028002000400070005300740061007400750073004F00700065006E00650064002C002000400070005300740061007400750073004100620061006E0064006F006E0065006400200029002000006F20002000200041004E004400200020002000500053005F00530054004100540055005300200020002000200020002000200020002000200020002000200020002000200020002000200020003D002000400070005300740061007400750073004F00700065006E006500640020000023400070005200650070006F007200740065006400420061006C0061006E0063006500002340007000420061006C0061006E00630065004D00690073006D0061007400630068000023400070005300740061007400750073004100620061006E0064006F006E0065006400006720002000200055005000440041005400450020002000200050004C00410059005F00530045005300530049004F004E005300200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000067200020002000200020002000530045005400200020002000500053005F0053005400410054005500530020002000200020002000200020002000200020003D002000400070005300740061007400750073004100620061006E0064006F006E00650064002000006720002000200020002000200020002000200020002C002000500053005F00460049004E0049005300480045004400200020002000200020002000200020003D0020004700450054004400410054004500280029002000200020002000200020002000200020000067200020002000200057004800450052004500200020002000500053005F0050004C00410059005F00530045005300530049004F004E005F004900440020003D0020004000700050006C0061007900530065007300730069006F006E004900640020002000200000395400720078005F004F006E0045006E0064004300610072006400530065007300730069006F006E0020006600610069006C00650064002E00004D5400720078005F0050006C0061007900530065007300730069006F006E004D00610072006B00410073004100620061006E0064006F006E006500640020006600610069006C00650064002E00003B5400720078005F005300650074004100630063006F0075006E00740042006C006F0063006B006500640020006600610069006C00650064002E0000352000530045004C00450043005400200020002000500053005F0050004C0041005900450044005F0043004F0055004E00540020000037200020002000200020002000200020002C002000500053005F0050004C0041005900450044005F0041004D004F0055004E0054002000002F200020002000200020002000200020002C002000500053005F0057004F004E005F0043004F0055004E00540020000031200020002000200020002000200020002C002000500053005F0057004F004E005F0041004D004F0055004E005400200000272000530045004C0045004300540020002000200043004F0055004E0054002800310029002000003B200020002000460052004F004D002000200020004100430043004F0055004E0054005F00500052004F004D004F00540049004F004E0053002000004F20002000570048004500520045002000200020004100430050005F004100430043004F0055004E0054005F0049004400200020003D002000400070004100630063006F0075006E007400490064000055200020002000200041004E0044002000200020004100430050005F005300540041005400550053002000200020002000200020003D002000400070005300740061007400750073004100630074006900760065000055200020002000200041004E0044002000200020004100430050005F004300520045004400490054005F005400590050004500200049004E0020002800400070004E00520031002C00400070004E00520032002900000B400070004E0052003100000B400070004E00520032000080AB200020002000530045005400200020002000410043005F00500052004F004D004F005F0049004E0049005F00520045005F00420041004C0041004E004300450020003D002000430041005300450020005700480045004E00200028002000410043005F00500052004F004D004F005F0049004E0049005F00520045005F00420041004C0041004E004300450020004900530020004E004F00540020004E0055004C004C002000290020000080B12000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020005400480045004E002000430041005300450020005700480045004E00200028002000410043005F00500052004F004D004F005F0049004E0049005F00520045005F00420041004C0041004E004300450020002B0020004000700041006D006F0075006E00740020003E003D00200030002900200000808D2000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020005400480045004E002000410043005F00500052004F004D004F005F0049004E0049005F00520045005F00420041004C0041004E004300450020002B0020004000700041006D006F0075006E0074002000006F20002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200045004C005300450020003000200045004E004400200045004C005300450020004E0055004C004C00200045004E0044002000004B200057004800450052004500200020002000410043005F004100430043004F0055004E0054005F004900440020003D002000400070004100630063006F0075006E007400490064002000005D200020002000530045005400200020002000410043005F00500052004F004D004F005F0049004E0049005F00520045005F00420041004C0041004E004300450020002000200020003D0020004000700041006D006F0075006E007400001F4D0075006C007400690053006900740065004D0065006D00620065007200005749004E005300450052005400200049004E0054004F002000200020004D0053005F00500045004E00440049004E0047005F00470041004D0045005F0050004C00410059005F00530045005300530049004F004E00530000472000200020002000200020002000200020002000200020002000200028004D00500053005F0050004C00410059005F00530045005300530049004F004E005F00490044002900004120002000200020002000560041004C0055004500530020002000200028004000700050006C0061007900530065007300730069006F006E004900640029002000003349004E005300450052005400200049004E0054004F0020005700430050005F0043004F004D004D0041004E00440053002000003920002000200020002000200020002000200020002000280043004D0044005F005400450052004D0049004E0041004C005F00490044002000002B20002000200020002000200020002000200020002C00200043004D0044005F0043004F00440045002000002F20002000200020002000200020002000200020002C00200043004D0044005F005300540041005400550053002000003120002000200020002000200020002000200020002C00200043004D0044005F0043005200450041005400450044002000003F20002000200020002000200020002000200020002C00200043004D0044005F005300540041005400550053005F004300480041004E004700450044002000002F20002000200020002000200020002000200020002C00200043004D0044005F00500053005F00490044002900200000809720002000200020002000560041004C0055004500530020002800200040005400650072006D0069006E0061006C004900640020002C002000310020002C002000300020002C00200047004500540044004100540045002800290020002C00200047004500540044004100540045002800290020002C00400050006C0061007900530065007300730069006F006E004900640020002900001740005400650072006D0069006E0061006C0049006400001D400050006C0061007900530065007300730069006F006E0049006400007B2000200057004800450052004500200020002000470050005F00470052004F00550050005F004B00450059002000200020003D002000270050006C00610079006500720054007200610063006B0069006E0067002E0044006F004E006F0074004100770061007200640050006F0069006E007400730027002000017B200020002000200041004E004400200020002000470050005F005300550042004A004500430054005F004B004500590020003D002000270050006C0061007900530065007300730069006F006E002E0050006F0069006E007400730041007200650047007200650061007400650072005400680061006E00270001192000200055004E0049004F004E00200041004C004C00200000808D200020002000200041004E004400200020002000470050005F005300550042004A004500430054005F004B004500590020003D002000270050006C0061007900530065007300730069006F006E002E0050006F0069006E00740073005000650072004D0069006E0075007400650041007200650047007200650061007400650072005400680061006E0027000169200020002000200041004E004400200020002000470050005F005300550042004A004500430054005F004B004500590020003D002000270050006C0061007900530065007300730069006F006E002E004800610073004E006F0050006C00610079007300270020000179200020002000200041004E004400200020002000470050005F005300550042004A004500430054005F004B004500590020003D002000270050006C0061007900530065007300730069006F006E002E00480061007300420061006C0061006E00630065004D00690073006D00610074006300680027002000012943006F006D0070007500740065006400200050006F0069006E00740073003A0020007B0030007D00003D2C00200050006F0069006E007400730020004100720065002000470072006500610074006500720020005400680061006E003A0020007B0030007D00002B2C0020004D0069006E007500740065007300200050006C0061007900650064003A0020007B0030007D00000930002E002300230000312C00200050006F0069006E0074007300200050006500720020004D0069006E007500740065003A0020007B0030007D0000532C00200050006F0069006E0074007300200050006500720020004D0069006E0075007400650020004100720065002000470072006500610074006500720020005400680061006E003A0020007B0030007D0000214E0075006D002E00200050006C0061007900650064003A0020007B0030007D0000292C00200048006100730020004E006F00200050006C006100790073003A0020005400720075006500002B420061006C0061006E006300650020004D00690073006D0061007400630068003A0020007B0030007D0000392C0020004800610073002000420061006C0061006E006300650020004D00690073006D0061007400630068003A0020005400720075006500002550006C0061007900200074006F00200072006500760069007300650020002D003E002000011D4100630063006F0075006E007400490064003A0020007B0030007D0000232C0020005400650072006D0069006E0061006C00490064003A0020007B0030007D00001B20004300720069007400650072006900610020002D003E002000013520002000200055005000440041005400450020002000200050004C00410059005F00530045005300530049004F004E0053002000008109200020002000200020002000530045005400200020002000500053005F0041005700410052004400450044005F0050004F0049004E00540053005F0053005400410054005500530020003D002000430041005300450020005700480045004E002000500053005F0053005400410054005500530020003C003E00200040007000530074006100740065004F00700065006E006500640020005400480045004E0020004000700050006F0069006E007400730053007400610074007500730020002000200045004C00530045002000500053005F0041005700410052004400450044005F0050004F0049004E00540053005F00530054004100540055005300200045004E00440020000080E120002000200020002000200020002000200020002C002000500053005F0043004F004D00500055005400450044005F0050004F0049004E005400530020002000200020002000200020003D002000430041005300450020005700480045004E002000500053005F0053005400410054005500530020003C003E00200040007000530074006100740065004F00700065006E006500640020005400480045004E0020004000700043006F006D007000750074006500640050006F0069006E0074007300200045004C005300450020004E0055004C004C00200045004E00440020000080E120002000200020002000200020002000200020002C002000500053005F0041005700410052004400450044005F0050004F0049004E0054005300200020002000200020002000200020003D002000430041005300450020005700480045004E002000500053005F0053005400410054005500530020003C003E00200040007000530074006100740065004F00700065006E006500640020005400480045004E00200040007000410077006100720064006500640050006F0069006E00740073002000200045004C005300450020004E0055004C004C00200045004E0044002000006F200020002000200057004800450052004500200020002000500053005F0050004C00410059005F00530045005300530049004F004E005F004900440020002000200020002000200020003D0020004000700050006C0061007900530065007300730069006F006E00490064002000001B40007000530074006100740065004F00700065006E0065006400001D4000700050006F0069006E007400730053007400610074007500730000214000700043006F006D007000750074006500640050006F0069006E0074007300001F40007000410077006100720064006500640050006F0069006E007400730000492000200057004800450052004500200020002000470050005F00470052004F00550050005F004B004500590020002000200020003D00200040007000470072006F00750070002000004D200020002000200041004E004400200020002000470050005F005300550042004A004500430054005F004B0045005900200020003D002000400070005300750062006A006500630074002000000F40007000470072006F00750070000013400070005300750062006A0065006300740000135700610072006E0069006E0067003A002000003F7B0030007D0020003C00520045003A0020007B0031007D002C002000500052003A0020007B0032007D002C0020004E0052003A0020007B0033007D003E0000197B0030007D002C002000500054003A0020007B0031007D00003555006E006B006E006F0077006E002000500072006F00760069006400650072004C0069007300740054007900700065003A00200000053A0020000019500072006F00760069006400650072004C0069007300740000114C006900730074005400790070006500000954007900700065000011500072006F007600690064006500720000094E0061006D00650000272000530045004C0045004300540020002000200043004F0055004E00540028002A00290020000031200020002000460052004F004D002000200020005400450052004D0049004E0041004C0053005F003300470053002000007D20002000200049004E004E004500520020004A004F0049004E0020005400450052004D0049004E0041004C00530020004F004E002000200054003300470053005F005400450052004D0049004E0041004C005F0049004400200020003D002000540045005F005400450052004D0049004E0041004C005F004900440000112000200057004800450052004500200000452000200054003300470053005F005400450052004D0049004E0041004C005F0049004400200020003D00200040005400650072006D0069006E0061006C0049006400200000772000530045004C004500430054002000200020004000490067006E006F00720065004D0061006300680069006E0065004E0075006D0062006500720020003D00200043004100530054002800470050005F004B00450059005F00560041004C0055004500200041005300200069006E007400290020000080A52000200057004800450052004500200020002000470050005F00470052004F00550050005F004B004500590020003D002000270049006E0074006500720066006100630065003300470053002700200041004E0044002000470050005F005300550042004A004500430054005F004B004500590020003D002700490067006E006F00720065004D0061006300680069006E0065004E0075006D006200650072002700200001332000530045004C00450043005400200020002000540045005F005400450052004D0049004E0041004C005F004900440020000047200020002000460052004F004D002000200020005400450052004D0049004E0041004C0053005F003300470053002C0020005400450052004D0049004E0041004C00530020000063200020005700480045005200450020002000200054003300470053005F00560045004E0044004F0052005F00490044002000200020002000200020002000200020002000200020003D00200040007000560065006E0064006F0072004900640020000080A9200020002000200020002000200020002000200041004E00440020002800200054003300470053005F00530045005200490041004C005F004E0055004D00420045005200200020003D00200040007000530065007200690061006C004E0075006D00620065007200200020004F00520020004000490067006E006F0072006500530065007200690061006C004E0075006D00620065007200200020003D00200031002000290020000080A9200020002000200020002000200020002000200041004E00440020002800200054003300470053005F004D0041004300480049004E0045005F004E0055004D0042004500520020003D002000400070004D0061006300680069006E0065004E0075006D0062006500720020004F00520020004000490067006E006F00720065004D0061006300680069006E0065004E0075006D0062006500720020003D0020003100200029002000006B200020002000200020002000200020002000200041004E004400200054003300470053005F005400450052004D0049004E0041004C005F00490044002000200020002000200020003D002000540045005F005400450052004D0049004E0041004C005F00490044002000001D40007000530065007200690061006C004E0075006D0062006500720000274000490067006E006F0072006500530065007200690061006C004E0075006D00620065007200001F400070004D0061006300680069006E0065004E0075006D0062006500720000294000490067006E006F00720065004D0061006300680069006E0065004E0075006D00620065007200001F20004900460020004E004F00540020004500580049005300540053002000000720002800200000192000530045004C00450043005400200020002000310020000039200020002000460052004F004D002000200020005400450052004D0049004E0041004C0053005F00500045004E00440049004E0047002000004F2000200057004800450052004500200020002000540050005F0053004F0055005200430045002000200020002000200020002000200020003D0020004000700053006F00750072006300650020000053200020002000200041004E004400200020002000540050005F00560045004E0044004F0052005F00490044002000200020002000200020003D00200040007000560065006E0064006F007200490064002000005B200020002000200041004E004400200020002000540050005F00530045005200490041004C005F004E0055004D00420045005200200020003D00200040007000530065007200690061006C004E0075006D0062006500720020000007200029002000005B20002000200049004E005300450052005400200049004E0054004F0020005400450052004D0049004E0041004C0053005F00500045004E00440049004E004700200028002000540050005F0053004F005500520043004500200000612000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000540050005F00560045004E0044004F0052005F0049004400200000692000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000540050005F00530045005200490041004C005F004E0055004D004200450052002000006F2000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000540050005F004D0041004300480049004E0045005F004E0055004D00420045005200200029002000005920002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000560041004C005500450053002000280020004000700053006F0075007200630065002000005D2000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C00200040007000560065006E0064006F00720049006400200000652000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C00200040007000530065007200690061006C004E0075006D006200650072002000006B2000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002C002000400070004D0061006300680069006E0065004E0075006D00620065007200200029002000000D200045004C00530045002000003D2000200020005500500044004100540045002000200020005400450052004D0049004E0041004C0053005F00500045004E00440049004E00470020000061200020002000200020002000530045005400200020002000540050005F004D0041004300480049004E0045005F004E0055004D0042004500520020003D002000400070004D0061006300680069006E0065004E0075006D0062006500720020000053200020002000200057004800450052004500200020002000540050005F0053004F0055005200430045002000200020002000200020002000200020003D0020004000700053006F0075007200630065002000005720002000200020002000200041004E004400200020002000540050005F00560045004E0044004F0052005F00490044002000200020002000200020003D00200040007000560065006E0064006F007200490064002000005F20002000200020002000200041004E004400200020002000540050005F00530045005200490041004C005F004E0055004D00420045005200200020003D00200040007000530065007200690061006C004E0075006D00620065007200200000114000700053006F007500720063006500002B530079007300740065006D00440065006600610075006C00740053006100730046006C00610067007300008087200020002000530045004C00450043005400200020002000540045005F005400450052004D0049004E0041004C005F00490044002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000808720002000200020002000200020002000200020002C002000540045005F00500052004F0056005F004900440020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000808720002000200020002000200020002000200020002C002000540045005F005400450052004D0049004E0041004C005F00540059005000450020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000808720002000200020002000200020002000200020002C002000540045005F004E0041004D00450020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000808720002000200020002000200020002000200020002C002000540045005F00500052004F00560049004400450052005F00490044002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000808720002000200020002000200020002000200020002C002000540045005F0042004C004F0043004B004500440020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000808720002000200020002000200020002000200020002C002000540045005F00530054004100540055005300200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000808720002000200020002000200020002000200020002C002000540045005F00430055005200520045004E0054005F0050004C00410059005F00530045005300530049004F004E005F00490044002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000808720002000200020002000200020002000200020002C002000540045005F00430055005200520045004E0054005F004100430043004F0055004E0054005F0049004400200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000808720002000200020002000200020002000200020002C002000500056005F0050004F0049004E00540053005F004D0055004C005400490050004C004900450052002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000808720002000200020002000200020002000200020002C00200043004100530054002800490053004E0055004C004C0020002800200028002000530045004C004500430054002000200020003100200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000080872000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000460052004F004D002000200020005400450052004D0049004E0041004C005F00470052004F005500500053002000200020002000200020002000200020002000200020002000200020002000200020002000200000808720002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200057004800450052004500200020002000540047005F0045004C0045004D0045004E0054005F00540059005000450020003D0020004000700045006C0065006D0065006E007400540079007000650020002000200020000080872000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200041004E004400200020002000540047005F005400450052004D0049004E0041004C005F004900440020003D002000540045005F005400450052004D0049004E0041004C005F004900440020002000200020000080872000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200041004E004400200020002000540047005F0045004C0045004D0045004E0054005F004900440020003D0020003100200029002C002000300020002900200041005300200042004900540029002000200020000080852000200009002000200020002000200020002000200041005300200053004900540045005F004A00410043004B0050004F005400200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000808720002000200020002000200020002000200020002C002000500056005F004F004E004C0059005F00520045004400450045004D00410042004C004500200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000808720002000200020002000200020002000200020002C002000540045005F005300410053005F0046004C00410047005300200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000808720002000200020002000200020002000200020002C002000540045005F005300410053005F0046004C004100470053005F005500530045005F0053004900540045005F00440045004600410055004C0054002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000808720002000200020002000200020002000200020002C002000490053004E0055004C004C0020002800200028002000530045004C00450043005400200020002000430041005300540020002800470050005F004B00450059005F00560041004C0055004500200041005300200049004E00540029002000200020002000200020002000200020000080872000200020000900200020002000200020002000200020002000200020002000200020002000200020002000200020002000460052004F004D00200020002000470045004E004500520041004C005F0050004100520041004D00530020002000200020002000200020002000200020002000200020002000200020002000200020002000200000808720002000200009002000200020002000200020002000200020002000200020002000200020002000200020002000200057004800450052004500200020002000470050005F00470052004F00550050005F004B00450059002000200020003D0020002700430072006500640069007400730027002000200020002000200020002000200020000180872000200020000900200020002000200020002000200020002000200020002000200020002000200020002000200020002000200041004E004400200020002000470050005F005300550042004A004500430054005F004B004500590020003D002000270050006C00610079004D006F0064006500270020002000200020002000200020002000018085200020000900200020002000200020002000200020002000200020002000200020002000200020002000200020002000200041004E004400200020002000490053004E0055004D0045005200490043002800470050005F004B00450059005F00560041004C0055004500290020003D0020003100200029002C002000300020002900200000808520002000090020002000200020002000200020002000410053002000530059005300540045004D005F0050004C00410059005F004D004F00440045002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000080852000200009002000200020002000200020002C002000540045005F00450058005400450052004E0041004C005F0049004400200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000080852000200009002000200020002000200020002C002000540045005F0046004C004F004F0052005F00490044002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000808720002000200020002000200020002000200020002C00200042004B005F004E0041004D0045002000410053002000420041004E004B00200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000808720002000200020002000200020002000200020002C002000540045005F00490053004F005F0043004F00440045002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000808720002000200020002000200020002000200020002C002000540045005F005400490054004F005F0048004F00530054005F0049004400200041005300200048004F00530054005F00490044002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000808720002000200020002000200020002000200020002C002000410052005F004E0041004D00450020004100530020004100520045004100200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000808720002000200020002000460052004F004D002000200020005400450052004D0049004E0041004C00530020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000080874C00450046005400200020004A004F0049004E0020002000500052004F0056004900440045005200530020004F004E002000500056005F004900440020003D002000540045005F00500052004F0056005F004900440020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000808749004E004E004500520020004A004F0049004E0020002000420041004E004B00530020004F004E00200042004B005F00420041004E004B005F004900440020003D002000540045005F00420041004E004B005F00490044002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000808749004E004E004500520020004A004F0049004E00200020004100520045004100530020004F004E00200042004B005F0041005200450041005F004900440020003D002000410052005F0041005200450041005F00490044002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000472000200057004800450052004500200020002000540045005F004E0041004D00450020003D002000400070005400650072006D0069006E0061006C004E0061006D006500200000612000200057004800450052004500200020002000540045005F00450058005400450052004E0041004C005F004900440020003D002000400070005400650072006D0069006E0061006C00450078007400650072006E0061006C0049006400200000512000200057004800450052004500200020002000540045005F005400450052004D0049004E0041004C005F004900440020003D002000400070005400650072006D0069006E0061006C00490064002000001B4000700045006C0065006D0065006E00740054007900700065000029400070005400650072006D0069006E0061006C00450078007400650072006E0061006C0049006400003B20002000530045004C00450043005400200020002000540045005F004E0041004D00450020002000200020002000200020002000200020002000003B2000200020002000200020002000200020002C002000540045005F0042004100530045005F004E0041004D004500200020002000200020002000003B2000200020002000200020002000200020002C002000540045005F0046004C004F004F0052005F00490044002000200020002000200020002000003B2000200020002000460052004F004D002000200020005400450052004D0049004E0041004C005300200020002000200020002000200020002000005520002000200057004800450052004500200020002000540045005F005400450052004D0049004E0041004C005F00490044002000200020003D002000400070005400650072006D0069006E0061004900640020000017400070005400650072006D0069006E00610049006400000B25004E0061006D0065000013250042006100730065004E0061006D0065000011250046006C006F006F007200490064000055200020002000530045004C00450043005400200020002000540045005F005300410053005F0046004C00410047005300200020002000200020002000200020002000200020002000200020002000200020002000005520002000200020002000200020002000200020002C002000540045005F005300410053005F0046004C004100470053005F005500530045005F0053004900540045005F00440045004600410055004C0054002000005520002000200020002000460052004F004D002000200020005400450052004D0049004E0041004C0053002000200020002000200020002000200020002000200020002000200020002000200020002000200020000055200020002000200057004800450052004500200020002000540045005F005400450052004D0049004E0041004C005F004900440020003D002000400070005400650072006D0069006E0061006C004900640020000019540045005F005300410053005F0046004C00410047005300003B540045005F005300410053005F0046004C004100470053005F005500530045005F0053004900540045005F00440045004600410055004C00540000214900460020004E004F00540020004500580049005300540053002000280020000023200020002000200020002000530045004C0045004300540020002000200031002000003720002000200020002000200020002000460052004F004D00200020002000470041004D0045005F004D00450054004500520053002000006120002000200020002000200020005700480045005200450020002000200047004D005F005400450052004D0049004E0041004C005F004900440020002000200020003D002000400070005400650072006D0069006E0061006C00490064002000006520002000200020002000200020002000200041004E00440020002000200047004D005F00470041004D0045005F0042004100530045005F004E0041004D00450020003D00200040007000470061006D00650042006100730065004E0061006D0065002000001120002000200020002000200029002000003B20002000200049004E005300450052005400200049004E0054004F00200020002000470041004D0045005F004D0045005400450052005300200000412000200020002000200020002000200020002000200020002000200020002800200047004D005F005400450052004D0049004E0041004C005F0049004400200000472000200020002000200020002000200020002000200020002000200020002C00200047004D005F00470041004D0045005F0042004100530045005F004E0041004D004500200000492000200020002000200020002000200020002000200020002000200020002C00200047004D005F005700430050005F00530045005100550045004E00430045005F0049004400200000432000200020002000200020002000200020002000200020002000200020002C00200047004D005F00440045004E004F004D0049004E004100540049004F004E00200000432000200020002000200020002000200020002000200020002000200020002C00200047004D005F0050004C0041005900450044005F0043004F0055004E005400200000452000200020002000200020002000200020002000200020002000200020002C00200047004D005F0050004C0041005900450044005F0041004D004F0055004E0054002000003D2000200020002000200020002000200020002000200020002000200020002C00200047004D005F0057004F004E005F0043004F0055004E0054002000003F2000200020002000200020002000200020002000200020002000200020002C00200047004D005F0057004F004E005F0041004D004F0055004E005400200000472000200020002000200020002000200020002000200020002000200020002C00200047004D005F004A00410043004B0050004F0054005F0041004D004F0055004E005400200000492000200020002000200020002000200020002000200020002000200020002C00200047004D005F00440045004C00540041005F00470041004D0045005F004E0041004D0045002000004F2000200020002000200020002000200020002000200020002000200020002C00200047004D005F00440045004C00540041005F0050004C0041005900450044005F0043004F0055004E005400200000512000200020002000200020002000200020002000200020002000200020002C00200047004D005F00440045004C00540041005F0050004C0041005900450044005F0041004D004F0055004E005400200000492000200020002000200020002000200020002000200020002000200020002C00200047004D005F00440045004C00540041005F0057004F004E005F0043004F0055004E0054002000004B2000200020002000200020002000200020002000200020002000200020002C00200047004D005F00440045004C00540041005F0057004F004E005F0041004D004F0055004E005400200000452000200020002000200020002000200020002000200020002000200020002C00200047004D005F004C004100530054005F005200450050004F0052005400450044002000002320002000200020002000200020002000200020002000200020002000200029002000001F20002000200020002000200020002000560041004C005500450053002000003D20002000200020002000200020002000200020002000200020002000200028002000400070005400650072006D0069006E0061006C0049006400200000412000200020002000200020002000200020002000200020002000200020002C00200040007000470061006D00650042006100730065004E0061006D006500200000432000200020002000200020002000200020002000200020002000200020002C00200040007000570063007000530065007100750065006E006300650049006400200000412000200020002000200020002000200020002000200020002000200020002C00200040007000440065006E006F006D0069006E006100740069006F006E00200000492000200020002000200020002000200020002000200020002000200020002C00200040007000440065006C007400610050006C00610079006500640043006F0075006E0074002000004B2000200020002000200020002000200020002000200020002000200020002C00200040007000440065006C007400610050006C00610079006500640041006D006F0075006E007400200000432000200020002000200020002000200020002000200020002000200020002C00200040007000440065006C007400610057006F006E0043006F0075006E007400200000452000200020002000200020002000200020002000200020002000200020002C00200040007000440065006C007400610057006F006E0041006D006F0075006E0074002000004D2000200020002000200020002000200020002000200020002000200020002C00200040007000440065006C00740061004A00610063006B0070006F00740041006D006F0075006E007400200000372000200020002000200020002000200020002000200020002000200020002C0020004700450054004400410054004500280029002000001F2000200020002000200020002000200020002000200020002000200020000031200020002000550050004400410054004500200020002000470041004D0045005F004D00450054004500520053002000006B20002000200020002000200053004500540020002000200047004D005F005700430050005F00530045005100550045004E00430045005F0049004400200020002000200020003D00200040007000570063007000530065007100750065006E0063006500490064002000006920002000200020002000200020002000200020002C00200047004D005F00440045004E004F004D0049004E004100540049004F004E00200020002000200020002000200020003D00200040007000440065006E006F006D0069006E006100740069006F006E0020000080A520002000200020002000200020002000200020002C00200047004D005F0050004C0041005900450044005F0043004F0055004E005400200020002000200020002000200020003D00200047004D005F0050004C0041005900450044005F0043004F0055004E0054002000200020002000200020002000200020002B00200040007000440065006C007400610050006C00610079006500640043006F0075006E00740020000080A720002000200020002000200020002000200020002C00200047004D005F0050004C0041005900450044005F0041004D004F0055004E00540020002000200020002000200020003D00200047004D005F0050004C0041005900450044005F0041004D004F0055004E005400200020002000200020002000200020002B00200040007000440065006C007400610050006C00610079006500640041006D006F0075006E007400200000809F20002000200020002000200020002000200020002C00200047004D005F0057004F004E005F0043004F0055004E005400200020002000200020002000200020002000200020003D00200047004D005F0057004F004E005F0043004F0055004E0054002000200020002000200020002000200020002000200020002B00200040007000440065006C007400610057006F006E0043006F0075006E00740020000080A120002000200020002000200020002000200020002C00200047004D005F0057004F004E005F0041004D004F0055004E00540020002000200020002000200020002000200020003D00200047004D005F0057004F004E005F0041004D004F0055004E005400200020002000200020002000200020002000200020002B00200040007000440065006C007400610057006F006E0041006D006F0075006E0074002000006920002000200020002000200020002000200020002C00200047004D005F00440045004C00540041005F00470041004D0045005F004E0041004D004500200020002000200020003D00200040007000470061006D00650042006100730065004E0061006D00650020000080A520002000200020002000200020002000200020002C00200047004D005F00440045004C00540041005F0050004C0041005900450044005F0043004F0055004E005400200020003D00200047004D005F00440045004C00540041005F0050004C0041005900450044005F0043004F0055004E0054002000200020002B00200040007000440065006C007400610050006C00610079006500640043006F0075006E00740020000080A720002000200020002000200020002000200020002C00200047004D005F00440045004C00540041005F0050004C0041005900450044005F0041004D004F0055004E00540020003D00200047004D005F00440045004C00540041005F0050004C0041005900450044005F0041004D004F0055004E005400200020002B00200040007000440065006C007400610050006C00610079006500640041006D006F0075006E007400200000809F20002000200020002000200020002000200020002C00200047004D005F00440045004C00540041005F0057004F004E005F0043004F0055004E005400200020002000200020003D00200047004D005F00440045004C00540041005F0057004F004E005F0043004F0055004E0054002000200020002000200020002B00200040007000440065006C007400610057006F006E0043006F0075006E00740020000080A120002000200020002000200020002000200020002C00200047004D005F00440045004C00540041005F0057004F004E005F0041004D004F0055004E00540020002000200020003D00200047004D005F00440045004C00540041005F0057004F004E005F0041004D004F0055004E005400200020002000200020002B00200040007000440065006C007400610057006F006E0041006D006F0075006E00740020000080A920002000200020002000200020002000200020002C00200047004D005F004A00410043004B0050004F0054005F0041004D004F0055004E0054002000200020002000200020003D00200047004D005F004A00410043004B0050004F0054005F0041004D004F0055004E00540020002000200020002000200020002B00200040007000440065006C00740061004A00610063006B0070006F00740041006D006F0075006E0074002000005F20002000200020002000200020002000200020002C00200047004D005F004C004100530054005F005200450050004F00520054004500440020002000200020002000200020003D0020004700450054004400410054004500280029002000006520002000200020005700480045005200450020002000200047004D005F005400450052004D0049004E0041004C005F00490044002000200020002000200020002000200020003D002000400070005400650072006D0069006E0061006C00490064002000006920002000200020002000200041004E00440020002000200047004D005F00470041004D0045005F0042004100530045005F004E0041004D0045002000200020002000200020003D00200040007000470061006D00650042006100730065004E0061006D00650020000080C12000200055005000440041005400450020002000200043004100530048004900450052005F00530045005300530049004F004E0053002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000080C120002000200020002000530045005400200020002000430053005F0053005400410054005500530020003D00200040007000500065006E00640069006E00670043006C006F00730069006E0067005300740061007400750073002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000080C120002000200057004800450052004500200020002000430053005F0043004100530048004900450052005F004900440020003D0020002800200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000080C12000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000530045004C00450043005400200020002000430054005F0043004100530048004900450052005F004900440020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000080C1200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000460052004F004D0020002000200043004100530048004900450052005F005400450052004D0049004E0041004C0053002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000080C12000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200057004800450052004500200020002000430054005F005400450052004D0049004E0041004C005F004900440020003D002000400070005400650072006D0069006E0061006C00490064002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000080C12000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002900200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000080C12000200020002000200041004E004400200020002000430053005F0053005400410054005500530020003D002000400070004F00700065006E0053007400610074007500730020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000080C12000200020002000200041004E004400200020002000430053005F00530045005300530049004F004E005F004900440020003C003E002000280020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000080C12000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000530045004C0045004300540020002000200054004F005000200031002000430053005F00530045005300530049004F004E005F004900440020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000080C1200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000460052004F004D0020002000200043004100530048004900450052005F00530045005300530049004F004E00530020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000080C12000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200057004800450052004500200020002000430053005F0043004100530048004900450052005F004900440020003D00200028002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000080C1200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000530045004C00450043005400200020002000430054005F0043004100530048004900450052005F0049004400200020002000200020002000200020002000200020002000200020002000200020000080C120002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000460052004F004D0020002000200043004100530048004900450052005F005400450052004D0049004E0041004C00530020002000200020002000200020002000200020002000200020000080C1200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200057004800450052004500200020002000430054005F005400450052004D0049004E0041004C005F004900440020003D002000400070005400650072006D0069006E0061006C004900640020000080C12000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200029002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000080C1200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200041004E004400200020002000430053005F0053005400410054005500530020003D002000300020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000080C1200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020004F005200440045005200200042005900200020002000430053005F004F00500045004E0049004E0047005F004400410054004500200044004500530043002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020000080C1200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200029002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000002D40007000500065006E00640069006E00670043006C006F00730069006E0067005300740061007400750073000019400070004F00700065006E00530074006100740075007300008095530045004C004500430054002000540045005F005400450052004D0049004E0041004C005F0054005900500045002000460052004F004D0020005400450052004D0049004E0041004C0053002000570048004500520045002000540045005F005400450052004D0049004E0041004C005F004900440020003D002000400070005400650072006D0069006E0061006C004900640000033000000D7B0030003A00440034007D00000D7B0030003A00440032007D00000D7B0030003A00440039007D00001769006E0070007500740042007500660066006500720000495400720061006E00730066006F0072006D0042006C006F0063006B003A00200049006E00700075007400200062007500660066006500720020006900730020006E0075006C006C0000196F0075007400700075007400420075006600660065007200004B5400720061006E00730066006F0072006D0042006C006F0063006B003A0020004F0075007400700075007400200062007500660066006500720020006900730020006E0075006C006C00004F5400720061006E00730066006F0072006D0042006C006F0063006B003A00200050006100720061006D006500740065007200730020006F007500740020006F0066002000720061006E0067006500004742006C006F0063006B00530069007A0065003A0020004500720072006F007200200049006E00760061006C0069006400200042006C006F0063006B002000530069007A0065000033490056003A0020004500720072006F007200200049006E00760061006C00690064002000490056002000530069007A006500003F4D006F00640065003A0020004500720072006F007200200049006E00760061006C0069006400200043006900700068006500720020004D006F00640065000047500061006400640069006E0067003A0020004500720072006F007200200049006E00760061006C00690064002000500061006400640069006E00670020004D006F0064006500000F41005200430046004F0055005200000F61006C0067004E0061006D006500002F4300720065006100740065003A0020004500720072006F0072005F0050006100720061006D004E0075006C006C000007520043003400000D5200670062004B0065007900003F43007200650061007400650044006500630072007900700074006F0072003A0020005200670062006B006500790020006900730020006E0075006C006C00004343007200650061007400650044006500630072007900700074006F0072003A00200049006E00760061006C006900640020006B00650079002000730069007A006500004143007200650061007400650044006500630072007900700074006F0072003A00200049006E00760061006C00690064002000490056002000730069007A006500003B42006C006F0063006B00530069007A0065003A00200049006E00760061006C0069006400200062006C006F0063006B002000730069007A006500003143007200650061007400650044006500630072007900700074006F0072003A00200044006900730070006F0073006500000D7200670062004B0065007900003F43007200650061007400650044006500630072007900700074006F0072003A0020007200620067006B006500790020006900730020006E0075006C006C00004343007200650061007400650044006500630072007900700074006F0072003A00200049006E00760061006C006900640020004B00650079002000530069007A006500004143007200650061007400650044006500630072007900700074006F0072003A00200049006E00760061006C00690064002000490056002000530069007A006500002F63006F006E007400650078007400200063006F006E006E0065006300740069006F006E003D00740072007500650000315400720078005F003300470053005F00530074006100720074004300610072006400530065007300730069006F006E0000174200690065006E00760065006E00690064006F002000005B5400720078005F003300470053005F00530074006100720074004300610072006400530065007300730069006F006E0020006600610069006C006500640021002C00200045007800630065007000740069006F006E003A00200000335400720078005F003300470053005F005500700064006100740065004300610072006400530065007300730069006F006E00005D5400720078005F003300470053005F005500700064006100740065004300610072006400530065007300730069006F006E0020006600610069006C006500640021002C00200045007800630065007000740069006F006E003A002000002D5400720078005F003300470053005F0045006E0064004300610072006400530065007300730069006F006E0000575400720078005F003300470053005F0045006E0064004300610072006400530065007300730069006F006E0020006600610069006C006500640021002C00200045007800630065007000740069006F006E003A002000002B5400720078005F003300470053005F004100630063006F0075006E00740053007400610074007500730000555400720078005F003300470053005F004100630063006F0075006E00740053007400610074007500730020006600610069006C006500640021002C00200045007800630065007000740069006F006E003A00200000235400720078005F003300470053005F00530065006E0064004500760065006E007400004D5400720078005F003300470053005F00530065006E0064004500760065006E00740020006600610069006C006500640021002C00200045007800630065007000740069006F006E003A00200000000000B005866C8747B94B88800CC14E21E6DC0008B77A5C561934E0890B00051281200E0E080E121D1100081281380E0E080E0A1281281121121D0A00041281200E0E08121D0E00071281200E0E0E080A1121121D0D00061281200E0E0811210A121D1100061281380812814C0A1281281121121D0C0004011281201008100E100E0C0004011281381008100E100E060002020E121D0A0004020A100A1008121D090003020A121D101144072002011218121D08200401080E0A121D03200001030612250306121802060802060E02060A042001010A052002010A0E052001011218072003011218080E06200301080E0A07200401080E0A0E0F2007020A0A11341121112111211121102008020A0A113411211121112111210A102008020A0A113411211121112111210E12200A020A0A113411211121112111210E080E13200B020A0A113411211121112111210E080E0E14200C020A0A113411211121112111210E080E0E0A0600020E0A121D0320000A0420010A0805200102121D0328000A020602030611290306112C04200012180306122D0520010112250620020112180E0A2005020A113011210A0E0B2006020A113011210A0E0E0C2007020A113011210A0E0E020C2007020A113011210A0E0E0E0D2008020A113011210A0E0E0E020F2009020A113011210A0E0E0E1121020E2008020A113011210A0E0E0E112110200A020A113011210A0E0E0E11210A0A11200B020A113011210A0E0E0E11210A0A0212200B020A113011210A0E0E0E11210A0A112113200C020A113011210A0E0E0E11210A0A11210226200F020A113011210A0E0E0E11210A0A112115113101118090151131011180D8151131010A02092004020A0A0E1280AC0B2005020A0A0E1280AC11210A2005020A0A0E1280AC0E0D2006020A0A0E1280AC1130112105200101112105200101112907200201112111210320000825200D01121D101235101239101239101239101239101239101239100E101130100E100210021000060111301121101121101121100E020B0004112111301121112102042800121804280012250306112004000000000401000000040200000004040000000408000000041000000004200000000440000000048000000004000100000400020000040004000004000800000400100000040020000004004000000400800000040000010004000002000400000400030611240403000000040500000004060000000407000000030611280409000000040A000000040B00000004FFFFFFFF03061130040C000000040D000000040E000000041400000004150000000416000000041700000004180000000419000000041C000000041D000000041E000000041F000000042100000004220000000423000000042400000004250000000426000000042700000004280000000429000000042A000000042B000000042C000000042D000000042E000000042F0000000430000000043100000004320000000433000000043400000004350000000436000000043700000004380000000439000000043A000000043B000000043C000000043D000000043E000000043F000000044100000004420000000443000000044400000004450000000446000000044700000004480000000449000000044A000000044B000000044C000000044D000000044E000000044F0000000450000000045400000004550000000456000000045700000004580000000459000000045A000000045B000000045C000000045D000000045E000000045F0000000460000000046100000004620000000463000000046400000004650000000466000000046700000004680000000469000000046A000000046B000000046C000000046D000000046E000000046F0000000470000000047100000004720000000473000000047400000004750000000476000000047700000004780000000479000000047A000000047B000000047C000000047D000000047E000000047F000000048B000000048C000000048D000000048E000000048F0000000490000000049100000004920000000493000000049400000004950000000496000000049700000004980000000499000000049A000000049B000000049C000000049D000000049E000000049F00000004A000000004A100000004BE00000004BF00000004C800000004C900000004CA00000004CB00000004CC00000004CE00000004CF00000004D0000000042B010000042C010000042D010000042E010000042F01000004300100000431010000043201000004330100000434010000043501000004360100000437010000048F01000004F401000004F501000004FE01000004FF010000040102000004020200000403020000040402000004050200000406020000040702000004080200000409020000040A020000040B020000040C020000040D020000040E020000040F0200000410020000041102000004120200000413020000041402000004150200000416020000041702000004180200000419020000041A020000041B020000041C020000041D020000041E020000041F0200000420020000042102000004220200000423020000042402000004250200000426020000042702000004280200000429020000042A020000042B020000042D020000042F020000043002000004320200000433020000043402000004350200000436020000043702000004380200000439020000043A020000043B020000043C020000043D020000043E020000043F020000044002000004410200000442020000044302000004440200000445020000044602000004470200000448020000044A020000044B020000044C020000044D020000044E020000044F0200000450020000045102000004520200000453020000045402000004550200000456020000045702000004580200000459020000045A020000045B020000045C020000045D020000045E020000045F0200000460020000046102000004620200000463020000046402000004650200000466020000046702000004680200000469020000046A020000046B020000046C0200000471020000044C04000004AF04000004B0040000041305000004140500000477050000047805000004DB05000004DC050000043F0600000440420F000480841E0004BFC62D0003061134041A000000041B0000000452000000045300000004CD00000004D100000004D200000004D300000004D400000004D500000004D600000004D700000004D800000004D900000004DA00000004DB00000004DC00000004DD00000004DE00000004DF00000004E000000004E100000004E200000004E300000004E400000004E500000004E600000004E700000004E800000004E900000004EA00000004EB00000004EC00000004ED00000004EE00000004EF00000004F000000004F100000004F200000004F300000004F400000004F500000004E803000004400600000306113804900100000306113C0306114003061144030611480306114C0306115003061154030611580306115C0400000800040000100004000020000400004000040000800003061160030611640306116804E703000004E903000004F20300000306116C0306117003061174030611780306117C0406118080040F00000004061180840406118088040611808C040611809004EA03000004EB03000004061180940406118098041100000004120000000413000000048100000004820000000483000000048400000004850000000486000000048700000004880000000489000000048A00000004A2000000040611809C04061180A004061180A404061180A80306112104061280B004200011210320000E042001010E04200101080320000204200101020520001180900620010111809005200011808C0620010111808C0520001280B0062001011280B004280011210328000E032800080328000205280011809005280011808C0528001280B0042000112905200011809406200101118094042800112905280011809404061180B404061180B804061180BC04061180C004061180C404061180C804061180CC04061180D004061180D404061180D804FEFFFFFF04061180DC04061180E004061180E804061180EC04061180F004061180F404061180F804061180FC040611810004061181040406118108040611810C0800000000000000800F0005021181500A101281441008121D0A000212812012811C121D1000061281200A12817C12814C020E121D1700091281200A12817C12814C02118118128144020E121D1A000C0212814C0A0811640E02118118128144020E10128120121D0C00050211210E0E101121121D1600081281200A12817C12814C0211811812814402121D0E0005021281440E0E10128144121D090005020A080A02121D080004020A080A121D070003020A08121D160009020A08116402118118128144100A10118168121D0E0007020A113C0E081164100A121D070003020A0A121D1300080211680A081164128144128144100A121D0F00050211640A12812810128128121D100006020811640E12812410128138121D110007020811640E1281240E10128138121D0C0004020A128144128144121D080003020A1002121D0B000302128140128138121D0B0005020A0A08128144121D0F000502113D1281401281381164121D0B00030212817C128144121D140007020A1281441281441281441281441121121D120008020A112111211121112111211121121D060002020A121D110006020A0A10112110112110128144121D080004020A0A0A121D0A0004020A11211121121D0800030211210A121D100007020A0A0A0A12814410128144121D110008020A0A0A0A1281440E10128144121D140009020A0A0A0A1281440210128144101225121D0800030210080A121D15000A020A0A0A0A128144020E10128144101225121D0A0003020A10128144121D0900020210118178121D0A00030212813C1235121D0B00040212813C123502121D0A0003020A1012813C121D08000112813C1281240B0004020A1121101121121D0C0005020A112102101121121D120006020A128144112110128144101121121D150007020A12814411211180F810128144101121121D0D0004020A12814410128144121D0F0005020A12814410128144100A121D120006020A12814410128144101121100A121D130006020A12814410128144100A10128144121D180008020A128144112110128144101121100A10128144121D190009020A12814411210210128144101121100A10128144121D110007020A0E1281441121021012814C121D140008020A0E1281441121021180F81012814C121D080003020A115C121D0B000502080A1002100A121D190008021181680A0A128144101281441012814410128144121D0B000502020A12814402121D0D000702020A128144020202121D140008020A12817C1121112111211011211002121D070002021008121D0A0005020A0E0E1164121D0B0007020A0A0E090E08121D0C0005020A1281440A1225121D0E000602080A1281441002100A121D0D0004020A11211181581011815C0E0005020A1121118158021011815C130007020A1121118158021011815C101121121D07000202100E121D0700020112813C020600010112813C08000201112412813C1100060211440A10114410114410113D121D0D0006020A11211121021134121D080003020A1235121D110009020A11210A0811640E0E101235121D090003020A128124121D0A0004020A11701121121D07000302080A121D1400070212817C1121113D1281401281381002121D0700030E0E0E121D04061181140406118118040612814403061235072002011181140E0406128128040612812C04061281300406118134072002011181340E06200101128144092003011121112111210B20040111211121112111210C2005011121112111211121021020070111211121112111210211211121122008011121112111211121021121112111210500001281440520001281440B000212814412814412814409000202128144128144042001021C080001128144128144090002128144128144020320001C050800128144052800128144062001011281480820020112814411210500001281480B000212814812814812814809000202128148128148050800128148040611815004061181540406118158040611815C04061181600406118164040611816803061241052000118170062001011181700720040E0E0E0E0E042001020E052001122D0E0720020E124111450820030112410E114905280011817004061181700600020208121D0C0005020E0E081012817C121D09000502080E0E08121D0A000302081012817C121D0A0003020E1012817C121D0C000502080E0E1012817C121D09000402080E100E121D08000302112008121D06000202081120060001124D121D0900030208121D10116404061181780520010211200400010E0E04F526000003061D05080003020E100E100807000302100E0E080500020E080A080003010E1008100A05000202080E090003021181880E100E0406118188020605052001011D050A2005081D0508081D05080820031D051D0508080420001D050520001D1251042000115505200101115504200011590520010111590500001281900600011281900E0428001D050528001D12510428001155042800115908200212111D051D050406128194120009010E0E080E100A1011211008100E100E18000E010E0E080E0A112111210808112111211008100E100E0F0007010E0E081011211008100E100E100009010E0E0E080A11211008100E100E0100062001011180950600030E1C1C1C0F070512812012817C12814C128120020E070612813812814C0A0812813802070002021121112116070912813812817C12814C1281240A081144128138020C070412812012814C1281200205200112350E0520001280A9092003010E1280A9121D0520001280AD08200212390E1180B1042001011C16070A12812012817C12814C0812350A124D128120020A110707128120123512817C08124D128120020700040E0E1C1C1C0600020E0E1D1C080002112111211121070002020E1011210600011121112109200312390E1180B1081A070C12813812812811211121112111210202124D128138021D1C050701118114050002020E0E0607021181340204061280C909070512351C124D02020520001280CD04200108080B07051235124D1280CD02020607021210113403070102040001020E0520001280D50600011280D90E0920021280DD0E1280D90620011280DD0E072001011D1280DD0907021280DD1D1280DD0600030E0E0E0E042000122D052002010E1C0520001280E105200101122D060703122D02020907060E1C0E124D0E02052001122D080420011C0E0407020A02060002020E10080520001280ED062001011180F1062001011180F505200101124D05200108122519070E123508020812390A0E122D124D1280F902021280ED12150807040A11210A121C0407011218071511310111809005200101130007151131011180D805151131010A08070202151131010A0A070511211121112102020F070711301130020211809011808C02060703113002020D0706113002021180900211808C060703112102020420001D1C0507030808022D07181235081239123912391239123912390E11300A0E0E0202122D124D1280F91281011280E502021280ED1215052001112108110707124D122D1280CD1280ED02113012150607030E1130020907041121112111300204070111210307010A0307010E0307010805070111809005070111808C0507011280B004070111290507011180940500020E0E0E0420011C08230710123512251281440E11280A124D1280F91239122D1280CD02021181501280ED12150507011281200A070412812008128120020420010E0804200102080500010E1D0E0700040E0E0E0E0E58072C0A128144128144128144128144128144121011816811211121020A12814412814412350A113C02020212814C112111211180F8128144020E0A0202124D1280CD124D1280CD0A1225112111210E1280E502021D0E1121042001080E0507030202020807041235124D0202090705123508124D02022707171235080A113C11440E0E020211441144113D080811211121112111211121124D1280CD02020E0707113812351239124D02113C02060703124D020212070912351239113811441121124D020211680500020A0A0A0500020E0E1C3207171281441281440202128140112112813C12813C02020E081281281121112112813C12814411815C11211280E5020211640D07071235081C124D1280E5020229071512814412814412814412814412814412101144114411440812817C113D020A0A08020A0202113422070D12814412814412813C12351281441281440212817C12814412814412814402020320000D042000123516070A12351225124D1280F91239122D02021280ED121518070C123512250208124D1280F91239122D02021280ED12150A070612351C0A124D020206070312250E020507021225020407020E0205200201081C200711123511241121020E122D080E124D1280CD1239122D02021280ED1124121509070412813C123502020B07051235124D0202118178062001011281110A000311211121081181150A20031D122D0E0E118119042000124D062001081D122D0600030E0E1C1C33071712351D122D11817802124D1280CD11211121112111211280F908122D12391280E502021181781D122D081D1C1280ED121510070612813C112111211121112112813C0A07041281441281440A020707031281440A0207070312814C020209070412814411210A02080703112112814402060702128144020507021121020520020E080316070E12350E08080808020202124D1280CD1280E5020231071712350A0A0E128144128144128144128144128144128144128144021144114402113D11211121124D1280CD121002021B070B12351281441281441281441281440A124D1280CD02021181680420010E0E06200212350E0E05000111210E070002112111210813070A11211121112112350808124D1280CD020206200101118129072002010E1180B10620011239123905200112390E1207091181540A0E124D1239123902021181541A070D12350A11211121124D123912391239122D02021280ED121517070D12351281440A0A0A0E0211211210124D1280CD02020F07071281440A1235124D02021181580C07051121122D1280ED0212150E070611211121122D1280ED021215052001112908062001113D11290F0707123511291129124D1280CD02020707040E124D02020B07051235124D1280E502020F070612812412813812101280E502020D0707123508124D124D02117002060002020E100605000111210806200212350E1C05000111210D07200212350E1D1C0620021235080E1B070F080806061180C41121123512351235124D1280CD02020D11210A070612351C0E124D0E020707030E1D1C11210507011281440607021281441C0507011281480507011181700500020E1C1C1107080E1235122D0E1181701280ED021215050702122D02060702021181700520001280D90620011280DD080520001281350B0703122D1181701D1280DD072002020E1181390C0705122D122D1280ED0212150820020112814111450807040E12813D0E0209200211491281491149060702128145020A07061C1235124D0802020A070612351C124D08020204200106081107091235080808124D1280CD02021181780520020E0E0E0F070912350E0E0E0E124D1280CD02020E07081235080808124D1280CD02020807031235124D124D090705021235124D02020607040E020E020607040E080E02050002080E0E0400010B0E0500011D050B0B2003011281651211118169072003081D0508080600020B1D05080520020E0808040001080E2107151281980E0E0E0E090E0B0B0B1D051D050E12111D0512815D1281610802020B072003011D0508081B070F1281980E0E090B1D05121112815D128161081D051D0502020B0400010A0E08070608080E0A02020507030802020300000104061181A40900020112817911817D0407011D05021D05052002010E0E07070505050808020707031D051D050206070405080802080003011281790808040001011C062003010808080807021D12511D1251040701115504070111590707021281991D05050701128190080003020E0E1181390607021281900205070212110204070112110507011D1251042000121D0E07051281201280A9121D1280E5021407081281381281280E11211280A9121D1280E5021107061281381281281280A9121D1280E5020F07061281200E1280A9121D1280E502190100145753492E53514C427573696E6573734C6F67696300000501000000000E0100094D6963726F736F667400002001001B436F7079726967687420C2A9204D6963726F736F6674203230313200002901002431306432653537662D303966322D343639642D396665312D35366639356262383932303600000C010007312E302E302E3000000801000701000000000801000800000000001E01000100540216577261704E6F6E457863657074696F6E5468726F7773010000000000000085A7685900000000020000001C01000038B5040038970400525344530C85F1BFCBFF244D91B668256DAFED4101000000633A5C5446535C5769676F735C52656C65617365735C30332E3030355C5769676F732053797374656D5C5753492E53514C427573696E6573734C6F6769635C6F626A5C44656275675C53514C427573696E6573734C6F6769632E7064620000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000100100000001800008000000000000000000000000000000100010000003000008000000000000000000000000000000100000000004800000058C00400480300000000000000000000480334000000560053005F00560045005200530049004F004E005F0049004E0046004F0000000000BD04EFFE00000100000001000000000000000100000000003F000000000000000400000002000000000000000000000000000000440000000100560061007200460069006C00650049006E0066006F00000000002400040000005400720061006E0073006C006100740069006F006E00000000000000B004A8020000010053007400720069006E006700460069006C00650049006E0066006F00000084020000010030003000300030003000340062003000000034000A00010043006F006D00700061006E0079004E0061006D006500000000004D006900630072006F0073006F00660074000000540015000100460069006C0065004400650073006300720069007000740069006F006E00000000005700530049002E00530051004C0042007500730069006E006500730073004C006F0067006900630000000000300008000100460069006C006500560065007200730069006F006E000000000031002E0030002E0030002E00300000004C001500010049006E007400650072006E0061006C004E0061006D0065000000530051004C0042007500730069006E006500730073004C006F006700690063002E0064006C006C00000000005C001B0001004C006500670061006C0043006F007000790072006900670068007400000043006F0070007900720069006700680074002000A90020004D006900630072006F0073006F006600740020003200300031003200000000005400150001004F0072006900670069006E0061006C00460069006C0065006E0061006D0065000000530051004C0042007500730069006E006500730073004C006F006700690063002E0064006C006C00000000004C0015000100500072006F0064007500630074004E0061006D006500000000005700530049002E00530051004C0042007500730069006E006500730073004C006F0067006900630000000000340008000100500072006F006400750063007400560065007200730069006F006E00000031002E0030002E0030002E003000000038000800010041007300730065006D0062006C0079002000560065007200730069006F006E00000031002E0030002E0030002E0030000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
WITH PERMISSION_SET = SAFE
GO

CREATE PROCEDURE [dbo].[Trx_3GS_UpdateCardSession]
	@VendorId [nvarchar](50),
	@SerialNumber [nvarchar](50),
	@MachineNumber [int],
	@ExternalTrackData [nvarchar](50),
	@PlaySessionId [bigint],
	@PlayedAmount [decimal](20, 2),
	@WonAmount [decimal](20, 2),
	@PlayedCount [int],
	@WonCount [int],
	@FinalBalance [decimal](20, 2),
	@BillIn [decimal](20, 2),
	@StatusCode [int] OUTPUT,
	@StatusText [nvarchar](max) OUTPUT,
	@StatusError [nvarchar](max) OUTPUT
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [SQLBusinessLogic].[WSI.Common.SqlProcedures].[Trx_3GS_UpdateCardSession]
GO

CREATE PROCEDURE [dbo].[Trx_3GS_StartCardSession]
	@VendorId [nvarchar](50),
	@SerialNumber [nvarchar](50),
	@MachineNumber [int],
	@ExternalTrackData [nvarchar](50),
	@PlaySessionId [bigint] OUTPUT,
	@PlayableBalance [decimal](20, 2) OUTPUT,
	@StatusCode [int] OUTPUT,
	@StatusText [nvarchar](max) OUTPUT,
	@StatusError [nvarchar](max) OUTPUT
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [SQLBusinessLogic].[WSI.Common.SqlProcedures].[Trx_3GS_StartCardSession]
GO

CREATE PROCEDURE [dbo].[Trx_3GS_SendEvent]
	@ExternalTrackData [nvarchar](50),
	@VendorId [nvarchar](50),
	@SerialNumber [nvarchar](50),
	@MachineNumber [int],
	@EventId [bigint],
	@Amount [decimal](20, 2),
	@StatusCode [int] OUTPUT,
	@StatusText [nvarchar](max) OUTPUT,
	@StatusError [nvarchar](max) OUTPUT
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [SQLBusinessLogic].[WSI.Common.SqlProcedures].[Trx_3GS_SendEvent]
GO

CREATE PROCEDURE [dbo].[Trx_3GS_EndCardSession]
	@VendorId [nvarchar](50),
	@SerialNumber [nvarchar](50),
	@MachineNumber [int],
	@ExternalTrackData [nvarchar](50),
	@PlaySessionId [bigint],
	@PlayedAmount [decimal](20, 2),
	@WonAmount [decimal](20, 2),
	@PlayedCount [int],
	@WonCount [int],
	@FinalBalance [decimal](20, 2),
	@BillIn [decimal](20, 2),
	@StatusCode [int] OUTPUT,
	@StatusText [nvarchar](max) OUTPUT,
	@StatusError [nvarchar](max) OUTPUT
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [SQLBusinessLogic].[WSI.Common.SqlProcedures].[Trx_3GS_EndCardSession]
GO

CREATE PROCEDURE [dbo].[Trx_3GS_AccountStatus]
	@ExternalTrackData [nvarchar](50),
	@VendorId [nvarchar](50),
	@MachineNumber [int],
	@Balance [decimal](20, 2) OUTPUT,
	@StatusCode [int] OUTPUT,
	@StatusText [nvarchar](max) OUTPUT,
	@StatusError [nvarchar](max) OUTPUT
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [SQLBusinessLogic].[WSI.Common.SqlProcedures].[Trx_3GS_AccountStatus]
GO



IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'TITO' AND GP_SUBJECT_KEY = 'PendingPrint.Enabled')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('TITO','PendingPrint.Enabled','1')


IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PSAClient' AND GP_SUBJECT_KEY = 'EnableDailyReportOffset')
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES('PSAClient', 'EnableDailyReportOffset', '0')
END
GO

/* PIN PAD */
IF NOT EXISTS (SELECT 1 
                FROM sys.columns 
                WHERE object_id = object_id(N'[dbo].[pinpad_transactions_reconciliation]') 
                  AND name = 'ptc_code'
              )
BEGIN              
  ALTER TABLE [dbo].[pinpad_transactions_reconciliation]
  ADD [ptc_code] [int] NOT NULL DEFAULT 20
END        
ELSE
BEGIN 
  SELECT '***** Field pinpad_transactions_reconciliation.ptc_code already exists *****';
END
GO  

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GT_Base_Report_Data]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[GT_Base_Report_Data]
GO

CREATE PROCEDURE [dbo].[GT_Base_Report_Data]
(
      @pBaseType                   INTEGER
    , @pTimeInterval               INTEGER
    , @pDateFrom                   DATETIME
    , @pDateTo                     DATETIME
    , @pOnlyActivity               INTEGER
    , @pOrderBy                    INTEGER
    , @pValidTypes                 VARCHAR(4096)
    , @pSelectedCurrency           NVARCHAR(100)
    , @pTotalToSelectedCurrency    INTEGER
)
AS
BEGIN

-- DECLARATIONS
   -- @_DAYS_AND_TABLES:    A temporary table with all intervals and all base type selected (used to show base types without activity).
   DECLARE @_DAYS_AND_TABLES TABLE(DATE_TIME DATETIME, TABLE_IDENTIFIER_VALUE BIGINT, TABLE_IDENT_NAME VARCHAR(50), TABLE_TYPE_IDENT BIGINT, TABLE_TYPE_NAME VARCHAR(50))
   -- @_DAY_VAR:            Variable to create the intervals
   DECLARE @_DAY_VAR DATETIME

-- PARAMETERS
     DECLARE @_BASE_TYPE                  AS   INTEGER -- 0 BY TABLE TYPE, 1 BY TABLE
     DECLARE @_TIME_INTERVAL              AS   INTEGER -- -1, 0 DAYS, 1 MONTH, 2 YEAR
     DECLARE @_DATE_FROM                  AS   DATETIME
     DECLARE @_DATE_TO                    AS   DATETIME
     DECLARE @_ONLY_ACTIVITY              AS   INTEGER -- 0 SHOW ALL, 1 SHOW ONLY WITH ACTIVITY
     DECLARE @_ORDER_BY                   AS   INTEGER -- 0 BY INTERVAL DESC, 1 BY IDENTIFIER ASC
     DECLARE @_DELIMITER                  AS   CHAR(1)
     DECLARE @_TYPES_TABLE                TABLE(SST_ID INT, SST_VALUE VARCHAR(50)) -- TO STORE THE VALID GAMING TABLE TYPES
     DECLARE @_SELECTED_CURRENCY          AS   NVARCHAR(3)
     DECLARE @_TOTAL_TO_SELECTED_CURRENCY AS   INTEGER
     DECLARE @_CHIP_RE                    AS   INTEGER
     DECLARE @_CHIP_NRE                   AS   INTEGER
     DECLARE @_CHIP_COLOR                 AS   INTEGER
----------------------------------------------------------------------------------------------------------------

-- Initialization --
  SET @_BASE_TYPE                  =   @pBaseType
  SET @_TIME_INTERVAL              =   @pTimeInterval
  SET @_DATE_FROM                  =   @pDateFrom
  SET @_DATE_TO                    =   @pDateTo
  SET @_ONLY_ACTIVITY              =   @pOnlyActivity
  SET @_ORDER_BY                   =   @pOrderBy
  SET @_DELIMITER                  =   ','
  SET @_SELECTED_CURRENCY          =   @pSelectedCurrency
  SET @_TOTAL_TO_SELECTED_CURRENCY =   @pTotalToSelectedCurrency
  SET @_CHIP_RE                    =   1001
  SET @_CHIP_NRE                   =   1002
  SET @_CHIP_COLOR                 =   1003
----------------------------------------------------------------------------------------------------------------

-- CHECK DATE PARAMETERS
IF @_DATE_FROM IS NULL
BEGIN
   -- IF DATE FROM IS NULL, USER FIRST POSIBLE DATE
   SET @_DATE_FROM = CAST('' AS DATETIME)
   SET @pDateFrom  = CAST('' AS DATETIME)
END

IF @_DATE_TO IS NULL
BEGIN
   -- IF DATE TO IS NULL, QUERY UNTIL CURRENT DATE
   SET @_DATE_TO = CAST(GETDATE() AS DATETIME)
   SET @pDateTo  = CAST(GETDATE() AS DATETIME)
END

-- ASSIGN TYPES PARAMETER INTO TABLE
INSERT INTO @_TYPES_TABLE SELECT * FROM dbo.SplitStringIntoTable(@pValidTypes, @_DELIMITER, DEFAULT)

IF @_TIME_INTERVAL <> -1
 BEGIN
   -- INTERVALS DATES AND TABLE PREPARATION

   -- PREPARE DATES RANGE DEPENDING ON TIME INTERVAL
   SET @_DATE_FROM = CASE
                        WHEN @_TIME_INTERVAL = 0 THEN -- BY DAY
                           DATEADD(dd, 0, DATEDIFF(dd, 0, @_DATE_FROM))
                        WHEN @_TIME_INTERVAL = 1 THEN -- BY MONTH
                           CAST(CAST(YEAR(@_DATE_FROM) AS VARCHAR(4)) + RIGHT('0' + CAST(MONTH(@_DATE_FROM) AS VARCHAR(2)), 2) + '01' AS DATETIME)
                        WHEN @_TIME_INTERVAL = 2 THEN -- BY YEAR
                           CAST(CAST(YEAR(@_DATE_FROM) AS VARCHAR(4)) + '0101' AS DATETIME)
                     END

   SET @_DATE_TO = CASE
          WHEN @_TIME_INTERVAL = 0 THEN -- BY DAY
                           DATEADD(DAY, 1, CAST(@_DATE_TO AS DATETIME))
                        WHEN @_TIME_INTERVAL = 1 THEN -- BY MONTH
                           DATEADD(MONTH, 1, CAST(CAST(YEAR(@_DATE_TO) AS VARCHAR(4)) + RIGHT('0' + CAST(MONTH(@_DATE_TO) AS VARCHAR(2)), 2) + '01' AS DATETIME))
                        WHEN @_TIME_INTERVAL = 2 THEN -- BY YEAR
                           DATEADD(YEAR, 1, CAST(CAST(YEAR(@_DATE_TO) AS VARCHAR(4)) + '0101' AS DATETIME))
                     END

   -- PREPARE THE TIME INTERVALS TABLE
   SET @_DAY_VAR = @_DATE_FROM
   WHILE @_DAY_VAR < @_DATE_TO
   BEGIN
         -- SET THE LINK FIELD FOR THE RESULTS
         IF @_BASE_TYPE = 0
          BEGIN
            -- LINK WITH TABLE TYPES
            INSERT INTO @_DAYS_AND_TABLES
                 SELECT   CAST(@_DAY_VAR AS DATETIME), X.GTT_GAMING_TABLE_TYPE_ID, X.GTT_NAME AS TABLE_IDENT, NULL, NULL
                   FROM   GAMING_TABLES_TYPES AS X
                  WHERE   X.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
                          AND     X.GTT_ENABLED = 1
          END
         ELSE
          BEGIN
            -- LINK WITH TABLES
            INSERT INTO @_DAYS_AND_TABLES
                 SELECT   CAST(@_DAY_VAR AS DATETIME), X.GT_GAMING_TABLE_ID, X.GT_NAME AS TABLE_IDENT, Z.GTT_GAMING_TABLE_TYPE_ID, Z.GTT_NAME
                   FROM   GAMING_TABLES AS X
              LEFT JOIN   GAMING_TABLES_TYPES AS Z
                     ON   Z.GTT_GAMING_TABLE_TYPE_ID = X.GT_TYPE_ID
                  WHERE   Z.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
                          AND X.GT_ENABLED = 1
          END

          -- SET INCREMENT
          SET @_DAY_VAR = CASE
                               WHEN @_TIME_INTERVAL = 0 THEN DATEADD(DAY,1,@_DAY_VAR)     -- DAY
                               WHEN @_TIME_INTERVAL = 1 THEN DATEADD(MONTH,1,@_DAY_VAR)   -- MONTH
                               WHEN @_TIME_INTERVAL = 2 THEN DATEADD(YEAR,1,@_DAY_VAR)    -- YEAR
                          END
   END

END -- IF INTERVALS <> - 1
ELSE
BEGIN

   -- SET THE LINK FIELD FOR THE RESULTS WITHOUT INTERVALS
   IF @_BASE_TYPE = 0
    BEGIN
      -- LINK WITH TABLE TYPES
      INSERT INTO @_DAYS_AND_TABLES
           SELECT   NULL, X.GTT_GAMING_TABLE_TYPE_ID, X.GTT_NAME AS TABLE_IDENT, NULL, NULL
             FROM   GAMING_TABLES_TYPES AS X
            WHERE   X.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
                    AND X.GTT_ENABLED = 1
    END
   ELSE
    BEGIN
      -- LINK WITH TABLES
      INSERT INTO @_DAYS_AND_TABLES
           SELECT   NULL, X.GT_GAMING_TABLE_ID, X.GT_NAME AS TABLE_IDENT, Z.GTT_GAMING_TABLE_TYPE_ID, Z.GTT_NAME
             FROM   GAMING_TABLES AS X
        LEFT JOIN   GAMING_TABLES_TYPES AS Z
               ON   Z.GTT_GAMING_TABLE_TYPE_ID = X.GT_TYPE_ID
            WHERE   Z.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
                    AND X.GT_ENABLED = 1
    END

 END


-- SELECT INTO TEMPORARY TABLE WITH ALL DATA
-- SPECIFIC REPORT QUERY
  SELECT   X.TABLE_IDENTIFIER
         , X.TABLE_NAME
         , X.TABLE_TYPE
         , X.TABLE_TYPE_NAME
         , SUM(X.S_DROP_GAMBLING_TABLE) + SUM (X.S_DROP_CASHIER) TOTAL_DROP
         , SUM(X.S_DROP_GAMBLING_TABLE) TOTAL_DROP_GAMBLING_TABLE
         , SUM(X.S_DROP_CASHIER) TOTAL_DROP_CASHIER
         , SUM(X.S_WIN) TOTAL_WIN
         , ISNULL(SUM(X.S_TIP),0)  TOTAL_TIP
         , CASE WHEN SUM(X.S_DROP_GAMBLING_TABLE) + SUM (X.S_DROP_CASHIER) = 0 THEN 0 ELSE CAST(SUM(X.S_WIN) AS DECIMAL(18,2)) / CAST(SUM(X.S_DROP_GAMBLING_TABLE) + SUM (X.S_DROP_CASHIER) AS DECIMAL(18,2)) * 100 END AS WIN_DROP
         , MIN(X.THEORIC_HOLD) AS THEORIC_HOLD
         , CASE WHEN SUM(X.S_DROP_GAMBLING_TABLE) + SUM (X.S_DROP_CASHIER) = 0 THEN 0 ELSE SUM(CAST(X.S_TIP AS DECIMAL(18,2))) / CAST(SUM(X.S_DROP_GAMBLING_TABLE) + SUM (X.S_DROP_CASHIER) AS DECIMAL(18,2)) * 100 END AS TIP_DROP
         , SUM(X.S_WIN) + SUM(X.S_TIP) AS WIN_TIPS
         , X.CM_DATE_ONLY
         , MIN(X.OPEN_HOUR) AS OPEN_HOUR
         , MAX(X.CLOSE_HOUR) AS CLOSE_HOUR
         , SUM(X.SESSION_SECONDS) AS SESSION_SECONDS
         , COUNT(X.SESSION_SECONDS) AS SESSION_SUM         

   INTO #GT_TEMPORARY_REPORT_DATA

   FROM (
           -- CORE QUERY
            SELECT CASE WHEN @_TIME_INTERVAL = 0 THEN     -- TO FILTER BY DAY
                        DATEADD(dd, 0, DATEDIFF(dd, 0, CS_OPENING_DATE))
                      WHEN @_TIME_INTERVAL = 1 THEN     -- TO FILTER BY MONTH
                        CAST(CAST(YEAR(CS_OPENING_DATE) AS VARCHAR(4)) + RIGHT('0' + CAST(MONTH(CS_OPENING_DATE) AS VARCHAR(2)), 2) + '01' AS DATETIME)
                      WHEN @_TIME_INTERVAL = 2 THEN     -- TO FILTER BY YEAR
                        CAST(CAST(YEAR(CS_OPENING_DATE) AS VARCHAR(4)) + '01'+ '01' AS DATETIME)
                        END                                                                                       AS CM_DATE_ONLY
                     , (CASE WHEN @_BASE_TYPE = 0 THEN GTT_GAMING_TABLE_TYPE_ID ELSE GT.GT_GAMING_TABLE_ID END)   AS TABLE_IDENTIFIER -- GET THE BASE TYPE IDENTIFIER
                     , (CASE WHEN @_BASE_TYPE = 0 THEN GTT_NAME ELSE GT.GT_NAME END)                              AS TABLE_NAME       -- GET THE BASE TYPE IDENTIFIER NAME
                     , (CASE WHEN @_BASE_TYPE = 0 THEN NULL ELSE GTT_GAMING_TABLE_TYPE_ID END)                    AS TABLE_TYPE       -- TYPE
                     , (CASE WHEN @_BASE_TYPE = 0 THEN NULL ELSE GTT_NAME END)                                    AS TABLE_TYPE_NAME  -- TYPE NAME

                     ,  CASE WHEN @_TOTAL_TO_SELECTED_CURRENCY = 0 THEN
                                     DBO.GT_CALCULATE_DROP_GAMBLING_TABLES(ISNULL(GTS_COLLECTED_AMOUNT                , 0)
                                                                         , ISNULL(GTS_COLLECTED_DROPBOX_AMOUNT        , 0)
                                                                         , ISNULL(GTS_COLLECTED_DROPBOX_CHIPS_AMOUNT  , 0)
                                                                         , 0 -- It does not allow takings of dropbox with tickets
                                                                         , GT_HAS_INTEGRATED_CASHIER)
                             WHEN @_TOTAL_TO_SELECTED_CURRENCY = 1 THEN
                                     DBO.GT_CALCULATE_DROP_GAMBLING_TABLES(SUM(ISNULL(GTSC_COLLECTED_AMOUNT                , 0))
                                                                         , SUM(ISNULL(CASE WHEN GTSC_TYPE = 0            THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                                           WHEN GTSC_TYPE = 1            THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                                      ELSE 0 END,  0))
                                                                         , SUM(ISNULL(CASE WHEN GTSC_TYPE = @_CHIP_RE    THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                                           WHEN GTSC_TYPE = @_CHIP_NRE   THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                                           WHEN GTSC_TYPE = @_CHIP_COLOR THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                             ELSE 0 END,  0))
                                                                         , 0 -- It does not allow takings of dropbox with tickets
                                                                         , GT_HAS_INTEGRATED_CASHIER)
                             END                                                                                  AS S_DROP_GAMBLING_TABLE

                     ,  CASE WHEN @_TOTAL_TO_SELECTED_CURRENCY = 0 THEN
                                     DBO.GT_CALCULATE_DROP_CASHIER(ISNULL(GTS_OWN_SALES_AMOUNT, 0)
                                                                 , ISNULL(GTS_EXTERNAL_SALES_AMOUNT, 0))
                             WHEN @_TOTAL_TO_SELECTED_CURRENCY = 1 THEN
                                     DBO.GT_CALCULATE_DROP_CASHIER(SUM(ISNULL(GTSC_OWN_SALES_AMOUNT, 0))
                                                                 , SUM(ISNULL(GTSC_EXTERNAL_SALES_AMOUNT, 0)))
                             END                                                                                  AS S_DROP_CASHIER

                     , GT_THEORIC_HOLD AS THEORIC_HOLD
                     ,  CASE WHEN @_TOTAL_TO_SELECTED_CURRENCY = 0 THEN
                                     DBO.GT_CALCULATE_WIN(ISNULL(GTS_FINAL_CHIPS_AMOUNT      , 0)
                                                        , ISNULL(GTS_INITIAL_CHIPS_AMOUNT    , 0)
                                                        , ISNULL(GTS_FILLS_CHIPS_AMOUNT      , 0)
                                                        , ISNULL(GTS_CREDITS_CHIPS_AMOUNT    , 0)                                                        
                                                        , ISNULL(GTS_TIPS                    , 0)
                                                        , ISNULL(GTS_OWN_SALES_AMOUNT        , 0)
                                                        , ISNULL(GTS_EXTERNAL_SALES_AMOUNT   , 0)
                                                        , ISNULL(GTS_COLLECTED_AMOUNT        , 0)
                                                        , ISNULL(GTS_COLLECTED_DROPBOX_AMOUNT        , 0)
                                                        , ISNULL(GTS_COLLECTED_DROPBOX_CHIPS_AMOUNT  , 0)
                                                        , 0 -- It does not allow takings of dropbox with tickets)
                                                        , GT_HAS_INTEGRATED_CASHIER)
                             WHEN @_TOTAL_TO_SELECTED_CURRENCY = 1 THEN
                                     DBO.GT_CALCULATE_WIN(SUM(ISNULL(GTSC_FINAL_CHIPS_AMOUNT   , 0))
                                                        , SUM(ISNULL(GTSC_INITIAL_CHIPS_AMOUNT , 0))
                                                        , SUM(ISNULL(GTSC_FILLS_CHIPS_AMOUNT   , 0))
                                                        , SUM(ISNULL(GTSC_CREDITS_CHIPS_AMOUNT  , 0))
                                                        , SUM(ISNULL(GTSC_TIPS                 , 0))
                                                        , SUM(ISNULL(GTSC_OWN_SALES_AMOUNT     , 0))
                                                        , SUM(ISNULL(GTSC_EXTERNAL_SALES_AMOUNT, 0))
                                                        , SUM(ISNULL(GTSC_COLLECTED_AMOUNT     , 0))
                                                        , SUM(ISNULL(CASE WHEN GTSC_TYPE = 0            THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                          WHEN GTSC_TYPE = 1            THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                          ELSE 0 END,  0))
                                                        , SUM(ISNULL(CASE WHEN GTSC_TYPE = @_CHIP_RE    THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                          WHEN GTSC_TYPE = @_CHIP_NRE   THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                          WHEN GTSC_TYPE = @_CHIP_COLOR THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                          ELSE 0 END,  0))
                                                        , 0 -- It does not allow takings of dropbox with tickets
                                                        , GT_HAS_INTEGRATED_CASHIER)
                             END AS S_WIN

                     ,  CASE WHEN @_TOTAL_TO_SELECTED_CURRENCY = 0  AND GTSC_TYPE = 0 THEN
                                     SUM(ISNULL(GTS_TIPS, 0))
                             WHEN @_TOTAL_TO_SELECTED_CURRENCY = 1 THEN
                                     SUM(ISNULL(GTSC_TIPS, 0))
                             ELSE 0
                             END AS S_TIP

                     , CS.CS_OPENING_DATE                                                                         AS OPEN_HOUR
                     , CS.CS_CLOSING_DATE                                                                         AS CLOSE_HOUR
                     , DATEDIFF(SECOND, CS_OPENING_DATE, ISNULL(CS_CLOSING_DATE, GETDATE()))                      AS SESSION_SECONDS
                                   FROM   GAMING_TABLES_SESSIONS GTS
                    LEFT  JOIN   GAMING_TABLES_SESSIONS_BY_CURRENCY GTSC
                           ON    GTSC.GTSC_GAMING_TABLE_SESSION_ID = GTS.GTS_GAMING_TABLE_SESSION_ID
                          AND    GTSC_ISO_CODE IN (@_SELECTED_CURRENCY)
                  INNER JOIN   CASHIER_SESSIONS CS ON GTS.GTS_CASHIER_SESSION_ID = CS.CS_SESSION_ID
                                         AND   CS_OPENING_DATE >= @pDateFrom AND CS_OPENING_DATE < @pDateTo
                  INNER JOIN   GAMING_TABLES GT ON GTS.GTS_GAMING_TABLE_ID = GT.GT_GAMING_TABLE_ID
                           AND   GT.GT_TYPE_ID IN (SELECT SST_VALUE FROM @_TYPES_TABLE)
                  INNER JOIN   GAMING_TABLES_TYPES GTT ON GT.GT_TYPE_ID = GTT.GTT_GAMING_TABLE_TYPE_ID
               WHERE   CS_STATUS = 1  -- Only closed sessions
               GROUP BY CS_OPENING_DATE, GTT_GAMING_TABLE_TYPE_ID, GT_GAMING_TABLE_ID, GTT_NAME, GT_NAME, GTS_OWN_SALES_AMOUNT, GTS_EXTERNAL_SALES_AMOUNT, GTS_COLLECTED_AMOUNT, GT_HAS_INTEGRATED_CASHIER
                      , GTS_COLLECTED_DROPBOX_AMOUNT, GTS_COLLECTED_DROPBOX_AMOUNT, GTS_DROPBOX_ENABLED, GTS_COLLECTED_DROPBOX_CHIPS_AMOUNT, GT_THEORIC_HOLD, GTS_FINAL_CHIPS_AMOUNT, GTS_INITIAL_CHIPS_AMOUNT
                      , GTS_FILLS_CHIPS_AMOUNT, GTS_CREDITS_CHIPS_AMOUNT, GTSC_TYPE, GTS_TIPS, GTSC_TIPS, CS_CLOSING_DATE
          -- END CORE QUERY

        ) AS X

  GROUP BY   X.TABLE_TYPE, X.TABLE_IDENTIFIER, X.TABLE_TYPE_NAME, X.TABLE_NAME, X.CM_DATE_ONLY  -- Group by indentifier and time interval

IF @_TIME_INTERVAL <> -1
BEGIN
   -- INTERVALS DATES AND TABLE FINAL PREPARATION

   -- FILTER ACTIVITY
   IF @_ONLY_ACTIVITY = 0
    BEGIN
      -- JOIN THE SELECT WITH ALL DATA
      SELECT
              DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER,
              DT.TABLE_IDENT_NAME AS TABLE_NAME,
              DT.TABLE_TYPE_IDENT AS TABLE_TYPE,
              DT.TABLE_TYPE_NAME AS TABLE_TYPE_NAME,
              ISNULL(ZZ.TOTAL_DROP_GAMBLING_TABLE,0) AS TOTAL_DROP_GAMBLING_TABLE,
              ISNULL(ZZ.TOTAL_DROP_CASHIER,0) AS  TOTAL_DROP_CASHIER,
              ISNULL(ZZ.TOTAL_DROP, 0) AS TOTAL_DROP,
              ISNULL(ZZ.TOTAL_WIN, 0) AS TOTAL_WIN,
              ISNULL(ZZ.TOTAL_TIP, 0) AS TOTAL_TIP,
              ISNULL(ZZ.WIN_DROP, 0) AS WIN_DROP,
              ISNULL(ZZ.THEORIC_HOLD, 0) AS THEORIC_HOLD,
              ISNULL(ZZ.TIP_DROP, 0) AS TIP_DROP,
              ISNULL(ZZ.WIN_TIPS, 0) AS WIN_TIPS,
              ZZ.OPEN_HOUR,
              ZZ.CLOSE_HOUR,
              CASE WHEN ZZ.CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED,
              CASE WHEN ZZ.SESSION_SECONDS = 0 THEN DATEDIFF(SECOND,ZZ.OPEN_HOUR,GETDATE()) ELSE ISNULL(ZZ.SESSION_SECONDS, 0) END AS SESSION_SECONDS,
              ISNULL(ZZ.SESSION_SUM, 0) AS SESSION_SUM,
              DATE_TIME              

      FROM @_DAYS_AND_TABLES DT

        LEFT JOIN #GT_TEMPORARY_REPORT_DATA ZZ
                     ON
                          (
                              (@_TIME_INTERVAL = 0 AND DATE_TIME = ZZ.CM_DATE_ONLY)
                           OR (@_TIME_INTERVAL = 1 AND MONTH(DATE_TIME) = MONTH(ZZ.CM_DATE_ONLY) AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
                           OR (@_TIME_INTERVAL = 2 AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
                          )
                    AND   TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER

       -- SET ORDER
       ORDER BY   DT.TABLE_TYPE_IDENT ASC,
                  CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC,
                  DATE_TIME DESC;

    END
   ELSE
    BEGIN
      -- JOIN THE SELECT HIDDING WITHOUT ACTIVITY
      SELECT
              DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER,
              DT.TABLE_IDENT_NAME AS TABLE_NAME,
              DT.TABLE_TYPE_IDENT AS TABLE_TYPE,
              DT.TABLE_TYPE_NAME AS TABLE_TYPE_NAME,
              ISNULL(ZZ.TOTAL_DROP_GAMBLING_TABLE,0) AS TOTAL_DROP_GAMBLING_TABLE,
              ISNULL(ZZ.TOTAL_DROP_CASHIER,0) AS  TOTAL_DROP_CASHIER,
              ISNULL(ZZ.TOTAL_DROP, 0) AS TOTAL_DROP,
              ISNULL(ZZ.TOTAL_WIN, 0) AS TOTAL_WIN,
              ISNULL(ZZ.TOTAL_TIP, 0) AS TOTAL_TIP,
              ISNULL(ZZ.WIN_DROP, 0) AS WIN_DROP,
              ISNULL(ZZ.THEORIC_HOLD, 0) AS THEORIC_HOLD,
              ISNULL(ZZ.TIP_DROP, 0) AS TIP_DROP,
              ISNULL(ZZ.WIN_TIPS, 0) AS WIN_TIPS,
              ZZ.OPEN_HOUR,
              ZZ.CLOSE_HOUR,
              CASE WHEN ZZ.CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED,
              CASE WHEN ZZ.SESSION_SECONDS = 0 THEN DATEDIFF(SECOND,ZZ.OPEN_HOUR,GETDATE()) ELSE ISNULL(ZZ.SESSION_SECONDS, 0) END AS SESSION_SECONDS,
              ISNULL(ZZ.SESSION_SUM, 0) AS SESSION_SUM,
              DATE_TIME

      FROM @_DAYS_AND_TABLES DT

       INNER JOIN #GT_TEMPORARY_REPORT_DATA ZZ
                     ON
                          (
                              (@_TIME_INTERVAL = 0 AND DATE_TIME = ZZ.CM_DATE_ONLY)
                           OR (@_TIME_INTERVAL = 1 AND MONTH(DATE_TIME) = MONTH(ZZ.CM_DATE_ONLY) AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
                           OR (@_TIME_INTERVAL = 2 AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
                          )
                    AND   TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER

       -- SET ORDER
       ORDER BY   DT.TABLE_TYPE_IDENT ASC,
                  CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC,
                  DATE_TIME DESC;

    END -- IF ONLY_ACTIVITY

END
ELSE  -- ELSE WITHOUT INTERVALS
BEGIN

   -- FINAL WITHOUT INTERVALS

      -- FILTER ACTIVITY
   IF @_ONLY_ACTIVITY = 0
    BEGIN
            -- JOIN DATA INCLUDING WITHOUT ACTIVITY
            SELECT   DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER,
                     DT.TABLE_IDENT_NAME AS TABLE_NAME,
                     DT.TABLE_TYPE_IDENT AS TABLE_TYPE,
                     DT.TABLE_TYPE_NAME,
                     ISNULL(ZZ.TOTAL_DROP_GAMBLING_TABLE,0) AS TOTAL_DROP_GAMBLING_TABLE,
                     ISNULL(ZZ.TOTAL_DROP_CASHIER,0) AS  TOTAL_DROP_CASHIER,
                     ISNULL(TOTAL_DROP, 0) AS TOTAL_DROP,
                     ISNULL(TOTAL_WIN, 0) AS TOTAL_WIN,
                     ISNULL(TOTAL_TIP, 0) AS TOTAL_TIP,
                     ISNULL(WIN_DROP, 0) AS WIN_DROP,
                     ISNULL(ZZ.THEORIC_HOLD, 0) AS THEORIC_HOLD,
                     ISNULL(TIP_DROP, 0) AS TIP_DROP,
                     ISNULL(WIN_TIPS, 0) AS WIN_TIPS,
                     OPEN_HOUR,
                     CLOSE_HOUR,
                     CASE WHEN CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED,
                     CASE WHEN SESSION_SECONDS = 0 THEN DATEDIFF(SECOND, OPEN_HOUR, GETDATE()) ELSE ISNULL(SESSION_SECONDS, 0) END AS SESSION_SECONDS,
                   ISNULL(SESSION_SUM, 0) AS SESSION_SUM
              FROM   @_DAYS_AND_TABLES DT

         LEFT JOIN   #GT_TEMPORARY_REPORT_DATA ZZ
                ON   DT.TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER

                          ORDER BY   DT.TABLE_TYPE_IDENT ASC,
                     CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC,
                     DATE_TIME DESC;

     END
    ELSE
     BEGIN
            -- JOIN DATA WITH ONLY ACTIVITY
          SELECT     DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER,
                     DT.TABLE_IDENT_NAME AS TABLE_NAME,
                     DT.TABLE_TYPE_IDENT AS TABLE_TYPE,
                     DT.TABLE_TYPE_NAME,
                     ISNULL(ZZ.TOTAL_DROP_GAMBLING_TABLE,0) AS TOTAL_DROP_GAMBLING_TABLE,
                     ISNULL(ZZ.TOTAL_DROP_CASHIER,0) AS  TOTAL_DROP_CASHIER,
                     ISNULL(TOTAL_DROP, 0) AS TOTAL_DROP,
                     ISNULL(TOTAL_WIN, 0) AS TOTAL_WIN,
                     ISNULL(TOTAL_TIP, 0) AS TOTAL_TIP,
                     ISNULL(WIN_DROP, 0) AS WIN_DROP,
                     ISNULL(ZZ.THEORIC_HOLD, 0) AS THEORIC_HOLD,
                     ISNULL(TIP_DROP, 0) AS TIP_DROP,
                     ISNULL(WIN_TIPS, 0) AS WIN_TIPS,
                     OPEN_HOUR,
                     CLOSE_HOUR,
                     CASE WHEN CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED,
                     CASE WHEN SESSION_SECONDS = 0 THEN DATEDIFF(SECOND, OPEN_HOUR, GETDATE()) ELSE ISNULL(SESSION_SECONDS, 0) END AS SESSION_SECONDS,
                     ISNULL(SESSION_SUM, 0) AS SESSION_SUM
              FROM   @_DAYS_AND_TABLES DT
                     
         INNER JOIN   #GT_TEMPORARY_REPORT_DATA ZZ
                ON   DT.TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER

           ORDER BY   DT.TABLE_TYPE_IDENT ASC,
                      CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC,
                      DATE_TIME DESC;

     END
 END

-- ERASE THE TEMPORARY DATA
DROP TABLE #GT_TEMPORARY_REPORT_DATA

END 
-- END PROCEDURE


GO

-- PERMISSIONS
GRANT EXECUTE ON [dbo].[GT_Base_Report_Data] TO [wggui] WITH GRANT OPTION
GO

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PSAClient' AND GP_SUBJECT_KEY ='AddOperationsOfGamingTables')
  INSERT INTO [dbo].[GENERAL_PARAMS] ([GP_group_key] ,[GP_subject_key] ,[GP_key_value]) VALUES ('PSAClient', 'AddOperationsOfGamingTables', '0');
GO

IF  EXISTS (SELECT * FROM SYS.OBJECTS WHERE OBJECT_ID = OBJECT_ID(N'[DBO].[sp_GetTablesActivity]') AND TYPE IN (N'P', N'PC'))
DROP PROCEDURE [DBO].[sp_GetTablesActivity]
GO

/****** OBJECT:  STOREDPROCEDURE [DBO].[sp_GetTablesActivity]    SCRIPT DATE: 05/17/2016 16:29:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [DBO].[sp_GetTablesActivity]
@pDate AS DATETIME
AS
BEGIN

DECLARE @Count INT
DECLARE @Cols VARCHAR(MAX)
DECLARE @ColsVal VARCHAR(MAX)
DECLARE @ColsValU VARCHAR(MAX)
DECLARE @ColsEnabled VARCHAR(MAX)
DECLARE @ColsUsed VARCHAR(MAX)

SELECT @Count = 0
SELECT @Cols = ''
SELECT @ColsVal = ''
SELECT @ColsValU = ''
SELECT @ColsEnabled = ''
SELECT @ColsUsed = ''

WHILE @Count < DAY(DATEADD(DD,-1,DATEADD(MM,DATEDIFF(MM,-1,@pDate),0)))
BEGIN
   SELECT @Count = @Count + 1
   
   IF @Count <> 1
    BEGIN
		SELECT @Cols = @Cols + ','
		SELECT @ColsVal = @ColsVal + ','
		SELECT @ColsValU = @ColsValU + ','
		SELECT @ColsEnabled = @ColsEnabled + ','
		SELECT @ColsUsed = @ColsUsed + ','
	END
	
   SELECT @Cols = @Cols + '[' + CONVERT(VARCHAR,@Count) + ']'
   SELECT @ColsVal = @ColsVal + ' ISNULL([' + CONVERT(VARCHAR,@Count) + '],0) As D' + CONVERT(VARCHAR,@Count) + ''
   SELECT @ColsValU = @ColsValU + ' ISNULL([' + CONVERT(VARCHAR,@Count) + '],0) As DU' + CONVERT(VARCHAR,@Count) + ''
   SELECT @ColsEnabled = @ColsEnabled + '[D' + CONVERT(VARCHAR,@Count) + ']'
   SELECT @ColsUsed = @ColsUsed + '[DU' + CONVERT(VARCHAR,@Count) + ']'
    
END

DECLARE @Query NVARCHAR(MAX)
SET @Query = '           
			
			DECLARE @DATE AS DATETIME
			SELECT @DATE = ''' + CONVERT(VARCHAR(10),@pDate,112) + '''

			SELECT 
				A.GMC_GAMINGTABLE_ID T_ID,
				A.GT_NAME T_NAME,
				A.GT_TYPE T_TYPE,
				A.GMC_MONTH T_MONTH,
				A.T_DAYS,
				' + @ColsEnabled + ',
				A.T_DAYSUSE,
				' + @ColsUsed + '
			FROM 
			(
				SELECT 
					GMC_GAMINGTABLE_ID , 
					GT_TYPE,
					GT_NAME,
					CASE WHEN T_DAYS > 0 THEN 1 ELSE 0 END GMC_MONTH, 
					T_DAYS,
					T_DAYSUSE,
					' + @ColsVal + '
				FROM 
				(
					SELECT 
						GMC.GMC_GAMINGTABLE_ID, 
						GTT.GTT_NAME GT_TYPE,
						GT.GT_NAME GT_NAME,
						YEAR(CONVERT(DATETIME,CONVERT(VARCHAR(8),GMC.GMC_GAMING_DAY))) AS GMC_YEAR, 
						MONTH(CONVERT(DATETIME,CONVERT(VARCHAR(8),GMC.GMC_GAMING_DAY))) AS GMC_MONTH, 
						DAY(CONVERT(DATETIME,CONVERT(VARCHAR(8),GMC.GMC_GAMING_DAY))) AS GMN_DAY, 
						CONVERT(INT,GMC.GMC_ENABLED) GMC_ENABLED, 
						(SELECT COUNT(*) FROM GAMING_TABLES_CONNECTED AS CO 
						  WHERE CO.GMC_ENABLED = 1 
							AND YEAR(CONVERT(DATETIME,CONVERT(VARCHAR(8),CO.GMC_GAMING_DAY))) = YEAR(@DATE) 
							AND MONTH(CONVERT(DATETIME,CONVERT(VARCHAR(8),CO.GMC_GAMING_DAY))) = MONTH(@DATE) 
							AND CO.GMC_GAMINGTABLE_ID = GMC.GMC_GAMINGTABLE_ID ) AS T_DAYS,
						(SELECT COUNT(*) FROM GAMING_TABLES_CONNECTED AS CO 
						  WHERE CO.GMC_USED  = 1 
							AND YEAR(CONVERT(DATETIME,CONVERT(VARCHAR(8),CO.GMC_GAMING_DAY))) = YEAR(@DATE) 
							AND MONTH(CONVERT(DATETIME,CONVERT(VARCHAR(8),CO.GMC_GAMING_DAY))) = MONTH(@DATE) 
							AND CO.GMC_GAMINGTABLE_ID = GMC.GMC_GAMINGTABLE_ID ) AS T_DAYSUSE
						FROM GAMING_TABLES_CONNECTED AS GMC
							INNER JOIN GAMING_TABLES GT
								ON GT.GT_GAMING_TABLE_ID = GMC.GMC_GAMINGTABLE_ID
							INNER JOIN GAMING_TABLES_TYPES GTT
								ON GTT.GTT_GAMING_TABLE_TYPE_ID = GT.GT_TYPE_ID
						WHERE  YEAR(CONVERT(DATETIME,CONVERT(VARCHAR(8),GMC.GMC_GAMING_DAY))) = YEAR(@DATE)
							AND MONTH(CONVERT(DATETIME,CONVERT(VARCHAR(8),GMC.GMC_GAMING_DAY))) = MONTH(@DATE) 
						GROUP BY GMC.GMC_GAMINGTABLE_ID, GTT.GTT_NAME, GT.GT_NAME, YEAR(CONVERT(DATETIME,CONVERT(VARCHAR(8),GMC.GMC_GAMING_DAY))), MONTH(CONVERT(DATETIME,CONVERT(VARCHAR(8),GMC.GMC_GAMING_DAY))), DAY(CONVERT(DATETIME,CONVERT(VARCHAR(8),GMC.GMC_GAMING_DAY)))
								,GMC.GMC_ENABLED, GMC.GMC_USED
				) AS SourceTable
				PIVOT
				(
					MAX(GMC_ENABLED) FOR GMN_DAY IN (' + @Cols + ')
				) AS PivotTable
			) A 
			INNER JOIN
			( 
				SELECT 
						GMC_GAMINGTABLE_ID , 
						GT_TYPE,
						GT_NAME,
						CASE WHEN T_DAYS > 0 THEN 1 ELSE 0 END GMC_MONTH, 
						T_DAYS,
						T_DAYSUSE,
						' + @ColsValU + '
					FROM 
					(
						SELECT 
							GMC.GMC_GAMINGTABLE_ID, 
							GTT.GTT_NAME GT_TYPE,
							GT.GT_NAME GT_NAME,
							YEAR(CONVERT(DATETIME,CONVERT(VARCHAR(8),GMC.GMC_GAMING_DAY))) AS GMC_YEAR, 
							MONTH(CONVERT(DATETIME,CONVERT(VARCHAR(8),GMC.GMC_GAMING_DAY))) AS GMC_MONTH, 
							DAY(CONVERT(DATETIME,CONVERT(VARCHAR(8),GMC.GMC_GAMING_DAY))) AS GMN_DAY, 
							CONVERT(INT,GMC.GMC_ENABLED) GMC_ENABLED, 
							(SELECT COUNT(*) FROM GAMING_TABLES_CONNECTED AS CO 
							  WHERE CO.GMC_ENABLED = 1 
								AND YEAR(CONVERT(DATETIME,CONVERT(VARCHAR(8),CO.GMC_GAMING_DAY))) = YEAR(@DATE) 
								AND MONTH(CONVERT(DATETIME,CONVERT(VARCHAR(8),CO.GMC_GAMING_DAY))) = MONTH(@DATE) 
								AND CO.GMC_GAMINGTABLE_ID = GMC.GMC_GAMINGTABLE_ID ) AS T_DAYS,
							CONVERT(INT,GMC.GMC_USED) GMC_USED,
							(SELECT COUNT(*) FROM GAMING_TABLES_CONNECTED AS CO 
							  WHERE CO.GMC_USED  = 1 
								AND YEAR(CONVERT(DATETIME,CONVERT(VARCHAR(8),CO.GMC_GAMING_DAY))) = YEAR(@DATE) 
								AND MONTH(CONVERT(DATETIME,CONVERT(VARCHAR(8),CO.GMC_GAMING_DAY))) = MONTH(@DATE) 
								AND CO.GMC_GAMINGTABLE_ID = GMC.GMC_GAMINGTABLE_ID ) AS T_DAYSUSE
							FROM GAMING_TABLES_CONNECTED AS GMC
								INNER JOIN GAMING_TABLES GT
									ON GT.GT_GAMING_TABLE_ID = GMC.GMC_GAMINGTABLE_ID
								INNER JOIN GAMING_TABLES_TYPES GTT
									ON GTT.GTT_GAMING_TABLE_TYPE_ID = GT.GT_TYPE_ID
							WHERE  YEAR(CONVERT(DATETIME,CONVERT(VARCHAR(8),GMC.GMC_GAMING_DAY))) = YEAR(@DATE)
								AND MONTH(CONVERT(DATETIME,CONVERT(VARCHAR(8),GMC.GMC_GAMING_DAY))) = MONTH(@DATE) 
							GROUP BY GMC.GMC_GAMINGTABLE_ID, GTT.GTT_NAME, GT.GT_NAME, YEAR(CONVERT(DATETIME,CONVERT(VARCHAR(8),GMC.GMC_GAMING_DAY))), MONTH(CONVERT(DATETIME,CONVERT(VARCHAR(8),GMC.GMC_GAMING_DAY))), DAY(CONVERT(DATETIME,CONVERT(VARCHAR(8),GMC.GMC_GAMING_DAY)))
									,GMC.GMC_ENABLED, GMC.GMC_USED
					) AS SourceTable
					PIVOT
					(
						MAX(GMC_USED) FOR GMN_DAY IN (' + @Cols + ')
					) AS PivotTable
			) B
			ON B.gmc_gamingtable_id = A.gmc_gamingtable_id
			AND B.GMC_MONTH = A.GMC_MONTH

            '     

EXEC SP_EXECUTESQL @Query

END

GO

GRANT EXECUTE ON [sp_GetTablesActivity] TO [WGGUI] WITH GRANT OPTION
GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GenerateTablesActivity]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[sp_GenerateTablesActivity]
GO

CREATE PROCEDURE [dbo].[sp_GenerateTablesActivity]
AS
BEGIN

	DECLARE @CurrentDay AS DATETIME
	SELECT @CurrentDay = [dbo].[TodayOpening] (1)

	SET NOCOUNT ON;


	-- Insert all gaming tables to terminals connected with default values to 0
	INSERT INTO   GAMING_TABLES_CONNECTED
		   SELECT   GT_GAMING_TABLE_ID, CONVERT(INT,CONVERT(VARCHAR(8), @CurrentDay, 112)), 0, 0
		     FROM   GAMING_TABLES
	  LEFT JOIN   GAMING_TABLES_CONNECTED ON GMC_GAMINGTABLE_ID = GT_GAMING_TABLE_ID AND GMC_GAMING_DAY = CONVERT(INT,CONVERT(VARCHAR(8), @CurrentDay, 112))
		    WHERE   GMC_GAMING_DAY IS NULL

    -- Update Enabled/Used
	UPDATE   GAMING_TABLES_CONNECTED
	   SET   GMC_ENABLED = (CASE WHEN S.S_ENABLED = 1 OR GMC_ENABLED = 1 THEN 1 ELSE 0 END)
		     , GMC_USED = CASE WHEN S.S_USED = 1 OR GMC_USED = 1 THEN 1 ELSE 0 END
	  FROM 
			(
				  SELECT   GT_GAMING_TABLE_ID AS S_GAMING_TABLE_ID
					       , CONVERT(INT,CONVERT(VARCHAR(8), @CurrentDay, 112)) AS S_GAMING_DAY
					       , GT_ENABLED AS S_ENABLED
					       , CASE WHEN MIN(GTS.GTS_CASHIER_SESSION_ID) > 0 THEN 1 ELSE 0 END AS S_USED
				    FROM   GAMING_TABLES
			INNER JOIN   GAMING_TABLES_CONNECTED ON GMC_GAMINGTABLE_ID = GT_GAMING_TABLE_ID AND GMC_GAMING_DAY = CONVERT(INT,CONVERT(VARCHAR(8), @CurrentDay, 112))
			 LEFT JOIN   GAMING_TABLES_SESSIONS GTS ON GMC_GAMINGTABLE_ID = GTS_GAMING_TABLE_ID  AND GMC_GAMING_DAY = CONVERT(INT,CONVERT(VARCHAR(8), @CurrentDay, 112))
			 LEFT JOIN   CASHIER_SESSIONS CS ON CS.CS_SESSION_ID = GTS.GTS_CASHIER_SESSION_ID
						 AND (
							   (@CurrentDay >= CONVERT(DATETIME,CS.CS_OPENING_DATE) AND @CurrentDay <= CONVERT(DATETIME,CS.CS_CLOSING_DATE)) 
							OR (@CurrentDay >= CONVERT(DATETIME,CS.CS_OPENING_DATE) AND CONVERT(DATETIME,CS.CS_CLOSING_DATE) IS NULL)
							OR (CONVERT(DATETIME,CS.CS_OPENING_DATE) > @CurrentDay AND CONVERT(DATETIME,CS.CS_OPENING_DATE) <= DATEADD(D,1,@CurrentDay))
							OR (CONVERT(DATETIME,CS.CS_CLOSING_DATE) > @CurrentDay AND CONVERT(DATETIME,CS.CS_CLOSING_DATE) <= DATEADD(D,1,@CurrentDay))
							 )
			  GROUP BY   GT_GAMING_TABLE_ID, GT_ENABLED  
			)S
	 WHERE   S.S_GAMING_TABLE_ID = GAMING_TABLES_CONNECTED.GMC_GAMINGTABLE_ID AND GMC_GAMING_DAY = S_GAMING_DAY

END

GO

GRANT EXECUTE ON [sp_GenerateTablesActivity] TO [WGGUI] WITH GRANT OPTION
GO



IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GenerateTablesActivity]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[sp_GenerateTablesActivity]
GO

CREATE PROCEDURE [dbo].[sp_GenerateTablesActivity]
AS
BEGIN

      DECLARE @CurrentDay AS DATETIME
      SELECT @CurrentDay = [dbo].[TodayOpening] (1)

      SET NOCOUNT ON;


      -- Insert all gaming tables to terminals connected with default values to 0
      INSERT INTO   GAMING_TABLES_CONNECTED
            SELECT   GT_GAMING_TABLE_ID, CONVERT(INT,CONVERT(VARCHAR(8), @CurrentDay, 112)), 0, 0
               FROM   GAMING_TABLES
        LEFT JOIN   GAMING_TABLES_CONNECTED ON GMC_GAMINGTABLE_ID = GT_GAMING_TABLE_ID AND GMC_GAMING_DAY = CONVERT(INT,CONVERT(VARCHAR(8), @CurrentDay, 112))
              WHERE   GMC_GAMING_DAY IS NULL

    -- Update Enabled
       UPDATE   GAMING_TABLES_CONNECTED
              SET   GMC_ENABLED = (CASE WHEN S.S_ENABLED = 1 OR GMC_ENABLED = 1 THEN 1 ELSE 0 END)
             FROM 
                  (
                          SELECT   GT_GAMING_TABLE_ID AS S_GAMING_TABLE_ID
                                    , CONVERT(INT,CONVERT(VARCHAR(8), @CurrentDay, 112)) AS S_GAMING_DAY
                                    , GT_ENABLED AS S_ENABLED
                            FROM   GAMING_TABLES
                    INNER JOIN   GAMING_TABLES_CONNECTED ON GMC_GAMINGTABLE_ID = GT_GAMING_TABLE_ID AND GMC_GAMING_DAY = CONVERT(INT,CONVERT(VARCHAR(8), @CurrentDay, 112))                  
                      GROUP BY   GT_GAMING_TABLE_ID, GT_ENABLED  
                  )S
            WHERE   S.S_GAMING_TABLE_ID = GAMING_TABLES_CONNECTED.GMC_GAMINGTABLE_ID AND GMC_GAMING_DAY = S_GAMING_DAY
    
    -- Update Used    
             UPDATE   GAMING_TABLES_CONNECTED
                SET   GMC_USED = CASE WHEN S.S_USED = 1 OR GMC_USED = 1 THEN 1 ELSE 0 END
               FROM 
                  (
                          SELECT   GT_GAMING_TABLE_ID AS S_GAMING_TABLE_ID
                                    , CONVERT(INT,CONVERT(VARCHAR(8), @CurrentDay, 112)) AS S_GAMING_DAY
                                    , CASE WHEN MAX(GTS.GTS_CASHIER_SESSION_ID) > 0 THEN 1 ELSE 0 END AS S_USED
                            FROM   GAMING_TABLES
                  INNER JOIN   GAMING_TABLES_CONNECTED ON GMC_GAMINGTABLE_ID = GT_GAMING_TABLE_ID AND GMC_GAMING_DAY = CONVERT(INT,CONVERT(VARCHAR(8), @CurrentDay, 112))
                  LEFT JOIN   GAMING_TABLES_SESSIONS GTS ON GMC_GAMINGTABLE_ID = GTS_GAMING_TABLE_ID  AND GMC_GAMING_DAY = CONVERT(INT,CONVERT(VARCHAR(8), @CurrentDay, 112))
                  INNER JOIN   CASHIER_SESSIONS CS ON CS.CS_SESSION_ID = GTS.GTS_CASHIER_SESSION_ID
                                   AND (
                                            (@CurrentDay >= CONVERT(DATETIME,CS.CS_OPENING_DATE) AND @CurrentDay <= CONVERT(DATETIME,CS.CS_CLOSING_DATE)) 
                                         OR (@CurrentDay >= CONVERT(DATETIME,CS.CS_OPENING_DATE) AND CONVERT(DATETIME,CS.CS_CLOSING_DATE) IS NULL)
                                         OR (CONVERT(DATETIME,CS.CS_OPENING_DATE) > @CurrentDay AND CONVERT(DATETIME,CS.CS_OPENING_DATE) <= DATEADD(D,1,@CurrentDay))
                                         OR (CONVERT(DATETIME,CS.CS_CLOSING_DATE) > @CurrentDay AND CONVERT(DATETIME,CS.CS_CLOSING_DATE) <= DATEADD(D,1,@CurrentDay))
                                         )
                    GROUP BY   GT_GAMING_TABLE_ID, GT_ENABLED  
                  )S
            WHERE   S.S_GAMING_TABLE_ID = GAMING_TABLES_CONNECTED.GMC_GAMINGTABLE_ID AND GMC_GAMING_DAY = S_GAMING_DAY

END

GO

GRANT EXECUTE ON [sp_GenerateTablesActivity] TO [WGGUI] WITH GRANT OPTION
GO


IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'GamingTables' AND GP_SUBJECT_KEY = 'Ticket.ManualConciliation')
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE)  VALUES ('GamingTables', 'Ticket.ManualConciliation', '0')
GO

/****** Object:  StoredProcedure [dbo].[SP_LastPlaySession]    Script Date: 20-09-2017 7:03:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_LastPlaySession]') AND type in (N'P', N'PC'))
   DROP PROCEDURE [dbo].[SP_LastPlaySession]
GO

/****** Object:  StoredProcedure [dbo].[SP_LastPlaySession]    Script Date: 20-09-2017 7:03:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Andreu Julia
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[SP_LastPlaySession] 
	@pTerminalId int, 
	@pOpened bit
AS
BEGIN
declare @_play_session_id as bigint


IF @pOpened = 1
    SELECT @_play_session_id = MAX(PS_PLAY_SESSION_ID) 
	  FROM PLAY_SESSIONS WITH (INDEX (IX_ps_status))      
	 WHERE PS_STATUS = 0 
	   AND ps_terminal_id = @pTerminalId
ELSE
    SELECT @_play_session_id = MAX(PS_PLAY_SESSION_ID) 
	  FROM PLAY_SESSIONS WITH (INDEX (IX_ps_terminal_id)) 
	 WHERE ps_terminal_id = @pTerminalId 
	   AND PS_STATUS != 0
	
SET @_play_session_id = ISNULL(@_play_session_id, 0)

SELECT   PS_PLAY_SESSION_ID
       , PS_WCP_TRANSACTION_ID  
	   , PS_TYPE  
	   , PS_STAND_ALONE  
	   , PS_INITIAL_BALANCE  
	   , PS_FINAL_BALANCE  
	   , TE_NAME  
	   , PS_ACCOUNT_ID  
	   , AC_TRACK_DATA  
	   , CASE WHEN ( PS_CASH_IN > 0 ) THEN 1 ELSE 0 END AS  HAS_CASHIN_AMOUNT 
	   , ISNULL (PS_CANCELLABLE_AMOUNT, 0)					PS_CANCELLABLE_AMOUNT  
	   , PS_INITIAL_BALANCE + PS_CASH_IN - PS_PLAYED_AMOUNT + PS_WON_AMOUNT    
  FROM   PLAY_SESSIONS  
 INNER JOIN TERMINALS  ON  PS_TERMINAL_ID = TE_TERMINAL_ID
 INNER JOIN ACCOUNTS   ON  PS_ACCOUNT_ID  = AC_ACCOUNT_ID
 WHERE  PS_PLAY_SESSION_ID = @_play_session_id 

END

GO





/****** Object:  Index [IX_ps_terminal_id]    Script Date: 20-09-2017 7:09:30 ******/
DROP INDEX [IX_ps_terminal_id] ON [dbo].[play_sessions]
GO

/****** Object:  Index [IX_ps_terminal_id]    Script Date: 20-09-2017 7:09:30 ******/
CREATE NONCLUSTERED INDEX [IX_ps_terminal_id] ON [dbo].[play_sessions]
(
	[ps_terminal_id] ASC,
	[ps_status] ASC,
	[ps_stand_alone] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CageGetGlobalReportData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CageGetGlobalReportData]
GO

CREATE PROCEDURE [dbo].[CageGetGlobalReportData]
        @pCageSessionIds NVARCHAR(MAX) = NULL
AS
BEGIN
  
  DECLARE @NationalIsoCode NVARCHAR(3)
  DECLARE @CageOperationType_FromTerminal INT
  DECLARE @TicketsId INT
  DECLARE @BillsId INT
  DECLARE @Currencies AS NVARCHAR(200)
  DECLARE @CountSessions INT
  DECLARE @List VARCHAR(MAX)
  DECLARE @StockIsStored BIT
  DECLARE @CageCurrencyType_ColorChips INT

  SELECT @NationalIsoCode = GP_KEY_VALUE 
    FROM GENERAL_PARAMS 
   WHERE GP_GROUP_KEY = 'RegionalOptions' 
     AND GP_SUBJECT_KEY = 'CurrencyISOCode'

  SELECT @Currencies = GP_KEY_VALUE 
    FROM GENERAL_PARAMS 
   WHERE GP_GROUP_KEY = 'RegionalOptions' 
     AND GP_SUBJECT_KEY = 'CurrenciesAccepted'

  -- Split currencies ISO codes
  SELECT   CURRENCY_ISO_CODE INTO #TMP_CURRENCIES_ISO_CODES 
    FROM ( SELECT   SST_VALUE AS CURRENCY_ISO_CODE FROM [SplitStringIntoTable] (@Currencies, ';', 1)
            UNION ALL 
           SELECT   'X01' AS CURRENCY_ISO_CODE    -- for retrocompatibility
            UNION ALL 
           SELECT   'X02' AS CURRENCY_ISO_CODE ) AS CURRENCIES
               
  SET @CageOperationType_FromTerminal = 104
  SET @TicketsId = -200
  SET @BillsId = 0
  SET @CageCurrencyType_ColorChips = 1003
   
  -- Session IDs Split
  SELECT SST_VALUE AS CAGE_SESSION_ID INTO #TMP_CAGE_SESSIONS FROM [SplitStringIntoTable] (@pCageSessionIds,','  , 1)
  SET @CountSessions = (SELECT COUNT(*) FROM #TMP_CAGE_SESSIONS )
  
  IF @CountSessions = 1 
    SELECT @List = CGS_CAGE_STOCK FROM CAGE_SESSIONS 
     WHERE CGS_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)

  -- TABLE[0]: Get cage stock 
  IF (@CountSessions = 1 AND @List IS NOT NULL) 
  BEGIN
    --Get stock on close session
    SET @StockIsStored =  1
    SELECT SST_VALUE AS STOCK_ROW INTO #TMP_STOCK_ROWS FROM [SplitStringIntoTable] (@List, '|', 1)
    
    -- LTC 2017-JUN-20
    UPDATE #TMP_STOCK_ROWS 
    SET STOCK_ROW = CASE WHEN (LEN(STOCK_ROW) - LEN(REPLACE(STOCK_ROW, ';',''))) = 3 THEN STOCK_ROW + ';' + '-1' ELSE STOCK_ROW END
    FROM #TMP_STOCK_ROWS

    SELECT left(STOCK_ROW, 3) AS CGS_ISO_CODE,                        
       CAST(REPLACE(PARSENAME(REPLACE(REPLACE(RIGHT(STOCK_ROW, LEN(STOCK_ROW)-4),'.', '|'), ';', '.'), 4), '|', '.') AS MONEY) AS CGS_DENOMINATION,   
       CAST(REPLACE(PARSENAME(REPLACE(REPLACE(RIGHT(STOCK_ROW, LEN(STOCK_ROW)-4), '.', '|'), ';', '.'), 3), '|', '.') AS DECIMAL(15,2)) AS CGS_QUANTITY,       
       CAST(REPLACE(PARSENAME(REPLACE(REPLACE(RIGHT(STOCK_ROW, LEN(STOCK_ROW)-4), '.', '|'), ';', '.'), 2), '|', '.') AS INT) AS CGS_CAGE_CURRENCY_TYPE,  
       CAST(REPLACE(PARSENAME(REPLACE(REPLACE(RIGHT(STOCK_ROW, LEN(STOCK_ROW)-4), '.', '|'), ';', '.'), 1), '|', '.') AS INT) AS CGS_CHIP_ID, 
       CH_NAME AS NAME, CH_DRAWING AS DRAWING
       INTO #TMP_STOCK                                                                                                               
    FROM #TMP_STOCK_ROWS 
    LEFT JOIN chips ON ch_chip_id = CAST(REPLACE(PARSENAME(REPLACE(REPLACE(RIGHT(STOCK_ROW, LEN(STOCK_ROW)-4), '.', '|'), ';', '.'), 1), '|', '.') AS INT)

    SELECT   A.*
           , CE_CURRENCY_ORDER 
      FROM   #TMP_STOCK AS A
      LEFT   JOIN CURRENCY_EXCHANGE ON CGS_ISO_CODE = CE_CURRENCY_ISO_CODE AND CE_TYPE = 0
  ORDER BY   CE_CURRENCY_ORDER
           , CGS_ISO_CODE
           , CASE WHEN CGS_CAGE_CURRENCY_TYPE < 1000 THEN 0
                  WHEN CGS_CAGE_CURRENCY_TYPE = 1003 THEN 9999999
                  ELSE 1 END
           , CGS_DENOMINATION DESC
           , CASE WHEN CGS_DENOMINATION >= 0 THEN CGS_DENOMINATION 
                  ELSE CGS_DENOMINATION * (-100000) END                                                                    

    DROP TABLE #TMP_STOCK_ROWS                                                                                                      
    DROP TABLE #TMP_STOCK     
    
  END --IF (@CountSessions = 1 AND @List IS NOT NULL)    
  ELSE --IF (@CountSessions = 1 AND @List IS NOT NULL) 
  BEGIN
    -- Get current stock
    SET @StockIsStored =  0
    EXEC CageCurrentStock
  END --ELSE
  
  IF @pCageSessionIds IS NOT NULL
  BEGIN
      
    -- TABLE[1]: Get cage sessions information
    SELECT  a.*
          , (ROW_NUMBER() OVER(ORDER BY CM_SOURCE_TARGET_ID
          , CASE CM_CONCEPT_ID WHEN 0 THEN '000' ELSE CC_DESCRIPTION END
          , CASE WHEN CAGE_CURRENCY_TYPE < 1000 THEN 0
                 WHEN CAGE_CURRENCY_TYPE = 1003 THEN 9999999
                 ELSE 1 END
          , CE_CURRENCY_ORDER
          , CM_ISO_CODE
          , CAGE_CURRENCY_TYPE)) as ORDEN
    FROM (
    SELECT CSM.CSM_SOURCE_TARGET_ID AS CM_SOURCE_TARGET_ID
         , CSM.CSM_CONCEPT_ID AS CM_CONCEPT_ID
         , CASE CSM.CSM_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CSM.CSM_ISO_CODE END AS CM_ISO_CODE
         , CASE CSM.CSM_ISO_CODE WHEN 'X01' THEN 1001 ELSE CASE WHEN CSM_CAGE_CURRENCY_TYPE < 1000 THEN 0 ELSE CSM_CAGE_CURRENCY_TYPE END END AS CAGE_CURRENCY_TYPE
         , SUM(CSM.CSM_VALUE_IN) AS CM_VALUE_IN
         , SUM(CSM.CSM_VALUE_OUT) AS CM_VALUE_OUT
         , CST.CST_SOURCE_TARGET_NAME
         , CC.CC_DESCRIPTION
      FROM CAGE_SESSION_METERS AS CSM
           INNER JOIN CAGE_SOURCE_TARGET_CONCEPTS AS CSTC ON CSTC.CSTC_CONCEPT_ID = CSM.CSM_CONCEPT_ID AND CSTC.CSTC_SOURCE_TARGET_ID = CSM.CSM_SOURCE_TARGET_ID
           INNER JOIN CAGE_CONCEPTS AS CC ON CC.CC_CONCEPT_ID = CSTC.CSTC_CONCEPT_ID
           INNER JOIN CAGE_SOURCE_TARGET AS CST ON CST.CST_SOURCE_TARGET_ID = CSTC.CSTC_SOURCE_TARGET_ID
     WHERE CSM.CSM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
       AND CC.CC_SHOW_IN_REPORT = 1
       AND CC.CC_ENABLED = 1
       AND (CC.CC_TYPE = 1 OR CC_CONCEPT_ID IN (9, 10,11))
       AND CSTC.CSTC_ENABLED = 1
       AND CSM_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)
  GROUP BY CSM.CSM_SOURCE_TARGET_ID
         , CSM.CSM_CONCEPT_ID
         , CASE CSM.CSM_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CSM.CSM_ISO_CODE END 
         , CASE CSM.CSM_ISO_CODE WHEN 'X01' THEN 1001 ELSE CASE WHEN CSM_CAGE_CURRENCY_TYPE < 1000 THEN 0 ELSE CSM_CAGE_CURRENCY_TYPE END END  
         , CST.CST_SOURCE_TARGET_NAME
         , CC.CC_DESCRIPTION
         ) as a
    LEFT  JOIN CURRENCY_EXCHANGE ON CM_ISO_CODE = CE_CURRENCY_ISO_CODE AND CE_TYPE = 0
  ORDER BY CM_SOURCE_TARGET_ID
         , CASE CM_CONCEPT_ID WHEN 0 THEN '000' ELSE CC_DESCRIPTION END
         , CASE WHEN CAGE_CURRENCY_TYPE < 1000 THEN 0
                WHEN CAGE_CURRENCY_TYPE = 1003 THEN 9999999
                ELSE 1 END
         , CE_CURRENCY_ORDER
         , CM_ISO_CODE
         , CAGE_CURRENCY_TYPE 
         
     -- TABLE[2]: Get dynamic liabilities
    SELECT CC.CC_CONCEPT_ID AS LIABILITY_CONCEPT_ID
         , CASE CC_CONCEPT_ID WHEN 0 THEN '000' ELSE CC_DESCRIPTION END AS LIABILITY_NAME 
         , CASE CSM_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CSM_ISO_CODE END AS LIABILITY_ISO_CODE
         , CASE WHEN CASE CSM_ISO_CODE WHEN 'X01' THEN 1001 ELSE CSM_CAGE_CURRENCY_TYPE END < 1000 THEN 0
                ELSE CSM_CAGE_CURRENCY_TYPE END   AS LIABILITY_CAGE_CURRENCY_TYPE
         , SUM(CSM.CSM_VALUE) AS LIABILITY_VALUE
      FROM CAGE_SESSION_METERS AS CSM
     INNER JOIN CAGE_CONCEPTS AS CC ON CC.CC_CONCEPT_ID = CSM.CSM_CONCEPT_ID
      LEFT JOIN CURRENCY_EXCHANGE ON CSM_ISO_CODE = CE_CURRENCY_ISO_CODE AND CE_TYPE = 0
     WHERE CC.CC_ENABLED = 1
       AND CC.CC_SHOW_IN_REPORT = 1
       AND CC.CC_IS_PROVISION = 1
       AND CSM.CSM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
       AND CSM_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)
  GROUP BY CC.CC_CONCEPT_ID  
         , CASE CC_CONCEPT_ID WHEN 0 THEN '000' ELSE CC_DESCRIPTION END
         , CASE WHEN CASE CSM_ISO_CODE WHEN 'X01' THEN 1001 ELSE CSM_CAGE_CURRENCY_TYPE END < 1000 THEN 0
                ELSE CSM_CAGE_CURRENCY_TYPE END
         , CE_CURRENCY_ORDER
         , CASE CSM_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CSM_ISO_CODE END
  ORDER BY CC.CC_CONCEPT_ID  
         , CASE CC_CONCEPT_ID WHEN 0 THEN '000' ELSE CC_DESCRIPTION END
         , CASE WHEN CASE CSM_ISO_CODE WHEN 'X01' THEN 1001 ELSE CSM_CAGE_CURRENCY_TYPE END < 1000 THEN 0
                ELSE CSM_CAGE_CURRENCY_TYPE END
         , CE_CURRENCY_ORDER
         , CASE CSM_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CSM_ISO_CODE END
      
    -- TEMP TABLE[3]: Get session sumary
    SELECT CMD_ISO_CODE AS ISO_CODE
         , CASE WHEN CAGE_CURRENCY_TYPE = 99 THEN 0 ELSE CAGE_CURRENCY_TYPE END AS CAGE_CURRENCY_TYPE
         , ISNULL(ORIGIN, 0) AS ORIGIN
         , SUM(CMD_DEPOSITS) AS DEPOSITS
         , SUM(CMD_WITHDRAWALS) AS WITHDRAWALS
		 INTO #TABLE_TEMP_3
      FROM (SELECT CASE CMD_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CMD_ISO_CODE END AS CMD_ISO_CODE
                 , CASE WHEN CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE CAGE_CURRENCY_TYPE END < 1000 THEN 0
                        ELSE CAGE_CURRENCY_TYPE END AS CAGE_CURRENCY_TYPE
                 , ORIGIN
                 , SUM(CMD_DEPOSITS) CMD_DEPOSITS
                 , SUM(CMD_WITHDRAWALS) CMD_WITHDRAWALS      
              FROM (SELECT CASE WHEN CMD_QUANTITY >= 0 THEN @BillsId
                                ELSE CMD_QUANTITY END  AS ORIGIN
                         , CASE CMD_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CMD_ISO_CODE END AS CMD_ISO_CODE
                         , CASE WHEN CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE CMD_CAGE_CURRENCY_TYPE END < 1000 THEN 0
                                ELSE CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE ISNULL(CMD_CAGE_CURRENCY_TYPE, 0) END END  AS CAGE_CURRENCY_TYPE
                         , CMD_DENOMINATION
                         , CASE WHEN CGM_TYPE IN (100, 101, 103, 104) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY < 0 AND CMD_QUANTITY <> @TicketsId THEN CMD_DENOMINATION
                                                                                            WHEN CMD_QUANTITY = @TicketsId THEN 0
                                                                                            WHEN CMD_CAGE_CURRENCY_TYPE = @CageCurrencyType_ColorChips THEN CMD_QUANTITY
                                                                                       ELSE CMD_QUANTITY * CMD_DENOMINATION END), 0)
                                ELSE 0 END AS CMD_DEPOSITS
                         , CASE WHEN CGM_TYPE IN (0, 1, 4, 5) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY < 0 AND CMD_QUANTITY <> @TicketsId THEN CMD_DENOMINATION
                                                                                    WHEN CMD_QUANTITY = @TicketsId THEN 0
                                                                                    WHEN CMD_CAGE_CURRENCY_TYPE = @CageCurrencyType_ColorChips THEN CMD_QUANTITY
                                                                               ELSE CMD_QUANTITY * CMD_DENOMINATION END), 0)
                                ELSE 0 END AS CMD_WITHDRAWALS
                      FROM CAGE_MOVEMENTS 
                INNER JOIN CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          
                     WHERE CGM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
                       AND (CGM_STATUS IN (2, 3, 4, 8, 9, 10) 
                            OR (CGM_STATUS = 0 AND CGM_TYPE = 0))
                  GROUP BY CASE WHEN CMD_QUANTITY >= 0 THEN @BillsId
                                ELSE CMD_QUANTITY END  
                         , CASE CMD_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CMD_ISO_CODE END 
                         , CASE WHEN CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE CMD_CAGE_CURRENCY_TYPE END < 1000 THEN 0
                                ELSE CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE ISNULL(CMD_CAGE_CURRENCY_TYPE, 0) END END 
                         , CMD_DENOMINATION
                         , CGM_TYPE 
                  -- Tickets 
                  UNION ALL
                  
                    SELECT @TicketsId AS ORIGIN
                         , ISNULL(TI_CUR0, @NationalIsoCode) AS CMD_ISO_CODE 
                         , ISNULL(CMD_CAGE_CURRENCY_TYPE, 0) AS CAGE_CURRENCY_TYPE                                    
                         , 1 AS CMD_DENOMINATION
                         , CASE WHEN CGM_TYPE IN (100, 101, 103, 104) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY = @TicketsId THEN 
                                                                                                 CASE WHEN TI_AMT0 IS NULL THEN TI_AMOUNT 
                                                                                                      ELSE TI_AMT0 END
                                                                                       ELSE 0 END), 0)
                                ELSE 0 END AS CMD_DEPOSITS
                         , CASE WHEN CGM_TYPE IN (0, 1, 4, 5) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY = @TicketsId THEN 
                                                                                         CASE WHEN TI_AMT0 IS NULL THEN TI_AMOUNT 
                                                                                              ELSE TI_AMT0 END
                                                                                    ELSE 0 END), 0)
                                ELSE 0 END AS CMD_WITHDRAWALS
                      FROM CAGE_MOVEMENTS 
                           INNER JOIN   CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          
                           INNER JOIN   TICKETS ON TICKETS.TI_COLLECTED_MONEY_COLLECTION = CAGE_MOVEMENTS.CGM_MC_COLLECTION_ID     
                     WHERE  CGM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
                  AND (CGM_STATUS IN (2, 3, 4, 8, 9, 10) 
                            OR (CGM_STATUS = 0 AND CGM_TYPE = 0)) AND CMD_QUANTITY = @TicketsId  
              AND CAGE_MOVEMENTS.CGM_TYPE <> @CageOperationType_FromTerminal
                     GROUP BY ISNULL(TI_CUR0, @NationalIsoCode) 
                         , ISNULL(CMD_CAGE_CURRENCY_TYPE, 0)                                 
                         , CGM_TYPE
                   ) AS T1        
          GROUP BY ORIGIN
                  ,CASE CMD_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CMD_ISO_CODE END 
                 , CASE WHEN CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE CAGE_CURRENCY_TYPE END < 1000 THEN 0
                        ELSE CAGE_CURRENCY_TYPE END 
                 

            -- GET Terminal value-in amounts
              
              UNION ALL 
              
              SELECT CASE TI_CUR0 WHEN 'X01' THEN @NationalIsoCode ELSE ISNULL(TI_CUR0, @NationalIsoCode) END AS CMD_ISO_CODE
                    , 0 AS CAGE_CURRENCY_TYPE
                    , @TicketsId AS ORIGIN
                    , ISNULL(SUM (CASE WHEN TI_AMT0 IS NULL THEN TI_AMOUNT 
                                        ELSE TI_AMT0 END),0)  AS DEPOSITS
                    , 0 AS WITHDRAWS
              FROM MONEY_COLLECTIONS
                  INNER JOIN CAGE_MOVEMENTS ON CGM_MC_COLLECTION_ID = MC_COLLECTION_ID 
          INNER JOIN TICKETS ON TICKETS.TI_COLLECTED_MONEY_COLLECTION = CAGE_MOVEMENTS.CGM_MC_COLLECTION_ID
             WHERE CAGE_MOVEMENTS.CGM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
               AND CAGE_MOVEMENTS.CGM_TYPE = @CAGEOPERATIONTYPE_FROMTERMINAL
               AND (CGM_STATUS IN (2, 3, 4, 8, 9, 10) 
                    OR (CGM_STATUS = 0 AND CGM_TYPE = 0))           
             GROUP BY CASE TI_CUR0 WHEN 'X01' THEN @NationalIsoCode ELSE ISNULL(TI_CUR0, @NationalIsoCode) END, CGM_TYPE

              UNION ALL
              
               SELECT CASE TE_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE ISNULL(TE_ISO_CODE, @NationalIsoCode) END AS CMD_ISO_CODE
                    , 0 AS CAGE_CURRENCY_TYPE
                    , @BillsId AS ORIGIN
                    , MC_COLLECTED_BILL_AMOUNT + MC_COLLECTED_COIN_AMOUNT AS DEPOSITS
                    , 0 AS WITHDRAWS
              FROM MONEY_COLLECTIONS
                  INNER JOIN CAGE_MOVEMENTS ON CGM_MC_COLLECTION_ID = MC_COLLECTION_ID 
                  INNER JOIN TERMINALS ON TE_TERMINAL_ID = MC_TERMINAL_ID
             WHERE CAGE_MOVEMENTS.CGM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
               AND CAGE_MOVEMENTS.CGM_TYPE = @CageOperationType_FromTerminal
               AND (CGM_STATUS IN (2, 3, 4, 8, 9, 10) 
                    OR (CGM_STATUS = 0 AND CGM_TYPE = 0))
                                      
          ) AS _TBL_GLB
  LEFT JOIN CURRENCY_EXCHANGE ON CMD_ISO_CODE = CE_CURRENCY_ISO_CODE AND CE_TYPE = 0
      WHERE CMD_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)
   GROUP BY CAGE_CURRENCY_TYPE 
          , CE_CURRENCY_ORDER
          , CMD_ISO_CODE 
          , ORIGIN  
   ORDER BY CAGE_CURRENCY_TYPE 
          , CE_CURRENCY_ORDER
          , CMD_ISO_CODE 
          , ORIGIN DESC 
    
    -- TABLE[3]    
        SELECT * FROM #TABLE_TEMP_3
			    WHERE ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)   
        DROP TABLE #TABLE_TEMP_3
        
    -- TABLE[4]: Other System Concepts    
    SELECT * FROM (       
    SELECT CSM_CONCEPT_ID AS CM_CONCEPT_ID
         , CC.CC_DESCRIPTION AS CM_DESCRIPTION
         , CASE CSM_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CSM.CSM_ISO_CODE END AS CM_ISO_CODE
         , CASE CSM_ISO_CODE WHEN 'X01' THEN 1001 ELSE CASE WHEN CSM_CAGE_CURRENCY_TYPE < 1000 THEN 0 ELSE CSM_CAGE_CURRENCY_TYPE END END AS CAGE_CURRENCY_TYPE
         , SUM(CSM_VALUE) AS CM_VALUE
      FROM CAGE_SESSION_METERS AS CSM
           INNER JOIN CAGE_CONCEPTS AS CC ON CSM.CSM_CONCEPT_ID = CC.CC_CONCEPT_ID
     WHERE CSM_SOURCE_TARGET_ID = 0
       AND CSM_CONCEPT_ID IN (7, 8)
       AND CSM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
       AND CSM_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)
  GROUP BY CSM_CONCEPT_ID
         , CC.CC_DESCRIPTION
         , CASE CSM_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CSM.CSM_ISO_CODE END 
         , CASE CSM_ISO_CODE WHEN 'X01' THEN 1001 ELSE CASE WHEN CSM_CAGE_CURRENCY_TYPE < 1000 THEN 0 ELSE CSM_CAGE_CURRENCY_TYPE END END
         ) AS A
  ORDER BY CM_CONCEPT_ID
         , CASE CM_ISO_CODE WHEN @NationalIsoCode THEN '000' ELSE CM_ISO_CODE END
         , CAGE_CURRENCY_TYPE
         
     -- TABLE[5]: Cage Counts
    SELECT ORIGIN AS ORIGIN
         , CASE CMD_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CMD_ISO_CODE END AS ISO_CODE
         , CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE CASE WHEN CAGE_CURRENCY_TYPE = 1 THEN 0 ELSE CAGE_CURRENCY_TYPE END END AS CAGE_CURRENCY_TYPE
         , ISNULL(SUM(CMD_DEPOSITS), 0) AS DEPOSITS
      FROM (SELECT CASE WHEN CMD_QUANTITY >= 0 THEN 0
                        ELSE CMD_QUANTITY END  AS ORIGIN
                 , CMD_ISO_CODE
                 , ISNULL(CMD_CAGE_CURRENCY_TYPE, 0) AS CAGE_CURRENCY_TYPE
                 , ISNULL(SUM (  CASE WHEN CMD_QUANTITY >= 0 THEN CMD_QUANTITY ELSE 1 END 
                               * CASE WHEN CGM_TYPE = 99 THEN -1 ELSE 1 END
                               * CASE WHEN CMD_CAGE_CURRENCY_TYPE = @CageCurrencyType_ColorChips THEN 1 ELSE CMD_DENOMINATION END), 0) AS CMD_DEPOSITS
              FROM CAGE_MOVEMENTS 
                   INNER JOIN CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          
             WHERE CGM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS) and CGM_TYPE IN (99, 199)
          GROUP BY CMD_ISO_CODE, ISNULL(CMD_CAGE_CURRENCY_TYPE, 0), CMD_DENOMINATION, CGM_TYPE, CMD_QUANTITY
            ) AS T1           
   GROUP BY ORIGIN, CASE CMD_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CMD_ISO_CODE END 
         , CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE CASE WHEN CAGE_CURRENCY_TYPE = 1 THEN 0 ELSE CAGE_CURRENCY_TYPE END END 
           
    UNION ALL
              
    SELECT NULL AS ORIGIN
         , CASE CMD_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CMD_ISO_CODE END AS ISO_CODE
         , CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE CASE WHEN CAGE_CURRENCY_TYPE = 1 THEN 0 ELSE CAGE_CURRENCY_TYPE END END AS CAGE_CURRENCY_TYPE
         , ISNULL(SUM(CMD_DEPOSITS), 0) AS DEPOSITS
      FROM (SELECT CMD_ISO_CODE
                 , ISNULL(CMD_CAGE_CURRENCY_TYPE, 0) AS CAGE_CURRENCY_TYPE
                 , ISNULL(SUM (  CASE WHEN CMD_QUANTITY >= 0 THEN CMD_QUANTITY ELSE 1 END 
                               * CASE WHEN CGM_TYPE = 99 THEN -1 ELSE 1 END
                               * CASE WHEN CMD_CAGE_CURRENCY_TYPE = @CageCurrencyType_ColorChips THEN 1 ELSE CMD_DENOMINATION END), 0) AS CMD_DEPOSITS
              FROM CAGE_MOVEMENTS 
                   INNER JOIN CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          
             WHERE CGM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS) and CGM_TYPE IN (99, 199)
          GROUP BY CMD_ISO_CODE, ISNULL(CMD_CAGE_CURRENCY_TYPE, 0), CMD_DENOMINATION, CGM_TYPE, CMD_QUANTITY
           ) AS T1           
  GROUP BY CASE CMD_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CMD_ISO_CODE END 
         , CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE CASE WHEN CAGE_CURRENCY_TYPE = 1 THEN 0 ELSE CAGE_CURRENCY_TYPE END END 
 
    DROP TABLE #TMP_CAGE_SESSIONS 

  END -- IF @pCageSessionIds IS NOT NULL THEN
      
  ELSE -- IF @pCageSessionIds IS NULL
  BEGIN
      
    -- TABLE[1]: Get cage sessions information
    SELECT a.* 
          , (ROW_NUMBER() OVER(ORDER BY CM_SOURCE_TARGET_ID
          , CASE CM_CONCEPT_ID WHEN 0 THEN '000' ELSE CC_DESCRIPTION END
          , CASE WHEN CAGE_CURRENCY_TYPE < 1000 THEN 0
                 WHEN CAGE_CURRENCY_TYPE = 1003 THEN 9999999
                 ELSE 1 END
          , CE_CURRENCY_ORDER
          , CM_ISO_CODE
          , CAGE_CURRENCY_TYPE)) as ORDEN
    FROM (
    SELECT CM.CM_SOURCE_TARGET_ID
         , CM.CM_CONCEPT_ID
         , CASE CM.CM_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CM.CM_ISO_CODE END AS CM_ISO_CODE
         , CASE CM.CM_ISO_CODE WHEN 'X01' THEN 1001 ELSE CASE WHEN CM_CAGE_CURRENCY_TYPE < 1000 THEN 0 ELSE CM_CAGE_CURRENCY_TYPE END END AS CAGE_CURRENCY_TYPE
         , SUM(CM.CM_VALUE_IN) AS CM_VALUE_IN
         , SUM(CM.CM_VALUE_OUT) AS CM_VALUE_OUT
         , CST.CST_SOURCE_TARGET_NAME
         , CC.CC_DESCRIPTION
      FROM CAGE_METERS AS CM
           INNER JOIN CAGE_SOURCE_TARGET_CONCEPTS AS CSTC ON CSTC.CSTC_CONCEPT_ID = CM.CM_CONCEPT_ID AND CSTC.CSTC_SOURCE_TARGET_ID = CM.CM_SOURCE_TARGET_ID
           INNER JOIN CAGE_CONCEPTS AS CC ON CC.CC_CONCEPT_ID = CSTC.CSTC_CONCEPT_ID
           INNER JOIN CAGE_SOURCE_TARGET AS CST ON CST.CST_SOURCE_TARGET_ID = CSTC.CSTC_SOURCE_TARGET_ID
     WHERE (CC.CC_TYPE = 1 OR CC_CONCEPT_ID IN (9, 10, 11))
       AND CC.CC_ENABLED = 1
       AND CSTC.CSTC_ENABLED = 1
       AND CC.CC_SHOW_IN_REPORT = 1
       AND CM_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)
  GROUP BY CM.CM_SOURCE_TARGET_ID
         , CM.CM_CONCEPT_ID
         , CASE CM.CM_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CM.CM_ISO_CODE END 
         , CASE CM.CM_ISO_CODE WHEN 'X01' THEN 1001 ELSE CASE WHEN CM_CAGE_CURRENCY_TYPE < 1000 THEN 0 ELSE CM_CAGE_CURRENCY_TYPE END END 
         , CST.CST_SOURCE_TARGET_NAME
         , CC.CC_DESCRIPTION
         ) as a
    LEFT  JOIN CURRENCY_EXCHANGE ON CM_ISO_CODE = CE_CURRENCY_ISO_CODE AND CE_TYPE = 0
  ORDER BY CM_SOURCE_TARGET_ID
         , CASE CM_CONCEPT_ID WHEN 0 THEN '000' ELSE CC_DESCRIPTION END
         , CASE WHEN CAGE_CURRENCY_TYPE < 1000 THEN 0
                WHEN CAGE_CURRENCY_TYPE = 1003 THEN 9999999
                ELSE 1 END
         , CE_CURRENCY_ORDER
         , CM_ISO_CODE
         , CAGE_CURRENCY_TYPE

    -- TABLE[2]: Get dynamic liabilities
    SELECT CC_CONCEPT_ID AS LIABILITY_CONCEPT_ID
         , CC_DESCRIPTION AS LIABILITY_NAME 
         , CASE CM.CM_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CM.CM_ISO_CODE END AS LIABILITY_ISO_CODE
         , CASE WHEN CASE CM.CM_ISO_CODE WHEN 'X01' THEN 1001 ELSE CM_CAGE_CURRENCY_TYPE END < 1000 THEN 0
                ELSE CM_CAGE_CURRENCY_TYPE END AS LIABILITY_CAGE_CURRENCY_TYPE
         , SUM(CM.CM_VALUE) AS LIABILITY_VALUE
      FROM CAGE_METERS AS CM
     INNER JOIN CAGE_CONCEPTS AS CC ON CC.CC_CONCEPT_ID = CM.CM_CONCEPT_ID
      LEFT JOIN CURRENCY_EXCHANGE ON CM_ISO_CODE = CE_CURRENCY_ISO_CODE AND CE_TYPE = 0
     WHERE CC.CC_ENABLED = 1
       AND CC.CC_SHOW_IN_REPORT = 1
       AND CC.CC_IS_PROVISION = 1
       AND CM_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)
  GROUP BY CC.CC_CONCEPT_ID  
         , CC_DESCRIPTION
         , CASE WHEN CASE CM.CM_ISO_CODE WHEN 'X01' THEN 1001 ELSE CM_CAGE_CURRENCY_TYPE END < 1000 THEN 0
                ELSE CM_CAGE_CURRENCY_TYPE END
         , CE_CURRENCY_ORDER
         , CASE CM.CM_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CM.CM_ISO_CODE END
  ORDER BY CC.CC_CONCEPT_ID  
         , CASE WHEN CASE CM.CM_ISO_CODE WHEN 'X01' THEN 1001 ELSE CM_CAGE_CURRENCY_TYPE END < 1000 THEN 0
                ELSE CM_CAGE_CURRENCY_TYPE END
         , CE_CURRENCY_ORDER
         , CASE CM.CM_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CM.CM_ISO_CODE END

      
    -- TEMP TABLE[3]: Get session sumary
    SELECT CMD_ISO_CODE AS ISO_CODE
         , CASE WHEN CAGE_CURRENCY_TYPE = 99 THEN 0 ELSE CAGE_CURRENCY_TYPE END AS CAGE_CURRENCY_TYPE
         , ISNULL(ORIGIN, 0) AS ORIGIN
         , SUM(CMD_DEPOSITS) AS DEPOSITS
         , SUM(CMD_WITHDRAWALS) AS WITHDRAWALS 
         INTO #TEMP_TABLE_3    
      FROM (SELECT CASE CMD_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CMD_ISO_CODE END AS CMD_ISO_CODE
                 , CASE WHEN CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE CAGE_CURRENCY_TYPE END < 1000 THEN 0
                        ELSE CAGE_CURRENCY_TYPE END AS CAGE_CURRENCY_TYPE
                 , ORIGIN
                 , SUM(CMD_DEPOSITS) CMD_DEPOSITS
                 , SUM(CMD_WITHDRAWALS) CMD_WITHDRAWALS      
              FROM (SELECT CASE WHEN CMD_QUANTITY >= 0 THEN @BillsId
                                ELSE CMD_QUANTITY END AS ORIGIN
                         , CASE CMD_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CMD_ISO_CODE END AS CMD_ISO_CODE
                         , CASE WHEN CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE CMD_CAGE_CURRENCY_TYPE END < 1000 THEN 0
                                ELSE CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE ISNULL(CMD_CAGE_CURRENCY_TYPE, 0) END END  AS CAGE_CURRENCY_TYPE
                         , CMD_DENOMINATION
                         , CASE WHEN CGM_TYPE IN (100, 101, 103, 104) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY < 0 AND CMD_QUANTITY <> @TicketsId THEN CMD_DENOMINATION
                                                                                            WHEN CMD_QUANTITY = @TicketsId THEN 0
                                                                                            WHEN CMD_CAGE_CURRENCY_TYPE = @CageCurrencyType_ColorChips THEN CMD_QUANTITY
                                                                                       ELSE CMD_QUANTITY * CMD_DENOMINATION END), 0)
                                ELSE 0 END AS CMD_DEPOSITS
                         , CASE WHEN CGM_TYPE IN (0, 1, 4, 5) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY < 0 AND CMD_QUANTITY <> @TicketsId THEN CMD_DENOMINATION
                                                                                    WHEN CMD_QUANTITY = @TicketsId THEN 0
                                                                                    WHEN CMD_CAGE_CURRENCY_TYPE = @CageCurrencyType_ColorChips THEN CMD_QUANTITY
                                                                                    ELSE CMD_QUANTITY * CMD_DENOMINATION END), 0)
                                ELSE 0 END AS CMD_WITHDRAWALS
                      FROM CAGE_MOVEMENTS 
                INNER JOIN CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          
                     WHERE CGM_STATUS IN (2, 3, 4, 8, 9, 10) 
                        OR (CGM_STATUS = 0 AND CGM_TYPE = 0)
                  GROUP BY CASE WHEN CMD_QUANTITY >= 0 THEN @BillsId
                                ELSE CMD_QUANTITY END 
                         , CASE CMD_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CMD_ISO_CODE END 
                         , CASE WHEN CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE CMD_CAGE_CURRENCY_TYPE END < 1000 THEN 0
                                ELSE CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE ISNULL(CMD_CAGE_CURRENCY_TYPE, 0) END END 
                         , CMD_DENOMINATION
                         , CGM_TYPE

          -- Tickets 
                  UNION ALL
                  
                    SELECT @TicketsId AS ORIGIN
                         , ISNULL(TI_CUR0, @NationalIsoCode) AS CMD_ISO_CODE 
                         , ISNULL(CMD_CAGE_CURRENCY_TYPE, 0) AS CAGE_CURRENCY_TYPE                                   
                         , 1 AS CMD_DENOMINATION
                         , CASE WHEN CGM_TYPE IN (100, 101, 103, 104) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY = @TicketsId THEN 
                                                                                                 CASE WHEN TI_AMT0 IS NULL THEN TI_AMOUNT 
                                                                                                      ELSE TI_AMT0 END
                                                                                       ELSE 0 END), 0)
                                ELSE 0 END AS CMD_DEPOSITS
                         , CASE WHEN CGM_TYPE IN (0, 1, 4, 5) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY = @TicketsId THEN 
                                                                                         CASE WHEN TI_AMT0 IS NULL THEN TI_AMOUNT 
                                                                                              ELSE TI_AMT0 END
                                                                                    ELSE 0 END), 0)
                                ELSE 0 END AS CMD_WITHDRAWALS
                      FROM CAGE_MOVEMENTS 
                           INNER JOIN   CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          
                           INNER JOIN   TICKETS ON TICKETS.TI_COLLECTED_MONEY_COLLECTION = CAGE_MOVEMENTS.CGM_MC_COLLECTION_ID     
                     WHERE  (CGM_STATUS IN (2, 3, 4, 8, 9, 10) 
                            OR (CGM_STATUS = 0 AND CGM_TYPE = 0)) AND CMD_QUANTITY = @TicketsId  
                            AND CAGE_MOVEMENTS.CGM_TYPE <> @CageOperationType_FromTerminal
                     GROUP BY ISNULL(TI_CUR0, @NationalIsoCode) 
                         , ISNULL(CMD_CAGE_CURRENCY_TYPE, 0)                             
                         , CGM_TYPE 
                    ) AS T1
           GROUP BY ORIGIN
                  ,CASE CMD_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CMD_ISO_CODE END 
                 , CASE WHEN CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE CAGE_CURRENCY_TYPE END < 1000 THEN 0
                        ELSE CAGE_CURRENCY_TYPE END 
                 

              -- GET Terminal value-in amounts
              
              UNION ALL 
              
              SELECT ISNULL(TI_CUR0, @NationalIsoCode) AS CMD_ISO_CODE
                    , 0 AS CAGE_CURRENCY_TYPE
                    , @TicketsId AS ORIGIN
                    , ISNULL(SUM (CASE WHEN TI_AMT0 IS NULL THEN TI_AMOUNT 
                                       ELSE TI_AMT0 END),0)  AS DEPOSITS
                    , 0 AS WITHDRAWS
              FROM MONEY_COLLECTIONS
        INNER JOIN CAGE_MOVEMENTS ON CGM_MC_COLLECTION_ID = MC_COLLECTION_ID 
        INNER JOIN TICKETS ON TICKETS.TI_COLLECTED_MONEY_COLLECTION = CAGE_MOVEMENTS.CGM_MC_COLLECTION_ID            
             WHERE CAGE_MOVEMENTS.CGM_TYPE = @CAGEOPERATIONTYPE_FROMTERMINAL
               AND (CGM_STATUS IN (2, 3, 4, 8, 9, 10) 
                    OR (CGM_STATUS = 0 AND CGM_TYPE = 0))
             GROUP BY ISNULL(TI_CUR0, @NationalIsoCode) 

              UNION ALL
              
                SELECT ISNULL(TE_ISO_CODE, @NationalIsoCode) AS CMD_ISO_CODE
                    , 0 AS CAGE_CURRENCY_TYPE
                    , @BillsId AS ORIGIN
                    , MC_COLLECTED_BILL_AMOUNT + MC_COLLECTED_COIN_AMOUNT  AS DEPOSITS
                    , 0 AS WITHDRAWS
              FROM MONEY_COLLECTIONS
                  INNER JOIN CAGE_MOVEMENTS ON CGM_MC_COLLECTION_ID = MC_COLLECTION_ID 
          INNER JOIN TERMINALS ON TE_TERMINAL_ID = MC_TERMINAL_ID
             WHERE CAGE_MOVEMENTS.CGM_TYPE = @CageOperationType_FromTerminal
               AND (CGM_STATUS IN (2, 3, 4, 8, 9, 10) 
                    OR (CGM_STATUS = 0 AND CGM_TYPE = 0))
                          
          ) AS _TBL_GLB
  LEFT JOIN CURRENCY_EXCHANGE ON CMD_ISO_CODE = CE_CURRENCY_ISO_CODE AND CE_TYPE = 0
   GROUP BY CAGE_CURRENCY_TYPE 
          , CE_CURRENCY_ORDER
          , CMD_ISO_CODE 
          , ORIGIN  
   ORDER BY CAGE_CURRENCY_TYPE 
          , CE_CURRENCY_ORDER
          , CMD_ISO_CODE 
          , ORIGIN DESC 
	-- TABLE[3]:
	  SELECT * FROM #TEMP_TABLE_3
	           WHERE ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)   
      DROP TABLE #TEMP_TABLE_3
         
    -- TABLE[4]: Other System Concepts    
    SELECT * FROM (       
    SELECT CM_CONCEPT_ID
         , CC.CC_DESCRIPTION AS CM_DESCRIPTION
         , CASE CM_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CM.CM_ISO_CODE END AS CM_ISO_CODE
         , CASE CM_ISO_CODE WHEN 'X01' THEN 1001 ELSE CASE WHEN CM_CAGE_CURRENCY_TYPE < 1000 THEN 0 ELSE CM_CAGE_CURRENCY_TYPE END END AS CAGE_CURRENCY_TYPE
         , SUM(CM_VALUE) AS CM_VALUE
      FROM CAGE_METERS AS CM
           INNER JOIN CAGE_CONCEPTS AS CC ON CM.CM_CONCEPT_ID = CC.CC_CONCEPT_ID
     WHERE CM_SOURCE_TARGET_ID = 0
       AND CM_CONCEPT_ID IN (7, 8)
       AND CM_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)
  GROUP BY CM_CONCEPT_ID
         , CASE CM_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CM.CM_ISO_CODE END
         , CASE CM_ISO_CODE WHEN 'X01' THEN 1001 ELSE CASE WHEN CM_CAGE_CURRENCY_TYPE < 1000 THEN 0 ELSE CM_CAGE_CURRENCY_TYPE END END
         , CC.CC_DESCRIPTION
         ) AS A
  ORDER BY CM_CONCEPT_ID
         , CASE CM_ISO_CODE WHEN @NationalIsoCode THEN '000' ELSE CM_ISO_CODE END
         , CAGE_CURRENCY_TYPE
    
    -- TABLE[5]: Cage Counts
    SELECT ORIGIN AS ORIGIN
         , CASE CMD_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CMD_ISO_CODE END AS ISO_CODE
         , CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE CASE WHEN CAGE_CURRENCY_TYPE = 1 THEN 0 ELSE CAGE_CURRENCY_TYPE END END AS CAGE_CURRENCY_TYPE
         , ISNULL(SUM(CMD_DEPOSITS), 0) AS DEPOSITS
      FROM (SELECT CASE WHEN CMD_QUANTITY >= 0 THEN 0
                        ELSE CMD_QUANTITY END  AS ORIGIN
                 , CMD_ISO_CODE
                 , ISNULL(CMD_CAGE_CURRENCY_TYPE, 0) AS CAGE_CURRENCY_TYPE
                 , ISNULL(SUM (  CASE WHEN CMD_QUANTITY >= 0 THEN CMD_QUANTITY ELSE 1 END 
                               * CASE WHEN CGM_TYPE = 99 THEN -1 ELSE 1 END
                               * CASE WHEN CMD_CAGE_CURRENCY_TYPE = @CageCurrencyType_ColorChips THEN 1 ELSE CMD_DENOMINATION END), 0) AS CMD_DEPOSITS
              FROM CAGE_MOVEMENTS 
                   INNER JOIN CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          
             WHERE CGM_TYPE IN (99, 199)
          GROUP BY CMD_ISO_CODE, ISNULL(CMD_CAGE_CURRENCY_TYPE, 0), CMD_DENOMINATION, CGM_TYPE, CMD_QUANTITY
            ) AS T1           
   GROUP BY ORIGIN, CASE CMD_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CMD_ISO_CODE END 
         , CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE CASE WHEN CAGE_CURRENCY_TYPE = 1 THEN 0 ELSE CAGE_CURRENCY_TYPE END END 
           
    UNION ALL
              
    SELECT NULL AS ORIGIN
         , CASE CMD_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CMD_ISO_CODE END AS ISO_CODE
         , CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE CASE WHEN CAGE_CURRENCY_TYPE = 1 THEN 0 ELSE CAGE_CURRENCY_TYPE END END AS CAGE_CURRENCY_TYPE
         , ISNULL(SUM(CMD_DEPOSITS), 0) AS DEPOSITS
      FROM (SELECT CMD_ISO_CODE
                 , ISNULL(CMD_CAGE_CURRENCY_TYPE, 0) AS CAGE_CURRENCY_TYPE
                 , ISNULL(SUM (  CASE WHEN CMD_QUANTITY >= 0 THEN CMD_QUANTITY ELSE 1 END 
                               * CASE WHEN CGM_TYPE = 99 THEN -1 ELSE 1 END
                               * CASE WHEN CMD_CAGE_CURRENCY_TYPE = @CageCurrencyType_ColorChips THEN 1 ELSE CMD_DENOMINATION END), 0) AS CMD_DEPOSITS
              FROM CAGE_MOVEMENTS 
                   INNER JOIN CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          
             WHERE CGM_TYPE IN (99, 199)
          GROUP BY CMD_ISO_CODE, ISNULL(CMD_CAGE_CURRENCY_TYPE, 0), CMD_DENOMINATION, CGM_TYPE, CMD_QUANTITY
           ) AS T1           
  GROUP BY CASE CMD_ISO_CODE WHEN 'X01' THEN @NationalIsoCode ELSE CMD_ISO_CODE END 
         , CASE CMD_ISO_CODE WHEN 'X01' THEN 1001 ELSE CASE WHEN CAGE_CURRENCY_TYPE = 1 THEN 0 ELSE CAGE_CURRENCY_TYPE END END 
 

  END -- ELSE
  
  -- TABLE[6]: Stock Stored 
  SELECT @StockIsStored
  
  DROP TABLE #TMP_CURRENCIES_ISO_CODES
                   
                   
END -- CageGetGlobalReportData
GO

GRANT EXECUTE ON [dbo].[CageGetGlobalReportData] TO [wggui] WITH GRANT OPTION
GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GT_Base_Report_Data]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[GT_Base_Report_Data]
GO

CREATE PROCEDURE [dbo].[GT_Base_Report_Data]
(
      @pBaseType                   INTEGER
    , @pTimeInterval               INTEGER
    , @pDateFrom                   DATETIME
    , @pDateTo                     DATETIME
    , @pOnlyActivity               INTEGER
    , @pOrderBy                    INTEGER
    , @pValidTypes                 VARCHAR(4096)
    , @pSelectedCurrency           NVARCHAR(100)
    , @pTotalToSelectedCurrency    INTEGER
)
AS
BEGIN

-- DECLARATIONS
   -- @_DAYS_AND_TABLES:    A temporary table with all intervals and all base type selected (used to show base types without activity).
   DECLARE @_DAYS_AND_TABLES TABLE(DATE_TIME DATETIME, TABLE_IDENTIFIER_VALUE BIGINT, TABLE_IDENT_NAME VARCHAR(50), TABLE_TYPE_IDENT BIGINT, TABLE_TYPE_NAME VARCHAR(50))
   -- @_DAY_VAR:            Variable to create the intervals
   DECLARE @_DAY_VAR DATETIME

-- PARAMETERS
     DECLARE @_BASE_TYPE                  AS   INTEGER -- 0 BY TABLE TYPE, 1 BY TABLE
     DECLARE @_TIME_INTERVAL              AS   INTEGER -- -1, 0 DAYS, 1 MONTH, 2 YEAR
     DECLARE @_DATE_FROM                  AS   DATETIME
     DECLARE @_DATE_TO                    AS   DATETIME
     DECLARE @_ONLY_ACTIVITY              AS   INTEGER -- 0 SHOW ALL, 1 SHOW ONLY WITH ACTIVITY
     DECLARE @_ORDER_BY                   AS   INTEGER -- 0 BY INTERVAL DESC, 1 BY IDENTIFIER ASC
     DECLARE @_DELIMITER                  AS   CHAR(1)
     DECLARE @_TYPES_TABLE                TABLE(SST_ID INT, SST_VALUE VARCHAR(50)) -- TO STORE THE VALID GAMING TABLE TYPES
     DECLARE @_SELECTED_CURRENCY          AS   NVARCHAR(3)
     DECLARE @_TOTAL_TO_SELECTED_CURRENCY AS   INTEGER
     DECLARE @_CHIP_RE                    AS   INTEGER
     DECLARE @_CHIP_NRE                   AS   INTEGER
     DECLARE @_CHIP_COLOR                 AS   INTEGER
----------------------------------------------------------------------------------------------------------------

-- Initialization --
  SET @_BASE_TYPE                  =   @pBaseType
  SET @_TIME_INTERVAL              =   @pTimeInterval
  SET @_DATE_FROM                  =   @pDateFrom
  SET @_DATE_TO                    =   @pDateTo
  SET @_ONLY_ACTIVITY              =   @pOnlyActivity
  SET @_ORDER_BY                   =   @pOrderBy
  SET @_DELIMITER                  =   ','
  SET @_SELECTED_CURRENCY          =   @pSelectedCurrency
  SET @_TOTAL_TO_SELECTED_CURRENCY =   @pTotalToSelectedCurrency
  SET @_CHIP_RE                    =   1001
  SET @_CHIP_NRE                   =   1002
  SET @_CHIP_COLOR                 =   1003
----------------------------------------------------------------------------------------------------------------

-- CHECK DATE PARAMETERS
IF @_DATE_FROM IS NULL
BEGIN
   -- IF DATE FROM IS NULL, USER FIRST POSIBLE DATE
   SET @_DATE_FROM = CAST('' AS DATETIME)
   SET @pDateFrom  = CAST('' AS DATETIME)
END

IF @_DATE_TO IS NULL
BEGIN
   -- IF DATE TO IS NULL, QUERY UNTIL CURRENT DATE
   SET @_DATE_TO = CAST(GETDATE() AS DATETIME)
   SET @pDateTo  = CAST(GETDATE() AS DATETIME)
END

-- ASSIGN TYPES PARAMETER INTO TABLE
INSERT INTO @_TYPES_TABLE SELECT * FROM dbo.SplitStringIntoTable(@pValidTypes, @_DELIMITER, DEFAULT)

IF @_TIME_INTERVAL <> -1
 BEGIN
   -- INTERVALS DATES AND TABLE PREPARATION

   -- PREPARE DATES RANGE DEPENDING ON TIME INTERVAL
   SET @_DATE_FROM = CASE
                        WHEN @_TIME_INTERVAL = 0 THEN -- BY DAY
                           DATEADD(dd, 0, DATEDIFF(dd, 0, @_DATE_FROM))
                        WHEN @_TIME_INTERVAL = 1 THEN -- BY MONTH
                           CAST(CAST(YEAR(@_DATE_FROM) AS VARCHAR(4)) + RIGHT('0' + CAST(MONTH(@_DATE_FROM) AS VARCHAR(2)), 2) + '01' AS DATETIME)
                        WHEN @_TIME_INTERVAL = 2 THEN -- BY YEAR
                           CAST(CAST(YEAR(@_DATE_FROM) AS VARCHAR(4)) + '0101' AS DATETIME)
                     END

   SET @_DATE_TO = CASE
          WHEN @_TIME_INTERVAL = 0 THEN -- BY DAY
                           DATEADD(DAY, 1, CAST(@_DATE_TO AS DATETIME))
                        WHEN @_TIME_INTERVAL = 1 THEN -- BY MONTH
                           DATEADD(MONTH, 1, CAST(CAST(YEAR(@_DATE_TO) AS VARCHAR(4)) + RIGHT('0' + CAST(MONTH(@_DATE_TO) AS VARCHAR(2)), 2) + '01' AS DATETIME))
                        WHEN @_TIME_INTERVAL = 2 THEN -- BY YEAR
                           DATEADD(YEAR, 1, CAST(CAST(YEAR(@_DATE_TO) AS VARCHAR(4)) + '0101' AS DATETIME))
                     END

   -- PREPARE THE TIME INTERVALS TABLE
   SET @_DAY_VAR = @_DATE_FROM
   WHILE @_DAY_VAR < @_DATE_TO
   BEGIN
         -- SET THE LINK FIELD FOR THE RESULTS
         IF @_BASE_TYPE = 0
          BEGIN
            -- LINK WITH TABLE TYPES
            INSERT INTO @_DAYS_AND_TABLES
                 SELECT   CAST(@_DAY_VAR AS DATETIME), X.GTT_GAMING_TABLE_TYPE_ID, X.GTT_NAME AS TABLE_IDENT, NULL, NULL
                   FROM   GAMING_TABLES_TYPES AS X
                  WHERE   X.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
                          AND     X.GTT_ENABLED = 1
          END
         ELSE
          BEGIN
            -- LINK WITH TABLES
            INSERT INTO @_DAYS_AND_TABLES
                 SELECT   CAST(@_DAY_VAR AS DATETIME), X.GT_GAMING_TABLE_ID, X.GT_NAME AS TABLE_IDENT, Z.GTT_GAMING_TABLE_TYPE_ID, Z.GTT_NAME
                   FROM   GAMING_TABLES AS X
              LEFT JOIN   GAMING_TABLES_TYPES AS Z
                     ON   Z.GTT_GAMING_TABLE_TYPE_ID = X.GT_TYPE_ID
                  WHERE   Z.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
                          AND X.GT_ENABLED = 1
          END

          -- SET INCREMENT
          SET @_DAY_VAR = CASE
                               WHEN @_TIME_INTERVAL = 0 THEN DATEADD(DAY,1,@_DAY_VAR)     -- DAY
                               WHEN @_TIME_INTERVAL = 1 THEN DATEADD(MONTH,1,@_DAY_VAR)   -- MONTH
                               WHEN @_TIME_INTERVAL = 2 THEN DATEADD(YEAR,1,@_DAY_VAR)    -- YEAR
                          END
   END

END -- IF INTERVALS <> - 1
ELSE
BEGIN

   -- SET THE LINK FIELD FOR THE RESULTS WITHOUT INTERVALS
   IF @_BASE_TYPE = 0
    BEGIN
      -- LINK WITH TABLE TYPES
      INSERT INTO @_DAYS_AND_TABLES
           SELECT   NULL, X.GTT_GAMING_TABLE_TYPE_ID, X.GTT_NAME AS TABLE_IDENT, NULL, NULL
             FROM   GAMING_TABLES_TYPES AS X
            WHERE   X.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
                    AND X.GTT_ENABLED = 1
    END
   ELSE
    BEGIN
      -- LINK WITH TABLES
      INSERT INTO @_DAYS_AND_TABLES
           SELECT   NULL, X.GT_GAMING_TABLE_ID, X.GT_NAME AS TABLE_IDENT, Z.GTT_GAMING_TABLE_TYPE_ID, Z.GTT_NAME
             FROM   GAMING_TABLES AS X
        LEFT JOIN   GAMING_TABLES_TYPES AS Z
               ON   Z.GTT_GAMING_TABLE_TYPE_ID = X.GT_TYPE_ID
            WHERE   Z.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
                    AND X.GT_ENABLED = 1
    END

 END


-- SELECT INTO TEMPORARY TABLE WITH ALL DATA
-- SPECIFIC REPORT QUERY
  SELECT   X.TABLE_IDENTIFIER
         , X.TABLE_NAME
         , X.TABLE_TYPE
         , X.TABLE_TYPE_NAME
         , SUM(X.S_DROP_GAMBLING_TABLE) + SUM (X.S_DROP_CASHIER) TOTAL_DROP
         , SUM(X.S_DROP_GAMBLING_TABLE) TOTAL_DROP_GAMBLING_TABLE
         , SUM(X.S_DROP_CASHIER) TOTAL_DROP_CASHIER
         , SUM(X.S_WIN) TOTAL_WIN
         , ISNULL(SUM(X.S_TIP),0)  TOTAL_TIP
         , CASE WHEN SUM(X.S_DROP_GAMBLING_TABLE) + SUM (X.S_DROP_CASHIER) = 0 THEN 0 ELSE CAST(SUM(X.S_WIN) AS DECIMAL(18,2)) / CAST(SUM(X.S_DROP_GAMBLING_TABLE) + SUM (X.S_DROP_CASHIER) AS DECIMAL(18,2)) * 100 END AS WIN_DROP
         , MIN(X.THEORIC_HOLD) AS THEORIC_HOLD
         , CASE WHEN SUM(X.S_DROP_GAMBLING_TABLE) + SUM (X.S_DROP_CASHIER) = 0 THEN 0 ELSE SUM(CAST(X.S_TIP AS DECIMAL(18,2))) / CAST(SUM(X.S_DROP_GAMBLING_TABLE) + SUM (X.S_DROP_CASHIER) AS DECIMAL(18,2)) * 100 END AS TIP_DROP
         , SUM(X.S_WIN) + SUM(X.S_TIP) AS WIN_TIPS
         , X.CM_DATE_ONLY
         , MIN(X.OPEN_HOUR) AS OPEN_HOUR
         , MAX(X.CLOSE_HOUR) AS CLOSE_HOUR
         , SUM(X.SESSION_SECONDS) AS SESSION_SECONDS
         , COUNT(X.SESSION_SECONDS) AS SESSION_SUM         

   INTO #GT_TEMPORARY_REPORT_DATA

   FROM (
           -- CORE QUERY
            SELECT CASE WHEN @_TIME_INTERVAL = 0 THEN     -- TO FILTER BY DAY
                        DATEADD(dd, 0, DATEDIFF(dd, 0, CS_OPENING_DATE))
                      WHEN @_TIME_INTERVAL = 1 THEN     -- TO FILTER BY MONTH
                        CAST(CAST(YEAR(CS_OPENING_DATE) AS VARCHAR(4)) + RIGHT('0' + CAST(MONTH(CS_OPENING_DATE) AS VARCHAR(2)), 2) + '01' AS DATETIME)
                      WHEN @_TIME_INTERVAL = 2 THEN     -- TO FILTER BY YEAR
                        CAST(CAST(YEAR(CS_OPENING_DATE) AS VARCHAR(4)) + '01'+ '01' AS DATETIME)
                        END                                                                                       AS CM_DATE_ONLY
                     , (CASE WHEN @_BASE_TYPE = 0 THEN GTT_GAMING_TABLE_TYPE_ID ELSE GT.GT_GAMING_TABLE_ID END)   AS TABLE_IDENTIFIER -- GET THE BASE TYPE IDENTIFIER
                     , (CASE WHEN @_BASE_TYPE = 0 THEN GTT_NAME ELSE GT.GT_NAME END)                              AS TABLE_NAME       -- GET THE BASE TYPE IDENTIFIER NAME
                     , (CASE WHEN @_BASE_TYPE = 0 THEN NULL ELSE GTT_GAMING_TABLE_TYPE_ID END)                    AS TABLE_TYPE       -- TYPE
                     , (CASE WHEN @_BASE_TYPE = 0 THEN NULL ELSE GTT_NAME END)                                    AS TABLE_TYPE_NAME  -- TYPE NAME

                     ,  CASE WHEN @_TOTAL_TO_SELECTED_CURRENCY = 0 THEN
                                     DBO.GT_CALCULATE_DROP_GAMBLING_TABLES(ISNULL(GTS_COLLECTED_AMOUNT                , 0)
                                                                         , ISNULL(GTS_COLLECTED_DROPBOX_AMOUNT        , 0)
                                                                         , ISNULL(GTS_COLLECTED_DROPBOX_CHIPS_AMOUNT  , 0)
                                                                         , 0 -- It does not allow takings of dropbox with tickets
                                                                         , GT_HAS_INTEGRATED_CASHIER)
                             WHEN @_TOTAL_TO_SELECTED_CURRENCY = 1 THEN
                                     DBO.GT_CALCULATE_DROP_GAMBLING_TABLES(SUM(ISNULL(GTSC_COLLECTED_AMOUNT                , 0))
                                                                         , SUM(ISNULL(CASE WHEN GTSC_TYPE = 0            THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                                           WHEN GTSC_TYPE = 1            THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                                      ELSE 0 END,  0))
                                                                         , SUM(ISNULL(CASE WHEN GTSC_TYPE = @_CHIP_RE    THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                                           WHEN GTSC_TYPE = @_CHIP_NRE   THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                                           WHEN GTSC_TYPE = @_CHIP_COLOR THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                             ELSE 0 END,  0))
                                                                         , 0 -- It does not allow takings of dropbox with tickets
                                                                         , GT_HAS_INTEGRATED_CASHIER)
                             END                                                                                  AS S_DROP_GAMBLING_TABLE

                     ,  CASE WHEN @_TOTAL_TO_SELECTED_CURRENCY = 0 THEN
                                     DBO.GT_CALCULATE_DROP_CASHIER(ISNULL(GTS_OWN_SALES_AMOUNT, 0)
                                                                 , ISNULL(GTS_EXTERNAL_SALES_AMOUNT, 0))
                             WHEN @_TOTAL_TO_SELECTED_CURRENCY = 1 THEN
                                     DBO.GT_CALCULATE_DROP_CASHIER(SUM(ISNULL(GTSC_OWN_SALES_AMOUNT, 0))
                                                                 , SUM(ISNULL(GTSC_EXTERNAL_SALES_AMOUNT, 0)))
                             END                                                                                  AS S_DROP_CASHIER

                     , GT_THEORIC_HOLD AS THEORIC_HOLD
                     ,  CASE WHEN @_TOTAL_TO_SELECTED_CURRENCY = 0 THEN
                                     DBO.GT_CALCULATE_WIN(ISNULL(GTS_FINAL_CHIPS_AMOUNT      , 0)
                                                        , ISNULL(GTS_INITIAL_CHIPS_AMOUNT    , 0)
                                                        , ISNULL(GTS_FILLS_CHIPS_AMOUNT      , 0)
                                                        , ISNULL(GTS_CREDITS_CHIPS_AMOUNT    , 0)                                                        
                                                        , ISNULL(GTS_TIPS                    , 0)
                                                        , ISNULL(GTS_OWN_SALES_AMOUNT        , 0)
                                                        , ISNULL(GTS_EXTERNAL_SALES_AMOUNT   , 0)
                                                        , ISNULL(GTS_COLLECTED_AMOUNT        , 0)
                                                        , ISNULL(GTS_COLLECTED_DROPBOX_AMOUNT        , 0)
                                                        , ISNULL(GTS_COLLECTED_DROPBOX_CHIPS_AMOUNT  , 0)
                                                        , 0 -- It does not allow takings of dropbox with tickets)
                                                        , GT_HAS_INTEGRATED_CASHIER)
                             WHEN @_TOTAL_TO_SELECTED_CURRENCY = 1 THEN
                                     DBO.GT_CALCULATE_WIN(SUM(ISNULL(GTSC_FINAL_CHIPS_AMOUNT   , 0))
                                                        , SUM(ISNULL(GTSC_INITIAL_CHIPS_AMOUNT , 0))
                                                        , SUM(ISNULL(GTSC_FILLS_CHIPS_AMOUNT   , 0))
                                                        , SUM(ISNULL(GTSC_CREDITS_CHIPS_AMOUNT  , 0))
                                                        , SUM(ISNULL(GTSC_TIPS                 , 0))
                                                        , SUM(ISNULL(GTSC_OWN_SALES_AMOUNT     , 0))
                                                        , SUM(ISNULL(GTSC_EXTERNAL_SALES_AMOUNT, 0))
                                                        , SUM(ISNULL(GTSC_COLLECTED_AMOUNT     , 0))
                                                        , SUM(ISNULL(CASE WHEN GTSC_TYPE = 0            THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                          WHEN GTSC_TYPE = 1            THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                          ELSE 0 END,  0))
                                                        , SUM(ISNULL(CASE WHEN GTSC_TYPE = @_CHIP_RE    THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                          WHEN GTSC_TYPE = @_CHIP_NRE   THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                          WHEN GTSC_TYPE = @_CHIP_COLOR THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                          ELSE 0 END,  0))
                                                        , 0 -- It does not allow takings of dropbox with tickets
                                                        , GT_HAS_INTEGRATED_CASHIER)
                             END AS S_WIN

                     ,  CASE WHEN @_TOTAL_TO_SELECTED_CURRENCY = 0 THEN
                                     SUM(ISNULL(GTS_TIPS, 0))
                             WHEN @_TOTAL_TO_SELECTED_CURRENCY = 1 THEN
                                     SUM(ISNULL(GTSC_TIPS, 0))
                             ELSE 0
                             END AS S_TIP

                     , CS.CS_OPENING_DATE                                                                         AS OPEN_HOUR
                     , CS.CS_CLOSING_DATE                                                                         AS CLOSE_HOUR
                     , DATEDIFF(SECOND, CS_OPENING_DATE, ISNULL(CS_CLOSING_DATE, GETDATE()))                      AS SESSION_SECONDS
                                   FROM   GAMING_TABLES_SESSIONS GTS
                    LEFT  JOIN   GAMING_TABLES_SESSIONS_BY_CURRENCY GTSC
                           ON    GTSC.GTSC_GAMING_TABLE_SESSION_ID = GTS.GTS_GAMING_TABLE_SESSION_ID
                          AND    GTSC_ISO_CODE IN (@_SELECTED_CURRENCY)
                  INNER JOIN   CASHIER_SESSIONS CS ON GTS.GTS_CASHIER_SESSION_ID = CS.CS_SESSION_ID
                                         AND   CS_OPENING_DATE >= @pDateFrom AND CS_OPENING_DATE < @pDateTo
                  INNER JOIN   GAMING_TABLES GT ON GTS.GTS_GAMING_TABLE_ID = GT.GT_GAMING_TABLE_ID
                           AND   GT.GT_TYPE_ID IN (SELECT SST_VALUE FROM @_TYPES_TABLE)
                  INNER JOIN   GAMING_TABLES_TYPES GTT ON GT.GT_TYPE_ID = GTT.GTT_GAMING_TABLE_TYPE_ID
               WHERE   CS_STATUS = 1 AND (@_TOTAL_TO_SELECTED_CURRENCY = 0 AND GTSC_TYPE = 0 OR @_TOTAL_TO_SELECTED_CURRENCY = 1) -- Only closed sessions
               GROUP BY CS_OPENING_DATE, GTT_GAMING_TABLE_TYPE_ID, GT_GAMING_TABLE_ID, GTT_NAME, GT_NAME, GTS_OWN_SALES_AMOUNT, GTS_EXTERNAL_SALES_AMOUNT, GTS_COLLECTED_AMOUNT, GT_HAS_INTEGRATED_CASHIER
                      , GTS_COLLECTED_DROPBOX_AMOUNT, GTS_COLLECTED_DROPBOX_AMOUNT, GTS_DROPBOX_ENABLED, GTS_COLLECTED_DROPBOX_CHIPS_AMOUNT, GT_THEORIC_HOLD, GTS_FINAL_CHIPS_AMOUNT, GTS_INITIAL_CHIPS_AMOUNT
                      , GTS_FILLS_CHIPS_AMOUNT, GTS_CREDITS_CHIPS_AMOUNT, GTSC_TYPE, GTS_TIPS, GTSC_TIPS, CS_CLOSING_DATE
          -- END CORE QUERY

        ) AS X

  GROUP BY   X.TABLE_TYPE, X.TABLE_IDENTIFIER, X.TABLE_TYPE_NAME, X.TABLE_NAME, X.CM_DATE_ONLY  -- Group by indentifier and time interval

IF @_TIME_INTERVAL <> -1
BEGIN
   -- INTERVALS DATES AND TABLE FINAL PREPARATION

   -- FILTER ACTIVITY
   IF @_ONLY_ACTIVITY = 0
    BEGIN
      -- JOIN THE SELECT WITH ALL DATA
      SELECT
              DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER,
              DT.TABLE_IDENT_NAME AS TABLE_NAME,
              DT.TABLE_TYPE_IDENT AS TABLE_TYPE,
              DT.TABLE_TYPE_NAME AS TABLE_TYPE_NAME,
              ISNULL(ZZ.TOTAL_DROP_GAMBLING_TABLE,0) AS TOTAL_DROP_GAMBLING_TABLE,
              ISNULL(ZZ.TOTAL_DROP_CASHIER,0) AS  TOTAL_DROP_CASHIER,
              ISNULL(ZZ.TOTAL_DROP, 0) AS TOTAL_DROP,
              ISNULL(ZZ.TOTAL_WIN, 0) AS TOTAL_WIN,
              ISNULL(ZZ.TOTAL_TIP, 0) AS TOTAL_TIP,
              ISNULL(ZZ.WIN_DROP, 0) AS WIN_DROP,
              ISNULL(ZZ.THEORIC_HOLD, 0) AS THEORIC_HOLD,
              ISNULL(ZZ.TIP_DROP, 0) AS TIP_DROP,
              ISNULL(ZZ.WIN_TIPS, 0) AS WIN_TIPS,
              ZZ.OPEN_HOUR,
              ZZ.CLOSE_HOUR,
              CASE WHEN ZZ.CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED,
              CASE WHEN ZZ.SESSION_SECONDS = 0 THEN DATEDIFF(SECOND,ZZ.OPEN_HOUR,GETDATE()) ELSE ISNULL(ZZ.SESSION_SECONDS, 0) END AS SESSION_SECONDS,
              ISNULL(ZZ.SESSION_SUM, 0) AS SESSION_SUM,
              DATE_TIME              

      FROM @_DAYS_AND_TABLES DT

        LEFT JOIN #GT_TEMPORARY_REPORT_DATA ZZ
                     ON
                          (
                              (@_TIME_INTERVAL = 0 AND DATE_TIME = ZZ.CM_DATE_ONLY)
                           OR (@_TIME_INTERVAL = 1 AND MONTH(DATE_TIME) = MONTH(ZZ.CM_DATE_ONLY) AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
                           OR (@_TIME_INTERVAL = 2 AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
                          )
                    AND   TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER

       -- SET ORDER
       ORDER BY   DT.TABLE_TYPE_IDENT ASC,
                  CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC,
                  DATE_TIME DESC;

    END
   ELSE
    BEGIN
      -- JOIN THE SELECT HIDDING WITHOUT ACTIVITY
      SELECT
              DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER,
              DT.TABLE_IDENT_NAME AS TABLE_NAME,
              DT.TABLE_TYPE_IDENT AS TABLE_TYPE,
              DT.TABLE_TYPE_NAME AS TABLE_TYPE_NAME,
              ISNULL(ZZ.TOTAL_DROP_GAMBLING_TABLE,0) AS TOTAL_DROP_GAMBLING_TABLE,
              ISNULL(ZZ.TOTAL_DROP_CASHIER,0) AS  TOTAL_DROP_CASHIER,
              ISNULL(ZZ.TOTAL_DROP, 0) AS TOTAL_DROP,
              ISNULL(ZZ.TOTAL_WIN, 0) AS TOTAL_WIN,
              ISNULL(ZZ.TOTAL_TIP, 0) AS TOTAL_TIP,
              ISNULL(ZZ.WIN_DROP, 0) AS WIN_DROP,
              ISNULL(ZZ.THEORIC_HOLD, 0) AS THEORIC_HOLD,
              ISNULL(ZZ.TIP_DROP, 0) AS TIP_DROP,
              ISNULL(ZZ.WIN_TIPS, 0) AS WIN_TIPS,
              ZZ.OPEN_HOUR,
              ZZ.CLOSE_HOUR,
              CASE WHEN ZZ.CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED,
              CASE WHEN ZZ.SESSION_SECONDS = 0 THEN DATEDIFF(SECOND,ZZ.OPEN_HOUR,GETDATE()) ELSE ISNULL(ZZ.SESSION_SECONDS, 0) END AS SESSION_SECONDS,
              ISNULL(ZZ.SESSION_SUM, 0) AS SESSION_SUM,
              DATE_TIME

      FROM @_DAYS_AND_TABLES DT

       INNER JOIN #GT_TEMPORARY_REPORT_DATA ZZ
                     ON
                          (
                              (@_TIME_INTERVAL = 0 AND DATE_TIME = ZZ.CM_DATE_ONLY)
                           OR (@_TIME_INTERVAL = 1 AND MONTH(DATE_TIME) = MONTH(ZZ.CM_DATE_ONLY) AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
                           OR (@_TIME_INTERVAL = 2 AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
                          )
                    AND   TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER

       -- SET ORDER
       ORDER BY   DT.TABLE_TYPE_IDENT ASC,
                  CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC,
                  DATE_TIME DESC;

    END -- IF ONLY_ACTIVITY

END
ELSE  -- ELSE WITHOUT INTERVALS
BEGIN

   -- FINAL WITHOUT INTERVALS

      -- FILTER ACTIVITY
   IF @_ONLY_ACTIVITY = 0
    BEGIN
            -- JOIN DATA INCLUDING WITHOUT ACTIVITY
            SELECT   DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER,
                     DT.TABLE_IDENT_NAME AS TABLE_NAME,
                     DT.TABLE_TYPE_IDENT AS TABLE_TYPE,
                     DT.TABLE_TYPE_NAME,
                     ISNULL(ZZ.TOTAL_DROP_GAMBLING_TABLE,0) AS TOTAL_DROP_GAMBLING_TABLE,
                     ISNULL(ZZ.TOTAL_DROP_CASHIER,0) AS  TOTAL_DROP_CASHIER,
                     ISNULL(TOTAL_DROP, 0) AS TOTAL_DROP,
                     ISNULL(TOTAL_WIN, 0) AS TOTAL_WIN,
                     ISNULL(TOTAL_TIP, 0) AS TOTAL_TIP,
                     ISNULL(WIN_DROP, 0) AS WIN_DROP,
                     ISNULL(ZZ.THEORIC_HOLD, 0) AS THEORIC_HOLD,
                     ISNULL(TIP_DROP, 0) AS TIP_DROP,
                     ISNULL(WIN_TIPS, 0) AS WIN_TIPS,
                     OPEN_HOUR,
                     CLOSE_HOUR,
                     CASE WHEN CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED,
                     CASE WHEN SESSION_SECONDS = 0 THEN DATEDIFF(SECOND, OPEN_HOUR, GETDATE()) ELSE ISNULL(SESSION_SECONDS, 0) END AS SESSION_SECONDS,
                   ISNULL(SESSION_SUM, 0) AS SESSION_SUM
              FROM   @_DAYS_AND_TABLES DT

         LEFT JOIN   #GT_TEMPORARY_REPORT_DATA ZZ
                ON   DT.TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER

                          ORDER BY   DT.TABLE_TYPE_IDENT ASC,
                     CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC,
                     DATE_TIME DESC;

     END
    ELSE
     BEGIN
            -- JOIN DATA WITH ONLY ACTIVITY
          SELECT     DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER,
                     DT.TABLE_IDENT_NAME AS TABLE_NAME,
                     DT.TABLE_TYPE_IDENT AS TABLE_TYPE,
                     DT.TABLE_TYPE_NAME,
                     ISNULL(ZZ.TOTAL_DROP_GAMBLING_TABLE,0) AS TOTAL_DROP_GAMBLING_TABLE,
                     ISNULL(ZZ.TOTAL_DROP_CASHIER,0) AS  TOTAL_DROP_CASHIER,
                     ISNULL(TOTAL_DROP, 0) AS TOTAL_DROP,
                     ISNULL(TOTAL_WIN, 0) AS TOTAL_WIN,
                     ISNULL(TOTAL_TIP, 0) AS TOTAL_TIP,
                     ISNULL(WIN_DROP, 0) AS WIN_DROP,
                     ISNULL(ZZ.THEORIC_HOLD, 0) AS THEORIC_HOLD,
                     ISNULL(TIP_DROP, 0) AS TIP_DROP,
                     ISNULL(WIN_TIPS, 0) AS WIN_TIPS,
                     OPEN_HOUR,
                     CLOSE_HOUR,
                     CASE WHEN CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED,
                     CASE WHEN SESSION_SECONDS = 0 THEN DATEDIFF(SECOND, OPEN_HOUR, GETDATE()) ELSE ISNULL(SESSION_SECONDS, 0) END AS SESSION_SECONDS,
                     ISNULL(SESSION_SUM, 0) AS SESSION_SUM
              FROM   @_DAYS_AND_TABLES DT
                     
         INNER JOIN   #GT_TEMPORARY_REPORT_DATA ZZ
                ON   DT.TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER

           ORDER BY   DT.TABLE_TYPE_IDENT ASC,
                      CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC,
                      DATE_TIME DESC;

     END
 END

-- ERASE THE TEMPORARY DATA
DROP TABLE #GT_TEMPORARY_REPORT_DATA

END 
-- END PROCEDURE


GO

-- PERMISSIONS
GRANT EXECUTE ON [dbo].[GT_Base_Report_Data] TO [wggui] WITH GRANT OPTION
GO


IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'cashier_sessions') and name = 'cs_has_pinpad_operations')
BEGIN
  ALTER TABLE cashier_sessions ADD cs_has_pinpad_operations BigInt NULL;
END
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WSP_AccountPendingHandpays]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[WSP_AccountPendingHandpays]

GO

CREATE PROCEDURE [dbo].[WSP_AccountPendingHandpays]
  @AccountId  BigInt
AS
BEGIN
       SET NOCOUNT ON;

  DECLARE @TypeCancelledCredits AS Int
  DECLARE @TypeJackpot AS Int
  DECLARE @TypeSiteJackpot AS Int
  DECLARE @TypeOrphanCredits AS Int
  DECLARE @oldest_handpay  AS Datetime
  DECLARE @candidate_exists as bit

  DECLARE @TimePeriod as int
  DECLARE @oldest_session_start as Datetime
  DECLARE @newest_session_end   as Datetime
  DECLARE @count                as int
  DECLARE @now   as Datetime

  DECLARE @cph_terminal_id as int
  DECLARE @cph_datetime as Datetime
  DECLARE @cph_amount as Money
  DECLARE @expired as int

  --
  -- Handpay types
  --
  SET @TypeCancelledCredits = 0
  SET @TypeJackpot          = 1
  SET @TypeSiteJackpot      = 20
  SET @TypeOrphanCredits    = 2

  SET @expired              = 16384 -- 0x4000 Expired

  --
  -- Oldest Handpay 'Not expired'
  --
  SET @now = GETDATE()

  SELECT   @TimePeriod              = CAST (GP_KEY_VALUE AS INT)
    FROM   GENERAL_PARAMS
   WHERE   GP_GROUP_KEY             = 'Cashier.HandPays'
     AND   GP_SUBJECT_KEY           = 'TimePeriod'
     AND   ISNUMERIC (GP_KEY_VALUE) = 1 --- Ensure is a numeric value

  SET @TimePeriod = ISNULL (@TimePeriod, 60)      -- Default value: 1 hour
  IF ( @TimePeriod <   5 ) SET @TimePeriod =   5  -- Minimum: 5 minutes
  IF ( @TimePeriod > 2500 ) SET @TimePeriod = 2500  -- Maximum: 40 h x 60 min/h = 2500 min approximately

  SET @oldest_handpay = DATEADD (MINUTE, -@TimePeriod, @now)

  --
  -- Pending Handpays
  --
  SELECT   *
    INTO   #PENDING_HANDPAYS
    FROM   HANDPAYS 
   WHERE   HP_MOVEMENT_ID IS NULL
     AND   HP_DATETIME >= @oldest_handpay
     AND   HP_STATUS_CALCULATED <> @expired
     AND   ( HP_TYPE IN (@TypeCancelledCredits, @TypeJackpot, @TypeOrphanCredits)
        OR ( HP_TYPE = @TypeSiteJackpot AND HP_SITE_JACKPOT_AWARDED_TO_ACCOUNT_ID = @AccountId ) )

  ---
  --- Loop on pending Handpays (excluding 'SiteJackpot')
  ---
  DECLARE   cursor_pending_handpays CURSOR FOR
   SELECT   HP_TERMINAL_ID, HP_DATETIME, HP_AMOUNT
      FROM  #PENDING_HANDPAYS
    WHERE   HP_TYPE <> @TypeSiteJackpot
    
  OPEN cursor_pending_handpays
  FETCH NEXT FROM cursor_pending_handpays INTO @cph_terminal_id, @cph_datetime, @cph_amount

  WHILE @@Fetch_Status = 0
  BEGIN
    
    SET @oldest_session_start = DATEADD (DAY, -1, @cph_datetime)
    SET @newest_session_end   = DATEADD (MINUTE, @TimePeriod, @cph_datetime)

    SELECT   @count = COUNT(*)
      FROM   PLAY_SESSIONS
     WHERE   PS_TERMINAL_ID = @cph_terminal_id
       AND   PS_ACCOUNT_ID  = @AccountId
       AND   PS_STARTED     >= @oldest_session_start
       AND   PS_STARTED     <  @cph_datetime
       AND   PS_STARTED     < PS_FINISHED -- Exclude 'paid handpays' sessions 
       AND   PS_FINISHED    >= @oldest_session_start
       AND   PS_FINISHED    <  @newest_session_end

    IF ( @count = 0 )
    BEGIN
      ---
      --- Not a candidate: delete it!
      ---
      DELETE   #PENDING_HANDPAYS
       WHERE   HP_TERMINAL_ID = @cph_terminal_id
         AND   HP_DATETIME    = @cph_datetime
         AND   HP_AMOUNT      = @cph_amount
    END

    --
    -- Next
    --
    FETCH NEXT FROM cursor_pending_handpays INTO @cph_terminal_id, @cph_datetime, @cph_amount

  END

  CLOSE cursor_pending_handpays
  DEALLOCATE cursor_pending_handpays

  SELECT   HP_TERMINAL_ID 
         , HP_DATETIME 
         , HP_TE_PROVIDER_ID 
         , HP_TE_NAME 
         , HP_TYPE 
         , ' '  HP_DUMMY
         , ISNULL (HP_SITE_JACKPOT_NAME, ' ')  HP_SITE_JACKPOT_NAME
         , ISNULL(HP_AMOUNT, HP_AMT0)
         , ISNULL (HP_SITE_JACKPOT_AWARDED_ON_TERMINAL_ID, 0)  HP_SITE_JACKPOT_AWARDED_ON_TERMINAL_ID  
         , ISNULL(HP_AMT0, HP_AMOUNT)     
         , ISNULL(HP_LEVEL,0) 
    FROM   #PENDING_HANDPAYS
   ORDER BY HP_DATETIME DESC


  DROP TABLE #PENDING_HANDPAYS

END -- WSP_AccountPendingHandpays

GO

/****** PERMISSIONS ******/
GRANT EXECUTE ON OBJECT::dbo.WSP_AccountPendingHandpays TO wggui;
GO



IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GT_Base_Report_Data]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[GT_Base_Report_Data]
GO

CREATE PROCEDURE [dbo].[GT_Base_Report_Data]
(
      @pBaseType                   INTEGER
    , @pTimeInterval               INTEGER
    , @pDateFrom                   DATETIME
    , @pDateTo                     DATETIME
    , @pOnlyActivity               INTEGER
    , @pOrderBy                    INTEGER
    , @pValidTypes                 VARCHAR(4096)
    , @pSelectedCurrency           NVARCHAR(100)
    , @pTotalToSelectedCurrency    INTEGER
)
AS
BEGIN

-- DECLARATIONS
   -- @_DAYS_AND_TABLES:    A temporary table with all intervals and all base type selected (used to show base types without activity).
   DECLARE @_DAYS_AND_TABLES TABLE(DATE_TIME DATETIME, TABLE_IDENTIFIER_VALUE BIGINT, TABLE_IDENT_NAME VARCHAR(50), TABLE_TYPE_IDENT BIGINT, TABLE_TYPE_NAME VARCHAR(50))
   -- @_DAY_VAR:            Variable to create the intervals
   DECLARE @_DAY_VAR DATETIME

-- PARAMETERS
     DECLARE @_BASE_TYPE                  AS   INTEGER -- 0 BY TABLE TYPE, 1 BY TABLE
     DECLARE @_TIME_INTERVAL              AS   INTEGER -- -1, 0 DAYS, 1 MONTH, 2 YEAR
     DECLARE @_DATE_FROM                  AS   DATETIME
     DECLARE @_DATE_TO                    AS   DATETIME
     DECLARE @_ONLY_ACTIVITY              AS   INTEGER -- 0 SHOW ALL, 1 SHOW ONLY WITH ACTIVITY
     DECLARE @_ORDER_BY                   AS   INTEGER -- 0 BY INTERVAL DESC, 1 BY IDENTIFIER ASC
     DECLARE @_DELIMITER                  AS   CHAR(1)
     DECLARE @_TYPES_TABLE                TABLE(SST_ID INT, SST_VALUE VARCHAR(50)) -- TO STORE THE VALID GAMING TABLE TYPES
     DECLARE @_SELECTED_CURRENCY          AS   NVARCHAR(3)
     DECLARE @_TOTAL_TO_SELECTED_CURRENCY AS   INTEGER
     DECLARE @_CHIP_RE                    AS   INTEGER
     DECLARE @_CHIP_NRE                   AS   INTEGER
     DECLARE @_CHIP_COLOR                 AS   INTEGER
----------------------------------------------------------------------------------------------------------------

-- Initialization --
  SET @_BASE_TYPE                  =   @pBaseType
  SET @_TIME_INTERVAL              =   @pTimeInterval
  SET @_DATE_FROM                  =   @pDateFrom
  SET @_DATE_TO                    =   @pDateTo
  SET @_ONLY_ACTIVITY              =   @pOnlyActivity
  SET @_ORDER_BY                   =   @pOrderBy
  SET @_DELIMITER                  =   ','
  
  IF @pTotalToSelectedCurrency = 0 
  BEGIN
    SET @_SELECTED_CURRENCY          =   '''' + replace(',', ''',''', @pSelectedCurrency) + ''''
  END
  ELSE
  BEGIN
    SET @_SELECTED_CURRENCY          =   @pSelectedCurrency
  END
  
  SET @_TOTAL_TO_SELECTED_CURRENCY =   @pTotalToSelectedCurrency
  SET @_CHIP_RE                    =   1001
  SET @_CHIP_NRE                   =   1002
  SET @_CHIP_COLOR                 =   1003
----------------------------------------------------------------------------------------------------------------

-- CHECK DATE PARAMETERS
IF @_DATE_FROM IS NULL
BEGIN
   -- IF DATE FROM IS NULL, USER FIRST POSIBLE DATE
   SET @_DATE_FROM = CAST('' AS DATETIME)
   SET @pDateFrom  = CAST('' AS DATETIME)
END

IF @_DATE_TO IS NULL
BEGIN
   -- IF DATE TO IS NULL, QUERY UNTIL CURRENT DATE
   SET @_DATE_TO = CAST(GETDATE() AS DATETIME)
   SET @pDateTo  = CAST(GETDATE() AS DATETIME)
END

-- ASSIGN TYPES PARAMETER INTO TABLE
INSERT INTO @_TYPES_TABLE SELECT * FROM dbo.SplitStringIntoTable(@pValidTypes, @_DELIMITER, DEFAULT)

IF @_TIME_INTERVAL <> -1
 BEGIN
   -- INTERVALS DATES AND TABLE PREPARATION

   -- PREPARE DATES RANGE DEPENDING ON TIME INTERVAL
   SET @_DATE_FROM = CASE
                        WHEN @_TIME_INTERVAL = 0 THEN -- BY DAY
                           DATEADD(dd, 0, DATEDIFF(dd, 0, @_DATE_FROM))
                        WHEN @_TIME_INTERVAL = 1 THEN -- BY MONTH
                           CAST(CAST(YEAR(@_DATE_FROM) AS VARCHAR(4)) + RIGHT('0' + CAST(MONTH(@_DATE_FROM) AS VARCHAR(2)), 2) + '01' AS DATETIME)
                        WHEN @_TIME_INTERVAL = 2 THEN -- BY YEAR
                           CAST(CAST(YEAR(@_DATE_FROM) AS VARCHAR(4)) + '0101' AS DATETIME)
                     END

   SET @_DATE_TO = CASE
          WHEN @_TIME_INTERVAL = 0 THEN -- BY DAY
                           DATEADD(DAY, 1, CAST(@_DATE_TO AS DATETIME))
                        WHEN @_TIME_INTERVAL = 1 THEN -- BY MONTH
                           DATEADD(MONTH, 1, CAST(CAST(YEAR(@_DATE_TO) AS VARCHAR(4)) + RIGHT('0' + CAST(MONTH(@_DATE_TO) AS VARCHAR(2)), 2) + '01' AS DATETIME))
                        WHEN @_TIME_INTERVAL = 2 THEN -- BY YEAR
                           DATEADD(YEAR, 1, CAST(CAST(YEAR(@_DATE_TO) AS VARCHAR(4)) + '0101' AS DATETIME))
                     END

   -- PREPARE THE TIME INTERVALS TABLE
   SET @_DAY_VAR = @_DATE_FROM
   WHILE @_DAY_VAR < @_DATE_TO
   BEGIN
         -- SET THE LINK FIELD FOR THE RESULTS
         IF @_BASE_TYPE = 0
          BEGIN
            -- LINK WITH TABLE TYPES
            INSERT INTO @_DAYS_AND_TABLES
                 SELECT   CAST(@_DAY_VAR AS DATETIME), X.GTT_GAMING_TABLE_TYPE_ID, X.GTT_NAME AS TABLE_IDENT, NULL, NULL
                   FROM   GAMING_TABLES_TYPES AS X
                  WHERE   X.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
                          AND     X.GTT_ENABLED = 1
          END
         ELSE
          BEGIN
            -- LINK WITH TABLES
            INSERT INTO @_DAYS_AND_TABLES
                 SELECT   CAST(@_DAY_VAR AS DATETIME), X.GT_GAMING_TABLE_ID, X.GT_NAME AS TABLE_IDENT, Z.GTT_GAMING_TABLE_TYPE_ID, Z.GTT_NAME
                   FROM   GAMING_TABLES AS X
              LEFT JOIN   GAMING_TABLES_TYPES AS Z
                     ON   Z.GTT_GAMING_TABLE_TYPE_ID = X.GT_TYPE_ID
                  WHERE   Z.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
                          AND X.GT_ENABLED = 1
          END

          -- SET INCREMENT
          SET @_DAY_VAR = CASE
                               WHEN @_TIME_INTERVAL = 0 THEN DATEADD(DAY,1,@_DAY_VAR)     -- DAY
                               WHEN @_TIME_INTERVAL = 1 THEN DATEADD(MONTH,1,@_DAY_VAR)   -- MONTH
                               WHEN @_TIME_INTERVAL = 2 THEN DATEADD(YEAR,1,@_DAY_VAR)    -- YEAR
                          END
   END

END -- IF INTERVALS <> - 1
ELSE
BEGIN

   -- SET THE LINK FIELD FOR THE RESULTS WITHOUT INTERVALS
   IF @_BASE_TYPE = 0
    BEGIN
      -- LINK WITH TABLE TYPES
      INSERT INTO @_DAYS_AND_TABLES
           SELECT   NULL, X.GTT_GAMING_TABLE_TYPE_ID, X.GTT_NAME AS TABLE_IDENT, NULL, NULL
             FROM   GAMING_TABLES_TYPES AS X
            WHERE   X.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
                    AND X.GTT_ENABLED = 1
    END
   ELSE
    BEGIN
      -- LINK WITH TABLES
      INSERT INTO @_DAYS_AND_TABLES
           SELECT   NULL, X.GT_GAMING_TABLE_ID, X.GT_NAME AS TABLE_IDENT, Z.GTT_GAMING_TABLE_TYPE_ID, Z.GTT_NAME
             FROM   GAMING_TABLES AS X
        LEFT JOIN   GAMING_TABLES_TYPES AS Z
               ON   Z.GTT_GAMING_TABLE_TYPE_ID = X.GT_TYPE_ID
            WHERE   Z.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
                    AND X.GT_ENABLED = 1
    END

 END


-- SELECT INTO TEMPORARY TABLE WITH ALL DATA
-- SPECIFIC REPORT QUERY
  SELECT   X.TABLE_IDENTIFIER
         , X.TABLE_NAME
         , X.TABLE_TYPE
         , X.TABLE_TYPE_NAME
         , SUM(X.S_DROP_GAMBLING_TABLE) + SUM (X.S_DROP_CASHIER) TOTAL_DROP
         , SUM(X.S_DROP_GAMBLING_TABLE) TOTAL_DROP_GAMBLING_TABLE
         , SUM(X.S_DROP_CASHIER) TOTAL_DROP_CASHIER
         , SUM(X.S_WIN) TOTAL_WIN
         , ISNULL(SUM(X.S_TIP),0)  TOTAL_TIP
         , CASE WHEN SUM(X.S_DROP_GAMBLING_TABLE) + SUM (X.S_DROP_CASHIER) = 0 THEN 0 ELSE CAST(SUM(X.S_WIN) AS DECIMAL(18,2)) / CAST(SUM(X.S_DROP_GAMBLING_TABLE) + SUM (X.S_DROP_CASHIER) AS DECIMAL(18,2)) * 100 END AS WIN_DROP
         , MIN(X.THEORIC_HOLD) AS THEORIC_HOLD
         , CASE WHEN SUM(X.S_DROP_GAMBLING_TABLE) + SUM (X.S_DROP_CASHIER) = 0 THEN 0 ELSE SUM(CAST(X.S_TIP AS DECIMAL(18,2))) / CAST(SUM(X.S_DROP_GAMBLING_TABLE) + SUM (X.S_DROP_CASHIER) AS DECIMAL(18,2)) * 100 END AS TIP_DROP
         , SUM(X.S_WIN) + SUM(X.S_TIP) AS WIN_TIPS
         , X.CM_DATE_ONLY
         , MIN(X.OPEN_HOUR) AS OPEN_HOUR
         , MAX(X.CLOSE_HOUR) AS CLOSE_HOUR
         , SUM(X.SESSION_SECONDS) AS SESSION_SECONDS
         , COUNT(X.SESSION_SECONDS) AS SESSION_SUM         

   INTO #GT_TEMPORARY_REPORT_DATA

   FROM (
           -- CORE QUERY
            SELECT CASE WHEN @_TIME_INTERVAL = 0 THEN     -- TO FILTER BY DAY
                        DATEADD(dd, 0, DATEDIFF(dd, 0, CS_OPENING_DATE))
                      WHEN @_TIME_INTERVAL = 1 THEN     -- TO FILTER BY MONTH
                        CAST(CAST(YEAR(CS_OPENING_DATE) AS VARCHAR(4)) + RIGHT('0' + CAST(MONTH(CS_OPENING_DATE) AS VARCHAR(2)), 2) + '01' AS DATETIME)
                      WHEN @_TIME_INTERVAL = 2 THEN     -- TO FILTER BY YEAR
                        CAST(CAST(YEAR(CS_OPENING_DATE) AS VARCHAR(4)) + '01'+ '01' AS DATETIME)
                        END                                                                                       AS CM_DATE_ONLY
                     , (CASE WHEN @_BASE_TYPE = 0 THEN GTT_GAMING_TABLE_TYPE_ID ELSE GT.GT_GAMING_TABLE_ID END)   AS TABLE_IDENTIFIER -- GET THE BASE TYPE IDENTIFIER
                     , (CASE WHEN @_BASE_TYPE = 0 THEN GTT_NAME ELSE GT.GT_NAME END)                              AS TABLE_NAME       -- GET THE BASE TYPE IDENTIFIER NAME
                     , (CASE WHEN @_BASE_TYPE = 0 THEN NULL ELSE GTT_GAMING_TABLE_TYPE_ID END)                    AS TABLE_TYPE       -- TYPE
                     , (CASE WHEN @_BASE_TYPE = 0 THEN NULL ELSE GTT_NAME END)                                    AS TABLE_TYPE_NAME  -- TYPE NAME

                     ,  CASE WHEN @_TOTAL_TO_SELECTED_CURRENCY = 0 THEN
                                     DBO.GT_CALCULATE_DROP_GAMBLING_TABLES(ISNULL(GTS_COLLECTED_AMOUNT                , 0)
                                                                         , ISNULL(GTS_COLLECTED_DROPBOX_AMOUNT        , 0)
                                                                         , ISNULL(GTS_COLLECTED_DROPBOX_CHIPS_AMOUNT  , 0)
                                                                         , 0 -- It does not allow takings of dropbox with tickets
                                                                         , GT_HAS_INTEGRATED_CASHIER)
                             WHEN @_TOTAL_TO_SELECTED_CURRENCY = 1 THEN
                                     DBO.GT_CALCULATE_DROP_GAMBLING_TABLES(SUM(ISNULL(GTSC_COLLECTED_AMOUNT                , 0))
                                                                         , SUM(ISNULL(CASE WHEN GTSC_TYPE = 0            THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                                           WHEN GTSC_TYPE = 1            THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                                      ELSE 0 END,  0))
                                                                         , SUM(ISNULL(CASE WHEN GTSC_TYPE = @_CHIP_RE    THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                                           WHEN GTSC_TYPE = @_CHIP_NRE   THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                                           --WHEN GTSC_TYPE = @_CHIP_COLOR THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                             ELSE 0 END,  0))
                                                                         , 0 -- It does not allow takings of dropbox with tickets
                                                                         , GT_HAS_INTEGRATED_CASHIER)
                             END                                                                                  AS S_DROP_GAMBLING_TABLE

                     ,  CASE WHEN @_TOTAL_TO_SELECTED_CURRENCY = 0 THEN
                                     DBO.GT_CALCULATE_DROP_CASHIER(ISNULL(GTS_OWN_SALES_AMOUNT, 0)
                                                                 , ISNULL(GTS_EXTERNAL_SALES_AMOUNT, 0))
                             WHEN @_TOTAL_TO_SELECTED_CURRENCY = 1 THEN
                                     DBO.GT_CALCULATE_DROP_CASHIER(SUM(ISNULL(GTSC_OWN_SALES_AMOUNT, 0))
                                                                 , SUM(ISNULL(GTSC_EXTERNAL_SALES_AMOUNT, 0)))
                             END                                                                                  AS S_DROP_CASHIER

                     , GT_THEORIC_HOLD AS THEORIC_HOLD
                     ,  CASE WHEN @_TOTAL_TO_SELECTED_CURRENCY = 0 THEN
                                     DBO.GT_CALCULATE_WIN(ISNULL(GTS_FINAL_CHIPS_AMOUNT      , 0)
                                                        , ISNULL(GTS_INITIAL_CHIPS_AMOUNT    , 0)
                                                        , ISNULL(GTS_FILLS_CHIPS_AMOUNT      , 0)
                                                        , ISNULL(GTS_CREDITS_CHIPS_AMOUNT    , 0)                                                        
                                                        , ISNULL(GTS_TIPS                    , 0)
                                                        , ISNULL(GTS_OWN_SALES_AMOUNT        , 0)
                                                        , ISNULL(GTS_EXTERNAL_SALES_AMOUNT   , 0)
                                                        , ISNULL(GTS_COLLECTED_AMOUNT        , 0)
                                                        , ISNULL(GTS_COLLECTED_DROPBOX_AMOUNT        , 0)
                                                        , ISNULL(GTS_COLLECTED_DROPBOX_CHIPS_AMOUNT  , 0)
                                                        , 0 -- It does not allow takings of dropbox with tickets)
                                                        , GT_HAS_INTEGRATED_CASHIER)
                             WHEN @_TOTAL_TO_SELECTED_CURRENCY = 1 THEN
                                     DBO.GT_CALCULATE_WIN(SUM(ISNULL(GTSC_FINAL_CHIPS_AMOUNT   , 0))
                                                        , SUM(ISNULL(GTSC_INITIAL_CHIPS_AMOUNT , 0))
                                                        , SUM(ISNULL(GTSC_FILLS_CHIPS_AMOUNT   , 0))
                                                        , SUM(ISNULL(GTSC_CREDITS_CHIPS_AMOUNT  , 0))
                                                        , SUM(ISNULL(GTSC_TIPS                 , 0))
                                                        , SUM(ISNULL(GTSC_OWN_SALES_AMOUNT     , 0))
                                                        , SUM(ISNULL(GTSC_EXTERNAL_SALES_AMOUNT, 0))
                                                        , SUM(ISNULL(GTSC_COLLECTED_AMOUNT     , 0))
                                                        , SUM(ISNULL(CASE WHEN GTSC_TYPE = 0            THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                          WHEN GTSC_TYPE = 1            THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                          ELSE 0 END,  0))
                                                        , SUM(ISNULL(CASE WHEN GTSC_TYPE = @_CHIP_RE    THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                          WHEN GTSC_TYPE = @_CHIP_NRE   THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                          --WHEN GTSC_TYPE = @_CHIP_COLOR THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                          ELSE 0 END,  0))
                                                        , 0 -- It does not allow takings of dropbox with tickets
                                                        , GT_HAS_INTEGRATED_CASHIER)
                             END AS S_WIN

                     ,  CASE WHEN @_TOTAL_TO_SELECTED_CURRENCY = 0 THEN
                                     SUM(ISNULL(GTS_TIPS, 0))
                             WHEN @_TOTAL_TO_SELECTED_CURRENCY = 1 THEN
                                     SUM(ISNULL(GTSC_TIPS, 0))
                             ELSE 0
                             END AS S_TIP

                     , CS.CS_OPENING_DATE                                                                         AS OPEN_HOUR
                     , CS.CS_CLOSING_DATE                                                                         AS CLOSE_HOUR
                     , DATEDIFF(SECOND, CS_OPENING_DATE, ISNULL(CS_CLOSING_DATE, GETDATE()))                      AS SESSION_SECONDS
                                   FROM   GAMING_TABLES_SESSIONS GTS
                    LEFT  JOIN   GAMING_TABLES_SESSIONS_BY_CURRENCY GTSC
                           ON    GTSC.GTSC_GAMING_TABLE_SESSION_ID = GTS.GTS_GAMING_TABLE_SESSION_ID
                          AND    GTSC_ISO_CODE IN (@_SELECTED_CURRENCY) AND GTSC_TYPE <> @_CHIP_COLOR
                  INNER JOIN   CASHIER_SESSIONS CS ON GTS.GTS_CASHIER_SESSION_ID = CS.CS_SESSION_ID
                                         AND   CS_OPENING_DATE >= @pDateFrom AND CS_OPENING_DATE < @pDateTo
                  INNER JOIN   GAMING_TABLES GT ON GTS.GTS_GAMING_TABLE_ID = GT.GT_GAMING_TABLE_ID
                           AND   GT.GT_TYPE_ID IN (SELECT SST_VALUE FROM @_TYPES_TABLE)
                  INNER JOIN   GAMING_TABLES_TYPES GTT ON GT.GT_TYPE_ID = GTT.GTT_GAMING_TABLE_TYPE_ID
               WHERE   CS_STATUS = 1 AND (@_TOTAL_TO_SELECTED_CURRENCY = 0 AND GTSC_TYPE = 0 OR @_TOTAL_TO_SELECTED_CURRENCY = 1) -- Only closed sessions
               GROUP BY CS_OPENING_DATE, GTT_GAMING_TABLE_TYPE_ID, GT_GAMING_TABLE_ID, GTT_NAME, GT_NAME, GTS_OWN_SALES_AMOUNT, GTS_EXTERNAL_SALES_AMOUNT, GTS_COLLECTED_AMOUNT, GT_HAS_INTEGRATED_CASHIER
                      , GTS_COLLECTED_DROPBOX_AMOUNT, GTS_COLLECTED_DROPBOX_AMOUNT, GTS_DROPBOX_ENABLED, GTS_COLLECTED_DROPBOX_CHIPS_AMOUNT, GT_THEORIC_HOLD, GTS_FINAL_CHIPS_AMOUNT, GTS_INITIAL_CHIPS_AMOUNT
                      , GTS_FILLS_CHIPS_AMOUNT, GTS_CREDITS_CHIPS_AMOUNT, GTSC_TYPE, GTS_TIPS, GTSC_TIPS, CS_CLOSING_DATE
          -- END CORE QUERY

        ) AS X

  GROUP BY   X.TABLE_TYPE, X.TABLE_IDENTIFIER, X.TABLE_TYPE_NAME, X.TABLE_NAME, X.CM_DATE_ONLY  -- Group by indentifier and time interval

IF @_TIME_INTERVAL <> -1
BEGIN
   -- INTERVALS DATES AND TABLE FINAL PREPARATION

   -- FILTER ACTIVITY
   IF @_ONLY_ACTIVITY = 0
    BEGIN
      -- JOIN THE SELECT WITH ALL DATA
      SELECT
              DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER,
              DT.TABLE_IDENT_NAME AS TABLE_NAME,
              DT.TABLE_TYPE_IDENT AS TABLE_TYPE,
              DT.TABLE_TYPE_NAME AS TABLE_TYPE_NAME,
              ISNULL(ZZ.TOTAL_DROP_GAMBLING_TABLE,0) AS TOTAL_DROP_GAMBLING_TABLE,
              ISNULL(ZZ.TOTAL_DROP_CASHIER,0) AS  TOTAL_DROP_CASHIER,
              ISNULL(ZZ.TOTAL_DROP, 0) AS TOTAL_DROP,
              ISNULL(ZZ.TOTAL_WIN, 0) AS TOTAL_WIN,
              ISNULL(ZZ.TOTAL_TIP, 0) AS TOTAL_TIP,
              ISNULL(ZZ.WIN_DROP, 0) AS WIN_DROP,
              ISNULL(ZZ.THEORIC_HOLD, 0) AS THEORIC_HOLD,
              ISNULL(ZZ.TIP_DROP, 0) AS TIP_DROP,
              ISNULL(ZZ.WIN_TIPS, 0) AS WIN_TIPS,
              ZZ.OPEN_HOUR,
              ZZ.CLOSE_HOUR,
              CASE WHEN ZZ.CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED,
              CASE WHEN ZZ.SESSION_SECONDS = 0 THEN DATEDIFF(SECOND,ZZ.OPEN_HOUR,GETDATE()) ELSE ISNULL(ZZ.SESSION_SECONDS, 0) END AS SESSION_SECONDS,
              ISNULL(ZZ.SESSION_SUM, 0) AS SESSION_SUM,
              DATE_TIME              

      FROM @_DAYS_AND_TABLES DT

        LEFT JOIN #GT_TEMPORARY_REPORT_DATA ZZ
                     ON
                          (
                              (@_TIME_INTERVAL = 0 AND DATE_TIME = ZZ.CM_DATE_ONLY)
                           OR (@_TIME_INTERVAL = 1 AND MONTH(DATE_TIME) = MONTH(ZZ.CM_DATE_ONLY) AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
                           OR (@_TIME_INTERVAL = 2 AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
                          )
                    AND   TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER

       -- SET ORDER
       ORDER BY   DT.TABLE_TYPE_IDENT ASC,
                  CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC,
                  DATE_TIME DESC;

    END
   ELSE
    BEGIN
      -- JOIN THE SELECT HIDDING WITHOUT ACTIVITY
      SELECT
              DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER,
              DT.TABLE_IDENT_NAME AS TABLE_NAME,
              DT.TABLE_TYPE_IDENT AS TABLE_TYPE,
              DT.TABLE_TYPE_NAME AS TABLE_TYPE_NAME,
              ISNULL(ZZ.TOTAL_DROP_GAMBLING_TABLE,0) AS TOTAL_DROP_GAMBLING_TABLE,
              ISNULL(ZZ.TOTAL_DROP_CASHIER,0) AS  TOTAL_DROP_CASHIER,
              ISNULL(ZZ.TOTAL_DROP, 0) AS TOTAL_DROP,
              ISNULL(ZZ.TOTAL_WIN, 0) AS TOTAL_WIN,
              ISNULL(ZZ.TOTAL_TIP, 0) AS TOTAL_TIP,
              ISNULL(ZZ.WIN_DROP, 0) AS WIN_DROP,
              ISNULL(ZZ.THEORIC_HOLD, 0) AS THEORIC_HOLD,
              ISNULL(ZZ.TIP_DROP, 0) AS TIP_DROP,
              ISNULL(ZZ.WIN_TIPS, 0) AS WIN_TIPS,
              ZZ.OPEN_HOUR,
              ZZ.CLOSE_HOUR,
              CASE WHEN ZZ.CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED,
              CASE WHEN ZZ.SESSION_SECONDS = 0 THEN DATEDIFF(SECOND,ZZ.OPEN_HOUR,GETDATE()) ELSE ISNULL(ZZ.SESSION_SECONDS, 0) END AS SESSION_SECONDS,
              ISNULL(ZZ.SESSION_SUM, 0) AS SESSION_SUM,
              DATE_TIME

      FROM @_DAYS_AND_TABLES DT

       INNER JOIN #GT_TEMPORARY_REPORT_DATA ZZ
                     ON
                          (
                              (@_TIME_INTERVAL = 0 AND DATE_TIME = ZZ.CM_DATE_ONLY)
                           OR (@_TIME_INTERVAL = 1 AND MONTH(DATE_TIME) = MONTH(ZZ.CM_DATE_ONLY) AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
                           OR (@_TIME_INTERVAL = 2 AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
                          )
                    AND   TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER

       -- SET ORDER
       ORDER BY   DT.TABLE_TYPE_IDENT ASC,
                  CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC,
                  DATE_TIME DESC;

    END -- IF ONLY_ACTIVITY

END
ELSE  -- ELSE WITHOUT INTERVALS
BEGIN

   -- FINAL WITHOUT INTERVALS

      -- FILTER ACTIVITY
   IF @_ONLY_ACTIVITY = 0
    BEGIN
            -- JOIN DATA INCLUDING WITHOUT ACTIVITY
            SELECT   DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER,
                     DT.TABLE_IDENT_NAME AS TABLE_NAME,
                     DT.TABLE_TYPE_IDENT AS TABLE_TYPE,
                     DT.TABLE_TYPE_NAME,
                     ISNULL(ZZ.TOTAL_DROP_GAMBLING_TABLE,0) AS TOTAL_DROP_GAMBLING_TABLE,
                     ISNULL(ZZ.TOTAL_DROP_CASHIER,0) AS  TOTAL_DROP_CASHIER,
                     ISNULL(TOTAL_DROP, 0) AS TOTAL_DROP,
                     ISNULL(TOTAL_WIN, 0) AS TOTAL_WIN,
                     ISNULL(TOTAL_TIP, 0) AS TOTAL_TIP,
                     ISNULL(WIN_DROP, 0) AS WIN_DROP,
                     ISNULL(ZZ.THEORIC_HOLD, 0) AS THEORIC_HOLD,
                     ISNULL(TIP_DROP, 0) AS TIP_DROP,
                     ISNULL(WIN_TIPS, 0) AS WIN_TIPS,
                     OPEN_HOUR,
                     CLOSE_HOUR,
                     CASE WHEN CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED,
                     CASE WHEN SESSION_SECONDS = 0 THEN DATEDIFF(SECOND, OPEN_HOUR, GETDATE()) ELSE ISNULL(SESSION_SECONDS, 0) END AS SESSION_SECONDS,
                   ISNULL(SESSION_SUM, 0) AS SESSION_SUM
              FROM   @_DAYS_AND_TABLES DT

         LEFT JOIN   #GT_TEMPORARY_REPORT_DATA ZZ
                ON   DT.TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER

                          ORDER BY   DT.TABLE_TYPE_IDENT ASC,
                     CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC,
                     DATE_TIME DESC;

     END
    ELSE
     BEGIN
            -- JOIN DATA WITH ONLY ACTIVITY
          SELECT     DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER,
                     DT.TABLE_IDENT_NAME AS TABLE_NAME,
                     DT.TABLE_TYPE_IDENT AS TABLE_TYPE,
                     DT.TABLE_TYPE_NAME,
                     ISNULL(ZZ.TOTAL_DROP_GAMBLING_TABLE,0) AS TOTAL_DROP_GAMBLING_TABLE,
                     ISNULL(ZZ.TOTAL_DROP_CASHIER,0) AS  TOTAL_DROP_CASHIER,
                     ISNULL(TOTAL_DROP, 0) AS TOTAL_DROP,
                     ISNULL(TOTAL_WIN, 0) AS TOTAL_WIN,
                     ISNULL(TOTAL_TIP, 0) AS TOTAL_TIP,
                     ISNULL(WIN_DROP, 0) AS WIN_DROP,
                     ISNULL(ZZ.THEORIC_HOLD, 0) AS THEORIC_HOLD,
                     ISNULL(TIP_DROP, 0) AS TIP_DROP,
                     ISNULL(WIN_TIPS, 0) AS WIN_TIPS,
                     OPEN_HOUR,
                     CLOSE_HOUR,
                     CASE WHEN CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED,
                     CASE WHEN SESSION_SECONDS = 0 THEN DATEDIFF(SECOND, OPEN_HOUR, GETDATE()) ELSE ISNULL(SESSION_SECONDS, 0) END AS SESSION_SECONDS,
                     ISNULL(SESSION_SUM, 0) AS SESSION_SUM
              FROM   @_DAYS_AND_TABLES DT
                     
         INNER JOIN   #GT_TEMPORARY_REPORT_DATA ZZ
                ON   DT.TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER

           ORDER BY   DT.TABLE_TYPE_IDENT ASC,
                      CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC,
                      DATE_TIME DESC;

     END
 END

-- ERASE THE TEMPORARY DATA
DROP TABLE #GT_TEMPORARY_REPORT_DATA

END 
-- END PROCEDURE


GO

-- PERMISSIONS
GRANT EXECUTE ON [dbo].[GT_Base_Report_Data] TO [wggui] WITH GRANT OPTION
GO


/******   Fin Object:  Job [jb_DailyMeters]    ******/



/****** Object:  StoredProcedure [dbo].[TITO_HoldvsWin]    Script Date: 04/12/2016 10:27:11 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TITO_HoldvsWin]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[TITO_HoldvsWin]
GO


/****** Object:  StoredProcedure [dbo].[TITO_HoldvsWin]    Script Date: 04/12/2016 10:27:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[TITO_HoldvsWin]
      (@pIniDate DATETIME,
       @pGroupByOption INT = 1,
       @pOrderGroup0 INT = 0)
AS
BEGIN

DECLARE @_d0 AS DATETIME
DECLARE @_d1 AS DATETIME


SET @_d0 = @pIniDate
SET @_d1 = DATEADD (DAY, 1, @_d0)


SET @_d1 = dbo.Opening(0, @_d0)
SET @_d0 = @_d1 -1 

SELECT   AssetNumber       
       , Manufacturer
       , M0                                                         CoinIn
       , (M1+M2)                                                    TotalWon
       , M0-(M1+M2)                                                 Hold
       , M11_BILLS                                                  BillIn
       , NBILL                                                      BillInPerDenom
       , M128                                                       CashableTicketIn
       , M130                                                       RestrictedTicketIn
       , M132                                                       NonRestrictedTicketIn
       , M134                                                       CashableTicketOut
       , M136                                                       RestrictedTicketOut
       , M2                                                         Jackpots
       , M3                                                         HandPaidCanceledCredits
       , (M11_BILLS + M128 + M130 + M132)                           TotalIn
       , M36                                                        TotalDrop
       , (M134 + M136 + M2 + M3)                                    TotalOut
       , (M11_BILLS + M128 + M130 + M132) - (M134 + M136 + M2 + M3) TotalWin
       , (M11_BILLS + M128 + 0 + M132)                              TotalReIn
       , (M134 + 0 + M2 + M3)                                       TotalReOut
       , (M11_BILLS + M128 + 0 + M132) - (M134 + 0 + M2 + M3)       TotalReWin
       , M5                                                         Games
       , M6                                                         Won
       , M160                                                       CashableCardsIn
       , M162                                                       RestrictedCardsIn
       , M164                                                       NonRestrictedCardsIn
       , M184                                                       CashableCardsOut
       , M186                                                       RestrictedCardsOut
       , M188                                                       NonRestrictedCardsOut
INTO #TerminalsMeter
FROM
(  
      SELECT   MIN(TE_TERMINAL_ID) TID
             , MIN(TE_NAME) AssetNumber
             , MIN(TE_PROVIDER_ID) Manufacturer
             , SUM (CASE WHEN TSMH_METER_CODE = 0 THEN ISNULL(TSMH_METER_INCREMENT, 0) ELSE 0 END ) * 0.01 M0
             , SUM (CASE WHEN TSMH_METER_CODE = 1 THEN ISNULL(TSMH_METER_INCREMENT, 0) ELSE 0 END ) * 0.01 M1
             , SUM (CASE WHEN TSMH_METER_CODE = 2 THEN ISNULL(TSMH_METER_INCREMENT, 0) ELSE 0 END ) * 0.01 M2
             , SUM (CASE WHEN TSMH_METER_CODE = 3 THEN ISNULL(TSMH_METER_INCREMENT, 0) ELSE 0 END ) * 0.01 M3
             , SUM (CASE WHEN TSMH_METER_CODE = 4 THEN ISNULL(TSMH_METER_INCREMENT, 0) ELSE 0 END ) * 0.01 M4
             , SUM (CASE WHEN TSMH_METER_CODE = 11 THEN ISNULL(TSMH_METER_INCREMENT, 0) ELSE 0 END )  * 0.01 M11_BILLS
             , SUM (ISNULL(TSMH_METER_INCREMENT, 0) * CASE TSMH_METER_CODE WHEN 64 THEN   1.00
                                                                           WHEN 65 THEN   2.00
                                                                           WHEN 66 THEN   5.00
                                                                           WHEN 67 THEN  10.00
                                                                           WHEN 68 THEN  20.00
                                                                           WHEN 69 THEN  25.00
                                                                           WHEN 70 THEN  50.00
                                                                           WHEN 71 THEN 100.00
                                                                           WHEN 72 THEN 200.00
                                                                           WHEN 73 THEN 250.00
                                                                           WHEN 74 THEN 500.00
                                                                           WHEN 75 THEN 1000.00
                                                                           WHEN 76 THEN 2000.00
                                                                           WHEN 77 THEN 2500.00
                                                                           WHEN 78 THEN 5000.00
                                                                           WHEN 79 THEN 10000.00
                                                                           WHEN 80 THEN 20000.00
                                                                           WHEN 81 THEN 25000.00
                                                                           WHEN 82 THEN 50000.00
                                                                           WHEN 83 THEN 100000.00
                                                                           WHEN 84 THEN 200000.00
                                                                           WHEN 85 THEN 250000.00
                                                                           WHEN 86 THEN 500000.00
                                                                           WHEN 87 THEN 1000000.00
                                                                           ELSE 0  END ) NBILL
      
             , SUM (CASE WHEN TSMH_METER_CODE = 128 THEN ISNULL(TSMH_METER_INCREMENT, 0) ELSE 0 END ) * 0.01 M128
             , SUM (CASE WHEN TSMH_METER_CODE = 130 THEN ISNULL(TSMH_METER_INCREMENT, 0) ELSE 0 END ) * 0.01 M130
             , SUM (CASE WHEN TSMH_METER_CODE = 132 THEN ISNULL(TSMH_METER_INCREMENT, 0) ELSE 0 END ) * 0.01 M132
      
             , SUM (CASE WHEN TSMH_METER_CODE = 134 THEN ISNULL(TSMH_METER_INCREMENT, 0) ELSE 0 END ) * 0.01 M134
             , SUM (CASE WHEN TSMH_METER_CODE = 136 THEN ISNULL(TSMH_METER_INCREMENT, 0) ELSE 0 END ) * 0.01 M136
      
             , SUM (CASE WHEN TSMH_METER_CODE = 36 THEN ISNULL(TSMH_METER_INCREMENT, 0) ELSE 0 END ) * 0.01 M36
      
             , SUM(CASE WHEN (TSMH_METER_CODE IN (11,128,130,132)) THEN ISNULL(TSMH_METER_INCREMENT, 0)  ELSE 0 END ) CommputedTotalIn
             , SUM(CASE WHEN (TSMH_METER_CODE IN (2,3,134,136)) THEN ISNULL(TSMH_METER_INCREMENT, 0)  ELSE 0 END ) CommputedTotalOut
      
             ,
               ( SUM (CASE WHEN tsmh_meter_code = 0 THEN ISNULL(tsmh_meter_increment, 0) ELSE 0 END ) 
               - SUM (CASE WHEN tsmh_meter_code = 1 THEN ISNULL(tsmh_meter_increment, 0) ELSE 0 END ) 
               - SUM (CASE WHEN tsmh_meter_code = 2 THEN ISNULL(tsmh_meter_increment, 0) ELSE 0 END ) 
               ) * 0.01 MNET0
             ,
               ( SUM (CASE WHEN tsmh_meter_code = 11 THEN ISNULL(tsmh_meter_increment, 0) ELSE 0 END ) 
               + SUM (CASE WHEN tsmh_meter_code = 128 THEN ISNULL(tsmh_meter_increment, 0) ELSE 0 END ) 
               + SUM (CASE WHEN tsmh_meter_code = 130 THEN ISNULL(tsmh_meter_increment, 0) ELSE 0 END ) 
               + SUM (CASE WHEN tsmh_meter_code = 132 THEN ISNULL(tsmh_meter_increment, 0) ELSE 0 END ) 
               - SUM (CASE WHEN tsmh_meter_code = 134 THEN ISNULL(tsmh_meter_increment, 0) ELSE 0 END ) 
               - SUM (CASE WHEN tsmh_meter_code = 136 THEN ISNULL(tsmh_meter_increment, 0) ELSE 0 END ) 
               - SUM (CASE WHEN tsmh_meter_code = 3 THEN ISNULL(tsmh_meter_increment, 0) ELSE 0 END ) 
               ) * 0.01 MNET1
             , SUM (CASE WHEN tsmh_meter_code = 5 THEN ISNULL(tsmh_meter_increment, 0) ELSE 0 END )   M5
	           , SUM (CASE WHEN tsmh_meter_code = 6 THEN ISNULL(tsmh_meter_increment, 0) ELSE 0 END )   M6
             , SUM (CASE WHEN tsmh_meter_code = 160 THEN ISNULL(tsmh_meter_increment, 0) ELSE 0 END ) M160
             , SUM (CASE WHEN tsmh_meter_code = 162 THEN ISNULL(tsmh_meter_increment, 0) ELSE 0 END ) M162
             , SUM (CASE WHEN tsmh_meter_code = 164 THEN ISNULL(tsmh_meter_increment, 0) ELSE 0 END ) M164
             , SUM (CASE WHEN tsmh_meter_code = 184 THEN ISNULL(tsmh_meter_increment, 0) ELSE 0 END ) M184
             , SUM (CASE WHEN tsmh_meter_code = 186 THEN ISNULL(tsmh_meter_increment, 0) ELSE 0 END ) M186
             , SUM (CASE WHEN tsmh_meter_code = 188 THEN ISNULL(tsmh_meter_increment, 0) ELSE 0 END ) M188
             
      FROM   TERMINAL_SAS_METERS_HISTORY H 
INNER JOIN   TERMINALS ON TE_TERMINAL_ID = TSMH_TERMINAL_ID 
     WHERE   TSMH_DATETIME >= @_d0 AND TSMH_DATETIME < @_d1
       AND   TSMH_TYPE = 20 -- DAILY
       AND   TSMH_DENOMINATION = 0
       AND   TSMH_GAME_ID      = 0
			 AND   TSMH_METER_CODE IN (0,1,2,3,4,5,6,11,36,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,128,130,132,134,136,160,162,164,184,186,188)
  GROUP BY   TE_TERMINAL_ID, TE_PROVIDER_ID, TE_NAME
) TerminalsMeters 

-- @pGroupByOption == 1 => Provider
-- @pGroupByOption == 2 => Total
-- @pGroupByOption == 0 => Terminal
-- @pGroupByOption == 3 => AGG

if @pGroupByOption = 1
   Begin
        select   Manufacturer,
                 sum(CoinIn) CoinIn, sum(TotalWon) TotalWon, sum(Hold) Hold,
                 sum(BillIn) BillIn, sum(CashableTicketIn) CashableTicketIn, sum(RestrictedTicketIn) RestrictedTicketIn, sum(NonRestrictedTicketIn) NonRestrictedTicketIn,
                 sum(CashableTicketOut) CashableTicketOut, sum(RestrictedTicketOut) RestrictedTicketOut, sum(Jackpots) Jackpots, sum(HandPaidCanceledCredits) HandPaidCanceledCredits,
        
                 sum(TotalIn) TotalIn,
                 sum(TotalOut) TotalOut,
                 sum(TotalOut) TotalWin,
                 
                 sum(TotalReIn) TotalReIn,
                 sum(TotalReOut) TotalReOut,
                 sum(TotalReWin) TotalReWin,
                 sum(1) NumEgms

        from
               ( select * from #TerminalsMeter ) ManufacturerHold
        GROUP BY  MANUFACTURER      
        ORDER BY  Manufacturer                                
   End
else if @pGroupByOption = 2
   begin    
        select   sum(CoinIn) CoinIn, sum(TotalWon) TotalWon, sum(Hold) Hold,
                 sum(BillIn) BillIn, sum(CashableTicketIn) CashableTicketIn, sum(RestrictedTicketIn) RestrictedTicketIn, sum(NonRestrictedTicketIn) NonRestrictedTicketIn,
                 sum(CashableTicketOut) CashableTicketOut, sum(RestrictedTicketOut) RestrictedTicketOut, sum(Jackpots) Jackpots, sum(HandPaidCanceledCredits) HandPaidCanceledCredits,
        
                 sum(TotalIn) TotalIn,
                 sum(TotalOut) TotalOut,
                 sum(TotalOut) TotalWin,
                 
                 sum(TotalReIn) TotalReIn,
                 sum(TotalReOut) TotalReOut,
                 sum(TotalReWin) TotalReWin

        from    #TerminalsMeter
   
    end
else if @pGroupByOption = 3 -- IN CASE OF ADDING COLUMN TO THIS SP, CHECK FOLLOWING OBJECTS:  sp_GenerateDailyMeters
begin

      select 
        AssetNumber,              
        CoinIn,        
        TotalWon,        
        BillIn,       
        CashableTicketIn,
        RestrictedTicketIn,
        NonRestrictedTicketIn,
        Jackpots,       
        Games,
        Won,
        CashableCardsIn,
        RestrictedCardsIn,
        NonRestrictedCardsIn,
        CashableCardsOut,
        RestrictedCardsOut,
        NonRestrictedCardsOut
      from #TerminalsMeter  
      ORDER BY AssetNumber
      
end
else
   begin  
   
      select 
        AssetNumber,       
        Manufacturer,
        CoinIn,
        TotalWon,
        Hold,
        BillIn,
        BillInPerDenom,
        CashableTicketIn,
        RestrictedTicketIn,
        NonRestrictedTicketIn,
        CashableTicketOut,
        RestrictedTicketOut,
        Jackpots,
        HandPaidCanceledCredits,
        TotalIn,
        TotalDrop,
        TotalOut,
        TotalWin,
        TotalReIn,
        TotalReOut,
        TotalReWin
      from #TerminalsMeter  
      ORDER BY 
      case @pOrderGroup0
        when 0
          then AssetNumber
        when 1
          then Manufacturer
      end
      , 
       case @pOrderGroup0
        when 0
          then Manufacturer
        when 1
          then AssetNumber
      end
                                
   end

DROP table #TerminalsMeter

END

GO

GRANT EXECUTE ON [dbo].[TITO_HoldvsWin] TO [wggui] WITH GRANT OPTION

GO



