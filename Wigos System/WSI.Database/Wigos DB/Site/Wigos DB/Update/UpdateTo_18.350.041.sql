/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 349;

SET @New_ReleaseId = 350;
SET @New_ScriptName = N'UpdateTo_18.350.041.sql';
SET @New_Description = N''; 

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/**** GENERAL PARAM *****/

/******* TABLES  *******/

/******* INDEXES *******/

/******* RECORDS *******/

/******* PROCEDURES *******/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetReportBanking]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[GetReportBanking]
GO

CREATE PROCEDURE [dbo].[GetReportBanking]
   @pFrom DATETIME,
   @pTo  DATETIME,
   @pStatus NVARCHAR(MAX),
   @pCardType NVARCHAR(MAX),
   @pNational NVARCHAR(MAX)
AS
BEGIN 
  DECLARE @NationalCurrency NVARCHAR(3)
  SET @NationalCurrency = (SELECT GP.GP_KEY_VALUE FROM GENERAL_PARAMS AS GP 
                           WHERE GP.GP_GROUP_KEY = 'RegionalOptions' AND GP.GP_SUBJECT_KEY = 'CurrencyISOCode')
  IF @pFrom IS NULL
  BEGIN
   SET @pFrom = (SELECT MIN(PT_CREATED) FROM PINPAD_TRANSACTIONS)
  END
  SELECT * FROM
  (                       
    SELECT 
      PT.PT_CREATED
     ,PT.PT_CONTROL_NUMBER
     ,GU.GU_USERNAME
     ,PT.PT_ACCOUNT_ID
     ,AC.AC_HOLDER_NAME
     ,PT.PT_BANK_NAME
     ,PT.PT_CARD_NUMBER
     ,PT.PT_CARD_TYPE  
     ,PT.PT_CARD_HOLDER
     ,PT.PT_TRANSACTION_AMOUNT
     ,PT.PT_COMMISSION_AMOUNT
     ,PT.PT_TOTAL_AMOUNT
     ,PT.PT_STATUS
     ,PT.PT_ERROR_MESSAGE
     ,PT.PT_REFERENCE
     ,PT.PT_OPERATION_ID
     ,PT.PT_MERCHANT_ID
     ,PT.PT_AUTH_CODE
  
     ,PT.PT_COLLECTED  
     ,CASE 
          WHEN PT.PT_CARD_ISO_CODE = @NationalCurrency  THEN 1
          ELSE 0
      END AS PT_IS_NATIONAL
     ,PT.PT_CASHIER_NAME
     ,PT.PT_PINPAD_ID  
    FROM PINPAD_TRANSACTIONS AS PT
    LEFT JOIN GUI_USERS AS GU ON PT.PT_USER_ID = GU.GU_USER_ID
    LEFT JOIN ACCOUNTS AS AC ON PT.PT_ACCOUNT_ID = AC.AC_ACCOUNT_ID
    WHERE PT.PT_CREATED >= @pFrom AND PT.PT_CREATED < @pTo
  ) AS PT
  WHERE PT.PT_STATUS IN (SELECT SST_VALUE FROM SplitStringIntoTable(@pStatus,',',1))
    AND PT.PT_CARD_TYPE IN (SELECT SST_VALUE FROM SplitStringIntoTable(@pCardType,',',1))
    AND PT.PT_IS_NATIONAL IN (SELECT SST_VALUE FROM SplitStringIntoTable(@pNational,',',1))
    ORDER BY PT_CREATED DESC
END
GO
GRANT EXECUTE ON GetReportBanking TO wggui WITH GRANT OPTION 
GO