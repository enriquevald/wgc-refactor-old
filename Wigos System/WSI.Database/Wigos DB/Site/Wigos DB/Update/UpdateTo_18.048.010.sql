/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 47;

SET @New_ReleaseId = 48;
SET @New_ScriptName = N'UpdateTo_18.048.010.sql';
SET @New_Description = N'Re-calculate the non-redeemable amount after a cash-in-while-playing.';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** STORED PROCEDURES ******/
ALTER PROCEDURE [dbo].[PT_ReadData]
  @PlaySessionId              bigint
, @TerminalId                 int             OUTPUT
, @StandAlone                 bit             OUTPUT
, @AccountId                  bigint          OUTPUT
, @InitialBalance             money           OUTPUT
, @CashIn                     money           OUTPUT
, @PlayedAmount               money           OUTPUT
, @WonAmount                  money           OUTPUT
, @FinalBalance               money           OUTPUT
, @InitialNonRedeemable       money           OUTPUT
, @InitialCashIn              money           OUTPUT
, @PrizeLock                  money           OUTPUT
, @AccountBalance             money           OUTPUT
, @PointsBalance              money           OUTPUT
, @HolderName                 nvarchar (50)   OUTPUT
, @HolderLevel                int             OUTPUT
, @MaxAllowedAccountBalance   numeric (20,6)  OUTPUT
, @StatusCode                 int             OUTPUT
, @StatusText                 nvarchar (254)  OUTPUT    
AS
BEGIN
  DECLARE @rc int
	
  SET @StatusCode = 1
	SET @StatusText = 'PT_ReadData: Reading PlaySession...'
	
	--
	-- Select data from PLAY_SESSIONS & ACCOUNTS table
	--
  SELECT @TerminalId            = PS_TERMINAL_ID
       , @StandAlone            = PS_STAND_ALONE
       , @AccountId             = PS_ACCOUNT_ID
       , @InitialBalance        = PS_INITIAL_BALANCE + PS_CASH_IN
       , @CashIn                = PS_CASH_IN
       , @PlayedAmount          = PS_PLAYED_AMOUNT
       , @WonAmount             = PS_WON_AMOUNT
       , @FinalBalance          = PS_FINAL_BALANCE
       , @InitialNonRedeemable  = AC_INITIAL_NOT_REDEEMABLE
       , @InitialCashIn         = AC_INITIAL_CASH_IN
       , @PrizeLock             = AC_NR_WON_LOCK
       , @AccountBalance        = AC_BALANCE
       , @PointsBalance         = AC_POINTS 
       , @HolderName            = ISNULL (AC_HOLDER_NAME, '')
       , @HolderLevel           = ISNULL (AC_HOLDER_LEVEL, 0)
    FROM PLAY_SESSIONS, ACCOUNTS
   WHERE PS_PLAY_SESSION_ID = @PlaySessionId
     AND PS_ACCOUNT_ID      = AC_ACCOUNT_ID

  SET @rc = @@ROWCOUNT
  IF ( @rc <> 1 )
  BEGIN
    SET @StatusCode  = 1
    SET @StatusText  = 'PT_ReadData: Invalid PlaySessionId.'
    GOTO ERROR_PROCEDURE
  END

  IF ( @HolderName = '' )
  BEGIN
    SET @HolderLevel = 0  
  END
  
  --- 
  --- Read Max Allowed Account Balance
  ---
  SELECT @MaxAllowedAccountBalance = GP_KEY_VALUE
    FROM GENERAL_PARAMS
   WHERE GP_GROUP_KEY = 'Cashier'
     AND GP_SUBJECT_KEY = 'MaxAllowedAccountBalance'

  SET @rc = @@ROWCOUNT
  IF ( @rc <> 1 )
  BEGIN
    SET @StatusCode = 1
    SET @StatusText = 'PT_ReadData: Invalid Cashier.MaxAllowedAccountBalance general parameter.'
    GOTO ERROR_PROCEDURE
  END
    
  --
  -- Read data successful
  --
  SET @StatusCode = 0
  SET @StatusText = 'PT_ReadData: PlaySession read.'

ERROR_PROCEDURE:

END -- PT_ReadData
GO


ALTER PROCEDURE [dbo].[PT_PlaySessionFinished]
  @PlaySessionId      bigint
AS
BEGIN

      -- Only for test
  DECLARE @status_code                       int
  DECLARE @status_text                       nvarchar (500)
  -- Only for test
  
  DECLARE @terminal_id                       int
  DECLARE @stand_alone                       bit
  DECLARE @account_id                        bigint
  DECLARE @total_cash_in                     money
  DECLARE @played_amount                     money
  DECLARE @won_amount                        money
  DECLARE @total_cash_out                    money
  DECLARE @initial_non_redeemable0           money
  DECLARE @initial_non_redeemable1           money
  DECLARE @initial_cash_in                   money
  DECLARE @prize_lock                        money
  DECLARE @account_balance                   money
  DECLARE @holder_name                       nvarchar (50)
  DECLARE @holder_level                      int
  
  DECLARE @non_redeemable_cash_in            money
  DECLARE @redeemable_cash_in                money
  DECLARE @non_redeemable_cash_out           money
  DECLARE @redeemable_cash_out               money
  DECLARE @non_redeemable_played             money
  DECLARE @redeemable_played                 money
  DECLARE @non_redeemable_won                money
  DECLARE @redeemable_won                    money
   
  DECLARE @spent_no_redeemable               money
  DECLARE @spent_redeemable                  money

  DECLARE @profit_no_redeemable              money
  DECLARE @profit_redeemable                 money
  
  DECLARE @total_sum_redimible               numeric (20,6)
  DECLARE @total_sum                         numeric (20,6)
  DECLARE @percent_redeemable                numeric (8,6)

  DECLARE @rest_played_and_no_spent          money
  DECLARE @rest_won_and_no_profit            money
  
  DECLARE @total_played_to_1_point           numeric (20,6)
  DECLARE @redeemable_played_to_1_point      numeric (20,6)
  DECLARE @redeemable_spent_to_1_point       numeric (20,6)

  DECLARE @points_balance_before             money
  DECLARE @points_balance_after              money
  DECLARE @points_total_played               money
  DECLARE @points_played_redeemable          money
  DECLARE @points_spent_redeemable           money
  DECLARE @won_points                        money
  DECLARE @points_multiplier                 money
  DECLARE @max_allowed_acc_balance           numeric (20,6)

  DECLARE @cash_in_while_playing             money
  DECLARE @session_cash_in                   money
  DECLARE @total_balance                     money
  DECLARE @redeemable_from_cash_in           money


  -- SET NOCOUNT ON added to prevent extra result sets from
  -- interfering with SELECT statements.
  SET NOCOUNT ON;
      
  SET @status_code  = 0
  SET @status_text  = 'PT_PlaySessionFinished: Init'

  SET @points_total_played      = 0
  SET @points_played_redeemable = 0
  SET @points_spent_redeemable  = 0
  SET @won_points               = 0  
  SET @points_balance_before    = 0
  SET @points_balance_after     = 0


  IF ( @PlaySessionId = 0 ) 
  BEGIN
    SET @status_text = 'PT_PlaySessionFinished: Invalid PlaySessionId'
    GOTO ERROR_PROCEDURE
  END

  --
  -- Read Session & Account & PlayerTracking Data
  --
  EXECUTE dbo.PT_ReadData @PlaySessionId, @terminal_id OUTPUT, @stand_alone OUTPUT, @account_id OUTPUT,
                          @total_cash_in OUTPUT, @session_cash_in OUTPUT, @played_amount OUTPUT, @won_amount OUTPUT, @total_cash_out OUTPUT, 
                          @initial_non_redeemable0 OUTPUT, @initial_cash_in OUTPUT, @prize_lock OUTPUT, @account_balance OUTPUT, 
                          @points_balance_before OUTPUT, @holder_name OUTPUT, @holder_level OUTPUT, @max_allowed_acc_balance OUTPUT,
                          @status_code OUTPUT, @status_text OUTPUT

  IF ( @status_code <> 0 )
    GOTO ERROR_PROCEDURE

  SET @initial_non_redeemable1 = @initial_non_redeemable0

  --
  -- AJQ 01-FEB-2012: Re-calculate the non-redeemable amount after a cash-in-while-playing.
  --
  IF ( @stand_alone = 1 AND @initial_non_redeemable0 > 0 AND ISNULL (@prize_lock, 0) = 0 )
  BEGIN
    --
    -- SAS HOST and Account with NR and No PrizeLock
    --
	  SET @cash_in_while_playing = @account_balance + @session_cash_in

    IF ( @cash_in_while_playing > 0 )
    BEGIN
      --
	    -- Player added money while playing (already on the session and/or in the account)
	    --
      SET @total_balance = @total_cash_out + @account_balance

      -- Compute "money while playing" part
      SET @redeemable_from_cash_in = @cash_in_while_playing
      IF ( @redeemable_from_cash_in > @total_balance )
        SET @redeemable_from_cash_in = @total_balance

      -- Remainig Balance without the "money while playing" part
      SET @total_balance = @total_balance - @redeemable_from_cash_in

      --
      -- Split the "remaining" balance
      --
      EXECUTE dbo.PT_BalancePartsWhenPlaying @total_balance, @initial_cash_in, @initial_non_redeemable0, @prize_lock, 
                                             @non_redeemable_cash_out OUTPUT, @redeemable_cash_out OUTPUT

      --
      -- Update the Initial Non Redeemable
      -- 
      SET @initial_non_redeemable1 = @non_redeemable_cash_out

      UPDATE   ACCOUNTS
         SET   AC_INITIAL_NOT_REDEEMABLE = @initial_non_redeemable1
       WHERE   AC_ACCOUNT_ID             = @account_id
      
    END
  END
    
  --
  -- Compute PlayedCredits, PlayedRedeemableCredits, SpentRedeemableCredits
  --
  EXECUTE dbo.PT_BalancePartsWhenPlaying @total_cash_in, @initial_cash_in, @initial_non_redeemable0, @prize_lock, 
                                         @non_redeemable_cash_in OUTPUT, @redeemable_cash_in OUTPUT

  EXECUTE dbo.PT_BalancePartsWhenPlaying @total_cash_out, @initial_cash_in, @initial_non_redeemable1, @prize_lock, 
                                         @non_redeemable_cash_out OUTPUT, @redeemable_cash_out OUTPUT

  SET @spent_no_redeemable      = dbo.Maximum_Money (0, @non_redeemable_cash_in - @non_redeemable_cash_out)
  SET @spent_redeemable         = dbo.Maximum_Money (0, @redeemable_cash_in - @redeemable_cash_out)

  SET @profit_no_redeemable     = dbo.Maximum_Money (0, @non_redeemable_cash_out - @non_redeemable_cash_in)
  SET @profit_redeemable        = dbo.Maximum_Money (0, @redeemable_cash_out - @redeemable_cash_in)

  SET @total_sum_redimible      = @redeemable_cash_in + @redeemable_cash_out 
  SET @total_sum                = @non_redeemable_cash_in + @non_redeemable_cash_out + @redeemable_cash_in + @redeemable_cash_out
  
  IF @total_sum = 0 GOTO ERROR_PROCEDURE   
  SET @percent_redeemable       = @total_sum_redimible / @total_sum

  SET @rest_played_and_no_spent = @played_amount - (@spent_no_redeemable + @spent_redeemable)
  SET @redeemable_played        = ROUND(@spent_redeemable + (@rest_played_and_no_spent * @percent_redeemable), 4)
  SET @non_redeemable_played    = @played_amount - @redeemable_played

  SET @rest_won_and_no_profit   = @won_amount - (@profit_no_redeemable + @profit_redeemable)
  SET @redeemable_won           = ROUND(@profit_redeemable + (@rest_won_and_no_profit * @percent_redeemable), 4)
  SET @non_redeemable_won       = @won_amount - @redeemable_won

  --
  -- Update PlaySession table (Redeemable & NonRedeemable amounts)
  --
  UPDATE PLAY_SESSIONS
     SET PS_NON_REDEEMABLE_CASH_IN    = @non_redeemable_cash_in
       , PS_NON_REDEEMABLE_CASH_OUT   = @non_redeemable_cash_out
       , PS_NON_REDEEMABLE_PLAYED     = @non_redeemable_played
       , PS_NON_REDEEMABLE_WON        = @non_redeemable_won
       , PS_REDEEMABLE_CASH_IN        = @redeemable_cash_in
       , PS_REDEEMABLE_CASH_OUT       = @redeemable_cash_out
       , PS_REDEEMABLE_PLAYED         = @redeemable_played
       , PS_REDEEMABLE_WON            = @redeemable_won
   WHERE PS_PLAY_SESSION_ID = @PlaySessionId 

  --
  -- RCI & ACC & AJQ 30/09/2010: Reset the Cancellable Operation when PlayedAmount greater than 0.
  --
  UPDATE   ACCOUNTS
     SET   AC_CANCELLABLE_OPERATION_ID = CASE WHEN (@played_amount > 0) THEN NULL ELSE AC_CANCELLABLE_OPERATION_ID END
   WHERE   AC_ACCOUNT_ID               = @account_id  

  --
  -- ACC 22/10/2010: Check maximum allowed account balance
  --
  IF ( @max_allowed_acc_balance > 0 AND @total_cash_out > @max_allowed_acc_balance )
  BEGIN
    UPDATE ACCOUNTS
       SET AC_BLOCKED       = 1
         , AC_BLOCK_REASON  = 2  -- MaxBalance
     WHERE AC_ACCOUNT_ID    = @account_id  
  END

  --- Anonymous accounts have Level=0
  IF ( @holder_level = 0 )
    GOTO EXIT_PROCEDURE

  --
  -- Read PlayerTracking Point Conversion Factors according to the holder's level
  --
  EXECUTE dbo.PT_ReadPointFactors @holder_level, 
                                  @redeemable_spent_to_1_point  OUTPUT,
                                  @redeemable_played_to_1_point OUTPUT,
                                  @total_played_to_1_point      OUTPUT,
                                  @status_code                  OUTPUT, 
                                  @status_text                  OUTPUT

  IF ( @status_code <> 0 )
    GOTO ERROR_PROCEDURE

  --
  -- Compute Points (it is possible to accummulate points per each type of credits at the same time)
  --  
  IF @redeemable_spent_to_1_point  > 0 SET @points_spent_redeemable  = @spent_redeemable  / @redeemable_spent_to_1_point
  IF @redeemable_played_to_1_point > 0 SET @points_played_redeemable = @redeemable_played / @redeemable_played_to_1_point
  IF @total_played_to_1_point      > 0 SET @points_total_played      = @played_amount     / @total_played_to_1_point
  
  SET @won_points = ROUND(@points_total_played + @points_played_redeemable + @points_spent_redeemable, 4)

  -- 
  -- AJQ 20-JAN-2012, Provider's points multiplier
  -- Get the "Provider" multiplier
  --
  SET @points_multiplier = ISNULL ( ( SELECT PV_POINTS_MULTIPLIER FROM PROVIDERS     WHERE PV_ID = (
                                      SELECT TE_PROV_ID           FROM TERMINALS     WHERE TE_TERMINAL_ID = ( 
                                      SELECT PS_TERMINAL_ID       FROM PLAY_SESSIONS WHERE PS_PLAY_SESSION_ID = @PlaySessionId ) ) ), 0 )
  IF ( @points_multiplier <= 0 )
    GOTO EXIT_PROCEDURE

  -- Apply multiplier
  SET @won_points = ROUND (@won_points * @points_multiplier, 4)

  --
  -- Accumulate points
  -- 
  UPDATE   ACCOUNTS
     SET   AC_POINTS                   = AC_POINTS  + @won_points
   WHERE   AC_ACCOUNT_ID               = @account_id  
  
  --  
  -- Create CardMovement.ObtainedPoints
  --
  -- PointsAwarded   = 36,
  SET @points_balance_after     = @points_balance_before + @won_points
  
  EXECUTE dbo.InsertMovement @PlaySessionId, @account_id, @terminal_id, 36, @points_balance_before, 0, @won_points, @points_balance_after

EXIT_PROCEDURE:

  --
  -- ACC 25/11/2010 Accumulate played amount into site jackpot
  --
  EXECUTE dbo.SiteJackpot_AccumulatePlayed @played_amount, @redeemable_played

  SET @status_code  = 0
  SET @status_text  = 'PT_PlaySessionFinished: Successful points accumulated.' 
                    + ' *** '
                    + ' Points.PerRedeemableSpent  = ' + CAST (@points_spent_redeemable AS nvarchar)  + ' (' + CAST (@spent_redeemable AS nvarchar)  + ' credits)'
                    + ' Points.PerRedeemablePlayed = ' + CAST (@points_played_redeemable AS nvarchar) + ' (' + CAST (@redeemable_played AS nvarchar) + ' credits)'
                    + ' Points.PerTotalPlayed      = ' + CAST (@points_total_played AS nvarchar)      + ' (' + CAST (@played_amount AS nvarchar)     + ' credits)'
                    + ' Points.TotalAwarded        = ' + CAST (@won_points AS nvarchar) 
                    + ' *** '

ERROR_PROCEDURE:
--
-- ACC 18-OCT-2010 DO NOT SELECT (3GS not works with this select)
--
--  SELECT @status_text AS StatusText

END -- PT_PlaySessionFinished
GO
