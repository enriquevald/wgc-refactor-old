/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 93;

SET @New_ReleaseId = 94;
SET @New_ScriptName = N'UpdateTo_18.094.022.sql';
SET @New_Description = N'.';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

BEGIN TRANSACTION
GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WSP_PlayerInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[WSP_PlayerInfo]

GO

CREATE PROCEDURE [dbo].[WSP_PlayerInfo]
    -- Add the parameters for the stored procedure here
    @pVendorID  NVARCHAR(16)
  , @TrackData  NVARCHAR(24)

AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;

--
-- 0: Success
-- 1: Vendor not authorized
-- 2: Account Not Found
-- 3: Account Blocked
-- 4: Error
--

DECLARE @pStatusCode    AS INT
DECLARE @pStatusText    AS NVARCHAR(256)

DECLARE @pBlocked       AS BIT
DECLARE @pPlayed        AS MONEY
DECLARE @pWon           AS MONEY
DECLARE @pToday         AS DATETIME
DECLARE @pSiteId        AS INT
DECLARE @pAccountId     AS BIGINT
DECLARE @pHolderLevel   AS INT
DECLARE @pLevelPrefix   AS NVARCHAR (50)
DECLARE @pLevelName     AS NVARCHAR (50)

SET @pStatusCode = 4
SET @pStatusText = N'Error'

SET @pAccountId = ( SELECT AC_ACCOUNT_ID From ACCOUNTS where ac_track_data  = dbo.TrackDataToInternal(@TrackData))
SET @pAccountId = ISNULL (@pAccountId, 0)

SET @pStatusCode = 2
SET @pStatusText = N'Account Not Found'

IF ( @pAccountId > 0 )
BEGIN

    SET @pStatusCode = 3
    SET @pStatusText = N'Account Blocked'

    SELECT @pBlocked = AC_BLOCKED, @pHolderLevel = AC_HOLDER_LEVEL FROM ACCOUNTS WHERE ac_account_id = @pAccountId

    IF ( @pHolderLevel = 1 )      SET @pLevelPrefix = 'Level01'
    ELSE IF ( @pHolderLevel = 2 ) SET @pLevelPrefix = 'Level02'
    ELSE IF ( @pHolderLevel = 3 ) SET @pLevelPrefix = 'Level03'
    ELSE IF ( @pHolderLevel = 4 ) SET @pLevelPrefix = 'Level04'
    ELSE SET @pLevelPrefix = NULL

    IF ( @pLevelPrefix IS NOT NULL )
    BEGIN
      SELECT @pLevelName = GP_KEY_VALUE
        FROM GENERAL_PARAMS
       WHERE GP_GROUP_KEY = 'PlayerTracking'
         AND GP_SUBJECT_KEY = @pLevelPrefix + '.Name'
    END

    SET @pSiteId = ISNULL ( ( SELECT   CAST (GP_KEY_VALUE AS INT) 
                       FROM   GENERAL_PARAMS
                      WHERE   GP_GROUP_KEY   = 'Site' 
                        AND   GP_SUBJECT_KEY = 'Identifier' ), 0 )

    SET @pToday = ( SELECT dbo.TodayOpening (@pSiteId) )

    SET @pPlayed = ( SELECT SUM(PS_PLAYED_AMOUNT) FROM PLAY_SESSIONS WHERE PS_ACCOUNT_ID = @pAccountId AND PS_STARTED >=  @pToday )
    SET @pWon    = ( SELECT SUM(PS_WON_AMOUNT)    FROM PLAY_SESSIONS WHERE PS_ACCOUNT_ID = @pAccountId AND PS_STARTED >=  @pToday )

    IF ( @pBlocked = 0 )
    BEGIN
        -- Insert statements for procedure here
        SELECT   0                                                                                        StatusCode
               , 'Success'                                                                                StatusText
               , CASE WHEN @pLevelPrefix IS NULL THEN NULL ELSE AC_HOLDER_NAME END                        PlayerName
               , CASE WHEN @pLevelPrefix IS NULL THEN NULL ELSE AC_HOLDER_GENDER END                      Gender
               , CASE WHEN @pLevelPrefix IS NULL THEN NULL ELSE AC_HOLDER_BIRTH_DATE END                  Birthdate
               , AC_HOLDER_LEVEL                                                                          Level
               , CASE WHEN @pLevelPrefix IS NULL THEN NULL ELSE @pLevelName END                           LevelName
               , CASE WHEN @pLevelPrefix IS NULL THEN NULL ELSE CAST (ROUND(AC_POINTS, 0, 1) AS INT) END  Points
               , AC_LAST_ACTIVITY                                                                         LastActivity
               , ISNULL (@pPlayed, 0)                                                                     PlayedToday
               , ISNULL (@pWon,    0)                                                                     WonToday

        From ACCOUNTS
        where ac_account_id = @pAccountId
    
        SET @pStatusCode = 0
        SET @pStatusText = N'Success'

    END
END

IF ( @pStatusCode <> 0 )
    SELECT  @pStatusCode   StatusCode
           , @pStatusText  StatusText
           , NULL PlayerName
           , NULL Gender
           , NULL Birthdate
           , NULL Level
           , NULL LevelName
           , NULL Points
           , NULL LastActivity
           , NULL PlayedToday
           , NULL WonToday

END

GO
COMMIT
