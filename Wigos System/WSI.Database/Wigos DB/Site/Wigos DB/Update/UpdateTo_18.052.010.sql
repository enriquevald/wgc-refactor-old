/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 51;

SET @New_ReleaseId = 52;
SET @New_ScriptName = N'UpdateTo_18.052.010.sql';
SET @New_Description = N'New columns in tables and new records in general params.';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/
/* gui_users.gu_user_type */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[gui_users]') and name = 'gu_user_type')
BEGIN
  ALTER TABLE [dbo].[gui_users]
          ADD [gu_user_type] [smallint] NOT NULL CONSTRAINT [DF_gui_users_gu_user_type] DEFAULT ((0))

  EXECUTE sp_addextendedproperty N'MS_Description', N'0 - USER, 1 - SYSTEM, 10 - SUPERUSER', N'SCHEMA', N'dbo', N'TABLE', N'gui_users', N'COLUMN', N'gu_user_type'
END
ELSE
  SELECT '***** Field gui_users.gu_user_type already exists *****';
GO

-- Update User SU as a SUPERUSER type. Do it after the GO.
UPDATE   GUI_USERS
   SET   GU_USER_TYPE = 10
 WHERE   GU_USER_ID   = 0

GO

/* gui_users.gu_logged_in */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[gui_users]') and name = 'gu_logged_in')
  ALTER TABLE [dbo].[gui_users]
          ADD [gu_logged_in] [datetime] NULL
ELSE
  SELECT '***** Field gui_users.gu_logged_in already exists *****';
GO

/* gui_users.gu_logon_computer */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[gui_users]') and name = 'gu_logon_computer')
  ALTER TABLE [dbo].[gui_users]
          ADD [gu_logon_computer] [nvarchar](50) NULL
ELSE
  SELECT '***** Field gui_users.gu_logon_computer already exists *****';
GO

/* gui_users.gu_last_activity */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[gui_users]') and name = 'gu_last_activity')
  ALTER TABLE [dbo].[gui_users]
          ADD [gu_last_activity] [datetime] NULL
ELSE
  SELECT '***** Field gui_users.gu_last_activity already exists *****';
GO

/* gui_users.gu_last_action */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[gui_users]') and name = 'gu_last_action')
  ALTER TABLE [dbo].[gui_users]
          ADD [gu_last_action] [nvarchar](50) NULL
ELSE
  SELECT '***** Field gui_users.gu_last_action already exists *****';
GO

/* gui_users.gu_exit_code */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[gui_users]') and name = 'gu_exit_code')
  ALTER TABLE [dbo].[gui_users]
          ADD [gu_exit_code] [smallint] NULL
ELSE
  SELECT '***** Field gui_users.gu_exit_code already exists *****';
GO

/* play_sessions.ps_cancellable_amount */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[play_sessions]') and name = 'ps_cancellable_amount')
  ALTER TABLE [dbo].[play_sessions]
          ADD [ps_cancellable_amount] [money] NULL
ELSE
  SELECT '***** Field play_sessions.ps_cancellable_amount already exists *****';
GO

/****** RECORDS ******/
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'NoteAcceptor' AND GP_SUBJECT_KEY ='Enabled')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key]
             ,[gp_subject_key]
             ,[gp_key_value])
       VALUES
             ('NoteAcceptor'
             ,'Enabled'
             ,'0');

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Witholding.Document' AND GP_SUBJECT_KEY ='EditableMinutes')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key]
             ,[gp_subject_key]
             ,[gp_key_value])
       VALUES
             ('Witholding.Document'
             ,'EditableMinutes'
             ,'30');

/*�nico rubro*/
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY ='Promo&CouponTogether')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key]
             ,[gp_subject_key]
             ,[gp_key_value])
       VALUES
             ('Cashier'
             ,'Promo&CouponTogether'
             ,'0');

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY ='Promo&CouponTogether.Text')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key]
             ,[gp_subject_key]
             ,[gp_key_value])
       VALUES
             ('Cashier'
             ,'Promo&CouponTogether.Text'
             ,'');

/*Expiraci�n de sesi�n GUI & Cashier*/
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'User' AND GP_SUBJECT_KEY ='SessionTimeOut.GUI')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key]
             ,[gp_subject_key]
             ,[gp_key_value])
       VALUES
             ('User'
             ,'SessionTimeOut.GUI'
             ,'20');

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'User' AND GP_SUBJECT_KEY ='SessionTimeOut.Cashier')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key]
             ,[gp_subject_key]
             ,[gp_key_value])
       VALUES
             ('User'
             ,'SessionTimeOut.Cashier'
             ,'20');

