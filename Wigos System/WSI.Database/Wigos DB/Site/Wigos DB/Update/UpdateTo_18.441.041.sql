/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 440;

SET @New_ReleaseId = 441;

SET @New_ScriptName = N'UpdateTo_18.441.041.sql';
SET @New_Description = N'New release v03.008.0001'; 

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO


/*********************************************************************************************************/


/**** GENERAL PARAM *****/

IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'ExternalLoyaltyProgram.Mode01' AND GP_SUBJECT_KEY = 'Username')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('ExternalLoyaltyProgram.Mode01', 'Username', '')
GO

IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'ExternalLoyaltyProgram.Mode01' AND GP_SUBJECT_KEY = 'Password')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('ExternalLoyaltyProgram.Mode01', 'Password', '')
GO

IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'ExternalLoyaltyProgram.Mode01' AND GP_SUBJECT_KEY = 'SpacenameAmerica')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('ExternalLoyaltyProgram.Mode01', 'SpacenameAmerica', '')
GO

IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'ExternalLoyaltyProgram.Mode01' AND GP_SUBJECT_KEY = 'SpacenameSiebel')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('ExternalLoyaltyProgram.Mode01', 'SpacenameSiebel', '')
GO

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'TerminalsTimeDisconnected' AND GP_SUBJECT_KEY = 'Thread.Enabled')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('TerminalsTimeDisconnected', 'Thread.Enabled', '0');
GO

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'TerminalsTimeDisconnected' AND GP_SUBJECT_KEY = 'Thread.WaitSeconds')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('TerminalsTimeDisconnected', 'Thread.WaitSeconds', '300');
GO

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'WigosGUI' AND GP_SUBJECT_KEY = 'HideTerminalsTimeDisconnected')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('WigosGUI', 'HideTerminalsTimeDisconnected', '1');
GO

IF EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'User' AND GP_SUBJECT_KEY = 'Password.OldRules')
  DELETE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'User' AND GP_SUBJECT_KEY = 'Password.OldRules'
GO

/**** TABLES *****/

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.terminals_time_disconnected') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[terminals_time_disconnected](
  [ttd_site_id] [bigint] NOT NULL,
	[ttd_terminal_id] [int] NOT NULL,
	[ttd_working_day] [datetime] NOT NULL,
	[ttd_egm_collector_seconds] [int] NOT NULL,	
	[ttd_collector_center_seconds] [int] NOT NULL
 CONSTRAINT [PK_ttd_id] PRIMARY KEY CLUSTERED 
(
	[ttd_site_id] ASC,
	[ttd_terminal_id] ASC,
	[ttd_working_day] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.buffer_terminals_time_disconnected') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[buffer_terminals_time_disconnected](
    [bttd_site_id] [bigint] NOT NULL,
	[bttd_terminal_id] [int] NOT NULL,
	[bttd_working_day] [datetime] NOT NULL,
	[bttd_timestamp] [datetime] NOT NULL,	
 CONSTRAINT [PK_bttd_id] PRIMARY KEY CLUSTERED 
(
	[bttd_site_id] ASC,
	[bttd_terminal_id] ASC,
	[bttd_working_day] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

IF NOT EXISTS( SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'reserved_terminal_configuration' AND COLUMN_NAME = 'rtc_selected_terminals') 
BEGIN
	ALTER TABLE		[dbo].[reserved_terminal_configuration] 
	ADD 			rtc_selected_terminals 	XML NULL
END
GO

IF NOT EXISTS( SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'gaming_tables' AND COLUMN_NAME = 'gt_processing') 
BEGIN
	ALTER TABLE		[dbo].[gaming_tables] 
	ADD 			gt_processing BIT NOT NULL DEFAULT 0
END
GO



/**** RECORDS *****/


/**** STORED PROCEDURES *****/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_TerminalsTimeDisconnected]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[SP_TerminalsTimeDisconnected]
GO

CREATE PROCEDURE [dbo].[SP_TerminalsTimeDisconnected]
  @pTimeSeconds       INTEGER
AS
BEGIN

 SELECT   TE_TERMINAL_ID
        , COLLECTOR_CENTER_SECONDS = CASE 
          WHEN (DATEDIFF(SECOND,WS_LAST_RCVD_MSG,GETDATE()) <= 180) 
           THEN 0 
           ELSE @pTimeSeconds 
          END 
        , EGM_COLLECTOR_SECONDS = CASE 
          WHEN (ISNULL(TS_SAS_HOST_ERROR, 0) = 0) 
           THEN 0 
           ELSE @pTimeSeconds 
          END
   FROM   TERMINALS 
   LEFT   OUTER JOIN (SELECT   WS_TERMINAL_ID
                             , MAX(WS_LAST_RCVD_MSG) AS WS_LAST_RCVD_MSG                             
                        FROM   WCP_SESSIONS WITH(INDEX(IX_wcp_status)) 
                       WHERE   WS_STATUS = 0 
                    GROUP BY   WS_TERMINAL_ID) LAST_WCP_SESSIONS ON TE_TERMINAL_ID = WS_TERMINAL_ID 
   LEFT   OUTER JOIN TERMINAL_STATUS ON TE_TERMINAL_ID = TS_TERMINAL_ID 
  WHERE   TE_CLIENT_ID IS NOT NULL -- SOFTWARE VERSION 
    AND   TE_BUILD_ID IS NOT NULL  -- SOFTWARE VERSION
    AND   TE_STATUS = 0            -- ACTIVE
    AND   TE_TERMINAL_TYPE = 5     -- SAS_HOST  
    
END
GO

GRANT EXECUTE ON [dbo].[SP_TerminalsTimeDisconnected] TO [wggui] WITH GRANT OPTION 
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetCageMovementType]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[GetCageMovementType]
GO

CREATE PROCEDURE [dbo].[GetCageMovementType]
 (@pStarDateTime DATETIME,
  @pEndDateTime  DATETIME)
AS
BEGIN
  
  CREATE TABLE #TABLE_ISO_CODE 
  ( 
    id INT Identity(1,1),
    iso_code_column NVARCHAR(5),
    iso_code NVARCHAR(3),
    data_type INTEGER
  ) 
  DECLARE @_count INTEGER	
  DECLARE @_count_aux INTEGER	
  DECLARE @_isoCodeLocal AS NVARCHAR(MAX)
  DECLARE @cols AS NVARCHAR(MAX)
  DECLARE @cols_type_1 AS NVARCHAR(MAX)
  DECLARE @cols_type_2 AS NVARCHAR(MAX) 
  DECLARE @colsFormat AS NVARCHAR(MAX)   
  DECLARE @query  AS NVARCHAR(MAX) 
  DECLARE @_counter INTEGER    
  DECLARE @_isoCode NVARCHAR(MAX)
  DECLARE @_isoCodeColumn NVARCHAR(MAX)
  DECLARE @_dataType INTEGER
  DECLARE @_queryColumns NVARCHAR(MAX)
  DECLARE @_activeDualCurrency NVARCHAR(1)
 
  -- Get All IsoCodes
  INSERT INTO #TABLE_ISO_CODE
  SELECT
    CD.CMD_ISO_CODE AS CMD_ISO_CODE_COLUMN
   ,CD.CMD_ISO_CODE AS CMD_ISO_CODE
   ,1
  FROM CAGE_MOVEMENTS AS CM
  LEFT JOIN CAGE_MOVEMENT_DETAILS AS CD ON CM.CGM_MOVEMENT_ID = CD.CMD_MOVEMENT_ID 
  WHERE   CM.CGM_MOVEMENT_DATETIME >= @pStarDateTime
          AND CM.CGM_MOVEMENT_DATETIME < @pEndDateTime 
          AND CD.CMD_ISO_CODE IS NOT NULL
          AND CD.CMD_ISO_CODE NOT IN (N'X01')
          AND CD.CMD_QUANTITY > 0
          AND CM.CGM_STATUS NOT IN(0, 1)
          AND CM.CGM_TYPE NOT IN(200)
  GROUP BY CD.CMD_ISO_CODE
  UNION 
  SELECT
   (CD.CMD_ISO_CODE + '_0') AS CMD_ISO_CODE
   ,CD.CMD_ISO_CODE
    ,2
  FROM CAGE_MOVEMENTS AS CM
  LEFT JOIN CAGE_MOVEMENT_DETAILS AS CD ON CM.CGM_MOVEMENT_ID = CD.CMD_MOVEMENT_ID 
  WHERE  CM.CGM_MOVEMENT_DATETIME >= @pStarDateTime
         AND CM.CGM_MOVEMENT_DATETIME < @pEndDateTime
         AND CD.CMD_ISO_CODE IS NOT NULL
         AND CD.CMD_ISO_CODE NOT IN (N'X01')   
         AND CD.CMD_QUANTITY IN(-1, -2, -100)
         AND CM.CGM_STATUS NOT IN(0, 1)
         AND CM.CGM_TYPE NOT IN(200)
  GROUP BY CD.CMD_ISO_CODE
  ORDER BY CMD_ISO_CODE
  
    -- Get current IsoCode for GENERAL_PARAMS
  SET @_isoCodeLocal = (SELECT GP.GP_KEY_VALUE 
                        FROM GENERAL_PARAMS AS GP 
                        WHERE GP.GP_GROUP_KEY = N'RegionalOptions' AND GP.GP_SUBJECT_KEY = N'CurrencyISOCode')
  
  SET @_activeDualCurrency = (SELECT CAST(GP.GP_KEY_VALUE AS BIT)  
                           FROM GENERAL_PARAMS AS GP 
                           WHERE GP.GP_GROUP_KEY = N'FloorDualCurrency' AND GP.GP_SUBJECT_KEY = N'Enabled')
      
  CREATE TABLE #GET_CAGE_MOVEMENT_TYPE_TABLE(        
         CMT_MOVEMENT_ID BIGINT,
         CMT_MOVEMENT_DATETIME DATETIME, 
         CMT_TYPE INTEGER,
         CMT_TYPE_NAME NVARCHAR(MAX),       
         CMT_STATUS_NAME NVARCHAR(MAX),
         CMT_USER_NAME NVARCHAR(MAX),
         CMT_CAGE_USER_NAME NVARCHAR(MAX),
         CMT_MACHINE_NAME NVARCHAR(MAX),  
         CMT_COLLECTION_ID BIGINT,
         CMT_TERMINAL_ID INTEGER,
         CMT_CHIPS_AMOUNT MONEY
    )   
        
    SET @_counter = 1		
    SET @_count = (SELECT COUNT(*) FROM #TABLE_ISO_CODE) + 1
    SET @_queryColumns = ''
    SET @cols = ''   
    SET @cols_type_1 = ''  
    SET @cols_type_2 = ''  
    SET @colsFormat =''
    WHILE NOT @_counter = @_count
    BEGIN

      SELECT 
        @_isoCode  =  TIC.iso_code,
        @_isoCodeColumn = TIC.iso_code_column,
        @_dataType = TIC.data_type  
      FROM #TABLE_ISO_CODE AS TIC
      WHERE TIC.id = @_counter

      -- Add column dynamic to #GET_CAGE_MOVEMENT_TYPE_TABLE for isocode
      SET @_queryColumns = N'ALTER TABLE #GET_CAGE_MOVEMENT_TYPE_TABLE ADD [' + @_isoCodeColumn + N'] MONEY '
      EXEC SP_EXECUTESQL @_queryColumns
      
      --Get colums isocode
      
      IF @_dataType = 1
        BEGIN
            SET @cols = @cols + N',AM.'+ @_isoCode
            SET @cols_type_1 = @cols_type_1 + N',' + @_isoCode
            SET @colsFormat = @colsFormat + N',ISNULL('+@_isoCodeColumn+N', 0) AS '+@_isoCode+''
        END
      IF @_dataType = 2
        BEGIN
            SET @cols = @cols + N',AZ.'+@_isoCode+''
            IF @cols_type_2 = ''
              BEGIN
                SET @cols_type_2 = @_isoCode 
              END
            ELSE
              BEGIN            
                SET @cols_type_2 = @cols_type_2 + N',' + @_isoCode 
              END
            SET @colsFormat = @colsFormat + N',ISNULL('+@_isoCodeColumn+N', 0) AS ''Otros ('+@_isoCode+N')'''
         END
     
      SET @_counter = @_counter + 1

    END  
                          
   SET @query = N'   
            INSERT INTO #GET_CAGE_MOVEMENT_TYPE_TABLE 
            SELECT
             CM.CGM_MOVEMENT_ID   
            ,CM.CGM_MOVEMENT_DATETIME
            ,CM.CGM_TYPE
            ,CASE 
                WHEN CM.CGM_TYPE = 0   THEN ''Envío a Cajero''                           WHEN CM.CGM_TYPE = 1   THEN ''Envío a destino personalizado''
                WHEN CM.CGM_TYPE = 2   THEN ''Cierre de bóveda''                         WHEN CM.CGM_TYPE = 3   THEN ''Arqueo de bóveda''
                WHEN CM.CGM_TYPE = 4   THEN ''Envío a mesa de juego''                    WHEN CM.CGM_TYPE = 5   THEN ''Envío a terminal''
                WHEN CM.CGM_TYPE = 99  THEN ''Pérdida de cuadratura''                    WHEN CM.CGM_TYPE = 100 THEN ''Recaudación de Cajero'' 
                WHEN CM.CGM_TYPE = 101 THEN ''Recaudación de origen personalizado''      WHEN CM.CGM_TYPE = 102 THEN ''Apertura de bóveda''
                WHEN CM.CGM_TYPE = 103 THEN ''Recaudación de mesa de juego''             WHEN CM.CGM_TYPE = 104 THEN ''Recaudación de terminal''
                WHEN CM.CGM_TYPE = 105 THEN ''Reapertura de bóveda''                     WHEN CM.CGM_TYPE = 106 THEN ''Recaudación de drop box de mesa de juego''		
                WHEN CM.CGM_TYPE = 107 THEN ''Recaudación de drop box de mesa de juego'' WHEN CM.CGM_TYPE = 108 THEN ''Recaudación de cierre de Cajero''	
                WHEN CM.CGM_TYPE = 109 THEN ''Recaudación de cierre de Mesa de Juego''	 WHEN CM.CGM_TYPE = 199 THEN ''Ganancia de cuadratura''
                WHEN CM.CGM_TYPE = 200 THEN ''Solicitud desde Cajero''                   WHEN CM.CGM_TYPE = 300 THEN ''Provisión de progresivo''
                WHEN CM.CGM_TYPE = 301 THEN ''Progresivo otorgado''                      WHEN CM.CGM_TYPE = 302 THEN ''(Anulación)Provisión de progresivo''
                WHEN CM.CGM_TYPE = 303 THEN ''(Anulación)Progresivo otorgado''           ELSE CAST(CM.CGM_TYPE AS NVARCHAR(MAX))
             END AS CGM_TYPE_NAME
            ,CASE
                WHEN CM.CGM_STATUS = 0 AND CM.CGM_TYPE IN (0, 4)                         THEN ''Enviado''
                WHEN CM.CGM_STATUS = 0 AND CM.CGM_TYPE IN (200)                          THEN ''Solicitud pendiente''
                WHEN CM.CGM_STATUS = 0 AND CM.CGM_TYPE IN (5, 100, 103, 104)             THEN ''Pendiente recaudar''            
                WHEN CM.CGM_STATUS = 2 AND CM.CGM_TYPE IN (0, 4, 5, 300, 301, 302, 303)  THEN ''Finalizado''
                WHEN CM.CGM_STATUS = 2 AND CM.CGM_TYPE IN (100, 103, 104)                THEN ''Recaudado''
                WHEN CM.CGM_STATUS = 2 AND CM.CGM_TYPE IN (200)                          THEN ''Solicitud tramitada''            
                WHEN CM.CGM_STATUS = 1 AND CM.CGM_TYPE IN (200)                          THEN ''Solicitud cancelada''
                WHEN CM.CGM_STATUS = 1 AND CM.CGM_TYPE NOT IN (200)                      THEN ''Cancelado''            
                WHEN CM.CGM_STATUS = 3 AND CM.CGM_TYPE IN (0)                            THEN ''Finalizado con descuadre denominaciones''
                WHEN CM.CGM_STATUS = 3 AND CM.CGM_TYPE NOT IN (0)                        THEN ''Recaudado con descuadre denominaciones''            
                WHEN CM.CGM_STATUS = 4 AND CM.CGM_TYPE IN (0)                            THEN ''Finalizado con descuadre del monto''
                WHEN CM.CGM_STATUS = 4 AND CM.CGM_TYPE NOT IN (0)                        THEN ''Recaudado con descuadre total''            
                WHEN CM.CGM_STATUS IN (9, 11, 12)                                        THEN ''Finalizado''            
                WHEN CM.CGM_STATUS = 10                                                  THEN ''Recaudado''            
                WHEN CM.CGM_STATUS = 13                                                  THEN ''(Cancelado) Pendiente recaudar'' 
                ELSE ''---''
             END AS CGM_STATUS_NAME ' 
        SET @query += 
		   N',ISNULL(GU.GU_FULL_NAME, '''') AS GU_FULL_NAME
            ,ISNULL(GC.GU_FULL_NAME,'''') AS GU_CAGE_FULL_NAME
            ,ISNULL(ISNULL(CA.CT_NAME, CT.CT_NAME),TE.TE_NAME) AS CGM_NAME   
            ,CM.CGM_MC_COLLECTION_ID
            ,CM.CGM_TERMINAL_CASHIER_ID
            ,X01 
            ' + @cols + N'
            FROM CAGE_MOVEMENTS AS CM
            LEFT JOIN GUI_USERS AS GU ON GU.GU_USER_ID = CM.CGM_USER_CASHIER_ID  
            LEFT JOIN GUI_USERS AS GC ON GC.GU_USER_ID = CM.CGM_USER_CAGE_ID 
            LEFT JOIN CASHIER_TERMINALS AS CA ON CA.CT_CASHIER_ID = CM.CGM_TERMINAL_CASHIER_ID
            LEFT JOIN CASHIER_SESSIONS AS CS ON CS.CS_SESSION_ID = CM.CGM_CASHIER_SESSION_ID  
            LEFT JOIN CASHIER_TERMINALS AS CT ON CT.CT_CASHIER_ID = CS.CS_CASHIER_ID
            LEFT JOIN TERMINALS AS TE ON TE.TE_TERMINAL_ID = CM.CGM_TERMINAL_CASHIER_ID      
            LEFT JOIN (
                SELECT * FROM(
                              SELECT  
                                     CM.CGM_MOVEMENT_ID 
                                    ,CASE
                                        WHEN CM.CGM_TYPE IN(0, 1, 4, 5) THEN (CD.CMD_QUANTITY * CD.CMD_DENOMINATION * -1)                                    
                                        ELSE (CD.CMD_QUANTITY * CD.CMD_DENOMINATION)
                                     END AS CD_AMOUNT
                                    ,CD.CMD_ISO_CODE
                               FROM CAGE_MOVEMENTS AS CM
                                    LEFT JOIN CAGE_MOVEMENT_DETAILS AS CD 
                                    ON CM.CGM_MOVEMENT_ID = CD.CMD_MOVEMENT_ID 
                               WHERE  CD.CMD_ISO_CODE IS NOT NULL AND CD.CMD_QUANTITY > 0
                             ) AS PU        
                             PIVOT
                             (
                                SUM(CD_AMOUNT)
                                FOR CMD_ISO_CODE IN(X01' + @cols_type_1 + N')
                             ) AS PVT 
            ) AS AM ON CM.CGM_MOVEMENT_ID = AM.CGM_MOVEMENT_ID
   '     
   IF @cols_type_2 <> ''
     BEGIN
       SET @query += N' 
            LEFT JOIN (
                SELECT * FROM(
                            SELECT  
                                   CM.CGM_MOVEMENT_ID 
                                  ,CASE
                                      WHEN CM.CGM_TYPE IN(0, 1, 4, 5) THEN (CD.CMD_DENOMINATION * -1)                                    
                                      ELSE CD.CMD_DENOMINATION
                                   END AS CD_AMOUNT
                                  ,CD.CMD_ISO_CODE
                             FROM CAGE_MOVEMENTS AS CM
                                  LEFT JOIN CAGE_MOVEMENT_DETAILS AS CD 
                                  ON CM.CGM_MOVEMENT_ID = CD.CMD_MOVEMENT_ID 
                             WHERE  CD.CMD_ISO_CODE IS NOT NULL AND CD.CMD_QUANTITY IN(-1, -2, -100) AND CD.CMD_ISO_CODE NOT IN (''X01'')
                           ) AS PU        
                           PIVOT
                           (
                              SUM(CD_AMOUNT)
                              FOR CMD_ISO_CODE IN('+@cols_type_2+N')
                           ) AS PVT             
            ) AS AZ ON CM.CGM_MOVEMENT_ID = AZ.CGM_MOVEMENT_ID
       '
     END
   
   SET @query += N'       
        WHERE CM.CGM_TYPE NOT IN(200) AND CM.CGM_STATUS NOT IN(0, 1) AND CM.CGM_MOVEMENT_DATETIME >= ''' + CONVERT(VARCHAR, @pStarDateTime, 21) + N'''
        AND CM.CGM_MOVEMENT_DATETIME <  ''' + CONVERT(VARCHAR, @pEndDateTime, 21) + N'''   
        ORDER BY CM.CGM_MOVEMENT_ID    
   '       

   EXEC (@query) 
   
   CREATE TABLE #TABLE_MACHINE_ISO_CODE 
   ( 
      id INT Identity(1,1),
      iso_code VARCHAR(5)
   ) 
   
   INSERT INTO #TABLE_MACHINE_ISO_CODE
   SELECT TE.TE_ISO_CODE FROM(SELECT 
      CM.CMT_MOVEMENT_ID
     ,CASE
         WHEN @_activeDualCurrency = '1'
           THEN ISNULL(TE_ISO_CODE,@_isoCodeLocal)
         ELSE @_isoCodeLocal
      END AS TE_ISO_CODE 
      FROM #GET_CAGE_MOVEMENT_TYPE_TABLE  AS CM
      INNER JOIN MONEY_COLLECTIONS AS MCU
      ON CM.CMT_COLLECTION_ID = MCU.MC_COLLECTION_ID AND CM.CMT_TYPE IN (5, 104) 
      LEFT JOIN TERMINALS AS TE
      ON CM.CMT_TERMINAL_ID = TE.TE_TERMINAL_ID 
      WHERE ((MCU.MC_COLLECTED_BILL_AMOUNT IS NOT NULL AND MCU.MC_COLLECTED_BILL_AMOUNT > 0) OR
                            (MCU.MC_EXPECTED_BILL_AMOUNT IS NOT NULL AND MCU.MC_EXPECTED_BILL_AMOUNT > 0) OR
                            (MCU.MC_COLLECTED_COIN_AMOUNT IS NOT NULL AND MCU.MC_COLLECTED_COIN_AMOUNT > 0) OR
                            (MCU.MC_EXPECTED_COIN_AMOUNT IS NOT NULL AND MCU.MC_EXPECTED_COIN_AMOUNT > 0))
      )AS TE
   GROUP BY TE.TE_ISO_CODE 
      
   SET @_counter = 1		
   SET @_count_aux = (SELECT COUNT(*) FROM #TABLE_MACHINE_ISO_CODE) + 1
   
   WHILE NOT @_counter = @_count_aux
     BEGIN
    
      SELECT 
        @_isoCode  =  TIC.iso_code
      FROM #TABLE_MACHINE_ISO_CODE AS TIC
      WHERE TIC.id = @_counter
      
      SET @_count =(SELECT COUNT(*) FROM #TABLE_ISO_CODE AS tic 
                                    WHERE tic.iso_code = @_isoCode AND tic.data_type = 1)                   
      IF @_count = 0
      BEGIN
        SET @_queryColumns = N'ALTER TABLE #GET_CAGE_MOVEMENT_TYPE_TABLE ADD [' + @_isoCode + N'] MONEY '
        EXEC SP_EXECUTESQL @_queryColumns
        SET @colsFormat = @colsFormat + N',ISNULL('+@_isoCode+N', 0) AS '+@_isoCode + '' 
      END
      
      SET @_queryColumns = N'        
            UPDATE
              MTP
              SET
                MTP.'+@_isoCode+N' = (CASE WHEN MTP.CMT_TYPE = 104 THEN MCU.COLLECTED_AMOUNT
                                                        ELSE MCU.EXPECTED_AMOUNT END)     
            FROM #GET_CAGE_MOVEMENT_TYPE_TABLE AS MTP 
            INNER JOIN (SELECT 
                          CM.CMT_MOVEMENT_ID
                         ,MC_EXPECTED_BILL_AMOUNT + MC_EXPECTED_COIN_AMOUNT AS EXPECTED_AMOUNT
                         ,MC_COLLECTED_BILL_AMOUNT + MC_COLLECTED_COIN_AMOUNT AS COLLECTED_AMOUNT
                         ,CASE
                             WHEN '''+@_activeDualCurrency+N''' = ''1''
                               THEN ISNULL(TE_ISO_CODE,'''+@_isoCodeLocal+N''')
                             ELSE '''+@_isoCodeLocal+N'''
                          END AS TE_ISO_CODE 
                          FROM #GET_CAGE_MOVEMENT_TYPE_TABLE  AS CM
                          INNER JOIN MONEY_COLLECTIONS AS MCU
                          ON CM.CMT_COLLECTION_ID = MCU.MC_COLLECTION_ID AND CM.CMT_TYPE IN (5, 104) 
                          LEFT JOIN TERMINALS AS TE
                          ON CM.CMT_TERMINAL_ID = TE.TE_TERMINAL_ID 
                          WHERE ((MCU.MC_COLLECTED_BILL_AMOUNT IS NOT NULL AND MCU.MC_COLLECTED_BILL_AMOUNT > 0) OR
                                                (MCU.MC_EXPECTED_BILL_AMOUNT IS NOT NULL AND MCU.MC_EXPECTED_BILL_AMOUNT > 0) OR
                                                (MCU.MC_COLLECTED_COIN_AMOUNT IS NOT NULL AND MCU.MC_COLLECTED_COIN_AMOUNT > 0) OR
                                                (MCU.MC_EXPECTED_COIN_AMOUNT IS NOT NULL AND MCU.MC_EXPECTED_COIN_AMOUNT > 0))  
            )AS MCU ON MTP.CMT_MOVEMENT_ID = MCU.CMT_MOVEMENT_ID AND MCU.TE_ISO_CODE = '''+@_isoCode+N'''         
          '
       EXEC SP_EXECUTESQL @_queryColumns      
       SET @_counter = @_counter + 1
     END  
   
   SET @_queryColumns = N'   
     SELECT      
        CONVERT(char(11), CMT.CMT_MOVEMENT_DATETIME, 103) + CONVERT(char(8), CMT.CMT_MOVEMENT_DATETIME, 108) AS ''Fecha''
       ,CMT.CMT_CAGE_USER_NAME AS ''Usuario de Bóveda''           
       ,CASE WHEN CMT.CMT_USER_NAME LIKE ''SYS-%'' THEN '''' ELSE ISNULL(CMT.CMT_USER_NAME,'''') END  AS ''Origen/Destino''
       ,ISNULL(CMT.CMT_MACHINE_NAME,'''') AS ''Terminal''           
       ,CMT.CMT_TYPE_NAME AS ''Tipo de movimiento''
       ,CMT.CMT_STATUS_NAME AS ''Estado''
       '+@colsFormat+
     N',ISNULL(CMT.CMT_CHIPS_AMOUNT, 0) AS ''Fichas''   
     FROM #GET_CAGE_MOVEMENT_TYPE_TABLE  AS CMT
     '
   EXEC SP_EXECUTESQL @_queryColumns 
   
   DROP TABLE #GET_CAGE_MOVEMENT_TYPE_TABLE  
   DROP TABLE #TABLE_MACHINE_ISO_CODE
   DROP TABLE #TABLE_ISO_CODE     
  
END
GO

GRANT EXECUTE ON [dbo].[GetCageMovementType] TO [wggui] WITH GRANT OPTION 
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[BirthDayAlarm]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[BirthDayAlarm]
GO

CREATE PROCEDURE [dbo].[BirthDayAlarm]
                       @pAccountId BIGINT
                     , @pAlarmType INT OUTPUT
AS
BEGIN
      DECLARE @_birthday                  DATETIME
      DECLARE @_date_from_range           DATETIME
      DECLARE @_today                     DATETIME
      DECLARE @_gp_days_to_remove         INT
      DECLARE @_birthday_alarm_code       INT
      DECLARE @_near_birthday_alarm_code  INT
      DECLARE @_is_leap_year              INT

      SET @pAlarmType = 0
      SET @_today = DATEADD(dd, DATEDIFF(dd, 0, getdate()), 0)
      SET @_birthday_alarm_code = 2097160
      SET @_near_birthday_alarm_code = 2097161

      SELECT   @_birthday = DATEADD(dd, DATEDIFF(dd, 0, AC_HOLDER_BIRTH_DATE ), 0)
        FROM   ACCOUNTS 
       WHERE   AC_ACCOUNT_ID = @pAccountId
       
      SET @_birthday = DATEADD(yy, DATEDIFF(yy, @_birthday, @_today), @_birthday)
       
      IF MONTH(@_birthday) = 2 AND DAY(@_birthday) = 29
      BEGIN
        SELECT @_is_leap_year = DATEPART(MM, DATEADD(DD, 1, CAST((CAST(YEAR(@_today) AS VARCHAR(4)) + '0228') AS DATETIME))) 
        
        IF @_is_leap_year = 0
        BEGIN
          SET @_birthday = DATEADD(dd,-1,@_birthday)
        END
      END
      
      IF @_birthday = @_today
      BEGIN
        /*SEARCH IF ALARMS EXISTS*/
        IF NOT EXISTS ( SELECT   1 
                          FROM   ALARMS WITH (INDEX(IX_ALARMS_DATETIME_SEVERITY_SOURCE))
                         WHERE   AL_DATETIME   >= @_birthday
                           AND   AL_DATETIME   <  DATEADD(DD, 1, @_birthday)
                           AND   AL_SEVERITY   = 1
                           AND   AL_SOURCE_ID  = @pAccountId
                           AND   AL_ALARM_CODE = @_birthday_alarm_code )
        BEGIN
          SET @pAlarmType = 1
          
          RETURN
        END --IF NOT EXISTS
      END --IF @_birthday = @_today
      
      ELSE
      BEGIN
        SELECT   @_gp_days_to_remove = GP_KEY_VALUE
          FROM   GENERAL_PARAMS
         WHERE   GP_GROUP_KEY = 'Accounts' AND GP_SUBJECT_KEY = 'Player.BirthdayWarningDays'

        SET @_gp_days_to_remove = CAST(@_gp_days_to_remove AS INT)
        SET @_date_from_range = DATEADD(dd,-@_gp_days_to_remove,@_birthday)

        /*Today's date is within range*/

        IF  @_today >=  @_date_from_range
        AND @_today <  @_birthday
        
        BEGIN
          /*SEARCH IF ALARMS EXISTS*/
          IF NOT EXISTS (SELECT   1
                           FROM   ALARMS WITH (INDEX(IX_ALARMS_DATETIME_SEVERITY_SOURCE))
                          WHERE   AL_DATETIME   >= @_date_from_range 
                            AND   AL_DATETIME   < @_birthday
                            AND   AL_SEVERITY   = 1
                            AND   AL_SOURCE_ID  = @pAccountId 
                            AND   AL_ALARM_CODE = @_near_birthday_alarm_code )
          BEGIN
            SET @pAlarmType=2
          END --IF NOT EXISTS
        END --IF  @_today >=  @_date_from_range AND @_today <  @_birthday
      END --ELSE (IF @_birthday = @_today)
END
GO

GRANT EXECUTE ON [dbo].[BirthDayAlarm] TO [wggui] WITH GRANT OPTION
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'ReportShortfallExcess')
DROP PROCEDURE ReportShortfallExcess
GO

CREATE PROCEDURE [dbo].[ReportShortfallExcess]
  @pDateFrom DATETIME,  
  @pDateTo   DATETIME,
	@pGamingDayMode BIT
    
AS 
BEGIN
  --GUI USERS  
  SELECT   GUI_USERS.GU_USER_ID     as  USERID   
      , GU_USER_TYPE                as  TYPE_USER  
      , GUI_USERS.GU_USERNAME       as  USERNAME   
      , (CASE 
          WHEN GU_USER_TYPE = 0
            THEN GUI_USERS.GU_FULL_NAME 
            ELSE CT_NAME 
         END)                       as  FULLNAME   
      , GUI_USERS.GU_EMPLOYEE_CODE  as  EMPLOYEECODE   
      , X.CM_CURRENCY_ISO_CODE      as  CURRENCY   
      , X.FALTANTE                  as  FALTANTE   
      , X.SOBRANTE                  as  SOBRANTE   
      ,(X.SOBRANTE - X.FALTANTE)    as  DIFERENCIA  
      , X.CS_CASHIER_ID             as  CASHIERID
      , CT_NAME                     as  CASHIERNAME
      FROM  
      (          
      SELECT   CS_USER_ID  
             , CM_CURRENCY_ISO_CODE  
             , SUM (CASE WHEN CM_TYPE = 160 THEN CM_ADD_AMOUNT ELSE 0 END) as FALTANTE  
             , SUM (CASE WHEN CM_TYPE = 161 THEN CM_ADD_AMOUNT ELSE 0 END) as SOBRANTE               
             , CS_CASHIER_ID
        FROM   CASHIER_MOVEMENTS  WITH (INDEX (IX_CM_DATE_TYPE))  
  INNER JOIN   CASHIER_SESSIONS ON  CM_SESSION_ID  = CS_SESSION_ID      
       WHERE CASE WHEN @pGamingDayMode = 1 THEN CS_GAMING_DAY ELSE CM_DATE END >= @pDateFrom    
         AND CASE WHEN @pGamingDayMode = 1 THEN CS_GAMING_DAY ELSE CM_DATE END < @pDateTo  
         AND CM_TYPE IN ( 160  -- CASH_CLOSING_SHORT  
                        , 161)  -- CASH_CLOSING_OVER  
   GROUP BY CS_USER_ID  
        , CM_CURRENCY_ISO_CODE
        , CS_CASHIER_ID  
    ) X  
           
  INNER JOIN  GUI_USERS  
          ON  GU_USER_ID = X.CS_USER_ID
         AND (X.FALTANTE <>0  OR X.SOBRANTE<>0)  
         AND (GU_USER_TYPE=0  OR GU_USER_TYPE=6)
   LEFT JOIN  CASHIER_TERMINALS
          ON  CT_CASHIER_ID = X.CS_CASHIER_ID

    UNION ALL
    
    --BANK MOBILE USERS (this kind of users have national currency only)
    SELECT   MOBILE_BANKS.MB_ACCOUNT_ID                       as  USERID 
           , 1                                                as  TYPE_USER 
           , 'MB-' + CAST(MOBILE_BANKS.mb_account_id as CHAR) as  USERNAME
           , MOBILE_BANKS.MB_HOLDER_NAME                      as  FULLNAME
           , ''                                               as  EMPLOYEECODE
           , ''                                               as  CURRENCY
           , X2.FALTANTE                                      as  FALTANTE
           , X2.SOBRANTE                                      as  SOBRANTE 
           , (X2.SOBRANTE - X2.FALTANTE)                      as  DIFERENCIA
           , 0                                                as  CASHIERID
           , ''                                               as  CASHIERNAME
     FROM
     (
          
        SELECT   MBM_MB_ID
                 , SUM(CASE WHEN  MBM_TYPE IN (8,11)  THEN MBM_SUB_AMOUNT ELSE 0 END) AS FALTANTE
                 , SUM(CASE WHEN  MBM_TYPE IN (9,10)       THEN MBM_ADD_AMOUNT ELSE 0 END) AS SOBRANTE
            FROM   MB_MOVEMENTS MB1 WITH (INDEX (IX_MBM_DATETIME_TYPE)) 
      INNER JOIN   cashier_sessions 
              ON   MB1.mbm_cashier_session_id = cs_session_id          
          
           WHERE CASE WHEN @pGamingDayMode = 1 THEN CS_GAMING_DAY ELSE MB1.MBM_DATETIME END >= @pDateFrom   
             AND CASE WHEN @pGamingDayMode = 1 THEN CS_GAMING_DAY ELSE MB1.MBM_DATETIME END < @pDateTo   
             AND  ( ( MBM_TYPE IN (8,9) ) 
              OR    ( MBM_TYPE IN (11,10) 
                     
                     AND  NOT EXISTS 
                        (SELECT   MB2.MBM_MB_ID
                           FROM   MB_MOVEMENTS MB2 
                          WHERE   MB2.MBM_CASHIER_SESSION_ID = MB1.MBM_CASHIER_SESSION_ID 
                            AND   MB2.MBM_DATETIME > MB1.MBM_DATETIME 
                            AND   CASE WHEN @pGamingDayMode = 1 THEN CS_GAMING_DAY ELSE MB2.MBM_DATETIME END < @pDateTo 
                            AND   MB2.MBM_TYPE IN (3) ) ) ) 
        GROUP BY   MBM_MB_ID
        
     ) X2
    
      INNER JOIN   MOBILE_BANKS
              ON   X2.MBM_MB_ID = MOBILE_BANKS.MB_ACCOUNT_ID
            AND   (X2.FALTANTE<>0 OR X2.SOBRANTE<>0)
      
        ORDER BY   USERNAME
END
GO

GRANT EXECUTE ON [dbo].[ReportShortfallExcess] TO [wggui] WITH GRANT OPTION
GO


/******* TRIGGERS *******/

IF EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[Trigger_ttd_buffer_AGG]'))
BEGIN
	DROP TRIGGER Trigger_ttd_buffer_AGG
END
GO

CREATE TRIGGER [dbo].[Trigger_ttd_buffer_AGG] on [dbo].[terminals_time_disconnected]
   AFTER INSERT, UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	Declare @_date as datetime
	
	set @_date = GETDATE()
	
	IF EXISTS (SELECT gp_key_value FROM   general_params WHERE   gp_group_key = 'AGG' AND   gp_subject_key = 'Buffering.Enabled' AND gp_key_value = '1')
	BEGIN	

		INSERT INTO   BUFFER_TERMINALS_TIME_DISCONNECTED
					( bttd_site_id, bttd_terminal_id, bttd_working_day, bttd_timestamp )
			 SELECT   ttd_site_id, ttd_terminal_id, ttd_working_day, @_date
			   FROM   INSERTED
		LEFT OUTER JOIN BUFFER_TERMINALS_TIME_DISCONNECTED 
		   ON bttd_site_id = INSERTED.ttd_site_id and bttd_terminal_id = INSERTED.ttd_terminal_id and bttd_working_day = INSERTED.ttd_working_day
		WHERE BUFFER_TERMINALS_TIME_DISCONNECTED.bttd_site_id is null;

    END    
    SET NOCOUNT OFF;
END
GO


/******* GENERIC REPORT *******/

BEGIN
  DECLARE @rtc_form_id INT
  DECLARE @rtc_design_sheet NVARCHAR(MAX)
  DECLARE @rtc_report_name NVARCHAR(MAX)
  
  SET @rtc_report_name =     N'<LanguageResources><NLS09><Label>Cage movements for currency</Label></NLS09><NLS10><Label>Movimientos de bóveda por divisa</Label></NLS10></LanguageResources>'                                                              
  SET @rtc_design_sheet =    N'<ArrayOfReportToolDesignSheetsDTO>
                                 <ReportToolDesignSheetsDTO>
                                   <LanguageResources>
                                     <NLS09>
                                       <Label>Cage movements for currency</Label>
                                     </NLS09>
                                     <NLS10>
                                       <Label>Movimientos de bóveda por divisa</Label>
                                     </NLS10>
                                   </LanguageResources>
                                   <Columns>
                                     <ReportToolDesignColumn>
                                       <Code>Fecha</Code>
                                       <Width>300</Width>
                                       <EquityMatchType>Equality</EquityMatchType>
                                       <LanguageResources>
                                         <NLS09>
                                           <Label>Date</Label>
                                         </NLS09>
                                         <NLS10>
                                           <Label>Fecha</Label>
                                         </NLS10>
                                      </LanguageResources>
                                    </ReportToolDesignColumn>
                                    <ReportToolDesignColumn>
                                     <Code>Usuario de Bóveda</Code>
                                     <Width>300</Width>
                                     <EquityMatchType>Equality</EquityMatchType>
                                     <LanguageResources>
                                       <NLS09>
                                         <Label>Cash cage user</Label>
                                       </NLS09>
                                       <NLS10>
                                         <Label>Usuario de Bóveda</Label>
                                       </NLS10>
                                     </LanguageResources>
                                   </ReportToolDesignColumn>
                                   <ReportToolDesignColumn>
                                     <Code>Origen/Destino</Code>
                                     <Width>300</Width>
                                     <EquityMatchType>Equality</EquityMatchType>
                                     <LanguageResources>
                                       <NLS09>
                                         <Label>Source/Destination</Label>
                                       </NLS09>
                                       <NLS10>
                                         <Label>Origen/Destino</Label>
                                       </NLS10>
                                     </LanguageResources>
                                   </ReportToolDesignColumn>
                                   <ReportToolDesignColumn>
                                     <Code>Tipo de movimiento</Code>
                                     <Width>300</Width>
                                     <EquityMatchType>Equality</EquityMatchType>
                                     <LanguageResources>
                                       <NLS09>
                                         <Label>Type</Label>
                                       </NLS09>
                                       <NLS10>
                                         <Label>Tipo de movimiento</Label>
                                       </NLS10>
                                     </LanguageResources>
                                   </ReportToolDesignColumn>
                                   <ReportToolDesignColumn>
                                     <Code>Estado</Code>
                                     <Width>300</Width>
                                     <EquityMatchType>Equality</EquityMatchType>
                                     <LanguageResources>
                                       <NLS09>
                                         <Label>Status</Label>
                                       </NLS09>
                                       <NLS10>
                                         <Label>Estado</Label>
                                       </NLS10>
                                     </LanguageResources>
                                   </ReportToolDesignColumn>
                                   <ReportToolDesignColumn>
                                     <Code>Otros</Code>
                                     <Width>300</Width>
                                     <EquityMatchType>Contains</EquityMatchType>
                                     <LanguageResources>
                                       <NLS09>
                                         <Label>Other</Label>
                                       </NLS09>
                                       <NLS10>
                                         <Label>Otros</Label>
                                       </NLS10>
                                     </LanguageResources>
                                   </ReportToolDesignColumn>
                                   <ReportToolDesignColumn>
                                     <Code>Fichas</Code>
                                     <Width>300</Width>
                                     <EquityMatchType>Equality</EquityMatchType>
                                     <LanguageResources>
                                       <NLS09>
                                         <Label>Chips</Label>
                                       </NLS09>
                                     <NLS10>
                                       <Label>Fichas</Label>
                                     </NLS10>
                                   </LanguageResources>
                                 </ReportToolDesignColumn>
                               </Columns>
                             </ReportToolDesignSheetsDTO>
                           </ArrayOfReportToolDesignSheetsDTO>'
  
  IF EXISTS(SELECT 1 FROM report_tool_config WHERE rtc_store_name = N'GetCageMovementType_GR')
  BEGIN
      UPDATE [dbo].[REPORT_TOOL_CONFIG] 
      SET RTC_REPORT_NAME =         /*RTC_REPORT_NAME*/     @rtc_report_name,
          RTC_DESIGN_SHEETS =       /*RTC_DESIGN_SHEETS*/   @rtc_design_sheet
      WHERE RTC_STORE_NAME = N'GetCageMovementType_GR'
  END
END
GO

BEGIN
  DECLARE @rtc_form_id INT
  DECLARE @rtc_design_sheet NVARCHAR(MAX)
  DECLARE @rtc_report_name NVARCHAR(MAX)
  
  SET @rtc_report_name =      N'<LanguageResources><NLS09><Label>Meters collection to collection</Label></NLS09><NLS10><Label>Contadores recolección a recolección</Label></NLS10></LanguageResources>'
  SET @rtc_design_sheet =     N'<ArrayOfReportToolDesignSheetsDTO>
                                <ReportToolDesignSheetsDTO>
                                  <LanguageResources>
                                    <NLS09>
                                      <Label>Collection to collection</Label>
                                    </NLS09>
                                    <NLS10>
                                      <Label>Recolección a recolección</Label>
                                    </NLS10>
                                  </LanguageResources>
                                  <Columns>
                                    <ReportToolDesignColumn>
                                      <Code>Gaming Date</Code>
                                      <Width>300</Width>
                                      <EquityMatchType>Equality</EquityMatchType>
                                      <LanguageResources>
                                        <NLS09>
                                          <Label>Gaming Date</Label>
                                        </NLS09>
                                        <NLS10>
                                          <Label>Fecha de juego</Label>
                                        </NLS10>
                                      </LanguageResources>
                                    </ReportToolDesignColumn>
                                    <ReportToolDesignColumn>
                                      <Code>Machine Number</Code>
                                      <Width>300</Width>
                                      <EquityMatchType>Equality</EquityMatchType>
                                      <LanguageResources>
                                        <NLS09>
                                          <Label>Machine Number</Label>
                                        </NLS09>
                                        <NLS10>
                                          <Label>Numero de maquina</Label>
                                        </NLS10>
                                      </LanguageResources>
                                    </ReportToolDesignColumn>
                                    <ReportToolDesignColumn>
                                      <Code>Machine Denomination</Code>
                                      <Width>300</Width>
                                      <EquityMatchType>Equality</EquityMatchType>
                                      <LanguageResources>
                                        <NLS09>
                                          <Label>Machine Denomination</Label>
                                        </NLS09>
                                        <NLS10>
                                          <Label>Denominación de maquina</Label>
                                        </NLS10>
                                      </LanguageResources>
                                    </ReportToolDesignColumn>
                                    <ReportToolDesignColumn>
                                      <Code>fisico</Code>
                                      <Width>300</Width>
                                      <EquityMatchType>Contains</EquityMatchType>
                                      <LanguageResources>
                                        <NLS09>
                                          <Label>physical</Label>
                                        </NLS09>
                                        <NLS10>
                                          <Label>fisico</Label>
                                        </NLS10>
                                      </LanguageResources>
                                    </ReportToolDesignColumn>
                                    <ReportToolDesignColumn>
                                      <Code>Total contado en el soft count</Code>
                                      <Width>300</Width>
                                      <EquityMatchType>Equality</EquityMatchType>
                                      <LanguageResources>
                                        <NLS09>
                                          <Label>Total count in soft count</Label>
                                        </NLS09>
                                        <NLS10>
                                          <Label>Total contado en el soft count</Label>
                                        </NLS10>
                                      </LanguageResources>
                                    </ReportToolDesignColumn>
                                  </Columns>
                                </ReportToolDesignSheetsDTO>
                              </ArrayOfReportToolDesignSheetsDTO>'
  
  IF EXISTS(SELECT 1 FROM report_tool_config WHERE rtc_store_name = N'GetMetersCollectionToCollection_GR')
  BEGIN
      UPDATE [dbo].[REPORT_TOOL_CONFIG] 
      SET RTC_REPORT_NAME =         /*RTC_REPORT_NAME*/     @rtc_report_name,
          RTC_DESIGN_SHEETS =       /*RTC_DESIGN_SHEETS*/   @rtc_design_sheet
      WHERE RTC_STORE_NAME = N'GetMetersCollectionToCollection_GR'
  END
END
GO




