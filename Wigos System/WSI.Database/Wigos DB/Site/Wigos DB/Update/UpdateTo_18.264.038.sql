/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 263;

SET @New_ReleaseId = 264;
SET @New_ScriptName = N'UpdateTo_18.264.038.sql';
SET @New_Description = N'Added transaction id field in money_collections';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/******* TABLES *******/

/* MONEY_COLLECTIONS: Add column mc_wcp_transaction_id */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[money_collections]') and name = 'mc_wcp_transaction_id')
   ALTER TABLE dbo.money_collections ADD mc_wcp_transaction_id BIGINT NULL
GO

/* MONEY_COLLECTIONS, INDEX: IX_mc_terminal_stacker_transaction */
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[money_collections]') AND name = N'IX_mc_terminal_stacker_transaction')
BEGIN
   CREATE NONCLUSTERED INDEX [IX_mc_terminal_stacker_transaction] ON [dbo].[money_collections] 
   (
         [mc_terminal_id] ASC,
         [mc_stacker_id] ASC,
         [mc_wcp_transaction_id] ASC
   )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
END
GO

/******* RECORDS *******/

-- COLOMBIA STATE NAME
DECLARE @CountryISOCode2 AS VARCHAR(10)
SELECT @CountryISOCode2 = GP_KEY_VALUE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'RegionalOptions' AND GP_SUBJECT_KEY = 'CountryISOCode2'
IF (@CountryISOCode2 = 'CO')
BEGIN
	UPDATE GENERAL_PARAMS SET GP_KEY_VALUE = 'Departamento' WHERE GP_GROUP_KEY = 'Account.Fields'  AND GP_SUBJECT_KEY = 'State.Name'
	UPDATE GENERAL_PARAMS SET GP_KEY_VALUE = 'CO'     WHERE GP_GROUP_KEY = 'Account.DefaultValues' AND GP_SUBJECT_KEY = 'Country'
	UPDATE GENERAL_PARAMS SET GP_KEY_VALUE = '055'    WHERE GP_GROUP_KEY = 'Account.DefaultValues' AND GP_SUBJECT_KEY = 'DocumentType' -- C�dula de Ciudadan�a
END
GO

-- COLOMBIA REGIONS
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Amazonas','CO')
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Antioquia','CO')
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Arauca','CO')
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Atl�ntico','CO')
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Bol�var','CO')
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Boyac�','CO')
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Caldas','CO')
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Caquet�','CO')
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Casanare','CO')
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Cauca','CO')
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Cesar','CO')
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Choc�','CO')
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('C�rdoba','CO')
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Cundinamarca','CO')
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Guain�a','CO')
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Guaviare','CO')
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Huila','CO')
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('La Guajira','CO')
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Magdalena','CO')
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Meta','CO')
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Nari�o','CO')
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Norte de Santander','CO')
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Putumayo','CO')
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Quind�o','CO')
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Risaralda','CO')
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('San Andr�s y Providencia','CO')
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Santander','CO')
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Sucre','CO')
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Tolima','CO')
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Valle del Cauca','CO')
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Vaup�s','CO')
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Vichada','CO')
GO

-- COLOMBIA OCCUPATIONS
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('NO DISPONIBLE','0000001',1,1000,'CO')
GO

-- COLOMBIA IDENTIFICATION_TYPES
INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (54,1,100,'Otro','CO')
INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (55,1,1,'C�dula de Ciudadan�a','CO')
INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (56,1,2,'Pasaporte','CO')
INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (57,1,3,'Licencia de conducir','CO')
GO

/******* STORED *******/

ALTER PROCEDURE [dbo].[AccountPointsCache_CalculatePoints]
       @AccountId                        BIGINT,
       @DateFrom                         DATETIME,
       @DateTo                           DATETIME,
       @MovementId                       BIGINT, 
       @LastMovementId                   BIGINT OUTPUT, 
       @PointsGeneratedForLevel          MONEY  OUTPUT,
       @PointsDiscretionalForLevel       MONEY  OUTPUT,
       @PointsDiscretionalOnlyForRedeem  MONEY  OUTPUT,
       @PointsPromotionOnlyForRedeem     MONEY  OUTPUT
AS
BEGIN
  DECLARE @points_awarded                        AS INT;
  DECLARE @manually_added_points_for_level       AS INT;
  DECLARE @imported_points_for_level             AS INT;
  DECLARE @imported_points_history               AS INT;
  DECLARE @manually_added_points_only_for_redeem AS INT;
  DECLARE @imported_points_only_for_redeem       AS INT;
  DECLARE @promotion_point                       AS INT;
  DECLARE @cancel_promotion_point                AS INT;
 
  DECLARE @Sql             AS NVARCHAR(MAX);
  DECLARE @ParamDefinition AS NVARCHAR(MAX);
  DECLARE @Index           AS NVARCHAR(MAX);
  DECLARE @Where           AS NVARCHAR(MAX); 
  
  -- Points for level
     -- Generated
  SET @points_awarded                        = 36
     -- Discretional
  SET @manually_added_points_for_level       = 68
  SET @imported_points_for_level             = 72
  SET @imported_points_history               = 73
  -- Points only for redeem
     -- Discretional 
  SET @manually_added_points_only_for_redeem = 50
  SET @imported_points_only_for_redeem       = 71
     -- Promotion 
  SET @promotion_point                       = 60
  SET @cancel_promotion_point                = 61
  
  SET @LastMovementId = @MovementId

    SET @Index = 'IX_am_account_id_type_datetime'
    SET @Where = ' WHERE AM_ACCOUNT_ID  = @AccountId ' +
                 '   AND AM_TYPE IN (@points_awarded, @manually_added_points_for_level, @imported_points_for_level, @imported_points_history, ' +
                 '                   @manually_added_points_only_for_redeem, @imported_points_only_for_redeem, @promotion_point, @cancel_promotion_point) ' + 
                 '   AND AM_DATETIME >= CAST(''' + CAST(@DateFrom AS VARCHAR(50)) + ''' AS DATETIME) ' +
                 '   AND AM_DATETIME  < CAST(''' + CAST(@DateTo AS VARCHAR(50)) + ''' AS DATETIME) ' +
                 '   AND AM_MOVEMENT_ID > ' + CAST(@MovementId AS NVARCHAR(MAX)) 

  SET @Sql = '
  SELECT   @PointsGeneratedForLevel_out          = ISNULL(SUM (CASE WHEN AM_TYPE IN (@points_awarded)
                                                               THEN AM_ADD_AMOUNT 
                                                               ELSE 0 
                                                               END ), 0) 
         , @PointsDiscretionalForLevel_out       = ISNULL(SUM (CASE WHEN AM_TYPE IN (@manually_added_points_for_level, @imported_points_for_level, @imported_points_history)
                                                               THEN AM_ADD_AMOUNT 
                                                               ELSE 0 
                                                               END ), 0) 
         , @PointsDiscretionalOnlyForRedeem_out  = ISNULL(SUM (CASE WHEN AM_TYPE IN (@manually_added_points_only_for_redeem, @imported_points_only_for_redeem)
                                                               THEN AM_ADD_AMOUNT 
                                                               ELSE 0 
                                                               END ), 0)
         , @PointsPromotionOnlyForRedeem_out     = ISNULL(SUM (CASE WHEN AM_TYPE IN (@promotion_point, @cancel_promotion_point)
                                                               THEN AM_ADD_AMOUNT-AM_SUB_AMOUNT
                                                               ELSE 0 
                                                               END ), 0) 
         , @LastMovementId_out                   = ISNULL(MAX (AM_MOVEMENT_ID), @LastMovementId_out)
    FROM   ACCOUNT_MOVEMENTS WITH (INDEX (' + @Index + ')) ' 
  + @Where
 
SET @ParamDefinition = N'@AccountId                             BIGINT, 
                        @points_awarded                         INT, 
                        @manually_added_points_for_level        INT, 
                        @imported_points_for_level              INT, 
                        @imported_points_history                INT, 
                        @manually_added_points_only_for_redeem  INT, 
                        @imported_points_only_for_redeem        INT, 
                        @promotion_point                        INT, 
                        @cancel_promotion_point                 INT, 
                        @LastMovementId_out                     BIGINT OUTPUT, 
                        @PointsGeneratedForLevel_out            MONEY  OUTPUT, 
                        @PointsDiscretionalForLevel_out         MONEY  OUTPUT, 
                        @PointsDiscretionalOnlyForRedeem_out    MONEY  OUTPUT, 
                        @PointsPromotionOnlyForRedeem_out       MONEY  OUTPUT' 

EXEC sp_executesql @Sql, 
                   @ParamDefinition,
                   @AccountId                             = @AccountId, 
                   @points_awarded                        = @points_awarded,                        
                   @manually_added_points_for_level       = @manually_added_points_for_level,
                   @imported_points_for_level             = @imported_points_for_level,
                   @imported_points_history               = @imported_points_history, 
                   @manually_added_points_only_for_redeem = @manually_added_points_only_for_redeem, 
                   @imported_points_only_for_redeem       = @imported_points_only_for_redeem, 
                   @promotion_point                       = @promotion_point,
                   @cancel_promotion_point                = @cancel_promotion_point,
                   @LastMovementId_out                    = @LastMovementId                   OUTPUT, 
                   @PointsGeneratedForLevel_out           = @PointsGeneratedForLevel          OUTPUT,
                   @PointsDiscretionalForLevel_out        = @PointsDiscretionalForLevel       OUTPUT,
                   @PointsDiscretionalOnlyForRedeem_out   = @PointsDiscretionalOnlyForRedeem  OUTPUT,
                   @PointsPromotionOnlyForRedeem_out      = @PointsPromotionOnlyForRedeem     OUTPUT 

---------------------------------------------------------------------------------------------------------------------------------
-- Note:
-- @LastMovementId_out : Is the variable�s name of the output parameter of '@Sql execute'
-- When run 'EXEC sp_executesql' is finished, the value of @LastMovementId_out is assigned to @LastMovementId
---------------------------------------------------------------------------------------------------------------------------------

END  -- PROCEDURE [dbo].[AccountPointsCache_CalculatePoints]
GO

ALTER PROCEDURE [dbo].[AccountPointsCache_CalculateToday]
       @AccountId     BIGINT
AS
BEGIN

  DECLARE @update_history                                  AS BIT;
  
  DECLARE @apc_today                                       AS DATETIME;
  DECLARE @apc_days                                        AS INT;
	DECLARE @apc_history_points_generated_for_level          AS MONEY;
	DECLARE @apc_history_points_discretional_for_level       AS MONEY;
	DECLARE @apc_history_points_discretional_only_for_redeem AS MONEY;
	DECLARE @apc_history_points_promotion_only_for_redeem    AS MONEY;
	DECLARE @apc_today_points_generated_for_level            AS MONEY;
	DECLARE @apc_today_points_discretional_for_level         AS MONEY;
	DECLARE @apc_today_points_discretional_only_for_redeem   AS MONEY;
	DECLARE @apc_today_points_promotion_only_for_redeem      AS MONEY;
  DECLARE @apc_today_last_movement_id                      AS BIGINT;
  
  DECLARE @estudy_period                                   AS INT;
  DECLARE @date_from                                       AS DATETIME;
  DECLARE @now                                             AS DATETIME;
  DECLARE @today_opening                                   AS DATETIME;
  
  SET @update_history = 0;
  
  -- Get register from ACCOUNT_POINTS_CACHE
  SELECT   @apc_today                  = APC_TODAY 
         , @apc_days                   = APC_DAYS
         , @apc_today_last_movement_id = APC_TODAY_LAST_MOVEMENT_ID
    FROM   ACCOUNT_POINTS_CACHE 
   WHERE   APC_ACCOUNT_ID = @AccountId 
     
  -- GET General param: "PlayerTracking", "Levels.DaysCountingPoints"
  SET @estudy_period = (SELECT CAST(GP_KEY_VALUE AS INT) FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PlayerTracking' AND GP_SUBJECT_KEY = 'Levels.DaysCountingPoints')
     
  --_day_start_counting_points = Misc.TodayOpening().AddDays(-(_days_counting_points - 1));
  SET @now = GETDATE();
  SET @today_opening = dbo.Opening(0, @now)
  SET @date_from = DATEADD(DAY, -1*(@estudy_period-1), @today_opening);
    
  IF (   @today_opening <> ISNULL(@apc_today, DATEADD(d, -1, GETDATE()))   --> no hay registro o cambio de jornada.
      OR @estudy_period <> ISNULL(@apc_days, -1) )                         --> no hay registro o cambio en el periodo de estudio.
  BEGIN
    SET @update_history = 1;
  END   
    
  IF (@update_history = 1)
  BEGIN
    SET @apc_today_last_movement_id = 0;
    
    -- Get history data 
    EXEC AccountPointsCache_CalculatePoints @AccountId, @date_from, @today_opening, 
                                            @apc_today_last_movement_id, 
                                            @apc_today_last_movement_id                      OUTPUT, 
                                            @apc_history_points_generated_for_level          OUTPUT, 
                                            @apc_history_points_discretional_for_level       OUTPUT, 
                                            @apc_history_points_discretional_only_for_redeem OUTPUT, 
                                            @apc_history_points_promotion_only_for_redeem    OUTPUT
    --> Get today data
    EXEC AccountPointsCache_CalculatePoints @AccountId, @today_opening, @now, 
                                            @apc_today_last_movement_id, 
                                            @apc_today_last_movement_id                    OUTPUT, 
                                            @apc_today_points_generated_for_level          OUTPUT, 
                                            @apc_today_points_discretional_for_level       OUTPUT, 
                                            @apc_today_points_discretional_only_for_redeem OUTPUT, 
                                            @apc_today_points_promotion_only_for_redeem    OUTPUT
  
    DELETE  FROM ACCOUNT_POINTS_CACHE 
     WHERE  APC_ACCOUNT_ID = @AccountId 
   
    INSERT INTO ACCOUNT_POINTS_CACHE ( APC_ACCOUNT_ID, 
                                       APC_DAYS,       
                                       APC_HISTORY_POINTS_GENERATED_FOR_LEVEL,          
                                       APC_HISTORY_POINTS_DISCRETIONAL_FOR_LEVEL,
                                       APC_HISTORY_POINTS_DISCRETIONAL_ONLY_FOR_REDEEM,
                                       APC_HISTORY_POINTS_PROMOTION_ONLY_FOR_REDEEM,
                                       APC_TODAY, 
                                       APC_TODAY_POINTS_GENERATED_FOR_LEVEL,
                                       APC_TODAY_POINTS_DISCRETIONAL_FOR_LEVEL, 
                                       APC_TODAY_POINTS_DISCRETIONAL_ONLY_FOR_REDEEM, 
                                       APC_TODAY_POINTS_PROMOTION_ONLY_FOR_REDEEM,
                                       APC_TODAY_LAST_MOVEMENT_ID, 
                                       APC_TODAY_LAST_UPDATED ) 
                              VALUES ( @AccountId,     
                                       @estudy_period, 
                                       @apc_history_points_generated_for_level,              
                                       @apc_history_points_discretional_for_level, 
                                       @apc_history_points_discretional_only_for_redeem,     
                                       @apc_history_points_promotion_only_for_redeem,
                                       @today_opening, 
                                       @apc_today_points_generated_for_level, 
                                       @apc_today_points_discretional_for_level, 
                                       @apc_today_points_discretional_only_for_redeem, 
                                       @apc_today_points_promotion_only_for_redeem, 
                                       @apc_today_last_movement_id, 
                                       @now )  
  END
  ELSE
  BEGIN
    --> Get today data
    EXEC AccountPointsCache_CalculatePoints @AccountId, @today_opening, @now, 
                                            @apc_today_last_movement_id, 
                                            @apc_today_last_movement_id                    OUTPUT, 
                                            @apc_today_points_generated_for_level          OUTPUT, 
                                            @apc_today_points_discretional_for_level       OUTPUT, 
                                            @apc_today_points_discretional_only_for_redeem OUTPUT, 
                                            @apc_today_points_promotion_only_for_redeem    OUTPUT
    
    --update today
    UPDATE   ACCOUNT_POINTS_CACHE  
       SET   APC_TODAY                                     = @today_opening
           , APC_TODAY_POINTS_GENERATED_FOR_LEVEL          = APC_TODAY_POINTS_GENERATED_FOR_LEVEL          + @apc_today_points_generated_for_level         
           , APC_TODAY_POINTS_DISCRETIONAL_FOR_LEVEL       = APC_TODAY_POINTS_DISCRETIONAL_FOR_LEVEL       + @apc_today_points_discretional_for_level      
           , APC_TODAY_POINTS_DISCRETIONAL_ONLY_FOR_REDEEM = APC_TODAY_POINTS_DISCRETIONAL_ONLY_FOR_REDEEM + @apc_today_points_discretional_only_for_redeem
           , APC_TODAY_POINTS_PROMOTION_ONLY_FOR_REDEEM    = APC_TODAY_POINTS_PROMOTION_ONLY_FOR_REDEEM    + @apc_today_points_promotion_only_for_redeem   
           , APC_TODAY_LAST_MOVEMENT_ID                    = @apc_today_last_movement_id 
           , APC_TODAY_LAST_UPDATED                        = @now
     WHERE   APC_ACCOUNT_ID = @AccountId
     
  END
    
END  -- PROCEDURE [dbo].[AccountPointsCache_CalculateToday]
GO  
