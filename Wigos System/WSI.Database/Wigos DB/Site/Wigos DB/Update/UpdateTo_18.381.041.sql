/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 380;

SET @New_ReleaseId = 381;
SET @New_ScriptName = N'UpdateTo_18.381.041.sql';
SET @New_Description = N''; 

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/**** GENERAL PARAM *****/
IF NOT EXISTS(SELECT 1  FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'StaticWebHost'
                        AND GP_SUBJECT_KEY = 'RestrictSubnet'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('StaticWebHost', 'RestrictSubnet', '0')
END

IF NOT EXISTS(SELECT 1  FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'StaticWebHost'
                        AND GP_SUBJECT_KEY = 'SubnetRestrictionMask'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('StaticWebHost', 'SubnetRestrictionMask', '255.255.255.0')
END


/******* TABLES  *******/
GO

IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[cashier_movements_grouped_by_hour]') AND is_primary_key=1)
BEGIN TRY
ALTER TABLE [dbo].[cashier_movements_grouped_by_hour] ADD  CONSTRAINT [PK_CASHIER_MOVEMENTS_GROUPED_BY_HOUR] PRIMARY KEY CLUSTERED 
(
      [CM_DATE] ASC,
      [CM_TYPE] ASC,
      [CM_SUB_TYPE] ASC,
      [CM_CURRENCY_ISO_CODE] ASC,
      [CM_CAGE_CURRENCY_TYPE] ASC,
      [CM_CURRENCY_DENOMINATION] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
END TRY
BEGIN CATCH
SELECT   
        ERROR_NUMBER() AS ErrorNumber  
       ,ERROR_MESSAGE() AS ErrorMessage;  
END CATCH
GO


/******* RECORDS *******/

/******* PROCEDURES *******/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[iStatsCashierMovements]') AND type in (N'P', N'PC'))
         DROP PROCEDURE [dbo].[iStatsCashierMovements]
GO  

CREATE PROCEDURE [dbo].[iStatsCashierMovements]
  @pDateFrom DATETIME,  
  @pDateTo   DATETIME,
  @pGrouped NVARCHAR
  
AS
  
BEGIN  

         DECLARE @pConcatDateFrom DateTime
         DECLARE @pConcatDateTo DateTime
         DECLARE @pConcatDate2007 DateTime
         DECLARE @pOH INT
         DECLARE @NationalCurrency VARCHAR(3)
         
         SET @pConcatDateFrom = dbo.ConcatOpeningTime(0, @pDateFrom)
         SET @pConcatDateTo = dbo.ConcatOpeningTime(0, @pDateTo)
         SET @pConcatDate2007 = dbo.ConcatOpeningTime(0, '2007/01/01')
         SET @pOH = DATEDIFF (HOUR, '2007/01/01', @pConcatDate2007)
         
         -- Get national currency
         SELECT @NationalCurrency = GP_KEY_VALUE FROM GENERAL_PARAMS 
     WHERE GP_GROUP_KEY = 'RegionalOptions' 
       AND GP_SUBJECT_KEY = 'CurrencyISOCode'       
         
         
         SELECT   BASE_DATE
                    , TOTAL_CASH_IN
                    , TOTAL_OUTPUTS - PRIZE_TAX AS TOTAL_CASH_OUT
                    , PRIZE_TAX + TAXES AS PRIZE_TAXES
                    , TOTAL_CASH_IN - TOTAL_OUTPUTS - TAXES - CASH_IN_TAXES AS RESULT_CASHIER
           FROM   (
                            SELECT  
                                   CASE @pGrouped
                                            WHEN 'D' THEN DATEADD (DAY, DATEDIFF (HOUR, @pConcatDate2007, CM_DATE) / 24, '01/01/2007')
                                            WHEN 'W' THEN DATEADD (DAY, DATEDIFF (HOUR, @pConcatDate2007, CM_DATE) / 24, '01/01/2007')  
                                            WHEN 'M' THEN DATEADD (MONTH, DATEDIFF (MONTH, @pConcatDate2007, DATEADD(HOUR, -@pOH, CM_DATE)), '01/01/2007') 
                                   END AS BASE_DATE                      
                                    , ( SUM(CASE WHEN CM_TYPE IN (9, 28, 34, 35, 37, 39, 54, 55, 71, 85, 86, 142, 146) THEN CM_SUB_AMOUNT + CM_ADD_AMOUNT ELSE 0 END) 
                   + SUM(CASE WHEN CM_TYPE IN (78, 79, 92,  147, 148, 152, 153, 154, 155) THEN CM_SUB_AMOUNT ELSE 0 END)
                   + SUM(CASE WHEN (CM_TYPE = 1000000 AND (ISNULL(cm_currency_iso_code, '') = '' OR cm_currency_iso_code = @NationalCurrency)) THEN CM_SUB_AMOUNT + CM_ADD_AMOUNT ELSE 0 END) ) AS TOTAL_CASH_IN
                 , ( SUM(CASE WHEN CM_TYPE IN (8, 10, 36, 38, 40, 41, 69, 70, 143, 302) THEN CM_SUB_AMOUNT + CM_ADD_AMOUNT ELSE 0 END) 
                   + SUM(CASE WHEN (CM_TYPE = 2000000 AND (ISNULL(cm_currency_iso_code, '') = '' OR cm_currency_iso_code = @NationalCurrency)) THEN CM_SUB_AMOUNT + CM_ADD_AMOUNT ELSE 0 END)) AS TOTAL_OUTPUTS
                 , SUM(CASE WHEN CM_TYPE IN (6, 14) THEN CM_SUB_AMOUNT + CM_ADD_AMOUNT ELSE 0 END) AS PRIZE_TAX
                 , SUM(CASE WHEN CM_TYPE IN (95, 96, 101, 102) THEN CM_SUB_AMOUNT + CM_ADD_AMOUNT ELSE 0 END) AS TAXES
                 , SUM(CASE WHEN CM_TYPE IN (142, 146) THEN CM_SUB_AMOUNT + CM_ADD_AMOUNT ELSE 0 END) AS CASH_IN_TAXES
                                   FROM   CASHIER_MOVEMENTS_GROUPED_BY_HOUR WITH (INDEX([PK_cashier_movements_grouped_by_hour]))
                             WHERE   CM_DATE <  @pConcatDateTo
                                    AND   CM_DATE >= @pConcatDateFrom
                                    AND   CM_TYPE in (6, 8, 9, 10, 14, 28, 34, 35, 36, 37, 38, 39, 40, 41, 54, 55, 69, 70, 71, 78, 79, 92, 85, 86, 95, 96, 101, 102, 142, 143, 146, 147, 148, 152, 153, 154, 155, 302, 1000000, 2000000)
                                    AND   CM_SUB_TYPE = 0
                            GROUP BY 
                                   CASE @pGrouped
                                            WHEN 'D' THEN DATEADD (DAY, DATEDIFF (HOUR, @pConcatDate2007, CM_DATE) / 24, '01/01/2007')
                                            WHEN 'W' THEN DATEADD (DAY, DATEDIFF (HOUR, @pConcatDate2007, CM_DATE) / 24, '01/01/2007')  
                                            WHEN 'M' THEN DATEADD (MONTH, DATEDIFF (MONTH, @pConcatDate2007, DATEADD(HOUR, -@pOH, CM_DATE)), '01/01/2007') 
                                   END 
                           
                           ) X
          WHERE   TOTAL_CASH_IN > 0
                 OR   TOTAL_OUTPUTS > 0
                 OR   PRIZE_TAX   > 0
                 OR   TAXES > 0
         ORDER BY BASE_DATE

END
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetMonthlyNetwin]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetMonthlyNetwin]
GO

CREATE PROCEDURE [dbo].[GetMonthlyNetwin]
@pMonth int,
@pYear  int
AS
BEGIN

DECLARE @_aux_date AS DATETIME
DECLARE @_aux_date_init AS DATETIME
DECLARE @_aux_date_final AS DATETIME
DECLARE @_aux_site_name AS NVARCHAR(200)

-- Create temp table for whole report
CREATE TABLE #TT_MONTHLYNETWIN (
TSMH_MONTH            NVARCHAR(5), 
SITE_NAME             NVARCHAR(200), 
TE_NAME               NVARCHAR(200), 
TE_SERIAL_NUMBER      NVARCHAR(200),
BILL_IN               MONEY, 
TICKET_IN_REDIMIBLE   MONEY,
TICKET_OUT_REDIMIBLE  MONEY,
TOTAL_IN              MONEY,
TOTAL_OUT             MONEY,
REDEEM_TICKETS        MONEY,
TICKET_IN_PROMOTIONAL MONEY,
CANCELLED_CREDITS     MONEY,
JACKPOTS              MONEY,
LGA_NETWIN            MONEY)

--Obtain the INITIAL and FINAL date
set @_aux_date = CONVERT(DATETIME,CONVERT(NVARCHAR(4),@pYear) + '-' + CONVERT(NVARCHAR(2),@pMonth) + '-' +  '1')
set @_aux_date_init = CONVERT(NVARCHAR(25),DATEADD(dd,-(DAY(@_aux_date)-1),@_aux_date),101)
set @_aux_date_final = DATEADD(dd,-(DAY(DATEADD(mm,1,@_aux_date))),DATEADD(hh,23,DATEADD(mi,59,DATEADD(ss,59,DATEADD(mm,1,@_aux_date)))))

--Obtain site ID and site name
set @_aux_site_name = (SELECT GP_KEY_VALUE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Site' AND GP_SUBJECT_KEY = 'Identifier') + ' - ' + (SELECT GP_KEY_VALUE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Site' AND GP_SUBJECT_KEY = 'Name')

INSERT INTO #TT_MONTHLYNETWIN
SELECT    (CASE WHEN TSMH_GROUP_ID IS NULL THEN DATEPART(MM,TSMH_DATETIME) ELSE DATEPART(MM,TSMH_CREATED_DATETIME)END) AS TSMH_MONTH
        , @_aux_site_name AS SITE_NAME
        , TE_NAME
        , TE_SERIAL_NUMBER
        /*BILL IN*/
        , SUM( CASE WHEN TSMH_TYPE <> 20 THEN
                  - CASE WHEN (TSMH_METER_CODE = 11)                           THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               ELSE
                    CASE WHEN (TSMH_METER_CODE = 11)                           THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               END ) AS BILL_IN
        /*TICKET IN*/
        , SUM( CASE WHEN TSMH_TYPE <> 20 THEN
                  - CASE WHEN (TSMH_METER_CODE = 128)                          THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               ELSE
                    CASE WHEN (TSMH_METER_CODE = 128)                          THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               END ) AS TICKET_IN_REDIMIBLE
        /*TICKET OUT*/
        , SUM( CASE WHEN TSMH_TYPE <> 20 THEN
                  - CASE WHEN (TSMH_METER_CODE = 134)                          THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               ELSE
                    CASE WHEN (TSMH_METER_CODE = 134)                          THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               END ) AS TICKET_OUT_REDIMIBLE
        /*TOTAL IN = BILL IN + TICKET IN*/
        , SUM( CASE WHEN TSMH_TYPE <> 20 THEN
                  - CASE WHEN (TSMH_METER_CODE IN (11, 128))                   THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               ELSE
                    CASE WHEN (TSMH_METER_CODE IN (11, 128))                   THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               END ) AS TOTAL_IN
        /*TOTAL OUT = JACKPOTS + CANCELLED CREDITS + TICKET OUT*/
        , SUM( CASE WHEN TSMH_TYPE <> 20 THEN
                  - CASE WHEN (TSMH_METER_CODE IN (2, 3, 134))                 THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               ELSE
                    CASE WHEN (TSMH_METER_CODE IN (2, 3, 134))                 THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               END ) AS  TOTAL_OUT
        /*REDEEM TICKETS*/
        , SUM( CASE WHEN TSMH_TYPE <> 20 THEN 
                  - CASE WHEN (TSMH_METER_CODE = 128)                          THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
                  + CASE WHEN (TSMH_METER_CODE = 134)                          THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END 
               ELSE 
                    CASE WHEN (TSMH_METER_CODE = 128)                          THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
                  - CASE WHEN (TSMH_METER_CODE = 134)                          THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END 
               END ) AS  REDEEM_TICKETS
        /*TICKET IN PROMOTIONAL (redeem, no redeem)*/
        , SUM( CASE WHEN TSMH_TYPE <> 20 THEN
                  - CASE WHEN (TSMH_METER_CODE IN (130, 132))                  THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               ELSE
                    CASE WHEN (TSMH_METER_CODE IN (130, 132))                  THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               END ) AS TICKET_IN_PROMOTIONAL
        /*CANCELLED CREDITS*/
        , SUM( CASE WHEN TSMH_TYPE <> 20 THEN
                  - CASE WHEN (TSMH_METER_CODE = 3)                            THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               ELSE
                    CASE WHEN (TSMH_METER_CODE = 3)                            THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               END ) AS CANCELLED_CREDITS
        /*JACKPOTS*/
        , SUM( CASE WHEN TSMH_TYPE <> 20 THEN
                  - CASE WHEN (TSMH_METER_CODE = 2)                            THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               ELSE
                    CASE WHEN (TSMH_METER_CODE = 2)                            THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               END ) AS JACKPOTS
        /*LGA NETWIN*/
        , SUM( CASE WHEN TSMH_TYPE <> 20 THEN
                  - CASE WHEN (TSMH_METER_CODE IN (11, 128))                   THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
                  + CASE WHEN (TSMH_METER_CODE IN (3, 130, 132, 134))          THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               ELSE
                    CASE WHEN (TSMH_METER_CODE IN (11, 128))                   THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
                  - CASE WHEN (TSMH_METER_CODE IN (3, 130, 132, 134))          THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               END ) AS LGA_NETWIN
FROM TERMINALS
INNER JOIN TERMINAL_SAS_METERS_HISTORY WITH(INDEX(PK_terminal_sas_meters_history)) ON (TE_TERMINAL_ID = TSMH_TERMINAL_ID)
WHERE TSMH_DATETIME >= @_aux_date_init AND TSMH_DATETIME < @_aux_date_final
      AND TSMH_TYPE IN ( 20   /*MINCETUR_DAILY_REPORT*/
                       , 10   /*METER_RESET*/
                       , 11   /*METER_ROLLOVER*/
                       , 15   /*METER_SERVICE_RAM_CLEAR*/
                       , 16   /*METER_MACHINE_RAM_CLEAR*/
                         )
      AND TSMH_METER_CODE IN  ( 0   /*Total IN*/
                              , 1   /*Total OUT*/
                              , 2   /*Jackpots*/
                              , 3   /*Cancelled credits*/
                              , 11  /*Bill IN*/
                              , 128 /*Redimible Ticket IN*/
                              , 130 /*Ticket IN redeem promotional*/
                              , 132 /*Ticket IN no redeem promotional*/
                              , 134 /*Ticket OUT */
                              ) 
GROUP BY  TE_TERMINAL_ID, TE_NAME, TE_SERIAL_NUMBER, TE_MACHINE_SERIAL_NUMBER,
          (CASE WHEN TSMH_GROUP_ID IS NULL THEN DATEPART(MM,TSMH_DATETIME) ELSE DATEPART(MM,TSMH_CREATED_DATETIME) END)
ORDER BY TE_NAME
          
/*Add the TOTAL ROW*/
SELECT TSMH_MONTH,SITE_NAME, TE_NAME, TE_SERIAL_NUMBER, BILL_IN, TICKET_IN_REDIMIBLE, TICKET_OUT_REDIMIBLE, TOTAL_IN, TOTAL_OUT, REDEEM_TICKETS, TICKET_IN_PROMOTIONAL, CANCELLED_CREDITS, JACKPOTS, LGA_NETWIN
FROM #TT_MONTHLYNETWIN
UNION ALL
SELECT 'TOTAL' AS TSMH_MONTH, '' AS SITE_NAME, '' AS TE_NAME, '' AS TE_SERIAL_NUMBER, SUM(BILL_IN), SUM(TICKET_IN_REDIMIBLE), SUM(TICKET_OUT_REDIMIBLE), SUM(TOTAL_IN), SUM(TOTAL_OUT), SUM(REDEEM_TICKETS), SUM(TICKET_IN_PROMOTIONAL), SUM(CANCELLED_CREDITS), SUM(JACKPOTS), SUM(LGA_NETWIN)
FROM #TT_MONTHLYNETWIN
          
DROP TABLE #TT_MONTHLYNETWIN

END
GO

GRANT EXECUTE ON [dbo].[GetMonthlyNetwin] TO wggui WITH GRANT OPTION
GO

IF NOT EXISTS(SELECT * FROM report_tool_config WHERE rtc_store_name = 'GetMonthlyNetwin')
BEGIN
INSERT INTO [dbo].[REPORT_TOOL_CONFIG] VALUES /*RTC_REPORT_TOOL_ID     4*/
                                      /*RTC_FORM_ID*/        (11003,
                                      /*RTC_LOCATION_MENU*/   2, 
                                      /*RTC_REPORT_NAME*/     '<LanguageResources><NLS09><Label>Monthly Report NetWin</Label></NLS09><NLS10><Label>Reporte mensual de NetWin</Label></NLS10></LanguageResources>',
                                      /*RTC_STORE_NAME*/      'GetMonthlyNetwin',
                                      /*RTC_DESIGN_FILTER*/   '<ReportToolDesignFilterDTO><FilterType>MonthYear</FilterType></ReportToolDesignFilterDTO>',
                                      /*RTC_DESIGN_SHEETS*/   '<ArrayOfReportToolDesignSheetsDTO>
                                                                <ReportToolDesignSheetsDTO>
                                                                  <LanguageResources>
                                                                    <NLS09>
                                                                      <Label>Monthly Report NetWin</Label>
                                                                    </NLS09>
                                                                    <NLS10>
                                                                      <Label>Reporte mensual de NetWin</Label>
                                                                    </NLS10>
                                                                  </LanguageResources>
                                                                  <Columns>
                                                                    <ReportToolDesignColumn>
                                                                      <Code>TSMH_MONTH</Code>
                                                                      <Width>150</Width>
                                                                      <EquityMatchType>Equality</EquityMatchType>
                                                                      <LanguageResources>
                                                                        <NLS09>
                                                                          <Label>Month</Label>
                                                                        </NLS09>
                                                                        <NLS10>
                                                                          <Label>Mes</Label>
                                                                        </NLS10>
                                                                      </LanguageResources>
                                                                    </ReportToolDesignColumn>
                                                                    <ReportToolDesignColumn>
                                                                      <Code>TSMH_DAY</Code>
                                                                      <Width>300</Width>
                                                                      <EquityMatchType>Equality</EquityMatchType>
                                                                      <LanguageResources>
                                                                        <NLS09>
                                                                          <Label>Day</Label>
                                                                        </NLS09>
                                                                        <NLS10>
                                                                          <Label>D�a</Label>
                                                                        </NLS10>
                                                                      </LanguageResources>
                                                                    </ReportToolDesignColumn>
                                                                    <ReportToolDesignColumn>
                                                                      <Code>SITE_NAME</Code>
                                                                      <Width>300</Width>
                                                                      <EquityMatchType>Equality</EquityMatchType>
                                                                      <LanguageResources>
                                                                        <NLS09>
                                                                          <Label>Site Name</Label>
                                                                        </NLS09>
                                                                        <NLS10>
                                                                          <Label>Nombre de la sala</Label>
                                                                        </NLS10>
                                                                      </LanguageResources>
                                                                    </ReportToolDesignColumn>
                                                                    <ReportToolDesignColumn>
                                                                      <Code>TE_NAME</Code>
                                                                      <Width>300</Width>
                                                                      <EquityMatchType>Equality</EquityMatchType>
                                                                      <LanguageResources>
                                                                        <NLS09>
                                                                          <Label>Terminal name</Label>
                                                                        </NLS09>
                                                                        <NLS10>
                                                                          <Label>Nombre de la terminal</Label>
                                                                        </NLS10>
                                                                      </LanguageResources>
                                                                    </ReportToolDesignColumn>
                                                                    <ReportToolDesignColumn>
                                                                      <Code>TE_SERIAL_NUMBER</Code>
                                                                      <Width>300</Width>
                                                                      <EquityMatchType>Equality</EquityMatchType>
                                                                      <LanguageResources>
                                                                        <NLS09>
                                                                          <Label>Serial number</Label>
                                                                        </NLS09>
                                                                        <NLS10>
                                                                          <Label>N�mero de serie</Label>
                                                                        </NLS10>
                                                                      </LanguageResources>
                                                                    </ReportToolDesignColumn>
                                                                    <!--Bill IN-->
                                                                    <ReportToolDesignColumn>
                                                                      <Code>BILL_IN</Code>
                                                                      <Width>300</Width>
                                                                      <EquityMatchType>Equality</EquityMatchType>
                                                                      <LanguageResources>
                                                                        <NLS09>
                                                                          <Label>Bill IN</Label>
                                                                        </NLS09>
                                                                        <NLS10>
                                                                          <Label>Entrada de billetes</Label>
                                                                        </NLS10>
                                                                      </LanguageResources>
                                                                    </ReportToolDesignColumn>
                                                                    <!--Ticket IN-->
                                                                    <ReportToolDesignColumn>
                                                                      <Code>TICKET_IN_REDIMIBLE</Code>
                                                                      <Width>300</Width>
                                                                      <EquityMatchType>Equality</EquityMatchType>
                                                                      <LanguageResources>
                                                                        <NLS09>
                                                                          <Label>Ticket IN redimible</Label>
                                                                        </NLS09>
                                                                        <NLS10>
                                                                          <Label>Entrada ticket redimible</Label>
                                                                        </NLS10>
                                                                      </LanguageResources>
                                                                    </ReportToolDesignColumn>
                                                                    <!--Ticket OUT-->
                                                                    <ReportToolDesignColumn>
                                                                      <Code>TICKET_OUT_REDIMIBLE</Code>
                                                                      <Width>300</Width>
                                                                      <EquityMatchType>Equality</EquityMatchType>
                                                                      <LanguageResources>
                                                                        <NLS09>
                                                                          <Label>Ticket OUT redimible</Label>
                                                                        </NLS09>
                                                                        <NLS10>
                                                                          <Label>Salida ticket redimible</Label>
                                                                        </NLS10>
                                                                      </LanguageResources>
                                                                    </ReportToolDesignColumn>
                                                                    <!--Total IN-->
                                                                    <ReportToolDesignColumn>
                                                                      <Code>TOTAL_IN</Code>
                                                                      <Width>300</Width>
                                                                      <EquityMatchType>Equality</EquityMatchType>
                                                                      <LanguageResources>
                                                                        <NLS09>
                                                                          <Label>Total IN</Label>
                                                                        </NLS09>
                                                                        <NLS10>
                                                                          <Label>Total entradas</Label>
                                                                        </NLS10>
                                                                      </LanguageResources>
                                                                    </ReportToolDesignColumn>
                                                                    <!--Total OUT-->
                                                                    <ReportToolDesignColumn>
                                                                      <Code>TOTAL_OUT</Code>
                                                                      <Width>300</Width>
                                                                      <EquityMatchType>Equality</EquityMatchType>
                                                                      <LanguageResources>
                                                                        <NLS09>
                                                                          <Label>Total OUT</Label>
                                                                        </NLS09>
                                                                        <NLS10>
                                                                          <Label>Total salidas</Label>
                                                                        </NLS10>
                                                                      </LanguageResources>
                                                                    </ReportToolDesignColumn>
                                                                    <!--Redeem tickets-->
                                                                    <ReportToolDesignColumn>
                                                                      <Code>REDEEM_TICKETS</Code>
                                                                      <Width>300</Width>
                                                                      <EquityMatchType>Equality</EquityMatchType>
                                                                      <LanguageResources>
                                                                        <NLS09>
                                                                          <Label>Redeem tickets</Label>
                                                                        </NLS09>
                                                                        <NLS10>
                                                                          <Label>Tickets redimibles</Label>
                                                                        </NLS10>
                                                                      </LanguageResources>
                                                                    </ReportToolDesignColumn>
                                                                    <!--Promotional ticket IN-->
                                                                    <ReportToolDesignColumn>
                                                                      <Code>TICKET_IN_PROMOTIONAL</Code>
                                                                      <Width>300</Width>
                                                                      <EquityMatchType>Equality</EquityMatchType>
                                                                      <LanguageResources>
                                                                        <NLS09>
                                                                          <Label>Promotional ticket IN</Label>
                                                                        </NLS09>
                                                                        <NLS10>
                                                                          <Label>Entrada ticket promocional</Label>
                                                                        </NLS10>
                                                                      </LanguageResources>
                                                                    </ReportToolDesignColumn>
                                                                    <!--Cancelled credits-->
                                                                    <ReportToolDesignColumn>
                                                                      <Code>CANCELLED_CREDITS</Code>
                                                                      <Width>300</Width>
                                                                      <EquityMatchType>Equality</EquityMatchType>
                                                                      <LanguageResources>
                                                                        <NLS09>
                                                                          <Label>Cancelled credits</Label>
                                                                        </NLS09>
                                                                        <NLS10>
                                                                          <Label>Cr�ditos cancelados</Label>
                                                                        </NLS10>
                                                                      </LanguageResources>
                                                                    </ReportToolDesignColumn>
                                                                    <!--Jackpots-->
                                                                    <ReportToolDesignColumn>
                                                                      <Code>JACKPOTS</Code>
                                                                      <Width>300</Width>
                                                                      <EquityMatchType>Equality</EquityMatchType>
                                                                      <LanguageResources>
                                                                        <NLS09>
                                                                          <Label>Jackpots</Label>
                                                                        </NLS09>
                                                                        <NLS10>
                                                                          <Label>Jackpots</Label>
                                                                        </NLS10>
                                                                      </LanguageResources>
                                                                    </ReportToolDesignColumn>
                                                                    <!--LGA Netwin-->
                                                                    <ReportToolDesignColumn>
                                                                      <Code>LGA_NETWIN</Code>
                                                                      <Width>300</Width>
                                                                      <EquityMatchType>Equality</EquityMatchType>
                                                                      <LanguageResources>
                                                                        <NLS09>
                                                                          <Label>LGA Netwin</Label>
                                                                        </NLS09>
                                                                        <NLS10>
                                                                          <Label>LGA Netwin</Label>
                                                                        </NLS10>
                                                                      </LanguageResources>
                                                                    </ReportToolDesignColumn>
                                                                  </Columns>
                                                                </ReportToolDesignSheetsDTO>
                                                              </ArrayOfReportToolDesignSheetsDTO>',
                                      /*RTC_MAILING*/         0,
                                      /*RTC_STATUS*/          1,
                                      /*RTC_MODE*/            1)
END

GO

IF  EXISTS (SELECT 	1 FROM 	sys.objects WHERE 	object_id = OBJECT_ID(N'[dbo].[GetProfitReport]') AND type in (N'P', N'PC'))
BEGIN
  DROP PROCEDURE [dbo].[GetProfitReport]
END
GO

CREATE PROCEDURE [dbo].[GetProfitReport]
  @pFrom  DATETIME,
  @pTo    DATETIME
AS
BEGIN

-- Create temp table for whole report
CREATE TABLE #TT_PROFITREPORT (
ORDER_TYPE            INT,
TSMH_DATETIME         DATETIME, 
TE_PROVIDER_ID        NVARCHAR(200), 
TE_NAME               NVARCHAR(200), 
BILL_IN               MONEY, 
TICKET_OUT_REDIMIBLE  MONEY,
HANDPAYS              MONEY,
CANCELLED_CREDITS     MONEY,
PROFIT                MONEY)

DECLARE @closing_hour    int
DECLARE @closing_minutes int

SET @closing_hour = ISNULL((SELECT   gp_key_value
                              FROM   general_params
                             WHERE   gp_group_key   = 'WigosGUI'
                               AND   gp_subject_key = 'ClosingTime'), 6)

SET @closing_minutes = ISNULL((SELECT   gp_key_value
                                 FROM   general_params
                                WHERE   gp_group_key   = 'WigosGUI'
                                  AND   gp_subject_key = 'ClosingTimeMinutes'), 0)
  INSERT INTO #TT_PROFITREPORT
  SELECT  0 ORDER_TYPE   
        , CASE WHEN DATEADD(HOUR, @closing_hour, DATEADD(MINUTE, @closing_minutes, DATEADD(DAY, DATEDIFF(DAY, 0, TSMH_DATETIME), 0))) > TSMH_DATETIME
               THEN DATEADD(DAY, -1, DATEADD(HOUR, @closing_hour, DATEADD(MINUTE, @closing_minutes, DATEADD(DAY, DATEDIFF(day, 0, TSMH_DATETIME), 0))))
               ELSE DATEADD(HOUR, @closing_hour, DATEADD(MINUTE, @closing_minutes, DATEADD(DAY, DATEDIFF(DAY, 0, TSMH_DATETIME), 0)))
                END 
        , '--'  TE_PROVIDER_ID
        , '--'  TE_NAME
        /*BILL IN 11 128*/
        , SUM( CASE WHEN TSMH_TYPE <> 20 THEN
                  - CASE WHEN (TSMH_METER_CODE IN (11,128))       THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               ELSE
                    CASE WHEN (TSMH_METER_CODE IN (11,128))       THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               END ) AS BILL_IN
        /*TICKET OUT 134 */
        , SUM( CASE WHEN TSMH_TYPE <> 20 THEN
                  - CASE WHEN (TSMH_METER_CODE = 134)             THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               ELSE
                    CASE WHEN (TSMH_METER_CODE = 134)             THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               END ) AS TICKET_OUT_REDIMIBLE
        /*HANDPAYS 130 132*/
        , SUM( CASE WHEN TSMH_TYPE <> 20 THEN
                  - CASE WHEN (TSMH_METER_CODE IN (130,132))      THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               ELSE
                    CASE WHEN (TSMH_METER_CODE IN (130,132))      THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               END ) AS HANDPAYS
        /*CANCELLED CREDITS 3*/
        , SUM( CASE WHEN TSMH_TYPE <> 20 THEN
                  - CASE WHEN (TSMH_METER_CODE = 3)               THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               ELSE
                    CASE WHEN (TSMH_METER_CODE = 3)               THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               END ) AS CANCELLED_CREDITS
        /*PROFIT*/
        , SUM( CASE WHEN TSMH_TYPE <> 20 THEN
                  - CASE WHEN (TSMH_METER_CODE IN (11, 128))                   THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
                  + CASE WHEN (TSMH_METER_CODE IN (3, 130, 132, 134))          THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               ELSE
                    CASE WHEN (TSMH_METER_CODE IN (11, 128))                   THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
                  - CASE WHEN (TSMH_METER_CODE IN (3, 130, 132, 134))          THEN TSMH_METER_INCREMENT/100.00   ELSE 0 END
               END ) AS PROFIT
  FROM TERMINALS
  INNER JOIN TERMINAL_SAS_METERS_HISTORY WITH(INDEX(PK_terminal_sas_meters_history)) ON (TE_TERMINAL_ID = TSMH_TERMINAL_ID)
  WHERE TSMH_DATETIME >= @pFrom AND TSMH_DATETIME < @pTo
      AND TSMH_TYPE IN ( 20   /*MINCETUR_DAILY_REPORT*/
                       , 10   /*METER_RESET*/
                       , 11   /*METER_ROLLOVER*/
                       , 15   /*METER_SERVICE_RAM_CLEAR*/
                       , 16   /*METER_MACHINE_RAM_CLEAR*/
                         )
      AND TSMH_METER_CODE IN  ( 3   /*Cancelled credits*/
                              , 11  /*Bill IN*/
                              , 128 /*Redimible Ticket IN*/
                              , 130 /*Ticket IN redeem promotional*/
                              , 132 /*Ticket IN no redeem promotional*/
                              , 134 /*Ticket OUT */
                              ) 
  GROUP BY CASE WHEN DATEADD(HOUR, @closing_hour, DATEADD(MINUTE, @closing_minutes, DATEADD(DAY, DATEDIFF(DAY, 0, TSMH_DATETIME), 0))) > TSMH_DATETIME
                THEN DATEADD(DAY, -1, DATEADD(HOUR, @closing_hour, DATEADD(MINUTE, @closing_minutes, DATEADD(DAY, DATEDIFF(day, 0, TSMH_DATETIME), 0))))
                ELSE DATEADD(HOUR, @closing_hour, DATEADD(MINUTE, @closing_minutes, DATEADD(DAY, DATEDIFF(DAY, 0, TSMH_DATETIME), 0)))
                 END 
  ORDER BY CASE WHEN DATEADD(HOUR, @closing_hour, DATEADD(MINUTE, @closing_minutes, DATEADD(DAY, DATEDIFF(DAY, 0, TSMH_DATETIME), 0))) > TSMH_DATETIME
                THEN DATEADD(DAY, -1, DATEADD(HOUR, @closing_hour, DATEADD(MINUTE, @closing_minutes, DATEADD(DAY, DATEDIFF(day, 0, TSMH_DATETIME), 0))))
                ELSE DATEADD(HOUR, @closing_hour, DATEADD(MINUTE, @closing_minutes, DATEADD(DAY, DATEDIFF(DAY, 0, TSMH_DATETIME), 0)))
                 END 


SELECT  CASE TSMH_DATETIME WHEN '9999-12-31' THEN '' ELSE CONVERT(NVARCHAR(10),TSMH_DATETIME,103) END TSMH_DATETIME
      , TE_PROVIDER_ID
      , TE_NAME
      , BILL_IN
      , TICKET_OUT_REDIMIBLE
      , HANDPAYS
      , CANCELLED_CREDITS
      , PROFIT 
FROM (
  SELECT  ORDER_TYPE 
        , TSMH_DATETIME
        , TE_PROVIDER_ID
        , TE_NAME
        , BILL_IN
        , TICKET_OUT_REDIMIBLE
        , HANDPAYS
        , CANCELLED_CREDITS
        , PROFIT
  FROM #TT_PROFITREPORT
  UNION ALL
  SELECT  3 ORDER_TYPE
        , CAST('9999-12-31' AS DATETIME) TSMH_DATETIME
        , 'TOTAL' TE_PROVIDER_ID
        , '' TE_NAME
        , SUM(BILL_IN)  AS BILL_IN
        , SUM(TICKET_OUT_REDIMIBLE) AS  TICKET_OUT_REDIMIBLE
        , SUM(HANDPAYS) AS  HANDPAYS
        , SUM(CANCELLED_CREDITS)  AS  CANCELLED_CREDITS
        , SUM(PROFIT) AS  PROFIT 
  FROM #TT_PROFITREPORT
  WHERE ORDER_TYPE = 0) T
ORDER BY T.TSMH_DATETIME,T.ORDER_TYPE
        
DROP TABLE #TT_PROFITREPORT

END
GO

GRANT EXECUTE ON [dbo].[GetProfitReport] TO wggui WITH GRANT OPTION
GO

IF NOT EXISTS(SELECT * FROM report_tool_config WHERE rtc_store_name = 'GetProfitReport')
  BEGIN
  
  DECLARE @rtc_form_id INT

    SELECT @rtc_form_id = ISNULL(MAX(rtc_form_id),10999) + 1
    FROM report_tool_config

    INSERT INTO report_tool_config
              ( rtc_form_id 
              , rtc_location_menu
              , rtc_report_name
              , rtc_store_name
              , rtc_design_filter
              , rtc_design_sheets
              , rtc_mailing
              , rtc_status
              , rtc_mode_type
              )
    VALUES    ( @rtc_form_id , -- rtc_form_id - int
                2 , -- rtc_location_menu - int
                '<LanguageResources><NLS09><Label>Profit Report</Label></NLS09><NLS10><Label>Reporte Fairbet</Label></NLS10></LanguageResources>' , -- rtc_report_name - xml
                'GetProfitReport' , -- rtc_store_name - nvarchar(200)
                '<ReportToolDesignFilterDTO><FilterType>FromToDate</FilterType></ReportToolDesignFilterDTO>' , -- rtc_design_filter - xml
                '<ArrayOfReportToolDesignSheetsDTO><ReportToolDesignSheetsDTO><LanguageResources><NLS09><Label>Profit Report</Label></NLS09><NLS10><Label>Reporte Fairbet</Label></NLS10></LanguageResources><Columns><ReportToolDesignColumn><Code>TSMH_DATETIME</Code><Width>150</Width><EquityMatchType>Equality</EquityMatchType><LanguageResources><NLS09><Label>Session Date</Label></NLS09><NLS10><Label>Fecha Sesi�n</Label></NLS10></LanguageResources></ReportToolDesignColumn><ReportToolDesignColumn><Code>TE_PROVIDER_ID</Code><Width>300</Width><EquityMatchType>Equality</EquityMatchType><LanguageResources><NLS09><Label>Provider</Label></NLS09><NLS10><Label>Proveedor</Label></NLS10></LanguageResources></ReportToolDesignColumn><ReportToolDesignColumn><Code>TE_NAME</Code><Width>300</Width><EquityMatchType>Equality</EquityMatchType><LanguageResources><NLS09><Label>Terminal name</Label></NLS09><NLS10><Label>Nombre de la terminal</Label></NLS10></LanguageResources></ReportToolDesignColumn><!--Bill IN--><ReportToolDesignColumn><Code>BILL_IN</Code><Width>300</Width><EquityMatchType>Equality</EquityMatchType><LanguageResources><NLS09><Label>Total Bill IN</Label></NLS09><NLS10><Label>Entrada de billetes</Label></NLS10></LanguageResources></ReportToolDesignColumn><!--Ticket OUT--><ReportToolDesignColumn><Code>TICKET_OUT_REDIMIBLE</Code><Width>300</Width><EquityMatchType>Equality</EquityMatchType><LanguageResources><NLS09><Label>Total OUT Redimido</Label></NLS09><NLS10><Label>Salida ticket redimible</Label></NLS10></LanguageResources></ReportToolDesignColumn><!--Handpays--><ReportToolDesignColumn><Code>HANDPAYS</Code><Width>300</Width><EquityMatchType>Equality</EquityMatchType><LanguageResources><NLS09><Label>Manual Handpays</Label></NLS09><NLS10><Label>Pagos Manuales</Label></NLS10></LanguageResources></ReportToolDesignColumn><!--Cancelled credits--><ReportToolDesignColumn><Code>CANCELLED_CREDITS</Code><Width>300</Width><EquityMatchType>Equality</EquityMatchType><LanguageResources><NLS09><Label>Cancelled credits</Label></NLS09><NLS10><Label>Cr�ditos cancelados</Label></NLS10></LanguageResources></ReportToolDesignColumn><!--Profit--><ReportToolDesignColumn><Code>PROFIT</Code><Width>300</Width><EquityMatchType>Equality</EquityMatchType><LanguageResources><NLS09><Label>Profit</Label></NLS09><NLS10><Label>Profit</Label></NLS10></LanguageResources></ReportToolDesignColumn></Columns></ReportToolDesignSheetsDTO></ArrayOfReportToolDesignSheetsDTO>' , -- rtc_design_sheets - xml
                0 , -- rtc_mailing - int
                1 , -- rtc_status - int
                0  -- rtc_mode_type - int
              )        
  END 
  
  GO

/******* TRIGGERS *******/
