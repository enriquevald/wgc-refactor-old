/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 176;

SET @New_ReleaseId = 177;
SET @New_ScriptName = N'UpdateTo_18.177.035.sql';
SET @New_Description = N'TITO: table tito_tasks and SP TITO_SetTicketCanceled, Updated several SP for Gambling Tables, and New GP for Cage, Transfer Credit and Close Cashier Session.';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/******* TABLES  *******/

IF EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[bonuses]') and name = 'bns_trasnferred_promo_re')
  EXECUTE sp_rename N'dbo.bonuses.bns_trasnferred_promo_re', N'bns_transferred_promo_re', 'COLUMN' 
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[cage_movements]') and name = 'cgm_mc_collection_id')
  ALTER TABLE dbo.cage_movements ADD cgm_mc_collection_id bigint NULL
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[cage_sessions]') and name = 'cgs_session_name')
  ALTER TABLE dbo.cage_sessions ADD cgs_session_name [nvarchar](50) NULL
GO

--
-- drop pending_tickets_action
-- 
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pending_tickets_action]') AND type in (N'U'))
  DROP TABLE [dbo].[pending_tickets_action]
GO

--
-- Create tito_task
-- 
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tito_tasks]') AND type in (N'U'))
  DROP TABLE [dbo].[tito_tasks]
GO
CREATE TABLE [dbo].[tito_tasks](
      [tt_task_unique_id] [bigint] IDENTITY(1,1) NOT NULL,
      [tt_task_inserted] [datetime] NOT NULL,
      [tt_task_type] [int] NOT NULL,
      [tt_ticket_id] [bigint] NOT NULL,
CONSTRAINT [PK_tito_tasks] PRIMARY KEY CLUSTERED 
(
      [tt_task_unique_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tito_tasks] ADD  CONSTRAINT [DF_tito_tasks_tt_task_inserted]  DEFAULT (getdate()) FOR [tt_task_inserted]
GO

ALTER TABLE dbo.tickets ADD
      ti_db_inserted datetime NOT NULL CONSTRAINT DF_tickets_ti_db_inserted DEFAULT GETDATE()
GO
UPDATE tickets SET ti_db_inserted = ti_created_datetime
GO

/******* INDEXES  *******/

/******* STORED  *******/

----------------------------------------------------------------------------------------------------------------
-------------------------- FUNCTIONS
----------------------------------------------------------------------------------------------------------------

-- Clean older functions if exists

IF OBJECT_ID (N'dbo.SplitStringIntoTable', N'TF') IS NOT NULL
    DROP FUNCTION DBO.SplitStringIntoTable;                 
GO    

IF OBJECT_ID (N'dbo.GT_Calculate_DROP', N'FN') IS NOT NULL
    DROP FUNCTION dbo.GT_Calculate_DROP;                 
GO    

IF OBJECT_ID (N'dbo.GT_Calculate_WIN', N'FN') IS NOT NULL
    DROP FUNCTION dbo.GT_Calculate_WIN;                 
GO

IF OBJECT_ID (N'dbo.GT_Calculate_TIP', N'FN') IS NOT NULL
    DROP FUNCTION dbo.GT_Calculate_TIP;                 
GO    

IF OBJECT_ID (N'dbo.ApplyExchange', N'FN') IS NOT NULL
    DROP FUNCTION DBO.ApplyExchange;                 
GO  

IF OBJECT_ID (N'dbo.GT_Base_Report_Data', N'P') IS NOT NULL
    DROP PROCEDURE dbo.GT_Base_Report_Data;                 
GO    

IF OBJECT_ID (N'dbo.GT_Chips_Operations', N'P') IS NOT NULL
    DROP PROCEDURE dbo.GT_Chips_Operations;                 
GO  

IF OBJECT_ID (N'dbo.GT_Session_Information', N'P') IS NOT NULL
    DROP PROCEDURE dbo.GT_Session_Information;                 
GO  

IF OBJECT_ID (N'dbo.GT_Cancellations', N'P') IS NOT NULL
    DROP PROCEDURE dbo.GT_Cancellations;                 
GO  
 
-- Create functions

    -- PURPOSE: Apply exchange to national currency
    --
    -- PARAMS:
    --     - INPUT:
    --           - @pAmount
    --           - @pIsoCode
    --
    -- RETURNS:
    --     - DECIMAL(16,8): Operation result
    --

CREATE FUNCTION ApplyExchange
(@pAmount  DECIMAL(16,8),
  @pIsoCode VARCHAR(3))
RETURNS DECIMAL(16,8)
AS
BEGIN

      DECLARE @Exchanged      DECIMAL(18,8)
      DECLARE @Change         DECIMAL(18,8)
      DECLARE @NumDecimals INT
      
      IF(@pIsoCode IS NULL)
            SET @Exchanged = @pAmount;
      ELSE
      BEGIN
            SELECT 
                  @Change = CE_CHANGE,
                  @NumDecimals = CE_NUM_DECIMALS
            FROM CURRENCY_EXCHANGE
            WHERE CE_TYPE = 0
                    AND CE_CURRENCY_ISO_CODE = @pIsoCode

            SET @NumDecimals = ISNULL(@NumDecimals, 0)     
            SET @Exchanged = ROUND(ISNULL(@Change * @pAmount, 0), @NumDecimals, 1)   

      END
      
      
      RETURN @Exchanged
END -- ApplyExchange
GO
GRANT EXECUTE ON [dbo].[ApplyExchange] TO [wggui]
GO

/*
   PURPOSE: SPLITS STRING INTO TABLE
   
   PARAMS: @pString: String list of items separate by same delimiter. Ex: '1,2,3,4,5,6'
           @pDelimiter: Separator char on the string list. Ex: ','
           @pTrimItems: Default 1 - remove left and right spaces on each item 
   
   RETURN:  TABLE with the selected values in row format: SST_ID, SST_VALUE
   
   NOTES: Select in must take the SST_VALUE to compare the item. 
          Ex: WHERE FIELD_NAME IN ( SELECT SST_VALUE FROM dbo.SplitStringIntoTable(@ListItemsString,@Separator, DEFAULT) )
*/
CREATE FUNCTION SplitStringIntoTable 
(
    @pString VARCHAR(4096),
    @pDelimiter VARCHAR(10),
    @pTrimItems BIT = 1
)
RETURNS 
@ReturnTable TABLE 
(
    [SST_ID] [INT] IDENTITY(1,1) NOT NULL,
    [SST_VALUE] [VARCHAR](50) NULL
)

AS
BEGIN
         
        DECLARE @_ISPACES INT
        DECLARE @_PART VARCHAR(50)

        -- INITIALIZE SPACES
        SELECT @_ISPACES = CHARINDEX(@pDelimiter,@pString,0)
        WHILE @_ISPACES > 0

        BEGIN
            SELECT @_PART = SUBSTRING(@pString,0,CHARINDEX(@pDelimiter,@pString,0))

            IF @pTrimItems = 1
             SET @_PART = LTRIM(RTRIM(@_PART))

            INSERT INTO @ReturnTable(SST_VALUE)
            SELECT @_PART

            SELECT @pString = SUBSTRING(@pString, @_ISPACES + LEN(@pDelimiter), LEN(@pString) - CHARINDEX(@pDelimiter, @pString, 0))

            SELECT @_ISPACES = CHARINDEX(@pDelimiter,@pString,0)
        END

        IF LEN(@pString) > 0
            INSERT INTO @ReturnTable
            SELECT CASE WHEN @pTrimItems = 1 THEN LTRIM(RTRIM(@pString)) ELSE @pString END

    RETURN 
END
GO

/*
   PURPOSE: Calculates the DROP field in a gaming table or table type
   
   PARAMS:  @pOwnSalesAmount: Chips sales
            @pExternalSalesAmount: Chips sales from other cashiers
   
   RETURN:  Money: Drop
*/
CREATE FUNCTION GT_Calculate_DROP ( @pOwnSalesAmount AS MONEY, @pExternalSalesAmount AS MONEY ) RETURNS MONEY
  BEGIN 
    
    RETURN @pOwnSalesAmount + @pExternalSalesAmount
  END
GO

/*
   PURPOSE: Calculates the WIN field in a gaming table or table type
   
   PARAMS:  @pPurchaseAmount:   Chips bought 
            @pCollectedAmount:  Money collected
            @pInitialAmount:    Chips sended by CAGE
   
   RETURN:  Money: Win
*/
CREATE FUNCTION GT_Calculate_WIN ( @pFinalAmount AS MONEY, @pInitialAmount AS MONEY, @pCollectedAmount AS MONEY ) RETURNS MONEY
  BEGIN 
    
    RETURN (@pFinalAmount - @pInitialAmount) + @pCollectedAmount
  END
GO

/*
   PURPOSE: Calculate the TIP field in a gaming table or table type
   
   PARAMS:  @pTipsAmount: Amount of tips
   
   RETURN:  Money: TIP
*/
CREATE FUNCTION GT_Calculate_TIP ( @pTipsAmount AS MONEY ) RETURNS MONEY
  BEGIN 
    
    RETURN @pTipsAmount
  END
GO

----------------------------------------------------------------------------------------------------------------
-------------------------- STORED PROCEDURE
----------------------------------------------------------------------------------------------------------------

/*
----------------------------------------------------------------------------------------------------------------
BASIC REPORT QUERY FOR GAMING TABLES

Version  Date           User     Description
----------------------------------------------------------------------------------------------------------------
1.0.1    24-DEC-2013    RMS      Using GAMING_TABLE_SESSION

Requeriments:
   -- Functions:
         dbo.GT_Calculate_DROP( Amount )
         dbo.GT_Calculate_WIN ( AmountAdded, AmountSubtracted )
         dbo.GT_Calculate_TIP ( Amount )

Parameters:
   -- BASE TYPE:      Indicates if data is based on table types (0) or the tables (1).
   -- TIME INTERVAL:  Range of time to group data between days (0), months (1) and years (2).
   -- DATE FROM:      Start date for data collection. (If NULL then use first available date).
   -- DATE TO:        End date for data collection. (If NULL then use current date).
   -- ONLY ACTIVITY:  If true (1) results will only show tables or table types with activity inside the dates range,
                      else (0) results will include all tables or table types.
   -- ORDER BY:       (0) to sort by date (interval) descendant, (1) to sort by table type or table identifier.   
   -- VALID TYPES:    A string that contains the valid table types to list.
                      
Process: (From inside to outside)
   
   1- Core Query:
         Query all game table session data linked to the cashier sessions that where linked with a gaming_table and/or gaming table type.
   
   2- Specific report query:
         Here we group by base type and time interval and includes the session information for the interval, generating a temporary
         table.
         
   3- Filter activity:
         Join the temporary table with the time intervals table generated at first doing a left join to show all data, else an inner
         join to filter tables or table types without activity.

   4- Drop the temporary table.
   
 Results:
   
   We can create 6 different report, based on table types or tables and grouped by days, months and years.
----------------------------------------------------------------------------------------------------------------   
*/

CREATE PROCEDURE [dbo].[GT_Base_Report_Data] 
 ( @pBaseType INTEGER
  ,@pTimeInterval INTEGER
  ,@pDateFrom DATETIME 
  ,@pDateTo DATETIME
  ,@pOnlyActivity INTEGER
  ,@pOrderBy INTEGER
  ,@pValidTypes VARCHAR(4096)
)
AS
BEGIN

-- DECLARATIONS
   -- @_DAYS_AND_TABLES:    A temporary table with all intervals and all base type selected (used to show base types without activity).
   DECLARE @_DAYS_AND_TABLES TABLE(DATE_TIME DATETIME, TABLE_IDENTIFIER_VALUE BIGINT, TABLE_IDENT_NAME VARCHAR(50), TABLE_TYPE_IDENT BIGINT, TABLE_TYPE_NAME VARCHAR(50))
   -- @_DAY_VAR:            Variable to create the intervals
   DECLARE @_DAY_VAR DATETIME

-- PARAMETERS
   DECLARE @_BASE_TYPE        AS   INTEGER -- 0 BY TABLE TYPE, 1 BY TABLE 
   DECLARE @_TIME_INTERVAL    AS   INTEGER -- -1, 0 DAYS, 1 MONTH, 2 YEAR
   DECLARE @_DATE_FROM        AS   DATETIME
   DECLARE @_DATE_TO          AS   DATETIME
   DECLARE @_ONLY_ACTIVITY    AS   INTEGER -- 0 SHOW ALL, 1 SHOW ONLY WITH ACTIVITY
   DECLARE @_ORDER_BY         AS   INTEGER -- 0 BY INTERVAL DESC, 1 BY IDENTIFIER ASC
   DECLARE @_DELIMITER        AS   CHAR(1)
   DECLARE @_TYPES_TABLE      TABLE(SST_ID INT, SST_VALUE VARCHAR(50)) -- TO STORE THE VALID GAMING TABLE TYPES 
   
----------------------------------------------------------------------------------------------------------------

-- Initialzitation --
SET @_BASE_TYPE        =   @pBaseType
SET @_TIME_INTERVAL    =   @pTimeInterval
SET @_DATE_FROM        =   @pDateFrom
SET @_DATE_TO          =   @pDateTo
SET @_ONLY_ACTIVITY    =   @pOnlyActivity
SET @_ORDER_BY         =   @pOrderBy
SET @_DELIMITER        =   ','

----------------------------------------------------------------------------------------------------------------

-- CHECK DATE PARAMETERS
IF @_DATE_FROM IS NULL
BEGIN
   -- IF DATE FROM IS NULL, USER FIRST POSIBLE DATE
   SET @_DATE_FROM = CAST('' AS DATETIME)
   SET @pDateFrom  = CAST('' AS DATETIME)
END
IF @_DATE_TO IS NULL
BEGIN
   -- IF DATE TO IS NULL, QUERY UNTIL CURRENT DATE
   SET @_DATE_TO = CAST(GETDATE() AS DATETIME)
   SET @pDateTo  = CAST(GETDATE() AS DATETIME)
END

-- ASSIGN TYPES PARAMETER INTO TABLE
INSERT INTO @_TYPES_TABLE SELECT * FROM dbo.SplitStringIntoTable(@pValidTypes, @_DELIMITER, DEFAULT)

IF @_TIME_INTERVAL <> -1 
 BEGIN
   -- INTERVALS DATES AND TABLE PREPARATION

   -- PREPARE DATES RANGE DEPENDING ON TIME INTERVAL
   SET @_DATE_FROM = CASE 
                        WHEN @_TIME_INTERVAL = 0 THEN -- BY DAY
                           CAST(@_DATE_FROM AS DATETIME) 
                        WHEN @_TIME_INTERVAL = 1 THEN -- BY MONTH
                           CAST(CAST(YEAR(@_DATE_FROM) AS VARCHAR(4)) + RIGHT('0' + CAST(MONTH(@_DATE_FROM) AS VARCHAR(2)), 2) + '01' AS DATETIME)
                        WHEN @_TIME_INTERVAL = 2 THEN -- BY YEAR
                           CAST(CAST(YEAR(@_DATE_FROM) AS VARCHAR(4)) + '0101' AS DATETIME)
                     END

   SET @_DATE_TO = CASE 
                        WHEN @_TIME_INTERVAL = 0 THEN -- BY DAY
                           DATEADD(DAY, 1, CAST(@_DATE_TO AS DATETIME))
                        WHEN @_TIME_INTERVAL = 1 THEN -- BY MONTH
                           DATEADD(MONTH, 1, CAST(CAST(YEAR(@_DATE_TO) AS VARCHAR(4)) + RIGHT('0' + CAST(MONTH(@_DATE_TO) AS VARCHAR(2)), 2) + '01' AS DATETIME))
                        WHEN @_TIME_INTERVAL = 2 THEN -- BY YEAR
                           DATEADD(YEAR, 1, CAST(CAST(YEAR(@_DATE_TO) AS VARCHAR(4)) + '0101' AS DATETIME))
                     END
              
   -- PREPARE THE TIME INTERVALS TABLE
   SET @_DAY_VAR = @_DATE_FROM
   WHILE @_DAY_VAR < @_DATE_TO
   BEGIN

         -- SET THE LINK FIELD FOR THE RESULTS 
         IF @_BASE_TYPE = 0
          BEGIN
            -- LINK WITH TABLE TYPES
            INSERT INTO @_DAYS_AND_TABLES 
                 SELECT   CAST(@_DAY_VAR AS DATETIME), X.GTT_GAMING_TABLE_TYPE_ID, X.GTT_NAME AS TABLE_IDENT, NULL, NULL
                   FROM   GAMING_TABLES_TYPES AS X
                  WHERE   X.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )  
          END 
         ELSE
          BEGIN
            -- LINK WITH TABLES
            INSERT INTO @_DAYS_AND_TABLES 
                 SELECT   CAST(@_DAY_VAR AS DATETIME), X.GT_GAMING_TABLE_ID, X.GT_NAME AS TABLE_IDENT, Z.GTT_GAMING_TABLE_TYPE_ID, Z.GTT_NAME 
                   FROM   GAMING_TABLES AS X
              LEFT JOIN   GAMING_TABLES_TYPES AS Z
                     ON   Z.GTT_GAMING_TABLE_TYPE_ID = X.GT_TYPE_ID
                  WHERE   Z.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
          END
          
          -- SET INCREMENT
          SET @_DAY_VAR = CASE 
                               WHEN @_TIME_INTERVAL = 0 THEN DATEADD(DAY,1,@_DAY_VAR)     -- DAY
                               WHEN @_TIME_INTERVAL = 1 THEN DATEADD(MONTH,1,@_DAY_VAR)   -- MONTH
                               WHEN @_TIME_INTERVAL = 2 THEN DATEADD(YEAR,1,@_DAY_VAR)    -- YEAR
                          END
   END

END -- IF INTERVALS <> - 1
ELSE
BEGIN
   
   -- SET THE LINK FIELD FOR THE RESULTS WITHOUT INTERVALS
   IF @_BASE_TYPE = 0
    BEGIN
      -- LINK WITH TABLE TYPES
      INSERT INTO @_DAYS_AND_TABLES 
           SELECT   NULL, X.GTT_GAMING_TABLE_TYPE_ID, X.GTT_NAME AS TABLE_IDENT, NULL, NULL
             FROM   GAMING_TABLES_TYPES AS X
            WHERE   X.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE ) 
    END 
   ELSE
    BEGIN
      -- LINK WITH TABLES
      INSERT INTO @_DAYS_AND_TABLES 
           SELECT   NULL, X.GT_GAMING_TABLE_ID, X.GT_NAME AS TABLE_IDENT, Z.GTT_GAMING_TABLE_TYPE_ID, Z.GTT_NAME 
             FROM   GAMING_TABLES AS X
        LEFT JOIN   GAMING_TABLES_TYPES AS Z
               ON   Z.GTT_GAMING_TABLE_TYPE_ID = X.GT_TYPE_ID
            WHERE   Z.GTT_GAMING_TABLE_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )   
    END
   
 END
  
-- SELECT INTO TEMPORARY TABLE WITH ALL DATA
-- SPECIFIC REPORT QUERY 
  SELECT   X.TABLE_IDENTIFIER   
         , X.TABLE_NAME
         , X.TABLE_TYPE
         , X.TABLE_TYPE_NAME
         , SUM(X.S_DROP) TOTAL_DROP         
         , SUM(X.S_WIN)  TOTAL_WIN         
         , ISNULL(SUM(X.S_TIP),0)  TOTAL_TIP         
         , CASE WHEN SUM(X.S_DROP) = 0 THEN 0 ELSE SUM(X.S_WIN) / SUM(X.S_DROP) * 100 END AS WIN_DROP         
         , CASE WHEN SUM(X.S_DROP) = 0 THEN 0 ELSE SUM(X.S_TIP) / SUM(X.S_DROP) * 100 END AS TIP_DROP         
         , SUM(X.S_WIN) + SUM(X.S_TIP) AS WIN_TIPS
         , X.CM_DATE_ONLY    
         , MIN(X.OPEN_HOUR) AS OPEN_HOUR
         , MAX(X.CLOSE_HOUR) AS CLOSE_HOUR
         , SUM(X.SESSION_HOURS) AS SESSION_HOURS
         
   INTO #GT_TEMPORARY_REPORT_DATA
   
   FROM (  
           -- CORE QUERY
                             SELECT    CASE 
                                                                       WHEN @_TIME_INTERVAL = 0 THEN     -- TO FILTER BY DAY
                                                                            DATEADD(dd, 0, DATEDIFF(dd, 0, CS_OPENING_DATE))
                                                                       WHEN @_TIME_INTERVAL = 1 THEN     -- TO FILTER BY MONTH
                                                                             CAST(CAST(YEAR(CS_OPENING_DATE) AS VARCHAR(4)) + RIGHT('0' + CAST(MONTH(CS_OPENING_DATE) AS VARCHAR(2)), 2) + '01' AS DATETIME)
                                                                       WHEN @_TIME_INTERVAL = 2 THEN     -- TO FILTER BY YEAR
                                                                             CAST(CAST(YEAR(CS_OPENING_DATE) AS VARCHAR(4)) + '01'+ '01' AS DATETIME)
                                                           END AS CM_DATE_ONLY 
                                                     , (CASE WHEN @_BASE_TYPE = 0 THEN GTT_GAMING_TABLE_TYPE_ID ELSE GT.GT_GAMING_TABLE_ID END) AS TABLE_IDENTIFIER  -- GET THE BASE TYPE IDENTIFIER
                                                     , (CASE WHEN @_BASE_TYPE = 0 THEN GTT_NAME ELSE GT.GT_NAME END) AS TABLE_NAME  -- GET THE BASE TYPE IDENTIFIER NAME
                                                     , (CASE WHEN @_BASE_TYPE = 0 THEN NULL ELSE GTT_GAMING_TABLE_TYPE_ID END) AS TABLE_TYPE  -- TYPE 
                                                     , (CASE WHEN @_BASE_TYPE = 0 THEN NULL ELSE GTT_NAME END) AS TABLE_TYPE_NAME  -- TYPE NAME
                                                     , DBO.GT_CALCULATE_DROP(ISNULL(GTS_OWN_SALES_AMOUNT, 0), ISNULL(GTS_EXTERNAL_SALES_AMOUNT, 0)) AS S_DROP
                                                     , DBO.GT_CALCULATE_WIN(ISNULL(GTS.GTS_FINAL_CHIPS_AMOUNT, 0), ISNULL(GTS.GTS_INITIAL_CHIPS_AMOUNT, 0), ISNULL(GTS.GTS_COLLECTED_AMOUNT, 0)) AS S_WIN
                                                     , DBO.GT_CALCULATE_TIP(ISNULL(GTS_TIPS, 0)) AS S_TIP
                                                     , CS.CS_OPENING_DATE AS OPEN_HOUR
                                                     , CS.CS_CLOSING_DATE AS CLOSE_HOUR
                                                     , DATEDIFF(MINUTE, CS_OPENING_DATE, ISNULL(CS_CLOSING_DATE, GETDATE())) AS SESSION_HOURS
                                   FROM   GAMING_TABLES_SESSIONS GTS
                  INNER JOIN   CASHIER_SESSIONS CS
                                         ON   GTS.GTS_CASHIER_SESSION_ID = CS.CS_SESSION_ID 
                                         AND   CS_OPENING_DATE >= @pDateFrom AND CS_OPENING_DATE < @pDateTo
                  INNER JOIN   GAMING_TABLES GT 
                                          ON   GTS.GTS_GAMING_TABLE_ID = GT.GT_GAMING_TABLE_ID 
                                         AND   GT.GT_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
                  INNER JOIN   GAMING_TABLES_TYPES GTT 
                                          ON   GT.GT_TYPE_ID = GTT.GTT_GAMING_TABLE_TYPE_ID
          
          -- END CORE QUERY
          
        ) AS X          
 
 GROUP BY   X.TABLE_TYPE, X.TABLE_IDENTIFIER, X.TABLE_TYPE_NAME, X.TABLE_NAME, X.CM_DATE_ONLY  -- Group by indentifier and time interval 

IF @_TIME_INTERVAL <> -1
BEGIN
   -- INTERVALS DATES AND TABLE FINAL PREPARATION

   -- FILTER ACTIVITY
   IF @_ONLY_ACTIVITY = 0
    BEGIN
      -- JOIN THE SELECT WITH ALL DATA
      SELECT  
              DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER,
              DT.TABLE_IDENT_NAME AS TABLE_NAME,
              DT.TABLE_TYPE_IDENT AS TABLE_TYPE, 
              DT.TABLE_TYPE_NAME AS TABLE_TYPE_NAME,
              ISNULL(ZZ.TOTAL_DROP, 0) AS TOTAL_DROP, 
              ISNULL(ZZ.TOTAL_WIN, 0) AS TOTAL_WIN, 
              ISNULL(ZZ.TOTAL_TIP, 0) AS TOTAL_TIP, 
              ISNULL(ZZ.WIN_DROP, 0) AS WIN_DROP, 
              ISNULL(ZZ.TIP_DROP, 0) AS TIP_DROP, 
              ISNULL(ZZ.WIN_TIPS, 0) AS WIN_TIPS, 
              ZZ.OPEN_HOUR,
              ZZ.CLOSE_HOUR,
              CASE WHEN ZZ.CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED,
              CASE WHEN ZZ.SESSION_HOURS = 0 THEN DATEDIFF(MINUTE,ZZ.OPEN_HOUR,GETDATE()) ELSE ISNULL(ZZ.SESSION_HOURS, 0) END AS SESSION_HOURS,
              DATE_TIME

      FROM @_DAYS_AND_TABLES DT
        
        LEFT JOIN #GT_TEMPORARY_REPORT_DATA ZZ 
                     ON   
                          (
                              (@_TIME_INTERVAL = 0 AND DATE_TIME = ZZ.CM_DATE_ONLY)
                           OR (@_TIME_INTERVAL = 1 AND MONTH(DATE_TIME) = MONTH(ZZ.CM_DATE_ONLY) AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
                           OR (@_TIME_INTERVAL = 2 AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
                          )
                    AND   TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER

       -- SET ORDER 
       ORDER BY   DT.TABLE_TYPE_IDENT ASC,
                  CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC,
                  DATE_TIME DESC;                                             
                    
    END
   ELSE
    BEGIN
      -- JOIN THE SELECT HIDDING WITHOUT ACTIVITY
      SELECT  
              DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER,
              DT.TABLE_IDENT_NAME AS TABLE_NAME,
              DT.TABLE_TYPE_IDENT AS TABLE_TYPE, 
              DT.TABLE_TYPE_NAME AS TABLE_TYPE_NAME,
              ISNULL(ZZ.TOTAL_DROP, 0) AS TOTAL_DROP, 
              ISNULL(ZZ.TOTAL_WIN, 0) AS TOTAL_WIN, 
              ISNULL(ZZ.TOTAL_TIP, 0) AS TOTAL_TIP, 
              ISNULL(ZZ.WIN_DROP, 0) AS WIN_DROP, 
              ISNULL(ZZ.TIP_DROP, 0) AS TIP_DROP, 
              ISNULL(ZZ.WIN_TIPS, 0) AS WIN_TIPS, 
              ZZ.OPEN_HOUR,
              ZZ.CLOSE_HOUR,
              CASE WHEN ZZ.CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED,
              CASE WHEN ZZ.SESSION_HOURS = 0 THEN DATEDIFF(MINUTE,ZZ.OPEN_HOUR,GETDATE()) ELSE ISNULL(ZZ.SESSION_HOURS, 0) END AS SESSION_HOURS,
              DATE_TIME 

      FROM @_DAYS_AND_TABLES DT
        
       INNER JOIN #GT_TEMPORARY_REPORT_DATA ZZ 
                     ON   
                          (
                              (@_TIME_INTERVAL = 0 AND DATE_TIME = ZZ.CM_DATE_ONLY)
                           OR (@_TIME_INTERVAL = 1 AND MONTH(DATE_TIME) = MONTH(ZZ.CM_DATE_ONLY) AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
                           OR (@_TIME_INTERVAL = 2 AND YEAR(DATE_TIME) = YEAR(ZZ.CM_DATE_ONLY))
                          )
                    AND   TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER
       
       -- SET ORDER             
       ORDER BY   DT.TABLE_TYPE_IDENT ASC,
                  CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC,
                  DATE_TIME DESC;
                  
    END -- IF ONLY_ACTIVITY

END
ELSE  -- ELSE WITHOUT INTERVALS
BEGIN
   
   -- FINAL WITHOUT INTERVALS 
   
      -- FILTER ACTIVITY
   IF @_ONLY_ACTIVITY = 0
    BEGIN
            -- JOIN DATA INCLUDING WITHOUT ACTIVITY
            SELECT   DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER,
                     DT.TABLE_IDENT_NAME AS TABLE_NAME,
                     DT.TABLE_TYPE_IDENT AS TABLE_TYPE, 
                     DT.TABLE_TYPE_NAME,
                     ISNULL(TOTAL_DROP, 0) AS TOTAL_DROP,
                     ISNULL(TOTAL_WIN, 0) AS TOTAL_WIN,
                     ISNULL(TOTAL_TIP, 0) AS TOTAL_TIP,
                     ISNULL(WIN_DROP, 0) AS WIN_DROP,
                     ISNULL(TIP_DROP, 0) AS TIP_DROP,
                     ISNULL(WIN_TIPS, 0) AS WIN_TIPS,
                     OPEN_HOUR,
                     CLOSE_HOUR,
                     CASE WHEN CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED,
                     CASE WHEN SESSION_HOURS = 0 THEN DATEDIFF(MINUTE, OPEN_HOUR, GETDATE()) ELSE ISNULL(SESSION_HOURS, 0) END AS SESSION_HOURS
              FROM   @_DAYS_AND_TABLES DT
                 
         LEFT JOIN   #GT_TEMPORARY_REPORT_DATA ZZ
                ON   DT.TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER
                
                          ORDER BY   DT.TABLE_TYPE_IDENT ASC,
                     CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC,
                     DATE_TIME DESC;                            

     END
    ELSE
     BEGIN
            -- JOIN DATA WITH ONLY ACTIVITY
            SELECT   DT.TABLE_IDENTIFIER_VALUE AS TABLE_IDENTIFIER,
                     DT.TABLE_IDENT_NAME AS TABLE_NAME,
                     DT.TABLE_TYPE_IDENT AS TABLE_TYPE, 
                     DT.TABLE_TYPE_NAME,
                     ISNULL(TOTAL_DROP, 0) AS TOTAL_DROP,
                     ISNULL(TOTAL_WIN, 0) AS TOTAL_WIN,
                     ISNULL(TOTAL_TIP, 0) AS TOTAL_TIP,
                     ISNULL(WIN_DROP, 0) AS WIN_DROP,
                     ISNULL(TIP_DROP, 0) AS TIP_DROP,
                     ISNULL(WIN_TIPS, 0) AS WIN_TIPS,
                     OPEN_HOUR,
                     CLOSE_HOUR,
                     CASE WHEN CLOSE_HOUR IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS SESSION_CLOSED,
                     CASE WHEN SESSION_HOURS = 0 THEN DATEDIFF(MINUTE, OPEN_HOUR, GETDATE()) ELSE ISNULL(SESSION_HOURS, 0) END AS SESSION_HOURS
              FROM   @_DAYS_AND_TABLES DT
                 
         INNER JOIN   #GT_TEMPORARY_REPORT_DATA ZZ
                ON   DT.TABLE_IDENTIFIER_VALUE = ZZ.TABLE_IDENTIFIER
                
           ORDER BY   DT.TABLE_TYPE_IDENT ASC,
                      CASE WHEN @_ORDER_BY = 1 THEN TABLE_IDENT_NAME END ASC,
                      DATE_TIME DESC;                            
                      
     END  
 END

-- ERASE THE TEMPORARY DATA 
DROP TABLE #GT_TEMPORARY_REPORT_DATA

END -- END PROCEDURE
GO

-- PERMISSIONS

GRANT EXECUTE ON [dbo].[GT_Base_Report_Data] TO [wggui] WITH GRANT OPTION
GO

--

/*
----------------------------------------------------------------------------------------------------------------
BASIC REPORT QUERY FOR CHIPS OPERATIONS

Version  Date           User     Description
----------------------------------------------------------------------------------------------------------------
1.0.0    09-JAN-2014    RMS      First Release

Requeriments:
   -- Functions:
         
Parameters:
   -- DATE FROM:      Start date for data collection. (If NULL then use first available date).
   -- DATE TO:        End date for data collection. (If NULL then use current date).
   -- STATUS:         The gaming table status tu filter (enabled or disabled) (-1 to show all).
   -- AREA:           Location Area of the gaming table.
   -- BANK:           Area bank of location.
   -- FLOOR:          Floor of location.
   -- CASHIERS:       Indicates if cashiers without gaming table must be included 
   -- CASHIER GROUP NAME: Sets the name of the gaming table type column for cashiers.
   -- VALID TYPES:    A string that contains the valid table types to list.
*/    
CREATE PROCEDURE [dbo].[GT_Chips_Operations] 
 ( @pDateFrom DATETIME
  ,@pDateTo DATETIME
  ,@pStatus INTEGER
  ,@pArea VARCHAR(50)
  ,@pBank VARCHAR(50)
  ,@pCashiers INTEGER
  ,@pCashierGroupName VARCHAR(50)
  ,@pValidTypes VARCHAR(4096)
 )
AS
BEGIN

----------------------------------------------------------------------------------------------------------------
DECLARE @_DATE_FROM     AS   DATETIME
DECLARE @_DATE_TO       AS   DATETIME
DECLARE @_STATUS        AS   INTEGER
DECLARE @_AREA          AS   VARCHAR(4096)
DECLARE @_BANK          AS   VARCHAR(4096)
DECLARE @_DELIMITER     AS   CHAR(1)
DECLARE @_CASHIERS      AS   INTEGER
DECLARE @_CASHIERS_NAME AS   VARCHAR(50)
DECLARE @_TYPES_TABLE        TABLE(SST_ID INT, SST_VALUE VARCHAR(50)) -- TO STORE THE VALID GAMING TABLE TYPES 

----------------------------------------------------------------------------------------------------------------

-- Initialzitation --
SET @_DELIMITER   =   ','
SET @_DATE_FROM   =   @pDateFrom
SET @_DATE_TO     =   @pDateTo
SET @_STATUS      =   ISNULL(@pStatus, -1)
SET @_AREA        =   ISNULL(@pArea, '')
SET @_BANK        =   ISNULL(@pBank, '')
SET @_CASHIERS    =   ISNULL(@pCashiers, 1)
SET @_CASHIERS_NAME = ISNULL(@pCashierGroupName, '---')

----------------------------------------------------------------------------------------------------------------
-- CHECK DATE PARAMETERS
IF @_DATE_FROM IS NULL
BEGIN
   -- IF DATE FROM IS NULL, USER FIRST POSIBLE DATE
   SET @_DATE_FROM = CAST('' AS DATETIME)
END
IF @_DATE_TO IS NULL
BEGIN
   -- IF DATE TO IS NULL, QUERY UNTIL CURRENT DATE
   SET @_DATE_TO = CAST(GETDATE() AS DATETIME)
END

-- ASSIGN TYPES PARAMETER INTO TABLE
INSERT INTO @_TYPES_TABLE SELECT * FROM dbo.SplitStringIntoTable(@pValidTypes, @_DELIMITER, DEFAULT)

----------------------------------------------------------------------------------------------------------------
-- MAIN QUERY

    SELECT 0 AS TYPE_SESSION
           ,  GTS.GTS_GAMING_TABLE_SESSION_ID AS SESSION_ID
           ,  GT.GT_NAME AS GT_NAME
           , GTT.GTT_NAME AS GTT_NAME
           , SUM(ISNULL(GTS.GTS_TOTAL_SALES_AMOUNT,0)) AS GTS_TOTAL_SALES_AMOUNT
           , SUM(ISNULL(GTS.GTS_TOTAL_PURCHASE_AMOUNT,0)) AS GTS_TOTAL_PURCHASE_AMOUNT
           
      INTO   #CHIPS_OPERATIONS_TABLE           
      FROM   GAMING_TABLES GT
 LEFT JOIN   GAMING_TABLES_SESSIONS GTS
        ON   GTS.GTS_GAMING_TABLE_ID = GT.GT_GAMING_TABLE_ID
 LEFT JOIN   CASHIER_SESSIONS CS
        ON   CS.CS_SESSION_ID = GTS.GTS_CASHIER_SESSION_ID
INNER JOIN   GAMING_TABLES_TYPES GTT
        ON   GTT.GTT_GAMING_TABLE_TYPE_ID = GT.GT_TYPE_ID 
     WHERE   GT.GT_ENABLED  = CASE WHEN @_STATUS = -1 THEN GT.GT_ENABLED ELSE @_STATUS END
       AND   GT.GT_AREA_ID  = CASE WHEN @_AREA   = '' THEN GT.GT_AREA_ID ELSE @_AREA END
       AND   GT.GT_BANK_ID  = CASE WHEN @_BANK   = '' THEN GT.GT_BANK_ID ELSE @_BANK END
       AND   GT.GT_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
       AND   (CS.CS_OPENING_DATE >= @_DATE_FROM AND CS.CS_OPENING_DATE < @_DATE_TO)  
       AND	 GT.GT_HAS_INTEGRATED_CASHIER = 1
  GROUP BY   GT.GT_NAME, GTT.GTT_NAME, GTS.GTS_GAMING_TABLE_SESSION_ID
  ORDER BY   GTT.GTT_NAME
  
-- Check if cashiers must be visible  
IF @_CASHIERS = 1 
BEGIN

-- Select and join data to show cashiers
   -- Adding cashiers without gaming tables

  INSERT INTO #CHIPS_OPERATIONS_TABLE
       SELECT 1 AS TYPE_SESSION
              ,  CM_SESSION_ID AS SESSION_ID
              ,  CT_NAME as GT_NAME
              , @_CASHIERS_NAME as GTT_NAME
              , SUM(CM_SUB_AMOUNT) AS GTS_TOTAL_SALES_AMOUNT
              , SUM(CM_ADD_AMOUNT) AS GTS_TOTAL_PURCHASE_AMOUNT
         FROM   CASHIER_MOVEMENTS
   INNER JOIN   CASHIER_TERMINALS
           ON   CM_CASHIER_ID = CT_CASHIER_ID
        WHERE   CM_CASHIER_ID NOT IN (SELECT DISTINCT(GT_CASHIER_ID) FROM GAMING_TABLES)
          AND   CM_TYPE IN (303,304)
          AND   (CM_DATE >= @_DATE_FROM AND CM_DATE < @_DATE_TO)
     GROUP BY   CT_NAME, CM_SESSION_ID

END
 -- Select results
 SELECT * FROM #CHIPS_OPERATIONS_TABLE ORDER BY GT_NAME
  
-- DROP TEMPORARY TABLE  
DROP TABLE #CHIPS_OPERATIONS_TABLE
  
END  -- END PROCEDURE
GO

-- PERMISSIONS

GRANT EXECUTE ON [dbo].[GT_Chips_Operations] TO [wggui] WITH GRANT OPTION
GO

--

/*
----------------------------------------------------------------------------------------------------------------
BASIC REPORT QUERY FOR GAMING TABLE SESSION INFORMATION

Version  Date           User     Description
----------------------------------------------------------------------------------------------------------------
1.0.0    31-JAN-2014    RMS      First Release

Requeriments:
   -- Functions:
         
Parameters:
   -- BASE TYPE:        To select if report data source is session or cashier movements. 
                          0 - By Session
                          1 - By Cashier Movements
   -- DATE FROM:        Start date for data collection. (If NULL then use first available date).
   -- DATE TO:          End date for data collection. (If NULL then use current date).
   -- STATUS:           The gaming table session status (Opened or Closed) (-1 to show all).
   -- ENABLED:          The gaming table status (Enabled or Disabled) (-1 to show all).
   -- AREA:             Location Area of the gaming table.
   -- BANK:             Area bank of location.
   -- CHIPS ISO CODE:   ISO Code for chips in cage.
   -- CHIPS COINS CODE: Coins code for chips in cage.
   -- VALID TYPES:      A string that contains the valid table types to list.
*/  

CREATE PROCEDURE [dbo].[GT_Session_Information] 
 ( @pBaseType INTEGER
  ,@pDateFrom DATETIME 
  ,@pDateTo DATETIME
  ,@pStatus INTEGER
  ,@pEnabled INTEGER
  ,@pAreaId INTEGER
  ,@pBankId INTEGER
  ,@pChipsISOCode VARCHAR(50)
  ,@pChipsCoinsCode INTEGER
  ,@pValidTypes VARCHAR(4096)
 )
AS
BEGIN

----------------------------------------------------------------------------------------------------------------
DECLARE @_BASE_TYPE        AS   INTEGER
DECLARE @_DATE_FROM        AS   DATETIME
DECLARE @_DATE_TO          AS   DATETIME
DECLARE @_STATUS           AS   INTEGER
DECLARE @_ENABLED          AS   INTEGER
DECLARE @_AREA             AS   INTEGER
DECLARE @_BANK             AS   INTEGER
DECLARE @_CHIPS_ISO_CODE   AS   VARCHAR(50)
DECLARE @_CHIPS_COINS_CODE AS   VARCHAR(50)
DECLARE @_DELIMITER        AS   CHAR(1)
DECLARE @_TYPES_TABLE           TABLE(SST_ID INT, SST_VALUE VARCHAR(50)) -- TO STORE THE VALID GAMING TABLE TYPES 

-- MOVEMENTS
DECLARE @_CHIPS_MOVEMENT_PURCHASE_TOTAL AS   INTEGER
DECLARE @_CHIPS_MOVEMENT_SALES_TOTAL    AS   INTEGER
DECLARE @_MOVEMENT_FILLER_IN            AS   INTEGER
DECLARE @_MOVEMENT_FILLER_OUT           AS   INTEGER
DECLARE @_MOVEMENT_CAGE_FILLER_OUT      AS   INTEGER
DECLARE @_MOVEMENT_CAGE_CLOSE_SESSION   AS   INTEGER

----------------------------------------------------------------------------------------------------------------

-- Initialzitation --
SET @_DELIMITER        =   ','
SET @_BASE_TYPE        =   @pBaseType
SET @_DATE_FROM        =   @pDateFrom
SET @_DATE_TO          =   @pDateTo
SET @_STATUS           =   ISNULL(@pStatus, -1)
SET @_ENABLED          =   ISNULL(@pEnabled, -1)
SET @_AREA             =   ISNULL(@pAreaId, 0)
SET @_BANK             =   ISNULL(@pBankId, 0)
SET @_CHIPS_ISO_CODE   =   ISNULL(@pChipsISOCode, 'X01')
SET @_CHIPS_COINS_CODE =   ISNULL(@pChipsCoinsCode, -100)

SET @_CHIPS_MOVEMENT_PURCHASE_TOTAL =   304
SET @_CHIPS_MOVEMENT_SALES_TOTAL    =   303
SET @_MOVEMENT_FILLER_IN            =   2
SET @_MOVEMENT_FILLER_OUT           =   3
SET @_MOVEMENT_CAGE_FILLER_OUT      =   203
SET @_MOVEMENT_CAGE_CLOSE_SESSION   =   201

----------------------------------------------------------------------------------------------------------------
-- CHECK DATE PARAMETERS
IF @_DATE_FROM IS NULL
BEGIN
   -- IF DATE FROM IS NULL, USER FIRST POSIBLE DATE
   SET @_DATE_FROM = CAST('' AS DATETIME)
END
IF @_DATE_TO IS NULL
BEGIN
   -- IF DATE TO IS NULL, QUERY UNTIL CURRENT DATE
   SET @_DATE_TO = CAST(GETDATE() AS DATETIME)
END

-- ASSIGN TYPES PARAMETER INTO TABLE
INSERT INTO @_TYPES_TABLE SELECT * FROM dbo.SplitStringIntoTable(@pValidTypes, @_DELIMITER, DEFAULT)

----------------------------------------------------------------------------------------------------------------

IF @_BASE_TYPE = 0 
BEGIN
   -- REPORT BY GAMING TABLE SESSION
 SELECT    GTS_CASHIER_SESSION_ID                                                           
         , CS_NAME                                                                          
         , CS_STATUS                                                                        
         , GT_NAME                                                                          
         , GTT_NAME                                                                         
         , GT_HAS_INTEGRATED_CASHIER                                                        
         , CT_NAME                                                                          
         , GU_USERNAME                                                                      
         , CS_OPENING_DATE                                                                  
         , CS_CLOSING_DATE                                                                  
         , GTS_TOTAL_PURCHASE_AMOUNT                                                        
         , GTS_TOTAL_SALES_AMOUNT                                                           
         , GTS_INITIAL_CHIPS_AMOUNT                                                         
         , GTS_FINAL_CHIPS_AMOUNT                                                           
         , GTS_TIPS                                                                         
         , GTS_COLLECTED_AMOUNT                                                             
         , GTS_CLIENT_VISITS                                                                
         , AR_NAME          		                                                            
         , BK_NAME          		                                                            
   FROM         GAMING_TABLES_SESSIONS                                                      
  INNER    JOIN CASHIER_SESSIONS      ON CS_SESSION_ID            = GTS_CASHIER_SESSION_ID  
   LEFT    JOIN GUI_USERS             ON GU_USER_ID               = CS_USER_ID              
   LEFT    JOIN GAMING_TABLES         ON GT_GAMING_TABLE_ID       = GTS_GAMING_TABLE_ID     
   LEFT    JOIN GAMING_TABLES_TYPES   ON GTT_GAMING_TABLE_TYPE_ID = GT_TYPE_ID              
   LEFT    JOIN AREAS                 ON GT_AREA_ID               = AR_AREA_ID              
   LEFT    JOIN BANKS                 ON GT_BANK_ID               = BK_BANK_ID              
   LEFT    JOIN CASHIER_TERMINALS     ON GT_CASHIER_ID            = CT_CASHIER_ID           
 WHERE   (CS_OPENING_DATE >= @_DATE_FROM AND (CS_CLOSING_DATE IS NULL OR CS_CLOSING_DATE <= @_DATE_TO))
   AND   (GT_ENABLED = CASE WHEN @_ENABLED = -1 THEN GT_ENABLED ELSE @_ENABLED END)  
   AND   (CS_STATUS  = CASE WHEN @_STATUS  = -1 THEN CS_STATUS  ELSE @_STATUS  END) 
   AND   (GT_AREA_ID = CASE WHEN @_AREA    = 0  THEN GT_AREA_ID ELSE @_AREA    END) 
   AND   (GT_BANK_ID = CASE WHEN @_BANK    = 0  THEN GT_BANK_ID ELSE @_BANK    END) 
   AND   (GT_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE ))
   ;
   
END
ELSE
BEGIN
   -- REPORT BY CASHIER MOVEMENTS

 SELECT     CM_SESSION_ID                 
          , CS_NAME                       
          , CS_STATUS                     
          , GT_NAME                       
          , GTT_NAME                      
          , GT_HAS_INTEGRATED_CASHIER     
          , CT_NAME                       
          , GU_USERNAME                   
          , CS_OPENING_DATE               
          , CS_CLOSING_DATE               
          , CM_TOTAL_PURCHASE_AMOUNT      
          , CM_TOTAL_SALES_AMOUNT         
          , CM_INITIAL_CHIPS_AMOUNT       
          , CM_FINAL_CHIPS_AMOUNT         
          , CM_TIPS                       
          , CASE WHEN CM_COLLECTED_AMOUNT < 0 THEN 0 
                ELSE CM_COLLECTED_AMOUNT END as CM_COLLECTED_AMOUNT 
          , GTS_CLIENT_VISITS             
          , AR_NAME                       
          , BK_NAME                       
 FROM      (                             
 SELECT    CM_SESSION_ID                                                                      
         , CS_NAME                                                                            
         , CS_STATUS                                                                          
         , GT_NAME                                                                            
         , GTT_NAME                                                                           
         , GT_HAS_INTEGRATED_CASHIER                                                          
         , CT_NAME                                                                            
         , GU_USERNAME                                                                        
         , CS_OPENING_DATE                                                                    
         , CS_CLOSING_DATE                                                                    
         , SUM(CASE WHEN CM_TYPE                  = @_CHIPS_MOVEMENT_PURCHASE_TOTAL
                    THEN CM_ADD_AMOUNT ELSE 0 END
              )  AS CM_TOTAL_PURCHASE_AMOUNT           
         , SUM(CASE WHEN CM_TYPE                  = @_CHIPS_MOVEMENT_SALES_TOTAL
                    THEN CM_SUB_AMOUNT ELSE 0 END
              )  AS CM_TOTAL_SALES_AMOUNT	            
         , SUM(CASE WHEN CM_CURRENCY_ISO_CODE     = @_CHIPS_ISO_CODE             
                     AND CM_TYPE                  = @_MOVEMENT_FILLER_IN
                    THEN CM_ADD_AMOUNT ELSE 0 END
              )  AS CM_INITIAL_CHIPS_AMOUNT	          
         , SUM(CASE WHEN CM_CURRENCY_ISO_CODE     = @_CHIPS_ISO_CODE             
                     AND CM_TYPE                  = @_MOVEMENT_FILLER_OUT
                    THEN CM_SUB_AMOUNT ELSE 0 END
              )  AS CM_FINAL_CHIPS_AMOUNT	            
         , SUM(CASE WHEN CM_TYPE                  = @_MOVEMENT_CAGE_FILLER_OUT
                     AND CM_CURRENCY_ISO_CODE     = @_CHIPS_ISO_CODE             
                     AND CM_CURRENCY_DENOMINATION = @_CHIPS_COINS_CODE
                    THEN CM_SUB_AMOUNT ELSE 0 END
              )  AS CM_TIPS                            
         , SUM(CASE WHEN CM_CURRENCY_ISO_CODE IS NULL OR CM_CURRENCY_ISO_CODE <> @_CHIPS_ISO_CODE THEN 
                    CASE WHEN CM_TYPE   = @_MOVEMENT_FILLER_IN            THEN -1 * DBO.ApplyExchange(CM_ADD_AMOUNT, CM_CURRENCY_ISO_CODE) 
                         WHEN CM_TYPE   = @_MOVEMENT_FILLER_OUT           THEN      DBO.ApplyExchange(CM_SUB_AMOUNT, CM_CURRENCY_ISO_CODE) 
                         WHEN CM_TYPE   = @_MOVEMENT_CAGE_CLOSE_SESSION   THEN      DBO.ApplyExchange(CM_INITIAL_BALANCE, CM_CURRENCY_ISO_CODE) 
                         ELSE 0 END 
               ELSE 0 END
              )  AS CM_COLLECTED_AMOUNT 
         , GTS_CLIENT_VISITS                                                                  
         , AR_NAME          		                                                              
         , BK_NAME          		                                                              
   FROM        CASHIER_MOVEMENTS                                                             
  INNER   JOIN	CASHIER_SESSIONS        ON CS_SESSION_ID            = CM_SESSION_ID           
  INNER   JOIN	GAMING_TABLES_SESSIONS  ON CS_SESSION_ID            = GTS_CASHIER_SESSION_ID  
   LEFT   JOIN	GUI_USERS               ON GU_USER_ID               = CS_USER_ID              
   LEFT   JOIN	GAMING_TABLES           ON GT_GAMING_TABLE_ID       = GTS_GAMING_TABLE_ID     
   LEFT   JOIN	GAMING_TABLES_TYPES     ON GTT_GAMING_TABLE_TYPE_ID = GT_TYPE_ID              
   LEFT   JOIN	AREAS                   ON GT_AREA_ID               = AR_AREA_ID              
   LEFT   JOIN	BANKS                   ON GT_BANK_ID               = BK_BANK_ID              
   LEFT   JOIN	CASHIER_TERMINALS       ON GT_CASHIER_ID            = CT_CASHIER_ID           
 WHERE   CM_MOVEMENT_ID IN 
            (
               SELECT   CM_MOVEMENT_ID 
                 FROM   CASHIER_MOVEMENTS  
                WHERE   (CM_DATE >= @_DATE_FROM AND CM_DATE <= @_DATE_TO) 
                  AND   CM_SESSION_ID = CS_SESSION_ID 
            ) 
   AND   (GT_ENABLED = CASE WHEN @_ENABLED = -1 THEN GT_ENABLED ELSE @_ENABLED END)  
   AND   (CS_STATUS  = CASE WHEN @_STATUS  = -1 THEN CS_STATUS  ELSE @_STATUS  END)             
   AND   (GT_AREA_ID = CASE WHEN @_AREA    = 0  THEN GT_AREA_ID ELSE @_AREA    END) 
   AND   (GT_BANK_ID = CASE WHEN @_BANK    = 0  THEN GT_BANK_ID ELSE @_BANK    END) 
   AND   (GT_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE ))
                  
  GROUP BY  CM_SESSION_ID         
      , CS_NAME                   
      , CS_STATUS                 
      , GT_NAME                   
      , GTT_NAME                  
      , GT_HAS_INTEGRATED_CASHIER 
      , CT_NAME                   
      , GU_USERNAME               
      , CS_OPENING_DATE           
      , CS_CLOSING_DATE           
      , GTS_CLIENT_VISITS         
      , AR_NAME                   
      , BK_NAME                   
  ) AS   CASHIER_MOVEMENTS        
  ;
  
END

END -- END PROCEDURE
GO

-- PERMISSIONS

GRANT EXECUTE ON [dbo].[GT_Session_Information] TO [wggui] WITH GRANT OPTION
GO


CREATE PROCEDURE GT_Cancellations
(
	  @pCashierId BIGINT,
    @pDateFrom DATETIME,
    @pDateTo DATETIME,
    @pType INT -- 0: only Sales ; 1: only Purchases ; -1: all
)
AS
BEGIN

	DECLARE @TYPE_CHIPS_SALE AS INT
	DECLARE @TYPE_CHIPS_PURCHASE AS INT
	DECLARE @TYPE_CHIPS_SALE_TOTAL AS INT
	DECLARE @TYPE_CHIPS_PURCHASE_TOTAL AS INT
	DECLARE @UNDO_STATUS AS INT

	SET @TYPE_CHIPS_SALE = 300
	SET @TYPE_CHIPS_PURCHASE = 301
	SET @TYPE_CHIPS_SALE_TOTAL = 303
	SET @TYPE_CHIPS_PURCHASE_TOTAL = 304
	SET @UNDO_STATUS = 2
	
	SET @pType = ISNULL(@pType, -1)

	SELECT
		CASE WHEN CM_TYPE IN (@TYPE_CHIPS_SALE_TOTAL, @TYPE_CHIPS_PURCHASE_TOTAL) THEN 
				ISNULL(GT.GT_NAME,CT.CT_NAME)
			 ELSE NULL END AS ORIGEN,
		CM_DATE,
		CASE WHEN CM_TYPE IN (@TYPE_CHIPS_SALE, @TYPE_CHIPS_SALE_TOTAL) THEN ABS(CM_SUB_AMOUNT)
			ELSE ABS(CM_ADD_AMOUNT) END AS AMOUNT,
			
		ABS(CASE WHEN CM_CURRENCY_DENOMINATION IS NULL OR CM_CURRENCY_DENOMINATION =0 THEN 0 
				 ELSE 
					CASE WHEN CM_TYPE IN (@TYPE_CHIPS_SALE, @TYPE_CHIPS_SALE_TOTAL) THEN CM_SUB_AMOUNT
						 ELSE CM_ADD_AMOUNT 
						 END / CM_CURRENCY_DENOMINATION 
					END) AS QUANTITY,
		CM_CURRENCY_DENOMINATION,
		CM.CM_MOVEMENT_ID,
		CM.CM_TYPE,
		GU.GU_USERNAME,
		CM.CM_OPERATION_ID
	FROM CASHIER_MOVEMENTS CM
 		 LEFT JOIN GAMING_TABLES_SESSIONS GTS ON GTS.GTS_GAMING_TABLE_SESSION_ID = CM.CM_GAMING_TABLE_SESSION_ID
 		 LEFT JOIN GAMING_TABLES GT ON GT.GT_GAMING_TABLE_ID = GTS.GTS_GAMING_TABLE_ID
		 LEFT JOIN CASHIER_TERMINALS CT ON CT.CT_CASHIER_ID = CM.CM_CASHIER_ID
		 LEFT JOIN GUI_USERS GU ON CM.CM_USER_ID = GU.GU_USER_ID
	WHERE (@pCashierId IS NULL OR @pCashierId = CT.CT_CASHIER_ID)
		  AND CM_UNDO_STATUS = @UNDO_STATUS 
		  AND CM_TYPE IN (@TYPE_CHIPS_SALE, @TYPE_CHIPS_PURCHASE, @TYPE_CHIPS_SALE_TOTAL, @TYPE_CHIPS_PURCHASE_TOTAL)
		  AND CM_DATE >= @pDateFrom AND CM_DATE < @pDateTo
		  AND (@pType = -1
				OR (@pType = 1 AND CM.CM_TYPE IN (@TYPE_CHIPS_PURCHASE, @TYPE_CHIPS_PURCHASE_TOTAL))
				OR (@pType = 0 AND CM.CM_TYPE IN (@TYPE_CHIPS_SALE, @TYPE_CHIPS_SALE_TOTAL)))

END --GT_CANCELLATIONS

GO

-- PERMISSIONS

GRANT EXECUTE ON [dbo].[GT_Cancellations] TO [wggui] WITH GRANT OPTION
GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TITO_SetTicketCanceled]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[TITO_SetTicketCanceled]
GO

CREATE PROCEDURE [dbo].[TITO_SetTicketCanceled]
      @pTerminalId                 INT,
      @pTicketValidationNumber     BIGINT,
      @MsgSequenceId               BIGINT,
      --- OUTPUT
      @pTicketId                   BIGINT   OUTPUT,
      @pTicketAmount               MONEY    OUTPUT
AS
BEGIN
      DECLARE  @_ticket_status_canceled              INT
      DECLARE  @_ticket_status_pending_cancel        INT
      DECLARE  @_min_ticket_id                       BIGINT
      DECLARE  @_max_ticket_id                       BIGINT
      DECLARE  @_ticket_amount                       MONEY

      SET NOCOUNT ON

      SET @pTicketId = NULL
      SET @pTicketAmount = NULL

      SET @_ticket_status_canceled       = 1 -- TITO_TICKET_STATUS.CANCELED
      SET @_ticket_status_pending_cancel = 5 -- TITO_TICKET_STATUS.PENDING_CANCEL

      SELECT   @_min_ticket_id = MIN (TI_TICKET_ID)
             , @_max_ticket_id = MAX (TI_TICKET_ID)
             , @_ticket_amount = MAX (TI_AMOUNT)
        FROM   TICKETS WITH (INDEX (IX_ti_validation_number_status))
      WHERE    TI_VALIDATION_NUMBER       = @pTicketValidationNumber
         AND   TI_STATUS                  = @_ticket_status_pending_cancel
         AND   TI_LAST_ACTION_TERMINAL_ID = @pTerminalId
         
      IF @_min_ticket_id =  @_max_ticket_id
      BEGIN
            -- One ticket found, change its status
            UPDATE   TICKETS 
               SET   TI_STATUS                  = @_ticket_status_canceled 
                   , TI_LAST_ACTION_DATETIME    = GETDATE() 
             WHERE   TI_TICKET_ID               = @_min_ticket_id 
               AND   TI_STATUS                  = @_ticket_status_pending_cancel
               AND   TI_LAST_ACTION_TERMINAL_ID = @pTerminalId
         
            IF @@ROWCOUNT = 1
            BEGIN
                  SET   @pTicketId     = @_min_ticket_id
                  SET   @pTicketAmount = @_ticket_amount

                  INSERT INTO   TITO_TASKS 
                              ( TT_TASK_TYPE
                              , TT_TICKET_ID )
                       VALUES ( 1 -- TITO_TICKET_STATUS.CANCELED
                              , @pTicketId   ) 
            END

            RETURN
      END
END
GO

/******* RECORDS *******/

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY = 'UnbalancedCashClosing.MaxAttempts')
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE)  VALUES ('Cashier', 'UnbalancedCashClosing.MaxAttempts', '3')

IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cage' AND GP_SUBJECT_KEY = 'NewMovement.PrintVoucher')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Cage', 'NewMovement.PrintVoucher', '1')

IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cage' AND GP_SUBJECT_KEY = 'Collection.DefaultCageSession')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Cage', 'Collection.DefaultCageSession', '0')

IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY = 'TransferCredit.KeepSourceCashInHistory')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Cashier', 'TransferCredit.KeepSourceCashInHistory', '0')

--
-- Gambling Tables: Change default values for chips weighing
--
IF EXISTS ( SELECT 1 FROM general_params WHERE gp_group_key = 'GamingTables' AND gp_subject_key = 'Chips.Weighing.High')
  DELETE GENERAL_PARAMS WHERE gp_group_key = 'GamingTables' AND gp_subject_key = 'Chips.Weighing.High'
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('GamingTables', 'Chips.Weighing.High', '25;25;25;24;1;0;0;0;0;0;0;0;0')

IF EXISTS ( SELECT 1 FROM general_params WHERE gp_group_key = 'GamingTables' AND gp_subject_key = 'Chips.Weighing.Medium')
  DELETE GENERAL_PARAMS WHERE gp_group_key = 'GamingTables' AND gp_subject_key = 'Chips.Weighing.Medium'
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('GamingTables', 'Chips.Weighing.Medium','0;0;0;0;0;25;25;25;24;1;0;0;0')

IF EXISTS ( SELECT 1 FROM general_params WHERE gp_group_key = 'GamingTables' AND gp_subject_key = 'Chips.Weighing.Small')
  DELETE GENERAL_PARAMS WHERE gp_group_key = 'GamingTables' AND gp_subject_key = 'Chips.Weighing.Small'
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('GamingTables', 'Chips.Weighing.Small', '0;0;0;0;0;0;0;0;25;25;25;24;1')

GO

--
-- Chips denominations
--
DECLARE @isallowed      INT
SET @isallowed = ISNULL ((SELECT TOP 1 caa_allowed FROM cage_amounts WHERE caa_iso_code = 'X01'),0)

IF NOT EXISTS ( SELECT 1 FROM cage_amounts WHERE caa_iso_code = 'X01' AND caa_denomination = 1)
BEGIN
INSERT INTO cage_amounts
           (caa_iso_code
           ,caa_denomination
           ,caa_allowed
               ,caa_color)
     VALUES
           ('X01',
            1,
            @isallowed,
                  -1)
END
ELSE
BEGIN
      UPDATE cage_amounts SET caa_color = -1 WHERE caa_iso_code = 'X01' AND caa_denomination = 1
END

IF NOT EXISTS ( SELECT 1 FROM cage_amounts WHERE caa_iso_code = 'X01' AND caa_denomination = 2)
BEGIN
INSERT INTO cage_amounts
           (caa_iso_code
           ,caa_denomination
           ,caa_allowed
               ,caa_color)
     VALUES
           ('X01',
            2, 
            @isallowed,
                  -256)
END
ELSE
BEGIN
      UPDATE cage_amounts SET caa_color = -256 WHERE caa_iso_code = 'X01' AND caa_denomination = 2
END


IF NOT EXISTS ( SELECT 1 FROM cage_amounts WHERE caa_iso_code = 'X01' AND caa_denomination = 5)
BEGIN
INSERT INTO cage_amounts
           (caa_iso_code
           ,caa_denomination
           ,caa_allowed
               ,caa_color)
     VALUES
           ('X01',
            5, 
            @isallowed,
                  -65536)
END
ELSE
BEGIN
      UPDATE cage_amounts SET caa_color = -65536 WHERE caa_iso_code = 'X01' AND caa_denomination = 5
END


IF NOT EXISTS ( SELECT 1 FROM cage_amounts WHERE caa_iso_code = 'X01' AND caa_denomination = 10)
BEGIN
INSERT INTO cage_amounts
           (caa_iso_code
           ,caa_denomination
           ,caa_allowed
               ,caa_color)
     VALUES
           ('X01',
           10, 
            @isallowed,
                  -16776961)
END   
ELSE
BEGIN
      UPDATE cage_amounts SET caa_color = -16776961 WHERE caa_iso_code = 'X01' AND caa_denomination = 10
END       


IF NOT EXISTS ( SELECT 1 FROM cage_amounts WHERE caa_iso_code = 'X01' AND caa_denomination = 20)
BEGIN
INSERT INTO cage_amounts
           (caa_iso_code
           ,caa_denomination
           ,caa_allowed
               ,caa_color)
     VALUES
           ('X01',
           20, 
            @isallowed,
                  -4144960)
END
ELSE
BEGIN
      UPDATE cage_amounts SET caa_color = -4144960 WHERE caa_iso_code = 'X01' AND caa_denomination = 20
END    

IF NOT EXISTS ( SELECT 1 FROM cage_amounts WHERE caa_iso_code = 'X01' AND caa_denomination = 25)
BEGIN
INSERT INTO cage_amounts
           (caa_iso_code
           ,caa_denomination
           ,caa_allowed
               ,caa_color)
     VALUES
           ('X01',
            25, 
            @isallowed,
                  -16744448)
END
ELSE
BEGIN
      UPDATE cage_amounts SET caa_color = -16744448 WHERE caa_iso_code = 'X01' AND caa_denomination = 25
END 

IF NOT EXISTS ( SELECT 1 FROM cage_amounts WHERE caa_iso_code = 'X01' AND caa_denomination = 50)
BEGIN
INSERT INTO cage_amounts
           (caa_iso_code
           ,caa_denomination
           ,caa_allowed
               ,caa_color)
     VALUES
           ('X01',
           50, 
            @isallowed,
                  -32768)
END
ELSE
BEGIN
      UPDATE cage_amounts SET caa_color = -32768 WHERE caa_iso_code = 'X01' AND caa_denomination = 50
END 

IF NOT EXISTS ( SELECT 1 FROM cage_amounts WHERE caa_iso_code = 'X01' AND caa_denomination = 100)
BEGIN
INSERT INTO cage_amounts
           (caa_iso_code
           ,caa_denomination
           ,caa_allowed
               ,caa_color)
     VALUES
           ('X01',
           100, 
            @isallowed,
                  -16711680)
END
ELSE
BEGIN
      UPDATE cage_amounts SET caa_color = -16711680 WHERE caa_iso_code = 'X01' AND caa_denomination = 100
END 

IF NOT EXISTS ( SELECT 1 FROM cage_amounts WHERE caa_iso_code = 'X01' AND caa_denomination = 250)
BEGIN
INSERT INTO cage_amounts
           (caa_iso_code
           ,caa_denomination
           ,caa_allowed
               ,caa_color)
     VALUES
           ('X01',
           250, 
            @isallowed,
                  -32576)
END
ELSE
BEGIN
      UPDATE cage_amounts SET caa_color = -32576 WHERE caa_iso_code = 'X01' AND caa_denomination = 250
END

IF NOT EXISTS ( SELECT 1 FROM cage_amounts WHERE caa_iso_code = 'X01' AND caa_denomination = 500)
BEGIN
INSERT INTO cage_amounts
           (caa_iso_code
           ,caa_denomination
           ,caa_allowed
               ,caa_color)
     VALUES
           ('X01',
           500, 
            1,
                  -8388480)
END
ELSE
BEGIN
      UPDATE cage_amounts SET caa_color = -8388480 WHERE caa_iso_code = 'X01' AND caa_denomination = 500
END

IF NOT EXISTS ( SELECT 1 FROM cage_amounts WHERE caa_iso_code = 'X01' AND caa_denomination = 1000)
BEGIN
INSERT INTO cage_amounts
           (caa_iso_code
           ,caa_denomination
           ,caa_allowed
               ,caa_color)
     VALUES
           ('X01',
           1000, 
            @isallowed,
                  -8388608)
END
ELSE
BEGIN
      UPDATE cage_amounts SET caa_color = -8388608 WHERE caa_iso_code = 'X01' AND caa_denomination = 1000
END

IF NOT EXISTS ( SELECT 1 FROM cage_amounts WHERE caa_iso_code = 'X01' AND caa_denomination = 2000)
BEGIN
INSERT INTO cage_amounts
           (caa_iso_code
           ,caa_denomination
           ,caa_allowed
               ,caa_color)
     VALUES
           ('X01',
           2000, 
            @isallowed,
                  -6760717)
END
ELSE
BEGIN
      UPDATE cage_amounts SET caa_color = -6760717 WHERE caa_iso_code = 'X01' AND caa_denomination = 2000
END


IF NOT EXISTS ( SELECT 1 FROM cage_amounts WHERE caa_iso_code = 'X01' AND caa_denomination = 5000)
BEGIN
INSERT INTO cage_amounts
           (caa_iso_code
           ,caa_denomination
           ,caa_allowed
               ,caa_color)
     VALUES
           ('X01',
           5000, 
            @isallowed,
                  -8372224)
END
ELSE
BEGIN
      UPDATE cage_amounts SET caa_color = -8372224 WHERE caa_iso_code = 'X01' AND caa_denomination = 5000
END

GO
