/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 177;

SET @New_ReleaseId = 178;
SET @New_ScriptName = N'UpdateTo_18.178.035.sql';
SET @New_Description = N'TITO: Insert in sequences table, Update in CAGE_SESSIONS, insert GP and Procedure GT_Chips_Operations.';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/******* TABLE *********/

IF EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[money_collection_meters]') and name = 'mcm_promo_re_num')
            ALTER TABLE [dbo].[money_collection_meters]    ALTER COLUMN [mcm_promo_re_num] [bigint] NULL
GO      

/******* RECORDS *******/

INSERT  SEQUENCES (seq_id, seq_next_value)
		   VALUES ( 100  , 1 )
GO

IF NOT EXISTS ( SELECT 1 FROM general_params WHERE gp_group_key = 'GamingTables' AND gp_subject_key = 'Chips.Weighing.Custom')
      INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('GamingTables', 'Chips.Weighing.Custom', '-1;-1;-1;-1;-1;40;-1;30;-1;20;8;-1;2')
GO

UPDATE   CAGE_SESSIONS
   SET   CGS_SESSION_NAME = 'Jornada ' + CAST(CGS_OPEN_DATETIME AS VARCHAR(20))
GO

/******* INDEXES  *******/

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[tickets]') AND name = N'IX_ti_terminal_id_transaction_id')
  DROP INDEX [IX_ti_terminal_id_transaction_id] ON [dbo].[tickets] WITH ( ONLINE = OFF )
GO

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[tickets]') AND name = N'IX_ti_terminal_id_transaction_id_validation_number')
  DROP INDEX [IX_ti_terminal_id_transaction_id_validation_number] ON [dbo].[tickets] WITH ( ONLINE = OFF )
GO

CREATE UNIQUE NONCLUSTERED INDEX [IX_ti_transaction_id_terminal_id_validation_number] ON [dbo].[tickets] 
(
      [ti_transaction_id] ASC,
      [ti_created_terminal_id] ASC,
      [ti_validation_number] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

/******* STORED  *******/

IF OBJECT_ID (N'dbo.GT_Chips_Operations', N'P') IS NOT NULL
    DROP PROCEDURE dbo.GT_Chips_Operations;                 
GO

CREATE PROCEDURE [dbo].[GT_Chips_Operations] 
 ( @pDateFrom DATETIME
  ,@pDateTo DATETIME
  ,@pStatus INTEGER
  ,@pArea VARCHAR(50)
  ,@pBank VARCHAR(50)
  ,@pCashiers INTEGER
  ,@pCashierGroupName VARCHAR(50)
  ,@pValidTypes VARCHAR(4096)
)
AS
BEGIN

----------------------------------------------------------------------------------------------------------------
DECLARE @_DATE_FROM     AS   DATETIME
DECLARE @_DATE_TO       AS   DATETIME
DECLARE @_STATUS        AS   INTEGER
DECLARE @_AREA          AS   VARCHAR(4096)
DECLARE @_BANK          AS   VARCHAR(4096)
DECLARE @_DELIMITER     AS   CHAR(1)
DECLARE @_CASHIERS      AS   INTEGER
DECLARE @_CASHIERS_NAME AS   VARCHAR(50)
DECLARE @_TYPES_TABLE        TABLE(SST_ID INT, SST_VALUE VARCHAR(50)) -- TO STORE THE VALID GAMING TABLE TYPES 

----------------------------------------------------------------------------------------------------------------

-- Initialzitation --
SET @_DELIMITER   =   ','
SET @_DATE_FROM   =   @pDateFrom
SET @_DATE_TO     =   @pDateTo
SET @_STATUS      =   ISNULL(@pStatus, -1)
SET @_AREA        =   ISNULL(@pArea, '')
SET @_BANK        =   ISNULL(@pBank, '')
SET @_CASHIERS    =   ISNULL(@pCashiers, 1)
SET @_CASHIERS_NAME = ISNULL(@pCashierGroupName, '---')

----------------------------------------------------------------------------------------------------------------
-- CHECK DATE PARAMETERS
IF @_DATE_FROM IS NULL
BEGIN
   -- IF DATE FROM IS NULL, USER FIRST POSIBLE DATE
   SET @_DATE_FROM = CAST('' AS DATETIME)
END
IF @_DATE_TO IS NULL
BEGIN
   -- IF DATE TO IS NULL, QUERY UNTIL CURRENT DATE
   SET @_DATE_TO = CAST(GETDATE() AS DATETIME)
END

-- ASSIGN TYPES PARAMETER INTO TABLE
INSERT INTO @_TYPES_TABLE SELECT * FROM dbo.SplitStringIntoTable(@pValidTypes, @_DELIMITER, DEFAULT)

----------------------------------------------------------------------------------------------------------------
-- MAIN QUERY

    SELECT 0 AS TYPE_SESSION
           ,  CS.CS_SESSION_ID AS SESSION_ID
           ,  GT.GT_NAME AS GT_NAME
           , GTT.GTT_NAME AS GTT_NAME
           , SUM(ISNULL(GTS.GTS_TOTAL_SALES_AMOUNT,0)) AS GTS_TOTAL_SALES_AMOUNT
           , SUM(ISNULL(GTS.GTS_TOTAL_PURCHASE_AMOUNT,0)) AS GTS_TOTAL_PURCHASE_AMOUNT
           
      INTO   #CHIPS_OPERATIONS_TABLE           
      FROM   GAMING_TABLES GT
LEFT JOIN   GAMING_TABLES_SESSIONS GTS
        ON   GTS.GTS_GAMING_TABLE_ID = GT.GT_GAMING_TABLE_ID
LEFT JOIN   CASHIER_SESSIONS CS
        ON   CS.CS_SESSION_ID = GTS.GTS_CASHIER_SESSION_ID
INNER JOIN   GAMING_TABLES_TYPES GTT
        ON   GTT.GTT_GAMING_TABLE_TYPE_ID = GT.GT_TYPE_ID 
     WHERE   GT.GT_ENABLED  = CASE WHEN @_STATUS = -1 THEN GT.GT_ENABLED ELSE @_STATUS END
       AND   GT.GT_AREA_ID  = CASE WHEN @_AREA   = '' THEN GT.GT_AREA_ID ELSE @_AREA END
       AND   GT.GT_BANK_ID  = CASE WHEN @_BANK   = '' THEN GT.GT_BANK_ID ELSE @_BANK END
       AND   GT.GT_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
       AND   (CS.CS_OPENING_DATE >= @_DATE_FROM AND CS.CS_OPENING_DATE < @_DATE_TO)  
       AND  GT.GT_HAS_INTEGRATED_CASHIER = 1
  GROUP BY   GT.GT_NAME, GTT.GTT_NAME, CS.CS_SESSION_ID
  ORDER BY   GTT.GTT_NAME
  
-- Check if cashiers must be visible  
IF @_CASHIERS = 1 
BEGIN

-- Select and join data to show cashiers
   -- Adding cashiers without gaming tables

  INSERT INTO #CHIPS_OPERATIONS_TABLE
       SELECT 1 AS TYPE_SESSION
              ,  CM_SESSION_ID AS SESSION_ID
              ,  CT_NAME as GT_NAME
              , @_CASHIERS_NAME as GTT_NAME
              , SUM(CM_SUB_AMOUNT) AS GTS_TOTAL_SALES_AMOUNT
              , SUM(CM_ADD_AMOUNT) AS GTS_TOTAL_PURCHASE_AMOUNT
         FROM   CASHIER_MOVEMENTS
   INNER JOIN   CASHIER_TERMINALS
           ON   CM_CASHIER_ID = CT_CASHIER_ID
        WHERE   CM_CASHIER_ID NOT IN (SELECT DISTINCT(GT_CASHIER_ID) FROM GAMING_TABLES)
          AND   CM_TYPE IN (303,304)
          AND   (CM_DATE >= @_DATE_FROM AND CM_DATE < @_DATE_TO)
     GROUP BY   CT_NAME, CM_SESSION_ID

END
-- Select results
SELECT * FROM #CHIPS_OPERATIONS_TABLE ORDER BY GT_NAME
  
-- DROP TEMPORARY TABLE  
DROP TABLE #CHIPS_OPERATIONS_TABLE
  
END  -- END PROCEDURE
GO

-- PERMISSIONS

GRANT EXECUTE ON [dbo].[GT_Chips_Operations] TO [wggui] WITH GRANT OPTION
GO

