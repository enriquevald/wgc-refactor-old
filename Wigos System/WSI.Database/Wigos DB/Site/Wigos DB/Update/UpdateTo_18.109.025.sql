/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 108;

SET @New_ReleaseId = 109;
SET @New_ScriptName = N'UpdateTo_18.109.025.sql';
SET @New_Description = N'Operations Schedule and MultiSite DB elements.';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/

--
-- Allow or not redeem points
--
ALTER TABLE dbo.accounts
        ADD CONSTRAINT DF_accounts_ac_points_status DEFAULT 0 FOR ac_points_status

GO

-- For anonymous accounts, set POINTS_STATUS = 0. 
UPDATE   ACCOUNTS
   SET   AC_POINTS_STATUS = 0
 WHERE   AC_USER_TYPE     = 0

GO

--
-- Operations Schedule
--
CREATE TABLE [dbo].[operations_schedule](
	[os_unique_id] [bigint] IDENTITY(1,1) NOT NULL,
	[os_type] [int] NOT NULL,
	[os_day_of_week] [int] NULL,
	[os_date_from] [datetime] NULL,
	[os_date_to] [datetime] NULL,
	[os_operations_allowed] [bit] NOT NULL,
	[os_time1_from] [int] NOT NULL,
	[os_time1_to] [int] NOT NULL,
	[os_time2_from] [int] NULL,
	[os_time2_to] [int] NULL,
 CONSTRAINT [PK_operations_schedule] PRIMARY KEY CLUSTERED 
(
	[os_unique_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[operations_schedule_status](
	[oss_type] [int] NOT NULL,
	[oss_allowed] [bit] NOT NULL,
	[oss_last_updated] [datetime] NOT NULL,
 CONSTRAINT [PK_operations_allowed] PRIMARY KEY CLUSTERED 
(
	[oss_type] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[operations_after_hours](
	[oah_from] [datetime] NOT NULL,
	[oah_to] [datetime] NULL,
	[oah_event_type] [bigint] NOT NULL,
	[oah_event_type_sent] [bigint] NOT NULL,
	[oah_changes] [int] NOT NULL,
	[oah_changes_sent] [int] NOT NULL,
	[oah_last_checked] [datetime] NULL,
	[oah_first_event_detected] [datetime] NULL,
 CONSTRAINT [PK_OPERATIONS_AFTER_HOURS] PRIMARY KEY CLUSTERED 
(
	[oah_from] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

--
-- Multisite
--
CREATE TABLE [dbo].[ms_site_tasks](
  [st_task_id] [int] NOT NULL,
  [st_enabled] [bit] NOT NULL CONSTRAINT [DF_ms_site_tasks_st_enabled]  DEFAULT ((1)),
  [st_running] [bit] NOT NULL CONSTRAINT [DF_ms_site_tasks_st_running]  DEFAULT ((0)),
  [st_started] [datetime] NULL,
  [st_local_sequence_id] [bigint] NULL,
  [st_remote_sequence_id] [bigint] NULL,
  [st_interval_seconds] [int] NULL,
  [st_last_run] [datetime] NULL,
  [st_num_pending] [int] NULL,
 CONSTRAINT [PK_ms_site_synch_control] PRIMARY KEY CLUSTERED 
(
  [st_task_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[ms_site_pending_account_movements](
      [spm_movement_id] [bigint] NOT NULL,
CONSTRAINT [PK_ms_site_pending_account_movements] PRIMARY KEY CLUSTERED 
(
      [spm_movement_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[ms_site_pending_accounts](
  [spa_account_id] [bigint] NOT NULL,
  [spa_guid] [uniqueidentifier] NULL,
 CONSTRAINT [PK_ms_site_pending_accounts] PRIMARY KEY CLUSTERED 
(
  [spa_account_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[ms_site_synchronize_accounts](
  [ssa_today] [datetime] NOT NULL,
  [ssa_track_data] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_ms_site_synchronize_accounts] PRIMARY KEY CLUSTERED 
(
  [ssa_today] ASC,
  [ssa_track_data] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[wwp_status](
      [wwp_type] [int] NOT NULL,
      [wwp_status] [int] NOT NULL CONSTRAINT [DF_wwp_status_wwp_status]  DEFAULT ((0)),
      [wwp_status_changed] [datetime] NULL,
      [wwp_last_sent_msg] [datetime] NULL,
      [wwp_last_address] [nvarchar](500) NULL,
CONSTRAINT [PK_wwp_status] PRIMARY KEY CLUSTERED 
(
      [wwp_type] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

--
-- ALTER table accounts
--
ALTER TABLE [dbo].[accounts] ADD 
      [ac_ms_has_local_changes] [bit] NULL,
      [ac_ms_change_guid] [uniqueidentifier] NULL,
      [ac_ms_created_on_site_id] [int] NULL,
      [ac_ms_modified_on_site_id] [int] NULL,
      [ac_ms_last_site_id] [int] NULL,
      [ac_ms_points_seq_id] [bigint] NULL,
      [ac_ms_points_synchronized] [datetime] NULL,
      [ac_ms_personal_info_seq_id] [bigint] NULL,
      [ac_ms_hash] [varbinary](20) NULL;

GO


/****** INDEXES ******/

/* Operations Schedule */
CREATE NONCLUSTERED INDEX [IX_os_type_date] ON [dbo].[operations_schedule] 
(
	[os_type] ASC,
	[os_date_to] ASC,
	[os_date_from] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

GO

CREATE NONCLUSTERED INDEX [IX_os_type_day_of_week] ON [dbo].[operations_schedule] 
(
	[os_type] ASC,
	[os_day_of_week] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

GO


/****** RECORDS ******/

--
-- Operations Schedule
--
DECLARE @closing_time AS INT
SELECT @closing_time = CAST(CASE WHEN GP_KEY_VALUE = '' THEN 0 ELSE GP_KEY_VALUE END AS INT) FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'WigosGUI' AND GP_SUBJECT_KEY = 'ClosingTime'
SET @closing_time = ISNULL(@closing_time, 0)

INSERT INTO OPERATIONS_SCHEDULE (OS_TYPE, OS_DAY_OF_WEEK, OS_DATE_FROM, OS_DATE_TO, OS_OPERATIONS_ALLOWED, OS_TIME1_FROM, OS_TIME1_TO, OS_TIME2_FROM, OS_TIME2_TO) VALUES (0, 0, NULL, NULL, 'True', @closing_time, @closing_time, NULL, NULL)
INSERT INTO OPERATIONS_SCHEDULE (OS_TYPE, OS_DAY_OF_WEEK, OS_DATE_FROM, OS_DATE_TO, OS_OPERATIONS_ALLOWED, OS_TIME1_FROM, OS_TIME1_TO, OS_TIME2_FROM, OS_TIME2_TO) VALUES (0, 1, NULL, NULL, 'True', @closing_time, @closing_time, NULL, NULL)
INSERT INTO OPERATIONS_SCHEDULE (OS_TYPE, OS_DAY_OF_WEEK, OS_DATE_FROM, OS_DATE_TO, OS_OPERATIONS_ALLOWED, OS_TIME1_FROM, OS_TIME1_TO, OS_TIME2_FROM, OS_TIME2_TO) VALUES (0, 2, NULL, NULL, 'True', @closing_time, @closing_time, NULL, NULL)
INSERT INTO OPERATIONS_SCHEDULE (OS_TYPE, OS_DAY_OF_WEEK, OS_DATE_FROM, OS_DATE_TO, OS_OPERATIONS_ALLOWED, OS_TIME1_FROM, OS_TIME1_TO, OS_TIME2_FROM, OS_TIME2_TO) VALUES (0, 3, NULL, NULL, 'True', @closing_time, @closing_time, NULL, NULL)
INSERT INTO OPERATIONS_SCHEDULE (OS_TYPE, OS_DAY_OF_WEEK, OS_DATE_FROM, OS_DATE_TO, OS_OPERATIONS_ALLOWED, OS_TIME1_FROM, OS_TIME1_TO, OS_TIME2_FROM, OS_TIME2_TO) VALUES (0, 4, NULL, NULL, 'True', @closing_time, @closing_time, NULL, NULL)
INSERT INTO OPERATIONS_SCHEDULE (OS_TYPE, OS_DAY_OF_WEEK, OS_DATE_FROM, OS_DATE_TO, OS_OPERATIONS_ALLOWED, OS_TIME1_FROM, OS_TIME1_TO, OS_TIME2_FROM, OS_TIME2_TO) VALUES (0, 5, NULL, NULL, 'True', @closing_time, @closing_time, NULL, NULL)
INSERT INTO OPERATIONS_SCHEDULE (OS_TYPE, OS_DAY_OF_WEEK, OS_DATE_FROM, OS_DATE_TO, OS_OPERATIONS_ALLOWED, OS_TIME1_FROM, OS_TIME1_TO, OS_TIME2_FROM, OS_TIME2_TO) VALUES (0, 6, NULL, NULL, 'True', @closing_time, @closing_time, NULL, NULL)

INSERT INTO OPERATIONS_SCHEDULE_STATUS (OSS_TYPE, OSS_ALLOWED, OSS_LAST_UPDATED) VALUES (0, 'True', GETDATE())

INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('OperationsSchedule', 'Enabled', '0')
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('OperationsSchedule', 'MailingEnabled', '0')
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('OperationsSchedule', 'Subject', '')
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('OperationsSchedule', 'MailingList', '0')

--
-- Multisite
--
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('MultiSite', 'MembersCardId', '')
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('MultiSite', 'CenterAddress1', '')
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('MultiSite', 'CenterAddress2', '')
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Site', 'Configured', '0')
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Site', 'MultiSiteMember', '0')

--SITE_TASK
INSERT INTO MS_SITE_TASKS (ST_TASK_ID, ST_ENABLED, ST_INTERVAL_SECONDS) VALUES ( 1, 1, 1200); --DownloadParameters
INSERT INTO MS_SITE_TASKS (ST_TASK_ID, ST_ENABLED, ST_INTERVAL_SECONDS) VALUES (11, 1,   60); --UploadPointsMovements
INSERT INTO MS_SITE_TASKS (ST_TASK_ID, ST_ENABLED, ST_INTERVAL_SECONDS) VALUES (12, 1,   60); --UploadPersonalInfo
INSERT INTO MS_SITE_TASKS (ST_TASK_ID, ST_ENABLED, ST_INTERVAL_SECONDS) VALUES (21, 0, 1200); --DownloadPoints_Site
INSERT INTO MS_SITE_TASKS (ST_TASK_ID, ST_ENABLED, ST_INTERVAL_SECONDS) VALUES (22, 1, 1200); --DownloadPoints_All 
INSERT INTO MS_SITE_TASKS (ST_TASK_ID, ST_ENABLED, ST_INTERVAL_SECONDS) VALUES (31, 0, 1200); --DownloadPersonalInfo_Card
INSERT INTO MS_SITE_TASKS (ST_TASK_ID, ST_ENABLED, ST_INTERVAL_SECONDS) VALUES (32, 0, 1200); --DownloadPersonalInfo_Site
INSERT INTO MS_SITE_TASKS (ST_TASK_ID, ST_ENABLED, ST_INTERVAL_SECONDS) VALUES (33, 1, 1200); --DownloadPersonalInfo_All 

-- WWP_STATUS
INSERT INTO WWP_STATUS (WWP_TYPE, WWP_STATUS) VALUES (1, 0);--MULTISITE,
INSERT INTO WWP_STATUS (WWP_TYPE, WWP_STATUS) VALUES (2, 0);--SUPERCENTER 

GO


/****** TRIGGERS ******/

--
-- MultiSite
--
CREATE TRIGGER [dbo].[MultiSiteTrigger_SiteAccountUpdate] ON [dbo].[accounts]
   AFTER UPDATE
NOT FOR REPLICATION
AS 
BEGIN
    DECLARE @AccountId          AS BIGINT
    DECLARE @hash0              as varbinary(20)
    DECLARE @hash1              as varbinary(20)
    DECLARE @value              as nvarchar(max)
    DECLARE @changed            as bit
    DECLARE @updated            AS BIT
    IF (UPDATE(AC_MS_CHANGE_GUID)) RETURN

    SET @updated = 0;  

IF UPDATE (AC_TRACK_DATA)
    OR UPDATE (AC_HOLDER_NAME)
    OR UPDATE (AC_HOLDER_ID)
    OR UPDATE (AC_HOLDER_ID_TYPE)
    OR UPDATE (AC_HOLDER_ADDRESS_01)
    OR UPDATE (AC_HOLDER_ADDRESS_02)
    OR UPDATE (AC_HOLDER_ADDRESS_03)
    OR UPDATE (AC_HOLDER_CITY)
    OR UPDATE (AC_HOLDER_ZIP)
    OR UPDATE (AC_HOLDER_EMAIL_01)
    OR UPDATE (AC_HOLDER_EMAIL_02)
    OR UPDATE (AC_HOLDER_PHONE_NUMBER_01)
    OR UPDATE (AC_HOLDER_PHONE_NUMBER_02)
    OR UPDATE (AC_HOLDER_COMMENTS)
    OR UPDATE (AC_HOLDER_ID1)
    OR UPDATE (AC_HOLDER_ID2)
    OR UPDATE (AC_HOLDER_DOCUMENT_ID1)
    OR UPDATE (AC_HOLDER_DOCUMENT_ID2)
    OR UPDATE (AC_HOLDER_NAME1)
    OR UPDATE (AC_HOLDER_NAME2)
    OR UPDATE (AC_HOLDER_NAME3)
    OR UPDATE (AC_HOLDER_GENDER)
    OR UPDATE (AC_HOLDER_MARITAL_STATUS)
    OR UPDATE (AC_HOLDER_BIRTH_DATE)
    OR UPDATE (AC_HOLDER_WEDDING_DATE)
    OR UPDATE (AC_HOLDER_LEVEL)
    OR UPDATE (AC_HOLDER_LEVEL_NOTIFY)
    OR UPDATE (AC_HOLDER_LEVEL_ENTERED)
    OR UPDATE (AC_HOLDER_LEVEL_EXPIRATION)
    OR UPDATE (AC_PIN)
    OR UPDATE (AC_PIN_FAILURES)
    OR UPDATE (AC_PIN_LAST_MODIFIED)
    OR UPDATE (AC_BLOCKED)
    OR UPDATE (AC_ACTIVATED)
    OR UPDATE (AC_BLOCK_REASON)
    OR UPDATE (AC_HOLDER_IS_VIP)
    OR UPDATE (AC_HOLDER_TITLE)
    OR UPDATE (AC_HOLDER_NAME4)
    OR UPDATE (AC_HOLDER_PHONE_TYPE_01)
    OR UPDATE (AC_HOLDER_PHONE_TYPE_02)
    OR UPDATE (AC_HOLDER_STATE)
    OR UPDATE (AC_HOLDER_COUNTRY)
    OR UPDATE (AC_USER_TYPE)
        SET @updated = 1;
        
    IF (@updated = 0) RETURN

    DECLARE PersonalInfoCursor CURSOR FOR 
     SELECT   AC_ACCOUNT_ID
            , HASHBYTES ('SHA1',  ISNULL(AC_TRACK_DATA,        '')
                                + ISNULL(AC_HOLDER_NAME,       '')
                                + ISNULL(AC_HOLDER_ID,         '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_ID_TYPE), '')
                                + ISNULL(AC_HOLDER_ADDRESS_01, '')
                                + ISNULL(AC_HOLDER_ADDRESS_02, '')
                                + ISNULL(AC_HOLDER_ADDRESS_03, '')
                                + ISNULL(AC_HOLDER_CITY,       '')
                                + ISNULL(AC_HOLDER_ZIP,        '')
                                + ISNULL(AC_HOLDER_EMAIL_01,   '')
                                + ISNULL(AC_HOLDER_EMAIL_02,   '')
                                + ISNULL(AC_HOLDER_PHONE_NUMBER_01, '')
                                + ISNULL(AC_HOLDER_PHONE_NUMBER_02, '')
                                + ISNULL(AC_HOLDER_COMMENTS,        '')
                                + ISNULL(AC_HOLDER_ID1, '')
                                + ISNULL(AC_HOLDER_ID2, '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_DOCUMENT_ID1), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_DOCUMENT_ID2), '')
                                + ISNULL(AC_HOLDER_NAME1, '')
                                + ISNULL(AC_HOLDER_NAME2, '')
                                + ISNULL(AC_HOLDER_NAME3, '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_GENDER),         '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_MARITAL_STATUS), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_BIRTH_DATE,   21), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_WEDDING_DATE, 21), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_LEVEL),                '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_LEVEL_NOTIFY),         '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_LEVEL_ENTERED,    21), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_LEVEL_EXPIRATION, 21), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_PIN),          '')
                                + ISNULL(CONVERT(NVARCHAR, AC_PIN_FAILURES), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_PIN_LAST_MODIFIED, 21), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_BLOCKED),           '')
                                + ISNULL(CONVERT(NVARCHAR, AC_ACTIVATED),         '')
                                + ISNULL(CONVERT(NVARCHAR, AC_BLOCK_REASON),         '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_IS_VIP),        '')
                                + ISNULL(AC_HOLDER_TITLE                        , '')
                                + ISNULL(AC_HOLDER_NAME4                        , '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_PHONE_TYPE_01),        '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_PHONE_TYPE_02),        '')
                                + ISNULL(AC_HOLDER_STATE                        , '')
                                + ISNULL(AC_HOLDER_COUNTRY                      , '') 
                                + ISNULL(CONVERT(NVARCHAR,AC_USER_TYPE)         , '') )
       FROM   INSERTED
      WHERE   AC_TRACK_DATA not like '%-NEW-%'

    SET NOCOUNT ON;

    OPEN PersonalInfoCursor

    FETCH NEXT FROM PersonalInfoCursor INTO @AccountId, @hash1

    WHILE @@FETCH_STATUS = 0
    BEGIN

        SELECT @hash0 = AC_MS_HASH FROM DELETED WHERE AC_ACCOUNT_ID = @AccountId

        SELECT @changed = CASE WHEN ( @hash0 = @hash1 ) THEN 0 ELSE 1 END

        IF @changed = 1
        BEGIN
           DECLARE @new_id as uniqueidentifier
           
           SET @new_id = NEWID()
           
            -- Personal Info
            UPDATE   ACCOUNTS
               SET   AC_MS_HASH                 = @hash1
                   , AC_MS_CHANGE_GUID          = @new_id
             WHERE   AC_ACCOUNT_ID              = @AccountId
             
            IF NOT EXISTS (SELECT 1 FROM MS_SITE_PENDING_ACCOUNTS WHERE SPA_ACCOUNT_ID = @AccountId)
              INSERT INTO   MS_SITE_PENDING_ACCOUNTS (SPA_ACCOUNT_ID, SPA_GUID) VALUES (@AccountId, @new_id )
            ELSE 
              UPDATE MS_SITE_PENDING_ACCOUNTS SET SPA_GUID = @new_id WHERE SPA_ACCOUNT_ID = @AccountId
            
        END

        FETCH NEXT FROM PersonalInfoCursor INTO @AccountId, @hash1
    END

    CLOSE PersonalInfoCursor
    DEALLOCATE PersonalInfoCursor

END

GO

CREATE TRIGGER [dbo].[MultiSiteTrigger_SiteAccountInsert] ON [dbo].[accounts]
   AFTER INSERT
NOT FOR REPLICATION
AS 
BEGIN
    DECLARE @Sequence10Value AS BIGINT
    DECLARE @Sequence11Value AS BIGINT
    DECLARE @AccountId       AS BIGINT
    DECLARE @NewId as uniqueidentifier
    
    DECLARE InsertedCursor CURSOR FOR 
     SELECT   INSERTED.AC_ACCOUNT_ID 
            , INSERTED.AC_MS_CHANGE_GUID            
       FROM   INSERTED
      WHERE   INSERTED.AC_TRACK_DATA NOT LIKE '%-NEW-%' 

    SET NOCOUNT ON;

    OPEN InsertedCursor

    FETCH NEXT FROM InsertedCursor INTO @AccountId, @NewId
    
    WHILE @@FETCH_STATUS = 0
    BEGIN      
      
      IF NOT EXISTS (SELECT 1 FROM MS_SITE_PENDING_ACCOUNTS WHERE SPA_ACCOUNT_ID = @AccountId)
      BEGIN
        INSERT INTO   MS_SITE_PENDING_ACCOUNTS 
                    ( SPA_ACCOUNT_ID
                    , SPA_GUID)
             VALUES ( @AccountId
                    , @NewId )
      END

      FETCH NEXT FROM InsertedCursor INTO @AccountId, @NewId
    END

    CLOSE InsertedCursor
    DEALLOCATE InsertedCursor

END

GO

CREATE TRIGGER [dbo].[Trigger_SiteToMultiSite_Points]
ON [dbo].[ACCOUNT_MOVEMENTS]
AFTER INSERT
NOT FOR REPLICATION
AS
BEGIN
  SET NOCOUNT ON

  -- Insert movement to synchronize
  INSERT INTO   MS_SITE_PENDING_ACCOUNT_MOVEMENTS 
              ( SPM_MOVEMENT_ID )
       SELECT   AM_MOVEMENT_ID 
         FROM   INSERTED 
        WHERE   AM_TYPE IN ( 36, 37, 38, 39, 40, 41, 42, 43, 46, 50, 60, 61, 62, 66, 67 ) 
 
  SET NOCOUNT OFF

END 

GO


/****** STORE PROCEDURES ******/

--
-- MultiSite
--
CREATE PROCEDURE [dbo].[CreateAccount]
@AccountId    BIGINT       OUTPUT
AS
BEGIN
	DECLARE @SiteId            as INT
	DECLARE @NewTrackData      as NVARCHAR(50)

	SELECT   @SiteId = CAST(GP_KEY_VALUE AS INT) 
	  FROM   GENERAL_PARAMS 
	 WHERE   GP_GROUP_KEY   = 'Site' 
	   AND   GP_SUBJECT_KEY = 'Identifier'
	   AND   ISNUMERIC(GP_KEY_VALUE) = 1

	IF ( ISNULL(@SiteId, 0) <= 0 ) 
		RAISERROR ('SiteId not configured!', 20, 0) WITH LOG

	IF @SiteId > 999
		RAISERROR ('SiteId is greater than 999', 20, 0) WITH LOG

		
    SET @NewTrackData = '-RECYCLED-NEW-' + CAST (NEWID() AS NVARCHAR(50))
		
	UPDATE   SEQUENCES 
	   SET   SEQ_NEXT_VALUE = SEQ_NEXT_VALUE + 1
	 WHERE   SEQ_ID         = 4

	IF @@ROWCOUNT = 0 
	BEGIN
		RAISERROR ('Sequence #4 does not exist!', 20, 0) WITH LOG
	END

	SELECT @AccountId = SEQ_NEXT_VALUE - 1 FROM SEQUENCES WHERE SEQ_ID  = 4

	SET @AccountId = 1000000 + @AccountId * 1000 + @SiteId % 1000
    
	INSERT INTO ACCOUNTS (AC_ACCOUNT_ID, AC_TYPE, AC_BLOCKED, AC_TRACK_DATA) VALUES (@AccountId, 2, 1, @NewTrackData)

END

GO

CREATE PROCEDURE [dbo].[Update_PersonalInfo]
  @pAccountId bigint
, @pTrackData nvarchar(50)
,	@pHolderName nvarchar(200)
,	@pHolderId nvarchar(20)
, @pHolderIdType int
, @pHolderAddress01 nvarchar(50)
, @pHolderAddress02 nvarchar(50)
, @pHolderAddress03 nvarchar(50)
, @pHolderCity nvarchar(50)
, @pHolderZip  nvarchar(10) 
, @pHolderEmail01 nvarchar(50)
,	@pHolderEmail02 nvarchar(50)
,	@pHolderPhoneNumber01 nvarchar(20)
, @pHolderPhoneNumber02 nvarchar(20)
, @pHolderComments nvarchar(100)
, @pHolderId1 nvarchar(20)
, @pHolderId2 nvarchar(20)
, @pHolderDocumentId1 bigint
, @pHolderDocumentId2 bigint
, @pHolderName1 nvarchar(50)
,	@pHolderName2 nvarchar(50)
,	@pHolderName3 nvarchar(50)
, @pHolderGender  int
, @pHolderMaritalStatus int
, @pHolderBirthDate datetime
, @pHolderWeddingDate datetime
, @pHolderLevel int
, @pHolderLevelNotify int
, @pHolderLevelEntered datetime
,	@pHolderLevelExpiration datetime
,	@pPin nvarchar(12)
, @pPinFailures int
, @pPinLastModified datetime
, @pBlocked bit
, @pActivated bit
, @pBlockReason int
, @pHolderIsVip int
, @pHolderTitle nvarchar(15)                       
, @pHolderName4 nvarchar(50)                       
, @pHolderPhoneType01  int
, @pHolderPhoneType02  int
, @pHolderState    nvarchar                    
, @pHolderCountry  nvarchar                     
, @pPersonalInfoSequenceId  bigint                     
, @pUserType int
AS
BEGIN

DECLARE @pOtherAccountId as BIGINT

SET @pOtherAccountId = ISNULL ((SELECT AC_ACCOUNT_ID FROM ACCOUNTS WHERE AC_TRACK_DATA = @pTrackData), 0)

IF @pOtherAccountId <> 0 AND @pOtherAccountId <> @pAccountId
  UPDATE   ACCOUNTS   
     SET   AC_TRACK_DATA =  '-RECYCLED-NEW-' + CAST (NEWID() AS NVARCHAR(50))
   WHERE   AC_ACCOUNT_ID = @pOtherAccountId


IF NOT EXISTS (SELECT 1 FROM ACCOUNTS WHERE ac_account_id = @pAccountId )  
BEGIN
  INSERT INTO   ACCOUNTS 
              ( AC_ACCOUNT_ID, AC_TYPE, AC_BLOCKED, AC_TRACK_DATA) 
       VALUES ( @pAccountId,         2,  @pBlocked, @pTrackData )
END  
  
 
  UPDATE   ACCOUNTS                   
     SET   AC_TRACK_DATA              = @pTrackData 
         , AC_HOLDER_NAME             = @pHolderName 
         , AC_HOLDER_ID               = @pHolderId 
         , AC_HOLDER_ID_TYPE          = @pHolderIdType 
         , AC_HOLDER_ADDRESS_01       = @pHolderAddress01 
         , AC_HOLDER_ADDRESS_02       = @pHolderAddress02 
         , AC_HOLDER_ADDRESS_03       = @pHolderAddress03 
         , AC_HOLDER_CITY             = @pHolderCity 
         , AC_HOLDER_ZIP              = @pHolderZip  
         , AC_HOLDER_EMAIL_01         = @pHolderEmail01 
         , AC_HOLDER_EMAIL_02         = @pHolderEmail02 
         , AC_HOLDER_PHONE_NUMBER_01  = @pHolderPhoneNumber01 
         , AC_HOLDER_PHONE_NUMBER_02  = @pHolderPhoneNumber02 
         , AC_HOLDER_COMMENTS         = @pHolderComments 
         , AC_HOLDER_ID1              = @pHolderId1 
         , AC_HOLDER_ID2              = @pHolderId2 
         , AC_HOLDER_DOCUMENT_ID1     = @pHolderDocumentId1 
         , AC_HOLDER_DOCUMENT_ID2     = @pHolderDocumentId2 
         , AC_HOLDER_NAME1            = @pHolderName1 
         , AC_HOLDER_NAME2            = @pHolderName2 
         , AC_HOLDER_NAME3            = @pHolderName3 
         , AC_HOLDER_GENDER           = @pHolderGender  
         , AC_HOLDER_MARITAL_STATUS   = @pHolderMaritalStatus 
         , AC_HOLDER_BIRTH_DATE       = @pHolderBirthDate 
         , AC_HOLDER_WEDDING_DATE     = @pHolderWeddingDate 
         , AC_HOLDER_LEVEL            = @pHolderLevel 
         , AC_HOLDER_LEVEL_NOTIFY     = @pHolderLevelNotify 
         , AC_HOLDER_LEVEL_ENTERED    = @pHolderLevelEntered 
         , AC_HOLDER_LEVEL_EXPIRATION = @pHolderLevelExpiration 
         , AC_PIN                     = @pPin 
         , AC_PIN_FAILURES            = @pPinFailures 
         , AC_PIN_LAST_MODIFIED       = @pPinLastModified 
         , AC_BLOCKED                 = @pBlocked 
         , AC_ACTIVATED               = @pActivated 
         , AC_BLOCK_REASON            = @pBlockReason 
         , AC_HOLDER_IS_VIP           = @pHolderIsVip 
         , AC_HOLDER_TITLE            = @pHolderTitle 
         , AC_HOLDER_NAME4            = @pHolderName4         
         , AC_HOLDER_PHONE_TYPE_01    = @pHolderPhoneType01  
         , AC_HOLDER_PHONE_TYPE_02    = @pHolderPhoneType02  
         , AC_HOLDER_STATE            = @pHolderState    
         , AC_HOLDER_COUNTRY          = @pHolderCountry  					
         , AC_MS_PERSONAL_INFO_SEQ_ID = @pPersonalInfoSequenceId           
         , AC_USER_TYPE				        = @pUserType
   WHERE   AC_ACCOUNT_ID              = @pAccountId 

END

GO

CREATE PROCEDURE [dbo].[Update_PointsInAccount]
	@pMovementId                BIGINT
,	@pAccountId                 BIGINT
, @pErrorCode                 INT         
, @pPointsSequenceId          BIGINT      
, @pReceivedPoints            MONEY       
, @pPointsStatus              INT           
, @pHolderLevel               INT         
, @pHolderLevelEntered        DATETIME    
, @pHolderLevelExpiration     DATETIME    

AS
BEGIN   
  DECLARE @LocalDelta AS INT

  IF NOT EXISTS (SELECT 1 FROM ACCOUNTS WHERE ac_account_id = @pAccountId )   
    INSERT INTO   ACCOUNTS 
                ( AC_ACCOUNT_ID, AC_TYPE, AC_BLOCKED, AC_TRACK_DATA) 
         VALUES ( @pAccountId,         2,          1, '-RECYCLED-NEW-' + CAST (NEWID() AS NVARCHAR(50)) )
         
  IF ((@pMovementId <> 0)AND(@pErrorCode = 0 OR @pErrorCode = 2))
    DELETE   MS_SITE_PENDING_ACCOUNT_MOVEMENTS
     WHERE   SPM_MOVEMENT_ID = @pMovementId
  
  IF (@pErrorCode <> 0) RETURN
  
  DECLARE @PrevSequence as BIGINT
  
  
  -- Lock the account and later add the local points
  UPDATE ACCOUNTS SET AC_POINTS = AC_POINTS + 0 WHERE AC_ACCOUNT_ID = @pAccountId

  SET @PrevSequence = (SELECT ISNULL(AC_MS_POINTS_SEQ_ID, 0) FROM ACCOUNTS WHERE AC_ACCOUNT_ID = @pAccountId)
  
  IF ( @PrevSequence >= @pPointsSequenceId ) RETURN
  
-- Local Points
  SELECT   @LocalDelta = SUM (am_add_amount - am_sub_amount) 
    FROM   ACCOUNT_MOVEMENTS, MS_SITE_PENDING_ACCOUNT_MOVEMENTS
   WHERE   AM_MOVEMENT_ID = SPM_MOVEMENT_ID
     AND   AM_ACCOUNT_ID  = @pAccountId
     AND   AM_TYPE IN (36,37,38,39,40,41,42,46,50,60,61,66) --Not included level movements
  

   	UPDATE   ACCOUNTS
	     SET   AC_POINTS                  = @pReceivedPoints + ISNULL (@LocalDelta, 0)
	         , AC_MS_POINTS_SEQ_ID        = @pPointsSequenceId
	         , AC_MS_POINTS_SYNCHRONIZED  = GETDATE ()
           , AC_POINTS_STATUS           = @pPointsStatus
           , AC_HOLDER_LEVEL_EXPIRATION = @pHolderLevelExpiration   
           , AC_HOLDER_LEVEL_ENTERED    = @pHolderLevelEntered
           , AC_HOLDER_LEVEL            = @pHolderLevel
	   WHERE   AC_ACCOUNT_ID              = @pAccountId
	     AND   ISNULL(AC_MS_POINTS_SEQ_ID, 0) < @pPointsSequenceId
END

GO

-- Insert the sequence for account IDs
DECLARE @AuxId as BIGINT

SELECT @AuxId = MAX(AC_ACCOUNT_ID) FROM ACCOUNTS
SET @AuxId = 1 + ISNULL(@AuxId, 0) 

IF @AuxId >= 1000000 
    RAISERROR ('Site too many accounts to become part of a MultiSite!', 20, 0) WITH LOG

INSERT INTO SEQUENCES (SEQ_ID, SEQ_NEXT_VALUE) VALUES (4, @AuxId);


/****** PERMISSIONS ******/

--
-- Multisite
--
GRANT EXECUTE ON [dbo].[CreateAccount] TO [wggui] WITH GRANT OPTION
