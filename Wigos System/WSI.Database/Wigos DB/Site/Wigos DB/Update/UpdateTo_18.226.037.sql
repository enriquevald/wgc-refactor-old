/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 225;

SET @New_ReleaseId = 226;
SET @New_ScriptName = N'UpdateTo_18.226.037.sql';
SET @New_Description = N'GP Added: Account.UniqueField - Name4, TITO - CloseSessionWhenRemoveCard';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/******* TABLES *******/

/******* INDEXES *******/

/******* RECORDS *******/

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Account.UniqueField' AND GP_SUBJECT_KEY = 'Name4')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('Account.UniqueField','Name4','0')
GO

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'TITO' AND GP_SUBJECT_KEY ='CloseSessionWhenRemoveCard')
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('TITO'
           ,'CloseSessionWhenRemoveCard'
           ,'0')
GO

/******* STORED PROCEDURES *******/

--------------------------------------------------------------------------------
-- Copyright � 2014 Win Systems International
--------------------------------------------------------------------------------
--
--   MODULE NAME: Update_AlarmPatterns.sql
--
--   DESCRIPTION: Update Patterns in the Site
--
--        AUTHOR: Jos� Mart�nez
--
-- CREATION DATE: 16-MAY-2014
--
-- REVISION HISTORY:
--
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 16-MAY-2014 JML    First release.  
-- 08-JUL-2014 JPJ    Added multilanguage support
-- 06-AUG-2014 DCS    Fixed Bug WIG-1162: Alarm catalog without update
-------------------------------------------------------------------------------- 

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Update_AlarmPatterns]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Update_AlarmPatterns]
GO 

CREATE PROCEDURE   [dbo].[Update_AlarmPatterns]
                    @pId                 BIGINT
                  , @pName               NVARCHAR(50)
                  , @pDescription        NVARCHAR(350)
                  , @pActive             BIT
                  , @pPattern            XML
                  , @pAlCode             INT
                  , @pAlName             NVARCHAR(50)
                  , @pAlDescription      NVARCHAR(350)
                  , @pAlSeverity         INT
                  , @pType               INT
                  , @pSource             INT
                  , @pLifeTime           INT
                  , @pLastFind           DATETIME
                  , @pDetections         INT
                  , @pScheduleTimeFrom   INT
                  , @pScheduleTimeTo     INT
                  , @pTimestamp          BIGINT

AS
BEGIN

  IF NOT EXISTS (SELECT 1 FROM PATTERNS WHERE PT_ID = @pId)
  
  BEGIN
    SET IDENTITY_INSERT PATTERNS ON

    INSERT INTO   PATTERNS ( PT_ID
                           , PT_NAME
                           , PT_DESCRIPTION
                           , PT_ACTIVE
                           , PT_PATTERN
                           , PT_AL_CODE
                           , PT_AL_NAME
                           , PT_AL_DESCRIPTION
                           , PT_AL_SEVERITY
                           , PT_TYPE
                           , PT_SOURCE
                           , PT_LIFE_TIME
                           , PT_LAST_FIND
                           , PT_DETECTIONS
                           , PT_SCHEDULE_TIME_FROM
                           , PT_SCHEDULE_TIME_TO
                           , PT_TIMESTAMP )
                    VALUES ( @pId 
                           , @pName
                           , @pDescription
                           , @pActive
                           , @pPattern
                           , @pAlCode
                           , @pAlName
                           , @pAlDescription
                           , @pAlSeverity
                           , @pType
                           , @pSource
                           , @pLifeTime
                           , NULL
                           , 0
                           , @pScheduleTimeFrom
                           , @pScheduleTimeTo
                           , @pTimestamp ) 

    SET IDENTITY_INSERT PATTERNS OFF
    
		-- English language
	  INSERT INTO ALARM_CATALOG (ALCG_ALARM_CODE,  ALCG_LANGUAGE_ID, ALCG_TYPE, ALCG_NAME, ALCG_DESCRIPTION, ALCG_VISIBLE)
												 VALUES (@pAlCode,         9,                1,         @pAlName,  @pAlDescription,  1           )
	  
		-- Spanish language
		INSERT INTO ALARM_CATALOG (ALCG_ALARM_CODE, ALCG_LANGUAGE_ID, ALCG_TYPE, ALCG_NAME, ALCG_DESCRIPTION, ALCG_VISIBLE)
												 VALUES (@pAlCode,        10,               1,         @pAlName,  @pAlDescription,  1           )
    
    -- Insert alarm in his category 
		INSERT INTO ALARM_CATALOG_PER_CATEGORY (ALCC_ALARM_CODE, ALCC_CATEGORY, ALCC_TYPE, ALCC_DATETIME)
																			VALUES (@pAlCode,        44,            1,         GetDate()    )
  
  END
  ELSE
  BEGIN
    UPDATE   PATTERNS
       SET   PT_NAME               = @pName
           , PT_DESCRIPTION        = @pDescription
           , PT_ACTIVE             = @pActive
           , PT_PATTERN            = @pPattern
           , PT_AL_CODE            = @pAlCode
           , PT_AL_NAME            = @pAlName
           , PT_AL_DESCRIPTION     = @pAlDescription
           , PT_AL_SEVERITY        = @pAlSeverity
           , PT_TYPE               = @pType
           , PT_SOURCE             = @pSource
           , PT_LIFE_TIME          = @pLifeTime
           , PT_SCHEDULE_TIME_FROM = @pScheduleTimeFrom
           , PT_SCHEDULE_TIME_TO   = @pScheduleTimeTo
           , PT_TIMESTAMP          = @pTimestamp
     WHERE   PT_ID  = @pId
     
     UPDATE ALARM_CATALOG 
			 SET   ALCG_DESCRIPTION			= @pAlDescription
					 , ALCG_NAME						= @pAlName
		 WHERE   ALCG_ALARM_CODE			= @pAlCode   
		 
  END

END
GO 


