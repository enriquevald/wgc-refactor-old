/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 308;

SET @New_ReleaseId = 309;
SET @New_ScriptName = N'UpdateTo_18.309.039.sql';
SET @New_Description = N'Add in SAS Meters catalog (bills & tickets) foreign currencies;Update field bucket_levels';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/******* TABLES  *******/

ALTER TABLE bucket_levels ALTER COLUMN bul_a_factor decimal(18, 2) NOT NULL
GO

ALTER TABLE bucket_levels ALTER COLUMN bul_b_factor decimal(18, 2) NOT NULL
GO

/******* INDEXES *******/

/******* RECORDS *******/

DELETE TERMINAL_SAS_METERS WHERE TSM_DENOMINATION <> 0 
GO

DELETE TERMINAL_SAS_METERS_HISTORY WHERE TSMH_DENOMINATION <> 0
GO

DECLARE @CurrencyISOCode           AS NVARCHAR(3)
SELECT @CurrencyISOCode = GP_KEY_VALUE  FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'RegionalOptions' AND GP_SUBJECT_KEY = 'CurrencyISOCode'

IF (@CurrencyISOCode <> 'MXN')
BEGIN 
 UPDATE general_params 
 SET GP_KEY_VALUE = @CurrencyISOCode + ';5;25;50;75;100'
 WHERE GP_GROUP_KEY = 'DisplayTouch' AND GP_SUBJECT_KEY = 'CustomButtons.TransferAmount.NR' AND GP_KEY_VALUE = 'MXN;5;25;50;75;100'
END

GO

CREATE PROCEDURE [dbo].[TMP_InsertInSasMeterCatalog] 
  @pBill AS DECIMAL
AS
BEGIN   
  DECLARE @_meter_code AS INTEGER
	
  SET @_meter_code = -1
  
  SELECT @_meter_code = 
    CASE WHEN @pBill = 1       THEN 64	
         WHEN @pBill = 2       THEN 65	
         WHEN @pBill = 5       THEN 66	
         WHEN @pBill = 10      THEN 67	
         WHEN @pBill = 20      THEN 68	
         WHEN @pBill = 25      THEN 69	
         WHEN @pBill = 50      THEN 70	
         WHEN @pBill = 100     THEN 71	
         WHEN @pBill = 200     THEN 72	
         WHEN @pBill = 250     THEN 73	
         WHEN @pBill = 500     THEN 74	
         WHEN @pBill = 1000    THEN 75	
         WHEN @pBill = 2000    THEN 76	
         WHEN @pBill = 2500    THEN 77	
         WHEN @pBill = 5000    THEN 78	
         WHEN @pBill = 10000   THEN 79	
         WHEN @pBill = 20000   THEN 80	
         WHEN @pBill = 25000   THEN 81	
         WHEN @pBill = 50000   THEN 82	
         WHEN @pBill = 100000  THEN 83	
         WHEN @pBill = 200000  THEN 84	
         WHEN @pBill = 250000  THEN 85	
         WHEN @pBill = 500000  THEN 86	
         WHEN @pBill = 1000000 THEN 87	
    END
  
  IF (ISNULL(@_meter_code,-1) = -1)
    SELECT 'Meter code not inserted: Bill -> ' +  CONVERT(VARCHAR(50),@pBill)
  ELSE IF (NOT EXISTS(SELECT * FROM SAS_METERS_CATALOG_PER_GROUP WHERE SMCG_GROUP_ID = 10002 AND SMCG_METER_CODE = @_meter_code))
    INSERT INTO SAS_METERS_CATALOG_PER_GROUP (SMCG_GROUP_ID,SMCG_METER_CODE) VALUES (10002,@_meter_code)
  
END  

GO 

DECLARE @CurrentCurrencyAccepted   AS NVARCHAR(3)
DECLARE @CurrentCurrencyDenom      AS DECIMAL
DECLARE @CurrencyISOCode           AS NVARCHAR(3)


SELECT @CurrencyISOCode = GP_KEY_VALUE  FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'RegionalOptions' AND GP_SUBJECT_KEY = 'CurrencyISOCode'

  
DECLARE Curs_Denoms CURSOR FOR 
	SELECT DISTINCT CGC_DENOMINATION FROM cage_currencies 
	WHERE cgc_cage_currency_type = 0 AND cgc_denomination > 0 AND cgc_iso_code <> @CurrencyISOCode

SET NOCOUNT ON;
OPEN Curs_Denoms

FETCH NEXT FROM Curs_Denoms INTO @CurrentCurrencyDenom
  
WHILE @@FETCH_STATUS = 0
  BEGIN     	    
	EXEC dbo.TMP_InsertInSasMeterCatalog @CurrentCurrencyDenom
	
	FETCH NEXT FROM Curs_Denoms INTO @CurrentCurrencyDenom    
  END
    
CLOSE Curs_Denoms
DEALLOCATE Curs_Denoms  


DROP PROCEDURE [dbo].[TMP_InsertInSasMeterCatalog]

GO


/******* PROCEDURES *******/
