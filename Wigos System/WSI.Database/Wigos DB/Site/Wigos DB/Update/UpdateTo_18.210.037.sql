/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 209;

SET @New_ReleaseId = 210;
SET @New_ScriptName = N'UpdateTo_18.210.037.sql';
SET @New_Description = N'Pattern Task; New GP: Cashier - AnyRedeemableExpireAsPrize/CreditsExpireDailyWhenBalanceLowerThanCents ';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/******* TABLES *******/

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.patterns') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[patterns](
	[pt_id] [bigint] IDENTITY(1,1) NOT NULL,
	[pt_name] [nvarchar](50) NOT NULL,
	[pt_description] [nvarchar](350) NOT NULL,
	[pt_active] [bit] NOT NULL,
	[pt_pattern] [xml] NOT NULL,
	[pt_al_code] [int] NOT NULL,
	[pt_al_name] [nvarchar](50) NOT NULL,
	[pt_al_description] [nvarchar](350) NULL,
	[pt_al_severity] [int] NOT NULL,
	[pt_type] [int] NOT NULL,
	[pt_source] [int] NOT NULL,
	[pt_life_time] [int] NOT NULL,
	[pt_last_find] [datetime] NULL,
	[pt_detections] [int] NOT NULL,
	[pt_restricted_to_terminal_list] [xml] NULL,
	[pt_schedule_time_from] [int] NULL,
	[pt_schedule_time_to] [int] NULL,
	[pt_timestamp] [bigint] NULL,
 CONSTRAINT [PK_patterns] PRIMARY KEY CLUSTERED 
(
	[pt_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.patterns_information') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[patterns_information](
	[pti_id] [bigint] IDENTITY(1,1) NOT NULL,
	[pti_state] [int] NOT NULL,
	[pti_open_patterns] [xml] NULL,
	[pti_sequence] [bigint] NOT NULL,
	[pti_creation] [datetime] NOT NULL,
 CONSTRAINT [PK_patterns_information] PRIMARY KEY CLUSTERED 
(
	[pti_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.historical_generated_patterns') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[historical_generated_patterns](
	[hgp_id] [bigint] IDENTITY(1,1) NOT NULL,
	[hgp_al_id] [bigint] NOT NULL,
	[hgp_pattern_id] [bigint] NOT NULL,
	[hgp_pattern_values] [xml] NOT NULL,
	[hgp_date] [datetime] NOT NULL,
	[hgp_element_id] [bigint] NOT NULL,
	[hgp_alarm_code] [int] NOT NULL,
	[hgp_alarm_name] [nvarchar](50) NOT NULL,
	[hgp_alarm_description] [nvarchar](350) NULL,
 CONSTRAINT [PK_historical_generated_patterns] PRIMARY KEY CLUSTERED 
(
	[hgp_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[alarm_groups]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[alarm_groups](
	[alg_alarm_group_id] [int] NOT NULL,
	[alg_type] [int] NOT NULL,
	[alg_name] [nvarchar](50) NOT NULL,
	[alg_description] [nvarchar](200) NULL,
	[alg_visible] [int] NOT NULL,
 CONSTRAINT [PK_alarm_group] PRIMARY KEY CLUSTERED 
(
	[alg_alarm_group_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[alarm_categories]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[alarm_categories](
	[alc_category_id] [int] NOT NULL,
	[alc_alarm_group_id] [int] NOT NULL,
	[alc_type] [int] NOT NULL,
	[alc_name] [nvarchar](50) NOT NULL,
	[alc_description] [nvarchar](200) NULL,
	[alc_visible] [int] NOT NULL,
 CONSTRAINT [PK_category] PRIMARY KEY CLUSTERED 
(
	[alc_category_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[alarm_catalog_per_category]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[alarm_catalog_per_category](
	[alcc_alarm_code] [int] NOT NULL,
	[alcc_category] [int] NOT NULL,
	[alcc_type] [int] NOT NULL,
	[alcc_datetime] [datetime] NOT NULL CONSTRAINT [DF_alarm_catalog_per_category_alcc_datetime] DEFAULT (getdate()),
 CONSTRAINT [PK_alarm_catalog_per_category] PRIMARY KEY CLUSTERED 
(
	[alcc_alarm_code] ASC,
	[alcc_category] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[alarm_catalog]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[alarm_catalog](
	[alcg_alarm_code] [int] NOT NULL,
	[alcg_type] [int] NOT NULL,
	[alcg_name] [nvarchar](50) NOT NULL,
	[alcg_description] [nvarchar](350) NULL,
	[alcg_visible] [int] NOT NULL,
 CONSTRAINT [PK_alarm_catalog] PRIMARY KEY CLUSTERED 
(
	[alcg_alarm_code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

ALTER TABLE SITE_JACKPOT_PARAMETERS ADD
      SJP_EXCEED_MAXIMUM_ALLOWED   BIT   NOT NULL CONSTRAINT DF_SITE_JACKPOT_PARAMETERS_SJP_EXCEED_MAXIMUM_ALLOWED   DEFAULT 1,
      SJP_CURRENT_COMPENSATION_PCT MONEY NOT NULL CONSTRAINT DF_SITE_JACKPOT_PARAMETERS_SJP_CURRENT_COMPENSATION_PCT DEFAULT 0
GO

/*Machine Without Plays*/
IF NOT EXISTS(SELECT * FROM SYS.COLUMNS WHERE OBJECT_ID = OBJECT_ID(N'[DBO].[TERMINAL_STATUS]') AND NAME = N'TS_WITHOUT_PLAYS_ALARM_ID')
                    ALTER TABLE dbo.terminal_status ADD ts_without_plays_alarm_id BIGINT NULL

IF NOT EXISTS(SELECT * FROM SYS.COLUMNS WHERE OBJECT_ID = OBJECT_ID(N'[DBO].[TERMINAL_STATUS]') AND NAME = N'TS_CURRENT_PAYOUT_ALARM_ID')
			  ALTER TABLE dbo.terminal_status ADD ts_current_payout_alarm_id BIGINT NULL


/******* INDEXES *******/

IF  NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[cage_movements]') AND name = N'IX_cgm_type_datetime')
CREATE NONCLUSTERED INDEX [IX_cgm_type_datetime] ON [dbo].[cage_movements] 
(
      [cgm_type] ASC,
      [cgm_movement_datetime] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

/******* STORED *******/

--------------------------------------------------------------------------------
-- Copyright � 2014 Win Systems International
--------------------------------------------------------------------------------
--
--   MODULE NAME: Update_AlarmPatterns.sql
--
--   DESCRIPTION: Update Patterns in the Site
--
--        AUTHOR: Jos� Mart�nez
--
-- CREATION DATE: 16-MAY-2014
--
-- REVISION HISTORY:
--
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 16-MAY-2014 JML    First release.  
-------------------------------------------------------------------------------- 

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Update_AlarmPatterns]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Update_AlarmPatterns]
GO 

CREATE PROCEDURE   [dbo].[Update_AlarmPatterns]
                    @pId                 BIGINT
                  , @pName               NVARCHAR(50)
                  , @pDescription        NVARCHAR(350)
                  , @pActive             BIT
                  , @pPattern            XML
                  , @pAlCode             INT
                  , @pAlName             NVARCHAR(50)
                  , @pAlDescription      NVARCHAR(350)
                  , @pAlSeverity         INT
                  , @pType               INT
                  , @pSource             INT
                  , @pLifeTime           INT
                  , @pLastFind           DATETIME
                  , @pDetections         INT
                  , @pScheduleTimeFrom   INT
                  , @pScheduleTimeTo     INT
                  , @pTimestamp          BIGINT

AS
BEGIN

  IF NOT EXISTS (SELECT 1 FROM PATTERNS WHERE PT_ID = @pId)
  BEGIN
    SET IDENTITY_INSERT PATTERNS ON

    INSERT INTO   PATTERNS ( PT_ID
                           , PT_NAME
                           , PT_DESCRIPTION
                           , PT_ACTIVE
                           , PT_PATTERN
                           , PT_AL_CODE
                           , PT_AL_NAME
                           , PT_AL_DESCRIPTION
                           , PT_AL_SEVERITY
                           , PT_TYPE
                           , PT_SOURCE
                           , PT_LIFE_TIME
                           , PT_LAST_FIND
                           , PT_DETECTIONS
                           , PT_SCHEDULE_TIME_FROM
                           , PT_SCHEDULE_TIME_TO
                           , PT_TIMESTAMP )
                    VALUES ( @pId 
                           , @pName
                           , @pDescription
                           , @pActive
                           , @pPattern
                           , @pAlCode
                           , @pAlName
                           , @pAlDescription
                           , @pAlSeverity
                           , @pType
                           , @pSource
                           , @pLifeTime
                           , @pLastFind
                           , @pDetections
                           , @pScheduleTimeFrom
                           , @pScheduleTimeTo
                           , @pTimestamp ) 

    SET IDENTITY_INSERT PATTERNS OFF
  END

  ELSE
  BEGIN
    UPDATE   PATTERNS
       SET   PT_NAME               = @pName
           , PT_DESCRIPTION        = @pDescription
           , PT_ACTIVE             = @pActive
           , PT_PATTERN            = @pPattern
           , PT_AL_CODE            = @pAlCode
           , PT_AL_NAME            = @pAlName
           , PT_AL_DESCRIPTION     = @pAlDescription
           , PT_AL_SEVERITY        = @pAlSeverity
           , PT_TYPE               = @pType
           , PT_SOURCE             = @pSource
           , PT_LIFE_TIME          = @pLifeTime
           , PT_LAST_FIND          = @pLastFind
           , PT_DETECTIONS         = @pDetections
           , PT_SCHEDULE_TIME_FROM = @pScheduleTimeFrom
           , PT_SCHEDULE_TIME_TO   = @pScheduleTimeTo
           , PT_TIMESTAMP          = @pTimestamp
     WHERE   PT_ID  = @pId
  END
  

  IF NOT EXISTS (SELECT 1 FROM ALARM_CATALOG WHERE ALCG_ALARM_CODE = @pAlCode)
  BEGIN
  
    INSERT INTO ALARM_CATALOG (ALCG_ALARM_CODE, ALCG_TYPE, ALCG_NAME, ALCG_DESCRIPTION, ALCG_VISIBLE)
                       VALUES (@pAlCode,        1,         @pAlName,  @pAlDescription,  1           )

  END

  IF NOT EXISTS (SELECT 1 FROM ALARM_CATALOG_PER_CATEGORY WHERE ALCC_ALARM_CODE = @pAlCode)
  BEGIN

    INSERT INTO ALARM_CATALOG_PER_CATEGORY (ALCC_ALARM_CODE, ALCC_CATEGORY, ALCC_TYPE, ALCC_DATETIME)
                                    VALUES (@pAlCode,        44,            1,         GetDate()    )

  END
  
END
GO

/******* RECORDS *******/

IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY = 'AnyRedeemableExpireAsPrize')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Cashier','AnyRedeemableExpireAsPrize', '0')
GO

IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY = 'CreditsExpireDailyWhenBalanceLowerThanCents')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Cashier','CreditsExpireDailyWhenBalanceLowerThanCents', '0')
GO

/*Machine Without Plays*/
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Alarms' AND GP_SUBJECT_KEY = 'MachineWithoutPlays.Enabled')
                     INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('Alarms','MachineWithoutPlays.Enabled','0')

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Alarms' AND GP_SUBJECT_KEY = 'MachineWithoutPlays.PeriodUnderStudy.Value')
                     INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('Alarms','MachineWithoutPlays.PeriodUnderStudy.Value','0')

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Alarms' AND GP_SUBJECT_KEY = 'MachineWithoutPlays.PeriodUnderStudy.Unit')
                     INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('Alarms','MachineWithoutPlays.PeriodUnderStudy.Unit','0')       


/*********** Patterns ***********/

IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Pattern' AND GP_SUBJECT_KEY = 'Pattern.LifeTime')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Pattern','Pattern.LifeTime', '5')
GO

IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Pattern' AND GP_SUBJECT_KEY = 'Thread.NumberAlarmsToProcess')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Pattern','Thread.NumberAlarmsToProcess', '500')
GO
IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Pattern' AND GP_SUBJECT_KEY = 'Thread.WaitSeconds')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Pattern', 'Thread.WaitSeconds', '20')
GO
IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Pattern' AND GP_SUBJECT_KEY = 'Thread.Enabled')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Pattern', 'Thread.Enabled', '1')
GO

INSERT [dbo].[alarm_groups] ([alg_alarm_group_id], [alg_type], [alg_name], [alg_description], [alg_visible]) VALUES (1, 1, N'Maquina', N'', 1)
INSERT [dbo].[alarm_groups] ([alg_alarm_group_id], [alg_type], [alg_name], [alg_description], [alg_visible]) VALUES (2, 1, N'Servicio', N'', 1)
INSERT [dbo].[alarm_groups] ([alg_alarm_group_id], [alg_type], [alg_name], [alg_description], [alg_visible]) VALUES (3, 1, N'Sistema', N'', 1)
INSERT [dbo].[alarm_groups] ([alg_alarm_group_id], [alg_type], [alg_name], [alg_description], [alg_visible]) VALUES (4, 1, N'Patrones', N' ', 1)
GO

INSERT [dbo].[alarm_categories] ([alc_category_id], [alc_alarm_group_id], [alc_type], [alc_name], [alc_description], [alc_visible]) VALUES (1, 1, 0, N'Contadores', N'', 1)
INSERT [dbo].[alarm_categories] ([alc_category_id], [alc_alarm_group_id], [alc_type], [alc_name], [alc_description], [alc_visible]) VALUES (2, 1, 0, N'CPU', N'', 1)
INSERT [dbo].[alarm_categories] ([alc_category_id], [alc_alarm_group_id], [alc_type], [alc_name], [alc_description], [alc_visible]) VALUES (3, 1, 0, N'Impresora', N'', 1)
INSERT [dbo].[alarm_categories] ([alc_category_id], [alc_alarm_group_id], [alc_type], [alc_name], [alc_description], [alc_visible]) VALUES (4, 1, 0, N'Kiosco', N'', 1)
INSERT [dbo].[alarm_categories] ([alc_category_id], [alc_alarm_group_id], [alc_type], [alc_name], [alc_description], [alc_visible]) VALUES (5, 1, 0, N'Lector c�digo de barras', N'', 1)
INSERT [dbo].[alarm_categories] ([alc_category_id], [alc_alarm_group_id], [alc_type], [alc_name], [alc_description], [alc_visible]) VALUES (6, 1, 0, N'Lector de tarjetas', N'', 1)
INSERT [dbo].[alarm_categories] ([alc_category_id], [alc_alarm_group_id], [alc_type], [alc_name], [alc_description], [alc_visible]) VALUES (7, 1, 0, N'M�dulo E/S', N'', 1)
INSERT [dbo].[alarm_categories] ([alc_category_id], [alc_alarm_group_id], [alc_type], [alc_name], [alc_description], [alc_visible]) VALUES (8, 1, 0, N'Teclado', N'', 1)
INSERT [dbo].[alarm_categories] ([alc_category_id], [alc_alarm_group_id], [alc_type], [alc_name], [alc_description], [alc_visible]) VALUES (9, 1, 0, N'Validador de billetes', N'', 1)
INSERT [dbo].[alarm_categories] ([alc_category_id], [alc_alarm_group_id], [alc_type], [alc_name], [alc_description], [alc_visible]) VALUES (10, 1, 0, N'Pantalla', N'', 1)
INSERT [dbo].[alarm_categories] ([alc_category_id], [alc_alarm_group_id], [alc_type], [alc_name], [alc_description], [alc_visible]) VALUES (11, 1, 0, N'Puerta', N'', 1)
INSERT [dbo].[alarm_categories] ([alc_category_id], [alc_alarm_group_id], [alc_type], [alc_name], [alc_description], [alc_visible]) VALUES (12, 1, 0, N'UPS', N'', 1)
INSERT [dbo].[alarm_categories] ([alc_category_id], [alc_alarm_group_id], [alc_type], [alc_name], [alc_description], [alc_visible]) VALUES (13, 1, 0, N'Comunicaciones', N'', 1)
INSERT [dbo].[alarm_categories] ([alc_category_id], [alc_alarm_group_id], [alc_type], [alc_name], [alc_description], [alc_visible]) VALUES (14, 1, 0, N'HandPays', N'', 1)
INSERT [dbo].[alarm_categories] ([alc_category_id], [alc_alarm_group_id], [alc_type], [alc_name], [alc_description], [alc_visible]) VALUES (15, 1, 0, N'Trasferencia de entrada', N'', 1)
INSERT [dbo].[alarm_categories] ([alc_category_id], [alc_alarm_group_id], [alc_type], [alc_name], [alc_description], [alc_visible]) VALUES (16, 1, 0, N'Soporte', N'', 1)
INSERT [dbo].[alarm_categories] ([alc_category_id], [alc_alarm_group_id], [alc_type], [alc_name], [alc_description], [alc_visible]) VALUES (17, 1, 0, N'Fallo/Error', N'', 1)
INSERT [dbo].[alarm_categories] ([alc_category_id], [alc_alarm_group_id], [alc_type], [alc_name], [alc_description], [alc_visible]) VALUES (18, 1, 0, N'Estado', N'', 1)
INSERT [dbo].[alarm_categories] ([alc_category_id], [alc_alarm_group_id], [alc_type], [alc_name], [alc_description], [alc_visible]) VALUES (19, 1, 0, N'Intrusi�n', N'', 1)
INSERT [dbo].[alarm_categories] ([alc_category_id], [alc_alarm_group_id], [alc_type], [alc_name], [alc_description], [alc_visible]) VALUES (20, 1, 0, N'Lanzar explorador', N'', 1)
INSERT [dbo].[alarm_categories] ([alc_category_id], [alc_alarm_group_id], [alc_type], [alc_name], [alc_description], [alc_visible]) VALUES (21, 1, 0, N'Men�', N'', 1)
INSERT [dbo].[alarm_categories] ([alc_category_id], [alc_alarm_group_id], [alc_type], [alc_name], [alc_description], [alc_visible]) VALUES (22, 1, 0, N'ModoTest', N'', 1)
INSERT [dbo].[alarm_categories] ([alc_category_id], [alc_alarm_group_id], [alc_type], [alc_name], [alc_description], [alc_visible]) VALUES (23, 1, 0, N'Usuario', N'', 1)
INSERT [dbo].[alarm_categories] ([alc_category_id], [alc_alarm_group_id], [alc_type], [alc_name], [alc_description], [alc_visible]) VALUES (24, 1, 0, N'Sesiones de Juego', N'', 1)
INSERT [dbo].[alarm_categories] ([alc_category_id], [alc_alarm_group_id], [alc_type], [alc_name], [alc_description], [alc_visible]) VALUES (25, 2, 0, N'Licencia', N'', 1)
INSERT [dbo].[alarm_categories] ([alc_category_id], [alc_alarm_group_id], [alc_type], [alc_name], [alc_description], [alc_visible]) VALUES (26, 2, 0, N'Versi�n', N'', 1)
INSERT [dbo].[alarm_categories] ([alc_category_id], [alc_alarm_group_id], [alc_type], [alc_name], [alc_description], [alc_visible]) VALUES (27, 2, 0, N'En ejecuci�n', N'', 1)
INSERT [dbo].[alarm_categories] ([alc_category_id], [alc_alarm_group_id], [alc_type], [alc_name], [alc_description], [alc_visible]) VALUES (28, 2, 0, N'En espera', N'', 1)
INSERT [dbo].[alarm_categories] ([alc_category_id], [alc_alarm_group_id], [alc_type], [alc_name], [alc_description], [alc_visible]) VALUES (29, 2, 0, N'Iniciado', N'', 1)
INSERT [dbo].[alarm_categories] ([alc_category_id], [alc_alarm_group_id], [alc_type], [alc_name], [alc_description], [alc_visible]) VALUES (30, 2, 0, N'Parado', N'', 1)
INSERT [dbo].[alarm_categories] ([alc_category_id], [alc_alarm_group_id], [alc_type], [alc_name], [alc_description], [alc_visible]) VALUES (31, 2, 0, N'Reiniciado', N'', 1)
INSERT [dbo].[alarm_categories] ([alc_category_id], [alc_alarm_group_id], [alc_type], [alc_name], [alc_description], [alc_visible]) VALUES (32, 2, 0, N'WWP', N'', 1)
INSERT [dbo].[alarm_categories] ([alc_category_id], [alc_alarm_group_id], [alc_type], [alc_name], [alc_description], [alc_visible]) VALUES (33, 2, 0, N'WC2', N'', 1)
INSERT [dbo].[alarm_categories] ([alc_category_id], [alc_alarm_group_id], [alc_type], [alc_name], [alc_description], [alc_visible]) VALUES (34, 2, 0, N'WCP', N'', 1)
INSERT [dbo].[alarm_categories] ([alc_category_id], [alc_alarm_group_id], [alc_type], [alc_name], [alc_description], [alc_visible]) VALUES (35, 2, 0, N'PSA', N'', 1)
INSERT [dbo].[alarm_categories] ([alc_category_id], [alc_alarm_group_id], [alc_type], [alc_name], [alc_description], [alc_visible]) VALUES (36, 3, 0, N'Actividad fuera horario', N'', 1)
INSERT [dbo].[alarm_categories] ([alc_category_id], [alc_alarm_group_id], [alc_type], [alc_name], [alc_description], [alc_visible]) VALUES (37, 3, 0, N'User', N'', 1)
INSERT [dbo].[alarm_categories] ([alc_category_id], [alc_alarm_group_id], [alc_type], [alc_name], [alc_description], [alc_visible]) VALUES (38, 2, 0, N'Multisite', N'', 1)
INSERT [dbo].[alarm_categories] ([alc_category_id], [alc_alarm_group_id], [alc_type], [alc_name], [alc_description], [alc_visible]) VALUES (39, 2, 0, N'ELP', N'', 1)
INSERT [dbo].[alarm_categories] ([alc_category_id], [alc_alarm_group_id], [alc_type], [alc_name], [alc_description], [alc_visible]) VALUES (44, 4, 0, N'Patrones', N' ', 1)
GO

INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (65553, 11, 0, CAST(0x0000A33A00E5BBC2 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (65554, 11, 0, CAST(0x0000A33A00E5BBCE AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (65555, 11, 0, CAST(0x0000A33A00E5BBE0 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (65556, 11, 0, CAST(0x0000A33A00E5BBEA AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (65557, 11, 0, CAST(0x0000A33A00E5BBF6 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (65558, 11, 0, CAST(0x0000A33A00E5BC00 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (65559, 18, 0, CAST(0x0000A33A00E5BC11 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (65560, 18, 0, CAST(0x0000A33A00E5BC1B AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (65561, 9, 0, CAST(0x0000A33A00E5BC24 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (65562, 9, 0, CAST(0x0000A33A00E5BC2C AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (65563, 9, 0, CAST(0x0000A33A00E5BC35 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (65564, 9, 0, CAST(0x0000A33A00E5BC3D AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (65565, 11, 0, CAST(0x0000A33A00E5BC46 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (65566, 11, 0, CAST(0x0000A33A00E5BC4F AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (65568, 17, 0, CAST(0x0000A33A00E5BC58 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (65575, 9, 0, CAST(0x0000A33A00E5BC60 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (65576, 9, 0, CAST(0x0000A33A00E5BC6C AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (65577, 9, 0, CAST(0x0000A33A00E5BC75 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (65578, 9, 0, CAST(0x0000A33A00E5BC7D AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (65579, 9, 0, CAST(0x0000A33A00E5BC86 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (65580, 9, 0, CAST(0x0000A33A00E5BC8E AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (65582, 9, 0, CAST(0x0000A33A00E5BC97 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (65585, 2, 0, CAST(0x0000A33A00E5BCA2 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (65586, 2, 0, CAST(0x0000A33A00E5BCAA AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (65587, 2, 0, CAST(0x0000A33A00E5BCB5 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (65588, 2, 0, CAST(0x0000A33A00E5BCC1 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (65589, 2, 0, CAST(0x0000A33A00E5BCCA AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (65590, 2, 0, CAST(0x0000A33A00E5BCD3 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (65591, 2, 0, CAST(0x0000A33A00E5BCDD AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (65592, 2, 0, CAST(0x0000A33A00E5BCE6 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (65593, 2, 0, CAST(0x0000A33A00E5BCEE AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (65594, 2, 0, CAST(0x0000A33A00E5BCF6 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (65595, 2, 0, CAST(0x0000A33A00E5BCFF AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (65596, 1, 0, CAST(0x0000A33A00E5BD08 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (65617, 14, 0, CAST(0x0000A33A00E5BD12 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (65618, 14, 0, CAST(0x0000A33A00E5BD1C AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (65620, 14, 0, CAST(0x0000A33A00E5BD25 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (65621, 14, 0, CAST(0x0000A33A00E5BD2E AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (65622, 1, 0, CAST(0x0000A33A00E5BD36 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (65632, 3, 0, CAST(0x0000A33A00E5BD3E AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (65633, 1, 0, CAST(0x0000A33A00E5BD47 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (65648, 17, 0, CAST(0x0000A33A00E5BD4F AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (65649, 16, 0, CAST(0x0000A33A00E5BD58 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (65650, 16, 0, CAST(0x0000A33A00E5BD61 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (65652, 3, 0, CAST(0x0000A33A00E5BD6B AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (65653, 3, 0, CAST(0x0000A33A00E5BD75 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (65654, 3, 0, CAST(0x0000A33A00E5BD7E AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (65655, 3, 0, CAST(0x0000A33A00E5BD86 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (65656, 3, 0, CAST(0x0000A33A00E5BD90 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (65658, 1, 0, CAST(0x0000A33A00E5BD98 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (65666, 1, 0, CAST(0x0000A33A00E5BDA2 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (65667, 1, 0, CAST(0x0000A33A00E5BDAA AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (65668, 22, 0, CAST(0x0000A33A00E5BDB3 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (65669, 22, 0, CAST(0x0000A33A00E5BDBB AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (65670, 18, 0, CAST(0x0000A33A00E5BDC4 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (65674, 22, 0, CAST(0x0000A33A00E5BDCD AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (65688, 22, 0, CAST(0x0000A33A00E5BDD6 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (65689, 22, 0, CAST(0x0000A33A00E5BDDF AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (65690, 22, 0, CAST(0x0000A33A00E5BDE7 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (65691, 22, 0, CAST(0x0000A33A00E5BDF0 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (65793, 13, 0, CAST(0x0000A33A00E5BDF8 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (65794, 13, 0, CAST(0x0000A33A00E5BE02 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (65808, 1, 0, CAST(0x0000A33A00E5BE0A AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (65809, 1, 0, CAST(0x0000A33A00E5BE13 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (65810, 1, 0, CAST(0x0000A33A00E5BE1B AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (65811, 1, 0, CAST(0x0000A33A00E5BE25 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (65812, 1, 0, CAST(0x0000A33A00E5BE2D AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (65813, 1, 0, CAST(0x0000A33A00E5BE35 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (65814, 1, 0, CAST(0x0000A33A00E5BE3E AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (65825, 1, 0, CAST(0x0000A33A00E5BE46 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (65826, 1, 0, CAST(0x0000A33A00E5BE4F AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (65827, 1, 0, CAST(0x0000A33A00E5BE58 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (65828, 1, 0, CAST(0x0000A33A00E5BE61 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (65829, 1, 0, CAST(0x0000A33A00E5BE69 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (65830, 1, 0, CAST(0x0000A33A00E5BE71 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (131073, 1, 0, CAST(0x0000A33A00E5BE7A AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (131074, 1, 0, CAST(0x0000A33A00E5BE82 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (131075, 1, 0, CAST(0x0000A33A00E5BE8C AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (131076, 1, 0, CAST(0x0000A33A00E5BE96 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (131077, 1, 0, CAST(0x0000A33A00E5BEA0 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (131078, 1, 0, CAST(0x0000A33A00E5BEA9 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (131079, 24, 0, CAST(0x0000A33A00E5BEB2 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (131080, 1, 0, CAST(0x0000A33A00E5BEBC AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (131081, 1, 0, CAST(0x0000A33A00E5BEC4 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (131089, 34, 0, CAST(0x0000A33A00E5BECD AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (131090, 34, 0, CAST(0x0000A33A00E5BED5 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (131105, 33, 0, CAST(0x0000A33A00E5BEDE AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (131106, 33, 0, CAST(0x0000A33A00E5BEE7 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (131121, 32, 0, CAST(0x0000A33A00E5BEF1 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (131122, 32, 0, CAST(0x0000A33A00E5BEFA AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (131137, 32, 0, CAST(0x0000A33A00E5BF03 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (131138, 32, 0, CAST(0x0000A33A00E5BF0B AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (135168, 1, 0, CAST(0x0000A33A00E5BF13 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (135169, 1, 0, CAST(0x0000A33A00E5BF1C AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (139264, 1, 0, CAST(0x0000A33A00E5BF29 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (143360, 1, 0, CAST(0x0000A33A00E5BF32 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (147456, 1, 0, CAST(0x0000A33A00E5BF3B AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (151552, 14, 0, CAST(0x0000A33A00E5BF43 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (196609, 29, 0, CAST(0x0000A33A00E5BF4B AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (196610, 30, 0, CAST(0x0000A33A00E5BF54 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (196611, 31, 0, CAST(0x0000A33A00E5BF5C AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (196612, 27, 0, CAST(0x0000A33A00E5BF65 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (196613, 28, 0, CAST(0x0000A33A00E5BF6D AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (196614, 36, 0, CAST(0x0000A33A00E5BF75 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (196615, 17, 0, CAST(0x0000A33A00E5BF7E AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (196625, 25, 0, CAST(0x0000A33A00E5BF87 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (196626, 25, 0, CAST(0x0000A33A00E5BF90 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (196641, 26, 0, CAST(0x0000A33A00E5BF98 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (196642, 26, 0, CAST(0x0000A33A00E5BFA1 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (200704, 1, 0, CAST(0x0000A33A00E5BFA9 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (200705, 1, 0, CAST(0x0000A33A00E5BFB1 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (204800, 1, 0, CAST(0x0000A33A00E5BFBA AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (208896, 1, 0, CAST(0x0000A33A00E5BFC5 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (262144, 17, 0, CAST(0x0000A33A00E5BFCD AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (262145, 23, 0, CAST(0x0000A33A00E5BFD6 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (262146, 23, 0, CAST(0x0000A33A00E5BFDF AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (262147, 23, 0, CAST(0x0000A33A00E5BFE7 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (262149, 23, 0, CAST(0x0000A33A00E5BFF1 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (262150, 23, 0, CAST(0x0000A33A00E5BFF9 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (262151, 23, 0, CAST(0x0000A33A00E5C003 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (327681, 12, 0, CAST(0x0000A33A00E5C00B AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (327682, 12, 0, CAST(0x0000A33A00E5C014 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (327683, 12, 0, CAST(0x0000A33A00E5C01D AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (393216, 34, 0, CAST(0x0000A33A00E5C026 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (393217, 18, 0, CAST(0x0000A33A00E5C02E AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (393218, 18, 0, CAST(0x0000A33A00E5C037 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (393219, 18, 0, CAST(0x0000A33A00E5C040 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (393220, 18, 0, CAST(0x0000A33A00E5C049 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (393221, 20, 0, CAST(0x0000A33A00E5C052 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (393222, 4, 0, CAST(0x0000A33A00E5C05B AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (393223, 4, 0, CAST(0x0000A33A00E5C063 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (393224, 4, 0, CAST(0x0000A33A00E5C06B AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (393225, 23, 0, CAST(0x0000A33A00E5C075 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (393226, 23, 0, CAST(0x0000A33A00E5C07D AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (393227, 10, 0, CAST(0x0000A33A00E5C086 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (393228, 11, 0, CAST(0x0000A33A00E5C08F AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (393229, 11, 0, CAST(0x0000A33A00E5C098 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (393230, 4, 0, CAST(0x0000A33A00E5C0A1 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (393231, 4, 0, CAST(0x0000A33A00E5C0AB AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (393232, 14, 0, CAST(0x0000A33A00E5C0B4 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (393233, 4, 0, CAST(0x0000A33A00E5C0BE AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (393234, 16, 0, CAST(0x0000A33A00E5C0C6 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (393235, 22, 0, CAST(0x0000A33A00E5C0CF AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (393236, 22, 0, CAST(0x0000A33A00E5C0D9 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (393237, 23, 0, CAST(0x0000A33A00E5C0E2 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (393238, 21, 0, CAST(0x0000A33A00E5C0ED AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (393239, 21, 0, CAST(0x0000A33A00E5C103 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (393240, 21, 0, CAST(0x0000A33A00E5C10E AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (393241, 21, 0, CAST(0x0000A33A00E5C118 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (393242, 21, 0, CAST(0x0000A33A00E5C121 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (393243, 14, 0, CAST(0x0000A33A00E5C12B AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (393244, 14, 0, CAST(0x0000A33A00E5C134 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (393245, 24, 0, CAST(0x0000A33A00E5C13D AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (393246, 24, 0, CAST(0x0000A33A00E5C146 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (393247, 23, 0, CAST(0x0000A33A00E5C14F AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (393248, 34, 0, CAST(0x0000A33A00E5C161 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (393249, 9, 0, CAST(0x0000A33A00E5C16B AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (393250, 34, 0, CAST(0x0000A33A00E5C174 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (393251, 17, 0, CAST(0x0000A33A00E5C17C AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (393252, 15, 0, CAST(0x0000A33A00E5C186 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (393253, 34, 0, CAST(0x0000A33A00E5C190 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (458752, 19, 0, CAST(0x0000A33A00E5C198 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (458753, 19, 0, CAST(0x0000A33A00E5C1A0 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (458754, 19, 0, CAST(0x0000A33A00E5C1A9 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (458852, 19, 0, CAST(0x0000A33A00E5C1B2 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (459008, 3, 0, CAST(0x0000A33A00E5C1BB AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (459009, 3, 0, CAST(0x0000A33A00E5C1C4 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (459010, 3, 0, CAST(0x0000A33A00E5C1CC AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (459011, 3, 0, CAST(0x0000A33A00E5C1D4 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (459012, 3, 0, CAST(0x0000A33A00E5C1DC AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (459013, 3, 0, CAST(0x0000A33A00E5C1E5 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (459014, 3, 0, CAST(0x0000A33A00E5C1ED AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (459015, 3, 0, CAST(0x0000A33A00E5C1F6 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (459016, 3, 0, CAST(0x0000A33A00E5C1FE AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (459108, 3, 0, CAST(0x0000A33A00E5C206 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (459264, 9, 0, CAST(0x0000A33A00E5C20F AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (459265, 9, 0, CAST(0x0000A33A00E5C218 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (459266, 9, 0, CAST(0x0000A33A00E5C222 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (459267, 9, 0, CAST(0x0000A33A00E5C22B AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (459268, 9, 0, CAST(0x0000A33A00E5C234 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (459269, 9, 0, CAST(0x0000A33A00E5C23D AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (459270, 9, 0, CAST(0x0000A33A00E5C246 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (459364, 9, 0, CAST(0x0000A33A00E5C24E AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (459520, 6, 0, CAST(0x0000A33A00E5C257 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (459521, 6, 0, CAST(0x0000A33A00E5C25F AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (459522, 6, 0, CAST(0x0000A33A00E5C268 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (459620, 6, 0, CAST(0x0000A33A00E5C270 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (460032, 9, 0, CAST(0x0000A33A00E5C278 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (460033, 9, 0, CAST(0x0000A33A00E5C281 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (460034, 9, 0, CAST(0x0000A33A00E5C28A AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (460035, 9, 0, CAST(0x0000A33A00E5C293 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (460036, 9, 0, CAST(0x0000A33A00E5C29D AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (460037, 9, 0, CAST(0x0000A33A00E5C2A6 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (460132, 9, 0, CAST(0x0000A33A00E5C2B0 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (460288, 10, 0, CAST(0x0000A33A00E5C2B8 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (460289, 10, 0, CAST(0x0000A33A00E5C2C2 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (460290, 10, 0, CAST(0x0000A33A00E5C2CB AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (460388, 10, 0, CAST(0x0000A33A00E5C2D3 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (460544, 5, 0, CAST(0x0000A33A00E5C2DC AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (460545, 5, 0, CAST(0x0000A33A00E5C2E6 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (460546, 5, 0, CAST(0x0000A33A00E5C2EE AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (460644, 5, 0, CAST(0x0000A33A00E5C2F6 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (460800, 19, 0, CAST(0x0000A33A00E5C2FF AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (460801, 19, 0, CAST(0x0000A33A00E5C308 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (460802, 19, 0, CAST(0x0000A33A00E5C311 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (461056, 12, 0, CAST(0x0000A33A00E5C31A AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (461057, 12, 0, CAST(0x0000A33A00E5C324 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (461058, 12, 0, CAST(0x0000A33A00E5C32C AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (461059, 12, 0, CAST(0x0000A33A00E5C335 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (461060, 12, 0, CAST(0x0000A33A00E5C33D AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (461061, 12, 0, CAST(0x0000A33A00E5C346 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (461062, 12, 0, CAST(0x0000A33A00E5C34F AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (461063, 12, 0, CAST(0x0000A33A00E5C357 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (461156, 12, 0, CAST(0x0000A33A00E5C361 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (462592, 7, 0, CAST(0x0000A33A00E5C36B AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (462593, 7, 0, CAST(0x0000A33A00E5C376 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (462594, 7, 0, CAST(0x0000A33A00E5C37E AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (462692, 7, 0, CAST(0x0000A33A00E5C387 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (463104, 10, 0, CAST(0x0000A33A00E5C393 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (463105, 10, 0, CAST(0x0000A33A00E5C39D AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (463106, 10, 0, CAST(0x0000A33A00E5C3A8 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (463204, 10, 0, CAST(0x0000A33A00E5C3B1 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (463360, 10, 0, CAST(0x0000A33A00E5C3BA AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (463361, 10, 0, CAST(0x0000A33A00E5C3C2 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (463362, 10, 0, CAST(0x0000A33A00E5C3CB AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (463460, 10, 0, CAST(0x0000A33A00E5C3D5 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (463616, 10, 0, CAST(0x0000A33A00E5C3DE AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (463617, 10, 0, CAST(0x0000A33A00E5C3E6 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (463618, 10, 0, CAST(0x0000A33A00E5C3EF AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (463716, 10, 0, CAST(0x0000A33A00E5C3F8 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (463872, 8, 0, CAST(0x0000A33A00E5C402 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (463873, 8, 0, CAST(0x0000A33A00E5C40E AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (463874, 8, 0, CAST(0x0000A33A00E5C416 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (463972, 8, 0, CAST(0x0000A33A00E5C420 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (464128, 10, 0, CAST(0x0000A33A00E5C429 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (464129, 10, 0, CAST(0x0000A33A00E5C433 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (464130, 10, 0, CAST(0x0000A33A00E5C43C AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (464228, 10, 0, CAST(0x0000A33A00E5C445 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (464384, 8, 0, CAST(0x0000A33A00E5C44F AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (464385, 8, 0, CAST(0x0000A33A00E5C458 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (464386, 8, 0, CAST(0x0000A33A00E5C462 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (464484, 8, 0, CAST(0x0000A33A00E5C46B AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (464640, 6, 0, CAST(0x0000A33A00E5C476 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (464641, 6, 0, CAST(0x0000A33A00E5C481 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (464642, 6, 0, CAST(0x0000A33A00E5C489 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (464740, 6, 0, CAST(0x0000A33A00E5C491 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (464896, 2, 0, CAST(0x0000A33A00E5C49E AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (464897, 2, 0, CAST(0x0000A33A00E5C4A9 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (464898, 2, 0, CAST(0x0000A33A00E5C4B4 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (464996, 2, 0, CAST(0x0000A33A00E5C4BE AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (524289, 35, 0, CAST(0x0000A33A00E5C4C9 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (524290, 35, 0, CAST(0x0000A33A00E5C4D1 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (524291, 35, 0, CAST(0x0000A33A00E5C4D9 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (589825, 38, 0, CAST(0x0000A33A00E5C4E2 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (589826, 38, 0, CAST(0x0000A33A00E5C4EA AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (589828, 38, 0, CAST(0x0000A33A00E5C4F3 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (1048577, 39, 0, CAST(0x0000A33A00E5C5D6 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (1048578, 39, 0, CAST(0x0000A33A00E5C5E2 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (1048579, 38, 0, CAST(0x0000A33A00E5C4FA AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (1048580, 38, 0, CAST(0x0000A33A00E5C503 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (1048581, 38, 0, CAST(0x0000A33A00E5C50C AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (1048582, 38, 0, CAST(0x0000A33A00E5C514 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (1114113, 17, 0, CAST(0x0000A33A00E5C51C AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (1114114, 34, 0, CAST(0x0000A33A00E5C528 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (1114115, 34, 0, CAST(0x0000A33A00E5C53E AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (1114116, 34, 0, CAST(0x0000A33A00E5C548 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (1114117, 34, 0, CAST(0x0000A33A00E5C554 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (1114121, 24, 0, CAST(0x0000A33A00E5C561 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (1114128, 34, 0, CAST(0x0000A33A00E5C56B AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (1114129, 34, 0, CAST(0x0000A33A00E5C576 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (1114130, 34, 0, CAST(0x0000A33A00E5C582 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (2097153, 24, 0, CAST(0x0000A33A00E5C58D AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (2097154, 24, 0, CAST(0x0000A33A00E5C599 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (2097155, 14, 0, CAST(0x0000A33A00E5C5A5 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (2097156, 14, 0, CAST(0x0000A33A00E5C5B1 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (2097157, 24, 0, CAST(0x0000A33A00E5C5C0 AS DateTime))
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (2097158, 9, 0, CAST(0x0000A33A00E5C5CB AS DateTime))
GO

INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (65553, 0, N'Slot door abierta', N'Slot door abierta', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (65554, 0, N'Slot door cerrada', N'Slot door cerrada', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (65555, 0, N'Drop door abierta', N'Drop door abierta', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (65556, 0, N'Drop door cerrada', N'Drop door cerrada', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (65557, 0, N'Card cage abierta', N'Card cage abierta', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (65558, 0, N'Card cage cerrada', N'Card cage cerrada', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (65559, 0, N'Terminal de juego conectado a la corriente', N'Terminal de juego conectado a la corriente', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (65560, 0, N'Terminal de juego desconectado de la corriente', N'Terminal de juego desconectado de la corriente', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (65561, 0, N'Cashbox door abierta', N'Cashbox door abierta', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (65562, 0, N'Cashbox door cerrada', N'Cashbox door cerrada', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (65563, 0, N'Cashbox extra�do', N'Cashbox extra�do', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (65564, 0, N'Cashbox instalado', N'Cashbox instalado', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (65565, 0, N'Belly door abierta', N'Belly door abierta', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (65566, 0, N'Belly door cerrada', N'Belly door cerrada', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (65568, 0, N'Fallo general', N'Fallo general', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (65575, 0, N'Stacker full', N'Stacker lleno', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (65576, 0, N'Bill jam', N'Atasco de billete', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (65577, 0, N'Bill acceptor hardware failure', N'Error de hardware del validador de billetes', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (65578, 0, N'Detectado billete al rev�s', N'Detectado billete al rev�s', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (65579, 0, N'Billete rechazado', N'Billete rechazado', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (65580, 0, N'Detectado billete falso', N'Detectado billete falso', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (65582, 0, N'Stacker near full', N'Stacker casi lleno', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (65585, 0, N'Error en RAM CMOS (datos recuperados EEPROM)', N'Error en RAM CMOS (datos recuperados de la EEPROM)', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (65586, 0, N'Error en RAM CMOS (datos no recuperados EEPROM)', N'Error en RAM CMOS (datos no recuperados de la EEPROM)', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (65587, 0, N'Error en RAM CMOS (error de dispositivo)', N'Error en RAM CMOS (error de dispositivo)', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (65588, 0, N'Error en EEPROM (error de datos)', N'Error en EEPROM (error de datos)', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (65589, 0, N'Error en EEPROM (error de dispositivo)', N'Error en EEPROM (error de dispositivo)', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (65590, 0, N'Error en EEPROM (cambio de versi�n)', N'Error en EEPROM (checksum distinto - cambio de versi�n)', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (65591, 0, N'Error en EEPROM (error de comparaci�n de checksum)', N'Error en EEPROM (error de comparaci�n de checksum)', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (65592, 0, N'Error en EEPROM particionada  (cambio de versi�n)', N'Error en EEPROM particionada (checksum - cambio de versi�n)', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (65593, 0, N'Error en EEPROM particionada (error comparaci�n)', N'Error en EEPROM particionada (error de comparaci�n de checksum)', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (65594, 0, N'Error de memoria eliminado (operador us� switch)', N'Error de memoria eliminado (el operador us� switch de auto-test)', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (65595, 0, N'Bater�a de reserva con poca carga', N'Bater�a de reserva con poca carga', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (65596, 0, N'El operador modific� opciones', N'El operador modific� opciones', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (65617, 0, N'Hay un pago manual pendiente', N'Hay un pago manual pendiente', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (65618, 0, N'Pago manual eliminado', N'Pago manual eliminado', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (65620, 0, N'Progresivo ganado (cashout / cr�dito pagado)', N'Progresivo ganado (cashout / cr�dito pagado)', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (65621, 0, N'El jugador cancel� la petici�n de pago manual', N'El jugador cancel� la petici�n de pago manual', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (65622, 0, N'Nivel de progresivo SAS alcanzado', N'Nivel de progresivo SAS alcanzado', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (65632, 0, N'Error de comunicaci�n de la impresora', N'Error de comunicaci�n de la impresora', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (65633, 0, N'Printer paper out error', N'Error en la salida de papel de la impresora', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (65648, 0, N'Desbordamiento en el buffer de excepciones', N'Desbordamiento en el buffer de excepciones', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (65649, 0, N'Petici�n de asistencia activada', N'Petici�n de asistencia activada', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (65650, 0, N'Petici�n de asistencia desactivada', N'Petici�n de asistencia desactivada', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (65652, 0, N'Printer paper low', N'No hay papel en la impresora', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (65653, 0, N'Printer power off', N'Impresora apagada', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (65654, 0, N'Impresora encendida', N'Impresora encendida', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (65655, 0, N'Cambiar cinta de la impresora', N'Cambiar cinta de la impresora', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (65656, 0, N'Printer carriage jammed', N'Carro de la impresora atascado', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (65658, 0, N'Contadores de terminal de juego puestos a cero', N'Contadores de terminal de juego puestos a cero', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (65666, 0, N'Entrada a pantalla de contadores', N'Entrada a pantalla de contadores o men� de asistente', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (65667, 0, N'Salida de pantalla de contadores', N'Salida de pantalla de contadores o men� de asistente', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (65668, 0, N'Entrada a auto-test o men� de operador', N'Entrada a auto-test o men� de operador', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (65669, 0, N'Salida de auto-test o men� de operador', N'Salida de auto-test o men� de operador', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (65670, 0, N'Terminal de juego deshabilitado (por asistente)', N'Terminal de juego deshabilitado (por asistente)', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (65674, 0, N'Acceso a hist�rico de jugadas', N'Acceso a hist�rico de jugadas', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (65688, 0, N'Acceso a card cage con terminal apagado', N'Acceso a card cage con terminal apagado', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (65689, 0, N'Acceso a  slot door con terminal apagado', N'Acceso a  slot door con terminal apagado', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (65690, 0, N'Acceso a cashbox con terminal apagado', N'Acceso a cashbox con terminal apagado', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (65691, 0, N'Acceso a drop door con terminal apagado', N'Acceso a drop door con terminal apagado', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (65793, 0, N'SAS desconectado', N'SAS desconectado', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (65794, 0, N'SAS conectado', N'SAS conectado', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (65808, 0, N'No se pudo obtener alguno de los contadores', N'No se pudo obtener alguno de los contadores', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (65809, 0, N'Error al obtener los contadores', N'Error al obtener los contadores', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (65810, 0, N'Error al obtener el contador de Pagos Manuales', N'Error al obtener el contador de Pagos Manuales (HP-Meter)', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (65811, 0, N'Error al obtener los contadores de juego', N'Error al obtener los contadores de juego (Coin-in/Coin-out)', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (65812, 0, N'Error al obtener los contadores extendidos', N'Error al obtener los contadores extendidos', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (65813, 0, N'Error al obtener el contador de Pagos Manuales', N'Error al obtener el contador extendido de Pagos Manuales (HP-Meter)', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (65814, 0, N'Error al obtener los contadores extendido de juego', N'Error al obtener los contadores extendidos de juego (Coin-in/Coin-out)', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (65825, 0, N'Error al obtener los contadores', N'Error recurrente al obtener los contadores', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (65826, 0, N'Error al obtener el contador de Pagos Manuales', N'Error recurrente al obtener el contador de Pagos Manuales (HP-Meter)', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (65827, 0, N'Error al obtener los contadores de juego', N'Error recurrente al obtener los contadores de juego (Coin-in/Coin-out)', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (65828, 0, N'Error al obtener los contadores extendidos', N'Error recurrente al obtener los contadores extendidos', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (65829, 0, N'Error al obtener contador extendido Pagos Manuales', N'Error recurrente al obtener el contador extendido de Pagos Manuales (HP-Meter)', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (65830, 0, N'Error al obtener contadores extendidos de juego', N'Error recurrente al obtener los contadores extendidos de juego (Coin-in/Coin-out)', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (99997, 0, N'Simple Alarm PEPE', N'Alarma sencilla para prueba sencilla PEPE', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (99998, 0, N'Simple Alarm 2', N'Alarma sencilla para prueba sencilla 2', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (131073, 0, N'Game Meters Reset', N'*** Game Meters Reset ***   Game:   Old meter values:       Played Count/Cents:            Won Count/Cents:         Jackpot Cents:      ', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (131074, 0, N'Game Meters BigIncrement', N'*** Game Meters BigIncrement ***   Game:    Old meter values:       Played Count/Cents:         Won Count/Cents:        Jackpot Cents:         New meter values:       Played Count/Cents:       Won Count/Cents:       Jackpot Cents:', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (131075, 0, N'BigIncrement', N'BigIncrement. TerminalId:  Old hpc meter value - Handpay Cents:  New hpc meter value - Handpay Cents:', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (131076, 0, N'FundTransfer AFT Machine Meters Reset', N'*** FundTransfer AFT Machine Meters Reset ***   Old meter values:       ToGm Quantity/Cents:     FromGm Quantity/Cents: ', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (131077, 0, N'PlayedWon Machine Meters BigIncrement ', N'*** PlayedWon Machine Meters BigIncrement ***   Old meter values:       ToGm Quantity/Cents:    FromGm Quantity/Cents:    New meter values:       ToGm Quantity/Cents:   FromGm Quantity/Cents: ', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (131078, 0, N'Game Meters Denomination Changed', N'*** Game Meters Denomination Changed ***  Game:    Old meter values:       Played Count/Cents:         Won Count/Cents:        Jackpot Cents:         New meter values:       Played Count/Cents:       Won Count/Cents:       Jackpot Cents:', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (131079, 0, N'Cierre de sesi�n de juego: Ya se cerr� manualmente', N'*** Cierre de sesi�n de juego: Ya se cerr� manualmente. Balance reportado:  ***       M�quina:  Cuenta: Inicio de sesi�n:  Cierre manual:  Balances en el cierre: ToGM , Jugado , Ganado , FromGM , N.Jugadas , N.Ganadas        Balances reportados: ToGM , Jugado , Ganado , FromGM , N.Jugadas , N.Ganadas ', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (131080, 0, N'TerminalSystem_SasMeterReset', N'', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (131081, 0, N'TerminalSystem_SasMeterBigIncrement', N'', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (131089, 0, N'Conectado al servicio WCP, servidor: XXX', N'Conectado al servicio WCP, servidor: XXX', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (131090, 0, N'Desconectado del servicio WCP, servidor: XXX', N'Desconectado del servicio WCP, servidor: XXX', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (131105, 0, N'Conectado al servicio WC2, servidor: ', N'Conectado al servicio WC2, servidor: ', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (131106, 0, N'Desconectado del servicio WC2, servidor:', N'Desconectado del servicio WC2, servidor:', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (131121, 0, N'Conectado al servicio WWP Center, servidor: XXX', N'Conectado al servicio WWP Center, servidor: XXX', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (131122, 0, N'Desconectado del servicio WWP Center', N'Desconectado del servicio WWP Center, servidor: XXX', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (131137, 0, N'Conectado al Centro MultiSite (WWP)', N'Conectado al Centro MultiSite (WWP), direcci�n: XXX', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (131138, 0, N'Desconectado del Centro MultiSite (WCP)', N'Desconectado del Centro MultiSite (WWP), direcci�n: XXX', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (135168, 0, N'Contadores atr�s del juego XXX', N'Contadores atr�s del juego XXX', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (135169, 0, N'Contadores atr�s del juego XXX', N'Contadores atr�s del juego XXX', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (139264, 0, N'Contadores atr�s de m�quina', N'Contadores atr�s de m�quina', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (143360, 0, N'Contadores atr�s de sesi�n de juego', N'Contadores atr�s de sesi�n de juego', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (147456, 0, N'Contadores atr�s de transferencias electr�nicas', N'Contadores atr�s de transferencias electr�nicas (AFT/EFT)', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (151552, 0, N'Contador atr�s de pago manual, reportado XXX atr�s', N'Contador atr�s de pago manual (HPC), reportado XXX atr�s', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (196609, 0, N'Servicio iniciado', N'Servicio iniciado', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (196610, 0, N'Servicio parado', N'Servicio parado', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (196611, 0, N'Servicio reiniciado', N'Servicio reiniciado', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (196612, 0, N'Servicio en ejecuci�n', N'Servicio en ejecuci�n', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (196613, 0, N'Servicio en espera', N'Servicio en espera', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (196614, 0, N'Actividad fuera de horario en la sala', N'Actividad fuera de horario - Resumen del periodo    Se ha detectado actividad en la sala XXX - DESARROLLO - SITE XXX fuera del horario de operaciones configurado.      PERIODO    -Desde: XXX    -Hasta: XXX    ACTIVIDAD DETECTADA    -Logins de usuario    -Operaciones desde WigosGUI    -Movimientos de caja', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (196615, 0, N'Service_TITO_OfflineTicketDiscardError', N'', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (196625, 0, N'Licencia caducada', N'Licencia caducada', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (196626, 0, N'La licencia caducar� pronto', N'La licencia caducar� pronto', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (196641, 0, N'Nueva versi�n descargada: XXX', N'Nueva versi�n descargada: XXX', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (196642, 0, N'Nueva versi�n disponible', N'Nueva versi�n disponible', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (200704, 0, N'Salto de los contadores de juego: XXXXX', N'Salto de los contadores de juego: XXXXX', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (200705, 0, N'Salto de los contadores de juego: XXXXX', N'Salto de los contadores de juego: XXXXX', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (204800, 0, N'Salto de los contadores de m�quina', N'Salto de los contadores de m�quina', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (208896, 0, N'Salto de los contadores de sesi�n de juego', N'Salto de los contadores de sesi�n de juego', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (212992, 0, N'Salto de los contadores de transferencias', N'Salto de los contadores de transferencias electr�nicas (AFT/EFT)', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (262144, 0, N'CRC error en evento del Protocol SAS', N'CRC error en evento del Protocol SAS', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (262145, 0, N'M�ximo n�mero intentos: inicio de sesi�n', N'M�ximo n�mero de intentos de inicio de sesi�n superado', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (262146, 0, N'Hay una diferencia en la entrega de: XXX', N'Hay una diferencia en la entrega de: XXX', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (262147, 0, N'Se ha cerrado la sesi�n de caja XXX desde el GUI.', N'Se ha cerrado la sesi�n de caja XXX desde el GUI.', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (262149, 0, N'User_TITO_TicketOffline', N'', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (262150, 0, N'Dep�sito de Caja con descuadre en denominaciones', N'Dep�sito de Caja con descuadre en denominaciones', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (262151, 0, N'Recaudaci�n con descuadre: XXX', N'Recaudaci�n con descuadre: XXX', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (327681, 0, N'UPS: Cambio entre AC/DC', N'UPS: Cambio entre AC/DC', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (327682, 0, N'UPS: Recuperaci�n despu�s de fallo cr�tico', N'UPS: Recuperaci�n despu�s de fallo cr�tico', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (327683, 0, N'UPS: Bater�a baja', N'UPS: Bater�a baja', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (393216, 0, N'WCP_OPERATION_CODE_NO_OPERATION', N'', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (393217, 0, N'Encendido', N'Encendido', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (393218, 0, N'Apagado', N'Apagado', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (393219, 0, N'Reiniciando', N'Reiniciando', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (393220, 0, N'Calibrar pantalla', N'Calibrar pantalla', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (393221, 0, N'Lanzar explorador', N'Lanzar explorador', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (393222, 0, N'Asignar kiosco', N'Asignar kiosco', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (393223, 0, N'Desasignar kiosco', N'Desasignar kiosco', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (393224, 0, N'Desbloqueo de kiosco mediante llave', N'Desbloqueo de kiosco mediante llave', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (393225, 0, N'Inicio usuario', N'Inicio usuario', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (393226, 0, N'Desconexi�n usuario', N'Desconexi�n usuario', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (393227, 0, N'Propiedades de pantalla', N'Propiedades de pantalla', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (393228, 0, N'Puerta X Abierta', N'Puerta X Abierta', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (393229, 0, N'Puerta X Cerrada', N'Puerta X Cerrada', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (393230, 0, N'Bloquear kiosco', N'Bloquear kiosco', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (393231, 0, N'Desbloquear kiosco', N'Desbloquear kiosco', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (393232, 0, N'Jackpot ganado XXX', N'Jackpot ganado XXX', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (393233, 0, N'Bloqueado por premio grande', N'Bloqueado por premio grande', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (393234, 0, N'Llame al responsable', N'Llame al responsable', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (393235, 0, N'Modo test iniciado', N'Modo test iniciado', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (393236, 0, N'Modo test finalizado', N'Modo test finalizado', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (393237, 0, N'Error de login', N'Error de login', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (393238, 0, N'Entrada al men� de encargado', N'Entrada al men� de encargado', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (393239, 0, N'Salida del men� de encargado', N'Salida del men� de encargado', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (393240, 0, N'Entrada al men� de operador', N'Entrada al men� de operador', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (393241, 0, N'Salida del men� de operador', N'Salida del men� de operador', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (393242, 0, N'Par�metros de configuraci�n modificados', N'Par�metros de configuraci�n modificados', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (393243, 0, N'Pago Manual (solicitado)', N'Pago Manual (solicitado)', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (393244, 0, N'Pago Manual (reset)', N'Pago Manual (reset)', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (393245, 0, N'Tarjeta abandonada', N'Tarjeta abandonada', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (393246, 0, N'Cr�ditos Abandonados. Cantidad: ', N'Cr�ditos Abandonados. Cantidad: ', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (393247, 0, N'Cambio de estado del terminal de juego', N'Cambio de estado del terminal de juego', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (393248, 0, N'WCP_OPERATION_CODE_EVENT', N'', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (393249, 0, N'El Stacker ha cambiado. Nuevo Stacker: XXX ', N'El Stacker del terminal ha cambiado. Nuevo Stacker: XXX ', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (393250, 0, N'WCP_OPERATION_CODE_SECURITY_CASHOUT_UNITS', N'Cr�ditos Abandonados. Cantidad: @p0', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (393251, 0, N'Error Fatal en Juego XXX', N'Error Fatal en Juego XXX', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (393252, 0, N'Transferencia ''Pendiente'' al iniciar el eBox.', N'Se ha encontrado una transferencia de entrada en estado ''Pendiente'' al iniciar el eBox (Monto: ). Posible p�rdida de dinero.', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (393253, 0, N'WCP_OPERATION_CODE_PENDING_TRANSFER_OUT', N'Se ha encontrado una transferencia de salida en estado ''Pendiente'' al iniciar el eBox (Monto: @p0). Posible p�rdida de dinero.', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (458752, 0, N'Dispositivo desconocido - Estado desconocido', N'Dispositivo desconocido - Estado desconocido', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (458753, 0, N'Dispositivo desconocido - OK', N'Dispositivo desconocido - OK', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (458754, 0, N'Dispositivo desconocido - Error', N'Dispositivo desconocido - Error', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (458852, 0, N'Dispositivo desconocido - No est� instalado', N'Dispositivo desconocido - No est� instalado', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (459008, 0, N'Impresora - Estado desconocido', N'Impresora - Estado desconocido', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (459009, 0, N'Impresora - OK', N'Impresora - OK', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (459010, 0, N'Impresora - Error', N'Impresora - Error', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (459011, 0, N'Impresora - Apagada', N'Impresora - Apagada', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (459012, 0, N'Impresora - Preparada', N'Impresora - Preparada', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (459013, 0, N'Impresora - No preparada', N'Impresora - No preparada', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (459014, 0, N'Impresora - Sin papel', N'Impresora - Sin papel', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (459015, 0, N'Impresora - Papel bajo', N'Impresora - Papel bajo', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (459016, 0, N'Impresora - Fuera de l�nea', N'Impresora - Fuera de l�nea', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (459108, 0, N'Impresora - No instalada', N'Impresora - No instalada', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (459264, 0, N'Aceptador de billetes - Estado desconocido', N'Aceptador de billetes - Estado desconocido', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (459265, 0, N'Aceptador de billetes - OK', N'Aceptador de billetes - OK', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (459266, 0, N'Aceptador de billetes - Error', N'Aceptador de billetes - Error', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (459267, 0, N'Aceptador de billetes - Atasco', N'Aceptador de billetes - Atasco', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (459268, 0, N'Aceptador de billetes - N�mero de serie err�neo', N'Aceptador de billetes - N�mero de serie err�neo', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (459269, 0, N'Aceptador de billetes - Lleno', N'Aceptador de billetes - Lleno', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (459270, 0, N'Aceptador de billetes - Abierto', N'Aceptador de billetes - Abierto', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (459364, 0, N'Aceptador de billetes - No instalado', N'Aceptador de billetes - No instalado', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (459520, 0, N'Lector de tarjetas - Estado desconocido', N'Lector de tarjetas - Estado desconocido', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (459521, 0, N'Lector de tarjetas - OK', N'Lector de tarjetas - OK', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (459522, 0, N'Lector de tarjetas - Error', N'Lector de tarjetas - Error', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (459620, 0, N'Lector de tarjetas - No instalado', N'Lector de tarjetas - No instalado', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (459776, 0, N'Ventana de cliente de agencia - Estado desconocido', N'Ventana de cliente de agencia - Estado desconocido', 0)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (459777, 0, N'Ventana de cliente de agencia - OK', N'Ventana de cliente de agencia - OK', 0)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (459778, 0, N'Ventana de cliente de agencia - Error', N'Ventana de cliente de agencia - Error', 0)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (459876, 0, N'Ventana de cliente de agencia - No instalado', N'Ventana de cliente de agencia - No instalado', 0)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (460032, 0, N'Aceptador de monedas - Estado desconocido', N'Aceptador de monedas - Estado desconocido', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (460033, 0, N'Aceptador de monedas - OK', N'Aceptador de monedas - OK', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (460034, 0, N'Aceptador de monedas - Error', N'Aceptador de monedas - Error', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (460035, 0, N'Aceptador de monedas - Atasco', N'Aceptador de monedas - Atasco', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (460036, 0, N'Aceptador de monedas - N�mero de serie err�neo', N'Aceptador de monedas - N�mero de serie err�neo', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (460037, 0, N'Aceptador de monedas - Lleno', N'Aceptador de monedas - Lleno', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (460132, 0, N'Aceptador de monedas - No instalado', N'Aceptador de monedas - No instalado', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (460288, 0, N'Pantalla superior - Estado desconocido', N'Pantalla superior - Estado desconocido', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (460289, 0, N'Pantalla superior - OK', N'Pantalla superior - OK', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (460290, 0, N'Pantalla superior - Error', N'Pantalla superior - Error', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (460388, 0, N'Pantalla superior - No instalado', N'Pantalla superior - No instalado', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (460544, 0, N'Lector de c�digos de barras - Estado desconocido', N'Lector de c�digos de barras - Estado desconocido', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (460545, 0, N'Lector de c�digos de barras - OK', N'Lector de c�digos de barras - OK', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (460546, 0, N'Lector de c�digos de barras - Error', N'Lector de c�digos de barras - Error', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (460644, 0, N'Lector de c�digos de barras - No instalado', N'Lector de c�digos de barras - No instalado', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (460800, 0, N'Intrusi�n - Detectada', N'Intrusi�n - Detectada', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (460801, 0, N'Intrusi�n - OK', N'Intrusi�n - OK', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (460802, 0, N'Intrusi�n - Detectada', N'Intrusi�n - Detectada', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (461056, 0, N'UPS - Estado desconocido', N'UPS - Estado desconocido', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (461057, 0, N'UPS - OK', N'UPS - OK', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (461058, 0, N'UPS - Error', N'UPS - Error', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (461059, 0, N'UPS - Sin electricidad', N'UPS - Sin electricidad', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (461060, 0, N'UPS - Bater�a baja', N'UPS - Bater�a baja', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (461061, 0, N'UPS - Sobrecarga', N'UPS - Sobrecarga', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (461062, 0, N'UPS - Fuera de l�nea', N'UPS - Fuera de l�nea', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (461063, 0, N'UPS - Fallo de bater�a', N'UPS - Fallo de bater�a', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (461156, 0, N'UPS - No instalado', N'UPS - No instalado', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (462592, 0, N'M�dulo de E/S - Estado desconocido', N'M�dulo de E/S - Estado desconocido', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (462593, 0, N'M�dulo de E/S - OK', N'M�dulo de E/S - OK', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (462594, 0, N'M�dulo de E/S - Error', N'M�dulo de E/S - Error', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (462692, 0, N'M�dulo de E/S - No instalado', N'M�dulo de E/S - No instalado', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (463104, 0, N'Pantalla 0 - Estado desconocido', N'Pantalla 0 - Estado desconocido', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (463105, 0, N'Pantalla 0 - OK', N'Pantalla 0 - OK', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (463106, 0, N'Pantalla 0 - Error', N'Pantalla 0 - Error', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (463204, 0, N'Pantalla 0 - No instalada', N'Pantalla 0 - No instalada', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (463360, 0, N'Pantalla 1 - Estado desconocido', N'Pantalla 1 - Estado desconocido', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (463361, 0, N'Pantalla 1 - OK', N'Pantalla 1 - OK', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (463362, 0, N'Pantalla 1 - Error', N'Pantalla 1 - Error', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (463460, 0, N'Pantalla 1 - No instalada', N'Pantalla 1 - No instalada', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (463616, 0, N'Pantalla T�ctil - Estado desconocido', N'Pantalla T�ctil - Estado desconocido', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (463617, 0, N'Pantalla T�ctil - OK', N'Pantalla T�ctil - OK', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (463618, 0, N'Pantalla T�ctil - Error', N'Pantalla T�ctil - Error', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (463716, 0, N'Pantalla T�ctil - No instalada', N'Pantalla T�ctil - No instalada', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (463872, 0, N'Teclado - Estado desconocido', N'Teclado - Estado desconocido', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (463873, 0, N'Teclado - OK', N'Teclado - OK', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (463874, 0, N'Teclado - Error', N'Teclado - Error', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (463972, 0, N'Teclado - No instalado', N'Teclado - No instalado', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (464128, 0, N'LCD Unicash Kit - Estado desconocido', N'LCD Unicash Kit - Estado desconocido', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (464129, 0, N'LCD Unicash Kit - OK', N'LCD Unicash Kit - OK', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (464130, 0, N'LCD Unicash Kit - Error', N'LCD Unicash Kit - Error', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (464228, 0, N'LCD Unicash Kit - No instalado', N'LCD Unicash Kit - No instalado', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (464384, 0, N'Keypad Unicash Kit - Estado desconocido', N'Keypad Unicash Kit - Estado desconocido', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (464385, 0, N'Keypad Unicash Kit - OK', N'Keypad Unicash Kit - OK', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (464386, 0, N'Keypad Unicash Kit - Error', N'Keypad Unicash Kit - Error', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (464484, 0, N'Keypad Unicash Kit - No instalado', N'Keypad Unicash Kit - No instalado', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (464640, 0, N'Lector de tarjetas Unicash Kit - desconocido', N'Lector de tarjetas Unicash Kit - Estado desconocido', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (464641, 0, N'Lector de tarjetas Unicash Kit - OK', N'Lector de tarjetas Unicash Kit - OK', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (464642, 0, N'Lector de tarjetas Unicash Kit - Error', N'Lector de tarjetas Unicash Kit - Error', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (464740, 0, N'Lector de tarjetas Unicash Kit - No instalado', N'Lector de tarjetas Unicash Kit - No instalado', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (464896, 0, N'Placa base Unicash Kit - Estado desconocido', N'Placa base Unicash Kit - Estado desconocido', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (464897, 0, N'Placa base Unicash Kit - OK', N'Placa base Unicash Kit - OK', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (464898, 0, N'Placa base Unicash Kit - Error', N'Placa base Unicash Kit - Error', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (464996, 0, N'Placa base Unicash Kit - No instalado', N'Placa base Unicash Kit - No instalado', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (524289, 0, N'PSA: Reporte Diario ''XXX'': Error obteniendo datos', N'WPA PSA: Reporte Diario ''XXX'': Error obteniendo datos', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (524290, 0, N'PSA:  Sesiones de juego: Error obteniendo datos', N'WPA PSA: Monitorizaci�n Diaria - Sesiones de juego: Error obteniendo datos', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (524291, 0, N'WPA PSA: Estad�sticas: Error obteniendo datos', N'WPA PSA: Monitorizaci�n Diaria - Estad�sticas: Error obteniendo datos', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (589825, 0, N'MultiSite_AccountWithNonUniqueHolderId', N'', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (589826, 0, N'MultiSite_AccountWithExistsTrackData', N'', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (589827, 0, N'MultiSite_AccountWithChangesOfOtherSite', N'', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (589828, 0, N'MultiSite_AccountWithNegativePoints', N'', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (589829, 0, N'MultiSite_UnknownSiteTriedToConnect', N'', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (1048577, 0, N'ELP_IncorrectExternalTrackdata', N'', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (1048578, 0, N'ELP_ErrorConfirmationRedeem', N'', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (1048579, 0, N'ELP_ErrorUpdateAccount', N'', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (1048580, 0, N'ELP_ErrorTrackdataRelation', N'', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (1048581, 0, N'ELP_ErrorStatus', N'', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (1048582, 0, N'ELP_ErrorInsertTransactionInterface', N'', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (1048584, 0, N'ELP_ErrorTrackdataNotAssignedToAccount', N'', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (1114113, 0, N'Discrepancia en la configuraci�n del terminal', N'Discrepancia en la configuraci�n del terminal      - CashableTickets.AllowEmission: XXX Esperado = XXX / Actual = XXX"    - PromotionalTickets.AllowEmission: Esperado = XXX / Actual = XXX" - AllowRedemption: Esperado = XXX / Actual = XXX"', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (1114114, 0, N'WCP_TITO_StackerChangeFail', N'', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (1114115, 0, N'WCP_TITO_WrongStackerState', N'', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (1114116, 0, N'WCP_TITO_EmptyTerminal', N'', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (1114117, 0, N'WCP_TITO_WrongStackerTerminal', N'', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (1114121, 0, N'N�mero de validaci�n incorrecto', N'N�mero de ticket err�neo. SystemId: XXX, ValidationSequence: XXX, Terminal: XXX, Cents: XXX, TicketType: XXX', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (1114128, 0, N'WCP_SuspectedHandPay', N'0x00110010C/0x00110010J', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (1114129, 0, N'WCP_SwValidationError', N'Firma err�nea del software de la m�quina.', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (1114130, 0, N'WCP_SwValidationOK', N'Firma correcta del software de la m�quina.', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (2097153, 0, N'CustomAlarm_Played', N'', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (2097154, 0, N'CustomAlarm_Won', N'', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (2097155, 0, N'CustomAlarm_Canceled_Credit', N'', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (2097156, 0, N'CustomAlarm_Jackpot', N'', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (2097157, 0, N'CustomAlarm_Payout', N'', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (2097158, 0, N'CustomAlarm_Counterfeit', N'', 1)
GO

/* DownloadAlarmPatterns */
INSERT INTO MS_SITE_TASKS (ST_TASK_ID, ST_ENABLED, ST_INTERVAL_SECONDS, ST_MAX_ROWS_TO_UPLOAD) 
VALUES(65,1,360,200)
GO
