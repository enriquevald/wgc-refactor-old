﻿/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 460;

SET @New_ReleaseId = 461;

SET @New_ScriptName = N'2018-08-03 - UpdateTo_18.461.041.sql';
SET @New_Description = N'New release v03.009.0001.01';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO


/*********************************************************************************************************/

/******* GENERAL PARAMS *******/


/******* VIEWS *******/


/******* TABLES  *******/


/******* INDEXES *******/


/******* TRIGERS *******/


/******* RECORDS *******/


/******* PROCEDURES *******/


IF OBJECT_ID (N'dbo.GetHandpaysBalanceReport', N'P') IS NOT NULL
    DROP PROCEDURE dbo.GetHandpaysBalanceReport                 
GO    
CREATE PROCEDURE [dbo].[GetHandpaysBalanceReport]
( @pFromDt            DATETIME   
  ,@pToDt             DATETIME    
  ,@pClosingTime      INTEGER
  ,@pTerminalWhere    NVARCHAR(MAX)
  ,@pWithOutActivity  BIT
  ,@pOnlyUnbalance    BIT
)
AS
BEGIN
  DECLARE @index        AS INT
  DECLARE @nRows        AS INT
  DECLARE @p2007Opening AS DATETIME
  
  DECLARE @c2007Opening AS CHAR(20)
  DECLARE @cFromDt      AS CHAR(20)
  DECLARE @cToDt        AS CHAR(20)
  DECLARE @query        AS VARCHAR(MAX)
  DECLARE @apos         AS CHAR(1)
  DECLARE @query_day    AS VARCHAR(MAX)
  DECLARE @query_where  AS VARCHAR(MAX)
  
  SET @index = 0   
  SET @query_day = ''
  SET @query_where = ''
  
  SET @p2007Opening  = CAST('2007-01-01 00:00:00' AS DATETIME)
  SET @p2007Opening  = DATEADD(HOUR, @pClosingTime, @p2007Opening)
  
  SET @c2007Opening = CONVERT(CHAR(20),@p2007Opening,120)
  SET @cFromDt = CONVERT(CHAR(20),@pFromDt,120)
  SET @cToDt = CONVERT(CHAR(20),@pToDt,120)
  
  SET @apos = CHAR(39)
  SET @nRows  = DATEDIFF(DAY, @pFromDt, @pToDt) + 1 
  
  
  WHILE @index < @nRows                           
  BEGIN
    SET @query_day = @query_day + ' UNION SELECT ' + LTRIM(RTRIM(CAST(@index AS CHAR(5)))) + ' AS ORDER_DATE '                                         
    SET @index = @index + 1                       
  END

  SET @query_day = ' SELECT DATEADD(DAY, ORDER_DATE, CAST('+ @apos + @cFromDt + @apos +' AS DATETIME)) AS ORDER_DATE 
                     FROM (' + SUBSTRING(@query_day,8, LEN(@query_day)) + ') TABLE_DAYS '
           
           
  IF @pWithOutActivity = 1
    BEGIN
      SET @query_where = ' AND (BALANCE_HANDPAYS.SYSTEM_HANDPAYS_TOTAL <> 0 OR BALANCE_HANDPAYS.COUNTERS_HANDPAYS_TOTAL <> 0) '
    END          
         
  IF @pOnlyUnbalance = 1
    BEGIN
      SET @query_where = @query_where + ' AND BALANCE_HANDPAYS.DIFFERENCE <> 0 '
    END
    
    
  IF @query_where <> ''
    BEGIN
      SET @query_where = ' WHERE ' + SUBSTRING(@query_where, 6, LEN(@query_where))
    END 


  SET @query = ' SELECT * FROM (
                                SELECT  TE_PROVIDER_ID, 
                                        TE_TERMINAL_ID, 
                                        TE_NAME, 
                                        ISNULL(TE_MULTI_DENOMINATION, ' + @apos + '--' + @apos + ') AS DENOMINATION, 
                                        
                                        ISNULL(SUM(HAND_PAYS.CREDIT_CANCEL),0) AS SYSTEM_CREDIT_CANCEL, 
                                        ISNULL(SUM(HAND_PAYS.NO_PROGRESIVES),0) AS SYSTEM_JACKPOTS,
                                        ISNULL(SUM(HAND_PAYS.PROGRESIVES),0) AS SYSTEM_PROGRESSIVE_JACKPOTS,
                                        ISNULL(SUM(HAND_PAYS.CREDIT_CANCEL),0) + ISNULL(SUM(HAND_PAYS.PROGRESIVES),0) + ISNULL(SUM(HAND_PAYS.NO_PROGRESIVES),0) AS SYSTEM_HANDPAYS_TOTAL,

                                        ISNULL(SUM(METERS.TOTAL_HAND_PAID_CANCELLED_CREDITS),0) AS COUNTERS_CREDIT_CANCEL,
                                        ISNULL(SUM(METERS.TOTAL_JACKPOT_CREDITS),0) AS COUNTERS_JACKPOTS,
                                        ISNULL(SUM(METERS.TOTAL_HAND_PAID_CANCELLED_CREDITS),0) + ISNULL(SUM(METERS.TOTAL_JACKPOT_CREDITS),0) AS COUNTERS_HANDPAYS_TOTAL,
                                        
                                        (ISNULL(SUM(HAND_PAYS.CREDIT_CANCEL),0) + ISNULL(SUM(HAND_PAYS.PROGRESIVES),0) + ISNULL(SUM(HAND_PAYS.NO_PROGRESIVES),0)) -
                                        (ISNULL(SUM(METERS.TOTAL_HAND_PAID_CANCELLED_CREDITS),0) + ISNULL(SUM(METERS.TOTAL_JACKPOT_CREDITS),0)) AS DIFFERENCE
                                        
                                FROM   TERMINALS 
                                LEFT JOIN (' + LTRIM(RTRIM(@query_day)) + '
                                          ) DIA ON ORDER_DATE <= GETDATE() 
                                LEFT JOIN (SELECT TC_TERMINAL_ID, 
                                                  DATEADD(HOUR, 10, TC_DATE) AS TC_DATE 
                                           FROM   TERMINALS_CONNECTED 
                                           WHERE  TC_DATE >= DATEADD(DAY, DATEDIFF(DAY, 0, CAST('+ @apos + @cFromDt + @apos +' AS DATETIME)), 0) 
                                                  AND TC_DATE < DATEADD(DAY, DATEDIFF(DAY, 0, CAST('+ @apos + @cToDt + @apos +' AS DATETIME)), 0) 
                                                  AND TC_STATUS = 0 
                                                  AND TC_CONNECTED = 1
                                          ) TC ON TC_TERMINAL_ID = TE_TERMINAL_ID AND TC_DATE = DIA.ORDER_DATE 
                                LEFT JOIN (SELECT HP_TERMINAL_ID, 
                                                  DATEADD(DAY, DATEDIFF(HOUR, CAST('+ @apos + @c2007Opening + @apos +' AS DATETIME), HP_DATETIME) / 24, CAST('+ @apos + @c2007Opening + @apos +' AS DATETIME)) AS HP_DATETIME, 
                                                  SUM(CASE WHEN HP_TYPE IN (0, 1000) 
                                                           THEN ISNULL(dbo.GetDualCurrencyAmount(HP_AMOUNT, HP_AMT0, 1), 0) 
																													 ELSE 0 END)  AS CREDIT_CANCEL, 
                                                  SUM(CASE WHEN (HP_TYPE IN (1, 1001) AND (HP_LEVEL >= 1 AND HP_LEVEL <= 32) AND HP_PROGRESSIVE_ID IS NOT NULL) 
                                                           THEN CASE WHEN HP_TAX_BASE_AMOUNT IS NULL 
                                                                     THEN ISNULL(dbo.GetDualCurrencyAmount(HP_AMOUNT, HP_AMT0, 1), 0) 
                                                                     ELSE ISNULL(HP_TAX_BASE_AMOUNT, 0) END 
                                                           ELSE 0 END) AS PROGRESIVES, 
                                                  SUM(CASE WHEN (HP_TYPE IN (1, 1001) AND (HP_LEVEL < 1 OR HP_LEVEL > 32 OR HP_LEVEL IS NULL)) 
                                                           THEN ISNULL(dbo.GetDualCurrencyAmount(HP_AMOUNT, HP_AMT0, 1), 0) 
                                                           ELSE 0 END) + 
                                                  SUM(CASE WHEN (HP_TYPE IN (1, 1001) AND (HP_LEVEL >= 1 AND HP_LEVEL <= 32 AND HP_PROGRESSIVE_ID IS NULL)) 
                                                           THEN CASE WHEN (HP_TAX_BASE_AMOUNT IS NULL) 
                                                                     THEN ISNULL(dbo.GetDualCurrencyAmount(HP_AMOUNT, HP_AMT0, 1), 0)
                                                                     ELSE ISNULL(HP_TAX_BASE_AMOUNT, 0) END 
                                                           ELSE 0 END) AS NO_PROGRESIVES 
                                           FROM HANDPAYS WITH (INDEX (IX_HP_STATUS_CHANGED)) 
                                           WHERE  HP_DATETIME >= CAST('+ @apos + @cFromDt + @apos +' AS DATETIME) 
                                                  AND HP_DATETIME < CAST('+ @apos + @cToDt + @apos +' AS DATETIME) 
                                                  AND HP_TYPE <> 20 
                                          GROUP BY HP_TERMINAL_ID, 
                                                    DATEADD(DAY, DATEDIFF(HOUR, CAST('+ @apos + @c2007Opening + @apos +' AS DATETIME), HP_DATETIME) / 24, CAST('+ @apos + @c2007Opening + @apos +' AS DATETIME))
                                          ) HAND_PAYS ON HP_TERMINAL_ID = TE_TERMINAL_ID AND HP_DATETIME = DIA.ORDER_DATE 
                                LEFT JOIN (SELECT TSMH_TERMINAL_ID, 
                                                  DATEADD(DAY, DATEDIFF(HOUR, CAST('+ @apos + @c2007Opening + @apos +' AS DATETIME), TSMH_DATETIME) / 24, CAST('+ @apos + @c2007Opening + @apos +' AS DATETIME)) AS TSMH_DATE, 
                                                  SUM(CASE WHEN TSMH_METER_CODE = 2 
                                                          THEN ISNULL(CAST(TSMH_METER_INCREMENT AS MONEY) / 100, 0) END) AS TOTAL_JACKPOT_CREDITS, 
                                                  SUM(CASE WHEN TSMH_METER_CODE = 3 
                                                          THEN ISNULL(CAST(TSMH_METER_INCREMENT AS MONEY) / 100, 0) END) AS TOTAL_HAND_PAID_CANCELLED_CREDITS 
                                           FROM   TERMINAL_SAS_METERS_HISTORY 
                                           WHERE  TSMH_TYPE = 1 
                                                  AND TSMH_METER_CODE IN (0, 1, 2, 3 ) 
                                                  AND TSMH_METER_INCREMENT > 0 
                                                  AND TSMH_DATETIME >= CAST('+ @apos + @cFromDt + @apos +' AS DATETIME) 
                                                  AND TSMH_DATETIME < CAST('+ @apos + @cToDt + @apos +' AS DATETIME) 
                                           GROUP BY TSMH_TERMINAL_ID, 
                                                    DATEADD(DAY, DATEDIFF(HOUR, CAST('+ @apos + @c2007Opening + @apos +' AS DATETIME), TSMH_DATETIME) / 24, CAST('+ @apos + @c2007Opening + @apos +' AS DATETIME))
                                          ) METERS ON TE_TERMINAL_ID = TSMH_TERMINAL_ID AND TSMH_DATE = DIA.ORDER_DATE 
                                WHERE (HAND_PAYS.CREDIT_CANCEL IS NOT NULL 
                                        OR HAND_PAYS.PROGRESIVES IS NOT NULL 
                                        OR HAND_PAYS.NO_PROGRESIVES IS NOT NULL 
                                        OR METERS.TOTAL_JACKPOT_CREDITS IS NOT NULL 
                                        OR METERS.TOTAL_HAND_PAID_CANCELLED_CREDITS IS NOT NULL 
                                        OR TC_DATE IS NOT NULL ) ' + LTRIM(RTRIM(@pTerminalWhere)) + '
                                GROUP BY  TE_PROVIDER_ID, 
                                          TE_TERMINAL_ID, 
                                          TE_NAME, 
                                          ISNULL(TE_MULTI_DENOMINATION, ' + @apos + '--' + @apos + ') 
                               )  BALANCE_HANDPAYS ' + @query_where + '                                                                
                 ORDER BY BALANCE_HANDPAYS.TE_PROVIDER_ID, 
                          BALANCE_HANDPAYS.TE_TERMINAL_ID, 
                          BALANCE_HANDPAYS.TE_NAME, 
                          BALANCE_HANDPAYS.DENOMINATION '

  EXECUTE (@query)                          
END 
GO
-- PERMISSIONS
GRANT EXECUTE ON [dbo].[GetHandpaysBalanceReport] TO [wggui] WITH GRANT OPTION
GO

/******* FUNCTIONS *******/


/******* TRIGGERS *******/

