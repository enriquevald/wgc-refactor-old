﻿/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 451;

SET @New_ReleaseId = 452;

SET @New_ScriptName = N'2018-06-27 - UpdateTo_18.452.041.sql';
SET @New_Description = N'New release v03.008.0022'; 

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO


/*********************************************************************************************************/

/**** GENERAL PARAM *****/


/**** VIEW *****/


/******* TABLES  *******/


/******* INDEXES *******/


/******* TRIGERS *******/


/******* RECORDS *******/
IF EXISTS(select * from alarm_catalog WHERE alcg_alarm_code = 262162 AND alcg_language_id = 10)
BEGIN
	UPDATE alarm_catalog
	SET alcg_name = N'Sesión de juego descuadrada', alcg_description = N'Sesión de juego descuadrada'
	WHERE alcg_alarm_code = 262162 AND alcg_language_id = 10
END
ELSE
BEGIN 
	INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible])
    VALUES ( 262162, 10, 0, N'Sesión de juego descuadrada', N'Sesión de juego descuadrada', 1)
END

GO 

IF EXISTS(select * from alarm_catalog WHERE alcg_alarm_code = 262160 AND alcg_language_id = 10)
BEGIN
	UPDATE alarm_catalog
	SET alcg_name = N'Reabrir Sesión', alcg_description = N'Reabrir Sesión'
	WHERE alcg_alarm_code = 262160 AND alcg_language_id = 10
END
ELSE
BEGIN 
	INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible])
    VALUES ( 262160, 10, 0, N'Reabrir Sesión', N'Reabrir Sesión', 1)
END
GO


/******* PROCEDURES *******/


/******* TRIGGERS *******/



