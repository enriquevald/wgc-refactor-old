/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 250;

SET @New_ReleaseId = 251;
SET @New_ScriptName = N'UpdateTo_18.251.037.sql';
SET @New_Description = N'New Index for cage_movements: IX_cgm_cashier_session_id, and alarm corrections.';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/******* TABLES *******/

/******* INDEXES *******/

IF  NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[cage_movements]') AND name = N'IX_cgm_cashier_session_id')
  CREATE NONCLUSTERED INDEX [IX_cgm_cashier_session_id] ON [dbo].[cage_movements] 
  (
        [cgm_cashier_session_id] ASC
  )
  INCLUDE ( [cgm_cage_session_id])
GO

/******* RECORDS *******/

-- Commission check
IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.PaymentMethod' AND GP_SUBJECT_KEY = 'CommissionToCompanyB.Check.VoucherTitle')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Cashier.PaymentMethod', 'CommissionToCompanyB.Check.VoucherTitle', 'Comisi�n uso de cheque')
GO

-- Commission currency
IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.PaymentMethod' AND GP_SUBJECT_KEY = 'CommissionToCompanyB.Currency.VoucherTitle')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Cashier.PaymentMethod', 'CommissionToCompanyB.Currency.VoucherTitle', 'Comisi�n uso de divisa')
GO

-- Alarms
IF NOT EXISTS (SELECT * FROM [dbo].[alarm_catalog] WHERE [alcg_alarm_code] = 212994)
BEGIN
    INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (212994, 10, 0, 'Sas Meter Rollover.', 'Sas Meter Rollover.', 1)
    INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (212994, 9, 0, 'Sas Meter Rollover.', 'Sas Meter Rollover.', 1)
END
ELSE
BEGIN
    UPDATE [dbo].[alarm_catalog] SET [alcg_type] = 0 WHERE [alcg_alarm_code] = 212994
END

IF NOT EXISTS (SELECT * FROM [dbo].[alarm_catalog_per_category] WHERE [alcc_alarm_code] = 212994)
BEGIN
    INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (212994, 1, 0, GETDATE())
END
ELSE
BEGIN
    UPDATE [dbo].[alarm_catalog_per_category] SET [alcc_type] = 0 WHERE [alcc_alarm_code] = 212994
END
      
IF NOT EXISTS (SELECT * FROM [dbo].[alarm_catalog] WHERE [alcg_alarm_code] = 212993)
BEGIN
    INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (212993, 10, 0, 'Contadores sin reportar en las �ltimas 24 horas.', ' El terminal XXX no report� algunos contadores en las �ltimas 24 horas.', 1)
    INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (212993, 9, 0, ' Unreported meters in the last 24 hours.', 'Terminal XXX did not report some meters in the last 24 hours.', 1)
END
ELSE
BEGIN
    UPDATE [dbo].[alarm_catalog] SET [alcg_type] = 0 WHERE [alcg_alarm_code] = 212993
END

IF NOT EXISTS (SELECT * FROM [dbo].[alarm_catalog_per_category] WHERE [alcc_alarm_code] = 212993)
BEGIN
    INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (212993, 1, 0, GETDATE())
END
ELSE
BEGIN
    UPDATE [dbo].[alarm_catalog_per_category] SET [alcc_type] = 0 WHERE [alcc_alarm_code] = 212993
END

GO

/******* STORED PROCEDURES *******/
