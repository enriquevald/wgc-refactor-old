/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 283;

SET @New_ReleaseId = 284;
SET @New_ScriptName = N'UpdateTo_18.284.038.sql';
SET @New_Description = N'ACCOUNTS table modified for PLD';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/******* TABLES *******/

IF EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[accounts]') and name = 'ac_external_aml_file_sequence')
BEGIN
  ALTER TABLE ACCOUNTS DROP COLUMN ac_external_aml_file_sequence;
  ALTER TABLE ACCOUNTS ADD ac_external_aml_file_sequence UNIQUEIDENTIFIER NULL;
END
GO

/******* INDEXES *******/

/******* RECORDS *******/

/******* PROCEDURES *******/

ALTER PROCEDURE [dbo].[wsp_001_pld_actualizacion_expediente]
 (  
    @v_NumeroTarjeta NVARCHAR (50)    = NULL
  , @v_Cliente_Id    BIGINT           = NULL
  , @v_Folio         UNIQUEIDENTIFIER = NULL
  , @v_Date          DATETIME         = NULL
 )
AS
BEGIN

  DECLARE @_account_id BIGINT
  DECLARE @_exist      BIT

  SET @_account_id = @v_Cliente_Id
  SET @_exist = 1
         
  IF @v_NumeroTarjeta IS NOT NULL  
  BEGIN 
    SET @_account_id = ( SELECT   AC_ACCOUNT_ID
                           FROM   ACCOUNTS
                          WHERE   AC_TRACK_DATA = dbo.TrackDataToInternal(@v_NumeroTarjeta)
                       )
                       
    IF (@v_Cliente_Id IS NOT NULL AND @v_Cliente_Id <> @_account_id) OR @_account_id IS NULL
    BEGIN
      SELECT   10 AS Estado -- Cuenta no encontrada
      SET @_exist = 0
    END                       
  END
  ELSE
  BEGIN
    SET @_exist = ( SELECT   1
                      FROM   ACCOUNTS
                     WHERE   AC_ACCOUNT_ID = @v_Cliente_Id 
                  )

    IF @_exist <> 1 OR @_exist IS NULL
    BEGIN
      SELECT   10 AS Estado -- Cuenta no encontrada
      SET @_exist = 0
    END
  END
  
  IF @_exist <> 0
  BEGIN
    BEGIN TRY 
      UPDATE   ACCOUNTS
         SET   AC_EXTERNAL_AML_FILE_SEQUENCE = @v_Folio,
               AC_EXTERNAL_AML_FILE_UPDATED  = @v_Date
       WHERE   AC_ACCOUNT_ID                 = @_account_id
       
      SELECT   0 AS  Estado -- Operaci�n realizada con �xito
    END TRY
    BEGIN CATCH
      SELECT   12 AS Estado -- Error: no se ha podido realizar la operaci�n (error de SQL)
    END CATCH    
  END
END
GO
