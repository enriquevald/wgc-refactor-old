﻿/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 446;

SET @New_ReleaseId = 447;

SET @New_ScriptName = N'2018-06-17 - UpdateTo_18.447.041.sql';
SET @New_Description = N'New release v03.008.0012'; 

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO


/*********************************************************************************************************/

/**** GENERAL PARAM *****/


/**** VIEW *****/


/******* TABLES  *******/


/******* INDEXES *******/

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[mb_movements]') AND name = N'IX_mbm_type')
BEGIN
  DROP INDEX [IX_mbm_type] ON [dbo].[mb_movements]
END


/******* TRIGERS *******/


/******* RECORDS *******/


/******* PROCEDURES *******/

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ReportShortfallExcess]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ReportShortfallExcess]

GO

CREATE PROCEDURE [dbo].[ReportShortfallExcess]
  @pDateFrom DATETIME,  
  @pDateTo   DATETIME,
  @pGamingDayMode BIT
    
AS 
BEGIN

if exists ( select  * from tempdb.dbo.sysobjects o where o.xtype in ('U') and o.id = object_id(N'tempdb..#mb_movements_filter'))
       DROP TABLE #mb_movements_filter

SELECT   MB2.MBM_MB_ID ,
             MB2.MBM_CASHIER_SESSION_ID
  INTO #mb_movements_filter
     FROM   MB_MOVEMENTS MB2 
    WHERE   MB2.MBM_DATETIME >= @pDateFrom
      AND   MB2.MBM_DATETIME < @pDateTo
      AND   MB2.MBM_TYPE IN (3) 

  IF @pGamingDayMode = 1 -- By CS_GAMING_DAY
  BEGIN
    --GUI USERS  
    SELECT   GUI_USERS.GU_USER_ID     as  USERID   
        , GU_USER_TYPE                as  TYPE_USER  
        , GUI_USERS.GU_USERNAME       as  USERNAME   
        , CASE 
            WHEN GU_USER_TYPE = 0
              THEN GUI_USERS.GU_FULL_NAME 
              ELSE CT_NAME 
           END                        as  FULLNAME   
        , GUI_USERS.GU_EMPLOYEE_CODE  as  EMPLOYEECODE   
        , X.CM_CURRENCY_ISO_CODE      as  CURRENCY   
        , X.FALTANTE                  as  FALTANTE   
        , X.SOBRANTE                  as  SOBRANTE   
        , X.SOBRANTE - X.FALTANTE     as  DIFERENCIA  
        , X.CS_CASHIER_ID             as  CASHIERID
        , CT_NAME                     as  CASHIERNAME
        FROM (SELECT   CS_USER_ID  
                     , CM_CURRENCY_ISO_CODE  
                     , SUM (CASE WHEN CM_TYPE = 160 THEN CM_ADD_AMOUNT ELSE 0 END) as FALTANTE  
                     , SUM (CASE WHEN CM_TYPE = 161 THEN CM_ADD_AMOUNT ELSE 0 END) as SOBRANTE               
                     , CS_CASHIER_ID
                FROM   CASHIER_SESSIONS  
          INNER JOIN   CASHIER_MOVEMENTS ON CS_SESSION_ID = CM_SESSION_ID 
               WHERE   CS_GAMING_DAY >= @pDateFrom    
                 AND   CS_GAMING_DAY < @pDateTo  
                 AND   CM_TYPE IN ( 160, 161)  -- 160 = CASH_CLOSING_SHORT, 161 = CASH_CLOSING_OVER  
            GROUP BY   CS_USER_ID  
                     , CM_CURRENCY_ISO_CODE
                     , CS_CASHIER_ID) AS X  
  INNER JOIN   GUI_USERS ON   GU_USER_ID = X.CS_USER_ID
                        AND (X.FALTANTE <>0  OR X.SOBRANTE<>0)  
                        AND (GU_USER_TYPE=0  OR GU_USER_TYPE=6)
   LEFT JOIN   CASHIER_TERMINALS ON   CT_CASHIER_ID = X.CS_CASHIER_ID
   UNION ALL
      --BANK MOBILE USERS (this kind of users have national currency only)
      SELECT   MOBILE_BANKS.MB_ACCOUNT_ID                       as  USERID 
             , 1                                                as  TYPE_USER 
             , 'MB-' + CAST(MOBILE_BANKS.mb_account_id as CHAR) as  USERNAME
             , MOBILE_BANKS.MB_HOLDER_NAME                      as  FULLNAME
             , ''                                               as  EMPLOYEECODE
             , ''                                               as  CURRENCY
             , X2.FALTANTE                                      as  FALTANTE
             , X2.SOBRANTE                                      as  SOBRANTE 
             , (X2.SOBRANTE - X2.FALTANTE)                      as  DIFERENCIA
             , 0                                                as  CASHIERID
             , ''                                               as  CASHIERNAME
        FROM   (SELECT   MBM_MB_ID
                       , SUM(CASE WHEN  MBM_TYPE IN (8,11)  THEN MBM_SUB_AMOUNT ELSE 0 END) AS FALTANTE
                       , SUM(CASE WHEN  MBM_TYPE IN (9,10)       THEN MBM_ADD_AMOUNT ELSE 0 END) AS SOBRANTE
                  FROM   MB_MOVEMENTS MB1 
            INNER JOIN   cashier_sessions ON   MB1.mbm_cashier_session_id = cs_session_id          
                 WHERE   CS_GAMING_DAY  >= @pDateFrom   
                   AND   CS_GAMING_DAY  < @pDateTo   
                   AND  ( ( MBM_TYPE IN (8,9) ) 
                         OR ( MBM_TYPE IN (11,10) 
                            AND  NOT EXISTS (SELECT   MBM_MB_ID
                                               FROM   #mb_movements_filter 
                                              WHERE   MBM_CASHIER_SESSION_ID = cs_session_id ) ) ) 
              GROUP BY   MBM_MB_ID ) AS X2
  INNER JOIN   MOBILE_BANKS ON  X2.MBM_MB_ID = MOBILE_BANKS.MB_ACCOUNT_ID 
       WHERE   X2.FALTANTE <> 0 
          OR   X2.SOBRANTE <> 0
    ORDER BY   USERNAME

  END
  ELSE   -- By CM_DATE or MBM_DATETIME
  BEGIN
    --GUI USERS  
    SELECT   GUI_USERS.GU_USER_ID        as  USERID   
           , GU_USER_TYPE                as  TYPE_USER  
           , GUI_USERS.GU_USERNAME       as  USERNAME   
           , CASE 
              WHEN GU_USER_TYPE = 0
               THEN GUI_USERS.GU_FULL_NAME 
              ELSE CT_NAME 
             END                        as  FULLNAME   
          , GUI_USERS.GU_EMPLOYEE_CODE  as  EMPLOYEECODE   
          , X.CM_CURRENCY_ISO_CODE      as  CURRENCY   
          , X.FALTANTE                  as  FALTANTE   
          , X.SOBRANTE                  as  SOBRANTE   
          , X.SOBRANTE - X.FALTANTE     as  DIFERENCIA  
          , X.CS_CASHIER_ID             as  CASHIERID
          , CT_NAME                     as  CASHIERNAME
     FROM ( SELECT   CS_USER_ID  
                   , CM_CURRENCY_ISO_CODE  
                   , SUM (CASE WHEN CM_TYPE = 160 THEN CM_ADD_AMOUNT ELSE 0 END) as FALTANTE  
                   , SUM (CASE WHEN CM_TYPE = 161 THEN CM_ADD_AMOUNT ELSE 0 END) as SOBRANTE               
                   , CS_CASHIER_ID
              FROM   CASHIER_MOVEMENTS
        INNER JOIN   CASHIER_SESSIONS ON  CM_SESSION_ID  = CS_SESSION_ID      
             WHERE   CM_DATE >= @pDateFrom    
               AND   CM_DATE < @pDateTo  
               AND   CM_TYPE IN (160, 161)  -- 160 = CASH_CLOSING_SHORT, 161 = CASH_CLOSING_OVER  
          GROUP BY   CS_USER_ID  
                   , CM_CURRENCY_ISO_CODE
                   , CS_CASHIER_ID) AS X  
INNER JOIN   GUI_USERS ON   GU_USER_ID = X.CS_USER_ID
                      AND   (X.FALTANTE <>0  OR X.SOBRANTE<>0)  
                      AND   (GU_USER_TYPE=0  OR GU_USER_TYPE=6)
LEFT JOIN   CASHIER_TERMINALS ON   CT_CASHIER_ID = X.CS_CASHIER_ID
UNION ALL
      --BANK MOBILE USERS (this kind of users have national currency only)
    SELECT   MOBILE_BANKS.MB_ACCOUNT_ID                       as  USERID 
           , 1                                                as  TYPE_USER 
           , 'MB-' + CAST(MOBILE_BANKS.mb_account_id as CHAR) as  USERNAME
           , MOBILE_BANKS.MB_HOLDER_NAME                      as  FULLNAME
           , ''                                               as  EMPLOYEECODE
           , ''                                               as  CURRENCY
           , X2.FALTANTE                                      as  FALTANTE
           , X2.SOBRANTE                                      as  SOBRANTE 
           , (X2.SOBRANTE - X2.FALTANTE)                      as  DIFERENCIA
           , 0                                                as  CASHIERID
           , ''                                               as  CASHIERNAME
      FROM ( SELECT   MBM_MB_ID
                    , SUM(CASE WHEN  MBM_TYPE IN (8,11)  THEN MBM_SUB_AMOUNT ELSE 0 END) AS FALTANTE
                    , SUM(CASE WHEN  MBM_TYPE IN (9,10)       THEN MBM_ADD_AMOUNT ELSE 0 END) AS SOBRANTE
               FROM   MB_MOVEMENTS MB1
         INNER JOIN   CASHIER_SESSIONS 
                 ON   MB1.MBM_CASHIER_SESSION_ID = CS_SESSION_ID          
              WHERE MB1.MBM_DATETIME >= @pDateFrom   
                AND MB1.MBM_DATETIME  < @pDateTo   
                AND  (   ( MBM_TYPE IN (8,9) ) 
                      OR ( MBM_TYPE IN (11,10) 
                      AND  NOT EXISTS  (SELECT   MBM_MB_ID
                                          FROM   #mb_movements_filter 
                                         WHERE   MBM_CASHIER_SESSION_ID = cs_session_id) ) ) 
           GROUP BY   MBM_MB_ID) X2
      
 INNER JOIN   MOBILE_BANKS ON   X2.MBM_MB_ID = MOBILE_BANKS.MB_ACCOUNT_ID
      WHERE   X2.FALTANTE<>0 OR X2.SOBRANTE<>0 
   ORDER BY   USERNAME
  END

if exists ( select  * from tempdb.dbo.sysobjects o where o.xtype in ('U') and o.id = object_id(N'tempdb..#mb_movements_filter'))
       DROP TABLE #mb_movements_filter

END
GO

GRANT EXECUTE ON [dbo].[ReportShortfallExcess] TO [wggui] WITH GRANT OPTION
GO


/******* TRIGGERS *******/



