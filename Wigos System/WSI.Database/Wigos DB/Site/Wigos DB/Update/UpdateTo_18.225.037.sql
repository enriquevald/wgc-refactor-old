/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 224;

SET @New_ReleaseId = 225;
SET @New_ScriptName = N'UpdateTo_18.225.037.sql';
SET @New_Description = N'Stored modified: GT_Session_Information and GT_Chips_Operations; Delete movement from cashier movements; Update alarm catalog';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/******* TABLES *******/

/******* INDEXES *******/

/******* RECORDS *******/

ALTER TABLE  [dbo].[cage_movements] ALTER COLUMN [cgm_related_movement_id] bigint NULL;
GO

UPDATE   ALARM_CATALOG
   SET   ALCG_NAME='Top Screen - Unknown status'
       , ALCG_DESCRIPTION='Top Screen - Unknown status'
 WHERE   ALCG_ALARM_CODE=460288 AND ALCG_LANGUAGE_ID=9
GO

UPDATE   ALARM_CATALOG
   SET   ALCG_NAME='Top Screen - OK'
       , ALCG_DESCRIPTION='Top Screen - OK'
 WHERE   ALCG_ALARM_CODE=460289 AND ALCG_LANGUAGE_ID=9
GO     
       
UPDATE   ALARM_CATALOG
   SET   ALCG_NAME='Top Screen - Error'
       , ALCG_DESCRIPTION='Top Screen - Error'
 WHERE   ALCG_ALARM_CODE=460290 AND ALCG_LANGUAGE_ID=9
GO     
       
UPDATE   ALARM_CATALOG
   SET   ALCG_NAME='Top Screen - Not Installed'
       , ALCG_DESCRIPTION='Top Screen - Not Installed'
 WHERE   ALCG_ALARM_CODE=460388 AND ALCG_LANGUAGE_ID=9
GO     
       
UPDATE   ALARM_CATALOG
   SET   ALCG_NAME='Intrusion - OK'
       , ALCG_DESCRIPTION='Intrusion - OK'
 WHERE   ALCG_ALARM_CODE=460801 AND ALCG_LANGUAGE_ID=9
GO     
       
UPDATE   ALARM_CATALOG
   SET   ALCG_NAME='Intrusion � Detected'
       , ALCG_DESCRIPTION='Intrusion � Detected'
 WHERE   ALCG_ALARM_CODE=460802 AND ALCG_LANGUAGE_ID=9
GO

-- DELETE MOVEMENT FROM CASHIER MOVEMENTS

-- CHIPS_SALE_REGISTER_CASH_IN_SPLIT1 = 309
-- CHIPS_SALE_REGISTER_CASH_IN_SPLIT2 = 310
-- CHIPS_SALE_REGISTER_CASH_IN_TAX_SPLIT1 = 312
-- CHIPS_SALE_REGISTER_CASH_IN = 313
-- CHIPS_SALE_REGISTER_CASH_IN_TAX_SPLIT2 = 314

DELETE FROM cashier_movements WHERE cm_type IN (309, 310, 312, 313, 314)
GO

DELETE FROM cashier_movements_grouped_by_hour WHERE cm_type IN (309, 310, 312, 313, 314)
GO

DELETE FROM cashier_movements_grouped_by_session_id WHERE cm_type IN (309, 310, 312, 313, 314)
GO

/******* STORED PROCEDURES *******/

IF OBJECT_ID (N'dbo.GT_Chips_Operations', N'P') IS NOT NULL
    DROP PROCEDURE dbo.GT_Chips_Operations;                 
GO  

CREATE PROCEDURE [dbo].[GT_Chips_Operations] 
 ( @pDateFrom DATETIME
  ,@pDateTo DATETIME
  ,@pStatus INTEGER
  ,@pArea VARCHAR(50)
  ,@pBank VARCHAR(50)
  ,@pOnlyTables INTEGER
  ,@pCashierGroupName VARCHAR(50)
  ,@pValidTypes VARCHAR(4096)
)
AS
BEGIN

----------------------------------------------------------------------------------------------------------------
DECLARE @_DATE_FROM     AS   DATETIME
DECLARE @_DATE_TO       AS   DATETIME
DECLARE @_STATUS        AS   INTEGER
DECLARE @_AREA          AS   VARCHAR(4096)
DECLARE @_BANK          AS   VARCHAR(4096)
DECLARE @_DELIMITER     AS   CHAR(1)
DECLARE @_ONLY_TABLES      AS   INTEGER
DECLARE @_CASHIERS_NAME AS   VARCHAR(50)
DECLARE @_TYPES_TABLE        TABLE(SST_ID INT, SST_VALUE VARCHAR(50)) -- TO STORE THE VALID GAMING TABLE TYPES 

----------------------------------------------------------------------------------------------------------------

-- Initialzitation --
SET @_DELIMITER   =   ','
SET @_DATE_FROM   =   @pDateFrom
SET @_DATE_TO     =   @pDateTo
SET @_STATUS      =   ISNULL(@pStatus, -1)
SET @_AREA        =   ISNULL(@pArea, '')
SET @_BANK        =   ISNULL(@pBank, '')
SET @_ONLY_TABLES =   ISNULL(@pOnlyTables, 1)

IF ISNULL(@_CASHIERS_NAME,'') = '' BEGIN
      SET @_CASHIERS_NAME  = '---CASHIER---'
END

----------------------------------------------------------------------------------------------------------------
-- CHECK DATE PARAMETERS
IF @_DATE_FROM IS NULL
BEGIN
   -- IF DATE FROM IS NULL, USER FIRST POSIBLE DATE
   SET @_DATE_FROM = CAST('' AS DATETIME)
END
IF @_DATE_TO IS NULL
BEGIN
   -- IF DATE TO IS NULL, QUERY UNTIL CURRENT DATE
   SET @_DATE_TO = CAST(GETDATE() AS DATETIME)
END

-- ASSIGN TYPES PARAMETER INTO TABLE
INSERT INTO @_TYPES_TABLE SELECT * FROM dbo.SplitStringIntoTable(@pValidTypes, @_DELIMITER, DEFAULT)

----------------------------------------------------------------------------------------------------------------
-- MAIN QUERY

    SELECT 0 AS TYPE_SESSION
           , CS_SESSION_ID AS SESSION_ID
           , GT_NAME AS GT_NAME
           , CS_OPENING_DATE AS SESSION_DATE
           , GTT_NAME AS GTT_NAME
           , SUM(CM_SUB_AMOUNT) AS GTS_TOTAL_SALES_AMOUNT
           , SUM(CM_ADD_AMOUNT) AS GTS_TOTAL_PURCHASE_AMOUNT
           
      INTO   #CHIPS_OPERATIONS_TABLE           
      FROM   CASHIER_MOVEMENTS
         INNER JOIN   CASHIER_SESSIONS       ON CS_SESSION_ID            = CM_SESSION_ID
         INNER JOIN   CASHIER_TERMINALS      ON CM_CASHIER_ID            = CT_CASHIER_ID
              LEFT JOIN   GAMING_TABLES_SESSIONS ON GTS_CASHIER_SESSION_ID   = CM_SESSION_ID
         INNER JOIN   GAMING_TABLES GT       ON GTS_GAMING_TABLE_ID      = GT_GAMING_TABLE_ID
         INNER JOIN   GAMING_TABLES_TYPES    ON GTT_GAMING_TABLE_TYPE_ID = GT_TYPE_ID 
     WHERE   GT_ENABLED  = CASE WHEN @_STATUS = -1 THEN GT_ENABLED ELSE @_STATUS END
       AND   GT_AREA_ID  = CASE WHEN @_AREA   = '' THEN GT_AREA_ID ELSE @_AREA END
       AND   GT_BANK_ID  = CASE WHEN @_BANK   = '' THEN GT_BANK_ID ELSE @_BANK END
       AND   GT_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE )
       AND   CM_DATE >= @_DATE_FROM
       AND   CM_DATE <  @_DATE_TO
       AND  GT_HAS_INTEGRATED_CASHIER = 1
       AND   CM_TYPE IN (303, 304)  -- 303 = CHIPS_SALE_TOTAL; 304 = CHIPS_PURCHASE_TOTAL
  GROUP BY   GT_NAME, GTT_NAME, CS_SESSION_ID, CS_OPENING_DATE
  ORDER BY   GTT_NAME
  
-- Check if cashiers must be visible  
IF @_ONLY_TABLES = 0
BEGIN

-- Select and join data to show cashiers
   -- Adding cashiers without gaming tables

  INSERT INTO #CHIPS_OPERATIONS_TABLE
       SELECT 1 AS TYPE_SESSION
              , CM_SESSION_ID AS SESSION_ID
              , CT_NAME as GT_NAME
              , CS_OPENING_DATE AS SESSION_DATE
              , @_CASHIERS_NAME as GTT_NAME
              , SUM(CM_SUB_AMOUNT) AS GTS_TOTAL_SALES_AMOUNT
              , SUM(CM_ADD_AMOUNT) AS GTS_TOTAL_PURCHASE_AMOUNT
         FROM   CASHIER_MOVEMENTS
   INNER JOIN   CASHIER_SESSIONS       ON CS_SESSION_ID          = CM_SESSION_ID
   INNER JOIN   CASHIER_TERMINALS      ON CM_CASHIER_ID          = CT_CASHIER_ID
    LEFT JOIN   GAMING_TABLES_SESSIONS ON GTS_CASHIER_SESSION_ID = CM_SESSION_ID
        WHERE   GTS_CASHIER_SESSION_ID IS NULL -- We want the chips operations that are from cashiers that are not gambling tables
          AND   CM_TYPE IN (303, 304)  -- 303 = CHIPS_SALE_TOTAL; 304 = CHIPS_PURCHASE_TOTAL
          AND   CM_DATE >= @_DATE_FROM
          AND   CM_DATE <  @_DATE_TO
     GROUP BY   CT_NAME, CM_SESSION_ID, CS_OPENING_DATE

END
-- Select results
SELECT * FROM #CHIPS_OPERATIONS_TABLE ORDER BY GTT_NAME,GT_NAME
  
-- DROP TEMPORARY TABLE  
DROP TABLE #CHIPS_OPERATIONS_TABLE
  
END  -- END PROCEDURE
GO

-- PERMISSIONS

GRANT EXECUTE ON [dbo].[GT_Chips_Operations] TO [wggui] WITH GRANT OPTION
GO

------------------------------------------------------------------------------------------------------------------------------------

IF OBJECT_ID (N'dbo.GT_Session_Information', N'P') IS NOT NULL
    DROP PROCEDURE dbo.GT_Session_Information;                 
GO  

CREATE PROCEDURE [dbo].[GT_Session_Information] 
 ( @pBaseType INTEGER
  ,@pDateFrom DATETIME 
  ,@pDateTo DATETIME
  ,@pStatus INTEGER
  ,@pEnabled INTEGER
  ,@pAreaId INTEGER
  ,@pBankId INTEGER
  ,@pChipsISOCode VARCHAR(50)
  ,@pChipsCoinsCode INTEGER
  ,@pValidTypes VARCHAR(4096)
)
AS
BEGIN

----------------------------------------------------------------------------------------------------------------
DECLARE @_BASE_TYPE        AS   INTEGER
DECLARE @_DATE_FROM        AS   DATETIME
DECLARE @_DATE_TO          AS   DATETIME
DECLARE @_STATUS           AS   INTEGER
DECLARE @_ENABLED          AS   INTEGER
DECLARE @_AREA             AS   INTEGER
DECLARE @_BANK             AS   INTEGER
DECLARE @_CHIPS_ISO_CODE   AS   VARCHAR(50)
DECLARE @_CHIPS_COINS_CODE AS   VARCHAR(50)
DECLARE @_DELIMITER        AS   CHAR(1)
DECLARE @_TYPES_TABLE           TABLE(SST_ID INT, SST_VALUE VARCHAR(50)) -- TO STORE THE VALID GAMING TABLE TYPES 

-- MOVEMENTS
DECLARE @_CHIPS_MOVEMENT_PURCHASE_TOTAL AS   INTEGER
DECLARE @_CHIPS_MOVEMENT_SALES_TOTAL    AS   INTEGER
DECLARE @_MOVEMENT_FILLER_IN            AS   INTEGER
DECLARE @_MOVEMENT_FILLER_OUT           AS   INTEGER
DECLARE @_MOVEMENT_CAGE_FILLER_OUT      AS   INTEGER
DECLARE @_MOVEMENT_CAGE_CLOSE_SESSION   AS   INTEGER
DECLARE @_MOVEMENT_CLOSE_SESSION         AS   INTEGER

----------------------------------------------------------------------------------------------------------------

-- Initialzitation --
SET @_DELIMITER        =   ','
SET @_BASE_TYPE        =   @pBaseType
SET @_DATE_FROM        =   @pDateFrom
SET @_DATE_TO          =   @pDateTo
SET @_STATUS           =   ISNULL(@pStatus, -1)
SET @_ENABLED          =   ISNULL(@pEnabled, -1)
SET @_AREA             =   ISNULL(@pAreaId, 0)
SET @_BANK             =   ISNULL(@pBankId, 0)
SET @_CHIPS_ISO_CODE   =   ISNULL(@pChipsISOCode, 'X01')
SET @_CHIPS_COINS_CODE =   ISNULL(@pChipsCoinsCode, -100)

SET @_CHIPS_MOVEMENT_PURCHASE_TOTAL =   304
SET @_CHIPS_MOVEMENT_SALES_TOTAL    =   303
SET @_MOVEMENT_FILLER_IN            =   2
SET @_MOVEMENT_FILLER_OUT           =   3
SET @_MOVEMENT_CAGE_FILLER_OUT      =   203
SET @_MOVEMENT_CAGE_CLOSE_SESSION   =   201
SET @_MOVEMENT_CLOSE_SESSION        =   1

----------------------------------------------------------------------------------------------------------------
-- CHECK DATE PARAMETERS
IF @_DATE_FROM IS NULL
BEGIN
   -- IF DATE FROM IS NULL, USER FIRST POSIBLE DATE
   SET @_DATE_FROM = CAST('' AS DATETIME)
END
IF @_DATE_TO IS NULL
BEGIN
   -- IF DATE TO IS NULL, QUERY UNTIL CURRENT DATE
   SET @_DATE_TO = CAST(GETDATE() AS DATETIME)
END

-- ASSIGN TYPES PARAMETER INTO TABLE
INSERT INTO @_TYPES_TABLE SELECT * FROM dbo.SplitStringIntoTable(@pValidTypes, @_DELIMITER, DEFAULT)

----------------------------------------------------------------------------------------------------------------

IF @_BASE_TYPE = 0 
BEGIN
   -- REPORT BY GAMING TABLE SESSION
SELECT    GTS_CASHIER_SESSION_ID                                                           
         , CS_NAME                                                                          
         , CS_STATUS                                                                        
         , GT_NAME                                                                          
         , GTT_NAME                                                                         
         , GT_HAS_INTEGRATED_CASHIER                                                        
         , CT_NAME                                                                          
         , GU_USERNAME                                                                      
         , CS_OPENING_DATE                                                                  
         , CS_CLOSING_DATE                                                                  
         , GTS_TOTAL_PURCHASE_AMOUNT                                                        
         , GTS_TOTAL_SALES_AMOUNT                                                           
         , GTS_INITIAL_CHIPS_AMOUNT                                                         
         , GTS_FINAL_CHIPS_AMOUNT                                                           
         , GTS_TIPS                                                                         
         , GTS_COLLECTED_AMOUNT                                                             
         , GTS_CLIENT_VISITS                                                                
         , AR_NAME                                                                             
         , BK_NAME                                                                             
   FROM         GAMING_TABLES_SESSIONS                                                      
  INNER    JOIN CASHIER_SESSIONS      ON CS_SESSION_ID            = GTS_CASHIER_SESSION_ID  
   LEFT    JOIN GUI_USERS             ON GU_USER_ID               = CS_USER_ID              
   LEFT    JOIN GAMING_TABLES         ON GT_GAMING_TABLE_ID       = GTS_GAMING_TABLE_ID     
   LEFT    JOIN GAMING_TABLES_TYPES   ON GTT_GAMING_TABLE_TYPE_ID = GT_TYPE_ID              
   LEFT    JOIN AREAS                 ON GT_AREA_ID               = AR_AREA_ID              
   LEFT    JOIN BANKS                 ON GT_BANK_ID               = BK_BANK_ID              
   LEFT    JOIN CASHIER_TERMINALS     ON GT_CASHIER_ID            = CT_CASHIER_ID           
 WHERE   (CS_OPENING_DATE >= @_DATE_FROM AND (CS_OPENING_DATE < @_DATE_TO))
   AND   (GT_ENABLED = CASE WHEN @_ENABLED = -1 THEN GT_ENABLED ELSE @_ENABLED END)  
   AND   (CS_STATUS  = CASE WHEN @_STATUS  = -1 THEN CS_STATUS  ELSE @_STATUS  END) 
   AND   (GT_AREA_ID = CASE WHEN @_AREA    = 0  THEN GT_AREA_ID ELSE @_AREA    END) 
   AND   (GT_BANK_ID = CASE WHEN @_BANK    = 0  THEN GT_BANK_ID ELSE @_BANK    END) 
   AND   (GT_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE ))
   ;
   
END
ELSE
BEGIN
   -- REPORT BY CASHIER MOVEMENTS

                      
 SELECT    CM_SESSION_ID                                                                      
         , CS_NAME                                                                            
         , CS_STATUS                                                                          
         , GT_NAME                                                                            
         , GTT_NAME                                                                           
         , GT_HAS_INTEGRATED_CASHIER                                                          
         , CT_NAME                                                                            
         , GU_USERNAME                                                                        
         , CS_OPENING_DATE                                                                    
         , CS_CLOSING_DATE                                                                    
         , SUM(CASE WHEN CM_TYPE                  = @_CHIPS_MOVEMENT_PURCHASE_TOTAL
                    THEN CM_ADD_AMOUNT ELSE 0 END
              )  AS CM_TOTAL_PURCHASE_AMOUNT           
         , SUM(CASE WHEN CM_TYPE                  = @_CHIPS_MOVEMENT_SALES_TOTAL
                    THEN CM_SUB_AMOUNT ELSE 0 END
              )  AS CM_TOTAL_SALES_AMOUNT                  
         , SUM(CASE WHEN CM_CURRENCY_ISO_CODE     = @_CHIPS_ISO_CODE             
                     AND CM_TYPE                  = @_MOVEMENT_FILLER_IN
                    THEN CM_ADD_AMOUNT ELSE 0 END
              )  AS CM_INITIAL_CHIPS_AMOUNT              
         , SUM(CASE WHEN CM_CURRENCY_ISO_CODE     = @_CHIPS_ISO_CODE             
                     AND CM_TYPE                  = @_MOVEMENT_FILLER_OUT
                    THEN CM_SUB_AMOUNT
                             --WHEN CM_CURRENCY_ISO_CODE     = @_CHIPS_ISO_CODE
                             --AND CM_TYPE                       = @_MOVEMENT_CLOSE_SESSION
                    --THEN CM_INITIAL_BALANCE 
                             WHEN CM_CURRENCY_ISO_CODE     = @_CHIPS_ISO_CODE
                             AND CM_TYPE                         = @_MOVEMENT_CAGE_CLOSE_SESSION
                    THEN CM_INITIAL_BALANCE
                    ELSE 0 END
              )  AS CM_FINAL_CHIPS_AMOUNT                                
         , SUM(CASE WHEN CM_TYPE                  = @_MOVEMENT_CAGE_FILLER_OUT
                     AND CM_CURRENCY_ISO_CODE     = @_CHIPS_ISO_CODE             
                     AND CM_CURRENCY_DENOMINATION = @_CHIPS_COINS_CODE
                    THEN CM_SUB_AMOUNT
                    WHEN CM_TYPE                  = @_MOVEMENT_CAGE_CLOSE_SESSION
                     AND CM_CURRENCY_ISO_CODE     = @_CHIPS_ISO_CODE             
                     AND CM_CURRENCY_DENOMINATION = @_CHIPS_COINS_CODE                    
                    THEN CM_INITIAL_BALANCE
                    ELSE 0 END
              )  AS CM_TIPS                       
         , SUM(CASE WHEN GT_HAS_INTEGRATED_CASHIER = 0 THEN --Only gaming table
                  CASE WHEN CM_CURRENCY_ISO_CODE IS NULL OR CM_CURRENCY_ISO_CODE <> @_CHIPS_ISO_CODE THEN 
                      CASE WHEN CM_TYPE   = @_MOVEMENT_FILLER_IN            THEN -1 * ISNULL(CM_AUX_AMOUNT, CM_ADD_AMOUNT)
                           WHEN CM_TYPE   = @_MOVEMENT_FILLER_OUT           THEN      ISNULL(CM_AUX_AMOUNT, CM_SUB_AMOUNT)
                           WHEN CM_TYPE   = @_MOVEMENT_CAGE_CLOSE_SESSION   THEN      ISNULL(CM_AUX_AMOUNT, CM_INITIAL_BALANCE)
                           ELSE 0 END 
                  ELSE 0 END
                WHEN GT_HAS_INTEGRATED_CASHIER = 1 THEN --Gaming table with cashier
                           CASE WHEN CM_TYPE   = @_CHIPS_MOVEMENT_PURCHASE_TOTAL THEN -1 * CM_ADD_AMOUNT
                                WHEN CM_TYPE   = @_CHIPS_MOVEMENT_SALES_TOTAL    THEN      CM_SUB_AMOUNT
                           ELSE 0 END
                ELSE 0 END
              )  AS CM_COLLECTED_AMOUNT 
         , GTS_CLIENT_VISITS                                                                  
         , AR_NAME                                                                               
         , BK_NAME                                                                               
   FROM        CASHIER_MOVEMENTS                                                             
  INNER   JOIN    CASHIER_SESSIONS        ON CS_SESSION_ID            = CM_SESSION_ID           
  INNER   JOIN    GAMING_TABLES_SESSIONS  ON CS_SESSION_ID            = GTS_CASHIER_SESSION_ID  
   LEFT   JOIN    GUI_USERS               ON GU_USER_ID               = CS_USER_ID              
   LEFT   JOIN    GAMING_TABLES           ON GT_GAMING_TABLE_ID       = GTS_GAMING_TABLE_ID     
   LEFT   JOIN    GAMING_TABLES_TYPES     ON GTT_GAMING_TABLE_TYPE_ID = GT_TYPE_ID              
   LEFT   JOIN    AREAS                   ON GT_AREA_ID               = AR_AREA_ID              
   LEFT   JOIN    BANKS                   ON GT_BANK_ID               = BK_BANK_ID              
   LEFT   JOIN    CASHIER_TERMINALS       ON GT_CASHIER_ID            = CT_CASHIER_ID           
 WHERE   (CM_DATE >= @_DATE_FROM AND CM_DATE <= @_DATE_TO) 
   AND    CM_SESSION_ID = CS_SESSION_ID 
   AND   (GT_ENABLED = CASE WHEN @_ENABLED = -1 THEN GT_ENABLED ELSE @_ENABLED END)  
   AND   (CS_STATUS  = CASE WHEN @_STATUS  = -1 THEN CS_STATUS  ELSE @_STATUS  END)             
   AND   (GT_AREA_ID = CASE WHEN @_AREA    = 0  THEN GT_AREA_ID ELSE @_AREA    END) 
   AND   (GT_BANK_ID = CASE WHEN @_BANK    = 0  THEN GT_BANK_ID ELSE @_BANK    END) 
   AND   (GT_TYPE_ID IN ( SELECT SST_VALUE FROM @_TYPES_TABLE ))
                  
  GROUP BY  CM_SESSION_ID         
      , CS_NAME                   
      , CS_STATUS                 
      , GT_NAME                   
      , GTT_NAME                  
      , GT_HAS_INTEGRATED_CASHIER 
      , CT_NAME                   
      , GU_USERNAME               
      , CS_OPENING_DATE           
      , CS_CLOSING_DATE           
      , GTS_CLIENT_VISITS         
      , AR_NAME                   
      , BK_NAME
  
END

END -- END PROCEDURE
GO

-- PERMISSIONS
GRANT EXECUTE ON [dbo].[GT_Session_Information] TO [wggui] WITH GRANT OPTION
GO


