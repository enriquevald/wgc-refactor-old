/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 36;

SET @New_ReleaseId = 38;
SET @New_ScriptName = N'UpdateTo_18.038.008.sql';
SET @New_Description = N'New index IX_datetime in table Cashier_Vouchers, and new records in general params.';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/

/****** INDEXES ******/
/****** Object:  Index [IX_datetime]    ******/
IF  NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[cashier_vouchers]') AND name = N'IX_datetime')
CREATE NONCLUSTERED INDEX [IX_datetime] ON [dbo].[cashier_vouchers] 
(
      [cv_datetime] ASC,
      [cv_type] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY];
ELSE
    SELECT '***** Index cashier_vouchers.IX_datetime already exists *****';

/****** RECORDS ******/
/* Cashier.MaxAllowedDailyCashIn */
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY ='MaxAllowedDailyCashIn')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key]
             ,[gp_subject_key]
             ,[gp_key_value])
       VALUES
             ('Cashier'
             ,'MaxAllowedDailyCashIn'
             ,'0');
ELSE
  SELECT '***** Record Cashier.MaxAllowedDailyCashIn already exists *****';

/* Cashier.PrizeComputationMode */
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY ='PrizeComputationMode')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key]
             ,[gp_subject_key]
             ,[gp_key_value])
       VALUES
             ('Cashier'
             ,'PrizeComputationMode'
             ,'0');
ELSE
  SELECT '***** Record Cashier.PrizeComputationMode already exists *****';

/* Cashier.PrizesExpireAfterDays */
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY ='PrizesExpireAfterDays')
BEGIN
  INSERT INTO [dbo].[general_params]
             ([gp_group_key]
             ,[gp_subject_key]
             ,[gp_key_value])
       VALUES
             ('Cashier'
             ,'PrizesExpireAfterDays'
             ,'0');
  UPDATE [dbo].[general_params]
     SET gp_key_value = (SELECT gp_key_value
                           FROM general_params
                          WHERE gp_group_key = 'Cashier'
                            AND gp_subject_key ='CreditsExpireAfterDays')
   WHERE gp_group_key = 'Cashier'
     AND gp_subject_key ='PrizesExpireAfterDays'
END
ELSE
  SELECT '***** Record Cashier.PrizesExpireAfterDays already exists *****';

/**************************************** 3GS Section ****************************************/

/**** CHECK DATABASE EXISTENCE SECTION *****/
GO
IF (NOT EXISTS (SELECT * FROM sys.databases WHERE name = 'sPOS'))
BEGIN
	SELECT 'No 3GS interface to update.';

	raiserror('No 3GS interface to update', 20, -1) with log;
END
ELSE 
    SELECT 'Updating 3GS interface...';


/**** 3GS DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);
    
/**** 3GS INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 6;

SET @New_ReleaseId = 8;
SET @New_ScriptName = N'UpdateTo_18.038.008.sql';
SET @New_Description = N'Only numeration version change.';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version_interface_3gs WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong 3GS DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION_INTERFACE_3GS

raiserror('Not updated', 20, -1) with log
END

/**** UPDATE VERSION SECTION *****/
UPDATE db_version_interface_3gs
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
