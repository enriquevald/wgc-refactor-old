/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 75;

SET @New_ReleaseId = 76;
SET @New_ScriptName = N'UpdateTo_18.076.019.sql';
SET @New_Description = N'Promo-Box (WKT) tables and data. Index for cashier_vouchers.';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO


/****** TABLES ******/

/* PromoBOX tables */

IF  NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wkt_ads]') AND type in (N'U'))
BEGIN
  CREATE TABLE [dbo].[wkt_ads](
        [ad_id] [bigint] IDENTITY(10000,1) NOT NULL,
        [ad_name] [nvarchar](50) NOT NULL,
        [ad_description] [nvarchar](500) NOT NULL,
        [ad_from] [datetime] NOT NULL,
        [ad_to] [datetime] NOT NULL,
        [ad_enabled] [bit] NOT NULL,
        [ad_weekday] [int] NOT NULL,
        [ad_time_from1] [int] NOT NULL,
        [ad_time_to1] [int] NOT NULL,
        [ad_time_from2] [int] NULL,
        [ad_time_to2] [int] NULL,
  CONSTRAINT [PK_WKT_Ads] PRIMARY KEY CLUSTERED 
  (
        [ad_id] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
  ) ON [PRIMARY]
END
ELSE
  SELECT '***** Table wkt_ads already exists *****'

GO

IF  NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wkt_ad_steps]') AND type in (N'U'))
BEGIN
  CREATE TABLE [dbo].[wkt_ad_steps](
        [as_ad_id] [bigint] NULL,
        [as_step_id] [bigint] IDENTITY(1,1) NOT NULL,
        [as_step_order] [int] NOT NULL,
        [as_duration] [int] NULL,
  CONSTRAINT [PK_wkt_ad_steps] PRIMARY KEY CLUSTERED 
  (
        [as_step_id] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
  ) ON [PRIMARY]

  ALTER TABLE [dbo].[wkt_ad_steps]  WITH CHECK ADD  CONSTRAINT [FK_wkt_ad_steps_wkt_ads] FOREIGN KEY([as_ad_id])
  REFERENCES [dbo].[wkt_ads] ([ad_id])
  ON DELETE CASCADE

  ALTER TABLE [dbo].[wkt_ad_steps] CHECK CONSTRAINT [FK_wkt_ad_steps_wkt_ads]
END
ELSE
  SELECT '***** Table wkt_ad_steps already exists *****'

GO

IF  NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wkt_ad_step_details]') AND type in (N'U'))
BEGIN
  CREATE TABLE [dbo].[wkt_ad_step_details](
        [ascr_step_id] [bigint] NOT NULL,
        [ascr_zorder] [int] NOT NULL,
        [ascr_rect_x] [int] NOT NULL,
        [ascr_rect_y] [int] NOT NULL,
        [ascr_rect_width] [int] NOT NULL,
        [ascr_rect_height] [int] NOT NULL,
        [ascr_media_type] [int] NULL,
        [ascr_resource_id] [bigint] NULL,
        [ascr_text] [nvarchar](max) NULL,
        [ascr_text_color] [int] NULL,
        [ascr_text_font] [int] NULL,
        [ascr_text_alignment] [int] NULL,
  CONSTRAINT [PK_wkt_ad_step_details] PRIMARY KEY CLUSTERED 
  (
        [ascr_step_id] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
  ) ON [PRIMARY]

  ALTER TABLE [dbo].[wkt_ad_step_details]  WITH CHECK ADD  CONSTRAINT [FK_wkt_ad_step_details_wkt_ad_steps] FOREIGN KEY([ascr_step_id])
  REFERENCES [dbo].[wkt_ad_steps] ([as_step_id])
  ON DELETE CASCADE

  ALTER TABLE [dbo].[wkt_ad_step_details] CHECK CONSTRAINT [FK_wkt_ad_step_details_wkt_ad_steps]

  ALTER TABLE [dbo].[wkt_ad_step_details]  WITH CHECK ADD  CONSTRAINT [FK_wkt_ad_step_details_wkt_resources] FOREIGN KEY([ascr_resource_id])
  REFERENCES [dbo].[wkt_resources] ([res_resource_id])

  ALTER TABLE [dbo].[wkt_ad_step_details] CHECK CONSTRAINT [FK_wkt_ad_step_details_wkt_resources]
END
ELSE
  SELECT '***** Table wkt_ad_step_details already exists *****'

GO

IF  NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wkt_functionalities]') AND type in (N'U'))
BEGIN
  CREATE TABLE [dbo].[wkt_functionalities](
        [fun_function_id] [int] NOT NULL,
        [fun_name] [nvarchar](50) NOT NULL,
        [fun_enabled] [int] NOT NULL,
  CONSTRAINT [PK_wkt_functionalities] PRIMARY KEY CLUSTERED 
  (
        [fun_function_id] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
  ) ON [PRIMARY]
END
ELSE
  SELECT '***** Table wkt_functionalities already exists *****'

GO

IF  NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wkt_images]') AND type in (N'U'))
BEGIN
  CREATE TABLE [dbo].[wkt_images](
        [cim_image_id] [int] NOT NULL,
        [cim_name] [nvarchar](50) NOT NULL,
        [cim_resource_id] [bigint] NOT NULL,
        [cim_expected_w] [int] NOT NULL,
        [cim_expected_h] [int] NOT NULL,
  CONSTRAINT [PK_wkt_images] PRIMARY KEY CLUSTERED 
  (
        [cim_image_id] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
  ) ON [PRIMARY]
END
ELSE
  SELECT '***** Table wkt_images already exists *****'

GO

IF  NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wkt_player_info_fields]') AND type in (N'U'))
BEGIN
  CREATE TABLE [dbo].[wkt_player_info_fields](
    [pif_field_id] [int] NOT NULL,
    [pif_name] [nvarchar](50) NOT NULL,
    [pif_shown] [bit] NOT NULL,
    [pif_user_allow_edit] [bit] NOT NULL,
    [pif_type] [int] NOT NULL,
    [pif_min_length] [int] NOT NULL,
    [pif_max_length] [int] NOT NULL,
    [pif_editable] [bit] NOT NULL,
    [pif_order] [int] IDENTITY(0,1) NOT NULL,
  CONSTRAINT [PK_wkt_player_info_fields] PRIMARY KEY CLUSTERED 
  (
    [pif_field_id] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
  ) ON [PRIMARY]
END
ELSE
  SELECT '***** Table wkt_player_info_fields already exists *****'
GO


/****** INDEXES ******/

IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[cashier_vouchers]') AND name = N'IX_cv_operation_id_datetime')
  CREATE NONCLUSTERED INDEX [IX_cv_operation_id_datetime] ON [dbo].[cashier_vouchers] 
  (
	  [cv_operation_id] ASC,
	  [cv_datetime] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
ELSE
  SELECT '***** Index cashier_vouchers.IX_cv_operation_id_datetime already exists *****';

GO

/****** RECORDS ******/

INSERT INTO WKT_PLAYER_INFO_FIELDS 
( pif_field_id,pif_name,pif_shown,pif_user_allow_edit,pif_type,pif_min_length,pif_max_length,pif_editable)
values(     1,'accounts.ac_holder_name',1,0,2,1,200,0)
INSERT INTO WKT_PLAYER_INFO_FIELDS 
( pif_field_id,pif_name,pif_shown,pif_user_allow_edit,pif_type,pif_min_length,pif_max_length,pif_editable)
values(     52,'accounts.ac_holder_name3',1,1,2,1,50,1)
INSERT INTO WKT_PLAYER_INFO_FIELDS 
( pif_field_id,pif_name,pif_shown,pif_user_allow_edit,pif_type,pif_min_length,pif_max_length,pif_editable)
values(     50,'accounts.ac_holder_name1',1,1,2,1,50,1)
INSERT INTO WKT_PLAYER_INFO_FIELDS 
( pif_field_id,pif_name,pif_shown,pif_user_allow_edit,pif_type,pif_min_length,pif_max_length,pif_editable)
values(     51,'accounts.ac_holder_name2',1,1,2,0,50,1)
INSERT INTO WKT_PLAYER_INFO_FIELDS 
( pif_field_id,pif_name,pif_shown,pif_user_allow_edit,pif_type,pif_min_length,pif_max_length,pif_editable)
values(     60,'accounts.ac_holder_birth_date',1,1,6,0,10,1)
INSERT INTO WKT_PLAYER_INFO_FIELDS 
( pif_field_id,pif_name,pif_shown,pif_user_allow_edit,pif_type,pif_min_length,pif_max_length,pif_editable)
values(     4,'accounts.ac_pin',1,1,1,4,4,1)
INSERT INTO WKT_PLAYER_INFO_FIELDS 
( pif_field_id,pif_name,pif_shown,pif_user_allow_edit,pif_type,pif_min_length,pif_max_length,pif_editable)
values(     20,'accounts.ac_holder_email_01',1,1,2,0,50,1)
INSERT INTO WKT_PLAYER_INFO_FIELDS 
( pif_field_id,pif_name,pif_shown,pif_user_allow_edit,pif_type,pif_min_length,pif_max_length,pif_editable)
values(     3,'accounts.ac_holder_zip',1,1,2,0,10,1)
INSERT INTO WKT_PLAYER_INFO_FIELDS 
( pif_field_id,pif_name,pif_shown,pif_user_allow_edit,pif_type,pif_min_length,pif_max_length,pif_editable)
values(     10,'accounts.ac_holder_address_01',1,1,2,0,50,1)
INSERT INTO WKT_PLAYER_INFO_FIELDS 
( pif_field_id,pif_name,pif_shown,pif_user_allow_edit,pif_type,pif_min_length,pif_max_length,pif_editable)
values(     11,'accounts.ac_holder_address_02',1,1,2,0,50,1)
INSERT INTO WKT_PLAYER_INFO_FIELDS 
( pif_field_id,pif_name,pif_shown,pif_user_allow_edit,pif_type,pif_min_length,pif_max_length,pif_editable)
values(     2,'accounts.ac_holder_city',1,1,2,0,50,1)
INSERT INTO WKT_PLAYER_INFO_FIELDS 
( pif_field_id,pif_name,pif_shown,pif_user_allow_edit,pif_type,pif_min_length,pif_max_length,pif_editable)
values(     30,'accounts.ac_holder_phone_number_01',1,1,2,0,20,1)
INSERT INTO WKT_PLAYER_INFO_FIELDS 
( pif_field_id,pif_name,pif_shown,pif_user_allow_edit,pif_type,pif_min_length,pif_max_length,pif_editable)
values(     31,'accounts.ac_holder_phone_number_02',1,1,2,0,20,1)
INSERT INTO WKT_PLAYER_INFO_FIELDS 
( pif_field_id,pif_name,pif_shown,pif_user_allow_edit,pif_type,pif_min_length,pif_max_length,pif_editable)
values(     12,'accounts.ac_holder_address_03',0,1,2,0,50,1)
INSERT INTO WKT_PLAYER_INFO_FIELDS 
( pif_field_id,pif_name,pif_shown,pif_user_allow_edit,pif_type,pif_min_length,pif_max_length,pif_editable)
values(     40,'accounts.ac_holder_id1',0,0,2,0,20,0)
INSERT INTO WKT_PLAYER_INFO_FIELDS 
( pif_field_id,pif_name,pif_shown,pif_user_allow_edit,pif_type,pif_min_length,pif_max_length,pif_editable)
values(     41,'accounts.ac_holder_id2',0,0,2,0,20,0)
INSERT INTO WKT_PLAYER_INFO_FIELDS 
( pif_field_id,pif_name,pif_shown,pif_user_allow_edit,pif_type,pif_min_length,pif_max_length,pif_editable)
values(     21,'accounts.ac_holder_email_02',0,1,2,0,50,1)

GO

INSERT INTO [dbo].[wkt_images]
           ([cim_image_id]
           ,[cim_name]
           ,[cim_resource_id]
           ,[cim_expected_w]
           ,[cim_expected_h])
     VALUES
           (0
           ,'IMG00'
           ,0
           ,0
           ,0)

INSERT INTO [dbo].[wkt_images]
           ([cim_image_id]
           ,[cim_name]
           ,[cim_resource_id]
           ,[cim_expected_w]
           ,[cim_expected_h])
     VALUES
           (1
           ,'IMG01'
           ,0
           ,0
           ,0)

GO

insert into wkt_functionalities(fun_function_id,fun_name,fun_enabled)
values (0,'Print list',0)

GO
