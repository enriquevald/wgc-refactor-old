/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 73;

SET @New_ReleaseId = 74;
SET @New_ScriptName = N'UpdateTo_18.074.018.sql';
SET @New_Description = N'Auditory columns size changed & General Params.';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO


--
-- Fix GP Names from version 18.073.018
--
UPDATE   [dbo].[general_params]
   SET   [gp_subject_key] = 'Password.MinDigits'
 WHERE   [gp_group_key]   = 'User'
   AND   [gp_subject_key] = 'Password.MinDig'

UPDATE   [dbo].[general_params]
   SET   [gp_subject_key] = 'Password.RulesEnabled'
 WHERE   [gp_group_key]   = 'User'
   AND   [gp_subject_key] = 'RulesEnabled'

UPDATE   [dbo].[general_params]
   SET   [gp_subject_key] = 'Password.ChangeNextTime'
 WHERE   [gp_group_key]   = 'User'
   AND   [gp_subject_key] = 'Password.ChangeFirstTime'

DELETE
  FROM   [dbo].[promotions]
 WHERE   [pm_name] = 'Importación - Redimible'
    OR   [pm_name] = 'Importación - No Redimible'

--
-- Sales Limits
--
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'MobileBank' AND GP_SUBJECT_KEY ='AutomaticRenewLimitAfterDeposit')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key]
             ,[gp_subject_key]
             ,[gp_key_value])
       VALUES
             ('MobileBank'
             ,'AutomaticRenewLimitAfterDeposit' 
             ,'1')

--
-- Auditory
--
IF EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[gui_audit]') and name = 'ga_nls_param01')
  ALTER TABLE  [dbo].[gui_audit]
  ALTER COLUMN [ga_nls_param01] [nvarchar](150) NULL
ELSE
  SELECT '***** Field gui_audit.ga_nls_param01 does not exist *****';

IF EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[gui_audit]') and name = 'ga_nls_param02')
  ALTER TABLE  [dbo].[gui_audit]
  ALTER COLUMN [ga_nls_param02] [nvarchar](150) NULL
ELSE
  SELECT '***** Field gui_audit.ga_nls_param02 does not exist *****';

IF EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[gui_audit]') and name = 'ga_nls_param03')
  ALTER TABLE  [dbo].[gui_audit]
  ALTER COLUMN [ga_nls_param03] [nvarchar](150) NULL
ELSE
  SELECT '***** Field gui_audit.ga_nls_param03 does not exist *****';

