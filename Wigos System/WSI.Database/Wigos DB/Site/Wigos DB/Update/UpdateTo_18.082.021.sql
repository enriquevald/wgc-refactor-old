/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 81;

SET @New_ReleaseId = 82;
SET @New_ScriptName = N'UpdateTo_18.082.021.sql';
SET @New_Description = N'Accounts Import & Request/Unique fields in cashier.';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/

ALTER TABLE dbo.accounts ADD
	ac_holder_is_vip bit NULL,
	ac_holder_title nvarchar(15) NULL,
	ac_holder_name4 nvarchar(50) NULL,
	ac_holder_wedding_date datetime NULL,
	ac_holder_phone_type_01 int NULL,
	ac_holder_phone_type_02 int NULL,
	ac_holder_state nvarchar(50) NULL,
	ac_holder_country nvarchar(50) NULL,
	ac_holder_address_01_alt nvarchar(50) NULL,
	ac_holder_address_02_alt nvarchar(50) NULL,
	ac_holder_address_03_alt nvarchar(50) NULL,
	ac_holder_city_alt nvarchar(50) NULL,
	ac_holder_zip_alt nvarchar(10) NULL,
	ac_holder_state_alt nvarchar(50) NULL,
	ac_holder_country_alt nvarchar(50) NULL,
	ac_holder_is_smoker bit NULL,
	ac_holder_nickname nvarchar(25) NULL,
	ac_holder_credit_limit money NULL,
	ac_holder_request_credit_limit money NULL,
	ac_pin_last_modified datetime NULL,
	ac_last_activity_site_id nvarchar(25) NULL,
	ac_creation_site_id nvarchar(25) NULL,
	ac_last_update_site_id nvarchar(25) NULL

GO

/****** RECORDS ******/

INSERT INTO GENERAL_PARAMS ( GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE ) VALUES ( 'Cashier', 'DecimalRoundingEnabled', '0' );

INSERT INTO GENERAL_PARAMS ( GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE ) VALUES ( 'Account.UniqueField', 'Name1',     '1' );
INSERT INTO GENERAL_PARAMS ( GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE ) VALUES ( 'Account.UniqueField', 'Name2',     '1' );
INSERT INTO GENERAL_PARAMS ( GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE ) VALUES ( 'Account.UniqueField', 'Name3',     '1' );
INSERT INTO GENERAL_PARAMS ( GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE ) VALUES ( 'Account.UniqueField', 'Gender',    '1' );
INSERT INTO GENERAL_PARAMS ( GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE ) VALUES ( 'Account.UniqueField', 'BirthDate', '1' );
INSERT INTO GENERAL_PARAMS ( GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE ) VALUES ( 'Account.UniqueField', 'Document',  '0' );
INSERT INTO GENERAL_PARAMS ( GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE ) VALUES ( 'Account.UniqueField', 'Phone1',    '0' );
---
INSERT INTO GENERAL_PARAMS ( GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE ) VALUES ( 'Account.RequestedField', 'Name1',     '1' );
INSERT INTO GENERAL_PARAMS ( GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE ) VALUES ( 'Account.RequestedField', 'Name2',     '1' );
INSERT INTO GENERAL_PARAMS ( GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE ) VALUES ( 'Account.RequestedField', 'Name3',     '1' );
INSERT INTO GENERAL_PARAMS ( GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE ) VALUES ( 'Account.RequestedField', 'Gender',    '1' );
INSERT INTO GENERAL_PARAMS ( GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE ) VALUES ( 'Account.RequestedField', 'BirthDate', '1' );
INSERT INTO GENERAL_PARAMS ( GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE ) VALUES ( 'Account.RequestedField', 'Document',  '1' );
INSERT INTO GENERAL_PARAMS ( GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE ) VALUES ( 'Account.RequestedField', 'Phone1',    '0' );
INSERT INTO GENERAL_PARAMS ( GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE ) VALUES ( 'Account.RequestedField', 'Email1',    '0' );

INSERT INTO GENERAL_PARAMS ( GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE ) VALUES ( 'Account', 'AllowDuplicate',    '0' );
