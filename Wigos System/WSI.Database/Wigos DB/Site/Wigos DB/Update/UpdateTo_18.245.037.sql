/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 244;

SET @New_ReleaseId = 245;
SET @New_ScriptName = N'UpdateTo_18.245.037.sql';
SET @New_Description = N'Added mb fields (excess), new alarms, SP modified: CashierMovementsGrouped_Read, PR_collection_by_machine_and_date';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/******* TABLES *******/

IF EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[wcp_commands]') AND name = 'cmd_parameter')
  ALTER TABLE [dbo].[wcp_commands] ALTER COLUMN [cmd_parameter] [nvarchar](2000) NULL;
GO

IF NOT EXISTS (SELECT * FROM sys.columns  WHERE object_id = object_id(N'[dbo].[mobile_banks]') AND name = 'mb_total_limit') 
  ALTER TABLE dbo.mobile_banks ADD mb_total_limit Money NULL
GO

IF NOT EXISTS (SELECT * FROM sys.columns  WHERE object_id = object_id(N'[dbo].[mobile_banks]') AND name = 'mb_recharge_limit') 
  ALTER TABLE dbo.mobile_banks ADD mb_recharge_limit Money NULL
GO

IF NOT EXISTS (SELECT * FROM sys.columns  WHERE object_id = object_id(N'[dbo].[mobile_banks]') AND name = 'mb_number_of_recharges_limit') 
  ALTER TABLE dbo.mobile_banks ADD mb_number_of_recharges_limit Int NULL
GO

IF NOT EXISTS (SELECT * FROM sys.columns  WHERE object_id = object_id(N'[dbo].[mobile_banks]') AND name = 'mb_actual_number_of_recharges') 
  ALTER TABLE dbo.mobile_banks ADD mb_actual_number_of_recharges Int NULL
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[mobile_banks]') and name = 'mb_employee_code')
  ALTER TABLE dbo.mobile_banks ADD mb_employee_code nvarchar(40) NULL
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[mobile_banks]') and name = 'mb_shortfall_cash')
  ALTER TABLE mobile_banks ADD mb_shortfall_cash MONEY NOT NULL CONSTRAINT DF_mobile_banks_mb_shortfall_cash DEFAULT(0)
GO


IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[cage_sessions]') and name = 'cgs_working_day')
  ALTER TABLE dbo.cage_sessions ADD cgs_working_day datetime NOT NULL CONSTRAINT DF_cage_sessions_cgs_working_day DEFAULT getdate()
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[money_collection_details]') and name = 'mcd_cage_currency_type')
  ALTER TABLE dbo.money_collection_details ADD mcd_cage_currency_type INT NOT NULL  DEFAULT (0)
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PK_money_collection_details]') AND type in (N'PK'))
  ALTER TABLE dbo.money_collection_details DROP CONSTRAINT PK_money_collection_details
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PK_money_collection_details]') AND type in (N'PK'))
      ALTER TABLE dbo.money_collection_details ADD CONSTRAINT
            PK_money_collection_details PRIMARY KEY CLUSTERED 
            (
            mcd_collection_id,
            mcd_face_value,
            mcd_cage_currency_type
            ) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

/******* INDEXES *******/

IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[cage_movements]') AND name = N'IX_cgm_mc_collection_id')
  CREATE NONCLUSTERED INDEX [IX_cgm_cage_session_id] ON [dbo].[cage_movements] 
  (
        [cgm_cage_session_id] ASC
  )
  INCLUDE ( [cgm_mc_collection_id]) 
  WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

/******* RECORDS *******/

UPDATE cage_sessions SET cgs_working_day = dbo.Opening(0, cgs_open_datetime)
GO

INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (212993, 10, 1, 'Contadores sin reportar en las �ltimas 24 horas.', ' El terminal XXX no report� algunos contadores en las �ltimas 24 horas.', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (212993, 9, 1, ' Unreported meters in the last 24 hours.', 'Terminal XXX did not report some meters in the last 24 hours.', 1)
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (212993, 1, 1, GETDATE());
GO

INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (393257, 9, 0, ' Application disconnected from DB', ' Application disconnected from DB', 1);
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (393257, 10, 0, 'Aplicaci�n desconectada de BD', 'Aplicaci�n desconectada de BD', 1);
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (393257, 48, 0, GETDATE());
GO

INSERT [dbo].[alarm_categories] ([alc_category_id], [alc_language_id], [alc_alarm_group_id], [alc_type], [alc_name], [alc_description], [alc_visible]) VALUES (48, 9, 3, 0, 'Applications', '', 1)
INSERT [dbo].[alarm_categories] ([alc_category_id], [alc_language_id], [alc_alarm_group_id], [alc_type], [alc_name], [alc_description], [alc_visible]) VALUES (48, 10, 3, 0, 'Aplicaciones', '', 1)
GO

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Alarms' AND GP_SUBJECT_KEY = 'Terminal.DailyUnreportedMeters')
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Alarms','Terminal.DailyUnreportedMeters','0');
GO

IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cage' AND GP_SUBJECT_KEY = 'Transfer.OnlyCashierIsOpen')
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Cage', 'Transfer.OnlyCashierIsOpen','0')
GO

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.Voucher' AND GP_SUBJECT_KEY = 'VoucherOnPromotion.HideCurrencySymbol')
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Cashier.Voucher', 'VoucherOnPromotion.HideCurrencySymbol', '0')
GO

IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'MobileBank' AND GP_SUBJECT_KEY = 'RegisterShortfallOnDeposit')
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('MobileBank', 'RegisterShortfallOnDeposit', '0')
GO

/******* STORED PROCEDURES *******/

--------------------------------------------------------------------------------
--   DESCRIPTION: Collection by machine and date
-- 
--        AUTHOR: Jos� Mart�nez
-- 
-- CREATION DATE: 09-SEP-2014
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 09-SEP-2014 JML    First release.
-- 09-SEP-2014 JML    PR_collection_by_machine_and_date
-------------------------------------------------------------------------------- 

IF OBJECT_ID (N'dbo.PR_collection_by_machine_and_date', N'P') IS NOT NULL
    DROP PROCEDURE dbo.PR_collection_by_machine_and_date;                 
GO    

CREATE PROCEDURE [dbo].[PR_collection_by_machine_and_date] 
 ( @pFromDt DATETIME   
  ,@pToDt DATETIME    
  ,@pClosingTime INTEGER
  ,@pMaskStatus INTEGER
  ,@pStatusPaid INTEGER
  ,@pTerminalWhere NVARCHAR(MAX) 
  ,@pBillDetail BIT
)
AS
BEGIN

  -- DECLARATIONS
   DECLARE @Sql AS VARCHAR(MAX)
   DECLARE @nRows AS INT                              
   DECLARE @index AS INT                              
   DECLARE @tDays AS TABLE(NumDay INT)                       
   DECLARE @Columns AS VARCHAR(MAX)
   DECLARE @ColumnChk AS VARCHAR(MAX)
   DECLARE @p2007Opening AS VARCHAR(MAX)

   SET @p2007Opening  = CAST('2007-01-01T00:00:00' AS DATETIME);
   SET @p2007Opening  = DATEADD(HOUR, @pClosingTime, @p2007Opening)

   SET @nRows  = DATEDIFF(DAY, @pFromDt, @pToDt)+1 
   SET @index = 0                                  

   WHILE @index < @nRows                           
   BEGIN	                                          
     INSERT INTO @tDays VALUES(@index)	                
     SET @index = @index + 1                       
   END 
   
    SET @Columns = ''
    SET @ColumnChk = ''

   SELECT NumDay INTO #TempTable_Days FROM @tDays 
   
   SELECT @Columns = COALESCE(@Columns + '[' + CAST(CGC_DENOMINATION AS NVARCHAR(20)) + '],', '')
     FROM (SELECT   DISTINCT CGC_DENOMINATION 
             FROM   CAGE_CURRENCIES 
            WHERE   CGC_ISO_CODE IN (SELECT   GP_KEY_VALUE 
                                       FROM   GENERAL_PARAMS 
                                      WHERE   GP_GROUP_KEY = 'RegionalOptions' 
                                        AND   GP_SUBJECT_KEY = 'CurrencyISOCode')
              AND   CGC_DENOMINATION >= 1 
              AND   CGC_ALLOWED = 1
                    UNION  
           SELECT   DISTINCT MCD_FACE_VALUE 
             FROM   MONEY_COLLECTION_DETAILS 
            INNER   JOIN MONEY_COLLECTIONS ON MCD_COLLECTION_ID = MC_COLLECTION_ID
            WHERE   MC_COLLECTION_DATETIME >= @pFromDt
              AND   MC_COLLECTION_DATETIME <  @pToDt
              AND   MC_TERMINAL_ID IS NOT NULL 
              AND   MCD_FACE_VALUE >= 1
          ) AS DTM
  ORDER BY CGC_DENOMINATION

  SET @ColumnChk = REPLACE( @Columns, ',', ' IS NOT NULL OR ')

  SET @Columns = LEFT(@Columns,LEN(@Columns)-1)
  SET @ColumnChk = LEFT(@ColumnChk,LEN(@ColumnChk)-3)
     
  SET @Sql = '
   SELECT   TE_PROVIDER_ID 
          , TE_TERMINAL_ID 
          , ISNULL(TE_MULTI_DENOMINATION, ''--'') AS DENOMINATION 
          , ORDER_DATE
          , DATEADD(DAY, 1, ORDER_DATE) AS ORDER_DATE_FIN
          , TICKET_IN_COUNT
          , TI_IN_AMOUNT_RE
          , TI_IN_AMOUNT_NO_RE
          , (ISNULL(TI_IN_AMOUNT_RE,0) + ISNULL(TI_IN_AMOUNT_NO_RE, 0)) AS TI_IN_AMOUNT
          , TICKET_OUT_COUNT
          , TI_OUT_AMOUNT_RE
          , TI_OUT_AMOUNT_NO_RE
          , (ISNULL(TI_OUT_AMOUNT_RE, 0) + ISNULL(TI_OUT_AMOUNT_NO_RE, 0)) AS TI_OUT_AMOUNT
          , (ISNULL(TI_OUT_AMOUNT_RE, 0) - ISNULL(TI_IN_AMOUNT_RE, 0)) AS NET_TITO
          , HAND_PAYS.MANUAL
          , HAND_PAYS.CREDIT_CANCEL
          , HAND_PAYS.JACKPOT_DE_SALA
          , HAND_PAYS.PROGRESIVES
          , HAND_PAYS.NO_PROGRESIVES
          , (ISNULL(HAND_PAYS.MANUAL, 0) + ISNULL(HAND_PAYS.CREDIT_CANCEL, 0) + ISNULL(HAND_PAYS.JACKPOT_DE_SALA, 0) + ISNULL(HAND_PAYS.NO_PROGRESIVES, 0) + ISNULL(PROVISIONS.PROGRESIVE_PROVISIONS, 0)) AS HAND_PAYS_TOTAL
          , PROVISIONS.PROGRESIVE_PROVISIONS
          , (ISNULL(TI_OUT_AMOUNT_RE, 0) - ISNULL(TI_IN_AMOUNT_RE, 0) +
             ISNULL(HAND_PAYS.MANUAL, 0) + ISNULL(HAND_PAYS.CREDIT_CANCEL, 0) + ISNULL(HAND_PAYS.JACKPOT_DE_SALA, 0) + ISNULL(HAND_PAYS.NO_PROGRESIVES, 0) + ISNULL(PROVISIONS.PROGRESIVE_PROVISIONS, 0)) 
              AS TOTAL_WIN_WITHOUT_BILLS
          , 0 AS GAP_BILL
          , BILLS.*
     FROM   TERMINALS 
 LEFT JOIN (SELECT   DATEADD(DAY, NumDay, CAST(''' + CAST(@pFromDt AS VARCHAR(50)) + ''' AS DATETIME)) AS ORDER_DATE FROM #TempTable_Days ) DIA ON ORDER_DATE <= GETDATE() 

 LEFT JOIN (SELECT   MC_TERMINAL_ID AS MC_TERMINAL_ID_T
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), CGS_WORKING_DAY)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS MC_COLLECTION_DATETIME_T
                   , SUM(MC_EXPECTED_TICKET_COUNT) AS TICKET_IN_COUNT
                   , SUM(ISNULL(MC_EXPECTED_RE_TICKET_AMOUNT, 0)+ISNULL(MC_EXPECTED_PROMO_RE_TICKET_AMOUNT, 0)) AS TI_IN_AMOUNT_RE
                   , SUM(ISNULL(MC_EXPECTED_PROMO_NR_TICKET_AMOUNT, 0)) AS TI_IN_AMOUNT_NO_RE
              FROM   MONEY_COLLECTIONS
        INNER JOIN   CAGE_MOVEMENTS ON CGM_MC_COLLECTION_ID = MC_COLLECTION_ID
        INNER JOIN   CAGE_SESSIONS ON CGS_CAGE_SESSION_ID = CGM_CAGE_SESSION_ID
             WHERE   CGS_WORKING_DAY >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
               AND   CGS_WORKING_DAY <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
               AND   MC_TERMINAL_ID IS NOT NULL
             GROUP   BY MC_TERMINAL_ID
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), CGS_WORKING_DAY)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) 
           ) TICKETS_IN ON TE_TERMINAL_ID = MC_TERMINAL_ID_T AND MC_COLLECTION_DATETIME_T = DIA.ORDER_DATE 
LEFT JOIN ( SELECT   TI_CREATED_TERMINAL_ID
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), TI_CREATED_DATETIME)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS TI_CREATED_DATETIME
                   , COUNT(1) AS TICKET_OUT_COUNT
                   , SUM(CASE WHEN TI_TYPE_ID = 2 THEN 0 ELSE TI_AMOUNT END) AS TI_OUT_AMOUNT_RE
                   , SUM(CASE WHEN TI_TYPE_ID = 2 THEN TI_AMOUNT ELSE 0 END) AS TI_OUT_AMOUNT_NO_RE
              FROM   TICKETS 
             WHERE   TI_CREATED_DATETIME >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
               AND   TI_CREATED_DATETIME <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
               AND   TI_CREATED_TERMINAL_TYPE = 1     -- TITO_TERMINAL_TYPE.TERMINAL = 1
               AND   TI_TYPE_ID IN (0,1,2,5) -- TITO_TICKET_TYPE
                                                --CASHABLE = 0,
                                                --PROMO_REDEEM = 1,
                                                --PROMO_NONREDEEM = 2,  // only playable
                                                --HANDPAY = 3,
                                                --JACKPOT = 4,
                                                --OFFLINE = 5
             GROUP   BY TI_CREATED_TERMINAL_ID
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), TI_CREATED_DATETIME)/24, CAST(''' + @p2007Opening + ''' AS DATETIME))
          ) TICKETS_OUT ON TE_TERMINAL_ID = TI_CREATED_TERMINAL_ID AND TI_CREATED_DATETIME = DIA.ORDER_DATE 
 LEFT JOIN (SELECT   HP_TERMINAL_ID 
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), HP_STATUS_CHANGED)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS HP_DATETIME
                   , SUM(CASE WHEN (HP_TYPE IN (10, 1000, 1001, 1010)) THEN ISNULL(HP_AMOUNT, 0) ELSE 0 END) AS MANUAL 
                   , SUM(CASE WHEN (HP_TYPE IN ( 0 )) THEN ISNULL(HP_AMOUNT, 0) ELSE 0 END) AS CREDIT_CANCEL 
                   , SUM(CASE WHEN (HP_TYPE IN ( 20)) THEN ISNULL(HP_AMOUNT, 0) ELSE 0 END) AS JACKPOT_DE_SALA 
                   , SUM(CASE WHEN (HP_TYPE IN ( 1 ) AND (HP_LEVEL>=1 AND HP_LEVEL<=32)) THEN ISNULL(HP_AMOUNT, 0) ELSE 0 END) AS PROGRESIVES 
                   , SUM(CASE WHEN (HP_TYPE IN ( 1 ) AND (HP_LEVEL <1 OR  HP_LEVEL >32)) THEN ISNULL(HP_AMOUNT, 0) ELSE 0 END) AS NO_PROGRESIVES 
              FROM   HANDPAYS WITH (INDEX (IX_hp_status_changed))
             WHERE   HP_STATUS & ' + CAST(@pMaskStatus AS VARCHAR(10)) + ' = ' + CAST(@pStatusPaid AS VARCHAR(10)) + 
            '  AND   HP_STATUS_CHANGED >= CAST(''' + CAST(@pFromDt AS VARCHAR(50)) + ''' AS DATETIME) 
               AND   HP_STATUS_CHANGED < CAST(''' + CAST(@pToDt AS VARCHAR(50)) + ''' AS DATETIME) 
             GROUP   BY HP_TERMINAL_ID 
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), HP_STATUS_CHANGED)/24, CAST(''' + @p2007Opening + ''' AS DATETIME))
                   ) HAND_PAYS ON HP_TERMINAL_ID = TE_TERMINAL_ID AND HP_DATETIME = DIA.ORDER_DATE 
 LEFT JOIN (SELECT   PPT_TERMINAL_ID 
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), PGP_HOUR_TO)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS PGP_HOUR_TO
                   , SUM(ISNULL(PPT_AMOUNT, 0)) AS PROGRESIVE_PROVISIONS 
             FROM    PROGRESSIVES_PROVISIONS_TERMINALS 
        LEFT JOIN    PROGRESSIVES_PROVISIONS ON PGP_PROVISION_ID = PPT_PROVISION_ID 
                                            AND PGP_PROGRESSIVE_ID = PPT_PROGRESSIVE_ID 
                                            AND PGP_HOUR_TO >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
                                            AND PGP_HOUR_TO <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
             GROUP   BY PPT_TERMINAL_ID 
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), PGP_HOUR_TO)/24, CAST(''' + @p2007Opening + ''' AS DATETIME))
                   ) PROVISIONS ON PPT_TERMINAL_ID = TE_TERMINAL_ID AND PGP_HOUR_TO = DIA.ORDER_DATE '
IF @pBillDetail = 1
BEGIN
  SET @Sql =  @Sql +
'LEFT JOIN (SELECT   *  
              FROM ( SELECT   MC_TERMINAL_ID
                            , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), CGS_WORKING_DAY)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS MC_COLLECTION_DATETIME
                            , MCD_FACE_VALUE
                            , (MCD_NUM_COLLECTED * MCD_FACE_VALUE) AS COLLECTED
                       FROM   MONEY_COLLECTION_DETAILS 
                 INNER JOIN   MONEY_COLLECTIONS ON MCD_COLLECTION_ID = MC_COLLECTION_ID
                 INNER JOIN   CAGE_MOVEMENTS ON CGM_MC_COLLECTION_ID = MC_COLLECTION_ID
                 INNER JOIN   CAGE_SESSIONS ON CGS_CAGE_SESSION_ID = CGM_CAGE_SESSION_ID
                      WHERE   CGS_WORKING_DAY >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
                        AND   CGS_WORKING_DAY <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
                        AND   MC_TERMINAL_ID IS NOT NULL
                   ) PIV
             PIVOT (  SUM(COLLECTED) FOR MCD_FACE_VALUE IN ('+ @Columns  + ')) AS CHILD
            ) BILLS ON TE_TERMINAL_ID = MC_TERMINAL_ID AND MC_COLLECTION_DATETIME = DIA.ORDER_DATE '
END
ELSE
BEGIN
  SET @Sql =  @Sql +
'LEFT JOIN (SELECT   MC_TERMINAL_ID
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), CGS_WORKING_DAY)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS MC_COLLECTION_DATETIME
                   , 0 AS MCD_FACE_VALUE
                   , SUM(ISNULL(MC_COLLECTED_BILL_AMOUNT, 0)) AS COLLECTED
              FROM   MONEY_COLLECTIONS
        INNER JOIN   CAGE_MOVEMENTS ON CGM_MC_COLLECTION_ID = MC_COLLECTION_ID
        INNER JOIN   CAGE_SESSIONS ON CGS_CAGE_SESSION_ID = CGM_CAGE_SESSION_ID
             WHERE   CGS_WORKING_DAY >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
               AND   CGS_WORKING_DAY <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
               AND   MC_TERMINAL_ID IS NOT NULL
             GROUP   BY MC_TERMINAL_ID
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), CGS_WORKING_DAY)/24, CAST(''' + @p2007Opening + ''' AS DATETIME))
          ) BILLS ON TE_TERMINAL_ID = MC_TERMINAL_ID AND MC_COLLECTION_DATETIME = DIA.ORDER_DATE '
END

SET @Sql =  @Sql + 'WHERE ( ' 

IF @pBillDetail = 1
BEGIN
  SET @Sql =  @Sql + @ColumnChk 
END
ELSE
BEGIN
  SET @Sql =  @Sql + ' BILLS.COLLECTED IS NOT NULL ' 
END

SET @Sql =  @Sql +
  ' OR TICKET_IN_COUNT IS NOT NULL
    OR TI_IN_AMOUNT_RE IS NOT NULL
    OR TI_IN_AMOUNT_NO_RE IS NOT NULL
    OR TICKET_OUT_COUNT IS NOT NULL
    OR TI_OUT_AMOUNT_RE IS NOT NULL
    OR TI_OUT_AMOUNT_NO_RE IS NOT NULL
    OR HAND_PAYS.MANUAL IS NOT NULL
    OR HAND_PAYS.CREDIT_CANCEL IS NOT NULL
    OR HAND_PAYS.JACKPOT_DE_SALA IS NOT NULL
    OR HAND_PAYS.PROGRESIVES IS NOT NULL
    OR HAND_PAYS.NO_PROGRESIVES IS NOT NULL
    OR PROVISIONS.PROGRESIVE_PROVISIONS IS NOT NULL
    )' + CAST(@pTerminalWhere AS Varchar(max)) +
' ORDER   BY TE_PROVIDER_ID 
        , TE_TERMINAL_ID 
        , TE_NAME 
        , ISNULL(TE_MULTI_DENOMINATION, ''--'')
        , ORDER_DATE '

 -- PRINT @Sql

  EXECUTE (@Sql)

  DROP TABLE #TempTable_Days

END -- END PROCEDURE
GO

-- PERMISSIONS

GRANT EXECUTE ON [dbo].[PR_collection_by_machine_and_date] TO [wggui] WITH GRANT OPTION
GO

--------------------------------------------------------------------------------
-- Copyright � 2013 Win Systems International
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: CashierMovementsGrouped_Read.sql
-- 
--   DESCRIPTION: Read historied or not historified cashier sessions
-- 
--        AUTHOR: JVV
-- 
-- CREATION DATE: 21-NOV-2013
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 21-NOV-2013 JVV    First release.
-- 11-FEB-2014 DLL    Fixed Bug WIG-613: Cashier session detail all with 0 value
-- 10-NOV-2014 DLL    Fixed Bug WIG-1651: Date filter without minutes. 
--------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[CashierMovementsGrouped_Read]        
         @pCashierSessionId NVARCHAR(MAX)
        ,@pDateFrom DATETIME
        ,@pDateTo DATETIME       
  AS
BEGIN 
DECLARE @_sql NVARCHAR(MAX)

	IF (@pCashierSessionId IS NOT NULL AND @pDateFrom IS NULL AND @pDateTo IS NULL)  --ByID
	BEGIN	
	SET @_sql =	'IF EXISTS (SELECT TOP 1 CM_SESSION_ID FROM cashier_movements_grouped_by_session_id WHERE CM_SESSION_ID IN ( ' + @pCashierSessionId + '))  
				BEGIN
	
						SELECT
							 CM_SESSION_ID
							,CM_TYPE
							,CM_SUB_TYPE
							,CM_TYPE_COUNT
							,CM_CURRENCY_ISO_CODE
							,CM_CURRENCY_DENOMINATION
							,CM_SUB_AMOUNT
							,CM_ADD_AMOUNT
							,CM_AUX_AMOUNT
							,CM_INITIAL_BALANCE
							,CM_FINAL_BALANCE
						FROM cashier_movements_grouped_by_session_id WHERE CM_SESSION_ID IN (' + @pCashierSessionId + ')
				END
				ELSE
				BEGIN 
					
					EXEC CashierMovementsGrouped_Select ''' + REPLACE(@pCashierSessionId,'''','''''') + ''',NULL ,NULL
				END	'
				
	  EXEC sp_executesql @_sql
				
	END
	ELSE
	BEGIN	  
		IF DATEPART(MINUTE,@pDateFrom) = 0 AND DATEPART(MINUTE,@pDateTo) = 0 AND (DATEDIFF(HOUR,@pDateFrom,@pDateTo) = (SELECT COUNT(DISTINCT(CM_DATE)) FROM cashier_movements_grouped_by_hour WHERE CM_DATE >= @pDateFrom AND CM_DATE < @pDateTo)) AND @pCashierSessionId IS NULL
		BEGIN
			SET @_sql ='
						SELECT
							 NULL
							,CM_TYPE
							,CM_SUB_TYPE
							,CM_TYPE_COUNT							
							,CM_CURRENCY_ISO_CODE
							,CM_CURRENCY_DENOMINATION
							,CM_SUB_AMOUNT
							,CM_ADD_AMOUNT
							,CM_AUX_AMOUNT
							,CM_INITIAL_BALANCE
							,CM_FINAL_BALANCE
						FROM cashier_movements_grouped_by_hour WHERE CM_DATE >= ''' +CONVERT(VARCHAR, @pDateFrom, 21) + '''  AND CM_DATE < ''' + CONVERT(VARCHAR, @pDateTo, 21) +''''
						
	      EXEC sp_executesql @_sql
				
		END
		ELSE
		BEGIN
			  EXEC CashierMovementsGrouped_Select @pCashierSessionId, @pDateFrom, @pDateTo
		END
	END

	
END --CashierMovementsGrouped_Read
GO

ALTER PROCEDURE [dbo].[SP_Cashier_Sessions_Report] 
 (  @pDateFrom DateTime
  , @pDateTo DateTime
  , @pUseCashierMovements as Bit
  , @pShowSystemSessions as Bit
  , @pSystemUsersTypes as VarChar(4096)
  , @pSessionStatus as VarChar(4096)
  , @pUserId as Integer
  , @pCashierId as Integer
  , @pCurrencyISOCode as VarChar(3)
)
AS
BEGIN

-- Members --
DECLARE @_delimiter AS CHAR(1)
SET @_delimiter = ','
DECLARE @pSystemUsersTypesTable AS TABLE (SUT_ID INT, SUT_VALUE VARCHAR(50))
DECLARE @pSessionsStatusTable AS TABLE (SUT_ID INT, SUT_VALUE VARCHAR(50))
DECLARE @_table_sessions_ AS TABLE ( sess_id                      BIGINT 
                                                           , sess_name                    NVARCHAR(50) 
                                                           , sess_user_id                 INT      
                                                          , sess_full_name               NVARCHAR(50) 
                                                           , sess_opening_date            DATETIME 
                                                           , sess_closing_date            DATETIME  
                                                           , sess_cashier_id              INT 
                                                           , sess_ct_name                 NVARCHAR(50) 
                                                           , sess_status                  INT 
                                                           , sess_cs_limit_sale           MONEY
                                                          , sess_mb_limit_sale           MONEY
                                                          , sess_collected_amount        MONEY
                                                          ) 
                                                           
-- SYSTEM USER TYPES --
IF @pShowSystemSessions = 0
BEGIN
INSERT INTO @pSystemUsersTypesTable SELECT * FROM dbo.SplitStringIntoTable(@pSystemUsersTypes, @_delimiter, DEFAULT)
END

-- SESSIONS STATUS
INSERT INTO @pSessionsStatusTable SELECT * FROM dbo.SplitStringIntoTable(@pSessionStatus, @_delimiter, DEFAULT)

IF @pUseCashierMovements = 0
   BEGIN -- Filter Dates on cashier session opening date  

      -- SESSIONS --
      INSERT   INTO @_table_sessions_
      SELECT   DISTINCT(CS_SESSION_ID) AS SESS_ID
             , CS_NAME AS SESS_NAME
             , CS_USER_ID AS SESS_USER_ID
             , GU_FULL_NAME AS SESS_FULL_NAME
             , CS_OPENING_DATE AS SESS_OPENING_DATE
             , CS_CLOSING_DATE AS SESS_CLOSING_DATE
             , CS_CASHIER_ID AS SESS_CASHIER_ID
             , CT_NAME AS SESS_CT_NAME
             , CS_STATUS AS SESS_STATUS
             , CS_SALES_LIMIT AS SESS_CS_LIMIT_SALE
             , CS_MB_SALES_LIMIT AS SESS_MB_LIMIT_SALE
             , CS_COLLECTED_AMOUNT AS SESS_COLLECTED_AMOUNT
        FROM   CASHIER_SESSIONS --WITH (INDEX (IX_cs_opening_date)) /* TODO: REVIEW INDEXES */
       INNER   JOIN GUI_USERS         ON CS_USER_ID    = GU_USER_ID
       INNER   JOIN CASHIER_TERMINALS ON CS_CASHIER_ID = CT_CASHIER_ID
       WHERE   (CS_OPENING_DATE >= @pDateFrom AND CS_OPENING_DATE <= @pDateTo)
          --   usuarios sistema s/n & user 
         AND   (      GU_USER_TYPE NOT IN ( SELECT SUT_VALUE FROM @pSystemUsersTypesTable )
                AND   CS_USER_ID = (CASE WHEN @pUserId IS NULL THEN CS_USER_ID ELSE @pUserId END) )
          --   cashier id
         AND   CS_CASHIER_ID = (CASE WHEN @pCashierId IS NULL THEN CS_CASHIER_ID ELSE @pCashierId END) 
          --   session status
         AND   CS_STATUS IN ( SELECT SUT_VALUE FROM @pSessionsStatusTable )
       ORDER   BY CS_OPENING_DATE DESC  

      -- MOVEMENTS
      -- By Movements in Historical Data --    
      SELECT   CM_SESSION_ID
             , CM_TYPE
             , CM_SUB_TYPE
             , ISNULL(CM_CURRENCY_ISO_CODE, @pCurrencyISOCode) AS CM_CURRENCY_ISO_CODE
             , CM_CURRENCY_DENOMINATION
             , CM_TYPE_COUNT
             , CM_SUB_AMOUNT
             , CM_ADD_AMOUNT
             , CM_AUX_AMOUNT
             , CM_INITIAL_BALANCE
             , CM_FINAL_BALANCE
        FROM   CASHIER_MOVEMENTS_GROUPED_BY_SESSION_ID
       WHERE   CM_SESSION_ID IN (SELECT SESS_ID FROM @_table_sessions_)

   END
ELSE
   BEGIN -- Filter Dates on cashier movements date
     
      -- MOVEMENTS
      -- By Cashier_Movements --   
      SELECT * 
      INTO #CASHIER_MOVEMENTS
      FROM ( 
            SELECT   CM_DATE
                   , CM_SESSION_ID
                   , CM_TYPE
                   , 0 as CM_SUB_TYPE
                   , ISNULL(CM_CURRENCY_ISO_CODE, @pCurrencyISOCode) AS CM_CURRENCY_ISO_CODE
                   , CM_CURRENCY_DENOMINATION
                   , 1 as CM_TYPE_COUNT
                   , CM_SUB_AMOUNT
                   , CM_ADD_AMOUNT
                   , CM_AUX_AMOUNT
                   , CM_INITIAL_BALANCE
                   , CM_FINAL_BALANCE
              FROM   CASHIER_MOVEMENTS 
             WHERE   CM_DATE >= @pDateFrom AND CM_DATE < @pDateTo
             UNION   ALL
            SELECT   MBM_DATETIME AS CM_DATE
                   , MBM_CASHIER_SESSION_ID AS CM_SESSION_ID
                   , MBM_TYPE AS CM_TYPE
                   , 1 AS CM_SUB_TYPE
                   , @pCurrencyISOCode AS CM_CURRENCY_ISO_CODE
                   , 0 AS CM_CURRENCY_DENOMINATION
                   , 1 AS CM_TYPE_COUNT
                   , ISNULL(MBM_SUB_AMOUNT, 0) AS CM_SUB_AMOUNT
                   , ISNULL(MBM_ADD_AMOUNT, 0) AS CM_ADD_AMOUNT
                   , 0 AS CM_AUX_AMOUNT
                   , 0 AS CM_INITIAL_BALANCE
                   , 0 AS CM_FINAL_BALANCE
              FROM   MB_MOVEMENTS 
             WHERE   MBM_DATETIME >= @pDateFrom AND MBM_DATETIME < @pDateTo
       ) AS MOVEMENTS
       
      CREATE NONCLUSTERED INDEX IX_CM_SESSION_ID ON #CASHIER_MOVEMENTS(CM_SESSION_ID)

      -- SESSIONS --
      INSERT   INTO @_table_sessions_
      SELECT   DISTINCT(CS_SESSION_ID) AS SESS_ID
             , CS_NAME AS SESS_NAME
             , CS_USER_ID AS SESS_USER_ID
             , GU_FULL_NAME AS SESS_FULL_NAME
             , CS_OPENING_DATE AS SESS_OPENING_DATE
             , CS_CLOSING_DATE AS SESS_CLOSING_DATE
             , CS_CASHIER_ID AS SESS_CASHIER_ID
             , CT_NAME AS SESS_CT_NAME
             , CS_STATUS AS SESS_STATUS
             , CS_SALES_LIMIT AS SESS_CS_LIMIT_SALE
             , CS_MB_SALES_LIMIT AS SESS_MB_LIMIT_SALE
             , CS_COLLECTED_AMOUNT AS SESS_COLLECTED_AMOUNT
        FROM   CASHIER_SESSIONS --WITH (INDEX (IX_cs_opening_date)) /* TODO: REVIEW INDEXES */
       INNER   JOIN GUI_USERS         ON CS_USER_ID    = GU_USER_ID
       INNER   JOIN CASHIER_TERMINALS ON CS_CASHIER_ID = CT_CASHIER_ID
       WHERE   CS_SESSION_ID IN (SELECT CM_SESSION_ID FROM #CASHIER_MOVEMENTS)
          --   usuarios sistema s/n & user 
         AND   (      GU_USER_TYPE NOT IN ( SELECT SUT_VALUE FROM @pSystemUsersTypesTable )
                AND   CS_USER_ID = (CASE WHEN @pUserId IS NULL THEN CS_USER_ID ELSE @pUserId END) )
          --   cashier id
         AND   CS_CASHIER_ID = (CASE WHEN @pCashierId IS NULL THEN CS_CASHIER_ID ELSE @pCashierId END) 
          --   session status
         AND   CS_STATUS IN ( SELECT SUT_VALUE FROM @pSessionsStatusTable )         
       ORDER   BY CS_OPENING_DATE DESC  
       
       SELECT * FROM #CASHIER_MOVEMENTS INNER JOIN @_table_sessions_ ON SESS_ID = CM_SESSION_ID
       
       DROP TABLE #CASHIER_MOVEMENTS      

   END 

-- SESSIONS with CAGE INFO
SELECT   SESS_ID
       , SESS_NAME
       , SESS_USER_ID
       , SESS_FULL_NAME
       , SESS_OPENING_DATE
       , SESS_CLOSING_DATE
       , SESS_CASHIER_ID
       , SESS_CT_NAME
       , SESS_STATUS
       , SESS_CS_LIMIT_SALE
       , SESS_MB_LIMIT_SALE
       , SESS_COLLECTED_AMOUNT 
       , X.CGS_CAGE_SESSION_ID AS CGS_CAGE_SESSION_ID
       , X.CGS_SESSION_NAME AS CGS_SESSION_NAME 
  FROM   @_table_sessions_
  LEFT   JOIN ( SELECT   CGS_SESSION_NAME
                       , CGM_CASHIER_SESSION_ID
                       , CGS_CAGE_SESSION_ID 
                  FROM   CAGE_MOVEMENTS AS CM
                 INNER   JOIN CAGE_SESSIONS ON CM.CGM_CAGE_SESSION_ID = CGS_CAGE_SESSION_ID
                 WHERE   CM.CGM_MOVEMENT_ID = (SELECT    TOP 1 CGM.CGM_MOVEMENT_ID
                                                 FROM    CAGE_MOVEMENTS AS CGM 
                                                WHERE    CGM.CGM_CASHIER_SESSION_ID = CM.CGM_CASHIER_SESSION_ID)
               ) AS X ON X.CGM_CASHIER_SESSION_ID = [@_table_sessions_].SESS_ID
               
-- SESSIONS CURRENCIES INFORMATION
IF @pUseCashierMovements = 0
    SELECT   CSC_SESSION_ID
           , CSC_ISO_CODE
           , CSC_TYPE
           , CSC_BALANCE
           , CSC_COLLECTED 
      FROM   CASHIER_SESSIONS_BY_CURRENCY 
     INNER JOIN @_table_sessions_ ON SESS_ID = CSC_SESSION_ID

END -- End Procedure
GO
 
--------------------------------------------------------------------------------
-- Copyright � 2014 Win Systems International
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: ReportShortfallExcess.sql
-- 
--   DESCRIPTION: Procedure for Cash ShortFall and Excess Report 
-- 
--        AUTHOR: Fernando Jim�nez
-- 
-- CREATION DATE: 04-OCT-2014
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 04-OCT-2014 FJC    First release.
-- 21-NOV-2014 FJC    Changes in Mobile bank query.
--------------------------------------------------------------------------------


--------------------------------------------------------------------------------
-- PURPOSE: Selects cash shortfall and excess report.
-- 
--  PARAMS:
--      - INPUT:
--   @pDateFrom	   DATETIME     
--   @pDateTo		   DATETIME      
--
--      - OUTPUT:
--
-- RETURNS:
--                  
--
--   NOTES:
--

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ReportShortfallExcess]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[ReportShortfallExcess]

GO

CREATE PROCEDURE [dbo].[ReportShortfallExcess]
  @pDateFrom DATETIME,  
  @pDateTo   DATETIME
    
AS 
BEGIN
    --GUI USERS
    SELECT   GUI_USERS.GU_USER_ID        as  USERID 
           , 0                           as  TYPE_USER
           , GUI_USERS.GU_USERNAME       as  USERNAME 
           , GUI_USERS.GU_FULL_NAME      as  FULLNAME 
           , GUI_USERS.GU_EMPLOYEE_CODE  as  EMPLOYEECODE 
           , X.CM_CURRENCY_ISO_CODE      as  CURRENCY 
           , X.FALTANTE                  as  FALTANTE 
           , X.SOBRANTE                  as  SOBRANTE 
           , (X.SOBRANTE - X.FALTANTE)   as  DIFERENCIA
      FROM
      (
        SELECT CM_USER_ID
               , CM_CURRENCY_ISO_CODE
               , SUM (CASE WHEN CM_TYPE = 158 THEN CM_ADD_AMOUNT ELSE 0 END) as FALTANTE
               , SUM (CASE WHEN CM_TYPE = 159 THEN CM_ADD_AMOUNT ELSE 0 END) as SOBRANTE
          FROM   CASHIER_MOVEMENTS  WITH (INDEX (IX_CM_DATE_TYPE))
         WHERE   CM_DATE    >= @pDateFrom  
           AND   CM_DATE    <= @pDateTo
           AND   CM_TYPE    IN (158, 159) 
      GROUP BY   CM_USER_ID
               , CM_CURRENCY_ISO_CODE
      ) X
           
    INNER JOIN   GUI_USERS
            ON   GU_USER_ID = X.CM_USER_ID
           AND  (X.FALTANTE<>0 OR X.SOBRANTE<>0)

    UNION ALL
    
    --BANK MOBILE USERS (this kind of users have national currency only)
    SELECT   MOBILE_BANKS.MB_ACCOUNT_ID                       as  USERID 
           , 1                                                as  TYPE_USER 
           , 'MB-' + CAST(MOBILE_BANKS.mb_account_id as CHAR) as  USERNAME
           , MOBILE_BANKS.MB_HOLDER_NAME                      as  FULLNAME
           , ''                                               as  EMPLOYEECODE
           , ''                                               as  CURRENCY
           , X2.FALTANTE                                      as  FALTANTE
           , X2.SOBRANTE                                      as  SOBRANTE 
           , (X2.SOBRANTE - X2.FALTANTE)                      as  DIFERENCIA
     FROM
     (
          
        SELECT   MBM_MB_ID
                 , SUM(CASE WHEN  MBM_TYPE IN (8,11)  THEN MBM_SUB_AMOUNT ELSE 0 END) AS FALTANTE
                 , SUM(CASE WHEN  MBM_TYPE = 9        THEN MBM_ADD_AMOUNT ELSE 0 END) AS SOBRANTE
            FROM   MB_MOVEMENTS MB1 WITH (INDEX (IX_MBM_DATETIME_TYPE))    
           WHERE   
                  (MBM_DATETIME  >= @pDateFrom  
             AND   MBM_DATETIME  <= @pDateTo)
             AND  ( ( MBM_TYPE IN (8,9) ) 
              OR    ( MBM_TYPE IN (11) 
                     
                     AND  NOT EXISTS 
                        (SELECT   MB2.MBM_MB_ID
                           FROM   MB_MOVEMENTS MB2 
                          WHERE   MB2.MBM_CASHIER_SESSION_ID = MB1.MBM_CASHIER_SESSION_ID 
                            AND   MB2.MBM_DATETIME > MB1.MBM_DATETIME 
                            AND   MB2.MBM_TYPE IN (3) ) ) ) 
        GROUP BY   MBM_MB_ID
        
     ) X2
    
      INNER JOIN   MOBILE_BANKS
              ON   X2.MBM_MB_ID = MOBILE_BANKS.MB_ACCOUNT_ID
             AND   (X2.FALTANTE<>0 OR X2.SOBRANTE<>0)
      
        ORDER BY   USERNAME
END
GO

GRANT EXECUTE ON [dbo].[ReportShortfallExcess] TO [wggui] WITH GRANT OPTION
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CageGetGlobalReportData]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[CageGetGlobalReportData]
GO

CREATE PROCEDURE [dbo].[CageGetGlobalReportData]
        @pCageSessionIds NVARCHAR(MAX) = NULL
AS
BEGIN

  DECLARE @NationalIsoCode NVARCHAR(3)
  DECLARE @CageOperationType_FromTerminal INT
  DECLARE @TicketsId INT
  DECLARE @BillsId INT
  DECLARE @Currencies AS NVARCHAR(200)

  SELECT @NationalIsoCode = GP_KEY_VALUE 
    FROM GENERAL_PARAMS 
   WHERE GP_GROUP_KEY = 'RegionalOptions' 
     AND GP_SUBJECT_KEY = 'CurrencyISOCode'

  SELECT @Currencies = GP_KEY_VALUE 
    FROM GENERAL_PARAMS 
   WHERE GP_GROUP_KEY = 'RegionalOptions' 
     AND GP_SUBJECT_KEY = 'CurrenciesAccepted'

  -- Split currencies ISO codes
  SELECT SST_VALUE AS CURRENCY_ISO_CODE INTO #TMP_CURRENCIES_ISO_CODES FROM [SplitStringIntoTable] (@Currencies, ';', 1)
               
  SET @CageOperationType_FromTerminal = 104
  SET @TicketsId = -200
  SET @BillsId = 0
      
  -- TABLE[0]: Get cage stock
  SELECT CGS_ISO_CODE                                          
       , CGS_DENOMINATION                                      
       , CGS_QUANTITY 
       , CGS_CAGE_CURRENCY_TYPE
    FROM (SELECT CGS_ISO_CODE                                          
               , CGS_DENOMINATION                                      
               , CGS_QUANTITY                     
               , CASE WHEN CGS_DENOMINATION > 0 THEN ISNULL(CGS_CAGE_CURRENCY_TYPE,0) ELSE 99 END AS CGS_CAGE_CURRENCY_TYPE                     
            FROM CAGE_STOCK
                  
          UNION ALL
                  
          SELECT C.CH_ISO_CODE AS CGS_ISO_CODE
               , C.CH_DENOMINATION AS CGS_DENOMINATION
               , CST.CHSK_QUANTITY AS CGS_QUANTITY
               , 0 AS CGS_CAGE_CURRENCY_TYPE
            FROM CHIPS_STOCK AS CST
                 INNER JOIN CHIPS AS C ON CST.CHSK_CHIP_ID = C.CH_CHIP_ID
          ) AS _tbl 
    WHERE CGS_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)                                                          
 ORDER BY CGS_ISO_CODE, CGS_CAGE_CURRENCY_TYPE ASC, CGS_DENOMINATION DESC
        , CASE WHEN CGS_DENOMINATION >= 0 THEN CGS_DENOMINATION 
               ELSE CGS_DENOMINATION * (-100000) END

  IF @pCageSessionIds IS NOT NULL
  BEGIN
  
    -- Session IDs Split
    SELECT SST_VALUE AS CAGE_SESSION_ID INTO #TMP_CAGE_SESSIONS FROM [SplitStringIntoTable] (@pCageSessionIds,','  , 1)
    
    -- TABLE[1]: Get cage sessions information
    SELECT CSM.CSM_SOURCE_TARGET_ID AS CM_SOURCE_TARGET_ID
         , CSM.CSM_CONCEPT_ID AS CM_CONCEPT_ID
         , CSM.CSM_ISO_CODE AS CM_ISO_CODE
         , SUM(CSM.CSM_VALUE_IN) AS CM_VALUE_IN
         , SUM(CSM.CSM_VALUE_OUT) AS CM_VALUE_OUT
         , CST.CST_SOURCE_TARGET_NAME
         , CC.CC_DESCRIPTION
      FROM CAGE_SESSION_METERS AS CSM
           INNER JOIN CAGE_SOURCE_TARGET_CONCEPTS AS CSTC ON CSTC.CSTC_CONCEPT_ID = CSM.CSM_CONCEPT_ID AND CSTC.CSTC_SOURCE_TARGET_ID = CSM.CSM_SOURCE_TARGET_ID
           INNER JOIN CAGE_CONCEPTS AS CC ON CC.CC_CONCEPT_ID = CSTC.CSTC_CONCEPT_ID
           INNER JOIN CAGE_SOURCE_TARGET AS CST ON CST.CST_SOURCE_TARGET_ID = CSTC.CSTC_SOURCE_TARGET_ID
     WHERE CSM.CSM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
       AND CC.CC_SHOW_IN_REPORT = 1
       AND CC.CC_ENABLED = 1
       AND (CC.CC_TYPE = 1 OR CC_CONCEPT_ID IN (9, 10))
       AND CSTC.CSTC_ENABLED = 1
       AND CSM_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)
  GROUP BY CSM.CSM_SOURCE_TARGET_ID, CSM.CSM_CONCEPT_ID, CSM.CSM_ISO_CODE, CST.CST_SOURCE_TARGET_NAME, CC.CC_DESCRIPTION
  ORDER BY CSM.CSM_SOURCE_TARGET_ID
         , CASE WHEN CSM.CSM_CONCEPT_ID = 0 THEN '000' ELSE CC.CC_DESCRIPTION END
         , CASE WHEN CSM.CSM_ISO_CODE = @NationalIsoCode THEN '000' ELSE CSM.CSM_ISO_CODE END                    

    -- TABLE[2]: Get dynamic liabilities
    SELECT CC.CC_CONCEPT_ID AS LIABILITY_CONCEPT_ID
         , CC.CC_DESCRIPTION AS LIABILITY_NAME 
         , CSM.CSM_ISO_CODE AS LIABILITY_ISO_CODE
         , SUM(CSM.CSM_VALUE) AS LIABILITY_VALUE
      FROM CAGE_SESSION_METERS AS CSM
           INNER JOIN CAGE_CONCEPTS AS CC ON CC.CC_CONCEPT_ID = CSM.CSM_CONCEPT_ID
     WHERE CC.CC_ENABLED = 1
       AND CC.CC_SHOW_IN_REPORT = 1
       AND CC.CC_IS_PROVISION = 1
       AND CSM.CSM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
       AND CSM_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)
 GROUP BY CC.CC_CONCEPT_ID, CC.CC_DESCRIPTION, CSM.CSM_ISO_CODE
  ORDER BY CC.CC_CONCEPT_ID ASC
         , CASE WHEN CSM.CSM_ISO_CODE = @NationalIsoCode THEN '000' ELSE CSM.CSM_ISO_CODE END 
      
    -- TABLE[3]: Get session sumary
    SELECT CMD_ISO_CODE AS ISO_CODE
         , ISNULL(ORIGIN, 0) AS ORIGIN
         , SUM(CMD_DEPOSITS) AS DEPOSITS
         , SUM(CMD_WITHDRAWALS) AS WITHDRAWALS
      FROM (SELECT CMD_ISO_CODE
                 , ORIGIN
                 , SUM(CMD_DEPOSITS) CMD_DEPOSITS
                 , SUM(CMD_WITHDRAWALS) CMD_WITHDRAWALS      
              FROM (SELECT CASE WHEN CMD_QUANTITY >= 0 THEN @BillsId
                                ELSE CMD_QUANTITY END  AS ORIGIN
                         , CMD_ISO_CODE
                         , CMD_DENOMINATION
                         , CASE WHEN CGM_TYPE IN (100, 101, 103, 104) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY < 0 THEN CMD_DENOMINATION
                                                                                            ELSE CMD_QUANTITY * CMD_DENOMINATION END), 0)
                                ELSE 0 END AS CMD_DEPOSITS
                         , CASE WHEN CGM_TYPE IN (0, 1, 4, 5) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY < 0 THEN CMD_DENOMINATION             
                                                                                    ELSE CMD_QUANTITY * CMD_DENOMINATION END), 0)
                                ELSE 0 END AS CMD_WITHDRAWALS
                      FROM CAGE_MOVEMENTS 
                           INNER JOIN   CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          
                     WHERE CGM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
                       AND (CGM_STATUS IN (2, 3, 4, 8, 9, 10) 
                            OR (CGM_STATUS = 0 AND CGM_TYPE = 0))
                  GROUP BY CMD_ISO_CODE, CMD_DENOMINATION, CGM_TYPE, CMD_QUANTITY
                   ) AS T1        
          GROUP BY ORIGIN, CMD_ISO_CODE

            -- GET Terminal value-in amounts
              
              UNION ALL 
              
              SELECT @NationalIsoCode AS CMD_ISO_CODE
                    , @TicketsId AS ORIGIN
                    , MC_COLLECTED_TICKET_AMOUNT AS DEPOSITS
                    , 0 AS WITHDRAWS
              FROM MONEY_COLLECTIONS
                  INNER JOIN CAGE_MOVEMENTS ON CGM_MC_COLLECTION_ID = MC_COLLECTION_ID 
             WHERE CAGE_MOVEMENTS.CGM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
               AND CAGE_MOVEMENTS.CGM_TYPE = @CAGEOPERATIONTYPE_FROMTERMINAL
               AND (CGM_STATUS IN (2, 3, 4, 8, 9, 10) 
                    OR (CGM_STATUS = 0 AND CGM_TYPE = 0))           
             
              UNION ALL
              
                SELECT @NationalIsoCode AS CMD_ISO_CODE
                    , @BillsId AS ORIGIN
                    , MC_COLLECTED_BILL_AMOUNT  AS DEPOSITS
                    , 0 AS WITHDRAWS
              FROM MONEY_COLLECTIONS
                  INNER JOIN CAGE_MOVEMENTS ON CGM_MC_COLLECTION_ID = MC_COLLECTION_ID 
             WHERE CAGE_MOVEMENTS.CGM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
               AND CAGE_MOVEMENTS.CGM_TYPE = @CageOperationType_FromTerminal
               AND (CGM_STATUS IN (2, 3, 4, 8, 9, 10) 
                    OR (CGM_STATUS = 0 AND CGM_TYPE = 0))
                                      
          ) AS _TBL_GLB
      WHERE CMD_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)
   GROUP BY _TBL_GLB.CMD_ISO_CODE, _TBL_GLB.ORIGIN
   ORDER BY CASE WHEN _TBL_GLB.CMD_ISO_CODE = @NationalIsoCode THEN '000' ELSE _TBL_GLB.CMD_ISO_CODE END 
          , _TBL_GLB.ORIGIN DESC
         
    -- TABLE[4]: Other System Concepts           
    SELECT CSM_CONCEPT_ID AS CM_CONCEPT_ID
         , CC.CC_DESCRIPTION AS CM_DESCRIPTION
         , CSM_ISO_CODE AS CM_ISO_CODE
         , SUM(CSM_VALUE) AS CM_VALUE
      FROM CAGE_SESSION_METERS AS CSM
           INNER JOIN CAGE_CONCEPTS AS CC ON CSM.CSM_CONCEPT_ID = CC.CC_CONCEPT_ID
     WHERE CSM_SOURCE_TARGET_ID = 0
       AND CSM_CONCEPT_ID IN (7, 8)
       AND CSM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
       AND CSM_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)
  GROUP BY CSM_CONCEPT_ID, CSM_ISO_CODE, CSM_VALUE, CC.CC_DESCRIPTION 
  ORDER BY CM_CONCEPT_ID
         , CASE WHEN CSM_ISO_CODE = @NationalIsoCode THEN '000' ELSE CSM_ISO_CODE END 
         
     -- TABLE[5]: Cage Counts
    SELECT ORIGIN AS ORIGIN
         , CMD_ISO_CODE AS ISO_CODE
         , ISNULL(SUM(CMD_DEPOSITS), 0) AS DEPOSITS
      FROM (SELECT CASE WHEN CMD_QUANTITY >= 0 THEN 0
                        ELSE CMD_QUANTITY END  AS ORIGIN
                 , CMD_ISO_CODE
                 , ISNULL(SUM (CASE WHEN CMD_QUANTITY >= 0 THEN CMD_QUANTITY 
                                    ELSE 1 END * CASE WHEN CGM_TYPE = 99 THEN -1 * CMD_DENOMINATION 
												                              ELSE CMD_DENOMINATION END), 0) AS CMD_DEPOSITS
              FROM CAGE_MOVEMENTS 
                   INNER JOIN CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          
--             WHERE CGM_TYPE = 3
				WHERE CGM_TYPE IN (99, 199)
               AND CGM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
          GROUP BY CMD_ISO_CODE, CMD_DENOMINATION, CGM_TYPE, CMD_QUANTITY
            ) AS T1           
   GROUP BY ORIGIN, CMD_ISO_CODE
           
    UNION ALL
              
    SELECT NULL AS ORIGIN
         , CMD_ISO_CODE AS ISO_CODE
         , ISNULL(SUM(CMD_DEPOSITS), 0) AS DEPOSITS
      FROM (SELECT CMD_ISO_CODE
                 , ISNULL(SUM (CASE WHEN CMD_QUANTITY >= 0 THEN CMD_QUANTITY 
                                    ELSE 1 END * CASE WHEN CGM_TYPE = 99 THEN -1 * CMD_DENOMINATION 
												                              ELSE CMD_DENOMINATION END), 0) AS CMD_DEPOSITS
              FROM CAGE_MOVEMENTS 
                   INNER JOIN CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          
             WHERE CGM_TYPE IN (99, 199)
               AND CGM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
          GROUP BY CMD_ISO_CODE, CMD_DENOMINATION, CGM_TYPE, CMD_QUANTITY
           ) AS T1           
  GROUP BY CMD_ISO_CODE

    DROP TABLE #TMP_CAGE_SESSIONS 

  END -- IF @pCageSessionIds IS NOT NULL THEN
      
  ELSE -- IF @pCageSessionIds IS NULL
  BEGIN
      
    -- TABLE[1]: Get cage sessions information
    SELECT CM.CM_SOURCE_TARGET_ID
         , CM.CM_CONCEPT_ID
         , CM.CM_ISO_CODE
         , CM.CM_VALUE_IN
         , CM.CM_VALUE_OUT
         , CST.CST_SOURCE_TARGET_NAME
         , CC.CC_DESCRIPTION
      FROM CAGE_METERS AS CM
           INNER JOIN CAGE_SOURCE_TARGET_CONCEPTS AS CSTC ON CSTC.CSTC_CONCEPT_ID = CM.CM_CONCEPT_ID AND CSTC.CSTC_SOURCE_TARGET_ID = CM.CM_SOURCE_TARGET_ID
           INNER JOIN CAGE_CONCEPTS AS CC ON CC.CC_CONCEPT_ID = CSTC.CSTC_CONCEPT_ID
           INNER JOIN CAGE_SOURCE_TARGET AS CST ON CST.CST_SOURCE_TARGET_ID = CSTC.CSTC_SOURCE_TARGET_ID
     WHERE (CC.CC_TYPE = 1 OR CC_CONCEPT_ID IN (9, 10))
       AND CC.CC_ENABLED = 1
       AND CSTC.CSTC_ENABLED = 1
       AND CC.CC_SHOW_IN_REPORT = 1
       AND CM_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)
  ORDER BY CM.CM_SOURCE_TARGET_ID
         , CASE WHEN CM.CM_CONCEPT_ID = 0 THEN '000' ELSE CC.CC_DESCRIPTION END
         , CASE WHEN CM.CM_ISO_CODE = @NationalIsoCode THEN '000' ELSE CM.CM_ISO_CODE END   

    -- TABLE[2]: Get dynamic liabilities
    SELECT CC.CC_CONCEPT_ID AS LIABILITY_CONCEPT_ID
         , CC.CC_DESCRIPTION AS LIABILITY_NAME 
         , CM.CM_ISO_CODE AS LIABILITY_ISO_CODE
         , SUM(CM.CM_VALUE) AS LIABILITY_VALUE
      FROM CAGE_METERS AS CM
           INNER JOIN CAGE_CONCEPTS AS CC ON CC.CC_CONCEPT_ID = CM.CM_CONCEPT_ID
     WHERE CC.CC_ENABLED = 1
       AND CC.CC_SHOW_IN_REPORT = 1
       AND CC.CC_IS_PROVISION = 1
       AND CM_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)
  GROUP BY CC.CC_CONCEPT_ID, CC.CC_DESCRIPTION, CM.CM_ISO_CODE
  ORDER BY CC.CC_CONCEPT_ID ASC 
         , CASE WHEN CM.CM_ISO_CODE = @NationalIsoCode THEN '000' ELSE CM.CM_ISO_CODE END 
      
    -- TABLE[3]: Get session sumary
    SELECT CMD_ISO_CODE AS ISO_CODE
         , ISNULL(ORIGIN, 0) AS ORIGIN
         , SUM(CMD_DEPOSITS) AS DEPOSITS
         , SUM(CMD_WITHDRAWALS) AS WITHDRAWALS     
      FROM (SELECT CMD_ISO_CODE
                 , ORIGIN
                 , SUM(CMD_DEPOSITS) CMD_DEPOSITS
                 , SUM(CMD_WITHDRAWALS) CMD_WITHDRAWALS      
              FROM (SELECT CASE WHEN CMD_QUANTITY >= 0 THEN @BillsId
                                ELSE CMD_QUANTITY END AS ORIGIN
                         , CMD_ISO_CODE
                         , CMD_DENOMINATION
                         , CASE WHEN CGM_TYPE IN (100, 101, 103, 104) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY < 0 THEN CMD_DENOMINATION
                                                                                            ELSE CMD_QUANTITY * CMD_DENOMINATION END), 0)
                                ELSE 0 END AS CMD_DEPOSITS
                         , CASE WHEN CGM_TYPE IN (0, 1, 4, 5) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY < 0 THEN CMD_DENOMINATION             
                                                                                    ELSE CMD_QUANTITY * CMD_DENOMINATION END), 0)
                                ELSE 0 END AS CMD_WITHDRAWALS
                      FROM CAGE_MOVEMENTS 
                           INNER JOIN   CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          
                     WHERE CGM_STATUS IN (2, 3, 4, 8, 9, 10) 
                        OR (CGM_STATUS = 0 AND CGM_TYPE = 0)
                  GROUP BY CMD_ISO_CODE, CMD_DENOMINATION, CGM_TYPE, CMD_QUANTITY
                    ) AS T1
           GROUP BY ORIGIN,CMD_ISO_CODE

              -- GET Terminal value-in amounts
              
              UNION ALL 
              
              SELECT @NationalIsoCode AS CMD_ISO_CODE
                    , @TicketsId AS ORIGIN
                    , MC_COLLECTED_TICKET_AMOUNT AS DEPOSITS
                    , 0 AS WITHDRAWS
              FROM MONEY_COLLECTIONS
                  INNER JOIN CAGE_MOVEMENTS ON CGM_MC_COLLECTION_ID = MC_COLLECTION_ID 
             WHERE CAGE_MOVEMENTS.CGM_TYPE = @CAGEOPERATIONTYPE_FROMTERMINAL
               AND (CGM_STATUS IN (2, 3, 4, 8, 9, 10) 
                    OR (CGM_STATUS = 0 AND CGM_TYPE = 0))           
             
              UNION ALL
              
                SELECT @NationalIsoCode AS CMD_ISO_CODE
                    , @BillsId AS ORIGIN
                    , MC_COLLECTED_BILL_AMOUNT  AS DEPOSITS
                    , 0 AS WITHDRAWS
              FROM MONEY_COLLECTIONS
                  INNER JOIN CAGE_MOVEMENTS ON CGM_MC_COLLECTION_ID = MC_COLLECTION_ID 
             WHERE CAGE_MOVEMENTS.CGM_TYPE = @CageOperationType_FromTerminal
               AND (CGM_STATUS IN (2, 3, 4, 8, 9, 10) 
                    OR (CGM_STATUS = 0 AND CGM_TYPE = 0))
                          
          ) AS _TBL_GLB 
      WHERE CMD_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)             
   GROUP BY _TBL_GLB.CMD_ISO_CODE, _TBL_GLB.ORIGIN
   ORDER BY CASE WHEN _TBL_GLB.CMD_ISO_CODE = @NationalIsoCode THEN '000' ELSE _TBL_GLB.CMD_ISO_CODE END 
          , _TBL_GLB.ORIGIN DESC 
         
    -- TABLE[4]: Other System Concepts           
    SELECT CM_CONCEPT_ID
         , CC.CC_DESCRIPTION AS CM_DESCRIPTION
         , CM_ISO_CODE
         , SUM(CM_VALUE) AS CM_VALUE
      FROM CAGE_METERS AS CM
           INNER JOIN CAGE_CONCEPTS AS CC ON CM.CM_CONCEPT_ID = CC.CC_CONCEPT_ID
     WHERE CM_SOURCE_TARGET_ID = 0
       AND CM_CONCEPT_ID IN (7, 8)
       AND CM_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)
  GROUP BY CM_CONCEPT_ID, CM_ISO_CODE, CM_VALUE, CC.CC_DESCRIPTION
  ORDER BY CM_CONCEPT_ID
         , CASE WHEN CM_ISO_CODE = @NationalIsoCode THEN '000' ELSE CM_ISO_CODE END
    
    -- TABLE[5]: Cage Counts
    SELECT ORIGIN AS ORIGIN
         , CMD_ISO_CODE AS ISO_CODE
         , ISNULL(SUM(CMD_DEPOSITS), 0) AS DEPOSITS
      FROM (SELECT CASE WHEN CMD_QUANTITY >= 0 THEN 0
                        ELSE CMD_QUANTITY END  AS ORIGIN
                 , CMD_ISO_CODE
                 , ISNULL(SUM (CASE WHEN CMD_QUANTITY >= 0 THEN CMD_QUANTITY 
                                    ELSE 1 END * CASE WHEN CGM_TYPE = 99 THEN -1 * CMD_DENOMINATION 
												                              ELSE CMD_DENOMINATION END), 0) AS CMD_DEPOSITS
              FROM CAGE_MOVEMENTS 
                   INNER JOIN CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          
             WHERE CGM_TYPE IN (99, 199)
          GROUP BY CMD_ISO_CODE, CMD_DENOMINATION, CGM_TYPE, CMD_QUANTITY
            ) AS T1           
   GROUP BY ORIGIN, CMD_ISO_CODE
           
    UNION ALL
              
    SELECT NULL AS ORIGIN
         , CMD_ISO_CODE AS ISO_CODE
         , ISNULL(SUM(CMD_DEPOSITS), 0) AS DEPOSITS
      FROM (SELECT CMD_ISO_CODE
                 , ISNULL(SUM (CASE WHEN CMD_QUANTITY >= 0 THEN CMD_QUANTITY 
                                    ELSE 1 END * CASE WHEN CGM_TYPE = 99 THEN -1 * CMD_DENOMINATION 
												                              ELSE CMD_DENOMINATION END), 0) AS CMD_DEPOSITS
              FROM CAGE_MOVEMENTS 
                   INNER JOIN CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          
             WHERE CGM_TYPE IN (99, 199)
          GROUP BY CMD_ISO_CODE, CMD_DENOMINATION, CGM_TYPE, CMD_QUANTITY
           ) AS T1           
  GROUP BY CMD_ISO_CODE


  END -- ELSE
  
  DROP TABLE #TMP_CURRENCIES_ISO_CODES
                   
END -- CageGetGlobalReportData

GO
GRANT EXECUTE ON [dbo].[CageGetGlobalReportData] TO [wggui] WITH GRANT OPTION 
GO
