/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 328;

SET @New_ReleaseId = 329;
SET @New_ScriptName = N'UpdateTo_18.329.041.sql';
SET @New_Description = N'report_tool_config';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/**** GENERAL PARAM *****/

/******* TABLES  *******/

-- ADD COLUMN rtc_mode_type IN report_tool_config

IF NOT EXISTS( SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
               WHERE TABLE_NAME = 'REPORT_TOOL_CONFIG' AND COLUMN_NAME = 'RTC_MODE_TYPE')
BEGIN
   ALTER TABLE report_tool_config
   ADD rtc_mode_type INTEGER
END

GO 

/******* INDEXES *******/

/******* RECORDS *******/

UPDATE REPORT_TOOL_CONFIG 
   SET RTC_MODE_TYPE = 0


UPDATE REPORT_TOOL_CONFIG 
   SET RTC_MODE_TYPE = 1,
   rtc_report_name = '<LanguageResources>
					  <NLS09>
						<Label>Meters collection to collection</Label>
					  </NLS09>
					  <NLS10>
						<Label>Contadores recolección a recolección</Label>
					  </NLS10>
					</LanguageResources>'
 WHERE RTC_LOCATION_MENU = 2
   
UPDATE REPORT_TOOL_CONFIG 
   SET RTC_MODE_TYPE = 11 WHERE RTC_LOCATION_MENU = 12

UPDATE REPORT_TOOL_CONFIG 
   SET RTC_MODE_TYPE = 1 WHERE RTC_LOCATION_MENU = 7

GO 

ALTER TABLE report_tool_config 
ALTER COLUMN rtc_mode_type INTEGER NOT NULL

GO 

/******* PROCEDURES *******/

IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'S2S_SessionStart')
       DROP PROCEDURE S2S_SessionStart
GO
CREATE PROCEDURE S2S_SessionStart
  @pTrackData        varchar(24),
  @pVendorID        varchar(16),
  @pSerialNumber    varchar(30),
  @pMachineNumber   int,
  @pVendorSessionID bigint = 0
AS
BEGIN

  DECLARE @CurrentJackPot money
  SET @CurrentJackPot = 0

  EXECUTE zsp_SessionStart @pTrackData, @pVendorID, @pSerialNumber, @pMachineNumber, @CurrentJackPot, @pVendorSessionID
    
END
GO