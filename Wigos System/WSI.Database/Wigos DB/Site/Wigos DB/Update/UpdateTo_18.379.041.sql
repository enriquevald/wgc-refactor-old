/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 378;

SET @New_ReleaseId = 379;
SET @New_ScriptName = N'UpdateTo_18.379.041.sql';
SET @New_Description = N'SP_AFIP;Machine draw'; 

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/**** GENERAL PARAM *****/

IF (NOT EXISTS(SELECT 1 FROM general_params WHERE gp_group_key = 'Log' and gp_subject_key = 'WriteLog.Message'))
BEGIN 
    INSERT INTO general_params (gp_group_key, gp_subject_key, gp_key_value)
    VALUES ('Log','WriteLog.Message', 0)
END 

/******* TABLES  *******/
GO

--TABLE PENDING RECHARGES
--PENDING CREATE INDEX IF NEEDED
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.terminal_draws_recharges') AND type in (N'U'))
BEGIN
CREATE TABLE dbo.terminal_draws_recharges(
	tdr_id              BIGINT IDENTITY(1,1)  NOT NULL,
	tdr_account_id      BIGINT                NOT NULL,
	tdr_status          INT                   NOT NULL,
	tdr_total_cash_in   MONEY                 NULL,
	tdr_re_bet          MONEY                 NULL,
	tdr_nr_bet          MONEY                 NULL,
	tdr_points_bet      MONEY                 NULL,
	tdr_operation_id    BIGINT                NOT NULL,
	tdr_game_id         BIGINT                NOT NULL
	
CONSTRAINT [PK_tdr_id] PRIMARY KEY CLUSTERED 
(
  tdr_id ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END

GO

/******* INDEXES *******/

/******* RECORDS *******/
GO

IF NOT EXISTS(SELECT * FROM TERMINALS WHERE te_base_name = 'SORTEADOR TERMINAL')
BEGIN
	INSERT INTO TERMINALS
	(
	TE_TYPE,
	TE_SERVER_ID,
	TE_EXTERNAL_ID,
	TE_BLOCKED,
	TE_ACTIVE,
	TE_PROVIDER_ID,
	TE_CLIENT_ID,
	TE_BUILD_ID,
	TE_TERMINAL_TYPE,
	TE_VENDOR_ID,
	TE_STATUS,
	TE_RETIREMENT_DATE,
	TE_RETIREMENT_REQUESTED,
	TE_DENOMINATION,
	TE_MULTI_DENOMINATION,
	TE_PROGRAM,
	TE_THEORETICAL_PAYOUT,
	TE_PROV_ID,
	TE_BANK_ID,
	TE_FLOOR_ID,
	TE_GAME_TYPE,
	TE_ACTIVATION_DATE,
	TE_CURRENT_ACCOUNT_ID,
	TE_CURRENT_PLAY_SESSION_ID,
	TE_REGISTRATION_CODE,
	TE_SAS_FLAGS,
	TE_SERIAL_NUMBER,
	TE_CABINET_TYPE,
	TE_JACKPOT_CONTRIBUTION_PCT,
	TE_CONTRACT_TYPE,
	TE_CONTRACT_ID,
	TE_ORDER_NUMBER,
	TE_WXP_REPORTED,
	TE_WXP_REPORTED_STATUS_DATETIME,
	TE_WXP_REPORTED_STATUS,
	TE_SEQUENCE_ID,
	TE_VALIDATION_TYPE,
	TE_ALLOWED_CASHABLE_EMISSION,
	TE_ALLOWED_PROMO_EMISSION,
	TE_ALLOWED_REDEMPTION,
	TE_MAX_ALLOWED_TI,
	TE_MAX_ALLOWED_TO,
	TE_SAS_VERSION,
	TE_SAS_MACHINE_NAME,
	TE_BONUS_FLAGS,
	TE_FEATURES_BYTES,
	TE_VIRTUAL_ACCOUNT_ID,
	TE_SAS_FLAGS_USE_SITE_DEFAULT,
	TE_AUTHENTICATION_METHOD,
	TE_AUTHENTICATION_SEED,
	TE_AUTHENTICATION_SIGNATURE,
	TE_AUTHENTICATION_STATUS,
	TE_AUTHENTICATION_LAST_CHECKED,
	TE_MACHINE_ID,
	TE_POSITION,
	TE_TOP_AWARD,
	TE_MAX_BET,
	TE_NUMBER_LINES,
	TE_GAME_THEME,
	TE_ACCOUNT_PROMOTION_ID,
	TE_MASTER_ID,
	TE_CHANGE_ID,
	TE_BASE_NAME,
	TE_MACHINE_ASSET_NUMBER,
	TE_ASSET_NUMBER,
	TE_MACHINE_SERIAL_NUMBER,
	TE_METER_DELTA_ID,
	TE_LAST_GAME_PLAYED_ID,
	TE_TRANSFER_STATUS,
	TE_SMIB2EGM_COMM_TYPE,
	TE_SMIB2EGM_CONF_ID,
	TE_BRAND_CODE,
	TE_MODEL,
	TE_MANUFACTURE_YEAR,
	TE_MET_HOMOLOGATED,
	TE_BET_CODE,
	TE_COIN_COLLECTION,
	TE_EQUITY_PERCENTAGE,
	TE_TERMINAL_CURRENCY_ID,
	TE_ISO_CODE,
	TE_SAS_ACCOUNTING_DENOM,
	TE_TITO_HOST_ID)
		SELECT TOP  1 
								TE_TYPE,
								TE_SERVER_ID,
								TE_EXTERNAL_ID,
								TE_BLOCKED,
								TE_ACTIVE,
								TE_PROVIDER_ID,
								TE_CLIENT_ID,
								TE_BUILD_ID,
								113,
								TE_VENDOR_ID,
								TE_STATUS,
								TE_RETIREMENT_DATE,
								TE_RETIREMENT_REQUESTED,
								TE_DENOMINATION,
								TE_MULTI_DENOMINATION,
								TE_PROGRAM,
								TE_THEORETICAL_PAYOUT,
								TE_PROV_ID,
								TE_BANK_ID,
								TE_FLOOR_ID,
								TE_GAME_TYPE,
								TE_ACTIVATION_DATE,
								TE_CURRENT_ACCOUNT_ID,
								TE_CURRENT_PLAY_SESSION_ID,
								TE_REGISTRATION_CODE,
								TE_SAS_FLAGS,
								TE_SERIAL_NUMBER,
								TE_CABINET_TYPE,
								TE_JACKPOT_CONTRIBUTION_PCT,
								TE_CONTRACT_TYPE,
								TE_CONTRACT_ID,
								TE_ORDER_NUMBER,
								TE_WXP_REPORTED,
								TE_WXP_REPORTED_STATUS_DATETIME,
								TE_WXP_REPORTED_STATUS,
								TE_SEQUENCE_ID,
								TE_VALIDATION_TYPE,
								TE_ALLOWED_CASHABLE_EMISSION,
								TE_ALLOWED_PROMO_EMISSION,
								TE_ALLOWED_REDEMPTION,
								TE_MAX_ALLOWED_TI,
								TE_MAX_ALLOWED_TO,
								TE_SAS_VERSION,
								TE_SAS_MACHINE_NAME,
								TE_BONUS_FLAGS,
								TE_FEATURES_BYTES,
								TE_VIRTUAL_ACCOUNT_ID,
								TE_SAS_FLAGS_USE_SITE_DEFAULT,
								TE_AUTHENTICATION_METHOD,
								TE_AUTHENTICATION_SEED,
								TE_AUTHENTICATION_SIGNATURE,
								TE_AUTHENTICATION_STATUS,
								TE_AUTHENTICATION_LAST_CHECKED,
								TE_MACHINE_ID,
								TE_POSITION,
								TE_TOP_AWARD,
								TE_MAX_BET,
								TE_NUMBER_LINES,
								TE_GAME_THEME,
								TE_ACCOUNT_PROMOTION_ID,
								TE_MASTER_ID,
								TE_CHANGE_ID,
								'SORTEADOR TERMINAL',
								TE_MACHINE_ASSET_NUMBER,
								TE_ASSET_NUMBER,
								TE_MACHINE_SERIAL_NUMBER,
								TE_METER_DELTA_ID,
								TE_LAST_GAME_PLAYED_ID,
								TE_TRANSFER_STATUS,
								TE_SMIB2EGM_COMM_TYPE,
								TE_SMIB2EGM_CONF_ID,
								TE_BRAND_CODE,
								TE_MODEL,
								TE_MANUFACTURE_YEAR,
								TE_MET_HOMOLOGATED,
								TE_BET_CODE,
								TE_COIN_COLLECTION,
								TE_EQUITY_PERCENTAGE,
								TE_TERMINAL_CURRENCY_ID,
								TE_ISO_CODE,
								TE_SAS_ACCOUNTING_DENOM,
								TE_TITO_HOST_ID
		FROM TERMINALS 
		WHERE TE_TERMINAL_TYPE = 106

END

GO

UPDATE terminals 
SET te_master_id = 
	(SELECT TOP 1
		te_terminal_id
	 FROM terminals
	 WHERE te_name = 'SORTEADOR TERMINAL')
WHERE te_terminal_id = (SELECT TOP 1
		TE_TERMINAL_ID
	 FROM terminals
	 WHERE te_name = 'SORTEADOR TERMINAL')
	 
	 
GO

/******* PROCEDURES *******/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_AFIP_GAMING_TABLES]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_AFIP_GAMING_TABLES]

GO

CREATE PROCEDURE [dbo].[SP_AFIP_GAMING_TABLES]
(
  @pStarDateTime    DATETIME
)
AS
BEGIN
  --Get the current iso code in general params
  DECLARE @_Currency_ISO_Code           AS  VARCHAR(5);
  DECLARE @_Default_Cashier_Afip_Type   AS  INT;
  DECLARE @_GUI_User_Type_SYS_GamingTables   AS  INT;  
  DECLARE @_GUI_User_Type_SYS_TITO_User      AS  INT;  
  DECLARE @_GUI_User_Type_SYS_SYSTEM         AS  INT;  
  DECLARE @_old_chips_iso_code               AS  VARCHAR(3);
  DECLARE @_opening_cash 					           AS INT;	
  DECLARE @_cage_close_session               AS INT;
  DECLARE @_cage_filler_in                   AS INT;
  DECLARE @_cage_filler_out                  AS INT;
  DECLARE @_chips_sale                       AS INT;
  DECLARE @_chips_purchase                   AS INT;
  DECLARE @_chips_sale_devolution_for_tito   AS INT;
  DECLARE @_chips_sale_with_cash_in          AS INT;
  DECLARE @_chips_sale_register_total        AS INT;
  DECLARE @_chips_redeemable                 AS VARCHAR(5);
    
  
  
  SET @_GUI_User_Type_SYS_SYSTEM         = 3;  
  SET @_GUI_User_Type_SYS_TITO_User      = 4;  
  SET @_GUI_User_Type_SYS_GamingTables   = 5; 
  SET @_opening_cash 					 = 0;   	
  SET @_cage_close_session               = 201; 
  SET @_cage_filler_in                   = 202;
  SET @_cage_filler_out                  = 203; 
  SET @_chips_sale                       = 300; 
  SET @_chips_purchase                   = 301; 
  SET @_chips_sale_devolution_for_tito   = 302; 
  SET @_chips_sale_with_cash_in          = 307; 
  SET @_chips_sale_register_total        = 311; 
  SET @_chips_redeemable                 = '1001';
  SET @_Default_Cashier_Afip_Type        = 99;  
  SET @_old_chips_iso_code               = 'X01';


  SET @_Currency_ISO_Code = (SELECT GP_KEY_VALUE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'RegionalOptions' and GP_SUBJECT_KEY = 'CurrencyISOCode')

  --Get the total sum of the GAMING TABLES
  SELECT TABLE_ID
       , GTABLE_NAME
       , GTABLE_TYPE_ID
       , GTABLE_TYPE_DESC
       , ISNULL(SUM(OPENING_CASH), 0) AS OPENING_CASH
       , ISNULL(SUM(CLOSING_CASH), 0) AS CLOSING_CASH
       , ISNULL(SUM(OPENING_CHIPS), 0) AS OPENING_CHIPS
       , ISNULL(SUM(CLOSING_CHIPS), 0) AS CLOSING_CHIPS
       , ISNULL(SUM(CASH_OUT_TOTAL), 0) AS CASH_OUT_TOTAL
       , ISNULL(SUM(CASH_IN_TOTAL), 0) AS CASH_IN_TOTAL
       , ISNULL(SUM(CHIPS_OUT_TOTAL), 0) AS CHIPS_OUT_TOTAL
       , ISNULL(SUM(CHIPS_IN_TOTAL), 0) AS CHIPS_IN_TOTAL
       , ISNULL(SUM(SALE_CHIPS_TOTAL), 0) AS SALE_CHIPS_TOTAL
       , ISNULL(SUM(BUY_CHIPS_TOTAL), 0) AS BUY_CHIPS_TOTAL
       , CS_OPENING_DATE
       , CS_CLOSING_DATE
  FROM
  (
      --Get partial sum
      SELECT CM_TYPE
           , CM_INITIAL_BALANCE
           , CM_GAMING_TABLE_SESSION_ID
           , CM_CURRENCY_ISO_CODE
           , GT_GAMING_TABLE_ID AS TABLE_ID
           , GT_NAME AS GTABLE_NAME
           , GTT_REPORT_TYPE_ID AS GTABLE_TYPE_ID
           , GTT_NAME AS GTABLE_TYPE_DESC
           , CASE WHEN CM_TYPE = @_opening_cash THEN CM_INITIAL_BALANCE ELSE 0 END AS OPENING_CASH
           , CASE WHEN CM_TYPE = @_cage_close_session AND CM_CURRENCY_ISO_CODE = @_Currency_ISO_Code THEN ISNULL(SUM(CM_FINAL_BALANCE), 0) ELSE 0 END AS CLOSING_CASH
           , GTS_INITIAL_CHIPS_AMOUNT AS OPENING_CHIPS
           , CASE WHEN CM_TYPE = @_cage_close_session AND (CM_CURRENCY_ISO_CODE = @_old_chips_iso_code OR CM_CAGE_CURRENCY_TYPE = @_chips_redeemable) THEN ISNULL(SUM(CM_FINAL_BALANCE), 0) ELSE 0 END AS CLOSING_CHIPS
           , CASE WHEN CM_TYPE = @_cage_filler_out AND CM_CURRENCY_ISO_CODE = @_Currency_ISO_Code   THEN ISNULL(SUM(CM_SUB_AMOUNT), 0) ELSE 0 END AS CASH_OUT_TOTAL
           , CASE WHEN CM_TYPE = @_cage_filler_in AND CM_CURRENCY_ISO_CODE = @_Currency_ISO_Code THEN ISNULL(SUM(CM_ADD_AMOUNT), 0) ELSE 0 END AS CASH_IN_TOTAL
           , CASE WHEN CM_TYPE = @_cage_filler_out AND (CM_CURRENCY_ISO_CODE = @_old_chips_iso_code  OR CM_CAGE_CURRENCY_TYPE = @_chips_redeemable) THEN ISNULL(SUM(CM_SUB_AMOUNT), 0) ELSE 0 END AS CHIPS_OUT_TOTAL
           , CASE WHEN CM_TYPE = @_cage_filler_in AND CM_CURRENCY_ISO_CODE = @_old_chips_iso_code THEN ISNULL(SUM(CM_ADD_AMOUNT), 0) ELSE 0 END AS CHIPS_IN_TOTAL
           , CASE WHEN CM_TYPE in (@_chips_sale, @_chips_sale_devolution_for_tito, @_chips_sale_with_cash_in, @_chips_sale_register_total) AND GTS_GAMING_TABLE_SESSION_ID = CM_GAMING_TABLE_SESSION_ID THEN ISNULL(SUM(CM_SUB_AMOUNT), 0) ELSE 0 END AS SALE_CHIPS_TOTAL
           , CASE WHEN CM_TYPE in (@_chips_purchase) AND GTS_GAMING_TABLE_SESSION_ID = CM_GAMING_TABLE_SESSION_ID THEN ISNULL(SUM(CM_ADD_AMOUNT), 0) ELSE 0 END AS BUY_CHIPS_TOTAL
           , CS_OPENING_DATE
           , CS_CLOSING_DATE
      FROM  GAMING_TABLES_TYPES
      INNER JOIN GAMING_TABLES            ON GTT_GAMING_TABLE_TYPE_ID = GT_TYPE_ID
      INNER JOIN GAMING_TABLES_SESSIONS   ON GT_GAMING_TABLE_ID = GTS_GAMING_TABLE_ID
      INNER JOIN CASHIER_MOVEMENTS        ON GTS_CASHIER_SESSION_ID = CM_SESSION_ID
      INNER JOIN CASHIER_SESSIONS         ON CM_SESSION_ID = CS_SESSION_ID
      INNER JOIN GUI_USERS                ON CS_USER_ID    = GU_USER_ID
      WHERE CM_DATE >= @pStarDateTime
        AND CM_DATE < DATEADD(DAY, 1, @pStarDateTime)
        AND CM_TYPE IN (@_opening_cash, @_cage_close_session, @_cage_filler_in, @_cage_filler_out, @_chips_sale, @_chips_purchase, @_chips_sale_devolution_for_tito, @_chips_sale_with_cash_in, @_chips_sale_register_total)
        AND GU_USER_TYPE = @_GUI_User_Type_SYS_GamingTables
      GROUP BY CM_TYPE, CM_INITIAL_BALANCE, CM_GAMING_TABLE_SESSION_ID, CM_CURRENCY_ISO_CODE,
            GT_GAMING_TABLE_ID, GT_NAME, GTT_REPORT_TYPE_ID, GTT_NAME, GTS_GAMING_TABLE_SESSION_ID, CS_OPENING_DATE,
            CS_CLOSING_DATE, GTS_CASHIER_SESSION_ID, GTS_INITIAL_CHIPS_AMOUNT, CM_CAGE_CURRENCY_TYPE
   ) AS GT
  GROUP BY  TABLE_ID, GTABLE_NAME, GTABLE_TYPE_ID, GTABLE_TYPE_DESC, CS_OPENING_DATE, CS_CLOSING_DATE

  UNION ALL

  --Get the total sum of the CASHIERS
  SELECT TABLE_ID
       , GTABLE_NAME
       , @_Default_Cashier_Afip_Type
       , TIPO_MESA
       , ISNULL(SUM(OPENING_CASH), 0) AS OPENING_CASH
       , ISNULL(SUM(CLOSING_CASH), 0) AS CLOSING_CASH
       , ISNULL(SUM(OPENING_CHIPS), 0) AS OPENING_CHIPS
       , ISNULL(SUM(CLOSING_CHIPS), 0) AS CLOSING_CHIPS
       , ISNULL(SUM(CASH_OUT_TOTAL), 0) AS CASH_OUT_TOTAL
       , ISNULL(SUM(CASH_IN_TOTAL), 0) AS CASH_IN_TOTAL
       , ISNULL(SUM(CHIPS_OUT_TOTAL), 0) AS CHIPS_OUT_TOTAL
       , ISNULL(SUM(CHIPS_IN_TOTAL), 0) AS CHIPS_IN_TOTAL
       , ISNULL(SUM(SALE_CHIPS_TOTAL), 0) AS SALE_CHIPS_TOTAL
       , ISNULL(SUM(BUY_CHIPS_TOTAL), 0) AS BUY_CHIPS_TOTAL
       , CS_OPENING_DATE
       , CS_CLOSING_DATE
  FROM
  (
      --Select partial sum
      SELECT CM_TYPE
           , CM_INITIAL_BALANCE
           , CM_GAMING_TABLE_SESSION_ID
           , CM_CURRENCY_ISO_CODE
           , CS_USER_ID AS TABLE_ID
           , GU_FULL_NAME AS GTABLE_NAME
           , 'OTROS' AS TIPO_MESA
           , CASE WHEN CM_TYPE = @_opening_cash THEN CM_INITIAL_BALANCE ELSE 0 END AS OPENING_CASH
           , CASE WHEN CM_TYPE = @_cage_close_session AND CM_CURRENCY_ISO_CODE = @_Currency_ISO_Code THEN ISNULL(SUM(CM_FINAL_BALANCE), 0) ELSE 0 END AS CLOSING_CASH
           , 0.00 AS OPENING_CHIPS
           , CASE WHEN CM_TYPE = @_cage_close_session AND CM_CURRENCY_ISO_CODE = 'X01' THEN ISNULL(SUM(CM_FINAL_BALANCE), 0) ELSE 0 END AS CLOSING_CHIPS
           , CASE WHEN CM_TYPE = @_cage_filler_out AND CM_CURRENCY_ISO_CODE = @_Currency_ISO_Code THEN ISNULL(SUM(CM_SUB_AMOUNT), 0) ELSE 0 END AS CASH_OUT_TOTAL
           , CASE WHEN CM_TYPE = @_cage_filler_in AND CM_CURRENCY_ISO_CODE = @_Currency_ISO_Code THEN ISNULL(SUM(CM_ADD_AMOUNT), 0) ELSE 0 END AS CASH_IN_TOTAL
           , CASE WHEN CM_TYPE = @_cage_filler_out AND CM_CURRENCY_ISO_CODE = 'X01' THEN ISNULL(SUM(CM_SUB_AMOUNT), 0) ELSE 0 END AS CHIPS_OUT_TOTAL
           , CASE WHEN CM_TYPE = @_cage_filler_in AND CM_CURRENCY_ISO_CODE = 'X01' THEN ISNULL(SUM(CM_ADD_AMOUNT), 0) ELSE 0 END AS CHIPS_IN_TOTAL
           , 0.00 AS SALE_CHIPS_TOTAL
           , 0.00 AS BUY_CHIPS_TOTAL
           , CS_OPENING_DATE, CS_CLOSING_DATE
      FROM CASHIER_MOVEMENTS
      INNER JOIN  CASHIER_SESSIONS  ON CM_SESSION_ID = CS_SESSION_ID
      INNER JOIN GUI_USERS          ON CS_USER_ID    = GU_USER_ID
      WHERE CM_DATE >= @pStarDateTime
        AND CM_DATE < DATEADD(DAY, 1, @pStarDateTime)
        AND CM_TYPE IN (@_opening_cash, @_cage_close_session, @_cage_filler_in, @_cage_filler_out, @_chips_sale, @_chips_purchase, @_chips_sale_devolution_for_tito, @_chips_sale_with_cash_in, @_chips_sale_register_total)
        AND GU_USER_TYPE NOT IN (@_GUI_User_Type_SYS_SYSTEM,@_GUI_User_Type_SYS_TITO_User,@_GUI_User_Type_SYS_GamingTables) 
      GROUP BY cs_session_id, CM_TYPE, CM_INITIAL_BALANCE, CM_GAMING_TABLE_SESSION_ID, CM_CURRENCY_ISO_CODE, CS_USER_ID, GU_FULL_NAME,
            CS_OPENING_DATE, CS_CLOSING_DATE
  ) AS GT
  GROUP BY TABLE_ID, GTABLE_NAME, TIPO_MESA, CS_OPENING_DATE, CS_CLOSING_DATE
END  -- End procedure SP_AFIP_GAMING_TABLES

GO

GRANT EXECUTE ON [dbo].[SP_AFIP_GAMING_TABLES] TO [WGGUI] WITH GRANT OPTION

GO


/****** Object:  StoredProcedure [dbo].[ReportTerminalDraws]    Script Date: 01/27/2017 11:03:08 ******/
SET ANSI_NULLS ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ReportTerminalDraws]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ReportTerminalDraws]
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[ReportTerminalDraws]  
  @pDrawFrom	    DATETIME      = NULL,
  @pDrawTo		    DATETIME      = NULL,
  @pDrawId		    BIGINT		  =	NULL,
  @pCashierId       INT			  = NULL,
  @pUserId          INT			  = NULL,
  @pSqlAccount      NVARCHAR(MAX) = NULL,
  @pCashierMovement INT	          = NULL,
  @pTerminalDraw	INT				= 1,
  @pLooserPrizeId   INT           = 5

AS
BEGIN

	SET NOCOUNT ON;
	DECLARE @_Terminal_draw_type AS INT;
	DECLARE @_Sorter_terminal_ID AS INT;
	DECLARE @_Promotion_Redeemeable AS INT;
	DECLARE @_Promotion_NoRedeemeable AS INT;
	SET @_Terminal_draw_type = 113
	SET @_Sorter_terminal_ID = (SELECT  TOP 1 te_terminal_id FROM terminals WHERE te_terminal_type = @_Terminal_draw_type)
	SET @_Promotion_Redeemeable = 14
	SET @_Promotion_NoRedeemeable = 13

    CREATE TABLE #ACCOUNTS_TEMP (AC_ACCOUNT_ID BIGINT NOT NULL, AC_HOLDER_NAME NVARCHAR(200), AC_TRACK_DATA NVARCHAR(50))
	IF @pSqlAccount IS NOT NULL
	BEGIN
	  INSERT INTO #ACCOUNTS_TEMP EXEC ('SELECT AC_ACCOUNT_ID, AC_HOLDER_NAME, AC_TRACK_DATA  FROM ACCOUNTS ' + @pSqlAccount)
		IF @@ROWCOUNT > 500
			ALTER TABLE #ACCOUNTS_TEMP ADD PRIMARY KEY NONCLUSTERED (AC_ACCOUNT_ID)
	END

	IF 	@pDrawId IS NOT NULL 
	--
	-- Filtering by Draw ID
	--
	BEGIN	
	  IF @pDrawFrom IS NULL SET @pDrawFrom = '2007-01-01T00:00:00'
		IF @pDrawTo   IS NULL SET @pDrawTo   = GETDATE ()
		
    --Filter by terminalDraw 
	IF  @pTerminalDraw = 1
	
	  BEGIN
		    SELECT DISTINCT
		        CD_DRAW_ID
		      , CD_DRAW_DATETIME
		      , TE_TERMINAL_TYPE
		      , CM_CASHIER_NAME		  
		      , CM_USER_NAME
		      , CD_OPERATION_ID		
		      , CD_ACCOUNT_ID
		      , AC_HOLDER_NAME
		      , CD_COMBINATION_BET
		      , CD_COMBINATION_WON
		      , CD_RE_BET
          , CASE WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_draw_type THEN AC.AO_PROMO_REDEEMABLE 
                 ELSE CD_RE_WON END CD_RE_WON
		      , CASE WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_draw_type  THEN AC.AO_PROMO_NOT_REDEEMABLE 
		             ELSE CD_NR_WON END CD_NR_WON
		      , CD_PRIZE_ID
		    FROM
		       CASHDESK_DRAWS
		       WITH(INDEX(IX_CD_DRAW_DATETIME_ACCOUNT))
		        INNER JOIN CASHIER_MOVEMENTS WITH(INDEX(IX_CM_OPERATION_ID)) ON CD_OPERATION_ID = CM_OPERATION_ID  
		        INNER JOIN ACCOUNTS   ON CD_ACCOUNT_ID   = AC_ACCOUNT_ID
		        INNER JOIN TERMINALS   ON (CASE WHEN CD_TERMINAL IS NOT NULL THEN CD_TERMINAL ELSE @_Sorter_terminal_ID END) = TE_TERMINAL_ID
		        LEFT JOIN ACCOUNT_OPERATIONS AC ON AC.AO_OPERATION_ID = CD_OPERATION_ID
		        LEFT  JOIN ACCOUNT_PROMOTIONS AP ON AP.ACP_OPERATION_ID = CD_OPERATION_ID AND AP.ACP_PROMO_TYPE IN (@_Promotion_Redeemeable, @_Promotion_NoRedeemeable)
		        
		    WHERE    
		        CD_DRAW_ID = @pDrawId	
		        AND CD_DRAW_DATETIME >= @pDrawFrom
		        AND CD_DRAW_DATETIME < @pDrawTo	
		        AND ((@pCashierId IS NULL) OR (CM_CASHIER_ID = @pCashierId))
		        AND ((@pUserId IS NULL) OR (CM_USER_ID = @pUserId))
		        AND ( (@pSqlAccount     IS NULL) OR (CD_ACCOUNT_ID IN ( SELECT AC_ACCOUNT_ID FROM #ACCOUNTS_TEMP ) ) )
		        AND te_terminal_type = @_Terminal_draw_type
		    ORDER BY CD_DRAW_ID
		    DROP TABLE #ACCOUNTS_TEMP

		    RETURN		    	  
	  END	

	
	IF 	@pSqlAccount IS NOT NULL 
	--
	-- Filtering by account, but not by draw ID
	--
	BEGIN		
		IF @pDrawFrom IS NULL SET @pDrawFrom = '2007-01-01T00:00:00'
		IF @pDrawTo   IS NULL SET @pDrawTo   = GETDATE ()

	  --Filter by TERMINAL draw
	  IF  @pTerminalDraw = 1
	  BEGIN
          SELECT DISTINCT
		        CD_DRAW_ID
		      , CD_DRAW_DATETIME
		      , TE_TERMINAL_TYPE
		      , CM_CASHIER_NAME		  
		      , CM_USER_NAME
		      , CD_OPERATION_ID		
		      , CD_ACCOUNT_ID
		      , CD_COMBINATION_BET
		      , CD_COMBINATION_WON
		      , CD_RE_BET
          , CASE WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_draw_type THEN AC.AO_PROMO_REDEEMABLE 
                 ELSE CD_RE_WON END CD_RE_WON
		      , CASE WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_draw_type  THEN AC.AO_PROMO_NOT_REDEEMABLE 
		             ELSE CD_NR_WON END CD_NR_WON
		      , CD_PRIZE_ID
		    FROM
		       CASHDESK_DRAWS
		       WITH(INDEX(IX_CD_ACCOUNT_DATETIME))
		        INNER JOIN CASHIER_MOVEMENTS WITH(INDEX(IX_CM_OPERATION_ID)) ON CD_OPERATION_ID = CM_OPERATION_ID 
		        INNER JOIN #ACCOUNTS_TEMP    ON CD_ACCOUNT_ID   = AC_ACCOUNT_ID
		        INNER JOIN TERMINALS   ON (CASE WHEN CD_TERMINAL IS NOT NULL THEN CD_TERMINAL ELSE @_Sorter_terminal_ID END) = TE_TERMINAL_ID
		        LEFT JOIN ACCOUNT_OPERATIONS AC ON AC.AO_OPERATION_ID = CD_OPERATION_ID
		        LEFT JOIN ACCOUNT_PROMOTIONS AP ON AP.ACP_OPERATION_ID = CD_OPERATION_ID AND AP.ACP_PROMO_TYPE IN (@_Promotion_Redeemeable, @_Promotion_NoRedeemeable)
		        
		    WHERE    
			    CD_DRAW_DATETIME >= @pDrawFrom
		       AND CD_DRAW_DATETIME < @pDrawTo 		   
		       AND((@pCashierId IS NULL) OR (CM_CASHIER_ID = @pCashierId))
		       AND ((@pUserId IS NULL) OR (CM_USER_ID = @pUserId))
		       AND    te_terminal_type = @_Terminal_draw_type
		    ORDER BY CD_DRAW_ID
		    DROP TABLE #ACCOUNTS_TEMP
		    
		    RETURN
	  END
	END 
	
	end		
	--
	-- Filter by Date, but not by account neither Draw ID
	--
	IF @pDrawFrom IS NULL SET @pDrawFrom = '2007-01-01T00:00:00'
	IF @pDrawTo   IS NULL SET @pDrawTo   = GETDATE ()
	

	--Filter by Terminal draw
	IF  @pTerminalDraw = 1
	BEGIN
      SELECT DISTINCT
	      CD_DRAW_ID
	    , CD_DRAW_DATETIME
	    , TE_TERMINAL_TYPE
	    , CM_CASHIER_NAME		  
	    , CM_USER_NAME
	    , CD_OPERATION_ID		
	    , CD_ACCOUNT_ID
	    , AC_HOLDER_NAME
	    , CD_COMBINATION_BET
	    , CD_COMBINATION_WON
	    , CD_RE_BET
      , CASE WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_draw_type THEN AC.AO_PROMO_REDEEMABLE 
             ELSE CD_RE_WON END CD_RE_WON
      , CASE WHEN CD_PRIZE_ID = @pLooserPrizeId AND TE_TERMINAL_TYPE = @_Terminal_draw_type  THEN AC.AO_PROMO_NOT_REDEEMABLE 
             ELSE CD_NR_WON END CD_NR_WON
	    , CD_PRIZE_ID
	  FROM
	     CASHDESK_DRAWS WITH ( INDEX(IX_CD_DATETIME))	   
	      INNER JOIN CASHIER_MOVEMENTS WITH(INDEX(IX_CM_OPERATION_ID)) ON CD_OPERATION_ID = CM_OPERATION_ID
	      INNER JOIN ACCOUNTS          ON CD_ACCOUNT_ID   = AC_ACCOUNT_ID
	      INNER JOIN TERMINALS   ON (CASE WHEN CD_TERMINAL IS NOT NULL THEN CD_TERMINAL ELSE @_Sorter_terminal_ID END) = TE_TERMINAL_ID
	      LEFT JOIN ACCOUNT_OPERATIONS AC ON AC.AO_OPERATION_ID = CD_OPERATION_ID
  	    LEFT JOIN ACCOUNT_PROMOTIONS AP ON AP.ACP_OPERATION_ID = CD_OPERATION_ID AND AP.ACP_PROMO_TYPE IN (@_Promotion_Redeemeable, @_Promotion_NoRedeemeable)
  	  
	  WHERE     
		  CD_DRAW_DATETIME >= @pDrawFrom
	     AND 		CD_DRAW_DATETIME < @pDrawTo	     
	     AND 		((@pCashierId IS NULL) OR (CM_CASHIER_ID = @pCashierId))
	     AND 		((@pUserId IS NULL) OR (CM_USER_ID = @pUserId))
	     AND    te_terminal_type = @_Terminal_draw_type
	  ORDER BY CD_DRAW_ID
	  DROP TABLE #ACCOUNTS_TEMP
	  RETURN
	END
end

GO

GRANT EXECUTE ON [dbo].[ReportTerminalDraws] TO [wggui] WITH GRANT OPTION 

GO

IF OBJECT_ID('sp_CheckCashDeskDrawConfig', 'P') IS NOT NULL
DROP PROC sp_CheckCashDeskDrawConfig
GO
create PROCEDURE [dbo].[sp_CheckCashDeskDrawConfig]
      -- Add the parameters for the stored procedure here
      @pID INT = 0,
      @pName VARCHAR(20) = ''
AS
BEGIN
SET NOCOUNT ON;

DELETE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CashDesk.Draw.00'
DELETE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration.00'
   
DECLARE @POSTFIJ AS VARCHAR(5)

IF @pID = 0
BEGIN
  SET @POSTFIJ = ''
END
ELSE 
BEGIN 
  SET @POSTFIJ = '.' + RIGHT( '00' + CONVERT(VARCHAR(2),@pID),2)
END


  --'CashDesk.Draw', 'Enabled', 0
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ
                        AND GP_SUBJECT_KEY = 'Enabled'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'Enabled', '0')
END


--'CashDesk.Draw', 'ShowCashDeskDraws', 0
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ
                        AND GP_SUBJECT_KEY = 'ShowCashDeskDraws'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'ShowCashDeskDraws', '0')
END


--'CashDesk.Draw', 'VoucherOnCashDeskDraws.Winner', 0
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'VoucherOnCashDeskDraws.Winner'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'VoucherOnCashDeskDraws.Winner', '0')
END


--'CashDesk.Draw', 'WinnerPrizeVoucherTitle', 'Sorteo en XXXX'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrizeVoucherTitle'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'WinnerPrizeVoucherTitle', 'Sorteo en ' + @pName)
END


--'CashDesk.Draw', 'VoucherOnCashDeskDraws.Loser', 0
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'VoucherOnCashDeskDraws.Loser'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'VoucherOnCashDeskDraws.Loser', '0')
END


--'CashDesk.Draw', 'AskForParticipation', 0
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'AskForParticipation'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'AskForParticipation', '0')
END


--'CashDesk.Draw', 'LoserPrizeVoucherTitle','Cortesía sorteo en XXXX'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'LoserPrizeVoucherTitle'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'LoserPrizeVoucherTitle', 'Cortesía sorteo en ' + @pName)
END


--'CashDesk.Draw', 'ActionOnServerDown','0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'ActionOnServerDown'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'ActionOnServerDown', '0')
END


--'CashDesk.Draw', 'LocalServer', '1'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'LocalServer'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'LocalServer', '1')
END


--'CashDesk.Draw', 'ServerBDConnectionString',''
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'ServerBDConnectionString'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'ServerBDConnectionString', '')
END


--'CashDesk.Draw', 'ServerAddress1',''
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'ServerAddress1'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'ServerAddress1', '')
END


--'CashDesk.Draw', 'ServerAddress2',''
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'ServerAddress2'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'ServerAddress2', '')
END


--'CashDesk.DrawConfiguration', 'BallsExtracted','0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'BallsExtracted'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'BallsExtracted', '0')
END


--'CashDesk.DrawConfiguration', 'BallsOfParticipant', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'BallsOfParticipant'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'BallsOfParticipant', '0')
END


--'CashDesk.DrawConfiguration', 'TotalsBallsNumber', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'TotalsBallsNumber'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'TotalsBallsNumber', '0')
END


--'CashDesk.DrawConfiguration', 'LoserPrize1.Fixed', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'LoserPrize1.Fixed'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'LoserPrize1.Fixed', '0')
END


--'CashDesk.DrawConfiguration', 'LoserPrize1.Percentage', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'LoserPrize1.Percentage'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'LoserPrize1.Percentage', '0')
END


--'CashDesk.DrawConfiguration', 'LoserPrize1.Enabled', 0
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'LoserPrize1.Enabled'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'LoserPrize1.Enabled', '0')
END


--'CashDesk.DrawConfiguration', 'NumberOfParticipants', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'NumberOfParticipants'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'NumberOfParticipants', '0')
END


--'CashDesk.DrawConfiguration', 'NumberOfWinners', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'NumberOfWinners'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'NumberOfWinners', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize1.Fixed', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize1.Fixed'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize1.Fixed', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize1.Percentage', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize1.Percentage'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize1.Percentage', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize1.Enabled', 0
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize1.Enabled'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize1.Enabled', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize2.Fixed', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize2.Fixed'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize2.Fixed', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize2.Percentage', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize2.Percentage'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize2.Percentage', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize2.Enabled', 0
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize2.Enabled'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize2.Enabled', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize3.Fixed', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize3.Fixed'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize3.Fixed', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize3.Percentage', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize3.Percentage'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize3.Percentage', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize3.Enabled', 0
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize3.Enabled'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize3.Enabled', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize4.Fixed', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize4.Fixed'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize4.Fixed', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize4.Percentage', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize4.Percentage'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize4.Percentage', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize4.Enabled', 0
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize4.Enabled'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize4.Enabled', '0')
END



--'CashDesk.DrawConfiguration', 'WinnerPrize1.PrizeType', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize1.PrizeType'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize1.PrizeType', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize2.PrizeType', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize2.PrizeType'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize2.PrizeType','0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize3.PrizeType', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize3.PrizeType'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize3.PrizeType', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize4.PrizeType', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize4.PrizeType'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize4.PrizeType', '0')
END


--'CashDesk.DrawConfiguration', 'LoserPrize1.PrizeType', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'LoserPrize1.PrizeType'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'LoserPrize1.PrizeType', '0')
END


-- 'CashDesk.Draw', 'AccountingMode', ''
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'AccountingMode'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'AccountingMode', '0')
END


-- 'CashDesk.DrawConfiguration', 'ParticipationPrice', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'ParticipationPrice'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'ParticipationPrice', '0')
END   
      
      
-- 'CashDesk.Draw', 'IsCashDeskDraw', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'IsCashDeskDraw'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'IsCashDeskDraw', '0')
END         


-- 'CashDesk.Draw', 'ReportUNRFromSP_SalesAndPayment', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'ReportUNRFromSP_SalesAndPayment'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'ReportUNRFromSP_SalesAndPayment', '0')
END   


-- 'CashDesk.Draw', 'AccountingMode.PromoNameMovement', 'PROMOCIÓN'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'AccountingMode.PromoNameMovement'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'AccountingMode.PromoNameMovement', 'Promoción')
END   

-- 'CashDesk.Draw', 'AccountingMode.KindOfNameMovement', 'Premio en especie'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'AccountingMode.KindOfNameMovement'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'AccountingMode.KindOfNameMovement', 'Premio en especie')
END   

-- 'CashDesk.Draw', 'AccountingMode.RE.KindOfNameMovement', 'Premio en especie (RE)'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'AccountingMode.RE.KindOfNameMovement'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'AccountingMode.RE.KindOfNameMovement', 'Premio en especie (RE)')
END   

-- 'CashDesk.Draw', 'AccountingMode.RE.PromoNameMovement', 'Promoción (RE)'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'AccountingMode.RE.PromoNameMovement'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'AccountingMode.RE.PromoNameMovement', 'Promoción (RE)')
END   


-- 'CashDesk.Draw', 'Voucher.HideCurrencySymbol', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'Voucher.HideCurrencySymbol'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'Voucher.HideCurrencySymbol', '0')
END   
      
-- 'CashDesk.Draw', 'Voucher.LoserPrizeLabel', 'Cortesia'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'Voucher.LoserPrizeLabel'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'Voucher.LoserPrizeLabel', 'Cortesia')
END   
      
-- 'CashDesk.Draw', 'Voucher.UNRLabel', 'FD'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'Voucher.UNRLabel'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'Voucher.UNRLabel', 'FD')
END   

-- 'CashDesk.Draw', 'Voucher.WinnerPrizeLabel', 'Premio'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'Voucher.WinnerPrizeLabel'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'Voucher.WinnerPrizeLabel', 'Premio')
END   



--/// Probability


IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'LoserPrize1.Probability'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'LoserPrize1.Probability', '0')
END

IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize1.Probability'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize1.Probability', '0')
END

IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize2.Probability'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize2.Probability', '0')
END

IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize3.Probability'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize3.Probability', '0')
END

IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'WinnerPrize4.Probability'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize4.Probability', '0')
END


IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'ParticipationMaxPrice'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'ParticipationMaxPrice', '1')
END



IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameUrl'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameUrl', '')
END



IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameTimeout'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameTimeout', '30')
END

IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameFirsIfTimeOutExpiresParticipateInDraw'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameFirsIfTimeOutExpiresParticipateInDraw', '0')
END


IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ
                        AND GP_SUBJECT_KEY = 'TerminalGameFirstDrawScreenForceParticipateInDraw'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameFirstDrawScreenForceParticipateInDraw', '0')
END

IF EXISTS(SELECT 1 
            FROM GENERAL_PARAMS 
           WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration.02' 
             AND GP_SUBJECT_KEY = 'IfTimeOutExpiresParticipateInDraw')
BEGIN
  UPDATE GENERAL_PARAMS 
     SET GP_SUBJECT_KEY = 'TerminalGameFirsIfTimeOutExpiresParticipateInDraw'
   WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration.02' 
     AND GP_SUBJECT_KEY = 'IfTimeOutExpiresParticipateInDraw'

END
      
END

GO

GRANT EXECUTE ON [dbo].[sp_CheckCashDeskDrawConfig] TO [wggui] WITH GRANT OPTION

GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_AFIP_GAMING_TABLES]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_AFIP_GAMING_TABLES]

GO

CREATE PROCEDURE [dbo].[SP_AFIP_GAMING_TABLES]
(
  @pStarDateTime    DATETIME
)
AS
BEGIN
DECLARE @Columns                           AS VARCHAR(MAX)	                                                                          
DECLARE @NationalCurrency                  as VARCHAR(5)  
DECLARE @pCageChipColor                    AS VARCHAR(3)-- X02
DECLARE @_MOVEMENT_CLOSE_SESSION           as integer
DECLARE @_Currency_ISO_Code                AS  VARCHAR(5);
DECLARE @_Default_Cashier_Afip_Type        AS  INT;
DECLARE @_old_chips_iso_code               AS  VARCHAR(3);
DECLARE @_opening_cash 					           AS INT;	
DECLARE @_cage_close_session               AS INT;
DECLARE @_cage_filler_in                   AS INT;
DECLARE @_cage_filler_out                  AS INT;
DECLARE @_chips_sale                       AS INT;
DECLARE @_chips_purchase                   AS INT;
DECLARE @_chips_sale_devolution_for_tito   AS INT;
DECLARE @_chips_sale_with_cash_in          AS INT;
DECLARE @_chips_sale_register_total        AS INT;
DECLARE @_chips_redeemable                 AS VARCHAR(5);
DECLARE @_GUI_User_Type_SYS_GamingTables   AS  INT;  
DECLARE @_GUI_User_Type_SYS_TITO_User      AS  INT;  
DECLARE @_GUI_User_Type_SYS_SYSTEM         AS  INT;  
DECLARE @_filler_in_closing_stock          AS  INT;  
DECLARE @_filler_out_closing_stock         AS  INT;  

SET @_GUI_User_Type_SYS_SYSTEM         = 3;  
SET @_GUI_User_Type_SYS_TITO_User      = 4;  
SET @_GUI_User_Type_SYS_GamingTables   = 5; 
SET @_opening_cash 					           = 0   	
SET @_filler_in_closing_stock          = 190  
SET @_filler_out_closing_stock         = 191  
SET @_cage_close_session               = 201 
SET @_cage_filler_in                   = 202 
SET @_cage_filler_out                  = 203 
SET @_chips_sale                       = 300 
SET @_chips_purchase                   = 301 
SET @_chips_sale_devolution_for_tito   = 302 
SET @_chips_sale_with_cash_in          = 307 
SET @_chips_sale_register_total        = 311 
SET @_chips_redeemable                 = '1001'
SET @pCageChipColor                    = 'X02'
SET @_MOVEMENT_CLOSE_SESSION           = 1 



SET @NationalCurrency = (SELECT GP_KEY_VALUE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'RegionalOptions' AND GP_SUBJECT_KEY = 'CurrencyISOCode')
           	                                                                                                          
    SELECT   CS_SESSION_ID
           , CS_USER_ID
           , MAX(CM_DATE)							      AS DATE_MOV                                                     
           , CM_TYPE									      AS TYPE_MOV                                                    
           , ISNULL(CCMR.CGM_MOVEMENT_ID,0)	AS ID_MOV 
           , CASE CM_TYPE
           WHEN @_cage_filler_in THEN SUM(CM_ADD_AMOUNT)
           WHEN @_cage_filler_out THEN SUM(CM_SUB_AMOUNT)
           WHEN @_cage_close_session THEN SUM(CM_FINAL_BALANCE)
           WHEN @_filler_in_closing_stock THEN SUM(CM_ADD_AMOUNT)
           WHEN @_filler_out_closing_stock  THEN SUM(CM_SUB_AMOUNT)
           WHEN @_chips_sale THEN SUM(CM_SUB_AMOUNT)
           WHEN @_chips_sale_devolution_for_tito THEN SUM(CM_SUB_AMOUNT)
           WHEN @_chips_sale_with_cash_in  THEN SUM(CM_SUB_AMOUNT)
           WHEN @_chips_sale_register_total THEN SUM(CM_SUB_AMOUNT)
           WHEN @_chips_purchase THEN SUM(CM_ADD_AMOUNT)
           ELSE 0 END
            AS  AMOUNT                                                                   
           ,  CASE ISNULL(CM_CURRENCY_ISO_CODE,'') WHEN '' THEN @NationalCurrency                          
              WHEN @pCageChipColor THEN ''                                                      
              ELSE CM.CM_CURRENCY_ISO_CODE END AS CURRENCY_ISO_CODE                                        
           ,  CM.CM_CAGE_CURRENCY_TYPE AS CURRENCY_TYPE                                                    
           ,  0 OPENER_MOV                                                                                 
           , CM.CM_OPERATION_ID
       , CS_OPENING_DATE
       , CS_CLOSING_DATE
      INTO   #TABLE_MOVEMENTS	                                                                             
      FROM   CASHIER_MOVEMENTS   AS CM
INNER JOIN   CASHIER_SESSIONS         ON CM_SESSION_ID = CS_SESSION_ID
 LEFT JOIN   CAGE_CASHIER_MOVEMENT_RELATION AS CCMR ON CCMR.CM_MOVEMENT_ID = CM.CM_MOVEMENT_ID
INNER JOIN   GUI_USERS          ON CS_USER_ID    = GU_USER_ID          
     WHERE   CS_CLOSING_DATE >= @pStarDateTime
       AND   CM_TYPE IN (@_opening_cash,                                                                          
 						             @_cage_filler_in,                                                              
 						             @_cage_filler_out,	                                                          
 						             @_cage_close_session,
 						             @_filler_out_closing_stock ,	                                                              
 						             @_chips_sale, 
									       @_chips_purchase, 
									       @_chips_sale_devolution_for_tito, 
									       @_chips_sale_with_cash_in, 
									       @_chips_sale_register_total)      	                                                                          
  GROUP BY   CS_SESSION_ID
           , CS_USER_ID
                     , CM_TYPE                         	                                                              
                     , CCMR.CGM_MOVEMENT_ID                    	                                              
                     , CM.CM_CURRENCY_ISO_CODE	                                                                      
                     , CM.CM_CAGE_CURRENCY_TYPE	                                                                      
                     , CM.CM_OPERATION_ID
           , CS_OPENING_DATE
           , CS_CLOSING_DATE
            ORDER BY   MAX(CM_DATE) DESC                                                                                                                                           
            
           	                                                                                                        
DECLARE @cursor_id_mov AS INT
    
DECLARE _cursor CURSOR  
FOR SELECT MIN(ID_MOV) FROM #TABLE_MOVEMENTS WHERE TYPE_MOV IN (@_cage_filler_in , @_filler_in_closing_stock)  GROUP BY CS_SESSION_ID
      OPEN _cursor  
      FETCH NEXT FROM _cursor INTO @cursor_id_mov
      WHILE @@FETCH_STATUS = 0  
        BEGIN    
            UPDATE #TABLE_MOVEMENTS
            SET OPENER_MOV = 1
            WHERE ID_MOV = @cursor_id_mov AND TYPE_MOV IN (@_cage_filler_in , @_filler_in_closing_stock)

            FETCH NEXT FROM _cursor INTO @cursor_id_mov 
        END   
CLOSE _cursor;  
DEALLOCATE _cursor;  

      SELECT GT_GAMING_TABLE_ID  AS TABLE_ID
           , ISNULL(GT_NAME, GU_FULL_NAME) AS TABLE_NAME
           , ISNULL(GTT_GAMING_TABLE_TYPE_ID, 99) AS GTABLE_TYPE_ID
           , ISNULL(GTT_NAME, 'OTROS') AS GTABLE_TYPE_DESC
           , SUM(case when OPENER_MOV=1 and currency_type =0 and currency_iso_code = @NationalCurrency then amount else 0 end ) as OPENING_CASH
           , SUM(case when TYPE_MOV in (@_filler_out_closing_stock,@_cage_close_session) and currency_type =0 and currency_iso_code = @NationalCurrency then amount else 0 end) as CLOSING_CASH
           , SUM(case when OPENER_MOV=1 and currency_type =@_chips_redeemable and currency_iso_code = @NationalCurrency then amount else 0 end) as OPENING_CHIPS
           , SUM(case when TYPE_MOV in (@_filler_out_closing_stock,@_cage_close_session) and currency_type =@_chips_redeemable and currency_iso_code = @NationalCurrency then amount else 0 end) as CLOSING_CHIPS
           , SUM(case when TYPE_MOV in (@_cage_filler_out) and currency_type =0 and currency_iso_code = @NationalCurrency then amount else 0 end) as CASH_OUT_TOTAL
           , SUM(case when OPENER_MOV=0 AND TYPE_MOV in (@_cage_filler_in) and currency_type =0 and currency_iso_code = @NationalCurrency then amount else 0 end) as CASH_IN_TOTAL
           , SUM(case when TYPE_MOV in (@_cage_filler_out) and currency_type =@_chips_redeemable and currency_iso_code = @NationalCurrency then amount else 0 end) as CHIPS_OUT_TOTAL
           , SUM(case when OPENER_MOV=0 AND TYPE_MOV in (@_cage_filler_in) and currency_type =@_chips_redeemable and currency_iso_code = @NationalCurrency then amount else 0 end) as CHIPS_IN_TOTAL
           , SUM(case when TYPE_MOV in (@_chips_sale, @_chips_sale_devolution_for_tito, @_chips_sale_with_cash_in, @_chips_sale_register_total) and currency_type = @_chips_redeemable and currency_iso_code = @NationalCurrency then amount else 0 end) as SALE_CHIPS_TOTAL
           , SUM(case when TYPE_MOV in (@_chips_purchase) and currency_type = @_chips_redeemable and currency_iso_code = @NationalCurrency then amount else 0 end) as BUY_CHIPS_TOTAL
      FROM   #TABLE_MOVEMENTS   AS CM
 LEFT JOIN   GAMING_TABLES_SESSIONS ON CS_SESSION_ID = GTS_CASHIER_SESSION_ID
 LEFT JOIN   gaming_tables on gts_gaming_table_id = gt_gaming_table_id
 LEFT JOIN   GAMING_TABLES_TYPES ON GTT_GAMING_TABLE_TYPE_ID = GT_TYPE_ID
INNER JOIN   GUI_USERS           ON CS_USER_ID    = GU_USER_ID   
     WHERE   DATE_MOV >= @pStarDateTime
       AND   GU_USER_TYPE NOT IN (@_GUI_User_Type_SYS_SYSTEM,@_GUI_User_Type_SYS_TITO_User) 
  group by   GT_GAMING_TABLE_ID
           , ISNULL(GT_NAME, GU_FULL_NAME)
           , ISNULL(gtt_gaming_table_type_id, 99) 
           , ISNULL(gtt_name, 'OTROS') 
  ORDER BY   MAX(DATE_MOV) DESC 

DROP TABLE #TABLE_MOVEMENTS	

END  -- End procedure SP_AFIP_GAMING_TABLES  -- End procedure SP_AFIP_GAMING_TABLES
GO

GRANT EXECUTE ON [dbo].[SP_AFIP_GAMING_TABLES] TO [WGGUI] WITH GRANT OPTION

GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_AFIP_GAMING_TABLES_TYPE]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[SP_AFIP_GAMING_TABLES_TYPE]
GO

CREATE PROCEDURE [dbo].[SP_AFIP_GAMING_TABLES_TYPE]
(
  @pStarDateTime    DATETIME
)
AS
BEGIN
  CREATE TABLE #GamingTable
  (
       TABLE_ID          INT
     , TABLE_NAME        VARCHAR(250)
     , ID_TABLE_TYPE     INT
     , DESC_TABLE_TYPE   VARCHAR(250)
     , OPENING_CASH      MONEY
     , CLOSING_CASH      MONEY
     , OPENING_CHIPS     MONEY
     , CLOSING_CHIPS     MONEY
     , CASH_OUT_TOTAL    MONEY
     , CASH_IN_TOTAL     MONEY
     , CHIPS_OUT_TOTAL   MONEY
     , CHIPS_IN_TOTAL    MONEY
     , CHIP_SALES_TOTAL  MONEY
     , CHIPS_BUY_TOTAL   MONEY
  )

  INSERT  #GamingTable EXEC SP_AFIP_GAMING_TABLES @pStarDateTime

  SELECT ID_TABLE_TYPE
       , DESC_TABLE_TYPE
       , COUNT(ID_TABLE_TYPE) AS TABLE_COUNT
       , SUM(OPENING_CASH) AS OPENING_CASH
       , SUM(CLOSING_CASH) AS CLOSING_CASH
       , SUM(OPENING_CHIPS) AS OPENING_CHIPS
       , SUM(CLOSING_CHIPS) AS CLOSING_CHIPS
       , SUM(CASH_OUT_TOTAL) AS CASH_OUT_TOTAL
       , SUM(CASH_IN_TOTAL) AS CASH_IN_TOTAL
       , SUM(CHIPS_OUT_TOTAL) AS CHIPS_OUT_TOTAL
       , SUM(CHIPS_IN_TOTAL) AS CHIPS_IN_TOTAL
       , SUM(CHIP_SALES_TOTAL) AS CHIP_SALES_TOTAL
       , SUM(CHIPS_BUY_TOTAL) AS CHIPS_BUY_TOTAL
  FROM #GamingTable
  GROUP BY ID_TABLE_TYPE, DESC_TABLE_TYPE

  DROP TABLE #GamingTable
END  -- END procedure SP_AFIP_GAMING_TABLES_TYPE

GO

GRANT EXECUTE ON [dbo].[SP_AFIP_GAMING_TABLES_TYPE] TO [WGGUI] WITH GRANT OPTION

GO

IF OBJECT_ID('sp_CheckCashDeskDrawConfig', 'P') IS NOT NULL
DROP PROC sp_CheckCashDeskDrawConfig
GO
create PROCEDURE [dbo].[sp_CheckCashDeskDrawConfig]
      -- Add the parameters for the stored procedure here
      @pID INT = 0,
      @pName VARCHAR(20) = ''
AS
BEGIN
SET NOCOUNT ON;

DELETE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CashDesk.Draw.00'
DELETE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration.00'
   
DECLARE @POSTFIJ AS VARCHAR(5)

IF @pID = 0
BEGIN
  SET @POSTFIJ = ''
END
ELSE 
BEGIN 
  SET @POSTFIJ = '.' + RIGHT( '00' + CONVERT(VARCHAR(2),@pID),2)
END


  --'CashDesk.Draw', 'Enabled', 0
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ
                        AND GP_SUBJECT_KEY = 'Enabled'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'Enabled', '0')
END


--'CashDesk.Draw', 'ShowCashDeskDraws', 0
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ
                        AND GP_SUBJECT_KEY = 'ShowCashDeskDraws'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'ShowCashDeskDraws', '0')
END


--'CashDesk.Draw', 'VoucherOnCashDeskDraws.Winner', 0
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'VoucherOnCashDeskDraws.Winner'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'VoucherOnCashDeskDraws.Winner', '0')
END


--'CashDesk.Draw', 'WinnerPrizeVoucherTitle', 'Sorteo en XXXX'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrizeVoucherTitle'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'WinnerPrizeVoucherTitle', 'Sorteo en ' + @pName)
END


--'CashDesk.Draw', 'VoucherOnCashDeskDraws.Loser', 0
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'VoucherOnCashDeskDraws.Loser'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'VoucherOnCashDeskDraws.Loser', '0')
END


--'CashDesk.Draw', 'AskForParticipation', 0
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'AskForParticipation'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'AskForParticipation', '0')
END


--'CashDesk.Draw', 'LoserPrizeVoucherTitle','Cortesía sorteo en XXXX'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'LoserPrizeVoucherTitle'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'LoserPrizeVoucherTitle', 'Cortesía sorteo en ' + @pName)
END


--'CashDesk.Draw', 'ActionOnServerDown','0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'ActionOnServerDown'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'ActionOnServerDown', '0')
END


--'CashDesk.Draw', 'LocalServer', '1'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'LocalServer'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'LocalServer', '1')
END


--'CashDesk.Draw', 'ServerBDConnectionString',''
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'ServerBDConnectionString'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'ServerBDConnectionString', '')
END


--'CashDesk.Draw', 'ServerAddress1',''
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'ServerAddress1'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'ServerAddress1', '')
END


--'CashDesk.Draw', 'ServerAddress2',''
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'ServerAddress2'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'ServerAddress2', '')
END


--'CashDesk.DrawConfiguration', 'BallsExtracted','0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'BallsExtracted'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'BallsExtracted', '0')
END


--'CashDesk.DrawConfiguration', 'BallsOfParticipant', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'BallsOfParticipant'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'BallsOfParticipant', '0')
END


--'CashDesk.DrawConfiguration', 'TotalsBallsNumber', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'TotalsBallsNumber'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'TotalsBallsNumber', '0')
END


--'CashDesk.DrawConfiguration', 'LoserPrize1.Fixed', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'LoserPrize1.Fixed'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'LoserPrize1.Fixed', '0')
END


--'CashDesk.DrawConfiguration', 'LoserPrize1.Percentage', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'LoserPrize1.Percentage'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'LoserPrize1.Percentage', '0')
END


--'CashDesk.DrawConfiguration', 'LoserPrize1.Enabled', 0
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'LoserPrize1.Enabled'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'LoserPrize1.Enabled', '0')
END


--'CashDesk.DrawConfiguration', 'NumberOfParticipants', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'NumberOfParticipants'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'NumberOfParticipants', '0')
END


--'CashDesk.DrawConfiguration', 'NumberOfWinners', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'NumberOfWinners'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'NumberOfWinners', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize1.Fixed', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize1.Fixed'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize1.Fixed', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize1.Percentage', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize1.Percentage'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize1.Percentage', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize1.Enabled', 0
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize1.Enabled'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize1.Enabled', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize2.Fixed', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize2.Fixed'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize2.Fixed', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize2.Percentage', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize2.Percentage'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize2.Percentage', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize2.Enabled', 0
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize2.Enabled'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize2.Enabled', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize3.Fixed', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize3.Fixed'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize3.Fixed', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize3.Percentage', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize3.Percentage'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize3.Percentage', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize3.Enabled', 0
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize3.Enabled'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize3.Enabled', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize4.Fixed', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize4.Fixed'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize4.Fixed', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize4.Percentage', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize4.Percentage'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize4.Percentage', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize4.Enabled', 0
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize4.Enabled'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize4.Enabled', '0')
END



--'CashDesk.DrawConfiguration', 'WinnerPrize1.PrizeType', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize1.PrizeType'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize1.PrizeType', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize2.PrizeType', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize2.PrizeType'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize2.PrizeType','0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize3.PrizeType', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize3.PrizeType'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize3.PrizeType', '0')
END


--'CashDesk.DrawConfiguration', 'WinnerPrize4.PrizeType', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize4.PrizeType'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize4.PrizeType', '0')
END


--'CashDesk.DrawConfiguration', 'LoserPrize1.PrizeType', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'LoserPrize1.PrizeType'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'LoserPrize1.PrizeType', '0')
END


-- 'CashDesk.Draw', 'AccountingMode', ''
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'AccountingMode'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'AccountingMode', '0')
END


-- 'CashDesk.DrawConfiguration', 'ParticipationPrice', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'ParticipationPrice'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.DrawConfiguration' + @POSTFIJ, 'ParticipationPrice', '0')
END   
      
      
-- 'CashDesk.Draw', 'IsCashDeskDraw', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'IsCashDeskDraw'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'IsCashDeskDraw', '0')
END         


-- 'CashDesk.Draw', 'ReportUNRFromSP_SalesAndPayment', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'ReportUNRFromSP_SalesAndPayment'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'ReportUNRFromSP_SalesAndPayment', '0')
END   


-- 'CashDesk.Draw', 'AccountingMode.PromoNameMovement', 'PROMOCIÓN'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'AccountingMode.PromoNameMovement'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'AccountingMode.PromoNameMovement', 'Promoción')
END   

-- 'CashDesk.Draw', 'AccountingMode.KindOfNameMovement', 'Premio en especie'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'AccountingMode.KindOfNameMovement'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'AccountingMode.KindOfNameMovement', 'Premio en especie')
END   

-- 'CashDesk.Draw', 'AccountingMode.RE.KindOfNameMovement', 'Premio en especie (RE)'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'AccountingMode.RE.KindOfNameMovement'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'AccountingMode.RE.KindOfNameMovement', 'Premio en especie (RE)')
END   

-- 'CashDesk.Draw', 'AccountingMode.RE.PromoNameMovement', 'Promoción (RE)'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'AccountingMode.RE.PromoNameMovement'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'AccountingMode.RE.PromoNameMovement', 'Promoción (RE)')
END   


-- 'CashDesk.Draw', 'Voucher.HideCurrencySymbol', '0'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'Voucher.HideCurrencySymbol'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'Voucher.HideCurrencySymbol', '0')
END   
      
-- 'CashDesk.Draw', 'Voucher.LoserPrizeLabel', 'Cortesia'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'Voucher.LoserPrizeLabel'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'Voucher.LoserPrizeLabel', 'Cortesia')
END   
      
-- 'CashDesk.Draw', 'Voucher.UNRLabel', 'FD'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'Voucher.UNRLabel'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'Voucher.UNRLabel', 'FD')
END   

-- 'CashDesk.Draw', 'Voucher.WinnerPrizeLabel', 'Premio'
IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.Draw' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'Voucher.WinnerPrizeLabel'
                  )
BEGIN
      INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
      VALUES ('CashDesk.Draw' + @POSTFIJ, 'Voucher.WinnerPrizeLabel', 'Premio')
END   



--/// Probability


IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'LoserPrize1.Probability'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'LoserPrize1.Probability', '0')
END

IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize1.Probability'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize1.Probability', '0')
END

IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize2.Probability'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize2.Probability', '0')
END

IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ  
                        AND GP_SUBJECT_KEY = 'WinnerPrize3.Probability'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize3.Probability', '0')
END

IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'WinnerPrize4.Probability'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'WinnerPrize4.Probability', '0')
END


IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'ParticipationMaxPrice'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'ParticipationMaxPrice', '1')
END



IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameUrl'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameUrl', '')
END



IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameTimeout'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameTimeout', '30')
END

IF NOT EXISTS(SELECT 1 
                        FROM GENERAL_PARAMS 
                        WHERE GP_GROUP_KEY = 'CashDesk.DrawConfiguration' + @POSTFIJ 
                        AND GP_SUBJECT_KEY = 'TerminalGameName'
                  )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('CashDesk.DrawConfiguration' + @POSTFIJ, 'TerminalGameName', 'Terminal Draw')
END

      
END
GO

GRANT EXECUTE ON [dbo].[sp_CheckCashDeskDrawConfig] TO [wggui]
GO

/******* TRIGGERS *******/
