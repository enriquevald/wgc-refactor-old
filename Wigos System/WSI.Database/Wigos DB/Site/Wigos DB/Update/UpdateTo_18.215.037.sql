/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 214;

SET @New_ReleaseId = 215;
SET @New_ScriptName = N'UpdateTo_18.215.037.sql';
SET @New_Description = N'Update alarm catalogs';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/******* TABLES *******/

/******* INDEXES *******/

/******* RECORDS *******/

IF (NOT EXISTS (SELECT * FROM alarm_catalog WHERE alcg_alarm_code=65809))
BEGIN
  INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (65809, 0, N'Err. al obtener los contadores', N'Error al obtener los contadores', 1)
  INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (65809, 1, 0, GETDATE())
END
GO

IF (NOT EXISTS (SELECT * FROM alarm_catalog WHERE alcg_alarm_code=65810))
BEGIN
  INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (65810, 0, N'Err. al obtener el contador de Pagos Manuales', N'Error al obtener el contador de Pagos Manuales', 1)
  INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (65810, 1, 0, GETDATE())
END
GO

IF (NOT EXISTS (SELECT * FROM alarm_catalog WHERE alcg_alarm_code=65811))
BEGIN
  INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (65811, 0, N'Err. al obtener los contadores de juego', N'Error al obtener los contadores de juego', 1)
  INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (65811, 1, 0, GETDATE())
END
GO

IF (NOT EXISTS (SELECT * FROM alarm_catalog WHERE alcg_alarm_code=65812))
BEGIN
  INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (65812, 0, N'Err. al obtener los contadores extendidos', N'Error al obtener los contadores extendidos', 1)
  INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (65812, 1, 0, GETDATE())
END
GO

IF (NOT EXISTS (SELECT * FROM alarm_catalog WHERE alcg_alarm_code=65826))
BEGIN
  INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (65826, 0, N'Err. recurrente en contador de Pagos Manuales', N'Error recurrente al obtener el contador de Pagos Manuales', 1)
  INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (65826, 1, 0, GETDATE())
END
GO

UPDATE ALARM_CATALOG SET ALCG_NAME='Err. al obtener contador ext. de Pagos Manuales',ALCG_DESCRIPTION='Error al obtener el contador extendido de Pagos Manuales' WHERE ALCG_ALARM_CODE=65813   
UPDATE ALARM_CATALOG SET ALCG_NAME='Err. al obtener contadores extendidos de juego',ALCG_DESCRIPTION='Error al obtener contadores extendidos de juego' WHERE ALCG_ALARM_CODE=65814   
UPDATE ALARM_CATALOG SET ALCG_NAME='Err. recurrente en los contadores',ALCG_DESCRIPTION='Error recurrente al obtener los contadores' WHERE ALCG_ALARM_CODE=65825   
UPDATE ALARM_CATALOG SET ALCG_NAME='Err. recurrente en los contadores de juego',ALCG_DESCRIPTION='Error recurrente al obtener los contadores de juego' WHERE ALCG_ALARM_CODE=65827   
UPDATE ALARM_CATALOG SET ALCG_NAME='Err. recurrente en los contadores extendidos',ALCG_DESCRIPTION='Error recurrente al obtener los contadores extendidos' WHERE ALCG_ALARM_CODE=65828   
UPDATE ALARM_CATALOG SET ALCG_NAME='Err. recurrente en el contador ext. de P. Manuales',ALCG_DESCRIPTION='Error recurrente al obtener el contador extendido de Pagos Manuales' WHERE ALCG_ALARM_CODE=65829   
UPDATE ALARM_CATALOG SET ALCG_NAME='Err. recurrente en los contadores ext. de juego',ALCG_DESCRIPTION='Error recurrente al obtener los contadores extendidos de juego' WHERE ALCG_ALARM_CODE=65830   
GO

/******* STORED PROCEDURES *******/

