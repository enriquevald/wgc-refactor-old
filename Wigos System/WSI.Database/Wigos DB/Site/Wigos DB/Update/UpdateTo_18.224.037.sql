/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 223;

SET @New_ReleaseId = 224;
SET @New_ScriptName = N'UpdateTo_18.224.037.sql';
SET @New_Description = N'LCD table and records';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/******* TABLES *******/

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.lcd_images') AND type in (N'U'))
BEGIN
  CREATE TABLE [dbo].[lcd_images](
	  [cim_image_id] [int] NOT NULL,
	  [cim_name] [nvarchar](50) NOT NULL,
	  [cim_resource_id] [bigint] NOT NULL,
	  [cim_expected_w] [int] NOT NULL,
	  [cim_expected_h] [int] NOT NULL,
   CONSTRAINT [PK_LCD_images] PRIMARY KEY CLUSTERED 
  (
	  [cim_image_id] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
  ) ON [PRIMARY]
END

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.lcd_functionalities') AND type in (N'U'))
BEGIN
  CREATE TABLE [dbo].[lcd_functionalities](
	  [fun_function_id] [int] NOT NULL,
	  [fun_name] [nvarchar](50) NOT NULL,
	  [fun_enabled] [int] NOT NULL,
   CONSTRAINT [PK_LCD_functionalities] PRIMARY KEY CLUSTERED 
  (
	  [fun_function_id] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
  ) ON [PRIMARY]
END

/******* INDEXES *******/

/******* RECORDS *******/

IF NOT EXISTS ( SELECT 1 FROM LCD_IMAGES WHERE CIM_IMAGE_ID =  0)
  INSERT INTO LCD_IMAGES (cim_image_id, cim_name, cim_resource_id, cim_expected_w, cim_expected_h) 
				  VALUES (0,			'IMG00',  0,			   0,			   0)
GO
IF NOT EXISTS ( SELECT 1 FROM LCD_IMAGES WHERE CIM_IMAGE_ID =  1)
  INSERT INTO LCD_IMAGES (cim_image_id, cim_name, cim_resource_id, cim_expected_w, cim_expected_h) 
				  VALUES (1,			'IMG01',  0,			   0,			   0)
GO

IF NOT EXISTS ( SELECT 1 FROM LCD_FUNCTIONALITIES WHERE FUN_FUNCTION_ID =  1)
  INSERT INTO LCD_FUNCTIONALITIES (fun_function_id, FUN_NAME, FUN_ENABLED) VALUES (1, 'ACCOUNT INFO', 0)
GO
IF NOT EXISTS ( SELECT 1 FROM LCD_FUNCTIONALITIES WHERE FUN_FUNCTION_ID =  2)
  INSERT INTO LCD_FUNCTIONALITIES (fun_function_id, FUN_NAME, FUN_ENABLED) VALUES (2, 'RAFFLES', 0)
GO
IF NOT EXISTS ( SELECT 1 FROM LCD_FUNCTIONALITIES WHERE FUN_FUNCTION_ID =  3)
  INSERT INTO LCD_FUNCTIONALITIES (fun_function_id, FUN_NAME, FUN_ENABLED) VALUES (3, 'PROMOTIONS', 0)
GO
IF NOT EXISTS ( SELECT 1 FROM LCD_FUNCTIONALITIES WHERE FUN_FUNCTION_ID =  4)
  INSERT INTO LCD_FUNCTIONALITIES (fun_function_id, FUN_NAME, FUN_ENABLED) VALUES (4, 'CASINO JACKPOT', 0)
GO
IF NOT EXISTS ( SELECT 1 FROM LCD_FUNCTIONALITIES WHERE FUN_FUNCTION_ID =  5)
  INSERT INTO LCD_FUNCTIONALITIES (fun_function_id, FUN_NAME, FUN_ENABLED) VALUES (5, 'LOYALTY CLUB', 0)
GO

/******* STORED PROCEDURES *******/

