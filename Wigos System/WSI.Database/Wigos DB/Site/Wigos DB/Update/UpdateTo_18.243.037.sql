/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 242;

SET @New_ReleaseId = 243;
SET @New_ScriptName = N'UpdateTo_18.243.037.sql';
SET @New_Description = N'New field mb_over_cash in table mobile_banks; New GP WCP - Terminal.UseStackerId; Update SP: CageGetGlobalReportData, PR_collection_by_machine_and_date; Update Trigger: MultiSiteTrigger_SiteAccountUpdate.';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/******* TABLES *******/

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[mobile_banks]') and name = 'mb_over_cash')
  ALTER TABLE mobile_banks ADD mb_over_cash MONEY NOT NULL CONSTRAINT DF_mobile_banks_mb_over_cash DEFAULT(0)
GO

/******* INDEXES *******/

/******* RECORDS *******/

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'WCP' AND GP_SUBJECT_KEY ='Terminal.UseStackerId')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('WCP', 'Terminal.UseStackerId', '0');
GO

UPDATE alarm_catalog SET ALCG_NAME = 'Id de stacker no existe' WHERE alcg_alarm_code = 1114114 AND alcg_language_id = 10
GO

/******* STORED PROCEDURES *******/

--
-- CageGetGlobalReportData
--

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CageGetGlobalReportData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CageGetGlobalReportData]
GO

CREATE PROCEDURE [dbo].[CageGetGlobalReportData]
        @pCageSessionIds NVARCHAR(MAX) = NULL
AS
BEGIN

  DECLARE @NationalIsoCode NVARCHAR(3)
  DECLARE @CageOperationType_FromTerminal INT
  DECLARE @TicketsId INT
  DECLARE @BillsId INT
  DECLARE @Currencies AS NVARCHAR(200)

  SELECT @NationalIsoCode = GP_KEY_VALUE 
    FROM GENERAL_PARAMS 
   WHERE GP_GROUP_KEY = 'RegionalOptions' 
     AND GP_SUBJECT_KEY = 'CurrencyISOCode'

  SELECT @Currencies = GP_KEY_VALUE 
    FROM GENERAL_PARAMS 
   WHERE GP_GROUP_KEY = 'RegionalOptions' 
     AND GP_SUBJECT_KEY = 'CurrenciesAccepted'

  -- Split currencies ISO codes
  SELECT SST_VALUE AS CURRENCY_ISO_CODE INTO #TMP_CURRENCIES_ISO_CODES FROM [SplitStringIntoTable] (@Currencies, ';', 1)
               
  SET @CageOperationType_FromTerminal = 104
  SET @TicketsId = -200
  SET @BillsId = 0
      
  -- TABLE[0]: Get cage stock
  SELECT CGS_ISO_CODE                                          
       , CGS_DENOMINATION                                      
       , CGS_QUANTITY 
    FROM (SELECT CGS_ISO_CODE                                          
               , CGS_DENOMINATION                                      
               , CGS_QUANTITY                                          
            FROM CAGE_STOCK
                  
          UNION ALL
                  
          SELECT C.CH_ISO_CODE AS CGS_ISO_CODE
               , C.CH_DENOMINATION AS CGS_DENOMINATION
               , CST.CHSK_QUANTITY AS CGS_QUANTITY
            FROM CHIPS_STOCK AS CST
                 INNER JOIN CHIPS AS C ON CST.CHSK_CHIP_ID = C.CH_CHIP_ID
          ) AS _tbl 
    WHERE CGS_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)                                                          
 ORDER BY CGS_ISO_CODE                                          
        , CASE WHEN CGS_DENOMINATION >= 0 THEN CGS_DENOMINATION 
               ELSE CGS_DENOMINATION * (-100000) END

  IF @pCageSessionIds IS NOT NULL
  BEGIN
  
    -- Session IDs Split
    SELECT SST_VALUE AS CAGE_SESSION_ID INTO #TMP_CAGE_SESSIONS FROM [SplitStringIntoTable] (@pCageSessionIds,','  , 1)
    
    -- TABLE[1]: Get cage sessions information
    SELECT CSM.CSM_SOURCE_TARGET_ID AS CM_SOURCE_TARGET_ID
         , CSM.CSM_CONCEPT_ID AS CM_CONCEPT_ID
         , CSM.CSM_ISO_CODE AS CM_ISO_CODE
         , SUM(CSM.CSM_VALUE_IN) AS CM_VALUE_IN
         , SUM(CSM.CSM_VALUE_OUT) AS CM_VALUE_OUT
         , CST.CST_SOURCE_TARGET_NAME
         , CC.CC_DESCRIPTION
      FROM CAGE_SESSION_METERS AS CSM
           INNER JOIN CAGE_SOURCE_TARGET_CONCEPTS AS CSTC ON CSTC.CSTC_CONCEPT_ID = CSM.CSM_CONCEPT_ID AND CSTC.CSTC_SOURCE_TARGET_ID = CSM.CSM_SOURCE_TARGET_ID
           INNER JOIN CAGE_CONCEPTS AS CC ON CC.CC_CONCEPT_ID = CSTC.CSTC_CONCEPT_ID
           INNER JOIN CAGE_SOURCE_TARGET AS CST ON CST.CST_SOURCE_TARGET_ID = CSTC.CSTC_SOURCE_TARGET_ID
     WHERE CSM.CSM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
       AND CC.CC_SHOW_IN_REPORT = 1
       AND CC.CC_ENABLED = 1
       AND (CC.CC_TYPE = 1 OR CC_CONCEPT_ID IN (9, 10))
       AND CSTC.CSTC_ENABLED = 1
       AND CSM_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)
  GROUP BY CSM.CSM_SOURCE_TARGET_ID, CSM.CSM_CONCEPT_ID, CSM.CSM_ISO_CODE, CST.CST_SOURCE_TARGET_NAME, CC.CC_DESCRIPTION
  ORDER BY CSM.CSM_SOURCE_TARGET_ID
         , CASE WHEN CSM.CSM_CONCEPT_ID = 0 THEN '000' ELSE CC.CC_DESCRIPTION END
         , CASE WHEN CSM.CSM_ISO_CODE = @NationalIsoCode THEN '000' ELSE CSM.CSM_ISO_CODE END                    

    -- TABLE[2]: Get dynamic liabilities
    SELECT CC.CC_CONCEPT_ID AS LIABILITY_CONCEPT_ID
         , CC.CC_DESCRIPTION AS LIABILITY_NAME 
         , CSM.CSM_ISO_CODE AS LIABILITY_ISO_CODE
         , SUM(CSM.CSM_VALUE) AS LIABILITY_VALUE
      FROM CAGE_SESSION_METERS AS CSM
           INNER JOIN CAGE_CONCEPTS AS CC ON CC.CC_CONCEPT_ID = CSM.CSM_CONCEPT_ID
     WHERE CC.CC_ENABLED = 1
       AND CC.CC_SHOW_IN_REPORT = 1
       AND CC.CC_IS_PROVISION = 1
       AND CSM.CSM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
       AND CSM_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)
 GROUP BY CC.CC_CONCEPT_ID, CC.CC_DESCRIPTION, CSM.CSM_ISO_CODE
  ORDER BY CC.CC_CONCEPT_ID ASC
         , CASE WHEN CSM.CSM_ISO_CODE = @NationalIsoCode THEN '000' ELSE CSM.CSM_ISO_CODE END 
      
    -- TABLE[3]: Get session sumary
    SELECT CMD_ISO_CODE AS ISO_CODE
         , ISNULL(ORIGIN, 0) AS ORIGIN
         , SUM(CMD_DEPOSITS) AS DEPOSITS
         , SUM(CMD_WITHDRAWALS) AS WITHDRAWALS
      FROM (SELECT CMD_ISO_CODE
                 , ORIGIN
                 , SUM(CMD_DEPOSITS) CMD_DEPOSITS
                 , SUM(CMD_WITHDRAWALS) CMD_WITHDRAWALS      
              FROM (SELECT CASE WHEN CMD_QUANTITY >= 0 THEN @BillsId
                                ELSE CMD_QUANTITY END  AS ORIGIN
                         , CMD_ISO_CODE
                         , CMD_DENOMINATION
                         , CASE WHEN CGM_TYPE IN (100, 101, 103, 104) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY < 0 THEN CMD_DENOMINATION
                                                                                            ELSE CMD_QUANTITY * CMD_DENOMINATION END), 0)
                                ELSE 0 END AS CMD_DEPOSITS
                         , CASE WHEN CGM_TYPE IN (0, 1, 4, 5) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY < 0 THEN CMD_DENOMINATION             
                                                                                    ELSE CMD_QUANTITY * CMD_DENOMINATION END), 0)
                                ELSE 0 END AS CMD_WITHDRAWALS
                      FROM CAGE_MOVEMENTS 
                           INNER JOIN   CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          
                     WHERE CGM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
                       AND (CGM_STATUS IN (2, 3, 4, 8, 9, 10) 
                            OR (CGM_STATUS = 0 AND CGM_TYPE = 0))
                  GROUP BY CMD_ISO_CODE, CMD_DENOMINATION, CGM_TYPE, CMD_QUANTITY
                   ) AS T1        
          GROUP BY ORIGIN, CMD_ISO_CODE

            -- GET Terminal value-in amounts
              
              UNION ALL 
              
              SELECT @NationalIsoCode AS CMD_ISO_CODE
                    , @TicketsId AS ORIGIN
                    , MC_COLLECTED_TICKET_AMOUNT AS DEPOSITS
                    , 0 AS WITHDRAWS
              FROM MONEY_COLLECTIONS
                  INNER JOIN CAGE_MOVEMENTS ON CGM_MC_COLLECTION_ID = MC_COLLECTION_ID 
             WHERE CAGE_MOVEMENTS.CGM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
               AND CAGE_MOVEMENTS.CGM_TYPE = @CAGEOPERATIONTYPE_FROMTERMINAL
               AND (CGM_STATUS IN (2, 3, 4, 8, 9, 10) 
                    OR (CGM_STATUS = 0 AND CGM_TYPE = 0))           
             
              UNION ALL
              
                SELECT @NationalIsoCode AS CMD_ISO_CODE
                    , @BillsId AS ORIGIN
                    , MC_COLLECTED_BILL_AMOUNT  AS DEPOSITS
                    , 0 AS WITHDRAWS
              FROM MONEY_COLLECTIONS
                  INNER JOIN CAGE_MOVEMENTS ON CGM_MC_COLLECTION_ID = MC_COLLECTION_ID 
             WHERE CAGE_MOVEMENTS.CGM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
               AND CAGE_MOVEMENTS.CGM_TYPE = @CageOperationType_FromTerminal
               AND (CGM_STATUS IN (2, 3, 4, 8, 9, 10) 
                    OR (CGM_STATUS = 0 AND CGM_TYPE = 0))
                                      
          ) AS _TBL_GLB
      WHERE CMD_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)
   GROUP BY _TBL_GLB.CMD_ISO_CODE, _TBL_GLB.ORIGIN
   ORDER BY CASE WHEN _TBL_GLB.CMD_ISO_CODE = @NationalIsoCode THEN '000' ELSE _TBL_GLB.CMD_ISO_CODE END 
          , _TBL_GLB.ORIGIN DESC
         
    -- TABLE[4]: Other System Concepts           
    SELECT CSM_CONCEPT_ID AS CM_CONCEPT_ID
         , CC.CC_DESCRIPTION AS CM_DESCRIPTION
         , CSM_ISO_CODE AS CM_ISO_CODE
         , SUM(CSM_VALUE) AS CM_VALUE
      FROM CAGE_SESSION_METERS AS CSM
           INNER JOIN CAGE_CONCEPTS AS CC ON CSM.CSM_CONCEPT_ID = CC.CC_CONCEPT_ID
     WHERE CSM_SOURCE_TARGET_ID = 0
       AND CSM_CONCEPT_ID IN (7, 8)
       AND CSM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
       AND CSM_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)
  GROUP BY CSM_CONCEPT_ID, CSM_ISO_CODE, CSM_VALUE, CC.CC_DESCRIPTION 
  ORDER BY CM_CONCEPT_ID
         , CASE WHEN CSM_ISO_CODE = @NationalIsoCode THEN '000' ELSE CSM_ISO_CODE END 
         
     -- TABLE[5]: Cage Counts
    SELECT ORIGIN AS ORIGIN
         , CMD_ISO_CODE AS ISO_CODE
         , ISNULL(SUM(CMD_DEPOSITS), 0) AS DEPOSITS
      FROM (SELECT CASE WHEN CMD_QUANTITY >= 0 THEN 0
                        ELSE CMD_QUANTITY END  AS ORIGIN
                 , CMD_ISO_CODE
                 , ISNULL(SUM (CMD_QUANTITY * CASE WHEN CGM_TYPE = 99 THEN -1 * CMD_DENOMINATION 
												   ELSE CMD_DENOMINATION END), 0) AS CMD_DEPOSITS
              FROM CAGE_MOVEMENTS 
                   INNER JOIN CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          
--             WHERE CGM_TYPE = 3
				WHERE CGM_TYPE IN (99,199)
               AND CGM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
          GROUP BY CMD_ISO_CODE, CMD_DENOMINATION, CGM_TYPE, CMD_QUANTITY
            ) AS T1           
   GROUP BY ORIGIN, CMD_ISO_CODE
           
    UNION ALL
              
    SELECT NULL AS ORIGIN
         , CMD_ISO_CODE AS ISO_CODE
         , ISNULL(SUM(CMD_DEPOSITS), 0) AS DEPOSITS
      FROM (SELECT CMD_ISO_CODE
                 , ISNULL(SUM (CMD_QUANTITY * CASE WHEN CGM_TYPE = 99 THEN -1 * CMD_DENOMINATION 
												   ELSE CMD_DENOMINATION END), 0) AS CMD_DEPOSITS
              FROM CAGE_MOVEMENTS 
                   INNER JOIN CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          
             WHERE CGM_TYPE IN (99,199)
               AND CGM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
          GROUP BY CMD_ISO_CODE, CMD_DENOMINATION, CGM_TYPE, CMD_QUANTITY
           ) AS T1           
  GROUP BY CMD_ISO_CODE

    DROP TABLE #TMP_CAGE_SESSIONS 

  END -- IF @pCageSessionIds IS NOT NULL THEN
      
  ELSE -- IF @pCageSessionIds IS NULL
  BEGIN
      
    -- TABLE[1]: Get cage sessions information
    SELECT CM.CM_SOURCE_TARGET_ID
         , CM.CM_CONCEPT_ID
         , CM.CM_ISO_CODE
         , CM.CM_VALUE_IN
         , CM.CM_VALUE_OUT
         , CST.CST_SOURCE_TARGET_NAME
         , CC.CC_DESCRIPTION
      FROM CAGE_METERS AS CM
           INNER JOIN CAGE_SOURCE_TARGET_CONCEPTS AS CSTC ON CSTC.CSTC_CONCEPT_ID = CM.CM_CONCEPT_ID AND CSTC.CSTC_SOURCE_TARGET_ID = CM.CM_SOURCE_TARGET_ID
           INNER JOIN CAGE_CONCEPTS AS CC ON CC.CC_CONCEPT_ID = CSTC.CSTC_CONCEPT_ID
           INNER JOIN CAGE_SOURCE_TARGET AS CST ON CST.CST_SOURCE_TARGET_ID = CSTC.CSTC_SOURCE_TARGET_ID
     WHERE (CC.CC_TYPE = 1 OR CC_CONCEPT_ID IN (9, 10))
       AND CC.CC_ENABLED = 1
       AND CSTC.CSTC_ENABLED = 1
       AND CC.CC_SHOW_IN_REPORT = 1
       AND CM_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)
  ORDER BY CM.CM_SOURCE_TARGET_ID
         , CASE WHEN CM.CM_CONCEPT_ID = 0 THEN '000' ELSE CC.CC_DESCRIPTION END
         , CASE WHEN CM.CM_ISO_CODE = @NationalIsoCode THEN '000' ELSE CM.CM_ISO_CODE END   

    -- TABLE[2]: Get dynamic liabilities
    SELECT CC.CC_CONCEPT_ID AS LIABILITY_CONCEPT_ID
         , CC.CC_DESCRIPTION AS LIABILITY_NAME 
         , CM.CM_ISO_CODE AS LIABILITY_ISO_CODE
         , SUM(CM.CM_VALUE) AS LIABILITY_VALUE
      FROM CAGE_METERS AS CM
           INNER JOIN CAGE_CONCEPTS AS CC ON CC.CC_CONCEPT_ID = CM.CM_CONCEPT_ID
     WHERE CC.CC_ENABLED = 1
       AND CC.CC_SHOW_IN_REPORT = 1
       AND CC.CC_IS_PROVISION = 1
       AND CM_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)
  GROUP BY CC.CC_CONCEPT_ID, CC.CC_DESCRIPTION, CM.CM_ISO_CODE
  ORDER BY CC.CC_CONCEPT_ID ASC 
         , CASE WHEN CM.CM_ISO_CODE = @NationalIsoCode THEN '000' ELSE CM.CM_ISO_CODE END 
      
    -- TABLE[3]: Get session sumary
    SELECT CMD_ISO_CODE AS ISO_CODE
         , ISNULL(ORIGIN, 0) AS ORIGIN
         , SUM(CMD_DEPOSITS) AS DEPOSITS
         , SUM(CMD_WITHDRAWALS) AS WITHDRAWALS     
      FROM (SELECT CMD_ISO_CODE
                 , ORIGIN
                 , SUM(CMD_DEPOSITS) CMD_DEPOSITS
                 , SUM(CMD_WITHDRAWALS) CMD_WITHDRAWALS      
              FROM (SELECT CASE WHEN CMD_QUANTITY >= 0 THEN @BillsId
                                ELSE CMD_QUANTITY END AS ORIGIN
                         , CMD_ISO_CODE
                         , CMD_DENOMINATION
                         , CASE WHEN CGM_TYPE IN (100, 101, 103, 104) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY < 0 THEN CMD_DENOMINATION
                                                                                            ELSE CMD_QUANTITY * CMD_DENOMINATION END), 0)
                                ELSE 0 END AS CMD_DEPOSITS
                         , CASE WHEN CGM_TYPE IN (0, 1, 4, 5) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY < 0 THEN CMD_DENOMINATION             
                                                                                    ELSE CMD_QUANTITY * CMD_DENOMINATION END), 0)
                                ELSE 0 END AS CMD_WITHDRAWALS
                      FROM CAGE_MOVEMENTS 
                           INNER JOIN   CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          
                     WHERE CGM_STATUS IN (2, 3, 4, 8, 9, 10) 
                        OR (CGM_STATUS = 0 AND CGM_TYPE = 0)
                  GROUP BY CMD_ISO_CODE, CMD_DENOMINATION, CGM_TYPE, CMD_QUANTITY
                    ) AS T1
           GROUP BY ORIGIN,CMD_ISO_CODE

              -- GET Terminal value-in amounts
              
              UNION ALL 
              
              SELECT @NationalIsoCode AS CMD_ISO_CODE
                    , @TicketsId AS ORIGIN
                    , MC_COLLECTED_TICKET_AMOUNT AS DEPOSITS
                    , 0 AS WITHDRAWS
              FROM MONEY_COLLECTIONS
                  INNER JOIN CAGE_MOVEMENTS ON CGM_MC_COLLECTION_ID = MC_COLLECTION_ID 
             WHERE CAGE_MOVEMENTS.CGM_TYPE = @CAGEOPERATIONTYPE_FROMTERMINAL
               AND (CGM_STATUS IN (2, 3, 4, 8, 9, 10) 
                    OR (CGM_STATUS = 0 AND CGM_TYPE = 0))           
             
              UNION ALL
              
                SELECT @NationalIsoCode AS CMD_ISO_CODE
                    , @BillsId AS ORIGIN
                    , MC_COLLECTED_BILL_AMOUNT  AS DEPOSITS
                    , 0 AS WITHDRAWS
              FROM MONEY_COLLECTIONS
                  INNER JOIN CAGE_MOVEMENTS ON CGM_MC_COLLECTION_ID = MC_COLLECTION_ID 
             WHERE CAGE_MOVEMENTS.CGM_TYPE = @CageOperationType_FromTerminal
               AND (CGM_STATUS IN (2, 3, 4, 8, 9, 10) 
                    OR (CGM_STATUS = 0 AND CGM_TYPE = 0))
                          
          ) AS _TBL_GLB 
      WHERE CMD_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)             
   GROUP BY _TBL_GLB.CMD_ISO_CODE, _TBL_GLB.ORIGIN
   ORDER BY CASE WHEN _TBL_GLB.CMD_ISO_CODE = @NationalIsoCode THEN '000' ELSE _TBL_GLB.CMD_ISO_CODE END 
          , _TBL_GLB.ORIGIN DESC 
         
    -- TABLE[4]: Other System Concepts           
    SELECT CM_CONCEPT_ID
         , CC.CC_DESCRIPTION AS CM_DESCRIPTION
         , CM_ISO_CODE
         , SUM(CM_VALUE) AS CM_VALUE
      FROM CAGE_METERS AS CM
           INNER JOIN CAGE_CONCEPTS AS CC ON CM.CM_CONCEPT_ID = CC.CC_CONCEPT_ID
     WHERE CM_SOURCE_TARGET_ID = 0
       AND CM_CONCEPT_ID IN (7, 8)
       AND CM_ISO_CODE IN (SELECT * FROM #TMP_CURRENCIES_ISO_CODES)
  GROUP BY CM_CONCEPT_ID, CM_ISO_CODE, CM_VALUE, CC.CC_DESCRIPTION
  ORDER BY CM_CONCEPT_ID
         , CASE WHEN CM_ISO_CODE = @NationalIsoCode THEN '000' ELSE CM_ISO_CODE END
    
    -- TABLE[5]: Cage Counts
    SELECT ORIGIN AS ORIGIN
         , CMD_ISO_CODE AS ISO_CODE
         , ISNULL(SUM(CMD_DEPOSITS), 0) AS DEPOSITS
      FROM (SELECT CASE WHEN CMD_QUANTITY >= 0 THEN 0
                        ELSE CMD_QUANTITY END  AS ORIGIN
                 , CMD_ISO_CODE
                 , ISNULL(SUM (CMD_QUANTITY * CASE WHEN CGM_TYPE = 99 THEN -1 * CMD_DENOMINATION 
												   ELSE CMD_DENOMINATION END), 0) AS CMD_DEPOSITS
              FROM CAGE_MOVEMENTS 
                   INNER JOIN CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          
             WHERE CGM_TYPE IN (99,199)
          GROUP BY CMD_ISO_CODE, CMD_DENOMINATION, CGM_TYPE, CMD_QUANTITY
            ) AS T1           
   GROUP BY ORIGIN, CMD_ISO_CODE
           
    UNION ALL
              
    SELECT NULL AS ORIGIN
         , CMD_ISO_CODE AS ISO_CODE
         , ISNULL(SUM(CMD_DEPOSITS), 0) AS DEPOSITS
      FROM (SELECT CMD_ISO_CODE
                 , ISNULL(SUM (CMD_QUANTITY * CASE WHEN CGM_TYPE = 99 THEN -1 * CMD_DENOMINATION 
												   ELSE CMD_DENOMINATION END), 0) AS CMD_DEPOSITS
              FROM CAGE_MOVEMENTS 
                   INNER JOIN CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          
             WHERE CGM_TYPE IN (99,199)
          GROUP BY CMD_ISO_CODE, CMD_DENOMINATION, CGM_TYPE, CMD_QUANTITY
           ) AS T1           
  GROUP BY CMD_ISO_CODE


  END -- ELSE
  
  DROP TABLE #TMP_CURRENCIES_ISO_CODES
                   
END -- CageGetGlobalReportData

GO
GRANT EXECUTE ON [dbo].[CageGetGlobalReportData] TO [wggui] WITH GRANT OPTION 
GO


--
-- PR_collection_by_machine_and_date
--

IF OBJECT_ID (N'dbo.PR_collection_by_machine_and_date', N'P') IS NOT NULL
    DROP PROCEDURE dbo.PR_collection_by_machine_and_date;                 
GO    

CREATE PROCEDURE [dbo].[PR_collection_by_machine_and_date] 
 ( @pFromDt DATETIME   
  ,@pToDt DATETIME    
  ,@pClosingTime INTEGER
  ,@pMaskStatus INTEGER
  ,@pStatusPaid INTEGER
  ,@pTerminalWhere NVARCHAR(MAX) 
  ,@pBillDetail BIT
)
AS
BEGIN

  -- DECLARATIONS
   DECLARE @Sql AS VARCHAR(MAX)
   DECLARE @nRows AS INT                              
   DECLARE @index AS INT                              
   DECLARE @tDays AS TABLE(NumDay INT)                       
   DECLARE @Columns AS VARCHAR(MAX)
   DECLARE @ColumnChk AS VARCHAR(MAX)
   DECLARE @p2007Opening AS VARCHAR(MAX)

   SET @p2007Opening  = CAST('2007-01-01T00:00:00' AS DATETIME);
   SET @p2007Opening  = DATEADD(HOUR, @pClosingTime, @p2007Opening)

   SET @nRows  = DATEDIFF(DAY, @pFromDt, @pToDt)+1 
   SET @index = 0                                  

   WHILE @index < @nRows                           
   BEGIN	                                          
     INSERT INTO @tDays VALUES(@index)	                
     SET @index = @index + 1                       
   END 
   
    SET @Columns = ''
    SET @ColumnChk = ''

   SELECT NumDay INTO #TempTable_Days FROM @tDays 
   
   SELECT @Columns = COALESCE(@Columns + '[' + CAST(CGC_DENOMINATION AS NVARCHAR(20)) + '],', '')
     FROM (SELECT   DISTINCT CGC_DENOMINATION 
             FROM   CAGE_CURRENCIES 
            WHERE   CGC_ISO_CODE IN (SELECT   GP_KEY_VALUE 
                                       FROM   GENERAL_PARAMS 
                                      WHERE   GP_GROUP_KEY = 'RegionalOptions' 
                                        AND   GP_SUBJECT_KEY = 'CurrencyISOCode')
              AND   CGC_DENOMINATION >= 1 
              AND   CGC_ALLOWED = 1
                    UNION  
           SELECT   DISTINCT MCD_FACE_VALUE 
             FROM   MONEY_COLLECTION_DETAILS 
            INNER   JOIN MONEY_COLLECTIONS ON MCD_COLLECTION_ID = MC_COLLECTION_ID
            WHERE   MC_COLLECTION_DATETIME >= @pFromDt
              AND   MC_COLLECTION_DATETIME <  @pToDt
              AND   MC_TERMINAL_ID IS NOT NULL 
              AND   MCD_FACE_VALUE >= 1
          ) AS DTM
  ORDER BY CGC_DENOMINATION

  SET @ColumnChk = REPLACE( @Columns, ',', ' IS NOT NULL OR ')

  SET @Columns = LEFT(@Columns,LEN(@Columns)-1)
  SET @ColumnChk = LEFT(@ColumnChk,LEN(@ColumnChk)-3)
     
  SET @Sql = '
   SELECT   TE_PROVIDER_ID 
          , TE_TERMINAL_ID 
          , ISNULL(TE_MULTI_DENOMINATION, ''--'') AS DENOMINATION 
          , ORDER_DATE
          , DATEADD(DAY, 1, ORDER_DATE) AS ORDER_DATE_FIN
          , TICKET_IN_COUNT
          , TI_IN_AMOUNT_RE
          , TI_IN_AMOUNT_NO_RE
          , (ISNULL(TI_IN_AMOUNT_RE,0) + ISNULL(TI_IN_AMOUNT_NO_RE, 0)) AS TI_IN_AMOUNT
          , TICKET_OUT_COUNT
          , TI_OUT_AMOUNT_RE
          , TI_OUT_AMOUNT_NO_RE
          , (ISNULL(TI_OUT_AMOUNT_RE, 0) + ISNULL(TI_OUT_AMOUNT_NO_RE, 0)) AS TI_OUT_AMOUNT
          , (ISNULL(TI_OUT_AMOUNT_RE, 0) - ISNULL(TI_IN_AMOUNT_RE, 0)) AS NET_TITO
          , HAND_PAYS.MANUAL
          , HAND_PAYS.CREDIT_CANCEL
          , HAND_PAYS.JACKPOT_DE_SALA
          , HAND_PAYS.PROGRESIVES
          , HAND_PAYS.NO_PROGRESIVES
          , (ISNULL(HAND_PAYS.MANUAL, 0) + ISNULL(HAND_PAYS.CREDIT_CANCEL, 0) + ISNULL(HAND_PAYS.JACKPOT_DE_SALA, 0) + ISNULL(HAND_PAYS.NO_PROGRESIVES, 0) + ISNULL(PROVISIONS.PROGRESIVE_PROVISIONS, 0)) AS HAND_PAYS_TOTAL
          , PROVISIONS.PROGRESIVE_PROVISIONS
          , (ISNULL(TI_OUT_AMOUNT_RE, 0) - ISNULL(TI_IN_AMOUNT_RE, 0) +
             ISNULL(HAND_PAYS.MANUAL, 0) + ISNULL(HAND_PAYS.CREDIT_CANCEL, 0) + ISNULL(HAND_PAYS.JACKPOT_DE_SALA, 0) + ISNULL(HAND_PAYS.NO_PROGRESIVES, 0) + ISNULL(PROVISIONS.PROGRESIVE_PROVISIONS, 0)) 
              AS TOTAL_WIN_WITHOUT_BILLS
          , 0 AS GAP_BILL
          , BILLS.*
     FROM   TERMINALS 
 LEFT JOIN (SELECT   DATEADD(DAY, NumDay, CAST(''' + CAST(@pFromDt AS VARCHAR(50)) + ''' AS DATETIME)) AS ORDER_DATE FROM #TempTable_Days ) DIA ON ORDER_DATE <= GETDATE() 

 LEFT JOIN (SELECT   MC_TERMINAL_ID AS MC_TERMINAL_ID_T
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), MC_DATETIME)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS MC_DATETIME_T
                   , SUM(MC_EXPECTED_TICKET_COUNT) AS TICKET_IN_COUNT
                   , SUM(ISNULL(MC_EXPECTED_RE_TICKET_AMOUNT, 0)+ISNULL(MC_COLLECTED_PROMO_RE_TICKET_AMOUNT, 0)) AS TI_IN_AMOUNT_RE
                   , SUM(ISNULL(MC_EXPECTED_PROMO_NR_TICKET_AMOUNT, 0)) AS TI_IN_AMOUNT_NO_RE
              FROM   MONEY_COLLECTIONS 
             WHERE   MC_DATETIME >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
               AND   MC_DATETIME <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
               AND   MC_TERMINAL_ID IS NOT NULL
             GROUP   BY MC_TERMINAL_ID
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), MC_DATETIME)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) 
           ) TICKETS_IN ON TE_TERMINAL_ID = MC_TERMINAL_ID_T AND MC_DATETIME_T = DIA.ORDER_DATE 
LEFT JOIN ( SELECT   TI_CREATED_TERMINAL_ID
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), TI_CREATED_DATETIME)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS TI_CREATED_DATETIME
                   , COUNT(1) AS TICKET_OUT_COUNT
                   , SUM(CASE WHEN TI_TYPE_ID = 2 THEN 0 ELSE TI_AMOUNT END) AS TI_OUT_AMOUNT_RE
                   , SUM(CASE WHEN TI_TYPE_ID = 2 THEN TI_AMOUNT ELSE 0 END) AS TI_OUT_AMOUNT_NO_RE
              FROM   TICKETS 
             WHERE   TI_CREATED_DATETIME >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
               AND   TI_CREATED_DATETIME <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
               AND   TI_CREATED_TERMINAL_TYPE = 1     -- TITO_TERMINAL_TYPE.TERMINAL = 1
               AND   TI_TYPE_ID IN (0,1,2,5) -- TITO_TICKET_TYPE
                                                --CASHABLE = 0,
                                                --PROMO_REDEEM = 1,
                                                --PROMO_NONREDEEM = 2,  // only playable
                                                --HANDPAY = 3,
                                                --JACKPOT = 4,
                                                --OFFLINE = 5
             GROUP   BY TI_CREATED_TERMINAL_ID
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), TI_CREATED_DATETIME)/24, CAST(''' + @p2007Opening + ''' AS DATETIME))
          ) TICKETS_OUT ON TE_TERMINAL_ID = TI_CREATED_TERMINAL_ID AND TI_CREATED_DATETIME = DIA.ORDER_DATE 
 LEFT JOIN (SELECT   HP_TERMINAL_ID 
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), HP_STATUS_CHANGED)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS HP_DATETIME
                   , SUM(CASE WHEN (HP_TYPE IN (10, 1000, 1001, 1010)) THEN ISNULL(HP_AMOUNT, 0) ELSE 0 END) AS MANUAL 
                   , SUM(CASE WHEN (HP_TYPE IN ( 0 )) THEN ISNULL(HP_AMOUNT, 0) ELSE 0 END) AS CREDIT_CANCEL 
                   , SUM(CASE WHEN (HP_TYPE IN ( 20)) THEN ISNULL(HP_AMOUNT, 0) ELSE 0 END) AS JACKPOT_DE_SALA 
                   , SUM(CASE WHEN (HP_TYPE IN ( 1 ) AND (HP_LEVEL>=1 AND HP_LEVEL<=32)) THEN ISNULL(HP_AMOUNT, 0) ELSE 0 END) AS PROGRESIVES 
                   , SUM(CASE WHEN (HP_TYPE IN ( 1 ) AND (HP_LEVEL <1 OR  HP_LEVEL >32)) THEN ISNULL(HP_AMOUNT, 0) ELSE 0 END) AS NO_PROGRESIVES 
              FROM   HANDPAYS WITH (INDEX (IX_hp_status_changed))
             WHERE   HP_STATUS & ' + CAST(@pMaskStatus AS VARCHAR(10)) + ' = ' + CAST(@pStatusPaid AS VARCHAR(10)) + 
            '  AND   HP_STATUS_CHANGED >= CAST(''' + CAST(@pFromDt AS VARCHAR(50)) + ''' AS DATETIME) 
               AND   HP_STATUS_CHANGED < CAST(''' + CAST(@pToDt AS VARCHAR(50)) + ''' AS DATETIME) 
             GROUP   BY HP_TERMINAL_ID 
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), HP_STATUS_CHANGED)/24, CAST(''' + @p2007Opening + ''' AS DATETIME))
                   ) HAND_PAYS ON HP_TERMINAL_ID = TE_TERMINAL_ID AND HP_DATETIME = DIA.ORDER_DATE 
 LEFT JOIN (SELECT   PPT_TERMINAL_ID 
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), PGP_HOUR_TO)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS PGP_HOUR_TO
                   , SUM(ISNULL(PPT_AMOUNT, 0)) AS PROGRESIVE_PROVISIONS 
             FROM    PROGRESSIVES_PROVISIONS_TERMINALS 
        LEFT JOIN    PROGRESSIVES_PROVISIONS ON PGP_PROVISION_ID = PPT_PROVISION_ID 
                                            AND PGP_PROGRESSIVE_ID = PPT_PROGRESSIVE_ID 
                                            AND PGP_HOUR_TO >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
                                            AND PGP_HOUR_TO <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
             GROUP   BY PPT_TERMINAL_ID 
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), PGP_HOUR_TO)/24, CAST(''' + @p2007Opening + ''' AS DATETIME))
                   ) PROVISIONS ON PPT_TERMINAL_ID = TE_TERMINAL_ID AND PGP_HOUR_TO = DIA.ORDER_DATE '
IF @pBillDetail = 1
BEGIN
  SET @Sql =  @Sql +
'LEFT JOIN (SELECT   *  
              FROM ( SELECT   MC_TERMINAL_ID
                            , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), MC_COLLECTION_DATETIME)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS MC_COLLECTION_DATETIME
                            , MCD_FACE_VALUE
                            , (MCD_NUM_COLLECTED * MCD_FACE_VALUE) AS COLLECTED
                       FROM   MONEY_COLLECTION_DETAILS 
                 INNER JOIN   MONEY_COLLECTIONS ON MCD_COLLECTION_ID = MC_COLLECTION_ID
                      WHERE   MC_COLLECTION_DATETIME >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
                        AND   MC_COLLECTION_DATETIME <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
                        AND   MC_TERMINAL_ID IS NOT NULL
                   ) PIV
             PIVOT (  SUM(COLLECTED) FOR MCD_FACE_VALUE IN ('+ @Columns  + ')) AS CHILD
            ) BILLS ON TE_TERMINAL_ID = MC_TERMINAL_ID AND MC_COLLECTION_DATETIME = DIA.ORDER_DATE '
END
ELSE
BEGIN
  SET @Sql =  @Sql +
'LEFT JOIN (SELECT   MC_TERMINAL_ID
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), MC_COLLECTION_DATETIME)/24, CAST(''' + @p2007Opening + ''' AS DATETIME)) AS MC_COLLECTION_DATETIME
                   , 0 AS MCD_FACE_VALUE
                   , SUM(ISNULL(MC_COLLECTED_BILL_AMOUNT, 0)) AS COLLECTED
              FROM   MONEY_COLLECTIONS 
             WHERE   MC_COLLECTION_DATETIME >= CAST('''+ CAST(@pFromDt AS VARCHAR(50))+''' AS DATETIME)
               AND   MC_COLLECTION_DATETIME <  CAST('''+ CAST(@pToDt AS VARCHAR(50))+''' AS DATETIME)
               AND   MC_TERMINAL_ID IS NOT NULL
             GROUP   BY MC_TERMINAL_ID
                   , DATEADD(DAY, DATEDIFF(HOUR, CAST(''' + @p2007Opening + ''' AS DATETIME), MC_COLLECTION_DATETIME)/24, CAST(''' + @p2007Opening + ''' AS DATETIME))
          ) BILLS ON TE_TERMINAL_ID = MC_TERMINAL_ID AND MC_COLLECTION_DATETIME = DIA.ORDER_DATE '
END

SET @Sql =  @Sql + 'WHERE ( ' 

IF @pBillDetail = 1
BEGIN
  SET @Sql =  @Sql + @ColumnChk 
END
ELSE
BEGIN
  SET @Sql =  @Sql + ' BILLS.COLLECTED IS NOT NULL ' 
END

SET @Sql =  @Sql +
  ' OR TICKET_IN_COUNT IS NOT NULL
    OR TI_IN_AMOUNT_RE IS NOT NULL
    OR TI_IN_AMOUNT_NO_RE IS NOT NULL
    OR TICKET_OUT_COUNT IS NOT NULL
    OR TI_OUT_AMOUNT_RE IS NOT NULL
    OR TI_OUT_AMOUNT_NO_RE IS NOT NULL
    OR HAND_PAYS.MANUAL IS NOT NULL
    OR HAND_PAYS.CREDIT_CANCEL IS NOT NULL
    OR HAND_PAYS.JACKPOT_DE_SALA IS NOT NULL
    OR HAND_PAYS.PROGRESIVES IS NOT NULL
    OR HAND_PAYS.NO_PROGRESIVES IS NOT NULL
    OR PROVISIONS.PROGRESIVE_PROVISIONS IS NOT NULL
    )' + CAST(@pTerminalWhere AS Varchar(max)) +
' ORDER   BY TE_PROVIDER_ID 
        , TE_TERMINAL_ID 
        , TE_NAME 
        , ISNULL(TE_MULTI_DENOMINATION, ''--'')
        , ORDER_DATE '

 -- PRINT @Sql

  EXECUTE (@Sql)

  DROP TABLE #TempTable_Days

END -- END PROCEDURE
GO

-- PERMISSIONS

GRANT EXECUTE ON [dbo].[PR_collection_by_machine_and_date] TO [wggui] WITH GRANT OPTION
GO

/******* TRIGGERS *******/

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[MultiSiteTrigger_SiteAccountUpdate]') AND type in (N'TR'))
DROP TRIGGER [dbo].[MultiSiteTrigger_SiteAccountUpdate]
GO

CREATE TRIGGER [dbo].[MultiSiteTrigger_SiteAccountUpdate] ON [dbo].[accounts]
   AFTER UPDATE
NOT FOR REPLICATION
AS 
BEGIN
    DECLARE @AccountId          AS BIGINT
    DECLARE @hash0              AS varbinary(20)
    DECLARE @hash1              AS varbinary(20)
    DECLARE @value              AS nvarchar(max)
    DECLARE @changed            AS bit
    DECLARE @updated            AS BIT    
-- For Last Activity 
    DECLARE @LastActyvity0          AS DATETIME
    DECLARE @ReBalance0             AS MONEY
    DECLARE @PromoREBalance0        AS MONEY
    DECLARE @PromoNRBalance0        AS MONEY
    DECLARE @SessionReBalance0      AS MONEY
    DECLARE @SessionPromoREBalance0 AS MONEY
    DECLARE @SessionPromoNRBalance0 AS MONEY
    DECLARE @LastActyvity1          AS DATETIME
    DECLARE @ReBalance1             AS MONEY
    DECLARE @PromoREBalance1        AS MONEY
    DECLARE @PromoNRBalance1        AS MONEY
    DECLARE @SessionReBalance1      AS MONEY
    DECLARE @SessionPromoREBalance1 AS MONEY
    DECLARE @SessionPromoNRBalance1 AS MONEY
    
    IF (UPDATE(AC_MS_CHANGE_GUID))  
         return

  IF NOT EXISTS (SELECT   1 
                   FROM   GENERAL_PARAMS 
                  WHERE   GP_GROUP_KEY   = N'Site' 
                    AND   GP_SUBJECT_KEY = N'MultiSiteMember' 
                    AND   GP_KEY_VALUE   = N'1')
  BEGIN
        RETURN
  END

  SET @updated = 0;  

  IF   UPDATE (AC_TRACK_DATA)
    OR UPDATE (AC_HOLDER_NAME)
    OR UPDATE (AC_HOLDER_ID)
    OR UPDATE (AC_HOLDER_ID_TYPE)
    OR UPDATE (AC_HOLDER_ADDRESS_01)
    OR UPDATE (AC_HOLDER_ADDRESS_02)
    OR UPDATE (AC_HOLDER_ADDRESS_03)
    OR UPDATE (AC_HOLDER_CITY)
    OR UPDATE (AC_HOLDER_ZIP)
    OR UPDATE (AC_HOLDER_EMAIL_01)
    OR UPDATE (AC_HOLDER_EMAIL_02)
    OR UPDATE (AC_HOLDER_TWITTER_ACCOUNT)
    OR UPDATE (AC_HOLDER_PHONE_NUMBER_01)
    OR UPDATE (AC_HOLDER_PHONE_NUMBER_02)
    OR UPDATE (AC_HOLDER_COMMENTS)
    OR UPDATE (AC_HOLDER_ID1)
    OR UPDATE (AC_HOLDER_ID2)
    OR UPDATE (AC_HOLDER_DOCUMENT_ID1)
    OR UPDATE (AC_HOLDER_DOCUMENT_ID2)
    OR UPDATE (AC_HOLDER_NAME1)
    OR UPDATE (AC_HOLDER_NAME2)
    OR UPDATE (AC_HOLDER_NAME3)
    OR UPDATE (AC_HOLDER_GENDER)
    OR UPDATE (AC_HOLDER_MARITAL_STATUS)
    OR UPDATE (AC_HOLDER_BIRTH_DATE)
    OR UPDATE (AC_HOLDER_WEDDING_DATE)
    OR UPDATE (AC_HOLDER_LEVEL)
    OR UPDATE (AC_HOLDER_LEVEL_NOTIFY)
    OR UPDATE (AC_HOLDER_LEVEL_ENTERED)
    OR UPDATE (AC_HOLDER_LEVEL_EXPIRATION)
    OR UPDATE (AC_PIN)
    OR UPDATE (AC_PIN_FAILURES)
    OR UPDATE (AC_PIN_LAST_MODIFIED)
    OR UPDATE (AC_BLOCKED)
    OR UPDATE (AC_ACTIVATED)
    OR UPDATE (AC_BLOCK_REASON)
    OR UPDATE (AC_HOLDER_IS_VIP)
    OR UPDATE (AC_HOLDER_TITLE)
    OR UPDATE (AC_HOLDER_NAME4)
    OR UPDATE (AC_HOLDER_PHONE_TYPE_01)
    OR UPDATE (AC_HOLDER_PHONE_TYPE_02)
    OR UPDATE (AC_HOLDER_STATE)
    OR UPDATE (AC_HOLDER_COUNTRY)
    OR UPDATE (AC_USER_TYPE)
    OR UPDATE (AC_POINTS_STATUS)
    OR UPDATE (AC_DEPOSIT)
    OR UPDATE (AC_CARD_PAID)
    OR UPDATE (AC_BLOCK_DESCRIPTION)    
    OR UPDATE (AC_MS_CREATED_ON_SITE_ID)
    OR UPDATE (AC_EXTERNAL_REFERENCE)
    OR UPDATE (AC_HOLDER_OCCUPATION)     
    OR UPDATE (AC_HOLDER_EXT_NUM)       
    OR UPDATE (AC_HOLDER_NATIONALITY)
    OR UPDATE (AC_HOLDER_BIRTH_COUNTRY)
    OR UPDATE (AC_HOLDER_FED_ENTITY)
    OR UPDATE (AC_HOLDER_ID1_TYPE)
    OR UPDATE (AC_HOLDER_ID2_TYPE)
    OR UPDATE (AC_HOLDER_ID3_TYPE)
    OR UPDATE (AC_HOLDER_ID3)
    OR UPDATE (AC_HOLDER_HAS_BENEFICIARY)
    OR UPDATE (AC_BENEFICIARY_NAME)
    OR UPDATE (AC_BENEFICIARY_NAME1)
    OR UPDATE (AC_BENEFICIARY_NAME2)
    OR UPDATE (AC_BENEFICIARY_NAME3)
    OR UPDATE (AC_BENEFICIARY_BIRTH_DATE)
    OR UPDATE (AC_BENEFICIARY_GENDER)
    OR UPDATE (AC_BENEFICIARY_OCCUPATION)
    OR UPDATE (AC_BENEFICIARY_ID1_TYPE)
    OR UPDATE (AC_BENEFICIARY_ID1)
    OR UPDATE (AC_BENEFICIARY_ID2_TYPE)
    OR UPDATE (AC_BENEFICIARY_ID2)
    OR UPDATE (AC_BENEFICIARY_ID3_TYPE)
    OR UPDATE (AC_BENEFICIARY_ID3)
    OR UPDATE (AC_HOLDER_OCCUPATION_ID)
    OR UPDATE (AC_BENEFICIARY_OCCUPATION_ID)
        SET @updated = 1;      
  -- For Last Activity  
  IF   UPDATE (AC_LAST_ACTIVITY)
    OR UPDATE (AC_RE_BALANCE)
    OR UPDATE (AC_PROMO_RE_BALANCE)
    OR UPDATE (AC_PROMO_NR_BALANCE)
    OR UPDATE (AC_IN_SESSION_RE_BALANCE)
    OR UPDATE (AC_IN_SESSION_PROMO_RE_BALANCE)
    OR UPDATE (AC_IN_SESSION_PROMO_NR_BALANCE)
        SET @updated = 2;
        
  IF (@updated = 0) RETURN

  DECLARE PersonalInfoCursor CURSOR FOR 
   SELECT   AC_ACCOUNT_ID
          , HASHBYTES ('SHA1',  ISNULL(AC_TRACK_DATA,        '')
                              + ISNULL(AC_HOLDER_NAME,       '')
                              + ISNULL(AC_HOLDER_ID,         '')
                              + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_ID_TYPE), '')
                              + ISNULL(AC_HOLDER_ADDRESS_01, '')
                              + ISNULL(AC_HOLDER_ADDRESS_02, '')
                              + ISNULL(AC_HOLDER_ADDRESS_03, '')
                              + ISNULL(AC_HOLDER_CITY,       '')
                              + ISNULL(AC_HOLDER_ZIP,        '')
                              + ISNULL(AC_HOLDER_EMAIL_01,   '')
                              + ISNULL(AC_HOLDER_EMAIL_02,   '')
                              + ISNULL(AC_HOLDER_TWITTER_ACCOUNT, '')
                              + ISNULL(AC_HOLDER_PHONE_NUMBER_01, '')
                              + ISNULL(AC_HOLDER_PHONE_NUMBER_02, '')
                              + ISNULL(AC_HOLDER_COMMENTS,        '')
                              + ISNULL(AC_HOLDER_ID1, '')
                              + ISNULL(AC_HOLDER_ID2, '')
                              + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_DOCUMENT_ID1), '')
                              + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_DOCUMENT_ID2), '')
                              + ISNULL(AC_HOLDER_NAME1, '')
                              + ISNULL(AC_HOLDER_NAME2, '')
                              + ISNULL(AC_HOLDER_NAME3, '')
                              + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_GENDER),         '')
                              + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_MARITAL_STATUS), '')
                              + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_BIRTH_DATE,   21), '')
                              + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_WEDDING_DATE, 21), '')
                              + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_LEVEL),                '')
                              + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_LEVEL_NOTIFY),         '')
                              + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_LEVEL_ENTERED,    21), '')
                              + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_LEVEL_EXPIRATION, 21), '')
                              + ISNULL(CONVERT(NVARCHAR, AC_PIN),          '')
                              + ISNULL(CONVERT(NVARCHAR, AC_PIN_FAILURES), '')
                              + ISNULL(CONVERT(NVARCHAR, AC_PIN_LAST_MODIFIED, 21), '')
                              + ISNULL(CONVERT(NVARCHAR, AC_BLOCKED),           '')
                              + ISNULL(CONVERT(NVARCHAR, AC_ACTIVATED),         '')
                              + ISNULL(CONVERT(NVARCHAR, AC_BLOCK_REASON),         '')
                              + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_IS_VIP),        '')
                              + ISNULL(AC_HOLDER_TITLE                        , '')
                              + ISNULL(AC_HOLDER_NAME4                        , '')
                              + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_PHONE_TYPE_01),        '')
                              + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_PHONE_TYPE_02),        '')
                              + ISNULL(AC_HOLDER_STATE                        , '')
                              + ISNULL(AC_HOLDER_COUNTRY                      , '') 
                              + ISNULL(CONVERT(NVARCHAR,AC_USER_TYPE)         , '')
                              + ISNULL(CONVERT(NVARCHAR,AC_POINTS_STATUS)     , '') 
                              + ISNULL(CONVERT(NVARCHAR,AC_DEPOSIT)           , '')
                              + ISNULL(CONVERT(NVARCHAR,AC_CARD_PAID)         , '')
                              + ISNULL(AC_BLOCK_DESCRIPTION, '')
                              + ISNULL(CONVERT(NVARCHAR,AC_MS_CREATED_ON_SITE_ID), '')
                              + ISNULL(AC_EXTERNAL_REFERENCE, '')
                              + ISNULL(AC_HOLDER_OCCUPATION, '')
                              + ISNULL(AC_HOLDER_EXT_NUM, '')
                              + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_NATIONALITY),         '')
                              + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_BIRTH_COUNTRY), '')
                              + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_FED_ENTITY), '')
                              + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_ID1_TYPE), '')
                              + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_ID2_TYPE), '')
                              + ISNULL(AC_HOLDER_ID3_TYPE, '')
                              + ISNULL(AC_HOLDER_ID3, '')
                              + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_HAS_BENEFICIARY), '')
                              + ISNULL(AC_BENEFICIARY_NAME, '')
                              + ISNULL(AC_BENEFICIARY_NAME1, '')
                              + ISNULL(AC_BENEFICIARY_NAME2, '')
                              + ISNULL(AC_BENEFICIARY_NAME3, '')
                              + ISNULL(CONVERT(NVARCHAR, AC_BENEFICIARY_BIRTH_DATE,   21), '')
                              + ISNULL(CONVERT(NVARCHAR, AC_BENEFICIARY_GENDER), '')  
                              + ISNULL(AC_BENEFICIARY_OCCUPATION, '')
                              + ISNULL(CONVERT(NVARCHAR, AC_BENEFICIARY_ID1_TYPE), '')        
                              + ISNULL(AC_BENEFICIARY_ID1, '')
                              + ISNULL(CONVERT(NVARCHAR, AC_BENEFICIARY_ID2_TYPE), '')        
                              + ISNULL(AC_BENEFICIARY_ID2, '')
                              + ISNULL(AC_BENEFICIARY_ID3_TYPE, '')        
                              + ISNULL(AC_BENEFICIARY_ID3, '') 
                              + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_OCCUPATION_ID), '')
                              + ISNULL(CONVERT(NVARCHAR, AC_BENEFICIARY_OCCUPATION_ID), '')) 
          , ISNULL(AC_LAST_ACTIVITY, GETDATE())
          , ISNULL(AC_RE_BALANCE, 0)
          , ISNULL(AC_PROMO_RE_BALANCE, 0)
          , ISNULL(AC_PROMO_NR_BALANCE, 0)
          , ISNULL(AC_IN_SESSION_RE_BALANCE, 0)
          , ISNULL(AC_IN_SESSION_PROMO_RE_BALANCE, 0)
          , ISNULL(AC_IN_SESSION_PROMO_NR_BALANCE, 0)
     FROM   INSERTED
    WHERE   AC_TRACK_DATA not like '%-NEW-%'
      AND   AC_TYPE NOT IN (4, 5) -- Virtual accounts don't have to be sent to the Center
      
  SET NOCOUNT ON;

  OPEN PersonalInfoCursor

  FETCH NEXT FROM PersonalInfoCursor INTO   @AccountId
                                          , @hash1
                                          , @LastActyvity1
                                          , @ReBalance1
                                          , @PromoREBalance1
                                          , @PromoNRBalance1
                                          , @SessionReBalance1
                                          , @SessionPromoREBalance1
                                          , @SessionPromoNRBalance1

  WHILE @@FETCH_STATUS = 0
  BEGIN

    SELECT   @hash0                  = AC_MS_HASH          
           , @LastActyvity0          = AC_LAST_ACTIVITY
           , @ReBalance0             = AC_RE_BALANCE
           , @PromoREBalance0        = AC_PROMO_RE_BALANCE
           , @PromoNRBalance0        = AC_PROMO_NR_BALANCE
           , @SessionReBalance0      = AC_IN_SESSION_RE_BALANCE
           , @SessionPromoREBalance0 = AC_IN_SESSION_PROMO_RE_BALANCE
           , @SessionPromoNRBalance0 = AC_IN_SESSION_PROMO_NR_BALANCE
      FROM   DELETED 
     WHERE   AC_ACCOUNT_ID = @AccountId
		 
    SELECT   @changed = CASE WHEN ( @hash0 = @hash1 ) THEN 0 ELSE 1 END

    IF @changed = 1
    BEGIN
      DECLARE @new_id as uniqueidentifier
           
      SET @new_id = NEWID()
           
      -- Personal Info
      UPDATE   ACCOUNTS
         SET   AC_MS_HASH         = @hash1
             , AC_MS_CHANGE_GUID  = @new_id
       WHERE   AC_ACCOUNT_ID      = @AccountId
            
      IF NOT EXISTS (SELECT 1 FROM MS_SITE_PENDING_ACCOUNTS WHERE SPA_ACCOUNT_ID = @AccountId)
        INSERT INTO   MS_SITE_PENDING_ACCOUNTS (SPA_ACCOUNT_ID, SPA_GUID) VALUES (@AccountId, @new_id )
      ELSE 
        UPDATE MS_SITE_PENDING_ACCOUNTS SET SPA_GUID = @new_id WHERE SPA_ACCOUNT_ID = @AccountId

    END

    SELECT @changed = CASE WHEN (    @LastActyvity0          = @LastActyvity1          
                                 AND @ReBalance0             = @ReBalance1             
                                 AND @PromoREBalance0        = @PromoREBalance1        
                                 AND @PromoNRBalance0        = @PromoNRBalance1        
                                 AND @SessionReBalance0      = @SessionReBalance1      
                                 AND @SessionPromoREBalance0 = @SessionPromoREBalance1 
                                 AND @SessionPromoNRBalance0 = @SessionPromoNRBalance1 ) THEN 0
                           ELSE 1 END

    IF (@changed = 1)
    BEGIN

      IF NOT EXISTS (SELECT 1 FROM MS_SITE_PENDING_LAST_ACTIVITY WHERE LAA_ACCOUNT_ID = @AccountId)
        INSERT INTO  MS_SITE_PENDING_LAST_ACTIVITY (LAA_ACCOUNT_ID) VALUES (@AccountId )
      ELSE 
        UPDATE MS_SITE_PENDING_LAST_ACTIVITY SET LAA_ACCOUNT_ID = @AccountId WHERE LAA_ACCOUNT_ID = @AccountId

    END

    FETCH NEXT FROM PersonalInfoCursor INTO   @AccountId
                                            , @hash1
                                            , @LastActyvity1
                                            , @ReBalance1
                                            , @PromoREBalance1
                                            , @PromoNRBalance1
                                            , @SessionReBalance1
                                            , @SessionPromoREBalance1
                                            , @SessionPromoNRBalance1

  END

  CLOSE PersonalInfoCursor
  DEALLOCATE PersonalInfoCursor

END
GO

--
-- Disable/enable triggers for site members
--

IF EXISTS (SELECT   1
             FROM   GENERAL_PARAMS 
            WHERE   GP_GROUP_KEY   = N'Site' 
              AND   GP_SUBJECT_KEY = N'MultiSiteMember' 
              AND   GP_KEY_VALUE   = N'1')
BEGIN
      EXEC MultiSiteTriggersEnable 1
END
ELSE
BEGIN
      EXEC MultiSiteTriggersEnable 0
END
GO
