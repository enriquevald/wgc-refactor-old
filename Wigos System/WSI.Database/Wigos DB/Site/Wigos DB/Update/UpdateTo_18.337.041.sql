/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 336;

SET @New_ReleaseId = 337;
SET @New_ScriptName = N'UpdateTo_18.337.041.sql';
SET @New_Description = N'GetBankTransacctionsToReconciliate'; 

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/**** GENERAL PARAM *****/

/******* TABLES  *******/

/******* INDEXES *******/

/******* RECORDS *******/

/******* PROCEDURES *******/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetBankTransacctionsToReconciliate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetBankTransacctionsToReconciliate]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetBankTransacctionsToReconciliate]
@pUser INT = 0,
@pWithdrawal VARCHAR(20)= ''
AS
BEGIN

SELECT 
  T.pt_created, 
  T.pt_id, 
  T.pt_operation_id, 
  T.pt_total_amount, 
  T.pt_card_number,
  R.ptc_reconciliate,
  U.gu_full_name  
FROM pinpad_transactions T
  INNER JOIN gui_users U
    ON U.gu_user_id = T.pt_user_id
  INNER JOIN account_operations A
    ON A.ao_operation_id = T.pt_operation_id
  INNER JOIN pinpad_transactions_reconciliation R
    ON R.ptc_id = T.pt_id
WHERE T.pt_status = 1
  AND A.ao_code IN (1,17)
  AND U.gu_user_id = @pUser
  AND CONVERT(VARCHAR(20),ptc_drawal,112) + ' ' + CONVERT(VARCHAR(20),ptc_drawal,108) = @pWithdrawal
  AND R.ptc_reconciliate = 0
  
ORDER BY T.pt_created

END

GO 

GRANT EXECUTE ON GetBankTransacctionsToReconciliate TO wggui WITH GRANT OPTION 

