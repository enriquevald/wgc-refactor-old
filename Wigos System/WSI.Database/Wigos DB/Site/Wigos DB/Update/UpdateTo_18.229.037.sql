/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 228;

SET @New_ReleaseId = 229;
SET @New_ScriptName = N'UpdateTo_18.229.037.sql';
SET @New_Description = N'Changes for: lcd, cage_movements, progressives - New GP: MassiveCollection.PreselectCageSession';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/******* TABLES *******/

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[lcd_images]') and name = 'cim_resource_length')
	ALTER TABLE dbo.lcd_images ADD
			   cim_resource_length int NOT NULL CONSTRAINT DF_lcd_images_cim_resource_length DEFAULT 0,
			   cim_resource_name nvarchar(50) NOT NULL CONSTRAINT DF_lcd_images_cim_resource_name DEFAULT ''
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[cage_movements]') and name = 'cgm_related_id')
	ALTER TABLE dbo.cage_movements ADD cgm_related_id bigint NULL
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[progressives_provisions]') and name = 'pgp_status')
	ALTER TABLE dbo.progressives_provisions ADD pgp_status int NOT NULL DEFAULT 0
GO


/******* INDEXES *******/

IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[tickets]') AND name = N'IX_ti_last_action_datetime')
  CREATE NONCLUSTERED INDEX [IX_ti_last_action_datetime] ON [dbo].[tickets] 
  (
    ti_last_action_datetime ASC
  )
  INCLUDE (
    ti_last_action_terminal_id,
    ti_type_id, 
    ti_amount 
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

/******* RECORDS *******/

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cage' AND GP_SUBJECT_KEY ='MassiveCollection.PreselectCageSession')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key]
             ,[gp_subject_key]
             ,[gp_key_value])
       VALUES
             ('Cage'
             ,'MassiveCollection.PreselectCageSession'
             ,'0');
GO

/******* STORED PROCEDURES *******/

/******* CAGE SECTION ********/



/*
      PAR�METROS GENERALES:  Cage -> Meters
*/

--Indica si los conceptos de Salida solo se pueden 'pagar' en moneda Nacional (por defecto verdadero)
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cage' AND GP_SUBJECT_KEY = 'Meters.OutputConcepts.OnlyNationalCurrency')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('Cage','Meters.OutputConcepts.OnlyNationalCurrency','1')
GO
--N� de items por fila en la pantalla de conceptos del Cajero (por defecto 2 botones por fila)
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cage' AND GP_SUBJECT_KEY = 'Meters.Cashier.MaxItemsPerRow')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('Cage','Meters.Cashier.MaxItemsPerRow','2')
GO
--Indica si los contadores de boveda est�n habilitados o no (TODO: EN GUI/CAJERO NO VISIBLES LOS CONTADORES)
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cage' AND GP_SUBJECT_KEY = 'Meters.Enabled')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('Cage','Meters.Enabled','1')




/*
      ELMINACI�N DE IDENTIDAD-SEED DE LA PK DE LA TABLA cage_source_target
      Necesario para poder generar nuestros propios rangos y reservar id's para sistema/personalizados
*/

GO

ALTER TABLE dbo.cage_source_target
      DROP CONSTRAINT PK_CAGE_SOURCE_TARGET
GO

CREATE TABLE dbo.Tmp_cage_source_target
(
      [cst_source_target_id] [bigint] NOT NULL,
      [cst_source_target_name] [nvarchar](50) NOT NULL,
      [cst_source] [bit] NOT NULL,
      [cst_target] [bit] NOT NULL,
CONSTRAINT [PK_CAGE_SOURCE_TARGET] PRIMARY KEY CLUSTERED 
(
      [cst_source_target_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
IF EXISTS(SELECT * FROM dbo.cage_source_target)
EXEC('INSERT INTO dbo.Tmp_cage_source_target (cst_source_target_id, cst_source_target_name, cst_source, cst_target)
SELECT cst_source_target_id, cst_source_target_name, cst_source, cst_target FROM dbo.cage_source_target WITH (HOLDLOCK TABLOCKX)')
GO
DROP TABLE dbo.cage_source_target
GO
EXECUTE sp_rename N'dbo.Tmp_cage_source_target', N'cage_source_target', 'OBJECT'

GO


/* CREACI�N NUEVAS TABLAS */

GO
CREATE TABLE [dbo].[cage_concepts](
      [cc_concept_id] [bigint] NOT NULL,
      [cc_description] [nvarchar](50) NOT NULL,
      [cc_is_provision] [bit] NOT NULL,
      [cc_show_in_report] [bit] NOT NULL,
      [cc_unit_price] [money] NULL,
      [cc_name] [nvarchar](20) NOT NULL,
      [cc_type] [int] NOT NULL,
      [cc_enabled] [bit] NULL,
CONSTRAINT [PK_cage_concepts] PRIMARY KEY CLUSTERED 
(
      [cc_concept_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
CREATE TABLE [dbo].[cage_source_target_concepts](
      [cstc_concept_id] [bigint] NOT NULL,
      [cstc_source_target_id] [bigint] NOT NULL,
      [cstc_type] [int] NOT NULL,
      [cstc_only_national_currency] [bit] NOT NULL,
      [cstc_cashier_action] [int] NULL,
      [cstc_cashier_container] [int] NULL,
      [cstc_cashier_btn_group] [varchar](50) NULL,
      [cstc_enabled] [bit] NOT NULL,
      [cstc_price_factor] [money] NOT NULL,
CONSTRAINT [PK_cage_source_target_concepts] PRIMARY KEY CLUSTERED 
(
      [cstc_concept_id] ASC,
      [cstc_source_target_id] ASC,
      [cstc_type] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[cage_source_target_concepts] ADD  CONSTRAINT [DF_cage_source_target_concepts_cstc_only_national_currency]  DEFAULT ((0)) FOR [cstc_only_national_currency]
GO
ALTER TABLE [dbo].[cage_source_target_concepts] ADD  CONSTRAINT [DF_cage_source_target_concepts_cstc_price_factor]  DEFAULT ((1)) FOR [cstc_price_factor]

GO
CREATE TABLE [dbo].[cage_meters](
      [cm_source_target_id] [bigint] NOT NULL,
      [cm_concept_id] [bigint] NOT NULL,
      [cm_iso_code] [nvarchar](3) NOT NULL,
      [cm_value] [money] NOT NULL,
      [cm_value_in] [money] NOT NULL,
      [cm_value_out] [money] NOT NULL,
CONSTRAINT [PK_cage_meters] PRIMARY KEY CLUSTERED 
(
      [cm_source_target_id] ASC,
      [cm_concept_id] ASC,
      [cm_iso_code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]


GO

ALTER TABLE [dbo].[cage_meters] ADD  CONSTRAINT [DF_cage_meters_cm_value_in]  DEFAULT ((0)) FOR [cm_value_in]
GO

ALTER TABLE [dbo].[cage_meters] ADD  CONSTRAINT [DF_cage_meters_cm_value_out]  DEFAULT ((0)) FOR [cm_value_out]
GO

CREATE TABLE [dbo].[cage_session_meters](
      [csm_cage_session_id] [bigint] NOT NULL,
      [csm_source_target_id] [bigint] NOT NULL,
      [csm_concept_id] [bigint] NOT NULL,
      [csm_iso_code] [nvarchar](3) NOT NULL,
      [csm_value] [money] NOT NULL,
      [csm_value_in] [money] NOT NULL,
      [csm_value_out] [money] NOT NULL,
CONSTRAINT [PK_cage_session_meters] PRIMARY KEY CLUSTERED 
(
      [csm_cage_session_id] ASC,
      [csm_source_target_id] ASC,
      [csm_concept_id] ASC,
      [csm_iso_code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[cage_session_meters] ADD  CONSTRAINT [DF_cage_session_meters_csm_value]  DEFAULT ((0)) FOR [csm_value]
GO

ALTER TABLE [dbo].[cage_session_meters] ADD  CONSTRAINT [DF_cage_session_meters_csm_value_in]  DEFAULT ((0)) FOR [csm_value_in]
GO

ALTER TABLE [dbo].[cage_session_meters] ADD  CONSTRAINT [DF_cage_session_meters_csm_value_out]  DEFAULT ((0)) FOR [csm_value_out]
GO


/* 

      STORED PROCEDURES 
      TODO:  CREAR SP's EN SOURCE SAFE

*/

GO
CREATE PROCEDURE [dbo].[CageCreateMeters]
        @pSourceTargetId BIGINT
      , @pConceptId BIGINT
      , @pCageSessionId BIGINT = NULL
      , @CreateMetersOption INT
      , @CurrencyForced VARCHAR(3) =  NULL
  AS
BEGIN   

      -- @@CreateMetersOption  =   0 - Create in cage_meters and cage_session_meters
      --                               =  1 - Create in cage_meters only
      --                              =  2 - Create in cage_session_meters only 


      DECLARE @Currencies VARCHAR(100)
      DECLARE @OnlyNationalCurrency BIT
      
      SET @OnlyNationalCurrency = 0
      
      SELECT @OnlyNationalCurrency = CSTC_ONLY_NATIONAL_CURRENCY 
        FROM CAGE_SOURCE_TARGET_CONCEPTS 
       WHERE CSTC_SOURCE_TARGET_ID = @pSourceTargetId
         AND  CSTC_CONCEPT_ID = @pConceptId  

      -- Get national currency
      IF @OnlyNationalCurrency = 1 AND @CurrencyForced IS NULL
            SELECT @Currencies = GP_KEY_VALUE FROM GENERAL_PARAMS 
         WHERE GP_GROUP_KEY = 'RegionalOptions' 
           AND GP_SUBJECT_KEY = 'CurrencyISOCode'
    
    -- Get accepted currencies
    ELSE
    BEGIN
            IF @CurrencyForced IS NOT NULL
                  SET @Currencies = @CurrencyForced
            ELSE
                  SELECT @Currencies = GP_KEY_VALUE FROM GENERAL_PARAMS 
                   WHERE GP_GROUP_KEY = 'RegionalOptions' 
                     AND GP_SUBJECT_KEY = 'CurrenciesAccepted'
    END
    
    -- Split currencies ISO codes
    SELECT SST_VALUE AS CURRENCY_ISO_CODE INTO #TMP_CURRENCIES_ISO_CODES FROM [SplitStringIntoTable] (@Currencies, ';', 1)
      
      
      -- Create a new meters for each currency
      DECLARE @CurrentCurrency VARCHAR(3)
      
      DECLARE Curs CURSOR FOR 
    SELECT CURRENCY_ISO_CODE
      FROM #TMP_CURRENCIES_ISO_CODES 
      
      SET NOCOUNT ON;

    OPEN Curs

    FETCH NEXT FROM Curs INTO @CurrentCurrency
    
    WHILE @@FETCH_STATUS = 0
    BEGIN
      
            -- CAGE METERS --
            IF @CreateMetersOption = 0 OR @CreateMetersOption = 1
            BEGIN
                  IF NOT EXISTS (SELECT TOP 1 CM_VALUE 
                                       FROM CAGE_METERS 
                                      WHERE CM_SOURCE_TARGET_ID = @pSourceTargetId
                                        AND CM_CONCEPT_ID = @pConceptId
                                        AND CM_ISO_CODE = @CurrentCurrency)
                  INSERT INTO CAGE_METERS
                               ( CM_SOURCE_TARGET_ID
                               , CM_CONCEPT_ID
                               , CM_ISO_CODE
                               , CM_VALUE
                               , CM_VALUE_IN
                               , CM_VALUE_OUT )
                        VALUES
                               ( @pSourceTargetId
                               , @pConceptId
                               , @CurrentCurrency
                               , 0
                               , 0
                               , 0 )
            END

            -- CAGE SESSION METERS --
            IF (@CreateMetersOption = 0 OR @CreateMetersOption = 2) AND @pCageSessionId IS NOT NULL
            BEGIN
                  IF NOT EXISTS (SELECT TOP 1 CSM_VALUE 
                                       FROM CAGE_SESSION_METERS 
                                      WHERE CSM_SOURCE_TARGET_ID = @pSourceTargetId
                                        AND CSM_CONCEPT_ID = @pConceptId
                                        AND CSM_ISO_CODE = @CurrentCurrency
                                        AND CSM_CAGE_SESSION_ID = @pCageSessionId)
                  INSERT INTO CAGE_SESSION_METERS
                               ( CSM_CAGE_SESSION_ID
                               , CSM_SOURCE_TARGET_ID
                               , CSM_CONCEPT_ID
                               , CSM_ISO_CODE
                               , CSM_VALUE
                               , CSM_VALUE_IN
                               , CSM_VALUE_OUT )
                        VALUES
                               ( @pCageSessionId
                               , @pSourceTargetId
                               , @pConceptId
                               , @CurrentCurrency
                               , 0
                                , 0
                               , 0 )
                          
            END
            
            FETCH NEXT FROM Curs INTO @CurrentCurrency
      
      END

    CLOSE Curs
    DEALLOCATE Curs
    DROP TABLE #TMP_CURRENCIES_ISO_CODES
              
END -- CageInsertNewMeters

GO

CREATE PROCEDURE [dbo].[CageGetGlobalReportData]
        @pCageSessionIds NVARCHAR(MAX) = NULL
  AS
BEGIN

      -- ENUM
      DECLARE @CIRCULANTING INT 
      DECLARE @STATE_TAX INT  
      DECLARE @FEDERAL_TAX INT  
      DECLARE @PROV_PROGRESIV INT 
      DECLARE @CARD_DEPOSIT INT 
      
      DECLARE @NationalIsoCode NVARCHAR(3)
      DECLARE @CageOperationType_FromTerminal INT 

      SET @CIRCULANTING = 1
      SET @STATE_TAX = 2 
      SET @FEDERAL_TAX = 3
      SET @PROV_PROGRESIV = 4
      SET @CARD_DEPOSIT = 5
      SET @CageOperationType_FromTerminal = 104
      
      -- TABLE[0]: Get cage stock
    SELECT CGS_ISO_CODE                                          
         , CGS_DENOMINATION                                      
             , CGS_QUANTITY 
      FROM (SELECT CGS_ISO_CODE                                          
                   , CGS_DENOMINATION                                      
                         , CGS_QUANTITY                                          
                    FROM CAGE_STOCK
                  
                  UNION ALL
                  
                  SELECT C.CH_ISO_CODE AS CGS_ISO_CODE
                        , C.CH_DENOMINATION AS CGS_DENOMINATION
                        , CST.CHSK_QUANTITY AS CGS_QUANTITY
                    FROM CHIPS_STOCK AS CST
                           INNER JOIN CHIPS AS C ON CST.CHSK_CHIP_ID = C.CH_CHIP_ID
                  ) AS _tbl         
                                                   
  ORDER BY CGS_ISO_CODE                                          
              ,CASE WHEN CGS_DENOMINATION >= 0 THEN CGS_DENOMINATION 
                      ELSE CGS_DENOMINATION * (-100000) END -- show negative denominations at the end (coins, cheks, etc.)
                
                                            

      IF @pCageSessionIds IS NOT NULL
      BEGIN
            -- Session IDs Split
            SELECT SST_VALUE AS CAGE_SESSION_ID INTO #TMP_CAGE_SESSIONS FROM [SplitStringIntoTable] (@pCageSessionIds,','  , 1)
      
          -- TABLE[1]: Get cage sessions information
            SELECT      CSM.CSM_SOURCE_TARGET_ID AS CM_SOURCE_TARGET_ID
                    , CSM.CSM_CONCEPT_ID AS CM_CONCEPT_ID
                    , CSM.CSM_ISO_CODE AS CM_ISO_CODE
                    , SUM(CSM.CSM_VALUE_IN) AS CM_VALUE_IN
                    , SUM(CSM.CSM_VALUE_OUT) AS CM_VALUE_OUT
                    , CST.CST_SOURCE_TARGET_NAME
                    , CC.CC_DESCRIPTION
              FROM      CAGE_SESSION_METERS AS CSM
                        INNER JOIN CAGE_SOURCE_TARGET_CONCEPTS AS CSTC ON CSTC.CSTC_CONCEPT_ID = CSM.CSM_CONCEPT_ID AND CSTC.CSTC_SOURCE_TARGET_ID = CSM.CSM_SOURCE_TARGET_ID
                        INNER JOIN CAGE_CONCEPTS AS CC ON CC.CC_CONCEPT_ID = CSTC.CSTC_CONCEPT_ID
                        INNER JOIN CAGE_SOURCE_TARGET AS CST ON CST.CST_SOURCE_TARGET_ID = CSTC.CSTC_SOURCE_TARGET_ID
            WHERE      CSM.CSM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
               AND      CC.CC_SHOW_IN_REPORT = 1
               AND      CC.CC_ENABLED = 1
               AND      CC.CC_TYPE = 1
             AND  CSTC.CSTC_ENABLED = 1
        GROUP BY  CSM.CSM_SOURCE_TARGET_ID, CSM.CSM_CONCEPT_ID, CSM.CSM_ISO_CODE, CST.CST_SOURCE_TARGET_NAME, CC.CC_DESCRIPTION
        ORDER BY  CSM.CSM_SOURCE_TARGET_ID                   

            -- TABLE[2]: Get dynamic liabilities
            SELECT      CC.CC_CONCEPT_ID AS LIABILITY_CONCEPT_ID
                    , CC.CC_DESCRIPTION AS LIABILITY_NAME 
                    , CSM.CSM_ISO_CODE AS LIABILITY_ISO_CODE
                    , SUM(CSM.CSM_VALUE) AS LIABILITY_VALUE
              FROM      CAGE_SESSION_METERS AS CSM
                        INNER JOIN CAGE_CONCEPTS AS CC ON CC.CC_CONCEPT_ID = CSM.CSM_CONCEPT_ID
            WHERE      CC.CC_IS_PROVISION = 1
               AND      CSM.CSM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS) 
      GROUP BY    CC.CC_CONCEPT_ID, CC.CC_DESCRIPTION, CSM.CSM_ISO_CODE
      ORDER BY CC.CC_CONCEPT_ID ASC
      
            -- TABLE[3]: Get session sumary
            SELECT @NationalIsoCode = GP_KEY_VALUE 
              FROM GENERAL_PARAMS 
             WHERE GP_GROUP_KEY = 'RegionalOptions' 
               AND GP_SUBJECT_KEY = 'CurrencyISOCode'

            SELECT CMD_ISO_CODE AS ISO_CODE
               , ISNULL(ORIGIN, 0) AS ORIGIN
               , SUM(CMD_DEPOSITS) AS DEPOSITS
               , SUM(CMD_WITHDRAWALS) AS WITHDRAWALS
                  
              FROM      (SELECT CMD_ISO_CODE
                               , ORIGIN
                               , SUM(CMD_DEPOSITS) CMD_DEPOSITS
                               , SUM(CMD_WITHDRAWALS) CMD_WITHDRAWALS
                           
                           FROM     (SELECT CASE WHEN CMD_QUANTITY >= 0 THEN NULL
                                                     ELSE CMD_QUANTITY END  AS ORIGIN
                                           , CMD_ISO_CODE
                                           , CMD_DENOMINATION
                                           , CASE WHEN CGM_TYPE >= 100 AND CGM_TYPE NOT IN (2, 102, 99, 199) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY < 0 THEN CMD_DENOMINATION
                                                                                                                                                                          ELSE CMD_QUANTITY * CMD_DENOMINATION END), 0)
                                                     ELSE 0 END AS CMD_DEPOSITS
                                           , CASE WHEN CGM_TYPE < 100 AND CGM_TYPE NOT IN (2, 102, 99, 199) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY < 0 THEN CMD_DENOMINATION             
                                                                                                                                                                           ELSE CMD_QUANTITY * CMD_DENOMINATION END), 0)                              
                                                      ELSE 0 END AS CMD_WITHDRAWALS

                                      FROM CAGE_MOVEMENTS 
                                               INNER JOIN   CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          
                                     WHERE CGM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)

                                GROUP BY CMD_ISO_CODE, CMD_DENOMINATION, CGM_TYPE, CMD_QUANTITY
                                ) AS T1
                                
                        GROUP BY ORIGIN,CMD_ISO_CODE

                        UNION ALL 

                        --The next one is for the Stackers / Terminals collection
                        SELECT @NationalIsoCode AS CMD_ISO_CODE                                                                                     
                              , 0 AS ORIGIN
                             , CASE WHEN ISNULL(MCD_FACE_VALUE,0) < 0 THEN MCD_NUM_COLLECTED    
                                         ELSE (ISNULL(MCD_FACE_VALUE,0) * ISNULL(MCD_NUM_COLLECTED,0)) END AS CMD_DEPOSITS     
                              , 0 AS CMD_WITHDRAWALS                                                                                     
                          FROM MONEY_COLLECTION_DETAILS                                                                                    
                                INNER JOIN MONEY_COLLECTIONS ON MONEY_COLLECTIONS.MC_COLLECTION_ID = MONEY_COLLECTION_DETAILS.MCD_COLLECTION_ID  
                                INNER JOIN CAGE_MOVEMENTS ON MC_COLLECTION_ID = CAGE_MOVEMENTS.CGM_MC_COLLECTION_ID                              
                         WHERE CAGE_MOVEMENTS.CGM_CAGE_SESSION_ID IN (SELECT CAGE_SESSION_ID FROM #TMP_CAGE_SESSIONS)
                           AND CAGE_MOVEMENTS.CGM_TYPE = @CageOperationType_FromTerminal
                           AND ISNULL(MCD_NUM_COLLECTED,0) > 0
                           
                        ) AS _TBL_GLB

         GROUP BY _TBL_GLB.CMD_ISO_CODE, _TBL_GLB.ORIGIN
         ORDER BY _TBL_GLB.CMD_ISO_CODE


            DROP TABLE #TMP_CAGE_SESSIONS 

      END -- IF @pCageSessionIds IS NOT NULL THEN
      
      ELSE
      BEGIN
      
            -- TABLE[1]: Get cage sessions information
            SELECT CM.CM_SOURCE_TARGET_ID
                  , CM.CM_CONCEPT_ID
                  , CM.CM_ISO_CODE
                  , CM.CM_VALUE_IN
                  , CM.CM_VALUE_OUT
                  , CST.CST_SOURCE_TARGET_NAME
                  , CC.CC_DESCRIPTION
              FROM CAGE_METERS AS CM
                     INNER JOIN CAGE_SOURCE_TARGET_CONCEPTS AS CSTC ON CSTC.CSTC_CONCEPT_ID = CM.CM_CONCEPT_ID AND CSTC.CSTC_SOURCE_TARGET_ID = CM.CM_SOURCE_TARGET_ID
                     INNER JOIN CAGE_CONCEPTS AS CC ON CC.CC_CONCEPT_ID = CSTC.CSTC_CONCEPT_ID
                     INNER JOIN CAGE_SOURCE_TARGET AS CST ON CST.CST_SOURCE_TARGET_ID = CSTC.CSTC_SOURCE_TARGET_ID
            WHERE CC.CC_TYPE = 1
               AND CC.CC_ENABLED = 1
             AND CSTC.CSTC_ENABLED = 1
        ORDER BY CM.CM_SOURCE_TARGET_ID 

            
            -- TABLE[2]: Get dynamic liabilities
            SELECT CC.CC_CONCEPT_ID AS LIABILITY_CONCEPT_ID
                  , CC.CC_DESCRIPTION AS LIABILITY_NAME 
                   , CM.CM_ISO_CODE AS LIABILITY_ISO_CODE
                  , SUM(CM.CM_VALUE) AS LIABILITY_VALUE
              FROM CAGE_METERS AS CM
                     INNER JOIN CAGE_CONCEPTS AS CC ON CC.CC_CONCEPT_ID = CM.CM_CONCEPT_ID
            WHERE CC.CC_IS_PROVISION = 1
      GROUP BY CC.CC_CONCEPT_ID, CC.CC_DESCRIPTION, CM.CM_ISO_CODE
      ORDER BY CC.CC_CONCEPT_ID ASC 
      
                  -- TABLE[3]: Get session sumary

            SELECT @NationalIsoCode = GP_KEY_VALUE 
              FROM GENERAL_PARAMS 
             WHERE GP_GROUP_KEY = 'RegionalOptions' 
               AND GP_SUBJECT_KEY = 'CurrencyISOCode'

            SELECT CMD_ISO_CODE AS ISO_CODE
               , ISNULL(ORIGIN, 0) AS ORIGIN
               , SUM(CMD_DEPOSITS) AS DEPOSITS
               , SUM(CMD_WITHDRAWALS) AS WITHDRAWALS
                  
              FROM      (SELECT CMD_ISO_CODE
                               , ORIGIN
                               , SUM(CMD_DEPOSITS) CMD_DEPOSITS
                               , SUM(CMD_WITHDRAWALS) CMD_WITHDRAWALS
                           
                           FROM     (SELECT CASE WHEN CMD_QUANTITY >= 0 THEN NULL
                                                     ELSE CMD_QUANTITY END  AS ORIGIN
                                           , CMD_ISO_CODE
                                           , CMD_DENOMINATION
                                           , CASE WHEN CGM_TYPE >= 100 AND CGM_TYPE NOT IN (2, 102, 99, 199) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY < 0 THEN CMD_DENOMINATION
                                                                                                                                                                          ELSE CMD_QUANTITY * CMD_DENOMINATION END), 0)
                                                     ELSE 0 END AS CMD_DEPOSITS
                                           , CASE WHEN CGM_TYPE < 100 AND CGM_TYPE NOT IN (2, 102, 99, 199) THEN ISNULL(SUM (CASE WHEN CMD_QUANTITY < 0 THEN CMD_DENOMINATION             
                                                                                                                                                                           ELSE CMD_QUANTITY * CMD_DENOMINATION END), 0)                              
                                                      ELSE 0 END AS CMD_WITHDRAWALS

                                      FROM CAGE_MOVEMENTS 
                                               INNER JOIN   CAGE_MOVEMENT_DETAILS ON CAGE_MOVEMENTS.CGM_MOVEMENT_ID = CAGE_MOVEMENT_DETAILS.CMD_MOVEMENT_ID          

                                GROUP BY CMD_ISO_CODE, CMD_DENOMINATION, CGM_TYPE, CMD_QUANTITY
                                ) AS T1
                                
                        GROUP BY ORIGIN,CMD_ISO_CODE

                        UNION ALL 

                        --The next one is for the Stackers / Terminals collection
                        SELECT @NationalIsoCode AS CMD_ISO_CODE                                                                                     
                              , 0 AS ORIGIN
                             , CASE WHEN ISNULL(MCD_FACE_VALUE,0) < 0 THEN MCD_NUM_COLLECTED    
                                         ELSE (ISNULL(MCD_FACE_VALUE,0) * ISNULL(MCD_NUM_COLLECTED,0)) END AS CMD_DEPOSITS     
                              , 0 AS CMD_WITHDRAWALS                                                                                     
                          FROM MONEY_COLLECTION_DETAILS                                                                                    
                                INNER JOIN MONEY_COLLECTIONS ON MONEY_COLLECTIONS.MC_COLLECTION_ID = MONEY_COLLECTION_DETAILS.MCD_COLLECTION_ID  
                                INNER JOIN CAGE_MOVEMENTS ON MC_COLLECTION_ID = CAGE_MOVEMENTS.CGM_MC_COLLECTION_ID                              
                         WHERE CAGE_MOVEMENTS.CGM_TYPE = @CageOperationType_FromTerminal
                           AND ISNULL(MCD_NUM_COLLECTED,0) > 0
                           
                        ) AS _TBL_GLB

         GROUP BY _TBL_GLB.CMD_ISO_CODE, _TBL_GLB.ORIGIN
         ORDER BY _TBL_GLB.CMD_ISO_CODE

      
      END --ELSE
                   
END
GO


CREATE PROCEDURE [dbo].[CageUpdateMeter]
            @pValueIn MONEY
      , @pValueOut MONEY
        ,   @pSourceTagetId BIGINT
        , @pConceptId BIGINT
        , @pIsoCode VARCHAR(3)
        , @pOperation INT
        , @pSessionId BIGINT = NULL
        , @pSessionGetMode INT
      
  AS
BEGIN 

      -- @pOperationId  =     0 - Set
      --                        =   1 - Update
      
      -- , @pCageSessionGetMode = 0 - Get cage session from parameters
      --                                   = 1 - Get cage_sessions


      DECLARE @CageSessionId BIGINT
      DECLARE @GetFromMode INT
      DECLARE @out_CageMetersValue TABLE(CM_VALUE DECIMAL)
      DECLARE @out_CageSessionMetersValue TABLE (CSM_VALUE DECIMAL)

      -- Get Cage Session
      
      IF @pSessionGetMode = 0
            SET @CageSessionId = @pSessionId

      ELSE IF @pSessionGetMode = 1
      BEGIN
      
            -------------- TODO : GET FROM GP!! -------------- 
            --SELECT @GetFromMode = GP_KEY_VALUE FROM GENERAL_PARAMS 
            -- WHERE GP_GROUP_KEY = 'RegionalOptions' 
            --   AND GP_SUBJECT_KEY = 'CurrencyISOCode'
            SET @GetFromMode = 1
     
            IF @GetFromMode = 1
                  SET @CageSessionId = (SELECT TOP 1 cgs_cage_session_id
                                                     FROM CAGE_SESSIONS
                                               ORDER BY cgs_cage_session_id DESC)
            ELSE
                  SET @CageSessionId = (SELECT TOP 1 cgs_cage_session_id
                                                     FROM CAGE_SESSIONS
                                               ORDER BY cgs_cage_session_id ASC)
      END                        

      -- CAGE METERS --
      
      UPDATE CAGE_METERS SET                                                                  
             CM_VALUE_IN = CASE WHEN @pOperation = 1 THEN ISNULL(CM_VALUE_IN, 0) + @pValueIn    
                              ELSE CM_VALUE_IN END
         , CM_VALUE_OUT = CASE WHEN @pOperation = 1 THEN ISNULL(CM_VALUE_OUT, 0) + @pValueOut  
                               ELSE CM_VALUE_OUT END                                                                       
         , CM_VALUE = CASE WHEN @pOperation = 1 THEN ISNULL(CM_VALUE, 0) + @pValueIn - @pValueOut            
                           WHEN @pOperation = 0 THEN @pValueIn - @pValueOut                                
                           ELSE CM_VALUE END 
    OUTPUT  INSERTED.CM_VALUE INTO @out_CageMetersValue
      WHERE CM_SOURCE_TARGET_ID = @pSourceTagetId
        AND CM_CONCEPT_ID = @pConceptId                                                     
         AND CM_ISO_CODE = @pIsoCode                                                         
         
         

      -- CAGE SESSION METERS --

      IF @CageSessionId IS NOT NULL
      BEGIN
           UPDATE CAGE_SESSION_METERS SET                                                          
                   CSM_VALUE_IN = ISNULL(CSM_VALUE_IN, 0) + @pValueIn 
                 , CSM_VALUE_OUT = ISNULL(CSM_VALUE_OUT, 0) + @pValueOut                                  
               , CSM_VALUE = ISNULL(CSM_VALUE, 0) + @pValueIn - @pValueOut                                      
            OUTPUT INSERTED.CSM_VALUE INTO @out_CageSessionMetersValue                                                            
             WHERE CSM_SOURCE_TARGET_ID = @pSourceTagetId
              AND CSM_CONCEPT_ID = @pConceptId                                                                                              
               AND CSM_ISO_CODE = @pIsoCode                                                        
               AND CSM_CAGE_SESSION_ID = @CageSessionId 
      END                                                        
 
    -- Create GlobalMeters. It checks if jot exists and create the meter
      EXEC CageCreateMeters @pSourceTagetId
                          , @pConceptId
                          , NULL  -- @pCageSessionId
                          , 1     -- Create in cage_meters only
                          , NULL  --@CurrencyForced
                                      
      SELECT  ISNULL((SELECT TOP 1 CM_VALUE FROM @out_CageMetersValue),0) as CM_VALUE
              , ISNULL((SELECT TOP 1 CSM_VALUE FROM @out_CageSessionMetersValue),0) as CSM_VALUE
              
 END  -- CageUpdateMeter

GO

CREATE PROCEDURE [dbo].[CageUpdateSystemMeters]
        @pCashierSessionId BIGINT
  AS
BEGIN             

  DECLARE @CageSessionId BIGINT
  DECLARE @NationalCurrency VARCHAR(3)
  
  DECLARE @Tax1 MONEY
  DECLARE @Tax2 MONEY
  
  DECLARE @CardRefundable BIT
  DECLARE @CardDeposit MONEY
  
  SELECT @NationalCurrency = GP_KEY_VALUE FROM GENERAL_PARAMS 
   WHERE GP_GROUP_KEY = 'RegionalOptions' 
     AND GP_SUBJECT_KEY = 'CurrencyISOCode'
  
  
  ---- GET CAGE SESSION ID ----
  
  SET @CageSessionId = (SELECT TOP 1 CGM_CAGE_SESSION_ID 
                                     FROM CAGE_MOVEMENTS
                                   WHERE CGM_CASHIER_SESSION_ID = @pCashierSessionId 
                                      AND CGM_TYPE IN (0, 1, 4, 5)
                               ORDER BY CGM_MOVEMENT_DATETIME DESC)

  IF @CageSessionId IS NULL
      SET @CageSessionId = (SELECT TOP 1 CGM_CAGE_SESSION_ID 
                                FROM CAGE_MOVEMENTS
                               WHERE CGM_CASHIER_SESSION_ID = @pCashierSessionId 
                                 AND CGM_TYPE IN (100, 101, 103, 104)
                          ORDER BY CGM_MOVEMENT_DATETIME DESC)
      
  IF @CageSessionId IS NULL
      SET @CageSessionId = (SELECT TOP 1 CGS_CAGE_SESSION_ID 
                                FROM CAGE_SESSIONS
                               WHERE CGS_CLOSE_DATETIME IS NULL
                          ORDER BY CGS_CAGE_SESSION_ID DESC)
   
   IF @CageSessionId IS NULL
      SET @CageSessionId = (SELECT TOP 1 CGS_CAGE_SESSION_ID 
                                FROM CAGE_SESSIONS
                             ORDER BY CGS_CAGE_SESSION_ID DESC)
   
      IF @CageSessionId IS NOT NULL
      BEGIN
  
            -- FEDERAL TAX --
  
            SELECT @Tax1 = CM_ADD_AMOUNT 
              FROM CASHIER_MOVEMENTS_GROUPED_BY_SESSION_ID  
           WHERE CM_TYPE = 142 
             AND CM_SESSION_ID = @pCashierSessionId

            SET @Tax1 = ISNULL(@Tax1, 0)
            
            EXEC [dbo].[CageUpdateMeter]
                  @pValueIn = @Tax1,
            @pValueOut = 0,
            @pSourceTagetId = 0,
            @pConceptId = 3,
            @pIsoCode = @NationalCurrency,
            @pOperation = 1,
            @pSessionId = @CageSessionId,
            @pSessiongetMode = 0 
    
     
            -- STATE TAX --
   
            SELECT @Tax2 = CM_ADD_AMOUNT 
              FROM CASHIER_MOVEMENTS_GROUPED_BY_SESSION_ID  
             WHERE CM_TYPE = 146 
               AND CM_SESSION_ID = @pCashierSessionId 
          
          SET @Tax2 = ISNULL(@Tax2, 0)
         
            EXEC [dbo].[CageUpdateMeter]
                  @pValueIn = @Tax2,
            @pValueOut = 0,
            @pSourceTagetId = 0,
            @pConceptId = 2,
            @pIsoCode = @NationalCurrency,
            @pOperation = 1,
            @pSessionId = @CageSessionId,
            @pSessiongetMode = 0 
          
          
            ---- CARD_PAYMENT ---- (IF @CardRefundable IS FALSE: WE DON'T NEED TO RETURN VALUE FOR LIABILITIES)                                                                    


            SELECT @CardRefundable = GP_KEY_VALUE 
              FROM GENERAL_PARAMS  
             WHERE GP_GROUP_KEY = 'Cashier'                             
               AND GP_SUBJECT_KEY = 'CardRefundable'                    

            IF ISNULL(@CardRefundable, 0) = 1                                                                        
            BEGIN                                                                                                   
                  SELECT @CardDeposit = SUM(CASE WHEN CM_TYPE = 9 THEN CM_ADD_AMOUNT   
                                                              ELSE -1 * CM_ADD_AMOUNT END)                                                  
                FROM CASHIER_MOVEMENTS_GROUPED_BY_SESSION_ID                                                    
               WHERE CM_TYPE IN (9, 10)                                             
                 AND CM_SESSION_ID IN(SELECT DISTINCT(CGM_CASHIER_SESSION_ID)                                   
                                                       FROM CAGE_MOVEMENTS                                                                     
                                                    WHERE CGM_CAGE_SESSION_ID = @pCashierSessionId)
                                                    
                  SET @CardDeposit = ISNULL(@CardDeposit, 0)
            
                  EXEC [dbo].[CageUpdateMeter]
                        @pValueIn = @CardDeposit,
                        @pValueOut = 0,
                        @pSourceTagetId = 0,
                        @pConceptId = 5,
                        @pIsoCode = @NationalCurrency,
                        @pOperation = 1,
                        @pSessionId = @CageSessionId,
                        @pSessiongetMode = 0
                     
            END                                                                                                     

      END                                               

END --CageSessionsInfoMovements

GO


/* *************** GRANTS ***************************************/

GRANT EXECUTE ON [dbo].[CageGetGlobalReportData] TO [wggui] WITH GRANT OPTION
GO

GRANT EXECUTE ON [dbo].[CageUpdateMeter] TO [wggui] WITH GRANT OPTION 
GO

GRANT EXECUTE ON [dbo].[CageUpdateSystemMeters] TO [wggui] WITH GRANT OPTION 
GO

GRANT EXECUTE ON [dbo].[CageCreateMeters] TO [wggui] WITH GRANT OPTION 
GO

/*
ALTA NUEVOS ORIGENES Y DESTINOS / CONCEPTOS / CONTADORES DE B�VEDA 
      SCRIPTS INSERTS: CREACI�N DESTINO/ORIGENES, CONCEPTOS Y CONTADORES DE SISTEMA Y PREDEFINIDOS
*/

-- 1. CONCEPTS

      -- INSERT GLOBAL CONCEPT ([0])
      IF NOT EXISTS (SELECT TOP 1 CC_CONCEPT_ID FROM CAGE_CONCEPTS WHERE CC_CONCEPT_ID = 0)
      INSERT INTO CAGE_CONCEPTS
                  ( CC_CONCEPT_ID, CC_DESCRIPTION
                  , CC_IS_PROVISION, CC_SHOW_IN_REPORT
                  , CC_TYPE, CC_ENABLED, CC_NAME )
            VALUES
                  ( 0, 'Global'
                  , 0, 1
                  , 1, 1, 'Global')
                  
                           

      -- INSERT SYSTEM CONCEPTS ([1] to [5])
      
      -- Circulante Fichas ([1])
      IF NOT EXISTS (SELECT TOP 1 CC_CONCEPT_ID FROM CAGE_CONCEPTS WHERE CC_CONCEPT_ID = 1)
      INSERT INTO CAGE_CONCEPTS
                  ( CC_CONCEPT_ID, CC_DESCRIPTION
                  , CC_IS_PROVISION, CC_SHOW_IN_REPORT
                  , CC_TYPE, CC_ENABLED, CC_NAME )
            VALUES
                  ( 1, 'Circulante Fichas'
                  , 1, 1
                  , 0, 1, 'Circulante Fichas')
                  
      -- Impuesto Estatal ([2])
      IF NOT EXISTS (SELECT TOP 1 CC_CONCEPT_ID FROM CAGE_CONCEPTS WHERE CC_CONCEPT_ID = 2)
      INSERT INTO CAGE_CONCEPTS
                  ( CC_CONCEPT_ID, CC_DESCRIPTION
                  , CC_IS_PROVISION, CC_SHOW_IN_REPORT
                  , CC_TYPE, CC_ENABLED, CC_NAME )
            VALUES
                  ( 2, 'Impuesto Estatal'
                  , 1, 1
                  , 0, 1, 'I. Estatal')
                  
      -- Impuesto Federal ([3])
      IF NOT EXISTS (SELECT TOP 1 CC_CONCEPT_ID FROM CAGE_CONCEPTS WHERE CC_CONCEPT_ID = 3)
      INSERT INTO CAGE_CONCEPTS
                  ( CC_CONCEPT_ID, CC_DESCRIPTION
                  , CC_IS_PROVISION, CC_SHOW_IN_REPORT
                  , CC_TYPE, CC_ENABLED, CC_NAME )
            VALUES
                  ( 3, 
                    'Impuesto Federal',
                    1,
                    1,
                    0,
                    1,
                    'I. Federal')
                  
      -- Provisi�n Progresivos ([4])
      IF NOT EXISTS (SELECT TOP 1 CC_CONCEPT_ID FROM CAGE_CONCEPTS WHERE CC_CONCEPT_ID = 4)
      INSERT INTO CAGE_CONCEPTS
                  ( CC_CONCEPT_ID, CC_DESCRIPTION
                  , CC_IS_PROVISION, CC_SHOW_IN_REPORT
                  , CC_TYPE, CC_ENABLED, CC_NAME )
            VALUES
                  ( 4, 'Provisi�n Progresivos'
                  , 1, 1
                  , 0, 1, 'Prov. Progresivos')
                  
      -- Dep�sito de Tarjetas ([5])
      IF NOT EXISTS (SELECT TOP 1 CC_CONCEPT_ID FROM CAGE_CONCEPTS WHERE CC_CONCEPT_ID = 5)
      INSERT INTO CAGE_CONCEPTS
                  ( CC_CONCEPT_ID, CC_DESCRIPTION
                  , CC_IS_PROVISION, CC_SHOW_IN_REPORT
                  , CC_TYPE, CC_ENABLED, CC_NAME )
            VALUES
                  ( 5, 'Dep�sito de Tarjetas'
                  , 1, 1
                  , 0, 1, 'Dep. de Tarjetas')



-- 2. SOURCE-TARGETS

      -- INSERT SYSTEM ST ([0])
      IF NOT EXISTS (SELECT TOP 1 CST_SOURCE_TARGET_ID FROM CAGE_SOURCE_TARGET WHERE CST_SOURCE_TARGET_ID = 0)
      INSERT INTO CAGE_SOURCE_TARGET
                    ( CST_SOURCE_TARGET_ID, CST_SOURCE_TARGET_NAME
              , CST_SOURCE, CST_TARGET )
     VALUES
                    ( 0, 'System'
                    , 1, 1 )
      
      
      -- INSERT PREDEFINED ST ([10] to [12]) 
      
      -- Cajas ([10])
      IF NOT EXISTS (SELECT TOP 1 CST_SOURCE_TARGET_ID FROM CAGE_SOURCE_TARGET WHERE CST_SOURCE_TARGET_ID = 10)
      INSERT INTO CAGE_SOURCE_TARGET
                    ( CST_SOURCE_TARGET_ID, CST_SOURCE_TARGET_NAME
              , CST_SOURCE, CST_TARGET )
     VALUES
                    ( 10, 'Cajas'
                    , 1, 1 )
                    
      -- Mesas de Juego ([11])
      IF NOT EXISTS (SELECT TOP 1 CST_SOURCE_TARGET_ID FROM CAGE_SOURCE_TARGET WHERE CST_SOURCE_TARGET_ID = 11)
      INSERT INTO CAGE_SOURCE_TARGET
                    ( CST_SOURCE_TARGET_ID, CST_SOURCE_TARGET_NAME
              , CST_SOURCE, CST_TARGET )
     VALUES
                    ( 11, 'Mesas de Juego'
                    , 1, 1 )
                    
      -- Terminales ([12])
      IF NOT EXISTS (SELECT TOP 1 CST_SOURCE_TARGET_ID FROM CAGE_SOURCE_TARGET WHERE CST_SOURCE_TARGET_ID = 12)
      INSERT INTO CAGE_SOURCE_TARGET
                    ( CST_SOURCE_TARGET_ID, CST_SOURCE_TARGET_NAME
              , CST_SOURCE, CST_TARGET )
     VALUES
                    ( 12, 'Terminales'
                    , 1, 1 )
                    
                    
                    
-- 3. SOURCE/TARGETS-CONCEPTS RELATIONS & CAGE METERS

      -- GLOBAL-CAJAS ([0]-[10])
      -- Create Relation
      IF NOT EXISTS (SELECT TOP 1 * FROM CAGE_SOURCE_TARGET_CONCEPTS WHERE CSTC_CONCEPT_ID = 0 
                                                                       AND CSTC_SOURCE_TARGET_ID = 10)
      INSERT INTO CAGE_SOURCE_TARGET_CONCEPTS
           ( CSTC_CONCEPT_ID, CSTC_SOURCE_TARGET_ID
           , CSTC_TYPE, CSTC_ONLY_NATIONAL_CURRENCY
           , CSTC_ENABLED )
     VALUES
           ( 0, 10
           , 2, 0
           , 1 )
    -- Create Meters
    EXEC CageCreateMeters
                  @pSourceTargetId = 10
              , @pConceptId = 0
              , @pCageSessionId = NULL
              , @CreateMetersOption = 1
              , @CurrencyForced = NULL
              
           
      -- GLOBAL-MESAS ([0]-[11])
      IF NOT EXISTS (SELECT TOP 1 * FROM CAGE_SOURCE_TARGET_CONCEPTS WHERE CSTC_CONCEPT_ID = 0 
                                                                       AND CSTC_SOURCE_TARGET_ID = 11)
      INSERT INTO CAGE_SOURCE_TARGET_CONCEPTS
           ( CSTC_CONCEPT_ID, CSTC_SOURCE_TARGET_ID
           , CSTC_TYPE, CSTC_ONLY_NATIONAL_CURRENCY
           , CSTC_ENABLED )
     VALUES
           ( 0, 11
           , 2, 0
           , 1 )
    -- Create Meters
    EXEC CageCreateMeters
                  @pSourceTargetId = 11
              , @pConceptId = 0
              , @pCageSessionId = NULL
              , @CreateMetersOption = 1
              , @CurrencyForced = NULL
              
           
    -- GLOBAL-TERMINALES ([0]-[12])
      IF NOT EXISTS (SELECT TOP 1 * FROM CAGE_SOURCE_TARGET_CONCEPTS WHERE CSTC_CONCEPT_ID = 0 
                                                                       AND CSTC_SOURCE_TARGET_ID = 12)
      INSERT INTO CAGE_SOURCE_TARGET_CONCEPTS
           ( CSTC_CONCEPT_ID, CSTC_SOURCE_TARGET_ID
           , CSTC_TYPE, CSTC_ONLY_NATIONAL_CURRENCY
           , CSTC_ENABLED )
     VALUES
           ( 0, 12
           , 0, 0
           , 1 )
    -- Create Meters
    EXEC CageCreateMeters
                  @pSourceTargetId = 12
              , @pConceptId = 0
              , @pCageSessionId = NULL
              , @CreateMetersOption = 1
              , @CurrencyForced = NULL
           
           
    -- CIRCULANTE FICHAS-SYSTEM ([1]-[0])
      IF NOT EXISTS (SELECT TOP 1 * FROM CAGE_SOURCE_TARGET_CONCEPTS WHERE CSTC_CONCEPT_ID = 1 
                                                                       AND CSTC_SOURCE_TARGET_ID = 0)
      INSERT INTO CAGE_SOURCE_TARGET_CONCEPTS
           ( CSTC_CONCEPT_ID, CSTC_SOURCE_TARGET_ID
           , CSTC_TYPE, CSTC_ONLY_NATIONAL_CURRENCY
           , CSTC_ENABLED )
     VALUES
           ( 1, 0
           , 0, 1
           , 1 )
    -- Create Meters
    EXEC CageCreateMeters
                  @pSourceTargetId = 0
              , @pConceptId = 1
              , @pCageSessionId = NULL
              , @CreateMetersOption = 1
              , @CurrencyForced = NULL
           
           
    -- IMPUESTO ESTATAL-SYSTEM ([2]-[0])
      IF NOT EXISTS (SELECT TOP 1 * FROM CAGE_SOURCE_TARGET_CONCEPTS WHERE CSTC_CONCEPT_ID = 2 
                                                                       AND CSTC_SOURCE_TARGET_ID = 0)
      INSERT INTO CAGE_SOURCE_TARGET_CONCEPTS
           ( CSTC_CONCEPT_ID, CSTC_SOURCE_TARGET_ID
           , CSTC_TYPE, CSTC_ONLY_NATIONAL_CURRENCY
           , CSTC_ENABLED )
     VALUES
           ( 2, 0
           , 0, 1
           , 1 )
    -- Create Meters
    EXEC CageCreateMeters
                  @pSourceTargetId = 0
              , @pConceptId = 2
              , @pCageSessionId = NULL
              , @CreateMetersOption = 1
              , @CurrencyForced = NULL
           
           
    -- IMPUESTO FEDERAL-SYSTEM ([3]-[0])
      IF NOT EXISTS (SELECT TOP 1 * FROM CAGE_SOURCE_TARGET_CONCEPTS WHERE CSTC_CONCEPT_ID = 3 
                                                                       AND CSTC_SOURCE_TARGET_ID = 0)
      INSERT INTO CAGE_SOURCE_TARGET_CONCEPTS
           ( CSTC_CONCEPT_ID, CSTC_SOURCE_TARGET_ID
           , CSTC_TYPE, CSTC_ONLY_NATIONAL_CURRENCY
           , CSTC_ENABLED )
     VALUES
           ( 3, 0
           , 0, 1
           , 1 )
    -- Create Meters
    EXEC CageCreateMeters
                  @pSourceTargetId = 0
              , @pConceptId = 3
              , @pCageSessionId = NULL
              , @CreateMetersOption = 1
              , @CurrencyForced = NULL
              
           
    -- PROVISI�N PROGRESIVOS-SYSTEM ([4]-[0])
      IF NOT EXISTS (SELECT TOP 1 * FROM CAGE_SOURCE_TARGET_CONCEPTS WHERE CSTC_CONCEPT_ID = 4
                                                                       AND CSTC_SOURCE_TARGET_ID = 0)
      INSERT INTO CAGE_SOURCE_TARGET_CONCEPTS
           ( CSTC_CONCEPT_ID, CSTC_SOURCE_TARGET_ID
           , CSTC_TYPE, CSTC_ONLY_NATIONAL_CURRENCY
           , CSTC_ENABLED )
     VALUES
           ( 4, 0
           , 0, 1
           , 1 )
    -- Create Meters
    EXEC CageCreateMeters
                  @pSourceTargetId = 0
              , @pConceptId = 4
              , @pCageSessionId = NULL
              , @CreateMetersOption = 1
              , @CurrencyForced = NULL
              
           
    -- DEP�SITO DE TARJETAS-SYSTEM ([5]-[0])
      IF NOT EXISTS (SELECT TOP 1 * FROM CAGE_SOURCE_TARGET_CONCEPTS WHERE CSTC_CONCEPT_ID = 5 
                                                                       AND CSTC_SOURCE_TARGET_ID = 0)
      INSERT INTO CAGE_SOURCE_TARGET_CONCEPTS
           ( CSTC_CONCEPT_ID, CSTC_SOURCE_TARGET_ID
           , CSTC_TYPE, CSTC_ONLY_NATIONAL_CURRENCY
           , CSTC_ENABLED )
     VALUES
           ( 5, 0
           , 0, 1
           , 1 )
    -- Create Meters
    EXEC CageCreateMeters
                  @pSourceTargetId = 0
              , @pConceptId = 5
              , @pCageSessionId = NULL
              , @CreateMetersOption = 1
              , @CurrencyForced = NULL
      
GO
