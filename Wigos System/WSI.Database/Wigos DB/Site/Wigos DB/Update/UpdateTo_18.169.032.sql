/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 168;

SET @New_ReleaseId = 169;
SET @New_ScriptName = N'UpdateTo_18.169.032.sql';
SET @New_Description = N'GamingTables - New GP SasHost-SASFlags';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[gaming_tables_sessions]') AND type in (N'U'))
DROP TABLE [dbo].[gaming_tables_sessions]
GO
CREATE TABLE [dbo].[gaming_tables_sessions](
      [gts_gaming_table_session_id] [bigint] IDENTITY(1,1) NOT NULL,
      [gts_gaming_table_id] [bigint] NOT NULL,
      [gts_cashier_session_id] [bigint] NOT NULL,
      [gts_initial_chips_amount] [money] NULL,
      [gts_final_chips_amount] [money] NULL,
      [gts_own_sales_amount] [money] NULL,
      [gts_external_sales_amount] [money] NULL,
      [gts_purchase_amount] [money] NULL,
      [gts_collected_amount] [money] NULL,
      [gts_tips] [money] NULL,
      [gts_client_visits] [int] NULL,
CONSTRAINT [PK_gaming_tables_sessions] PRIMARY KEY CLUSTERED 
(
      [gts_gaming_table_session_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]


/******* RECORDS *******/
IF NOT EXISTS ( SELECT 1 FROM general_params WHERE gp_group_key = 'SasHost' AND gp_subject_key = 'SASFlags')
      INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('SasHost', 'SASFlags', CONVERT(INT, 0xFFFFFFFF))
GO
