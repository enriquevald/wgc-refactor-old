/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 90;

SET @New_ReleaseId = 91;
SET @New_ScriptName = N'UpdateTo_18.091.022.sql';
SET @New_Description = N'.';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** INDEXES ******/
BEGIN TRANSACTION
GO

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[account_promotions]') AND name = N'IX_acp_activation')
DROP INDEX [IX_acp_activation] ON [dbo].[account_promotions] WITH ( ONLINE = OFF )

CREATE NONCLUSTERED INDEX [IX_acp_activation] ON [dbo].[account_promotions] 
(
    [acp_activation] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]


IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[accounts]') AND name = N'IX_ac_current_play_session_id')
DROP INDEX [IX_ac_current_play_session_id] ON [dbo].[accounts] WITH ( ONLINE = OFF )

CREATE NONCLUSTERED INDEX [IX_ac_current_play_session_id] ON [dbo].[accounts] 
(
    [ac_current_play_session_id] ASC,
    [ac_last_activity] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[accounts]') AND name = N'IX_ac_last_activity')
DROP INDEX [IX_ac_last_activity] ON [dbo].[accounts] WITH ( ONLINE = OFF )

CREATE NONCLUSTERED INDEX [IX_ac_last_activity] ON [dbo].[accounts] 
(
    [ac_last_activity] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]


INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('WigosKiosk', 'Msg.1000', '');
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('WigosKiosk', 'Msg.1001', '');

INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('WigosKiosk', 'Schedule.From', '0000');
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('WigosKiosk', 'Schedule.To', '0000');

INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('WigosKiosk', 'Voucher.Recharges.Header', '');
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('WigosKiosk', 'Voucher.Recharges.Footer', '');

INSERT INTO WKT_FUNCTIONALITIES(FUN_FUNCTION_ID,FUN_NAME,FUN_ENABLED) VALUES (5,'SCHEDULED GIFT REQUEST',0);
INSERT INTO WKT_FUNCTIONALITIES(FUN_FUNCTION_ID,FUN_NAME,FUN_ENABLED) VALUES (6,'SCHEDULED RECHARGES',0);
INSERT INTO WKT_FUNCTIONALITIES(FUN_FUNCTION_ID,FUN_NAME,FUN_ENABLED) VALUES (7,'PRINT RECHARGES TICKET',0);



GO
COMMIT
