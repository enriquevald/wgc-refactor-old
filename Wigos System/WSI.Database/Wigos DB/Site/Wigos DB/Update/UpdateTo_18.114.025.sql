/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 113;

SET @New_ReleaseId = 114;
SET @New_ScriptName = N'UpdateTo_18.114.025.sql';
SET @New_Description = N'Add columns ac_holder_twitter_account/sph_theoretical_won_amount into accounts/sales_per_hour tables. Update Multisite procedures and triggers. Enable/Disable MultiSite Triggers.';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO


/****** TABLES ******/

ALTER TABLE dbo.accounts ADD
      ac_holder_twitter_account nvarchar(50) NULL
GO

ALTER TABLE dbo.sales_per_hour ADD
      sph_theoretical_won_amount money NOT NULL CONSTRAINT DF_sales_per_hour_sph_theoretical_won_amount DEFAULT 0
GO

/****** UPDATE SALES_PER_HOUR..SPH_THEORETICAL_WON_AMOUNT ******/

DECLARE @default_payout AS MONEY

SELECT @default_payout = CAST(GP_KEY_VALUE AS MONEY) FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PlayerTracking' AND GP_SUBJECT_KEY = 'TerminalDefaultPayout' AND ISNUMERIC(GP_KEY_VALUE) = 1

SET @default_payout = ISNULL(@default_payout, 95) / 100

UPDATE   SALES_PER_HOUR
   SET   SPH_THEORETICAL_WON_AMOUNT = SPH_PLAYED_AMOUNT * ISNULL(TE_THEORETICAL_PAYOUT, @default_payout)
  FROM   SALES_PER_HOUR
INNER   JOIN TERMINALS ON TE_TERMINAL_ID = SPH_TERMINAL_ID

GO


/****** PROCEDURES ******/

--------------------------------------------------------------------------------
-- Copyright � 2013 Win Systems International
--------------------------------------------------------------------------------
--
--   MODULE NAME: Update_PointsInAccount.sql
--
--   DESCRIPTION: Update points of the account in site 
--
--        AUTHOR: Dani Dom�nguez
--
-- CREATION DATE: 07-MAR-2013
--
-- REVISION HISTORY:
--
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 07-MAR-2013 DDM    First release.
--------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[Update_PointsInAccount]
	@pMovementId                BIGINT
,	@pAccountId                 BIGINT
, @pErrorCode                 INT         
, @pPointsSequenceId          BIGINT      
, @pReceivedPoints            MONEY          

AS
BEGIN   
  DECLARE @LocalDelta AS MONEY

  IF NOT EXISTS (SELECT 1 FROM ACCOUNTS WHERE ac_account_id = @pAccountId )   
    INSERT INTO   ACCOUNTS 
                ( AC_ACCOUNT_ID, AC_TYPE, AC_BLOCKED, AC_TRACK_DATA) 
         VALUES ( @pAccountId,         2,          1, '-RECYCLED-NEW-' + CAST (NEWID() AS NVARCHAR(50)) )
         
  IF ((@pMovementId <> 0)AND(@pErrorCode = 0 OR @pErrorCode = 2))
    DELETE   MS_SITE_PENDING_ACCOUNT_MOVEMENTS
     WHERE   SPM_MOVEMENT_ID = @pMovementId
  
  IF (@pErrorCode <> 0) RETURN
  
  DECLARE @PrevSequence as BIGINT
  
  
  -- Lock the account and later add the local points
  UPDATE ACCOUNTS SET AC_POINTS = AC_POINTS + 0 WHERE AC_ACCOUNT_ID = @pAccountId

  SET @PrevSequence = (SELECT ISNULL(AC_MS_POINTS_SEQ_ID, 0) FROM ACCOUNTS WHERE AC_ACCOUNT_ID = @pAccountId)
  
  IF ( @PrevSequence >= @pPointsSequenceId ) RETURN
  
-- Local Points
  SELECT   @LocalDelta = SUM (am_add_amount - am_sub_amount) 
    FROM   ACCOUNT_MOVEMENTS, MS_SITE_PENDING_ACCOUNT_MOVEMENTS
   WHERE   AM_MOVEMENT_ID = SPM_MOVEMENT_ID
     AND   AM_ACCOUNT_ID  = @pAccountId
     AND   AM_TYPE IN (36,37,38,40,41,46,50,60,61,66) -- Not included level movements,(39)PointsGiftDelivery, 
                                                      -- (42)PointsGiftServices and (67)PointsStatusChanged
  

   	UPDATE   ACCOUNTS
	     SET   AC_POINTS                  = @pReceivedPoints + ISNULL (@LocalDelta, 0)
	         , AC_MS_POINTS_SEQ_ID        = @pPointsSequenceId
	         , AC_MS_POINTS_SYNCHRONIZED  = GETDATE ()
	   WHERE   AC_ACCOUNT_ID              = @pAccountId
	     AND   ISNULL(AC_MS_POINTS_SEQ_ID, 0) < @pPointsSequenceId
END
GO


--------------------------------------------------------------------------------
-- Copyright � 2013 Win Systems International
--------------------------------------------------------------------------------
--
--   MODULE NAME: Update_PersonalInfo.sql.sql
--
--   DESCRIPTION: Update personal Information 
--
--        AUTHOR: Dani Dom�nguez
--
-- CREATION DATE: 08-MAR-2013
--
-- REVISION HISTORY:
--
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 08-MAR-2013 DDM    First release.
--------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[Update_PersonalInfo]
  @pAccountId bigint
, @pTrackData nvarchar(50)
,	@pHolderName nvarchar(200)
,	@pHolderId nvarchar(20)
, @pHolderIdType int
, @pHolderAddress01 nvarchar(50)
, @pHolderAddress02 nvarchar(50)
, @pHolderAddress03 nvarchar(50)
, @pHolderCity nvarchar(50)
, @pHolderZip  nvarchar(10) 
, @pHolderEmail01 nvarchar(50)
,	@pHolderEmail02 nvarchar(50)
,	@pHolderTwitter nvarchar(50)
,	@pHolderPhoneNumber01 nvarchar(20)
, @pHolderPhoneNumber02 nvarchar(20)
, @pHolderComments nvarchar(100)
, @pHolderId1 nvarchar(20)
, @pHolderId2 nvarchar(20)
, @pHolderDocumentId1 bigint
, @pHolderDocumentId2 bigint
, @pHolderName1 nvarchar(50)
,	@pHolderName2 nvarchar(50)
,	@pHolderName3 nvarchar(50)
, @pHolderGender  int
, @pHolderMaritalStatus int
, @pHolderBirthDate datetime
, @pHolderWeddingDate datetime
, @pHolderLevel int
, @pHolderLevelNotify int
, @pHolderLevelEntered datetime
,	@pHolderLevelExpiration datetime
,	@pPin nvarchar(12)
, @pPinFailures int
, @pPinLastModified datetime
, @pBlocked bit
, @pActivated bit
, @pBlockReason int
, @pHolderIsVip int
, @pHolderTitle nvarchar(15)                       
, @pHolderName4 nvarchar(50)                       
, @pHolderPhoneType01  int
, @pHolderPhoneType02  int
, @pHolderState    nvarchar                    
, @pHolderCountry  nvarchar                     
, @pPersonalInfoSequenceId  bigint                     
, @pUserType int
AS
BEGIN

DECLARE @pOtherAccountId as BIGINT

SET @pOtherAccountId = ISNULL ((SELECT AC_ACCOUNT_ID FROM ACCOUNTS WHERE AC_TRACK_DATA = @pTrackData), 0)

IF @pOtherAccountId <> 0 AND @pOtherAccountId <> @pAccountId
  UPDATE   ACCOUNTS   
     SET   AC_TRACK_DATA =  '-RECYCLED-NEW-' + CAST (NEWID() AS NVARCHAR(50))
   WHERE   AC_ACCOUNT_ID = @pOtherAccountId


IF NOT EXISTS (SELECT 1 FROM ACCOUNTS WHERE ac_account_id = @pAccountId )  
BEGIN
  INSERT INTO   ACCOUNTS 
              ( AC_ACCOUNT_ID, AC_TYPE, AC_BLOCKED, AC_TRACK_DATA) 
       VALUES ( @pAccountId,         2,  @pBlocked, @pTrackData )
END  
  
 
  UPDATE   ACCOUNTS                   
     SET   AC_TRACK_DATA              = @pTrackData 
         , AC_HOLDER_NAME             = @pHolderName 
         , AC_HOLDER_ID               = @pHolderId 
         , AC_HOLDER_ID_TYPE          = @pHolderIdType 
         , AC_HOLDER_ADDRESS_01       = @pHolderAddress01 
         , AC_HOLDER_ADDRESS_02       = @pHolderAddress02 
         , AC_HOLDER_ADDRESS_03       = @pHolderAddress03 
         , AC_HOLDER_CITY             = @pHolderCity 
         , AC_HOLDER_ZIP              = @pHolderZip  
         , AC_HOLDER_EMAIL_01         = @pHolderEmail01 
         , AC_HOLDER_EMAIL_02         = @pHolderEmail02 
         , AC_HOLDER_TWITTER_ACCOUNT  = @pHolderTwitter 
         , AC_HOLDER_PHONE_NUMBER_01  = @pHolderPhoneNumber01 
         , AC_HOLDER_PHONE_NUMBER_02  = @pHolderPhoneNumber02 
         , AC_HOLDER_COMMENTS         = @pHolderComments 
         , AC_HOLDER_ID1              = @pHolderId1 
         , AC_HOLDER_ID2              = @pHolderId2 
         , AC_HOLDER_DOCUMENT_ID1     = @pHolderDocumentId1 
         , AC_HOLDER_DOCUMENT_ID2     = @pHolderDocumentId2 
         , AC_HOLDER_NAME1            = @pHolderName1 
         , AC_HOLDER_NAME2            = @pHolderName2 
         , AC_HOLDER_NAME3            = @pHolderName3 
         , AC_HOLDER_GENDER           = @pHolderGender  
         , AC_HOLDER_MARITAL_STATUS   = @pHolderMaritalStatus 
         , AC_HOLDER_BIRTH_DATE       = @pHolderBirthDate 
         , AC_HOLDER_WEDDING_DATE     = @pHolderWeddingDate 
         , AC_HOLDER_LEVEL            = @pHolderLevel 
         , AC_HOLDER_LEVEL_NOTIFY     = @pHolderLevelNotify 
         , AC_HOLDER_LEVEL_ENTERED    = @pHolderLevelEntered 
         , AC_HOLDER_LEVEL_EXPIRATION = @pHolderLevelExpiration 
         , AC_PIN                     = @pPin 
         , AC_PIN_FAILURES            = @pPinFailures 
         , AC_PIN_LAST_MODIFIED       = @pPinLastModified 
         , AC_BLOCKED                 = @pBlocked 
         , AC_ACTIVATED               = @pActivated 
         , AC_BLOCK_REASON            = @pBlockReason 
         , AC_HOLDER_IS_VIP           = @pHolderIsVip 
         , AC_HOLDER_TITLE            = @pHolderTitle 
         , AC_HOLDER_NAME4            = @pHolderName4         
         , AC_HOLDER_PHONE_TYPE_01    = @pHolderPhoneType01  
         , AC_HOLDER_PHONE_TYPE_02    = @pHolderPhoneType02  
         , AC_HOLDER_STATE            = @pHolderState    
         , AC_HOLDER_COUNTRY          = @pHolderCountry  					
         , AC_MS_PERSONAL_INFO_SEQ_ID = @pPersonalInfoSequenceId           
         , AC_USER_TYPE				        = @pUserType
         , AC_MS_CHANGE_GUID          = AC_MS_CHANGE_GUID -- avoid trigger on update
   WHERE   AC_ACCOUNT_ID              = @pAccountId 

END
GO


--------------------------------------------------------------------------------
-- Copyright � 2013 Win Systems International
--------------------------------------------------------------------------------
--
--   MODULE NAME: MultiSiteTriggersEnable
--
--   DESCRIPTION: Enable and disable the triggers of Multisite
--
--        AUTHOR: Dani Dom�nguez and Andreu Juli�
--
-- CREATION DATE: 02-APR-2013
--
-- REVISION HISTORY:
--
-- Date        Author   Description
-- ----------- --------- ----------------------------------------------------------
-- 08-MAR-2013 AJQ & DDM    First release.  
-----------------------------------------------------------------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MultiSiteTriggersEnable]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[MultiSiteTriggersEnable]
GO

Create PROCEDURE [dbo].[MultiSiteTriggersEnable] 
@Enable as bit
WITH EXECUTE AS OWNER
AS
BEGIN
	
  SET NOCOUNT ON;
	
  IF @Enable = 1 
  BEGIN
    ENABLE  TRIGGER MultiSiteTrigger_SiteAccountUpdate ON ACCOUNTS;
    ENABLE  TRIGGER Trigger_SiteToMultiSite_Points     ON ACCOUNT_MOVEMENTS;
  END
  ELSE 
  BEGIN
    DISABLE  TRIGGER MultiSiteTrigger_SiteAccountUpdate ON ACCOUNTS;
    DISABLE  TRIGGER Trigger_SiteToMultiSite_Points     ON ACCOUNT_MOVEMENTS;
  END
	
END
GO

GRANT EXECUTE ON [dbo].[MultiSiteTriggersEnable] TO [wggui] WITH GRANT OPTION
GO


/****** TRIGGERS ******/

-- Se elimina el trigger de Insert account
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[MultiSiteTrigger_SiteAccountInsert]') AND type in (N'TR'))
DROP TRIGGER [dbo].[MultiSiteTrigger_SiteAccountInsert]
GO


--------------------------------------------------------------------------------
-- Copyright � 2013 Win Systems International 
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: AccountMovementsSynchronize.sql
-- 
--   DESCRIPTION: Procedures for trigger ACCOUNT_MOVEMENTS_SYNCHRONIZE and related issues
-- 
--        AUTHOR: Jos� Mart�nez
-- 
-- CREATION DATE: 25-FEB-2013
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 25-FEB-2013 JML    First release.
-------------------------------------------------------------------------------
--
--   NOTES :
--
-- Movements related with points:
--     
--  36, 'PointsAwarded'
--  37, 'PointsToGiftRequest'
--  38, 'PointsToNotRedeemable'
--  39, 'PointsGiftDelivery'
--  40, 'PointsExpired'
--  41, 'PointsToDrawTicketPrint'
--  42, 'PointsGiftServices'
--  43, 'HolderLevelChanged'
--  46, 'PointsToRedeemable'
--  50, 'CardAdjustment'
--  60, 'PromotionPoint'
--  61, 'CancelPromotionPoint'
--  62, 'ManualHolderLevelChanged'
--  66, 'CancelGiftInstance' 
--  67, 'PointsStatusChanged' 
--  71, 'MultiSitePointsOnlyForRedeem'
-- 101, 'ImportedPointsOnlyForRedeem'
--
--

ALTER TRIGGER [dbo].[Trigger_SiteToMultiSite_Points]
ON [dbo].[ACCOUNT_MOVEMENTS]
AFTER INSERT
NOT FOR REPLICATION
AS
  BEGIN
  
    IF NOT EXISTS (SELECT   1 
                     FROM   GENERAL_PARAMS 
                    WHERE   GP_GROUP_KEY   = N'Site' 
                      AND   GP_SUBJECT_KEY = N'MultiSiteMember' 
                      AND   GP_KEY_VALUE   = N'1')
    BEGIN
          RETURN
    END
  
    SET NOCOUNT ON

    -- Insert movement to synchronize
    INSERT INTO   MS_SITE_PENDING_ACCOUNT_MOVEMENTS 
                ( SPM_MOVEMENT_ID )
         SELECT   AM_MOVEMENT_ID 
           FROM   INSERTED 
          WHERE   AM_TYPE IN ( 36, 37, 38, 39, 40, 41, 42, 43, 46, 50, 60, 61, 62, 66, 67, 71, 101 ) 
   
    SET NOCOUNT OFF

  END -- [Trigger_SiteToMultiSite_Points]
GO
 

--------------------------------------------------------------------------------
-- Copyright � 2013 Win Systems International 
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: MultiSiteTrigger_SiteAccountUpdate.sql
-- 
--   DESCRIPTION: Procedures for trigger MultiSiteTrigger_SiteAccountUpdate and related issues
-- 
--        AUTHOR: Jos� Mart�nez
-- 
-- CREATION DATE: 07-MAR-2013
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 07-MAR-2013 JML    First release.
--------------------------------------------------------------------------------

ALTER TRIGGER [dbo].[MultiSiteTrigger_SiteAccountUpdate] ON [dbo].[accounts]
   AFTER UPDATE
NOT FOR REPLICATION
AS 
BEGIN
    DECLARE @AccountId          AS BIGINT
    DECLARE @hash0              as varbinary(20)
    DECLARE @hash1              as varbinary(20)
    DECLARE @value              as nvarchar(max)
    DECLARE @changed            as bit
    DECLARE @updated            AS BIT
    IF (UPDATE(AC_MS_CHANGE_GUID)) RETURN

  IF NOT EXISTS (SELECT   1 
                   FROM   GENERAL_PARAMS 
                  WHERE   GP_GROUP_KEY   = N'Site' 
                    AND   GP_SUBJECT_KEY = N'MultiSiteMember' 
                    AND   GP_KEY_VALUE   = N'1')
  BEGIN
        RETURN
  END

  SET @updated = 0;  

IF UPDATE (AC_TRACK_DATA)
    OR UPDATE (AC_HOLDER_NAME)
    OR UPDATE (AC_HOLDER_ID)
    OR UPDATE (AC_HOLDER_ID_TYPE)
    OR UPDATE (AC_HOLDER_ADDRESS_01)
    OR UPDATE (AC_HOLDER_ADDRESS_02)
    OR UPDATE (AC_HOLDER_ADDRESS_03)
    OR UPDATE (AC_HOLDER_CITY)
    OR UPDATE (AC_HOLDER_ZIP)
    OR UPDATE (AC_HOLDER_EMAIL_01)
    OR UPDATE (AC_HOLDER_EMAIL_02)
    OR UPDATE (AC_HOLDER_TWITTER_ACCOUNT)
    OR UPDATE (AC_HOLDER_PHONE_NUMBER_01)
    OR UPDATE (AC_HOLDER_PHONE_NUMBER_02)
    OR UPDATE (AC_HOLDER_COMMENTS)
    OR UPDATE (AC_HOLDER_ID1)
    OR UPDATE (AC_HOLDER_ID2)
    OR UPDATE (AC_HOLDER_DOCUMENT_ID1)
    OR UPDATE (AC_HOLDER_DOCUMENT_ID2)
    OR UPDATE (AC_HOLDER_NAME1)
    OR UPDATE (AC_HOLDER_NAME2)
    OR UPDATE (AC_HOLDER_NAME3)
    OR UPDATE (AC_HOLDER_GENDER)
    OR UPDATE (AC_HOLDER_MARITAL_STATUS)
    OR UPDATE (AC_HOLDER_BIRTH_DATE)
    OR UPDATE (AC_HOLDER_WEDDING_DATE)
    OR UPDATE (AC_HOLDER_LEVEL)
    OR UPDATE (AC_HOLDER_LEVEL_NOTIFY)
    OR UPDATE (AC_HOLDER_LEVEL_ENTERED)
    OR UPDATE (AC_HOLDER_LEVEL_EXPIRATION)
    OR UPDATE (AC_PIN)
    OR UPDATE (AC_PIN_FAILURES)
    OR UPDATE (AC_PIN_LAST_MODIFIED)
    OR UPDATE (AC_BLOCKED)
    OR UPDATE (AC_ACTIVATED)
    OR UPDATE (AC_BLOCK_REASON)
    OR UPDATE (AC_HOLDER_IS_VIP)
    OR UPDATE (AC_HOLDER_TITLE)
    OR UPDATE (AC_HOLDER_NAME4)
    OR UPDATE (AC_HOLDER_PHONE_TYPE_01)
    OR UPDATE (AC_HOLDER_PHONE_TYPE_02)
    OR UPDATE (AC_HOLDER_STATE)
    OR UPDATE (AC_HOLDER_COUNTRY)
    OR UPDATE (AC_USER_TYPE)
        SET @updated = 1;
        
    IF (@updated = 0) RETURN

    DECLARE PersonalInfoCursor CURSOR FOR 
     SELECT   AC_ACCOUNT_ID
            , HASHBYTES ('SHA1',  ISNULL(AC_TRACK_DATA,        '')
                                + ISNULL(AC_HOLDER_NAME,       '')
                                + ISNULL(AC_HOLDER_ID,         '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_ID_TYPE), '')
                                + ISNULL(AC_HOLDER_ADDRESS_01, '')
                                + ISNULL(AC_HOLDER_ADDRESS_02, '')
                                + ISNULL(AC_HOLDER_ADDRESS_03, '')
                                + ISNULL(AC_HOLDER_CITY,       '')
                                + ISNULL(AC_HOLDER_ZIP,        '')
                                + ISNULL(AC_HOLDER_EMAIL_01,   '')
                                + ISNULL(AC_HOLDER_EMAIL_02,   '')
                                + ISNULL(AC_HOLDER_TWITTER_ACCOUNT, '')
                                + ISNULL(AC_HOLDER_PHONE_NUMBER_01, '')
                                + ISNULL(AC_HOLDER_PHONE_NUMBER_02, '')
                                + ISNULL(AC_HOLDER_COMMENTS,        '')
                                + ISNULL(AC_HOLDER_ID1, '')
                                + ISNULL(AC_HOLDER_ID2, '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_DOCUMENT_ID1), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_DOCUMENT_ID2), '')
                                + ISNULL(AC_HOLDER_NAME1, '')
                                + ISNULL(AC_HOLDER_NAME2, '')
                                + ISNULL(AC_HOLDER_NAME3, '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_GENDER),         '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_MARITAL_STATUS), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_BIRTH_DATE,   21), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_WEDDING_DATE, 21), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_LEVEL),                '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_LEVEL_NOTIFY),         '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_LEVEL_ENTERED,    21), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_LEVEL_EXPIRATION, 21), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_PIN),          '')
                                + ISNULL(CONVERT(NVARCHAR, AC_PIN_FAILURES), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_PIN_LAST_MODIFIED, 21), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_BLOCKED),           '')
                                + ISNULL(CONVERT(NVARCHAR, AC_ACTIVATED),         '')
                                + ISNULL(CONVERT(NVARCHAR, AC_BLOCK_REASON),         '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_IS_VIP),        '')
                                + ISNULL(AC_HOLDER_TITLE                        , '')
                                + ISNULL(AC_HOLDER_NAME4                        , '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_PHONE_TYPE_01),        '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_PHONE_TYPE_02),        '')
                                + ISNULL(AC_HOLDER_STATE                        , '')
                                + ISNULL(AC_HOLDER_COUNTRY                      , '') 
                                + ISNULL(CONVERT(NVARCHAR,AC_USER_TYPE)         , '') )
       FROM   INSERTED
      WHERE   AC_TRACK_DATA not like '%-NEW-%'

    SET NOCOUNT ON;

    OPEN PersonalInfoCursor

    FETCH NEXT FROM PersonalInfoCursor INTO @AccountId, @hash1

    WHILE @@FETCH_STATUS = 0
    BEGIN

        SELECT @hash0 = AC_MS_HASH FROM DELETED WHERE AC_ACCOUNT_ID = @AccountId

        SELECT @changed = CASE WHEN ( @hash0 = @hash1 ) THEN 0 ELSE 1 END

        IF @changed = 1
        BEGIN
           DECLARE @new_id as uniqueidentifier
           
           SET @new_id = NEWID()
           
            -- Personal Info
            UPDATE   ACCOUNTS
               SET   AC_MS_HASH                 = @hash1
                   , AC_MS_CHANGE_GUID          = @new_id
             WHERE   AC_ACCOUNT_ID              = @AccountId
             
            IF NOT EXISTS (SELECT 1 FROM MS_SITE_PENDING_ACCOUNTS WHERE SPA_ACCOUNT_ID = @AccountId)
              INSERT INTO   MS_SITE_PENDING_ACCOUNTS (SPA_ACCOUNT_ID, SPA_GUID) VALUES (@AccountId, @new_id )
            ELSE 
              UPDATE MS_SITE_PENDING_ACCOUNTS SET SPA_GUID = @new_id WHERE SPA_ACCOUNT_ID = @AccountId
            
        END

        FETCH NEXT FROM PersonalInfoCursor INTO @AccountId, @hash1
    END

    CLOSE PersonalInfoCursor
    DEALLOCATE PersonalInfoCursor

END
GO


/****** Enable/Disable MultiSite Triggers ******/

IF EXISTS (SELECT   1 
             FROM   GENERAL_PARAMS 
            WHERE   GP_GROUP_KEY   = N'Site' 
              AND   GP_SUBJECT_KEY = N'MultiSiteMember' 
              AND   GP_KEY_VALUE   = N'1')
      EXEC MultiSiteTriggersEnable 1
ELSE
      EXEC MultiSiteTriggersEnable 0

GO
