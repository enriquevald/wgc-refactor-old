/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 291;

SET @New_ReleaseId = 292;
SET @New_ScriptName = N'UpdateTo_18.292.038.sql';
SET @New_Description = N'New GP: TITO - Tickets.MaxMinutesAllowedToValidate';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/******* TABLES  *******/

/******* INDEXES *******/

/******* RECORDS *******/

IF NOT EXISTS (SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'TITO' AND GP_SUBJECT_KEY = 'Tickets.MaxMinutesAllowedToValidate')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('TITO', 'Tickets.MaxMinutesAllowedToValidate', '2880')
GO

IF NOT EXISTS (SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'TITO' AND GP_SUBJECT_KEY = 'PendingCancel.AllowToPlayOnEGM')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('TITO', 'PendingCancel.AllowToPlayOnEGM', '0')
GO

IF NOT EXISTS (SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'TITO' AND GP_SUBJECT_KEY = 'PendingCancel.AllowToRedeem')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('TITO', 'PendingCancel.AllowToRedeem', '0')
GO

IF NOT EXISTS (SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'TITO' AND GP_SUBJECT_KEY = 'PendingCancel.AllowToVoid')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('TITO', 'PendingCancel.AllowToVoid', '0')
GO

/******* PROCEDURES *******/

--------------------------------------------------------------------------------
-- Copyright � 2010 Win Systems International
--------------------------------------------------------------------------------
--
--   MODULE NAME: TITO_SetTicketPendingCancel.sql
--
--   DESCRIPTION: Update the valid tickets to pending cancellation
--
--        AUTHOR: Andreu Juli�
--
-- CREATION DATE: 14-JAN-2013
--
-- REVISION HISTORY:
--
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 14-JAN-2014 JPJ      First release (Include the script in the project).
-- 05-MAR-2014 DDM/JPJ  Added restrictions by Terminal and by provider
--------------------------------------------------------------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TITO_SetTicketPendingCancel]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[TITO_SetTicketPendingCancel]
GO

Create PROCEDURE [dbo].[TITO_SetTicketPendingCancel] 
  @pTerminalId                  INT,
  @pTicketValidationNumber      BIGINT,
  @pDefaultAllowRedemption      BIT,
  @pDefaultMaxAllowedTicketIn   MONEY,
  --- OUTPUT
  @pTicketId                    BIGINT   OUTPUT,
  @pTicketType                  INT      OUTPUT,
  @pTicketAmount                MONEY    OUTPUT,
  @pTicketExpiration            DATETIME OUTPUT

AS
BEGIN
  DECLARE  @_ticket_status_valid            INT
  DECLARE  @_ticket_status_pending_cancel   INT
  DECLARE  @_min_ticket_id                  BIGINT
  DECLARE  @_max_ticket_id                  BIGINT
  DECLARE  @_max_ticket_amount              MONEY
  DECLARE  @_redeem_allowed                 BIT
  DECLARE  @_promotion_id                   BIGINT
  DECLARE  @_restricted                     INT
  DECLARE  @_prov_id                        INT
  DECLARE  @_element_type                   INT
  DECLARE  @_ticket_type                    INT
  DECLARE  @_only_redeemable                INT
  DECLARE  @_virtual_account_id             BIGINT
  
  SET NOCOUNT ON

  SET @pTicketId = NULL
  SET @pTicketType = NULL
  SET @pTicketAmount = NULL
  SET @pTicketExpiration = NULL
  SET @_element_type = 3  -- EXPLOIT_ELEMENT_TYPE.PROMOTION

  SELECT    @_redeem_allowed    = ISNULL(TE_ALLOWED_REDEMPTION, @pDefaultAllowRedemption)
          , @_max_ticket_amount = ISNULL(TE_MAX_ALLOWED_TI,     @pDefaultMaxAllowedTicketIn)
          , @_prov_id           = ISNULL(TE_PROV_ID, -1)
          , @_virtual_account_id = ISNULL(TE_VIRTUAL_ACCOUNT_ID, 0)
    FROM   TERMINALS                     
   WHERE   TE_TERMINAL_ID = @pTerminalId 

  IF   @_max_ticket_amount IS NULL 
    OR @_redeem_allowed    IS NULL 
    OR @_redeem_allowed    = 0 
    RETURN
      
  SET @_ticket_status_valid          = 0 -- TITO_TICKET_STATUS.VALID
  SET @_ticket_status_pending_cancel = 5 -- TITO_TICKET_STATUS.PENDING_CANCEL

  SELECT    @_min_ticket_id = MIN (TI_TICKET_ID)
          , @_max_ticket_id = MAX (TI_TICKET_ID)
          , @_promotion_id  = MAX (ISNULL(TI_PROMOTION_ID,0))
          , @_ticket_type   = MAX (TI_TYPE_ID)
    FROM   TICKETS WITH (INDEX (IX_ti_validation_number_status))
   WHERE   TI_VALIDATION_NUMBER  = @pTicketValidationNumber
     AND   TI_STATUS             IN (@_ticket_status_valid, @_ticket_status_pending_cancel)
     AND   TI_AMOUNT  > 0
     AND ( TI_EXPIRATION_DATETIME IS NULL OR TI_EXPIRATION_DATETIME > GETDATE () )
     AND ( @_max_ticket_amount = 0 OR TI_AMOUNT <= @_max_ticket_amount )


  IF @_min_ticket_id =  @_max_ticket_id
  BEGIN
  
    -- We verify if the provider is restricted with no-redeemable
    SET @_only_redeemable = (SELECT ISNULL(PV_ONLY_REDEEMABLE,-1) FROM PROVIDERS WHERE PV_ID = @_prov_id)
      
    -- TITO_TICKET_TYPE.PROMO_NONREDEEM
    IF ((@_only_redeemable = 1 AND @_ticket_type = 2) or (@_only_redeemable = -1))
      RETURN

    -- We verify if the ticket can be played in this terminal due to promotion restriction
    -- @_restricted could be:
    --                -  null : Reedemable ticket
    --                -  1    : Terminal isn't restricted, ticket can be played
    --                -  0    : Terminal is restricted, ticket can't be played
    SELECT   @_restricted = CASE WHEN PM_RESTRICTED_TO_TERMINAL_LIST IS NOT NULL 
                                 THEN ISNULL( (SELECT   1  FROM   TERMINAL_GROUPS  WHERE   TG_TERMINAL_ID  = @pTerminalId 
                                                                                     AND   TG_ELEMENT_ID   = @_promotion_id 
                                                                                     AND   TG_ELEMENT_TYPE = @_element_type), 0) 
                                 ELSE 1 END
      FROM   PROMOTIONS 
     WHERE   PM_PROMOTION_ID = @_promotion_id
    
    IF @_restricted = 0
      RETURN
      
    -- One ticket found, change its status
    UPDATE    TICKETS 
       SET    TI_STATUS                    = @_ticket_status_pending_cancel 
            , TI_LAST_ACTION_TERMINAL_ID   = @pTerminalId
            , TI_LAST_ACTION_TERMINAL_TYPE = 1  --Always 1 from terminal (WCP)
            , TI_LAST_ACTION_DATETIME      = GETDATE() 
            , TI_LAST_ACTION_ACCOUNT_ID    = @_virtual_account_id 
       WHERE  TI_TICKET_ID                 = @_min_ticket_id 
        AND   TI_STATUS                    IN (@_ticket_status_valid, @_ticket_status_pending_cancel)
        AND   TI_AMOUNT  > 0
        AND ( TI_EXPIRATION_DATETIME IS NULL OR TI_EXPIRATION_DATETIME > GETDATE () )
        AND ( @_max_ticket_amount = 0 OR TI_AMOUNT <= @_max_ticket_amount )

    IF @@ROWCOUNT = 1
    SELECT    @pTicketId         = TI_TICKET_ID
            , @pTicketType       = TI_TYPE_ID
            , @pTicketAmount     = TI_AMOUNT
            , @pTicketExpiration = TI_EXPIRATION_DATETIME
      FROM    TICKETS 
     WHERE    TI_TICKET_ID  = @_min_ticket_id 

  END

END

GO