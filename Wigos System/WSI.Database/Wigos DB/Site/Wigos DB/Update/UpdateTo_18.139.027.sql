/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 138;

SET @New_ReleaseId = 139;
SET @New_ScriptName = N'UpdateTo_18.139.027.sql';
SET @New_Description = N'Tables & SP for Antimoney laundering control.';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END
 
/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/

CREATE TABLE [dbo].[aml_monthly](
	[amm_month] [datetime] NOT NULL,
	[amm_account_id] [bigint] NOT NULL,
	[amm_split_a] [money] NOT NULL,
	[amm_prize] [money] NOT NULL,
 CONSTRAINT [PK_aml_monthly] PRIMARY KEY CLUSTERED 
(
	[amm_month] ASC,
	[amm_account_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[aml_daily](
	[amd_day] [datetime] NOT NULL,
	[amd_account_id] [bigint] NOT NULL,
	[amd_split_a] [money] NOT NULL,
	[amd_prize] [money] NOT NULL,
 CONSTRAINT [PK_aml_daily] PRIMARY KEY CLUSTERED 
(
	[amd_day] ASC,
	[amd_account_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [IX_amm_account_month] ON [dbo].[aml_monthly] 
(
	[amm_account_id] ASC,
	[amm_month] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [IX_amd_account_day] ON [dbo].[aml_daily] 
(
	[amd_account_id] ASC,
	[amd_day] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

/****** PROCEDURES ******/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AML_CreateDailyMonthly]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[AML_CreateDailyMonthly]
GO

CREATE PROCEDURE AML_CreateDailyMonthly 
	@Day DATETIME
AS
BEGIN
	DECLARE @Date1 DATETIME
	DECLARE @Date2 DATETIME
	DECLARE @Count DATETIME

	SET NOCOUNT ON;

	-- Delete any previous row of the given day	
	SET @Date1 = dbo.Opening (0, @Day)
	SET @Date1 = DATEADD (HOUR, -DATEPART(HOUR, @Date1), @Date1)
	SET @Date2 = DATEADD (DAY, 1, @Date1)

	SELECT @Count = COUNT(*) FROM AML_DAILY WHERE amd_day >= @Date1 AND amd_day < @Date2
	IF @Count > 0 
    BEGIN
		DELETE AML_DAILY WHERE amd_day >= @Date1 AND amd_day < @Date2
    END

    -- Generate 'day'
	SET @Date1 = dbo.Opening (0, @Day)
	SET @Date2 = DATEADD (DAY, 1, @Date1)

	INSERT INTO AML_DAILY (amd_day, amd_account_id, amd_split_a, amd_prize)
	(
		select	DATEADD(DAY, DATEDIFF(HOUR, @Date1, am_datetime)/24, @Date1)			
			   , am_account_id 															
			   , sum(case when (am_type =  1) then am_add_amount else 0 end )
			   - sum(case when (am_type = 77) then am_sub_amount else 0 end )           
			   , sum(case when (am_type =  2) then am_sub_amount else 0 end )			
		  from account_movements  with (index (IX_am_datetime))
		where am_datetime >= @Date1
		  and am_datetime <  @Date2
		  and am_type in (1, 2, 77)
		group by am_account_id,  DATEADD(DAY, DATEDIFF(HOUR, @Date1, am_datetime)/24, @Date1)	
	)

	-- First day of the month at '00:00:00'
	SET @Date1 = DATEADD(DAY, 1 - DATEPART(DAY, @Date1), @Date1)
    SET @Date1 = DATEADD (HOUR, -DATEPART(HOUR, @Date1), @Date1)
	SELECT @Count = COUNT(*) FROM AML_MONTHLY WHERE amm_month >= @Date1 AND amm_month < DATEADD(MONTH, 1, @Date1)
	IF @Count > 0 
    BEGIN
		DELETE AML_MONTHLY WHERE amm_month >= @Date1 AND amm_month < DATEADD(MONTH, 1, @Date1)
    END

	-- First day of the month at 'Opening'
	SET @Date1 = dbo.Opening (0, @Day)
    SET @Date1 = DATEADD(DAY, 1 - DATEPART(DAY, @Date1), @Date1)
    INSERT INTO AML_MONTHLY (amm_month, amm_account_id, amm_split_a, amm_prize)
	(
        select	@Date1			
			   , amd_account_id 		
			   , SUM(amd_split_a) 
			   , SUM(amd_prize)  
		  from AML_DAILY  with (index (PK_aml_daily))
		where amd_day >= @Date1
		  and amd_day <  DATEADD(MONTH, 1, @Date1)
		group by amd_account_id
	)
END
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AML_DailyWork]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[AML_DailyWork]
GO

CREATE PROCEDURE AML_DailyWork 
AS
BEGIN
	DECLARE @Date0 DATETIME
	DECLARE @Date1 DATETIME
	DECLARE @Start DATETIME

	SET @Start = GETDATE ()
	SET @Date1 = (SELECT MAX(amd_day) FROM AML_DAILY WITH (INDEX (PK_aml_daily)))
	SET @Date1 = DATEADD (DAY, 1, @Date1)

	IF @Date1 IS NULL 
	BEGIN
	   SET @Date0 = dbo.Opening(0, '2013-07-01T23:59:59')  --- First Day 'AntiLavado'
	   SET @Date1 = (SELECT dbo.Opening(0, MIN(AM_DATETIME)) 
					   FROM ACCOUNT_MOVEMENTS
					  WHERE AM_DATETIME >= @Date0)
	   IF @Date1 IS NULL RETURN
	END

	SET @Date0 =  (SELECT dbo.TodayOpening(0))

	WHILE (@Date1 < @Date0)
	BEGIN
	   EXEC AML_CreateDailyMonthly @Date1
	   SET @Date1 = @Date1 + 1
   	   IF DATEDIFF (SECOND, @Start, GETDATE()) > 60 RETURN
	END

END
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AML_ThisMonthRechargesPrizes]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[AML_ThisMonthRechargesPrizes]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].AML_AccountCurrentMonth') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[AML_AccountCurrentMonth]
GO

CREATE PROCEDURE [dbo].AML_AccountCurrentMonth 
  @AccountId  BIGINT,
  @SplitA     MONEY OUTPUT,
  @Prizes     MONEY OUTPUT
AS
BEGIN
  SET NOCOUNT ON;

  DECLARE @_today as DATETIME
  DECLARE @_day_1 as DATETIME
  DECLARE @Date1  as DATETIME

  SET  @_today = dbo.TodayOpening(0)
  SET  @_day_1 = DATEADD (DAY, 1 - DATEPART(DAY, @_today), @_today)

  SET @Date1 = NULL;

  SELECT @Date1 = MAX(amd_day), @SplitA = SUM(amd_split_a), @Prizes = SUM(amd_prize)
    FROM AML_DAILY WITH (INDEX (IX_amd_account_day))
   WHERE amd_account_id = @AccountId
     AND amd_day       >= @_day_1 

  IF @Date1 IS NULL 
  BEGIN 
      SET @Date1  = @_day_1;
	  SET @SplitA = 0;
	  SET @Prizes = 0;
  END
  ELSE  
  BEGIN
      SET @Date1  = DATEADD(DAY, 1, @Date1)
	  SET @SplitA = ISNULL(@SplitA, 0);
	  SET @Prizes = ISNULL(@Prizes, 0);
  END


  SELECT   @SplitA = @SplitA + 
			     isnull(sum(case when (am_type =  1) then am_add_amount else 0 end ), 0)
			   - isnull(sum(case when (am_type = 77) then am_sub_amount else 0 end ), 0),
		   @Prizes = @Prizes + isnull(sum(case when (am_type =  2) then am_sub_amount else 0 end ), 0)			
    FROM   ACCOUNT_MOVEMENTS WITH (INDEX (IX_MOVEMENTS_ACCOUNT_DATE))
   WHERE   AM_DATETIME >= @Date1
     AND   AM_TYPE IN (1, 2)
     AND   AM_ACCOUNT_ID = @AccountId

END
GO

GRANT EXECUTE ON [dbo].[AML_AccountCurrentMonth] TO [wggui] WITH GRANT OPTION
GO

/****** RECORDS ******/

--
--
--
INSERT INTO GENERAL_PARAMS ( GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE ) VALUES ('Gifts.RedeemableCredits', 'AsCashIn', '0')

--
--
--
INSERT INTO GENERAL_PARAMS ( GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE ) VALUES ('NoteAcceptor', 'HighestBankNoteValue', '1000')

