/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 117;

SET @New_ReleaseId = 118;
SET @New_ScriptName = N'UpdateTo_18.118.025.sql';
SET @New_Description = N'Update Multisite trigger: Trigger_SiteToMultiSite_Points. Delete 3 tasks in MS_SITE_TASKS table.';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/


/****** INDEXES ******/


/****** RECORDS ******/

DELETE   MS_SITE_TASKS
WHERE   st_task_id IN(21,31,32)


GO

/****** TRIGGERS ******/

ALTER TRIGGER [dbo].[Trigger_SiteToMultiSite_Points]
ON [dbo].[ACCOUNT_MOVEMENTS]
AFTER INSERT
NOT FOR REPLICATION
AS
  BEGIN
  
    IF NOT EXISTS (SELECT   1 
                     FROM   GENERAL_PARAMS 
                    WHERE   GP_GROUP_KEY   = N'Site' 
                      AND   GP_SUBJECT_KEY = N'MultiSiteMember' 
                      AND   GP_KEY_VALUE   = N'1')
    BEGIN
          RETURN
    END
  
    SET NOCOUNT ON

    -- Insert movement to synchronize
    INSERT INTO   MS_SITE_PENDING_ACCOUNT_MOVEMENTS 
                ( SPM_MOVEMENT_ID )
         SELECT   AM_MOVEMENT_ID 
           FROM   INSERTED 
          WHERE   AM_TYPE IN ( 36, 37, 38, 39, 40, 41, 42, 46, 50, 60, 61, 62, 66, 67, 71, 101 ) 
   
    SET NOCOUNT OFF

  END -- [Trigger_SiteToMultiSite_Points]

GO
