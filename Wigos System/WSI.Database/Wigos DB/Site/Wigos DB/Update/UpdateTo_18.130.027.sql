/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 129;

SET @New_ReleaseId = 130;
SET @New_ScriptName = N'UpdateTo_18.130.027.sql';
SET @New_Description = N'Update InsertProvidersGames';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END
 
/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO


  DECLARE @providers_site_nul AS INT
  DECLARE @is_multisite AS INT
  SET @is_multisite = (SELECT gp_key_value FROM general_params gp WHERE gp.gp_group_key = 'Site' AND gp.gp_subject_key = 'MultiSiteMember')

  IF @is_multisite IS NOT NULL AND @is_multisite = 1
  BEGIN
  	PRINT ''
  END
  ELSE
  BEGIN

     IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROVIDERS_GAMES_TMP]') AND type in (N'U'))
     DROP TABLE [dbo].[PROVIDERS_GAMES_TMP]
     
       CREATE TABLE [dbo].[providers_bck](
             [pv_id] [int] IDENTITY(0,1) NOT NULL,
             [pv_name] [nvarchar](50) NOT NULL,
             [pv_hide] [bit] NOT NULL,
             [pv_points_multiplier] [money] NOT NULL,
             [pv_3gs] [bit] NOT NULL,
             [pv_3gs_vendor_id] [nvarchar](50) NULL,
             [pv_3gs_vendor_ip] [nvarchar](50) NULL,
             [pv_site_jackpot] [bit] NOT NULL,
             [pv_only_redeemable] [bit] NOT NULL,
             [pv_id_new] [int]
       CONSTRAINT [PK_providers_bck] PRIMARY KEY CLUSTERED 
       (
             [pv_id] ASC
       )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
       ) ON [PRIMARY]

 
       SET IDENTITY_INSERT providers_bck ON

       INSERT INTO [providers_bck]
                  (pv_id, pv_name, pv_hide, pv_3gs, pv_3gs_vendor_id,
                   pv_3gs_vendor_ip, pv_points_multiplier, pv_site_jackpot,
                   pv_only_redeemable)
            SELECT P.pv_id, P.pv_name, P.pv_hide, P.pv_3gs, P.pv_3gs_vendor_id,
                   P.pv_3gs_vendor_ip, P.pv_points_multiplier, P.pv_site_jackpot,
                   P.pv_only_redeemable
              FROM providers p

       SET IDENTITY_INSERT providers_bck OFF
  
       --BEGIN TRANSACTION
       -- SITE NO HACE FALTA BORRAR
       --  DELETE FROM providers
       --ROLLBACK
       
     /*EMPEZAMOS CON LOS JUEGOS*/
       
       IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[providers_games_new]') AND type in (N'U'))
       DROP TABLE [dbo].[providers_games_new]
       
       IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROVIDERS_GAMES_TMP]') AND type in (N'U'))
       DROP TABLE [dbo].[PROVIDERS_GAMES_TMP]
       
       IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROVIDERS_GAMES_TMP_REPEATED]') AND type in (N'U'))
       DROP TABLE [dbo].[PROVIDERS_GAMES_TMP_REPEATED]
       
       IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROVIDERS_GAMES_FINALLY]') AND type in (N'U'))
       DROP TABLE [dbo].[PROVIDERS_GAMES_FINALLY]
       
       CREATE TABLE PROVIDERS_GAMES_FINALLY
       (
         PG_PV_ID [INT],
         PG_GAME_ID [INT] IDENTITY(1,1) NOT NULL,
         PG_GAME_NAME [NVARCHAR](50),
         PG_PAYOUT_1 [MONEY] NULL,
         PG_PAYOUT_2 [MONEY] NULL,
         PG_PAYOUT_3 [MONEY] NULL,
         PG_PAYOUT_4 [MONEY] NULL,
         PG_PAYOUT_5 [MONEY] NULL,
         PG_PAYOUT_6 [MONEY] NULL,
         PG_PAYOUT_7 [MONEY] NULL,
         PG_PAYOUT_8 [MONEY] NULL,
         PG_PAYOUT_9 [MONEY] NULL,
         PG_PAYOUT_10 [MONEY] NULL,
         PG_GAME_TARGET [NVARCHAR](50) NULL,
         PG_PV_ID_NEW [INT] NULL,
         PG_GAME_ID_NEW [INT] NULL,
       )

       CREATE TABLE PROVIDERS_GAMES_TMP
       (
         PG_PV_ID [INT],
         PG_GAME_ID [INT] IDENTITY(1,1) NOT NULL,
         PG_GAME_NAME [NVARCHAR](50),
         PG_PAYOUT_1 [MONEY] NULL,
         PG_PAYOUT_2 [MONEY] NULL,
         PG_PAYOUT_3 [MONEY] NULL,
         PG_PAYOUT_4 [MONEY] NULL,
         PG_PAYOUT_5 [MONEY] NULL,
         PG_PAYOUT_6 [MONEY] NULL,
         PG_PAYOUT_7 [MONEY] NULL,
         PG_PAYOUT_8 [MONEY] NULL,
         PG_PAYOUT_9 [MONEY] NULL,
         PG_PAYOUT_10 [MONEY] NULL,
         PG_GAME_SOURCE_ID [INT],
         PG_TARGET_GAME_ID [INT],
         PG_GAME_NAME_ORIGINAL [NVARCHAR](50)
       )

       CREATE TABLE PROVIDERS_GAMES_TMP_REPEATED
       (
         PG_PV_ID [INT],
         PG_GAME_ID [INT],
         PG_GAME_NAME [NVARCHAR](50),
         PG_PAYOUT_1 [MONEY] NULL,
         PG_TARGET_GAME_ID [INT],
         PG_GAME_NAME_ORIGINAL [NVARCHAR](50),
         PG_GAME_ID_DESTINATION [INT],
         PG_PAYOUT_ID_DESTINATION [INT],
       )

       CREATE TABLE [dbo].[providers_games_new](
             [pg_pv_id] [int] NOT NULL,
             [pg_game_id] [int] IDENTITY(1,1) NOT NULL,
             [pg_game_name] [nvarchar](50) NOT NULL,
             [pg_payout_1] [money] NULL,
             [pg_payout_2] [money] NULL,
             [pg_payout_3] [money] NULL,
             [pg_payout_4] [money] NULL,
             [pg_payout_5] [money] NULL,
             [pg_payout_6] [money] NULL,
             [pg_payout_7] [money] NULL,
             [pg_payout_8] [money] NULL,
             [pg_payout_9] [money] NULL,
             [pg_payout_10] [money] NULL,
             [pg_ms_sequence_id] [bigint] NULL,
       CONSTRAINT [PK_providers_games_new] PRIMARY KEY CLUSTERED 
       (
             [pg_pv_id] ASC,
             [pg_game_id] ASC
       )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
       ) ON [PRIMARY]

       /** FIN Creamos Tablas necesarias
       *  [providers_games_new]
       **/

       /** Insertamos datos 
       *  [PROVIDERS_GAMES_TMP]
       **/
       ----SET IDENTITY_INSERT PROVIDERS_GAMES_TMP ON
       
       INSERT INTO PROVIDERS_GAMES_TMP (pg_pv_id, PG_TARGET_GAME_ID, pg_game_name, pg_payout_1, PG_GAME_NAME_ORIGINAL)
             SELECT  TE.te_prov_id,                                                                                                     -- ID_PROVEEDOR
                         TGT.tgt_target_game_id, 
                               (     CASE 
                                                WHEN GM.gm_name LIKE '%!%!]' ESCAPE '!'  THEN 
                                                      SUBSTRING (GM.gm_name, 0, Len (GM.gm_name) - 7)
                                                ELSE  GM.gm_name
                                      END ) as game_name,

                               (     CASE  
                                                WHEN GM.gm_name LIKE '%!%!]' ESCAPE '!' THEN 
                                                      --REPLACE( SUBSTRING ( GM.gm_name ,Len(GM.gm_name) - 6 , 5 ), '.',',')
                                                      --SUBSTRING ( GM.gm_name ,Len(GM.gm_name) - 6 , 5 )
                                                      LEFT(SUBSTRING(GM.gm_name, CHARINDEX('[',GM.gm_name)+1, 100),CHARINDEX(']',SUBSTRING(GM.gm_name, CHARINDEX('[',GM.gm_name)+1, 100))-2)
                                                ELSE  ''
                                      END ) as game_payout,
                          GM.gm_name

             FROM  terminal_game_translation TGT, 
                               terminals TE, 
                               games GM

             WHERE TGT.tgt_terminal_id       = TE.te_terminal_id 
               AND TGT.tgt_target_game_id    = GM.gm_game_id
               --AND TGT.tgt_target_game_id    <> @Unknown_id

             GROUP BY TE.te_prov_id, TGT.tgt_target_game_id, GM.gm_name

             ORDER BY TE.te_prov_id, TGT.tgt_target_game_id;

 
       ----SET IDENTITY_INSERT PROVIDERS_GAMES_TMP OFF

 
       /** repetidos **/

       INSERT INTO PROVIDERS_GAMES_TMP_REPEATED (PG_GAME_NAME , PG_GAME_ID, PG_PV_ID, PG_PAYOUT_1, PG_TARGET_GAME_ID, PG_GAME_NAME_ORIGINAL)
                     SELECT   DISTINCT PG_GAME_NAME, PG_GAME_ID, PG_PV_ID, PG_PAYOUT_1, PG_TARGET_GAME_ID,
                            PG_GAME_NAME_ORIGINAL
                       FROM   PROVIDERS_GAMES_TMP
                      WHERE   PG_GAME_NAME IN
                              (
                                    SELECT   PG_GAME_NAME
                                      FROM   PROVIDERS_GAMES_TMP AS TMP
                                  GROUP BY   PG_GAME_NAME
                                    HAVING   COUNT(*) > 1
                              ) 
                   ORDER BY   PG_GAME_NAME

 
       /** FIN Insertamos datos 
       *  [PROVIDERS_GAMES_TMP]
       **/

 
       /** CURSOR TO DELETE REPETIDOS **/
       DECLARE @provider_id INT
       DECLARE @provider_id_ant INT
       DECLARE @game_id INT
       DECLARE @game_id_ant INT
       DECLARE @game_name NVARCHAR(50)
       DECLARE @game_name_ant NVARCHAR(50)
       DECLARE @pg_payout MONEY
       DECLARE @delete BIT
       DECLARE @payout_id INT

       DECLARE @pg_payout_1 MONEY
       DECLARE @pg_payout_2 MONEY
       DECLARE @pg_payout_3 MONEY
       DECLARE @pg_payout_4 MONEY
       DECLARE @pg_payout_5 MONEY
       DECLARE @pg_payout_6 MONEY
       DECLARE @pg_payout_7 MONEY
       DECLARE @pg_payout_8 MONEY
       DECLARE @pg_payout_9 MONEY
       DECLARE @pg_payout_10 MONEY

            DECLARE   _PROVIDERS_GAMES CURSOR FOR
             SELECT   PG_PV_ID, PG_GAME_ID, PG_GAME_NAME, PG_PAYOUT_1
               FROM   PROVIDERS_GAMES_TMP_REPEATED
           ORDER BY   PG_PV_ID, PG_GAME_NAME  
 
         OPEN _providers_games

         FETCH NEXT FROM _providers_games INTO @provider_id, @game_id, @game_name, @pg_payout

         WHILE @@Fetch_Status = 0
         BEGIN -- CURSOR
           
           SET @delete = 1
           SET @payout_id = 0
           
           IF (@game_name_ant IS NULL OR @provider_id_ant IS NULL)
           BEGIN
               SET @game_name_ant = @game_name
               SET @provider_id_ant = @provider_id
               SET @game_id_ant = @game_id
               SET @delete = 0
           END
           IF (@provider_id_ant <> @provider_id OR @game_name_ant <> @game_name)
           BEGIN
             SET @game_name_ant = @game_name
             SET @provider_id_ant = @provider_id
             SET @game_id_ant = @game_id
             SET @delete = 0
           END
           
            IF @delete = 1
             BEGIN
               -- DELETE OTHERS
                       
               SELECT   @pg_payout_1  = PG_PAYOUT_1 
                      , @pg_payout_2  = PG_PAYOUT_2
                      , @pg_payout_3  = PG_PAYOUT_3
                      , @pg_payout_4  = PG_PAYOUT_4
                      , @pg_payout_5  = PG_PAYOUT_5
                      , @pg_payout_6  = PG_PAYOUT_6
                      , @pg_payout_7  = PG_PAYOUT_7
                      , @pg_payout_8  = PG_PAYOUT_8
                      , @pg_payout_9  = PG_PAYOUT_9
                      , @pg_payout_10 = PG_PAYOUT_10
                 FROM   PROVIDERS_GAMES_TMP
                WHERE   PG_PV_ID = @provider_id_ant
                  AND   PG_GAME_ID = @game_id_ant        
               
               
               IF @pg_payout_1 IS NULL
               BEGIN
                   UPDATE PROVIDERS_GAMES_TMP SET PG_PAYOUT_1 = @pg_payout WHERE PG_PV_ID = @provider_id_ant AND PG_GAME_ID = @game_id_ant
                   SET @payout_id = 1
               END
               IF @pg_payout_2 IS NULL AND @payout_id = 0
               BEGIN
                   UPDATE PROVIDERS_GAMES_TMP SET PG_PAYOUT_2 = @pg_payout WHERE PG_PV_ID = @provider_id_ant AND PG_GAME_ID = @game_id_ant
                   SET @payout_id = 2
               END
               IF @pg_payout_3 IS NULL AND @payout_id = 0
               BEGIN
                   UPDATE PROVIDERS_GAMES_TMP SET PG_PAYOUT_3 = @pg_payout WHERE PG_PV_ID = @provider_id_ant AND PG_GAME_ID = @game_id_ant
                   SET @payout_id = 3
               END
               IF @pg_payout_4 IS NULL AND @payout_id = 0
               BEGIN
                   UPDATE PROVIDERS_GAMES_TMP SET PG_PAYOUT_4 = @pg_payout WHERE PG_PV_ID = @provider_id_ant AND PG_GAME_ID = @game_id_ant
                   SET @payout_id = 4
               END
               IF @pg_payout_5 IS NULL AND @payout_id = 0
               BEGIN
                   UPDATE PROVIDERS_GAMES_TMP SET PG_PAYOUT_5 = @pg_payout WHERE PG_PV_ID = @provider_id_ant AND PG_GAME_ID = @game_id_ant
                   SET @payout_id = 5
               END
               IF @pg_payout_6 IS NULL AND @payout_id = 0
               BEGIN
                   UPDATE PROVIDERS_GAMES_TMP SET PG_PAYOUT_6 = @pg_payout WHERE PG_PV_ID = @provider_id_ant AND PG_GAME_ID = @game_id_ant
                   SET @payout_id = 6
               END
               IF @pg_payout_7 IS NULL AND @payout_id = 0
               BEGIN
                   UPDATE PROVIDERS_GAMES_TMP SET PG_PAYOUT_7 = @pg_payout WHERE PG_PV_ID = @provider_id_ant AND PG_GAME_ID = @game_id_ant
                   SET @payout_id = 7
               END
               IF @pg_payout_8 IS NULL AND @payout_id = 0
               BEGIN
                   UPDATE PROVIDERS_GAMES_TMP SET PG_PAYOUT_8 = @pg_payout WHERE PG_PV_ID = @provider_id_ant AND PG_GAME_ID = @game_id_ant
                   SET @payout_id = 8
               END
               IF @pg_payout_9 IS NULL AND @payout_id = 0
               BEGIN
                   UPDATE PROVIDERS_GAMES_TMP SET PG_PAYOUT_9 = @pg_payout WHERE PG_PV_ID = @provider_id_ant AND PG_GAME_ID = @game_id_ant
                   SET @payout_id = 9
               END
               IF  @pg_payout_10 IS NULL AND @payout_id = 0
               BEGIN
                   UPDATE PROVIDERS_GAMES_TMP SET PG_PAYOUT_10 = @pg_payout WHERE PG_PV_ID = @provider_id_ant AND PG_GAME_ID = @game_id_ant
                   SET @payout_id = 10
               END
               
               
               DELETE FROM PROVIDERS_GAMES_TMP WHERE PG_PV_ID = @provider_id AND PG_GAME_ID = @game_id
               
               UPDATE PROVIDERS_GAMES_TMP_REPEATED
                  SET
                   PG_GAME_ID_DESTINATION = @game_id_ant,
                   PG_PAYOUT_ID_DESTINATION = @payout_id
                WHERE PG_PV_ID = @provider_id
                  AND PG_GAME_ID = @game_id
                  
                  PRINT @payout_id
             END

           FETCH NEXT FROM _providers_games INTO @provider_id, @game_id, @game_name, @pg_payout    
         END -- CURSOR

         CLOSE _providers_games
         DEALLOCATE _providers_games

         DELETE FROM PROVIDERS_GAMES_TMP_REPEATED WHERE PG_PAYOUT_ID_DESTINATION IS NULL AND PG_PAYOUT_ID_DESTINATION IS NULL
         /** FIN CURSOR TO DELETE REPETIDOS **/

         SET IDENTITY_INSERT PROVIDERS_GAMES_FINALLY ON
         --PRINT 'PROVIDERS_GAMES_FINALLY'
         INSERT INTO PROVIDERS_GAMES_FINALLY (PG_PV_ID, PG_GAME_ID, PG_GAME_NAME,
                     PG_PAYOUT_1, PG_PAYOUT_2, PG_PAYOUT_3, PG_PAYOUT_4, PG_PAYOUT_5,
                     PG_PAYOUT_6, PG_PAYOUT_7, PG_PAYOUT_8, PG_PAYOUT_9, PG_PAYOUT_10)
         SELECT pgt.PG_PV_ID, pgt.PG_GAME_ID, pgt.PG_GAME_NAME, pgt.PG_PAYOUT_1,
                pgt.PG_PAYOUT_2, pgt.PG_PAYOUT_3, pgt.PG_PAYOUT_4, pgt.PG_PAYOUT_5,
                pgt.PG_PAYOUT_6, pgt.PG_PAYOUT_7, pgt.PG_PAYOUT_8, pgt.PG_PAYOUT_9,
                pgt.PG_PAYOUT_10
           FROM PROVIDERS_GAMES_TMP pgt
         SET IDENTITY_INSERT PROVIDERS_GAMES_FINALLY OFF
         
         
         --PRINT 'La primera tabla es PROVIDERS'
         --PRINT 'La segunda tabla es GAMES'
         --PRINT 'Hay que insertar estos datos en el Site'
         --SELECT * FROM PROVIDERS_BCK
         --SELECT * FROM PROVIDERS_GAMES_FINALLY
         
         /* INSERTAMOS TODOS LOS JUEGOS */
         
         SET IDENTITY_INSERT providers_games ON
         INSERT INTO [providers_games]
           ([pg_pv_id] ,[PG_GAME_ID] ,[pg_game_name]
                                     ,[pg_payout_1] ,[pg_payout_2] ,[pg_payout_3] ,[pg_payout_4] ,[pg_payout_5]
                                     ,[pg_payout_6] ,[pg_payout_7] ,[pg_payout_8] ,[pg_payout_9] ,[pg_payout_10])
         SELECT pgt.PG_PV_ID, pgt.PG_GAME_ID, pgt.PG_GAME_NAME, pgt.PG_PAYOUT_1,
                pgt.PG_PAYOUT_2, pgt.PG_PAYOUT_3, pgt.PG_PAYOUT_4, pgt.PG_PAYOUT_5,
                pgt.PG_PAYOUT_6, pgt.PG_PAYOUT_7, pgt.PG_PAYOUT_8, pgt.PG_PAYOUT_9,
                pgt.PG_PAYOUT_10
           FROM PROVIDERS_GAMES_FINALLY pgt

         SET IDENTITY_INSERT providers_games OFF
         /* FIN INSERTAMOS TODOS LOS JUEGOS*/
     /*FIN EMPEZAMOS CON LOS JUEGOS*/
     IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROVIDERS_GAMES_TMP]') AND type in (N'U'))
     DROP TABLE [dbo].[PROVIDERS_GAMES_TMP]


     IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROVIDERS_GAMES_TMP]') AND type in (N'U'))
     BEGIN
     DROP TABLE [dbo].[PROVIDERS_GAMES_TMP]
     END
     
   IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[terminal_game_translation_tmp]') AND type in (N'U'))
   BEGIN
     DROP TABLE [dbo].[terminal_game_translation_tmp]
   END
   
   IF  NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[providers_games]') AND type in (N'U'))
   BEGIN
      SELECT * FROM providers_bck WHERE pv_id_new IS NULL
      PRINT 'FALTAN PROVEEDORES, EN PESTA�A RESULTADOS PUEDE CONSULTARLOS!!'
      RAISERROR('FALTAN PROVEEDORES, EN PESTA�A RESULTADOS PUEDE CONSULTARLOS!!', 20,0) with LOG
   END
   
      SELECT @providers_site_nul = COUNT(*) FROM providers
   IF @providers_site_nul < 1
   BEGIN
      SELECT * FROM providers_bck WHERE pv_id_new IS NULL
      PRINT 'FALTAN PROVEEDORES, EN PESTA�A RESULTADOS PUEDE CONSULTARLOS!!'
       RAISERROR('FALTAN PROVEEDORES, EN PESTA�A RESULTADOS PUEDE CONSULTARLOS!!', 20,0) with LOG
       --NO EXISTEN TODOS LOS PROVEEDORES, DAR DE ALTA EN EL CENTRO Y VOLVER A EJECUTAR CUANDO SE DESCARGUEN NUEVAMENTE
   END
   
   --REPRESENTA QUE YA SE DESCARGARON LOS PROVIDERS A LA NUEVA TABLA
   UPDATE providers_bck SET pv_id_new = (SELECT p.pv_id FROM providers p WHERE p.pv_name = providers_bck.pv_name)
   
   SELECT @providers_site_nul = COUNT(*) FROM providers_bck WHERE pv_id_new IS NULL

   IF @providers_site_nul > 0
   BEGIN
      SELECT * FROM providers_bck WHERE pv_id_new IS NULL
      PRINT 'FALTAN PROVEEDORES, EN PESTA�A RESULTADOS PUEDE CONSULTARLOS!!'
       RAISERROR('FALTAN PROVEEDORES, EN PESTA�A RESULTADOS PUEDE CONSULTARLOS!!', 20,0) with LOG
       --NO EXISTEN TODOS LOS PROVEEDORES, DAR DE ALTA EN EL CENTRO Y VOLVER A EJECUTAR CUANDO SE DESCARGUEN NUEVAMENTE
   END
   
   --ACTUALIZAMOS PROVIDERS Y TERMINALS SEGUN LOS DATOS DESCARGADOS Y LOS QUE TEN�AMOS
   ALTER TABLE providers DISABLE TRIGGER ProviderTerminalTrigger
   ALTER TABLE terminals DISABLE TRIGGER TerminalProviderTrigger

   UPDATE  providers
      SET   pv_hide = (SELECT providers_bck.pv_hide FROM providers_bck WHERE providers_bck.pv_id_new = providers.pv_id) ,
             pv_points_multiplier = (SELECT providers_bck.pv_points_multiplier FROM providers_bck WHERE providers_bck.pv_id_new = providers.pv_id) ,
             pv_3gs = (SELECT providers_bck.pv_3gs FROM providers_bck WHERE providers_bck.pv_id_new = providers.pv_id) ,
             pv_3gs_vendor_id = (SELECT providers_bck.pv_3gs_vendor_id FROM providers_bck WHERE providers_bck.pv_id_new = providers.pv_id) ,
             pv_3gs_vendor_ip = (SELECT providers_bck.pv_3gs_vendor_ip FROM providers_bck WHERE providers_bck.pv_id_new = providers.pv_id) ,
             pv_site_jackpot = (SELECT providers_bck.pv_site_jackpot FROM providers_bck WHERE providers_bck.pv_id_new = providers.pv_id) ,
             pv_only_redeemable = (SELECT providers_bck.pv_only_redeemable FROM providers_bck WHERE providers_bck.pv_id_new = providers.pv_id)
     
   UPDATE  terminals 
      SET  te_prov_id = ISNULL((SELECT p.pv_id_new FROM providers_bck p WHERE p.pv_id = te_prov_id),te_prov_id)
          ,te_provider_id = ISNULL((SELECT p.pv_name FROM providers_bck p WHERE p.pv_id = te_prov_id),te_provider_id)
          
  ALTER TABLE providers ENABLE TRIGGER ProviderTerminalTrigger
  ALTER TABLE terminals ENABLE TRIGGER TerminalProviderTrigger
  -- FIN ACTUALIZAMOS PROVIDERS Y TERMINALS SEGUN LOS DATOS DESCARGADOS Y LOS QUE TEN�AMOS
  
  
  /*PARSEAMOS TODOS LOS JUEGOS PARA BUSCAR SUS IGUALES*/
          /* PRIMERO MIRAMOS QUE TODOS ESTEN DESCARGADOS*/
                      CREATE TABLE PROVIDERS_GAMES_TMP
                        (
                            PG_PV_ID [INT],
                            PG_GAME_ID [INT] IDENTITY(1,1) NOT NULL,
                            PG_GAME_NAME [NVARCHAR](50),
                            PG_PAYOUT_1 [MONEY] NULL,
                            PG_PAYOUT_2 [MONEY] NULL,
                            PG_PAYOUT_3 [MONEY] NULL,
                            PG_PAYOUT_4 [MONEY] NULL,
                            PG_PAYOUT_5 [MONEY] NULL,
                            PG_PAYOUT_6 [MONEY] NULL,
                            PG_PAYOUT_7 [MONEY] NULL,
                            PG_PAYOUT_8 [MONEY] NULL,
                            PG_PAYOUT_9 [MONEY] NULL,
                            PG_PAYOUT_10 [MONEY] NULL,
                            PG_GAME_SOURCE_ID [INT],
                            PG_TARGET_GAME_ID [INT],
                            PG_GAME_NAME_ORIGINAL [NVARCHAR](50)
                        )
                        INSERT INTO PROVIDERS_GAMES_TMP (pg_pv_id, PG_TARGET_GAME_ID, pg_game_name, pg_payout_1, PG_GAME_NAME_ORIGINAL)
                            SELECT  TE.te_prov_id, -- ID_PROVEEDOR
                                             TGT.tgt_target_game_id, 
                                                     (     CASE 
                                                                              WHEN GM.gm_name LIKE '%!%!]' ESCAPE '!'  THEN 
                                                                                       SUBSTRING (GM.gm_name, 0, Len (GM.gm_name) - 7)
                                                                              ELSE  GM.gm_name
                                                                END ) as game_name,

                                                      (     CASE  
                                                                              WHEN GM.gm_name LIKE '%!%!]' ESCAPE '!' THEN 
                                                                                       --REPLACE( SUBSTRING ( GM.gm_name ,Len(GM.gm_name) - 6 , 5 ), '.',',')
                                                                                       --SUBSTRING ( GM.gm_name ,Len(GM.gm_name) - 6 , 5 )
                                                                                       LEFT(SUBSTRING(GM.gm_name, CHARINDEX('[',GM.gm_name)+1, 100),CHARINDEX(']',SUBSTRING(GM.gm_name, CHARINDEX('[',GM.gm_name)+1, 100))-2)
                                                                              ELSE  ''
                                                                END ) as game_payout,
                                              GM.gm_name

                            FROM  terminal_game_translation TGT, 
                                                     terminals TE, 
                                                     games GM

                            WHERE TGT.tgt_terminal_id       = TE.te_terminal_id 
                              AND TGT.tgt_target_game_id    = GM.gm_game_id
                              --AND TGT.tgt_target_game_id    <> @Unknown_id
                            GROUP BY TE.te_prov_id, TGT.tgt_target_game_id, GM.gm_name
                            ORDER BY TE.te_prov_id, TGT.tgt_target_game_id;
                            
                            
            SET @providers_site_nul = 0
            SELECT @providers_site_nul = (
                                      SELECT    COUNT(*)
                                                FROM    PROVIDERS_GAMES_TMP
                                                             LEFT OUTER JOIN PROVIDERS_GAMES
                                      ON    PROVIDERS_GAMES.PG_PV_ID = PROVIDERS_GAMES_TMP.PG_PV_ID
                                     AND    PROVIDERS_GAMES.PG_GAME_NAME = PROVIDERS_GAMES_TMP.PG_GAME_NAME
                                   WHERE    PROVIDERS_GAMES.PG_PV_ID  IS NULL
                                 )

            IF @providers_site_nul > 0
            BEGIN
                  SELECT    *
                FROM    PROVIDERS_GAMES_TMP
              LEFT OUTER JOIN PROVIDERS_GAMES
                  ON    PROVIDERS_GAMES.PG_PV_ID = PROVIDERS_GAMES_TMP.PG_PV_ID
                 AND    PROVIDERS_GAMES.PG_GAME_NAME = PROVIDERS_GAMES_TMP.PG_GAME_NAME
               WHERE    PROVIDERS_GAMES.PG_PV_ID  IS NULL
              PRINT 'NO SE DESCARGARON TODOS LOS JUEGOS DEL SITE!!'
                  RAISERROR('NO SE DESCARGARON TODOS LOS JUEGOS DEL SITE!!', 20,0) with LOG
                  --NO EXISTEN TODOS LOS PROVEEDORES, DAR DE ALTA EN EL CENTRO Y VOLVER A EJECUTAR CUANDO SE DESCARGUEN NUEVAMENTE
            END            
          /* FIN PRIMERO MIRAMOS QUE TODOS ESTEN DESCARGADOS*/
          /* CREAMOS TERMINAL_GAME_TRANSLATION_TMP */
                CREATE TABLE [dbo].[terminal_game_translation_tmp](
                  [tgt_terminal_id] [int] NOT NULL,
                  [tgt_source_game_id] [int] NOT NULL,
                  [tgt_target_game_id] [int] NOT NULL,
                  [tgt_GAME_NAME] [NVARCHAR](50) NULL,
                              [tgt_PAYOUT] [MONEY] NULL,
                              [tgt_PV_ID] [INT] NULL,
                              [tgt_translated_game_id] [INT] NULL,
                              [tgt_payout_idx] [INT] NULL,
                              [tgt_game_payoutZ] [VARCHAR] (max) NULL
                )


                 INSERT INTO [terminal_game_translation_tmp]
                 ( [tgt_PAYOUT], [tgt_terminal_id], [tgt_source_game_id], [tgt_target_game_id], [tgt_GAME_NAME], [tgt_PV_ID], [tgt_game_payoutZ])
                 SELECT    CAST (  game_payoutZ AS MONEY) AS game_payout
                          , [tgt_terminal_id]
                          , [tgt_source_game_id]
                          , [tgt_target_game_id]
                          , game_name
                          , te_prov_id
                          , game_payoutZ
                    FROM  (
                              SELECT   [tgt_terminal_id]
                                     , [tgt_source_game_id]
                                     , [tgt_target_game_id]
                                     , (
                                          CASE
                                              WHEN  GM.gm_name LIKE '%!%!]' ESCAPE '!'  THEN 
                                                    SUBSTRING (GM.gm_name, 0, Len (GM.gm_name) - 7)
                                              ELSE  GM.gm_name
                                              END
                                        ) AS game_name
                                     , CAST (
                                              (
                                                CASE
                                                    WHEN  GM.gm_name LIKE '%!%!]' ESCAPE '!' THEN 
                                                          --CAST (SUBSTRING ( GM.gm_name ,Len(GM.gm_name) - 6 , 5 ) AS VARCHAR)
                                                          CAST (LEFT(SUBSTRING(GM.gm_name, CHARINDEX('[',GM.gm_name)+1, 100),CHARINDEX(']',SUBSTRING(GM.gm_name, CHARINDEX('[',GM.gm_name)+1, 100))-2) AS VARCHAR)
                                                    ELSE  CAST ('0.00' AS VARCHAR )
                                                    END
                                              ) AS VARCHAR (max)) as game_payoutZ
                                     , te.te_prov_id
                                FROM     terminal_game_translation TGT
                                     , terminals TE
                                     , games GM
                               WHERE   TGT.tgt_terminal_id = TE.te_terminal_id 
                                 AND   TGT.tgt_target_game_id    = GM.gm_game_id
                          ) x 


                
                --INSERT INTO [terminal_game_translation_tmp]([tgt_terminal_id], [tgt_source_game_id], [tgt_target_game_id], [tgt_GAME_NAME], [tgt_PAYOUT], [tgt_PV_ID])
                            --SELECT  [tgt_terminal_id],
                            --                   [tgt_source_game_id],
                            --                   [tgt_target_game_id],
                            --                         (     CASE 
                            --                                                  WHEN GM.gm_name LIKE '%!%!]' ESCAPE '!'  THEN 
                            --                                                           SUBSTRING (GM.gm_name, 0, Len (GM.gm_name) - 7)
                            --                                                  ELSE  GM.gm_name
                            --                                    END ) as game_name,

                            --                         (     CASE  
                            --                                                  WHEN GM.gm_name LIKE '%!%!]' ESCAPE '!' THEN 
                            --                                                           --REPLACE( SUBSTRING ( GM.gm_name ,Len(GM.gm_name) - 6 , 5 ), '.',',')
                            --                                                            SUBSTRING ( GM.gm_name ,Len(GM.gm_name) - 6 , 5 )
                            --                                                  ELSE  ''
                            --                                    END ) as game_payout,
                            --                                 te.te_prov_id

                            --FROM  terminal_game_translation TGT, 
                            --                         terminals TE, 
                            --                         games GM
                            --WHERE TGT.tgt_terminal_id       = TE.te_terminal_id 
                            --  AND TGT.tgt_target_game_id    = GM.gm_game_id
                            
          /* FIN CREAMOS TERMINAL_GAME_TRANSLATION_TMP*/
          /* UPDATE TERMINAL_GAME_TRANSLATION_TMP*/
              UPDATE terminal_game_translation_tmp 
          set tgt_translated_game_id = ( select PG_GAME_ID 
                                            from PROVIDERS_GAMES
                                            where PROVIDERS_GAMES.pg_game_name = tgt_game_name 
                                            and PROVIDERS_GAMES.pg_pv_id = tgt_pv_id )
          where tgt_translated_game_id is NULL

          --PAYOUT 1--
          UPDATE terminal_game_translation_tmp 
          set tgt_payout_idx = (
                 SELECT pg_payout_1 FROM providers_games WHERE pg_payout_1 = tgt_payout AND pg_pv_id = tgt_pv_id AND pg_game_id = tgt_translated_game_id
            )
          where tgt_translated_game_id IS NOT NULL AND tgt_payout_idx IS NULL

          UPDATE terminal_game_translation_tmp SET tgt_payout_idx = 1 WHERE tgt_payout_idx > 10

          --PAYOUT 2--
          UPDATE terminal_game_translation_tmp 
          set tgt_payout_idx = (
                 SELECT pg_payout_2 FROM providers_games WHERE pg_payout_2 = tgt_payout AND pg_pv_id = tgt_pv_id AND pg_game_id = tgt_translated_game_id
            )
          where tgt_translated_game_id IS NOT NULL AND tgt_payout_idx IS NULL

          UPDATE terminal_game_translation_tmp SET tgt_payout_idx = 2 WHERE tgt_payout_idx > 10

          --PAYOUT 3--
          UPDATE terminal_game_translation_tmp 
          set tgt_payout_idx = (
                 SELECT pg_payout_3 FROM providers_games WHERE pg_payout_3 = tgt_payout AND pg_pv_id = tgt_pv_id AND pg_game_id = tgt_translated_game_id
            )
          where tgt_translated_game_id IS NOT NULL AND tgt_payout_idx IS NULL

          UPDATE terminal_game_translation_tmp SET tgt_payout_idx = 3 WHERE tgt_payout_idx > 10

          --PAYOUT 4--
          UPDATE terminal_game_translation_tmp 
          set tgt_payout_idx = (
                 SELECT pg_payout_4 FROM providers_games WHERE pg_payout_4 = tgt_payout AND pg_pv_id = tgt_pv_id AND pg_game_id = tgt_translated_game_id
            )
          where tgt_translated_game_id IS NOT NULL AND tgt_payout_idx IS NULL

          UPDATE terminal_game_translation_tmp SET tgt_payout_idx = 4 WHERE tgt_payout_idx > 10

          --PAYOUT 5--
          UPDATE terminal_game_translation_tmp 
          set tgt_payout_idx = (
                 SELECT pg_payout_5 FROM providers_games WHERE pg_payout_5 = tgt_payout AND pg_pv_id = tgt_pv_id AND pg_game_id = tgt_translated_game_id
            )
          where tgt_translated_game_id IS NOT NULL AND tgt_payout_idx IS NULL

          UPDATE terminal_game_translation_tmp SET tgt_payout_idx = 5 WHERE tgt_payout_idx > 10
    /* FIN UPDATE TERMINAL_GAME_TRANSLATION_TMP*/
    
    /*UPDATE FINAL*/
          UPDATE   terminal_game_translation
             SET   tgt_translated_game_id = tmp.tgt_translated_game_id
                 , tgt_payout_idx =tmp.tgt_payout_idx
            FROM   terminal_game_translation orig
      INNER JOIN   terminal_game_translation_tmp tmp
              ON   tmp.tgt_terminal_id = orig.tgt_terminal_id
             AND   tmp.tgt_source_game_id = orig.tgt_source_game_id
             AND   tmp.tgt_target_game_id = orig.tgt_target_game_id

    /*FIN UPDATE FINAL*/
  /*FIN PARSEAMOS TODOS LOS JUEGOS PARA BUSCAR SUS IGUALES*/
  
  /*LLEGADOS AQUI, PODEMOS BORRAR LAS TABLAS TEMPORALES*/  
  
  IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[providers_bck]') AND type in (N'U'))
    EXECUTE sp_rename N'dbo.providers_bck', N'providers_bck_final', 'OBJECT'

  IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROVIDERS_GAMES_FINALLY]') AND type in (N'U'))
    DROP TABLE [dbo].[PROVIDERS_GAMES_FINALLY]
    
  IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROVIDERS_GAMES_TMP]') AND type in (N'U'))
    DROP TABLE [dbo].[PROVIDERS_GAMES_TMP]
    
  IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[terminal_game_translation_tmp]') AND type in (N'U'))
    DROP TABLE [dbo].[terminal_game_translation_tmp]
    
  /*FIN LLEGADOS AQUI, PODEMOS BORRAR LAS TABLAS TEMPORALES*/
  END


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[providers_games_new]') AND type in (N'U'))
  DROP TABLE [dbo].[providers_games_new]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROVIDERS_GAMES_TMP_REPEATED]') AND type in (N'U'))
  DROP TABLE [dbo].[PROVIDERS_GAMES_TMP_REPEATED]
GO