/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 50;

SET @New_ReleaseId = 51;
SET @New_ScriptName = N'UpdateTo_18.051.010.sql';
SET @New_Description = N'New index IX_hp_type_jackpot_acc_datetime for handpays.';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/
/* Providers */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[providers]') and name = 'pv_site_jackpot')
  ALTER TABLE [dbo].[providers]
          ADD [pv_site_jackpot] [bit] NOT NULL CONSTRAINT [DF_providers_pv_site_jackpot]  DEFAULT ((1))
ELSE
  SELECT '***** Field providers.pv_site_jackpot already exists *****';
GO


/****** INDEXES ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[handpays]') AND name = N'IX_hp_type_jackpot_acc_datetime')
  CREATE NONCLUSTERED INDEX [IX_hp_type_jackpot_acc_datetime] ON [dbo].[handpays] 
  (
    [hp_type] ASC,
    [hp_site_jackpot_awarded_to_account_id] ASC,
    [hp_datetime] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
ELSE
  SELECT '***** Index handpays.IX_hp_type_jackpot_acc_datetime already exists *****';

GO

IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[cashier_movements]') AND name = N'IX_cm_session_id')
  CREATE NONCLUSTERED INDEX [IX_cm_session_id] ON [dbo].[cashier_movements] 
  (
    [cm_session_id] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
ELSE
  SELECT '***** Index cashier_movements.IX_cm_session_id already exists *****';

GO


/****** RECORDS ******/

/****** STORED PROCEDURES ******/
ALTER PROCEDURE [dbo].[SiteJackpot_AccumulatePlayed]
  @PlaySessionId          bigint
, @PlayedTotal            money
, @PlayedRedeemable       money
AS
BEGIN
  DECLARE @site_jackpot   bit

  -- 
  -- AJQ 20-JAN-2012, Provider's points multiplier
  -- Get the "Provider" multiplier
  --
  SET @site_jackpot = ISNULL ( ( SELECT PV_SITE_JACKPOT FROM PROVIDERS     WHERE PV_ID = (
                                 SELECT TE_PROV_ID      FROM TERMINALS     WHERE TE_TERMINAL_ID = ( 
                                 SELECT PS_TERMINAL_ID  FROM PLAY_SESSIONS WHERE PS_PLAY_SESSION_ID = @PlaySessionId ) ) ), 0 )
  IF ( @site_jackpot = 1 )
  BEGIN
    --
    -- Update Site Jackpot Played amounts
    --
    UPDATE SITE_JACKPOT_PARAMETERS
       SET SJP_PLAYED = SJP_PLAYED + CASE WHEN (SJP_ONLY_REDEEMABLE = 1) THEN @PlayedRedeemable ELSE @PlayedTotal END
     WHERE SJP_ENABLED = 1 
  END

END -- SiteJackpot_AccumulatePlayed
GO

ALTER PROCEDURE [dbo].[PT_PlaySessionFinished]
  @PlaySessionId      bigint
AS
BEGIN

      -- Only for test
  DECLARE @status_code                       int
  DECLARE @status_text                       nvarchar (500)
  -- Only for test
  
  DECLARE @terminal_id                       int
  DECLARE @stand_alone                       bit
  DECLARE @account_id                        bigint
  DECLARE @total_cash_in                     money
  DECLARE @played_amount                     money
  DECLARE @won_amount                        money
  DECLARE @total_cash_out                    money
  DECLARE @initial_non_redeemable0           money
  DECLARE @initial_non_redeemable1           money
  DECLARE @non_redeemable2_0                 money
  DECLARE @non_redeemable2_1                 money
  DECLARE @initial_cash_in                   money
  DECLARE @prize_lock                        money
  DECLARE @account_balance                   money
  DECLARE @holder_name                       nvarchar (50)
  DECLARE @holder_level                      int
  
  DECLARE @non_redeemable_cash_in            money
  DECLARE @redeemable_cash_in                money
  DECLARE @non_redeemable_cash_out           money
  DECLARE @redeemable_cash_out               money
  DECLARE @part_nr1_out                      money
  DECLARE @part_nr2_out                      money
  DECLARE @part_refund_out                   money
  DECLARE @non_redeemable_played             money
  DECLARE @redeemable_played                 money
  DECLARE @non_redeemable_won                money
  DECLARE @redeemable_won                    money

  DECLARE @spent_no_redeemable               money
  DECLARE @spent_redeemable                  money

  DECLARE @profit_no_redeemable              money
  DECLARE @profit_redeemable                 money
  
  DECLARE @total_sum_redimible               numeric (20,6)
  DECLARE @total_sum                         numeric (20,6)
  DECLARE @percent_redeemable                numeric (8,6)

  DECLARE @rest_played_and_no_spent          money
  DECLARE @rest_won_and_no_profit            money
  
  DECLARE @total_played_to_1_point           numeric (20,6)
  DECLARE @redeemable_played_to_1_point      numeric (20,6)
  DECLARE @redeemable_spent_to_1_point       numeric (20,6)

  DECLARE @points_balance_before             money
  DECLARE @points_balance_after              money
  DECLARE @points_total_played               money
  DECLARE @points_played_redeemable          money
  DECLARE @points_spent_redeemable           money
  DECLARE @won_points                        money
  DECLARE @points_multiplier                 money
  DECLARE @max_allowed_acc_balance           numeric (20,6)

  DECLARE @cash_in_while_playing             money
  DECLARE @session_cash_in                   money
  DECLARE @total_balance                     money
  DECLARE @redeemable_from_cash_in           money


  -- SET NOCOUNT ON added to prevent extra result sets from
  -- interfering with SELECT statements.
  SET NOCOUNT ON;
      
  SET @status_code  = 0
  SET @status_text  = 'PT_PlaySessionFinished: Init'

  SET @points_total_played      = 0
  SET @points_played_redeemable = 0
  SET @points_spent_redeemable  = 0
  SET @won_points               = 0  
  SET @points_balance_before    = 0
  SET @points_balance_after     = 0


  IF ( @PlaySessionId = 0 ) 
  BEGIN
    SET @status_text = 'PT_PlaySessionFinished: Invalid PlaySessionId'
    GOTO ERROR_PROCEDURE
  END

  --
  -- Read Session & Account & PlayerTracking Data
  --
  EXECUTE dbo.PT_ReadData @PlaySessionId, @terminal_id OUTPUT, @stand_alone OUTPUT, @account_id OUTPUT,
                          @total_cash_in OUTPUT, @session_cash_in OUTPUT, @played_amount OUTPUT, @won_amount OUTPUT, @total_cash_out OUTPUT, 
                          @initial_non_redeemable0 OUTPUT, @non_redeemable2_0 OUTPUT, @initial_cash_in OUTPUT, @prize_lock OUTPUT, @account_balance  OUTPUT, 
                          @points_balance_before OUTPUT, @holder_name OUTPUT, @holder_level OUTPUT, @max_allowed_acc_balance OUTPUT,
                          @status_code OUTPUT, @status_text OUTPUT

  IF ( @status_code <> 0 )
    GOTO ERROR_PROCEDURE

  SET @initial_non_redeemable1 = @initial_non_redeemable0
  SET @non_redeemable2_1       = @non_redeemable2_0

  --
  -- AJQ 01-FEB-2012: Re-calculate the non-redeemable amount after a cash-in-while-playing.
  --
  IF ( @stand_alone = 1 AND @initial_non_redeemable0 > 0 AND ISNULL (@prize_lock, 0) = 0 )
  BEGIN
    --
    -- SAS HOST and Account with NR and No PrizeLock
    --
        SET @cash_in_while_playing = @account_balance + @session_cash_in

    IF ( @cash_in_while_playing > 0 )
    BEGIN
      --
          -- Player added money while playing (already on the session and/or in the account)
          --
      SET @total_balance = @total_cash_out + @account_balance

      -- Compute "money while playing" part
      SET @redeemable_from_cash_in = @cash_in_while_playing
      IF ( @redeemable_from_cash_in > @total_balance )
        SET @redeemable_from_cash_in = @total_balance

      -- Remainig Balance without the "money while playing" part
      SET @total_balance = @total_balance - @redeemable_from_cash_in

      --
      -- Split the "remaining" balance
      --
      EXECUTE dbo.PT_BalancePartsWhenPlaying @total_balance, @initial_cash_in, @initial_non_redeemable0, @non_redeemable2_0, @prize_lock, 
                                             @non_redeemable_cash_out OUTPUT, @redeemable_cash_out OUTPUT,
                                             @part_nr1_out OUTPUT, @part_nr2_out OUTPUT, @part_refund_out OUTPUT

      --
      -- Update the Initial Non Redeemable
      -- 
      SET @initial_non_redeemable1 = @part_nr1_out

      UPDATE   ACCOUNTS
         SET   AC_INITIAL_NOT_REDEEMABLE = @initial_non_redeemable1
       WHERE   AC_ACCOUNT_ID             = @account_id
      
    END
  END
    
  --
  -- Compute PlayedCredits, PlayedRedeemableCredits, SpentRedeemableCredits
  --
  EXECUTE dbo.PT_BalancePartsWhenPlaying @total_cash_in, @initial_cash_in, @initial_non_redeemable0, @non_redeemable2_0, @prize_lock, 
                                         @non_redeemable_cash_in OUTPUT, @redeemable_cash_in OUTPUT,
                                        @part_nr1_out OUTPUT, @part_nr2_out OUTPUT, @part_refund_out OUTPUT  -- not used in the IN part
  --
  -- If played greater than the refundable part, then recalculate NR2.
  --
  IF @played_amount > @part_refund_out
  BEGIN
    SET @non_redeemable2_1 = @non_redeemable2_0 - (@played_amount - @part_refund_out)
    SET @non_redeemable2_1 = CASE WHEN ( @non_redeemable2_1 < 0 ) THEN 0 ELSE @non_redeemable2_1 END
    
    UPDATE   ACCOUNTS
       SET   AC_NOT_REDEEMABLE = @non_redeemable2_1
     WHERE   AC_ACCOUNT_ID     = @account_id
  END

  EXECUTE dbo.PT_BalancePartsWhenPlaying @total_cash_out, @initial_cash_in, @initial_non_redeemable1, @non_redeemable2_1, @prize_lock, 
                                         @non_redeemable_cash_out OUTPUT, @redeemable_cash_out OUTPUT,
                                         @part_nr1_out OUTPUT, @part_nr2_out OUTPUT, @part_refund_out OUTPUT

  SET @spent_no_redeemable      = dbo.Maximum_Money (0, @non_redeemable_cash_in - @non_redeemable_cash_out)
  SET @spent_redeemable         = dbo.Maximum_Money (0, @redeemable_cash_in - @redeemable_cash_out)

  SET @profit_no_redeemable     = dbo.Maximum_Money (0, @non_redeemable_cash_out - @non_redeemable_cash_in)
  SET @profit_redeemable        = dbo.Maximum_Money (0, @redeemable_cash_out - @redeemable_cash_in)

  SET @total_sum_redimible      = @redeemable_cash_in + @redeemable_cash_out 
  SET @total_sum                = @non_redeemable_cash_in + @non_redeemable_cash_out + @redeemable_cash_in + @redeemable_cash_out
  
  IF @total_sum = 0 GOTO ERROR_PROCEDURE   
  SET @percent_redeemable       = @total_sum_redimible / @total_sum

  SET @rest_played_and_no_spent = @played_amount - (@spent_no_redeemable + @spent_redeemable)
  SET @redeemable_played        = ROUND(@spent_redeemable + (@rest_played_and_no_spent * @percent_redeemable), 4)
  SET @non_redeemable_played    = @played_amount - @redeemable_played

  SET @rest_won_and_no_profit   = @won_amount - (@profit_no_redeemable + @profit_redeemable)
  SET @redeemable_won           = ROUND(@profit_redeemable + (@rest_won_and_no_profit * @percent_redeemable), 4)
  SET @non_redeemable_won       = @won_amount - @redeemable_won

  --
  -- Update PlaySession table (Redeemable & NonRedeemable amounts)
  --
  UPDATE PLAY_SESSIONS
     SET PS_NON_REDEEMABLE_CASH_IN    = @non_redeemable_cash_in
       , PS_NON_REDEEMABLE_CASH_OUT   = @non_redeemable_cash_out
       , PS_NON_REDEEMABLE_PLAYED     = @non_redeemable_played
       , PS_NON_REDEEMABLE_WON        = @non_redeemable_won
       , PS_REDEEMABLE_CASH_IN        = @redeemable_cash_in
       , PS_REDEEMABLE_CASH_OUT       = @redeemable_cash_out
       , PS_REDEEMABLE_PLAYED         = @redeemable_played
       , PS_REDEEMABLE_WON            = @redeemable_won
   WHERE PS_PLAY_SESSION_ID = @PlaySessionId 

  --
  -- RCI & ACC & AJQ 30/09/2010: Reset the Cancellable Operation when PlayedAmount greater than 0.
  --
  UPDATE   ACCOUNTS
     SET   AC_CANCELLABLE_OPERATION_ID = CASE WHEN (@played_amount > 0) THEN NULL ELSE AC_CANCELLABLE_OPERATION_ID END
   WHERE   AC_ACCOUNT_ID               = @account_id  

  --
  -- ACC 22/10/2010: Check maximum allowed account balance
  --
  IF ( @max_allowed_acc_balance > 0 AND @total_cash_out > @max_allowed_acc_balance )
  BEGIN
    UPDATE ACCOUNTS
       SET AC_BLOCKED       = 1
         , AC_BLOCK_REASON  = 2  -- MaxBalance
     WHERE AC_ACCOUNT_ID    = @account_id  
  END

  --- Anonymous accounts have Level=0
  IF ( @holder_level = 0 )
    GOTO EXIT_PROCEDURE

  --
  -- Read PlayerTracking Point Conversion Factors according to the holder's level
  --
  EXECUTE dbo.PT_ReadPointFactors @holder_level, 
                                  @redeemable_spent_to_1_point  OUTPUT,
                                  @redeemable_played_to_1_point OUTPUT,
                                  @total_played_to_1_point      OUTPUT,
                                  @status_code                  OUTPUT, 
                                  @status_text                  OUTPUT

  IF ( @status_code <> 0 )
    GOTO ERROR_PROCEDURE

  --
  -- Compute Points (it is possible to accummulate points per each type of credits at the same time)
  --  
  IF @redeemable_spent_to_1_point  > 0 SET @points_spent_redeemable  = @spent_redeemable  / @redeemable_spent_to_1_point
  IF @redeemable_played_to_1_point > 0 SET @points_played_redeemable = @redeemable_played / @redeemable_played_to_1_point
  IF @total_played_to_1_point      > 0 SET @points_total_played      = @played_amount     / @total_played_to_1_point
  
  SET @won_points = ROUND(@points_total_played + @points_played_redeemable + @points_spent_redeemable, 4)

  -- 
  -- AJQ 20-JAN-2012, Provider's points multiplier
  -- Get the "Provider" multiplier
  --
  SET @points_multiplier = ISNULL ( ( SELECT PV_POINTS_MULTIPLIER FROM PROVIDERS     WHERE PV_ID = (
                                      SELECT TE_PROV_ID           FROM TERMINALS     WHERE TE_TERMINAL_ID = ( 
                                      SELECT PS_TERMINAL_ID       FROM PLAY_SESSIONS WHERE PS_PLAY_SESSION_ID = @PlaySessionId ) ) ), 0 )
  IF ( @points_multiplier <= 0 )
    GOTO EXIT_PROCEDURE

  -- Apply multiplier
  SET @won_points = ROUND (@won_points * @points_multiplier, 4)

  --
  -- Accumulate points
  -- 
  UPDATE   ACCOUNTS
     SET   AC_POINTS                   = AC_POINTS  + @won_points
   WHERE   AC_ACCOUNT_ID               = @account_id  
  
  --  
  -- Create CardMovement.ObtainedPoints
  --
  -- PointsAwarded   = 36,
  SET @points_balance_after     = @points_balance_before + @won_points
  
  EXECUTE dbo.InsertMovement @PlaySessionId, @account_id, @terminal_id, 36, @points_balance_before, 0, @won_points, @points_balance_after

EXIT_PROCEDURE:

  --
  -- ACC 25/11/2010 Accumulate played amount into site jackpot
  --
  EXECUTE dbo.SiteJackpot_AccumulatePlayed @PlaySessionId, @played_amount, @redeemable_played

  SET @status_code  = 0
  SET @status_text  = 'PT_PlaySessionFinished: Successful points accumulated.' 
                    + ' *** '
                    + ' Points.PerRedeemableSpent  = ' + CAST (@points_spent_redeemable AS nvarchar)  + ' (' + CAST (@spent_redeemable AS nvarchar)  + ' credits)'
                    + ' Points.PerRedeemablePlayed = ' + CAST (@points_played_redeemable AS nvarchar) + ' (' + CAST (@redeemable_played AS nvarchar) + ' credits)'
                    + ' Points.PerTotalPlayed      = ' + CAST (@points_total_played AS nvarchar)      + ' (' + CAST (@played_amount AS nvarchar)     + ' credits)'
                    + ' Points.TotalAwarded        = ' + CAST (@won_points AS nvarchar) 
                    + ' *** '

ERROR_PROCEDURE:
--
-- ACC 18-OCT-2010 DO NOT SELECT (3GS not works with this select)
--
--  SELECT @status_text AS StatusText

END -- PT_PlaySessionFinished

GO


/**************************************** 3GS Section ****************************************/

