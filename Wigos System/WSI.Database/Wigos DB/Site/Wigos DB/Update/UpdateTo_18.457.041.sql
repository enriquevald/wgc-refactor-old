﻿/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 456;

SET @New_ReleaseId = 457;

SET @New_ScriptName = N'2018-06-26 - UpdateTo_18.457.041.sql';
SET @New_Description = N'New release v03.008.S2S.0001';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO


/*********************************************************************************************************/

/**** GENERAL PARAM *****/
-- TITO CashableTickets.Title.Jackpot
IF NOT EXISTS (
	SELECT * 
	FROM GENERAL_PARAMS 
	WHERE GP_GROUP_KEY = 'TITO' 
		AND GP_SUBJECT_KEY ='CashableTickets.Title.Jackpot')
BEGIN
	INSERT INTO [dbo].[general_params] (
		[gp_group_key]
		, [gp_subject_key]
		, [gp_key_value])
	VALUES (
		'TITO'
		, 'CashableTickets.Title.Jackpot'
		, 'JACKPOT TICKET');
END
ELSE
BEGIN
	SELECT '***** Record TITO CashableTickets.Title.Jackpot exists *****';
END

-- GSA S2S.Enable
IF NOT EXISTS (
	SELECT * 
	FROM GENERAL_PARAMS 
	WHERE GP_GROUP_KEY = 'GSA' 
		AND GP_SUBJECT_KEY ='S2S.Enable')
BEGIN
	INSERT INTO [dbo].[general_params] (
		[gp_group_key]
		, [gp_subject_key]
		, [gp_key_value])
	VALUES (
		'GSA'
		, 'S2S.Enable'
		, '0');
END
ELSE
BEGIN
	SELECT '***** Record GSA S2S.Enable already exists *****';
END

-- GSA S2S.Ip
IF NOT EXISTS (
	SELECT * 
	FROM GENERAL_PARAMS 
	WHERE GP_GROUP_KEY = 'GSA' 
		AND GP_SUBJECT_KEY ='S2S.Ip')
BEGIN
	INSERT INTO [dbo].[general_params] (
		[gp_group_key]
		, [gp_subject_key]
		, [gp_key_value])
	VALUES (
		'GSA'
		, 'S2S.Ip'
		, 'http://localhost');
END
ELSE
BEGIN
	SELECT '***** Record GSA S2S.Ip already exists *****';
END

-- GSA S2S.Port
IF NOT EXISTS (
	SELECT * 
	FROM GENERAL_PARAMS 
	WHERE GP_GROUP_KEY = 'GSA' 
		AND GP_SUBJECT_KEY ='S2S.Port')
BEGIN
	INSERT INTO [dbo].[general_params] (
		[gp_group_key]
		, [gp_subject_key]
		, [gp_key_value])
	VALUES (
		'GSA'
		, 'S2S.Port'
		, '8890');
END
ELSE
BEGIN
	SELECT '***** Record GSA S2S.Port already exists *****';
END

-- GSA S2S.KeepAliveCommandInterval
IF NOT EXISTS (
	SELECT * 
	FROM GENERAL_PARAMS 
	WHERE GP_GROUP_KEY = 'GSA' 
		AND GP_SUBJECT_KEY ='S2S.KeepAliveCommandInterval')
BEGIN
	INSERT INTO [dbo].[general_params] (
		[gp_group_key]
		, [gp_subject_key]
		, [gp_key_value])
	VALUES (
		'GSA'
		, 'S2S.KeepAliveCommandInterval'
		, 60000);
END
ELSE
BEGIN
	SELECT '***** Record GSA S2S.KeepAliveCommandInterval already exists *****';
END

-- GSA S2S.ValidationIdListSize
IF NOT EXISTS (
	SELECT * 
	FROM GENERAL_PARAMS 
	WHERE GP_GROUP_KEY = 'GSA' 
		AND GP_SUBJECT_KEY ='S2S.ValidationIdListSize')
BEGIN
	INSERT INTO [dbo].[general_params] (
		[gp_group_key]
		, [gp_subject_key]
		, [gp_key_value])
	VALUES (
		'GSA'
		, 'S2S.ValidationIdListSize'
		, 3);
END
ELSE
BEGIN
	SELECT '***** Record GSA S2S.ValidationIdListSize already exists *****';
END

-- GSA S2S.SendRetryInterval
IF NOT EXISTS (
	SELECT * 
	FROM GENERAL_PARAMS 
	WHERE GP_GROUP_KEY = 'GSA' 
		AND GP_SUBJECT_KEY ='S2S.SendRetryInterval')
BEGIN
	INSERT INTO [dbo].[general_params] (
		[gp_group_key]
		, [gp_subject_key]
		, [gp_key_value])
	VALUES (
		'GSA'
		, 'S2S.SendRetryInterval'
		, 30000);
END
ELSE
BEGIN
	SELECT '***** Record GSA S2S.SendRetryInterval already exists *****';
END

-- GSA S2S.ClientConnectionTimeout
IF NOT EXISTS (
	SELECT * 
	FROM GENERAL_PARAMS 
	WHERE GP_GROUP_KEY = 'GSA' 
		AND GP_SUBJECT_KEY ='S2S.ClientConnectionTimeout')
BEGIN
	INSERT INTO [dbo].[general_params] (
		[gp_group_key]
		, [gp_subject_key]
		, [gp_key_value])
	VALUES (
		'GSA'
		, 'S2S.ClientConnectionTimeout'
		, 30000);
END
ELSE
BEGIN
	SELECT '***** Record GSA S2S.ClientConnectionTimeout already exists *****';
END

-- GSA S2S.ConfigurationIdAutoIncreaseable
IF NOT EXISTS (
	SELECT * 
	FROM GENERAL_PARAMS 
	WHERE GP_GROUP_KEY = 'GSA' 
		AND GP_SUBJECT_KEY ='S2S.ConfigurationIdAutoIncreaseable')
BEGIN
	INSERT INTO [dbo].[general_params] (
		[gp_group_key]
		, [gp_subject_key]
		, [gp_key_value])
	VALUES (
		'GSA'
		, 'S2S.ConfigurationIdAutoIncreaseable'
		, 0);
END
ELSE
BEGIN
	SELECT '***** Record GSA S2S.ConfigurationIdAutoIncreaseable already exists *****';
END

-- GSA S2S.OffsetMinutesToExpireSeedValues
IF NOT EXISTS (
	SELECT * 
	FROM GENERAL_PARAMS 
	WHERE GP_GROUP_KEY = 'GSA' 
		AND GP_SUBJECT_KEY ='S2S.OffsetMinutesToExpireSeedValues')
BEGIN
	INSERT INTO [dbo].[general_params] (
		[gp_group_key]
		, [gp_subject_key]
		, [gp_key_value])
	VALUES (
		'GSA'
		, 'S2S.OffsetMinutesToExpireSeedValues'
		, 60);
END
ELSE
BEGIN
	SELECT '***** Record GSA S2S.OffsetMinutesToExpireSeedValues already exists *****';
END

-- USA-Intuicode / USA-Red Mile
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Account' AND GP_SUBJECT_KEY = 'CreateOnCardIn')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key]
             ,[gp_subject_key]
             ,[gp_key_value])
       VALUES
             ('Account'
             ,'CreateOnCardIn'
             ,'0');
GO

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Site' AND GP_SUBJECT_KEY = 'AnyCard.Enabled')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key]
             ,[gp_subject_key]
             ,[gp_key_value])
       VALUES
             ('Site'
             ,'AnyCard.Enabled'
             ,'0');
GO

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Site' AND GP_SUBJECT_KEY = 'AnyCard.RegEx')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key]
             ,[gp_subject_key]
             ,[gp_key_value])
       VALUES
             ('Site'
             ,'AnyCard.RegEx'
             ,'');
GO

/**** VIEW *****/


/******* TABLES  *******/


/******* INDEXES *******/


/******* TRIGERS *******/


/******* RECORDS *******/

/************************************* begin ********************************/

/************************************* end   ********************************/


/******* PROCEDURES *******/


/******* FUNCTIONS *******/
-- USA-Intuicode / USA-Red Mile
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TrackDataToExternal]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
  DROP FUNCTION [dbo].[TrackDataToExternal]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TrackDataToExternal_DLL]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
  DROP FUNCTION [dbo].[TrackDataToExternal_DLL]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TrackDataToInternal]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
  DROP FUNCTION [dbo].[TrackDataToInternal]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TrackDataToInternal_DLL]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
  DROP FUNCTION [dbo].[TrackDataToInternal_DLL]
GO

CREATE FUNCTION TrackDataToExternal_DLL(@Input nvarchar(255)) RETURNS nvarchar(255) 
       EXTERNAL NAME SQLUtilities.TrackData.ToExternal
GO


CREATE FUNCTION TrackDataToInternal_DLL(@Input nvarchar(255)) RETURNS nvarchar(255) 
       EXTERNAL NAME SQLUtilities.TrackData.ToInternal
GO

CREATE FUNCTION TrackDataToInternal
  (@TrackData nvarchar(50))
RETURNS nvarchar(50)
AS
BEGIN
         declare @_mode as nvarchar(50);

         select @_mode =  gp_key_value
           from general_params
          where gp_group_key ='Site'
            and gp_subject_key = 'AnyCard.Enabled';

         if @_mode = 1 return  @TrackData
  RETURN dbo.TrackDataToInternal_DLL(@TrackData);
END -- TrackDataToInternal
GO

CREATE FUNCTION TrackDataToExternal
  (@TrackData nvarchar(50))
RETURNS nvarchar(50)
AS
BEGIN
         declare @_mode as nvarchar(50);

         select @_mode =  gp_key_value
           from general_params
          where gp_group_key ='Site'
            and gp_subject_key = 'AnyCard.Enabled';

         if @_mode = 1 return  @TrackData
  RETURN dbo.TrackDataToExternal_DLL(@TrackData);
END -- TrackDataToExternal
GO

GRANT EXECUTE ON dbo.TrackDataToExternal TO wggui;
GRANT EXECUTE ON dbo.TrackDataToInternal TO wggui;

GRANT EXECUTE ON dbo.TrackDataToExternal_DLL TO wggui;
GRANT EXECUTE ON dbo.TrackDataToInternal_DLL TO wggui;


/******* TRIGGERS *******/