/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 132;

SET @New_ReleaseId = 133;
SET @New_ScriptName = N'UpdateTo_18.133.027.sql';
SET @New_Description = N'Main Update: MoneyLaundering';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END
 
/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/

/* GUI_USERS */
ALTER TABLE  gui_users ALTER COLUMN gu_full_name  nvarchar(50) NULL
GO

/* MASTER PROFILE */ 
ALTER TABLE dbo.gui_user_profiles ADD gup_master_id int NULL
GO
ALTER TABLE dbo.gui_user_profiles ADD gup_master_secuence_id bigint NULL
GO

ALTER TABLE   dbo.terminals 
        ADD   te_contract_type nvarchar(50) NULL,
              te_contract_id   nvarchar(50) NULL,
              te_order_number  nvarchar(50) NULL
GO

--
-- Created for MoneyLaundering
--

CREATE TABLE [dbo].[account_documents](
	[ad_account_id] [bigint] NOT NULL,
	[ad_created] [datetime] NOT NULL CONSTRAINT [DF_account_documents_created]  DEFAULT (getdate()),
	[ad_modified] [datetime] NOT NULL CONSTRAINT [DF_account_documents_modified]  DEFAULT (getdate()),
	[ad_data] [varbinary](max) NULL,
  [ad_ms_sequence_id] [bigint] NULL,
 CONSTRAINT [PK_account_documents] PRIMARY KEY CLUSTERED 
(
	[ad_account_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[ms_site_pending_account_documents](
	[pad_account_id] [bigint] NOT NULL,
 CONSTRAINT [PK_ms_pending_accounts_documents] PRIMARY KEY CLUSTERED 
(
	[pad_account_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE dbo.accounts ADD
	ac_holder_occupation nvarchar(50) NULL,
	ac_holder_ext_num nvarchar(10) NULL,
	ac_holder_nationality Int NULL,
	ac_holder_birth_country Int NULL,
	ac_holder_fed_entity Int NULL,
	ac_holder_id1_type int NULL CONSTRAINT [DF_ac_holder_id1_type] DEFAULT ((1)), -- RFC
	ac_holder_id2_type int NULL CONSTRAINT [DF_ac_holder_id2_type] DEFAULT ((2)), -- CURP
	ac_holder_id3_type int NULL,
	ac_holder_id3 nvarchar(20) NULL,
	ac_holder_as_beneficiary bit NULL CONSTRAINT [DF_ac_holder_as_beneficiary] DEFAULT ((1)),
	ac_beneficiary_name nvarchar(200) NULL,
	ac_beneficiary_name1 nvarchar(50) NULL,
	ac_beneficiary_name2 nvarchar(50) NULL,
	ac_beneficiary_name3 nvarchar(50) NULL,
	ac_beneficiary_birth_date Datetime NULL,
	ac_beneficiary_gender int NULL,
	ac_beneficiary_occupation nvarchar(50) NULL,
	ac_beneficiary_id1_type int NULL CONSTRAINT [DF_ac_beneficiary_id1_type] DEFAULT ((1)), -- RFC
	ac_beneficiary_id1 nvarchar(20) NULL,
	ac_beneficiary_id2_type int NULL CONSTRAINT [DF_ac_beneficiary_id2_type] DEFAULT ((2)), -- CURP
	ac_beneficiary_id2 nvarchar(20) NULL,
	ac_beneficiary_id3_type int NULL,
	ac_beneficiary_id3 nvarchar(20) NULL
GO

CREATE TABLE [dbo].[countries](
       [co_country_id] [int] NOT NULL,
         [co_language_id] [nvarchar](5) NOT NULL,
       [co_name] [nvarchar](50) NOT NULL,      
       [co_adjective] [nvarchar](50) NOT NULL,      
CONSTRAINT [PK_countries] PRIMARY KEY CLUSTERED 
(
       [co_country_id] ASC, 
         [co_language_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[federal_states](
	[fs_state_id] [int] IDENTITY(1,1) NOT NULL,
	[fs_name] [nvarchar](50) NOT NULL,	
 CONSTRAINT [PK_federal_states] PRIMARY KEY CLUSTERED 
(
	[fs_state_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** VIEWS ******/

/****** CONSTRAINTS ******/

/****** FUNCTIONS ******/

/****** INDEXES ******/

/****** STORED PROCEDURES ******/

--------------------------------------------------------------------------------
-- Copyright � 2013 Win Systems International
--------------------------------------------------------------------------------
--
--   MODULE NAME: MultiSiteTriggersEnable
--
--   DESCRIPTION: Enable and disable the triggers of Multisite
--
--        AUTHOR: Dani Dom�nguez and Andreu Juli�
--
-- CREATION DATE: 02-APR-2013
--
-- REVISION HISTORY:
--
-- Date        Author    Description
-- ----------- --------- ----------------------------------------------------------
-- 08-MAR-2013 AJQ & DDM First release.  
-- 04-JUL-2013 JML       Add control of trigger on account_documents
-----------------------------------------------------------------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MultiSiteTriggersEnable]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[MultiSiteTriggersEnable]
GO

CREATE PROCEDURE [dbo].[MultiSiteTriggersEnable] 
@Enable as bit
WITH EXECUTE AS OWNER
AS
BEGIN
	
  SET NOCOUNT ON;
	
  IF @Enable = 1 
  BEGIN
    ENABLE  TRIGGER MultiSiteTrigger_SiteAccountUpdate    ON ACCOUNTS;
    ENABLE  TRIGGER Trigger_SiteToMultiSite_Points        ON ACCOUNT_MOVEMENTS;
    ENABLE  TRIGGER MultiSiteTrigger_SiteAccountDocuments ON ACCOUNT_DOCUMENTS;
  END
  ELSE 
  BEGIN
    DISABLE  TRIGGER MultiSiteTrigger_SiteAccountUpdate    ON ACCOUNTS;
    DISABLE  TRIGGER Trigger_SiteToMultiSite_Points        ON ACCOUNT_MOVEMENTS;
    DISABLE  TRIGGER MultiSiteTrigger_SiteAccountDocuments ON ACCOUNT_DOCUMENTS;
  END
	
END
GO

GRANT EXECUTE ON [dbo].[MultiSiteTriggersEnable] TO [wggui] WITH GRANT OPTION
GO

--------------------------------------------------------------------------------
-- Copyright � 2013 Win Systems International
--------------------------------------------------------------------------------
--
--   MODULE NAME: Update_AccountDocuments.sql
--
--   DESCRIPTION: Update Account Documents in Multisite
--
--        AUTHOR: Jos� Mart�nez L�pez
--
-- CREATION DATE: 02-JUL-2013
--
-- REVISION HISTORY:
--
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 02-JUL-2013 JML    First release.  
-------------------------------------------------------------------------------- 

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Update_AccountDocuments]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Update_AccountDocuments]
GO

CREATE PROCEDURE Update_AccountDocuments
                 @pAccountId BIGINT
               , @pCreated DATETIME
               , @pModified DATETIME
               , @pData VARBINARY(MAX)
AS
BEGIN   
IF EXISTS (SELECT 1 FROM ACCOUNT_DOCUMENTS WHERE AD_ACCOUNT_ID = @pAccountId)
  BEGIN   
	  UPDATE   ACCOUNT_DOCUMENTS
       SET   AD_CREATED        = @pCreated				
           , AD_MODIFIED       = @pModified		  
           , AD_DATA           = @pData  
     WHERE   AD_ACCOUNT_ID     = @pAccountId 
  END
ELSE
  BEGIN   
  
    INSERT INTO   ACCOUNT_DOCUMENTS
                ( AD_ACCOUNT_ID
                , AD_CREATED
                , AD_MODIFIED
                , AD_DATA )
         VALUES ( @pAccountId 
                , @pCreated
                , @pModified
                , @pData )

  END
END -- Update_AccountDocuments
GO
 

--------------------------------------------------------------------------------
-- Copyright � 2013 Win Systems International
--------------------------------------------------------------------------------
--
--   MODULE NAME: Update_PersonalInfo.sql.sql
--
--   DESCRIPTION: Update personal Information 
--
--        AUTHOR: Dani Dom�nguez
--
-- CREATION DATE: 08-MAR-2013
--
-- REVISION HISTORY:
--
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 08-MAR-2013 DDM    First release.
-- 22-MAY-2013 DDM    Fixed bugs 783,793 and 693
--                    Added field AC_BLOCK_DESCRIPTION
-- 28-MAY-2013 DDM    Fixed bug #803
--                    Added field AC_EXTERNAL_REFERENCE
--------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Update_PersonalInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Update_PersonalInfo]
GO
CREATE PROCEDURE [dbo].[Update_PersonalInfo]
  @pAccountId bigint
, @pTrackData nvarchar(50)
,	@pHolderName nvarchar(200)
,	@pHolderId nvarchar(20)
, @pHolderIdType int
, @pHolderAddress01 nvarchar(50)
, @pHolderAddress02 nvarchar(50)
, @pHolderAddress03 nvarchar(50)
, @pHolderCity nvarchar(50)
, @pHolderZip  nvarchar(10) 
, @pHolderEmail01 nvarchar(50)
,	@pHolderEmail02 nvarchar(50)
,	@pHolderTwitter nvarchar(50)
,	@pHolderPhoneNumber01 nvarchar(20)
, @pHolderPhoneNumber02 nvarchar(20)
, @pHolderComments nvarchar(100)
, @pHolderId1 nvarchar(20)
, @pHolderId2 nvarchar(20)
, @pHolderDocumentId1 bigint
, @pHolderDocumentId2 bigint
, @pHolderName1 nvarchar(50)
,	@pHolderName2 nvarchar(50)
,	@pHolderName3 nvarchar(50)
, @pHolderGender  int
, @pHolderMaritalStatus int
, @pHolderBirthDate datetime
, @pHolderWeddingDate datetime
, @pHolderLevel int
, @pHolderLevelNotify int
, @pHolderLevelEntered datetime
,	@pHolderLevelExpiration datetime
,	@pPin nvarchar(12)
, @pPinFailures int
, @pPinLastModified datetime
, @pBlocked bit
, @pActivated bit
, @pBlockReason int
, @pHolderIsVip int
, @pHolderTitle nvarchar(15)                       
, @pHolderName4 nvarchar(50)                       
, @pHolderPhoneType01  int
, @pHolderPhoneType02  int
, @pHolderState    nvarchar(50)                    
, @pHolderCountry  nvarchar(50)                
, @pPersonalInfoSequenceId  bigint                     
, @pUserType int
, @pPointsStatus int
, @pDeposit money
, @pCardPay bit
, @pBlockDescription nvarchar(256) 
, @pMSHash varbinary(20) 
, @pMSCreatedOnSiteId int
, @pCreated datetime
, @pExternalReference nvarchar(50) 
AS
BEGIN

DECLARE @pOtherAccountId as BIGINT

SET @pOtherAccountId = ISNULL ((SELECT AC_ACCOUNT_ID FROM ACCOUNTS WHERE AC_TRACK_DATA = @pTrackData), 0)

SET @pUserType = ISNULL(@pUserType,CASE WHEN ISNULL(@pHolderLevel,0) > 0  THEN 1 ELSE 0 END)

IF @pOtherAccountId <> 0 AND @pOtherAccountId <> @pAccountId
  UPDATE   ACCOUNTS   
     SET   AC_TRACK_DATA =  '-RECYCLED-NEW-' + CAST (NEWID() AS NVARCHAR(50))
   WHERE   AC_ACCOUNT_ID = @pOtherAccountId


IF NOT EXISTS (SELECT 1 FROM ACCOUNTS WHERE ac_account_id = @pAccountId )  
BEGIN
  INSERT INTO   ACCOUNTS 
              ( AC_ACCOUNT_ID, AC_TYPE, AC_BLOCKED, AC_TRACK_DATA) 
       VALUES ( @pAccountId,         2,  @pBlocked, @pTrackData )
END  

 
  UPDATE   ACCOUNTS                   
     SET   AC_TRACK_DATA              = @pTrackData 
         , AC_HOLDER_NAME             = @pHolderName 
         , AC_HOLDER_ID               = @pHolderId 
         , AC_HOLDER_ID_TYPE          = @pHolderIdType 
         , AC_HOLDER_ADDRESS_01       = @pHolderAddress01 
         , AC_HOLDER_ADDRESS_02       = @pHolderAddress02 
         , AC_HOLDER_ADDRESS_03       = @pHolderAddress03 
         , AC_HOLDER_CITY             = @pHolderCity 
         , AC_HOLDER_ZIP              = @pHolderZip  
         , AC_HOLDER_EMAIL_01         = @pHolderEmail01 
         , AC_HOLDER_EMAIL_02         = @pHolderEmail02 
         , AC_HOLDER_TWITTER_ACCOUNT  = @pHolderTwitter 
         , AC_HOLDER_PHONE_NUMBER_01  = @pHolderPhoneNumber01 
         , AC_HOLDER_PHONE_NUMBER_02  = @pHolderPhoneNumber02 
         , AC_HOLDER_COMMENTS         = @pHolderComments 
         , AC_HOLDER_ID1              = @pHolderId1 
         , AC_HOLDER_ID2              = @pHolderId2 
         , AC_HOLDER_DOCUMENT_ID1     = @pHolderDocumentId1 
         , AC_HOLDER_DOCUMENT_ID2     = @pHolderDocumentId2 
         , AC_HOLDER_NAME1            = @pHolderName1 
         , AC_HOLDER_NAME2            = @pHolderName2 
         , AC_HOLDER_NAME3            = @pHolderName3 
         , AC_HOLDER_GENDER           = @pHolderGender  
         , AC_HOLDER_MARITAL_STATUS   = @pHolderMaritalStatus 
         , AC_HOLDER_BIRTH_DATE       = @pHolderBirthDate 
         , AC_HOLDER_WEDDING_DATE     = @pHolderWeddingDate 
         , AC_HOLDER_LEVEL            = @pHolderLevel 
         , AC_HOLDER_LEVEL_NOTIFY     = @pHolderLevelNotify 
         , AC_HOLDER_LEVEL_ENTERED    = @pHolderLevelEntered 
         , AC_HOLDER_LEVEL_EXPIRATION = @pHolderLevelExpiration 
         , AC_PIN                     = @pPin 
         , AC_PIN_FAILURES            = @pPinFailures 
         , AC_PIN_LAST_MODIFIED       = @pPinLastModified 
         , AC_BLOCKED                 = @pBlocked 
         , AC_ACTIVATED               = @pActivated 
         , AC_BLOCK_REASON            = @pBlockReason 
         , AC_HOLDER_IS_VIP           = @pHolderIsVip 
         , AC_HOLDER_TITLE            = @pHolderTitle 
         , AC_HOLDER_NAME4            = @pHolderName4         
         , AC_HOLDER_PHONE_TYPE_01    = @pHolderPhoneType01  
         , AC_HOLDER_PHONE_TYPE_02    = @pHolderPhoneType02  
         , AC_HOLDER_STATE            = @pHolderState    
         , AC_HOLDER_COUNTRY          = @pHolderCountry  					
         , AC_MS_PERSONAL_INFO_SEQ_ID = @pPersonalInfoSequenceId           
         , AC_USER_TYPE				        = @pUserType
         , AC_POINTS_STATUS           = @pPointsStatus
         , AC_MS_CHANGE_GUID          = AC_MS_CHANGE_GUID -- avoid trigger on update
         , AC_DEPOSIT                 = @pDeposit 
         , AC_CARD_PAID               = @pCardPay 
         , AC_BLOCK_DESCRIPTION       = @pBlockDescription                
         , AC_CREATED                 = @pCreated
         , AC_MS_CREATED_ON_SITE_ID   = @pMSCreatedOnSiteId
         , AC_MS_HASH                 = @pMSHash
         , AC_EXTERNAL_REFERENCE      = ISNULL(@pExternalReference,AC_EXTERNAL_REFERENCE)
   WHERE   AC_ACCOUNT_ID              = @pAccountId 

END
GO

--
-- Update_MasterProfiles
--

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Update_MasterProfiles]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Update_MasterProfiles]
GO 

CREATE PROCEDURE [dbo].[Update_MasterProfiles]
  @pReturn INT OUTPUT
AS
BEGIN

  DECLARE @MasterId           AS BIGINT
  DECLARE @ProfileName        AS NVARCHAR(40)
  DECLARE @ProfileId          AS BIGINT
  DECLARE @MaxUsers           AS INT
  DECLARE @MasterTimeStamp    AS BIGINT
  
  DECLARE @ProfileGuiId       AS INT
  DECLARE @ProfileFormId      AS INT
  DECLARE @ProfileReadPerm    AS BIT
  DECLARE @ProfileWritePerm   AS BIT
  DECLARE @ProfileDeletePerm  AS BIT
  DECLARE @ProfileExecutePerm AS BIT

  DECLARE @FormOrder          AS INT
  DECLARE @FormNLSId          AS INT

  SET @pReturn = 0

  DECLARE FormsCursor CURSOR FOR SELECT DISTINCT GPF_GUI_ID, GPF_FORM_ID, GF_FORM_ORDER, GF_NLS_ID FROM #TEMP_MASTERS_PROFILES 
  OPEN    FormsCursor

  FETCH NEXT FROM FormsCursor INTO @ProfileGuiId, @ProfileFormId, @FormOrder, @FormNLSId
  WHILE @@FETCH_STATUS = 0
  BEGIN
	  IF NOT EXISTS (SELECT 1 FROM GUI_FORMS WHERE GF_GUI_ID = @ProfileGuiId AND GF_FORM_ID = @ProfileFormId )
    BEGIN
		  INSERT INTO GUI_FORMS (GF_GUI_ID,     GF_FORM_ID,     GF_FORM_ORDER, GF_NLS_ID  ) 
                     VALUES (@ProfileGuiId, @ProfileFormId, @FormOrder,    @FormNLSId )
	  END
	  FETCH NEXT FROM FormsCursor INTO @ProfileGuiId, @ProfileFormId, @FormOrder, @FormNLSId
  END

  CLOSE      FormsCursor
  DEALLOCATE FormsCursor

  DECLARE ProfilesCursor CURSOR FOR SELECT DISTINCT GUP_MASTER_ID, GUP_NAME, GUP_MAX_USERS, GUP_TIMESTAMP  FROM #TEMP_MASTERS_PROFILES 
  OPEN    ProfilesCursor

  FETCH NEXT FROM ProfilesCursor INTO @MasterId, @ProfileName, @MaxUsers, @MasterTimeStamp
  WHILE @@FETCH_STATUS = 0
  BEGIN
    SET @ProfileId = NULL
    SET @ProfileId = ( SELECT GUP_PROFILE_ID FROM GUI_USER_PROFILES WHERE GUP_MASTER_ID = @MasterId )

    IF @ProfileId IS NULL 
    BEGIN
      SET @ProfileId = ( SELECT GUP_PROFILE_ID FROM GUI_USER_PROFILES WHERE GUP_NAME = @ProfileName )
    END

    IF @ProfileId IS NULL 
    BEGIN
      -- is new profile
      SET @ProfileId = ISNULL((SELECT MAX(GUP_PROFILE_ID) FROM GUI_USER_PROFILES), 0) + 1
      
      INSERT INTO GUI_USER_PROFILES (GUP_PROFILE_ID, GUP_NAME) 
                             VALUES (@ProfileId,     @ProfileName)
    END

    UPDATE   GUI_USER_PROFILES 
       SET   GUP_MAX_USERS          = ISNULL(@MaxUsers, 0) 
           , GUP_NAME               = @ProfileName 
           , GUP_MASTER_ID          = @MasterId
           , GUP_MASTER_SECUENCE_ID = @MasterTimeStamp
     WHERE   GUP_PROFILE_ID = @ProfileId

    DELETE   GUI_PROFILE_FORMS
     WHERE   GPF_PROFILE_ID = @ProfileId
    
    INSERT   INTO GUI_PROFILE_FORMS 
		       ( GPF_PROFILE_ID, GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM )
		       
	  SELECT   @ProfileId,     GPF_GUI_ID, GPF_FORM_ID, GPF_READ_PERM, GPF_WRITE_PERM, GPF_DELETE_PERM, GPF_EXECUTE_PERM 
	    FROM   #TEMP_MASTERS_PROFILES
	   WHERE   GUP_NAME = @ProfileName
	     AND   GPF_READ_PERM IS NOT NULL

    FETCH NEXT FROM ProfilesCursor INTO @MasterId, @ProfileName, @MaxUsers, @MasterTimeStamp
  END
  CLOSE      ProfilesCursor
  DEALLOCATE ProfilesCursor

  DROP TABLE #TEMP_MASTERS_PROFILES
  
  SET @pReturn = 1
 
END
GO

/****** RECORDS ******/

INSERT INTO MS_SITE_TASKS (ST_TASK_ID, ST_ENABLED, ST_INTERVAL_SECONDS) VALUES (60, 1, 600); -- DownloadMastersProfiles   
GO
--
-- INSERT TASKS
--

-- UploadAccountDocuments
INSERT INTO MS_SITE_TASKS (ST_TASK_ID, ST_ENABLED, ST_INTERVAL_SECONDS)
VALUES (13, 1, 180);

-- DownloadAccountsDocuments
INSERT INTO MS_SITE_TASKS (ST_TASK_ID, ST_ENABLED, ST_INTERVAL_SECONDS)
VALUES (43, 1, 600);

--
-- New GeneralParams to disable cancellations.
--
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY = 'CancellationDisabled')
  INSERT INTO [dbo].[general_params]
             ([gp_group_key]
             ,[gp_subject_key]
             ,[gp_key_value])
       VALUES
             ('Cashier'
             ,'CancellationDisabled'
             ,'0');
GO

/************************************/
--
-- Script purpose: MoneyLaundering
--
/************************************/
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('1','es','M�XICO','MEXICANA');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('2','es','ESPA�A', 'ESPA�OLA');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('3','es','EE.UU.','ESTADOUNIDENSE');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('4','es','CANAD�', 'CANADIENSE');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('5','es','REPUBLICA DOMINICANA', 'DOMINICANA');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('6','es','COSTA RICA','COSTARRICENSE');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('7','es','GUATEMALA','GUATEMALTECA');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('8','es','PANAM�','PANAME�A');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('9','es','EL SALVADOR','SALVADORE�A');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('10','es','HONDURAS','HONDURE�A');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('11','es','NICARAGUA','NICARAG�ENSE');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('12','es','CUBA','CUBANA');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('13','es','COLOMBIA','COLOMBIANA');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('14','es','VENEZUELA','VENEZOLANA');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('15','es','PER�','PERUANA');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('16','es','ECUADOR','ECUATORIANA');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('17','es','ARGENTINA','ARGENTINA');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('18','es','BOLIVIA','BOLIVIANA');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('19','es','PARAGUAY','PARAGUAYA');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('20','es','URUGUAY','URUGUAYA');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('21','es','CHILE','CHILENA');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('22','es','BRASIL','BRASILE�A');

INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('1','en','MEXICO','MEXICAN');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('2','en','SPAIN', 'SPANISH');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('3','en','USA','AMERICAN');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('4','en','CANADA', 'CANADIAN');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('5','en','DOMINICAN REPUBLIC', 'DOMINICAN');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('6','en','COSTA RICA','COSTA RICAN');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('7','en','GUATEMALA','GUATEMALAN');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('8','en','PANAMA','PANAMANIAN');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('9','en','EL SALVADOR','SALVADOREAN');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('10','en','HONDURAS','HONDURAN');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('11','en','NICARAGUA','NICARAGUAN');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('12','en','CUBA','CUBAN');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('13','en','COLOMBIA','COLOMBIAN');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('14','en','VENEZUELA','VENEZUELAN');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('15','en','PERU','PERUVIAN');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('16','en','ECUADOR','ECUADORIAN');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('17','en','ARGENTINA','ARGENTINIAN');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('18','en','BOLIVIA','BOLIVIAN');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('19','en','PARAGUAY','PARAGUAYAN');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('20','en','URUGUAY','URUGUAYAN');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('21','en','CHILE','CHILEAN');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('22','en','BRAZIL','BRAZILIAN');
GO

/************************************/
--
-- Script purpose: MoneyLaundering
--
/************************************/

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'MoneyLaundering' AND GP_SUBJECT_KEY = 'Enabled')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('MoneyLaundering', 'Enabled', '0');
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'MoneyLaundering' AND GP_SUBJECT_KEY = 'ScanCountDown')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('MoneyLaundering', 'ScanCountDown', '30');
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.Voucher' AND GP_SUBJECT_KEY = 'AccountCustomize.Enabled')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Cashier.Voucher', 'AccountCustomize.Enabled', '0');
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.Voucher' AND GP_SUBJECT_KEY = 'AccountCustomize.Footer')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Cashier.Voucher', 'AccountCustomize.Footer', '');
GO

UPDATE   dbo.accounts
   SET   ac_holder_id1_type       = 1 -- RFC
       , ac_holder_id2_type       = 2 -- CURP
       , ac_beneficiary_id1_type  = 1 -- RFC
       , ac_beneficiary_id2_type  = 2 -- CURP
       , ac_holder_as_beneficiary = 1
GO

INSERT INTO Federal_States (FS_NAME) VALUES ('Distrito Federal');
INSERT INTO Federal_States (FS_NAME) VALUES ('Aguascalientes');
INSERT INTO Federal_States (FS_NAME) VALUES ('Baja California');
INSERT INTO Federal_States (FS_NAME) VALUES ('Baja California Sur');
INSERT INTO Federal_States (FS_NAME) VALUES ('Campeche');
INSERT INTO Federal_States (FS_NAME) VALUES ('Chiapas');
INSERT INTO Federal_States (FS_NAME) VALUES ('Chihuahua');
INSERT INTO Federal_States (FS_NAME) VALUES ('Coahuila de Zaragoza');
INSERT INTO Federal_States (FS_NAME) VALUES ('Colima');
INSERT INTO Federal_States (FS_NAME) VALUES ('Durango');
INSERT INTO Federal_States (FS_NAME) VALUES ('Guanajuato');
INSERT INTO Federal_States (FS_NAME) VALUES ('Guerrero');
INSERT INTO Federal_States (FS_NAME) VALUES ('Hidalgo');
INSERT INTO Federal_States (FS_NAME) VALUES ('Jalisco');
INSERT INTO Federal_States (FS_NAME) VALUES ('M�xico');
INSERT INTO Federal_States (FS_NAME) VALUES ('Michoac�n de Ocampo');
INSERT INTO Federal_States (FS_NAME) VALUES ('Morelos');
INSERT INTO Federal_States (FS_NAME) VALUES ('Nayarit');
INSERT INTO Federal_States (FS_NAME) VALUES ('Nuevo Le�n');
INSERT INTO Federal_States (FS_NAME) VALUES ('Oaxaca');
INSERT INTO Federal_States (FS_NAME) VALUES ('Puebla');
INSERT INTO Federal_States (FS_NAME) VALUES ('Quer�taro');
INSERT INTO Federal_States (FS_NAME) VALUES ('Quintana Roo');
INSERT INTO Federal_States (FS_NAME) VALUES ('San Luis Potos�');
INSERT INTO Federal_States (FS_NAME) VALUES ('Sinaloa');
INSERT INTO Federal_States (FS_NAME) VALUES ('Sonora');
INSERT INTO Federal_States (FS_NAME) VALUES ('Tabasco');
INSERT INTO Federal_States (FS_NAME) VALUES ('Tamaulipas');
INSERT INTO Federal_States (FS_NAME) VALUES ('Tlaxcala');
INSERT INTO Federal_States (FS_NAME) VALUES ('Veracruz de Ignacio de la Llave');
INSERT INTO Federal_States (FS_NAME) VALUES ('Yucat�n');
INSERT INTO Federal_States (FS_NAME) VALUES ('Zacatecas');
GO

/****** TRIGGERS ******/

--------------------------------------------------------------------------------
-- Copyright � 2013 Win Systems International 
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: MultiSiteTrigger_SiteAccountUpdate.sql
-- 
--   DESCRIPTION: Procedures for trigger MultiSiteTrigger_SiteAccountUpdate and related issues
-- 
--        AUTHOR: Jos� Mart�nez
-- 
-- CREATION DATE: 07-MAR-2013
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 07-MAR-2013 JML    First release.
-- 22-MAY-2013 DDM    Fixed bug #793
--------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[MultiSiteTrigger_SiteAccountUpdate]') AND type in (N'TR'))
DROP TRIGGER [dbo].[MultiSiteTrigger_SiteAccountUpdate]
GO

CREATE TRIGGER [dbo].[MultiSiteTrigger_SiteAccountUpdate] ON [dbo].[accounts]
   AFTER UPDATE
NOT FOR REPLICATION
AS 
BEGIN
    DECLARE @AccountId          AS BIGINT
    DECLARE @hash0              as varbinary(20)
    DECLARE @hash1              as varbinary(20)
    DECLARE @value              as nvarchar(max)
    DECLARE @changed            as bit
    DECLARE @updated            AS BIT    
    
    IF (UPDATE(AC_MS_CHANGE_GUID))  
         return

  IF NOT EXISTS (SELECT   1 
                   FROM   GENERAL_PARAMS 
                  WHERE   GP_GROUP_KEY   = N'Site' 
                    AND   GP_SUBJECT_KEY = N'MultiSiteMember' 
                    AND   GP_KEY_VALUE   = N'1')
  BEGIN
        RETURN
  END

  SET @updated = 0;  

IF UPDATE (AC_TRACK_DATA)
    OR UPDATE (AC_HOLDER_NAME)
    OR UPDATE (AC_HOLDER_ID)
    OR UPDATE (AC_HOLDER_ID_TYPE)
    OR UPDATE (AC_HOLDER_ADDRESS_01)
    OR UPDATE (AC_HOLDER_ADDRESS_02)
    OR UPDATE (AC_HOLDER_ADDRESS_03)
    OR UPDATE (AC_HOLDER_CITY)
    OR UPDATE (AC_HOLDER_ZIP)
    OR UPDATE (AC_HOLDER_EMAIL_01)
    OR UPDATE (AC_HOLDER_EMAIL_02)
    OR UPDATE (AC_HOLDER_TWITTER_ACCOUNT)
    OR UPDATE (AC_HOLDER_PHONE_NUMBER_01)
    OR UPDATE (AC_HOLDER_PHONE_NUMBER_02)
    OR UPDATE (AC_HOLDER_COMMENTS)
    OR UPDATE (AC_HOLDER_ID1)
    OR UPDATE (AC_HOLDER_ID2)
    OR UPDATE (AC_HOLDER_DOCUMENT_ID1)
    OR UPDATE (AC_HOLDER_DOCUMENT_ID2)
    OR UPDATE (AC_HOLDER_NAME1)
    OR UPDATE (AC_HOLDER_NAME2)
    OR UPDATE (AC_HOLDER_NAME3)
    OR UPDATE (AC_HOLDER_GENDER)
    OR UPDATE (AC_HOLDER_MARITAL_STATUS)
    OR UPDATE (AC_HOLDER_BIRTH_DATE)
    OR UPDATE (AC_HOLDER_WEDDING_DATE)
    OR UPDATE (AC_HOLDER_LEVEL)
    OR UPDATE (AC_HOLDER_LEVEL_NOTIFY)
    OR UPDATE (AC_HOLDER_LEVEL_ENTERED)
    OR UPDATE (AC_HOLDER_LEVEL_EXPIRATION)
    OR UPDATE (AC_PIN)
    OR UPDATE (AC_PIN_FAILURES)
    OR UPDATE (AC_PIN_LAST_MODIFIED)
    OR UPDATE (AC_BLOCKED)
    OR UPDATE (AC_ACTIVATED)
    OR UPDATE (AC_BLOCK_REASON)
    OR UPDATE (AC_HOLDER_IS_VIP)
    OR UPDATE (AC_HOLDER_TITLE)
    OR UPDATE (AC_HOLDER_NAME4)
    OR UPDATE (AC_HOLDER_PHONE_TYPE_01)
    OR UPDATE (AC_HOLDER_PHONE_TYPE_02)
    OR UPDATE (AC_HOLDER_STATE)
    OR UPDATE (AC_HOLDER_COUNTRY)
    OR UPDATE (AC_USER_TYPE)
    OR UPDATE (AC_POINTS_STATUS)
    OR UPDATE (AC_DEPOSIT)
    OR UPDATE (AC_CARD_PAID)
    OR UPDATE (AC_BLOCK_DESCRIPTION)    
    OR UPDATE (AC_MS_CREATED_ON_SITE_ID)
    OR UPDATE (AC_EXTERNAL_REFERENCE)
    OR UPDATE (AC_HOLDER_OCCUPATION)     
    OR UPDATE (AC_HOLDER_EXT_NUM)       
    OR UPDATE (AC_HOLDER_NATIONALITY)
    OR UPDATE (AC_HOLDER_BIRTH_COUNTRY)
    OR UPDATE (AC_HOLDER_FED_ENTITY)
    OR UPDATE (AC_HOLDER_ID1_TYPE)
    OR UPDATE (AC_HOLDER_ID2_TYPE)
    OR UPDATE (AC_HOLDER_ID3_TYPE)
    OR UPDATE (AC_HOLDER_ID3)
    OR UPDATE (AC_HOLDER_AS_BENEFICIARY)
    OR UPDATE (AC_BENEFICIARY_NAME)
    OR UPDATE (AC_BENEFICIARY_NAME1)
    OR UPDATE (AC_BENEFICIARY_NAME2)
    OR UPDATE (AC_BENEFICIARY_NAME3)
    OR UPDATE (AC_BENEFICIARY_BIRTH_DATE)
    OR UPDATE (AC_BENEFICIARY_GENDER)
    OR UPDATE (AC_BENEFICIARY_OCCUPATION)
    OR UPDATE (AC_BENEFICIARY_ID1_TYPE)
    OR UPDATE (AC_BENEFICIARY_ID1)
    OR UPDATE (AC_BENEFICIARY_ID2_TYPE)
    OR UPDATE (AC_BENEFICIARY_ID2)
    OR UPDATE (AC_BENEFICIARY_ID3_TYPE)
    OR UPDATE (AC_BENEFICIARY_ID3)
        SET @updated = 1;
        
    IF (@updated = 0) RETURN

    DECLARE PersonalInfoCursor CURSOR FOR 
     SELECT   AC_ACCOUNT_ID
            , HASHBYTES ('SHA1',  ISNULL(AC_TRACK_DATA,        '')
                                + ISNULL(AC_HOLDER_NAME,       '')
                                + ISNULL(AC_HOLDER_ID,         '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_ID_TYPE), '')
                                + ISNULL(AC_HOLDER_ADDRESS_01, '')
                                + ISNULL(AC_HOLDER_ADDRESS_02, '')
                                + ISNULL(AC_HOLDER_ADDRESS_03, '')
                                + ISNULL(AC_HOLDER_CITY,       '')
                                + ISNULL(AC_HOLDER_ZIP,        '')
                                + ISNULL(AC_HOLDER_EMAIL_01,   '')
                                + ISNULL(AC_HOLDER_EMAIL_02,   '')
                                + ISNULL(AC_HOLDER_TWITTER_ACCOUNT, '')
                                + ISNULL(AC_HOLDER_PHONE_NUMBER_01, '')
                                + ISNULL(AC_HOLDER_PHONE_NUMBER_02, '')
                                + ISNULL(AC_HOLDER_COMMENTS,        '')
                                + ISNULL(AC_HOLDER_ID1, '')
                                + ISNULL(AC_HOLDER_ID2, '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_DOCUMENT_ID1), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_DOCUMENT_ID2), '')
                                + ISNULL(AC_HOLDER_NAME1, '')
                                + ISNULL(AC_HOLDER_NAME2, '')
                                + ISNULL(AC_HOLDER_NAME3, '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_GENDER),         '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_MARITAL_STATUS), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_BIRTH_DATE,   21), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_WEDDING_DATE, 21), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_LEVEL),                '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_LEVEL_NOTIFY),         '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_LEVEL_ENTERED,    21), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_LEVEL_EXPIRATION, 21), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_PIN),          '')
                                + ISNULL(CONVERT(NVARCHAR, AC_PIN_FAILURES), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_PIN_LAST_MODIFIED, 21), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_BLOCKED),           '')
                                + ISNULL(CONVERT(NVARCHAR, AC_ACTIVATED),         '')
                                + ISNULL(CONVERT(NVARCHAR, AC_BLOCK_REASON),         '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_IS_VIP),        '')
                                + ISNULL(AC_HOLDER_TITLE                        , '')
                                + ISNULL(AC_HOLDER_NAME4                        , '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_PHONE_TYPE_01),        '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_PHONE_TYPE_02),        '')
                                + ISNULL(AC_HOLDER_STATE                        , '')
                                + ISNULL(AC_HOLDER_COUNTRY                      , '') 
                                + ISNULL(CONVERT(NVARCHAR,AC_USER_TYPE)         , '')
                                + ISNULL(CONVERT(NVARCHAR,AC_POINTS_STATUS)     , '') 
                                + ISNULL(CONVERT(NVARCHAR,AC_DEPOSIT)           , '')
                                + ISNULL(CONVERT(NVARCHAR,AC_CARD_PAID)         , '')
                                + ISNULL(AC_BLOCK_DESCRIPTION, '')
                                + ISNULL(CONVERT(NVARCHAR,AC_MS_CREATED_ON_SITE_ID), '')
                                + ISNULL(AC_EXTERNAL_REFERENCE, '')
                                + ISNULL(AC_HOLDER_OCCUPATION, '')
                                + ISNULL(AC_HOLDER_EXT_NUM, '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_NATIONALITY),         '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_BIRTH_COUNTRY), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_ID1_TYPE), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_ID2_TYPE), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_ID3_TYPE), '')
                                + ISNULL(AC_HOLDER_ID3, '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_AS_BENEFICIARY), '')
                                + ISNULL(AC_BENEFICIARY_NAME, '')
                                + ISNULL(AC_BENEFICIARY_NAME1, '')
                                + ISNULL(AC_BENEFICIARY_NAME2, '')
                                + ISNULL(AC_BENEFICIARY_NAME3, '')
                                + ISNULL(CONVERT(NVARCHAR, AC_BENEFICIARY_BIRTH_DATE,   21), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_BENEFICIARY_GENDER), '')	
                                + ISNULL(AC_BENEFICIARY_OCCUPATION, '')
                                + ISNULL(CONVERT(NVARCHAR, AC_BENEFICIARY_ID1_TYPE), '')				
                                + ISNULL(AC_BENEFICIARY_ID1, '')
                                + ISNULL(CONVERT(NVARCHAR, AC_BENEFICIARY_ID2_TYPE), '')				
                                + ISNULL(AC_BENEFICIARY_ID2, '')
                                + ISNULL(CONVERT(NVARCHAR, AC_BENEFICIARY_ID3_TYPE), '')				
                                + ISNULL(AC_BENEFICIARY_ID3, '') )
       FROM   INSERTED
      WHERE   AC_TRACK_DATA not like '%-NEW-%'

    SET NOCOUNT ON;

    OPEN PersonalInfoCursor

    FETCH NEXT FROM PersonalInfoCursor INTO @AccountId, @hash1

    WHILE @@FETCH_STATUS = 0
    BEGIN

        SELECT @hash0 = AC_MS_HASH FROM DELETED WHERE AC_ACCOUNT_ID = @AccountId

        SELECT @changed = CASE WHEN ( @hash0 = @hash1 ) THEN 0 ELSE 1 END

        IF @changed = 1
        BEGIN
           DECLARE @new_id as uniqueidentifier
           
           SET @new_id = NEWID()
           
            -- Personal Info
            UPDATE   ACCOUNTS
               SET   AC_MS_HASH                 = @hash1
                   , AC_MS_CHANGE_GUID          = @new_id
             WHERE   AC_ACCOUNT_ID              = @AccountId
            
            
            IF NOT EXISTS (SELECT 1 FROM MS_SITE_PENDING_ACCOUNTS WHERE SPA_ACCOUNT_ID = @AccountId)
              INSERT INTO   MS_SITE_PENDING_ACCOUNTS (SPA_ACCOUNT_ID, SPA_GUID) VALUES (@AccountId, @new_id )
            ELSE 
              UPDATE MS_SITE_PENDING_ACCOUNTS SET SPA_GUID = @new_id WHERE SPA_ACCOUNT_ID = @AccountId
            
            
        END

        FETCH NEXT FROM PersonalInfoCursor INTO @AccountId, @hash1
    END

    CLOSE PersonalInfoCursor
    DEALLOCATE PersonalInfoCursor

END
GO

--------------------------------------------------------------------------------
-- Copyright � 2013 Win Systems International 
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: MultiSiteTrigger_SiteAccountDocuments.sql
-- 
--   DESCRIPTION: Trigger MultiSiteTrigger_SiteAccountDocuments for insert data into MS_SITE_PENDING_ACCOUNT_DOCUMENTS
-- 
--        AUTHOR: Jos� Mart�nez
-- 
-- CREATION DATE: 02-JUL-2013
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 02-JUL-2013 JML    First release.
-------------------------------------------------------------------------------
--

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[MultiSiteTrigger_SiteAccountDocuments]'))
DROP TRIGGER [dbo].[MultiSiteTrigger_SiteAccountDocuments]
GO

CREATE TRIGGER [dbo].[MultiSiteTrigger_SiteAccountDocuments]
ON [dbo].[account_documents]
AFTER UPDATE
NOT FOR REPLICATION
AS
  BEGIN
    IF ( UPDATE(AD_MS_SEQUENCE_ID) )
    BEGIN      
      RETURN
    END

    IF NOT EXISTS (SELECT   1 
                     FROM   GENERAL_PARAMS 
                    WHERE   GP_GROUP_KEY   = N'Site' 
                      AND   GP_SUBJECT_KEY = N'MultiSiteMember' 
                      AND   GP_KEY_VALUE   = N'1')
    BEGIN
          RETURN
    END
  
    SET NOCOUNT ON

    -- Insert movement to synchronize
    INSERT INTO   MS_SITE_PENDING_ACCOUNT_DOCUMENTS
                ( PAD_ACCOUNT_ID )
         SELECT   AD_ACCOUNT_ID 
           FROM   INSERTED 
          WHERE   AD_ACCOUNT_ID NOT IN (SELECT PAD_ACCOUNT_ID FROM MS_SITE_PENDING_ACCOUNT_DOCUMENTS)
   
    SET NOCOUNT OFF

  END -- [MultiSiteTrigger_SiteAccountDocuments]
GO