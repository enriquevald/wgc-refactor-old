/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 96;

SET @New_ReleaseId = 97;
SET @New_ScriptName = N'UpdateTo_18.097.022.sql';
SET @New_Description = N'Updates for terminal-game relation, WSP Interface, PSA Client and payment orders.';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/

-- Datetime updated columns for terminal-game relation.
ALTER TABLE dbo.terminal_game_translation ADD
	tgt_created datetime NOT NULL CONSTRAINT DF_terminal_game_translation_tgt_created DEFAULT getdate(),
	tgt_updated datetime NOT NULL CONSTRAINT DF_terminal_game_translation_tgt_updated DEFAULT getdate()

GO

--
-- BEGIN Tables for WSP Store Procedures
--

CREATE TABLE [dbo].[wsp_authorized_vendors](
	[wav_vendor_id] [nvarchar](50) NOT NULL,
	[wav_authorized] [bit] NOT NULL,
 CONSTRAINT [PK_wsp_authorized_vendors] PRIMARY KEY CLUSTERED 
(
	[wav_vendor_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[wsp_player_spent_by_day](
	[wps_account_id] [bigint] NOT NULL,
	[wps_day] [datetime] NOT NULL,
	[wps_amount] [money] NOT NULL,
	[wps_used] [bit] NOT NULL,
 CONSTRAINT [PK_wsp_player_spent_by_day] PRIMARY KEY CLUSTERED 
(
	[wps_account_id] ASC,
	[wps_day] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[wsp_player_recharge](
	[wpr_unique_id] [bigint] IDENTITY(1,1) NOT NULL,
	[wpr_account_id] [bigint] NOT NULL,
	[wpr_nr_amount] [money] NOT NULL,
	[wpr_status] [int] NOT NULL,
	[wpr_created] [datetime] NOT NULL CONSTRAINT [DF_wsp_player_recharge_wpr_created]  DEFAULT (getdate()),
 CONSTRAINT [PK_wsp_player_recharge] PRIMARY KEY CLUSTERED 
(
	[wpr_unique_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 - Pending; 2 - In Progress; 3 - Error; 4 - Ok; 5 - Timeout' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'wsp_player_recharge', @level2type=N'COLUMN',@level2name=N'wpr_status'

--
-- END Tables for WSP Store Procedures
--

GO

--
-- BEGIN Tables for PSA Client
--

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[sales_per_hour]') and name = 'sph_timestamp')
  ALTER TABLE [dbo].[sales_per_hour]
          ADD [sph_timestamp] [timestamp] NULL
ELSE
  SELECT '***** Field sales_per_hour.sph_timestamp already exists *****';
  
--
-- END Tables for PSA Client
--

GO

--
-- BEGIN Tables for Payment Order
--

CREATE TABLE [dbo].[bank_accounts](
	[ba_account_id] [int] IDENTITY(1,1) NOT NULL,
	[ba_bank_name] [nvarchar](50) NOT NULL,
	[ba_description] [nvarchar](50) NOT NULL,
	[ba_customer_number] [nvarchar](50) NOT NULL,
	[ba_account_number] [nvarchar](50) NOT NULL,
	[ba_effective_days] [nvarchar](50) NOT NULL,
	[ba_method_payment] [nvarchar](50) NOT NULL,
	[ba_enabled] [bit] NOT NULL,
 CONSTRAINT [PK_bank_accounts] PRIMARY KEY CLUSTERED 
(
	[ba_account_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[account_payment_orders](
	[apo_operation_id] [bigint] NOT NULL,
	[apo_account_id] [bigint] NOT NULL,
	[apo_datetime] [datetime] NOT NULL CONSTRAINT [DF_account_payment_order_apo_datetime]  DEFAULT (getdate()),
	[apo_player_name1] [nvarchar](50) NOT NULL,
	[apo_player_name2] [nvarchar](50) NULL,
	[apo_player_name3] [nvarchar](50) NULL,
	[apo_player_doc_type1] [nvarchar](20) NOT NULL,
	[apo_player_doc_id1] [nvarchar](20) NOT NULL,
	[apo_player_doc_type2] [nvarchar](20) NOT NULL,
	[apo_player_doc_id2] [nvarchar](20) NOT NULL,
	[apo_effective_days] [nvarchar](20) NOT NULL,
	[apo_application_date] [datetime] NOT NULL,
	[apo_payment_type] [nvarchar](20) NOT NULL,
	[apo_bank_id] [bigint] NOT NULL,
	[apo_bank_name] [nvarchar](50) NULL,
	[apo_bank_customer_number] [nvarchar](50) NULL,
	[apo_bank_account_number] [nvarchar](50) NULL,
	[apo_account_balance] [money] NOT NULL,
	[apo_return_balance] [money] NOT NULL,
	[apo_prize] [money] NOT NULL,
	[apo_tax1] [money] NULL,
	[apo_tax2] [money] NULL,
	[apo_tax3] [money] NULL,
	[apo_total_payment] [money] NOT NULL,
	[apo_cash_payment] [money] NOT NULL,
	[apo_check_payment] [money] NOT NULL,
	[apo_authorized_by_title1] [nvarchar](50) NULL,
	[apo_authorized_by_name1] [nvarchar](200) NULL,
	[apo_authorized_by_title2] [nvarchar](50) NULL,
	[apo_authorized_by_name2] [nvarchar](200) NULL,
	[apo_doc_reference] [nvarchar](20) NULL,
	[apo_document_id] [bigint] NULL,
 CONSTRAINT [PK_account_payment_order] PRIMARY KEY CLUSTERED 
(
	[apo_operation_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[account_payment_orders]  WITH CHECK ADD  CONSTRAINT [FK_account_payment_order_account_operations] FOREIGN KEY([apo_operation_id])
REFERENCES [dbo].[account_operations] ([ao_operation_id])
GO
ALTER TABLE [dbo].[account_payment_orders] CHECK CONSTRAINT [FK_account_payment_order_account_operations]
GO
ALTER TABLE [dbo].[account_payment_orders]  WITH CHECK ADD  CONSTRAINT [FK_account_payment_order_accounts] FOREIGN KEY([apo_account_id])
REFERENCES [dbo].[accounts] ([ac_account_id])
GO
ALTER TABLE [dbo].[account_payment_orders] CHECK CONSTRAINT [FK_account_payment_order_accounts]

--
-- END Tables for Payment Order
--

GO

/****** INDEXES ******/

--
-- BEGIN Indexes for PSA Client
--

CREATE UNIQUE NONCLUSTERED INDEX [IX_sph_timestamp] ON [dbo].[sales_per_hour] 
(
      [sph_timestamp] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

GO

CREATE UNIQUE NONCLUSTERED INDEX [IX_ps_timestamp] ON [dbo].[play_sessions] 
(
      [ps_timestamp] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

--
-- END Indexes for PSA Client
--

--
-- BEGIN Indexes for Payment Order
--

CREATE NONCLUSTERED INDEX [IX_apo_account_datetime] ON [dbo].[account_payment_orders] 
(
	[apo_account_id] ASC,
	[apo_datetime] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

GO

CREATE NONCLUSTERED INDEX [IX_apo_datetime] ON [dbo].[account_payment_orders] 
(
	[apo_datetime] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

--
-- END Indexes for Payment Order
--

GO

/****** RECORDS ******/

/****** WSP - External NR promotion ******/
DECLARE @expiration_value AS INT

SET @expiration_value = 30

IF NOT EXISTS (SELECT * FROM promotions WHERE pm_type = 10)
BEGIN
  INSERT INTO [dbo].[promotions]
             ([pm_name]
		         ,[pm_enabled]
             ,[pm_type]
             ,[pm_date_start]
             ,[pm_date_finish]
             ,[pm_schedule_weekday]
             ,[pm_schedule1_time_from]
             ,[pm_schedule1_time_to]
             ,[pm_schedule2_enabled]
             ,[pm_gender_filter]
             ,[pm_birthday_filter]
             ,[pm_expiration_type]
             ,[pm_expiration_value]
             ,[pm_min_cash_in]
             ,[pm_min_cash_in_reward]
             ,[pm_cash_in]
             ,[pm_cash_in_reward]
             ,[pm_num_tokens]
             ,[pm_token_reward]
             ,[pm_level_filter]
             ,[pm_permission]
             ,[pm_min_spent]
             ,[pm_min_spent_reward]
             ,[pm_spent]
             ,[pm_spent_reward]
             ,[pm_min_played]
             ,[pm_min_played_reward]
             ,[pm_played]
             ,[pm_played_reward]
             ,[pm_play_restricted_to_provider_list]
             ,[pm_credit_type]
             ,[pm_visible_on_promobox])
       VALUES
             ( 'NR Externa'
             , 1
             , 10
             , CAST('01-01-2010 00:00:00' as DATETIME)
             , CAST('01-01-2023 00:00:00' as DATETIME)
             , 127
             , 0
             , 0
             , 0
             , 0
             , 0
             , 1
             , @expiration_value
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , 0
             , 0.00
             , 0
             , 0
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , 0.00
             , 0
             , 1
             , 0)
END

INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('PSAClient', 'MonitorizacionWaitMinutesForShipping', '15')
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('PSAClient', 'AllowSendMonitorizacion', '0')

UPDATE   GENERAL_PARAMS 
   SET   GP_SUBJECT_KEY = 'NumberOfPreviousMonthsToReport' 
       , GP_KEY_VALUE   = '1' 
 WHERE   GP_GROUP_KEY   = 'PSAClient' 
   AND   GP_SUBJECT_KEY = 'NumberOfMonthsToReport'

DELETE   GENERAL_PARAMS 
 WHERE   GP_GROUP_KEY = 'PSAClient' 
   AND   GP_SUBJECT_KEY='ClaveEstablecimiento'

--
-- Payment Order
--

INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('PaymentOrder', 'Enabled', '0')
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('PaymentOrder', 'MinAmount', '0')

INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('PaymentOrder', 'DocumentTitle', 'Orden de Pago')

INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('PaymentOrder', 'Authorization1.Required', '1')
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('PaymentOrder', 'Authorization1.DefaultTitle', 'Gerente')
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('PaymentOrder', 'Authorization1.DefaultName', '')

INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('PaymentOrder', 'Authorization2.Required', '1')
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('PaymentOrder', 'Authorization2.DefaultTitle', 'Cajero General')
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('PaymentOrder', 'Authorization2.DefaultName', '')

INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('PaymentOrder', 'AllowedDocuments1', 'IFE;FM2;FM3;PASAPORTE')
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('PaymentOrder', 'AllowedDocuments2', '')

INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
     SELECT 'PaymentOrder', 'EditableMinutes', GP_KEY_VALUE
       FROM GENERAL_PARAMS
      WHERE GP_GROUP_KEY   = 'Witholding.Document'
        AND GP_SUBJECT_KEY = 'EditableMinutes';

INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
     SELECT 'PaymentOrder', 'UpdateAccount', GP_KEY_VALUE
       FROM GENERAL_PARAMS
      WHERE GP_GROUP_KEY   = 'Witholding.Document'
        AND GP_SUBJECT_KEY = 'UpdateAccount';


--
-- Delete Segmentation Reports
--
DELETE   REPORTS 
 WHERE   REP_TYPE = 0


/****** STORE PROCEDURES ******/

--
-- BEGIN WSP Interface
--

--------------------------------------------------------------------------------
-- PURPOSE: Check Conditions
-- 
--  PARAMS:
--      - INPUT:
--        @pVendorID    NVARCHAR(16)
--        @pTrackData   NVARCHAR(24)
--        @pCheckWCP    BIT
--
--      - OUTPUT:
--        @pStatusCode   INT
--        @pStatusText   NVARCHAR(256)
--
--   NOTES:
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WSP_CheckConditions]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[WSP_CheckConditions]
GO
CREATE PROCEDURE [dbo].[WSP_CheckConditions]
  @pVendorID       NVARCHAR(16),
  @pAccountID      BIGINT,
  @pCheckWCP       BIT,
  @pStatusCode     INT           OUTPUT,
  @pStatusText     NVARCHAR(256) OUTPUT
AS
BEGIN
  DECLARE @auth_vendor      AS BIT
  DECLARE @blocked          AS BIT
  DECLARE @wcp_num_running  AS INT

  SET @pStatusCode = 4
  SET @pStatusText = N'Error'

  -- Check if vendor is authorized
  --
  SELECT   @auth_vendor  = WAV_AUTHORIZED
    FROM   WSP_AUTHORIZED_VENDORS
   WHERE   WAV_VENDOR_ID = @pVendorID
 
  SET @auth_vendor = ISNULL(@auth_vendor, 0)
  IF @auth_vendor = 0
  BEGIN
    SET @pStatusCode = 1
    SET @pStatusText = N'Vendor Not Authorized'
  
    RETURN
  END
 
  -- Check if account exists
  -- 
  SET @pAccountID = ISNULL(@pAccountID, 0)
  IF @pAccountID <= 0
  BEGIN
    SET @pStatusCode = 2
    SET @pStatusText = N'Account Not Found'

    RETURN
  END

  -- Check if account is blocked
  --
  SELECT   @blocked      = AC_BLOCKED
    FROM   ACCOUNTS
   WHERE   AC_ACCOUNT_ID = @pAccountID

  SET @blocked = ISNULL(@blocked, 1)
  IF @blocked = 1
  BEGIN
    SET @pStatusCode = 3
    SET @pStatusText = N'Account Blocked'

    RETURN
  END

  SET @pCheckWCP = ISNULL(@pCheckWCP, 1)
  IF @pCheckWCP = 0
  BEGIN
    SET @pStatusCode = 0
    SET @pStatusText = N'Success'

    RETURN
  END

  -- Check if the WCP thread is on.
  SELECT   @wcp_num_running = COUNT(SVC_STATUS)
    FROM   SERVICES
   WHERE   SVC_STATUS       = 'RUNNING'
     AND   SVC_PROTOCOL     = 'WCP'

  IF @wcp_num_running = 0
  BEGIN
    SET @pStatusCode = 4
    SET @pStatusText = N'Can not add NR amount. WCP Service not running.'

    RETURN
  END

  SET @pStatusCode = 0
  SET @pStatusText = N'Success'

END -- WSP_CheckConditions

GO

--------------------------------------------------------------------------------
-- PURPOSE : Returns personal data, information about the Wigos loyalty program and activity information
--           from the customer.
--
--  PARAMS :
--      - INPUT :
--          @pVendorID  NVARCHAR(16)
--          @pTrackData NVARCHAR(24)
--
--      - OUTPUT :
--          StatusCode
--          StatusText
--          PlayerName
--          Gender
--          Birthdate
--          Level
--          LevelName
--          Points
--          LastActivity
--          PlayedToday
--          WonToday
--
-- RETURNS :
--
--   NOTES :
--      Possible values for StatusCode:
--        0: Success
--        1: Vendor Not Authorized
--        2: Account Not Found
--        3: Account Blocked
--        4: Error

ALTER PROCEDURE [dbo].[WSP_PlayerInfo]
    @pVendorID   NVARCHAR(16),
    @pTrackData  NVARCHAR(24)
AS
BEGIN
  -- SET NOCOUNT ON added to prevent extra result sets from
  -- interfering with SELECT statements.
  SET NOCOUNT ON;

  DECLARE @pStatusCode    AS INT
  DECLARE @pStatusText    AS NVARCHAR(256)

  DECLARE @pBlocked       AS BIT
  DECLARE @pPlayed        AS MONEY
  DECLARE @pWon           AS MONEY
  DECLARE @pToday         AS DATETIME
  DECLARE @pSiteId        AS INT
  DECLARE @pAccountId     AS BIGINT
  DECLARE @pHolderLevel   AS INT
  DECLARE @pLevelPrefix   AS NVARCHAR (50)
  DECLARE @pLevelName     AS NVARCHAR (50)
  DECLARE @auth_vendor    AS BIT

  SET @pStatusCode = 4
  SET @pStatusText = N'Error'

  BEGIN TRY
  
    -- Check if vendor is authorized
    --
    SELECT   @auth_vendor  = WAV_AUTHORIZED
      FROM   WSP_AUTHORIZED_VENDORS
     WHERE   WAV_VENDOR_ID = @pVendorID

    SET @auth_vendor = ISNULL(@auth_vendor, 0)
    IF @auth_vendor = 0
    BEGIN
      SET @pStatusCode = 1
      SET @pStatusText = N'Vendor Not Authorized'

      GOTO END_PROCEDURE
    END

    SET @pAccountId = ( SELECT AC_ACCOUNT_ID From ACCOUNTS where ac_track_data  = dbo.TrackDataToInternal(@pTrackData))
    SET @pAccountId = ISNULL (@pAccountId, 0)

    SET @pStatusCode = 2
    SET @pStatusText = N'Account Not Found'

    IF ( @pAccountId > 0 )
    BEGIN

      SET @pStatusCode = 3
      SET @pStatusText = N'Account Blocked'

      SELECT @pBlocked = AC_BLOCKED, @pHolderLevel = AC_HOLDER_LEVEL FROM ACCOUNTS WHERE ac_account_id = @pAccountId

      IF ( @pHolderLevel = 1 )      SET @pLevelPrefix = 'Level01'
      ELSE IF ( @pHolderLevel = 2 ) SET @pLevelPrefix = 'Level02'
      ELSE IF ( @pHolderLevel = 3 ) SET @pLevelPrefix = 'Level03'
      ELSE IF ( @pHolderLevel = 4 ) SET @pLevelPrefix = 'Level04'
      ELSE SET @pLevelPrefix = NULL

      IF ( @pLevelPrefix IS NOT NULL )
      BEGIN
        SELECT @pLevelName = GP_KEY_VALUE
          FROM GENERAL_PARAMS
         WHERE GP_GROUP_KEY = 'PlayerTracking'
           AND GP_SUBJECT_KEY = @pLevelPrefix + '.Name'
      END

      SET @pSiteId = ISNULL ( ( SELECT   CAST (GP_KEY_VALUE AS INT)
                         FROM   GENERAL_PARAMS
                        WHERE   GP_GROUP_KEY   = 'Site'
                          AND   GP_SUBJECT_KEY = 'Identifier' ), 0 )

      SET @pToday = ( SELECT dbo.TodayOpening (@pSiteId) )

      SELECT   @pPlayed       = SUM(PS_PLAYED_AMOUNT)
             , @pWon          = SUM(PS_WON_AMOUNT)
        FROM   PLAY_SESSIONS WITH(INDEX(IX_ps_account_id_started))
       WHERE   PS_ACCOUNT_ID  = @pAccountId
         AND   PS_STARTED    >= @pToday

      IF ( @pBlocked = 0 )
      BEGIN
        -- Insert statements for procedure here
        SELECT   0                                                                                        StatusCode
               , 'Success'                                                                                StatusText
               , CASE WHEN @pLevelPrefix IS NULL THEN NULL ELSE AC_HOLDER_NAME END                        PlayerName
               , CASE WHEN @pLevelPrefix IS NULL THEN NULL ELSE AC_HOLDER_GENDER END                      Gender
               , CASE WHEN @pLevelPrefix IS NULL THEN NULL ELSE AC_HOLDER_BIRTH_DATE END                  Birthdate
               , AC_HOLDER_LEVEL                                                                          Level
               , CASE WHEN @pLevelPrefix IS NULL THEN NULL ELSE @pLevelName END                           LevelName
               , CASE WHEN @pLevelPrefix IS NULL THEN NULL ELSE CAST (ROUND(AC_POINTS, 0, 1) AS INT) END  Points
               , AC_LAST_ACTIVITY                                                                         LastActivity
               , ISNULL (@pPlayed, 0)                                                                     PlayedToday
               , ISNULL (@pWon,    0)                                                                     WonToday

        From ACCOUNTS
        where ac_account_id = @pAccountId

        SET @pStatusCode = 0
        SET @pStatusText = N'Success'

      END
    END

  END TRY
  BEGIN CATCH
    SET @pStatusCode = 4
    SET @pStatusText  = N'Error:'
                      + N' ERROR_NUMBER: '    + CAST(ERROR_NUMBER()               AS NVARCHAR)
                      + N' ERROR_SEVERITY: '  + CAST(ERROR_SEVERITY()             AS NVARCHAR)
                      + N' ERROR_STATE: '     + CAST(ERROR_STATE()                AS NVARCHAR)
                      + N' ERROR_PROCEDURE: ' + CAST(ISNULL(ERROR_PROCEDURE(),'') AS NVARCHAR)
                      + N' ERROR_LINE: '      + CAST(ERROR_LINE()                 AS NVARCHAR)
                      + N' ERROR_MESSAGE: '   + CAST(ERROR_MESSAGE()              AS NVARCHAR(MAX))
  END CATCH

END_PROCEDURE:
  IF ( @pStatusCode <> 0 )
    SELECT  @pStatusCode   StatusCode
          , @pStatusText  StatusText
          , NULL PlayerName
          , NULL Gender
          , NULL Birthdate
          , NULL Level
          , NULL LevelName
          , NULL Points
          , NULL LastActivity
          , NULL PlayedToday
          , NULL WonToday

END -- WSP_PlayerInfo

GO

--------------------------------------------------------------------------------
-- PURPOSE : Obtain the redeemable amount that the customer spent during the previous day.
--
--  PARAMS :
--      - INPUT :
--          @pVendorID  NVARCHAR(16)
--          @pTrackData NVARCHAR(24)
--
--      - OUTPUT :
--          StatusCode
--          StatusText
--          SpentYesterday
--
-- RETURNS :
--
--   NOTES :
--      Possible values for StatusCode:
--        0: Success
--        1: Vendor Not Authorized
--        2: Account Not Found
--        3: Account Blocked
--        4: Error
--        5: Amount Already Reset

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WSP_PlayerQuerySpentYesterday]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[WSP_PlayerQuerySpentYesterday]
GO
CREATE PROCEDURE [dbo].[WSP_PlayerQuerySpentYesterday]
    @pVendorID  NVARCHAR(16),
    @pTrackData NVARCHAR(24)
AS
BEGIN
  -- SET NOCOUNT ON added to prevent extra result sets from
  -- interfering with SELECT statements.
  SET NOCOUNT ON;

  DECLARE @status_code      AS INT
  DECLARE @status_text      AS NVARCHAR(256)
  DECLARE @status_code_aux  AS INT
  DECLARE @status_text_aux  AS NVARCHAR(256)
  
  DECLARE @spent            AS MONEY
  DECLARE @account_id       AS BIGINT
  DECLARE @spent_used       AS BIT
  DECLARE @spent_amount     AS MONEY
  DECLARE @site_id          AS INT
  DECLARE @today            AS DATETIME
  DECLARE @yesterday        AS DATETIME
  DECLARE @yesterday_day    AS DATETIME
  DECLARE @rc               AS INT

  SET @spent = 0

  SET @status_code = 4
  SET @status_text = N'Error'

  BEGIN TRY
  
    -- Select @account_id
    SELECT  @account_id   = AC_ACCOUNT_ID
    FROM    ACCOUNTS
    WHERE   AC_TRACK_DATA = dbo.TrackDataToInternal(@pTrackData)
        
    SET @account_id = ISNULL(@account_id, 0)
   
    -- Check conditions
    EXEC dbo.WSP_CheckConditions @pVendorID, @account_id, 0, @status_code_aux OUTPUT, @status_text_aux OUTPUT

    IF @status_code_aux <> 0
    BEGIN
      SET @status_code = @status_code_aux
      SET @status_text = @status_text_aux
      
      GOTO END_PROCEDURE
    END

    -- Get yesterday 'day'
    SET @site_id = ISNULL ( ( SELECT   CAST (GP_KEY_VALUE AS INT)
                                FROM   GENERAL_PARAMS
                               WHERE   GP_GROUP_KEY   = 'Site'
                                 AND   GP_SUBJECT_KEY = 'Identifier' ), 0 )
    SET @today = dbo.TodayOpening(@site_id)
    SET @yesterday = DATEADD(DAY, -1, @today)
    -- Trunc date to start of the day (00:00h).
    SET @yesterday_day = DATEADD(DAY, DATEDIFF(day, 0, @yesterday), 0)

    -- Check if spent amount is already used
    --
    SELECT   @spent_used    = WPS_USED
           , @spent_amount  = WPS_AMOUNT
      FROM   WSP_PLAYER_SPENT_BY_DAY
     WHERE   WPS_ACCOUNT_ID = @account_id
       AND   WPS_DAY        = @yesterday_day

    SET @spent_used = ISNULL(@spent_used, 0)
    IF @spent_used = 1
    BEGIN
      SET @status_code = 5
      SET @status_text = N'Amount Already Reset'

      GOTO END_PROCEDURE
    END

    -- Check if the spent amount is already calculated
    --
    IF @spent_amount IS NOT NULL
    BEGIN
      SET @spent = @spent_amount
      SET @status_code = 0
      SET @status_text = N'Success'

      GOTO END_PROCEDURE
    END

    -- Calculate the spent amount and save it
    --
    SELECT   @spent = SUM(CASE WHEN PS_REDEEMABLE_PLAYED > PS_REDEEMABLE_WON
                               THEN PS_REDEEMABLE_PLAYED - PS_REDEEMABLE_WON
                               ELSE 0 END)
      FROM   PLAY_SESSIONS WITH(INDEX(IX_ps_account_id_finished))
     WHERE   PS_ACCOUNT_ID  = @account_id
       AND   PS_FINISHED   >= @yesterday
       AND   PS_FINISHED    < @today
       AND   PS_STATUS     <> 0   -- PlaySessionStatus.Opened = 0

    SET @spent = ISNULL(@spent, 0)

    INSERT INTO   WSP_PLAYER_SPENT_BY_DAY
                ( WPS_ACCOUNT_ID
                , WPS_DAY
                , WPS_AMOUNT
                , WPS_USED
                )
         VALUES
                ( @account_id
                , @yesterday_day
                , @spent
                , 0
                )

    SET @rc = @@ROWCOUNT
    IF @rc <> 1
    BEGIN
      SET @status_code = 4
      SET @status_text = N'Error: Can not save the spent amount of ' + CAST(@spent AS NVARCHAR)
                       + N' for account_id ' + CAST(@account_id AS NVARCHAR)
                       + N', day ' + CAST(@yesterday_day AS NVARCHAR)
                       + N'.'

      GOTO END_PROCEDURE
    END

    SET @status_code = 0
    SET @status_text = N'Success'

  END TRY
  BEGIN CATCH
    SET @status_code = 4
    SET @status_text = N'Error:'
                     + N' ERROR_NUMBER: '    + CAST(ERROR_NUMBER()               AS NVARCHAR)
                     + N' ERROR_SEVERITY: '  + CAST(ERROR_SEVERITY()             AS NVARCHAR)
                     + N' ERROR_STATE: '     + CAST(ERROR_STATE()                AS NVARCHAR)
                     + N' ERROR_PROCEDURE: ' + CAST(ISNULL(ERROR_PROCEDURE(),'') AS NVARCHAR)
                     + N' ERROR_LINE: '      + CAST(ERROR_LINE()                 AS NVARCHAR)
                     + N' ERROR_MESSAGE: '   + CAST(ERROR_MESSAGE()              AS NVARCHAR(MAX))
  END CATCH

END_PROCEDURE:
  SELECT @status_code AS StatusCode, @status_text AS StatusText, @spent AS SpentYesterday

END -- WSP_PlayerQuerySpentYesterday

GO

--------------------------------------------------------------------------------
-- PURPOSE : Places a mark in the account to indicate that the customer has already used the amount
--           spent in the previous day.
--
--  PARAMS :
--      - INPUT :
--          @pVendorID  NVARCHAR(16)
--          @pTrackData NVARCHAR(24)
--
--      - OUTPUT :
--          StatusCode
--          StatusText
--
-- RETURNS :
--
--   NOTES :
--      Possible values for StatusCode:
--        0: Success
--        1: Vendor Not Authorized
--        2: Account Not Found
--        3: Account Blocked
--        4: Error

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WSP_PlayerResetSpentYesterday]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[WSP_PlayerResetSpentYesterday]
GO
CREATE PROCEDURE [dbo].[WSP_PlayerResetSpentYesterday]
    @pVendorID  NVARCHAR(16),
    @pTrackData NVARCHAR(24)
AS
BEGIN
  -- SET NOCOUNT ON added to prevent extra result sets from
  -- interfering with SELECT statements.
  SET NOCOUNT ON;

  DECLARE @status_code      AS INT
  DECLARE @status_text      AS NVARCHAR(256)
  DECLARE @status_code_aux  AS INT
  DECLARE @status_text_aux  AS NVARCHAR(256)  

  DECLARE @account_id       AS BIGINT
  DECLARE @site_id          AS INT
  DECLARE @today            AS DATETIME
  DECLARE @yesterday        AS DATETIME
  DECLARE @yesterday_day    AS DATETIME
  DECLARE @rc               AS INT

  SET @status_code = 4
  SET @status_text = N'Error'

  BEGIN TRY

    -- Select @account_id
    SELECT  @account_id   = AC_ACCOUNT_ID
    FROM    ACCOUNTS
    WHERE   AC_TRACK_DATA = dbo.TrackDataToInternal(@pTrackData)

    SET @account_id = ISNULL(@account_id, 0)

    -- Check conditions
    EXEC dbo.WSP_CheckConditions @pVendorID, @account_id, 0, @status_code_aux OUTPUT, @status_text_aux OUTPUT

    IF @status_code_aux <> 0
    BEGIN
      SET @status_code = @status_code_aux
      SET @status_text = @status_text_aux

      GOTO END_PROCEDURE
    END

    -- Get yesterday 'day'
    SET @site_id = ISNULL ( ( SELECT   CAST (GP_KEY_VALUE AS INT)
                                FROM   GENERAL_PARAMS
                               WHERE   GP_GROUP_KEY   = 'Site'
                                 AND   GP_SUBJECT_KEY = 'Identifier' ), 0 )
    SET @today = dbo.TodayOpening(@site_id)
    SET @yesterday = DATEADD(DAY, -1, @today)
    -- Trunc date to start of the day (00:00h).
    SET @yesterday_day = DATEADD(DAY, DATEDIFF(day, 0, @yesterday), 0)

    UPDATE   WSP_PLAYER_SPENT_BY_DAY
       SET   WPS_USED       = 1
     WHERE   WPS_ACCOUNT_ID = @account_id
       AND   WPS_DAY        = @yesterday_day

    -- If no row is updated, don't raise an error.
    -- At this point, always return ok.

    SET @status_code = 0
    SET @status_text = N'Success'

  END TRY
  BEGIN CATCH
    SET @status_code = 4
    SET @status_text = N'Error:'
                     + N' ERROR_NUMBER: '    + CAST(ERROR_NUMBER()               AS NVARCHAR)
                     + N' ERROR_SEVERITY: '  + CAST(ERROR_SEVERITY()             AS NVARCHAR)
                     + N' ERROR_STATE: '     + CAST(ERROR_STATE()                AS NVARCHAR)
                     + N' ERROR_PROCEDURE: ' + CAST(ISNULL(ERROR_PROCEDURE(),'') AS NVARCHAR)
                     + N' ERROR_LINE: '      + CAST(ERROR_LINE()                 AS NVARCHAR)
                     + N' ERROR_MESSAGE: '   + CAST(ERROR_MESSAGE()              AS NVARCHAR(MAX))
  END CATCH

END_PROCEDURE:
  SELECT @status_code AS StatusCode, @status_text AS StatusText

END -- WSP_PlayerResetSpentYesterday

GO

--------------------------------------------------------------------------------
-- PURPOSE : Add the specified amount of non-redeemable credits to the specified user account.
--
--  PARAMS :
--      - INPUT :
--          @pVendorID  NVARCHAR(16)
--          @pTrackData NVARCHAR(24)
--          @pNRAmount  MONEY
--
--      - OUTPUT :
--          StatusCode
--          StatusText
--
-- RETURNS :
--
--   NOTES :
--      Possible values for StatusCode:
--        0: Success
--        1: Vendor Not Authorized
--        2: Account Not Found
--        3: Account Blocked
--        4: Error

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WSP_PlayerAddNonRedeemableAmount]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[WSP_PlayerAddNonRedeemableAmount]
GO
CREATE PROCEDURE [dbo].[WSP_PlayerAddNonRedeemableAmount]
    @pVendorID  NVARCHAR(16),
    @pTrackData NVARCHAR(24),
    @pNRAmount  MONEY
AS
BEGIN
  -- SET NOCOUNT ON added to prevent extra result sets from
  -- interfering with SELECT statements.
  SET NOCOUNT ON;

  DECLARE @status_code      AS INT
  DECLARE @status_text      AS NVARCHAR(256)
  DECLARE @status_code_aux  AS INT
  DECLARE @status_text_aux  AS NVARCHAR(256)  

  DECLARE @account_id       AS BIGINT
  DECLARE @promo_exists     AS BIT
  DECLARE @wcp_num_running  AS INT
  DECLARE @nr_amount        AS MONEY

  DECLARE @time_delay       AS VARCHAR(12)
  DECLARE @timeout          AS VARCHAR(8)
  DECLARE @is_timeout       AS BIT
  DECLARE @recharge_id      AS BIGINT
  DECLARE @start_time       AS DATETIME
  DECLARE @wpr_status       AS INT

  SET @status_code = 4
  SET @status_text = N'Error'

  BEGIN TRY

    -- Select @account_id
    SELECT  @account_id   = AC_ACCOUNT_ID
    FROM    ACCOUNTS
    WHERE   AC_TRACK_DATA = dbo.TrackDataToInternal(@pTrackData)

    SET @account_id = ISNULL(@account_id, 0)

    -- Check conditions
    EXEC dbo.WSP_CheckConditions @pVendorID, @account_id, 1, @status_code_aux OUTPUT, @status_text_aux OUTPUT

    IF @status_code_aux <> 0
    BEGIN
      SET @status_code = @status_code_aux
      SET @status_text = @status_text_aux

      GOTO END_PROCEDURE
    END     

    -- Check if received amount is rounded ok.
    --
    SET @nr_amount = ROUND(@pNRAmount, 2)   -- Currency: 2 decimals only!
    IF @nr_amount <> @pNRAmount
    BEGIN
      SET @status_code = 4
      SET @status_text = N'NR Amount must be 2 decimals rounded.'

      GOTO END_PROCEDURE
    END
    
    --
    -- Add the NR amount to the account
    --

    -- Check the EXTERNAL (type = 10) promotion exists
    --
    SELECT   @promo_exists = 1
      FROM   PROMOTIONS
     WHERE   PM_TYPE       = 10 -- EXTERNAL

    SET @promo_exists = ISNULL(@promo_exists, 0)
    IF @promo_exists = 0
    BEGIN
      SET @status_code = 4
      SET @status_text = N'Error: EXTERNAL Promotion does not exist.'

      GOTO END_PROCEDURE
    END


    SET @wpr_status = 1 --- Pending

    -- Insert the request to an intermediate table, so the WCP thread can add the NR amount.
    --
    INSERT INTO   WSP_PLAYER_RECHARGE
                ( WPR_ACCOUNT_ID
                , WPR_NR_AMOUNT
                , WPR_STATUS
                )
         VALUES
                ( @account_id
                , @nr_amount
                , @wpr_status
                )
    SET @recharge_id = SCOPE_IDENTITY()

    --  Wait for the answer from the WCP thread (using WPR_STATUS column)
    --
    SET @start_time = GETDATE()

    SET @time_delay = '00:00:00.500'
    SET @timeout = 30
    SET @is_timeout = 0

    WHILE @wpr_status = 1 --- While 'PENDING'
    BEGIN

      SELECT   @wpr_status   = WPR_STATUS
        FROM   WSP_PLAYER_RECHARGE
       WHERE   WPR_UNIQUE_ID = @recharge_id

      IF @wpr_status <> 1
        BREAK
         
      IF DATEDIFF(second, @start_time, GETDATE()) >= @timeout
      BEGIN
        SET @is_timeout = 1
        BREAK
      END

      WAITFOR DELAY @time_delay
    END

    DECLARE @tmp_table TABLE (RECHARGE_STATUS INT)

    DELETE   WSP_PLAYER_RECHARGE
    OUTPUT   DELETED.WPR_STATUS INTO @tmp_table
     WHERE   WPR_UNIQUE_ID = @recharge_id

    SELECT @wpr_status = RECHARGE_STATUS FROM @tmp_table

    SET @wpr_status = ISNULL(@wpr_status, 3)

    IF @wpr_status = 4  -- Ok
    BEGIN
      SET @status_code = 0
      SET @status_text = N'Success'
    END
    ELSE
    BEGIN
      --- ERRORS
      SET @status_code = 4
      SET @status_text = N'Unknown error. wpr_status: ' + CAST(@wpr_status AS NVARCHAR) + '.'
      IF @wpr_status = 3  -- Error
      BEGIN
        SET @status_text = N'Error processing the recharge.'
      END
      IF @wpr_status = 5 OR @is_timeout = 1 -- Timeout
      BEGIN
        SET @status_text = N'Timeout processing the recharge.'
      END
    END

  END TRY
  BEGIN CATCH
    SET @status_code = 4
    SET @status_text = N'Error:'
                     + N' ERROR_NUMBER: '    + CAST(ERROR_NUMBER()               AS NVARCHAR)
                     + N' ERROR_SEVERITY: '  + CAST(ERROR_SEVERITY()             AS NVARCHAR)
                     + N' ERROR_STATE: '     + CAST(ERROR_STATE()                AS NVARCHAR)
                     + N' ERROR_PROCEDURE: ' + CAST(ISNULL(ERROR_PROCEDURE(),'') AS NVARCHAR)
                     + N' ERROR_LINE: '      + CAST(ERROR_LINE()                 AS NVARCHAR)
                     + N' ERROR_MESSAGE: '   + CAST(ERROR_MESSAGE()              AS NVARCHAR(MAX))
  END CATCH

END_PROCEDURE:
  SELECT @status_code AS StatusCode, @status_text AS StatusText

END -- WSP_PlayerAddNonRedeemableAmount

GO

--
-- END WSP Interface
--
