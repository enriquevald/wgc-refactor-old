/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 435;

SET @New_ReleaseId = 436;

SET @New_ScriptName = N'UpdateTo_18.436.041.sql';
SET @New_Description = N'New release v03.007.0003'; 

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO



/**** GENERAL PARAM *****/

/**** TABLES *****/



/**** RECORDS *****/



/**** STORED PROCEDURES *****/
IF EXISTS (SELECT *
			FROM 	sys.objects 
			WHERE 	OBJECT_ID = OBJECT_ID(N'[dbo].[GetTablePlayersReport]') 
				AND type in (N'P', N'PC'))
BEGIN
  DROP PROCEDURE GetTablePlayersReport
END
GO

CREATE PROCEDURE GetTablePlayersReport
	 @pStart VARCHAR(MAX) 
	,@pEnd VARCHAR(MAX)
	,@pIsoCode VARCHAR(5) 
	,@pTableTypes VARCHAR(MAX) 
	,@pAccountNumber VARCHAR(MAX) 
	,@pTrackData VARCHAR(MAX)
	,@pHolderName VARCHAR(MAX)
	,@pIsVip BIT 
AS
BEGIN

DECLARE @SqlCmdA AS NVARCHAR(MAX)
DECLARE @SqlCmdB AS NVARCHAR(MAX)
DECLARE @SqlCmdC AS NVARCHAR(MAX)

DECLARE @ChipRE AS NVARCHAR(4)
DECLARE @ChipNRE AS NVARCHAR(4)
DECLARE @ChipColor AS NVARCHAR(4)

SET @ChipRE = '1001'
SET @ChipNRE = '1002'
SET @ChipColor = '1003'

if( @pEnd = NULL)
  SET @pEnd = ''

if( @pAccountNumber = NULL)
  SET @pAccountNumber = ''

if( @pTrackData = NULL)
  SET @pTrackData = ''

if( @pHolderName = NULL)
  SET @pHolderName = ''

SET @SqlCmdA = '
    SELECT 	   
	    ISNULL(SUM(GTPS_TOTAL_SELL_CHIPS),0) AS GTPS_BUY_IN  
	   ,ISNULL(SUM(GTPS_CHIPS_IN),0) AS GTPS_CHIPS_IN   
	   ,ISNULL(SUM(GTPS_NETWIN),0) AS GTPS_NETWIN 	  
	FROM  GT_PLAY_SESSIONS WITH( INDEX(IX_gtps_account_id_started))	  
	WHERE GTPS_FINISHED >= CAST('''+ @pStart +''' AS DATETIME)
	  AND GTPS_ISO_CODE IN ('''+ @pIsoCode +''')
'
IF(@pEnd <> '')
  SET @SqlCmdA += ' AND GTPS_FINISHED < CAST('''+@pEnd+''' AS DATETIME) '

SET @SqlCmdB = '
    SELECT 
	        ISNULL(SUM(GTSC_DROP_TABLES),0) + ISNULL(SUM(GTSC_DROP_CASHIER),0) GTSC_DROP
	       ,ISNULL(SUM(GTSC_WIN),0)                                            GTSC_WIN
	       
	
	FROM(
	SELECT 
	dbo.GT_Calculate_DROP_GAMBLING_TABLES(ISNULL(SUM(GTSC_COLLECTED_AMOUNT),0) 
										,SUM(ISNULL(CASE WHEN GTSC_TYPE = 0 THEN GTSC_COLLECTED_DROPBOX_AMOUNT
														WHEN GTSC_TYPE = 1 THEN GTSC_COLLECTED_DROPBOX_AMOUNT
											ELSE 0 END,  0))
										,SUM(ISNULL(CASE WHEN GTSC_TYPE = '+ @ChipRE +'    THEN GTSC_COLLECTED_DROPBOX_AMOUNT
														WHEN GTSC_TYPE =  '+ @ChipNRE +'   THEN GTSC_COLLECTED_DROPBOX_AMOUNT
														WHEN GTSC_TYPE =  '+ @ChipColor +' THEN GTSC_COLLECTED_DROPBOX_AMOUNT
																					ELSE 0 END,  0))
										,0
										,GT_HAS_INTEGRATED_CASHIER) AS GTSC_DROP_TABLES
    
	,DBO.GT_Calculate_DROP_CASHIER(SUM(ISNULL(GTSC_OWN_SALES_AMOUNT, 0))
                                  ,SUM(ISNULL(GTSC_EXTERNAL_SALES_AMOUNT, 0))) AS GTSC_DROP_CASHIER

	,dbo.GT_Calculate_WIN(SUM(ISNULL(GTSC_FINAL_CHIPS_AMOUNT   , 0))
										, SUM(ISNULL(GTSC_INITIAL_CHIPS_AMOUNT , 0))
										, SUM(ISNULL(GTSC_FILLS_CHIPS_AMOUNT   , 0))
										, SUM(ISNULL(GTSC_CREDITS_CHIPS_AMOUNT , 0))
										, SUM(ISNULL(GTSC_TIPS                 , 0))
										, SUM(ISNULL(GTSC_OWN_SALES_AMOUNT     , 0))
										, SUM(ISNULL(GTSC_EXTERNAL_SALES_AMOUNT, 0))
										, SUM(ISNULL(GTSC_COLLECTED_AMOUNT     , 0))
										, SUM(ISNULL(CASE WHEN GTSC_TYPE = 0 THEN GTSC_COLLECTED_DROPBOX_AMOUNT
														WHEN GTSC_TYPE = 1 THEN GTSC_COLLECTED_DROPBOX_AMOUNT
														ELSE 0 END,  0))
										, SUM(ISNULL(CASE WHEN GTSC_TYPE = '+ @ChipRE +'    THEN GTSC_COLLECTED_DROPBOX_AMOUNT
														  WHEN GTSC_TYPE = '+ @ChipNRE +'   THEN GTSC_COLLECTED_DROPBOX_AMOUNT
														  WHEN GTSC_TYPE = '+ @ChipColor +' THEN GTSC_COLLECTED_DROPBOX_AMOUNT
														ELSE 0 END,  0))
										, 0
										, GT_HAS_INTEGRATED_CASHIER) AS GTSC_WIN
	FROM   GAMING_TABLES_SESSIONS GTS
	LEFT   JOIN GAMING_TABLES_SESSIONS_BY_CURRENCY GTSC ON GTSC.GTSC_GAMING_TABLE_SESSION_ID = GTS.GTS_GAMING_TABLE_SESSION_ID
														AND GTSC_ISO_CODE IN ('''+ @pIsoCode +''')
	INNER   JOIN CASHIER_SESSIONS CS  ON GTS.GTS_CASHIER_SESSION_ID = CS.CS_SESSION_ID
									  AND CS_OPENING_DATE >=  CAST('''+ @pStart +''' AS DATETIME) '
IF(@pEnd <> '')
  SET @SqlCmdB +=	'							  AND CS_OPENING_DATE < CAST('''+@pEnd+''' AS DATETIME) '

SET @SqlCmdB +=	'INNER   JOIN GAMING_TABLES GT ON GTS.GTS_GAMING_TABLE_ID = GT.GT_GAMING_TABLE_ID
									 AND GT.GT_TYPE_ID IN ('+ @pTableTypes +')
	INNER   JOIN GAMING_TABLES_TYPES GTT ON GT.GT_TYPE_ID = GTT.GTT_GAMING_TABLE_TYPE_ID
	WHERE   CS_STATUS = 1  
	 GROUP BY CS_OPENING_DATE, GTT_GAMING_TABLE_TYPE_ID, GT_GAMING_TABLE_ID, GTT_NAME, GT_NAME
                      , GTS_OWN_SALES_AMOUNT, GTS_EXTERNAL_SALES_AMOUNT, GTS_COLLECTED_AMOUNT, GT_HAS_INTEGRATED_CASHIER
                      , GTS_COLLECTED_DROPBOX_AMOUNT, GTS_COLLECTED_DROPBOX_AMOUNT, GTS_DROPBOX_ENABLED
                      , GTS_COLLECTED_DROPBOX_CHIPS_AMOUNT, GT_THEORIC_HOLD, GTS_FINAL_CHIPS_AMOUNT, GTS_INITIAL_CHIPS_AMOUNT
                      , GTS_FILLS_CHIPS_AMOUNT, GTS_CREDITS_CHIPS_AMOUNT, GTSC_TYPE, GTS_TIPS, GTSC_TIPS, CS_CLOSING_DATE
                      , GTS_COPY_DEALER_VALIDATED_AMOUNT
    ) A
'

SET @SqlCmdC = '
  SELECT * FROM(
	SELECT 
		AC_ACCOUNT_ID
	   ,AC_HOLDER_NAME 
	   ,GT_NAME
	   ,GTPS_STARTED
	   ,GTPS_FINISHED
	   ,ISNULL(GTPS_TOTAL_SELL_CHIPS, 0) AS GTPS_BUY_IN  
	   ,ISNULL(GTPS_CHIPS_IN,0) AS GTPS_CHIPS_IN   
	   ,ISNULL(GTPS_NETWIN,0) AS GTPS_NETWIN   
	   ,CASE WHEN AC_HOLDER_NAME IS NOT NULL THEN 0 ELSE 1 END AC_IS_ANONYMOUS	
	   ,GTPS_PLAY_SESSION_ID
	FROM  GT_PLAY_SESSIONS WITH( INDEX(IX_gtps_account_id_started))
	INNER JOIN ACCOUNTS                                                         
	ON   GTPS_ACCOUNT_ID = AC_ACCOUNT_ID
	INNER JOIN   GAMING_TABLES                                                    
	ON   GTPS_GAMING_TABLE_ID = GT_GAMING_TABLE_ID  
	WHERE GTPS_FINISHED >= CAST('''+ @pStart +''' AS DATETIME)
	  AND GTPS_ISO_CODE IN ('''+ @pIsoCode +''')
	  AND GT_TYPE_ID IN ('+ @pTableTypes +')
	 
'
IF(@pEnd <> '')
  SET @SqlCmdC += ' AND GTPS_FINISHED < CAST('''+@pEnd+''' AS DATETIME) '

IF(@pAccountNumber <> '' )
  SET @SqlCmdC += ' AND AC_ACCOUNT_ID = '+ @pAccountNumber +' ' 

IF(@pTrackData <> '')
  SET @SqlCmdC += ' AND AC_TRACK_DATA = '''+ @pTrackData +''' '

IF(@pHolderName <> '')
  SET @SqlCmdC += ' AND AC_HOLDER_NAME LIKE ''%'+@pHolderName +'%'' '

IF(@pIsVip = 1)
  SET @SqlCmdC += ' AND AC_HOLDER_IS_VIP = 1 '

SET @SqlCmdC += ' ) A ORDER BY AC_IS_ANONYMOUS, AC_ACCOUNT_ID, GTPS_PLAY_SESSION_ID '

EXEC SP_EXECUTESQL @SqlCmdA
EXEC SP_EXECUTESQL @SqlCmdB
EXEC SP_EXECUTESQL @SqlCmdC 

END
GO

GRANT EXECUTE ON GetTablePlayersReport TO [wggui] WITH GRANT OPTION

IF  EXISTS (SELECT 1 
						FROM sys.objects 
						WHERE object_id = OBJECT_ID(N'[dbo].[GT_CALCULATE_CHIPS_RESULT]') 
							AND type in (N'FN'))
BEGIN 						
	DROP FUNCTION [dbo].[GT_CALCULATE_CHIPS_RESULT]
END 
GO
CREATE FUNCTION [dbo].[GT_CALCULATE_CHIPS_RESULT] 
	(
		@pChipsFinalAmount		AS MONEY
	, @pChipsInitialAmount	AS MONEY
	, @pChipsFillAmount			AS MONEY
	, @pChipsCreditAmount		AS MONEY
	, @pTipsAmount					AS MONEY
	) RETURNS MONEY
BEGIN
	DECLARE @chipsResult as Money
  
  SET @chipsResult = (@pChipsFinalAmount - @pChipsInitialAmount) + (@pChipsCreditAmount - @pChipsFillAmount) - @pTipsAmount
   
  RETURN @chipsResult
    
END
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/*
----------------------------------------------------------------------------------------------------------------
TABLE GAMES ANALYSIS REPORT

Version	Date					User	Description
----------------------------------------------------------------------------------------------------------------
1.0.0	 	29-SEP-2017		EOR		PBI 29907: WIGOS-4821 Table games analysis report

Requeriments:
   -- Functions:
         
Parameters:
   -- @pDate			
   -- @pTypeAcum 					
   -- @pTypeTable
	 -- @pIsoCode 
*/

IF  EXISTS (SELECT 1 
						FROM sys.objects 
						WHERE object_id = OBJECT_ID(N'[dbo].[GetGamingTableAnalisys]') 
							AND type in (N'P', N'PC'))
BEGIN 						
	DROP PROCEDURE [dbo].[GetGamingTableAnalisys]
END 
GO
CREATE PROCEDURE [dbo].[GetGamingTableAnalisys]
(
	@pDate				DATETIME,
	@pTypeAcum		BIT,
	@pTypeTables	VARCHAR(4096),
	@pIsoCode			VARCHAR(3)
)
AS 
BEGIN
	DECLARE @_ISO_CODE					VARCHAR(3)
	DECLARE @_CHIP_RE           INTEGER
  DECLARE @_CHIP_NRE          INTEGER
  DECLARE @_CHIP_COLOR        INTEGER
	DECLARE @_DELIMITER         CHAR(1)
	DECLARE @_DATE_INITIAL			DATETIME
	DECLARE @_DATE_FINAL				DATETIME
	DECLARE @_DATE_INITIAL_ACUM	DATETIME
	DECLARE @_DATE_FINAL_ACUM		DATETIME
	DECLARE @_TYPES_TABLE				TABLE(SST_ID		INT
																	, SST_VALUE VARCHAR(50)
																		)
	SET @_ISO_CODE			= @pIsoCode
	SET @_DELIMITER			= ','
	SET @_DATE_INITIAL	= @pDate
	SET @_DATE_FINAL		= DATEADD(DAY, 1, @_DATE_INITIAL)
  SET @_CHIP_RE       = 1001
  SET @_CHIP_NRE      = 1002
  SET @_CHIP_COLOR    = 1003	
	
	
	INSERT INTO @_TYPES_TABLE 
	SELECT * FROM dbo.SplitStringIntoTable(@pTypeTables, @_DELIMITER, DEFAULT)
	
	IF  @pTypeAcum = 0
	BEGIN
		SET @_DATE_INITIAL_ACUM = DATEADD(DAY, ((DAY(@_DATE_INITIAL) - 1) * -1), @_DATE_INITIAL) 
		SET @_DATE_FINAL_ACUM = DATEADD(MONTH, 1, @_DATE_INITIAL_ACUM) 
	END
	ELSE
	BEGIN
		SET @_DATE_INITIAL_ACUM = CAST(CAST(YEAR(@_DATE_INITIAL) AS CHAR) + '-01-01 ' + CONVERT(CHAR(8), @_DATE_INITIAL, 114) AS DATETIME)
		SET @_DATE_FINAL_ACUM = DATEADD(YEAR, 1, @_DATE_INITIAL_ACUM) 
	END
	
	SELECT	TB.TABLE_TYPE                                                                                                                                                               
				, TB.TABLE_TYPE_NAME                                                                                                                                                          
				,	TB.TABLE_ID                                                                                                                                                                 
				,	TB.TABLE_NAME                                                                                                                                                               
				,	SUM(TB.DROP_GAMBLING_TABLE)	DROP_GAMBLING_TABLE                                                                                                                             
				,	SUM(TB.DROP_CASHIER)	DROP_CASHIER                                                                                                                                          
				,	SUM(TB.DROP_TOTAL)	DROP_TOTAL                                                                                                                                              
				,	SUM(TB.CHIPS_RESULT)	CHIPS_RESULT                                                                                                                              
				,	SUM(TB.WIN) WIN                                                                                                                              
	INTO	#GAMING_TABLE_DAY			                                                                                                                                                        
	FROM (SELECT  GTT_GAMING_TABLE_TYPE_ID  AS TABLE_TYPE                                                                                                                               
						  , GTT_NAME  AS TABLE_TYPE_NAME                                                                                                                                          
					    ,	GT.GT_GAMING_TABLE_ID AS TABLE_ID                                                                                                                                     
						  , GT.GT_NAME  AS TABLE_NAME                                                                                                                                             
							, DBO.GT_CALCULATE_DROP_GAMBLING_TABLES(SUM(ISNULL(GTSC_COLLECTED_AMOUNT, 0))
                                                    , SUM(ISNULL(CASE WHEN GTSC_TYPE = 0	THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                      WHEN GTSC_TYPE = 1  THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                      ELSE 0 END, 0))
                                                    , SUM(ISNULL(CASE WHEN GTSC_TYPE = @_CHIP_RE		THEN GTSC_COLLECTED_DROPBOX_AMOUNT
																																			WHEN GTSC_TYPE = @_CHIP_NRE		THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                      WHEN GTSC_TYPE = @_CHIP_COLOR THEN GTSC_COLLECTED_DROPBOX_AMOUNT
																																			ELSE 0 END, 0))
																										, 0 -- It does not allow takings of dropbox with tickets
                                                    , GT_HAS_INTEGRATED_CASHIER)  AS DROP_GAMBLING_TABLE                                                                              
				      , DBO.GT_CALCULATE_DROP_CASHIER(SUM(ISNULL(GTSC_OWN_SALES_AMOUNT, 0))
                                            , SUM(ISNULL(GTSC_EXTERNAL_SALES_AMOUNT, 0))) AS DROP_CASHIER                                                                                   
							, DBO.GT_CALCULATE_DROP_GAMBLING_TABLES(SUM(ISNULL(GTSC_COLLECTED_AMOUNT, 0))
                                                    , SUM(ISNULL(CASE WHEN GTSC_TYPE = 0	THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                      WHEN GTSC_TYPE = 1  THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                      ELSE 0 END, 0))
                                                    , SUM(ISNULL(CASE WHEN GTSC_TYPE = @_CHIP_RE		THEN GTSC_COLLECTED_DROPBOX_AMOUNT
																																			WHEN GTSC_TYPE = @_CHIP_NRE		THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                      WHEN GTSC_TYPE = @_CHIP_COLOR THEN GTSC_COLLECTED_DROPBOX_AMOUNT
																																			ELSE 0 END, 0))
																										, 0 -- It does not allow takings of dropbox with tickets
                                                    , GT_HAS_INTEGRATED_CASHIER)  +                                                                                                   
								DBO.GT_CALCULATE_DROP_CASHIER(SUM(ISNULL(GTSC_OWN_SALES_AMOUNT, 0))
                                            , SUM(ISNULL(GTSC_EXTERNAL_SALES_AMOUNT, 0))) AS DROP_TOTAL                                                                                     
							,	DBO.GT_CALCULATE_CHIPS_RESULT(SUM(ISNULL(GTSC_FINAL_CHIPS_AMOUNT, 0))
																	,	SUM(ISNULL(GTSC_INITIAL_CHIPS_AMOUNT, 0))
																	, SUM(ISNULL(GTSC_FILLS_CHIPS_AMOUNT, 0))
                                  , SUM(ISNULL(GTSC_CREDITS_CHIPS_AMOUNT, 0))
																	, SUM(ISNULL(GTSC_TIPS, 0)))	AS	CHIPS_RESULT
							, DBO.GT_CALCULATE_WIN(SUM(ISNULL(GTSC_FINAL_CHIPS_AMOUNT, 0))
																	,	SUM(ISNULL(GTSC_INITIAL_CHIPS_AMOUNT, 0))
																	, SUM(ISNULL(GTSC_FILLS_CHIPS_AMOUNT, 0))
                                  , SUM(ISNULL(GTSC_CREDITS_CHIPS_AMOUNT, 0))
																	, SUM(ISNULL(GTSC_TIPS, 0))
																	, SUM(ISNULL(GTSC_OWN_SALES_AMOUNT, 0))
																	, SUM(ISNULL(GTSC_EXTERNAL_SALES_AMOUNT, 0))
																	, SUM(ISNULL(GTSC_COLLECTED_AMOUNT, 0))
																	, SUM(ISNULL(CASE WHEN GTSC_TYPE = 0	THEN GTSC_COLLECTED_DROPBOX_AMOUNT
																										WHEN GTSC_TYPE = 1	THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                    ELSE 0 END, 0))
																	, SUM(ISNULL(CASE WHEN GTSC_TYPE = @_CHIP_RE    THEN GTSC_COLLECTED_DROPBOX_AMOUNT
																										WHEN GTSC_TYPE = @_CHIP_NRE   THEN GTSC_COLLECTED_DROPBOX_AMOUNT
																										WHEN GTSC_TYPE = @_CHIP_COLOR THEN GTSC_COLLECTED_DROPBOX_AMOUNT
																										ELSE 0 END, 0))
																	, 0 -- It does not allow takings of dropbox with tickets
																	, GT_HAS_INTEGRATED_CASHIER)	AS	WIN                                                                                          
				FROM  GAMING_TABLES_SESSIONS GTS                                                                                                                                              
				LEFT JOIN GAMING_TABLES_SESSIONS_BY_CURRENCY GTSC ON GTSC.GTSC_GAMING_TABLE_SESSION_ID = GTS.GTS_GAMING_TABLE_SESSION_ID AND GTSC.GTSC_ISO_CODE = @_ISO_CODE                                                      
				INNER JOIN CASHIER_SESSIONS CS  ON GTS.GTS_CASHIER_SESSION_ID = CS.CS_SESSION_ID AND CS_OPENING_DATE >= @_DATE_INITIAL AND CS_OPENING_DATE < @_DATE_FINAL                       
				INNER JOIN GAMING_TABLES GT ON GTS.GTS_GAMING_TABLE_ID = GT.GT_GAMING_TABLE_ID AND GT.GT_TYPE_ID IN (SELECT SST_VALUE FROM @_TYPES_TABLE)                                                                                                
				INNER JOIN GAMING_TABLES_TYPES GTT ON GT.GT_TYPE_ID = GTT.GTT_GAMING_TABLE_TYPE_ID                                                                                            
				WHERE CS_STATUS = 1
				GROUP BY GTT.GTT_GAMING_TABLE_TYPE_ID, GTT.GTT_NAME, GT.GT_GAMING_TABLE_ID, GT.GT_NAME, GT.GT_HAS_INTEGRATED_CASHIER, GTSC_TYPE
			) TB                                                                                                                                                                            
	GROUP BY TB.TABLE_TYPE,	TB.TABLE_TYPE_NAME, TB.TABLE_ID, TB.TABLE_NAME                                                                                                              
                                                                                                                                                                                    
                                                                                                                                                                                    
                                                                                                                                                                                    
                                                                                                                                                                                    
	SELECT	TB.TABLE_TYPE                                                                                                                                                               
				, TB.TABLE_TYPE_NAME                                                                                                                                                          
				,	TB.TABLE_ID                                                                                                                                                                 
				,	TB.TABLE_NAME                                                                                                                                                               
				,	SUM(TB.DROP_GAMBLING_TABLE)	DROP_GAMBLING_TABLE                                                                                                                             
				,	SUM(TB.DROP_CASHIER)	DROP_CASHIER                                                                                                                                          
				,	SUM(TB.DROP_TOTAL)	DROP_TOTAL                                                                                                                                              
				,	SUM(TB.CHIPS_RESULT)	CHIPS_RESULT                                                                                                                              
				,	SUM(TB.WIN) WIN                                                                                                                              
	INTO	#GAMING_TABLE_ACUM			                                                                                                                                                      
	FROM (SELECT  GTT_GAMING_TABLE_TYPE_ID  AS TABLE_TYPE                                                                                                                               
						  , GTT_NAME  AS TABLE_TYPE_NAME                                                                                                                                          
					    ,	GT.GT_GAMING_TABLE_ID AS TABLE_ID                                                                                                                                     
						  , GT.GT_NAME  AS TABLE_NAME                                                                                                                                             
							, DBO.GT_CALCULATE_DROP_GAMBLING_TABLES(SUM(ISNULL(GTSC_COLLECTED_AMOUNT, 0))
                                                    , SUM(ISNULL(CASE WHEN GTSC_TYPE = 0	THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                      WHEN GTSC_TYPE = 1  THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                      ELSE 0 END, 0))
                                                    , SUM(ISNULL(CASE WHEN GTSC_TYPE = @_CHIP_RE		THEN GTSC_COLLECTED_DROPBOX_AMOUNT
																																			WHEN GTSC_TYPE = @_CHIP_NRE		THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                      WHEN GTSC_TYPE = @_CHIP_COLOR THEN GTSC_COLLECTED_DROPBOX_AMOUNT
																																			ELSE 0 END, 0))
																										, 0 -- It does not allow takings of dropbox with tickets
                                                    , GT_HAS_INTEGRATED_CASHIER)  AS DROP_GAMBLING_TABLE                                                                              
				      , DBO.GT_CALCULATE_DROP_CASHIER(SUM(ISNULL(GTSC_OWN_SALES_AMOUNT, 0))
                                            , SUM(ISNULL(GTSC_EXTERNAL_SALES_AMOUNT, 0))) AS DROP_CASHIER                                                                                   
							, DBO.GT_CALCULATE_DROP_GAMBLING_TABLES(SUM(ISNULL(GTSC_COLLECTED_AMOUNT, 0))
                                                    , SUM(ISNULL(CASE WHEN GTSC_TYPE = 0	THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                      WHEN GTSC_TYPE = 1  THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                      ELSE 0 END, 0))
                                                    , SUM(ISNULL(CASE WHEN GTSC_TYPE = @_CHIP_RE		THEN GTSC_COLLECTED_DROPBOX_AMOUNT
																																			WHEN GTSC_TYPE = @_CHIP_NRE		THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                                      WHEN GTSC_TYPE = @_CHIP_COLOR THEN GTSC_COLLECTED_DROPBOX_AMOUNT
																																			ELSE 0 END, 0))
																										, 0 -- It does not allow takings of dropbox with tickets
                                                    , GT_HAS_INTEGRATED_CASHIER)  +                                                                                                   
								DBO.GT_CALCULATE_DROP_CASHIER(SUM(ISNULL(GTSC_OWN_SALES_AMOUNT, 0))
                                            , SUM(ISNULL(GTSC_EXTERNAL_SALES_AMOUNT, 0))) AS DROP_TOTAL                                                                                    
							,	DBO.GT_CALCULATE_CHIPS_RESULT(SUM(ISNULL(GTSC_FINAL_CHIPS_AMOUNT, 0))
																	,	SUM(ISNULL(GTSC_INITIAL_CHIPS_AMOUNT, 0))
																	, SUM(ISNULL(GTSC_FILLS_CHIPS_AMOUNT, 0))
                                  , SUM(ISNULL(GTSC_CREDITS_CHIPS_AMOUNT, 0))
																	, SUM(ISNULL(GTSC_TIPS, 0)))	AS	CHIPS_RESULT
							, DBO.GT_CALCULATE_WIN(SUM(ISNULL(GTSC_FINAL_CHIPS_AMOUNT, 0))
																	,	SUM(ISNULL(GTSC_INITIAL_CHIPS_AMOUNT, 0))
																	, SUM(ISNULL(GTSC_FILLS_CHIPS_AMOUNT, 0))
                                  , SUM(ISNULL(GTSC_CREDITS_CHIPS_AMOUNT, 0))
																	, SUM(ISNULL(GTSC_TIPS, 0))
																	, SUM(ISNULL(GTSC_OWN_SALES_AMOUNT, 0))
																	, SUM(ISNULL(GTSC_EXTERNAL_SALES_AMOUNT, 0))
																	, SUM(ISNULL(GTSC_COLLECTED_AMOUNT, 0))
																	, SUM(ISNULL(CASE WHEN GTSC_TYPE = 0	THEN GTSC_COLLECTED_DROPBOX_AMOUNT
																										WHEN GTSC_TYPE = 1	THEN GTSC_COLLECTED_DROPBOX_AMOUNT
                                                    ELSE 0 END, 0))
																	, SUM(ISNULL(CASE WHEN GTSC_TYPE = @_CHIP_RE    THEN GTSC_COLLECTED_DROPBOX_AMOUNT
																										WHEN GTSC_TYPE = @_CHIP_NRE   THEN GTSC_COLLECTED_DROPBOX_AMOUNT
																										WHEN GTSC_TYPE = @_CHIP_COLOR THEN GTSC_COLLECTED_DROPBOX_AMOUNT
																										ELSE 0 END, 0))
																	, 0 -- It does not allow takings of dropbox with tickets
																	, GT_HAS_INTEGRATED_CASHIER)	AS	WIN  							                                                                                          
				FROM  GAMING_TABLES_SESSIONS GTS                                                                                                                                              
				LEFT JOIN GAMING_TABLES_SESSIONS_BY_CURRENCY GTSC ON GTSC.GTSC_GAMING_TABLE_SESSION_ID = GTS.GTS_GAMING_TABLE_SESSION_ID AND GTSC.GTSC_ISO_CODE = @_ISO_CODE                                                                                                           
				INNER JOIN CASHIER_SESSIONS CS  ON GTS.GTS_CASHIER_SESSION_ID = CS.CS_SESSION_ID AND CS_OPENING_DATE >= @_DATE_INITIAL_ACUM AND CS_OPENING_DATE < @_DATE_FINAL_ACUM               
				INNER JOIN GAMING_TABLES GT ON GTS.GTS_GAMING_TABLE_ID = GT.GT_GAMING_TABLE_ID AND GT.GT_TYPE_ID IN (SELECT SST_VALUE FROM @_TYPES_TABLE)                                                                                                
				INNER JOIN GAMING_TABLES_TYPES GTT ON GT.GT_TYPE_ID = GTT.GTT_GAMING_TABLE_TYPE_ID                                                                                            
				WHERE CS_STATUS = 1
				GROUP BY GTT.GTT_GAMING_TABLE_TYPE_ID, GTT.GTT_NAME, GT.GT_GAMING_TABLE_ID, GT.GT_NAME, GT.GT_HAS_INTEGRATED_CASHIER, GTSC_TYPE
			) TB                                                                                                                                                                            
	GROUP BY TB.TABLE_TYPE,	TB.TABLE_TYPE_NAME, TB.TABLE_ID, TB.TABLE_NAME                                                                                                              
                                                                                                                                                                                    
                                                                                                                                                                                    
                                                                                                                                                                                    
                                                                                                                                                                                    
	SELECT	GTA.TABLE_TYPE                                                                                                                                                              
				,	GTA.TABLE_TYPE_NAME                                                                                                                                                         
				,	GTA.TABLE_ID                                                                                                                                                                
				,	GTA.TABLE_NAME                                                                                                                                                              
				,	ISNULL(GTD.DROP_GAMBLING_TABLE, 0)	DROP_GAMBLING_TABLE                                                                                                                     
				,	ISNULL(GTD.DROP_CASHIER, 0)	DROP_CASHIER                                                                                                                                    
				,	ISNULL(GTD.DROP_TOTAL, 0)	DROP_TOTAL                                                                                                                                        
				,	ISNULL(GTD.CHIPS_RESULT, 0)	CHIPS_RESULT                                                                                                                        
				,	ISNULL(GTD.WIN, 0)	WIN                                                                                                                                                     
				,	ISNULL(CASE GTD.DROP_TOTAL	
									WHEN 0 THEN 0	
									ELSE (GTD.WIN / GTD.DROP_TOTAL) * 100 END, 0) HOLD                                                                                
				,	ISNULL(GTA.WIN, 0)	WIN_ACUM                                                                                                                                                
				,	ISNULL(GTA.DROP_TOTAL, 0)	DROP_TOTAL_ACUM                                                                                                                                   
				, ISNULL(CASE GTA.DROP_TOTAL	WHEN 0 THEN 0	ELSE (GTA.WIN / GTA.DROP_TOTAL) * 100 END, 0) HOLD_WIN                                                                            
	FROM #GAMING_TABLE_ACUM GTA                                                                                                                                                         
	LEFT JOIN	#GAMING_TABLE_DAY GTD ON GTD.TABLE_TYPE = GTA.TABLE_TYPE AND GTD.TABLE_ID = GTA.TABLE_ID                                                                                  
	ORDER BY GTA.TABLE_TYPE, GTA.TABLE_NAME                                                                                                                                               
                                                                                                                                                                                    
                                                                                                                                                                                    
	DROP TABLE #GAMING_TABLE_DAY                                                                                                                                                        
	DROP TABLE #GAMING_TABLE_ACUM                                                                                                                                                       
END
GO
	GRANT EXECUTE ON [dbo].[GetGamingTableAnalisys] TO wggui WITH GRANT OPTION
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ScoreReport]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[ScoreReport]
GO

CREATE  PROCEDURE [dbo].[ScoreReport]
(
    @pDateFrom        AS DATETIME
  , @pCageFillerIn    AS INT
  , @pCageFillerOut   AS INT
  , @pChipRE          AS INT
  , @pSaleChips       AS INT
 )
AS
BEGIN
  DECLARE @old_time AS DATETIME
  DECLARE @DateTo   AS DATETIME
  DECLARE @Horas    AS NVARCHAR(1000)
  
  DECLARE @cols             AS NVARCHAR(MAX)
  DECLARE @query            AS NVARCHAR(MAX)
  DECLARE @NationalCurrency AS NVARCHAR(3)

  SET @old_time = '2007-01-01T07:00:00'
  SET @DateTo = DATEADD(DAY, 1, @pDateFrom)  
  SET @Horas = N'[0],[1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12],[13],[14],[15],[16],[17],[18],[19],[20],[21],[22],[23],[9999]'
  SET @NationalCurrency = (SELECT GP_KEY_VALUE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'RegionalOptions' AND GP_SUBJECT_KEY = 'CurrencyIsoCode')
  
  SELECT * 
    INTO #GROUPS_BY_SESSION 
    FROM SplitStringIntoTable(REPLACE(REPLACE(@Horas, '[', ''), ']', ''), ',', 1)

      SELECT   GTT_NAME                             AS 'TYPE_NAME'
             , GT_NAME                              AS 'TABLE_NAME'
             , CS_OPENING_DATE                      AS 'OPENING_DATE'
             , CS_CLOSING_DATE                      AS 'CLOSING_DATE'
             , GTS_GAMING_TABLE_SESSION_ID          AS 'GAMING_TABLE_SESSION_ID'
             , GTS_CASHIER_SESSION_ID               AS 'CASHIER_SESSION_ID'
             , CS_NAME                              AS 'SESION_NAME'
             , ISNULL(GTSC_INITIAL_CHIPS_AMOUNT, 0) AS 'INITIAL_BALANCE'
             , ISNULL(GTSC_FINAL_CHIPS_AMOUNT, 0)   AS 'FINAL_BALANCE'
             , CAST(GT_NUM_SEATS AS DECIMAL(18,2))  AS 'NUM_SEATS_IN_TABLE'
             , CAST(SST_VALUE AS INT)               AS 'HORA'
             , SST_VALUE                            AS 'HORA_STR'
             , CAST(0 AS DECIMAL(18,2))             AS 'GT_DROP'
             , CAST(0 AS DECIMAL(18,2))             AS 'GT_WIN_LOSS'
             , CAST(0 AS DECIMAL(18,2))             AS 'GT_HOLD'
             , CAST(0 AS DECIMAL(18,2))             AS 'GT_OCCUPATION_NUMBER'
        INTO   #VALUE_TABLE
        FROM   GAMING_TABLES
  INNER JOIN   GAMING_TABLES_TYPES                ON GT_TYPE_ID                    = GTT_GAMING_TABLE_TYPE_ID
  INNER JOIN   GAMING_TABLES_SESSIONS             ON GT_GAMING_TABLE_ID            = GTS_GAMING_TABLE_ID
   LEFT JOIN   GAMING_TABLES_SESSIONS_BY_CURRENCY ON GTSC_GAMING_TABLE_SESSION_ID  = GTS_GAMING_TABLE_SESSION_ID AND GTSC_TYPE = @pChipRE AND GTSC_ISO_CODE = @NATIONALCURRENCY
  INNER JOIN   CASHIER_SESSIONS                   ON GTS_CASHIER_SESSION_ID        = CS_SESSION_ID 
 CROSS APPLY   #GROUPS_BY_SESSION
       WHERE   CS_OPENING_DATE >= @pDateFrom
         AND   CS_OPENING_DATE <  @DateTo         
    ORDER BY   GTT_NAME                 
             , GT_NAME                  
             , CS_OPENING_DATE          
             , CAST(SST_VALUE AS INT)                                            

     --  SET DROP BY HOUR - TICKETS  
      UPDATE   #VALUE_TABLE
         SET   GT_DROP = GT_DROP + ISNULL((SELECT   SUM(TI_AMOUNT)
                                             FROM   GT_COPY_DEALER_VALIDATED 
                                       INNER JOIN   TICKETS  ON GTCD_TICKET_ID = TI_TICKET_ID
                                            WHERE   GAMING_TABLE_SESSION_ID = GTCD_GAMING_TABLE_SESSION_ID 
                                              AND   DATEADD(HOUR, DATEDIFF(HOUR, @old_time, GTCD_VALIDATION_DATETIME), @old_time) = DATEADD(HOUR, HORA,  @pDateFrom)), 0)
 
     --  SET DROP BY HOUR - AMOUNTS
      UPDATE   #VALUE_TABLE
         SET   GT_DROP = GT_DROP + ISNULL((SELECT   SUM(CASE WHEN GTPM_ISO_CODE <> @NationalCurrency THEN dbo.ApplyExchange2(GTPM_VALUE, GTPM_ISO_CODE, @NationalCurrency) ELSE  GTPM_VALUE END)
                                             FROM   GT_PLAYERTRACKING_MOVEMENTS 
                                            WHERE   GAMING_TABLE_SESSION_ID = GTPM_GAMING_TABLE_SESSION_ID 
                                              AND   DATEADD(HOUR, DATEDIFF(HOUR, @old_time, GTPM_DATETIME), @old_time) = DATEADD(HOUR, HORA,  @pDateFrom)
                                              AND   GTPM_TYPE = @pSaleChips), 0)                                                                                           
                                              
      --  SET WIN/LOSS WITH CURSORS      
      DECLARE @GT_SESSION_ID BIGINT
      DECLARE GT_SESSION_WIN_LOSS_CURSOR CURSOR GLOBAL
      FOR SELECT GAMING_TABLE_SESSION_ID
            FROM #VALUE_TABLE 
        GROUP BY GAMING_TABLE_SESSION_ID
                                                            
      OPEN GT_SESSION_WIN_LOSS_CURSOR
      FETCH NEXT FROM GT_SESSION_WIN_LOSS_CURSOR INTO @GT_SESSION_ID
      
      -- FIRST CURSOR: ITERANCE THROUGH ALL SESSIONS OF #VALUE_TABLE
      WHILE(@@FETCH_STATUS = 0)
        BEGIN        
        
          DECLARE @WIN_LOSS_HOUR INT
          DECLARE @WIN_LOSS_LAST_AMOUNT MONEY
          DECLARE @WIN_LOSS_CURRENT_AMOUNT MONEY
          DECLARE @WIN_LOSS_FINAL_AMOUNT MONEY
          DECLARE @WIN_LOSS_CLOSING_HOUR AS INT
          DECLARE @WIN_LOSS_CLOSING_TABLE_AMOUNT MONEY
          DECLARE @CREDITS_SUB_FILLS MONEY     
          DECLARE @DROP_CURRENT_AMOUNT MONEY		
            
          DECLARE HOUR_WIN_LOSS_CURSOR CURSOR GLOBAL
          FOR SELECT HORA
                FROM #VALUE_TABLE
               WHERE GAMING_TABLE_SESSION_ID = @GT_SESSION_ID              
                                                            
          OPEN HOUR_WIN_LOSS_CURSOR
          FETCH NEXT FROM HOUR_WIN_LOSS_CURSOR INTO @WIN_LOSS_HOUR     
                                      
          -- WE HAVE THE HOUR THE TABLE WAS CLOSED                                                            
          SET @WIN_LOSS_CLOSING_HOUR = (SELECT DATEPART(HOUR, CLOSING_DATE)
                                          FROM #VALUE_TABLE
                                         WHERE GAMING_TABLE_SESSION_ID = @GT_SESSION_ID
                                      GROUP BY CLOSING_DATE)  
                                      
          SET @WIN_LOSS_FINAL_AMOUNT = 0
          SET @WIN_LOSS_CLOSING_TABLE_AMOUNT = (SELECT FINAL_BALANCE
                                                  FROM #VALUE_TABLE
                                                 WHERE GAMING_TABLE_SESSION_ID = @GT_SESSION_ID
                                              GROUP BY FINAL_BALANCE)                 
                                                          
          -- SECOND CURSOR: ITERANCE THE 24 HOURS OF THE PREVIOUS CURSOR SESSION
          WHILE(@@FETCH_STATUS = 0)
            BEGIN
              
              IF @WIN_LOSS_HOUR <> 9999
              BEGIN
                -- GET CURRENT WIN LOSS AMOUNT BY THE CURRENT TIME ITERANCE              
                SET @WIN_LOSS_CURRENT_AMOUNT = (SELECT ISNULL(GTWL_WIN_LOSS_AMOUNT, 0)
                                                  FROM GAMING_TABLES_WIN_LOSS
                                                 WHERE GTWL_GAMING_TABLE_SESSION_ID = @GT_SESSION_ID AND GTWL_DATETIME_HOUR = DATEADD(HOUR, @WIN_LOSS_HOUR, @pDateFrom))
                
                SET @CREDITS_SUB_FILLS = (SELECT SUM(CASE WHEN CM_CURRENCY_ISO_CODE <> @NationalCurrency THEN dbo.ApplyExchange2(CM_SUB_AMOUNT, CM_CURRENCY_ISO_CODE, @NationalCurrency) ELSE  CM_SUB_AMOUNT END -
                                                     CASE WHEN CM_CURRENCY_ISO_CODE <> @NationalCurrency THEN dbo.ApplyExchange2(CM_ADD_AMOUNT, CM_CURRENCY_ISO_CODE, @NationalCurrency) ELSE  CM_ADD_AMOUNT END)
                                            FROM CASHIER_MOVEMENTS
                                           WHERE CM_DATE BETWEEN DATEADD(HOUR, @WIN_LOSS_HOUR, @pDateFrom) AND DATEADD(HOUR, @WIN_LOSS_HOUR + 1, @pDateFrom)
                                             AND CM_TYPE IN (@pCageFillerIn, @pCageFillerOut)
                                             AND CM_CAGE_CURRENCY_TYPE = @pChipRE
                                             AND CM_SESSION_ID = (SELECT CASHIER_SESSION_ID
                                                                    FROM #VALUE_TABLE
                                                                   WHERE GAMING_TABLE_SESSION_ID = @GT_SESSION_ID
                                                                GROUP BY CASHIER_SESSION_ID))                                           
                
                IF DATEPART(HOUR, DATEADD(HOUR, @WIN_LOSS_HOUR,  @pDateFrom)) <= @WIN_LOSS_CLOSING_HOUR
                BEGIN
                  UPDATE #VALUE_TABLE
                     SET GT_WIN_LOSS = ISNULL(@WIN_LOSS_CURRENT_AMOUNT, 0) - ISNULL(@WIN_LOSS_LAST_AMOUNT, 0) + ISNULL(@CREDITS_SUB_FILLS, 0) + GT_DROP, @DROP_CURRENT_AMOUNT = GT_DROP
                   WHERE GAMING_TABLE_SESSION_ID = @GT_SESSION_ID AND HORA = @WIN_LOSS_HOUR                              
                END                
                
                -- IF THE ITERATED TIME IS EQUAL TO THE CLOSURE OF THE GAMBLING TABLE WE SET THE VALUE OF WIN LOSS
                IF DATEPART(HOUR, DATEADD(HOUR, @WIN_LOSS_HOUR,  @pDateFrom)) = @WIN_LOSS_CLOSING_HOUR
                BEGIN
                  SET @WIN_LOSS_FINAL_AMOUNT = ISNULL(@WIN_LOSS_CURRENT_AMOUNT, 0) - ISNULL(@WIN_LOSS_LAST_AMOUNT, 0) + ISNULL(@CREDITS_SUB_FILLS, 0) + ISNULL(@WIN_LOSS_CLOSING_TABLE_AMOUNT, 0)
                END
                
                -- UPDATE WIN LOSS LAST AMOUNT WITH CURRENT WIN LOSS AMOUNT
                IF DATEADD(HOUR, @WIN_LOSS_HOUR,  @pDateFrom) <= GETDATE()
                BEGIN
                  SET @WIN_LOSS_LAST_AMOUNT = ISNULL(@WIN_LOSS_CURRENT_AMOUNT, 0)
                END
                ELSE
                BEGIN
                  SET @WIN_LOSS_LAST_AMOUNT = 0
                END
              END
                           
              FETCH NEXT FROM HOUR_WIN_LOSS_CURSOR INTO @WIN_LOSS_HOUR                         
            
            END       
            CLOSE HOUR_WIN_LOSS_CURSOR
            DEALLOCATE HOUR_WIN_LOSS_CURSOR
  
        FETCH NEXT FROM GT_SESSION_WIN_LOSS_CURSOR INTO @GT_SESSION_ID
      END
      CLOSE GT_SESSION_WIN_LOSS_CURSOR
      DEALLOCATE GT_SESSION_WIN_LOSS_CURSOR                   

      UPDATE #VALUE_TABLE
          SET GT_WIN_LOSS = (SELECT ISNULL(SUM(CASE WHEN CM_TYPE = 191 THEN ISNULL(CM_SUB_AMOUNT, 0)
                                               WHEN CM_TYPE = 201 THEN ISNULL(CM_FINAL_BALANCE, 0)
                                               END ), 0)
                               FROM CASHIER_MOVEMENTS
                              WHERE CM_SESSION_ID = CASHIER_SESSION_ID
                                AND cm_type IN (191, 201)
                                AND cm_cage_currency_type = 1001)
        WHERE HORA = 9999

      --  SET HOLD BY HOUR    
      UPDATE   #VALUE_TABLE
         SET   GT_HOLD = A.GT_DROP / A.GT_WIN_LOSS * 100
        FROM   #VALUE_TABLE AS A
       WHERE   GAMING_TABLE_SESSION_ID = A.GAMING_TABLE_SESSION_ID
         AND   HORA = A.HORA
         AND   A.GT_WIN_LOSS <> 0
 
      --  SET OCCUPATION BY HOUR 
      UPDATE   #VALUE_TABLE
         SET   GT_OCCUPATION_NUMBER = (SELECT   CAST(COUNT(DISTINCT GTPS_SEAT_ID) AS DECIMAL(18,2))
                                  FROM   GT_PLAY_SESSIONS 
                                 WHERE   GAMING_TABLE_SESSION_ID = GTPS_GAMING_TABLE_SESSION_ID 
                                   AND ( GTPS_STARTED <= DATEADD(HOUR, HORA,  @pDateFrom) 
                                   AND   ISNULL(GTPS_FINISHED, GETDATE()) > DATEADD(HOUR, HORA,  @pDateFrom)
                                    OR   GTPS_STARTED >= DATEADD(HOUR, HORA,  @pDateFrom)
                                   AND   GTPS_STARTED < DATEADD(HOUR, 1, DATEADD(HOUR, HORA,  @pDateFrom))))
       WHERE  NUM_SEATS_IN_TABLE <> 0
 
      --  SET OCCUPATION BY HOUR -- TOTAL --
      UPDATE   #VALUE_TABLE
         SET   GT_OCCUPATION_NUMBER = ISNULL((SELECT   SUM(A.GT_OCCUPATION_NUMBER) / CAST(SUM(CASE WHEN A.GT_OCCUPATION_NUMBER > 0 THEN 1 ELSE 0 END)  AS DECIMAL(18,2))
                                  FROM   #VALUE_TABLE AS A
                                 WHERE   #VALUE_TABLE.GAMING_TABLE_SESSION_ID = A.GAMING_TABLE_SESSION_ID 
                                   AND   A.GT_OCCUPATION_NUMBER > 0), 0)
       WHERE  HORA = 9999
       
      -- SET HOUR = 0 WHEN WIN/LOSS IS NOT ENTER 
      DECLARE @Table_Name AS NVARCHAR(50)
      DECLARE @Hour       AS DATETIME
      
      DECLARE BLANK_HOUR_CURSOR CURSOR GLOBAL
        FOR SELECT   GT_NAME
                   , GTWL_DATETIME_HOUR
              FROM   GAMING_TABLES_WIN_LOSS
        INNER JOIN   GAMING_TABLES_SESSIONS ON GTS_GAMING_TABLE_SESSION_ID = GTWL_GAMING_TABLE_SESSION_ID
        INNER JOIN   GAMING_TABLES  ON GT_GAMING_TABLE_ID = GTS_GAMING_TABLE_ID
             WHERE   GTWL_DATETIME_HOUR >= @pDateFrom
               AND   GTWL_DATETIME_HOUR <= DATEADD(DAY, 1, @pDateFrom)
          GROUP BY   GT_NAME, GTWL_DATETIME_HOUR
            HAVING   MAX(GTWL_WIN_LOSS_AMOUNT) IS NULL
                                                            
      OPEN BLANK_HOUR_CURSOR
      FETCH NEXT FROM BLANK_HOUR_CURSOR INTO @Table_Name, @Hour     
      WHILE(@@FETCH_STATUS = 0)
        BEGIN
          UPDATE   #VALUE_TABLE
             SET   GT_DROP = 0
                 , GT_WIN_LOSS = 0
                 , GT_HOLD = 0
                 , GT_OCCUPATION_NUMBER = 0
           WHERE   TABLE_NAME = @Table_Name
             AND   DATEADD(HOUR, HORA,  @pDateFrom) = @Hour
        
          FETCH NEXT FROM BLANK_HOUR_CURSOR INTO @Table_Name, @Hour                          
        END       
      CLOSE BLANK_HOUR_CURSOR
      DEALLOCATE BLANK_HOUR_CURSOR           
            
            
      --PIVOT COLUMS FOR REPORT
      SELECT   @cols = STUFF((SELECT ',' + QUOTENAME( CASE WHEN HORA < 25 
                                                           THEN REPLACE(LEFT(RIGHT(CONVERT(NVARCHAR(20), DATEADD(HOUR, HORA, @pDateFrom), 120), 8), 5), ':', '') 
                                                           ELSE '9999'
                                                           END   +'-'+COL) 
        FROM   #VALUE_TABLE t
 CROSS APPLY ( SELECT 'GT_DROP', 1 UNION ALL
               SELECT 'GT_WIN_LOSS', 2 UNION ALL
               SELECT 'GT_HOLD', 3 UNION ALL
               SELECT 'GT_OCCUPATION_NUMBER', 4
             ) C (COL, SO)
    GROUP BY   COL
             , SO
             , HORA
    ORDER BY   HORA
             , SO FOR XML PATH('')
             , TYPE ).value('.', 'NVARCHAR(MAX)'),1,1,'')


set @query = 'SELECT   TYPE_NAME
                     , TABLE_NAME
                     , OPENING_DATE
                     , CLOSING_DATE
                     , GAMING_TABLE_SESSION_ID
                     , SESION_NAME
                     , INITIAL_BALANCE
                     , FINAL_BALANCE
                     , NUM_SEATS_IN_TABLE
                     , ' + @cols + ' 
                FROM ( SELECT   TYPE_NAME
                              , TABLE_NAME
                              , OPENING_DATE
                              , CLOSING_DATE
                              , GAMING_TABLE_SESSION_ID
                              , SESION_NAME
                              , INITIAL_BALANCE
                              , FINAL_BALANCE
                              , NUM_SEATS_IN_TABLE
                              , COL = CASE WHEN HORA < 25 
                                           THEN REPLACE( LEFT(  RIGHT(CONVERT(NVARCHAR(20), DATEADD(HOUR, HORA, ''' + CONVERT(NVARCHAR(20), @pDateFrom, 120) + '''), 120), 8), 5), '':'', '''' )
                                           ELSE ''9999'' 
                                           END  +''-''+COL
                              , VALUE
                         FROM   #VALUE_TABLE T
                  CROSS APPLY ( SELECT   ''GT_DROP''
                                       , GT_DROP UNION ALL
                                SELECT   ''GT_WIN_LOSS''
                                       , GT_WIN_LOSS UNION ALL
                                SELECT   ''GT_HOLD''
                                       , GT_HOLD UNION ALL
                                SELECT   ''GT_OCCUPATION_NUMBER''
                                       , GT_OCCUPATION_NUMBER
                              ) C (COL, VALUE)
                     ) X
               PIVOT ( MAX(VALUE) FOR COL IN (' + @cols + ') ) PVT '


  EXECUTE SP_EXECUTESQL @query

  DROP TABLE #GROUPS_BY_SESSION
  
END --ScoreReport
GO

GRANT EXECUTE ON [dbo].[ScoreReport] TO [wggui] WITH GRANT OPTION
GO

/******* TRIGGERS *******/
