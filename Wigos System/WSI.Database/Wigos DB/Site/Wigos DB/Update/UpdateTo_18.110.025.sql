/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 109;

SET @New_ReleaseId = 110;
SET @New_ScriptName = N'UpdateTo_18.110.025.sql';
SET @New_Description = N'Operations Schedule and MultiSite DB elements.';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

ALTER PROCEDURE [dbo].[Update_PointsInAccount]
      @pMovementId                BIGINT
,     @pAccountId                 BIGINT
, @pErrorCode                 INT         
, @pPointsSequenceId          BIGINT      
, @pReceivedPoints            MONEY       
, @pPointsStatus              INT           
, @pHolderLevel               INT         
, @pHolderLevelEntered        DATETIME    
, @pHolderLevelExpiration     DATETIME    

AS
BEGIN   
  DECLARE @LocalDelta AS MONEY

  IF NOT EXISTS (SELECT 1 FROM ACCOUNTS WHERE ac_account_id = @pAccountId )   
    INSERT INTO   ACCOUNTS 
                ( AC_ACCOUNT_ID, AC_TYPE, AC_BLOCKED, AC_TRACK_DATA) 
         VALUES ( @pAccountId,         2,          1, '-RECYCLED-NEW-' + CAST (NEWID() AS NVARCHAR(50)) )
         
  IF ((@pMovementId <> 0)AND(@pErrorCode = 0 OR @pErrorCode = 2))
    DELETE   MS_SITE_PENDING_ACCOUNT_MOVEMENTS
     WHERE   SPM_MOVEMENT_ID = @pMovementId
  
  IF (@pErrorCode <> 0) RETURN
  
  DECLARE @PrevSequence as BIGINT
  
  
  -- Lock the account and later add the local points
  UPDATE ACCOUNTS SET AC_POINTS = AC_POINTS + 0 WHERE AC_ACCOUNT_ID = @pAccountId

  SET @PrevSequence = (SELECT ISNULL(AC_MS_POINTS_SEQ_ID, 0) FROM ACCOUNTS WHERE AC_ACCOUNT_ID = @pAccountId)
  
  IF ( @PrevSequence >= @pPointsSequenceId ) RETURN
  
-- Local Points
  SELECT   @LocalDelta = SUM (am_add_amount - am_sub_amount) 
    FROM   ACCOUNT_MOVEMENTS, MS_SITE_PENDING_ACCOUNT_MOVEMENTS
   WHERE   AM_MOVEMENT_ID = SPM_MOVEMENT_ID
     AND   AM_ACCOUNT_ID  = @pAccountId
     AND   AM_TYPE IN (36,37,38,39,40,41,42,46,50,60,61,66) --Not included level movements
  

      UPDATE   ACCOUNTS
           SET   AC_POINTS                  = @pReceivedPoints + ISNULL (@LocalDelta, 0)
               , AC_MS_POINTS_SEQ_ID        = @pPointsSequenceId
               , AC_MS_POINTS_SYNCHRONIZED  = GETDATE ()
           , AC_POINTS_STATUS           = @pPointsStatus
           , AC_HOLDER_LEVEL_EXPIRATION = @pHolderLevelExpiration   
           , AC_HOLDER_LEVEL_ENTERED    = @pHolderLevelEntered
           , AC_HOLDER_LEVEL            = @pHolderLevel
         WHERE   AC_ACCOUNT_ID              = @pAccountId
           AND   ISNULL(AC_MS_POINTS_SEQ_ID, 0) < @pPointsSequenceId
END

GO

