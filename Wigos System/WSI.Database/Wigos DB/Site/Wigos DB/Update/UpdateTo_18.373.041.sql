/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 372;

SET @New_ReleaseId = 373;
SET @New_ScriptName = N'UpdateTo_18.373.041.sql';
SET @New_Description = N'GetSpentGroupByTerminal'; 

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/**** GENERAL PARAM *****/

/******* TABLES  *******/

/******* INDEXES *******/

/******* RECORDS *******/

/******* PROCEDURES *******/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetSpentGroupByTerminal]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[GetSpentGroupByTerminal]
GO

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[GetSpentGroupByTerminal]  
       @pAccountId as BigInt
     , @pLowerLimitDatetime as DateTime
     , @pPlaySessionOpened as Int = 0
AS
BEGIN

  DECLARE @MaxUnbalanceInCents as decimal(19,3)
  DECLARE @MaxBetAmountAverageInCents as decimal(19,3)
  SET NOCOUNT ON;

  SELECT @MaxUnbalanceInCents = CAST(GP_KEY_VALUE AS decimal)/100
    FROM GENERAL_PARAMS 
   WHERE GP_GROUP_KEY = 'Cashier.DrawTicket' 
     AND GP_SUBJECT_KEY = 'ToGiveDrawNumbers.MaxUnbalanceInCents'

  SELECT @MaxBetAmountAverageInCents = CAST(GP_KEY_VALUE AS decimal)/100
    FROM GENERAL_PARAMS 
   WHERE GP_GROUP_KEY = 'Cashier.DrawTicket' 
     AND GP_SUBJECT_KEY = 'ToGiveDrawNumbers.MaxBetAmountAverageInCents'
                                                         
      SELECT   TE_PROVIDER_ID  
             , TE_TERMINAL_ID
             , DATEADD (HOUR, DATEDIFF (HOUR, @pLowerLimitDatetime, PS_STARTED),  @pLowerLimitDatetime) AS STARTED 
             , DATEADD (HOUR, DATEDIFF (HOUR, @pLowerLimitDatetime, PS_FINISHED), @pLowerLimitDatetime) AS FINISHED 
             , SUM (PS_PLAYED_AMOUNT * CASE ISNULL(ce_change, 0) WHEN 0 THEN 1 ELSE ce_change END ) AS TOTAL_PLAYED                                      
             , SUM (PS_REDEEMABLE_PLAYED * CASE ISNULL(ce_change, 0) WHEN 0 THEN 1 ELSE ce_change END ) AS REDEEMABLE_PLAYED                                 
             , SUM (CASE WHEN PS_PLAYED_AMOUNT     > PS_WON_AMOUNT     THEN PS_PLAYED_AMOUNT     - PS_WON_AMOUNT     ELSE 0 END * CASE ISNULL(CE_CHANGE, 0) WHEN 0 THEN 1 ELSE CE_CHANGE END ) AS TOTAL_SPENT                                       
             , SUM (CASE WHEN PS_REDEEMABLE_PLAYED > PS_REDEEMABLE_WON THEN PS_REDEEMABLE_PLAYED - PS_REDEEMABLE_WON ELSE 0 END * CASE ISNULL(CE_CHANGE, 0) WHEN 0 THEN 1 ELSE CE_CHANGE END ) AS REDEEMABLE_SPENT                                  
             , SUM (PS_INITIAL_BALANCE) AS BAL_INI
             , SUM (PS_FINAL_BALANCE) AS BAL_FIN
             , SUM (PS_WON_AMOUNT) AS WON_AMOUNT
             , SUM (PS_PLAYED_COUNT) AS NUM_PLAYS
             , ABS(PS_FINAL_BALANCE - (PS_INITIAL_BALANCE - PS_PLAYED_AMOUNT + PS_WON_AMOUNT))    AS UNBALANCE
             , CASE WHEN PS_PLAYED_COUNT = 0 THEN 0 ELSE ABS(PS_PLAYED_AMOUNT / PS_PLAYED_COUNT) END AS AVG_BET
             , @MaxUnbalanceInCents as a
        FROM   PLAY_SESSIONS                                         
  INNER JOIN   TERMINALS ON PS_TERMINAL_ID = TE_TERMINAL_ID 
   LEFT JOIN   CURRENCY_EXCHANGE ON TE_ISO_CODE = CE_CURRENCY_ISO_CODE AND CE_TYPE = 0 
       WHERE   PS_FINISHED   >= @pLowerLimitDatetime                 
         AND   PS_STATUS     <> @pPlaySessionOpened                  
         AND   PS_ACCOUNT_ID  = @pAccountId    
         AND   (ABS(PS_FINAL_BALANCE - (PS_INITIAL_BALANCE - PS_PLAYED_AMOUNT + PS_WON_AMOUNT)) < @MaxUnbalanceInCents OR @MaxUnbalanceInCents = 0)
         AND   (CASE WHEN PS_PLAYED_COUNT = 0 THEN 0 ELSE ABS(PS_PLAYED_AMOUNT / PS_PLAYED_COUNT) END < @MaxBetAmountAverageInCents OR @MaxBetAmountAverageInCents = 0)
    GROUP BY   TE_PROVIDER_ID
             , TE_TERMINAL_ID                           
             , DATEADD (HOUR, DATEDIFF (HOUR, @pLowerLimitDatetime, PS_STARTED),  @pLowerLimitDatetime)   
             , DATEADD (HOUR, DATEDIFF (HOUR, @pLowerLimitDatetime, PS_FINISHED), @pLowerLimitDatetime)   
             , ABS(PS_FINAL_BALANCE - (PS_INITIAL_BALANCE - PS_PLAYED_AMOUNT + PS_WON_AMOUNT))
             , CASE WHEN PS_PLAYED_COUNT = 0 THEN 0 ELSE ABS(PS_PLAYED_AMOUNT / PS_PLAYED_COUNT) END


      SELECT   PS_PLAY_SESSION_ID
             , PS_ACCOUNT_ID
             , TE_PROVIDER_ID  
             , TE_TERMINAL_ID
             , PS_STARTED
             , PS_FINISHED
             , PS_PLAYED_AMOUNT * CASE ISNULL(ce_change, 0) WHEN 0 THEN 1 ELSE ce_change END      AS TOTAL_PLAYED                                      
             , PS_REDEEMABLE_PLAYED * CASE ISNULL(ce_change, 0) WHEN 0 THEN 1 ELSE ce_change END  AS REDEEMABLE_PLAYED                                 
             , CASE WHEN PS_PLAYED_AMOUNT     > PS_WON_AMOUNT     THEN PS_PLAYED_AMOUNT     - PS_WON_AMOUNT     ELSE 0 END * CASE ISNULL(CE_CHANGE, 0) WHEN 0 THEN 1 ELSE CE_CHANGE END  AS TOTAL_SPENT                                       
             , CASE WHEN PS_REDEEMABLE_PLAYED > PS_REDEEMABLE_WON THEN PS_REDEEMABLE_PLAYED - PS_REDEEMABLE_WON ELSE 0 END * CASE ISNULL(CE_CHANGE, 0) WHEN 0 THEN 1 ELSE CE_CHANGE END  AS REDEEMABLE_SPENT                                  
             , PS_INITIAL_BALANCE
             , PS_FINAL_BALANCE
             , PS_WON_AMOUNT
             , PS_PLAYED_COUNT
             , ABS(PS_FINAL_BALANCE - (PS_INITIAL_BALANCE - PS_PLAYED_AMOUNT + PS_WON_AMOUNT))    AS UNBALANCE
             , CASE WHEN PS_PLAYED_COUNT = 0 THEN 0 ELSE ABS(PS_PLAYED_AMOUNT / PS_PLAYED_COUNT) END AS AVG_BET
        FROM   PLAY_SESSIONS                                         
  INNER JOIN   TERMINALS ON PS_TERMINAL_ID = TE_TERMINAL_ID 
   LEFT JOIN   CURRENCY_EXCHANGE ON TE_ISO_CODE = CE_CURRENCY_ISO_CODE AND CE_TYPE = 0 
       WHERE   PS_FINISHED   >= @pLowerLimitDatetime                 
         AND   PS_STATUS     <> @pPlaySessionOpened                  
         AND   PS_ACCOUNT_ID  = @pAccountId    
         AND   (  (ABS(PS_FINAL_BALANCE - (PS_INITIAL_BALANCE - PS_PLAYED_AMOUNT + PS_WON_AMOUNT)) >= @MaxUnbalanceInCents AND @MaxUnbalanceInCents > 0)
               OR (CASE WHEN PS_PLAYED_COUNT = 0 THEN 0 ELSE ABS(PS_PLAYED_AMOUNT / PS_PLAYED_COUNT) END >= @MaxBetAmountAverageInCents AND @MaxBetAmountAverageInCents > 0))
    ORDER BY   TE_PROVIDER_ID
             , TE_TERMINAL_ID                           
             , DATEADD (HOUR, DATEDIFF (HOUR, @pLowerLimitDatetime, PS_STARTED),  @pLowerLimitDatetime)   
             , DATEADD (HOUR, DATEDIFF (HOUR, @pLowerLimitDatetime, PS_FINISHED), @pLowerLimitDatetime)   

END
GO

GRANT EXECUTE ON [dbo].[GetSpentGroupByTerminal] TO [wggui] WITH GRANT OPTION
GO

/******* TRIGGERS *******/

