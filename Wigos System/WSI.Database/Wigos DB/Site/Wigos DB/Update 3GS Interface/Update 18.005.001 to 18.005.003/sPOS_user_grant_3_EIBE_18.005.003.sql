
USE [wgdb_000]
GO

/****** Object:  User [wg_interface]    Script Date: 05/10/2010 09:33:21 ******/
IF EXISTS (SELECT * FROM sys.server_principals WHERE name = N'EIBE' AND type in (N'S'))
BEGIN

IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'EIBE' AND type in (N'S'))
CREATE USER [EIBE] FOR LOGIN [EIBE] WITH DEFAULT_SCHEMA=[dbo];

GRANT EXECUTE ON [dbo].[zsp_AccountStatus] TO [EIBE] WITH GRANT OPTION;
GRANT EXECUTE ON [dbo].[zsp_SendEvent]     TO [EIBE] WITH GRANT OPTION;
GRANT EXECUTE ON [dbo].[zsp_SessionEnd]    TO [EIBE] WITH GRANT OPTION;
GRANT EXECUTE ON [dbo].[zsp_SessionStart]  TO [EIBE] WITH GRANT OPTION;
GRANT EXECUTE ON [dbo].[zsp_SessionUpdate] TO [EIBE] WITH GRANT OPTION;

EXEC sp_addrolemember 'db_denydatareader','EIBE';
EXEC sp_addrolemember 'db_denydatawriter','EIBE';

END
ELSE
/**** VERSION FAILURE SECTION *****/
SELECT 'Nothing to Update.'
GO