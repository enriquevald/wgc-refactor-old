USE [sPOS]
GO

/****** Object:  User [wginterface]    Script Date: 05/10/2010 09:33:21 ******/
CREATE USER [EIBE] FOR LOGIN [EIBE] WITH DEFAULT_SCHEMA=[dbo]
GO

GRANT EXECUTE ON [dbo].[zsp_AccountStatus] TO [EIBE] WITH GRANT OPTION
GO
GRANT EXECUTE ON [dbo].[zsp_SendEvent]     TO [EIBE] WITH GRANT OPTION
GO
GRANT EXECUTE ON [dbo].[zsp_SessionEnd]    TO [EIBE] WITH GRANT OPTION
GO
GRANT EXECUTE ON [dbo].[zsp_SessionStart]  TO [EIBE] WITH GRANT OPTION
GO
GRANT EXECUTE ON [dbo].[zsp_SessionUpdate] TO [EIBE] WITH GRANT OPTION
GO
sp_addrolemember 'db_denydatareader','EIBE' 
GO
sp_addrolemember 'db_denydatawriter','EIBE' 
GO