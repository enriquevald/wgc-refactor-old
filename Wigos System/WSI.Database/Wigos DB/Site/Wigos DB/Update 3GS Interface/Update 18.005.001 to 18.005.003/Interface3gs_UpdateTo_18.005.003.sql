USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 1;

SET @New_ReleaseId = 3;
SET @New_ScriptName = N'Interface3gs_UpdateTo_18.005.003.sql';
SET @New_Description = N'Won Count, Dummy SessionStart';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version_interface_3gs WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** UPDATE VERSION SECTION *****/
UPDATE db_version_interface_3gs
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;


--------------------------------------------------------------------------------
-- PURPOSE: Check Gaming Data from the Play Session
--          Also calculates the delta values: DeltaValue = ActualValue - PreviousValue
-- 
--  PARAMS:
--      - INPUT:
--        @SessionId      bigint
--        @CreditsPlayed  money,
--        @CreditsWon     money,
--        @GamesPlayed    int,
--        @GamesWon       int
--
--      - OUTPUT:
--
--   NOTES:
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CheckAndGetPlaySessionData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CheckAndGetPlaySessionData]
GO
CREATE PROCEDURE dbo.CheckAndGetPlaySessionData
 (@SessionId      bigint,
  @PlayedAmount   money,
  @WonAmount      money,
  @PlayedCount    int,
  @WonCount       int,
  @CreditBalance  money,
  @IsEndSession   int)
AS
BEGIN
  DECLARE @rc                   int
  DECLARE @status_code          int
	DECLARE @status_text          nvarchar (254)
  DECLARE @played_amount        money
  DECLARE @won_amount           money
  DECLARE @played_count         int
  DECLARE @won_count            int
  DECLARE @delta_played_amount  money
  DECLARE @delta_won_amount     money
  DECLARE @delta_played_count   int
  DECLARE @delta_won_count      int

  SET @status_code = 0
	SET @status_text = ''

  IF (  ( @PlayedAmount  < 0 )
     OR ( @WonAmount     < 0 )
     OR ( @PlayedCount   < 0 )
     OR ( @WonCount      < 0 )
     OR ( @CreditBalance < 0 ))
  BEGIN
    SET @status_code  = 3
    SET @status_text  = 'Invalid Session ID number'
    GOTO ERROR_PROCEDURE
  END

  SELECT @played_count = PS_PLAYED_COUNT
       , @played_amount = PS_PLAYED_AMOUNT
       , @won_count = PS_WON_COUNT
       , @won_amount = PS_WON_AMOUNT
  FROM PLAY_SESSIONS
  WHERE PS_PLAY_SESSION_ID = @SessionId

  SET @rc = @@ROWCOUNT
  IF ( @rc <> 1 )
  BEGIN
    SET @status_code  = 3
    SET @status_text  = 'Invalid Session ID number'
    GOTO ERROR_PROCEDURE
  END

  -- 04-AUG-2010 MBF: Patch for CADILLAC vendor
  IF ( ( @IsEndSession <> 0 ) AND ( @WonCount = 0 ) )
  BEGIN
    SET @WonCount = @won_count
  END
    
  IF ( ( @PlayedCount  < @played_count
      OR @PlayedAmount < @played_amount )
    OR ( @WonCount     < @won_count
      OR @WonAmount    < @won_amount ) )
  BEGIN
    -- report error
    SET @status_code  = 3
    SET @status_text  = 'Invalid Session ID number'
    GOTO ERROR_PROCEDURE
  END

  SET @delta_played_amount = @PlayedAmount - @played_amount
  SET @delta_won_amount    = @WonAmount    - @won_amount
  SET @delta_played_count  = @PlayedCount  - @played_count
  SET @delta_won_count     = @WonCount     - @won_count

ERROR_PROCEDURE:
	SELECT @status_code         AS StatusCode,        @status_text      AS StatusText
	     , @delta_played_amount AS DeltaPlayedAmount, @delta_won_amount AS DeltaWonAmount
	     , @delta_played_count  AS DeltaPlayedCount,  @delta_won_count  AS DeltaWonCount

END -- CheckAndGetPlaySessionData

GO

--------------------------------------------------------------------------------
-- PURPOSE: Update session (INTERNAL)
-- 
--  PARAMS:
--      - INPUT:
--          @AccountId      bigint
--          @TerminalId     int
--          @SessionId      bigint
--          @AmountPlayed   money
--          @AmountWon      money
--          @GamesPlayed    int
--          @GamesWon       int
--          @CreditBalance  money
--          @CurrentJackpot money = 0
--
--      - OUTPUT:
--
-- RETURNS:
--      StatusCode
--      StatusText
--
--   NOTES:
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SessionUpdate_Internal]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SessionUpdate_Internal]
GO
CREATE PROCEDURE dbo.SessionUpdate_Internal
  @AccountId         bigint,
  @TerminalId        int,
  @SerialNumber      varchar(30),
  @SessionId         bigint,
  @AmountPlayed      money,
  @AmountWon         money,
  @GamesPlayed       int,
  @GamesWon          int,
  @CreditBalance     money,
  @CurrentJackpot    money = 0,
  @IsEndSession      int = 0,
  @DeltaAmountPlayed money OUTPUT,
  @DeltaAmountWon    money OUTPUT,
  @status_code       int OUTPUT,
  @status_text       nvarchar (254) OUTPUT
AS
BEGIN
  -- SET NOCOUNT ON added to prevent extra result sets from
  -- interfering with SELECT statements.
  SET NOCOUNT ON;

  DECLARE @balance              money
  DECLARE @delta_played_amount  money
  DECLARE @delta_won_amount     money
  DECLARE @delta_played_count   int
  DECLARE @delta_won_count      int

  SET @status_code = 0
  SET @status_text = ''

  -- Check if terminal has session active
  -- Check if terminal session_id is equal to received terminal id
  -- Check if play session is Open
  IF ((SELECT dbo.CheckTerminalPlaySession(@TerminalId, @SessionId)) <> 1)
  BEGIN
    SET @status_code = 3
    SET @status_text = 'Invalid Session ID number'
    GOTO ERROR_PROCEDURE
  END	
  
    -- Check play session data and get delta values (actual - previous)
  DECLARE @update_cs_table TABLE
     (StatusCode int, StatusText nvarchar(254)
    , DeltaPlayedAmount money, DeltaWonAmount money
	  , DeltaPlayedCount int, DeltaWonCount int)

  INSERT INTO @update_cs_table
    EXECUTE dbo.CheckAndGetPlaySessionData @SessionId, @AmountPlayed, @AmountWon,
                                           @GamesPlayed, @GamesWon, @CreditBalance, @IsEndSession

  SELECT @status_code         = StatusCode
       , @status_text         = StatusText
       , @delta_played_amount = DeltaPlayedAmount
       , @delta_won_amount    = DeltaWonAmount
       , @delta_played_count  = DeltaPlayedCount
       , @delta_won_count     = DeltaWonCount
    FROM @update_cs_table

  IF ( @status_code <> 0 )
    GOTO ERROR_PROCEDURE

  -- Update account data!
  DECLARE @update_ac_table TABLE (StatusCode int, StatusText nvarchar(254), Balance money)
  INSERT INTO @update_ac_table
    EXECUTE dbo.UpdateAccountData @AccountId, @SessionId, @TerminalId, @delta_played_amount,
                                  @delta_won_amount
  SELECT @status_code = StatusCode
       , @status_text = StatusText
       , @balance     = Balance
    FROM @update_ac_table
  
  IF ( @status_code <> 0 )
    GOTO ERROR_PROCEDURE 

  -- REVISAR!!!
  IF ( @balance <> @CreditBalance )
  BEGIN
    SET @status_code = 5
    SET @status_text = 'Final balance does not match validation 501'
    GOTO ERROR_PROCEDURE
  END
    
  -- Update Game Meters
  DECLARE @update_gm_table TABLE (StatusCode int, StatusText nvarchar(254))
  INSERT INTO @update_gm_table
    EXECUTE dbo.UpdateGameMeters @TerminalId, @delta_played_count, @delta_played_amount,
                                 @delta_won_count, @delta_won_amount
  
  SELECT @status_code = StatusCode
       , @status_text = StatusText
    FROM @update_gm_table
    
  IF ( @status_code <> 0 )
    GOTO ERROR_PROCEDURE

  -- Update data play session
  DECLARE @update_se_table TABLE (StatusCode int, StatusText nvarchar(254))
  INSERT INTO @update_se_table
    EXECUTE dbo.UpdatePlaySessionData @SessionId, @AmountPlayed, @AmountWon, @GamesPlayed,
                                      @GamesWon, @CreditBalance
  SELECT @status_code = StatusCode
       , @status_text = StatusText
    FROM @update_se_table

  IF ( @status_code <> 0 )
    GOTO ERROR_PROCEDURE

  SET @DeltaAmountPlayed = @delta_played_amount
  SET @DeltaAmountWon    = @delta_won_amount


ERROR_PROCEDURE:

END -- SessionUpdate_Internal

GO


--------------------------------------------------------------------------------
-- PURPOSE: End play session
-- 
--  PARAMS:
--      - INPUT:
--          @AccountId      varchar(24)
--          @VendorId       varchar(16)
--          @SerialNumber   varchar(30)
--          @MachineNumber  int
--          @SessionId      bigint
--          @AmountPlayed   money
--          @AmountWon      money
--          @GamesPlayed    int
--          @GamesWon       int
--          @CreditBalance  money,
--          @CurrentJackpot money = 0
--
--      - OUTPUT:
--
-- RETURNS:
--      StatusCode
--      StatusText
--
--   NOTES:
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[zsp_SessionEnd]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[zsp_SessionEnd]
GO
CREATE PROCEDURE [dbo].[zsp_SessionEnd]
  @AccountId      varchar(24),
  @VendorId       varchar(16),
  @SerialNumber   varchar(30),
  @MachineNumber  int,
  @SessionId 	    bigint,
  @AmountPlayed   money,
  @AmountWon      money,
  @GamesPlayed    int,
  @GamesWon       int,
  @CreditBalance  money,
  @CurrentJackpot money = 0
AS
BEGIN

  BEGIN TRAN

  -- SET NOCOUNT ON added to prevent extra result sets from
  -- interfering with SELECT statements.
  SET NOCOUNT ON;

  DECLARE @status_code         int
  DECLARE @status_text         nvarchar (254)
  DECLARE @terminal_id         int
  DECLARE @account_id          bigint
  DECLARE @previous_balance    money
  DECLARE @delta_amount_played money
  DECLARE @delta_amount_won    money
  DECLARE @ignore_session_id   int
  DECLARE @is_end_session      int

  SET @status_code         = 0
  SET @status_text         = 'Successful End Session'
  SET @delta_amount_played = 0
  SET @delta_amount_won    = 0
  SET @is_end_session      = 1

  SELECT @terminal_id = dbo.GetTerminalID(@VendorId, @SerialNumber, @MachineNumber) 

  IF (@terminal_id = 0)
  BEGIN
    SET @status_code = 2
    SET @status_text = 'Invalid Machine Information'
    GOTO ERROR_PROCEDURE
  END

  SELECT @account_id = dbo.GetAccountID(@AccountId)

  IF (@account_id = 0)
  BEGIN
    SET @status_code = 1
    SET @status_text = 'Invalid Account Number'
    GOTO ERROR_PROCEDURE
  END
  
  SELECT @ignore_session_id = CAST(GP_KEY_VALUE AS int)
    FROM GENERAL_PARAMS 
   WHERE GP_GROUP_KEY = 'Interface3GS' AND GP_SUBJECT_KEY ='IgnoreSessionID'
  
  IF ( @ignore_session_id = 1 )
  BEGIN
    SELECT @SessionId = ISNULL(AC_CURRENT_PLAY_SESSION_ID , 0 ) 
      FROM   ACCOUNTS                   
     WHERE ( AC_ACCOUNT_ID = @account_id )
  END

  EXECUTE dbo.SessionUpdate_Internal @account_id, @terminal_id, @SerialNumber, @SessionId,
                                     @AmountPlayed, @AmountWon, @GamesPlayed, @GamesWon,
                                     @CreditBalance, @CurrentJackpot, @is_end_session, 
                                     @delta_amount_played OUTPUT, @delta_amount_won OUTPUT,
                                     @status_code OUTPUT, @status_text OUTPUT
  IF ( @status_code <> 0 )
    GOTO ERROR_PROCEDURE

  -- close play session
  EXECUTE dbo.CloseOpenedPlaySession @SessionId, @status_code OUTPUT, @status_text OUTPUT

  IF ( @status_code <> 0 )
    GOTO ERROR_PROCEDURE
    
  -- Close account session
  EXECUTE dbo.CloseAccountSession @account_id

  -- Insert Movement
  
  -- Use delta values to calculate initial balance
  --SET @previous_balance = @CreditBalance + @AmountPlayed - @AmountWon
  SET @previous_balance = @CreditBalance + @delta_amount_played - @delta_amount_won

  IF (1 = (SELECT   CASE WHEN ( (AC_PROMO_CREATION <= GETDATE()) AND (GETDATE() < AC_PROMO_EXPIRATION) AND (AC_PROMO_BALANCE >0) ) THEN 1 ELSE 0 END
            FROM   ACCOUNTS                   
           WHERE ( AC_ACCOUNT_ID = @account_id )))
  BEGIN
    -- PromoEndSession   = 25,
    EXECUTE dbo.InsertMovement @SessionId, @account_id, @terminal_id, 25, @previous_balance, @delta_amount_played, @delta_amount_won, @CreditBalance

  END
	ELSE
  BEGIN
    -- EndCardSession = 6,
    EXECUTE dbo.InsertMovement @SessionId, @account_id, @terminal_id,  6, @previous_balance, @delta_amount_played, @delta_amount_won, @CreditBalance
  END
	  
  SET @status_text = 'Successful End Session'

  COMMIT TRAN
	GOTO OK  
	  
	ERROR_PROCEDURE:
	  ROLLBACK TRAN
	  
	OK:  
	
	-- Audit MBF 17-JUN-2010
	BEGIN TRAN
	
    DECLARE @input AS nvarchar(MAX)
    DECLARE @output AS nvarchar(MAX)	
  		  	
	  SET @input = '@AccountID='+@AccountID
	             +';@VendorId='+@VendorId
	             +';@SerialNumber='+CAST (@SerialNumber AS nvarchar)
	             +';@MachineNumber='+CAST (@MachineNumber AS nvarchar)
	             +';@SessionId='+CAST (@SessionId AS nvarchar)
	             +';@AmountPlayed='+CAST (@AmountPlayed AS nvarchar)
	             +';@AmountWon='+CAST (@AmountWon AS nvarchar)
	             +';@GamesPlayed='+CAST (@GamesPlayed AS nvarchar)
	             +';@GamesWon='+CAST (@GamesWon AS nvarchar)
	             +';@CreditBalance='+CAST (@CreditBalance AS nvarchar)
	             +';@CurrentJackpot='+CAST (@CurrentJackpot AS nvarchar)
  	           
  	           
    SET @output = 'StatusCode='+CAST (@status_code AS nvarchar)
                 +';StatusText='+@status_text
     	
	  EXECUTE dbo.zsp_Audit 'zsp_SessionEnd'
                        , @AccountID
                        , @VendorId 
                        , @SerialNumber
                        , @MachineNumber
                        , @SessionId
                        , @status_code
                        , @CreditBalance
                        , @input
                        , @output
                      
  COMMIT TRAN
	-- End Audit
	
  SELECT @status_code AS StatusCode, @status_text AS StatusText

END -- zsp_SessionEnd

GO


--------------------------------------------------------------------------------
-- PURPOSE: Update play session
-- 
--  PARAMS:
--      - INPUT:
--          @AccountId      varchar(24)
--          @VendorId       varchar(16)
--          @SerialNumber   varchar(30)
--          @MachineNumber  int
--          @SessionId      bigint
--          @AmountPlayed   money
--          @AmountWon      money
--          @GamesPlayed    int
--          @GamesWon       int
--          @CreditBalance  money,
--          @CurrentJackpot money = 0
--
--      - OUTPUT:
--
-- RETURNS:
--      StatusCode
--      StatusText
--
--   NOTES:
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[zsp_SessionUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[zsp_SessionUpdate]
GO
CREATE PROCEDURE [dbo].[zsp_SessionUpdate]
  @AccountId      varchar(24),
  @VendorId       varchar(16),
  @SerialNumber   varchar(30),
  @MachineNumber  int,
  @SessionId      bigint,
  @AmountPlayed   money,
  @AmountWon      money,
  @GamesPlayed    int,
  @GamesWon       int,
  @CreditBalance  money,
  @CurrentJackpot money = 0
AS
BEGIN

  BEGIN TRAN

  -- SET NOCOUNT ON added to prevent extra result sets from
  -- interfering with SELECT statements.
  SET NOCOUNT ON;
	
  DECLARE @status_code         int
  DECLARE @status_text         nvarchar (254)
  DECLARE @terminal_id         int
  DECLARE @account_id          bigint
  DECLARE @previous_balance    money
  DECLARE @delta_amount_played money
  DECLARE @delta_amount_won    money
  DECLARE @ignore_session_id   int
  DECLARE @dummy               int

  SET @status_code         = 0
  SET @status_text         = 'Successful Session Update'
  SET @delta_amount_played = 0
  SET @delta_amount_won    = 0
  SET @dummy               = 0

  SELECT @terminal_id = dbo.GetTerminalID(@VendorId, @SerialNumber, @MachineNumber) 
		
  IF (@terminal_id = 0)
  BEGIN
    SET @status_code = 2
    SET @status_text = 'Invalid Machine Information'
    GOTO ERROR_PROCEDURE
  END

  SELECT @account_id = dbo.GetAccountID(@AccountId)

  IF (@account_id = 0)
  BEGIN
    SET @status_code = 1
    SET @status_text = 'Invalid Account Number'
    GOTO ERROR_PROCEDURE
  END
  
  SELECT @ignore_session_id = CAST(GP_KEY_VALUE AS int)
    FROM GENERAL_PARAMS 
   WHERE GP_GROUP_KEY = 'Interface3GS' AND GP_SUBJECT_KEY ='IgnoreSessionID'
  
  IF ( @ignore_session_id = 1 )
  BEGIN
    SELECT @SessionId = ISNULL(AC_CURRENT_PLAY_SESSION_ID , AC_LAST_PLAY_SESSION_ID ) 
      FROM   ACCOUNTS                   
     WHERE ( AC_ACCOUNT_ID = @account_id )
  END

  EXECUTE dbo.SessionUpdate_Internal @account_id, @terminal_id, @SerialNumber, @SessionId,
                                     @AmountPlayed, @AmountWon, @GamesPlayed, @GamesWon,
                                     @CreditBalance, @CurrentJackpot, @dummy, 
                                     @delta_amount_played OUTPUT, @delta_amount_won OUTPUT,
                                     @status_code OUTPUT, @status_text OUTPUT
  IF ( @status_code <> 0 )
    GOTO ERROR_PROCEDURE

  
  -- Insert Movement
  
  -- Use delta values to calculate initial balance
  --SET @previous_balance = @CreditBalance + @AmountPlayed - @AmountWon
  SET @previous_balance = @CreditBalance + @delta_amount_played - @delta_amount_won
  
  EXECUTE dbo.InsertMovement @SessionId, @account_id, @terminal_id, 0, @previous_balance, @delta_amount_played, @delta_amount_won, @CreditBalance

  SET @status_text = 'Successful Session Update'
	
  COMMIT TRAN
	GOTO OK  
	  
ERROR_PROCEDURE:
  ROLLBACK TRAN
	  
OK: 

	-- Audit MBF 17-JUN-2010
	BEGIN TRAN
	
    DECLARE @input AS nvarchar(MAX)
    DECLARE @output AS nvarchar(MAX)	
  		  	
	  SET @input = '@AccountID='+@AccountID
	             +';@VendorId='+@VendorId
	             +';@SerialNumber='+CAST (@SerialNumber AS nvarchar)
	             +';@MachineNumber='+CAST (@MachineNumber AS nvarchar)
	             +';@SessionId='+CAST (@SessionId AS nvarchar)
	             +';@AmountPlayed='+CAST (@AmountPlayed AS nvarchar)
	             +';@AmountWon='+CAST (@AmountWon AS nvarchar)
	             +';@GamesPlayed='+CAST (@GamesPlayed AS nvarchar)
	             +';@GamesWon='+CAST (@GamesWon AS nvarchar)
	             +';@CreditBalance='+CAST (@CreditBalance AS nvarchar)
	             +';@CurrentJackpot='+CAST (@CurrentJackpot AS nvarchar)
  	           
  	           
    SET @output = 'StatusCode='+CAST (@status_code AS nvarchar)
                 +';StatusText='+@status_text
     	
	  EXECUTE dbo.zsp_Audit 'zsp_SessionUpdate'
                        , @AccountID
                        , @VendorId 
                        , @SerialNumber
                        , @MachineNumber
                        , @SessionId
                        , @status_code
                        , @CreditBalance
                        , @input
                        , @output
                      
  COMMIT TRAN
	-- End Audit

  SELECT @status_code AS StatusCode, @status_text AS StatusText

END -- zsp_SessionUpdate

GO


/****** Object:  User [wg_interface]    Script Date: 05/10/2010 09:33:21 ******/
IF EXISTS (SELECT * FROM sys.server_principals WHERE name = N'wg_interface' AND type in (N'S'))
BEGIN 

IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'wg_interface' AND type in (N'S'))
CREATE USER [wg_interface] FOR LOGIN [wg_interface] WITH DEFAULT_SCHEMA=[dbo];

GRANT EXECUTE ON [dbo].[zsp_AccountStatus] TO [wg_interface] WITH GRANT OPTION;
GRANT EXECUTE ON [dbo].[zsp_SendEvent]     TO [wg_interface] WITH GRANT OPTION;
GRANT EXECUTE ON [dbo].[zsp_SessionEnd]    TO [wg_interface] WITH GRANT OPTION;
GRANT EXECUTE ON [dbo].[zsp_SessionStart]  TO [wg_interface] WITH GRANT OPTION;
GRANT EXECUTE ON [dbo].[zsp_SessionUpdate] TO [wg_interface] WITH GRANT OPTION;

EXEC sp_addrolemember 'db_denydatareader','wg_interface';
EXEC sp_addrolemember 'db_denydatawriter','wg_interface';

END
ELSE
/**** VERSION FAILURE SECTION *****/
SELECT 'Nothing to Update.'
 
USE [sPOS]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[zsp_SessionStart]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[zsp_SessionStart]
GO

CREATE PROCEDURE [dbo].[zsp_SessionStart]
	@pAccountID 	    varchar(24),
	@pVendorId 	      varchar(16),
	@pSerialNumber    varchar(30),
	@pMachineNumber   int,
	@pCurrentJackpot  money = 0,
	@pVendorSessionId bigint = 0,
	@pGameTitle       varchar(50) = '',
	@pDummy           int = 0
AS
BEGIN
  EXECUTE wgdb_000.dbo.zsp_SessionStart  @pAccountID
                                       , @pVendorId
                                       , @pSerialNumber
                                       , @pMachineNumber
                                       , @pCurrentJackpot
                                       , @pVendorSessionId
END
GO

/****** Object:  User [wg_interface]    Script Date: 05/10/2010 09:33:21 ******/
IF EXISTS (SELECT * FROM sys.server_principals WHERE name = N'wg_interface' AND type in (N'S'))
BEGIN

IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'wg_interface' AND type in (N'S'))
CREATE USER [wg_interface] FOR LOGIN [wg_interface] WITH DEFAULT_SCHEMA=[dbo];

GRANT EXECUTE ON [dbo].[zsp_AccountStatus] TO [wg_interface] WITH GRANT OPTION;
GRANT EXECUTE ON [dbo].[zsp_SendEvent]     TO [wg_interface] WITH GRANT OPTION;
GRANT EXECUTE ON [dbo].[zsp_SessionEnd]    TO [wg_interface] WITH GRANT OPTION;
GRANT EXECUTE ON [dbo].[zsp_SessionStart]  TO [wg_interface] WITH GRANT OPTION;
GRANT EXECUTE ON [dbo].[zsp_SessionUpdate] TO [wg_interface] WITH GRANT OPTION;

EXEC sp_addrolemember 'db_denydatareader','wg_interface';
EXEC sp_addrolemember 'db_denydatawriter','wg_interface';

END
ELSE
/**** VERSION FAILURE SECTION *****/
SELECT 'Nothing to Update.'
GO

SELECT 'Update OK.'