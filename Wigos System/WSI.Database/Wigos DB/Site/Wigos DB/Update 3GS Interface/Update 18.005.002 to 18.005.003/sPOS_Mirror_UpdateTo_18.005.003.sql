USE [sPOS]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[zsp_SessionStart]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[zsp_SessionStart]
GO

CREATE PROCEDURE [dbo].[zsp_SessionStart]
	@pAccountID 	    varchar(24),
	@pVendorId 	      varchar(16),
	@pSerialNumber    varchar(30),
	@pMachineNumber   int,
	@pCurrentJackpot  money = 0,
	@pVendorSessionId bigint = 0,
	@pGameTitle       varchar(50) = '',
	@pDummy           int = 0
AS
BEGIN
  EXECUTE wgdb_000.dbo.zsp_SessionStart  @pAccountID
                                       , @pVendorId
                                       , @pSerialNumber
                                       , @pMachineNumber
                                       , @pCurrentJackpot
                                       , @pVendorSessionId
END
GO

/****** Object:  User [wg_interface]    Script Date: 05/10/2010 09:33:21 ******/
IF EXISTS (SELECT * FROM sys.server_principals WHERE name = N'wg_interface' AND type in (N'S'))
BEGIN

IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'wg_interface' AND type in (N'S'))
CREATE USER [wg_interface] FOR LOGIN [wg_interface] WITH DEFAULT_SCHEMA=[dbo];

GRANT EXECUTE ON [dbo].[zsp_AccountStatus] TO [wg_interface] WITH GRANT OPTION;
GRANT EXECUTE ON [dbo].[zsp_SendEvent]     TO [wg_interface] WITH GRANT OPTION;
GRANT EXECUTE ON [dbo].[zsp_SessionEnd]    TO [wg_interface] WITH GRANT OPTION;
GRANT EXECUTE ON [dbo].[zsp_SessionStart]  TO [wg_interface] WITH GRANT OPTION;
GRANT EXECUTE ON [dbo].[zsp_SessionUpdate] TO [wg_interface] WITH GRANT OPTION;

EXEC sp_addrolemember 'db_denydatareader','wg_interface';
EXEC sp_addrolemember 'db_denydatawriter','wg_interface';

END
ELSE
/**** VERSION FAILURE SECTION *****/
SELECT 'Nothing to Update - wg_interface.'
GO


IF EXISTS (SELECT * FROM sys.server_principals WHERE name = N'EIBE' AND type in (N'S'))
BEGIN

IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'EIBE' AND type in (N'S'))
CREATE USER [EIBE] FOR LOGIN [EIBE] WITH DEFAULT_SCHEMA=[dbo];

GRANT EXECUTE ON [dbo].[zsp_AccountStatus] TO [EIBE] WITH GRANT OPTION;
GRANT EXECUTE ON [dbo].[zsp_SendEvent]     TO [EIBE] WITH GRANT OPTION;
GRANT EXECUTE ON [dbo].[zsp_SessionEnd]    TO [EIBE] WITH GRANT OPTION;
GRANT EXECUTE ON [dbo].[zsp_SessionStart]  TO [EIBE] WITH GRANT OPTION;
GRANT EXECUTE ON [dbo].[zsp_SessionUpdate] TO [EIBE] WITH GRANT OPTION;

EXEC sp_addrolemember 'db_denydatareader','EIBE';
EXEC sp_addrolemember 'db_denydatawriter','EIBE';

END
ELSE
/**** VERSION FAILURE SECTION *****/
SELECT 'Nothing to Update - EIBE.'
GO

SELECT 'Update OK.'
