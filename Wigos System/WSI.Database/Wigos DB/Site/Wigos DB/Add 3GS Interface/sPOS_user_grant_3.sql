
USE [wgdb_000]
GO

/****** Object:  User [wg_interface]    Script Date: 05/10/2010 09:33:21 ******/
IF EXISTS (SELECT * FROM sys.server_principals WHERE name = N'wg_interface' AND type in (N'S'))
BEGIN

IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'wg_interface' AND type in (N'S'))
CREATE USER [wg_interface] FOR LOGIN [wg_interface] WITH DEFAULT_SCHEMA=[dbo];

GRANT EXECUTE ON [dbo].[zsp_AccountStatus] TO [wg_interface] WITH GRANT OPTION;
GRANT EXECUTE ON [dbo].[zsp_SendEvent]     TO [wg_interface] WITH GRANT OPTION;
GRANT EXECUTE ON [dbo].[zsp_SessionEnd]    TO [wg_interface] WITH GRANT OPTION;
GRANT EXECUTE ON [dbo].[zsp_SessionStart]  TO [wg_interface] WITH GRANT OPTION;
GRANT EXECUTE ON [dbo].[zsp_SessionUpdate] TO [wg_interface] WITH GRANT OPTION;

EXEC sp_addrolemember 'db_denydatareader','wg_interface';
EXEC sp_addrolemember 'db_denydatawriter','wg_interface';

END
ELSE
/**** VERSION FAILURE SECTION *****/
SELECT 'Nothing to Update for wg_interface.'
GO

/****** Object:  User [EIBE] ******/
IF EXISTS (SELECT * FROM sys.server_principals WHERE name = N'EIBE' AND type in (N'S'))
BEGIN

IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'EIBE' AND type in (N'S'))
CREATE USER [EIBE] FOR LOGIN [EIBE] WITH DEFAULT_SCHEMA=[dbo];

GRANT EXECUTE ON [dbo].[zsp_AccountStatus] TO [EIBE] WITH GRANT OPTION;
GRANT EXECUTE ON [dbo].[zsp_SendEvent]     TO [EIBE] WITH GRANT OPTION;
GRANT EXECUTE ON [dbo].[zsp_SessionEnd]    TO [EIBE] WITH GRANT OPTION;
GRANT EXECUTE ON [dbo].[zsp_SessionStart]  TO [EIBE] WITH GRANT OPTION;
GRANT EXECUTE ON [dbo].[zsp_SessionUpdate] TO [EIBE] WITH GRANT OPTION;

EXEC sp_addrolemember 'db_denydatareader','EIBE';
EXEC sp_addrolemember 'db_denydatawriter','EIBE';

END
ELSE
/**** VERSION FAILURE SECTION *****/
SELECT 'Nothing to Update for EIBE.'
GO

/****** Object:  User [3GS] ******/
IF EXISTS (SELECT * FROM sys.server_principals WHERE name = N'3GS' AND type in (N'S'))
BEGIN

IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'3GS' AND type in (N'S'))
CREATE USER [3GS] FOR LOGIN [3GS] WITH DEFAULT_SCHEMA=[dbo];

GRANT EXECUTE ON [dbo].[zsp_AccountStatus] TO [3GS] WITH GRANT OPTION;
GRANT EXECUTE ON [dbo].[zsp_SendEvent]     TO [3GS] WITH GRANT OPTION;
GRANT EXECUTE ON [dbo].[zsp_SessionEnd]    TO [3GS] WITH GRANT OPTION;
GRANT EXECUTE ON [dbo].[zsp_SessionStart]  TO [3GS] WITH GRANT OPTION;
GRANT EXECUTE ON [dbo].[zsp_SessionUpdate] TO [3GS] WITH GRANT OPTION;

EXEC sp_addrolemember 'db_denydatareader','3GS';
EXEC sp_addrolemember 'db_denydatawriter','3GS';

END
ELSE
/**** VERSION FAILURE SECTION *****/
SELECT 'Nothing to Update for 3GS.'
GO
