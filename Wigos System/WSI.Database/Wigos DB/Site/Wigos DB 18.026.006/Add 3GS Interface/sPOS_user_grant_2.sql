USE [sPOS]
GO

/****** Object:  User [wginterface]    Script Date: 05/10/2010 09:33:21 ******/
CREATE USER [wg_interface] FOR LOGIN [wg_interface] WITH DEFAULT_SCHEMA=[dbo]
GO

GRANT EXECUTE ON [dbo].[zsp_AccountStatus] TO [wg_interface] WITH GRANT OPTION
GO
GRANT EXECUTE ON [dbo].[zsp_SendEvent]     TO [wg_interface] WITH GRANT OPTION
GO
GRANT EXECUTE ON [dbo].[zsp_SessionEnd]    TO [wg_interface] WITH GRANT OPTION
GO
GRANT EXECUTE ON [dbo].[zsp_SessionStart]  TO [wg_interface] WITH GRANT OPTION
GO
GRANT EXECUTE ON [dbo].[zsp_SessionUpdate] TO [wg_interface] WITH GRANT OPTION
GO
sp_addrolemember 'db_denydatareader','wg_interface' 
GO
sp_addrolemember 'db_denydatawriter','wg_interface' 
GO

/****** Object:  User [EIBE] ******/
CREATE USER [EIBE] FOR LOGIN [EIBE] WITH DEFAULT_SCHEMA=[dbo]
GO

GRANT EXECUTE ON [dbo].[zsp_AccountStatus] TO [EIBE] WITH GRANT OPTION
GO
GRANT EXECUTE ON [dbo].[zsp_SendEvent]     TO [EIBE] WITH GRANT OPTION
GO
GRANT EXECUTE ON [dbo].[zsp_SessionEnd]    TO [EIBE] WITH GRANT OPTION
GO
GRANT EXECUTE ON [dbo].[zsp_SessionStart]  TO [EIBE] WITH GRANT OPTION
GO
GRANT EXECUTE ON [dbo].[zsp_SessionUpdate] TO [EIBE] WITH GRANT OPTION
GO
sp_addrolemember 'db_denydatareader','EIBE' 
GO
sp_addrolemember 'db_denydatawriter','EIBE' 
GO

/****** Object:  User [3GS] ******/
CREATE USER [3GS] FOR LOGIN [3GS] WITH DEFAULT_SCHEMA=[dbo]
GO

GRANT EXECUTE ON [dbo].[zsp_AccountStatus] TO [3GS] WITH GRANT OPTION
GO
GRANT EXECUTE ON [dbo].[zsp_SendEvent]     TO [3GS] WITH GRANT OPTION
GO
GRANT EXECUTE ON [dbo].[zsp_SessionEnd]    TO [3GS] WITH GRANT OPTION
GO
GRANT EXECUTE ON [dbo].[zsp_SessionStart]  TO [3GS] WITH GRANT OPTION
GO
GRANT EXECUTE ON [dbo].[zsp_SessionUpdate] TO [3GS] WITH GRANT OPTION
GO
sp_addrolemember 'db_denydatareader','3GS' 
GO
sp_addrolemember 'db_denydatawriter','3GS' 
GO