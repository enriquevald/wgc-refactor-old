USE [master]
GO

/**** CHECK DATABASE EXISTENCE SECTION *****/
IF (EXISTS (SELECT * FROM sys.databases WHERE name = 'wgdb_000'))
BEGIN
/**** FAILURE SECTION *****/
SELECT 'Database wgdb_000 already exists.'

raiserror('Not updated', 20, -1) with log
END

/****** Object:  Database [wgdb_000]    Script Date: 05/01/2007 17:12:26 ******/
CREATE DATABASE [wgdb_000] ON  PRIMARY 
( NAME = N'wgdb_000', FILENAME = N'C:\Wigos\Database\wgdb_000.mdf' , SIZE = 5173248KB , MAXSIZE = UNLIMITED, FILEGROWTH = 10240KB )
 LOG ON 
( NAME = N'wgdb_000_log', FILENAME = N'C:\Wigos\Database\wgdb_000_1.ldf' , SIZE = 17030144KB , MAXSIZE = UNLIMITED , FILEGROWTH = 10240KB )
 COLLATE SQL_Latin1_General_CP1_CI_AS
GO
EXEC dbo.sp_dbcmptlevel @dbname=N'wgdb_000', @new_cmptlevel=90
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [wgdb_000].[dbo].[sp_fulltext_database] @action = 'disable'
end
GO
ALTER DATABASE [wgdb_000] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [wgdb_000] SET ANSI_NULLS OFF
GO
ALTER DATABASE [wgdb_000] SET ANSI_PADDING ON
GO
ALTER DATABASE [wgdb_000] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [wgdb_000] SET ARITHABORT OFF
GO
ALTER DATABASE [wgdb_000] SET AUTO_CLOSE OFF
GO
ALTER DATABASE [wgdb_000] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [wgdb_000] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [wgdb_000] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [wgdb_000] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [wgdb_000] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [wgdb_000] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [wgdb_000] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [wgdb_000] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [wgdb_000] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [wgdb_000] SET ENABLE_BROKER
GO
ALTER DATABASE [wgdb_000] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [wgdb_000] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [wgdb_000] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [wgdb_000] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [wgdb_000] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [wgdb_000] SET READ_WRITE
GO
ALTER DATABASE [wgdb_000] SET RECOVERY FULL
GO
ALTER DATABASE [wgdb_000] SET MULTI_USER
GO
ALTER DATABASE [wgdb_000] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [wgdb_000] SET DB_CHAINING OFF
GO
ALTER DATABASE [wgdb_000] SET READ_COMMITTED_SNAPSHOT ON
GO

/* User Logins */
USE [master]
CREATE LOGIN [wgroot_000] WITH PASSWORD=N'Wigos_ro_000', DEFAULT_DATABASE=[wgdb_000], DEFAULT_LANGUAGE=[us_english], CHECK_EXPIRATION=OFF, CHECK_POLICY=ON, SID=0x79E1DE223FE15A41808F68255704F081
GO
CREATE LOGIN [wgpublic_000] WITH PASSWORD=N'Wigos_pu_000', DEFAULT_DATABASE=[wgdb_000], DEFAULT_LANGUAGE=[us_english], CHECK_EXPIRATION=OFF, CHECK_POLICY=ON, SID=0x0009EFF6DE33334883F49D4EE11F84A2
GO
CREATE LOGIN [wggui_000] WITH PASSWORD=N'Wigos_gi_000', DEFAULT_DATABASE=[wgdb_000], DEFAULT_LANGUAGE=[us_english], CHECK_EXPIRATION=OFF, CHECK_POLICY=ON, SID=0xA2D4ADBAD2FB1840BD905E6237B4DB0D
GO

/* Database Users */
USE [wgdb_000]
GO
CREATE USER [wgroot] FOR LOGIN [wgroot_000] WITH DEFAULT_SCHEMA=[dbo]
GO
CREATE USER [wgpublic] FOR LOGIN [wgpublic_000] WITH DEFAULT_SCHEMA=[dbo]
GO
CREATE USER [wggui] FOR LOGIN [wggui_000] WITH DEFAULT_SCHEMA=[dbo]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/* Tables */
/****** Object:  Table [dbo].[account_movements]    Script Date: 12/09/2008 16:30:22 ******/
CREATE TABLE [dbo].[account_movements](
  [am_movement_id] [bigint] IDENTITY(1,1) NOT NULL,
  [am_play_session_id] [bigint] NULL,
  [am_account_id] [bigint] NOT NULL,
  [am_terminal_id] [int] NULL,
  [am_wcp_sequence_id] [bigint] NULL,
  [am_wcp_transaction_id] [bigint] NULL,
  [am_datetime] [datetime] NOT NULL,
  [am_type] [int] NOT NULL,
  [am_initial_balance] [money] NOT NULL,
  [am_sub_amount] [money] NOT NULL,
  [am_add_amount] [money] NOT NULL,
  [am_final_balance] [money] NOT NULL,
  [am_cashier_id] [int] NULL,
  [am_cashier_name] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  [am_terminal_name] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  [am_operation_id] [bigint] NULL,
 CONSTRAINT [PK_movements] PRIMARY KEY CLUSTERED 
(
  [am_movement_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_movements] ON [dbo].[account_movements] 
(
  [am_play_session_id] ASC,
  [am_datetime] ASC,
  [am_movement_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_movements_transaction] ON [dbo].[account_movements] 
(
  [am_terminal_id] ASC,
  [am_wcp_transaction_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_movements_account_date] ON [dbo].[account_movements] 
(
                [am_account_id] ASC, 
                [am_datetime] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_type_date_account] ON [dbo].[account_movements] 
(
  [am_type] ASC,
  [am_datetime] ASC,
  [am_account_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[account_operations]    Script Date: 10/04/2010 11:57:13 ******/
CREATE TABLE [dbo].[account_operations](
	[ao_operation_id] [bigint] IDENTITY(1,1) NOT NULL,
	[ao_datetime] [datetime] NOT NULL CONSTRAINT [DF_account_operations_ao_datetime]  DEFAULT (getdate()),
	[ao_code] [int] NOT NULL,
	[ao_account_id] [bigint] NOT NULL,
	[ao_cashier_session_id] [bigint] NULL,
	[ao_mb_account_id] [bigint] NULL,
	[ao_promo_id] [bigint] NULL,
	[ao_amount] [money] NULL,
	[ao_non_redeemable] [money] NULL,
	[ao_won_lock] [money] NULL,
	[ao_operation_data] [bigint] NULL,
 CONSTRAINT [PK_account_operations] PRIMARY KEY CLUSTERED 
(
	[ao_operation_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_ao_date_account_promo] ON [dbo].[account_operations] 
(
	[ao_datetime] ASC,
	[ao_account_id] ASC,
	[ao_promo_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[accounts]    Script Date: 06/15/2010 18:01:21 ******/
CREATE TABLE [dbo].[accounts](
	[ac_account_id] [bigint] IDENTITY(1,1) NOT NULL,
	[ac_type] [int] NOT NULL,
	[ac_holder_name] [nvarchar](50) NULL,
	[ac_blocked] [bit] NOT NULL,
	[ac_not_valid_before] [datetime] NULL,
	[ac_not_valid_after] [datetime] NULL,
	[ac_balance] [money] NOT NULL CONSTRAINT [DF_accounts_ac_balance]  DEFAULT ((0)),
	[ac_cash_in] [money] NOT NULL CONSTRAINT [DF_accounts_ac_cash_in]  DEFAULT ((0)),
	[ac_cash_won] [money] NOT NULL CONSTRAINT [DF_accounts_ac_cash_won]  DEFAULT ((0)),
	[ac_not_redeemable] [money] NOT NULL CONSTRAINT [DF_accounts_ac_not_redeemable]  DEFAULT ((0)),
	[ac_timestamp] [timestamp] NULL,
	[ac_track_data] [nvarchar](50) NULL,
	[ac_total_cash_in] [money] NOT NULL CONSTRAINT [DF_accounts_ac_total_cash_in]  DEFAULT ((0)),
	[ac_total_cash_out] [money] NOT NULL CONSTRAINT [DF_accounts_ac_total_cash_out]  DEFAULT ((0)),
	[ac_initial_cash_in] [money] NOT NULL CONSTRAINT [DF_accounts_ac_initial_cash_in]  DEFAULT ((0)),
	[ac_activated] [bit] NOT NULL CONSTRAINT [DF_accounts_ac_activated]  DEFAULT ((0)),
	[ac_deposit] [money] NOT NULL CONSTRAINT [DF_accounts_ac_deposit]  DEFAULT ((0)),
	[ac_current_terminal_id] [int] NULL,
	[ac_current_terminal_name] [nvarchar](50) NULL,
	[ac_current_play_session_id] [bigint] NULL,
	[ac_last_terminal_id] [int] NULL,
	[ac_last_terminal_name] [nvarchar](50) NULL,
	[ac_last_play_session_id] [bigint] NULL,
	[ac_user_type] [int] NULL CONSTRAINT [DF_accounts_ac_user_type]  DEFAULT ((0)),
	[ac_points] [money] NULL CONSTRAINT [DF_accounts_ac_points]  DEFAULT ((0)),
	[ac_initial_not_redeemable] [money] NOT NULL CONSTRAINT [DF_accounts_ac_initial_not_redeemable]  DEFAULT ((0)),
	[ac_created] [datetime] NOT NULL CONSTRAINT [DF_accounts_ac_created] DEFAULT (getdate()),
	[ac_promo_balance] [money] NOT NULL CONSTRAINT [DF_accounts_ac_promo_balance] DEFAULT ((0)),
	[ac_promo_limit] [money] NOT NULL CONSTRAINT [DF_accounts_ac_promo_limit] DEFAULT ((0)),
	[ac_promo_creation] [datetime] NOT NULL CONSTRAINT [DF_accounts_ac_promo_creation] DEFAULT (getdate()),
	[ac_promo_expiration] [datetime] NOT NULL CONSTRAINT [DF_accounts_ac_promo_expiration] DEFAULT (getdate()),
	[ac_last_activity] [datetime] NOT NULL CONSTRAINT [DF_accounts_ac_last_activity] DEFAULT (getdate()),
	[ac_holder_id] [nvarchar](20) NULL,
	[ac_holder_address_01] [nvarchar](50) NULL,
	[ac_holder_address_02] [nvarchar](50) NULL,
	[ac_holder_address_03] [nvarchar](50) NULL,
	[ac_holder_city] [nvarchar](50) NULL,
	[ac_holder_zip] [nvarchar](10) NULL,
	[ac_holder_email_01] [nvarchar](50) NULL,
	[ac_holder_email_02] [nvarchar](50) NULL,
	[ac_holder_phone_number_01] [nvarchar](20) NULL,
	[ac_holder_phone_number_02] [nvarchar](20) NULL,
	[ac_holder_comments] [nvarchar](100) NULL,
	[ac_holder_gender] [int] NULL,
	[ac_holder_marital_status] [int] NULL,
	[ac_holder_birth_date] [datetime] NULL,
	[ac_draw_last_play_session_id] [bigint] NOT NULL CONSTRAINT [DF_accounts_ac_draw_last_session_id]  DEFAULT ((0)),
	[ac_draw_last_play_session_remainder] [money] NOT NULL CONSTRAINT [DF_accounts_ac_draw_last_remainder]  DEFAULT ((0)),
	[ac_holder_id_indexed]  AS (isnull([ac_holder_id],'<NULL>'+CONVERT([nvarchar],[ac_account_id],(0)))),
	[ac_nr_won_lock] [money] NULL CONSTRAINT [DF_accounts_ac_nr_won_lock] DEFAULT ((0)),
	[ac_nr_expiration] [datetime] NULL,
	[ac_cashin_while_playing] [money] NULL,
	[ac_holder_level] [int] NOT NULL CONSTRAINT [DF_accounts_ac_holder_level] DEFAULT ((0)),
	[ac_card_paid] [bit] NOT NULL CONSTRAINT [DF_accounts_ac_card_paid] DEFAULT ((0)),
	[ac_cancellable_operation_id] [bigint] NULL,
	[ac_current_promotion_id] [bigint] NULL,
	[ac_block_reason] [int] NOT NULL CONSTRAINT [DF_ac_block_reason] DEFAULT (0),
	[ac_holder_level_expiration] [datetime] NULL,
	[ac_holder_level_entered] [datetime] NULL CONSTRAINT [DF_accounts_ac_holder_level_entered] DEFAULT (GetDate()),
	[ac_holder_level_notify] [int] NULL CONSTRAINT [DF_accounts_ac_holder_level_notify] DEFAULT (0),
 CONSTRAINT [PK_accounts] PRIMARY KEY CLUSTERED 
(
	[ac_account_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_track_data] ON [dbo].[accounts] 
(
  [ac_track_data] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_ac_holder_id_indexed] ON [dbo].[accounts] 
(
	[ac_holder_id_indexed] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_ac_holder_level_expiration] ON [dbo].[accounts] 
(
	[ac_holder_level_expiration] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[alesis_parameters]    Script Date: 05/12/2009 16:38:05 ******/
CREATE TABLE [dbo].[alesis_parameters](
  [ap_sql_server_ip_address] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  [ap_sql_database_name] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  [ap_sql_user] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  [ap_sql_user_password] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  [ap_vendor_id] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[alesis_terminals]    Script Date: 05/12/2009 16:38:26 ******/
CREATE TABLE [dbo].[alesis_terminals](
  [at_terminal_id] [int] NOT NULL,
  [at_machine_id] [int] NOT NULL,
  [at_session_id] [bigint] NULL,
  [at_status] [int] NOT NULL CONSTRAINT [DF_alesis_terminals_at_status]  DEFAULT ((0)),
 CONSTRAINT [PK_alesis_terminals] PRIMARY KEY CLUSTERED 
(
  [at_terminal_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [IX_alesis_terminals] UNIQUE NONCLUSTERED 
(
  [at_machine_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[applications]    Script Date: // :: ******/
CREATE TABLE [dbo].[applications](
  [app_name] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
  [app_version] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
  [app_machine] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
  [app_ip_address] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
  [app_os_username] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
  [app_login_name] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
  [app_last_access] [datetime] NOT NULL,
 CONSTRAINT [PK_applications] PRIMARY KEY CLUSTERED 
(
  [app_name] ASC,
  [app_machine] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[audit_3gs]    Script Date: 06/17/2010 16:56:51 ******/
CREATE TABLE [dbo].[audit_3gs](
	[a3gs_execution] [datetime] NOT NULL CONSTRAINT [DF_audit_procedure_a3gs_exec_time]  DEFAULT (getdate()),
	[a3gs_id] [bigint] IDENTITY(1,1) NOT NULL,
	[a3gs_procedure] [nvarchar](50) NOT NULL,
	[a3gs_account_id] [nvarchar](24) NOT NULL,
	[a3gs_vendor_id] [nvarchar](16) NOT NULL,
	[a3gs_machine_number] [int] NOT NULL,
	[a3gs_serial_number] [nvarchar](30) NULL,
	[a3gs_session_id] [bigint] NULL,
	[a3gs_balance] [money] NULL,
	[a3gs_status_code] [int] NOT NULL,
	[a3gs_input] [nvarchar](max) NULL,
	[a3gs_output] [nvarchar](max) NULL
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_audit_3gs] ON [dbo].[audit_3gs] 
(
	[a3gs_execution] ASC,
	[a3gs_vendor_id] ASC,
	[a3gs_machine_number] ASC,
	[a3gs_status_code] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[c2_cards]    Script Date: 12/09/2008 16:30:21 ******/
CREATE TABLE [dbo].[c2_cards](
  [c2c_card_id] [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
  [c2c_request_time] [datetime] NULL,
  [c2c_terminal_id] [int] NULL,
  [c2c_contents] [xml] NOT NULL,
 CONSTRAINT [PK_c2_cards] PRIMARY KEY CLUSTERED 
(
  [c2c_card_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[c2_draw_audit]    Script Date: 12/09/2008 16:30:22 ******/
CREATE TABLE [dbo].[c2_draw_audit](
  [da_draw_id] [bigint] IDENTITY(1,1) NOT NULL,
  [da_draw_datetime] [datetime] NOT NULL,
  [da_datetime] [datetime] NOT NULL CONSTRAINT [DF_draw_audit_da_datetime]  DEFAULT (getdate()),
  [da_winning_numbers] [xml] NOT NULL,
 CONSTRAINT [PK_draw_audit] PRIMARY KEY CLUSTERED 
(
  [da_draw_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Draw_Datetime] ON [dbo].[c2_draw_audit] 
(
  [da_draw_datetime] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[c2_draw_audit_plays]    Script Date: 12/09/2008 16:30:22 ******/
CREATE TABLE [dbo].[c2_draw_audit_plays](
  [dap_play_id] [bigint] IDENTITY(1,1) NOT NULL,
  [dap_draw_id] [bigint] NOT NULL,
  [dap_play_datetime] [datetime] NOT NULL,
  [dap_game_id] [int] NOT NULL,
  [dap_game_name] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  [dap_terminal_id] [int] NOT NULL,
  [dap_terminal_name] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  [dap_denomination] [money] NOT NULL,
  [dap_played_credits] [bigint] NOT NULL,
  [dap_won_credits] [bigint] NOT NULL,
  [dap_jackpot_bound_credits] [bigint] NOT NULL,
  [dap_prize_list] [xml] NULL,
  [dap_cards] [xml] NOT NULL,
  [dap_wcp_transaction_id] [bigint] NOT NULL CONSTRAINT [DF_draw_audit_plays_wcp_transaction_id]  DEFAULT ((0)),
 CONSTRAINT [PK_Draw_Audit_Plays] PRIMARY KEY CLUSTERED 
(
  [dap_play_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [FK_Draw_Audit_Plays_draw_audit] UNIQUE NONCLUSTERED 
(
  [dap_play_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_c2_draw_audit_plays] ON [dbo].[c2_draw_audit_plays] 
(
  [dap_play_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Draw_Audit_Plays_Draw_Id] ON [dbo].[c2_draw_audit_plays] 
(
  [dap_draw_id] ASC
)
INCLUDE ( [dap_game_id],
[dap_terminal_id]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[c2_jackpot_counters]    Script Date: 12/09/2008 16:30:22 ******/
CREATE TABLE [dbo].[c2_jackpot_counters](
  [c2jc_index] [int] NOT NULL,
  [c2jc_accumulated] [numeric](20, 8) NOT NULL CONSTRAINT [DF_c2_jackpot_counters_c2jc_current_amount]  DEFAULT ((0)),
  [c2jc_to_compensate] [numeric](20, 8) NOT NULL CONSTRAINT [DF_c2_jackpot_counters_c2jc_compensation_amount]  DEFAULT ((0)),
 CONSTRAINT [PK_c2_jackpot_counters] PRIMARY KEY CLUSTERED 
(
  [c2jc_index] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[c2_jackpot_history]    Script Date: 12/09/2008 16:30:22 ******/
CREATE TABLE [dbo].[c2_jackpot_history](
  [c2jh_jackpot_id] [bigint] IDENTITY(1,1) NOT NULL,
  [c2jh_index] [int] NOT NULL,
  [c2jh_name] [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  [c2jh_status] [int] NOT NULL CONSTRAINT [DF_c2_jackpot_history_c2jh_status]  DEFAULT ((0)),
  [c2jh_amount] [money] NOT NULL,
  [c2jh_awarded] [datetime] NOT NULL CONSTRAINT [DF_c2_jackpot_history_c2jh_awarded]  DEFAULT (getdate()),
  [c2jh_play_id] [bigint] NULL,
  [c2jh_terminal_id] [int] NOT NULL,
  [c2jh_terminal_name] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  [c2jh_game_id] [int] NOT NULL,
  [c2jh_game_name] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_c2_jackpot_history] PRIMARY KEY CLUSTERED 
(
  [c2jh_jackpot_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_Jackpot_Play_Id] ON [dbo].[c2_jackpot_history] 
(
  [c2jh_play_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[c2_jackpot_instances]    Script Date: 12/09/2008 16:30:21 ******/
CREATE TABLE [dbo].[c2_jackpot_instances](
  [c2ji_index] [int] NOT NULL,
  [c2ji_name] [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  [c2ji_contribution_pct] [numeric](5, 2) NOT NULL CONSTRAINT [DF_c2_jackpots_c2j_amount]  DEFAULT ((0)),
  [c2ji_minimum_bet] [money] NOT NULL CONSTRAINT [DF_c2_jackpots_c2j_min_bet_amount]  DEFAULT ((0)),
  [c2ji_minimum] [money] NOT NULL,
  [c2ji_maximum] [money] NOT NULL,
  [c2ji_average] [money] NOT NULL,
  [c2ji_timestamp] [timestamp] NOT NULL,
 CONSTRAINT [PK_c2_jackpot_instances] PRIMARY KEY CLUSTERED 
(
  [c2ji_index] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[c2_jackpot_parameters]    Script Date: 12/09/2008 16:30:21 ******/
CREATE TABLE [dbo].[c2_jackpot_parameters](
  [c2jp_enabled] [bit] NOT NULL,
  [c2jp_contribution_pct] [numeric](5, 2) NOT NULL CONSTRAINT [DF_c2_jackpot_parameters_c2jp_contribution_pct]  DEFAULT ((0)),
  [c2jp_awarding_days] [int] NOT NULL CONSTRAINT [DF_c2_jackpot_parameters_c2jp_working_days]  DEFAULT ((127)),
  [c2jp_awarding_start] [int] NOT NULL CONSTRAINT [DF_c2_jackpot_parameters_c2jp_working_start]  DEFAULT ((0)),
  [c2jp_awarding_end] [int] NOT NULL CONSTRAINT [DF_c2_jackpot_parameters_c2jp_working_end]  DEFAULT ((86399)),
  [c2jp_compensation_pct] [numeric](5, 2) NOT NULL CONSTRAINT [DF_c2_jackpot_parameters_c2jp_compensation_pct]  DEFAULT ((0)),
  [c2jp_timestamp] [timestamp] NOT NULL,
  [c2jp_block_mode] [int] NOT NULL CONSTRAINT [DF_c2_jackpot_parameters_c2jp_block_mode] DEFAULT ((0)),
  [c2jp_block_min_amount] [money] NOT NULL CONSTRAINT [DF_c2_jackpot_parameters_c2jp_block_min_amount]  DEFAULT ((0)),
  [c2jp_block_interval] [int] NOT NULL CONSTRAINT [DF_c2_jackpot_parameters_c2jp_block_interval] DEFAULT ((300)),
  [c2jp_animation_interval] [int] NOT NULL CONSTRAINT [DF_c2_jackpot_parameters_c2jp_animation_interval] DEFAULT ((15)),
  [c2jp_recent_interval] [int] NOT NULL CONSTRAINT [DF_c2_jackpot_parameters_c2jp_recent_interval] DEFAULT ((3600)),
  [c2jp_promo_message1] [nvarchar](MAX) NULL,  
  [c2jp_promo_message2] [nvarchar](MAX) NULL,
  [c2jp_promo_message3] [nvarchar](MAX) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[c2_winning_patterns]    Script Date: 12/09/2008 16:30:21 ******/
CREATE TABLE [dbo].[c2_winning_patterns](
  [w2p_pattern_id] [int] NOT NULL,
  [w2p_min_prize] [bigint] NOT NULL,
  [w2p_max_prize] [bigint] NOT NULL,
  [w2p_position_list] [bigint] NOT NULL,
 CONSTRAINT [PK_c2_winning_patterns] PRIMARY KEY CLUSTERED 
(
  [w2p_pattern_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Card_Generation]    Script Date: 12/09/2008 16:30:21 ******/
CREATE TABLE [dbo].[Card_Generation](
  [cg_site_id] [int] NOT NULL,
  [cg_last_sequence] [bigint] NOT NULL,
 CONSTRAINT [PK_Card_Generation] PRIMARY KEY CLUSTERED 
(
  [cg_site_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cashier_movements]    Script Date: 12/09/2008 16:30:22 ******/
CREATE TABLE [dbo].[cashier_movements](
  [cm_movement_id] [bigint] IDENTITY(1,1) NOT NULL,
  [cm_session_id] [bigint] NOT NULL,
  [cm_cashier_id] [int] NOT NULL,
  [cm_user_id] [int] NOT NULL,
  [cm_date] [datetime] NOT NULL CONSTRAINT [DF_cashier_movements_cm_date]  DEFAULT (getdate()),
  [cm_type] [int] NOT NULL,
  [cm_money_type] [int] NOT NULL CONSTRAINT [DF_cashier_movements_cm_money_type]  DEFAULT ((0)),
  [cm_account_movement_id] [bigint] NULL,
  [cm_initial_balance] [money] NOT NULL,
  [cm_sub_amount] [money] NOT NULL,
  [cm_add_amount] [money] NOT NULL,
  [cm_final_balance] [money] NOT NULL,
  [cm_user_name] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  [cm_cashier_name] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  [cm_card_track_data] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  [cm_account_id] [bigint] NULL,
  [cm_operation_id] [bigint] NULL,
 CONSTRAINT [PK_cashier_movements] PRIMARY KEY CLUSTERED 
(
  [cm_movement_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cashier_sessions]    Script Date: 12/09/2008 16:30:21 ******/
CREATE TABLE [dbo].[cashier_sessions](
  [cs_session_id] [bigint] IDENTITY(1,1) NOT NULL,
  [cs_name] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
  [cs_cashier_id] [int] NOT NULL,
  [cs_user_id] [int] NOT NULL,
  [cs_opening_date] [datetime] NOT NULL CONSTRAINT [DF_cashier_sessions_cs_opening_date]  DEFAULT (getdate()),
  [cs_closing_date] [datetime] NULL,
  [cs_status] [int] NOT NULL CONSTRAINT [DF_cashier_sessions_cs_status]  DEFAULT ((0)),
  [cs_balance] [money] NOT NULL CONSTRAINT [DF_cashier_sessions_cs_balance]  DEFAULT ((0)),
  [cs_other_balance_1] [money] NOT NULL CONSTRAINT [DF_cashier_sessions_cs_other_balance_1]  DEFAULT ((0)),
  [cs_other_balance_2] [money] NOT NULL CONSTRAINT [DF_cashier_sessions_cs_other_balance_2]  DEFAULT ((0)),
  [cs_tax_a_pct] [decimal](5, 2) NULL,
  [cs_tax_b_pct] [decimal](5, 2) NULL,  
  [cs_collected_amount] [money] NULL,
 CONSTRAINT [PK_cashier_sessions] PRIMARY KEY CLUSTERED 
(
  [cs_session_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_cashier_sessions] ON [dbo].[cashier_sessions] 
(
  [cs_name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cashier_terminals]    Script Date: 12/09/2008 16:30:21 ******/
CREATE TABLE [dbo].[cashier_terminals](
  [ct_cashier_id] [int] IDENTITY(1,1) NOT NULL,
  [ct_name] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
 CONSTRAINT [PK_cashier_terminals] PRIMARY KEY CLUSTERED 
(
  [ct_cashier_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_cashier_terminals] ON [dbo].[cashier_terminals] 
(
  [ct_name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cashier_vouchers]    Script Date: 12/09/2008 16:30:22 ******/
CREATE TABLE [dbo].[cashier_vouchers](
	[cv_voucher_id] [bigint] IDENTITY(1,1) NOT NULL,
	[cv_datetime] [datetime] NOT NULL CONSTRAINT [DF_cashier_vouchers_cv_datetime]  DEFAULT (getdate()),
	[cv_session_id] [bigint] NOT NULL,
	[cv_user_id] [int] NOT NULL,
	[cv_cashier_id] [int] NOT NULL,
	[cv_user_name] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[cv_cashier_name] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[cv_html] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[cv_type] [int] NOT NULL CONSTRAINT [DF_cashier_vouchers_cv_type]  DEFAULT ((0)),
	[cv_amount] [money] NOT NULL CONSTRAINT [DF_cashier_vouchers_cv_amount]  DEFAULT ((0)),
	[cv_account_id] [bigint] NULL,
	[cv_operation_id] [bigint] NULL,
 CONSTRAINT [PK_cashier_vouchers] PRIMARY KEY CLUSTERED 
(
  [cv_voucher_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Objeto:  Index [IX_session_id]    Fecha de la secuencia de comandos: 01/05/2011 04:36:04 ******/
CREATE NONCLUSTERED INDEX [IX_session_id] ON [dbo].[cashier_vouchers] 
	( [cv_session_id] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Objeto:  Index [IX_operation_id]    Fecha de la secuencia de comandos: 01/05/2011 04:36:20 ******/
CREATE NONCLUSTERED INDEX [IX_operation_id] ON [dbo].[cashier_vouchers] 
(
	[cv_operation_id] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cj_parameters]    Script Date: 12/16/2010 16:40:26 ******/
CREATE TABLE [dbo].[cj_parameters](
	[cjp_local_ip_1] [nvarchar](50) NULL,
	[cjp_remote_ip_1] [nvarchar](50) NULL,
	[cjp_vendor_id_1] [nvarchar](50) NULL,
	[cjp_local_ip_2] [nvarchar](50) NULL,
	[cjp_remote_ip_2] [nvarchar](50) NULL,
	[cjp_vendor_id_2] [nvarchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cj_queues]    Script Date: 12/09/2008 16:30:21 ******/
CREATE TABLE [dbo].[cj_queues](
  [cq_queue_id] [bigint] NOT NULL,
  [cq_vendor_id] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  [cq_vendor_ip] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  [cq_path] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  [cq_sequence_number] [int] NOT NULL CONSTRAINT [DF_cj_queues_cq_sequence_number]  DEFAULT ((1)),
 CONSTRAINT [PK_cj_queues] PRIMARY KEY CLUSTERED 
(
  [cq_queue_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cj_transactions]    Script Date: 12/09/2008 16:30:22 ******/
CREATE TABLE [dbo].[cj_transactions](
  [ctx_queue_id] [bigint] NOT NULL,
  [ctx_sequence_number] [int] NOT NULL,
  [ctx_terminal_id] [int] NOT NULL,
  [ctx_session_id] [bigint] NOT NULL,
  [ctx_sequence_id] [bigint] NOT NULL,
  [ctx_transaction_id] [bigint] NOT NULL CONSTRAINT [DF_cj_transactions_ctx_transaction_id]  DEFAULT ((0)),
  [ctx_status] [int] NOT NULL CONSTRAINT [DF_cj_transactions_ctx_status]  DEFAULT ((0)),
  [ctx_request_msg_id] [bigint] NULL,
  [ctx_response_msg_id] [bigint] NULL,
 CONSTRAINT [PK_cj_transactions] PRIMARY KEY CLUSTERED 
(
  [ctx_sequence_number] ASC,
  [ctx_queue_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_cj_transactions] ON [dbo].[cj_transactions] 
(
  [ctx_terminal_id] ASC,
  [ctx_transaction_id] ASC,
  [ctx_sequence_id] ASC,
  [ctx_session_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0 - Running, 1 - Finished' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'cj_transactions', @level2type=N'COLUMN',@level2name=N'ctx_status'
GO
/****** Object:  Table [dbo].[db_users]    Script Date: 12/09/2008 16:30:21 ******/
CREATE TABLE [dbo].[db_users](
  [du_username] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
  [du_password] [binary](40) NOT NULL,
 CONSTRAINT [PK_db_users] PRIMARY KEY CLUSTERED 
(
  [du_username] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[db_version]    Script Date: 12/09/2008 16:30:22 ******/
CREATE TABLE [dbo].[db_version](
  [db_client_id] [int] NOT NULL,
  [db_common_build_id] [int] NOT NULL,
  [db_client_build_id] [int] NOT NULL,
  [db_release_id] [int] NOT NULL,
  [db_updated_script] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  [db_updated] [datetime] NULL,
  [db_description] [nvarchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_db_version] PRIMARY KEY CLUSTERED 
(
  [db_client_id] ASC,
  [db_common_build_id] ASC,
  [db_client_build_id] ASC,
  [db_release_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[db_version_interface_3gs]    Script Date: 06/15/2010 18:04:15 ******/
CREATE TABLE [dbo].[db_version_interface_3gs](
	[db_client_id] [int] NOT NULL,
	[db_common_build_id] [int] NOT NULL,
	[db_client_build_id] [int] NOT NULL,
	[db_release_id] [int] NOT NULL,
	[db_updated_script] [nvarchar](50) NULL,
	[db_updated] [datetime] NULL,
	[db_description] [nvarchar](1000) NULL,
 CONSTRAINT [PK_db_version_interface_3gs] PRIMARY KEY CLUSTERED 
(
	[db_client_id] ASC,
	[db_common_build_id] ASC,
	[db_client_build_id] ASC,
	[db_release_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[draw_tickets]    Script Date: 06/15/2010 18:04:15 ******/
CREATE TABLE [dbo].[draw_tickets](
	[dt_id] [bigint] IDENTITY(1,1) NOT NULL,
	[dt_created] [datetime] NOT NULL CONSTRAINT [DF_draw_tickets_dt_created]  DEFAULT (getdate()),
	[dt_draw_id] [bigint] NOT NULL,
	[dt_account_id] [bigint] NOT NULL,
	[dt_first_number] [bigint] NOT NULL,
	[dt_last_number] [bigint] NOT NULL,
	[dt_voucher_id] [bigint] NULL
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_draw_id] ON [dbo].[draw_tickets] 
(
	[dt_draw_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_first_number] ON [dbo].[draw_tickets] 
(
	[dt_first_number] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_last_number] ON [dbo].[draw_tickets] 
(
	[dt_last_number] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[draws]    Script Date: 06/15/2010 18:02:57 ******/
CREATE TABLE [dbo].[draws](
	[dr_id] [bigint] IDENTITY(1,1) NOT NULL,
	[dr_name] [nvarchar](50) NOT NULL,
	[dr_starting_date] [datetime] NOT NULL,
	[dr_ending_date] [datetime] NOT NULL,
	[dr_last_number] [bigint] NOT NULL CONSTRAINT [DF_draws_dr_last_number]  DEFAULT ((-1)),
	[dr_number_price] [money] NOT NULL,
	[dr_status] [int] NOT NULL CONSTRAINT [DF_draws_dr_status]  DEFAULT ((0)),
	[dr_limited] [bit] NOT NULL CONSTRAINT [DF_draws_dr_limit_1]  DEFAULT ((0)),
	[dr_limit] [int] NOT NULL CONSTRAINT [DF_draws_dr_limit_2]  DEFAULT ((10)),
	[dr_limit_per_voucher] [int] NOT NULL CONSTRAINT [DF_draws_dr_limit_per_voucher]  DEFAULT ((300)),
	[dr_initial_number] [bigint] NOT NULL CONSTRAINT [DF_dr_initial_number]  DEFAULT ((1)),
	[dr_max_number] [bigint] NOT NULL CONSTRAINT [DF_dr_max_number]  DEFAULT ((999999999)),
	[dr_credit_type] [int] NOT NULL CONSTRAINT [DF_dr_credit_type]  DEFAULT ((0)),
	[dr_header] [nvarchar](260) NULL,
	[dr_footer] [nvarchar](512) NULL,
	[dr_detail1] [nvarchar](260) NULL,
	[dr_detail2] [nvarchar](260) NULL,
	[dr_detail3] [nvarchar](260) NULL,
	[dr_min_played_pct] [numeric](6, 2) NULL,
	[dr_min_spent_pct] [numeric](5, 2) NULL,
	[dr_min_cash_in] [money] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[event_history]    Script Date: 05/12/2009 16:39:09 ******/
CREATE TABLE [dbo].[event_history](
	[eh_event_id] [bigint] IDENTITY(1,1) NOT NULL,
	[eh_reported] [datetime] NOT NULL CONSTRAINT [DF_event_history_eh_reported]  DEFAULT (getdate()),
	[eh_terminal_id] [int] NOT NULL,
	[eh_session_id] [int] NULL,
	[eh_datetime] [datetime] NOT NULL,
	[eh_event_type] [int] NOT NULL,
	[eh_event_data] [nvarchar](50) NULL,
	[eh_ack_date] [datetime] NULL,
	[eh_ack_user] [nvarchar](50) NULL,
	[eh_timestamp] [timestamp] NOT NULL,
	[eh_device_code] [int] NULL,
	[eh_device_status] [int] NULL,
	[eh_device_priority] [int] NULL,
	[eh_operation_code] [int] NULL,
	[eh_operation_data] [money] NULL,
 CONSTRAINT [PK_event_history] PRIMARY KEY CLUSTERED 
(
	[eh_datetime] ASC,
	[eh_terminal_id] ASC,
	[eh_event_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_event_timestamp] ON [dbo].[event_history] 
(
	[eh_timestamp] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[game_meters]    Script Date: 19/04/2010  ******/
CREATE TABLE [dbo].[game_meters](
	[gm_terminal_id] [int] NOT NULL,
	[gm_game_base_name] [nvarchar](50) NOT NULL,
	[gm_wcp_sequence_id] [bigint] NOT NULL,
	[gm_denomination] [money] NOT NULL,
	[gm_played_count] [bigint] NOT NULL,
	[gm_played_amount] [money] NOT NULL,
	[gm_won_count] [bigint] NOT NULL,
	[gm_won_amount] [money] NOT NULL,
	[gm_jackpot_amount] [money] NOT NULL,
	[gm_handpays_amount] [money] NOT NULL CONSTRAINT [DF_game_meters_gm_handpays_amount]  DEFAULT ((0)),	
	[gm_last_reported] [datetime] NOT NULL,
	[gm_delta_game_name] [nvarchar](50) NULL,
	[gm_delta_played_count] [bigint] NULL CONSTRAINT [DF_game_meters_gm_delta_played_count]  DEFAULT ((0)),
	[gm_delta_played_amount] [money] NULL CONSTRAINT [DF_game_meters_gm_delta_played_amount]  DEFAULT ((0)),
	[gm_delta_won_count] [bigint] NULL CONSTRAINT [DF_game_meters_gm_delta_won_count]  DEFAULT ((0)),
	[gm_delta_won_amount] [money] NULL CONSTRAINT [DF_game_meters_gm_delta_won_amount]  DEFAULT ((0)),
	[gm_delta_jackpot_amount] [money] NULL CONSTRAINT [DF_game_meters_gm_delta_jackpot_amount]  DEFAULT ((0)),
	[gm_delta_handpays_amount] [money] NULL CONSTRAINT [DF_game_meters_gm_delta_handpays_amount]  DEFAULT ((0)),
	[gm_delta_updating] [bit] NULL CONSTRAINT [DF_game_meters_gm_delta_updating]  DEFAULT ((0)),
 CONSTRAINT [PK_game_meters] PRIMARY KEY CLUSTERED 
(
	[gm_terminal_id] ASC,
	[gm_game_base_name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[games]    Script Date: 12/09/2008 16:30:21 ******/
CREATE TABLE [dbo].[games](
  [gm_game_id] [int] IDENTITY(1,1) NOT NULL,
  [gm_name] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
  [gm_timestamp] [timestamp] NULL,
 CONSTRAINT [PK_games] PRIMARY KEY CLUSTERED 
(
  [gm_game_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_games] ON [dbo].[games] 
(
  [gm_name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[gds_group_elements]    Script Date: 12/09/2008 16:30:22 ******/
CREATE TABLE [dbo].[gds_group_elements](
  [gge_group_id] [bigint] NOT NULL,
  [gge_is_group] [bit] NOT NULL CONSTRAINT [DF_gds_group_elements_gge_is_group]  DEFAULT ((0)),
  [gge_element_id] [bigint] NOT NULL,
 CONSTRAINT [IX_gds_group_elements] UNIQUE NONCLUSTERED 
(
  [gge_group_id] ASC,
  [gge_is_group] ASC,
  [gge_element_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0 - Single Element; 1 - Group Definition' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'gds_group_elements', @level2type=N'COLUMN',@level2name=N'gge_is_group'
GO
/****** Object:  Table [dbo].[gds_groups]    Script Date: 12/09/2008 16:30:22 ******/
CREATE TABLE [dbo].[gds_groups](
  [gg_group_id] [bigint] IDENTITY(1,1) NOT NULL,
  [gg_group_is_explicit] [bit] NOT NULL,
  [gg_element_type] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
  [gg_name] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  [gg_sql_definition] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_gds_groups] PRIMARY KEY CLUSTERED 
(
  [gg_group_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0 - Group defined through SQL definition; 1 - Group defined by explicit assignment' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'gds_groups', @level2type=N'COLUMN',@level2name=N'gg_group_is_explicit'
GO
/****** Object:  Table [dbo].[gds_platforms]    Script Date: 12/09/2008 16:30:21 ******/
CREATE TABLE [dbo].[gds_platforms](
  [gp_platform_id] [bigint] IDENTITY(1,1) NOT NULL,
  [gp_name] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[gds_schedule_details]    Script Date: 12/09/2008 16:30:21 ******/
CREATE TABLE [dbo].[gds_schedule_details](
  [gsd_id] [bigint] IDENTITY(1,1) NOT NULL,
  [gsd_schedule_id] [bigint] NOT NULL,
  [gsd_order] [int] NOT NULL CONSTRAINT [DF_gds_schedule_details_gsd_order]  DEFAULT ((0)),
  [gsd_step_index] [int] NOT NULL,
  [gsd_datetime] [datetime] NOT NULL,
  [gsd_terminal_id] [int] NOT NULL,
  [gsd_action_type] [int] NOT NULL,
  [gsd_action_data] [bigint] NULL,
  [gsd_lock_service_id] [int] NULL,
  [gsd_lock_datetime] [datetime] NULL,
  [gsd_action_status] [int] NOT NULL CONSTRAINT [DF_gds_schedule_details_gsd_action_status]  DEFAULT ((0)),
  [gsd_progress] [decimal](18, 0) NOT NULL CONSTRAINT [DF_gds_schedule_details_gds_progress]  DEFAULT ((0)),
  [gsd_txt] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  [gsd_recurrence] [int] NULL,
 CONSTRAINT [PK_gds_schedule_details] PRIMARY KEY CLUSTERED 
(
  [gsd_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_gds_schedule_details] ON [dbo].[gds_schedule_details] 
(
  [gsd_schedule_id] ASC,
  [gsd_step_index] ASC,
  [gsd_datetime] ASC,
  [gsd_terminal_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[gds_schedule_steps]    Script Date: 12/09/2008 16:30:22 ******/
CREATE TABLE [dbo].[gds_schedule_steps](
  [gss_schedule_id] [bigint] NOT NULL,
  [gss_step_index] [int] NOT NULL,
  [gss_target_type] [int] NOT NULL,
  [gss_target_id] [bigint] NOT NULL,
  [gss_action_type] [int] NOT NULL,
  [gss_action_data] [bigint] NULL,
 CONSTRAINT [PK_gds_schedule_steps] UNIQUE NONCLUSTERED 
(
  [gss_schedule_id] ASC,
  [gss_step_index] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[gds_schedule_time]    Script Date: 12/09/2008 16:30:21 ******/
CREATE TABLE [dbo].[gds_schedule_time](
  [gst_schedule_id] [bigint] IDENTITY(1,1) NOT NULL,
  [gst_order] [int] NOT NULL,
  [gst_enabled] [bit] NOT NULL CONSTRAINT [DF_gds_schedule_time_gst_enabled]  DEFAULT ((1)),
  [gst_recurrence] [int] NOT NULL,
  [gst_description] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  [gst_next_run] [datetime] NULL,
  [gst_last_run] [datetime] NULL,
  [gst_created] [datetime] NOT NULL CONSTRAINT [DF_gds_schedule_time_gst_created]  DEFAULT (getdate()),
  [gst_start] [datetime] NOT NULL,
  [gst_end] [datetime] NULL,
  [gst_lock_service_id] [int] NULL,
  [gst_lock_datetime] [datetime] NULL,
 CONSTRAINT [PK_gst_schedule_time] PRIMARY KEY CLUSTERED 
(
  [gst_schedule_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_gst_order] ON [dbo].[gds_schedule_time] 
(
  [gst_order] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0 - One time; 1 - Daily; 2 - Week Days; 3 - Weekends; 4 - Weekly' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'gds_schedule_time', @level2type=N'COLUMN',@level2name=N'gst_recurrence'
GO
/****** Object:  Table [dbo].[general_params]    Script Date: 12/09/2008 16:30:22 ******/
CREATE TABLE [dbo].[general_params](
  [gp_group_key] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
  [gp_subject_key] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
  [gp_key_value] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_general_params] PRIMARY KEY CLUSTERED 
(
  [gp_group_key] ASC,
  [gp_subject_key] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[gift_instances]    Script Date: 10/06/2010 10:55:55 ******/
CREATE TABLE [dbo].[gift_instances](
	[gin_gift_instance_id] [bigint] IDENTITY(1,1) NOT NULL,
	[gin_oper_request_id] [bigint] NOT NULL,
	[gin_oper_delivery_id] [bigint] NULL,
	[gin_account_id] [bigint] NOT NULL,
	[gin_gift_id] [bigint] NOT NULL,
	[gin_gift_name] [nvarchar](50) NOT NULL,
	[gin_gift_type] [int] NOT NULL,
	[gin_points] [money] NOT NULL,
	[gin_conversion_to_nrc] [money] NULL,
	[gin_spent_points] [money] NOT NULL,
	[gin_requested] [datetime] NOT NULL,
	[gin_delivered] [datetime] NULL,
	[gin_expiration] [datetime] NOT NULL,
	[gin_request_status] [int] NOT NULL CONSTRAINT [DF_gin_request_status] DEFAULT (0),
	[gin_num_items] [int] NOT NULL CONSTRAINT [DF_gin_num_items] DEFAULT (1),
	[gin_data_01] [bigint] NULL,
 CONSTRAINT [PK_gift_instances] PRIMARY KEY CLUSTERED 
(
	[gin_gift_instance_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_gin_request_status] ON [dbo].[gift_instances] 
(
	[gin_request_status] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[gifts]    Script Date: 09/20/2010 17:49:15 ******/
CREATE TABLE [dbo].[gifts](
	[gi_gift_id] [bigint] IDENTITY(1,1) NOT NULL,
	[gi_name] [nvarchar](50) NOT NULL,
	[gi_points] [money] NOT NULL,
	[gi_conversion_to_nrc] [money] NULL,
	[gi_available] [bit] NOT NULL,
	[gi_type] [int] NOT NULL,
	[gi_current_stock] [int] NOT NULL CONSTRAINT [DF_gi_current_stock] DEFAULT (0),
	[gi_request_counter] [int] NOT NULL CONSTRAINT [DF_gi_request_counter] DEFAULT (0),
	[gi_delivery_counter] [int] NOT NULL CONSTRAINT [DF_gi_delivery_counter] DEFAULT (0)
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[gui_audit]    Script Date: 12/09/2008 16:30:22 ******/
CREATE TABLE [dbo].[gui_audit](
  [ga_audit_id] [bigint] NOT NULL,
  [ga_gui_id] [int] NOT NULL,
  [ga_gui_user_id] [int] NULL,
  [ga_gui_username] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
  [ga_computer_name] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
  [ga_datetime] [datetime] NOT NULL,
  [ga_audit_code] [int] NOT NULL,
  [ga_audit_level] [int] NOT NULL,
  [ga_item_order] [int] NOT NULL,
  [ga_nls_id] [int] NULL,
  [ga_nls_param01] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  [ga_nls_param02] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  [ga_nls_param03] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  [ga_nls_param04] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  [ga_nls_param05] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_gui_audit] PRIMARY KEY CLUSTERED 
(
  [ga_audit_id] ASC,
  [ga_item_order] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[gui_forms]    Script Date: 12/09/2008 16:30:21 ******/
CREATE TABLE [dbo].[gui_forms](
  [gf_gui_id] [int] NOT NULL,
  [gf_form_id] [int] NOT NULL,
  [gf_form_order] [int] NOT NULL,
  [gf_nls_id] [int] NOT NULL,
 CONSTRAINT [PK_gui_forms] PRIMARY KEY CLUSTERED 
(
  [gf_gui_id] ASC,
  [gf_form_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[gui_profile_forms]    Script Date: 12/09/2008 16:30:22 ******/
CREATE TABLE [dbo].[gui_profile_forms](
  [gpf_profile_id] [int] NOT NULL,
  [gpf_gui_id] [int] NOT NULL,
  [gpf_form_id] [int] NOT NULL,
  [gpf_read_perm] [bit] NOT NULL,
  [gpf_write_perm] [bit] NOT NULL,
  [gpf_delete_perm] [bit] NOT NULL,
  [gpf_execute_perm] [bit] NOT NULL,
 CONSTRAINT [PK_gui_profile_forms] PRIMARY KEY CLUSTERED 
(
  [gpf_profile_id] ASC,
  [gpf_gui_id] ASC,
  [gpf_form_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[gui_user_profiles]    Script Date: 12/09/2008 16:30:21 ******/
CREATE TABLE [dbo].[gui_user_profiles](
  [gup_profile_id] [int] NOT NULL,
  [gup_name] [nvarchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
  [gup_timestamp] [timestamp] NULL,
 CONSTRAINT [PK_gui_user_profiles] PRIMARY KEY CLUSTERED 
(
  [gup_profile_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[gui_users]    Script Date: 12/09/2008 16:30:22 ******/
CREATE TABLE [dbo].[gui_users](
  [gu_user_id] [int] NOT NULL,
  [gu_profile_id] [int] NOT NULL,
  [gu_username] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
  [gu_enabled] [bit] NOT NULL,
  [gu_password] [binary](40) NOT NULL,
  [gu_not_valid_before] [datetime] NOT NULL,
  [gu_not_valid_after] [datetime] NULL,
  [gu_last_changed] [datetime] NULL,
  [gu_password_exp] [datetime] NULL,
  [gu_pwd_chg_req] [bit] NOT NULL,
  [gu_login_failures] [int] NULL,
  [gu_password_h1] [binary](40) NULL,
  [gu_password_h2] [binary](40) NULL,
  [gu_password_h3] [binary](40) NULL,
  [gu_password_h4] [binary](40) NULL,
  [gu_password_h5] [binary](40) NULL,
  [gu_full_name] [nchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  [gu_timestamp] [timestamp] NULL,
 CONSTRAINT [PK_gui_users] PRIMARY KEY CLUSTERED 
(
  [gu_user_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UNQ_gui_users] UNIQUE NONCLUSTERED 
(
  [gu_username] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[handpays]    Script Date: 06/15/2010 18:04:15 ******/
CREATE TABLE [dbo].[handpays](
  [hp_terminal_id] [int] NULL,
  [hp_game_base_name] [nvarchar](50) NULL,
  [hp_datetime] [datetime] NOT NULL CONSTRAINT [DF_handpays_hp_datetime]  DEFAULT (getdate()),
  [hp_previous_meters] [datetime] NULL,
  [hp_amount] [money] NOT NULL,
  [hp_te_name] [nvarchar](50) NULL,
  [hp_te_provider_id] [nvarchar](50) NULL,
  [hp_movement_id] [bigint] NULL,
  [hp_type] [int] NOT NULL CONSTRAINT [DF_handpays_hp_type] DEFAULT ((0)),
  [hp_play_session_id] [bigint] NULL,
  [hp_site_jackpot_index] [int] NULL,
  [hp_site_jackpot_name] [nvarchar](50) NULL,
  [hp_site_jackpot_awarded_on_terminal_id] [int] NULL,
  [hp_site_jackpot_awarded_to_account_id] [bigint] NULL,
  [hp_status] [int] NULL CONSTRAINT [DF_handpays_hp_status] DEFAULT ((0))	
) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_hp_datetime_terminal_id] ON [dbo].[handpays] 
(
  [hp_datetime] ASC,
  [hp_terminal_id] ASC,
  [hp_amount] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_hp_type_mov_date] ON [dbo].[handpays] 
(
  [hp_type] ASC,
  [hp_movement_id] ASC,
  [hp_datetime] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Objeto:  Table [dbo].[hpc_meter]    Fecha de la secuencia de comandos: 07/21/2010 14:53:40 ******/
CREATE TABLE [dbo].[hpc_meter](
      [hpc_terminal_id] [int] NOT NULL,
      [hpc_wcp_sequence_id] [bigint] NOT NULL,
      [hpc_handpays_amount] [money] NOT NULL CONSTRAINT [DF_hpc_meter_gm_handpays_amount]  DEFAULT ((0)),
      [hpc_last_reported] [datetime] NOT NULL,
 CONSTRAINT [PK_hpc_meter] PRIMARY KEY CLUSTERED 
(
      [hpc_terminal_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[licences]    Script Date: 05/12/2009 16:40:58 ******/
CREATE TABLE [dbo].[licences](
  [wl_id] [int] IDENTITY(1,1) NOT NULL,
  [wl_licence] [xml] NOT NULL,
  [wl_insertion_date] [datetime] NOT NULL CONSTRAINT [DF_wsi_licence_wl_insertion_date]  DEFAULT (getdate()),
  [wl_expiration_date] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mailing_instances]    Script Date: 01/14/2011 15:35:52 ******/
CREATE TABLE [dbo].[mailing_instances](
	[mi_mailing_instance_id] [bigint] IDENTITY(1,1) NOT NULL,
	[mi_prog_id] [bigint] NOT NULL,
	[mi_prog_date] [datetime] NOT NULL,
	[mi_prog_data] [bigint] NOT NULL,
	[mi_name] [nvarchar](50) NOT NULL,
	[mi_type] [int] NOT NULL,
	[mi_datetime] [datetime] NOT NULL,
	[mi_address_list] [nvarchar](500) NOT NULL,
	[mi_subject] [nvarchar](200) NOT NULL,
	[mi_message] [nvarchar](max) NOT NULL,
	[mi_status] [int] NOT NULL CONSTRAINT [DF_mailing_instances_mi_status]  DEFAULT ((0)),
	[mi_result] [nvarchar](500) NULL,
 CONSTRAINT [PK_mailing_instances] PRIMARY KEY CLUSTERED 
(
	[mi_mailing_instance_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Index [IX_mailing_instances]    Script Date: 01/14/2011 15:39:04 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_mailing_instances] ON [dbo].[mailing_instances] 
(
	[mi_prog_id] ASC,
	[mi_prog_date] ASC,
	[mi_prog_data] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Time in minutes (0..1439)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'mailing_instances', @level2type=N'COLUMN',@level2name=N'mi_prog_data'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1: Statistics' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'mailing_instances', @level2type=N'COLUMN',@level2name=N'mi_type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0: Pending; 1: Running; 2: Successful; 3: Failed' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'mailing_instances', @level2type=N'COLUMN',@level2name=N'mi_status'
GO
/****** Object:  Table [dbo].[mailing_programming]    Script Date: 01/14/2011 15:32:18 ******/
CREATE TABLE [dbo].[mailing_programming](
	[mp_prog_id] [bigint] IDENTITY(1,1) NOT NULL,
	[mp_name] [nvarchar](50) NOT NULL,
	[mp_enabled] [bit] NOT NULL CONSTRAINT [DF_mailing_scheduling_ms_enabled]  DEFAULT ((0)),
	[mp_type] [int] NOT NULL,
	[mp_address_list] [nvarchar](500) NOT NULL,
	[mp_subject] [nvarchar](200) NOT NULL,
	[mp_schedule_weekday] [int] NOT NULL,
	[mp_schedule_time_from] [int] NOT NULL,
	[mp_schedule_time_to] [int] NOT NULL,
	[mp_schedule_time_step] [int] NOT NULL,
 CONSTRAINT [PK_mailing_scheduling] PRIMARY KEY CLUSTERED 
(
	[mp_prog_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1: Statistics' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'mailing_programming', @level2type=N'COLUMN',@level2name=N'mp_type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Time in minutes (0..1439)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'mailing_programming', @level2type=N'COLUMN',@level2name=N'mp_schedule_time_from'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Time in minutes (0..1439)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'mailing_programming', @level2type=N'COLUMN',@level2name=N'mp_schedule_time_to'
GO
/****** Object:  Table [dbo].[mb_movements]    Script Date: 11/28/2009 15:28:24 ******/
CREATE TABLE [dbo].[mb_movements](
  [mbm_movement_id] [bigint] IDENTITY(1,1) NOT NULL,
  [mbm_type] [int] NOT NULL,
  [mbm_datetime] [datetime] NOT NULL,
  [mbm_cashier_session_id] [bigint] NOT NULL,
  [mbm_mb_id] [bigint] NOT NULL,
  [mbm_terminal_id] [int] NULL,
  [mbm_terminal_name] [nvarchar](50) NULL,
  [mbm_player_acct_id] [bigint] NULL,
  [mbm_player_trackdata] [nvarchar](50) NULL,
  [mbm_wcp_sequence_id] [bigint] NULL,
  [mbm_wcp_transaction_id] [bigint] NULL,
  [mbm_acct_movement_id] [bigint] NULL,
  [mbm_cashier_id] [int] NULL,
  [mbm_cashier_name] [nvarchar](50) NULL,
  [mbm_initial_balance] [money] NOT NULL,
  [mbm_add_amount] [money] NOT NULL,
  [mbm_sub_amount] [money] NOT NULL,
  [mbm_final_balance] [money] NOT NULL,
  [mbm_operation_id] [bigint] NULL,
 CONSTRAINT [PK_mb_movements] PRIMARY KEY CLUSTERED 
(
  [mbm_movement_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Index [IX_mb_movements]    Script Date: 12/02/2009 11:14:38 ******/
CREATE NONCLUSTERED INDEX [IX_mb_movements] ON [dbo].[mb_movements] 
(
  [mbm_cashier_session_id] ASC,
  [mbm_datetime] ASC,
  [mbm_movement_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[meters]    Script Date: 12/09/2008 16:30:22 ******/
CREATE TABLE [dbo].[meters](
  [me_meter_id] [bigint] IDENTITY(1,1) NOT NULL,
  [me_terminal_id] [int] NOT NULL,
  [me_played_count] [int] NOT NULL,
  [me_played_amount] [money] NOT NULL,
  [me_won_count] [int] NOT NULL,
  [me_won_amount] [money] NOT NULL,
  [me_cash_in] [money] NOT NULL,
  [me_cash_out] [money] NOT NULL,
  [me_last_reported] [datetime] NOT NULL,
  [me_timestamp] [timestamp] NULL,
 CONSTRAINT [PK_meters] PRIMARY KEY CLUSTERED 
(
  [me_meter_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Terminals] ON [dbo].[meters] 
(
  [me_terminal_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mobile_banks]    Script Date: 11/28/2009 15:28:24 ******/
CREATE TABLE [dbo].[mobile_banks](
  [mb_account_id] [bigint] IDENTITY(1,1) NOT NULL,
  [mb_account_type] [smallint] NOT NULL CONSTRAINT [DF_mobile_banks_mb_type]  DEFAULT ((0)),
  [mb_holder_name] [nvarchar](50) NULL,
  [mb_blocked] [bit] NOT NULL,
  [mb_track_data] [nvarchar](50) NULL,
  [mb_balance] [money] NOT NULL CONSTRAINT [DF_mobile_bank_mb_limit]  DEFAULT ((0)),
  [mb_initial_limit] [money] NOT NULL CONSTRAINT [DF_Table_1_mb_cash]  DEFAULT ((0)),
  [mb_cash_in] [money] NOT NULL CONSTRAINT [DF_mobile_banks_mb_cash_in]  DEFAULT ((0)),
  [mb_deposit] [money] NOT NULL CONSTRAINT [DF_mobile_banks_mb_deposit]  DEFAULT ((0)),
  [mb_extension] [money] NOT NULL CONSTRAINT [DF_mobile_banks_mb_extension]  DEFAULT ((0)),
  [mb_pending_cash] [money] NOT NULL CONSTRAINT [DF_mobile_banks_mb_pending_cash]  DEFAULT ((0)),
  [mb_pin] [nvarchar](12) NOT NULL,
  [mb_cashier_session_id] [bigint] NULL,
  [mb_failed_login_attempts] [smallint] NOT NULL CONSTRAINT [DF_mobile_banks_mb_failed_login_attempts]  DEFAULT ((0)),
  [mb_last_activity] [datetime] NULL,
  [mb_last_terminal_id] [int] NULL,
  [mb_last_terminal_name] [nvarchar](50) NULL,
  [mb_timestamp] [timestamp] NULL,
  [mb_track_number]  AS (CONVERT([bigint],[mb_track_data],0)),
 CONSTRAINT [PK_mobile_bank] PRIMARY KEY CLUSTERED 
(
  [mb_account_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Index [IX_mb_track_data]    Script Date: 07/27/2010 15:13:39 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_mb_track_data] ON [dbo].[mobile_banks] 
(
  [mb_track_data] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = OFF) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[money_meters]    Script Date: 12/09/2008 16:30:22 ******/
CREATE TABLE [dbo].[money_meters](
  [mm_meter_id] [bigint] NOT NULL,
  [mm_cash_type] [int] NOT NULL,
  [mm_money_type] [int] NOT NULL,
  [mm_face_value] [money] NOT NULL,
  [mm_count] [int] NOT NULL,
  [mm_amount] [money] NOT NULL,
  [mm_last_reported] [datetime] NOT NULL,
  [mm_timestamp] [timestamp] NULL,
 CONSTRAINT [PK_money_meters] PRIMARY KEY CLUSTERED 
(
  [mm_meter_id] ASC,
  [mm_cash_type] ASC,
  [mm_money_type] ASC,
  [mm_face_value] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1-CashIn, 2-CashOut' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'money_meters', @level2type=N'COLUMN',@level2name=N'mm_cash_type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1-Note, 2-Coin' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'money_meters', @level2type=N'COLUMN',@level2name=N'mm_money_type'
GO
/****** Object:  Table [dbo].[play_sessions]    Script Date: 12/09/2008 16:30:22 ******/
CREATE TABLE [dbo].[play_sessions](
  [ps_play_session_id] [bigint] IDENTITY(1,1) NOT NULL,
  [ps_account_id] [bigint] NULL,
  [ps_terminal_id] [int] NOT NULL,
  [ps_type] [int] NOT NULL,
  [ps_type_data] [xml] NULL,
  [ps_status] [int] NOT NULL CONSTRAINT [DF_play_sessions_ps_status]  DEFAULT ((0)),
  [ps_started] [datetime] NOT NULL CONSTRAINT [DF_play_sessions_ps_started]  DEFAULT (getdate()),
  [ps_initial_balance] [money] NOT NULL,
  [ps_played_count] [int] NOT NULL CONSTRAINT [DF_play_sessions_ps_played_count]  DEFAULT ((0)),
  [ps_played_amount] [money] NOT NULL CONSTRAINT [DF_play_sessions_ps_played_amount]  DEFAULT ((0)),
  [ps_won_count] [int] NOT NULL CONSTRAINT [DF_play_sessions_ps_won_count]  DEFAULT ((0)),
  [ps_won_amount] [money] NOT NULL CONSTRAINT [DF_play_sessions_ps_won_amount]  DEFAULT ((0)),
  [ps_cash_in] [money] NOT NULL CONSTRAINT [DF_play_sessions_ps_cash_in]  DEFAULT ((0)),
  [ps_cash_out] [money] NOT NULL CONSTRAINT [DF_play_sessions_ps_cash_out]  DEFAULT ((0)),
  [ps_finished] [datetime] NULL,
  [ps_final_balance] [money] NULL,
  [ps_timestamp] [timestamp] NULL,
  [ps_locked] [datetime] NULL,
  [ps_stand_alone] [bit] NOT NULL CONSTRAINT DF_play_sessions_ps_stand_alone DEFAULT ((0)),
  [ps_promo] [bit] NOT NULL CONSTRAINT DF_play_sessions_ps_promo DEFAULT ((0)),
  [ps_wcp_transaction_id] [bigint] NULL CONSTRAINT [DF_play_sessions_ps_wcp_transaction_id]  DEFAULT ((0)), 
  [ps_total_cash_in] AS ([ps_initial_balance]+[ps_cash_in]) PERSISTED,
  [ps_total_cash_out] AS (isnull([ps_final_balance],(0))+[ps_cash_out]) PERSISTED,
  [ps_total_played] AS ([ps_played_amount]) PERSISTED NOT NULL,
  [ps_total_won] AS ([ps_won_amount]),
  [ps_redeemable_cash_in] [money] NOT NULL CONSTRAINT [DF_play_sessions_ps_redeemable_cash_in]  DEFAULT ((0)),
  [ps_redeemable_cash_out] [money] NOT NULL CONSTRAINT [DF_play_sessions_ps_redeemable_cash_out]  DEFAULT ((0)),
  [ps_redeemable_played] [money] NOT NULL CONSTRAINT [DF_play_sessions_ps_redeemable_played]  DEFAULT ((0)),
  [ps_redeemable_won] [money] NOT NULL CONSTRAINT [DF_play_sessions_ps_redeemable_won]  DEFAULT ((0)),
  [ps_non_redeemable_cash_in] [money] NOT NULL CONSTRAINT [DF_play_sessions_ps_non_redeemable_cash_in]  DEFAULT ((0)),
  [ps_non_redeemable_cash_out] [money] NOT NULL CONSTRAINT [DF_play_sessions_ps_non_redeemable_cash_out]  DEFAULT ((0)),
  [ps_non_redeemable_played] [money] NOT NULL CONSTRAINT [DF_play_sessions_ps_non_redeemable_played]  DEFAULT ((0)),
  [ps_non_redeemable_won] [money] NOT NULL CONSTRAINT [DF_play_sessions_ps_non_redeemable_won]  DEFAULT ((0)),
  CONSTRAINT [PK_play_sessions] PRIMARY KEY CLUSTERED 
(
  [ps_play_session_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0-Opened; 1-Closed; 2-Abandoned' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'play_sessions', @level2type=N'COLUMN',@level2name=N'ps_status'
GO
/****** Object:  Index [IX_ps_status]    Script Date: 01/09/2009 10:18:00 ******/
CREATE NONCLUSTERED INDEX [IX_ps_status] ON [dbo].[play_sessions] 
(
	[ps_status] ASC,
	[ps_terminal_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Objeto:  Index [IX_ps_terminal_id]    Fecha de la secuencia de comandos: 07/19/2010 03:47:51 ******/
CREATE NONCLUSTERED INDEX [IX_ps_terminal_id] ON [dbo].[play_sessions] 
(
	[ps_terminal_id] ASC,
	[ps_play_session_id] ASC,
	[ps_status] ASC,
	[ps_stand_alone] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_ps_finished] ON [dbo].[play_sessions] 
(
	[ps_terminal_id] ASC,
	[ps_finished] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY];
GO
/****** Objeto:  Index [IX_ps_started]    Fecha de la secuencia de comandos: 01/27/2011 10:19:25 ******/
CREATE NONCLUSTERED INDEX [IX_ps_started] ON [dbo].[play_sessions] 
(
	[ps_started] ASC,
	[ps_status] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Objeto:  Index [IX_ps_started_terminal_id]    Fecha de la secuencia de comandos: 02/09/2011 12:15:28 ******/
CREATE NONCLUSTERED INDEX [IX_ps_started_terminal_id] ON [dbo].[play_sessions] 
(
	[ps_started] ASC,
	[ps_terminal_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[plays]    Script Date: 12/09/2008 16:30:22 ******/
CREATE TABLE [dbo].[plays](
  [pl_play_id] [bigint] IDENTITY(1,1) NOT NULL,
  [pl_play_session_id] [bigint] NULL,
  [pl_account_id] [bigint] NULL,
  [pl_terminal_id] [int] NOT NULL,
  [pl_wcp_sequence_id] [bigint] NOT NULL,
  [pl_wcp_transaction_id] [bigint] NOT NULL,
  [pl_datetime] [datetime] NOT NULL,
  [pl_game_id] [int] NOT NULL,
  [pl_initial_balance] [money] NOT NULL,
  [pl_played_amount] [money] NOT NULL,
  [pl_won_amount] [money] NOT NULL,
  [pl_final_balance] [money] NOT NULL,
  [pl_transferred] [bit] NULL CONSTRAINT [DF_plays_pl_transferred]  DEFAULT ((0)),
 CONSTRAINT [PK_plays] PRIMARY KEY CLUSTERED 
(
  [pl_play_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_plays] ON [dbo].[plays] 
(
  [pl_play_session_id] ASC,
  [pl_datetime] ASC,
  [pl_play_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_plays_transaction] ON [dbo].[plays] 
(
  [pl_terminal_id] ASC,
  [pl_wcp_transaction_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[promotions]    Script Date: 09/20/2010 17:51:21 ******/
CREATE TABLE [dbo].[promotions](
	[pm_promotion_id] [bigint] IDENTITY(1,1) NOT NULL,
	[pm_name] [nvarchar](50) NULL,
	[pm_enabled] [bit] NOT NULL,
	[pm_type] [int] NOT NULL,
	[pm_date_start] [datetime] NOT NULL,
	[pm_date_finish] [datetime] NOT NULL,
	[pm_schedule_weekday] [int] NOT NULL,
	[pm_schedule1_time_from] [int] NOT NULL,
	[pm_schedule1_time_to] [int] NOT NULL,
	[pm_schedule2_enabled] [bit] NOT NULL,
	[pm_schedule2_time_from] [int] NULL,
	[pm_schedule2_time_to] [int] NULL,
	[pm_gender_filter] [int] NOT NULL,
	[pm_birthday_filter] [int] NOT NULL,
	[pm_expiration_type] [int] NOT NULL,
	[pm_expiration_value] [int] NOT NULL,
	[pm_min_cash_in] [money] NOT NULL,
	[pm_min_cash_in_reward] [money] NOT NULL,
	[pm_cash_in] [money] NOT NULL,
	[pm_cash_in_reward] [money] NOT NULL,
	[pm_won_lock] [money] NULL,
	[pm_num_tokens] [int] NOT NULL,
	[pm_token_name] [nvarchar](50) NULL,
	[pm_token_reward] [money] NOT NULL,
	[pm_daily_limit] [money] NULL,
	[pm_monthly_limit] [money] NULL,
	[pm_level_filter] [int] NOT NULL CONSTRAINT [DF_promotions_pm_level_filter] DEFAULT (0),
	[pm_permission] [int] NOT NULL CONSTRAINT [DF_promotions_pm_permission] DEFAULT (0),
	[pm_freq_filter_last_days] [int] NULL CONSTRAINT [DF_promotions_pm_freq_last_days] DEFAULT (0),
	[pm_freq_filter_min_days] [int] NULL CONSTRAINT [DF_promotions_pm_freq_min_days] DEFAULT (0),
	[pm_freq_filter_min_cash_in] [money] NULL CONSTRAINT [DF_promotions_pm_freq_min_cash_in] DEFAULT (0),
	[pm_min_spent] [money] NOT NULL CONSTRAINT [DF_promotions_pm_min_spent] DEFAULT (0),
	[pm_min_spent_reward] [money] NOT NULL CONSTRAINT [DF_promotions_pm_min_spent_reward] DEFAULT (0),
	[pm_spent] [money] NOT NULL CONSTRAINT [DF_promotions_pm_spent] DEFAULT (0),
	[pm_spent_reward] [money] NOT NULL CONSTRAINT [DF_promotions_pm_spent_reward] DEFAULT (0),
 CONSTRAINT [PK_promotions] PRIMARY KEY CLUSTERED 
(
	[pm_promotion_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sales_per_hour]    Script Date: 12/09/2008 16:30:22 ******/
CREATE TABLE [dbo].[sales_per_hour](
  [sph_base_hour] [datetime] NOT NULL,
  [sph_terminal_id] [int] NOT NULL,
  [sph_terminal_name] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  [sph_game_id] [int] NOT NULL,
  [sph_game_name] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
  [sph_played_count] [bigint] NOT NULL,
  [sph_played_amount] [money] NOT NULL,
  [sph_won_count] [bigint] NOT NULL,
  [sph_won_amount] [money] NOT NULL,
  [sph_num_active_terminals] [smallint] NOT NULL,
  [sph_last_play_id] [bigint] NOT NULL,
 CONSTRAINT [PK_sales_per_hour] UNIQUE NONCLUSTERED 
(
  [sph_base_hour] ASC,
  [sph_terminal_id] ASC,
  [sph_game_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[services]    Script Date: 05/12/2009 16:36:50 ******/
CREATE TABLE [dbo].[services](
  [svc_protocol] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
  [svc_machine] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
  [svc_ip_address] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
  [svc_last_access] [datetime] NOT NULL,
  [svc_status] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
  [svc_version] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_services] PRIMARY KEY CLUSTERED 
(
  [svc_protocol] ASC,
  [svc_machine] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[site]    Script Date: 12/09/2008 16:30:22 ******/
CREATE TABLE [dbo].[site](
  [st_id] [uniqueidentifier] ROWGUIDCOL  NOT NULL CONSTRAINT [DF_Table_1_st_site_id]  DEFAULT (newid()),
  [st_name] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
 CONSTRAINT [PK_site] PRIMARY KEY CLUSTERED 
(
  [st_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[site_jackpot_instances]    Script Date: 12/09/2010 15:27:16 ******/
CREATE TABLE [dbo].[site_jackpot_instances](
	[sji_index] [int] NOT NULL,
	[sji_name] [nvarchar](20) NOT NULL,
	[sji_contribution_pct] [numeric](5, 2) NOT NULL,
	[sji_minimum] [money] NOT NULL,
	[sji_maximum] [money] NOT NULL,
	[sji_average] [money] NOT NULL,
	[sji_accumulated] [numeric](20, 8) NOT NULL CONSTRAINT [DF_site_jackpot_instances_sji_accumulated] DEFAULT ((0)),
	[sji_num_pending] [int] NOT NULL CONSTRAINT [DF_site_jackpot_instances_sji_awarded] DEFAULT ((0)),
 CONSTRAINT [PK_site_jackpot_instances] PRIMARY KEY CLUSTERED 
(
	[sji_index] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[site_jackpot_parameters]    Script Date: 12/09/2010 15:30:53 ******/
CREATE TABLE [dbo].[site_jackpot_parameters](
	[sjp_enabled] [bit] NOT NULL,
	[sjp_contribution_pct] [numeric](7, 4) NOT NULL CONSTRAINT [DF_site_jackpot_parameters_sjp_contribution_pct]  DEFAULT ((0)),
	[sjp_awarding_days] [int] NOT NULL CONSTRAINT [DF_site_jackpot_parameters_sjp_working_days]  DEFAULT ((127)),
	[sjp_awarding_start] [int] NOT NULL CONSTRAINT [DF_site_jackpot_parameters_sjp_working_start]  DEFAULT ((0)),
	[sjp_awarding_end] [int] NOT NULL CONSTRAINT [DF_site_jackpot_parameters_sjp_working_end]  DEFAULT ((86399)),
	[sjp_awarding_min_occupation_pct] [numeric](5, 2) NOT NULL CONSTRAINT [DF_site_jackpot_parameters_sjp_min_occupation_pct]  DEFAULT ((0)),
	[sjp_awarding_exclude_promotions] [bit] NOT NULL CONSTRAINT [DF_site_jackpot_parameters_sjp_awarding_exclude_promotions]  DEFAULT ((1)),
	[sjp_awarding_exclude_anonymous] [bit] NOT NULL CONSTRAINT [DF_site_jackpot_parameters_sjp_awarding_exclude_anonymous]  DEFAULT ((1)),
	[sjp_animation_interval] [int] NOT NULL CONSTRAINT [DF_site_jackpot_parameters_sjp_animation_interval]  DEFAULT ((15)),
	[sjp_recent_interval] [int] NOT NULL CONSTRAINT [DF_site_jackpot_parameters_sjp_recent_interval]  DEFAULT ((3600)),
	[sjp_promo_message1] [nvarchar](max) NULL,
	[sjp_promo_message2] [nvarchar](max) NULL,
	[sjp_promo_message3] [nvarchar](max) NULL,
	[sjp_to_compensate] [numeric](20, 8) NOT NULL CONSTRAINT [DF_site_jackpot_parameters_sjp_to_compensate]  DEFAULT ((0)),
	[sjp_only_redeemable] [bit] NOT NULL,
	[sjp_average_interval_hours] [int] NOT NULL CONSTRAINT [DF_site_jackpot_parameters_sjp_average_interval_hours]  DEFAULT ((0)),
	[sjp_played] [numeric](20, 8) NOT NULL CONSTRAINT [DF_site_jackpot_parameters_sjp_played_total]  DEFAULT ((0)),
	[sjp_animation_interval2] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sph_control]    Script Date: 12/09/2008 16:30:21 ******/
CREATE TABLE [dbo].[sph_control](
  [sphc_last_play_id] [bigint] NOT NULL,
  [sphc_last_update] [datetime] NOT NULL,
 CONSTRAINT [PK_sph_control] PRIMARY KEY CLUSTERED 
(
  [sphc_last_play_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[terminal_game_translation]    Script Date: 15/12/2010  ******/
CREATE TABLE [dbo].[terminal_game_translation](
  [tgt_terminal_id] [int] NOT NULL,
  [tgt_source_game_id] [int] NOT NULL,
  [tgt_target_game_id] [int] NOT NULL,
  CONSTRAINT [PK_terminal_game_translation] PRIMARY KEY CLUSTERED 
  (
    [tgt_terminal_id] ASC,
    [tgt_source_game_id] ASC
  ) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[terminal_software_versions]    Script Date: 12/09/2008 16:30:21 ******/
CREATE TABLE [dbo].[terminal_software_versions](
  [tsv_client_id] [smallint] NOT NULL,
  [tsv_build_id] [smallint] NOT NULL,
  [tsv_terminal_type] [smallint] NOT NULL,
  [tsv_insertion_date] [datetime] NOT NULL CONSTRAINT [DF_kiosk_software_versions_ksv_insertion_date]  DEFAULT (getdate()),
 CONSTRAINT [PK_kiosk_software_versions] PRIMARY KEY CLUSTERED 
(
  [tsv_client_id] ASC,
  [tsv_build_id] ASC,
  [tsv_terminal_type] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[terminal_types]    Script Date: 19/04/2010  ******/
CREATE TABLE [dbo].[terminal_types](
	[tt_type] [int] NOT NULL,
	[tt_name] [nchar](50) NOT NULL,
 CONSTRAINT [PK_terminal_types] PRIMARY KEY CLUSTERED 
(
	[tt_type] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_terminal_type_name] ON [dbo].[terminal_types] 
(
	[tt_name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[terminals]    Script Date: 12/09/2008 16:30:21 ******/
CREATE TABLE [dbo].[terminals](
  [te_terminal_id] [int] IDENTITY(10000,1) NOT NULL,
  [te_type] [int] NOT NULL,
  [te_server_id] [int] NULL,
  [te_name] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  [te_external_id] [nvarchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  [te_blocked] [bit] NOT NULL,
  [te_timestamp] [timestamp] NULL,
  [te_active] [bit] NOT NULL CONSTRAINT [DF_terminals_te_active]  DEFAULT ((1)),
  [te_provider_id] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  [te_client_id] [smallint] NULL CONSTRAINT [DF_terminals_te_client_id]  DEFAULT (NULL),
  [te_build_id] [smallint] NULL CONSTRAINT [DF_terminals_te_build_id]  DEFAULT (NULL),
  [te_terminal_type] [smallint] NOT NULL CONSTRAINT [DF_terminals_te_terminal_type] DEFAULT (-1),
  [te_vendor_id] [nvarchar](50) NULL,
  [te_unique_external_id]  AS (isnull([te_external_id],'<NULL>'+CONVERT([nvarchar],[te_terminal_id],(0)))),
  [te_status] [int] NOT NULL CONSTRAINT [DF_te_status] DEFAULT (0),
 CONSTRAINT [PK_terminals] PRIMARY KEY CLUSTERED 
(
  [te_terminal_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_terminal_external_id] ON [dbo].[terminals] 
(
	[te_external_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX IX_terminal_name ON dbo.terminals
  (
  te_name
  ) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_terminal_unique_ext_id] ON [dbo].[terminals] 
(
	[te_unique_external_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[terminals_3gs]    Script Date: 05/27/2010 16:16:17 ******/
CREATE TABLE [dbo].[terminals_3gs](
      [t3gs_vendor_id] [nvarchar](50) NOT NULL,
      [t3gs_serial_number] [nvarchar](50) NOT NULL,
      [t3gs_machine_number] [int] NOT NULL,
      [t3gs_terminal_id] [int] NULL,
 CONSTRAINT [PK_terminals_3gs] PRIMARY KEY CLUSTERED 
(
      [t3gs_vendor_id] ASC,
      [t3gs_serial_number] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_3gs_vendor_serialnumber] ON [dbo].[terminals_3gs] 
(
	[t3gs_vendor_id] ASC,
	[t3gs_machine_number] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Objeto:  Table [dbo].[terminals_connected]    Fecha de la secuencia de comandos: 11/04/2010 16:39:19 ******/
CREATE TABLE [dbo].[terminals_connected](
       [tc_date] [datetime] NOT NULL,
       [tc_terminal_id] [int] NOT NULL,
 CONSTRAINT [PK_terminals_connected] PRIMARY KEY CLUSTERED 
(
       [tc_date] ASC,
       [tc_terminal_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[terminals_pending]    Script Date: 05/27/2010 16:16:36 ******/
CREATE TABLE [dbo].[terminals_pending](
      [tp_id] [int] IDENTITY(1,1) NOT NULL,
      [tp_source] [int] NULL CONSTRAINT [DF_terminals_new_tn_source]  DEFAULT ((0)),
      [tp_vendor_id] [nvarchar](50) NULL,
      [tp_serial_number] [nvarchar](50) NULL,
      [tp_machine_number] [int] NULL,
      [tp_reported] [datetime] NOT NULL CONSTRAINT [DF_terminals_new_tn_reported]  DEFAULT (getdate()),
      [tp_ignore] [bit] NOT NULL CONSTRAINT [DF_terminals_new_tn_ignore]  DEFAULT ((0)),
 CONSTRAINT [PK_terminals_pending] PRIMARY KEY CLUSTERED 
(
      [tp_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tp_source_vendor_sn] ON [dbo].[terminals_pending] 
(
	[tp_source] ASC,
	[tp_vendor_id] ASC,
	[tp_serial_number] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[wc2_messages]    Script Date: 12/09/2008 16:30:22 ******/
CREATE TABLE [dbo].[wc2_messages](
  [w2m_message_id] [bigint] IDENTITY(1,1) NOT NULL,
  [w2m_datetime] [datetime] NOT NULL CONSTRAINT [DF_wc2_messages_wc2_datetime]  DEFAULT (getdate()),
  [w2m_terminal_id] [int] NULL,
  [w2m_session_id] [bigint] NULL,
  [w2m_sequence_id] [bigint] NULL,
  [w2m_towards_to_terminal] [bit] NOT NULL CONSTRAINT [DF_wc2_messages_wc2_to_gs]  DEFAULT ((0)),
  [w2m_message] [xml] NOT NULL,
 CONSTRAINT [PK_wc2_messages] PRIMARY KEY CLUSTERED 
(
  [w2m_message_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[wc2_sessions]    Script Date: 12/09/2008 16:30:22 ******/
CREATE TABLE [dbo].[wc2_sessions](
  [w2s_session_id] [bigint] IDENTITY(1,1) NOT NULL,
  [w2s_terminal_id] [int] NOT NULL,
  [w2s_last_sequence_id] [bigint] NOT NULL CONSTRAINT [DF_wc2_sessions_w2s_last_sequence_id]  DEFAULT ((0)),
  [w2s_last_transaction_id] [bigint] NOT NULL CONSTRAINT [DF_wc2_sessions_w2s_last_transaction_id]  DEFAULT ((0)),
  [w2s_last_rcvd_msg] [datetime] NULL,
  [w2s_started] [datetime] NOT NULL CONSTRAINT [DF_wc2_sessions_w2s_started]  DEFAULT (getdate()),
  [w2s_finished] [datetime] NULL,
  [w2s_status] [int] NOT NULL CONSTRAINT [DF_wc2_sessions_w2s_status]  DEFAULT ((0)),
  [w2s_timestamp] [timestamp] NULL,
  [w2s_server_name] [nvarchar](50) NULL,
 CONSTRAINT [PK_wc2_sessions] PRIMARY KEY CLUSTERED 
(
  [w2s_session_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_wc2_sessions] ON [dbo].[wc2_sessions] 
(
  [w2s_terminal_id] ASC,
  [w2s_started] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0 - Opened, 1 - Closed, 2 - Abandoned, 3 - Timeout' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'wc2_sessions', @level2type=N'COLUMN',@level2name=N'w2s_status'
GO
CREATE NONCLUSTERED INDEX [IX_wc2_status] ON [dbo].[wc2_sessions] 
(
	[w2s_status] ASC,
	[w2s_terminal_id] ASC,
	[w2s_session_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[wc2_transactions]    Script Date: 12/09/2008 16:30:22 ******/
CREATE TABLE [dbo].[wc2_transactions](
  [w2tx_terminal_id] [int] NOT NULL,
  [w2tx_sequence_id] [bigint] NOT NULL,
  [w2tx_requested_by_terminal] [bit] NOT NULL,
  [w2tx_session_id] [bigint] NOT NULL,
  [w2tx_status] [int] NOT NULL CONSTRAINT [DF_wc2_transactions_w2tx_status]  DEFAULT ((0)),
  [w2tx_request_msg_id] [bigint] NULL,
  [w2tx_response_msg_id] [bigint] NULL,
 CONSTRAINT [PK_wc2_transactions] PRIMARY KEY CLUSTERED 
(
  [w2tx_terminal_id] ASC,
  [w2tx_sequence_id] ASC,
  [w2tx_requested_by_terminal] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_wc2_transactions] ON [dbo].[wc2_transactions] 
(
  [w2tx_terminal_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0 - Running, 1 - Finished' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'wc2_transactions', @level2type=N'COLUMN',@level2name=N'w2tx_status'
GO
/****** Object:  Table [dbo].[wcp_commands]    Script Date: 06/15/2010 18:04:15 ******/
CREATE TABLE [dbo].[wcp_commands](
	[cmd_id] [bigint] IDENTITY(1,1) NOT NULL,
	[cmd_terminal_id] [int] NOT NULL,
	[cmd_code] [int] NOT NULL,
	[cmd_parameter] [nvarchar](200) NULL,
	[cmd_status] [int] NOT NULL,
	[cmd_created] [datetime] NOT NULL CONSTRAINT [DF_wcp_commands_cmd_created]  DEFAULT (getdate()),
	[cmd_status_changed] [datetime] NOT NULL CONSTRAINT [DF_wcp_commands_cmd_status_changed]  DEFAULT (getdate()),
	[cmd_response] [nvarchar](max) NULL,
 CONSTRAINT [PK_wcp_commands] PRIMARY KEY CLUSTERED 
(
	[cmd_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_status_terminal_cmd] ON [dbo].[wcp_commands] 
(
	[cmd_status] ASC,
	[cmd_terminal_id] ASC,
	[cmd_code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Objeto:  Index [IX_status]    Fecha de la secuencia de comandos: 08/27/2010 05:24:43 ******/
CREATE NONCLUSTERED INDEX [IX_status] ON [dbo].[wcp_commands] 
(
                [cmd_status] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[wcp_messages]    Script Date: 12/09/2008 16:30:22 ******/
CREATE TABLE [dbo].[wcp_messages](
  [wm_message_id] [bigint] IDENTITY(1,1) NOT NULL,
  [wm_datetime] [datetime] NOT NULL CONSTRAINT [DF_wcp_messages_wcp_datetime]  DEFAULT (getdate()),
  [wm_terminal_id] [int] NULL,
  [wm_session_id] [bigint] NULL,
  [wm_sequence_id] [bigint] NULL,
  [wm_towards_to_terminal] [bit] NOT NULL CONSTRAINT [DF_wcp_messages_wcp_to_gs]  DEFAULT ((0)),
  [wm_message] [xml] NOT NULL,
 CONSTRAINT [PK_wcp_messages] PRIMARY KEY CLUSTERED 
(
  [wm_message_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[wcp_services]    Script Date: 12/09/2008 16:30:21 ******/
CREATE TABLE [dbo].[wcp_services](
  [wsvr_service_id] [bigint] IDENTITY(1,1) NOT NULL,
  [wsvr_name] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
  [wsvr_watchdog] [datetime] NULL,
 CONSTRAINT [PK_wcp_services] PRIMARY KEY CLUSTERED 
(
  [wsvr_service_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[wcp_services_to_cj_queues]    Script Date: 12/09/2008 16:30:22 ******/
CREATE TABLE [dbo].[wcp_services_to_cj_queues](
  [wscq_service_id] [bigint] NOT NULL,
  [wscq_queue_id] [bigint] NOT NULL,
 CONSTRAINT [PK_wcp_services_to_cj_queues_1] PRIMARY KEY CLUSTERED 
(
  [wscq_service_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[wcp_sessions]    Script Date: 12/09/2008 16:30:22 ******/
CREATE TABLE [dbo].[wcp_sessions](
  [ws_session_id] [bigint] IDENTITY(1,1) NOT NULL,
  [ws_terminal_id] [int] NOT NULL,
  [ws_last_sequence_id] [bigint] NOT NULL CONSTRAINT [DF_wcp_sessions_ws_last_sequence_id]  DEFAULT ((0)),
  [ws_last_transaction_id] [bigint] NOT NULL CONSTRAINT [DF_wcp_sessions_ws_last_transaction_id]  DEFAULT ((0)),
  [ws_last_rcvd_msg] [datetime] NULL,
  [ws_started] [datetime] NOT NULL CONSTRAINT [DF_wcp_sessions_ws_started]  DEFAULT (getdate()),
  [ws_finished] [datetime] NULL,
  [ws_status] [int] NOT NULL CONSTRAINT [DF_wcp_sessions_ws_status]  DEFAULT ((0)),
  [ws_timestamp] [timestamp] NULL,
  [ws_server_name] [nvarchar](50) NULL,
 CONSTRAINT [PK_wcp_sessions] PRIMARY KEY CLUSTERED 
(
  [ws_session_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_wcp_sessions] ON [dbo].[wcp_sessions] 
(
  [ws_terminal_id] ASC,
  [ws_started] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0 - Opened, 1 - Closed, 2 - Abandoned, 3 - Timeout' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'wcp_sessions', @level2type=N'COLUMN',@level2name=N'ws_status'
GO
CREATE NONCLUSTERED INDEX [IX_wcp_status] ON [dbo].[wcp_sessions] 
(
	[ws_status] ASC,
	[ws_terminal_id] ASC,
	[ws_session_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[wcp_transactions]    Script Date: 12/09/2008 16:30:22 ******/
CREATE TABLE [dbo].[wcp_transactions](
  [wtx_terminal_id] [int] NOT NULL,
  [wtx_sequence_id] [bigint] NOT NULL,
  [wtx_requested_by_terminal] [bit] NOT NULL,
  [wtx_session_id] [bigint] NOT NULL,
  [wtx_status] [int] NOT NULL CONSTRAINT [DF_wcp_transactions_wtx_status]  DEFAULT ((0)),
  [wtx_request_msg_id] [bigint] NULL,
  [wtx_response_msg_id] [bigint] NULL,
 CONSTRAINT [PK_wcp_transactions] PRIMARY KEY CLUSTERED 
(
  [wtx_terminal_id] ASC,
  [wtx_sequence_id] ASC,
  [wtx_requested_by_terminal] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_wcp_transactions] ON [dbo].[wcp_transactions] 
(
  [wtx_terminal_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0 - Running, 1 - Finished' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'wcp_transactions', @level2type=N'COLUMN',@level2name=N'wtx_status'
GO

/* User-defined Functions */
/****** Object:  UserDefinedFunction [dbo].[Minimum]    Script Date: 12/09/2008 16:30:21 ******/
-- =============================================
-- Author:    Armando Alva
-- Create date: 07-08-2008
-- Description: Returns the minimum value of the two given arguments 
-- =============================================
CREATE FUNCTION [dbo].[Minimum]
       (
            @value1 Money = 0,
            @value2 Money = 0
       )
RETURNS Money

AS
      BEGIN
            DECLARE @Result Money
            SET @value1 = ISNULL(@value1,0)
            SET @value2 = ISNULL(@value2,0)
            IF @value1 <= @value2
                  SET @Result = @value1
            ELSE
                  SET @Result = @value2
      RETURN @Result
      END
GO
/****** Object:  UserDefinedFunction [dbo].[Maximum_Money]    Script Date: 12/09/2008 16:30:21 ******/
-- =============================================
-- Author:    Agustí Poch
-- Create date: 14-11-2008
-- Description: Returns the maximum value of the two given 
--              arguments of type Money
-- =============================================
CREATE FUNCTION [dbo].[Maximum_Money]
       (
            @value1 Money = 0,
            @value2 Money = 0
       )
RETURNS Money

AS
      BEGIN
            DECLARE @Result Money
            SET @value1 = ISNULL(@value1,0)
            SET @value2 = ISNULL(@value2,0)
            IF @value1 >= @value2
                  SET @Result = @value1
            ELSE
                  SET @Result = @value2
      RETURN @Result
      END
GO
/****** Object:  UserDefinedFunction [dbo].[Maximum_Bigint]    Script Date: 12/09/2008 16:30:21 ******/
-- =============================================
-- Author:    Agustí Poch
-- Create date: 14-11-2008
-- Description: Returns the maximum value of the two given 
--              arguments of type Bigint
-- =============================================
CREATE FUNCTION [dbo].[Maximum_Bigint]
       (
            @value1 Bigint = 0,
            @value2 Bigint = 0
       )
RETURNS Bigint

AS
      BEGIN
            DECLARE @Result Bigint
            SET @value1 = ISNULL(@value1,0)
            SET @value2 = ISNULL(@value2,0)
            IF @value1 >= @value2
                  SET @Result = @value1
            ELSE
                  SET @Result = @value2
      RETURN @Result
      END
GO
/****** Object:  UserDefinedFunction [dbo].[Maximum_Datetime]    Script Date: 12/09/2008 16:30:21 ******/
-- =============================================
-- Author:    Agustí Poch
-- Create date: 14-11-2008
-- Description: Returns the maximum value of the two given 
--              arguments of type Datetime
-- =============================================
CREATE FUNCTION [dbo].[Maximum_Datetime]
       (
            @value1 Datetime = 0,
            @value2 Datetime = 0
       )
RETURNS Datetime

AS
      BEGIN
            DECLARE @Result Datetime
            SET @value1 = ISNULL(@value1,0)
            SET @value2 = ISNULL(@value2,0)
            IF @value1 >= @value2
                  SET @Result = @value1
            ELSE
                  SET @Result = @value2
      RETURN @Result
      END
GO
/****** Object:  UserDefinedFunction [dbo].[Minimum_Money]    Script Date: 12/09/2008 16:30:21 ******/
-- =============================================
-- Author:    Agustí Poch
-- Create date: 14-11-2008
-- Description: Returns the minimum value of the two given 
--              arguments of type Money
-- =============================================
CREATE FUNCTION [dbo].[Minimum_Money]
       (
            @value1 Money = 0,
            @value2 Money = 0
       )
RETURNS Money

AS
      BEGIN
            DECLARE @Result Money
            SET @value1 = ISNULL(@value1,0)
            SET @value2 = ISNULL(@value2,0)
            IF @value1 <= @value2
                  SET @Result = @value1
            ELSE
                  SET @Result = @value2
      RETURN @Result
      END
GO
/****** Object:  UserDefinedFunction [dbo].[Minimum_Bigint]    Script Date: 12/09/2008 16:30:21 ******/
-- =============================================
-- Author:    Agustí Poch
-- Create date: 14-11-2008
-- Description: Returns the minimum value of the two given 
--              arguments of type Bigint
-- =============================================
CREATE FUNCTION [dbo].[Minimum_Bigint]
       (
            @value1 Bigint = 0,
            @value2 Bigint = 0
       )
RETURNS Bigint

AS
      BEGIN
            DECLARE @Result Bigint
            SET @value1 = ISNULL(@value1,0)
            SET @value2 = ISNULL(@value2,0)
            IF @value1 <= @value2
                  SET @Result = @value1
            ELSE
                  SET @Result = @value2
      RETURN @Result
      END
GO
/****** Object:  UserDefinedFunction [dbo].[Minimum_Datetime]    Script Date: 12/09/2008 16:30:21 ******/
-- =============================================
-- Author:    Agustí Poch
-- Create date: 14-11-2008
-- Description: Returns the minimum value of the two given 
--              arguments of type Datetime
-- =============================================
CREATE FUNCTION [dbo].[Minimum_Datetime]
       (
            @value1 Datetime = 0,
            @value2 Datetime = 0
       )
RETURNS Datetime

AS
      BEGIN
            DECLARE @Result Datetime
            SET @value1 = ISNULL(@value1,0)
            SET @value2 = ISNULL(@value2,0)
            IF @value1 <= @value2
                  SET @Result = @value1
            ELSE
                  SET @Result = @value2
      RETURN @Result
      END
GO

/* Views */
/****** Object:  View [dbo].[VIEW_CARD_BALANCE]    Script Date: 12/09/2008 16:30:22 ******/
CREATE VIEW [dbo].[VIEW_CARD_BALANCE]
AS
SELECT     ac_initial_cash_in AS [Initial Cash In], ac_initial_not_redeemable AS [Initial Not Redeemable], ac_balance AS Balance, dbo.Minimum(ac_balance, 
                      ac_initial_not_redeemable) AS [Not Redeemable], dbo.Minimum(ac_balance - dbo.Minimum(ac_balance, ac_initial_not_redeemable), ac_initial_cash_in) AS [Cash In], 
                      ac_balance - ac_not_redeemable - dbo.Minimum(ac_balance - ac_not_redeemable, ac_initial_cash_in) AS [Cash Won]
FROM         dbo.accounts
GO
/****** Object:  View [dbo].[terminals_last_play_session]    Script Date: 05/12/2009 16:41:48 ******/
CREATE VIEW [dbo].[terminals_last_play_session]
AS
SELECT     ps_terminal_id AS lp_terminal_id, MAX(ps_play_session_id) AS lp_play_session_id
FROM         dbo.play_sessions
GROUP BY ps_terminal_id
GO
/****** Object:  View [dbo].[terminals_view]    Script Date: 05/12/2009 16:42:03 ******/
CREATE VIEW [dbo].[terminals_view]
AS
SELECT     dbo.terminals.te_terminal_id, dbo.terminals.te_type, dbo.terminals.te_server_id, dbo.terminals.te_name, dbo.terminals.te_external_id, 
                      dbo.terminals.te_blocked, dbo.terminals.te_timestamp, dbo.terminals.te_active, dbo.terminals.te_provider_id, dbo.terminals.te_client_id, 
                      dbo.terminals.te_build_id, dbo.play_sessions.ps_play_session_id, dbo.play_sessions.ps_account_id, dbo.play_sessions.ps_terminal_id, 
                      dbo.play_sessions.ps_type, dbo.play_sessions.ps_type_data, dbo.play_sessions.ps_status, dbo.play_sessions.ps_started, 
                      dbo.play_sessions.ps_initial_balance, dbo.play_sessions.ps_played_count, dbo.play_sessions.ps_played_amount, 
                      dbo.play_sessions.ps_won_count, dbo.play_sessions.ps_won_amount, dbo.play_sessions.ps_cash_in, dbo.play_sessions.ps_cash_out, 
                      dbo.play_sessions.ps_finished, dbo.play_sessions.ps_final_balance, dbo.play_sessions.ps_timestamp, dbo.play_sessions.ps_locked, 
                      dbo.accounts.ac_account_id, dbo.accounts.ac_type, dbo.accounts.ac_holder_name, dbo.accounts.ac_blocked, dbo.accounts.ac_not_valid_before, 
                      dbo.accounts.ac_not_valid_after, dbo.accounts.ac_balance, dbo.accounts.ac_cash_in, dbo.accounts.ac_cash_won, dbo.accounts.ac_not_redeemable, 
                      dbo.accounts.ac_timestamp, dbo.accounts.ac_track_data, dbo.accounts.ac_total_cash_in, dbo.accounts.ac_total_cash_out, 
                      dbo.accounts.ac_initial_cash_in, dbo.accounts.ac_activated, dbo.accounts.ac_deposit, dbo.accounts.ac_current_terminal_id, 
                      dbo.accounts.ac_current_terminal_name, dbo.accounts.ac_current_play_session_id, dbo.accounts.ac_last_terminal_id, 
                      dbo.accounts.ac_last_terminal_name, dbo.accounts.ac_last_play_session_id, dbo.accounts.ac_user_type, dbo.accounts.ac_points, 
                      dbo.accounts.ac_initial_not_redeemable, dbo.play_sessions.ps_stand_alone
FROM         dbo.accounts RIGHT OUTER JOIN
                      dbo.terminals_last_play_session INNER JOIN
                      dbo.play_sessions ON dbo.terminals_last_play_session.lp_play_session_id = dbo.play_sessions.ps_play_session_id ON 
                      dbo.accounts.ac_account_id = dbo.play_sessions.ps_account_id AND 
                      dbo.accounts.ac_current_play_session_id = dbo.play_sessions.ps_play_session_id RIGHT OUTER JOIN
                      dbo.terminals ON dbo.terminals_last_play_session.lp_terminal_id = dbo.terminals.te_terminal_id
GO
/****** Object:  View [dbo].[alesis_view]    Script Date: 05/12/2009 16:41:27 ******/
CREATE VIEW [dbo].[alesis_view]
AS
SELECT     dbo.terminals_view.te_terminal_id, dbo.terminals_view.te_type, dbo.terminals_view.te_server_id, dbo.terminals_view.te_name, dbo.terminals_view.te_external_id, 
                      dbo.terminals_view.te_blocked, dbo.terminals_view.te_timestamp, dbo.terminals_view.te_active, dbo.terminals_view.te_provider_id, dbo.terminals_view.te_client_id, 
                      dbo.terminals_view.te_build_id, dbo.terminals_view.ps_play_session_id, dbo.terminals_view.ps_account_id, dbo.terminals_view.ps_terminal_id, 
                      dbo.terminals_view.ps_type, dbo.terminals_view.ps_type_data, dbo.terminals_view.ps_status, dbo.terminals_view.ps_started, 
                      dbo.terminals_view.ps_initial_balance, dbo.terminals_view.ps_played_count, dbo.terminals_view.ps_played_amount, dbo.terminals_view.ps_won_count, 
                      dbo.terminals_view.ps_won_amount, dbo.terminals_view.ps_cash_in, dbo.terminals_view.ps_cash_out, dbo.terminals_view.ps_finished, 
                      dbo.terminals_view.ps_final_balance, dbo.terminals_view.ps_timestamp, dbo.terminals_view.ps_locked, dbo.terminals_view.ac_account_id, 
                      dbo.terminals_view.ac_type, dbo.terminals_view.ac_holder_name, dbo.terminals_view.ac_blocked, dbo.terminals_view.ac_not_valid_before, 
                      dbo.terminals_view.ac_not_valid_after, dbo.terminals_view.ac_balance, dbo.terminals_view.ac_cash_in, dbo.terminals_view.ac_cash_won, 
                      dbo.terminals_view.ac_not_redeemable, dbo.terminals_view.ac_timestamp, dbo.terminals_view.ac_track_data, dbo.terminals_view.ac_total_cash_in, 
                      dbo.terminals_view.ac_total_cash_out, dbo.terminals_view.ac_initial_cash_in, dbo.terminals_view.ac_activated, dbo.terminals_view.ac_deposit, 
                      dbo.terminals_view.ac_current_terminal_id, dbo.terminals_view.ac_current_terminal_name, dbo.terminals_view.ac_current_play_session_id, 
                      dbo.terminals_view.ac_last_terminal_id, dbo.terminals_view.ac_last_terminal_name, dbo.terminals_view.ac_last_play_session_id, 
                      dbo.terminals_view.ac_user_type, dbo.terminals_view.ac_points, dbo.terminals_view.ac_initial_not_redeemable, dbo.alesis_terminals.at_terminal_id, 
                      dbo.alesis_terminals.at_machine_id, dbo.alesis_terminals.at_status, dbo.alesis_terminals.at_session_id, dbo.alesis_parameters.ap_sql_server_ip_address, 
                      dbo.alesis_parameters.ap_sql_database_name, dbo.alesis_parameters.ap_sql_user, dbo.alesis_parameters.ap_sql_user_password, 
                      dbo.alesis_parameters.ap_vendor_id
FROM         dbo.alesis_terminals RIGHT OUTER JOIN
                      dbo.terminals_view ON dbo.alesis_terminals.at_terminal_id = dbo.terminals_view.te_terminal_id CROSS JOIN
                      dbo.alesis_parameters
GO
/****** Object:  View [dbo].[sales_per_hour_v]    Script Date: 01/12/2010 17:36:46 ******/
CREATE VIEW [dbo].[sales_per_hour_v]
AS
SELECT dbo.sales_per_hour.sph_base_hour, dbo.sales_per_hour.sph_terminal_id, dbo.sales_per_hour.sph_terminal_name, dbo.sales_per_hour.sph_played_count, 
       dbo.sales_per_hour.sph_played_amount, dbo.sales_per_hour.sph_won_count, dbo.sales_per_hour.sph_won_amount, 
       dbo.sales_per_hour.sph_num_active_terminals, dbo.sales_per_hour.sph_last_play_id, 
       dbo.terminal_game_translation.tgt_target_game_id AS SPH_GAME_ID, 
       dbo.games.gm_name AS SPH_GAME_NAME
FROM   dbo.sales_per_hour INNER JOIN
       dbo.terminal_game_translation ON dbo.sales_per_hour.sph_terminal_id = dbo.terminal_game_translation.tgt_terminal_id AND 
       dbo.sales_per_hour.sph_game_id = dbo.terminal_game_translation.tgt_source_game_id INNER JOIN
       dbo.games ON dbo.terminal_game_translation.tgt_target_game_id = dbo.games.gm_game_id
GO
/****** Object:  View [dbo].[plays_v]    Script Date: 01/12/2010 17:38:42 ******/
CREATE VIEW [dbo].[plays_v]
AS
SELECT     dbo.plays.pl_play_id, dbo.plays.pl_play_session_id, dbo.plays.pl_account_id, dbo.plays.pl_terminal_id, dbo.plays.pl_wcp_sequence_id, 
                      dbo.plays.pl_wcp_transaction_id, dbo.plays.pl_datetime, dbo.plays.pl_initial_balance, dbo.plays.pl_played_amount, dbo.plays.pl_won_amount, 
                      dbo.plays.pl_final_balance, dbo.plays.pl_transferred, dbo.terminal_game_translation.tgt_target_game_id AS pl_game_id

FROM         dbo.plays INNER JOIN
                      dbo.terminal_game_translation ON dbo.plays.pl_terminal_id = dbo.terminal_game_translation.tgt_terminal_id AND 
                      dbo.plays.pl_game_id = dbo.terminal_game_translation.tgt_source_game_id
GO
/****** Object:  View [dbo].[games_v]    Script Date: 01/12/2010 17:39:51 ******/
CREATE VIEW [dbo].[games_v]
AS
SELECT DISTINCT dbo.terminal_game_translation.tgt_target_game_id as gm_game_id, dbo.games.gm_name, dbo.games.gm_timestamp
FROM         dbo.games INNER JOIN
                      dbo.terminal_game_translation ON dbo.games.gm_game_id = dbo.terminal_game_translation.tgt_target_game_id
GO
/****** Object:  View [dbo].[c2_draw_audit_plays_v]    Script Date: 01/19/2010 17:17:21 ******/
CREATE VIEW [dbo].[c2_draw_audit_plays_v]
AS
SELECT dbo.c2_draw_audit_plays.dap_play_id, dbo.c2_draw_audit_plays.dap_draw_id, dbo.c2_draw_audit_plays.dap_play_datetime, 
       dbo.c2_draw_audit_plays.dap_terminal_id, dbo.c2_draw_audit_plays.dap_terminal_name, dbo.c2_draw_audit_plays.dap_denomination, 
       dbo.c2_draw_audit_plays.dap_played_credits, dbo.c2_draw_audit_plays.dap_won_credits, dbo.c2_draw_audit_plays.dap_jackpot_bound_credits, 
       dbo.c2_draw_audit_plays.dap_prize_list, dbo.c2_draw_audit_plays.dap_cards, dbo.c2_draw_audit_plays.dap_wcp_transaction_id, 
       dbo.terminal_game_translation.tgt_target_game_id AS DAP_GAME_ID, 
       dbo.games.gm_name AS DAP_GAME_NAME
FROM   dbo.c2_draw_audit_plays INNER JOIN
       dbo.terminal_game_translation ON dbo.c2_draw_audit_plays.dap_terminal_id = dbo.terminal_game_translation.tgt_terminal_id AND 
       dbo.c2_draw_audit_plays.dap_game_id = dbo.terminal_game_translation.tgt_source_game_id INNER JOIN
       dbo.games ON dbo.terminal_game_translation.tgt_target_game_id = dbo.games.gm_game_id
GO

/* Constraints */
/****** Object:  Check [CK_terminals]    Script Date: 12/09/2008 16:30:21 ******/
ALTER TABLE [dbo].[terminals]  WITH CHECK ADD  CONSTRAINT [CK_terminals] CHECK  (([te_type]=(1) OR [te_type]=(0)))
GO
ALTER TABLE [dbo].[terminals] CHECK CONSTRAINT [CK_terminals]
GO
/****** Object:  ForeignKey [FK_terminals_terminal_servers]    Script Date: 12/09/2008 16:30:21 ******/
ALTER TABLE [dbo].[terminals]  WITH CHECK ADD  CONSTRAINT [FK_terminals_terminal_servers] FOREIGN KEY([te_server_id])
REFERENCES [dbo].[terminals] ([te_terminal_id])
GO
ALTER TABLE [dbo].[terminals] CHECK CONSTRAINT [FK_terminals_terminal_servers]
GO
/****** Object:  ForeignKey [FK_wcp_services_to_cj_queues_cj_queues]    Script Date: 12/09/2008 16:30:22 ******/
ALTER TABLE [dbo].[wcp_services_to_cj_queues]  WITH CHECK ADD  CONSTRAINT [FK_wcp_services_to_cj_queues_cj_queues] FOREIGN KEY([wscq_queue_id])
REFERENCES [dbo].[cj_queues] ([cq_queue_id])
GO
ALTER TABLE [dbo].[wcp_services_to_cj_queues] CHECK CONSTRAINT [FK_wcp_services_to_cj_queues_cj_queues]
GO
/****** Object:  ForeignKey [FK_wcp_services_to_cj_queues_wcp_services]    Script Date: 12/09/2008 16:30:22 ******/
ALTER TABLE [dbo].[wcp_services_to_cj_queues]  WITH CHECK ADD  CONSTRAINT [FK_wcp_services_to_cj_queues_wcp_services] FOREIGN KEY([wscq_service_id])
REFERENCES [dbo].[wcp_services] ([wsvr_service_id])
GO
ALTER TABLE [dbo].[wcp_services_to_cj_queues] CHECK CONSTRAINT [FK_wcp_services_to_cj_queues_wcp_services]
GO
/****** Object:  ForeignKey [FK_gui_audit_gui_users]    Script Date: 12/09/2008 16:30:22 ******/
ALTER TABLE [dbo].[gui_audit]  WITH CHECK ADD  CONSTRAINT [FK_gui_audit_gui_users] FOREIGN KEY([ga_gui_user_id])
REFERENCES [dbo].[gui_users] ([gu_user_id])
GO
ALTER TABLE [dbo].[gui_audit] CHECK CONSTRAINT [FK_gui_audit_gui_users]
GO
/****** Object:  ForeignKey [FK_c2_jackpot_counters_c2_jackpot_instances]    Script Date: 12/09/2008 16:30:22 ******/
ALTER TABLE [dbo].[c2_jackpot_counters]  WITH CHECK ADD  CONSTRAINT [FK_c2_jackpot_counters_c2_jackpot_instances] FOREIGN KEY([c2jc_index])
REFERENCES [dbo].[c2_jackpot_instances] ([c2ji_index])
GO
ALTER TABLE [dbo].[c2_jackpot_counters] CHECK CONSTRAINT [FK_c2_jackpot_counters_c2_jackpot_instances]
GO
/****** Object:  ForeignKey [FK_cj_transactions_cj_queues]    Script Date: 12/09/2008 16:30:22 ******/
ALTER TABLE [dbo].[cj_transactions]  WITH CHECK ADD  CONSTRAINT [FK_cj_transactions_cj_queues] FOREIGN KEY([ctx_queue_id])
REFERENCES [dbo].[cj_queues] ([cq_queue_id])
GO
ALTER TABLE [dbo].[cj_transactions] CHECK CONSTRAINT [FK_cj_transactions_cj_queues]
GO
/****** Object:  ForeignKey [FK_cj_transactions_terminals]    Script Date: 12/09/2008 16:30:22 ******/
ALTER TABLE [dbo].[cj_transactions]  WITH CHECK ADD  CONSTRAINT [FK_cj_transactions_terminals] FOREIGN KEY([ctx_terminal_id])
REFERENCES [dbo].[terminals] ([te_terminal_id])
GO
ALTER TABLE [dbo].[cj_transactions] CHECK CONSTRAINT [FK_cj_transactions_terminals]
GO
/****** Object:  ForeignKey [FK_cj_transactions_wcp_sessions]    Script Date: 12/09/2008 16:30:22 ******/
ALTER TABLE [dbo].[cj_transactions]  WITH CHECK ADD  CONSTRAINT [FK_cj_transactions_wcp_sessions] FOREIGN KEY([ctx_session_id])
REFERENCES [dbo].[wcp_sessions] ([ws_session_id])
GO
ALTER TABLE [dbo].[cj_transactions] CHECK CONSTRAINT [FK_cj_transactions_wcp_sessions]
GO
/****** Object:  ForeignKey [FK_plays_accounts]    Script Date: 12/09/2008 16:30:22 ******/
ALTER TABLE [dbo].[plays]  WITH CHECK ADD  CONSTRAINT [FK_plays_accounts] FOREIGN KEY([pl_account_id])
REFERENCES [dbo].[accounts] ([ac_account_id])
GO
ALTER TABLE [dbo].[plays] CHECK CONSTRAINT [FK_plays_accounts]
GO
/****** Object:  ForeignKey [FK_plays_play_sessions]    Script Date: 12/09/2008 16:30:22 ******/
ALTER TABLE [dbo].[plays]  WITH CHECK ADD  CONSTRAINT [FK_plays_play_sessions] FOREIGN KEY([pl_play_session_id])
REFERENCES [dbo].[play_sessions] ([ps_play_session_id])
GO
ALTER TABLE [dbo].[plays] CHECK CONSTRAINT [FK_plays_play_sessions]
GO
/****** Object:  ForeignKey [FK_plays_terminals]    Script Date: 12/09/2008 16:30:22 ******/
ALTER TABLE [dbo].[plays]  WITH CHECK ADD  CONSTRAINT [FK_plays_terminals] FOREIGN KEY([pl_terminal_id])
REFERENCES [dbo].[terminals] ([te_terminal_id])
GO
ALTER TABLE [dbo].[plays] CHECK CONSTRAINT [FK_plays_terminals]
GO
/****** Object:  ForeignKey [FK_account_movements_accounts]    Script Date: 12/09/2008 16:30:22 ******/
ALTER TABLE [dbo].[account_movements]  WITH CHECK ADD  CONSTRAINT [FK_account_movements_accounts] FOREIGN KEY([am_account_id])
REFERENCES [dbo].[accounts] ([ac_account_id])
GO
ALTER TABLE [dbo].[account_movements] CHECK CONSTRAINT [FK_account_movements_accounts]
GO
/****** Object:  ForeignKey [FK_account_movements_play_sessions]    Script Date: 12/09/2008 16:30:22 ******/
ALTER TABLE [dbo].[account_movements]  WITH CHECK ADD  CONSTRAINT [FK_account_movements_play_sessions] FOREIGN KEY([am_play_session_id])
REFERENCES [dbo].[play_sessions] ([ps_play_session_id])
GO
ALTER TABLE [dbo].[account_movements] CHECK CONSTRAINT [FK_account_movements_play_sessions]
GO
/****** Object:  ForeignKey [FK_play_sessions_accounts]    Script Date: 12/09/2008 16:30:22 ******/
ALTER TABLE [dbo].[play_sessions]  WITH CHECK ADD  CONSTRAINT [FK_play_sessions_accounts] FOREIGN KEY([ps_account_id])
REFERENCES [dbo].[accounts] ([ac_account_id])
GO
ALTER TABLE [dbo].[play_sessions] CHECK CONSTRAINT [FK_play_sessions_accounts]
GO
/****** Object:  ForeignKey [FK_play_sessions_terminals]    Script Date: 12/09/2008 16:30:22 ******/
ALTER TABLE [dbo].[play_sessions]  WITH CHECK ADD  CONSTRAINT [FK_play_sessions_terminals] FOREIGN KEY([ps_terminal_id])
REFERENCES [dbo].[terminals] ([te_terminal_id])
GO
ALTER TABLE [dbo].[play_sessions] CHECK CONSTRAINT [FK_play_sessions_terminals]
GO
/****** Object:  ForeignKey [FK_gui_profile_forms_gui_forms]    Script Date: 12/09/2008 16:30:22 ******/
ALTER TABLE [dbo].[gui_profile_forms]  WITH CHECK ADD  CONSTRAINT [FK_gui_profile_forms_gui_forms] FOREIGN KEY([gpf_gui_id], [gpf_form_id])
REFERENCES [dbo].[gui_forms] ([gf_gui_id], [gf_form_id])
GO
ALTER TABLE [dbo].[gui_profile_forms] CHECK CONSTRAINT [FK_gui_profile_forms_gui_forms]
GO
/****** Object:  ForeignKey [FK_gui_profile_forms_gui_user_profiles]    Script Date: 12/09/2008 16:30:22 ******/
ALTER TABLE [dbo].[gui_profile_forms]  WITH CHECK ADD  CONSTRAINT [FK_gui_profile_forms_gui_user_profiles] FOREIGN KEY([gpf_profile_id])
REFERENCES [dbo].[gui_user_profiles] ([gup_profile_id])
GO
ALTER TABLE [dbo].[gui_profile_forms] CHECK CONSTRAINT [FK_gui_profile_forms_gui_user_profiles]
GO
/****** Object:  ForeignKey [FK_wc2_transactions_terminals]    Script Date: 12/09/2008 16:30:22 ******/
ALTER TABLE [dbo].[wc2_transactions]  WITH CHECK ADD  CONSTRAINT [FK_wc2_transactions_terminals] FOREIGN KEY([w2tx_terminal_id])
REFERENCES [dbo].[terminals] ([te_terminal_id])
GO
ALTER TABLE [dbo].[wc2_transactions] CHECK CONSTRAINT [FK_wc2_transactions_terminals]
GO
/****** Object:  ForeignKey [FK_wc2_transactions_wc2_sessions]    Script Date: 12/09/2008 16:30:22 ******/
ALTER TABLE [dbo].[wc2_transactions]  WITH CHECK ADD  CONSTRAINT [FK_wc2_transactions_wc2_sessions] FOREIGN KEY([w2tx_session_id])
REFERENCES [dbo].[wc2_sessions] ([w2s_session_id])
GO
ALTER TABLE [dbo].[wc2_transactions] CHECK CONSTRAINT [FK_wc2_transactions_wc2_sessions]
GO
/****** Object:  ForeignKey [FK_wc2_messages_terminals]    Script Date: 12/09/2008 16:30:22 ******/
ALTER TABLE [dbo].[wc2_messages]  WITH CHECK ADD  CONSTRAINT [FK_wc2_messages_terminals] FOREIGN KEY([w2m_terminal_id])
REFERENCES [dbo].[terminals] ([te_terminal_id])
GO
ALTER TABLE [dbo].[wc2_messages] CHECK CONSTRAINT [FK_wc2_messages_terminals]
GO
/****** Object:  ForeignKey [FK_wc2_messages_wc2_sessions]    Script Date: 12/09/2008 16:30:22 ******/
ALTER TABLE [dbo].[wc2_messages]  WITH CHECK ADD  CONSTRAINT [FK_wc2_messages_wc2_sessions] FOREIGN KEY([w2m_session_id])
REFERENCES [dbo].[wc2_sessions] ([w2s_session_id])
GO
ALTER TABLE [dbo].[wc2_messages] CHECK CONSTRAINT [FK_wc2_messages_wc2_sessions]
GO
/****** Object:  ForeignKey [FK_wcp_transactions_terminals]    Script Date: 12/09/2008 16:30:22 ******/
ALTER TABLE [dbo].[wcp_transactions]  WITH CHECK ADD  CONSTRAINT [FK_wcp_transactions_terminals] FOREIGN KEY([wtx_terminal_id])
REFERENCES [dbo].[terminals] ([te_terminal_id])
GO
ALTER TABLE [dbo].[wcp_transactions] CHECK CONSTRAINT [FK_wcp_transactions_terminals]
GO
/****** Object:  ForeignKey [FK_wcp_transactions_wcp_sessions]    Script Date: 12/09/2008 16:30:22 ******/
ALTER TABLE [dbo].[wcp_transactions]  WITH CHECK ADD  CONSTRAINT [FK_wcp_transactions_wcp_sessions] FOREIGN KEY([wtx_session_id])
REFERENCES [dbo].[wcp_sessions] ([ws_session_id])
GO
ALTER TABLE [dbo].[wcp_transactions] CHECK CONSTRAINT [FK_wcp_transactions_wcp_sessions]
GO
/****** Object:  ForeignKey [FK_meters_terminals]    Script Date: 12/09/2008 16:30:22 ******/
ALTER TABLE [dbo].[meters]  WITH CHECK ADD  CONSTRAINT [FK_meters_terminals] FOREIGN KEY([me_terminal_id])
REFERENCES [dbo].[terminals] ([te_terminal_id])
GO
ALTER TABLE [dbo].[meters] CHECK CONSTRAINT [FK_meters_terminals]
GO
/****** Object:  ForeignKey [FK_wc2_sessions_terminals]    Script Date: 12/09/2008 16:30:22 ******/
ALTER TABLE [dbo].[wc2_sessions]  WITH CHECK ADD  CONSTRAINT [FK_wc2_sessions_terminals] FOREIGN KEY([w2s_terminal_id])
REFERENCES [dbo].[terminals] ([te_terminal_id])
GO
ALTER TABLE [dbo].[wc2_sessions] CHECK CONSTRAINT [FK_wc2_sessions_terminals]
GO
/****** Object:  ForeignKey [FK_wcp_messages_terminals]    Script Date: 12/09/2008 16:30:22 ******/
ALTER TABLE [dbo].[wcp_messages]  WITH CHECK ADD  CONSTRAINT [FK_wcp_messages_terminals] FOREIGN KEY([wm_terminal_id])
REFERENCES [dbo].[terminals] ([te_terminal_id])
GO
ALTER TABLE [dbo].[wcp_messages] CHECK CONSTRAINT [FK_wcp_messages_terminals]
GO
/****** Object:  ForeignKey [FK_wcp_messages_wcp_sessions]    Script Date: 12/09/2008 16:30:22 ******/
ALTER TABLE [dbo].[wcp_messages]  WITH CHECK ADD  CONSTRAINT [FK_wcp_messages_wcp_sessions] FOREIGN KEY([wm_session_id])
REFERENCES [dbo].[wcp_sessions] ([ws_session_id])
GO
ALTER TABLE [dbo].[wcp_messages] CHECK CONSTRAINT [FK_wcp_messages_wcp_sessions]
GO
/****** Object:  ForeignKey [FK_event_history_terminals]    Script Date: 12/09/2008 16:30:22 ******/
ALTER TABLE [dbo].[event_history]  WITH CHECK ADD  CONSTRAINT [FK_event_history_terminals] FOREIGN KEY([eh_terminal_id])
REFERENCES [dbo].[terminals] ([te_terminal_id])
GO
ALTER TABLE [dbo].[event_history] CHECK CONSTRAINT [FK_event_history_terminals]
GO
/****** Object:  ForeignKey [FK_wcp_sessions_terminals]    Script Date: 12/09/2008 16:30:22 ******/
ALTER TABLE [dbo].[wcp_sessions]  WITH CHECK ADD  CONSTRAINT [FK_wcp_sessions_terminals] FOREIGN KEY([ws_terminal_id])
REFERENCES [dbo].[terminals] ([te_terminal_id])
GO
ALTER TABLE [dbo].[wcp_sessions] CHECK CONSTRAINT [FK_wcp_sessions_terminals]
GO
/****** Object:  ForeignKey [FK_Jackpot_History_Draw_Audit_Plays]    Script Date: 12/09/2008 16:30:22 ******/
ALTER TABLE [dbo].[c2_jackpot_history]  WITH CHECK ADD  CONSTRAINT [FK_Jackpot_History_Draw_Audit_Plays] FOREIGN KEY([c2jh_play_id])
REFERENCES [dbo].[c2_draw_audit_plays] ([dap_play_id])
GO
ALTER TABLE [dbo].[c2_jackpot_history] CHECK CONSTRAINT [FK_Jackpot_History_Draw_Audit_Plays]
GO
/****** Object:  ForeignKey [FK_gds_schedule_steps_gds_schedule_time]    Script Date: 12/09/2008 16:30:22 ******/
ALTER TABLE [dbo].[gds_schedule_steps]  WITH CHECK ADD  CONSTRAINT [FK_gds_schedule_steps_gds_schedule_time] FOREIGN KEY([gss_schedule_id])
REFERENCES [dbo].[gds_schedule_time] ([gst_schedule_id])
GO
ALTER TABLE [dbo].[gds_schedule_steps] CHECK CONSTRAINT [FK_gds_schedule_steps_gds_schedule_time]
GO
/****** Object:  ForeignKey [FK_money_meters_meters]    Script Date: 12/09/2008 16:30:22 ******/
ALTER TABLE [dbo].[money_meters]  WITH CHECK ADD  CONSTRAINT [FK_money_meters_meters] FOREIGN KEY([mm_meter_id])
REFERENCES [dbo].[meters] ([me_meter_id])
GO
ALTER TABLE [dbo].[money_meters] CHECK CONSTRAINT [FK_money_meters_meters]
GO
/****** Object:  ForeignKey [FK_cashier_vouchers_cashier_sessions]    Script Date: 12/09/2008 16:30:22 ******/
ALTER TABLE [dbo].[cashier_vouchers]  WITH CHECK ADD  CONSTRAINT [FK_cashier_vouchers_cashier_sessions] FOREIGN KEY([cv_session_id])
REFERENCES [dbo].[cashier_sessions] ([cs_session_id])
GO
ALTER TABLE [dbo].[cashier_vouchers] CHECK CONSTRAINT [FK_cashier_vouchers_cashier_sessions]
GO
/****** Object:  ForeignKey [FK_cashier_movements_cashier_sessions]    Script Date: 12/09/2008 16:30:22 ******/
ALTER TABLE [dbo].[cashier_movements]  WITH CHECK ADD  CONSTRAINT [FK_cashier_movements_cashier_sessions] FOREIGN KEY([cm_session_id])
REFERENCES [dbo].[cashier_sessions] ([cs_session_id])
GO
ALTER TABLE [dbo].[cashier_movements] CHECK CONSTRAINT [FK_cashier_movements_cashier_sessions]
GO
/****** Object:  ForeignKey [FK_gui_users_gui_user_profiles]    Script Date: 12/09/2008 16:30:22 ******/
ALTER TABLE [dbo].[gui_users]  WITH CHECK ADD  CONSTRAINT [FK_gui_users_gui_user_profiles] FOREIGN KEY([gu_profile_id])
REFERENCES [dbo].[gui_user_profiles] ([gup_profile_id])
GO
ALTER TABLE [dbo].[gui_users] CHECK CONSTRAINT [FK_gui_users_gui_user_profiles]
GO
/****** Object:  ForeignKey [FK_gds_group_elements_gds_groups]    Script Date: 12/09/2008 16:30:22 ******/
ALTER TABLE [dbo].[gds_group_elements]  WITH CHECK ADD  CONSTRAINT [FK_gds_group_elements_gds_groups] FOREIGN KEY([gge_group_id])
REFERENCES [dbo].[gds_groups] ([gg_group_id])
GO
ALTER TABLE [dbo].[gds_group_elements] CHECK CONSTRAINT [FK_gds_group_elements_gds_groups]
GO
/****** Object:  ForeignKey [FK_c2_draw_audit_plays_c2_draw_audit]    Script Date: 12/09/2008 16:30:22 ******/
ALTER TABLE [dbo].[c2_draw_audit_plays]  WITH CHECK ADD  CONSTRAINT [FK_c2_draw_audit_plays_c2_draw_audit] FOREIGN KEY([dap_draw_id])
REFERENCES [dbo].[c2_draw_audit] ([da_draw_id])
GO
ALTER TABLE [dbo].[c2_draw_audit_plays] CHECK CONSTRAINT [FK_c2_draw_audit_plays_c2_draw_audit]
GO
/****** Object:  ForeignKey [FK_game_meters_terminals]    Script Date: 19/04/2010 19:00:00 ******/
ALTER TABLE [dbo].[game_meters]  WITH CHECK ADD  CONSTRAINT [FK_game_meters_terminals] FOREIGN KEY([gm_terminal_id])
REFERENCES [dbo].[terminals] ([te_terminal_id])
GO
ALTER TABLE [dbo].[game_meters] CHECK CONSTRAINT [FK_game_meters_terminals]
GO
/****** Object:  ForeignKey [FK_hpc_meter_terminals]    Script Date: 19/04/2010 19:00:00 ******/
ALTER TABLE [dbo].[hpc_meter]  WITH CHECK ADD  CONSTRAINT [FK_hpc_meter_terminals] FOREIGN KEY([hpc_terminal_id])
REFERENCES [dbo].[terminals] ([te_terminal_id])
GO
ALTER TABLE [dbo].[hpc_meter] CHECK CONSTRAINT [FK_hpc_meter_terminals]
GO

/* Stored Procedures */
--------------------------------------------------------------------------------
-- PURPOSE: Inserts a new movement in the database
-- 
--  PARAMS:
--      - INPUT:
--          @PlaySessionId  bigint ,
--          @AccountId      bigint ,
--          @TerminalId     int ,
--          @MovementType   int ,                                       
--          @InitialBalance money , 
--          @SubAmount      money , 
--          @AddAmount      money , 
--          @FinalBalance   money
--
--      - OUTPUT:
--
-- RETURNS:
--      StatusCode, 
--      StatusText, 
--      SessionID
--   NOTES:
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InsertMovement]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[InsertMovement]
GO
CREATE PROCEDURE dbo.InsertMovement
  @PlaySessionId  bigint ,
  @AccountId      bigint ,
  @TerminalId     int ,
  @MovementType   int ,                                       
  @InitialBalance money , 
  @SubAmount      money , 
  @AddAmount      money , 
  @FinalBalance   money
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
  DECLARE @terminal_name nvarchar (254)
  
  SET @terminal_name = ''

  SELECT  @terminal_name = TE_NAME 
    FROM  TERMINALS
   WHERE  TE_TERMINAL_ID = @TerminalId 

  INSERT INTO ACCOUNT_MOVEMENTS (AM_PLAY_SESSION_ID
                               , AM_ACCOUNT_ID 
                               , AM_TERMINAL_ID 
                               , AM_TYPE 
                               , AM_INITIAL_BALANCE 
                               , AM_SUB_AMOUNT 
                               , AM_ADD_AMOUNT 
                               , AM_FINAL_BALANCE 
                               , AM_DATETIME 
                               , AM_TERMINAL_NAME 
                               ) 
                         VALUES (@PlaySessionId
                               , @AccountId
                               , @TerminalId
                               , @MovementType
                               , @InitialBalance
                               , @SubAmount
                               , @AddAmount
                               , @FinalBalance
                               , GETDATE()
                               , @terminal_name 
                               )
                               
  -- SET @MovementId = SCOPE_IDENTITY() 

END -- InsertMovement
GO 

--------------------------------------------------------------------------------
-- PURPOSE : Read PlaySession & Accounts & PlayerTracking data
-- 
--  PARAMS :
--      - INPUT :
--          @PlaySessionId      bigint
--
--      - OUTPUT :
--          @TerminalId                 int            
--          @AccountId                  bigint         
--          @InitialBalance             money          
--          @PlayedAmount               money          
--          @WonAmount                  money          
--          @FinalBalance               money        
--          @InitialNonRedeemable       money        
--          @AccountBalance             money          
--          @HolderName                 nvarchar (50)
--          @HolderLevel                int          
--          @MaxAllowedAccountBalance   numeric (20,6)
--          @StatusCode                 int            
--          @StatusText                 nvarchar (254) 
--
-- RETURNS :
--
--   NOTES :

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PT_ReadData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PT_ReadData]
GO
CREATE PROCEDURE [dbo].[PT_ReadData]
  @PlaySessionId              bigint
, @TerminalId                 int             OUTPUT
, @AccountId                  bigint          OUTPUT
, @InitialBalance             money           OUTPUT
, @PlayedAmount               money           OUTPUT
, @WonAmount                  money           OUTPUT
, @FinalBalance               money           OUTPUT
, @InitialNonRedeemable       money           OUTPUT
, @InitialCashIn              money           OUTPUT
, @PrizeLock                  money           OUTPUT
, @AccountBalance             money           OUTPUT
, @PointsBalance              money           OUTPUT
, @HolderName                 nvarchar (50)   OUTPUT
, @HolderLevel                int             OUTPUT
, @MaxAllowedAccountBalance   numeric (20,6)  OUTPUT
, @StatusCode                 int             OUTPUT
, @StatusText                 nvarchar (254)  OUTPUT    
AS
BEGIN
  DECLARE @rc int
	
  SET @StatusCode = 1
	SET @StatusText = 'PT_ReadData: Reading PlaySession...'
	
	--
	-- Select data from PLAY_SESSIONS & ACCOUNTS table
	--
  SELECT @TerminalId            = PS_TERMINAL_ID
       , @AccountId             = PS_ACCOUNT_ID
       , @InitialBalance        = PS_INITIAL_BALANCE + PS_CASH_IN
       , @PlayedAmount          = PS_PLAYED_AMOUNT
       , @WonAmount             = PS_WON_AMOUNT
       , @FinalBalance          = PS_FINAL_BALANCE
       , @InitialNonRedeemable  = AC_INITIAL_NOT_REDEEMABLE
       , @InitialCashIn         = AC_INITIAL_CASH_IN
       , @PrizeLock             = AC_NR_WON_LOCK
       , @AccountBalance        = AC_BALANCE
       , @PointsBalance         = AC_POINTS 
       , @HolderName            = ISNULL (AC_HOLDER_NAME, '')
       , @HolderLevel           = ISNULL (AC_HOLDER_LEVEL, 0)
    FROM PLAY_SESSIONS, ACCOUNTS
   WHERE PS_PLAY_SESSION_ID = @PlaySessionId
     AND PS_ACCOUNT_ID      = AC_ACCOUNT_ID

  SET @rc = @@ROWCOUNT
  IF ( @rc <> 1 )
  BEGIN
    SET @StatusCode  = 1
    SET @StatusText  = 'PT_ReadData: Invalid PlaySessionId.'
    GOTO ERROR_PROCEDURE
  END

  IF ( @HolderName = '' )
  BEGIN
    SET @HolderLevel = 0  
  END
  
  --- 
  --- Read Max Allowed Account Balance
  ---
  SELECT @MaxAllowedAccountBalance = GP_KEY_VALUE
    FROM GENERAL_PARAMS
   WHERE GP_GROUP_KEY = 'Cashier'
     AND GP_SUBJECT_KEY = 'MaxAllowedAccountBalance'

  SET @rc = @@ROWCOUNT
  IF ( @rc <> 1 )
  BEGIN
    SET @StatusCode = 1
    SET @StatusText = 'PT_ReadData: Invalid Cashier.MaxAllowedAccountBalance general parameter.'
    GOTO ERROR_PROCEDURE
  END
    
  --
  -- Read data successful
  --
  SET @StatusCode = 0
  SET @StatusText = 'PT_ReadData: PlaySession read.'

ERROR_PROCEDURE:

END -- PT_ReadData
GO

--------------------------------------------------------------------------------
-- PURPOSE : Compute the Balance parts.
--           The balace is splitted as:
--              - Non Redeemable
--              - Redeemable
--  PARAMS :
--      - INPUT :
--          @Balance                money
--          @InitialCashIn          money
--          @InitialNonRedeemable   money
--          @PrizeLock              money
--
--      - OUTPUT :
--          @PartNonRedeemable      money
--          @PartRedeemable         money
--
-- RETURNS :
--
--   NOTES :

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PT_BalancePartsWhenPlaying]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PT_BalancePartsWhenPlaying]
GO
CREATE PROCEDURE [dbo].[PT_BalancePartsWhenPlaying]
  @Balance                money
, @InitialCashIn          money
, @InitialNonRedeemable   money
, @PrizeLock              money
, @PartNonRedeemable      money  OUTPUT
, @PartRedeemable         money  OUTPUT
AS
BEGIN
  DECLARE @part_prize money
	
  -- PartNonRedeemable
  SET @PartNonRedeemable = dbo.Minimum_Money (@Balance, @InitialNonRedeemable)
  SET @Balance = @Balance - @PartNonRedeemable
  -- PartRedeemable
  SET @PartRedeemable = dbo.Minimum_Money (@Balance, @InitialCashIn)
  SET @Balance = @Balance - @PartRedeemable
  -- PartPrize
  SET @part_prize = @Balance

  IF ( @PrizeLock > 0 )
    BEGIN
      -- WonLock -> Non Redeemable + Prize
      SET @PartNonRedeemable = @PartNonRedeemable + @part_prize
    END
  ELSE
    BEGIN
      SET @PartRedeemable = @PartRedeemable + @part_prize;
    END

END -- PT_BalancePartsWhenPlaying
GO

--------------------------------------------------------------------------------
-- PURPOSE : Read Point calculation factors
-- 
--  PARAMS :
--      - INPUT :
--          @HolderName
--          @HolderLevel
--
--      - OUTPUT :
--          @RedeemablePlayedTo1Point
--          @TotalPlayedTo1Point
--          @RedeemableSpentTo1Point
--          @StatusCode     
--          @StatusText     
--
-- RETURNS :
--
--   NOTES :

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PT_ReadPointFactors]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PT_ReadPointFactors]
GO
CREATE PROCEDURE [dbo].[PT_ReadPointFactors]
  @HolderLevel                          int
, @RedeemableSpentTo1Point              numeric (20,6)  OUTPUT
, @RedeemablePlayedTo1Point             numeric (20,6)  OUTPUT
, @TotalPlayedTo1Point                  numeric (20,6)  OUTPUT
, @StatusCode                           int             OUTPUT
, @StatusText                           nvarchar (254)  OUTPUT    
AS
BEGIN
  DECLARE @rc           int
  DECLARE @level_name   nvarchar (50)

  SET @StatusCode = 1
  SET @StatusText = 'PT_ReadPointFactors: Reading Point Calculation Factors...'

  SET @RedeemableSpentTo1Point  = 0.00
  SET @RedeemablePlayedTo1Point = 0.00       
  SET @TotalPlayedTo1Point      = 0.00

	--
	-- Select data from GENERAL_PARAMS table: 
	--      * PlayerTracking.LevelXX.RedeemablePlayedTo1Point
	--      * PlayerTracking.LevelXX.TotalPlayedTo1Point
	--      * PlayerTracking.LevelXX.RedeemableSpentTo1Point
	--

  IF ( @HolderLevel = 1 )
    SET @level_name = 'Level01'
  ELSE IF ( @HolderLevel = 2 )
    SET @level_name = 'Level02'
  ELSE IF ( @HolderLevel = 3 )
    SET @level_name = 'Level03'
  ELSE IF ( @HolderLevel = 4 )
    SET @level_name = 'Level04'
  ELSE
    BEGIN
      --- 
      --- Wong Holder's Level 
      ---
      SET @StatusCode = 1
      SET @StatusText = 'PT_ReadPointFactors: Invalid Holder Level.'

      GOTO ERROR_PROCEDURE
    END  

  --- 
  --- Read level's RedeemablePlayedTo1Point
  ---
  SELECT @RedeemablePlayedTo1Point = GP_KEY_VALUE
    FROM GENERAL_PARAMS
   WHERE GP_GROUP_KEY = 'PlayerTracking'
     AND GP_SUBJECT_KEY = @level_name + '.RedeemablePlayedTo1Point'

  SET @rc = @@ROWCOUNT
  IF ( @rc <> 1 )
  BEGIN
    SET @StatusCode = 1
    SET @StatusText = 'PT_ReadPointFactors: Invalid Player Tracking setting Holder Level: PlayerTracking.' + @level_name + '.RedeemablePlayedTo1Point'
    GOTO ERROR_PROCEDURE
  END

  --- 
  --- Read level's TotalPlayedTo1Point
  ---
  SELECT @TotalPlayedTo1Point = GP_KEY_VALUE
    FROM GENERAL_PARAMS
   WHERE GP_GROUP_KEY = 'PlayerTracking'
     AND GP_SUBJECT_KEY = @level_name + '.TotalPlayedTo1Point'  

  SET @rc = @@ROWCOUNT
  IF ( @rc <> 1 )
  BEGIN
    SET @StatusCode = 1
    SET @StatusText = 'PT_ReadPointFactors: Invalid Player Tracking setting Holder Level: PlayerTracking.' + @level_name + '.TotalPlayedTo1Point'
    GOTO ERROR_PROCEDURE
  END

  --- 
  --- Read level's RedeemableSpentTo1Point
  ---
  SELECT @RedeemableSpentTo1Point = GP_KEY_VALUE
    FROM GENERAL_PARAMS
   WHERE GP_GROUP_KEY = 'PlayerTracking'
     AND GP_SUBJECT_KEY = @level_name + '.RedeemableSpentTo1Point'  

  SET @rc = @@ROWCOUNT
  IF ( @rc <> 1 )
  BEGIN
    SET @StatusCode = 1
    SET @StatusText = 'PT_ReadPointFactors: Invalid Player Tracking setting Holder Level: PlayerTracking.' + @level_name + '.RedeemableSpentTo1Point'
    GOTO ERROR_PROCEDURE
  END

  --
  -- Read data successful
  --
  SET @StatusCode = 0
  SET @StatusText = 'PT_ReadPointFactors: Point Calculation read.'

  GOTO EXIT_PROCEDURE

ERROR_PROCEDURE:
  SET @RedeemablePlayedTo1Point = 0.00
  SET @TotalPlayedTo1Point      = 0.00
  SET @RedeemableSpentTo1Point  = 0.00

EXIT_PROCEDURE:

END -- PT_ReadPointFactors
GO

--------------------------------------------------------------------------------
-- PURPOSE : Compute the Balance parts.
--           The balace is splitted as:
--              - Non Redeemable
--              - Redeemable
--  PARAMS :
--      - INPUT :
--          @PlayedTotal            money
--          @PlayedRedeemable       money
--
--      - OUTPUT :
--
-- RETURNS :
--
--   NOTES :

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SiteJackpot_AccumulatePlayed]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SiteJackpot_AccumulatePlayed]
GO
CREATE PROCEDURE [dbo].SiteJackpot_AccumulatePlayed
  @PlayedTotal            money
, @PlayedRedeemable       money
AS
BEGIN
	
  --
  -- Update Site Jackpot Played amounts
  --
  UPDATE SITE_JACKPOT_PARAMETERS
     SET SJP_PLAYED = SJP_PLAYED + CASE WHEN (SJP_ONLY_REDEEMABLE = 1) THEN @PlayedRedeemable ELSE @PlayedTotal END
   WHERE SJP_ENABLED = 1 

END -- SiteJackpot_AccumulatePlayed
GO

--------------------------------------------------------------------------------
-- PURPOSE : Accumulate points for player when play session is finished
-- 
--  PARAMS :
--      - INPUT :
--          @PlaySessionId      bigint
--
--      - OUTPUT :
--
-- RETURNS :
--
--   NOTES :

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PT_PlaySessionFinished]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PT_PlaySessionFinished]
GO
CREATE PROCEDURE [dbo].[PT_PlaySessionFinished]
  @PlaySessionId      bigint
AS
BEGIN

	-- Only for test
  DECLARE @status_code                       int
  DECLARE @status_text                       nvarchar (500)
  -- Only for test
  
  DECLARE @terminal_id                       int
  DECLARE @account_id                        bigint
  DECLARE @total_cash_in                     money
  DECLARE @played_amount                     money
  DECLARE @won_amount                        money
  DECLARE @total_cash_out                    money
  DECLARE @initial_non_redeemable            money
  DECLARE @initial_cash_in                   money
  DECLARE @prize_lock                        money
  DECLARE @account_balance                   money
  DECLARE @holder_name                       nvarchar (50)
  DECLARE @holder_level                      int
  
  DECLARE @non_redeemable_cash_in            money
  DECLARE @redeemable_cash_in                money
  DECLARE @non_redeemable_cash_out           money
  DECLARE @redeemable_cash_out               money
  DECLARE @non_redeemable_played             money
  DECLARE @redeemable_played                 money
  DECLARE @non_redeemable_won                money
  DECLARE @redeemable_won                    money
    
  DECLARE @spent_no_redeemable               money
  DECLARE @spent_redeemable                  money

  DECLARE @profit_no_redeemable              money
  DECLARE @profit_redeemable                 money
  
  DECLARE @total_sum_redimible               numeric (20,6)
  DECLARE @total_sum                         numeric (20,6)
  DECLARE @percent_redeemable                numeric (8,6)

  DECLARE @rest_played_and_no_spent          money
  DECLARE @rest_won_and_no_profit            money
  
  DECLARE @total_played_to_1_point           numeric (20,6)
  DECLARE @redeemable_played_to_1_point      numeric (20,6)
  DECLARE @redeemable_spent_to_1_point       numeric (20,6)

  DECLARE @points_balance_before             money
  DECLARE @points_balance_after              money
  DECLARE @points_total_played               money
  DECLARE @points_played_redeemable          money
  DECLARE @points_spent_redeemable           money
  DECLARE @won_points                        money
  
  DECLARE @max_allowed_acc_balance           numeric (20,6)

  -- SET NOCOUNT ON added to prevent extra result sets from
  -- interfering with SELECT statements.
  SET NOCOUNT ON;
	
  SET @status_code  = 0
  SET @status_text  = 'PT_PlaySessionFinished: Init'

  SET @points_total_played      = 0
  SET @points_played_redeemable = 0
  SET @points_spent_redeemable  = 0
  SET @won_points               = 0  
  SET @points_balance_before    = 0
  SET @points_balance_after     = 0


  IF ( @PlaySessionId = 0 ) 
  BEGIN
    SET @status_text = 'PT_PlaySessionFinished: Invalid PlaySessionId'
    GOTO ERROR_PROCEDURE
  END

  --
  -- Read Session & Account & PlayerTracking Data
  --
  EXECUTE dbo.PT_ReadData @PlaySessionId, @terminal_id OUTPUT, @account_id OUTPUT,
                          @total_cash_in OUTPUT, @played_amount OUTPUT, @won_amount OUTPUT, @total_cash_out OUTPUT, 
                          @initial_non_redeemable OUTPUT, @initial_cash_in OUTPUT, @prize_lock OUTPUT, @account_balance OUTPUT, 
                          @points_balance_before OUTPUT, @holder_name OUTPUT, @holder_level OUTPUT, @max_allowed_acc_balance OUTPUT,
                          @status_code OUTPUT, @status_text OUTPUT

  IF ( @status_code <> 0 )
    GOTO ERROR_PROCEDURE
    
  --
  -- Compute PlayedCredits, PlayedRedeemableCredits, SpentRedeemableCredits
  --
  EXECUTE dbo.PT_BalancePartsWhenPlaying @total_cash_in, @initial_cash_in, @initial_non_redeemable, @prize_lock, 
                                         @non_redeemable_cash_in OUTPUT, @redeemable_cash_in OUTPUT

  EXECUTE dbo.PT_BalancePartsWhenPlaying @total_cash_out, @initial_cash_in, @initial_non_redeemable, @prize_lock, 
                                         @non_redeemable_cash_out OUTPUT, @redeemable_cash_out OUTPUT

  SET @spent_no_redeemable      = dbo.Maximum_Money (0, @non_redeemable_cash_in - @non_redeemable_cash_out)
  SET @spent_redeemable         = dbo.Maximum_Money (0, @redeemable_cash_in - @redeemable_cash_out)

  SET @profit_no_redeemable     = dbo.Maximum_Money (0, @non_redeemable_cash_out - @non_redeemable_cash_in)
  SET @profit_redeemable        = dbo.Maximum_Money (0, @redeemable_cash_out - @redeemable_cash_in)

  SET @total_sum_redimible      = @redeemable_cash_in + @redeemable_cash_out 
  SET @total_sum                = @non_redeemable_cash_in + @non_redeemable_cash_out + @redeemable_cash_in + @redeemable_cash_out
  
  IF @total_sum = 0 GOTO ERROR_PROCEDURE   
  SET @percent_redeemable       = @total_sum_redimible / @total_sum

  SET @rest_played_and_no_spent = @played_amount - (@spent_no_redeemable + @spent_redeemable)
  SET @redeemable_played        = ROUND(@spent_redeemable + (@rest_played_and_no_spent * @percent_redeemable), 4)
  SET @non_redeemable_played    = @played_amount - @redeemable_played

  SET @rest_won_and_no_profit   = @won_amount - (@profit_no_redeemable + @profit_redeemable)
  SET @redeemable_won           = ROUND(@profit_redeemable + (@rest_won_and_no_profit * @percent_redeemable), 4)
  SET @non_redeemable_won       = @won_amount - @redeemable_won

  --
  -- Update PlaySession table (Redeemable & NonRedeemable amounts)
  --
  UPDATE PLAY_SESSIONS
     SET PS_NON_REDEEMABLE_CASH_IN    = @non_redeemable_cash_in
       , PS_NON_REDEEMABLE_CASH_OUT   = @non_redeemable_cash_out
       , PS_NON_REDEEMABLE_PLAYED     = @non_redeemable_played
       , PS_NON_REDEEMABLE_WON        = @non_redeemable_won
       , PS_REDEEMABLE_CASH_IN        = @redeemable_cash_in
       , PS_REDEEMABLE_CASH_OUT       = @redeemable_cash_out
       , PS_REDEEMABLE_PLAYED         = @redeemable_played
       , PS_REDEEMABLE_WON            = @redeemable_won
   WHERE PS_PLAY_SESSION_ID = @PlaySessionId 

  --
  -- RCI & ACC & AJQ 30/09/2010: Reset the Cancellable Operation when PlayedAmount greater than 0.
  --
  UPDATE   ACCOUNTS
     SET   AC_CANCELLABLE_OPERATION_ID = CASE WHEN (@played_amount > 0) THEN NULL ELSE AC_CANCELLABLE_OPERATION_ID END
   WHERE   AC_ACCOUNT_ID               = @account_id  

  --
  -- ACC 22/10/2010: Check maximum allowed account balance
  --
  IF ( @max_allowed_acc_balance > 0 AND @total_cash_out > @max_allowed_acc_balance )
  BEGIN
    UPDATE ACCOUNTS
       SET AC_BLOCKED       = 1
         , AC_BLOCK_REASON  = 2  -- MaxBalance
     WHERE AC_ACCOUNT_ID    = @account_id  
  END

  --- Anonymous accounts have Level=0
  IF ( @holder_level = 0 )
    GOTO EXIT_PROCEDURE

  --
  -- Read PlayerTracking Point Conversion Factors according to the holder's level
  --
  EXECUTE dbo.PT_ReadPointFactors @holder_level, 
                                  @redeemable_spent_to_1_point  OUTPUT,
                                  @redeemable_played_to_1_point OUTPUT,
                                  @total_played_to_1_point      OUTPUT,
                                  @status_code                  OUTPUT, 
                                  @status_text                  OUTPUT

  IF ( @status_code <> 0 )
    GOTO ERROR_PROCEDURE

  --
  -- Compute Points (it is possible to accummulate points per each type of credits at the same time)
  --  
  IF @redeemable_spent_to_1_point  > 0 SET @points_spent_redeemable  = @spent_redeemable  / @redeemable_spent_to_1_point
  IF @redeemable_played_to_1_point > 0 SET @points_played_redeemable = @redeemable_played / @redeemable_played_to_1_point
  IF @total_played_to_1_point      > 0 SET @points_total_played      = @played_amount     / @total_played_to_1_point
  
  SET @won_points = ROUND(@points_total_played + @points_played_redeemable + @points_spent_redeemable, 4)

  --
  -- Accumulate points
  -- 
  UPDATE   ACCOUNTS
     SET   AC_POINTS                   = AC_POINTS  + @won_points
   WHERE   AC_ACCOUNT_ID               = @account_id  
  
  --  
  -- Create CardMovement.ObtainedPoints
  --
  -- PointsAwarded   = 36,
  SET @points_balance_after     = @points_balance_before + @won_points
  
  EXECUTE dbo.InsertMovement @PlaySessionId, @account_id, @terminal_id, 36, @points_balance_before, 0, @won_points, @points_balance_after

EXIT_PROCEDURE:

  --
  -- ACC 25/11/2010 Accumulate played amount into site jackpot
  --
  EXECUTE dbo.SiteJackpot_AccumulatePlayed @played_amount, @redeemable_played

  SET @status_code  = 0
  SET @status_text  = 'PT_PlaySessionFinished: Successful points accumulated.' 
                    + ' *** '
                    + ' Points.PerRedeemableSpent  = ' + CAST (@points_spent_redeemable AS nvarchar)  + ' (' + CAST (@spent_redeemable AS nvarchar)  + ' credits)'
                    + ' Points.PerRedeemablePlayed = ' + CAST (@points_played_redeemable AS nvarchar) + ' (' + CAST (@redeemable_played AS nvarchar) + ' credits)'
                    + ' Points.PerTotalPlayed      = ' + CAST (@points_total_played AS nvarchar)      + ' (' + CAST (@played_amount AS nvarchar)     + ' credits)'
                    + ' Points.TotalAwarded        = ' + CAST (@won_points AS nvarchar) 
                    + ' *** '

ERROR_PROCEDURE:
--
-- ACC 18-OCT-2010 DO NOT SELECT (3GS not works with this select)
--
--  SELECT @status_text AS StatusText

END -- PT_PlaySessionFinished
GO


--------------------------------------------------------------------------------
-- PURPOSE : Trigger on PlaySession when play session is finished
-- 
--  PARAMS :
--      - INPUT :
--
--      - OUTPUT :
--
-- RETURNS :
--
--   NOTES :

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PlayerTrackingTrigger]') AND type in (N'TR'))
DROP TRIGGER [dbo].[PlayerTrackingTrigger]
GO

CREATE TRIGGER [dbo].[PlayerTrackingTrigger]
ON [dbo].[play_sessions]
AFTER UPDATE
NOT FOR REPLICATION
AS
DECLARE @new_status     int
DECLARE @old_status     int
DECLARE @play_session   bigint
BEGIN

  IF UPDATE (PS_STATUS)
  BEGIN
    SET @old_status   = (SELECT PS_STATUS FROM DELETED)
    SET @new_status   = (SELECT PS_STATUS FROM INSERTED)
    SET @play_session = (SELECT PS_PLAY_SESSION_ID FROM INSERTED)

    IF @old_status = 0 AND @new_status <> 0 EXECUTE dbo.PT_PlaySessionFinished @play_session
  END
  
END -- PlayerTrackingTrigger
GO

--------------------------------------------------------------------------------
-- PURPOSE : Trigger on PlaySession when a handpay is registered.
--           Play sessions are directly inserted with status = 1 (Closed).
-- 
--  PARAMS :
--      - INPUT :
--
--      - OUTPUT :
--
-- RETURNS :
--
--   NOTES :

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PlayerTrackingTrigger_Insert]') AND type in (N'TR'))
DROP TRIGGER [dbo].[PlayerTrackingTrigger_Insert]
GO

CREATE TRIGGER [dbo].[PlayerTrackingTrigger_Insert]
ON [dbo].[play_sessions]
AFTER INSERT
NOT FOR REPLICATION
AS
DECLARE @new_status     int
DECLARE @play_session   bigint
BEGIN

  IF UPDATE (PS_STATUS)
  BEGIN
    SET @new_status   = (SELECT PS_STATUS FROM INSERTED)
    SET @play_session = (SELECT PS_PLAY_SESSION_ID FROM INSERTED)

    IF @new_status = 1 EXECUTE dbo.PT_PlaySessionFinished @play_session
  END
  
END -- PlayerTrackingTrigger_Insert
GO

/* Permissions */
USE [wgdb_000]
GO
GRANT CONNECT TO [wggui]
GO
GRANT CONNECT TO [wgpublic]
GO
GRANT CONNECT TO [wgroot]
GO
USE [wgdb_000]
GO
EXEC sp_addrolemember 'db_datawriter', 'wggui'
GO
EXEC sp_addrolemember 'db_datareader', 'wggui'
GO
EXEC sp_addrolemember 'db_owner', 'wgroot'
GO
GRANT SELECT ON [dbo].[db_users] TO [wgpublic]
GO
GRANT SELECT ON [dbo].[db_version] TO [wgpublic]
GO

/****** Initial Inserts ******/
/*** DB Users ***/
INSERT INTO [dbo].[db_users]
           ([du_username]
           ,[du_password])
     VALUES
           ('wggui_000'
           ,cast(0x895ACD254B1EE304CB3CB701AA91BFF04693FC354945C2E5CBB001ABCC063D7B74A0F560C62A073F as binary(40)) )
GO
INSERT INTO [dbo].[db_users]
           ([du_username]
           ,[du_password])
     VALUES
           ('wgroot_000'
           ,cast(0x6B81737F424B3AD13E1FE06EAD0FC67FA4322386476EBCAB448378EFAF0629CE6E6302BFE400B3C9 as binary(40)) )
GO
/*** GUI SU Profile ***/
INSERT INTO [dbo].[gui_user_profiles]
           ([gup_profile_id]
           ,[gup_name])
     VALUES
           ( 0
           , 'PROFILE_SU')
GO
/*** SU ***/
INSERT INTO [dbo].[gui_users]
           ([gu_user_id]
           ,[gu_profile_id]
           ,[gu_username]
           ,[gu_enabled]
           ,[gu_password]
           ,[gu_not_valid_before]
           ,[gu_not_valid_after]
           ,[gu_last_changed]
           ,[gu_password_exp]
           ,[gu_pwd_chg_req]
           ,[gu_login_failures]
           ,[gu_password_h1]
           ,[gu_password_h2]
           ,[gu_password_h3]
           ,[gu_password_h4]
           ,[gu_password_h5]
           ,[gu_full_name])
     VALUES
           (0
           ,0
           ,'SU'
           ,1
           ,cast('wigos00' as binary(40))
           ,getdate()
           ,null
           ,null
           ,null
           ,1
           ,null
           ,null
           ,null
           ,null
           ,null
           ,null
           ,'Super User')
GO
/*** Default GUI User Profiles ***/
INSERT INTO [dbo].[gui_user_profiles]
           ([gup_profile_id]
           ,[gup_name])
     VALUES
           ( 1
           , 'DEFAULT_PROFILE')
GO
INSERT INTO [dbo].[gui_forms] 
                 ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])    /* ENUM_FORM */
		/* Wigos GUI */                 
           SELECT  14, 	        1,            0,               3883           /* FORM_USER_PROFILE_SEL */
 UNION ALL SELECT  14, 	        2,            1,               3825           /* FORM_USER_EDIT */
 UNION ALL SELECT  14,	        3,            2,               3818           /* FORM_PROFILE_EDIT */
 UNION ALL SELECT  14,	        4,            3,               6327           /* FORM_GUI_AUDITOR */
 UNION ALL SELECT  14,	        5,            4,               6267           /* FORM_EVENTS_HISTORY */
 UNION ALL SELECT  14,	        6,            5,               6328           /* FORM_TERMINALS_SELECTION */
 UNION ALL SELECT  14,	        61,           6,               6365           /* FORM_TERMINALS_EDIT */
 UNION ALL SELECT  14,	        49,           7,               6461           /* FORM_TERMINALS_3GS_EDIT */
 UNION ALL SELECT  14,	        55,           8,               6463           /* FORM_TERMINALS_PENDING */
 UNION ALL SELECT  14,	        30,           9,               6460           /* FORM_GENERAL_PARAMS */
 UNION ALL SELECT  14,	        7,            10,              11359          /* FORM_STATISTICS_TERMINALS */
 UNION ALL SELECT  14,	        8,            11,              11360          /* FORM_STATISTICS_GAMES */
 UNION ALL SELECT  14,	        9,            12,              11361          /* FORM_STATISTICS_DATES */
 UNION ALL SELECT  14,	        10,           13,              11362          /* FORM_STATISTICS_HOURS */
 UNION ALL SELECT  14,	        11,           14,              11363          /* FORM_STATISTICS_CARDS */
 UNION ALL SELECT  14,	        12,           15,              11357          /* FORM_PLAYS_SESSIONS */
 UNION ALL SELECT  14,	        13,           16,              11354          /* FORM_PLAYS_PLAYS */
 UNION ALL SELECT  14,	        14,           17,              11355          /* FORM_WS_SESSIONS */
 UNION ALL SELECT  14,	        15,           18,              11366          /* FORM_STATISTICS_HOURS_GROUP */
 UNION ALL SELECT  14,	        16,           19,              13204          /* FORM_ACCT_TAXES */
 UNION ALL SELECT  14,	        17,           20,              13217          /* FORM_CASHIER_MOVS */
 UNION ALL SELECT  14,	        18,           21,              13227          /* FORM_CASHIER_SESSIONS */
 UNION ALL SELECT  14,	        19,           22,              13228          /* FORM_ACCOUNT_MOVEMENTS */
 UNION ALL SELECT  14,	        20,           23,              13229          /* FORM_ACCOUNT_SUMMARY */
 UNION ALL SELECT  14,	        21,           24,              13249          /* FORM_DATE_TERMINAL */
 UNION ALL SELECT  14,	        22,           25,              15952          /* FORM_CLASS_II_DRAW_AUDIT */
 UNION ALL SELECT  14,	        24,           26,              11731          /* FORM_CLASS_II_JACKPOT_CONFIGURATION */
 UNION ALL SELECT  14,	        36,           27,              11735          /* FORM_CLASS_II_JACKPOT_PROMO_MESSAGE */
 UNION ALL SELECT  14,	        25,           28,              11732          /* FORM_CLASS_II_JACKPOT_HISTORY */
 UNION ALL SELECT  14,	        26,           29,              11733          /* FORM_CLASS_II_JACKPOT_MONITOR */
 UNION ALL SELECT  14,	        27,           30,              13701          /* FORM_SW_DOWNLOAD_VERSIONS */
 UNION ALL SELECT  14,	        28,           31,              13800          /* FORM_SW_DOWNLOAD_DETAILS */
 UNION ALL SELECT  14,	        29,           32,              15958          /* FORM_CARD_RECORD */
 UNION ALL SELECT  14,	        31,           33,              13465          /* FORM_PROVIDER_DAY_TERMINAL */
 UNION ALL SELECT  14,	        32,           34,              12201          /* FORM_SERVICE_MONITOR */
 UNION ALL SELECT  14,	        34,           35,              13713          /* FORM_TERMINALS_BY_BUILD */
 UNION ALL SELECT  14,	        35,           36,              13296          /* FORM_CASHIER_SESSION_DETAIL */
 UNION ALL SELECT  14,	        37,           37,              3701           /* FORM_CASHIER_CONFIGURATION */
 UNION ALL SELECT  14,	        39,           38,              13739          /* FORM_MISC_SW_BY_BUILD */
 UNION ALL SELECT  14,	        38,           39,              13729          /* FORM_LICENCE_VERSIONS */
 UNION ALL SELECT  14,	        40,           40,              13751          /* FORM_LICENCE_DETAILS */
 UNION ALL SELECT  14,	        47,           41,              16211          /* FORM_PROMOTIONS */
 UNION ALL SELECT  14,	        48,           42,              16257          /* FORM_PROMOTION_EDIT */
 UNION ALL SELECT  14,	        50,           43,              16210          /* FORM_DRAWS */
 UNION ALL SELECT  14,	        51,           44,              16320          /* FORM_DRAW_EDIT */
 UNION ALL SELECT  14,	        45,           45,              3968           /* FORM_TERMINAL_GAME_RELATION */
 UNION ALL SELECT  14,	        46,           46,              13469          /* FORM_EXPIRED_CREDITS */
 UNION ALL SELECT  14,	        147,          47,              11395          /* FORM_GAME_METERS */
 UNION ALL SELECT  14,	        52,           48,              5421           /* FORM_COMMANDS_SEND */
 UNION ALL SELECT  14,	        53,           49,              5422           /* FORM_COMMANDS_HISTORY */
 UNION ALL SELECT  14,	        66,           50,              13361          /* FORM_TERMINALS_WITHOUT_ACTIVITY */
 UNION ALL SELECT  14,	        54,           51,              6462           /* FORM_HANDPAYS */
 UNION ALL SELECT  14,	        56,           52,              13313          /* FORM_MOBILE_BANK_MOVEMENTS */
 UNION ALL SELECT  14,	        58,           53,              16349          /* FORM_GIFTS_SEL */
 UNION ALL SELECT  14,	        59,           54,              16352          /* FORM_GIFTS_EDIT */
 UNION ALL SELECT  14,	        65,           55,              16429          /* FORM_GIFTS_HISTORY */
 UNION ALL SELECT  14,	        60,           56,              13346          /* FORM_TAXES_REPORT */
 UNION ALL SELECT  14,	        57,           57,              13345          /* FORM_PROMOTION_REPORT */
 UNION ALL SELECT  14,	        63,           58,              3982           /* FORM_SITE_GAP_REPORT */
 UNION ALL SELECT  14,	        62,           59,              11830          /* FORM_SITE_JACKPOT_CONFIGURATION */
 UNION ALL SELECT  14,	        67,           60,              11425          /* FORM_MAILING_PROGRAMMING_SEL */
 UNION ALL SELECT  14,	        68,           61,              11426          /* FORM_MAILING_PROGRAMMING_EDIT */
 UNION ALL SELECT  14,	        69,           62,              11437          /* FORM_MAILING_PARAMETERS */
 UNION ALL SELECT  14,	        64,           63,              11850          /* FORM_SITE_JACKPOT_HISTORY */
 		/* CASHIER */
 UNION ALL SELECT  15,	        0,            0,               3845           /* FORM_MAIN */
 UNION ALL SELECT  15,	        1,            1,               3846           /* FORM_VIRTUAL_ADD_NO_REDEEM */
 UNION ALL SELECT  15,	        2,            2,               3847           /* FORM_VIRTUAL_CASH_DESK_OPEN_CLOSE */
 UNION ALL SELECT  15,	        3,            3,               3849           /* FORM_VIRTUAL_CASH_DESK_DEPOSIT */
 UNION ALL SELECT  15,	        4,            4,               3850           /* FORM_VIRTUAL_CASH_DESK_WITHDRAWN */
 UNION ALL SELECT  15,	        5,            5,               3851           /* FORM_VIRTUAL_CARD_OPER_ADD_REDEEM_CREDIT */
 UNION ALL SELECT  15,	        6,            6,               3854           /* FORM_VIRTUAL_CARD_OPER_LAST_MOV */
 UNION ALL SELECT  15,	        7,            7,               3855           /* FORM_VIRTUAL_ACCOUNT_OPER_CARD */
 UNION ALL SELECT  15,	        8,            8,               3856           /* FORM_VIRTUAL_ACCOUNT_OPER_EDIT */
 UNION ALL SELECT  15,	        9,            9,               3857           /* FORM_VIRTUAL_ACCOUNT_OPER_LOCK */
 UNION ALL SELECT  15,	        10,           10,              3863           /* FORM_VIRTUAL_PRINT_CARD */
 UNION ALL SELECT  15,	        11,           11,              3859           /* FORM_VIRTUAL_OPTIONS_DB_CONFIG */
 UNION ALL SELECT  15,	        12,           12,              3860           /* FORM_VIRTUAL_OPTIONS_LANGUAGE */
 UNION ALL SELECT  15,	        13,           13,              3861           /* FORM_VIRTUAL_OPTIONS_CALIBRATE */
 UNION ALL SELECT  15,	        14,           14,              3862           /* FORM_VIRTUAL_GAME_SESSIONS */
 UNION ALL SELECT  15,	        15,           15,              3858           /* FORM_VIRTUAL_MOBILE_BANK */
 UNION ALL SELECT  15,	        16,           16,              3848           /* FORM_VIRTUAL_CASH_DESK_ACCESS */
 UNION ALL SELECT  15,	        17,           17,              3864           /* FORM_VIRTUAL_ACCOUNT_OPER_HANDPAY */
 UNION ALL SELECT  15,	        18,           18,              3865           /* FORM_VIRTUAL_ACCOUNT_OPER_HANDPAY_CANCEL */
 UNION ALL SELECT  15,	        19,           19,              3978           /* FORM_VIRTUAL_ACCOUNT_LOG_OFF */
 UNION ALL SELECT  15,	        20,           20,              3866           /* FORM_VIRTUAL_MANUAL_OPER_HANDPAY */
 UNION ALL SELECT  15,	        21,           21,              3867           /* FORM_VIRTUAL_MANUAL_OPER_HANDPAY_CANCEL */
 UNION ALL SELECT  15,	        22,           22,              3872           /* FORM_VIRTUAL_GIFT_REQUEST */
 UNION ALL SELECT  15,	        23,           23,              3873           /* FORM_VIRTUAL_GIFT_DELIVERY */
 UNION ALL SELECT  15,	        24,           24,              3979           /* FORM_VIRTUAL_ACCOUNT_LEVEL */
 UNION ALL SELECT  15,	        25,           25,              3980           /* FORM_VIRTUAL_PROMO_SPECIAL_PERMISSION_A */
 UNION ALL SELECT  15,	        26,           26,              3981           /* FORM_VIRTUAL_PROMO_SPECIAL_PERMISSION_B */
		         
GO

INSERT INTO [dbo].[gui_user_profiles]
           ([gup_profile_id]
           ,[gup_name])
     VALUES
           ( 2
           , 'AUDITOR DE SALA')
GO

      INSERT INTO [dbo].[gui_profile_forms]
                 ([gpf_profile_id], [gpf_gui_id], [gpf_form_id], [gpf_read_perm], [gpf_write_perm], [gpf_delete_perm], [gpf_execute_perm])
                         /*  FORM  R   W   D   X    */
           SELECT  2,  14,  1,   1,  0,  1,  1    /* FORM_USER_PROFILE_SEL */
 UNION ALL SELECT  2,  14,  2,   1,  0,  1,  1    /* FORM_USER_EDIT */
 UNION ALL SELECT  2,  14,  3,   1,  0,  1,  1    /* FORM_PROFILE_EDIT */
 UNION ALL SELECT  2,  14,  4,   0,  0,  0,  0    /* FORM_GUI_AUDITOR */
 UNION ALL SELECT  2,  14,  5,   0,  0,  0,  0    /* FORM_EVENTS_HISTORY */
 UNION ALL SELECT  2,  14,  6,   0,  0,  0,  0    /* FORM_TERMINALS_SELECTION */
 UNION ALL SELECT  2,  14,  7,   1,  0,  1,  1    /* FORM_STATISTICS_TERMINALS */
 UNION ALL SELECT  2,  14,  8,   1,  0,  1,  1    /* FORM_STATISTICS_GAMES */
 UNION ALL SELECT  2,  14,  9,   1,  0,  1,  1    /* FORM_STATISTICS_DATES */
 UNION ALL SELECT  2,  14,  10,  1,  0,  1,  1    /* FORM_STATISTICS_HOURS */
 UNION ALL SELECT  2,  14,  11,  1,  0,  1,  1    /* FORM_STATISTICS_CARDS */
 UNION ALL SELECT  2,  14,  12,  1,  0,  1,  1    /* FORM_PLAYS_SESSIONS */
 UNION ALL SELECT  2,  14,  13,  1,  0,  1,  1    /* FORM_PLAYS_PLAYS */
 UNION ALL SELECT  2,  14,  14,  1,  0,  1,  1    /* FORM_WS_SESSIONS */
 UNION ALL SELECT  2,  14,  15,  1,  0,  1,  1    /* FORM_STATISTICS_HOURS_GROUP */
 UNION ALL SELECT  2,  14,  16,  1,  0,  1,  1    /* FORM_ACCT_TAXES */
 UNION ALL SELECT  2,  14,  17,  1,  0,  1,  1    /* FORM_CASHIER_MOVS */
 UNION ALL SELECT  2,  14,  18,  1,  0,  1,  1    /* FORM_CASHIER_SESSIONS */
 UNION ALL SELECT  2,  14,  19,  1,  0,  1,  1    /* FORM_ACCOUNT_MOVEMENTS */
 UNION ALL SELECT  2,  14,  20,  1,  0,  1,  1    /* FORM_ACCOUNT_SUMMARY */
 UNION ALL SELECT  2,  14,  21,  1,  0,  1,  1    /* FORM_DATE_TERMINAL */
 UNION ALL SELECT  2,  14,  22,  0,  0,  0,  0    /* FORM_CLASS_II_DRAW_AUDIT */
 UNION ALL SELECT  2,  14,  24,  0,  0,  0,  0    /* FORM_CLASS_II_JACKPOT_CONFIGURATION */
 UNION ALL SELECT  2,  14,  25,  0,  0,  0,  0    /* FORM_CLASS_II_JACKPOT_HISTORY */
 UNION ALL SELECT  2,  14,  26,  0,  0,  0,  0    /* FORM_CLASS_II_JACKPOT_MONITOR */
 UNION ALL SELECT  2,  14,  27,  0,  0,  0,  0    /* FORM_SW_DOWNLOAD_VERSIONS */
 UNION ALL SELECT  2,  14,  28,  0,  0,  0,  0    /* FORM_SW_DOWNLOAD_DETAILS */
 UNION ALL SELECT  2,  14,  29,  0,  0,  0,  0    /* FORM_CARD_RECORD */
 UNION ALL SELECT  2,  14,  30,  0,  0,  0,  0    /* FORM_GENERAL_PARAMS */
 UNION ALL SELECT  2,  14,  31,  1,  0,  1,  1    /* FORM_PROVIDER_DAY_TERMINAL */
 UNION ALL SELECT  2,  14,  32,  0,  0,  0,  0    /* FORM_SERVICE_MONITOR */
 UNION ALL SELECT  2,  14,  34,  0,  0,  0,  0    /* FORM_TERMINALS_BY_BUILD */
 UNION ALL SELECT  2,  14,  35,  1,  0,  1,  1    /* FORM_CASHIER_SESSION_DETAIL */
 UNION ALL SELECT  2,  14,  36,  0,  0,  0,  0    /* FORM_CLASS_II_JACKPOT_PROMO_MESSAGE */
 UNION ALL SELECT  2,  14,  37,  0,  0,  0,  0    /* FORM_CASHIER_CONFIGURATION */
 UNION ALL SELECT  2,  14,  38,  0,  0,  0,  0    /* FORM_LICENCE_VERSIONS */
 UNION ALL SELECT  2,  14,  39,  0,  0,  0,  0    /* FORM_MISC_SW_BY_BUILD */
 UNION ALL SELECT  2,  14,  40,  0,  0,  0,  0    /* FORM_LICENCE_DETAILS */
 UNION ALL SELECT  2,  14,  45,  0,  0,  0,  0    /* FORM_TERMINAL_GAME_RELATION */
 UNION ALL SELECT  2,  14,  46,  1,  0,  1,  1    /* FORM_EXPIRED_CREDITS */
 UNION ALL SELECT  2,  14,  47,  1,  0,  1,  1    /* FORM_PROMOTIONS */
 UNION ALL SELECT  2,  14,  48,  0,  0,  0,  0    /* FORM_PROMOTION_EDIT */
 UNION ALL SELECT  2,  14,  49,  0,  0,  0,  0    /* FORM_TERMINALS_3GS_EDIT */
 UNION ALL SELECT  2,  14,  50,  1,  0,  1,  1    /* FORM_DRAWS */
 UNION ALL SELECT  2,  14,  51,  0,  0,  0,  0    /* FORM_DRAW_EDIT */
 UNION ALL SELECT  2,  14,  52,  0,  0,  0,  0    /* FORM_COMMANDS_SEND */
 UNION ALL SELECT  2,  14,  53,  0,  0,  0,  0    /* FORM_COMMANDS_HISTORY */
 UNION ALL SELECT  2,  14,  54,  1,  0,  1,  1    /* FORM_HANDPAYS */
 UNION ALL SELECT  2,  14,  55,  0,  0,  0,  0    /* FORM_TERMINALS_PENDING */
 UNION ALL SELECT  2,  14,  56,  1,  0,  1,  1    /* FORM_MOBILE_BANK_MOVEMENTS */
 UNION ALL SELECT  2,  14,  57,  1,  0,  1,  1    /* FORM_PROMOTION_REPORT */
 UNION ALL SELECT  2,  14,  58,  1,  0,  1,  1    /* FORM_GIFTS_SEL */
 UNION ALL SELECT  2,  14,  59,  1,  0,  1,  1    /* FORM_GIFTS_EDIT */
 UNION ALL SELECT  2,  14,  60,  1,  0,  1,  1    /* FORM_TAXES_REPORT */
 UNION ALL SELECT  2,  14,  61,  0,  0,  0,  0    /* FORM_TERMINALS_EDIT */
 UNION ALL SELECT  2,  14,  62,  0,  0,  0,  0    /* FORM_SITE_JACKPOT_CONFIGURATION */
 UNION ALL SELECT  2,  14,  63,  1,  0,  1,  1    /* FORM_SITE_GAP_REPORT */
 UNION ALL SELECT  2,  14,  64,  0,  0,  0,  0    /* FORM_SITE_JACKPOT_HISTORY */
 UNION ALL SELECT  2,  14,  65,  1,  0,  1,  1    /* FORM_GIFTS_HISTORY */
 UNION ALL SELECT  2,  14,  66,  1,  0,  1,  1    /* FORM_TERMINALS_WITHOUT_ACTIVITY */
 UNION ALL SELECT  2,  14,  67,  0,  0,  0,  0    /* FORM_MAILING_PROGRAMMING_SEL */
 UNION ALL SELECT  2,  14,  68,  0,  0,  0,  0    /* FORM_MAILING_PROGRAMMING_EDIT */
 UNION ALL SELECT  2,  14,  69,  0,  0,  0,  0    /* FORM_MAILING_PARAMETERS */
 UNION ALL SELECT  2,  14,  147, 0,  0,  0,  0    /* FORM_GAME_METERS */
 		/* CASHIER */
 UNION ALL SELECT  2,  15,  0,   0,  0,  0,  0    /* FORM_MAIN */
 UNION ALL SELECT  2,  15,  1,   0,  0,  0,  0    /* FORM_VIRTUAL_ADD_NO_REDEEM */
 UNION ALL SELECT  2,  15,  2,   0,  0,  0,  0    /* FORM_VIRTUAL_CASH_DESK_OPEN_CLOSE */
 UNION ALL SELECT  2,  15,  3,   0,  0,  0,  0    /* FORM_VIRTUAL_CASH_DESK_DEPOSIT */
 UNION ALL SELECT  2,  15,  4,   0,  0,  0,  0    /* FORM_VIRTUAL_CASH_DESK_WITHDRAWN */
 UNION ALL SELECT  2,  15,  5,   0,  0,  0,  0    /* FORM_VIRTUAL_CARD_OPER_ADD_REDEEM_CREDIT */
 UNION ALL SELECT  2,  15,  6,   0,  0,  0,  0    /* FORM_VIRTUAL_CARD_OPER_LAST_MOV */
 UNION ALL SELECT  2,  15,  7,   0,  0,  0,  0    /* FORM_VIRTUAL_ACCOUNT_OPER_CARD */
 UNION ALL SELECT  2,  15,  8,   0,  0,  0,  0    /* FORM_VIRTUAL_ACCOUNT_OPER_EDIT */
 UNION ALL SELECT  2,  15,  9,   0,  0,  0,  0    /* FORM_VIRTUAL_ACCOUNT_OPER_LOCK */
 UNION ALL SELECT  2,  15,  10,  0,  0,  0,  0    /* FORM_VIRTUAL_PRINT_CARD */
 UNION ALL SELECT  2,  15,  11,  0,  0,  0,  0    /* FORM_VIRTUAL_OPTIONS_DB_CONFIG */
 UNION ALL SELECT  2,  15,  12,  0,  0,  0,  0    /* FORM_VIRTUAL_OPTIONS_LANGUAGE */
 UNION ALL SELECT  2,  15,  13,  0,  0,  0,  0    /* FORM_VIRTUAL_OPTIONS_CALIBRATE */
 UNION ALL SELECT  2,  15,  14,  0,  0,  0,  0    /* FORM_VIRTUAL_GAME_SESSIONS */
 UNION ALL SELECT  2,  15,  15,  0,  0,  0,  0    /* FORM_VIRTUAL_MOBILE_BANK */
 UNION ALL SELECT  2,  15,  16,  0,  0,  0,  0    /* FORM_VIRTUAL_CASH_DESK_ACCESS */
 UNION ALL SELECT  2,  15,  17,  0,  0,  0,  0    /* FORM_VIRTUAL_ACCOUNT_OPER_HANDPAY */
 UNION ALL SELECT  2,  15,  18,  0,  0,  0,  0    /* FORM_VIRTUAL_ACCOUNT_OPER_HANDPAY_CANCEL */
 UNION ALL SELECT  2,  15,  19,  0,  0,  0,  0    /* FORM_VIRTUAL_ACCOUNT_LOG_OFF */
 UNION ALL SELECT  2,  15,  20,  0,  0,  0,  0    /* FORM_VIRTUAL_MANUAL_OPER_HANDPAY */
 UNION ALL SELECT  2,  15,  21,  0,  0,  0,  0    /* FORM_VIRTUAL_MANUAL_OPER_HANDPAY_CANCEL */
 UNION ALL SELECT  2,  15,  22,  0,  0,  0,  0    /* FORM_VIRTUAL_GIFT_REQUEST */
 UNION ALL SELECT  2,  15,  23,  0,  0,  0,  0    /* FORM_VIRTUAL_GIFT_DELIVERY */
 UNION ALL SELECT  2,  15,  24,  0,  0,  0,  0    /* FORM_VIRTUAL_ACCOUNT_LEVEL */
 UNION ALL SELECT  2,  15,  25,  0,  0,  0,  0    /* FORM_VIRTUAL_PROMO_SPECIAL_PERMISSION_A */
 UNION ALL SELECT  2,  15,  26,  0,  0,  0,  0    /* FORM_VIRTUAL_PROMO_SPECIAL_PERMISSION_B */
 
 GO
 
 INSERT INTO [dbo].[gui_user_profiles]
            ([gup_profile_id]
            ,[gup_name])
      VALUES
            ( 3
            , 'BANCO MÓVIL')
 GO
 
       INSERT INTO [dbo].[gui_profile_forms]
                  ([gpf_profile_id], [gpf_gui_id], [gpf_form_id], [gpf_read_perm], [gpf_write_perm], [gpf_delete_perm], [gpf_execute_perm])
                          /*  FORM  R   W   D   X    */
            SELECT  3,  14,  1,   0,  0,  0,  0    /* FORM_USER_PROFILE_SEL */
  UNION ALL SELECT  3,  14,  2,   0,  0,  0,  0    /* FORM_USER_EDIT */
  UNION ALL SELECT  3,  14,  3,   0,  0,  0,  0    /* FORM_PROFILE_EDIT */
  UNION ALL SELECT  3,  14,  4,   0,  0,  0,  0    /* FORM_GUI_AUDITOR */
  UNION ALL SELECT  3,  14,  5,   0,  0,  0,  0    /* FORM_EVENTS_HISTORY */
  UNION ALL SELECT  3,  14,  6,   0,  0,  0,  0    /* FORM_TERMINALS_SELECTION */
  UNION ALL SELECT  3,  14,  7,   0,  0,  0,  0    /* FORM_STATISTICS_TERMINALS */
  UNION ALL SELECT  3,  14,  8,   0,  0,  0,  0    /* FORM_STATISTICS_GAMES */
  UNION ALL SELECT  3,  14,  9,   0,  0,  0,  0    /* FORM_STATISTICS_DATES */
  UNION ALL SELECT  3,  14,  10,  0,  0,  0,  0    /* FORM_STATISTICS_HOURS */
  UNION ALL SELECT  3,  14,  11,  0,  0,  0,  0    /* FORM_STATISTICS_CARDS */
  UNION ALL SELECT  3,  14,  12,  0,  0,  0,  0    /* FORM_PLAYS_SESSIONS */
  UNION ALL SELECT  3,  14,  13,  0,  0,  0,  0    /* FORM_PLAYS_PLAYS */
  UNION ALL SELECT  3,  14,  14,  0,  0,  0,  0    /* FORM_WS_SESSIONS */
  UNION ALL SELECT  3,  14,  15,  0,  0,  0,  0    /* FORM_STATISTICS_HOURS_GROUP */
  UNION ALL SELECT  3,  14,  16,  0,  0,  0,  0    /* FORM_ACCT_TAXES */
  UNION ALL SELECT  3,  14,  17,  0,  0,  0,  0    /* FORM_CASHIER_MOVS */
  UNION ALL SELECT  3,  14,  18,  0,  0,  0,  0    /* FORM_CASHIER_SESSIONS */
  UNION ALL SELECT  3,  14,  19,  0,  0,  0,  0    /* FORM_ACCOUNT_MOVEMENTS */
  UNION ALL SELECT  3,  14,  20,  0,  0,  0,  0    /* FORM_ACCOUNT_SUMMARY */
  UNION ALL SELECT  3,  14,  21,  0,  0,  0,  0    /* FORM_DATE_TERMINAL */
  UNION ALL SELECT  3,  14,  22,  0,  0,  0,  0    /* FORM_CLASS_II_DRAW_AUDIT */
  UNION ALL SELECT  3,  14,  24,  0,  0,  0,  0    /* FORM_CLASS_II_JACKPOT_CONFIGURATION */
  UNION ALL SELECT  3,  14,  25,  0,  0,  0,  0    /* FORM_CLASS_II_JACKPOT_HISTORY */
  UNION ALL SELECT  3,  14,  26,  0,  0,  0,  0    /* FORM_CLASS_II_JACKPOT_MONITOR */
  UNION ALL SELECT  3,  14,  27,  0,  0,  0,  0    /* FORM_SW_DOWNLOAD_VERSIONS */
  UNION ALL SELECT  3,  14,  28,  0,  0,  0,  0    /* FORM_SW_DOWNLOAD_DETAILS */
  UNION ALL SELECT  3,  14,  29,  0,  0,  0,  0    /* FORM_CARD_RECORD */
  UNION ALL SELECT  3,  14,  30,  0,  0,  0,  0    /* FORM_GENERAL_PARAMS */
  UNION ALL SELECT  3,  14,  31,  0,  0,  0,  0    /* FORM_PROVIDER_DAY_TERMINAL */
  UNION ALL SELECT  3,  14,  32,  0,  0,  0,  0    /* FORM_SERVICE_MONITOR */
  UNION ALL SELECT  3,  14,  34,  0,  0,  0,  0    /* FORM_TERMINALS_BY_BUILD */
  UNION ALL SELECT  3,  14,  35,  0,  0,  0,  0    /* FORM_CASHIER_SESSION_DETAIL */
  UNION ALL SELECT  3,  14,  36,  0,  0,  0,  0    /* FORM_CLASS_II_JACKPOT_PROMO_MESSAGE */
  UNION ALL SELECT  3,  14,  37,  0,  0,  0,  0    /* FORM_CASHIER_CONFIGURATION */
  UNION ALL SELECT  3,  14,  38,  0,  0,  0,  0    /* FORM_LICENCE_VERSIONS */
  UNION ALL SELECT  3,  14,  39,  0,  0,  0,  0    /* FORM_MISC_SW_BY_BUILD */
  UNION ALL SELECT  3,  14,  40,  0,  0,  0,  0    /* FORM_LICENCE_DETAILS */
  UNION ALL SELECT  3,  14,  45,  0,  0,  0,  0    /* FORM_TERMINAL_GAME_RELATION */
  UNION ALL SELECT  3,  14,  46,  0,  0,  0,  0    /* FORM_EXPIRED_CREDITS */
  UNION ALL SELECT  3,  14,  47,  0,  0,  0,  0    /* FORM_PROMOTIONS */
  UNION ALL SELECT  3,  14,  48,  0,  0,  0,  0    /* FORM_PROMOTION_EDIT */
  UNION ALL SELECT  3,  14,  49,  0,  0,  0,  0    /* FORM_TERMINALS_3GS_EDIT */
  UNION ALL SELECT  3,  14,  50,  0,  0,  0,  0    /* FORM_DRAWS */
  UNION ALL SELECT  3,  14,  51,  0,  0,  0,  0    /* FORM_DRAW_EDIT */
  UNION ALL SELECT  3,  14,  52,  0,  0,  0,  0    /* FORM_COMMANDS_SEND */
  UNION ALL SELECT  3,  14,  53,  0,  0,  0,  0    /* FORM_COMMANDS_HISTORY */
  UNION ALL SELECT  3,  14,  54,  0,  0,  0,  0    /* FORM_HANDPAYS */
  UNION ALL SELECT  3,  14,  55,  0,  0,  0,  0    /* FORM_TERMINALS_PENDING */
  UNION ALL SELECT  3,  14,  56,  0,  0,  0,  0    /* FORM_MOBILE_BANK_MOVEMENTS */
  UNION ALL SELECT  3,  14,  57,  0,  0,  0,  0    /* FORM_PROMOTION_REPORT */
  UNION ALL SELECT  3,  14,  58,  0,  0,  0,  0    /* FORM_GIFTS_SEL */
  UNION ALL SELECT  3,  14,  59,  0,  0,  0,  0    /* FORM_GIFTS_EDIT */
  UNION ALL SELECT  3,  14,  60,  0,  0,  0,  0    /* FORM_TAXES_REPORT */
  UNION ALL SELECT  3,  14,  61,  0,  0,  0,  0    /* FORM_TERMINALS_EDIT */
  UNION ALL SELECT  3,  14,  62,  0,  0,  0,  0    /* FORM_SITE_JACKPOT_CONFIGURATION */
  UNION ALL SELECT  3,  14,  63,  0,  0,  0,  0    /* FORM_SITE_GAP_REPORT */
  UNION ALL SELECT  3,  14,  64,  0,  0,  0,  0    /* FORM_SITE_JACKPOT_HISTORY */
  UNION ALL SELECT  3,  14,  65,  0,  0,  0,  0    /* FORM_GIFTS_HISTORY */
  UNION ALL SELECT  3,  14,  66,  0,  0,  0,  0    /* FORM_TERMINALS_WITHOUT_ACTIVITY */
  UNION ALL SELECT  3,  14,  67,  0,  0,  0,  0    /* FORM_MAILING_PROGRAMMING_SEL */
  UNION ALL SELECT  3,  14,  68,  0,  0,  0,  0    /* FORM_MAILING_PROGRAMMING_EDIT */
  UNION ALL SELECT  3,  14,  69,  0,  0,  0,  0    /* FORM_MAILING_PARAMETERS */
  UNION ALL SELECT  3,  14,  147, 0,  0,  0,  0    /* FORM_GAME_METERS */
  		/* CASHIER */
  UNION ALL SELECT  3,  15,  0,   1,  1,  1,  1    /* FORM_MAIN */
  UNION ALL SELECT  3,  15,  1,   0,  0,  0,  0    /* FORM_VIRTUAL_ADD_NO_REDEEM */
  UNION ALL SELECT  3,  15,  2,   1,  1,  1,  1    /* FORM_VIRTUAL_CASH_DESK_OPEN_CLOSE */
  UNION ALL SELECT  3,  15,  3,   0,  0,  0,  0    /* FORM_VIRTUAL_CASH_DESK_DEPOSIT */
  UNION ALL SELECT  3,  15,  4,   0,  0,  0,  0    /* FORM_VIRTUAL_CASH_DESK_WITHDRAWN */
  UNION ALL SELECT  3,  15,  5,   0,  0,  0,  0    /* FORM_VIRTUAL_CARD_OPER_ADD_REDEEM_CREDIT */
  UNION ALL SELECT  3,  15,  6,   0,  0,  0,  0    /* FORM_VIRTUAL_CARD_OPER_LAST_MOV */
  UNION ALL SELECT  3,  15,  7,   0,  0,  0,  0    /* FORM_VIRTUAL_ACCOUNT_OPER_CARD */
  UNION ALL SELECT  3,  15,  8,   0,  0,  0,  0    /* FORM_VIRTUAL_ACCOUNT_OPER_EDIT */
  UNION ALL SELECT  3,  15,  9,   0,  0,  0,  0    /* FORM_VIRTUAL_ACCOUNT_OPER_LOCK */
  UNION ALL SELECT  3,  15,  10,  0,  0,  0,  0    /* FORM_VIRTUAL_PRINT_CARD */
  UNION ALL SELECT  3,  15,  11,  0,  0,  0,  0    /* FORM_VIRTUAL_OPTIONS_DB_CONFIG */
  UNION ALL SELECT  3,  15,  12,  0,  0,  0,  0    /* FORM_VIRTUAL_OPTIONS_LANGUAGE */
  UNION ALL SELECT  3,  15,  13,  0,  0,  0,  0    /* FORM_VIRTUAL_OPTIONS_CALIBRATE */
  UNION ALL SELECT  3,  15,  14,  0,  0,  0,  0    /* FORM_VIRTUAL_GAME_SESSIONS */
  UNION ALL SELECT  3,  15,  15,  1,  1,  1,  1    /* FORM_VIRTUAL_MOBILE_BANK */
  UNION ALL SELECT  3,  15,  16,  1,  1,  1,  1    /* FORM_VIRTUAL_CASH_DESK_ACCESS */
  UNION ALL SELECT  3,  15,  17,  0,  0,  0,  0    /* FORM_VIRTUAL_ACCOUNT_OPER_HANDPAY */
  UNION ALL SELECT  3,  15,  18,  0,  0,  0,  0    /* FORM_VIRTUAL_ACCOUNT_OPER_HANDPAY_CANCEL */
  UNION ALL SELECT  3,  15,  19,  0,  0,  0,  0    /* FORM_VIRTUAL_ACCOUNT_LOG_OFF */
  UNION ALL SELECT  3,  15,  20,  0,  0,  0,  0    /* FORM_VIRTUAL_MANUAL_OPER_HANDPAY */
  UNION ALL SELECT  3,  15,  21,  0,  0,  0,  0    /* FORM_VIRTUAL_MANUAL_OPER_HANDPAY_CANCEL */
  UNION ALL SELECT  3,  15,  22,  0,  0,  0,  0    /* FORM_VIRTUAL_GIFT_REQUEST */
  UNION ALL SELECT  3,  15,  23,  0,  0,  0,  0    /* FORM_VIRTUAL_GIFT_DELIVERY */
  UNION ALL SELECT  3,  15,  24,  0,  0,  0,  0    /* FORM_VIRTUAL_ACCOUNT_LEVEL */
  UNION ALL SELECT  3,  15,  25,  0,  0,  0,  0    /* FORM_VIRTUAL_PROMO_SPECIAL_PERMISSION_A */
  UNION ALL SELECT  3,  15,  26,  0,  0,  0,  0    /* FORM_VIRTUAL_PROMO_SPECIAL_PERMISSION_B */
  
 GO
 
  INSERT INTO [dbo].[gui_user_profiles]
             ([gup_profile_id]
             ,[gup_name])
       VALUES
             ( 4
             , 'CAJERO')
 GO
  
        INSERT INTO [dbo].[gui_profile_forms]
                   ([gpf_profile_id], [gpf_gui_id], [gpf_form_id], [gpf_read_perm], [gpf_write_perm], [gpf_delete_perm], [gpf_execute_perm])
                           /*  FORM  R   W   D   X    */
             SELECT  4,  14,  1,   0,  0,  0,  0    /* FORM_USER_PROFILE_SEL */
   UNION ALL SELECT  4,  14,  2,   0,  0,  0,  0    /* FORM_USER_EDIT */
   UNION ALL SELECT  4,  14,  3,   0,  0,  0,  0    /* FORM_PROFILE_EDIT */
   UNION ALL SELECT  4,  14,  4,   0,  0,  0,  0    /* FORM_GUI_AUDITOR */
   UNION ALL SELECT  4,  14,  5,   0,  0,  0,  0    /* FORM_EVENTS_HISTORY */
   UNION ALL SELECT  4,  14,  6,   0,  0,  0,  0    /* FORM_TERMINALS_SELECTION */
   UNION ALL SELECT  4,  14,  7,   0,  0,  0,  0    /* FORM_STATISTICS_TERMINALS */
   UNION ALL SELECT  4,  14,  8,   0,  0,  0,  0    /* FORM_STATISTICS_GAMES */
   UNION ALL SELECT  4,  14,  9,   0,  0,  0,  0    /* FORM_STATISTICS_DATES */
   UNION ALL SELECT  4,  14,  10,  0,  0,  0,  0    /* FORM_STATISTICS_HOURS */
   UNION ALL SELECT  4,  14,  11,  0,  0,  0,  0    /* FORM_STATISTICS_CARDS */
   UNION ALL SELECT  4,  14,  12,  0,  0,  0,  0    /* FORM_PLAYS_SESSIONS */
   UNION ALL SELECT  4,  14,  13,  0,  0,  0,  0    /* FORM_PLAYS_PLAYS */
   UNION ALL SELECT  4,  14,  14,  0,  0,  0,  0    /* FORM_WS_SESSIONS */
   UNION ALL SELECT  4,  14,  15,  0,  0,  0,  0    /* FORM_STATISTICS_HOURS_GROUP */
   UNION ALL SELECT  4,  14,  16,  0,  0,  0,  0    /* FORM_ACCT_TAXES */
   UNION ALL SELECT  4,  14,  17,  0,  0,  0,  0    /* FORM_CASHIER_MOVS */
   UNION ALL SELECT  4,  14,  18,  0,  0,  0,  0    /* FORM_CASHIER_SESSIONS */
   UNION ALL SELECT  4,  14,  19,  0,  0,  0,  0    /* FORM_ACCOUNT_MOVEMENTS */
   UNION ALL SELECT  4,  14,  20,  0,  0,  0,  0    /* FORM_ACCOUNT_SUMMARY */
   UNION ALL SELECT  4,  14,  21,  0,  0,  0,  0    /* FORM_DATE_TERMINAL */
   UNION ALL SELECT  4,  14,  22,  0,  0,  0,  0    /* FORM_CLASS_II_DRAW_AUDIT */
   UNION ALL SELECT  4,  14,  24,  0,  0,  0,  0    /* FORM_CLASS_II_JACKPOT_CONFIGURATION */
   UNION ALL SELECT  4,  14,  25,  0,  0,  0,  0    /* FORM_CLASS_II_JACKPOT_HISTORY */
   UNION ALL SELECT  4,  14,  26,  0,  0,  0,  0    /* FORM_CLASS_II_JACKPOT_MONITOR */
   UNION ALL SELECT  4,  14,  27,  0,  0,  0,  0    /* FORM_SW_DOWNLOAD_VERSIONS */
   UNION ALL SELECT  4,  14,  28,  0,  0,  0,  0    /* FORM_SW_DOWNLOAD_DETAILS */
   UNION ALL SELECT  4,  14,  29,  0,  0,  0,  0    /* FORM_CARD_RECORD */
   UNION ALL SELECT  4,  14,  30,  0,  0,  0,  0    /* FORM_GENERAL_PARAMS */
   UNION ALL SELECT  4,  14,  31,  0,  0,  0,  0    /* FORM_PROVIDER_DAY_TERMINAL */
   UNION ALL SELECT  4,  14,  32,  0,  0,  0,  0    /* FORM_SERVICE_MONITOR */
   UNION ALL SELECT  4,  14,  34,  0,  0,  0,  0    /* FORM_TERMINALS_BY_BUILD */
   UNION ALL SELECT  4,  14,  35,  0,  0,  0,  0    /* FORM_CASHIER_SESSION_DETAIL */
   UNION ALL SELECT  4,  14,  36,  0,  0,  0,  0    /* FORM_CLASS_II_JACKPOT_PROMO_MESSAGE */
   UNION ALL SELECT  4,  14,  37,  0,  0,  0,  0    /* FORM_CASHIER_CONFIGURATION */
   UNION ALL SELECT  4,  14,  38,  0,  0,  0,  0    /* FORM_LICENCE_VERSIONS */
   UNION ALL SELECT  4,  14,  39,  0,  0,  0,  0    /* FORM_MISC_SW_BY_BUILD */
   UNION ALL SELECT  4,  14,  40,  0,  0,  0,  0    /* FORM_LICENCE_DETAILS */
   UNION ALL SELECT  4,  14,  45,  0,  0,  0,  0    /* FORM_TERMINAL_GAME_RELATION */
   UNION ALL SELECT  4,  14,  46,  0,  0,  0,  0    /* FORM_EXPIRED_CREDITS */
   UNION ALL SELECT  4,  14,  47,  0,  0,  0,  0    /* FORM_PROMOTIONS */
   UNION ALL SELECT  4,  14,  48,  0,  0,  0,  0    /* FORM_PROMOTION_EDIT */
   UNION ALL SELECT  4,  14,  49,  0,  0,  0,  0    /* FORM_TERMINALS_3GS_EDIT */
   UNION ALL SELECT  4,  14,  50,  0,  0,  0,  0    /* FORM_DRAWS */
   UNION ALL SELECT  4,  14,  51,  0,  0,  0,  0    /* FORM_DRAW_EDIT */
   UNION ALL SELECT  4,  14,  52,  0,  0,  0,  0    /* FORM_COMMANDS_SEND */
   UNION ALL SELECT  4,  14,  53,  0,  0,  0,  0    /* FORM_COMMANDS_HISTORY */
   UNION ALL SELECT  4,  14,  54,  0,  0,  0,  0    /* FORM_HANDPAYS */
   UNION ALL SELECT  4,  14,  55,  0,  0,  0,  0    /* FORM_TERMINALS_PENDING */
   UNION ALL SELECT  4,  14,  56,  0,  0,  0,  0    /* FORM_MOBILE_BANK_MOVEMENTS */
   UNION ALL SELECT  4,  14,  57,  0,  0,  0,  0    /* FORM_PROMOTION_REPORT */
   UNION ALL SELECT  4,  14,  58,  0,  0,  0,  0    /* FORM_GIFTS_SEL */
   UNION ALL SELECT  4,  14,  59,  0,  0,  0,  0    /* FORM_GIFTS_EDIT */
   UNION ALL SELECT  4,  14,  60,  0,  0,  0,  0    /* FORM_TAXES_REPORT */
   UNION ALL SELECT  4,  14,  61,  0,  0,  0,  0    /* FORM_TERMINALS_EDIT */
   UNION ALL SELECT  4,  14,  62,  0,  0,  0,  0    /* FORM_SITE_JACKPOT_CONFIGURATION */
   UNION ALL SELECT  4,  14,  63,  0,  0,  0,  0    /* FORM_SITE_GAP_REPORT */
   UNION ALL SELECT  4,  14,  64,  0,  0,  0,  0    /* FORM_SITE_JACKPOT_HISTORY */
   UNION ALL SELECT  4,  14,  65,  0,  0,  0,  0    /* FORM_GIFTS_HISTORY */
   UNION ALL SELECT  4,  14,  66,  0,  0,  0,  0    /* FORM_TERMINALS_WITHOUT_ACTIVITY */
   UNION ALL SELECT  4,  14,  67,  0,  0,  0,  0    /* FORM_MAILING_PROGRAMMING_SEL */
   UNION ALL SELECT  4,  14,  68,  0,  0,  0,  0    /* FORM_MAILING_PROGRAMMING_EDIT */
   UNION ALL SELECT  4,  14,  69,  0,  0,  0,  0    /* FORM_MAILING_PARAMETERS */
   UNION ALL SELECT  4,  14,  147, 0,  0,  0,  0    /* FORM_GAME_METERS */
   		/* CASHIER */
   UNION ALL SELECT  4,  15,  0,   1,  1,  1,  1    /* FORM_MAIN */
   UNION ALL SELECT  4,  15,  1,   0,  0,  0,  0    /* FORM_VIRTUAL_ADD_NO_REDEEM */
   UNION ALL SELECT  4,  15,  2,   0,  0,  0,  0    /* FORM_VIRTUAL_CASH_DESK_OPEN_CLOSE */
   UNION ALL SELECT  4,  15,  3,   0,  0,  0,  0    /* FORM_VIRTUAL_CASH_DESK_DEPOSIT */
   UNION ALL SELECT  4,  15,  4,   0,  0,  0,  0    /* FORM_VIRTUAL_CASH_DESK_WITHDRAWN */
   UNION ALL SELECT  4,  15,  5,   1,  1,  1,  1    /* FORM_VIRTUAL_CARD_OPER_ADD_REDEEM_CREDIT */
   UNION ALL SELECT  4,  15,  6,   0,  0,  0,  0    /* FORM_VIRTUAL_CARD_OPER_LAST_MOV */
   UNION ALL SELECT  4,  15,  7,   0,  0,  0,  0    /* FORM_VIRTUAL_ACCOUNT_OPER_CARD */
   UNION ALL SELECT  4,  15,  8,   1,  1,  1,  1    /* FORM_VIRTUAL_ACCOUNT_OPER_EDIT */
   UNION ALL SELECT  4,  15,  9,   1,  1,  1,  1    /* FORM_VIRTUAL_ACCOUNT_OPER_LOCK */
   UNION ALL SELECT  4,  15,  10,  1,  1,  1,  1    /* FORM_VIRTUAL_PRINT_CARD */
   UNION ALL SELECT  4,  15,  11,  0,  0,  0,  0    /* FORM_VIRTUAL_OPTIONS_DB_CONFIG */
   UNION ALL SELECT  4,  15,  12,  0,  0,  0,  0    /* FORM_VIRTUAL_OPTIONS_LANGUAGE */
   UNION ALL SELECT  4,  15,  13,  0,  0,  0,  0    /* FORM_VIRTUAL_OPTIONS_CALIBRATE */
   UNION ALL SELECT  4,  15,  14,  0,  0,  0,  0    /* FORM_VIRTUAL_GAME_SESSIONS */
   UNION ALL SELECT  4,  15,  15,  0,  0,  0,  0    /* FORM_VIRTUAL_MOBILE_BANK */
   UNION ALL SELECT  4,  15,  16,  0,  0,  0,  0    /* FORM_VIRTUAL_CASH_DESK_ACCESS */
   UNION ALL SELECT  4,  15,  17,  0,  0,  0,  0    /* FORM_VIRTUAL_ACCOUNT_OPER_HANDPAY */
   UNION ALL SELECT  4,  15,  18,  0,  0,  0,  0    /* FORM_VIRTUAL_ACCOUNT_OPER_HANDPAY_CANCEL */
   UNION ALL SELECT  4,  15,  19,  0,  0,  0,  0    /* FORM_VIRTUAL_ACCOUNT_LOG_OFF */
   UNION ALL SELECT  4,  15,  20,  0,  0,  0,  0    /* FORM_VIRTUAL_MANUAL_OPER_HANDPAY */
   UNION ALL SELECT  4,  15,  21,  0,  0,  0,  0    /* FORM_VIRTUAL_MANUAL_OPER_HANDPAY_CANCEL */
   UNION ALL SELECT  4,  15,  22,  0,  0,  0,  0    /* FORM_VIRTUAL_GIFT_REQUEST */
   UNION ALL SELECT  4,  15,  23,  0,  0,  0,  0    /* FORM_VIRTUAL_GIFT_DELIVERY */
   UNION ALL SELECT  4,  15,  24,  0,  0,  0,  0    /* FORM_VIRTUAL_ACCOUNT_LEVEL */
   UNION ALL SELECT  4,  15,  25,  0,  0,  0,  0    /* FORM_VIRTUAL_PROMO_SPECIAL_PERMISSION_A */
   UNION ALL SELECT  4,  15,  26,  0,  0,  0,  0    /* FORM_VIRTUAL_PROMO_SPECIAL_PERMISSION_B */
   
GO
 
 INSERT INTO [dbo].[gui_user_profiles]
            ([gup_profile_id]
            ,[gup_name])
      VALUES
            ( 5
            , 'GERENTE')
GO
 
       INSERT INTO [dbo].[gui_profile_forms]
                  ([gpf_profile_id], [gpf_gui_id], [gpf_form_id], [gpf_read_perm], [gpf_write_perm], [gpf_delete_perm], [gpf_execute_perm])
                          /*  FORM  R   W   D   X    */
            SELECT  5,  14,  1,   1,  0,  1,  1    /* FORM_USER_PROFILE_SEL */
  UNION ALL SELECT  5,  14,  2,   1,  0,  1,  1    /* FORM_USER_EDIT */
  UNION ALL SELECT  5,  14,  3,   1,  0,  1,  1    /* FORM_PROFILE_EDIT */
  UNION ALL SELECT  5,  14,  4,   1,  0,  1,  1    /* FORM_GUI_AUDITOR */
  UNION ALL SELECT  5,  14,  5,   0,  0,  0,  0    /* FORM_EVENTS_HISTORY */
  UNION ALL SELECT  5,  14,  6,   0,  0,  0,  0    /* FORM_TERMINALS_SELECTION */
  UNION ALL SELECT  5,  14,  7,   1,  0,  1,  1    /* FORM_STATISTICS_TERMINALS */
  UNION ALL SELECT  5,  14,  8,   1,  0,  1,  1    /* FORM_STATISTICS_GAMES */
  UNION ALL SELECT  5,  14,  9,   1,  0,  1,  1    /* FORM_STATISTICS_DATES */
  UNION ALL SELECT  5,  14,  10,  1,  0,  1,  1    /* FORM_STATISTICS_HOURS */
  UNION ALL SELECT  5,  14,  11,  1,  0,  1,  1    /* FORM_STATISTICS_CARDS */
  UNION ALL SELECT  5,  14,  12,  1,  0,  1,  1    /* FORM_PLAYS_SESSIONS */
  UNION ALL SELECT  5,  14,  13,  1,  0,  1,  1    /* FORM_PLAYS_PLAYS */
  UNION ALL SELECT  5,  14,  14,  0,  0,  0,  0    /* FORM_WS_SESSIONS */
  UNION ALL SELECT  5,  14,  15,  1,  0,  1,  1    /* FORM_STATISTICS_HOURS_GROUP */
  UNION ALL SELECT  5,  14,  16,  1,  0,  1,  1    /* FORM_ACCT_TAXES */
  UNION ALL SELECT  5,  14,  17,  1,  0,  1,  1    /* FORM_CASHIER_MOVS */
  UNION ALL SELECT  5,  14,  18,  1,  0,  1,  1    /* FORM_CASHIER_SESSIONS */
  UNION ALL SELECT  5,  14,  19,  1,  0,  1,  1    /* FORM_ACCOUNT_MOVEMENTS */
  UNION ALL SELECT  5,  14,  20,  1,  0,  1,  1    /* FORM_ACCOUNT_SUMMARY */
  UNION ALL SELECT  5,  14,  21,  1,  0,  1,  1    /* FORM_DATE_TERMINAL */
  UNION ALL SELECT  5,  14,  22,  0,  0,  0,  0    /* FORM_CLASS_II_DRAW_AUDIT */
  UNION ALL SELECT  5,  14,  24,  0,  0,  0,  0    /* FORM_CLASS_II_JACKPOT_CONFIGURATION */
  UNION ALL SELECT  5,  14,  25,  0,  0,  0,  0    /* FORM_CLASS_II_JACKPOT_HISTORY */
  UNION ALL SELECT  5,  14,  26,  0,  0,  0,  0    /* FORM_CLASS_II_JACKPOT_MONITOR */
  UNION ALL SELECT  5,  14,  27,  0,  0,  0,  0    /* FORM_SW_DOWNLOAD_VERSIONS */
  UNION ALL SELECT  5,  14,  28,  0,  0,  0,  0    /* FORM_SW_DOWNLOAD_DETAILS */
  UNION ALL SELECT  5,  14,  29,  0,  0,  0,  0    /* FORM_CARD_RECORD */
  UNION ALL SELECT  5,  14,  30,  0,  0,  0,  0    /* FORM_GENERAL_PARAMS */
  UNION ALL SELECT  5,  14,  31,  1,  0,  1,  1    /* FORM_PROVIDER_DAY_TERMINAL */
  UNION ALL SELECT  5,  14,  32,  0,  0,  0,  0    /* FORM_SERVICE_MONITOR */
  UNION ALL SELECT  5,  14,  34,  0,  0,  0,  0    /* FORM_TERMINALS_BY_BUILD */
  UNION ALL SELECT  5,  14,  35,  1,  0,  1,  1    /* FORM_CASHIER_SESSION_DETAIL */
  UNION ALL SELECT  5,  14,  36,  0,  0,  0,  0    /* FORM_CLASS_II_JACKPOT_PROMO_MESSAGE */
  UNION ALL SELECT  5,  14,  37,  0,  0,  0,  0    /* FORM_CASHIER_CONFIGURATION */
  UNION ALL SELECT  5,  14,  38,  0,  0,  0,  0    /* FORM_LICENCE_VERSIONS */
  UNION ALL SELECT  5,  14,  39,  0,  0,  0,  0    /* FORM_MISC_SW_BY_BUILD */
  UNION ALL SELECT  5,  14,  40,  0,  0,  0,  0    /* FORM_LICENCE_DETAILS */
  UNION ALL SELECT  5,  14,  45,  0,  0,  0,  0    /* FORM_TERMINAL_GAME_RELATION */
  UNION ALL SELECT  5,  14,  46,  1,  0,  1,  1    /* FORM_EXPIRED_CREDITS */
  UNION ALL SELECT  5,  14,  47,  1,  0,  1,  1    /* FORM_PROMOTIONS */
  UNION ALL SELECT  5,  14,  48,  1,  0,  1,  1    /* FORM_PROMOTION_EDIT */
  UNION ALL SELECT  5,  14,  49,  0,  0,  0,  0    /* FORM_TERMINALS_3GS_EDIT */
  UNION ALL SELECT  5,  14,  50,  1,  0,  1,  1    /* FORM_DRAWS */
  UNION ALL SELECT  5,  14,  51,  1,  0,  1,  1    /* FORM_DRAW_EDIT */
  UNION ALL SELECT  5,  14,  52,  0,  0,  0,  0    /* FORM_COMMANDS_SEND */
  UNION ALL SELECT  5,  14,  53,  0,  0,  0,  0    /* FORM_COMMANDS_HISTORY */
  UNION ALL SELECT  5,  14,  54,  1,  0,  1,  1    /* FORM_HANDPAYS */
  UNION ALL SELECT  5,  14,  55,  0,  0,  0,  0    /* FORM_TERMINALS_PENDING */
  UNION ALL SELECT  5,  14,  56,  1,  0,  1,  1    /* FORM_MOBILE_BANK_MOVEMENTS */
  UNION ALL SELECT  5,  14,  57,  1,  0,  1,  1    /* FORM_PROMOTION_REPORT */
  UNION ALL SELECT  5,  14,  58,  1,  0,  1,  1    /* FORM_GIFTS_SEL */
  UNION ALL SELECT  5,  14,  59,  1,  0,  1,  1    /* FORM_GIFTS_EDIT */
  UNION ALL SELECT  5,  14,  60,  1,  0,  1,  1    /* FORM_TAXES_REPORT */
  UNION ALL SELECT  5,  14,  61,  0,  0,  0,  0    /* FORM_TERMINALS_EDIT */
  UNION ALL SELECT  5,  14,  62,  0,  0,  0,  0    /* FORM_SITE_JACKPOT_CONFIGURATION */
  UNION ALL SELECT  5,  14,  63,  1,  0,  1,  1    /* FORM_SITE_GAP_REPORT */
  UNION ALL SELECT  5,  14,  64,  1,  0,  1,  1    /* FORM_SITE_JACKPOT_HISTORY */
  UNION ALL SELECT  5,  14,  65,  1,  0,  1,  1    /* FORM_GIFTS_HISTORY */
  UNION ALL SELECT  5,  14,  66,  1,  0,  1,  1    /* FORM_TERMINALS_WITHOUT_ACTIVITY */
  UNION ALL SELECT  5,  14,  67,  0,  0,  0,  0    /* FORM_MAILING_PROGRAMMING_SEL */
  UNION ALL SELECT  5,  14,  68,  0,  0,  0,  0    /* FORM_MAILING_PROGRAMMING_EDIT */
  UNION ALL SELECT  5,  14,  69,  0,  0,  0,  0    /* FORM_MAILING_PARAMETERS */
  UNION ALL SELECT  5,  14,  147, 0,  0,  0,  0    /* FORM_GAME_METERS */
  		/* CASHIER */
  UNION ALL SELECT  5,  15,  0,   0,  0,  0,  0    /* FORM_MAIN */
  UNION ALL SELECT  5,  15,  1,   0,  0,  0,  0    /* FORM_VIRTUAL_ADD_NO_REDEEM */
  UNION ALL SELECT  5,  15,  2,   0,  0,  0,  0    /* FORM_VIRTUAL_CASH_DESK_OPEN_CLOSE */
  UNION ALL SELECT  5,  15,  3,   0,  0,  0,  0    /* FORM_VIRTUAL_CASH_DESK_DEPOSIT */
  UNION ALL SELECT  5,  15,  4,   0,  0,  0,  0    /* FORM_VIRTUAL_CASH_DESK_WITHDRAWN */
  UNION ALL SELECT  5,  15,  5,   0,  0,  0,  0    /* FORM_VIRTUAL_CARD_OPER_ADD_REDEEM_CREDIT */
  UNION ALL SELECT  5,  15,  6,   0,  0,  0,  0    /* FORM_VIRTUAL_CARD_OPER_LAST_MOV */
  UNION ALL SELECT  5,  15,  7,   0,  0,  0,  0    /* FORM_VIRTUAL_ACCOUNT_OPER_CARD */
  UNION ALL SELECT  5,  15,  8,   0,  0,  0,  0    /* FORM_VIRTUAL_ACCOUNT_OPER_EDIT */
  UNION ALL SELECT  5,  15,  9,   0,  0,  0,  0    /* FORM_VIRTUAL_ACCOUNT_OPER_LOCK */
  UNION ALL SELECT  5,  15,  10,  0,  0,  0,  0    /* FORM_VIRTUAL_PRINT_CARD */
  UNION ALL SELECT  5,  15,  11,  0,  0,  0,  0    /* FORM_VIRTUAL_OPTIONS_DB_CONFIG */
  UNION ALL SELECT  5,  15,  12,  0,  0,  0,  0    /* FORM_VIRTUAL_OPTIONS_LANGUAGE */
  UNION ALL SELECT  5,  15,  13,  0,  0,  0,  0    /* FORM_VIRTUAL_OPTIONS_CALIBRATE */
  UNION ALL SELECT  5,  15,  14,  0,  0,  0,  0    /* FORM_VIRTUAL_GAME_SESSIONS */
  UNION ALL SELECT  5,  15,  15,  0,  0,  0,  0    /* FORM_VIRTUAL_MOBILE_BANK */
  UNION ALL SELECT  5,  15,  16,  0,  0,  0,  0    /* FORM_VIRTUAL_CASH_DESK_ACCESS */
  UNION ALL SELECT  5,  15,  17,  0,  0,  0,  0    /* FORM_VIRTUAL_ACCOUNT_OPER_HANDPAY */
  UNION ALL SELECT  5,  15,  18,  0,  0,  0,  0    /* FORM_VIRTUAL_ACCOUNT_OPER_HANDPAY_CANCEL */
  UNION ALL SELECT  5,  15,  19,  0,  0,  0,  0    /* FORM_VIRTUAL_ACCOUNT_LOG_OFF */
  UNION ALL SELECT  5,  15,  20,  0,  0,  0,  0    /* FORM_VIRTUAL_MANUAL_OPER_HANDPAY */
  UNION ALL SELECT  5,  15,  21,  0,  0,  0,  0    /* FORM_VIRTUAL_MANUAL_OPER_HANDPAY_CANCEL */
  UNION ALL SELECT  5,  15,  22,  0,  0,  0,  0    /* FORM_VIRTUAL_GIFT_REQUEST */
  UNION ALL SELECT  5,  15,  23,  0,  0,  0,  0    /* FORM_VIRTUAL_GIFT_DELIVERY */
  UNION ALL SELECT  5,  15,  24,  0,  0,  0,  0    /* FORM_VIRTUAL_ACCOUNT_LEVEL */
  UNION ALL SELECT  5,  15,  25,  0,  0,  0,  0    /* FORM_VIRTUAL_PROMO_SPECIAL_PERMISSION_A */
  UNION ALL SELECT  5,  15,  26,  0,  0,  0,  0    /* FORM_VIRTUAL_PROMO_SPECIAL_PERMISSION_B */
  
GO
 
   INSERT INTO [dbo].[gui_user_profiles]
              ([gup_profile_id]
              ,[gup_name])
        VALUES
              ( 6
              , 'JEFE DE CAJA')
GO
   
         INSERT INTO [dbo].[gui_profile_forms]
                    ([gpf_profile_id], [gpf_gui_id], [gpf_form_id], [gpf_read_perm], [gpf_write_perm], [gpf_delete_perm], [gpf_execute_perm])
                            /*  FORM  R   W   D   X    */
              SELECT  6,  14,  1,   0,  0,  0,  0    /* FORM_USER_PROFILE_SEL */
    UNION ALL SELECT  6,  14,  2,   0,  0,  0,  0    /* FORM_USER_EDIT */
    UNION ALL SELECT  6,  14,  3,   0,  0,  0,  0    /* FORM_PROFILE_EDIT */
    UNION ALL SELECT  6,  14,  4,   0,  0,  0,  0    /* FORM_GUI_AUDITOR */
    UNION ALL SELECT  6,  14,  5,   0,  0,  0,  0    /* FORM_EVENTS_HISTORY */
    UNION ALL SELECT  6,  14,  6,   0,  0,  0,  0    /* FORM_TERMINALS_SELECTION */
    UNION ALL SELECT  6,  14,  7,   0,  0,  0,  0    /* FORM_STATISTICS_TERMINALS */
    UNION ALL SELECT  6,  14,  8,   0,  0,  0,  0    /* FORM_STATISTICS_GAMES */
    UNION ALL SELECT  6,  14,  9,   0,  0,  0,  0    /* FORM_STATISTICS_DATES */
    UNION ALL SELECT  6,  14,  10,  0,  0,  0,  0    /* FORM_STATISTICS_HOURS */
    UNION ALL SELECT  6,  14,  11,  0,  0,  0,  0    /* FORM_STATISTICS_CARDS */
    UNION ALL SELECT  6,  14,  12,  0,  0,  0,  0    /* FORM_PLAYS_SESSIONS */
    UNION ALL SELECT  6,  14,  13,  0,  0,  0,  0    /* FORM_PLAYS_PLAYS */
    UNION ALL SELECT  6,  14,  14,  0,  0,  0,  0    /* FORM_WS_SESSIONS */
    UNION ALL SELECT  6,  14,  15,  0,  0,  0,  0    /* FORM_STATISTICS_HOURS_GROUP */
    UNION ALL SELECT  6,  14,  16,  0,  0,  0,  0    /* FORM_ACCT_TAXES */
    UNION ALL SELECT  6,  14,  17,  0,  0,  0,  0    /* FORM_CASHIER_MOVS */
    UNION ALL SELECT  6,  14,  18,  0,  0,  0,  0    /* FORM_CASHIER_SESSIONS */
    UNION ALL SELECT  6,  14,  19,  0,  0,  0,  0    /* FORM_ACCOUNT_MOVEMENTS */
    UNION ALL SELECT  6,  14,  20,  0,  0,  0,  0    /* FORM_ACCOUNT_SUMMARY */
    UNION ALL SELECT  6,  14,  21,  0,  0,  0,  0    /* FORM_DATE_TERMINAL */
    UNION ALL SELECT  6,  14,  22,  0,  0,  0,  0    /* FORM_CLASS_II_DRAW_AUDIT */
    UNION ALL SELECT  6,  14,  24,  0,  0,  0,  0    /* FORM_CLASS_II_JACKPOT_CONFIGURATION */
    UNION ALL SELECT  6,  14,  25,  0,  0,  0,  0    /* FORM_CLASS_II_JACKPOT_HISTORY */
    UNION ALL SELECT  6,  14,  26,  0,  0,  0,  0    /* FORM_CLASS_II_JACKPOT_MONITOR */
    UNION ALL SELECT  6,  14,  27,  0,  0,  0,  0    /* FORM_SW_DOWNLOAD_VERSIONS */
    UNION ALL SELECT  6,  14,  28,  0,  0,  0,  0    /* FORM_SW_DOWNLOAD_DETAILS */
    UNION ALL SELECT  6,  14,  29,  0,  0,  0,  0    /* FORM_CARD_RECORD */
    UNION ALL SELECT  6,  14,  30,  0,  0,  0,  0    /* FORM_GENERAL_PARAMS */
    UNION ALL SELECT  6,  14,  31,  0,  0,  0,  0    /* FORM_PROVIDER_DAY_TERMINAL */
    UNION ALL SELECT  6,  14,  32,  0,  0,  0,  0    /* FORM_SERVICE_MONITOR */
    UNION ALL SELECT  6,  14,  34,  0,  0,  0,  0    /* FORM_TERMINALS_BY_BUILD */
    UNION ALL SELECT  6,  14,  35,  0,  0,  0,  0    /* FORM_CASHIER_SESSION_DETAIL */
    UNION ALL SELECT  6,  14,  36,  0,  0,  0,  0    /* FORM_CLASS_II_JACKPOT_PROMO_MESSAGE */
    UNION ALL SELECT  6,  14,  37,  0,  0,  0,  0    /* FORM_CASHIER_CONFIGURATION */
    UNION ALL SELECT  6,  14,  38,  0,  0,  0,  0    /* FORM_LICENCE_VERSIONS */
    UNION ALL SELECT  6,  14,  39,  0,  0,  0,  0    /* FORM_MISC_SW_BY_BUILD */
    UNION ALL SELECT  6,  14,  40,  0,  0,  0,  0    /* FORM_LICENCE_DETAILS */
    UNION ALL SELECT  6,  14,  45,  0,  0,  0,  0    /* FORM_TERMINAL_GAME_RELATION */
    UNION ALL SELECT  6,  14,  46,  0,  0,  0,  0    /* FORM_EXPIRED_CREDITS */
    UNION ALL SELECT  6,  14,  47,  0,  0,  0,  0    /* FORM_PROMOTIONS */
    UNION ALL SELECT  6,  14,  48,  0,  0,  0,  0    /* FORM_PROMOTION_EDIT */
    UNION ALL SELECT  6,  14,  49,  0,  0,  0,  0    /* FORM_TERMINALS_3GS_EDIT */
    UNION ALL SELECT  6,  14,  50,  0,  0,  0,  0    /* FORM_DRAWS */
    UNION ALL SELECT  6,  14,  51,  0,  0,  0,  0    /* FORM_DRAW_EDIT */
    UNION ALL SELECT  6,  14,  52,  0,  0,  0,  0    /* FORM_COMMANDS_SEND */
    UNION ALL SELECT  6,  14,  53,  0,  0,  0,  0    /* FORM_COMMANDS_HISTORY */
    UNION ALL SELECT  6,  14,  54,  0,  0,  0,  0    /* FORM_HANDPAYS */
    UNION ALL SELECT  6,  14,  55,  0,  0,  0,  0    /* FORM_TERMINALS_PENDING */
    UNION ALL SELECT  6,  14,  56,  0,  0,  0,  0    /* FORM_MOBILE_BANK_MOVEMENTS */
    UNION ALL SELECT  6,  14,  57,  0,  0,  0,  0    /* FORM_PROMOTION_REPORT */
    UNION ALL SELECT  6,  14,  58,  0,  0,  0,  0    /* FORM_GIFTS_SEL */
    UNION ALL SELECT  6,  14,  59,  0,  0,  0,  0    /* FORM_GIFTS_EDIT */
    UNION ALL SELECT  6,  14,  60,  0,  0,  0,  0    /* FORM_TAXES_REPORT */
    UNION ALL SELECT  6,  14,  61,  0,  0,  0,  0    /* FORM_TERMINALS_EDIT */
    UNION ALL SELECT  6,  14,  62,  0,  0,  0,  0    /* FORM_SITE_JACKPOT_CONFIGURATION */
    UNION ALL SELECT  6,  14,  63,  0,  0,  0,  0    /* FORM_SITE_GAP_REPORT */
    UNION ALL SELECT  6,  14,  64,  0,  0,  0,  0    /* FORM_SITE_JACKPOT_HISTORY */
    UNION ALL SELECT  6,  14,  65,  0,  0,  0,  0    /* FORM_GIFTS_HISTORY */
    UNION ALL SELECT  6,  14,  66,  0,  0,  0,  0    /* FORM_TERMINALS_WITHOUT_ACTIVITY */
    UNION ALL SELECT  6,  14,  67,  0,  0,  0,  0    /* FORM_MAILING_PROGRAMMING_SEL */
    UNION ALL SELECT  6,  14,  68,  0,  0,  0,  0    /* FORM_MAILING_PROGRAMMING_EDIT */
    UNION ALL SELECT  6,  14,  69,  0,  0,  0,  0    /* FORM_MAILING_PARAMETERS */
    UNION ALL SELECT  6,  14,  147, 0,  0,  0,  0    /* FORM_GAME_METERS */
    		/* CASHIER */
    UNION ALL SELECT  6,  15,  0,   1,  1,  1,  1    /* FORM_MAIN */
    UNION ALL SELECT  6,  15,  1,   1,  1,  1,  1    /* FORM_VIRTUAL_ADD_NO_REDEEM */
    UNION ALL SELECT  6,  15,  2,   1,  1,  1,  1    /* FORM_VIRTUAL_CASH_DESK_OPEN_CLOSE */
    UNION ALL SELECT  6,  15,  3,   1,  1,  1,  1    /* FORM_VIRTUAL_CASH_DESK_DEPOSIT */
    UNION ALL SELECT  6,  15,  4,   1,  1,  1,  1    /* FORM_VIRTUAL_CASH_DESK_WITHDRAWN */
    UNION ALL SELECT  6,  15,  5,   1,  1,  1,  1    /* FORM_VIRTUAL_CARD_OPER_ADD_REDEEM_CREDIT */
    UNION ALL SELECT  6,  15,  6,   1,  1,  1,  1    /* FORM_VIRTUAL_CARD_OPER_LAST_MOV */
    UNION ALL SELECT  6,  15,  7,   1,  1,  1,  1    /* FORM_VIRTUAL_ACCOUNT_OPER_CARD */
    UNION ALL SELECT  6,  15,  8,   1,  1,  1,  1    /* FORM_VIRTUAL_ACCOUNT_OPER_EDIT */
    UNION ALL SELECT  6,  15,  9,   1,  1,  1,  1    /* FORM_VIRTUAL_ACCOUNT_OPER_LOCK */
    UNION ALL SELECT  6,  15,  10,  1,  1,  1,  1    /* FORM_VIRTUAL_PRINT_CARD */
    UNION ALL SELECT  6,  15,  11,  0,  0,  0,  0    /* FORM_VIRTUAL_OPTIONS_DB_CONFIG */
    UNION ALL SELECT  6,  15,  12,  1,  1,  1,  1    /* FORM_VIRTUAL_OPTIONS_LANGUAGE */
    UNION ALL SELECT  6,  15,  13,  1,  1,  1,  1    /* FORM_VIRTUAL_OPTIONS_CALIBRATE */
    UNION ALL SELECT  6,  15,  14,  1,  1,  1,  1    /* FORM_VIRTUAL_GAME_SESSIONS */
    UNION ALL SELECT  6,  15,  15,  1,  1,  1,  1    /* FORM_VIRTUAL_MOBILE_BANK */
    UNION ALL SELECT  6,  15,  16,  1,  1,  1,  1    /* FORM_VIRTUAL_CASH_DESK_ACCESS */
    UNION ALL SELECT  6,  15,  17,  1,  1,  1,  1    /* FORM_VIRTUAL_ACCOUNT_OPER_HANDPAY */
    UNION ALL SELECT  6,  15,  18,  1,  1,  1,  1    /* FORM_VIRTUAL_ACCOUNT_OPER_HANDPAY_CANCEL */
    UNION ALL SELECT  6,  15,  19,  1,  1,  1,  1    /* FORM_VIRTUAL_ACCOUNT_LOG_OFF */
    UNION ALL SELECT  6,  15,  20,  1,  1,  1,  1    /* FORM_VIRTUAL_MANUAL_OPER_HANDPAY */
    UNION ALL SELECT  6,  15,  21,  1,  1,  1,  1    /* FORM_VIRTUAL_MANUAL_OPER_HANDPAY_CANCEL */
    UNION ALL SELECT  6,  15,  22,  1,  1,  1,  1    /* FORM_VIRTUAL_GIFT_REQUEST */
    UNION ALL SELECT  6,  15,  23,  1,  1,  1,  1    /* FORM_VIRTUAL_GIFT_DELIVERY */
    UNION ALL SELECT  6,  15,  24,  1,  1,  1,  1    /* FORM_VIRTUAL_ACCOUNT_LEVEL */
    UNION ALL SELECT  6,  15,  25,  1,  1,  1,  1    /* FORM_VIRTUAL_PROMO_SPECIAL_PERMISSION_A */
    UNION ALL SELECT  6,  15,  26,  1,  1,  1,  1    /* FORM_VIRTUAL_PROMO_SPECIAL_PERMISSION_B */
    
GO
 
  INSERT INTO [dbo].[gui_user_profiles]
             ([gup_profile_id]
             ,[gup_name])
       VALUES
             ( 7
             , 'TÉCNICO DE SALA')
GO
  
        INSERT INTO [dbo].[gui_profile_forms]
                   ([gpf_profile_id], [gpf_gui_id], [gpf_form_id], [gpf_read_perm], [gpf_write_perm], [gpf_delete_perm], [gpf_execute_perm])
                           /*  FORM  R   W   D   X    */
             SELECT  7,  14,  1,   0,  0,  0,  0    /* FORM_USER_PROFILE_SEL */
   UNION ALL SELECT  7,  14,  2,   0,  0,  0,  0    /* FORM_USER_EDIT */
   UNION ALL SELECT  7,  14,  3,   0,  0,  0,  0    /* FORM_PROFILE_EDIT */
   UNION ALL SELECT  7,  14,  4,   0,  0,  0,  0    /* FORM_GUI_AUDITOR */
   UNION ALL SELECT  7,  14,  5,   0,  0,  0,  0    /* FORM_EVENTS_HISTORY */
   UNION ALL SELECT  7,  14,  6,   0,  0,  0,  0    /* FORM_TERMINALS_SELECTION */
   UNION ALL SELECT  7,  14,  7,   0,  0,  0,  0    /* FORM_STATISTICS_TERMINALS */
   UNION ALL SELECT  7,  14,  8,   0,  0,  0,  0    /* FORM_STATISTICS_GAMES */
   UNION ALL SELECT  7,  14,  9,   0,  0,  0,  0    /* FORM_STATISTICS_DATES */
   UNION ALL SELECT  7,  14,  10,  0,  0,  0,  0    /* FORM_STATISTICS_HOURS */
   UNION ALL SELECT  7,  14,  11,  0,  0,  0,  0    /* FORM_STATISTICS_CARDS */
   UNION ALL SELECT  7,  14,  12,  0,  0,  0,  0    /* FORM_PLAYS_SESSIONS */
   UNION ALL SELECT  7,  14,  13,  0,  0,  0,  0    /* FORM_PLAYS_PLAYS */
   UNION ALL SELECT  7,  14,  14,  0,  0,  0,  0    /* FORM_WS_SESSIONS */
   UNION ALL SELECT  7,  14,  15,  0,  0,  0,  0    /* FORM_STATISTICS_HOURS_GROUP */
   UNION ALL SELECT  7,  14,  16,  0,  0,  0,  0    /* FORM_ACCT_TAXES */
   UNION ALL SELECT  7,  14,  17,  0,  0,  0,  0    /* FORM_CASHIER_MOVS */
   UNION ALL SELECT  7,  14,  18,  0,  0,  0,  0    /* FORM_CASHIER_SESSIONS */
   UNION ALL SELECT  7,  14,  19,  0,  0,  0,  0    /* FORM_ACCOUNT_MOVEMENTS */
   UNION ALL SELECT  7,  14,  20,  0,  0,  0,  0    /* FORM_ACCOUNT_SUMMARY */
   UNION ALL SELECT  7,  14,  21,  0,  0,  0,  0    /* FORM_DATE_TERMINAL */
   UNION ALL SELECT  7,  14,  22,  0,  0,  0,  0    /* FORM_CLASS_II_DRAW_AUDIT */
   UNION ALL SELECT  7,  14,  24,  0,  0,  0,  0    /* FORM_CLASS_II_JACKPOT_CONFIGURATION */
   UNION ALL SELECT  7,  14,  25,  0,  0,  0,  0    /* FORM_CLASS_II_JACKPOT_HISTORY */
   UNION ALL SELECT  7,  14,  26,  0,  0,  0,  0    /* FORM_CLASS_II_JACKPOT_MONITOR */
   UNION ALL SELECT  7,  14,  27,  0,  0,  0,  0    /* FORM_SW_DOWNLOAD_VERSIONS */
   UNION ALL SELECT  7,  14,  28,  0,  0,  0,  0    /* FORM_SW_DOWNLOAD_DETAILS */
   UNION ALL SELECT  7,  14,  29,  0,  0,  0,  0    /* FORM_CARD_RECORD */
   UNION ALL SELECT  7,  14,  30,  0,  0,  0,  0    /* FORM_GENERAL_PARAMS */
   UNION ALL SELECT  7,  14,  31,  0,  0,  0,  0    /* FORM_PROVIDER_DAY_TERMINAL */
   UNION ALL SELECT  7,  14,  32,  0,  0,  0,  0    /* FORM_SERVICE_MONITOR */
   UNION ALL SELECT  7,  14,  34,  0,  0,  0,  0    /* FORM_TERMINALS_BY_BUILD */
   UNION ALL SELECT  7,  14,  35,  0,  0,  0,  0    /* FORM_CASHIER_SESSION_DETAIL */
   UNION ALL SELECT  7,  14,  36,  0,  0,  0,  0    /* FORM_CLASS_II_JACKPOT_PROMO_MESSAGE */
   UNION ALL SELECT  7,  14,  37,  0,  0,  0,  0    /* FORM_CASHIER_CONFIGURATION */
   UNION ALL SELECT  7,  14,  38,  0,  0,  0,  0    /* FORM_LICENCE_VERSIONS */
   UNION ALL SELECT  7,  14,  39,  0,  0,  0,  0    /* FORM_MISC_SW_BY_BUILD */
   UNION ALL SELECT  7,  14,  40,  0,  0,  0,  0    /* FORM_LICENCE_DETAILS */
   UNION ALL SELECT  7,  14,  45,  0,  0,  0,  0    /* FORM_TERMINAL_GAME_RELATION */
   UNION ALL SELECT  7,  14,  46,  0,  0,  0,  0    /* FORM_EXPIRED_CREDITS */
   UNION ALL SELECT  7,  14,  47,  0,  0,  0,  0    /* FORM_PROMOTIONS */
   UNION ALL SELECT  7,  14,  48,  0,  0,  0,  0    /* FORM_PROMOTION_EDIT */
   UNION ALL SELECT  7,  14,  49,  0,  0,  0,  0    /* FORM_TERMINALS_3GS_EDIT */
   UNION ALL SELECT  7,  14,  50,  0,  0,  0,  0    /* FORM_DRAWS */
   UNION ALL SELECT  7,  14,  51,  0,  0,  0,  0    /* FORM_DRAW_EDIT */
   UNION ALL SELECT  7,  14,  52,  0,  0,  0,  0    /* FORM_COMMANDS_SEND */
   UNION ALL SELECT  7,  14,  53,  0,  0,  0,  0    /* FORM_COMMANDS_HISTORY */
   UNION ALL SELECT  7,  14,  54,  0,  0,  0,  0    /* FORM_HANDPAYS */
   UNION ALL SELECT  7,  14,  55,  0,  0,  0,  0    /* FORM_TERMINALS_PENDING */
   UNION ALL SELECT  7,  14,  56,  0,  0,  0,  0    /* FORM_MOBILE_BANK_MOVEMENTS */
   UNION ALL SELECT  7,  14,  57,  0,  0,  0,  0    /* FORM_PROMOTION_REPORT */
   UNION ALL SELECT  7,  14,  58,  0,  0,  0,  0    /* FORM_GIFTS_SEL */
   UNION ALL SELECT  7,  14,  59,  0,  0,  0,  0    /* FORM_GIFTS_EDIT */
   UNION ALL SELECT  7,  14,  60,  0,  0,  0,  0    /* FORM_TAXES_REPORT */
   UNION ALL SELECT  7,  14,  61,  0,  0,  0,  0    /* FORM_TERMINALS_EDIT */
   UNION ALL SELECT  7,  14,  62,  0,  0,  0,  0    /* FORM_SITE_JACKPOT_CONFIGURATION */
   UNION ALL SELECT  7,  14,  63,  0,  0,  0,  0    /* FORM_SITE_GAP_REPORT */
   UNION ALL SELECT  7,  14,  64,  0,  0,  0,  0    /* FORM_SITE_JACKPOT_HISTORY */
   UNION ALL SELECT  7,  14,  65,  0,  0,  0,  0    /* FORM_GIFTS_HISTORY */
   UNION ALL SELECT  7,  14,  66,  1,  1,  0,  1    /* FORM_TERMINALS_WITHOUT_ACTIVITY */
   UNION ALL SELECT  7,  14,  67,  0,  0,  0,  0    /* FORM_MAILING_PROGRAMMING_SEL */
   UNION ALL SELECT  7,  14,  68,  0,  0,  0,  0    /* FORM_MAILING_PROGRAMMING_EDIT */
   UNION ALL SELECT  7,  14,  69,  0,  0,  0,  0    /* FORM_MAILING_PARAMETERS */
   UNION ALL SELECT  7,  14,  147, 0,  0,  0,  0    /* FORM_GAME_METERS */
   		/* CASHIER */
   UNION ALL SELECT  7,  15,  0,   0,  0,  0,  0    /* FORM_MAIN */
   UNION ALL SELECT  7,  15,  1,   0,  0,  0,  0    /* FORM_VIRTUAL_ADD_NO_REDEEM */
   UNION ALL SELECT  7,  15,  2,   0,  0,  0,  0    /* FORM_VIRTUAL_CASH_DESK_OPEN_CLOSE */
   UNION ALL SELECT  7,  15,  3,   0,  0,  0,  0    /* FORM_VIRTUAL_CASH_DESK_DEPOSIT */
   UNION ALL SELECT  7,  15,  4,   0,  0,  0,  0    /* FORM_VIRTUAL_CASH_DESK_WITHDRAWN */
   UNION ALL SELECT  7,  15,  5,   0,  0,  0,  0    /* FORM_VIRTUAL_CARD_OPER_ADD_REDEEM_CREDIT */
   UNION ALL SELECT  7,  15,  6,   0,  0,  0,  0    /* FORM_VIRTUAL_CARD_OPER_LAST_MOV */
   UNION ALL SELECT  7,  15,  7,   0,  0,  0,  0    /* FORM_VIRTUAL_ACCOUNT_OPER_CARD */
   UNION ALL SELECT  7,  15,  8,   0,  0,  0,  0    /* FORM_VIRTUAL_ACCOUNT_OPER_EDIT */
   UNION ALL SELECT  7,  15,  9,   0,  0,  0,  0    /* FORM_VIRTUAL_ACCOUNT_OPER_LOCK */
   UNION ALL SELECT  7,  15,  10,  0,  0,  0,  0    /* FORM_VIRTUAL_PRINT_CARD */
   UNION ALL SELECT  7,  15,  11,  0,  0,  0,  0    /* FORM_VIRTUAL_OPTIONS_DB_CONFIG */
   UNION ALL SELECT  7,  15,  12,  0,  0,  0,  0    /* FORM_VIRTUAL_OPTIONS_LANGUAGE */
   UNION ALL SELECT  7,  15,  13,  0,  0,  0,  0    /* FORM_VIRTUAL_OPTIONS_CALIBRATE */
   UNION ALL SELECT  7,  15,  14,  0,  0,  0,  0    /* FORM_VIRTUAL_GAME_SESSIONS */
   UNION ALL SELECT  7,  15,  15,  0,  0,  0,  0    /* FORM_VIRTUAL_MOBILE_BANK */
   UNION ALL SELECT  7,  15,  16,  0,  0,  0,  0    /* FORM_VIRTUAL_CASH_DESK_ACCESS */
   UNION ALL SELECT  7,  15,  17,  0,  0,  0,  0    /* FORM_VIRTUAL_ACCOUNT_OPER_HANDPAY */
   UNION ALL SELECT  7,  15,  18,  0,  0,  0,  0    /* FORM_VIRTUAL_ACCOUNT_OPER_HANDPAY_CANCEL */
   UNION ALL SELECT  7,  15,  19,  0,  0,  0,  0    /* FORM_VIRTUAL_ACCOUNT_LOG_OFF */
   UNION ALL SELECT  7,  15,  20,  0,  0,  0,  0    /* FORM_VIRTUAL_MANUAL_OPER_HANDPAY */
   UNION ALL SELECT  7,  15,  21,  0,  0,  0,  0    /* FORM_VIRTUAL_MANUAL_OPER_HANDPAY_CANCEL */
   UNION ALL SELECT  7,  15,  22,  0,  0,  0,  0    /* FORM_VIRTUAL_GIFT_REQUEST */
   UNION ALL SELECT  7,  15,  23,  0,  0,  0,  0    /* FORM_VIRTUAL_GIFT_DELIVERY */
   UNION ALL SELECT  7,  15,  24,  0,  0,  0,  0    /* FORM_VIRTUAL_ACCOUNT_LEVEL */
   UNION ALL SELECT  7,  15,  25,  0,  0,  0,  0    /* FORM_VIRTUAL_PROMO_SPECIAL_PERMISSION_A */
   UNION ALL SELECT  7,  15,  26,  0,  0,  0,  0    /* FORM_VIRTUAL_PROMO_SPECIAL_PERMISSION_B */
   
GO

  INSERT INTO [dbo].[gui_user_profiles]
             ([gup_profile_id]
             ,[gup_name])
       VALUES
             ( 8
             , 'TÉCNICO WIN')
GO
  
        INSERT INTO [dbo].[gui_profile_forms]
                   ([gpf_profile_id], [gpf_gui_id], [gpf_form_id], [gpf_read_perm], [gpf_write_perm], [gpf_delete_perm], [gpf_execute_perm])
                           /*  FORM  R   W   D   X    */
             SELECT 8,  14,  1,   1,  1,  0,  1    /* FORM_USER_PROFILE_SEL */
   UNION ALL SELECT 8,  14,  2,   1,  1,  0,  1    /* FORM_USER_EDIT */
   UNION ALL SELECT 8,  14,  3,   1,  1,  0,  1    /* FORM_PROFILE_EDIT */
   UNION ALL SELECT 8,  14,  4,   1,  1,  0,  1    /* FORM_GUI_AUDITOR */
   UNION ALL SELECT 8,  14,  5,   0,  0,  0,  0    /* FORM_EVENTS_HISTORY */
   UNION ALL SELECT 8,  14,  6,   1,  1,  0,  1    /* FORM_TERMINALS_SELECTION */
   UNION ALL SELECT 8,  14,  7,   0,  0,  0,  0    /* FORM_STATISTICS_TERMINALS */
   UNION ALL SELECT 8,  14,  8,   0,  0,  0,  0    /* FORM_STATISTICS_GAMES */
   UNION ALL SELECT 8,  14,  9,   0,  0,  0,  0    /* FORM_STATISTICS_DATES */
   UNION ALL SELECT 8,  14,  10,  0,  0,  0,  0    /* FORM_STATISTICS_HOURS */
   UNION ALL SELECT 8,  14,  11,  0,  0,  0,  0    /* FORM_STATISTICS_CARDS */
   UNION ALL SELECT 8,  14,  12,  0,  0,  0,  0    /* FORM_PLAYS_SESSIONS */
   UNION ALL SELECT 8,  14,  13,  0,  0,  0,  0    /* FORM_PLAYS_PLAYS */
   UNION ALL SELECT 8,  14,  14,  0,  0,  0,  0    /* FORM_WS_SESSIONS */
   UNION ALL SELECT 8,  14,  15,  0,  0,  0,  0    /* FORM_STATISTICS_HOURS_GROUP */
   UNION ALL SELECT 8,  14,  16,  0,  0,  0,  0    /* FORM_ACCT_TAXES */
   UNION ALL SELECT 8,  14,  17,  0,  0,  0,  0    /* FORM_CASHIER_MOVS */
   UNION ALL SELECT 8,  14,  18,  0,  0,  0,  0    /* FORM_CASHIER_SESSIONS */
   UNION ALL SELECT 8,  14,  19,  0,  0,  0,  0    /* FORM_ACCOUNT_MOVEMENTS */
   UNION ALL SELECT 8,  14,  20,  0,  0,  0,  0    /* FORM_ACCOUNT_SUMMARY */
   UNION ALL SELECT 8,  14,  21,  0,  0,  0,  0    /* FORM_DATE_TERMINAL */
   UNION ALL SELECT 8,  14,  22,  0,  0,  0,  0    /* FORM_CLASS_II_DRAW_AUDIT */
   UNION ALL SELECT 8,  14,  24,  0,  0,  0,  0    /* FORM_CLASS_II_JACKPOT_CONFIGURATION */
   UNION ALL SELECT 8,  14,  25,  0,  0,  0,  0    /* FORM_CLASS_II_JACKPOT_HISTORY */
   UNION ALL SELECT 8,  14,  26,  0,  0,  0,  0    /* FORM_CLASS_II_JACKPOT_MONITOR */
   UNION ALL SELECT 8,  14,  27,  1,  0,  0,  0    /* FORM_SW_DOWNLOAD_VERSIONS */
   UNION ALL SELECT 8,  14,  28,  0,  0,  0,  0    /* FORM_SW_DOWNLOAD_DETAILS */
   UNION ALL SELECT 8,  14,  29,  0,  0,  0,  0    /* FORM_CARD_RECORD */
   UNION ALL SELECT 8,  14,  30,  0,  0,  0,  0    /* FORM_GENERAL_PARAMS */
   UNION ALL SELECT 8,  14,  31,  0,  0,  0,  0    /* FORM_PROVIDER_DAY_TERMINAL */
   UNION ALL SELECT 8,  14,  32,  1,  1,  0,  1    /* FORM_SERVICE_MONITOR */
   UNION ALL SELECT 8,  14,  34,  1,  1,  0,  1    /* FORM_TERMINALS_BY_BUILD */
   UNION ALL SELECT 8,  14,  35,  0,  0,  0,  0    /* FORM_CASHIER_SESSION_DETAIL */
   UNION ALL SELECT 8,  14,  36,  0,  0,  0,  0    /* FORM_CLASS_II_JACKPOT_PROMO_MESSAGE */
   UNION ALL SELECT 8,  14,  37,  0,  0,  0,  0    /* FORM_CASHIER_CONFIGURATION */
   UNION ALL SELECT 8,  14,  38,  1,  1,  0,  1    /* FORM_LICENCE_VERSIONS */
   UNION ALL SELECT 8,  14,  39,  1,  1,  0,  1    /* FORM_MISC_SW_BY_BUILD */
   UNION ALL SELECT 8,  14,  40,  1,  1,  0,  1    /* FORM_LICENCE_DETAILS */
   UNION ALL SELECT 8,  14,  45,  1,  1,  0,  1    /* FORM_TERMINAL_GAME_RELATION */
   UNION ALL SELECT 8,  14,  46,  0,  0,  0,  0    /* FORM_EXPIRED_CREDITS */
   UNION ALL SELECT 8,  14,  47,  0,  0,  0,  0    /* FORM_PROMOTIONS */
   UNION ALL SELECT 8,  14,  48,  0,  0,  0,  0    /* FORM_PROMOTION_EDIT */
   UNION ALL SELECT 8,  14,  49,  1,  1,  0,  1    /* FORM_TERMINALS_3GS_EDIT */
   UNION ALL SELECT 8,  14,  50,  0,  0,  0,  0    /* FORM_DRAWS */
   UNION ALL SELECT 8,  14,  51,  0,  0,  0,  0    /* FORM_DRAW_EDIT */
   UNION ALL SELECT 8,  14,  52,  0,  0,  0,  0    /* FORM_COMMANDS_SEND */
   UNION ALL SELECT 8,  14,  53,  0,  0,  0,  0    /* FORM_COMMANDS_HISTORY */
   UNION ALL SELECT 8,  14,  54,  0,  0,  0,  0    /* FORM_HANDPAYS */
   UNION ALL SELECT 8,  14,  55,  1,  1,  0,  1    /* FORM_TERMINALS_PENDING */
   UNION ALL SELECT 8,  14,  56,  0,  0,  0,  0    /* FORM_MOBILE_BANK_MOVEMENTS */
   UNION ALL SELECT 8,  14,  57,  0,  0,  0,  0    /* FORM_PROMOTION_REPORT */
   UNION ALL SELECT 8,  14,  58,  0,  0,  0,  0    /* FORM_GIFTS_SEL */
   UNION ALL SELECT 8,  14,  59,  0,  0,  0,  0    /* FORM_GIFTS_EDIT */
   UNION ALL SELECT 8,  14,  60,  0,  0,  0,  0    /* FORM_TAXES_REPORT */
   UNION ALL SELECT 8,  14,  61,  1,  1,  0,  1    /* FORM_TERMINALS_EDIT */
   UNION ALL SELECT 8,  14,  62,  0,  0,  0,  0    /* FORM_SITE_JACKPOT_CONFIGURATION */
   UNION ALL SELECT 8,  14,  63,  0,  0,  0,  0    /* FORM_SITE_GAP_REPORT */
   UNION ALL SELECT 8,  14,  64,  0,  0,  0,  0    /* FORM_SITE_JACKPOT_HISTORY */
   UNION ALL SELECT 8,  14,  65,  0,  0,  0,  0    /* FORM_GIFTS_HISTORY */
   UNION ALL SELECT 8,  14,  66,  1,  1,  0,  1    /* FORM_TERMINALS_WITHOUT_ACTIVITY */
   UNION ALL SELECT 8,  14,  67,  0,  0,  0,  0    /* FORM_MAILING_PROGRAMMING_SEL */
   UNION ALL SELECT 8,  14,  68,  0,  0,  0,  0    /* FORM_MAILING_PROGRAMMING_EDIT */
   UNION ALL SELECT 8,  14,  69,  0,  0,  0,  0    /* FORM_MAILING_PARAMETERS */
   UNION ALL SELECT 8,  14,  147, 0,  0,  0,  0    /* FORM_GAME_METERS */
   		/* CASHIER */
   UNION ALL SELECT 8,  15,  0,   0,  0,  0,  0    /* FORM_MAIN */
   UNION ALL SELECT 8,  15,  1,   0,  0,  0,  0    /* FORM_VIRTUAL_ADD_NO_REDEEM */
   UNION ALL SELECT 8,  15,  2,   0,  0,  0,  0    /* FORM_VIRTUAL_CASH_DESK_OPEN_CLOSE */
   UNION ALL SELECT 8,  15,  3,   0,  0,  0,  0    /* FORM_VIRTUAL_CASH_DESK_DEPOSIT */
   UNION ALL SELECT 8,  15,  4,   0,  0,  0,  0    /* FORM_VIRTUAL_CASH_DESK_WITHDRAWN */
   UNION ALL SELECT 8,  15,  5,   0,  0,  0,  0    /* FORM_VIRTUAL_CARD_OPER_ADD_REDEEM_CREDIT */
   UNION ALL SELECT 8,  15,  6,   0,  0,  0,  0    /* FORM_VIRTUAL_CARD_OPER_LAST_MOV */
   UNION ALL SELECT 8,  15,  7,   0,  0,  0,  0    /* FORM_VIRTUAL_ACCOUNT_OPER_CARD */
   UNION ALL SELECT 8,  15,  8,   0,  0,  0,  0    /* FORM_VIRTUAL_ACCOUNT_OPER_EDIT */
   UNION ALL SELECT 8,  15,  9,   0,  0,  0,  0    /* FORM_VIRTUAL_ACCOUNT_OPER_LOCK */
   UNION ALL SELECT 8,  15,  10,  0,  0,  0,  0    /* FORM_VIRTUAL_PRINT_CARD */
   UNION ALL SELECT 8,  15,  11,  0,  0,  0,  0    /* FORM_VIRTUAL_OPTIONS_DB_CONFIG */
   UNION ALL SELECT 8,  15,  12,  0,  0,  0,  0    /* FORM_VIRTUAL_OPTIONS_LANGUAGE */
   UNION ALL SELECT 8,  15,  13,  0,  0,  0,  0    /* FORM_VIRTUAL_OPTIONS_CALIBRATE */
   UNION ALL SELECT 8,  15,  14,  0,  0,  0,  0    /* FORM_VIRTUAL_GAME_SESSIONS */
   UNION ALL SELECT 8,  15,  15,  0,  0,  0,  0    /* FORM_VIRTUAL_MOBILE_BANK */
   UNION ALL SELECT 8,  15,  16,  0,  0,  0,  0    /* FORM_VIRTUAL_CASH_DESK_ACCESS */
   UNION ALL SELECT 8,  15,  17,  0,  0,  0,  0    /* FORM_VIRTUAL_ACCOUNT_OPER_HANDPAY */
   UNION ALL SELECT 8,  15,  18,  0,  0,  0,  0    /* FORM_VIRTUAL_ACCOUNT_OPER_HANDPAY_CANCEL */
   UNION ALL SELECT 8,  15,  19,  0,  0,  0,  0    /* FORM_VIRTUAL_ACCOUNT_LOG_OFF */
   UNION ALL SELECT 8,  15,  20,  0,  0,  0,  0    /* FORM_VIRTUAL_MANUAL_OPER_HANDPAY */
   UNION ALL SELECT 8,  15,  21,  0,  0,  0,  0    /* FORM_VIRTUAL_MANUAL_OPER_HANDPAY_CANCEL */
   UNION ALL SELECT 8,  15,  22,  0,  0,  0,  0    /* FORM_VIRTUAL_GIFT_REQUEST */
   UNION ALL SELECT 8,  15,  23,  0,  0,  0,  0    /* FORM_VIRTUAL_GIFT_DELIVERY */
   UNION ALL SELECT 8,  15,  24,  0,  0,  0,  0    /* FORM_VIRTUAL_ACCOUNT_LEVEL */
   UNION ALL SELECT 8,  15,  25,  0,  0,  0,  0    /* FORM_VIRTUAL_PROMO_SPECIAL_PERMISSION_A */
   UNION ALL SELECT 8,  15,  26,  0,  0,  0,  0    /* FORM_VIRTUAL_PROMO_SPECIAL_PERMISSION_B */
   
GO

/*** Default Server ***/
INSERT INTO [dbo].[terminals]
           ([te_type]
           ,[te_name]
           ,[te_external_id]
           ,[te_blocked])
     VALUES
           (0
           ,'DEFAULT_SERVER'
           ,'DEFAULT_SERVER'
           ,0)
GO
/*** PAGOS SALA ***/
INSERT INTO [dbo].[terminals]
           ([te_type]
           ,[te_name]
           ,[te_external_id]
           ,[te_blocked]
           ,[te_active]
           ,[te_provider_id]
           ,[te_terminal_type]
           ,[te_status])
     VALUES (1
            ,'PAGOS SALA'
            ,'PAGOS SALA'
            ,0
            ,1
            ,'CASINO'
            ,100
            ,0)
GO            
/*** Terminal Types ***/
INSERT INTO   dbo.terminal_types (tt_type, tt_name)
     VALUES ( -1, 'Not set' );
GO
INSERT INTO   dbo.terminal_types (tt_type, tt_name)
     VALUES (  1, 'LKT WIN' );
GO
INSERT INTO   dbo.terminal_types (tt_type, tt_name)
     VALUES (  5, 'LKT SAS Host' );
GO
INSERT INTO   dbo.terminal_types (tt_type, tt_name)
     VALUES ( 100 ,'SITE' );
GO
INSERT INTO   dbo.terminal_types (tt_type, tt_name)
     VALUES ( 101 ,'SITE JACKPOT' );
GO
INSERT INTO   dbo.terminal_types (tt_type, tt_name)
     VALUES ( 102 ,'MOBILE BANK' );
GO
/*** General Parameters ***/
INSERT INTO [dbo].[general_params] 
                    ([gp_group_key],        [gp_subject_key],                    [gp_key_value])
/* Cashier */
              SELECT 'Cashier',             'CardPrice',                         '5'
    UNION ALL SELECT 'Cashier',             'CashierMode',                       'PerUser'
    UNION ALL SELECT 'Cashier',             'Language',                          'es'
    UNION ALL SELECT 'Cashier',             'PersonalCardPrice',                 '3'
    UNION ALL SELECT 'Cashier',             'PersonalCardReplacementPrice',      '3'
    UNION ALL SELECT 'Cashier',             'CardRefundable',                    '1'
    UNION ALL SELECT 'Cashier',             'CreditsExpireAfterDays',            '21'
    UNION ALL SELECT 'Cashier',             'LegalAge',                          '18'
    UNION ALL SELECT 'Cashier',	            'MaxAllowedAccountBalance',	         '100000' 
    UNION ALL SELECT 'Cashier',             'MinTimeToCloseSession',             '300'
    UNION ALL SELECT 'Cashier',             'TrackDataVisibleChars',             '6'   
    UNION ALL SELECT 'Cashier',             'Split.A.CashInPct',		 '100'
    UNION ALL SELECT 'Cashier',             'Split.A.CompanyName',		 ''    
    UNION ALL SELECT 'Cashier',             'Split.A.DevolutionPct',		 '100'    
    UNION ALL SELECT 'Cashier',             'Split.A.Name',			 ''    
    UNION ALL SELECT 'Cashier',             'Split.A.Tax.Name',			 ''    
    UNION ALL SELECT 'Cashier',             'Split.A.Tax.Pct',			 '0'    
    UNION ALL SELECT 'Cashier',             'Split.B.CashInPct',		 '0'    
    UNION ALL SELECT 'Cashier',             'Split.B.CompanyName',		 ''    
    UNION ALL SELECT 'Cashier',             'Split.B.DevolutionPct',		 '0'    
    UNION ALL SELECT 'Cashier',             'Split.B.Enabled',			 '0'    
    UNION ALL SELECT 'Cashier',             'Split.B.Name',			 ''    
    UNION ALL SELECT 'Cashier',             'Split.B.Tax.Name',			 ''    
    UNION ALL SELECT 'Cashier',             'Split.B.Tax.Pct',			 '0'    
    UNION ALL SELECT 'Cashier',             'SplitAsWon',			 '0'    
    UNION ALL SELECT 'Cashier',             'Tax.OnPrize.1.Name',		 'Impuesto Federal'    
    UNION ALL SELECT 'Cashier',             'Tax.OnPrize.1.Pct',		 '1'        
    UNION ALL SELECT 'Cashier',             'Tax.OnPrize.1.Threshold',		 '0'        
    UNION ALL SELECT 'Cashier',             'Tax.OnPrize.2.Name',		 'Impuesto Estatal'    
    UNION ALL SELECT 'Cashier',             'Tax.OnPrize.2.Pct',		 '2'        
    UNION ALL SELECT 'Cashier',             'Tax.OnPrize.2.Threshold',		 '0'
    UNION ALL SELECT 'Cashier',             'MaxAllowedCashIn',                  '50000'
    UNION ALL SELECT 'Cashier',             'InitialCashIn.LastHours',           '0'
    UNION ALL SELECT 'Cashier',             'MaxAllowedDeposit',                 '500000'
    UNION ALL SELECT 'Cashier',             'HidePromotion',                     '0' 
    UNION ALL SELECT 'Cashier',             'InitialCashIn.LimitToOpeningTime',  '1'
    UNION ALL SELECT 'Cashier',             'AllowedAnonymous',                  '1'
    UNION ALL SELECT 'Cashier',             'KeepEnabledButtonsAfterOperation',  '0'
/* Cashier.AddAmount */
    UNION ALL SELECT 'Cashier.AddAmount',   'CustomButton1',                     '50'
    UNION ALL SELECT 'Cashier.AddAmount',   'CustomButton2',                     '100'
    UNION ALL SELECT 'Cashier.AddAmount',   'CustomButton3',                     '200'
    UNION ALL SELECT 'Cashier.AddAmount',   'CustomButton4',                     '300'
/* Cashier.CardPrint */    		    
    UNION ALL SELECT 'Cashier.CardPrint',   'PrintNameArea_TopX',                '0'
    UNION ALL SELECT 'Cashier.CardPrint',   'PrintNameArea_TopY',                '0'
    UNION ALL SELECT 'Cashier.CardPrint',   'PrintNameArea_BottomX',             '0'
    UNION ALL SELECT 'Cashier.CardPrint',   'PrintNameArea_BottomY',             '0'
    UNION ALL SELECT 'Cashier.CardPrint',   'PrintNameArea_FontSize',            '10'
    UNION ALL SELECT 'Cashier.CardPrint',   'PrintNameArea_Bold',                '1'
    UNION ALL SELECT 'Cashier.CardPrint',   'QueueName',                         'Magicard Pronto'    
/* Cashier.DrawTicket */            
    UNION ALL SELECT 'Cashier.DrawTicket',  'MaxNumbersPerTicket',               '300'
    UNION ALL SELECT 'Cashier.DrawTicket',  'SalesIntervalInHours',              '24'
    UNION ALL SELECT 'Cashier.DrawTicket',  'AutomaticPrintAfterCashIn',         '0'
    UNION ALL SELECT 'Cashier.DrawTicket',  'AutomaticPrintAfterCashOut',        '0'
/* Cashier.HandPays */                
    UNION ALL SELECT 'Cashier.HandPays',    'TimePeriod',                        '60'
    UNION ALL SELECT 'Cashier.HandPays',    'TimeToCancel',                      '5'
/* Cashier.Voucher */                    
    UNION ALL SELECT 'Cashier.Voucher',     'Footer',                            'DB Footer Line1\nDB Footer Line2'
    UNION ALL SELECT 'Cashier.Voucher',     'Header',                            'DB Header Line1\nDB Header Line2\n'
    UNION ALL SELECT 'Cashier.Voucher',     'Print',                             '1'
    UNION ALL SELECT 'Cashier.Voucher',     'EstimatedPrintTime',                '1500'
    UNION ALL SELECT 'Cashier.Voucher',     'Voucher.A.Footer',                  ''
    UNION ALL SELECT 'Cashier.Voucher',     'Voucher.A.Footer.Cancel',           ''
    UNION ALL SELECT 'Cashier.Voucher',     'Voucher.A.Footer.CashIn',           ''
    UNION ALL SELECT 'Cashier.Voucher',     'Voucher.A.Footer.Dev',              ''
    UNION ALL SELECT 'Cashier.Voucher',     'Voucher.A.Header',                  ''
    UNION ALL SELECT 'Cashier.Voucher',     'Voucher.B.Footer',       		 ''
    UNION ALL SELECT 'Cashier.Voucher',     'Voucher.B.Footer.Cancel',		 ''
    UNION ALL SELECT 'Cashier.Voucher',     'Voucher.B.Footer.CashIn', 		 ''
    UNION ALL SELECT 'Cashier.Voucher',     'Voucher.B.Footer.Dev',   		 ''
    UNION ALL SELECT 'Cashier.Voucher',     'Voucher.B.Header',       		 ''    
    UNION ALL SELECT 'Cashier.Voucher',     'ReprintText',                       ''    
    UNION ALL SELECT 'Cashier.Voucher',     'TotalInLetters',                    '0'
    UNION ALL SELECT 'Cashier.Voucher',     'PrintCopy',                         '0'
/* Cashless */
    UNION ALL SELECT 'Cashless',            'Server',                            'WIN'
/* Class II */    
    UNION ALL SELECT 'Class II',            'Severity',                          '0'
/* Interface3GS */
    UNION ALL SELECT 'Interface3GS',        'AuditEnabled',                      '0'
    UNION ALL SELECT 'Interface3GS',        'IgnoreMachineNumber',               '0'
    UNION ALL SELECT 'Interface3GS',        'IgnoreSessionID',                   '0'   
/* Mailing */     
    UNION ALL SELECT 'Mailing',             'Enabled',      			 '0'
    UNION ALL SELECT 'Mailing',             'SMTP.Enabled',      		 '0'
    UNION ALL SELECT 'Mailing',             'SMTP.Connection.ServerAddress',	 ''
    UNION ALL SELECT 'Mailing',             'SMTP.Connection.ServerPort',	 ''
    UNION ALL SELECT 'Mailing',             'SMTP.Connection.Secured',	 	 ''
    UNION ALL SELECT 'Mailing',             'SMTP.Credentials.UserName',	 ''
    UNION ALL SELECT 'Mailing',             'SMTP.Credentials.Password',	 ''
    UNION ALL SELECT 'Mailing',             'SMTP.Credentials.Domain',	 	 ''
    UNION ALL SELECT 'Mailing',             'SMTP.Credentials.EMailAddress',	 ''
    UNION ALL SELECT 'Mailing',             'AuthorizedServerList',              ''
/* MobileBank */ 
    UNION ALL SELECT 'MobileBank',          'AutomaticallyAssignPromotion',      '0'
/* Play */    
    UNION ALL SELECT 'Play',                'MinimumPlayers',                    '0'
    UNION ALL SELECT 'Play',                'ActivityTimeout',                   '10000'
    UNION ALL SELECT 'Play',                'ActivityDueToCardIn',               '0'
    UNION ALL SELECT 'Play',                'WaitForMinimumPlayers',             '1000'
    UNION ALL SELECT 'Play',                'LockTimeOut',                       '10'
    UNION ALL SELECT 'Play',                'StandAlone',                        '1'
    UNION ALL SELECT 'Play',                'IgnoreSasClientStatistics',         '0'
/* PlayerTracking */
    UNION ALL SELECT 'PlayerTracking',      'PointsExpirationDays',              '30'
    UNION ALL SELECT 'PlayerTracking',      'GiftExpirationDays',                '30'
    UNION ALL SELECT 'PlayerTracking',      'Level02.DaysOnLevel',               '90'
    UNION ALL SELECT 'PlayerTracking',      'Level03.DaysOnLevel',               '90'
    UNION ALL SELECT 'PlayerTracking',      'Level04.DaysOnLevel',               '90'    
    UNION ALL SELECT 'PlayerTracking',      'Level01.Name',                      'Plata'
    UNION ALL SELECT 'PlayerTracking',      'Level02.Name',                      'Oro'
    UNION ALL SELECT 'PlayerTracking',      'Level03.Name',                      'Platino'
    UNION ALL SELECT 'PlayerTracking',      'Level04.Name',                      'Win'
    UNION ALL SELECT 'PlayerTracking',      'Level02.PointsToEnter',             '1000'
    UNION ALL SELECT 'PlayerTracking',      'Level03.PointsToEnter',             '2000'
    UNION ALL SELECT 'PlayerTracking',      'Level04.PointsToEnter',             '3000'
    UNION ALL SELECT 'PlayerTracking',      'Level01.RedeemablePlayedTo1Point',  '0'
    UNION ALL SELECT 'PlayerTracking',      'Level02.RedeemablePlayedTo1Point',  '0'
    UNION ALL SELECT 'PlayerTracking',      'Level03.RedeemablePlayedTo1Point',  '0'
    UNION ALL SELECT 'PlayerTracking',      'Level04.RedeemablePlayedTo1Point',  '0'
    UNION ALL SELECT 'PlayerTracking',      'Level01.RedeemableSpentTo1Point', 	 '0'
    UNION ALL SELECT 'PlayerTracking',      'Level02.RedeemableSpentTo1Point', 	 '0'
    UNION ALL SELECT 'PlayerTracking',      'Level03.RedeemableSpentTo1Point', 	 '0'
    UNION ALL SELECT 'PlayerTracking',      'Level04.RedeemableSpentTo1Point',   '0'
    UNION ALL SELECT 'PlayerTracking',      'Level01.TotalPlayedTo1Point',       '0'
    UNION ALL SELECT 'PlayerTracking',      'Level02.TotalPlayedTo1Point',       '0'
    UNION ALL SELECT 'PlayerTracking',      'Level03.TotalPlayedTo1Point',       '0'
    UNION ALL SELECT 'PlayerTracking',      'Level04.TotalPlayedTo1Point',       '0'
    UNION ALL SELECT 'PlayerTracking',      'Levels.DaysCountingPoints',         '30'
/* Site */        
    UNION ALL SELECT 'Site',                'Identifier',                        '0'
    UNION ALL SELECT 'Site',                'Name',                              'SITE NAME'
    UNION ALL SELECT 'Site',                'DisableNewSessions',                '0'    
/* Software.Download */            
    UNION ALL SELECT                        'Software.Download',                 'Location1', ''
    UNION ALL SELECT                        'Software.Download',                 'Location2', ''
/* WCP */                
    UNION ALL SELECT 'WCP',                 'ProvisioningManual',                '1'
/* WigosGUI */                    
    UNION ALL SELECT 'WigosGUI',            'Language',                          'es'
    UNION ALL SELECT 'WigosGUI',            'ClosingTime',                       '8'
    UNION ALL SELECT 'WigosGUI',            'ClosingTimeMinutes',                '0'    
GO    

/*** Jackpot configuration ***/
INSERT INTO [dbo].[c2_jackpot_parameters]
           ([c2jp_enabled]
           ,[c2jp_contribution_pct]
           ,[c2jp_awarding_days]
           ,[c2jp_awarding_start]
           ,[c2jp_awarding_end]
           ,[c2jp_compensation_pct])
     VALUES
           (0
           ,0
           ,127
           ,0
           ,86399
           ,0)
GO
INSERT INTO [dbo].[c2_jackpot_instances]
           ([c2ji_index]
           ,[c2ji_name]
           ,[c2ji_contribution_pct]
           ,[c2ji_minimum_bet]
           ,[c2ji_minimum]
           ,[c2ji_maximum]
           ,[c2ji_average])
     VALUES
           (1
           ,'GOLD'
           ,50
           ,0
           ,0
           ,0
           ,0)
GO
INSERT INTO [dbo].[c2_jackpot_instances]
           ([c2ji_index]
           ,[c2ji_name]
           ,[c2ji_contribution_pct]
           ,[c2ji_minimum_bet]
           ,[c2ji_minimum]
           ,[c2ji_maximum]
           ,[c2ji_average])
     VALUES
           (2
           ,'SILVER'
           ,30
           ,0
           ,0
           ,0
           ,0)
GO
INSERT INTO [dbo].[c2_jackpot_instances]
           ([c2ji_index]
           ,[c2ji_name]
           ,[c2ji_contribution_pct]
           ,[c2ji_minimum_bet]
           ,[c2ji_minimum]
           ,[c2ji_maximum]
           ,[c2ji_average])
     VALUES
           (3
           ,'BRONZE'
           ,20
           ,0
           ,0
           ,0
           ,0)
GO
INSERT INTO [dbo].[c2_jackpot_counters]
           ([c2jc_index]
           ,[c2jc_accumulated]
           ,[c2jc_to_compensate])
     VALUES
           (1
           ,0
           ,0)
GO           
INSERT INTO [dbo].[c2_jackpot_counters]
           ([c2jc_index]
           ,[c2jc_accumulated]
           ,[c2jc_to_compensate])
     VALUES
           (2
           ,0
           ,0)
GO           
INSERT INTO [dbo].[c2_jackpot_counters]
           ([c2jc_index]
           ,[c2jc_accumulated]
           ,[c2jc_to_compensate])
     VALUES
           (3
           ,0
           ,0)
GO           
INSERT INTO [dbo].[alesis_parameters]
           ([ap_sql_server_ip_address]
           ,[ap_sql_database_name]
           ,[ap_sql_user]
           ,[ap_sql_user_password]
           ,[ap_vendor_id])
     VALUES
           ( NULL
           , NULL
           , NULL
           , NULL
           , NULL)
GO
INSERT INTO [dbo].[cj_parameters]
           ([cjp_local_ip_1]
           ,[cjp_remote_ip_1]
           ,[cjp_vendor_id_1]
           ,[cjp_local_ip_2]
           ,[cjp_remote_ip_2]
           ,[cjp_vendor_id_2])
     VALUES
           ( NULL
           , NULL
           , NULL
           , NULL
           , NULL
           , NULL)
GO
/*** Player Tracking configuration ***/
INSERT INTO [dbo].[promotions]
           ([pm_name]
           ,[pm_enabled]
           ,[pm_type]
           ,[pm_date_start]
           ,[pm_date_finish]
           ,[pm_schedule_weekday]
           ,[pm_schedule1_time_from]
           ,[pm_schedule1_time_to]
           ,[pm_schedule2_enabled]
           ,[pm_schedule2_time_from]
           ,[pm_schedule2_time_to]
           ,[pm_gender_filter]
           ,[pm_birthday_filter]
           ,[pm_expiration_type]
           ,[pm_expiration_value]
           ,[pm_min_cash_in]
           ,[pm_min_cash_in_reward]
           ,[pm_cash_in]
           ,[pm_cash_in_reward]
           ,[pm_num_tokens]
           ,[pm_token_name]
           ,[pm_token_reward])
     VALUES
           ('Programa de Puntos'
           ,1
           ,1
           ,CAST('01-01-2010 00:00:00' as DATETIME)
           ,CAST('01-01-2100 00:00:00' as DATETIME)
           ,127
           ,0
           ,0
           ,0
           ,0
           ,0
           ,0
           ,0
           ,1
           ,1
           ,0
           ,0
           ,0
           ,0
           ,0
           ,''
           ,0)
GO
/*** Site_Jackpot_Parameters ***/
INSERT INTO [dbo].[site_jackpot_parameters]
           ([sjp_enabled]
           ,[sjp_contribution_pct]
           ,[sjp_awarding_days]
           ,[sjp_awarding_start]
           ,[sjp_awarding_end]
           ,[sjp_awarding_min_occupation_pct]
           ,[sjp_awarding_exclude_promotions]
           ,[sjp_awarding_exclude_anonymous]
           ,[sjp_animation_interval]
           ,[sjp_recent_interval]
           ,[sjp_promo_message1]
           ,[sjp_promo_message2]
           ,[sjp_promo_message3]
           ,[sjp_to_compensate]
           ,[sjp_only_redeemable]
           ,[sjp_average_interval_hours]
           ,[sjp_played]
           ,[sjp_animation_interval2])
     VALUES
           (0,
            0.0,
            0,
            0,
            0,
            0.0,
            1,
            1,
            15,
            3600,
            NULL,
            NULL,
            NULL,
            0.0,
            1,
            0,
            0.0,
            15);           
GO
/* Site_Jackpot_Instances */
INSERT INTO [dbo].[site_jackpot_instances]
           ([sji_index]
           ,[sji_name]
           ,[sji_contribution_pct]
           ,[sji_minimum]
           ,[sji_maximum]
           ,[sji_average]
           ,[sji_accumulated]
           ,[sji_num_pending])
     VALUES
           (1,
            'HIGH',
            0.0,
            0.0,
            0.0,
            0.0,
            0.0,
            0);
GO
INSERT INTO [dbo].[site_jackpot_instances]
           ([sji_index]
           ,[sji_name]
           ,[sji_contribution_pct]
           ,[sji_minimum]
           ,[sji_maximum]
           ,[sji_average]
           ,[sji_accumulated]
           ,[sji_num_pending])
     VALUES
           (2,
            'MEDIUM',
            0.0,
            0.0,
            0.0,
            0.0,
            0.0,
            0);
GO            
INSERT INTO [dbo].[site_jackpot_instances]
           ([sji_index]
           ,[sji_name]
           ,[sji_contribution_pct]
           ,[sji_minimum]
           ,[sji_maximum]
           ,[sji_average]
           ,[sji_accumulated]
           ,[sji_num_pending])
     VALUES
           (3,
            'LOW',
            0.0,
            0.0,
            0.0,
            0.0,
            0.0,
            0);
GO
/**** DECLARATION SECTION *****/
DECLARE 
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000),

@New_I3GS_ReleaseId int,
@New_I3GS_ScriptName nvarchar(50),
@New_I3GS_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @New_ReleaseId = 26;
SET @New_ScriptName = N'Create_18.026.sql';
SET @New_Description = N'Vouchers figures in letter.';

SET @New_I3GS_ReleaseId = 1;
SET @New_I3GS_ScriptName = @New_ScriptName;
SET @New_I3GS_Description = N'Initial.';

/*** DB Version ***/
INSERT INTO [dbo].[db_version]
           ([db_client_id]
           ,[db_common_build_id]
           ,[db_client_build_id]
           ,[db_release_id]
           ,[db_updated_script]
           ,[db_updated]
           ,[db_description])
     VALUES
           (18
           ,100
           ,1
           ,@New_ReleaseId
           ,@New_ScriptName
           ,Getdate()
           ,@New_Description);
           
INSERT INTO [dbo].[db_version_interface_3gs]
           ([db_client_id]
           ,[db_common_build_id]
           ,[db_client_build_id]
           ,[db_release_id]
           ,[db_updated_script]
           ,[db_updated]
           ,[db_description])
     VALUES
           (18
           ,100
           ,1
           ,@New_I3GS_ReleaseId
           ,@New_I3GS_ScriptName
           ,Getdate()
           ,@New_I3GS_Description);
GO