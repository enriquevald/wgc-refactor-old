USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 1;

SET @New_ReleaseId = 6;
SET @New_ScriptName = N'Interface3gs_18.024.006.sql';
SET @New_Description = N'Updated status output parameters from nvarchar to varchar.';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version_interface_3gs WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB. Update not completed. '
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** UPDATE VERSION SECTION *****/
UPDATE db_version_interface_3gs
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;

SELECT 'DB_Version_Interface_3gs Update OK.';
   
--------------------------------------------------------------------------------
-- Copyright � 2010 Win Systems International
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: Misc.sql
-- 
--   DESCRIPTION: Misc procedure functions
-- 
--        AUTHOR: Miquel Beltran
-- 
-- CREATION DATE: 28-APR-2010
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 28-APR-2010 MBF    First release.
--------------------------------------------------------------------------------

USE [wgdb_000]
GO

--------------------------------------------------------------------------------
-- PURPOSE: Return the internal terminal ID
-- 
--  PARAMS:
--      - INPUT:
--        @VendorId 	   varchar(16)
--        @SerialNumber    varchar(30)
--        @MachineNumber   int
--      - OUTPUT:
--
-- RETURNS:
--      Internal Terminal ID, 0 if error
--   NOTES:
--

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetTerminalID]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[GetTerminalID]
GO
CREATE FUNCTION dbo.GetTerminalID
 (@VendorId 	   varchar(16),
  @SerialNumber    varchar(30),
  @MachineNumber   int) 
RETURNS INT
AS
BEGIN
  DECLARE @terminal_id int
  DECLARE @ignore_machine_number int
  
  SET @terminal_id = 0
  
	SELECT @ignore_machine_number = CAST(GP_KEY_VALUE AS int)
    FROM GENERAL_PARAMS 
   WHERE GP_GROUP_KEY = 'Interface3GS' AND GP_SUBJECT_KEY ='IgnoreMachineNumber'
  
   SELECT @terminal_id = TE_TERMINAL_ID 
     FROM TERMINALS_3GS, TERMINALS
  WHERE T3GS_VENDOR_ID      = @VendorId
    AND T3GS_SERIAL_NUMBER  = @SerialNumber
    AND (     T3GS_MACHINE_NUMBER = @MachineNumber
          OR  @ignore_machine_number = 1 )
    AND T3GS_TERMINAL_ID    = TE_TERMINAL_ID
  
  RETURN @terminal_id
END -- GetTerminalID

GO

--------------------------------------------------------------------------------
-- PURPOSE: No terminal found, insert to Pending
-- 
--  PARAMS:
--      - INPUT:
--        @CardData 	    varchar(24)
--
--      - OUTPUT:
--
-- RETURNS:
--      Account ID
--   NOTES:
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InsertTerminal]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[InsertTerminal]
GO
CREATE PROCEDURE dbo.InsertTerminal
 (@Source          int,
  @VendorId 	     varchar(16),
  @SerialNumber    varchar(30),
  @MachineNumber   int)
AS
BEGIN
  -- No terminal found, insert to Pending
  BEGIN TRAN 
  IF (( SELECT COUNT(*) 
          FROM TERMINALS_PENDING 
         WHERE TP_SOURCE         = @Source
           AND TP_VENDOR_ID      = @VendorId
           AND TP_SERIAL_NUMBER  = @SerialNumber) = 0)
  BEGIN
     INSERT INTO TERMINALS_PENDING (TP_SOURCE
                                 , TP_VENDOR_ID
                                 , TP_SERIAL_NUMBER
                                 , TP_MACHINE_NUMBER)
                           VALUES (@Source
                                 , @VendorId
                                 , @SerialNumber
                                 , @MachineNumber)
  END
  ELSE
  BEGIN
    -- MACHINE_NUMBER HAS CHANGED
    UPDATE TERMINALS_PENDING 
       SET TP_MACHINE_NUMBER = @MachineNumber
     WHERE TP_SOURCE         = @Source
       AND TP_VENDOR_ID      = @VendorId
       AND TP_SERIAL_NUMBER  = @SerialNumber   
  END
  COMMIT TRAN
END -- InsertTerminal

GO

--------------------------------------------------------------------------------
-- PURPOSE: Return the account ID
-- 
--  PARAMS:
--      - INPUT:
--        @CardData 	    varchar(24)
--
--      - OUTPUT:
--
-- RETURNS:
--      Account ID
--   NOTES:
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetAccountID]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[GetAccountID]
GO
CREATE FUNCTION dbo.GetAccountID
 (@CardData 	    varchar(24)) 
RETURNS bigint
AS
BEGIN
  DECLARE @internal nvarchar (255)

  -- -- Force 20 digits (like BETSTONE)
  SET @internal = dbo.TrackDataToInternal(RIGHT('00000000000000000000' + @CardData, 20))
  
  -- -- To use the received track data (like ZITRO, the account has to be updated with this track data)
  -- SET @internal = @CardData

  RETURN (ISNULL((SELECT AC_ACCOUNT_ID FROM ACCOUNTS WHERE AC_TRACK_DATA LIKE @internal),0))

END -- GetAccountID

GO

--------------------------------------------------------------------------------
-- PURPOSE: Check if the account is blocked
-- 
--  PARAMS:
--      - INPUT:
--        @AccountId      bigint
--
--      - OUTPUT:
--
-- RETURNS:
--      0 if it is NOT blocked
--      1 if it is blocked
--   NOTES:
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CheckAccountBlocked]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[CheckAccountBlocked]
GO
CREATE FUNCTION dbo.CheckAccountBlocked
 (@AccountId       bigint) 
RETURNS int
AS
BEGIN
  DECLARE @blocked int

  SELECT @blocked = AC_BLOCKED FROM ACCOUNTS WHERE AC_ACCOUNT_ID = @AccountId
  IF @blocked = 1 RETURN 1

  RETURN 0
END -- CheckAccountBlocked

GO

--------------------------------------------------------------------------------
-- PURPOSE: Check if the terminal is blocked
-- 
--  PARAMS:
--      - INPUT:
--        @TerminalId	  int
--
--      - OUTPUT:
--
-- RETURNS:
--      0 if it is NOT blocked
--      1 if it is blocked
--   NOTES:
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CheckTerminalBlocked]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[CheckTerminalBlocked]
GO
CREATE FUNCTION dbo.CheckTerminalBlocked
 (@TerminalId	   int)
RETURNS int
AS
BEGIN
  DECLARE @blocked int
  DECLARE @status int
  DECLARE @disable_new_sessions int
  
  --- 
  --- Do not allow to start session if new sessions are disabled
  ---
  SELECT @disable_new_sessions = GP_KEY_VALUE
    FROM GENERAL_PARAMS
   WHERE GP_GROUP_KEY = 'Site'
     AND GP_SUBJECT_KEY = 'DisableNewSessions'
     
  IF @disable_new_sessions = 1 RETURN 1

  SELECT @blocked = TE_BLOCKED ,
         @status  = TE_STATUS 
    FROM TERMINALS 
   WHERE TE_TERMINAL_ID = @TerminalId
   
  -- Terminal Blocked or Retired
  IF @blocked = 1 OR @status <> 0 RETURN 1

  RETURN 0
END -- CheckTerminalBlocked

GO

--------------------------------------------------------------------------------
-- PURPOSE: Unlocks the play session
-- 
--  PARAMS:
--      - INPUT:
--        @SessionId	    bigint
--
--      - OUTPUT:
--
--   NOTES:
--      - @SessionId must be a valid Session Id. It is not checked in this procedure
--        that it is valid.
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UnlockPlaySession]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UnlockPlaySession]
GO
CREATE PROCEDURE dbo.UnlockPlaySession
 (@SessionId bigint)
AS
BEGIN
  UPDATE PLAY_SESSIONS 
  SET PS_LOCKED = NULL 
  WHERE PS_PLAY_SESSION_ID = @SessionId
END -- UnlockPlaySession

GO

--------------------------------------------------------------------------------
-- PURPOSE: say if the play session is active (open)
-- 
--  PARAMS:
--      - INPUT:
--        @SessionId	    bigint
--
--      - OUTPUT:
--
--   NOTES:
--      - @SessionId must be a valid Session Id. It is not checked in this procedure
--        that it is valid.
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[IsPlaySessionActive]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[IsPlaySessionActive]
GO
CREATE FUNCTION dbo.IsPlaySessionActive
 (@SessionId bigint)
RETURNS int
AS
BEGIN
  IF NOT EXISTS  (SELECT PS_PLAY_SESSION_ID 
                    FROM PLAY_SESSIONS
                   WHERE PS_PLAY_SESSION_ID = @SessionId
                     AND PS_STATUS = 0)
    RETURN 0
  
  RETURN 1
END -- IsPlaySessionActive

GO

--------------------------------------------------------------------------------
-- PURPOSE: Check if terminal has an active session and it's open
-- 
--  PARAMS:
--      - INPUT:
--        @TerminalId       int
--        @SessionId        bigint
--
--      - OUTPUT:
--
-- RETURNS:
--      0 if false
--      1 if true
--   NOTES:
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CheckTerminalPlaySession]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[CheckTerminalPlaySession]
GO
CREATE FUNCTION dbo.CheckTerminalPlaySession
 (@TerminalId    int,
  @PlaySessionId bigint)
RETURNS int
AS
BEGIN
  -- Check if terminal.te_session_id is equal to received pSessionId
  RETURN (SELECT COUNT(*)
            FROM PLAY_SESSIONS
           WHERE PS_TERMINAL_ID     = @TerminalId
             AND PS_PLAY_SESSION_ID = @PlaySessionId 
             AND PS_STATUS          = 0)
END -- CheckTerminalPlaySession

GO

--------------------------------------------------------------------------------
-- PURPOSE: Check account.ac_balance vs pCreditBalance
-- 
--  PARAMS:
--      - INPUT:
--       @CreditBalance     money
--       @AccountId         bigint
--
--      - OUTPUT:
--
-- RETURNS:
--      0 if are not equal
--      1 if are equal
--   NOTES:
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CheckAccountBalance]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[CheckAccountBalance]
GO
CREATE FUNCTION dbo.CheckAccountBalance
  (@CreditBalance money,
   @AccountId bigint)
RETURNS int
AS
BEGIN
  IF NOT EXISTS (SELECT AC_ACCOUNT_ID FROM ACCOUNTS
                   WHERE AC_ACCOUNT_ID = @AccountId
                     AND AC_BALANCE = @CreditBalance)
  BEGIN
    RETURN 0
  END

  RETURN 1
END -- CheckAccountBalance

GO

--------------------------------------------------------------------------------
-- PURPOSE: Check Gaming Data from the Play Session
--          Also calculates the delta values: DeltaValue = ActualValue - PreviousValue
-- 
--  PARAMS:
--      - INPUT:
--        @SessionId      bigint
--        @CreditsPlayed  money,
--        @CreditsWon     money,
--        @GamesPlayed    int,
--        @GamesWon       int
--
--      - OUTPUT:
--
--   NOTES:
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CheckAndGetPlaySessionData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CheckAndGetPlaySessionData]
GO
CREATE PROCEDURE dbo.CheckAndGetPlaySessionData
 (@SessionId      bigint,
  @PlayedAmount   money,
  @WonAmount      money,
  @PlayedCount    int,
  @WonCount       int,
  @CreditBalance  money,
  @IsEndSession   int)
AS
BEGIN
  DECLARE @rc                   int
  DECLARE @status_code          int
  DECLARE @status_text          varchar (254)
  DECLARE @played_amount        money
  DECLARE @won_amount           money
  DECLARE @played_count         int
  DECLARE @won_count            int
  DECLARE @delta_played_amount  money
  DECLARE @delta_won_amount     money
  DECLARE @delta_played_count   int
  DECLARE @delta_won_count      int

  SET @status_code = 0
  SET @status_text = ''

  IF (  ( @PlayedAmount  < 0 )
     OR ( @WonAmount     < 0 )
     OR ( @PlayedCount   < 0 )
     OR ( @WonCount      < 0 )
     OR ( @CreditBalance < 0 ))
  BEGIN
    SET @status_code  = 3
    SET @status_text  = 'Invalid Session ID number'
    GOTO ERROR_PROCEDURE
  END

  SELECT   @played_count  = PS_PLAYED_COUNT
         , @played_amount = PS_PLAYED_AMOUNT
         , @won_count     = PS_WON_COUNT
         , @won_amount    = PS_WON_AMOUNT
    FROM   PLAY_SESSIONS
   WHERE   PS_PLAY_SESSION_ID = @SessionId

  SET @rc = @@ROWCOUNT
  IF ( @rc <> 1 )
  BEGIN
    SET @status_code  = 3
    SET @status_text  = 'Invalid Session ID number'
    GOTO ERROR_PROCEDURE
  END

  -- 04-AUG-2010 MBF: Patch for CADILLAC vendor
  IF ( ( @IsEndSession <> 0 ) AND ( @WonCount = 0 ) )
  BEGIN
    SET @WonCount = @won_count
  END
    
  -- AJQ 18-SEP-2010, Accept counters going back

  SET @delta_played_amount = @PlayedAmount - @played_amount
  SET @delta_won_amount    = @WonAmount    - @won_amount
  SET @delta_played_count  = @PlayedCount  - @played_count
  SET @delta_won_count     = @WonCount     - @won_count

ERROR_PROCEDURE:
	SELECT @status_code         AS StatusCode,        @status_text      AS StatusText
	     , @delta_played_amount AS DeltaPlayedAmount, @delta_won_amount AS DeltaWonAmount
	     , @delta_played_count  AS DeltaPlayedCount,  @delta_won_count  AS DeltaWonCount

END -- CheckAndGetPlaySessionData


GO

--------------------------------------------------------------------------------
-- PURPOSE: Update Gaming Data from the Play Session
-- 
--  PARAMS:
--      - INPUT:
--        @SessionId      bigint
--        @CreditsPlayed  money,
--        @CreditsWon     money,
--        @GamesPlayed    int,
--        @GamesWon       int,
--        @CreditBalance  money
--
--      - OUTPUT:
--
--   NOTES:
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UpdatePlaySessionData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UpdatePlaySessionData]
GO
CREATE PROCEDURE dbo.UpdatePlaySessionData
 (@SessionId      bigint,
  @CreditsPlayed  money,
  @CreditsWon 	  money,
  @GamesPlayed 	  int,
  @GamesWon 	  int,
  @CreditBalance  money)
AS
BEGIN
  DECLARE @rc             int
  DECLARE @status_code    int
	DECLARE @status_text    varchar (254)

  SET @status_code = 0
	SET @status_text = ''

  UPDATE PLAY_SESSIONS
  SET PS_PLAYED_COUNT  = @GamesPlayed
    , PS_PLAYED_AMOUNT = @CreditsPlayed
    , PS_WON_COUNT     = @GamesWon
    , PS_WON_AMOUNT    = @CreditsWon
    , PS_FINAL_BALANCE = @CreditBalance
    , PS_FINISHED      = GETDATE()  
  WHERE PS_PLAY_SESSION_ID = @SessionId

  SET @rc = @@ROWCOUNT
  IF ( @rc <> 1 )
  BEGIN
    SET @status_code  = 3
    SET @status_text  = 'Invalid Session ID number'
    GOTO ERROR_PROCEDURE
  END

ERROR_PROCEDURE:
	SELECT @status_code AS StatusCode, @status_text AS StatusText

END -- UpdatePlaySessionData

GO

--------------------------------------------------------------------------------
-- PURPOSE: Update Gaming Data from the Play Session using Delta values
-- 
--  PARAMS:
--      - INPUT:
--        @AccountId          bigint
--        @SessionId          bigint
--        @TerminalId         int
--        @DeltaPlayedAmount  money
--        @DeltaWonAmount     money
--
--      - OUTPUT:
--
--   NOTES:
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UpdateAccountData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UpdateAccountData]
GO
CREATE PROCEDURE dbo.UpdateAccountData
 (@AccountId         bigint,
  @SessionId         bigint,
  @TerminalId        int,
  @ReportedBalance   money,
  @IsEndSession      int)
AS
BEGIN
  DECLARE @is_promotion   int
  DECLARE @promo_balance  money
  DECLARE @balance        money
  DECLARE @rc             int
  DECLARE @status_code    int
  DECLARE @status_text    varchar (254)
  DECLARE @aux_amount     money
  DECLARE @cashin_playing money
  DECLARE @am_id_started  bigint

  SET @status_code = 0
    SET @status_text = ''

  SELECT @is_promotion   = CASE WHEN ( (AC_PROMO_CREATION <= GETDATE()) AND (GETDATE() < AC_PROMO_EXPIRATION) AND (AC_PROMO_BALANCE >0) ) THEN 1 ELSE 0 END
       , @promo_balance  = AC_PROMO_BALANCE 
       , @balance        = AC_BALANCE 
  FROM   ACCOUNTS                   
  WHERE ( AC_ACCOUNT_ID = @AccountId )

  SET @rc = @@ROWCOUNT
  IF ( @rc <> 1 )
  BEGIN
    SET @is_promotion = 0
    SET @balance      = 0
    SET @status_code  = 1
    SET @status_text  = 'Invalid Account Number'
    GOTO ERROR_PROCEDURE
  END

  IF ( @is_promotion = 1 )
  BEGIN
    -- AJQ 16-SEP-2010, We believe the machine
    SET @promo_balance  = @ReportedBalance 
  END
  ELSE
  BEGIN
    -- AJQ 16-SEP-2010, We believe the machine
    SET @balance        = @ReportedBalance 
    
    SELECT   @cashin_playing    = ISNULL (AC_CASHIN_WHILE_PLAYING, 0)
      FROM   ACCOUNTS
     WHERE   AC_ACCOUNT_ID      = @AccountId;
       
    -- AJQ 17-SEP-2010, Add all the CashIn's while playing
    SET @balance = @ReportedBalance + @cashin_playing      

  END

  IF ( @is_promotion = 1 )
  BEGIN
    UPDATE ACCOUNTS
    SET AC_PROMO_BALANCE  = @promo_balance
      , AC_LAST_ACTIVITY  = GETDATE()
    WHERE AC_ACCOUNT_ID              = @AccountId
      AND AC_CURRENT_TERMINAL_ID     = @TerminalId
      AND AC_CURRENT_PLAY_SESSION_ID = @SessionId 
  END
  ELSE
  BEGIN
  
    IF ( @IsEndSession = 1 )
      BEGIN
        UPDATE ACCOUNTS
           SET AC_CASHIN_WHILE_PLAYING    = NULL
         WHERE AC_ACCOUNT_ID              = @AccountId
           AND AC_CURRENT_TERMINAL_ID     = @TerminalId
           AND AC_CURRENT_PLAY_SESSION_ID = @SessionId     	
    END
    
    UPDATE ACCOUNTS
    SET AC_BALANCE        = @balance
      , AC_LAST_ACTIVITY  = GETDATE()
    WHERE AC_ACCOUNT_ID              = @AccountId
      AND AC_CURRENT_TERMINAL_ID     = @TerminalId
      AND AC_CURRENT_PLAY_SESSION_ID = @SessionId 
    
  END
  
  SET @rc = @@ROWCOUNT
  IF ( @rc <> 1 )
  BEGIN
    SET @status_code  = 1
    SET @status_text  = 'Invalid Account Number'
    GOTO ERROR_PROCEDURE
  END

ERROR_PROCEDURE:
  IF ( @is_promotion = 0 ) SET @aux_amount = @balance
  ELSE SET @aux_amount = @promo_balance

    SELECT @status_code AS StatusCode, @status_text AS StatusText, @aux_amount AS Balance

END -- UpdateAccountData

GO

--------------------------------------------------------------------------------
-- PURPOSE: Update session (INTERNAL)
-- 
--  PARAMS:
--      - INPUT:
--          @AccountId      bigint
--          @TerminalId     int
--          @SessionId      bigint
--          @AmountPlayed   money
--          @AmountWon      money
--          @GamesPlayed    int
--          @GamesWon       int
--          @CreditBalance  money
--          @CurrentJackpot money = 0
--
--      - OUTPUT:
--
-- RETURNS:
--      StatusCode
--      StatusText
--
--   NOTES:
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SessionUpdate_Internal]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SessionUpdate_Internal]
GO
CREATE PROCEDURE dbo.SessionUpdate_Internal
  @AccountId         bigint,
  @TerminalId        int,
  @SerialNumber      varchar(30),
  @SessionId         bigint,
  @AmountPlayed      money,
  @AmountWon         money,
  @GamesPlayed       int,
  @GamesWon          int,
  @CreditBalance     money,
  @CurrentJackpot    money = 0,
  @IsEndSession      int = 0,
  @DeltaAmountPlayed money OUTPUT,
  @DeltaAmountWon    money OUTPUT,
  @status_code       int OUTPUT,
  @status_text       nvarchar (254) OUTPUT
AS
BEGIN
  -- SET NOCOUNT ON added to prevent extra result sets from
  -- interfering with SELECT statements.
  SET NOCOUNT ON;

  DECLARE @balance              money
  DECLARE @delta_played_amount  money
  DECLARE @delta_won_amount     money
  DECLARE @delta_played_count   int
  DECLARE @delta_won_count      int

  SET @status_code = 0
  SET @status_text = ''

  -- Check if terminal has session active
  -- Check if terminal session_id is equal to received terminal id
  -- Check if play session is Open
  IF ((SELECT dbo.CheckTerminalPlaySession(@TerminalId, @SessionId)) <> 1)
  BEGIN
    SET @status_code = 3
    SET @status_text = 'Invalid Session ID number'
    GOTO ERROR_PROCEDURE
  END	
  
    -- Check play session data and get delta values (actual - previous)
  DECLARE @update_cs_table TABLE
     (StatusCode int, StatusText nvarchar(254)
    , DeltaPlayedAmount money, DeltaWonAmount money
	  , DeltaPlayedCount int, DeltaWonCount int)

  INSERT INTO @update_cs_table
    EXECUTE dbo.CheckAndGetPlaySessionData @SessionId, 
                                           @AmountPlayed, @AmountWon,
                                           @GamesPlayed,  @GamesWon, 
                                           @CreditBalance, 
                                           @IsEndSession

  SELECT @status_code         = StatusCode
       , @status_text         = StatusText
       , @delta_played_amount = DeltaPlayedAmount
       , @delta_won_amount    = DeltaWonAmount
       , @delta_played_count  = DeltaPlayedCount
       , @delta_won_count     = DeltaWonCount
    FROM @update_cs_table

  IF ( @status_code <> 0 )
    GOTO ERROR_PROCEDURE

  -- Update account data!
  DECLARE @update_ac_table TABLE (StatusCode int, StatusText nvarchar(254), Balance money)
  INSERT INTO @update_ac_table
    EXECUTE dbo.UpdateAccountData @AccountId, @SessionId, @TerminalId, @CreditBalance, @IsEndSession
  SELECT @status_code = StatusCode
       , @status_text = StatusText
       , @balance     = Balance
    FROM @update_ac_table
  
  IF ( @status_code <> 0 )
    GOTO ERROR_PROCEDURE 

  -- Update Game Meters
  DECLARE @update_gm_table TABLE (StatusCode int, StatusText nvarchar(254))
  INSERT INTO @update_gm_table
    EXECUTE dbo.UpdateGameMeters @TerminalId, 
                                 @delta_played_count, @delta_played_amount,
                                 @delta_won_count,    @delta_won_amount
  
  SELECT @status_code = StatusCode
       , @status_text = StatusText
    FROM @update_gm_table
    
  IF ( @status_code <> 0 )
    GOTO ERROR_PROCEDURE

  -- Update data play session
  DECLARE @update_se_table TABLE (StatusCode int, StatusText nvarchar(254))
  INSERT INTO @update_se_table
    EXECUTE dbo.UpdatePlaySessionData @SessionId, @AmountPlayed, @AmountWon, @GamesPlayed,
                                      @GamesWon, @CreditBalance
  SELECT @status_code = StatusCode
       , @status_text = StatusText
    FROM @update_se_table

  IF ( @status_code <> 0 )
    GOTO ERROR_PROCEDURE

  SET @DeltaAmountPlayed = @delta_played_amount
  SET @DeltaAmountWon    = @delta_won_amount


ERROR_PROCEDURE:

END -- SessionUpdate_Internal

GO

--------------------------------------------------------------------------------
-- PURPOSE: Close Play Session
-- 
--  PARAMS:
--      - INPUT:
--        @SessionId       bigint
--
--      - OUTPUT:
--        @StatusCode int OUTPUT
--        @StatusText nvarchar (254) OUTPUT
--
--   NOTES:
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CloseOpenedPlaySession]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CloseOpenedPlaySession]
GO
CREATE PROCEDURE dbo.CloseOpenedPlaySession
 (@SessionId bigint,
  @StatusCode int OUTPUT,
  @StatusText nvarchar (254) OUTPUT)
AS
BEGIN
  DECLARE @rc             int

  SET @StatusCode = 0
	SET @StatusText = ''

  UPDATE PLAY_SESSIONS
  SET PS_STATUS            = 1
    , PS_LOCKED            = NULL
    , PS_FINISHED          = GETDATE()
  WHERE PS_PLAY_SESSION_ID = @SessionId
    AND PS_STATUS          = 0
  
  SET @rc = @@ROWCOUNT
  IF ( @rc <> 1 )
  BEGIN
    SET @StatusCode = 3
    SET @StatusText = 'Invalid Session ID number'
  END

END -- ClosePlaySession

GO

--------------------------------------------------------------------------------
-- PURPOSE: Close Account Session
-- 
--  PARAMS:
--      - INPUT:
--        @AccountId       bigint
--
--      - OUTPUT:
--
--   NOTES:
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CloseAccountSession]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CloseAccountSession]
GO
CREATE PROCEDURE dbo.CloseAccountSession
 (@AccountId bigint)
AS
BEGIN

  UPDATE ACCOUNTS
  SET AC_LAST_TERMINAL_ID        = AC_CURRENT_TERMINAL_ID
    , AC_LAST_TERMINAL_NAME      = AC_CURRENT_TERMINAL_NAME
    , AC_LAST_PLAY_SESSION_ID    = AC_CURRENT_PLAY_SESSION_ID
    , AC_CURRENT_TERMINAL_ID     = NULL
    , AC_CURRENT_TERMINAL_NAME   = NULL
    , AC_CURRENT_PLAY_SESSION_ID = NULL
  WHERE AC_ACCOUNT_ID            = @AccountId
    AND AC_CURRENT_TERMINAL_ID     IS NOT NULL
    AND AC_CURRENT_PLAY_SESSION_ID IS NOT NULL
    AND AC_CURRENT_TERMINAL_NAME   IS NOT NULL
  
END -- CloseAccountSession

GO
-- =============================================
-- Author:		Name
-- Create date: 
-- Description:	
-- =============================================
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[AM_3GS_Trigger]'))
DROP TRIGGER [dbo].[AM_3GS_Trigger]
GO
CREATE TRIGGER [dbo].[AM_3GS_Trigger]
   ON  [dbo].[account_movements] 
   AFTER INSERT
NOT FOR REPLICATION
AS 
BEGIN
DECLARE @mov_type    int
DECLARE @new_cashin  money
DECLARE @account_id  bigint
DECLARE @terminal_id int

      -- SET NOCOUNT ON added to prevent extra result sets from
      -- interfering with SELECT statements.
      SET NOCOUNT ON;

    -- Check Movement Type
      -- 1 - CashIn
      -- 9 - NonRedeemable CashIn
      -- 44 - Prize Coupon
      SET @mov_type = (SELECT AM_TYPE FROM INSERTED)
    
      IF ( @mov_type = 1 OR @mov_type = 9 OR @mov_type = 44 ) 
    BEGIN
       SET @account_id  = (SELECT AM_ACCOUNT_ID FROM INSERTED)
         
       -- Check if it is in session
       SET @terminal_id = (SELECT AC_CURRENT_TERMINAL_ID FROM ACCOUNTS WHERE AC_ACCOUNT_ID = @account_id)
         -- Is Account 'InUse'?
       IF @terminal_id IS NOT NULL
       BEGIN
            -- Yes, It is 'InUse'
        -- Is 3GS Terminal?
            IF EXISTS ( SELECT T3GS_TERMINAL_ID FROM TERMINALS_3GS WHERE T3GS_TERMINAL_ID = @terminal_id )
        BEGIN
              -- 3GS Terminal
          SET @new_cashin  = (SELECT AM_ADD_AMOUNT FROM INSERTED)
          -- Update 'cashin_while_playing'
              UPDATE ACCOUNTS 
             SET AC_CASHIN_WHILE_PLAYING = ISNULL (AC_CASHIN_WHILE_PLAYING, 0) + @new_cashin
           WHERE AC_ACCOUNT_ID           = @account_id
             AND AC_CURRENT_TERMINAL_ID  = @terminal_id         
        END
       END
      END

END
GO      
--------------------------------------------------------------------------------
-- Copyright � 2010 Win Systems International
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: Audit.sql
-- 
--   DESCRIPTION: Audit procedure
-- 
--        AUTHOR: Miquel Beltran
-- 
-- CREATION DATE: 28-APR-2010
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 17-JUN-2010 MBF    First release.
--------------------------------------------------------------------------------

USE [wgdb_000]
GO

--------------------------------------------------------------------------------
-- PURPOSE: Audit procedure execution
-- 
--  PARAMS:
--      - INPUT:
--         @ProcedureName nvarchar(50),
--         @AccountID     nvarchar(24), 
--         @VendorID      nvarchar(16), 
--         @SerialNumber  nvarchar(30), 
--         @MachineNumber int,
--         @StatusCode    int,
--         @Balance       Money,
--         @Input         nvarchar(MAX),
--         @Output        nvarchar(MAX) 
--      - OUTPUT:
--
-- RETURNS:
--      Nothing
--
--   NOTES:
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[zsp_Audit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[zsp_Audit]
GO
CREATE PROCEDURE [dbo].[zsp_Audit]
  @ProcedureName nvarchar(50),
  @AccountID     nvarchar(24), 
  @VendorID      nvarchar(16), 
  @SerialNumber  nvarchar(30), 
  @MachineNumber int,
  @SessionId     bigint,
  @StatusCode    int,
  @Balance       Money,
  @Input         nvarchar(MAX),
  @Output        nvarchar(MAX) 
AS
BEGIN 

  DECLARE @active int
  
  -- Check general params
  SELECT @active = CAST(GP_KEY_VALUE AS int)
    FROM GENERAL_PARAMS 
   WHERE GP_GROUP_KEY = 'Interface3GS' AND GP_SUBJECT_KEY ='AuditEnabled'

  IF (@active = 1)
  BEGIN

    INSERT INTO AUDIT_3GS ( A3GS_PROCEDURE
                          , A3GS_ACCOUNT_ID
                          , A3GS_VENDOR_ID
                          , A3GS_SERIAL_NUMBER
                          , A3GS_MACHINE_NUMBER
                          , A3GS_SESSION_ID
                          , A3GS_STATUS_CODE
                          , A3GS_BALANCE
                          , A3GS_INPUT
                          , A3GS_OUTPUT )
                   VALUES ( @ProcedureName
                          , @AccountID
                          , @VendorID
                          , @SerialNumber
                          , @MachineNumber
                          , @SessionId
                          , @StatusCode
                          , @Balance
                          , @Input
                          , @Output )
                          
  END      
                      
END -- [zsp_Audit]

GO   
--------------------------------------------------------------------------------
-- Copyright � 2010 Win Systems International
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: AccountStatus.sql
-- 
--   DESCRIPTION: Returns account status, including Balance
--
--        AUTHOR: Miquel Beltran
-- 
-- CREATION DATE: 07-MAY-2010
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 07-MAY-2010 MBF    First
-- 17-JUN-2010 MBF    Audit
--------------------------------------------------------------------------------

USE [wgdb_000]
GO

--------------------------------------------------------------------------------
-- PURPOSE: Returns account status
-- 
--  PARAMS:
--      - INPUT:
--         @AccountID 	    varchar(24)
--         @VendorId 	      varchar(16)
--         @MachineNumber   int
--
--      - OUTPUT:
--
-- RETURNS:
--      StatusCode
--      StatusText
--      AcctBalance  money   Account balance if available
--      VID          bigint  Account ID if available
--
--   NOTES:
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[zsp_AccountStatus]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[zsp_AccountStatus]
GO
CREATE PROCEDURE [dbo].[zsp_AccountStatus]
	@AccountID 	     varchar(24),
	@VendorId 	     varchar(16),
	@MachineNumber   int
AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
  DECLARE @status_code           int
	DECLARE @status_text           varchar (254)
	DECLARE @terminal_id           bigint
	DECLARE @account_id            bigint
	DECLARE @balance               money
	DECLARE @is_promotion          int
	DECLARE @promo_balance         money
	DECLARE @play_session_id       bigint
  DECLARE @ignore_machine_number int
	
  SET @status_code        = 0
	SET @status_text        = 'Available Account'
	SET @balance            = 0
	
	SELECT @ignore_machine_number = CAST(GP_KEY_VALUE AS int)
    FROM GENERAL_PARAMS 
   WHERE GP_GROUP_KEY = 'Interface3GS' AND GP_SUBJECT_KEY ='IgnoreMachineNumber'
	
	IF ( @ignore_machine_number <> 1 )
	BEGIN
    IF (1 <> (SELECT COUNT(*) 
                FROM TERMINALS_3GS
               WHERE T3GS_MACHINE_NUMBER = @MachineNumber
                 AND T3GS_VENDOR_ID      = @VendorId))
    BEGIN
      SET @status_code = 3
      SET @status_text = 'Invalid Machine Number'
      GOTO ERROR_PROCEDURE
    END
  END
		
	SELECT @account_id = dbo.GetAccountID(@AccountID)
	
	IF (@account_id = 0)
  BEGIN
    SET @status_code = 1
    SET @status_text = 'Invalid Account'
    GOTO ERROR_PROCEDURE
  END

  
  SELECT @play_session_id     = ISNULL(AC_CURRENT_PLAY_SESSION_ID , 0 ) 
       , @is_promotion        = CASE WHEN ( (AC_PROMO_CREATION <= GETDATE()) AND (GETDATE() < AC_PROMO_EXPIRATION) AND (AC_PROMO_BALANCE >0) ) THEN 1 ELSE 0 END
       , @promo_balance       = AC_PROMO_BALANCE 
       , @balance             = AC_BALANCE 
    FROM   ACCOUNTS                   
   WHERE ( AC_ACCOUNT_ID = @account_id )
   
 	-- Check if account has a play session
	IF (@play_session_id <> 0)
	BEGIN
	  SET @status_code = 2
    SET @status_text = 'Account In Session'
    GOTO ERROR_PROCEDURE
	END  
	
	IF (@is_promotion = 1)
  BEGIN
    SET @balance = @promo_balance
  END 
     
	ERROR_PROCEDURE:
	IF (@status_code <> 0)
	  SET @balance = 0
	
	-- Audit MBF 17-JUN-2010
	BEGIN TRAN
	
    DECLARE @input AS nvarchar(MAX)
    DECLARE @output AS nvarchar(MAX)	
  		  	  		  	
	  SET @input = '@AccountID='+@AccountID
	             +';@VendorId='+@VendorId
	             +';@MachineNumber='+CAST (@MachineNumber AS nvarchar)  	           
  	           
    SET @output = 'StatusCode='+CAST (@status_code AS nvarchar)
                 +';StatusText='+@status_text
                 +';AcctBalance='+CAST (@balance AS nvarchar)
                 +';VID=0'
     	
	  EXECUTE dbo.zsp_Audit 'zsp_AccountStatus'
                        , @AccountID
                        , @VendorId 
                        , NULL
                        , @MachineNumber
                        , NULL
                        , @status_code
                        , NULL
                        , @input
                        , @output
                      
  COMMIT TRAN
	-- End Audit
	
	SELECT @status_code AS StatusCode, @status_text AS StatusText, @balance AS Balance, '0' AS VID

END -- zsp_AccountStatus

GO   
--------------------------------------------------------------------------------
-- Copyright � 2010 Win Systems International
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: GameMeters.sql
-- 
--   DESCRIPTION: Game Meters related procedures
-- 
--        AUTHOR: Miquel Beltran
-- 
-- CREATION DATE: 03-MAY-2010
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 03-MAY-2010 MBF    First release.
--------------------------------------------------------------------------------

USE [wgdb_000]
GO

-- INIT GAME_3GS ON PROCEDURE CREATION TIME
IF NOT EXISTS ( SELECT * FROM GAMES WHERE GM_NAME = 'GAME_3GS')
INSERT INTO GAMES (GM_NAME) VALUES ('GAME_3GS')

GO
--------------------------------------------------------------------------------
-- PURPOSE: Insert game meter if not exists
-- 
--  PARAMS:
--      - INPUT:
--         @TerminalID      int
--
--      - OUTPUT:
--
-- RETURNS:
--
--   NOTES:
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InsertGameMeters]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[InsertGameMeters]
GO
CREATE PROCEDURE dbo.InsertGameMeters
	@TerminalID      int
AS
BEGIN

  IF ( (SELECT COUNT(*) FROM GAME_METERS WHERE GM_TERMINAL_ID = @TerminalID AND GM_GAME_BASE_NAME = 'GAME_3GS') = 0 )
  BEGIN
    INSERT INTO GAME_METERS ( GM_TERMINAL_ID 
                            , GM_GAME_BASE_NAME 
                            , GM_DELTA_GAME_NAME
                            , GM_WCP_SEQUENCE_ID 
                            , GM_DENOMINATION 
                            , GM_PLAYED_COUNT 
                            , GM_PLAYED_AMOUNT 
                            , GM_WON_COUNT 
                            , GM_WON_AMOUNT
                            , GM_JACKPOT_AMOUNT
                            , GM_LAST_REPORTED ) 
                   VALUES   ( @TerminalID 
                            , 'GAME_3GS'
                            , 'GAME_3GS'
                            , 0 
                            , 0.01
                            , 0 
                            , 0 
                            , 0 
                            , 0 
                            , 0 
                            , GETDATE() )
  END
 
END -- InsertGameMeters 

GO

--------------------------------------------------------------------------------
-- PURPOSE: Update game meter
-- 
--  PARAMS:
--      - INPUT:
--         @TerminalID         int
--         @DeltaPlayedCount   money
--         @DeltaPlayedAmount  money
--         @DeltaWonCount      money
--         @DeltaWonAmount     money
--
--      - OUTPUT:
--
-- RETURNS:
--
--   NOTES:
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UpdateGameMeters]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UpdateGameMeters]
GO
CREATE PROCEDURE dbo.UpdateGameMeters
	@TerminalID         int,
	@DeltaPlayedCount   money,
	@DeltaPlayedAmount  money,
	@DeltaWonCount      money,
	@DeltaWonAmount     money
AS
BEGIN

  DECLARE @status_code             int
	DECLARE @status_text             varchar (254)
  DECLARE @rc                      int

	
  SET @status_code = 0
  SET @status_text = ''
	

  IF EXISTS ( SELECT GM_PLAYED_COUNT FROM GAME_METERS WHERE GM_TERMINAL_ID = @TerminalID AND GM_GAME_BASE_NAME = 'GAME_3GS' )
  BEGIN
    
    UPDATE GAME_METERS SET GM_WCP_SEQUENCE_ID            = 0
                         , GM_DENOMINATION               = 0.01
                         , GM_PLAYED_COUNT               = GM_PLAYED_COUNT         + @DeltaPlayedCount
                         , GM_PLAYED_AMOUNT              = GM_PLAYED_AMOUNT        + @DeltaPlayedAmount
                         , GM_WON_COUNT                  = GM_WON_COUNT            + @DeltaWonCount
                         , GM_WON_AMOUNT                 = GM_WON_AMOUNT           + @DeltaWonAmount
                         , GM_DELTA_PLAYED_COUNT         = GM_DELTA_PLAYED_COUNT   + @DeltaPlayedCount
                         , GM_DELTA_PLAYED_AMOUNT        = GM_DELTA_PLAYED_AMOUNT  + @DeltaPlayedAmount
                         , GM_DELTA_WON_COUNT            = GM_DELTA_WON_COUNT      + @DeltaWonCount
                         , GM_DELTA_WON_AMOUNT           = GM_DELTA_WON_AMOUNT     + @DeltaWonAmount
                         , GM_LAST_REPORTED              = GETDATE()
                   WHERE   GM_TERMINAL_ID                = @TerminalID
                       AND GM_GAME_BASE_NAME             = 'GAME_3GS'
    
    SET @rc = @@ROWCOUNT
    IF ( @rc <> 1 )
    BEGIN
      SET @status_code = 2
      SET @status_text = 'Invalid Machine Information'
    END
  END
  ELSE
  BEGIN
    SET @status_code = 2
    SET @status_text = 'Invalid Machine Information'
  END
  
  SELECT @status_code AS StatusCode, @status_text AS StatusText
  
END -- UpdateGameMeters

GO   
--------------------------------------------------------------------------------
-- Copyright � 2010 Win Systems International
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: InsertPlaySession.sql
-- 
--   DESCRIPTION: Insert new play session procedure
-- 
--        AUTHOR: Miquel Beltran
-- 
-- CREATION DATE: 30-APR-2010
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 30-APR-2010 MBF    First release.
--------------------------------------------------------------------------------

USE [wgdb_000]
GO

--------------------------------------------------------------------------------
-- PURPOSE: Inserts the play session
-- 
--  PARAMS:
--      - INPUT:
--        TerminalId 	   int
--        AccountId 	   bigint
--        InitialBalance money
--        Promo          int
--
--      - OUTPUT:
--
-- RETURNS:
--      StatusCode, 
--      StatusText, 
--      SessionID
--   NOTES:
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InsertPlaySession]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[InsertPlaySession]
GO
CREATE PROCEDURE dbo.InsertPlaySession
  	@TerminalId 	  int,
  	@AccountId 	    bigint,
  	@InitialBalance money,
  	@Promo          int
AS
BEGIN
  
  DECLARE @activity_due_to_card_in int  
  DECLARE @ac_promo_balance        money
  DECLARE @ac_balance              money
  DECLARE @playsession_id          int
  DECLARE @status_code             int
	DECLARE @status_text             varchar (254)
  DECLARE @rc                      int
  
  SET @activity_due_to_card_in = 0
  SET @ac_promo_balance        = 0 
  SET @ac_balance              = 0 
  SET @status_code             = 0
	SET @status_text             = 'Successful Session Start'
	SET @playsession_id          = 0

  SELECT   @activity_due_to_card_in = CAST (GP_KEY_VALUE AS INT) 
    FROM   GENERAL_PARAMS 
   WHERE   GP_GROUP_KEY   = 'Play' 
     AND   GP_SUBJECT_KEY = 'ActivityDueToCardIn' 
     
   -- Mark abandoned play session
  UPDATE   PLAY_SESSIONS SET PS_STATUS        = 2 -- Abandoned
         -- AJQ 18-SEP-2010 Don't touch the final balance! , PS_FINAL_BALANCE = PS_INITIAL_BALANCE - PS_PLAYED_AMOUNT + PS_WON_AMOUNT + PS_CASH_IN - PS_CASH_OUT  
         , PS_FINISHED      = GETDATE()  
   WHERE   PS_TERMINAL_ID   = @TerminalId  
     AND   PS_STATUS        = 0 -- Opened
     AND   PS_STAND_ALONE   = 0 -- Not stand alone  

  -- Get current balance on Card
  SELECT   @ac_promo_balance       = AC_PROMO_BALANCE
         , @ac_balance             = AC_BALANCE
    FROM   ACCOUNTS
   WHERE  AC_ACCOUNT_ID = @AccountId
       
  IF ( @Promo = 1 )
    BEGIN
      SET @ac_balance = @ac_promo_balance
    END
  
  IF ( @InitialBalance <> @ac_balance )
    BEGIN
      SET @status_code = 4
      SET @status_text = 'Access Denied'
	    GOTO ERROR_PROCEDURE
    END
        
  INSERT INTO PLAY_SESSIONS (PS_ACCOUNT_ID 
                           , PS_TERMINAL_ID
                           , PS_TYPE
                           , PS_TYPE_DATA 
                           , PS_INITIAL_BALANCE 
                           , PS_FINAL_BALANCE 
                           , PS_FINISHED 
                           , PS_STAND_ALONE 
                           , PS_PROMO) 
                     VALUES (@AccountId
                           , @TerminalId
                           , 2            -- 2 = TYPE WIN
                           , NULL
                           , @InitialBalance
                           , @InitialBalance
                           , CASE WHEN (@activity_due_to_card_in = 1) THEN GETDATE() ELSE NULL END
                           , 0
                           , @Promo ) 
                         
  SET @playsession_id = SCOPE_IDENTITY()
   
  IF ( @playsession_id = 0 )
  BEGIN
      SET @status_code = 4
      SET @status_text = 'Access Denied'
    GOTO ERROR_PROCEDURE
  END
   
  UPDATE  ACCOUNTS 
     SET  AC_CURRENT_TERMINAL_ID      = @TerminalId
        , AC_CURRENT_TERMINAL_NAME    = (SELECT TE_NAME FROM TERMINALS WHERE TE_TYPE = 1 AND TE_TERMINAL_ID = @TerminalId)
        , AC_CURRENT_PLAY_SESSION_ID  = @playsession_id 
   WHERE  AC_ACCOUNT_ID               = @AccountId 
     AND  AC_CURRENT_TERMINAL_ID     IS NULL 
     AND  AC_CURRENT_TERMINAL_NAME   IS NULL  
     AND  AC_CURRENT_PLAY_SESSION_ID IS NULL 
 
  SET @rc = @@ROWCOUNT
  IF ( @rc <> 1 )
  BEGIN
    SET @status_code = 4
    SET @status_text = 'Access Denied'
    GOTO ERROR_PROCEDURE
  END
   
 ERROR_PROCEDURE:
	SELECT @status_code AS StatusCode, @status_text AS StatusText, @playsession_id AS SessionID

END -- InsertPlaySession 

GO   
--------------------------------------------------------------------------------
-- Copyright � 2010 Win Systems International
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: SendEvent.sql
-- 
--   DESCRIPTION: Send Event procedure
--                  - Jackpot won
--                  - Kiosk blocked for high prize
--                  - Call Attendant
-- 
--        AUTHOR: Miquel Beltran
-- 
-- CREATION DATE: 06-MAY-2010
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 06-MAY-2010 MBF    First
-- 17-JUN-2010 MBF    Audit
--------------------------------------------------------------------------------

USE [wgdb_000]
GO

--------------------------------------------------------------------------------
-- PURPOSE: Start play session
-- 
--  PARAMS:
--      - INPUT:
--         @AccountID 	    varchar(24)
--         @VendorId 	      varchar(16)
--         @SerialNumber    varchar(30)
--         @MachineNumber   int
--         @EventID         bigint
--         @Amount          money
--
--      - OUTPUT:
--
-- RETURNS:
--      StatusCode
--      StatusText
--
--   NOTES:
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[zsp_SendEvent]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[zsp_SendEvent]
GO
CREATE PROCEDURE [dbo].[zsp_SendEvent]
	@AccountID 	     varchar(24),
	@VendorId 	     varchar(16),
	@SerialNumber    varchar(30),
	@MachineNumber   int,
	@EventID         bigint, 
	@Amount          money
AS
BEGIN

  BEGIN TRAN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
  DECLARE @status_code         int
	DECLARE @status_text         varchar (254)
	DECLARE @operation_code      int
	DECLARE @play_session_id     bigint
	DECLARE @terminal_id         bigint
	DECLARE @account_id          bigint
	DECLARE @account_terminal_id bigint
	
  SET @status_code        = @EventID + 100
	SET @status_text        = ''
		
	SELECT @terminal_id = dbo.GetTerminalID(@VendorId, @SerialNumber, @MachineNumber) 
	  
  IF (@terminal_id = 0)
  BEGIN
    EXECUTE dbo.InsertTerminal 0, @VendorId, @SerialNumber, @MachineNumber
    SET @status_code = 3
    SET @status_text = 'Invalid Machine Information'
	  COMMIT TRAN
    BEGIN TRAN
    GOTO ERROR_PROCEDURE
  END
		
	SELECT @account_id = dbo.GetAccountID(@AccountID)
	
	IF (@account_id = 0)
  BEGIN
    SET @status_code = 1
    SET @status_text = 'Invalid Account Number'
    GOTO ERROR_PROCEDURE
  END
	
	SET @operation_code = CASE WHEN (@EventID = 1) THEN 16
	                           WHEN (@EventID = 2) THEN 17
	                           WHEN (@EventID = 3) THEN 18
	                                               ELSE  0 END
	
	-- Unknown codes, do nothing
	IF (@operation_code = 0)
	BEGIN
    GOTO ERROR_PROCEDURE
	END
	
	SELECT @play_session_id     = ISNULL(AC_CURRENT_PLAY_SESSION_ID , 0 ) 
       , @account_terminal_id = ISNULL(AC_CURRENT_TERMINAL_ID , 0 )
  FROM   ACCOUNTS                   
  WHERE ( AC_ACCOUNT_ID = @account_id )
	
	-- Check if account has a play session
	IF (@play_session_id = 0)
	BEGIN
	  SET @status_code = 4
    SET @status_text = 'Access Denied'
    GOTO ERROR_PROCEDURE
	END
	
	-- Check if the terminal is the same as the playsession
	IF (@account_terminal_id <> @terminal_id)
	BEGIN
	  SET @status_code = 4
    SET @status_text = 'Access Denied'
    GOTO ERROR_PROCEDURE
	END
		
	INSERT INTO EVENT_HISTORY (EH_TERMINAL_ID 
                           , EH_SESSION_ID 
                           , EH_DATETIME 
                           , EH_EVENT_TYPE 
                           , EH_OPERATION_CODE 
                           , EH_OPERATION_DATA)
                     VALUES (@terminal_id
                           , @play_session_id
                           , GETDATE()
                           , 2 
                           , @operation_code
                           , @Amount)
	
	COMMIT TRAN
	GOTO OK  
	  
	ERROR_PROCEDURE:
	ROLLBACK TRAN
	  
	OK: 
	
	-- Audit MBF 17-JUN-2010
	BEGIN TRAN
	
    DECLARE @input AS nvarchar(MAX)
    DECLARE @output AS nvarchar(MAX)
  		  	
	  SET @input = '@AccountID='+@AccountID
	             +';@VendorId='+@VendorId
	             +';@SerialNumber='+CAST (@SerialNumber AS nvarchar)
	             +';@MachineNumber='+CAST (@MachineNumber AS nvarchar)
	             +';@EventID='+CAST (@EventID AS nvarchar)
	             +';@Amount='+CAST (@Amount AS nvarchar)  	           
  	           
    SET @output = 'StatusCode='+CAST (@status_code AS nvarchar)
                 +';StatusText='+@status_text
     	
	  EXECUTE dbo.zsp_Audit 'zsp_SendEvent'
                        , @AccountID
                        , @VendorId 
                        , @SerialNumber
                        , @MachineNumber
                        , NULL
                        , @status_code
                        , NULL
                        , @input
                        , @output
                      
  COMMIT TRAN
	-- End Audit
	 
	SELECT @status_code AS StatusCode, @status_text AS StatusText

	
END -- zsp_SendEvent

GO   
--------------------------------------------------------------------------------
-- Copyright � 2010 Win Systems International
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: SessionEnd.sql
-- 
--   DESCRIPTION: End Session procedure
-- 
--        AUTHOR: Miquel Beltran
-- 
-- CREATION DATE: 30-APR-2010
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 30-APR-2010 MBF    First release.
-- 17-JUN-2010 MBF    Audit
--------------------------------------------------------------------------------

USE [wgdb_000]
GO

--------------------------------------------------------------------------------
-- PURPOSE: End play session
-- 
--  PARAMS:
--      - INPUT:
--          @AccountId      varchar(24)
--          @VendorId       varchar(16)
--          @SerialNumber   varchar(30)
--          @MachineNumber  int
--          @SessionId      bigint
--          @AmountPlayed   money
--          @AmountWon      money
--          @GamesPlayed    int
--          @GamesWon       int
--          @CreditBalance  money,
--          @CurrentJackpot money = 0
--
--      - OUTPUT:
--
-- RETURNS:
--      StatusCode
--      StatusText
--
--   NOTES:
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[zsp_SessionEnd]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[zsp_SessionEnd]
GO
CREATE PROCEDURE [dbo].[zsp_SessionEnd]
  @AccountId      varchar(24),
  @VendorId       varchar(16),
  @SerialNumber   varchar(30),
  @MachineNumber  int,
  @SessionId 	    bigint,
  @AmountPlayed   money,
  @AmountWon      money,
  @GamesPlayed    int,
  @GamesWon       int,
  @CreditBalance  money,
  @CurrentJackpot money = 0
AS
BEGIN

  BEGIN TRAN

  -- SET NOCOUNT ON added to prevent extra result sets from
  -- interfering with SELECT statements.
  SET NOCOUNT ON;

  DECLARE @status_code         int
  DECLARE @status_text         varchar (254)
  DECLARE @terminal_id         int
  DECLARE @account_id          bigint
  DECLARE @previous_balance    money
  DECLARE @delta_amount_played money
  DECLARE @delta_amount_won    money
  DECLARE @ignore_session_id   int
  DECLARE @is_end_session      int

  SET @status_code         = 0
  SET @status_text         = 'Successful End Session'
  SET @delta_amount_played = 0
  SET @delta_amount_won    = 0
  SET @is_end_session      = 1

  SELECT @terminal_id = dbo.GetTerminalID(@VendorId, @SerialNumber, @MachineNumber) 

  IF (@terminal_id = 0)
  BEGIN
    SET @status_code = 2
    SET @status_text = 'Invalid Machine Information'
    GOTO ERROR_PROCEDURE
  END

  SELECT @account_id = dbo.GetAccountID(@AccountId)

  IF (@account_id = 0)
  BEGIN
    SET @status_code = 1
    SET @status_text = 'Invalid Account Number'
    GOTO ERROR_PROCEDURE
  END
  
  SELECT @ignore_session_id = CAST(GP_KEY_VALUE AS int)
    FROM GENERAL_PARAMS 
   WHERE GP_GROUP_KEY = 'Interface3GS' AND GP_SUBJECT_KEY ='IgnoreSessionID'
  
  IF ( @ignore_session_id = 1 )
  BEGIN
    SELECT @SessionId = ISNULL(AC_CURRENT_PLAY_SESSION_ID , 0 ) 
      FROM   ACCOUNTS                   
     WHERE ( AC_ACCOUNT_ID = @account_id )
  END

  EXECUTE dbo.SessionUpdate_Internal @account_id, @terminal_id, @SerialNumber, @SessionId,
                                     @AmountPlayed, @AmountWon, @GamesPlayed, @GamesWon,
                                     @CreditBalance, @CurrentJackpot, @is_end_session, 
                                     @delta_amount_played OUTPUT, @delta_amount_won OUTPUT,
                                     @status_code OUTPUT, @status_text OUTPUT
  IF ( @status_code <> 0 )
    GOTO ERROR_PROCEDURE

  -- close play session
  EXECUTE dbo.CloseOpenedPlaySession @SessionId, @status_code OUTPUT, @status_text OUTPUT

  IF ( @status_code <> 0 )
    GOTO ERROR_PROCEDURE
    
  -- Close account session
  EXECUTE dbo.CloseAccountSession @account_id

  -- Insert Movement
  
  -- Use delta values to calculate initial balance
  --SET @previous_balance = @CreditBalance + @AmountPlayed - @AmountWon
  SET @previous_balance = @CreditBalance + @delta_amount_played - @delta_amount_won

  IF (1 = (SELECT   CASE WHEN ( (AC_PROMO_CREATION <= GETDATE()) AND (GETDATE() < AC_PROMO_EXPIRATION) AND (AC_PROMO_BALANCE >0) ) THEN 1 ELSE 0 END
            FROM   ACCOUNTS                   
           WHERE ( AC_ACCOUNT_ID = @account_id )))
  BEGIN
    -- PromoEndSession   = 25,
    EXECUTE dbo.InsertMovement @SessionId, @account_id, @terminal_id, 25, @previous_balance, @delta_amount_played, @delta_amount_won, @CreditBalance

  END
	ELSE
  BEGIN
    -- EndCardSession = 6,
    EXECUTE dbo.InsertMovement @SessionId, @account_id, @terminal_id,  6, @previous_balance, @delta_amount_played, @delta_amount_won, @CreditBalance
  END
	  
  SET @status_text = 'Successful End Session'

  COMMIT TRAN
	GOTO OK  
	  
	ERROR_PROCEDURE:
	  ROLLBACK TRAN
	  
	OK:  
	
	-- Audit MBF 17-JUN-2010
	BEGIN TRAN
	
    DECLARE @input AS nvarchar(MAX)
    DECLARE @output AS nvarchar(MAX)	
  		  	
	  SET @input = '@AccountID='+@AccountID
	             +';@VendorId='+@VendorId
	             +';@SerialNumber='+CAST (@SerialNumber AS nvarchar)
	             +';@MachineNumber='+CAST (@MachineNumber AS nvarchar)
	             +';@SessionId='+CAST (@SessionId AS nvarchar)
	             +';@AmountPlayed='+CAST (@AmountPlayed AS nvarchar)
	             +';@AmountWon='+CAST (@AmountWon AS nvarchar)
	             +';@GamesPlayed='+CAST (@GamesPlayed AS nvarchar)
	             +';@GamesWon='+CAST (@GamesWon AS nvarchar)
	             +';@CreditBalance='+CAST (@CreditBalance AS nvarchar)
	             +';@CurrentJackpot='+CAST (@CurrentJackpot AS nvarchar)
  	           
  	           
    SET @output = 'StatusCode='+CAST (@status_code AS nvarchar)
                 +';StatusText='+@status_text
     	
	  EXECUTE dbo.zsp_Audit 'zsp_SessionEnd'
                        , @AccountID
                        , @VendorId 
                        , @SerialNumber
                        , @MachineNumber
                        , @SessionId
                        , @status_code
                        , @CreditBalance
                        , @input
                        , @output
                      
  COMMIT TRAN
	-- End Audit
	
  SELECT @status_code AS StatusCode, @status_text AS StatusText

END -- zsp_SessionEnd

GO   
--------------------------------------------------------------------------------
-- Copyright � 2010 Win Systems International
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: SessionStart.sql
-- 
--   DESCRIPTION: Start Card procedure
-- 
--        AUTHOR: Miquel Beltran
-- 
-- CREATION DATE: 28-APR-2010
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 28-APR-2010 MBF    First release.
-- 17-JUN-2010 MBF    Audit
--------------------------------------------------------------------------------

USE [wgdb_000]
GO

--------------------------------------------------------------------------------
-- PURPOSE: Start play session
-- 
--  PARAMS:
--      - INPUT:
--         @AccountID 	     varchar(24)
--         @VendorId 	     varchar(16)
--         @SerialNumber    varchar(30)
--         @MachineNumber   int
--         @CurrentJackpot  money = 0
--         @VendorSessionID bigint = 0
--
--      - OUTPUT:
--
-- RETURNS:
--      StatusCode
--      StatusText
--      SessionID
--      Balance
--
--   NOTES:
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[zsp_SessionStart]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[zsp_SessionStart]
GO
CREATE PROCEDURE [dbo].[zsp_SessionStart]
	@AccountID 	    varchar(24),
	@VendorId 	      varchar(16),
	@SerialNumber    varchar(30),
	@MachineNumber   int,
	@CurrentJackpot  money = 0,
	@VendorSessionID bigint = 0
AS
BEGIN

  BEGIN TRAN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
		
  DECLARE @status_code         int
	DECLARE @status_text         varchar (254)
	DECLARE @session_id          bigint
	DECLARE @balance             money
	DECLARE @terminal_id         bigint
	DECLARE @account_id          bigint
	DECLARE @is_promotion        int
	DECLARE @promo_balance       money
	DECLARE @account_terminal_id bigint
	DECLARE @current_session_id  bigint
	DECLARE @movement_type       int
	DECLARE @cash_in_while_playing money
    -- 09-AUG-2010, Return the welcome message with the customer name
    DECLARE @holder_name         nvarchar (50)
	
    SET @status_code        = 0
	SET @status_text        = 'Bienvenido'
	SET @session_id         = 0
	SET @balance            = 0
	SET @current_session_id = 0

	SELECT @terminal_id = dbo.GetTerminalID(@VendorId, @SerialNumber, @MachineNumber) 
		
  IF (@terminal_id = 0)
  BEGIN
    EXECUTE dbo.InsertTerminal 0, @VendorId, @SerialNumber, @MachineNumber
    SET @status_code = 3
    SET @status_text = 'Invalid Machine Information'
	  COMMIT TRAN
    BEGIN TRAN
    GOTO ERROR_PROCEDURE
  END
  
	IF ((SELECT dbo.CheckTerminalBlocked(@terminal_ID)) = 1)
  BEGIN
    SET @status_code = 3
    SET @status_text = 'Invalid Machine Information'
    GOTO ERROR_PROCEDURE
  END

	SELECT @account_id = dbo.GetAccountID(@AccountID)
  
  IF (@account_ID = 0)
  BEGIN
    SET @status_code = 1
    SET @status_text = 'Invalid Account Number'
    GOTO ERROR_PROCEDURE
  END
	
	IF ((SELECT dbo.CheckAccountBlocked(@account_ID)) = 1)
  BEGIN
    SET @status_code = 4
    SET @status_text = 'Access Denied'
    GOTO ERROR_PROCEDURE
  END
	
	SELECT @is_promotion        = CASE WHEN ( (AC_PROMO_CREATION <= GETDATE()) AND (GETDATE() < AC_PROMO_EXPIRATION) AND (AC_PROMO_BALANCE >0) ) THEN 1 ELSE 0 END
       , @promo_balance       = AC_PROMO_BALANCE 
       , @balance             = AC_BALANCE 
       , @current_session_id  = ISNULL(AC_CURRENT_PLAY_SESSION_ID , 0 ) 
       , @account_terminal_id = ISNULL(AC_CURRENT_TERMINAL_ID , 0 )
       -- 09-AUG-2010, Return the welcome message with the customer name
       , @holder_name         = ISNULL(AC_HOLDER_NAME, ' ')
       , @cash_in_while_playing = ISNULL(AC_CASHIN_WHILE_PLAYING, 0)
  FROM   ACCOUNTS                   
  WHERE ( AC_ACCOUNT_ID = @account_ID )
	
	IF (@is_promotion = 1)
  BEGIN
    SET @balance = @promo_balance
  END
	
	
	IF (@current_session_id <> 0)
	BEGIN
	  IF ( @account_terminal_id <> @terminal_id )
	  BEGIN
	    SET @status_code = 2
      SET @status_text = 'Account Locked (In Session)' 
      GOTO ERROR_PROCEDURE
	  END

    IF (@cash_in_while_playing = 0)
    BEGIN
	    SET @status_code = 2
      SET @status_text = 'Account Locked (In Session)' 
      GOTO ERROR_PROCEDURE
	  END

    -- Close account session
    EXECUTE dbo.CloseAccountSession @account_id
  
	END





  DECLARE @insert_ps_table TABLE (StatusCode int, StatusText nvarchar(254), SessionID bigint)
  INSERT INTO @insert_ps_table EXECUTE  dbo.InsertPlaySession @terminal_id, @account_id, @balance, @is_promotion    
  SELECT @status_code = StatusCode
       , @status_text = StatusText
       , @session_id  = SessionID
    FROM @insert_ps_table
  
  IF ( @status_code <> 0 )
    GOTO ERROR_PROCEDURE
   
  EXECUTE dbo.InsertGameMeters @terminal_id
	
	SET @movement_type = CASE WHEN (@is_promotion = 1) THEN 24 ELSE 5 END
	
	EXECUTE dbo.InsertMovement @session_id
                           , @account_id
                           , @terminal_id
                           , @movement_type
                           , @balance
                           , 0
                           , 0
                           , @balance
	
	

	
    -- 09-AUG-2010, Return the welcome message with the customer name
    SET @status_text = 'Bienvenido ' + @holder_name
	--    IF (@is_promotion = 1)
	--    BEGIN
	--       SET @status_text = @status_text + ' --- NO REDIMIBLE ---'
	--    END

    -- AJQ 17-SEP-2010, Reset CashinWhilePlaying
    UPDATE ACCOUNTS
       SET AC_CASHIN_WHILE_PLAYING    = NULL
     WHERE AC_ACCOUNT_ID              = @account_id
       AND AC_CURRENT_TERMINAL_ID     = @terminal_id
       AND AC_CURRENT_PLAY_SESSION_ID = @session_id 
	 
	COMMIT TRAN
	GOTO OK  
	  
	ERROR_PROCEDURE:
	  ROLLBACK TRAN
	  
	OK:  
	
	-- Audit MBF 17-JUN-2010
	BEGIN TRAN
	
    DECLARE @input AS nvarchar(MAX)
    DECLARE @output AS nvarchar(MAX)	
  	
	  SET @input = '@AccountID='+@AccountID
	             +';@VendorId='+@VendorId
	             +';@SerialNumber='+CAST (@SerialNumber AS nvarchar)
	             +';@MachineNumber='+CAST (@MachineNumber AS nvarchar)
	             +';@CurrentJackpot='+CAST (@CurrentJackpot AS nvarchar)
	             +';@VendorSessionID='+CAST (@VendorSessionID AS nvarchar)
  	           
  	           
    SET @output = 'StatusCode='+CAST (@status_code AS nvarchar)
                 +';StatusText='+@status_text
                 +';SessionID='+CAST(@session_id AS nvarchar)
                 +';Balance='+CAST(@balance AS nvarchar)
     	
	  EXECUTE dbo.zsp_Audit 'zsp_SessionStart'
                        , @AccountID
                        , @VendorId 
                        , @SerialNumber
                        , @MachineNumber
                        , @session_id
                        , @status_code
                        , @balance
                        , @input
                        , @output        
  COMMIT TRAN
	-- End Audit
	
	SELECT @status_code AS StatusCode, @status_text AS StatusText, @session_id AS SessionId, @balance AS Balance

END -- zsp_SessionStart

GO   
--------------------------------------------------------------------------------
-- Copyright � 2010 Win Systems International
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: SessionUpdate.sql
-- 
--   DESCRIPTION: Update Session procedure
-- 
--        AUTHOR: Miquel Beltran
-- 
-- CREATION DATE: 30-APR-2010
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 30-APR-2010 MBF    First release.
-- 17-JUN-2010 MBF    Audit
--------------------------------------------------------------------------------

USE [wgdb_000]
GO

--------------------------------------------------------------------------------
-- PURPOSE: Update play session
-- 
--  PARAMS:
--      - INPUT:
--          @AccountId      varchar(24)
--          @VendorId       varchar(16)
--          @SerialNumber   varchar(30)
--          @MachineNumber  int
--          @SessionId      bigint
--          @AmountPlayed   money
--          @AmountWon      money
--          @GamesPlayed    int
--          @GamesWon       int
--          @CreditBalance  money,
--          @CurrentJackpot money = 0
--
--      - OUTPUT:
--
-- RETURNS:
--      StatusCode
--      StatusText
--
--   NOTES:
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[zsp_SessionUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[zsp_SessionUpdate]
GO
CREATE PROCEDURE [dbo].[zsp_SessionUpdate]
  @AccountId      varchar(24),
  @VendorId       varchar(16),
  @SerialNumber   varchar(30),
  @MachineNumber  int,
  @SessionId      bigint,
  @AmountPlayed   money,
  @AmountWon      money,
  @GamesPlayed    int,
  @GamesWon       int,
  @CreditBalance  money,
  @CurrentJackpot money = 0
AS
BEGIN

  BEGIN TRAN

  -- SET NOCOUNT ON added to prevent extra result sets from
  -- interfering with SELECT statements.
  SET NOCOUNT ON;
	
  DECLARE @status_code         int
  DECLARE @status_text         varchar (254)
  DECLARE @terminal_id         int
  DECLARE @account_id          bigint
  DECLARE @previous_balance    money
  DECLARE @delta_amount_played money
  DECLARE @delta_amount_won    money
  DECLARE @ignore_session_id   int
  DECLARE @dummy               int

  SET @status_code         = 0
  SET @status_text         = 'Successful Session Update'
  SET @delta_amount_played = 0
  SET @delta_amount_won    = 0
  SET @dummy               = 0

  SELECT @terminal_id = dbo.GetTerminalID(@VendorId, @SerialNumber, @MachineNumber) 
		
  IF (@terminal_id = 0)
  BEGIN
    SET @status_code = 2
    SET @status_text = 'Invalid Machine Information'
    GOTO ERROR_PROCEDURE
  END

  SELECT @account_id = dbo.GetAccountID(@AccountId)

  IF (@account_id = 0)
  BEGIN
    SET @status_code = 1
    SET @status_text = 'Invalid Account Number'
    GOTO ERROR_PROCEDURE
  END
  
  SELECT @ignore_session_id = CAST(GP_KEY_VALUE AS int)
    FROM GENERAL_PARAMS 
   WHERE GP_GROUP_KEY = 'Interface3GS' AND GP_SUBJECT_KEY ='IgnoreSessionID'
  
  IF ( @ignore_session_id = 1 )
  BEGIN
    SELECT @SessionId = ISNULL(AC_CURRENT_PLAY_SESSION_ID , AC_LAST_PLAY_SESSION_ID ) 
      FROM   ACCOUNTS                   
     WHERE ( AC_ACCOUNT_ID = @account_id )
  END

  EXECUTE dbo.SessionUpdate_Internal @account_id, @terminal_id, @SerialNumber, @SessionId,
                                     @AmountPlayed, @AmountWon, @GamesPlayed, @GamesWon,
                                     @CreditBalance, @CurrentJackpot, @dummy, 
                                     @delta_amount_played OUTPUT, @delta_amount_won OUTPUT,
                                     @status_code OUTPUT, @status_text OUTPUT
  IF ( @status_code <> 0 )
    GOTO ERROR_PROCEDURE

  
  -- Insert Movement
  
  -- Use delta values to calculate initial balance
  --SET @previous_balance = @CreditBalance + @AmountPlayed - @AmountWon
  SET @previous_balance = @CreditBalance + @delta_amount_played - @delta_amount_won
  
  EXECUTE dbo.InsertMovement @SessionId, @account_id, @terminal_id, 0, @previous_balance, @delta_amount_played, @delta_amount_won, @CreditBalance

  SET @status_text = 'Successful Session Update'
	
  COMMIT TRAN
	GOTO OK  
	  
ERROR_PROCEDURE:
  ROLLBACK TRAN
	  
OK: 

	-- Audit MBF 17-JUN-2010
	BEGIN TRAN
	
    DECLARE @input AS nvarchar(MAX)
    DECLARE @output AS nvarchar(MAX)	
  		  	
	  SET @input = '@AccountID='+@AccountID
	             +';@VendorId='+@VendorId
	             +';@SerialNumber='+CAST (@SerialNumber AS nvarchar)
	             +';@MachineNumber='+CAST (@MachineNumber AS nvarchar)
	             +';@SessionId='+CAST (@SessionId AS nvarchar)
	             +';@AmountPlayed='+CAST (@AmountPlayed AS nvarchar)
	             +';@AmountWon='+CAST (@AmountWon AS nvarchar)
	             +';@GamesPlayed='+CAST (@GamesPlayed AS nvarchar)
	             +';@GamesWon='+CAST (@GamesWon AS nvarchar)
	             +';@CreditBalance='+CAST (@CreditBalance AS nvarchar)
	             +';@CurrentJackpot='+CAST (@CurrentJackpot AS nvarchar)
  	           
  	           
    SET @output = 'StatusCode='+CAST (@status_code AS nvarchar)
                 +';StatusText='+@status_text
     	
	  EXECUTE dbo.zsp_Audit 'zsp_SessionUpdate'
                        , @AccountID
                        , @VendorId 
                        , @SerialNumber
                        , @MachineNumber
                        , @SessionId
                        , @status_code
                        , @CreditBalance
                        , @input
                        , @output
                      
  COMMIT TRAN
	-- End Audit

  SELECT @status_code AS StatusCode, @status_text AS StatusText

END -- zsp_SessionUpdate

GO   
USE [master]
GO
/*****************CLR*ENABLED**************/
sp_configure 'clr enabled', 1;
GO
reconfigure
GO

/*****************DROP*******************/
USE [wgdb_000]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TrackDataToExternal]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[TrackDataToExternal]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TrackDataToInternal]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[TrackDataToInternal]
GO

IF  EXISTS (SELECT * FROM sys.assemblies asms WHERE asms.name = N'SQLUtilities')
DROP ASSEMBLY [SQLUtilities]

/*****************CREATE*****************/
USE [wgdb_000]
GO
CREATE ASSEMBLY [SQLUtilities]
AUTHORIZATION [wgroot]
FROM 0x4D5A90000300000004000000FFFF0000B800000000000000400000000000000000000000000000000000000000000000000000000000000000000000800000000E1FBA0E00B409CD21B8014CCD21546869732070726F6772616D2063616E6E6F742062652072756E20696E20444F53206D6F64652E0D0D0A2400000000000000504500004C010300ABBDE24B0000000000000000E00002210B0108000030000000200000000000009E4C000000200000006000000000400000200000001000000400000000000000040000000000000000A000000010000000000000030040850000100000100000000010000010000000000000100000000000000000000000484C000053000000006000002803000000000000000000000000000000000000008000000C000000C84B00001C0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000200000080000000000000000000000082000004800000000000000000000002E74657874000000A42C0000002000000030000000100000000000000000000000000000200000602E7273726300000028030000006000000010000000400000000000000000000000000000400000402E72656C6F6300000C00000000800000001000000050000000000000000000000000000040000042000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000804C0000000000004800000002000500302E0000981D00000100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000133003002C0000000100001100022C0B026F1100000A16FE022B01160D092D0872010000700C2B0E1200021628070000060B060C2B00082A13300300380000000200001100022C0B026F1100000A16FE022B01160D092D0872010000700C2B1A021200120128060000060D092D0872010000700C2B04060C2B00082A1E02281200000A2A0000000023215E48475E2423464453484A76622D133003001A00000003000011001F108D1B00000125D00E000004281400000A0A060B2B00072A000013300300140000000400001100178D1B0000010C0816179C080A060B2B00072A1B300400A001000005000011001E8D1B000001130A0372010000705104165400026F1100000A1F0DFE0416FE01131311132D0900161312DD6C010000732A0000060A061F406F1600000A0006280400000628050000066F1700000A130D156A13141214281800000A130C02110C281900000A16FE0216FE01131311132D0900161312DD2101000002281A00000A13071107281B00000A130E110E731C00000A130F110F110D16731D00000A13101110110A161E6F1E00000A26110A16281F00000A13081208281800000A1000021F141F306F2000000A10000216176F2100000A0B02171A6F2100000A0C021B1F0D6F2100000A0D021F12186F2100000A1304077203000070282200000A16FE01131311132D0900161312DD8C000000161305091104282300000A281A00000A13091109281B00000A130B1613112B12001105110B111191581305001111175813111111110B8E69FE04131311132DE0110520F52600005E1305720700007011058C28000001282400000A1306081106282200000A16FE01131311132D0600161312DE18030951041104282500000A54171312DE072600161312DE000011122A411C00000000000013000000820100009501000007000000010000011B3004004D01000006000011000272010000705100036F1100000A1F0DFE01130E110E2D090016130DDD270100000416FE01130E110E2D090016130DDD14010000732A0000060A061F406F1600000A0006280400000628050000066F2600000A1307732700000A13081108110717731D00000A1309037215000070048C29000001282400000A282300000A281A00000A13041104281B00000A1306160D16130A2B1000091106110A91580D00110A1758130A110A11068E69FE04130E110E2DE20920F52600005E0D7207000070098C28000001282400000A0C08037215000070048C29000001282400000A282800000A0B07281A00000A13051105281B00000A130B1109110B16110B8E696F2900000A0011096F2A00000A0011086F2B00000A130C0272010000705102110C16281F00000A130F120F281800000A510202501F141F306F2000000A5117130DDE07260016130DDE0000110D2A000000411C000000000000080000003A010000420100000700000001000001133003002B00000007000011007207000070028C29000001282400000A7223000070038C2A000001282400000A282300000A0A2B00062A7E000302161A6F2100000A282C00000A5404021B6F2D00000A282E00000A552A00133003002200000008000011000403510217FE0116FE010C082D0D000304120028060000060B2B04170B2B00072A1E02281200000A2AEE02281200000A000002036F2F00000A740100001B7D0500000402038E697D060000040220000100008D1B0000017D0700000402281300000600002A000013300100070000000900001100170A2B00062A0013300100070000000900001100170A2B00062A0013300100070000000A00001100170A2B00062A0013300100070000000A00001100170A2B00062A00133005005C0100000B00001100027B0A00000416FE01130411042D120002283000000A6F3100000A733200000A7A0314FE0116FE01130411042D110072310000707249000070733300000A7A0E0414FE0116FE01130411042D1100729300007072AD000070733300000A7A0416321C0E05163217040558038E69300F0E0505580E048E69FE0216FE012B0116130411042D0C0072F9000070733400000A7A0405580C38AE0000000002027B08000004175820000100005DD27D0800000402027B09000004027B07000004027B08000004915820000100005DD27D09000004027B07000004027B08000004910B027B07000004027B08000004027B07000004027B09000004919C027B07000004027B09000004079C027B07000004027B0800000491027B07000004027B09000004915820000100005DD20A0E040E05030491027B07000004069161D29C0004175810020E05175810050408FE04130411043A45FFFFFF050D2B00092A13300600400000000C00001100027B0A00000416FE010C082D120002283000000A6F3100000A733200000A7A058D1B0000010A02030405061628110000062602281300000600060B2B00072A13300400910000000D00001100160B2B1000027B070000040707D29C000717580B07027B070000048E69FE040D092DE102167D0800000402167D09000004160C160B2B490008027B07000004079158027B0500000407027B060000045D915820000100005D0C027B0700000407910A027B0700000407027B0700000408919C027B0700000408069C000717580B07027B070000048E69FE040D092DA82A00000013300300540000000900001100027B0A0000040A062D4800027B0500000416027B050000048E69283500000A00027B0700000416027B070000048E69283500000A0002167D0800000402167D0900000402177D0A00000402283600000A00002A4A02283700000A0000021F407D3800000A002A0013300100070000000A000011001E0A2B00062A0013300200160000000900001100031EFE010A062D0C007249010070733900000A7A2A000013300100070000000A00001100733A00000A7A1E00733A00000A7A00133001000C0000000E00001100178D1B0000010A2B00062A13300200210000000900001100032C0B038E6917FE0216FE012B01170A062D0C007291010070733900000A7A2A00000013300600190000000F00001100178D060000010B07161E1E16733B00000AA2070A2B00062A000000133006001D0000000F00001100178D060000010B07161E20000800001E733B00000AA2070A2B00062A00000013300100070000001000001100190A2B00062A00133002001600000009000011000319FE010A062D0C0072C5010070733900000A7A2A000013300100070000001100001100170A2B00062A00133002001600000009000011000317FE010A062D0C007205020070733900000A7A2A0A002A00000013300200260000001200001100733C00000A0A026F3D00000A1E5B8D1B0000010B06076F3E00000A0002076F3F00000A002A000013300100100000001300001100724D02007028250000060A2B00062A133003005B00000014000011000214FE0116FE010B072D1100725D020070726D020070733300000A7A02729D02007019284000000A16FE010B072D0900732A0000060A2B2002724D02007019284000000A16FE010B072D090073260000060A2B04140A2B00062A2A0228150000060000002A0000133003008B0000001500001100027B0B00000416FE010B072D120002283000000A6F3100000A733200000A7A0314FE0116FE010B072D110072A502007072B3020070733300000A7A038E692C0F038E692000010000FE0216FE012B01160B072D0C0072F3020070733900000A7A042C0B048E6917FE0216FE012B01170B072D0C007237030070733900000A7A03730C0000060A2B00062A00133003000E00000016000011000203046F1700000A0A2B00062A46000203284100000A0002177D0B0000042A5602281500000600000273260000067D0C000004002A000013300100110000000A00001100027B0C0000046F4200000A0A2B00062A00000013300200230000000900001100031EFE010A062D0C007279030070733900000A7A027B0C000004036F4300000A002A0013300100110000000A00001100027B0C0000046F4400000A0A2B00062A3E00027B0C000004036F4500000A002A00000013300100110000000E00001100027B0C0000046F4600000A0A2B00062A3E00027B0C000004036F4700000A002A00000013300100110000000E00001100027B0C0000046F4800000A0A2B00062A3E00027B0C000004036F3F00000A002A00000013300100110000000A00001100027B0C0000046F3D00000A0A2B00062A3E00027B0C000004036F1600000A002A00000013300100110000001700001100027B0C0000046F4900000A0A2B00062A00000013300100110000001700001100027B0C0000046F4A00000A0A2B00062A00000013300100110000001000001100027B0C0000046F4B00000A0A2B00062A3E00027B0C000004036F4C00000A002A00000013300100110000001100001100027B0C0000046F4D00000A0A2B00062A3E00027B0C000004036F4E00000A002A3A00027B0C0000046F4F00000A002A3A00027B0C0000046F5000000A002A0013300300970000001500001100027B0D00000416FE010B072D170002283000000A6F3100000A72B5030070735100000A7A0314FE0116FE010B072D110072E703007072F5030070733300000A7A038E692C0F038E692000010000FE0216FE012B01160B072D0C007235040070733900000A7A042C0B048E6917FE0216FE012B01170B072D0C007279040070733900000A7A027B0C00000403046F1700000A0A2B00062A00133003000E00000016000011000203046F1700000A0A2B00062A0000133002003E0000000900001100027B0D0000040A062D320002177D0D000004027B0C00000414FE010A062D1500027B0C0000046F5200000A0002147D0C0000040002283600000A00002A000042534A4201000100000000000C00000076322E302E35303732370000000005006C0000004C0B0000237E0000B80B0000240A000023537472696E677300000000DC150000BC04000023555300981A0000100000002347554944000000A81A0000F002000023426C6F620000000000000002000001579FA2290902000000FA01330016000001000000350000000A0000000E0000003F0000002F000000020000005200000003000000100000000100000017000000030000001400000020000000010000000100000001000000020000000200000000000A0001000000000006008E0087000600950087000600B7009A000600C80087000600D4009A00060006039A00060035039A00060052039A0006005A0448040600770448040600AF0490040600BD0490040600D10448040600EA04480406000505480406002005480406003905480406005205480406007105480406008E0548040600B805A5055700CC0500000600FB05DB0506001B06DB050A006D0652060600900687000600A20687000600EC06DB0506000707870006004207DB050600510787000600570787000600810790040600970787000600B50787000600D507CB070600E2079A000600EF07CB070600F6079A0006003C08870006004A08870006007E0887000600840887000600FD08870006001709870006002F09870006004509870006006709870006008E099A000600A50987000600BB099A000600D4099A000600F20987000000000001000000000001000100010010001B00000005000100010001001000250000000500010004000201000030000000090002000C00000110003A000000050005000C00810010005200000015000B001500010110005600000018000B002600010110006500000018000C002A0000000000A706000005000E004000130100001107000075000F00400051800301130006069101130056809901490056809E0149000100A20157000100B00113000100BB0157000100CC015B000100D3015B000100DA015E000100D9035E0001000504DF000100DA015E0013012E07FC005020000000009600E7000A0001008820000000009600F2000A000200CC20000000008618FD000F000300E82000000000910013011B00030010210000000091001A011B0003003021000000009600200120000300F82200000000960036012900060070240000000096004C0131000900A724000000009600660137000B00C824000000009600800140000E00F624000000008618FD000F001100FE24000000008618FD00610011003C2500000000E609E20167001200502500000000E609F80167001200642500000000E60917026B001200782500000000E6092A026B0012008C2500000000E6013E026F001200F42600000000E6014D027A001700402700000000810061020F001A00E02700000000E60166020F001A004028000000008418FD000F001A00542800000000C608BA026B001A00682800000000C608C8028B001A008C2800000000C608D6026B001B009F2800000000C608E7028B001B00A82800000000C608F80290001C00C02800000000C608FF0261001C00F02800000000C6080F0395001D00182900000000C608230395001D00442900000000C60840039B001D00582900000000C6084903A0001D007C2900000000C6085E03A6001E00902900000000C6086A03AB001E00B22900000000C60076030F001F00B82900000000C60081030F001F00EC290000000096008D03B1001F00082A0000000096008D03B6001F006F2A000000008618FD000F0020007C2A00000000C600E503D1002000142B00000000C600F503D10022002E2B00000000C4006602DA002400402B000000008618FD000F002500582B00000000C608BA026B002500782B00000000C608C8028B002500A82B00000000C608D6026B002600C52B00000000C608E7028B002600D82B00000000C608F80290002700F52B00000000C608FF0261002700082C00000000C608140490002800252C00000000C6081C0461002800382C00000000C60824046B002900552C00000000C60830048B002900682C00000000C6080F0395002A00882C00000000C608230395002A00A82C00000000C60840039B002A00C52C00000000C6084903A0002A00D82C00000000C6085E03A6002B00F52C00000000C6086A03AB002B00052D00000000C60076030F002C00142D00000000C60081030F002C00242D00000000C600E503D1002C00C82D00000000C600F503D1002E00E42D00000000810066020F003000000001008206000001008206000001001B00020002007A07020003008E07020001001B00000002007A07000003008E07000001006E08000002007508000001002500020002006E0802000300750800000100CC03000002009C0802000300AE08000001003C0400000100C00800000200CC0800000300D80800000400E30800000500F00800000100C00800000200CC0800000300D80800000100880900000100880900000100880900000100880900000100880900000100EA09000001000A0A00000200110A000001000A0A00000200110A00000100170A000001008809000001008809000001008809000001008809000001008809000001008809000001008809000001000A0A00000200110A000001000A0A00000200110A05000D00050011004900FD00E3005100FD00E3005900FD00E3006100FD00DA006900FD00E3007100FD00E3007900FD00E3008100FD00E3008900FD00E3009100FD00E3009900FD00E300A100FD00E300A900FD00E800B900FD008B00C100FD000F00C900FD000F00D10097066B000900FD000F00E100FD000F00F1006A0700010901FD000F00290030048B002900E503D10011019E071901D100A7071D011101AF0723011901C20728012101FD0061002901FD002E01310107083A0119010C084201D10015084901D1001D084F01D10027085501D10035085B01D100430861014901AF0767012900F503D1002101FD000F00D10035088D01310150089401290156080F0021016608900059018C086701D1001D08BC0159019408C101F900BA08CC0109000209DB0161010A0919016901FD00E3007101FD00E1017901FD00E300F9006109FE0181016A0906022900FD000F0029007B0913008901FD00E3009101FD000F003100FD0010029901FD000F00290024046B00A101C207610029001C046100D100030A370229006602DA002900BA026B002900C8028B002900D6026B002900E7028B002900F80290002900FF02610029001404900029000F039500290023039500290040039B0029004903A00029005E03A60029006A03AB00290076030F00290081030F006901FD00E101290061090F0008000400160008000C004D000800100052002000830052002E003B0094022E004300AC022E000B0057022E001B0064022E0023008E022E0033008E022E005B008E022E007B00D1022E004B008E022E0053008E022E006300AC022E006B00BF022E007300C80240008300520023019B0052000100100000000A00EE00F500090110016C019C01B801C601D301D701E701EF01F7010B021702200225022A023202400246024C025102050001000600050008000C0000006E02830000008002830000009B0287000000AA02870000009403870000009E0387000000AB03BC000000AE03C1000000BE03C1000000CC03C7000000D103CC0000009403870000009E0387000000AB03BC0000003C04BC000000400487000000AE03C1000000BE03C1000000CC03C7000000D103CC0002000D00030002000E00050002000F000700020010000900020016000B00010017000B00020018000D00010019000D0002001A000F0001001B000F0002001C00110002001D00130002001E00150001001F00150001002100170002002000170002002B00190001002C00190002002D001B0001002E001B0002002F001D00010030001D00010032001F00020031001F0002003300210001003400210002003500230002003600250002003700270001003800270002003900290001003A002900D001D82000000E000480000001000000000000000000000000003906000002000000000000000000000001007E00000000000200000000000000000000000100460600000000040003000A0009000000003C4D6F64756C653E0053514C5574696C69746965732E646C6C00547261636B4461746100436172644E756D6265720043726970744D6F646500415243466F75724D616E616765645472616E73666F726D0052433400415243466F75724D616E616765640052433443727970746F5365727669636550726F7669646572006D73636F726C69620053797374656D004F626A65637400456E756D0053797374656D2E53656375726974792E43727970746F677261706879004943727970746F5472616E73666F726D0049446973706F7361626C650053796D6D6574726963416C676F726974686D00546F45787465726E616C00546F496E7465726E616C002E63746F7200434845434B53554D5F4D4F44554C45004765744B657900476574495600547261636B44617461546F436172644E756D62657200436172644E756D626572546F547261636B4461746100476574436172644E756D62657246726F6D436172644461746100476574436172644461746146726F6D436172644E756D62657200436F6E76657274547261636B446174610076616C75655F5F004E4F4E4500574350007472616E73666F726D5F6B6579006B65795F6C656E67746800646174615F7065726D75746174696F6E00696E6465783100696E6465783200646973706F7365006765745F43616E52657573655472616E73666F726D006765745F43616E5472616E73666F726D4D756C7469706C65426C6F636B73006765745F496E707574426C6F636B53697A65006765745F4F7574707574426C6F636B53697A65005472616E73666F726D426C6F636B005472616E73666F726D46696E616C426C6F636B00496E697400446973706F73650043616E52657573655472616E73666F726D0043616E5472616E73666F726D4D756C7469706C65426C6F636B7300496E707574426C6F636B53697A65004F7574707574426C6F636B53697A65006765745F426C6F636B53697A65007365745F426C6F636B53697A65006765745F466565646261636B53697A65007365745F466565646261636B53697A65006765745F4956007365745F4956004B657953697A6573006765745F4C6567616C426C6F636B53697A6573006765745F4C6567616C4B657953697A6573004369706865724D6F6465006765745F4D6F6465007365745F4D6F64650050616464696E674D6F6465006765745F50616464696E67007365745F50616464696E670047656E657261746549560047656E65726174654B65790043726561746500426C6F636B53697A6500466565646261636B53697A65004956004C6567616C426C6F636B53697A6573004C6567616C4B657953697A6573004D6F64650050616464696E670069735F646973706F73656400437265617465446563727970746F7200437265617465456E63727970746F7200617263666F75726D616E61676564006765745F4B6579007365745F4B6579006765745F4B657953697A65007365745F4B657953697A65004B6579004B657953697A650053797374656D2E5265666C656374696F6E00417373656D626C7946696C6556657273696F6E41747472696275746500417373656D626C7956657273696F6E4174747269627574650053797374656D2E52756E74696D652E496E7465726F705365727669636573004775696441747472696275746500436F6D56697369626C6541747472696275746500417373656D626C7943756C7475726541747472696275746500417373656D626C7954726164656D61726B41747472696275746500417373656D626C79436F7079726967687441747472696275746500417373656D626C7950726F6475637441747472696275746500417373656D626C79436F6D70616E7941747472696275746500417373656D626C79436F6E66696775726174696F6E41747472696275746500417373656D626C794465736372697074696F6E41747472696275746500417373656D626C795469746C654174747269627574650053797374656D2E446961676E6F73746963730044656275676761626C6541747472696275746500446562756767696E674D6F6465730053797374656D2E52756E74696D652E436F6D70696C6572536572766963657300436F6D70696C6174696F6E52656C61786174696F6E734174747269627574650052756E74696D65436F6D7061746962696C6974794174747269627574650053514C5574696C69746965730053797374656D2E44617461004D6963726F736F66742E53716C5365727665722E5365727665720053716C46756E6374696F6E4174747269627574650043617264547261636B4461746100537472696E67006765745F4C656E6774680042797465003C50726976617465496D706C656D656E746174696F6E44657461696C733E7B44364133463942342D363137392D343237352D413635432D4233344645333737353633387D00436F6D70696C657247656E6572617465644174747269627574650056616C756554797065005F5F5374617469634172726179496E69745479706553697A653D31360024246D6574686F643078363030303030342D310052756E74696D6548656C706572730041727261790052756E74696D654669656C6448616E646C6500496E697469616C697A65417272617900436172644964004F75744174747269627574650043617264547970650055496E74363400546F537472696E6700436F6D7061726500506172736500426974436F6E7665727465720047657442797465730053797374656D2E494F004D656D6F727953747265616D0043727970746F53747265616D0053747265616D0043727970746F53747265616D4D6F6465005265616400546F55496E743634005061644C65667400537562737472696E67006F705F496E657175616C69747900436F6E6361740055496E74333200466F726D617400496E74333200577269746500466C75736846696E616C426C6F636B00546F4172726179005369746549640053657175656E636500496E74363400436F6E7665727400546F496E74333200546F496E743634005265636569766564547261636B44617461004442547261636B4461746100436C6F6E6500496E70757442756666657200496E7075744F666673657400496E707574436F756E74004F7574707574427566666572004F75747075744F666673657400547970650047657454797065006765745F46756C6C4E616D65004F626A656374446973706F736564457863657074696F6E00417267756D656E744E756C6C457863657074696F6E00417267756D656E744F75744F6652616E6765457863657074696F6E00436C65617200474300537570707265737346696E616C697A65004B657953697A6556616C75650076616C75650043727970746F67726170686963457863657074696F6E004E6F74537570706F72746564457863657074696F6E00524E4743727970746F5365727669636550726F76696465720052616E646F6D4E756D62657247656E657261746F7200416C674E616D6500537472696E67436F6D70617269736F6E00457175616C73005267624B657900526762495600446973706F73696E6700000000000100033000000D7B0030003A00440034007D00000D7B0030003A00440032007D00000D7B0030003A00440039007D00001769006E0070007500740042007500660066006500720000495400720061006E00730066006F0072006D0042006C006F0063006B003A00200049006E00700075007400200062007500660066006500720020006900730020006E0075006C006C0000196F0075007400700075007400420075006600660065007200004B5400720061006E00730066006F0072006D0042006C006F0063006B003A0020004F0075007400700075007400200062007500660066006500720020006900730020006E0075006C006C00004F5400720061006E00730066006F0072006D0042006C006F0063006B003A00200050006100720061006D006500740065007200730020006F007500740020006F0066002000720061006E0067006500004742006C006F0063006B00530069007A0065003A0020004500720072006F007200200049006E00760061006C0069006400200042006C006F0063006B002000530069007A0065000033490056003A0020004500720072006F007200200049006E00760061006C00690064002000490056002000530069007A006500003F4D006F00640065003A0020004500720072006F007200200049006E00760061006C0069006400200043006900700068006500720020004D006F00640065000047500061006400640069006E0067003A0020004500720072006F007200200049006E00760061006C00690064002000500061006400640069006E00670020004D006F0064006500000F41005200430046004F0055005200000F61006C0067004E0061006D006500002F4300720065006100740065003A0020004500720072006F0072005F0050006100720061006D004E0075006C006C000007520043003400000D5200670062004B0065007900003F43007200650061007400650044006500630072007900700074006F0072003A0020005200670062006B006500790020006900730020006E0075006C006C00004343007200650061007400650044006500630072007900700074006F0072003A00200049006E00760061006C006900640020006B00650079002000730069007A006500004143007200650061007400650044006500630072007900700074006F0072003A00200049006E00760061006C00690064002000490056002000730069007A006500003B42006C006F0063006B00530069007A0065003A00200049006E00760061006C0069006400200062006C006F0063006B002000730069007A006500003143007200650061007400650044006500630072007900700074006F0072003A00200044006900730070006F0073006500000D7200670062004B0065007900003F43007200650061007400650044006500630072007900700074006F0072003A0020007200620067006B006500790020006900730020006E0075006C006C00004343007200650061007400650044006500630072007900700074006F0072003A00200049006E00760061006C006900640020004B00650079002000530069007A006500004143007200650061007400650044006500630072007900700074006F0072003A00200049006E00760061006C00690064002000490056002000530069007A0065000000B4F9A3D679617542A65CB34FE37756380008B77A5C561934E0890400010E0E0320000102060804F52600000400001D05080003020E100E100807000302100E0E080500020E080A080003010E1008100A0800030211100E100E030611100400000000040100000003061D05020605020602052001011D0503200002032000080A2005081D0508081D05080820031D051D050808032800020328000804200101080420001D050520001D1219042000111D05200101111D0420001121052001011121040000121805000112180E0428001D050528001D1219042800111D0428001121082002120D1D051D0504200101020306121C042001010E0520010111590607040E020E020607040E080E020306112808000201127D1180810607021D051D050807031D051D051D050320000E050002080E0E0400010B0E0500011D050B0B200301128099120D11809D072003081D0508080600020B1D05080520020E08030520020E0808050002020E0E0500020E0E0E0500020E0E1C040001080E20071512200E0E0E0E090E0B0B0B1D051D050E120D1D051280911280950802020B0600030E0E0E0E072003011D0508081B071012200E0E090B0B1D05120D128091128095081D051D0502020B0307010E0420010E080400010A0E0507030802020320001C021D0503070102030701080520001280B1052002010E0E07070505050808020707031D051D05020607040508080207000301127D0808040001011C0407011D05062003010808080807021D12191D1219040701111D04070111210707021280CD1D050407011218080003020E0E1180D5050702121802050702120D02040701120D0507011D12190C010007312E302E302E3000002901002464643435653136382D656230622D346363362D383030342D326336653033326530306334000005010000000017010012436F7079726967687420C2A920203230313000001201000D5753492E43727970746F53514C00000801000701000000000801000800000000001E01000100540216577261704E6F6E457863657074696F6E5468726F77730100000000ABBDE24B000000000200000064000000E44B0000E43B000052534453986C9C43EE8A804CB19D11E3562ECD8701000000433A5C5769676F735C5769676F73204465765C5769676F732053797374656D5C5753492E53514C5574696C69746965735C6F626A5C44656275675C53514C5574696C69746965732E70646200704C000000000000000000008E4C0000002000000000000000000000000000000000000000000000804C000000000000000000000000000000005F436F72446C6C4D61696E006D73636F7265652E646C6C0000000000FF2500204000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000100100000001800008000000000000000000000000000000100010000003000008000000000000000000000000000000100000000004800000058600000D00200000000000000000000D00234000000560053005F00560045005200530049004F004E005F0049004E0046004F0000000000BD04EFFE00000100000001000000000000000100000000003F000000000000000400000002000000000000000000000000000000440000000100560061007200460069006C00650049006E0066006F00000000002400040000005400720061006E0073006C006100740069006F006E00000000000000B00430020000010053007400720069006E006700460069006C00650049006E0066006F0000000C020000010030003000300030003000340062003000000044000E000100460069006C0065004400650073006300720069007000740069006F006E00000000005700530049002E00430072007900700074006F00530051004C000000300008000100460069006C006500560065007200730069006F006E000000000031002E0030002E0030002E003000000044001100010049006E007400650072006E0061006C004E0061006D0065000000530051004C005500740069006C00690074006900650073002E0064006C006C00000000004800120001004C006500670061006C0043006F007000790072006900670068007400000043006F0070007900720069006700680074002000A90020002000320030003100300000004C00110001004F0072006900670069006E0061006C00460069006C0065006E0061006D0065000000530051004C005500740069006C00690074006900650073002E0064006C006C00000000003C000E000100500072006F0064007500630074004E0061006D006500000000005700530049002E00430072007900700074006F00530051004C000000340008000100500072006F006400750063007400560065007200730069006F006E00000031002E0030002E0030002E003000000038000800010041007300730065006D0062006C0079002000560065007200730069006F006E00000031002E0030002E0030002E0030000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000004000000C000000A03C000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
WITH PERMISSION_SET = SAFE

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION TrackDataToExternal(@Input nvarchar(255)) RETURNS nvarchar(255) 
EXTERNAL NAME SQLUtilities.TrackData.ToExternal
GO

CREATE FUNCTION TrackDataToInternal(@Input nvarchar(255)) RETURNS nvarchar(255) 
EXTERNAL NAME SQLUtilities.TrackData.ToInternal
GO

   

USE [wgdb_000]
GO

/****** Object:  User [wg_interface]    Script Date: 05/10/2010 09:33:21 ******/
IF EXISTS (SELECT * FROM sys.server_principals WHERE name = N'wg_interface' AND type in (N'S'))
BEGIN

IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'wg_interface' AND type in (N'S'))
CREATE USER [wg_interface] FOR LOGIN [wg_interface] WITH DEFAULT_SCHEMA=[dbo];

GRANT EXECUTE ON [dbo].[zsp_AccountStatus] TO [wg_interface] WITH GRANT OPTION;
GRANT EXECUTE ON [dbo].[zsp_SendEvent]     TO [wg_interface] WITH GRANT OPTION;
GRANT EXECUTE ON [dbo].[zsp_SessionEnd]    TO [wg_interface] WITH GRANT OPTION;
GRANT EXECUTE ON [dbo].[zsp_SessionStart]  TO [wg_interface] WITH GRANT OPTION;
GRANT EXECUTE ON [dbo].[zsp_SessionUpdate] TO [wg_interface] WITH GRANT OPTION;

EXEC sp_addrolemember 'db_denydatareader','wg_interface';
EXEC sp_addrolemember 'db_denydatawriter','wg_interface';

END
ELSE
/**** VERSION FAILURE SECTION *****/
SELECT 'Nothing to Update for wg_interface.'
GO

/****** Object:  User [EIBE] ******/
IF EXISTS (SELECT * FROM sys.server_principals WHERE name = N'EIBE' AND type in (N'S'))
BEGIN

IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'EIBE' AND type in (N'S'))
CREATE USER [EIBE] FOR LOGIN [EIBE] WITH DEFAULT_SCHEMA=[dbo];

GRANT EXECUTE ON [dbo].[zsp_AccountStatus] TO [EIBE] WITH GRANT OPTION;
GRANT EXECUTE ON [dbo].[zsp_SendEvent]     TO [EIBE] WITH GRANT OPTION;
GRANT EXECUTE ON [dbo].[zsp_SessionEnd]    TO [EIBE] WITH GRANT OPTION;
GRANT EXECUTE ON [dbo].[zsp_SessionStart]  TO [EIBE] WITH GRANT OPTION;
GRANT EXECUTE ON [dbo].[zsp_SessionUpdate] TO [EIBE] WITH GRANT OPTION;

EXEC sp_addrolemember 'db_denydatareader','EIBE';
EXEC sp_addrolemember 'db_denydatawriter','EIBE';

END
ELSE
/**** VERSION FAILURE SECTION *****/
SELECT 'Nothing to Update for EIBE.'
GO

/****** Object:  User [3GS] ******/
IF EXISTS (SELECT * FROM sys.server_principals WHERE name = N'3GS' AND type in (N'S'))
BEGIN

IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'3GS' AND type in (N'S'))
CREATE USER [3GS] FOR LOGIN [3GS] WITH DEFAULT_SCHEMA=[dbo];

GRANT EXECUTE ON [dbo].[zsp_AccountStatus] TO [3GS] WITH GRANT OPTION;
GRANT EXECUTE ON [dbo].[zsp_SendEvent]     TO [3GS] WITH GRANT OPTION;
GRANT EXECUTE ON [dbo].[zsp_SessionEnd]    TO [3GS] WITH GRANT OPTION;
GRANT EXECUTE ON [dbo].[zsp_SessionStart]  TO [3GS] WITH GRANT OPTION;
GRANT EXECUTE ON [dbo].[zsp_SessionUpdate] TO [3GS] WITH GRANT OPTION;

EXEC sp_addrolemember 'db_denydatareader','3GS';
EXEC sp_addrolemember 'db_denydatawriter','3GS';

END
ELSE
/**** VERSION FAILURE SECTION *****/
SELECT 'Nothing to Update for 3GS.'
GO
   
