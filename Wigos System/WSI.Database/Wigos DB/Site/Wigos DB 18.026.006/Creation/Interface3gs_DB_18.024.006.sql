USE [master]
GO

/**** CHECK DATABASE EXISTENCE SECTION *****/
IF (EXISTS (SELECT * FROM sys.databases WHERE name = 'sPOS'))
BEGIN
/**** FAILURE SECTION *****/
SELECT 'Database sPOS already exists.'

raiserror('Not updated', 20, -1) with log
END

/****** Object:  Database [sPOS]    Script Date: 05/18/2010 15:00:53 ******/
CREATE DATABASE [sPOS] ON  PRIMARY 
( NAME = N'sPOS', FILENAME = N'C:\Wigos\Database\sPOS.mdf' , MAXSIZE = UNLIMITED, FILEGROWTH = 10240KB )
 LOG ON 
( NAME = N'sPOS_log', FILENAME = N'C:\Wigos\Database\sPOS_1.ldf' , MAXSIZE = UNLIMITED , FILEGROWTH = 10240KB )
 COLLATE SQL_Latin1_General_CP1_CI_AS
GO
EXEC dbo.sp_dbcmptlevel @dbname=N'sPOS', @new_cmptlevel=90
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [sPOS].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [sPOS] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [sPOS] SET ANSI_NULLS OFF
GO
ALTER DATABASE [sPOS] SET ANSI_PADDING OFF
GO
ALTER DATABASE [sPOS] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [sPOS] SET ARITHABORT OFF
GO
ALTER DATABASE [sPOS] SET AUTO_CLOSE OFF
GO
ALTER DATABASE [sPOS] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [sPOS] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [sPOS] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [sPOS] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [sPOS] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [sPOS] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [sPOS] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [sPOS] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [sPOS] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [sPOS] SET  ENABLE_BROKER
GO
ALTER DATABASE [sPOS] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [sPOS] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [sPOS] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [sPOS] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [sPOS] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [sPOS] SET  READ_WRITE
GO
ALTER DATABASE [sPOS] SET RECOVERY FULL
GO
ALTER DATABASE [sPOS] SET  MULTI_USER
GO
ALTER DATABASE [sPOS] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [sPOS] SET DB_CHAINING OFF
GO
USE [sPOS]
/****** Object:  StoredProcedure [dbo].[zsp_AccountStatus]    Script Date: 05/18/2010 15:00:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[zsp_AccountStatus]
	@pAccountID 	     varchar(24),
	@pVendorId 	     varchar(16),
	@pMachineNumber   int
AS
BEGIN
  EXECUTE wgdb_000.dbo.zsp_AccountStatus  @pAccountID
                                                                   , @pVendorId
                                                                   , @pMachineNumber
END
GO
/****** Object:  StoredProcedure [dbo].[zsp_SendEvent]    Script Date: 05/18/2010 15:00:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[zsp_SendEvent]
	@pAccountID 	     varchar(24),
	@pVendorId 	     varchar(16),
	@pSerialNumber    varchar(30),
	@pMachineNumber   int,
	@pEventID         bigint, 
	@pPayoutAmt       money
AS
BEGIN
  EXECUTE wgdb_000.dbo.zsp_SendEvent  @pAccountID
                                        , @pVendorId
                                        , @pSerialNumber
                                        , @pMachineNumber  
                                        , @pEventID        
                                        , @pPayoutAmt  
END
GO
/****** Object:  StoredProcedure [dbo].[zsp_SessionStart]    Script Date: 05/18/2010 15:00:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[zsp_SessionStart]
	@pAccountID 	    varchar(24),
	@pVendorId 	      varchar(16),
	@pSerialNumber    varchar(30),
	@pMachineNumber   int,
	@pCurrentJackpot  money = 0,
	@pVendorSessionId bigint = 0,
	@pGameTitle       varchar(50) = '',
	@pDummy           int = 0
AS
BEGIN
  EXECUTE wgdb_000.dbo.zsp_SessionStart  @pAccountID
                                       , @pVendorId
                                       , @pSerialNumber
                                       , @pMachineNumber
                                       , @pCurrentJackpot
                                       , @pVendorSessionId
END
GO
/****** Object:  StoredProcedure [dbo].[zsp_SessionUpdate]    Script Date: 05/18/2010 15:00:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[zsp_SessionUpdate]
  @pAccountID      varchar(24),
  @pVendorId       varchar(16),
  @pSerialNumber   varchar(30),
  @pMachineNumber  int,
  @pSessionId      bigint,
  @pCreditsPlayed  money,
  @pCreditsWon     money,
  @pGamesPlayed    int,
  @pGamesWon        int,
  @pCreditBalance   money,
  @pCurrentJackpot  money = 0
AS
BEGIN
  EXECUTE wgdb_000.dbo.zsp_SessionUpdate  @pAccountID
                                        , @pVendorId
                                        , @pSerialNumber
                                        , @pMachineNumber
                                        , @pSessionId 
                                        , @pCreditsPlayed
                                        , @pCreditsWon 
                                        , @pGamesPlayed 
                                        , @pGamesWon  
                                        , @pCreditBalance
                                        , @pCurrentJackpot
END
GO
/****** Object:  StoredProcedure [dbo].[zsp_SessionEnd]    Script Date: 05/18/2010 15:01:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[zsp_SessionEnd]
  @pAccountID      varchar(24),
  @pVendorId       varchar(16),
  @pSerialNumber   varchar(30),
  @pMachineNumber  int,
  @pSessionId 	    bigint,
  @pCreditsPlayed   money,
  @pCreditsWon      money,
  @pGamesPlayed    int,
  @pGamesWon       int,
  @pCreditBalance  money,
  @pCurrentJackpot money = 0
AS
BEGIN
  EXECUTE wgdb_000.dbo.zsp_SessionEnd  @pAccountID
                                     , @pVendorId
                                     , @pSerialNumber
                                     , @pMachineNumber
                                     , @pSessionId 
                                     , @pCreditsPlayed
                                     , @pCreditsWon 
                                     , @pGamesPlayed 
                                     , @pGamesWon  
                                     , @pCreditBalance
                                     , @pCurrentJackpot
END
GO
   
/****** Object:  Login [wg_interface]    Script Date: 05/10/2010 09:33:20 ******/

USE [sPOS]
GO

/****** Object:  Login [wg_interface]    Script Date: 05/12/2010 14:29:39 ******/
IF  EXISTS (SELECT * FROM sys.server_principals WHERE name = N'wg_interface')
DROP LOGIN [wg_interface]
GO

CREATE LOGIN [wg_interface] WITH PASSWORD=N'wg_interface_pwd', DEFAULT_DATABASE=[sPOS], DEFAULT_LANGUAGE=[us_english], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF, SID=0x70A641208FA0834A9DBA6F1F09E84B2D
GO
ALTER LOGIN [wg_interface] ENABLE
GO

/****** Object:  Login [EIBE]    Script Date: 05/12/2010 14:29:39 ******/
IF  EXISTS (SELECT * FROM sys.server_principals WHERE name = N'EIBE')
DROP LOGIN [EIBE]
GO

CREATE LOGIN [EIBE] WITH PASSWORD=N'EIBE', DEFAULT_DATABASE=[sPOS], DEFAULT_LANGUAGE=[us_english], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF, SID=0x6FD4AFBDF7F55342B507927F575A4B57
GO
ALTER LOGIN [EIBE] ENABLE
GO

/****** Object:  Login [3GS]  ******/
IF  EXISTS (SELECT * FROM sys.server_principals WHERE name = N'3GS')
DROP LOGIN [3GS]
GO

CREATE LOGIN [3GS] WITH PASSWORD=N'3GS_PWD', DEFAULT_DATABASE=[sPOS], DEFAULT_LANGUAGE=[us_english], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF, SID=0xAE48EE0F60BD3C49B6AFA6D0E231A791
GO
ALTER LOGIN [3GS] ENABLE
GO
   
USE [sPOS]
GO

/****** Object:  User [wginterface]    Script Date: 05/10/2010 09:33:21 ******/
CREATE USER [wg_interface] FOR LOGIN [wg_interface] WITH DEFAULT_SCHEMA=[dbo]
GO

GRANT EXECUTE ON [dbo].[zsp_AccountStatus] TO [wg_interface] WITH GRANT OPTION
GO
GRANT EXECUTE ON [dbo].[zsp_SendEvent]     TO [wg_interface] WITH GRANT OPTION
GO
GRANT EXECUTE ON [dbo].[zsp_SessionEnd]    TO [wg_interface] WITH GRANT OPTION
GO
GRANT EXECUTE ON [dbo].[zsp_SessionStart]  TO [wg_interface] WITH GRANT OPTION
GO
GRANT EXECUTE ON [dbo].[zsp_SessionUpdate] TO [wg_interface] WITH GRANT OPTION
GO
sp_addrolemember 'db_denydatareader','wg_interface' 
GO
sp_addrolemember 'db_denydatawriter','wg_interface' 
GO

/****** Object:  User [EIBE] ******/
CREATE USER [EIBE] FOR LOGIN [EIBE] WITH DEFAULT_SCHEMA=[dbo]
GO

GRANT EXECUTE ON [dbo].[zsp_AccountStatus] TO [EIBE] WITH GRANT OPTION
GO
GRANT EXECUTE ON [dbo].[zsp_SendEvent]     TO [EIBE] WITH GRANT OPTION
GO
GRANT EXECUTE ON [dbo].[zsp_SessionEnd]    TO [EIBE] WITH GRANT OPTION
GO
GRANT EXECUTE ON [dbo].[zsp_SessionStart]  TO [EIBE] WITH GRANT OPTION
GO
GRANT EXECUTE ON [dbo].[zsp_SessionUpdate] TO [EIBE] WITH GRANT OPTION
GO
sp_addrolemember 'db_denydatareader','EIBE' 
GO
sp_addrolemember 'db_denydatawriter','EIBE' 
GO

/****** Object:  User [3GS] ******/
CREATE USER [3GS] FOR LOGIN [3GS] WITH DEFAULT_SCHEMA=[dbo]
GO

GRANT EXECUTE ON [dbo].[zsp_AccountStatus] TO [3GS] WITH GRANT OPTION
GO
GRANT EXECUTE ON [dbo].[zsp_SendEvent]     TO [3GS] WITH GRANT OPTION
GO
GRANT EXECUTE ON [dbo].[zsp_SessionEnd]    TO [3GS] WITH GRANT OPTION
GO
GRANT EXECUTE ON [dbo].[zsp_SessionStart]  TO [3GS] WITH GRANT OPTION
GO
GRANT EXECUTE ON [dbo].[zsp_SessionUpdate] TO [3GS] WITH GRANT OPTION
GO
sp_addrolemember 'db_denydatareader','3GS' 
GO
sp_addrolemember 'db_denydatawriter','3GS' 
GO   

USE [wgdb_000]
GO

/****** Object:  User [wg_interface]    Script Date: 05/10/2010 09:33:21 ******/
IF EXISTS (SELECT * FROM sys.server_principals WHERE name = N'wg_interface' AND type in (N'S'))
BEGIN

IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'wg_interface' AND type in (N'S'))
CREATE USER [wg_interface] FOR LOGIN [wg_interface] WITH DEFAULT_SCHEMA=[dbo];

GRANT EXECUTE ON [dbo].[zsp_AccountStatus] TO [wg_interface] WITH GRANT OPTION;
GRANT EXECUTE ON [dbo].[zsp_SendEvent]     TO [wg_interface] WITH GRANT OPTION;
GRANT EXECUTE ON [dbo].[zsp_SessionEnd]    TO [wg_interface] WITH GRANT OPTION;
GRANT EXECUTE ON [dbo].[zsp_SessionStart]  TO [wg_interface] WITH GRANT OPTION;
GRANT EXECUTE ON [dbo].[zsp_SessionUpdate] TO [wg_interface] WITH GRANT OPTION;

EXEC sp_addrolemember 'db_denydatareader','wg_interface';
EXEC sp_addrolemember 'db_denydatawriter','wg_interface';

END
ELSE
/**** VERSION FAILURE SECTION *****/
SELECT 'Nothing to Update for wg_interface.'
GO

/****** Object:  User [EIBE] ******/
IF EXISTS (SELECT * FROM sys.server_principals WHERE name = N'EIBE' AND type in (N'S'))
BEGIN

IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'EIBE' AND type in (N'S'))
CREATE USER [EIBE] FOR LOGIN [EIBE] WITH DEFAULT_SCHEMA=[dbo];

GRANT EXECUTE ON [dbo].[zsp_AccountStatus] TO [EIBE] WITH GRANT OPTION;
GRANT EXECUTE ON [dbo].[zsp_SendEvent]     TO [EIBE] WITH GRANT OPTION;
GRANT EXECUTE ON [dbo].[zsp_SessionEnd]    TO [EIBE] WITH GRANT OPTION;
GRANT EXECUTE ON [dbo].[zsp_SessionStart]  TO [EIBE] WITH GRANT OPTION;
GRANT EXECUTE ON [dbo].[zsp_SessionUpdate] TO [EIBE] WITH GRANT OPTION;

EXEC sp_addrolemember 'db_denydatareader','EIBE';
EXEC sp_addrolemember 'db_denydatawriter','EIBE';

END
ELSE
/**** VERSION FAILURE SECTION *****/
SELECT 'Nothing to Update for EIBE.'
GO

/****** Object:  User [3GS] ******/
IF EXISTS (SELECT * FROM sys.server_principals WHERE name = N'3GS' AND type in (N'S'))
BEGIN

IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'3GS' AND type in (N'S'))
CREATE USER [3GS] FOR LOGIN [3GS] WITH DEFAULT_SCHEMA=[dbo];

GRANT EXECUTE ON [dbo].[zsp_AccountStatus] TO [3GS] WITH GRANT OPTION;
GRANT EXECUTE ON [dbo].[zsp_SendEvent]     TO [3GS] WITH GRANT OPTION;
GRANT EXECUTE ON [dbo].[zsp_SessionEnd]    TO [3GS] WITH GRANT OPTION;
GRANT EXECUTE ON [dbo].[zsp_SessionStart]  TO [3GS] WITH GRANT OPTION;
GRANT EXECUTE ON [dbo].[zsp_SessionUpdate] TO [3GS] WITH GRANT OPTION;

EXEC sp_addrolemember 'db_denydatareader','3GS';
EXEC sp_addrolemember 'db_denydatawriter','3GS';

END
ELSE
/**** VERSION FAILURE SECTION *****/
SELECT 'Nothing to Update for 3GS.'
GO
   
