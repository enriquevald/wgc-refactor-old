USE [sPOS]
GO

/****** Object:  User [3GS] ******/
CREATE USER [3GS] FOR LOGIN [3GS] WITH DEFAULT_SCHEMA=[dbo]
GO

GRANT EXECUTE ON [dbo].[zsp_AccountStatus] TO [3GS] WITH GRANT OPTION
GO
GRANT EXECUTE ON [dbo].[zsp_SendEvent]     TO [3GS] WITH GRANT OPTION
GO
GRANT EXECUTE ON [dbo].[zsp_SessionEnd]    TO [3GS] WITH GRANT OPTION
GO
GRANT EXECUTE ON [dbo].[zsp_SessionStart]  TO [3GS] WITH GRANT OPTION
GO
GRANT EXECUTE ON [dbo].[zsp_SessionUpdate] TO [3GS] WITH GRANT OPTION
GO
sp_addrolemember 'db_denydatareader','3GS' 
GO
sp_addrolemember 'db_denydatawriter','3GS' 
GO