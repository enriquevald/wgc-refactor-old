/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 3;

SET @New_ReleaseId = 7;
SET @New_ScriptName = N'UpdateTo_18.007.sql';
SET @New_Description = N'Missing & New Indexes; Handpays; WCP Provisioning.';

/**** CHECK VERSION SECTION *****/
IF (EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON


/****** TABLES ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[test]') AND type in (N'U'))
DROP TABLE [dbo].[test];
	
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[accounts]') AND name = N'IX_ac_holder_id_indexed')
DROP INDEX [IX_ac_holder_id_indexed] ON [dbo].[accounts];

IF EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[accounts]') and name = 'ac_holder_id_indexed')
ALTER TABLE [dbo].[accounts]
	DROP COLUMN [ac_holder_id_indexed];

ALTER TABLE [dbo].[accounts] 
	ADD [ac_holder_id_indexed] AS (isnull([ac_holder_id],'<NULL>'+CONVERT([nvarchar],[ac_account_id],(0))));

CREATE UNIQUE NONCLUSTERED INDEX [IX_ac_holder_id_indexed] ON [dbo].[accounts] 
(
	[ac_holder_id_indexed] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY];

ALTER TABLE  [dbo].[c2_jackpot_parameters]
ALTER COLUMN [c2jp_promo_message1] nvarchar(max) NULL;

ALTER TABLE  [dbo].[c2_jackpot_parameters]
ALTER COLUMN [c2jp_promo_message2] nvarchar(max) NULL;

ALTER TABLE  [dbo].[c2_jackpot_parameters]
ALTER COLUMN [c2jp_promo_message3] nvarchar(max) NULL;

ALTER TABLE  [dbo].[draws]
ALTER COLUMN [dr_name] nvarchar(50) NOT NULL;

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[handpays]') AND type in (N'U'))
DROP TABLE [dbo].[handpays];

CREATE TABLE [dbo].[handpays](
	[hp_terminal_id] [int] NULL,
	[hp_game_base_name] [nvarchar](50) NULL,
	[hp_datetime] [datetime] NOT NULL CONSTRAINT [DF_handpays_hp_datetime]  DEFAULT (getdate()),
	[hp_previous_meters] [datetime] NULL,
	[hp_amount] [money] NOT NULL,
	[hp_te_name] [nvarchar](50) NULL,
	[hp_te_provider_id] [nvarchar](50) NULL,
	[hp_movement_id] [bigint] NULL,
	[hp_type] [int] NOT NULL CONSTRAINT [DF_handpays_hp_type]  DEFAULT ((0))
) ON [PRIMARY];

CREATE UNIQUE NONCLUSTERED INDEX [IX_hp_datetime_terminal_id] ON [dbo].[handpays] 
(
	[hp_datetime] ASC,
	[hp_terminal_id] ASC,
	[hp_amount] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY];

/****** Objeto:  Table [dbo].[game_meters] ******/
IF EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[game_meters]') and name = 'gm_handpays_amount')
ALTER TABLE [dbo].[game_meters]
	DROP CONSTRAINT [DF_game_meters_gm_handpays_amount],
		COLUMN [gm_handpays_amount];

IF EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[game_meters]') and name = 'gm_delta_handpays_amount')
ALTER TABLE [dbo].[game_meters]
	DROP CONSTRAINT [DF_game_meters_gm_delta_handpays_amount],
		COLUMN [gm_delta_handpays_amount];

ALTER TABLE [dbo].[game_meters]   ADD
	[gm_handpays_amount] [money] NOT NULL CONSTRAINT [DF_game_meters_gm_handpays_amount]  DEFAULT ((0)),
	[gm_delta_handpays_amount] [money] NULL CONSTRAINT [DF_game_meters_gm_delta_handpays_amount]  DEFAULT ((0));
	
/****** Objeto:  Table [dbo].[hpc_meter]    Fecha de la secuencia de comandos: 07/21/2010 14:53:40 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[hpc_meter]') AND type in (N'U'))
DROP TABLE [dbo].[hpc_meter];

CREATE TABLE [dbo].[hpc_meter](
      [hpc_terminal_id] [int] NOT NULL,
      [hpc_wcp_sequence_id] [bigint] NOT NULL,
      [hpc_handpays_amount] [money] NOT NULL CONSTRAINT [DF_hpc_meter_gm_handpays_amount]  DEFAULT ((0)),
      [hpc_last_reported] [datetime] NOT NULL,
 CONSTRAINT [PK_hpc_meter] PRIMARY KEY CLUSTERED 
(
      [hpc_terminal_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY];

ALTER TABLE [dbo].[hpc_meter]  WITH CHECK ADD  CONSTRAINT [FK_hpc_meter_terminals] FOREIGN KEY([hpc_terminal_id])
REFERENCES [dbo].[terminals] ([te_terminal_id]);

ALTER TABLE [dbo].[hpc_meter] CHECK CONSTRAINT [FK_hpc_meter_terminals];

/****** Objeto:  Table [dbo].[draws] ******/
IF EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[draws]') and name = 'dr_limited')
ALTER TABLE [dbo].[draws]
	DROP CONSTRAINT [DF_draws_dr_limit_1],
		COLUMN [dr_limited];

IF EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[draws]') and name = 'dr_limit')
ALTER TABLE [dbo].[draws]
	DROP CONSTRAINT [DF_draws_dr_limit_2],
		COLUMN [dr_limit];

IF EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[draws]') and name = 'dr_limit_per_voucher')
ALTER TABLE [dbo].[draws]
	DROP CONSTRAINT [DF_draws_dr_limit_per_voucher],
		COLUMN [dr_limit_per_voucher];

ALTER TABLE [dbo].[draws]   ADD
	[dr_limited] [bit] NOT NULL CONSTRAINT [DF_draws_dr_limit_1]  DEFAULT ((0)),
	[dr_limit] [int] NOT NULL CONSTRAINT [DF_draws_dr_limit_2]  DEFAULT ((10)),
	[dr_limit_per_voucher] [int] NOT NULL CONSTRAINT [DF_draws_dr_limit_per_voucher]  DEFAULT ((300));

/****** Objeto:  Table [dbo].[terminals] ******/
ALTER TABLE  [dbo].[terminals]
ALTER COLUMN [te_external_id] [nvarchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL;

IF EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[terminals]') and name = 'te_unique_external_id')
ALTER TABLE [dbo].[terminals]
	DROP COLUMN [te_unique_external_id];

ALTER TABLE [dbo].[terminals] 
	ADD [te_unique_external_id] AS (isnull([te_external_id],'<NULL>'+CONVERT([nvarchar],[te_terminal_id],(0))));

/****** Objeto:  Table [dbo].[play_sessions] ******/
IF EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[play_sessions]') and name = 'ps_wcp_transaction_id')
ALTER TABLE [dbo].[play_sessions]
	DROP CONSTRAINT [DF_play_sessions_ps_wcp_transaction_id],
		COLUMN [ps_wcp_transaction_id];

ALTER TABLE [dbo].[play_sessions]
	ADD [ps_wcp_transaction_id] [bigint] NULL CONSTRAINT [DF_play_sessions_ps_wcp_transaction_id]  DEFAULT ((0));

/****** Objeto:  Table [dbo].[handpays] ******/
IF EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[handpays]') and name = 'hp_play_session_id')
ALTER TABLE [dbo].[handpays]
	DROP COLUMN [hp_play_session_id];

ALTER TABLE [dbo].[handpays] 
	ADD [hp_play_session_id] [bigint] NULL;


/****** INDEXES ******/
/****** Object:  Index [IX_3gs_vendor_serialnumber]    Script Date: 07/01/2010 15:57:08 ******/
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[terminals_3gs]') AND name = N'IX_3gs_vendor_serialnumber')
DROP INDEX [IX_3gs_vendor_serialnumber] ON [dbo].[terminals_3gs];

CREATE UNIQUE NONCLUSTERED INDEX [IX_3gs_vendor_serialnumber] ON [dbo].[terminals_3gs] 
(
	[t3gs_vendor_id] ASC,
	[t3gs_machine_number] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY];

/****** Object:  Index [IX_tp_source_vendor_sn]    Script Date: 07/01/2010 15:57:33 ******/
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[terminals_pending]') AND name = N'IX_tp_source_vendor_sn')
DROP INDEX [IX_tp_source_vendor_sn] ON [dbo].[terminals_pending];

CREATE NONCLUSTERED INDEX [IX_tp_source_vendor_sn] ON [dbo].[terminals_pending] 
(
	[tp_source] ASC,
	[tp_vendor_id] ASC,
	[tp_serial_number] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY];

/****** Object:  Index [IX_terminal_type_name]    Script Date: 07/01/2010 15:57:59 ******/
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[terminal_types]') AND name = N'IX_terminal_type_name')
DROP INDEX [IX_terminal_type_name] ON [dbo].[terminal_types];

CREATE UNIQUE NONCLUSTERED INDEX [IX_terminal_type_name] ON [dbo].[terminal_types] 
(
	[tt_name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY];

/****** Objeto:  Index [IX_ps_terminal_id]    Fecha de la secuencia de comandos: 07/19/2010 03:47:51 ******/
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[play_sessions]') AND name = N'IX_ps_terminal_id')
DROP INDEX [IX_ps_terminal_id] ON [dbo].[play_sessions];

CREATE NONCLUSTERED INDEX [IX_ps_terminal_id] ON [dbo].[play_sessions] 
(
                [ps_terminal_id] ASC,
                [ps_play_session_id] ASC,
                [ps_status] ASC,
                [ps_stand_alone] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY];

/****** Object:  Index [IX_mb_track_data]    Script Date: 07/27/2010 15:13:52 ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[mobile_banks]') AND name = N'IX_mb_track_data')
DROP INDEX [IX_mb_track_data] ON [dbo].[mobile_banks] WITH ( ONLINE = OFF );

CREATE UNIQUE NONCLUSTERED INDEX [IX_mb_track_data] ON [dbo].[mobile_banks] 
(
  [mb_track_data] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = OFF) ON [PRIMARY];

/****** Objeto:  Index [PK_event_history]    Fecha de la secuencia de comandos: 07/19/2010 03:47:51 ******/
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[event_history]') AND name = N'PK_event_history')
ALTER TABLE [event_history] DROP CONSTRAINT [pk_event_history];

ALTER TABLE [event_history] ADD CONSTRAINT [pk_event_history] PRIMARY KEY CLUSTERED 
(
	[eh_datetime] ASC,
	[eh_terminal_id] ASC,
	[eh_event_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY];

/****** Objeto:  Index [IX_event_history]    Fecha de la secuencia de comandos: 07/19/2010 03:47:51 ******/
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[event_history]') AND name = N'IX_event_history')
DROP INDEX [IX_event_history] ON [dbo].[event_history];

/****** Objeto:  Index [IX_event_timestamp]    Fecha de la secuencia de comandos: 07/19/2010 03:47:51 ******/
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[event_history]') AND name = N'IX_event_timestamp')
DROP INDEX [IX_event_timestamp] ON [dbo].[event_history];

CREATE NONCLUSTERED INDEX [IX_event_timestamp] ON [dbo].[event_history] 
(
	[eh_timestamp] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY];

/****** Object:  Index [IX_terminal_unique_ext_id]    Script Date: 08/23/2010 15:57:58 ******/
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[terminals]') AND name = N'IX_terminal_external_id')
DROP INDEX [IX_terminal_external_id] ON [dbo].[terminals];

CREATE NONCLUSTERED INDEX [IX_terminal_external_id] ON [dbo].[terminals] 
(
	[te_external_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY];


IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[terminals]') AND name = N'IX_terminal_unique_ext_id')
DROP INDEX [IX_terminal_unique_ext_id] ON [dbo].[terminals];

CREATE UNIQUE NONCLUSTERED INDEX [IX_terminal_unique_ext_id] ON [dbo].[terminals] 
(
	[te_unique_external_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY];


/****** RECORDS ******/
IF EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.HandPays' AND GP_SUBJECT_KEY ='TimeInterval')
DELETE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.HandPays' AND GP_SUBJECT_KEY ='TimeInterval';

INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('Cashier.HandPays'
           ,'TimePeriod'
           ,'60');

IF EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.HandPays' AND GP_SUBJECT_KEY ='TimeToCancel')
DELETE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.HandPays' AND GP_SUBJECT_KEY ='TimeToCancel';

INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('Cashier.HandPays'
           ,'TimeToCancel'
           ,'5');

IF EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY ='LegalAge')
DELETE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY ='LegalAge';

INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('Cashier'
           ,'LegalAge'
           ,'18');

IF EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'WCP' AND GP_SUBJECT_KEY ='ProvisioningManual')
DELETE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'WCP' AND GP_SUBJECT_KEY ='ProvisioningManual';

INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('WCP'
           ,'ProvisioningManual'
           ,'1');
           

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;

END
ELSE
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION
GO
