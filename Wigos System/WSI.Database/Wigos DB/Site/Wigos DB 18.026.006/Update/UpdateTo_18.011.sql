/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 8;

SET @New_ReleaseId = 11;
SET @New_ScriptName = N'UpdateTo_18.011.sql';
SET @New_Description = N'Promotions; player tracking; taxes. Fixes.';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;

/****** TABLES ******/
/* Account_Movements */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[account_movements]') and name = 'am_operation_id')
ALTER TABLE [dbo].[account_movements] 
	ADD [am_operation_id] [bigint] NULL;
ELSE
SELECT '***** Field account_movements.am_operation_id already exists *****';

/* Accounts */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[accounts]') and name = 'ac_nr_won_lock')
ALTER TABLE [dbo].[accounts] 
	ADD [ac_nr_won_lock] [money] NULL CONSTRAINT [DF_accounts_ac_nr_won_lock] DEFAULT ((0));
ELSE
SELECT '***** Field accounts.ac_nr_won_lock already exists *****';
	
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[accounts]') and name = 'ac_nr_expiration')
ALTER TABLE [dbo].[accounts] 
	ADD [ac_nr_expiration] [datetime] NULL;
ELSE
SELECT '***** Field accounts.ac_nr_expiration already exists *****';
	
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[accounts]') and name = 'ac_cashin_while_playing')
ALTER TABLE [dbo].[accounts] 
	ADD [ac_cashin_while_playing] [money] NULL;
ELSE
SELECT '***** Field accounts.ac_cashin_while_playing already exists *****';
	
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[accounts]') and name = 'ac_holder_level')
ALTER TABLE [dbo].[accounts] 
	ADD [ac_holder_level] [int] NOT NULL CONSTRAINT [DF_accounts_ac_holder_level] DEFAULT ((0));
ELSE
SELECT '***** Field accounts.ac_holder_level already exists *****';

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[accounts]') and name = 'ac_card_paid')
ALTER TABLE [dbo].[accounts] 
	ADD [ac_card_paid] [bit] NOT NULL CONSTRAINT [DF_accounts_ac_card_paid] DEFAULT ((0));
ELSE
SELECT '***** Field accounts.ac_card_paid already exists *****';

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[accounts]') and name = 'ac_cancellable_operation_id')
ALTER TABLE [dbo].[accounts] 
	ADD [ac_cancellable_operation_id] [bigint] NULL;	
ELSE
SELECT '***** Field accounts.ac_cancellable_operation_id already exists *****';

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[accounts]') and name = 'ac_current_promotion_id')
ALTER TABLE [dbo].[accounts] 
	ADD [ac_current_promotion_id] [bigint] NULL;	
ELSE
SELECT '***** Field accounts.ac_current_promotion_id already exists *****';

/* Cashier_Movements */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[cashier_movements]') and name = 'cm_operation_id')
ALTER TABLE [dbo].[cashier_movements] 
	ADD [cm_operation_id] [bigint] NULL;
ELSE
SELECT '***** Field cashier_movements.cm_operation_id already exists *****';

/* Cashier_Vouchers */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[cashier_vouchers]') and name = 'cv_operation_id')
ALTER TABLE [dbo].[cashier_vouchers] 
	ADD [cv_operation_id] [bigint] NULL;
ELSE
SELECT '***** Field cashier_vouchers.cv_operation_id already exists *****';

/* MB_Movements */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[mb_movements]') and name = 'mbm_operation_id')
ALTER TABLE [dbo].[mb_movements] 
	ADD [mbm_operation_id] [bigint] NULL;
ELSE
SELECT '***** Field mb_movements.mbm_operation_id already exists *****';
	
/* Mobile_Banks */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[mobile_banks]') and name = 'mb_track_number')
ALTER TABLE [dbo].[mobile_banks] 
	ADD [mb_track_number] AS (CONVERT([bigint],[mb_track_data],0));
ELSE
SELECT '***** Field mobile_banks.mb_track_number already exists *****';
	
/* Play_Sessions */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[play_sessions]') and name = 'ps_total_cash_in')
ALTER TABLE [dbo].[play_sessions] 
	ADD [ps_total_cash_in] AS ([ps_initial_balance]+[ps_cash_in]) PERSISTED;
ELSE
SELECT '***** Field play_sessions.ps_total_cash_in already exists *****';

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[play_sessions]') and name = 'ps_total_cash_out')
ALTER TABLE [dbo].[play_sessions] 
	ADD [ps_total_cash_out] AS (isnull([ps_final_balance],(0))+[ps_cash_out]) PERSISTED;
ELSE
SELECT '***** Field play_sessions.ps_total_cash_out already exists *****';
	
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[play_sessions]') and name = 'ps_total_played')
ALTER TABLE [dbo].[play_sessions] 
	ADD [ps_total_played] AS ([ps_played_amount]) PERSISTED NOT NULL;
ELSE
SELECT '***** Field play_sessions.ps_total_played already exists *****';
	
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[play_sessions]') and name = 'ps_total_won')
ALTER TABLE [dbo].[play_sessions] 
	ADD [ps_total_won] AS ([ps_won_amount]);
ELSE
SELECT '***** Field play_sessions.ps_total_won already exists *****';
	
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[play_sessions]') and name = 'ps_redeemable_cash_in')
ALTER TABLE [dbo].[play_sessions] 
	ADD [ps_redeemable_cash_in] [money] NOT NULL CONSTRAINT [DF_play_sessions_ps_redeemable_cash_in] DEFAULT ((0));
ELSE
SELECT '***** Field play_sessions.ps_redeemable_cash_in already exists *****';
	
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[play_sessions]') and name = 'ps_redeemable_cash_out')
ALTER TABLE [dbo].[play_sessions] 
	ADD [ps_redeemable_cash_out] [money] NOT NULL CONSTRAINT [DF_play_sessions_ps_redeemable_cash_out] DEFAULT ((0));
ELSE
SELECT '***** Field play_sessions.ps_redeemable_cash_out already exists *****';
	
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[play_sessions]') and name = 'ps_redeemable_played')
ALTER TABLE [dbo].[play_sessions] 
	ADD [ps_redeemable_played] [money] NOT NULL CONSTRAINT [DF_play_sessions_ps_redeemable_played] DEFAULT ((0));
ELSE
SELECT '***** Field play_sessions.ps_redeemable_played already exists *****';
	
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[play_sessions]') and name = 'ps_redeemable_won')
ALTER TABLE [dbo].[play_sessions] 
	ADD [ps_redeemable_won] [money] NOT NULL CONSTRAINT [DF_play_sessions_ps_redeemable_won] DEFAULT ((0));
ELSE
SELECT '***** Field play_sessions.ps_redeemable_won already exists *****';
	
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[play_sessions]') and name = 'ps_non_redeemable_cash_in')
ALTER TABLE [dbo].[play_sessions] 
	ADD [ps_non_redeemable_cash_in] [money] NOT NULL CONSTRAINT [DF_play_sessions_ps_non_redeemable_cash_in] DEFAULT ((0));
ELSE
SELECT '***** Field play_sessions.ps_non_redeemable_cash_in already exists *****';
	
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[play_sessions]') and name = 'ps_non_redeemable_cash_out')
ALTER TABLE [dbo].[play_sessions] 
	ADD [ps_non_redeemable_cash_out] [money] NOT NULL CONSTRAINT [DF_play_sessions_ps_non_redeemable_cash_out] DEFAULT ((0));
ELSE
SELECT '***** Field play_sessions.ps_non_redeemable_cash_out already exists *****';
	
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[play_sessions]') and name = 'ps_non_redeemable_played')
ALTER TABLE [dbo].[play_sessions] 
	ADD [ps_non_redeemable_played] [money] NOT NULL CONSTRAINT [DF_play_sessions_ps_non_redeemable_played] DEFAULT ((0));
ELSE
SELECT '***** Field play_sessions.ps_non_redeemable_played already exists *****';

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[play_sessions]') and name = 'ps_non_redeemable_won')
ALTER TABLE [dbo].[play_sessions] 
	ADD [ps_non_redeemable_won] [money] NOT NULL CONSTRAINT [DF_play_sessions_ps_non_redeemable_won] DEFAULT ((0));
ELSE
SELECT '***** Field play_sessions.ps_non_redeemable_won already exists *****';

/* Account_Operations */
/****** Object:  Table [dbo].[account_operations]    Script Date: 10/04/2010 11:57:13 ******/
IF NOT EXISTS (SELECT * FROM SYS.OBJECTS WHERE TYPE = 'U' AND NAME = 'account_operations')
BEGIN
CREATE TABLE [dbo].[account_operations](
	[ao_operation_id] [bigint] IDENTITY(1,1) NOT NULL,
	[ao_datetime] [datetime] NOT NULL CONSTRAINT [DF_account_operations_ao_datetime]  DEFAULT (getdate()),
	[ao_code] [int] NOT NULL,
	[ao_account_id] [bigint] NOT NULL,
	[ao_cashier_session_id] [bigint] NULL,
	[ao_mb_account_id] [bigint] NULL,
	[ao_promo_id] [bigint] NULL,
	[ao_amount] [money] NULL,
	[ao_non_redeemable] [money] NULL,
	[ao_won_lock] [money] NULL,
 CONSTRAINT [PK_account_operations] PRIMARY KEY CLUSTERED 
(
	[ao_operation_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY];
CREATE NONCLUSTERED INDEX [IX_ao_date_account_promo] ON [dbo].[account_operations] 
(
	[ao_datetime] ASC,
	[ao_account_id] ASC,
	[ao_promo_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY];
END
ELSE
SELECT '***** Table Account_Operations already exists *****';

/* Gift_Instances */
IF NOT EXISTS (SELECT * FROM SYS.OBJECTS WHERE TYPE = 'U' AND NAME = 'Gift_Instances')
CREATE TABLE [dbo].[gift_instances](
	[gin_gift_instance_id] [bigint] IDENTITY(1,1) NOT NULL,
	[gin_oper_request_id] [bigint] NOT NULL,
	[gin_oper_delivery_id] [bigint] NULL,
	[gin_account_id] [bigint] NOT NULL,
	[gin_gift_id] [bigint] NOT NULL,
	[gin_gift_name] [nvarchar](50) NOT NULL,
	[gin_gift_type] [int] NOT NULL,
	[gin_points] [money] NOT NULL,
	[gin_conversion_to_nrc] [money] NULL,
	[gin_spent_points] [money] NOT NULL,
	[gin_requested] [datetime] NOT NULL,
	[gin_delivered] [datetime] NULL,
	[gin_expiration] [datetime] NOT NULL,
 CONSTRAINT [PK_gift_instances] PRIMARY KEY CLUSTERED 
(
	[gin_gift_instance_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY];
ELSE
SELECT '***** Table Gift_Instances already exists *****';

/* Gifts */
IF NOT EXISTS (SELECT * FROM SYS.OBJECTS WHERE TYPE = 'U' AND NAME = 'gifts')
CREATE TABLE [dbo].[gifts](
	[gi_gift_id] [bigint] IDENTITY(1,1) NOT NULL,
	[gi_name] [nvarchar](50) NOT NULL,
	[gi_points] [money] NOT NULL,
	[gi_conversion_to_nrc] [money] NULL,
	[gi_available] [bit] NOT NULL,
	[gi_type] [int] NOT NULL
) ON [PRIMARY];
ELSE
SELECT '***** Table Gifts already exists *****';

/* Promotions */
IF NOT EXISTS (SELECT * FROM SYS.OBJECTS WHERE TYPE = 'U' AND NAME = 'promotions')
CREATE TABLE [dbo].[promotions](
	[pm_promotion_id] [bigint] IDENTITY(1,1) NOT NULL,
	[pm_name] [nvarchar](50) NULL,
	[pm_enabled] [bit] NOT NULL,
	[pm_type] [int] NOT NULL,
	[pm_date_start] [datetime] NOT NULL,
	[pm_date_finish] [datetime] NOT NULL,
	[pm_schedule_weekday] [int] NOT NULL,
	[pm_schedule1_time_from] [int] NOT NULL,
	[pm_schedule1_time_to] [int] NOT NULL,
	[pm_schedule2_enabled] [bit] NOT NULL,
	[pm_schedule2_time_from] [int] NULL,
	[pm_schedule2_time_to] [int] NULL,
	[pm_gender_filter] [int] NOT NULL,
	[pm_birthday_filter] [int] NOT NULL,
	[pm_expiration_type] [int] NOT NULL,
	[pm_expiration_value] [int] NOT NULL,
	[pm_min_cash_in] [money] NOT NULL,
	[pm_min_cash_in_reward] [money] NOT NULL,
	[pm_cash_in] [money] NOT NULL,
	[pm_cash_in_reward] [money] NOT NULL,
	[pm_won_lock] [money] NOT NULL,
	[pm_num_tokens] [int] NOT NULL,
	[pm_token_name] [nvarchar](50) NULL,
	[pm_token_reward] [money] NOT NULL,
	[pm_daily_limit] [money] NULL,
	[pm_monthly_limit] [money] NULL,
 CONSTRAINT [PK_promotions] PRIMARY KEY CLUSTERED 
(
	[pm_promotion_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY];
ELSE
SELECT '***** Table Promotions already exists *****';

/****** INDEXES ******/


/****** STORED PROCEDURES ******/

--------------------------------------------------------------------------------
-- PURPOSE: Inserts a new movement in the database
-- 
--  PARAMS:
--      - INPUT:
--          @PlaySessionId  bigint ,
--          @AccountId      bigint ,
--          @TerminalId     int ,
--          @MovementType   int ,                                       
--          @InitialBalance money , 
--          @SubAmount      money , 
--          @AddAmount      money , 
--          @FinalBalance   money
--
--      - OUTPUT:
--
-- RETURNS:
--      StatusCode, 
--      StatusText, 
--      SessionID
--   NOTES:
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InsertMovement]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[InsertMovement]
GO
CREATE PROCEDURE dbo.InsertMovement
  @PlaySessionId  bigint ,
  @AccountId      bigint ,
  @TerminalId     int ,
  @MovementType   int ,                                       
  @InitialBalance money , 
  @SubAmount      money , 
  @AddAmount      money , 
  @FinalBalance   money
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
  DECLARE @terminal_name nvarchar (254)
  
  SET @terminal_name = ''

  SELECT  @terminal_name = TE_NAME 
    FROM  TERMINALS
   WHERE  TE_TERMINAL_ID = @TerminalId 

  INSERT INTO ACCOUNT_MOVEMENTS (AM_PLAY_SESSION_ID
                               , AM_ACCOUNT_ID 
                               , AM_TERMINAL_ID 
                               , AM_TYPE 
                               , AM_INITIAL_BALANCE 
                               , AM_SUB_AMOUNT 
                               , AM_ADD_AMOUNT 
                               , AM_FINAL_BALANCE 
                               , AM_DATETIME 
                               , AM_TERMINAL_NAME 
                               ) 
                         VALUES (@PlaySessionId
                               , @AccountId
                               , @TerminalId
                               , @MovementType
                               , @InitialBalance
                               , @SubAmount
                               , @AddAmount
                               , @FinalBalance
                               , GETDATE()
                               , @terminal_name 
                               )
                               
  -- SET @MovementId = SCOPE_IDENTITY() 

END -- InsertMovement
GO 

--------------------------------------------------------------------------------
-- PURPOSE : Read PlaySession & Accounts & PlayerTracking data
-- 
--  PARAMS :
--      - INPUT :
--          @PlaySessionId      bigint
--
--      - OUTPUT :
--          @TerminalId       int            
--          @AccountId        bigint         
--          @InitialBalance   money          
--          @PlayedAmount     money          
--          @WonAmount        money          
--          @FinalBalance     money        
--          @InitialNonRedeemable  money        
--          @AccountBalance   money          
--          @HolderName       nvarchar (50)
--          @HolderLevel      int          
--          @StatusCode       int            
--          @StatusText       nvarchar (254) 
--
-- RETURNS :
--
--   NOTES :

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PT_ReadData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PT_ReadData]
GO
CREATE PROCEDURE [dbo].[PT_ReadData]
  @PlaySessionId          bigint
, @TerminalId             int             OUTPUT
, @AccountId              bigint          OUTPUT
, @InitialBalance         money           OUTPUT
, @PlayedAmount           money           OUTPUT
, @WonAmount              money           OUTPUT
, @FinalBalance           money           OUTPUT
, @InitialNonRedeemable   money           OUTPUT
, @InitialCashIn          money           OUTPUT
, @PrizeLock              money           OUTPUT
, @AccountBalance         money           OUTPUT
, @PointsBalance          money           OUTPUT
, @HolderName             nvarchar (50)   OUTPUT
, @HolderLevel            int             OUTPUT
, @StatusCode             int             OUTPUT
, @StatusText             nvarchar (254)  OUTPUT    
AS
BEGIN
  DECLARE @rc int
	
  SET @StatusCode = 1
	SET @StatusText = 'PT_ReadData: Reading PlaySession...'
	
	--
	-- Select data from PLAY_SESSIONS & ACCOUNTS table
	--
  SELECT @TerminalId            = PS_TERMINAL_ID
       , @AccountId             = PS_ACCOUNT_ID
       , @InitialBalance        = PS_INITIAL_BALANCE + PS_CASH_IN
       , @PlayedAmount          = PS_PLAYED_AMOUNT
       , @WonAmount             = PS_WON_AMOUNT
       , @FinalBalance          = PS_FINAL_BALANCE
       , @InitialNonRedeemable  = AC_INITIAL_NOT_REDEEMABLE
       , @InitialCashIn         = AC_INITIAL_CASH_IN
       , @PrizeLock             = AC_NR_WON_LOCK
       , @AccountBalance        = AC_BALANCE
       , @PointsBalance         = AC_POINTS 
       , @HolderName            = ISNULL (AC_HOLDER_NAME, '')
       , @HolderLevel           = ISNULL (AC_HOLDER_LEVEL, 0)
    FROM PLAY_SESSIONS, ACCOUNTS
   WHERE PS_PLAY_SESSION_ID = @PlaySessionId
     AND PS_ACCOUNT_ID      = AC_ACCOUNT_ID

  SET @rc = @@ROWCOUNT
  IF ( @rc <> 1 )
  BEGIN
    SET @StatusCode  = 1
    SET @StatusText  = 'PT_ReadData: Invalid PlaySessionId.'
    GOTO ERROR_PROCEDURE
  END

  IF ( @HolderName = '' )
  BEGIN
    SET @HolderLevel = 0  
  END  
  --
  -- Read data successful
  --
  SET @StatusCode = 0
  SET @StatusText = 'PT_ReadData: PlaySession read.'

ERROR_PROCEDURE:

END -- PT_ReadData
GO

--------------------------------------------------------------------------------
-- PURPOSE : Compute the Balance parts.
--           The balace is splitted as:
--              - Non Redeemable
--              - Redeemable
--  PARAMS :
--      - INPUT :
--          @Balance                money
--          @InitialCashIn          money
--          @InitialNonRedeemable   money
--          @PrizeLock              money
--
--      - OUTPUT :
--          @PartNonRedeemable      money
--          @PartRedeemable         money
--
-- RETURNS :
--
--   NOTES :

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PT_BalancePartsWhenPlaying]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PT_BalancePartsWhenPlaying]
GO
CREATE PROCEDURE [dbo].[PT_BalancePartsWhenPlaying]
  @Balance                money
, @InitialCashIn          money
, @InitialNonRedeemable   money
, @PrizeLock              money
, @PartNonRedeemable      money  OUTPUT
, @PartRedeemable         money  OUTPUT
AS
BEGIN
  DECLARE @part_prize money
	
  -- PartNonRedeemable
  SET @PartNonRedeemable = dbo.Minimum_Money (@Balance, @InitialNonRedeemable)
  SET @Balance = @Balance - @PartNonRedeemable
  -- PartRedeemable
  SET @PartRedeemable = dbo.Minimum_Money (@Balance, @InitialCashIn)
  SET @Balance = @Balance - @PartRedeemable
  -- PartPrize
  SET @part_prize = @Balance

  IF ( @PrizeLock > 0 )
    BEGIN
      -- WonLock -> Non Redeemable + Prize
      SET @PartNonRedeemable = @PartNonRedeemable + @part_prize
    END
  ELSE
    BEGIN
      SET @PartRedeemable = @PartRedeemable + @part_prize;
    END

END -- PT_BalancePartsWhenPlaying
GO

--------------------------------------------------------------------------------
-- PURPOSE : Read Point calculation factors
-- 
--  PARAMS :
--      - INPUT :
--          @HolderName
--          @HolderLevel
--
--      - OUTPUT :
--          @RedeemablePlayedTo1Point
--          @TotalPlayedTo1Point
--          @RedeemableSpentTo1Point
--          @StatusCode     
--          @StatusText     
--
-- RETURNS :
--
--   NOTES :

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PT_ReadPointFactors]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PT_ReadPointFactors]
GO
CREATE PROCEDURE [dbo].[PT_ReadPointFactors]
  @HolderLevel                          int
, @RedeemableSpentTo1Point              numeric (20,6)  OUTPUT
, @RedeemablePlayedTo1Point             numeric (20,6)  OUTPUT
, @TotalPlayedTo1Point                  numeric (20,6)  OUTPUT
, @StatusCode                           int             OUTPUT
, @StatusText                           nvarchar (254)  OUTPUT    
AS
BEGIN
  DECLARE @rc           int
  DECLARE @level_name   nvarchar (50)

  SET @StatusCode = 1
  SET @StatusText = 'PT_ReadPointFactors: Reading Point Calculation Factors...'

  SET @RedeemableSpentTo1Point  = 0.00
  SET @RedeemablePlayedTo1Point = 0.00       
  SET @TotalPlayedTo1Point      = 0.00

	--
	-- Select data from GENERAL_PARAMS table: 
	--      * PlayerTracking.LevelXX.RedeemablePlayedTo1Point
	--      * PlayerTracking.LevelXX.TotalPlayedTo1Point
	--      * PlayerTracking.LevelXX.RedeemableSpentTo1Point
	--

  IF ( @HolderLevel = 1 )
    SET @level_name = 'Level01'
  ELSE IF ( @HolderLevel = 2 )
    SET @level_name = 'Level02'
  ELSE IF ( @HolderLevel = 3 )
    SET @level_name = 'Level03'
  ELSE
    BEGIN
      --- 
      --- Wong Holder's Level 
      ---
      SET @StatusCode = 1
      SET @StatusText = 'PT_ReadPointFactors: Invalid Holder Level.'

      GOTO ERROR_PROCEDURE
    END  

  --- 
  --- Read level's RedeemablePlayedTo1Point
  ---
  SELECT @RedeemablePlayedTo1Point = GP_KEY_VALUE
    FROM GENERAL_PARAMS
   WHERE GP_GROUP_KEY = 'PlayerTracking'
     AND GP_SUBJECT_KEY = @level_name + '.RedeemablePlayedTo1Point'

  SET @rc = @@ROWCOUNT
  IF ( @rc <> 1 )
  BEGIN
    SET @StatusCode = 1
    SET @StatusText = 'PT_ReadPointFactors: Invalid Player Tracking setting Holder Level: PlayerTracking.' + @level_name + '.RedeemablePlayedTo1Point'
    GOTO ERROR_PROCEDURE
  END

  --- 
  --- Read level's TotalPlayedTo1Point
  ---
  SELECT @TotalPlayedTo1Point = GP_KEY_VALUE
    FROM GENERAL_PARAMS
   WHERE GP_GROUP_KEY = 'PlayerTracking'
     AND GP_SUBJECT_KEY = @level_name + '.TotalPlayedTo1Point'  

  SET @rc = @@ROWCOUNT
  IF ( @rc <> 1 )
  BEGIN
    SET @StatusCode = 1
    SET @StatusText = 'PT_ReadPointFactors: Invalid Player Tracking setting Holder Level: PlayerTracking.' + @level_name + '.TotalPlayedTo1Point'
    GOTO ERROR_PROCEDURE
  END

  --- 
  --- Read level's RedeemableSpentTo1Point
  ---
  SELECT @RedeemableSpentTo1Point = GP_KEY_VALUE
    FROM GENERAL_PARAMS
   WHERE GP_GROUP_KEY = 'PlayerTracking'
     AND GP_SUBJECT_KEY = @level_name + '.RedeemableSpentTo1Point'  

  SET @rc = @@ROWCOUNT
  IF ( @rc <> 1 )
  BEGIN
    SET @StatusCode = 1
    SET @StatusText = 'PT_ReadPointFactors: Invalid Player Tracking setting Holder Level: PlayerTracking.' + @level_name + '.RedeemableSpentTo1Point'
    GOTO ERROR_PROCEDURE
  END

  --
  -- Read data successful
  --
  SET @StatusCode = 0
  SET @StatusText = 'PT_ReadPointFactors: Point Calculation read.'

  GOTO EXIT_PROCEDURE

ERROR_PROCEDURE:
  SET @RedeemablePlayedTo1Point = 0.00
  SET @TotalPlayedTo1Point      = 0.00
  SET @RedeemableSpentTo1Point  = 0.00

EXIT_PROCEDURE:

END -- PT_ReadPointFactors
GO

--------------------------------------------------------------------------------
-- PURPOSE : Accumulate points for player when play session is finished
-- 
--  PARAMS :
--      - INPUT :
--          @PlaySessionId      bigint
--
--      - OUTPUT :
--
-- RETURNS :
--
--   NOTES :

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PT_PlaySessionFinished]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PT_PlaySessionFinished]
GO
CREATE PROCEDURE [dbo].[PT_PlaySessionFinished]
  @PlaySessionId      bigint
AS
BEGIN

	-- Only for test
  DECLARE @status_code                       int
  DECLARE @status_text                       nvarchar (500)
  -- Only for test
  
  DECLARE @terminal_id                       int
  DECLARE @account_id                        bigint
  DECLARE @total_cash_in                     money
  DECLARE @played_amount                     money
  DECLARE @won_amount                        money
  DECLARE @total_cash_out                    money
  DECLARE @initial_non_redeemable            money
  DECLARE @initial_cash_in                   money
  DECLARE @prize_lock                        money
  DECLARE @account_balance                   money
  DECLARE @holder_name                       nvarchar (50)
  DECLARE @holder_level                      int
  
  DECLARE @non_redeemable_cash_in            money
  DECLARE @redeemable_cash_in                money
  DECLARE @non_redeemable_cash_out           money
  DECLARE @redeemable_cash_out               money
  DECLARE @non_redeemable_played             money
  DECLARE @redeemable_played                 money
  DECLARE @non_redeemable_won                money
  DECLARE @redeemable_won                    money
    
  DECLARE @spent_no_redeemable               money
  DECLARE @spent_redeemable                  money

  DECLARE @profit_no_redeemable              money
  DECLARE @profit_redeemable                 money
  
  DECLARE @total_sum_redimible               numeric (20,6)
  DECLARE @total_sum                         numeric (20,6)
  DECLARE @percent_redeemable                numeric (8,6)

  DECLARE @rest_played_and_no_spent          money
  DECLARE @rest_won_and_no_profit            money
  
  DECLARE @total_played_to_1_point           numeric (20,6)
  DECLARE @redeemable_played_to_1_point      numeric (20,6)
  DECLARE @redeemable_spent_to_1_point       numeric (20,6)

  DECLARE @points_balance_before             money
  DECLARE @points_balance_after              money
  DECLARE @points_total_played               money
  DECLARE @points_played_redeemable          money
  DECLARE @points_spent_redeemable           money
  DECLARE @won_points                        money

  -- SET NOCOUNT ON added to prevent extra result sets from
  -- interfering with SELECT statements.
  SET NOCOUNT ON;
	
  SET @status_code  = 0
  SET @status_text  = 'PT_PlaySessionFinished: Init'

  SET @points_total_played      = 0
  SET @points_played_redeemable = 0
  SET @points_spent_redeemable  = 0
  SET @won_points               = 0  
  SET @points_balance_before    = 0
  SET @points_balance_after     = 0


  IF ( @PlaySessionId = 0 ) 
  BEGIN
    SET @status_text = 'PT_PlaySessionFinished: Invalid PlaySessionId'
    GOTO ERROR_PROCEDURE
  END

  --
  -- Read Session & Account & PlayerTracking Data
  --
  EXECUTE dbo.PT_ReadData @PlaySessionId, @terminal_id OUTPUT, @account_id OUTPUT,
                          @total_cash_in OUTPUT, @played_amount OUTPUT, @won_amount OUTPUT, @total_cash_out OUTPUT, 
                          @initial_non_redeemable OUTPUT, @initial_cash_in OUTPUT, @prize_lock OUTPUT, @account_balance OUTPUT, 
                          @points_balance_before OUTPUT, @holder_name OUTPUT, @holder_level OUTPUT, 
                          @status_code OUTPUT, @status_text OUTPUT

  IF ( @status_code <> 0 )
    GOTO ERROR_PROCEDURE
    
  --- Anonymous accounts have Level=0
  IF ( @holder_level = 0 )
    GOTO EXIT_PROCEDURE

  --
  -- Compute PlayedCredits, PlayedRedeemableCredits, SpentRedeemableCredits
  --
  EXECUTE dbo.PT_BalancePartsWhenPlaying @total_cash_in, @initial_cash_in, @initial_non_redeemable, @prize_lock, 
                                         @non_redeemable_cash_in OUTPUT, @redeemable_cash_in OUTPUT

  EXECUTE dbo.PT_BalancePartsWhenPlaying @total_cash_out, @initial_cash_in, @initial_non_redeemable, @prize_lock, 
                                         @non_redeemable_cash_out OUTPUT, @redeemable_cash_out OUTPUT

  SET @spent_no_redeemable      = dbo.Maximum_Money (0, @non_redeemable_cash_in - @non_redeemable_cash_out)
  SET @spent_redeemable         = dbo.Maximum_Money (0, @redeemable_cash_in - @redeemable_cash_out)

  SET @profit_no_redeemable     = dbo.Maximum_Money (0, @non_redeemable_cash_out - @non_redeemable_cash_in)
  SET @profit_redeemable        = dbo.Maximum_Money (0, @redeemable_cash_out - @redeemable_cash_in)

  SET @total_sum_redimible      = @redeemable_cash_in + @redeemable_cash_out 
  SET @total_sum                = @non_redeemable_cash_in + @non_redeemable_cash_out + @redeemable_cash_in + @redeemable_cash_out
  
  IF @total_sum = 0 GOTO EXIT_PROCEDURE   
  SET @percent_redeemable       = @total_sum_redimible / @total_sum

  SET @rest_played_and_no_spent = @played_amount - (@spent_no_redeemable + @spent_redeemable)
  SET @redeemable_played        = ROUND(@spent_redeemable + (@rest_played_and_no_spent * @percent_redeemable), 4)
  SET @non_redeemable_played    = @played_amount - @redeemable_played

  SET @rest_won_and_no_profit   = @won_amount - (@profit_no_redeemable + @profit_redeemable)
  SET @redeemable_won           = ROUND(@profit_redeemable + (@rest_won_and_no_profit * @percent_redeemable), 4)
  SET @non_redeemable_won       = @won_amount - @redeemable_won

  --
  -- Update PlaySession table (Redeemable & NonRedeemable amounts)
  --
  UPDATE PLAY_SESSIONS
     SET PS_NON_REDEEMABLE_CASH_IN    = @non_redeemable_cash_in
       , PS_NON_REDEEMABLE_CASH_OUT   = @non_redeemable_cash_out
       , PS_NON_REDEEMABLE_PLAYED     = @non_redeemable_played
       , PS_NON_REDEEMABLE_WON        = @non_redeemable_won
       , PS_REDEEMABLE_CASH_IN        = @redeemable_cash_in
       , PS_REDEEMABLE_CASH_OUT       = @redeemable_cash_out
       , PS_REDEEMABLE_PLAYED         = @redeemable_played
       , PS_REDEEMABLE_WON            = @redeemable_won
   WHERE PS_PLAY_SESSION_ID = @PlaySessionId 

  --
  -- Read PlayerTracking Point Conversion Factors according to the holder's level
  --
  EXECUTE dbo.PT_ReadPointFactors @holder_level, 
                                  @redeemable_spent_to_1_point  OUTPUT,
                                  @redeemable_played_to_1_point OUTPUT,
                                  @total_played_to_1_point      OUTPUT,
                                  @status_code                  OUTPUT, 
                                  @status_text                  OUTPUT

  IF ( @status_code <> 0 )
    GOTO ERROR_PROCEDURE

  --
  -- Compute Points (it is possible to accummulate points per each type of credits at the same time)
  --  
  IF @redeemable_spent_to_1_point  > 0 SET @points_spent_redeemable  = @spent_redeemable  / @redeemable_spent_to_1_point
  IF @redeemable_played_to_1_point > 0 SET @points_played_redeemable = @redeemable_played / @redeemable_played_to_1_point
  IF @total_played_to_1_point      > 0 SET @points_total_played      = @played_amount     / @total_played_to_1_point
  
  SET @won_points = ROUND(@points_total_played + @points_played_redeemable + @points_spent_redeemable, 4)

  --
  -- Accumulate points
  -- RCI & ACC & AJQ 30/09/2010: Reset the Cancellable Operation when PlayedAmount greater than 0.
  UPDATE   ACCOUNTS
     SET   AC_POINTS                   = AC_POINTS  + @won_points
         , AC_CANCELLABLE_OPERATION_ID = CASE WHEN (@played_amount > 0) THEN NULL ELSE AC_CANCELLABLE_OPERATION_ID END
   WHERE   AC_ACCOUNT_ID               = @account_id  
  
  --  
  -- Create CardMovement.ObtainedPoints
  --
  -- PointsAwarded   = 36,
  SET @points_balance_after     = @points_balance_before + @won_points
  
  EXECUTE dbo.InsertMovement @PlaySessionId, @account_id, @terminal_id, 36, @points_balance_before, 0, @won_points, @points_balance_after

EXIT_PROCEDURE:
  SET @status_code  = 0
  SET @status_text  = 'PT_PlaySessionFinished: Successful points accumulated.' 
                    + ' *** '
                    + ' Points.PerRedeemableSpent  = ' + CAST (@points_spent_redeemable AS nvarchar)  + ' (' + CAST (@spent_redeemable AS nvarchar)  + ' credits)'
                    + ' Points.PerRedeemablePlayed = ' + CAST (@points_played_redeemable AS nvarchar) + ' (' + CAST (@redeemable_played AS nvarchar) + ' credits)'
                    + ' Points.PerTotalPlayed      = ' + CAST (@points_total_played AS nvarchar)      + ' (' + CAST (@played_amount AS nvarchar)     + ' credits)'
                    + ' Points.TotalAwarded        = ' + CAST (@won_points AS nvarchar) 
                    + ' *** '

ERROR_PROCEDURE:
  SELECT @status_text AS StatusText

END -- PT_PlaySessionFinished
GO

/****** TRIGGERS ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AM_3GS_Trigger]') AND type in (N'TR'))
DROP TRIGGER [dbo].[AM_3GS_Trigger]
GO
-- =============================================
-- Author:		Name
-- Create date: 
-- Description:	
-- =============================================
CREATE TRIGGER [dbo].[AM_3GS_Trigger]
   ON  [dbo].[account_movements] 
   AFTER INSERT
NOT FOR REPLICATION
AS 
BEGIN
DECLARE @mov_type    int
DECLARE @new_cashin  money
DECLARE @account_id  bigint
DECLARE @terminal_id int

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Check Movement Type
	-- 1 - CashIn
	-- 9 - NonRedeemable CashIn
	SET @mov_type = (SELECT AM_TYPE FROM INSERTED)
    
	IF ( @mov_type = 1 OR @mov_type = 9 ) 
    BEGIN
       SET @account_id  = (SELECT AM_ACCOUNT_ID FROM INSERTED)
	   
       -- Check if it is in session
       SET @terminal_id = (SELECT AC_CURRENT_TERMINAL_ID FROM ACCOUNTS WHERE AC_ACCOUNT_ID = @account_id)
	   -- Is Account 'InUse'?
       IF @terminal_id IS NOT NULL
       BEGIN
		-- Yes, It is 'InUse'
        -- Is 3GS Terminal?
		IF EXISTS ( SELECT T3GS_TERMINAL_ID FROM TERMINALS_3GS WHERE T3GS_TERMINAL_ID = @terminal_id )
        BEGIN
		  -- 3GS Terminal
          SET @new_cashin  = (SELECT AM_ADD_AMOUNT FROM INSERTED)
          -- Update 'cashin_while_playing'
		  UPDATE ACCOUNTS 
             SET AC_CASHIN_WHILE_PLAYING = ISNULL (AC_CASHIN_WHILE_PLAYING, 0) + @new_cashin
           WHERE AC_ACCOUNT_ID           = @account_id
             AND AC_CURRENT_TERMINAL_ID  = @terminal_id         
        END
       END
    END

END
GO

--------------------------------------------------------------------------------
-- PURPOSE : Trigger on PlaySession when play session is finished
-- 
--  PARAMS :
--      - INPUT :
--
--      - OUTPUT :
--
-- RETURNS :
--
--   NOTES :

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PlayerTrackingTrigger]') AND type in (N'TR'))
DROP TRIGGER [dbo].[PlayerTrackingTrigger]
GO

CREATE TRIGGER [dbo].[PlayerTrackingTrigger]
ON [dbo].[play_sessions]
AFTER UPDATE
NOT FOR REPLICATION
AS
DECLARE @new_status     int
DECLARE @old_status     int
DECLARE @play_session   bigint
BEGIN

  IF UPDATE (PS_STATUS)
  BEGIN
    SET @old_status   = (SELECT PS_STATUS FROM DELETED)
    SET @new_status   = (SELECT PS_STATUS FROM INSERTED)
    SET @play_session = (SELECT PS_PLAY_SESSION_ID FROM INSERTED)

    IF @old_status = 0 AND @new_status <> 0 EXECUTE dbo.PT_PlaySessionFinished @play_session
  END
  
END -- PlayerTrackingTrigger
GO

/****** RECORDS ******/          
UPDATE ACCOUNTS 
   SET ac_card_paid = 1; 

UPDATE ACCOUNTS 
   SET ac_card_paid = 0 
 WHERE AC_HOLDER_NAME IS NULL 
       and AC_BALANCE = 0 
       and ac_deposit = 0 
       And ac_promo_balance = 0
       And ac_current_terminal_id IS NULL; 

/* Cashier */
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY = 'Split.A.CashInPct')
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('Cashier'
           ,'Split.A.CashInPct'
           ,'100');
ELSE
SELECT '***** Record Cashier_Split.A.CashInPct already exists *****';           

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY = 'Split.A.CompanyName')
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('Cashier'
           ,'Split.A.CompanyName'
           ,'');
ELSE
SELECT '***** Record Cashier_Split.A.CompanyName already exists *****';           

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY = 'Split.A.DevolutionPct')
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('Cashier'
           ,'Split.A.DevolutionPct'
           ,'100');
ELSE
SELECT '***** Record Cashier_Split.A.DevolutionPct already exists *****';           

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY = 'Split.A.Name')
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('Cashier'
           ,'Split.A.Name'
           ,'');
ELSE
SELECT '***** Record Cashier_Split.A.Name already exists *****';           

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY = 'Split.A.Tax.Name')
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('Cashier'
           ,'Split.A.Tax.Name'
           ,'');
ELSE
SELECT '***** Record Cashier_Split.A.Tax.Name already exists *****';           

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY = 'Split.A.Tax.Pct')
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('Cashier'
           ,'Split.A.Tax.Pct'
           ,'0');
ELSE
SELECT '***** Record Cashier_Split.A.Tax.Pct already exists *****';           

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY = 'Split.B.CashInPct')
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('Cashier'
           ,'Split.B.CashInPct'
           ,'0');
ELSE
SELECT '***** Record Cashier_Split.B.CashInPct already exists *****';           

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY = 'Split.B.CompanyName')
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('Cashier'
           ,'Split.B.CompanyName'
           ,'');
ELSE
SELECT '***** Record Cashier_Split.B.CompanyName already exists *****';           

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY = 'Split.B.DevolutionPct')
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('Cashier'
           ,'Split.B.DevolutionPct'
           ,'0');
ELSE
SELECT '***** Record Cashier_Split.B.DevolutionPct already exists *****';           

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY = 'Split.B.Enabled')
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('Cashier'
           ,'Split.B.Enabled'
           ,'0');
ELSE
SELECT '***** Record Cashier_Split.B.Enabled already exists *****';           

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY = 'Split.B.Name')
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('Cashier'
           ,'Split.B.Name'
           ,'');
ELSE
SELECT '***** Record Cashier_Split.B.Name already exists *****';           

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY = 'Split.B.Tax.Name')
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('Cashier'
           ,'Split.B.Tax.Name'
           ,'');
ELSE
SELECT '***** Record Cashier_Split.B.Tax.Name already exists *****';           

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY = 'Split.B.Tax.Pct')
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('Cashier'
           ,'Split.B.Tax.Pct'
           ,'0');
ELSE
SELECT '***** Record Cashier_Split.B.Tax.Pct already exists *****';           

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY = 'SplitAsWon')
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('Cashier'
           ,'SplitAsWon'
           ,'0');
ELSE
SELECT '***** Record Cashier_SplitAsWon already exists *****';           

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY = 'Tax.OnPrize.1.Name')
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('Cashier'
           ,'Tax.OnPrize.1.Name'
           ,'Impuesto Federal');
ELSE
SELECT '***** Record Cashier_Tax.OnPrize.1.Name already exists *****';           

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY = 'Tax.OnPrize.1.Pct')
BEGIN
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('Cashier'
           ,'Tax.OnPrize.1.Pct'
           ,'0');
UPDATE [dbo].[general_params]
   SET [gp_key_value] = (SELECT [gp_key_value] 
   			   FROM [dbo].[general_params]
			  WHERE [gp_group_key] = 'Cashier'
			    AND [gp_subject_key] = 'FederalTaxOnWonPct')
 WHERE [gp_group_key] = 'Cashier'
   AND [gp_subject_key] = 'Tax.OnPrize.1.Pct';
END           
ELSE
SELECT '***** Record Cashier_Tax.OnPrize.1.Pct already exists *****';           

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY = 'Tax.OnPrize.1.Threshold')
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('Cashier'
           ,'Tax.OnPrize.1.Threshold'
           ,'0');
ELSE
SELECT '***** Record Cashier_Tax.OnPrize.1.Threshold already exists *****';           

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY = 'Tax.OnPrize.2.Name')
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('Cashier'
           ,'Tax.OnPrize.2.Name'
           ,'Impuesto Estatal');
ELSE
SELECT '***** Record Cashier_Tax.OnPrize.2.Name already exists *****';           

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY = 'Tax.OnPrize.2.Pct')
BEGIN
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('Cashier'
           ,'Tax.OnPrize.2.Pct'
           ,'0');
UPDATE [dbo].[general_params]
   SET [gp_key_value] = (SELECT [gp_key_value] 
   			   FROM [dbo].[general_params]
			  WHERE [gp_group_key] = 'Cashier'
			    AND [gp_subject_key] = 'StateTaxOnWonPct')
 WHERE [gp_group_key] = 'Cashier'
   AND [gp_subject_key] = 'Tax.OnPrize.2.Pct';           
END           
ELSE
SELECT '***** Record Cashier_Tax.OnPrize.2.Pct already exists *****';           

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY = 'Tax.OnPrize.2.Threshold')
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('Cashier'
           ,'Tax.OnPrize.2.Threshold'
           ,'0');
ELSE
SELECT '***** Record Cashier_Tax.OnPrize.2.Threshold already exists *****';           

/* Cashier Voucher */
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.Voucher' AND GP_SUBJECT_KEY = 'EstimatedPrintTime')
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('Cashier.Voucher'
           ,'EstimatedPrintTime'
           ,'1500');
ELSE
SELECT '***** Record Cashier.Voucher_EstimatedPrintTime already exists *****';

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.Voucher' AND GP_SUBJECT_KEY = 'Voucher.A.Footer')
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('Cashier.Voucher'
           ,'Voucher.A.Footer'
           ,'');
ELSE
SELECT '***** Record Cashier.Voucher_Voucher.A.Footer already exists *****';

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.Voucher' AND GP_SUBJECT_KEY = 'Voucher.A.Footer.Cancel')
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('Cashier.Voucher'
           ,'Voucher.A.Footer.Cancel'
           ,'');
ELSE
SELECT '***** Record Cashier.Voucher_Voucher.A.Footer.Cancel already exists *****';

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.Voucher' AND GP_SUBJECT_KEY = 'Voucher.A.Footer.CashIn')
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('Cashier.Voucher'
           ,'Voucher.A.Footer.CashIn'
           ,'');
ELSE
SELECT '***** Record Cashier.Voucher_Voucher.A.Footer.CashIn already exists *****';

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.Voucher' AND GP_SUBJECT_KEY = 'Voucher.A.Footer.Dev')
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('Cashier.Voucher'
           ,'Voucher.A.Footer.Dev'
           ,'');
ELSE
SELECT '***** Record Cashier.Voucher_Voucher.A.Footer.Dev already exists *****';

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.Voucher' AND GP_SUBJECT_KEY = 'Voucher.A.Header')
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('Cashier.Voucher'
           ,'Voucher.A.Header'
           ,'');
ELSE
SELECT '***** Record Cashier.Voucher_Voucher.A.Header already exists *****';

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.Voucher' AND GP_SUBJECT_KEY = 'Voucher.B.Footer')
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('Cashier.Voucher'
           ,'Voucher.B.Footer'
           ,'');
ELSE
SELECT '***** Record Cashier.Voucher_Voucher.B.Footer already exists *****';

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.Voucher' AND GP_SUBJECT_KEY = 'Voucher.B.Footer.Cancel')
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('Cashier.Voucher'
           ,'Voucher.B.Footer.Cancel'
           ,'');
ELSE
SELECT '***** Record Cashier.Voucher_Voucher.B.Footer.Cancel already exists *****';

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.Voucher' AND GP_SUBJECT_KEY = 'Voucher.B.Footer.CashIn')
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('Cashier.Voucher'
           ,'Voucher.B.Footer.CashIn'
           ,'');
ELSE
SELECT '***** Record Cashier.Voucher_Voucher.B.Footer.CashIn already exists *****';

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.Voucher' AND GP_SUBJECT_KEY = 'Voucher.B.Footer.Dev')
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('Cashier.Voucher'
           ,'Voucher.B.Footer.Dev'
           ,'');
ELSE
SELECT '***** Record Cashier.Voucher_Voucher.B.Footer.Dev already exists *****';

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.Voucher' AND GP_SUBJECT_KEY = 'Voucher.B.Header')
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('Cashier.Voucher'
           ,'Voucher.B.Header'
           ,'');
ELSE
SELECT '***** Record Cashier.Voucher_Voucher.B.Header already exists *****';

/* MobileBank */ 
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'MobileBank' AND GP_SUBJECT_KEY ='AutomaticallyAssignPromotion')
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('MobileBank'
           ,'AutomaticallyAssignPromotion'
           ,'0');
ELSE
SELECT '***** Record MobileBank_AutomaticallyAssignPromotion already exists *****';    

/* Player Tracking */
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PlayerTracking' AND GP_SUBJECT_KEY ='PointsExpirationDays')
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('PlayerTracking'
           ,'PointsExpirationDays'
           ,'30');
ELSE
SELECT '***** Record PlayerTracking_PointsExpirationDays already exists *****';

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PlayerTracking' AND GP_SUBJECT_KEY ='GiftExpirationDays')
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('PlayerTracking'
           ,'GiftExpirationDays'
           ,'30');
ELSE
SELECT '***** Record PlayerTracking_GiftExpirationDays already exists *****';

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PlayerTracking' AND GP_SUBJECT_KEY ='Level01.Name')
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('PlayerTracking'
           ,'Level01.Name'
           ,'Plata');
ELSE
SELECT '***** Record PlayerTracking_Level01.Name already exists *****';

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PlayerTracking' AND GP_SUBJECT_KEY ='Level02.Name')
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('PlayerTracking'
           ,'Level02.Name'
           ,'Oro');
ELSE
SELECT '***** Record PlayerTracking_Level02.Name already exists *****';

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PlayerTracking' AND GP_SUBJECT_KEY ='Level03.Name')
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('PlayerTracking'
           ,'Level03.Name'
           ,'Platino');
ELSE
SELECT '***** Record PlayerTracking_Level03.Name already exists *****';

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PlayerTracking' AND GP_SUBJECT_KEY ='Level01.RedeemablePlayedTo1Point')
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('PlayerTracking'
           ,'Level01.RedeemablePlayedTo1Point'
           ,'0');
ELSE
SELECT '***** Record PlayerTracking_Level01.RedeemablePlayedTo1Point already exists *****';

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PlayerTracking' AND GP_SUBJECT_KEY ='Level02.RedeemablePlayedTo1Point')
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('PlayerTracking'
           ,'Level02.RedeemablePlayedTo1Point'
           ,'0');
ELSE
SELECT '***** Record PlayerTracking_Level02.RedeemablePlayedTo1Point already exists *****';

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PlayerTracking' AND GP_SUBJECT_KEY ='Level03.RedeemablePlayedTo1Point')
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('PlayerTracking'
           ,'Level03.RedeemablePlayedTo1Point'
           ,'0');
ELSE
SELECT '***** Record PlayerTracking_Level03.RedeemablePlayedTo1Point already exists *****';

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PlayerTracking' AND GP_SUBJECT_KEY ='Level01.TotalPlayedTo1Point')
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('PlayerTracking'
           ,'Level01.TotalPlayedTo1Point'
           ,'0');
ELSE
SELECT '***** Record PlayerTracking_Level01.TotalPlayedTo1Point already exists *****';

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PlayerTracking' AND GP_SUBJECT_KEY ='Level02.TotalPlayedTo1Point')
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('PlayerTracking'
           ,'Level02.TotalPlayedTo1Point'
           ,'0');
ELSE
SELECT '***** Record PlayerTracking_Level02.TotalPlayedTo1Point already exists *****';

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PlayerTracking' AND GP_SUBJECT_KEY ='Level03.TotalPlayedTo1Point')
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('PlayerTracking'
           ,'Level03.TotalPlayedTo1Point'
           ,'0');
ELSE
SELECT '***** Record PlayerTracking_Level03.TotalPlayedTo1Point already exists *****';

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PlayerTracking' AND GP_SUBJECT_KEY ='Level01.RedeemableSpentTo1Point')
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('PlayerTracking'
           ,'Level01.RedeemableSpentTo1Point'
           ,'0');
ELSE
SELECT '***** Record PlayerTracking_Level01.RedeemableSpentTo1Point already exists *****';

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PlayerTracking' AND GP_SUBJECT_KEY ='Level02.RedeemableSpentTo1Point')
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('PlayerTracking'
           ,'Level02.RedeemableSpentTo1Point'
           ,'0');
ELSE
SELECT '***** Record PlayerTracking_Level02.RedeemableSpentTo1Point already exists *****';

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PlayerTracking' AND GP_SUBJECT_KEY ='Level03.RedeemableSpentTo1Point')
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('PlayerTracking'
           ,'Level03.RedeemableSpentTo1Point'
           ,'0');
ELSE
SELECT '***** Record PlayerTracking_Level03.RedeemableSpentTo1Point already exists *****';

/* Player Tracking */
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'WigosGUI' AND GP_SUBJECT_KEY ='ClosingTimeMinutes')
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('WigosGUI'
           ,'ClosingTimeMinutes'
           ,'0');
ELSE
SELECT '***** Record WigosGUI_ClosingTimeMinutes already exists *****';

/*** Player Tracking configuration ***/
IF NOT EXISTS (SELECT * FROM promotions WHERE pm_type = 1)
INSERT INTO [dbo].[promotions]
           ([pm_name]
           ,[pm_enabled]
           ,[pm_type]
           ,[pm_date_start]
           ,[pm_date_finish]
           ,[pm_schedule_weekday]
           ,[pm_schedule1_time_from]
           ,[pm_schedule1_time_to]
           ,[pm_schedule2_enabled]
           ,[pm_schedule2_time_from]
           ,[pm_schedule2_time_to]
           ,[pm_gender_filter]
           ,[pm_birthday_filter]
           ,[pm_expiration_type]
           ,[pm_expiration_value]
           ,[pm_min_cash_in]
           ,[pm_min_cash_in_reward]
           ,[pm_cash_in]
           ,[pm_cash_in_reward]
           ,[pm_won_lock]
           ,[pm_num_tokens]
           ,[pm_token_name]
           ,[pm_token_reward])
     VALUES
           ('Programa de Puntos'
           ,1
           ,1
           ,CAST('01-01-2010 00:00:00' as DATETIME)
           ,CAST('01-01-2100 00:00:00' as DATETIME)
           ,127
           ,0
           ,0
           ,0
           ,0
           ,0
           ,0
           ,0
           ,1
           ,1
           ,0
           ,0
           ,0
           ,0
           ,0
           ,0
           ,''
           ,0)
GO

