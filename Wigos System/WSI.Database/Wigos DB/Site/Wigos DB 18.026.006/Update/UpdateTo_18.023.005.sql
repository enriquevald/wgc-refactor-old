/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 22;

SET @New_ReleaseId = 23;
SET @New_ScriptName = N'UpdateTo_18.023.005.sql';
SET @New_Description = N'';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/
/* Cashier_Sessions */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[cashier_sessions]') and name = 'cs_collected_amount')
ALTER TABLE [dbo].[cashier_sessions] 
	ADD [cs_collected_amount] [money] NULL;
ELSE
SELECT '***** Field cashier_sessions.cs_collected_amount already exists *****';


/****** INDEXES ******/

/****** Object:  Index [IX_ac_holder_level_expiration]    Script Date: 01/24/2011 15:06:23 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[accounts]') AND name = N'IX_ac_holder_level_expiration')
CREATE NONCLUSTERED INDEX [IX_ac_holder_level_expiration] ON [dbo].[accounts] 
    (
	[ac_holder_level_expiration] ASC
    )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
ELSE
    SELECT '***** Index accounts.IX_ac_holder_level_expiration already exists *****';

/****** Objeto:  Index [IX_ps_started]    Fecha de la secuencia de comandos: 01/27/2011 10:19:25 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[play_sessions]') AND name = N'IX_ps_started')
CREATE NONCLUSTERED INDEX [IX_ps_started] ON [dbo].[play_sessions] 
(
	[ps_started] ASC,
	[ps_status] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
ELSE
    SELECT '***** Index play_sessions.IX_ps_started already exists *****';


/****** TRIGGERS ******/


/****** RECORDS ******/
/* Cashier */
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY ='KeepEnabledButtonsAfterOperation')
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('Cashier'
           ,'KeepEnabledButtonsAfterOperation'
           ,'0');
ELSE
SELECT '***** Record Cashier_KeepEnabledButtonsAfterOperation already exists *****';

/* Mailing */
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Mailing' AND GP_SUBJECT_KEY ='AuthorizedServerList')
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('Mailing'
           ,'AuthorizedServerList'
           ,'');
ELSE
SELECT '***** Record Mailing_AuthorizedServerList already exists *****';

/* Site */
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Site' AND GP_SUBJECT_KEY ='DisableNewSessions')
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('Site'
           ,'DisableNewSessions'
           ,'0');
ELSE
SELECT '***** Record Site_DisableNewSessions already exists *****';

/**************************************** 3GS Section ****************************************/

/**** CHECK DATABASE EXISTENCE SECTION *****/
GO
IF (NOT EXISTS (SELECT * FROM sys.databases WHERE name = 'sPOS'))
BEGIN
	SELECT 'No 3GS interface to update.';

	raiserror('No 3GS interface to update', 20, -1) with log;
END
ELSE 
    SELECT 'Updating 3GS interface...';


/**** 3GS DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);
    
/**** 3GS INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 4;

SET @New_ReleaseId = 5;
SET @New_ScriptName = N'UpdateTo_18.023.005.sql';
SET @New_Description = N'Updated 3GS trigger from Account_Movements: fix Cup�n Premio & WhilePlaying; check te_status when starting card session.';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version_interface_3gs WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong 3GS DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION_INTERFACE_3GS

raiserror('Not updated', 20, -1) with log
END

/**** UPDATE VERSION SECTION *****/
UPDATE db_version_interface_3gs
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;


/**** UPDATE BODY SECTION *****/
GO
ALTER TRIGGER [dbo].[AM_3GS_Trigger]
   ON  [dbo].[account_movements] 
   AFTER INSERT
NOT FOR REPLICATION
AS 
BEGIN
DECLARE @mov_type    int
DECLARE @new_cashin  money
DECLARE @account_id  bigint
DECLARE @terminal_id int

      -- SET NOCOUNT ON added to prevent extra result sets from
      -- interfering with SELECT statements.
      SET NOCOUNT ON;

    -- Check Movement Type
      -- 1 - CashIn
      -- 9 - NonRedeemable CashIn
      -- 44 - Prize Coupon
      SET @mov_type = (SELECT AM_TYPE FROM INSERTED)
    
      IF ( @mov_type = 1 OR @mov_type = 9 OR @mov_type = 44 ) 
    BEGIN
       SET @account_id  = (SELECT AM_ACCOUNT_ID FROM INSERTED)
         
       -- Check if it is in session
       SET @terminal_id = (SELECT AC_CURRENT_TERMINAL_ID FROM ACCOUNTS WHERE AC_ACCOUNT_ID = @account_id)
         -- Is Account 'InUse'?
       IF @terminal_id IS NOT NULL
       BEGIN
            -- Yes, It is 'InUse'
        -- Is 3GS Terminal?
            IF EXISTS ( SELECT T3GS_TERMINAL_ID FROM TERMINALS_3GS WHERE T3GS_TERMINAL_ID = @terminal_id )
        BEGIN
              -- 3GS Terminal
          SET @new_cashin  = (SELECT AM_ADD_AMOUNT FROM INSERTED)
          -- Update 'cashin_while_playing'
              UPDATE ACCOUNTS 
             SET AC_CASHIN_WHILE_PLAYING = ISNULL (AC_CASHIN_WHILE_PLAYING, 0) + @new_cashin
           WHERE AC_ACCOUNT_ID           = @account_id
             AND AC_CURRENT_TERMINAL_ID  = @terminal_id         
        END
       END
      END

END;
GO      

ALTER PROCEDURE [dbo].[zsp_SessionStart]
	@AccountID 	    varchar(24),
	@VendorId 	      varchar(16),
	@SerialNumber    varchar(30),
	@MachineNumber   int,
	@CurrentJackpot  money = 0,
	@VendorSessionID bigint = 0
AS
BEGIN

  BEGIN TRAN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
		
  DECLARE @status_code         int
	DECLARE @status_text         nvarchar (254)
	DECLARE @session_id          bigint
	DECLARE @balance             money
	DECLARE @terminal_id         bigint
	DECLARE @account_id          bigint
	DECLARE @is_promotion        int
	DECLARE @promo_balance       money
	DECLARE @account_terminal_id bigint
	DECLARE @current_session_id  bigint
	DECLARE @movement_type       int
	DECLARE @cash_in_while_playing money
    -- 09-AUG-2010, Return the welcome message with the customer name
    DECLARE @holder_name         nvarchar (50)
	
    SET @status_code        = 0
	SET @status_text        = 'Bienvenido'
	SET @session_id         = 0
	SET @balance            = 0
	SET @current_session_id = 0

	SELECT @terminal_id = dbo.GetTerminalID(@VendorId, @SerialNumber, @MachineNumber) 
		
  IF (@terminal_id = 0)
  BEGIN
    EXECUTE dbo.InsertTerminal 0, @VendorId, @SerialNumber, @MachineNumber
    SET @status_code = 3
    SET @status_text = 'Invalid Machine Information'
	  COMMIT TRAN
    BEGIN TRAN
    GOTO ERROR_PROCEDURE
  END
  
	IF ((SELECT dbo.CheckTerminalBlocked(@terminal_ID)) = 1)
  BEGIN
    SET @status_code = 3
    SET @status_text = 'Invalid Machine Information'
    GOTO ERROR_PROCEDURE
  END

	SELECT @account_id = dbo.GetAccountID(@AccountID)
  
  IF (@account_ID = 0)
  BEGIN
    SET @status_code = 1
    SET @status_text = 'Invalid Account Number'
    GOTO ERROR_PROCEDURE
  END
	
	IF ((SELECT dbo.CheckAccountBlocked(@account_ID)) = 1)
  BEGIN
    SET @status_code = 4
    SET @status_text = 'Access Denied'
    GOTO ERROR_PROCEDURE
  END
	
	SELECT @is_promotion        = CASE WHEN ( (AC_PROMO_CREATION <= GETDATE()) AND (GETDATE() < AC_PROMO_EXPIRATION) AND (AC_PROMO_BALANCE >0) ) THEN 1 ELSE 0 END
       , @promo_balance       = AC_PROMO_BALANCE 
       , @balance             = AC_BALANCE 
       , @current_session_id  = ISNULL(AC_CURRENT_PLAY_SESSION_ID , 0 ) 
       , @account_terminal_id = ISNULL(AC_CURRENT_TERMINAL_ID , 0 )
       -- 09-AUG-2010, Return the welcome message with the customer name
       , @holder_name         = ISNULL(AC_HOLDER_NAME, ' ')
       , @cash_in_while_playing = ISNULL(AC_CASHIN_WHILE_PLAYING, 0)
  FROM   ACCOUNTS                   
  WHERE ( AC_ACCOUNT_ID = @account_ID )
	
	IF (@is_promotion = 1)
  BEGIN
    SET @balance = @promo_balance
  END
		
	IF (@current_session_id <> 0)
	BEGIN
	  IF ( @account_terminal_id <> @terminal_id )
	  BEGIN
	    SET @status_code = 2
      SET @status_text = 'Account Locked (In Session)' 
      GOTO ERROR_PROCEDURE
	  END

    IF (@cash_in_while_playing = 0)
    BEGIN
	    SET @status_code = 2
      SET @status_text = 'Account Locked (In Session)' 
      GOTO ERROR_PROCEDURE
	  END

    -- Close account session
    EXECUTE dbo.CloseAccountSession @account_id
  
	END

  DECLARE @insert_ps_table TABLE (StatusCode int, StatusText nvarchar(254), SessionID bigint)
  INSERT INTO @insert_ps_table EXECUTE  dbo.InsertPlaySession @terminal_id, @account_id, @balance, @is_promotion    
  SELECT @status_code = StatusCode
       , @status_text = StatusText
       , @session_id  = SessionID
    FROM @insert_ps_table
  
  IF ( @status_code <> 0 )
    GOTO ERROR_PROCEDURE
   
  EXECUTE dbo.InsertGameMeters @terminal_id
	
	SET @movement_type = CASE WHEN (@is_promotion = 1) THEN 24 ELSE 5 END
	
	EXECUTE dbo.InsertMovement @session_id
                           , @account_id
                           , @terminal_id
                           , @movement_type
                           , @balance
                           , 0
                           , 0
                           , @balance
	
	

	
    -- 09-AUG-2010, Return the welcome message with the customer name
    SET @status_text = 'Bienvenido ' + @holder_name
	--    IF (@is_promotion = 1)
	--    BEGIN
	--       SET @status_text = @status_text + ' --- NO REDIMIBLE ---'
	--    END

    -- AJQ 17-SEP-2010, Reset CashinWhilePlaying
    UPDATE ACCOUNTS
       SET AC_CASHIN_WHILE_PLAYING    = NULL
     WHERE AC_ACCOUNT_ID              = @account_id
       AND AC_CURRENT_TERMINAL_ID     = @terminal_id
       AND AC_CURRENT_PLAY_SESSION_ID = @session_id 
	 
	COMMIT TRAN
	GOTO OK  
	  
	ERROR_PROCEDURE:
	  ROLLBACK TRAN
	  
	OK:  
	
	-- Audit MBF 17-JUN-2010
	BEGIN TRAN
	
    DECLARE @input AS nvarchar(MAX)
    DECLARE @output AS nvarchar(MAX)	
  	
	  SET @input = '@AccountID='+@AccountID
	             +';@VendorId='+@VendorId
	             +';@SerialNumber='+CAST (@SerialNumber AS nvarchar)
	             +';@MachineNumber='+CAST (@MachineNumber AS nvarchar)
	             +';@CurrentJackpot='+CAST (@CurrentJackpot AS nvarchar)
	             +';@VendorSessionID='+CAST (@VendorSessionID AS nvarchar)
  	           
  	           
    SET @output = 'StatusCode='+CAST (@status_code AS nvarchar)
                 +';StatusText='+@status_text
                 +';SessionID='+CAST(@session_id AS nvarchar)
                 +';Balance='+CAST(@balance AS nvarchar)
     	
	  EXECUTE dbo.zsp_Audit 'zsp_SessionStart'
                        , @AccountID
                        , @VendorId 
                        , @SerialNumber
                        , @MachineNumber
                        , @session_id
                        , @status_code
                        , @balance
                        , @input
                        , @output        
  COMMIT TRAN
	-- End Audit
	
	SELECT @status_code AS StatusCode, @status_text AS StatusText, @session_id AS SessionId, @balance AS Balance

END -- zsp_SessionStart

GO
ALTER FUNCTION dbo.CheckTerminalBlocked
 (@TerminalId	   int)
RETURNS int
AS
BEGIN
  DECLARE @blocked int
  DECLARE @status int
  DECLARE @disable_new_sessions int
  
  --- 
  --- Do not allow to start session if new sessions are disabled
  ---
  SELECT @disable_new_sessions = GP_KEY_VALUE
    FROM GENERAL_PARAMS
   WHERE GP_GROUP_KEY = 'Site'
     AND GP_SUBJECT_KEY = 'DisableNewSessions'
     
  IF @disable_new_sessions = 1 RETURN 1

  SELECT @blocked = TE_BLOCKED ,
         @status  = TE_STATUS 
    FROM TERMINALS 
   WHERE TE_TERMINAL_ID = @TerminalId
   
  -- Terminal Blocked or Retired
  IF @blocked = 1 OR @status <> 0 RETURN 1

  RETURN 0
END -- CheckTerminalBlocked
GO

/**** FINAL SECTION *****/
SELECT '3GS Update OK.';
