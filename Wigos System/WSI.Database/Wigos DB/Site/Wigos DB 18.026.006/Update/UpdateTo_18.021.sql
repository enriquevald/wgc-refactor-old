/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 20;

SET @New_ReleaseId = 21;
SET @New_ScriptName = N'UpdateTo_18.021.sql';
SET @New_Description = N'Cadillac Jack mode; general_params value updated to 500.';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/

/****** Object:  Table [dbo].[cj_parameters]    Script Date: 12/16/2010 16:40:26 ******/
IF NOT EXISTS (SELECT * FROM SYS.OBJECTS WHERE TYPE = 'U' AND NAME = 'cj_parameters')
CREATE TABLE [dbo].[cj_parameters](
	[cjp_local_ip_1] [nvarchar](50) NULL,
	[cjp_remote_ip_1] [nvarchar](50) NULL,
	[cjp_vendor_id_1] [nvarchar](50) NULL,
	[cjp_local_ip_2] [nvarchar](50) NULL,
	[cjp_remote_ip_2] [nvarchar](50) NULL,
	[cjp_vendor_id_2] [nvarchar](50) NULL
) ON [PRIMARY];

/* General_Params */
ALTER TABLE [dbo].[general_params]
  ALTER COLUMN gp_key_value [nvarchar](500) NULL;
  

/****** INDEXES ******/


/****** STORED PROCEDURES ******/


/****** TRIGGERS ******/


/****** RECORDS ******/
/* CJ_Parameters */
IF NOT EXISTS (SELECT * FROM cj_parameters)
INSERT INTO [dbo].[cj_parameters]
           ([cjp_local_ip_1]
           ,[cjp_remote_ip_1]
           ,[cjp_vendor_id_1]
           ,[cjp_local_ip_2]
           ,[cjp_remote_ip_2]
           ,[cjp_vendor_id_2])
     VALUES
           ( NULL
           , NULL
           , NULL
           , NULL
           , NULL
           , NULL);           
ELSE
SELECT '***** Record in cj_parameters already exists *****';

