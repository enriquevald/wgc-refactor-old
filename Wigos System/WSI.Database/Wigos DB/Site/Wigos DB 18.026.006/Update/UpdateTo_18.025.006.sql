/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 24;

SET @New_ReleaseId = 25;
SET @New_ScriptName = N'UpdateTo_18.025.sql';
SET @New_Description = N'New default GUI user profiles; new indexes AM & HP.';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/

ALTER TABLE [dbo].[promotions]
ALTER COLUMN [pm_won_lock] [money] NULL;

UPDATE [dbo].[promotions]
   SET pm_won_lock = CASE WHEN ( pm_won_lock = 0 ) THEN NULL ELSE pm_won_lock END;


/****** INDEXES ******/

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[account_movements]') AND name = N'IX_type_account_date')
DROP INDEX [IX_type_account_date] ON [dbo].[account_movements] WITH ( ONLINE = OFF );

IF  NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[account_movements]') AND name = N'IX_type_date_account')
CREATE NONCLUSTERED INDEX [IX_type_date_account] ON [dbo].[account_movements] 
(
      [am_type] ASC,
      [am_datetime] ASC,
      [am_account_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY];


IF  NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[handpays]') AND name = N'IX_hp_type_mov_date')
CREATE NONCLUSTERED INDEX [IX_hp_type_mov_date] ON [dbo].[handpays] 
(
      [hp_type] ASC,
      [hp_movement_id] ASC,
      [hp_datetime] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY];


/****** TRIGGERS ******/


/****** RECORDS ******/
   
/* Cashier.Voucher */
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.Voucher' AND GP_SUBJECT_KEY ='ReprintText')
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('Cashier.Voucher'
           ,'ReprintText'
           ,'');
ELSE
SELECT '***** Record Cashier.Voucher_ReprintText already exists *****';

/* GUI Forms */
UPDATE [dbo].[gui_forms]
   SET gf_nls_id = 13713
 WHERE gf_gui_id = 14
   AND gf_form_id = 34
   AND gf_nls_id = 13800;

   
/*** Default GUI User Profiles ***/   

/* Check GUI Functionalities: if any of them does not exist it will be created */

DECLARE @form_order AS int;

SELECT @form_order = ISNULL(max(gf_form_order) + 1, 0) FROM gui_forms WHERE gf_gui_id = 14;

IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 14 AND GF_FORM_ID = 1)
BEGIN  
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])    
       VALUES (14,          1,            @form_order,     3883);		/* FORM_USER_PROFILE_SEL */ 
  SET @form_order = @form_order + 1;       
END;

IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 14 AND GF_FORM_ID = 2)
BEGIN  
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])    
       VALUES (14,          2,            @form_order,     3825);		/* FORM_USER_EDIT */
  SET @form_order = @form_order + 1;
END;
 
IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 14 AND GF_FORM_ID = 3)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])
       VALUES (14,	    3,            @form_order,     3818);          /* FORM_PROFILE_EDIT */
  SET @form_order = @form_order + 1;
END;

IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 14 AND GF_FORM_ID = 4)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])
       VALUES (14,	    4,            @form_order,     6327);          /* FORM_GUI_AUDITOR */       
  SET @form_order = @form_order + 1;
END;
     
IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 14 AND GF_FORM_ID = 5)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])
       VALUES (14,	    5,            @form_order,     6267);          /* FORM_EVENTS_HISTORY */       
  SET @form_order = @form_order + 1;       
END;
     
IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 14 AND GF_FORM_ID = 6)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])
       VALUES (14,	  6,              @form_order,     6328);          /* FORM_TERMINALS_SELECTION */
  SET @form_order = @form_order + 1;       
END;
     
IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 14 AND GF_FORM_ID = 61)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])
       VALUES (14,	  61,             @form_order,     6365);          /* FORM_TERMINALS_EDIT */       
  SET @form_order = @form_order + 1;
END;
     
IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 14 AND GF_FORM_ID = 49)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])
       VALUES (14,	    49,           @form_order,     6461);          /* FORM_TERMINALS_3GS_EDIT */       
  SET @form_order = @form_order + 1;       
END;

IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 14 AND GF_FORM_ID = 55)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])
       VALUES (14,	    55,           @form_order,     6463);          /* FORM_TERMINALS_PENDING */
  SET @form_order = @form_order + 1;
END;

IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 14 AND GF_FORM_ID = 30)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])
       VALUES (14,	    30,           @form_order,     6460);          /* FORM_GENERAL_PARAMS */       
  SET @form_order = @form_order + 1;
END;

IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 14 AND GF_FORM_ID = 7)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])
       VALUES (14,	    7,            @form_order,     11359);         /* FORM_STATISTICS_TERMINALS */
  SET @form_order = @form_order + 1;       
END;

IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 14 AND GF_FORM_ID = 8)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])
       VALUES (14,	    8,            @form_order,     11360);         /* FORM_STATISTICS_GAMES */       
  SET @form_order = @form_order + 1;       
END;

IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 14 AND GF_FORM_ID = 9)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])
       VALUES (14,	    9,            @form_order,     11361);         /* FORM_STATISTICS_DATES */
  SET @form_order = @form_order + 1;
END;

IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 14 AND GF_FORM_ID = 10)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])
       VALUES (14,	    10,           @form_order,     11362);         /* FORM_STATISTICS_HOURS */
  SET @form_order = @form_order + 1;
END;

IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 14 AND GF_FORM_ID = 11)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])
       VALUES (14,	    11,           @form_order,     11363);         /* FORM_STATISTICS_CARDS */
  SET @form_order = @form_order + 1;
END;

IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 14 AND GF_FORM_ID = 12)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])
       VALUES (14,	    12,           @form_order,     11357);         /* FORM_PLAYS_SESSIONS */
  SET @form_order = @form_order + 1;       
END;
     
IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 14 AND GF_FORM_ID = 13)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])
       VALUES (14,	    13,           @form_order,     11354);         /* FORM_PLAYS_PLAYS */       
  SET @form_order = @form_order + 1;
END;

IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 14 AND GF_FORM_ID = 14)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])
       VALUES (14,	    14,           @form_order,     11355);         /* FORM_WS_SESSIONS */       
  SET @form_order = @form_order + 1;
END;

IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 14 AND GF_FORM_ID = 15)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])
       VALUES (14,	    15,           @form_order,     11366);         /* FORM_STATISTICS_HOURS_GROUP */
  SET @form_order = @form_order + 1;
END;

IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 14 AND GF_FORM_ID = 16)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])
       VALUES (14,	    16,           @form_order,     13204);         /* FORM_ACCT_TAXES */
  SET @form_order = @form_order + 1;
END;

IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 14 AND GF_FORM_ID = 17)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])
       VALUES (14,	    17,           @form_order,     13217);         /* FORM_CASHIER_MOVS */
  SET @form_order = @form_order + 1;
END;

IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 14 AND GF_FORM_ID = 18)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])
       VALUES (14,	    18,           @form_order,     13227);         /* FORM_CASHIER_SESSIONS */       
  SET @form_order = @form_order + 1;
END;

IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 14 AND GF_FORM_ID = 19)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])
       VALUES (14,	    19,           @form_order,     13228);         /* FORM_ACCOUNT_MOVEMENTS */
  SET @form_order = @form_order + 1;
END;

IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 14 AND GF_FORM_ID = 20)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])
       VALUES (14,	    20,           @form_order,     13229);        /* FORM_ACCOUNT_SUMMARY */
  SET @form_order = @form_order + 1;
END;

IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 14 AND GF_FORM_ID = 21)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])
       VALUES (14,	    21,           @form_order,     13249);         /* FORM_DATE_TERMINAL */
  SET @form_order = @form_order + 1;
END;

IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 14 AND GF_FORM_ID = 22)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])
       VALUES (14,	    22,           @form_order,     15952);         /* FORM_CLASS_II_DRAW_AUDIT */
  SET @form_order = @form_order + 1;
END;

IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 14 AND GF_FORM_ID = 24)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])
       VALUES (14,	    24,           @form_order,     11731);         /* FORM_CLASS_II_JACKPOT_CONFIGURATION */
  SET @form_order = @form_order + 1;
END;

IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 14 AND GF_FORM_ID = 36)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])
       VALUES (14,	    36,           @form_order,     11735);         /* FORM_CLASS_II_JACKPOT_PROMO_MESSAGE */
  SET @form_order = @form_order + 1;       
END;

IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 14 AND GF_FORM_ID = 25)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])
       VALUES (14,	    25,           @form_order,     11732);         /* FORM_CLASS_II_JACKPOT_HISTORY */
  SET @form_order = @form_order + 1;
END;

IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 14 AND GF_FORM_ID = 26)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])
       VALUES (14,	    26,           @form_order,     11733);         /* FORM_CLASS_II_JACKPOT_MONITOR */
  SET @form_order = @form_order + 1;
END;

IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 14 AND GF_FORM_ID = 27)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])
       VALUES (14,	    27,           @form_order,     13701);         /* FORM_SW_DOWNLOAD_VERSIONS */
  SET @form_order = @form_order + 1;
END;

IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 14 AND GF_FORM_ID = 28)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])
       VALUES (14,	    28,           @form_order,     13800);         /* FORM_SW_DOWNLOAD_DETAILS */
  SET @form_order = @form_order + 1;
END;

IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 14 AND GF_FORM_ID = 29)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])
       VALUES (14,	    29,           @form_order,     15958);         /* FORM_CARD_RECORD */
  SET @form_order = @form_order + 1;
END;

IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 14 AND GF_FORM_ID = 31)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])
       VALUES (14,	    31,           @form_order,     13465);         /* FORM_PROVIDER_DAY_TERMINAL */
  SET @form_order = @form_order + 1;
END;

IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 14 AND GF_FORM_ID = 32)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])
       VALUES (14,	    32,           @form_order,     12201);         /* FORM_SERVICE_MONITOR */
  SET @form_order = @form_order + 1;
END;

IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 14 AND GF_FORM_ID = 34)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])
       VALUES (14,	    34,           @form_order,     13713);         /* FORM_TERMINALS_BY_BUILD */
  SET @form_order = @form_order + 1;
END;

IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 14 AND GF_FORM_ID = 35)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])
       VALUES (14,	    35,           @form_order,     13296);         /* FORM_CASHIER_SESSION_DETAIL */
  SET @form_order = @form_order + 1;
END;

IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 14 AND GF_FORM_ID = 37)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])
       VALUES (14,	    37,           @form_order,     3701);          /* FORM_CASHIER_CONFIGURATION */
  SET @form_order = @form_order + 1;
END;

IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 14 AND GF_FORM_ID = 39)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])
       VALUES (14,	    39,           @form_order,     13739);         /* FORM_MISC_SW_BY_BUILD */
  SET @form_order = @form_order + 1;
END;

IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 14 AND GF_FORM_ID = 38)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])
       VALUES (14,	    38,           @form_order,     13729);         /* FORM_LICENCE_VERSIONS */
  SET @form_order = @form_order + 1;
END;

IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 14 AND GF_FORM_ID = 40)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])
       VALUES (14,	    40,           @form_order,     13751);         /* FORM_LICENCE_DETAILS */
  SET @form_order = @form_order + 1;
END;

IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 14 AND GF_FORM_ID = 47)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])
       VALUES (14,	    47,           @form_order,     16211);         /* FORM_PROMOTIONS */
  SET @form_order = @form_order + 1;
END;

IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 14 AND GF_FORM_ID = 48)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])
       VALUES (14,	    48,           @form_order,     16257);         /* FORM_PROMOTION_EDIT */
  SET @form_order = @form_order + 1;
END;

IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 14 AND GF_FORM_ID = 50)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])
       VALUES (14,	    50,           @form_order,     16210);         /* FORM_DRAWS */
  SET @form_order = @form_order + 1;
END;

IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 14 AND GF_FORM_ID = 51)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])
       VALUES (14,	    51,           @form_order,     16320);         /* FORM_DRAW_EDIT */
  SET @form_order = @form_order + 1;
END;

IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 14 AND GF_FORM_ID = 45)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])
       VALUES (14,	    45,           @form_order,     3968);          /* FORM_TERMINAL_GAME_RELATION */
  SET @form_order = @form_order + 1;
END;

IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 14 AND GF_FORM_ID = 46)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])
       VALUES (14,	    46,           @form_order,     13469);         /* FORM_EXPIRED_CREDITS */
  SET @form_order = @form_order + 1;
END;

IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 14 AND GF_FORM_ID = 147)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])
       VALUES (14,	    147,          @form_order,     11395);         /* FORM_GAME_METERS */
  SET @form_order = @form_order + 1;
END;

IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 14 AND GF_FORM_ID = 52)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])
       VALUES (14,	    52,           @form_order,     5421);          /* FORM_COMMANDS_SEND */
  SET @form_order = @form_order + 1;
END;

IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 14 AND GF_FORM_ID = 53)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])
       VALUES (14,	    53,           @form_order,     5422);          /* FORM_COMMANDS_HISTORY */
  SET @form_order = @form_order + 1;
END;

IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 14 AND GF_FORM_ID = 66)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])
       VALUES (14,	    66,           @form_order,     13361);         /* FORM_TERMINALS_WITHOUT_ACTIVITY */
  SET @form_order = @form_order + 1;
END;

IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 14 AND GF_FORM_ID = 54)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])
       VALUES (14,	    54,           @form_order,     6462);          /* FORM_HANDPAYS */
  SET @form_order = @form_order + 1;
END;

IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 14 AND GF_FORM_ID = 56)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])
       VALUES (14,	    56,           @form_order,     13313);         /* FORM_MOBILE_BANK_MOVEMENTS */
  SET @form_order = @form_order + 1;
END;

IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 14 AND GF_FORM_ID = 58)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])
       VALUES (14,	    58,           @form_order,     16349);         /* FORM_GIFTS_SEL */
  SET @form_order = @form_order + 1;
END;

IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 14 AND GF_FORM_ID = 59)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])
       VALUES (14,	    59,           @form_order,     16352);         /* FORM_GIFTS_EDIT */
  SET @form_order = @form_order + 1;
END;

IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 14 AND GF_FORM_ID = 65)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])
       VALUES (14,	    65,           @form_order,     16429);         /* FORM_GIFTS_HISTORY */
  SET @form_order = @form_order + 1;
END;

IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 14 AND GF_FORM_ID = 60)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])
       VALUES (14,	    60,           @form_order,     13346);         /* FORM_TAXES_REPORT */
  SET @form_order = @form_order + 1;
END;

IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 14 AND GF_FORM_ID = 57)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])
       VALUES (14,	    57,           @form_order,     13345);         /* FORM_PROMOTION_REPORT */
  SET @form_order = @form_order + 1;
END;

IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 14 AND GF_FORM_ID = 63)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])
       VALUES (14,	    63,           @form_order,     3982);          /* FORM_SITE_GAP_REPORT */
  SET @form_order = @form_order + 1;
END;

IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 14 AND GF_FORM_ID = 62)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])
       VALUES (14,	    62,           @form_order,     11830);         /* FORM_SITE_JACKPOT_CONFIGURATION */
  SET @form_order = @form_order + 1;
END;

IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 14 AND GF_FORM_ID = 64)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])
       VALUES (14,	    64,           @form_order,     11850);         /* FORM_SITE_JACKPOT_HISTORY */
  SET @form_order = @form_order + 1;
END;

IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 14 AND GF_FORM_ID = 67)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])
       VALUES (14,	    67,           @form_order,     11425);         /* FORM_MAILING_PROGRAMMING_SEL */
  SET @form_order = @form_order + 1;
END;

IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 14 AND GF_FORM_ID = 68)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])
       VALUES (14,	    68,           @form_order,     11426);         /* FORM_MAILING_PROGRAMMING_EDIT */
  SET @form_order = @form_order + 1;
END;

IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 14 AND GF_FORM_ID = 69)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])
       VALUES (14,	    69,           @form_order,     11437);         /* FORM_MAILING_PARAMETERS */
  SET @form_order = @form_order + 1;
END;
     
 		/* CASHIER */
SELECT @form_order = ISNULL(max(gf_form_order) + 1, 0) FROM gui_forms WHERE gf_gui_id = 15;
 		
IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 15 AND GF_FORM_ID = 0)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])    
       VALUES (15,	    0,            @form_order,     3845);         /* FORM_MAIN */
  SET @form_order = @form_order + 1;
END;
     
IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 15 AND GF_FORM_ID = 1)
BEGIN  
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])    
       VALUES (15,	    1,            @form_order,     3846);         /* FORM_VIRTUAL_ADD_NO_REDEEM */
  SET @form_order = @form_order + 1;
END;
     
IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 15 AND GF_FORM_ID = 2)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])    
       VALUES (15,	    2,            @form_order,     3847);         /* FORM_VIRTUAL_CASH_DESK_OPEN_CLOSE */
  SET @form_order = @form_order + 1;
END;
     
IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 15 AND GF_FORM_ID = 3)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])    
       VALUES (15,	    3,            @form_order,     3849);         /* FORM_VIRTUAL_CASH_DESK_DEPOSIT */
  SET @form_order = @form_order + 1;
END;
     
IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 15 AND GF_FORM_ID = 4)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])    
       VALUES (15,	    4,            @form_order,     3850);         /* FORM_VIRTUAL_CASH_DESK_WITHDRAWN */
  SET @form_order = @form_order + 1;
END;
     
IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 15 AND GF_FORM_ID = 5)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])    
       VALUES (15,	    5,            @form_order,     3851);         /* FORM_VIRTUAL_CARD_OPER_ADD_REDEEM_CREDIT */
  SET @form_order = @form_order + 1;
END;
     
IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 15 AND GF_FORM_ID = 6)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])    
       VALUES (15,	    6,            @form_order,     3854);         /* FORM_VIRTUAL_CARD_OPER_LAST_MOV */
  SET @form_order = @form_order + 1;
END;
     
IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 15 AND GF_FORM_ID = 7)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])    
       VALUES (15,	    7,            @form_order,     3855);         /* FORM_VIRTUAL_ACCOUNT_OPER_CARD */
  SET @form_order = @form_order + 1;
END;
     
IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 15 AND GF_FORM_ID = 8)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])    
       VALUES (15,	    8,            @form_order,     3856);         /* FORM_VIRTUAL_ACCOUNT_OPER_EDIT */
  SET @form_order = @form_order + 1;
END;
     
IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 15 AND GF_FORM_ID = 9)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])    
       VALUES (15,	    9,            @form_order,     3857);         /* FORM_VIRTUAL_ACCOUNT_OPER_LOCK */
  SET @form_order = @form_order + 1;
END;
     
IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 15 AND GF_FORM_ID = 10)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])    
       VALUES (15,	    10,           @form_order,     3863);         /* FORM_VIRTUAL_PRINT_CARD */
  SET @form_order = @form_order + 1;
END;
     
IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 15 AND GF_FORM_ID = 11)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])    
       VALUES (15,	    11,           @form_order,     3859);         /* FORM_VIRTUAL_OPTIONS_DB_CONFIG */
  SET @form_order = @form_order + 1;
END;
     
IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 15 AND GF_FORM_ID = 12)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])    
       VALUES (15,	    12,           @form_order,     3860);         /* FORM_VIRTUAL_OPTIONS_LANGUAGE */
  SET @form_order = @form_order + 1;
END;
     
IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 15 AND GF_FORM_ID = 13)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])    
       VALUES (15,	    13,           @form_order,     3861);         /* FORM_VIRTUAL_OPTIONS_CALIBRATE */
  SET @form_order = @form_order + 1;
END;
     
IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 15 AND GF_FORM_ID = 14)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])    
       VALUES (15,	    14,           @form_order,     3862);         /* FORM_VIRTUAL_GAME_SESSIONS */
  SET @form_order = @form_order + 1;
END;
     
IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 15 AND GF_FORM_ID = 15)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])    
       VALUES (15,	    15,           @form_order,     3858);         /* FORM_VIRTUAL_MOBILE_BANK */
  SET @form_order = @form_order + 1;
END;
     
IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 15 AND GF_FORM_ID = 16)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])    
       VALUES (15,	    16,           @form_order,     3848);         /* FORM_VIRTUAL_CASH_DESK_ACCESS */
  SET @form_order = @form_order + 1;
END;
     
IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 15 AND GF_FORM_ID = 17)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])    
       VALUES (15,	    17,           @form_order,     3864);         /* FORM_VIRTUAL_ACCOUNT_OPER_HANDPAY */
  SET @form_order = @form_order + 1;
END;
     
IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 15 AND GF_FORM_ID = 18)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])    
       VALUES (15,	    18,           @form_order,     3865);         /* FORM_VIRTUAL_ACCOUNT_OPER_HANDPAY_CANCEL */
  SET @form_order = @form_order + 1;
END;
     
IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 15 AND GF_FORM_ID = 19)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])    
       VALUES (15,	    19,           @form_order,     3978);         /* FORM_VIRTUAL_ACCOUNT_LOG_OFF */
  SET @form_order = @form_order + 1;
END;
     
IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 15 AND GF_FORM_ID = 20)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])    
       VALUES (15,	    20,           @form_order,     3866);         /* FORM_VIRTUAL_MANUAL_OPER_HANDPAY */
  SET @form_order = @form_order + 1;
END;
     
IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 15 AND GF_FORM_ID = 21)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])    
       VALUES (15,	    21,           @form_order,     3867);         /* FORM_VIRTUAL_MANUAL_OPER_HANDPAY_CANCEL */
  SET @form_order = @form_order + 1;
END;
     
IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 15 AND GF_FORM_ID = 22)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])    
       VALUES (15,	    22,           @form_order,     3872);         /* FORM_VIRTUAL_GIFT_REQUEST */
  SET @form_order = @form_order + 1;
END;
     
IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 15 AND GF_FORM_ID = 23)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])    
       VALUES (15,	    23,           @form_order,     3873);         /* FORM_VIRTUAL_GIFT_DELIVERY */
  SET @form_order = @form_order + 1;
END;
     
IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 15 AND GF_FORM_ID = 24)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])    
       VALUES (15,	    24,           @form_order,     3979);         /* FORM_VIRTUAL_ACCOUNT_LEVEL */
  SET @form_order = @form_order + 1;
END;
     
IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 15 AND GF_FORM_ID = 25)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])    
       VALUES (15,	    25,           @form_order,     3980);         /* FORM_VIRTUAL_PROMO_SPECIAL_PERMISSION_A */
  SET @form_order = @form_order + 1;
END;

IF NOT EXISTS (SELECT * FROM GUI_FORMS WHERE GF_GUI_ID = 15 AND GF_FORM_ID = 26)
BEGIN
  INSERT INTO [dbo].[gui_forms] 
              ([gf_gui_id], [gf_form_id], [gf_form_order], [gf_nls_id])    
       VALUES (15,	    26,           @form_order,     3981);         /* FORM_VIRTUAL_PROMO_SPECIAL_PERMISSION_B */
  SET @form_order = @form_order + 1;
END;

/*****  AUDITOR DE SALA  *****/
IF NOT EXISTS (SELECT * FROM gui_user_profiles WHERE gup_name = 'AUDITOR DE SALA')
BEGIN

  DECLARE @new_profile_id0 AS int
  
  SELECT @new_profile_id0 = max (gup_profile_id) + 1 FROM gui_user_profiles

  INSERT INTO [dbo].[gui_user_profiles]
             ([gup_profile_id]
             ,[gup_name])
       VALUES
             ( @new_profile_id0
             , 'AUDITOR DE SALA');
 
        INSERT INTO [dbo].[gui_profile_forms]
                   ([gpf_profile_id], [gpf_gui_id], [gpf_form_id], [gpf_read_perm], [gpf_write_perm], [gpf_delete_perm], [gpf_execute_perm])
                                        /*  FORM  R   W   D   X    */
             SELECT  @new_profile_id0,  14,  1,   1,  0,  1,  1    /* FORM_USER_PROFILE_SEL */
   UNION ALL SELECT  @new_profile_id0,  14,  2,   1,  0,  1,  1    /* FORM_USER_EDIT */
   UNION ALL SELECT  @new_profile_id0,  14,  3,   1,  0,  1,  1    /* FORM_PROFILE_EDIT */
   UNION ALL SELECT  @new_profile_id0,  14,  4,   0,  0,  0,  0    /* FORM_GUI_AUDITOR */
   UNION ALL SELECT  @new_profile_id0,  14,  5,   0,  0,  0,  0    /* FORM_EVENTS_HISTORY */
   UNION ALL SELECT  @new_profile_id0,  14,  6,   0,  0,  0,  0    /* FORM_TERMINALS_SELECTION */
   UNION ALL SELECT  @new_profile_id0,  14,  7,   1,  0,  1,  1    /* FORM_STATISTICS_TERMINALS */
   UNION ALL SELECT  @new_profile_id0,  14,  8,   1,  0,  1,  1    /* FORM_STATISTICS_GAMES */
   UNION ALL SELECT  @new_profile_id0,  14,  9,   1,  0,  1,  1    /* FORM_STATISTICS_DATES */
   UNION ALL SELECT  @new_profile_id0,  14,  10,  1,  0,  1,  1    /* FORM_STATISTICS_HOURS */
   UNION ALL SELECT  @new_profile_id0,  14,  11,  1,  0,  1,  1    /* FORM_STATISTICS_CARDS */
   UNION ALL SELECT  @new_profile_id0,  14,  12,  1,  0,  1,  1    /* FORM_PLAYS_SESSIONS */
   UNION ALL SELECT  @new_profile_id0,  14,  13,  1,  0,  1,  1    /* FORM_PLAYS_PLAYS */
   UNION ALL SELECT  @new_profile_id0,  14,  14,  1,  0,  1,  1    /* FORM_WS_SESSIONS */
   UNION ALL SELECT  @new_profile_id0,  14,  15,  1,  0,  1,  1    /* FORM_STATISTICS_HOURS_GROUP */
   UNION ALL SELECT  @new_profile_id0,  14,  16,  1,  0,  1,  1    /* FORM_ACCT_TAXES */
   UNION ALL SELECT  @new_profile_id0,  14,  17,  1,  0,  1,  1    /* FORM_CASHIER_MOVS */
   UNION ALL SELECT  @new_profile_id0,  14,  18,  1,  0,  1,  1    /* FORM_CASHIER_SESSIONS */
   UNION ALL SELECT  @new_profile_id0,  14,  19,  1,  0,  1,  1    /* FORM_ACCOUNT_MOVEMENTS */
   UNION ALL SELECT  @new_profile_id0,  14,  20,  1,  0,  1,  1    /* FORM_ACCOUNT_SUMMARY */
   UNION ALL SELECT  @new_profile_id0,  14,  21,  1,  0,  1,  1    /* FORM_DATE_TERMINAL */
   UNION ALL SELECT  @new_profile_id0,  14,  22,  0,  0,  0,  0    /* FORM_CLASS_II_DRAW_AUDIT */
   UNION ALL SELECT  @new_profile_id0,  14,  24,  0,  0,  0,  0    /* FORM_CLASS_II_JACKPOT_CONFIGURATION */
   UNION ALL SELECT  @new_profile_id0,  14,  25,  0,  0,  0,  0    /* FORM_CLASS_II_JACKPOT_HISTORY */
   UNION ALL SELECT  @new_profile_id0,  14,  26,  0,  0,  0,  0    /* FORM_CLASS_II_JACKPOT_MONITOR */
   UNION ALL SELECT  @new_profile_id0,  14,  27,  0,  0,  0,  0    /* FORM_SW_DOWNLOAD_VERSIONS */
   UNION ALL SELECT  @new_profile_id0,  14,  28,  0,  0,  0,  0    /* FORM_SW_DOWNLOAD_DETAILS */
   UNION ALL SELECT  @new_profile_id0,  14,  29,  0,  0,  0,  0    /* FORM_CARD_RECORD */
   UNION ALL SELECT  @new_profile_id0,  14,  30,  0,  0,  0,  0    /* FORM_GENERAL_PARAMS */
   UNION ALL SELECT  @new_profile_id0,  14,  31,  1,  0,  1,  1    /* FORM_PROVIDER_DAY_TERMINAL */
   UNION ALL SELECT  @new_profile_id0,  14,  32,  0,  0,  0,  0    /* FORM_SERVICE_MONITOR */
   UNION ALL SELECT  @new_profile_id0,  14,  34,  0,  0,  0,  0    /* FORM_TERMINALS_BY_BUILD */
   UNION ALL SELECT  @new_profile_id0,  14,  35,  1,  0,  1,  1    /* FORM_CASHIER_SESSION_DETAIL */
   UNION ALL SELECT  @new_profile_id0,  14,  36,  0,  0,  0,  0    /* FORM_CLASS_II_JACKPOT_PROMO_MESSAGE */
   UNION ALL SELECT  @new_profile_id0,  14,  37,  0,  0,  0,  0    /* FORM_CASHIER_CONFIGURATION */
   UNION ALL SELECT  @new_profile_id0,  14,  38,  0,  0,  0,  0    /* FORM_LICENCE_VERSIONS */
   UNION ALL SELECT  @new_profile_id0,  14,  39,  0,  0,  0,  0    /* FORM_MISC_SW_BY_BUILD */
   UNION ALL SELECT  @new_profile_id0,  14,  40,  0,  0,  0,  0    /* FORM_LICENCE_DETAILS */
   UNION ALL SELECT  @new_profile_id0,  14,  45,  0,  0,  0,  0    /* FORM_TERMINAL_GAME_RELATION */
   UNION ALL SELECT  @new_profile_id0,  14,  46,  1,  0,  1,  1    /* FORM_EXPIRED_CREDITS */
   UNION ALL SELECT  @new_profile_id0,  14,  47,  1,  0,  1,  1    /* FORM_PROMOTIONS */
   UNION ALL SELECT  @new_profile_id0,  14,  48,  0,  0,  0,  0    /* FORM_PROMOTION_EDIT */
   UNION ALL SELECT  @new_profile_id0,  14,  49,  0,  0,  0,  0    /* FORM_TERMINALS_3GS_EDIT */
   UNION ALL SELECT  @new_profile_id0,  14,  50,  1,  0,  1,  1    /* FORM_DRAWS */
   UNION ALL SELECT  @new_profile_id0,  14,  51,  0,  0,  0,  0    /* FORM_DRAW_EDIT */
   UNION ALL SELECT  @new_profile_id0,  14,  52,  0,  0,  0,  0    /* FORM_COMMANDS_SEND */
   UNION ALL SELECT  @new_profile_id0,  14,  53,  0,  0,  0,  0    /* FORM_COMMANDS_HISTORY */
   UNION ALL SELECT  @new_profile_id0,  14,  54,  1,  0,  1,  1    /* FORM_HANDPAYS */
   UNION ALL SELECT  @new_profile_id0,  14,  55,  0,  0,  0,  0    /* FORM_TERMINALS_PENDING */
   UNION ALL SELECT  @new_profile_id0,  14,  56,  1,  0,  1,  1    /* FORM_MOBILE_BANK_MOVEMENTS */
   UNION ALL SELECT  @new_profile_id0,  14,  57,  1,  0,  1,  1    /* FORM_PROMOTION_REPORT */
   UNION ALL SELECT  @new_profile_id0,  14,  58,  1,  0,  1,  1    /* FORM_GIFTS_SEL */
   UNION ALL SELECT  @new_profile_id0,  14,  59,  1,  0,  1,  1    /* FORM_GIFTS_EDIT */
   UNION ALL SELECT  @new_profile_id0,  14,  60,  1,  0,  1,  1    /* FORM_TAXES_REPORT */
   UNION ALL SELECT  @new_profile_id0,  14,  61,  0,  0,  0,  0    /* FORM_TERMINALS_EDIT */
   UNION ALL SELECT  @new_profile_id0,  14,  62,  0,  0,  0,  0    /* FORM_SITE_JACKPOT_CONFIGURATION */
   UNION ALL SELECT  @new_profile_id0,  14,  63,  1,  0,  1,  1    /* FORM_SITE_GAP_REPORT */
   UNION ALL SELECT  @new_profile_id0,  14,  64,  0,  0,  0,  0    /* FORM_SITE_JACKPOT_HISTORY */
   UNION ALL SELECT  @new_profile_id0,  14,  65,  1,  0,  1,  1    /* FORM_GIFTS_HISTORY */
   UNION ALL SELECT  @new_profile_id0,  14,  66,  1,  0,  1,  1    /* FORM_TERMINALS_WITHOUT_ACTIVITY */
   UNION ALL SELECT  @new_profile_id0,  14,  67,  0,  0,  0,  0    /* FORM_MAILING_PROGRAMMING_SEL */
   UNION ALL SELECT  @new_profile_id0,  14,  68,  0,  0,  0,  0    /* FORM_MAILING_PROGRAMMING_EDIT */
   UNION ALL SELECT  @new_profile_id0,  14,  69,  0,  0,  0,  0    /* FORM_MAILING_PARAMETERS */
   UNION ALL SELECT  @new_profile_id0,  14,  147, 0,  0,  0,  0    /* FORM_GAME_METERS */
   		/* CASHIER */
   UNION ALL SELECT  @new_profile_id0,  15,  0,   0,  0,  0,  0    /* FORM_MAIN */
   UNION ALL SELECT  @new_profile_id0,  15,  1,   0,  0,  0,  0    /* FORM_VIRTUAL_ADD_NO_REDEEM */
   UNION ALL SELECT  @new_profile_id0,  15,  2,   0,  0,  0,  0    /* FORM_VIRTUAL_CASH_DESK_OPEN_CLOSE */
   UNION ALL SELECT  @new_profile_id0,  15,  3,   0,  0,  0,  0    /* FORM_VIRTUAL_CASH_DESK_DEPOSIT */
   UNION ALL SELECT  @new_profile_id0,  15,  4,   0,  0,  0,  0    /* FORM_VIRTUAL_CASH_DESK_WITHDRAWN */
   UNION ALL SELECT  @new_profile_id0,  15,  5,   0,  0,  0,  0    /* FORM_VIRTUAL_CARD_OPER_ADD_REDEEM_CREDIT */
   UNION ALL SELECT  @new_profile_id0,  15,  6,   0,  0,  0,  0    /* FORM_VIRTUAL_CARD_OPER_LAST_MOV */
   UNION ALL SELECT  @new_profile_id0,  15,  7,   0,  0,  0,  0    /* FORM_VIRTUAL_ACCOUNT_OPER_CARD */
   UNION ALL SELECT  @new_profile_id0,  15,  8,   0,  0,  0,  0    /* FORM_VIRTUAL_ACCOUNT_OPER_EDIT */
   UNION ALL SELECT  @new_profile_id0,  15,  9,   0,  0,  0,  0    /* FORM_VIRTUAL_ACCOUNT_OPER_LOCK */
   UNION ALL SELECT  @new_profile_id0,  15,  10,  0,  0,  0,  0    /* FORM_VIRTUAL_PRINT_CARD */
   UNION ALL SELECT  @new_profile_id0,  15,  11,  0,  0,  0,  0    /* FORM_VIRTUAL_OPTIONS_DB_CONFIG */
   UNION ALL SELECT  @new_profile_id0,  15,  12,  0,  0,  0,  0    /* FORM_VIRTUAL_OPTIONS_LANGUAGE */
   UNION ALL SELECT  @new_profile_id0,  15,  13,  0,  0,  0,  0    /* FORM_VIRTUAL_OPTIONS_CALIBRATE */
   UNION ALL SELECT  @new_profile_id0,  15,  14,  0,  0,  0,  0    /* FORM_VIRTUAL_GAME_SESSIONS */
   UNION ALL SELECT  @new_profile_id0,  15,  15,  0,  0,  0,  0    /* FORM_VIRTUAL_MOBILE_BANK */
   UNION ALL SELECT  @new_profile_id0,  15,  16,  0,  0,  0,  0    /* FORM_VIRTUAL_CASH_DESK_ACCESS */
   UNION ALL SELECT  @new_profile_id0,  15,  17,  0,  0,  0,  0    /* FORM_VIRTUAL_ACCOUNT_OPER_HANDPAY */
   UNION ALL SELECT  @new_profile_id0,  15,  18,  0,  0,  0,  0    /* FORM_VIRTUAL_ACCOUNT_OPER_HANDPAY_CANCEL */
   UNION ALL SELECT  @new_profile_id0,  15,  19,  0,  0,  0,  0    /* FORM_VIRTUAL_ACCOUNT_LOG_OFF */
   UNION ALL SELECT  @new_profile_id0,  15,  20,  0,  0,  0,  0    /* FORM_VIRTUAL_MANUAL_OPER_HANDPAY */
   UNION ALL SELECT  @new_profile_id0,  15,  21,  0,  0,  0,  0    /* FORM_VIRTUAL_MANUAL_OPER_HANDPAY_CANCEL */
   UNION ALL SELECT  @new_profile_id0,  15,  22,  0,  0,  0,  0    /* FORM_VIRTUAL_GIFT_REQUEST */
   UNION ALL SELECT  @new_profile_id0,  15,  23,  0,  0,  0,  0    /* FORM_VIRTUAL_GIFT_DELIVERY */
   UNION ALL SELECT  @new_profile_id0,  15,  24,  0,  0,  0,  0    /* FORM_VIRTUAL_ACCOUNT_LEVEL */
   UNION ALL SELECT  @new_profile_id0,  15,  25,  0,  0,  0,  0    /* FORM_VIRTUAL_PROMO_SPECIAL_PERMISSION_A */
   UNION ALL SELECT  @new_profile_id0,  15,  26,  0,  0,  0,  0    /* FORM_VIRTUAL_PROMO_SPECIAL_PERMISSION_B */
 
END
ELSE
  SELECT '***** Profile AUDITOR DE SALA already exists *****';


/*****  BANCO M�VIL  *****/

IF NOT EXISTS (SELECT * FROM gui_user_profiles WHERE gup_name = 'BANCO M�VIL')
BEGIN

  DECLARE @new_profile_id1 AS int
  
  SELECT @new_profile_id1 = max (gup_profile_id) + 1 FROM gui_user_profiles
  
  INSERT INTO [dbo].[gui_user_profiles]
             ([gup_profile_id]
             ,[gup_name])
       VALUES
             ( @new_profile_id1
             , 'BANCO M�VIL');
 
       INSERT INTO [dbo].[gui_profile_forms]
                  ([gpf_profile_id], [gpf_gui_id], [gpf_form_id], [gpf_read_perm], [gpf_write_perm], [gpf_delete_perm], [gpf_execute_perm])
                                       /*  FORM  R   W   D   X    */
            SELECT  @new_profile_id1,  14,  1,   0,  0,  0,  0    /* FORM_USER_PROFILE_SEL */
  UNION ALL SELECT  @new_profile_id1,  14,  2,   0,  0,  0,  0    /* FORM_USER_EDIT */
  UNION ALL SELECT  @new_profile_id1,  14,  3,   0,  0,  0,  0    /* FORM_PROFILE_EDIT */
  UNION ALL SELECT  @new_profile_id1,  14,  4,   0,  0,  0,  0    /* FORM_GUI_AUDITOR */
  UNION ALL SELECT  @new_profile_id1,  14,  5,   0,  0,  0,  0    /* FORM_EVENTS_HISTORY */
  UNION ALL SELECT  @new_profile_id1,  14,  6,   0,  0,  0,  0    /* FORM_TERMINALS_SELECTION */
  UNION ALL SELECT  @new_profile_id1,  14,  7,   0,  0,  0,  0    /* FORM_STATISTICS_TERMINALS */
  UNION ALL SELECT  @new_profile_id1,  14,  8,   0,  0,  0,  0    /* FORM_STATISTICS_GAMES */
  UNION ALL SELECT  @new_profile_id1,  14,  9,   0,  0,  0,  0    /* FORM_STATISTICS_DATES */
  UNION ALL SELECT  @new_profile_id1,  14,  10,  0,  0,  0,  0    /* FORM_STATISTICS_HOURS */
  UNION ALL SELECT  @new_profile_id1,  14,  11,  0,  0,  0,  0    /* FORM_STATISTICS_CARDS */
  UNION ALL SELECT  @new_profile_id1,  14,  12,  0,  0,  0,  0    /* FORM_PLAYS_SESSIONS */
  UNION ALL SELECT  @new_profile_id1,  14,  13,  0,  0,  0,  0    /* FORM_PLAYS_PLAYS */
  UNION ALL SELECT  @new_profile_id1,  14,  14,  0,  0,  0,  0    /* FORM_WS_SESSIONS */
  UNION ALL SELECT  @new_profile_id1,  14,  15,  0,  0,  0,  0    /* FORM_STATISTICS_HOURS_GROUP */
  UNION ALL SELECT  @new_profile_id1,  14,  16,  0,  0,  0,  0    /* FORM_ACCT_TAXES */
  UNION ALL SELECT  @new_profile_id1,  14,  17,  0,  0,  0,  0    /* FORM_CASHIER_MOVS */
  UNION ALL SELECT  @new_profile_id1,  14,  18,  0,  0,  0,  0    /* FORM_CASHIER_SESSIONS */
  UNION ALL SELECT  @new_profile_id1,  14,  19,  0,  0,  0,  0    /* FORM_ACCOUNT_MOVEMENTS */
  UNION ALL SELECT  @new_profile_id1,  14,  20,  0,  0,  0,  0    /* FORM_ACCOUNT_SUMMARY */
  UNION ALL SELECT  @new_profile_id1,  14,  21,  0,  0,  0,  0    /* FORM_DATE_TERMINAL */
  UNION ALL SELECT  @new_profile_id1,  14,  22,  0,  0,  0,  0    /* FORM_CLASS_II_DRAW_AUDIT */
  UNION ALL SELECT  @new_profile_id1,  14,  24,  0,  0,  0,  0    /* FORM_CLASS_II_JACKPOT_CONFIGURATION */
  UNION ALL SELECT  @new_profile_id1,  14,  25,  0,  0,  0,  0    /* FORM_CLASS_II_JACKPOT_HISTORY */
  UNION ALL SELECT  @new_profile_id1,  14,  26,  0,  0,  0,  0    /* FORM_CLASS_II_JACKPOT_MONITOR */
  UNION ALL SELECT  @new_profile_id1,  14,  27,  0,  0,  0,  0    /* FORM_SW_DOWNLOAD_VERSIONS */
  UNION ALL SELECT  @new_profile_id1,  14,  28,  0,  0,  0,  0    /* FORM_SW_DOWNLOAD_DETAILS */
  UNION ALL SELECT  @new_profile_id1,  14,  29,  0,  0,  0,  0    /* FORM_CARD_RECORD */
  UNION ALL SELECT  @new_profile_id1,  14,  30,  0,  0,  0,  0    /* FORM_GENERAL_PARAMS */
  UNION ALL SELECT  @new_profile_id1,  14,  31,  0,  0,  0,  0    /* FORM_PROVIDER_DAY_TERMINAL */
  UNION ALL SELECT  @new_profile_id1,  14,  32,  0,  0,  0,  0    /* FORM_SERVICE_MONITOR */
  UNION ALL SELECT  @new_profile_id1,  14,  34,  0,  0,  0,  0    /* FORM_TERMINALS_BY_BUILD */
  UNION ALL SELECT  @new_profile_id1,  14,  35,  0,  0,  0,  0    /* FORM_CASHIER_SESSION_DETAIL */
  UNION ALL SELECT  @new_profile_id1,  14,  36,  0,  0,  0,  0    /* FORM_CLASS_II_JACKPOT_PROMO_MESSAGE */
  UNION ALL SELECT  @new_profile_id1,  14,  37,  0,  0,  0,  0    /* FORM_CASHIER_CONFIGURATION */
  UNION ALL SELECT  @new_profile_id1,  14,  38,  0,  0,  0,  0    /* FORM_LICENCE_VERSIONS */
  UNION ALL SELECT  @new_profile_id1,  14,  39,  0,  0,  0,  0    /* FORM_MISC_SW_BY_BUILD */
  UNION ALL SELECT  @new_profile_id1,  14,  40,  0,  0,  0,  0    /* FORM_LICENCE_DETAILS */
  UNION ALL SELECT  @new_profile_id1,  14,  45,  0,  0,  0,  0    /* FORM_TERMINAL_GAME_RELATION */
  UNION ALL SELECT  @new_profile_id1,  14,  46,  0,  0,  0,  0    /* FORM_EXPIRED_CREDITS */
  UNION ALL SELECT  @new_profile_id1,  14,  47,  0,  0,  0,  0    /* FORM_PROMOTIONS */
  UNION ALL SELECT  @new_profile_id1,  14,  48,  0,  0,  0,  0    /* FORM_PROMOTION_EDIT */
  UNION ALL SELECT  @new_profile_id1,  14,  49,  0,  0,  0,  0    /* FORM_TERMINALS_3GS_EDIT */
  UNION ALL SELECT  @new_profile_id1,  14,  50,  0,  0,  0,  0    /* FORM_DRAWS */
  UNION ALL SELECT  @new_profile_id1,  14,  51,  0,  0,  0,  0    /* FORM_DRAW_EDIT */
  UNION ALL SELECT  @new_profile_id1,  14,  52,  0,  0,  0,  0    /* FORM_COMMANDS_SEND */
  UNION ALL SELECT  @new_profile_id1,  14,  53,  0,  0,  0,  0    /* FORM_COMMANDS_HISTORY */
  UNION ALL SELECT  @new_profile_id1,  14,  54,  0,  0,  0,  0    /* FORM_HANDPAYS */
  UNION ALL SELECT  @new_profile_id1,  14,  55,  0,  0,  0,  0    /* FORM_TERMINALS_PENDING */
  UNION ALL SELECT  @new_profile_id1,  14,  56,  0,  0,  0,  0    /* FORM_MOBILE_BANK_MOVEMENTS */
  UNION ALL SELECT  @new_profile_id1,  14,  57,  0,  0,  0,  0    /* FORM_PROMOTION_REPORT */
  UNION ALL SELECT  @new_profile_id1,  14,  58,  0,  0,  0,  0    /* FORM_GIFTS_SEL */
  UNION ALL SELECT  @new_profile_id1,  14,  59,  0,  0,  0,  0    /* FORM_GIFTS_EDIT */
  UNION ALL SELECT  @new_profile_id1,  14,  60,  0,  0,  0,  0    /* FORM_TAXES_REPORT */
  UNION ALL SELECT  @new_profile_id1,  14,  61,  0,  0,  0,  0    /* FORM_TERMINALS_EDIT */
  UNION ALL SELECT  @new_profile_id1,  14,  62,  0,  0,  0,  0    /* FORM_SITE_JACKPOT_CONFIGURATION */
  UNION ALL SELECT  @new_profile_id1,  14,  63,  0,  0,  0,  0    /* FORM_SITE_GAP_REPORT */
  UNION ALL SELECT  @new_profile_id1,  14,  64,  0,  0,  0,  0    /* FORM_SITE_JACKPOT_HISTORY */
  UNION ALL SELECT  @new_profile_id1,  14,  65,  0,  0,  0,  0    /* FORM_GIFTS_HISTORY */
  UNION ALL SELECT  @new_profile_id1,  14,  66,  0,  0,  0,  0    /* FORM_TERMINALS_WITHOUT_ACTIVITY */
  UNION ALL SELECT  @new_profile_id1,  14,  67,  0,  0,  0,  0    /* FORM_MAILING_PROGRAMMING_SEL */
  UNION ALL SELECT  @new_profile_id1,  14,  68,  0,  0,  0,  0    /* FORM_MAILING_PROGRAMMING_EDIT */
  UNION ALL SELECT  @new_profile_id1,  14,  69,  0,  0,  0,  0    /* FORM_MAILING_PARAMETERS */
  UNION ALL SELECT  @new_profile_id1,  14,  147, 0,  0,  0,  0    /* FORM_GAME_METERS */
  		/* CASHIER */
  UNION ALL SELECT  @new_profile_id1,  15,  0,   1,  1,  1,  1    /* FORM_MAIN */
  UNION ALL SELECT  @new_profile_id1,  15,  1,   0,  0,  0,  0    /* FORM_VIRTUAL_ADD_NO_REDEEM */
  UNION ALL SELECT  @new_profile_id1,  15,  2,   1,  1,  1,  1    /* FORM_VIRTUAL_CASH_DESK_OPEN_CLOSE */
  UNION ALL SELECT  @new_profile_id1,  15,  3,   0,  0,  0,  0    /* FORM_VIRTUAL_CASH_DESK_DEPOSIT */
  UNION ALL SELECT  @new_profile_id1,  15,  4,   0,  0,  0,  0    /* FORM_VIRTUAL_CASH_DESK_WITHDRAWN */
  UNION ALL SELECT  @new_profile_id1,  15,  5,   0,  0,  0,  0    /* FORM_VIRTUAL_CARD_OPER_ADD_REDEEM_CREDIT */
  UNION ALL SELECT  @new_profile_id1,  15,  6,   0,  0,  0,  0    /* FORM_VIRTUAL_CARD_OPER_LAST_MOV */
  UNION ALL SELECT  @new_profile_id1,  15,  7,   0,  0,  0,  0    /* FORM_VIRTUAL_ACCOUNT_OPER_CARD */
  UNION ALL SELECT  @new_profile_id1,  15,  8,   0,  0,  0,  0    /* FORM_VIRTUAL_ACCOUNT_OPER_EDIT */
  UNION ALL SELECT  @new_profile_id1,  15,  9,   0,  0,  0,  0    /* FORM_VIRTUAL_ACCOUNT_OPER_LOCK */
  UNION ALL SELECT  @new_profile_id1,  15,  10,  0,  0,  0,  0    /* FORM_VIRTUAL_PRINT_CARD */
  UNION ALL SELECT  @new_profile_id1,  15,  11,  0,  0,  0,  0    /* FORM_VIRTUAL_OPTIONS_DB_CONFIG */
  UNION ALL SELECT  @new_profile_id1,  15,  12,  0,  0,  0,  0    /* FORM_VIRTUAL_OPTIONS_LANGUAGE */
  UNION ALL SELECT  @new_profile_id1,  15,  13,  0,  0,  0,  0    /* FORM_VIRTUAL_OPTIONS_CALIBRATE */
  UNION ALL SELECT  @new_profile_id1,  15,  14,  0,  0,  0,  0    /* FORM_VIRTUAL_GAME_SESSIONS */
  UNION ALL SELECT  @new_profile_id1,  15,  15,  1,  1,  1,  1    /* FORM_VIRTUAL_MOBILE_BANK */
  UNION ALL SELECT  @new_profile_id1,  15,  16,  1,  1,  1,  1    /* FORM_VIRTUAL_CASH_DESK_ACCESS */
  UNION ALL SELECT  @new_profile_id1,  15,  17,  0,  0,  0,  0    /* FORM_VIRTUAL_ACCOUNT_OPER_HANDPAY */
  UNION ALL SELECT  @new_profile_id1,  15,  18,  0,  0,  0,  0    /* FORM_VIRTUAL_ACCOUNT_OPER_HANDPAY_CANCEL */
  UNION ALL SELECT  @new_profile_id1,  15,  19,  0,  0,  0,  0    /* FORM_VIRTUAL_ACCOUNT_LOG_OFF */
  UNION ALL SELECT  @new_profile_id1,  15,  20,  0,  0,  0,  0    /* FORM_VIRTUAL_MANUAL_OPER_HANDPAY */
  UNION ALL SELECT  @new_profile_id1,  15,  21,  0,  0,  0,  0    /* FORM_VIRTUAL_MANUAL_OPER_HANDPAY_CANCEL */
  UNION ALL SELECT  @new_profile_id1,  15,  22,  0,  0,  0,  0    /* FORM_VIRTUAL_GIFT_REQUEST */
  UNION ALL SELECT  @new_profile_id1,  15,  23,  0,  0,  0,  0    /* FORM_VIRTUAL_GIFT_DELIVERY */
  UNION ALL SELECT  @new_profile_id1,  15,  24,  0,  0,  0,  0    /* FORM_VIRTUAL_ACCOUNT_LEVEL */
  UNION ALL SELECT  @new_profile_id1,  15,  25,  0,  0,  0,  0    /* FORM_VIRTUAL_PROMO_SPECIAL_PERMISSION_A */
  UNION ALL SELECT  @new_profile_id1,  15,  26,  0,  0,  0,  0    /* FORM_VIRTUAL_PROMO_SPECIAL_PERMISSION_B */

END
ELSE
  SELECT '***** Profile BANCO M�VIL already exists *****';


/*****  CAJERO  *****/

IF NOT EXISTS (SELECT * FROM gui_user_profiles WHERE gup_name = 'CAJERO')
BEGIN

  DECLARE @new_profile_id2 AS int
  
  SELECT @new_profile_id2 = max (gup_profile_id) + 1 FROM gui_user_profiles
  
  INSERT INTO [dbo].[gui_user_profiles]
             ([gup_profile_id]
             ,[gup_name])
       VALUES
             ( @new_profile_id2
             , 'CAJERO');
 
        INSERT INTO [dbo].[gui_profile_forms]
                   ([gpf_profile_id], [gpf_gui_id], [gpf_form_id], [gpf_read_perm], [gpf_write_perm], [gpf_delete_perm], [gpf_execute_perm])
                                        /*  FORM  R   W   D   X    */
             SELECT  @new_profile_id2,  14,  1,   0,  0,  0,  0    /* FORM_USER_PROFILE_SEL */
   UNION ALL SELECT  @new_profile_id2,  14,  2,   0,  0,  0,  0    /* FORM_USER_EDIT */
   UNION ALL SELECT  @new_profile_id2,  14,  3,   0,  0,  0,  0    /* FORM_PROFILE_EDIT */
   UNION ALL SELECT  @new_profile_id2,  14,  4,   0,  0,  0,  0    /* FORM_GUI_AUDITOR */
   UNION ALL SELECT  @new_profile_id2,  14,  5,   0,  0,  0,  0    /* FORM_EVENTS_HISTORY */
   UNION ALL SELECT  @new_profile_id2,  14,  6,   0,  0,  0,  0    /* FORM_TERMINALS_SELECTION */
   UNION ALL SELECT  @new_profile_id2,  14,  7,   0,  0,  0,  0    /* FORM_STATISTICS_TERMINALS */
   UNION ALL SELECT  @new_profile_id2,  14,  8,   0,  0,  0,  0    /* FORM_STATISTICS_GAMES */
   UNION ALL SELECT  @new_profile_id2,  14,  9,   0,  0,  0,  0    /* FORM_STATISTICS_DATES */
   UNION ALL SELECT  @new_profile_id2,  14,  10,  0,  0,  0,  0    /* FORM_STATISTICS_HOURS */
   UNION ALL SELECT  @new_profile_id2,  14,  11,  0,  0,  0,  0    /* FORM_STATISTICS_CARDS */
   UNION ALL SELECT  @new_profile_id2,  14,  12,  0,  0,  0,  0    /* FORM_PLAYS_SESSIONS */
   UNION ALL SELECT  @new_profile_id2,  14,  13,  0,  0,  0,  0    /* FORM_PLAYS_PLAYS */
   UNION ALL SELECT  @new_profile_id2,  14,  14,  0,  0,  0,  0    /* FORM_WS_SESSIONS */
   UNION ALL SELECT  @new_profile_id2,  14,  15,  0,  0,  0,  0    /* FORM_STATISTICS_HOURS_GROUP */
   UNION ALL SELECT  @new_profile_id2,  14,  16,  0,  0,  0,  0    /* FORM_ACCT_TAXES */
   UNION ALL SELECT  @new_profile_id2,  14,  17,  0,  0,  0,  0    /* FORM_CASHIER_MOVS */
   UNION ALL SELECT  @new_profile_id2,  14,  18,  0,  0,  0,  0    /* FORM_CASHIER_SESSIONS */
   UNION ALL SELECT  @new_profile_id2,  14,  19,  0,  0,  0,  0    /* FORM_ACCOUNT_MOVEMENTS */
   UNION ALL SELECT  @new_profile_id2,  14,  20,  0,  0,  0,  0    /* FORM_ACCOUNT_SUMMARY */
   UNION ALL SELECT  @new_profile_id2,  14,  21,  0,  0,  0,  0    /* FORM_DATE_TERMINAL */
   UNION ALL SELECT  @new_profile_id2,  14,  22,  0,  0,  0,  0    /* FORM_CLASS_II_DRAW_AUDIT */
   UNION ALL SELECT  @new_profile_id2,  14,  24,  0,  0,  0,  0    /* FORM_CLASS_II_JACKPOT_CONFIGURATION */
   UNION ALL SELECT  @new_profile_id2,  14,  25,  0,  0,  0,  0    /* FORM_CLASS_II_JACKPOT_HISTORY */
   UNION ALL SELECT  @new_profile_id2,  14,  26,  0,  0,  0,  0    /* FORM_CLASS_II_JACKPOT_MONITOR */
   UNION ALL SELECT  @new_profile_id2,  14,  27,  0,  0,  0,  0    /* FORM_SW_DOWNLOAD_VERSIONS */
   UNION ALL SELECT  @new_profile_id2,  14,  28,  0,  0,  0,  0    /* FORM_SW_DOWNLOAD_DETAILS */
   UNION ALL SELECT  @new_profile_id2,  14,  29,  0,  0,  0,  0    /* FORM_CARD_RECORD */
   UNION ALL SELECT  @new_profile_id2,  14,  30,  0,  0,  0,  0    /* FORM_GENERAL_PARAMS */
   UNION ALL SELECT  @new_profile_id2,  14,  31,  0,  0,  0,  0    /* FORM_PROVIDER_DAY_TERMINAL */
   UNION ALL SELECT  @new_profile_id2,  14,  32,  0,  0,  0,  0    /* FORM_SERVICE_MONITOR */
   UNION ALL SELECT  @new_profile_id2,  14,  34,  0,  0,  0,  0    /* FORM_TERMINALS_BY_BUILD */
   UNION ALL SELECT  @new_profile_id2,  14,  35,  0,  0,  0,  0    /* FORM_CASHIER_SESSION_DETAIL */
   UNION ALL SELECT  @new_profile_id2,  14,  36,  0,  0,  0,  0    /* FORM_CLASS_II_JACKPOT_PROMO_MESSAGE */
   UNION ALL SELECT  @new_profile_id2,  14,  37,  0,  0,  0,  0    /* FORM_CASHIER_CONFIGURATION */
   UNION ALL SELECT  @new_profile_id2,  14,  38,  0,  0,  0,  0    /* FORM_LICENCE_VERSIONS */
   UNION ALL SELECT  @new_profile_id2,  14,  39,  0,  0,  0,  0    /* FORM_MISC_SW_BY_BUILD */
   UNION ALL SELECT  @new_profile_id2,  14,  40,  0,  0,  0,  0    /* FORM_LICENCE_DETAILS */
   UNION ALL SELECT  @new_profile_id2,  14,  45,  0,  0,  0,  0    /* FORM_TERMINAL_GAME_RELATION */
   UNION ALL SELECT  @new_profile_id2,  14,  46,  0,  0,  0,  0    /* FORM_EXPIRED_CREDITS */
   UNION ALL SELECT  @new_profile_id2,  14,  47,  0,  0,  0,  0    /* FORM_PROMOTIONS */
   UNION ALL SELECT  @new_profile_id2,  14,  48,  0,  0,  0,  0    /* FORM_PROMOTION_EDIT */
   UNION ALL SELECT  @new_profile_id2,  14,  49,  0,  0,  0,  0    /* FORM_TERMINALS_3GS_EDIT */
   UNION ALL SELECT  @new_profile_id2,  14,  50,  0,  0,  0,  0    /* FORM_DRAWS */
   UNION ALL SELECT  @new_profile_id2,  14,  51,  0,  0,  0,  0    /* FORM_DRAW_EDIT */
   UNION ALL SELECT  @new_profile_id2,  14,  52,  0,  0,  0,  0    /* FORM_COMMANDS_SEND */
   UNION ALL SELECT  @new_profile_id2,  14,  53,  0,  0,  0,  0    /* FORM_COMMANDS_HISTORY */
   UNION ALL SELECT  @new_profile_id2,  14,  54,  0,  0,  0,  0    /* FORM_HANDPAYS */
   UNION ALL SELECT  @new_profile_id2,  14,  55,  0,  0,  0,  0    /* FORM_TERMINALS_PENDING */
   UNION ALL SELECT  @new_profile_id2,  14,  56,  0,  0,  0,  0    /* FORM_MOBILE_BANK_MOVEMENTS */
   UNION ALL SELECT  @new_profile_id2,  14,  57,  0,  0,  0,  0    /* FORM_PROMOTION_REPORT */
   UNION ALL SELECT  @new_profile_id2,  14,  58,  0,  0,  0,  0    /* FORM_GIFTS_SEL */
   UNION ALL SELECT  @new_profile_id2,  14,  59,  0,  0,  0,  0    /* FORM_GIFTS_EDIT */
   UNION ALL SELECT  @new_profile_id2,  14,  60,  0,  0,  0,  0    /* FORM_TAXES_REPORT */
   UNION ALL SELECT  @new_profile_id2,  14,  61,  0,  0,  0,  0    /* FORM_TERMINALS_EDIT */
   UNION ALL SELECT  @new_profile_id2,  14,  62,  0,  0,  0,  0    /* FORM_SITE_JACKPOT_CONFIGURATION */
   UNION ALL SELECT  @new_profile_id2,  14,  63,  0,  0,  0,  0    /* FORM_SITE_GAP_REPORT */
   UNION ALL SELECT  @new_profile_id2,  14,  64,  0,  0,  0,  0    /* FORM_SITE_JACKPOT_HISTORY */
   UNION ALL SELECT  @new_profile_id2,  14,  65,  0,  0,  0,  0    /* FORM_GIFTS_HISTORY */
   UNION ALL SELECT  @new_profile_id2,  14,  66,  0,  0,  0,  0    /* FORM_TERMINALS_WITHOUT_ACTIVITY */
   UNION ALL SELECT  @new_profile_id2,  14,  67,  0,  0,  0,  0    /* FORM_MAILING_PROGRAMMING_SEL */
   UNION ALL SELECT  @new_profile_id2,  14,  68,  0,  0,  0,  0    /* FORM_MAILING_PROGRAMMING_EDIT */
   UNION ALL SELECT  @new_profile_id2,  14,  69,  0,  0,  0,  0    /* FORM_MAILING_PARAMETERS */
   UNION ALL SELECT  @new_profile_id2,  14,  147, 0,  0,  0,  0    /* FORM_GAME_METERS */
   		/* CASHIER */
   UNION ALL SELECT  @new_profile_id2,  15,  0,   1,  1,  1,  1    /* FORM_MAIN */
   UNION ALL SELECT  @new_profile_id2,  15,  1,   0,  0,  0,  0    /* FORM_VIRTUAL_ADD_NO_REDEEM */
   UNION ALL SELECT  @new_profile_id2,  15,  2,   0,  0,  0,  0    /* FORM_VIRTUAL_CASH_DESK_OPEN_CLOSE */
   UNION ALL SELECT  @new_profile_id2,  15,  3,   0,  0,  0,  0    /* FORM_VIRTUAL_CASH_DESK_DEPOSIT */
   UNION ALL SELECT  @new_profile_id2,  15,  4,   0,  0,  0,  0    /* FORM_VIRTUAL_CASH_DESK_WITHDRAWN */
   UNION ALL SELECT  @new_profile_id2,  15,  5,   1,  1,  1,  1    /* FORM_VIRTUAL_CARD_OPER_ADD_REDEEM_CREDIT */
   UNION ALL SELECT  @new_profile_id2,  15,  6,   0,  0,  0,  0    /* FORM_VIRTUAL_CARD_OPER_LAST_MOV */
   UNION ALL SELECT  @new_profile_id2,  15,  7,   0,  0,  0,  0    /* FORM_VIRTUAL_ACCOUNT_OPER_CARD */
   UNION ALL SELECT  @new_profile_id2,  15,  8,   1,  1,  1,  1    /* FORM_VIRTUAL_ACCOUNT_OPER_EDIT */
   UNION ALL SELECT  @new_profile_id2,  15,  9,   1,  1,  1,  1    /* FORM_VIRTUAL_ACCOUNT_OPER_LOCK */
   UNION ALL SELECT  @new_profile_id2,  15,  10,  1,  1,  1,  1    /* FORM_VIRTUAL_PRINT_CARD */
   UNION ALL SELECT  @new_profile_id2,  15,  11,  0,  0,  0,  0    /* FORM_VIRTUAL_OPTIONS_DB_CONFIG */
   UNION ALL SELECT  @new_profile_id2,  15,  12,  0,  0,  0,  0    /* FORM_VIRTUAL_OPTIONS_LANGUAGE */
   UNION ALL SELECT  @new_profile_id2,  15,  13,  0,  0,  0,  0    /* FORM_VIRTUAL_OPTIONS_CALIBRATE */
   UNION ALL SELECT  @new_profile_id2,  15,  14,  0,  0,  0,  0    /* FORM_VIRTUAL_GAME_SESSIONS */
   UNION ALL SELECT  @new_profile_id2,  15,  15,  0,  0,  0,  0    /* FORM_VIRTUAL_MOBILE_BANK */
   UNION ALL SELECT  @new_profile_id2,  15,  16,  0,  0,  0,  0    /* FORM_VIRTUAL_CASH_DESK_ACCESS */
   UNION ALL SELECT  @new_profile_id2,  15,  17,  0,  0,  0,  0    /* FORM_VIRTUAL_ACCOUNT_OPER_HANDPAY */
   UNION ALL SELECT  @new_profile_id2,  15,  18,  0,  0,  0,  0    /* FORM_VIRTUAL_ACCOUNT_OPER_HANDPAY_CANCEL */
   UNION ALL SELECT  @new_profile_id2,  15,  19,  0,  0,  0,  0    /* FORM_VIRTUAL_ACCOUNT_LOG_OFF */
   UNION ALL SELECT  @new_profile_id2,  15,  20,  0,  0,  0,  0    /* FORM_VIRTUAL_MANUAL_OPER_HANDPAY */
   UNION ALL SELECT  @new_profile_id2,  15,  21,  0,  0,  0,  0    /* FORM_VIRTUAL_MANUAL_OPER_HANDPAY_CANCEL */
   UNION ALL SELECT  @new_profile_id2,  15,  22,  0,  0,  0,  0    /* FORM_VIRTUAL_GIFT_REQUEST */
   UNION ALL SELECT  @new_profile_id2,  15,  23,  0,  0,  0,  0    /* FORM_VIRTUAL_GIFT_DELIVERY */
   UNION ALL SELECT  @new_profile_id2,  15,  24,  0,  0,  0,  0    /* FORM_VIRTUAL_ACCOUNT_LEVEL */
   UNION ALL SELECT  @new_profile_id2,  15,  25,  0,  0,  0,  0    /* FORM_VIRTUAL_PROMO_SPECIAL_PERMISSION_A */
   UNION ALL SELECT  @new_profile_id2,  15,  26,  0,  0,  0,  0    /* FORM_VIRTUAL_PROMO_SPECIAL_PERMISSION_B */
    
END
ELSE
  SELECT '***** Profile CAJERO already exists *****';


/*****  GERENTE  *****/

IF NOT EXISTS (SELECT * FROM gui_user_profiles WHERE gup_name = 'GERENTE')
BEGIN

  DECLARE @new_profile_id3 AS int
  
  SELECT @new_profile_id3 = max (gup_profile_id) + 1 FROM gui_user_profiles
  
  INSERT INTO [dbo].[gui_user_profiles]
             ([gup_profile_id]
             ,[gup_name])
       VALUES
             ( @new_profile_id3
             , 'GERENTE');

       INSERT INTO [dbo].[gui_profile_forms]
                  ([gpf_profile_id], [gpf_gui_id], [gpf_form_id], [gpf_read_perm], [gpf_write_perm], [gpf_delete_perm], [gpf_execute_perm])
                                       /*  FORM  R   W   D   X    */
            SELECT  @new_profile_id3,  14,  1,   1,  0,  1,  1    /* FORM_USER_PROFILE_SEL */
  UNION ALL SELECT  @new_profile_id3,  14,  2,   1,  0,  1,  1    /* FORM_USER_EDIT */
  UNION ALL SELECT  @new_profile_id3,  14,  3,   1,  0,  1,  1    /* FORM_PROFILE_EDIT */
  UNION ALL SELECT  @new_profile_id3,  14,  4,   1,  0,  1,  1    /* FORM_GUI_AUDITOR */
  UNION ALL SELECT  @new_profile_id3,  14,  5,   0,  0,  0,  0    /* FORM_EVENTS_HISTORY */
  UNION ALL SELECT  @new_profile_id3,  14,  6,   0,  0,  0,  0    /* FORM_TERMINALS_SELECTION */
  UNION ALL SELECT  @new_profile_id3,  14,  7,   1,  0,  1,  1    /* FORM_STATISTICS_TERMINALS */
  UNION ALL SELECT  @new_profile_id3,  14,  8,   1,  0,  1,  1    /* FORM_STATISTICS_GAMES */
  UNION ALL SELECT  @new_profile_id3,  14,  9,   1,  0,  1,  1    /* FORM_STATISTICS_DATES */
  UNION ALL SELECT  @new_profile_id3,  14,  10,  1,  0,  1,  1    /* FORM_STATISTICS_HOURS */
  UNION ALL SELECT  @new_profile_id3,  14,  11,  1,  0,  1,  1    /* FORM_STATISTICS_CARDS */
  UNION ALL SELECT  @new_profile_id3,  14,  12,  1,  0,  1,  1    /* FORM_PLAYS_SESSIONS */
  UNION ALL SELECT  @new_profile_id3,  14,  13,  1,  0,  1,  1    /* FORM_PLAYS_PLAYS */
  UNION ALL SELECT  @new_profile_id3,  14,  14,  0,  0,  0,  0    /* FORM_WS_SESSIONS */
  UNION ALL SELECT  @new_profile_id3,  14,  15,  1,  0,  1,  1    /* FORM_STATISTICS_HOURS_GROUP */
  UNION ALL SELECT  @new_profile_id3,  14,  16,  1,  0,  1,  1    /* FORM_ACCT_TAXES */
  UNION ALL SELECT  @new_profile_id3,  14,  17,  1,  0,  1,  1    /* FORM_CASHIER_MOVS */
  UNION ALL SELECT  @new_profile_id3,  14,  18,  1,  0,  1,  1    /* FORM_CASHIER_SESSIONS */
  UNION ALL SELECT  @new_profile_id3,  14,  19,  1,  0,  1,  1    /* FORM_ACCOUNT_MOVEMENTS */
  UNION ALL SELECT  @new_profile_id3,  14,  20,  1,  0,  1,  1    /* FORM_ACCOUNT_SUMMARY */
  UNION ALL SELECT  @new_profile_id3,  14,  21,  1,  0,  1,  1    /* FORM_DATE_TERMINAL */
  UNION ALL SELECT  @new_profile_id3,  14,  22,  0,  0,  0,  0    /* FORM_CLASS_II_DRAW_AUDIT */
  UNION ALL SELECT  @new_profile_id3,  14,  24,  0,  0,  0,  0    /* FORM_CLASS_II_JACKPOT_CONFIGURATION */
  UNION ALL SELECT  @new_profile_id3,  14,  25,  0,  0,  0,  0    /* FORM_CLASS_II_JACKPOT_HISTORY */
  UNION ALL SELECT  @new_profile_id3,  14,  26,  0,  0,  0,  0    /* FORM_CLASS_II_JACKPOT_MONITOR */
  UNION ALL SELECT  @new_profile_id3,  14,  27,  0,  0,  0,  0    /* FORM_SW_DOWNLOAD_VERSIONS */
  UNION ALL SELECT  @new_profile_id3,  14,  28,  0,  0,  0,  0    /* FORM_SW_DOWNLOAD_DETAILS */
  UNION ALL SELECT  @new_profile_id3,  14,  29,  0,  0,  0,  0    /* FORM_CARD_RECORD */
  UNION ALL SELECT  @new_profile_id3,  14,  30,  0,  0,  0,  0    /* FORM_GENERAL_PARAMS */
  UNION ALL SELECT  @new_profile_id3,  14,  31,  1,  0,  1,  1    /* FORM_PROVIDER_DAY_TERMINAL */
  UNION ALL SELECT  @new_profile_id3,  14,  32,  0,  0,  0,  0    /* FORM_SERVICE_MONITOR */
  UNION ALL SELECT  @new_profile_id3,  14,  34,  0,  0,  0,  0    /* FORM_TERMINALS_BY_BUILD */
  UNION ALL SELECT  @new_profile_id3,  14,  35,  1,  0,  1,  1    /* FORM_CASHIER_SESSION_DETAIL */
  UNION ALL SELECT  @new_profile_id3,  14,  36,  0,  0,  0,  0    /* FORM_CLASS_II_JACKPOT_PROMO_MESSAGE */
  UNION ALL SELECT  @new_profile_id3,  14,  37,  0,  0,  0,  0    /* FORM_CASHIER_CONFIGURATION */
  UNION ALL SELECT  @new_profile_id3,  14,  38,  0,  0,  0,  0    /* FORM_LICENCE_VERSIONS */
  UNION ALL SELECT  @new_profile_id3,  14,  39,  0,  0,  0,  0    /* FORM_MISC_SW_BY_BUILD */
  UNION ALL SELECT  @new_profile_id3,  14,  40,  0,  0,  0,  0    /* FORM_LICENCE_DETAILS */
  UNION ALL SELECT  @new_profile_id3,  14,  45,  0,  0,  0,  0    /* FORM_TERMINAL_GAME_RELATION */
  UNION ALL SELECT  @new_profile_id3,  14,  46,  1,  0,  1,  1    /* FORM_EXPIRED_CREDITS */
  UNION ALL SELECT  @new_profile_id3,  14,  47,  1,  0,  1,  1    /* FORM_PROMOTIONS */
  UNION ALL SELECT  @new_profile_id3,  14,  48,  1,  0,  1,  1    /* FORM_PROMOTION_EDIT */
  UNION ALL SELECT  @new_profile_id3,  14,  49,  0,  0,  0,  0    /* FORM_TERMINALS_3GS_EDIT */
  UNION ALL SELECT  @new_profile_id3,  14,  50,  1,  0,  1,  1    /* FORM_DRAWS */
  UNION ALL SELECT  @new_profile_id3,  14,  51,  1,  0,  1,  1    /* FORM_DRAW_EDIT */
  UNION ALL SELECT  @new_profile_id3,  14,  52,  0,  0,  0,  0    /* FORM_COMMANDS_SEND */
  UNION ALL SELECT  @new_profile_id3,  14,  53,  0,  0,  0,  0    /* FORM_COMMANDS_HISTORY */
  UNION ALL SELECT  @new_profile_id3,  14,  54,  1,  0,  1,  1    /* FORM_HANDPAYS */
  UNION ALL SELECT  @new_profile_id3,  14,  55,  0,  0,  0,  0    /* FORM_TERMINALS_PENDING */
  UNION ALL SELECT  @new_profile_id3,  14,  56,  1,  0,  1,  1    /* FORM_MOBILE_BANK_MOVEMENTS */
  UNION ALL SELECT  @new_profile_id3,  14,  57,  1,  0,  1,  1    /* FORM_PROMOTION_REPORT */
  UNION ALL SELECT  @new_profile_id3,  14,  58,  1,  0,  1,  1    /* FORM_GIFTS_SEL */
  UNION ALL SELECT  @new_profile_id3,  14,  59,  1,  0,  1,  1    /* FORM_GIFTS_EDIT */
  UNION ALL SELECT  @new_profile_id3,  14,  60,  1,  0,  1,  1    /* FORM_TAXES_REPORT */
  UNION ALL SELECT  @new_profile_id3,  14,  61,  0,  0,  0,  0    /* FORM_TERMINALS_EDIT */
  UNION ALL SELECT  @new_profile_id3,  14,  62,  0,  0,  0,  0    /* FORM_SITE_JACKPOT_CONFIGURATION */
  UNION ALL SELECT  @new_profile_id3,  14,  63,  1,  0,  1,  1    /* FORM_SITE_GAP_REPORT */
  UNION ALL SELECT  @new_profile_id3,  14,  64,  1,  0,  1,  1    /* FORM_SITE_JACKPOT_HISTORY */
  UNION ALL SELECT  @new_profile_id3,  14,  65,  1,  0,  1,  1    /* FORM_GIFTS_HISTORY */
  UNION ALL SELECT  @new_profile_id3,  14,  66,  1,  0,  1,  1    /* FORM_TERMINALS_WITHOUT_ACTIVITY */
  UNION ALL SELECT  @new_profile_id3,  14,  67,  0,  0,  0,  0    /* FORM_MAILING_PROGRAMMING_SEL */
  UNION ALL SELECT  @new_profile_id3,  14,  68,  0,  0,  0,  0    /* FORM_MAILING_PROGRAMMING_EDIT */
  UNION ALL SELECT  @new_profile_id3,  14,  69,  0,  0,  0,  0    /* FORM_MAILING_PARAMETERS */
  UNION ALL SELECT  @new_profile_id3,  14,  147, 0,  0,  0,  0    /* FORM_GAME_METERS */
  		/* CASHIER */
  UNION ALL SELECT  @new_profile_id3,  15,  0,   0,  0,  0,  0    /* FORM_MAIN */
  UNION ALL SELECT  @new_profile_id3,  15,  1,   0,  0,  0,  0    /* FORM_VIRTUAL_ADD_NO_REDEEM */
  UNION ALL SELECT  @new_profile_id3,  15,  2,   0,  0,  0,  0    /* FORM_VIRTUAL_CASH_DESK_OPEN_CLOSE */
  UNION ALL SELECT  @new_profile_id3,  15,  3,   0,  0,  0,  0    /* FORM_VIRTUAL_CASH_DESK_DEPOSIT */
  UNION ALL SELECT  @new_profile_id3,  15,  4,   0,  0,  0,  0    /* FORM_VIRTUAL_CASH_DESK_WITHDRAWN */
  UNION ALL SELECT  @new_profile_id3,  15,  5,   0,  0,  0,  0    /* FORM_VIRTUAL_CARD_OPER_ADD_REDEEM_CREDIT */
  UNION ALL SELECT  @new_profile_id3,  15,  6,   0,  0,  0,  0    /* FORM_VIRTUAL_CARD_OPER_LAST_MOV */
  UNION ALL SELECT  @new_profile_id3,  15,  7,   0,  0,  0,  0    /* FORM_VIRTUAL_ACCOUNT_OPER_CARD */
  UNION ALL SELECT  @new_profile_id3,  15,  8,   0,  0,  0,  0    /* FORM_VIRTUAL_ACCOUNT_OPER_EDIT */
  UNION ALL SELECT  @new_profile_id3,  15,  9,   0,  0,  0,  0    /* FORM_VIRTUAL_ACCOUNT_OPER_LOCK */
  UNION ALL SELECT  @new_profile_id3,  15,  10,  0,  0,  0,  0    /* FORM_VIRTUAL_PRINT_CARD */
  UNION ALL SELECT  @new_profile_id3,  15,  11,  0,  0,  0,  0    /* FORM_VIRTUAL_OPTIONS_DB_CONFIG */
  UNION ALL SELECT  @new_profile_id3,  15,  12,  0,  0,  0,  0    /* FORM_VIRTUAL_OPTIONS_LANGUAGE */
  UNION ALL SELECT  @new_profile_id3,  15,  13,  0,  0,  0,  0    /* FORM_VIRTUAL_OPTIONS_CALIBRATE */
  UNION ALL SELECT  @new_profile_id3,  15,  14,  0,  0,  0,  0    /* FORM_VIRTUAL_GAME_SESSIONS */
  UNION ALL SELECT  @new_profile_id3,  15,  15,  0,  0,  0,  0    /* FORM_VIRTUAL_MOBILE_BANK */
  UNION ALL SELECT  @new_profile_id3,  15,  16,  0,  0,  0,  0    /* FORM_VIRTUAL_CASH_DESK_ACCESS */
  UNION ALL SELECT  @new_profile_id3,  15,  17,  0,  0,  0,  0    /* FORM_VIRTUAL_ACCOUNT_OPER_HANDPAY */
  UNION ALL SELECT  @new_profile_id3,  15,  18,  0,  0,  0,  0    /* FORM_VIRTUAL_ACCOUNT_OPER_HANDPAY_CANCEL */
  UNION ALL SELECT  @new_profile_id3,  15,  19,  0,  0,  0,  0    /* FORM_VIRTUAL_ACCOUNT_LOG_OFF */
  UNION ALL SELECT  @new_profile_id3,  15,  20,  0,  0,  0,  0    /* FORM_VIRTUAL_MANUAL_OPER_HANDPAY */
  UNION ALL SELECT  @new_profile_id3,  15,  21,  0,  0,  0,  0    /* FORM_VIRTUAL_MANUAL_OPER_HANDPAY_CANCEL */
  UNION ALL SELECT  @new_profile_id3,  15,  22,  0,  0,  0,  0    /* FORM_VIRTUAL_GIFT_REQUEST */
  UNION ALL SELECT  @new_profile_id3,  15,  23,  0,  0,  0,  0    /* FORM_VIRTUAL_GIFT_DELIVERY */
  UNION ALL SELECT  @new_profile_id3,  15,  24,  0,  0,  0,  0    /* FORM_VIRTUAL_ACCOUNT_LEVEL */
  UNION ALL SELECT  @new_profile_id3,  15,  25,  0,  0,  0,  0    /* FORM_VIRTUAL_PROMO_SPECIAL_PERMISSION_A */
  UNION ALL SELECT  @new_profile_id3,  15,  26,  0,  0,  0,  0    /* FORM_VIRTUAL_PROMO_SPECIAL_PERMISSION_B */
 
END
ELSE
  SELECT '***** Profile GERENTE already exists *****';



/*****  JEFE DE CAJA  *****/

IF NOT EXISTS (SELECT * FROM gui_user_profiles WHERE gup_name = 'JEFE DE CAJA')
BEGIN

  DECLARE @new_profile_id4 AS int
  
  SELECT @new_profile_id4 = max (gup_profile_id) + 1 FROM gui_user_profiles
  
  INSERT INTO [dbo].[gui_user_profiles]
             ([gup_profile_id]
             ,[gup_name])
       VALUES
             ( @new_profile_id4
             , 'JEFE DE CAJA');

         INSERT INTO [dbo].[gui_profile_forms]
                    ([gpf_profile_id], [gpf_gui_id], [gpf_form_id], [gpf_read_perm], [gpf_write_perm], [gpf_delete_perm], [gpf_execute_perm])
                                         /*  FORM  R   W   D   X    */
              SELECT  @new_profile_id4,  14,  1,   0,  0,  0,  0    /* FORM_USER_PROFILE_SEL */
    UNION ALL SELECT  @new_profile_id4,  14,  2,   0,  0,  0,  0    /* FORM_USER_EDIT */
    UNION ALL SELECT  @new_profile_id4,  14,  3,   0,  0,  0,  0    /* FORM_PROFILE_EDIT */
    UNION ALL SELECT  @new_profile_id4,  14,  4,   0,  0,  0,  0    /* FORM_GUI_AUDITOR */
    UNION ALL SELECT  @new_profile_id4,  14,  5,   0,  0,  0,  0    /* FORM_EVENTS_HISTORY */
    UNION ALL SELECT  @new_profile_id4,  14,  6,   0,  0,  0,  0    /* FORM_TERMINALS_SELECTION */
    UNION ALL SELECT  @new_profile_id4,  14,  7,   0,  0,  0,  0    /* FORM_STATISTICS_TERMINALS */
    UNION ALL SELECT  @new_profile_id4,  14,  8,   0,  0,  0,  0    /* FORM_STATISTICS_GAMES */
    UNION ALL SELECT  @new_profile_id4,  14,  9,   0,  0,  0,  0    /* FORM_STATISTICS_DATES */
    UNION ALL SELECT  @new_profile_id4,  14,  10,  0,  0,  0,  0    /* FORM_STATISTICS_HOURS */
    UNION ALL SELECT  @new_profile_id4,  14,  11,  0,  0,  0,  0    /* FORM_STATISTICS_CARDS */
    UNION ALL SELECT  @new_profile_id4,  14,  12,  0,  0,  0,  0    /* FORM_PLAYS_SESSIONS */
    UNION ALL SELECT  @new_profile_id4,  14,  13,  0,  0,  0,  0    /* FORM_PLAYS_PLAYS */
    UNION ALL SELECT  @new_profile_id4,  14,  14,  0,  0,  0,  0    /* FORM_WS_SESSIONS */
    UNION ALL SELECT  @new_profile_id4,  14,  15,  0,  0,  0,  0    /* FORM_STATISTICS_HOURS_GROUP */
    UNION ALL SELECT  @new_profile_id4,  14,  16,  0,  0,  0,  0    /* FORM_ACCT_TAXES */
    UNION ALL SELECT  @new_profile_id4,  14,  17,  0,  0,  0,  0    /* FORM_CASHIER_MOVS */
    UNION ALL SELECT  @new_profile_id4,  14,  18,  0,  0,  0,  0    /* FORM_CASHIER_SESSIONS */
    UNION ALL SELECT  @new_profile_id4,  14,  19,  0,  0,  0,  0    /* FORM_ACCOUNT_MOVEMENTS */
    UNION ALL SELECT  @new_profile_id4,  14,  20,  0,  0,  0,  0    /* FORM_ACCOUNT_SUMMARY */
    UNION ALL SELECT  @new_profile_id4,  14,  21,  0,  0,  0,  0    /* FORM_DATE_TERMINAL */
    UNION ALL SELECT  @new_profile_id4,  14,  22,  0,  0,  0,  0    /* FORM_CLASS_II_DRAW_AUDIT */
    UNION ALL SELECT  @new_profile_id4,  14,  24,  0,  0,  0,  0    /* FORM_CLASS_II_JACKPOT_CONFIGURATION */
    UNION ALL SELECT  @new_profile_id4,  14,  25,  0,  0,  0,  0    /* FORM_CLASS_II_JACKPOT_HISTORY */
    UNION ALL SELECT  @new_profile_id4,  14,  26,  0,  0,  0,  0    /* FORM_CLASS_II_JACKPOT_MONITOR */
    UNION ALL SELECT  @new_profile_id4,  14,  27,  0,  0,  0,  0    /* FORM_SW_DOWNLOAD_VERSIONS */
    UNION ALL SELECT  @new_profile_id4,  14,  28,  0,  0,  0,  0    /* FORM_SW_DOWNLOAD_DETAILS */
    UNION ALL SELECT  @new_profile_id4,  14,  29,  0,  0,  0,  0    /* FORM_CARD_RECORD */
    UNION ALL SELECT  @new_profile_id4,  14,  30,  0,  0,  0,  0    /* FORM_GENERAL_PARAMS */
    UNION ALL SELECT  @new_profile_id4,  14,  31,  0,  0,  0,  0    /* FORM_PROVIDER_DAY_TERMINAL */
    UNION ALL SELECT  @new_profile_id4,  14,  32,  0,  0,  0,  0    /* FORM_SERVICE_MONITOR */
    UNION ALL SELECT  @new_profile_id4,  14,  34,  0,  0,  0,  0    /* FORM_TERMINALS_BY_BUILD */
    UNION ALL SELECT  @new_profile_id4,  14,  35,  0,  0,  0,  0    /* FORM_CASHIER_SESSION_DETAIL */
    UNION ALL SELECT  @new_profile_id4,  14,  36,  0,  0,  0,  0    /* FORM_CLASS_II_JACKPOT_PROMO_MESSAGE */
    UNION ALL SELECT  @new_profile_id4,  14,  37,  0,  0,  0,  0    /* FORM_CASHIER_CONFIGURATION */
    UNION ALL SELECT  @new_profile_id4,  14,  38,  0,  0,  0,  0    /* FORM_LICENCE_VERSIONS */
    UNION ALL SELECT  @new_profile_id4,  14,  39,  0,  0,  0,  0    /* FORM_MISC_SW_BY_BUILD */
    UNION ALL SELECT  @new_profile_id4,  14,  40,  0,  0,  0,  0    /* FORM_LICENCE_DETAILS */
    UNION ALL SELECT  @new_profile_id4,  14,  45,  0,  0,  0,  0    /* FORM_TERMINAL_GAME_RELATION */
    UNION ALL SELECT  @new_profile_id4,  14,  46,  0,  0,  0,  0    /* FORM_EXPIRED_CREDITS */
    UNION ALL SELECT  @new_profile_id4,  14,  47,  0,  0,  0,  0    /* FORM_PROMOTIONS */
    UNION ALL SELECT  @new_profile_id4,  14,  48,  0,  0,  0,  0    /* FORM_PROMOTION_EDIT */
    UNION ALL SELECT  @new_profile_id4,  14,  49,  0,  0,  0,  0    /* FORM_TERMINALS_3GS_EDIT */
    UNION ALL SELECT  @new_profile_id4,  14,  50,  0,  0,  0,  0    /* FORM_DRAWS */
    UNION ALL SELECT  @new_profile_id4,  14,  51,  0,  0,  0,  0    /* FORM_DRAW_EDIT */
    UNION ALL SELECT  @new_profile_id4,  14,  52,  0,  0,  0,  0    /* FORM_COMMANDS_SEND */
    UNION ALL SELECT  @new_profile_id4,  14,  53,  0,  0,  0,  0    /* FORM_COMMANDS_HISTORY */
    UNION ALL SELECT  @new_profile_id4,  14,  54,  0,  0,  0,  0    /* FORM_HANDPAYS */
    UNION ALL SELECT  @new_profile_id4,  14,  55,  0,  0,  0,  0    /* FORM_TERMINALS_PENDING */
    UNION ALL SELECT  @new_profile_id4,  14,  56,  0,  0,  0,  0    /* FORM_MOBILE_BANK_MOVEMENTS */
    UNION ALL SELECT  @new_profile_id4,  14,  57,  0,  0,  0,  0    /* FORM_PROMOTION_REPORT */
    UNION ALL SELECT  @new_profile_id4,  14,  58,  0,  0,  0,  0    /* FORM_GIFTS_SEL */
    UNION ALL SELECT  @new_profile_id4,  14,  59,  0,  0,  0,  0    /* FORM_GIFTS_EDIT */
    UNION ALL SELECT  @new_profile_id4,  14,  60,  0,  0,  0,  0    /* FORM_TAXES_REPORT */
    UNION ALL SELECT  @new_profile_id4,  14,  61,  0,  0,  0,  0    /* FORM_TERMINALS_EDIT */
    UNION ALL SELECT  @new_profile_id4,  14,  62,  0,  0,  0,  0    /* FORM_SITE_JACKPOT_CONFIGURATION */
    UNION ALL SELECT  @new_profile_id4,  14,  63,  0,  0,  0,  0    /* FORM_SITE_GAP_REPORT */
    UNION ALL SELECT  @new_profile_id4,  14,  64,  0,  0,  0,  0    /* FORM_SITE_JACKPOT_HISTORY */
    UNION ALL SELECT  @new_profile_id4,  14,  65,  0,  0,  0,  0    /* FORM_GIFTS_HISTORY */
    UNION ALL SELECT  @new_profile_id4,  14,  66,  0,  0,  0,  0    /* FORM_TERMINALS_WITHOUT_ACTIVITY */
    UNION ALL SELECT  @new_profile_id4,  14,  67,  0,  0,  0,  0    /* FORM_MAILING_PROGRAMMING_SEL */
    UNION ALL SELECT  @new_profile_id4,  14,  68,  0,  0,  0,  0    /* FORM_MAILING_PROGRAMMING_EDIT */
    UNION ALL SELECT  @new_profile_id4,  14,  69,  0,  0,  0,  0    /* FORM_MAILING_PARAMETERS */
    UNION ALL SELECT  @new_profile_id4,  14,  147, 0,  0,  0,  0    /* FORM_GAME_METERS */
    		/* CASHIER */
    UNION ALL SELECT  @new_profile_id4,  15,  0,   1,  1,  1,  1    /* FORM_MAIN */
    UNION ALL SELECT  @new_profile_id4,  15,  1,   1,  1,  1,  1    /* FORM_VIRTUAL_ADD_NO_REDEEM */
    UNION ALL SELECT  @new_profile_id4,  15,  2,   1,  1,  1,  1    /* FORM_VIRTUAL_CASH_DESK_OPEN_CLOSE */
    UNION ALL SELECT  @new_profile_id4,  15,  3,   1,  1,  1,  1    /* FORM_VIRTUAL_CASH_DESK_DEPOSIT */
    UNION ALL SELECT  @new_profile_id4,  15,  4,   1,  1,  1,  1    /* FORM_VIRTUAL_CASH_DESK_WITHDRAWN */
    UNION ALL SELECT  @new_profile_id4,  15,  5,   1,  1,  1,  1    /* FORM_VIRTUAL_CARD_OPER_ADD_REDEEM_CREDIT */
    UNION ALL SELECT  @new_profile_id4,  15,  6,   1,  1,  1,  1    /* FORM_VIRTUAL_CARD_OPER_LAST_MOV */
    UNION ALL SELECT  @new_profile_id4,  15,  7,   1,  1,  1,  1    /* FORM_VIRTUAL_ACCOUNT_OPER_CARD */
    UNION ALL SELECT  @new_profile_id4,  15,  8,   1,  1,  1,  1    /* FORM_VIRTUAL_ACCOUNT_OPER_EDIT */
    UNION ALL SELECT  @new_profile_id4,  15,  9,   1,  1,  1,  1    /* FORM_VIRTUAL_ACCOUNT_OPER_LOCK */
    UNION ALL SELECT  @new_profile_id4,  15,  10,  1,  1,  1,  1    /* FORM_VIRTUAL_PRINT_CARD */
    UNION ALL SELECT  @new_profile_id4,  15,  11,  0,  0,  0,  0    /* FORM_VIRTUAL_OPTIONS_DB_CONFIG */
    UNION ALL SELECT  @new_profile_id4,  15,  12,  1,  1,  1,  1    /* FORM_VIRTUAL_OPTIONS_LANGUAGE */
    UNION ALL SELECT  @new_profile_id4,  15,  13,  1,  1,  1,  1    /* FORM_VIRTUAL_OPTIONS_CALIBRATE */
    UNION ALL SELECT  @new_profile_id4,  15,  14,  1,  1,  1,  1    /* FORM_VIRTUAL_GAME_SESSIONS */
    UNION ALL SELECT  @new_profile_id4,  15,  15,  1,  1,  1,  1    /* FORM_VIRTUAL_MOBILE_BANK */
    UNION ALL SELECT  @new_profile_id4,  15,  16,  1,  1,  1,  1    /* FORM_VIRTUAL_CASH_DESK_ACCESS */
    UNION ALL SELECT  @new_profile_id4,  15,  17,  1,  1,  1,  1    /* FORM_VIRTUAL_ACCOUNT_OPER_HANDPAY */
    UNION ALL SELECT  @new_profile_id4,  15,  18,  1,  1,  1,  1    /* FORM_VIRTUAL_ACCOUNT_OPER_HANDPAY_CANCEL */
    UNION ALL SELECT  @new_profile_id4,  15,  19,  1,  1,  1,  1    /* FORM_VIRTUAL_ACCOUNT_LOG_OFF */
    UNION ALL SELECT  @new_profile_id4,  15,  20,  1,  1,  1,  1    /* FORM_VIRTUAL_MANUAL_OPER_HANDPAY */
    UNION ALL SELECT  @new_profile_id4,  15,  21,  1,  1,  1,  1    /* FORM_VIRTUAL_MANUAL_OPER_HANDPAY_CANCEL */
    UNION ALL SELECT  @new_profile_id4,  15,  22,  1,  1,  1,  1    /* FORM_VIRTUAL_GIFT_REQUEST */
    UNION ALL SELECT  @new_profile_id4,  15,  23,  1,  1,  1,  1    /* FORM_VIRTUAL_GIFT_DELIVERY */
    UNION ALL SELECT  @new_profile_id4,  15,  24,  1,  1,  1,  1    /* FORM_VIRTUAL_ACCOUNT_LEVEL */
    UNION ALL SELECT  @new_profile_id4,  15,  25,  1,  1,  1,  1    /* FORM_VIRTUAL_PROMO_SPECIAL_PERMISSION_A */
    UNION ALL SELECT  @new_profile_id4,  15,  26,  1,  1,  1,  1    /* FORM_VIRTUAL_PROMO_SPECIAL_PERMISSION_B */
    
END
ELSE
  SELECT '***** Profile JEFE DE CAJA already exists *****';


/*****  T�CNICO DE SALA  *****/

IF NOT EXISTS (SELECT * FROM gui_user_profiles WHERE gup_name = 'T�CNICO DE SALA')
BEGIN

  DECLARE @new_profile_id5 AS int
  
  SELECT @new_profile_id5 = max (gup_profile_id) + 1 FROM gui_user_profiles
  
  INSERT INTO [dbo].[gui_user_profiles]
             ([gup_profile_id]
             ,[gup_name])
       VALUES
             ( @new_profile_id5
             , 'T�CNICO DE SALA');

        INSERT INTO [dbo].[gui_profile_forms]
                   ([gpf_profile_id], [gpf_gui_id], [gpf_form_id], [gpf_read_perm], [gpf_write_perm], [gpf_delete_perm], [gpf_execute_perm])
                                        /*  FORM  R   W   D   X    */
             SELECT  @new_profile_id5,  14,  1,   0,  0,  0,  0    /* FORM_USER_PROFILE_SEL */
   UNION ALL SELECT  @new_profile_id5,  14,  2,   0,  0,  0,  0    /* FORM_USER_EDIT */
   UNION ALL SELECT  @new_profile_id5,  14,  3,   0,  0,  0,  0    /* FORM_PROFILE_EDIT */
   UNION ALL SELECT  @new_profile_id5,  14,  4,   0,  0,  0,  0    /* FORM_GUI_AUDITOR */
   UNION ALL SELECT  @new_profile_id5,  14,  5,   0,  0,  0,  0    /* FORM_EVENTS_HISTORY */
   UNION ALL SELECT  @new_profile_id5,  14,  6,   0,  0,  0,  0    /* FORM_TERMINALS_SELECTION */
   UNION ALL SELECT  @new_profile_id5,  14,  7,   0,  0,  0,  0    /* FORM_STATISTICS_TERMINALS */
   UNION ALL SELECT  @new_profile_id5,  14,  8,   0,  0,  0,  0    /* FORM_STATISTICS_GAMES */
   UNION ALL SELECT  @new_profile_id5,  14,  9,   0,  0,  0,  0    /* FORM_STATISTICS_DATES */
   UNION ALL SELECT  @new_profile_id5,  14,  10,  0,  0,  0,  0    /* FORM_STATISTICS_HOURS */
   UNION ALL SELECT  @new_profile_id5,  14,  11,  0,  0,  0,  0    /* FORM_STATISTICS_CARDS */
   UNION ALL SELECT  @new_profile_id5,  14,  12,  0,  0,  0,  0    /* FORM_PLAYS_SESSIONS */
   UNION ALL SELECT  @new_profile_id5,  14,  13,  0,  0,  0,  0    /* FORM_PLAYS_PLAYS */
   UNION ALL SELECT  @new_profile_id5,  14,  14,  0,  0,  0,  0    /* FORM_WS_SESSIONS */
   UNION ALL SELECT  @new_profile_id5,  14,  15,  0,  0,  0,  0    /* FORM_STATISTICS_HOURS_GROUP */
   UNION ALL SELECT  @new_profile_id5,  14,  16,  0,  0,  0,  0    /* FORM_ACCT_TAXES */
   UNION ALL SELECT  @new_profile_id5,  14,  17,  0,  0,  0,  0    /* FORM_CASHIER_MOVS */
   UNION ALL SELECT  @new_profile_id5,  14,  18,  0,  0,  0,  0    /* FORM_CASHIER_SESSIONS */
   UNION ALL SELECT  @new_profile_id5,  14,  19,  0,  0,  0,  0    /* FORM_ACCOUNT_MOVEMENTS */
   UNION ALL SELECT  @new_profile_id5,  14,  20,  0,  0,  0,  0    /* FORM_ACCOUNT_SUMMARY */
   UNION ALL SELECT  @new_profile_id5,  14,  21,  0,  0,  0,  0    /* FORM_DATE_TERMINAL */
   UNION ALL SELECT  @new_profile_id5,  14,  22,  0,  0,  0,  0    /* FORM_CLASS_II_DRAW_AUDIT */
   UNION ALL SELECT  @new_profile_id5,  14,  24,  0,  0,  0,  0    /* FORM_CLASS_II_JACKPOT_CONFIGURATION */
   UNION ALL SELECT  @new_profile_id5,  14,  25,  0,  0,  0,  0    /* FORM_CLASS_II_JACKPOT_HISTORY */
   UNION ALL SELECT  @new_profile_id5,  14,  26,  0,  0,  0,  0    /* FORM_CLASS_II_JACKPOT_MONITOR */
   UNION ALL SELECT  @new_profile_id5,  14,  27,  0,  0,  0,  0    /* FORM_SW_DOWNLOAD_VERSIONS */
   UNION ALL SELECT  @new_profile_id5,  14,  28,  0,  0,  0,  0    /* FORM_SW_DOWNLOAD_DETAILS */
   UNION ALL SELECT  @new_profile_id5,  14,  29,  0,  0,  0,  0    /* FORM_CARD_RECORD */
   UNION ALL SELECT  @new_profile_id5,  14,  30,  0,  0,  0,  0    /* FORM_GENERAL_PARAMS */
   UNION ALL SELECT  @new_profile_id5,  14,  31,  0,  0,  0,  0    /* FORM_PROVIDER_DAY_TERMINAL */
   UNION ALL SELECT  @new_profile_id5,  14,  32,  0,  0,  0,  0    /* FORM_SERVICE_MONITOR */
   UNION ALL SELECT  @new_profile_id5,  14,  34,  0,  0,  0,  0    /* FORM_TERMINALS_BY_BUILD */
   UNION ALL SELECT  @new_profile_id5,  14,  35,  0,  0,  0,  0    /* FORM_CASHIER_SESSION_DETAIL */
   UNION ALL SELECT  @new_profile_id5,  14,  36,  0,  0,  0,  0    /* FORM_CLASS_II_JACKPOT_PROMO_MESSAGE */
   UNION ALL SELECT  @new_profile_id5,  14,  37,  0,  0,  0,  0    /* FORM_CASHIER_CONFIGURATION */
   UNION ALL SELECT  @new_profile_id5,  14,  38,  0,  0,  0,  0    /* FORM_LICENCE_VERSIONS */
   UNION ALL SELECT  @new_profile_id5,  14,  39,  0,  0,  0,  0    /* FORM_MISC_SW_BY_BUILD */
   UNION ALL SELECT  @new_profile_id5,  14,  40,  0,  0,  0,  0    /* FORM_LICENCE_DETAILS */
   UNION ALL SELECT  @new_profile_id5,  14,  45,  0,  0,  0,  0    /* FORM_TERMINAL_GAME_RELATION */
   UNION ALL SELECT  @new_profile_id5,  14,  46,  0,  0,  0,  0    /* FORM_EXPIRED_CREDITS */
   UNION ALL SELECT  @new_profile_id5,  14,  47,  0,  0,  0,  0    /* FORM_PROMOTIONS */
   UNION ALL SELECT  @new_profile_id5,  14,  48,  0,  0,  0,  0    /* FORM_PROMOTION_EDIT */
   UNION ALL SELECT  @new_profile_id5,  14,  49,  0,  0,  0,  0    /* FORM_TERMINALS_3GS_EDIT */
   UNION ALL SELECT  @new_profile_id5,  14,  50,  0,  0,  0,  0    /* FORM_DRAWS */
   UNION ALL SELECT  @new_profile_id5,  14,  51,  0,  0,  0,  0    /* FORM_DRAW_EDIT */
   UNION ALL SELECT  @new_profile_id5,  14,  52,  0,  0,  0,  0    /* FORM_COMMANDS_SEND */
   UNION ALL SELECT  @new_profile_id5,  14,  53,  0,  0,  0,  0    /* FORM_COMMANDS_HISTORY */
   UNION ALL SELECT  @new_profile_id5,  14,  54,  0,  0,  0,  0    /* FORM_HANDPAYS */
   UNION ALL SELECT  @new_profile_id5,  14,  55,  0,  0,  0,  0    /* FORM_TERMINALS_PENDING */
   UNION ALL SELECT  @new_profile_id5,  14,  56,  0,  0,  0,  0    /* FORM_MOBILE_BANK_MOVEMENTS */
   UNION ALL SELECT  @new_profile_id5,  14,  57,  0,  0,  0,  0    /* FORM_PROMOTION_REPORT */
   UNION ALL SELECT  @new_profile_id5,  14,  58,  0,  0,  0,  0    /* FORM_GIFTS_SEL */
   UNION ALL SELECT  @new_profile_id5,  14,  59,  0,  0,  0,  0    /* FORM_GIFTS_EDIT */
   UNION ALL SELECT  @new_profile_id5,  14,  60,  0,  0,  0,  0    /* FORM_TAXES_REPORT */
   UNION ALL SELECT  @new_profile_id5,  14,  61,  0,  0,  0,  0    /* FORM_TERMINALS_EDIT */
   UNION ALL SELECT  @new_profile_id5,  14,  62,  0,  0,  0,  0    /* FORM_SITE_JACKPOT_CONFIGURATION */
   UNION ALL SELECT  @new_profile_id5,  14,  63,  0,  0,  0,  0    /* FORM_SITE_GAP_REPORT */
   UNION ALL SELECT  @new_profile_id5,  14,  64,  0,  0,  0,  0    /* FORM_SITE_JACKPOT_HISTORY */
   UNION ALL SELECT  @new_profile_id5,  14,  65,  0,  0,  0,  0    /* FORM_GIFTS_HISTORY */
   UNION ALL SELECT  @new_profile_id5,  14,  66,  1,  1,  0,  1    /* FORM_TERMINALS_WITHOUT_ACTIVITY */
   UNION ALL SELECT  @new_profile_id5,  14,  67,  0,  0,  0,  0    /* FORM_MAILING_PROGRAMMING_SEL */
   UNION ALL SELECT  @new_profile_id5,  14,  68,  0,  0,  0,  0    /* FORM_MAILING_PROGRAMMING_EDIT */
   UNION ALL SELECT  @new_profile_id5,  14,  69,  0,  0,  0,  0    /* FORM_MAILING_PARAMETERS */
   UNION ALL SELECT  @new_profile_id5,  14,  147, 0,  0,  0,  0    /* FORM_GAME_METERS */
   		/* CASHIER */
   UNION ALL SELECT  @new_profile_id5,  15,  0,   0,  0,  0,  0    /* FORM_MAIN */
   UNION ALL SELECT  @new_profile_id5,  15,  1,   0,  0,  0,  0    /* FORM_VIRTUAL_ADD_NO_REDEEM */
   UNION ALL SELECT  @new_profile_id5,  15,  2,   0,  0,  0,  0    /* FORM_VIRTUAL_CASH_DESK_OPEN_CLOSE */
   UNION ALL SELECT  @new_profile_id5,  15,  3,   0,  0,  0,  0    /* FORM_VIRTUAL_CASH_DESK_DEPOSIT */
   UNION ALL SELECT  @new_profile_id5,  15,  4,   0,  0,  0,  0    /* FORM_VIRTUAL_CASH_DESK_WITHDRAWN */
   UNION ALL SELECT  @new_profile_id5,  15,  5,   0,  0,  0,  0    /* FORM_VIRTUAL_CARD_OPER_ADD_REDEEM_CREDIT */
   UNION ALL SELECT  @new_profile_id5,  15,  6,   0,  0,  0,  0    /* FORM_VIRTUAL_CARD_OPER_LAST_MOV */
   UNION ALL SELECT  @new_profile_id5,  15,  7,   0,  0,  0,  0    /* FORM_VIRTUAL_ACCOUNT_OPER_CARD */
   UNION ALL SELECT  @new_profile_id5,  15,  8,   0,  0,  0,  0    /* FORM_VIRTUAL_ACCOUNT_OPER_EDIT */
   UNION ALL SELECT  @new_profile_id5,  15,  9,   0,  0,  0,  0    /* FORM_VIRTUAL_ACCOUNT_OPER_LOCK */
   UNION ALL SELECT  @new_profile_id5,  15,  10,  0,  0,  0,  0    /* FORM_VIRTUAL_PRINT_CARD */
   UNION ALL SELECT  @new_profile_id5,  15,  11,  0,  0,  0,  0    /* FORM_VIRTUAL_OPTIONS_DB_CONFIG */
   UNION ALL SELECT  @new_profile_id5,  15,  12,  0,  0,  0,  0    /* FORM_VIRTUAL_OPTIONS_LANGUAGE */
   UNION ALL SELECT  @new_profile_id5,  15,  13,  0,  0,  0,  0    /* FORM_VIRTUAL_OPTIONS_CALIBRATE */
   UNION ALL SELECT  @new_profile_id5,  15,  14,  0,  0,  0,  0    /* FORM_VIRTUAL_GAME_SESSIONS */
   UNION ALL SELECT  @new_profile_id5,  15,  15,  0,  0,  0,  0    /* FORM_VIRTUAL_MOBILE_BANK */
   UNION ALL SELECT  @new_profile_id5,  15,  16,  0,  0,  0,  0    /* FORM_VIRTUAL_CASH_DESK_ACCESS */
   UNION ALL SELECT  @new_profile_id5,  15,  17,  0,  0,  0,  0    /* FORM_VIRTUAL_ACCOUNT_OPER_HANDPAY */
   UNION ALL SELECT  @new_profile_id5,  15,  18,  0,  0,  0,  0    /* FORM_VIRTUAL_ACCOUNT_OPER_HANDPAY_CANCEL */
   UNION ALL SELECT  @new_profile_id5,  15,  19,  0,  0,  0,  0    /* FORM_VIRTUAL_ACCOUNT_LOG_OFF */
   UNION ALL SELECT  @new_profile_id5,  15,  20,  0,  0,  0,  0    /* FORM_VIRTUAL_MANUAL_OPER_HANDPAY */
   UNION ALL SELECT  @new_profile_id5,  15,  21,  0,  0,  0,  0    /* FORM_VIRTUAL_MANUAL_OPER_HANDPAY_CANCEL */
   UNION ALL SELECT  @new_profile_id5,  15,  22,  0,  0,  0,  0    /* FORM_VIRTUAL_GIFT_REQUEST */
   UNION ALL SELECT  @new_profile_id5,  15,  23,  0,  0,  0,  0    /* FORM_VIRTUAL_GIFT_DELIVERY */
   UNION ALL SELECT  @new_profile_id5,  15,  24,  0,  0,  0,  0    /* FORM_VIRTUAL_ACCOUNT_LEVEL */
   UNION ALL SELECT  @new_profile_id5,  15,  25,  0,  0,  0,  0    /* FORM_VIRTUAL_PROMO_SPECIAL_PERMISSION_A */
   UNION ALL SELECT  @new_profile_id5,  15,  26,  0,  0,  0,  0    /* FORM_VIRTUAL_PROMO_SPECIAL_PERMISSION_B */
    
END
ELSE
  SELECT '***** Profile T�CNICO DE SALA already exists *****';


/*****  T�CNICO WIN  *****/

IF NOT EXISTS (SELECT * FROM gui_user_profiles WHERE gup_name = 'T�CNICO WIN')
BEGIN

  DECLARE @new_profile_id6 AS int
  
  SELECT @new_profile_id6 = max (gup_profile_id) + 1 FROM gui_user_profiles
  
  INSERT INTO [dbo].[gui_user_profiles]
             ([gup_profile_id]
             ,[gup_name])
       VALUES
             ( @new_profile_id6
             , 'T�CNICO WIN');
 
        INSERT INTO [dbo].[gui_profile_forms]
                   ([gpf_profile_id], [gpf_gui_id], [gpf_form_id], [gpf_read_perm], [gpf_write_perm], [gpf_delete_perm], [gpf_execute_perm])
                                      /*  FORM  R   W   D   X    */
            SELECT @new_profile_id6,  14,  1,   1,  1,  0,  1    /* FORM_USER_PROFILE_SEL */
  UNION ALL SELECT @new_profile_id6,  14,  2,   1,  1,  0,  1    /* FORM_USER_EDIT */
  UNION ALL SELECT @new_profile_id6,  14,  3,   1,  1,  0,  1    /* FORM_PROFILE_EDIT */
  UNION ALL SELECT @new_profile_id6,  14,  4,   1,  1,  0,  1    /* FORM_GUI_AUDITOR */
  UNION ALL SELECT @new_profile_id6,  14,  5,   0,  0,  0,  0    /* FORM_EVENTS_HISTORY */
  UNION ALL SELECT @new_profile_id6,  14,  6,   1,  1,  0,  1    /* FORM_TERMINALS_SELECTION */
  UNION ALL SELECT @new_profile_id6,  14,  7,   0,  0,  0,  0    /* FORM_STATISTICS_TERMINALS */
  UNION ALL SELECT @new_profile_id6,  14,  8,   0,  0,  0,  0    /* FORM_STATISTICS_GAMES */
  UNION ALL SELECT @new_profile_id6,  14,  9,   0,  0,  0,  0    /* FORM_STATISTICS_DATES */
  UNION ALL SELECT @new_profile_id6,  14,  10,  0,  0,  0,  0    /* FORM_STATISTICS_HOURS */
  UNION ALL SELECT @new_profile_id6,  14,  11,  0,  0,  0,  0    /* FORM_STATISTICS_CARDS */
  UNION ALL SELECT @new_profile_id6,  14,  12,  0,  0,  0,  0    /* FORM_PLAYS_SESSIONS */
  UNION ALL SELECT @new_profile_id6,  14,  13,  0,  0,  0,  0    /* FORM_PLAYS_PLAYS */
  UNION ALL SELECT @new_profile_id6,  14,  14,  0,  0,  0,  0    /* FORM_WS_SESSIONS */
  UNION ALL SELECT @new_profile_id6,  14,  15,  0,  0,  0,  0    /* FORM_STATISTICS_HOURS_GROUP */
  UNION ALL SELECT @new_profile_id6,  14,  16,  0,  0,  0,  0    /* FORM_ACCT_TAXES */
  UNION ALL SELECT @new_profile_id6,  14,  17,  0,  0,  0,  0    /* FORM_CASHIER_MOVS */
  UNION ALL SELECT @new_profile_id6,  14,  18,  0,  0,  0,  0    /* FORM_CASHIER_SESSIONS */
  UNION ALL SELECT @new_profile_id6,  14,  19,  0,  0,  0,  0    /* FORM_ACCOUNT_MOVEMENTS */
  UNION ALL SELECT @new_profile_id6,  14,  20,  0,  0,  0,  0    /* FORM_ACCOUNT_SUMMARY */
  UNION ALL SELECT @new_profile_id6,  14,  21,  0,  0,  0,  0    /* FORM_DATE_TERMINAL */
  UNION ALL SELECT @new_profile_id6,  14,  22,  0,  0,  0,  0    /* FORM_CLASS_II_DRAW_AUDIT */
  UNION ALL SELECT @new_profile_id6,  14,  24,  0,  0,  0,  0    /* FORM_CLASS_II_JACKPOT_CONFIGURATION */
  UNION ALL SELECT @new_profile_id6,  14,  25,  0,  0,  0,  0    /* FORM_CLASS_II_JACKPOT_HISTORY */
  UNION ALL SELECT @new_profile_id6,  14,  26,  0,  0,  0,  0    /* FORM_CLASS_II_JACKPOT_MONITOR */
  UNION ALL SELECT @new_profile_id6,  14,  27,  1,  0,  0,  0    /* FORM_SW_DOWNLOAD_VERSIONS */
  UNION ALL SELECT @new_profile_id6,  14,  28,  0,  0,  0,  0    /* FORM_SW_DOWNLOAD_DETAILS */
  UNION ALL SELECT @new_profile_id6,  14,  29,  0,  0,  0,  0    /* FORM_CARD_RECORD */
  UNION ALL SELECT @new_profile_id6,  14,  30,  0,  0,  0,  0    /* FORM_GENERAL_PARAMS */
  UNION ALL SELECT @new_profile_id6,  14,  31,  0,  0,  0,  0    /* FORM_PROVIDER_DAY_TERMINAL */
  UNION ALL SELECT @new_profile_id6,  14,  32,  1,  1,  0,  1    /* FORM_SERVICE_MONITOR */
  UNION ALL SELECT @new_profile_id6,  14,  34,  1,  1,  0,  1    /* FORM_TERMINALS_BY_BUILD */
  UNION ALL SELECT @new_profile_id6,  14,  35,  0,  0,  0,  0    /* FORM_CASHIER_SESSION_DETAIL */
  UNION ALL SELECT @new_profile_id6,  14,  36,  0,  0,  0,  0    /* FORM_CLASS_II_JACKPOT_PROMO_MESSAGE */
  UNION ALL SELECT @new_profile_id6,  14,  37,  0,  0,  0,  0    /* FORM_CASHIER_CONFIGURATION */
  UNION ALL SELECT @new_profile_id6,  14,  38,  1,  1,  0,  1    /* FORM_LICENCE_VERSIONS */
  UNION ALL SELECT @new_profile_id6,  14,  39,  1,  1,  0,  1    /* FORM_MISC_SW_BY_BUILD */
  UNION ALL SELECT @new_profile_id6,  14,  40,  1,  1,  0,  1    /* FORM_LICENCE_DETAILS */
  UNION ALL SELECT @new_profile_id6,  14,  45,  1,  1,  0,  1    /* FORM_TERMINAL_GAME_RELATION */
  UNION ALL SELECT @new_profile_id6,  14,  46,  0,  0,  0,  0    /* FORM_EXPIRED_CREDITS */
  UNION ALL SELECT @new_profile_id6,  14,  47,  0,  0,  0,  0    /* FORM_PROMOTIONS */
  UNION ALL SELECT @new_profile_id6,  14,  48,  0,  0,  0,  0    /* FORM_PROMOTION_EDIT */
  UNION ALL SELECT @new_profile_id6,  14,  49,  1,  1,  0,  1    /* FORM_TERMINALS_3GS_EDIT */
  UNION ALL SELECT @new_profile_id6,  14,  50,  0,  0,  0,  0    /* FORM_DRAWS */
  UNION ALL SELECT @new_profile_id6,  14,  51,  0,  0,  0,  0    /* FORM_DRAW_EDIT */
  UNION ALL SELECT @new_profile_id6,  14,  52,  0,  0,  0,  0    /* FORM_COMMANDS_SEND */
  UNION ALL SELECT @new_profile_id6,  14,  53,  0,  0,  0,  0    /* FORM_COMMANDS_HISTORY */
  UNION ALL SELECT @new_profile_id6,  14,  54,  0,  0,  0,  0    /* FORM_HANDPAYS */
  UNION ALL SELECT @new_profile_id6,  14,  55,  1,  1,  0,  1    /* FORM_TERMINALS_PENDING */
  UNION ALL SELECT @new_profile_id6,  14,  56,  0,  0,  0,  0    /* FORM_MOBILE_BANK_MOVEMENTS */
  UNION ALL SELECT @new_profile_id6,  14,  57,  0,  0,  0,  0    /* FORM_PROMOTION_REPORT */
  UNION ALL SELECT @new_profile_id6,  14,  58,  0,  0,  0,  0    /* FORM_GIFTS_SEL */
  UNION ALL SELECT @new_profile_id6,  14,  59,  0,  0,  0,  0    /* FORM_GIFTS_EDIT */
  UNION ALL SELECT @new_profile_id6,  14,  60,  0,  0,  0,  0    /* FORM_TAXES_REPORT */
  UNION ALL SELECT @new_profile_id6,  14,  61,  1,  1,  0,  1    /* FORM_TERMINALS_EDIT */
  UNION ALL SELECT @new_profile_id6,  14,  62,  0,  0,  0,  0    /* FORM_SITE_JACKPOT_CONFIGURATION */
  UNION ALL SELECT @new_profile_id6,  14,  63,  0,  0,  0,  0    /* FORM_SITE_GAP_REPORT */
  UNION ALL SELECT @new_profile_id6,  14,  64,  0,  0,  0,  0    /* FORM_SITE_JACKPOT_HISTORY */
  UNION ALL SELECT @new_profile_id6,  14,  65,  0,  0,  0,  0    /* FORM_GIFTS_HISTORY */
  UNION ALL SELECT @new_profile_id6,  14,  66,  1,  1,  0,  1    /* FORM_TERMINALS_WITHOUT_ACTIVITY */
  UNION ALL SELECT @new_profile_id6,  14,  67,  0,  0,  0,  0    /* FORM_MAILING_PROGRAMMING_SEL */
  UNION ALL SELECT @new_profile_id6,  14,  68,  0,  0,  0,  0    /* FORM_MAILING_PROGRAMMING_EDIT */
  UNION ALL SELECT @new_profile_id6,  14,  69,  0,  0,  0,  0    /* FORM_MAILING_PARAMETERS */
  UNION ALL SELECT @new_profile_id6,  14,  147, 0,  0,  0,  0    /* FORM_GAME_METERS */
  		/* CASHIER */
  UNION ALL SELECT @new_profile_id6,  15,  0,   0,  0,  0,  0    /* FORM_MAIN */
  UNION ALL SELECT @new_profile_id6,  15,  1,   0,  0,  0,  0    /* FORM_VIRTUAL_ADD_NO_REDEEM */
  UNION ALL SELECT @new_profile_id6,  15,  2,   0,  0,  0,  0    /* FORM_VIRTUAL_CASH_DESK_OPEN_CLOSE */
  UNION ALL SELECT @new_profile_id6,  15,  3,   0,  0,  0,  0    /* FORM_VIRTUAL_CASH_DESK_DEPOSIT */
  UNION ALL SELECT @new_profile_id6,  15,  4,   0,  0,  0,  0    /* FORM_VIRTUAL_CASH_DESK_WITHDRAWN */
  UNION ALL SELECT @new_profile_id6,  15,  5,   0,  0,  0,  0    /* FORM_VIRTUAL_CARD_OPER_ADD_REDEEM_CREDIT */
  UNION ALL SELECT @new_profile_id6,  15,  6,   0,  0,  0,  0    /* FORM_VIRTUAL_CARD_OPER_LAST_MOV */
  UNION ALL SELECT @new_profile_id6,  15,  7,   0,  0,  0,  0    /* FORM_VIRTUAL_ACCOUNT_OPER_CARD */
  UNION ALL SELECT @new_profile_id6,  15,  8,   0,  0,  0,  0    /* FORM_VIRTUAL_ACCOUNT_OPER_EDIT */
  UNION ALL SELECT @new_profile_id6,  15,  9,   0,  0,  0,  0    /* FORM_VIRTUAL_ACCOUNT_OPER_LOCK */
  UNION ALL SELECT @new_profile_id6,  15,  10,  0,  0,  0,  0    /* FORM_VIRTUAL_PRINT_CARD */
  UNION ALL SELECT @new_profile_id6,  15,  11,  0,  0,  0,  0    /* FORM_VIRTUAL_OPTIONS_DB_CONFIG */
  UNION ALL SELECT @new_profile_id6,  15,  12,  0,  0,  0,  0    /* FORM_VIRTUAL_OPTIONS_LANGUAGE */
  UNION ALL SELECT @new_profile_id6,  15,  13,  0,  0,  0,  0    /* FORM_VIRTUAL_OPTIONS_CALIBRATE */
  UNION ALL SELECT @new_profile_id6,  15,  14,  0,  0,  0,  0    /* FORM_VIRTUAL_GAME_SESSIONS */
  UNION ALL SELECT @new_profile_id6,  15,  15,  0,  0,  0,  0    /* FORM_VIRTUAL_MOBILE_BANK */
  UNION ALL SELECT @new_profile_id6,  15,  16,  0,  0,  0,  0    /* FORM_VIRTUAL_CASH_DESK_ACCESS */
  UNION ALL SELECT @new_profile_id6,  15,  17,  0,  0,  0,  0    /* FORM_VIRTUAL_ACCOUNT_OPER_HANDPAY */
  UNION ALL SELECT @new_profile_id6,  15,  18,  0,  0,  0,  0    /* FORM_VIRTUAL_ACCOUNT_OPER_HANDPAY_CANCEL */
  UNION ALL SELECT @new_profile_id6,  15,  19,  0,  0,  0,  0    /* FORM_VIRTUAL_ACCOUNT_LOG_OFF */
  UNION ALL SELECT @new_profile_id6,  15,  20,  0,  0,  0,  0    /* FORM_VIRTUAL_MANUAL_OPER_HANDPAY */
  UNION ALL SELECT @new_profile_id6,  15,  21,  0,  0,  0,  0    /* FORM_VIRTUAL_MANUAL_OPER_HANDPAY_CANCEL */
  UNION ALL SELECT @new_profile_id6,  15,  22,  0,  0,  0,  0    /* FORM_VIRTUAL_GIFT_REQUEST */
  UNION ALL SELECT @new_profile_id6,  15,  23,  0,  0,  0,  0    /* FORM_VIRTUAL_GIFT_DELIVERY */
  UNION ALL SELECT @new_profile_id6,  15,  24,  0,  0,  0,  0    /* FORM_VIRTUAL_ACCOUNT_LEVEL */
  UNION ALL SELECT @new_profile_id6,  15,  25,  0,  0,  0,  0    /* FORM_VIRTUAL_PROMO_SPECIAL_PERMISSION_A */
  UNION ALL SELECT @new_profile_id6,  15,  26,  0,  0,  0,  0    /* FORM_VIRTUAL_PROMO_SPECIAL_PERMISSION_B */
 
END
ELSE
  SELECT '***** Profile T�CNICO WIN already exists *****';

