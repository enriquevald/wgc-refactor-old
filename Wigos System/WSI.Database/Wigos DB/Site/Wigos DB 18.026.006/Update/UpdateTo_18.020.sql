/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 19;

SET @New_ReleaseId = 20;
SET @New_ScriptName = N'UpdateTo_18.020.sql';
SET @New_Description = N'Misc. Promotions & Indexes; tax pct fields on Cashier_Sessions; new Site Jackpots - 2.';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/


/****** INDEXES ******/


/****** STORED PROCEDURES ******/


/****** TRIGGERS ******/


/****** RECORDS ******/
/* Site_Jackpot_Parameters */
IF NOT EXISTS (SELECT * FROM Site_Jackpot_Parameters)
INSERT INTO [dbo].[site_jackpot_parameters]
           ([sjp_enabled]
           ,[sjp_contribution_pct]
           ,[sjp_awarding_days]
           ,[sjp_awarding_start]
           ,[sjp_awarding_end]
           ,[sjp_awarding_min_occupation_pct]
           ,[sjp_awarding_exclude_promotions]
           ,[sjp_awarding_exclude_anonymous]
           ,[sjp_animation_interval]
           ,[sjp_recent_interval]
           ,[sjp_promo_message1]
           ,[sjp_promo_message2]
           ,[sjp_promo_message3]
           ,[sjp_to_compensate]
           ,[sjp_only_redeemable]
           ,[sjp_average_interval_hours]
           ,[sjp_played])
     VALUES
           (0,
            0.0,
            0,
            0,
            0,
            0.0,
            1,
            1,
            15,
            3600,
            NULL,
            NULL,
            NULL,
            0.0,
            1,
            0,
            0.0 );           
ELSE
SELECT '***** Record Site_Jackpot_Parameters already exists *****';

/* Site_Jackpot_Instances */
IF NOT EXISTS (SELECT * FROM Site_Jackpot_Instances WHERE sji_index = 1)
INSERT INTO [dbo].[site_jackpot_instances]
           ([sji_index]
           ,[sji_name]
           ,[sji_contribution_pct]
           ,[sji_minimum]
           ,[sji_maximum]
           ,[sji_average]
           ,[sji_accumulated]
           ,[sji_num_pending])
     VALUES
           (1,
            'HIGH',
            0.0,
            0.0,
            0.0,
            0.0,
            0.0,
            0);
ELSE
SELECT '***** Record Site_Jackpot_Instances.1 already exists *****';            

IF NOT EXISTS (SELECT * FROM Site_Jackpot_Instances WHERE sji_index = 2)
INSERT INTO [dbo].[site_jackpot_instances]
           ([sji_index]
           ,[sji_name]
           ,[sji_contribution_pct]
           ,[sji_minimum]
           ,[sji_maximum]
           ,[sji_average]
           ,[sji_accumulated]
           ,[sji_num_pending])
     VALUES
           (2,
            'MEDIUM',
            0.0,
            0.0,
            0.0,
            0.0,
            0.0,
            0);
ELSE
SELECT '***** Record Site_Jackpot_Instances.2 already exists *****';            
            
IF NOT EXISTS (SELECT * FROM Site_Jackpot_Instances WHERE sji_index = 3)
INSERT INTO [dbo].[site_jackpot_instances]
           ([sji_index]
           ,[sji_name]
           ,[sji_contribution_pct]
           ,[sji_minimum]
           ,[sji_maximum]
           ,[sji_average]
           ,[sji_accumulated]
           ,[sji_num_pending])
     VALUES
           (3,
            'LOW',
            0.0,
            0.0,
            0.0,
            0.0,
            0.0,
            0);
ELSE
SELECT '***** Record Site_Jackpot_Instances.3 already exists *****';
