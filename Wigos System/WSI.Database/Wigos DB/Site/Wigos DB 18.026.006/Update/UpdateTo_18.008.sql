/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 7;

SET @New_ReleaseId = 8;
SET @New_ScriptName = N'UpdateTo_18.008.sql';
SET @New_Description = N'Missing & New Indexes; Handpays; WCP Provisioning. (Part II)';

/**** CHECK VERSION SECTION *****/
IF (EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON


/****** TABLES ******/


/****** INDEXES ******/
/****** Objeto:  Index [IX_status]    Fecha de la secuencia de comandos: 08/27/2010 05:24:43 ******/
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[wcp_commands]') AND name = N'IX_status')
DROP INDEX [IX_status] ON [dbo].[wcp_commands];

CREATE NONCLUSTERED INDEX [IX_status] ON [dbo].[wcp_commands] 
(
                [cmd_status] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY];


/****** RECORDS ******/
IF EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY ='MinTimeToCloseSession')
DELETE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY ='MinTimeToCloseSession';

INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('Cashier'
           ,'MinTimeToCloseSession'
           ,'300');

IF EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY ='TrackDataVisibleChars')
DELETE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY ='TrackDataVisibleChars';

INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('Cashier'
           ,'TrackDataVisibleChars'
           ,'6');
           

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;

END
ELSE
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION
GO
