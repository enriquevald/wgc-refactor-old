/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 23;

SET @New_ReleaseId = 24;
SET @New_ScriptName = N'UpdateTo_18.024.006.sql';
SET @New_Description = N'New field sjp_animation_interval2 in table Site_Jackpot_Parameters.';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/
/* Site_Jackpot_Parameters */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[site_jackpot_parameters]') and name = 'sjp_animation_interval2')
ALTER TABLE [dbo].[site_jackpot_parameters] 
	ADD [sjp_animation_interval2] [int] NULL;
ELSE
SELECT '***** Field site_jackpot_parameters.sjp_animation_interval2 already exists *****';
GO
UPDATE [dbo].[site_jackpot_parameters] 
   SET sjp_animation_interval2 = sjp_animation_interval;
GO
ALTER TABLE [dbo].[site_jackpot_parameters] 
	ALTER COLUMN [sjp_animation_interval2] [int] NOT NULL;


/****** INDEXES ******/
/****** Objeto:  Index [IX_ps_started_terminal_id]    Fecha de la secuencia de comandos: 02/09/2011 12:15:28 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[play_sessions]') AND name = N'IX_ps_started_terminal_id')
CREATE NONCLUSTERED INDEX [IX_ps_started_terminal_id] ON [dbo].[play_sessions] 
(
	[ps_started] ASC,
	[ps_terminal_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
ELSE
    SELECT '***** Index play_sessions.IX_ps_started_terminal_id already exists *****';


/****** TRIGGERS ******/


/****** RECORDS ******/
   
   
/**************************************** 3GS Section ****************************************/

/**** CHECK DATABASE EXISTENCE SECTION *****/
GO
IF (NOT EXISTS (SELECT * FROM sys.databases WHERE name = 'sPOS'))
BEGIN
	SELECT 'No 3GS interface to update.';

	raiserror('No 3GS interface to update', 20, -1) with log;
END
ELSE 
    SELECT 'Updating 3GS interface...';


/**** 3GS DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);
    
/**** 3GS INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 5;

SET @New_ReleaseId = 6;
SET @New_ScriptName = N'UpdateTo_18.024.006.sql';
SET @New_Description = N'Updated status output parameters from nvarchar to varchar.';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version_interface_3gs WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong 3GS DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION_INTERFACE_3GS

raiserror('Not updated', 20, -1) with log
END

/**** UPDATE VERSION SECTION *****/
UPDATE db_version_interface_3gs
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;


/**** UPDATE BODY SECTION *****/
GO
/***** zsp_SessionEnd *****/
ALTER PROCEDURE [dbo].[zsp_SessionEnd]
  @AccountId      varchar(24),
  @VendorId       varchar(16),
  @SerialNumber   varchar(30),
  @MachineNumber  int,
  @SessionId 	    bigint,
  @AmountPlayed   money,
  @AmountWon      money,
  @GamesPlayed    int,
  @GamesWon       int,
  @CreditBalance  money,
  @CurrentJackpot money = 0
AS
BEGIN

  BEGIN TRAN

  -- SET NOCOUNT ON added to prevent extra result sets from
  -- interfering with SELECT statements.
  SET NOCOUNT ON;

  DECLARE @status_code         int
  DECLARE @status_text         varchar (254)
  DECLARE @terminal_id         int
  DECLARE @account_id          bigint
  DECLARE @previous_balance    money
  DECLARE @delta_amount_played money
  DECLARE @delta_amount_won    money
  DECLARE @ignore_session_id   int
  DECLARE @is_end_session      int

  SET @status_code         = 0
  SET @status_text         = 'Successful End Session'
  SET @delta_amount_played = 0
  SET @delta_amount_won    = 0
  SET @is_end_session      = 1

  SELECT @terminal_id = dbo.GetTerminalID(@VendorId, @SerialNumber, @MachineNumber) 

  IF (@terminal_id = 0)
  BEGIN
    SET @status_code = 2
    SET @status_text = 'Invalid Machine Information'
    GOTO ERROR_PROCEDURE
  END

  SELECT @account_id = dbo.GetAccountID(@AccountId)

  IF (@account_id = 0)
  BEGIN
    SET @status_code = 1
    SET @status_text = 'Invalid Account Number'
    GOTO ERROR_PROCEDURE
  END
  
  SELECT @ignore_session_id = CAST(GP_KEY_VALUE AS int)
    FROM GENERAL_PARAMS 
   WHERE GP_GROUP_KEY = 'Interface3GS' AND GP_SUBJECT_KEY ='IgnoreSessionID'
  
  IF ( @ignore_session_id = 1 )
  BEGIN
    SELECT @SessionId = ISNULL(AC_CURRENT_PLAY_SESSION_ID , 0 ) 
      FROM   ACCOUNTS                   
     WHERE ( AC_ACCOUNT_ID = @account_id )
  END

  EXECUTE dbo.SessionUpdate_Internal @account_id, @terminal_id, @SerialNumber, @SessionId,
                                     @AmountPlayed, @AmountWon, @GamesPlayed, @GamesWon,
                                     @CreditBalance, @CurrentJackpot, @is_end_session, 
                                     @delta_amount_played OUTPUT, @delta_amount_won OUTPUT,
                                     @status_code OUTPUT, @status_text OUTPUT
  IF ( @status_code <> 0 )
    GOTO ERROR_PROCEDURE

  -- close play session
  EXECUTE dbo.CloseOpenedPlaySession @SessionId, @status_code OUTPUT, @status_text OUTPUT

  IF ( @status_code <> 0 )
    GOTO ERROR_PROCEDURE
    
  -- Close account session
  EXECUTE dbo.CloseAccountSession @account_id

  -- Insert Movement
  
  -- Use delta values to calculate initial balance
  --SET @previous_balance = @CreditBalance + @AmountPlayed - @AmountWon
  SET @previous_balance = @CreditBalance + @delta_amount_played - @delta_amount_won

  IF (1 = (SELECT   CASE WHEN ( (AC_PROMO_CREATION <= GETDATE()) AND (GETDATE() < AC_PROMO_EXPIRATION) AND (AC_PROMO_BALANCE >0) ) THEN 1 ELSE 0 END
            FROM   ACCOUNTS                   
           WHERE ( AC_ACCOUNT_ID = @account_id )))
  BEGIN
    -- PromoEndSession   = 25,
    EXECUTE dbo.InsertMovement @SessionId, @account_id, @terminal_id, 25, @previous_balance, @delta_amount_played, @delta_amount_won, @CreditBalance

  END
	ELSE
  BEGIN
    -- EndCardSession = 6,
    EXECUTE dbo.InsertMovement @SessionId, @account_id, @terminal_id,  6, @previous_balance, @delta_amount_played, @delta_amount_won, @CreditBalance
  END
	  
  SET @status_text = 'Successful End Session'

  COMMIT TRAN
	GOTO OK  
	  
	ERROR_PROCEDURE:
	  ROLLBACK TRAN
	  
	OK:  
	
	-- Audit MBF 17-JUN-2010
	BEGIN TRAN
	
    DECLARE @input AS nvarchar(MAX)
    DECLARE @output AS nvarchar(MAX)	
  		  	
	  SET @input = '@AccountID='+@AccountID
	             +';@VendorId='+@VendorId
	             +';@SerialNumber='+CAST (@SerialNumber AS nvarchar)
	             +';@MachineNumber='+CAST (@MachineNumber AS nvarchar)
	             +';@SessionId='+CAST (@SessionId AS nvarchar)
	             +';@AmountPlayed='+CAST (@AmountPlayed AS nvarchar)
	             +';@AmountWon='+CAST (@AmountWon AS nvarchar)
	             +';@GamesPlayed='+CAST (@GamesPlayed AS nvarchar)
	             +';@GamesWon='+CAST (@GamesWon AS nvarchar)
	             +';@CreditBalance='+CAST (@CreditBalance AS nvarchar)
	             +';@CurrentJackpot='+CAST (@CurrentJackpot AS nvarchar)
  	           
  	           
    SET @output = 'StatusCode='+CAST (@status_code AS nvarchar)
                 +';StatusText='+@status_text
     	
	  EXECUTE dbo.zsp_Audit 'zsp_SessionEnd'
                        , @AccountID
                        , @VendorId 
                        , @SerialNumber
                        , @MachineNumber
                        , @SessionId
                        , @status_code
                        , @CreditBalance
                        , @input
                        , @output
                      
  COMMIT TRAN
	-- End Audit
	
  SELECT @status_code AS StatusCode, @status_text AS StatusText

END -- zsp_SessionEnd
GO


/***** zsp_SessionStart *****/
ALTER PROCEDURE [dbo].[zsp_SessionStart]
	@AccountID 	    varchar(24),
	@VendorId 	      varchar(16),
	@SerialNumber    varchar(30),
	@MachineNumber   int,
	@CurrentJackpot  money = 0,
	@VendorSessionID bigint = 0
AS
BEGIN

  BEGIN TRAN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
		
  DECLARE @status_code         int
	DECLARE @status_text         varchar (254)
	DECLARE @session_id          bigint
	DECLARE @balance             money
	DECLARE @terminal_id         bigint
	DECLARE @account_id          bigint
	DECLARE @is_promotion        int
	DECLARE @promo_balance       money
	DECLARE @account_terminal_id bigint
	DECLARE @current_session_id  bigint
	DECLARE @movement_type       int
	DECLARE @cash_in_while_playing money
    -- 09-AUG-2010, Return the welcome message with the customer name
    DECLARE @holder_name         nvarchar (50)
	
    SET @status_code        = 0
	SET @status_text        = 'Bienvenido'
	SET @session_id         = 0
	SET @balance            = 0
	SET @current_session_id = 0

	SELECT @terminal_id = dbo.GetTerminalID(@VendorId, @SerialNumber, @MachineNumber) 
		
  IF (@terminal_id = 0)
  BEGIN
    EXECUTE dbo.InsertTerminal 0, @VendorId, @SerialNumber, @MachineNumber
    SET @status_code = 3
    SET @status_text = 'Invalid Machine Information'
	  COMMIT TRAN
    BEGIN TRAN
    GOTO ERROR_PROCEDURE
  END
  
	IF ((SELECT dbo.CheckTerminalBlocked(@terminal_ID)) = 1)
  BEGIN
    SET @status_code = 3
    SET @status_text = 'Invalid Machine Information'
    GOTO ERROR_PROCEDURE
  END

	SELECT @account_id = dbo.GetAccountID(@AccountID)
  
  IF (@account_ID = 0)
  BEGIN
    SET @status_code = 1
    SET @status_text = 'Invalid Account Number'
    GOTO ERROR_PROCEDURE
  END
	
	IF ((SELECT dbo.CheckAccountBlocked(@account_ID)) = 1)
  BEGIN
    SET @status_code = 4
    SET @status_text = 'Access Denied'
    GOTO ERROR_PROCEDURE
  END
	
	SELECT @is_promotion        = CASE WHEN ( (AC_PROMO_CREATION <= GETDATE()) AND (GETDATE() < AC_PROMO_EXPIRATION) AND (AC_PROMO_BALANCE >0) ) THEN 1 ELSE 0 END
       , @promo_balance       = AC_PROMO_BALANCE 
       , @balance             = AC_BALANCE 
       , @current_session_id  = ISNULL(AC_CURRENT_PLAY_SESSION_ID , 0 ) 
       , @account_terminal_id = ISNULL(AC_CURRENT_TERMINAL_ID , 0 )
       -- 09-AUG-2010, Return the welcome message with the customer name
       , @holder_name         = ISNULL(AC_HOLDER_NAME, ' ')
       , @cash_in_while_playing = ISNULL(AC_CASHIN_WHILE_PLAYING, 0)
  FROM   ACCOUNTS                   
  WHERE ( AC_ACCOUNT_ID = @account_ID )
	
	IF (@is_promotion = 1)
  BEGIN
    SET @balance = @promo_balance
  END
	
	
	IF (@current_session_id <> 0)
	BEGIN
	  IF ( @account_terminal_id <> @terminal_id )
	  BEGIN
	    SET @status_code = 2
      SET @status_text = 'Account Locked (In Session)' 
      GOTO ERROR_PROCEDURE
	  END

    IF (@cash_in_while_playing = 0)
    BEGIN
	    SET @status_code = 2
      SET @status_text = 'Account Locked (In Session)' 
      GOTO ERROR_PROCEDURE
	  END

    -- Close account session
    EXECUTE dbo.CloseAccountSession @account_id
  
	END





  DECLARE @insert_ps_table TABLE (StatusCode int, StatusText nvarchar(254), SessionID bigint)
  INSERT INTO @insert_ps_table EXECUTE  dbo.InsertPlaySession @terminal_id, @account_id, @balance, @is_promotion    
  SELECT @status_code = StatusCode
       , @status_text = StatusText
       , @session_id  = SessionID
    FROM @insert_ps_table
  
  IF ( @status_code <> 0 )
    GOTO ERROR_PROCEDURE
   
  EXECUTE dbo.InsertGameMeters @terminal_id
	
	SET @movement_type = CASE WHEN (@is_promotion = 1) THEN 24 ELSE 5 END
	
	EXECUTE dbo.InsertMovement @session_id
                           , @account_id
                           , @terminal_id
                           , @movement_type
                           , @balance
                           , 0
                           , 0
                           , @balance
	
	

	
    -- 09-AUG-2010, Return the welcome message with the customer name
    SET @status_text = 'Bienvenido ' + @holder_name
	--    IF (@is_promotion = 1)
	--    BEGIN
	--       SET @status_text = @status_text + ' --- NO REDIMIBLE ---'
	--    END

    -- AJQ 17-SEP-2010, Reset CashinWhilePlaying
    UPDATE ACCOUNTS
       SET AC_CASHIN_WHILE_PLAYING    = NULL
     WHERE AC_ACCOUNT_ID              = @account_id
       AND AC_CURRENT_TERMINAL_ID     = @terminal_id
       AND AC_CURRENT_PLAY_SESSION_ID = @session_id 
	 
	COMMIT TRAN
	GOTO OK  
	  
	ERROR_PROCEDURE:
	  ROLLBACK TRAN
	  
	OK:  
	
	-- Audit MBF 17-JUN-2010
	BEGIN TRAN
	
    DECLARE @input AS nvarchar(MAX)
    DECLARE @output AS nvarchar(MAX)	
  	
	  SET @input = '@AccountID='+@AccountID
	             +';@VendorId='+@VendorId
	             +';@SerialNumber='+CAST (@SerialNumber AS nvarchar)
	             +';@MachineNumber='+CAST (@MachineNumber AS nvarchar)
	             +';@CurrentJackpot='+CAST (@CurrentJackpot AS nvarchar)
	             +';@VendorSessionID='+CAST (@VendorSessionID AS nvarchar)
  	           
  	           
    SET @output = 'StatusCode='+CAST (@status_code AS nvarchar)
                 +';StatusText='+@status_text
                 +';SessionID='+CAST(@session_id AS nvarchar)
                 +';Balance='+CAST(@balance AS nvarchar)
     	
	  EXECUTE dbo.zsp_Audit 'zsp_SessionStart'
                        , @AccountID
                        , @VendorId 
                        , @SerialNumber
                        , @MachineNumber
                        , @session_id
                        , @status_code
                        , @balance
                        , @input
                        , @output        
  COMMIT TRAN
	-- End Audit
	
	SELECT @status_code AS StatusCode, @status_text AS StatusText, @session_id AS SessionId, @balance AS Balance

END -- zsp_SessionStart
GO


/***** zsp_SendEvent *****/
ALTER PROCEDURE [dbo].[zsp_SendEvent]
	@AccountID 	     varchar(24),
	@VendorId 	     varchar(16),
	@SerialNumber    varchar(30),
	@MachineNumber   int,
	@EventID         bigint, 
	@Amount          money
AS
BEGIN

  BEGIN TRAN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
  DECLARE @status_code         int
	DECLARE @status_text         varchar (254)
	DECLARE @operation_code      int
	DECLARE @play_session_id     bigint
	DECLARE @terminal_id         bigint
	DECLARE @account_id          bigint
	DECLARE @account_terminal_id bigint
	
  SET @status_code        = @EventID + 100
	SET @status_text        = ''
		
	SELECT @terminal_id = dbo.GetTerminalID(@VendorId, @SerialNumber, @MachineNumber) 
	  
  IF (@terminal_id = 0)
  BEGIN
    EXECUTE dbo.InsertTerminal 0, @VendorId, @SerialNumber, @MachineNumber
    SET @status_code = 3
    SET @status_text = 'Invalid Machine Information'
	  COMMIT TRAN
    BEGIN TRAN
    GOTO ERROR_PROCEDURE
  END
		
	SELECT @account_id = dbo.GetAccountID(@AccountID)
	
	IF (@account_id = 0)
  BEGIN
    SET @status_code = 1
    SET @status_text = 'Invalid Account Number'
    GOTO ERROR_PROCEDURE
  END
	
	SET @operation_code = CASE WHEN (@EventID = 1) THEN 16
	                           WHEN (@EventID = 2) THEN 17
	                           WHEN (@EventID = 3) THEN 18
	                                               ELSE  0 END
	
	-- Unknown codes, do nothing
	IF (@operation_code = 0)
	BEGIN
    GOTO ERROR_PROCEDURE
	END
	
	SELECT @play_session_id     = ISNULL(AC_CURRENT_PLAY_SESSION_ID , 0 ) 
       , @account_terminal_id = ISNULL(AC_CURRENT_TERMINAL_ID , 0 )
  FROM   ACCOUNTS                   
  WHERE ( AC_ACCOUNT_ID = @account_id )
	
	-- Check if account has a play session
	IF (@play_session_id = 0)
	BEGIN
	  SET @status_code = 4
    SET @status_text = 'Access Denied'
    GOTO ERROR_PROCEDURE
	END
	
	-- Check if the terminal is the same as the playsession
	IF (@account_terminal_id <> @terminal_id)
	BEGIN
	  SET @status_code = 4
    SET @status_text = 'Access Denied'
    GOTO ERROR_PROCEDURE
	END
		
	INSERT INTO EVENT_HISTORY (EH_TERMINAL_ID 
                           , EH_SESSION_ID 
                           , EH_DATETIME 
                           , EH_EVENT_TYPE 
                           , EH_OPERATION_CODE 
                           , EH_OPERATION_DATA)
                     VALUES (@terminal_id
                           , @play_session_id
                           , GETDATE()
                           , 2 
                           , @operation_code
                           , @Amount)
	
	COMMIT TRAN
	GOTO OK  
	  
	ERROR_PROCEDURE:
	ROLLBACK TRAN
	  
	OK: 
	
	-- Audit MBF 17-JUN-2010
	BEGIN TRAN
	
    DECLARE @input AS nvarchar(MAX)
    DECLARE @output AS nvarchar(MAX)
  		  	
	  SET @input = '@AccountID='+@AccountID
	             +';@VendorId='+@VendorId
	             +';@SerialNumber='+CAST (@SerialNumber AS nvarchar)
	             +';@MachineNumber='+CAST (@MachineNumber AS nvarchar)
	             +';@EventID='+CAST (@EventID AS nvarchar)
	             +';@Amount='+CAST (@Amount AS nvarchar)  	           
  	           
    SET @output = 'StatusCode='+CAST (@status_code AS nvarchar)
                 +';StatusText='+@status_text
     	
	  EXECUTE dbo.zsp_Audit 'zsp_SendEvent'
                        , @AccountID
                        , @VendorId 
                        , @SerialNumber
                        , @MachineNumber
                        , NULL
                        , @status_code
                        , NULL
                        , @input
                        , @output
                      
  COMMIT TRAN
	-- End Audit
	 
	SELECT @status_code AS StatusCode, @status_text AS StatusText

	
END -- zsp_SendEvent
GO


/***** CheckAndGetPlaySessionData *****/
ALTER PROCEDURE dbo.CheckAndGetPlaySessionData
 (@SessionId      bigint,
  @PlayedAmount   money,
  @WonAmount      money,
  @PlayedCount    int,
  @WonCount       int,
  @CreditBalance  money,
  @IsEndSession   int)
AS
BEGIN
  DECLARE @rc                   int
  DECLARE @status_code          int
  DECLARE @status_text          varchar (254)
  DECLARE @played_amount        money
  DECLARE @won_amount           money
  DECLARE @played_count         int
  DECLARE @won_count            int
  DECLARE @delta_played_amount  money
  DECLARE @delta_won_amount     money
  DECLARE @delta_played_count   int
  DECLARE @delta_won_count      int

  SET @status_code = 0
  SET @status_text = ''

  IF (  ( @PlayedAmount  < 0 )
     OR ( @WonAmount     < 0 )
     OR ( @PlayedCount   < 0 )
     OR ( @WonCount      < 0 )
     OR ( @CreditBalance < 0 ))
  BEGIN
    SET @status_code  = 3
    SET @status_text  = 'Invalid Session ID number'
    GOTO ERROR_PROCEDURE
  END

  SELECT   @played_count  = PS_PLAYED_COUNT
         , @played_amount = PS_PLAYED_AMOUNT
         , @won_count     = PS_WON_COUNT
         , @won_amount    = PS_WON_AMOUNT
    FROM   PLAY_SESSIONS
   WHERE   PS_PLAY_SESSION_ID = @SessionId

  SET @rc = @@ROWCOUNT
  IF ( @rc <> 1 )
  BEGIN
    SET @status_code  = 3
    SET @status_text  = 'Invalid Session ID number'
    GOTO ERROR_PROCEDURE
  END

  -- 04-AUG-2010 MBF: Patch for CADILLAC vendor
  IF ( ( @IsEndSession <> 0 ) AND ( @WonCount = 0 ) )
  BEGIN
    SET @WonCount = @won_count
  END
    
  -- AJQ 18-SEP-2010, Accept counters going back

  SET @delta_played_amount = @PlayedAmount - @played_amount
  SET @delta_won_amount    = @WonAmount    - @won_amount
  SET @delta_played_count  = @PlayedCount  - @played_count
  SET @delta_won_count     = @WonCount     - @won_count

ERROR_PROCEDURE:
	SELECT @status_code         AS StatusCode,        @status_text      AS StatusText
	     , @delta_played_amount AS DeltaPlayedAmount, @delta_won_amount AS DeltaWonAmount
	     , @delta_played_count  AS DeltaPlayedCount,  @delta_won_count  AS DeltaWonCount

END -- CheckAndGetPlaySessionData
GO


/***** UpdatePlaySessionData *****/
ALTER PROCEDURE dbo.UpdatePlaySessionData
 (@SessionId      bigint,
  @CreditsPlayed  money,
  @CreditsWon 	  money,
  @GamesPlayed 	  int,
  @GamesWon 	  int,
  @CreditBalance  money)
AS
BEGIN
  DECLARE @rc             int
  DECLARE @status_code    int
	DECLARE @status_text    varchar (254)

  SET @status_code = 0
	SET @status_text = ''

  UPDATE PLAY_SESSIONS
  SET PS_PLAYED_COUNT  = @GamesPlayed
    , PS_PLAYED_AMOUNT = @CreditsPlayed
    , PS_WON_COUNT     = @GamesWon
    , PS_WON_AMOUNT    = @CreditsWon
    , PS_FINAL_BALANCE = @CreditBalance
    , PS_FINISHED      = GETDATE()  
  WHERE PS_PLAY_SESSION_ID = @SessionId

  SET @rc = @@ROWCOUNT
  IF ( @rc <> 1 )
  BEGIN
    SET @status_code  = 3
    SET @status_text  = 'Invalid Session ID number'
    GOTO ERROR_PROCEDURE
  END

ERROR_PROCEDURE:
	SELECT @status_code AS StatusCode, @status_text AS StatusText

END -- UpdatePlaySessionData
GO


/***** UpdateAccountData *****/
ALTER PROCEDURE dbo.UpdateAccountData
 (@AccountId         bigint,
  @SessionId         bigint,
  @TerminalId        int,
  @ReportedBalance   money,
  @IsEndSession      int)
AS
BEGIN
  DECLARE @is_promotion   int
  DECLARE @promo_balance  money
  DECLARE @balance        money
  DECLARE @rc             int
  DECLARE @status_code    int
  DECLARE @status_text    varchar (254)
  DECLARE @aux_amount     money
  DECLARE @cashin_playing money
  DECLARE @am_id_started  bigint

  SET @status_code = 0
    SET @status_text = ''

  SELECT @is_promotion   = CASE WHEN ( (AC_PROMO_CREATION <= GETDATE()) AND (GETDATE() < AC_PROMO_EXPIRATION) AND (AC_PROMO_BALANCE >0) ) THEN 1 ELSE 0 END
       , @promo_balance  = AC_PROMO_BALANCE 
       , @balance        = AC_BALANCE 
  FROM   ACCOUNTS                   
  WHERE ( AC_ACCOUNT_ID = @AccountId )

  SET @rc = @@ROWCOUNT
  IF ( @rc <> 1 )
  BEGIN
    SET @is_promotion = 0
    SET @balance      = 0
    SET @status_code  = 1
    SET @status_text  = 'Invalid Account Number'
    GOTO ERROR_PROCEDURE
  END

  IF ( @is_promotion = 1 )
  BEGIN
    -- AJQ 16-SEP-2010, We believe the machine
    SET @promo_balance  = @ReportedBalance 
  END
  ELSE
  BEGIN
    -- AJQ 16-SEP-2010, We believe the machine
    SET @balance        = @ReportedBalance 
    
    SELECT   @cashin_playing    = ISNULL (AC_CASHIN_WHILE_PLAYING, 0)
      FROM   ACCOUNTS
     WHERE   AC_ACCOUNT_ID      = @AccountId;
       
    -- AJQ 17-SEP-2010, Add all the CashIn's while playing
    SET @balance = @ReportedBalance + @cashin_playing      

  END

  IF ( @is_promotion = 1 )
  BEGIN
    UPDATE ACCOUNTS
    SET AC_PROMO_BALANCE  = @promo_balance
      , AC_LAST_ACTIVITY  = GETDATE()
    WHERE AC_ACCOUNT_ID              = @AccountId
      AND AC_CURRENT_TERMINAL_ID     = @TerminalId
      AND AC_CURRENT_PLAY_SESSION_ID = @SessionId 
  END
  ELSE
  BEGIN
  
    IF ( @IsEndSession = 1 )
      BEGIN
        UPDATE ACCOUNTS
           SET AC_CASHIN_WHILE_PLAYING    = NULL
         WHERE AC_ACCOUNT_ID              = @AccountId
           AND AC_CURRENT_TERMINAL_ID     = @TerminalId
           AND AC_CURRENT_PLAY_SESSION_ID = @SessionId     	
    END
    
    UPDATE ACCOUNTS
    SET AC_BALANCE        = @balance
      , AC_LAST_ACTIVITY  = GETDATE()
    WHERE AC_ACCOUNT_ID              = @AccountId
      AND AC_CURRENT_TERMINAL_ID     = @TerminalId
      AND AC_CURRENT_PLAY_SESSION_ID = @SessionId 
    
  END
  
  SET @rc = @@ROWCOUNT
  IF ( @rc <> 1 )
  BEGIN
    SET @status_code  = 1
    SET @status_text  = 'Invalid Account Number'
    GOTO ERROR_PROCEDURE
  END

ERROR_PROCEDURE:
  IF ( @is_promotion = 0 ) SET @aux_amount = @balance
  ELSE SET @aux_amount = @promo_balance

    SELECT @status_code AS StatusCode, @status_text AS StatusText, @aux_amount AS Balance

END -- UpdateAccountData
GO


/***** zsp_AccountStatus *****/
ALTER PROCEDURE [dbo].[zsp_AccountStatus]
	@AccountID 	     varchar(24),
	@VendorId 	     varchar(16),
	@MachineNumber   int
AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
  DECLARE @status_code           int
	DECLARE @status_text           varchar (254)
	DECLARE @terminal_id           bigint
	DECLARE @account_id            bigint
	DECLARE @balance               money
	DECLARE @is_promotion          int
	DECLARE @promo_balance         money
	DECLARE @play_session_id       bigint
  DECLARE @ignore_machine_number int
	
  SET @status_code        = 0
	SET @status_text        = 'Available Account'
	SET @balance            = 0
	
	SELECT @ignore_machine_number = CAST(GP_KEY_VALUE AS int)
    FROM GENERAL_PARAMS 
   WHERE GP_GROUP_KEY = 'Interface3GS' AND GP_SUBJECT_KEY ='IgnoreMachineNumber'
	
	IF ( @ignore_machine_number <> 1 )
	BEGIN
    IF (1 <> (SELECT COUNT(*) 
                FROM TERMINALS_3GS
               WHERE T3GS_MACHINE_NUMBER = @MachineNumber
                 AND T3GS_VENDOR_ID      = @VendorId))
    BEGIN
      SET @status_code = 3
      SET @status_text = 'Invalid Machine Number'
      GOTO ERROR_PROCEDURE
    END
  END
		
	SELECT @account_id = dbo.GetAccountID(@AccountID)
	
	IF (@account_id = 0)
  BEGIN
    SET @status_code = 1
    SET @status_text = 'Invalid Account'
    GOTO ERROR_PROCEDURE
  END

  
  SELECT @play_session_id     = ISNULL(AC_CURRENT_PLAY_SESSION_ID , 0 ) 
       , @is_promotion        = CASE WHEN ( (AC_PROMO_CREATION <= GETDATE()) AND (GETDATE() < AC_PROMO_EXPIRATION) AND (AC_PROMO_BALANCE >0) ) THEN 1 ELSE 0 END
       , @promo_balance       = AC_PROMO_BALANCE 
       , @balance             = AC_BALANCE 
    FROM   ACCOUNTS                   
   WHERE ( AC_ACCOUNT_ID = @account_id )
   
 	-- Check if account has a play session
	IF (@play_session_id <> 0)
	BEGIN
	  SET @status_code = 2
    SET @status_text = 'Account In Session'
    GOTO ERROR_PROCEDURE
	END  
	
	IF (@is_promotion = 1)
  BEGIN
    SET @balance = @promo_balance
  END 
     
	ERROR_PROCEDURE:
	IF (@status_code <> 0)
	  SET @balance = 0
	
	-- Audit MBF 17-JUN-2010
	BEGIN TRAN
	
    DECLARE @input AS nvarchar(MAX)
    DECLARE @output AS nvarchar(MAX)	
  		  	  		  	
	  SET @input = '@AccountID='+@AccountID
	             +';@VendorId='+@VendorId
	             +';@MachineNumber='+CAST (@MachineNumber AS nvarchar)  	           
  	           
    SET @output = 'StatusCode='+CAST (@status_code AS nvarchar)
                 +';StatusText='+@status_text
                 +';AcctBalance='+CAST (@balance AS nvarchar)
                 +';VID=0'
     	
	  EXECUTE dbo.zsp_Audit 'zsp_AccountStatus'
                        , @AccountID
                        , @VendorId 
                        , NULL
                        , @MachineNumber
                        , NULL
                        , @status_code
                        , NULL
                        , @input
                        , @output
                      
  COMMIT TRAN
	-- End Audit
	
	SELECT @status_code AS StatusCode, @status_text AS StatusText, @balance AS Balance, '0' AS VID

END -- zsp_AccountStatus
GO


/***** zsp_SessionUpdate *****/
ALTER PROCEDURE [dbo].[zsp_SessionUpdate]
  @AccountId      varchar(24),
  @VendorId       varchar(16),
  @SerialNumber   varchar(30),
  @MachineNumber  int,
  @SessionId      bigint,
  @AmountPlayed   money,
  @AmountWon      money,
  @GamesPlayed    int,
  @GamesWon       int,
  @CreditBalance  money,
  @CurrentJackpot money = 0
AS
BEGIN

  BEGIN TRAN

  -- SET NOCOUNT ON added to prevent extra result sets from
  -- interfering with SELECT statements.
  SET NOCOUNT ON;
	
  DECLARE @status_code         int
  DECLARE @status_text         varchar (254)
  DECLARE @terminal_id         int
  DECLARE @account_id          bigint
  DECLARE @previous_balance    money
  DECLARE @delta_amount_played money
  DECLARE @delta_amount_won    money
  DECLARE @ignore_session_id   int
  DECLARE @dummy               int

  SET @status_code         = 0
  SET @status_text         = 'Successful Session Update'
  SET @delta_amount_played = 0
  SET @delta_amount_won    = 0
  SET @dummy               = 0

  SELECT @terminal_id = dbo.GetTerminalID(@VendorId, @SerialNumber, @MachineNumber) 
		
  IF (@terminal_id = 0)
  BEGIN
    SET @status_code = 2
    SET @status_text = 'Invalid Machine Information'
    GOTO ERROR_PROCEDURE
  END

  SELECT @account_id = dbo.GetAccountID(@AccountId)

  IF (@account_id = 0)
  BEGIN
    SET @status_code = 1
    SET @status_text = 'Invalid Account Number'
    GOTO ERROR_PROCEDURE
  END
  
  SELECT @ignore_session_id = CAST(GP_KEY_VALUE AS int)
    FROM GENERAL_PARAMS 
   WHERE GP_GROUP_KEY = 'Interface3GS' AND GP_SUBJECT_KEY ='IgnoreSessionID'
  
  IF ( @ignore_session_id = 1 )
  BEGIN
    SELECT @SessionId = ISNULL(AC_CURRENT_PLAY_SESSION_ID , AC_LAST_PLAY_SESSION_ID ) 
      FROM   ACCOUNTS                   
     WHERE ( AC_ACCOUNT_ID = @account_id )
  END

  EXECUTE dbo.SessionUpdate_Internal @account_id, @terminal_id, @SerialNumber, @SessionId,
                                     @AmountPlayed, @AmountWon, @GamesPlayed, @GamesWon,
                                     @CreditBalance, @CurrentJackpot, @dummy, 
                                     @delta_amount_played OUTPUT, @delta_amount_won OUTPUT,
                                     @status_code OUTPUT, @status_text OUTPUT
  IF ( @status_code <> 0 )
    GOTO ERROR_PROCEDURE

  
  -- Insert Movement
  
  -- Use delta values to calculate initial balance
  --SET @previous_balance = @CreditBalance + @AmountPlayed - @AmountWon
  SET @previous_balance = @CreditBalance + @delta_amount_played - @delta_amount_won
  
  EXECUTE dbo.InsertMovement @SessionId, @account_id, @terminal_id, 0, @previous_balance, @delta_amount_played, @delta_amount_won, @CreditBalance

  SET @status_text = 'Successful Session Update'
	
  COMMIT TRAN
	GOTO OK  
	  
ERROR_PROCEDURE:
  ROLLBACK TRAN
	  
OK: 

	-- Audit MBF 17-JUN-2010
	BEGIN TRAN
	
    DECLARE @input AS nvarchar(MAX)
    DECLARE @output AS nvarchar(MAX)	
  		  	
	  SET @input = '@AccountID='+@AccountID
	             +';@VendorId='+@VendorId
	             +';@SerialNumber='+CAST (@SerialNumber AS nvarchar)
	             +';@MachineNumber='+CAST (@MachineNumber AS nvarchar)
	             +';@SessionId='+CAST (@SessionId AS nvarchar)
	             +';@AmountPlayed='+CAST (@AmountPlayed AS nvarchar)
	             +';@AmountWon='+CAST (@AmountWon AS nvarchar)
	             +';@GamesPlayed='+CAST (@GamesPlayed AS nvarchar)
	             +';@GamesWon='+CAST (@GamesWon AS nvarchar)
	             +';@CreditBalance='+CAST (@CreditBalance AS nvarchar)
	             +';@CurrentJackpot='+CAST (@CurrentJackpot AS nvarchar)
  	           
  	           
    SET @output = 'StatusCode='+CAST (@status_code AS nvarchar)
                 +';StatusText='+@status_text
     	
	  EXECUTE dbo.zsp_Audit 'zsp_SessionUpdate'
                        , @AccountID
                        , @VendorId 
                        , @SerialNumber
                        , @MachineNumber
                        , @SessionId
                        , @status_code
                        , @CreditBalance
                        , @input
                        , @output
                      
  COMMIT TRAN
	-- End Audit

  SELECT @status_code AS StatusCode, @status_text AS StatusText

END -- zsp_SessionUpdate
GO


/**** FINAL SECTION *****/
SELECT '3GS Update OK.';
