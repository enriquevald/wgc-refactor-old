/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 17;

SET @New_ReleaseId = 18;
SET @New_ScriptName = N'UpdateTo_18.018.sql';
SET @New_Description = N'Missing fields from DB releases 15-17.';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/

/* Draws */

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[draws]') and name = 'dr_credit_type')
ALTER TABLE [dbo].[draws] 
	ADD [dr_credit_type] [int] NOT NULL CONSTRAINT [DF_dr_credit_type] DEFAULT (0);
ELSE
SELECT '***** Field draws.dr_credit_type already exists *****';

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[draws]') and name = 'dr_header')
ALTER TABLE [dbo].[draws] 
	ADD [dr_header] [nvarchar](260) NULL;
ELSE
SELECT '***** Field draws.dr_header already exists *****';

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[draws]') and name = 'dr_footer')
ALTER TABLE [dbo].[draws] 
	ADD [dr_footer] [nvarchar](512) NULL;
ELSE
SELECT '***** Field draws.dr_footer already exists *****';

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[draws]') and name = 'dr_detail1')
ALTER TABLE [dbo].[draws] 
	ADD [dr_detail1] [nvarchar](260) NULL;
ELSE
SELECT '***** Field draws.dr_detail1 already exists *****';

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[draws]') and name = 'dr_detail2')
ALTER TABLE [dbo].[draws] 
	ADD [dr_detail2] [nvarchar](260) NULL;
ELSE
SELECT '***** Field draws.dr_detail2 already exists *****';

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[draws]') and name = 'dr_detail3')
ALTER TABLE [dbo].[draws] 
	ADD [dr_detail3] [nvarchar](260) NULL;
ELSE
SELECT '***** Field draws.dr_detail3 already exists *****';

/* Account_Operations */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[account_operations]') and name = 'ao_operation_data')
ALTER TABLE [dbo].[account_operations] 
	ADD [ao_operation_data] [bigint] NULL;
ELSE
SELECT '***** Field account_operations.ao_operation_data already exists *****';

/* This statement intentionally left here */
GO

UPDATE [dbo].[draws] 
	SET dr_detail1 = dr_name;


/****** INDEXES ******/


/****** STORED PROCEDURES ******/


/****** TRIGGERS ******/


/****** RECORDS ******/

