/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 21;

SET @New_ReleaseId = 22;
SET @New_ScriptName = N'UpdateTo_18.022.sql';
SET @New_Description = N'New Mailing services; automatic printing of draw tickets.';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/

/****** Object:  Table [dbo].[mailing_programming]    Script Date: 12/21/2010 18:19:42 ******/
IF NOT EXISTS (SELECT * FROM SYS.OBJECTS WHERE TYPE = 'U' AND NAME = 'mailing_programming')
BEGIN
    CREATE TABLE [dbo].[mailing_programming](
	[mp_prog_id] [bigint] IDENTITY(1,1) NOT NULL,
	[mp_name] [nvarchar](50) NOT NULL,
	[mp_enabled] [bit] NOT NULL CONSTRAINT [DF_mailing_scheduling_ms_enabled]  DEFAULT ((0)),
	[mp_type] [int] NOT NULL,
	[mp_address_list] [nvarchar](500) NOT NULL,
	[mp_subject] [nvarchar](200) NOT NULL,
	[mp_schedule_weekday] [int] NOT NULL,
	[mp_schedule_time_from] [int] NOT NULL,
	[mp_schedule_time_to] [int] NOT NULL,
	[mp_schedule_time_step] [int] NOT NULL,
    CONSTRAINT [PK_mailing_programming] PRIMARY KEY CLUSTERED 
    (
	[mp_prog_id] ASC
    )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
    ) ON [PRIMARY];

    EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1: Statistics' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'mailing_programming', @level2type=N'COLUMN',@level2name=N'mp_type';
    EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Time in minutes (0..1439)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'mailing_programming', @level2type=N'COLUMN',@level2name=N'mp_schedule_time_from';
    EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Time in minutes (0..1439)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'mailing_programming', @level2type=N'COLUMN',@level2name=N'mp_schedule_time_to';
END
ELSE
    SELECT '***** Table mailing_programming already exists *****';

/****** Object:  Table [dbo].[mailing_instances]    Script Date: 12/21/2010 17:45:13 ******/
IF NOT EXISTS (SELECT * FROM SYS.OBJECTS WHERE TYPE = 'U' AND NAME = 'mailing_instances')
BEGIN
    CREATE TABLE [dbo].[mailing_instances](
	[mi_mailing_instance_id] [bigint] IDENTITY(1,1) NOT NULL,
	[mi_prog_id] [bigint] NOT NULL,
	[mi_prog_date] [datetime] NOT NULL,
	[mi_prog_data] [bigint] NOT NULL,
	[mi_name] [nvarchar](50) NOT NULL,
	[mi_type] [int] NOT NULL,
	[mi_datetime] [datetime] NOT NULL,
	[mi_address_list] [nvarchar](500) NOT NULL,
	[mi_subject] [nvarchar](200) NOT NULL,
	[mi_message] [nvarchar](max) NOT NULL,
	[mi_status] [int] NOT NULL CONSTRAINT [DF_mailing_instances_mi_status]  DEFAULT ((0)),
	[mi_result] [nvarchar](500) NULL,
     CONSTRAINT [PK_mailing_instances] PRIMARY KEY CLUSTERED 
    (
	[mi_mailing_instance_id] ASC
    )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
    ) ON [PRIMARY];

    EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Time in minutes (0..1439)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'mailing_instances', @level2type=N'COLUMN',@level2name=N'mi_prog_data';
    EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1: Statistics' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'mailing_instances', @level2type=N'COLUMN',@level2name=N'mi_type';
    EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0: Pending; 1: Running; 2: Successful; 3: Failed' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'mailing_instances', @level2type=N'COLUMN',@level2name=N'mi_status';
END
ELSE
    SELECT '***** Table mailing_instances already exists *****';

/* Draws */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[draws]') and name = 'dr_min_played_pct')
ALTER TABLE [dbo].[draws] 
	ADD [dr_min_played_pct] [numeric](6, 2) NULL;
ELSE
SELECT '***** Field draws.dr_min_played_pct already exists *****';

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[draws]') and name = 'dr_min_spent_pct')
ALTER TABLE [dbo].[draws] 
	ADD [dr_min_spent_pct] [numeric](5, 2) NULL;
ELSE
SELECT '***** Field draws.dr_min_spent_pct already exists *****';

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[draws]') and name = 'dr_min_cash_in')
ALTER TABLE [dbo].[draws] 
	ADD [dr_min_cash_in] [money] NULL;
ELSE
SELECT '***** Field draws.dr_min_cash_in already exists *****';



/****** INDEXES ******/
/****** Objeto:  Index [IX_session_id]    Fecha de la secuencia de comandos: 01/05/2011 04:36:04 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[cashier_vouchers]') AND name = N'IX_session_id')
    CREATE NONCLUSTERED INDEX [IX_session_id] ON [dbo].[cashier_vouchers] 
	( [cv_session_id] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY];
ELSE
    SELECT '***** Index cashier_vouchers.IX_session_id already exists *****';

/****** Objeto:  Index [IX_operation_id]    Fecha de la secuencia de comandos: 01/05/2011 04:36:20 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[cashier_vouchers]') AND name = N'IX_operation_id')
    CREATE NONCLUSTERED INDEX [IX_operation_id] ON [dbo].[cashier_vouchers] 
	( [cv_operation_id] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY];
ELSE
    SELECT '***** Index cashier_vouchers.IX_operation_id already exists *****';

/****** Object:  Index [IX_mailing_instances]    Script Date: 01/14/2011 15:39:04 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[mailing_instances]') AND name = N'IX_mailing_instances')
    CREATE UNIQUE NONCLUSTERED INDEX [IX_mailing_instances] ON [dbo].[mailing_instances] 
	( [mi_prog_id] ASC,
	  [mi_prog_date] ASC,
	  [mi_prog_data] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY];
ELSE
    SELECT '***** Index mailing_instances.IX_mailing_instances already exists *****';

/****** Objeto:  Index [IX_ps_finished]    Fecha de la secuencia de comandos: 01/17/2011 04:00:04 ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[play_sessions]') AND name = N'IX_ps_finished')
DROP INDEX [IX_ps_finished] ON [dbo].[play_sessions] WITH ( ONLINE = OFF );

CREATE NONCLUSTERED INDEX [IX_ps_finished] ON [dbo].[play_sessions] 
(
	[ps_terminal_id] ASC,
	[ps_finished] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY];
    

/****** STORED PROCEDURES ******/
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PT_PlaySessionFinished]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PT_PlaySessionFinished]
GO
CREATE PROCEDURE [dbo].[PT_PlaySessionFinished]
  @PlaySessionId      bigint
AS
BEGIN

	-- Only for test
  DECLARE @status_code                       int
  DECLARE @status_text                       nvarchar (500)
  -- Only for test
  
  DECLARE @terminal_id                       int
  DECLARE @account_id                        bigint
  DECLARE @total_cash_in                     money
  DECLARE @played_amount                     money
  DECLARE @won_amount                        money
  DECLARE @total_cash_out                    money
  DECLARE @initial_non_redeemable            money
  DECLARE @initial_cash_in                   money
  DECLARE @prize_lock                        money
  DECLARE @account_balance                   money
  DECLARE @holder_name                       nvarchar (50)
  DECLARE @holder_level                      int
  
  DECLARE @non_redeemable_cash_in            money
  DECLARE @redeemable_cash_in                money
  DECLARE @non_redeemable_cash_out           money
  DECLARE @redeemable_cash_out               money
  DECLARE @non_redeemable_played             money
  DECLARE @redeemable_played                 money
  DECLARE @non_redeemable_won                money
  DECLARE @redeemable_won                    money
    
  DECLARE @spent_no_redeemable               money
  DECLARE @spent_redeemable                  money

  DECLARE @profit_no_redeemable              money
  DECLARE @profit_redeemable                 money
  
  DECLARE @total_sum_redimible               numeric (20,6)
  DECLARE @total_sum                         numeric (20,6)
  DECLARE @percent_redeemable                numeric (8,6)

  DECLARE @rest_played_and_no_spent          money
  DECLARE @rest_won_and_no_profit            money
  
  DECLARE @total_played_to_1_point           numeric (20,6)
  DECLARE @redeemable_played_to_1_point      numeric (20,6)
  DECLARE @redeemable_spent_to_1_point       numeric (20,6)

  DECLARE @points_balance_before             money
  DECLARE @points_balance_after              money
  DECLARE @points_total_played               money
  DECLARE @points_played_redeemable          money
  DECLARE @points_spent_redeemable           money
  DECLARE @won_points                        money
  
  DECLARE @max_allowed_acc_balance           numeric (20,6)

  -- SET NOCOUNT ON added to prevent extra result sets from
  -- interfering with SELECT statements.
  SET NOCOUNT ON;
	
  SET @status_code  = 0
  SET @status_text  = 'PT_PlaySessionFinished: Init'

  SET @points_total_played      = 0
  SET @points_played_redeemable = 0
  SET @points_spent_redeemable  = 0
  SET @won_points               = 0  
  SET @points_balance_before    = 0
  SET @points_balance_after     = 0


  IF ( @PlaySessionId = 0 ) 
  BEGIN
    SET @status_text = 'PT_PlaySessionFinished: Invalid PlaySessionId'
    GOTO ERROR_PROCEDURE
  END

  --
  -- Read Session & Account & PlayerTracking Data
  --
  EXECUTE dbo.PT_ReadData @PlaySessionId, @terminal_id OUTPUT, @account_id OUTPUT,
                          @total_cash_in OUTPUT, @played_amount OUTPUT, @won_amount OUTPUT, @total_cash_out OUTPUT, 
                          @initial_non_redeemable OUTPUT, @initial_cash_in OUTPUT, @prize_lock OUTPUT, @account_balance OUTPUT, 
                          @points_balance_before OUTPUT, @holder_name OUTPUT, @holder_level OUTPUT, @max_allowed_acc_balance OUTPUT,
                          @status_code OUTPUT, @status_text OUTPUT

  IF ( @status_code <> 0 )
    GOTO ERROR_PROCEDURE
    
  --
  -- Compute PlayedCredits, PlayedRedeemableCredits, SpentRedeemableCredits
  --
  EXECUTE dbo.PT_BalancePartsWhenPlaying @total_cash_in, @initial_cash_in, @initial_non_redeemable, @prize_lock, 
                                         @non_redeemable_cash_in OUTPUT, @redeemable_cash_in OUTPUT

  EXECUTE dbo.PT_BalancePartsWhenPlaying @total_cash_out, @initial_cash_in, @initial_non_redeemable, @prize_lock, 
                                         @non_redeemable_cash_out OUTPUT, @redeemable_cash_out OUTPUT

  SET @spent_no_redeemable      = dbo.Maximum_Money (0, @non_redeemable_cash_in - @non_redeemable_cash_out)
  SET @spent_redeemable         = dbo.Maximum_Money (0, @redeemable_cash_in - @redeemable_cash_out)

  SET @profit_no_redeemable     = dbo.Maximum_Money (0, @non_redeemable_cash_out - @non_redeemable_cash_in)
  SET @profit_redeemable        = dbo.Maximum_Money (0, @redeemable_cash_out - @redeemable_cash_in)

  SET @total_sum_redimible      = @redeemable_cash_in + @redeemable_cash_out 
  SET @total_sum                = @non_redeemable_cash_in + @non_redeemable_cash_out + @redeemable_cash_in + @redeemable_cash_out
  
  IF @total_sum = 0 GOTO ERROR_PROCEDURE   
  SET @percent_redeemable       = @total_sum_redimible / @total_sum

  SET @rest_played_and_no_spent = @played_amount - (@spent_no_redeemable + @spent_redeemable)
  SET @redeemable_played        = ROUND(@spent_redeemable + (@rest_played_and_no_spent * @percent_redeemable), 4)
  SET @non_redeemable_played    = @played_amount - @redeemable_played

  SET @rest_won_and_no_profit   = @won_amount - (@profit_no_redeemable + @profit_redeemable)
  SET @redeemable_won           = ROUND(@profit_redeemable + (@rest_won_and_no_profit * @percent_redeemable), 4)
  SET @non_redeemable_won       = @won_amount - @redeemable_won

  --
  -- Update PlaySession table (Redeemable & NonRedeemable amounts)
  --
  UPDATE PLAY_SESSIONS
     SET PS_NON_REDEEMABLE_CASH_IN    = @non_redeemable_cash_in
       , PS_NON_REDEEMABLE_CASH_OUT   = @non_redeemable_cash_out
       , PS_NON_REDEEMABLE_PLAYED     = @non_redeemable_played
       , PS_NON_REDEEMABLE_WON        = @non_redeemable_won
       , PS_REDEEMABLE_CASH_IN        = @redeemable_cash_in
       , PS_REDEEMABLE_CASH_OUT       = @redeemable_cash_out
       , PS_REDEEMABLE_PLAYED         = @redeemable_played
       , PS_REDEEMABLE_WON            = @redeemable_won
   WHERE PS_PLAY_SESSION_ID = @PlaySessionId 

  --
  -- RCI & ACC & AJQ 30/09/2010: Reset the Cancellable Operation when PlayedAmount greater than 0.
  --
  UPDATE   ACCOUNTS
     SET   AC_CANCELLABLE_OPERATION_ID = CASE WHEN (@played_amount > 0) THEN NULL ELSE AC_CANCELLABLE_OPERATION_ID END
   WHERE   AC_ACCOUNT_ID               = @account_id  

  --
  -- ACC 22/10/2010: Check maximum allowed account balance
  --
  IF ( @max_allowed_acc_balance > 0 AND @total_cash_out > @max_allowed_acc_balance )
  BEGIN
    UPDATE ACCOUNTS
       SET AC_BLOCKED       = 1
         , AC_BLOCK_REASON  = 2  -- MaxBalance
     WHERE AC_ACCOUNT_ID    = @account_id  
  END

  --- Anonymous accounts have Level=0
  IF ( @holder_level = 0 )
    GOTO EXIT_PROCEDURE

  --
  -- Read PlayerTracking Point Conversion Factors according to the holder's level
  --
  EXECUTE dbo.PT_ReadPointFactors @holder_level, 
                                  @redeemable_spent_to_1_point  OUTPUT,
                                  @redeemable_played_to_1_point OUTPUT,
                                  @total_played_to_1_point      OUTPUT,
                                  @status_code                  OUTPUT, 
                                  @status_text                  OUTPUT

  IF ( @status_code <> 0 )
    GOTO ERROR_PROCEDURE

  --
  -- Compute Points (it is possible to accummulate points per each type of credits at the same time)
  --  
  IF @redeemable_spent_to_1_point  > 0 SET @points_spent_redeemable  = @spent_redeemable  / @redeemable_spent_to_1_point
  IF @redeemable_played_to_1_point > 0 SET @points_played_redeemable = @redeemable_played / @redeemable_played_to_1_point
  IF @total_played_to_1_point      > 0 SET @points_total_played      = @played_amount     / @total_played_to_1_point
  
  SET @won_points = ROUND(@points_total_played + @points_played_redeemable + @points_spent_redeemable, 4)

  --
  -- Accumulate points
  -- 
  UPDATE   ACCOUNTS
     SET   AC_POINTS                   = AC_POINTS  + @won_points
   WHERE   AC_ACCOUNT_ID               = @account_id  
  
  --  
  -- Create CardMovement.ObtainedPoints
  --
  -- PointsAwarded   = 36,
  SET @points_balance_after     = @points_balance_before + @won_points
  
  EXECUTE dbo.InsertMovement @PlaySessionId, @account_id, @terminal_id, 36, @points_balance_before, 0, @won_points, @points_balance_after

EXIT_PROCEDURE:

  --
  -- ACC 25/11/2010 Accumulate played amount into site jackpot
  --
  EXECUTE dbo.SiteJackpot_AccumulatePlayed @played_amount, @redeemable_played

  SET @status_code  = 0
  SET @status_text  = 'PT_PlaySessionFinished: Successful points accumulated.' 
                    + ' *** '
                    + ' Points.PerRedeemableSpent  = ' + CAST (@points_spent_redeemable AS nvarchar)  + ' (' + CAST (@spent_redeemable AS nvarchar)  + ' credits)'
                    + ' Points.PerRedeemablePlayed = ' + CAST (@points_played_redeemable AS nvarchar) + ' (' + CAST (@redeemable_played AS nvarchar) + ' credits)'
                    + ' Points.PerTotalPlayed      = ' + CAST (@points_total_played AS nvarchar)      + ' (' + CAST (@played_amount AS nvarchar)     + ' credits)'
                    + ' Points.TotalAwarded        = ' + CAST (@won_points AS nvarchar) 
                    + ' *** '

ERROR_PROCEDURE:
--
-- ACC 18-OCT-2010 DO NOT SELECT (3GS not works with this select)
--
--  SELECT @status_text AS StatusText

END -- PT_PlaySessionFinished
GO

/****** TRIGGERS ******/


/****** RECORDS ******/
/* Mailing */
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Mailing' AND GP_SUBJECT_KEY ='Enabled')
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('Mailing'
           ,'Enabled'
           ,'0');
ELSE
SELECT '***** Record Mailing_Enabled already exists *****';

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Mailing' AND GP_SUBJECT_KEY ='SMTP.Enabled')
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('Mailing'
           ,'SMTP.Enabled'
           ,'0');
ELSE
SELECT '***** Record Mailing_SMTP.Enabled already exists *****';

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Mailing' AND GP_SUBJECT_KEY ='SMTP.Connection.ServerAddress')
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('Mailing'
           ,'SMTP.Connection.ServerAddress'
           ,'');
ELSE
SELECT '***** Record Mailing_SMTP.Connection.ServerAddress already exists *****';

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Mailing' AND GP_SUBJECT_KEY ='SMTP.Connection.ServerPort')
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('Mailing'
           ,'SMTP.Connection.ServerPort'
           ,'');
ELSE
SELECT '***** Record Mailing_SMTP.Connection.ServerPort already exists *****';

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Mailing' AND GP_SUBJECT_KEY ='SMTP.Connection.Secured')
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('Mailing'
           ,'SMTP.Connection.Secured'
           ,'');
ELSE
SELECT '***** Record Mailing_SMTP.Connection.Secured already exists *****';

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Mailing' AND GP_SUBJECT_KEY ='SMTP.Credentials.UserName')
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('Mailing'
           ,'SMTP.Credentials.UserName'
           ,'');
ELSE
SELECT '***** Record Mailing_SMTP.Credentials.UserName already exists *****';

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Mailing' AND GP_SUBJECT_KEY ='SMTP.Credentials.Password')
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('Mailing'
           ,'SMTP.Credentials.Password'
           ,'');
ELSE
SELECT '***** Record Mailing_SMTP.Credentials.Password already exists *****';

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Mailing' AND GP_SUBJECT_KEY ='SMTP.Credentials.Domain')
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('Mailing'
           ,'SMTP.Credentials.Domain'
           ,'');
ELSE
SELECT '***** Record Mailing_SMTP.Credentials.Domain already exists *****';

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Mailing' AND GP_SUBJECT_KEY ='SMTP.Credentials.EMailAddress')
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('Mailing'
           ,'SMTP.Credentials.EMailAddress'
           ,'');
ELSE
SELECT '***** Record Mailing_SMTP.Credentials.EMailAddress already exists *****';

/* Cashier */
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY ='AllowedAnonymous')
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('Cashier'
           ,'AllowedAnonymous'
           ,'1');
ELSE
SELECT '***** Record Cashier_AllowedAnonymous already exists *****';

/* Cashier.DrawTicket */
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.DrawTicket' AND GP_SUBJECT_KEY ='AutomaticPrintAfterCashIn')
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('Cashier.DrawTicket'
           ,'AutomaticPrintAfterCashIn'
           ,'0');
ELSE
SELECT '***** Record Cashier.DrawTicket_AutomaticPrintAfterCashIn already exists *****';

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.DrawTicket' AND GP_SUBJECT_KEY ='AutomaticPrintAfterCashOut')
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('Cashier.DrawTicket'
           ,'AutomaticPrintAfterCashOut'
           ,'0');
ELSE
SELECT '***** Record Cashier.DrawTicket_AutomaticPrintAfterCashOut already exists *****';


/* Special update to avoid strange sw behavior */
UPDATE [dbo].[ACCOUNTS]
   SET AC_NR_WON_LOCK = 0 
 WHERE AC_NR_WON_LOCK IS NULL;