/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 13;

SET @New_ReleaseId = 14;
SET @New_ScriptName = N'UpdateTo_18.014.sql';
SET @New_Description = N'Promotions; player tracking; taxes. Fixes(4).';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/


/****** INDEXES ******/


/****** STORED PROCEDURES ******/


/****** TRIGGERS ******/
/****** Object:  Trigger [dbo].[PlayerTrackingTrigger_Insert]    Script Date: 10/25/2010 10:26:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PlayerTrackingTrigger_Insert]') AND type in (N'TR'))
DROP TRIGGER [dbo].[PlayerTrackingTrigger_Insert]
GO

CREATE TRIGGER [dbo].[PlayerTrackingTrigger_Insert]
ON [dbo].[play_sessions]
AFTER INSERT
NOT FOR REPLICATION
AS
DECLARE @new_status     int
DECLARE @play_session   bigint
BEGIN

  IF UPDATE (PS_STATUS)
  BEGIN
    SET @new_status   = (SELECT PS_STATUS FROM INSERTED)
    SET @play_session = (SELECT PS_PLAY_SESSION_ID FROM INSERTED)

    IF @new_status = 1 EXECUTE dbo.PT_PlaySessionFinished @play_session
  END
  
END -- PlayerTrackingTrigger_Insert
GO


/****** RECORDS ******/




