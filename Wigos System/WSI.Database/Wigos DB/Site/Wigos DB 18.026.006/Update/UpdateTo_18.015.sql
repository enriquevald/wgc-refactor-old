/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 14;

SET @New_ReleaseId = 15;
SET @New_ScriptName = N'UpdateTo_18.015.sql';
SET @New_Description = N'Gift counters; account balance limit; account lock reason.';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/

/* Gifts */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[gifts]') and name = 'gi_current_stock')
ALTER TABLE [dbo].[gifts] 
	ADD [gi_current_stock] [int] NOT NULL CONSTRAINT [DF_gi_current_stock] DEFAULT (0);
ELSE
SELECT '***** Field gifts.gi_current_stock already exists *****';

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[gifts]') and name = 'gi_request_counter')
ALTER TABLE [dbo].[gifts] 
	ADD [gi_request_counter] [int] NOT NULL CONSTRAINT [DF_gi_request_counter] DEFAULT (0);
ELSE
SELECT '***** Field gifts.gi_request_counter already exists *****';

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[gifts]') and name = 'gi_delivery_counter')
ALTER TABLE [dbo].[gifts] 
	ADD [gi_delivery_counter] [int] NOT NULL CONSTRAINT [DF_gi_delivery_counter] DEFAULT (0);
ELSE
SELECT '***** Field gifts.gi_delivery_counter already exists *****';

/* Gift_Instances */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[gift_instances]') and name = 'gin_request_status')
ALTER TABLE [dbo].[gift_instances] 
	ADD [gin_request_status] [int] NOT NULL CONSTRAINT [DF_gin_request_status] DEFAULT (0);
ELSE
SELECT '***** Field gift_instances.gin_request_status already exists *****';

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[gift_instances]') and name = 'gin_num_items')
ALTER TABLE [dbo].[gift_instances] 
	ADD [gin_num_items] [int] NOT NULL CONSTRAINT [DF_gin_num_items] DEFAULT (1);
ELSE
SELECT '***** Field gift_instances.gin_num_items already exists *****';

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[gift_instances]') and name = 'gin_data_01')
ALTER TABLE [dbo].[gift_instances] 
	ADD [gin_data_01] [bigint] NULL;
ELSE
SELECT '***** Field gift_instances.gin_data_01 already exists *****';

/* Accounts */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[accounts]') and name = 'ac_block_reason')
ALTER TABLE [dbo].[accounts] 
	ADD [ac_block_reason] [int] NOT NULL CONSTRAINT [DF_ac_block_reason] DEFAULT (0);
ELSE
SELECT '***** Field accounts.ac_block_reason already exists *****';

/* Draws */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[draws]') and name = 'dr_initial_number')
ALTER TABLE [dbo].[draws] 
	ADD [dr_initial_number] [bigint] NOT NULL CONSTRAINT [DF_dr_initial_number] DEFAULT (1);
ELSE
SELECT '***** Field draws.dr_initial_number already exists *****';

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[draws]') and name = 'dr_max_number')
ALTER TABLE [dbo].[draws] 
	ADD [dr_max_number] [bigint] NOT NULL CONSTRAINT [DF_dr_max_number] DEFAULT (999999999);
ELSE
SELECT '***** Field draws.dr_max_number already exists *****';

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[draws]') and name = 'dr_credit_type')
ALTER TABLE [dbo].[draws] 
	ADD [dr_credit_type] [int] NOT NULL CONSTRAINT [DF_dr_credit_type] DEFAULT (0);
ELSE
SELECT '***** Field draws.dr_credit_type already exists *****';

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[draws]') and name = 'dr_header')
ALTER TABLE [dbo].[draws] 
	ADD [dr_header] [nvarchar](260) NULL;
ELSE
SELECT '***** Field draws.dr_header already exists *****';

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[draws]') and name = 'dr_footer')
ALTER TABLE [dbo].[draws] 
	ADD [dr_footer] [nvarchar](512) NULL;
ELSE
SELECT '***** Field draws.dr_footer already exists *****';

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[draws]') and name = 'dr_detail1')
ALTER TABLE [dbo].[draws] 
	ADD [dr_detail1] [nvarchar](260) NULL;
ELSE
SELECT '***** Field draws.dr_detail1 already exists *****';

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[draws]') and name = 'dr_detail2')
ALTER TABLE [dbo].[draws] 
	ADD [dr_detail2] [nvarchar](260) NULL;
ELSE
SELECT '***** Field draws.dr_detail2 already exists *****';

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[draws]') and name = 'dr_detail3')
ALTER TABLE [dbo].[draws] 
	ADD [dr_detail3] [nvarchar](260) NULL;
ELSE
SELECT '***** Field draws.dr_detail3 already exists *****';

/* Account_Operations */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[account_operations]') and name = 'ao_operation_data')
ALTER TABLE [dbo].[account_operations] 
	ADD [ao_operation_data] [bigint] NULL;
ELSE
SELECT '***** Field account_operations.ao_operation_data already exists *****';

/* This statement intentionally left here */
GO

ALTER TABLE [dbo].[draws] 
	DROP CONSTRAINT [DF_draws_dr_last_number];

ALTER TABLE [dbo].[draws] 
	ADD CONSTRAINT [DF_draws_dr_last_number] DEFAULT (-1) FOR [dr_last_number];

UPDATE [dbo].[draws] 
	SET dr_last_number = -1
  WHERE dr_last_number = 0;

UPDATE [dbo].[draws] 
	SET dr_detail1 = dr_name;

/****** INDEXES ******/

/****** Object:  Index [IX_gin_expiration]    Script Date: 10/22/2010 11:32:19 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[gift_instances]') AND name = N'IX_gin_request_status')
CREATE NONCLUSTERED INDEX [IX_gin_request_status] ON [dbo].[gift_instances] 
(
	[gin_request_status] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY];
ELSE
SELECT '***** Index gift_instances.IX_gin_request_status already exists *****';


/****** STORED PROCEDURES ******/
--------------------------------------------------------------------------------
-- PURPOSE : Read PlaySession & Accounts & PlayerTracking data
-- 
--  PARAMS :
--      - INPUT :
--          @PlaySessionId      bigint
--
--      - OUTPUT :
--          @TerminalId                 int            
--          @AccountId                  bigint         
--          @InitialBalance             money          
--          @PlayedAmount               money          
--          @WonAmount                  money          
--          @FinalBalance               money        
--          @InitialNonRedeemable       money        
--          @AccountBalance             money          
--          @HolderName                 nvarchar (50)
--          @HolderLevel                int          
--          @MaxAllowedAccountBalance   numeric (20,6)
--          @StatusCode                 int            
--          @StatusText                 nvarchar (254) 
--
-- RETURNS :
--
--   NOTES :

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PT_ReadData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PT_ReadData]
GO
CREATE PROCEDURE [dbo].[PT_ReadData]
  @PlaySessionId              bigint
, @TerminalId                 int             OUTPUT
, @AccountId                  bigint          OUTPUT
, @InitialBalance             money           OUTPUT
, @PlayedAmount               money           OUTPUT
, @WonAmount                  money           OUTPUT
, @FinalBalance               money           OUTPUT
, @InitialNonRedeemable       money           OUTPUT
, @InitialCashIn              money           OUTPUT
, @PrizeLock                  money           OUTPUT
, @AccountBalance             money           OUTPUT
, @PointsBalance              money           OUTPUT
, @HolderName                 nvarchar (50)   OUTPUT
, @HolderLevel                int             OUTPUT
, @MaxAllowedAccountBalance   numeric (20,6)  OUTPUT
, @StatusCode                 int             OUTPUT
, @StatusText                 nvarchar (254)  OUTPUT    
AS
BEGIN
  DECLARE @rc int
	
  SET @StatusCode = 1
	SET @StatusText = 'PT_ReadData: Reading PlaySession...'
	
	--
	-- Select data from PLAY_SESSIONS & ACCOUNTS table
	--
  SELECT @TerminalId            = PS_TERMINAL_ID
       , @AccountId             = PS_ACCOUNT_ID
       , @InitialBalance        = PS_INITIAL_BALANCE + PS_CASH_IN
       , @PlayedAmount          = PS_PLAYED_AMOUNT
       , @WonAmount             = PS_WON_AMOUNT
       , @FinalBalance          = PS_FINAL_BALANCE
       , @InitialNonRedeemable  = AC_INITIAL_NOT_REDEEMABLE
       , @InitialCashIn         = AC_INITIAL_CASH_IN
       , @PrizeLock             = AC_NR_WON_LOCK
       , @AccountBalance        = AC_BALANCE
       , @PointsBalance         = AC_POINTS 
       , @HolderName            = ISNULL (AC_HOLDER_NAME, '')
       , @HolderLevel           = ISNULL (AC_HOLDER_LEVEL, 0)
    FROM PLAY_SESSIONS, ACCOUNTS
   WHERE PS_PLAY_SESSION_ID = @PlaySessionId
     AND PS_ACCOUNT_ID      = AC_ACCOUNT_ID

  SET @rc = @@ROWCOUNT
  IF ( @rc <> 1 )
  BEGIN
    SET @StatusCode  = 1
    SET @StatusText  = 'PT_ReadData: Invalid PlaySessionId.'
    GOTO ERROR_PROCEDURE
  END

  IF ( @HolderName = '' )
  BEGIN
    SET @HolderLevel = 0  
  END
  
  --- 
  --- Read Max Allowed Account Balance
  ---
  SELECT @MaxAllowedAccountBalance = GP_KEY_VALUE
    FROM GENERAL_PARAMS
   WHERE GP_GROUP_KEY = 'Cashier'
     AND GP_SUBJECT_KEY = 'MaxAllowedAccountBalance'

  SET @rc = @@ROWCOUNT
  IF ( @rc <> 1 )
  BEGIN
    SET @StatusCode = 1
    SET @StatusText = 'PT_ReadData: Invalid Cashier.MaxAllowedAccountBalance general parameter.'
    GOTO ERROR_PROCEDURE
  END
    
  --
  -- Read data successful
  --
  SET @StatusCode = 0
  SET @StatusText = 'PT_ReadData: PlaySession read.'

ERROR_PROCEDURE:

END -- PT_ReadData
GO

--------------------------------------------------------------------------------
-- PURPOSE : Accumulate points for player when play session is finished
-- 
--  PARAMS :
--      - INPUT :
--          @PlaySessionId      bigint
--
--      - OUTPUT :
--
-- RETURNS :
--
--   NOTES :

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PT_PlaySessionFinished]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PT_PlaySessionFinished]
GO
CREATE PROCEDURE [dbo].[PT_PlaySessionFinished]
  @PlaySessionId      bigint
AS
BEGIN

	-- Only for test
  DECLARE @status_code                       int
  DECLARE @status_text                       nvarchar (500)
  -- Only for test
  
  DECLARE @terminal_id                       int
  DECLARE @account_id                        bigint
  DECLARE @total_cash_in                     money
  DECLARE @played_amount                     money
  DECLARE @won_amount                        money
  DECLARE @total_cash_out                    money
  DECLARE @initial_non_redeemable            money
  DECLARE @initial_cash_in                   money
  DECLARE @prize_lock                        money
  DECLARE @account_balance                   money
  DECLARE @holder_name                       nvarchar (50)
  DECLARE @holder_level                      int
  
  DECLARE @non_redeemable_cash_in            money
  DECLARE @redeemable_cash_in                money
  DECLARE @non_redeemable_cash_out           money
  DECLARE @redeemable_cash_out               money
  DECLARE @non_redeemable_played             money
  DECLARE @redeemable_played                 money
  DECLARE @non_redeemable_won                money
  DECLARE @redeemable_won                    money
    
  DECLARE @spent_no_redeemable               money
  DECLARE @spent_redeemable                  money

  DECLARE @profit_no_redeemable              money
  DECLARE @profit_redeemable                 money
  
  DECLARE @total_sum_redimible               numeric (20,6)
  DECLARE @total_sum                         numeric (20,6)
  DECLARE @percent_redeemable                numeric (8,6)

  DECLARE @rest_played_and_no_spent          money
  DECLARE @rest_won_and_no_profit            money
  
  DECLARE @total_played_to_1_point           numeric (20,6)
  DECLARE @redeemable_played_to_1_point      numeric (20,6)
  DECLARE @redeemable_spent_to_1_point       numeric (20,6)

  DECLARE @points_balance_before             money
  DECLARE @points_balance_after              money
  DECLARE @points_total_played               money
  DECLARE @points_played_redeemable          money
  DECLARE @points_spent_redeemable           money
  DECLARE @won_points                        money
  
  DECLARE @max_allowed_acc_balance           numeric (20,6)

  -- SET NOCOUNT ON added to prevent extra result sets from
  -- interfering with SELECT statements.
  SET NOCOUNT ON;
	
  SET @status_code  = 0
  SET @status_text  = 'PT_PlaySessionFinished: Init'

  SET @points_total_played      = 0
  SET @points_played_redeemable = 0
  SET @points_spent_redeemable  = 0
  SET @won_points               = 0  
  SET @points_balance_before    = 0
  SET @points_balance_after     = 0


  IF ( @PlaySessionId = 0 ) 
  BEGIN
    SET @status_text = 'PT_PlaySessionFinished: Invalid PlaySessionId'
    GOTO ERROR_PROCEDURE
  END

  --
  -- Read Session & Account & PlayerTracking Data
  --
  EXECUTE dbo.PT_ReadData @PlaySessionId, @terminal_id OUTPUT, @account_id OUTPUT,
                          @total_cash_in OUTPUT, @played_amount OUTPUT, @won_amount OUTPUT, @total_cash_out OUTPUT, 
                          @initial_non_redeemable OUTPUT, @initial_cash_in OUTPUT, @prize_lock OUTPUT, @account_balance OUTPUT, 
                          @points_balance_before OUTPUT, @holder_name OUTPUT, @holder_level OUTPUT, @max_allowed_acc_balance OUTPUT,
                          @status_code OUTPUT, @status_text OUTPUT

  IF ( @status_code <> 0 )
    GOTO ERROR_PROCEDURE
    
  --
  -- Compute PlayedCredits, PlayedRedeemableCredits, SpentRedeemableCredits
  --
  EXECUTE dbo.PT_BalancePartsWhenPlaying @total_cash_in, @initial_cash_in, @initial_non_redeemable, @prize_lock, 
                                         @non_redeemable_cash_in OUTPUT, @redeemable_cash_in OUTPUT

  EXECUTE dbo.PT_BalancePartsWhenPlaying @total_cash_out, @initial_cash_in, @initial_non_redeemable, @prize_lock, 
                                         @non_redeemable_cash_out OUTPUT, @redeemable_cash_out OUTPUT

  SET @spent_no_redeemable      = dbo.Maximum_Money (0, @non_redeemable_cash_in - @non_redeemable_cash_out)
  SET @spent_redeemable         = dbo.Maximum_Money (0, @redeemable_cash_in - @redeemable_cash_out)

  SET @profit_no_redeemable     = dbo.Maximum_Money (0, @non_redeemable_cash_out - @non_redeemable_cash_in)
  SET @profit_redeemable        = dbo.Maximum_Money (0, @redeemable_cash_out - @redeemable_cash_in)

  SET @total_sum_redimible      = @redeemable_cash_in + @redeemable_cash_out 
  SET @total_sum                = @non_redeemable_cash_in + @non_redeemable_cash_out + @redeemable_cash_in + @redeemable_cash_out
  
  IF @total_sum = 0 GOTO EXIT_PROCEDURE   
  SET @percent_redeemable       = @total_sum_redimible / @total_sum

  SET @rest_played_and_no_spent = @played_amount - (@spent_no_redeemable + @spent_redeemable)
  SET @redeemable_played        = ROUND(@spent_redeemable + (@rest_played_and_no_spent * @percent_redeemable), 4)
  SET @non_redeemable_played    = @played_amount - @redeemable_played

  SET @rest_won_and_no_profit   = @won_amount - (@profit_no_redeemable + @profit_redeemable)
  SET @redeemable_won           = ROUND(@profit_redeemable + (@rest_won_and_no_profit * @percent_redeemable), 4)
  SET @non_redeemable_won       = @won_amount - @redeemable_won

  --
  -- Update PlaySession table (Redeemable & NonRedeemable amounts)
  --
  UPDATE PLAY_SESSIONS
     SET PS_NON_REDEEMABLE_CASH_IN    = @non_redeemable_cash_in
       , PS_NON_REDEEMABLE_CASH_OUT   = @non_redeemable_cash_out
       , PS_NON_REDEEMABLE_PLAYED     = @non_redeemable_played
       , PS_NON_REDEEMABLE_WON        = @non_redeemable_won
       , PS_REDEEMABLE_CASH_IN        = @redeemable_cash_in
       , PS_REDEEMABLE_CASH_OUT       = @redeemable_cash_out
       , PS_REDEEMABLE_PLAYED         = @redeemable_played
       , PS_REDEEMABLE_WON            = @redeemable_won
   WHERE PS_PLAY_SESSION_ID = @PlaySessionId 

  --
  -- RCI & ACC & AJQ 30/09/2010: Reset the Cancellable Operation when PlayedAmount greater than 0.
  --
  UPDATE   ACCOUNTS
     SET   AC_CANCELLABLE_OPERATION_ID = CASE WHEN (@played_amount > 0) THEN NULL ELSE AC_CANCELLABLE_OPERATION_ID END
   WHERE   AC_ACCOUNT_ID               = @account_id  

  --
  -- ACC 22/10/2010: Check maximum allowed account balance
  --
  IF ( @max_allowed_acc_balance > 0 AND @total_cash_out > @max_allowed_acc_balance )
  BEGIN
    UPDATE ACCOUNTS
       SET AC_BLOCKED       = 1
         , AC_BLOCK_REASON  = 2  -- MaxBalance
     WHERE AC_ACCOUNT_ID    = @account_id  
  END

  --- Anonymous accounts have Level=0
  IF ( @holder_level = 0 )
    GOTO EXIT_PROCEDURE

  --
  -- Read PlayerTracking Point Conversion Factors according to the holder's level
  --
  EXECUTE dbo.PT_ReadPointFactors @holder_level, 
                                  @redeemable_spent_to_1_point  OUTPUT,
                                  @redeemable_played_to_1_point OUTPUT,
                                  @total_played_to_1_point      OUTPUT,
                                  @status_code                  OUTPUT, 
                                  @status_text                  OUTPUT

  IF ( @status_code <> 0 )
    GOTO ERROR_PROCEDURE

  --
  -- Compute Points (it is possible to accummulate points per each type of credits at the same time)
  --  
  IF @redeemable_spent_to_1_point  > 0 SET @points_spent_redeemable  = @spent_redeemable  / @redeemable_spent_to_1_point
  IF @redeemable_played_to_1_point > 0 SET @points_played_redeemable = @redeemable_played / @redeemable_played_to_1_point
  IF @total_played_to_1_point      > 0 SET @points_total_played      = @played_amount     / @total_played_to_1_point
  
  SET @won_points = ROUND(@points_total_played + @points_played_redeemable + @points_spent_redeemable, 4)

  --
  -- Accumulate points
  -- 
  UPDATE   ACCOUNTS
     SET   AC_POINTS                   = AC_POINTS  + @won_points
   WHERE   AC_ACCOUNT_ID               = @account_id  
  
  --  
  -- Create CardMovement.ObtainedPoints
  --
  -- PointsAwarded   = 36,
  SET @points_balance_after     = @points_balance_before + @won_points
  
  EXECUTE dbo.InsertMovement @PlaySessionId, @account_id, @terminal_id, 36, @points_balance_before, 0, @won_points, @points_balance_after

EXIT_PROCEDURE:
  SET @status_code  = 0
  SET @status_text  = 'PT_PlaySessionFinished: Successful points accumulated.' 
                    + ' *** '
                    + ' Points.PerRedeemableSpent  = ' + CAST (@points_spent_redeemable AS nvarchar)  + ' (' + CAST (@spent_redeemable AS nvarchar)  + ' credits)'
                    + ' Points.PerRedeemablePlayed = ' + CAST (@points_played_redeemable AS nvarchar) + ' (' + CAST (@redeemable_played AS nvarchar) + ' credits)'
                    + ' Points.PerTotalPlayed      = ' + CAST (@points_total_played AS nvarchar)      + ' (' + CAST (@played_amount AS nvarchar)     + ' credits)'
                    + ' Points.TotalAwarded        = ' + CAST (@won_points AS nvarchar) 
                    + ' *** '

ERROR_PROCEDURE:
--
-- ACC 18-OCT-2010 DO NOT SELECT (3GS not works with this select)
--
--  SELECT @status_text AS StatusText

END -- PT_PlaySessionFinished
GO


/****** TRIGGERS ******/


/****** RECORDS ******/

/* Gift_Instances */
UPDATE gift_instances
   SET gin_request_status = (CASE WHEN gin_delivered IS NOT NULL THEN 1 ELSE 0 END);
   
/* Cashier */
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY ='MaxAllowedAccountBalance')
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('Cashier'
           ,'MaxAllowedAccountBalance'
           ,'0');
ELSE
SELECT '***** Record Cashier_MaxAllowedAccountBalance already exists *****';


