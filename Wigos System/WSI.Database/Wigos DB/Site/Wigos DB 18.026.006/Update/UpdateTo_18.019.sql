/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_000]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 18;

SET @New_ReleaseId = 19;
SET @New_ScriptName = N'UpdateTo_18.019.sql';
SET @New_Description = N'Misc. Promotions & Indexes; tax pct fields on Cashier_Sessions; new Site Jackpots.';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/

/* Accounts */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[accounts]') and name = 'ac_holder_level_expiration')
ALTER TABLE [dbo].[accounts] 
	ADD [ac_holder_level_expiration] [datetime] NULL;
ELSE
SELECT '***** Field accounts.ac_holder_level_expiration already exists *****';

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[accounts]') and name = 'ac_holder_level_entered')
ALTER TABLE [dbo].[accounts] 
	ADD [ac_holder_level_entered] [datetime] NULL CONSTRAINT [DF_accounts_ac_holder_level_entered] DEFAULT (GetDate());
ELSE
SELECT '***** Field accounts.ac_holder_level_entered already exists *****';

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[accounts]') and name = 'ac_holder_level_notify')
ALTER TABLE [dbo].[accounts] 
	ADD [ac_holder_level_notify] [int] NULL CONSTRAINT [DF_accounts_ac_holder_level_notify] DEFAULT (0);
ELSE
SELECT '***** Field accounts.ac_holder_level_notify already exists *****';

/* Promotions */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[promotions]') and name = 'pm_level_filter')
ALTER TABLE [dbo].[promotions] 
	ADD [pm_level_filter] [int] NOT NULL CONSTRAINT [DF_promotions_pm_level_filter] DEFAULT (0);
ELSE
SELECT '***** Field promotions.pm_level_filter already exists *****';

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[promotions]') and name = 'pm_permission')
ALTER TABLE [dbo].[promotions] 
	ADD [pm_permission] [int] NOT NULL CONSTRAINT [DF_promotions_pm_permission] DEFAULT (0);
ELSE
SELECT '***** Field promotions.pm_permission already exists *****';

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[promotions]') and name = 'pm_freq_filter_last_days')
ALTER TABLE [dbo].[promotions] 
	ADD [pm_freq_filter_last_days] [int] NULL CONSTRAINT [DF_promotions_pm_freq_last_days] DEFAULT ((0));
ELSE
SELECT '***** Field promotions.pm_freq_filter_last_days already exists *****';

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[promotions]') and name = 'pm_freq_filter_min_days')
ALTER TABLE [dbo].[promotions] 
	ADD [pm_freq_filter_min_days] [int] NULL CONSTRAINT [DF_promotions_pm_freq_min_days]  DEFAULT ((0));
ELSE
SELECT '***** Field promotions.pm_freq_filter_min_days already exists *****';

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[promotions]') and name = 'pm_freq_filter_min_cash_in')
ALTER TABLE [dbo].[promotions] 
	ADD [pm_freq_filter_min_cash_in] [money] NULL CONSTRAINT [DF_promotions_pm_freq_min_cash_in]  DEFAULT ((0));
ELSE
SELECT '***** Field promotions.pm_freq_filter_min_cash_in already exists *****';

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[promotions]') and name = 'pm_min_spent')
ALTER TABLE [dbo].[promotions] 
	ADD [pm_min_spent] [money] NOT NULL CONSTRAINT [DF_promotions_pm_min_spent]  DEFAULT ((0));
ELSE
SELECT '***** Field promotions.pm_min_spent already exists *****';
	
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[promotions]') and name = 'pm_min_spent_reward')
ALTER TABLE [dbo].[promotions] 
	ADD [pm_min_spent_reward] [money] NOT NULL CONSTRAINT [DF_promotions_pm_min_spent_reward]  DEFAULT ((0));
ELSE
SELECT '***** Field promotions.pm_min_spent_reward already exists *****';

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[promotions]') and name = 'pm_spent')
ALTER TABLE [dbo].[promotions] 
	ADD [pm_spent] [money] NOT NULL CONSTRAINT [DF_promotions_pm_spent]  DEFAULT ((0));
ELSE
SELECT '***** Field promotions.pm_spent already exists *****';

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[promotions]') and name = 'pm_spent_reward')
ALTER TABLE [dbo].[promotions] 
	ADD [pm_spent_reward] [money] NOT NULL CONSTRAINT [DF_promotions_pm_spent_reward]  DEFAULT ((0));
ELSE
SELECT '***** Field promotions.pm_spent_reward already exists *****';

/* WC2_sessions */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[wc2_sessions]') and name = 'w2s_server_name')
ALTER TABLE [dbo].[wc2_sessions] 
	ADD [w2s_server_name] [nvarchar](50) NULL;
ELSE
SELECT '***** Field wc2_sessions.w2s_server_name already exists *****';

/* WCP_sessions */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[wcp_sessions]') and name = 'ws_server_name')
ALTER TABLE [dbo].[wcp_sessions] 
	ADD [ws_server_name] [nvarchar](50) NULL;
ELSE
SELECT '***** Field wcp_sessions.ws_server_name already exists *****';

/* Cashier_Sessions */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[cashier_sessions]') and name = 'cs_tax_a_pct')
ALTER TABLE [dbo].[cashier_sessions] 
	ADD [cs_tax_a_pct] [decimal](5, 2) NULL;
ELSE
SELECT '***** Field cashier_sessions.cs_tax_a_pct already exists *****';

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[cashier_sessions]') and name = 'cs_tax_b_pct')
ALTER TABLE [dbo].[cashier_sessions] 
	ADD [cs_tax_b_pct] [decimal](5, 2) NULL;
ELSE
SELECT '***** Field cashier_sessions.cs_tax_b_pct already exists *****';

/* Handpays */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[handpays]') and name = 'hp_site_jackpot_index')
ALTER TABLE [dbo].[handpays] 
	ADD [hp_site_jackpot_index] [int] NULL;
ELSE
SELECT '***** Field handpays.hp_site_jackpot_index already exists *****';

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[handpays]') and name = 'hp_site_jackpot_name')
ALTER TABLE [dbo].[handpays] 
	ADD [hp_site_jackpot_name] [nvarchar](50) NULL;
ELSE
SELECT '***** Field handpays.hp_site_jackpot_name already exists *****';

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[handpays]') and name = 'hp_site_jackpot_awarded_on_terminal_id')
ALTER TABLE [dbo].[handpays] 
	ADD [hp_site_jackpot_awarded_on_terminal_id] [int] NULL;
ELSE
SELECT '***** Field handpays.hp_site_jackpot_awarded_on_terminal_id already exists *****';

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[handpays]') and name = 'hp_site_jackpot_awarded_to_account_id')
ALTER TABLE [dbo].[handpays] 
	ADD [hp_site_jackpot_awarded_to_account_id] [bigint] NULL;
ELSE
SELECT '***** Field handpays.hp_site_jackpot_awarded_to_account_id already exists *****';

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[handpays]') and name = 'hp_status')
ALTER TABLE [dbo].[handpays] 
	ADD [hp_status] [int] NULL CONSTRAINT [DF_handpays_hp_status] DEFAULT ((0));
ELSE
SELECT '***** Field handpays.hp_status already exists *****';

/* Terminals */
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[terminals]') and name = 'te_status')
ALTER TABLE [dbo].[terminals] 
	ADD [te_status] [int] NOT NULL CONSTRAINT [DF_te_status] DEFAULT (0);
ELSE
SELECT '***** Field terminals.te_status already exists *****';


/****** Object:  Table [dbo].[site_jackpot_instances]    Script Date: 12/09/2010 15:27:16 ******/
IF NOT EXISTS (SELECT * FROM SYS.OBJECTS WHERE TYPE = 'U' AND NAME = 'site_jackpot_instances')
CREATE TABLE [dbo].[site_jackpot_instances](
	[sji_index] [int] NOT NULL,
	[sji_name] [nvarchar](20) NOT NULL,
	[sji_contribution_pct] [numeric](5, 2) NOT NULL,
	[sji_minimum] [money] NOT NULL,
	[sji_maximum] [money] NOT NULL,
	[sji_average] [money] NOT NULL,
	[sji_accumulated] [numeric](20, 8) NOT NULL CONSTRAINT [DF_site_jackpot_instances_sji_accumulated] DEFAULT ((0)),
	[sji_num_pending] [int] NOT NULL CONSTRAINT [DF_site_jackpot_instances_sji_awarded] DEFAULT ((0)),
 CONSTRAINT [PK_site_jackpot_instances] PRIMARY KEY CLUSTERED 
(
	[sji_index] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY];
ELSE
SELECT '***** Table site_jackpot_instances already exists *****';

/****** Object:  Table [dbo].[site_jackpot_parameters]    Script Date: 12/09/2010 15:30:53 ******/
IF NOT EXISTS (SELECT * FROM SYS.OBJECTS WHERE TYPE = 'U' AND NAME = 'site_jackpot_parameters')
CREATE TABLE [dbo].[site_jackpot_parameters](
	[sjp_enabled] [bit] NOT NULL,
	[sjp_contribution_pct] [numeric](7, 4) NOT NULL CONSTRAINT [DF_site_jackpot_parameters_sjp_contribution_pct]  DEFAULT ((0)),
	[sjp_awarding_days] [int] NOT NULL CONSTRAINT [DF_site_jackpot_parameters_sjp_working_days]  DEFAULT ((127)),
	[sjp_awarding_start] [int] NOT NULL CONSTRAINT [DF_site_jackpot_parameters_sjp_working_start]  DEFAULT ((0)),
	[sjp_awarding_end] [int] NOT NULL CONSTRAINT [DF_site_jackpot_parameters_sjp_working_end]  DEFAULT ((86399)),
	[sjp_awarding_min_occupation_pct] [numeric](5, 2) NOT NULL CONSTRAINT [DF_site_jackpot_parameters_sjp_min_occupation_pct]  DEFAULT ((0)),
	[sjp_awarding_exclude_promotions] [bit] NOT NULL CONSTRAINT [DF_site_jackpot_parameters_sjp_awarding_exclude_promotions]  DEFAULT ((1)),
	[sjp_awarding_exclude_anonymous] [bit] NOT NULL CONSTRAINT [DF_site_jackpot_parameters_sjp_awarding_exclude_anonymous]  DEFAULT ((1)),
	[sjp_animation_interval] [int] NOT NULL CONSTRAINT [DF_site_jackpot_parameters_sjp_animation_interval]  DEFAULT ((15)),
	[sjp_recent_interval] [int] NOT NULL CONSTRAINT [DF_site_jackpot_parameters_sjp_recent_interval]  DEFAULT ((3600)),
	[sjp_promo_message1] [nvarchar](max) NULL,
	[sjp_promo_message2] [nvarchar](max) NULL,
	[sjp_promo_message3] [nvarchar](max) NULL,
	[sjp_to_compensate] [numeric](20, 8) NOT NULL CONSTRAINT [DF_site_jackpot_parameters_sjp_to_compensate]  DEFAULT ((0)),
	[sjp_only_redeemable] [bit] NOT NULL,
	[sjp_average_interval_hours] [int] NOT NULL CONSTRAINT [DF_site_jackpot_parameters_sjp_average_interval_hours]  DEFAULT ((0)),
	[sjp_played] [numeric](20, 8) NOT NULL CONSTRAINT [DF_site_jackpot_parameters_sjp_played_total]  DEFAULT ((0))
) ON [PRIMARY];
ELSE
SELECT '***** Table site_jackpot_parameters already exists *****';


/****** INDEXES ******/

/****** Objeto:  Index [IX_type_account_date]    Fecha de la secuencia de comandos: 11/03/2010 18:38:10 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[account_movements]') AND name = N'IX_type_account_date')
CREATE NONCLUSTERED INDEX [IX_type_account_date] ON [dbo].[account_movements] 
(
                [am_type] ASC,
                [am_account_id] ASC,
                [am_datetime] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY];
ELSE
SELECT '***** Index account_movements.IX_type_account_date already exists *****';

/****** Objeto:  Index [IX_ps_finished]    Fecha de la secuencia de comandos: 11/15/2010 09:57:57 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[play_sessions]') AND name = N'IX_ps_finished')
CREATE NONCLUSTERED INDEX [IX_ps_finished] ON [dbo].[play_sessions] 
(
                [ps_finished] ASC,
                [ps_terminal_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY];
ELSE
SELECT '***** Index play_sessions.IX_type_account_date already exists *****';


/****** STORED PROCEDURES ******/
GO
ALTER PROCEDURE [dbo].[PT_ReadPointFactors]
  @HolderLevel                          int
, @RedeemableSpentTo1Point              numeric (20,6)  OUTPUT
, @RedeemablePlayedTo1Point             numeric (20,6)  OUTPUT
, @TotalPlayedTo1Point                  numeric (20,6)  OUTPUT
, @StatusCode                           int             OUTPUT
, @StatusText                           nvarchar (254)  OUTPUT    
AS
BEGIN
  DECLARE @rc           int
  DECLARE @level_name   nvarchar (50)

  SET @StatusCode = 1
  SET @StatusText = 'PT_ReadPointFactors: Reading Point Calculation Factors...'

  SET @RedeemableSpentTo1Point  = 0.00
  SET @RedeemablePlayedTo1Point = 0.00       
  SET @TotalPlayedTo1Point      = 0.00

	--
	-- Select data from GENERAL_PARAMS table: 
	--      * PlayerTracking.LevelXX.RedeemablePlayedTo1Point
	--      * PlayerTracking.LevelXX.TotalPlayedTo1Point
	--      * PlayerTracking.LevelXX.RedeemableSpentTo1Point
	--

  IF ( @HolderLevel = 1 )
    SET @level_name = 'Level01'
  ELSE IF ( @HolderLevel = 2 )
    SET @level_name = 'Level02'
  ELSE IF ( @HolderLevel = 3 )
    SET @level_name = 'Level03'
  ELSE IF ( @HolderLevel = 4 )
    SET @level_name = 'Level04'
  ELSE
    BEGIN
      --- 
      --- Wong Holder's Level 
      ---
      SET @StatusCode = 1
      SET @StatusText = 'PT_ReadPointFactors: Invalid Holder Level.'

      GOTO ERROR_PROCEDURE
    END  

  --- 
  --- Read level's RedeemablePlayedTo1Point
  ---
  SELECT @RedeemablePlayedTo1Point = GP_KEY_VALUE
    FROM GENERAL_PARAMS
   WHERE GP_GROUP_KEY = 'PlayerTracking'
     AND GP_SUBJECT_KEY = @level_name + '.RedeemablePlayedTo1Point'

  SET @rc = @@ROWCOUNT
  IF ( @rc <> 1 )
  BEGIN
    SET @StatusCode = 1
    SET @StatusText = 'PT_ReadPointFactors: Invalid Player Tracking setting Holder Level: PlayerTracking.' + @level_name + '.RedeemablePlayedTo1Point'
    GOTO ERROR_PROCEDURE
  END

  --- 
  --- Read level's TotalPlayedTo1Point
  ---
  SELECT @TotalPlayedTo1Point = GP_KEY_VALUE
    FROM GENERAL_PARAMS
   WHERE GP_GROUP_KEY = 'PlayerTracking'
     AND GP_SUBJECT_KEY = @level_name + '.TotalPlayedTo1Point'  

  SET @rc = @@ROWCOUNT
  IF ( @rc <> 1 )
  BEGIN
    SET @StatusCode = 1
    SET @StatusText = 'PT_ReadPointFactors: Invalid Player Tracking setting Holder Level: PlayerTracking.' + @level_name + '.TotalPlayedTo1Point'
    GOTO ERROR_PROCEDURE
  END

  --- 
  --- Read level's RedeemableSpentTo1Point
  ---
  SELECT @RedeemableSpentTo1Point = GP_KEY_VALUE
    FROM GENERAL_PARAMS
   WHERE GP_GROUP_KEY = 'PlayerTracking'
     AND GP_SUBJECT_KEY = @level_name + '.RedeemableSpentTo1Point'  

  SET @rc = @@ROWCOUNT
  IF ( @rc <> 1 )
  BEGIN
    SET @StatusCode = 1
    SET @StatusText = 'PT_ReadPointFactors: Invalid Player Tracking setting Holder Level: PlayerTracking.' + @level_name + '.RedeemableSpentTo1Point'
    GOTO ERROR_PROCEDURE
  END

  --
  -- Read data successful
  --
  SET @StatusCode = 0
  SET @StatusText = 'PT_ReadPointFactors: Point Calculation read.'

  GOTO EXIT_PROCEDURE

ERROR_PROCEDURE:
  SET @RedeemablePlayedTo1Point = 0.00
  SET @TotalPlayedTo1Point      = 0.00
  SET @RedeemableSpentTo1Point  = 0.00

EXIT_PROCEDURE:

END -- PT_ReadPointFactors
GO

--------------------------------------------------------------------------------
-- PURPOSE : Compute the Balance parts.
--           The balace is splitted as:
--              - Non Redeemable
--              - Redeemable
--  PARAMS :
--      - INPUT :
--          @PlayedTotal            money
--          @PlayedRedeemable       money
--
--      - OUTPUT :
--
-- RETURNS :
--
--   NOTES :

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SiteJackpot_AccumulatePlayed]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SiteJackpot_AccumulatePlayed]
GO
CREATE PROCEDURE [dbo].SiteJackpot_AccumulatePlayed
  @PlayedTotal            money
, @PlayedRedeemable       money
AS
BEGIN
	
  --
  -- Update Site Jackpot Played amounts
  --
  UPDATE SITE_JACKPOT_PARAMETERS
     SET SJP_PLAYED = SJP_PLAYED + CASE WHEN (SJP_ONLY_REDEEMABLE = 1) THEN @PlayedRedeemable ELSE @PlayedTotal END
   WHERE SJP_ENABLED = 1 

END -- SiteJackpot_AccumulatePlayed
GO

--------------------------------------------------------------------------------
-- PURPOSE : Accumulate points for player when play session is finished
-- 
--  PARAMS :
--      - INPUT :
--          @PlaySessionId      bigint
--
--      - OUTPUT :
--
-- RETURNS :
--
--   NOTES :

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PT_PlaySessionFinished]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PT_PlaySessionFinished]
GO
CREATE PROCEDURE [dbo].[PT_PlaySessionFinished]
  @PlaySessionId      bigint
AS
BEGIN

	-- Only for test
  DECLARE @status_code                       int
  DECLARE @status_text                       nvarchar (500)
  -- Only for test
  
  DECLARE @terminal_id                       int
  DECLARE @account_id                        bigint
  DECLARE @total_cash_in                     money
  DECLARE @played_amount                     money
  DECLARE @won_amount                        money
  DECLARE @total_cash_out                    money
  DECLARE @initial_non_redeemable            money
  DECLARE @initial_cash_in                   money
  DECLARE @prize_lock                        money
  DECLARE @account_balance                   money
  DECLARE @holder_name                       nvarchar (50)
  DECLARE @holder_level                      int
  
  DECLARE @non_redeemable_cash_in            money
  DECLARE @redeemable_cash_in                money
  DECLARE @non_redeemable_cash_out           money
  DECLARE @redeemable_cash_out               money
  DECLARE @non_redeemable_played             money
  DECLARE @redeemable_played                 money
  DECLARE @non_redeemable_won                money
  DECLARE @redeemable_won                    money
    
  DECLARE @spent_no_redeemable               money
  DECLARE @spent_redeemable                  money

  DECLARE @profit_no_redeemable              money
  DECLARE @profit_redeemable                 money
  
  DECLARE @total_sum_redimible               numeric (20,6)
  DECLARE @total_sum                         numeric (20,6)
  DECLARE @percent_redeemable                numeric (8,6)

  DECLARE @rest_played_and_no_spent          money
  DECLARE @rest_won_and_no_profit            money
  
  DECLARE @total_played_to_1_point           numeric (20,6)
  DECLARE @redeemable_played_to_1_point      numeric (20,6)
  DECLARE @redeemable_spent_to_1_point       numeric (20,6)

  DECLARE @points_balance_before             money
  DECLARE @points_balance_after              money
  DECLARE @points_total_played               money
  DECLARE @points_played_redeemable          money
  DECLARE @points_spent_redeemable           money
  DECLARE @won_points                        money
  
  DECLARE @max_allowed_acc_balance           numeric (20,6)

  -- SET NOCOUNT ON added to prevent extra result sets from
  -- interfering with SELECT statements.
  SET NOCOUNT ON;
	
  SET @status_code  = 0
  SET @status_text  = 'PT_PlaySessionFinished: Init'

  SET @points_total_played      = 0
  SET @points_played_redeemable = 0
  SET @points_spent_redeemable  = 0
  SET @won_points               = 0  
  SET @points_balance_before    = 0
  SET @points_balance_after     = 0


  IF ( @PlaySessionId = 0 ) 
  BEGIN
    SET @status_text = 'PT_PlaySessionFinished: Invalid PlaySessionId'
    GOTO ERROR_PROCEDURE
  END

  --
  -- Read Session & Account & PlayerTracking Data
  --
  EXECUTE dbo.PT_ReadData @PlaySessionId, @terminal_id OUTPUT, @account_id OUTPUT,
                          @total_cash_in OUTPUT, @played_amount OUTPUT, @won_amount OUTPUT, @total_cash_out OUTPUT, 
                          @initial_non_redeemable OUTPUT, @initial_cash_in OUTPUT, @prize_lock OUTPUT, @account_balance OUTPUT, 
                          @points_balance_before OUTPUT, @holder_name OUTPUT, @holder_level OUTPUT, @max_allowed_acc_balance OUTPUT,
                          @status_code OUTPUT, @status_text OUTPUT

  IF ( @status_code <> 0 )
    GOTO ERROR_PROCEDURE
    
  --
  -- Compute PlayedCredits, PlayedRedeemableCredits, SpentRedeemableCredits
  --
  EXECUTE dbo.PT_BalancePartsWhenPlaying @total_cash_in, @initial_cash_in, @initial_non_redeemable, @prize_lock, 
                                         @non_redeemable_cash_in OUTPUT, @redeemable_cash_in OUTPUT

  EXECUTE dbo.PT_BalancePartsWhenPlaying @total_cash_out, @initial_cash_in, @initial_non_redeemable, @prize_lock, 
                                         @non_redeemable_cash_out OUTPUT, @redeemable_cash_out OUTPUT

  SET @spent_no_redeemable      = dbo.Maximum_Money (0, @non_redeemable_cash_in - @non_redeemable_cash_out)
  SET @spent_redeemable         = dbo.Maximum_Money (0, @redeemable_cash_in - @redeemable_cash_out)

  SET @profit_no_redeemable     = dbo.Maximum_Money (0, @non_redeemable_cash_out - @non_redeemable_cash_in)
  SET @profit_redeemable        = dbo.Maximum_Money (0, @redeemable_cash_out - @redeemable_cash_in)

  SET @total_sum_redimible      = @redeemable_cash_in + @redeemable_cash_out 
  SET @total_sum                = @non_redeemable_cash_in + @non_redeemable_cash_out + @redeemable_cash_in + @redeemable_cash_out
  
  IF @total_sum = 0 GOTO EXIT_PROCEDURE   
  SET @percent_redeemable       = @total_sum_redimible / @total_sum

  SET @rest_played_and_no_spent = @played_amount - (@spent_no_redeemable + @spent_redeemable)
  SET @redeemable_played        = ROUND(@spent_redeemable + (@rest_played_and_no_spent * @percent_redeemable), 4)
  SET @non_redeemable_played    = @played_amount - @redeemable_played

  SET @rest_won_and_no_profit   = @won_amount - (@profit_no_redeemable + @profit_redeemable)
  SET @redeemable_won           = ROUND(@profit_redeemable + (@rest_won_and_no_profit * @percent_redeemable), 4)
  SET @non_redeemable_won       = @won_amount - @redeemable_won

  --
  -- Update PlaySession table (Redeemable & NonRedeemable amounts)
  --
  UPDATE PLAY_SESSIONS
     SET PS_NON_REDEEMABLE_CASH_IN    = @non_redeemable_cash_in
       , PS_NON_REDEEMABLE_CASH_OUT   = @non_redeemable_cash_out
       , PS_NON_REDEEMABLE_PLAYED     = @non_redeemable_played
       , PS_NON_REDEEMABLE_WON        = @non_redeemable_won
       , PS_REDEEMABLE_CASH_IN        = @redeemable_cash_in
       , PS_REDEEMABLE_CASH_OUT       = @redeemable_cash_out
       , PS_REDEEMABLE_PLAYED         = @redeemable_played
       , PS_REDEEMABLE_WON            = @redeemable_won
   WHERE PS_PLAY_SESSION_ID = @PlaySessionId 

  --
  -- RCI & ACC & AJQ 30/09/2010: Reset the Cancellable Operation when PlayedAmount greater than 0.
  --
  UPDATE   ACCOUNTS
     SET   AC_CANCELLABLE_OPERATION_ID = CASE WHEN (@played_amount > 0) THEN NULL ELSE AC_CANCELLABLE_OPERATION_ID END
   WHERE   AC_ACCOUNT_ID               = @account_id  

  --
  -- ACC 22/10/2010: Check maximum allowed account balance
  --
  IF ( @max_allowed_acc_balance > 0 AND @total_cash_out > @max_allowed_acc_balance )
  BEGIN
    UPDATE ACCOUNTS
       SET AC_BLOCKED       = 1
         , AC_BLOCK_REASON  = 2  -- MaxBalance
     WHERE AC_ACCOUNT_ID    = @account_id  
  END

  --- Anonymous accounts have Level=0
  IF ( @holder_level = 0 )
    GOTO EXIT_PROCEDURE

  --
  -- Read PlayerTracking Point Conversion Factors according to the holder's level
  --
  EXECUTE dbo.PT_ReadPointFactors @holder_level, 
                                  @redeemable_spent_to_1_point  OUTPUT,
                                  @redeemable_played_to_1_point OUTPUT,
                                  @total_played_to_1_point      OUTPUT,
                                  @status_code                  OUTPUT, 
                                  @status_text                  OUTPUT

  IF ( @status_code <> 0 )
    GOTO ERROR_PROCEDURE

  --
  -- Compute Points (it is possible to accummulate points per each type of credits at the same time)
  --  
  IF @redeemable_spent_to_1_point  > 0 SET @points_spent_redeemable  = @spent_redeemable  / @redeemable_spent_to_1_point
  IF @redeemable_played_to_1_point > 0 SET @points_played_redeemable = @redeemable_played / @redeemable_played_to_1_point
  IF @total_played_to_1_point      > 0 SET @points_total_played      = @played_amount     / @total_played_to_1_point
  
  SET @won_points = ROUND(@points_total_played + @points_played_redeemable + @points_spent_redeemable, 4)

  --
  -- Accumulate points
  -- 
  UPDATE   ACCOUNTS
     SET   AC_POINTS                   = AC_POINTS  + @won_points
   WHERE   AC_ACCOUNT_ID               = @account_id  
  
  --  
  -- Create CardMovement.ObtainedPoints
  --
  -- PointsAwarded   = 36,
  SET @points_balance_after     = @points_balance_before + @won_points
  
  EXECUTE dbo.InsertMovement @PlaySessionId, @account_id, @terminal_id, 36, @points_balance_before, 0, @won_points, @points_balance_after

EXIT_PROCEDURE:

  --
  -- ACC 25/11/2010 Accumulate played amount into site jackpot
  --
  EXECUTE dbo.SiteJackpot_AccumulatePlayed @played_amount, @redeemable_played

  SET @status_code  = 0
  SET @status_text  = 'PT_PlaySessionFinished: Successful points accumulated.' 
                    + ' *** '
                    + ' Points.PerRedeemableSpent  = ' + CAST (@points_spent_redeemable AS nvarchar)  + ' (' + CAST (@spent_redeemable AS nvarchar)  + ' credits)'
                    + ' Points.PerRedeemablePlayed = ' + CAST (@points_played_redeemable AS nvarchar) + ' (' + CAST (@redeemable_played AS nvarchar) + ' credits)'
                    + ' Points.PerTotalPlayed      = ' + CAST (@points_total_played AS nvarchar)      + ' (' + CAST (@played_amount AS nvarchar)     + ' credits)'
                    + ' Points.TotalAwarded        = ' + CAST (@won_points AS nvarchar) 
                    + ' *** '

ERROR_PROCEDURE:
--
-- ACC 18-OCT-2010 DO NOT SELECT (3GS not works with this select)
--
--  SELECT @status_text AS StatusText

END -- PT_PlaySessionFinished
GO

/****** TRIGGERS ******/


/****** RECORDS ******/
/* Cashier */
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY ='HidePromotion')
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('Cashier'
           ,'HidePromotion'
           ,'0');
ELSE
SELECT '***** Record Cashier_HidePromotion already exists *****';

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY ='InitialCashIn.LimitToOpeningTime')
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('Cashier'
           ,'InitialCashIn.LimitToOpeningTime'
           ,'1');
ELSE
SELECT '***** Record Cashier_InitialCashIn.LimitToOpeningTime already exists *****';
      
/* PlayerTracking */
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PlayerTracking' AND GP_SUBJECT_KEY ='Level04.Name')
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('PlayerTracking'
           ,'Level04.Name'
           ,'Win');
ELSE
SELECT '***** Record PlayerTracking_Level04.Name already exists *****';

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PlayerTracking' AND GP_SUBJECT_KEY ='Level04.RedeemablePlayedTo1Point')
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('PlayerTracking'
           ,'Level04.RedeemablePlayedTo1Point'
           ,'0');
ELSE
SELECT '***** Record PlayerTracking_Level04.RedeemablePlayedTo1Point already exists *****';

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PlayerTracking' AND GP_SUBJECT_KEY ='Level04.TotalPlayedTo1Point')
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('PlayerTracking'
           ,'Level04.TotalPlayedTo1Point'
           ,'0');
ELSE
SELECT '***** Record PlayerTracking_Level04.TotalPlayedTo1Point already exists *****';

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PlayerTracking' AND GP_SUBJECT_KEY ='Level04.RedeemableSpentTo1Point')
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('PlayerTracking'
           ,'Level04.RedeemableSpentTo1Point'
           ,'0');
ELSE
SELECT '***** Record PlayerTracking_Level04.RedeemableSpentTo1Point already exists *****';

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PlayerTracking' AND GP_SUBJECT_KEY ='Level02.PointsToEnter')
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('PlayerTracking'
           ,'Level02.PointsToEnter'
           ,'1000');
ELSE
SELECT '***** Record PlayerTracking_Level02.PointsToEnter already exists *****';

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PlayerTracking' AND GP_SUBJECT_KEY ='Level02.DaysOnLevel')
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('PlayerTracking'
           ,'Level02.DaysOnLevel'
           ,'90');
ELSE
SELECT '***** Record PlayerTracking_Level02.DaysOnLevel already exists *****';

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PlayerTracking' AND GP_SUBJECT_KEY ='Level03.PointsToEnter')
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('PlayerTracking'
           ,'Level03.PointsToEnter'
           ,'2000');
ELSE
SELECT '***** Record PlayerTracking_Level03.PointsToEnter already exists *****';

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PlayerTracking' AND GP_SUBJECT_KEY ='Level03.DaysOnLevel')
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('PlayerTracking'
           ,'Level03.DaysOnLevel'
           ,'90');
ELSE
SELECT '***** Record PlayerTracking_Level03.DaysOnLevel already exists *****';

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PlayerTracking' AND GP_SUBJECT_KEY ='Level04.PointsToEnter')
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('PlayerTracking'
           ,'Level04.PointsToEnter'
           ,'3000');
ELSE
SELECT '***** Record PlayerTracking_Level04.PointsToEnter already exists *****';

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PlayerTracking' AND GP_SUBJECT_KEY ='Level04.DaysOnLevel')
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('PlayerTracking'
           ,'Level04.DaysOnLevel'
           ,'90');
ELSE
SELECT '***** Record PlayerTracking_Level04.DaysOnLevel already exists *****';

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PlayerTracking' AND GP_SUBJECT_KEY ='Levels.DaysCountingPoints')
INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('PlayerTracking'
           ,'Levels.DaysCountingPoints'
           ,'30');
ELSE
SELECT '***** Record PlayerTracking_Levels.DaysCountingPoints already exists *****';

/*** Terminal Types ***/
IF NOT EXISTS (SELECT * FROM terminal_types WHERE tt_type = 100)
INSERT INTO [dbo].[terminal_types]
           ([tt_type]
           ,[tt_name])
     VALUES (100
            ,'SITE');
ELSE            
SELECT '***** Record Terminal_Types_100 already exists *****';

IF NOT EXISTS (SELECT * FROM terminal_types WHERE tt_type = 101)
INSERT INTO [dbo].[terminal_types]
           ([tt_type]
           ,[tt_name])
     VALUES (101
            ,'SITE JACKPOT');
ELSE            
SELECT '***** Record Terminal_Types_101 already exists *****';

IF NOT EXISTS (SELECT * FROM terminal_types WHERE tt_type = 102)
INSERT INTO [dbo].[terminal_types]
           ([tt_type]
           ,[tt_name])
     VALUES (102
            ,'MOBILE BANK');
ELSE            
SELECT '***** Record Terminal_Types_102 already exists *****';


/*** Terminals ***/
IF NOT EXISTS (SELECT * FROM terminals WHERE te_terminal_type = 100)
INSERT INTO [dbo].[terminals]
           ([te_type]
           ,[te_name]
           ,[te_external_id]
           ,[te_blocked]
           ,[te_active]
           ,[te_provider_id]
           ,[te_terminal_type]
           ,[te_status])
     VALUES (1
            ,'PAGOS SALA'
            ,'PAGOS SALA'
            ,0
            ,1
            ,'CASINO'
            ,100
            ,0);
ELSE            
SELECT '***** Record terminals_PAGOS SALA already exists *****';


/* Initialization of new fields in Cashier_Sessions */
/* IEPS */
IF EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY ='Split.A.Tax.Pct')
UPDATE [dbo].[cashier_sessions]
   SET cs_tax_a_pct = (SELECT CAST(gp_key_value AS DECIMAL(5,2)) FROM general_params WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY ='Split.A.Tax.Pct');
ELSE
SELECT '***** Record Cashier_Split.A.Tax.Pct does not exist *****';

/* IVA */
IF EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY ='Split.B.Tax.Pct')
UPDATE [dbo].[cashier_sessions]
   SET cs_tax_b_pct = (SELECT CAST(gp_key_value AS DECIMAL(5,2)) FROM general_params WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY ='Split.B.Tax.Pct');
ELSE
SELECT '***** Record Cashier_Split.B.Tax.Pct does not exist *****';


/* Populate Terminals_Connected table - Phase 1 */
USE [wgdb_000]
GO

DECLARE TerminalSessionCursor CURSOR
FOR
select ws_terminal_id
     , dateadd(dd,datediff(dd,0,ws_started),0) AS SessionStart
     , dateadd(dd,datediff(dd,0,isnull(ws_finished, Getdate())),0) AS SessionFinish
from wcp_sessions
group by ws_terminal_id
     , dateadd(dd,datediff(dd,0,ws_started),0) 
     , dateadd(dd,datediff(dd,0,isnull(ws_finished, Getdate())),0)
order by ws_terminal_id, SessionStart, SessionFinish

DECLARE @tmp_terminal_id           AS bigint
DECLARE @tmp_start_date            AS Datetime
DECLARE @tmp_end_date              AS Datetime
DECLARE @tmp_last_date             AS DateTime
DECLARE @tmp_current_date          AS DateTime
DECLARE @tmp_term_count		   AS bigint

OPEN TerminalSessionCursor 

-- FETCH FIRST Terminal
FETCH TerminalSessionCursor  INTO @tmp_terminal_id
                                , @tmp_start_date
                                , @tmp_end_date

SET @tmp_last_date = dateadd(dd, -1, @tmp_start_date)
SET @tmp_term_count = 0

WHILE @@Fetch_Status = 0
  BEGIN
	
    SET @tmp_term_count = @tmp_term_count + 1
    SET @tmp_current_date = CASE WHEN @tmp_start_date = @tmp_last_date THEN dateadd(dd, +1, @tmp_last_date) 
                                                                       ELSE @tmp_start_date END
    WHILE @tmp_current_date <= @tmp_end_date
      BEGIN

        IF NOT EXISTS (SELECT * FROM TERMINALS_CONNECTED WHERE TC_TERMINAL_ID = @tmp_terminal_id and tc_date = @tmp_current_date)
          INSERT INTO terminals_connected
          		  ( tc_terminal_id
        		  , tc_date )
        	VALUES
        	  	  ( @tmp_terminal_id
        		  , @tmp_current_date)
        --ELSE
        --  SELECT 'Record not inserted ' + CAST(@tmp_terminal_id as Varchar(10)) + ' - ' + CAST(@tmp_current_date as Varchar(10))
        		
        SET @tmp_current_date = dateadd(dd, +1, @tmp_current_date)
      END

    SET @tmp_last_date = @tmp_end_date
    
    -- FETCH NEXT RESULT
    FETCH TerminalSessionCursor  INTO @tmp_terminal_id
                                    , @tmp_start_date
                                    , @tmp_end_date

  END

  SELECT 'Records processed: ' + CAST(@tmp_term_count as Varchar(10))

CLOSE TerminalSessionCursor 

DEALLOCATE TerminalSessionCursor 
GO

/* Populate Terminals_Connected table - Phase 2 */
USE [wgdb_000]
GO

DECLARE Terminals3GSCursor CURSOR
FOR
SELECT t3gs_terminal_id
FROM terminals_3gs
ORDER BY t3gs_terminal_id

DECLARE @tmp_terminal_id           AS bigint
DECLARE @tmp_start_date            AS Datetime
DECLARE @tmp_end_date              AS Datetime
DECLARE @tmp_current_date          AS DateTime
DECLARE @tmp_term_count		   AS bigint

OPEN Terminals3GSCursor 

-- FETCH FIRST Terminal
FETCH Terminals3GSCursor  INTO @tmp_terminal_id

SET @tmp_term_count = 0
SELECT @tmp_start_date = DateAdd(DD, DateDiff(DD, 0, min (sph_base_hour)), 0) FROM sales_per_hour
SELECT @tmp_end_date = DateAdd(DD, DateDiff(DD, 0, GetDate()), -1)

SELECT 'Starting from date: ' + CAST(@tmp_start_date as Varchar(20))
SELECT 'Ending in date: ' + CAST(@tmp_end_date as Varchar(20))

WHILE @@Fetch_Status = 0
  BEGIN
	
    SET @tmp_term_count = @tmp_term_count + 1
    SET @tmp_current_date = @tmp_start_date
                            
    WHILE @tmp_current_date <= @tmp_end_date
      BEGIN
        IF NOT EXISTS (SELECT * FROM TERMINALS_CONNECTED WHERE TC_TERMINAL_ID = @tmp_terminal_id and tc_date = @tmp_current_date)
          INSERT INTO terminals_connected
          		  ( tc_terminal_id
        		  , tc_date )
        	  VALUES
        		  ( @tmp_terminal_id
        		  , @tmp_current_date)
        ELSE
          SELECT 'Record not inserted: ' + CAST(@tmp_terminal_id as Varchar(10)) + ' - ' + CAST(@tmp_current_date as Varchar(20))
        		
        SET @tmp_current_date = dateadd(dd, +1, @tmp_current_date)
      END
    
    -- FETCH NEXT RESULT
    FETCH Terminals3GSCursor INTO @tmp_terminal_id

  END

  SELECT 'Terminals processed: ' + CAST(@tmp_term_count as Varchar(10))

CLOSE Terminals3GSCursor 

DEALLOCATE Terminals3GSCursor 
GO

/* Update te_status (@Terminals) field from records in Terminals_Connected table */
USE [wgdb_000]
GO

DECLARE TerminalStatusCursor CURSOR
FOR
SELECT te_terminal_id, CASE WHEN max(tc_date) is null THEN 9999 
                            ELSE DATEDIFF(DD, max(tc_date), GetDate())
                            END
FROM terminals LEFT OUTER JOIN terminals_connected ON te_terminal_id = tc_terminal_id
WHERE te_type = 1 AND te_terminal_type < 100
GROUP BY te_terminal_id

DECLARE @tmp_terminal_id           AS bigint
DECLARE @tmp_inactivity            AS bigint
DECLARE @tmp_term_count		   AS bigint

OPEN TerminalStatusCursor 

-- FETCH FIRST Terminal
FETCH TerminalStatusCursor INTO @tmp_terminal_id, @tmp_inactivity

SET @tmp_term_count = 0

WHILE @@Fetch_Status = 0
  BEGIN
	
    SET @tmp_term_count = @tmp_term_count + 1
    
    UPDATE terminals
       SET te_status = CASE WHEN te_blocked = 1 THEN 1
                            WHEN @tmp_inactivity >= 30 THEN 2
                            WHEN @tmp_inactivity >= 7 THEN 1
                            ELSE 0 
                       END
    WHERE te_terminal_id = @tmp_terminal_id
    
    -- FETCH NEXT RESULT
    FETCH TerminalStatusCursor INTO @tmp_terminal_id, @tmp_inactivity

  END

  SELECT 'Terminals processed: ' + CAST(@tmp_term_count as Varchar(10))

CLOSE TerminalStatusCursor 

DEALLOCATE TerminalStatusCursor 
GO