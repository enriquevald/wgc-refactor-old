/******* GENERIC REPORT *******/

BEGIN
  DECLARE @rtc_form_id INT
  DECLARE @rtc_design_sheet NVARCHAR(MAX)
  DECLARE @rtc_report_name NVARCHAR(MAX)
  
  SET @rtc_report_name =     N'<LanguageResources><NLS09><Label>Cage movements for currency</Label></NLS09><NLS10><Label>Movimientos de bóveda por divisa</Label></NLS10></LanguageResources>'                                                              
  SET @rtc_design_sheet =    N'<ArrayOfReportToolDesignSheetsDTO>
                                 <ReportToolDesignSheetsDTO>
                                   <LanguageResources>
                                     <NLS09>
                                       <Label>Cage movements for currency</Label>
                                     </NLS09>
                                     <NLS10>
                                       <Label>Movimientos de bóveda por divisa</Label>
                                     </NLS10>
                                   </LanguageResources>
                                   <Columns>
                                     <ReportToolDesignColumn>
                                       <Code>Fecha</Code>
                                       <Width>300</Width>
                                       <EquityMatchType>Equality</EquityMatchType>
                                       <LanguageResources>
                                         <NLS09>
                                           <Label>Date</Label>
                                         </NLS09>
                                         <NLS10>
                                           <Label>Fecha</Label>
                                         </NLS10>
                                      </LanguageResources>
                                    </ReportToolDesignColumn>
                                    <ReportToolDesignColumn>
                                     <Code>Usuario de Bóveda</Code>
                                     <Width>300</Width>
                                     <EquityMatchType>Equality</EquityMatchType>
                                     <LanguageResources>
                                       <NLS09>
                                         <Label>Cash cage user</Label>
                                       </NLS09>
                                       <NLS10>
                                         <Label>Usuario de Bóveda</Label>
                                       </NLS10>
                                     </LanguageResources>
                                   </ReportToolDesignColumn>
                                   <ReportToolDesignColumn>
                                     <Code>Origen/Destino</Code>
                                     <Width>300</Width>
                                     <EquityMatchType>Equality</EquityMatchType>
                                     <LanguageResources>
                                       <NLS09>
                                         <Label>Source/Destination</Label>
                                       </NLS09>
                                       <NLS10>
                                         <Label>Origen/Destino</Label>
                                       </NLS10>
                                     </LanguageResources>
                                   </ReportToolDesignColumn>
                                   <ReportToolDesignColumn>
                                     <Code>Tipo de movimiento</Code>
                                     <Width>300</Width>
                                     <EquityMatchType>Equality</EquityMatchType>
                                     <LanguageResources>
                                       <NLS09>
                                         <Label>Type</Label>
                                       </NLS09>
                                       <NLS10>
                                         <Label>Tipo de movimiento</Label>
                                       </NLS10>
                                     </LanguageResources>
                                   </ReportToolDesignColumn>
                                   <ReportToolDesignColumn>
                                     <Code>Estado</Code>
                                     <Width>300</Width>
                                     <EquityMatchType>Equality</EquityMatchType>
                                     <LanguageResources>
                                       <NLS09>
                                         <Label>Status</Label>
                                       </NLS09>
                                       <NLS10>
                                         <Label>Estado</Label>
                                       </NLS10>
                                     </LanguageResources>
                                   </ReportToolDesignColumn>
                                   <ReportToolDesignColumn>
                                     <Code>Otros</Code>
                                     <Width>300</Width>
                                     <EquityMatchType>Contains</EquityMatchType>
                                     <LanguageResources>
                                       <NLS09>
                                         <Label>Other</Label>
                                       </NLS09>
                                       <NLS10>
                                         <Label>Otros</Label>
                                       </NLS10>
                                     </LanguageResources>
                                   </ReportToolDesignColumn>
                                   <ReportToolDesignColumn>
                                     <Code>Fichas</Code>
                                     <Width>300</Width>
                                     <EquityMatchType>Equality</EquityMatchType>
                                     <LanguageResources>
                                       <NLS09>
                                         <Label>Chips</Label>
                                       </NLS09>
                                     <NLS10>
                                       <Label>Fichas</Label>
                                     </NLS10>
                                   </LanguageResources>
                                 </ReportToolDesignColumn>
                               </Columns>
                             </ReportToolDesignSheetsDTO>
                           </ArrayOfReportToolDesignSheetsDTO>'
  
  IF EXISTS(SELECT 1 FROM report_tool_config WHERE rtc_store_name = N'GetCageMovementType_GR')
  BEGIN
      UPDATE [dbo].[REPORT_TOOL_CONFIG] 
      SET RTC_REPORT_NAME =         /*RTC_REPORT_NAME*/     @rtc_report_name,
          RTC_DESIGN_SHEETS =       /*RTC_DESIGN_SHEETS*/   @rtc_design_sheet
      WHERE RTC_STORE_NAME = N'GetCageMovementType_GR'
  END
END
GO

BEGIN
  DECLARE @rtc_form_id INT
  DECLARE @rtc_design_sheet NVARCHAR(MAX)
  DECLARE @rtc_report_name NVARCHAR(MAX)
  
  SET @rtc_report_name =      N'<LanguageResources><NLS09><Label>Meters collection to collection</Label></NLS09><NLS10><Label>Contadores recolección a recolección</Label></NLS10></LanguageResources>'
  SET @rtc_design_sheet =     N'<ArrayOfReportToolDesignSheetsDTO>
                                <ReportToolDesignSheetsDTO>
                                  <LanguageResources>
                                    <NLS09>
                                      <Label>Collection to collection</Label>
                                    </NLS09>
                                    <NLS10>
                                      <Label>Recolección a recolección</Label>
                                    </NLS10>
                                  </LanguageResources>
                                  <Columns>
                                    <ReportToolDesignColumn>
                                      <Code>Gaming Date</Code>
                                      <Width>300</Width>
                                      <EquityMatchType>Equality</EquityMatchType>
                                      <LanguageResources>
                                        <NLS09>
                                          <Label>Gaming Date</Label>
                                        </NLS09>
                                        <NLS10>
                                          <Label>Fecha de juego</Label>
                                        </NLS10>
                                      </LanguageResources>
                                    </ReportToolDesignColumn>
                                    <ReportToolDesignColumn>
                                      <Code>Machine Number</Code>
                                      <Width>300</Width>
                                      <EquityMatchType>Equality</EquityMatchType>
                                      <LanguageResources>
                                        <NLS09>
                                          <Label>Machine Number</Label>
                                        </NLS09>
                                        <NLS10>
                                          <Label>Numero de maquina</Label>
                                        </NLS10>
                                      </LanguageResources>
                                    </ReportToolDesignColumn>
                                    <ReportToolDesignColumn>
                                      <Code>Machine Denomination</Code>
                                      <Width>300</Width>
                                      <EquityMatchType>Equality</EquityMatchType>
                                      <LanguageResources>
                                        <NLS09>
                                          <Label>Machine Denomination</Label>
                                        </NLS09>
                                        <NLS10>
                                          <Label>Denominación de maquina</Label>
                                        </NLS10>
                                      </LanguageResources>
                                    </ReportToolDesignColumn>
                                    <ReportToolDesignColumn>
                                      <Code>fisico</Code>
                                      <Width>300</Width>
                                      <EquityMatchType>Contains</EquityMatchType>
                                      <LanguageResources>
                                        <NLS09>
                                          <Label>physical</Label>
                                        </NLS09>
                                        <NLS10>
                                          <Label>fisico</Label>
                                        </NLS10>
                                      </LanguageResources>
                                    </ReportToolDesignColumn>
                                    <ReportToolDesignColumn>
                                      <Code>Total contado en el soft count</Code>
                                      <Width>300</Width>
                                      <EquityMatchType>Equality</EquityMatchType>
                                      <LanguageResources>
                                        <NLS09>
                                          <Label>Total count in soft count</Label>
                                        </NLS09>
                                        <NLS10>
                                          <Label>Total contado en el soft count</Label>
                                        </NLS10>
                                      </LanguageResources>
                                    </ReportToolDesignColumn>
                                  </Columns>
                                </ReportToolDesignSheetsDTO>
                              </ArrayOfReportToolDesignSheetsDTO>'
  
  IF EXISTS(SELECT 1 FROM report_tool_config WHERE rtc_store_name = N'GetMetersCollectionToCollection_GR')
  BEGIN
      UPDATE [dbo].[REPORT_TOOL_CONFIG] 
      SET RTC_REPORT_NAME =         /*RTC_REPORT_NAME*/     @rtc_report_name,
          RTC_DESIGN_SHEETS =       /*RTC_DESIGN_SHEETS*/   @rtc_design_sheet
      WHERE RTC_STORE_NAME = N'GetMetersCollectionToCollection_GR'
  END
END
GO