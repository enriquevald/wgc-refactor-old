﻿
IF OBJECT_ID (N'dbo.SP_Cashier_Sessions_Report', N'P') IS NOT NULL
    DROP PROCEDURE dbo.SP_Cashier_Sessions_Report;                 
GO  

/*
----------------------------------------------------------------------------------------------------------------
CASHIER SESSIONS REPORTS

Version  Date           User     Description
----------------------------------------------------------------------------------------------------------------
1.0.0    28-FEB-2014    RMS      First Release
1.0.1    14-APR-2015    RMS      Gaming Day
1.0.2    01-FEB-2016    FOS      Floor Dual Currency
1.0.3	   03-JUL-2018		FJC			 Fixed Bug: WIGOS-13348 AGG - SITE: Cash session search doesn't work properly. After search option, "Search" button is disabled in cash session form.

Parameters:
   -- :
  
 Results:
   
----------------------------------------------------------------------------------------------------------------   
*/


CREATE PROCEDURE [dbo].[SP_Cashier_Sessions_Report]
(  @pDateFrom DateTime
  , @pDateTo DateTime
  , @pReportMode as Int
  , @pShowSystemSessions as Bit
  , @pSystemUsersTypes as VarChar(4096)
  , @pSessionStatus as VarChar(4096)
  , @pUserId as Integer
  , @pCashierId as Integer
  , @pCurrencyISOCode as VarChar(3)
)
AS
BEGIN
-- Members --
DECLARE @_delimiter AS CHAR(1)
SET @_delimiter = ','
DECLARE @pSystemUsersTypesTable AS TABLE (SUT_ID INT, SUT_VALUE VARCHAR(50))
DECLARE @pSessionsStatusTable AS TABLE (SUT_ID INT, SUT_VALUE VARCHAR(50))
DECLARE @_table_sessions_ AS TABLE (  sess_id                      BIGINT 
                                    , sess_name                    NVARCHAR(50) 
                                    , sess_user_id                 INT      
                                    , sess_full_name               NVARCHAR(50) 
                                    , sess_opening_date            DATETIME 
                                    , sess_closing_date            DATETIME  
                                    , sess_cashier_id              INT 
                                    , sess_ct_name                 NVARCHAR(50) 
                                    , sess_status                  INT 
                                    , sess_cs_limit_sale           MONEY
                                    , sess_mb_limit_sale           MONEY
                                    , sess_collected_amount        MONEY
                                    , sess_gaming_day              DATETIME
                                    , sess_te_iso_code             NVARCHAR(3)
                                   ) 
                                                           
-- SYSTEM USER TYPES --
IF @pShowSystemSessions = 0
BEGIN
INSERT INTO @pSystemUsersTypesTable SELECT * FROM dbo.SplitStringIntoTable(@pSystemUsersTypes, @_delimiter, DEFAULT)
END

-- SESSIONS STATUS
INSERT INTO @pSessionsStatusTable SELECT * FROM dbo.SplitStringIntoTable(@pSessionStatus, @_delimiter, DEFAULT)

IF @pReportMode = 0 OR @pReportMode = 2
   BEGIN -- Filter Dates on cashier session opening date  

    IF @pReportMode = 0
      BEGIN
        -- SESSIONS BY OPENING DATE --
        INSERT   INTO @_table_sessions_
        SELECT   DISTINCT(CS_SESSION_ID) AS SESS_ID
               , CS_NAME AS SESS_NAME
               , CS_USER_ID AS SESS_USER_ID
               , GU_FULL_NAME AS SESS_FULL_NAME
               , CS_OPENING_DATE AS SESS_OPENING_DATE
               , CS_CLOSING_DATE AS SESS_CLOSING_DATE
               , CS_CASHIER_ID AS SESS_CASHIER_ID
               , CT_NAME AS SESS_CT_NAME
               , CS_STATUS AS SESS_STATUS
               , CS_SALES_LIMIT AS SESS_CS_LIMIT_SALE
               , CS_MB_SALES_LIMIT AS SESS_MB_LIMIT_SALE
               , CS_COLLECTED_AMOUNT AS SESS_COLLECTED_AMOUNT
               , CS_GAMING_DAY AS SESS_GAMING_DAY
               , ISNULL(TE_ISO_CODE, @pCurrencyISOCode) AS SESS_TE_ISO_CODE 
          FROM   CASHIER_SESSIONS --WITH (INDEX (IX_cs_opening_date)) /* TODO: REVIEW INDEXES */
         INNER   JOIN GUI_USERS         ON CS_USER_ID    = GU_USER_ID
         INNER   JOIN CASHIER_TERMINALS ON CS_CASHIER_ID = CT_CASHIER_ID
          LEFT   JOIN TERMINALS         ON CT_TERMINAL_ID = TE_TERMINAL_ID   
         WHERE   (CS_OPENING_DATE >= @pDateFrom AND CS_OPENING_DATE < @pDateTo)
            --   usuarios sistema s/n & user 
           AND   (      GU_USER_TYPE NOT IN ( SELECT SUT_VALUE FROM @pSystemUsersTypesTable )
                  AND   CS_USER_ID = (CASE WHEN @pUserId IS NULL THEN CS_USER_ID ELSE @pUserId END) )
            --   cashier id
           AND   CS_CASHIER_ID = (CASE WHEN @pCashierId IS NULL THEN CS_CASHIER_ID ELSE @pCashierId END) 
            --   session status
           AND   CS_STATUS IN ( SELECT SUT_VALUE FROM @pSessionsStatusTable )
         ORDER   BY CS_OPENING_DATE DESC  
      END
    ELSE
      BEGIN
        -- @pReportMode = 2
        -- SESSIONS BY GAMING DAY --
        INSERT   INTO @_table_sessions_
        SELECT   DISTINCT(CS_SESSION_ID) AS SESS_ID
               , CS_NAME AS SESS_NAME
               , CS_USER_ID AS SESS_USER_ID
               , GU_FULL_NAME AS SESS_FULL_NAME
               , CS_OPENING_DATE AS SESS_OPENING_DATE
               , CS_CLOSING_DATE AS SESS_CLOSING_DATE
               , CS_CASHIER_ID AS SESS_CASHIER_ID
               , CT_NAME AS SESS_CT_NAME
               , CS_STATUS AS SESS_STATUS
               , CS_SALES_LIMIT AS SESS_CS_LIMIT_SALE
               , CS_MB_SALES_LIMIT AS SESS_MB_LIMIT_SALE
               , CS_COLLECTED_AMOUNT AS SESS_COLLECTED_AMOUNT
               , CS_GAMING_DAY AS SESS_GAMING_DAY
               , ISNULL(TE_ISO_CODE, @pCurrencyISOCode) AS SESS_TE_ISO_CODE 
          FROM   CASHIER_SESSIONS --WITH (INDEX (IX_cs_opening_date)) /* TODO: REVIEW INDEXES */
         INNER   JOIN GUI_USERS         ON CS_USER_ID    = GU_USER_ID
         INNER   JOIN CASHIER_TERMINALS ON CS_CASHIER_ID = CT_CASHIER_ID
          LEFT   JOIN TERMINALS         ON CT_TERMINAL_ID = TE_TERMINAL_ID   
         WHERE   (CS_GAMING_DAY >= @pDateFrom AND CS_GAMING_DAY < @pDateTo)
            --   usuarios sistema s/n & user 
           AND   (      GU_USER_TYPE NOT IN ( SELECT SUT_VALUE FROM @pSystemUsersTypesTable )
                  AND   CS_USER_ID = (CASE WHEN @pUserId IS NULL THEN CS_USER_ID ELSE @pUserId END) )
            --   cashier id
           AND   CS_CASHIER_ID = (CASE WHEN @pCashierId IS NULL THEN CS_CASHIER_ID ELSE @pCashierId END) 
            --   session status
           AND   CS_STATUS IN ( SELECT SUT_VALUE FROM @pSessionsStatusTable )
         ORDER   BY CS_GAMING_DAY DESC  
      END
      
    -- MOVEMENTS
    -- By Movements in Historical Data --    
    SELECT   CM_SESSION_ID
           , CM_TYPE
           , CM_SUB_TYPE
           , ISNULL(CM_CURRENCY_ISO_CODE, @pCurrencyISOCode) AS CM_CURRENCY_ISO_CODE
           , CM_CURRENCY_DENOMINATION
           , CM_TYPE_COUNT
           , CM_SUB_AMOUNT
           , CM_ADD_AMOUNT
           , CM_AUX_AMOUNT
           , CM_INITIAL_BALANCE
           , CM_FINAL_BALANCE
           , CM_CAGE_CURRENCY_TYPE
      FROM   CASHIER_MOVEMENTS_GROUPED_BY_SESSION_ID
     WHERE   CM_SESSION_ID IN (SELECT SESS_ID FROM @_table_sessions_)

   END
ELSE
  BEGIN -- Filter Dates on cashier movements date (@pReportMode = 1)
       
      -- MOVEMENTS
      -- By Cashier_Movements --   
      SELECT * 
      INTO #CASHIER_MOVEMENTS
      FROM ( 
            SELECT   CM_DATE
                   , CM_SESSION_ID
                   , CM_TYPE
                   , 0 as CM_SUB_TYPE
                   , ISNULL(CM_CURRENCY_ISO_CODE, @pCurrencyISOCode) AS CM_CURRENCY_ISO_CODE
                   , CM_CURRENCY_DENOMINATION
                   , CASE WHEN CM_SUB_AMOUNT < 0 OR CM_ADD_AMOUNT < 0 THEN -1 ELSE 1 END    AS CM_TYPE_COUNT
                   , CM_SUB_AMOUNT
                   , CM_ADD_AMOUNT
                   , CM_AUX_AMOUNT
                   , CM_INITIAL_BALANCE
                   , CM_FINAL_BALANCE
                   , ISNULL(CM_CAGE_CURRENCY_TYPE, 0) AS CM_CAGE_CURRENCY_TYPE
              FROM   CASHIER_MOVEMENTS 
             WHERE   CM_DATE >= @pDateFrom AND CM_DATE < @pDateTo
             UNION   ALL
            SELECT   MBM_DATETIME AS CM_DATE
                   , MBM_CASHIER_SESSION_ID AS CM_SESSION_ID
                   , MBM_TYPE AS CM_TYPE
                   , 1 AS CM_SUB_TYPE
                   , @pCurrencyISOCode AS CM_CURRENCY_ISO_CODE
                   , 0 AS CM_CURRENCY_DENOMINATION
                   , 1 AS CM_TYPE_COUNT
                   , ISNULL(MBM_SUB_AMOUNT, 0) AS CM_SUB_AMOUNT
                   , ISNULL(MBM_ADD_AMOUNT, 0) AS CM_ADD_AMOUNT
                   , 0 AS CM_AUX_AMOUNT
                   , 0 AS CM_INITIAL_BALANCE
                   , 0 AS CM_FINAL_BALANCE
                   , 0 AS CM_CAGE_CURRENCY_TYPE
              FROM   MB_MOVEMENTS 
             WHERE   MBM_DATETIME >= @pDateFrom AND MBM_DATETIME < @pDateTo
       ) AS MOVEMENTS
       
      CREATE NONCLUSTERED INDEX IX_CM_SESSION_ID ON #CASHIER_MOVEMENTS(CM_SESSION_ID)

      -- SESSIONS --  
      INSERT   INTO @_table_sessions_  
      SELECT   DISTINCT(CS_SESSION_ID) AS SESS_ID  
             , CS_NAME AS SESS_NAME  
             , CS_USER_ID AS SESS_USER_ID  
             , GU_FULL_NAME AS SESS_FULL_NAME  
             , CS_OPENING_DATE AS SESS_OPENING_DATE  
			       , CS_CLOSING_DATE AS SESS_CLOSING_DATE  
             , CS_CASHIER_ID AS SESS_CASHIER_ID  
             , CT_NAME AS SESS_CT_NAME  
             , CS_STATUS AS SESS_STATUS  
             , CS_SALES_LIMIT AS SESS_CS_LIMIT_SALE  
             , CS_MB_SALES_LIMIT AS SESS_MB_LIMIT_SALE  
             , CS_COLLECTED_AMOUNT AS SESS_COLLECTED_AMOUNT  
             , CS_GAMING_DAY AS SESS_GAMING_DAY  
			       , ISNULL(TE_ISO_CODE, @pCurrencyISOCode) AS SESS_TE_ISO_CODE  
        FROM   CASHIER_SESSIONS --WITH (INDEX (IX_cs_opening_date)) /* TODO: REVIEW INDEXES */  
       INNER   JOIN GUI_USERS         ON CS_USER_ID    = GU_USER_ID  
       INNER   JOIN CASHIER_TERMINALS ON CS_CASHIER_ID = CT_CASHIER_ID  
	      LEFT   JOIN TERMINALS         ON CT_TERMINAL_ID = TE_TERMINAL_ID   
       WHERE   CS_SESSION_ID IN (SELECT CM_SESSION_ID FROM #CASHIER_MOVEMENTS)  
          --   usuarios sistema s/n & user   
         AND   (      GU_USER_TYPE NOT IN ( SELECT SUT_VALUE FROM @pSystemUsersTypesTable )  
                AND   CS_USER_ID = (CASE WHEN @pUserId IS NULL THEN CS_USER_ID ELSE @pUserId END) )  
          --   cashier id  
         AND   CS_CASHIER_ID = (CASE WHEN @pCashierId IS NULL THEN CS_CASHIER_ID ELSE @pCashierId END)   
          --   session status  
         AND   CS_STATUS IN ( SELECT SUT_VALUE FROM @pSessionsStatusTable )  
       ORDER   BY CS_OPENING_DATE DESC    
        
       SELECT * FROM #CASHIER_MOVEMENTS INNER JOIN @_table_sessions_ ON SESS_ID = CM_SESSION_ID  
                
       DROP TABLE #CASHIER_MOVEMENTS        
         
   END   
  
-- SESSIONS with CAGE INFO  

DECLARE @pDateFromAccountsOperation AS DATETIME = CONVERT(DATETIME,0)
DECLARE @pDateToAccountsOperation AS DATETIME = DATEADD(YEAR, 1, GETDATE() )

SELECT   SESS_ID  
       , SESS_NAME  
       , SESS_USER_ID  
       , SESS_FULL_NAME  
       , SESS_OPENING_DATE  
       , SESS_CLOSING_DATE  
       , SESS_CASHIER_ID  
       , SESS_CT_NAME  
       , SESS_STATUS  
       , SESS_CS_LIMIT_SALE  
       , SESS_MB_LIMIT_SALE  
       , SESS_COLLECTED_AMOUNT   
       , X.CGS_CAGE_SESSION_ID AS CGS_CAGE_SESSION_ID  
       , X.CGS_SESSION_NAME AS CGS_SESSION_NAME   
       , SESS_GAMING_DAY  
			 , SESS_TE_ISO_CODE 
       , (SELECT	SUM(AO_AMOUNT) 
					  FROM  ACCOUNT_OPERATIONS A1  
					 WHERE  A1.AO_CODE IN (1, 3, 17) 
					   AND NOT EXISTS(SELECT	A.ao_operation_id 
					  FROM	pinpad_transactions T 
		  INNER JOIN	gui_users U 
							ON	U.gu_user_id				= T.pt_user_id 
			INNER JOIN	account_operations A 
							ON	A.ao_operation_id			= T.pt_operation_id 
					 WHERE	T.pt_status					= 1 
			 				    AND	A.ao_code					IN (1, 3, 17) 
								  AND	A.ao_operation_id			= A1.ao_operation_id 
								  AND	A.ao_cashier_session_id		= SESS_ID) 
								  AND	A1.ao_cashier_session_id	= SESS_ID
								  AND  A1.ao_datetime				>= @pDateFromAccountsOperation 
								  AND  A1.ao_datetime				< @pDateToAccountsOperation
								  ) AS SESS_MONEY_RECHARGE_AMOUNT

       , (SELECT	SUM(T.pt_total_amount) FROM pinpad_transactions T 
			INNER JOIN	gui_users U 
						  ON	U.gu_user_id = T.pt_user_id 
			INNER JOIN	account_operations A 
							ON	A.ao_operation_id = T.pt_operation_id 
				   WHERE	T.pt_status = 1 
						 AND	A.ao_code IN (1, 3, 17) 
					   AND	A.ao_cashier_session_id = SESS_ID
						 AND	A.ao_datetime				>= @pDateFromAccountsOperation 
						 AND	A.ao_datetime				< @pDateToAccountsOperation
			) AS SESS_BANK_CARD_RECHARGE_AMOUNT

       , (SELECT	Count(*) 
				    FROM	pinpad_transactions T 
	    INNER JOIN	gui_users U 
						  ON	U.gu_user_id = T.pt_user_id 
			INNER JOIN	account_operations A 
							ON	A.ao_operation_id = T.pt_operation_id 
					 WHERE	T.pt_status = 1 
						 AND	A.ao_code IN (1, 3, 17, 125) 
						 AND	A.ao_cashier_session_id = SESS_ID
						 AND	A.ao_datetime				>= @pDateFromAccountsOperation 
						 AND	A.ao_datetime				< @pDateToAccountsOperation
			 ) AS SESS_TOTAL_VOUCHERS

       , (SELECT	Count(*) 
			      FROM	pinpad_transactions T 
	    INNER JOIN	gui_users U 
		       	  ON	U.gu_user_id = T.pt_user_id 
	    INNER JOIN	account_operations A 
							ON	A.ao_operation_id = T.pt_operation_id 
					 WHERE	T.pt_status = 1 
						 AND	A.ao_code IN (1, 3, 17, 125) 
						 AND	A.ao_cashier_session_id = SESS_ID 
						 AND	EXISTS(SELECT 1 FROM pinpad_transactions_reconciliation WHERE ptc_id = T.pt_id AND ptc_reconciliate = 1)
						 AND	A.ao_datetime				>= @pDateFromAccountsOperation 
						 AND	A.ao_datetime				< @pDateToAccountsOperation
			 ) AS SESS_DELIVERED_VOUCHERS

       , (SELECT	ISNULL(SUM(T.pt_total_amount),0) 
			      FROM	pinpad_transactions T 
	    INNER JOIN	gui_users U 
			        ON	U.gu_user_id = T.pt_user_id 
	    INNER JOIN	account_operations A 
			        ON	A.ao_operation_id = T.pt_operation_id 
					 WHERE	T.pt_status = 1 
						 AND	A.ao_code IN (1, 3, 17, 125) 
						 AND	A.ao_cashier_session_id = SESS_ID 
						 AND	EXISTS(SELECT 1 FROM pinpad_transactions_reconciliation WHERE ptc_id = T.pt_id AND ptc_reconciliate = 1)
						 AND	A.ao_datetime				>= @pDateFromAccountsOperation 
						 AND	A.ao_datetime				< @pDateToAccountsOperation
			 ) AS SESS_TOTAL_VOUCHERS_AMOUNT
  FROM   @_table_sessions_  
  LEFT   JOIN ( SELECT   CGS_SESSION_NAME  
                       , CGM_CASHIER_SESSION_ID  
                       , CGS_CAGE_SESSION_ID   
                  FROM   CAGE_MOVEMENTS AS CM  
                 INNER   JOIN CAGE_SESSIONS ON CM.CGM_CAGE_SESSION_ID = CGS_CAGE_SESSION_ID  
                 WHERE   CM.CGM_MOVEMENT_ID = (SELECT    TOP 1 CGM.CGM_MOVEMENT_ID  
                                                 FROM    CAGE_MOVEMENTS AS CGM   
                                                WHERE    CGM.CGM_CASHIER_SESSION_ID = CM.CGM_CASHIER_SESSION_ID)  
               ) AS X ON X.CGM_CASHIER_SESSION_ID = [@_table_sessions_].SESS_ID 
                                              
-- SESSIONS CURRENCIES INFORMATION
IF @pReportMode = 0 OR @pReportMode = 2
    SELECT   CSC_SESSION_ID
           , CSC_ISO_CODE
           , CSC_TYPE
           , CSC_BALANCE
           , CSC_COLLECTED 
      FROM   CASHIER_SESSIONS_BY_CURRENCY 
     INNER JOIN @_table_sessions_ ON SESS_ID = CSC_SESSION_ID

END -- End Procedure
GO

-- PERMISSIONS

GRANT EXECUTE ON [dbo].[SP_Cashier_Sessions_Report] TO [wggui] WITH GRANT OPTION
GO
