﻿
IF Exists(SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'Update_Meters_SalesPerHour')) BEGIN
	DROP PROCEDURE Update_Meters_SalesPerHour
END

GO

CREATE PROCEDURE [dbo].[Update_Meters_SalesPerHour]
      @pBaseHour                  DATETIME 
,     @pTerminalId                INT
,     @pGameId                    INT
,     @pTerminalDefaultPayout     MONEY
,     @pSPH_Played_Count          BIGINT
,     @pSPH_Played_Amount         MONEY
,     @pSPH_Won_Count             BIGINT
,     @pSPH_Won_Amount            MONEY
,     @pSPH_Jackpot_Amount        MONEY

AS
BEGIN 

  DECLARE @pTerminalName AS NVARCHAR(50)
  DECLARE @pGameName AS NVARCHAR(50)
  DECLARE @payout AS MONEY 

  IF @pGameId = 0
  BEGIN
    -- Get game id from sales per hour
    SELECT   @pGameId = ISNULL(MIN(SPH_GAME_ID), 0)
      FROM   SALES_PER_HOUR
     WHERE   SPH_BASE_HOUR >= dbo.Opening(0, @pBaseHour)
       AND   SPH_BASE_HOUR < DATEADD(DAY, 1, dbo.Opening(0, @pBaseHour))
       AND   SPH_TERMINAL_ID = @pTerminalId
     
    IF @pGameId = 0
    BEGIN
      -- Get game id from terminals
      SELECT   @pGameId = ISNULL(TE_LAST_GAME_PLAYED_ID, 0)
        FROM   TERMINALS
       WHERE   TE_TERMINAL_ID = @pTerminalId
    END
    
    IF @pGameId = 0
    BEGIN
      -- Get game id from terminal game translation
		SELECT TOP 1  @pGameId = ISNULL(TGT_SOURCE_GAME_ID, 0)
		FROM   TERMINAL_GAME_TRANSLATION
		WHERE   TGT_TERMINAL_ID = @pTerminalId
		ORDER BY isnull(tgt_updated, tgt_created) DESC
    END
  END
  
  IF @pGameId = 0
  BEGIN
    RAISERROR (N'Terminal without activity can not be adjusted', 16, 10)
    RETURN
  END

  -- Get terminal name 
  SELECT   @pTerminalName = TE_NAME
         , @payout = ISNULL(TE_THEORETICAL_PAYOUT, @pTerminalDefaultPayout) 
    FROM   TERMINALS
   WHERE   TE_TERMINAL_ID = @pTerminalId

  IF NOT EXISTS ( SELECT   *
                    FROM   SALES_PER_HOUR
                   WHERE   SPH_BASE_HOUR = @pBaseHour
                     AND   SPH_TERMINAL_ID = @pTerminalId
                     AND   SPH_GAME_ID = @pGameId)
  BEGIN

    -- Get game name
    SELECT   @pGameName = GM_NAME
      FROM   GAMES
     WHERE   GM_GAME_ID = @pGameId   
     
    INSERT INTO SALES_PER_HOUR
               ( SPH_BASE_HOUR
               , SPH_TERMINAL_ID
               , SPH_TERMINAL_NAME
               , SPH_GAME_ID
               , SPH_GAME_NAME
               , SPH_PLAYED_COUNT
               , SPH_PLAYED_AMOUNT
               , SPH_WON_COUNT
               , SPH_WON_AMOUNT
               , SPH_NUM_ACTIVE_TERMINALS
               , SPH_LAST_PLAY_ID
               , SPH_THEORETICAL_WON_AMOUNT
               , SPH_JACKPOT_AMOUNT
               , SPH_PROGRESSIVE_JACKPOT_AMOUNT
               , SPH_PROGRESSIVE_JACKPOT_AMOUNT_0
               , SPH_PROGRESSIVE_PROVISION_AMOUNT)
         VALUES
               ( @pBaseHour
               , @pTerminalId
               , @pTerminalName
               , @pGameId
               , @pGameName
               , ISNULL(@pSPH_Played_Count, 0)
               , ISNULL(@pSPH_Played_Amount, 0)
               , ISNULL(@pSPH_Won_Count, 0)
               , ISNULL(@pSPH_Won_Amount, 0)
               , 0
               , 0
               , ISNULL(@pSPH_Played_Amount, 0) * @payout
               , ISNULL(@pSPH_Jackpot_Amount, 0)
               , 0
               , 0
               , 0)

  END
  ELSE
  BEGIN

    UPDATE   SALES_PER_HOUR
       SET   SPH_PLAYED_COUNT   = ISNULL(@pSPH_Played_Count, SPH_PLAYED_COUNT)
           , SPH_PLAYED_AMOUNT  = ISNULL(@pSPH_Played_Amount, SPH_PLAYED_AMOUNT)
           , SPH_WON_COUNT      = ISNULL(@pSPH_Won_Count, SPH_WON_COUNT)
           , SPH_WON_AMOUNT     = ISNULL(@pSPH_Won_Amount, SPH_WON_AMOUNT)
           , SPH_JACKPOT_AMOUNT = ISNULL(@pSPH_Jackpot_Amount, SPH_JACKPOT_AMOUNT)
           , SPH_THEORETICAL_WON_AMOUNT = CASE WHEN ISNULL(SPH_PLAYED_AMOUNT, 0) = 0
                                               THEN ISNULL(@pSPH_Played_Amount, SPH_PLAYED_AMOUNT) * @payout
                                               ELSE ISNULL(@pSPH_Played_Amount, SPH_PLAYED_AMOUNT) * (SPH_THEORETICAL_WON_AMOUNT / SPH_PLAYED_AMOUNT)
                                                END
     WHERE   SPH_BASE_HOUR = @pBaseHour
       AND   SPH_TERMINAL_ID = @pTerminalId
       AND   SPH_GAME_ID = @pGameId

  END
            
END
