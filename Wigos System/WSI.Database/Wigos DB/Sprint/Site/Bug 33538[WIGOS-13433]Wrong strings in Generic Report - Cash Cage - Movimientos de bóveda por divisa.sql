 DECLARE @_name AS NVARCHAR(MAX)
 SET @_name =	'
					<LanguageResources>
					  <NLS09>
						<Label>Cage movements for currency</Label>
					  </NLS09>
					  <NLS10>
						<Label>Movimientos de b�veda por divisa</Label>
					  </NLS10>
					</LanguageResources>
				'
IF EXISTS (SELECT * FROM report_tool_config WHERE rtc_store_name = 'GetCageMovementType_GR')
	BEGIN
		UPDATE report_tool_config
		 SET rtc_report_name = @_name 
		WHERE rtc_store_name = 'GetCageMovementType_GR'
	END

GO