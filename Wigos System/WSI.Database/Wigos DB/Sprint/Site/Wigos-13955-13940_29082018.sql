﻿IF NOT EXISTS (	SELECT gp_group_key
	FROM general_params
	WHERE gp_group_key = 'Cashier'
	AND gp_subject_key = 'Document.EnableExpirationDate') 
	
BEGIN
	INSERT INTO general_params
	VALUES ('Cashier', 'Document.EnableExpirationDate', '0') 	
END
GO

IF NOT EXISTS (	SELECT gp_group_key
	FROM general_params
	WHERE gp_group_key = 'Raffle'
	AND gp_subject_key = 'AllowBlankMinFields') 
	
BEGIN
	INSERT INTO general_params
	VALUES ('Raffle', 'AllowBlankMinFields', '0') 	
END
GO

IF NOT EXISTS (	SELECT gp_group_key
	FROM general_params
	WHERE gp_group_key = 'Account'
	AND gp_subject_key = 'PlayerPicture.AllowZoom') 
	
BEGIN
	INSERT INTO general_params
	VALUES ('Account', 'PlayerPicture.AllowZoom', '0') 	
END
GO