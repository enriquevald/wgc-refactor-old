﻿
IF  EXISTS (SELECT   *
              FROM   sys.objects
             WHERE   object_id = OBJECT_ID(N'[dbo].[Update_PointsAccountMovement]')
               AND   type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[Update_PointsAccountMovement]
GO

CREATE PROCEDURE [dbo].[Update_PointsAccountMovement]
     @pSiteId                   INT
,    @pMovementId               BIGINT
,    @pPlaySessionId            BIGINT
,    @pAccountId                BIGINT
,    @pTerminalId               INT
,    @pWcpSequenceId            BIGINT
,    @pWcpTransactionId         BIGINT
,    @pDatetime                 DATETIME
,    @pType                     INT
,    @pInitialBalance           MONEY
,    @pSubAmount                MONEY
,    @pAddAmount                MONEY
,    @pFinalBalance             MONEY
,    @pCashierId                INT
,    @pCashierName              NVARCHAR(50)
,    @pTerminalName             NVARCHAR(50)
,    @pOperationId              BIGINT
,    @pDetails                  NVARCHAR(256)
,    @pReasons                  NVARCHAR(64)
,    @pPlayerTrackingMode       INT
,    @pModifiedBucketReason     INT
,    @pErrorCode                INT             OUTPUT
,    @pPointsSequenceId         BIGINT          OUTPUT
,    @pPoints                   MONEY           OUTPUT
,    @pCenterInitialBalance     MONEY           OUTPUT
,    @pBucketId                 BIGINT          OUTPUT
AS
BEGIN
  DECLARE @LocalDelta           AS MONEY
  DECLARE @IsDelta              AS BIT
  DECLARE @CheckAccountId       AS BIGINT -- For check if exists account

  -- Output parameters
  SET @pErrorCode = 1
  SET @pPointsSequenceId = NULL
  SET @pPoints = NULL
  SET @pCenterInitialBalance = NULL
  SET @pBucketId = NULL

  -- Local variables
  SET @LocalDelta = 0
  SET @IsDelta = 1
  SET @CheckAccountId = NULL

  SET NOCOUNT ON;

  -- Set BucketId
  IF (@pType >= 1100 AND @pType <= 1599)
  BEGIN
    SET  @pBucketId = (@pType % 100) -- Last 2 dígits
  END
  ELSE
  BEGIN
    IF (@pType In (36, 37, 38, 39, 40, 41, 46, 50, 60, 61, 66, 71, 101))
      SET  @pBucketId = 1    -- BucketId.RedemptionPoints
    ELSE
      SET  @pBucketId = 2    -- BucketId.RankingLevelPoints
  END
 
  -- The Bucket does not exist in table BUCKETS
  IF NOT EXISTS(SELECT   1
                  FROM   BUCKETS
                 WHERE   BU_BUCKET_ID = @pBucketId)
  BEGIN
    SET @pErrorCode = 1
    RETURN
  END

  SELECT   @pCenterInitialBalance = CBU_VALUE
    FROM   CUSTOMER_BUCKET
   WHERE   CBU_CUSTOMER_ID        = @pAccountId
     AND   CBU_BUCKET_ID          = @pBucketId

  SET @pCenterInitialBalance = ISNULL(@pCenterInitialBalance, 0)

  SELECT   @CheckAccountId = AC_ACCOUNT_ID
    FROM   ACCOUNTS
   WHERE   AC_ACCOUNT_ID   = @pAccountId

  -- The Account does not exist
  IF (@CheckAccountId IS NULL)
  BEGIN
    SET @pErrorCode = 1
    RETURN
  END

  IF NOT EXISTS (SELECT   1
                   FROM   ACCOUNT_MOVEMENTS
                  WHERE   AM_SITE_ID     = @pSiteId
                    AND   AM_MOVEMENT_ID = @pMovementId)
  BEGIN
    INSERT INTO   ACCOUNT_MOVEMENTS
    (
        AM_SITE_ID
      , AM_MOVEMENT_ID
      , AM_PLAY_SESSION_ID
      , AM_ACCOUNT_ID
      , AM_TERMINAL_ID
      , AM_WCP_SEQUENCE_ID
      , AM_WCP_TRANSACTION_ID
      , AM_DATETIME
      , AM_TYPE
      , AM_INITIAL_BALANCE
      , AM_SUB_AMOUNT
      , AM_ADD_AMOUNT
      , AM_FINAL_BALANCE
      , AM_CASHIER_ID
      , AM_CASHIER_NAME
      , AM_TERMINAL_NAME
      , AM_OPERATION_ID
      , AM_DETAILS
      , AM_REASONS
      , AM_MODIFIED_BUCKET_REASON
    )
    VALUES
    (
        @pSiteId
      , @pMovementId
      , @pPlaySessionId
      , @pAccountId
      , @pTerminalId
      , @pWcpSequenceId
      , @pWcpTransactionId
      , @pDatetime
      , @pType
      , @pInitialBalance
      , @pSubAmount
      , @pAddAmount
      , @pFinalBalance
      , @pCashierId
      , @pCashierName
      , @pTerminalName
      , @pOperationId
      , @pDetails
      , @pReasons
      , @pModifiedBucketReason
    )

    SET @LocalDelta = ISNULL(@pAddAmount, 0) - ISNULL(@pSubAmount, 0)

    -- Points Gift Delivery/Points Gift Services/Points Status/Imported Points History
    --IF (@pType IN (39, 42, 67, 62, 73))  SET @LocalDelta = 0
    IF (@pType IN (39, 42, 62, 67))  SET @LocalDelta = 0

    -- Multisite initial points
    IF (@pType = 101) SET @IsDelta = 0

    -- Update Account
    UPDATE   ACCOUNTS
       SET   AC_MS_LAST_SITE_ID  = @pSiteId
           , AC_LAST_ACTIVITY    = GETDATE()
     WHERE   AC_ACCOUNT_ID       = @pAccountId

    IF NOT EXISTS (SELECT   1
                     FROM   CUSTOMER_BUCKET
                    WHERE   CBU_CUSTOMER_ID = @pAccountId
                      AND   CBU_BUCKET_ID   = @pBucketId)
    BEGIN
      INSERT INTO   CUSTOMER_BUCKET(CBU_CUSTOMER_ID, CBU_BUCKET_ID, CBU_VALUE, CBU_UPDATED)
           VALUES  (@pAccountId, @pBucketId, 0, GETDATE())
    END

    UPDATE   CUSTOMER_BUCKET
       SET   CBU_VALUE        = CASE WHEN (@pPlayerTrackingMode = 1)
                                         THEN CBU_VALUE
                                         ELSE (CASE WHEN (@IsDelta = 1)THEN CBU_VALUE + @LocalDelta
                                                                       ELSE @LocalDelta
                                               END)
                                END
           , CBU_UPDATED      = GETDATE()
     WHERE   CBU_CUSTOMER_ID  = @pAccountId
       AND   CBU_BUCKET_ID    = @pBucketId

    -- TODO BUCKET_HISTORY
  END

  SET @pErrorCode = 0

  -- Get sequence ID
  SELECT   @pPointsSequenceId = AC_MS_POINTS_SEQ_ID
    FROM   ACCOUNTS
   WHERE   AC_ACCOUNT_ID      = @pAccountId

  -- Get Bucket Value
  SELECT   @pPoints           = CBU_VALUE
    FROM   CUSTOMER_BUCKET
   WHERE   CBU_CUSTOMER_ID    = @pAccountId
     AND   CBU_BUCKET_ID      = @pBucketId
     
  SET @pPoints = ISNULL(@pPoints, 0)

  IF (@pPlayerTrackingMode = 1)
  BEGIN
    SET @pPointsSequenceId = ISNULL(@pPointsSequenceId, 0)
  END

END
GO

