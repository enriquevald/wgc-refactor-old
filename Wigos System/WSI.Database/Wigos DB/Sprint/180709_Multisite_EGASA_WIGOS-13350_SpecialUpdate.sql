﻿/*EGASA: To activate in master profiles, menu Gaming Table permissions*/
IF NOT EXISTS (SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Features' AND GP_SUBJECT_KEY = 'GamingTables')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE, GP_MS_DOWNLOAD_TYPE) VALUES ('Features', 'GamingTables', '1', 0)
ELSE
  UPDATE GENERAL_PARAMS SET GP_KEY_VALUE='1' WHERE GP_GROUP_KEY = 'Features' AND GP_SUBJECT_KEY = 'GamingTables'
GO

IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cage' AND GP_SUBJECT_KEY = 'Enabled')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE, GP_MS_DOWNLOAD_TYPE) VALUES ('Cage', 'Enabled', '1', 0)
ELSE
  UPDATE GENERAL_PARAMS SET GP_KEY_VALUE='1' WHERE GP_GROUP_KEY = 'Cage' AND GP_SUBJECT_KEY = 'Enabled'
GO

IF EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'GamingTables' AND GP_SUBJECT_KEY = 'Mode')
  UPDATE GENERAL_PARAMS SET GP_KEY_VALUE='1' WHERE GP_GROUP_KEY = 'GamingTables' AND GP_SUBJECT_KEY = 'Mode'
GO

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'GamingTables' AND GP_SUBJECT_KEY = 'Enabled')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE, GP_MS_DOWNLOAD_TYPE) VALUES ('GamingTables', 'Enabled', '1', 0)
ELSE
  UPDATE GENERAL_PARAMS SET GP_KEY_VALUE='1' WHERE GP_GROUP_KEY = 'GamingTables' AND GP_SUBJECT_KEY = 'Enabled'
GO
