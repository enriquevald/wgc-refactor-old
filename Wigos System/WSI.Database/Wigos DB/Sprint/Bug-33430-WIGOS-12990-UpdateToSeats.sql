﻿  IF (EXISTS (SELECT * 
                FROM INFORMATION_SCHEMA.TABLES 
                WHERE TABLE_NAME = 'gaming_tables'))
  BEGIN
    -- Update only RelativeLocationX
    UPDATE gaming_tables
        SET gt_design_xml.modify('replace value of (/TableDesign/GamingSeatProperties/RelativeLocationX/text())[1] with "-325"')
    WHERE [gt_design_xml].value('(/TableDesign/GamingSeatProperties/RelativeLocationX/node())[1]', 'nvarchar(max)') = '-321'
      and [gt_design_xml].value('(/TableDesign/GamingSeatProperties/RelativeLocationY/node())[1]', 'nvarchar(max)') = '-78'
      and [gt_design_xml].value('(/TableDesign/GamingSeatProperties/SeatPosition/node())[1]', 'nvarchar(max)') = '1' 
      and gt_num_seats = 1

    -- Update only RelativeLocationY
    UPDATE gaming_tables
        SET gt_design_xml.modify('replace value of (/TableDesign/GamingSeatProperties/RelativeLocationY/text())[1] with "-28"')
    WHERE [gt_design_xml].value('(/TableDesign/GamingSeatProperties/RelativeLocationY/node())[1]', 'nvarchar(max)') = '-78'
      and [gt_design_xml].value('(/TableDesign/GamingSeatProperties/SeatPosition/node())[1]', 'nvarchar(max)') = '1' 
      and [gt_design_xml].value('(/TableDesign/GamingSeatProperties/RelativeLocationX/node())[1]', 'nvarchar(max)') = '-325'
      and gt_num_seats = 1

  END

  GO