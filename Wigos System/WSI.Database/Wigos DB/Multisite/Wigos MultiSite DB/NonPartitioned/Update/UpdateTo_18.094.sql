/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_100]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 93;

SET @New_ReleaseId = 94;
SET @New_ScriptName = N'UpdateTo_18.094.sql';
SET @New_Description = N'Cashier Movements Grouped by Hour';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/

IF NOT EXISTS (SELECT * FROM SYS.COLUMNS WHERE OBJECT_ID = OBJECT_ID(N'[DBO].[CASHIER_MOVEMENTS_GROUPED_BY_HOUR]') AND NAME = 'cm_cage_currency_type')
  ALTER TABLE [dbo].[CASHIER_MOVEMENTS_GROUPED_BY_HOUR] ADD cm_cage_currency_type INT NOT NULL DEFAULT 0
GO 

IF  EXISTS (SELECT * FROM SYS.INDEXES WHERE OBJECT_ID = OBJECT_ID(N'[DBO].[cashier_movements_grouped_by_hour]') AND NAME = N'IX_cmgh_site_id_date_type_sub_type_currency_iso_code_currency_denomination')
  DROP INDEX [IX_cmgh_site_id_date_type_sub_type_currency_iso_code_currency_denomination] ON [dbo].[cashier_movements_grouped_by_hour] WITH ( ONLINE = OFF )
GO

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[cashier_movements_grouped_by_hour]') AND name = N'IX_cmgh_site_id_date_type_sub_type_currency_iso_code_currency_type_currency_denomination')
    DROP INDEX [IX_cmgh_site_id_date_type_sub_type_currency_iso_code_currency_type_currency_denomination] ON [dbo].[cashier_movements_grouped_by_hour] WITH ( ONLINE = OFF )
GO

/****** INDEXES ******/

CREATE NONCLUSTERED INDEX [IX_cmgh_site_id_date_type_sub_type_currency_iso_code_currency_type_currency_denomination] ON [dbo].[cashier_movements_grouped_by_hour] 
(
  [cm_site_id] ASC,
  [cm_date] ASC,
  [cm_type] ASC,
  [cm_sub_type] ASC,
  [cm_currency_iso_code] ASC,
  [cm_cage_currency_type] ASC,
  [cm_currency_denomination] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

/****** RECORDS ******/

/****** STORED *******/

-- MultiSite_CashierMovementsGroupedByHour
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MultiSite_CashierMovementsGroupedByHour]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[MultiSite_CashierMovementsGroupedByHour]
GO
CREATE PROCEDURE [dbo].[MultiSite_CashierMovementsGroupedByHour]        
         @pSiteId    NVARCHAR(MAX)
        ,@pDateFrom  DATETIME
        ,@pDateTo    DATETIME       
  AS
BEGIN 
DECLARE @_sql NVARCHAR(MAX)

  IF (@pSiteId IS NOT NULL)
       SET @_sql = 'SELECT   CM_SITE_ID'
  ELSE
      SET @_sql = 'SELECT   NULL'
 
      SET @_sql   = @_sql + '
            , CM_TYPE
            , CM_SUB_TYPE
            , CM_TYPE_COUNT              
            , CM_CURRENCY_ISO_CODE
            , CM_CURRENCY_DENOMINATION
            , CM_SUB_AMOUNT
            , CM_ADD_AMOUNT
            , CM_AUX_AMOUNT
            , CM_INITIAL_BALANCE
            , CM_FINAL_BALANCE
            , CM_CAGE_CURRENCY_TYPE
       FROM   CASHIER_MOVEMENTS_GROUPED_BY_HOUR 
      WHERE   CM_DATE >= ''' + CONVERT(VARCHAR, @pDateFrom, 21) + '''  
        AND   CM_DATE < ''' + CONVERT(VARCHAR, @pDateTo, 21) +''''
    
    IF (@pSiteId IS NOT NULL)
        SET @_sql = @_sql + ' AND CM_SITE_ID IN (' + @pSiteId + ')'
      
    EXEC sp_executesql @_sql
          
END -- MultiSite_CashierMovementsGroupedByHour
GO

-- Update_CashierMovementsGrupedByHour
GRANT EXECUTE ON [dbo].[MultiSite_CashierMovementsGroupedByHour] TO [wggui] WITH GRANT OPTION
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Update_CashierMovementsGrupedByHour]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[Update_CashierMovementsGrupedByHour]
GO
 
CREATE PROCEDURE  [dbo].[Update_CashierMovementsGrupedByHour]
                  @pSiteId               INT  
                , @pDate                 DATETIME
                , @pType                 INT
                , @pSubType              INT
                , @pCurrencyIsoCode      NVARCHAR(3)
                , @pCurrencyDenomination MONEY
                , @pTypeCount            INT
                , @pSubAmount            MONEY
                , @pAddAmount            MONEY
                , @pAuxAmount            MONEY
                , @pInitialBalance       MONEY
                , @pFinalBalance         MONEY
                , @pTimeStamp            BIGINT
                , @pUniqueId             BIGINT
                , @pCageCurrencyType     INT
AS
BEGIN   

  IF EXISTS (SELECT   1 
               FROM   CASHIER_MOVEMENTS_GROUPED_BY_HOUR
              WHERE   CM_SITE_ID            = @pSiteId 
                AND   CM_UNIQUE_ID            = @pUniqueId)
  BEGIN
      UPDATE   CASHIER_MOVEMENTS_GROUPED_BY_HOUR                   
         SET   CM_DATE                  = @pDate                   
             , CM_TYPE                  = @pType                   
             , CM_SUB_TYPE              = @pSubType                
             , CM_CURRENCY_ISO_CODE     = @pCurrencyIsoCode        
             , CM_CURRENCY_DENOMINATION = @pCurrencyDenomination   
             , CM_TYPE_COUNT            = @pTypeCount              
             , CM_SUB_AMOUNT            = @pSubAmount              
             , CM_ADD_AMOUNT            = @pAddAmount              
             , CM_AUX_AMOUNT            = @pAuxAmount              
             , CM_INITIAL_BALANCE       = @pInitialBalance         
             , CM_FINAL_BALANCE         = @pFinalBalance           
             , CM_TIMESTAMP             = @pTimeStamp
             , CM_CAGE_CURRENCY_TYPE    = ISNULL(@pCageCurrencyType, 0)        
       WHERE   CM_SITE_ID        = @pSiteId                        
         AND   CM_UNIQUE_ID        = @pUniqueId
  END
  ELSE
  BEGIN
      INSERT INTO   CASHIER_MOVEMENTS_GROUPED_BY_HOUR
                  ( CM_SITE_ID
                  , CM_DATE
                  , CM_TYPE
                  , CM_SUB_TYPE
                  , CM_CURRENCY_ISO_CODE
                  , CM_CURRENCY_DENOMINATION
                  , CM_TYPE_COUNT
                  , CM_SUB_AMOUNT
                  , CM_ADD_AMOUNT
                  , CM_AUX_AMOUNT
                  , CM_INITIAL_BALANCE
                  , CM_FINAL_BALANCE
                  , CM_TIMESTAMP 
                  , CM_UNIQUE_ID
                  , CM_CAGE_CURRENCY_TYPE )
          VALUES
                  ( @pSiteId    
                  , @pDate  
                  , @pType 
                  , @pSubType 
                  , @pCurrencyIsoCode 
                  , @pCurrencyDenomination 
                  , @pTypeCount 
                  , @pSubAmount 
                  , @pAddAmount 
                  , @pAuxAmount 
                  , @pInitialBalance 
                  , @pFinalBalance 
                  , @pTimeStamp  
                  , @pUniqueId
                  , ISNULL(@pCageCurrencyType, 0))
  END
END
GO 

-- Update_CashierMovementsGrupedByHourForOldVersion
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Update_CashierMovementsGrupedByHourForOldVersion]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[Update_CashierMovementsGrupedByHourForOldVersion]
GO
 
CREATE PROCEDURE  [dbo].[Update_CashierMovementsGrupedByHourForOldVersion]
                  @pSiteId               INT  
                , @pDate                 DATETIME
                , @pType                 INT
                , @pSubType              INT
                , @pCurrencyIsoCode      NVARCHAR(3)
                , @pCurrencyDenomination MONEY
                , @pTypeCount            INT
                , @pSubAmount            MONEY
                , @pAddAmount            MONEY
                , @pAuxAmount            MONEY
                , @pInitialBalance       MONEY
                , @pFinalBalance         MONEY
                , @pTimeStamp            BIGINT
                , @pUniqueId             BIGINT
AS
BEGIN   

  IF EXISTS (SELECT   1 
               FROM   CASHIER_MOVEMENTS_GROUPED_BY_HOUR
              WHERE   CM_SITE_ID      = @pSiteId 
                AND   CM_UNIQUE_ID    = @pUniqueId )
  BEGIN
      UPDATE   CASHIER_MOVEMENTS_GROUPED_BY_HOUR                   
         SET   CM_DATE                  = @pDate                   
             , CM_TYPE                  = @pType                   
             , CM_SUB_TYPE              = @pSubType                
             , CM_CURRENCY_ISO_CODE     = @pCurrencyIsoCode        
             , CM_CURRENCY_DENOMINATION = @pCurrencyDenomination   
             , CM_TYPE_COUNT            = @pTypeCount              
             , CM_SUB_AMOUNT            = @pSubAmount              
             , CM_ADD_AMOUNT            = @pAddAmount              
             , CM_AUX_AMOUNT            = @pAuxAmount              
             , CM_INITIAL_BALANCE       = @pInitialBalance         
             , CM_FINAL_BALANCE         = @pFinalBalance           
             , CM_TIMESTAMP             = @pTimeStamp              
       WHERE   CM_SITE_ID    = @pSiteId                        
         AND   CM_UNIQUE_ID  = @pUniqueId 
  END
  ELSE
  BEGIN
      INSERT INTO   CASHIER_MOVEMENTS_GROUPED_BY_HOUR
                  ( CM_SITE_ID
                  , CM_DATE
                  , CM_TYPE
                  , CM_SUB_TYPE
                  , CM_CURRENCY_ISO_CODE
                  , CM_CURRENCY_DENOMINATION
                  , CM_TYPE_COUNT
                  , CM_SUB_AMOUNT
                  , CM_ADD_AMOUNT
                  , CM_AUX_AMOUNT
                  , CM_INITIAL_BALANCE
                  , CM_FINAL_BALANCE
                  , CM_TIMESTAMP 
                  , CM_UNIQUE_ID )
          VALUES
                  ( @pSiteId    
                  , @pDate  
                  , @pType 
                  , @pSubType 
                  , @pCurrencyIsoCode 
                  , @pCurrencyDenomination 
                  , @pTypeCount 
                  , @pSubAmount 
                  , @pAddAmount 
                  , @pAuxAmount 
                  , @pInitialBalance 
                  , @pFinalBalance 
                  , @pTimeStamp  
                  , @pUniqueId )
  END
END
GO 

