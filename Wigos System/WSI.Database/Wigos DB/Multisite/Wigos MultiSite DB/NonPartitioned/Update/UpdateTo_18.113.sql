/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_100]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 112;

SET @New_ReleaseId = 113;
SET @New_ScriptName = N'UpdateTo_18.113.sql';
SET @New_Description = N'New release v03.007.0008'; 

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/**** GENERAL PARAM *****/
 
-- Days that meters can be changed
IF NOT EXISTS(SELECT 1 
				FROM GENERAL_PARAMS 
				WHERE GP_GROUP_KEY = 'SasMeter' 
				AND GP_SUBJECT_KEY = 'MetersHistory.MaxWorkingDaysAllowedEdit'
			 )
BEGIN
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
  VALUES('SasMeter', 'MetersHistory.MaxWorkingDaysAllowedEdit', 7)
END 

/******* TABLES  *******/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sas_meters_adjustments]') AND type in (N'U'))
	DROP TABLE [dbo].[sas_meters_adjustments]
GO


/****** Object:  Table [dbo].[sas_meters_adjustments]    Script Date: 10/29/2017 13:05:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[sas_meters_adjustments](
	[tma_gaming_day] [datetime] NOT NULL,
	[tma_site_id] [int] NOT NULL,
	[tma_terminal_id] [int] NOT NULL,
	[tma_meter_code] [int] NOT NULL,
	[tma_game_id] [int] NOT NULL,
	[tma_denomination] [int] NOT NULL,
	[tma_unique_id] [bigint] IDENTITY(1,1) NOT NULL,
	[tma_datetime] [datetime] NOT NULL,
	[tma_type] [int] NOT NULL,
	[tma_sub_type] [int] NOT NULL,
	[tma_old_initial_value] [bigint] NOT NULL,
	[tma_old_final_value] [bigint] NOT NULL,
	[tma_old_delta_value] [bigint] NOT NULL,
	[tma_new_initial_value] [bigint] NOT NULL,
	[tma_new_final_value] [bigint] NOT NULL,
	[tma_new_delta_value] [bigint] NOT NULL,
	[tma_user_id] [int] NOT NULL,
	[tma_reason] [int] NOT NULL,
	[tma_remarks] [nvarchar](512) NULL,
 CONSTRAINT [PK_sas_meters_adjustments] PRIMARY KEY CLUSTERED 
(
	[tma_gaming_day] ASC,
	[tma_site_id] ASC,
	[tma_terminal_id] ASC,
	[tma_meter_code] ASC,
	[tma_game_id] ASC,
	[tma_denomination] ASC,
	[tma_unique_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

/******* RECORDS *******/

/******* PROCEDURES *******/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Update_TerminalSasMeterHistory]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Update_TerminalSasMeterHistory]
GO


/****** Object:  StoredProcedure [dbo].[Update_TerminalSasMeterHistory]    Script Date: 10/29/2017 13:02:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[Update_TerminalSasMeterHistory]
    @pSiteId          Int
  , @pTerminaId       Int
  , @pMeterCode       Int
  , @pGameId          Int
  , @pDenomination    Money
  , @pType            Int
  , @pDateTime        DateTime
  , @pMeterIniValue   BigInt
  , @pMeterFinValue   BigInt
  , @pMeterIncrement  BigInt
AS
BEGIN

  IF NOT EXISTS (SELECT 1 
                   FROM TERMINAL_SAS_METERS_HISTORY 
                  WHERE TSMH_SITE_ID = @pSiteId
                    AND TSMH_TERMINAL_ID  = @pTerminaId
                    AND TSMH_METER_CODE   = @pMeterCode
                    AND TSMH_GAME_ID      = @pGameId
                    AND TSMH_DENOMINATION = @pDenomination
                    AND TSMH_TYPE         = @pType
                    AND TSMH_DATETIME     = @pDateTime )
  BEGIN
    INSERT INTO TERMINAL_SAS_METERS_HISTORY
              ( TSMH_SITE_ID
              , TSMH_TERMINAL_ID  
              , TSMH_METER_CODE  
              , TSMH_GAME_ID  
              , TSMH_DENOMINATION  
              , TSMH_TYPE  
              , TSMH_DATETIME  
              , TSMH_METER_INI_VALUE  
              , TSMH_METER_FIN_VALUE  
              , TSMH_METER_INCREMENT  
              , TSMH_RAW_METER_INCREMENT  
              , TSMH_LAST_REPORTED  )
       VALUES
             (  @pSiteId
              , @pTerminaId     
              , @pMeterCode     
              , @pGameId        
              , @pDenomination  
              , @pType          
              , @pDateTime      
              , @pMeterIniValue 
              , @pMeterFinValue 
              , @pMeterIncrement
              , 0
              , NULL)
  END  
  ELSE
  BEGIN

    UPDATE   TERMINAL_SAS_METERS_HISTORY
       SET   TSMH_METER_INI_VALUE = @pMeterIniValue
           , TSMH_METER_FIN_VALUE = @pMeterFinValue
           , TSMH_METER_INCREMENT = @pMeterIncrement
     WHERE   TSMH_SITE_ID = @pSiteId
       AND   TSMH_TERMINAL_ID = @pTerminaId
       AND   TSMH_METER_CODE = @pMeterCode
       AND   TSMH_GAME_ID = @pGameId
       AND   TSMH_DENOMINATION = @pDenomination
       AND   TSMH_TYPE = @pType
       AND   TSMH_DATETIME = @pDateTime
  END
END
GO

GRANT EXECUTE ON [Update_TerminalSasMeterHistory] TO [wggui] WITH GRANT OPTION
GO


/******* TRIGGERS *******/
