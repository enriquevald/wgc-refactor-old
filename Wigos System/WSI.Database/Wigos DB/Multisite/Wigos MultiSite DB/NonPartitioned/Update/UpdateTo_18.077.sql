/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_100]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 76;

SET @New_ReleaseId = 77;
SET @New_ScriptName = N'UpdateTo_18.077.sql';
SET @New_Description = N'Update alarms groups name';
/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[play_sessions]') and name = 'ps_aux_ft_re_cash_in')
  ALTER TABLE [dbo].[play_sessions] ADD ps_aux_ft_re_cash_in MONEY NULL;
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[play_sessions]') and name = 'ps_aux_ft_nr_cash_in')
  ALTER TABLE [dbo].[play_sessions] ADD ps_aux_ft_nr_cash_in MONEY NULL;
GO

IF EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[play_sessions]') and name = 'ps_total_cash_in')
  ALTER TABLE [dbo].[play_sessions] DROP COLUMN [ps_total_cash_in] 
GO

ALTER TABLE [dbo].[play_sessions] ADD [ps_total_cash_in] AS  ((((([ps_initial_balance]+[ps_cash_in])+isnull([ps_re_ticket_in],(0)))+isnull([ps_promo_re_ticket_in],(0)))+ isnull([ps_aux_ft_re_cash_in],(0))+isnull([ps_promo_nr_ticket_in],(0))+isnull([ps_aux_ft_nr_cash_in],(0))))
GO

/****** INDEXES ******/

/****** RECORDS ******/
UPDATE [alarm_groups] SET alg_name = 'Pattern' WHERE [alg_alarm_group_id] = 4 and [alg_language_id] = 9
GO

/******* STORED PROCEDURES *******/
