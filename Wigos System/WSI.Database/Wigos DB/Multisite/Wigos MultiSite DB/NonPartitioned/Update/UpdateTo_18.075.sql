/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_100]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 74;

SET @New_ReleaseId = 75;
SET @New_ScriptName = N'UpdateTo_18.075.sql';
SET @New_Description = N'New alarms catalog, new task for MS and SP modified';
/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[handpays]') AND type in (N'U'))
BEGIN
  CREATE TABLE [dbo].[handpays](
       [hp_site_id] [int] NOT NULL,
       [hp_id] [bigint]   NOT NULL,
       [hp_terminal_id] [int] NULL,
       [hp_game_base_name] [nvarchar](50) NULL,
       [hp_datetime] [datetime] NOT NULL,
       [hp_previous_meters] [datetime] NULL,
       [hp_amount] [money] NOT NULL,
       [hp_te_name] [nvarchar](50) NULL,
       [hp_te_provider_id] [nvarchar](50) NULL,
       [hp_movement_id] [bigint] NULL,
       [hp_type] [int] NOT NULL,
       [hp_play_session_id] [bigint] NULL,
       [hp_site_jackpot_index] [int] NULL,
       [hp_site_jackpot_name] [nvarchar](50) NULL,
       [hp_site_jackpot_awarded_on_terminal_id] [int] NULL,
       [hp_site_jackpot_awarded_to_account_id] [bigint] NULL,
       [hp_status] [int] NULL,
       [hp_site_jackpot_notified] [bit] NOT NULL,
       [hp_ticket_id] [bigint] NULL,
       [hp_transaction_id] [bigint] NULL,
       [hp_candidate_play_session_id] [bigint] NULL,
       [hp_candidate_prev_play_session_id] [bigint] NULL,
       [hp_long_poll_1b_data] [xml] NULL,
       [hp_progressive_id] [bigint] NULL,
       [hp_level] [int] NULL,
       [hp_status_changed] [datetime] NULL,
       [hp_tax_base_amount] [money] NULL,
       [hp_tax_amount] [money] NULL,
       [hp_tax_pct] [decimal](18, 0) NULL,
       [hp_timestamp] [timestamp] NOT NULL,
  CONSTRAINT [PK_handpays] PRIMARY KEY CLUSTERED 
  (
       [hp_site_id] ASC,
       [hp_id] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
  ) ON [PRIMARY]
  
  ALTER TABLE [dbo].[handpays] ADD  CONSTRAINT [DF_handpays_hp_datetime]  DEFAULT (getdate()) FOR [hp_datetime]
  ALTER TABLE [dbo].[handpays] ADD  CONSTRAINT [DF_handpays_hp_type]  DEFAULT ((0)) FOR [hp_type]
  ALTER TABLE [dbo].[handpays] ADD  CONSTRAINT [DF_handpays_hp_status]  DEFAULT ((0)) FOR [hp_status]
  ALTER TABLE [dbo].[handpays] ADD  CONSTRAINT [DF_handpays_hp_site_jackpot_notified]  DEFAULT ((0)) FOR [hp_site_jackpot_notified]
END
GO

/****** INDEXES ******/

/****** RECORDS ******/

INSERT INTO ALARM_CATALOG (ALCG_ALARM_CODE, ALCG_LANGUAGE_ID, ALCG_TYPE, ALCG_NAME, ALCG_DESCRIPTION, ALCG_VISIBLE)
   SELECT  589830, ALCG_LANGUAGE_ID, ALCG_TYPE, ALCG_NAME, ALCG_DESCRIPTION, ALCG_VISIBLE FROM  ALARM_CATALOG WHERE  ALCG_ALARM_CODE = 589827 
GO
INSERT INTO ALARM_CATALOG_PER_CATEGORY (ALCC_ALARM_CODE, ALCC_CATEGORY, ALCC_TYPE, ALCC_DATETIME) 
   VALUES (589830, 38, 0, GETDATE())
GO

UPDATE ALARM_CATALOG SET ALCG_NAME = 'Meter Big Increment', ALCG_DESCRIPTION = 'Meter Big Increment' WHERE ALCG_ALARM_CODE = 212994
UPDATE ALARM_CATALOG SET ALCG_NAME = 'Meter Rollover'     , ALCG_DESCRIPTION = 'Meter Rollover'      WHERE ALCG_ALARM_CODE = 131081
GO

IF NOT EXISTS ( SELECT ALCG_ALARM_CODE FROM [ALARM_CATALOG] WHERE  ALCG_ALARM_CODE = 131082 )
  BEGIN
    INSERT INTO [ALARM_CATALOG] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (131082, 9, 0, 'Meter First Time', 'Meter First Time', 1)
    INSERT INTO [ALARM_CATALOG] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (131082, 10, 0, 'Meter Primera vez', 'Meter Primera vez', 1)	
	INSERT INTO [ALARM_CATALOG_PER_CATEGORY] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (131082, 1, 0, GETDATE())
  END
GO  

UPDATE [ALARM_CATALOG] SET [alcg_name] = 'Meters reset', [ALCG_DESCRIPTION] = 'Meters reset' WHERE ALCG_ALARM_CODE = 131080 AND ALCG_LANGUAGE_ID = 9		
UPDATE [ALARM_CATALOG] SET [alcg_name] = 'Reinicio Meters', [ALCG_DESCRIPTION] = 'Reinicio Meters' WHERE ALCG_ALARM_CODE = 131080 AND ALCG_LANGUAGE_ID = 10		
GO

SELECT ST_SITE_ID AS SITE_ID INTO #CURRENT_SITES FROM MS_SITE_TASKS GROUP BY ST_SITE_ID
  
IF NOT EXISTS(SELECT ST_TASK_ID FROM MS_SITE_TASKS WHERE ST_TASK_ID = 67) -- UploadHandpays
BEGIN
  INSERT INTO MS_SITE_TASKS ( ST_TASK_ID, ST_ENABLED, ST_INTERVAL_SECONDS, ST_MAX_ROWS_TO_UPLOAD, ST_SITE_ID) 
				     SELECT           67,          1,                3600,                   100, SITE_ID     FROM #CURRENT_SITES;
END
    
DROP TABLE #CURRENT_SITES
GO

/******* STORED PROCEDURES *******/

     --------------------------------------------------------------------------------
  -- Copyright � 2013 Win Systems International
  --------------------------------------------------------------------------------
  --
  --   MODULE NAME: Update_PersonalInfo.sql.sql
  --
  --   DESCRIPTION: Update personal Information 
  --
  --        AUTHOR: Dani Dom�nguez
  --
  -- CREATION DATE: 08-MAR-2013
  --
  -- REVISION HISTORY:
  --
  -- Date        Author Description
  -- ----------- ------ ----------------------------------------------------------
  -- 08-MAR-2013 DDM    First release.  
  -- 22-MAY-2013 DDM    Fixed bugs 783,793 and 693
  --                    Added field AC_BLOCK_DESCRIPTION
  -- 28-MAY-2013 DDM    Fixed bug #803
  --                    Added field AC_EXTERNAL_REFERENCE
  -- 03-JUL-2013 DDM    Added new fields about Money Laundering
  -- 22-DIC-2014 JML    When SPACE is active, only update trackdata if account is anonymous
  -------------------------------------------------------------------------------- 

  IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Update_PersonalInfoForOldVersion]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Update_PersonalInfoForOldVersion]
  GO
  CREATE PROCEDURE [dbo].[Update_PersonalInfoForOldVersion]
    @pCallingSiteId int
  , @pAccountId bigint
  , @pTrackData nvarchar(50)
  , @pAccountCreated Datetime 
  , @pHolderName nvarchar(200)
  , @pHolderId nvarchar(20)
  , @pHolderIdType int
  , @pHolderAddress01 nvarchar(50)
  , @pHolderAddress02 nvarchar(50)
  , @pHolderAddress03 nvarchar(50)
  , @pHolderCity nvarchar(50)
  , @pHolderZip  nvarchar(10) 
  , @pHolderEmail01 nvarchar(50)
  , @pHolderEmail02 nvarchar(50)
  , @pHolderTwitter nvarchar(50)
  , @pHolderPhoneNumber01 nvarchar(20)
  , @pHolderPhoneNumber02 nvarchar(20)
  , @pHolderComments nvarchar(100)
  , @pHolderId1 nvarchar(20)
  , @pHolderId2 nvarchar(20)
  , @pHolderDocumentId1 bigint
  , @pHolderDocumentId2 bigint
  , @pHolderName1 nvarchar(50)
  , @pHolderName2 nvarchar(50)
  , @pHolderName3 nvarchar(50)
  , @pHolderGender  int
  , @pHolderMaritalStatus int
  , @pHolderBirthDate datetime
  , @pHolderWeddingDate datetime
  , @pHolderLevel int
  , @pHolderLevelNotify int
  , @pHolderLevelEntered datetime
  , @pHolderLevelExpiration datetime
  , @pPin nvarchar(12)
  , @pPinFailures int
  , @pPinLastModified datetime
  , @pBlocked bit
  , @pActivated bit
  , @pBlockReason int
  , @pHolderIsVip int
  , @pHolderTitle nvarchar(15)                       
  , @pHolderName4 nvarchar(50)                       
  , @pHolderPhoneType01  int
  , @pHolderPhoneType02  int
  , @pHolderState    nvarchar(50)                    
  , @pHolderCountry  nvarchar(50) 
  , @pUserType  int
  , @pPersonalInfoSeqId  int
  , @pPointsStatus int
  , @pDeposit money
  , @pCardPay bit
  , @pBlockDescription nvarchar(256) 
  , @pMSCreatedOnSiteId Int 
  , @pExternalReference nvarchar(50) 
  , @pHolderOccupation  nvarchar(50) 	
  , @pHolderExtNum      nvarchar(10) 
  , @pHolderNationality  Int 
  , @pHolderBirthCountry Int 
  , @pHolderFedEntity    Int 
  , @pHolderId1Type      Int -- RFC
  , @pHolderId2Type      Int -- CURP
  , @pHolderId3Type       nvarchar(50)   
  , @pHolderId3           nvarchar(20) 
  , @pHolderHasBeneficiary bit  
  , @pBeneficiaryName     nvarchar(200) 
  , @pBeneficiaryName1    nvarchar(50) 
  , @pBeneficiaryName2    nvarchar(50) 
  , @pBeneficiaryName3    nvarchar(50) 
  , @pBeneficiaryBirthDate Datetime 
  , @pBeneficiaryGender    int 
  , @pBeneficiaryOccupation nvarchar(50) 
  , @pBeneficiaryId1Type    int   -- RFC
  , @pBeneficiaryId1        nvarchar(20) 
  , @pBeneficiaryId2Type    int   -- CURP
  , @pBeneficiaryId2        nvarchar(20) 
  , @pBeneficiaryId3Type    nvarchar(50)  
  , @pBeneficiaryId3  	    nvarchar(20)   
  , @pOutputStatus           INT       OUTPUT
  , @pOutputAccountOtherId   BIGINT    OUTPUT
  AS
  BEGIN

  DECLARE @pOtherAccountId as BIGINT
  DECLARE @IsNewAccount    as BIT
  DECLARE @PreviousUserType as INT
  DECLARE @PreviousPersonalInfoSeqId as BIGINT
  DECLARE @NumAccounts as INT
  DECLARE @AcBlocked_new as BIT
  DECLARE @isELPMode01 AS BIT
  DECLARE @UpdatedTrackData as Bit
  DECLARE @NewTrackData AS nvarchar(50)

  SET @pOutputStatus = 0
  SET @pOutputAccountOtherId = 0
  SET @isELPMode01 = 0
  SET @pOtherAccountId = 0
  SET @UpdatedTrackData  = 1
    
    /* Get ELP parameter  */
  SET @isELPMode01 = (SELECT CAST(GP_KEY_VALUE AS INT) FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PlayerTracking.ExternalLoyaltyProgram' AND GP_SUBJECT_KEY = 'Mode')
  
  SET @pUserType = ISNULL(@pUserType,CASE WHEN ISNULL(@pHolderLevel,0) > 0  THEN 1 ELSE 0 END)
  
  SET @NumAccounts = (SELECT COUNT(AC_ACCOUNT_ID) FROM ACCOUNTS WHERE AC_ACCOUNT_ID <> @pAccountId AND AC_HOLDER_ID = @pHolderId)
  IF @NumAccounts > 0 
  BEGIN
    SET @pOutputStatus += 1
  END

  IF (@isELPMode01 = 1 AND @pUserType = 1 )   
  BEGIN
    -- Trackdata don't update if Space is active and account is a personal account
    SET @UpdatedTrackData = 0
  END
  
  SELECT   @pOtherAccountId = AC_ACCOUNT_ID
    FROM   ACCOUNTS 
   WHERE   AC_TRACK_DATA = @pTrackData 

  SET @pOtherAccountId = ISNULL ((SELECT AC_ACCOUNT_ID FROM ACCOUNTS WHERE AC_TRACK_DATA = @pTrackData), 0)
  IF @pOtherAccountId <> 0 AND @pOtherAccountId <> @pAccountId
  BEGIN  
    -- Don't recycle cards if Space is active and account is a personal account
    IF ( @UpdatedTrackData = 1 ) -- (@isELPMode01 = 0 ) OR (@isELPMode01 = 1 AND @pUserType = 0 )  
    BEGIN 
      UPDATE   ACCOUNTS 
         SET   AC_TRACK_DATA = '00000000000000000000-RECYCLED-' + CAST (@pOtherAccountId AS NVARCHAR) 
       WHERE   AC_ACCOUNT_ID = @pOtherAccountId

      SET @pOutputStatus += 2
      SET @pOutputAccountOtherId = @pOtherAccountId
    END
  END

  SELECT @PreviousPersonalInfoSeqId = AC_MS_PERSONAL_INFO_SEQ_ID FROM ACCOUNTS WHERE AC_ACCOUNT_ID = @pAccountId 

  IF (@PreviousPersonalInfoSeqId IS NOT NULL AND @PreviousPersonalInfoSeqId > @pPersonalInfoSeqId)
  BEGIN
    SET @pOutputStatus += 4
    SELECT @pOutputAccountOtherId = ISNULL(AC_MS_LAST_SITE_ID, 0) FROM ACCOUNTS WHERE AC_ACCOUNT_ID = @pAccountId 
  END

  SET @IsNewAccount = 0
  IF NOT EXISTS (SELECT 1 FROM ACCOUNTS WHERE ac_account_id = @pAccountId )  
  BEGIN
    SET @IsNewAccount = 1

    INSERT INTO   ACCOUNTS 
                ( AC_ACCOUNT_ID, AC_TYPE, AC_BLOCKED, AC_MS_LAST_SITE_ID, AC_CREATED) 
         VALUES ( @pAccountId,         2,          1, @pCallingSiteId   , @pAccountCreated)
  END 
  ELSE
  BEGIN  
    SELECT @PreviousUserType = AC_USER_TYPE FROM ACCOUNTS WHERE AC_ACCOUNT_ID = @pAccountId     
    IF (@PreviousUserType = 1 AND @pUserType = 0) 
    BEGIN
      -- Personal account updated with anonymous account
      SET @pOutputStatus += 8
      SELECT @pOutputAccountOtherId = ISNULL(AC_MS_LAST_SITE_ID, 0) FROM ACCOUNTS WHERE AC_ACCOUNT_ID = @pAccountId 
      
      SET @pUserType = 1
    END
  END
   
  -- Don't recycle cards if Space is active and account is a personal account
   SELECT   @AcBlocked_new = CASE WHEN (AC_BLOCK_REASON & 0x20000) = 0x20000 THEN 1 ELSE @pBlocked END
          , @NewTrackData  = CASE WHEN @UpdatedTrackData = 1 THEN @pTrackData ELSE AC_TRACK_DATA  END
     FROM   ACCOUNTS
    WHERE   AC_ACCOUNT_ID = @pAccountId

  UPDATE   ACCOUNTS                   
     SET   AC_TRACK_DATA              = @NewTrackData 
         , AC_HOLDER_NAME             = @pHolderName 
         , AC_HOLDER_ID               = @pHolderId 
         , AC_HOLDER_ID_TYPE          = @pHolderIdType 
         , AC_HOLDER_ADDRESS_01       = @pHolderAddress01 
         , AC_HOLDER_ADDRESS_02       = @pHolderAddress02 
         , AC_HOLDER_ADDRESS_03       = @pHolderAddress03 
         , AC_HOLDER_CITY             = @pHolderCity 
         , AC_HOLDER_ZIP              = @pHolderZip  
         , AC_HOLDER_EMAIL_01         = @pHolderEmail01 
         , AC_HOLDER_EMAIL_02         = @pHolderEmail02 
         , AC_HOLDER_TWITTER_ACCOUNT  = @pHolderTwitter 
         , AC_HOLDER_PHONE_NUMBER_01  = @pHolderPhoneNumber01 
         , AC_HOLDER_PHONE_NUMBER_02  = @pHolderPhoneNumber02 
         , AC_HOLDER_COMMENTS         = @pHolderComments 
         , AC_HOLDER_ID1              = @pHolderId1 
         , AC_HOLDER_ID2              = @pHolderId2 
         , AC_HOLDER_DOCUMENT_ID1     = @pHolderDocumentId1 
         , AC_HOLDER_DOCUMENT_ID2     = @pHolderDocumentId2 
         , AC_HOLDER_NAME1            = @pHolderName1 
         , AC_HOLDER_NAME2            = @pHolderName2 
         , AC_HOLDER_NAME3            = @pHolderName3 
         , AC_HOLDER_GENDER           = @pHolderGender  
         , AC_HOLDER_MARITAL_STATUS   = @pHolderMaritalStatus 
         , AC_HOLDER_BIRTH_DATE       = @pHolderBirthDate 
         , AC_HOLDER_WEDDING_DATE     = @pHolderWeddingDate 
         , AC_HOLDER_LEVEL            = CASE WHEN (@IsNewAccount = 0) AND (AC_USER_TYPE = @pUserType)  THEN AC_HOLDER_LEVEL            ELSE @pHolderLevel           END
         , AC_HOLDER_LEVEL_NOTIFY     = CASE WHEN (@IsNewAccount = 0) AND (AC_USER_TYPE = @pUserType)  THEN AC_HOLDER_LEVEL_NOTIFY     ELSE @pHolderLevelNotify     END 
         , AC_HOLDER_LEVEL_ENTERED    = CASE WHEN (@IsNewAccount = 0) AND (AC_USER_TYPE = @pUserType)  THEN AC_HOLDER_LEVEL_ENTERED    ELSE @pHolderLevelEntered    END  
         , AC_HOLDER_LEVEL_EXPIRATION = CASE WHEN (@IsNewAccount = 0) AND (AC_USER_TYPE = @pUserType)  THEN AC_HOLDER_LEVEL_EXPIRATION ELSE @pHolderLevelExpiration END   
         , AC_PIN                     = @pPin 
         , AC_PIN_FAILURES            = @pPinFailures 
         , AC_PIN_LAST_MODIFIED       = @pPinLastModified 
         , AC_BLOCKED                 = @AcBlocked_new
         , AC_ACTIVATED               = @pActivated 
         , AC_BLOCK_REASON            = CASE WHEN @AcBlocked_new = 0 THEN 0 ELSE (AC_BLOCK_REASON | dbo.BlockReason_ToNewEnumerate(@pBlockReason)) END 
         , AC_HOLDER_IS_VIP           = @pHolderIsVip 
         , AC_HOLDER_TITLE            = @pHolderTitle 
         , AC_HOLDER_NAME4            = @pHolderName4         
         , AC_HOLDER_PHONE_TYPE_01    = @pHolderPhoneType01  
         , AC_HOLDER_PHONE_TYPE_02    = @pHolderPhoneType02  
         , AC_HOLDER_STATE            = @pHolderState    
         , AC_HOLDER_COUNTRY          = @pHolderCountry   
         , AC_USER_TYPE               = CASE WHEN (AC_USER_TYPE = 1  AND @pUserType = 0) THEN AC_USER_TYPE ELSE @pUserType END -- ! personal account --> anonymous ��IS NOT POSSIBLE!!!
         , AC_POINTS_STATUS           = CASE WHEN (@pUserType = 0) THEN NULL ELSE CASE WHEN (@IsNewAccount = 0) THEN @pPointsStatus ELSE 0 END END           
         , AC_DEPOSIT                 = @pDeposit 
         , AC_CARD_PAID               = @pCardPay 
         , AC_BLOCK_DESCRIPTION       = @pBlockDescription          
         , AC_MS_CREATED_ON_SITE_ID   = ISNULL(AC_MS_CREATED_ON_SITE_ID, ISNULL(@pMSCreatedOnSiteId, @pCallingSiteId))
         , AC_EXTERNAL_REFERENCE      = ISNULL(@pExternalReference,AC_EXTERNAL_REFERENCE)
         , AC_HOLDER_OCCUPATION       = @pHolderOccupation
         , AC_HOLDER_EXT_NUM          = @pHolderExtNum
         , AC_HOLDER_NATIONALITY      = @pHolderNationality 
         , AC_HOLDER_BIRTH_COUNTRY    = @pHolderBirthCountry 
         , AC_HOLDER_FED_ENTITY       = @pHolderFedEntity
         , AC_HOLDER_ID1_TYPE         = @pHolderId1Type 
         , AC_HOLDER_ID2_TYPE         = @pHolderId2Type 
         , AC_HOLDER_ID3_TYPE         = @pHolderId3Type 
         , AC_HOLDER_ID3              = @pHolderId3
         , AC_HOLDER_HAS_BENEFICIARY  = @pHolderHasBeneficiary 
         , AC_BENEFICIARY_NAME        = @pBeneficiaryName 
         , AC_BENEFICIARY_NAME1       = @pBeneficiaryName1
         , AC_BENEFICIARY_NAME2       = @pBeneficiaryName2
         , AC_BENEFICIARY_NAME3       = @pBeneficiaryName3
         , AC_BENEFICIARY_BIRTH_DATE  = @pBeneficiaryBirthDate 
         , AC_BENEFICIARY_GENDER      = @pBeneficiaryGender 
         , AC_BENEFICIARY_OCCUPATION  = @pBeneficiaryOccupation
         , AC_BENEFICIARY_ID1_TYPE    = @pBeneficiaryId1Type 
         , AC_BENEFICIARY_ID1         = @pBeneficiaryId1 
         , AC_BENEFICIARY_ID2_TYPE    = @pBeneficiaryId2Type 
         , AC_BENEFICIARY_ID2         = @pBeneficiaryId2 
         , AC_BENEFICIARY_ID3_TYPE    = @pBeneficiaryId3Type 
         , AC_BENEFICIARY_ID3         = @pBeneficiaryId3 
   WHERE   AC_ACCOUNT_ID              = @pAccountId 
       
END
GO

  --------------------------------------------------------------------------------
  -- Copyright � 2013 Win Systems International
  --------------------------------------------------------------------------------
  --
  --   MODULE NAME: Update_PersonalInfo.sql.sql
  --
  --   DESCRIPTION: Update personal Information 
  --
  --        AUTHOR: Dani Dom�nguez
  --
  -- CREATION DATE: 08-MAR-2013
  --
  -- REVISION HISTORY:
  --
  -- Date        Author Description
  -- ----------- ------ ----------------------------------------------------------
  -- 08-MAR-2013 DDM    First release.  
  -- 22-MAY-2013 DDM    Fixed bugs 783,793 and 693
  --                    Added field AC_BLOCK_DESCRIPTION
  -- 28-MAY-2013 DDM    Fixed bug #803
  --                    Added field AC_EXTERNAL_REFERENCE
  -- 03-JUL-2013 DDM    Added new fields about Money Laundering
  -- 11-NOV-2013 JPJ    Added new fields AC_HOLDER_OCCUPATION_ID and AC_BENEFICIARY_OCCUPATION_ID
  -- 22-DIC-2014 JML    When SPACE is active, no update personalize accounts in the center, but it's posssible recycled cards
  -------------------------------------------------------------------------------- 

  IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Update_PersonalInfo]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Update_PersonalInfo]
  GO
  CREATE PROCEDURE [dbo].[Update_PersonalInfo]
    @pCallingSiteId              int
  , @pAccountId                  bigint
  , @pTrackData                  nvarchar(50)
  , @pAccountCreated             Datetime 
  , @pHolderName                 nvarchar(200)
  , @pHolderId                   nvarchar(20)
  , @pHolderIdType               int
  , @pHolderAddress01            nvarchar(50)
  , @pHolderAddress02            nvarchar(50)
  , @pHolderAddress03            nvarchar(50)
  , @pHolderCity                 nvarchar(50)
  , @pHolderZip                  nvarchar(10) 
  , @pHolderEmail01              nvarchar(50)
  , @pHolderEmail02              nvarchar(50)
  , @pHolderTwitter              nvarchar(50)
  , @pHolderPhoneNumber01        nvarchar(20)
  , @pHolderPhoneNumber02        nvarchar(20)
  , @pHolderComments             nvarchar(100)
  , @pHolderId1                  nvarchar(20)
  , @pHolderId2                  nvarchar(20)
  , @pHolderDocumentId1          bigint
  , @pHolderDocumentId2          bigint
  , @pHolderName1                nvarchar(50)
  , @pHolderName2                nvarchar(50)
  , @pHolderName3                nvarchar(50)
  , @pHolderGender               int
  , @pHolderMaritalStatus        int
  , @pHolderBirthDate            datetime
  , @pHolderWeddingDate          datetime
  , @pHolderLevel                int
  , @pHolderLevelNotify          int
  , @pHolderLevelEntered         datetime
  , @pHolderLevelExpiration      datetime
  , @pPin                        nvarchar(12)
  , @pPinFailures                int
  , @pPinLastModified            datetime
  , @pBlocked                    bit
  , @pActivated                  bit
  , @pBlockReason                int
  , @pHolderIsVip                int
  , @pHolderTitle                nvarchar(15)                       
  , @pHolderName4                nvarchar(50)                       
  , @pHolderPhoneType01          int
  , @pHolderPhoneType02          int
  , @pHolderState                nvarchar(50)                    
  , @pHolderCountry              nvarchar(50) 
  , @pUserType                   int
  , @pPersonalInfoSeqId          int
  , @pPointsStatus               int
  , @pDeposit                    money
  , @pCardPay                    bit
  , @pBlockDescription           nvarchar(256) 
  , @pMSCreatedOnSiteId          Int 
  , @pExternalReference          nvarchar(50) 
  , @pHolderOccupation           nvarchar(50) 	
  , @pHolderExtNum               nvarchar(10) 
  , @pHolderNationality          Int 
  , @pHolderBirthCountry         Int 
  , @pHolderFedEntity            Int 
  , @pHolderId1Type              Int -- RFC
  , @pHolderId2Type              Int -- CURP
  , @pHolderId3Type              nvarchar(50)   
  , @pHolderId3                  nvarchar(20) 
  , @pHolderHasBeneficiary       bit  
  , @pBeneficiaryName            nvarchar(200) 
  , @pBeneficiaryName1           nvarchar(50) 
  , @pBeneficiaryName2           nvarchar(50) 
  , @pBeneficiaryName3           nvarchar(50) 
  , @pBeneficiaryBirthDate       Datetime 
  , @pBeneficiaryGender          int 
  , @pBeneficiaryOccupation      nvarchar(50) 
  , @pBeneficiaryId1Type         int   -- RFC
  , @pBeneficiaryId1             nvarchar(20) 
  , @pBeneficiaryId2Type         int   -- CURP
  , @pBeneficiaryId2             nvarchar(20) 
  , @pBeneficiaryId3Type         nvarchar(50)  
  , @pBeneficiaryId3  	         nvarchar(20)   
  , @pHolderOccupationId         int
  , @pBeneficiaryOccupationId    int
  , @pOutputStatus               INT               OUTPUT
  , @pOutputAccountOtherId       BIGINT            OUTPUT
  AS
  BEGIN

  DECLARE @pOtherAccountId as BIGINT
  DECLARE @IsNewAccount    as BIT
  DECLARE @PreviousUserType as INT
  DECLARE @PreviousPersonalInfoSeqId as BIGINT
  DECLARE @NumAccounts as INT
  DECLARE @AcBlocked_new as BIT
  DECLARE @isELPMode01 AS BIT
  DECLARE @UpdatedTrackData as Bit
  DECLARE @NewTrackData AS nvarchar(50)
	
  SET @pOutputStatus = 0
  SET @pOutputAccountOtherId = 0
  SET @isELPMode01 = 0
  SET @pOtherAccountId = 0
  SET @UpdatedTrackData = 1
  
  /* Get ELP parameter  */
  SET @isELPMode01 = (SELECT CAST(GP_KEY_VALUE AS INT) FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PlayerTracking.ExternalLoyaltyProgram' AND GP_SUBJECT_KEY = 'Mode')
  
  SET @pUserType = ISNULL(@pUserType,CASE WHEN ISNULL(@pHolderLevel,0) > 0  THEN 1 ELSE 0 END)
  
  SET @NumAccounts = (SELECT COUNT(AC_ACCOUNT_ID) FROM ACCOUNTS WHERE AC_ACCOUNT_ID <> @pAccountId AND AC_HOLDER_ID = @pHolderId)
  IF @NumAccounts > 0 
  BEGIN
    SET @pOutputStatus += 1
  END
  
  IF (@isELPMode01 = 1 AND @pUserType = 1 )   
  BEGIN
    -- Trackdata don't update if Space is active and account is a personal account
    SET @UpdatedTrackData = 0
  END
  
  SELECT   @pOtherAccountId = AC_ACCOUNT_ID
    FROM   ACCOUNTS 
   WHERE   AC_TRACK_DATA = @pTrackData 
   
  SET @pOtherAccountId = ISNULL ((SELECT AC_ACCOUNT_ID FROM ACCOUNTS WHERE AC_TRACK_DATA = @pTrackData), 0)
  IF @pOtherAccountId <> 0 AND @pOtherAccountId <> @pAccountId
  BEGIN
    -- Don't recycle cards if Space is active and account is a personal account
    IF ( @UpdatedTrackData = 1 ) -- (@isELPMode01 = 0 ) OR (@isELPMode01 = 1 AND @pUserType = 0 )  
    BEGIN 
      UPDATE   ACCOUNTS 
         SET   AC_TRACK_DATA = '00000000000000000000-RECYCLED-' + CAST (@pOtherAccountId AS NVARCHAR) 
       WHERE   AC_ACCOUNT_ID = @pOtherAccountId

      SET @pOutputStatus += 2
      SET @pOutputAccountOtherId = @pOtherAccountId
    END    
  END

  SELECT @PreviousPersonalInfoSeqId = AC_MS_PERSONAL_INFO_SEQ_ID FROM ACCOUNTS WHERE AC_ACCOUNT_ID = @pAccountId 

  IF (@PreviousPersonalInfoSeqId IS NOT NULL AND @PreviousPersonalInfoSeqId > @pPersonalInfoSeqId)
  BEGIN
    SET @pOutputStatus += 4
    SELECT @pOutputAccountOtherId = ISNULL(AC_MS_LAST_SITE_ID, 0) FROM ACCOUNTS WHERE AC_ACCOUNT_ID = @pAccountId 
  END

  SET @IsNewAccount = 0
  IF NOT EXISTS (SELECT 1 FROM ACCOUNTS WHERE ac_account_id = @pAccountId )  
  BEGIN
    SET @IsNewAccount = 1

    INSERT INTO   ACCOUNTS 
                ( AC_ACCOUNT_ID, AC_TYPE, AC_BLOCKED, AC_MS_LAST_SITE_ID, AC_CREATED) 
         VALUES ( @pAccountId,         2,          1, @pCallingSiteId   , @pAccountCreated)
  END 
  ELSE
  BEGIN  
    SELECT @PreviousUserType = AC_USER_TYPE FROM ACCOUNTS WHERE AC_ACCOUNT_ID = @pAccountId     
    IF (@PreviousUserType = 1 AND @pUserType = 0) 
    BEGIN
      -- Personal account updated with anonymous account
      SET @pOutputStatus += 8
      SELECT @pOutputAccountOtherId = ISNULL(AC_MS_LAST_SITE_ID, 0) FROM ACCOUNTS WHERE AC_ACCOUNT_ID = @pAccountId 
      
      SET @pUserType = 1
    END
  END
  
  -- Don't recycle cards if Space is active and account is a personal account
   SELECT   @AcBlocked_new = CASE WHEN (AC_BLOCK_REASON & 0x20000) = 0x20000 THEN 1 ELSE @pBlocked END
          , @NewTrackData  = CASE WHEN @UpdatedTrackData = 1 THEN @pTrackData ELSE AC_TRACK_DATA  END
     FROM   ACCOUNTS
    WHERE   AC_ACCOUNT_ID = @pAccountId
   
  UPDATE   ACCOUNTS                   
     SET   AC_TRACK_DATA                = @NewTrackData
         , AC_HOLDER_NAME               = @pHolderName 
         , AC_HOLDER_ID                 = @pHolderId 
         , AC_HOLDER_ID_TYPE            = @pHolderIdType 
         , AC_HOLDER_ADDRESS_01         = @pHolderAddress01 
         , AC_HOLDER_ADDRESS_02         = @pHolderAddress02 
         , AC_HOLDER_ADDRESS_03         = @pHolderAddress03 
         , AC_HOLDER_CITY               = @pHolderCity 
         , AC_HOLDER_ZIP                = @pHolderZip  
         , AC_HOLDER_EMAIL_01           = @pHolderEmail01 
         , AC_HOLDER_EMAIL_02           = @pHolderEmail02 
         , AC_HOLDER_TWITTER_ACCOUNT    = @pHolderTwitter 
         , AC_HOLDER_PHONE_NUMBER_01    = @pHolderPhoneNumber01 
         , AC_HOLDER_PHONE_NUMBER_02    = @pHolderPhoneNumber02 
         , AC_HOLDER_COMMENTS           = @pHolderComments 
         , AC_HOLDER_ID1                = @pHolderId1 
         , AC_HOLDER_ID2                = @pHolderId2 
         , AC_HOLDER_DOCUMENT_ID1       = @pHolderDocumentId1 
         , AC_HOLDER_DOCUMENT_ID2       = @pHolderDocumentId2 
         , AC_HOLDER_NAME1              = @pHolderName1 
         , AC_HOLDER_NAME2              = @pHolderName2 
         , AC_HOLDER_NAME3              = @pHolderName3 
         , AC_HOLDER_GENDER             = @pHolderGender  
         , AC_HOLDER_MARITAL_STATUS     = @pHolderMaritalStatus 
         , AC_HOLDER_BIRTH_DATE         = @pHolderBirthDate 
         , AC_HOLDER_WEDDING_DATE       = @pHolderWeddingDate 
         , AC_HOLDER_LEVEL              = CASE WHEN (@IsNewAccount = 0) AND (AC_USER_TYPE = @pUserType)  THEN AC_HOLDER_LEVEL            ELSE @pHolderLevel           END
         , AC_HOLDER_LEVEL_NOTIFY       = CASE WHEN (@IsNewAccount = 0) AND (AC_USER_TYPE = @pUserType)  THEN AC_HOLDER_LEVEL_NOTIFY     ELSE @pHolderLevelNotify     END 
         , AC_HOLDER_LEVEL_ENTERED      = CASE WHEN (@IsNewAccount = 0) AND (AC_USER_TYPE = @pUserType)  THEN AC_HOLDER_LEVEL_ENTERED    ELSE @pHolderLevelEntered    END  
         , AC_HOLDER_LEVEL_EXPIRATION   = CASE WHEN (@IsNewAccount = 0) AND (AC_USER_TYPE = @pUserType)  THEN AC_HOLDER_LEVEL_EXPIRATION ELSE @pHolderLevelExpiration END   
         , AC_PIN                       = @pPin 
         , AC_PIN_FAILURES              = @pPinFailures 
         , AC_PIN_LAST_MODIFIED         = @pPinLastModified 
         , AC_BLOCKED                   = @AcBlocked_new
         , AC_ACTIVATED                 = @pActivated 
         , AC_BLOCK_REASON              = CASE WHEN @AcBlocked_new = 0 THEN 0 ELSE (AC_BLOCK_REASON | dbo.BlockReason_ToNewEnumerate(@pBlockReason)) END 
         , AC_HOLDER_IS_VIP             = @pHolderIsVip 
         , AC_HOLDER_TITLE              = @pHolderTitle 
         , AC_HOLDER_NAME4              = @pHolderName4         
         , AC_HOLDER_PHONE_TYPE_01      = @pHolderPhoneType01  
         , AC_HOLDER_PHONE_TYPE_02      = @pHolderPhoneType02  
         , AC_HOLDER_STATE              = @pHolderState    
         , AC_HOLDER_COUNTRY            = @pHolderCountry   
         , AC_USER_TYPE                 = CASE WHEN (AC_USER_TYPE = 1  AND @pUserType = 0) THEN AC_USER_TYPE ELSE @pUserType END -- ! personal account --> anonymous ��IS NOT POSSIBLE!!!
         , AC_POINTS_STATUS             = CASE WHEN (@pUserType = 0) THEN NULL ELSE CASE WHEN (@IsNewAccount = 0) THEN @pPointsStatus ELSE 0 END END           
         , AC_DEPOSIT                   = @pDeposit 
         , AC_CARD_PAID                 = @pCardPay 
         , AC_BLOCK_DESCRIPTION         = @pBlockDescription          
         , AC_MS_CREATED_ON_SITE_ID     = ISNULL(AC_MS_CREATED_ON_SITE_ID, ISNULL(@pMSCreatedOnSiteId, @pCallingSiteId))
         , AC_EXTERNAL_REFERENCE        = ISNULL(@pExternalReference, AC_EXTERNAL_REFERENCE)
         , AC_HOLDER_OCCUPATION         = @pHolderOccupation
         , AC_HOLDER_EXT_NUM            = @pHolderExtNum
         , AC_HOLDER_NATIONALITY        = @pHolderNationality 
         , AC_HOLDER_BIRTH_COUNTRY      = @pHolderBirthCountry 
         , AC_HOLDER_FED_ENTITY         = @pHolderFedEntity
         , AC_HOLDER_ID1_TYPE           = @pHolderId1Type 
         , AC_HOLDER_ID2_TYPE           = @pHolderId2Type 
         , AC_HOLDER_ID3_TYPE           = @pHolderId3Type 
         , AC_HOLDER_ID3                = @pHolderId3
         , AC_HOLDER_HAS_BENEFICIARY    = @pHolderHasBeneficiary 
         , AC_BENEFICIARY_NAME          = @pBeneficiaryName 
         , AC_BENEFICIARY_NAME1         = @pBeneficiaryName1
         , AC_BENEFICIARY_NAME2         = @pBeneficiaryName2
         , AC_BENEFICIARY_NAME3         = @pBeneficiaryName3
         , AC_BENEFICIARY_BIRTH_DATE    = @pBeneficiaryBirthDate 
         , AC_BENEFICIARY_GENDER        = @pBeneficiaryGender 
         , AC_BENEFICIARY_OCCUPATION    = @pBeneficiaryOccupation
         , AC_BENEFICIARY_ID1_TYPE      = @pBeneficiaryId1Type 
         , AC_BENEFICIARY_ID1           = @pBeneficiaryId1 
         , AC_BENEFICIARY_ID2_TYPE      = @pBeneficiaryId2Type 
         , AC_BENEFICIARY_ID2           = @pBeneficiaryId2 
         , AC_BENEFICIARY_ID3_TYPE      = @pBeneficiaryId3Type 
         , AC_BENEFICIARY_ID3           = @pBeneficiaryId3 
         , AC_HOLDER_OCCUPATION_ID      = ISNULL(@pHolderOccupationId, AC_HOLDER_OCCUPATION_ID)
         , AC_BENEFICIARY_OCCUPATION_ID = ISNULL(@pBeneficiaryOccupationId, AC_BENEFICIARY_OCCUPATION_ID)
   WHERE   AC_ACCOUNT_ID                = @pAccountId 
       
END
GO

--------------------------------------------------------------------------------
-- Copyright � 2013 Win Systems International
--------------------------------------------------------------------------------
--
--   MODULE NAME:		Update_SiteHandpays.sql
--
--   DESCRIPTION:		Update_SiteHandpays
--
--   AUTHOR:				Didac Campanals Subirats
--
--   CREATION DATE: 07-JAN-2015
--
-- REVISION HISTORY:
--
-- Date         Author     Description
-- -----------  ---------  ----------------------------------------------------------
-- 07-JAN-2015  DCS        First release.  
-- ------------ ---------  ---------------------------------------------------------- 
  
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Update_SiteHandpays]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[Update_SiteHandpays]
GO

CREATE PROCEDURE   [dbo].[Update_SiteHandpays]
                   @pSiteid													INT
								 , @pHandpayId											BIGINT
								 , @pTerminalId											INT
								 , @pGameBaseName										NVARCHAR(50)
                 , @pDatetime												DATETIME
								 , @pPreviousMeters									DATETIME
								 , @pAmount													MONEY
								 , @pTeName													NVARCHAR(50)
								 , @pTeProviderId										NVARCHAR(50)
								 , @pMovementId											BIGINT
								 , @pType														INT
								 , @pPlaySessionId									BIGINT
								 , @pSiteJackpotIndex								INT
								 , @pSiteJackpotName								NVARCHAR(50)
								 , @pSiteJackpotAwardedOnTerminalId	INT
								 , @pSiteJackpotAwardedToAccountId	BIGINT
								 , @pStatus													INT
								 , @pSiteJackpotNotified						BIT
								 , @pTicketId												BIGINT
								 , @pTransactionId									BIGINT
								 , @pCandidatePlaySessionId					BIGINT
								 , @pCandidatePrevPlaySessionId			BIGINT
								 , @pLongPoll1bData									XML
								 , @pProgressiveId									BIGINT
								 , @pLevel													INT
								 , @pStatusChanged									DATETIME
								 , @pTaxBaseAmount									MONEY
								 , @pTaxAmount											MONEY
								 , @pTaxPct													DECIMAL								 
AS
BEGIN   
  IF EXISTS (SELECT   1 
               FROM   HANDPAYS 
              WHERE   HP_SITE_ID  = @pSiteId 
                AND   HP_ID = @pHandpayId)
                
/****** Update site Handpays ******/
	BEGIN                
		     UPDATE   HANDPAYS
			      SET		HP_TERMINAL_ID													= @pTerminalId
								, HP_GAME_BASE_NAME												= @pGameBaseName
								, HP_DATETIME															= @pDatetime
								, HP_PREVIOUS_METERS											= @pPreviousMeters
								, HP_AMOUNT																= @pAmount
     						, HP_TE_NAME															= @pTeName
 								, HP_TE_PROVIDER_ID												= @pTeProviderId
 								, HP_MOVEMENT_ID													= @pMovementId
								, HP_TYPE																	= @pType
								, HP_PLAY_SESSION_ID											= @pPlaySessionId
								, HP_SITE_JACKPOT_INDEX										= @pSiteJackpotIndex
								, HP_SITE_JACKPOT_NAME										= @pSiteJackpotName
								, HP_SITE_JACKPOT_AWARDED_ON_TERMINAL_ID	= @pSiteJackpotAwardedOnTerminalId
								, HP_SITE_JACKPOT_AWARDED_TO_ACCOUNT_ID		= @pSiteJackpotAwardedToAccountId
								, HP_STATUS																= @pStatus
								, HP_SITE_JACKPOT_NOTIFIED								= @pSiteJackpotNotified
								, HP_TICKET_ID														= @pTicketId
								, HP_TRANSACTION_ID												= @pTransactionId
								, HP_CANDIDATE_PLAY_SESSION_ID						= @pCandidatePlaySessionId
								, HP_CANDIDATE_PREV_PLAY_SESSION_ID				= @pCandidatePrevPlaySessionId
								, HP_LONG_POLL_1B_DATA										= @pLongPoll1bData
								, HP_PROGRESSIVE_ID												= @pProgressiveId
								, HP_LEVEL																= @pLevel
								, HP_STATUS_CHANGED												= @pStatusChanged
								, HP_TAX_BASE_AMOUNT											= @pTaxBaseAmount
								, HP_TAX_AMOUNT														= @pTaxAmount
								, HP_TAX_PCT															= @pTaxPct				       
				  WHERE   HP_SITE_ID															= @pSiteId 
			      AND   HP_ID																		= @pHandpayId
  END
ELSE
	BEGIN  
/****** Insert site Handpays ******/  

    INSERT INTO   HANDPAYS
								( HP_SITE_ID
								, HP_ID
								, HP_TERMINAL_ID
								, HP_GAME_BASE_NAME
								, HP_DATETIME
								, HP_PREVIOUS_METERS
								, HP_AMOUNT
								, HP_TE_NAME
								, HP_TE_PROVIDER_ID
								, HP_MOVEMENT_ID
								, HP_TYPE
								, HP_PLAY_SESSION_ID
								, HP_SITE_JACKPOT_INDEX	
								, HP_SITE_JACKPOT_NAME
								, HP_SITE_JACKPOT_AWARDED_ON_TERMINAL_ID
								, HP_SITE_JACKPOT_AWARDED_TO_ACCOUNT_ID
								, HP_STATUS
								, HP_SITE_JACKPOT_NOTIFIED
								, HP_TICKET_ID
								, HP_TRANSACTION_ID
								, HP_CANDIDATE_PLAY_SESSION_ID
								, HP_CANDIDATE_PREV_PLAY_SESSION_ID
								, HP_LONG_POLL_1B_DATA
								, HP_PROGRESSIVE_ID
								, HP_LEVEL
								, HP_STATUS_CHANGED
								, HP_TAX_BASE_AMOUNT
								, HP_TAX_AMOUNT
								, HP_TAX_PCT
								)
				 VALUES ( @pSiteid  
								, @pHandpayId  
								, @pTerminalId  
								, @pGameBaseName  
								, @pDatetime  
								, @pPreviousMeters  
								, @pAmount  
								, @pTeName  
								, @pTeProviderId  
								, @pMovementId  
								, @pType  
								, @pPlaySessionId  
								, @pSiteJackpotIndex  
								, @pSiteJackpotName  
								, @pSiteJackpotAwardedOnTerminalId  
								, @pSiteJackpotAwardedToAccountId  
								, @pStatus  
								, @pSiteJackpotNotified  
								, @pTicketId  
								, @pTransactionId  
								, @pCandidatePlaySessionId  
								, @pCandidatePrevPlaySessionId  
								, @pLongPoll1bData  
								, @pProgressiveId  
								, @pLevel  
								, @pStatusChanged  
								, @pTaxBaseAmount 
								, @pTaxAmount
								, @pTaxPct
								)
														
	END
	      
END -- Update_SiteHandpays
GO


