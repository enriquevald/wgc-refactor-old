/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_100]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 129;

SET @New_ReleaseId = 130;
SET @New_ScriptName = N'UpdateTo_18.130.sql';
SET @New_Description = N'New release v03.008.0008'; 

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO


/**** GENERAL PARAM *****/

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'WigosGUI' AND GP_SUBJECT_KEY = 'CageMovements.MonitorRefreshTime')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY, GP_KEY_VALUE, GP_MS_DOWNLOAD_TYPE) VALUES ('WigosGUI','CageMovements.MonitorRefreshTime','5', 3);
GO

/******* TABLES  *******/
IF EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[sites_requests]') and name = 'sr_computed_id')
BEGIN

    IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[sites_requests]') AND name = N'IX_avoid_duplicates')
    BEGIN
       DROP INDEX [IX_avoid_duplicates] ON [dbo].[sites_requests] WITH ( ONLINE = OFF )
    END

    ALTER TABLE dbo.sites_requests DROP COLUMN	sr_computed_id  
    ALTER TABLE dbo.sites_requests ADD
	    sr_computed_id  AS case when [sr_input_data] like '%points%' then (SUBSTRING([sr_input_data],PATINDEX('%<account_id>%',[sr_input_data])+12,PATINDEX('%</account_id>%',[sr_input_data])-PATINDEX('%<account_id>%',[sr_input_data])-12)) else [sr_mr_unique_id] end

    CREATE UNIQUE NONCLUSTERED INDEX [IX_avoid_duplicates] ON [dbo].[sites_requests]
    (
	    [sr_site_id] ASC,
	    [sr_computed_id] ASC
    )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

END
GO

/******* RECORDS *******/


/******* PROCEDURES *******/


/******* TRIGGERS *******/




