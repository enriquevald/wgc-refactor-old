/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_100]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 64;

SET @New_ReleaseId = 65;
SET @New_ScriptName = N'UpdateTo_18.065.sql';
SET @New_Description = N'Changes for alarm_categories';
/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/

/****** INDEXES ******/

/****** RECORDS ******/

INSERT [dbo].[alarm_categories] ([alc_category_id], [alc_language_id], [alc_alarm_group_id], [alc_type], [alc_name], [alc_description], [alc_visible]) VALUES (40, 10, 3, 0, N'Operaciones', N'', 1)
update alarm_categories set alc_name = 'Control de acceso' where alc_category_id = 23 AND alc_language_id = 10
INSERT [dbo].[alarm_categories] ([alc_category_id], [alc_language_id], [alc_alarm_group_id], [alc_type], [alc_name], [alc_description], [alc_visible]) VALUES (40,  9, 3, 0, N'Operations', N'', 1)
update alarm_categories set alc_name = 'Access control' where alc_category_id = 23 AND alc_language_id = 9
update alarm_categories set alc_alarm_group_id = 3 where alc_category_id = 23 
update alarm_catalog_per_category SET alcc_category = 40 where alcc_alarm_code in (262146, 262147, 262149, 262150, 262151, 393247)
GO

/******* STORED PROCEDURES *******/
