/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_100]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 82;

SET @New_ReleaseId = 83;
SET @New_ScriptName = N'UpdateTo_18.083.sql';
SET @New_Description = N'Alarms catalog for coin collection';
/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/

/****** INDEXES ******/

/****** RECORDS ******/

-- TerminalSystem_GameMeterBigIncrement_ByMeter = 0x000A0000,     // Warn => 0x000A0000 + MeterCode
-- TerminalSystem_MachineMeterBigIncrement_ByMeter = 0x000B0000,  // Warn => 0x000B0000 + MeterCode

-- Meters:
-- 0x0005 Games played / Jugadas
-- 0x0000 Total coin in credits / Monto jugado
-- 0x0006 Games won / Jugadas ganadas
-- 0x0001 Total coin out credits / Monto ganado
-- 0x0002 Total jackpot credits / Total cr�ditos jackpot
-- 0x1000 Total progressive jackpot credits / Total cr�ditos jackpot progresivo


CREATE TABLE #ALARMS (ALARM_CODE INT NOT NULL, EN NVARCHAR(350), SP NVARCHAR(350))
 INSERT INTO #ALARMS (ALARM_CODE,EN,SP) VALUES (CONVERT(INT, 0x000A0000) + CONVERT(INT, 0x0005), 'Big increment in MM: Games played'                      , 'Salto de contador en MM: Jugadas')
 INSERT INTO #ALARMS (ALARM_CODE,EN,SP) VALUES (CONVERT(INT, 0x000A0000) + CONVERT(INT, 0x0000), 'Big increment in MM: Total coin in credits'             , 'Salto de contador en MM: Monto jugado')
 INSERT INTO #ALARMS (ALARM_CODE,EN,SP) VALUES (CONVERT(INT, 0x000A0000) + CONVERT(INT, 0x0006), 'Big increment in MM: Games won'                         , 'Salto de contador en MM: Jugadas ganadas')
 INSERT INTO #ALARMS (ALARM_CODE,EN,SP) VALUES (CONVERT(INT, 0x000A0000) + CONVERT(INT, 0x0001), 'Big increment in MM: Total coin out credits'            , 'Salto de contador en MM: Monto ganado')
 INSERT INTO #ALARMS (ALARM_CODE,EN,SP) VALUES (CONVERT(INT, 0x000A0000) + CONVERT(INT, 0x0002), 'Big increment in MM: Total jackpot credits'             , 'Salto de contador en MM: Total cr�ditos jackpot')
 INSERT INTO #ALARMS (ALARM_CODE,EN,SP) VALUES (CONVERT(INT, 0x000A0000) + CONVERT(INT, 0x1000), 'Big increment in MM: Total prog. jackpot credits'       , 'Salto de contador en MM: Total cr�d. jackpot prog.')
                                                                                                       
 INSERT INTO #ALARMS (ALARM_CODE,EN,SP) VALUES (CONVERT(INT, 0x000B0000) + CONVERT(INT, 0x0005), 'Big increment in GM: Games played'                      , 'Salto de contador en GM: Jugadas')
 INSERT INTO #ALARMS (ALARM_CODE,EN,SP) VALUES (CONVERT(INT, 0x000B0000) + CONVERT(INT, 0x0000), 'Big increment in GM: Total coin in credits'             , 'Salto de contador en GM: Monto jugado')
 INSERT INTO #ALARMS (ALARM_CODE,EN,SP) VALUES (CONVERT(INT, 0x000B0000) + CONVERT(INT, 0x0006), 'Big increment in GM: Games won'                         , 'Salto de contador en GM: Jugadas ganadas')
 INSERT INTO #ALARMS (ALARM_CODE,EN,SP) VALUES (CONVERT(INT, 0x000B0000) + CONVERT(INT, 0x0001), 'Big increment in GM: Total coin out credits'            , 'Salto de contador en GM: Monto ganado')
 INSERT INTO #ALARMS (ALARM_CODE,EN,SP) VALUES (CONVERT(INT, 0x000B0000) + CONVERT(INT, 0x0002), 'Big increment in GM: Total jackpot credits'             , 'Salto de contador en GM: Total cr�ditos jackpot')
 INSERT INTO #ALARMS (ALARM_CODE,EN,SP) VALUES (CONVERT(INT, 0x000B0000) + CONVERT(INT, 0x1000), 'Big increment in GM: Total prog. jackpot credits'       , 'Salto de contador en GM: Total cr�d. jackpot prog.')

--DELETE from [alarm_catalog] where [alcg_alarm_code] IN (select alarm_code from #ALARMS)
--DELETE from [alarm_catalog_per_category] where [alcc_alarm_code] IN (select alarm_code from #ALARMS)

IF NOT EXISTS (SELECT [alcg_alarm_code] from [dbo].[alarm_catalog] WHERE [alcg_alarm_code] = CONVERT(INT, 0x000A0000) + CONVERT(INT, 0x0005) AND [alcg_language_id] = 10)
  INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) 
                         SELECT          ALARM_CODE,                 10,           0,          SP,                 SP,              1 FROM #ALARMS ORDER BY ALARM_CODE DESC
GO

IF NOT EXISTS (SELECT [alcg_alarm_code] from [dbo].[alarm_catalog] WHERE [alcg_alarm_code] = CONVERT(INT, 0x000A0000) + CONVERT(INT, 0x0005) AND [alcg_language_id] = 9)
  INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) 
                         SELECT          ALARM_CODE,                  9,           0,          EN,                 EN,              1 FROM #ALARMS ORDER BY ALARM_CODE DESC
GO

IF NOT EXISTS (SELECT [alcc_alarm_code] from [dbo].[alarm_catalog_per_category] WHERE [alcc_alarm_code] = CONVERT(INT, 0x000A0000) + CONVERT(INT, 0x0005) AND [alcc_category] = 1) 
  INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) 
                                       SELECT       ALARM_CODE,               1,           0,         GETDATE()  FROM #ALARMS
GO

DROP TABLE #ALARMS
GO

-- ALARM_CATEGORY - Coins validator
IF NOT EXISTS (SELECT [alc_category_id] from [dbo].[alarm_categories] WHERE [alc_category_id] = 50 AND [alc_language_id] = 10)
BEGIN
  INSERT [dbo].[alarm_categories] ([alc_category_id], [alc_language_id], [alc_alarm_group_id], [alc_type], [alc_name], [alc_description], [alc_visible]) 
  VALUES (50, 10, 1, 0, N'Validador de monedas', N'', 1)
END 
GO

IF NOT EXISTS (SELECT [alc_category_id] from [dbo].[alarm_categories] WHERE [alc_category_id] = 50 AND [alc_language_id] = 9)
BEGIN
  INSERT [dbo].[alarm_categories] ([alc_category_id], [alc_language_id], [alc_alarm_group_id], [alc_type], [alc_name], [alc_description], [alc_visible]) 
    VALUES (50,  9, 1, 0, N'Coin validator', N'', 1)
END 
GO

-- Coin in tilt
IF NOT EXISTS (SELECT [alcg_alarm_code] from [dbo].[alarm_catalog] WHERE [alcg_alarm_code] = 65569 AND [alcg_language_id] = 10)
BEGIN
  INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) 
    VALUES ( 65569, 10, 0, N'Atasco de moneda', N'Atasco de moneda', 1)
END
GO

IF NOT EXISTS (SELECT [alcg_alarm_code] from [dbo].[alarm_catalog] WHERE [alcg_alarm_code] = 65569 AND [alcg_language_id] = 9)
BEGIN
  INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) 
    VALUES ( 65569,  9, 0, N'Coin in tilt', N'Coin in tilt', 1)
END 
GO

IF NOT EXISTS (SELECT [alcc_alarm_code] from [dbo].[alarm_catalog_per_category] WHERE [alcc_alarm_code] = 65569 AND [alcc_category] = 50) 
BEGIN
  INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) 
    VALUES ( 65569, 50, 0, GETDATE() )
END
GO

-- Reverse coin in detected
IF NOT EXISTS (SELECT [alcg_alarm_code] from [dbo].[alarm_catalog] WHERE [alcg_alarm_code] = 65581 AND [alcg_language_id] = 10)
BEGIN
  INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) 
    VALUES ( 65581, 10, 0, N'Regreso de moneda', N'Regreso de moneda', 1)
END
GO

IF NOT EXISTS (SELECT [alcg_alarm_code] from [dbo].[alarm_catalog] WHERE [alcg_alarm_code] = 65581 AND [alcg_language_id] = 9)
BEGIN
  INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) 
    VALUES ( 65581,  9, 0, N'Reverse coin in detected', N'Reverse coin in detected', 1)
END 
GO

IF NOT EXISTS (SELECT [alcc_alarm_code] from [dbo].[alarm_catalog_per_category] WHERE [alcc_alarm_code] = 65581 AND [alcc_category] = 50) 
BEGIN
  INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) 
    VALUES ( 65581, 50, 0, GETDATE() )
END
GO

-- Coin in lockout malfunction
IF NOT EXISTS (SELECT [alcg_alarm_code] from [dbo].[alarm_catalog] WHERE [alcg_alarm_code] = 65657 AND [alcg_language_id] = 10)
BEGIN
  INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) 
    VALUES ( 65657, 10, 0, N'Fallo en bloqueo de monedas', N'Fallo en bloqueo de monedas', 1)
END
GO

IF NOT EXISTS (SELECT [alcg_alarm_code] from [dbo].[alarm_catalog] WHERE [alcg_alarm_code] = 65657 AND [alcg_language_id] = 9)
BEGIN
  INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) 
    VALUES ( 65657,  9, 0, N'Coin in lockout malfunction', N'Coin in lockout malfunction', 1)
END 
GO

IF NOT EXISTS (SELECT [alcc_alarm_code] from [dbo].[alarm_catalog_per_category] WHERE [alcc_alarm_code] = 65657 AND [alcc_category] = 50) 
BEGIN
  INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) 
    VALUES ( 65657, 50, 0, GETDATE() )
END
GO
