/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_100]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 35;

SET @New_ReleaseId = 36;
SET @New_ScriptName = N'UpdateTo_18.036.sql';
SET @New_Description = N'AntiMoney Laundering GPs and tabulated accounts-occupations added';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO


/****** TABLES ******/

-- OCCUPATIONS
CREATE TABLE [dbo].[occupations](
	[oc_id] [int] IDENTITY(1,1) NOT NULL,
	[oc_description] [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[oc_code] [nvarchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[oc_enabled] [bit] NOT NULL CONSTRAINT [DF_occupations_oc_enabled]  DEFAULT ((1)),
	[oc_order] [int]  NOT NULL DEFAULT ((1000)),
 CONSTRAINT [PK_occupations] PRIMARY KEY CLUSTERED 
(
	[oc_id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

-- ACCOUNTS
ALTER TABLE dbo.accounts ADD
	ac_holder_occupation_id int NULL,
	ac_beneficiary_occupation_id int NULL
GO


/****** INDEXES ******/

/****** STORED PROCEDURES ******/

--[Update_PersonalInfo]
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Update_PersonalInfo]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Update_PersonalInfo]
  GO
  CREATE PROCEDURE [dbo].[Update_PersonalInfo]
    @pCallingSiteId              int
  , @pAccountId                  bigint
  , @pTrackData                  nvarchar(50)
  , @pAccountCreated             Datetime 
  , @pHolderName                 nvarchar(200)
  , @pHolderId                   nvarchar(20)
  , @pHolderIdType               int
  , @pHolderAddress01            nvarchar(50)
  , @pHolderAddress02            nvarchar(50)
  , @pHolderAddress03            nvarchar(50)
  , @pHolderCity                 nvarchar(50)
  , @pHolderZip                  nvarchar(10) 
  , @pHolderEmail01              nvarchar(50)
  , @pHolderEmail02              nvarchar(50)
  , @pHolderTwitter              nvarchar(50)
  , @pHolderPhoneNumber01        nvarchar(20)
  , @pHolderPhoneNumber02        nvarchar(20)
  , @pHolderComments             nvarchar(100)
  , @pHolderId1                  nvarchar(20)
  , @pHolderId2                  nvarchar(20)
  , @pHolderDocumentId1          bigint
  , @pHolderDocumentId2          bigint
  , @pHolderName1                nvarchar(50)
  , @pHolderName2                nvarchar(50)
  , @pHolderName3                nvarchar(50)
  , @pHolderGender               int
  , @pHolderMaritalStatus        int
  , @pHolderBirthDate            datetime
  , @pHolderWeddingDate          datetime
  , @pHolderLevel                int
  , @pHolderLevelNotify          int
  , @pHolderLevelEntered         datetime
  , @pHolderLevelExpiration      datetime
  , @pPin                        nvarchar(12)
  , @pPinFailures                int
  , @pPinLastModified            datetime
  , @pBlocked                    bit
  , @pActivated                  bit
  , @pBlockReason                int
  , @pHolderIsVip                int
  , @pHolderTitle                nvarchar(15)                       
  , @pHolderName4                nvarchar(50)                       
  , @pHolderPhoneType01          int
  , @pHolderPhoneType02          int
  , @pHolderState                nvarchar(50)                    
  , @pHolderCountry              nvarchar(50) 
  , @pUserType                   int
  , @pPersonalInfoSeqId          int
  , @pPointsStatus               int
  , @pDeposit                    money
  , @pCardPay                    bit
  , @pBlockDescription           nvarchar(256) 
  , @pMSCreatedOnSiteId          Int 
  , @pExternalReference          nvarchar(50) 
  , @pHolderOccupation           nvarchar(50) 	
  , @pHolderExtNum               nvarchar(10) 
  , @pHolderNationality          Int 
  , @pHolderBirthCountry         Int 
  , @pHolderFedEntity            Int 
  , @pHolderId1Type              Int -- RFC
  , @pHolderId2Type              Int -- CURP
  , @pHolderId3Type              nvarchar(50)   
  , @pHolderId3                  nvarchar(20) 
  , @pHolderHasBeneficiary       bit  
  , @pBeneficiaryName            nvarchar(200) 
  , @pBeneficiaryName1           nvarchar(50) 
  , @pBeneficiaryName2           nvarchar(50) 
  , @pBeneficiaryName3           nvarchar(50) 
  , @pBeneficiaryBirthDate       Datetime 
  , @pBeneficiaryGender          int 
  , @pBeneficiaryOccupation      nvarchar(50) 
  , @pBeneficiaryId1Type         int   -- RFC
  , @pBeneficiaryId1             nvarchar(20) 
  , @pBeneficiaryId2Type         int   -- CURP
  , @pBeneficiaryId2             nvarchar(20) 
  , @pBeneficiaryId3Type         nvarchar(50)  
  , @pBeneficiaryId3  	         nvarchar(20)   
  , @pHolderOccupationId         int
  , @pBeneficiaryOccupationId    int
  , @pOutputStatus               INT               OUTPUT
  , @pOutputAccountOtherId       BIGINT            OUTPUT
  AS
  BEGIN

  DECLARE @pOtherAccountId as BIGINT
  DECLARE @IsNewAccount    as BIT
  DECLARE @PreviousUserType as INT
  DECLARE @PreviousPersonalInfoSeqId as BIGINT
  DECLARE @NumAccounts as INT

  SET @pOutputStatus = 0
  SET @pOutputAccountOtherId = 0
  
  SET @pUserType = ISNULL(@pUserType,CASE WHEN ISNULL(@pHolderLevel,0) > 0  THEN 1 ELSE 0 END)
  
  SET @NumAccounts = (SELECT COUNT(AC_ACCOUNT_ID) FROM ACCOUNTS WHERE AC_ACCOUNT_ID <> @pAccountId AND AC_HOLDER_ID = @pHolderId)
  IF @NumAccounts > 0 
  BEGIN
    SET @pOutputStatus = 1
  END

  SET @pOtherAccountId = ISNULL ((SELECT AC_ACCOUNT_ID FROM ACCOUNTS WHERE AC_TRACK_DATA = @pTrackData), 0)
  IF @pOtherAccountId <> 0 AND @pOtherAccountId <> @pAccountId
  BEGIN  
    UPDATE   ACCOUNTS 
       SET   AC_TRACK_DATA = '00000000000000000000-RECYCLED-' + CAST (@pOtherAccountId AS NVARCHAR) 
     WHERE   AC_ACCOUNT_ID = @pOtherAccountId

    SET @pOutputStatus = 2
    SET @pOutputAccountOtherId = @pOtherAccountId
  END

  SELECT @PreviousPersonalInfoSeqId = AC_MS_PERSONAL_INFO_SEQ_ID FROM ACCOUNTS WHERE AC_ACCOUNT_ID = @pAccountId 

  IF (@PreviousPersonalInfoSeqId IS NOT NULL AND @PreviousPersonalInfoSeqId > @pPersonalInfoSeqId)
  BEGIN
    SET @pOutputStatus = 3
    SELECT @pOutputAccountOtherId = ISNULL(AC_MS_LAST_SITE_ID, 0) FROM ACCOUNTS WHERE AC_ACCOUNT_ID = @pAccountId 
  END

  SET @IsNewAccount = 0
  IF NOT EXISTS (SELECT 1 FROM ACCOUNTS WHERE ac_account_id = @pAccountId )  
  BEGIN
    SET @IsNewAccount = 1

    INSERT INTO   ACCOUNTS 
                ( AC_ACCOUNT_ID, AC_TYPE, AC_BLOCKED, AC_MS_LAST_SITE_ID, AC_CREATED) 
         VALUES ( @pAccountId,         2,          1, @pCallingSiteId   , @pAccountCreated)
  END 
  ELSE
  BEGIN  
    SELECT @PreviousUserType = AC_USER_TYPE FROM ACCOUNTS WHERE AC_ACCOUNT_ID = @pAccountId     
    IF (@PreviousUserType = 1 AND @pUserType = 0) 
    BEGIN
      -- TODO: Make Alarm.
      -- SET @pOutputStatus = 4
      
      SET @pUserType = 1
    END
  END
   
  UPDATE   ACCOUNTS                   
     SET   AC_TRACK_DATA                = @pTrackData 
         , AC_HOLDER_NAME               = @pHolderName 
         , AC_HOLDER_ID                 = @pHolderId 
         , AC_HOLDER_ID_TYPE            = @pHolderIdType 
         , AC_HOLDER_ADDRESS_01         = @pHolderAddress01 
         , AC_HOLDER_ADDRESS_02         = @pHolderAddress02 
         , AC_HOLDER_ADDRESS_03         = @pHolderAddress03 
         , AC_HOLDER_CITY               = @pHolderCity 
         , AC_HOLDER_ZIP                = @pHolderZip  
         , AC_HOLDER_EMAIL_01           = @pHolderEmail01 
         , AC_HOLDER_EMAIL_02           = @pHolderEmail02 
         , AC_HOLDER_TWITTER_ACCOUNT    = @pHolderTwitter 
         , AC_HOLDER_PHONE_NUMBER_01    = @pHolderPhoneNumber01 
         , AC_HOLDER_PHONE_NUMBER_02    = @pHolderPhoneNumber02 
         , AC_HOLDER_COMMENTS           = @pHolderComments 
         , AC_HOLDER_ID1                = @pHolderId1 
         , AC_HOLDER_ID2                = @pHolderId2 
         , AC_HOLDER_DOCUMENT_ID1       = @pHolderDocumentId1 
         , AC_HOLDER_DOCUMENT_ID2       = @pHolderDocumentId2 
         , AC_HOLDER_NAME1              = @pHolderName1 
         , AC_HOLDER_NAME2              = @pHolderName2 
         , AC_HOLDER_NAME3              = @pHolderName3 
         , AC_HOLDER_GENDER             = @pHolderGender  
         , AC_HOLDER_MARITAL_STATUS     = @pHolderMaritalStatus 
         , AC_HOLDER_BIRTH_DATE         = @pHolderBirthDate 
         , AC_HOLDER_WEDDING_DATE       = @pHolderWeddingDate 
         , AC_HOLDER_LEVEL              = CASE WHEN (@IsNewAccount = 0) AND (AC_USER_TYPE = @pUserType)  THEN AC_HOLDER_LEVEL            ELSE @pHolderLevel           END
         , AC_HOLDER_LEVEL_NOTIFY       = CASE WHEN (@IsNewAccount = 0) AND (AC_USER_TYPE = @pUserType)  THEN AC_HOLDER_LEVEL_NOTIFY     ELSE @pHolderLevelNotify     END 
         , AC_HOLDER_LEVEL_ENTERED      = CASE WHEN (@IsNewAccount = 0) AND (AC_USER_TYPE = @pUserType)  THEN AC_HOLDER_LEVEL_ENTERED    ELSE @pHolderLevelEntered    END  
         , AC_HOLDER_LEVEL_EXPIRATION   = CASE WHEN (@IsNewAccount = 0) AND (AC_USER_TYPE = @pUserType)  THEN AC_HOLDER_LEVEL_EXPIRATION ELSE @pHolderLevelExpiration END   
         , AC_PIN                       = @pPin 
         , AC_PIN_FAILURES              = @pPinFailures 
         , AC_PIN_LAST_MODIFIED         = @pPinLastModified 
         , AC_BLOCKED                   = @pBlocked 
         , AC_ACTIVATED                 = @pActivated 
         , AC_BLOCK_REASON              = @pBlockReason 
         , AC_HOLDER_IS_VIP             = @pHolderIsVip 
         , AC_HOLDER_TITLE              = @pHolderTitle 
         , AC_HOLDER_NAME4              = @pHolderName4         
         , AC_HOLDER_PHONE_TYPE_01      = @pHolderPhoneType01  
         , AC_HOLDER_PHONE_TYPE_02      = @pHolderPhoneType02  
         , AC_HOLDER_STATE              = @pHolderState    
         , AC_HOLDER_COUNTRY            = @pHolderCountry   
         , AC_USER_TYPE                 = @pUserType
         , AC_POINTS_STATUS             = CASE WHEN (@pUserType = 0) THEN NULL ELSE CASE WHEN (@IsNewAccount = 0) THEN @pPointsStatus ELSE 0 END END           
         , AC_DEPOSIT                   = @pDeposit 
         , AC_CARD_PAID                 = @pCardPay 
         , AC_BLOCK_DESCRIPTION         = @pBlockDescription          
         , AC_MS_CREATED_ON_SITE_ID     = ISNULL(AC_MS_CREATED_ON_SITE_ID, ISNULL(@pMSCreatedOnSiteId, @pCallingSiteId))
         , AC_EXTERNAL_REFERENCE        = ISNULL(@pExternalReference,AC_EXTERNAL_REFERENCE)
         , AC_HOLDER_OCCUPATION         = @pHolderOccupation
         , AC_HOLDER_EXT_NUM            = @pHolderExtNum
         , AC_HOLDER_NATIONALITY        = @pHolderNationality 
         , AC_HOLDER_BIRTH_COUNTRY      = @pHolderBirthCountry 
         , AC_HOLDER_FED_ENTITY         = @pHolderFedEntity
         , AC_HOLDER_ID1_TYPE           = @pHolderId1Type 
         , AC_HOLDER_ID2_TYPE           = @pHolderId2Type 
         , AC_HOLDER_ID3_TYPE           = @pHolderId3Type 
         , AC_HOLDER_ID3                = @pHolderId3
         , AC_HOLDER_HAS_BENEFICIARY    = @pHolderHasBeneficiary 
         , AC_BENEFICIARY_NAME          = @pBeneficiaryName 
         , AC_BENEFICIARY_NAME1         = @pBeneficiaryName1
         , AC_BENEFICIARY_NAME2         = @pBeneficiaryName2
         , AC_BENEFICIARY_NAME3         = @pBeneficiaryName3
         , AC_BENEFICIARY_BIRTH_DATE    = @pBeneficiaryBirthDate 
         , AC_BENEFICIARY_GENDER        = @pBeneficiaryGender 
         , AC_BENEFICIARY_OCCUPATION    = @pBeneficiaryOccupation
         , AC_BENEFICIARY_ID1_TYPE      = @pBeneficiaryId1Type 
         , AC_BENEFICIARY_ID1           = @pBeneficiaryId1 
         , AC_BENEFICIARY_ID2_TYPE      = @pBeneficiaryId2Type 
         , AC_BENEFICIARY_ID2           = @pBeneficiaryId2 
         , AC_BENEFICIARY_ID3_TYPE      = @pBeneficiaryId3Type 
         , AC_BENEFICIARY_ID3           = @pBeneficiaryId3 
         , AC_HOLDER_OCCUPATION_ID      = ISNULL(@pHolderOccupationId, AC_HOLDER_OCCUPATION_ID)
         , AC_BENEFICIARY_OCCUPATION_ID = ISNULL(@pBeneficiaryOccupationId, AC_BENEFICIARY_OCCUPATION_ID)
   WHERE   AC_ACCOUNT_ID                = @pAccountId 
       
END
GO

--Update_PersonalInfoForOldVersion
  IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Update_PersonalInfoForOldVersion]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Update_PersonalInfoForOldVersion]
  GO
  CREATE PROCEDURE [dbo].[Update_PersonalInfoForOldVersion]
    @pCallingSiteId int
  , @pAccountId bigint
  , @pTrackData nvarchar(50)
  , @pAccountCreated Datetime 
  , @pHolderName nvarchar(200)
  , @pHolderId nvarchar(20)
  , @pHolderIdType int
  , @pHolderAddress01 nvarchar(50)
  , @pHolderAddress02 nvarchar(50)
  , @pHolderAddress03 nvarchar(50)
  , @pHolderCity nvarchar(50)
  , @pHolderZip  nvarchar(10) 
  , @pHolderEmail01 nvarchar(50)
  , @pHolderEmail02 nvarchar(50)
  , @pHolderTwitter nvarchar(50)
  , @pHolderPhoneNumber01 nvarchar(20)
  , @pHolderPhoneNumber02 nvarchar(20)
  , @pHolderComments nvarchar(100)
  , @pHolderId1 nvarchar(20)
  , @pHolderId2 nvarchar(20)
  , @pHolderDocumentId1 bigint
  , @pHolderDocumentId2 bigint
  , @pHolderName1 nvarchar(50)
  , @pHolderName2 nvarchar(50)
  , @pHolderName3 nvarchar(50)
  , @pHolderGender  int
  , @pHolderMaritalStatus int
  , @pHolderBirthDate datetime
  , @pHolderWeddingDate datetime
  , @pHolderLevel int
  , @pHolderLevelNotify int
  , @pHolderLevelEntered datetime
  , @pHolderLevelExpiration datetime
  , @pPin nvarchar(12)
  , @pPinFailures int
  , @pPinLastModified datetime
  , @pBlocked bit
  , @pActivated bit
  , @pBlockReason int
  , @pHolderIsVip int
  , @pHolderTitle nvarchar(15)                       
  , @pHolderName4 nvarchar(50)                       
  , @pHolderPhoneType01  int
  , @pHolderPhoneType02  int
  , @pHolderState    nvarchar(50)                    
  , @pHolderCountry  nvarchar(50) 
  , @pUserType  int
  , @pPersonalInfoSeqId  int
  , @pPointsStatus int
  , @pDeposit money
  , @pCardPay bit
  , @pBlockDescription nvarchar(256) 
  , @pMSCreatedOnSiteId Int 
  , @pExternalReference nvarchar(50) 
  , @pHolderOccupation  nvarchar(50) 	
  , @pHolderExtNum      nvarchar(10) 
  , @pHolderNationality  Int 
  , @pHolderBirthCountry Int 
  , @pHolderFedEntity    Int 
  , @pHolderId1Type      Int -- RFC
  , @pHolderId2Type      Int -- CURP
  , @pHolderId3Type       nvarchar(50)   
  , @pHolderId3           nvarchar(20) 
  , @pHolderHasBeneficiary bit  
  , @pBeneficiaryName     nvarchar(200) 
  , @pBeneficiaryName1    nvarchar(50) 
  , @pBeneficiaryName2    nvarchar(50) 
  , @pBeneficiaryName3    nvarchar(50) 
  , @pBeneficiaryBirthDate Datetime 
  , @pBeneficiaryGender    int 
  , @pBeneficiaryOccupation nvarchar(50) 
  , @pBeneficiaryId1Type    int   -- RFC
  , @pBeneficiaryId1        nvarchar(20) 
  , @pBeneficiaryId2Type    int   -- CURP
  , @pBeneficiaryId2        nvarchar(20) 
  , @pBeneficiaryId3Type    nvarchar(50)  
  , @pBeneficiaryId3  	    nvarchar(20)   
  , @pOutputStatus INT               OUTPUT
  , @pOutputAccountOtherId BIGINT    OUTPUT
  AS
  BEGIN

  DECLARE @pOtherAccountId as BIGINT
  DECLARE @IsNewAccount    as BIT
  DECLARE @PreviousUserType as INT
  DECLARE @PreviousPersonalInfoSeqId as BIGINT
  DECLARE @NumAccounts as INT

  SET @pOutputStatus = 0
  SET @pOutputAccountOtherId = 0
  
  SET @pUserType = ISNULL(@pUserType,CASE WHEN ISNULL(@pHolderLevel,0) > 0  THEN 1 ELSE 0 END)
  
  SET @NumAccounts = (SELECT COUNT(AC_ACCOUNT_ID) FROM ACCOUNTS WHERE AC_ACCOUNT_ID <> @pAccountId AND AC_HOLDER_ID = @pHolderId)
  IF @NumAccounts > 0 
  BEGIN
    SET @pOutputStatus = 1
  END

  SET @pOtherAccountId = ISNULL ((SELECT AC_ACCOUNT_ID FROM ACCOUNTS WHERE AC_TRACK_DATA = @pTrackData), 0)
  IF @pOtherAccountId <> 0 AND @pOtherAccountId <> @pAccountId
  BEGIN  
    UPDATE   ACCOUNTS 
       SET   AC_TRACK_DATA = '00000000000000000000-RECYCLED-' + CAST (@pOtherAccountId AS NVARCHAR) 
     WHERE   AC_ACCOUNT_ID = @pOtherAccountId

    SET @pOutputStatus = 2
    SET @pOutputAccountOtherId = @pOtherAccountId
  END

  SELECT @PreviousPersonalInfoSeqId = AC_MS_PERSONAL_INFO_SEQ_ID FROM ACCOUNTS WHERE AC_ACCOUNT_ID = @pAccountId 

  IF (@PreviousPersonalInfoSeqId IS NOT NULL AND @PreviousPersonalInfoSeqId > @pPersonalInfoSeqId)
  BEGIN
    SET @pOutputStatus = 3
    SELECT @pOutputAccountOtherId = ISNULL(AC_MS_LAST_SITE_ID, 0) FROM ACCOUNTS WHERE AC_ACCOUNT_ID = @pAccountId 
  END

  SET @IsNewAccount = 0
  IF NOT EXISTS (SELECT 1 FROM ACCOUNTS WHERE ac_account_id = @pAccountId )  
  BEGIN
    SET @IsNewAccount = 1

    INSERT INTO   ACCOUNTS 
                ( AC_ACCOUNT_ID, AC_TYPE, AC_BLOCKED, AC_MS_LAST_SITE_ID, AC_CREATED) 
         VALUES ( @pAccountId,         2,          1, @pCallingSiteId   , @pAccountCreated)
  END 
  ELSE
  BEGIN  
    SELECT @PreviousUserType = AC_USER_TYPE FROM ACCOUNTS WHERE AC_ACCOUNT_ID = @pAccountId     
    IF (@PreviousUserType = 1 AND @pUserType = 0) 
    BEGIN
      -- TODO: Make Alarm.
      -- SET @pOutputStatus = 4
      
      SET @pUserType = 1
    END
  END
   
  UPDATE   ACCOUNTS                   
     SET   AC_TRACK_DATA              = @pTrackData 
         , AC_HOLDER_NAME             = @pHolderName 
         , AC_HOLDER_ID               = @pHolderId 
         , AC_HOLDER_ID_TYPE          = @pHolderIdType 
         , AC_HOLDER_ADDRESS_01       = @pHolderAddress01 
         , AC_HOLDER_ADDRESS_02       = @pHolderAddress02 
         , AC_HOLDER_ADDRESS_03       = @pHolderAddress03 
         , AC_HOLDER_CITY             = @pHolderCity 
         , AC_HOLDER_ZIP              = @pHolderZip  
         , AC_HOLDER_EMAIL_01         = @pHolderEmail01 
         , AC_HOLDER_EMAIL_02         = @pHolderEmail02 
         , AC_HOLDER_TWITTER_ACCOUNT  = @pHolderTwitter 
         , AC_HOLDER_PHONE_NUMBER_01  = @pHolderPhoneNumber01 
         , AC_HOLDER_PHONE_NUMBER_02  = @pHolderPhoneNumber02 
         , AC_HOLDER_COMMENTS         = @pHolderComments 
         , AC_HOLDER_ID1              = @pHolderId1 
         , AC_HOLDER_ID2              = @pHolderId2 
         , AC_HOLDER_DOCUMENT_ID1     = @pHolderDocumentId1 
         , AC_HOLDER_DOCUMENT_ID2     = @pHolderDocumentId2 
         , AC_HOLDER_NAME1            = @pHolderName1 
         , AC_HOLDER_NAME2            = @pHolderName2 
         , AC_HOLDER_NAME3            = @pHolderName3 
         , AC_HOLDER_GENDER           = @pHolderGender  
         , AC_HOLDER_MARITAL_STATUS   = @pHolderMaritalStatus 
         , AC_HOLDER_BIRTH_DATE       = @pHolderBirthDate 
         , AC_HOLDER_WEDDING_DATE     = @pHolderWeddingDate 
         , AC_HOLDER_LEVEL            = CASE WHEN (@IsNewAccount = 0) AND (AC_USER_TYPE = @pUserType)  THEN AC_HOLDER_LEVEL            ELSE @pHolderLevel           END
         , AC_HOLDER_LEVEL_NOTIFY     = CASE WHEN (@IsNewAccount = 0) AND (AC_USER_TYPE = @pUserType)  THEN AC_HOLDER_LEVEL_NOTIFY     ELSE @pHolderLevelNotify     END 
         , AC_HOLDER_LEVEL_ENTERED    = CASE WHEN (@IsNewAccount = 0) AND (AC_USER_TYPE = @pUserType)  THEN AC_HOLDER_LEVEL_ENTERED    ELSE @pHolderLevelEntered    END  
         , AC_HOLDER_LEVEL_EXPIRATION = CASE WHEN (@IsNewAccount = 0) AND (AC_USER_TYPE = @pUserType)  THEN AC_HOLDER_LEVEL_EXPIRATION ELSE @pHolderLevelExpiration END   
         , AC_PIN                     = @pPin 
         , AC_PIN_FAILURES            = @pPinFailures 
         , AC_PIN_LAST_MODIFIED       = @pPinLastModified 
         , AC_BLOCKED                 = @pBlocked 
         , AC_ACTIVATED               = @pActivated 
         , AC_BLOCK_REASON            = @pBlockReason 
         , AC_HOLDER_IS_VIP           = @pHolderIsVip 
         , AC_HOLDER_TITLE            = @pHolderTitle 
         , AC_HOLDER_NAME4            = @pHolderName4         
         , AC_HOLDER_PHONE_TYPE_01    = @pHolderPhoneType01  
         , AC_HOLDER_PHONE_TYPE_02    = @pHolderPhoneType02  
         , AC_HOLDER_STATE            = @pHolderState    
         , AC_HOLDER_COUNTRY          = @pHolderCountry   
         , AC_USER_TYPE               = @pUserType
         , AC_POINTS_STATUS           = CASE WHEN (@pUserType = 0) THEN NULL ELSE CASE WHEN (@IsNewAccount = 0) THEN @pPointsStatus ELSE 0 END END           
         , AC_DEPOSIT                 = @pDeposit 
         , AC_CARD_PAID               = @pCardPay 
         , AC_BLOCK_DESCRIPTION       = @pBlockDescription          
         , AC_MS_CREATED_ON_SITE_ID   = ISNULL(AC_MS_CREATED_ON_SITE_ID, ISNULL(@pMSCreatedOnSiteId, @pCallingSiteId))
         , AC_EXTERNAL_REFERENCE      = ISNULL(@pExternalReference,AC_EXTERNAL_REFERENCE)
         , AC_HOLDER_OCCUPATION       = @pHolderOccupation
         , AC_HOLDER_EXT_NUM          = @pHolderExtNum
         , AC_HOLDER_NATIONALITY      = @pHolderNationality 
         , AC_HOLDER_BIRTH_COUNTRY    = @pHolderBirthCountry 
         , AC_HOLDER_FED_ENTITY       = @pHolderFedEntity
         , AC_HOLDER_ID1_TYPE         = @pHolderId1Type 
         , AC_HOLDER_ID2_TYPE         = @pHolderId2Type 
         , AC_HOLDER_ID3_TYPE         = @pHolderId3Type 
         , AC_HOLDER_ID3              = @pHolderId3
         , AC_HOLDER_HAS_BENEFICIARY  = @pHolderHasBeneficiary 
         , AC_BENEFICIARY_NAME        = @pBeneficiaryName 
         , AC_BENEFICIARY_NAME1       = @pBeneficiaryName1
         , AC_BENEFICIARY_NAME2       = @pBeneficiaryName2
         , AC_BENEFICIARY_NAME3       = @pBeneficiaryName3
         , AC_BENEFICIARY_BIRTH_DATE  = @pBeneficiaryBirthDate 
         , AC_BENEFICIARY_GENDER      = @pBeneficiaryGender 
         , AC_BENEFICIARY_OCCUPATION  = @pBeneficiaryOccupation
         , AC_BENEFICIARY_ID1_TYPE    = @pBeneficiaryId1Type 
         , AC_BENEFICIARY_ID1         = @pBeneficiaryId1 
         , AC_BENEFICIARY_ID2_TYPE    = @pBeneficiaryId2Type 
         , AC_BENEFICIARY_ID2         = @pBeneficiaryId2 
         , AC_BENEFICIARY_ID3_TYPE    = @pBeneficiaryId3Type 
         , AC_BENEFICIARY_ID3         = @pBeneficiaryId3 
   WHERE   AC_ACCOUNT_ID              = @pAccountId 
       
END
GO


/****** TRIGGERS ******/

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[MultiSiteTrigger_AccountUpdate]') AND type in (N'TR'))
DROP TRIGGER [dbo].[MultiSiteTrigger_AccountUpdate]
GO


CREATE TRIGGER [dbo].[MultiSiteTrigger_AccountUpdate] ON [dbo].[accounts]
   AFTER UPDATE
NOT FOR REPLICATION
AS 
BEGIN
    DECLARE @Sequence11Value    AS BIGINT
    DECLARE @Sequence10Value    AS BIGINT
    DECLARE @AccountId          AS BIGINT
    DECLARE @hash0              as varbinary(20)
    DECLARE @hash1              as varbinary(20)
    DECLARE @value              as nvarchar(max)
    DECLARE @changed            as bit
    DECLARE @updated            as bit
    
    SET @updated = 0;        
            
    IF (UPDATE(AC_MS_CHANGE_GUID)) RETURN
 
    IF (UPDATE(AC_POINTS))
      BEGIN
          DECLARE PointsCursor CURSOR FOR 
           SELECT   INSERTED.AC_ACCOUNT_ID 
             FROM   INSERTED, DELETED 
            WHERE   INSERTED.AC_ACCOUNT_ID    =  DELETED.AC_ACCOUNT_ID
              AND ( INSERTED.AC_POINTS        <> DELETED.AC_POINTS )
               
        SET NOCOUNT ON;

        OPEN PointsCursor

        FETCH NEXT FROM PointsCursor INTO @AccountId
          
        WHILE @@FETCH_STATUS = 0
        BEGIN
            UPDATE   SEQUENCES 
               SET   SEQ_NEXT_VALUE = SEQ_NEXT_VALUE + 1
             WHERE   SEQ_ID         = 11

            SELECT   @Sequence11Value = SEQ_NEXT_VALUE - 1 FROM SEQUENCES WHERE SEQ_ID = 11

            UPDATE   ACCOUNTS
               SET   AC_MS_POINTS_SEQ_ID = @Sequence11Value 
             WHERE   AC_ACCOUNT_ID       = @AccountId

            FETCH NEXT FROM PointsCursor INTO @AccountId
        END

        CLOSE PointsCursor
        DEALLOCATE PointsCursor
    END


    IF UPDATE (AC_TRACK_DATA)
    OR UPDATE (AC_HOLDER_NAME)
    OR UPDATE (AC_HOLDER_ID)
    OR UPDATE (AC_HOLDER_ID_TYPE)
    OR UPDATE (AC_HOLDER_ADDRESS_01)
    OR UPDATE (AC_HOLDER_ADDRESS_02)
    OR UPDATE (AC_HOLDER_ADDRESS_03)
    OR UPDATE (AC_HOLDER_CITY)
    OR UPDATE (AC_HOLDER_ZIP)
    OR UPDATE (AC_HOLDER_EMAIL_01)
    OR UPDATE (AC_HOLDER_EMAIL_02)
    OR UPDATE (AC_HOLDER_TWITTER_ACCOUNT)
    OR UPDATE (AC_HOLDER_PHONE_NUMBER_01)
    OR UPDATE (AC_HOLDER_PHONE_NUMBER_02)
    OR UPDATE (AC_HOLDER_COMMENTS)
    OR UPDATE (AC_HOLDER_ID1)
    OR UPDATE (AC_HOLDER_ID2)
    OR UPDATE (AC_HOLDER_DOCUMENT_ID1)
    OR UPDATE (AC_HOLDER_DOCUMENT_ID2)
    OR UPDATE (AC_HOLDER_NAME1)
    OR UPDATE (AC_HOLDER_NAME2)
    OR UPDATE (AC_HOLDER_NAME3)
    OR UPDATE (AC_HOLDER_GENDER)
    OR UPDATE (AC_HOLDER_MARITAL_STATUS)
    OR UPDATE (AC_HOLDER_BIRTH_DATE)
    OR UPDATE (AC_HOLDER_WEDDING_DATE)
    OR UPDATE (AC_HOLDER_LEVEL)
    OR UPDATE (AC_HOLDER_LEVEL_NOTIFY)
    OR UPDATE (AC_HOLDER_LEVEL_ENTERED)
    OR UPDATE (AC_HOLDER_LEVEL_EXPIRATION)
    OR UPDATE (AC_PIN)
    OR UPDATE (AC_PIN_FAILURES)
    OR UPDATE (AC_PIN_LAST_MODIFIED)
    OR UPDATE (AC_BLOCKED)
    OR UPDATE (AC_ACTIVATED)
    OR UPDATE (AC_BLOCK_REASON)
    OR UPDATE (AC_HOLDER_IS_VIP)
    OR UPDATE (AC_HOLDER_TITLE)
    OR UPDATE (AC_HOLDER_NAME4)
    OR UPDATE (AC_HOLDER_PHONE_TYPE_01)
    OR UPDATE (AC_HOLDER_PHONE_TYPE_02)
    OR UPDATE (AC_HOLDER_STATE)
    OR UPDATE (AC_HOLDER_COUNTRY)
    OR UPDATE (AC_USER_TYPE)
    OR UPDATE (AC_POINTS_STATUS)
    OR UPDATE (AC_DEPOSIT)
    OR UPDATE (AC_CARD_PAID)
    OR UPDATE (AC_BLOCK_DESCRIPTION)    
    OR UPDATE (AC_MS_CREATED_ON_SITE_ID)
    OR UPDATE (AC_EXTERNAL_REFERENCE)
    OR UPDATE (AC_HOLDER_OCCUPATION)     
    OR UPDATE (AC_HOLDER_EXT_NUM)       
    OR UPDATE (AC_HOLDER_NATIONALITY)
    OR UPDATE (AC_HOLDER_BIRTH_COUNTRY)
    OR UPDATE (AC_HOLDER_FED_ENTITY)
    OR UPDATE (AC_HOLDER_ID1_TYPE)
    OR UPDATE (AC_HOLDER_ID2_TYPE)
    OR UPDATE (AC_HOLDER_ID3_TYPE)
    OR UPDATE (AC_HOLDER_ID3)
    OR UPDATE (AC_HOLDER_HAS_BENEFICIARY)
    OR UPDATE (AC_BENEFICIARY_NAME)
    OR UPDATE (AC_BENEFICIARY_NAME1)
    OR UPDATE (AC_BENEFICIARY_NAME2)
    OR UPDATE (AC_BENEFICIARY_NAME3)
    OR UPDATE (AC_BENEFICIARY_BIRTH_DATE)
    OR UPDATE (AC_BENEFICIARY_GENDER)
    OR UPDATE (AC_BENEFICIARY_OCCUPATION)
    OR UPDATE (AC_BENEFICIARY_ID1_TYPE)
    OR UPDATE (AC_BENEFICIARY_ID1)
    OR UPDATE (AC_BENEFICIARY_ID2_TYPE)
    OR UPDATE (AC_BENEFICIARY_ID2)
    OR UPDATE (AC_BENEFICIARY_ID3_TYPE)
    OR UPDATE (AC_BENEFICIARY_ID3)
    OR UPDATE (AC_HOLDER_OCCUPATION_ID)
    OR UPDATE (AC_BENEFICIARY_OCCUPATION_ID)
        SET @updated = 1;
        
    IF (@updated = 0) RETURN

    DECLARE PersonalInfoCursor CURSOR FOR 
     SELECT   AC_ACCOUNT_ID
            , HASHBYTES ('SHA1',  ISNULL(AC_TRACK_DATA,        '')
                                + ISNULL(AC_HOLDER_NAME,       '')
                                + ISNULL(AC_HOLDER_ID,         '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_ID_TYPE), '')
                                + ISNULL(AC_HOLDER_ADDRESS_01, '')
                                + ISNULL(AC_HOLDER_ADDRESS_02, '')
                                + ISNULL(AC_HOLDER_ADDRESS_03, '')
                                + ISNULL(AC_HOLDER_CITY,       '')
                                + ISNULL(AC_HOLDER_ZIP,        '')
                                + ISNULL(AC_HOLDER_EMAIL_01,   '')
                                + ISNULL(AC_HOLDER_EMAIL_02,   '')
                                + ISNULL(AC_HOLDER_TWITTER_ACCOUNT, '')
                                + ISNULL(AC_HOLDER_PHONE_NUMBER_01, '')
                                + ISNULL(AC_HOLDER_PHONE_NUMBER_02, '')
                                + ISNULL(AC_HOLDER_COMMENTS,        '')
                                + ISNULL(AC_HOLDER_ID1, '')
                                + ISNULL(AC_HOLDER_ID2, '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_DOCUMENT_ID1), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_DOCUMENT_ID2), '')
                                + ISNULL(AC_HOLDER_NAME1, '')
                                + ISNULL(AC_HOLDER_NAME2, '')
                                + ISNULL(AC_HOLDER_NAME3, '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_GENDER),         '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_MARITAL_STATUS), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_BIRTH_DATE,   21), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_WEDDING_DATE, 21), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_LEVEL),                '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_LEVEL_NOTIFY),         '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_LEVEL_ENTERED,    21), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_LEVEL_EXPIRATION, 21), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_PIN),          '')
                                + ISNULL(CONVERT(NVARCHAR, AC_PIN_FAILURES), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_PIN_LAST_MODIFIED, 21), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_BLOCKED),           '')
                                + ISNULL(CONVERT(NVARCHAR, AC_ACTIVATED),         '')
                                + ISNULL(CONVERT(NVARCHAR, AC_BLOCK_REASON),         '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_IS_VIP),        '')
                                + ISNULL(AC_HOLDER_TITLE                        , '')
                                + ISNULL(AC_HOLDER_NAME4                        , '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_PHONE_TYPE_01),        '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_PHONE_TYPE_02),        '')
                                + ISNULL(AC_HOLDER_STATE                        , '')
                                + ISNULL(AC_HOLDER_COUNTRY                      , '')
                                + ISNULL(CONVERT(NVARCHAR,AC_USER_TYPE)         , '') 
                                + ISNULL(CONVERT(NVARCHAR,AC_POINTS_STATUS)     , '') 
                                + ISNULL(CONVERT(NVARCHAR,AC_DEPOSIT)           , '')
                                + ISNULL(CONVERT(NVARCHAR,AC_CARD_PAID)         , '')
                                + ISNULL(AC_BLOCK_DESCRIPTION, '')                                                                  
                                + ISNULL(CONVERT(NVARCHAR,AC_MS_CREATED_ON_SITE_ID), '')
                                + ISNULL(AC_EXTERNAL_REFERENCE, '')
                                + ISNULL(AC_HOLDER_OCCUPATION, '')
                                + ISNULL(AC_HOLDER_EXT_NUM, '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_NATIONALITY),         '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_BIRTH_COUNTRY), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_ID1_TYPE), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_ID2_TYPE), '')
                                + ISNULL(AC_HOLDER_ID3_TYPE, '')
                                + ISNULL(AC_HOLDER_ID3, '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_HAS_BENEFICIARY), '')
                                + ISNULL(AC_BENEFICIARY_NAME, '')
                                + ISNULL(AC_BENEFICIARY_NAME1, '')
                                + ISNULL(AC_BENEFICIARY_NAME2, '')
                                + ISNULL(AC_BENEFICIARY_NAME3, '')
                                + ISNULL(CONVERT(NVARCHAR, AC_BENEFICIARY_BIRTH_DATE,   21), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_BENEFICIARY_GENDER), '')	
                                + ISNULL(AC_BENEFICIARY_OCCUPATION, '')
                                + ISNULL(CONVERT(NVARCHAR, AC_BENEFICIARY_ID1_TYPE), '')				
                                + ISNULL(AC_BENEFICIARY_ID1, '')
                                + ISNULL(CONVERT(NVARCHAR, AC_BENEFICIARY_ID2_TYPE), '')				
                                + ISNULL(AC_BENEFICIARY_ID2, '')
                                + ISNULL(AC_BENEFICIARY_ID3_TYPE, '')				
                                + ISNULL(AC_BENEFICIARY_ID3, '') 
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_OCCUPATION_ID), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_BENEFICIARY_OCCUPATION_ID), '')) 
       FROM   INSERTED

    SET NOCOUNT ON;

    OPEN PersonalInfoCursor

    FETCH NEXT FROM PersonalInfoCursor INTO @AccountId, @hash1

    WHILE @@FETCH_STATUS = 0
    BEGIN

        SELECT @hash0 = AC_MS_HASH FROM DELETED WHERE AC_ACCOUNT_ID = @AccountId

        SELECT @changed = CASE WHEN ( @hash0 = @hash1 ) THEN 0 ELSE 1 END

        IF @changed = 1
        BEGIN
            -- Personal Info
            UPDATE   SEQUENCES 
               SET   SEQ_NEXT_VALUE = SEQ_NEXT_VALUE + 1
             WHERE   SEQ_ID         = 10

            SELECT   @Sequence10Value = SEQ_NEXT_VALUE - 1 FROM SEQUENCES WHERE SEQ_ID = 10

            UPDATE   ACCOUNTS
               SET   AC_MS_PERSONAL_INFO_SEQ_ID = @Sequence10Value
                   , AC_MS_HASH                 = @hash1
                   , AC_MS_CHANGE_GUID          = NEWID()
             WHERE   AC_ACCOUNT_ID              = @AccountId
        END

        FETCH NEXT FROM PersonalInfoCursor INTO @AccountId, @hash1
    END

    CLOSE PersonalInfoCursor
    DEALLOCATE PersonalInfoCursor

END
GO

/****** RECORDS ******/

UPDATE accounts
        SET ac_holder_comments = 
            SUBSTRING ( 
                        'Ocupaci�n: ' + ac_holder_occupation +   char(13) + char(10) + 
                        isnull(ac_holder_comments , ''),0,100
                       )
         WHERE len(isnull(ac_holder_occupation,'')) > 0
GO

-- NAME 
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'AntiMoneyLaundering' AND GP_SUBJECT_KEY = 'Name')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE, GP_MS_DOWNLOAD_TYPE) VALUES ('AntiMoneyLaundering','Name','Antilavado de Dinero',1)
GO

-- RECHARGE.MAXIMUM
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'AntiMoneyLaundering' AND GP_SUBJECT_KEY = 'Recharge.MaxAllowed.Limit')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE, GP_MS_DOWNLOAD_TYPE) VALUES ('AntiMoneyLaundering','Recharge.MaxAllowed.Limit','3210',1)
GO

-- RECHARGE.MAXIMUM.MESSAGE
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'AntiMoneyLaundering' AND GP_SUBJECT_KEY = 'Recharge.MaxAllowed.Message')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE, GP_MS_DOWNLOAD_TYPE) VALUES ('AntiMoneyLaundering','Recharge.MaxAllowed.Message','Por la ley antilavado de dinero, al realizar esta recarga, el cliente supera el l�mite m�ximo.',1)
GO

-- PRIZE.MAXIMUM
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'AntiMoneyLaundering' AND GP_SUBJECT_KEY = 'Prize.MaxAllowed.Limit')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE, GP_MS_DOWNLOAD_TYPE) VALUES ('AntiMoneyLaundering','Prize.MaxAllowed.Limit','3210',1)
GO

-- PRIZE.MAXIMUM.MESSAGE
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'AntiMoneyLaundering' AND GP_SUBJECT_KEY = 'Prize.MaxAllowed.Message')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE, GP_MS_DOWNLOAD_TYPE) VALUES ('AntiMoneyLaundering','Prize.MaxAllowed.Message','Por la ley antilavado de dinero, al realizar este reintegro, el cliente supera el l�mite m�ximo.',1)
GO

-- INSERT INTO OCCUPATIONS
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('0100008','AGRICULTURA')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('0200006','GANADER�A')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('0300004','SILVICULTURA')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('0400002','PESCA')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('0500000','CAZA')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('1100007','EXTRACCI�N Y BENEFICIO DE CARB�N MINERAL Y GRAFITO')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('1200005','EXTRACCI�N DE PETR�LEO CRUDO Y GAS NATURAL')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('1300003','EXTRACCI�N Y BENEFICIO DE MINERALES MET�LICOS')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('1311018','EXTRACCION Y BENEFICIO DE MINERAL DE HIERRO')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('1322015','EXTRACCION Y BENEFICIO DE MERCURIO Y ANTIMONIO')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('1329011','EXTRACCION Y BENEFICIO DE COBRE  PLOMO  ZINC Y OTROS MINERALES NO FERROSOS')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('1400001','EXTRACCI�N DE MINERALES NO MET�LICOS, EXCEPTO SAL')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('1500009','EXPLOTACI�N DE SAL')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('2000008','FABRICACI�N DE ALIMENTOS')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('2012011','EMPACADORA DE CONSERVAS ALIMENTICIAS')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('2012029','EMPACADORA DE FRUTAS Y LEGUMBRES')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('2025014','BENEFICIO DE CAFE EXCEPTO MOLIENDA Y TOSTADO')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('2049022','FABRICACION DE CARNES FRIAS Y EMBUTIDOS')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('2100006','FABRICACI�N Y ELABORACI�N DE BEBIDAS (AGUA, REFRESCOS, CERVEZA, VINOS Y LICORES)')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('2300002','INDUSTRIA TEXTIL (FABRICACI�N DE: HILADOS Y TEJIDOS)')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('2400000','FABRICACI�N DE PRENDAS DE VESTIR Y OTROS ART�CULOS CONFECCIONADOS CON TEXTILES Y OTROS MATERIALES EXCEPTO CALZADO')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('2500008','FABRICACI�N DE CALZADO E INDUSTRIA DEL CUERO')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('2600006','INDUSTRIA Y PRODUCTOS DE MADERA Y CORCHO; EXCEPTO MUEBLES')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('2711019','FABRICACION DE MUEBLES DE MADERA')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('2711027','FABRICACION DE MUEBLES DE MATERIAL SINTETICO')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('2800002','INDUSTRIA DEL PAPEL')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('2900000','INDUSTRIAS EDITORIAL, DE IMPRESI�N Y CONEXAS')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('3000007','INDUSTRIA QU�MICA')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('3021011','FABRICACION DE ABONOS Y FERTILIZANTES QUIMICOS')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('3100005','REFINACI�N DE PETR�LEO Y DERIVADOS DEL CARB�N MINERAL')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('3111010','FABRICACION DE GASOLINA Y OTROS PRODUCTOS DERIVADOS DE LA REFINACION DE PETROLEO')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('3112018','FABRICACION DE PRODUCTOS PETROQUIMICOS BASICOS')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('3113016','FABRICACION DE ACEITES Y LUBRICANTES')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('3200003','FABRICACI�N DE PRODUCTOS DE HULE Y DE PL�STICO')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('3300001','FABRICACI�N DE PRODUCTOS DE MINERALES NO MET�LICOS; EXCEPTO DEL PETR�LEO Y DEL CARB�N MINERAL')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('3322013','FABRICACION DE CRISTALES PARA AUTOMOVIL')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('3331022','FABRICACION DE LADRILLOS')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('3341013','FABRICACION DE CEMENTO')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('3400009','INDUSTRIAS MET�LICAS B�SICAS')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('3411022','FUNDICION DE FIERRO Y ACERO')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('3411030','PLANTA METALURGICA')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('3412012','FABRICACION DE LAMINAS DE HIERRO Y ACERO')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('3413010','FABRICACION DE TUBOS DE HIERRO Y ACERO')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('3500007','FABRICACI�N DE PRODUCTOS MET�LICOS; EXCEPTO MAQUINARIA Y EQUIPO')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('3599026','FABRICACION DE CAJAS FUERTES')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('3600005','FABRICACI�N, ENSAMBLE Y REPARACI�N DE MAQUINARIA, EQUIPO Y SUS PARTES; EXCEPTO LOS EL�CTRICOS')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('3700003','FABRICACI�N Y ENSAMBLE DE MAQUINARIA, EQUIPO, APARATOS, ACCESORIOS Y ART�CULOS EL�CTRICOS, ELECTR�NICOS Y SUS PARTES')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('3800001','CONSTRUCCI�N, RECONSTRUCCI�N Y ENSAMBLE DE EQUIPO DE TRANSPORTE Y SUS PARTES')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('3819010','FABRICACION DE REFACCIONES Y ACCESORIOS AUTOMOTRICES')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('3831014','FABRICACION Y REPARACION DE BUQUES Y BARCOS')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('3832012','FABRICACION ENSAMBLE Y REPARACION DE AERONAVES')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('3933018','FABRICACION DE ARTICULOS DE QUINCALLERIA Y BISUTERIA')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('3997014','FABRICACION DE ARMAS')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('4100004','CONTRATACI�N DE OBRAS COMPLETAS DE CONSTRUCCI�N (CASAS, DEPARTAMENTOS, INMUEBLES, PAVIMENTACI�N, NO RESIDENCIALES, VIAS DE COMUNICACI�N)')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('4111019','CONSTRUCCION DE CASAS Y TECHOS DESARMABLES')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('4111027','CONSTRUCCION DE INMUEBLES')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('4112017','CONSTRUCCION DE EDIFICIOS PARA OFICINAS ESCUELAS HOSPITALES HOTELES Y OTROS NO RESIDENCIALES')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('4113015','CONSTRUCCION DE EDIFICIOS INDUSTRIALES Y PARA FINES ANALOGOS')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('4121018','CONSTRUCCION DE VIAS DE COMUNICACION')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('4199015','CONSTRUCCION DE ESTADIOS MONUMENTOS Y OTRAS OBRAS DE INGENIERIA')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('5012018','DISTRIBUCION DE ENERGIA ELECTRICA')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('6100002','COMPRAVENTA DE ALIMENTOS, BEBIDAS Y PRODUCTOS DE TABACO')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('6121024','COMPRAVENTA DE GANADO MAYOR EN PIE')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('6121032','COMPRAVENTA DE GANADO MENOR EN PIE')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('6131023','TIENDA DE ABARROTES Y MISCELANEA')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('6200000','COMPRAVENTA DE PRENDAS DE VESTIR Y OTROS ART�CULOS DE USO PERSONAL')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('1321017','EXTRACCION Y BENEFICIO DE ORO PLATA Y OTROS METALES PRECIOSOS')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('3921013','FABRICACION DE RELOJES')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('3932010','FABRICACION DE ARTICULOS DE JOYERIA')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('3932036','TALLADO DE PIEDRAS PRECIOSAS')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('6225016','COMPRAVENTA DE ARTICULOS DE PLATA')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('6225024','COMPRAVENTA DE JOYAS')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('6225032','COMPRAVENTA DE RELOJES')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('6999017','COMPRAVENTA DE DIAMANTES')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9900916','COMPRAVENTA DE ARTICULOS DE ORO')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9900917','COMPRAVENTA DE ARTICULOS DE PLATINO')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9900918','COMPRAVENTA DE AGUAMARINAS,  ESMERALDAS, RUB�ES, TOPACIOS, TURQUESAS Y/O ZAFIROS')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9900919','COMPRAVENTA DE PLATA, ORO O PLATINO A GRANEL')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('6325014','COMPRAVENTA DE ANTIG�EDADES')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('8832017','GALERIAS DE ARTES GRAFICAS Y MUSEOS')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9900920','COMPRAVENTA DE OBRAS DE ARTE')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9900921','CASA DE SUBASTAS DE OBRAS DE ARTE, JOYAS Y/O ANTIG�EDADES')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('6300008','COMPRAVENTA DE ART�CULOS PARA EL HOGAR (ELECTRODOMESTICOS, REFACCIONES, LOZA Y PORCELANA, ANTIGUEDADES)')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('6400006','COMPRAVENTA EN TIENDAS DE AUTOSERVICIO Y DE DEPARTAMENTOS ESPECIALIZADOS POR L�NEA DE MERCANC�AS')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('6500004','COMPRAVENTA DE GASES, COMBUSTIBLES Y LUBRICANTES')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('6513015','COMPRAVENTA DE GASOLINA Y DIESEL')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('6514013','COMPRAVENTA DE PETROLEO COMBUSTIBLE')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('6515011','COMPRAVENTA DE LUBRICANTES')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('6600002','COMPRAVENTA DE MATERIAS PRIMAS, MATERIALES Y AUXILIARES (ALGOD�N, CEMENTO, SANITARIOS, PIELES, FERRETERIA, MADERA, PINTURAS)')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('6691019','COMPRAVENTA DE FERTILIZANTES Y PLAGUICIDAS')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('6695011','COMPRAVENTA DE SUBSTANCIAS QUIMICAS PARA LA INDUSTRIA')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('6700000','COMPRAVENTA DE MAQUINARIA, EQUIPO, INSTRUMENTOS, APARATOS Y HERRAMIENTAS, SUS REFACCIONES Y ACCESORIOS')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('6712013','COMPRAVENTA DE ARTICULOS PARA LA EXPLOTACION DE MINAS')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('6811013','COMPRAVENTA DE AUTOMOVILES Y CAMIONES NUEVOS')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('6812011','COMPRAVENTA DE AUTOMOVILES Y CAMIONES USADOS')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9900922','COMPRAVENTA DE VEHICULOS MAR�TIMOS')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('6819033','COMPRAVENTA DE VEHICULOS AEREOS')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('6813027','COMPRAVENTA DE MOTOCICLETAS Y SUS ACCESORIOS')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('6819017','COMPRAVENTA DE PARTES Y REFACCIONES PARA VEHICULOS TERRESTRES, A�REOS Y MAR�TIMOS')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('4111051','DESARROLLADORES DE VIVIENDA')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('6900006','COMPRAVENTA DE BIENES INMUEBLES Y ART�CULOS DIVERSOS')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('6911053','COMPRAVENTA DE TERRENOS')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('8313017','SERVICIO DE CORREDORES DE BIENES RAICES')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('6991013','COMPRAVENTA DE ARMAS DE FUEGO')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('6992011','AGENCIAS DE RIFAS Y SORTEOS (QUINIELAS Y LOTERIA)')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('8829022','HIPODROMO')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9900910','SALAS DE JUEGOS Y APUESTAS')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9900911','ORGANIZACI�N DE FERIAS REGIONALES CON APUESTAS')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9900912','ORGANIZACI�N DE CARRERAS DE CABALLOS O PELEAS DE GALLOS EN ESCENARIOS TEMPORALES')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('7100001','TRANSPORTE TERRESTRE')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('7200009','TRANSPORTE POR AGUA')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('7300007','TRANSPORTE A�REO')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('7312010','SERVICIOS RELACIONADOS CON EL TRANSPORTE EN AERONAVES CON MATRICULA EXTRANJERA')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('7400005','SERVICIOS CONEXOS AL TRANSPORTE')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('8429038','EMPRESAS DE SEGURIDAD PRIVADA')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('8429046','EMPRESAS TRANSPORTADORAS DE VALORES')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9900924','EMPRESAS DE CUSTODIA DE VALORES')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('7512016','AGENCIA DE TURISMO')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('7513014','AGENCIA ADUANAL')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9900928','AGENTE ADUANAL')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('8524010','ALQUILER O RENTA DE AUTOMOVILES SIN CHOFER')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('7519020','ALQUILER DE LANCHAS Y VELEROS')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('7519038','RENTA DE VEHICULOS AEREOS')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('8311011','ALQUILER DE TERRENOS LOCALES Y EDIFICIOS NO RESIDENCIALES')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('8312019','ARRENDAMIENTO DE INMUEBLES RESIDENCIALES')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('7600001','COMUNICACIONES')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('8114019','SERVICIOS DE FONDOS Y FIDEICOMISOS DE FOMENTO ECONOMICO')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('8123010','INSTITUCIONES DE BANCA M�LTIPLE')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9900929','INSTITUCIONES DE LA BANCA DE DESARROLLO')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('8123052','SOCIEDADES DE AHORRO Y PRESTAMO')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('8123060','SOCIEDADES DE AHORRO Y CREDITO POPULAR')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('8123078','SOCIEDADES FINANCIERAS DE OBJETO LIMITADO')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('8123086','SOCIEDADES FINANCIERAS DE OBJETO MULTIPLE REGULADAS')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('8123094','SOCIEDADES FINANCIERAS DE OBJETO MULTIPLE NO REGULADAS')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('8131021','ALMACENES DE DEPOSITO')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('8132029','UNIONES DE CREDITO')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('8133027','COMPA�IAS DE FIANZAS')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('8142010','SOCIEDADES DE INVERSION')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('8151029','COMPA�IAS DE SEGUROS PRIVADAS')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('8200008','SERVICIOS COLATERALES A INSTITUCIONES FINANCIERAS Y DE SEGUROS')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('8211013','INVERSIONISTA')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('8211021','AGENTE DE BOLSA')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('8211047','CASAS DE BOLSA')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('8219017','AGENTE DE SEGUROS')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('8219025','CASA DE CAMBIO')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('6999992','CENTROS CAMBIARIOS')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('8219033','CORRESPONSAL BANCARIO')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('8219041','CAJA DE AHORROS')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('8219075','FACTORING')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('8511033','ARRENDADORAS FINANCIERAS')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9311044','SOCIEDADES COOPERATIVAS')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9900902','TRANSMISORES DE DINERO O DISPERSORES')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9900903','CAMBISTAS O CENTROS CAMBIARIOS')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9911018','INSTITUCIONES FINANCIERAS DEL EXTRANJERO')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('6999124','CREDITOS PARA ADQUISICION DE BIENES DE CONSUMO DURADERO')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('6999132','CREDITOS CONSUMOS PERSONALES')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('6999166','CREDITOS AUTOMOTRIZ')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('6999174','CREDITOS ADQUISICION DE BIENES MUEBLES')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('8219059','MONTEPIO')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('8219067','PRESTAMISTA')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('8219122','EMPRESAS DE AUTOFINANCIAMIENTO AUTOMOTRIZ')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('8219130','EMPRESAS DE AUTOFINANCIAMIENTO RESIDENCIAL')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9900904','CASAS DE EMPE�O')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('8219114','ADMINISTRADORAS DE TARJETA DE CREDITO')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9900913','ADMINISTRADORAS DE TARJETA DE SERVICIOS')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9505001','VENTA DE TARJETAS PREPAGADAS')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9900914','ADMINISTRADORAS Y/O COMERCIALIZADORAS DE TARJETAS DE PREPAGO')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9900915','COMERCIALIZADORA DE CHEQUES DE VIAJERO')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('8219083','EMPRESAS CONTROLADORAS FINANCIERAS')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('8300006','SERVICIOS RELACIONADOS CON INMUEBLES')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('8314015','ADMINISTRACION DE INMUEBLES')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('8400004','SERVICIOS PROFESIONALES Y T�CNICOS')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('8412017','SERVICIOS DE BUFETES JURIDICOS')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('8413015','SERVICIOS DE CONTADURIA Y AUDITORIA; INCLUSO TENEDURIA DE LIBROS')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('8414013','SERVICIOS DE ASESORIA Y ESTUDIOS TECNICOS DE ARQUITECTURA E INGENIERIA (INCLUSO DISE�O INDUSTRIAL)')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('8419013','SERVICIO DE INVESTIGACION DE MERCADO  SOLVENCIA FINANCIERA, DE PATENTES  Y MARCAS INDUSTRIALES Y OTROS SIMILARES')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('8424012','SERVICIOS ADMINISTRATIVOS DE TRAMITE Y COBRANZA; INCLUSO ESCRITORIOS PUBLICOS')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('8411019','SERVICIOS DE NOTARIAS PUBLICAS')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9900925','SERVICIOS DE CORREDUR�AS PUBLICAS')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9100009','SERVICIOS DE ENSE�ANZA, INVESTIGACI�N CIENT�FICA Y DIFUSI�N CULTURAL')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9200007','SERVICIOS M�DICOS, DE ASISTENCIA SOCIAL Y VETERINARIOS')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9221011','CENTRO DE BENEFICENCIA')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9311010','ASOCIACIONES Y CONFEDERACIONES')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9311028','CAMARAS DE COMERCIO')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9311036','CAMARAS INDUSTRIALES')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9312018','ORGANIZACIONES DE ABOGADOS MEDICOS INGENIEROS Y OTRAS ASOCIACIONES DE PROFESIONALES')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9319014','ORGANIZACIONES CIVICAS')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9321019','ORGANIZACIONES LABORALES Y SINDICALES')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9322017','ORGANIZACIONES POLITICAS')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9331018','ORGANIZACIONES RELIGIOSAS')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9900926','OTRA ASOCIACI�N CIVIL O SOCIEDAD CIVIL')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9900927','OTRA INSTUTUCION DE ASISTENCIA PRIVADA, INSTITUCION DE BENEFICENCIA PRIVADA O ASOCIACI�N DE ASISTENCIA PRIVADA')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('8600000','SERVICIOS DE ALOJAMIENTO TEMPORAL')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('8700008','PREPARACI�N Y SERVICIO DE ALIMENTOS Y BEBIDAS')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('8711021','RESTAURANTE')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('8721012','BARES Y CANTINAS')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('8800006','SERVICIOS RECREATIVOS Y DE ESPARCIMIENTO')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('8829048','PROMOCION DE ESPECTACULOS DEPORTIVOS')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('8831019','CENTRO NOCTURNO')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('8833015','FEDERACIONES Y ASOCIACIONES DEPORTIVAS Y OTRAS CON FINES RECREATIVOS')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('8900004','SERVICIOS PERSONALES, PARA EL HOGAR Y DIVERSOS')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9900923','SERVICIOS DE BLINDAJE DE VEH�CULOS TERRESTRES Y/O INMUEBLES O PARTES DE ELLOS')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('8911019','TALLER DE REPARACION GENERAL DE AUTOMOVILES Y CAMIONES')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('8914013','SERVICIOS DE REPARACION DE CARROCERIAS PINTURA TAPICERIA HOJALATERIA Y CRISTALES DE AUTOMOVILES')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('8916019','ESTACIONAMIENTO PRIVADO PARA VEHICULOS')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('8916027','ESTACIONAMIENTO PUBLICO PARA VEHICULOS')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('8991011','QUEHACERES DEL HOGAR')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9900905','ESTUDIANTE � MENOR DE EDAD SIN OCUPACI�N')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9900906','DESEMPLEADO')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9900907','JUBILADO')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9900908','PENSIONADO')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9900909','AMA DE CASA')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9900930','MINISTROS DE CULTO RELIGIOSO (SACERDOTE, PASTOR, MONJA, ETC)')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9501009','EMPLEADO DEL SECTOR PRIVADO')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9411018','GOBIERNO FEDERAL')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9411026','GOBIERNO ESTATAL')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9411034','GOBIERNO MUNICIPAL')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9411998','EMPLEADO PUBLICO')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9471012','PRESTACION DE SERVICIOS PUBLICOS Y SOCIALES')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9900003','SERVICIOS DE ORGANIZACIONES INTERNACIONALES Y OTROS ORGANISMOS EXTRATERRITORIALES')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9912016','CONSULADO')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9912024','GOBIERNO EXTRANJERO')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9800101','PRESIDENTE DE LA REPUBLICA')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9800102','SECRETARIA DE ESTADO,  TITULAR DE ENTIDAD / PROCURADOR GENERAL DE LA REP�BLICA')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9800103','SUBSECRETARIA DE ESTADO')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9800104','OFICIAL MAYOR')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9800105','TITULAR DE ENTIDAD')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9800106','JEFATURA DE UNIDAD')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9800107','DIRECCI�N GENERAL U HOMOLOGA')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9800112','DIPUTADO  DEL H. CONGRESO DE LA UNI�N')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9800113','SENADOR DE LA REPUBLICA')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9800114','SECRETARIA DE LA DEFENSA NACIONAL (MILITAR)')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9800115','SECRETARIA DE MARINA (MARINO)')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9800116','EMBAJADOR')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9800117','C�NSUL')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9800200','EMPLEADO DE GOBIERNO DE ENTIDAD FEDERATIVA')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9800201','GOBERNADOR CONSTITUCIONAL DEL ESTADO � JEFE DE GOBIERNO DEL DF')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9800202','SECRETARIO DEL RAMO')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9800203','PROCURADOR GENERAL DE JUSTICIA DEL ESTADO')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9800204','DIPUTADO AL H. CONGRESO DEL ESTADO')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9800205','SUB-PROCURADOR GENERAL DE JUSTICIA DEL ESTAD')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9800206','CONTRALOR DEL H. CONGRESO DEL ESTADO')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9800207','TESORERO DEL ESTADO')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9800208','SUB-SECRETARIO')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9800209','VOCAL EJECUTIVO')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9800210','OFICIAL MAYOR DE ENTIDAD FEDERATIVA')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9800211','DIRECTOR GENERAL')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9800212','DIRECTOR EJECUTIVO')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9800213','JEFE DE LA POLIC�A JUDICIAL DEL ESTADO')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9800214','JEFE DE UNIDAD')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9800300','EMPLEADO DE GOBIERNO MUNICIPAL O DELEGACIONES (D.F)')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9800301','PRESIDENTE MUNICIPAL O DELEGADO')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9800302','SUB-DELEGADO')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9800303','REGIDOR')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9800304','DIRECTOR GENERAL')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9800400','EMPLEADO DEL PODER JUDICIAL')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9800401','MINISTRO DE LA SUPREMA CORTE DE JUSTICIA DE LA NACI�N')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9800402','MAGISTRADO FEDERAL')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9800403','JUEZ FEDERAL')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9800404','EMPLEADO DEL PODER JUDICIAL FEDERAL (SECRETARIOS, ACTUARIOS, ETC)')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9800405','MAGISTRADO DE ENTIDAD FEDERATIVA')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9800406','JUEZ DE ENTIDAD FEDERATIVA')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9800407','EMPLEADO DEL PODER JUDICIAL DE ENTIDAD FEDERATIVA (SECRETARIOS, ACTUARIOS, ETC)')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9800408','JUEZ MUNICIPAL � DE DELEGACI�N')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9800409','EMPLEADO DEL PODER JUDICIAL DE MUNICIPIO � DELEGACI�N (SECRETARIOS, ACTUARIOS, ETC)')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9900900','DEPENDENCIAS DE GOBIERNO O EMPRESAS PARAESTATALES')
INSERT INTO OCCUPATIONS (OC_CODE, OC_DESCRIPTION) VALUES('9999999','OTROS')
GO



