/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_100]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 120;

SET @New_ReleaseId = 121;
SET @New_ScriptName = N'UpdateTo_18.121.sql';
SET @New_Description = N'New release v03.007.0030'; 

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/**** GENERAL PARAM *****/



/******* TABLES  *******/



/******* RECORDS *******/



/******* PROCEDURES *******/
/* ConcatOpeningTime */
IF  EXISTS (SELECT * FROM sys.objects WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[ConcatOpeningTime]') AND TYPE IN (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[ConcatOpeningTime]
GO

CREATE FUNCTION [dbo].[ConcatOpeningTime]
  (@SiteId int,
   @Date   datetime)
RETURNS datetime
AS
BEGIN
  DECLARE @closing_hour    int
  DECLARE @closing_minutes int
  DECLARE @today_opening  datetime

  SET @closing_hour = ISNULL((SELECT   gp_key_value
                                FROM   general_params
                               WHERE   gp_group_key   = 'WigosGUI'
                                 AND   gp_subject_key = 'ClosingTime'), 6)

  SET @closing_minutes = ISNULL((SELECT   gp_key_value
                                   FROM   general_params
                                  WHERE   gp_group_key   = 'WigosGUI'
                                    AND   gp_subject_key = 'ClosingTimeMinutes'), 0)

  -- Trunc date to start of the day (00:00h).
  SET @today_opening = DATEADD(DAY, DATEDIFF(day, 0, @Date), 0)
  
  SET @today_opening = DATEADD(HOUR, @closing_hour, @today_opening)
  SET @today_opening = DATEADD(MINUTE, @closing_minutes, @today_opening)

  RETURN @today_opening
END -- ConcatOpeningTime
GO

/* Opening */
IF  EXISTS (SELECT * FROM sys.objects WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[Opening]') AND TYPE IN (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[Opening]
GO

CREATE FUNCTION [dbo].[Opening]
  (@SiteId int,
   @Date   datetime)
RETURNS datetime
AS
BEGIN
  DECLARE @now           datetime
  DECLARE @today_opening datetime

  SET @now = @Date
  SET @today_opening = dbo.ConcatOpeningTime(@SiteId, @now)

  IF @today_opening > @now
  BEGIN
    SET @today_opening = DATEADD(DAY, -1, @today_opening)
  END

  RETURN @today_opening
END -- Opening
GO

/* GamingDayFromDateTime */
IF  EXISTS (SELECT * FROM sys.objects WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[GamingDayFromDateTime]') AND TYPE IN (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[GamingDayFromDateTime]
GO

CREATE FUNCTION [dbo].[GamingDayFromDateTime] 
(
			@DateTime DATETIME
)
RETURNS int
AS
BEGIN
			DECLARE @_open               AS DATETIME
			DECLARE @_gaming_day    AS INT
			
			SET @_open       = dbo.Opening (0, @DateTime)
			SET @_gaming_day = YEAR (@_open) * 10000 + MONTH(@_open) * 100 + DAY(@_open)

			RETURN @_gaming_day

END -- GamingDayFromDateTime
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_AGG_ReportProfitByMachineDay]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_AGG_ReportProfitByMachineDay]
GO

CREATE  PROCEDURE [dbo].[SP_AGG_ReportProfitByMachineDay]
  @pSites        NVARCHAR(256),
  @pTerminals    NVARCHAR(4000) = NULL,
  @pFrom         BIGINT,
  @pTo           BIGINT,
  @pOnlyClosed   BIT,
  @pIncrement    MONEY = NULL
AS
BEGIN

DECLARE @_QUERY  AS NVARCHAR(MAX);
DECLARE @_THEORIAL_PAYOUT  AS NVARCHAR(3);

SELECT @_THEORIAL_PAYOUT = ISNULL(GP_KEY_VALUE, 95) FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PlayerTracking' AND GP_SUBJECT_KEY = 'TerminalDefaultPayout'

SET @_QUERY = '
    SELECT   EMD_SITE_ID AS SITE
           , TE_NAME AS TERMINAL_NAME
           , GM_GAME_NAME AS GAME
           , TT_NAME AS TERMINAL_TYPE
           , CASE WHEN SUM(EMD_MC_0005_INCREMENT) > 0 
                  THEN (CAST(SUM(EMD_MC_0000_INCREMENT) AS DECIMAL) / CAST(SUM(EMD_MC_0005_INCREMENT) AS DECIMAL))
                  ELSE 0 END AS BET_AVG           
           , SUM(EMD_MC_0000_INCREMENT) AS BETS
           , SUM(EMD_MC_0001_INCREMENT) AS PAYMENTS
           , SUM(EMD_MC_0002_INCREMENT) AS JACKPOTS
           , EMD_WORKING_DAY AS WORKING_DAY
           , SUM(EMD_MC_0000_INCREMENT - EMD_MC_0001_INCREMENT - EMD_MC_0002_INCREMENT) AS PROFIT           
           , SUM(EMD_MC_0000_INCREMENT - EMD_MC_0001_INCREMENT - EMD_MC_0002_INCREMENT) AS PROFIT_AVG           
           , CASE WHEN SUM(EMD_MC_0000_INCREMENT) > 0 
                  THEN (CAST(SUM(EMD_MC_0001_INCREMENT + EMD_MC_0002_INCREMENT) AS DECIMAL) / CAST(SUM(EMD_MC_0000_INCREMENT) AS DECIMAL))*100
                  ELSE 0 END AS PAYOUT
           ,ISNULL(TE_THEORETICAL_PAYOUT, ' + @_THEORIAL_PAYOUT + ') AS THEORICAL_PAYOUT
      FROM   EGM_METERS_BY_DAY '

IF @pOnlyClosed = 1
BEGIN
SET @_QUERY = @_QUERY + '
INNER JOIN   EGM_DAILY ON ED_WORKING_DAY = EMD_WORKING_DAY AND ED_SITE_ID = EMD_SITE_ID AND ED_STATUS = 1 '
END
      
SET @_QUERY = @_QUERY + '
INNER JOIN   TERMINALS ON TE_TERMINAL_ID = EMD_TERMINAL_ID '

IF @pTerminals IS NOT NULL
BEGIN
SET @_QUERY = @_QUERY + '
AND TE_TERMINAL_ID IN ( ' + @pTerminals + ' ) '
END

SET @_QUERY = @_QUERY + '
INNER JOIN   TERMINAL_TYPES ON TT_TYPE = TE_TYPE
LEFT  JOIN   GAMES ON GM_GAME_ID = EMD_GAME_ID
     WHERE   EMD_WORKING_DAY >= ' + CAST(@pFrom AS NVARCHAR) + ' 
       AND   EMD_WORKING_DAY < ' + CAST(@pTo AS NVARCHAR) + '
       AND   EMD_SITE_ID IN ( ' + @pSites + ')
  GROUP BY   EMD_SITE_ID, EMD_WORKING_DAY, TE_NAME, GM_GAME_NAME, TT_NAME, TE_THEORETICAL_PAYOUT '
  
IF @pIncrement IS NOT NULL
BEGIN  
SET @_QUERY = @_QUERY + '  
    HAVING   SUM(EMD_MC_0000_INCREMENT - EMD_MC_0001_INCREMENT - EMD_MC_0002_INCREMENT) > ' + CAST(@pIncrement AS NVARCHAR) + ' '
END

SET @_QUERY = @_QUERY + '
ORDER BY EMD_SITE_ID ASC, TE_NAME ASC, EMD_WORKING_DAY ASC'

EXEC (@_QUERY)

END
GO

GRANT EXECUTE ON [dbo].[SP_AGG_ReportProfitByMachineDay] TO [wggui] WITH GRANT OPTION
GO


/******* TRIGGERS *******/
