/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_100]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 71;

SET @New_ReleaseId = 72;
SET @New_ScriptName = N'UpdateTo_18.072.sql';
SET @New_Description = N'Update SP for multiple account block reason';
/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/

/****** INDEXES ******/

/****** RECORDS ******/

/******* STORED PROCEDURES *******/

--
-- BlockReason_ToNewEnumerate
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[BlockReason_ToNewEnumerate]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
  DROP FUNCTION [dbo].[BlockReason_ToNewEnumerate]
GO
CREATE FUNCTION BlockReason_ToNewEnumerate
(@pBlockReason INT) returns INT
AS
BEGIN
      DECLARE @Result INT
      
    IF @pBlockReason > 0 AND @pBlockReason < 256
              SET @Result  = POWER(2, @pBlockReason + 7) -- 2^(AC_BLOCK_REASON-1) x 256
    ELSE 
              SET @Result = @pBlockReason
              
      RETURN @Result
END -- BlockReason_ToNewEnumerate
GO

GRANT EXECUTE ON [dbo].[BlockReason_ToNewEnumerate] TO [wggui] WITH GRANT OPTION 
GO

--
-- Update_PersonalInfo
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Update_PersonalInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Update_PersonalInfo]
GO
CREATE PROCEDURE [dbo].[Update_PersonalInfo]
    @pCallingSiteId              int
  , @pAccountId                  bigint
  , @pTrackData                  nvarchar(50)
  , @pAccountCreated             Datetime 
  , @pHolderName                 nvarchar(200)
  , @pHolderId                   nvarchar(20)
  , @pHolderIdType               int
  , @pHolderAddress01            nvarchar(50)
  , @pHolderAddress02            nvarchar(50)
  , @pHolderAddress03            nvarchar(50)
  , @pHolderCity                 nvarchar(50)
  , @pHolderZip                  nvarchar(10) 
  , @pHolderEmail01              nvarchar(50)
  , @pHolderEmail02              nvarchar(50)
  , @pHolderTwitter              nvarchar(50)
  , @pHolderPhoneNumber01        nvarchar(20)
  , @pHolderPhoneNumber02        nvarchar(20)
  , @pHolderComments             nvarchar(100)
  , @pHolderId1                  nvarchar(20)
  , @pHolderId2                  nvarchar(20)
  , @pHolderDocumentId1          bigint
  , @pHolderDocumentId2          bigint
  , @pHolderName1                nvarchar(50)
  , @pHolderName2                nvarchar(50)
  , @pHolderName3                nvarchar(50)
  , @pHolderGender               int
  , @pHolderMaritalStatus        int
  , @pHolderBirthDate            datetime
  , @pHolderWeddingDate          datetime
  , @pHolderLevel                int
  , @pHolderLevelNotify          int
  , @pHolderLevelEntered         datetime
  , @pHolderLevelExpiration      datetime
  , @pPin                        nvarchar(12)
  , @pPinFailures                int
  , @pPinLastModified            datetime
  , @pBlocked                    bit
  , @pActivated                  bit
  , @pBlockReason                int
  , @pHolderIsVip                int
  , @pHolderTitle                nvarchar(15)                       
  , @pHolderName4                nvarchar(50)                       
  , @pHolderPhoneType01          int
  , @pHolderPhoneType02          int
  , @pHolderState                nvarchar(50)                    
  , @pHolderCountry              nvarchar(50) 
  , @pUserType                   int
  , @pPersonalInfoSeqId          int
  , @pPointsStatus               int
  , @pDeposit                    money
  , @pCardPay                    bit
  , @pBlockDescription           nvarchar(256) 
  , @pMSCreatedOnSiteId          Int 
  , @pExternalReference          nvarchar(50) 
  , @pHolderOccupation           nvarchar(50) 	
  , @pHolderExtNum               nvarchar(10) 
  , @pHolderNationality          Int 
  , @pHolderBirthCountry         Int 
  , @pHolderFedEntity            Int 
  , @pHolderId1Type              Int -- RFC
  , @pHolderId2Type              Int -- CURP
  , @pHolderId3Type              nvarchar(50)   
  , @pHolderId3                  nvarchar(20) 
  , @pHolderHasBeneficiary       bit  
  , @pBeneficiaryName            nvarchar(200) 
  , @pBeneficiaryName1           nvarchar(50) 
  , @pBeneficiaryName2           nvarchar(50) 
  , @pBeneficiaryName3           nvarchar(50) 
  , @pBeneficiaryBirthDate       Datetime 
  , @pBeneficiaryGender          int 
  , @pBeneficiaryOccupation      nvarchar(50) 
  , @pBeneficiaryId1Type         int   -- RFC
  , @pBeneficiaryId1             nvarchar(20) 
  , @pBeneficiaryId2Type         int   -- CURP
  , @pBeneficiaryId2             nvarchar(20) 
  , @pBeneficiaryId3Type         nvarchar(50)  
  , @pBeneficiaryId3  	         nvarchar(20)   
  , @pHolderOccupationId         int
  , @pBeneficiaryOccupationId    int
  , @pOutputStatus               INT               OUTPUT
  , @pOutputAccountOtherId       BIGINT            OUTPUT
  AS
  BEGIN

  DECLARE @pOtherAccountId as BIGINT
  DECLARE @IsNewAccount    as BIT
  DECLARE @PreviousUserType as INT
  DECLARE @PreviousPersonalInfoSeqId as BIGINT
  DECLARE @NumAccounts as INT
	DECLARE @AcBlocked_new as BIT
	
  SET @pOutputStatus = 0
  SET @pOutputAccountOtherId = 0
  
  SET @pUserType = ISNULL(@pUserType,CASE WHEN ISNULL(@pHolderLevel,0) > 0  THEN 1 ELSE 0 END)
  
  SET @NumAccounts = (SELECT COUNT(AC_ACCOUNT_ID) FROM ACCOUNTS WHERE AC_ACCOUNT_ID <> @pAccountId AND AC_HOLDER_ID = @pHolderId)
  IF @NumAccounts > 0 
  BEGIN
    SET @pOutputStatus = 1
  END

  SET @pOtherAccountId = ISNULL ((SELECT AC_ACCOUNT_ID FROM ACCOUNTS WHERE AC_TRACK_DATA = @pTrackData), 0)
  IF @pOtherAccountId <> 0 AND @pOtherAccountId <> @pAccountId
  BEGIN  
    UPDATE   ACCOUNTS 
       SET   AC_TRACK_DATA = '00000000000000000000-RECYCLED-' + CAST (@pOtherAccountId AS NVARCHAR) 
     WHERE   AC_ACCOUNT_ID = @pOtherAccountId

    SET @pOutputStatus = 2
    SET @pOutputAccountOtherId = @pOtherAccountId
  END

  SELECT @PreviousPersonalInfoSeqId = AC_MS_PERSONAL_INFO_SEQ_ID FROM ACCOUNTS WHERE AC_ACCOUNT_ID = @pAccountId 

  IF (@PreviousPersonalInfoSeqId IS NOT NULL AND @PreviousPersonalInfoSeqId > @pPersonalInfoSeqId)
  BEGIN
    SET @pOutputStatus = 3
    SELECT @pOutputAccountOtherId = ISNULL(AC_MS_LAST_SITE_ID, 0) FROM ACCOUNTS WHERE AC_ACCOUNT_ID = @pAccountId 
  END

  SET @IsNewAccount = 0
  IF NOT EXISTS (SELECT 1 FROM ACCOUNTS WHERE ac_account_id = @pAccountId )  
  BEGIN
    SET @IsNewAccount = 1

    INSERT INTO   ACCOUNTS 
                ( AC_ACCOUNT_ID, AC_TYPE, AC_BLOCKED, AC_MS_LAST_SITE_ID, AC_CREATED) 
         VALUES ( @pAccountId,         2,          1, @pCallingSiteId   , @pAccountCreated)
  END 
  ELSE
  BEGIN  
    SELECT @PreviousUserType = AC_USER_TYPE FROM ACCOUNTS WHERE AC_ACCOUNT_ID = @pAccountId     
    IF (@PreviousUserType = 1 AND @pUserType = 0) 
    BEGIN
      -- TODO: Make Alarm.
      -- SET @pOutputStatus = 4
      
      SET @pUserType = 1
    END
  END
   
   SELECT @AcBlocked_new = CASE WHEN (AC_BLOCK_REASON & 0x20000) = 0x20000 THEN 1
                                      ELSE @pBlocked END
                          FROM   ACCOUNTS
                         WHERE   AC_ACCOUNT_ID = @pAccountId
   
  UPDATE   ACCOUNTS                   
     SET   AC_TRACK_DATA                = @pTrackData 
         , AC_HOLDER_NAME               = @pHolderName 
         , AC_HOLDER_ID                 = @pHolderId 
         , AC_HOLDER_ID_TYPE            = @pHolderIdType 
         , AC_HOLDER_ADDRESS_01         = @pHolderAddress01 
         , AC_HOLDER_ADDRESS_02         = @pHolderAddress02 
         , AC_HOLDER_ADDRESS_03         = @pHolderAddress03 
         , AC_HOLDER_CITY               = @pHolderCity 
         , AC_HOLDER_ZIP                = @pHolderZip  
         , AC_HOLDER_EMAIL_01           = @pHolderEmail01 
         , AC_HOLDER_EMAIL_02           = @pHolderEmail02 
         , AC_HOLDER_TWITTER_ACCOUNT    = @pHolderTwitter 
         , AC_HOLDER_PHONE_NUMBER_01    = @pHolderPhoneNumber01 
         , AC_HOLDER_PHONE_NUMBER_02    = @pHolderPhoneNumber02 
         , AC_HOLDER_COMMENTS           = @pHolderComments 
         , AC_HOLDER_ID1                = @pHolderId1 
         , AC_HOLDER_ID2                = @pHolderId2 
         , AC_HOLDER_DOCUMENT_ID1       = @pHolderDocumentId1 
         , AC_HOLDER_DOCUMENT_ID2       = @pHolderDocumentId2 
         , AC_HOLDER_NAME1              = @pHolderName1 
         , AC_HOLDER_NAME2              = @pHolderName2 
         , AC_HOLDER_NAME3              = @pHolderName3 
         , AC_HOLDER_GENDER             = @pHolderGender  
         , AC_HOLDER_MARITAL_STATUS     = @pHolderMaritalStatus 
         , AC_HOLDER_BIRTH_DATE         = @pHolderBirthDate 
         , AC_HOLDER_WEDDING_DATE       = @pHolderWeddingDate 
         , AC_HOLDER_LEVEL              = CASE WHEN (@IsNewAccount = 0) AND (AC_USER_TYPE = @pUserType)  THEN AC_HOLDER_LEVEL            ELSE @pHolderLevel           END
         , AC_HOLDER_LEVEL_NOTIFY       = CASE WHEN (@IsNewAccount = 0) AND (AC_USER_TYPE = @pUserType)  THEN AC_HOLDER_LEVEL_NOTIFY     ELSE @pHolderLevelNotify     END 
         , AC_HOLDER_LEVEL_ENTERED      = CASE WHEN (@IsNewAccount = 0) AND (AC_USER_TYPE = @pUserType)  THEN AC_HOLDER_LEVEL_ENTERED    ELSE @pHolderLevelEntered    END  
         , AC_HOLDER_LEVEL_EXPIRATION   = CASE WHEN (@IsNewAccount = 0) AND (AC_USER_TYPE = @pUserType)  THEN AC_HOLDER_LEVEL_EXPIRATION ELSE @pHolderLevelExpiration END   
         , AC_PIN                       = @pPin 
         , AC_PIN_FAILURES              = @pPinFailures 
         , AC_PIN_LAST_MODIFIED         = @pPinLastModified 
         
         , AC_BLOCKED                   = @AcBlocked_new
                                               
         , AC_ACTIVATED                 = @pActivated 
         
         , AC_BLOCK_REASON              = CASE WHEN @AcBlocked_new = 0 THEN 0 ELSE (AC_BLOCK_REASON | dbo.BlockReason_ToNewEnumerate(@pBlockReason)) END 
         
         , AC_HOLDER_IS_VIP             = @pHolderIsVip 
         , AC_HOLDER_TITLE              = @pHolderTitle 
         , AC_HOLDER_NAME4              = @pHolderName4         
         , AC_HOLDER_PHONE_TYPE_01      = @pHolderPhoneType01  
         , AC_HOLDER_PHONE_TYPE_02      = @pHolderPhoneType02  
         , AC_HOLDER_STATE              = @pHolderState    
         , AC_HOLDER_COUNTRY            = @pHolderCountry   
         , AC_USER_TYPE                 = @pUserType
         , AC_POINTS_STATUS             = CASE WHEN (@pUserType = 0) THEN NULL ELSE CASE WHEN (@IsNewAccount = 0) THEN @pPointsStatus ELSE 0 END END           
         , AC_DEPOSIT                   = @pDeposit 
         , AC_CARD_PAID                 = @pCardPay 
         , AC_BLOCK_DESCRIPTION         = @pBlockDescription          
         , AC_MS_CREATED_ON_SITE_ID     = ISNULL(AC_MS_CREATED_ON_SITE_ID, ISNULL(@pMSCreatedOnSiteId, @pCallingSiteId))
         , AC_EXTERNAL_REFERENCE        = ISNULL(@pExternalReference,AC_EXTERNAL_REFERENCE)
         , AC_HOLDER_OCCUPATION         = @pHolderOccupation
         , AC_HOLDER_EXT_NUM            = @pHolderExtNum
         , AC_HOLDER_NATIONALITY        = @pHolderNationality 
         , AC_HOLDER_BIRTH_COUNTRY      = @pHolderBirthCountry 
         , AC_HOLDER_FED_ENTITY         = @pHolderFedEntity
         , AC_HOLDER_ID1_TYPE           = @pHolderId1Type 
         , AC_HOLDER_ID2_TYPE           = @pHolderId2Type 
         , AC_HOLDER_ID3_TYPE           = @pHolderId3Type 
         , AC_HOLDER_ID3                = @pHolderId3
         , AC_HOLDER_HAS_BENEFICIARY    = @pHolderHasBeneficiary 
         , AC_BENEFICIARY_NAME          = @pBeneficiaryName 
         , AC_BENEFICIARY_NAME1         = @pBeneficiaryName1
         , AC_BENEFICIARY_NAME2         = @pBeneficiaryName2
         , AC_BENEFICIARY_NAME3         = @pBeneficiaryName3
         , AC_BENEFICIARY_BIRTH_DATE    = @pBeneficiaryBirthDate 
         , AC_BENEFICIARY_GENDER        = @pBeneficiaryGender 
         , AC_BENEFICIARY_OCCUPATION    = @pBeneficiaryOccupation
         , AC_BENEFICIARY_ID1_TYPE      = @pBeneficiaryId1Type 
         , AC_BENEFICIARY_ID1           = @pBeneficiaryId1 
         , AC_BENEFICIARY_ID2_TYPE      = @pBeneficiaryId2Type 
         , AC_BENEFICIARY_ID2           = @pBeneficiaryId2 
         , AC_BENEFICIARY_ID3_TYPE      = @pBeneficiaryId3Type 
         , AC_BENEFICIARY_ID3           = @pBeneficiaryId3 
         , AC_HOLDER_OCCUPATION_ID      = ISNULL(@pHolderOccupationId, AC_HOLDER_OCCUPATION_ID)
         , AC_BENEFICIARY_OCCUPATION_ID = ISNULL(@pBeneficiaryOccupationId, AC_BENEFICIARY_OCCUPATION_ID)
   WHERE   AC_ACCOUNT_ID                = @pAccountId 
       
END
GO

--
-- Update_PersonalInfoForOldVersion
--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Update_PersonalInfoForOldVersion]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Update_PersonalInfoForOldVersion]
GO
CREATE PROCEDURE [dbo].[Update_PersonalInfoForOldVersion]
    @pCallingSiteId int
  , @pAccountId bigint
  , @pTrackData nvarchar(50)
  , @pAccountCreated Datetime 
  , @pHolderName nvarchar(200)
  , @pHolderId nvarchar(20)
  , @pHolderIdType int
  , @pHolderAddress01 nvarchar(50)
  , @pHolderAddress02 nvarchar(50)
  , @pHolderAddress03 nvarchar(50)
  , @pHolderCity nvarchar(50)
  , @pHolderZip  nvarchar(10) 
  , @pHolderEmail01 nvarchar(50)
  , @pHolderEmail02 nvarchar(50)
  , @pHolderTwitter nvarchar(50)
  , @pHolderPhoneNumber01 nvarchar(20)
  , @pHolderPhoneNumber02 nvarchar(20)
  , @pHolderComments nvarchar(100)
  , @pHolderId1 nvarchar(20)
  , @pHolderId2 nvarchar(20)
  , @pHolderDocumentId1 bigint
  , @pHolderDocumentId2 bigint
  , @pHolderName1 nvarchar(50)
  , @pHolderName2 nvarchar(50)
  , @pHolderName3 nvarchar(50)
  , @pHolderGender  int
  , @pHolderMaritalStatus int
  , @pHolderBirthDate datetime
  , @pHolderWeddingDate datetime
  , @pHolderLevel int
  , @pHolderLevelNotify int
  , @pHolderLevelEntered datetime
  , @pHolderLevelExpiration datetime
  , @pPin nvarchar(12)
  , @pPinFailures int
  , @pPinLastModified datetime
  , @pBlocked bit
  , @pActivated bit
  , @pBlockReason int
  , @pHolderIsVip int
  , @pHolderTitle nvarchar(15)                       
  , @pHolderName4 nvarchar(50)                       
  , @pHolderPhoneType01  int
  , @pHolderPhoneType02  int
  , @pHolderState    nvarchar(50)                    
  , @pHolderCountry  nvarchar(50) 
  , @pUserType  int
  , @pPersonalInfoSeqId  int
  , @pPointsStatus int
  , @pDeposit money
  , @pCardPay bit
  , @pBlockDescription nvarchar(256) 
  , @pMSCreatedOnSiteId Int 
  , @pExternalReference nvarchar(50) 
  , @pHolderOccupation  nvarchar(50) 	
  , @pHolderExtNum      nvarchar(10) 
  , @pHolderNationality  Int 
  , @pHolderBirthCountry Int 
  , @pHolderFedEntity    Int 
  , @pHolderId1Type      Int -- RFC
  , @pHolderId2Type      Int -- CURP
  , @pHolderId3Type       nvarchar(50)   
  , @pHolderId3           nvarchar(20) 
  , @pHolderHasBeneficiary bit  
  , @pBeneficiaryName     nvarchar(200) 
  , @pBeneficiaryName1    nvarchar(50) 
  , @pBeneficiaryName2    nvarchar(50) 
  , @pBeneficiaryName3    nvarchar(50) 
  , @pBeneficiaryBirthDate Datetime 
  , @pBeneficiaryGender    int 
  , @pBeneficiaryOccupation nvarchar(50) 
  , @pBeneficiaryId1Type    int   -- RFC
  , @pBeneficiaryId1        nvarchar(20) 
  , @pBeneficiaryId2Type    int   -- CURP
  , @pBeneficiaryId2        nvarchar(20) 
  , @pBeneficiaryId3Type    nvarchar(50)  
  , @pBeneficiaryId3  	    nvarchar(20)   
  , @pOutputStatus INT               OUTPUT
  , @pOutputAccountOtherId BIGINT    OUTPUT
  AS
  BEGIN

  DECLARE @pOtherAccountId as BIGINT
  DECLARE @IsNewAccount    as BIT
  DECLARE @PreviousUserType as INT
  DECLARE @PreviousPersonalInfoSeqId as BIGINT
  DECLARE @NumAccounts as INT
  DECLARE @AcBlocked_new as BIT

  SET @pOutputStatus = 0
  SET @pOutputAccountOtherId = 0
  
  SET @pUserType = ISNULL(@pUserType,CASE WHEN ISNULL(@pHolderLevel,0) > 0  THEN 1 ELSE 0 END)
  
  SET @NumAccounts = (SELECT COUNT(AC_ACCOUNT_ID) FROM ACCOUNTS WHERE AC_ACCOUNT_ID <> @pAccountId AND AC_HOLDER_ID = @pHolderId)
  IF @NumAccounts > 0 
  BEGIN
    SET @pOutputStatus = 1
  END

  SET @pOtherAccountId = ISNULL ((SELECT AC_ACCOUNT_ID FROM ACCOUNTS WHERE AC_TRACK_DATA = @pTrackData), 0)
  IF @pOtherAccountId <> 0 AND @pOtherAccountId <> @pAccountId
  BEGIN  
    UPDATE   ACCOUNTS 
       SET   AC_TRACK_DATA = '00000000000000000000-RECYCLED-' + CAST (@pOtherAccountId AS NVARCHAR) 
     WHERE   AC_ACCOUNT_ID = @pOtherAccountId

    SET @pOutputStatus = 2
    SET @pOutputAccountOtherId = @pOtherAccountId
  END

  SELECT @PreviousPersonalInfoSeqId = AC_MS_PERSONAL_INFO_SEQ_ID FROM ACCOUNTS WHERE AC_ACCOUNT_ID = @pAccountId 

  IF (@PreviousPersonalInfoSeqId IS NOT NULL AND @PreviousPersonalInfoSeqId > @pPersonalInfoSeqId)
  BEGIN
    SET @pOutputStatus = 3
    SELECT @pOutputAccountOtherId = ISNULL(AC_MS_LAST_SITE_ID, 0) FROM ACCOUNTS WHERE AC_ACCOUNT_ID = @pAccountId 
  END

  SET @IsNewAccount = 0
  IF NOT EXISTS (SELECT 1 FROM ACCOUNTS WHERE ac_account_id = @pAccountId )  
  BEGIN
    SET @IsNewAccount = 1

    INSERT INTO   ACCOUNTS 
                ( AC_ACCOUNT_ID, AC_TYPE, AC_BLOCKED, AC_MS_LAST_SITE_ID, AC_CREATED) 
         VALUES ( @pAccountId,         2,          1, @pCallingSiteId   , @pAccountCreated)
  END 
  ELSE
  BEGIN  
    SELECT @PreviousUserType = AC_USER_TYPE FROM ACCOUNTS WHERE AC_ACCOUNT_ID = @pAccountId     
    IF (@PreviousUserType = 1 AND @pUserType = 0) 
    BEGIN
      -- TODO: Make Alarm.
      -- SET @pOutputStatus = 4
      
      SET @pUserType = 1
    END
  END
   
   SELECT @AcBlocked_new = CASE WHEN (AC_BLOCK_REASON & 0x20000) = 0x20000 THEN 1
                                      ELSE @pBlocked END
                          FROM   ACCOUNTS
                         WHERE   AC_ACCOUNT_ID = @pAccountId

  UPDATE   ACCOUNTS                   
     SET   AC_TRACK_DATA              = @pTrackData 
         , AC_HOLDER_NAME             = @pHolderName 
         , AC_HOLDER_ID               = @pHolderId 
         , AC_HOLDER_ID_TYPE          = @pHolderIdType 
         , AC_HOLDER_ADDRESS_01       = @pHolderAddress01 
         , AC_HOLDER_ADDRESS_02       = @pHolderAddress02 
         , AC_HOLDER_ADDRESS_03       = @pHolderAddress03 
         , AC_HOLDER_CITY             = @pHolderCity 
         , AC_HOLDER_ZIP              = @pHolderZip  
         , AC_HOLDER_EMAIL_01         = @pHolderEmail01 
         , AC_HOLDER_EMAIL_02         = @pHolderEmail02 
         , AC_HOLDER_TWITTER_ACCOUNT  = @pHolderTwitter 
         , AC_HOLDER_PHONE_NUMBER_01  = @pHolderPhoneNumber01 
         , AC_HOLDER_PHONE_NUMBER_02  = @pHolderPhoneNumber02 
         , AC_HOLDER_COMMENTS         = @pHolderComments 
         , AC_HOLDER_ID1              = @pHolderId1 
         , AC_HOLDER_ID2              = @pHolderId2 
         , AC_HOLDER_DOCUMENT_ID1     = @pHolderDocumentId1 
         , AC_HOLDER_DOCUMENT_ID2     = @pHolderDocumentId2 
         , AC_HOLDER_NAME1            = @pHolderName1 
         , AC_HOLDER_NAME2            = @pHolderName2 
         , AC_HOLDER_NAME3            = @pHolderName3 
         , AC_HOLDER_GENDER           = @pHolderGender  
         , AC_HOLDER_MARITAL_STATUS   = @pHolderMaritalStatus 
         , AC_HOLDER_BIRTH_DATE       = @pHolderBirthDate 
         , AC_HOLDER_WEDDING_DATE     = @pHolderWeddingDate 
         , AC_HOLDER_LEVEL            = CASE WHEN (@IsNewAccount = 0) AND (AC_USER_TYPE = @pUserType)  THEN AC_HOLDER_LEVEL            ELSE @pHolderLevel           END
         , AC_HOLDER_LEVEL_NOTIFY     = CASE WHEN (@IsNewAccount = 0) AND (AC_USER_TYPE = @pUserType)  THEN AC_HOLDER_LEVEL_NOTIFY     ELSE @pHolderLevelNotify     END 
         , AC_HOLDER_LEVEL_ENTERED    = CASE WHEN (@IsNewAccount = 0) AND (AC_USER_TYPE = @pUserType)  THEN AC_HOLDER_LEVEL_ENTERED    ELSE @pHolderLevelEntered    END  
         , AC_HOLDER_LEVEL_EXPIRATION = CASE WHEN (@IsNewAccount = 0) AND (AC_USER_TYPE = @pUserType)  THEN AC_HOLDER_LEVEL_EXPIRATION ELSE @pHolderLevelExpiration END   
         , AC_PIN                     = @pPin 
         , AC_PIN_FAILURES            = @pPinFailures 
         , AC_PIN_LAST_MODIFIED       = @pPinLastModified 
         , AC_BLOCKED                 = @AcBlocked_new
         , AC_ACTIVATED               = @pActivated 
         , AC_BLOCK_REASON            = CASE WHEN @AcBlocked_new = 0 THEN 0 ELSE (AC_BLOCK_REASON | dbo.BlockReason_ToNewEnumerate(@pBlockReason)) END 
         , AC_HOLDER_IS_VIP           = @pHolderIsVip 
         , AC_HOLDER_TITLE            = @pHolderTitle 
         , AC_HOLDER_NAME4            = @pHolderName4         
         , AC_HOLDER_PHONE_TYPE_01    = @pHolderPhoneType01  
         , AC_HOLDER_PHONE_TYPE_02    = @pHolderPhoneType02  
         , AC_HOLDER_STATE            = @pHolderState    
         , AC_HOLDER_COUNTRY          = @pHolderCountry   
         , AC_USER_TYPE               = @pUserType
         , AC_POINTS_STATUS           = CASE WHEN (@pUserType = 0) THEN NULL ELSE CASE WHEN (@IsNewAccount = 0) THEN @pPointsStatus ELSE 0 END END           
         , AC_DEPOSIT                 = @pDeposit 
         , AC_CARD_PAID               = @pCardPay 
         , AC_BLOCK_DESCRIPTION       = @pBlockDescription          
         , AC_MS_CREATED_ON_SITE_ID   = ISNULL(AC_MS_CREATED_ON_SITE_ID, ISNULL(@pMSCreatedOnSiteId, @pCallingSiteId))
         , AC_EXTERNAL_REFERENCE      = ISNULL(@pExternalReference,AC_EXTERNAL_REFERENCE)
         , AC_HOLDER_OCCUPATION       = @pHolderOccupation
         , AC_HOLDER_EXT_NUM          = @pHolderExtNum
         , AC_HOLDER_NATIONALITY      = @pHolderNationality 
         , AC_HOLDER_BIRTH_COUNTRY    = @pHolderBirthCountry 
         , AC_HOLDER_FED_ENTITY       = @pHolderFedEntity
         , AC_HOLDER_ID1_TYPE         = @pHolderId1Type 
         , AC_HOLDER_ID2_TYPE         = @pHolderId2Type 
         , AC_HOLDER_ID3_TYPE         = @pHolderId3Type 
         , AC_HOLDER_ID3              = @pHolderId3
         , AC_HOLDER_HAS_BENEFICIARY  = @pHolderHasBeneficiary 
         , AC_BENEFICIARY_NAME        = @pBeneficiaryName 
         , AC_BENEFICIARY_NAME1       = @pBeneficiaryName1
         , AC_BENEFICIARY_NAME2       = @pBeneficiaryName2
         , AC_BENEFICIARY_NAME3       = @pBeneficiaryName3
         , AC_BENEFICIARY_BIRTH_DATE  = @pBeneficiaryBirthDate 
         , AC_BENEFICIARY_GENDER      = @pBeneficiaryGender 
         , AC_BENEFICIARY_OCCUPATION  = @pBeneficiaryOccupation
         , AC_BENEFICIARY_ID1_TYPE    = @pBeneficiaryId1Type 
         , AC_BENEFICIARY_ID1         = @pBeneficiaryId1 
         , AC_BENEFICIARY_ID2_TYPE    = @pBeneficiaryId2Type 
         , AC_BENEFICIARY_ID2         = @pBeneficiaryId2 
         , AC_BENEFICIARY_ID3_TYPE    = @pBeneficiaryId3Type 
         , AC_BENEFICIARY_ID3         = @pBeneficiaryId3 
   WHERE   AC_ACCOUNT_ID              = @pAccountId 
       
END
GO

--
-- Migrate BlockReasons enumerate
--
DECLARE @ACCOUNT_ID BIGINT
DECLARE @ACCOUNTS_TABLE TABLE(AC_ACCOUNT_ID BIGINT)
DECLARE @DATETIME_INIT DATETIME
DECLARE @DATETIME_FINISH DATETIME

INSERT INTO @ACCOUNTS_TABLE(AC_ACCOUNT_ID)SELECT AC_ACCOUNT_ID FROM ACCOUNTS WHERE AC_BLOCK_REASON > 0 AND AC_BLOCK_REASON <= 10

SET @DATETIME_INIT = GETDATE()
SELECT 'BEGIN' _STATE, @DATETIME_INIT _DATETIME

DECLARE Curs CURSOR FOR SELECT AC_ACCOUNT_ID FROM @ACCOUNTS_TABLE

OPEN Curs
	FETCH NEXT FROM Curs INTO @ACCOUNT_ID

	WHILE @@FETCH_STATUS = 0
	BEGIN
		  UPDATE  ACCOUNTS  
		     SET  AC_BLOCK_REASON = POWER(2, AC_BLOCK_REASON + 7) -- 2^(AC_BLOCK_REASON-1) x 256
		   WHERE  AC_ACCOUNT_ID = @ACCOUNT_ID
		
		FETCH NEXT FROM Curs INTO @ACCOUNT_ID
	END -- WHILE
	
CLOSE Curs
DEALLOCATE Curs
SET @DATETIME_FINISH = GETDATE()

SELECT 'END' _STATE, @DATETIME_FINISH _DATETIME
SELECT DATEDIFF(MINUTE, @DATETIME_INIT, @DATETIME_FINISH) 'TOTAL TIME (minutes)'

GO
