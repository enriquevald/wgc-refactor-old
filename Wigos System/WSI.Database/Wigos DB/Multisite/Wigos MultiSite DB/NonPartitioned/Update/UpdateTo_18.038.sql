/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_100]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 37;

SET @New_ReleaseId = 38;
SET @New_ScriptName = N'UpdateTo_18.038.sql';
SET @New_Description = N'Index for sites_requests and new GP.';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO


/*******  INDEX ********/
--
-- OLD INDEX TO DELETE
--
--[IX_mr_unique_id]
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[sites_requests]') AND name = N'IX_mr_unique_id')
  DROP INDEX [IX_mr_unique_id] ON [dbo].[sites_requests] WITH ( ONLINE = OFF )
GO

--[IX_status_site_id] 
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[sites_requests]') AND name = N'IX_status_site_id')
  DROP INDEX [IX_status_site_id] ON [dbo].[sites_requests] WITH ( ONLINE = OFF )
GO

--
-- NEW INDEX
--
--[IX_sr_status_changed]  
IF  NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[sites_requests]') AND name = N'IX_sr_status_changed')
  CREATE NONCLUSTERED INDEX [IX_sr_status_changed] ON [dbo].[sites_requests] 
  (
        [sr_status_changed] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

--[IX_sr_site_id_mr_unique_id]   
IF  NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[sites_requests]') AND name = N'IX_sr_site_id_mr_unique_id')
  CREATE NONCLUSTERED INDEX [IX_sr_site_id_mr_unique_id] ON [dbo].[sites_requests] 
  (
        [sr_site_id] ASC,
        [sr_mr_unique_id] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

--[IX_sr_status] 
IF  NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[sites_requests]') AND name = N'IX_sr_status')
  CREATE NONCLUSTERED INDEX [IX_sr_status] ON [dbo].[sites_requests] 
  (
        [sr_status] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO


/****** RECORDS ******/

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PSAClient' AND GP_SUBJECT_KEY ='Constancias.ReportarCURP')
  INSERT INTO [dbo].[GENERAL_PARAMS]
             ([GP_group_key] ,[GP_subject_key] ,[GP_key_value], [gp_ms_download_type])
       VALUES
             ('PSAClient', 'Constancias.ReportarCURP', '0', '1');
GO

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PSAClient' AND GP_SUBJECT_KEY ='Salidas.ISR1')
  INSERT INTO [dbo].[GENERAL_PARAMS]
             ([GP_group_key] ,[GP_subject_key] ,[GP_key_value], [gp_ms_download_type])
       VALUES
             ('PSAClient', 'Salidas.ISR1', '1', '1');
GO

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PSAClient' AND GP_SUBJECT_KEY ='Salidas.ISR2')
  INSERT INTO [dbo].[GENERAL_PARAMS]
             ([GP_group_key] ,[GP_subject_key] ,[GP_key_value], [gp_ms_download_type])
       VALUES
             ('PSAClient', 'Salidas.ISR2', '1', '1');
GO


IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'ExternalLoyaltyProgram.Mode01' AND GP_SUBJECT_KEY = 'MaxQueuedTime')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE, GP_MS_DOWNLOAD_TYPE)
                      VALUES ('ExternalLoyaltyProgram.Mode01','MaxQueuedTime','20000',0)
GO
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'ExternalLoyaltyProgram.Mode01' AND GP_SUBJECT_KEY = 'IgnoreRequestPoints')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE, GP_MS_DOWNLOAD_TYPE)
                      VALUES ('ExternalLoyaltyProgram.Mode01','IgnoreRequestPoints','1',1)
GO

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'ExternalLoyaltyProgram.Mode01' AND GP_SUBJECT_KEY = 'WsRedeem.NumWorkers')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE, GP_MS_DOWNLOAD_TYPE)
                      VALUES ('ExternalLoyaltyProgram.Mode01','WsRedeem.NumWorkers','120',0)
GO
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'ExternalLoyaltyProgram.Mode01' AND GP_SUBJECT_KEY = 'WsPoints.NumWorkers')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE, GP_MS_DOWNLOAD_TYPE)
                      VALUES ('ExternalLoyaltyProgram.Mode01','WsPoints.NumWorkers','240',0)
GO
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'ExternalLoyaltyProgram.Mode01' AND GP_SUBJECT_KEY = 'WsConfirm.NumWorkers')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE, GP_MS_DOWNLOAD_TYPE)
                      VALUES ('ExternalLoyaltyProgram.Mode01','WsConfirm.NumWorkers','40',0)
GO

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'ExternalLoyaltyProgram.Mode01' AND GP_SUBJECT_KEY = 'WsPoints.Timeout')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE, GP_MS_DOWNLOAD_TYPE)
                      VALUES ('ExternalLoyaltyProgram.Mode01','WsPoints.Timeout','30000',0)
GO
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'ExternalLoyaltyProgram.Mode01' AND GP_SUBJECT_KEY = 'WsConfirm.Timeout')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE, GP_MS_DOWNLOAD_TYPE)
                      VALUES ('ExternalLoyaltyProgram.Mode01','WsConfirm.Timeout','30000',0)
GO
IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'ExternalLoyaltyProgram.Mode01' AND GP_SUBJECT_KEY = 'WsRedeem.Timeout')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE, GP_MS_DOWNLOAD_TYPE)
                      VALUES ('ExternalLoyaltyProgram.Mode01','WsRedeem.Timeout','30000',0)
GO
