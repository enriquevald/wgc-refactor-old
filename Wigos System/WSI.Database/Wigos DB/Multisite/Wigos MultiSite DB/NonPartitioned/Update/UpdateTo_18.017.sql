/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_100]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 16;

SET @New_ReleaseId = 17;
SET @New_ScriptName = N'UpdateTo_18.017.sql';
SET @New_Description = N'SP Insert_Elp01PlaySessions; SP Insert_GamePlaySessions; SP Update_PointsAccountMovement';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/

/****** VIEWS ******/

/****** CONSTRAINTS ******/

/****** FUNCTIONS ******/

/****** INDEXES ******/

/****** STORED PROCEDURES ******/
GO
--------------------------------------------------------------------------------
-- Copyright � 2013 Win Systems International
--------------------------------------------------------------------------------
--
--   MODULE NAME: Update_PointsAccountMovement.sql
--
--   DESCRIPTION: Update points to account & movement procedure
--
--        AUTHOR: Dani Dom�nguez
--
-- CREATION DATE: 07-MAR-2013
--
-- REVISION HISTORY:
--
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 07-MAR-2013 DDM    First release.
-- 07-MAR-2013 DDM    Add in/out parameters
-- 03-JUN-2013 DDM    Fixed Bug #812
--------------------------------------------------------------------------------


--------------------------------------------------------------------------------
-- PURPOSE: Update points to account & movement procedure
--
--  PARAMS:
--      - INPUT:
--           @pSiteId          
--           @pMovementId      
--           @pPlaySessionId   
--           @pAccountId       
--           @pTerminalId      
--           @pWcpSequenceId   
--           @pWcpTransactionId
--           @pDatetime        
--           @pType            
--           @pInitialBalance  
--           @pSubAmount       
--           @pAddAmount       
--           @pFinalBalance    
--           @pCashierId       
--           @pCashierName     
--           @pTperminalName   
--           @pOperationId     
--           @pDetails         
--           @pReasons         
--
--      - OUTPUT:
--           @pErrorCode           
--           @PointsSequenceId     
--           @Points               
--           @PointsStatus         
--           @HolderLevel          
--           @HolderLevelEntered   
--           @HolderLevelExpiration
--
-- RETURNS:
--
--   NOTES:
--
--------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Update_PointsAccountMovement]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Update_PointsAccountMovement]
GO

CREATE PROCEDURE [dbo].[Update_PointsAccountMovement]
	@pSiteId                    INT 
,	@pMovementId                BIGINT
,	@pPlaySessionId             BIGINT
,	@pAccountId                 BIGINT
,	@pTerminalId                INT
,	@pWcpSequenceId             BIGINT
,	@pWcpTransactionId          BIGINT
,	@pDatetime                  DATETIME
,	@pType                      INT
,	@pInitialBalance            MONEY
,	@pSubAmount                 MONEY
,	@pAddAmount                 MONEY
,	@pFinalBalance              MONEY
,	@pCashierId                 INT
,	@pCashierName               NVARCHAR(50)
,	@pTperminalName             NVARCHAR(50)
,	@pOperationId               BIGINT
,	@pDetails                   NVARCHAR(256)
,	@pReasons                   NVARCHAR(64)
, @pErrorCode                 INT          OUTPUT
, @PointsSequenceId           BIGINT       OUTPUT
, @Points                     MONEY        OUTPUT
,	@pCenterInitialBalance      MONEY        OUTPUT

AS
BEGIN   
  DECLARE @LocalDelta      AS MONEY 
  DECLARE @IsDelta         AS BIT
  DECLARE @pCheckAccountId AS BIGINT -- For check if exists account
  
  SET @pErrorCode = 1
  SET @PointsSequenceId = NULL
  SET @Points = NULL
  SET @pCenterInitialBalance = NULL
  SET @IsDelta = 1
  SET @pCheckAccountId = NULL
 
  SET NOCOUNT ON; 
 
  SELECT   @pCenterInitialBalance = AC_POINTS
         , @pCheckAccountId       = AC_ACCOUNT_ID 
    FROM   ACCOUNTS
   WHERE   AC_ACCOUNT_ID = @pAccountId 
 
  IF ( @pCheckAccountId IS NULL)
  BEGIN
    SET @pErrorCode = 1
    RETURN
  END
 
  IF NOT EXISTS (SELECT 1 FROM ACCOUNT_MOVEMENTS WHERE AM_SITE_ID = @pSiteId AND AM_MOVEMENT_ID = @pMovementId) 
  BEGIN  
    INSERT INTO   ACCOUNT_MOVEMENTS
                ( [am_site_id]
                , [am_movement_id]
                , [am_play_session_id]
                , [am_account_id]
                , [am_terminal_id]
                , [am_wcp_sequence_id]
                , [am_wcp_transaction_id]
                , [am_datetime]
                , [am_type]
                , [am_initial_balance]
                , [am_sub_amount]
                , [am_add_amount]
                , [am_final_balance]
                , [am_cashier_id]
                , [am_cashier_name]
                , [am_terminal_name]
                , [am_operation_id]
                , [am_details]
                , [am_reasons])
         VALUES
                ( @pSiteId           
                , @pMovementId       
                , @pPlaySessionId   
                , @pAccountId        
                , @pTerminalId       
                , @pWcpSequenceId    
                , @pWcpTransactionId 
                , @pDatetime         
                , @pType             
                , @pInitialBalance  
                , @pSubAmount        
                , @pAddAmount        
                , @pFinalBalance     
                , @pCashierId        
                , @pCashierName      
                , @pTperminalName    
                , @pOperationId      
                , @pDetails          
                , @pReasons  )          
  
    SET @LocalDelta = ISNULL (@pAddAmount - @pSubAmount, 0)
    
    -- Points Gift Delivery/Points Gift Services/Points Status/Imported Points History
    IF (@pType = 39  OR @pType = 42  OR @pType = 67 OR @pType = 73)  SET @LocalDelta = 0 
    
    -- Multisite initial points
    IF (@pType = 101) SET @IsDelta    = 0 
        
    UPDATE   ACCOUNTS
       SET   AC_POINTS           = CASE WHEN (@IsDelta = 1) THEN AC_POINTS + @LocalDelta ELSE @LocalDelta END
           , AC_MS_LAST_SITE_ID  = @pSiteId 
           , AC_LAST_ACTIVITY    = GETDATE()         
     WHERE   AC_ACCOUNT_ID       = @pAccountId 
    
  END
  
  SET @pErrorCode = 0
 
  SELECT   @PointsSequenceId     = AC_MS_POINTS_SEQ_ID 
         , @Points               = AC_POINTS
    FROM   ACCOUNTS
   WHERE   AC_ACCOUNT_ID = @pAccountId 
   
END -- Update_PointsAccountMovement
GO

--------------------------------------------------------------------------------
-- Copyright � 2013 Win Systems International 
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: Insert_GamePlaySessions
-- 
--   DESCRIPTION: Insert Game Play Sessions
-- 
--        AUTHOR: Alberto Marcos
-- 
-- CREATION DATE: 03-JUN-2013
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 03-JUN-2013 AMF    First release.
-------------------------------------------------------------------------------- 
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Insert_GamePlaySessions]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].Insert_GamePlaySessions
GO
CREATE PROCEDURE Insert_GamePlaySessions
  @pSiteId INT
, @pPlaySessionId BIGINT
, @pGameId INT
, @pAccountId BIGINT
, @pTerminalId INT
, @pPlayedCount INT
, @pPlayedAmount MONEY
, @pWonCount INT
, @pWonAmount MONEY
, @pPayout MONEY
AS
BEGIN   		  
  IF NOT EXISTS(SELECT   1 
				          FROM   GAME_PLAY_SESSIONS 
			           WHERE   GPS_PLAY_SESSION_ID = @pPlaySessionId
				           AND   GPS_GAME_ID         = @pGameId 
				           AND   GPS_SITE_ID         = @pSiteId)
              
  BEGIN
  INSERT INTO  GAME_PLAY_SESSIONS
  		       ( GPS_SITE_ID     
  		       , GPS_PLAY_SESSION_ID
 		         , GPS_GAME_ID      
		         , GPS_ACCOUNT_ID   
		         , GPS_TERMINAL_ID  
		         , GPS_PLAYED_COUNT 
		         , GPS_PLAYED_AMOUNT
 		         , GPS_WON_COUNT    
 		         , GPS_WON_AMOUNT   
 		         , GPS_PAYOUT )     
       VALUES                       
	           ( @pSiteId           
	           , @pPlaySessionId    
	           , @pGameId           
	           , @pAccountId        
	           , @pTerminalId       
	           , @pPlayedCount      
	           , @pPlayedAmount     
	           , @pWonCount         
	           , @pWonAmount        
	           , @pPayout )
  END
END -- Insert_GamePlaySessions
GO
--------------------------------------------------------------------------------
-- Copyright � 2013 Win Systems International 
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: Insert_Elp01PlaySessions
-- 
--   DESCRIPTION: Insert ELP01 Play Sessions
-- 
--        AUTHOR: Alberto Marcos
-- 
-- CREATION DATE: 03-JUN-2013
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 03-JUN-2013 AMF    First release.
-------------------------------------------------------------------------------- 
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Insert_Elp01PlaySessions]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].Insert_Elp01PlaySessions
GO
CREATE PROCEDURE Insert_Elp01PlaySessions
  @pSiteId INT
, @pId BIGINT
, @pTicketNumber DECIMAL
, @pSlotSerialNumber NVARCHAR(50)
, @pSlotHouseNumber NVARCHAR(40)
, @pVenueCode INT
, @pAreaCode INT
, @pBankCode INT
, @pVendorCode INT
, @pGameCode INT
, @pStartTime DATETIME
, @pEndTime DATETIME
, @pBetAmount DECIMAL
, @pPaidAmount DECIMAL
, @pGamesPlayed INT
, @pInitialAmount DECIMAL
, @pAditionalAmount DECIMAL
, @pFinalAmount DECIMAL
, @pBetCombCode INT
, @pKindofTicket INT
, @pSequenceNumber BIGINT
, @pCuponNumber DECIMAL
, @pDateUpdated DATETIME
, @pDateInserted DATETIME
, @pAccountId BIGINT
AS
BEGIN   		  
  IF NOT EXISTS(SELECT   1 
				          FROM   ELP01_PLAY_SESSIONS 
			           WHERE   EPS_TICKET_NUMBER = @pTicketNumber
				           AND   EPS_KINDOF_TICKET = @pKindofTicket 
				           AND   EPS_SITE_ID       = @pSiteId)
              
  BEGIN
  
  DECLARE @externalCardId as NUMERIC(20,0)
  DECLARE @externalAccountId as NUMERIC(15,0)

  SELECT   @externalCardId    = ET_EXTERNAL_CARD_ID
		     , @externalAccountId = ET_EXTERNAL_ACCOUNT_ID
    FROM   ELP01_TRACKDATA
   WHERE   ET_AC_ACCOUNT_ID   = @pAccountId
		  
  INSERT INTO  ELP01_PLAY_SESSIONS
             ( EPS_SITE_ID
             , EPS_ID
             , EPS_TICKET_NUMBER
             , EPS_CUSTOMER_NUMBER
             , EPS_SLOT_SERIAL_NUMBER
             , EPS_SLOT_HOUSE_NUMBER
             , EPS_VENUE_CODE
             , EPS_AREA_CODE
             , EPS_BANK_CODE
             , EPS_VENDOR_CODE
             , EPS_GAME_CODE
             , EPS_START_TIME
             , EPS_END_TIME
             , EPS_BET_AMOUNT
             , EPS_PAID_AMOUNT
             , EPS_GAMES_PLAYED
             , EPS_INITIAL_AMOUNT
             , EPS_ADITIONAL_AMOUNT
             , EPS_FINAL_AMOUNT
             , EPS_BET_COMB_CODE
             , EPS_KINDOF_TICKET
             , EPS_SEQUENCE_NUMBER
             , EPS_CUPON_NUMBER
             , EPS_DATE_UPDATED
             , EPS_CARD_NUMBER
             , EPS_DATE_INSERTED)
       VALUES 
			       ( @pSiteId
          	 , @pId
             , @pTicketNumber
             , @externalAccountId
             , @pSlotSerialNumber
             , @pSlotHouseNumber
             , @pVenueCode
             , @pAreaCode
             , @pBankCode
             , @pVendorCode
             , @pGameCode
             , @pStartTime
             , @pEndTime
             , @pBetAmount
             , @pPaidAmount
             , @pGamesPlayed
             , @pInitialAmount
             , @pAditionalAmount
             , @pFinalAmount
             , @pBetCombCode
             , @pKindofTicket
             , @pSequenceNumber
             , @pCuponNumber
             , @pDateUpdated
             , @externalCardId
             , @pDateInserted )
  END
END -- Insert_Elp01PlaySessions
GO
/****** TRIGGERS ******/

/****** RECORDS ******/
