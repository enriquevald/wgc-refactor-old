/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_100]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 126;

SET @New_ReleaseId = 127;
SET @New_ScriptName = N'UpdateTo_18.127.sql';
SET @New_Description = N'New release v03.007.0041'; 

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/**** GENERAL PARAM *****/


/******* TABLES  *******/



/******* RECORDS *******/



/******* PROCEDURES *******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_AGG_ReportProfitByMachineDay]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_AGG_ReportProfitByMachineDay]
GO

CREATE  PROCEDURE [dbo].[SP_AGG_ReportProfitByMachineDay]
  @pSites        NVARCHAR(256),
  @pTerminals    NVARCHAR(4000) = NULL,
  @pFrom         BIGINT,
  @pTo           BIGINT,
  @pOnlyClosed   BIT,
  @pIncrement    MONEY = NULL
AS
BEGIN

DECLARE @_QUERY  AS NVARCHAR(MAX);
DECLARE @_THEORIAL_PAYOUT  AS NVARCHAR(3);

SELECT @_THEORIAL_PAYOUT = ISNULL(GP_KEY_VALUE, 95) FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PlayerTracking' AND GP_SUBJECT_KEY = 'TerminalDefaultPayout'

SET @_QUERY = '
    SELECT   EMD_SITE_ID AS SITE
           , TE_NAME AS TERMINAL_NAME
           , TE_GAME_THEME AS GAME
           , TT_NAME AS TERMINAL_TYPE
           , CASE WHEN SUM(EMD_MC_0005_INCREMENT) > 0 
                  THEN (CAST(SUM(EMD_MC_0000_INCREMENT) AS DECIMAL) / CAST(SUM(EMD_MC_0005_INCREMENT) AS DECIMAL))
                  ELSE 0 END AS BET_AVG           
           , SUM(EMD_MC_0000_INCREMENT) AS BETS
           , SUM(EMD_MC_0001_INCREMENT) AS PAYMENTS
           , SUM(EMD_MC_0002_INCREMENT) AS JACKPOTS
           , EMD_WORKING_DAY AS WORKING_DAY
           , SUM(EMD_MC_0000_INCREMENT - EMD_MC_0001_INCREMENT - EMD_MC_0002_INCREMENT) AS PROFIT           
           , SUM(EMD_MC_0000_INCREMENT - EMD_MC_0001_INCREMENT - EMD_MC_0002_INCREMENT) AS PROFIT_AVG           
           , CASE WHEN SUM(EMD_MC_0000_INCREMENT) > 0 
                  THEN (CAST(SUM(EMD_MC_0001_INCREMENT + EMD_MC_0002_INCREMENT) AS DECIMAL) / CAST(SUM(EMD_MC_0000_INCREMENT) AS DECIMAL))*100
                  ELSE 0 END AS PAYOUT
           , ISNULL(TE_THEORETICAL_PAYOUT * 100, ' + @_THEORIAL_PAYOUT + ') AS THEORICAL_PAYOUT
		   , SUM(EMD_MC_0005_INCREMENT) AS NUM_PLAYS
      FROM   EGM_METERS_BY_DAY '

IF @pOnlyClosed = 1
BEGIN
SET @_QUERY = @_QUERY + '
INNER JOIN   EGM_DAILY ON ED_WORKING_DAY = EMD_WORKING_DAY AND ED_SITE_ID = EMD_SITE_ID AND ED_STATUS = 1 '
END
      
SET @_QUERY = @_QUERY + '
INNER JOIN   TERMINALS ON TE_TERMINAL_ID = EMD_TERMINAL_ID AND TE_SITE_ID = EMD_SITE_ID '

IF @pTerminals IS NOT NULL
BEGIN
SET @_QUERY = @_QUERY + '
AND TE_TERMINAL_ID IN ( ' + @pTerminals + ' ) '
END

SET @_QUERY = @_QUERY + '
INNER JOIN   TERMINAL_TYPES ON TT_TYPE = TE_TYPE
     WHERE   EMD_WORKING_DAY >= ' + CAST(@pFrom AS NVARCHAR) + ' 
       AND   EMD_WORKING_DAY < ' + CAST(@pTo AS NVARCHAR) + '
       AND   EMD_SITE_ID IN ( ' + @pSites + ')
  GROUP BY   EMD_SITE_ID, EMD_WORKING_DAY, TE_NAME, TE_GAME_THEME, TT_NAME, TE_THEORETICAL_PAYOUT '
  
IF @pIncrement IS NOT NULL
BEGIN  
SET @_QUERY = @_QUERY + '  
    HAVING   SUM(EMD_MC_0000_INCREMENT - EMD_MC_0001_INCREMENT - EMD_MC_0002_INCREMENT) > ' + CAST(@pIncrement AS NVARCHAR) + ' '
END

SET @_QUERY = @_QUERY + '
ORDER BY EMD_SITE_ID ASC, TE_NAME ASC, EMD_WORKING_DAY ASC'

EXEC (@_QUERY)

END
GO

GRANT EXECUTE ON [dbo].[SP_AGG_ReportProfitByMachineDay] TO [wggui] WITH GRANT OPTION
GO



/******* TRIGGERS *******/
