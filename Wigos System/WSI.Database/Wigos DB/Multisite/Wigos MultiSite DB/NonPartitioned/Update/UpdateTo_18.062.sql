/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_100]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 61;

SET @New_ReleaseId = 62;
SET @New_ScriptName = N'UpdateTo_18.062.sql';
SET @New_Description = N'Delete movement from cashier movements; Update alarm catalog';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/

/****** INDEXES ******/

/****** RECORDS ******/

-- DELETE MOVEMENT FROM CASHIER MOVEMENTS

-- CHIPS_SALE_REGISTER_CASH_IN_SPLIT1 = 309
-- CHIPS_SALE_REGISTER_CASH_IN_SPLIT2 = 310
-- CHIPS_SALE_REGISTER_CASH_IN_TAX_SPLIT1 = 312
-- CHIPS_SALE_REGISTER_CASH_IN = 313
-- CHIPS_SALE_REGISTER_CASH_IN_TAX_SPLIT2 = 314

DELETE FROM cashier_movements_grouped_by_hour WHERE cm_type IN (309, 310, 312, 313, 314)
GO

UPDATE   ALARM_CATALOG
   SET   ALCG_NAME='Top Screen - Unknown status'
       , ALCG_DESCRIPTION='Top Screen - Unknown status'
 WHERE   ALCG_ALARM_CODE=460288 AND ALCG_LANGUAGE_ID=9
GO

UPDATE   ALARM_CATALOG
   SET   ALCG_NAME='Top Screen - OK'
       , ALCG_DESCRIPTION='Top Screen - OK'
 WHERE   ALCG_ALARM_CODE=460289 AND ALCG_LANGUAGE_ID=9
GO     
       
UPDATE   ALARM_CATALOG
   SET   ALCG_NAME='Top Screen - Error'
       , ALCG_DESCRIPTION='Top Screen - Error'
 WHERE   ALCG_ALARM_CODE=460290 AND ALCG_LANGUAGE_ID=9
GO     
       
UPDATE   ALARM_CATALOG
   SET   ALCG_NAME='Top Screen - Not Installed'
       , ALCG_DESCRIPTION='Top Screen - Not Installed'
 WHERE   ALCG_ALARM_CODE=460388 AND ALCG_LANGUAGE_ID=9
GO     
       
UPDATE   ALARM_CATALOG
   SET   ALCG_NAME='Intrusion - OK'
       , ALCG_DESCRIPTION='Intrusion - OK'
 WHERE   ALCG_ALARM_CODE=460801 AND ALCG_LANGUAGE_ID=9
GO     
       
UPDATE   ALARM_CATALOG
   SET   ALCG_NAME='Intrusion � Detected'
       , ALCG_DESCRIPTION='Intrusion � Detected'
 WHERE   ALCG_ALARM_CODE=460802 AND ALCG_LANGUAGE_ID=9
GO

/******* STORED PROCEDURES *******/

