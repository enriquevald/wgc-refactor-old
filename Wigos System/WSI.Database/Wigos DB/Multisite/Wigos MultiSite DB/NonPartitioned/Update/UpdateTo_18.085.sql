/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_100]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 84;

SET @New_ReleaseId = 85;
SET @New_ScriptName = N'UpdateTo_18.085.sql';
SET @New_Description = N'Update elp01_trackdata, game_play_sessions, accounts, play_sessions';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/

/****** INDEXES ******/

/****** RECORDS ******/

CREATE PROCEDURE [dbo].[AddTimestampToTable] 	
	@sTableName VARCHAR(50),
	@sColumnName VARCHAR(50),
	@sIndexName VARCHAR(50)
AS
BEGIN
	DECLARE @tSql VARCHAR (500)
	SET @tSql ='IF NOT EXISTS(SELECT * 
					          FROM sys.columns 
						      WHERE Name = '+ CHAR(39)+ @sColumnName + CHAR(39)+' AND Object_ID = Object_ID('+ CHAR(39)+ @sTableName + CHAR(39)+'))
			   	BEGIN '
			   + ' ALTER TABLE [' + @sTableName + '] ADD [' + @sColumnName + '] TIMESTAMP; '
			   +'END '
		       +'IF NOT EXISTS(SELECT * 
			     			   FROM sys.indexes 
			     		       WHERE name='+ CHAR(39)+ @sIndexName + CHAR(39)+' AND object_id = OBJECT_ID('+CHAR(39)+ @sTableName +CHAR(39)+'))'
		       +'BEGIN'
		       +' CREATE UNIQUE INDEX '+ @sIndexName + ' ON [' + @sTableName + '] (' + @sColumnName + ');'
		       +'END';
	EXEC(@tSql);
END
GO

exec AddTimestampToTable 'elp01_trackdata', 'et_timestamp', 'IX_et_timestamp';
exec AddTimestampToTable 'game_play_sessions', 'gps_timestamp', 'IX_gsp_timestamp';

exec AddTimestampToTable 'accounts', 'ac_timestamp', 'IX_ac_timestamp';
exec AddTimestampToTable 'play_sessions', 'ps_ms_timestamp', 'IX_ps_ms_timestamp';

DROP PROCEDURE AddTimestampToTable