/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_100]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 21;

SET @New_ReleaseId = 22;
SET @New_ScriptName = N'UpdateTo_18.022.sql';
SET @New_Description = N'Update_PersonalInfo; MultiSiteTrigger_AccountUpdate; ADD gu_master_id; NEW TASK: DownloadCommonUsers; AntiMoneyLaundering;  ';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/

--
-- NEW TASK: USER CORP.
--
ALTER TABLE GUI_USERS ADD gu_master_id INT NULL
GO

ALTER TABLE SITES ADD st_elp BIT NULL
ALTER TABLE SITES ADD CONSTRAINT st_elp_default DEFAULT 0 FOR st_elp;
GO

--
-- AntiMoneyLaundering
--

ALTER TABLE dbo.accounts ADD ac_holder_has_beneficiary bit NOT NULL CONSTRAINT DF_accounts_ac_holder_has_beneficiary DEFAULT ((0))
GO

UPDATE dbo.accounts SET ac_holder_has_beneficiary = 1 WHERE ac_holder_as_beneficiary  = 0
GO

ALTER TABLE dbo.accounts DROP CONSTRAINT DF_ac_holder_as_beneficiary
GO

ALTER TABLE dbo.accounts DROP COLUMN ac_holder_as_beneficiary
GO

--
--
--
ALTER TABLE accounts ALTER COLUMN ac_holder_id3_type nvarchar(50)
ALTER TABLE accounts ALTER COLUMN ac_beneficiary_id3_type nvarchar(50)
GO

/****** VIEWS ******/

/****** CONSTRAINTS ******/

/****** FUNCTIONS ******/

/****** INDEXES ******/

/****** STORED PROCEDURES ******/

  --------------------------------------------------------------------------------
  -- Copyright � 2013 Win Systems International
  --------------------------------------------------------------------------------
  --
  --   MODULE NAME: Update_PersonalInfo.sql.sql
  --
  --   DESCRIPTION: Update personal Information 
  --
  --        AUTHOR: Dani Dom�nguez
  --
  -- CREATION DATE: 08-MAR-2013
  --
  -- REVISION HISTORY:
  --
  -- Date        Author Description
  -- ----------- ------ ----------------------------------------------------------
  -- 08-MAR-2013 DDM    First release.  
  -- 22-MAY-2013 DDM    Fixed bugs 783,793 and 693
  --                    Added field AC_BLOCK_DESCRIPTION
  -- 28-MAY-2013 DDM    Fixed bug #803
  --                    Added field AC_EXTERNAL_REFERENCE
  -- 03-JUL-2013 DDM    Added new fields about Money Laundering
  -------------------------------------------------------------------------------- 

  IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Update_PersonalInfo]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Update_PersonalInfo]
  GO
  CREATE PROCEDURE [dbo].[Update_PersonalInfo]
    @pCallingSiteId int
  , @pAccountId bigint
  , @pTrackData nvarchar(50)
  , @pAccountCreated Datetime 
  , @pHolderName nvarchar(200)
  , @pHolderId nvarchar(20)
  , @pHolderIdType int
  , @pHolderAddress01 nvarchar(50)
  , @pHolderAddress02 nvarchar(50)
  , @pHolderAddress03 nvarchar(50)
  , @pHolderCity nvarchar(50)
  , @pHolderZip  nvarchar(10) 
  , @pHolderEmail01 nvarchar(50)
  , @pHolderEmail02 nvarchar(50)
  , @pHolderTwitter nvarchar(50)
  , @pHolderPhoneNumber01 nvarchar(20)
  , @pHolderPhoneNumber02 nvarchar(20)
  , @pHolderComments nvarchar(100)
  , @pHolderId1 nvarchar(20)
  , @pHolderId2 nvarchar(20)
  , @pHolderDocumentId1 bigint
  , @pHolderDocumentId2 bigint
  , @pHolderName1 nvarchar(50)
  , @pHolderName2 nvarchar(50)
  , @pHolderName3 nvarchar(50)
  , @pHolderGender  int
  , @pHolderMaritalStatus int
  , @pHolderBirthDate datetime
  , @pHolderWeddingDate datetime
  , @pHolderLevel int
  , @pHolderLevelNotify int
  , @pHolderLevelEntered datetime
  , @pHolderLevelExpiration datetime
  , @pPin nvarchar(12)
  , @pPinFailures int
  , @pPinLastModified datetime
  , @pBlocked bit
  , @pActivated bit
  , @pBlockReason int
  , @pHolderIsVip int
  , @pHolderTitle nvarchar(15)                       
  , @pHolderName4 nvarchar(50)                       
  , @pHolderPhoneType01  int
  , @pHolderPhoneType02  int
  , @pHolderState    nvarchar(50)                    
  , @pHolderCountry  nvarchar(50) 
  , @pUserType  int
  , @pPersonalInfoSeqId  int
  , @pPointsStatus int
  , @pDeposit money
  , @pCardPay bit
  , @pBlockDescription nvarchar(256) 
  , @pMSCreatedOnSiteId Int 
  , @pExternalReference nvarchar(50) 
  , @pHolderOccupation  nvarchar(50) 	
  , @pHolderExtNum      nvarchar(10) 
  , @pHolderNationality  Int 
  , @pHolderBirthCountry Int 
  , @pHolderFedEntity    Int 
  , @pHolderId1Type      Int -- RFC
  , @pHolderId2Type      Int -- CURP
  , @pHolderId3Type       nvarchar(50)   
  , @pHolderId3           nvarchar(20) 
  , @pHolderHasBeneficiary bit  
  , @pBeneficiaryName     nvarchar(200) 
  , @pBeneficiaryName1    nvarchar(50) 
  , @pBeneficiaryName2    nvarchar(50) 
  , @pBeneficiaryName3    nvarchar(50) 
  , @pBeneficiaryBirthDate Datetime 
  , @pBeneficiaryGender    int 
  , @pBeneficiaryOccupation nvarchar(50) 
  , @pBeneficiaryId1Type    int   -- RFC
  , @pBeneficiaryId1        nvarchar(20) 
  , @pBeneficiaryId2Type    int   -- CURP
  , @pBeneficiaryId2        nvarchar(20) 
  , @pBeneficiaryId3Type    nvarchar(50)  
  , @pBeneficiaryId3  	    nvarchar(20)   
  , @pOutputStatus INT               OUTPUT
  , @pOutputAccountOtherId BIGINT    OUTPUT
  AS
  BEGIN

  DECLARE @pOtherAccountId as BIGINT
  DECLARE @IsNewAccount    as BIT
  DECLARE @PreviousUserType as INT
  DECLARE @PreviousPersonalInfoSeqId as BIGINT
  DECLARE @NumAccounts as INT

  SET @pOutputStatus = 0
  SET @pOutputAccountOtherId = 0
  
  SET @pUserType = ISNULL(@pUserType,CASE WHEN ISNULL(@pHolderLevel,0) > 0  THEN 1 ELSE 0 END)
  
  SET @NumAccounts = (SELECT COUNT(AC_ACCOUNT_ID) FROM ACCOUNTS WHERE AC_ACCOUNT_ID <> @pAccountId AND AC_HOLDER_ID = @pHolderId)
  IF @NumAccounts > 0 
  BEGIN
    SET @pOutputStatus = 1
  END

  SET @pOtherAccountId = ISNULL ((SELECT AC_ACCOUNT_ID FROM ACCOUNTS WHERE AC_TRACK_DATA = @pTrackData), 0)
  IF @pOtherAccountId <> 0 AND @pOtherAccountId <> @pAccountId
  BEGIN  
    UPDATE   ACCOUNTS 
       SET   AC_TRACK_DATA = '00000000000000000000-RECYCLED-' + CAST (@pOtherAccountId AS NVARCHAR) 
     WHERE   AC_ACCOUNT_ID = @pOtherAccountId

    SET @pOutputStatus = 2
    SET @pOutputAccountOtherId = @pOtherAccountId
  END

  SELECT @PreviousPersonalInfoSeqId = AC_MS_PERSONAL_INFO_SEQ_ID FROM ACCOUNTS WHERE AC_ACCOUNT_ID = @pAccountId 

  IF (@PreviousPersonalInfoSeqId IS NOT NULL AND @PreviousPersonalInfoSeqId > @pPersonalInfoSeqId)
  BEGIN
    SET @pOutputStatus = 3
    SELECT @pOutputAccountOtherId = ISNULL(AC_MS_LAST_SITE_ID, 0) FROM ACCOUNTS WHERE AC_ACCOUNT_ID = @pAccountId 
  END

  SET @IsNewAccount = 0
  IF NOT EXISTS (SELECT 1 FROM ACCOUNTS WHERE ac_account_id = @pAccountId )  
  BEGIN
    SET @IsNewAccount = 1

    INSERT INTO   ACCOUNTS 
                ( AC_ACCOUNT_ID, AC_TYPE, AC_BLOCKED, AC_MS_LAST_SITE_ID, AC_CREATED) 
         VALUES ( @pAccountId,         2,          1, @pCallingSiteId   , @pAccountCreated)
  END 
  ELSE
  BEGIN  
    SELECT @PreviousUserType = AC_USER_TYPE FROM ACCOUNTS WHERE AC_ACCOUNT_ID = @pAccountId     
    IF (@PreviousUserType = 1 AND @pUserType = 0) 
    BEGIN
      -- TODO: Make Alarm.
      -- SET @pOutputStatus = 4
      
      SET @pUserType = 1
    END
  END
   
  UPDATE   ACCOUNTS                   
     SET   AC_TRACK_DATA              = @pTrackData 
         , AC_HOLDER_NAME             = @pHolderName 
         , AC_HOLDER_ID               = @pHolderId 
         , AC_HOLDER_ID_TYPE          = @pHolderIdType 
         , AC_HOLDER_ADDRESS_01       = @pHolderAddress01 
         , AC_HOLDER_ADDRESS_02       = @pHolderAddress02 
         , AC_HOLDER_ADDRESS_03       = @pHolderAddress03 
         , AC_HOLDER_CITY             = @pHolderCity 
         , AC_HOLDER_ZIP              = @pHolderZip  
         , AC_HOLDER_EMAIL_01         = @pHolderEmail01 
         , AC_HOLDER_EMAIL_02         = @pHolderEmail02 
         , AC_HOLDER_TWITTER_ACCOUNT  = @pHolderTwitter 
         , AC_HOLDER_PHONE_NUMBER_01  = @pHolderPhoneNumber01 
         , AC_HOLDER_PHONE_NUMBER_02  = @pHolderPhoneNumber02 
         , AC_HOLDER_COMMENTS         = @pHolderComments 
         , AC_HOLDER_ID1              = @pHolderId1 
         , AC_HOLDER_ID2              = @pHolderId2 
         , AC_HOLDER_DOCUMENT_ID1     = @pHolderDocumentId1 
         , AC_HOLDER_DOCUMENT_ID2     = @pHolderDocumentId2 
         , AC_HOLDER_NAME1            = @pHolderName1 
         , AC_HOLDER_NAME2            = @pHolderName2 
         , AC_HOLDER_NAME3            = @pHolderName3 
         , AC_HOLDER_GENDER           = @pHolderGender  
         , AC_HOLDER_MARITAL_STATUS   = @pHolderMaritalStatus 
         , AC_HOLDER_BIRTH_DATE       = @pHolderBirthDate 
         , AC_HOLDER_WEDDING_DATE     = @pHolderWeddingDate 
         , AC_HOLDER_LEVEL            = CASE WHEN (@IsNewAccount = 0) AND (AC_USER_TYPE = @pUserType)  THEN AC_HOLDER_LEVEL            ELSE @pHolderLevel           END
         , AC_HOLDER_LEVEL_NOTIFY     = CASE WHEN (@IsNewAccount = 0) AND (AC_USER_TYPE = @pUserType)  THEN AC_HOLDER_LEVEL_NOTIFY     ELSE @pHolderLevelNotify     END 
         , AC_HOLDER_LEVEL_ENTERED    = CASE WHEN (@IsNewAccount = 0) AND (AC_USER_TYPE = @pUserType)  THEN AC_HOLDER_LEVEL_ENTERED    ELSE @pHolderLevelEntered    END  
         , AC_HOLDER_LEVEL_EXPIRATION = CASE WHEN (@IsNewAccount = 0) AND (AC_USER_TYPE = @pUserType)  THEN AC_HOLDER_LEVEL_EXPIRATION ELSE @pHolderLevelExpiration END   
         , AC_PIN                     = @pPin 
         , AC_PIN_FAILURES            = @pPinFailures 
         , AC_PIN_LAST_MODIFIED       = @pPinLastModified 
         , AC_BLOCKED                 = @pBlocked 
         , AC_ACTIVATED               = @pActivated 
         , AC_BLOCK_REASON            = @pBlockReason 
         , AC_HOLDER_IS_VIP           = @pHolderIsVip 
         , AC_HOLDER_TITLE            = @pHolderTitle 
         , AC_HOLDER_NAME4            = @pHolderName4         
         , AC_HOLDER_PHONE_TYPE_01    = @pHolderPhoneType01  
         , AC_HOLDER_PHONE_TYPE_02    = @pHolderPhoneType02  
         , AC_HOLDER_STATE            = @pHolderState    
         , AC_HOLDER_COUNTRY          = @pHolderCountry   
         , AC_USER_TYPE               = @pUserType
         , AC_POINTS_STATUS           = CASE WHEN (@pUserType = 0) THEN NULL ELSE CASE WHEN (@IsNewAccount = 0) THEN @pPointsStatus ELSE 0 END END           
         , AC_DEPOSIT                 = @pDeposit 
         , AC_CARD_PAID               = @pCardPay 
         , AC_BLOCK_DESCRIPTION       = @pBlockDescription          
         , AC_MS_CREATED_ON_SITE_ID   = ISNULL(AC_MS_CREATED_ON_SITE_ID, ISNULL(@pMSCreatedOnSiteId, @pCallingSiteId))
         , AC_EXTERNAL_REFERENCE      = ISNULL(@pExternalReference,AC_EXTERNAL_REFERENCE)
         , AC_HOLDER_OCCUPATION       = @pHolderOccupation
         , AC_HOLDER_EXT_NUM          = @pHolderExtNum
         , AC_HOLDER_NATIONALITY      = @pHolderNationality 
         , AC_HOLDER_BIRTH_COUNTRY    = @pHolderBirthCountry 
         , AC_HOLDER_FED_ENTITY       = @pHolderFedEntity
         , AC_HOLDER_ID1_TYPE         = @pHolderId1Type 
         , AC_HOLDER_ID2_TYPE         = @pHolderId2Type 
         , AC_HOLDER_ID3_TYPE         = @pHolderId3Type 
         , AC_HOLDER_ID3              = @pHolderId3
         , AC_HOLDER_HAS_BENEFICIARY  = @pHolderHasBeneficiary 
         , AC_BENEFICIARY_NAME        = @pBeneficiaryName 
         , AC_BENEFICIARY_NAME1       = @pBeneficiaryName1
         , AC_BENEFICIARY_NAME2       = @pBeneficiaryName2
         , AC_BENEFICIARY_NAME3       = @pBeneficiaryName3
         , AC_BENEFICIARY_BIRTH_DATE  = @pBeneficiaryBirthDate 
         , AC_BENEFICIARY_GENDER      = @pBeneficiaryGender 
         , AC_BENEFICIARY_OCCUPATION  = @pBeneficiaryOccupation
         , AC_BENEFICIARY_ID1_TYPE    = @pBeneficiaryId1Type 
         , AC_BENEFICIARY_ID1         = @pBeneficiaryId1 
         , AC_BENEFICIARY_ID2_TYPE    = @pBeneficiaryId2Type 
         , AC_BENEFICIARY_ID2         = @pBeneficiaryId2 
         , AC_BENEFICIARY_ID3_TYPE    = @pBeneficiaryId3Type 
         , AC_BENEFICIARY_ID3         = @pBeneficiaryId3 
   WHERE   AC_ACCOUNT_ID              = @pAccountId 
       
END
GO

/****** TRIGGERS ******/
--------------------------------------------------------------------------------
-- Copyright � 2013 Win Systems International 
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: MultiSiteTrigger_AccountUpdate.sql
-- 
--   DESCRIPTION: Procedures for trigger MultiSiteTrigger_AccountUpdate and related issues
-- 
--        AUTHOR: Jos� Mart�nez
-- 
-- CREATION DATE: 07-MAR-2013
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 07-MAR-2013 JML    First release.
--------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[MultiSiteTrigger_AccountUpdate]') AND type in (N'TR'))
DROP TRIGGER [dbo].[MultiSiteTrigger_AccountUpdate]
GO


CREATE TRIGGER [dbo].[MultiSiteTrigger_AccountUpdate] ON [dbo].[accounts]
   AFTER UPDATE
NOT FOR REPLICATION
AS 
BEGIN
    DECLARE @Sequence11Value    AS BIGINT
    DECLARE @Sequence10Value    AS BIGINT
    DECLARE @AccountId          AS BIGINT
    DECLARE @hash0              as varbinary(20)
    DECLARE @hash1              as varbinary(20)
    DECLARE @value              as nvarchar(max)
    DECLARE @changed            as bit
    DECLARE @updated            as bit
    
    SET @updated = 0;        
            
    IF (UPDATE(AC_MS_CHANGE_GUID)) RETURN
 
    IF (UPDATE(AC_POINTS))
      BEGIN
          DECLARE PointsCursor CURSOR FOR 
           SELECT   INSERTED.AC_ACCOUNT_ID 
             FROM   INSERTED, DELETED 
            WHERE   INSERTED.AC_ACCOUNT_ID    =  DELETED.AC_ACCOUNT_ID
              AND ( INSERTED.AC_POINTS        <> DELETED.AC_POINTS )
               
        SET NOCOUNT ON;

        OPEN PointsCursor

        FETCH NEXT FROM PointsCursor INTO @AccountId
          
        WHILE @@FETCH_STATUS = 0
        BEGIN
            UPDATE   SEQUENCES 
               SET   SEQ_NEXT_VALUE = SEQ_NEXT_VALUE + 1
             WHERE   SEQ_ID         = 11

            SELECT   @Sequence11Value = SEQ_NEXT_VALUE - 1 FROM SEQUENCES WHERE SEQ_ID = 11

            UPDATE   ACCOUNTS
               SET   AC_MS_POINTS_SEQ_ID = @Sequence11Value 
             WHERE   AC_ACCOUNT_ID       = @AccountId

            FETCH NEXT FROM PointsCursor INTO @AccountId
        END

        CLOSE PointsCursor
        DEALLOCATE PointsCursor
    END


    IF UPDATE (AC_TRACK_DATA)
    OR UPDATE (AC_HOLDER_NAME)
    OR UPDATE (AC_HOLDER_ID)
    OR UPDATE (AC_HOLDER_ID_TYPE)
    OR UPDATE (AC_HOLDER_ADDRESS_01)
    OR UPDATE (AC_HOLDER_ADDRESS_02)
    OR UPDATE (AC_HOLDER_ADDRESS_03)
    OR UPDATE (AC_HOLDER_CITY)
    OR UPDATE (AC_HOLDER_ZIP)
    OR UPDATE (AC_HOLDER_EMAIL_01)
    OR UPDATE (AC_HOLDER_EMAIL_02)
    OR UPDATE (AC_HOLDER_TWITTER_ACCOUNT)
    OR UPDATE (AC_HOLDER_PHONE_NUMBER_01)
    OR UPDATE (AC_HOLDER_PHONE_NUMBER_02)
    OR UPDATE (AC_HOLDER_COMMENTS)
    OR UPDATE (AC_HOLDER_ID1)
    OR UPDATE (AC_HOLDER_ID2)
    OR UPDATE (AC_HOLDER_DOCUMENT_ID1)
    OR UPDATE (AC_HOLDER_DOCUMENT_ID2)
    OR UPDATE (AC_HOLDER_NAME1)
    OR UPDATE (AC_HOLDER_NAME2)
    OR UPDATE (AC_HOLDER_NAME3)
    OR UPDATE (AC_HOLDER_GENDER)
    OR UPDATE (AC_HOLDER_MARITAL_STATUS)
    OR UPDATE (AC_HOLDER_BIRTH_DATE)
    OR UPDATE (AC_HOLDER_WEDDING_DATE)
    OR UPDATE (AC_HOLDER_LEVEL)
    OR UPDATE (AC_HOLDER_LEVEL_NOTIFY)
    OR UPDATE (AC_HOLDER_LEVEL_ENTERED)
    OR UPDATE (AC_HOLDER_LEVEL_EXPIRATION)
    OR UPDATE (AC_PIN)
    OR UPDATE (AC_PIN_FAILURES)
    OR UPDATE (AC_PIN_LAST_MODIFIED)
    OR UPDATE (AC_BLOCKED)
    OR UPDATE (AC_ACTIVATED)
    OR UPDATE (AC_BLOCK_REASON)
    OR UPDATE (AC_HOLDER_IS_VIP)
    OR UPDATE (AC_HOLDER_TITLE)
    OR UPDATE (AC_HOLDER_NAME4)
    OR UPDATE (AC_HOLDER_PHONE_TYPE_01)
    OR UPDATE (AC_HOLDER_PHONE_TYPE_02)
    OR UPDATE (AC_HOLDER_STATE)
    OR UPDATE (AC_HOLDER_COUNTRY)
    OR UPDATE (AC_USER_TYPE)
    OR UPDATE (AC_POINTS_STATUS)
    OR UPDATE (AC_DEPOSIT)
    OR UPDATE (AC_CARD_PAID)
    OR UPDATE (AC_BLOCK_DESCRIPTION)    
    OR UPDATE (AC_MS_CREATED_ON_SITE_ID)
    OR UPDATE (AC_EXTERNAL_REFERENCE)
    OR UPDATE (AC_HOLDER_OCCUPATION)     
    OR UPDATE (AC_HOLDER_EXT_NUM)       
    OR UPDATE (AC_HOLDER_NATIONALITY)
    OR UPDATE (AC_HOLDER_BIRTH_COUNTRY)
    OR UPDATE (AC_HOLDER_FED_ENTITY)
    OR UPDATE (AC_HOLDER_ID1_TYPE)
    OR UPDATE (AC_HOLDER_ID2_TYPE)
    OR UPDATE (AC_HOLDER_ID3_TYPE)
    OR UPDATE (AC_HOLDER_ID3)
    OR UPDATE (AC_HOLDER_HAS_BENEFICIARY)
    OR UPDATE (AC_BENEFICIARY_NAME)
    OR UPDATE (AC_BENEFICIARY_NAME1)
    OR UPDATE (AC_BENEFICIARY_NAME2)
    OR UPDATE (AC_BENEFICIARY_NAME3)
    OR UPDATE (AC_BENEFICIARY_BIRTH_DATE)
    OR UPDATE (AC_BENEFICIARY_GENDER)
    OR UPDATE (AC_BENEFICIARY_OCCUPATION)
    OR UPDATE (AC_BENEFICIARY_ID1_TYPE)
    OR UPDATE (AC_BENEFICIARY_ID1)
    OR UPDATE (AC_BENEFICIARY_ID2_TYPE)
    OR UPDATE (AC_BENEFICIARY_ID2)
    OR UPDATE (AC_BENEFICIARY_ID3_TYPE)
	  OR UPDATE (AC_BENEFICIARY_ID3)
        SET @updated = 1;
        
    IF (@updated = 0) RETURN

    DECLARE PersonalInfoCursor CURSOR FOR 
     SELECT   AC_ACCOUNT_ID
            , HASHBYTES ('SHA1',  ISNULL(AC_TRACK_DATA,        '')
                                + ISNULL(AC_HOLDER_NAME,       '')
                                + ISNULL(AC_HOLDER_ID,         '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_ID_TYPE), '')
                                + ISNULL(AC_HOLDER_ADDRESS_01, '')
                                + ISNULL(AC_HOLDER_ADDRESS_02, '')
                                + ISNULL(AC_HOLDER_ADDRESS_03, '')
                                + ISNULL(AC_HOLDER_CITY,       '')
                                + ISNULL(AC_HOLDER_ZIP,        '')
                                + ISNULL(AC_HOLDER_EMAIL_01,   '')
                                + ISNULL(AC_HOLDER_EMAIL_02,   '')
                                + ISNULL(AC_HOLDER_TWITTER_ACCOUNT, '')
                                + ISNULL(AC_HOLDER_PHONE_NUMBER_01, '')
                                + ISNULL(AC_HOLDER_PHONE_NUMBER_02, '')
                                + ISNULL(AC_HOLDER_COMMENTS,        '')
                                + ISNULL(AC_HOLDER_ID1, '')
                                + ISNULL(AC_HOLDER_ID2, '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_DOCUMENT_ID1), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_DOCUMENT_ID2), '')
                                + ISNULL(AC_HOLDER_NAME1, '')
                                + ISNULL(AC_HOLDER_NAME2, '')
                                + ISNULL(AC_HOLDER_NAME3, '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_GENDER),         '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_MARITAL_STATUS), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_BIRTH_DATE,   21), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_WEDDING_DATE, 21), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_LEVEL),                '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_LEVEL_NOTIFY),         '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_LEVEL_ENTERED,    21), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_LEVEL_EXPIRATION, 21), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_PIN),          '')
                                + ISNULL(CONVERT(NVARCHAR, AC_PIN_FAILURES), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_PIN_LAST_MODIFIED, 21), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_BLOCKED),           '')
                                + ISNULL(CONVERT(NVARCHAR, AC_ACTIVATED),         '')
                                + ISNULL(CONVERT(NVARCHAR, AC_BLOCK_REASON),         '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_IS_VIP),        '')
                                + ISNULL(AC_HOLDER_TITLE                        , '')
                                + ISNULL(AC_HOLDER_NAME4                        , '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_PHONE_TYPE_01),        '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_PHONE_TYPE_02),        '')
                                + ISNULL(AC_HOLDER_STATE                        , '')
                                + ISNULL(AC_HOLDER_COUNTRY                      , '')
                                + ISNULL(CONVERT(NVARCHAR,AC_USER_TYPE)         , '') 
                                + ISNULL(CONVERT(NVARCHAR,AC_POINTS_STATUS)     , '') 
                                + ISNULL(CONVERT(NVARCHAR,AC_DEPOSIT)           , '')
                                + ISNULL(CONVERT(NVARCHAR,AC_CARD_PAID)         , '')
                                + ISNULL(AC_BLOCK_DESCRIPTION, '')                                                                  
                                + ISNULL(CONVERT(NVARCHAR,AC_MS_CREATED_ON_SITE_ID), '')
                                + ISNULL(AC_EXTERNAL_REFERENCE, '')
                                + ISNULL(AC_HOLDER_OCCUPATION, '')
                                + ISNULL(AC_HOLDER_EXT_NUM, '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_NATIONALITY),         '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_BIRTH_COUNTRY), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_ID1_TYPE), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_ID2_TYPE), '')
                                + ISNULL(AC_HOLDER_ID3_TYPE, '')
                                + ISNULL(AC_HOLDER_ID3, '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_HAS_BENEFICIARY), '')
                                + ISNULL(AC_BENEFICIARY_NAME, '')
                                + ISNULL(AC_BENEFICIARY_NAME1, '')
                                + ISNULL(AC_BENEFICIARY_NAME2, '')
                                + ISNULL(AC_BENEFICIARY_NAME3, '')
                                + ISNULL(CONVERT(NVARCHAR, AC_BENEFICIARY_BIRTH_DATE,   21), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_BENEFICIARY_GENDER), '')	
                                + ISNULL(AC_BENEFICIARY_OCCUPATION, '')
                                + ISNULL(CONVERT(NVARCHAR, AC_BENEFICIARY_ID1_TYPE), '')				
                                + ISNULL(AC_BENEFICIARY_ID1, '')
                                + ISNULL(CONVERT(NVARCHAR, AC_BENEFICIARY_ID2_TYPE), '')				
                                + ISNULL(AC_BENEFICIARY_ID2, '')
                                + ISNULL(AC_BENEFICIARY_ID3_TYPE, '')				
                                + ISNULL(AC_BENEFICIARY_ID3, '') )
       FROM   INSERTED

    SET NOCOUNT ON;

    OPEN PersonalInfoCursor

    FETCH NEXT FROM PersonalInfoCursor INTO @AccountId, @hash1

    WHILE @@FETCH_STATUS = 0
    BEGIN

        SELECT @hash0 = AC_MS_HASH FROM DELETED WHERE AC_ACCOUNT_ID = @AccountId

        SELECT @changed = CASE WHEN ( @hash0 = @hash1 ) THEN 0 ELSE 1 END

        IF @changed = 1
        BEGIN
            -- Personal Info
            UPDATE   SEQUENCES 
               SET   SEQ_NEXT_VALUE = SEQ_NEXT_VALUE + 1
             WHERE   SEQ_ID         = 10

            SELECT   @Sequence10Value = SEQ_NEXT_VALUE - 1 FROM SEQUENCES WHERE SEQ_ID = 10

            UPDATE   ACCOUNTS
               SET   AC_MS_PERSONAL_INFO_SEQ_ID = @Sequence10Value
                   , AC_MS_HASH                 = @hash1
                   , AC_MS_CHANGE_GUID          = NEWID()
             WHERE   AC_ACCOUNT_ID              = @AccountId
        END

        FETCH NEXT FROM PersonalInfoCursor INTO @AccountId, @hash1
    END

    CLOSE PersonalInfoCursor
    DEALLOCATE PersonalInfoCursor

END
GO

/****** RECORDS ******/
UPDATE MS_SITE_TASKS SET ST_MAX_ROWS_TO_UPLOAD = 1 WHERE ST_TASK_ID = 60 -- DownloadMasterProfiles
GO
--
-- NEW TASK: DownloadCommonUsers
--
SELECT ST_SITE_ID AS SITE_ID INTO #CURRENT_SITES FROM MS_SITE_TASKS GROUP BY ST_SITE_ID
INSERT INTO MS_SITE_TASKS (ST_TASK_ID, ST_ENABLED, ST_INTERVAL_SECONDS, ST_MAX_ROWS_TO_UPLOAD, ST_SITE_ID) SELECT 61, 1, 600,200, SITE_ID FROM #CURRENT_SITES; -- DownloadCommonUsers
DROP TABLE #CURRENT_SITES
GO

--
-- GENERAL PARAMS: AntiMoneyLaundering
--
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Account.RequestedField', 'AntiMoneyLaundering.BirthCountry', '0');
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Account.RequestedField', 'AntiMoneyLaundering.BirthDate', '1');
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Account.RequestedField', 'AntiMoneyLaundering.Document', '1');
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Account.RequestedField', 'AntiMoneyLaundering.DocumentTypeList', '');
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Account.RequestedField', 'AntiMoneyLaundering.DocScan', '0');
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Account.RequestedField', 'AntiMoneyLaundering.DocScanTypeList', '');
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Account.RequestedField', 'AntiMoneyLaundering.Email1', '0');
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Account.RequestedField', 'AntiMoneyLaundering.Gender', '1');
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Account.RequestedField', 'AntiMoneyLaundering.Name1', '1');
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Account.RequestedField', 'AntiMoneyLaundering.Name2', '0');
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Account.RequestedField', 'AntiMoneyLaundering.Name3', '1');
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Account.RequestedField', 'AntiMoneyLaundering.Nationality', '0');
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Account.RequestedField', 'AntiMoneyLaundering.Occupation', '0');
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Account.RequestedField', 'AntiMoneyLaundering.Phone1', '0');

INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Beneficiary.RequestedField', 'AntiMoneyLaundering.Document', '1');
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Beneficiary.RequestedField', 'AntiMoneyLaundering.DocumentTypeList', '');
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Beneficiary.RequestedField', 'AntiMoneyLaundering.Name1', '1');
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Beneficiary.RequestedField', 'AntiMoneyLaundering.Name2', '0');
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Beneficiary.RequestedField', 'AntiMoneyLaundering.Name3', '1');
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Beneficiary.RequestedField', 'AntiMoneyLaundering.BirthDate', '1');
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Beneficiary.RequestedField', 'AntiMoneyLaundering.Gender', '1');
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Beneficiary.RequestedField', 'AntiMoneyLaundering.Occupation', '0');
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Beneficiary.RequestedField', 'AntiMoneyLaundering.DocScan', '0');
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Beneficiary.RequestedField', 'AntiMoneyLaundering.DocScanTypeList', '');

DECLARE @AntiMoneyLaunderingEnabled AS INT
SELECT @AntiMoneyLaunderingEnabled = GP_KEY_VALUE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'MoneyLaundering' AND GP_SUBJECT_KEY = 'Enabled'
SET @AntiMoneyLaunderingEnabled = ISNULL(@AntiMoneyLaunderingEnabled, 0)

INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('AntiMoneyLaundering', 'Enabled', @AntiMoneyLaunderingEnabled);
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('AntiMoneyLaundering', 'BaseAmount', '64.76');
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('AntiMoneyLaundering', 'BaseName', 'SMVGDF');

INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('AntiMoneyLaundering', 'Recharge.Identification.Limit', '325');
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('AntiMoneyLaundering', 'Recharge.Identification.Message', 'El cliente ha superado el l�mite 325 SMVGFD. Por la ley antilavado de dinero, debe identificarse.');
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('AntiMoneyLaundering', 'Recharge.Identification.Warning.Limit', '310');
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('AntiMoneyLaundering', 'Recharge.Identification.Warning.Message', 'Por la ley antilavado de dinero, el cliente deber� identificarse en pr�ximas recargas.');
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('AntiMoneyLaundering', 'Recharge.Identification.DontAllowMB', '0'); 

INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('AntiMoneyLaundering', 'Recharge.Report.Limit', '645');
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('AntiMoneyLaundering', 'Recharge.Report.Message', 'Por la ley antilavado de dinero, el cliente va a ser inclu�do en un reporte para el SAT.');
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('AntiMoneyLaundering', 'Recharge.Report.Warning.Limit', '625');
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('AntiMoneyLaundering', 'Recharge.Report.Warning.Message', 'Por la ley antilavado de dinero, el cliente ser� reportado al SAT en pr�ximas recargas.');
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('AntiMoneyLaundering', 'Recharge.Report.DontAllowMB', '0'); 

INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('AntiMoneyLaundering', 'Prize.Identification.Limit', '325');
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('AntiMoneyLaundering', 'Prize.Identification.Message', 'El cliente ha superado el l�mite 325 SMVGFD. Por la ley antilavado de dinero, debe identificarse.');
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('AntiMoneyLaundering', 'Prize.Identification.Warning.Limit', '310');
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('AntiMoneyLaundering', 'Prize.Identification.Warning.Message', 'Por la ley antilavado de dinero, el cliente deber� identificarse en pr�ximos retiros.');

INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('AntiMoneyLaundering', 'Prize.Report.Limit', '645');
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('AntiMoneyLaundering', 'Prize.Report.Message', 'Por la ley antilavado de dinero, el cliente va a ser inclu�do en un reporte para el SAT.');
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('AntiMoneyLaundering', 'Prize.Report.Warning.Limit', '625');
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('AntiMoneyLaundering', 'Prize.Report.Warning.Message', 'Por la ley antilavado de dinero, el cliente ser� reportado al SAT en pr�ximos retiros.');
---
--- DELETE OLD GP
---
DELETE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'MoneyLaundering'
GO
---
--- DOWNLOAD TO ALL SITES
---
UPDATE GENERAL_PARAMS SET GP_MS_DOWNLOAD_TYPE = 1 WHERE GP_GROUP_KEY LIKE 'AntiMoneyLaundering%' OR GP_SUBJECT_KEY LIKE 'AntiMoneyLaundering%' 
GO

-- Allow to download External Loyalty General Params
UPDATE GENERAL_PARAMS SET GP_MS_DOWNLOAD_TYPE = 1 WHERE GP_GROUP_KEY = 'ExternalLoyaltyProgram.Mode01'

-- New General Param
INSERT INTO general_params(gp_group_key,gp_subject_key,gp_key_value,gp_ms_download_type) VALUES('ExternalLoyaltyProgram.Mode01','Name','Space',1)

--
--
UPDATE ACCOUNTS SET ac_holder_id3_type      = CASE WHEN cast(ac_holder_id3_type as INT)      < 0 THEN NULL ELSE REPLACE(STR(ac_holder_id3_type, 3), SPACE(1), '0')      END WHERE ac_holder_id3_type      IS NOT NULL
UPDATE ACCOUNTS SET ac_beneficiary_id3_type = CASE WHEN cast(ac_beneficiary_id3_type as INT) < 0 THEN NULL ELSE REPLACE(STR(ac_beneficiary_id3_type, 3), SPACE(1), '0') END WHERE ac_beneficiary_id3_type IS NOT NULL 
GO

---
--- SAVE ACCOUNT_DOCUMENTS 
---
SELECT * INTO account_documents_backup FROM ACCOUNT_DOCUMENTS 