/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_100]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 109;

SET @New_ReleaseId = 110;
SET @New_ScriptName = N'UpdateTo_18.110.sql';
SET @New_Description = N'New release v03.006.0029'; 

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/**** GENERAL PARAM *****/


/******* TABLES  *******/


/******* RECORDS *******/

/******* RECORDS *******/

IF NOT EXISTS (SELECT 1 FROM ALARM_GROUPS WHERE ALG_ALARM_GROUP_ID = 5)
BEGIN
  -- ALARM GROUPS
  INSERT INTO ALARM_GROUPS VALUES( 5, 9,  0, N'Redeem Kiosk',        '', 1)            
  INSERT INTO ALARM_GROUPS VALUES( 5, 10, 0, N'Kiosko de Redención', '', 1)
                 
  -- ALARM CATEGORIES                    
  INSERT INTO ALARM_CATEGORIES VALUES(51, 9,  5, 0, N'Door'  , '', 1)
  INSERT INTO ALARM_CATEGORIES VALUES(51, 10, 5, 0, N'Puerta', '', 1)

  INSERT INTO ALARM_CATEGORIES VALUES(52, 9, 5,  0, N'Operator card'      , '', 1)
  INSERT INTO ALARM_CATEGORIES VALUES(52, 10, 5, 0, N'Tarjeta de operador', '', 1)

  INSERT INTO ALARM_CATEGORIES VALUES(53, 9, 5,  0, N'Customer card'     ,  '', 1)
  INSERT INTO ALARM_CATEGORIES VALUES(53, 10, 5, 0, N'Tarjeta de cliente',  '', 1)

  INSERT INTO ALARM_CATEGORIES VALUES(54, 9, 5,  0, N'Banknote' ,  '', 1)
  INSERT INTO ALARM_CATEGORIES VALUES(54, 10, 5, 0, N'Billete'  ,  '', 1)

  INSERT INTO ALARM_CATEGORIES VALUES(55, 9, 5,  0, N'Ticket',  '', 1)
  INSERT INTO ALARM_CATEGORIES VALUES(55, 10, 5, 0, N'Ticket',  '', 1)

  INSERT INTO ALARM_CATEGORIES VALUES(56, 9, 5,  0, N'Call operator'   ,  '', 1)
  INSERT INTO ALARM_CATEGORIES VALUES(56, 10, 5, 0, N'Llamada operador',  '', 1)

  -- ALARM CATALOG
  INSERT INTO ALARM_CATALOG VALUES(5242881, 9, 0, N'Main door closed', N'Main door closed', 1)
  INSERT INTO ALARM_CATALOG VALUES(5242882, 9, 0, N'Main door opened', N'Main door opened', 1)
  INSERT INTO ALARM_CATALOG VALUES(5242883, 9, 0, N'Head door closed', N'Head door closed', 1)
  INSERT INTO ALARM_CATALOG VALUES(5242884, 9, 0, N'Head door opened', N'Head door opened', 1)
  INSERT INTO ALARM_CATALOG VALUES(5242885, 9, 0, N'Rejected', N'Rejected', 1)
  INSERT INTO ALARM_CATALOG VALUES(5242886, 9, 0, N'Inserted', N'Inserted', 1)
  INSERT INTO ALARM_CATALOG VALUES(5242887, 9, 0, N'Captured', N'Captured', 1)
  INSERT INTO ALARM_CATALOG VALUES(5242888, 9, 0, N'Issued', N'Issued', 1)
  INSERT INTO ALARM_CATALOG VALUES(5242889, 9, 0, N'Rejected', N'Rejected', 1)
  INSERT INTO ALARM_CATALOG VALUES(5242896, 9, 0, N'Inserted', N'Inserted', 1)
  INSERT INTO ALARM_CATALOG VALUES(5242897, 9, 0, N'Captured', N'Captured', 1)
  INSERT INTO ALARM_CATALOG VALUES(5242898, 9, 0, N'Issued', N'Issued', 1)
  INSERT INTO ALARM_CATALOG VALUES(5242899, 9, 0, N'Rejected', N'Rejected', 1)
  INSERT INTO ALARM_CATALOG VALUES(5242900, 9, 0, N'Inserted', N'Inserted', 1)
  INSERT INTO ALARM_CATALOG VALUES(5242901, 9, 0, N'Stacked', N'Stacked', 1)
  INSERT INTO ALARM_CATALOG VALUES(5242902, 9, 0, N'Rejected', N'Rejected', 1)
  INSERT INTO ALARM_CATALOG VALUES(5242903, 9, 0, N'Inserted', N'Inserted', 1)
  INSERT INTO ALARM_CATALOG VALUES(5242904, 9, 0, N'Stacked', N'Stacked', 1)
  INSERT INTO ALARM_CATALOG VALUES(5242905, 9, 0, N'Call operator', N'Call operator', 1)

  INSERT INTO ALARM_CATALOG VALUES(5242881, 10, 0, N'Puerta principal cerrada', N'Puerta principal cerrada', 1)
  INSERT INTO ALARM_CATALOG VALUES(5242882, 10, 0, N'Puerta principal abierta', N'Puerta principal abierta', 1)
  INSERT INTO ALARM_CATALOG VALUES(5242883, 10, 0, N'Puerta cabecera cerrada', N'Puerta cabecera cerrada', 1)
  INSERT INTO ALARM_CATALOG VALUES(5242884, 10, 0, N'Puerta cabecera abierta', N'Puerta cabecera abierta', 1)
  INSERT INTO ALARM_CATALOG VALUES(5242885, 10, 0, N'Rechazada', N'Rechazada', 1)
  INSERT INTO ALARM_CATALOG VALUES(5242886, 10, 0, N'Insertada', N'Insertada', 1)
  INSERT INTO ALARM_CATALOG VALUES(5242887, 10, 0, N'Capturada', N'Capturada', 1)
  INSERT INTO ALARM_CATALOG VALUES(5242888, 10, 0, N'Emitida', N'Emitida', 1)
  INSERT INTO ALARM_CATALOG VALUES(5242889, 10, 0, N'Rechazada', N'Rechazada', 1)
  INSERT INTO ALARM_CATALOG VALUES(5242896, 10, 0, N'Insertada', N'Insertada', 1)
  INSERT INTO ALARM_CATALOG VALUES(5242897, 10, 0, N'Capturada', N'Capturada', 1)
  INSERT INTO ALARM_CATALOG VALUES(5242898, 10, 0, N'Emitida', N'Emitida', 1)
  INSERT INTO ALARM_CATALOG VALUES(5242899, 10, 0, N'Rechazado', N'Rechazado', 1)
  INSERT INTO ALARM_CATALOG VALUES(5242900, 10, 0, N'Insertado', N'Insertado', 1)
  INSERT INTO ALARM_CATALOG VALUES(5242901, 10, 0, N'Apilado', N'Apilado', 1)
  INSERT INTO ALARM_CATALOG VALUES(5242902, 10, 0, N'Rechazado', N'Rechazado', 1)
  INSERT INTO ALARM_CATALOG VALUES(5242903, 10, 0, N'Insertado', N'Insertado', 1)
  INSERT INTO ALARM_CATALOG VALUES(5242904, 10, 0, N'Apilado', N'Apilado', 1)
  INSERT INTO ALARM_CATALOG VALUES(5242905, 10, 0, N'Llamada operador', N'Llamada operador', 1)

  -- ALARM CATALOG PER CATEGORY
  INSERT INTO ALARM_CATALOG_PER_CATEGORY VALUES(5242881,51, 0, GETDATE())
  INSERT INTO ALARM_CATALOG_PER_CATEGORY VALUES(5242882,51, 0, GETDATE())
  INSERT INTO ALARM_CATALOG_PER_CATEGORY VALUES(5242883,51, 0, GETDATE())
 INSERT INTO ALARM_CATALOG_PER_CATEGORY VALUES(5242884,51, 0, GETDATE())
  INSERT INTO ALARM_CATALOG_PER_CATEGORY VALUES(5242885,52, 0, GETDATE())
  INSERT INTO ALARM_CATALOG_PER_CATEGORY VALUES(5242886,52, 0, GETDATE())
  INSERT INTO ALARM_CATALOG_PER_CATEGORY VALUES(5242887,52, 0, GETDATE())
  INSERT INTO ALARM_CATALOG_PER_CATEGORY VALUES(5242888,52, 0, GETDATE())
  INSERT INTO ALARM_CATALOG_PER_CATEGORY VALUES(5242889,53, 0, GETDATE())
  INSERT INTO ALARM_CATALOG_PER_CATEGORY VALUES(5242896,53, 0, GETDATE())
  INSERT INTO ALARM_CATALOG_PER_CATEGORY VALUES(5242897,53, 0, GETDATE())
  INSERT INTO ALARM_CATALOG_PER_CATEGORY VALUES(5242898,53, 0, GETDATE())
  INSERT INTO ALARM_CATALOG_PER_CATEGORY VALUES(5242899,54, 0, GETDATE())
  INSERT INTO ALARM_CATALOG_PER_CATEGORY VALUES(5242900,54, 0, GETDATE())
  INSERT INTO ALARM_CATALOG_PER_CATEGORY VALUES(5242901,54, 0, GETDATE())
  INSERT INTO ALARM_CATALOG_PER_CATEGORY VALUES(5242902,55, 0, GETDATE())
  INSERT INTO ALARM_CATALOG_PER_CATEGORY VALUES(5242903,55, 0, GETDATE())
  INSERT INTO ALARM_CATALOG_PER_CATEGORY VALUES(5242904,55, 0, GETDATE())
  INSERT INTO ALARM_CATALOG_PER_CATEGORY VALUES(5242905,56, 0, GETDATE())
END
GO


/******* PROCEDURES *******/

IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'Update_Creditlines')
                DROP PROCEDURE Update_Creditlines
GO

CREATE PROCEDURE Update_Creditlines
           @pSiteId  INT  
          ,@pId BIGINT
          ,@pAccountId BIGINT
          ,@pLimitAmount MONEY
          ,@pTTOAmount MONEY
          ,@pSpentAmount MONEY
          ,@pISOCode nvarchar(3)
          ,@pStatus INT
          ,@pIBAN nvarchar(34)
          ,@pXMLSigners XML
          ,@pCreation DATETIME
          ,@pUpdate DATETIME
          ,@pExpired DATETIME
          ,@pLastPayback DATETIME
AS
BEGIN   
IF EXISTS (SELECT   1 
             FROM   CREDIT_LINES
            WHERE   CL_ID =  @pId 
              AND   CL_SITE_ID =  @pSiteId)
              
UPDATE CREDIT_LINES
   SET CL_LIMIT_AMOUNT   = @pLimitAmount
      ,CL_TTO_AMOUNT     = @pTTOAmount
      ,CL_SPENT_AMOUNT   = @pSpentAmount
      ,CL_ISO_CODE       = @pISOCode
      ,CL_STATUS         = @pStatus
      ,CL_IBAN           = @pIBAN
      ,CL_XML_SIGNERS    = @pXMLSigners
      ,CL_CREATION       = @pCreation
      ,CL_UPDATE         = @pUpdate
      ,CL_EXPIRED        = @pExpired
      ,CL_LAST_PAYBACK   = @pLastPayback  
            WHERE   CL_ID =  @pId  
              AND   CL_SITE_ID    =  @pSiteId
ELSE
INSERT INTO CREDIT_LINES
           (CL_SITE_ID
           ,CL_ID
           ,CL_ACCOUNT_ID
           ,CL_LIMIT_AMOUNT
           ,CL_TTO_AMOUNT
           ,CL_SPENT_AMOUNT
           ,CL_ISO_CODE
           ,CL_STATUS
           ,CL_IBAN
           ,CL_XML_SIGNERS
           ,CL_CREATION
           ,CL_UPDATE  
           ,CL_EXPIRED 
           ,CL_LAST_PAYBACK)
     VALUES 
           (@pSiteId 
           ,@pId
           ,@pAccountID
           ,@pLimitAmount
           ,@pTTOAmount
           ,@pSpentAmount
           ,@pISOCode
           ,@pStatus
           ,@pIBAN
           ,@pXMLSigners
           ,@pCreation
           ,@pUpdate
           ,@pExpired
           ,@pLastPayback)  
END -- Update_Creditlines
GO

/******* TRIGGERS *******/
