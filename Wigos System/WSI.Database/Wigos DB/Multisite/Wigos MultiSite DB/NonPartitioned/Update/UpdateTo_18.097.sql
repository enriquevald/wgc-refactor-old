/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_100]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 96;

SET @New_ReleaseId = 97;
SET @New_ScriptName = N'UpdateTo_18.097.sql';
SET @New_Description = N'RECEPCION';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/

-- CREATE CUSTOMER_RECORDS_HISTORY
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[customer_records_history]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[customer_records_history](
      [curh_record_history_id] [bigint] IDENTITY(1,1) NOT NULL,
      [curh_record_id] [bigint] NOT NULL,
      [curh_customer_id] [bigint] NOT NULL,
      [curh_deleted] [bit] NOT NULL,
      [curh_created] [datetime] NOT NULL,
      [curh_expiration] [datetime] NULL,
      [curh_logdate] [datetime] NOT NULL,
CONSTRAINT [PK_customer_records_history] PRIMARY KEY CLUSTERED 
(
      [curh_record_history_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[customer_records_history]  WITH CHECK ADD  CONSTRAINT [FK_customer_records_history_customers] FOREIGN KEY([curh_customer_id])
REFERENCES [dbo].[customers] ([cus_customer_id])

ALTER TABLE [dbo].[customer_records_history] CHECK CONSTRAINT [FK_customer_records_history_customers]
END
GO


/****** INDEXES ******/

/****** RECORDS ******/

/****** PROCEDURES ******/