/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_100]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 90;

SET @New_ReleaseId = 91;
SET @New_ScriptName = N'UpdateTo_18.091.sql';
SET @New_Description = N'Update MultiSiteTrigger_CenterAccountDocument';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/

/****** INDEXES ******/

/****** RECORDS ******/
 
/****** STORED *******/

--------------------------------------------------------------------------------
-- Copyright � 2013 Win Systems International 
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: MultiSiteTrigger_CenterAccountDocument
-- 
--   DESCRIPTION: Trigger MultiSiteTrigger_CenterAccountDocument for increasing the sequence
-- 
--        AUTHOR: Jos� Mart�nez L�pez
-- 
-- CREATION DATE: 02-JUL-2013
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 02-JUL-2013 JML    First release.
-- 22-MAR-2016 JML    Bug 10880:MS-Meier: Not upload account_documents
--------------------------------------------------------------------------------  

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[MultiSiteTrigger_CenterAccountDocument]') AND type in (N'TR'))
DROP TRIGGER [dbo].[MultiSiteTrigger_CenterAccountDocument]
GO

CREATE  TRIGGER [dbo].[MultiSiteTrigger_CenterAccountDocument] ON [dbo].[ACCOUNT_DOCUMENTS]
AFTER INSERT, UPDATE
NOT FOR REPLICATION
AS 
BEGIN
    DECLARE @Sequence15Value AS BIGINT
    DECLARE @AccountId       AS BIGINT

    DECLARE InsertedCursor CURSOR FOR 
     SELECT   INSERTED.AD_ACCOUNT_ID
       FROM   INSERTED 

    SET NOCOUNT ON;

    OPEN InsertedCursor

    FETCH NEXT FROM InsertedCursor INTO @AccountId
    
    WHILE @@FETCH_STATUS = 0
    BEGIN
    
        -- Account Documents
        UPDATE   SEQUENCES 
           SET   SEQ_NEXT_VALUE = SEQ_NEXT_VALUE + 1
         WHERE   SEQ_ID         = 15

        SELECT   @Sequence15Value = SEQ_NEXT_VALUE - 1 FROM SEQUENCES WHERE SEQ_ID = 15

        UPDATE   ACCOUNT_DOCUMENTS
           SET   AD_MS_SEQUENCE_ID = @Sequence15Value
         WHERE   AD_ACCOUNT_ID     = @AccountId

        FETCH NEXT FROM InsertedCursor INTO @AccountId
    END

    CLOSE InsertedCursor
    DEALLOCATE InsertedCursor

END   -- MultiSiteTrigger_CenterAccountDocument
GO