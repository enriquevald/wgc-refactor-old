﻿USE [wgdb_100]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 132;

SET @New_ReleaseId = 133;
SET @New_ScriptName = N'2018-06-29 - UpdateTo_18.133.sql';
SET @New_Description = N'New release v03.008.0024'; 

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO



/*********************************************************************************************************/
/**** GENERAL PARAM *****/


/**** VIEW *****/


/******* TABLES  *******/


/******* INDEXES *******/


/********************* CATALOGS *********************/


/****** Object:  Index [PK_catalogs]    Script Date: 22/06/2018 12:08:31 ******/
ALTER TABLE [dbo].[catalogs] DROP CONSTRAINT [PK_catalogs]
GO

/****** Object:  Index [PK_catalogs]    Script Date: 22/06/2018 12:08:31 ******/
ALTER TABLE [dbo].[catalogs] ADD  CONSTRAINT [PK_catalogs] PRIMARY KEY CLUSTERED 
(
      [cat_site_id] ASC,
      [cat_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


/********************* CATALOG ITEMS *********************/

/****** Object:  Index [PK_catalog_items]    Script Date: 22/06/2018 12:08:13 ******/
ALTER TABLE [dbo].[catalog_items] DROP CONSTRAINT [PK_catalog_items]
GO

/****** Object:  Index [PK_catalog_items]    Script Date: 22/06/2018 12:08:13 ******/
ALTER TABLE [dbo].[catalog_items] ADD  CONSTRAINT [PK_catalog_items] PRIMARY KEY CLUSTERED 
(
      [cai_site_id] ASC,
      [cai_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO



/******* TRIGERS *******/


/******* RECORDS *******/


/******* PROCEDURES *******/


/******* TRIGGERS *******/

