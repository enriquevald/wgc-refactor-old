/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_100]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 117;

SET @New_ReleaseId = 118;
SET @New_ScriptName = N'UpdateTo_18.118.sql';
SET @New_Description = N'New release v03.007.0018'; 

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/**** GENERAL PARAM *****/
IF NOT EXISTS(SELECT   1 
                FROM   GENERAL_PARAMS 
               WHERE   GP_GROUP_KEY = 'Monitor'
                 AND   GP_SUBJECT_KEY = 'Refresh.Interval'
             )
BEGIN
    INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
    VALUES ('Monitor', 'Refresh.Interval', '30')
END
GO


/******* TABLES  *******/
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[terminal_status]') AND type in (N'U'))
BEGIN

    CREATE TABLE [dbo].[terminal_status](
        [ts_site_id] [bigint] NOT NULL,
        [ts_terminal_id] [int] NOT NULL,
        [ts_sas_host_error] [bit] NOT NULL,
        [ts_current_payout] [money] NULL,
        [ts_stacker_counter] [int] NOT NULL,
        [ts_stacker_status] [int] NOT NULL,
        [ts_egm_flags] [int] NOT NULL,
        [ts_door_flags] [int] NOT NULL,
        [ts_bill_flags] [int] NOT NULL,
        [ts_printer_flags] [int] NOT NULL,
        [ts_played_won_flags] [int] NOT NULL,
        [ts_jackpot_flags] [int] NOT NULL,
        [ts_call_attendant_flags] [bit] NOT NULL,
        [ts_machine_flags] [int] NOT NULL,
        [ts_played_alarm_id] [bigint] NULL,
        [ts_won_alarm_id] [bigint] NULL,
        [ts_jackpot_alarm_id] [bigint] NULL,
        [ts_canceled_credit_alarm_id] [bigint] NULL,
        [ts_counterfeit_alarm_id] [bigint] NULL,
        [ts_without_plays_alarm_id] [bigint] NULL,
        [ts_current_payout_alarm_id] [bigint] NULL,
        [ts_coin_flags] [int] NOT NULL,
        [ts_highroller_alarm_id] [bigint] NULL,
        [ts_highroller_anonymous_alarm_id] [bigint] NULL,
        [ts_is_reserved] [bit] NULL,
        [ts_ps_busy] [bit] NULL,
    CONSTRAINT [PK_terminal_status] PRIMARY KEY CLUSTERED 
    (
        [ts_site_id] ASC,
        [ts_terminal_id] ASC
    )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
    ) ON [PRIMARY]
	
END

GO

DECLARE @RC INT

SELECT @RC = COUNT(*) FROM sys.default_constraints WHERE name like '%terminal___ts%'

IF (@RC < 1) 
BEGIN

    ALTER TABLE [dbo].[terminal_status] ADD  DEFAULT ((0)) FOR [ts_sas_host_error]
    ALTER TABLE [dbo].[terminal_status] ADD  DEFAULT ((0)) FOR [ts_stacker_counter]
    ALTER TABLE [dbo].[terminal_status] ADD  DEFAULT ((0)) FOR [ts_stacker_status]
    ALTER TABLE [dbo].[terminal_status] ADD  DEFAULT ((0)) FOR [ts_egm_flags]
    ALTER TABLE [dbo].[terminal_status] ADD  DEFAULT ((0)) FOR [ts_door_flags]
    ALTER TABLE [dbo].[terminal_status] ADD  DEFAULT ((0)) FOR [ts_bill_flags]
    ALTER TABLE [dbo].[terminal_status] ADD  DEFAULT ((0)) FOR [ts_printer_flags]
    ALTER TABLE [dbo].[terminal_status] ADD  DEFAULT ((0)) FOR [ts_played_won_flags]
    ALTER TABLE [dbo].[terminal_status] ADD  DEFAULT ((0)) FOR [ts_jackpot_flags]
    ALTER TABLE [dbo].[terminal_status] ADD  DEFAULT ((0)) FOR [ts_call_attendant_flags]
    ALTER TABLE [dbo].[terminal_status] ADD  DEFAULT ((0)) FOR [ts_machine_flags]
    ALTER TABLE [dbo].[terminal_status] ADD  DEFAULT ((0)) FOR [ts_coin_flags]

END

GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wc2_sessions]') AND type in (N'U'))
BEGIN

    CREATE TABLE [dbo].[wc2_sessions](
        [w2s_site_id] [int] NOT NULL,
        [w2s_session_id] [bigint] NOT NULL,
        [w2s_terminal_id] [int] NOT NULL,
        [w2s_last_sequence_id] [bigint] NOT NULL,
        [w2s_last_transaction_id] [bigint] NOT NULL,
        [w2s_last_rcvd_msg] [datetime] NULL,
        [w2s_started] [datetime] NOT NULL,
        [w2s_finished] [datetime] NULL,
        [w2s_status] [int] NOT NULL,
        [w2s_timestamp] [bigint] NULL,
        [w2s_server_name] [nvarchar](50) NULL,
    CONSTRAINT [PK_wc2_sessions] PRIMARY KEY CLUSTERED 
    (
        [w2s_site_id] ASC,
        [w2s_session_id] ASC
    )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
    ) ON [PRIMARY]

END

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0 - Opened, 1 - Closed, 2 - Abandoned, 3 - Timeout' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'wc2_sessions', @level2type=N'COLUMN',@level2name=N'w2s_status'
GO

DECLARE @RC INT

SELECT @RC = COUNT(*) FROM sys.default_constraints WHERE name like '%wc2_sessions%'

IF (@RC < 1) 
BEGIN

    ALTER TABLE [dbo].[wc2_sessions] ADD  CONSTRAINT [DF_wc2_sessions_w2s_last_sequence_id]  DEFAULT ((0)) FOR [w2s_last_sequence_id]
    ALTER TABLE [dbo].[wc2_sessions] ADD  CONSTRAINT [DF_wc2_sessions_w2s_last_transaction_id]  DEFAULT ((0)) FOR [w2s_last_transaction_id]
    ALTER TABLE [dbo].[wc2_sessions] ADD  CONSTRAINT [DF_wc2_sessions_w2s_started]  DEFAULT (getdate()) FOR [w2s_started]
    ALTER TABLE [dbo].[wc2_sessions] ADD  CONSTRAINT [DF_wc2_sessions_w2s_status]  DEFAULT ((0)) FOR [w2s_status]

END

GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wcp_sessions]') AND type in (N'U'))
BEGIN

    CREATE TABLE [dbo].[wcp_sessions](
        [ws_site_id] [int] NOT NULL,
        [ws_session_id] [bigint] NOT NULL,
        [ws_terminal_id] [int] NOT NULL,
        [ws_last_sequence_id] [bigint] NOT NULL,
        [ws_last_transaction_id] [bigint] NOT NULL,
        [ws_last_rcvd_msg] [datetime] NULL,
        [ws_started] [datetime] NOT NULL,
        [ws_finished] [datetime] NULL,
        [ws_status] [int] NOT NULL,
        [ws_timestamp] [bigint] NULL,
        [ws_server_name] [nvarchar](50) NULL,
    CONSTRAINT [PK_wcp_sessions] PRIMARY KEY CLUSTERED 
    (
        [ws_site_id] ASC,
        [ws_session_id] ASC
    )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
    ) ON [PRIMARY]

END

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0 - Opened, 1 - Closed, 2 - Abandoned, 3 - Timeout' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'wcp_sessions', @level2type=N'COLUMN',@level2name=N'ws_status'
GO

DECLARE @RC INT

SELECT @RC = COUNT(*) FROM sys.default_constraints WHERE name like '%wcp_sessions%'

IF (@RC < 1) 
BEGIN

    ALTER TABLE [dbo].[wcp_sessions] ADD  CONSTRAINT [DF_wcp_sessions_ws_last_sequence_id]  DEFAULT ((0)) FOR [ws_last_sequence_id]
    ALTER TABLE [dbo].[wcp_sessions] ADD  CONSTRAINT [DF_wcp_sessions_ws_last_transaction_id]  DEFAULT ((0)) FOR [ws_last_transaction_id]
    ALTER TABLE [dbo].[wcp_sessions] ADD  CONSTRAINT [DF_wcp_sessions_ws_started]  DEFAULT (getdate()) FOR [ws_started]
    ALTER TABLE [dbo].[wcp_sessions] ADD  CONSTRAINT [DF_wcp_sessions_ws_status]  DEFAULT ((0)) FOR [ws_status]

END

GO

-- ADD COLUMNS
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[TERMINALS]') and name = 'TE_NUMBER_LINES')
BEGIN
    ALTER TABLE TERMINALS ADD TE_NUMBER_LINES nvarchar(50) NULL
END

GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[TERMINALS]') and name = 'TE_CONTRACT_ID')
BEGIN
    ALTER TABLE TERMINALS ADD TE_CONTRACT_ID nvarchar(50) NULL
END

GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[terminals_last_changed]'))
DROP VIEW [dbo].[terminals_last_changed]
GO

CREATE VIEW [dbo].[terminals_last_changed]
AS

SELECT   TE_MASTER_ID as TLC_MASTER_ID
			 , TE_BASE_NAME as TLC_BASE_NAME
			 , MAX(TE_CHANGE_ID) AS TLC_CHANGE_ID
			 , MAX(TE_TERMINAL_ID) AS TLC_TERMINAL_ID
			 , MIN(TE_ACTIVATION_DATE) AS TLC_CREATED
			 , MAX(TE_ACTIVATION_DATE) AS TLC_CHANGED
			 , TE_SITE_ID AS TLC_SITE_ID
 FROM TERMINALS
 GROUP BY TE_MASTER_ID,TE_BASE_NAME, TE_SITE_ID


GO

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[wcp_sessions]') AND name = N'IX_wcp_status')
DROP INDEX [IX_wcp_status] ON [dbo].[wcp_sessions] WITH ( ONLINE = OFF )
GO

CREATE NONCLUSTERED INDEX [IX_wcp_status] ON [dbo].[wcp_sessions] 
(
    [ws_status] ASC,
    [ws_site_id] ASC,
    [ws_terminal_id] ASC,
    [ws_session_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[wc2_sessions]') AND name = N'IX_wc2_status')
DROP INDEX [IX_wc2_status] ON [dbo].[wc2_sessions] WITH ( ONLINE = OFF )
GO

CREATE NONCLUSTERED INDEX [IX_wc2_status] ON [dbo].[wc2_sessions] 
(
    [w2s_status] ASC,
    [w2s_site_id] ASC,
    [w2s_terminal_id] ASC,
    [w2s_session_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

/******* RECORDS *******/



/******* PROCEDURES *******/



/******* TRIGGERS *******/
