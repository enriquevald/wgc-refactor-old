/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_100]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 1;

SET @New_ReleaseId = 2;
SET @New_ScriptName = N'UpdateTo_18.002.sql';
SET @New_Description = N'.';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** CHANGES ******/
BEGIN TRANSACTION
GO

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[account_movements]') AND name = N'IX_type_date_account')
DROP INDEX [IX_type_date_account] ON [dbo].[account_movements] WITH ( ONLINE = OFF )
GO
CREATE NONCLUSTERED INDEX [IX_type_date_account] ON [dbo].[account_movements] 
(
	[am_type] ASC,
	[am_datetime] ASC,
	[am_account_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO


IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[accounts]') AND name = N'IX_ac_last_activity')
DROP INDEX [IX_ac_last_activity] ON [dbo].[accounts] WITH ( ONLINE = OFF )
GO
CREATE NONCLUSTERED INDEX [IX_ac_last_activity] ON [dbo].[accounts] 
(
	[ac_last_activity] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

ALTER PROCEDURE [dbo].[Update_PointsAccountMovement]
      @pSiteId                    INT 
,     @pMovementId                BIGINT
,     @pPlaySessionId             BIGINT
,     @pAccountId                 BIGINT
,     @pTerminalId                INT
,     @pWcpSequenceId             BIGINT
,     @pWcpTransactionId          BIGINT
,     @pDatetime                  DATETIME
,     @pType                      INT
,     @pInitialBalance            MONEY
,     @pSubAmount                 MONEY
,     @pAddAmount                 MONEY
,     @pFinalBalance              MONEY
,     @pCashierId                 INT
,     @pCashierName               NVARCHAR(50)
,     @pTperminalName             NVARCHAR(50)
,     @pOperationId               BIGINT
,     @pDetails                   NVARCHAR(256)
,     @pReasons                   NVARCHAR(64)
, @pErrorCode                 INT          OUTPUT
, @PointsSequenceId           BIGINT       OUTPUT
, @Points                     MONEY        OUTPUT

AS
BEGIN   
  DECLARE @LocalDelta AS MONEY

  SET @pErrorCode = 1
  SET @PointsSequenceId = NULL
  SET @Points = null

  IF NOT EXISTS (SELECT 1 FROM ACCOUNTS WHERE ac_account_id = @pAccountId ) 
  BEGIN
    SET @pErrorCode = 1
    RETURN
  END

  IF NOT EXISTS (SELECT 1 FROM ACCOUNT_MOVEMENTS WHERE AM_SITE_ID = @pSiteId AND AM_MOVEMENT_ID = @pMovementId) 
  BEGIN  
    INSERT INTO   ACCOUNT_MOVEMENTS
                ( [am_site_id]
                , [am_movement_id]
                , [am_play_session_id]
                , [am_account_id]
                , [am_terminal_id]
                , [am_wcp_sequence_id]
                , [am_wcp_transaction_id]
                , [am_datetime]
                , [am_type]
                , [am_initial_balance]
                , [am_sub_amount]
                , [am_add_amount]
                , [am_final_balance]
                , [am_cashier_id]
                , [am_cashier_name]
                , [am_terminal_name]
                , [am_operation_id]
                , [am_details]
                , [am_reasons])
         VALUES
                ( @pSiteId           
                , @pMovementId       
                , @pPlaySessionId   
                , @pAccountId        
                , @pTerminalId       
                , @pWcpSequenceId    
                , @pWcpTransactionId 
                , @pDatetime         
                , @pType             
                , @pInitialBalance  
                , @pSubAmount        
                , @pAddAmount        
                , @pFinalBalance     
                , @pCashierId        
                , @pCashierName      
                , @pTperminalName    
                , @pOperationId      
                , @pDetails          
                , @pReasons  )          
  
    SET @LocalDelta = ISNULL (@pAddAmount - @pSubAmount, 0)

    IF (@pType = 39 OR @pType = 67)
       SET @LocalDelta = 0
    
    UPDATE   ACCOUNTS
       SET   AC_POINTS           = AC_POINTS + @LocalDelta  
           , AC_MS_LAST_SITE_ID  = @pSiteId 
           , AC_LAST_ACTIVITY    = GETDATE()         
     WHERE   AC_ACCOUNT_ID       = @pAccountId 
  END
  
  SET @pErrorCode = 0

  SELECT   @PointsSequenceId     = AC_MS_POINTS_SEQ_ID 
         , @Points               = AC_POINTS
    FROM   ACCOUNTS
   WHERE   AC_ACCOUNT_ID = @pAccountId 
   
END -- Update_PointsAccountMovement
GO

INSERT INTO [dbo].[general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value])
     VALUES
           ('WigosGUI'
           ,'ClosingTime'
           ,'8')
GO

--SITE_TASK
DELETE MS_SITE_TASKS 
INSERT INTO MS_SITE_TASKS (ST_SITE_ID, ST_TASK_ID, ST_ENABLED, ST_INTERVAL_SECONDS) VALUES (0,  1, 1,   60); --DownloadParameters
INSERT INTO MS_SITE_TASKS (ST_SITE_ID, ST_TASK_ID, ST_ENABLED, ST_INTERVAL_SECONDS) VALUES (0, 11, 1,   60); --UploadPointsMovements
INSERT INTO MS_SITE_TASKS (ST_SITE_ID, ST_TASK_ID, ST_ENABLED, ST_INTERVAL_SECONDS) VALUES (0, 12, 1,   60); --UploadPersonalInfo
INSERT INTO MS_SITE_TASKS (ST_SITE_ID, ST_TASK_ID, ST_ENABLED, ST_INTERVAL_SECONDS) VALUES (0, 21, 0,  180); --DownloadPoints_Site
INSERT INTO MS_SITE_TASKS (ST_SITE_ID, ST_TASK_ID, ST_ENABLED, ST_INTERVAL_SECONDS) VALUES (0, 22, 1,  180); --DownloadPoints_All 
INSERT INTO MS_SITE_TASKS (ST_SITE_ID, ST_TASK_ID, ST_ENABLED, ST_INTERVAL_SECONDS) VALUES (0, 31, 0,  180); --DownloadPersonalInfo_Card
INSERT INTO MS_SITE_TASKS (ST_SITE_ID, ST_TASK_ID, ST_ENABLED, ST_INTERVAL_SECONDS) VALUES (0, 32, 0,  180); --DownloadPersonalInfo_Site
INSERT INTO MS_SITE_TASKS (ST_SITE_ID, ST_TASK_ID, ST_ENABLED, ST_INTERVAL_SECONDS) VALUES (0, 33, 1,  180); --DownloadPersonalInfo_All 

GO

COMMIT
