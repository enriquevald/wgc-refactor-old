/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_100]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 114;

SET @New_ReleaseId = 115;
SET @New_ScriptName = N'UpdateTo_18.115.sql';
SET @New_Description = N'New release v03.007.0014'; 

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/**** GENERAL PARAM *****/
--ENABLED
IF NOT EXISTS(SELECT   1 
                FROM   GENERAL_PARAMS 
               WHERE   GP_GROUP_KEY = 'InHouseAPI'
                 AND   GP_SUBJECT_KEY = 'Enabled'
             )
BEGIN
    INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
    VALUES ('InHouseAPI', 'Enabled', '0')
END
GO

-- Get if Reception is enabled
DECLARE @ReceptionVisitEnabledValue  as nvarchar(50)

SELECT   @ReceptionVisitEnabledValue = MIN(gp_key_value)
  FROM   GENERAL_PARAMS 
 WHERE   1 = 1 
   AND   GP_GROUP_KEY = 'Reception'
   AND   GP_SUBJECT_KEY = 'Enabled'

--ReceptionVisit is enabled if Reception is enabled.
set @ReceptionVisitEnabledValue = ISNULL(@ReceptionVisitEnabledValue, '0');

--PlaySession is enabled if Reception is NOT enabled.
DECLARE @PlaySessionEnabledValue    AS nvarchar(50)
IF (@ReceptionVisitEnabledValue = '0')
    SET @PlaySessionEnabledValue = '1'
ELSE
    SET @PlaySessionEnabledValue = '0'

--Reception
IF NOT EXISTS(SELECT   1 
                FROM   GENERAL_PARAMS 
               WHERE   GP_GROUP_KEY = 'InHouseAPI'
                 AND   GP_SUBJECT_KEY = 'ReceptionVisitEnabled'
             )
BEGIN
    INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
    VALUES ('InHouseAPI', 'ReceptionVisitEnabled', @ReceptionVisitEnabledValue)
END

--Play Session
IF NOT EXISTS(SELECT   1 
                FROM   GENERAL_PARAMS 
               WHERE   GP_GROUP_KEY = 'InHouseAPI'
                 AND   GP_SUBJECT_KEY = 'PlaySessionEnabled'
             )
BEGIN
    INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
    VALUES ('InHouseAPI', 'PlaySessionEnabled', @PlaySessionEnabledValue)
END

--AskChildsInterval (in seconds)
IF NOT EXISTS(SELECT   1 
                FROM   GENERAL_PARAMS 
               WHERE   GP_GROUP_KEY = 'InHouseAPI'
                 AND   GP_SUBJECT_KEY = 'AskChildsInterval'
             )
BEGIN
    INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
    VALUES ('InHouseAPI', 'AskChildsInterval', '300')
END
GO

-- DeleteThreshold (in hours)
IF NOT EXISTS(SELECT   1 
                FROM   GENERAL_PARAMS 
               WHERE   GP_GROUP_KEY = 'InHouseAPI'
                 AND   GP_SUBJECT_KEY = 'DeleteThreshold'
             )
BEGIN
    INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE)
    VALUES ('InHouseAPI', 'DeleteThreshold', '48')
END
GO


/******* TABLES  *******/
-- ALTER TABLE SITE

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'sites' AND COLUMN_NAME = 'st_inhouse_api_url') 
BEGIN
	  ALTER TABLE		[dbo].[sites] 
	  ADD				st_inhouse_api_url		NVARCHAR(255)		NULL
END
GO

-- ALTER TABLE ACCOUNTS

IF NOT EXISTS( SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'accounts' AND COLUMN_NAME = 'ac_last_update_in_local_time')
BEGIN
    ALTER TABLE     [dbo].[accounts]    ADD     ac_last_update_in_local_time    DATETIME    DEFAULT GETDATE()   NOT NULL
END
GO

IF NOT EXISTS( SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'accounts' AND COLUMN_NAME = 'ac_last_update_in_utc_time')
BEGIN
    ALTER TABLE     [dbo].[accounts]    ADD     ac_last_update_in_utc_time  DATETIME    DEFAULT GETUTCDATE()   NOT NULL
END
GO


-- CREATE TABLE IN_HOUSE_EVENTS
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[in_house_events]') AND type in (N'U'))
BEGIN
    CREATE TABLE [dbo].[in_house_events]
    (
        ihe_event_id                        BIGINT  IDENTITY(1, 1)  NOT NULL,
        ihe_site_id                         INT                     NOT NULL,
        ihe_event_inserted_in_local_time    DATETIME                NOT NULL,
        ihe_event_inserted_in_utc_time      DATETIME                NOT NULL,
        ihe_event_datetime_in_local_time    DATETIME                NOT NULL,
        ihe_event_datetime_in_utc_time      DATETIME                NOT NULL,
        ihe_event_type                      INT                     NOT NULL,
        ihe_customer_id                     BIGINT                  NOT NULL,
        ihe_egm_id                          INT                     NULL,
		ihe_event_site_id					BIGINT					NOT NULL,
        CONSTRAINT [PK_in_house_events] PRIMARY KEY CLUSTERED
        (
            ihe_event_id,
			ihe_event_site_id,
			ihe_site_id
        ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
    ) ON [PRIMARY]
END
GO


/******* RECORDS *******/



/******* PROCEDURES *******/



/******* TRIGGERS *******/
