/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_100]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 42;

SET @New_ReleaseId = 43;
SET @New_ScriptName = N'UpdateTo_18.043.sql';
SET @New_Description = N'Added table identification_types';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/
CREATE TABLE [dbo].[identification_types](
      [idt_id] [int] NOT NULL,
      [idt_enabled] [bit] NOT NULL,
      [idt_order] [int] NOT NULL,
      [idt_name] [nvarchar](50) NOT NULL,
      [idt_description] [nvarchar](max) NULL,
      [idt_regex] [nvarchar](max) NULL,
      [idt_aux_name] [nvarchar](50) NULL,
CONSTRAINT [PK_identification_types] PRIMARY KEY CLUSTERED 
(
      [idt_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[identification_types] ADD  CONSTRAINT [DF_identification_types_idt_enabled]  DEFAULT ((1)) FOR [idt_enabled]
GO

ALTER TABLE [dbo].[identification_types] ADD  CONSTRAINT [DF_identification_types_idt_order]  DEFAULT ((0)) FOR [idt_order]
GO

-- MEXICO
INSERT INTO   IDENTIFICATION_TYPES ( IDT_ID, IDT_ORDER, IDT_NAME )  VALUES (  0,100, 'Otro' )
INSERT INTO   IDENTIFICATION_TYPES ( IDT_ID, IDT_ORDER, IDT_NAME )  VALUES (  1,  1, 'RFC' )
INSERT INTO   IDENTIFICATION_TYPES ( IDT_ID, IDT_ORDER, IDT_NAME )  VALUES (  2,  2, 'CURP' )
INSERT INTO   IDENTIFICATION_TYPES ( IDT_ID, IDT_ORDER, IDT_NAME )  VALUES (  3,  3, 'IFE' )
INSERT INTO   IDENTIFICATION_TYPES ( IDT_ID, IDT_ORDER, IDT_NAME )  VALUES (  4,  4, 'Pasaporte' )
INSERT INTO   IDENTIFICATION_TYPES ( IDT_ID, IDT_ORDER, IDT_NAME )  VALUES (  5,  5, 'Cartilla Militar' )
INSERT INTO   IDENTIFICATION_TYPES ( IDT_ID, IDT_ORDER, IDT_NAME )  VALUES (  6,  6, 'Residente Permanente' )
INSERT INTO   IDENTIFICATION_TYPES ( IDT_ID, IDT_ORDER, IDT_NAME )  VALUES (  7,  7, 'Residente Temporal' )
INSERT INTO   IDENTIFICATION_TYPES ( IDT_ID, IDT_ORDER, IDT_NAME )  VALUES (  8,  8, 'Licencia de Conducir' )
INSERT INTO   IDENTIFICATION_TYPES ( IDT_ID, IDT_ORDER, IDT_NAME )  VALUES (  9,  9, 'C�dula Profesional' )
INSERT INTO   IDENTIFICATION_TYPES ( IDT_ID, IDT_ORDER, IDT_NAME )  VALUES ( 10, 10, 'Matr�cula Consular' )
GO