/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_100]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 111;

SET @New_ReleaseId = 112;
SET @New_ScriptName = N'UpdateTo_18.112.sql';
SET @New_Description = N'New release v03.007.0003'; 

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/**** GENERAL PARAM *****/
IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'AGG' AND GP_SUBJECT_KEY = 'Buffering.Enabled')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('AGG', 'Buffering.Enabled', '0')
GO  

/******* TABLES  *******/
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[terminal_sas_meters]') AND type in (N'U'))
BEGIN
	CREATE TABLE [dbo].[terminal_sas_meters](
		[tsm_site_id] [bigint] NOT NULL,
		[tsm_terminal_id] [int] NOT NULL,
		[tsm_meter_code] [int] NOT NULL,
		[tsm_game_id] [int] NOT NULL,
		[tsm_denomination] [money] NOT NULL,
		[tsm_wcp_sequence_id] [bigint] NOT NULL,
		[tsm_last_reported] [datetime] NOT NULL,
		[tsm_last_modified] [datetime] NULL,
		[tsm_meter_value] [bigint] NOT NULL,
		[tsm_meter_max_value] [bigint] NOT NULL,
		[tsm_delta_value] [bigint] NOT NULL,
		[tsm_raw_delta_value] [bigint] NOT NULL,
		[tsm_delta_updating] [bit] NOT NULL,
		[tsm_sas_accounting_denom] [money] NULL,
		[tsm_timestamp] [timestamp] NOT NULL,
	 CONSTRAINT [PK_terminal_sas_meters] PRIMARY KEY CLUSTERED 
	(
	    [tsm_site_id] ASC,
		[tsm_terminal_id] ASC,
		[tsm_meter_code] ASC,
		[tsm_game_id] ASC,
		[tsm_denomination] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]
END

GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[terminal_sas_meters_history]') AND type in (N'U'))
BEGIN
	CREATE TABLE [dbo].[terminal_sas_meters_history](
		[tsmh_site_id] [bigint] NOT NULL,
		[tsmh_terminal_id] [int] NOT NULL,
		[tsmh_meter_code] [int] NOT NULL,
		[tsmh_game_id] [int] NOT NULL,
		[tsmh_denomination] [money] NOT NULL,
		[tsmh_type] [int] NOT NULL,
		[tsmh_datetime] [datetime] NOT NULL,
		[tsmh_meter_ini_value] [bigint] NULL,
		[tsmh_meter_fin_value] [bigint] NOT NULL,
		[tsmh_meter_increment] [bigint] NOT NULL,
		[tsmh_raw_meter_increment] [bigint] NOT NULL,
		[tsmh_last_reported] [datetime] NULL,
		[tsmh_sas_accounting_denom] [money] NULL,
		[tsmh_created_datetime] [datetime] NULL,
		[tsmh_group_id] [bigint] NULL,
		[tsmh_meter_origin] [int] NULL,
		[tsmh_meter_max_value] [bigint] NULL,
		[tsmh_timestamp] [timestamp] NOT NULL,
	 CONSTRAINT [PK_terminal_sas_meters_history] PRIMARY KEY CLUSTERED 
	(
	    [tsmh_site_id] ASC,
		[tsmh_terminal_id] ASC,
		[tsmh_meter_code] ASC,
		[tsmh_game_id] ASC,
		[tsmh_denomination] ASC,
		[tsmh_type] ASC,
		[tsmh_datetime] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]
END

GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[terminal_types]') AND type in (N'U'))

	CREATE TABLE [dbo].[terminal_types](
		[tt_type] [int] NOT NULL,
		[tt_name] [nchar](50) NOT NULL,
	 CONSTRAINT [PK_terminal_types] PRIMARY KEY CLUSTERED 
	(
		[tt_type] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]
	
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sas_meters_catalog]') AND type in (N'U'))
	CREATE TABLE [dbo].[sas_meters_catalog](
		[smc_meter_code] [int] NOT NULL,
		[smc_description] [nvarchar](max) NOT NULL,
		[smc_recomended] [int] NOT NULL,
		[smc_required] [int] NULL,
		[smc_name] [nvarchar](200) NULL,
	 CONSTRAINT [PK_sas_meters_catalog] PRIMARY KEY CLUSTERED 
	(
		[smc_meter_code] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sas_meters_groups]') AND type in (N'U'))
	CREATE TABLE [dbo].[sas_meters_groups](
		[smg_group_id] [int] NOT NULL,
		[smg_name] [nvarchar](50) NOT NULL,
		[smg_description] [nvarchar](200) NULL,
		[smg_required] [bit] NOT NULL,
	 CONSTRAINT [PK_sas_meters_groups] PRIMARY KEY CLUSTERED 
	(
		[smg_group_id] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]

GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sas_meters_catalog_per_group]') AND type in (N'U'))
	CREATE TABLE [dbo].[sas_meters_catalog_per_group](
		[smcg_group_id] [int] NOT NULL,
		[smcg_meter_code] [int] NOT NULL,
	 CONSTRAINT [PK_sas_meters_catalog_per_group] PRIMARY KEY CLUSTERED 
	(
		[smcg_group_id] ASC,
		[smcg_meter_code] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]

GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[terminals]') and name = 'te_iso_code')
  ALTER TABLE dbo.terminals ADD te_iso_code NVARCHAR(3) NULL
GO

IF NOT EXISTS (SELECT * FROM SYS.COLUMNS WHERE OBJECT_ID = OBJECT_ID(N'[DBO].[TERMINALS]') AND NAME = 'te_sas_accounting_denom')
	ALTER TABLE [dbo].[TERMINALS] ADD te_sas_accounting_denom MONEY NULL
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[terminals_last_changed]'))
	DROP VIEW [dbo].[terminals_last_changed]
GO

CREATE VIEW [dbo].[terminals_last_changed]
AS

SELECT   TE_MASTER_ID as TLC_MASTER_ID
			 , TE_BASE_NAME as TLC_BASE_NAME
			 , MAX(TE_CHANGE_ID) AS TLC_CHANGE_ID
			 , MAX(TE_TERMINAL_ID) AS TLC_TERMINAL_ID
			 , MIN(TE_ACTIVATION_DATE) AS TLC_CREATED
			 , MAX(TE_ACTIVATION_DATE) AS TLC_CHANGED
 FROM TERMINALS
 GROUP BY TE_MASTER_ID,TE_BASE_NAME

GO

/******* RECORDS *******/
	INSERT [dbo].[terminal_types] ([tt_type], [tt_name]) VALUES (3, N'3GS                                               ')
	INSERT [dbo].[terminal_types] ([tt_type], [tt_name]) VALUES (109, N'BONOPLAY                                          ')
	INSERT [dbo].[terminal_types] ([tt_type], [tt_name]) VALUES (106, N'CASHDESK DRAW                                     ')
	INSERT [dbo].[terminal_types] ([tt_type], [tt_name]) VALUES (111, N'CASHDESK DRAW TABLE 1                             ')
	INSERT [dbo].[terminal_types] ([tt_type], [tt_name]) VALUES (107, N'GAMING TABLE SEAT                                 ')
	INSERT [dbo].[terminal_types] ([tt_type], [tt_name]) VALUES (103, N'iMB                                               ')
	INSERT [dbo].[terminal_types] ([tt_type], [tt_name]) VALUES (104, N'iSTATS                                            ')
	INSERT [dbo].[terminal_types] ([tt_type], [tt_name]) VALUES (5, N'LKT SAS Host                                      ')
	INSERT [dbo].[terminal_types] ([tt_type], [tt_name]) VALUES (1, N'LKT WIN                                           ')
	INSERT [dbo].[terminal_types] ([tt_type], [tt_name]) VALUES (102, N'MOBILE BANK                                       ')
	INSERT [dbo].[terminal_types] ([tt_type], [tt_name]) VALUES (-1, N'Not set                                           ')
	INSERT [dbo].[terminal_types] ([tt_type], [tt_name]) VALUES (108, N'Offline                                           ')
	INSERT [dbo].[terminal_types] ([tt_type], [tt_name]) VALUES (105, N'PromoBOX                                          ')
	INSERT [dbo].[terminal_types] ([tt_type], [tt_name]) VALUES (100, N'SITE                                              ')
	INSERT [dbo].[terminal_types] ([tt_type], [tt_name]) VALUES (101, N'SITE JACKPOT                                      ')



-- INSERT
-- [sas_meters_catalog]

INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (0, N'Total coin in credits', 7, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (1, N'Total coin out credits', 7, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (2, N'Total jackpot credits', 7, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (3, N'Total hand paid cancelled credits', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (4, N'Total cancelled credits', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (5, N'Games played', 7, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (6, N'Games won', 7, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (7, N'Games lost', 7, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (8, N'Total credits from coin acceptor', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (9, N'Total credits paid from hopper', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (10, N'Total credits from coins to drop', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (11, N'Total credits from bills accepted', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (12, N'Current credits', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (13, N'Total SAS cashable ticket in, including nonrestricted tickets (cents) [same as meter 0080 + 0084]', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (14, N'Total SAS cashable ticket out, including debit tickets (cents) [same as meter 0086 + 008A]', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (15, N'Total SAS restricted ticket in (cents) [same as meter 0082]', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (16, N'Total SAS restricted ticket out (cents) [same as meter 0088]', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (17, N'Total SAS cashable ticket in, including nonrestricted tickets (quantity) [same as meter 0081 + 0085]', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (18, N'Total SAS cashable ticket out, including debit tickets (quantity) [same as meter 0087 + 008B]', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (19, N'Total SAS restricted ticket in (quantity) [same as meter 0083]', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (20, N'Total SAS restricted ticket out (quantity)  [same as meter 0089]', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (21, N'Total ticket in, including cashable, nonrestricted and restricted tickets (credits)', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (22, N'Total ticket out, including cashable, nonrestricted, restricted and debit tickets (credits)', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (23, N'Total electronic transfers to gaming machine, including cashable, nonrestricted, restricted and debit, whether transfer is to credit meter or to ticket (credits)', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (24, N'Total electronic transfers to host, including cashable, nonrestricted, restricted and win amounts (credits)', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (25, N'Total restricted amount played (credits)', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (26, N'Total nonrestricted amount played (credits)', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (27, N'Current restricted credits', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (28, N'Total machine paid paytable win, not including progressive or external bonus amounts (credits)', 7, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (29, N'Total machine paid progressive win (credits)', 7, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (30, N'Total machine paid external bonus win (credits)', 7, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (31, N'Total attendant paid paytable win, not including progressive or external bonus amounts (credits)', 7, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (32, N'Total attendant paid progressive win (credits)', 7, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (33, N'Total attendant paid external bonus win (credits)', 7, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (34, N'Total won credits (sum of total coin out and total jackpot)', 7, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (35, N'Total hand paid credits (sum of total hand paid cancelled credits and total jackpot)', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (36, N'Total drop, including but not limited to coins to drop, bills to drop, tickets to drop, and electronic in (credits)', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (37, N'Games since last power reset', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (38, N'Games since slot door closure', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (39, N'Total credits from external coin acceptor', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (40, N'Total cashable ticket in, including nonrestricted promotional tickets (credits)', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (41, N'Total regular cashable ticket in (credits)', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (42, N'Total restricted promotional ticket in (credits)', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (43, N'Total nonrestricted promotional ticket in (credits)', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (44, N'Total cashable ticket out, including debit tickets (credits)', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (45, N'Total restricted promotional ticket out (credits)', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (46, N'Electronic regular cashable transfers to gaming machine, not including external bonus awards (credits)', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (47, N'Electronic restricted promotional transfers to gaming machine, not including external bonus awards (credits)', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (48, N'Electronic nonrestricted promotional transfers to gaming machine, not including external bonus awards (credits)', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (49, N'Electronic debit transfers to gaming machine (credits)', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (50, N'Electronic regular cashable transfers to host (credits)', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (51, N'Electronic restricted promotional transfers to host (credits)', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (52, N'Electronic nonrestricted promotional transfers to host (credits)', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (53, N'Total regular cashable ticket in (quantity)', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (54, N'Total restricted promotional ticket in (quantity)', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (55, N'Total nonrestricted promotional ticket in (quantity)', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (56, N'Total cashable ticket out, including debit tickets (quantity)', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (57, N'Total restricted promotional ticket out (quantity)', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (62, N'Number of bills currently in the stacker (Issue exception 7B when this meter is reset)', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (63, N'Total value of bills currently in the stacker (credits) (Issue exception 7B when this meter is reset)', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (64, N'Total number of $1.00 bills accepted', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (65, N'Total number of $2.00 bills accepted', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (66, N'Total number of $5.00 bills accepted', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (67, N'Total number of $10.00 bills accepted', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (68, N'Total number of $20.00 bills accepted', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (69, N'Total number of $25.00 bills accepted', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (70, N'Total number of $50.00 bills accepted', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (71, N'Total number of $100.00 bills accepted', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (72, N'Total number of $200.00 bills accepted', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (73, N'Total number of $250.00 bills accepted', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (74, N'Total number of $500.00 bills accepted', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (75, N'Total number of $1,000.00 bills accepted', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (76, N'Total number of $2,000.00 bills accepted', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (77, N'Total number of $2,500.00 bills accepted', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (78, N'Total number of $5,000.00 bills accepted', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (79, N'Total number of $10,000.00 bills accepted', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (80, N'Total number of $20,000.00 bills accepted', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (81, N'Total number of $25,000.00 bills accepted', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (82, N'Total number of $50,000.00 bills accepted', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (83, N'Total number of $100,000.00 bills accepted', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (84, N'Total number of $200,000.00 bills accepted', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (85, N'Total number of $250,000.00 bills accepted', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (86, N'Total number of $500,000.00 bills accepted', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (87, N'Total number of $1,000,000.00 bills accepted', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (88, N'Total credits from bills to drop', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (89, N'Total number of $1.00 bills to drop', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (90, N'Total number of $2.00 bills to drop', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (91, N'Total number of $5.00 bills to drop', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (92, N'Total number of $10.00 bills to drop', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (93, N'Total number of $20.00 bills to drop', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (94, N'Total number of $50.00 bills to drop', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (95, N'Total number of $100.00 bills to drop', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (96, N'Total number of $200.00 bills to drop', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (97, N'Total number of $500.00 bills to drop', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (98, N'Total number of $1000.00 bills to drop', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (99, N'Total credits from bills diverted to hopper', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (100, N'Total number of $1.00 bills diverted to hopper', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (101, N'Total number of $2.00 bills diverted to hopper', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (102, N'Total number of $5.00 bills diverted to hopper', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (103, N'Total number of $10.00 bills diverted to hopper', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (104, N'Total number of $20.00 bills diverted to hopper', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (105, N'Total number of $50.00 bills diverted to hopper', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (106, N'Total number of $100.00 bills diverted to hopper', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (107, N'Total number of $200.00 bills diverted to hopper', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (108, N'Total number of $500.00 bills diverted to hopper', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (109, N'Total number of $1000.00 bills diverted to hopper', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (110, N'Total credits from bills dispensed from hopper', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (111, N'Total number of $1.00 bills dispensed from hopper', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (112, N'Total number of $2.00 bills dispensed from hopper', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (113, N'Total number of $5.00 bills dispensed from hopper', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (114, N'Total number of $10.00 bills dispensed from hopper', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (115, N'Total number of $20.00 bills dispensed from hopper', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (116, N'Total number of $50.00 bills dispensed from hopper', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (117, N'Total number of $100.00 bills dispensed from hopper', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (118, N'Total number of $200.00 bills dispensed from hopper', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (119, N'Total number of $500.00 bills dispensed from hopper', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (120, N'Total number of $1000.00 bills dispensed from hopper', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (127, N'Weighted average theoretical payback percentage in hundredths of a percent', 3, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (128, N'Regular cashable ticket in (cents)', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (129, N'Regular cashable ticket in (quantity)', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (130, N'Restricted ticket in (cents)', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (131, N'Restricted ticket in (quantity)', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (132, N'Nonrestricted ticket in (cents)', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (133, N'Nonrestricted ticket in (quantity)', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (134, N'Regular cashable ticket out (cents)', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (135, N'Regular cashable ticket out (quantity)', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (136, N'Restricted ticket out (cents)', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (137, N'Restricted ticket out (quantity)', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (138, N'Debit ticket out (cents)', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (139, N'Debit ticket out (quantity)', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (140, N'Validated cancelled credit handpay, receipt printed (cents)', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (141, N'Validated cancelled credit handpay, receipt printed (quantity)', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (142, N'Validated jackpot handpay, receipt printed (cents)', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (143, N'Validated jackpot handpay, receipt printed (quantity)', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (144, N'Validated cancelled credit handpay, no receipt (cents)', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (145, N'Validated cancelled credit handpay, no receipt (quantity)', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (146, N'Validated jackpot handpay, no receipt (cents)', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (147, N'Validated jackpot handpay, no receipt (quantity)', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (160, N'In-house cashable transfers to gaming machine (cents)', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (161, N'In-House transfers to gaming machine that included cashable amounts (quantity)', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (162, N'In-house restricted transfers to gaming machine (cents)', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (163, N'In-house transfers to gaming machine that included restricted amounts (quantity)', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (164, N'In-house nonrestricted transfers to gaming machine (cents)', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (165, N'In-house transfers to gaming machine that included nonrestricted amounts (quantity)', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (166, N'Debit transfers to gaming machine (cents)', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (167, N'Debit transfers to gaming machine (quantity)', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (168, N'In-house cashable transfers to ticket (cents)', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (169, N'In-house cashable transfers to ticket (quantity)', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (170, N'In-house restricted transfers to ticket (cents)', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (171, N'In-house restricted transfers to ticket (quantity)', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (172, N'Debit transfers to ticket (cents)', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (173, N'Debit transfers to ticket (quantity)', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (174, N'Bonus cashable transfers to gaming machine (cents)', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (175, N'Bonus transfers to gaming machine that included cashable amounts (quantity)', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (176, N'Bonus nonrestricted transfers to gaming machine (cents)', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (177, N'Bonus transfers to gaming machine that included nonrestricted amounts (quantity)', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (184, N'In-house cashable transfers to host (cents)', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (185, N'In-house transfers to host that included cashable amounts (quantity)', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (186, N'In-house restricted transfers to host (cents)', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (187, N'In-house transfers to host that included restricted amounts (quantity)', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (188, N'In-house nonrestricted transfers to host (cents)', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (189, N'In-house transfers to host that included nonrestricted amounts (quantity)', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (4096, N'Total progressive jackpot credits', 7, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (8192, N'Port 1 from PCD', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (8193, N'Port 2 from PCD', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (8194, N'Port 3 from PCD', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (8195, N'Port 4 from PCD', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (8196, N'Port 5 from PCD', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (8197, N'Port 6 from PCD', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (8198, N'Port 7 from PCD', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (8199, N'Port 8 from PCD', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (8200, N'Port 9 from PCD', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (8201, N'Port 10 from PCD', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (8202, N'Port 11 from PCD', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (8203, N'Port 12 from PCD', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (8204, N'Port 13 from PCD', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (8205, N'Port 14 from PCD', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (8206, N'Port 15 from PCD', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (8207, N'Port 16 from PCD', 1, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (65553, N'Slot door was opened', 0, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (65554, N'Slot door was closed', 0, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (65555, N'Drop door was opened', 0, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (65556, N'Drop door was closed', 0, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (65557, N'Card cage was opened', 0, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (65558, N'Card cage was closed', 0, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (65559, N'AC power was applied to gaming machine', 0, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (65561, N'Cashbox door was opened', 0, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (65562, N'Cashbox door was closed', 0, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (65565, N'Belly door was opened', 0, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (65566, N'Belly door was closed', 0, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (65576, N'Bill jam', 0, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (65577, N'Bill acceptor hardware failure', 0, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (65578, N'Reverse bill detected', 0, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (65579, N'bill rejected', 0, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (65580, N'Counterfeit bill detected', 0, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (65585, N'CMOS RAM error (data recovered from EEPROM)', 0, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (65586, N'CMOS RAM error (no data recovered from EEPROM)', 0, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (65587, N'CMOS RAM error (bad device)', 0, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (65588, N'EEPROM error (data error)', 0, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (65589, N'EEPROM error (bad device)', 0, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (65590, N'EPROM error (different checksum - version changed)', 0, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (65591, N'EPROM error (bad checksum compare)', 0, NULL, NULL)
GO

INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (65592, N'Partitioned EPROM error (checksum - versoin changed)', 0, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (65593, N'Partitioned EPROM error (bad checksum compare)', 0, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (65595, N'Low backup battery detected', 0, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (65632, N'Printer communication error', 0, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (65633, N'Printer paper out error', 0, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (65652, N'Printer paper low', 0, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (65653, N'Printer power off', 0, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (65654, N'Printer power on', 0, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (65655, N'Replace printer ribbon', 0, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (65656, N'Printer carriage jammed', 0, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (65793, N'SAS disconnected', 0, NULL, NULL)
INSERT [dbo].[sas_meters_catalog] ([smc_meter_code], [smc_description], [smc_recomended], [smc_required], [smc_name]) VALUES (65794, N'SAS connected', 0, NULL, NULL)


-- [sas_meters_groups]

INSERT [dbo].[sas_meters_groups] ([smg_group_id], [smg_name], [smg_description], [smg_required]) VALUES (1, N'System Bills', N'', 0)
INSERT [dbo].[sas_meters_groups] ([smg_group_id], [smg_name], [smg_description], [smg_required]) VALUES (2, N'Stackers', N'', 0)
INSERT [dbo].[sas_meters_groups] ([smg_group_id], [smg_name], [smg_description], [smg_required]) VALUES (3, N'PCD Meters', N'PCD Meters', 0)
INSERT [dbo].[sas_meters_groups] ([smg_group_id], [smg_name], [smg_description], [smg_required]) VALUES (10001, N'Tickets', N'', 0)
INSERT [dbo].[sas_meters_groups] ([smg_group_id], [smg_name], [smg_description], [smg_required]) VALUES (10002, N'Bills And Coins', N'', 0)
INSERT [dbo].[sas_meters_groups] ([smg_group_id], [smg_name], [smg_description], [smg_required]) VALUES (10003, N'Events', N'Events', 0)
INSERT [dbo].[sas_meters_groups] ([smg_group_id], [smg_name], [smg_description], [smg_required]) VALUES (10004, N'Played - Won', N'Played - Won', 0)
INSERT [dbo].[sas_meters_groups] ([smg_group_id], [smg_name], [smg_description], [smg_required]) VALUES (10005, N'Gaming Hall', N'Gaming Hall', 0)

-- [sas_meters_catalog_per_group]

INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (1, 64)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (1, 65)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (1, 66)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (1, 67)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (1, 68)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (1, 69)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (1, 70)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (1, 71)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (1, 72)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (1, 73)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (1, 74)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (1, 75)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (1, 76)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (1, 77)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (1, 78)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (1, 79)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (1, 80)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (1, 81)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (1, 82)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (1, 83)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (1, 84)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (1, 85)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (1, 86)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (1, 87)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (2, 11)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (2, 64)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (2, 65)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (2, 66)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (2, 67)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (2, 68)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (2, 69)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (2, 70)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (2, 71)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (2, 72)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (2, 73)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (2, 74)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (2, 75)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (2, 76)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (2, 77)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (2, 78)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (2, 79)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (2, 80)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (2, 81)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (2, 82)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (2, 83)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (2, 84)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (2, 85)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (2, 86)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (2, 87)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (2, 128)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (2, 129)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (2, 130)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (2, 131)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (2, 132)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (2, 133)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (3, 0)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (3, 1)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (3, 2)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (3, 3)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (3, 5)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (3, 6)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (3, 8)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (3, 9)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (3, 11)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (3, 36)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (3, 110)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (10001, 128)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (10001, 129)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (10001, 130)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (10001, 131)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (10001, 132)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (10001, 133)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (10001, 134)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (10001, 135)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (10001, 136)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (10001, 137)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (10001, 138)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (10001, 139)

INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (10003, 65553)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (10003, 65554)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (10003, 65555)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (10003, 65556)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (10003, 65557)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (10003, 65558)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (10003, 65559)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (10003, 65561)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (10003, 65562)
GO

INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (10003, 65565)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (10003, 65566)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (10003, 65576)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (10003, 65577)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (10003, 65578)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (10003, 65579)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (10003, 65580)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (10003, 65585)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (10003, 65586)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (10003, 65587)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (10003, 65588)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (10003, 65589)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (10003, 65590)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (10003, 65591)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (10003, 65592)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (10003, 65593)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (10003, 65595)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (10003, 65632)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (10003, 65633)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (10003, 65652)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (10003, 65653)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (10003, 65654)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (10003, 65655)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (10003, 65656)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (10003, 65793)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (10003, 65794)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (10004, 0)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (10004, 1)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (10004, 2)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (10004, 3)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (10004, 5)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (10004, 6)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (10004, 12)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (10004, 25)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (10004, 26)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (10004, 29)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (10004, 30)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (10004, 32)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (10004, 33)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (10004, 35)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (10004, 37)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (10004, 160)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (10004, 161)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (10004, 162)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (10004, 163)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (10004, 184)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (10004, 185)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (10004, 186)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (10004, 187)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (10004, 4096)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (10005, 0)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (10005, 1)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (10005, 2)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (10005, 3)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (10005, 5)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (10005, 6)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (10005, 9)
INSERT [dbo].[sas_meters_catalog_per_group] ([smcg_group_id], [smcg_meter_code]) VALUES (10005, 36)
GO

ALTER TABLE [dbo].[sas_meters_catalog_per_group]  WITH CHECK ADD  CONSTRAINT [FK_sas_meters_catalog_per_group_sas_meters_catalog] FOREIGN KEY([smcg_meter_code])
REFERENCES [dbo].[sas_meters_catalog] ([smc_meter_code])
GO
ALTER TABLE [dbo].[sas_meters_catalog_per_group] CHECK CONSTRAINT [FK_sas_meters_catalog_per_group_sas_meters_catalog]
GO
/****** Object:  ForeignKey [FK_sas_meters_catalog_per_group_sas_meters_groups]    Script Date: 09/18/2017 12:28:02 ******/
ALTER TABLE [dbo].[sas_meters_catalog_per_group]  WITH CHECK ADD  CONSTRAINT [FK_sas_meters_catalog_per_group_sas_meters_groups] FOREIGN KEY([smcg_group_id])
REFERENCES [dbo].[sas_meters_groups] ([smg_group_id])
GO

ALTER TABLE [dbo].[sas_meters_catalog_per_group] CHECK CONSTRAINT [FK_sas_meters_catalog_per_group_sas_meters_groups]
GO
/******* PROCEDURES *******/


/******* TRIGGERS *******/
