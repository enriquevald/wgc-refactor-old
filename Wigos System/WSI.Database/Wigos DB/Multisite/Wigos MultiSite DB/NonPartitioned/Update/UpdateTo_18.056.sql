/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_100]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 55;

SET @New_ReleaseId = 56;
SET @New_ScriptName = N'UpdateTo_18.056.sql';
SET @New_Description = N'Update alarms catalog';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/

/****** INDEXES ******/

/****** RECORDS ******/

DELETE FROM ALARM_CATEGORIES WHERE ALC_CATEGORY_ID=37  
DELETE FROM ALARM_CATALOG WHERE ALCG_ALARM_CODE=99997
DELETE FROM ALARM_CATALOG WHERE ALCG_ALARM_CODE=99998
DELETE FROM ALARM_CATALOG WHERE ALCG_ALARM_CODE=1048584
GO

INSERT INTO ALARM_CATALOG_PER_CATEGORY (ALCC_ALARM_CODE,ALCC_CATEGORY,ALCC_TYPE) VALUES (589827,38,0)
INSERT INTO ALARM_CATALOG_PER_CATEGORY (ALCC_ALARM_CODE,ALCC_CATEGORY,ALCC_TYPE) VALUES (589829,38,0)
GO

UPDATE ALARM_GROUPS SET ALG_TYPE=0
UPDATE ALARM_GROUPS SET ALG_NAME='M�quina' WHERE ALG_ALARM_GROUP_ID =1
GO

INSERT INTO   [dbo].[ALARM_CATEGORIES] 
            ( [ALC_CATEGORY_ID], [ALC_ALARM_GROUP_ID], [ALC_TYPE], [ALC_NAME], [ALC_VISIBLE] )
     VALUES ( 45, 1, 0, N'T�cnico', 1 )
     
INSERT INTO   [dbo].[ALARM_CATALOG] 
            ( [ALCG_ALARM_CODE], [ALCG_TYPE], [ALCG_NAME], [ALCG_DESCRIPTION], [ALCG_VISIBLE] )
     VALUES ( 393254, 0, N'Insertada tarjeta de t�cnico', N'Insertada tarjeta de t�cnico', 1 )

INSERT INTO   [dbo].[ALARM_CATALOG] 
            ( [ALCG_ALARM_CODE], [ALCG_TYPE], [ALCG_NAME], [ALCG_DESCRIPTION], [ALCG_VISIBLE] )
     VALUES ( 393255, 0, N'Retirada tarjeta de t�cnico', N'Retirada tarjeta de t�cnico', 1 )
     
INSERT INTO   [dbo].[ALARM_CATALOG_PER_CATEGORY] 
            ( [ALCC_ALARM_CODE], [ALCC_CATEGORY], [ALCC_TYPE], [ALCC_DATETIME] ) 
     VALUES ( 393254, 45, 0, GETDATE() )
     
INSERT INTO   [dbo].[ALARM_CATALOG_PER_CATEGORY] 
            ( [ALCC_ALARM_CODE], [ALCC_CATEGORY], [ALCC_TYPE], [ALCC_DATETIME] ) 
     VALUES ( 393255, 45, 0, GETDATE() )
GO	 

/******* STORED PROCEDURES *******/
