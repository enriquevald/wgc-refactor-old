/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_100]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 92;

SET @New_ReleaseId = 93;
SET @New_ScriptName = N'UpdateTo_18.093.sql';
SET @New_Description = N'AR Regionalization';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/

/****** INDEXES ******/

/****** RECORDS ******/

-- ARGENTINA OCCUPATIONS
IF (NOT EXISTS (SELECT * FROM occupations WHERE oc_country_iso_code2 = 'AR' ))
BEGIN
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('NOT AVAILABLE','0000001',1,1000,'AR')
END
GO

-- ARGENTINA IDENTIFICATION_TYPES
IF (NOT EXISTS (SELECT * FROM identification_types WHERE idt_country_iso_code2 = 'AR' ))
BEGIN
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (160,1,100,'DNI','AR')
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (161,1,1,'Pasaporte','AR')
END
GO

-- ARGENTINA REGIONS
IF (NOT EXISTS (SELECT * FROM federal_states WHERE fs_country_iso_code2 = 'AR' ))
BEGIN
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Buenos Aires','AR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Catamarca','AR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('C�rdoba','AR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Corrientes','AR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Chaco','AR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Chubut','AR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Entre R�os','AR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Formosa','AR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Jujuy','AR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('La Pampa','AR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('La Rioja','AR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Mendoza','AR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Misiones','AR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Neuqu�n','AR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('R�o Negro','AR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Salta','AR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('San Juan','AR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('San Luis','AR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Santa Cruz','AR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Santa Fe','AR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Santiago del Estero','AR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Tierra del Fuego, Ant�rtida e islas del Atl. Sur','AR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Tucum�n','AR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Ciudad Aut�noma de Buenos Aires','AR');
END
GO

IF (NOT EXISTS (SELECT * FROM federal_states WHERE fs_country_iso_code2 = 'PH' ))
BEGIN
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Ilocos','PH')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Cagayan Valley','PH')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Central Luzon','PH')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Calabarzon','PH')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Mimaropa','PH')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Bicol','PH')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Western Visayas','PH')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Central Visayas','PH')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Eastern Visayas','PH')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Zamboanga Peninsula','PH')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Northern Mindanao','PH')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Davao Region','PH')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Soccsksargen','PH')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Caraga','PH')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Cordillera Administrative Region�(CAR)','PH')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('National Capital Region�(NCR)','PH')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Negros Island Region�(NIR)','PH')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Autonomous Region in Muslim Mindanao�(ARMM)','PH')

END
GO

-- PHILIPPINES OCCUPATIONS
IF (NOT EXISTS (SELECT * FROM occupations WHERE oc_country_iso_code2 = 'PH' ))
BEGIN
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('NO DISPONIBLE','0000001',1,1000,'PH')
END
GO

-- PHILIPPINES IDENTIFICATION_TYPES
IF (NOT EXISTS (SELECT * FROM identification_types WHERE idt_country_iso_code2 = 'PH' ))
BEGIN
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (140,1,100,'Other','PH')
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (141,1,1,'Passport ','PH')
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (142,1,2,'Driver�s License','PH')
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (143,1,3,'Professional Regulations Commission (PRC) ID','PH')
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (144,1,4,'Postal ID','PH')
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (145,1,5,'Voter�s ID','PH')
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (146,1,6,'Barangay Certification','PH')
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (147,1,7,'Tax Identification (TIN)','PH')
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (148,1,8,'Social Security System (SSS) Card','PH')
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (149,1,9,'Senior Citizen Card','PH')
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (150,1,10,'Alien Certificate of Registration','PH')
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (151,1,11,'Unified Multi-purpose ID (UMID)','PH')
END
GO

IF (NOT EXISTS (SELECT * FROM occupations WHERE oc_country_iso_code2 = 'NI' ))
BEGIN
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('NO DISPONIBLE','0000001',1,1000,'NI')
END
GO

-- NICARAGUA IDENTIFICATION_TYPES
IF (NOT EXISTS (SELECT * FROM identification_types WHERE idt_country_iso_code2 = 'NI' ))
BEGIN
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (170,1,100,'C�dula de identidad','NI')
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (171,1,1,'Pasaporte','NI')
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (172,1,2,'Licencia de conducir','NI')
END
GO

-- NICARAGUA REGIONS
IF (NOT EXISTS (SELECT * FROM federal_states WHERE fs_country_iso_code2 = 'NI' ))
BEGIN
	INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Boaco','NI');
	INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Carazo','NI');
	INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Chinandega','NI');
	INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Chontales','NI');
	INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Estel�','NI');
	INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Granada','NI');
	INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Jinotega','NI');
	INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Le�n','NI');
	INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Madriz','NI');
	INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Managua','NI');
	INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Masaya','NI');
	INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Matagalpa','NI');
	INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Nueva Segovia','NI');
	INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Rivas','NI');
	INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('R�o San Juan','NI');
END
GO

/****** STORED *******/

