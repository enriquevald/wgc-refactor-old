/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_100]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 65;

SET @New_ReleaseId = 66;
SET @New_ScriptName = N'UpdateTo_18.066.sql';
SET @New_Description = N'Changes for storeds Update_SalesPerHourFor; Delete GP for BankTransaction';
/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/

/****** INDEXES ******/

/****** RECORDS ******/

-- BankCard.DifferentiateType
DELETE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='Cashier.PaymentMethod' AND GP_SUBJECT_KEY='BankCard.DifferentiateType'
-- BankCard.EnterTransactionDetails
DELETE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='Cashier.PaymentMethod' AND GP_SUBJECT_KEY='BankCard.EnterTransactionDetails'
-- Check.EnterTransactionDetails
DELETE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='Cashier.PaymentMethod' AND GP_SUBJECT_KEY='Check.EnterTransactionDetails'
GO

UPDATE ALARM_CATALOG_PER_CATEGORY SET ALCC_CATEGORY = 40 WHERE ALCC_ALARM_CODE = 262152
GO

/******* STORED PROCEDURES *******/

  --------------------------------------------------------------------------------
  -- Copyright � 2013 Win Systems International
  --------------------------------------------------------------------------------
  --
  --   MODULE NAME: Insert_SalesPerHour.sql
  --
  --   DESCRIPTION: Insert_SalesPerHour
  --
  --        AUTHOR: Rafa Xandri
  --
  -- CREATION DATE: 08-MAR-2013
  --
  -- REVISION HISTORY:
  --
  -- Date        Author Description
  -- ----------- ------ ----------------------------------------------------------
  -- 26-MAR-2013 RXM    First release.  
  -- 30-05-2013  JML    Add translated game & Payout
  -- 01-SEP-2014 AMF    Progressive Jackpot
  -------------------------------------------------------------------------------- 

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Update_SalesPerHour]') AND type in (N'P', N'PC'))
   DROP PROCEDURE [dbo].[Update_SalesPerHour]
GO
  
CREATE PROCEDURE Update_SalesPerHour
  @pSiteId											INT 
, @pBaseHour										DATETIME
, @pTerminalId									INT
, @pGameId											INT
, @pPlayedCount									BIGINT
, @pPlayedAmount								MONEY
, @pWonCount										BIGINT
, @pWonAmount										MONEY
, @pTerminalName								NVARCHAR(50)
, @pGameName										NVARCHAR(50)
, @pTimeStamp										BIGINT
, @pTheoreticalWonAmount				MONEY
, @pPayout											MONEY
, @pUniqueId										BIGINT
, @pProgressiveProvisionAmount	MONEY
, @pJackpotAmount								MONEY
, @pProgressiveJackpotAmount		MONEY
, @pProgressiveJackpotAmount0		MONEY

AS
BEGIN   

  IF EXISTS (SELECT   1 
               FROM   SALES_PER_HOUR
              WHERE   sph_unique_id   = @pUniqueId
                AND   SPH_SITE_ID     = @pSiteId)

     UPDATE   SALES_PER_HOUR
        SET   SPH_BASE_HOUR										 = @pBaseHour
            , SPH_PLAYED_COUNT							   = @pPlayedCount
            , SPH_PLAYED_AMOUNT								 = @pPlayedAmount
            , SPH_WON_COUNT										 = @pWonCount
            , SPH_WON_AMOUNT									 = @pWonAmount
            , SPH_TERMINAL_NAME								 = @pTerminalName
            , SPH_GAME_NAME										 = @pGameName
            , SPH_TIMESTAMP										 = @pTimeStamp
            , SPH_THEORETICAL_WON_AMOUNT			 = @pTheoreticalWonAmount
            , SPH_PAYOUT											 = @pPayout
            , SPH_TERMINAL_ID									 = @pTerminalId
            , SPH_GAME_ID											 = @pGameId
            ,	SPH_PROGRESSIVE_PROVISION_AMOUNT = @pProgressiveProvisionAmount
            , SPH_JACKPOT_AMOUNT							 = @pJackpotAmount
            , SPH_PROGRESSIVE_JACKPOT_AMOUNT	 = @pProgressiveJackpotAmount
            , SPH_PROGRESSIVE_JACKPOT_AMOUNT_0 = @pProgressiveJackpotAmount0
      WHERE   sph_unique_id     = @pUniqueId
        AND   SPH_SITE_ID       = @pSiteId
        
  ELSE
  
    INSERT INTO   SALES_PER_HOUR
                ( SPH_BASE_HOUR
                , SPH_TERMINAL_ID
                , SPH_GAME_ID
                , SPH_PLAYED_COUNT
                , SPH_PLAYED_AMOUNT
                , SPH_WON_COUNT
                , SPH_WON_AMOUNT
                , SPH_SITE_ID
                , SPH_TERMINAL_NAME
                , SPH_GAME_NAME
                , SPH_TIMESTAMP
                , SPH_THEORETICAL_WON_AMOUNT
                , SPH_PAYOUT 
                , SPH_UNIQUE_ID
                , SPH_PROGRESSIVE_PROVISION_AMOUNT
                , SPH_JACKPOT_AMOUNT
                , SPH_PROGRESSIVE_JACKPOT_AMOUNT
                , SPH_PROGRESSIVE_JACKPOT_AMOUNT_0
                )
         VALUES
                ( @pBaseHour
                , @pTerminalId
                , @pGameId
                , @pPlayedCount
                , @pPlayedAmount
                , @pWonCount
                , @pWonAmount
                , @pSiteId
                , @pTerminalName 
                , @pGameName
                , @pTimeStamp
                , @pTheoreticalWonAmount
                , @pPayout 
                , @pUniqueId 
                , @pProgressiveProvisionAmount
                , @pJackpotAmount
                , @pProgressiveJackpotAmount
                , @pProgressiveJackpotAmount0               
                )

END -- Update_SalesPerHour
GO

  --------------------------------------------------------------------------------
  -- Copyright � 2013 Win Systems International
  --------------------------------------------------------------------------------
  --
  --   MODULE NAME: Insert_SalesPerHourForOldVersion.sql
  --
  --   DESCRIPTION: Insert_SalesPerHourForOldVersion
  --
  --        AUTHOR: Alberto Marcos
  --
  -- CREATION DATE: 01-SEP-2014
  --
  -- REVISION HISTORY:
  --
  -- Date        Author Description
  -- ----------- ------ ----------------------------------------------------------
  -- 01-SEP-2014 AMF    First release.  
  -------------------------------------------------------------------------------- 

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Update_SalesPerHourForOldVersion]') AND type in (N'P', N'PC'))
   DROP PROCEDURE [dbo].[Update_SalesPerHourForOldVersion]
GO
  
CREATE PROCEDURE Update_SalesPerHourForOldVersion
  @pSiteId											INT 
, @pBaseHour										DATETIME
, @pTerminalId									INT
, @pGameId											INT
, @pPlayedCount									BIGINT
, @pPlayedAmount								MONEY
, @pWonCount										BIGINT
, @pWonAmount										MONEY
, @pTerminalName								NVARCHAR(50)
, @pGameName										NVARCHAR(50)
, @pTimeStamp										BIGINT
, @pTheoreticalWonAmount				MONEY
, @pPayout											MONEY
, @pUniqueId										BIGINT

AS
BEGIN   

  IF EXISTS (SELECT   1 
               FROM   SALES_PER_HOUR
              WHERE   sph_unique_id   = @pUniqueId
                AND   SPH_SITE_ID     = @pSiteId)

     UPDATE   SALES_PER_HOUR
        SET   SPH_BASE_HOUR										 = @pBaseHour
            , SPH_PLAYED_COUNT							   = @pPlayedCount
            , SPH_PLAYED_AMOUNT								 = @pPlayedAmount
            , SPH_WON_COUNT										 = @pWonCount
            , SPH_WON_AMOUNT									 = @pWonAmount
            , SPH_TERMINAL_NAME								 = @pTerminalName
            , SPH_GAME_NAME										 = @pGameName
            , SPH_TIMESTAMP										 = @pTimeStamp
            , SPH_THEORETICAL_WON_AMOUNT			 = @pTheoreticalWonAmount
            , SPH_PAYOUT											 = @pPayout
            , SPH_TERMINAL_ID									 = @pTerminalId
            , SPH_GAME_ID											 = @pGameId
      WHERE   sph_unique_id     = @pUniqueId
        AND   SPH_SITE_ID       = @pSiteId
        
  ELSE
  
    INSERT INTO   SALES_PER_HOUR
                ( SPH_BASE_HOUR
                , SPH_TERMINAL_ID
                , SPH_GAME_ID
                , SPH_PLAYED_COUNT
                , SPH_PLAYED_AMOUNT
                , SPH_WON_COUNT
                , SPH_WON_AMOUNT
                , SPH_SITE_ID
                , SPH_TERMINAL_NAME
                , SPH_GAME_NAME
                , SPH_TIMESTAMP
                , SPH_THEORETICAL_WON_AMOUNT
                , SPH_PAYOUT 
                , SPH_UNIQUE_ID
                )
         VALUES
                ( @pBaseHour
                , @pTerminalId
                , @pGameId
                , @pPlayedCount
                , @pPlayedAmount
                , @pWonCount
                , @pWonAmount
                , @pSiteId
                , @pTerminalName 
                , @pGameName
                , @pTimeStamp
                , @pTheoreticalWonAmount
                , @pPayout 
                , @pUniqueId     
                )

END -- Update_SalesPerHourForOldVersion
GO


