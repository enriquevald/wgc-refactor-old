/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_100]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 57;

SET @New_ReleaseId = 58;
SET @New_ScriptName = N'UpdateTo_18.058.sql';
SET @New_Description = N'Columns added: ac_show_comments_on_cashier and ac_card_replacement_count; Index added: IX_am_account_id_type_datetime';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[accounts]') and name = 'ac_show_comments_on_cashier')
  ALTER TABLE [dbo].[accounts] ADD ac_show_comments_on_cashier bit NULL
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[accounts]') and name = 'ac_card_replacement_count')
  ALTER TABLE dbo.accounts ADD ac_card_replacement_count int NULL
GO

/****** INDEXES ******/

--
--
--
IF  NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[account_movements]') AND name = N'IX_am_account_id_type_datetime')
  CREATE NONCLUSTERED INDEX [IX_am_account_id_type_datetime] ON [dbo].[account_movements] 
  (
        [am_account_id] ASC,
        [am_type] ASC,
        [am_datetime] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

/****** RECORDS ******/

/******* STORED PROCEDURES *******/
