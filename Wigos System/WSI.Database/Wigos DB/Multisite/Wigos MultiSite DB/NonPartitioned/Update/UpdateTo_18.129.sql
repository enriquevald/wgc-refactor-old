/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_100]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@DoubleCheck_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 128;
SET @DoubleCheck_ReleaseId= 148;

SET @New_ReleaseId = 129;

SET @New_ScriptName = N'UpdateTo_18.129.sql';
SET @New_Description = N'Unificación de scripts 129 to 148'; 

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id BETWEEN @Exp_ReleaseId and @DoubleCheck_ReleaseId ))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id BETWEEN @Exp_ReleaseId and @DoubleCheck_ReleaseId ;
GO

/**** GENERAL PARAM *****/

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'WCP' AND GP_SUBJECT_KEY = 'SasMetersInterval')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE, GP_MS_DOWNLOAD_TYPE) VALUES ('WCP', 'SasMetersInterval', '30', '0');  
GO

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'WCP' AND GP_SUBJECT_KEY = 'IntervalBilling')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE, GP_MS_DOWNLOAD_TYPE) VALUES ('WCP', 'IntervalBilling', '30', '0');
GO

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'WCP' AND GP_SUBJECT_KEY = 'SasMetersInterval')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE, GP_MS_DOWNLOAD_TYPE) VALUES ('WCP', 'SasMetersInterval', '30', '0');  
GO

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'WCP' AND GP_SUBJECT_KEY = 'IntervalBilling')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE, GP_MS_DOWNLOAD_TYPE) VALUES ('WCP', 'IntervalBilling', '30', '0');
GO

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'WCP' AND GP_SUBJECT_KEY = 'IntervalBilling.CorretionTime')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE, GP_MS_DOWNLOAD_TYPE) VALUES ('WCP', 'IntervalBilling.CorretionTime', '1', '0');
GO

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PSAClient' AND GP_SUBJECT_KEY ='Salidas.ISR3')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE, GP_MS_DOWNLOAD_TYPE) VALUES ('PSAClient', 'Salidas.ISR3', '0', '1');
GO

IF NOT EXISTS (SELECT * FROM general_params WHERE gp_group_key = 'GUI' AND gp_subject_key = 'TerminalMassiveSearch')
BEGIN
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE, GP_MS_DOWNLOAD_TYPE) VALUES ('GUI', 'TerminalMassiveSearch', '0', '0')
END
GO

IF NOT EXISTS (SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'AGG' AND GP_SUBJECT_KEY = 'MaxValuesOfBigIncrement' )
BEGIN
    INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('AGG', 'MaxValuesOfBigIncrement', '0.01:500000;0.1:50000;1:5000')
END
GO

IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'AGG' AND GP_SUBJECT_KEY = 'MaxValuesOfBigIncrement')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) 
  VALUES ('AGG', 'MaxValuesOfBigIncrement', '0.01:500;0.02:1000;0.05:2500;0.1:5000;0.2:10000;0.25:12500;0.5:25000;1:50000;2:100000;5:250000:10:500000')
GO

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'AGG' AND GP_SUBJECT_KEY = 'TerminalMassiveSearch')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('AGG',' TerminalMassiveSearch', 0)
GO


/******* TABLES  *******/

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[terminal_groups]') AND type in (N'U'))
BEGIN
  CREATE TABLE [dbo].[terminal_groups](
         [tg_site_id] [int] NOT NULL,
         [tg_terminal_id] [int] NOT NULL,
         [tg_element_id] [bigint] NOT NULL,
         [tg_element_type] [int] NOT NULL,
  CONSTRAINT [PK_terminal_groups] PRIMARY KEY CLUSTERED 
  (
         [tg_site_id] ASC,
         [tg_terminal_id] ASC,
         [tg_element_id] ASC,
         [tg_element_type] ASC
  )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
  ) ON [PRIMARY]
END
GO


-- HANDPAYS TABLES

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[handpays]') AND type in (N'U'))
BEGIN

  CREATE TABLE [dbo].[handpays](
    [hp_site_id] [int] NOT NULL,
    [hp_id] [bigint] NOT NULL,
    [hp_terminal_id] [int] NULL,
    [hp_game_base_name] [nvarchar](50) NULL,
    [hp_datetime] [datetime] NOT NULL,
    [hp_previous_meters] [datetime] NULL,
    [hp_amount] [money] NOT NULL,
    [hp_te_name] [nvarchar](50) NULL,
    [hp_te_provider_id] [nvarchar](50) NULL,
    [hp_movement_id] [bigint] NULL,
    [hp_type] [int] NOT NULL,
    [hp_play_session_id] [bigint] NULL,
    [hp_site_jackpot_index] [int] NULL,
    [hp_site_jackpot_name] [nvarchar](50) NULL,
    [hp_site_jackpot_awarded_on_terminal_id] [int] NULL,
    [hp_site_jackpot_awarded_to_account_id] [bigint] NULL,
    [hp_status] [int] NULL,
    [hp_site_jackpot_notified] [bit] NOT NULL,
    [hp_ticket_id] [bigint] NULL,
    [hp_transaction_id] [bigint] NULL,
    [hp_candidate_play_session_id] [bigint] NULL,
    [hp_candidate_prev_play_session_id] [bigint] NULL,
    [hp_long_poll_1b_data] [xml] NULL,
    [hp_progressive_id] [bigint] NULL,
    [hp_level] [int] NULL,
    [hp_status_changed] [datetime] NULL,
    [hp_tax_base_amount] [money] NULL,
    [hp_tax_amount] [money] NULL,
    [hp_tax_pct] [decimal](18, 0) NULL,
    [hp_timestamp] [timestamp] NOT NULL,
    [hp_account_id] [bigint] NULL,
    [hp_payment_mode] [int] NOT NULL,
    [hp_status_calculated]  AS ([hp_status]&0xF000),
    [hp_operation_id] [bigint] NULL,
    [hp_amt0] [money] NULL,
    [hp_cur0] [nvarchar](3) NULL,
    [hp_amt1] [money] NULL,
    [hp_cur1] [nvarchar](3) NULL,    
   CONSTRAINT [PK_handpays] PRIMARY KEY CLUSTERED 
  (
    [hp_site_id] ASC,
    [hp_id] ASC
  )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
  ) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

END

GO

DECLARE @RC INT

SELECT @RC = COUNT(*) FROM sys.default_constraints WHERE name like '%DF_handpays%'

IF (@RC < 1) 
BEGIN

ALTER TABLE [dbo].[handpays] ADD  CONSTRAINT [DF_handpays_hp_datetime]  DEFAULT (getdate()) FOR [hp_datetime]
ALTER TABLE [dbo].[handpays] ADD  CONSTRAINT [DF_handpays_hp_type]  DEFAULT ((0)) FOR [hp_type]
ALTER TABLE [dbo].[handpays] ADD  CONSTRAINT [DF_handpays_hp_status]  DEFAULT ((0)) FOR [hp_status]
ALTER TABLE [dbo].[handpays] ADD  CONSTRAINT [DF_handpays_hp_site_jackpot_notified]  DEFAULT ((0)) FOR [hp_site_jackpot_notified]

END

GO

-- ADD COLUMNS
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[HANDPAYS]') and name = 'HP_ACCOUNT_ID')
BEGIN
    ALTER TABLE HANDPAYS ADD hp_account_id bigint NULL
END

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[HANDPAYS]') and name = 'hp_payment_mode')
BEGIN
  ALTER TABLE HANDPAYS ADD hp_payment_mode int NULL
END
  
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[HANDPAYS]') and name = 'hp_status_calculated')
BEGIN  
  ALTER TABLE HANDPAYS ADD hp_status_calculated AS ([hp_status]&0xF000)
END  

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[HANDPAYS]') and name = 'hp_operation_id')
BEGIN  
    ALTER TABLE HANDPAYS ADD hp_operation_id bigint NULL  
END  

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[HANDPAYS]') and name = 'hp_amt0')
BEGIN
    ALTER TABLE HANDPAYS ADD hp_amt0 money NULL  
END  

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[HANDPAYS]') and name = 'hp_cur0')
BEGIN
    ALTER TABLE HANDPAYS ADD hp_cur0 [nvarchar](3) NULL  
END  

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[HANDPAYS]') and name = 'hp_amt1')
BEGIN
    ALTER TABLE HANDPAYS ADD hp_amt1 money NULL  
END  

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[HANDPAYS]') and name = 'hp_cur1')
BEGIN
    ALTER TABLE HANDPAYS ADD hp_cur1 [nvarchar](3) NULL  
END
GO

IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'AFIPClient' AND GP_SUBJECT_KEY = 'ShowExtendedMeterType')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('AFIPClient', 'ShowExtendedMeterType', '511')
GO  


-- PROGRESSIVES TABLES

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[progressives]') AND type in (N'U'))
BEGIN

  CREATE TABLE [dbo].[progressives](
    [pgs_site_id] [bigint] NOT NULL,
    [pgs_progressive_id] [bigint] NOT NULL,
    [pgs_name] [nvarchar](50) NOT NULL,
    [pgs_created] [datetime] NOT NULL,
    [pgs_contribution_pct] [numeric](7, 4) NOT NULL,
    [pgs_num_levels] [int] NOT NULL,
    [pgs_amount] [money] NOT NULL,
    [pgs_terminal_list] [xml] NOT NULL,
    [pgs_status] [int] NOT NULL,
    [pgs_last_provisioned_hour] [datetime] NULL,
    [pgs_status_changed] [datetime] NOT NULL,
   CONSTRAINT [PK_progressives] PRIMARY KEY CLUSTERED 
  (
      [pgs_site_id] ASC ,
    [pgs_progressive_id] ASC
  )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
  ) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

END

GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[progressives]') and name = 'pgs_brand')
BEGIN
    ALTER TABLE progressives ADD pgs_brand varchar(50) NULL
END
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[progressives]') and name = 'pgs_server')
BEGIN
    ALTER TABLE progressives ADD pgs_server varchar(50) NULL
END

GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[progressives]') and name = 'pgs_multiseat')
BEGIN
    ALTER TABLE progressives ADD pgs_multiseat bit DEFAULT 0
END

GO

/****** Object:  Table [dbo].[progressives_provisions]    Script Date: 30/11/2017 11:46:33 ******/
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[progressives_provisions]') AND type in (N'U'))
BEGIN

  CREATE TABLE [dbo].[progressives_provisions](
    [pgp_site_id] [bigint] NOT NULL,
    [pgp_provision_id] [bigint] NOT NULL,
    [pgp_progressive_id] [bigint] NOT NULL,
    [pgp_created] [datetime] NOT NULL,
    [pgp_hour_from] [datetime] NOT NULL,
    [pgp_hour_to] [datetime] NOT NULL,
    [pgp_amount] [money] NOT NULL,
    [pgp_theoretical_amount] [money] NOT NULL,
    [pgp_cage_session_id] [bigint] NULL,
    [pgp_cage_amount] [money] NULL,
    [pgp_status] [int] NOT NULL,
    [pgp_gui_user_id] [int] NULL,
    [pgp_current_amount] [money] NULL,
   CONSTRAINT [PK_progressives_provisions] PRIMARY KEY CLUSTERED 
  (
    [pgp_site_id] ASC ,
    [pgp_provision_id] ASC
  )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
  ) ON [PRIMARY]

END

GO

/****** Object:  Table [dbo].[progressives_provisions_terminals]    Script Date: 30/11/2017 11:50:36 ******/
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[progressives_provisions_terminals]') AND type in (N'U'))
BEGIN

  CREATE TABLE [dbo].[progressives_provisions_terminals](
    [ppt_site_id] [bigint] NOT NULL,
    [ppt_provision_id] [bigint] NOT NULL,
    [ppt_terminal_id] [int] NOT NULL,
    [ppt_progressive_id] [bigint] NOT NULL,
    [ppt_amount] [money] NOT NULL,
    [ppt_diff_amount] [money] NULL,
   CONSTRAINT [PK_progressives_provisions_terminals] PRIMARY KEY CLUSTERED 
  (
    [ppt_site_id] ASC,
    [ppt_provision_id] ASC,
    [ppt_terminal_id] ASC
  )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
  ) ON [PRIMARY]

END

GO

/****** Object:  Table [dbo].[progressives_levels]    Script Date: 30/11/2017 11:52:43 ******/
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[progressives_levels]') AND type in (N'U'))
BEGIN

  CREATE TABLE [dbo].[progressives_levels](
    [pgl_site_id] [bigint] NOT NULL,
    [pgl_progressive_id] [bigint] NOT NULL,
    [pgl_level_id] [int] NOT NULL,
    [pgl_name] [nvarchar](50) NULL,
    [pgl_contribution_pct] [numeric](5, 2) NOT NULL,
    [pgl_amount] [money] NOT NULL,
    [pgl_show_on_winup] [bit] NULL,
   CONSTRAINT [PK_progressives_levels] PRIMARY KEY CLUSTERED 
  (
    [pgl_site_id] ASC,
    [pgl_progressive_id] ASC,
    [pgl_level_id] ASC
  )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
  ) ON [PRIMARY]

END

GO

/****** Object:  Table [dbo].[progressives_provisions_levels]    Script Date: 30/11/2017 11:51:50 ******/
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[progressives_provisions_levels]') AND type in (N'U'))
BEGIN

  CREATE TABLE [dbo].[progressives_provisions_levels](
    [ppl_site_id] [bigint] NOT NULL,
    [ppl_provision_id] [bigint] NOT NULL,
    [ppl_level_id] [int] NOT NULL,
    [ppl_amount] [money] NOT NULL,
    [ppl_theoretical_amount] [money] NOT NULL,
    [ppl_current_amount] [money] NULL,
   CONSTRAINT [PK_progressives_provisions_levels] PRIMARY KEY CLUSTERED 
  (
    [ppl_site_id] ASC,
    [ppl_provision_id] ASC,
    [ppl_level_id] ASC
  )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
  ) ON [PRIMARY]

END

GO

/*Script disabled because of WIGOS-12975 - Multisite - Terminals Connected task not working properly - WWP site log error*/
/*
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = N'TERMINALS_CONNECTED' and column_name = 'TC_TIMESTAMP')
BEGIN
    ALTER TABLE TERMINALS_CONNECTED ADD tc_timestamp TIMESTAMP NULL
END
ELSE
    DECLARE @IsNullabe NVARCHAR(10)

    SELECT @IsNullabe = IS_NULLABLE FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = N'TERMINALS_CONNECTED' and column_name = 'TC_TIMESTAMP'

    IF @IsNullabe = 'NO'
    BEGIN
                   ALTER TABLE TERMINALS_CONNECTED DROP COLUMN tc_timestamp
                   ALTER TABLE TERMINALS_CONNECTED ADD tc_timestamp TIMESTAMP NULL
    END
GO*/

------------------------------------------------------------------------------------
-- Drop and create table EGM_CONTROL_MARK 
------------------------------------------------------------------------------------
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[egm_control_mark]') AND TYPE IN (N'U'))
    DROP TABLE [dbo].[egm_control_mark]
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[egm_control_mark]') AND TYPE IN (N'U'))
  CREATE TABLE [dbo].[egm_control_mark](
      [ecm_site_id] INT NOT NULL, 
      [ecm_control_mark] [DATETIME] NOT NULL
  CONSTRAINT [PK_egm_control_mark] PRIMARY KEY CLUSTERED 
    (
    [ecm_site_id]   ASC
    ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
  ) ON [PRIMARY]
GO 

-------------------------------------------
-- Drop and create table EGM_DAILY
-------------------------------------------
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[egm_daily]') AND TYPE IN (N'U'))  
  CREATE TABLE [dbo].[egm_daily](
    [ed_working_day]                 [INT]      NOT NULL,
    [ed_site_id]                     [INT]      NOT NULL,
    [ed_status]                      [TINYINT]  NOT NULL DEFAULT 0,
    [ed_has_new_meters]              [BIT]      NOT NULL DEFAULT 0,
    [ed_terminals_connected]         [INT]      NOT NULL,
    [ed_terminals_number]            [INT]      NOT NULL,  
    [ed_last_updated_meters]         [DATETIME] NULL,
    [ed_last_updated_user]           [INT]      NULL,
    [ed_last_updated_user_datetime]  [DATETIME] NULL,
    [ed_is_calculated]               [BIT]      NOT NULL DEFAULT 0
    CONSTRAINT [PK_egm_daily] PRIMARY KEY CLUSTERED 
    (
    [ed_working_day]   ASC,
    [ed_site_id]     ASC

    ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
  ) ON [PRIMARY]
GO

/* Index: 'IX_ed_workingday_site' for table 'egm_daily'. */
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[egm_daily]') AND name = N'IX_ed_workingday_site')
 CREATE UNIQUE NONCLUSTERED INDEX [IX_ed_workingday_site] ON [dbo].[egm_daily] 
 (
   [ed_working_day] ASC,
   [ed_site_id] ASC
 )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
 GO

-------------------------------------------
-- Drop and create table EGM_METERS_BY_DAY
-------------------------------------------
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[egm_meters_by_day]') AND TYPE IN (N'U'))
 CREATE TABLE [dbo].[egm_meters_by_day](
   [emd_working_day]             [INT]           NOT NULL,
   [emd_site_id]                 [INT]           NOT NULL,
   [emd_terminal_id]             [INT]           NOT NULL,
   [emd_game_id]                 [INT]           NOT NULL,  
   [emd_warning]                 [INT]           NULL DEFAULT 0,
   [emd_user_period_modified]    [BIT]           NULL DEFAULT 0,
   [emd_user_validated]          [BIT]           NULL DEFAULT 0,
   [emd_user_validated_datetime] [DATETIME]      NULL,
   [emd_last_updated]            [DATETIME]      NULL,
   [emd_last_updated_user_id]    [INT]           NULL,  
   [emd_connected]               [BIT]           NOT NULL,
   [emd_status]                  [INT]           NOT NULL,
   [emd_mc_0000_increment]       [DECIMAL](20,2) NOT NULL,
   [emd_mc_0001_increment]       [DECIMAL](20,2) NOT NULL,
   [emd_mc_0002_increment]       [DECIMAL](20,2) NOT NULL,
   [emd_mc_0005_increment]       [BIGINT]        NOT NULL,
   [emd_mc_0000]                 [BIGINT]        NOT NULL,
   [emd_mc_0001]                 [BIGINT]        NOT NULL,
   [emd_mc_0002]                 [BIGINT]        NOT NULL,
   [emd_mc_0005]                 [BIGINT]        NOT NULL,
   CONSTRAINT [PK_egm_meters_by_day] PRIMARY KEY CLUSTERED 
   (
   [emd_working_day]   ASC,
   [emd_site_id]     ASC,
   [emd_terminal_id]   ASC

   ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
 ) ON [PRIMARY]
GO

/* Index: 'IX_emd_workingday_site_terminal' for table 'egm_meters_by_day'. */
IF NOT  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[egm_meters_by_day]') AND name = N'IX_emd_workingday_site_terminal')
 CREATE UNIQUE NONCLUSTERED INDEX [IX_emd_workingday_site_terminal] ON [dbo].[egm_meters_by_day] 
 (
   [emd_working_day] ASC,
   [emd_site_id] ASC,
   [emd_terminal_id] ASC
 )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
-------------------------------------------
-- Drop and create table EGM_METERS_BY_PERIOD
-------------------------------------------
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[egm_meters_by_period]') AND TYPE IN (N'U'))
 CREATE TABLE [dbo].[egm_meters_by_period](
   [emp_working_day]                 [INT]      NOT NULL,
   [emp_site_id]                     [INT]      NOT NULL,
   [emp_terminal_id]                 [INT]      NOT NULL,
   [emp_game_id]                     [INT]      NOT NULL,
   [emp_denomination]                [MONEY]    NOT NULL,
   [emp_datetime]                    [DATETIME] NOT NULL,
   [emp_record_type]                 [INT]      NOT NULL,
   [emp_sas_accounting_denom]        [MONEY]    NULL,
   [emp_last_created_datetime]       [DATETIME] NOT NULL,
   [emp_last_reported_datetime]      [DATETIME] NOT NULL,
   [emp_user_ignored]                [BIT]      NOT NULL,
   [emp_user_ignored_datetime]       [DATETIME] NULL,
   [emp_last_updated_user_id]        [INT]      NULL,
   [emp_last_updated_user_datetime]  [DATETIME] NULL,
   [emp_status]                      [INT]      NOT NULL,  
   [emp_mc_0000_increment]           [DECIMAL](20, 2) NOT NULL,
   [emp_mc_0001_increment]           [DECIMAL](20, 2) NOT NULL,
   [emp_mc_0002_increment]           [DECIMAL](20, 2) NOT NULL,
   [emp_mc_0005_increment]           [BIGINT]   NOT NULL,
   [emp_mc_0000]                     [BIGINT]   NOT NULL,
   [emp_mc_0001]                     [BIGINT]   NOT NULL,
   [emp_mc_0002]                     [BIGINT]   NOT NULL,
   [emp_mc_0005]                     [BIGINT]   NOT NULL,
   CONSTRAINT [PK_egm_meters_by_period] PRIMARY KEY CLUSTERED 
   (
   [emp_working_day]   ASC,
   [emp_site_id]     ASC,
   [emp_terminal_id]   ASC,
   [emp_game_id]     ASC,
   [emp_denomination]   ASC,
   [emp_datetime]     ASC
   ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
 ) ON [PRIMARY]
GO

 /* Index: 'IX_emp_workingday_site_terminal_datetime' for table 'egm_meters_by_period'. */
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[egm_meters_by_period]') AND name = N'IX_emp_workingday_site_terminal_datetime')
 CREATE NONCLUSTERED INDEX [IX_emp_workingday_site_terminal_datetime] ON [dbo].[egm_meters_by_period] 
 (
   [emp_working_day] ASC,
   [emp_site_id] ASC,
   [emp_terminal_id] ASC,
   [emp_datetime] ASC
 )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

------------------------------------------
-- Drop and create table ErrorHandling
-------------------------------------------

IF NOT EXISTS (SELECT * FROM sys.objects WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[ErrorHandling]') AND TYPE IN (N'U'))
 CREATE TABLE [dbo].[ErrorHandling](
   [pkErrorHandlingID] [INT]           IDENTITY(1,1) NOT NULL,
   [Error_Number]      [INT]                         NOT NULL,
   [Error_Message]     [VARCHAR](4000)               NULL,
   [Error_Severity]    [SMALLINT]                    NOT NULL,
   [Error_State]       [SMALLINT]                    NOT NULL  DEFAULT 1,
   [Error_Procedure]   [VARCHAR](200)                NOT NULL,
   [Error_Line]        [INT]                         NOT NULL  DEFAULT 0,
   [UserName]          [VARCHAR](128)                NOT NULL  DEFAULT '',
   [HostName]          [VARCHAR](128)                NOT NULL  DEFAULT '',
   [Time_Stamp]        [DATETIME]                    NOT NULL,
 PRIMARY KEY CLUSTERED 
 (
   [pkErrorHandlingID] ASC
 )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
 ) ON [PRIMARY]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[catalog_items]') AND type in (N'U'))
DROP TABLE [dbo].[catalog_items]
GO

CREATE TABLE [dbo].[catalog_items](
  [cai_site_id]     [int]           NOT NULL,
  [cai_id]          [bigint]        NOT NULL,
  [cai_catalog_id]  [bigint]        NOT NULL,
  [cai_name]        [nvarchar](50)  NULL,
  [cai_description] [nvarchar](250) NULL,
  [cai_enabled]     [bit]           NOT NULL,
 CONSTRAINT [PK_catalog_items] PRIMARY KEY CLUSTERED 
(
  [cai_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

-------------------------------------------------
-- TERMINAL SAS METERS
-------------------------------------------------

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[terminal_sas_meters]') AND type in (N'U'))
BEGIN
  CREATE TABLE [dbo].[terminal_sas_meters](
    [tsm_site_id] [bigint] NOT NULL,
    [tsm_terminal_id] [int] NOT NULL,
    [tsm_meter_code] [int] NOT NULL,
    [tsm_game_id] [int] NOT NULL,
    [tsm_denomination] [money] NOT NULL,
    [tsm_wcp_sequence_id] [bigint] NOT NULL,
    [tsm_last_reported] [datetime] NOT NULL,
    [tsm_last_modified] [datetime] NULL,
    [tsm_meter_value] [bigint] NOT NULL,
    [tsm_meter_max_value] [bigint] NOT NULL,
    [tsm_delta_value] [bigint] NOT NULL,
    [tsm_raw_delta_value] [bigint] NOT NULL,
    [tsm_delta_updating] [bit] NOT NULL,
    [tsm_sas_accounting_denom] [money] NULL,
    [tsm_timestamp] [timestamp] NOT NULL,
   CONSTRAINT [PK_terminal_sas_meters] PRIMARY KEY CLUSTERED 
  (
      [tsm_site_id] ASC,
    [tsm_terminal_id] ASC,
    [tsm_meter_code] ASC,
    [tsm_game_id] ASC,
    [tsm_denomination] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
  ) ON [PRIMARY]
END
GO

-------------------------------------------------
-- TERMINAL SAS METERS HISTORY
-------------------------------------------------

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[terminal_sas_meters_history]') AND type in (N'U'))
BEGIN
  CREATE TABLE [dbo].[terminal_sas_meters_history](
    [tsmh_site_id] [bigint] NOT NULL,
    [tsmh_terminal_id] [int] NOT NULL,
    [tsmh_meter_code] [int] NOT NULL,
    [tsmh_game_id] [int] NOT NULL,
    [tsmh_denomination] [money] NOT NULL,
    [tsmh_type] [int] NOT NULL,
    [tsmh_datetime] [datetime] NOT NULL,
    [tsmh_meter_ini_value] [bigint] NULL,
    [tsmh_meter_fin_value] [bigint] NOT NULL,
    [tsmh_meter_increment] [bigint] NOT NULL,
    [tsmh_raw_meter_increment] [bigint] NOT NULL,
    [tsmh_last_reported] [datetime] NULL,
    [tsmh_sas_accounting_denom] [money] NULL,
    [tsmh_created_datetime] [datetime] NULL,
    [tsmh_group_id] [bigint] NULL,
    [tsmh_meter_origin] [int] NULL,
    [tsmh_meter_max_value] [bigint] NULL,
    [tsmh_timestamp] [timestamp] NOT NULL,
   CONSTRAINT [PK_terminal_sas_meters_history] PRIMARY KEY CLUSTERED 
  (
      [tsmh_site_id] ASC,
    [tsmh_terminal_id] ASC,
    [tsmh_meter_code] ASC,
    [tsmh_game_id] ASC,
    [tsmh_denomination] ASC,
    [tsmh_type] ASC,
    [tsmh_datetime] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
  ) ON [PRIMARY]
END
GO


-------------------------------------------------
-- TERMINAL_STATUS
-------------------------------------------------

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[terminal_status]') AND type in (N'U'))
BEGIN

    CREATE TABLE [dbo].[terminal_status](
        [ts_site_id] [bigint] NOT NULL,
        [ts_terminal_id] [int] NOT NULL,
        [ts_sas_host_error] [bit] NOT NULL  DEFAULT 0,
        [ts_current_payout] [money] NULL,
        [ts_stacker_counter] [int] NOT NULL  DEFAULT 0,
        [ts_stacker_status] [int] NOT NULL  DEFAULT 0,
        [ts_egm_flags] [int] NOT NULL  DEFAULT 0,
        [ts_door_flags] [int] NOT NULL  DEFAULT 0,
        [ts_bill_flags] [int] NOT NULL  DEFAULT 0,
        [ts_printer_flags] [int] NOT NULL  DEFAULT 0,
        [ts_played_won_flags] [int] NOT NULL  DEFAULT 0,
        [ts_jackpot_flags] [int] NOT NULL  DEFAULT 0,
        [ts_call_attendant_flags] [bit] NOT NULL  DEFAULT 0,
        [ts_machine_flags] [int] NOT NULL  DEFAULT 0,
        [ts_played_alarm_id] [bigint] NULL,
        [ts_won_alarm_id] [bigint] NULL,
        [ts_jackpot_alarm_id] [bigint] NULL,
        [ts_canceled_credit_alarm_id] [bigint] NULL,
        [ts_counterfeit_alarm_id] [bigint] NULL,
        [ts_without_plays_alarm_id] [bigint] NULL,
        [ts_current_payout_alarm_id] [bigint] NULL,
        [ts_coin_flags] [int] NOT NULL  DEFAULT 0,
        [ts_highroller_alarm_id] [bigint] NULL,
        [ts_highroller_anonymous_alarm_id] [bigint] NULL,
        [ts_is_reserved] [bit] NULL,
        [ts_ps_busy] [bit] NULL,
    CONSTRAINT [PK_terminal_status] PRIMARY KEY CLUSTERED 
    (
        [ts_site_id] ASC,
        [ts_terminal_id] ASC
    )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
    ) ON [PRIMARY]
  
END
GO


-------------------------------------------------
-- WCP SESSIONS
-------------------------------------------------

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wcp_sessions]') AND type in (N'U'))
BEGIN

    CREATE TABLE [dbo].[wcp_sessions](
        [ws_site_id] [int] NOT NULL,
        [ws_session_id] [bigint] NOT NULL,
        [ws_terminal_id] [int] NOT NULL,
        [ws_last_sequence_id] [bigint] NOT NULL  DEFAULT 0,
        [ws_last_transaction_id] [bigint] NOT NULL  DEFAULT 0,
        [ws_last_rcvd_msg] [datetime] NULL,
        [ws_started] [datetime] NOT NULL  DEFAULT (GETDATE()),
        [ws_finished] [datetime] NULL,
        [ws_status] [int] NOT NULL  DEFAULT 0,
        [ws_timestamp] [bigint] NULL,
        [ws_server_name] [nvarchar](50) NULL,
    CONSTRAINT [PK_wcp_sessions] PRIMARY KEY CLUSTERED 
    (
        [ws_site_id] ASC,
        [ws_session_id] ASC
    )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
    ) ON [PRIMARY]

END

GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wcp_sessions]') AND type in (N'U'))
BEGIN
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0 - Opened, 1 - Closed, 2 - Abandoned, 3 - Timeout' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'wcp_sessions', @level2type=N'COLUMN',@level2name=N'ws_status'
END
GO

-------------------------------------------------
-- WC2 SESSIONS
-------------------------------------------------

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wc2_sessions]') AND type in (N'U'))
BEGIN

    CREATE TABLE [dbo].[wc2_sessions](
        [w2s_site_id] [int] NOT NULL,
        [w2s_session_id] [bigint] NOT NULL,
        [w2s_terminal_id] [int] NOT NULL,
        [w2s_last_sequence_id] [bigint] NOT NULL  DEFAULT 0,
        [w2s_last_transaction_id] [bigint] NOT NULL  DEFAULT 0,
        [w2s_last_rcvd_msg] [datetime] NULL,
        [w2s_started] [datetime] NOT NULL  DEFAULT (GETDATE()),
        [w2s_finished] [datetime] NULL,
        [w2s_status] [int] NOT NULL  DEFAULT 0,
        [w2s_timestamp] [bigint] NULL,
        [w2s_server_name] [nvarchar](50) NULL,
    CONSTRAINT [PK_wc2_sessions] PRIMARY KEY CLUSTERED 
    (
        [w2s_site_id] ASC,
        [w2s_session_id] ASC
    )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
    ) ON [PRIMARY]

END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wc2_sessions]') AND type in (N'U'))
BEGIN
  EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0 - Opened, 1 - Closed, 2 - Abandoned, 3 - Timeout' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'wc2_sessions', @level2type=N'COLUMN',@level2name=N'w2s_status'
END
GO



-------------------------------------------------
-- TERMINALS
-------------------------------------------------

-- ADD COLUMNS

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[TERMINALS]') and name = 'TE_NUMBER_LINES')
BEGIN
    ALTER TABLE TERMINALS ADD TE_NUMBER_LINES nvarchar(50) NULL
END
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[TERMINALS]') and name = 'TE_CONTRACT_ID')
BEGIN
    ALTER TABLE TERMINALS ADD TE_CONTRACT_ID nvarchar(50) NULL
END
GO

-------------------------------------------------
-- HANDPAYS
-------------------------------------------------
-- ADD COLUMNS
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[HANDPAYS]') and name = 'HP_ACCOUNT_ID')
BEGIN
    ALTER TABLE HANDPAYS ADD hp_account_id bigint NULL
END
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[HANDPAYS]') and name = 'hp_payment_mode')
BEGIN
  ALTER TABLE HANDPAYS ADD hp_payment_mode int NULL
END
GO
  
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[HANDPAYS]') and name = 'hp_status_calculated')
BEGIN  
  ALTER TABLE HANDPAYS ADD hp_status_calculated AS ([hp_status]&0xF000)
END  
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[HANDPAYS]') and name = 'hp_operation_id')
BEGIN  
    ALTER TABLE HANDPAYS ADD hp_operation_id bigint NULL  
END  
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[HANDPAYS]') and name = 'hp_amt0')
BEGIN
    ALTER TABLE HANDPAYS ADD hp_amt0 money NULL  
END  
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[HANDPAYS]') and name = 'hp_cur0')
BEGIN
    ALTER TABLE HANDPAYS ADD hp_cur0 [nvarchar](3) NULL  
END  
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[HANDPAYS]') and name = 'hp_amt1')
BEGIN
    ALTER TABLE HANDPAYS ADD hp_amt1 money NULL  
END  
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[HANDPAYS]') and name = 'hp_cur1')
BEGIN
    ALTER TABLE HANDPAYS ADD hp_cur1 [nvarchar](3) NULL  
END
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[catalogs]') AND type in (N'U'))
  DROP TABLE [dbo].[catalogs]
GO

CREATE TABLE [dbo].[catalogs](
     [cat_site_id]     [int]           NOT NULL
    ,[cat_id]          [bigint]        NOT NULL
    ,[cat_type]        [smallint]      NOT NULL
    ,[cat_name]        [nvarchar](50)  NULL
    ,[cat_description] [nvarchar](250) NULL
    ,[cat_enabled]     [bit]           NOT NULL
    ,[cat_system_type] [int]           NULL  DEFAULT 0
  CONSTRAINT [PK_catalogs] PRIMARY KEY CLUSTERED (
     [cat_id] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

--MultiseatId appoint to a catalog
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'terminals' AND COLUMN_NAME = 'te_multiseat_id')
BEGIN
    ALTER TABLE [dbo].[terminals] ADD     te_multiseat_id      INT       NULL
END
GO

-------------------------------------------
-- Drop and create table EGM_METERS_MAX_VALUES
-------------------------------------------
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[egm_meters_max_values]') AND TYPE IN (N'U'))
  CREATE TABLE [dbo].[egm_meters_max_values](
      [emmv_site_id]      [BIGINT]    NOT NULL
    , [emmv_terminal_id]  [INT]       NOT NULL
    , [emmv_meter_code]   [INT]       NOT NULL
    , [emmv_game_id]      [INT]       NOT NULL
    , [emmv_denomination] [MONEY]     NOT NULL
    , [emmv_type]         [INT]       NOT NULL
    , [emmv_datetime]     [DATETIME]  NOT NULL
    , [emmv_max_value]    [BIGINT]    NULL
     CONSTRAINT [PK_egm_meters_max_values] PRIMARY KEY CLUSTERED 
    (
        [emmv_site_id]  
      ,  [emmv_terminal_id]
      ,  [emmv_meter_code]
      ,  [emmv_game_id]  
      ,  [emmv_denomination]
      ,  [emmv_type]    
      ,  [emmv_datetime] 
    )
    WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
  ) ON [PRIMARY]

  GO


/******* RECORDS *******/

------------------------------------------------------------------------------------
-- Add default values to EGM_CONTROL_MARK
------------------------------------------------------------------------------------
DECLARE @is_multisite   BIT;

SELECT   @is_multisite  = GP_KEY_VALUE
  FROM   GENERAL_PARAMS
 WHERE   GP_GROUP_KEY   = 'MultiSite'
   AND   GP_SUBJECT_KEY = 'IsCenter'

DECLARE   @SITES_AUX TABLE(
                     SITE_ID           BIGINT  NULL
                   )
IF (@is_multisite=1)
    INSERT INTO @SITES_AUX
    SELECT ST_SITE_ID FROM SITES
ELSE
    INSERT INTO @SITES_AUX
    SELECT gp_key_value FROM general_params WHERE gp_group_key = 'Site' AND gp_subject_key = 'Identifier'

/* Add default value as previous working day. */
IF ((SELECT COUNT(ecm_control_mark) FROM egm_control_mark)=0)
    DECLARE @SiteId AS INT
    DECLARE CurSite CURSOR FOR SELECT SITE_ID FROM @SITES_AUX
    OPEN CurSite
    FETCH NEXT FROM CurSite INTO @SiteId
    WHILE @@fetch_status = 0
    BEGIN
      INSERT INTO egm_control_mark (ecm_site_id, ecm_control_mark) VALUES(@SiteId, DATEADD(DAY, -1, dbo.Opening(0,GETDATE())))
      FETCH NEXT FROM CurSite INTO @SiteId
    END
    CLOSE CurSite
    DEALLOCATE CurSite
GO


/******* PROCEDURES *******/

/* ConcatOpeningTime */
IF  EXISTS (SELECT * FROM sys.objects WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[ConcatOpeningTime]') AND TYPE IN (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[ConcatOpeningTime]
GO

CREATE FUNCTION [dbo].[ConcatOpeningTime]
  (@SiteId int,
   @Date   datetime)
RETURNS datetime
AS
BEGIN
  DECLARE @closing_hour    int
  DECLARE @closing_minutes int
  DECLARE @today_opening  datetime

  SET @closing_hour = ISNULL((SELECT   gp_key_value
                                FROM   general_params
                               WHERE   gp_group_key   = 'WigosGUI'
                                 AND   gp_subject_key = 'ClosingTime'), 6)

  SET @closing_minutes = ISNULL((SELECT   gp_key_value
                                   FROM   general_params
                                  WHERE   gp_group_key   = 'WigosGUI'
                                    AND   gp_subject_key = 'ClosingTimeMinutes'), 0)

  -- Trunc date to start of the day (00:00h).
  SET @today_opening = DATEADD(DAY, DATEDIFF(day, 0, @Date), 0)
  
  SET @today_opening = DATEADD(HOUR, @closing_hour, @today_opening)
  SET @today_opening = DATEADD(MINUTE, @closing_minutes, @today_opening)

  RETURN @today_opening
END -- ConcatOpeningTime
GO

GRANT EXECUTE ON [dbo].[ConcatOpeningTime] TO [wggui]
GO

/* Opening */
IF  EXISTS (SELECT * FROM sys.objects WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[Opening]') AND TYPE IN (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[Opening]
GO

CREATE FUNCTION [dbo].[Opening]
  (@SiteId int,
   @Date   datetime)
RETURNS datetime
AS
BEGIN
  DECLARE @now           datetime
  DECLARE @today_opening datetime

  SET @now = @Date
  SET @today_opening = dbo.ConcatOpeningTime(@SiteId, @now)

  IF @today_opening > @now
  BEGIN
    SET @today_opening = DATEADD(DAY, -1, @today_opening)
  END

  RETURN @today_opening
END -- Opening
GO

GRANT EXECUTE ON [dbo].[Opening] TO [wggui]
GO

/* GamingDayFromDateTime */
IF  EXISTS (SELECT * FROM sys.objects WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[GamingDayFromDateTime]') AND TYPE IN (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[GamingDayFromDateTime]
GO

CREATE FUNCTION [dbo].[GamingDayFromDateTime] 
(
      @DateTime DATETIME
)
RETURNS int
AS
BEGIN
      DECLARE @_open               AS DATETIME
      DECLARE @_gaming_day    AS INT
      
      SET @_open       = dbo.Opening (0, @DateTime)
      SET @_gaming_day = YEAR (@_open) * 10000 + MONTH(@_open) * 100 + DAY(@_open)

      RETURN @_gaming_day

END -- GamingDayFromDateTime
GO

GRANT EXECUTE ON [dbo].[GamingDayFromDateTime] TO [wggui]
GO

/* Function: IntToBin*/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[IntToBin]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[IntToBin]
GO

CREATE FUNCTION [dbo].[IntToBin]
(
  @value INT
)
  RETURNS VARCHAR(1000)
AS
  BEGIN
  DECLARE @result VARCHAR(1000) = '';
  DECLARE @fixedSize INT = '8';
   WHILE (@value != 0)
   BEGIN
    IF(@value%2 = 0) 
     SET @Result = '0' + @Result;
    ELSE
     SET @Result = '1' + @Result;
     
    SET @value = @value / 2;
   END;
   IF(@FixedSize > 0 AND LEN(@Result) < @FixedSize)
    SET @result = RIGHT('00000000000000000000' + @Result, @FixedSize);
  RETURN @Result;
  END
GO

GRANT EXECUTE ON [dbo].[IntToBin] TO [wggui]
GO

/* Function: BinToInt*/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[BinToInt]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[BinToInt]
GO

CREATE FUNCTION [dbo].[BinToInt]
(
  @Input varchar(255)
)
RETURNS bigint
AS
BEGIN

  DECLARE @Cnt tinyint = 1
  DECLARE @Len tinyint = LEN(@Input)
  DECLARE @Output bigint = CAST(SUBSTRING(@Input, @Len, 1) AS bigint)

  WHILE(@Cnt < @Len) BEGIN
    SET @Output = @Output + POWER(CAST(SUBSTRING(@Input, @Len - @Cnt, 1) * 2 AS bigint), @Cnt)

    SET @Cnt = @Cnt + 1
  END
  RETURN @Output
END
GO

GRANT EXECUTE ON [dbo].[BinToInt] TO [wggui]
GO

/* CompareTwoAnomaliesMasks */
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CompareTwoAnomaliesMasks]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[CompareTwoAnomaliesMasks]
GO

CREATE FUNCTION [dbo].[CompareTwoAnomaliesMasks]
(
  @anomalia_1 INT,
  @anomalia_2 INT
)
RETURNS INT
AS
BEGIN
  DECLARE @index INT = 1;
  DECLARE @first VARCHAR(8);
  DECLARE @second VARCHAR(8);
  DECLARE @return_string VARCHAR(8);
  
  SET @return_string = '';  
  SET @first = CONVERT(VARCHAR(8),(SELECT dbo.IntToBin(@anomalia_1)));
  SET @second = CONVERT(VARCHAR(8),(SELECT dbo.IntToBin(@anomalia_2)));

  WHILE (@index <= LEN(@first))
    BEGIN 
      IF (SUBSTRING(@first, @index,1)=1 AND SUBSTRING(@second, @index,1)=1)
          SET @return_string += '1';
      IF (SUBSTRING(@first, @index,1)=0 AND SUBSTRING(@second, @index,1)=0)
          SET @return_string += '0';
      IF  ((SUBSTRING(@first, @index,1)=0 AND SUBSTRING(@second, @index,1)=1) OR (SUBSTRING(@first, @index,1)=1 AND SUBSTRING(@second, @index,1)=0))
          SET @return_string += '1';
      SET @index += 1;
    END
  RETURN dbo.BinToInt(@return_string)
END
GO

GRANT EXECUTE ON [dbo].[CompareTwoAnomaliesMasks] TO [wggui]
GO


IF EXISTS (SELECT   * 
             FROM   sys.objects 
            WHERE   object_id = OBJECT_ID(N'[dbo].[MaxIncrementValue]') 
              AND   type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
  DROP FUNCTION [dbo].[MaxIncrementValue]
GO
CREATE FUNCTION [dbo].[MaxIncrementValue]
(
  @denomination as Decimal
)
RETURNS BigInt
AS
BEGIN
  
  DECLARE @defaultValue BigInt
  DECLARE @Result Nvarchar(20)
  
  SET @defaultValue = 500000
  
  DECLARE @tag nvarchar(200)
  
  SELECT @tag  = gp_key_value
  FROM general_params
  WHERE gp_subject_key = 'MaxValuesOfBigIncrement'


  SET @Result = ( SELECT tt.MaxValue
            FROM (SELECT LEFT(SST_VALUE,CHARINDEX(':',SST_VALUE)-1) as Denomination,
                    LTRIM(RIGHT(SST_VALUE,LEN(SST_VALUE) - CHARINDEX(':',SST_VALUE) )) AS MaxValue
                   FROM dbo.SplitStringIntoTable(@tag,';',1)) AS tt
           WHERE tt.Denomination  = CAST (0.01 AS NVARCHAR)
           )
           
           
  --print cast (@Result as bigint)

  RETURN CAST(ISNULL(@Result,@defaultValue) AS BIGINT)
END
GO

GRANT EXECUTE ON [dbo].[MaxIncrementValue] TO [wggui]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spErrorHandling]') AND type in (N'P'))
 DROP PROCEDURE [dbo].[spErrorHandling]
GO

CREATE PROCEDURE [dbo].[spErrorHandling] 
AS 
BEGIN
  -- Declaration statements
  DECLARE @Error_Number     INT
  DECLARE @Error_Message     VARCHAR(4000)
  DECLARE @Error_Severity   INT
  DECLARE @Error_State     INT
  DECLARE @Error_Procedure   VARCHAR(200)
  DECLARE @Error_Line     INT
  DECLARE @UserName       VARCHAR(200)
  DECLARE @HostName       VARCHAR(200)
  DECLARE @Time_Stamp     DATETIME

-- Initialize variables
SELECT
  @Error_Number = ISNULL(ERROR_NUMBER(), 0)
   ,@Error_Message = ISNULL(ERROR_MESSAGE(), 'NULL Message')
   ,@Error_Severity = ISNULL(ERROR_SEVERITY(), 0)
   ,@Error_State = ISNULL(ERROR_STATE(), 1)
   ,@Error_Line = ISNULL(ERROR_LINE(), 0)
   ,@Error_Procedure = ISNULL(ERROR_PROCEDURE(), '')
   ,@UserName = SUSER_SNAME()
   ,@HostName = HOST_NAME()
   ,@Time_Stamp = GETDATE();

-- Insert into the dbo.ErrorHandling table (protecting if table is in the database to avoid errors).
IF EXISTS (SELECT
      *
    FROM INFORMATION_SCHEMA.TABLES
    WHERE TABLE_TYPE = 'BASE TABLE'
    AND TABLE_NAME = 'ErrorHandling')
INSERT INTO dbo.ErrorHandling (Error_Number, Error_Message, Error_Severity, Error_State, Error_Line, Error_Procedure, UserName, HostName, Time_Stamp)
  SELECT
    @Error_Number
     ,@Error_Message
     ,@Error_Severity
     ,@Error_State
     ,@Error_Line
     ,@Error_Procedure
     ,@UserName
     ,@HostName
     ,@Time_Stamp
END 
GO

/* 
**********************************************************
sp_EGM_Meters_Get_TSMH_Period_Data
**********************************************************
*/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EGM_Meters_Get_TSMH_Period_Data]') AND type in (N'P', N'PC'))
 DROP PROCEDURE [dbo].[sp_EGM_Meters_Get_TSMH_Period_Data]
 GO

CREATE PROCEDURE [dbo].[sp_EGM_Meters_Get_TSMH_Period_Data]
  @param_site_id INT,
  @param_datetime DATETIME = NULL
AS
BEGIN
SET NOCOUNT ON;
    -- Insert statements for procedure here
    DECLARE @query          NVARCHAR(MAX);
  DECLARE @site_id        INT;
  DECLARE @sas_meters_interval  INT;
    DECLARE @is_multisite      BIT;

SET @is_multisite = (SELECT
    gp_key_value
  FROM general_params
  WHERE gp_group_key = 'MultiSite'
  AND gp_subject_key = 'IsCenter');
SET @site_id = (SELECT
    gp_key_value
  FROM general_params
  WHERE gp_group_key = 'Site'
  AND gp_subject_key = 'Identifier');

EXEC  sp_EGM_Meters_Get_Interval @param_site_id = @param_site_id, @param_interval = @sas_meters_interval OUTPUT

SET @sas_meters_interval = @sas_meters_interval * 2 -- Get double time

SET @query = '  SELECT
  ';
  
  IF (@is_multisite=1)
SET @query += '    tsmh_site_id';
ELSE
SET @query += '    ' + CONVERT(VARCHAR(10), @site_id) + 'AS tsmh_site_id';
SET @query += '
    , tsmh_terminal_id
    , tsmh_meter_code
    , tsmh_game_id
    , tsmh_denomination
    , tsmh_type
    , tsmh_datetime
    , tsmh_meter_ini_value
    , tsmh_meter_fin_value
    , tsmh_meter_increment
    , tsmh_raw_meter_increment
    , tsmh_last_reported
    , tsmh_sas_accounting_denom
    , tsmh_created_datetime
    , tsmh_group_id
    , tsmh_meter_origin
    , tsmh_meter_max_value 
  FROM 
    terminal_sas_meters_history 
      WITH (INDEX(IX_tsmh_datetime_type))'
SET @query += 'WHERE tsmh_datetime >= DATEADD(MINUTE, -' + CONVERT(VARCHAR(10), @sas_meters_interval) + ', CONVERT(DATETIME, ''' + CONVERT(VARCHAR(1000), @param_datetime, 120) + '''))
    AND tsmh_datetime < CONVERT(DATETIME, ''' + CONVERT(VARCHAR(1000), @param_datetime, 120) + ''')
    AND tsmh_type IN (1, 10, 11, 12, 13, 14, 15, 16 , 110, 140, 150, 160, 170, 180) /*) :: Pending to Review with Dani.*/
    AND (tsmh_meter_origin IN (0, 1, 2) OR tsmh_meter_origin IS NULL)
    AND tsmh_meter_code in (0,1,2,5)
  ';
EXEC sp_executesql @query
END
GO

/* 
**********************************************************
sp_EGM_Meters_Drop_Create_And_Fill_Temporary_Working_Table
**********************************************************
*/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EGM_Meters_Drop_Create_And_Fill_Temporary_Working_Table]') AND type in (N'P'))
  DROP PROCEDURE [dbo].[sp_EGM_Meters_Drop_Create_And_Fill_Temporary_Working_Table]
GO
CREATE PROCEDURE [dbo].[sp_EGM_Meters_Drop_Create_And_Fill_Temporary_Working_Table]
(
  @param_site_id INT,
  @param_datetime DATETIME
)
AS
BEGIN
  BEGIN TRY
    SET NOCOUNT ON;
    -- Variables/Params Declarations
    DECLARE @stored_call      VARCHAR(1000);
    DECLARE @site_id        INT    
    DECLARE @tsmh_period_obtained TABLE(
        tsmh_site_id           BIGINT       NULL
      , tsmh_terminal_id         INT       NULL
      , tsmh_meter_code         INT       NULL
      , tsmh_game_id           INT       NULL
      , tsmh_denomination       MONEY       NULL
      , tsmh_type           INT       NULL
      , tsmh_datetime         DATETIME2(3)  NULL
      , tsmh_meter_ini_value       BIGINT       NULL
      , tsmh_meter_fin_value       BIGINT       NULL
      , tsmh_meter_increment       BIGINT       NULL
      , tsmh_raw_meter_increment     BIGINT       NULL
      , tsmh_last_reported       DATETIME      NULL
      , tsmh_sas_accounting_denom   MONEY       NULL
      , tsmh_created_datetime     DATETIME     NULL
      , tsmh_group_id         BIGINT       NULL
      , tsmh_meter_origin       INT       NULL
      , tsmh_meter_max_value       BIGINT       NULL
    );
    DECLARE @tsmh_period_processed TABLE(
        tsmh_site_id           BIGINT       NULL
      , tsmh_terminal_id         INT       NULL
      , tsmh_meter_code         INT       NULL
      , tsmh_game_id           INT       NULL
      , tsmh_denomination       MONEY       NULL
      , tsmh_type           INT       NULL
      , tsmh_datetime         DATETIME2(3)   NULL
      , tsmh_meter_ini_value       BIGINT     NULL
      , tsmh_meter_fin_value       BIGINT     NULL
      , tsmh_meter_increment       BIGINT     NULL
      , tsmh_raw_meter_increment     BIGINT     NULL
      , tsmh_last_reported       DATETIME    NULL
      , tsmh_sas_accounting_denom   MONEY     NULL
      , tsmh_created_datetime     DATETIME   NULL
      , tsmh_group_id         BIGINT     NULL
      , tsmh_meter_origin       INT     NULL
      , tsmh_meter_max_value       BIGINT     NULL
    );
    SET @site_id = (
      SELECT gp_key_value
      FROM general_params
      WHERE gp_group_key = 'Site'
        AND gp_subject_key = 'Identifier'
    );

    SET @stored_call = 'dbo.sp_EGM_Meters_Get_TSMH_Period_Data @param_site_id = ''' + CONVERT(VARCHAR(100),@param_site_id) + ''', @param_datetime = ''' + CONVERT(VARCHAR(100), @param_datetime, 120) + '''';

  -----------------------------------------------------------------
  -- Deleting Temporary Working Table.
  -----------------------------------------------------------------
  IF OBJECT_ID('egm_meters_tsmh_temporary_working_table') IS NOT NULL
    BEGIN
      DROP TABLE egm_meters_tsmh_temporary_working_table
    END;
  IF OBJECT_ID('egm_meters_tsmh_temporary_working_table_ranked') IS NOT NULL
    BEGIN
      DROP TABLE egm_meters_tsmh_temporary_working_table_ranked
    END;

    -----------------------------------------------------------------
    -- Building CTE over TSMH with raw data for current execution.
    -----------------------------------------------------------------
    INSERT INTO @tsmh_period_obtained EXEC (@stored_call);

    -----------------------------------------------------------------
    -- Pre-Processing Data to validate tsmh_meter_ini_value is not null, in this case tsmh_meter_fin_value of previous record will be taken.
    -----------------------------------------------------------------
    INSERT INTO @tsmh_period_processed
    SELECT tsmh_site_id AS tsmh_site_id
       , tsmh_terminal_id AS tsmh_terminal_id
       , tsmh_meter_code AS tsmh_meter_code
       , tsmh_game_id AS tsmh_game_id
       , tsmh_denomination AS tsmh_denomination
       , tsmh_type AS tsmh_type
       , tsmh_datetime AS tsmh_datetime
         -----------------------------------------------------------------------------------
         /* This change is requested by Dani, in order to avoid NULL values, in 'tsmh_meter_ini_value' to be able to that 
          we take previous row 'tsmh_meter_fin_value' if isn't NULL else if is NULL we take current tsmh_fin_value.
          This change is validated with Andreu & Joaquin.
         . */
       , CASE
           WHEN (
             MAX(tsmh_meter_ini_value) IS NULL AND LAG(MAX(tsmh_meter_fin_value), 1) OVER (
             PARTITION BY
             tsmh_site_id
             , tsmh_terminal_id
             , tsmh_meter_code
             ORDER BY
             tsmh_site_id ASC
             , tsmh_terminal_id ASC
             , tsmh_meter_code ASC
             , tsmh_game_id ASC
             , tsmh_denomination ASC
             , tsmh_type ASC
             , tsmh_datetime ASC
             ) IS NOT NULL
             ) THEN LAG(MAX(tsmh_meter_fin_value), 1) OVER (
             PARTITION BY
             tsmh_site_id
             , tsmh_terminal_id
             , tsmh_meter_code
             ORDER BY
             tsmh_site_id ASC
             , tsmh_terminal_id ASC
             , tsmh_meter_code ASC
             , tsmh_game_id ASC
             , tsmh_denomination ASC
             , tsmh_type ASC
             , tsmh_datetime ASC
             )
           WHEN (
             MAX(tsmh_meter_ini_value) IS NULL AND LAG(MAX(tsmh_meter_fin_value), 1) OVER (
             PARTITION BY
             tsmh_site_id
             , tsmh_terminal_id
             , tsmh_meter_code
             ORDER BY
             tsmh_site_id ASC
             , tsmh_terminal_id ASC
             , tsmh_meter_code ASC
             , tsmh_game_id ASC
             , tsmh_denomination ASC
             , tsmh_type ASC
             , tsmh_datetime ASC
             ) IS NULL
             ) THEN MAX(tsmh_meter_fin_value)
           ELSE MAX(tsmh_meter_ini_value)
         END AS tsmh_meter_ini_value
         -----------------------------------------------------------------------------------
       , MAX(tsmh_meter_fin_value) AS tsmh_meter_fin_value
       , MAX(tsmh_meter_increment) AS tsmh_meter_increment
       , MAX(tsmh_raw_meter_increment) AS tsmh_raw_meter_increment
       , MAX(tsmh_last_reported) AS tsmh_last_reported
       , MAX(ISNULL(tsmh_sas_accounting_denom, 0)) AS tsmh_sas_accounting_denom /* FIX: This change has been requested by Dani in order to avoid NULL values in this field to not affect Gain operation */
       , MAX(tsmh_created_datetime) AS tsmh_created_datetime
       , MAX(tsmh_group_id) AS tsmh_group_id
       , MAX(tsmh_meter_origin) AS tsmh_meter_origin
       , MAX(tsmh_meter_max_value) AS tsmh_meter_max_value
    FROM @tsmh_period_obtained
      WHERE tsmh_site_id = @param_site_id
    GROUP BY tsmh_site_id
         , tsmh_terminal_id
         , tsmh_meter_code
         , tsmh_game_id
         , tsmh_denomination
         , tsmh_type
         , tsmh_datetime
    ORDER BY tsmh_site_id ASC
         , tsmh_terminal_id ASC
         , tsmh_meter_code ASC
         , tsmh_game_id ASC
         , tsmh_denomination ASC
         , tsmh_type ASC
         , tsmh_datetime ASC;

    -----------------------------------------------------------------
    -- Data transformation to be able to insert into the model tables.
    -----------------------------------------------------------------

    WITH cte_tsmh (tsmh_site_id, tsmh_terminal_id, tsmh_meter_code, tsmh_game_id, tsmh_denomination, tsmh_type, tsmh_datetime, tsmh_meter_ini_value, tsmh_meter_fin_value, tsmh_meter_increment, tsmh_raw_meter_increment, tsmh_last_reported, tsmh_sas_accounting_denom, tsmh_created_datetime, tsmh_group_id, tsmh_meter_origin, tsmh_meter_max_value) AS
    (
      SELECT tsmh_site_id
         , tsmh_terminal_id
         , tsmh_meter_code
         , tsmh_game_id
         , tsmh_denomination
         , tsmh_type
         , tsmh_datetime
         , tsmh_meter_ini_value
         , tsmh_meter_fin_value
         , tsmh_meter_increment
         , tsmh_raw_meter_increment
         , tsmh_last_reported
         , tsmh_sas_accounting_denom
         , tsmh_created_datetime
         , tsmh_group_id
         , tsmh_meter_origin
         , tsmh_meter_max_value
      FROM @tsmh_period_processed
    )
    --3-. Over TSMH work for extract HOURLY DATA (TSMH_TYPE=1) as is expected to retrieve.
    ,
    -----------------------------------------------------------------
    -- HOURLY RECORDS | METER TYPE --> (1)
    -----------------------------------------------------------------
    cte_pivoted_hourly_data_tsmh (emp_site_id, emp_datetime, emp_terminal_id, emp_game_id, emp_denomination, emp_record_type, emp_sas_accounting_denom, emp_last_created_datetime, emp_last_reported_datetime, emp_mc_0000, emp_mc_0001, emp_mc_0002, emp_mc_0005, emp_mc_0000_increment, emp_mc_0001_increment, emp_mc_0002_increment, emp_mc_0005_increment) AS
    (
      SELECT tsmh_site_id AS tsmh_site_id
         , tsmh_datetime AS tsmh_datetime
         , tsmh_terminal_id AS tsmh_terminal_id
         , tsmh_game_id AS tsmh_game_id
         , tsmh_denomination AS tsmh_denomination
         , tsmh_type AS tsmh_type
         , tsmh_sas_accounting_denom AS tsmh_sas_accounting_denom
         , tsmh_created_datetime AS tsmh_created_datetime
         , tsmh_last_reported AS tsmh_last_reported
         , CAST(ISNULL([0], 0) * 0.01 / NULLIF(tsmh_sas_accounting_denom, 0) AS BIGINT) AS emp_mc_0000
         , CAST(ISNULL([1], 0) * 0.01 / NULLIF(tsmh_sas_accounting_denom, 0) AS BIGINT) AS emp_mc_0001
         , CAST(ISNULL([2], 0) * 0.01 / NULLIF(tsmh_sas_accounting_denom, 0) AS BIGINT) AS emp_mc_0002
         , ISNULL([5], 0) AS emp_mc_0005
         , 0 AS emp_mc_0000_increment
         , 0 AS emp_mc_0001_increment
         , 0 AS emp_mc_0002_increment
         , 0 AS emp_mc_0005_increment
      FROM (
        SELECT tsmh_site_id
           , tsmh_terminal_id
           , tsmh_meter_code
           , tsmh_game_id
           , tsmh_denomination
           , tsmh_type
           , tsmh_datetime
           , tsmh_meter_ini_value
           , tsmh_meter_fin_value
           , tsmh_meter_increment
           , tsmh_raw_meter_increment
           , tsmh_last_reported
           , tsmh_sas_accounting_denom
           , tsmh_created_datetime
           , tsmh_group_id
           , tsmh_meter_origin
           , tsmh_meter_max_value
        FROM cte_tsmh
          WHERE tsmh_type IN (1, 180)
         AND tsmh_meter_code IN (0, 1, 2, 5)
         AND tsmh_site_id = @param_site_id

      ) AS tsmh
      PIVOT (
      MAX(tsmh_meter_ini_value)
      FOR tsmh_meter_code IN ([0], [1], [2], [5]
      )
      ) AS TSMH_PIVOTED_FIN_VALUE
      UNION ALL
      SELECT tsmh_site_id AS tsmh_site_id
         , tsmh_datetime AS tsmh_datetime
         , tsmh_terminal_id AS tsmh_terminal_id
         , tsmh_game_id AS tsmh_game_id
         , tsmh_denomination AS tsmh_denomination
         , tsmh_type AS tsmh_type
         , tsmh_sas_accounting_denom AS tsmh_sas_accounting_denom
         , tsmh_created_datetime AS tsmh_created_datetime
         , tsmh_last_reported AS tsmh_last_reported
         , 0 AS emp_mc_0000
         , 0 AS emp_mc_0001
         , 0 AS emp_mc_0002
         , 0 AS emp_mc_0005
         , CONVERT(DECIMAL(20, 2), ISNULL([0], 0) * 0.01) AS emp_mc_0000_increment
         , CONVERT(DECIMAL(20, 2), ISNULL([1], 0) * 0.01) AS emp_mc_0001_increment
         , CONVERT(DECIMAL(20, 2), ISNULL([2], 0) * 0.01) AS emp_mc_0002_increment
         , ISNULL([5], 0) AS emp_mc_0005_increment
      FROM (
        SELECT tsmh_site_id
           , tsmh_terminal_id
           , tsmh_meter_code
           , tsmh_game_id
           , tsmh_denomination
           , tsmh_type
           , tsmh_datetime
           , tsmh_meter_ini_value
           , tsmh_meter_fin_value
           , tsmh_meter_increment
           , tsmh_raw_meter_increment
           , tsmh_last_reported
           , tsmh_sas_accounting_denom
           , tsmh_created_datetime
           , tsmh_group_id
           , tsmh_meter_origin
           , tsmh_meter_max_value
        FROM cte_tsmh
        WHERE tsmh_type IN (1, 180)
          AND tsmh_meter_code IN (0, 1, 2, 5)
          AND tsmh_site_id = @param_site_id
      ) AS tsmh
      PIVOT (
      MAX(tsmh_meter_increment)
      FOR tsmh_meter_code IN ([0], [1], [2], [5]
      )
      ) AS TSMH_PIVOTED_INCREMENT
    ),
    -----------------------------------------------------------------
    -- ANOMALY RECORDS | METER TYPES --> (10, 13, 14, 15, 16)
    -----------------------------------------------------------------
    cte_pivoted_anomalies_data_tsmh (emp_site_id, emp_datetime, emp_terminal_id, emp_game_id, emp_denomination, emp_record_type, emp_sas_accounting_denom, emp_last_created_datetime, emp_last_reported_datetime, emp_mc_0000, emp_mc_0001, emp_mc_0002, emp_mc_0005, emp_mc_0000_increment, emp_mc_0001_increment, emp_mc_0002_increment, emp_mc_0005_increment) AS
    (
      SELECT tsmh_site_id
         , tsmh_datetime
         , tsmh_terminal_id
         , tsmh_game_id
         , tsmh_denomination
         , tsmh_type
         , tsmh_sas_accounting_denom
         , tsmh_created_datetime
         , tsmh_last_reported
         , CAST(ISNULL([0], 0) * 0.01 / NULLIF(tsmh_sas_accounting_denom, 0) AS BIGINT) AS emp_mc_0000
         , CAST(ISNULL([1], 0) * 0.01 / NULLIF(tsmh_sas_accounting_denom, 0) AS BIGINT) AS emp_mc_0001
         , CAST(ISNULL([2], 0) * 0.01 / NULLIF(tsmh_sas_accounting_denom, 0) AS BIGINT) AS emp_mc_0002
         , ISNULL([5], 0) AS emp_mc_0005
         , 0 AS emp_mc_0000_increment
         , 0 AS emp_mc_0001_increment
         , 0 AS emp_mc_0002_increment
         , 0 AS emp_mc_0005_increment

      FROM (
        SELECT tsmh_site_id
           , tsmh_terminal_id
           , tsmh_meter_code
           , tsmh_game_id
           , tsmh_denomination
           , tsmh_type
           , tsmh_datetime
           , tsmh_meter_ini_value
           , tsmh_meter_fin_value
           , tsmh_meter_increment
           , tsmh_raw_meter_increment
           , tsmh_last_reported
           , tsmh_sas_accounting_denom
           , tsmh_created_datetime
           , tsmh_group_id
           , tsmh_meter_origin
           , tsmh_meter_max_value
        FROM cte_tsmh
        WHERE tsmh_type IN (10, 13, 14, 15, 16)
          AND tsmh_meter_code IN (0, 1, 2, 5)
          AND tsmh_site_id = @param_site_id
      ) AS tsmh
      PIVOT (MAX(tsmh_meter_ini_value) FOR tsmh_meter_code IN ([0], [1], [2], [5])) AS TSMH_PIVOTED_FIN_VALUE
      UNION ALL
      SELECT tsmh_site_id
         , tsmh_datetime
         , tsmh_terminal_id
         , tsmh_game_id
         , tsmh_denomination
         , tsmh_type
         , tsmh_sas_accounting_denom
         , tsmh_created_datetime
         , tsmh_last_reported
         , 0 AS emp_mc_0000
         , 0 AS emp_mc_0001
         , 0 AS emp_mc_0002
         , 0 AS emp_mc_0005
         , CONVERT(DECIMAL(20, 2), ISNULL([0], 0) * 0.01) AS emp_mc_0000_increment
         , CONVERT(DECIMAL(20, 2), ISNULL([1], 0) * 0.01) AS emp_mc_0001_increment
         , CONVERT(DECIMAL(20, 2), ISNULL([2], 0) * 0.01) AS emp_mc_0002_increment
         , ISNULL([5], 0) AS emp_mc_0005_increment
      FROM (
        SELECT tsmh_site_id
           , tsmh_terminal_id
           , tsmh_meter_code
           , tsmh_game_id
           , tsmh_denomination
           , tsmh_type
           , tsmh_datetime
           , tsmh_meter_ini_value
           , tsmh_meter_fin_value
           , tsmh_meter_increment
           , tsmh_raw_meter_increment
           , tsmh_last_reported
           , tsmh_sas_accounting_denom
           , tsmh_created_datetime
           , tsmh_group_id
           , tsmh_meter_origin
           , tsmh_meter_max_value
        FROM cte_tsmh
        WHERE tsmh_type IN (10, 13, 14, 15, 16)
          AND tsmh_meter_code IN (0, 1, 2, 5)
          AND tsmh_site_id = @param_site_id
      ) AS tsmh
      PIVOT (MAX(tsmh_meter_increment) FOR tsmh_meter_code IN ([0], [1], [2], [5])) AS TSMH_PIVOTED_INCREMENT
    ),
    cte_leading_anomalies AS
    (
      SELECT t1.emp_site_id AS emp_site_id
         , t1.emp_terminal_id AS emp_terminal_id
         , t1.emp_game_id AS emp_game_id
         , t1.emp_denomination AS emp_denomination
         , t1.emp_datetime AS emp_datetime
         , MAX(t1.emp_record_type) AS emp_record_type
         , MAX(t1.emp_sas_accounting_denom) AS emp_sas_accounting_denom
         , MAX(t1.emp_last_created_datetime) AS emp_last_created_datetime
         , MAX(t1.emp_last_reported_datetime) AS emp_last_reported_datetime
         , MAX(t1.emp_mc_0000) AS emp_mc_0000
         , MAX(t1.emp_mc_0001) AS emp_mc_0001
         , MAX(t1.emp_mc_0002) AS emp_mc_0002
         , MAX(t1.emp_mc_0005) AS emp_mc_0005
         , t1.emp_mc_0000_increment AS emp_mc_0000_increment
         , t1.emp_mc_0001_increment AS emp_mc_0001_increment
         , t1.emp_mc_0002_increment AS emp_mc_0002_increment
         , t1.emp_mc_0005_increment AS emp_mc_0005_increment
      FROM cte_pivoted_anomalies_data_tsmh t1
      WHERE t1.emp_site_id = @param_datetime
      GROUP BY t1.emp_site_id
           , t1.emp_terminal_id
           , t1.emp_game_id
           , t1.emp_denomination
           , t1.emp_datetime
           , t1.emp_mc_0000_increment
           , t1.emp_mc_0001_increment
           , t1.emp_mc_0002_increment
           , t1.emp_mc_0005_increment
    )
    -----------------------------------------------------------------
    -- ROLLOVER ANOMALY RECORDS | METER TYPES --> (11)
    -----------------------------------------------------------------
    ,
    cte_pivoted_rollover_anomalies_data_tsmh (emp_site_id, emp_datetime, emp_terminal_id, emp_game_id, emp_denomination, emp_record_type, emp_sas_accounting_denom, emp_last_created_datetime, emp_last_reported_datetime, emp_mc_0000, emp_mc_0001, emp_mc_0002, emp_mc_0005, emp_mc_0000_increment, emp_mc_0001_increment, emp_mc_0002_increment, emp_mc_0005_increment) AS
    (
      SELECT tsmh_site_id
         , tsmh_datetime
         , tsmh_terminal_id
         , tsmh_game_id
         , tsmh_denomination
         , tsmh_type
         , tsmh_sas_accounting_denom
         , tsmh_created_datetime
         , tsmh_last_reported
         , CAST(ISNULL([0], 0) * 0.01 / NULLIF(tsmh_sas_accounting_denom, 0) AS BIGINT) AS emp_mc_0000
         , CAST(ISNULL([1], 0) * 0.01 / NULLIF(tsmh_sas_accounting_denom, 0) AS BIGINT) AS emp_mc_0001
         , CAST(ISNULL([2], 0) * 0.01 / NULLIF(tsmh_sas_accounting_denom, 0) AS BIGINT) AS emp_mc_0002
         , ISNULL([5], 0) AS emp_mc_0005
         , 0 AS emp_mc_0000_increment
         , 0 AS emp_mc_0001_increment
         , 0 AS emp_mc_0002_increment
         , 0 AS emp_mc_0005_increment
      FROM (
        SELECT tsmh_site_id
           , tsmh_terminal_id
           , tsmh_meter_code
           , tsmh_game_id
           , tsmh_denomination
           , tsmh_type
           , tsmh_datetime
           , tsmh_meter_ini_value
           , tsmh_meter_fin_value
           , tsmh_meter_increment
           , tsmh_raw_meter_increment
           , tsmh_last_reported
           , tsmh_sas_accounting_denom
           , tsmh_created_datetime
           , tsmh_group_id
           , tsmh_meter_origin
           , tsmh_meter_max_value
        FROM cte_tsmh
        WHERE tsmh_type IN (11)
          AND tsmh_meter_code IN (0, 1, 2, 5)
          AND tsmh_site_id = @param_site_id
      ) AS tsmh
      PIVOT (MAX(tsmh_meter_ini_value) FOR tsmh_meter_code IN ([0], [1], [2], [5])) AS TSMH_PIVOTED_FIN_VALUE
      UNION ALL
      SELECT tsmh_site_id
         , tsmh_datetime
         , tsmh_terminal_id
         , tsmh_game_id
         , tsmh_denomination
         , tsmh_type
         , tsmh_sas_accounting_denom
         , tsmh_created_datetime
         , tsmh_last_reported
         , 0 AS emp_mc_0000
         , 0 AS emp_mc_0001
         , 0 AS emp_mc_0002
         , 0 AS emp_mc_0005
         , CONVERT(DECIMAL(20, 2), ISNULL([0], 0) * 0.01) AS emp_mc_0000_increment
         , CONVERT(DECIMAL(20, 2), ISNULL([1], 0) * 0.01) AS emp_mc_0001_increment
         , CONVERT(DECIMAL(20, 2), ISNULL([2], 0) * 0.01) AS emp_mc_0002_increment
         , ISNULL([5], 0) AS emp_mc_0005_increment
      FROM (
        SELECT tsmh_site_id
           , tsmh_terminal_id
           , tsmh_meter_code
           , tsmh_game_id
           , tsmh_denomination
           , tsmh_type
           , tsmh_datetime
           , tsmh_meter_ini_value
           , tsmh_meter_fin_value
           , tsmh_meter_increment
           , tsmh_raw_meter_increment
           , tsmh_last_reported
           , tsmh_sas_accounting_denom
           , tsmh_created_datetime
           , tsmh_group_id
           , tsmh_meter_origin
           , tsmh_meter_max_value
        FROM cte_tsmh
        WHERE tsmh_type IN (11)
          AND tsmh_meter_code IN (0, 1, 2, 5)
          AND tsmh_site_id = @param_site_id
      ) AS tsmh
      PIVOT (MAX(tsmh_meter_increment) FOR tsmh_meter_code IN ([0], [1], [2], [5])) AS TSMH_PIVOTED_INCREMENT
    ),
    cte_leading_rollover_anomalies AS
    (
      SELECT t1.emp_site_id AS emp_site_id
         , t1.emp_terminal_id AS emp_terminal_id
         , t1.emp_game_id AS emp_game_id
         , t1.emp_denomination AS emp_denomination
         , t1.emp_datetime AS emp_datetime
         , MAX(t1.emp_record_type) AS emp_record_type
         , MAX(t1.emp_sas_accounting_denom) AS emp_sas_accounting_denom
         , MAX(t1.emp_last_created_datetime) AS emp_last_created_datetime
         , MAX(t1.emp_last_reported_datetime) AS emp_last_reported_datetime
         , MAX(t1.emp_mc_0000) AS emp_mc_0000
         , MAX(t1.emp_mc_0001) AS emp_mc_0001
         , MAX(t1.emp_mc_0002) AS emp_mc_0002
         , MAX(t1.emp_mc_0005) AS emp_mc_0005
         , t1.emp_mc_0000_increment AS emp_mc_0000_increment
         , t1.emp_mc_0001_increment AS emp_mc_0001_increment
         , t1.emp_mc_0002_increment AS emp_mc_0002_increment
         , t1.emp_mc_0005_increment AS emp_mc_0005_increment
      FROM cte_pivoted_rollover_anomalies_data_tsmh t1
      WHERE t1.emp_site_id = @param_site_id
      GROUP BY t1.emp_site_id
           , t1.emp_terminal_id
           , t1.emp_game_id
           , t1.emp_denomination
           , t1.emp_datetime
           , t1.emp_mc_0000_increment
           , t1.emp_mc_0001_increment
           , t1.emp_mc_0002_increment
           , t1.emp_mc_0005_increment
    )
    -----------------------------------------------------------------
    -- FIRST TIME ANOMALY RECORDS | METER TYPES --> (12)
    -----------------------------------------------------------------
    ,
    cte_pivoted_first_time_anomalies_data_tsmh (emp_site_id, emp_datetime, emp_terminal_id, emp_game_id, emp_denomination, emp_record_type, emp_sas_accounting_denom, emp_last_created_datetime, emp_last_reported_datetime, emp_mc_0000, emp_mc_0001, emp_mc_0002, emp_mc_0005, emp_mc_0000_increment, emp_mc_0001_increment, emp_mc_0002_increment, emp_mc_0005_increment) AS
    (
      SELECT tsmh_site_id
         , tsmh_datetime
         , tsmh_terminal_id
         , tsmh_game_id
         , tsmh_denomination
         , tsmh_type
         , tsmh_sas_accounting_denom
         , tsmh_created_datetime
         , tsmh_last_reported
         , CAST(ISNULL([0], 0) * 0.01 / NULLIF(tsmh_sas_accounting_denom, 0) AS BIGINT) AS emp_mc_0000
         , CAST(ISNULL([1], 0) * 0.01 / NULLIF(tsmh_sas_accounting_denom, 0) AS BIGINT) AS emp_mc_0001
         , CAST(ISNULL([2], 0) * 0.01 / NULLIF(tsmh_sas_accounting_denom, 0) AS BIGINT) AS emp_mc_0002
         , ISNULL([5], 0) AS emp_mc_0005
         , 0 AS emp_mc_0000_increment
         , 0 AS emp_mc_0001_increment
         , 0 AS emp_mc_0002_increment
         , 0 AS emp_mc_0005_increment
      FROM (
        SELECT tsmh_site_id
           , tsmh_terminal_id
           , tsmh_meter_code
           , tsmh_game_id
           , tsmh_denomination
           , tsmh_type
           , tsmh_datetime
           , tsmh_meter_ini_value
           , tsmh_meter_fin_value
           , tsmh_meter_increment
           , tsmh_raw_meter_increment
           , tsmh_last_reported
           , tsmh_sas_accounting_denom
           , tsmh_created_datetime
           , tsmh_group_id
           , tsmh_meter_origin
           , tsmh_meter_max_value
        FROM cte_tsmh
        WHERE tsmh_type IN (12)
          AND tsmh_meter_code IN (0, 1, 2, 5)
          AND tsmh_site_id = @param_site_id
      ) AS tsmh
      PIVOT (MAX(tsmh_meter_fin_value) FOR tsmh_meter_code IN ([0], [1], [2], [5])) AS TSMH_PIVOTED_FIN_VALUE
      UNION ALL
      SELECT tsmh_site_id
         , tsmh_datetime
         , tsmh_terminal_id
         , tsmh_game_id
         , tsmh_denomination
         , tsmh_type
         , tsmh_sas_accounting_denom
         , tsmh_created_datetime
         , tsmh_last_reported
         , 0 AS emp_mc_0000
         , 0 AS emp_mc_0001
         , 0 AS emp_mc_0002
         , 0 AS emp_mc_0005
         , CONVERT(DECIMAL(20, 2), ISNULL([0], 0) * 0.01) AS emp_mc_0000_increment
         , CONVERT(DECIMAL(20, 2), ISNULL([1], 0) * 0.01) AS emp_mc_0001_increment
         , CONVERT(DECIMAL(20, 2), ISNULL([2], 0) * 0.01) AS emp_mc_0002_increment
         , ISNULL([5], 0) AS emp_mc_0005_increment
      FROM (
        SELECT tsmh_site_id
           , tsmh_terminal_id
           , tsmh_meter_code
           , tsmh_game_id
           , tsmh_denomination
           , tsmh_type
           , tsmh_datetime
           , tsmh_meter_ini_value
           , tsmh_meter_fin_value
           , tsmh_meter_increment
           , tsmh_raw_meter_increment
           , tsmh_last_reported
           , tsmh_sas_accounting_denom
           , tsmh_created_datetime
           , tsmh_group_id
           , tsmh_meter_origin
           , tsmh_meter_max_value
        FROM cte_tsmh
        WHERE tsmh_type IN (12)
          AND tsmh_meter_code IN (0, 1, 2, 5)
          AND tsmh_site_id = @param_site_id
      ) AS tsmh
      PIVOT (MAX(tsmh_meter_increment) FOR tsmh_meter_code IN ([0], [1], [2], [5])) AS TSMH_PIVOTED_INCREMENT
    ),
    cte_leading_first_time_anomalies AS
    (
      SELECT t1.emp_site_id AS emp_site_id
         , t1.emp_terminal_id AS emp_terminal_id
         , t1.emp_game_id AS emp_game_id
         , t1.emp_denomination AS emp_denomination
         , t1.emp_datetime AS emp_datetime
         , MAX(t1.emp_record_type) AS emp_record_type
         , MAX(t1.emp_sas_accounting_denom) AS emp_sas_accounting_denom
         , MAX(t1.emp_last_created_datetime) AS emp_last_created_datetime
         , MAX(t1.emp_last_reported_datetime) AS emp_last_reported_datetime
         , MAX(t1.emp_mc_0000) AS emp_mc_0000
         , MAX(t1.emp_mc_0001) AS emp_mc_0001
         , MAX(t1.emp_mc_0002) AS emp_mc_0002
         , MAX(t1.emp_mc_0005) AS emp_mc_0005
         , t1.emp_mc_0000_increment AS emp_mc_0000_increment
         , t1.emp_mc_0001_increment AS emp_mc_0001_increment
         , t1.emp_mc_0002_increment AS emp_mc_0002_increment
         , t1.emp_mc_0005_increment AS emp_mc_0005_increment
      FROM cte_pivoted_first_time_anomalies_data_tsmh t1
      WHERE t1.emp_site_id = @param_site_id
      GROUP BY t1.emp_site_id
           , t1.emp_terminal_id
           , t1.emp_game_id
           , t1.emp_denomination
           , t1.emp_datetime
           , t1.emp_mc_0000_increment
           , t1.emp_mc_0001_increment
           , t1.emp_mc_0002_increment
           , t1.emp_mc_0005_increment
    )
    -----------------------------------------------------------------
    -- GROUPED ANOMALIES RECORDS | METER TYPES --> (140, 150, 160, 170)
    -----------------------------------------------------------------
    ,
    cte_pivoted_group_anomalies_data_tsmh (emp_site_id, emp_datetime, emp_terminal_id, emp_game_id, emp_denomination, emp_record_type, emp_sas_accounting_denom, emp_last_created_datetime, emp_last_reported_datetime, emp_mc_0000, emp_mc_0001, emp_mc_0002, emp_mc_0005, emp_mc_0000_increment, emp_mc_0001_increment, emp_mc_0002_increment, emp_mc_0005_increment) AS
    (
      SELECT tsmh_site_id
         , tsmh_datetime
         , tsmh_terminal_id
         , tsmh_game_id
         , tsmh_denomination
         , tsmh_type
         , tsmh_sas_accounting_denom
         , tsmh_created_datetime
         , tsmh_last_reported
         , CAST(ISNULL([0], 0) * 0.01 / NULLIF(tsmh_sas_accounting_denom, 0) AS BIGINT) AS emp_mc_0000
         , CAST(ISNULL([1], 0) * 0.01 / NULLIF(tsmh_sas_accounting_denom, 0) AS BIGINT) AS emp_mc_0001
         , CAST(ISNULL([2], 0) * 0.01 / NULLIF(tsmh_sas_accounting_denom, 0) AS BIGINT) AS emp_mc_0002
         , ISNULL([5], 0) AS emp_mc_0005
         , 0 AS emp_mc_0000_increment
         , 0 AS emp_mc_0001_increment
         , 0 AS emp_mc_0002_increment
         , 0 AS emp_mc_0005_increment
      FROM (
        SELECT tsmh_site_id
           , tsmh_terminal_id
           , tsmh_meter_code
           , tsmh_game_id
           , tsmh_denomination
           , tsmh_type
           , tsmh_datetime AS tsmh_datetime
           , MAX(tsmh_meter_ini_value) AS tsmh_meter_ini_value
           , MAX(tsmh_meter_fin_value) AS tsmh_meter_fin_value
           , MAX(tsmh_meter_increment) AS tsmh_meter_increment
           , MAX(tsmh_raw_meter_increment) AS tsmh_raw_meter_increment
           , MAX(tsmh_last_reported) AS tsmh_last_reported
           , MAX(tsmh_sas_accounting_denom) AS tsmh_sas_accounting_denom
           , MAX(tsmh_created_datetime) AS tsmh_created_datetime
           , MAX(tsmh_group_id) AS tsmh_group_id
           , MAX(tsmh_meter_origin) AS tsmh_meter_origin
           , MAX(tsmh_meter_max_value) AS tsmh_meter_max_value
        FROM cte_tsmh
        WHERE tsmh_type IN (140, 150, 160, 170)
          AND tsmh_meter_code IN (0, 1, 2, 5)
          AND tsmh_site_id = @param_site_id
        GROUP BY tsmh_datetime
             , tsmh_site_id
             , tsmh_terminal_id
             , tsmh_meter_code
             , tsmh_game_id
             , tsmh_denomination
             , tsmh_type
      ) AS tsmh
      PIVOT (MAX(tsmh_meter_ini_value) FOR tsmh_meter_code IN ([0], [1], [2], [5])) AS TSMH_PIVOTED_FIN_VALUE
      UNION ALL
      SELECT tsmh_site_id
         , tsmh_datetime
         , tsmh_terminal_id
         , tsmh_game_id
         , tsmh_denomination
         , tsmh_type
         , tsmh_sas_accounting_denom
         , tsmh_created_datetime
         , tsmh_last_reported
         , 0 AS emp_mc_0000
         , 0 AS emp_mc_0001
         , 0 AS emp_mc_0002
         , 0 AS emp_mc_0005
         , CONVERT(DECIMAL(20, 2), ISNULL([0], 0) * 0.01) AS emp_mc_0000_increment
         , CONVERT(DECIMAL(20, 2), ISNULL([1], 0) * 0.01) AS emp_mc_0001_increment
         , CONVERT(DECIMAL(20, 2), ISNULL([2], 0) * 0.01) AS emp_mc_0002_increment
         , ISNULL([5], 0) AS emp_mc_0005_increment
      FROM (
        SELECT tsmh_site_id
           , tsmh_terminal_id
           , tsmh_meter_code
           , tsmh_game_id
           , tsmh_denomination
           , tsmh_type
           , tsmh_datetime AS tsmh_datetime
           , MAX(tsmh_meter_ini_value) AS tsmh_meter_ini_value
           , MAX(tsmh_meter_fin_value) AS tsmh_meter_fin_value
           , MAX(tsmh_meter_increment) AS tsmh_meter_increment
           , MAX(tsmh_raw_meter_increment) AS tsmh_raw_meter_increment
           , MAX(tsmh_last_reported) AS tsmh_last_reported
           , MAX(tsmh_sas_accounting_denom) AS tsmh_sas_accounting_denom
           , MAX(tsmh_created_datetime) AS tsmh_created_datetime
           , MAX(tsmh_group_id) AS tsmh_group_id
           , MAX(tsmh_meter_origin) AS tsmh_meter_origin
           , MAX(tsmh_meter_max_value) AS tsmh_meter_max_value
        FROM cte_tsmh
        WHERE tsmh_type IN (140, 150, 160, 170)
          AND tsmh_meter_code IN (0, 1, 2, 5)
          AND tsmh_site_id = @param_site_id
        GROUP BY tsmh_datetime
             , tsmh_site_id
             , tsmh_terminal_id
             , tsmh_meter_code
             , tsmh_game_id
             , tsmh_denomination
             , tsmh_type
      ) AS tsmh
      PIVOT (MAX(tsmh_meter_increment) FOR tsmh_meter_code IN ([0], [1], [2], [5])) AS TSMH_PIVOTED_INCREMENT
    ),
    cte_leading_group_anomalies AS
    (
      SELECT t1.emp_site_id AS emp_site_id
         , t1.emp_terminal_id AS emp_terminal_id
         , t1.emp_game_id AS emp_game_id
         , t1.emp_denomination AS emp_denomination
         , t1.emp_datetime AS emp_datetime
         , MAX(t1.emp_record_type) AS emp_record_type
         , MAX(t1.emp_sas_accounting_denom) AS emp_sas_accounting_denom
         , MAX(t1.emp_last_created_datetime) AS emp_last_created_datetime
         , MAX(t1.emp_last_reported_datetime) AS emp_last_reported_datetime
         , MAX(t1.emp_mc_0000) AS emp_mc_0000
         , MAX(t1.emp_mc_0001) AS emp_mc_0001
         , MAX(t1.emp_mc_0002) AS emp_mc_0002
         , MAX(t1.emp_mc_0005) AS emp_mc_0005
         , t1.emp_mc_0000_increment AS emp_mc_0000_increment
         , t1.emp_mc_0001_increment AS emp_mc_0001_increment
         , t1.emp_mc_0002_increment AS emp_mc_0002_increment
         , t1.emp_mc_0005_increment AS emp_mc_0005_increment
      FROM cte_pivoted_group_anomalies_data_tsmh t1
      WHERE t1.emp_site_id = @param_site_id
      GROUP BY t1.emp_site_id
           , t1.emp_terminal_id
           , t1.emp_game_id
           , t1.emp_denomination
           , t1.emp_datetime
           , t1.emp_mc_0000_increment
           , t1.emp_mc_0001_increment
           , t1.emp_mc_0002_increment
           , t1.emp_mc_0005_increment
    )
    -----------------------------------------------------------------
    -- GROUPED ANOMALIES RECORDS | METER TYPES --> (110)
    -----------------------------------------------------------------
    ,
    cte_pivoted_rollover_group_anomalies_data_tsmh (emp_site_id, emp_datetime, emp_terminal_id, emp_game_id, emp_denomination, emp_record_type, emp_sas_accounting_denom, emp_last_created_datetime, emp_last_reported_datetime, emp_mc_0000, emp_mc_0001, emp_mc_0002, emp_mc_0005, emp_mc_0000_increment, emp_mc_0001_increment, emp_mc_0002_increment, emp_mc_0005_increment) AS
    (
      SELECT tsmh_site_id
         , tsmh_datetime
         , tsmh_terminal_id
         , tsmh_game_id
         , tsmh_denomination
         , tsmh_type
         , tsmh_sas_accounting_denom
         , tsmh_created_datetime
         , tsmh_last_reported
         , CAST(ISNULL([0], 0) * 0.01 / NULLIF(tsmh_sas_accounting_denom, 0) AS BIGINT) AS emp_mc_0000
         , CAST(ISNULL([1], 0) * 0.01 / NULLIF(tsmh_sas_accounting_denom, 0) AS BIGINT) AS emp_mc_0001
         , CAST(ISNULL([2], 0) * 0.01 / NULLIF(tsmh_sas_accounting_denom, 0) AS BIGINT) AS emp_mc_0002
         , ISNULL([5], 0) AS emp_mc_0005
         , 0 AS emp_mc_0000_increment
         , 0 AS emp_mc_0001_increment
         , 0 AS emp_mc_0002_increment
         , 0 AS emp_mc_0005_increment
      FROM (
        SELECT tsmh_site_id
           , tsmh_terminal_id
           , tsmh_meter_code
           , tsmh_game_id
           , tsmh_denomination
           , tsmh_type
           , tsmh_datetime AS tsmh_datetime
           , MAX(tsmh_meter_ini_value) AS tsmh_meter_ini_value
           , MAX(tsmh_meter_fin_value) AS tsmh_meter_fin_value
           , MAX(tsmh_meter_increment) AS tsmh_meter_increment
           , MAX(tsmh_raw_meter_increment) AS tsmh_raw_meter_increment
           , MAX(tsmh_last_reported) AS tsmh_last_reported
           , MAX(tsmh_sas_accounting_denom) AS tsmh_sas_accounting_denom
           , MAX(tsmh_created_datetime) AS tsmh_created_datetime
           , MAX(tsmh_group_id) AS tsmh_group_id
           , MAX(tsmh_meter_origin) AS tsmh_meter_origin
           , MAX(tsmh_meter_max_value) AS tsmh_meter_max_value
        FROM cte_tsmh
        WHERE tsmh_type IN (110)
          AND tsmh_meter_code IN (0, 1, 2, 5)
          AND tsmh_site_id = @param_site_id
        GROUP BY tsmh_datetime
             , tsmh_site_id
             , tsmh_terminal_id
             , tsmh_meter_code
             , tsmh_game_id
             , tsmh_denomination
             , tsmh_type
      ) AS tsmh
      PIVOT (MAX(tsmh_meter_fin_value) FOR tsmh_meter_code IN ([0], [1], [2], [5])) AS TSMH_PIVOTED_FIN_VALUE
      UNION ALL
      SELECT tsmh_site_id
         , tsmh_datetime
         , tsmh_terminal_id
         , tsmh_game_id
         , tsmh_denomination
         , tsmh_type
         , tsmh_sas_accounting_denom
         , tsmh_created_datetime
         , tsmh_last_reported
         , 0 AS emp_mc_0000
         , 0 AS emp_mc_0001
         , 0 AS emp_mc_0002
         , 0 AS emp_mc_0005
         , CONVERT(DECIMAL(20, 2), ISNULL([0], 0) * 0.01) AS emp_mc_0000_increment
         , CONVERT(DECIMAL(20, 2), ISNULL([1], 0) * 0.01) AS emp_mc_0001_increment
         , CONVERT(DECIMAL(20, 2), ISNULL([2], 0) * 0.01) AS emp_mc_0002_increment
         , ISNULL([5], 0) AS emp_mc_0005_increment
      FROM (
        SELECT tsmh_site_id
           , tsmh_terminal_id
           , tsmh_meter_code
           , tsmh_game_id
           , tsmh_denomination
           , tsmh_type
           , tsmh_datetime AS tsmh_datetime
           , MAX(tsmh_meter_ini_value) AS tsmh_meter_ini_value
           , MAX(tsmh_meter_fin_value) AS tsmh_meter_fin_value
           , MAX(tsmh_meter_increment) AS tsmh_meter_increment
           , MAX(tsmh_raw_meter_increment) AS tsmh_raw_meter_increment
           , MAX(tsmh_last_reported) AS tsmh_last_reported
           , MAX(tsmh_sas_accounting_denom) AS tsmh_sas_accounting_denom
           , MAX(tsmh_created_datetime) AS tsmh_created_datetime
           , MAX(tsmh_group_id) AS tsmh_group_id
           , MAX(tsmh_meter_origin) AS tsmh_meter_origin
           , MAX(tsmh_meter_max_value) AS tsmh_meter_max_value
        FROM cte_tsmh
        WHERE tsmh_type IN (110)
          AND tsmh_meter_code IN (0, 1, 2, 5)
          AND tsmh_site_id = @param_site_id
        GROUP BY tsmh_datetime
             , tsmh_site_id
             , tsmh_terminal_id
             , tsmh_meter_code
             , tsmh_game_id
             , tsmh_denomination
             , tsmh_type
      ) AS tsmh
      PIVOT (MAX(tsmh_meter_increment) FOR tsmh_meter_code IN ([0], [1], [2], [5])) AS TSMH_PIVOTED_INCREMENT
    ),
    cte_leading_rollover_group_anomalies AS
    (
      SELECT t1.emp_site_id AS emp_site_id
         , t1.emp_terminal_id AS emp_terminal_id
         , t1.emp_game_id AS emp_game_id
         , t1.emp_denomination AS emp_denomination
         , t1.emp_datetime AS emp_datetime
         , MAX(t1.emp_record_type) AS emp_record_type
         , MAX(t1.emp_sas_accounting_denom) AS emp_sas_accounting_denom
         , MAX(t1.emp_last_created_datetime) AS emp_last_created_datetime
         , MAX(t1.emp_last_reported_datetime) AS emp_last_reported_datetime
         , MAX(t1.emp_mc_0000) AS emp_mc_0000
         , MAX(t1.emp_mc_0001) AS emp_mc_0001
         , MAX(t1.emp_mc_0002) AS emp_mc_0002
         , MAX(t1.emp_mc_0005) AS emp_mc_0005
         , t1.emp_mc_0000_increment AS emp_mc_0000_increment
         , t1.emp_mc_0001_increment AS emp_mc_0001_increment
         , t1.emp_mc_0002_increment AS emp_mc_0002_increment
         , t1.emp_mc_0005_increment AS emp_mc_0005_increment
      FROM cte_pivoted_rollover_group_anomalies_data_tsmh t1
      WHERE t1.emp_site_id = @param_site_id
      GROUP BY t1.emp_site_id
           , t1.emp_terminal_id
           , t1.emp_game_id
           , t1.emp_denomination
           , t1.emp_datetime
           , t1.emp_mc_0000_increment
           , t1.emp_mc_0001_increment
           , t1.emp_mc_0002_increment
           , t1.emp_mc_0005_increment
    )
    -----------------------------------------------------------------
    -- SELECT UNION FOR --> HOURLY + ANOMALIES + ROLLOVER + FIRST TIME ANOMALIES + GROUPED ANOMALIES
    -----------------------------------------------------------------
    SELECT emp_working_day
       , emp_site_id
       , emp_datetime
       , emp_terminal_id
       , emp_game_id
       , emp_denomination
       , emp_record_type
       , emp_sas_accounting_denom
       , emp_last_created_datetime
       , emp_last_reported_datetime
       , emp_user_ignored
       , emp_user_ignored_datetime
       , emp_last_updated_user_id
       , emp_last_updated_user_datetime
       , emp_status
       , emp_mc_0000
       , emp_mc_0001
       , emp_mc_0002
       , emp_mc_0005
       , emp_mc_0000_increment
       , emp_mc_0001_increment
       , emp_mc_0002_increment
       , emp_mc_0005_increment
       , RANK() OVER (ORDER BY CASE emp_record_type
         WHEN 1                  THEN 0
         WHEN 10 | 11 | 12 | 13 | 14 | 15 | 16  THEN 1
         WHEN 170                THEN 2
         WHEN 140 | 150 | 160 | 110        THEN 3
         ELSE emp_record_type          END ASC) AS ranking
    INTO egm_meters_tsmh_temporary_working_table_ranked
    FROM (
      ----------
      -- HOURLY
      ----------
      (SELECT CASE
            WHEN (NOT EXISTS (
                SELECT *
                FROM egm_daily
                WHERE ed_working_day = dbo.GamingDayFromDateTime(emp_datetime)
              ) OR EXISTS (
                SELECT *
                FROM egm_daily
                WHERE ed_working_day = dbo.GamingDayFromDateTime(emp_datetime)
                  AND ed_status = 0
              )) THEN dbo.GamingDayFromDateTime(emp_datetime)
            ELSE (
                SELECT TOP (1) ed_working_day
                FROM egm_daily
                WHERE ed_working_day > dbo.GamingDayFromDateTime(emp_datetime)
                  AND ed_status = 0
                ORDER BY ed_working_day ASC
              )
          END AS emp_working_day
          , emp_site_id AS emp_site_id
          , emp_terminal_id AS emp_terminal_id
          , emp_game_id AS emp_game_id
          , emp_denomination AS emp_denomination
          , emp_datetime AS emp_datetime
          , emp_record_type AS emp_record_type
          , emp_sas_accounting_denom AS emp_sas_accounting_denom
          , MAX(emp_last_created_datetime) AS emp_last_created_datetime
          , MAX(emp_last_reported_datetime) AS emp_last_reported_datetime
          , NULL AS emp_user_ignored
          , NULL AS emp_user_ignored_datetime
          , NULL AS emp_last_updated_user_id
          , NULL AS emp_last_updated_user_datetime
          , NULL AS emp_status
          , SUM(emp_mc_0000) AS emp_mc_0000
          , SUM(emp_mc_0001) AS emp_mc_0001
          , SUM(emp_mc_0002) AS emp_mc_0002
          , SUM(emp_mc_0005) AS emp_mc_0005
          , 0 AS emp_mc_0000_increment
          , 0 AS emp_mc_0001_increment
          , 0 AS emp_mc_0002_increment
          , SUM(emp_mc_0005_increment) AS emp_mc_0005_increment
      FROM cte_pivoted_hourly_data_tsmh
      WHERE emp_site_id = @param_site_id
      GROUP BY dbo.GamingDayFromDateTime(emp_datetime)
           , emp_site_id
           , emp_datetime
           , emp_terminal_id
           , emp_game_id
           , emp_denomination
           , emp_record_type
           , emp_sas_accounting_denom
      )
      UNION
      ------------
      -- ANOMALIES
      ------------
      (
      SELECT CASE
             WHEN (NOT EXISTS (
                 SELECT *
                 FROM egm_daily
                 WHERE ed_working_day = dbo.GamingDayFromDateTime(emp_datetime)
               ) OR EXISTS (
                 SELECT *
                 FROM egm_daily
                 WHERE ed_working_day = dbo.GamingDayFromDateTime(emp_datetime)
                   AND ed_status = 0
               )) THEN dbo.GamingDayFromDateTime(emp_datetime)
             ELSE (
                 SELECT TOP (1) ed_working_day
                 FROM egm_daily
                 WHERE ed_working_day > dbo.GamingDayFromDateTime(emp_datetime)
                   AND ed_status = 0
                 ORDER BY ed_working_day ASC
               )
           END AS emp_working_day
         , emp_site_id AS emp_site_id
         , emp_terminal_id AS emp_terminal_id
         , emp_game_id AS emp_game_id
         , emp_denomination AS emp_denomination
         , emp_datetime AS emp_datetime
         , emp_record_type AS emp_record_type
         , emp_sas_accounting_denom AS emp_sas_accounting_denom
         , MAX(emp_last_created_datetime) AS emp_last_created_datetime
         , MAX(emp_last_reported_datetime) AS emp_last_reported_datetime
         , NULL AS emp_user_ignored
         , NULL AS emp_user_ignored_datetime
         , NULL AS emp_last_updated_user_id
         , NULL AS emp_last_updated_user_datetime
         , NULL AS emp_status
         , MAX(emp_mc_0000) AS emp_mc_0000
         , MAX(emp_mc_0001) AS emp_mc_0001
         , MAX(emp_mc_0002) AS emp_mc_0002
         , MAX(emp_mc_0005) AS emp_mc_0005
         , SUM(emp_mc_0000_increment) AS emp_mc_0000_increment
         , SUM(emp_mc_0001_increment) AS emp_mc_0001_increment
         , SUM(emp_mc_0002_increment) AS emp_mc_0002_increment
         , SUM(emp_mc_0005_increment) AS emp_mc_0005_increment
      FROM cte_leading_anomalies
      WHERE emp_site_id = @param_site_id
      GROUP BY dbo.GamingDayFromDateTime(emp_datetime)
           , emp_site_id
           , emp_datetime
           , emp_terminal_id
           , emp_game_id
           , emp_denomination
           , emp_record_type
           , emp_sas_accounting_denom
      )
      UNION
      -----------------------
      -- ROLLOVER ANOMALIES
      -----------------------
      (
      SELECT CASE
             WHEN (NOT EXISTS (
                 SELECT *
                 FROM egm_daily
                 WHERE ed_working_day = dbo.GamingDayFromDateTime(emp_datetime)
               ) OR EXISTS (
                 SELECT *
                 FROM egm_daily
                 WHERE ed_working_day = dbo.GamingDayFromDateTime(emp_datetime)
                   AND ed_status = 0
               )) THEN dbo.GamingDayFromDateTime(emp_datetime)
             ELSE (
                 SELECT TOP (1) ed_working_day
                 FROM egm_daily
                 WHERE ed_working_day > dbo.GamingDayFromDateTime(emp_datetime)
                   AND ed_status = 0
                 ORDER BY ed_working_day ASC
               )
           END AS emp_working_day
         , emp_site_id AS emp_site_id
         , emp_terminal_id AS emp_terminal_id
         , emp_game_id AS emp_game_id
         , emp_denomination AS emp_denomination
         , emp_datetime AS emp_datetime
         , emp_record_type AS emp_record_type
         , emp_sas_accounting_denom AS emp_sas_accounting_denom
         , MAX(emp_last_created_datetime) AS emp_last_created_datetime
         , MAX(emp_last_reported_datetime) AS emp_last_reported_datetime
         , NULL AS emp_user_ignored
         , NULL AS emp_user_ignored_datetime
         , NULL AS emp_last_updated_user_id
         , NULL AS emp_last_updated_user_datetime
         , NULL AS emp_status
         , MAX(emp_mc_0000) AS emp_mc_0000
         , MAX(emp_mc_0001) AS emp_mc_0001
         , MAX(emp_mc_0002) AS emp_mc_0002
         , MAX(emp_mc_0005) AS emp_mc_0005
         , SUM(emp_mc_0000_increment) AS emp_mc_0000_increment
         , SUM(emp_mc_0001_increment) AS emp_mc_0001_increment
         , SUM(emp_mc_0002_increment) AS emp_mc_0002_increment
         , SUM(emp_mc_0005_increment) AS emp_mc_0005_increment
      FROM cte_leading_rollover_anomalies
      WHERE emp_site_id = @param_site_id
      GROUP BY dbo.GamingDayFromDateTime(emp_datetime)
           , emp_site_id
           , emp_datetime
           , emp_terminal_id
           , emp_game_id
           , emp_denomination
           , emp_record_type
           , emp_sas_accounting_denom
      )
      UNION
      -----------------------
      -- FIRST TIME ANOMALIES
      -----------------------
      (
      SELECT CASE
             WHEN (NOT EXISTS (
                 SELECT *
                 FROM egm_daily
                 WHERE ed_working_day = dbo.GamingDayFromDateTime(emp_datetime)
               ) OR EXISTS (
                 SELECT *
                 FROM egm_daily
                 WHERE ed_working_day = dbo.GamingDayFromDateTime(emp_datetime)
                   AND ed_status = 0
               )) THEN dbo.GamingDayFromDateTime(emp_datetime)
             ELSE (
                 SELECT TOP (1) ed_working_day
                 FROM egm_daily
                 WHERE ed_working_day > dbo.GamingDayFromDateTime(emp_datetime)
                   AND ed_status = 0
                 ORDER BY ed_working_day ASC
               )
           END AS emp_working_day
         , emp_site_id AS emp_site_id
         , emp_terminal_id AS emp_terminal_id
         , emp_game_id AS emp_game_id
         , emp_denomination AS emp_denomination
         , CONVERT(DATETIME2(3), DATEADD(ms, 997, DATEADD(ms, -DATEPART(ms, emp_datetime), emp_datetime))) AS emp_datetime
         , emp_record_type AS emp_record_type
         , emp_sas_accounting_denom AS emp_sas_accounting_denom
         , MAX(emp_last_created_datetime) AS emp_last_created_datetime
         , MAX(emp_last_reported_datetime) AS emp_last_reported_datetime
         , NULL AS emp_user_ignored
         , NULL AS emp_user_ignored_datetime
         , NULL AS emp_last_updated_user_id
         , NULL AS emp_last_updated_user_datetime
         , NULL AS emp_status
         , MAX(emp_mc_0000) AS emp_mc_0000
         , MAX(emp_mc_0001) AS emp_mc_0001
         , MAX(emp_mc_0002) AS emp_mc_0002
         , MAX(emp_mc_0005) AS emp_mc_0005
         , SUM(emp_mc_0000_increment) AS emp_mc_0000_increment
         , SUM(emp_mc_0001_increment) AS emp_mc_0001_increment
         , SUM(emp_mc_0002_increment) AS emp_mc_0002_increment
         , SUM(emp_mc_0005_increment) AS emp_mc_0005_increment
      FROM cte_leading_first_time_anomalies
      WHERE emp_site_id = @param_site_id
      GROUP BY dbo.GamingDayFromDateTime(emp_datetime)
           , emp_site_id
           , CONVERT(DATETIME2(3), DATEADD(ms, 997, DATEADD(ms, -DATEPART(ms, emp_datetime), emp_datetime)))
           , emp_terminal_id
           , emp_game_id
           , emp_denomination
           , emp_record_type
           , emp_sas_accounting_denom
      )
      UNION
      -----------------------------
      -- GROUPED ANOMALIES
      -----------------------------
      (SELECT CASE
            WHEN (NOT EXISTS (
                SELECT *
                FROM egm_daily
                WHERE ed_working_day = dbo.GamingDayFromDateTime(emp_datetime)
              ) OR EXISTS (
                SELECT *
                FROM egm_daily
                WHERE ed_working_day = dbo.GamingDayFromDateTime(emp_datetime)
                  AND ed_status = 0
              )) THEN dbo.GamingDayFromDateTime(emp_datetime)
            ELSE (
                SELECT TOP (1) ed_working_day
                FROM egm_daily
                WHERE ed_working_day > dbo.GamingDayFromDateTime(emp_datetime)
                  AND ed_status = 0
                ORDER BY ed_working_day ASC
              )
          END AS emp_working_day
          , emp_site_id AS emp_site_id
          , emp_terminal_id AS emp_terminal_id
          , emp_game_id AS emp_game_id
          , emp_denomination AS emp_denomination
          , CONVERT(DATETIME2(3), DATEADD(ms, 997, DATEADD(ms, -DATEPART(ms, emp_datetime), emp_datetime))) AS emp_datetime
          , emp_record_type AS emp_record_type
          , emp_sas_accounting_denom AS emp_sas_accounting_denom
          , MAX(emp_last_created_datetime) AS emp_last_created_datetime
          , MAX(emp_last_reported_datetime) AS emp_last_reported_datetime
          , NULL AS emp_user_ignored
          , NULL AS emp_user_ignored_datetime
          , NULL AS emp_last_updated_user_id
          , NULL AS emp_last_updated_user_datetime
          , NULL AS emp_status
          , MAX(emp_mc_0000) AS emp_mc_0000
          , MAX(emp_mc_0001) AS emp_mc_0001
          , MAX(emp_mc_0002) AS emp_mc_0002
          , MAX(emp_mc_0005) AS emp_mc_0005
          , SUM(emp_mc_0000_increment) AS emp_mc_0000_increment
          , SUM(emp_mc_0001_increment) AS emp_mc_0001_increment
          , SUM(emp_mc_0002_increment) AS emp_mc_0002_increment
          , SUM(emp_mc_0005_increment) AS emp_mc_0005_increment
      FROM cte_leading_group_anomalies
      WHERE emp_site_id = @param_site_id
      GROUP BY dbo.GamingDayFromDateTime(emp_datetime)
           , emp_site_id
           , CONVERT(DATETIME2(3), DATEADD(ms, 997, DATEADD(ms, -DATEPART(ms, emp_datetime), emp_datetime)))
           , emp_terminal_id
           , emp_game_id
           , emp_denomination
           , emp_record_type
           , emp_sas_accounting_denom
      )
      UNION
      --------------------
      -- ROLLOVER GROUPED ANOMALIES
      --------------------
      (
      SELECT CASE
             WHEN (NOT EXISTS (
                 SELECT *
                 FROM egm_daily
                 WHERE ed_working_day = dbo.GamingDayFromDateTime(emp_datetime)
               ) OR EXISTS (
                 SELECT *
                 FROM egm_daily
                 WHERE ed_working_day = dbo.GamingDayFromDateTime(emp_datetime)
                   AND ed_status = 0
               )) THEN dbo.GamingDayFromDateTime(emp_datetime)
             ELSE (
                 SELECT TOP (1) ed_working_day
                 FROM egm_daily
                 WHERE ed_working_day > dbo.GamingDayFromDateTime(emp_datetime)
                   AND ed_status = 0
                 ORDER BY ed_working_day ASC
               )
           END AS emp_working_day
         , emp_site_id AS emp_site_id
         , emp_terminal_id AS emp_terminal_id
         , emp_game_id AS emp_game_id
         , emp_denomination AS emp_denomination
         , CONVERT(DATETIME2(3), DATEADD(ms, 997, DATEADD(ms, -DATEPART(ms, emp_datetime), emp_datetime))) AS emp_datetime
         , emp_record_type AS emp_record_type
         , emp_sas_accounting_denom AS emp_sas_accounting_denom
         , MAX(emp_last_created_datetime) AS emp_last_created_datetime
         , MAX(emp_last_reported_datetime) AS emp_last_reported_datetime
         , NULL AS emp_user_ignored
         , NULL AS emp_user_ignored_datetime
         , NULL AS emp_last_updated_user_id
         , NULL AS emp_last_updated_user_datetime
         , NULL AS emp_status
         , MAX(emp_mc_0000) AS emp_mc_0000
         , MAX(emp_mc_0001) AS emp_mc_0001
         , MAX(emp_mc_0002) AS emp_mc_0002
         , MAX(emp_mc_0005) AS emp_mc_0005
         , SUM(emp_mc_0000_increment) AS emp_mc_0000_increment
         , SUM(emp_mc_0001_increment) AS emp_mc_0001_increment
         , SUM(emp_mc_0002_increment) AS emp_mc_0002_increment
         , SUM(emp_mc_0005_increment) AS emp_mc_0005_increment
      FROM cte_leading_rollover_group_anomalies
      WHERE emp_site_id = @param_site_id
      GROUP BY dbo.GamingDayFromDateTime(emp_datetime)
           , emp_site_id
           , CONVERT(DATETIME2(3), DATEADD(ms, 997, DATEADD(ms, -DATEPART(ms, emp_datetime), emp_datetime)))
           , emp_terminal_id
           , emp_game_id
           , emp_denomination
           , emp_record_type
           , emp_sas_accounting_denom
      )
    ) AS X;          


    SELECT emp_working_day
      , emp_site_id
      , emp_datetime
      , emp_terminal_id
      , emp_game_id
      , emp_denomination
      , emp_record_type
      , emp_sas_accounting_denom
      , emp_last_created_datetime
      , emp_last_reported_datetime
      , emp_user_ignored
      , emp_user_ignored_datetime
      , emp_last_updated_user_id
      , emp_last_updated_user_datetime
      , emp_status
      , emp_mc_0000
      , emp_mc_0001
      , emp_mc_0002
      , emp_mc_0005
      , emp_mc_0000_increment
      , emp_mc_0001_increment
      , emp_mc_0002_increment
      , emp_mc_0005_increment 
      INTO egm_meters_tsmh_temporary_working_table
      FROM egm_meters_tsmh_temporary_working_table_ranked;
      
    RETURN 0;
  END TRY
  BEGIN CATCH
    EXEC dbo.spErrorHandling
  END CATCH
END
GO

/* 
**********************************************************
sp_EGM_Meters_Get_Control_Mark
**********************************************************
*/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EGM_Meters_Get_Control_Mark]') AND type in (N'P'))
 DROP PROCEDURE [dbo].[sp_EGM_Meters_Get_Control_Mark]
GO

CREATE PROCEDURE [dbo].[sp_EGM_Meters_Get_Control_Mark] 
(
  @param_site_id  INT,
  @param_datetime DATETIME OUTPUT
)
AS
BEGIN
  BEGIN TRY
  SET NOCOUNT ON;
      SELECT   TOP (1) @param_datetime = ISNULL(ecm_control_mark, GETDATE())
        FROM   egm_control_mark
    WHERE   ecm_site_id = @param_site_id ;

      IF @param_datetime IS NULL
      BEGIN
       SET @param_datetime = ISNULL(@param_datetime, GETDATE())
      END
  END TRY
  BEGIN CATCH
  EXEC dbo.spErrorHandling
  END CATCH
END
GO

/* 
**********************************************************
sp_EGM_Meters_Update_Control_Mark
**********************************************************
*/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EGM_Meters_Update_Control_Mark]') AND type in (N'P'))
  DROP PROCEDURE [dbo].[sp_EGM_Meters_Update_Control_Mark]
GO

CREATE PROCEDURE [dbo].[sp_EGM_Meters_Update_Control_Mark] 
(
  @param_site_id INT,
  @param_datetime DATETIME
)
AS
BEGIN
  BEGIN TRY
    SET NOCOUNT ON;
    DECLARE @correction_time       INT;

    IF @param_datetime IS NOT NULL AND @param_datetime <= GETDATE()
    BEGIN

       SET @correction_time = ISNULL((SELECT gp_key_value FROM general_params WHERE gp_group_key = 'WCP' AND gp_subject_key = 'IntervalBilling.CorretionTime'), 1)

       SET @param_datetime = DATEADD(MINUTE, -@correction_time, @param_datetime)

       DELETE FROM egm_control_mark WHERE ecm_site_id = @param_site_id;
       INSERT INTO egm_control_mark (ecm_site_id, ecm_control_mark) VALUES (@param_site_id,@param_datetime);
    END
    
    RETURN 0;
  END TRY
  BEGIN CATCH
    EXEC dbo.spErrorHandling
  END CATCH
END
GO

/* 
**********************************************************
sp_EGM_Meters_Upsert_Table_egm_meters_by_day
**********************************************************
*/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EGM_Meters_Upsert_Table_egm_meters_by_day]') AND type in (N'P'))
 DROP PROCEDURE [dbo].[sp_EGM_Meters_Upsert_Table_egm_meters_by_day]
GO

CREATE PROCEDURE [dbo].[sp_EGM_Meters_Upsert_Table_egm_meters_by_day]
  @param_working_day INT
AS
BEGIN
  BEGIN TRY
    SET NOCOUNT ON;  
    -- 0-. Declare Variables Area.
    DECLARE @table_temporary_by_day TABLE(
        emd_working_day INT NULL
      , emd_site_id BIGINT NULL
      , emd_terminal_id INT NULL
      , emd_game_id INT NULL
      , emd_warning INT NULL
      , emd_user_period_modified BIT NULL
      , emd_user_validated BIT NULL
      , emd_user_validated_datetime DATETIME NULL
      , emd_last_updated DATETIME NULL
      , emd_last_updated_user_id INT NULL
      , emd_connected BIT NULL
      , emd_status INT NULL
      , emd_mc_0000_increment DECIMAL(20,2) NULL
      , emd_mc_0001_increment DECIMAL(20,2) NULL
      , emd_mc_0002_increment DECIMAL(20,2) NULL
      , emd_mc_0005_increment BIGINT NULL
      , emd_mc_0000 BIGINT NULL
      , emd_mc_0001 BIGINT NULL
      , emd_mc_0002 BIGINT NULL
      , emd_mc_0005 BIGINT NULL
    );

  -- 1-. Deleting Temporary Working Table.
  IF OBJECT_ID('egm_meters_by_day_temporary_working_table') IS NOT NULL
    BEGIN
  DROP TABLE egm_meters_by_day_temporary_working_table
  END;
  INSERT INTO @table_temporary_by_day
    SELECT
      p.emp_working_day
       ,p.emp_site_id
       ,p.emp_terminal_id
       ,MAX(p3.emp_game_id)
       ,SUM(
      DISTINCT CASE p.emp_record_type
        WHEN 1 THEN 0   /* Hourly Report - [Isn't an anomaly] */
        WHEN 10 THEN 2   /* Reset */
        WHEN 110 THEN 4   /* Rollover */
        WHEN 12 THEN 8   /* First Time */
        WHEN 13 THEN 16   /* Discarded */
        WHEN 140 THEN 32   /* SAS Accounting Denom Change */
        WHEN 150 THEN 64   /* Service RAM Clear */
        WHEN 160 THEN 128   /* Machine RAM Clear */
        ELSE 0   /* Rest Of Cases */
      END
      )
       ,1
       ,1
       ,NULL
       ,NULL
       ,NULL
       ,0
       ,0
       ,SUM(ISNULL(p.emp_mc_0000_increment, 0))
       ,SUM(ISNULL(p.emp_mc_0001_increment, 0))
       ,SUM(ISNULL(p.emp_mc_0002_increment, 0))
       ,SUM(ISNULL(p.emp_mc_0005_increment, 0))
       ,MAX(ISNULL(p2.emp_mc_0000, 0))
       ,MAX(ISNULL(p2.emp_mc_0001, 0))
       ,MAX(ISNULL(p2.emp_mc_0002, 0))
       ,MAX(ISNULL(p2.emp_mc_0005, 0))
    FROM dbo.egm_meters_by_period p
    LEFT JOIN dbo.egm_meters_by_period p2
      ON p.emp_working_day = p2.emp_working_day
        AND p.emp_site_id = p2.emp_site_id
        AND p.emp_terminal_id = p2.emp_terminal_id
        AND p.emp_game_id = p2.emp_game_id
        AND p.emp_denomination = p2.emp_denomination
        AND p.emp_datetime = p2.emp_datetime
        AND p2.emp_datetime = (SELECT
            MAX(p21.emp_datetime)
          FROM dbo.egm_meters_by_period p21
          WHERE p.emp_working_day = p21.emp_working_day
          AND p.emp_site_id = p21.emp_site_id
          AND p.emp_terminal_id = p21.emp_terminal_id
          AND p.emp_game_id = p21.emp_game_id
          AND p.emp_denomination = p21.emp_denomination)
    LEFT JOIN dbo.egm_meters_by_period p3
      ON p.emp_working_day = p3.emp_working_day
        AND p.emp_site_id = p3.emp_site_id
        AND p.emp_terminal_id = p3.emp_terminal_id
        AND p.emp_game_id = p3.emp_game_id
        AND p.emp_denomination = p3.emp_denomination
        AND p.emp_datetime = p3.emp_datetime
        AND p3.emp_datetime = (SELECT
            MIN(p31.emp_datetime)
          FROM dbo.egm_meters_by_period p31
          WHERE p.emp_working_day = p31.emp_working_day
          AND p.emp_site_id = p31.emp_site_id
          AND p.emp_terminal_id = p31.emp_terminal_id
          AND p.emp_game_id = p31.emp_game_id
          AND p.emp_denomination = p31.emp_denomination)
    LEFT JOIN dbo.terminals_connected tc
      ON tc.tc_date = CONVERT(DATETIME, CONVERT(CHAR(8), @param_working_day))
        AND tc.tc_connected = 1
        AND tc.tc_terminal_id = p.emp_terminal_id
    WHERE p.emp_working_day = @param_working_day
    GROUP BY p.emp_working_day
        ,p.emp_site_id
        ,p.emp_terminal_id
        ,p.emp_game_id
        ,p.emp_denomination;


    WITH cte_temporary_by_day (emd_working_day, emd_site_id, emd_terminal_id, emd_game_id, emd_warning, emd_user_period_modified, emd_user_validated, emd_user_validated_datetime, emd_last_updated, emd_last_updated_user_id, emd_connected, emd_status, emd_mc_0000_increment, emd_mc_0001_increment, emd_mc_0002_increment, emd_mc_0005_increment, emd_mc_0000, emd_mc_0001, emd_mc_0002, emd_mc_0005)
    AS
    (SELECT
        emd_working_day
         ,emd_site_id
         ,emd_terminal_id
         ,emd_game_id
         ,emd_warning
         ,emd_user_period_modified
         ,emd_user_validated
         ,emd_user_validated_datetime
         ,emd_last_updated
         ,emd_last_updated_user_id
         ,emd_connected
         ,emd_status
         ,emd_mc_0000_increment
         ,emd_mc_0001_increment
         ,emd_mc_0002_increment
         ,emd_mc_0005_increment
         ,emd_mc_0000
         ,emd_mc_0001
         ,emd_mc_0002
         ,emd_mc_0005
      FROM @table_temporary_by_day)
    SELECT
      emd_working_day
       ,emd_site_id
       ,emd_terminal_id
       ,emd_game_id
       ,emd_warning
       ,emd_user_period_modified
       ,emd_user_validated
       ,emd_user_validated_datetime
       ,emd_last_updated
       ,emd_last_updated_user_id
       ,emd_connected
       ,emd_status
       ,emd_mc_0000_increment
       ,emd_mc_0001_increment
       ,emd_mc_0002_increment
       ,emd_mc_0005_increment
       ,emd_mc_0000
       ,emd_mc_0001
       ,emd_mc_0002
       ,emd_mc_0005 
    INTO egm_meters_by_day_temporary_working_table
    FROM cte_temporary_by_day
    ORDER BY emd_working_day DESC
    , emd_terminal_id ASC
    -------------------------
    -- 3-. UPDATE [egm_meters_by_period] Treatment
    -------------------------
    UPDATE dest
    SET dest.emd_warning = dbo.CompareTwoAnomaliesMasks(src.emd_warning, dest.emd_warning)
       ,dest.emd_status = ISNULL(src.emd_status, 0)
       ,dest.emd_mc_0000_increment = ISNULL(src.emd_mc_0000_increment, 0)
       ,dest.emd_mc_0001_increment = ISNULL(src.emd_mc_0001_increment, 0)
       ,dest.emd_mc_0002_increment = ISNULL(src.emd_mc_0002_increment, 0)
       ,dest.emd_mc_0005_increment = ISNULL(src.emd_mc_0005_increment, 0)
       ,dest.emd_mc_0000 = ISNULL(src.emd_mc_0000, 0)
       ,dest.emd_mc_0001 = ISNULL(src.emd_mc_0001, 0)
       ,dest.emd_mc_0002 = ISNULL(src.emd_mc_0002, 0)
       ,dest.emd_mc_0005 = ISNULL(src.emd_mc_0005, 0)
    FROM egm_meters_by_day_temporary_working_table src
    INNER JOIN egm_meters_by_day dest
      ON src.emd_working_day = dest.emd_working_day
      AND src.emd_site_id = dest.emd_site_id
      AND src.emd_terminal_id = dest.emd_terminal_id
    -------------------------
    -- 4-. INSERT [egm_meters_by_period] Treatment
    -------------------------
    INSERT INTO egm_meters_by_day
      SELECT
        src.emd_working_day
         ,src.emd_site_id
         ,src.emd_terminal_id
         ,src.emd_game_id
         ,src.emd_warning
         ,0
         ,0
         ,NULL
         ,NULL
         ,NULL
         ,src.emd_connected
         ,ISNULL(src.emd_status, 0)
         ,ISNULL(src.emd_mc_0000_increment, 0)
         ,ISNULL(src.emd_mc_0001_increment, 0)
         ,ISNULL(src.emd_mc_0002_increment, 0)
         ,ISNULL(src.emd_mc_0005_increment, 0)
         ,ISNULL(src.emd_mc_0000, 0)
         ,ISNULL(src.emd_mc_0001, 0)
         ,ISNULL(src.emd_mc_0002, 0)
         ,ISNULL(src.emd_mc_0005, 0)
      FROM egm_meters_by_day_temporary_working_table src
      LEFT JOIN egm_meters_by_day dest
        ON src.emd_site_id = dest.emd_site_id
          AND src.emd_terminal_id = dest.emd_terminal_id
          AND src.emd_working_day = dest.emd_working_day
      WHERE dest.emd_working_day IS NULL
      AND dest.emd_terminal_id IS NULL
      AND dest.emd_site_id IS NULL;
  END TRY
  BEGIN CATCH
    EXEC dbo.spErrorHandling
  END CATCH
END
GO

/* 
**********************************************************
sp_EGM_Meters_Upsert_Table_egm_meters_by_period
**********************************************************
*/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EGM_Meters_Upsert_Table_egm_meters_by_period]') AND type in (N'P'))
 DROP PROCEDURE [dbo].[sp_EGM_Meters_Upsert_Table_egm_meters_by_period]
GO

CREATE PROCEDURE [dbo].[sp_EGM_Meters_Upsert_Table_egm_meters_by_period]
AS
BEGIN
  BEGIN TRY
    SET NOCOUNT ON;
    -------------------------
    --UPDATE [egm_meters_by_period] Treatment
    -------------------------
    UPDATE dest
    SET dest.emp_status        = ISNULL(src.emp_status, 0)
       ,dest.emp_mc_0000       = ISNULL(src.emp_mc_0000, 0)
       ,dest.emp_mc_0001       = ISNULL(src.emp_mc_0001, 0)
       ,dest.emp_mc_0002       = ISNULL(src.emp_mc_0002, 0)
       ,dest.emp_mc_0005       = ISNULL(src.emp_mc_0005, 0)
       ,dest.emp_mc_0000_increment   = ISNULL(src.emp_mc_0000_increment, 0)
       ,dest.emp_mc_0001_increment   = ISNULL(src.emp_mc_0001_increment, 0)
       ,dest.emp_mc_0002_increment   = ISNULL(src.emp_mc_0002_increment, 0)
       ,dest.emp_mc_0005_increment   = ISNULL(src.emp_mc_0005_increment, 0)
    FROM egm_meters_tsmh_temporary_working_table src
    INNER JOIN egm_meters_by_period dest
      ON   src.emp_working_day       = dest.emp_working_day
      AND src.emp_site_id         = dest.emp_site_id
      AND src.emp_datetime         = dest.emp_datetime
      AND src.emp_terminal_id       = dest.emp_terminal_id
      AND src.emp_game_id         = dest.emp_game_id
      AND src.emp_denomination       = dest.emp_denomination
      AND src.emp_record_type       = dest.emp_record_type
      AND src.emp_sas_accounting_denom   = dest.emp_sas_accounting_denom
    -------------------------
    -- INSERT [egm_meters_by_period] Treatment
    -------------------------
    INSERT INTO egm_meters_by_period
      SELECT
        src.emp_working_day
         ,src.emp_site_id
         ,src.emp_terminal_id
         ,src.emp_game_id
         ,src.emp_denomination
         ,src.emp_datetime
         ,src.emp_record_type
         ,src.emp_sas_accounting_denom
         ,ISNULL(src.emp_last_created_datetime, GETDATE())
         ,ISNULL(src.emp_last_reported_datetime, GETDATE())
         ,ISNULL(src.emp_user_ignored, 0)
         ,src.emp_user_ignored_datetime
         ,src.emp_last_updated_user_id
         ,src.emp_last_updated_user_datetime
         ,ISNULL(src.emp_status, 0)
         ,ISNULL(src.emp_mc_0000_increment, 0)
         ,ISNULL(src.emp_mc_0001_increment, 0)
         ,ISNULL(src.emp_mc_0002_increment, 0)
         ,ISNULL(src.emp_mc_0005_increment, 0)
         ,ISNULL(src.emp_mc_0000, 0)
         ,ISNULL(src.emp_mc_0001, 0)
         ,ISNULL(src.emp_mc_0002, 0)
         ,ISNULL(src.emp_mc_0005, 0)
      FROM egm_meters_tsmh_temporary_working_table src
      LEFT JOIN egm_meters_by_period dest
        ON src.emp_working_day           = dest.emp_working_day
          AND src.emp_site_id         = dest.emp_site_id
          AND src.emp_datetime         = dest.emp_datetime
          AND src.emp_terminal_id       = dest.emp_terminal_id
          AND src.emp_game_id         = dest.emp_game_id
          AND src.emp_denomination       = dest.emp_denomination
          AND src.emp_record_type       = dest.emp_record_type
          AND src.emp_sas_accounting_denom   = dest.emp_sas_accounting_denom
      WHERE   dest.emp_working_day   IS NULL
        AND dest.emp_terminal_id   IS NULL
        AND dest.emp_site_id     IS NULL
        AND dest.emp_datetime     IS NULL;
    RETURN 0;
  END TRY
  BEGIN CATCH
    EXEC dbo.spErrorHandling
  END CATCH
END
GO

/* 
**********************************************************
sp_EGM_Meters_Fill_CTE_Temporary_Daily
**********************************************************
*/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EGM_Meters_Fill_CTE_Temporary_Daily]') AND type in (N'P'))
DROP PROCEDURE [dbo].[sp_EGM_Meters_Fill_CTE_Temporary_Daily]
GO

CREATE PROCEDURE [dbo].[sp_EGM_Meters_Fill_CTE_Temporary_Daily] 
  @param_int_datetime INT = NULL
AS
  BEGIN
    SET NOCOUNT ON;
    -- Insert statements for procedure here
    DECLARE @query          NVARCHAR(MAX);
    DECLARE @site_id        INT;
    DECLARE @is_multisite      BIT;

    SET @is_multisite       =      (SELECT gp_key_value FROM general_params WHERE gp_group_key = 'MultiSite'   AND gp_subject_key = 'IsCenter');
    SET @site_id         =      (SELECT gp_key_value FROM general_params WHERE gp_group_key = 'Site'     AND gp_subject_key = 'Identifier');
    
    SET @query = '  SELECT
      ';

    SET @query += '
          emd_working_day
        , emd_site_id
        , NULL
        , NULL
        , SUM(CASE WHEN emd_connected = 1 THEN 1 ELSE 0 END)
        , COUNT (DISTINCT te_terminal_id)
        , GETDATE()
        , NULL
        , NULL
      FROM 
        egm_meters_by_day 
      INNER JOIN terminals
        ON  te_type        =   1
                                AND te_terminal_id   = emd_terminal_id
                AND te_terminal_type  IN  (1, 3, 5)
                AND te_activation_date <= DATEADD(DAY, 2, dbo.Opening(0, CONVERT(DATETIME, ''' + CONVERT(VARCHAR(8), @param_int_datetime) + ''')))
                AND (
                      te_retirement_date IS NULL 
                    OR  te_retirement_date > CONVERT(DATETIME, ''' + CONVERT(VARCHAR(8), @param_int_datetime) + ''')
                  )
                  ';
          IF (@is_multisite=1)
    SET @query += 'AND te_site_id=emd_site_id
        ';

    SET @query += 'WHERE emd_working_day = ' + CONVERT(VARCHAR(8), @param_int_datetime) + '
          GROUP BY emd_working_day , emd_site_id';
    EXEC sp_executesql @query;
END
GO

/* 
**********************************************************
sp_EGM_Meters_Upsert_Table_egm_meters_daily
**********************************************************
*/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EGM_Meters_Upsert_Table_egm_meters_daily]') AND type in (N'P'))
 DROP PROCEDURE [dbo].[sp_EGM_Meters_Upsert_Table_egm_meters_daily]
GO

CREATE PROCEDURE [dbo].[sp_EGM_Meters_Upsert_Table_egm_meters_daily]
  @param_int_datetime INT
AS
BEGIN
  BEGIN TRY
    SET NOCOUNT ON;
    -- 0-. Variables Declaration & Setting.
    DECLARE @stored_call      VARCHAR(1000);
    DECLARE @temp_table TABLE
    (
        ed_working_day         INT     NULL
      , ed_site_id           INT     NULL
      , ed_status           TINYINT   NULL
      , ed_has_new_meters       BIT     NULL
      , ed_terminals_connected     INT     NULL
      , ed_terminals_number       INT     NULL
      , ed_last_updated_meters     DATETIME   NULL
      , ed_last_updated_user       INT     NULL
      , ed_last_updated_user_datetime DATETIME   NULL
    );

    IF OBJECT_ID('egm_daily_temporary_working_table') IS NOT NULL
    BEGIN
    DROP TABLE egm_daily_temporary_working_table
    END;

    SET @stored_call = 'dbo.sp_EGM_Meters_Fill_CTE_Temporary_Daily @param_int_datetime = ' + CONVERT(VARCHAR(8), @param_int_datetime);

    INSERT INTO @temp_table EXEC (@stored_call);

    -- 2-. Build a CTE as helper to insert current working day data into egm_meters_by_day_temporary_working_table
    WITH cte_temporary_daily (ed_working_day, ed_site_id, ed_status, ed_has_new_meters, ed_terminals_connected, ed_terminals_number, ed_last_updated_meters, ed_last_updated_user, ed_last_updated_user_datetime)
    AS
    (SELECT
        ed_working_day
         ,ed_site_id
         ,ed_status
         ,ed_has_new_meters
         ,ed_terminals_connected
         ,ed_terminals_number
         ,ed_last_updated_meters
         ,ed_last_updated_user
         ,ed_last_updated_user_datetime
      FROM @temp_table)
    SELECT
      ed_working_day
       ,ed_site_id
       ,ed_status
       ,ed_has_new_meters
       ,ed_terminals_connected
       ,ed_terminals_number
       ,ed_last_updated_meters
       ,ed_last_updated_user
       ,ed_last_updated_user_datetime 
    INTO egm_daily_temporary_working_table
    FROM cte_temporary_daily
    ORDER BY ed_working_day DESC
    -------------------------
    -- 3-. UPDATE [egm_meters_by_period] Treatment
    -------------------------
    UPDATE dest
    SET dest.ed_has_new_meters     = 1
       ,dest.ed_last_updated_meters = src.ed_last_updated_meters
       ,dest.ed_terminals_number   = src.ed_terminals_number
    FROM egm_daily_temporary_working_table src
    INNER JOIN egm_daily dest
      ON src.ed_working_day  = dest.ed_working_day
      AND src.ed_site_id     = dest.ed_site_id
    -------------------------
    -- 4-. INSERT [egm_meters_by_period] Treatment
    -------------------------
    INSERT INTO egm_daily
      SELECT
        src.ed_working_day
         ,src.ed_site_id
         ,0
         ,1
         ,0
         ,src.ed_terminals_number
         ,src.ed_last_updated_meters
         ,NULL
         ,NULL
         ,0
      FROM egm_daily_temporary_working_table   src
      LEFT JOIN egm_daily           dest
        ON  src.ed_site_id     = dest.ed_site_id
        AND src.ed_working_day   = dest.ed_working_day
      WHERE dest.ed_working_day   IS NULL
      AND dest.ed_site_id     IS NULL;
  END TRY
  BEGIN CATCH
    EXEC dbo.spErrorHandling
  END CATCH
END
GO



IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_AGG_ReportProfitByMonth]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_AGG_ReportProfitByMonth]
GO

CREATE  PROCEDURE [dbo].[SP_AGG_ReportProfitByMonth]
  @pSites        NVARCHAR(256),
  @pFrom         BIGINT,
  @pTo           BIGINT,
  @pOnlyClosed   BIT,
  @pIncrement    MONEY = NULL
AS
BEGIN
  DECLARE @_QUERY  AS NVARCHAR(MAX);
  DECLARE @_DAILY_STATUS  AS NVARCHAR(MAX);

  SET @_DAILY_STATUS = '';

  IF @pOnlyClosed = 1
  BEGIN
    SET @_DAILY_STATUS = ' AND ED_STATUS = 1 ';
  END

  SET @_QUERY = '
       SELECT  EMD_SITE_ID AS SITE
             , (SELECT AVG(ED_TERMINALS_CONNECTED) FROM EGM_DAILY WHERE ED_WORKING_DAY >= ' + CAST(@pFrom AS NVARCHAR) + ' AND ED_WORKING_DAY < ' + CAST(@pTo AS NVARCHAR) + ' AND ED_SITE_ID IN ( ' + @pSites + ') ' + @_DAILY_STATUS + ') AS TERMINALS_CONNECTED 
             , (SELECT AVG(ED_TERMINALS_NUMBER)    FROM EGM_DAILY WHERE ED_WORKING_DAY >= ' + CAST(@pFrom AS NVARCHAR) + ' AND ED_WORKING_DAY < ' + CAST(@pTo AS NVARCHAR) + ' AND ED_SITE_ID IN ( ' + @pSites + ') ' + @_DAILY_STATUS + ') AS TERMINALS_NUMBER
             , SUM(EMD_MC_0000_INCREMENT) AS BETS
             , SUM(EMD_MC_0001_INCREMENT + EMD_MC_0002_INCREMENT) AS PAYMENTS
             , SUM(EMD_MC_0000_INCREMENT - EMD_MC_0001_INCREMENT - EMD_MC_0002_INCREMENT) AS PROFIT           
             , CASE WHEN SUM(EMD_MC_0000_INCREMENT) > 0 
                    THEN (SUM(EMD_MC_0001_INCREMENT + EMD_MC_0002_INCREMENT) / SUM(EMD_MC_0000_INCREMENT))*100
                    ELSE 0 END AS PAYOUT
             , DATEPART(YEAR,CONVERT(CHAR(10), EMD_WORKING_DAY, 120)) * 10000 +                                   
               DATEPART(MONTH,CONVERT(CHAR(10), EMD_WORKING_DAY, 120)) * 100 + 1 AS WORKING_DAY           
        FROM   EGM_METERS_BY_DAY 
  INNER JOIN   EGM_DAILY ON ED_WORKING_DAY = EMD_WORKING_DAY AND ED_SITE_ID = EMD_SITE_ID ' + @_DAILY_STATUS + '
       WHERE   EMD_WORKING_DAY >= ' + CAST(@pFrom AS NVARCHAR) + ' 
         AND   EMD_WORKING_DAY < ' + CAST(@pTo AS NVARCHAR) + '
         AND   EMD_SITE_ID IN ( ' + @pSites + ')
    GROUP BY   EMD_SITE_ID, DATEPART(YEAR, CONVERT(char(10), EMD_WORKING_DAY, 120)) * 10000 + DATEPART(MONTH, CONVERT(char(10), EMD_WORKING_DAY, 120)) * 100 + 1 '
    
  IF @pIncrement IS NOT NULL
  BEGIN  
  SET @_QUERY = @_QUERY + '
      HAVING   SUM(EMD_MC_0000_INCREMENT - EMD_MC_0001_INCREMENT - EMD_MC_0002_INCREMENT) > ' + CAST(@pIncrement AS NVARCHAR) + ' '
  END

  SET @_QUERY = @_QUERY + '
  ORDER BY EMD_SITE_ID ASC, DATEPART(YEAR, CONVERT(char(10), EMD_WORKING_DAY, 120)) * 10000 + DATEPART(MONTH, CONVERT(char(10), EMD_WORKING_DAY, 120)) * 100 + 1 ASC '

  EXEC (@_QUERY)
END
GO

GRANT EXECUTE ON [dbo].[SP_AGG_ReportProfitByMonth] TO [wggui] WITH GRANT OPTION
GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_AGG_ReportProfitByDay]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_AGG_ReportProfitByDay]
GO

CREATE  PROCEDURE [dbo].[SP_AGG_ReportProfitByDay]
  @pSites        NVARCHAR(256),
  @pFrom         BIGINT,
  @pTo           BIGINT,
  @pOnlyClosed   BIT,
  @pIncrement    MONEY = NULL
AS
BEGIN
  DECLARE @_QUERY  AS NVARCHAR(MAX);

  SET @_QUERY = '
      SELECT   EMD_SITE_ID AS SITE           
             , ED_TERMINALS_CONNECTED AS TERMINALS_CONNECTED
             , ED_TERMINALS_NUMBER AS TERMINALS_NUMBER
             , SUM(EMD_MC_0000_INCREMENT) AS BETS
             , SUM(EMD_MC_0001_INCREMENT + EMD_MC_0002_INCREMENT) AS PAYMENTS
             , SUM(EMD_MC_0000_INCREMENT - EMD_MC_0001_INCREMENT - EMD_MC_0002_INCREMENT) AS PROFIT           
             , CASE WHEN SUM(EMD_MC_0000_INCREMENT) > 0 
                    THEN (SUM(EMD_MC_0001_INCREMENT + EMD_MC_0002_INCREMENT) / SUM(EMD_MC_0000_INCREMENT))*100
                    ELSE 0 END AS PAYOUT
             , EMD_WORKING_DAY AS WORKING_DAY                  
        FROM   EGM_METERS_BY_DAY 
  INNER JOIN   EGM_DAILY ON ED_WORKING_DAY = EMD_WORKING_DAY AND ED_SITE_ID = EMD_SITE_ID '

  IF @pOnlyClosed = 1
  BEGIN
  SET @_QUERY = @_QUERY + '
   AND ED_STATUS = 1 '
  END

  SET @_QUERY = @_QUERY + '
       WHERE   EMD_WORKING_DAY >= ' + CAST(@pFrom AS NVARCHAR) + ' 
         AND   EMD_WORKING_DAY < ' + CAST(@pTo AS NVARCHAR) + '
         AND   EMD_SITE_ID IN ( ' + @pSites + ')
    GROUP BY   EMD_WORKING_DAY, EMD_SITE_ID, ED_TERMINALS_CONNECTED, ED_TERMINALS_NUMBER '
    
  IF @pIncrement IS NOT NULL
  BEGIN  
  SET @_QUERY = @_QUERY + '
      HAVING   SUM(EMD_MC_0000_INCREMENT - EMD_MC_0001_INCREMENT - EMD_MC_0002_INCREMENT) > ' + CAST(@pIncrement AS NVARCHAR) + ' '
  END

  SET @_QUERY = @_QUERY + '
  ORDER BY EMD_SITE_ID ASC, EMD_WORKING_DAY ASC'

  EXEC (@_QUERY)
END
GO

GRANT EXECUTE ON [dbo].[SP_AGG_ReportProfitByDay] TO [wggui] WITH GRANT OPTION
GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_AGG_ReportProfitByMachineDay]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_AGG_ReportProfitByMachineDay]
GO

CREATE  PROCEDURE [dbo].[SP_AGG_ReportProfitByMachineDay]
  @pSites        NVARCHAR(256),
  @pTerminals    NVARCHAR(4000) = NULL,
  @pFrom         BIGINT,
  @pTo           BIGINT,
  @pOnlyClosed   BIT,
  @pIncrement    MONEY = NULL
AS
BEGIN
  DECLARE @_QUERY  AS NVARCHAR(MAX);
  DECLARE @_THEORIAL_PAYOUT  AS NVARCHAR(3);

  SELECT @_THEORIAL_PAYOUT = ISNULL(GP_KEY_VALUE, 95) FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PlayerTracking' AND GP_SUBJECT_KEY = 'TerminalDefaultPayout'

  SET @_QUERY = '
      SELECT   EMD_SITE_ID AS SITE
             , TE_NAME AS TERMINAL_NAME
             , TE_GAME_THEME AS GAME
             , TT_NAME AS TERMINAL_TYPE
             , CASE WHEN SUM(EMD_MC_0005_INCREMENT) > 0 
                    THEN SUM(EMD_MC_0000_INCREMENT) / SUM(EMD_MC_0005_INCREMENT)
                    ELSE 0 END AS BET_AVG           
             , SUM(EMD_MC_0000_INCREMENT) AS BETS
             , SUM(EMD_MC_0001_INCREMENT) AS PAYMENTS
             , SUM(EMD_MC_0002_INCREMENT) AS JACKPOTS
             , EMD_WORKING_DAY AS WORKING_DAY
             , SUM(EMD_MC_0000_INCREMENT - EMD_MC_0001_INCREMENT - EMD_MC_0002_INCREMENT) AS PROFIT           
             , SUM(EMD_MC_0000_INCREMENT - EMD_MC_0001_INCREMENT - EMD_MC_0002_INCREMENT) AS PROFIT_AVG           
             , CASE WHEN SUM(EMD_MC_0000_INCREMENT) > 0 
                    THEN (SUM(EMD_MC_0001_INCREMENT + EMD_MC_0002_INCREMENT) / SUM(EMD_MC_0000_INCREMENT))*100
                    ELSE 0 END AS PAYOUT
             , ISNULL(TE_THEORETICAL_PAYOUT * 100, ' + @_THEORIAL_PAYOUT + ') AS THEORICAL_PAYOUT
         , SUM(EMD_MC_0005_INCREMENT) AS NUM_PLAYS
        FROM   EGM_METERS_BY_DAY '

  IF @pOnlyClosed = 1
  BEGIN
  SET @_QUERY = @_QUERY + '
  INNER JOIN   EGM_DAILY ON ED_WORKING_DAY = EMD_WORKING_DAY AND ED_SITE_ID = EMD_SITE_ID AND ED_STATUS = 1 '
  END
        
  SET @_QUERY = @_QUERY + '
  INNER JOIN   TERMINALS ON TE_TERMINAL_ID = EMD_TERMINAL_ID AND TE_SITE_ID = EMD_SITE_ID '

  IF @pTerminals IS NOT NULL
  BEGIN
  SET @_QUERY = @_QUERY + '
  AND TE_TERMINAL_ID IN ( ' + @pTerminals + ' ) '
  END

  SET @_QUERY = @_QUERY + '
  INNER JOIN   TERMINAL_TYPES ON TT_TYPE = TE_TYPE
       WHERE   EMD_WORKING_DAY >= ' + CAST(@pFrom AS NVARCHAR) + ' 
         AND   EMD_WORKING_DAY < ' + CAST(@pTo AS NVARCHAR) + '
         AND   EMD_SITE_ID IN ( ' + @pSites + ')
    GROUP BY   EMD_SITE_ID, EMD_WORKING_DAY, TE_NAME, TE_GAME_THEME, TT_NAME, TE_THEORETICAL_PAYOUT '
    
  IF @pIncrement IS NOT NULL
  BEGIN  
  SET @_QUERY = @_QUERY + '  
      HAVING   SUM(EMD_MC_0000_INCREMENT - EMD_MC_0001_INCREMENT - EMD_MC_0002_INCREMENT) > ' + CAST(@pIncrement AS NVARCHAR) + ' '
  END

  SET @_QUERY = @_QUERY + '
  ORDER BY EMD_SITE_ID ASC, TE_NAME ASC, EMD_WORKING_DAY ASC'

  EXEC (@_QUERY)

END
GO

GRANT EXECUTE ON [dbo].[SP_AGG_ReportProfitByMachineDay] TO [wggui] WITH GRANT OPTION
GO

/* Stored Procedure EnableTriggerByNameAndTable */
IF  EXISTS ( SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EnableTriggerByNameAndTable]') AND type IN (N'P', N'PC'))
  DROP PROCEDURE [dbo].[EnableTriggerByNameAndTable]
GO

CREATE PROCEDURE [dbo].[EnableTriggerByNameAndTable] 
   @TableName AS VARCHAR(150)
 , @ProcName AS VARCHAR(200)
 , @Enable AS BIT
AS
  BEGIN
    BEGIN TRY
      SET NOCOUNT ON;
      DECLARE @sql NVARCHAR(MAX);
      IF @Enable = 1 
        BEGIN
          SET @sql = 'ENABLE  TRIGGER ' + @ProcName + '    ON ' + @TableName + ';'
        END
      ELSE 
        BEGIN
          SET @sql = 'DISABLE  TRIGGER ' + @ProcName + '    ON ' + @TableName + ';'
        END
      IF EXISTS (SELECT * FROM sys.objects WHERE type = 'TR' AND name = @ProcName)
      EXEC sp_executesql @sql;
    END TRY
    BEGIN CATCH
      EXEC dbo.spErrorHandling
    END CATCH
  END
GO

/* Stored Procedure EnableTriggers_By_Service */
IF  EXISTS ( SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EnableTriggers_By_Service]') AND type IN (N'P', N'PC'))
  DROP PROCEDURE [dbo].[EnableTriggers_By_Service]
GO

CREATE PROCEDURE [dbo].[EnableTriggers_By_Service] 
 @ServiceName VARCHAR(100)
AS
  BEGIN
  DECLARE @trigger_status_text VARCHAR(100);
  DECLARE @service_exists BIT = 0;
    BEGIN TRY
      SET NOCOUNT ON;
      /* InHouseAPI */
      IF(@ServiceName = 'InHouseAPI')
        BEGIN
          DECLARE @INHOUSEAPI BIT = ISNULL(( SELECT gp_key_value FROM general_params WHERE gp_group_key = 'InHouseAPI' AND gp_subject_key = 'Enabled'), 0);
          SET @service_exists = 1;
          EXEC EnableTriggerByNameAndTable @TableName = N'PLAY_SESSIONS', @ProcName = N'InHouseAPI_Play_Sessions_Insert', @Enable = @INHOUSEAPI
          EXEC EnableTriggerByNameAndTable @TableName = N'CUSTOMER_VISITS', @ProcName = N'InHouseAPI_Customer_Visits_Insert', @Enable = @INHOUSEAPI
          /* Action Performed Message  */
          IF @INHOUSEAPI = 1
            SET @trigger_status_text = ' :: Triggers Enabled';
          ELSE
            SET @trigger_status_text = ' :: Triggers Disabled';
        END
      /* AGG */
      /*ELSE IF(@ServiceName = 'AGG')
        BEGIN
          DECLARE @AGG BIT = ISNULL((SELECT gp_key_value FROM general_params WHERE gp_group_key = 'AGG' AND gp_subject_key = 'Buffering.Enabled'), 0);
          SET @service_exists = 1;
          EXEC EnableTriggerByNameAndTable @TableName = N'ACCOUNTS', @ProcName = N'MultiSiteTrigger_SiteAccountUpdate', @Enable = @AGG
          EXEC EnableTriggerByNameAndTable @TableName = N'ACCOUNT_MOVEMENTS', @ProcName = N'Trigger_SiteToMultiSite_Points', @Enable = @AGG
          EXEC EnableTriggerByNameAndTable @TableName = N'ACCOUNT_DOCUMENTS', @ProcName = N'MultiSiteTrigger_SiteAccountDocuments', @Enable = @AGG
          /* Action Performed Message  */
          IF @AGG = 1
            SET @trigger_status_text = ' :: Triggers Enabled';
          ELSE
            SET @trigger_status_text = ' :: Triggers Disabled';
        END*/
      /* Future Services Added Section ...*/
      /*
      ----------------------------------------------------------------------------------------
      ELSE IF(@ServiceName = 'XXX')
        BEGIN
          IF @XXX = 1
            SET @trigger_status_text = ' :: Triggers Enabled';
          ELSE
            SET @trigger_status_text = ' :: Triggers Disabled';        
        END
      ----------------------------------------------------------------------------------------
      */
      IF (@service_exists = 1)
        SELECT @ServiceName + @trigger_status_text;
      ELSE
        SELECT 'ServiceName "' + @ServiceName + '" not found!';
    END TRY
    BEGIN CATCH
      EXEC dbo.spErrorHandling
    END CATCH
  END
GO

/* 
**********************************************************
sp_Fix_Working_day_row_last_time_zone
**********************************************************
 */
 IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Fix_Working_day_row_last_time_zone]') AND TYPE IN (N'P'))
  DROP PROCEDURE [dbo].[sp_Fix_Working_day_row_last_time_zone]
GO

CREATE PROCEDURE [dbo].[sp_Fix_Working_day_row_last_time_zone]
  @datetime_working_day    DATETIME,
  @datetime_working_day_int  INT OUTPUT
AS
BEGIN
  DECLARE @datetime_param AS DATETIME;
  SET @datetime_param = @datetime_working_day;

  DECLARE @hours INT;
  DECLARE @minutes INT;
  DECLARE @ClosingTimeHour INT;
  DECLARE @ClosingTimeMinutes INT;
  DECLARE @WorkingDayToBeReturned INT;
  /* Hour & Minutes Of Closing Time General Param */
  SET @ClosingTimeHour = ISNULL((SELECT gp_key_value FROM general_params WHERE gp_group_key = 'WigosGUI' AND gp_subject_key = 'ClosingTime'),               8);
  SET @ClosingTimeMinutes = ISNULL((SELECT gp_key_value FROM general_params WHERE gp_group_key = 'WigosGUI' AND gp_subject_key = 'ClosingTimeMinutes'), 0);

  /* Hour & Minutes Of Parameter Reveived Datetime */
  SET @hours           = DATEPART(HOUR,     @datetime_param);
  SET @minutes  = DATEPART(MINUTE,   @datetime_param);

  /* Lògica per decidir si és o no jornada actual o anterior si quadra l'hora rebuda. */
  IF ((@hours = @ClosingTimeHour) AND (@minutes = @ClosingTimeMinutes))
  BEGIN         
    SET @WorkingDayToBeReturned = dbo.GamingDayFromDateTime(DATEADD(DAY,-1,@datetime_param));
    --SELECT 'L''hora és igual que la del ClosingTime, s''ha de restar 1 a la jornada'
  END
  ELSE
  BEGIN
    SET @WorkingDayToBeReturned = dbo.GamingDayFromDateTime(@datetime_param);
    --SELECT 'No és l''hora del ClosingTime, la jornada ja és la correcta!!';
  END

  SELECT @datetime_working_day_int = @WorkingDayToBeReturned;

END
GO


/* 
**********************************************************
sp_EGM_Meters_Upsert_Table_egm_meters_max_value
**********************************************************
 */
/*
----------------------------------------------------------------------------------------------------------------
Stored Procedure: 'sp_EGM_Meters_Upsert_Table_egm_meters_max_value'.

New auxiliar stored procedure to be able to inform 'egm_meters_max_value' table witch is needed to 
detect tsmh_sas_accounting_denom modifications and in runtime of job apply right value when is modified.

Version     Date          User      Description
----------------------------------------------------------------------------------------------------------------
1.0.0       02-FEB-2018   OMC       New procedure

Requeriments:

Parameters:
  -- @param_datetime DATETIME
*/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EGM_Meters_Upsert_Table_egm_meters_max_value]') AND TYPE IN (N'P'))
  DROP PROCEDURE [dbo].[sp_EGM_Meters_Upsert_Table_egm_meters_max_value]
GO

CREATE PROCEDURE [dbo].[sp_EGM_Meters_Upsert_Table_egm_meters_max_value]
  @param_site_id INT,
  @param_datetime DATETIME
AS
BEGIN
  BEGIN TRY
    SET NOCOUNT ON;
        
    -------------------------  
    -- 1-. Variables Declaration.
    -------------------------  
    DECLARE @stored_call      VARCHAR(1000);
    DECLARE @site_id        INT
    DECLARE @sas_meters_interval  INT
    DECLARE @tsmh_period_obtained   TABLE(
        tsmh_site_id           BIGINT     NULL
      , tsmh_terminal_id         INT     NULL
      , tsmh_meter_code         INT     NULL
      , tsmh_game_id           INT     NULL
      , tsmh_denomination       MONEY     NULL
      , tsmh_type           INT     NULL
      , tsmh_datetime         DATETIME   NULL
      , tsmh_meter_ini_value       BIGINT     NULL
      , tsmh_meter_fin_value       BIGINT     NULL
      , tsmh_meter_increment       BIGINT     NULL
      , tsmh_raw_meter_increment     BIGINT     NULL
      , tsmh_last_reported       DATETIME    NULL
      , tsmh_sas_accounting_denom   MONEY     NULL
      , tsmh_created_datetime     DATETIME   NULL
      , tsmh_group_id         BIGINT     NULL
      , tsmh_meter_origin       INT     NULL
      , tsmh_meter_max_value       BIGINT     NULL
    );

    -------------------------  
    -- 1-. Variables/Params Set
    -------------------------  
    SET @stored_call = 'dbo.sp_EGM_Meters_Get_TSMH_Period_Data @param_site_id = ''' + CONVERT(VARCHAR(100),@param_site_id) + ''', @param_datetime = ''' + CONVERT(VARCHAR(100), @param_datetime, 120) + '''';

    -------------------------  
    -- 2-. Building CTE over TSMH with raw data for current execution.  
    -------------------------  
    INSERT INTO @tsmh_period_obtained EXEC (@stored_call);

    -------------------------
    -- 3-. UPDATE [egm_meters_max_values] Treatment
    -------------------------  
    UPDATE dest
    SET dest.emmv_max_value     = src.tsmh_meter_max_value
    FROM @tsmh_period_obtained src
    INNER JOIN egm_meters_max_values dest
      ON src.tsmh_site_id     = dest.emmv_site_id
      AND src.tsmh_terminal_id   = dest.emmv_terminal_id
      AND src.tsmh_meter_code   = dest.emmv_meter_code
      AND src.tsmh_game_id     = dest.emmv_game_id
      AND src.tsmh_denomination   = dest.emmv_denomination
      AND src.tsmh_type       = dest.emmv_type
      AND src.tsmh_datetime     = dest.emmv_datetime

    -------------------------
    -- 4-. INSERT [egm_meters_max_values] Treatment
    -------------------------
    INSERT INTO egm_meters_max_values
      SELECT
        src.tsmh_site_id         AS emmv_site_id
         ,src.tsmh_terminal_id       AS emmv_terminal_id
         ,src.tsmh_meter_code       AS emmv_meter_code
         ,src.tsmh_game_id         AS emmv_game_id
         ,src.tsmh_denomination       AS emmv_denomination
         ,src.tsmh_type           AS emmv_type
         ,src.tsmh_datetime         AS emmv_datetime
         ,src.tsmh_meter_max_value     AS emmv_max_value
      FROM @tsmh_period_obtained src
      LEFT JOIN egm_meters_max_values dest
        ON  src.tsmh_site_id       = dest.emmv_site_id
        AND src.tsmh_terminal_id     = dest.emmv_terminal_id
        AND src.tsmh_meter_code     = dest.emmv_meter_code
        AND src.tsmh_game_id       = dest.emmv_game_id
        AND src.tsmh_denomination     = dest.emmv_denomination
        AND src.tsmh_type         = dest.emmv_type
        AND src.tsmh_datetime       = dest.emmv_datetime
      WHERE dest.emmv_site_id     IS NULL
        AND dest.emmv_terminal_id   IS NULL
        AND dest.emmv_meter_code    IS NULL
        AND dest.emmv_game_id     IS NULL
        AND dest.emmv_denomination   IS NULL
        AND dest.emmv_type       IS NULL
        AND dest.emmv_datetime    IS NULL
        AND src.tsmh_type       IN (11)
        AND src.tsmh_meter_code     IN (0, 1, 2);
  RETURN 0;
  END TRY
  BEGIN CATCH
    EXEC dbo.spErrorHandling
  END CATCH
END
GO


/* 
**********************************************************
sp_EGM_Meters_Get_Interval
**********************************************************
 */
 IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EGM_Meters_Get_Interval]') AND TYPE IN (N'P'))
  DROP PROCEDURE [dbo].[sp_EGM_Meters_Get_Interval]
GO

CREATE PROCEDURE [dbo].[sp_EGM_Meters_Get_Interval] 
(
  @param_site_id INT,  
  @param_interval INT OUTPUT
)
AS
BEGIN
  BEGIN TRY
    SET NOCOUNT ON;

    DECLARE @IntervalBilling   AS INT
    DECLARE @SasMetersInterval AS INT
    DECLARE @DatetimeCM        AS DATETIME
    DECLARE @DatetimeJob       AS DATETIME

    SET @IntervalBilling = ISNULL((SELECT gp_key_value FROM general_params WHERE gp_group_key = 'WCP' AND gp_subject_key = 'IntervalBilling'), 60)
    SET @SasMetersInterval = ISNULL((SELECT gp_key_value FROM general_params WHERE gp_group_key = 'WCP' AND gp_subject_key = 'SasMetersInterval'), 60)
    SELECT @DatetimeCM = ecm_control_mark FROM egm_control_mark WHERE ecm_site_id = @param_site_id

        SELECT   @DatetimeJob = msdb.dbo.agent_datetime(last_run_date,last_run_time) 
          FROM   msdb.dbo.sysjobs j
    INNER JOIN   msdb.dbo.sysjobsteps js ON j.job_id = js.job_id         
         WHERE   j.name LIKE 'jb_Egm_Meters_Reporting_' + DB_NAME()

    SET @param_interval = CASE WHEN ABS(DATEDIFF(minute, @DatetimeCM, @DatetimeJob)) > @SasMetersInterval THEN @SasMetersInterval
                               WHEN ABS(DATEDIFF(minute, @DatetimeCM, @DatetimeJob)) < @SasMetersInterval AND 
                                    ABS(DATEDIFF(minute, @DatetimeCM, @DatetimeJob)) > @IntervalBilling THEN ABS(DATEDIFF(minute, @DatetimeCM, @DatetimeJob))
                               ELSE @IntervalBilling
                          END

    RETURN 0;
  END TRY
  BEGIN CATCH
    EXEC dbo.spErrorHandling
  END CATCH
END
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Get_MSLotery_TerminalMaster]') AND TYPE IN (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Get_MSLotery_TerminalMaster]
GO

CREATE PROCEDURE [dbo].[Get_MSLotery_TerminalMaster] @SiteId Int
AS
BEGIN
  DECLARE @DefaultPayout AS Money
  DECLARE @pLanguageID AS Int
  DECLARE @QuantityPuestos AS Int
  DECLARE @QuantityEGMAccumulate AS Int
  DECLARE @QuantityEGMAccumulate_WithoutMultiPuesto AS Int
  DECLARE @QuantityProgresives AS Int
  Declare @QuantitySeats as int
  --DECLARE  @SiteId as int
  --set @SiteId = 042



  -- Total number of terminals (puestos) 
  set @QuantityPuestos = (select count(1) from terminals WHERE   te_terminal_type	IN (1, 3, 5, 108)  
                                and  te_status = 0 
                                and  te_site_id         = @SiteId)

  -- Total number of terminals without Ruletas Multipuesto and Terminals that  have Progresives without multipuesto. That is to say, progresive.multiseat = null or 0
  set @QuantityEGMAccumulate_WithoutMultiPuesto = (select count(1) from    terminals 
                            left   join	 (select	tg_terminal_id, Max(tg_element_id ) as tg_element_id 
                                            from	terminal_groups tgs 
                                             where	tg_element_type			= 6	AND tgs.tg_site_id         = @SiteId
                                             group    by tgs.tg_terminal_id) as tg 
                                      on   tg.tg_terminal_id  = te_terminal_id 												  																	  
                            left   JOIN progressives p	ON	p.pgs_progressive_id	= tg.tg_element_id 
                                           AND  p.pgs_site_id = @SiteId
                            WHERE    te_terminal_type	IN (1, 3, 5, 108)
                              AND    te_multiseat_id is null  -- Without  rulets													  
                                and    te_status  = 0
                              and    te_site_id = @SiteId
                              and    (pgs_multiseat = 0 or pgs_multiseat is null ))   -- Without  progresives Multipuesto /*progresive compute as "machine"*/))
                              
  -- Total number of Multipuesto-progresives 
  set @QuantityProgresives = (select Count(distinct(pgs_progressive_id)) from    terminals 
                              left   join	 (select	tg_terminal_id, Max(tg_element_id ) as tg_element_id 
                                            from	terminal_groups tgs 
                                             where	tg_element_type			= 6	AND tgs.tg_site_id         = @SiteId
                                             group    by tgs.tg_terminal_id) as tg 
                                      on   tg.tg_terminal_id  = te_terminal_id 												  																	  
                                left   JOIN progressives p	ON	p.pgs_progressive_id	= tg.tg_element_id 
                                               AND  p.pgs_site_id = @SiteId
                              WHERE    te_terminal_type	IN (1, 3, 5, 108)
                              AND    te_multiseat_id is null  -- Without  rulets
                              And    tg.tg_terminal_id  is not null -- With  progresives
                                and    te_status = 0
                              and    PGS_STATUS = 1-- progresives enabled
                              and  te_site_id         = @SiteId
                              and    (pgs_multiseat = 1)   /*progresive compute as "puesto"*/)
  -- Total number of Multipuesto-ruletes. 
  set @QuantitySeats = (select Count(distinct(te_multiseat_id)) from    terminals 
                            WHERE    te_terminal_type	IN (1, 3, 5, 108)
                              AND    te_multiseat_id is not null  -- With  rulets
                                and    te_status = 0 
                              and  te_site_id         = @SiteId)

  set @QuantityEGMAccumulate =  @QuantityEGMAccumulate_WithoutMultiPuesto + @QuantityProgresives + @QuantitySeats


  SET @DefaultPayout = (SELECT 1 - CAST (GP_KEY_VALUE AS MONEY) 
  FROM GENERAL_PARAMS
  WHERE GP_GROUP_KEY = 'PlayerTracking'
   AND GP_SUBJECT_KEY = 'TerminalDefaultPayout')
   
  SELECT @pLanguageID = CASE UPPER(gp_key_value)
      WHEN 'ES' THEN 9
      ELSE 10 END
  FROM GENERAL_PARAMS
  WHERE GP_GROUP_KEY = 'WigosGUI'
   AND GP_SUBJECT_KEY = 'Language'

  SELECT 
   te.te_base_name										AS '[Terminal]'
   , substring(te.te_base_name,0,4)+ '-'+   substring(te.te_base_name,5,5) AS '[ID de Planta]'  /*te.te_floor_id	*/
   , te.te_external_id								AS '[ID protocolo]'
   , te.te_provider_id								AS '[Fabricante]'
   , te.te_cabinet_type								AS '[Tipo de gabinete]'
   , isnull(te.te_game_theme, '')									AS '[Juego]'
   , te.te_program									AS '[Programa]' 
   , te.te_serial_number								AS '[Numero de serie]' 
   , te.te_sas_accounting_denom						AS '[Denominacion contable]'
   , te.te_multi_denomination						    as '[Multidenominacion]'
   , ISNULL(te.te_theoretical_payout*100, @DefaultPayout)		AS '[% de payout]'
   --, te.te_number_lines								AS '[Numero de linias]'
   --, te.te_contract_id								AS '[ID contrato]'
   , gt.gt_name										AS '[Tipo de juego]'
   , ci.cai_name										AS '[Ruleta]'
   , p.pgs_name										AS '[Progresivo]'  
   , p.pgs_brand										as '[Marca]'
   , p.pgs_server										as '[Server]'
   , IsNull(RighT('0' + 	cast(te.te_manufacture_day    as nvarchar(2)),2) + '/'+ 
          RighT('0' + 	cast(te.te_manufacture_month  as nvarchar(2)),2) + '/'+ 	
                        cast(te.te_manufacture_year   as nvarchar(4))
    ,'') 	AS '[Fabricacion]'
  FROM terminals te
   INNER JOIN game_types	gt							ON te.te_game_type			= gt.gt_game_type			AND gt_language_id		= @pLanguageID
   left  JOIN catalog_items ci						ON ci.cai_id				= te.te_multiseat_id		and cai_site_id = @SiteId
   left  JOIN catalogs c								ON c.cat_id					= ci.cai_catalog_id			and cat_site_id = @SiteId
   left   join	 (select	tg_terminal_id, Max(tg_element_id ) as tg_element_id 
                                            from	terminal_groups tgs 
                                             where	tg_element_type			= 6	AND tgs.tg_site_id         = @SiteId
                                             group    by tgs.tg_terminal_id) as tg 
                on   tg.tg_terminal_id  = te_terminal_id 												  																	  
   left   JOIN progressives p	ON	p.pgs_progressive_id	= tg.tg_element_id 
                AND  p.pgs_site_id = @SiteId
   
  WHERE
         te.te_terminal_type	IN (1, 3, 5, 108)
   and  te.te_status = 0 
   and  te.te_site_id = @SiteId
   union  all
  (
   select     
    'TOTAL DE PUESTOS ' AS '[Terminal]'
   , cast(@QuantityPuestos as nvarchar(5)) AS '[ID de Planta]'
   , Null AS '[ID protocolo]'
   , Null AS '[Fabricante]'
   , Null AS '[Tipo de gabinete]'
   , Null AS '[Juego]'
   , Null AS '[Programa]' 
   , Null AS '[Numero de serie]' 
   , Null AS '[Denominacion contable]'
   , Null as '[Multidenominacion]'
   , Null AS '[% de payout]'
   --, Null AS '[Numero de linias]'
   --, Null AS '[ID contrato]'
   , Null AS '[Tipo de juego]'
   , Null AS '[Ruleta]'
   , Null AS '[Progresivo]' 
   , Null	as '[Marca]'
   , Null	as '[Server]'
   , Null	as '[Fabricacion]'
   )
    union  all
   (
   select  
      'TOTAL DE MÁQUINAS ' AS '[Terminal]'
   , cast ( @QuantityEGMAccumulate as nvarchar(5)) AS '[ID de Planta]'
   , Null AS '[ID protocolo]'
   , Null AS '[Fabricante]'
   , Null AS '[Tipo de gabinete]'
   , Null AS '[Juego]'
   , Null AS '[Programa]' 
   , Null AS '[Numero de serie]' 
   , Null AS '[Denominacion contable]'
   , Null as '[Multidenominacion]'
   , Null AS '[% de payout]'
   --, Null AS '[Numero de linias]'
   --, Null AS '[ID contrato]'
   , Null AS '[Tipo de juego]'
   , Null AS '[Ruleta]'
   , Null AS '[Progresivo]' 
   , Null	as '[Marca]'
   , Null	as '[Server]'
   , Null	as '[Fabricacion]'
   )
  -- Tab 2
  --//MULTIPUESTO - RULETAS
  --
  SELECT 
     ci.cai_name										AS '[Ruleta]'
   , te.te_name										AS '[Terminal]'
   , substring(te.te_base_name,0,4)+ '-'+   substring(te.te_base_name,5,5) AS '[ID de Planta]'  /*te.te_floor_id	*/
   , te.te_external_id								AS '[ID protocolo]'
   , te.te_provider_id								AS '[Fabricante]'
   , te.te_cabinet_type								AS '[Tipo de gabinete]'
   , te.te_vendor_id									AS '[Juego]'
   , te.te_program									AS '[Programa]' 
   , te.te_serial_number								AS '[Numero de serie]' 
   , te.te_sas_accounting_denom						AS '[Denominacion contable]'
   , ISNULL(te.te_theoretical_payout*100, @DefaultPayout)		AS '[% de payout]' 
   , gt.gt_name										AS '[Tipo de juego]'
  FROM terminals te
   INNER JOIN game_types	gt							ON te.te_game_type			= gt.gt_game_type		AND gt_language_id		= @pLanguageID
   left  JOIN catalog_items ci						ON ci.cai_id				= te.te_multiseat_id	and cai_site_id = @SiteId
   left JOIN catalogs c								ON c.cat_id					= ci.cai_catalog_id		and cat_site_id = @SiteId 
  WHERE
         te.te_terminal_type	IN (1, 3, 5, 108)
   and  te.te_status = 0 
   and  ci.cai_name is not null
   and  te.te_site_id = @SiteId
   order by 1

   --// Tab 3
   --// MULTIPUESTO - PROGRESIV
   --//
  SELECT  
            PGS_NAME             '[nombre]'     
      , pgs_brand				'[Marca]'        
      , pgs_server								'[Server]'   
      , isnull(TE_GAME_THEME,'')	   '[Juego]'
      , Case when PGS_STATUS = 1 then 'Habilitado' else 'Deshabilitado'	end		'[Status]'
      --, ''      
      , PV_NAME				'[Proveedor]'
      , TE_NAME				'[Terminal]'		
       FROM   PROGRESSIVES      
  LEFT JOIN		TERMINAL_GROUPS  ON		PGS_PROGRESSIVE_ID =  TG_ELEMENT_ID AND	TG_ELEMENT_TYPE = 6 AND TG_SITE_ID         = @SiteId
  LEFT JOIN		TERMINALS        ON		TE_TERMINAL_ID     = TG_TERMINAL_ID and TE_SITE_ID = @SiteId
  LEFT JOIN		PROVIDERS        ON		PV_ID = TE_PROV_ID  
  where pgs_site_id = @SiteId
    and pgs_multiseat = 1 -- MultiPuesto - Progresives
END
GO

GRANT EXECUTE ON [Get_MSLotery_TerminalMaster] TO [wggui] WITH GRANT OPTION
GO

----------------------------------------------------------------------------------------------------------
----               Report to get the benefits bu EGM                   ------
---- Author: Xavi Guirado                                      ------
---- Date: 23/Nov/2017                                        ------
----------------------------------------------------------------------------------------------------------  
/****** Object:  StoredProcedure [dbo].[sp_GetTerminals]    Script Date: 11/17/2017 13:58:38 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ReportBenefitsEGMperDay]') AND type in (N'P', N'PC'))
 DROP PROCEDURE [dbo].ReportBenefitsEGMperDay
GO

CREATE PROCEDURE [dbo].[ReportBenefitsEGMperDay]
 (@pFrom	DATETIME)
AS
BEGIN

DECLARE @tsmh_from DATETIME;
DECLARE @tsmh_to DATETIME;

SET @tsmh_from = DATEADD(DAY, -1, @pFrom);
SET @tsmh_to = @pFrom;

	  SELECT 
		tsmh_terminal_id																										AS Terminal,
		--MAX(Games.gm_name)																										AS Juego,
		''																														AS Juego,
		'C1'																													AS Tipo_C,
		SUM(CASE WHEN TSMH_METER_CODE = 0							THEN ISNULL(TSMH_METER_INI_VALUE, 0) ELSE 0 END)
			/ NULLIF(SUM (CASE WHEN TSMH_METER_CODE = 5				THEN ISNULL(TSMH_METER_INI_VALUE, 0) ELSE 0 END),0)			AS Promedio_Apuesta,
		SUM(CASE WHEN TSMH_METER_CODE = 0							THEN ISNULL(TSMH_METER_INI_VALUE, 0) ELSE 0 END)			AS Apuestas,
		SUM((CASE WHEN TSMH_METER_CODE = 1							THEN ISNULL(TSMH_METER_INI_VALUE, 0) ELSE 0 END)
				+ (CASE WHEN TSMH_METER_CODE = 2					THEN ISNULL(TSMH_METER_INI_VALUE, 0) ELSE 0 END))			AS Pagos,
		SUM(CASE WHEN TSMH_METER_CODE = 2							THEN ISNULL(TSMH_METER_INI_VALUE, 0) ELSE 0 END)			AS JackPot,		
		'1'																														AS Dias,	
		SUM(CASE WHEN TSMH_METER_CODE = 0							THEN ISNULL(TSMH_METER_INI_VALUE, 0) ELSE 0 END)
			- SUM((CASE WHEN TSMH_METER_CODE = 1					THEN ISNULL(TSMH_METER_INI_VALUE, 0) ELSE 0 END)
			+ (CASE WHEN TSMH_METER_CODE = 2						THEN ISNULL(TSMH_METER_INI_VALUE, 0) ELSE 0 END))			As Beneficios,
		SUM (CASE WHEN TSMH_METER_CODE = 0							THEN ISNULL(TSMH_METER_INI_VALUE, 0) ELSE 0 END)
			- SUM((CASE WHEN TSMH_METER_CODE = 1					THEN ISNULL(TSMH_METER_INI_VALUE, 0) ELSE 0 END)
			+ (CASE WHEN TSMH_METER_CODE = 2						THEN ISNULL(TSMH_METER_INI_VALUE, 0) ELSE 0 END))			AS Promedio_Diario,
		NULLIF(
		CONVERT(DECIMAL(16,2), SUM((CASE WHEN TSMH_METER_CODE = 1	THEN ISNULL(TSMH_METER_INI_VALUE, 0) ELSE 0 END)
			+ (CASE WHEN TSMH_METER_CODE = 2						THEN ISNULL(TSMH_METER_INI_VALUE, 0) ELSE 0 END))*100.00
			/ NULLIF(SUM(CASE WHEN TSMH_METER_CODE = 0				THEN ISNULL(TSMH_METER_INI_VALUE, 0) ELSE 0 END),0)),0)		AS Payout_Real,
		MAX(terminals.te_theoretical_payout)																					AS Payout_Teo,
		'0,00'																													AS Promocion,
		SUM (CASE WHEN TSMH_METER_CODE = 0							THEN ISNULL(TSMH_METER_INI_VALUE, 0) ELSE 0 END)
			- SUM((CASE WHEN TSMH_METER_CODE = 1					THEN ISNULL(TSMH_METER_INI_VALUE, 0) ELSE 0 END)
			+ (CASE WHEN TSMH_METER_CODE = 2						THEN ISNULL(TSMH_METER_INI_VALUE, 0) ELSE 0 END))			AS Benef_Real,
		SUM(CASE WHEN TSMH_METER_CODE = 0							THEN ISNULL(TSMH_METER_INI_VALUE, 0) ELSE 0 END)
			- SUM((CASE WHEN TSMH_METER_CODE = 1					THEN ISNULL(TSMH_METER_INI_VALUE, 0) ELSE 0 END)
			+ (CASE WHEN TSMH_METER_CODE = 2						THEN ISNULL(TSMH_METER_INI_VALUE, 0) ELSE 0 END))			AS Promo_Diaio_real	
		
	FROM terminal_sas_meters_history
	INNER JOIN terminals ON tsmh_terminal_id = terminals.te_terminal_id
--	LEFT JOIN games ON tsmh_game_id = games.gm_game_id

	WHERE 
			 TSMH_DATETIME >= @tsmh_from 
			AND TSMH_DATETIME <= @tsmh_to
	AND tsmh_meter_code IN (0,1,2,5)
	AND tsmh_type = 20

	GROUP BY tsmh_datetime, tsmh_site_id, tsmh_terminal_id

END -- ReporteBeneficioMaquinaPorDia
GO

GRANT EXECUTE ON ReportBenefitsEGMperDay TO [wggui] WITH GRANT OPTION
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Ganancias_Por_Mes]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Ganancias_Por_Mes]
GO

CREATE PROCEDURE [dbo].[Ganancias_Por_Mes]
	@pMonth			INT,
	@pYear			INT
AS
	BEGIN
		SET NOCOUNT ON;
		------------------------------------------------------------------------------
		-- DECLARATIONS.
		DECLARE @param_datetime							DATETIME;
		DECLARE @string_datetime						VARCHAR(100);
		DECLARE @tsmh_from								DATETIME;
		DECLARE @tsmh_from_days_to_substract			INT;
		DECLARE @tsmh_to								DATETIME;
		DECLARE @cantidad_de_puestos					INT;
		------------------------------------------------------------------------------
		-- SET INITIAL VALUES.
		SET @string_datetime = CONVERT(VARCHAR(4),@pYear)+'-'+CONVERT(VARCHAR(2),@pMonth)+'-01';
		SET @param_datetime = CONVERT(DATETIME,@string_datetime, 120);
		SET @tsmh_from_days_to_substract = DATEPART(DAY,@param_datetime)-1;
		SET @tsmh_from			= DATEADD(DAY, -@tsmh_from_days_to_substract, @param_datetime);
		SET @tsmh_to			= DATEADD(month, ((YEAR(@param_datetime) - 1900) * 12) + MONTH(@param_datetime), -1);
		SET @cantidad_de_puestos = (SELECT COUNT(TE_TERMINAL_ID) FROM TERMINALS);
		------------------------------------------------------------------------------
		SELECT 
				REPLACE(CONVERT(VARCHAR(11), tsmh_datetime, 105), '-', '/')																			AS 'Día'
			,	MAX(ST_NAME)																														AS 'Sala'
			,	COUNT (DISTINCT TSMH_TERMINAL_ID)																												AS 'Puestos_Operativos'
			,	@cantidad_de_puestos																												AS 'Cant_de_Puestos'
			,	SUM (CASE WHEN TSMH_METER_CODE = 0				THEN ISNULL(TSMH_METER_INI_VALUE, 0) ELSE 0 END )		 							AS 'Apostado'
			,	(   SUM (CASE WHEN TSMH_METER_CODE = 1			THEN ISNULL(TSMH_METER_INI_VALUE, 0) ELSE 0 END )
				  + SUM (CASE WHEN TSMH_METER_CODE = 2			THEN ISNULL(TSMH_METER_INI_VALUE, 0) ELSE 0 END ))									AS 'Pagado'
			,	(       SUM (CASE WHEN TSMH_METER_CODE = 0		THEN ISNULL(TSMH_METER_INI_VALUE, 0) ELSE 0 END )
				 - (	SUM (CASE WHEN TSMH_METER_CODE = 1		THEN ISNULL(TSMH_METER_INI_VALUE, 0) ELSE 0 END )
					+	SUM (CASE WHEN TSMH_METER_CODE = 2		THEN ISNULL(TSMH_METER_INI_VALUE, 0) ELSE 0 END )))									AS 'Ganancia' -- Ganancia = Apostado - Pagado
			,	CASE WHEN SUM (CASE WHEN TSMH_METER_CODE = 0				THEN ISNULL(TSMH_METER_INI_VALUE, 0) ELSE 0 END ) <> 0 THEN
					  CONVERT(DECIMAL(19,2), (( SUM (CASE WHEN TSMH_METER_CODE = 1				THEN ISNULL(TSMH_METER_INI_VALUE, 0) ELSE 0 END )
					+ SUM (CASE WHEN TSMH_METER_CODE = 2				THEN ISNULL(TSMH_METER_INI_VALUE, 0) ELSE 0 END )))
					/ CONVERT(DECIMAL, SUM (CASE WHEN TSMH_METER_CODE = 0				THEN ISNULL(TSMH_METER_INI_VALUE, 0) ELSE 0 END )) * 100.00)
				ELSE
					0.00
				END																																	AS 'Porcentaje_de_Pago' -- (Pagado/Apostado)*100
		 FROM		  TERMINAL_SAS_METERS_HISTORY
			LEFT JOIN TERMINALS 
				ON	TE_TERMINAL_ID	= TSMH_TERMINAL_ID
				AND	TE_SITE_ID	= TSMH_SITE_ID
			LEFT JOIN SITES		
				ON	ST_SITE_ID		= TE_SITE_ID 
				AND ST_SITE_ID		= TSMH_SITE_ID	
		 WHERE   
				TSMH_TYPE			= 20
			AND	TSMH_METER_CODE   IN (0,1,2,3,4,5,11,36,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,128,130,132,134,136)
			AND TSMH_DATETIME >= @tsmh_from 
			AND TSMH_DATETIME <= @tsmh_to
		 GROUP BY 
			  CONVERT(VARCHAR(11), tsmh_datetime, 105)
			, TSMH_SITE_ID
	END
GO

GRANT EXECUTE ON [Ganancias_Por_Mes] TO [wggui] WITH GRANT OPTION
GO




/******* TRIGGERS *******/


/****** GENERIC REPORT ***********/


BEGIN
  DECLARE @rtc_store_name  NVARCHAR(200)
  DECLARE @rtc_form_id INT
  DECLARE @rtc_design_sheet VARCHAR(MAX)
  DECLARE @rtc_design_filter VARCHAR(MAX)
  DECLARE @rtc_report_name VARCHAR(MAX)

  SET @rtc_store_name = 'ReportBenefitsEGMperDay'
  
  SET @rtc_report_name = '<LanguageResources><NLS09><Label>Report EGM Benefits</Label></NLS09><NLS10><Label>Reporte Beneficios por Terminal</Label></NLS10></LanguageResources>'
  
  SET @rtc_design_sheet = '<ArrayOfReportToolDesignSheetsDTO>
    <ReportToolDesignSheetsDTO>
    <LanguageResources>
      <NLS09>
      <Label>Report EGM Benefits</Label>
      </NLS09>
      <NLS10>
      <Label>Reporte de Beneficios Terminal</Label>
      </NLS10>
    </LanguageResources>
    <Columns>    
      <ReportToolDesignColumn>
      <Code>Terminal</Code>
      <Width>300</Width>
      <EquityMatchType>Equality</EquityMatchType>
      <LanguageResources>
        <NLS09>
        <Label>Terminal</Label>
        </NLS09>
        <NLS10>
        <Label>Terminal</Label>
        </NLS10>
      </LanguageResources>
      </ReportToolDesignColumn>   
      <ReportToolDesignColumn>
      <Code>Juego</Code>
      <Width>300</Width>
      <EquityMatchType>Equality</EquityMatchType>
      <LanguageResources>
        <NLS09>
        <Label>Game</Label>
        </NLS09>
        <NLS10>
        <Label>Juego</Label>
        </NLS10>
      </LanguageResources>
      </ReportToolDesignColumn>    
      <ReportToolDesignColumn>
      <Code>Tipo_C</Code>
      <Width>300</Width>
      <EquityMatchType>Equality</EquityMatchType>
      <LanguageResources>
        <NLS09>
        <Label>Type C</Label>
        </NLS09>
        <NLS10>
        <Label>Tipo C</Label>
        </NLS10>
      </LanguageResources>
      </ReportToolDesignColumn>     
      <ReportToolDesignColumn>
      <Code>Promedio_Apuesta</Code>
      <Width>300</Width>
      <EquityMatchType>Equality</EquityMatchType>
      <LanguageResources>
        <NLS09>
        <Label>Average bet</Label>
        </NLS09>
        <NLS10>
        <Label>Promodio Apuesta</Label>
        </NLS10>
      </LanguageResources>
      </ReportToolDesignColumn>      
      <ReportToolDesignColumn>
      <Code>Apuestas</Code>
      <Width>300</Width>
      <EquityMatchType>Equality</EquityMatchType>
      <LanguageResources>
        <NLS09>
        <Label>Bets</Label>
        </NLS09>
        <NLS10>
        <Label>Apuestas</Label>
        </NLS10>
      </LanguageResources>
      </ReportToolDesignColumn>     
      <ReportToolDesignColumn>
      <Code>Pagos</Code>
      <Width>300</Width>
      <EquityMatchType>Equality</EquityMatchType>
      <LanguageResources>
        <NLS09>
        <Label>Payments</Label>
        </NLS09>
        <NLS10>
        <Label>Pagos</Label>
        </NLS10>
      </LanguageResources>
      </ReportToolDesignColumn>    
      <ReportToolDesignColumn>
      <Code>JackPot</Code>
      <Width>300</Width>
      <EquityMatchType>Equality</EquityMatchType>
      <LanguageResources>
        <NLS09>
        <Label>JackPot</Label>
        </NLS09>
        <NLS10>
        <Label>JackPot</Label>
        </NLS10>
      </LanguageResources>
      </ReportToolDesignColumn>    
      <ReportToolDesignColumn>
      <Code>Dias</Code>
      <Width>300</Width>
      <EquityMatchType>Equality</EquityMatchType>
      <LanguageResources>
        <NLS09>
        <Label>Days</Label>
        </NLS09>
        <NLS10>
        <Label>Días</Label>
        </NLS10>
      </LanguageResources>
      </ReportToolDesignColumn>
      <ReportToolDesignColumn>
      <Code>Beneficios</Code>
      <Width>300</Width>
      <EquityMatchType>Equality</EquityMatchType>
      <LanguageResources>
        <NLS09>
        <Label>Benefits</Label>
        </NLS09>
        <NLS10>
        <Label>Beneficios</Label>
        </NLS10>
      </LanguageResources>
      </ReportToolDesignColumn>    
      <ReportToolDesignColumn>
      <Code>Promedio_Diario</Code>
      <Width>300</Width>
      <EquityMatchType>Equality</EquityMatchType>
      <LanguageResources>
        <NLS09>
        <Label>Daily average</Label>
        </NLS09>
        <NLS10>
        <Label>Promedio Diario</Label>
        </NLS10>
      </LanguageResources>
      </ReportToolDesignColumn>    
      <ReportToolDesignColumn>
      <Code>Payout_Real</Code>
      <Width>300</Width>
      <EquityMatchType>Equality</EquityMatchType>
      <LanguageResources>
        <NLS09>
        <Label>% Real</Label>
        </NLS09>
        <NLS10>
        <Label>% Real</Label>
        </NLS10>
      </LanguageResources>
      </ReportToolDesignColumn>    
      <ReportToolDesignColumn>
      <Code>Payout_Teo</Code>
      <Width>300</Width>
      <EquityMatchType>Equality</EquityMatchType>
      <LanguageResources>
        <NLS09>
        <Label>% Theo</Label>
        </NLS09>
        <NLS10>
        <Label>% Teo</Label>
        </NLS10>
      </LanguageResources>
      </ReportToolDesignColumn>    
      <ReportToolDesignColumn>
      <Code>Promocion</Code>
      <Width>300</Width>
      <EquityMatchType>Equality</EquityMatchType>
      <LanguageResources>
        <NLS09>
        <Label>Promotion</Label>
        </NLS09>
        <NLS10>
        <Label>Promocion</Label>
        </NLS10>
      </LanguageResources>
      </ReportToolDesignColumn>
      <ReportToolDesignColumn>
      <Code>Benef_Real</Code>
      <Width>300</Width>
      <EquityMatchType>Equality</EquityMatchType>
      <LanguageResources>
        <NLS09>
        <Label>Real Benef.</Label>
        </NLS09>
        <NLS10>
        <Label>Benef.Real</Label>
        </NLS10>
      </LanguageResources>
      </ReportToolDesignColumn>
      <ReportToolDesignColumn>
      <Code>Promo_Diaio_real</Code>
      <Width>300</Width>
      <EquityMatchType>Equality</EquityMatchType>
      <LanguageResources>
        <NLS09>
        <Label>Real daily prom.</Label>
        </NLS09>
        <NLS10>
        <Label>Promo. Diario Real</Label>
        </NLS10>
      </LanguageResources>
      </ReportToolDesignColumn>
    </Columns>
    </ReportToolDesignSheetsDTO>
  </ArrayOfReportToolDesignSheetsDTO>
  '
  
  SET @rtc_design_filter = '<ReportToolDesignFilterDTO>
  <FilterType>CustomFilters</FilterType>
  <Filters>
    <ReportToolDesignFilter>
      <TypeControl>uc_date_picker</TypeControl>
      <TextControl>
        <LanguageResources>
          <NLS09>
            <Label>Date</Label>
          </NLS09>
          <NLS10>
            <Label>Fecha</Label>
          </NLS10>
        </LanguageResources>
      </TextControl>
      <ParameterStoredProcedure>
        <Name>@pFrom</Name>
        <Type>DateTime</Type>
      </ParameterStoredProcedure>
      <Methods>
        <Method>
          <Name>SetFormat</Name>
          <Parameters>1,0</Parameters>
        </Method>
      </Methods>
      <Value>TodayOpening() - 1</Value>
    </ReportToolDesignFilter>
  </Filters>
</ReportToolDesignFilterDTO>'
  
  IF NOT EXISTS(SELECT 1 
        FROM report_tool_config 
        WHERE rtc_store_name = @rtc_store_name)
  BEGIN
    SELECT @rtc_form_id = ISNULL(MAX(rtc_form_id), 10999) + 1
    FROM report_tool_config
    
    INSERT INTO [dbo].[REPORT_TOOL_CONFIG] 
    VALUES  /*RTC_REPORT_TOOL_ID */
      /*RTC_FORM_ID*/        (@rtc_form_id,
      /*RTC_LOCATION_MENU*/   2, 
            /*RTC_REPORT_NAME*/     @rtc_report_name,
            /*RTC_STORE_NAME*/      @rtc_store_name,
            /*RTC_DESIGN_FILTER*/   @rtc_design_filter,
            /*RTC_DESIGN_SHEETS*/   @rtc_design_sheet,
            /*RTC_MAILING*/         0,
            /*RTC_STATUS*/          1,
            /*RTC_MODE*/            36,
      /*RTC_HTML_HEADER*/    NULL,
        /*RTC_HTML_FOOTER*/    NULL)
                                      
  END
  ELSE
  BEGIN
      UPDATE [dbo].[REPORT_TOOL_CONFIG] 
      SET RTC_DESIGN_SHEETS = /*RTC_DESIGN_SHEETS*/   @rtc_design_sheet
        , RTC_DESIGN_FILTER = /*RTC_DESIGN_FILTER*/   @rtc_design_filter
        , RTC_REPORT_NAME   = /*RTC_REPORT_NAME*/     @rtc_report_name
        , RTC_MODE_TYPE     =  /*RTC_MODE*/ 36
      WHERE RTC_STORE_NAME = @rtc_store_name
  END
END
GO





