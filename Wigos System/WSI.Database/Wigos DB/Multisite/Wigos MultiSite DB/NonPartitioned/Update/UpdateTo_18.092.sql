/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_100]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 91;

SET @New_ReleaseId = 92;
SET @New_ScriptName = N'UpdateTo_18.092.sql';
SET @New_Description = N'PR Regionalization';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[buckets]') AND type in (N'U'))
DROP TABLE buckets
GO

CREATE TABLE [dbo].[buckets](
	[bu_bucket_id] [bigint] NOT NULL,
	[bu_name] nvarchar(100) NOT NULL,
	[bu_enabled] [bit] NULL,
	[bu_system_type] [int] NULL,
	[bu_bucket_type] [int] NULL,
	[bu_visible_flags] [bigint] NULL,
	[bu_order_on_reports] [int] NULL,
	[bu_expiration_days] [int] NULL,
	[bu_expiration_date] varchar(10) NULL,
	[bu_level_flags] [bit] NOT NULL,
	[bu_k_factor] [bit] NULL,
	[bu_timestamp] [timestamp] NULL,
        [bu_master_sequence_id] [bigint] NULL
 CONSTRAINT [PK_buckets] PRIMARY KEY CLUSTERED 
(
	[bu_bucket_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- TABLE customer_bucket_by_gaming_day -------------------------------------------------------------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[customer_bucket_by_gaming_day]') AND type in (N'U'))
DROP TABLE customer_bucket_by_gaming_day
GO

CREATE TABLE [dbo].[customer_bucket_by_gaming_day](
	[cbud_gaming_day] [int] NOT NULL,
	[cbud_customer_id] [bigint] NOT NULL,
	[cbud_bucket_id] [bigint] NOT NULL,
	[cbud_value] [money] NULL,
	[cbud_value_added] [money] NULL,
	[cbud_value_substracted] [money] NULL,
 CONSTRAINT [PK_customer_bucket_by_gaming_day] PRIMARY KEY CLUSTERED 
(
	[cbud_gaming_day] ASC,
	[cbud_customer_id] ASC,
	[cbud_bucket_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- TABLE customer_bucket -------------------------------------------------------------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[customer_bucket]') AND type in (N'U'))
DROP TABLE customer_bucket
GO

CREATE TABLE [dbo].[customer_bucket](
	[cbu_customer_id] [bigint] NOT NULL,
	[cbu_bucket_id] [bigint] NOT NULL,
	[cbu_value] [money] NULL,
	[cbu_updated] [datetime] NOT NULL,
	[cbu_timestamp] [timestamp] NULL,
 CONSTRAINT [PK_customer_bucket] PRIMARY KEY CLUSTERED 
(
	[cbu_customer_id] ASC,
	[cbu_bucket_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO



-- TABLE bucket_levels -------------------------------------------------------------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[bucket_levels]') AND type in (N'U'))
DROP TABLE bucket_levels
GO

CREATE TABLE [dbo].[bucket_levels](
	[bul_bucket_id] [bigint] NOT NULL,
	[bul_level_id] [bigint] NOT NULL,
	[bul_a_factor] [decimal] NOT NULL,
	[bul_b_factor] [decimal] NOT NULL,
 CONSTRAINT [PK_bucket_levels] PRIMARY KEY CLUSTERED 
(
	[bul_bucket_id] ASC,
	[bul_level_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

-- TABLE account_buckets_expired_control -------------------------------------------------------------------------------------------------------------------------------------
IF EXISTS (SELECT   1 
             FROM   sys.objects 
            WHERE   object_id = OBJECT_ID(N'[dbo].[account_buckets_expired_control]') 
              AND   type in (N'U'))
  DROP TABLE [account_buckets_expired_control]
GO

CREATE TABLE account_buckets_expired_control
(
	abec_bucket_id bigint NOT NULL,
	abec_day_month nvarchar(5) NOT NULL,
	abec_year int NOT NULL,
	abec_execution datetime NULL,
	CONSTRAINT PK_account_buckets_expired_control PRIMARY KEY CLUSTERED 
	(
		abec_bucket_id ASC, abec_day_month ASC, abec_year ASC
	)
	WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]	
GO

-- TABLE account_buckets_expired_list -------------------------------------------------------------------------------------------------------------------------------------
IF EXISTS (SELECT   1 
             FROM   sys.objects 
            WHERE   object_id = OBJECT_ID(N'[dbo].[account_buckets_expired_list]') 
              AND   type in (N'U'))
  DROP TABLE [account_buckets_expired_list]
GO

CREATE TABLE account_buckets_expired_list
(
	abel_bucket_id bigint NOT NULL,
	abel_account_id bigint NOT NULL,
	abel_value_to_expire money NOT NULL,
	abel_datetime datetime NOT NULL,
	CONSTRAINT PK_account_buckets_expired_list PRIMARY KEY CLUSTERED 
	(
		abel_bucket_id ASC, abel_account_id ASC
	)
	WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE bucket_levels
ALTER COLUMN bul_a_factor decimal(18, 2) NOT NULL
GO

ALTER TABLE bucket_levels
ALTER COLUMN bul_b_factor decimal(18, 2) NOT NULL
GO
/****** INDEXES ******/

/****** RECORDS ******/

IF NOT EXISTS (SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Account.VisibleField' AND GP_SUBJECT_KEY = 'Photo')
      INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY, GP_KEY_VALUE, GP_MS_DOWNLOAD_TYPE) VALUES ('Account.VisibleField','Photo','1', 1)
GO

IF NOT EXISTS (SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Account.RequestedField' AND GP_SUBJECT_KEY = 'Photo')
      INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY, GP_KEY_VALUE, GP_MS_DOWNLOAD_TYPE) VALUES ('Account.RequestedField','Photo','0', 1)
GO
 
 -- PUERTO RICO STATE NAME
DECLARE @CountryISOCode2 AS VARCHAR(10)
SELECT @CountryISOCode2 = GP_KEY_VALUE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'RegionalOptions' AND GP_SUBJECT_KEY = 'CountryISOCode2'
IF (@CountryISOCode2 = 'PR')
BEGIN
  UPDATE GENERAL_PARAMS SET GP_KEY_VALUE = 'Municipality' WHERE GP_GROUP_KEY = 'Account.Fields'  AND GP_SUBJECT_KEY = 'State.Name'
  UPDATE GENERAL_PARAMS SET GP_KEY_VALUE = 'PR'     WHERE GP_GROUP_KEY = 'Account.DefaultValues' AND GP_SUBJECT_KEY = 'Country'
  UPDATE GENERAL_PARAMS SET GP_KEY_VALUE = '130'    WHERE GP_GROUP_KEY = 'Account.DefaultValues' AND GP_SUBJECT_KEY = 'DocumentType' -- Driver License
END
GO

-- PUERTO RICO OCCUPATIONS
IF (NOT EXISTS (SELECT * FROM occupations WHERE oc_country_iso_code2 = 'PR' ))
BEGIN
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('NOT AVAILABLE','0000001',1,1000,'PR')
END
GO

-- PUERTO RICO IDENTIFICATION_TYPES
IF (NOT EXISTS (SELECT * FROM identification_types WHERE idt_country_iso_code2 = 'PR' ))
BEGIN
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (130,1,100,'Driver License','PR')
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (131,1,1,'Passport','PR')
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (132,1,2,'Military','PR')
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (133,1,3,'State Retirement','PR')
END
GO

-- PUERTO RICO REGIONS
IF (NOT EXISTS (SELECT * FROM federal_states WHERE fs_country_iso_code2 = 'PR' ))
BEGIN
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Adjuntas','PR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Aguada','PR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Aguadilla','PR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Aguas Buenas','PR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Aibonito','PR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('A�asco','PR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Arecibo','PR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Arroyo','PR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Barceloneta','PR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Barranquitas','PR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Bayam�n','PR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Cabo Rojo','PR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Caguas','PR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Camuy','PR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Can�vanas','PR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Carolina','PR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Cata�o','PR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Cayey','PR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Ceiba','PR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Ciales','PR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Cidra','PR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Coamo','PR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Comer�o','PR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Corozal','PR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Culebra','PR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Dorado','PR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Fajardo','PR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Florida','PR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Gu�nica','PR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Guayama','PR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Guayanilla','PR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Guaynabo','PR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Gurabo','PR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Hatillo','PR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Hormigueros','PR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Humacao','PR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Isabela','PR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Jayuya','PR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Juana D�az','PR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Juncos','PR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Lajas','PR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Lares','PR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Las Mar�as','PR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Las Piedras','PR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Lo�za','PR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Luquillo','PR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Manat�','PR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Maricao','PR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Maunabo','PR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Mayag�ez','PR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Moca','PR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Morovis','PR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Naguabo','PR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Naranjito','PR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Orocovis','PR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Patillas','PR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Pe�uelas','PR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Ponce','PR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Quebradillas','PR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Rinc�n','PR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('R�o Grande','PR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Sabana Grande','PR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Salinas','PR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('San Germ�n','PR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('San Juan','PR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('San Lorenzo','PR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('San Sebasti�n','PR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Santa Isabel','PR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Toa Alta','PR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Toa Baja','PR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Trujillo Alto','PR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Utuado','PR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Vega Alta','PR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Vega Baja','PR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Vieques','PR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Villalba','PR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Yabucoa','PR');
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Yauco','PR');
END
GO

 IF  EXISTS (SELECT  *
              FROM  sys.objects
             WHERE  object_id = OBJECT_ID(N'[dbo].[MultiSiteTrigger_AccountUpdate]')
               AND  type in (N'TR'))
    DROP TRIGGER [dbo].[MultiSiteTrigger_AccountUpdate]
GO

CREATE TRIGGER [dbo].[MultiSiteTrigger_AccountUpdate] ON [dbo].[accounts]
    AFTER UPDATE
    NOT FOR REPLICATION
AS
BEGIN
    DECLARE @Sequence10Value    AS BIGINT
    DECLARE @AccountId          AS BIGINT
    DECLARE @hash0              AS VARBINARY(20)
    DECLARE @hash1              AS VARBINARY(20)
    DECLARE @changed            AS BIT
    DECLARE @updated            AS BIT

    SET @updated = 0;

    IF (UPDATE(AC_MS_CHANGE_GUID)) RETURN

    IF UPDATE (AC_TRACK_DATA)
    OR UPDATE (AC_HOLDER_NAME)
    OR UPDATE (AC_HOLDER_ID)
    OR UPDATE (AC_HOLDER_ID_TYPE)
    OR UPDATE (AC_HOLDER_ADDRESS_01)
    OR UPDATE (AC_HOLDER_ADDRESS_02)
    OR UPDATE (AC_HOLDER_ADDRESS_03)
    OR UPDATE (AC_HOLDER_CITY)
    OR UPDATE (AC_HOLDER_ZIP)
    OR UPDATE (AC_HOLDER_EMAIL_01)
    OR UPDATE (AC_HOLDER_EMAIL_02)
    OR UPDATE (AC_HOLDER_TWITTER_ACCOUNT)
    OR UPDATE (AC_HOLDER_PHONE_NUMBER_01)
    OR UPDATE (AC_HOLDER_PHONE_NUMBER_02)
    OR UPDATE (AC_HOLDER_COMMENTS)
    OR UPDATE (AC_HOLDER_ID1)
    OR UPDATE (AC_HOLDER_ID2)
    OR UPDATE (AC_HOLDER_DOCUMENT_ID1)
    OR UPDATE (AC_HOLDER_DOCUMENT_ID2)
    OR UPDATE (AC_HOLDER_NAME1)
    OR UPDATE (AC_HOLDER_NAME2)
    OR UPDATE (AC_HOLDER_NAME3)
    OR UPDATE (AC_HOLDER_GENDER)
    OR UPDATE (AC_HOLDER_MARITAL_STATUS)
    OR UPDATE (AC_HOLDER_BIRTH_DATE)
    OR UPDATE (AC_HOLDER_WEDDING_DATE)
    OR UPDATE (AC_HOLDER_LEVEL)
    OR UPDATE (AC_HOLDER_LEVEL_NOTIFY)
    OR UPDATE (AC_HOLDER_LEVEL_ENTERED)
    OR UPDATE (AC_HOLDER_LEVEL_EXPIRATION)
    OR UPDATE (AC_PIN)
    OR UPDATE (AC_PIN_FAILURES)
    OR UPDATE (AC_PIN_LAST_MODIFIED)
    OR UPDATE (AC_BLOCKED)
    OR UPDATE (AC_ACTIVATED)
    OR UPDATE (AC_BLOCK_REASON)
    OR UPDATE (AC_HOLDER_IS_VIP)
    OR UPDATE (AC_HOLDER_TITLE)
    OR UPDATE (AC_HOLDER_NAME4)
    OR UPDATE (AC_HOLDER_PHONE_TYPE_01)
    OR UPDATE (AC_HOLDER_PHONE_TYPE_02)
    OR UPDATE (AC_HOLDER_STATE)
    OR UPDATE (AC_HOLDER_COUNTRY)
    OR UPDATE (AC_USER_TYPE)
    OR UPDATE (AC_POINTS_STATUS)
    OR UPDATE (AC_DEPOSIT)
    OR UPDATE (AC_CARD_PAID)
    OR UPDATE (AC_BLOCK_DESCRIPTION)
    OR UPDATE (AC_MS_CREATED_ON_SITE_ID)
    OR UPDATE (AC_EXTERNAL_REFERENCE)
    OR UPDATE (AC_HOLDER_OCCUPATION)
    OR UPDATE (AC_HOLDER_EXT_NUM)
    OR UPDATE (AC_HOLDER_NATIONALITY)
    OR UPDATE (AC_HOLDER_BIRTH_COUNTRY)
    OR UPDATE (AC_HOLDER_FED_ENTITY)
    OR UPDATE (AC_HOLDER_ID1_TYPE)
    OR UPDATE (AC_HOLDER_ID2_TYPE)
    OR UPDATE (AC_HOLDER_ID3_TYPE)
    OR UPDATE (AC_HOLDER_ID3)
    OR UPDATE (AC_HOLDER_HAS_BENEFICIARY)
    OR UPDATE (AC_BENEFICIARY_NAME)
    OR UPDATE (AC_BENEFICIARY_NAME1)
    OR UPDATE (AC_BENEFICIARY_NAME2)
    OR UPDATE (AC_BENEFICIARY_NAME3)
    OR UPDATE (AC_BENEFICIARY_BIRTH_DATE)
    OR UPDATE (AC_BENEFICIARY_GENDER)
    OR UPDATE (AC_BENEFICIARY_OCCUPATION)
    OR UPDATE (AC_BENEFICIARY_ID1_TYPE)
    OR UPDATE (AC_BENEFICIARY_ID1)
    OR UPDATE (AC_BENEFICIARY_ID2_TYPE)
    OR UPDATE (AC_BENEFICIARY_ID2)
    OR UPDATE (AC_BENEFICIARY_ID3_TYPE)
    OR UPDATE (AC_BENEFICIARY_ID3)
    OR UPDATE (AC_HOLDER_OCCUPATION_ID)
    OR UPDATE (AC_BENEFICIARY_OCCUPATION_ID)
        SET @updated = 1;

    IF (@updated = 0) RETURN

    DECLARE PersonalInfoCursor CURSOR FOR
     SELECT   AC_ACCOUNT_ID
            , HASHBYTES ('SHA1',  ISNULL(AC_TRACK_DATA,        '')
                                + ISNULL(AC_HOLDER_NAME,       '')
                                + ISNULL(AC_HOLDER_ID,         '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_ID_TYPE), '')
                                + ISNULL(AC_HOLDER_ADDRESS_01, '')
                                + ISNULL(AC_HOLDER_ADDRESS_02, '')
                                + ISNULL(AC_HOLDER_ADDRESS_03, '')
                                + ISNULL(AC_HOLDER_CITY,       '')
                                + ISNULL(AC_HOLDER_ZIP,        '')
                                + ISNULL(AC_HOLDER_EMAIL_01,   '')
                                + ISNULL(AC_HOLDER_EMAIL_02,   '')
                                + ISNULL(AC_HOLDER_TWITTER_ACCOUNT, '')
                                + ISNULL(AC_HOLDER_PHONE_NUMBER_01, '')
                                + ISNULL(AC_HOLDER_PHONE_NUMBER_02, '')
                                + ISNULL(AC_HOLDER_COMMENTS,        '')
                                + ISNULL(AC_HOLDER_ID1, '')
                                + ISNULL(AC_HOLDER_ID2, '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_DOCUMENT_ID1), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_DOCUMENT_ID2), '')
                                + ISNULL(AC_HOLDER_NAME1, '')
                                + ISNULL(AC_HOLDER_NAME2, '')
                                + ISNULL(AC_HOLDER_NAME3, '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_GENDER),         '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_MARITAL_STATUS), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_BIRTH_DATE,   21), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_WEDDING_DATE, 21), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_LEVEL),                '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_LEVEL_NOTIFY),         '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_LEVEL_ENTERED,    21), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_LEVEL_EXPIRATION, 21), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_PIN),          '')
                                + ISNULL(CONVERT(NVARCHAR, AC_PIN_FAILURES), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_PIN_LAST_MODIFIED, 21), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_BLOCKED),           '')
                                + ISNULL(CONVERT(NVARCHAR, AC_ACTIVATED),         '')
                                + ISNULL(CONVERT(NVARCHAR, AC_BLOCK_REASON),         '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_IS_VIP),        '')
                                + ISNULL(AC_HOLDER_TITLE                        , '')
                                + ISNULL(AC_HOLDER_NAME4                        , '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_PHONE_TYPE_01),        '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_PHONE_TYPE_02),        '')
                                + ISNULL(AC_HOLDER_STATE                        , '')
                                + ISNULL(AC_HOLDER_COUNTRY                      , '')
                                + ISNULL(CONVERT(NVARCHAR,AC_USER_TYPE)         , '')
                                + ISNULL(CONVERT(NVARCHAR,AC_POINTS_STATUS)     , '')
                                + ISNULL(CONVERT(NVARCHAR,AC_DEPOSIT)           , '')
                                + ISNULL(CONVERT(NVARCHAR,AC_CARD_PAID)         , '')
                                + ISNULL(AC_BLOCK_DESCRIPTION, '')
                                + ISNULL(CONVERT(NVARCHAR,AC_MS_CREATED_ON_SITE_ID), '')
                                + ISNULL(AC_EXTERNAL_REFERENCE, '')
                                + ISNULL(AC_HOLDER_OCCUPATION, '')
                                + ISNULL(AC_HOLDER_EXT_NUM, '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_NATIONALITY),         '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_BIRTH_COUNTRY), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_FED_ENTITY), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_ID1_TYPE), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_ID2_TYPE), '')
                                + ISNULL(AC_HOLDER_ID3_TYPE, '')
                                + ISNULL(AC_HOLDER_ID3, '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_HAS_BENEFICIARY), '')
                                + ISNULL(AC_BENEFICIARY_NAME, '')
                                + ISNULL(AC_BENEFICIARY_NAME1, '')
                                + ISNULL(AC_BENEFICIARY_NAME2, '')
                                + ISNULL(AC_BENEFICIARY_NAME3, '')
                                + ISNULL(CONVERT(NVARCHAR, AC_BENEFICIARY_BIRTH_DATE,   21), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_BENEFICIARY_GENDER), '')	
                                + ISNULL(AC_BENEFICIARY_OCCUPATION, '')
                                + ISNULL(CONVERT(NVARCHAR, AC_BENEFICIARY_ID1_TYPE), '')				
                                + ISNULL(AC_BENEFICIARY_ID1, '')
                                + ISNULL(CONVERT(NVARCHAR, AC_BENEFICIARY_ID2_TYPE), '')				
                                + ISNULL(AC_BENEFICIARY_ID2, '')
                                + ISNULL(AC_BENEFICIARY_ID3_TYPE, '')				
                                + ISNULL(AC_BENEFICIARY_ID3, '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_OCCUPATION_ID), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_BENEFICIARY_OCCUPATION_ID), ''))
       FROM   INSERTED
       WHERE  AC_TYPE NOT IN (4, 5) -- Virtual accounts

    SET NOCOUNT ON;

    OPEN PersonalInfoCursor

    FETCH NEXT FROM PersonalInfoCursor INTO @AccountId, @hash1

    WHILE @@FETCH_STATUS = 0
    BEGIN
        SELECT @hash0 = AC_MS_HASH FROM DELETED WHERE AC_ACCOUNT_ID = @AccountId

        SELECT @changed = CASE WHEN ( @hash0 = @hash1 ) THEN 0 ELSE 1 END

        IF @changed = 1
        BEGIN
            -- Personal Info
            UPDATE   SEQUENCES
               SET   SEQ_NEXT_VALUE = SEQ_NEXT_VALUE + 1
             WHERE   SEQ_ID         = 10

            SELECT   @Sequence10Value = SEQ_NEXT_VALUE - 1 FROM SEQUENCES WHERE SEQ_ID = 10

            UPDATE   ACCOUNTS
               SET   AC_MS_PERSONAL_INFO_SEQ_ID = @Sequence10Value
                   , AC_MS_HASH                 = @hash1
                   , AC_MS_CHANGE_GUID          = NEWID()
             WHERE   AC_ACCOUNT_ID              = @AccountId
        END

        FETCH NEXT FROM PersonalInfoCursor INTO @AccountId, @hash1
    END

    CLOSE PersonalInfoCursor
    DEALLOCATE PersonalInfoCursor

END
GO




--------------------------------------------------------------------------------
-- Copyright � 2013 Win Systems International
--------------------------------------------------------------------------------
--
--   MODULE NAME: MultiSiteTrigger_Customer_Bucket_Insert.sql
--
--   DESCRIPTION: Trigger MultiSiteTrigger_Customer_Bucket_Insert for Center MultiSite
--
--        AUTHOR: Francis Gretz
--
-- CREATION DATE: 07-MAR-2013
--
-- REVISION HISTORY:
--
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 07-MAR-2016 FGB    PBI 10125: Multiple Buckets: Sincronizaci�n MultiSite: Tablas Customer Buckets
-------------------------------------------------------------------------------
--
--   NOTES : Updates field AC_MS_POINTS_SEQ_ID from table ACCOUNTS for MultiSite Synch
--
IF EXISTS (SELECT  *
             FROM  sys.objects
            WHERE  object_id = OBJECT_ID(N'[dbo].[MultiSiteTrigger_Customer_Bucket_Insert]')
              AND  type in (N'TR'))
    DROP TRIGGER [dbo].[MultiSiteTrigger_Customer_Bucket_Insert]
GO

CREATE TRIGGER [dbo].[MultiSiteTrigger_Customer_Bucket_Insert] ON [dbo].[Customer_Bucket]
    AFTER INSERT
    NOT FOR REPLICATION
AS
BEGIN
    DECLARE @CBU_CUSTOMER_ID    AS BIGINT
    DECLARE @CBU_Value          AS MONEY
    DECLARE @Sequence11Value    AS BIGINT

    DECLARE curCustomerBucket CURSOR
    FOR
        SELECT   INSERTED.CBU_CUSTOMER_ID
               , INSERTED.CBU_VALUE
          FROM   INSERTED

    SET NOCOUNT ON;

    OPEN curCustomerBucket

    FETCH NEXT
     FROM curCustomerBucket
     INTO @CBU_CUSTOMER_ID
        , @CBU_Value

    WHILE @@FETCH_STATUS = 0
    BEGIN
        IF (ISNULL(@CBU_Value, 0) <> 0)
        BEGIN
            -- Save next Sequence ID
            UPDATE   SEQUENCES
               SET   SEQ_NEXT_VALUE = SEQ_NEXT_VALUE + 1
             WHERE   SEQ_ID         = 11

            -- Get Sequence ID
            SELECT   @Sequence11Value = SEQ_NEXT_VALUE - 1
              FROM   SEQUENCES
             WHERE   SEQ_ID = 11

            -- Set Sequence ID to Account
            UPDATE   ACCOUNTS
               SET   AC_MS_POINTS_SEQ_ID = @Sequence11Value
             WHERE   AC_ACCOUNT_ID       = @CBU_CUSTOMER_ID
        END

        FETCH NEXT
         FROM curCustomerBucket
         INTO @CBU_CUSTOMER_ID
            , @CBU_Value
    END

    CLOSE curCustomerBucket
    DEALLOCATE curCustomerBucket
END
GO




--------------------------------------------------------------------------------
-- Copyright � 2013 Win Systems International
--------------------------------------------------------------------------------
--
--   MODULE NAME: MultiSiteTrigger_Customer_Bucket_Update.sql
--
--   DESCRIPTION: Trigger MultiSiteTrigger_Customer_Bucket_Update for Center MultiSite
--
--        AUTHOR: Francis Gretz
--
-- CREATION DATE: 07-MAR-2013
--
-- REVISION HISTORY:
--
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 07-MAR-2016 FGB    PBI 10125: Multiple Buckets: Sincronizaci�n MultiSite: Tablas Customer Buckets
-------------------------------------------------------------------------------
--
--   NOTES : Updates field AC_MS_POINTS_SEQ_ID from table ACCOUNTS for MultiSite Synch
--
IF EXISTS (SELECT  *
             FROM  sys.objects
            WHERE  object_id = OBJECT_ID(N'[dbo].[MultiSiteTrigger_Customer_Bucket_Update]')
              AND  type in (N'TR'))
   DROP TRIGGER [dbo].[MultiSiteTrigger_Customer_Bucket_Update]
GO

CREATE TRIGGER [dbo].[MultiSiteTrigger_Customer_Bucket_Update] ON [dbo].[Customer_Bucket]
    AFTER UPDATE
    NOT FOR REPLICATION
AS
BEGIN
    DECLARE @CBU_CUSTOMER_ID    AS BIGINT
    DECLARE @Sequence11Value    AS BIGINT

    IF (UPDATE(CBU_VALUE))
    BEGIN
        DECLARE curCustomerBucket CURSOR
        FOR
            SELECT   INSERTED.CBU_CUSTOMER_ID
              FROM   INSERTED, DELETED
             WHERE   (INSERTED.CBU_CUSTOMER_ID      = DELETED.CBU_CUSTOMER_ID)
               AND   (ISNULL(INSERTED.CBU_VALUE, 0) <> ISNULL(DELETED.CBU_VALUE, 0))

        SET NOCOUNT ON;

        OPEN curCustomerBucket

        FETCH NEXT
         FROM curCustomerBucket
         INTO @CBU_CUSTOMER_ID

        WHILE @@FETCH_STATUS = 0
        BEGIN
            -- Save next Sequence ID
            UPDATE   SEQUENCES
               SET   SEQ_NEXT_VALUE = SEQ_NEXT_VALUE + 1
             WHERE   SEQ_ID         = 11

            -- Get Sequence ID
            SELECT   @Sequence11Value = SEQ_NEXT_VALUE - 1
              FROM   SEQUENCES
             WHERE   SEQ_ID = 11

            -- Set Sequence ID to Account
            UPDATE   ACCOUNTS
               SET   AC_MS_POINTS_SEQ_ID = @Sequence11Value
             WHERE   AC_ACCOUNT_ID       = @CBU_CUSTOMER_ID

            FETCH NEXT
             FROM curCustomerBucket
             INTO @CBU_CUSTOMER_ID
        END

        CLOSE curCustomerBucket
        DEALLOCATE curCustomerBucket
    END
END
GO





/****** Object:  UserDefinedFunction [dbo].[SplitStringIntoTable]    Script Date: 10/03/2016 12:26:21 ******/
IF EXISTS (SELECT 1 FROM sys.objects WHERE NAME = 'SplitStringIntoTable' AND TYPE = 'TF')  
	DROP FUNCTION [dbo].[SplitStringIntoTable]
GO

/****** Object:  UserDefinedFunction [dbo].[SplitStringIntoTable]    Script Date: 10/03/2016 12:26:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



/*
   PURPOSE: SPLITS STRING INTO TABLE
   
   PARAMS: @pString: String list of items separate by same delimiter. Ex: '1,2,3,4,5,6'
           @pDelimiter: Separator char on the string list. Ex: ','
           @pTrimItems: Default 1 - remove left and right spaces on each item 
   
   RETURN:  TABLE with the selected values in row format: SST_ID, SST_VALUE
   
   NOTES: Select in must take the SST_VALUE to compare the item. 
          Ex: WHERE FIELD_NAME IN ( SELECT SST_VALUE FROM dbo.SplitStringIntoTable(@ListItemsString,@Separator, DEFAULT) )
*/
CREATE FUNCTION [dbo].[SplitStringIntoTable] 
(
    @pString VARCHAR(4096),
    @pDelimiter VARCHAR(10),
    @pTrimItems BIT = 1
)
RETURNS 
@ReturnTable TABLE 
(
    [SST_ID] [INT] IDENTITY(1,1) NOT NULL,
    [SST_VALUE] [VARCHAR](50) NULL
)

AS
BEGIN
         
        DECLARE @_ISPACES INT
        DECLARE @_PART VARCHAR(50)

        -- INITIALIZE SPACES
        SELECT @_ISPACES = CHARINDEX(@pDelimiter,@pString,0)
        WHILE @_ISPACES > 0

        BEGIN
            SELECT @_PART = SUBSTRING(@pString,0,CHARINDEX(@pDelimiter,@pString,0))

            IF @pTrimItems = 1
             SET @_PART = LTRIM(RTRIM(@_PART))

            INSERT INTO @ReturnTable(SST_VALUE)
            SELECT @_PART

            SELECT @pString = SUBSTRING(@pString, @_ISPACES + LEN(@pDelimiter), LEN(@pString) - CHARINDEX(@pDelimiter, @pString, 0))

            SELECT @_ISPACES = CHARINDEX(@pDelimiter,@pString,0)
        END

        IF LEN(@pString) > 0
            INSERT INTO @ReturnTable
            SELECT CASE WHEN @pTrimItems = 1 THEN LTRIM(RTRIM(@pString)) ELSE @pString END

    RETURN 
END

GO





--------------------------------------------------------------------------------
-- Copyright � 2013 Win Systems International
--------------------------------------------------------------------------------
--
--   MODULE NAME: Update_PointsAccountMovement.sql
--
--   DESCRIPTION: Update points to account & movement procedure in MultiSite Center
--
--        AUTHOR: Dani Dom�nguez
--
-- CREATION DATE: 07-MAR-2013
--
-- REVISION HISTORY:
--
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 07-MAR-2013 DDM    First release.
-- 07-MAR-2013 DDM    Add in/out parameters
-- 03-JUN-2013 DDM    Fixed Bug #812
-- 03-MAR-2016 FGB    PBI 10125: Multiple Buckets: Sincronizaci�n MultiSite: Tablas Customer Buckets
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- PURPOSE: Update points to Buckets & movement procedure
--
--  PARAMS:
--      - INPUT:
--           @pSiteId
--           @pMovementId
--           @pPlaySessionId
--           @pAccountId
--           @pTerminalId
--           @pWcpSequenceId
--           @pWcpTransactionId
--           @pDatetime
--           @pType
--           @pInitialBalance
--           @pSubAmount
--           @pAddAmount
--           @pFinalBalance
--           @pCashierId
--           @pCashierName
--           @pTperminalName
--           @pOperationId
--           @pDetails
--           @pReasons
--           @pPlayerTrackingMode
--
--      - OUTPUT:
--           @pErrorCode
--           @pPointsSequenceId
--           @pPoints
--           @pCenterInitialBalance
--           @pBucketId
--
-- RETURNS:
--
--   NOTES:
--
--------------------------------------------------------------------------------

IF  EXISTS (SELECT   *
              FROM   sys.objects
             WHERE   object_id = OBJECT_ID(N'[dbo].[Update_PointsAccountMovement]')
               AND   type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[Update_PointsAccountMovement]
GO

CREATE PROCEDURE [dbo].[Update_PointsAccountMovement]
     @pSiteId                   INT
,    @pMovementId               BIGINT
,    @pPlaySessionId            BIGINT
,    @pAccountId                BIGINT
,    @pTerminalId               INT
,    @pWcpSequenceId            BIGINT
,    @pWcpTransactionId         BIGINT
,    @pDatetime                 DATETIME
,    @pType                     INT
,    @pInitialBalance           MONEY
,    @pSubAmount                MONEY
,    @pAddAmount                MONEY
,    @pFinalBalance             MONEY
,    @pCashierId                INT
,    @pCashierName              NVARCHAR(50)
,    @pTperminalName            NVARCHAR(50)
,    @pOperationId              BIGINT
,    @pDetails                  NVARCHAR(256)
,    @pReasons                  NVARCHAR(64)
,    @pPlayerTrackingMode       INT
,    @pErrorCode                INT             OUTPUT
,    @pPointsSequenceId         BIGINT          OUTPUT
,    @pPoints                   MONEY           OUTPUT
,    @pCenterInitialBalance     MONEY           OUTPUT
,    @pBucketId                 BIGINT          OUTPUT
AS
BEGIN
  DECLARE @LocalDelta           AS MONEY
  DECLARE @IsDelta              AS BIT
  DECLARE @CheckAccountId       AS BIGINT -- For check if exists account

  -- Output parameters
  SET @pErrorCode = 1
  SET @pPointsSequenceId = NULL
  SET @pPoints = NULL
  SET @pCenterInitialBalance = NULL
  SET @pBucketId = NULL

  -- Local variables
  SET @LocalDelta = 0
  SET @IsDelta = 1
  SET @CheckAccountId = NULL

  SET NOCOUNT ON;

  -- Set BucketId
  IF (@pType >= 1100 AND @pType <= 1599)
  BEGIN
    SET  @pBucketId = (@pType % 100) -- Last 2 d�gits
  END
  ELSE
  BEGIN
    IF (@pType In (36, 37, 38, 39, 40, 41, 46, 50, 60, 61, 66, 71, 101))
      SET  @pBucketId = 1    -- BucketId.RedemptionPoints
    ELSE
      SET  @pBucketId = 2    -- BucketId.RankingLevelPoints
  END
 
  -- The Bucket does not exist in table BUCKETS
  IF NOT EXISTS(SELECT   1
                  FROM   BUCKETS
                 WHERE   BU_BUCKET_ID = @pBucketId)
  BEGIN
    SET @pErrorCode = 1
    RETURN
  END

  SELECT   @pCenterInitialBalance = CBU_VALUE
    FROM   CUSTOMER_BUCKET
   WHERE   CBU_CUSTOMER_ID        = @pAccountId
     AND   CBU_BUCKET_ID          = @pBucketId

  SET @pCenterInitialBalance = ISNULL(@pCenterInitialBalance, 0)

  SELECT   @CheckAccountId = AC_ACCOUNT_ID
    FROM   ACCOUNTS
   WHERE   AC_ACCOUNT_ID   = @pAccountId

  -- The Account does not exist
  IF (@CheckAccountId IS NULL)
  BEGIN
    SET @pErrorCode = 1
    RETURN
  END

  IF NOT EXISTS (SELECT   1
                   FROM   ACCOUNT_MOVEMENTS
                  WHERE   AM_SITE_ID     = @pSiteId
                    AND   AM_MOVEMENT_ID = @pMovementId)
  BEGIN
    INSERT INTO   ACCOUNT_MOVEMENTS
    (
        AM_SITE_ID
      , AM_MOVEMENT_ID
      , AM_PLAY_SESSION_ID
      , AM_ACCOUNT_ID
      , AM_TERMINAL_ID
      , AM_WCP_SEQUENCE_ID
      , AM_WCP_TRANSACTION_ID
      , AM_DATETIME
      , AM_TYPE
      , AM_INITIAL_BALANCE
      , AM_SUB_AMOUNT
      , AM_ADD_AMOUNT
      , AM_FINAL_BALANCE
      , AM_CASHIER_ID
      , AM_CASHIER_NAME
      , AM_TERMINAL_NAME
      , AM_OPERATION_ID
      , AM_DETAILS
      , AM_REASONS
    )
    VALUES
    (
        @pSiteId
      , @pMovementId
      , @pPlaySessionId
      , @pAccountId
      , @pTerminalId
      , @pWcpSequenceId
      , @pWcpTransactionId
      , @pDatetime
      , @pType
      , @pInitialBalance
      , @pSubAmount
      , @pAddAmount
      , @pFinalBalance
      , @pCashierId
      , @pCashierName
      , @pTperminalName
      , @pOperationId
      , @pDetails
      , @pReasons
    )

    SET @LocalDelta = ISNULL(@pAddAmount, 0) - ISNULL(@pSubAmount, 0)

    -- Points Gift Delivery/Points Gift Services/Points Status/Imported Points History
    --IF (@pType IN (39, 42, 67, 62, 73))  SET @LocalDelta = 0
	IF (@pType IN (39, 42, 62, 67))  SET @LocalDelta = 0

    -- Multisite initial points
    IF (@pType = 101) SET @IsDelta = 0

    -- Update Account
    UPDATE   ACCOUNTS
       SET   AC_MS_LAST_SITE_ID  = @pSiteId
           , AC_LAST_ACTIVITY    = GETDATE()
     WHERE   AC_ACCOUNT_ID       = @pAccountId

    IF NOT EXISTS (SELECT   1
                     FROM   CUSTOMER_BUCKET
                    WHERE   CBU_CUSTOMER_ID = @pAccountId
                      AND   CBU_BUCKET_ID   = @pBucketId)
    BEGIN
      INSERT INTO   CUSTOMER_BUCKET(CBU_CUSTOMER_ID, CBU_BUCKET_ID, CBU_VALUE, CBU_UPDATED)
           VALUES  (@pAccountId, @pBucketId, 0, GETDATE())
    END

    UPDATE   CUSTOMER_BUCKET
       SET   CBU_VALUE        = CASE WHEN (@pPlayerTrackingMode = 1)
                                         THEN CBU_VALUE
                                         ELSE (CASE WHEN (@IsDelta = 1)THEN CBU_VALUE + @LocalDelta
                                                                       ELSE @LocalDelta
                                               END)
                                END
           , CBU_UPDATED      = GETDATE()
     WHERE   CBU_CUSTOMER_ID  = @pAccountId
       AND   CBU_BUCKET_ID    = @pBucketId

    -- TODO BUCKET_HISTORY
  END

  SET @pErrorCode = 0

  -- Get sequence ID
  SELECT   @pPointsSequenceId = AC_MS_POINTS_SEQ_ID
    FROM   ACCOUNTS
   WHERE   AC_ACCOUNT_ID      = @pAccountId

  -- Get Bucket Value
  SELECT   @pPoints           = CBU_VALUE
    FROM   CUSTOMER_BUCKET
   WHERE   CBU_CUSTOMER_ID    = @pAccountId
     AND   CBU_BUCKET_ID      = @pBucketId
     
  SET @pPoints = ISNULL(@pPoints, 0)
END

GO



IF NOT EXISTS (SELECT 1 FROM MS_SITE_TASKS  WHERE ST_TASK_ID = 70)
INSERT INTO MS_SITE_TASKS
            (
            ST_SITE_ID,
            ST_TASK_ID,
            ST_ENABLED,
            ST_RUNNING,
            ST_STARTED,
            ST_LOCAL_SEQUENCE_ID,
            ST_REMOTE_SEQUENCE_ID,
            ST_INTERVAL_SECONDS,
            ST_LAST_RUN,
            ST_NUM_PENDING,
            ST_MAX_ROWS_TO_UPLOAD,
            ST_START_TIME,
            ST_FORCE_RESYNCH
            )
    VALUES
            (
            0,
            70,
            1,
            0,
            NULL,
            NULL,
            NULL,
            60,
            NULL,
            NULL,
            1000,
            0,
            0
            )

GO

DECLARE @Puntos_canje_id int
DECLARE @now datetime

SET @Puntos_canje_id = 1

SET @now = GETDATE()

INSERT INTO   CUSTOMER_BUCKET 
             (CBU_CUSTOMER_ID
            , CBU_BUCKET_ID
            , CBU_VALUE
            , CBU_UPDATED)
     SELECT   AC_ACCOUNT_ID
            , @Puntos_canje_id
            , ISNULL(AC_POINTS,0)
            , @now
       FROM   ACCOUNTS
  LEFT JOIN   CUSTOMER_BUCKET
         ON   AC_ACCOUNT_ID = CBU_CUSTOMER_ID AND CBU_BUCKET_ID = @Puntos_canje_id
      WHERE   CBU_CUSTOMER_ID IS NULL
        AND   ISNULL(AC_POINTS,0) <> 0


GO


DELETE FROM BUCKETS
GO

DECLARE @bu_name_bucket_1 NVARCHAR(100)
DECLARE @bu_name_bucket_2 NVARCHAR(100)
DECLARE @bu_name_bucket_3 NVARCHAR(100)
DECLARE @bu_name_bucket_4 NVARCHAR(100)
DECLARE @bu_name_bucket_5 NVARCHAR(100)
DECLARE @bu_name_bucket_6 NVARCHAR(100)
DECLARE @bu_name_bucket_7 NVARCHAR(100)

if exists(select 1 from GENERAL_PARAMS where gp_group_key='WigosGUI' and gp_subject_key = 'Language' and gp_key_value = 'es') begin
            set @bu_name_bucket_1 = 'Puntos de canje'
            set @bu_name_bucket_2 = 'Puntos de nivel'
            set @bu_name_bucket_3 = 'Cr�dito NR'
            set @bu_name_bucket_4 = 'Cr�dito RE'
            set @bu_name_bucket_5 = 'Bucket oculto 1: Puntos de Canje'
            set @bu_name_bucket_6 = 'Bucket oculto 2: Cr�dito NR o free plays'
            set @bu_name_bucket_7 = 'Bucket oculto 3: Cr�dito RE o Comps'

end else 
begin
            set @bu_name_bucket_1 = 'Redemption points'
            set @bu_name_bucket_2 = 'Ranking level points'
            set @bu_name_bucket_3 = 'Non-redeemable credit'
            set @bu_name_bucket_4 = 'Redeemable credit'
            set @bu_name_bucket_5 = 'Hidden Bucket 1: Redemption points'
            set @bu_name_bucket_6 = 'Hidden Bucket 2: Non-redeemable credit or free plays'
            set @bu_name_bucket_7 = 'Hidden Bucket 3: Redeemable credit or Comps'
end


INSERT INTO BUCKETS
		(
		BU_BUCKET_ID,
		BU_NAME,
		BU_ENABLED, 
		BU_SYSTEM_TYPE, 
		BU_BUCKET_TYPE, 
		BU_VISIBLE_FLAGS, 
		BU_ORDER_ON_REPORTS, 
		BU_EXPIRATION_DAYS, 
		BU_EXPIRATION_DATE, 
		BU_LEVEL_FLAGS, -- Por nivel / General
		BU_K_FACTOR
		)

SELECT 1, @bu_name_bucket_1, 0, 1, 1, 4294967291, 1, 0, NULL, 1, 0
UNION ALL                                        
SELECT 2, @bu_name_bucket_2, 0, 1, 2, 4294967291, 2, 0, NULL, 1, 0
UNION ALL                                        
SELECT 3, @bu_name_bucket_3, 0, 1, 3, 4294967291, 3, 0, NULL, 1, 0
UNION ALL                                        
SELECT 4, @bu_name_bucket_4, 0, 1, 4, 4294967291, 4, 0, NULL, 1, 0
UNION ALL                                        
SELECT 5, @bu_name_bucket_5, 0, 1, 5, 4294967291, 5, 0, NULL, 1, 0
UNION ALL                                        
SELECT 6, @bu_name_bucket_6, 0, 1, 6, 4294967291, 6, 0, NULL, 1, 0
UNION ALL                                        
SELECT 7, @bu_name_bucket_7, 0, 1, 7, 4294967291, 7, 0, NULL, 1, 0

GO




declare @Now datetime
declare @Today datetime
declare @ClosingTime Integer
declare @DaysCountingPoints Integer
declare @Date_From datetime
declare @Date_To datetime
declare @MaxDays Integer
declare @LastActivity datetime
declare @Puntos_nivel_id int

-- Bucket Puntos de Nivel
SET @Puntos_nivel_id = 2

-- Today
select @ClosingTime = gp_key_value from general_params where gp_group_key = 'WigosGUI' and gp_subject_key = 'ClosingTime'
SET @ClosingTime = isnull(@ClosingTime, 0)
SET @Now = DATEADD(dd, 0, DATEDIFF(dd, 0, getdate()))
SET @Today = DATEADD(hh, @ClosingTime, @Now)

-- Date From
SELECT @DaysCountingPoints = gp_key_value from general_params where gp_group_key = 'PlayerTracking' and gp_subject_key = 'Levels.DaysCountingPoints'
SET @Date_From = DATEADD(dd, -@DaysCountingPoints, @Today)

-- LastActivity
SET @MaxDays = 400 + @DaysCountingPoints
SET @LastActivity = DATEADD (DAY, -@MaxDays,@Today)



DELETE FROM CUSTOMER_BUCKET WHERE cbu_bucket_id = @Puntos_nivel_id

INSERT INTO   CUSTOMER_BUCKET 
             (CBU_CUSTOMER_ID
            , CBU_BUCKET_ID
            , CBU_VALUE
            , CBU_UPDATED)

SELECT     AC.ACCOUNT_ID      
		 , @Puntos_nivel_id
         , ISNULL (POINTS, 0) 
         , LAST_ACTIVITY      
FROM
		(
		SELECT   AC_ACCOUNT_ID              AS ACCOUNT_ID
			   , AC_LAST_ACTIVITY           AS LAST_ACTIVITY
		  FROM   ACCOUNTS WITH (INDEX (IX_ac_last_activity))
		 WHERE   AC_LAST_ACTIVITY >= @LastActivity
		   AND   AC_HOLDER_LEVEL  >= 1
		) AC LEFT JOIN
		(
			SELECT   AM_ACCOUNT_ID      AS ACCOUNT_ID
				   , SUM(AM_ADD_AMOUNT) AS POINTS 
			  FROM   ACCOUNT_MOVEMENTS WITH (INDEX (IX_type_date_account)) 
		INNER JOIN   ACCOUNTS ON AC_ACCOUNT_ID = AM_ACCOUNT_ID
			WHERE   AM_TYPE IN (36, 68, 72, 73)
			  AND   AM_DATETIME  >= @Date_From 
			  AND   AM_DATETIME  <  @Today
		 GROUP BY   AM_ACCOUNT_ID
		) PO ON AC.ACCOUNT_ID = PO.ACCOUNT_ID

WHERE ISNULL (POINTS, 0) > 0

GO

DELETE FROM BUCKET_LEVELS


DECLARE @BucketId_PuntosCanje INTEGER
DECLARE @BucketId_PuntosNivel INTEGER
DECLARE @Level01_Points decimal(10,2)
DECLARE @Level02_Points decimal(10,2)
DECLARE @Level03_Points decimal(10,2)
DECLARE @Level04_Points decimal(10,2)
DECLARE @Level05_Points decimal(10,2)

SET @Level01_Points = 0
SET @Level02_Points = 0
SET @Level03_Points = 0
SET @Level04_Points = 0
SET @Level05_Points = 0

SELECT @Level01_Points = ISNULL(convert(decimal(10,2),replace(GP_KEY_VALUE,',','.')),0) FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PlayerTracking' AND GP_SUBJECT_KEY = 'Level01.RedeemablePlayedTo1Point'
SELECT @Level02_Points = ISNULL(convert(decimal(10,2),replace(GP_KEY_VALUE,',','.')),0) FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PlayerTracking' AND GP_SUBJECT_KEY = 'Level02.RedeemablePlayedTo1Point'
SELECT @Level03_Points = ISNULL(convert(decimal(10,2),replace(GP_KEY_VALUE,',','.')),0) FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PlayerTracking' AND GP_SUBJECT_KEY = 'Level03.RedeemablePlayedTo1Point'
SELECT @Level04_Points = ISNULL(convert(decimal(10,2),replace(GP_KEY_VALUE,',','.')),0) FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PlayerTracking' AND GP_SUBJECT_KEY = 'Level04.RedeemablePlayedTo1Point'
SELECT @Level05_Points = ISNULL(convert(decimal(10,2),replace(GP_KEY_VALUE,',','.')),0) FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PlayerTracking' AND GP_SUBJECT_KEY = 'Level05.RedeemablePlayedTo1Point'

-- BUCKET: Puntos de canje
SET @BucketId_PuntosCanje = 1
INSERT INTO  BUCKET_LEVELS (BUL_BUCKET_ID, BUL_LEVEL_ID, BUL_A_FACTOR, BUL_B_FACTOR)
     SELECT  @BucketId_PuntosCanje, 1, 1, @Level01_Points WHERE @Level01_Points <> 0
      UNION
     SELECT  @BucketId_PuntosCanje, 2, 1, @Level02_Points WHERE @Level02_Points <> 0
      UNION
     SELECT  @BucketId_PuntosCanje, 3, 1, @Level03_Points WHERE @Level03_Points <> 0
      UNION
     SELECT  @BucketId_PuntosCanje, 4, 1, @Level04_Points WHERE @Level04_Points <> 0
      UNION
     SELECT  @BucketId_PuntosCanje, 5, 1, @Level05_Points WHERE @Level05_Points <> 0
     
		-- Disable Buckets with RedeemablePlayedTo1Point = 0
		IF (   @Level01_Points <> 0
			OR @Level02_Points <> 0
			OR @Level03_Points <> 0
			OR @Level04_Points <> 0
			OR @Level05_Points <> 0)
			
			UPDATE BUCKETS SET bu_enabled = 1 WHERE bu_bucket_id = @BucketId_PuntosCanje
											

-- BUCKET: Puntos de nivel
SET @BucketId_PuntosNivel = 2
INSERT INTO  BUCKET_LEVELS (BUL_BUCKET_ID, BUL_LEVEL_ID, BUL_A_FACTOR, BUL_B_FACTOR)
     SELECT  @BucketId_PuntosNivel, 1, 1, @Level01_Points WHERE @Level01_Points <> 0
      UNION												 
     SELECT  @BucketId_PuntosNivel, 2, 1, @Level02_Points WHERE @Level02_Points <> 0
      UNION												 
     SELECT  @BucketId_PuntosNivel, 3, 1, @Level03_Points WHERE @Level03_Points <> 0
      UNION												 
     SELECT  @BucketId_PuntosNivel, 4, 1, @Level04_Points WHERE @Level04_Points <> 0
      UNION												 
     SELECT  @BucketId_PuntosNivel, 5, 1, @Level05_Points WHERE @Level05_Points <> 0



		-- Disable Buckets with RedeemablePlayedTo1Point = 0
		IF (   @Level01_Points <> 0
			OR @Level02_Points <> 0
			OR @Level03_Points <> 0
			OR @Level04_Points <> 0
			OR @Level05_Points <> 0)
			
			UPDATE BUCKETS SET bu_enabled = 1 WHERE bu_bucket_id = @BucketId_PuntosNivel
												
											
											
GO



IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AccountPointsCache_CalculatePoints]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[AccountPointsCache_CalculatePoints]
GO

CREATE PROCEDURE [dbo].[AccountPointsCache_CalculatePoints]
       @AccountId                        BIGINT,
       @DateFrom                         DATETIME,
       @DateTo                           DATETIME,
       @MovementId                       BIGINT, 
       @LastMovementId                   BIGINT OUTPUT, 
       @PointsGeneratedForLevel          MONEY  OUTPUT,
       @PointsDiscretionalForLevel       MONEY  OUTPUT,
       @PointsDiscretionalOnlyForRedeem  MONEY  OUTPUT,
       @PointsPromotionOnlyForRedeem     MONEY  OUTPUT
AS
BEGIN
  DECLARE @points_awarded                        AS INT;
  DECLARE @manually_added_points_for_level       AS INT;
  DECLARE @imported_points_for_level             AS INT;
  DECLARE @imported_points_history               AS INT;
  DECLARE @manually_added_points_only_for_redeem AS INT;
  DECLARE @imported_points_only_for_redeem       AS INT;
  DECLARE @promotion_point                       AS INT;
  DECLARE @cancel_promotion_point                AS INT;
  -- 11-FEB-2016 JRC    PBI 7909: Multiple Buckets. 
  DECLARE @EndSession_RankingLevelPoints		AS INT;	
  DECLARE @Expired_RankingLevelPoints		    AS INT
  DECLARE @Manual_Add_RankingLevelPoints		AS INT
  DECLARE @Manual_Sub_RankingLevelPoints		AS INT
  DECLARE @Manual_Set_RankingLevelPoints		AS INT
  DECLARE @EndSession_RedemptionPoints			AS INT
  DECLARE @Expired_RedemptionPoints				AS INT
  DECLARE @Manual_Add_RedemptionPoints			AS INT
  DECLARE @Manual_Sub_RedemptionPoints			AS INT
  DECLARE @Manual_Set_RedemptionPoints			AS INT
  


  DECLARE @Sql             AS NVARCHAR(MAX);
  DECLARE @ParamDefinition AS NVARCHAR(MAX);
  DECLARE @Index           AS NVARCHAR(MAX);
  DECLARE @Where           AS NVARCHAR(MAX); 
  
  -- Points for level
     -- Generated
  SET @points_awarded                        = 36
  --PointsAwarded
  
     -- Discretional
  
  SET @manually_added_points_for_level       = 68
  --ManuallyAddedPointsForLevel
  
  SET @imported_points_for_level             = 72
  --ImportedPointsForLevel

  SET @imported_points_history               = 73
  --ImportedPointsHistory

  -- Points only for redeem
     -- Discretional 
  SET @manually_added_points_only_for_redeem = 50
  --ManuallyAddedPointsOnlyForRedeem

  SET @imported_points_only_for_redeem       = 71
  --ImportedPointsOnlyForRedeem

     -- Promotion 
  SET @promotion_point                       = 60
  --PromotionPoint
  
  SET @cancel_promotion_point                = 61
  --CancelPromotionPoint 

  SET @EndSession_RankingLevelPoints		     = 1102
  --'MULTIPLE_BUCKETS_END_SESSION + RankingLevelPoints PuntosNivel'
  SET @Expired_RankingLevelPoints		         = 1202
  --'MULTIPLE_BUCKETS_Expired + RankingLevelPoints PuntosNivel'
  SET @Manual_Add_RankingLevelPoints		     = 1302
  --'MULTIPLE_BUCKETS_Manual_Add + RankingLevelPoints PuntosNivel'
  SET @Manual_Sub_RankingLevelPoints		     = 1402
  --'MULTIPLE_BUCKETS_Manual_Sub + RankingLevelPoints PuntosNivel'
  SET @Manual_Set_RankingLevelPoints		     = 1502
  --'MULTIPLE_BUCKETS_Manual_Set + RankingLevelPoints PuntosNivel'


  SET @EndSession_RedemptionPoints		     = 1101
  --'MULTIPLE_BUCKETS_END_SESSION + RedemptionPoints PuntosCanje'
  SET @Expired_RedemptionPoints		         = 1201
  --'MULTIPLE_BUCKETS_Expired + RedemptionPoints PuntosCanje'
  SET @Manual_Add_RedemptionPoints		     = 1301
  --'MULTIPLE_BUCKETS_Manual_Add + RedemptionPoints PuntosCanje'
  SET @Manual_Sub_RedemptionPoints		     = 1401
  --'MULTIPLE_BUCKETS_Manual_Sub + RedemptionPoints PuntosCanje'
  SET @Manual_Set_RedemptionPoints		     = 1501
  --'MULTIPLE_BUCKETS_Manual_Set + RedemptionPoints PuntosCanje'




  
  SET @LastMovementId = @MovementId

    SET @Index = 'IX_am_account_id_type_datetime'
    SET @Where = ' WHERE AM_ACCOUNT_ID  = @AccountId ' +
                 '   AND AM_TYPE IN (@points_awarded, @manually_added_points_for_level, @imported_points_for_level, @imported_points_history, ' +
				 '                   @EndSession_RankingLevelPoints, @Expired_RankingLevelPoints, @Manual_Add_RankingLevelPoints, @Manual_Sub_RankingLevelPoints, @Manual_Set_RankingLevelPoints,' +
				 '                   @EndSession_RedemptionPoints, @Expired_RedemptionPoints, @Manual_Add_RedemptionPoints, @Manual_Sub_RedemptionPoints, @Manual_Set_RedemptionPoints,' +
                 '                   @manually_added_points_only_for_redeem, @imported_points_only_for_redeem, @promotion_point, @cancel_promotion_point) ' + 
                 '   AND AM_DATETIME >= CAST(''' + CAST(@DateFrom AS VARCHAR(50)) + ''' AS DATETIME) ' +
                 '   AND AM_DATETIME  < CAST(''' + CAST(@DateTo AS VARCHAR(50)) + ''' AS DATETIME) ' +
                 '   AND AM_MOVEMENT_ID > ' + CAST(@MovementId AS NVARCHAR(MAX)) 

  SET @Sql = '
  SELECT   @PointsGeneratedForLevel_out          = ISNULL(SUM (CASE WHEN AM_TYPE IN (@points_awarded,@EndSession_RankingLevelPoints, @Expired_RankingLevelPoints)
                                                               THEN AM_ADD_AMOUNT-AM_SUB_AMOUNT 
                                                               ELSE 0 
                                                               END ), 0) 
         , @PointsDiscretionalForLevel_out       = ISNULL(SUM (CASE WHEN AM_TYPE IN (@manually_added_points_for_level, @imported_points_for_level, @imported_points_history, @Manual_Add_RankingLevelPoints, @Manual_Sub_RankingLevelPoints, @Manual_Set_RankingLevelPoints)
                                                               THEN AM_ADD_AMOUNT-AM_SUB_AMOUNT 
                                                               ELSE 0 
                                                               END ), 0) 
         , @PointsDiscretionalOnlyForRedeem_out  = ISNULL(SUM (CASE WHEN AM_TYPE IN (@manually_added_points_only_for_redeem, @imported_points_only_for_redeem, @Manual_Add_RedemptionPoints, @Manual_Sub_RedemptionPoints, @Manual_Set_RedemptionPoints)
                                                               THEN AM_ADD_AMOUNT-AM_SUB_AMOUNT 
                                                               ELSE 0 
                                                               END ), 0)
         , @PointsPromotionOnlyForRedeem_out     = ISNULL(SUM (CASE WHEN AM_TYPE IN (@promotion_point, @cancel_promotion_point,@EndSession_RedemptionPoints, @Expired_RedemptionPoints)
                                                               THEN AM_ADD_AMOUNT-AM_SUB_AMOUNT
                                                               ELSE 0 
                                                               END ), 0) 
         , @LastMovementId_out                   = ISNULL(MAX (AM_MOVEMENT_ID), @LastMovementId_out)
    FROM   ACCOUNT_MOVEMENTS WITH (INDEX (' + @Index + ')) ' 
  + @Where
 
SET @ParamDefinition = N'@AccountId                             BIGINT, 
                        @points_awarded                         INT, 
                        @manually_added_points_for_level        INT, 
                        @imported_points_for_level              INT, 
                        @imported_points_history                INT, 
                        @manually_added_points_only_for_redeem  INT, 
                        @imported_points_only_for_redeem        INT, 
                        @promotion_point                        INT, 
                        @cancel_promotion_point                 INT, 
						@EndSession_RankingLevelPoints			INT,
						@Expired_RankingLevelPoints				INT,	
						@Manual_Add_RankingLevelPoints			INT,
						@Manual_Sub_RankingLevelPoints			INT,
						@Manual_Set_RankingLevelPoints			INT,
						@EndSession_RedemptionPoints			INT,	
						@Expired_RedemptionPoints				INT,	
						@Manual_Add_RedemptionPoints			INT,	
						@Manual_Sub_RedemptionPoints			INT,	
						@Manual_Set_RedemptionPoints			INT,	
                        @LastMovementId_out                     BIGINT OUTPUT, 
                        @PointsGeneratedForLevel_out            MONEY  OUTPUT, 
                        @PointsDiscretionalForLevel_out         MONEY  OUTPUT, 
                        @PointsDiscretionalOnlyForRedeem_out    MONEY  OUTPUT, 
                        @PointsPromotionOnlyForRedeem_out       MONEY  OUTPUT' 

EXEC sp_executesql @Sql, 
					@ParamDefinition,
					@AccountId                             = @AccountId, 
					@points_awarded                        = @points_awarded,                        
					@manually_added_points_for_level       = @manually_added_points_for_level,
					@imported_points_for_level             = @imported_points_for_level,
					@imported_points_history               = @imported_points_history, 
					@manually_added_points_only_for_redeem = @manually_added_points_only_for_redeem, 
					@imported_points_only_for_redeem       = @imported_points_only_for_redeem, 
					@promotion_point                       = @promotion_point,
					@cancel_promotion_point                = @cancel_promotion_point,
					@EndSession_RankingLevelPoints    =   @EndSession_RankingLevelPoints, 
					@Expired_RankingLevelPoints	  =   @Expired_RankingLevelPoints,
					@Manual_Add_RankingLevelPoints    =   @Manual_Add_RankingLevelPoints ,
					@Manual_Sub_RankingLevelPoints    =   @Manual_Sub_RankingLevelPoints ,
					@Manual_Set_RankingLevelPoints    =   @Manual_Set_RankingLevelPoints ,
					@EndSession_RedemptionPoints	  =   @EndSession_RedemptionPoints	,
					@Expired_RedemptionPoints	  =   @Expired_RedemptionPoints		,
					@Manual_Add_RedemptionPoints	  =   @Manual_Add_RedemptionPoints	,
					@Manual_Sub_RedemptionPoints	  =   @Manual_Sub_RedemptionPoints	,
					@Manual_Set_RedemptionPoints	  =   @Manual_Set_RedemptionPoints	,
					@LastMovementId_out                    = @LastMovementId                   OUTPUT, 
					@PointsGeneratedForLevel_out           = @PointsGeneratedForLevel          OUTPUT,
					@PointsDiscretionalForLevel_out        = @PointsDiscretionalForLevel       OUTPUT,
					@PointsDiscretionalOnlyForRedeem_out   = @PointsDiscretionalOnlyForRedeem  OUTPUT,
					@PointsPromotionOnlyForRedeem_out      = @PointsPromotionOnlyForRedeem     OUTPUT 

---------------------------------------------------------------------------------------------------------------------------------
-- Note:
-- @LastMovementId_out : Is the variable�s name of the output parameter of '@Sql execute'
-- When run 'EXEC sp_executesql' is finished, the value of @LastMovementId_out is assigned to @LastMovementId
---------------------------------------------------------------------------------------------------------------------------------

END  -- PROCEDURE [dbo].[AccountPointsCache_CalculatePoints]
GO


IF  EXISTS (SELECT   *
              FROM   sys.objects
             WHERE   object_id = OBJECT_ID(N'[dbo].[Update_PersonalInfo]')
               AND   type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[Update_PersonalInfo]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  CREATE PROCEDURE [dbo].[Update_PersonalInfo]
    @pCallingSiteId              int
  , @pAccountId                  bigint
  , @pTrackData                  nvarchar(50)
  , @pAccountCreated             Datetime 
  , @pHolderName                 nvarchar(200)
  , @pHolderId                   nvarchar(20)
  , @pHolderIdType               int
  , @pHolderAddress01            nvarchar(50)
  , @pHolderAddress02            nvarchar(50)
  , @pHolderAddress03            nvarchar(50)
  , @pHolderCity                 nvarchar(50)
  , @pHolderZip                  nvarchar(10) 
  , @pHolderEmail01              nvarchar(50)
  , @pHolderEmail02              nvarchar(50)
  , @pHolderTwitter              nvarchar(50)
  , @pHolderPhoneNumber01        nvarchar(20)
  , @pHolderPhoneNumber02        nvarchar(20)
  , @pHolderComments             nvarchar(100)
  , @pHolderId1                  nvarchar(20)
  , @pHolderId2                  nvarchar(20)
  , @pHolderDocumentId1          bigint
  , @pHolderDocumentId2          bigint
  , @pHolderName1                nvarchar(50)
  , @pHolderName2                nvarchar(50)
  , @pHolderName3                nvarchar(50)
  , @pHolderGender               int
  , @pHolderMaritalStatus        int
  , @pHolderBirthDate            datetime
  , @pHolderWeddingDate          datetime
  , @pHolderLevel                int
  , @pHolderLevelNotify          int
  , @pHolderLevelEntered         datetime
  , @pHolderLevelExpiration      datetime
  , @pPin                        nvarchar(12)
  , @pPinFailures                int
  , @pPinLastModified            datetime
  , @pBlocked                    bit
  , @pActivated                  bit
  , @pBlockReason                int
  , @pHolderIsVip                int
  , @pHolderTitle                nvarchar(15)                       
  , @pHolderName4                nvarchar(50)                       
  , @pHolderPhoneType01          int
  , @pHolderPhoneType02          int
  , @pHolderState                nvarchar(50)                    
  , @pHolderCountry              nvarchar(50) 
  , @pUserType                   int
  , @pPersonalInfoSeqId          int
  , @pPointsStatus               int
  , @pDeposit                    money
  , @pCardPay                    bit
  , @pBlockDescription           nvarchar(256) 
  , @pMSCreatedOnSiteId          Int 
  , @pExternalReference          nvarchar(50) 
  , @pHolderOccupation           nvarchar(50)         
  , @pHolderExtNum               nvarchar(10) 
  , @pHolderNationality          Int 
  , @pHolderBirthCountry         Int 
  , @pHolderFedEntity            Int 
  , @pHolderId1Type              Int -- RFC
  , @pHolderId2Type              Int -- CURP
  , @pHolderId3Type              nvarchar(50)   
  , @pHolderId3                  nvarchar(20) 
  , @pHolderHasBeneficiary       bit  
  , @pBeneficiaryName            nvarchar(200) 
  , @pBeneficiaryName1           nvarchar(50) 
  , @pBeneficiaryName2           nvarchar(50) 
  , @pBeneficiaryName3           nvarchar(50) 
  , @pBeneficiaryBirthDate       Datetime 
  , @pBeneficiaryGender          int 
  , @pBeneficiaryOccupation      nvarchar(50) 
  , @pBeneficiaryId1Type         int   -- RFC
  , @pBeneficiaryId1             nvarchar(20) 
  , @pBeneficiaryId2Type         int   -- CURP
  , @pBeneficiaryId2             nvarchar(20) 
  , @pBeneficiaryId3Type         nvarchar(50)  
  , @pBeneficiaryId3                nvarchar(20)   
  , @pHolderOccupationId         int
  , @pBeneficiaryOccupationId    int
  , @pOutputStatus               INT               OUTPUT
  , @pOutputAccountOtherId       BIGINT            OUTPUT
  AS
  BEGIN

  DECLARE @pOtherAccountId as BIGINT
  DECLARE @IsNewAccount    as BIT
  DECLARE @PreviousUserType as INT
  DECLARE @PreviousPersonalInfoSeqId as BIGINT
  DECLARE @NumAccounts as INT
  DECLARE @AcBlocked_new as BIT
  DECLARE @isELPMode01 AS BIT
  DECLARE @UpdatedTrackData as Bit
  DECLARE @NewTrackData AS nvarchar(50)
                
  SET @pOutputStatus = 0
  SET @pOutputAccountOtherId = 0
  SET @isELPMode01 = 0
  SET @pOtherAccountId = 0
  SET @UpdatedTrackData = 1
     
     
  /* Get ELP parameter  */
  SET @isELPMode01 = (SELECT CAST(GP_KEY_VALUE AS INT) FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PlayerTracking.ExternalLoyaltyProgram' AND GP_SUBJECT_KEY = 'Mode')
  
  SET @pUserType = ISNULL(@pUserType,CASE WHEN ISNULL(@pHolderLevel,0) > 0  THEN 1 ELSE 0 END)
  
  SET @NumAccounts = (SELECT COUNT(AC_ACCOUNT_ID) FROM ACCOUNTS WHERE AC_ACCOUNT_ID <> @pAccountId AND AC_HOLDER_ID = @pHolderId)
  IF @NumAccounts > 0 
  BEGIN
    SET @pOutputStatus += 1
  END
  
  IF (@isELPMode01 = 1 AND @pUserType = 1 )   
  BEGIN
    -- Trackdata don't update if Space is active and account is a personal account
    SET @UpdatedTrackData = 0
  END
  
  SELECT   @pOtherAccountId = AC_ACCOUNT_ID
    FROM   ACCOUNTS 
   WHERE   AC_TRACK_DATA = @pTrackData 
   
  SET @pOtherAccountId = ISNULL ((SELECT AC_ACCOUNT_ID FROM ACCOUNTS WHERE AC_TRACK_DATA = @pTrackData), 0)
  IF @pOtherAccountId <> 0 AND @pOtherAccountId <> @pAccountId
  BEGIN
    -- Don't recycle cards if Space is active and account is a personal account
    IF ( @UpdatedTrackData = 1 ) -- (@isELPMode01 = 0 ) OR (@isELPMode01 = 1 AND @pUserType = 0 )  
    BEGIN 
      UPDATE   ACCOUNTS 
         SET   AC_TRACK_DATA = '00000000000000000000-RECYCLED-' + CAST (@pOtherAccountId AS NVARCHAR) 
       WHERE   AC_ACCOUNT_ID = @pOtherAccountId

      SET @pOutputStatus += 2
      SET @pOutputAccountOtherId = @pOtherAccountId
    END    
  END

  SELECT @PreviousPersonalInfoSeqId = AC_MS_PERSONAL_INFO_SEQ_ID FROM ACCOUNTS WHERE AC_ACCOUNT_ID = @pAccountId 

  IF (@PreviousPersonalInfoSeqId IS NOT NULL AND @PreviousPersonalInfoSeqId > @pPersonalInfoSeqId)
  BEGIN
    SET @pOutputStatus += 4
    SELECT @pOutputAccountOtherId = ISNULL(AC_MS_LAST_SITE_ID, 0) FROM ACCOUNTS WHERE AC_ACCOUNT_ID = @pAccountId 
  END

  SET @IsNewAccount = 0
  IF NOT EXISTS (SELECT 1 FROM ACCOUNTS WHERE ac_account_id = @pAccountId )  
  BEGIN
    SET @IsNewAccount = 1

    INSERT INTO   ACCOUNTS 
                ( AC_ACCOUNT_ID, AC_TYPE, AC_BLOCKED, AC_MS_LAST_SITE_ID, AC_CREATED, AC_TRACK_DATA) 
         VALUES ( @pAccountId,         2,          1, @pCallingSiteId   , @pAccountCreated, '00000000000000000000-RECYCLED-' + CAST (@pAccountId AS NVARCHAR))
  END 
  ELSE
  BEGIN  
    SELECT @PreviousUserType = AC_USER_TYPE FROM ACCOUNTS WHERE AC_ACCOUNT_ID = @pAccountId     
    IF (@PreviousUserType = 1 AND @pUserType = 0) 
    BEGIN
      -- Personal account updated with anonymous account
      SET @pOutputStatus += 8
      SELECT @pOutputAccountOtherId = ISNULL(AC_MS_LAST_SITE_ID, 0) FROM ACCOUNTS WHERE AC_ACCOUNT_ID = @pAccountId 
      
      SET @pUserType = 1
    END
  END
  
  -- Don't recycle cards if Space is active and account is a personal account
   SELECT   @AcBlocked_new = CASE WHEN (AC_BLOCK_REASON & 0x20000) = 0x20000 THEN 1 ELSE @pBlocked END
          , @NewTrackData  = CASE WHEN @UpdatedTrackData = 1 THEN @pTrackData ELSE AC_TRACK_DATA  END
     FROM   ACCOUNTS
    WHERE   AC_ACCOUNT_ID = @pAccountId

  UPDATE   ACCOUNTS                   
     SET   AC_TRACK_DATA                = @NewTrackData
         , AC_HOLDER_NAME               = @pHolderName 
         , AC_HOLDER_ID                 = @pHolderId 
         , AC_HOLDER_ID_TYPE            = @pHolderIdType 
         , AC_HOLDER_ADDRESS_01         = @pHolderAddress01 
         , AC_HOLDER_ADDRESS_02         = @pHolderAddress02 
         , AC_HOLDER_ADDRESS_03         = @pHolderAddress03 
         , AC_HOLDER_CITY               = @pHolderCity 
         , AC_HOLDER_ZIP                = @pHolderZip  
         , AC_HOLDER_EMAIL_01           = @pHolderEmail01 
         , AC_HOLDER_EMAIL_02           = @pHolderEmail02 
         , AC_HOLDER_TWITTER_ACCOUNT    = @pHolderTwitter 
         , AC_HOLDER_PHONE_NUMBER_01    = @pHolderPhoneNumber01 
         , AC_HOLDER_PHONE_NUMBER_02    = @pHolderPhoneNumber02 
         , AC_HOLDER_COMMENTS           = @pHolderComments 
         , AC_HOLDER_ID1                = @pHolderId1 
         , AC_HOLDER_ID2                = @pHolderId2 
         , AC_HOLDER_DOCUMENT_ID1       = @pHolderDocumentId1 
         , AC_HOLDER_DOCUMENT_ID2       = @pHolderDocumentId2 
         , AC_HOLDER_NAME1              = @pHolderName1 
         , AC_HOLDER_NAME2              = @pHolderName2 
         , AC_HOLDER_NAME3              = @pHolderName3 
         , AC_HOLDER_GENDER             = @pHolderGender  
         , AC_HOLDER_MARITAL_STATUS     = @pHolderMaritalStatus 
         , AC_HOLDER_BIRTH_DATE         = @pHolderBirthDate 
         , AC_HOLDER_WEDDING_DATE       = @pHolderWeddingDate 
         , AC_HOLDER_LEVEL              = CASE WHEN (@IsNewAccount = 0) AND (AC_USER_TYPE = @pUserType)  THEN AC_HOLDER_LEVEL            ELSE @pHolderLevel           END
         , AC_HOLDER_LEVEL_NOTIFY       = CASE WHEN (@IsNewAccount = 0) AND (AC_USER_TYPE = @pUserType)  THEN AC_HOLDER_LEVEL_NOTIFY     ELSE @pHolderLevelNotify     END 
         , AC_HOLDER_LEVEL_ENTERED      = CASE WHEN (@IsNewAccount = 0) AND (AC_USER_TYPE = @pUserType)  THEN AC_HOLDER_LEVEL_ENTERED    ELSE @pHolderLevelEntered    END  
         , AC_HOLDER_LEVEL_EXPIRATION   = CASE WHEN (@IsNewAccount = 0) AND (AC_USER_TYPE = @pUserType)  THEN AC_HOLDER_LEVEL_EXPIRATION ELSE @pHolderLevelExpiration END   
         , AC_PIN                       = @pPin 
         , AC_PIN_FAILURES              = @pPinFailures 
         , AC_PIN_LAST_MODIFIED         = @pPinLastModified 
         , AC_BLOCKED                   = @AcBlocked_new
         , AC_ACTIVATED                 = @pActivated 
         , AC_BLOCK_REASON              = CASE WHEN @AcBlocked_new = 0 THEN 0 ELSE (AC_BLOCK_REASON | dbo.BlockReason_ToNewEnumerate(@pBlockReason)) END 
         , AC_HOLDER_IS_VIP             = @pHolderIsVip 
         , AC_HOLDER_TITLE              = @pHolderTitle 
         , AC_HOLDER_NAME4              = @pHolderName4         
         , AC_HOLDER_PHONE_TYPE_01      = @pHolderPhoneType01  
         , AC_HOLDER_PHONE_TYPE_02      = @pHolderPhoneType02  
         , AC_HOLDER_STATE              = @pHolderState    
         , AC_HOLDER_COUNTRY            = @pHolderCountry   
         , AC_USER_TYPE                 = CASE WHEN (AC_USER_TYPE = 1  AND @pUserType = 0) THEN AC_USER_TYPE ELSE @pUserType END -- ! personal account --> anonymous ��IS NOT POSSIBLE!!!
         , AC_POINTS_STATUS             = CASE WHEN (@pUserType = 0) THEN NULL ELSE CASE WHEN (@IsNewAccount = 0) THEN @pPointsStatus ELSE 0 END END           
         , AC_DEPOSIT                   = @pDeposit 
         , AC_CARD_PAID                 = @pCardPay 
         , AC_BLOCK_DESCRIPTION         = @pBlockDescription          
         , AC_MS_CREATED_ON_SITE_ID     = ISNULL(AC_MS_CREATED_ON_SITE_ID, ISNULL(@pMSCreatedOnSiteId, @pCallingSiteId))
         , AC_EXTERNAL_REFERENCE        = ISNULL(@pExternalReference, AC_EXTERNAL_REFERENCE)
         , AC_HOLDER_OCCUPATION         = @pHolderOccupation
         , AC_HOLDER_EXT_NUM            = @pHolderExtNum
         , AC_HOLDER_NATIONALITY        = @pHolderNationality 
         , AC_HOLDER_BIRTH_COUNTRY      = @pHolderBirthCountry 
         , AC_HOLDER_FED_ENTITY         = @pHolderFedEntity
         , AC_HOLDER_ID1_TYPE           = @pHolderId1Type 
         , AC_HOLDER_ID2_TYPE           = @pHolderId2Type 
         , AC_HOLDER_ID3_TYPE           = @pHolderId3Type 
         , AC_HOLDER_ID3                = @pHolderId3
         , AC_HOLDER_HAS_BENEFICIARY    = @pHolderHasBeneficiary 
         , AC_BENEFICIARY_NAME          = @pBeneficiaryName 
         , AC_BENEFICIARY_NAME1         = @pBeneficiaryName1
         , AC_BENEFICIARY_NAME2         = @pBeneficiaryName2
         , AC_BENEFICIARY_NAME3         = @pBeneficiaryName3
         , AC_BENEFICIARY_BIRTH_DATE    = @pBeneficiaryBirthDate 
         , AC_BENEFICIARY_GENDER        = @pBeneficiaryGender 
         , AC_BENEFICIARY_OCCUPATION    = @pBeneficiaryOccupation
         , AC_BENEFICIARY_ID1_TYPE      = @pBeneficiaryId1Type 
         , AC_BENEFICIARY_ID1           = @pBeneficiaryId1 
         , AC_BENEFICIARY_ID2_TYPE      = @pBeneficiaryId2Type 
         , AC_BENEFICIARY_ID2           = @pBeneficiaryId2 
         , AC_BENEFICIARY_ID3_TYPE      = @pBeneficiaryId3Type 
         , AC_BENEFICIARY_ID3           = @pBeneficiaryId3 
         , AC_HOLDER_OCCUPATION_ID      = ISNULL(@pHolderOccupationId, AC_HOLDER_OCCUPATION_ID)
         , AC_BENEFICIARY_OCCUPATION_ID = ISNULL(@pBeneficiaryOccupationId, AC_BENEFICIARY_OCCUPATION_ID)
   WHERE   AC_ACCOUNT_ID                = @pAccountId 
       
END

GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetBucketValue]') AND type in (N'F', N'FN'))
DROP FUNCTION [dbo].[GetBucketValue]
GO

CREATE FUNCTION [dbo].[GetBucketValue]  (@BucketId bigint, @AccountId bigint) 
RETURNS DECIMAL(12,2)
AS
BEGIN
  DECLARE @Result DECIMAL(12,2)
  
  
  SELECT @Result  = ROUND ( CBU_VALUE , 2 , 1 )  -- When the third parameter != 0 it truncates rather than rounds
  FROM CUSTOMER_BUCKET 
  WHERE CBU_BUCKET_ID = @BucketId
  AND CBU_CUSTOMER_ID = @AccountId
  
  RETURN ISNULL(@Result ,0)

END -- GetBucketValue

GO
GRANT EXECUTE ON [dbo].[GetBucketValue] TO [wggui] WITH GRANT OPTION
/****** STORED *******/

