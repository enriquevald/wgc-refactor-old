/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_100]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 45;

SET @New_ReleaseId = 46;
SET @New_ScriptName = N'UpdateTo_18.046.sql';
SET @New_Description = N'New task UploadCashierMovementsGrupedByHour ';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/

/* Create Table cashier_movements_grouped_by_hour */
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[cashier_movements_grouped_by_hour]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[cashier_movements_grouped_by_hour](
      [cm_site_id] [int] NOT NULL,
      [cm_date] [datetime] NOT NULL,
      [cm_type] [int] NOT NULL,
      [cm_sub_type] [int] NOT NULL,
      [cm_currency_iso_code] [nvarchar](3) NOT NULL,
      [cm_currency_denomination] [money] NOT NULL,
      [cm_type_count] [int] NOT NULL,
      [cm_sub_amount] [money] NULL,
      [cm_add_amount] [money] NULL,
      [cm_aux_amount] [money] NULL,
      [cm_initial_balance] [money] NULL,
      [cm_final_balance] [money] NULL,
      [cm_timestamp] [bigint] NULL,
      [cm_unique_id] [bigint] NOT NULL,
CONSTRAINT [PK_cashier_movements_grouped_by_hour] PRIMARY KEY CLUSTERED 
(     [cm_site_id] ASC,
      [cm_unique_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

/****** INDEXES ******/

/* IX_cmgh_site_id_date_type_sub_type_currency_iso_code_currency_denomination  */
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[cashier_movements_grouped_by_hour]') AND name = N'IX_cmgh_site_id_date_type_sub_type_currency_iso_code_currency_denomination')
CREATE NONCLUSTERED INDEX [IX_cmgh_site_id_date_type_sub_type_currency_iso_code_currency_denomination] ON [dbo].[cashier_movements_grouped_by_hour] 
(     [cm_site_id] ASC,
      [cm_date] ASC,
      [cm_type] ASC,
      [cm_sub_type] ASC,
      [cm_currency_iso_code] ASC,
      [cm_currency_denomination] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

/* IX_cmgh_site_id_type_sub_type_date */
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[cashier_movements_grouped_by_hour]') AND name = N'IX_cmgh_site_id_type_sub_type_date')
CREATE NONCLUSTERED INDEX [IX_cmgh_site_id_type_sub_type_date] ON [dbo].[cashier_movements_grouped_by_hour] 
(
      [cm_site_id] ASC,
      [cm_type] ASC,
      [cm_sub_type] ASC,
      [cm_date] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

/* IX_cmgh_date **/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[cashier_movements_grouped_by_hour]') AND name = N'IX_cmgh_date')
CREATE NONCLUSTERED INDEX [IX_cmgh_date] ON [dbo].[cashier_movements_grouped_by_hour] 
(     [cm_date] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

/******* STORED PROCEDURES *******/

  --------------------------------------------------------------------------------
  -- Copyright � 2013 Win Systems International
  --------------------------------------------------------------------------------
  --
  --   MODULE NAME: Update_CashierMovementsGrupedByHour.sql
  --
  --   DESCRIPTION: Update_CashierMovementsGrupedByHour
  --
  --        AUTHOR: Jos� Mart�nez
  --
  -- CREATION DATE: 27-FEB-2014
  --
  -- REVISION HISTORY:
  --
  -- Date        Author Description
  -- ----------- ------ ----------------------------------------------------------
  -- 27-FEB-2014 JML    First release.  
  -------------------------------------------------------------------------------- 
  
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Update_CashierMovementsGrupedByHour]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[Update_CashierMovementsGrupedByHour]
GO
 
CREATE PROCEDURE  [dbo].[Update_CashierMovementsGrupedByHour]
                  @pSiteId               INT  
                , @pDate                 DATETIME
                , @pType                 INT
                , @pSubType              INT
                , @pCurrencyIsoCode      NVARCHAR(3)
                , @pCurrencyDenomination MONEY
                , @pTypeCount            INT
                , @pSubAmount            MONEY
                , @pAddAmount            MONEY
                , @pAuxAmount            MONEY
                , @pInitialBalance       MONEY
                , @pFinalBalance         MONEY
                , @pTimeStamp            BIGINT
                , @pUniqueId             BIGINT
AS
BEGIN   

  IF EXISTS (SELECT   1 
               FROM   CASHIER_MOVEMENTS_GROUPED_BY_HOUR
              WHERE   CM_SITE_ID      = @pSiteId 
                AND   CM_UNIQUE_ID    = @pUniqueId )
  BEGIN
      UPDATE   CASHIER_MOVEMENTS_GROUPED_BY_HOUR
         SET   CM_TYPE_COUNT       = @pTypeCount 
             , CM_SUB_AMOUNT       = @pSubAmount   
             , CM_ADD_AMOUNT       = @pAddAmount 
             , CM_AUX_AMOUNT       = @pAuxAmount 
             , CM_INITIAL_BALANCE  = @pInitialBalance
             , CM_FINAL_BALANCE    = @pFinalBalance 
             , CM_TIMESTAMP        = @pTimeStamp 
       WHERE   CM_SITE_ID        = @pSiteId 
         AND   CM_UNIQUE_ID      = @pUniqueId 
  END
  ELSE
  BEGIN
      INSERT INTO   CASHIER_MOVEMENTS_GROUPED_BY_HOUR
                  ( CM_SITE_ID
                  , CM_DATE
                  , CM_TYPE
                  , CM_SUB_TYPE
                  , CM_CURRENCY_ISO_CODE
                  , CM_CURRENCY_DENOMINATION
                  , CM_TYPE_COUNT
                  , CM_SUB_AMOUNT
                  , CM_ADD_AMOUNT
                  , CM_AUX_AMOUNT
                  , CM_INITIAL_BALANCE
                  , CM_FINAL_BALANCE
                  , CM_TIMESTAMP 
                  , CM_UNIQUE_ID )
          VALUES
                  ( @pSiteId    
                  , @pDate  
                  , @pType 
                  , @pSubType 
                  , @pCurrencyIsoCode 
                  , @pCurrencyDenomination 
                  , @pTypeCount 
                  , @pSubAmount 
                  , @pAddAmount 
                  , @pAuxAmount 
                  , @pInitialBalance 
                  , @pFinalBalance 
                  , @pTimeStamp  
                  , @pUniqueId )
  END
END
GO 

  --------------------------------------------------------------------------------
  -- Copyright � 2013 Win Systems International
  --------------------------------------------------------------------------------
  --
  --   MODULE NAME: MultiSite_CashierMovementsGroupedByHour
  --
  --   DESCRIPTION: Stored Procedure that gets the cashier movements by hour and site
  --
  --        AUTHOR: Joan Marc Pepi� Jamison
  --
  -- CREATION DATE: 17-MAR-2014
  --
  -- REVISION HISTORY:
  --
  -- Date        Author Description
  -- ----------- ------ ----------------------------------------------------------
  -- 17-MAR-2014 JPJ    First release.  
  -------------------------------------------------------------------------------- 

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MultiSite_CashierMovementsGroupedByHour]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[MultiSite_CashierMovementsGroupedByHour]
GO
CREATE PROCEDURE [dbo].[MultiSite_CashierMovementsGroupedByHour]        
         @pSiteId    NVARCHAR(MAX)
        ,@pDateFrom  DATETIME
        ,@pDateTo    DATETIME       
  AS
BEGIN 
DECLARE @_sql NVARCHAR(MAX)

  SET @_sql ='
			  SELECT   NULL
				        ,CM_TYPE
				        ,CM_SUB_TYPE
				        ,CM_TYPE_COUNT							
				        ,CM_CURRENCY_ISO_CODE
				        ,CM_CURRENCY_DENOMINATION
				        ,CM_SUB_AMOUNT
				        ,CM_ADD_AMOUNT
				        ,CM_AUX_AMOUNT
				        ,CM_INITIAL_BALANCE
				        ,CM_FINAL_BALANCE
			  FROM    CASHIER_MOVEMENTS_GROUPED_BY_HOUR 
			  WHERE   CM_DATE >= ''' + CONVERT(VARCHAR, @pDateFrom, 21) + '''  
			          AND CM_DATE < ''' + CONVERT(VARCHAR, @pDateTo, 21) +''''
  	
	  IF (@pSiteId IS NOT NULL)
	     SET @_sql = @_sql + ' AND CM_SITE_ID IN (' + @pSiteId + ')'
  		
    EXEC sp_executesql @_sql
					
END -- MultiSite_CashierMovementsGroupedByHour
GO

GRANT EXECUTE ON [dbo].[MultiSite_CashierMovementsGroupedByHour] TO [wggui] WITH GRANT OPTION
GO

/****** RECORDS ******/

/* General Params */
IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'RegionalOptions' AND GP_SUBJECT_KEY = 'CurrencyISOCode')
  INSERT INTO [GENERAL_PARAMS] ([GP_GROUP_KEY], [GP_SUBJECT_KEY], [GP_KEY_VALUE])
  VALUES ('RegionalOptions', 'CurrencyISOCode', 'MXN')
GO

IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY = 'Tax.OnPrize.1.Name')
  INSERT INTO [GENERAL_PARAMS] ([GP_GROUP_KEY], [GP_SUBJECT_KEY], [GP_KEY_VALUE])
  VALUES ('Cashier', 'Tax.OnPrize.1.Name', 'Impuesto Federal')
GO

IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY = 'Tax.OnPrize.2.Name')
  INSERT INTO [GENERAL_PARAMS] ([GP_GROUP_KEY], [GP_SUBJECT_KEY], [GP_KEY_VALUE])
  VALUES ('Cashier', 'Tax.OnPrize.2.Name', 'Impuesto Estatal')
GO

IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY = 'Split.A.Name')
  INSERT INTO [GENERAL_PARAMS] ([GP_GROUP_KEY], [GP_SUBJECT_KEY], [GP_KEY_VALUE])
  VALUES ('Cashier', 'Split.A.Name', 'Empresa A')
GO

IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY = 'Split.B.Name')
  INSERT INTO [GENERAL_PARAMS] ([GP_GROUP_KEY], [GP_SUBJECT_KEY], [GP_KEY_VALUE])
  VALUES ('Cashier', 'Split.B.Name', 'Empresa B')
GO

/* Task UploadCashierMovementsGrupedByHour*/
SELECT ST_SITE_ID AS SITE_ID INTO #CURRENT_SITES FROM MS_SITE_TASKS GROUP BY ST_SITE_ID
INSERT INTO MS_SITE_TASKS (ST_TASK_ID, ST_ENABLED, ST_INTERVAL_SECONDS, ST_MAX_ROWS_TO_UPLOAD, ST_SITE_ID) 
                   SELECT          63,          1,                3600,                   200,    SITE_ID  FROM #CURRENT_SITES;
DROP TABLE #CURRENT_SITES
GO

USE [master]
GO
/*****************CLR ENABLED**************/
sp_configure 'clr enabled', 1;
GO
reconfigure
GO
