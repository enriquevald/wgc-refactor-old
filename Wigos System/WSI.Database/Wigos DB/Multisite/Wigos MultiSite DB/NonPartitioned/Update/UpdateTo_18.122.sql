/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_100]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 121;

SET @New_ReleaseId = 122;
SET @New_ScriptName = N'UpdateTo_18.122.sql';
SET @New_Description = N'New release v03.007.0033'; 

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/**** GENERAL PARAM *****/



/******* TABLES  *******/



/******* RECORDS *******/



/******* PROCEDURES *******/
/* Function: IntToBin*/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[IntToBin]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[IntToBin]
GO

CREATE FUNCTION [dbo].[IntToBin]
(
	@value INT
)
	RETURNS VARCHAR(1000)
AS
	BEGIN
	DECLARE @result VARCHAR(1000) = '';
	DECLARE @fixedSize INT = '8';
	 WHILE (@value != 0)
	 BEGIN
	  IF(@value%2 = 0) 
	   SET @Result = '0' + @Result;
	  ELSE
	   SET @Result = '1' + @Result;
	   
	  SET @value = @value / 2;
	 END;
	 IF(@FixedSize > 0 AND LEN(@Result) < @FixedSize)
	  SET @result = RIGHT('00000000000000000000' + @Result, @FixedSize);
	RETURN @Result;
	END
GO

/* Function: BinToInt*/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[BinToInt]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[BinToInt]
GO

CREATE FUNCTION [dbo].[BinToInt]
(
	@Input varchar(255)
)
RETURNS bigint
AS
BEGIN

	DECLARE @Cnt tinyint = 1
	DECLARE @Len tinyint = LEN(@Input)
	DECLARE @Output bigint = CAST(SUBSTRING(@Input, @Len, 1) AS bigint)

	WHILE(@Cnt < @Len) BEGIN
		SET @Output = @Output + POWER(CAST(SUBSTRING(@Input, @Len - @Cnt, 1) * 2 AS bigint), @Cnt)

		SET @Cnt = @Cnt + 1
	END
	RETURN @Output
END
GO

/* CompareTwoAnomaliesMasks */
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CompareTwoAnomaliesMasks]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[CompareTwoAnomaliesMasks]
GO

CREATE FUNCTION [dbo].[CompareTwoAnomaliesMasks]
(
	@anomalia_1 INT,
	@anomalia_2 INT
)
RETURNS INT
AS
BEGIN
	DECLARE @index INT = 1;
	DECLARE @first VARCHAR(8);
	DECLARE @second VARCHAR(8);
	DECLARE @return_string VARCHAR(8);
	
	SET @return_string = '';	
	SET @first = CONVERT(VARCHAR(8),(SELECT dbo.IntToBin(@anomalia_1)));
	SET @second = CONVERT(VARCHAR(8),(SELECT dbo.IntToBin(@anomalia_2)));

	WHILE (@index <= LEN(@first))
		BEGIN 
			IF (SUBSTRING(@first, @index,1)=1 AND SUBSTRING(@second, @index,1)=1)
					SET @return_string += '1';
			IF (SUBSTRING(@first, @index,1)=0 AND SUBSTRING(@second, @index,1)=0)
					SET @return_string += '0';
			IF	((SUBSTRING(@first, @index,1)=0 AND SUBSTRING(@second, @index,1)=1) OR (SUBSTRING(@first, @index,1)=1 AND SUBSTRING(@second, @index,1)=0))
					SET @return_string += '1';
			SET @index += 1;
		END
	RETURN dbo.BinToInt(@return_string)
END
GO


/******* TRIGGERS *******/
