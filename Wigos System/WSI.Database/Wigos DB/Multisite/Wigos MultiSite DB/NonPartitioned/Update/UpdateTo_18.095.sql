/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_100]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 94;

SET @New_ReleaseId = 95;
SET @New_ScriptName = N'UpdateTo_18.095.sql';
SET @New_Description = N'New buckets 8 and 9';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/

/****** INDEXES ******/

/****** RECORDS ******/

IF NOT EXISTS (SELECT 1 FROM BUCKETS WHERE bu_bucket_id = 8)
	INSERT INTO dbo.buckets ([bu_bucket_id], [bu_name], [bu_enabled], [bu_system_type], [bu_bucket_type], [bu_visible_flags], [bu_order_on_reports], 
	                         [bu_expiration_days], [bu_expiration_date], [bu_level_flags], [bu_k_factor], [BU_MASTER_SEQUENCE_ID])   
	VALUES ( 8,  'Puntos de canje discrecionales', 1, 0, 8,  '4294967291', 1, 0, Null, 1, 0, Null)
GO

IF NOT EXISTS (SELECT 1 FROM BUCKETS WHERE bu_bucket_id = 9)
	INSERT INTO dbo.buckets ([bu_bucket_id], [bu_name], [bu_enabled], [bu_system_type], [bu_bucket_type], [bu_visible_flags], [bu_order_on_reports], 
	                         [bu_expiration_days], [bu_expiration_date], [bu_level_flags], [bu_k_factor], [BU_MASTER_SEQUENCE_ID])   
	VALUES ( 9,  'Puntos de canje generados', 1, 0, 9,  '4294967291', 1, 0, Null, 1, 0, Null)

GO


  SET NOCOUNT ON
  DECLARE @study_period                                    AS INT;
  DECLARE @date_from                                       AS DATETIME;
  DECLARE @now                                             AS DATETIME;
  --DECLARE @today_opening                                   AS DATETIME;
  
  DECLARE @AccountId                          BIGINT
  DECLARE @PointsGeneratedForLevel_8          MONEY 
  DECLARE @PointsDiscretionalForLevel_9       MONEY 
  DECLARE @PointsBucket2                      MONEY

  DECLARE @points_awarded                     AS INT
  DECLARE @manually_added_points_for_level    AS INT
  DECLARE @imported_points_for_level          AS INT
  DECLARE @imported_points_history            AS INT
  DECLARE @EndSession_RankingLevelPoints             AS INT
  DECLARE @Expired_RankingLevelPoints                AS INT
  DECLARE @Manual_Add_RankingLevelPoints                 AS INT
  DECLARE @Manual_Sub_RankingLevelPoints                 AS INT
  DECLARE @Manual_Set_RankingLevelPoints                 AS INT

  -- Points for level
    -- Generated
  SET @points_awarded                        = 36
  --PointsAwarded
     -- Discretional
  SET @manually_added_points_for_level       = 68
  --ManuallyAddedPointsForLevel
  SET @imported_points_for_level             = 72
  --ImportedPointsForLevel
  SET @imported_points_history               = 73
  --ImportedPointsHistory
  SET @EndSession_RankingLevelPoints              = 1102
  --'MULTIPLE_BUCKETS_END_SESSION + RankingLevelPoints PuntosNivel'
  SET @Expired_RankingLevelPoints                 = 1202
  --'MULTIPLE_BUCKETS_Expired + RankingLevelPoints PuntosNivel'
  SET @Manual_Add_RankingLevelPoints              = 1302
  --'MULTIPLE_BUCKETS_Manual_Add + RankingLevelPoints PuntosNivel'
  SET @Manual_Sub_RankingLevelPoints              = 1402
  --'MULTIPLE_BUCKETS_Manual_Sub + RankingLevelPoints PuntosNivel'
  SET @Manual_Set_RankingLevelPoints              = 1502
  --'MULTIPLE_BUCKETS_Manual_Set + RankingLevelPoints PuntosNivel'


  SET @study_period = (SELECT CAST(GP_KEY_VALUE AS INT) FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PlayerTracking' AND GP_SUBJECT_KEY = 'Levels.DaysCountingPoints')
  --_day_start_counting_points = Misc.TodayOpening().AddDays(-(_days_counting_points - 1));


  SET @now = GETDATE();
  --SET @today_opening = dbo.Opening(0, @now)

  --SET @date_from = DATEADD(DAY, -1*(@study_period-1), @today_opening);
  SET @date_from = DATEADD(DAY, -1*(@study_period-1), @now);

  DELETE FROM CUSTOMER_BUCKET WHERE cbu_bucket_id in(8,9)

-- bucket 8
    INSERT INTO CUSTOMER_BUCKET(cbu_customer_id,cbu_bucket_id,cbu_value,cbu_updated)

                        SELECT   am_account_id
                                   ,8
                                   ,SUM(ISNULL(AM_ADD_AMOUNT,0)-ISNULL(AM_SUB_AMOUNT,0) )
                                   ,GETDATE()
                              
                        FROM Account_Movements
                        WHERE AM_TYPE IN (@points_awarded,@EndSession_RankingLevelPoints, @Expired_RankingLevelPoints)
                        and am_datetime >= @date_from 
                        and am_datetime < @now--@today_opening
                        GROUP BY am_account_id
    
-- bucket 9
    INSERT INTO CUSTOMER_BUCKET(cbu_customer_id,cbu_bucket_id,cbu_value,cbu_updated)

                        SELECT   am_account_id
                                   ,9
                                   ,SUM(ISNULL(AM_ADD_AMOUNT,0)-ISNULL(AM_SUB_AMOUNT,0)) 
                                   ,GETDATE()
                              
                        FROM Account_Movements
                        WHERE AM_TYPE IN (@manually_added_points_for_level, @imported_points_for_level, @imported_points_history, 
                                                           @Manual_Add_RankingLevelPoints, @Manual_Sub_RankingLevelPoints, @Manual_Set_RankingLevelPoints)
                        and am_datetime >= @date_from 
                        and am_datetime < @now--@today_opening
                        GROUP BY am_account_id




-- BUCKET 2 <> BUCKET 8 + 9
-- b. if bucket_2 <> bucket_8 + bucket_9     then update bucket_2 = bucket_8 + bucket_9

    UPDATE 
      CUSTOMER_BUCKET SET CBU_VALUE = CBU8.VALUE
        FROM CUSTOMER_BUCKET CB 
        INNER JOIN 
        (
                SELECT   CB2.CBU_CUSTOMER_ID
                        ,( CB2.CBU_VALUE - CB9.CBU_VALUE) VALUE  
                  FROM  CUSTOMER_BUCKET CB2
            INNER JOIN  CUSTOMER_BUCKET CB8 ON CB2.CBU_CUSTOMER_ID = CB8.CBU_CUSTOMER_ID
            INNER JOIN  CUSTOMER_BUCKET CB9 ON CB2.CBU_CUSTOMER_ID = CB9.CBU_CUSTOMER_ID
                 WHERE  CB2.CBU_BUCKET_ID = 2
                        AND CB8.CBU_BUCKET_ID = 8
                        AND CB9.CBU_BUCKET_ID = 9
                        AND CB8.CBU_VALUE <> CB2.CBU_VALUE - CB9.CBU_VALUE 
        ) CBU8
        ON CBU8.CBU_CUSTOMER_ID = CB.CBU_CUSTOMER_ID
        WHERE CB.CBU_BUCKET_ID = 8


-- a. if bucket_2 == null                    then INSERT bucket_2       -->  (bucket_2 = bucket_8 + bucket_9)
-- NO EXISTE BUCKET 2 
    INSERT INTO CUSTOMER_BUCKET (cbu_bucket_id,cbu_customer_id,cbu_updated,cbu_value) 
            SELECT    2
                    , CB8.CBU_CUSTOMER_ID, GETDATE()
                    , (CB8.CBU_VALUE + CB9.CBU_VALUE) VALUE
              FROM  CUSTOMER_BUCKET CB8 
        INNER JOIN  CUSTOMER_BUCKET CB9 ON CB8.CBU_CUSTOMER_ID = CB9.CBU_CUSTOMER_ID
         LEFT JOIN  CUSTOMER_BUCKET CB2 ON CB8.CBU_CUSTOMER_ID = CB2.CBU_CUSTOMER_ID AND CB2.CBU_BUCKET_ID = 2
             WHERE  CB8.CBU_BUCKET_ID = 8
                    AND CB9.CBU_BUCKET_ID = 9
                    AND CB2.CBU_CUSTOMER_ID IS NULL


GO

/****** STORED *******/

IF EXISTS( SELECT 1 FROM sys.objects WHERE name = 'Insert_Update_Customer_Bucket') 
	DROP PROCEDURE Insert_Update_Customer_Bucket
GO

CREATE PROCEDURE Insert_Update_Customer_Bucket (@pAccountId BIGINT,@pBucketId BIGINT,@pPlayerTrackingMode INT ,@IsDelta BIT, @LocalDelta MONEY)
AS
BEGIN
	-- Inserts or updates a record in customer_bucket depending on the existence
	-- called from Update_PointsAccountMovement

    IF NOT EXISTS (SELECT   1
                     FROM   CUSTOMER_BUCKET
                    WHERE   CBU_CUSTOMER_ID = @pAccountId
                      AND   CBU_BUCKET_ID   = @pBucketId)
    BEGIN
      INSERT INTO   CUSTOMER_BUCKET(CBU_CUSTOMER_ID, CBU_BUCKET_ID, CBU_VALUE, CBU_UPDATED)
           VALUES  (@pAccountId, @pBucketId, 0, GETDATE())
    END

    UPDATE   CUSTOMER_BUCKET
       SET   CBU_VALUE        = CASE WHEN (@pPlayerTrackingMode = 1)
                                         THEN CBU_VALUE
                                         ELSE (CASE WHEN (@IsDelta = 1)THEN CBU_VALUE + @LocalDelta
                                                                       ELSE @LocalDelta
                                               END)
                                END
           , CBU_UPDATED      = GETDATE()
     WHERE   CBU_CUSTOMER_ID  = @pAccountId
       AND   CBU_BUCKET_ID    = @pBucketId
END

GO

IF EXISTS( SELECT 1 FROM sys.objects WHERE NAME = 'Update_PointsAccountMovement')
  DROP PROCEDURE [dbo].[Update_PointsAccountMovement]
GO

CREATE PROCEDURE [dbo].[Update_PointsAccountMovement]
     @pSiteId                   INT
,    @pMovementId               BIGINT
,    @pPlaySessionId            BIGINT
,    @pAccountId                BIGINT
,    @pTerminalId               INT
,    @pWcpSequenceId            BIGINT
,    @pWcpTransactionId         BIGINT
,    @pDatetime                 DATETIME
,    @pType                     INT
,    @pInitialBalance           MONEY
,    @pSubAmount                MONEY
,    @pAddAmount                MONEY
,    @pFinalBalance             MONEY
,    @pCashierId                INT
,    @pCashierName              NVARCHAR(50)
,    @pTperminalName            NVARCHAR(50)
,    @pOperationId              BIGINT
,    @pDetails                  NVARCHAR(256)
,    @pReasons                  NVARCHAR(64)
,    @pPlayerTrackingMode       INT
,    @pErrorCode                INT             OUTPUT
,    @pPointsSequenceId         BIGINT          OUTPUT
,    @pPoints                   MONEY           OUTPUT
,    @pCenterInitialBalance     MONEY           OUTPUT
,    @pBucketId                 BIGINT          OUTPUT
AS
BEGIN
  DECLARE @LocalDelta           AS MONEY
  DECLARE @IsDelta              AS BIT
  DECLARE @CheckAccountId       AS BIGINT -- For check if exists account

  DECLARE @ValueForSpecialBucket	AS INT





  -- Output parameters
  SET @pErrorCode = 1
  SET @pPointsSequenceId = NULL
  SET @pPoints = NULL
  SET @pCenterInitialBalance = NULL
  SET @pBucketId = NULL

  -- Local variables
  SET @LocalDelta = 0
  SET @IsDelta = 1
  SET @CheckAccountId = NULL
  SET @ValueForSpecialBucket = 0


  SET NOCOUNT ON;

  -- Set BucketId
  IF (@pType >= 1100 AND @pType <= 1599)
  BEGIN
    SET  @pBucketId = (@pType % 100) -- Last 2 d�gits
  END
  ELSE
  BEGIN
    IF (@pType In ( 37, 38, 39, 40, 41, 46, 50, 60, 61, 66, 71, 101))  -- 36 was included
      SET  @pBucketId = 1    -- BucketId.RedemptionPoints
    ELSE
      SET  @pBucketId = 2    -- BucketId.RankingLevelPoints
  END

  IF (@pType = 36 OR @pType = 1102 OR @pType = 1202)  -- MULTIPLE_BUCKETS_EndSession MULTIPLE_BUCKETS_Expired de bucket RankingLevelPoints
  BEGIN
	SET @ValueForSpecialBucket = 8 --BucketId.RankingLevelPoints_Generated
  END
  
  IF (@pType = 68 OR @pType = 72 OR @pType = 73 OR @pType = 1302 OR @pType = 1402 OR @pType = 1502) --MovementType ImportedPointsForLevel
  BEGIN
	SET @ValueForSpecialBucket = 9 --BucketId.RankingLevelPoints_Discretional
  END

 
  -- The Bucket does not exist in table BUCKETS
  IF NOT EXISTS(SELECT   1
                  FROM   BUCKETS
                 WHERE   BU_BUCKET_ID = @pBucketId)
  BEGIN
    SET @pErrorCode = 1
    RETURN
  END

  SELECT   @pCenterInitialBalance = CBU_VALUE
    FROM   CUSTOMER_BUCKET
   WHERE   CBU_CUSTOMER_ID        = @pAccountId
     AND   CBU_BUCKET_ID          = @pBucketId

  SET @pCenterInitialBalance = ISNULL(@pCenterInitialBalance, 0)

  SELECT   @CheckAccountId = AC_ACCOUNT_ID
    FROM   ACCOUNTS
   WHERE   AC_ACCOUNT_ID   = @pAccountId

  -- The Account does not exist
  IF (@CheckAccountId IS NULL)
  BEGIN
    SET @pErrorCode = 1
    RETURN
  END

  IF NOT EXISTS (SELECT   1
                   FROM   ACCOUNT_MOVEMENTS
                  WHERE   AM_SITE_ID     = @pSiteId
                    AND   AM_MOVEMENT_ID = @pMovementId)
  BEGIN
    INSERT INTO   ACCOUNT_MOVEMENTS
    (
        AM_SITE_ID
      , AM_MOVEMENT_ID
      , AM_PLAY_SESSION_ID
      , AM_ACCOUNT_ID
      , AM_TERMINAL_ID
      , AM_WCP_SEQUENCE_ID
      , AM_WCP_TRANSACTION_ID
      , AM_DATETIME
      , AM_TYPE
      , AM_INITIAL_BALANCE
      , AM_SUB_AMOUNT
      , AM_ADD_AMOUNT
      , AM_FINAL_BALANCE
      , AM_CASHIER_ID
      , AM_CASHIER_NAME
      , AM_TERMINAL_NAME
      , AM_OPERATION_ID
      , AM_DETAILS
      , AM_REASONS
    )
    VALUES
    (
        @pSiteId
      , @pMovementId
      , @pPlaySessionId
      , @pAccountId
      , @pTerminalId
      , @pWcpSequenceId
      , @pWcpTransactionId
      , @pDatetime
      , @pType
      , @pInitialBalance
      , @pSubAmount
      , @pAddAmount
      , @pFinalBalance
      , @pCashierId
      , @pCashierName
      , @pTperminalName
      , @pOperationId
      , @pDetails
      , @pReasons
    )

  SET @LocalDelta = ISNULL(@pAddAmount, 0) - ISNULL(@pSubAmount, 0)

  -- Points Gift Delivery/Points Gift Services/Points Status/Imported Points History
  --IF (@pType IN (39, 42, 67, 62, 73))  SET @LocalDelta = 0
	IF (@pType IN (39, 42, 62, 67))  SET @LocalDelta = 0

  -- Multisite initial points	- @IsDelta = 0 are like original points on install
  IF (@pType = 101) SET @IsDelta = 0

  -- Update Account
  UPDATE   ACCOUNTS
     SET   AC_MS_LAST_SITE_ID  = @pSiteId
         , AC_LAST_ACTIVITY    = GETDATE()
   WHERE   AC_ACCOUNT_ID       = @pAccountId



	-- Update Buckets
	EXECUTE Insert_Update_Customer_Bucket @pAccountId, @pBucketId, @pPlayerTrackingMode, @IsDelta, @LocalDelta

	-- Update Buckets 8 OR 9
	IF @ValueForSpecialBucket <> 0 
		EXECUTE Insert_Update_Customer_Bucket @pAccountId, @ValueForSpecialBucket, @pPlayerTrackingMode, @IsDelta, @LocalDelta


    -- TODO BUCKET_HISTORY
  END

  SET @pErrorCode = 0

  -- Get sequence ID
  SELECT   @pPointsSequenceId = AC_MS_POINTS_SEQ_ID
    FROM   ACCOUNTS
   WHERE   AC_ACCOUNT_ID      = @pAccountId

  -- Get Bucket Value
  SELECT   @pPoints           = CBU_VALUE
    FROM   CUSTOMER_BUCKET
   WHERE   CBU_CUSTOMER_ID    = @pAccountId
     AND   CBU_BUCKET_ID      = @pBucketId
     
  SET @pPoints = ISNULL(@pPoints, 0)
END

GO

IF EXISTS( SELECT 1 FROM sys.objects WHERE name = 'GetAccountPointsCache')
  DROP PROCEDURE [dbo].[GetAccountPointsCache] 
GO

CREATE PROCEDURE [dbo].[GetAccountPointsCache] @pAccountId bigint
AS
BEGIN

  --EXEC AccountPointsCache_CalculateToday @pAccountId

  DECLARE @BucketPuntosCanje Int
  DECLARE @BucketNR Int 
  DECLARE @BucketRE Int
  DECLARE @BucketPuntosCanjeGenerados Int
  DECLARE @BucketPuntosCanjeDiscrecionales Int

    
  SET @BucketPuntosCanje = 1
  SET @BucketPuntosCanjeGenerados = 8
  SET @BucketPuntosCanjeDiscrecionales = 9
  
  --Realizar select ACCOUNT_POINTS_CACHE con inner join a ACCOUNTS. -- select similar al que se hace en WKT_Player.cs funci�n Read(..)
  SELECT   ISNULL(AC_HOLDER_NAME, '')             AS AC_HOLDER_NAME 
         , ISNULL(AC_HOLDER_GENDER, 0)            AS AC_HOLDER_GENDER 
         , AC_HOLDER_BIRTH_DATE 
         , AC_BALANCE 
         , AC_HOLDER_LEVEL                        AS AC_CURRENT_HOLDER_LEVEL
         , AC_HOLDER_LEVEL_EXPIRATION 
         , AC_HOLDER_LEVEL_ENTERED 
         , AC_RE_BALANCE 
         , AC_PROMO_RE_BALANCE 
         , AC_PROMO_NR_BALANCE 
         , ISNULL (AC_CURRENT_PLAY_SESSION_ID, 0) AS AC_CURRENT_PLAY_SESSION_ID 
         , AC_CURRENT_TERMINAL_NAME 
		     , ISNULL(AC_POINTS.CBU_VALUE                      ,0) AS  AC_POINTS                 
		     , ISNULL(AM_POINTS_GENERATED.CBU_VALUE      	  ,0) AS  AM_POINTS_GENERATED       
		     , ISNULL(AM_POINTS_DISCRETIONARIES.CBU_VALUE	  ,0) AS  AM_POINTS_DISCRETIONARIES 
    FROM   ACCOUNTS 
      LEFT JOIN CUSTOMER_BUCKET AC_POINTS ON AC_POINTS.CBU_CUSTOMER_ID = AC_ACCOUNT_ID AND AC_POINTS.CBU_BUCKET_ID = 1 /* REDEMPTIONPOINTS */
      LEFT JOIN CUSTOMER_BUCKET AM_POINTS_GENERATED ON AM_POINTS_GENERATED.CBU_CUSTOMER_ID = AC_ACCOUNT_ID AND AM_POINTS_GENERATED.CBU_BUCKET_ID = 8 /* RANKINGLEVELPOINTS_GENERATED */
      LEFT JOIN CUSTOMER_BUCKET AM_POINTS_DISCRETIONARIES ON AM_POINTS_DISCRETIONARIES.CBU_CUSTOMER_ID = AC_ACCOUNT_ID AND AM_POINTS_DISCRETIONARIES.CBU_BUCKET_ID = 9 /* RANKINGLEVELPOINTS_DISCRETIONAL */
   WHERE   AC_ACCOUNT_ID = @pAccountId; 
END

GO

GRANT EXECUTE ON [dbo].[GetAccountPointsCache] TO [wggui] WITH GRANT OPTION
GO