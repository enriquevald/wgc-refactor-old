USE [master]
GO

/**** CHECK DATABASE EXISTENCE SECTION *****/
IF (EXISTS (SELECT * FROM sys.databases WHERE name = 'wgdb_100'))
BEGIN
/**** FAILURE SECTION *****/
SELECT 'Database wgdb_100 already exists.'

raiserror('Not updated', 20, -1) with log
END

/****** Object:  Database [wgdb_100]    Script Date: 05/01/2007 17:12:26 ******/
CREATE DATABASE [wgdb_100] ON  PRIMARY 
( NAME = N'wgdb_100', FILENAME = N'C:\WigosMultiSite\Database\wgdb_100.mdf' , SIZE = 5173248KB , MAXSIZE = UNLIMITED, FILEGROWTH = 10240KB )
 LOG ON 
( NAME = N'wgdb_100_log', FILENAME = N'C:\WigosMultiSite\Database\wgdb_100_1.ldf' , SIZE = 17030144KB , MAXSIZE = UNLIMITED , FILEGROWTH = 10240KB )
 COLLATE SQL_Latin1_General_CP1_CI_AS
GO
EXEC dbo.sp_dbcmptlevel @dbname=N'wgdb_100', @new_cmptlevel=90
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [wgdb_100].[dbo].[sp_fulltext_database] @action = 'disable'
end
GO
ALTER DATABASE [wgdb_100] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [wgdb_100] SET ANSI_NULLS OFF
GO
ALTER DATABASE [wgdb_100] SET ANSI_PADDING ON
GO
ALTER DATABASE [wgdb_100] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [wgdb_100] SET ARITHABORT OFF
GO
ALTER DATABASE [wgdb_100] SET AUTO_CLOSE OFF
GO
ALTER DATABASE [wgdb_100] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [wgdb_100] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [wgdb_100] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [wgdb_100] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [wgdb_100] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [wgdb_100] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [wgdb_100] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [wgdb_100] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [wgdb_100] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [wgdb_100] SET ENABLE_BROKER
GO
ALTER DATABASE [wgdb_100] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [wgdb_100] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [wgdb_100] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [wgdb_100] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [wgdb_100] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [wgdb_100] SET READ_WRITE
GO
ALTER DATABASE [wgdb_100] SET RECOVERY FULL
GO
ALTER DATABASE [wgdb_100] SET MULTI_USER
GO
ALTER DATABASE [wgdb_100] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [wgdb_100] SET DB_CHAINING OFF
GO
ALTER DATABASE [wgdb_100] SET READ_COMMITTED_SNAPSHOT ON
GO

/* User Logins */
USE [master]
CREATE LOGIN [wgroot_100] WITH PASSWORD=N'Wigos_ro_100', DEFAULT_DATABASE=[wgdb_100], DEFAULT_LANGUAGE=[us_english], CHECK_EXPIRATION=OFF, CHECK_POLICY=ON--, SID=0xA974C8AB0E92BF4798999D3ABFD394C2
GO
CREATE LOGIN [wgpublic_100] WITH PASSWORD=N'Wigos_pu_100', DEFAULT_DATABASE=[wgdb_100], DEFAULT_LANGUAGE=[us_english], CHECK_EXPIRATION=OFF, CHECK_POLICY=ON--, SID=0x3106118F0A22F146994661393ED200D1
GO
CREATE LOGIN [wggui_100] WITH PASSWORD=N'Wigos_gi_100', DEFAULT_DATABASE=[wgdb_100], DEFAULT_LANGUAGE=[us_english], CHECK_EXPIRATION=OFF, CHECK_POLICY=ON--, SID=0x15C93A479F370A409E3D4724386170B1
GO

USE [wgdb_100]
GO

/****** Object:  User [cms_admin]    Script Date: 03/12/2013 17:48:29 ******/
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'cms_admin')
CREATE USER [cms_admin] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [wggui]    Script Date: 03/12/2013 17:48:29 ******/
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'wggui')
CREATE USER [wggui] FOR LOGIN [wggui_100] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [wgpublic]    Script Date: 03/12/2013 17:48:29 ******/
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'wgpublic')
CREATE USER [wgpublic] FOR LOGIN [wgpublic_100] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [wgroot]    Script Date: 03/12/2013 17:48:29 ******/
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'wgroot')
CREATE USER [wgroot] FOR LOGIN [wgroot_100] WITH DEFAULT_SCHEMA=[dbo]
GO


/****** Object:  Table [dbo].[terminal_software_versions]    Script Date: 03/12/2013 17:48:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[terminal_software_versions]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[terminal_software_versions](
	[tsv_client_id] [smallint] NOT NULL,
	[tsv_build_id] [smallint] NOT NULL,
	[tsv_terminal_type] [smallint] NOT NULL,
	[tsv_insertion_date] [datetime] NOT NULL,
 CONSTRAINT [PK_kiosk_software_versions] PRIMARY KEY CLUSTERED 
(
	[tsv_client_id] ASC,
	[tsv_build_id] ASC,
	[tsv_terminal_type] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[account_movements]    Script Date: 03/12/2013 17:48:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[account_movements]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[account_movements](
	[am_site_id] [int] NOT NULL,
	[am_movement_id] [bigint] NOT NULL,
	[am_play_session_id] [bigint] NULL,
	[am_account_id] [bigint] NOT NULL,
	[am_terminal_id] [int] NULL,
	[am_wcp_sequence_id] [bigint] NULL,
	[am_wcp_transaction_id] [bigint] NULL,
	[am_datetime] [datetime] NOT NULL,
	[am_type] [int] NOT NULL,
	[am_initial_balance] [money] NOT NULL,
	[am_sub_amount] [money] NOT NULL,
	[am_add_amount] [money] NOT NULL,
	[am_final_balance] [money] NOT NULL,
	[am_cashier_id] [int] NULL,
	[am_cashier_name] [nvarchar](50) NULL,
	[am_terminal_name] [nvarchar](50) NULL,
	[am_operation_id] [bigint] NULL,
	[am_details] [nvarchar](256) NULL,
	[am_reasons] [nvarchar](64) NULL
 )
END
GO

GO
/****** Object:  UserDefinedFunction [dbo].[Minimum]    Script Date: 03/12/2013 17:48:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Minimum]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
/* User-defined Functions */
/****** Object:  UserDefinedFunction [dbo].[Minimum]    Script Date: 12/09/2008 16:30:21 ******/
-- =============================================
-- Author:    Armando Alva
-- Create date: 07-08-2008
-- Description: Returns the minimum value of the two given arguments 
-- =============================================
CREATE FUNCTION [dbo].[Minimum]
       (
            @value1 Money = 0,
            @value2 Money = 0
       )
RETURNS Money

AS
      BEGIN
            DECLARE @Result Money
            SET @value1 = ISNULL(@value1,0)
            SET @value2 = ISNULL(@value2,0)
            IF @value1 <= @value2
                  SET @Result = @value1
            ELSE
                  SET @Result = @value2
      RETURN @Result
      END
' 
END
GO
/****** Object:  UserDefinedFunction [dbo].[Maximum_Money]    Script Date: 03/12/2013 17:48:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Maximum_Money]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'/****** Object:  UserDefinedFunction [dbo].[Maximum_Money]    Script Date: 12/09/2008 16:30:21 ******/
-- =============================================
-- Author:    Agustí Poch
-- Create date: 14-11-2008
-- Description: Returns the maximum value of the two given 
--              arguments of type Money
-- =============================================
CREATE FUNCTION [dbo].[Maximum_Money]
       (
            @value1 Money = 0,
            @value2 Money = 0
       )
RETURNS Money

AS
      BEGIN
            DECLARE @Result Money
            SET @value1 = ISNULL(@value1,0)
            SET @value2 = ISNULL(@value2,0)
            IF @value1 >= @value2
                  SET @Result = @value1
            ELSE
                  SET @Result = @value2
      RETURN @Result
      END
' 
END
GO
/****** Object:  UserDefinedFunction [dbo].[Maximum_Bigint]    Script Date: 03/12/2013 17:48:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Maximum_Bigint]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'/****** Object:  UserDefinedFunction [dbo].[Maximum_Bigint]    Script Date: 12/09/2008 16:30:21 ******/
-- =============================================
-- Author:    Agustí Poch
-- Create date: 14-11-2008
-- Description: Returns the maximum value of the two given 
--              arguments of type Bigint
-- =============================================
CREATE FUNCTION [dbo].[Maximum_Bigint]
       (
            @value1 Bigint = 0,
            @value2 Bigint = 0
       )
RETURNS Bigint

AS
      BEGIN
            DECLARE @Result Bigint
            SET @value1 = ISNULL(@value1,0)
            SET @value2 = ISNULL(@value2,0)
            IF @value1 >= @value2
                  SET @Result = @value1
            ELSE
                  SET @Result = @value2
      RETURN @Result
      END
' 
END
GO
/****** Object:  UserDefinedFunction [dbo].[Maximum_Datetime]    Script Date: 03/12/2013 17:48:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Maximum_Datetime]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'/****** Object:  UserDefinedFunction [dbo].[Maximum_Datetime]    Script Date: 12/09/2008 16:30:21 ******/
-- =============================================
-- Author:    Agustí Poch
-- Create date: 14-11-2008
-- Description: Returns the maximum value of the two given 
--              arguments of type Datetime
-- =============================================
CREATE FUNCTION [dbo].[Maximum_Datetime]
       (
            @value1 Datetime = 0,
            @value2 Datetime = 0
       )
RETURNS Datetime

AS
      BEGIN
            DECLARE @Result Datetime
            SET @value1 = ISNULL(@value1,0)
            SET @value2 = ISNULL(@value2,0)
            IF @value1 >= @value2
                  SET @Result = @value1
            ELSE
                  SET @Result = @value2
      RETURN @Result
      END
' 
END
GO
/****** Object:  UserDefinedFunction [dbo].[Minimum_Money]    Script Date: 03/12/2013 17:48:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Minimum_Money]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'/****** Object:  UserDefinedFunction [dbo].[Minimum_Money]    Script Date: 12/09/2008 16:30:21 ******/
-- =============================================
-- Author:    Agustí Poch
-- Create date: 14-11-2008
-- Description: Returns the minimum value of the two given 
--              arguments of type Money
-- =============================================
CREATE FUNCTION [dbo].[Minimum_Money]
       (
            @value1 Money = 0,
            @value2 Money = 0
       )
RETURNS Money

AS
      BEGIN
            DECLARE @Result Money
            SET @value1 = ISNULL(@value1,0)
            SET @value2 = ISNULL(@value2,0)
            IF @value1 <= @value2
                  SET @Result = @value1
            ELSE
                  SET @Result = @value2
      RETURN @Result
      END
' 
END
GO
/****** Object:  UserDefinedFunction [dbo].[Minimum_Bigint]    Script Date: 03/12/2013 17:48:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Minimum_Bigint]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'/****** Object:  UserDefinedFunction [dbo].[Minimum_Bigint]    Script Date: 12/09/2008 16:30:21 ******/
-- =============================================
-- Author:    Agustí Poch
-- Create date: 14-11-2008
-- Description: Returns the minimum value of the two given 
--              arguments of type Bigint
-- =============================================
CREATE FUNCTION [dbo].[Minimum_Bigint]
       (
            @value1 Bigint = 0,
            @value2 Bigint = 0
       )
RETURNS Bigint

AS
      BEGIN
            DECLARE @Result Bigint
            SET @value1 = ISNULL(@value1,0)
            SET @value2 = ISNULL(@value2,0)
            IF @value1 <= @value2
                  SET @Result = @value1
            ELSE
                  SET @Result = @value2
      RETURN @Result
      END
' 
END
GO
/****** Object:  UserDefinedFunction [dbo].[Minimum_Datetime]    Script Date: 03/12/2013 17:48:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Minimum_Datetime]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'/****** Object:  UserDefinedFunction [dbo].[Minimum_Datetime]    Script Date: 12/09/2008 16:30:21 ******/
-- =============================================
-- Author:    Agustí Poch
-- Create date: 14-11-2008
-- Description: Returns the minimum value of the two given 
--              arguments of type Datetime
-- =============================================
CREATE FUNCTION [dbo].[Minimum_Datetime]
       (
            @value1 Datetime = 0,
            @value2 Datetime = 0
       )
RETURNS Datetime

AS
      BEGIN
            DECLARE @Result Datetime
            SET @value1 = ISNULL(@value1,0)
            SET @value2 = ISNULL(@value2,0)
            IF @value1 <= @value2
                  SET @Result = @value1
            ELSE
                  SET @Result = @value2
      RETURN @Result
      END
' 
END
GO
/****** Object:  Table [dbo].[applications]    Script Date: 03/12/2013 17:48:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[applications]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[applications](
	[app_name] [nvarchar](50) NOT NULL,
	[app_version] [nvarchar](50) NOT NULL,
	[app_machine] [nvarchar](50) NOT NULL,
	[app_ip_address] [nvarchar](50) NOT NULL,
	[app_os_username] [nvarchar](50) NOT NULL,
	[app_login_name] [nvarchar](50) NOT NULL,
	[app_last_access] [datetime] NOT NULL,
 CONSTRAINT [PK_applications] PRIMARY KEY CLUSTERED 
(
	[app_name] ASC,
	[app_machine] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[ms_site_tasks]    Script Date: 03/12/2013 17:48:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ms_site_tasks]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ms_site_tasks](
	[st_site_id] [int] NOT NULL,
	[st_task_id] [int] NOT NULL,
	[st_enabled] [bit] NOT NULL,
	[st_running] [bit] NOT NULL,
	[st_started] [datetime] NULL,
	[st_local_sequence_id] [bigint] NULL,
	[st_remote_sequence_id] [bigint] NULL,
	[st_interval_seconds] [int] NULL,
	[st_last_run] [datetime] NULL,
	[st_num_pending] [int] NULL,
 CONSTRAINT [PK_ms_site_synch_control] PRIMARY KEY CLUSTERED 
(
	[st_site_id] ASC,
	[st_task_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[sequences]    Script Date: 03/12/2013 17:48:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sequences]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[sequences](
	[seq_id] [int] NOT NULL,
	[seq_next_value] [bigint] NOT NULL,
 CONSTRAINT [PK_sequences] PRIMARY KEY CLUSTERED 
(
	[seq_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[accounts]    Script Date: 03/12/2013 17:48:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[accounts]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[accounts](
	[ac_account_id] [bigint] NOT NULL,
	[ac_type] [int] NOT NULL,
	[ac_holder_name] [nvarchar](200) NULL,
	[ac_blocked] [bit] NOT NULL,
	[ac_not_valid_before] [datetime] NULL,
	[ac_not_valid_after] [datetime] NULL,
	[ac_balance] [money] NOT NULL,
	[ac_cash_in] [money] NOT NULL,
	[ac_cash_won] [money] NOT NULL,
	[ac_not_redeemable] [money] NOT NULL,
	[ac_timestamp] [timestamp] NULL,
	[ac_track_data] [nvarchar](50) NULL,
	[ac_total_cash_in] [money] NOT NULL,
	[ac_total_cash_out] [money] NOT NULL,
	[ac_initial_cash_in] [money] NOT NULL,
	[ac_activated] [bit] NOT NULL,
	[ac_deposit] [money] NOT NULL,
	[ac_current_terminal_id] [int] NULL,
	[ac_current_terminal_name] [nvarchar](50) NULL,
	[ac_current_play_session_id] [bigint] NULL,
	[ac_last_terminal_id] [int] NULL,
	[ac_last_terminal_name] [nvarchar](50) NULL,
	[ac_last_play_session_id] [bigint] NULL,
	[ac_user_type] [int] NULL,
	[ac_points] [money] NULL,
	[ac_initial_not_redeemable] [money] NOT NULL,
	[ac_created] [datetime] NOT NULL,
	[ac_promo_balance] [money] NOT NULL,
	[ac_promo_limit] [money] NOT NULL,
	[ac_promo_creation] [datetime] NOT NULL,
	[ac_promo_expiration] [datetime] NOT NULL,
	[ac_last_activity] [datetime] NOT NULL,
	[ac_holder_id] [nvarchar](20) NULL,
	[ac_holder_address_01] [nvarchar](50) NULL,
	[ac_holder_address_02] [nvarchar](50) NULL,
	[ac_holder_address_03] [nvarchar](50) NULL,
	[ac_holder_city] [nvarchar](50) NULL,
	[ac_holder_zip] [nvarchar](10) NULL,
	[ac_holder_email_01] [nvarchar](50) NULL,
	[ac_holder_email_02] [nvarchar](50) NULL,
	[ac_holder_phone_number_01] [nvarchar](20) NULL,
	[ac_holder_phone_number_02] [nvarchar](20) NULL,
	[ac_holder_comments] [nvarchar](100) NULL,
	[ac_holder_gender] [int] NULL,
	[ac_holder_marital_status] [int] NULL,
	[ac_holder_birth_date] [datetime] NULL,
	[ac_draw_last_play_session_id] [bigint] NOT NULL,
	[ac_draw_last_play_session_remainder] [money] NOT NULL,
	[ac_nr_won_lock] [money] NULL,
	[ac_nr_expiration] [datetime] NULL,
	[ac_cashin_while_playing] [money] NULL,
	[ac_holder_level] [int] NOT NULL,
	[ac_card_paid] [bit] NOT NULL,
	[ac_cancellable_operation_id] [bigint] NULL,
	[ac_current_promotion_id] [bigint] NULL,
	[ac_block_reason] [int] NOT NULL,
	[ac_holder_level_expiration] [datetime] NULL,
	[ac_holder_level_entered] [datetime] NULL,
	[ac_holder_level_notify] [int] NULL,
	[ac_pin] [nvarchar](12) NULL,
	[ac_pin_failures] [int] NULL,
	[ac_holder_id1] [nvarchar](20) NULL,
	[ac_holder_id2] [nvarchar](20) NULL,
	[ac_holder_document_id1] [bigint] NULL,
	[ac_holder_document_id2] [bigint] NULL,
	[ac_holder_name1] [nvarchar](50) NULL,
	[ac_holder_name2] [nvarchar](50) NULL,
	[ac_holder_name3] [nvarchar](50) NULL,
	[ac_nr2_expiration] [datetime] NULL,
	[ac_recommended_by] [bigint] NULL,
	[ac_re_balance] [money] NOT NULL,
	[ac_promo_re_balance] [money] NOT NULL,
	[ac_promo_nr_balance] [money] NOT NULL,
	[ac_in_session_played] [money] NOT NULL,
	[ac_in_session_won] [money] NOT NULL,
	[ac_in_session_re_balance] [money] NOT NULL,
	[ac_in_session_promo_re_balance] [money] NOT NULL,
	[ac_in_session_promo_nr_balance] [money] NOT NULL,
	[ac_in_session_re_to_gm] [money] NOT NULL,
	[ac_in_session_promo_re_to_gm] [money] NOT NULL,
	[ac_in_session_promo_nr_to_gm] [money] NOT NULL,
	[ac_in_session_re_from_gm] [money] NOT NULL,
	[ac_in_session_promo_re_from_gm] [money] NOT NULL,
	[ac_in_session_promo_nr_from_gm] [money] NOT NULL,
	[ac_in_session_re_played] [money] NOT NULL,
	[ac_in_session_nr_played] [money] NOT NULL,
	[ac_in_session_re_won] [money] NOT NULL,
	[ac_in_session_nr_won] [money] NOT NULL,
	[ac_in_session_re_cancellable] [money] NOT NULL,
	[ac_in_session_promo_re_cancellable] [money] NOT NULL,
	[ac_in_session_promo_nr_cancellable] [money] NOT NULL,
	[ac_in_session_cancellable_transaction_id] [bigint] NOT NULL,
	[ac_holder_id_type] [int] NOT NULL,
	[ac_holder_id_indexed]  AS (isnull((CONVERT([nvarchar],[ac_holder_id_type],(0))+' - ')+[ac_holder_id],'<NULL>'+CONVERT([nvarchar],[ac_account_id],(0)))),
	[ac_promo_ini_re_balance] [money] NULL,
	[ac_holder_is_vip] [bit] NULL,
	[ac_holder_title] [nvarchar](15) NULL,
	[ac_holder_name4] [nvarchar](50) NULL,
	[ac_holder_wedding_date] [datetime] NULL,
	[ac_holder_phone_type_01] [int] NULL,
	[ac_holder_phone_type_02] [int] NULL,
	[ac_holder_state] [nvarchar](50) NULL,
	[ac_holder_country] [nvarchar](50) NULL,
	[ac_holder_address_01_alt] [nvarchar](50) NULL,
	[ac_holder_address_02_alt] [nvarchar](50) NULL,
	[ac_holder_address_03_alt] [nvarchar](50) NULL,
	[ac_holder_city_alt] [nvarchar](50) NULL,
	[ac_holder_zip_alt] [nvarchar](10) NULL,
	[ac_holder_state_alt] [nvarchar](50) NULL,
	[ac_holder_country_alt] [nvarchar](50) NULL,
	[ac_holder_is_smoker] [bit] NULL,
	[ac_holder_nickname] [nvarchar](25) NULL,
	[ac_holder_credit_limit] [money] NULL,
	[ac_holder_request_credit_limit] [money] NULL,
	[ac_pin_last_modified] [datetime] NULL,
	[ac_last_activity_site_id] [nvarchar](25) NULL,
	[ac_creation_site_id] [nvarchar](25) NULL,
	[ac_last_update_site_id] [nvarchar](25) NULL,
	[ac_external_reference] [nvarchar](50) NULL,
	[ac_points_status] [int] NULL,
	[ac_ms_has_local_changes] [bit] NULL,
	[ac_ms_change_guid] [uniqueidentifier] NULL,
	[ac_ms_created_on_site_id] [int] NULL,
	[ac_ms_modified_on_site_id] [int] NULL,
	[ac_ms_last_site_id] [int] NULL,
	[ac_ms_points_seq_id] [bigint] NULL,
	[ac_ms_points_synchronized] [datetime] NULL,
	[ac_ms_personal_info_seq_id] [bigint] NULL,
	[ac_ms_hash] [varbinary](20) NULL,
 CONSTRAINT [PK_accounts] PRIMARY KEY CLUSTERED 
(
	[ac_account_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Index [IX_ms_personal_info_seq_id]    Script Date: 03/18/2013 11:51:20 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[accounts]') AND name = N'IX_ms_personal_info_seq_id')
CREATE NONCLUSTERED INDEX [IX_ms_personal_info_seq_id] ON [dbo].[accounts] 
(
	[ac_ms_personal_info_seq_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_ms_points_seq_id]    Script Date: 03/18/2013 11:51:28 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[accounts]') AND name = N'IX_ms_points_seq_id')
CREATE NONCLUSTERED INDEX [IX_ms_points_seq_id] ON [dbo].[accounts] 
(
	[ac_ms_points_seq_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[db_users]    Script Date: 03/12/2013 17:48:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[db_users]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[db_users](
	[du_username] [nvarchar](50) NOT NULL,
	[du_password] [binary](40) NOT NULL,
 CONSTRAINT [PK_db_users] PRIMARY KEY CLUSTERED 
(
	[du_username] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[db_version]    Script Date: 03/12/2013 17:48:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[db_version]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[db_version](
	[db_client_id] [int] NOT NULL,
	[db_common_build_id] [int] NOT NULL,
	[db_client_build_id] [int] NOT NULL,
	[db_release_id] [int] NOT NULL,
	[db_updated_script] [nvarchar](50) NULL,
	[db_updated] [datetime] NULL,
	[db_description] [nvarchar](1000) NULL,
 CONSTRAINT [PK_db_version] PRIMARY KEY CLUSTERED 
(
	[db_client_id] ASC,
	[db_common_build_id] ASC,
	[db_client_build_id] ASC,
	[db_release_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[general_params]    Script Date: 03/12/2013 17:48:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[general_params]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[general_params](
	[gp_group_key] [nvarchar](50) NOT NULL,
	[gp_subject_key] [nvarchar](50) NOT NULL,
	[gp_key_value] [nvarchar](500) NULL,
	[gp_timestamp] [binary](8) NULL,
 CONSTRAINT [PK_general_params_1] PRIMARY KEY CLUSTERED 
(
	[gp_group_key] ASC,
	[gp_subject_key] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[gui_forms]    Script Date: 03/12/2013 17:48:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[gui_forms]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[gui_forms](
	[gf_gui_id] [int] NOT NULL,
	[gf_form_id] [int] NOT NULL,
	[gf_form_order] [int] NOT NULL,
	[gf_nls_id] [int] NOT NULL,
 CONSTRAINT [PK_gui_forms] PRIMARY KEY CLUSTERED 
(
	[gf_gui_id] ASC,
	[gf_form_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[gui_user_profiles]    Script Date: 03/12/2013 17:48:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[gui_user_profiles]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[gui_user_profiles](
	[gup_profile_id] [int] NOT NULL,
	[gup_name] [nvarchar](40) NOT NULL,
	[gup_timestamp] [timestamp] NULL,
 CONSTRAINT [PK_gui_user_profiles] PRIMARY KEY CLUSTERED 
(
	[gup_profile_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
ALTER TABLE dbo.gui_user_profiles
        ADD gup_max_users int NOT NULL CONSTRAINT DF_gui_user_profiles_gup_max_users DEFAULT 0
GO
/****** Object:  Table [dbo].[licences]    Script Date: 03/12/2013 17:48:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[licences]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[licences](
	[wl_id] [int] IDENTITY(1,1) NOT NULL,
	[wl_licence] [xml] NOT NULL,
	[wl_insertion_date] [datetime] NOT NULL,
	[wl_expiration_date] [datetime] NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[site_jobs]    Script Date: 03/12/2013 17:48:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[site_jobs]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[site_jobs](
	[sjb_site_id] [int] NOT NULL,
	[sjb_type] [int] NOT NULL,
	[sjb_name] [nvarchar](50) NOT NULL,
	[sjb_enabled] [bit] NOT NULL,
	[sjb_period] [int] NOT NULL,
	[sjb_next_try] [datetime] NOT NULL,
	[sjb_order] [int] NOT NULL,
	[sjb_last_try] [datetime] NULL,
	[sjb_num_tries] [int] NOT NULL,
	[sjb_last_try_ok] [datetime] NULL,
	[sjb_sync_date] [datetime] NULL,
	[sjb_sync_timestamp] [varbinary](8) NULL,
	[sjb_num_executions] [int] NOT NULL,
	[sjb_total_execution_time] [bigint] NOT NULL,
	[sjb_num_messages] [int] NOT NULL,
	[sjb_total_message_time] [bigint] NOT NULL,
	[sjb_total_roundtrip_time] [bigint] NOT NULL,
 CONSTRAINT [PK_site_jobs] PRIMARY KEY CLUSTERED 
(
	[sjb_site_id] ASC,
	[sjb_type] ASC,
	[sjb_name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'site_jobs', N'COLUMN',N'sjb_type'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0 - Table, 1 - License, 2 - Other' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'site_jobs', @level2type=N'COLUMN',@level2name=N'sjb_type'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'site_jobs', N'COLUMN',N'sjb_period'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Period in seconds' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'site_jobs', @level2type=N'COLUMN',@level2name=N'sjb_period'
GO
/****** Object:  Table [dbo].[services]    Script Date: 03/12/2013 17:48:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[services]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[services](
	[svc_protocol] [nvarchar](50) NOT NULL,
	[svc_machine] [nvarchar](50) NOT NULL,
	[svc_ip_address] [nvarchar](50) NOT NULL,
	[svc_last_access] [datetime] NOT NULL,
	[svc_status] [nvarchar](50) NOT NULL,
	[svc_version] [nvarchar](50) NULL,
 CONSTRAINT [PK_services] PRIMARY KEY CLUSTERED 
(
	[svc_protocol] ASC,
	[svc_machine] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[alarms]    Script Date: 03/12/2013 17:48:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[alarms]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[alarms](
	[al_alarm_id] [bigint] IDENTITY(1,1) NOT NULL,
	[al_source_code] [int] NOT NULL,
	[al_source_id] [bigint] NOT NULL,
	[al_source_name] [nvarchar](100) NOT NULL,
	[al_alarm_code] [int] NOT NULL,
	[al_alarm_name] [nvarchar](50) NOT NULL,
	[al_alarm_description] [nvarchar](max) NULL,
	[al_severity] [int] NOT NULL,
	[al_reported] [datetime] NOT NULL,
	[al_datetime] [datetime] NOT NULL,
	[al_ack_datetime] [datetime] NULL,
	[al_ack_user_id] [int] NULL,
	[al_ack_user_name] [nvarchar](50) NULL,
	[al_timestamp] [timestamp] NOT NULL 
)
END
GO
/****** Object:  Table [dbo].[site_operators]    Script Date: 03/12/2013 17:48:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[site_operators]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[site_operators](
	[sop_operator_id] [int] IDENTITY(1,1) NOT NULL,
	[sop_name] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_site_operators] PRIMARY KEY CLUSTERED 
(
	[sop_operator_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[gui_profile_forms]    Script Date: 03/12/2013 17:48:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[gui_profile_forms]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[gui_profile_forms](
	[gpf_profile_id] [int] NOT NULL,
	[gpf_gui_id] [int] NOT NULL,
	[gpf_form_id] [int] NOT NULL,
	[gpf_read_perm] [bit] NOT NULL,
	[gpf_write_perm] [bit] NOT NULL,
	[gpf_delete_perm] [bit] NOT NULL,
	[gpf_execute_perm] [bit] NOT NULL,
 CONSTRAINT [PK_gui_profile_forms] PRIMARY KEY CLUSTERED 
(
	[gpf_profile_id] ASC,
	[gpf_gui_id] ASC,
	[gpf_form_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[gui_users]    Script Date: 03/12/2013 17:48:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[gui_users]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[gui_users](
	[gu_user_id] [int] NOT NULL,
	[gu_profile_id] [int] NOT NULL,
	[gu_username] [nvarchar](50) NOT NULL,
	[gu_enabled] [bit] NOT NULL,
	[gu_password] [binary](40) NOT NULL,
	[gu_not_valid_before] [datetime] NOT NULL,
	[gu_not_valid_after] [datetime] NULL,
	[gu_last_changed] [datetime] NULL,
	[gu_password_exp] [datetime] NULL,
	[gu_pwd_chg_req] [bit] NOT NULL,
	[gu_login_failures] [int] NULL,
	[gu_password_h1] [binary](40) NULL,
	[gu_password_h2] [binary](40) NULL,
	[gu_password_h3] [binary](40) NULL,
	[gu_password_h4] [binary](40) NULL,
	[gu_password_h5] [binary](40) NULL,
	[gu_full_name] [nchar](20) NULL,
	[gu_timestamp] [timestamp] NULL,
	[gu_user_type] [smallint] NOT NULL,
	[gu_logged_in] [datetime] NULL,
	[gu_logon_computer] [nchar](50) NULL,
	[gu_last_activity] [datetime] NULL,
	[gu_last_action] [nchar](50) NULL,
	[gu_exit_code] [smallint] NULL,
	[gu_sales_limit] [money] NULL,
	[gu_mb_sales_limit] [money] NULL,
	[gu_block_reason] [int] NOT NULL,
 CONSTRAINT [PK_gui_users] PRIMARY KEY CLUSTERED 
(
	[gu_user_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UNQ_gui_users] UNIQUE NONCLUSTERED 
(
	[gu_username] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[gui_audit]    Script Date: 03/12/2013 17:48:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[gui_audit]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[gui_audit](
	[ga_audit_id] [bigint] NOT NULL,
	[ga_gui_id] [int] NOT NULL,
	[ga_gui_user_id] [int] NULL,
	[ga_gui_username] [nvarchar](50) NOT NULL,
	[ga_computer_name] [nvarchar](50) NOT NULL,
	[ga_datetime] [datetime] NOT NULL,
	[ga_audit_code] [int] NOT NULL,
	[ga_audit_level] [int] NOT NULL,
	[ga_item_order] [int] NOT NULL,
	[ga_nls_id] [int] NULL,
	[ga_nls_param01] [nvarchar](150) NULL,
	[ga_nls_param02] [nvarchar](150) NULL,
	[ga_nls_param03] [nvarchar](150) NULL,
	[ga_nls_param04] [nvarchar](50) NULL,
	[ga_nls_param05] [nvarchar](50) NULL
) 
END
GO
/****** Object:  Table [dbo].[sites]    Script Date: 03/12/2013 17:48:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sites]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[sites](
	[st_site_id] [int] NOT NULL,
	[st_name] [nvarchar](50) NOT NULL,
	[st_operator_id] [int] NOT NULL,
	[st_state] [int] NOT NULL,
	[st_ip_address] [nvarchar](50) NULL,
	[st_rdp_port] [int] NULL,
	[st_last_known_ip] [nvarchar](50) NULL,
 CONSTRAINT [PK_sites] PRIMARY KEY CLUSTERED 
(
	[st_site_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'sites', N'COLUMN',N'st_state'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0 - Not Active, 1 - Active, 2 - Temporally Closed, 3 - Closed' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sites', @level2type=N'COLUMN',@level2name=N'st_state'
GO
/****** Object:  Table [dbo].[wwp_sessions]    Script Date: 03/12/2013 17:48:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wwp_sessions]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[wwp_sessions](
	[wws_session_id] [bigint] IDENTITY(1,1) NOT NULL,
	[wws_site_id] [int] NOT NULL,
	[wws_last_sequence_id] [bigint] NOT NULL,
	[wws_last_transaction_id] [bigint] NOT NULL,
	[wws_last_rcvd_msg] [datetime] NULL,
	[wws_started] [datetime] NOT NULL,
	[wws_finished] [datetime] NULL,
	[wws_status] [int] NOT NULL,
	[wws_timestamp] [timestamp] NULL,
	[wws_server_name] [nvarchar](50) NULL 
)
END
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'wwp_sessions', N'COLUMN',N'wws_status'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0 - Opened, 1 - Closed, 2 - Abandoned, 3 - Timeout' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'wwp_sessions', @level2type=N'COLUMN',@level2name=N'wws_status'
GO
/****** Object:  Default [DF_account_movements_am_site_id]    Script Date: 03/12/2013 17:48:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_account_movements_am_site_id]') AND parent_object_id = OBJECT_ID(N'[dbo].[account_movements]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_account_movements_am_site_id]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[account_movements] ADD  CONSTRAINT [DF_account_movements_am_site_id]  DEFAULT ((0)) FOR [am_site_id]
END


End
GO
/****** Object:  Default [DF_accounts_ac_balance]    Script Date: 03/12/2013 17:48:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_accounts_ac_balance]') AND parent_object_id = OBJECT_ID(N'[dbo].[accounts]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_accounts_ac_balance]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[accounts] ADD  CONSTRAINT [DF_accounts_ac_balance]  DEFAULT ((0)) FOR [ac_balance]
END


End
GO
/****** Object:  Default [DF_accounts_ac_cash_in]    Script Date: 03/12/2013 17:48:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_accounts_ac_cash_in]') AND parent_object_id = OBJECT_ID(N'[dbo].[accounts]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_accounts_ac_cash_in]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[accounts] ADD  CONSTRAINT [DF_accounts_ac_cash_in]  DEFAULT ((0)) FOR [ac_cash_in]
END


End
GO
/****** Object:  Default [DF_accounts_ac_cash_won]    Script Date: 03/12/2013 17:48:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_accounts_ac_cash_won]') AND parent_object_id = OBJECT_ID(N'[dbo].[accounts]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_accounts_ac_cash_won]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[accounts] ADD  CONSTRAINT [DF_accounts_ac_cash_won]  DEFAULT ((0)) FOR [ac_cash_won]
END


End
GO
/****** Object:  Default [DF_accounts_ac_not_redeemable]    Script Date: 03/12/2013 17:48:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_accounts_ac_not_redeemable]') AND parent_object_id = OBJECT_ID(N'[dbo].[accounts]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_accounts_ac_not_redeemable]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[accounts] ADD  CONSTRAINT [DF_accounts_ac_not_redeemable]  DEFAULT ((0)) FOR [ac_not_redeemable]
END


End
GO
/****** Object:  Default [DF_accounts_ac_total_cash_in]    Script Date: 03/12/2013 17:48:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_accounts_ac_total_cash_in]') AND parent_object_id = OBJECT_ID(N'[dbo].[accounts]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_accounts_ac_total_cash_in]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[accounts] ADD  CONSTRAINT [DF_accounts_ac_total_cash_in]  DEFAULT ((0)) FOR [ac_total_cash_in]
END


End
GO
/****** Object:  Default [DF_accounts_ac_total_cash_out]    Script Date: 03/12/2013 17:48:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_accounts_ac_total_cash_out]') AND parent_object_id = OBJECT_ID(N'[dbo].[accounts]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_accounts_ac_total_cash_out]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[accounts] ADD  CONSTRAINT [DF_accounts_ac_total_cash_out]  DEFAULT ((0)) FOR [ac_total_cash_out]
END


End
GO
/****** Object:  Default [DF_accounts_ac_initial_cash_in]    Script Date: 03/12/2013 17:48:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_accounts_ac_initial_cash_in]') AND parent_object_id = OBJECT_ID(N'[dbo].[accounts]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_accounts_ac_initial_cash_in]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[accounts] ADD  CONSTRAINT [DF_accounts_ac_initial_cash_in]  DEFAULT ((0)) FOR [ac_initial_cash_in]
END


End
GO
/****** Object:  Default [DF_accounts_ac_activated]    Script Date: 03/12/2013 17:48:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_accounts_ac_activated]') AND parent_object_id = OBJECT_ID(N'[dbo].[accounts]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_accounts_ac_activated]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[accounts] ADD  CONSTRAINT [DF_accounts_ac_activated]  DEFAULT ((0)) FOR [ac_activated]
END


End
GO
/****** Object:  Default [DF_accounts_ac_deposit]    Script Date: 03/12/2013 17:48:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_accounts_ac_deposit]') AND parent_object_id = OBJECT_ID(N'[dbo].[accounts]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_accounts_ac_deposit]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[accounts] ADD  CONSTRAINT [DF_accounts_ac_deposit]  DEFAULT ((0)) FOR [ac_deposit]
END


End
GO
/****** Object:  Default [DF_accounts_ac_user_type]    Script Date: 03/12/2013 17:48:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_accounts_ac_user_type]') AND parent_object_id = OBJECT_ID(N'[dbo].[accounts]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_accounts_ac_user_type]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[accounts] ADD  CONSTRAINT [DF_accounts_ac_user_type]  DEFAULT ((0)) FOR [ac_user_type]
END


End
GO
/****** Object:  Default [DF_accounts_ac_points]    Script Date: 03/12/2013 17:48:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_accounts_ac_points]') AND parent_object_id = OBJECT_ID(N'[dbo].[accounts]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_accounts_ac_points]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[accounts] ADD  CONSTRAINT [DF_accounts_ac_points]  DEFAULT ((0)) FOR [ac_points]
END


End
GO
/****** Object:  Default [DF_accounts_ac_initial_not_redeemable]    Script Date: 03/12/2013 17:48:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_accounts_ac_initial_not_redeemable]') AND parent_object_id = OBJECT_ID(N'[dbo].[accounts]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_accounts_ac_initial_not_redeemable]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[accounts] ADD  CONSTRAINT [DF_accounts_ac_initial_not_redeemable]  DEFAULT ((0)) FOR [ac_initial_not_redeemable]
END


End
GO
/****** Object:  Default [DF_accounts_ac_created]    Script Date: 03/12/2013 17:48:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_accounts_ac_created]') AND parent_object_id = OBJECT_ID(N'[dbo].[accounts]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_accounts_ac_created]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[accounts] ADD  CONSTRAINT [DF_accounts_ac_created]  DEFAULT (getdate()) FOR [ac_created]
END


End
GO
/****** Object:  Default [DF_accounts_ac_promo_balance]    Script Date: 03/12/2013 17:48:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_accounts_ac_promo_balance]') AND parent_object_id = OBJECT_ID(N'[dbo].[accounts]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_accounts_ac_promo_balance]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[accounts] ADD  CONSTRAINT [DF_accounts_ac_promo_balance]  DEFAULT ((0)) FOR [ac_promo_balance]
END


End
GO
/****** Object:  Default [DF_accounts_ac_promo_limit]    Script Date: 03/12/2013 17:48:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_accounts_ac_promo_limit]') AND parent_object_id = OBJECT_ID(N'[dbo].[accounts]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_accounts_ac_promo_limit]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[accounts] ADD  CONSTRAINT [DF_accounts_ac_promo_limit]  DEFAULT ((0)) FOR [ac_promo_limit]
END


End
GO
/****** Object:  Default [DF_accounts_ac_promo_creation]    Script Date: 03/12/2013 17:48:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_accounts_ac_promo_creation]') AND parent_object_id = OBJECT_ID(N'[dbo].[accounts]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_accounts_ac_promo_creation]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[accounts] ADD  CONSTRAINT [DF_accounts_ac_promo_creation]  DEFAULT (getdate()) FOR [ac_promo_creation]
END


End
GO
/****** Object:  Default [DF_accounts_ac_promo_expiration]    Script Date: 03/12/2013 17:48:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_accounts_ac_promo_expiration]') AND parent_object_id = OBJECT_ID(N'[dbo].[accounts]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_accounts_ac_promo_expiration]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[accounts] ADD  CONSTRAINT [DF_accounts_ac_promo_expiration]  DEFAULT (getdate()) FOR [ac_promo_expiration]
END


End
GO
/****** Object:  Default [DF_accounts_ac_last_activity]    Script Date: 03/12/2013 17:48:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_accounts_ac_last_activity]') AND parent_object_id = OBJECT_ID(N'[dbo].[accounts]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_accounts_ac_last_activity]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[accounts] ADD  CONSTRAINT [DF_accounts_ac_last_activity]  DEFAULT (getdate()) FOR [ac_last_activity]
END


End
GO
/****** Object:  Default [DF_accounts_ac_draw_last_session_id]    Script Date: 03/12/2013 17:48:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_accounts_ac_draw_last_session_id]') AND parent_object_id = OBJECT_ID(N'[dbo].[accounts]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_accounts_ac_draw_last_session_id]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[accounts] ADD  CONSTRAINT [DF_accounts_ac_draw_last_session_id]  DEFAULT ((0)) FOR [ac_draw_last_play_session_id]
END


End
GO
/****** Object:  Default [DF_accounts_ac_draw_last_remainder]    Script Date: 03/12/2013 17:48:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_accounts_ac_draw_last_remainder]') AND parent_object_id = OBJECT_ID(N'[dbo].[accounts]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_accounts_ac_draw_last_remainder]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[accounts] ADD  CONSTRAINT [DF_accounts_ac_draw_last_remainder]  DEFAULT ((0)) FOR [ac_draw_last_play_session_remainder]
END


End
GO
/****** Object:  Default [DF_accounts_ac_nr_won_lock]    Script Date: 03/12/2013 17:48:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_accounts_ac_nr_won_lock]') AND parent_object_id = OBJECT_ID(N'[dbo].[accounts]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_accounts_ac_nr_won_lock]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[accounts] ADD  CONSTRAINT [DF_accounts_ac_nr_won_lock]  DEFAULT ((0)) FOR [ac_nr_won_lock]
END


End
GO
/****** Object:  Default [DF_accounts_ac_holder_level]    Script Date: 03/12/2013 17:48:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_accounts_ac_holder_level]') AND parent_object_id = OBJECT_ID(N'[dbo].[accounts]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_accounts_ac_holder_level]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[accounts] ADD  CONSTRAINT [DF_accounts_ac_holder_level]  DEFAULT ((0)) FOR [ac_holder_level]
END


End
GO
/****** Object:  Default [DF_accounts_ac_card_paid]    Script Date: 03/12/2013 17:48:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_accounts_ac_card_paid]') AND parent_object_id = OBJECT_ID(N'[dbo].[accounts]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_accounts_ac_card_paid]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[accounts] ADD  CONSTRAINT [DF_accounts_ac_card_paid]  DEFAULT ((0)) FOR [ac_card_paid]
END


End
GO
/****** Object:  Default [DF_ac_block_reason]    Script Date: 03/12/2013 17:48:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_ac_block_reason]') AND parent_object_id = OBJECT_ID(N'[dbo].[accounts]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_ac_block_reason]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[accounts] ADD  CONSTRAINT [DF_ac_block_reason]  DEFAULT ((0)) FOR [ac_block_reason]
END


End
GO
/****** Object:  Default [DF_accounts_ac_holder_level_entered]    Script Date: 03/12/2013 17:48:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_accounts_ac_holder_level_entered]') AND parent_object_id = OBJECT_ID(N'[dbo].[accounts]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_accounts_ac_holder_level_entered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[accounts] ADD  CONSTRAINT [DF_accounts_ac_holder_level_entered]  DEFAULT (getdate()) FOR [ac_holder_level_entered]
END


End
GO
/****** Object:  Default [DF_accounts_ac_holder_level_notify]    Script Date: 03/12/2013 17:48:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_accounts_ac_holder_level_notify]') AND parent_object_id = OBJECT_ID(N'[dbo].[accounts]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_accounts_ac_holder_level_notify]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[accounts] ADD  CONSTRAINT [DF_accounts_ac_holder_level_notify]  DEFAULT ((0)) FOR [ac_holder_level_notify]
END


End
GO
/****** Object:  Default [DF__accounts__ac_re___0FAD2F12]    Script Date: 03/12/2013 17:48:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__accounts__ac_re___0FAD2F12]') AND parent_object_id = OBJECT_ID(N'[dbo].[accounts]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__accounts__ac_re___0FAD2F12]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[accounts] ADD  CONSTRAINT [DF__accounts__ac_re___0FAD2F12]  DEFAULT ((0)) FOR [ac_re_balance]
END


End
GO
/****** Object:  Default [DF__accounts__ac_nr___10A1534B]    Script Date: 03/12/2013 17:48:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__accounts__ac_nr___10A1534B]') AND parent_object_id = OBJECT_ID(N'[dbo].[accounts]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__accounts__ac_nr___10A1534B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[accounts] ADD  CONSTRAINT [DF__accounts__ac_nr___10A1534B]  DEFAULT ((0)) FOR [ac_promo_re_balance]
END


End
GO
/****** Object:  Default [DF_accounts_ac_promo_nr_balance]    Script Date: 03/12/2013 17:48:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_accounts_ac_promo_nr_balance]') AND parent_object_id = OBJECT_ID(N'[dbo].[accounts]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_accounts_ac_promo_nr_balance]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[accounts] ADD  CONSTRAINT [DF_accounts_ac_promo_nr_balance]  DEFAULT ((0)) FOR [ac_promo_nr_balance]
END


End
GO
/****** Object:  Default [DF__accounts__ac_in___11957784]    Script Date: 03/12/2013 17:48:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__accounts__ac_in___11957784]') AND parent_object_id = OBJECT_ID(N'[dbo].[accounts]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__accounts__ac_in___11957784]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[accounts] ADD  CONSTRAINT [DF__accounts__ac_in___11957784]  DEFAULT ((0)) FOR [ac_in_session_played]
END


End
GO
/****** Object:  Default [DF__accounts__ac_in___12899BBD]    Script Date: 03/12/2013 17:48:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__accounts__ac_in___12899BBD]') AND parent_object_id = OBJECT_ID(N'[dbo].[accounts]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__accounts__ac_in___12899BBD]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[accounts] ADD  CONSTRAINT [DF__accounts__ac_in___12899BBD]  DEFAULT ((0)) FOR [ac_in_session_won]
END


End
GO
/****** Object:  Default [DF__accounts__ac_in___137DBFF6]    Script Date: 03/12/2013 17:48:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__accounts__ac_in___137DBFF6]') AND parent_object_id = OBJECT_ID(N'[dbo].[accounts]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__accounts__ac_in___137DBFF6]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[accounts] ADD  CONSTRAINT [DF__accounts__ac_in___137DBFF6]  DEFAULT ((0)) FOR [ac_in_session_re_balance]
END


End
GO
/****** Object:  Default [DF__accounts__ac_in___1471E42F]    Script Date: 03/12/2013 17:48:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__accounts__ac_in___1471E42F]') AND parent_object_id = OBJECT_ID(N'[dbo].[accounts]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__accounts__ac_in___1471E42F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[accounts] ADD  CONSTRAINT [DF__accounts__ac_in___1471E42F]  DEFAULT ((0)) FOR [ac_in_session_promo_re_balance]
END


End
GO
/****** Object:  Default [DF_accounts_ac_in_session_promo_nr_balance]    Script Date: 03/12/2013 17:48:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_accounts_ac_in_session_promo_nr_balance]') AND parent_object_id = OBJECT_ID(N'[dbo].[accounts]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_accounts_ac_in_session_promo_nr_balance]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[accounts] ADD  CONSTRAINT [DF_accounts_ac_in_session_promo_nr_balance]  DEFAULT ((0)) FOR [ac_in_session_promo_nr_balance]
END


End
GO
/****** Object:  Default [DF__accounts__ac_in___23B427BF]    Script Date: 03/12/2013 17:48:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__accounts__ac_in___23B427BF]') AND parent_object_id = OBJECT_ID(N'[dbo].[accounts]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__accounts__ac_in___23B427BF]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[accounts] ADD  CONSTRAINT [DF__accounts__ac_in___23B427BF]  DEFAULT ((0)) FOR [ac_in_session_re_to_gm]
END


End
GO
/****** Object:  Default [DF__accounts__ac_in___24A84BF8]    Script Date: 03/12/2013 17:48:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__accounts__ac_in___24A84BF8]') AND parent_object_id = OBJECT_ID(N'[dbo].[accounts]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__accounts__ac_in___24A84BF8]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[accounts] ADD  CONSTRAINT [DF__accounts__ac_in___24A84BF8]  DEFAULT ((0)) FOR [ac_in_session_promo_re_to_gm]
END


End
GO
/****** Object:  Default [DF_accounts_ac_in_session_promo_nr_to_gm]    Script Date: 03/12/2013 17:48:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_accounts_ac_in_session_promo_nr_to_gm]') AND parent_object_id = OBJECT_ID(N'[dbo].[accounts]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_accounts_ac_in_session_promo_nr_to_gm]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[accounts] ADD  CONSTRAINT [DF_accounts_ac_in_session_promo_nr_to_gm]  DEFAULT ((0)) FOR [ac_in_session_promo_nr_to_gm]
END


End
GO
/****** Object:  Default [DF__accounts__ac_in___259C7031]    Script Date: 03/12/2013 17:48:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__accounts__ac_in___259C7031]') AND parent_object_id = OBJECT_ID(N'[dbo].[accounts]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__accounts__ac_in___259C7031]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[accounts] ADD  CONSTRAINT [DF__accounts__ac_in___259C7031]  DEFAULT ((0)) FOR [ac_in_session_re_from_gm]
END


End
GO
/****** Object:  Default [DF__accounts__ac_in___2690946A]    Script Date: 03/12/2013 17:48:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__accounts__ac_in___2690946A]') AND parent_object_id = OBJECT_ID(N'[dbo].[accounts]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__accounts__ac_in___2690946A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[accounts] ADD  CONSTRAINT [DF__accounts__ac_in___2690946A]  DEFAULT ((0)) FOR [ac_in_session_promo_re_from_gm]
END


End
GO
/****** Object:  Default [DF_accounts_ac_in_session_promo_nr_from_gm]    Script Date: 03/12/2013 17:48:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_accounts_ac_in_session_promo_nr_from_gm]') AND parent_object_id = OBJECT_ID(N'[dbo].[accounts]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_accounts_ac_in_session_promo_nr_from_gm]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[accounts] ADD  CONSTRAINT [DF_accounts_ac_in_session_promo_nr_from_gm]  DEFAULT ((0)) FOR [ac_in_session_promo_nr_from_gm]
END


End
GO
/****** Object:  Default [DF__accounts__ac_in___2784B8A3]    Script Date: 03/12/2013 17:48:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__accounts__ac_in___2784B8A3]') AND parent_object_id = OBJECT_ID(N'[dbo].[accounts]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__accounts__ac_in___2784B8A3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[accounts] ADD  CONSTRAINT [DF__accounts__ac_in___2784B8A3]  DEFAULT ((0)) FOR [ac_in_session_re_played]
END


End
GO
/****** Object:  Default [DF__accounts__ac_in___2878DCDC]    Script Date: 03/12/2013 17:48:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__accounts__ac_in___2878DCDC]') AND parent_object_id = OBJECT_ID(N'[dbo].[accounts]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__accounts__ac_in___2878DCDC]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[accounts] ADD  CONSTRAINT [DF__accounts__ac_in___2878DCDC]  DEFAULT ((0)) FOR [ac_in_session_nr_played]
END


End
GO
/****** Object:  Default [DF__accounts__ac_in___296D0115]    Script Date: 03/12/2013 17:48:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__accounts__ac_in___296D0115]') AND parent_object_id = OBJECT_ID(N'[dbo].[accounts]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__accounts__ac_in___296D0115]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[accounts] ADD  CONSTRAINT [DF__accounts__ac_in___296D0115]  DEFAULT ((0)) FOR [ac_in_session_re_won]
END


End
GO
/****** Object:  Default [DF__accounts__ac_in___2A61254E]    Script Date: 03/12/2013 17:48:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__accounts__ac_in___2A61254E]') AND parent_object_id = OBJECT_ID(N'[dbo].[accounts]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__accounts__ac_in___2A61254E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[accounts] ADD  CONSTRAINT [DF__accounts__ac_in___2A61254E]  DEFAULT ((0)) FOR [ac_in_session_nr_won]
END


End
GO
/****** Object:  Default [DF__accounts__ac_in___32024716]    Script Date: 03/12/2013 17:48:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__accounts__ac_in___32024716]') AND parent_object_id = OBJECT_ID(N'[dbo].[accounts]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__accounts__ac_in___32024716]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[accounts] ADD  CONSTRAINT [DF__accounts__ac_in___32024716]  DEFAULT ((0)) FOR [ac_in_session_re_cancellable]
END


End
GO
/****** Object:  Default [DF__accounts__ac_in___32F66B4F]    Script Date: 03/12/2013 17:48:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__accounts__ac_in___32F66B4F]') AND parent_object_id = OBJECT_ID(N'[dbo].[accounts]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__accounts__ac_in___32F66B4F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[accounts] ADD  CONSTRAINT [DF__accounts__ac_in___32F66B4F]  DEFAULT ((0)) FOR [ac_in_session_promo_re_cancellable]
END


End
GO
/****** Object:  Default [DF_accounts_ac_in_session_promo_nr_cancellable]    Script Date: 03/12/2013 17:48:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_accounts_ac_in_session_promo_nr_cancellable]') AND parent_object_id = OBJECT_ID(N'[dbo].[accounts]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_accounts_ac_in_session_promo_nr_cancellable]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[accounts] ADD  CONSTRAINT [DF_accounts_ac_in_session_promo_nr_cancellable]  DEFAULT ((0)) FOR [ac_in_session_promo_nr_cancellable]
END


End
GO
/****** Object:  Default [DF_accounts_ac_in_session_cancellable_transaction_id]    Script Date: 03/12/2013 17:48:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_accounts_ac_in_session_cancellable_transaction_id]') AND parent_object_id = OBJECT_ID(N'[dbo].[accounts]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_accounts_ac_in_session_cancellable_transaction_id]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[accounts] ADD  CONSTRAINT [DF_accounts_ac_in_session_cancellable_transaction_id]  DEFAULT ((0)) FOR [ac_in_session_cancellable_transaction_id]
END


End
GO
/****** Object:  Default [DF_accounts_ac_holder_id_type]    Script Date: 03/12/2013 17:48:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_accounts_ac_holder_id_type]') AND parent_object_id = OBJECT_ID(N'[dbo].[accounts]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_accounts_ac_holder_id_type]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[accounts] ADD  CONSTRAINT [DF_accounts_ac_holder_id_type]  DEFAULT ((3)) FOR [ac_holder_id_type]
END


End
GO
/****** Object:  Default [DF_alarms_al_reported]    Script Date: 03/12/2013 17:48:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_alarms_al_reported]') AND parent_object_id = OBJECT_ID(N'[dbo].[alarms]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_alarms_al_reported]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[alarms] ADD  CONSTRAINT [DF_alarms_al_reported]  DEFAULT (getdate()) FOR [al_reported]
END


End
GO
/****** Object:  Default [DF_gui_users_gu_user_type]    Script Date: 03/12/2013 17:48:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_gui_users_gu_user_type]') AND parent_object_id = OBJECT_ID(N'[dbo].[gui_users]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_gui_users_gu_user_type]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[gui_users] ADD  CONSTRAINT [DF_gui_users_gu_user_type]  DEFAULT ((0)) FOR [gu_user_type]
END


End
GO
/****** Object:  Default [DF_gui_users_gu_block_reason]    Script Date: 03/12/2013 17:48:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_gui_users_gu_block_reason]') AND parent_object_id = OBJECT_ID(N'[dbo].[gui_users]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_gui_users_gu_block_reason]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[gui_users] ADD  CONSTRAINT [DF_gui_users_gu_block_reason]  DEFAULT ((0)) FOR [gu_block_reason]
END


End
GO
/****** Object:  Default [DF_wsi_licence_wl_insertion_date]    Script Date: 03/12/2013 17:48:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_wsi_licence_wl_insertion_date]') AND parent_object_id = OBJECT_ID(N'[dbo].[licences]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_wsi_licence_wl_insertion_date]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[licences] ADD  CONSTRAINT [DF_wsi_licence_wl_insertion_date]  DEFAULT (getdate()) FOR [wl_insertion_date]
END


End
GO
/****** Object:  Default [DF_ms_site_tasks_st_enabled]    Script Date: 03/12/2013 17:48:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_ms_site_tasks_st_enabled]') AND parent_object_id = OBJECT_ID(N'[dbo].[ms_site_tasks]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_ms_site_tasks_st_enabled]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ms_site_tasks] ADD  CONSTRAINT [DF_ms_site_tasks_st_enabled]  DEFAULT ((1)) FOR [st_enabled]
END


End
GO
/****** Object:  Default [DF_ms_site_tasks_st_running]    Script Date: 03/12/2013 17:48:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_ms_site_tasks_st_running]') AND parent_object_id = OBJECT_ID(N'[dbo].[ms_site_tasks]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_ms_site_tasks_st_running]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ms_site_tasks] ADD  CONSTRAINT [DF_ms_site_tasks_st_running]  DEFAULT ((0)) FOR [st_running]
END


End
GO
/****** Object:  Default [DF_site_jobs_sjb_enabled]    Script Date: 03/12/2013 17:48:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_site_jobs_sjb_enabled]') AND parent_object_id = OBJECT_ID(N'[dbo].[site_jobs]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_site_jobs_sjb_enabled]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[site_jobs] ADD  CONSTRAINT [DF_site_jobs_sjb_enabled]  DEFAULT ((1)) FOR [sjb_enabled]
END


End
GO
/****** Object:  Default [DF_site_jobs_sjb_period]    Script Date: 03/12/2013 17:48:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_site_jobs_sjb_period]') AND parent_object_id = OBJECT_ID(N'[dbo].[site_jobs]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_site_jobs_sjb_period]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[site_jobs] ADD  CONSTRAINT [DF_site_jobs_sjb_period]  DEFAULT (((1)*(60))*(60)) FOR [sjb_period]
END


End
GO
/****** Object:  Default [DF_site_jobs_sjb_order]    Script Date: 03/12/2013 17:48:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_site_jobs_sjb_order]') AND parent_object_id = OBJECT_ID(N'[dbo].[site_jobs]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_site_jobs_sjb_order]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[site_jobs] ADD  CONSTRAINT [DF_site_jobs_sjb_order]  DEFAULT ((0)) FOR [sjb_order]
END


End
GO
/****** Object:  Default [DF_site_jobs_sjb_num_tries]    Script Date: 03/12/2013 17:48:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_site_jobs_sjb_num_tries]') AND parent_object_id = OBJECT_ID(N'[dbo].[site_jobs]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_site_jobs_sjb_num_tries]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[site_jobs] ADD  CONSTRAINT [DF_site_jobs_sjb_num_tries]  DEFAULT ((0)) FOR [sjb_num_tries]
END


End
GO
/****** Object:  Default [DF_site_jobs_sjb_exec_n]    Script Date: 03/12/2013 17:48:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_site_jobs_sjb_exec_n]') AND parent_object_id = OBJECT_ID(N'[dbo].[site_jobs]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_site_jobs_sjb_exec_n]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[site_jobs] ADD  CONSTRAINT [DF_site_jobs_sjb_exec_n]  DEFAULT ((0)) FOR [sjb_num_executions]
END


End
GO
/****** Object:  Default [DF_site_jobs_sjb_exec_s]    Script Date: 03/12/2013 17:48:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_site_jobs_sjb_exec_s]') AND parent_object_id = OBJECT_ID(N'[dbo].[site_jobs]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_site_jobs_sjb_exec_s]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[site_jobs] ADD  CONSTRAINT [DF_site_jobs_sjb_exec_s]  DEFAULT ((0)) FOR [sjb_total_execution_time]
END


End
GO
/****** Object:  Default [DF_site_jobs_sjb_cmd_n]    Script Date: 03/12/2013 17:48:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_site_jobs_sjb_cmd_n]') AND parent_object_id = OBJECT_ID(N'[dbo].[site_jobs]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_site_jobs_sjb_cmd_n]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[site_jobs] ADD  CONSTRAINT [DF_site_jobs_sjb_cmd_n]  DEFAULT ((0)) FOR [sjb_num_messages]
END


End
GO
/****** Object:  Default [DF_site_jobs_sjb_cmd_s]    Script Date: 03/12/2013 17:48:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_site_jobs_sjb_cmd_s]') AND parent_object_id = OBJECT_ID(N'[dbo].[site_jobs]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_site_jobs_sjb_cmd_s]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[site_jobs] ADD  CONSTRAINT [DF_site_jobs_sjb_cmd_s]  DEFAULT ((0)) FOR [sjb_total_message_time]
END


End
GO
/****** Object:  Default [DF_site_jobs_sjb_total_roundtrip_time]    Script Date: 03/12/2013 17:48:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_site_jobs_sjb_total_roundtrip_time]') AND parent_object_id = OBJECT_ID(N'[dbo].[site_jobs]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_site_jobs_sjb_total_roundtrip_time]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[site_jobs] ADD  CONSTRAINT [DF_site_jobs_sjb_total_roundtrip_time]  DEFAULT ((0)) FOR [sjb_total_roundtrip_time]
END


End
GO
/****** Object:  Default [DF_sites_st_status]    Script Date: 03/12/2013 17:48:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_sites_st_status]') AND parent_object_id = OBJECT_ID(N'[dbo].[sites]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_sites_st_status]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[sites] ADD  CONSTRAINT [DF_sites_st_status]  DEFAULT ((0)) FOR [st_state]
END


End
GO
/****** Object:  Default [DF_kiosk_software_versions_ksv_insertion_date]    Script Date: 03/12/2013 17:48:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_kiosk_software_versions_ksv_insertion_date]') AND parent_object_id = OBJECT_ID(N'[dbo].[terminal_software_versions]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_kiosk_software_versions_ksv_insertion_date]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[terminal_software_versions] ADD  CONSTRAINT [DF_kiosk_software_versions_ksv_insertion_date]  DEFAULT (getdate()) FOR [tsv_insertion_date]
END


End
GO
/****** Object:  Default [DF_wwp_sessions_wws_last_sequence_id]    Script Date: 03/12/2013 17:48:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_wwp_sessions_wws_last_sequence_id]') AND parent_object_id = OBJECT_ID(N'[dbo].[wwp_sessions]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_wwp_sessions_wws_last_sequence_id]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[wwp_sessions] ADD  CONSTRAINT [DF_wwp_sessions_wws_last_sequence_id]  DEFAULT ((0)) FOR [wws_last_sequence_id]
END


End
GO
/****** Object:  Default [DF_wwp_sessions_wws_last_transaction_id]    Script Date: 03/12/2013 17:48:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_wwp_sessions_wws_last_transaction_id]') AND parent_object_id = OBJECT_ID(N'[dbo].[wwp_sessions]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_wwp_sessions_wws_last_transaction_id]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[wwp_sessions] ADD  CONSTRAINT [DF_wwp_sessions_wws_last_transaction_id]  DEFAULT ((0)) FOR [wws_last_transaction_id]
END


End
GO
/****** Object:  Default [DF_wwp_sessions_wws_started]    Script Date: 03/12/2013 17:48:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_wwp_sessions_wws_started]') AND parent_object_id = OBJECT_ID(N'[dbo].[wwp_sessions]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_wwp_sessions_wws_started]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[wwp_sessions] ADD  CONSTRAINT [DF_wwp_sessions_wws_started]  DEFAULT (getdate()) FOR [wws_started]
END


End
GO
/****** Object:  Default [DF_wwp_sessions_wws_status]    Script Date: 03/12/2013 17:48:27 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_wwp_sessions_wws_status]') AND parent_object_id = OBJECT_ID(N'[dbo].[wwp_sessions]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_wwp_sessions_wws_status]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[wwp_sessions] ADD  CONSTRAINT [DF_wwp_sessions_wws_status]  DEFAULT ((0)) FOR [wws_status]
END


End
GO
/****** Object:  ForeignKey [FK_gui_audit_gui_users]    Script Date: 03/12/2013 17:48:27 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_gui_audit_gui_users]') AND parent_object_id = OBJECT_ID(N'[dbo].[gui_audit]'))
ALTER TABLE [dbo].[gui_audit]  WITH CHECK ADD  CONSTRAINT [FK_gui_audit_gui_users] FOREIGN KEY([ga_gui_user_id])
REFERENCES [dbo].[gui_users] ([gu_user_id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_gui_audit_gui_users]') AND parent_object_id = OBJECT_ID(N'[dbo].[gui_audit]'))
ALTER TABLE [dbo].[gui_audit] CHECK CONSTRAINT [FK_gui_audit_gui_users]
GO
/****** Object:  ForeignKey [FK_gui_profile_forms_gui_forms]    Script Date: 03/12/2013 17:48:27 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_gui_profile_forms_gui_forms]') AND parent_object_id = OBJECT_ID(N'[dbo].[gui_profile_forms]'))
ALTER TABLE [dbo].[gui_profile_forms]  WITH CHECK ADD  CONSTRAINT [FK_gui_profile_forms_gui_forms] FOREIGN KEY([gpf_gui_id], [gpf_form_id])
REFERENCES [dbo].[gui_forms] ([gf_gui_id], [gf_form_id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_gui_profile_forms_gui_forms]') AND parent_object_id = OBJECT_ID(N'[dbo].[gui_profile_forms]'))
ALTER TABLE [dbo].[gui_profile_forms] CHECK CONSTRAINT [FK_gui_profile_forms_gui_forms]
GO
/****** Object:  ForeignKey [FK_gui_profile_forms_gui_user_profiles]    Script Date: 03/12/2013 17:48:27 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_gui_profile_forms_gui_user_profiles]') AND parent_object_id = OBJECT_ID(N'[dbo].[gui_profile_forms]'))
ALTER TABLE [dbo].[gui_profile_forms]  WITH CHECK ADD  CONSTRAINT [FK_gui_profile_forms_gui_user_profiles] FOREIGN KEY([gpf_profile_id])
REFERENCES [dbo].[gui_user_profiles] ([gup_profile_id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_gui_profile_forms_gui_user_profiles]') AND parent_object_id = OBJECT_ID(N'[dbo].[gui_profile_forms]'))
ALTER TABLE [dbo].[gui_profile_forms] CHECK CONSTRAINT [FK_gui_profile_forms_gui_user_profiles]
GO
/****** Object:  ForeignKey [FK_gui_users_gui_user_profiles]    Script Date: 03/12/2013 17:48:27 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_gui_users_gui_user_profiles]') AND parent_object_id = OBJECT_ID(N'[dbo].[gui_users]'))
ALTER TABLE [dbo].[gui_users]  WITH CHECK ADD  CONSTRAINT [FK_gui_users_gui_user_profiles] FOREIGN KEY([gu_profile_id])
REFERENCES [dbo].[gui_user_profiles] ([gup_profile_id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_gui_users_gui_user_profiles]') AND parent_object_id = OBJECT_ID(N'[dbo].[gui_users]'))
ALTER TABLE [dbo].[gui_users] CHECK CONSTRAINT [FK_gui_users_gui_user_profiles]
GO
/****** Object:  ForeignKey [FK_sites_site_operators]    Script Date: 03/12/2013 17:48:27 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_sites_site_operators]') AND parent_object_id = OBJECT_ID(N'[dbo].[sites]'))
ALTER TABLE [dbo].[sites]  WITH CHECK ADD  CONSTRAINT [FK_sites_site_operators] FOREIGN KEY([st_operator_id])
REFERENCES [dbo].[site_operators] ([sop_operator_id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_sites_site_operators]') AND parent_object_id = OBJECT_ID(N'[dbo].[sites]'))
ALTER TABLE [dbo].[sites] CHECK CONSTRAINT [FK_sites_site_operators]
GO
/****** Object:  ForeignKey [FK_wwp_sessions_sites]    Script Date: 03/12/2013 17:48:27 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_wwp_sessions_sites]') AND parent_object_id = OBJECT_ID(N'[dbo].[wwp_sessions]'))
ALTER TABLE [dbo].[wwp_sessions]  WITH CHECK ADD  CONSTRAINT [FK_wwp_sessions_sites] FOREIGN KEY([wws_site_id])
REFERENCES [dbo].[sites] ([st_site_id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_wwp_sessions_sites]') AND parent_object_id = OBJECT_ID(N'[dbo].[wwp_sessions]'))
ALTER TABLE [dbo].[wwp_sessions] CHECK CONSTRAINT [FK_wwp_sessions_sites]
GO

/* Permissions */
USE [wgdb_100]
GO
GRANT CONNECT TO [wggui]
GO
GRANT CONNECT TO [wgpublic]
GO
GRANT CONNECT TO [wgroot]
GO
USE [wgdb_100]
GO
EXEC sp_addrolemember 'db_datawriter', 'wggui'
GO
EXEC sp_addrolemember 'db_datareader', 'wggui'
GO
EXEC sp_addrolemember 'db_owner', 'wgroot'
GO
GRANT SELECT ON [dbo].[db_users] TO [wgpublic]
GO
GRANT SELECT ON [dbo].[db_version] TO [wgpublic]
GO

/****** Initial Inserts ******/
/*** DB Users ***/  
INSERT INTO [dbo].[db_users]
           ([du_username] 
           ,[du_password]) 
     VALUES 
           ('wggui_100' 
           ,cast(0x826A41E8E27225F6FCA6AD56A206706742087BFCA60E4E27ECE0F0954EC61694FCDED722A6C37263 as binary(40)) ) 
GO 

INSERT INTO [dbo].[db_users] 
           ([du_username] 
           ,[du_password]) 
     VALUES 
           ('wgroot_100' 
           ,cast(0xF51473ADD352F9EE7A0008C913B314F5104B66DCC7F83C336A95EA92205E55A1BC69AE262EB9F8CD as binary(40)) ) 
GO
/*** GUI SU Profile ***/
INSERT INTO [dbo].[gui_user_profiles]
           ([gup_profile_id]
           ,[gup_name])
     VALUES
           ( 0
           , 'PROFILE_SU')
GO
/*** SU ***/
INSERT INTO [dbo].[gui_users]
           ([gu_user_id]
           ,[gu_profile_id]
           ,[gu_username]
           ,[gu_enabled]
           ,[gu_password]
           ,[gu_not_valid_before]
           ,[gu_not_valid_after]
           ,[gu_last_changed]
           ,[gu_password_exp]
           ,[gu_pwd_chg_req]
           ,[gu_login_failures]
           ,[gu_password_h1]
           ,[gu_password_h2]
           ,[gu_password_h3]
           ,[gu_password_h4]
           ,[gu_password_h5]
           ,[gu_full_name])
     VALUES
           (0
           ,0
           ,'SU'
           ,1
           ,cast('wigos00' as binary(40))
           ,getdate()
           ,null
           ,null
           ,null
           ,1
           ,null
           ,null
           ,null
           ,null
           ,null
           ,null
           ,'Super User')
GO
/*** Default GUI User Profiles ***/
INSERT INTO [dbo].[gui_user_profiles]
           ([gup_profile_id]
           ,[gup_name])
     VALUES
           ( 1
           , 'DEFAULT_PROFILE')
GO


/****** Object:  Table dbo.general_params    Script Date: 03/12/2013 17:51:35 ******/
INSERT dbo.general_params (gp_group_key, gp_subject_key, gp_key_value, gp_timestamp) 
          SELECT  N'MultiSite', N'CenterAddress1', N'', NULL                              -- TODO DEFAULT BLANK (198.168.1.1:1500)
UNION ALL SELECT  N'MultiSite', N'CenterAddress2', N'', NULL                              -- TODO DEFAULT BLANK (198.168.1.1:1500)
UNION ALL SELECT  N'MultiSite', N'IsCenter', N'1', NULL
UNION ALL SELECT  N'MultiSite', N'MembersCardId', N'', NULL                               -- TODO DEFAULT BLANK
UNION ALL SELECT  N'PlayerTracking', N'GiftExpirationDays', N'1', NULL
UNION ALL SELECT  N'PlayerTracking', N'Level01.Name', N'BRONZE', NULL
UNION ALL SELECT  N'PlayerTracking', N'Level01.RedeemablePlayedTo1Point', N'1', NULL
UNION ALL SELECT  N'PlayerTracking', N'Level01.RedeemableSpentTo1Point', N'1', NULL
UNION ALL SELECT  N'PlayerTracking', N'Level01.TotalPlayedTo1Point', N'1', NULL
UNION ALL SELECT  N'PlayerTracking', N'Level02.DaysOnLevel', N'1', NULL
UNION ALL SELECT  N'PlayerTracking', N'Level02.MaxDaysNoActivity', N'1', NULL
UNION ALL SELECT  N'PlayerTracking', N'Level02.Name', N'GOLD', NULL
UNION ALL SELECT  N'PlayerTracking', N'Level02.PointsToEnter', N'1000', NULL
UNION ALL SELECT  N'PlayerTracking', N'Level02.PointsToKeep', N'1', NULL
UNION ALL SELECT  N'PlayerTracking', N'Level02.RedeemablePlayedTo1Point', N'1002', NULL
UNION ALL SELECT  N'PlayerTracking', N'Level02.RedeemableSpentTo1Point', N'2', NULL
UNION ALL SELECT  N'PlayerTracking', N'Level02.TotalPlayedTo1Point', N'2', NULL
UNION ALL SELECT  N'PlayerTracking', N'Level03.DaysOnLevel', N'2', NULL
UNION ALL SELECT  N'PlayerTracking', N'Level03.MaxDaysNoActivity', N'2', NULL
UNION ALL SELECT  N'PlayerTracking', N'Level03.Name', N'PLATINUM', NULL
UNION ALL SELECT  N'PlayerTracking', N'Level03.PointsToEnter', N'2000', NULL
UNION ALL SELECT  N'PlayerTracking', N'Level03.PointsToKeep', N'2', NULL
UNION ALL SELECT  N'PlayerTracking', N'Level03.RedeemablePlayedTo1Point', N'1003', NULL
UNION ALL SELECT  N'PlayerTracking', N'Level03.RedeemableSpentTo1Point', N'3', NULL
UNION ALL SELECT  N'PlayerTracking', N'Level03.TotalPlayedTo1Point', N'3', NULL
UNION ALL SELECT  N'PlayerTracking', N'Level04.DaysOnLevel', N'3', NULL
UNION ALL SELECT  N'PlayerTracking', N'Level04.MaxDaysNoActivity', N'3', NULL
UNION ALL SELECT  N'PlayerTracking', N'Level04.Name', N'VIP', NULL
UNION ALL SELECT  N'PlayerTracking', N'Level04.PointsToEnter', N'3000', NULL
UNION ALL SELECT  N'PlayerTracking', N'Level04.PointsToKeep', N'3', NULL
UNION ALL SELECT  N'PlayerTracking', N'Level04.RedeemablePlayedTo1Point', N'4', NULL
UNION ALL SELECT  N'PlayerTracking', N'Level04.RedeemableSpentTo1Point', N'4', NULL
UNION ALL SELECT  N'PlayerTracking', N'Level04.TotalPlayedTo1Point', N'4', NULL
UNION ALL SELECT  N'PlayerTracking', N'Levels.DaysCountingPoints', N'6', NULL
UNION ALL SELECT  N'PlayerTracking', N'Levels.ExpirationDayMonth', N'05/12', NULL
UNION ALL SELECT  N'PlayerTracking', N'Levels.MaxNoActivityDays', N'180', NULL
UNION ALL SELECT  N'PlayerTracking', N'PointsExpirationDays', N'91', NULL
UNION ALL SELECT  N'PlayerTracking', N'TerminalDefaultPayout', N'95', NULL
UNION ALL SELECT  N'Site', N'Configured', N'', NULL                                       -- TODO DEFAULT BLANK
UNION ALL SELECT  N'Site', N'DisableNewAlarms', N'0', NULL                                -- TODO DEFAULT 0
UNION ALL SELECT  N'Site', N'Identifier', N'', NULL                                       -- TODO DEFAULT 0
UNION ALL SELECT  N'Site', N'Name', N'', NULL                                             -- TODO DEFAULT BLANK
UNION ALL SELECT  N'Software.Download', N'Location1', N'', NULL                           -- TODO DEFAULT BLANK (IP)
UNION ALL SELECT  N'Software.Download', N'Location2', N'', NULL
UNION ALL SELECT  N'User', N'Login.MinLength', N'0', NULL
UNION ALL SELECT  N'User', N'MaxDaysWithoutLogin', N'24', NULL
UNION ALL SELECT  N'User', N'MaxLoginAttempts', N'3', NULL
UNION ALL SELECT  N'User', N'Password.ChangeInNDays', N'60', NULL
UNION ALL SELECT  N'User', N'Password.MaxPasswordHistory', N'5', NULL
UNION ALL SELECT  N'User', N'Password.MinDigits', N'1', NULL
UNION ALL SELECT  N'User', N'Password.MinLength', N'5', NULL
UNION ALL SELECT  N'User', N'Password.MinLowerCase', N'1', NULL
UNION ALL SELECT  N'User', N'Password.MinSpecialCase', N'0', NULL
UNION ALL SELECT  N'User', N'Password.MinUpperCase', N'1', NULL
UNION ALL SELECT  N'User', N'Password.RulesEnabled', N'0', NULL
UNION ALL SELECT  N'User', N'SessionTimeOut.GUI', N'60', NULL
UNION ALL SELECT  N'WigosGUI', N'CurrencySymbol', N'$', NULL
UNION ALL SELECT  N'WigosGUI', N'Language', N'es', NULL
UNION ALL SELECT  N'WWP', N'ProvisioningManual', N'0', NULL                               -- TODO DEFAULT BLANK

/****** Object:  Table dbo.site_operators    Script Date: 03/12/2013 17:51:35 ******/
SET IDENTITY_INSERT dbo.site_operators ON
INSERT dbo.site_operators (sop_operator_id, sop_name) 
           SELECT 1, N'WSI'
SET IDENTITY_INSERT dbo.site_operators OFF


/****** Object:  Table dbo.sequences    Script Date: 03/12/2013 17:51:35 ******/
INSERT dbo.sequences (seq_id, seq_next_value)
           SELECT 10, 1 
 UNION ALL SELECT 11, 1 
 UNION ALL SELECT 12, 1 

--SITE_TASK
INSERT INTO MS_SITE_TASKS (ST_SITE_ID, ST_TASK_ID, ST_ENABLED, ST_INTERVAL_SECONDS) VALUES (0,  1, 1, 1200); --DownloadParameters
INSERT INTO MS_SITE_TASKS (ST_SITE_ID, ST_TASK_ID, ST_ENABLED, ST_INTERVAL_SECONDS) VALUES (0, 11, 1,   60); --UploadPointsMovements
INSERT INTO MS_SITE_TASKS (ST_SITE_ID, ST_TASK_ID, ST_ENABLED, ST_INTERVAL_SECONDS) VALUES (0, 12, 1,   60); --UploadPersonalInfo
INSERT INTO MS_SITE_TASKS (ST_SITE_ID, ST_TASK_ID, ST_ENABLED, ST_INTERVAL_SECONDS) VALUES (0, 21, 0, 1200); --DownloadPoints_Site
INSERT INTO MS_SITE_TASKS (ST_SITE_ID, ST_TASK_ID, ST_ENABLED, ST_INTERVAL_SECONDS) VALUES (0, 22, 1, 1200); --DownloadPoints_All 
INSERT INTO MS_SITE_TASKS (ST_SITE_ID, ST_TASK_ID, ST_ENABLED, ST_INTERVAL_SECONDS) VALUES (0, 31, 0, 1200); --DownloadPersonalInfo_Card
INSERT INTO MS_SITE_TASKS (ST_SITE_ID, ST_TASK_ID, ST_ENABLED, ST_INTERVAL_SECONDS) VALUES (0, 32, 0, 1200); --DownloadPersonalInfo_Site
INSERT INTO MS_SITE_TASKS (ST_SITE_ID, ST_TASK_ID, ST_ENABLED, ST_INTERVAL_SECONDS) VALUES (0, 33, 1, 1200); --DownloadPersonalInfo_All 

/**** PROCEDURES AND TRIGGERS SECTION *****/

--------------------------------------------------------------------------------
-- Copyright © 2013 Win Systems International
--------------------------------------------------------------------------------
--
--   MODULE NAME: Update_PointsAccountMovement.sql
--
--   DESCRIPTION: Update points to account & movement procedure
--
--        AUTHOR: Dani Domínguez
--
-- CREATION DATE: 07-MAR-2013
--
-- REVISION HISTORY:
--
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 07-MAR-2013 DDM    First release.
-- 07-MAR-2013 DDM    Add in/out parameters
--------------------------------------------------------------------------------


--------------------------------------------------------------------------------
-- PURPOSE: Update points to account & movement procedure
--
--  PARAMS:
--      - INPUT:
--           @pSiteId          
--           @pMovementId      
--           @pPlaySessionId   
--           @pAccountId       
--           @pTerminalId      
--           @pWcpSequenceId   
--           @pWcpTransactionId
--           @pDatetime        
--           @pType            
--           @pInitialBalance  
--           @pSubAmount       
--           @pAddAmount       
--           @pFinalBalance    
--           @pCashierId       
--           @pCashierName     
--           @pTperminalName   
--           @pOperationId     
--           @pDetails         
--           @pReasons         
--
--      - OUTPUT:
--           @pErrorCode           
--           @PointsSequenceId     
--           @Points               
--           @PointsStatus         
--           @HolderLevel          
--           @HolderLevelEntered   
--           @HolderLevelExpiration
--
-- RETURNS:
--
--   NOTES:
--
--------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Update_PointsAccountMovement]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Update_PointsAccountMovement]
GO
CREATE PROCEDURE [dbo].[Update_PointsAccountMovement]
      @pSiteId                    INT 
,     @pMovementId                BIGINT
,     @pPlaySessionId             BIGINT
,     @pAccountId                 BIGINT
,     @pTerminalId                INT
,     @pWcpSequenceId             BIGINT
,     @pWcpTransactionId          BIGINT
,     @pDatetime                  DATETIME
,     @pType                      INT
,     @pInitialBalance            MONEY
,     @pSubAmount                 MONEY
,     @pAddAmount                 MONEY
,     @pFinalBalance              MONEY
,     @pCashierId                 INT
,     @pCashierName               NVARCHAR(50)
,     @pTperminalName             NVARCHAR(50)
,     @pOperationId               BIGINT
,     @pDetails                   NVARCHAR(256)
,     @pReasons                   NVARCHAR(64)
, @pErrorCode                 INT          OUTPUT
, @PointsSequenceId           BIGINT       OUTPUT
, @Points                     MONEY        OUTPUT
, @PointsStatus               INT          OUTPUT  
, @HolderLevel                INT          OUTPUT
, @HolderLevelEntered         DATETIME     OUTPUT
, @HolderLevelExpiration      DATETIME     OUTPUT

AS
BEGIN   
  DECLARE @LocalDelta AS MONEY

  SET @pErrorCode = 1
  SET @PointsSequenceId = NULL
  SET @Points = null
  SET @PointsStatus = null
  SET @HolderLevel = null
  SET @HolderLevelEntered =  NULL
  SET @HolderLevelExpiration= NULL  
 
  IF NOT EXISTS (SELECT 1 FROM ACCOUNTS WHERE ac_account_id = @pAccountId ) 
  BEGIN
    SET @pErrorCode = 1
    RETURN
  END

  IF NOT EXISTS (SELECT 1 FROM ACCOUNT_MOVEMENTS WHERE AM_SITE_ID = @pSiteId AND AM_MOVEMENT_ID = @pMovementId) 
  BEGIN  
    INSERT INTO   ACCOUNT_MOVEMENTS
                ( [am_site_id]
                , [am_movement_id]
                , [am_play_session_id]
                , [am_account_id]
                , [am_terminal_id]
                , [am_wcp_sequence_id]
                , [am_wcp_transaction_id]
                , [am_datetime]
                , [am_type]
                , [am_initial_balance]
                , [am_sub_amount]
                , [am_add_amount]
                , [am_final_balance]
                , [am_cashier_id]
                , [am_cashier_name]
                , [am_terminal_name]
                , [am_operation_id]
                , [am_details]
                , [am_reasons])
         VALUES
                ( @pSiteId           
                , @pMovementId       
                , @pPlaySessionId   
                , @pAccountId        
                , @pTerminalId       
                , @pWcpSequenceId    
                , @pWcpTransactionId 
                , @pDatetime         
                , @pType             
                , @pInitialBalance  
                , @pSubAmount        
                , @pAddAmount        
                , @pFinalBalance     
                , @pCashierId        
                , @pCashierName      
                , @pTperminalName    
                , @pOperationId      
                , @pDetails          
                , @pReasons  )          
  
  SET @LocalDelta = ISNULL (@pAddAmount - @pSubAmount, 0)

  IF (@pType = 67)
    UPDATE   ACCOUNTS
       SET   AC_POINTS_STATUS    =  ISNULL(CAST(@pAddAmount AS INT), 1)
           , AC_MS_LAST_SITE_ID  = @pSiteId 
           , AC_LAST_ACTIVITY    = GETDATE()
     WHERE   AC_ACCOUNT_ID       = @pAccountId 
  ELSE IF (@pType = 39)
    UPDATE   ACCOUNTS
       SET   AC_MS_LAST_SITE_ID  = @pSiteId 
           , AC_LAST_ACTIVITY    = GETDATE()
     WHERE   AC_ACCOUNT_ID       = @pAccountId 
  ELSE
    UPDATE   ACCOUNTS
       SET   AC_POINTS           = AC_POINTS + @LocalDelta  
           , AC_MS_LAST_SITE_ID  = @pSiteId 
           , AC_LAST_ACTIVITY    = GETDATE()         
     WHERE   AC_ACCOUNT_ID       = @pAccountId 
     
END

  set @pErrorCode = 0

  SELECT   @PointsSequenceId     = AC_MS_POINTS_SEQ_ID 
         , @Points               = AC_POINTS
         , @PointsStatus         = AC_POINTS_STATUS 
         , @HolderLevel          = AC_HOLDER_LEVEL
         , @HolderLevelEntered   = AC_HOLDER_LEVEL_ENTERED
         , @HolderLevelExpiration= AC_HOLDER_LEVEL_EXPIRATION
    FROM   ACCOUNTS
   WHERE   AC_ACCOUNT_ID = @pAccountId 
   
END -- Update_PointsAccountMovement
GO



  --------------------------------------------------------------------------------
  -- Copyright © 2013 Win Systems International
  --------------------------------------------------------------------------------
  --
  --   MODULE NAME: Update_PersonalInfo.sql.sql
  --
  --   DESCRIPTION: Update personal Information 
  --
  --        AUTHOR: Dani Domínguez
  --
  -- CREATION DATE: 08-MAR-2013
  --
  -- REVISION HISTORY:
  --
  -- Date        Author Description
  -- ----------- ------ ----------------------------------------------------------
  -- 08-MAR-2013 DDM    First release.
  -------------------------------------------------------------------------------- 

  IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Update_PersonalInfo]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Update_PersonalInfo]
  GO
  CREATE PROCEDURE [dbo].[Update_PersonalInfo]
    @pCallingSiteId int
  , @pAccountId bigint
  , @pTrackData nvarchar(50)
  , @pAccountCreated Datetime 
  ,   @pHolderName nvarchar(200)
  ,   @pHolderId nvarchar(20)
  , @pHolderIdType int
  , @pHolderAddress01 nvarchar(50)
  , @pHolderAddress02 nvarchar(50)
  , @pHolderAddress03 nvarchar(50)
  , @pHolderCity nvarchar(50)
  , @pHolderZip  nvarchar(10) 
  , @pHolderEmail01 nvarchar(50)
  ,   @pHolderEmail02 nvarchar(50)
  ,   @pHolderPhoneNumber01 nvarchar(20)
  , @pHolderPhoneNumber02 nvarchar(20)
  , @pHolderComments nvarchar(100)
  , @pHolderId1 nvarchar(20)
  , @pHolderId2 nvarchar(20)
  , @pHolderDocumentId1 bigint
  , @pHolderDocumentId2 bigint
  , @pHolderName1 nvarchar(50)
  ,   @pHolderName2 nvarchar(50)
  ,   @pHolderName3 nvarchar(50)
  , @pHolderGender  int
  , @pHolderMaritalStatus int
  , @pHolderBirthDate datetime
  , @pHolderWeddingDate datetime
  , @pHolderLevel int
  , @pHolderLevelNotify int
  , @pHolderLevelEntered datetime
  ,   @pHolderLevelExpiration datetime
  ,   @pPin nvarchar(12)
  , @pPinFailures int
  , @pPinLastModified datetime
  , @pBlocked bit
  , @pActivated bit
  , @pBlockReason int
  , @pHolderIsVip int
  , @pHolderTitle nvarchar(15)                       
  , @pHolderName4 nvarchar(50)                       
  , @pHolderPhoneType01  int
  , @pHolderPhoneType02  int
  , @pHolderState    nvarchar                    
  , @pHolderCountry  nvarchar                     
  , @pUserType  int
  AS
  BEGIN

  DECLARE @pOtherAccountId as BIGINT
  DECLARE @IsNewAccount    as BIT

  SET @pOtherAccountId = ISNULL ((SELECT AC_ACCOUNT_ID FROM ACCOUNTS WHERE AC_TRACK_DATA = @pTrackData), 0)

  IF @pOtherAccountId <> 0 AND @pOtherAccountId <> @pAccountId
    UPDATE   ACCOUNTS 
       SET   AC_TRACK_DATA = '00000000000000000000-RECYCLED-' + CAST (@pOtherAccountId AS NVARCHAR) 
     WHERE   AC_ACCOUNT_ID = @pOtherAccountId

  SET @IsNewAccount = 0
  IF NOT EXISTS (SELECT 1 FROM ACCOUNTS WHERE ac_account_id = @pAccountId )
  BEGIN
    SET @IsNewAccount = 1

    INSERT INTO   ACCOUNTS 
                ( AC_ACCOUNT_ID, AC_TYPE, AC_BLOCKED, AC_MS_CREATED_ON_SITE_ID, AC_MS_LAST_SITE_ID, AC_CREATED) 
         VALUES ( @pAccountId,         2,          1, @pCallingSiteId         , @pCallingSiteId   , @pAccountCreated)

  END
   
   
    UPDATE   ACCOUNTS                   
       SET   AC_TRACK_DATA              = @pTrackData 
           , AC_HOLDER_NAME             = @pHolderName 
           , AC_HOLDER_ID               = @pHolderId 
           , AC_HOLDER_ID_TYPE          = @pHolderIdType 
           , AC_HOLDER_ADDRESS_01       = @pHolderAddress01 
           , AC_HOLDER_ADDRESS_02       = @pHolderAddress02 
           , AC_HOLDER_ADDRESS_03       = @pHolderAddress03 
           , AC_HOLDER_CITY             = @pHolderCity 
           , AC_HOLDER_ZIP              = @pHolderZip  
           , AC_HOLDER_EMAIL_01         = @pHolderEmail01 
           , AC_HOLDER_EMAIL_02         = @pHolderEmail02 
           , AC_HOLDER_PHONE_NUMBER_01  = @pHolderPhoneNumber01 
           , AC_HOLDER_PHONE_NUMBER_02  = @pHolderPhoneNumber02 
           , AC_HOLDER_COMMENTS         = @pHolderComments 
           , AC_HOLDER_ID1              = @pHolderId1 
           , AC_HOLDER_ID2              = @pHolderId2 
           , AC_HOLDER_DOCUMENT_ID1     = @pHolderDocumentId1 
           , AC_HOLDER_DOCUMENT_ID2     = @pHolderDocumentId2 
           , AC_HOLDER_NAME1            = @pHolderName1 
           , AC_HOLDER_NAME2            = @pHolderName2 
           , AC_HOLDER_NAME3            = @pHolderName3 
           , AC_HOLDER_GENDER           = @pHolderGender  
           , AC_HOLDER_MARITAL_STATUS   = @pHolderMaritalStatus 
           , AC_HOLDER_BIRTH_DATE       = @pHolderBirthDate 
           , AC_HOLDER_WEDDING_DATE     = @pHolderWeddingDate 
           , AC_HOLDER_LEVEL            = CASE WHEN (@IsNewAccount = 0) THEN AC_HOLDER_LEVEL            ELSE @pHolderLevel           END
           , AC_HOLDER_LEVEL_NOTIFY     = CASE WHEN (@IsNewAccount = 0) THEN AC_HOLDER_LEVEL_NOTIFY     ELSE @pHolderLevelNotify     END 
           , AC_HOLDER_LEVEL_ENTERED    = CASE WHEN (@IsNewAccount = 0) THEN AC_HOLDER_LEVEL_ENTERED    ELSE @pHolderLevelEntered    END  
           , AC_HOLDER_LEVEL_EXPIRATION = CASE WHEN (@IsNewAccount = 0) THEN AC_HOLDER_LEVEL_EXPIRATION ELSE @pHolderLevelExpiration END   
           , AC_PIN                     = @pPin 
           , AC_PIN_FAILURES            = @pPinFailures 
           , AC_PIN_LAST_MODIFIED       = @pPinLastModified 
           , AC_BLOCKED                 = @pBlocked 
           , AC_ACTIVATED               = @pActivated 
           , AC_BLOCK_REASON            = @pBlockReason 
           , AC_HOLDER_IS_VIP           = @pHolderIsVip 
           , AC_HOLDER_TITLE            = @pHolderTitle 
           , AC_HOLDER_NAME4            = @pHolderName4         
           , AC_HOLDER_PHONE_TYPE_01    = @pHolderPhoneType01  
           , AC_HOLDER_PHONE_TYPE_02    = @pHolderPhoneType02  
           , AC_HOLDER_STATE            = @pHolderState    
           , AC_HOLDER_COUNTRY          = @pHolderCountry        
           , AC_USER_TYPE               = @pUserType
           , AC_POINTS_STATUS           = CASE WHEN (@pUserType = 0) THEN NULL ELSE CASE WHEN (@IsNewAccount = 0) THEN AC_POINTS_STATUS ELSE 0 END END           
     WHERE   AC_ACCOUNT_ID              = @pAccountId 

  END
GO


--------------------------------------------------------------------------------
-- Copyright © 2013 Win Systems International 
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: MultiSiteTrigger_AccountUpdate.sql
-- 
--   DESCRIPTION: Procedures for trigger MultiSiteTrigger_AccountUpdate and related issues
-- 
--        AUTHOR: José Martínez
-- 
-- CREATION DATE: 07-MAR-2013
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 07-MAR-2013 JML    First release.
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- PURPOSE : Trigger on ACCOUNTS when center receive a changes, mark account to synchronize:
--                                                                      - Points if changed
--                                                                      - Personal info
-- 
--  PARAMS :
--      - INPUT :
--
--      - OUTPUT :
--
-- RETURNS :
--
--
--   NOTES :
--

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[MultiSiteTrigger_AccountUpdate]') AND type in (N'TR'))
DROP TRIGGER [dbo].[MultiSiteTrigger_AccountUpdate]
GO


CREATE TRIGGER [dbo].[MultiSiteTrigger_AccountUpdate] ON [dbo].[accounts]
   AFTER UPDATE
NOT FOR REPLICATION
AS 
BEGIN
    DECLARE @Sequence11Value    AS BIGINT
    DECLARE @Sequence10Value    AS BIGINT
    DECLARE @AccountId          AS BIGINT
    DECLARE @hash0              as varbinary(20)
    DECLARE @hash1              as varbinary(20)
    DECLARE @value              as nvarchar(max)
    DECLARE @changed            as bit
    DECLARE @updated            as bit
    
    SET @updated = 0;        
            
    IF (UPDATE(AC_MS_CHANGE_GUID)) RETURN
 
    IF (UPDATE(AC_POINTS) OR UPDATE(AC_POINTS_STATUS))
      BEGIN
          DECLARE PointsCursor CURSOR FOR 
           SELECT   INSERTED.AC_ACCOUNT_ID 
             FROM   INSERTED, DELETED 
            WHERE   INSERTED.AC_ACCOUNT_ID    =  DELETED.AC_ACCOUNT_ID
              AND ( INSERTED.AC_POINTS        <> DELETED.AC_POINTS
               OR   INSERTED.AC_POINTS_STATUS <> DELETED.AC_POINTS_STATUS )
               
        SET NOCOUNT ON;

        OPEN PointsCursor

        FETCH NEXT FROM PointsCursor INTO @AccountId
          
        WHILE @@FETCH_STATUS = 0
        BEGIN
            UPDATE   SEQUENCES 
               SET   SEQ_NEXT_VALUE = SEQ_NEXT_VALUE + 1
             WHERE   SEQ_ID         = 11

            SELECT   @Sequence11Value = SEQ_NEXT_VALUE - 1 FROM SEQUENCES WHERE SEQ_ID = 11

            UPDATE   ACCOUNTS
               SET   AC_MS_POINTS_SEQ_ID = @Sequence11Value 
             WHERE   AC_ACCOUNT_ID       = @AccountId

            FETCH NEXT FROM PointsCursor INTO @AccountId
        END

        CLOSE PointsCursor
        DEALLOCATE PointsCursor
    END


    IF UPDATE (AC_TRACK_DATA)
    OR UPDATE (AC_HOLDER_NAME)
    OR UPDATE (AC_HOLDER_ID)
    OR UPDATE (AC_HOLDER_ID_TYPE)
    OR UPDATE (AC_HOLDER_ADDRESS_01)
    OR UPDATE (AC_HOLDER_ADDRESS_02)
    OR UPDATE (AC_HOLDER_ADDRESS_03)
    OR UPDATE (AC_HOLDER_CITY)
    OR UPDATE (AC_HOLDER_ZIP)
    OR UPDATE (AC_HOLDER_EMAIL_01)
    OR UPDATE (AC_HOLDER_EMAIL_02)
    OR UPDATE (AC_HOLDER_PHONE_NUMBER_01)
    OR UPDATE (AC_HOLDER_PHONE_NUMBER_02)
    OR UPDATE (AC_HOLDER_COMMENTS)
    OR UPDATE (AC_HOLDER_ID1)
    OR UPDATE (AC_HOLDER_ID2)
    OR UPDATE (AC_HOLDER_DOCUMENT_ID1)
    OR UPDATE (AC_HOLDER_DOCUMENT_ID2)
    OR UPDATE (AC_HOLDER_NAME1)
    OR UPDATE (AC_HOLDER_NAME2)
    OR UPDATE (AC_HOLDER_NAME3)
    OR UPDATE (AC_HOLDER_GENDER)
    OR UPDATE (AC_HOLDER_MARITAL_STATUS)
    OR UPDATE (AC_HOLDER_BIRTH_DATE)
    OR UPDATE (AC_HOLDER_WEDDING_DATE)
    OR UPDATE (AC_HOLDER_LEVEL)
    OR UPDATE (AC_HOLDER_LEVEL_NOTIFY)
    OR UPDATE (AC_HOLDER_LEVEL_ENTERED)
    OR UPDATE (AC_HOLDER_LEVEL_EXPIRATION)
    OR UPDATE (AC_PIN)
    OR UPDATE (AC_PIN_FAILURES)
    OR UPDATE (AC_PIN_LAST_MODIFIED)
    OR UPDATE (AC_BLOCKED)
    OR UPDATE (AC_ACTIVATED)
    OR UPDATE (AC_BLOCK_REASON)
    OR UPDATE (AC_HOLDER_IS_VIP)
    OR UPDATE (AC_HOLDER_TITLE)
    OR UPDATE (AC_HOLDER_NAME4)
    OR UPDATE (AC_HOLDER_PHONE_TYPE_01)
    OR UPDATE (AC_HOLDER_PHONE_TYPE_02)
    OR UPDATE (AC_HOLDER_STATE)
    OR UPDATE (AC_HOLDER_COUNTRY)
    OR UPDATE (AC_USER_TYPE)
        SET @updated = 1;
        
    IF (@updated = 0) RETURN

    DECLARE PersonalInfoCursor CURSOR FOR 
     SELECT   AC_ACCOUNT_ID
            , HASHBYTES ('SHA1',  ISNULL(AC_TRACK_DATA,        '')
                                + ISNULL(AC_HOLDER_NAME,       '')
                                + ISNULL(AC_HOLDER_ID,         '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_ID_TYPE), '')
                                + ISNULL(AC_HOLDER_ADDRESS_01, '')
                                + ISNULL(AC_HOLDER_ADDRESS_02, '')
                                + ISNULL(AC_HOLDER_ADDRESS_03, '')
                                + ISNULL(AC_HOLDER_CITY,       '')
                                + ISNULL(AC_HOLDER_ZIP,        '')
                                + ISNULL(AC_HOLDER_EMAIL_01,   '')
                                + ISNULL(AC_HOLDER_EMAIL_02,   '')
                                + ISNULL(AC_HOLDER_PHONE_NUMBER_01, '')
                                + ISNULL(AC_HOLDER_PHONE_NUMBER_02, '')
                                + ISNULL(AC_HOLDER_COMMENTS,        '')
                                + ISNULL(AC_HOLDER_ID1, '')
                                + ISNULL(AC_HOLDER_ID2, '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_DOCUMENT_ID1), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_DOCUMENT_ID2), '')
                                + ISNULL(AC_HOLDER_NAME1, '')
                                + ISNULL(AC_HOLDER_NAME2, '')
                                + ISNULL(AC_HOLDER_NAME3, '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_GENDER),         '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_MARITAL_STATUS), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_BIRTH_DATE,   21), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_WEDDING_DATE, 21), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_LEVEL),                '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_LEVEL_NOTIFY),         '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_LEVEL_ENTERED,    21), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_LEVEL_EXPIRATION, 21), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_PIN),          '')
                                + ISNULL(CONVERT(NVARCHAR, AC_PIN_FAILURES), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_PIN_LAST_MODIFIED, 21), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_BLOCKED),           '')
                                + ISNULL(CONVERT(NVARCHAR, AC_ACTIVATED),         '')
                                + ISNULL(CONVERT(NVARCHAR, AC_BLOCK_REASON),         '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_IS_VIP),        '')
                                + ISNULL(AC_HOLDER_TITLE                        , '')
                                + ISNULL(AC_HOLDER_NAME4                        , '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_PHONE_TYPE_01),        '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_PHONE_TYPE_02),        '')
                                + ISNULL(AC_HOLDER_STATE                        , '')
                                + ISNULL(AC_HOLDER_COUNTRY                      , '')
                                + ISNULL(CONVERT(NVARCHAR,AC_USER_TYPE)         , '') )
       FROM   INSERTED

    SET NOCOUNT ON;

    OPEN PersonalInfoCursor

    FETCH NEXT FROM PersonalInfoCursor INTO @AccountId, @hash1

    WHILE @@FETCH_STATUS = 0
    BEGIN

        SELECT @hash0 = AC_MS_HASH FROM DELETED WHERE AC_ACCOUNT_ID = @AccountId

        SELECT @changed = CASE WHEN ( @hash0 = @hash1 ) THEN 0 ELSE 1 END

        IF @changed = 1
        BEGIN
            -- Personal Info
            UPDATE   SEQUENCES 
               SET   SEQ_NEXT_VALUE = SEQ_NEXT_VALUE + 1
             WHERE   SEQ_ID         = 10

            SELECT   @Sequence10Value = SEQ_NEXT_VALUE - 1 FROM SEQUENCES WHERE SEQ_ID = 10

            UPDATE   ACCOUNTS
               SET   AC_MS_PERSONAL_INFO_SEQ_ID = @Sequence10Value
                   , AC_MS_HASH                 = @hash1
                   , AC_MS_CHANGE_GUID          = NEWID()
             WHERE   AC_ACCOUNT_ID              = @AccountId
        END

        FETCH NEXT FROM PersonalInfoCursor INTO @AccountId, @hash1
    END

    CLOSE PersonalInfoCursor
    DEALLOCATE PersonalInfoCursor

END

GO


--------------------------------------------------------------------------------
-- Copyright © 2013 Win Systems International 
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: MultiSiteTrigger_AccountInsert.sql
-- 
--   DESCRIPTION: Procedures for trigger MultiSiteTrigger_AccountInsert and related issues
-- 
--        AUTHOR: José Martínez
-- 
-- CREATION DATE: 07-MAR-2013
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 07-MAR-2013 JML    First release.
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- PURPOSE : Trigger on ACCOUNTS when center receive a new account, mark it to synchronize.
-- 
--  PARAMS :
--      - INPUT :
--
--      - OUTPUT :
--
-- RETURNS :
--
--
--   NOTES :
--

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[MultiSiteTrigger_AccountInsert]') AND type in (N'TR'))
DROP TRIGGER [dbo].[MultiSiteTrigger_AccountInsert]
GO

CREATE TRIGGER [dbo].[MultiSiteTrigger_AccountInsert] ON [dbo].[accounts]
   AFTER INSERT
NOT FOR REPLICATION
AS 
BEGIN
    DECLARE @Sequence10Value AS BIGINT
    DECLARE @Sequence11Value AS BIGINT
    DECLARE @AccountId       AS BIGINT

    RAISERROR ('Row Inserted!', 0, 0) WITH LOG

    DECLARE InsertedCursor CURSOR FOR 
     SELECT   INSERTED.AC_ACCOUNT_ID 
       FROM   INSERTED 

    SET NOCOUNT ON;

    OPEN InsertedCursor

    FETCH NEXT FROM InsertedCursor INTO @AccountId
    
    WHILE @@FETCH_STATUS = 0
    BEGIN

        -- Personal Info
        UPDATE   SEQUENCES 
           SET   SEQ_NEXT_VALUE = SEQ_NEXT_VALUE + 1
         WHERE   SEQ_ID         = 10

        -- Points
        UPDATE   SEQUENCES 
           SET   SEQ_NEXT_VALUE = SEQ_NEXT_VALUE + 1
         WHERE   SEQ_ID         = 11

        SELECT   @Sequence10Value = SEQ_NEXT_VALUE - 1 FROM SEQUENCES WHERE SEQ_ID = 10
        SELECT   @Sequence11Value = SEQ_NEXT_VALUE - 1 FROM SEQUENCES WHERE SEQ_ID = 11

        UPDATE   ACCOUNTS
           SET   AC_MS_PERSONAL_INFO_SEQ_ID = @Sequence10Value
               , AC_MS_POINTS_SEQ_ID        = @Sequence11Value 
         WHERE   AC_ACCOUNT_ID              = @AccountId

        FETCH NEXT FROM InsertedCursor INTO @AccountId
    END

    CLOSE InsertedCursor
    DEALLOCATE InsertedCursor

END

GO


/**** DECLARATION SECTION *****/
DECLARE 
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000)

/**** INITIALIZATION SECTION *****/
SET @New_ReleaseId = 1;
SET @New_ScriptName = N'MultiSite_Center_Create_18.001.sql';
SET @New_Description = N'MultiSite DB first Release.';

/*** DB Version ***/
INSERT INTO [dbo].[db_version]
           ([db_client_id]
           ,[db_common_build_id]
           ,[db_client_build_id]
           ,[db_release_id]
           ,[db_updated_script]
           ,[db_updated]
           ,[db_description])
     VALUES
           (18
           ,100
           ,1
           ,@New_ReleaseId
           ,@New_ScriptName
           ,Getdate()
           ,@New_Description);

USE [wgdb_100]
GO
--INDEXES NON PARTITIONED: account_movements
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[account_movements]') AND name = N'PK_movements')
ALTER TABLE [dbo].[account_movements] DROP CONSTRAINT [PK_movements]
GO
ALTER TABLE [dbo].[account_movements] ADD  CONSTRAINT [PK_movements] PRIMARY KEY NONCLUSTERED
(
	[am_site_id] ASC,
	[am_movement_id] ASC,
	[am_datetime] ASC
) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[account_movements]') AND name = N'IX_am_datetime')
DROP INDEX [IX_am_datetime] ON [dbo].[account_movements] WITH ( ONLINE = OFF )
GO
CREATE CLUSTERED INDEX [IX_am_datetime] ON [dbo].[account_movements] 
(
	[am_datetime] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[account_movements]') AND name = N'IX_movements_account_date')
DROP INDEX [IX_movements_account_date] ON [dbo].[account_movements] WITH ( ONLINE = OFF )
GO
CREATE NONCLUSTERED INDEX [IX_movements_account_date] ON [dbo].[account_movements] 
(
	[am_account_id] ASC,
	[am_datetime] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

--INDEXES NON PARTITIONED: alarms
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[alarms]') AND name = N'PK_alarms')
ALTER TABLE [dbo].[alarms] DROP CONSTRAINT [PK_alarms]
GO
ALTER TABLE [dbo].[alarms] ADD CONSTRAINT [PK_alarms] PRIMARY KEY CLUSTERED 
(
	[al_alarm_id] ASC,
	[al_reported] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

--INDEXES NON PARTITIONED: gui_audit
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[gui_audit]') AND name = N'PK_gui_audit')
ALTER TABLE [dbo].[gui_audit] DROP CONSTRAINT [PK_gui_audit]
GO
ALTER TABLE [dbo].[gui_audit] ADD CONSTRAINT [PK_gui_audit] PRIMARY KEY CLUSTERED 
(
	[ga_audit_id] ASC,
	[ga_item_order] ASC,
	[ga_datetime] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

--INDEXES NON PARTITIONED: wwp_sessions
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[wwp_sessions]') AND name = N'PK_wwp_sessions')
ALTER TABLE [dbo].[wwp_sessions] DROP CONSTRAINT [PK_wwp_sessions]
GO
ALTER TABLE [dbo].[wwp_sessions] ADD CONSTRAINT [PK_wwp_sessions] PRIMARY KEY CLUSTERED 
(
	[wws_session_id] ASC,
	[wws_started] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]






















	
