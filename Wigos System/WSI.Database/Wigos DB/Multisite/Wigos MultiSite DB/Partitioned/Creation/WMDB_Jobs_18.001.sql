/**** JOBS SECTION ****/
--/***************************** Job Add_partition_tables  *******************************/
--------------------------------------------------------------------------------
-- Copyright � 2013 Win Systems International
--------------------------------------------------------------------------------
--
--   MODULE NAME: Job_Add_partition_tables.sql
--
--   DESCRIPTION: This Job execute Partitioning Stored Procedure (Add_Partition_Tables)
--
--        AUTHOR: Jordi Vera
--
-- CREATION DATE: 18-MAR-2013
--
-- REVISION HISTORY:
--
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 18-MAR-2013 JVV    First release.
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
-- PURPOSE: Execute Stored Procedure [Add_Partition_Tables] with scheduler
--   NOTES:
--			Execution scheduled each day at 12.00 am
--			4 Steps configured:
--				EXEC Add_Partition_Tables @TableName='account_movements', @TypeRange='MONTH'
--				EXEC Add_Partition_Tables @TableName='alarms', @TypeRange='MONTH'
--				EXEC Add_Partition_Tables @TableName='gui_audit', @TypeRange='MONTH'
--				EXEC Add_Partition_Tables @TableName='wwp_sessions', @TypeRange='MONTH'
--			On error, every step is retried 2 times with a time interval of 60 minutes, whatever the outcome is advanced to the next step.
--			Each error is logged in the job history.
--			After the execution of all steps on failure generates an entry in the Windows Event Viewer.
--	PERMISSIONS:		
--			SQL user used to run the job: @owner_login_name=N'sa'  
--------------------------------------------------------------------------------

USE [msdb]
GO

/****** Object:  Job [Job_Add_partition_tables]    Script Date: 03/19/2013 12:44:14 ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [Database Maintenance]    Script Date: 03/19/2013 12:44:14 ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'Database Maintenance' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'Database Maintenance'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'Job_Add_partition_tables', 
		@enabled=1, 
		@notify_level_eventlog=2, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'Add partitions in WIGOS Multisite tables: account_movements, alarms, gui_audit, wwwp_sessions', 
		@category_name=N'Database Maintenance', 
		@owner_login_name=N'sa', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [EXEC Add_Partition_Tables @TableName='account_movements', @TypeRange='MONTH']    Script Date: 03/19/2013 12:44:14 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'EXEC Add_Partition_Tables @TableName=''account_movements'', @TypeRange=''MONTH''', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=3, 
		@on_fail_step_id=0, 
		@retry_attempts=2, 
		@retry_interval=60, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'EXEC Add_Partition_Tables @TableName=''account_movements'', @TypeRange=''MONTH''', 
		@database_name=N'wgdb_100', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [EXEC Add_Partition_Tables @TableName='alarms', @TypeRange='MONTH']    Script Date: 03/19/2013 12:44:14 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'EXEC Add_Partition_Tables @TableName=''alarms'', @TypeRange=''MONTH''', 
		@step_id=2, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=3, 
		@on_fail_step_id=0, 
		@retry_attempts=2, 
		@retry_interval=60, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'EXEC Add_Partition_Tables @TableName=''alarms'', @TypeRange=''MONTH''', 
		@database_name=N'wgdb_100', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [EXEC Add_Partition_Tables @TableName='gui_audit', @TypeRange='MONTH']    Script Date: 03/19/2013 12:44:14 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'EXEC Add_Partition_Tables @TableName=''gui_audit'', @TypeRange=''MONTH''', 
		@step_id=3, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=3, 
		@on_fail_step_id=0, 
		@retry_attempts=2, 
		@retry_interval=60, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'EXEC Add_Partition_Tables @TableName=''gui_audit'', @TypeRange=''MONTH''', 
		@database_name=N'wgdb_100', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [EXEC Add_Partition_Tables @TableName='wwp_sessions', @TypeRange='MONTH']    Script Date: 03/19/2013 12:44:14 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'EXEC Add_Partition_Tables @TableName=''wwp_sessions'', @TypeRange=''MONTH''', 
		@step_id=4, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=2, 
		@retry_interval=60, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'EXEC Add_Partition_Tables @TableName=''wwp_sessions'', @TypeRange=''MONTH''', 
		@database_name=N'wgdb_100', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Add partition tables Scheduler', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=1, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20130319, 
		@active_end_date=99991231, 
		@active_start_time=0, 
		@active_end_time=235959, 
		@schedule_uid=N'4ffbbf7a-1951-4a04-addb-a25c37ff552a'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:

GO






















