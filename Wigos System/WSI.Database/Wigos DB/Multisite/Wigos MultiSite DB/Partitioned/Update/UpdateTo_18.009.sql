/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_100]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 8;

SET @New_ReleaseId = 9;
SET @New_ScriptName = N'UpdateTo_18.009.sql';
SET @New_Description = N'ALTER TABLE gui_users ALTER COLUMNS: gu_logon_computer, gu_last_action, gu_full_name  NCHAR TO NVARCHAR; UPDATE gui_users COLUMNS gu_logon_computer, gu_last_action, gu_full_name WITH RTRIM';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/
ALTER TABLE gui_users ALTER COLUMN gu_logon_computer NVARCHAR(50) NULL
ALTER TABLE gui_users ALTER COLUMN gu_last_action NVARCHAR(50) NULL
ALTER TABLE gui_users ALTER COLUMN gu_full_name NVARCHAR(20) NULL
GO

/****** CONSTRAINTS ******/


/****** INDEXES ******/


/****** STORED PROCEDURES ******/


/****** TRIGGERS ******/


/****** RECORDS ******/ 
UPDATE gui_users SET gu_logon_computer = RTRIM(gu_logon_computer), gu_last_action = RTRIM(gu_last_action), gu_full_name = RTRIM(gu_full_name)
GO







