/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_100]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 26;

SET @New_ReleaseId = 27;
SET @New_ScriptName = N'UpdateTo_18.027.sql';
SET @New_Description = N'GP UpgradeDowngradeAction';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/******* RECORDS *******/

IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE gp_group_key ='PlayerTracking' AND gp_subject_key = 'Level01.UpgradeDowngradeAction')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE, GP_MS_DOWNLOAD_TYPE) VALUES ('PlayerTracking', 'Level01.UpgradeDowngradeAction', 0, 1)

IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE gp_group_key ='PlayerTracking' AND gp_subject_key = 'Level02.UpgradeDowngradeAction')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE, GP_MS_DOWNLOAD_TYPE) VALUES ('PlayerTracking', 'Level02.UpgradeDowngradeAction', 0, 1)

IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE gp_group_key ='PlayerTracking' AND gp_subject_key = 'Level03.UpgradeDowngradeAction')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE, GP_MS_DOWNLOAD_TYPE) VALUES ('PlayerTracking', 'Level03.UpgradeDowngradeAction', 0, 1)

IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE gp_group_key ='PlayerTracking' AND gp_subject_key = 'Level04.UpgradeDowngradeAction')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE, GP_MS_DOWNLOAD_TYPE) VALUES ('PlayerTracking', 'Level04.UpgradeDowngradeAction', 0, 1)
 