/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_100]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 67;

SET @New_ReleaseId = 68;
SET @New_ScriptName = N'UpdateTo_18.068.sql';
SET @New_Description = N'Added regional data.';
/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/

/****** INDEXES ******/

/****** RECORDS ******/

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Account.Fields' AND GP_SUBJECT_KEY = 'State.Name')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE,GP_MS_DOWNLOAD_TYPE) VALUES ('Account.Fields','State.Name','', 1)
GO

-- PANAM� PROVINCES
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Bocas del Toro','PA')
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Cocl�','PA')
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Col�n','PA')
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Chiriqu�','PA')
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Dari�n','PA')
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Herrera','PA')
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Los Santos','PA')
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Panam�','PA')
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Panam� Oeste','PA')
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Veraguas','PA')
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Guna Yala','PA')
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Ember�-Wounaan','PA')
INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Ng�be-Bugl�','PA')

-- PANAM� OCCUPATIONS
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('NO DISPONIBLE','0000001',1,1000,'PA')

-- PANAM� IDENTIFICATION_TYPES
INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (30,1,100,'Otro','PA')
INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (31,1,1,'Pasaporte','PA')
INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (32,1,2,'C�dula de identidad','PA')
INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (33,1,3,'Licencia de conducir','PA')

GO

/******* STORED PROCEDURES *******/
