/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_100]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 90;

SET @New_ReleaseId = 91;
SET @New_ScriptName = N'UpdateTo_18.091.sql';
SET @New_Description = N'Update MultiSiteTrigger_CenterAccountDocument';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/

/****** INDEXES ******/

/****** RECORDS ******/
 
/****** STORED *******/

--------------------------------------------------------------------------------
-- Copyright � 2013 Win Systems International 
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: MultiSiteTrigger_CenterAccountDocument
-- 
--   DESCRIPTION: Trigger MultiSiteTrigger_CenterAccountDocument for increasing the sequence
-- 
--        AUTHOR: Jos� Mart�nez L�pez
-- 
-- CREATION DATE: 02-JUL-2013
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 02-JUL-2013 JML    First release.
-- 22-MAR-2016 JML    Bug 10880:MS-Meier: Not upload account_documents
--------------------------------------------------------------------------------  

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[MultiSiteTrigger_CenterAccountDocument]') AND type in (N'TR'))
DROP TRIGGER [dbo].[MultiSiteTrigger_CenterAccountDocument]
GO

CREATE  TRIGGER [dbo].[MultiSiteTrigger_CenterAccountDocument] ON [dbo].[ACCOUNT_DOCUMENTS]
AFTER INSERT, UPDATE
NOT FOR REPLICATION
AS 
BEGIN
    DECLARE @Sequence15Value AS BIGINT
    DECLARE @AccountId       AS BIGINT

    DECLARE InsertedCursor CURSOR FOR 
     SELECT   INSERTED.AD_ACCOUNT_ID
       FROM   INSERTED 

    SET NOCOUNT ON;

    OPEN InsertedCursor

    FETCH NEXT FROM InsertedCursor INTO @AccountId
    
    WHILE @@FETCH_STATUS = 0
    BEGIN
    
        -- Account Documents
        UPDATE   SEQUENCES 
           SET   SEQ_NEXT_VALUE = SEQ_NEXT_VALUE + 1
         WHERE   SEQ_ID         = 15

        SELECT   @Sequence15Value = SEQ_NEXT_VALUE - 1 FROM SEQUENCES WHERE SEQ_ID = 15

        UPDATE   ACCOUNT_DOCUMENTS
           SET   AD_MS_SEQUENCE_ID = @Sequence15Value
         WHERE   AD_ACCOUNT_ID     = @AccountId

        FETCH NEXT FROM InsertedCursor INTO @AccountId
    END

    CLOSE InsertedCursor
    DEALLOCATE InsertedCursor

END   -- MultiSiteTrigger_CenterAccountDocument
GO




/****** Object:  UserDefinedFunction [dbo].[ConcatOpeningTime]    Script Date: 03/17/2016 18:57:02 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ConcatOpeningTime]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
  DROP FUNCTION [dbo].[ConcatOpeningTime]
GO

CREATE FUNCTION [dbo].[ConcatOpeningTime]
  (@SiteId int,
   @Date   datetime)
RETURNS datetime
AS
BEGIN
  DECLARE @closing_hour    int
  DECLARE @closing_minutes int
  DECLARE @today_opening  datetime

  SET @closing_hour = ISNULL((SELECT   gp_key_value
                                FROM   general_params
                               WHERE   gp_group_key   = 'WigosGUI'
                                 AND   gp_subject_key = 'ClosingTime'), 6)

  SET @closing_minutes = ISNULL((SELECT   gp_key_value
                                   FROM   general_params
                                  WHERE   gp_group_key   = 'WigosGUI'
                                    AND   gp_subject_key = 'ClosingTimeMinutes'), 0)

  -- Trunc date to start of the day (00:00h).
  SET @today_opening = DATEADD(DAY, DATEDIFF(day, 0, @Date), 0)
  
  SET @today_opening = DATEADD(HOUR, @closing_hour, @today_opening)
  SET @today_opening = DATEADD(MINUTE, @closing_minutes, @today_opening)

  RETURN @today_opening
END -- ConcatOpeningTime


GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Opening]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
  DROP FUNCTION [dbo].[Opening]
GO

CREATE FUNCTION [dbo].[Opening]
  (@SiteId int,
   @Date   datetime)
RETURNS datetime
AS
BEGIN
  DECLARE @now           datetime
  DECLARE @today_opening datetime

  SET @now = @Date
  SET @today_opening = dbo.ConcatOpeningTime(@SiteId, @now)

  IF @today_opening > @now
  BEGIN
    SET @today_opening = DATEADD(DAY, -1, @today_opening)
  END

  RETURN @today_opening
END -- Opening


GO


IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.account_points_cache') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[account_points_cache](
      apc_account_id bigint NOT NULL,
      apc_days int NOT NULL,
      apc_history_points_generated_for_level money NOT NULL,
      apc_history_points_discretional_for_level money NOT NULL,
      apc_history_points_discretional_only_for_redeem money NOT NULL,
      apc_history_points_promotion_only_for_redeem money NOT NULL,
      apc_today datetime NULL,
      apc_today_points_generated_for_level money NULL,
      apc_today_points_discretional_for_level money NULL,
      apc_today_points_discretional_only_for_redeem money NULL,
      apc_today_points_promotion_only_for_redeem money NULL,
      apc_today_last_movement_id bigint NULL,
      apc_today_last_updated datetime NULL,
  CONSTRAINT [PK_account_points_cache] PRIMARY KEY CLUSTERED 
(
      [apc_account_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO


--------------------------------------------------------------------------------
-- PURPOSE: Calculate AccountPoints for level
-- 
--  PARAMS:
--      - INPUT:
--           @AccountId                        BIGINT,         
--           @DateFrom                         DATETIME,
--           @DateTo                           DATETIME,
--           @MovementId                       BIGINT, 
--
--      - OUTPUT:
--           @LastMovementId                   BIGINT OUTPUT, 
--           @PointsGeneratedForLevel          MONEY OUTPUT,
--           @PointsDiscretionalForLevel       MONEY OUTPUT,
--           @PointsDiscretionalOnlyForRedeem  MONEY OUTPUT,
--           @PointsPromotionOnlyForRedeem     MONEY OUTPUT
--
-- RETURNS:
--
--   NOTES:
--------------------------------------------------------------------------------


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AccountPointsCache_CalculatePoints]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[AccountPointsCache_CalculatePoints]
GO

CREATE PROCEDURE [dbo].[AccountPointsCache_CalculatePoints]
       @AccountId                        BIGINT,
       @DateFrom                         DATETIME,
       @DateTo                           DATETIME,
       @MovementId                       BIGINT, 
       @LastMovementId                   BIGINT OUTPUT, 
       @PointsGeneratedForLevel          MONEY  OUTPUT,
       @PointsDiscretionalForLevel       MONEY  OUTPUT,
       @PointsDiscretionalOnlyForRedeem  MONEY  OUTPUT,
       @PointsPromotionOnlyForRedeem     MONEY  OUTPUT
AS
BEGIN
  DECLARE @points_awarded                        AS INT;
  DECLARE @manually_added_points_for_level       AS INT;
  DECLARE @imported_points_for_level             AS INT;
  DECLARE @imported_points_history               AS INT;
  DECLARE @manually_added_points_only_for_redeem AS INT;
  DECLARE @imported_points_only_for_redeem       AS INT;
  DECLARE @promotion_point                       AS INT;
  DECLARE @cancel_promotion_point                AS INT;
  -- 11-FEB-2016 JRC    PBI 7909: Multiple Buckets. 
  DECLARE @EndSession_RankingLevelPoints		AS INT;	
  DECLARE @Expired_RankingLevelPoints		    AS INT
  DECLARE @Manual_Add_RankingLevelPoints		AS INT
  DECLARE @Manual_Sub_RankingLevelPoints		AS INT
  DECLARE @Manual_Set_RankingLevelPoints		AS INT
  DECLARE @EndSession_RedemptionPoints			AS INT
  DECLARE @Expired_RedemptionPoints				AS INT
  DECLARE @Manual_Add_RedemptionPoints			AS INT
  DECLARE @Manual_Sub_RedemptionPoints			AS INT
  DECLARE @Manual_Set_RedemptionPoints			AS INT
 


  DECLARE @Sql             AS NVARCHAR(MAX);
  DECLARE @ParamDefinition AS NVARCHAR(MAX);
  DECLARE @Index           AS NVARCHAR(MAX);
  DECLARE @Where           AS NVARCHAR(MAX); 
  
  -- Points for level
     -- Generated
  SET @points_awarded                        = 36
  --PointsAwarded

     -- Discretional

  SET @manually_added_points_for_level       = 68
  --ManuallyAddedPointsForLevel
  
  SET @imported_points_for_level             = 72
  --ImportedPointsForLevel

  SET @imported_points_history               = 73
  --ImportedPointsHistory

  -- Points only for redeem
     -- Discretional 
  SET @manually_added_points_only_for_redeem = 50
  --ManuallyAddedPointsOnlyForRedeem

  SET @imported_points_only_for_redeem       = 71
  --ImportedPointsOnlyForRedeem

     -- Promotion 
  SET @promotion_point                       = 60
  --PromotionPoint
  
  SET @cancel_promotion_point                = 61
  --CancelPromotionPoint 

  SET @EndSession_RankingLevelPoints		     = 1102
  --'MULTIPLE_BUCKETS_END_SESSION + RankingLevelPoints PuntosNivel'
  SET @Expired_RankingLevelPoints		         = 1202
  --'MULTIPLE_BUCKETS_Expired + RankingLevelPoints PuntosNivel'
  SET @Manual_Add_RankingLevelPoints		     = 1302
  --'MULTIPLE_BUCKETS_Manual_Add + RankingLevelPoints PuntosNivel'
  SET @Manual_Sub_RankingLevelPoints		     = 1402
  --'MULTIPLE_BUCKETS_Manual_Sub + RankingLevelPoints PuntosNivel'
  SET @Manual_Set_RankingLevelPoints		     = 1502
  --'MULTIPLE_BUCKETS_Manual_Set + RankingLevelPoints PuntosNivel'


  SET @EndSession_RedemptionPoints		     = 1101
  --'MULTIPLE_BUCKETS_END_SESSION + RedemptionPoints PuntosCanje'
  SET @Expired_RedemptionPoints		         = 1201
  --'MULTIPLE_BUCKETS_Expired + RedemptionPoints PuntosCanje'
  SET @Manual_Add_RedemptionPoints		     = 1301
  --'MULTIPLE_BUCKETS_Manual_Add + RedemptionPoints PuntosCanje'
  SET @Manual_Sub_RedemptionPoints		     = 1401
  --'MULTIPLE_BUCKETS_Manual_Sub + RedemptionPoints PuntosCanje'
  SET @Manual_Set_RedemptionPoints		     = 1501
  --'MULTIPLE_BUCKETS_Manual_Set + RedemptionPoints PuntosCanje'
  




  SET @LastMovementId = @MovementId

    SET @Index = 'IX_am_account_id_type_datetime'
    SET @Where = ' WHERE AM_ACCOUNT_ID  = @AccountId ' +
                 '   AND AM_TYPE IN (@points_awarded, @manually_added_points_for_level, @imported_points_for_level, @imported_points_history, ' +
				 '                   @EndSession_RankingLevelPoints, @Expired_RankingLevelPoints, @Manual_Add_RankingLevelPoints, @Manual_Sub_RankingLevelPoints, @Manual_Set_RankingLevelPoints,' +
				 '                   @EndSession_RedemptionPoints, @Expired_RedemptionPoints, @Manual_Add_RedemptionPoints, @Manual_Sub_RedemptionPoints, @Manual_Set_RedemptionPoints,' +
                 '                   @manually_added_points_only_for_redeem, @imported_points_only_for_redeem, @promotion_point, @cancel_promotion_point) ' + 
                 '   AND AM_DATETIME >= CAST(''' + CAST(@DateFrom AS VARCHAR(50)) + ''' AS DATETIME) ' +
                 '   AND AM_DATETIME  < CAST(''' + CAST(@DateTo AS VARCHAR(50)) + ''' AS DATETIME) ' +
                 '   AND AM_MOVEMENT_ID > ' + CAST(@MovementId AS NVARCHAR(MAX)) 

  SET @Sql = '
  SELECT   @PointsGeneratedForLevel_out          = ISNULL(SUM (CASE WHEN AM_TYPE IN (@points_awarded,@EndSession_RankingLevelPoints, @Expired_RankingLevelPoints)
                                                               THEN AM_ADD_AMOUNT-AM_SUB_AMOUNT 
                                                               ELSE 0 
                                                               END ), 0) 
         , @PointsDiscretionalForLevel_out       = ISNULL(SUM (CASE WHEN AM_TYPE IN (@manually_added_points_for_level, @imported_points_for_level, @imported_points_history, @Manual_Add_RankingLevelPoints, @Manual_Sub_RankingLevelPoints, @Manual_Set_RankingLevelPoints)
                                                               THEN AM_ADD_AMOUNT-AM_SUB_AMOUNT 
                                                               ELSE 0 
                                                               END ), 0) 
         , @PointsDiscretionalOnlyForRedeem_out  = ISNULL(SUM (CASE WHEN AM_TYPE IN (@manually_added_points_only_for_redeem, @imported_points_only_for_redeem, @Manual_Add_RedemptionPoints, @Manual_Sub_RedemptionPoints, @Manual_Set_RedemptionPoints)
                                                               THEN AM_ADD_AMOUNT-AM_SUB_AMOUNT 
                                                               ELSE 0 
                                                               END ), 0)
         , @PointsPromotionOnlyForRedeem_out     = ISNULL(SUM (CASE WHEN AM_TYPE IN (@promotion_point, @cancel_promotion_point,@EndSession_RedemptionPoints, @Expired_RedemptionPoints)
                                                               THEN AM_ADD_AMOUNT-AM_SUB_AMOUNT
                                                               ELSE 0 
                                                               END ), 0) 
         , @LastMovementId_out                   = ISNULL(MAX (AM_MOVEMENT_ID), @LastMovementId_out)
    FROM   ACCOUNT_MOVEMENTS WITH (INDEX (' + @Index + ')) ' 
  + @Where
 
SET @ParamDefinition = N'@AccountId                             BIGINT, 
                        @points_awarded                         INT, 
                        @manually_added_points_for_level        INT, 
                        @imported_points_for_level              INT, 
                        @imported_points_history                INT, 
                        @manually_added_points_only_for_redeem  INT, 
                        @imported_points_only_for_redeem        INT, 
                        @promotion_point                        INT, 
                        @cancel_promotion_point                 INT, 
						@EndSession_RankingLevelPoints			INT,
						@Expired_RankingLevelPoints				INT,	
						@Manual_Add_RankingLevelPoints			INT,
						@Manual_Sub_RankingLevelPoints			INT,
						@Manual_Set_RankingLevelPoints			INT,
						@EndSession_RedemptionPoints			INT,	
						@Expired_RedemptionPoints				INT,	
						@Manual_Add_RedemptionPoints			INT,	
						@Manual_Sub_RedemptionPoints			INT,	
						@Manual_Set_RedemptionPoints			INT,	
                        @LastMovementId_out                     BIGINT OUTPUT, 
                        @PointsGeneratedForLevel_out            MONEY  OUTPUT, 
                        @PointsDiscretionalForLevel_out         MONEY  OUTPUT, 
                        @PointsDiscretionalOnlyForRedeem_out    MONEY  OUTPUT, 
                        @PointsPromotionOnlyForRedeem_out       MONEY  OUTPUT' 

EXEC sp_executesql @Sql, 
                   @ParamDefinition,
                   @AccountId                             = @AccountId, 
                   @points_awarded                        = @points_awarded,                        
                   @manually_added_points_for_level       = @manually_added_points_for_level,
                   @imported_points_for_level             = @imported_points_for_level,
                   @imported_points_history               = @imported_points_history, 
                   @manually_added_points_only_for_redeem = @manually_added_points_only_for_redeem, 
                   @imported_points_only_for_redeem       = @imported_points_only_for_redeem, 
                   @promotion_point                       = @promotion_point,
                   @cancel_promotion_point                = @cancel_promotion_point,
					@EndSession_RankingLevelPoints    =   @EndSession_RankingLevelPoints, 
					@Expired_RankingLevelPoints		  =   @Expired_RankingLevelPoints		,
					@Manual_Add_RankingLevelPoints    =   @Manual_Add_RankingLevelPoints ,
					@Manual_Sub_RankingLevelPoints    =   @Manual_Sub_RankingLevelPoints ,
					@Manual_Set_RankingLevelPoints    =   @Manual_Set_RankingLevelPoints ,
					@EndSession_RedemptionPoints	  =   @EndSession_RedemptionPoints	,
					@Expired_RedemptionPoints		  =   @Expired_RedemptionPoints		,
					@Manual_Add_RedemptionPoints	  =   @Manual_Add_RedemptionPoints	,
					@Manual_Sub_RedemptionPoints	  =   @Manual_Sub_RedemptionPoints	,
					@Manual_Set_RedemptionPoints	  =   @Manual_Set_RedemptionPoints	,
                   @LastMovementId_out                    = @LastMovementId                   OUTPUT, 
                   @PointsGeneratedForLevel_out           = @PointsGeneratedForLevel          OUTPUT,
                   @PointsDiscretionalForLevel_out        = @PointsDiscretionalForLevel       OUTPUT,
                   @PointsDiscretionalOnlyForRedeem_out   = @PointsDiscretionalOnlyForRedeem  OUTPUT,
                   @PointsPromotionOnlyForRedeem_out      = @PointsPromotionOnlyForRedeem     OUTPUT 

---------------------------------------------------------------------------------------------------------------------------------
-- Note:
-- @LastMovementId_out : Is the variable�s name of the output parameter of '@Sql execute'
-- When run 'EXEC sp_executesql' is finished, the value of @LastMovementId_out is assigned to @LastMovementId
---------------------------------------------------------------------------------------------------------------------------------

END  -- PROCEDURE [dbo].[AccountPointsCache_CalculatePoints]
GO

--------------------------------------------------------------------------------
-- PURPOSE: Save AccountPoints for level in DB (for simulate a cache)
-- 
--  PARAMS:
--      - INPUT:
--           @AccountId       BIGINT       
--
--      - OUTPUT:
--
-- RETURNS:
--
--   NOTES:
--------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AccountPointsCache_CalculateToday]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[AccountPointsCache_CalculateToday]
GO

CREATE PROCEDURE [dbo].[AccountPointsCache_CalculateToday]
       @AccountId     BIGINT
AS
BEGIN

  DECLARE @update_history                                  AS BIT;
  
  DECLARE @apc_today                                       AS DATETIME;
  DECLARE @apc_days                                        AS INT;
	DECLARE @apc_history_points_generated_for_level          AS MONEY;
	DECLARE @apc_history_points_discretional_for_level       AS MONEY;
	DECLARE @apc_history_points_discretional_only_for_redeem AS MONEY;
	DECLARE @apc_history_points_promotion_only_for_redeem    AS MONEY;
	DECLARE @apc_today_points_generated_for_level            AS MONEY;
	DECLARE @apc_today_points_discretional_for_level         AS MONEY;
	DECLARE @apc_today_points_discretional_only_for_redeem   AS MONEY;
	DECLARE @apc_today_points_promotion_only_for_redeem      AS MONEY;
  DECLARE @apc_today_last_movement_id                      AS BIGINT;
  
  DECLARE @estudy_period                                   AS INT;
  DECLARE @date_from                                       AS DATETIME;
  DECLARE @now                                             AS DATETIME;
  DECLARE @today_opening                                   AS DATETIME;
  
  SET @update_history = 0;
  
  -- Get register from ACCOUNT_POINTS_CACHE
  SELECT   @apc_today                  = APC_TODAY 
         , @apc_days                   = APC_DAYS
         , @apc_today_last_movement_id = APC_TODAY_LAST_MOVEMENT_ID
    FROM   ACCOUNT_POINTS_CACHE 
   WHERE   APC_ACCOUNT_ID = @AccountId 
     
  -- GET General param: "PlayerTracking", "Levels.DaysCountingPoints"
  SET @estudy_period = (SELECT CAST(GP_KEY_VALUE AS INT) FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PlayerTracking' AND GP_SUBJECT_KEY = 'Levels.DaysCountingPoints')
     
  --_day_start_counting_points = Misc.TodayOpening().AddDays(-(_days_counting_points - 1));
  SET @now = GETDATE();
  SET @today_opening = dbo.Opening(0, @now)
  SET @date_from = DATEADD(DAY, -1*(@estudy_period-1), @today_opening);
    
  IF (   @today_opening <> ISNULL(@apc_today, DATEADD(d, -1, GETDATE()))   --> no hay registro o cambio de jornada.
      OR @estudy_period <> ISNULL(@apc_days, -1) )                         --> no hay registro o cambio en el periodo de estudio.
  BEGIN
    SET @update_history = 1;
  END   
    
  IF (@update_history = 1)
  BEGIN
    SET @apc_today_last_movement_id = 0;
    
    -- Get history data 
    EXEC AccountPointsCache_CalculatePoints @AccountId, @date_from, @today_opening, 
                                            @apc_today_last_movement_id, 
                                            @apc_today_last_movement_id                      OUTPUT, 
                                            @apc_history_points_generated_for_level          OUTPUT, 
                                            @apc_history_points_discretional_for_level       OUTPUT, 
                                            @apc_history_points_discretional_only_for_redeem OUTPUT, 
                                            @apc_history_points_promotion_only_for_redeem    OUTPUT
    --> Get today data
    EXEC AccountPointsCache_CalculatePoints @AccountId, @today_opening, @now, 
                                            @apc_today_last_movement_id, 
                                            @apc_today_last_movement_id                    OUTPUT, 
                                            @apc_today_points_generated_for_level          OUTPUT, 
                                            @apc_today_points_discretional_for_level       OUTPUT, 
                                            @apc_today_points_discretional_only_for_redeem OUTPUT, 
                                            @apc_today_points_promotion_only_for_redeem    OUTPUT
  
    DELETE  FROM ACCOUNT_POINTS_CACHE 
     WHERE  APC_ACCOUNT_ID = @AccountId 
   
    INSERT INTO ACCOUNT_POINTS_CACHE ( APC_ACCOUNT_ID, 
                                       APC_DAYS,       
                                       APC_HISTORY_POINTS_GENERATED_FOR_LEVEL,          
                                       APC_HISTORY_POINTS_DISCRETIONAL_FOR_LEVEL,
                                       APC_HISTORY_POINTS_DISCRETIONAL_ONLY_FOR_REDEEM,
                                       APC_HISTORY_POINTS_PROMOTION_ONLY_FOR_REDEEM,
                                       APC_TODAY, 
                                       APC_TODAY_POINTS_GENERATED_FOR_LEVEL,
                                       APC_TODAY_POINTS_DISCRETIONAL_FOR_LEVEL, 
                                       APC_TODAY_POINTS_DISCRETIONAL_ONLY_FOR_REDEEM, 
                                       APC_TODAY_POINTS_PROMOTION_ONLY_FOR_REDEEM,
                                       APC_TODAY_LAST_MOVEMENT_ID, 
                                       APC_TODAY_LAST_UPDATED ) 
                              VALUES ( @AccountId,     
                                       @estudy_period, 
                                       @apc_history_points_generated_for_level,              
                                       @apc_history_points_discretional_for_level, 
                                       @apc_history_points_discretional_only_for_redeem,     
                                       @apc_history_points_promotion_only_for_redeem,
                                       @today_opening, 
                                       @apc_today_points_generated_for_level, 
                                       @apc_today_points_discretional_for_level, 
                                       @apc_today_points_discretional_only_for_redeem, 
                                       @apc_today_points_promotion_only_for_redeem, 
                                       @apc_today_last_movement_id, 
                                       @now )  
  END
  ELSE
  BEGIN
    --> Get today data
    EXEC AccountPointsCache_CalculatePoints @AccountId, @today_opening, @now, 
                                            @apc_today_last_movement_id, 
                                            @apc_today_last_movement_id                    OUTPUT, 
                                            @apc_today_points_generated_for_level          OUTPUT, 
                                            @apc_today_points_discretional_for_level       OUTPUT, 
                                            @apc_today_points_discretional_only_for_redeem OUTPUT, 
                                            @apc_today_points_promotion_only_for_redeem    OUTPUT
    
    --update today
    UPDATE   ACCOUNT_POINTS_CACHE  
       SET   APC_TODAY                                     = @today_opening
           , APC_TODAY_POINTS_GENERATED_FOR_LEVEL          = APC_TODAY_POINTS_GENERATED_FOR_LEVEL          + @apc_today_points_generated_for_level         
           , APC_TODAY_POINTS_DISCRETIONAL_FOR_LEVEL       = APC_TODAY_POINTS_DISCRETIONAL_FOR_LEVEL       + @apc_today_points_discretional_for_level      
           , APC_TODAY_POINTS_DISCRETIONAL_ONLY_FOR_REDEEM = APC_TODAY_POINTS_DISCRETIONAL_ONLY_FOR_REDEEM + @apc_today_points_discretional_only_for_redeem
           , APC_TODAY_POINTS_PROMOTION_ONLY_FOR_REDEEM    = APC_TODAY_POINTS_PROMOTION_ONLY_FOR_REDEEM    + @apc_today_points_promotion_only_for_redeem   
           , APC_TODAY_LAST_MOVEMENT_ID                    = @apc_today_last_movement_id 
           , APC_TODAY_LAST_UPDATED                        = @now
     WHERE   APC_ACCOUNT_ID = @AccountId
     
  END
    
END  -- PROCEDURE [dbo].[AccountPointsCache_CalculateToday]
GO  

--------------------------------------------------------------------------------
-- PURPOSE: Return Accounts & AccountPointsCache data for account
-- 
--  PARAMS:
--      - INPUT:
--           @AccountId       BIGINT 
--
--      - OUTPUT:
--           AC_HOLDER_NAME 
--           AC_HOLDER_GENDER 
--           AC_HOLDER_BIRTH_DATE 
--           AC_BALANCE 
--           AC_POINTS 
--           AC_CURRENT_HOLDER_LEVEL 
--           AC_HOLDER_LEVEL_EXPIRATION 
--           AC_HOLDER_LEVEL_ENTERED 
--           AC_RE_BALANCE 
--           AC_PROMO_RE_BALANCE 
--           AC_PROMO_NR_BALANCE 
--           AC_CURRENT_PLAY_SESSION_ID 
--           AC_CURRENT_TERMINAL_NAME 
--           AM_POINTS_GENERATED 
--           AM_POINTS_DISCRETIONARIES 
--           AM_POINTS_PROMO_DISCRETIONARIES 
--           AM_POINTS_PROMO_ONLY_FOR_REDEEM 
--           APC_TODAY_LAST_MOVEMENT_ID 
-- RETURNS:
--
--   NOTES:
--------------------------------------------------------------------------------


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetAccountPointsCache]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[GetAccountPointsCache]
GO

CREATE PROCEDURE [dbo].[GetAccountPointsCache]
       @pAccountId    bigint
AS
BEGIN
  
  EXEC AccountPointsCache_CalculateToday @pAccountId

  DECLARE @BucketPuntosCanje Int
  SET @BucketPuntosCanje = 1
  
  --Realizar select ACCOUNT_POINTS_CACHE con inner join a ACCOUNTS. -- select similar al que se hace en WKT_Player.cs funci�n Read(..)
  SELECT   ISNULL(AC_HOLDER_NAME, '')             AS AC_HOLDER_NAME 
         , ISNULL(AC_HOLDER_GENDER, 0)            AS AC_HOLDER_GENDER 
         , AC_HOLDER_BIRTH_DATE 
         , AC_BALANCE 
         , ISNULL (DBO.GETBUCKETVALUE(@BucketPuntosCanje, @pAccountId), 0)  AS AC_POINTS
         , AC_HOLDER_LEVEL                        AS AC_CURRENT_HOLDER_LEVEL
         , AC_HOLDER_LEVEL_EXPIRATION 
         , AC_HOLDER_LEVEL_ENTERED 
         , AC_RE_BALANCE 
         , AC_PROMO_RE_BALANCE 
         , AC_PROMO_NR_BALANCE 
         , ISNULL (AC_CURRENT_PLAY_SESSION_ID, 0) AS AC_CURRENT_PLAY_SESSION_ID 
         , AC_CURRENT_TERMINAL_NAME 
         , ISNULL(APC_HISTORY_POINTS_GENERATED_FOR_LEVEL         , 0) + ISNULL(APC_TODAY_POINTS_GENERATED_FOR_LEVEL         , 0)  AS AM_POINTS_GENERATED 
         , ISNULL(APC_HISTORY_POINTS_DISCRETIONAL_FOR_LEVEL      , 0) + ISNULL(APC_TODAY_POINTS_DISCRETIONAL_FOR_LEVEL      , 0)  AS AM_POINTS_DISCRETIONARIES
         , ISNULL(APC_HISTORY_POINTS_DISCRETIONAL_ONLY_FOR_REDEEM, 0) + ISNULL(APC_TODAY_POINTS_DISCRETIONAL_ONLY_FOR_REDEEM, 0)  AS AM_POINTS_PROMO_DISCRETIONARIES 
         , ISNULL(APC_HISTORY_POINTS_PROMOTION_ONLY_FOR_REDEEM   , 0) + ISNULL(APC_TODAY_POINTS_PROMOTION_ONLY_FOR_REDEEM   , 0)  AS AM_POINTS_PROMO_ONLY_FOR_REDEEM
         , APC_TODAY_LAST_MOVEMENT_ID 
    FROM   ACCOUNTS 
   INNER   JOIN ACCOUNT_POINTS_CACHE ON APC_ACCOUNT_ID = AC_ACCOUNT_ID
   WHERE   AC_ACCOUNT_ID = @pAccountId; 
  
END  -- PROCEDURE [dbo].[GetAccountPointsCache]
GO

GRANT EXECUTE ON [dbo].[GetAccountPointsCache] TO [wggui] WITH GRANT OPTION
GO