/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_100]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 125;

SET @New_ReleaseId = 126;
SET @New_ScriptName = N'UpdateTo_18.126.sql';
SET @New_Description = N'New release v03.007.0040'; 

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/**** GENERAL PARAM *****/


/******* TABLES  *******/



/******* RECORDS *******/
IF (NOT EXISTS (SELECT * FROM occupations WHERE oc_country_iso_code2 = 'CL' AND oc_description = 'Abogado/a'))
BEGIN
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Abogado/a', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Actor', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Actriz', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Administrador/ra de Empresas', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Administrador/ra en Gastron�mica Internacional', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Administrador/ra en Hoteler�a y Servicios', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Administrador/ra en Turismo', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Administrativo/a', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Agricultor/a', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Agronomo/a', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Alba�il', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Analista De Sistema', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Analista Programador', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Animador/a', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Arquitecto/a', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Artesano/a', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Asesora de Hogar', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Asistente De Educaci�n', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Asistente Social', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Bailarin/a', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Biologo/a', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Biologo/a Marino', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Botones', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Cajero/a', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Camarografo', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Carabinero', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Carpintero', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Cesante', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Chef', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Chofer / Conductor', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Climatizaci�n', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Cocinero/a', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Colectivero', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Comerciante', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Conserje ', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Constructor', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Constructor/ra Civil', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Contador/a', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Contador/a Auditor', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Contador/a General', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Contador/a P�blico', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Contratista', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Contructor Civil', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Corredor de propiedades', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Corredor de seguro', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Cosmetologo/a', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Costurero/a', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Dibujante', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Director/a Colegio', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Dise�ador/a de Moda', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Dise�ador/a Gr�fico', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Dise�ador/a Web', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Dise�adora de vestuario', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Docente Universitario', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Due�o/a de Casa', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Economista', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Educadora Diferencial', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Ejecutivo/a de Ventas', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Electrico', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Electrico/a Industrial', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Electromec�nico/a', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Electr�nico/a Industrial', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Empleado Judicial', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Empresario/a', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Enfermero/a', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Estelista', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Estudiante', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Farmaceutico', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('FFAA', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Florista', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Fonoaudiologo/a', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Fotografo', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Funcionario/a Publica', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Futbolista', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Garzon/a', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Gasfiter', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Geologo/a', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Gerente de Asuntos Corporativos', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Gerente de Asuntos Publicos', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Gerente de Finanzas', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Gerente de Informatica', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Gerente de Marketing', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Gerente de Operaciones', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Gerente de Personas', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Gerente de RRHH', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Gerente General', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Guardia', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Independiente', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Productor Grafico', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Productor de Eventos', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Ingeniero/a', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Ingeniero/a  Agr�cola', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Ingeniero/a  Comercial', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Ingeniero/a  Electr�nica', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Ingeniero/a  en Administraci�n de Empresas ', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Ingeniero/a  en Automatizaci�n y Control Industrial', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Ingeniero/a  en Ciberseguridad', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Ingeniero/a  en Climatizaci�n', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Ingeniero/a  en Comercio Exterior', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Ingeniero/a  en Construcci�n', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Ingeniero/a  en Electricidad', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Ingeniero/a  en Geomensura', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Ingeniero/a  en Inform�tica', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Ingeniero/a  en Maquinaria', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Ingeniero/a  en Metalurgia', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Ingeniero/a  en Minas', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Ingeniero/a  en Prevenci�n de Riesgos', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Ingeniero/a  en Producci�n Ganadera', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Ingeniero/a  en Qu�mica Industrial', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Ingeniero/a  en Refrigeraci�n', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Ingeniero/a  en Sonido', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Ingeniero/a  en Telecomunicaciones, Conectividad y Redes', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Ingeniero/a  Industrial', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Ingeniero/a  Mec�nica en Mantenimiento Industrial', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Ingeniero/a  Mec�nica en Producci�n Industrial', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Ingeniero/a Agricola', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Ingeniero/a Civil', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Ingeniero/a Civil Electrico', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Ingeniero/a Comercial', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Ingeniero/a Electrico', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Ingeniero/a en Ejecuci�n ', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Ingeniero/a en Minas', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Ingeniero/a en Prevenci�n de Riesgos', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Ingeniero/a Industrial', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Ingeniero/a Mecanico', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Ingeniero/a Metalurgia', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Inspector/a', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Instructor Deportivo', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Instructor Zumba', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Instrumentista', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Intructor Maquinas', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Jardinero/a', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Jefe administrativo', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Jubilado/a', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Kinesiologo/a', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Laboratorista Cl�nico, Banco de Sangre e Imagenolog�a', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Maestro Minero', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Manicurista', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Mantenedor Equipos Industriales', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Mantenedor/a Industrial', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Mantenedor/a Mec�nico de Plantas Mineras', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Marino', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Marketing Publicidad', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Masajista', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Matron/a', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Mecanico', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Mec�nico Automotriz en Maquinaria Pesada', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Mec�nico Automotriz en Sistemas Electr�nicos', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Mec�nico en Producci�n Industrial', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Medico/a', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Medico/a Veterinario', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Militar', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Minero', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Mueblista', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Nutricionista', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Odontologo/a', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Operador Maquinaria', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Operador Planta', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Paramedico', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Particular', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Parvulario/a', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Pastelero/a', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Pedicurista', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Peluquera', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Periodista', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Piloto', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Pintor', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Profesor/ra B�sica o Media', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Profesor/ra Universitario', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Psicologo/a', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Psicopedagogo/a', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Quimico farmac�utico', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Quiropractivo', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Recepcionista', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Relacionador/a P�blico', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Secretario/a', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Servicio Publico', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Sicologo/a', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Soldador', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Sonidista', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Steward', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Supervisora', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Taxista', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Tecnico Agricola', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('T�cnico en Enfermer�a', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('T�cnico en Farmacia', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('T�cnico en Metalurgia Extractiva', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Tecnico en Minas', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('T�cnico en Miner�a', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('T�cnico en Odontolog�a', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Tecnico en plantas', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Tecnico en Telecomunicaciones', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Tecnico Enfermeria', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Tecnico Financiero', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Tecnico Mecanico', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Tecnico Universitario', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Tecnolog�a Agr�cola', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Tecnolog�a en An�lisis Qu�mico', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Tecnolog�a en Producci�n Ganadera', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Tecnolog�a en Sonido', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Tecnologo Medico', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Tecnologo/a Medico Imagenologia', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Tecnologo/a Medico Laboratorio', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Tecnologo/a Medico Oftalmologia', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Telefonista', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Terapeuta', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Terapeuta Ocupacional', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Topografo/a', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Trabajador Sector Privado', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Trabajador Sector Publico', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Trabajador/a Social', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Transportista', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Turismo', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Vendedor/a', '0000', 1, 1000, 'CL')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('Vigilante', '0000', 1, 1000, 'CL')

END
GO


/******* PROCEDURES *******/



/******* TRIGGERS *******/
