/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_100]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 17;

SET @New_ReleaseId = 18;
SET @New_ScriptName = N'UpdateTo_18.018.sql';
SET @New_Description = N'Update SP Insert_Elp01PlaySessions';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/

/****** VIEWS ******/

/****** CONSTRAINTS ******/

/****** FUNCTIONS ******/

/****** INDEXES ******/

/****** STORED PROCEDURES ******/
GO
--MULTISITE
--------------------------------------------------------------------------------
-- Copyright � 2013 Win Systems International 
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: Insert_Elp01PlaySessions
-- 
--   DESCRIPTION: Insert ELP01 Play Sessions
-- 
--        AUTHOR: Alberto Marcos
-- 
-- CREATION DATE: 03-JUN-2013
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 03-JUN-2013 AMF    First release.
-------------------------------------------------------------------------------- 
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Insert_Elp01PlaySessions]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].Insert_Elp01PlaySessions
GO
CREATE PROCEDURE Insert_Elp01PlaySessions
  @pSiteId INT
, @pId BIGINT
, @pTicketNumber DECIMAL
, @pSlotSerialNumber NVARCHAR(50)
, @pSlotHouseNumber NVARCHAR(40)
, @pVenueCode INT
, @pAreaCode INT
, @pBankCode INT
, @pVendorCode INT
, @pGameCode INT
, @pStartTime DATETIME
, @pEndTime DATETIME
, @pBetAmount MONEY
, @pPaidAmount MONEY
, @pGamesPlayed INT
, @pInitialAmount MONEY
, @pAditionalAmount MONEY
, @pFinalAmount MONEY
, @pBetCombCode INT
, @pKindofTicket INT
, @pSequenceNumber BIGINT
, @pCuponNumber DECIMAL
, @pDateUpdated DATETIME
, @pDateInserted DATETIME
, @pAccountId BIGINT
AS
BEGIN   		  
  IF NOT EXISTS(SELECT   1 
				          FROM   ELP01_PLAY_SESSIONS 
			           WHERE   EPS_TICKET_NUMBER = @pTicketNumber
				           AND   EPS_KINDOF_TICKET = @pKindofTicket 
				           AND   EPS_SITE_ID       = @pSiteId)
              
  BEGIN
  
  DECLARE @externalCardId as NUMERIC(20,0)
  DECLARE @externalAccountId as NUMERIC(15,0)

  SELECT   @externalCardId    = ET_EXTERNAL_CARD_ID
		     , @externalAccountId = ET_EXTERNAL_ACCOUNT_ID
    FROM   ELP01_TRACKDATA
   WHERE   ET_AC_ACCOUNT_ID   = @pAccountId
		  
  INSERT INTO  ELP01_PLAY_SESSIONS
             ( EPS_SITE_ID
             , EPS_ID
             , EPS_TICKET_NUMBER
             , EPS_CUSTOMER_NUMBER
             , EPS_SLOT_SERIAL_NUMBER
             , EPS_SLOT_HOUSE_NUMBER
             , EPS_VENUE_CODE
             , EPS_AREA_CODE
             , EPS_BANK_CODE
             , EPS_VENDOR_CODE
             , EPS_GAME_CODE
             , EPS_START_TIME
             , EPS_END_TIME
             , EPS_BET_AMOUNT
             , EPS_PAID_AMOUNT
             , EPS_GAMES_PLAYED
             , EPS_INITIAL_AMOUNT
             , EPS_ADITIONAL_AMOUNT
             , EPS_FINAL_AMOUNT
             , EPS_BET_COMB_CODE
             , EPS_KINDOF_TICKET
             , EPS_SEQUENCE_NUMBER
             , EPS_CUPON_NUMBER
             , EPS_DATE_UPDATED
             , EPS_CARD_NUMBER
             , EPS_DATE_INSERTED)
       VALUES 
			       ( @pSiteId
          	 , @pId
             , @pTicketNumber
             , @externalAccountId
             , @pSlotSerialNumber
             , @pSlotHouseNumber
             , @pVenueCode
             , @pAreaCode
             , @pBankCode
             , @pVendorCode
             , @pGameCode
             , @pStartTime
             , @pEndTime
             , @pBetAmount
             , @pPaidAmount
             , @pGamesPlayed
             , @pInitialAmount
             , @pAditionalAmount
             , @pFinalAmount
             , @pBetCombCode
             , @pKindofTicket
             , @pSequenceNumber
             , @pCuponNumber
             , @pDateUpdated
             , @externalCardId
             , @pDateInserted )
  END
END -- Insert_Elp01PlaySessions
GO
/****** TRIGGERS ******/

/****** RECORDS ******/

