/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_100]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 51;

SET @New_ReleaseId = 52;
SET @New_ScriptName = N'UpdateTo_18.052.sql';
SET @New_Description = N'Updated alarms catalog.';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/

/****** INDEXES ******/

/****** RECORDS ******/

/*DELETE*/

DELETE FROM ALARM_CATALOG WHERE ALCG_ALARM_CODE = 65826
DELETE FROM ALARM_CATALOG WHERE ALCG_ALARM_CODE = 135168
DELETE FROM ALARM_CATALOG WHERE ALCG_ALARM_CODE = 65810
DELETE FROM ALARM_CATALOG WHERE ALCG_ALARM_CODE = 65809
DELETE FROM ALARM_CATALOG WHERE ALCG_ALARM_CODE = 65811
DELETE FROM ALARM_CATALOG WHERE ALCG_ALARM_CODE = 65812
DELETE FROM ALARM_CATALOG WHERE ALCG_ALARM_CODE = 460800
DELETE FROM ALARM_CATALOG WHERE ALCG_ALARM_CODE = 200704
DELETE FROM ALARM_CATALOG WHERE ALCG_ALARM_CODE = 393248
DELETE FROM ALARM_CATALOG WHERE ALCG_ALARM_CODE = 393216
GO

DELETE FROM ALARM_CATALOG_PER_CATEGORY WHERE ALCC_ALARM_CODE = 65826
DELETE FROM ALARM_CATALOG_PER_CATEGORY WHERE ALCC_ALARM_CODE = 135168
DELETE FROM ALARM_CATALOG_PER_CATEGORY WHERE ALCC_ALARM_CODE = 65810
DELETE FROM ALARM_CATALOG_PER_CATEGORY WHERE ALCC_ALARM_CODE = 65809
DELETE FROM ALARM_CATALOG_PER_CATEGORY WHERE ALCC_ALARM_CODE = 65811
DELETE FROM ALARM_CATALOG_PER_CATEGORY WHERE ALCC_ALARM_CODE = 65812
DELETE FROM ALARM_CATALOG_PER_CATEGORY WHERE ALCC_ALARM_CODE = 460800
DELETE FROM ALARM_CATALOG_PER_CATEGORY WHERE ALCC_ALARM_CODE = 200704
DELETE FROM ALARM_CATALOG_PER_CATEGORY WHERE ALCC_ALARM_CODE = 393248
DELETE FROM ALARM_CATALOG_PER_CATEGORY WHERE ALCC_ALARM_CODE = 393216
GO

/*INSERT*/

IF (NOT EXISTS (SELECT * FROM alarm_catalog WHERE alcg_alarm_code=200007))
BEGIN
  INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (200007, 0, N'Terminal sin jugadas', N'Terminal sin jugadas', 1)
  INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (200007, 24, 0, GETDATE())
END
GO

/*UPDATE*/

UPDATE ALARM_GROUPS SET ALG_NAME = 'Patr�n' WHERE ALG_ALARM_GROUP_ID = 4

UPDATE ALARM_CATALOG SET ALCG_NAME='M�quina de juego conectada a la corriente',ALCG_DESCRIPTION='M�quina de juego conectada a la corriente' WHERE ALCG_ALARM_CODE=65559   
UPDATE ALARM_CATALOG SET ALCG_NAME='Stacker lleno',ALCG_DESCRIPTION='Stacker lleno' WHERE ALCG_ALARM_CODE=65575   
UPDATE ALARM_CATALOG SET ALCG_NAME='Fallo del validador de billetes',ALCG_DESCRIPTION='Fallo del validador de billetes' WHERE ALCG_ALARM_CODE=65577      
UPDATE ALARM_CATALOG SET ALCG_NAME='Billete mal insertado',ALCG_DESCRIPTION='Billete mal insertado' WHERE ALCG_ALARM_CODE=65578    
UPDATE ALARM_CATALOG SET ALCG_NAME='Billete falso',ALCG_DESCRIPTION='Billete falso' WHERE ALCG_ALARM_CODE=65580   
UPDATE ALARM_CATALOG SET ALCG_NAME='Stacker casi lleno',ALCG_DESCRIPTION='Stacker casi lleno' WHERE ALCG_ALARM_CODE=65582   
UPDATE ALARM_CATALOG SET ALCG_NAME='Error CMOS RAM (datos recuperados de EEPROM)',ALCG_DESCRIPTION='Error CMOS RAM (datos recuperados de EEPROM)' WHERE ALCG_ALARM_CODE=65585   
UPDATE ALARM_CATALOG SET ALCG_NAME=N'Error CMOS RAM (sin recuperados de EEPROM)',ALCG_DESCRIPTION=N'Error CMOS RAM (no hay datos recuperados de EEPROM)' WHERE ALCG_ALARM_CODE=65586   
UPDATE ALARM_CATALOG SET ALCG_NAME='Error CMOS RAM (dispositivo)',ALCG_DESCRIPTION='Error CMOS RAM (dispositivo)' WHERE ALCG_ALARM_CODE=65587   
UPDATE ALARM_CATALOG SET ALCG_NAME='Error EEPROM (error de datos)',ALCG_DESCRIPTION='Error EEPROM (error de datos)' WHERE ALCG_ALARM_CODE=65588  
UPDATE ALARM_CATALOG SET ALCG_NAME='Error EEPROM (dispositivo)',ALCG_DESCRIPTION='Error EEPROM (dispositivo)' WHERE ALCG_ALARM_CODE=65589     
UPDATE ALARM_CATALOG SET ALCG_NAME='Error EPROM (checksum - versi�n)',ALCG_DESCRIPTION='Error EPROM (checksum - versi�n)' WHERE ALCG_ALARM_CODE=65590     
UPDATE ALARM_CATALOG SET ALCG_NAME='Error EPROM (checksum - comparaci�n)',ALCG_DESCRIPTION='Error EPROM (checksum - comparaci�n)' WHERE ALCG_ALARM_CODE=65591   
UPDATE ALARM_CATALOG SET ALCG_NAME='Error EPROM particionada (checksum - versi�n)',ALCG_DESCRIPTION='Error EPROM particionada (checksum - versi�n)' WHERE ALCG_ALARM_CODE=65592   
UPDATE ALARM_CATALOG SET ALCG_NAME='Error EPROM particionada (checksum - comparaci�n)',ALCG_DESCRIPTION='Error EPROM particionada (checksum - comparaci�n)' WHERE ALCG_ALARM_CODE=65593   
UPDATE ALARM_CATALOG SET ALCG_NAME='Atasco salida de papel',ALCG_DESCRIPTION='Atasco salida de papel' WHERE ALCG_ALARM_CODE=65633     
UPDATE ALARM_CATALOG SET ALCG_NAME='Impresora apagada',ALCG_DESCRIPTION='Impresora apagada' WHERE ALCG_ALARM_CODE=65653   
UPDATE ALARM_CATALOG SET ALCG_NAME='Reemplazar cartucho de tinta',ALCG_DESCRIPTION='Reemplazar cartucho de tinta' WHERE ALCG_ALARM_CODE=65655  
UPDATE ALARM_CATALOG SET ALCG_NAME='Carro de la impresora atascado',ALCG_DESCRIPTION='Carro de la impresora atascado' WHERE ALCG_ALARM_CODE=65656 
UPDATE ALARM_CATALOG SET ALCG_NAME='Played Big Increment',ALCG_DESCRIPTION='Played Big Increment' WHERE ALCG_ALARM_CODE=131074   
UPDATE ALARM_CATALOG SET ALCG_NAME='HPC Big Increment',ALCG_DESCRIPTION='HPC Big Increment' WHERE ALCG_ALARM_CODE=131075  
UPDATE ALARM_CATALOG SET ALCG_NAME='Reinicio SAS Meters',ALCG_DESCRIPTION='Reinicio SAS Meters' WHERE ALCG_ALARM_CODE=131080      
UPDATE ALARM_CATALOG SET ALCG_NAME='SAS Meters Big Increment',ALCG_DESCRIPTION='SAS Meters Big Increment' WHERE ALCG_ALARM_CODE=131081     
UPDATE ALARM_CATALOG SET ALCG_NAME='Salto atr�s de contadores',ALCG_DESCRIPTION='Salto atr�s de contadores' WHERE ALCG_ALARM_CODE=135168     
UPDATE ALARM_CATALOG SET ALCG_NAME='Monto incorrecto en ticket TITO offline redimido',ALCG_DESCRIPTION='Monto incorrecto en ticket TITO offline redimido' WHERE ALCG_ALARM_CODE=196615  
UPDATE ALARM_CATALOG SET ALCG_NAME='Salto de contadores',ALCG_DESCRIPTION='Salto de contadores' WHERE ALCG_ALARM_CODE=200704
UPDATE ALARM_CATALOG SET ALCG_NAME='Pago de Ticket Offline',ALCG_DESCRIPTION='Pago de Ticket Offline' WHERE ALCG_ALARM_CODE=262149     
UPDATE ALARM_CATALOG SET ALCG_NAME='Cr�ditos abandonados',ALCG_DESCRIPTION='Cr�ditos abandonados' WHERE ALCG_ALARM_CODE=393250 
UPDATE ALARM_CATALOG SET ALCG_NAME='Transferencia de salida en estado Pendiente',ALCG_DESCRIPTION='Transferencia de salida en estado Pendiente' WHERE ALCG_ALARM_CODE=393253  
UPDATE ALARM_CATALOG SET ALCG_NAME='Reportado un Documento existente en otra cuenta',ALCG_DESCRIPTION='Reportado un Documento existente en otra cuenta' WHERE ALCG_ALARM_CODE=589825  
UPDATE ALARM_CATALOG SET ALCG_NAME='Cambio de tarjeta a otra cuenta',ALCG_DESCRIPTION=' La tarjeta YYY de la cuenta X se ha asignado a la cuenta YYY' WHERE ALCG_ALARM_CODE=589826   
UPDATE ALARM_CATALOG SET ALCG_NAME='Cuenta sobrescrita por la sala',ALCG_DESCRIPTION=' La sala YYY sobreescribe los datos de la cuenta YYY' WHERE ALCG_ALARM_CODE=589827      
UPDATE ALARM_CATALOG SET ALCG_NAME='Balance negativo',ALCG_DESCRIPTION='Se actualiz� el balance de puntos de la cuenta YYY quedando en negativo YYY.' WHERE ALCG_ALARM_CODE=589828      
UPDATE ALARM_CATALOG SET ALCG_NAME='Sala no habilitada para Multisite',ALCG_DESCRIPTION='La sala XXX no est� habilitada para conectarse al MultiSite' WHERE ALCG_ALARM_CODE=589829    
UPDATE ALARM_CATALOG SET ALCG_NAME='Trackdata externo incorrecto',ALCG_DESCRIPTION='Trackdata externo incorrecto' WHERE ALCG_ALARM_CODE=1048577     
UPDATE ALARM_CATALOG SET ALCG_NAME='Error de confirmaci�n al redimir',ALCG_DESCRIPTION='Error de confirmaci�n al redimir' WHERE ALCG_ALARM_CODE=1048578   
UPDATE ALARM_CATALOG SET ALCG_NAME='Error al actualizar la cuenta',ALCG_DESCRIPTION='Error al actualizar la cuenta' WHERE ALCG_ALARM_CODE=1048579   
UPDATE ALARM_CATALOG SET ALCG_NAME='Error de relaci�n de trackdata',ALCG_DESCRIPTION='Error de relaci�n de trackdata' WHERE ALCG_ALARM_CODE=1048580     
UPDATE ALARM_CATALOG SET ALCG_NAME='Error de estado',ALCG_DESCRIPTION='Error de estado' WHERE ALCG_ALARM_CODE=1048581 
UPDATE ALARM_CATALOG SET ALCG_NAME='Error de interfaz de la transacci�n de inserci�n',ALCG_DESCRIPTION='Error de interfaz de la transacci�n de inserci�n' WHERE ALCG_ALARM_CODE=1048582 
UPDATE ALARM_CATALOG SET ALCG_NAME='Error no se ha asignado el trackdata a la cuenta',ALCG_DESCRIPTION='Error no se ha asignado el trackdata a la cuenta' WHERE ALCG_ALARM_CODE=1048584 
UPDATE ALARM_CATALOG SET ALCG_NAME='Id de stacker existe',ALCG_DESCRIPTION='El id de stacker introducido no existe' WHERE ALCG_ALARM_CODE=1114114  
UPDATE ALARM_CATALOG SET ALCG_NAME='Stacker pre-asignado incorrectamente',ALCG_DESCRIPTION='Stacker pre-asignado incorrectamente' WHERE ALCG_ALARM_CODE=1114115 
UPDATE ALARM_CATALOG SET ALCG_NAME='Error cambio de stacker',ALCG_DESCRIPTION='Error cambio de stacker' WHERE ALCG_ALARM_CODE=1114116 
UPDATE ALARM_CATALOG SET ALCG_NAME='Stacker ya introducido',ALCG_DESCRIPTION='El stacker est� preasignado a un terminal pero se ha insertado en el otro terminal' WHERE ALCG_ALARM_CODE=1114117 
UPDATE ALARM_CATALOG SET ALCG_NAME='Posible duplicidad en pago manual/Jackpot',ALCG_DESCRIPTION='Posible duplicidad en pago manual/Jackpot' WHERE ALCG_ALARM_CODE=1114128 
UPDATE ALARM_CATALOG SET ALCG_NAME='Firma err�nea del software de la m�quina',ALCG_DESCRIPTION='Firma err�nea del software de la m�quina' WHERE ALCG_ALARM_CODE=1114129 
UPDATE ALARM_CATALOG SET ALCG_NAME='Firma correcta del software de la m�quina',ALCG_DESCRIPTION='Firma correcta del software de la m�quina' WHERE ALCG_ALARM_CODE=1114130 
UPDATE ALARM_CATALOG SET ALCG_NAME='Alarma personalizada por jugado',ALCG_DESCRIPTION='Alarma personalizada por jugado' WHERE ALCG_ALARM_CODE=2097153     
UPDATE ALARM_CATALOG SET ALCG_NAME='Alarma personalizada por ganado',ALCG_DESCRIPTION='Alarma personalizada por ganado' WHERE ALCG_ALARM_CODE=2097154     
UPDATE ALARM_CATALOG SET ALCG_NAME='Alarma personalizada por cr�ditos cancelados',ALCG_DESCRIPTION='Alarma personalizada por pagos manuales de cr�ditos cancelados' WHERE ALCG_ALARM_CODE=2097155 
UPDATE ALARM_CATALOG SET ALCG_NAME='Alarma personalizada por pagos manuales de jackpot',ALCG_DESCRIPTION='Alarma personalizada por pagos manuales de jackpot' WHERE ALCG_ALARM_CODE=2097156 
UPDATE ALARM_CATALOG SET ALCG_NAME='Alarma personalizada por payout',ALCG_DESCRIPTION='Alarma personalizada por payout' WHERE ALCG_ALARM_CODE=2097157     
UPDATE ALARM_CATALOG SET ALCG_NAME='Alarma personalizada por billetes falsos',ALCG_DESCRIPTION='Alarma personalizada por billetes falsos' WHERE ALCG_ALARM_CODE=2097158 
GO

/******* STORED PROCEDURES *******/
