/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_100]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 95;

SET @New_ReleaseId = 96;
SET @New_ScriptName = N'UpdateTo_18.096.sql';
SET @New_Description = N'RECEPCION';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[customer_visits]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[customer_visits](
	[cut_visit_id] [bigint] IDENTITY(1,1) NOT NULL,
	[cut_gaming_day] [int] NOT NULL,
	[cut_customer_id] [bigint] NOT NULL,
	[cut_level] [int] NOT NULL,
	[cut_is_vip] [bit] NOT NULL,
 CONSTRAINT [PK_customer_visits] PRIMARY KEY CLUSTERED 
(
	[cut_visit_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[customer_visits] ADD  CONSTRAINT [DF_customer_visits_cut_level]  DEFAULT ((0)) FOR [cut_level]

ALTER TABLE [dbo].[customer_visits] ADD  CONSTRAINT [DF_customer_visits_cut_is_vip]  DEFAULT ((0)) FOR [cut_is_vip]
END 
GO

--- RECEPCION
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[customers]') AND type in (N'U'))
BEGIN
  CREATE TABLE [dbo].[customers](
	  [cus_customer_id] [bigint] NOT NULL,
   CONSTRAINT [PK_customers] PRIMARY KEY CLUSTERED 
  (
	  [cus_customer_id] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
  ) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[customer_records]') AND type in (N'U'))
BEGIN
  CREATE TABLE [dbo].[customer_records](
	  [cur_record_id] [bigint] IDENTITY(1,1) NOT NULL,
	  [cur_customer_id] [bigint] NOT NULL,
	  [cur_deleted] [bit] NOT NULL,
	  [cur_created] [datetime] NOT NULL,
	  [cur_expiration] [datetime] NULL,
   CONSTRAINT [PK_customer_records] PRIMARY KEY CLUSTERED 
  (
	  [cur_record_id] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
  ) ON [PRIMARY]
END
GO

IF  NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[customer_record_details]') AND type in (N'U'))
BEGIN
  CREATE TABLE [dbo].[customer_record_details](
	  [curd_detail_id] [bigint] IDENTITY(1,1) NOT NULL,
	  [curd_record_id] [bigint] NOT NULL,
	  [curd_deleted] [bit] NOT NULL,
	  [curd_type] [int] NOT NULL,
	  [curd_data] [nvarchar](max) NULL,
	  [curd_created] [datetime] NOT NULL,
	  [curd_expiration] [datetime] NULL,
	  [curd_image] [varbinary](max) NULL,
   CONSTRAINT [PK_customer_record_details] PRIMARY KEY CLUSTERED 
  (
	  [curd_detail_id] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
  ) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[customer_visit_gt_stats]') AND type in (N'U'))
BEGIN
  CREATE TABLE [dbo].[customer_visit_gt_stats](
	  [cvgt_visit_id] [bigint] NOT NULL,
	  [cvgt_playing_time] [int] NOT NULL
  ) ON [PRIMARY]
END
GO

IF  NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[customer_visit_egm_stats]') AND type in (N'U'))
BEGIN
  CREATE TABLE [dbo].[customer_visit_egm_stats](
	  [cve_visit_id] [bigint] NOT NULL,
	  [cve_credit_type] [int] NOT NULL,
	  [cve_total_in] [money] NOT NULL,
	  [cve_total_out] [money] NOT NULL,
	  [cve_coin_in] [money] NOT NULL,
	  [cve_coin_out] [money] NOT NULL,
	  [cve_jackpot] [money] NOT NULL,
	  [cve_theoretical_coin_out] [money] NOT NULL,
	  [cve_playing_time] [int] NULL
  ) ON [PRIMARY]
END
GO

IF  NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[blacklist_internal_block_list]') AND type in (N'U'))
BEGIN
  CREATE TABLE [dbo].[blacklist_internal_block_list](
	  [bkl_id] [bigint] IDENTITY(1,1) NOT NULL,
	  [bkl_account_id] [bigint] NOT NULL,
	  [bkl_inclusion_date] [datetime] NOT NULL,
	  [bkl_reason] [smallint] NOT NULL,
	  [bkl_reason_description] [varchar](max) NULL,
   CONSTRAINT [PK_blacklist_internal_block_list] PRIMARY KEY CLUSTERED 
  (
	  [bkl_id] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
  ) ON [PRIMARY]
END
GO

IF  NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[customer_entrances_prices]') AND type in (N'U'))
BEGIN
  CREATE TABLE [dbo].[customer_entrances_prices](
	  [cuep_price_id] [bigint] IDENTITY(1,1) NOT NULL,
	  [cuep_description] [nvarchar](max) NOT NULL,
	  [cuep_price] [money] NOT NULL,
	  [cuep_customer_level] [int] NULL,
	  [cuep_default] [bit] NOT NULL,
   CONSTRAINT [PK_Customer_Entrances_Prices] PRIMARY KEY CLUSTERED 
  (
	  [cuep_price_id] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
  ) ON [PRIMARY]
END
GO

IF  NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[customer_entrances]') AND type = 'U')
BEGIN
  CREATE TABLE [dbo].[customer_entrances](
	[cue_entrance_datetime] [datetime] NOT NULL,
	[cue_visit_id] [bigint] NOT NULL,
	[cue_cashier_session_id] [bigint] NULL,
	[cue_cashier_user_id] [int] NULL,
	[cue_entrance_block_reason] [int] NULL,
	[cue_entrance_block_reason2] [int] NULL,
	[cue_entrance_block_description] [nvarchar](256) NULL,
	[cue_remarks] [nvarchar](256) NULL,
	[cue_ticket_entry_id] [bigint] NULL,
	[cue_ticket_entry_price_real] [money] NULL,
	[cue_document_type] [int] NULL,
	[cue_document_number] [nvarchar](50) NULL,
	[cue_voucher_id] [bigint] NULL,
	[cue_cashier_terminal_id] [bigint] NULL,
	[cue_coupon] [nchar](250) NULL,
	[cue_voucher_sequence] [bigint] NULL,
	[cue_entrance_expiration] [datetime] NULL,
	[cue_ticket_entry_price_paid] [money] NULL,
	[cue_ticket_entry_price_difference] [money] NULL,
 CONSTRAINT [PK_customer_entrances] PRIMARY KEY CLUSTERED 
(
	[cue_entrance_datetime] ASC,
	[cue_visit_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[customer_entrances]  WITH CHECK ADD  CONSTRAINT [FK_customer_entrances_customer_visits] FOREIGN KEY([cue_visit_id])
REFERENCES [dbo].[customer_visits] ([cut_visit_id])

ALTER TABLE [dbo].[customer_entrances] CHECK CONSTRAINT [FK_customer_entrances_customer_visits]

ALTER TABLE [dbo].[customer_entrances] ADD  CONSTRAINT [DF_customer_entrances_ce_entrance_datetime]  DEFAULT (getdate()) FOR [cue_entrance_datetime]

END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[blacklist_internal_block_list]') AND type in (N'U'))
BEGIN
  CREATE TABLE [dbo].[blacklist_internal_block_list](
	  [bkl_id] [bigint] IDENTITY(1,1) NOT NULL,
	  [bkl_account_id] [bigint] NOT NULL,
	  [bkl_inclusion_date] [datetime] NOT NULL,
	  [bkl_reason] [smallint] NOT NULL,
	  [bkl_reason_description] [varchar](max) NULL,
   CONSTRAINT [PK_blacklist_internal_block_list] PRIMARY KEY CLUSTERED 
  (
	  [bkl_id] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
  ) ON [PRIMARY]
END
GO

IF  NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[blacklist_file_imported]') AND type in (N'U'))
BEGIN
  CREATE TABLE [dbo].[blacklist_file_imported](
	  [blkf_id] [int] IDENTITY(1,1) NOT NULL,
	  [blkf_name] [nvarchar](50) NOT NULL,
	  [blkf_middle_name] [nvarchar](50) NULL,
	  [blkf_lastname_1] [nvarchar](50) NOT NULL,
	  [blkf_lastname_2] [nvarchar](50) NULL,
	  [blkf_document_type] [smallint] NOT NULL,
	  [blkf_document] [nvarchar](20) NOT NULL,
	  [bklf_exclusion_date] [datetime] NOT NULL,
	  [bklf_reason_type] [smallint] NULL,
	  [bklf_reason_description] [nvarchar](max) NOT NULL,
	  [bklf_exclusion_duration] [int] NULL,
	  [bklf_origin] [smallint] NULL,
	  [blkf_reference] [nvarchar](50) NULL
  ) ON [PRIMARY]
END
GO

/** DELETE CONSTRAINTS **/

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_customer_entrances_customer_visits]') AND parent_object_id = OBJECT_ID(N'[dbo].[customer_entrances]'))
ALTER TABLE [dbo].[customer_entrances] DROP CONSTRAINT [FK_customer_entrances_customer_visits]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_customer_record_details_customer_records]') AND parent_object_id = OBJECT_ID(N'[dbo].[customer_record_details]'))
ALTER TABLE [dbo].[customer_record_details] DROP CONSTRAINT [FK_customer_record_details_customer_records]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_customer_record_details_history_customer_records_history]') AND parent_object_id = OBJECT_ID(N'[dbo].[customer_record_details_history]'))
ALTER TABLE [dbo].[customer_record_details_history] DROP CONSTRAINT [FK_customer_record_details_history_customer_records_history]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_customer_records_customers]') AND parent_object_id = OBJECT_ID(N'[dbo].[customer_records]'))
ALTER TABLE [dbo].[customer_records] DROP CONSTRAINT [FK_customer_records_customers]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_customer_records_history_customers]') AND parent_object_id = OBJECT_ID(N'[dbo].[customer_records_history]'))
ALTER TABLE [dbo].[customer_records_history] DROP CONSTRAINT [FK_customer_records_history_customers]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_customer_visit_egm_stats_customer_visits]') AND parent_object_id = OBJECT_ID(N'[dbo].[customer_visit_egm_stats]'))
ALTER TABLE [dbo].[customer_visit_egm_stats] DROP CONSTRAINT [FK_customer_visit_egm_stats_customer_visits]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_customer_visit_gt_stats_customer_visits]') AND parent_object_id = OBJECT_ID(N'[dbo].[customer_visit_gt_stats]'))
ALTER TABLE [dbo].[customer_visit_gt_stats] DROP CONSTRAINT [FK_customer_visit_gt_stats_customer_visits]
GO

/** INDICES **/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[customer_entrances]') AND name = N'IX_cue_visit_id')
BEGIN
  CREATE NONCLUSTERED INDEX [IX_cue_visit_id] ON [dbo].[customer_entrances] 
  (
	  [cue_visit_id] ASC
  )
  INCLUDE ( [cue_cashier_user_id],
  [cue_ticket_entry_price_real],
  [cue_ticket_entry_price_paid]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
END
GO

IF  NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[customer_entrances]') AND name = N'IX_dta_index_customer_entrances_cue_visit_id')
BEGIN
  CREATE NONCLUSTERED INDEX [IX_dta_index_customer_entrances_cue_visit_id] ON [dbo].[customer_entrances] 
  (
	  [cue_visit_id] ASC
  )
  INCLUDE ( [cue_ticket_entry_price_real],
  [cue_ticket_entry_price_paid]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
END
GO

IF  NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[customer_visits]') AND name = N'IX_cut_customer_id__cut_gaming_day__cut_visit_id')
BEGIN
  CREATE NONCLUSTERED INDEX [IX_cut_customer_id__cut_gaming_day__cut_visit_id] ON [dbo].[customer_visits] 
  (
	  [cut_customer_id] ASC,
	  [cut_gaming_day] ASC,
	  [cut_visit_id] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[customer_visits]') AND name = N'IX_cut_gaming_day')
BEGIN
  CREATE NONCLUSTERED INDEX [IX_cut_gaming_day] ON [dbo].[customer_visits] 
  (
	  [cut_gaming_day] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
END
GO

IF  NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[customer_visits]') AND name = N'IX_cut_gaming_day_cut_visit_id')
BEGIN
  CREATE NONCLUSTERED INDEX [IX_cut_gaming_day_cut_visit_id] ON [dbo].[customer_visits] 
  (
	  [cut_gaming_day] ASC,
	  [cut_visit_id] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
END
GO

IF  NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[customer_visits]') AND name = N'IX_cut_visit_id_cut_gaming_day')
BEGIN
  CREATE NONCLUSTERED INDEX [IX_cut_visit_id_cut_gaming_day] ON [dbo].[customer_visits] 
  (
	  [cut_visit_id] ASC,
	  [cut_gaming_day] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
END
GO

/** VISTAS **/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[v_TotalVisitsByDay]'))
DROP VIEW [dbo].[v_TotalVisitsByDay]
GO
CREATE VIEW [dbo].[v_TotalVisitsByDay]
AS
SELECT     TOP (100) PERCENT dbo.customer_visits.cut_gaming_day AS SQL_COLUMN_GAMING_DAY, COUNT(DISTINCT dbo.customer_visits.cut_visit_id) AS TotalByDay, 
                      '' AS Colum1, '' AS Colum2, 0 AS Column3, SUM(ISNULL(dbo.customer_entrances.cue_ticket_entry_price_real, 0)) AS TotalTeoricByDay, 
                      SUM(ISNULL(dbo.customer_entrances.cue_ticket_entry_price_paid, 0)) AS TotalRealByDay, 
                      SUM(ISNULL(ISNULL(dbo.customer_entrances.cue_ticket_entry_price_real, 0) - ISNULL(dbo.customer_entrances.cue_ticket_entry_price_paid, 0), 0)) 
                      AS SQL_COLUMN_DIFFERENCE_ACCUMULATE, COUNT(dbo.customer_visits.cut_visit_id) AS TotalEntries
FROM         dbo.customer_visits INNER JOIN
                      dbo.customer_entrances ON dbo.customer_visits.cut_visit_id = dbo.customer_entrances.cue_visit_id
GROUP BY dbo.customer_visits.cut_gaming_day
ORDER BY SQL_COLUMN_GAMING_DAY

GO



/****** Object:  View [dbo].[v_TotalEntrancesByVisits]    Script Date: 02/18/2016 13:12:11 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[v_TotalEntrancesByVisits]'))
  DROP VIEW [dbo].[v_TotalEntrancesByVisits]
GO

CREATE VIEW [dbo].[v_TotalEntrancesByVisits]
AS
SELECT     TOP (100) PERCENT dbo.v_TotalVisitsByDay.SQL_COLUMN_GAMING_DAY, dbo.v_TotalVisitsByDay.TotalByDay AS SQL_COLUMN_VISIT_NUMBER, 
                      dbo.accounts.ac_holder_name AS SQL_COLUMN_HOLDER_NAME, dbo.TrackDataToExternal(dbo.accounts.ac_track_data) AS SQL_COLUMN_CARD_TRACK, 
                      COUNT(dbo.customer_entrances.cue_visit_id) AS SQL_COLUMN_ENTRIES, dbo.v_TotalVisitsByDay.TotalTeoricByDay AS SQL_COLUMN_THEORICAL_BY_DAY, 
                      dbo.v_TotalVisitsByDay.TotalRealByDay AS SQL_COLUMN_REAL_BY_DAY, 
                      dbo.v_TotalVisitsByDay.TotalRealByDay - dbo.v_TotalVisitsByDay.TotalTeoricByDay AS SQL_COLUMN_DIFFERENCE_BY_DAY, 
                      SUM(ISNULL(dbo.customer_entrances.cue_ticket_entry_price_real, 0)) AS SQL_COLUMN_THEORICAL_COLLECTION, 
                      SUM(ISNULL(dbo.customer_entrances.cue_ticket_entry_price_paid, 0)) AS SQL_COLUMN_REAL_COLLECTION, 
                      SUM(ISNULL(dbo.customer_entrances.cue_ticket_entry_price_paid, 0) - ISNULL(dbo.customer_entrances.cue_ticket_entry_price_real, 0)) 
                      AS SQL_COLUMN_DIFFERENCE
FROM         dbo.accounts INNER JOIN
                      dbo.customer_visits ON dbo.accounts.ac_account_id = dbo.customer_visits.cut_customer_id INNER JOIN
                      dbo.v_TotalVisitsByDay ON dbo.customer_visits.cut_gaming_day = dbo.v_TotalVisitsByDay.SQL_COLUMN_GAMING_DAY INNER JOIN
                      dbo.customer_entrances ON dbo.customer_visits.cut_visit_id = dbo.customer_entrances.cue_visit_id
GROUP BY dbo.v_TotalVisitsByDay.TotalByDay, dbo.accounts.ac_holder_name, dbo.TrackDataToExternal(dbo.accounts.ac_track_data), 
                      dbo.v_TotalVisitsByDay.TotalTeoricByDay, dbo.v_TotalVisitsByDay.TotalRealByDay, 
                      dbo.v_TotalVisitsByDay.TotalRealByDay - dbo.v_TotalVisitsByDay.TotalTeoricByDay, dbo.v_TotalVisitsByDay.SQL_COLUMN_GAMING_DAY
ORDER BY dbo.v_TotalVisitsByDay.SQL_COLUMN_GAMING_DAY DESC

GO



/****** Object:  View [dbo].[v_TotalEntriesByMonth]    Script Date: 02/18/2016 13:06:01 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[v_TotalEntriesByMonth]'))
  DROP VIEW [dbo].[v_TotalEntriesByMonth]
GO

CREATE VIEW [dbo].[v_TotalEntriesByMonth]
AS
SELECT     TOP (100) PERCENT dbo.customer_visits.cut_gaming_day AS SQL_COLUMN_GAMING_DAY, COUNT(DISTINCT dbo.customer_visits.cut_visit_id) 
                      AS SQL_COLUMN_VISIT_NUMBER, '""' AS Col1, '""' AS Col2, 0 AS Col3, SUM(ISNULL(dbo.customer_entrances.cue_ticket_entry_price_real, 0)) 
                      AS SQL_COLUMN_THEORICAL_BY_DAY, SUM(ISNULL(dbo.customer_entrances.cue_ticket_entry_price_paid, 0)) AS SQL_COLUMN_REAL_BY_DAY, 
                      SUM(ISNULL(dbo.customer_entrances.cue_ticket_entry_price_paid, 0) - ISNULL(dbo.customer_entrances.cue_ticket_entry_price_real, 0)) 
                      AS SQL_COLUMN_DIFFERENCE_BY_DAY, COUNT(dbo.customer_entrances.cue_cashier_user_id) AS SQL_COLUMN_TOTAL_ENTRIES
FROM         dbo.customer_visits INNER JOIN
                      dbo.customer_entrances ON dbo.customer_visits.cut_visit_id = dbo.customer_entrances.cue_visit_id
GROUP BY dbo.customer_visits.cut_gaming_day
ORDER BY SQL_COLUMN_GAMING_DAY DESC

GO

/** GENERAL PARAMS RECEPTION/VISITS MODULE **/

IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='Reception' AND GP_SUBJECT_KEY = 'Enabled')
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Reception', 'Enabled', '0')
GO   
IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='Reception' AND GP_SUBJECT_KEY = 'RequireNewCard')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Reception', 'RequireNewCard','1')
GO
IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='Reception' AND GP_SUBJECT_KEY = 'CardFunctionsEnabled') 	
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Reception', 'CardFunctionsEnabled','1')
GO
IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='Account.VisibleField' AND GP_SUBJECT_KEY = 'Photo') 	
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Account.VisibleField','Photo','1')
GO
IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='Account.RequestedField' AND GP_SUBJECT_KEY = 'Photo') 	
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Account.RequestedField','Photo','0')
GO
IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='Account.RequestedField' AND GP_SUBJECT_KEY = 'AntiMoneyLaundering.Photo') 	
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Account.RequestedField','AntiMoneyLaundering.Photo','0')
GO
IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Reception' AND GP_SUBJECT_KEY ='Agreement.Text')
	INSERT INTO general_params (gp_group_key, gp_subject_key, gp_key_value)VALUES ('Reception', 'Agreement.Text', '');
GO
IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Reception' AND GP_SUBJECT_KEY ='Agreement.ShowMode')
	INSERT INTO general_params (gp_group_key, gp_subject_key, gp_key_value) VALUES ('Reception', 'Agreement.ShowMode', '0');
GO
IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.Voucher' AND GP_SUBJECT_KEY ='Reception.Footer')
	INSERT INTO general_params (gp_group_key, gp_subject_key, gp_key_value) VALUES ('Cashier.Voucher', 'Reception.Footer', '');
GO
IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Reception' AND GP_SUBJECT_KEY ='Record.ExpirationDays')
	INSERT INTO general_params (gp_group_key, gp_subject_key, gp_key_value) VALUES ('Reception', 'Record.ExpirationDays', '90');
GO
IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Reception' AND GP_SUBJECT_KEY ='Mode')
	INSERT INTO general_params (gp_group_key, gp_subject_key, gp_key_value) VALUES ('Reception', 'Mode', '0');
GO
IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Reception' AND GP_SUBJECT_KEY ='TimeBeforeEntry')
	INSERT INTO general_params (gp_group_key, gp_subject_key, gp_key_value) VALUES ('Reception', 'TimeBeforeEntry', '0');
GO