/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_100]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 10;

SET @New_ReleaseId = 14;
SET @New_ScriptName = N'UpdateTo_18.014.sql';
SET @New_Description = N'Import accounts';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/

/****** VIEWS ******/

/****** CONSTRAINTS ******/

/****** FUNCTIONS ******/

/****** INDEXES ******/

/****** STORED PROCEDURES ******/
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CreateAccount]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CreateAccount]
GO

CREATE PROCEDURE [dbo].[CreateAccount]
@AccountId    BIGINT       OUTPUT
AS
BEGIN
                DECLARE @SiteId            as INT
                DECLARE @NewTrackData      as NVARCHAR(50)

--            SELECT   @SiteId = CAST(GP_KEY_VALUE AS INT) 
--              FROM   GENERAL_PARAMS 
--            WHERE   GP_GROUP_KEY   = 'Site' 
--               AND   GP_SUBJECT_KEY = 'Identifier'
--               AND   ISNUMERIC(GP_KEY_VALUE) = 1

  set @SiteId = 0

--            IF ( ISNULL(@SiteId, 0) <= 0 ) 
--                            RAISERROR ('SiteId not configured!', 20, 0) WITH LOG

                IF @SiteId > 999
                               RAISERROR ('SiteId is greater than 999', 20, 0) WITH LOG

                               
    SET @NewTrackData = '-RECYCLED-NEW-' + CAST (NEWID() AS NVARCHAR(50))
                               
                UPDATE   SEQUENCES 
                   SET   SEQ_NEXT_VALUE = SEQ_NEXT_VALUE + 1
                WHERE   SEQ_ID         = 4

                IF @@ROWCOUNT = 0 
                BEGIN
                               RAISERROR ('Sequence #4 does not exist!', 20, 0) WITH LOG
                END

                SELECT @AccountId = SEQ_NEXT_VALUE - 1 FROM SEQUENCES WHERE SEQ_ID  = 4

                SET @AccountId = 1000000 + @AccountId * 1000 + @SiteId % 1000
    
                INSERT INTO ACCOUNTS (AC_ACCOUNT_ID, AC_TYPE, AC_BLOCKED, AC_TRACK_DATA) VALUES (@AccountId, 2, 1, @NewTrackData)

END
GO

GRANT EXECUTE ON [dbo].[CreateAccount] TO [wggui] WITH GRANT OPTION
GO

IF NOT EXISTS(SELECT SEQ_ID FROM SEQUENCES WHERE SEQ_ID = 4)
BEGIN
                INSERT INTO SEQUENCES (SEQ_ID, SEQ_NEXT_VALUE) VALUES (4, 1);
END
GO


/****** TRIGGERS ******/

/****** RECORDS ******/






