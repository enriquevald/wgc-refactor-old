/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_100]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 97;

SET @New_ReleaseId = 98;
SET @New_ScriptName = N'UpdateTo_18.098.sql';
SET @New_Description = N'buckets';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[customer_record_details_history]'))
  DROP TABLE [dbo].[customer_record_details_history]
GO
CREATE TABLE [dbo].[customer_record_details_history](
      [curdh_detail_history_id] [bigint] IDENTITY(1,1) NOT NULL,
      [curdh_record_id] [bigint] NOT NULL,
      [curdh_deleted] [bit] NOT NULL,
      [curdh_type] [int] NOT NULL,
      [curdh_data] [nvarchar](max) NULL,
      [curdh_created] [datetime] NOT NULL,
      [curdh_expiration] [datetime] NULL,
      [curdh_logdate] [datetime] NOT NULL,
      [curdh_image] [varbinary](max) NULL,
CONSTRAINT [PK_customer_record_details_history] PRIMARY KEY CLUSTERED 
(
      [curdh_detail_history_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO


/****** INDEXES ******/

/****** RECORDS ******/

DECLARE @bu_name_bucket_8 NVARCHAR(100)
DECLARE @bu_name_bucket_9 NVARCHAR(100)

if exists(select 1 from GENERAL_PARAMS where gp_group_key='WigosGUI' and gp_subject_key = 'Language' and gp_key_value = 'es') begin
            set @bu_name_bucket_8 = 'Puntos de nivel - Generados'
            set @bu_name_bucket_9 = 'Puntos de nivel - Discrecionales'

end else 
begin
            set @bu_name_bucket_8 = 'Ranking level points - Generated'
            set @bu_name_bucket_9 = 'Ranking level points - Discretionary'
end


UPDATE buckets set bu_name = @bu_name_bucket_8 where bu_bucket_id = 8

UPDATE buckets set bu_name = @bu_name_bucket_9 where bu_bucket_id = 9

GO


DECLARE @bucket_enabled bit

select @bucket_enabled = bu_enabled from buckets where bu_bucket_id = 2 -- Puntos de nivel

set @bucket_enabled = ISNULL(@bucket_enabled, 0)

update buckets set bu_enabled = @bucket_enabled where bu_bucket_id in (8,9)

GO

/****** PROCEDURES ******/