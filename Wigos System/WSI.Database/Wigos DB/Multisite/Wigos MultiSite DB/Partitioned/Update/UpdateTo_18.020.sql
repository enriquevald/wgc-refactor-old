/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_100]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 19;

SET @New_ReleaseId = 20;
SET @New_ScriptName = N'UpdateTo_18.020.sql';
SET @New_Description = N'Provider CASINO is Created; ADD st_connection_string Field to SITES; ADD gup_master_id in gui_user_profiles; Update SP Insert_Elp01PlaySessions,Update_PersonalInfo,Update_PersonalInfoForOdVersion; Update MultiSiteTrigger_AccountUpdate; Add Update_AccountDocuments; Add MultiSiteTrigger_CenterAccountDocument';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/
--02-07-2013
ALTER TABLE dbo.accounts ADD
	ac_holder_occupation nvarchar(50) NULL,
	ac_holder_ext_num nvarchar(10) NULL,
	ac_holder_nationality Int NULL,
	ac_holder_birth_country Int NULL,
	ac_holder_fed_entity Int NULL,
	ac_holder_id1_type int NULL CONSTRAINT [DF_ac_holder_id1_type] DEFAULT ((1)), -- RFC
	ac_holder_id2_type int NULL CONSTRAINT [DF_ac_holder_id2_type] DEFAULT ((2)), -- CURP
	ac_holder_id3_type int NULL,
	ac_holder_id3 nvarchar(20) NULL,
	ac_holder_as_beneficiary bit NULL CONSTRAINT [DF_ac_holder_as_beneficiary] DEFAULT ((1)),
	ac_beneficiary_name nvarchar(200) NULL,
	ac_beneficiary_name1 nvarchar(50) NULL,
	ac_beneficiary_name2 nvarchar(50) NULL,
	ac_beneficiary_name3 nvarchar(50) NULL,
	ac_beneficiary_birth_date Datetime NULL,
	ac_beneficiary_gender int NULL,
	ac_beneficiary_occupation nvarchar(50) NULL,
	ac_beneficiary_id1_type int NULL CONSTRAINT [DF_ac_beneficiary_id1_type] DEFAULT ((1)), -- RFC
	ac_beneficiary_id1 nvarchar(20) NULL,
	ac_beneficiary_id2_type int NULL CONSTRAINT [DF_ac_beneficiary_id2_type] DEFAULT ((2)), -- CURP
	ac_beneficiary_id2 nvarchar(20) NULL,
	ac_beneficiary_id3_type int NULL,
	ac_beneficiary_id3 nvarchar(20) NULL
GO
UPDATE dbo.accounts
SET   ac_holder_id1_type = 1 -- RFC
    , ac_holder_id2_type = 2 -- CURP
    , ac_beneficiary_id1_type = 1 -- RFC
    , ac_beneficiary_id2_type = 2 -- CURP
	, ac_holder_as_beneficiary = 1
GO

CREATE TABLE [dbo].[federal_states](
	[fs_state_id] [int] IDENTITY(1,1) NOT NULL,
	[fs_name] [nvarchar](50) NOT NULL,	
 CONSTRAINT [PK_federal_states] PRIMARY KEY CLUSTERED 
(
	[fs_state_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[countries](
       [co_country_id] [int] NOT NULL,
         [co_language_id] [nvarchar](5) NOT NULL,
       [co_name] [nvarchar](50) NOT NULL,      
       [co_adjective] [nvarchar](50) NOT NULL,      
CONSTRAINT [PK_countries] PRIMARY KEY CLUSTERED 
(
       [co_country_id] ASC, 
         [co_language_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


--02-07-2013
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[account_documents]') AND type in (N'U'))
BEGIN
	DROP TABLE [account_documents]
END
GO
CREATE TABLE [dbo].[account_documents](
      [ad_account_id] [bigint] NOT NULL,
      [ad_created] [datetime] NOT NULL CONSTRAINT [DF_account_documents_created]  DEFAULT (getdate()),
      [ad_modified] [datetime] NOT NULL CONSTRAINT [DF_account_documents_modified]  DEFAULT (getdate()),
      [ad_data] [varbinary](max) NULL,
      [ad_ms_sequence_id] [bigint] NULL,
CONSTRAINT [PK_account_documents] PRIMARY KEY CLUSTERED 
(
      [ad_account_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
--02-07-2013

--MULTISITE
GO
ALTER TABLE SITES ADD [st_connection_string] [xml] NULL
GO
ALTER TABLE dbo.gui_user_profiles ADD gup_master_id int NULL
GO
/****** VIEWS ******/

/****** CONSTRAINTS ******/

/****** FUNCTIONS ******/

/****** INDEXES ******/

/****** STORED PROCEDURES ******/
--05-07-2013
--------------------------------------------------------------------------------
-- Copyright � 2013 Win Systems International
--------------------------------------------------------------------------------
--
--   MODULE NAME: Update_AccountDocuments.sql
--
--   DESCRIPTION: Update Account Documents in Multisite
--
--        AUTHOR: Jos� Mart�nez L�pez
--
-- CREATION DATE: 02-JUL-2013
--
-- REVISION HISTORY:
--
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 02-JUL-2013 JML    First release.  
-------------------------------------------------------------------------------- 
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Update_AccountDocuments]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Update_AccountDocuments]
GO

CREATE PROCEDURE Update_AccountDocuments
                 @pAccountId BIGINT
               , @pCreated DATETIME
               , @pModified DATETIME
               , @pData VARBINARY(MAX)
AS
BEGIN   
IF EXISTS (SELECT 1 FROM ACCOUNT_DOCUMENTS WHERE AD_ACCOUNT_ID = @pAccountId)
  BEGIN   
	  UPDATE   ACCOUNT_DOCUMENTS
       SET   AD_CREATED        = @pCreated				
           , AD_MODIFIED       = @pModified		  
           , AD_DATA           = @pData  
     WHERE   AD_ACCOUNT_ID     = @pAccountId 
  END
ELSE
  BEGIN   
  
    INSERT INTO   ACCOUNT_DOCUMENTS
                ( AD_ACCOUNT_ID
                , AD_CREATED
                , AD_MODIFIED
                , AD_DATA )
         VALUES ( @pAccountId 
                , @pCreated
                , @pModified
                , @pData )

  END
END -- Update_AccountDocuments
GO
 
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Insert_Elp01PlaySessions]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].Insert_Elp01PlaySessions
GO
CREATE PROCEDURE Insert_Elp01PlaySessions
  @pSiteId INT
, @pId BIGINT
, @pTicketNumber NUMERIC(20,0)
, @pSlotSerialNumber NVARCHAR(50)
, @pSlotHouseNumber NVARCHAR(40)
, @pVenueCode INT
, @pAreaCode INT
, @pBankCode INT
, @pVendorCode INT
, @pGameCode INT
, @pStartTime DATETIME
, @pEndTime DATETIME
, @pBetAmount MONEY
, @pPaidAmount MONEY
, @pGamesPlayed INT
, @pInitialAmount MONEY
, @pAditionalAmount MONEY
, @pFinalAmount MONEY
, @pBetCombCode INT
, @pKindofTicket INT
, @pSequenceNumber NUMERIC(15,0)
, @pCuponNumber  VARCHAR(20)
, @pDateUpdated DATETIME
, @pDateInserted DATETIME
, @pAccountId BIGINT
AS
BEGIN   		  
  IF NOT EXISTS(SELECT   1 
				          FROM   ELP01_PLAY_SESSIONS 
			           WHERE   EPS_TICKET_NUMBER = @pTicketNumber
				           AND   EPS_KINDOF_TICKET = @pKindofTicket 
				           AND   EPS_SITE_ID       = @pSiteId)
              
  BEGIN
  
  DECLARE @external_card_id as NUMERIC(20,0)
  DECLARE @external_account_id as NUMERIC(15,0)

  SELECT   @external_card_id    = ET_EXTERNAL_CARD_ID
		     , @external_account_id = ET_EXTERNAL_ACCOUNT_ID
    FROM   ELP01_TRACKDATA
   WHERE   ET_AC_ACCOUNT_ID   = @pAccountId
		  
  INSERT INTO  ELP01_PLAY_SESSIONS
             ( EPS_SITE_ID
             , EPS_ID
             , EPS_TICKET_NUMBER
             , EPS_CUSTOMER_NUMBER
             , EPS_SLOT_SERIAL_NUMBER
             , EPS_SLOT_HOUSE_NUMBER
             , EPS_VENUE_CODE
             , EPS_AREA_CODE
             , EPS_BANK_CODE
             , EPS_VENDOR_CODE
             , EPS_GAME_CODE
             , EPS_START_TIME
             , EPS_END_TIME
             , EPS_BET_AMOUNT
             , EPS_PAID_AMOUNT
             , EPS_GAMES_PLAYED
             , EPS_INITIAL_AMOUNT
             , EPS_ADITIONAL_AMOUNT
             , EPS_FINAL_AMOUNT
             , EPS_BET_COMB_CODE
             , EPS_KINDOF_TICKET
             , EPS_SEQUENCE_NUMBER
             , EPS_CUPON_NUMBER
             , EPS_DATE_UPDATED
             , EPS_CARD_NUMBER
             , EPS_DATE_INSERTED)
       VALUES 
			 ( @pSiteId
          	 , @pId
             , @pTicketNumber
             , @external_account_id
             , @pSlotSerialNumber
             , @pSlotHouseNumber
             , @pVenueCode
             , @pAreaCode
             , @pBankCode
             , @pVendorCode
             , @pGameCode
             , @pStartTime
             , @pEndTime
             , @pBetAmount
             , @pPaidAmount
             , @pGamesPlayed
             , @pInitialAmount
             , @pAditionalAmount
             , @pFinalAmount
             , @pBetCombCode
             , @pKindofTicket
             , @pSequenceNumber
             , @pCuponNumber
             , @pDateUpdated
             , @external_card_id
             , @pDateInserted )
  END
END -- Insert_Elp01PlaySessions
GO
  --------------------------------------------------------------------------------
  -- Copyright � 2013 Win Systems International
  --------------------------------------------------------------------------------
  --
  --   MODULE NAME: Update_PersonalInfo.sql.sql
  --
  --   DESCRIPTION: Update personal Information 
  --
  --        AUTHOR: Dani Dom�nguez
  --
  -- CREATION DATE: 08-MAR-2013
  --
  -- REVISION HISTORY:
  --
  -- Date        Author Description
  -- ----------- ------ ----------------------------------------------------------
  -- 08-MAR-2013 DDM    First release.  
  -- 22-MAY-2013 DDM    Fixed bugs 783,793 and 693
  --                    Added field AC_BLOCK_DESCRIPTION
  -- 28-MAY-2013 DDM    Fixed bug #803
  --                    Added field AC_EXTERNAL_REFERENCE
  -- 03-JUL-2013 DDM    Added new fields about Money Laundering
  -------------------------------------------------------------------------------- 

  IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Update_PersonalInfo]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Update_PersonalInfo]
  GO
  CREATE PROCEDURE [dbo].[Update_PersonalInfo]
    @pCallingSiteId int
  , @pAccountId bigint
  , @pTrackData nvarchar(50)
  , @pAccountCreated Datetime 
  , @pHolderName nvarchar(200)
  , @pHolderId nvarchar(20)
  , @pHolderIdType int
  , @pHolderAddress01 nvarchar(50)
  , @pHolderAddress02 nvarchar(50)
  , @pHolderAddress03 nvarchar(50)
  , @pHolderCity nvarchar(50)
  , @pHolderZip  nvarchar(10) 
  , @pHolderEmail01 nvarchar(50)
  , @pHolderEmail02 nvarchar(50)
  , @pHolderTwitter nvarchar(50)
  , @pHolderPhoneNumber01 nvarchar(20)
  , @pHolderPhoneNumber02 nvarchar(20)
  , @pHolderComments nvarchar(100)
  , @pHolderId1 nvarchar(20)
  , @pHolderId2 nvarchar(20)
  , @pHolderDocumentId1 bigint
  , @pHolderDocumentId2 bigint
  , @pHolderName1 nvarchar(50)
  , @pHolderName2 nvarchar(50)
  , @pHolderName3 nvarchar(50)
  , @pHolderGender  int
  , @pHolderMaritalStatus int
  , @pHolderBirthDate datetime
  , @pHolderWeddingDate datetime
  , @pHolderLevel int
  , @pHolderLevelNotify int
  , @pHolderLevelEntered datetime
  , @pHolderLevelExpiration datetime
  , @pPin nvarchar(12)
  , @pPinFailures int
  , @pPinLastModified datetime
  , @pBlocked bit
  , @pActivated bit
  , @pBlockReason int
  , @pHolderIsVip int
  , @pHolderTitle nvarchar(15)                       
  , @pHolderName4 nvarchar(50)                       
  , @pHolderPhoneType01  int
  , @pHolderPhoneType02  int
  , @pHolderState    nvarchar(50)                    
  , @pHolderCountry  nvarchar(50) 
  , @pUserType  int
  , @pPersonalInfoSeqId  int
  , @pPointsStatus int
  , @pDeposit money
  , @pCardPay bit
  , @pBlockDescription nvarchar(256) 
  , @pMSCreatedOnSiteId Int 
  , @pExternalReference nvarchar(50) 
  , @pHolderOccupation  nvarchar(50) 	
  , @pHolderExtNum      nvarchar(10) 
  , @pHolderNationality  Int 
  , @pHolderBirthCountry Int 
  , @pHolderFedEntity    Int 
  , @pHolderId1Type      Int -- RFC
  , @pHolderId2Type      Int -- CURP
  , @pHolderId3Type      Int 
  , @pHolderId3           nvarchar(20) 
  , @pHolderAsBeneficiary bit  
  , @pBeneficiaryName     nvarchar(200) 
  , @pBeneficiaryName1    nvarchar(50) 
  , @pBeneficiaryName2    nvarchar(50) 
  , @pBeneficiaryName3    nvarchar(50) 
  , @pBeneficiaryBirthDate Datetime 
  , @pBeneficiaryGender    int 
  , @pBeneficiaryOccupation nvarchar(50) 
  , @pBeneficiaryId1Type    int   -- RFC
  , @pBeneficiaryId1        nvarchar(20) 
  , @pBeneficiaryId2Type    int   -- CURP
  , @pBeneficiaryId2        nvarchar(20) 
  , @pBeneficiaryId3Type    int 
  , @pBeneficiaryId3  	    nvarchar(20)   
  , @pOutputStatus INT               OUTPUT
  , @pOutputAccountOtherId BIGINT    OUTPUT
  AS
  BEGIN

  DECLARE @pOtherAccountId as BIGINT
  DECLARE @IsNewAccount    as BIT
  DECLARE @PreviousUserType as INT
  DECLARE @PreviousPersonalInfoSeqId as BIGINT
  DECLARE @NumAccounts as INT

  SET @pOutputStatus = 0
  SET @pOutputAccountOtherId = 0
  
  SET @pUserType = ISNULL(@pUserType,CASE WHEN ISNULL(@pHolderLevel,0) > 0  THEN 1 ELSE 0 END)
  
  SET @NumAccounts = (SELECT COUNT(AC_ACCOUNT_ID) FROM ACCOUNTS WHERE AC_ACCOUNT_ID <> @pAccountId AND AC_HOLDER_ID = @pHolderId)
  IF @NumAccounts > 0 
  BEGIN
    SET @pOutputStatus = 1
  END

  SET @pOtherAccountId = ISNULL ((SELECT AC_ACCOUNT_ID FROM ACCOUNTS WHERE AC_TRACK_DATA = @pTrackData), 0)
  IF @pOtherAccountId <> 0 AND @pOtherAccountId <> @pAccountId
  BEGIN  
    UPDATE   ACCOUNTS 
       SET   AC_TRACK_DATA = '00000000000000000000-RECYCLED-' + CAST (@pOtherAccountId AS NVARCHAR) 
     WHERE   AC_ACCOUNT_ID = @pOtherAccountId

    SET @pOutputStatus = 2
    SET @pOutputAccountOtherId = @pOtherAccountId
  END

  SELECT @PreviousPersonalInfoSeqId = AC_MS_PERSONAL_INFO_SEQ_ID FROM ACCOUNTS WHERE AC_ACCOUNT_ID = @pAccountId 

  IF (@PreviousPersonalInfoSeqId IS NOT NULL AND @PreviousPersonalInfoSeqId > @pPersonalInfoSeqId)
  BEGIN
    SET @pOutputStatus = 3
    SELECT @pOutputAccountOtherId = ISNULL(AC_MS_LAST_SITE_ID, 0) FROM ACCOUNTS WHERE AC_ACCOUNT_ID = @pAccountId 
  END

  SET @IsNewAccount = 0
  IF NOT EXISTS (SELECT 1 FROM ACCOUNTS WHERE ac_account_id = @pAccountId )  
  BEGIN
    SET @IsNewAccount = 1

    INSERT INTO   ACCOUNTS 
                ( AC_ACCOUNT_ID, AC_TYPE, AC_BLOCKED, AC_MS_LAST_SITE_ID, AC_CREATED) 
         VALUES ( @pAccountId,         2,          1, @pCallingSiteId   , @pAccountCreated)
  END 
  ELSE
  BEGIN  
    SELECT @PreviousUserType = AC_USER_TYPE FROM ACCOUNTS WHERE AC_ACCOUNT_ID = @pAccountId     
    IF (@PreviousUserType = 1 AND @pUserType = 0) 
    BEGIN
      -- TODO: Make Alarm.
      -- SET @pOutputStatus = 4
      
      SET @pUserType = 1
    END
  END
   
  UPDATE   ACCOUNTS                   
     SET   AC_TRACK_DATA              = @pTrackData 
         , AC_HOLDER_NAME             = @pHolderName 
         , AC_HOLDER_ID               = @pHolderId 
         , AC_HOLDER_ID_TYPE          = @pHolderIdType 
         , AC_HOLDER_ADDRESS_01       = @pHolderAddress01 
         , AC_HOLDER_ADDRESS_02       = @pHolderAddress02 
         , AC_HOLDER_ADDRESS_03       = @pHolderAddress03 
         , AC_HOLDER_CITY             = @pHolderCity 
         , AC_HOLDER_ZIP              = @pHolderZip  
         , AC_HOLDER_EMAIL_01         = @pHolderEmail01 
         , AC_HOLDER_EMAIL_02         = @pHolderEmail02 
         , AC_HOLDER_TWITTER_ACCOUNT  = @pHolderTwitter 
         , AC_HOLDER_PHONE_NUMBER_01  = @pHolderPhoneNumber01 
         , AC_HOLDER_PHONE_NUMBER_02  = @pHolderPhoneNumber02 
         , AC_HOLDER_COMMENTS         = @pHolderComments 
         , AC_HOLDER_ID1              = @pHolderId1 
         , AC_HOLDER_ID2              = @pHolderId2 
         , AC_HOLDER_DOCUMENT_ID1     = @pHolderDocumentId1 
         , AC_HOLDER_DOCUMENT_ID2     = @pHolderDocumentId2 
         , AC_HOLDER_NAME1            = @pHolderName1 
         , AC_HOLDER_NAME2            = @pHolderName2 
         , AC_HOLDER_NAME3            = @pHolderName3 
         , AC_HOLDER_GENDER           = @pHolderGender  
         , AC_HOLDER_MARITAL_STATUS   = @pHolderMaritalStatus 
         , AC_HOLDER_BIRTH_DATE       = @pHolderBirthDate 
         , AC_HOLDER_WEDDING_DATE     = @pHolderWeddingDate 
         , AC_HOLDER_LEVEL            = CASE WHEN (@IsNewAccount = 0) AND (AC_USER_TYPE = @pUserType)  THEN AC_HOLDER_LEVEL            ELSE @pHolderLevel           END
         , AC_HOLDER_LEVEL_NOTIFY     = CASE WHEN (@IsNewAccount = 0) AND (AC_USER_TYPE = @pUserType)  THEN AC_HOLDER_LEVEL_NOTIFY     ELSE @pHolderLevelNotify     END 
         , AC_HOLDER_LEVEL_ENTERED    = CASE WHEN (@IsNewAccount = 0) AND (AC_USER_TYPE = @pUserType)  THEN AC_HOLDER_LEVEL_ENTERED    ELSE @pHolderLevelEntered    END  
         , AC_HOLDER_LEVEL_EXPIRATION = CASE WHEN (@IsNewAccount = 0) AND (AC_USER_TYPE = @pUserType)  THEN AC_HOLDER_LEVEL_EXPIRATION ELSE @pHolderLevelExpiration END   
         , AC_PIN                     = @pPin 
         , AC_PIN_FAILURES            = @pPinFailures 
         , AC_PIN_LAST_MODIFIED       = @pPinLastModified 
         , AC_BLOCKED                 = @pBlocked 
         , AC_ACTIVATED               = @pActivated 
         , AC_BLOCK_REASON            = @pBlockReason 
         , AC_HOLDER_IS_VIP           = @pHolderIsVip 
         , AC_HOLDER_TITLE            = @pHolderTitle 
         , AC_HOLDER_NAME4            = @pHolderName4         
         , AC_HOLDER_PHONE_TYPE_01    = @pHolderPhoneType01  
         , AC_HOLDER_PHONE_TYPE_02    = @pHolderPhoneType02  
         , AC_HOLDER_STATE            = @pHolderState    
         , AC_HOLDER_COUNTRY          = @pHolderCountry   
         , AC_USER_TYPE               = @pUserType
         , AC_POINTS_STATUS           = CASE WHEN (@pUserType = 0) THEN NULL ELSE CASE WHEN (@IsNewAccount = 0) THEN @pPointsStatus ELSE 0 END END           
         , AC_DEPOSIT                 = @pDeposit 
         , AC_CARD_PAID               = @pCardPay 
         , AC_BLOCK_DESCRIPTION       = @pBlockDescription          
         , AC_MS_CREATED_ON_SITE_ID   = ISNULL(AC_MS_CREATED_ON_SITE_ID, ISNULL(@pMSCreatedOnSiteId, @pCallingSiteId))
         , AC_EXTERNAL_REFERENCE      = ISNULL(@pExternalReference,AC_EXTERNAL_REFERENCE)
         , AC_HOLDER_OCCUPATION       = @pHolderOccupation
         , AC_HOLDER_EXT_NUM          = @pHolderExtNum
         , AC_HOLDER_NATIONALITY      = @pHolderNationality 
         , AC_HOLDER_BIRTH_COUNTRY    = @pHolderBirthCountry 
         , AC_HOLDER_FED_ENTITY       = @pHolderFedEntity
         , AC_HOLDER_ID1_TYPE         = @pHolderId1Type 
         , AC_HOLDER_ID2_TYPE         = @pHolderId2Type 
         , AC_HOLDER_ID3_TYPE         = @pHolderId3Type 
         , AC_HOLDER_ID3              = @pHolderId3
         , AC_HOLDER_AS_BENEFICIARY   = @pHolderAsBeneficiary 
         , AC_BENEFICIARY_NAME        = @pBeneficiaryName 
         , AC_BENEFICIARY_NAME1       = @pBeneficiaryName1
         , AC_BENEFICIARY_NAME2       = @pBeneficiaryName2
         , AC_BENEFICIARY_NAME3       = @pBeneficiaryName3
         , AC_BENEFICIARY_BIRTH_DATE  = @pBeneficiaryBirthDate 
         , AC_BENEFICIARY_GENDER      = @pBeneficiaryGender 
         , AC_BENEFICIARY_OCCUPATION  = @pBeneficiaryOccupation
         , AC_BENEFICIARY_ID1_TYPE    = @pBeneficiaryId1Type 
         , AC_BENEFICIARY_ID1         = @pBeneficiaryId1 
         , AC_BENEFICIARY_ID2_TYPE    = @pBeneficiaryId2Type 
         , AC_BENEFICIARY_ID2         = @pBeneficiaryId2 
         , AC_BENEFICIARY_ID3_TYPE    = @pBeneficiaryId3Type 
         , AC_BENEFICIARY_ID3         = @pBeneficiaryId3 
   WHERE   AC_ACCOUNT_ID              = @pAccountId 
       
END
GO
   --------------------------------------------------------------------------------
  -- Copyright � 2013 Win Systems International
  --------------------------------------------------------------------------------
  --
  --   MODULE NAME: Update_PersonalInfoForOldVersion.
  --
  --   DESCRIPTION: Update personal Information for old version
  --
  --        AUTHOR: Dani Dom�nguez
  --
  -- CREATION DATE: 03-Jul-2013
  --
  -- REVISION HISTORY:
  --
  -- Date        Author Description
  -- ----------- ------ ----------------------------------------------------------
  -- 03-Jul-2013 DDM    First release.  
  -------------------------------------------------------------------------------- 

  IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Update_PersonalInfoForOldVersion]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Update_PersonalInfoForOldVersion]
  GO
  CREATE PROCEDURE [dbo].[Update_PersonalInfoForOldVersion]
    @pCallingSiteId int
  , @pAccountId bigint
  , @pTrackData nvarchar(50)
  , @pAccountCreated Datetime 
  , @pHolderName nvarchar(200)
  , @pHolderId nvarchar(20)
  , @pHolderIdType int
  , @pHolderAddress01 nvarchar(50)
  , @pHolderAddress02 nvarchar(50)
  , @pHolderAddress03 nvarchar(50)
  , @pHolderCity nvarchar(50)
  , @pHolderZip  nvarchar(10) 
  , @pHolderEmail01 nvarchar(50)
  , @pHolderEmail02 nvarchar(50)
  , @pHolderTwitter nvarchar(50)
  , @pHolderPhoneNumber01 nvarchar(20)
  , @pHolderPhoneNumber02 nvarchar(20)
  , @pHolderComments nvarchar(100)
  , @pHolderId1 nvarchar(20)
  , @pHolderId2 nvarchar(20)
  , @pHolderDocumentId1 bigint
  , @pHolderDocumentId2 bigint
  , @pHolderName1 nvarchar(50)
  , @pHolderName2 nvarchar(50)
  , @pHolderName3 nvarchar(50)
  , @pHolderGender  int
  , @pHolderMaritalStatus int
  , @pHolderBirthDate datetime
  , @pHolderWeddingDate datetime
  , @pHolderLevel int
  , @pHolderLevelNotify int
  , @pHolderLevelEntered datetime
  , @pHolderLevelExpiration datetime
  , @pPin nvarchar(12)
  , @pPinFailures int
  , @pPinLastModified datetime
  , @pBlocked bit
  , @pActivated bit
  , @pBlockReason int
  , @pHolderIsVip int
  , @pHolderTitle nvarchar(15)                       
  , @pHolderName4 nvarchar(50)                       
  , @pHolderPhoneType01  int
  , @pHolderPhoneType02  int
  , @pHolderState    nvarchar(50)                    
  , @pHolderCountry  nvarchar(50) 
  , @pUserType  int
  , @pPersonalInfoSeqId  int
  , @pPointsStatus int
  , @pDeposit money
  , @pCardPay bit
  , @pBlockDescription nvarchar(256) 
  , @pMSCreatedOnSiteId Int 
  , @pExternalReference nvarchar(50) 
  , @pOutputStatus INT               OUTPUT
  , @pOutputAccountOtherId BIGINT    OUTPUT
  AS
  BEGIN

  DECLARE @pOtherAccountId as BIGINT
  DECLARE @IsNewAccount    as BIT
  DECLARE @PreviousUserType as INT
  DECLARE @PreviousPersonalInfoSeqId as BIGINT
  DECLARE @NumAccounts as INT

  SET @pOutputStatus = 0
  SET @pOutputAccountOtherId = 0
  
  SET @pUserType = ISNULL(@pUserType,CASE WHEN ISNULL(@pHolderLevel,0) > 0  THEN 1 ELSE 0 END)
  
  SET @NumAccounts = (SELECT COUNT(AC_ACCOUNT_ID) FROM ACCOUNTS WHERE AC_ACCOUNT_ID <> @pAccountId AND AC_HOLDER_ID = @pHolderId)
  IF @NumAccounts > 0 
  BEGIN
    SET @pOutputStatus = 1
  END

  SET @pOtherAccountId = ISNULL ((SELECT AC_ACCOUNT_ID FROM ACCOUNTS WHERE AC_TRACK_DATA = @pTrackData), 0)
  IF @pOtherAccountId <> 0 AND @pOtherAccountId <> @pAccountId
  BEGIN  
    UPDATE   ACCOUNTS 
       SET   AC_TRACK_DATA = '00000000000000000000-RECYCLED-' + CAST (@pOtherAccountId AS NVARCHAR) 
     WHERE   AC_ACCOUNT_ID = @pOtherAccountId

    SET @pOutputStatus = 2
    SET @pOutputAccountOtherId = @pOtherAccountId
  END

  SELECT @PreviousPersonalInfoSeqId = AC_MS_PERSONAL_INFO_SEQ_ID FROM ACCOUNTS WHERE AC_ACCOUNT_ID = @pAccountId 

  IF (@PreviousPersonalInfoSeqId IS NOT NULL AND @PreviousPersonalInfoSeqId > @pPersonalInfoSeqId)
  BEGIN
    SET @pOutputStatus = 3
    SELECT @pOutputAccountOtherId = ISNULL(AC_MS_LAST_SITE_ID, 0) FROM ACCOUNTS WHERE AC_ACCOUNT_ID = @pAccountId 
  END

  SET @IsNewAccount = 0
  IF NOT EXISTS (SELECT 1 FROM ACCOUNTS WHERE ac_account_id = @pAccountId )  
  BEGIN
    SET @IsNewAccount = 1

    INSERT INTO   ACCOUNTS 
                ( AC_ACCOUNT_ID, AC_TYPE, AC_BLOCKED, AC_MS_LAST_SITE_ID, AC_CREATED) 
         VALUES ( @pAccountId,         2,          1, @pCallingSiteId   , @pAccountCreated)
  END 
  ELSE
  BEGIN  
    SELECT @PreviousUserType = AC_USER_TYPE FROM ACCOUNTS WHERE AC_ACCOUNT_ID = @pAccountId     
    IF (@PreviousUserType = 1 AND @pUserType = 0) 
    BEGIN
      -- TODO: Make Alarm.
      -- SET @pOutputStatus = 4
      
      SET @pUserType = 1
    END
  END
   
  UPDATE   ACCOUNTS                   
     SET   AC_TRACK_DATA              = @pTrackData 
         , AC_HOLDER_NAME             = @pHolderName 
         , AC_HOLDER_ID               = @pHolderId 
         , AC_HOLDER_ID_TYPE          = @pHolderIdType 
         , AC_HOLDER_ADDRESS_01       = @pHolderAddress01 
         , AC_HOLDER_ADDRESS_02       = @pHolderAddress02 
         , AC_HOLDER_ADDRESS_03       = @pHolderAddress03 
         , AC_HOLDER_CITY             = @pHolderCity 
         , AC_HOLDER_ZIP              = @pHolderZip  
         , AC_HOLDER_EMAIL_01         = @pHolderEmail01 
         , AC_HOLDER_EMAIL_02         = @pHolderEmail02 
         , AC_HOLDER_TWITTER_ACCOUNT  = @pHolderTwitter 
         , AC_HOLDER_PHONE_NUMBER_01  = @pHolderPhoneNumber01 
         , AC_HOLDER_PHONE_NUMBER_02  = @pHolderPhoneNumber02 
         , AC_HOLDER_COMMENTS         = @pHolderComments 
         , AC_HOLDER_ID1              = @pHolderId1 
         , AC_HOLDER_ID2              = @pHolderId2 
         , AC_HOLDER_DOCUMENT_ID1     = @pHolderDocumentId1 
         , AC_HOLDER_DOCUMENT_ID2     = @pHolderDocumentId2 
         , AC_HOLDER_NAME1            = @pHolderName1 
         , AC_HOLDER_NAME2            = @pHolderName2 
         , AC_HOLDER_NAME3            = @pHolderName3 
         , AC_HOLDER_GENDER           = @pHolderGender  
         , AC_HOLDER_MARITAL_STATUS   = @pHolderMaritalStatus 
         , AC_HOLDER_BIRTH_DATE       = @pHolderBirthDate 
         , AC_HOLDER_WEDDING_DATE     = @pHolderWeddingDate 
         , AC_HOLDER_LEVEL            = CASE WHEN (@IsNewAccount = 0) AND (AC_USER_TYPE = @pUserType)  THEN AC_HOLDER_LEVEL            ELSE @pHolderLevel           END
         , AC_HOLDER_LEVEL_NOTIFY     = CASE WHEN (@IsNewAccount = 0) AND (AC_USER_TYPE = @pUserType)  THEN AC_HOLDER_LEVEL_NOTIFY     ELSE @pHolderLevelNotify     END 
         , AC_HOLDER_LEVEL_ENTERED    = CASE WHEN (@IsNewAccount = 0) AND (AC_USER_TYPE = @pUserType)  THEN AC_HOLDER_LEVEL_ENTERED    ELSE @pHolderLevelEntered    END  
         , AC_HOLDER_LEVEL_EXPIRATION = CASE WHEN (@IsNewAccount = 0) AND (AC_USER_TYPE = @pUserType)  THEN AC_HOLDER_LEVEL_EXPIRATION ELSE @pHolderLevelExpiration END   
         , AC_PIN                     = @pPin 
         , AC_PIN_FAILURES            = @pPinFailures 
         , AC_PIN_LAST_MODIFIED       = @pPinLastModified 
         , AC_BLOCKED                 = @pBlocked 
         , AC_ACTIVATED               = @pActivated 
         , AC_BLOCK_REASON            = @pBlockReason 
         , AC_HOLDER_IS_VIP           = @pHolderIsVip 
         , AC_HOLDER_TITLE            = @pHolderTitle 
         , AC_HOLDER_NAME4            = @pHolderName4         
         , AC_HOLDER_PHONE_TYPE_01    = @pHolderPhoneType01  
         , AC_HOLDER_PHONE_TYPE_02    = @pHolderPhoneType02  
         , AC_HOLDER_STATE            = @pHolderState    
         , AC_HOLDER_COUNTRY          = @pHolderCountry   
         , AC_USER_TYPE               = @pUserType
         , AC_POINTS_STATUS           = CASE WHEN (@pUserType = 0) THEN NULL ELSE CASE WHEN (@IsNewAccount = 0) THEN @pPointsStatus ELSE 0 END END           
         , AC_DEPOSIT                 = @pDeposit 
         , AC_CARD_PAID               = @pCardPay 
         , AC_BLOCK_DESCRIPTION       = @pBlockDescription          
         , AC_MS_CREATED_ON_SITE_ID   = ISNULL(AC_MS_CREATED_ON_SITE_ID, ISNULL(@pMSCreatedOnSiteId, @pCallingSiteId))
         , AC_EXTERNAL_REFERENCE      = ISNULL(@pExternalReference,AC_EXTERNAL_REFERENCE)
   WHERE   AC_ACCOUNT_ID              = @pAccountId 
       
END

/****** TRIGGERS ******/
--05-07-2013
GO
--------------------------------------------------------------------------------
-- Copyright � 2013 Win Systems International 
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: MultiSiteTrigger_CenterAccountDocument
-- 
--   DESCRIPTION: Trigger MultiSiteTrigger_CenterAccountDocument for increasing the sequence
-- 
--        AUTHOR: Jos� Mart�nez L�pez
-- 
-- CREATION DATE: 02-JUL-2013
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 02-JUL-2013 JML    First release.
--------------------------------------------------------------------------------  

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[MultiSiteTrigger_CenterAccountDocument]') AND type in (N'TR'))
DROP TRIGGER [dbo].[MultiSiteTrigger_CenterAccountDocument]
GO

CREATE  TRIGGER [dbo].[MultiSiteTrigger_CenterAccountDocument] ON [dbo].[ACCOUNT_DOCUMENTS]
AFTER INSERT, UPDATE
NOT FOR REPLICATION
AS 
BEGIN
    DECLARE @Sequence15Value AS BIGINT
    DECLARE @AccountId       AS INT

    DECLARE InsertedCursor CURSOR FOR 
     SELECT   INSERTED.AD_ACCOUNT_ID
       FROM   INSERTED 

    SET NOCOUNT ON;

    OPEN InsertedCursor

    FETCH NEXT FROM InsertedCursor INTO @AccountId
    
    WHILE @@FETCH_STATUS = 0
    BEGIN
    
        -- Account Documents
        UPDATE   SEQUENCES 
           SET   SEQ_NEXT_VALUE = SEQ_NEXT_VALUE + 1
         WHERE   SEQ_ID         = 15

        SELECT   @Sequence15Value = SEQ_NEXT_VALUE - 1 FROM SEQUENCES WHERE SEQ_ID = 15

        UPDATE   ACCOUNT_DOCUMENTS
           SET   AD_MS_SEQUENCE_ID = @Sequence15Value
         WHERE   AD_ACCOUNT_ID     = @AccountId

        FETCH NEXT FROM InsertedCursor INTO @AccountId
    END

    CLOSE InsertedCursor
    DEALLOCATE InsertedCursor

END   -- MultiSiteTrigger_CenterAccountDocument

--03-07-2013
GO

--------------------------------------------------------------------------------
-- Copyright � 2013 Win Systems International 
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: MultiSiteTrigger_AccountUpdate.sql
-- 
--   DESCRIPTION: Procedures for trigger MultiSiteTrigger_AccountUpdate and related issues
-- 
--        AUTHOR: Jos� Mart�nez
-- 
-- CREATION DATE: 07-MAR-2013
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 07-MAR-2013 JML    First release.
--------------------------------------------------------------------------------
GO
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[MultiSiteTrigger_AccountUpdate]') AND type in (N'TR'))
DROP TRIGGER [dbo].[MultiSiteTrigger_AccountUpdate]
GO


CREATE TRIGGER [dbo].[MultiSiteTrigger_AccountUpdate] ON [dbo].[accounts]
   AFTER UPDATE
NOT FOR REPLICATION
AS 
BEGIN
    DECLARE @Sequence11Value    AS BIGINT
    DECLARE @Sequence10Value    AS BIGINT
    DECLARE @AccountId          AS BIGINT
    DECLARE @hash0              as varbinary(20)
    DECLARE @hash1              as varbinary(20)
    DECLARE @value              as nvarchar(max)
    DECLARE @changed            as bit
    DECLARE @updated            as bit
    
    SET @updated = 0;        
            
    IF (UPDATE(AC_MS_CHANGE_GUID)) RETURN
 
    IF (UPDATE(AC_POINTS))
      BEGIN
          DECLARE PointsCursor CURSOR FOR 
           SELECT   INSERTED.AC_ACCOUNT_ID 
             FROM   INSERTED, DELETED 
            WHERE   INSERTED.AC_ACCOUNT_ID    =  DELETED.AC_ACCOUNT_ID
              AND ( INSERTED.AC_POINTS        <> DELETED.AC_POINTS )
               
        SET NOCOUNT ON;

        OPEN PointsCursor

        FETCH NEXT FROM PointsCursor INTO @AccountId
          
        WHILE @@FETCH_STATUS = 0
        BEGIN
            UPDATE   SEQUENCES 
               SET   SEQ_NEXT_VALUE = SEQ_NEXT_VALUE + 1
             WHERE   SEQ_ID         = 11

            SELECT   @Sequence11Value = SEQ_NEXT_VALUE - 1 FROM SEQUENCES WHERE SEQ_ID = 11

            UPDATE   ACCOUNTS
               SET   AC_MS_POINTS_SEQ_ID = @Sequence11Value 
             WHERE   AC_ACCOUNT_ID       = @AccountId

            FETCH NEXT FROM PointsCursor INTO @AccountId
        END

        CLOSE PointsCursor
        DEALLOCATE PointsCursor
    END


    IF UPDATE (AC_TRACK_DATA)
    OR UPDATE (AC_HOLDER_NAME)
    OR UPDATE (AC_HOLDER_ID)
    OR UPDATE (AC_HOLDER_ID_TYPE)
    OR UPDATE (AC_HOLDER_ADDRESS_01)
    OR UPDATE (AC_HOLDER_ADDRESS_02)
    OR UPDATE (AC_HOLDER_ADDRESS_03)
    OR UPDATE (AC_HOLDER_CITY)
    OR UPDATE (AC_HOLDER_ZIP)
    OR UPDATE (AC_HOLDER_EMAIL_01)
    OR UPDATE (AC_HOLDER_EMAIL_02)
    OR UPDATE (AC_HOLDER_TWITTER_ACCOUNT)
    OR UPDATE (AC_HOLDER_PHONE_NUMBER_01)
    OR UPDATE (AC_HOLDER_PHONE_NUMBER_02)
    OR UPDATE (AC_HOLDER_COMMENTS)
    OR UPDATE (AC_HOLDER_ID1)
    OR UPDATE (AC_HOLDER_ID2)
    OR UPDATE (AC_HOLDER_DOCUMENT_ID1)
    OR UPDATE (AC_HOLDER_DOCUMENT_ID2)
    OR UPDATE (AC_HOLDER_NAME1)
    OR UPDATE (AC_HOLDER_NAME2)
    OR UPDATE (AC_HOLDER_NAME3)
    OR UPDATE (AC_HOLDER_GENDER)
    OR UPDATE (AC_HOLDER_MARITAL_STATUS)
    OR UPDATE (AC_HOLDER_BIRTH_DATE)
    OR UPDATE (AC_HOLDER_WEDDING_DATE)
    OR UPDATE (AC_HOLDER_LEVEL)
    OR UPDATE (AC_HOLDER_LEVEL_NOTIFY)
    OR UPDATE (AC_HOLDER_LEVEL_ENTERED)
    OR UPDATE (AC_HOLDER_LEVEL_EXPIRATION)
    OR UPDATE (AC_PIN)
    OR UPDATE (AC_PIN_FAILURES)
    OR UPDATE (AC_PIN_LAST_MODIFIED)
    OR UPDATE (AC_BLOCKED)
    OR UPDATE (AC_ACTIVATED)
    OR UPDATE (AC_BLOCK_REASON)
    OR UPDATE (AC_HOLDER_IS_VIP)
    OR UPDATE (AC_HOLDER_TITLE)
    OR UPDATE (AC_HOLDER_NAME4)
    OR UPDATE (AC_HOLDER_PHONE_TYPE_01)
    OR UPDATE (AC_HOLDER_PHONE_TYPE_02)
    OR UPDATE (AC_HOLDER_STATE)
    OR UPDATE (AC_HOLDER_COUNTRY)
    OR UPDATE (AC_USER_TYPE)
    OR UPDATE (AC_POINTS_STATUS)
    OR UPDATE (AC_DEPOSIT)
    OR UPDATE (AC_CARD_PAID)
    OR UPDATE (AC_BLOCK_DESCRIPTION)    
    OR UPDATE (AC_MS_CREATED_ON_SITE_ID)
    OR UPDATE (AC_EXTERNAL_REFERENCE)
    OR UPDATE (AC_HOLDER_OCCUPATION)     
    OR UPDATE (AC_HOLDER_EXT_NUM)       
    OR UPDATE (AC_HOLDER_NATIONALITY)
    OR UPDATE (AC_HOLDER_BIRTH_COUNTRY)
    OR UPDATE (AC_HOLDER_FED_ENTITY)
    OR UPDATE (AC_HOLDER_ID1_TYPE)
    OR UPDATE (AC_HOLDER_ID2_TYPE)
    OR UPDATE (AC_HOLDER_ID3_TYPE)
    OR UPDATE (AC_HOLDER_ID3)
    OR UPDATE (AC_HOLDER_AS_BENEFICIARY)
    OR UPDATE (AC_BENEFICIARY_NAME)
    OR UPDATE (AC_BENEFICIARY_NAME1)
    OR UPDATE (AC_BENEFICIARY_NAME2)
    OR UPDATE (AC_BENEFICIARY_NAME3)
    OR UPDATE (AC_BENEFICIARY_BIRTH_DATE)
    OR UPDATE (AC_BENEFICIARY_GENDER)
    OR UPDATE (AC_BENEFICIARY_OCCUPATION)
    OR UPDATE (AC_BENEFICIARY_ID1_TYPE)
    OR UPDATE (AC_BENEFICIARY_ID1)
    OR UPDATE (AC_BENEFICIARY_ID2_TYPE)
    OR UPDATE (AC_BENEFICIARY_ID2)
    OR UPDATE (AC_BENEFICIARY_ID3_TYPE)
	  OR UPDATE (AC_BENEFICIARY_ID3)
        SET @updated = 1;
        
    IF (@updated = 0) RETURN

    DECLARE PersonalInfoCursor CURSOR FOR 
     SELECT   AC_ACCOUNT_ID
            , HASHBYTES ('SHA1',  ISNULL(AC_TRACK_DATA,        '')
                                + ISNULL(AC_HOLDER_NAME,       '')
                                + ISNULL(AC_HOLDER_ID,         '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_ID_TYPE), '')
                                + ISNULL(AC_HOLDER_ADDRESS_01, '')
                                + ISNULL(AC_HOLDER_ADDRESS_02, '')
                                + ISNULL(AC_HOLDER_ADDRESS_03, '')
                                + ISNULL(AC_HOLDER_CITY,       '')
                                + ISNULL(AC_HOLDER_ZIP,        '')
                                + ISNULL(AC_HOLDER_EMAIL_01,   '')
                                + ISNULL(AC_HOLDER_EMAIL_02,   '')
                                + ISNULL(AC_HOLDER_TWITTER_ACCOUNT, '')
                                + ISNULL(AC_HOLDER_PHONE_NUMBER_01, '')
                                + ISNULL(AC_HOLDER_PHONE_NUMBER_02, '')
                                + ISNULL(AC_HOLDER_COMMENTS,        '')
                                + ISNULL(AC_HOLDER_ID1, '')
                                + ISNULL(AC_HOLDER_ID2, '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_DOCUMENT_ID1), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_DOCUMENT_ID2), '')
                                + ISNULL(AC_HOLDER_NAME1, '')
                                + ISNULL(AC_HOLDER_NAME2, '')
                                + ISNULL(AC_HOLDER_NAME3, '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_GENDER),         '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_MARITAL_STATUS), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_BIRTH_DATE,   21), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_WEDDING_DATE, 21), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_LEVEL),                '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_LEVEL_NOTIFY),         '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_LEVEL_ENTERED,    21), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_LEVEL_EXPIRATION, 21), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_PIN),          '')
                                + ISNULL(CONVERT(NVARCHAR, AC_PIN_FAILURES), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_PIN_LAST_MODIFIED, 21), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_BLOCKED),           '')
                                + ISNULL(CONVERT(NVARCHAR, AC_ACTIVATED),         '')
                                + ISNULL(CONVERT(NVARCHAR, AC_BLOCK_REASON),         '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_IS_VIP),        '')
                                + ISNULL(AC_HOLDER_TITLE                        , '')
                                + ISNULL(AC_HOLDER_NAME4                        , '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_PHONE_TYPE_01),        '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_PHONE_TYPE_02),        '')
                                + ISNULL(AC_HOLDER_STATE                        , '')
                                + ISNULL(AC_HOLDER_COUNTRY                      , '')
                                + ISNULL(CONVERT(NVARCHAR,AC_USER_TYPE)         , '') 
                                + ISNULL(CONVERT(NVARCHAR,AC_POINTS_STATUS)     , '') 
                                + ISNULL(CONVERT(NVARCHAR,AC_DEPOSIT)           , '')
                                + ISNULL(CONVERT(NVARCHAR,AC_CARD_PAID)         , '')
                                + ISNULL(AC_BLOCK_DESCRIPTION, '')                                                                  
                                + ISNULL(CONVERT(NVARCHAR,AC_MS_CREATED_ON_SITE_ID), '')
                                + ISNULL(AC_EXTERNAL_REFERENCE, '')
                                + ISNULL(AC_HOLDER_OCCUPATION, '')
                                + ISNULL(AC_HOLDER_EXT_NUM, '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_NATIONALITY),         '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_BIRTH_COUNTRY), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_ID1_TYPE), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_ID2_TYPE), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_ID3_TYPE), '')
                                + ISNULL(AC_HOLDER_ID3, '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_AS_BENEFICIARY), '')
                                + ISNULL(AC_BENEFICIARY_NAME, '')
                                + ISNULL(AC_BENEFICIARY_NAME1, '')
                                + ISNULL(AC_BENEFICIARY_NAME2, '')
                                + ISNULL(AC_BENEFICIARY_NAME3, '')
                                + ISNULL(CONVERT(NVARCHAR, AC_BENEFICIARY_BIRTH_DATE,   21), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_BENEFICIARY_GENDER), '')	
                                + ISNULL(AC_BENEFICIARY_OCCUPATION, '')
                                + ISNULL(CONVERT(NVARCHAR, AC_BENEFICIARY_ID1_TYPE), '')				
                                + ISNULL(AC_BENEFICIARY_ID1, '')
                                + ISNULL(CONVERT(NVARCHAR, AC_BENEFICIARY_ID2_TYPE), '')				
                                + ISNULL(AC_BENEFICIARY_ID2, '')
                                + ISNULL(CONVERT(NVARCHAR, AC_BENEFICIARY_ID3_TYPE), '')				
                                + ISNULL(AC_BENEFICIARY_ID3, '') )
       FROM   INSERTED

    SET NOCOUNT ON;

    OPEN PersonalInfoCursor

    FETCH NEXT FROM PersonalInfoCursor INTO @AccountId, @hash1

    WHILE @@FETCH_STATUS = 0
    BEGIN

        SELECT @hash0 = AC_MS_HASH FROM DELETED WHERE AC_ACCOUNT_ID = @AccountId

        SELECT @changed = CASE WHEN ( @hash0 = @hash1 ) THEN 0 ELSE 1 END

        IF @changed = 1
        BEGIN
            -- Personal Info
            UPDATE   SEQUENCES 
               SET   SEQ_NEXT_VALUE = SEQ_NEXT_VALUE + 1
             WHERE   SEQ_ID         = 10

            SELECT   @Sequence10Value = SEQ_NEXT_VALUE - 1 FROM SEQUENCES WHERE SEQ_ID = 10

            UPDATE   ACCOUNTS
               SET   AC_MS_PERSONAL_INFO_SEQ_ID = @Sequence10Value
                   , AC_MS_HASH                 = @hash1
                   , AC_MS_CHANGE_GUID          = NEWID()
             WHERE   AC_ACCOUNT_ID              = @AccountId
        END

        FETCH NEXT FROM PersonalInfoCursor INTO @AccountId, @hash1
    END

    CLOSE PersonalInfoCursor
    DEALLOCATE PersonalInfoCursor

END
GO
--03-07-2013


/****** RECORDS ******/
--MULTISITE
--02-07-2013
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('MoneyLaundering', 'Enabled', '0');
GO
INSERT INTO Federal_States (FS_NAME) VALUES ('Distrito Federal');
INSERT INTO Federal_States (FS_NAME) VALUES ('Aguascalientes');
INSERT INTO Federal_States (FS_NAME) VALUES ('Baja California');
INSERT INTO Federal_States (FS_NAME) VALUES ('Baja California Sur');
INSERT INTO Federal_States (FS_NAME) VALUES ('Campeche');
INSERT INTO Federal_States (FS_NAME) VALUES ('Chiapas');
INSERT INTO Federal_States (FS_NAME) VALUES ('Chihuahua');
INSERT INTO Federal_States (FS_NAME) VALUES ('Coahuila de Zaragoza');
INSERT INTO Federal_States (FS_NAME) VALUES ('Colima');
INSERT INTO Federal_States (FS_NAME) VALUES ('Durango');
INSERT INTO Federal_States (FS_NAME) VALUES ('Guanajuato');
INSERT INTO Federal_States (FS_NAME) VALUES ('Guerrero');
INSERT INTO Federal_States (FS_NAME) VALUES ('Hidalgo');
INSERT INTO Federal_States (FS_NAME) VALUES ('Jalisco');
INSERT INTO Federal_States (FS_NAME) VALUES ('M�xico');
INSERT INTO Federal_States (FS_NAME) VALUES ('Michoac�n de Ocampo');
INSERT INTO Federal_States (FS_NAME) VALUES ('Morelos');
INSERT INTO Federal_States (FS_NAME) VALUES ('Nayarit');
INSERT INTO Federal_States (FS_NAME) VALUES ('Nuevo Le�n');
INSERT INTO Federal_States (FS_NAME) VALUES ('Oaxaca');
INSERT INTO Federal_States (FS_NAME) VALUES ('Puebla');
INSERT INTO Federal_States (FS_NAME) VALUES ('Quer�taro');
INSERT INTO Federal_States (FS_NAME) VALUES ('Quintana Roo');
INSERT INTO Federal_States (FS_NAME) VALUES ('San Luis Potos�');
INSERT INTO Federal_States (FS_NAME) VALUES ('Sinaloa');
INSERT INTO Federal_States (FS_NAME) VALUES ('Sonora');
INSERT INTO Federal_States (FS_NAME) VALUES ('Tabasco');
INSERT INTO Federal_States (FS_NAME) VALUES ('Tamaulipas');
INSERT INTO Federal_States (FS_NAME) VALUES ('Tlaxcala');
INSERT INTO Federal_States (FS_NAME) VALUES ('Veracruz de Ignacio de la Llave');
INSERT INTO Federal_States (FS_NAME) VALUES ('Yucat�n');
INSERT INTO Federal_States (FS_NAME) VALUES ('Zacatecas');
GO
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('1','es','M�XICO','MEXICANA');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('2','es','ESPA�A', 'ESPA�OLA');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('3','es','EE.UU.','ESTADOUNIDENSE');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('4','es','CANAD�', 'CANADIENSE');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('5','es','REPUBLICA DOMINICANA', 'DOMINICANA');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('6','es','COSTA RICA','COSTARRICENSE');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('7','es','GUATEMALA','GUATEMALTECA');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('8','es','PANAM�','PANAME�A');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('9','es','EL SALVADOR','SALVADORE�A');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('10','es','HONDURAS','HONDURE�A');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('11','es','NICARAGUA','NICARAG�ENSE');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('12','es','CUBA','CUBANA');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('13','es','COLOMBIA','COLOMBIANA');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('14','es','VENEZUELA','VENEZOLANA');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('15','es','PER�','PERUANA');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('16','es','ECUADOR','ECUATORIANA');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('17','es','ARGENTINA','ARGENTINA');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('18','es','BOLIVIA','BOLIVIANA');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('19','es','PARAGUAY','PARAGUAYA');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('20','es','URUGUAY','URUGUAYA');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('21','es','CHILE','CHILENA');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('22','es','BRASIL','BRASILE�A');

INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('1','en','MEXICO','MEXICAN');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('2','en','SPAIN', 'SPANISH');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('3','en','USA','AMERICAN');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('4','en','CANADA', 'CANADIAN');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('5','en','DOMINICAN REPUBLIC', 'DOMINICAN');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('6','en','COSTA RICA','COSTA RICAN');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('7','en','GUATEMALA','GUATEMALAN');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('8','en','PANAMA','PANAMANIAN');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('9','en','EL SALVADOR','SALVADOREAN');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('10','en','HONDURAS','HONDURAN');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('11','en','NICARAGUA','NICARAGUAN');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('12','en','CUBA','CUBAN');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('13','en','COLOMBIA','COLOMBIAN');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('14','en','VENEZUELA','VENEZUELAN');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('15','en','PERU','PERUVIAN');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('16','en','ECUADOR','ECUADORIAN');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('17','en','ARGENTINA','ARGENTINIAN');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('18','en','BOLIVIA','BOLIVIAN');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('19','en','PARAGUAY','PARAGUAYAN');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('20','en','URUGUAY','URUGUAYAN');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('21','en','CHILE','CHILEAN');
INSERT INTO countries (CO_COUNTRY_ID, CO_LANGUAGE_ID, CO_NAME, CO_ADJECTIVE) VALUES ('22','en','BRAZIL','BRAZILIAN');
GO
--RemoteDesktop
insert into general_params(gp_group_key,gp_subject_key,gp_key_value) VALUES ('RemoteDesktop','DefaultPort','3389')
GO
--If not exist Provider CASINO is Created.
DECLARE @casino_provider_id AS INT
SET @casino_provider_id = (SELECT p.pv_id FROM providers p WHERE p.pv_name = 'CASINO')
IF @casino_provider_id IS NULL
BEGIN
      INSERT INTO [providers]
                     ([pv_site_id]
                     ,[pv_name]
                     ,[pv_hide]
                     ,[pv_points_multiplier]
                     ,[pv_3gs]
                     ,[pv_site_jackpot]
                     ,[pv_only_redeemable])
            VALUES
                     (0
                     ,'CASINO'
                     ,0
                     ,0
                     ,0
                     ,0
                     ,0)
      SET @casino_provider_id = @@identity


      INSERT INTO [games]
                     ([gm_site_id]
                     ,[gm_pv_id]
                     ,[gm_game_name]
                    )
            VALUES
                     (0
                     ,@casino_provider_id
                     ,'CASINO'
                     )
  
END
GO
--DownloadMastersProfiles
SELECT ST_SITE_ID AS SITE_ID INTO #CURRENT_SITES FROM MS_SITE_TASKS GROUP BY ST_SITE_ID
INSERT INTO MS_SITE_TASKS (ST_TASK_ID, ST_ENABLED, ST_INTERVAL_SECONDS, ST_SITE_ID) SELECT 60, 1, 600, SITE_ID FROM #CURRENT_SITES; -- DownloadMastersProfiles
DROP TABLE #CURRENT_SITES
GO
--
-- INSERT TASKS
--
DECLARE @pSiteId INT
DECLARE cSiteId CURSOR FOR SELECT DISTINCT ST_SITE_ID FROM MS_SITE_TASKS -- WHERE st_site_id > 0

OPEN  cSiteId
FETCH NEXT FROM cSiteId INTO @pSiteId
WHILE @@FETCH_STATUS = 0 
BEGIN
      INSERT INTO MS_SITE_TASKS (ST_SITE_ID, ST_TASK_ID, ST_ENABLED, ST_INTERVAL_SECONDS,ST_MAX_ROWS_TO_UPLOAD)
      VALUES (@pSiteId, 13, 1, 180,1);
      INSERT INTO MS_SITE_TASKS (ST_SITE_ID, ST_TASK_ID, ST_ENABLED, ST_INTERVAL_SECONDS,ST_MAX_ROWS_TO_UPLOAD)
      VALUES (@pSiteId, 43, 1, 600,1);
      FETCH NEXT FROM cSiteId INTO @pSiteId
END
CLOSE cSiteId
DEALLOCATE cSiteId
GO
--
-- INSERT SEQUENCES 
--
INSERT INTO SEQUENCES ( SEQ_ID, SEQ_NEXT_VALUE ) 
               VALUES (     15, 0)

GO			   

