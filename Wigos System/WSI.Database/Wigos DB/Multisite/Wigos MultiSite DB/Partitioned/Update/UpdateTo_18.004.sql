/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_100]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 3;

SET @New_ReleaseId = 4;
SET @New_ScriptName = N'UpdateTo_18.004.sql';
SET @New_Description = N'ADD COLUMNS IN ms_site_tasks,accounts; CREATE INDEX IX_ac_holder_birth_date; UPDATE PROCEDURES Update_PersonalInfo, Update_PointsAccountMovement; UPDATE TRIGGER MultiSiteTrigger_AccountUpdate';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/
ALTER TABLE dbo.ms_site_tasks ADD
                st_max_rows_to_upload int NOT NULL CONSTRAINT DF_ms_site_tasks_st_max_rows_to_upload DEFAULT 100,
                st_start_time int NOT NULL CONSTRAINT DF_ms_site_tasks_st_start_time DEFAULT 0,
				st_force_resynch bit NOT NULL CONSTRAINT DF_ms_site_tasks_st_force_resynch DEFAULT 0
GO
ALTER TABLE dbo.accounts ADD
      ac_holder_twitter_account nvarchar(50) NULL

GO

/****** CONSTRAINTS ******/


/****** INDEXES ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[accounts]') AND name = N'IX_ac_holder_birth_date')
DROP INDEX [IX_ac_holder_birth_date] ON [dbo].[accounts] WITH ( ONLINE = OFF )
GO
CREATE NONCLUSTERED INDEX [IX_ac_holder_birth_date] ON [dbo].[accounts] 
(
	[ac_holder_birth_date] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

/****** STORED PROCEDURES ******/

  --------------------------------------------------------------------------------
  -- Copyright � 2013 Win Systems International
  --------------------------------------------------------------------------------
  --
  --   MODULE NAME: Update_PersonalInfo.sql.sql
  --
  --   DESCRIPTION: Update personal Information 
  --
  --        AUTHOR: Dani Dom�nguez
  --
  -- CREATION DATE: 08-MAR-2013
  --
  -- REVISION HISTORY:
  --
  -- Date        Author Description
  -- ----------- ------ ----------------------------------------------------------
  -- 08-MAR-2013 DDM    First release.  
  -------------------------------------------------------------------------------- 

ALTER PROCEDURE [dbo].[Update_PersonalInfo]
    @pCallingSiteId int
  , @pAccountId bigint
  , @pTrackData nvarchar(50)
  , @pAccountCreated Datetime 
  , @pHolderName nvarchar(200)
  , @pHolderId nvarchar(20)
  , @pHolderIdType int
  , @pHolderAddress01 nvarchar(50)
  , @pHolderAddress02 nvarchar(50)
  , @pHolderAddress03 nvarchar(50)
  , @pHolderCity nvarchar(50)
  , @pHolderZip  nvarchar(10) 
  , @pHolderEmail01 nvarchar(50)
  , @pHolderEmail02 nvarchar(50)
  , @pHolderTwitter nvarchar(50)
  , @pHolderPhoneNumber01 nvarchar(20)
  , @pHolderPhoneNumber02 nvarchar(20)
  , @pHolderComments nvarchar(100)
  , @pHolderId1 nvarchar(20)
  , @pHolderId2 nvarchar(20)
  , @pHolderDocumentId1 bigint
  , @pHolderDocumentId2 bigint
  , @pHolderName1 nvarchar(50)
  , @pHolderName2 nvarchar(50)
  , @pHolderName3 nvarchar(50)
  , @pHolderGender  int
  , @pHolderMaritalStatus int
  , @pHolderBirthDate datetime
  , @pHolderWeddingDate datetime
  , @pHolderLevel int
  , @pHolderLevelNotify int
  , @pHolderLevelEntered datetime
  , @pHolderLevelExpiration datetime
  , @pPin nvarchar(12)
  , @pPinFailures int
  , @pPinLastModified datetime
  , @pBlocked bit
  , @pActivated bit
  , @pBlockReason int
  , @pHolderIsVip int
  , @pHolderTitle nvarchar(15)                       
  , @pHolderName4 nvarchar(50)                       
  , @pHolderPhoneType01  int
  , @pHolderPhoneType02  int
  , @pHolderState    nvarchar                    
  , @pHolderCountry  nvarchar 
  , @pUserType  int
  , @pPersonalInfoSeqId  int
  , @pOutputStatus INT               OUTPUT
  , @pOutputAccountOtherId BIGINT    OUTPUT
  AS
  BEGIN

  DECLARE @pOtherAccountId as BIGINT
  DECLARE @IsNewAccount    as BIT
  DECLARE @PreviousUserType as INT
  DECLARE @PreviousPersonalInfoSeqId as BIGINT

  SET @pOutputStatus = 0
  SET @pOutputAccountOtherId = 0

  SET @pOtherAccountId = ISNULL ((SELECT AC_ACCOUNT_ID FROM ACCOUNTS WHERE AC_HOLDER_ID_TYPE = @pHolderIdType AND AC_HOLDER_ID = @pHolderId), 0)
  IF @pOtherAccountId <> 0 AND @pOtherAccountId <> @pAccountId
  BEGIN
    SET @pOutputStatus = 1
    SET @pOutputAccountOtherId = @pOtherAccountId
  END

  SET @pOtherAccountId = ISNULL ((SELECT AC_ACCOUNT_ID FROM ACCOUNTS WHERE AC_TRACK_DATA = @pTrackData), 0)
  IF @pOtherAccountId <> 0 AND @pOtherAccountId <> @pAccountId
  BEGIN  
    UPDATE   ACCOUNTS 
       SET   AC_TRACK_DATA = '00000000000000000000-RECYCLED-' + CAST (@pOtherAccountId AS NVARCHAR) 
     WHERE   AC_ACCOUNT_ID = @pOtherAccountId

    SET @pOutputStatus = 2
    SET @pOutputAccountOtherId = @pOtherAccountId
  END

  SELECT @PreviousPersonalInfoSeqId = AC_MS_PERSONAL_INFO_SEQ_ID FROM ACCOUNTS WHERE AC_ACCOUNT_ID = @pAccountId 

  IF (@PreviousPersonalInfoSeqId IS NOT NULL AND @PreviousPersonalInfoSeqId > @pPersonalInfoSeqId)
  BEGIN
    SET @pOutputStatus = 3
  END

  SET @IsNewAccount = 0
  --  IF NOT EXISTS (SELECT @PreviousUserType = AC_USER_TYPE FROM ACCOUNTS WHERE ac_account_id = @pAccountId )
  SELECT @PreviousUserType = AC_USER_TYPE FROM ACCOUNTS WHERE AC_ACCOUNT_ID = @pAccountId 
  IF (@PreviousUserType IS NULL)  
  BEGIN
    SET @IsNewAccount = 1

    INSERT INTO   ACCOUNTS 
                ( AC_ACCOUNT_ID, AC_TYPE, AC_BLOCKED, AC_MS_CREATED_ON_SITE_ID, AC_MS_LAST_SITE_ID, AC_CREATED) 
         VALUES ( @pAccountId,         2,          1, @pCallingSiteId         , @pCallingSiteId   , @pAccountCreated)
  END 
  ELSE
  BEGIN  
    IF (@PreviousUserType = 1 AND @pUserType = 0) RETURN
  END
   
  UPDATE   ACCOUNTS                   
     SET   AC_TRACK_DATA              = @pTrackData 
         , AC_HOLDER_NAME             = @pHolderName 
         , AC_HOLDER_ID               = @pHolderId 
         , AC_HOLDER_ID_TYPE          = @pHolderIdType 
         , AC_HOLDER_ADDRESS_01       = @pHolderAddress01 
         , AC_HOLDER_ADDRESS_02       = @pHolderAddress02 
         , AC_HOLDER_ADDRESS_03       = @pHolderAddress03 
         , AC_HOLDER_CITY             = @pHolderCity 
         , AC_HOLDER_ZIP              = @pHolderZip  
         , AC_HOLDER_EMAIL_01         = @pHolderEmail01 
         , AC_HOLDER_EMAIL_02         = @pHolderEmail02 
         , AC_HOLDER_TWITTER_ACCOUNT  = @pHolderTwitter 
         , AC_HOLDER_PHONE_NUMBER_01  = @pHolderPhoneNumber01 
         , AC_HOLDER_PHONE_NUMBER_02  = @pHolderPhoneNumber02 
         , AC_HOLDER_COMMENTS         = @pHolderComments 
         , AC_HOLDER_ID1              = @pHolderId1 
         , AC_HOLDER_ID2              = @pHolderId2 
         , AC_HOLDER_DOCUMENT_ID1     = @pHolderDocumentId1 
         , AC_HOLDER_DOCUMENT_ID2     = @pHolderDocumentId2 
         , AC_HOLDER_NAME1            = @pHolderName1 
         , AC_HOLDER_NAME2            = @pHolderName2 
         , AC_HOLDER_NAME3            = @pHolderName3 
         , AC_HOLDER_GENDER           = @pHolderGender  
         , AC_HOLDER_MARITAL_STATUS   = @pHolderMaritalStatus 
         , AC_HOLDER_BIRTH_DATE       = @pHolderBirthDate 
         , AC_HOLDER_WEDDING_DATE     = @pHolderWeddingDate 
         , AC_HOLDER_LEVEL            = CASE WHEN (@IsNewAccount = 0) AND (AC_USER_TYPE = @pUserType)  THEN AC_HOLDER_LEVEL            ELSE @pHolderLevel           END
         , AC_HOLDER_LEVEL_NOTIFY     = CASE WHEN (@IsNewAccount = 0) AND (AC_USER_TYPE = @pUserType)  THEN AC_HOLDER_LEVEL_NOTIFY     ELSE @pHolderLevelNotify     END 
         , AC_HOLDER_LEVEL_ENTERED    = CASE WHEN (@IsNewAccount = 0) AND (AC_USER_TYPE = @pUserType)  THEN AC_HOLDER_LEVEL_ENTERED    ELSE @pHolderLevelEntered    END  
         , AC_HOLDER_LEVEL_EXPIRATION = CASE WHEN (@IsNewAccount = 0) AND (AC_USER_TYPE = @pUserType)  THEN AC_HOLDER_LEVEL_EXPIRATION ELSE @pHolderLevelExpiration END   
         , AC_PIN                     = @pPin 
         , AC_PIN_FAILURES            = @pPinFailures 
         , AC_PIN_LAST_MODIFIED       = @pPinLastModified 
         , AC_BLOCKED                 = @pBlocked 
         , AC_ACTIVATED               = @pActivated 
         , AC_BLOCK_REASON            = @pBlockReason 
         , AC_HOLDER_IS_VIP           = @pHolderIsVip 
         , AC_HOLDER_TITLE            = @pHolderTitle 
         , AC_HOLDER_NAME4            = @pHolderName4         
         , AC_HOLDER_PHONE_TYPE_01    = @pHolderPhoneType01  
         , AC_HOLDER_PHONE_TYPE_02    = @pHolderPhoneType02  
         , AC_HOLDER_STATE            = @pHolderState    
         , AC_HOLDER_COUNTRY          = @pHolderCountry   
         , AC_USER_TYPE               = @pUserType
         , AC_POINTS_STATUS           = CASE WHEN (@pUserType = 0) THEN NULL ELSE CASE WHEN (@IsNewAccount = 0) THEN AC_POINTS_STATUS ELSE 0 END END           
   WHERE   AC_ACCOUNT_ID              = @pAccountId 
    
END
GO

--------------------------------------------------------------------------------
-- Copyright � 2013 Win Systems International
--------------------------------------------------------------------------------
--
--   MODULE NAME: Update_PointsAccountMovement.sql
--
--   DESCRIPTION: Update points to account & movement procedure
--
--        AUTHOR: Dani Dom�nguez
--
-- CREATION DATE: 07-MAR-2013
--
-- REVISION HISTORY:
--
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 07-MAR-2013 DDM    First release.
-- 07-MAR-2013 DDM    Add in/out parameters
--------------------------------------------------------------------------------


--------------------------------------------------------------------------------
-- PURPOSE: Update points to account & movement procedure
--
--  PARAMS:
--      - INPUT:
--           @pSiteId          
--           @pMovementId      
--           @pPlaySessionId   
--           @pAccountId       
--           @pTerminalId      
--           @pWcpSequenceId   
--           @pWcpTransactionId
--           @pDatetime        
--           @pType            
--           @pInitialBalance  
--           @pSubAmount       
--           @pAddAmount       
--           @pFinalBalance    
--           @pCashierId       
--           @pCashierName     
--           @pTperminalName   
--           @pOperationId     
--           @pDetails         
--           @pReasons         
--
--      - OUTPUT:
--           @pErrorCode           
--           @PointsSequenceId     
--           @Points               
--           @PointsStatus         
--           @HolderLevel          
--           @HolderLevelEntered   
--           @HolderLevelExpiration
--
-- RETURNS:
--
--   NOTES:
--
--------------------------------------------------------------------------------
ALTER PROCEDURE [dbo].[Update_PointsAccountMovement]
	@pSiteId                    INT 
,	@pMovementId                BIGINT
,	@pPlaySessionId             BIGINT
,	@pAccountId                 BIGINT
,	@pTerminalId                INT
,	@pWcpSequenceId             BIGINT
,	@pWcpTransactionId          BIGINT
,	@pDatetime                  DATETIME
,	@pType                      INT
,	@pInitialBalance            MONEY
,	@pSubAmount                 MONEY
,	@pAddAmount                 MONEY
,	@pFinalBalance              MONEY
,	@pCashierId                 INT
,	@pCashierName               NVARCHAR(50)
,	@pTperminalName             NVARCHAR(50)
,	@pOperationId               BIGINT
,	@pDetails                   NVARCHAR(256)
,	@pReasons                   NVARCHAR(64)
, @pErrorCode                 INT          OUTPUT
, @PointsSequenceId           BIGINT       OUTPUT
, @Points                     MONEY        OUTPUT

AS
BEGIN   
  DECLARE @LocalDelta AS MONEY
  DECLARE @IsDelta    AS BIT

  SET @pErrorCode = 1
  SET @PointsSequenceId = NULL
  SET @Points = null
  SET @IsDelta = 1
 
  IF NOT EXISTS (SELECT 1 FROM ACCOUNTS WHERE ac_account_id = @pAccountId ) 
  BEGIN
    SET @pErrorCode = 1
    RETURN
  END
 
  IF NOT EXISTS (SELECT 1 FROM ACCOUNT_MOVEMENTS WHERE AM_SITE_ID = @pSiteId AND AM_MOVEMENT_ID = @pMovementId) 
  BEGIN  
    INSERT INTO   ACCOUNT_MOVEMENTS
                ( [am_site_id]
                , [am_movement_id]
                , [am_play_session_id]
                , [am_account_id]
                , [am_terminal_id]
                , [am_wcp_sequence_id]
                , [am_wcp_transaction_id]
                , [am_datetime]
                , [am_type]
                , [am_initial_balance]
                , [am_sub_amount]
                , [am_add_amount]
                , [am_final_balance]
                , [am_cashier_id]
                , [am_cashier_name]
                , [am_terminal_name]
                , [am_operation_id]
                , [am_details]
                , [am_reasons])
         VALUES
                ( @pSiteId           
                , @pMovementId       
                , @pPlaySessionId   
                , @pAccountId        
                , @pTerminalId       
                , @pWcpSequenceId    
                , @pWcpTransactionId 
                , @pDatetime         
                , @pType             
                , @pInitialBalance  
                , @pSubAmount        
                , @pAddAmount        
                , @pFinalBalance     
                , @pCashierId        
                , @pCashierName      
                , @pTperminalName    
                , @pOperationId      
                , @pDetails          
                , @pReasons  )          
  
    SET @LocalDelta = ISNULL (@pAddAmount - @pSubAmount, 0)

    IF (@pType = 39  OR @pType = 42  OR @pType = 67)  SET @LocalDelta = 0 -- Points Gift Delivery/Points Gift Services/Points Status
    IF (@pType = 71  OR @pType = 101) SET @IsDelta    = 0                 -- Import/Multisite initial points
    
    UPDATE   ACCOUNTS
       SET   AC_POINTS           = CASE WHEN (@IsDelta = 1) THEN AC_POINTS + @LocalDelta ELSE @LocalDelta END
           , AC_MS_LAST_SITE_ID  = @pSiteId 
           , AC_LAST_ACTIVITY    = GETDATE()         
     WHERE   AC_ACCOUNT_ID       = @pAccountId 
    
  END
  
  SET @pErrorCode = 0
 
  SELECT   @PointsSequenceId     = AC_MS_POINTS_SEQ_ID 
         , @Points               = AC_POINTS
    FROM   ACCOUNTS
   WHERE   AC_ACCOUNT_ID = @pAccountId 
   
END -- Update_PointsAccountMovement
GO





/****** TRIGGERS ******/
--------------------------------------------------------------------------------
-- Copyright � 2013 Win Systems International 
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: MultiSiteTrigger_AccountUpdate.sql
-- 
--   DESCRIPTION: Procedures for trigger MultiSiteTrigger_AccountUpdate and related issues
-- 
--        AUTHOR: Jos� Mart�nez
-- 
-- CREATION DATE: 07-MAR-2013
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 07-MAR-2013 JML    First release.
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- PURPOSE : Trigger on ACCOUNTS when center receive a changes, mark account to synchronize:
--                                                                      - Points if changed
--                                                                      - Personal info
-- 
--  PARAMS :
--      - INPUT :
--
--      - OUTPUT :
--
-- RETURNS :
--
--
--   NOTES :
--

ALTER TRIGGER [dbo].[MultiSiteTrigger_AccountUpdate] ON [dbo].[accounts]
   AFTER UPDATE
NOT FOR REPLICATION
AS 
BEGIN
    DECLARE @Sequence11Value    AS BIGINT
    DECLARE @Sequence10Value    AS BIGINT
    DECLARE @AccountId          AS BIGINT
    DECLARE @hash0              as varbinary(20)
    DECLARE @hash1              as varbinary(20)
    DECLARE @value              as nvarchar(max)
    DECLARE @changed            as bit
    DECLARE @updated            as bit
    
    SET @updated = 0;        
            
    IF (UPDATE(AC_MS_CHANGE_GUID)) RETURN
 
    IF (UPDATE(AC_POINTS) OR UPDATE(AC_POINTS_STATUS))
      BEGIN
          DECLARE PointsCursor CURSOR FOR 
           SELECT   INSERTED.AC_ACCOUNT_ID 
             FROM   INSERTED, DELETED 
            WHERE   INSERTED.AC_ACCOUNT_ID    =  DELETED.AC_ACCOUNT_ID
              AND ( INSERTED.AC_POINTS        <> DELETED.AC_POINTS
               OR   INSERTED.AC_POINTS_STATUS <> DELETED.AC_POINTS_STATUS )
               
        SET NOCOUNT ON;

        OPEN PointsCursor

        FETCH NEXT FROM PointsCursor INTO @AccountId
          
        WHILE @@FETCH_STATUS = 0
        BEGIN
            UPDATE   SEQUENCES 
               SET   SEQ_NEXT_VALUE = SEQ_NEXT_VALUE + 1
             WHERE   SEQ_ID         = 11

            SELECT   @Sequence11Value = SEQ_NEXT_VALUE - 1 FROM SEQUENCES WHERE SEQ_ID = 11

            UPDATE   ACCOUNTS
               SET   AC_MS_POINTS_SEQ_ID = @Sequence11Value 
             WHERE   AC_ACCOUNT_ID       = @AccountId

            FETCH NEXT FROM PointsCursor INTO @AccountId
        END

        CLOSE PointsCursor
        DEALLOCATE PointsCursor
    END


    IF UPDATE (AC_TRACK_DATA)
    OR UPDATE (AC_HOLDER_NAME)
    OR UPDATE (AC_HOLDER_ID)
    OR UPDATE (AC_HOLDER_ID_TYPE)
    OR UPDATE (AC_HOLDER_ADDRESS_01)
    OR UPDATE (AC_HOLDER_ADDRESS_02)
    OR UPDATE (AC_HOLDER_ADDRESS_03)
    OR UPDATE (AC_HOLDER_CITY)
    OR UPDATE (AC_HOLDER_ZIP)
    OR UPDATE (AC_HOLDER_EMAIL_01)
    OR UPDATE (AC_HOLDER_EMAIL_02)
    OR UPDATE (AC_HOLDER_TWITTER_ACCOUNT)
    OR UPDATE (AC_HOLDER_PHONE_NUMBER_01)
    OR UPDATE (AC_HOLDER_PHONE_NUMBER_02)
    OR UPDATE (AC_HOLDER_COMMENTS)
    OR UPDATE (AC_HOLDER_ID1)
    OR UPDATE (AC_HOLDER_ID2)
    OR UPDATE (AC_HOLDER_DOCUMENT_ID1)
    OR UPDATE (AC_HOLDER_DOCUMENT_ID2)
    OR UPDATE (AC_HOLDER_NAME1)
    OR UPDATE (AC_HOLDER_NAME2)
    OR UPDATE (AC_HOLDER_NAME3)
    OR UPDATE (AC_HOLDER_GENDER)
    OR UPDATE (AC_HOLDER_MARITAL_STATUS)
    OR UPDATE (AC_HOLDER_BIRTH_DATE)
    OR UPDATE (AC_HOLDER_WEDDING_DATE)
    OR UPDATE (AC_HOLDER_LEVEL)
    OR UPDATE (AC_HOLDER_LEVEL_NOTIFY)
    OR UPDATE (AC_HOLDER_LEVEL_ENTERED)
    OR UPDATE (AC_HOLDER_LEVEL_EXPIRATION)
    OR UPDATE (AC_PIN)
    OR UPDATE (AC_PIN_FAILURES)
    OR UPDATE (AC_PIN_LAST_MODIFIED)
    OR UPDATE (AC_BLOCKED)
    OR UPDATE (AC_ACTIVATED)
    OR UPDATE (AC_BLOCK_REASON)
    OR UPDATE (AC_HOLDER_IS_VIP)
    OR UPDATE (AC_HOLDER_TITLE)
    OR UPDATE (AC_HOLDER_NAME4)
    OR UPDATE (AC_HOLDER_PHONE_TYPE_01)
    OR UPDATE (AC_HOLDER_PHONE_TYPE_02)
    OR UPDATE (AC_HOLDER_STATE)
    OR UPDATE (AC_HOLDER_COUNTRY)
    OR UPDATE (AC_USER_TYPE)
        SET @updated = 1;
        
    IF (@updated = 0) RETURN

    DECLARE PersonalInfoCursor CURSOR FOR 
     SELECT   AC_ACCOUNT_ID
            , HASHBYTES ('SHA1',  ISNULL(AC_TRACK_DATA,        '')
                                + ISNULL(AC_HOLDER_NAME,       '')
                                + ISNULL(AC_HOLDER_ID,         '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_ID_TYPE), '')
                                + ISNULL(AC_HOLDER_ADDRESS_01, '')
                                + ISNULL(AC_HOLDER_ADDRESS_02, '')
                                + ISNULL(AC_HOLDER_ADDRESS_03, '')
                                + ISNULL(AC_HOLDER_CITY,       '')
                                + ISNULL(AC_HOLDER_ZIP,        '')
                                + ISNULL(AC_HOLDER_EMAIL_01,   '')
                                + ISNULL(AC_HOLDER_EMAIL_02,   '')
                                + ISNULL(AC_HOLDER_TWITTER_ACCOUNT, '')
                                + ISNULL(AC_HOLDER_PHONE_NUMBER_01, '')
                                + ISNULL(AC_HOLDER_PHONE_NUMBER_02, '')
                                + ISNULL(AC_HOLDER_COMMENTS,        '')
                                + ISNULL(AC_HOLDER_ID1, '')
                                + ISNULL(AC_HOLDER_ID2, '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_DOCUMENT_ID1), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_DOCUMENT_ID2), '')
                                + ISNULL(AC_HOLDER_NAME1, '')
                                + ISNULL(AC_HOLDER_NAME2, '')
                                + ISNULL(AC_HOLDER_NAME3, '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_GENDER),         '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_MARITAL_STATUS), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_BIRTH_DATE,   21), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_WEDDING_DATE, 21), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_LEVEL),                '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_LEVEL_NOTIFY),         '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_LEVEL_ENTERED,    21), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_LEVEL_EXPIRATION, 21), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_PIN),          '')
                                + ISNULL(CONVERT(NVARCHAR, AC_PIN_FAILURES), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_PIN_LAST_MODIFIED, 21), '')
                                + ISNULL(CONVERT(NVARCHAR, AC_BLOCKED),           '')
                                + ISNULL(CONVERT(NVARCHAR, AC_ACTIVATED),         '')
                                + ISNULL(CONVERT(NVARCHAR, AC_BLOCK_REASON),         '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_IS_VIP),        '')
                                + ISNULL(AC_HOLDER_TITLE                        , '')
                                + ISNULL(AC_HOLDER_NAME4                        , '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_PHONE_TYPE_01),        '')
                                + ISNULL(CONVERT(NVARCHAR, AC_HOLDER_PHONE_TYPE_02),        '')
                                + ISNULL(AC_HOLDER_STATE                        , '')
                                + ISNULL(AC_HOLDER_COUNTRY                      , '')
                                + ISNULL(CONVERT(NVARCHAR,AC_USER_TYPE)         , '') )
       FROM   INSERTED

    SET NOCOUNT ON;

    OPEN PersonalInfoCursor

    FETCH NEXT FROM PersonalInfoCursor INTO @AccountId, @hash1

    WHILE @@FETCH_STATUS = 0
    BEGIN

        SELECT @hash0 = AC_MS_HASH FROM DELETED WHERE AC_ACCOUNT_ID = @AccountId

        SELECT @changed = CASE WHEN ( @hash0 = @hash1 ) THEN 0 ELSE 1 END

        IF @changed = 1
        BEGIN
            -- Personal Info
            UPDATE   SEQUENCES 
               SET   SEQ_NEXT_VALUE = SEQ_NEXT_VALUE + 1
             WHERE   SEQ_ID         = 10

            SELECT   @Sequence10Value = SEQ_NEXT_VALUE - 1 FROM SEQUENCES WHERE SEQ_ID = 10

            UPDATE   ACCOUNTS
               SET   AC_MS_PERSONAL_INFO_SEQ_ID = @Sequence10Value
                   , AC_MS_HASH                 = @hash1
                   , AC_MS_CHANGE_GUID          = NEWID()
             WHERE   AC_ACCOUNT_ID              = @AccountId
        END

        FETCH NEXT FROM PersonalInfoCursor INTO @AccountId, @hash1
    END

    CLOSE PersonalInfoCursor
    DEALLOCATE PersonalInfoCursor

END
GO

/****** RECORDS ******/ 









