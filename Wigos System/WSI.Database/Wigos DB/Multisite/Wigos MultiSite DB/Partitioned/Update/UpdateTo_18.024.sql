/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_100]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 23;

SET @New_ReleaseId = 24;
SET @New_ScriptName = N'UpdateTo_18.024.sql';
SET @New_Description = N'Request fields for accounts and beneficiary, and LastActivity info for MultiSite.';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[last_account_activity]') AND type in (N'U'))
BEGIN
      CREATE TABLE [dbo].[last_account_activity](
            [laa_site_id] [int] NOT NULL,
            [laa_account_id] [bigint] NOT NULL,
            [laa_last_activity] [date] NOT NULL,
            [laa_re_balance] [money] NULL,
            [laa_promo_re_balance] [money] NULL,
            [laa_promo_nr_balance] [money] NULL,
            [laa_in_session_re_balance] [money] NULL,
            [laa_in_session_promo_re_balance] [money] NULL,
            [laa_in_session_promo_nr_balance] [money] NULL,
      CONSTRAINT [PK_LAST_ACCOUNT_ACTIVITY_1] PRIMARY KEY CLUSTERED 
      (
            [laa_site_id] ASC,
            [laa_account_id] ASC
      )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
      ) ON [PRIMARY]
END
GO

/****** RECORDS ******/

--
-- NEW TASK: Upload Last Activity
--
SELECT ST_SITE_ID AS SITE_ID INTO #CURRENT_SITES FROM MS_SITE_TASKS GROUP BY ST_SITE_ID
INSERT INTO MS_SITE_TASKS (ST_TASK_ID, ST_ENABLED, ST_INTERVAL_SECONDS, ST_MAX_ROWS_TO_UPLOAD, ST_SITE_ID) 
                   SELECT          62,          1,                 600,                   200,    SITE_ID  FROM #CURRENT_SITES;
DROP TABLE #CURRENT_SITES
GO

INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE, GP_MS_DOWNLOAD_TYPE) VALUES ('Account.RequestedField'    , 'BirthCountry'    , '0', 1);
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE, GP_MS_DOWNLOAD_TYPE) VALUES ('Account.RequestedField'    , 'BirthDate'       , '1', 1);
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE, GP_MS_DOWNLOAD_TYPE) VALUES ('Account.RequestedField'    , 'Document'        , '1', 1);
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE, GP_MS_DOWNLOAD_TYPE) VALUES ('Account.RequestedField'    , 'DocumentTypeList', '' , 1);
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE, GP_MS_DOWNLOAD_TYPE) VALUES ('Account.RequestedField'    , 'DocScan'         , '0', 1);
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE, GP_MS_DOWNLOAD_TYPE) VALUES ('Account.RequestedField'    , 'DocScanTypeList' , '' , 1);
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE, GP_MS_DOWNLOAD_TYPE) VALUES ('Account.RequestedField'    , 'Email1'          , '0', 1);
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE, GP_MS_DOWNLOAD_TYPE) VALUES ('Account.RequestedField'    , 'Gender'          , '1', 1);
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE, GP_MS_DOWNLOAD_TYPE) VALUES ('Account.RequestedField'    , 'Name1'           , '1', 1);
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE, GP_MS_DOWNLOAD_TYPE) VALUES ('Account.RequestedField'    , 'Name2'           , '0', 1);
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE, GP_MS_DOWNLOAD_TYPE) VALUES ('Account.RequestedField'    , 'Name3'           , '1', 1);
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE, GP_MS_DOWNLOAD_TYPE) VALUES ('Account.RequestedField'    , 'Nationality'     , '0', 1);
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE, GP_MS_DOWNLOAD_TYPE) VALUES ('Account.RequestedField'    , 'Occupation'      , '0', 1);
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE, GP_MS_DOWNLOAD_TYPE) VALUES ('Account.RequestedField'    , 'Phone1'          , '0', 1);
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE, GP_MS_DOWNLOAD_TYPE) VALUES ('Beneficiary.RequestedField', 'Document'        , '1', 1);
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE, GP_MS_DOWNLOAD_TYPE) VALUES ('Beneficiary.RequestedField', 'DocumentTypeList', '' , 1);
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE, GP_MS_DOWNLOAD_TYPE) VALUES ('Beneficiary.RequestedField', 'Name1'           , '1', 1);
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE, GP_MS_DOWNLOAD_TYPE) VALUES ('Beneficiary.RequestedField', 'Name2'           , '0', 1);
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE, GP_MS_DOWNLOAD_TYPE) VALUES ('Beneficiary.RequestedField', 'Name3'           , '1', 1);
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE, GP_MS_DOWNLOAD_TYPE) VALUES ('Beneficiary.RequestedField', 'BirthDate'       , '1', 1);
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE, GP_MS_DOWNLOAD_TYPE) VALUES ('Beneficiary.RequestedField', 'Gender'          , '1', 1);
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE, GP_MS_DOWNLOAD_TYPE) VALUES ('Beneficiary.RequestedField', 'Occupation'      , '0', 1);
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE, GP_MS_DOWNLOAD_TYPE) VALUES ('Beneficiary.RequestedField', 'DocScan'         , '0', 1);
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE, GP_MS_DOWNLOAD_TYPE) VALUES ('Beneficiary.RequestedField', 'DocScanTypeList' , '' , 1);
GO

UPDATE   GENERAL_PARAMS
   SET   GP_KEY_VALUE = 'Por la ley antilavado de dinero, al realizar esta recarga, el cliente va a ser incluido en un reporte para el SAT.'
 WHERE   GP_GROUP_KEY = 'AntiMoneyLaundering'   AND GP_SUBJECT_KEY = 'Recharge.Report.Message'

UPDATE   GENERAL_PARAMS
   SET   GP_KEY_VALUE = 'Por la ley antilavado de dinero, al realizar este reintegro, el cliente va a ser incluido en un reporte para el SAT.'
 WHERE   GP_GROUP_KEY = 'AntiMoneyLaundering'   AND GP_SUBJECT_KEY = 'Prize.Report.Message'
GO

/****** STORED PROCEDURES ******/

  --------------------------------------------------------------------------------
  -- Copyright � 2013 Win Systems International
  --------------------------------------------------------------------------------
  --
  --   MODULE NAME: Update_LastActivity.sql
  --
  --   DESCRIPTION: Update account activity 
  --
  --        AUTHOR: Joan Marc Pepi�
  --
  -- CREATION DATE: 13-AUG-2013
  --
  -- REVISION HISTORY:
  --
  -- Date        Author Description
  -- ----------- ------ ----------------------------------------------------------
  -- 13-AUG-2013 JPJ    First release.  
  -------------------------------------------------------------------------------- 
  
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Update_LastActivity]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Update_LastActivity]
GO

CREATE   PROCEDURE [dbo].[Update_LastActivity]
	 @pSite_Id											INT
	,@pAccount_Id										BIGINT
	,@pLast_Activity								DATETIME
	,@pRE_Balance										MONEY
	,@pPromo_RE_Balance							MONEY
	,@pPromo_NR_Balance							MONEY
	,@pIn_Session_RE_Balance				MONEY
	,@pIn_Session_Promo_RE_Balance	MONEY
	,@pIn_Session_Promo_NR_Balance	MONEY
AS
BEGIN   

  IF EXISTS (SELECT   1 
               FROM   LAST_ACCOUNT_ACTIVITY
              WHERE   LAA_SITE_ID			         = @pSite_Id
                AND   LAA_ACCOUNT_ID	         = @pAccount_Id)

     UPDATE   LAST_ACCOUNT_ACTIVITY
        SET   LAA_LAST_ACTIVITY									= @pLast_Activity				
						, LAA_RE_BALANCE										= @pRE_Balance					
						, LAA_PROMO_RE_BALANCE							= @pPromo_RE_Balance				
						, LAA_PROMO_NR_BALANCE							= @pPromo_NR_Balance 			
						, LAA_IN_SESSION_RE_BALANCE					= @pIn_Session_RE_Balance		
						, LAA_IN_SESSION_PROMO_RE_BALANCE		= @pIn_Session_Promo_RE_Balance	
						, LAA_IN_SESSION_PROMO_NR_BALANCE		= @pIn_Session_Promo_NR_Balance	
			WHERE   LAA_SITE_ID		= @PSITE_ID
		    AND   LAA_ACCOUNT_ID  = @PACCOUNT_ID
        
  ELSE
  
    INSERT INTO   LAST_ACCOUNT_ACTIVITY
                ( LAA_SITE_ID
                , LAA_ACCOUNT_ID
                , LAA_LAST_ACTIVITY
							  , LAA_RE_BALANCE
							  , LAA_PROMO_RE_BALANCE
							  , LAA_PROMO_NR_BALANCE
								, LAA_IN_SESSION_RE_BALANCE
								, LAA_IN_SESSION_PROMO_RE_BALANCE
								, LAA_IN_SESSION_PROMO_NR_BALANCE)
         VALUES
                ( @pSite_Id
                , @pAccount_Id
                , @pLast_Activity				
								, @pRE_Balance					
								, @pPromo_RE_Balance				
								, @pPromo_NR_Balance 			
								, @pIn_Session_RE_Balance		
								, @pIn_Session_Promo_RE_Balance	
								, @pIn_Session_Promo_NR_Balance	)

END -- Update_LastActivity
GO
