/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_100]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 79;

SET @New_ReleaseId = 80;
SET @New_ScriptName = N'UpdateTo_18.080.sql';
SET @New_Description = N'New table for currencies;New alarms;';
/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/

/* Create Table currencies */
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[currencies]') AND type in (N'U'))
BEGIN
	CREATE TABLE [dbo].[currencies](
		  [cur_iso_code] [nvarchar](3) NOT NULL,
		  [cur_description] [nvarchar](50) NULL
	CONSTRAINT [pk_iso_code] PRIMARY KEY CLUSTERED 
	(
		  [cur_iso_code] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]
END
GO

/* Create Table exchange_rates */
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[exchange_rates]') AND type in (N'U'))
BEGIN
	CREATE TABLE [dbo].[exchange_rates](
		  [er_iso_code] [nvarchar](3) NOT NULL,
		  [er_working_day] [datetime] NOT NULL,
		  [er_change] [decimal](16,8) NOT NULL,
		  [er_date_updated] [datetime] NOT NULL DEFAULT GETDATE(),
	CONSTRAINT [pk_iso_code_working_day] PRIMARY KEY CLUSTERED 
	(
		  [er_iso_code] ASC,
		  [er_working_day] DESC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[site_currencies]') AND type in (N'U'))
BEGIN
  CREATE TABLE [dbo].[site_currencies](
	  [sc_site_id] [int] NOT NULL,
	  [sc_iso_code] [nvarchar](3) NOT NULL,
	  [sc_type] [int] NOT NULL,	  
	  [sc_description] [nvarchar](50) NULL,
	  [sc_change] [decimal](16, 8) NOT NULL,
   CONSTRAINT [pk_site_id_iso_code_type] PRIMARY KEY CLUSTERED 
  (
	  [sc_site_id] ASC,
	  [sc_iso_code] ASC,
	  [sc_type] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
  ) ON [PRIMARY]
end

GO

/****** INDEXES ******/

/****** RECORDS ******/

IF EXISTS (SELECT [alcg_alarm_code] from [dbo].[alarm_catalog] WHERE [alcg_alarm_code] = 393265 AND [alcg_language_id] = 10)
BEGIN
  UPDATE [dbo].[alarm_catalog] SET  [alcg_description] = 'N�mero de equipo no esperado', [alcg_name] = 'N�mero de equipo no esperado' WHERE [alcg_alarm_code] = 393265 AND [alcg_language_id] = 10
END
GO

------------------------------------------------------------
-- MultiSite_MultiCurrencyNewMultiSiteCurrency = 0x00090007
------------------------------------------------------------

IF  EXISTS (SELECT * FROM [dbo].[alarm_catalog] WHERE alcg_alarm_code =589831)
	DELETE FROM [dbo].[alarm_catalog] WHERE alcg_alarm_code =589831
GO
IF  EXISTS (SELECT * FROM [dbo].[alarm_catalog_per_category] WHERE alcc_alarm_code =589831)
	DELETE FROM [dbo].[alarm_catalog_per_category] WHERE alcc_alarm_code =589831
GO

INSERT    [dbo].[alarm_catalog] 
        ( [alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) 
VALUES  ( 589831, 9, 0, N'New currency on MultiSite', N'New national currency configured on site @p0: @p1', 1)
		
INSERT    [dbo].[alarm_catalog]
        ( [alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) 
VALUES  ( 589831, 10, 0, N'Nueva divisa en el MultiSite', N'Nueva moneda nacional configurada en la sala @p0: @p1', 1)

INSERT    [dbo].[alarm_catalog_per_category] 
		( [alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) 
VALUES  ( 589831, 32, 0, GETDATE())
		
------------------------------------------------------------
-- MultiSite_MultiCurrencyNewSiteCurrency = 0x00090008
------------------------------------------------------------
		
IF  EXISTS (SELECT * FROM [dbo].[alarm_catalog] WHERE alcg_alarm_code =589832)
	DELETE FROM [dbo].[alarm_catalog] WHERE alcg_alarm_code =589832
GO
IF  EXISTS (SELECT * FROM [dbo].[alarm_catalog_per_category] WHERE alcc_alarm_code =589832)
	DELETE FROM [dbo].[alarm_catalog_per_category] WHERE alcc_alarm_code =589832
GO

INSERT    [dbo].[alarm_catalog] 
        ( [alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) 
VALUES  ( 589832, 9, 0, N'National currency modification on site', N'National currency modification on site @p0: @p1 (before @p2)', 1)
		
INSERT    [dbo].[alarm_catalog]
        ( [alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) 
VALUES  ( 589832, 10, 0, N'Modificaci�n de moneda nacional en la sala', N'Modificaci�n de moneda nacional en la sala @p0: @p1 (antes @p2)', 1)

INSERT    [dbo].[alarm_catalog_per_category] 
		( [alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) 
VALUES  ( 589832, 32, 0, GETDATE())

------------------------------------------------------------
-- MultiSite_MultiCurrencyNewSiteCurrencyForeign = 0x00090009
------------------------------------------------------------		

IF  EXISTS (SELECT * FROM [dbo].[alarm_catalog] WHERE alcg_alarm_code =589833)
	DELETE FROM [dbo].[alarm_catalog] WHERE alcg_alarm_code =589833
GO
IF  EXISTS (SELECT * FROM [dbo].[alarm_catalog_per_category] WHERE alcc_alarm_code =589833)
	DELETE FROM [dbo].[alarm_catalog_per_category] WHERE alcc_alarm_code =589833
GO

INSERT    [dbo].[alarm_catalog] 
        ( [alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) 
VALUES  ( 589833, 9, 0, N'Foreign currency modification on site', N'Foreign currency modification on site @p0: @p1 (before @p2)', 1)
		
INSERT    [dbo].[alarm_catalog] 
        ( [alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) 
VALUES  ( 589833, 10, 0, N'Modificaci�n de divisa en la sala', N'Modificaci�n de divisa en la sala @p0: @p1 (antes @p2)', 1)

INSERT    [dbo].[alarm_catalog_per_category] 
		( [alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) 
VALUES  ( 589833, 32, 0, GETDATE())
		    
GO

-----------------------------------------------------------------------------
-- INSERT: General Params
----------------------------------------------------------------------------- 

IF NOT EXISTS (SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'MultiSite.Multicurrency' AND GP_SUBJECT_KEY = 'Enabled')
      INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY              ,GP_SUBJECT_KEY, GP_KEY_VALUE, GP_MS_DOWNLOAD_TYPE) 
                          VALUES ('MultiSite.Multicurrency' , 'Enabled'    ,0            , 1)
GO

IF NOT EXISTS (SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'MultiSite.Multicurrency' AND GP_SUBJECT_KEY = 'MainCurrency')
      INSERT INTO GENERAL_PARAMS ( GP_GROUP_KEY             , GP_SUBJECT_KEY, GP_KEY_VALUE, GP_MS_DOWNLOAD_TYPE) 
			                    VALUES ( 'MultiSite.Multicurrency', 'MainCurrency', ''          , 0)
GO	

/******* STORED PROCEDURES *******/

--------------------------------------------------------------------------------
-- Copyright � 2013 Win Systems International
--------------------------------------------------------------------------------
--
--   MODULE NAME: ManteinanceExchangeRates
--
--   DESCRIPTION: Stored Procedure that manteinance table exchange rates
--
--        AUTHOR: Didac Campanals Subirats
--
-- CREATION DATE: 27-ABR-2015
--
-- REVISION HISTORY:
--
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 27-ABR-2015 DCS    First release.  
-- 19-MAY-2015 DCS    Update with last changes --> Only update last 30 days  
-------------------------------------------------------------------------------- 

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ManteinanceExchangeRates]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ManteinanceExchangeRates]
GO

CREATE PROCEDURE [dbo].[ManteinanceExchangeRates]        
AS
BEGIN 

DECLARE @pIsoCode as NVARCHAR(3)
DECLARE @pFirstDateIsoCode as DATETIME
DECLARE @pNowDate as DATETIME
DECLARE @pNumRows as BIGINT
DECLARE @pMaxDaysUpdated as BIGINT

	-- This variable indicates the maximum number of days to update, back today.
	SET @pMaxDaysUpdated = 30

	SET @pNowDate = DATEADD(HOUR, - DATEPART(HOUR, GETDATE()) , DATEADD(HOUR, DATEDIFF(HOUR, 0, GETDATE()), 0))

	DECLARE   CursIsoCode CURSOR FOR 
	 SELECT   CUR_ISO_CODE
	   FROM   CURRENCIES		 
	        
	  SET NOCOUNT ON;
	 OPEN CursIsoCode
	FETCH NEXT FROM CursIsoCode INTO @pIsoCode
	
	WHILE @@FETCH_STATUS = 0
	BEGIN

		SELECT   @pFirstDateIsoCode = MIN(ER_WORKING_DAY)
					 , @pNumRows = COUNT (*)
			FROM   EXCHANGE_RATES
		 WHERE   ER_ISO_CODE = @pIsoCode
		   AND   ER_WORKING_DAY >= (SELECT DATEADD(DAY,-@pMaxDaysUpdated,DATEADD(HOUR, - DATEPART(HOUR, GETDATE()) , DATEADD(HOUR, DATEDIFF(HOUR, 0, GETDATE()), 0))))
		
		IF (@pNumRows > 0 )
		BEGIN
			
			IF ( SELECT (DATEDIFF(DAY, @pFirstDateIsoCode, @pNowDate ) + 1) - @pNumRows ) > 0 
			BEGIN

			WITH DATES  AS
			(
					SELECT   @pFirstDateIsoCode  AS CALENDARDATE
			 UNION ALL
					SELECT   DATEADD (DAY, 1, CALENDARDATE) AS CALENDARDATE
						FROM   DATES
					 WHERE   DATEADD (DAY, 1, CALENDARDATE) <= @pNowDate
			) 
					INSERT   INTO EXCHANGE_RATES
					SELECT   @pIsoCode AS ER_ISO_CODEY
								 , AA.CALENDARDATE AS ER_WORKING_DAY
								 , ISNULL ( ER_CHANGE
													, ( SELECT   TOP 1 ER_CHANGE 
												 				FROM   EXCHANGE_RATES AS ER2 
															 WHERE   ER2.ER_ISO_CODE = @pIsoCode 
																 AND   ER2.ER_WORKING_DAY < AA.CALENDARDATE 
															 ORDER   BY ER_WORKING_DAY DESC
														 )
													) AS ER_CHANGE
								 , GETDATE() AS ER_DATE_UPDATED
						FROM   DATES AS AA 
			 LEFT JOIN   EXCHANGE_RATES AS ER 
		  				ON   DATEADD (DAY, DATEDIFF(DD, 0, AA.CALENDARDATE), 0) =  DATEADD (DAY, DATEDIFF(DD, 0, ER_WORKING_DAY), 0)
						 AND   ER_ISO_CODE = @pIsoCode
					 WHERE   ER_ISO_CODE IS NULL

			OPTION (MAXRECURSION 32767)
			
				END -- DATEDIFF
			END -- NumRows

		FETCH NEXT FROM CursIsoCode INTO @pIsoCode

	END -- WHILE

CLOSE CursIsoCode
DEALLOCATE CursIsoCode

END -- PROCEDURE
GO

  --------------------------------------------------------------------------------
  -- Copyright � 2013 Win Systems International
  --------------------------------------------------------------------------------
  --
  --   MODULE NAME: MultiSite_CashierMovementsGroupedByHour
  --
  --   DESCRIPTION: Stored Procedure that gets the cashier movements by hour and site
  --
  --        AUTHOR: Joan Marc Pepi� Jamison
  --
  -- CREATION DATE: 17-MAR-2014
  --
  -- REVISION HISTORY:
  --
  -- Date        Author Description
  -- ----------- ------ ----------------------------------------------------------
  -- 17-MAR-2014 JPJ    First release.  
  -------------------------------------------------------------------------------- 

ALTER PROCEDURE [dbo].[MultiSite_CashierMovementsGroupedByHour]        
         @pSiteId    NVARCHAR(MAX)
        ,@pDateFrom  DATETIME
        ,@pDateTo    DATETIME       
  AS
BEGIN 
DECLARE @_sql NVARCHAR(MAX)

  IF (@pSiteId IS NOT NULL)
 			SET @_sql = 'SELECT   CM_SITE_ID'
  ELSE
			SET @_sql = 'SELECT   NULL'
 
			SET @_sql   = @_sql + '
													, CM_TYPE
													, CM_SUB_TYPE
													, CM_TYPE_COUNT							
													, CM_CURRENCY_ISO_CODE
													, CM_CURRENCY_DENOMINATION
													, CM_SUB_AMOUNT
													, CM_ADD_AMOUNT
													, CM_AUX_AMOUNT
													, CM_INITIAL_BALANCE
													, CM_FINAL_BALANCE
										 FROM   CASHIER_MOVEMENTS_GROUPED_BY_HOUR 
										WHERE   CM_DATE >= ''' + CONVERT(VARCHAR, @pDateFrom, 21) + '''  
											AND   CM_DATE < ''' + CONVERT(VARCHAR, @pDateTo, 21) +''''
  	
		IF (@pSiteId IS NOT NULL)
				SET @_sql = @_sql + ' AND CM_SITE_ID IN (' + @pSiteId + ')'
  		
    EXEC sp_executesql @_sql
					
END -- MultiSite_CashierMovementsGroupedByHour
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MultiSite_UpdateCurrencies]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[MultiSite_UpdateCurrencies]
GO
CREATE PROCEDURE [dbo].[MultiSite_UpdateCurrencies]        
          @pIsoCode                 NVARCHAR(3)
        , @pSiteId                  INT
        , @pType                    INT
        , @pDescription             NVARCHAR(MAX)
        , @pForeignCurrencyType     INT
        , @pNationalCurrencyType    INT
        , @pChange                  DECIMAL(16,8)
        , @pResponseCode			INT               OUTPUT
        , @pBeforeCurrencyIsoCode   NVARCHAR(3)		  OUTPUT
  AS
BEGIN 

SET @pBeforeCurrencyIsoCode = (SELECT SC_ISO_CODE FROM SITE_CURRENCIES WHERE SC_SITE_ID = @pSiteId AND SC_TYPE = @pType)

SET @pResponseCode = 0         

------------------------------------------
-- INSERT NEW NATIONAL CURRENCY IN CURRENCIES
------------------------------------------
IF NOT EXISTS( SELECT TOP 1 CUR_ISO_CODE FROM CURRENCIES WHERE CUR_ISO_CODE = @pIsoCode) AND (@pType = @pNationalCurrencyType)
BEGIN
    INSERT   INTO CURRENCIES (CUR_ISO_CODE, CUR_DESCRIPTION)    
                       VALUES(@pIsoCode   , @pDescription  )      
 
    SET @pResponseCode = 1   -- IS NEW NATIONAL CURRENCY!   
 END  
 
 
----------------------------------------------------
-- INSERT OR UPDATE CURRENCY IN SITE_CURRENCIES
----------------------------------------------------
IF NOT EXISTS ( SELECT TOP 1 SC_ISO_CODE FROM SITE_CURRENCIES WHERE SC_SITE_ID = @pSiteId AND SC_TYPE = @pType)  
BEGIN
  -- NEW NATIONAL CURRENCY IN SITE_CURRENCIES
  INSERT INTO   SITE_CURRENCIES  
              ( SC_SITE_ID            
              , SC_ISO_CODE           
              , SC_DESCRIPTION        
              , SC_TYPE               
              , SC_CHANGE             
              )                       
       VALUES ( @pSiteId              
              , @pIsoCode             
              , @pDescription         
              , @pType                
              , @pChange              
              )

  IF ( @pType = @pNationalCurrencyType)
    SET @pResponseCode = @pResponseCode + 2 --IS NEW SITE NATIONAL CURRENCY
  ELSE
    SET @pResponseCode = @pResponseCode + 4 --IS NEW SITE FOREIGN CURRENCY
END
ELSE
BEGIN
  IF NOT EXISTS ( SELECT TOP 1 SC_ISO_CODE FROM SITE_CURRENCIES WHERE SC_SITE_ID = @pSiteId AND SC_ISO_CODE = @pIsoCode AND SC_TYPE = @pType)  
  BEGIN
    IF ( @pType = @pNationalCurrencyType)
      SET @pResponseCode = @pResponseCode + 2 --IS UPDATED SITE NATIONAL CURRENCY
    ELSE
      SET @pResponseCode = @pResponseCode + 4 --IS UPDATED SITE FOREIGN CURRENCY
  END
      
  UPDATE   SITE_CURRENCIES           
     SET   SC_ISO_CODE = @pIsoCode 
         , SC_DESCRIPTION = @pDescription
         , SC_CHANGE = @pChange      
   WHERE   SC_SITE_ID = @pSiteId     
     AND   SC_TYPE = @pType 
      
END  

SELECT @pResponseCode, @pBeforeCurrencyIsoCode
END
GO
