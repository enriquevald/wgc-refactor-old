/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_100]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 63;

SET @New_ReleaseId = 64;
SET @New_ScriptName = N'UpdateTo_18.064.sql';
SET @New_Description = N'Changes for: Mailing, Credit card / debit and Alarms';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/

--
-- BEGIN OF PROGRESSIVE
--

/*                             */ 
/*   SALES_PER_HOUR            */
/*                             */ 
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[sales_per_hour]') and name = 'sph_jackpot_amount')
ALTER TABLE dbo.sales_per_hour ADD
      sph_jackpot_amount money NULL,
      sph_progressive_jackpot_amount money NULL,
      sph_progressive_jackpot_amount_0 money NULL,
      sph_progressive_provision_amount money NULL
GO

/*                                              */ 
/*  VIEW SALES_PER_HOUR_V2 --> DROP AND CREATE  */
/*                                              */ 
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[sales_per_hour_v2]'))
DROP VIEW [dbo].[sales_per_hour_v2]
GO

CREATE VIEW [dbo].[sales_per_hour_v2]
AS
SELECT     dbo.sales_per_hour.sph_base_hour, dbo.sales_per_hour.sph_terminal_id, dbo.sales_per_hour.sph_terminal_name, dbo.sales_per_hour.sph_played_count, 
           dbo.sales_per_hour.sph_played_amount, dbo.sales_per_hour.sph_won_count, dbo.sales_per_hour.sph_won_amount, 
           0 AS sph_num_active_terminals, 0 AS sph_last_play_id, dbo.sales_per_hour.sph_theoretical_won_amount, 
           ISNULL (SPH_GAME_ID,0) AS SPH_GAME_ID,
           --ISNULL (dbo.games.gm_game_name,'UNKNOWN') AS SPH_GAME_NAME, sph_site_id as site_id
           (
           ISNULL (dbo.games.gm_game_name,'UNKNOWN') +
           ISNULL ( ' [' + cast(dbo.sales_per_hour.sph_payout as varchar(10)) + '%]','')
           )AS SPH_GAME_NAME,
           dbo.sales_per_hour.sph_payout, sph_site_id as site_id,
           dbo.sales_per_hour.sph_jackpot_amount, dbo.sales_per_hour.sph_progressive_jackpot_amount, 
           dbo.sales_per_hour.sph_progressive_jackpot_amount_0, dbo.sales_per_hour.sph_progressive_provision_amount
FROM       dbo.sales_per_hour INNER JOIN
           dbo.games ON sph_game_id = dbo.games.gm_game_id
           --AND dbo.terminal_game_translation.tgt_site_id = dbo.providers_games.pg_site_id 
           --AND dbo.sales_per_hour.sph_site_id = CASE WHEN  dbo.games.gm_site_id = 0 THEN dbo.sales_per_hour.sph_site_id ELSE dbo.games.gm_site_id END

GO

--
-- END OF PROGRESSIVE
--

--
-- BEGIN OF MAILING
--

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[PATTERNS]') and name = 'pt_active_mailing')
  ALTER TABLE [dbo].[PATTERNS] ADD [pt_active_mailing] BIT NOT NULL DEFAULT (0)
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[PATTERNS]') and name = 'pt_address_list')
  ALTER TABLE [dbo].[PATTERNS] ADD [pt_address_list] NVARCHAR(500) NULL
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[PATTERNS]') and name = 'pt_subject')
  ALTER TABLE [dbo].[PATTERNS] ADD [pt_subject] NVARCHAR(200) NULL
GO

--
-- END OF MAILING
--

/****** INDEXES ******/

/****** RECORDS ******/

--
-- BEGIN CREDIT CARD / DEBIT
--

-- SpecificBankCardTypes
IF NOT EXISTS (SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.PaymentMethod' AND GP_SUBJECT_KEY = 'BankCard.DifferentiateType')
      INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY, GP_KEY_VALUE, GP_MS_DOWNLOAD_TYPE) VALUES ('Cashier.PaymentMethod','BankCard.DifferentiateType','0', 1)
GO

-- Card EditBankTransactionData
IF NOT EXISTS (SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.PaymentMethod' AND GP_SUBJECT_KEY = 'BankCard.EnterTransactionDetails')
      INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY, GP_KEY_VALUE, GP_MS_DOWNLOAD_TYPE) VALUES ('Cashier.PaymentMethod','BankCard.EnterTransactionDetails','0', 1)
GO

-- Check EditBankTransactionData
IF NOT EXISTS (SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.PaymentMethod' AND GP_SUBJECT_KEY = 'Check.EnterTransactionDetails')
      INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY, GP_KEY_VALUE, GP_MS_DOWNLOAD_TYPE) VALUES ('Cashier.PaymentMethod','Check.EnterTransactionDetails','0', 1)
GO

--
-- END CREDIT CARD / DEBIT
--

--
-- NEW ALARMS
--

INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES ( 262152,  9, 0, N'Cash cage reopening', N'Cash cage reopening', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES ( 262152, 10, 0, N'Reapertura de b�veda', N'Reapertura de b�veda', 1)
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES ( 262152, 1, 0, GETDATE() )
GO

/******* STORED PROCEDURES *******/
