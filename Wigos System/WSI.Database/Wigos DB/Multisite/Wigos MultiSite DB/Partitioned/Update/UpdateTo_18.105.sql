/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_100]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 104;

SET @New_ReleaseId = 105;
SET @New_ScriptName = N'UpdateTo_18.105.sql';
SET @New_Description = N'New release v03.006'; 

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/**** GENERAL PARAM *****/
IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'CreditLine' AND GP_SUBJECT_KEY = 'Enabled')
   INSERT INTO [dbo].[general_params]([gp_group_key],[gp_subject_key],[gp_key_value],[gp_ms_download_type]) VALUES ('CreditLine','Enabled','0',1)
GO

/******* TABLES  *******/
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[credit_lines]') AND type in (N'U'))
BEGIN
	CREATE TABLE [dbo].[credit_lines](
		[cl_site_id] [int] NOT NULL,
		[cl_id] [bigint] NOT NULL,
		[cl_account_id] [bigint] NOT NULL,
		[cl_limit_amount] [money] NOT NULL,
		[cl_tto_amount] [money] NULL,
		[cl_spent_amount] [money] NOT NULL,
		[cl_iso_code] [nvarchar](3) NOT NULL,
		[cl_status] [int] NOT NULL,
		[cl_iban] [nvarchar](34) NULL,
		[cl_xml_signers] [xml] NULL,
		[cl_creation] [datetime] NOT NULL,
		[cl_update] [datetime] NOT NULL,
		[cl_expired] [datetime] NULL,
		[cl_last_payback] [datetime] NULL,
		[cl_timestamp] [timestamp] NOT NULL,
	 CONSTRAINT [PK_creditlines] PRIMARY KEY CLUSTERED 
	(
	  [cl_site_id] ASC,
		[cl_id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]
END

GO

/****** Object:  Table [dbo].[credit_line_movements] ******/

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[credit_line_movements]') AND type in (N'U'))
BEGIN

	CREATE TABLE [dbo].[credit_line_movements](
		[clm_site_id] [int] NOT NULL,
		[clm_id] [bigint] NOT NULL,
		[clm_credit_line_id] [bigint] NOT NULL,
		[clm_operation_id] [bigint] NOT NULL,
		[clm_type] [int] NOT NULL,
		[clm_old_value] [xml] NULL,
		[clm_new_value] [xml] NOT NULL,
		[clm_creation] [datetime] NOT NULL,
		[clm_creation_user] [nvarchar](50) NOT NULL,
		[clm_timestamp] [timestamp] NOT NULL,
	 CONSTRAINT [PK_creditlinemovements] PRIMARY KEY CLUSTERED 
	(
	  [clm_site_id] ASC,
		[clm_id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE NAME = 'IDX_cl_account_id')
CREATE NONCLUSTERED INDEX IDX_cl_account_id ON [dbo].[credit_lines] 
(
	[cl_account_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE NAME = 'IDX_clm_credit_line_id')
CREATE NONCLUSTERED INDEX [IDX_clm_credit_line_id] ON [dbo].[credit_line_movements] 
(
	[clm_credit_line_id] ASC,
	[clm_type] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

/******* RECORDS *******/
IF NOT EXISTS (SELECT [alcg_alarm_code] from [dbo].[alarm_catalog] WHERE [alcg_alarm_code] = 262164 AND [alcg_language_id] = 10)
BEGIN
  INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) 
    VALUES ( 262164, 10, 0, N'Conciliado ticket no esperado', N'Conciliado ticket no esperado', 1)
END
GO

IF NOT EXISTS (SELECT [alcg_alarm_code] from [dbo].[alarm_catalog] WHERE [alcg_alarm_code] = 262164 AND [alcg_language_id] = 9)
BEGIN
  INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) 
    VALUES ( 262164,  9, 0, N'Unexpected ticket conciliated', N'Unexpected ticket conciliated', 1)
END 
GO

IF NOT EXISTS (SELECT [alcc_alarm_code] from [dbo].[alarm_catalog_per_category] WHERE [alcc_alarm_code] = 262164 AND [alcc_category] = 40) 
BEGIN
  INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) 
    VALUES ( 262164, 40, 0, GETDATE() )
END
GO


IF NOT EXISTS (SELECT [alcg_alarm_code] from [dbo].[alarm_catalog] WHERE [alcg_alarm_code] = 262165 AND [alcg_language_id] = 10)
BEGIN
  INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) 
    VALUES ( 262165, 10, 0, N'Ticket conciliado no valido', N'Ticket conciliado no valido', 1)
END
GO

IF NOT EXISTS (SELECT [alcg_alarm_code] from [dbo].[alarm_catalog] WHERE [alcg_alarm_code] = 262165 AND [alcg_language_id] = 9)
BEGIN
  INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) 
    VALUES ( 262165,  9, 0, N'Invalid ticket conciliated', N'Invalid ticket conciliated', 1)
END 
GO

IF NOT EXISTS (SELECT [alcc_alarm_code] from [dbo].[alarm_catalog_per_category] WHERE [alcc_alarm_code] = 262165 AND [alcc_category] = 40) 
BEGIN
  INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) 
    VALUES ( 262165, 40, 0, GETDATE() )
END
GO

/***** REMOVE EXISTING REGISTERS ****/
DELETE FROM MS_SITE_TASKS 
WHERE ST_TASK_ID IN (76,77)
GO

/****** INSERT TASK (76 - CREDITLINES) IN MULTISITE ******/
SELECT   ST_SITE_ID AS SITE_ID 
  INTO   #CURRENT_SITES 
  FROM   MS_SITE_TASKS 
 GROUP   BY ST_SITE_ID
  
       IF NOT EXISTS(SELECT ST_TASK_ID FROM MS_SITE_TASKS WHERE ST_TASK_ID = 76)
       BEGIN
         INSERT   INTO MS_SITE_TASKS 
                     ( ST_TASK_ID
                    , ST_ENABLED
                    , ST_INTERVAL_SECONDS
                    , ST_MAX_ROWS_TO_UPLOAD
                    , ST_SITE_ID
                    ) 
              SELECT  76
                    , 1
                    , 180
                    , 200
                    , SITE_ID  
             FROM   #CURRENT_SITES;
       END
DROP TABLE #CURRENT_SITES
GO

/****** INSERT TASK (77 - CREDITLINE MOVEMENTS) IN MULTISITE ******/
SELECT   ST_SITE_ID AS SITE_ID 
  INTO   #CURRENT_SITES 
  FROM   MS_SITE_TASKS 
 GROUP   BY ST_SITE_ID
  
       IF NOT EXISTS(SELECT ST_TASK_ID FROM MS_SITE_TASKS WHERE ST_TASK_ID = 77)
       BEGIN
         INSERT   INTO MS_SITE_TASKS 
                     ( ST_TASK_ID
                    , ST_ENABLED
                    , ST_INTERVAL_SECONDS
                    , ST_MAX_ROWS_TO_UPLOAD
                    , ST_SITE_ID
                    ) 
              SELECT  77
                    , 1
                    , 180
                    , 200
                    , SITE_ID  
             FROM   #CURRENT_SITES;
       END
DROP TABLE #CURRENT_SITES
GO



/******* PROCEDURES *******/
IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'Update_CreditlineMovements')
 DROP PROCEDURE Update_CreditlineMovements
GO

CREATE PROCEDURE [dbo].[Update_CreditlineMovements]
	   @pSiteId  INT  
	  ,@pId BIGINT
	  ,@pCreditlineId BIGINT
	  ,@pOperationId BIGINT
	  ,@pType INT
	  ,@pOldvalue XML
	  ,@pNewValue XML
	  ,@pCreation DATETIME
	  ,@pCreationUser NVARCHAR(50)
AS
BEGIN   
IF EXISTS (SELECT   1 
			 FROM   CREDIT_LINE_MOVEMENTS
			WHERE   CLM_ID =  @pId 
			  AND   CLM_SITE_ID =  @pSiteId)
			  
UPDATE CREDIT_LINE_MOVEMENTS
   SET CLM_CREDIT_LINE_ID    = @pCreditlineId
	  ,CLM_OPERATION_ID      = @pOperationId
	  ,CLM_TYPE              = @pType
	  ,CLM_OLD_VALUE          = @pOldvalue
	  ,CLM_NEW_VALUE          = @pNewValue
	  ,CLM_CREATION           = @pCreation
	  ,CLM_CREATION_USER      = @pCreationUser
			WHERE   CLM_ID      =  @pId  
			  AND   CLM_SITE_ID =  @pSiteId


ELSE
INSERT INTO CREDIT_LINE_MOVEMENTS
		   (CLM_SITE_ID
		   ,CLM_ID
		   ,CLM_CREDIT_LINE_ID
		   ,CLM_OPERATION_ID  
		   ,CLM_TYPE          
		   ,CLM_OLD_VALUE      
		   ,CLM_NEW_VALUE      
		   ,CLM_CREATION       
		   ,CLM_CREATION_USER)  
	 VALUES 
		   (@pSiteId 
		   ,@pID
		   ,@pCreditlineId
		   ,@pOperationId
		   ,@pType
		   ,@pOldvalue
		   ,@pNewValue
		   ,@pCreation
		   ,@pCreationUser)  
END -- Update_CreditlineMovements
GO

IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'Update_Creditlines')
 DROP PROCEDURE Update_Creditlines
GO

CREATE PROCEDURE Update_Creditlines
           @pSiteId  INT  
		      ,@pId INT
          ,@pAccountId INT
          ,@pLimitAmount MONEY
          ,@pTTOAmount MONEY
          ,@pSpentAmount MONEY
          ,@pISOCode nvarchar(3)
          ,@pStatus INT
          ,@pIBAN nvarchar(34)
          ,@pXMLSigners XML
          ,@pCreation DATETIME
          ,@pUpdate DATETIME
          ,@pExpired DATETIME
          ,@pLastPayback DATETIME
AS
BEGIN   
IF EXISTS (SELECT   1 
             FROM   CREDIT_LINES
            WHERE   CL_ID =  @pId 
              AND   CL_SITE_ID =  @pSiteId)
              
UPDATE CREDIT_LINES
   SET CL_LIMIT_AMOUNT   = @pLimitAmount
      ,CL_TTO_AMOUNT     = @pTTOAmount
      ,CL_SPENT_AMOUNT   = @pSpentAmount
      ,CL_ISO_CODE       = @pISOCode
      ,CL_STATUS         = @pStatus
      ,CL_IBAN           = @pIBAN
      ,CL_XML_SIGNERS    = @pXMLSigners
      ,CL_CREATION       = @pCreation
      ,CL_UPDATE         = @pUpdate
      ,CL_EXPIRED        = @pExpired
      ,CL_LAST_PAYBACK   = @pLastPayback  
            WHERE   CL_ID =  @pId  
              AND   CL_SITE_ID    =  @pSiteId


ELSE
INSERT INTO CREDIT_LINES
           (CL_SITE_ID
           ,CL_ID
           ,CL_ACCOUNT_ID
           ,CL_LIMIT_AMOUNT
           ,CL_TTO_AMOUNT
           ,CL_SPENT_AMOUNT
           ,CL_ISO_CODE
           ,CL_STATUS
           ,CL_IBAN
           ,CL_XML_SIGNERS
           ,CL_CREATION
           ,CL_UPDATE  
           ,CL_EXPIRED 
           ,CL_LAST_PAYBACK)
     VALUES 
           (@pSiteId 
           ,@pId
           ,@pAccountID
           ,@pLimitAmount
           ,@pTTOAmount
           ,@pSpentAmount
           ,@pISOCode
           ,@pStatus
           ,@pIBAN
           ,@pXMLSigners
           ,@pCreation
           ,@pUpdate
           ,@pExpired
           ,@pLastPayback)  
END -- Update_Creditlines
GO

/******* TRIGGERS *******/
