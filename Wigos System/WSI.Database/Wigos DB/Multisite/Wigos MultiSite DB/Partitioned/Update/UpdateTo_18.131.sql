﻿/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_100]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 130;

SET @New_ReleaseId = 131;
SET @New_ScriptName = N'UpdateTo_18.131.sql';
SET @New_Description = N'New release v03.008.0011'; 

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO



/*********************************************************************************************************/

/**** GENERAL PARAM *****/


/**** VIEW *****/


/******* TABLES  *******/


/******* INDEXES *******/


/******* TRIGERS *******/


/******* RECORDS *******/


/******* PROCEDURES *******/


/*** This script will be executed only if TERMINALS_CONNECTED.TC_TIMESTAMP type is TIMESTAMP ***/

IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = N'TERMINALS_CONNECTED' and column_name = 'TC_TIMESTAMP' and DATA_TYPE = 'timestamp')
BEGIN
	    IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[terminals_connected]') AND type in (N'U'))
		    EXEC sp_rename 'terminals_connected','terminals_connected_old'

	    IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DF_terminals_connected_tc_status]') AND type in (N'D'))
	       ALTER TABLE dbo.terminals_connected_old  DROP CONSTRAINT DF_terminals_connected_tc_status

	    IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DF_terminals_connected_tc_connected]') AND type in (N'D'))
	       ALTER TABLE dbo.terminals_connected_old  DROP CONSTRAINT DF_terminals_connected_tc_connected

	    IF EXISTS (SELECT * FROM sys.key_constraints WHERE type = 'PK' AND parent_object_id = OBJECT_ID(N'[dbo].[terminals_connected_old]') AND Name = 'PK_terminals_connected')
	       ALTER TABLE dbo.terminals_connected_old  DROP CONSTRAINT PK_terminals_connected

	    CREATE TABLE [dbo].[terminals_connected](
	      [tc_site_id] [int] NOT NULL,
	      [tc_date] [datetime] NOT NULL,
	      [tc_terminal_id] [int] NOT NULL,
	      [tc_status] [int] NOT NULL CONSTRAINT [DF_terminals_connected_tc_status]  DEFAULT ((0)),
	      [tc_connected] [bit] NOT NULL CONSTRAINT [DF_terminals_connected_tc_connected]  DEFAULT ((1)),
	      [tc_timestamp] [bigint] NOT NULL,
	    CONSTRAINT [PK_terminals_connected] PRIMARY KEY CLUSTERED 
	    (
	      [tc_site_id] ASC,
	      [tc_date] ASC,
	      [tc_terminal_id] ASC
	    )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	    ) ON [PRIMARY]

	    IF EXISTS (SELECT * FROM sys.key_constraints WHERE type = 'PK' AND parent_object_id = OBJECT_ID(N'[dbo].[terminals_connected]') AND Name = 'PK_terminals_connected')
	       ALTER TABLE dbo.terminals_connected  DROP CONSTRAINT PK_terminals_connected

	    IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[terminals_connected]') and name = 'tc_master_id')
	      ALTER TABLE dbo.terminals_connected  ADD  tc_master_id INT null

	    UPDATE dbo.terminals_connected
	       SET tc_master_id = tc_terminal_id

	    IF EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[terminals_connected]') and name = 'tc_master_id')
	      ALTER TABLE dbo.terminals_connected  ALTER COLUMN  tc_master_id INT NOT NULL

	    ALTER TABLE dbo.terminals_connected 
	       ADD CONSTRAINT PK_terminals_connected
	       PRIMARY KEY(TC_SITE_ID, TC_DATE,TC_MASTER_ID)

	    INSERT INTO terminals_connected (TC_SITE_ID
		    , TC_DATE
		    , TC_MASTER_ID
		    , TC_TERMINAL_ID
		    , TC_STATUS
		    , TC_CONNECTED
		    , TC_TIMESTAMP)
	    (SELECT TC_SITE_ID
		    , TC_DATE
		    , TC_MASTER_ID
		    , TC_TERMINAL_ID
		    , TC_STATUS
		    , TC_CONNECTED
		    , CAST(TC_TIMESTAMP as BIGINT)
	    FROM [terminals_connected_old])
END
GO
