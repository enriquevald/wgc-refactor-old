/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_100]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 88;

SET @New_ReleaseId = 89;
SET @New_ScriptName = N'UpdateTo_18.089.sql';
SET @New_Description = N'';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/

/****** INDEXES ******/

/****** RECORDS ******/

-- GENERAL PARAM
IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='PlayerTracking.ExternalLoyaltyProgram' AND GP_SUBJECT_KEY = 'WebService.LogEnabled')
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('PlayerTracking.ExternalLoyaltyProgram', 'WebService.LogEnabled', '1')

IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='PlayerTracking.ExternalLoyaltyProgram' AND GP_SUBJECT_KEY = 'WebService.Uri')
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('PlayerTracking.ExternalLoyaltyProgram', 'WebService.Uri', 'http://winsystemsintl.com:7773/WigosService')     
   
IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='PlayerTracking.ExternalLoyaltyProgram' AND GP_SUBJECT_KEY = 'WebService.UserId')
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('PlayerTracking.ExternalLoyaltyProgram', 'WebService.UserId', 'resu@user')
   
IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='PlayerTracking.ExternalLoyaltyProgram' AND GP_SUBJECT_KEY = 'WebService.Password')
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('PlayerTracking.ExternalLoyaltyProgram', 'WebService.Password', '2016@reSu')   
  
/****** STORED *******/

--------------------------------------------------------------------------------
-- Copyright � 2013 Win Systems International
--------------------------------------------------------------------------------
--
--   MODULE NAME: Update_PointsAccountMovement.sql
--
--   DESCRIPTION: Update points to account & movement procedure
--
--        AUTHOR: Dani Dom�nguez
--
-- CREATION DATE: 07-MAR-2013
--
-- REVISION HISTORY:
--
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 07-MAR-2013 DDM    First release.
-- 07-MAR-2013 DDM    Add in/out parameters
-- 03-JUN-2013 DDM    Fixed Bug #812
--------------------------------------------------------------------------------


--------------------------------------------------------------------------------
-- PURPOSE: Update points to account & movement procedure
--
--  PARAMS:
--      - INPUT:
--           @pSiteId          
--           @pMovementId      
--           @pPlaySessionId   
--           @pAccountId       
--           @pTerminalId      
--           @pWcpSequenceId   
--           @pWcpTransactionId
--           @pDatetime        
--           @pType            
--           @pInitialBalance  
--           @pSubAmount       
--           @pAddAmount       
--           @pFinalBalance    
--           @pCashierId       
--           @pCashierName     
--           @pTperminalName   
--           @pOperationId     
--           @pDetails         
--           @pReasons         
--
--      - OUTPUT:
--           @pErrorCode           
--           @PointsSequenceId     
--           @Points               
--           @PointsStatus         
--           @HolderLevel          
--           @HolderLevelEntered   
--           @HolderLevelExpiration
--
-- RETURNS:
--
--   NOTES:
--
--------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Update_PointsAccountMovement]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Update_PointsAccountMovement]
GO

CREATE PROCEDURE [dbo].[Update_PointsAccountMovement]
	@pSiteId                    INT 
,	@pMovementId                BIGINT
,	@pPlaySessionId             BIGINT
,	@pAccountId                 BIGINT
,	@pTerminalId                INT
,	@pWcpSequenceId             BIGINT
,	@pWcpTransactionId          BIGINT
,	@pDatetime                  DATETIME
,	@pType                      INT
,	@pInitialBalance            MONEY
,	@pSubAmount                 MONEY
,	@pAddAmount                 MONEY
,	@pFinalBalance              MONEY
,	@pCashierId                 INT
,	@pCashierName               NVARCHAR(50)
,	@pTperminalName             NVARCHAR(50)
,	@pOperationId               BIGINT
,	@pDetails                   NVARCHAR(256)
,	@pReasons                   NVARCHAR(64)
, @pPlayerTrackingMode        INT
, @pErrorCode                 INT          OUTPUT
, @PointsSequenceId           BIGINT       OUTPUT
, @Points                     MONEY        OUTPUT
,	@pCenterInitialBalance      MONEY        OUTPUT

AS
BEGIN   
  DECLARE @LocalDelta      AS MONEY 
  DECLARE @IsDelta         AS BIT
  DECLARE @pCheckAccountId AS BIGINT -- For check if exists account
  
  SET @pErrorCode = 1
  SET @PointsSequenceId = NULL
  SET @Points = NULL
  SET @pCenterInitialBalance = NULL
  SET @IsDelta = 1
  SET @pCheckAccountId = NULL
 
  SET NOCOUNT ON; 
 
  SELECT   @pCenterInitialBalance = AC_POINTS
         , @pCheckAccountId       = AC_ACCOUNT_ID 
    FROM   ACCOUNTS
   WHERE   AC_ACCOUNT_ID = @pAccountId 
 
  IF ( @pCheckAccountId IS NULL)
  BEGIN
    SET @pErrorCode = 1
    RETURN
  END
 
  IF NOT EXISTS (SELECT 1 FROM ACCOUNT_MOVEMENTS WHERE AM_SITE_ID = @pSiteId AND AM_MOVEMENT_ID = @pMovementId) 
  BEGIN  
    INSERT INTO   ACCOUNT_MOVEMENTS
                ( [am_site_id]
                , [am_movement_id]
                , [am_play_session_id]
                , [am_account_id]
                , [am_terminal_id]
                , [am_wcp_sequence_id]
                , [am_wcp_transaction_id]
                , [am_datetime]
                , [am_type]
                , [am_initial_balance]
                , [am_sub_amount]
                , [am_add_amount]
                , [am_final_balance]
                , [am_cashier_id]
                , [am_cashier_name]
                , [am_terminal_name]
                , [am_operation_id]
                , [am_details]
                , [am_reasons])
         VALUES
                ( @pSiteId           
                , @pMovementId       
                , @pPlaySessionId   
                , @pAccountId        
                , @pTerminalId       
                , @pWcpSequenceId    
                , @pWcpTransactionId 
                , @pDatetime         
                , @pType             
                , @pInitialBalance  
                , @pSubAmount        
                , @pAddAmount        
                , @pFinalBalance     
                , @pCashierId        
                , @pCashierName      
                , @pTperminalName    
                , @pOperationId      
                , @pDetails          
                , @pReasons  )          
  
    SET @LocalDelta = ISNULL (@pAddAmount - @pSubAmount, 0)
    
    -- Points Gift Delivery/Points Gift Services/Points Status/Imported Points History
    IF (@pType = 39  OR @pType = 42  OR @pType = 67 OR @pType = 73)  SET @LocalDelta = 0 
    
    -- Multisite initial points
    IF (@pType = 101) SET @IsDelta    = 0 
        
    UPDATE   ACCOUNTS
       SET   AC_POINTS           = CASE WHEN (@pPlayerTrackingMode = 1) THEN AC_POINTS ELSE (CASE WHEN (@IsDelta = 1) THEN AC_POINTS + @LocalDelta ELSE @LocalDelta END) END
           , AC_MS_LAST_SITE_ID  = @pSiteId 
           , AC_LAST_ACTIVITY    = GETDATE()         
     WHERE   AC_ACCOUNT_ID       = @pAccountId 
    
  END
  
  SET @pErrorCode = 0
 
  SELECT   @PointsSequenceId     = AC_MS_POINTS_SEQ_ID 
         , @Points               = AC_POINTS
    FROM   ACCOUNTS
   WHERE   AC_ACCOUNT_ID = @pAccountId 
   
END -- Update_PointsAccountMovement

GO