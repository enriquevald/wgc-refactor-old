/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_100]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 58;

SET @New_ReleaseId = 59;
SET @New_ScriptName = N'UpdateTo_18.059.sql';
SET @New_Description = N'TITO Mode for MultiSite';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/

IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='TITO' AND GP_SUBJECT_KEY='TITOMode')
  INSERT INTO GENERAL_PARAMS  (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE) VALUES ('TITO','TITOMode','0')
GO

IF NOT EXISTS ( SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cage' AND GP_SUBJECT_KEY = 'Enabled')
  INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Cage', 'Enabled', '0')
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[play_sessions]') and name = 'ps_re_ticket_in')
  ALTER TABLE [dbo].[play_sessions] ADD [ps_re_ticket_in] [money] NULL
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[play_sessions]') and name = 'ps_promo_re_ticket_in')
  ALTER TABLE [dbo].[play_sessions] ADD [ps_promo_re_ticket_in] [money] NULL
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[play_sessions]') and name = 'ps_promo_nr_ticket_in')
  ALTER TABLE [dbo].[play_sessions] ADD [ps_promo_nr_ticket_in] [money] NULL
GO  
                       
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[play_sessions]') and name = 'ps_re_ticket_out')
  ALTER TABLE [dbo].[play_sessions] ADD [ps_re_ticket_out] [money] NULL
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[play_sessions]') and name = 'ps_promo_nr_ticket_out')
  ALTER TABLE [dbo].[play_sessions] ADD [ps_promo_nr_ticket_out] [money] NULL
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[play_sessions]') and name = 'ps_bills_in_amount')
  ALTER TABLE [dbo].[play_sessions] ADD [ps_bills_in_amount] [money] NULL
GO  
         
IF EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[play_sessions]') and name = 'ps_total_cash_in')
  ALTER TABLE [dbo].[play_sessions] DROP COLUMN [ps_total_cash_in]     
GO   
        
ALTER TABLE [dbo].[play_sessions] ADD [ps_total_cash_in] AS ((((([ps_initial_balance]+[ps_cash_in])+isnull([ps_re_ticket_in],(0)))+isnull([ps_promo_re_ticket_in],(0)))+isnull([ps_promo_nr_ticket_in],(0)))+isnull([ps_bills_in_amount],(0)))
GO

IF EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[play_sessions]') and name = 'ps_total_cash_out')
  ALTER TABLE [dbo].[play_sessions] DROP COLUMN [ps_total_cash_out]                                 
GO  
         
ALTER TABLE [dbo].[play_sessions] ADD [ps_total_cash_out] AS (((isnull([ps_final_balance],(0))+[ps_cash_out])+isnull([ps_re_ticket_out],(0)))+isnull([ps_promo_nr_ticket_out],(0)))                
GO

/****** INDEXES ******/

/****** RECORDS ******/

/******* STORED PROCEDURES *******/

  --------------------------------------------------------------------------------
  -- Copyright � 2013 Win Systems International
  --------------------------------------------------------------------------------
  --
  --   MODULE NAME: Insert_PlaySessions.sql
  --
  --   DESCRIPTION: Insert_PlaySessions
  --
  --        AUTHOR: Rafa Xandri
  --
  -- CREATION DATE: 08-MAR-2013
  --
  -- REVISION HISTORY:
  --
  -- Date        Author Description
  -- ----------- ------ ----------------------------------------------------------
  -- 26-MAR-2013 RXM    First release.  
  -- 22-JUL-2014 AMF    Tito Columns.
  -------------------------------------------------------------------------------- 
  
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Update_PlaySessions]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].Update_PlaySessions
GO  
  
CREATE PROCEDURE Update_PlaySessions 
  @pPlaySessionId  BIGINT
, @pAccountId  BIGINT
, @pTerminalId  INT  
, @pType  INT  
, @pTypedata  XML  
, @pStatus  INT  
, @pStarted  DATETIME  
, @pInitialBalance  MONEY  
, @pPlayedCount  INT  
, @pPlayedAmount  MONEY  
, @pWonCount  INT  
, @pWonAmount  MONEY  
, @pCashin  MONEY  
, @pCashout  MONEY  
, @pFinished  DATETIME  
, @pFinalBalance  MONEY  
, @pLocked  DATETIME  
, @pStandalone  BIT  
, @pPromo  BIT  
, @pWcpTransactionId  BIGINT  
, @pRedeemableCashin  MONEY  
, @pRedeemableCashout  MONEY  
, @pRedeemablePlayed  MONEY  
, @pRedeemableWon  MONEY  
, @pNonRedeemableCashin  MONEY  
, @pNonRedeemableCashout  MONEY  
, @pNonRedeemablePlayed  MONEY  
, @pNonRedeemableWon  MONEY  
, @pBalanceMismatch  BIT  
, @pSpentUsed  MONEY  
, @pCancellableAmount  MONEY  
, @pReportedBalanceMismatch  MONEY  
, @pReCashin  MONEY  
, @pPromoReCashin  MONEY  
, @pReCashout  MONEY  
, @pPromoReCashout  MONEY  
, @pSiteId  INT  
, @pTimeStamp Bigint
, @pReTicketIn MONEY
, @pPromoReTicketIn MONEY
, @pBillsInAmount MONEY
, @pReTicketOut MONEY
, @pPromoNrTicketIn MONEY
, @pPromoNrTicketOut MONEY

AS
BEGIN   

	IF EXISTS (SELECT   1 
				 FROM   PLAY_SESSIONS
				WHERE   PS_SITE_ID         =  @pSiteId 
				  AND   PS_PLAY_SESSION_ID =  @pPlaySessionId)

	BEGIN
	UPDATE   PLAY_SESSIONS
	   SET   PS_ACCOUNT_ID                = @pAccountId  
		   , PS_TERMINAL_ID               = @pTerminalId    
		   , PS_TYPE                      = @pType  
		   , PS_TYPE_DATA                 = @pTypedata  
		   , PS_STATUS                    = @pStatus  
		   , PS_STARTED                   = @pStarted  
		   , PS_INITIAL_BALANCE           =	@pInitialBalance  
		   , PS_PLAYED_COUNT              = @pPlayedCount  
		   , PS_PLAYED_AMOUNT             = @pPlayedAmount  
		   , PS_WON_COUNT                 = @pWonCount  
		   , PS_WON_AMOUNT                = @pWonAmount  
		   , PS_CASH_IN                   = @pCashin  
		   , PS_CASH_OUT                  = @pCashout  
		   , PS_FINISHED                  = @pFinished  
		   , PS_FINAL_BALANCE             = @pFinalBalance  
		   , PS_LOCKED                    = @pLocked  
		   , PS_STAND_ALONE               = @pStandalone  
		   , PS_PROMO                     = @pPromo  
		   , PS_WCP_TRANSACTION_ID        = @pWcpTransactionId  
		   , PS_REDEEMABLE_CASH_IN        = @pRedeemableCashin  
		   , PS_REDEEMABLE_CASH_OUT       = @pRedeemableCashout  
		   , PS_REDEEMABLE_PLAYED         = @pRedeemablePlayed  
		   , PS_REDEEMABLE_WON            =	@pRedeemableWon  
		   , PS_NON_REDEEMABLE_CASH_IN    = @pNonRedeemableCashin  
		   , PS_NON_REDEEMABLE_CASH_OUT   = @pNonRedeemableCashout  
		   , PS_NON_REDEEMABLE_PLAYED     = @pNonRedeemablePlayed  
		   , PS_NON_REDEEMABLE_WON        = @pNonRedeemableWon  
		   , PS_BALANCE_MISMATCH          = @pBalanceMismatch  
		   , PS_SPENT_USED                = @pSpentUsed  
		   , PS_CANCELLABLE_AMOUNT        = @pCancellableAmount  
		   , PS_REPORTED_BALANCE_MISMATCH = @pReportedBalanceMismatch  
		   , PS_RE_CASH_IN                = @pReCashin  
		   , PS_PROMO_RE_CASH_IN          = @pPromoReCashin  
		   , PS_RE_CASH_OUT               = @pReCashout  
		   , PS_PROMO_RE_CASH_OUT         = @pPromoReCashout
		   , PS_TIMESTAMP                 = @pTimeStamp 
			 , PS_RE_TICKET_IN							= @pReTicketIn
			 , PS_PROMO_RE_TICKET_IN				= @pPromoReTicketIn
			 , PS_BILLS_IN_AMOUNT						= @pBillsInAmount
			 , PS_RE_TICKET_OUT							= @pReTicketOut
			 , PS_PROMO_NR_TICKET_IN				= @pPromoNrTicketIn
			 , PS_PROMO_NR_TICKET_OUT				= @pPromoNrTicketOut
	 WHERE   PS_SITE_ID                   = @pSiteId 
	   AND   PS_PLAY_SESSION_ID           = @pPlaySessionId
	END
	ELSE
	BEGIN
	INSERT INTO   PLAY_SESSIONS
				( PS_PLAY_SESSION_ID 
				, PS_ACCOUNT_ID
				, PS_TERMINAL_ID
				, PS_TYPE
				, PS_TYPE_DATA
				, PS_STATUS
				, PS_STARTED
				, PS_INITIAL_BALANCE
				, PS_PLAYED_COUNT
				, PS_PLAYED_AMOUNT
				, PS_WON_COUNT
				, PS_WON_AMOUNT
				, PS_CASH_IN
				, PS_CASH_OUT
				, PS_FINISHED
				, PS_FINAL_BALANCE
				, PS_LOCKED
				, PS_STAND_ALONE
				, PS_PROMO
				, PS_WCP_TRANSACTION_ID
				, PS_REDEEMABLE_CASH_IN
				, PS_REDEEMABLE_CASH_OUT
				, PS_REDEEMABLE_PLAYED
				, PS_REDEEMABLE_WON
				, PS_NON_REDEEMABLE_CASH_IN
				, PS_NON_REDEEMABLE_CASH_OUT
				, PS_NON_REDEEMABLE_PLAYED
				, PS_NON_REDEEMABLE_WON
				, PS_BALANCE_MISMATCH
				, PS_SPENT_USED
				, PS_CANCELLABLE_AMOUNT
				, PS_REPORTED_BALANCE_MISMATCH
				, PS_RE_CASH_IN
				, PS_PROMO_RE_CASH_IN
				, PS_RE_CASH_OUT
				, PS_PROMO_RE_CASH_OUT
				, PS_SITE_ID
				, PS_TIMESTAMP
				, PS_RE_TICKET_IN 
				, PS_PROMO_RE_TICKET_IN 
				, PS_BILLS_IN_AMOUNT 
				, PS_RE_TICKET_OUT 
				, PS_PROMO_NR_TICKET_IN 
				, PS_PROMO_NR_TICKET_OUT)
		 VALUES
				( @pPlaySessionId
				, @pAccountId  
				, @pTerminalId    
				, @pType  
				, @pTypedata  
				, @pStatus  
				, @pStarted  
				, @pInitialBalance  
				, @pPlayedCount  
				, @pPlayedAmount  
				, @pWonCount  
				, @pWonAmount  
				, @pCashin  
				, @pCashout  
				, @pFinished  
				, @pFinalBalance  
				, @pLocked  
				, @pStandalone  
				, @pPromo  
				, @pWcpTransactionId  
				, @pRedeemableCashin  
				, @pRedeemableCashout  
				, @pRedeemablePlayed  
				, @pRedeemableWon  
				, @pNonRedeemableCashin  
				, @pNonRedeemableCashout  
				, @pNonRedeemablePlayed  
				, @pNonRedeemableWon  
				, @pBalanceMismatch  
				, @pSpentUsed  
				, @pCancellableAmount  
				, @pReportedBalanceMismatch  
				, @pReCashin  
				, @pPromoReCashin  
				, @pReCashout  
				, @pPromoReCashout    
				, @pSiteId
				, @pTimeStamp   
				, @pReTicketIn
				, @pPromoReTicketIn
				, @pBillsInAmount
				, @pReTicketOut
				, @pPromoNrTicketIn
				, @pPromoNrTicketOut)
	END
	
END
GO

--------------------------------------------------------------------------------
  -- Copyright � 2013 Win Systems International
  --------------------------------------------------------------------------------
  --
  --   MODULE NAME: Update_PlaySessionsForOldVersion.sql
  --
  --   DESCRIPTION: Update_PlaySessionsForOldVersion
  --
  --        AUTHOR: Rafa Xandri
  --
  -- CREATION DATE: 08-MAR-2013
  --
  -- REVISION HISTORY:
  --
  -- Date        Author Description
  -- ----------- ------ ----------------------------------------------------------
  -- 26-MAR-2013 RXM    First release.  
  -------------------------------------------------------------------------------- 
  
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Update_PlaySessionsForOldVersion]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].Update_PlaySessionsForOldVersion
GO    
  
CREATE PROCEDURE Update_PlaySessionsForOldVersion
  @pPlaySessionId  BIGINT
, @pAccountId  BIGINT
, @pTerminalId  INT  
, @pType  INT  
, @pTypedata  XML  
, @pStatus  INT  
, @pStarted  DATETIME  
, @pInitialBalance  MONEY  
, @pPlayedCount  INT  
, @pPlayedAmount  MONEY  
, @pWonCount  INT  
, @pWonAmount  MONEY  
, @pCashin  MONEY  
, @pCashout  MONEY  
, @pFinished  DATETIME  
, @pFinalBalance  MONEY  
, @pLocked  DATETIME  
, @pStandalone  BIT  
, @pPromo  BIT  
, @pWcpTransactionId  BIGINT  
, @pRedeemableCashin  MONEY  
, @pRedeemableCashout  MONEY  
, @pRedeemablePlayed  MONEY  
, @pRedeemableWon  MONEY  
, @pNonRedeemableCashin  MONEY  
, @pNonRedeemableCashout  MONEY  
, @pNonRedeemablePlayed  MONEY  
, @pNonRedeemableWon  MONEY  
, @pBalanceMismatch  BIT  
, @pSpentUsed  MONEY  
, @pCancellableAmount  MONEY  
, @pReportedBalanceMismatch  MONEY  
, @pReCashin  MONEY  
, @pPromoReCashin  MONEY  
, @pReCashout  MONEY  
, @pPromoReCashout  MONEY  
, @pSiteId  INT  
, @pTimeStamp Bigint
AS
BEGIN   

	IF EXISTS (SELECT   1 
				 FROM   PLAY_SESSIONS
				WHERE   PS_SITE_ID         =  @pSiteId 
				  AND   PS_PLAY_SESSION_ID =  @pPlaySessionId)

	BEGIN
	UPDATE   PLAY_SESSIONS
	   SET   PS_ACCOUNT_ID                = @pAccountId  
		   , PS_TERMINAL_ID               = @pTerminalId    
		   , PS_TYPE                      = @pType  
		   , PS_TYPE_DATA                 = @pTypedata  
		   , PS_STATUS                    = @pStatus  
		   , PS_STARTED                   = @pStarted  
		   , PS_INITIAL_BALANCE           =	@pInitialBalance  
		   , PS_PLAYED_COUNT              = @pPlayedCount  
		   , PS_PLAYED_AMOUNT             = @pPlayedAmount  
		   , PS_WON_COUNT                 = @pWonCount  
		   , PS_WON_AMOUNT                = @pWonAmount  
		   , PS_CASH_IN                   = @pCashin  
		   , PS_CASH_OUT                  = @pCashout  
		   , PS_FINISHED                  = @pFinished  
		   , PS_FINAL_BALANCE             = @pFinalBalance  
		   , PS_LOCKED                    = @pLocked  
		   , PS_STAND_ALONE               = @pStandalone  
		   , PS_PROMO                     = @pPromo  
		   , PS_WCP_TRANSACTION_ID        = @pWcpTransactionId  
		   , PS_REDEEMABLE_CASH_IN        = @pRedeemableCashin  
		   , PS_REDEEMABLE_CASH_OUT       = @pRedeemableCashout  
		   , PS_REDEEMABLE_PLAYED         = @pRedeemablePlayed  
		   , PS_REDEEMABLE_WON            =	@pRedeemableWon  
		   , PS_NON_REDEEMABLE_CASH_IN    = @pNonRedeemableCashin  
		   , PS_NON_REDEEMABLE_CASH_OUT   = @pNonRedeemableCashout  
		   , PS_NON_REDEEMABLE_PLAYED     = @pNonRedeemablePlayed  
		   , PS_NON_REDEEMABLE_WON        = @pNonRedeemableWon  
		   , PS_BALANCE_MISMATCH          = @pBalanceMismatch  
		   , PS_SPENT_USED                = @pSpentUsed  
		   , PS_CANCELLABLE_AMOUNT        = @pCancellableAmount  
		   , PS_REPORTED_BALANCE_MISMATCH = @pReportedBalanceMismatch  
		   , PS_RE_CASH_IN                = @pReCashin  
		   , PS_PROMO_RE_CASH_IN          = @pPromoReCashin  
		   , PS_RE_CASH_OUT               = @pReCashout  
		   , PS_PROMO_RE_CASH_OUT         = @pPromoReCashout
		   , PS_TIMESTAMP                 = @pTimeStamp 
	 WHERE   PS_SITE_ID                   = @pSiteId 
	   AND   PS_PLAY_SESSION_ID           = @pPlaySessionId
	END
	ELSE
	BEGIN
	INSERT INTO   PLAY_SESSIONS
				( PS_PLAY_SESSION_ID 
				, PS_ACCOUNT_ID
				, PS_TERMINAL_ID
				, PS_TYPE
				, PS_TYPE_DATA
				, PS_STATUS
				, PS_STARTED
				, PS_INITIAL_BALANCE
				, PS_PLAYED_COUNT
				, PS_PLAYED_AMOUNT
				, PS_WON_COUNT
				, PS_WON_AMOUNT
				, PS_CASH_IN
				, PS_CASH_OUT
				, PS_FINISHED
				, PS_FINAL_BALANCE
				, PS_LOCKED
				, PS_STAND_ALONE
				, PS_PROMO
				, PS_WCP_TRANSACTION_ID
				, PS_REDEEMABLE_CASH_IN
				, PS_REDEEMABLE_CASH_OUT
				, PS_REDEEMABLE_PLAYED
				, PS_REDEEMABLE_WON
				, PS_NON_REDEEMABLE_CASH_IN
				, PS_NON_REDEEMABLE_CASH_OUT
				, PS_NON_REDEEMABLE_PLAYED
				, PS_NON_REDEEMABLE_WON
				, PS_BALANCE_MISMATCH
				, PS_SPENT_USED
				, PS_CANCELLABLE_AMOUNT
				, PS_REPORTED_BALANCE_MISMATCH
				, PS_RE_CASH_IN
				, PS_PROMO_RE_CASH_IN
				, PS_RE_CASH_OUT
				, PS_PROMO_RE_CASH_OUT
				, PS_SITE_ID
				, PS_TIMESTAMP)
		 VALUES
				( @pPlaySessionId
				, @pAccountId  
				, @pTerminalId    
				, @pType  
				, @pTypedata  
				, @pStatus  
				, @pStarted  
				, @pInitialBalance  
				, @pPlayedCount  
				, @pPlayedAmount  
				, @pWonCount  
				, @pWonAmount  
				, @pCashin  
				, @pCashout  
				, @pFinished  
				, @pFinalBalance  
				, @pLocked  
				, @pStandalone  
				, @pPromo  
				, @pWcpTransactionId  
				, @pRedeemableCashin  
				, @pRedeemableCashout  
				, @pRedeemablePlayed  
				, @pRedeemableWon  
				, @pNonRedeemableCashin  
				, @pNonRedeemableCashout  
				, @pNonRedeemablePlayed  
				, @pNonRedeemableWon  
				, @pBalanceMismatch  
				, @pSpentUsed  
				, @pCancellableAmount  
				, @pReportedBalanceMismatch  
				, @pReCashin  
				, @pPromoReCashin  
				, @pReCashout  
				, @pPromoReCashout    
				, @pSiteId
				, @pTimeStamp   )
	END
	
END
GO
