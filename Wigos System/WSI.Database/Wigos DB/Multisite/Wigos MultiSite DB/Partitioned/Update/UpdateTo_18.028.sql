/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_100]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 27;

SET @New_ReleaseId = 28;
SET @New_ScriptName = N'UpdateTo_18.028.sql';
SET @New_Description = N'ELP TABLES';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/******* TABLES *******/
ALTER TABLE elp01_trackdata ALTER COLUMN et_external_account_id NUMERIC (20,0) NULL
GO

DECLARE @count bigint
SET @count = 0

     SELECT   @count = count(*) 
       FROM   information_schema.table_constraints TC
 INNER JOIN   information_schema.constraint_column_usage CC 
         ON   TC.Constraint_Name = CC.Constraint_Name
      WHERE   TC.constraint_type = 'Unique' 
        AND   TC.Constraint_Name = 'DF_elp01_trackdata_et_external_account_id'
		
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[elp01_trackdata]') AND name = N'DF_elp01_trackdata_et_external_account_id')
IF @count > 0 
  BEGIN
    ALTER TABLE [dbo].[elp01_trackdata] DROP CONSTRAINT [DF_elp01_trackdata_et_external_account_id]
  END
  ELSE
  BEGIN
    DROP INDEX [DF_elp01_trackdata_et_external_account_id] ON [dbo].[elp01_trackdata] WITH ( ONLINE = OFF )
  END
GO

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[elp01_trackdata]') AND name = N'IX_elp01_trackdata_et_external_account_id_NO_NULL')
  DROP INDEX [IX_elp01_trackdata_et_external_account_id_NO_NULL] ON [dbo].[elp01_trackdata] WITH ( ONLINE = OFF )
GO

/******* INDEXES ********/
CREATE UNIQUE NONCLUSTERED INDEX [IX_elp01_trackdata_et_external_account_id_NO_NULL] ON [dbo].[elp01_trackdata] 
(
   [et_external_account_id] ASC
)
WHERE ([et_external_account_id] IS NOT NULL)
WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO


