﻿USE [wgdb_100]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 131;

SET @New_ReleaseId = 132;
SET @New_ScriptName = N'2018-06-20 - UpdateTo_18.132.sql';
SET @New_Description = N'New release v03.008.0015'; 

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO



/*********************************************************************************************************/
/**** GENERAL PARAM *****/


/**** VIEW *****/


/******* TABLES  *******/


/******* INDEXES *******/


/******* TRIGERS *******/


/******* RECORDS *******/

-- ALARM CATALOG 262406
IF NOT EXISTS (SELECT 1 FROM ALARM_CATALOG WHERE ALCG_ALARM_CODE = 262406)
BEGIN
  INSERT ALARM_CATALOG VALUES (262406,  9, 0, N'Player activity at the gambling table', N'Player activity at the gambling table', 1)
  INSERT ALARM_CATALOG VALUES (262406, 10, 0, N'Actividad del jugador en la mesa de juego', N'Actividad del jugador en la mesa de juego', 1)
END

-- ALARM CATALOG PER CATEGORY 262406
IF NOT EXISTS (SELECT 1 FROM ALARM_CATALOG_PER_CATEGORY WHERE ALCC_ALARM_CODE = 262406 AND ALCC_CATEGORY = 62) 
BEGIN
  INSERT alarm_catalog_per_category VALUES (262406, 62, 0, GETDATE() )
END

-- ALARM CATALOG 262409
IF NOT EXISTS (SELECT 1 FROM ALARM_CATALOG WHERE ALCG_ALARM_CODE = 262409)
BEGIN
  INSERT ALARM_CATALOG VALUES (262409,  9, 0, N'Gambling table 3 day loss', N'Gambling table with loss of three days for (  Craps).', 1)
  INSERT ALARM_CATALOG VALUES (262409, 10, 0, N'Mesa 3 días de pérdida', N'Mesa con pérdida de tres días por ( Craps). ', 1)
END

-- ALARM CATALOG PER CATEGORY 262409
IF NOT EXISTS (SELECT 1 FROM ALARM_CATALOG_PER_CATEGORY WHERE ALCC_ALARM_CODE = 262409 AND ALCC_CATEGORY = 62) 
BEGIN
  INSERT alarm_catalog_per_category VALUES (262409, 62, 0, GETDATE() )
END


/******* PROCEDURES *******/


/******* TRIGGERS *******/



