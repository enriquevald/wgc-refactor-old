/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_100]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 47;

SET @New_ReleaseId = 48;
SET @New_ScriptName = N'UpdateTo_18.048.sql';
SET @New_Description = N'New task for site alarms.';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/

/* Create table site_alarms */
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[site_alarms]') AND type in (N'U'))
DROP TABLE [dbo].[site_alarms]
GO
CREATE TABLE [dbo].[site_alarms](
      [sa_site_id] [bigint] NOT NULL,
      [sa_alarm_id] [bigint] NOT NULL,
      [sa_source_code] [int] NOT NULL,
      [sa_source_id] [bigint] NOT NULL,
      [sa_source_name] [nvarchar](100) NOT NULL,
      [sa_alarm_code] [int] NOT NULL,
      [sa_alarm_name] [nvarchar](50) NOT NULL,
      [sa_alarm_description] [nvarchar](max) NULL,
      [sa_severity] [int] NOT NULL,
      [sa_reported] [datetime] NOT NULL,
      [sa_datetime] [datetime] NOT NULL,
      [sa_ack_datetime] [datetime] NULL,
      [sa_ack_user_id] [int] NULL,
      [sa_ack_user_name] [nvarchar](50) NULL,
      [sa_timestamp] [bigint] NOT NULL)
        
GO

/****** INDEXES ******/

--/**** INDEXES / PARTITION SCHEMA / PARTITION FUNCTION SECTION ****/
-- <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< PARTITIONED: site_alarms >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

--CREATE PARTITION SCHEMA 
IF  EXISTS (SELECT * FROM sys.partition_schemes WHERE name = N'ps_site_alarms')
DROP PARTITION SCHEME [ps_site_alarms]
GO

--CREATE PARTITION FUNCTION 
IF  EXISTS (SELECT * FROM sys.partition_functions WHERE name = N'pf_site_alarms')
DROP PARTITION FUNCTION [pf_site_alarms]
GO

DECLARE @CreatePartitionFunction nvarchar(max) = N'CREATE PARTITION FUNCTION [pf_site_alarms] (datetime) AS RANGE RIGHT FOR VALUES (';

DECLARE @listStr VARCHAR(MAX)
      SELECT @listStr = COALESCE(@listStr+''',' ,'') + ''''+ CONVERT(VARCHAR(8),DATEADD(m,DATEDIFF(m,0,al_reported),0),112)
      FROM alarms
      GROUP BY CONVERT(VARCHAR(8),DATEADD(m,DATEDIFF(m,0,al_reported),0),112)
      ORDER BY CONVERT(VARCHAR(8),DATEADD(m,DATEDIFF(m,0,al_reported),0),112)
SET @CreatePartitionFunction += @listStr + ''')';

EXEC sp_executesql @CreatePartitionFunction;

GO

CREATE PARTITION SCHEME [ps_site_alarms] 
AS PARTITION [pf_site_alarms] ALL TO ([PRIMARY]); --asignamos todas las particiones a un mismo Grupo de Ficheros (FileGroup).
GO


--CREATE PK AND INDEXES
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[site_alarms]') AND name = N'PK_site_alarms')
ALTER TABLE [dbo].[site_alarms] DROP CONSTRAINT [PK_site_alarms]
GO
ALTER TABLE [dbo].[site_alarms] ADD  CONSTRAINT [PK_site_alarms] PRIMARY KEY CLUSTERED
(
    [sa_site_id] ASC,
    [sa_alarm_id] ASC,
    [sa_reported] ASC
) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON ps_site_alarms(sa_reported)
GO

-- COMPRESSION SECTION
-- COMPRESS ALL PARTITIONS TABLE AND ALL INDEX
ALTER TABLE [dbo].[site_alarms] REBUILD PARTITION = ALL WITH(DATA_COMPRESSION = ROW )
GO
ALTER INDEX PK_site_alarms ON site_alarms REBUILD WITH ( DATA_COMPRESSION = ROW )
GO

ALTER TABLE dbo.elp01_trackdata ADD et_space_reported int NULL
GO

/******* STORED PROCEDURES *******/

  --------------------------------------------------------------------------------
  -- Copyright � 2013 Win Systems International
  --------------------------------------------------------------------------------
  --
  --   MODULE NAME: Update_SiteAlarms.sql
  --
  --   DESCRIPTION: Update_SiteAlarms
  --
  --        AUTHOR: Jos� Mart�nez
  --
  -- CREATION DATE: 15-ABR-2014
  --
  -- REVISION HISTORY:
  --
  -- Date        Author    Description
  -- ----------- --------- ----------------------------------------------------------
  -- 15-ABR-2014 JML & JMM First release.  
  -------------------------------------------------------------------------------- 
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Update_SiteAlarms]') AND type in (N'P', N'PC'))
   DROP PROCEDURE [dbo].[Update_SiteAlarms]
GO
/****** Insert/Update site alarms ******/
CREATE PROCEDURE   [dbo].[Update_SiteAlarms]
                   @pSiteid             INT
                 , @pAlarmId            BIGINT
                 , @pSourceCode         INT
                 , @pSourceId           BIGINT 
                 , @pSourceName         NVARCHAR(100)
                 , @pAlarmCode          INT
                 , @pAlarmName          NVARCHAR(50)
                 , @pAlarmDescription   NVARCHAR(MAX)
                 , @pSeverity           INT
                 , @pReported           DATETIME
                 , @pDatetime           DATETIME
                 , @pAckDatetime        DATETIME
                 , @pAckUserId          INT
                 , @pAckUserName        NVARCHAR(50)
                 , @pTimestamp          BIGINT
AS
BEGIN   
  IF EXISTS (SELECT   1 
               FROM   SITE_ALARMS 
              WHERE   SA_SITE_ID  = @pSiteId 
                AND   SA_ALARM_ID = @pAlarmId
                AND   SA_REPORTED = @pReported)
     UPDATE   SITE_ALARMS
        SET   SA_SITE_ID           = @pSiteid
            , SA_ALARM_ID          = @pAlarmId
            , SA_SOURCE_CODE       = @pSourceCode
            , SA_SOURCE_ID         = @pSourceId
            , SA_SOURCE_NAME       = @pSourceName
            , SA_ALARM_CODE        = @pAlarmCode
            , SA_ALARM_NAME        = @pAlarmName
            , SA_ALARM_DESCRIPTION = @pAlarmDescription
            , SA_SEVERITY          = @pSeverity
            , SA_REPORTED          = @pReported
            , SA_DATETIME          = @pDatetime
            , SA_ACK_DATETIME      = @pAckDatetime
            , SA_ACK_USER_ID       = @pAckUserId
            , SA_ACK_USER_NAME     = @pAckUserName
            , SA_TIMESTAMP         = @pTimestamp
      WHERE   SA_SITE_ID  = @pSiteId
        AND   SA_ALARM_ID = @pAlarmId
        AND   SA_REPORTED = @pReported
  ELSE
    INSERT INTO   SITE_ALARMS
                ( SA_SITE_ID
                , SA_ALARM_ID
                , SA_SOURCE_CODE
                , SA_SOURCE_ID
                , SA_SOURCE_NAME
                , SA_ALARM_CODE
                , SA_ALARM_NAME
                , SA_ALARM_DESCRIPTION
                , SA_SEVERITY
                , SA_REPORTED
                , SA_DATETIME
                , SA_ACK_DATETIME
                , SA_ACK_USER_ID
                , SA_ACK_USER_NAME
                , SA_TIMESTAMP )
         VALUES ( @pSiteid
                , @pAlarmId
                , @pSourceCode
                , @pSourceId
                , @pSourceName
                , @pAlarmCode
                , @pAlarmName
                , @pAlarmDescription
                , @pSeverity
                , @pReported
                , @pDatetime
                , @pAckDatetime
                , @pAckUserId
                , @pAckUserName
                , @pTimestamp)      
END -- Update_SiteAlarms
GO


--------------------------------------------------------------------------------
-- Copyright � 2013 Win Systems International
--------------------------------------------------------------------------------
--
--   MODULE NAME: Update_CashierMovementsGrupedByHour.sql
--
--   DESCRIPTION: Update_CashierMovementsGrupedByHour
--
--        AUTHOR: Jos� Mart�nez
--
-- CREATION DATE: 27-FEB-2014
--
-- REVISION HISTORY:
--
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 27-FEB-2014 JML    First release.  
-------------------------------------------------------------------------------- 
  
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Update_CashierMovementsGrupedByHour]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[Update_CashierMovementsGrupedByHour]
GO
 
CREATE PROCEDURE  [dbo].[Update_CashierMovementsGrupedByHour]
                  @pSiteId               INT  
                , @pDate                 DATETIME
                , @pType                 INT
                , @pSubType              INT
                , @pCurrencyIsoCode      NVARCHAR(3)
                , @pCurrencyDenomination MONEY
                , @pTypeCount            INT
                , @pSubAmount            MONEY
                , @pAddAmount            MONEY
                , @pAuxAmount            MONEY
                , @pInitialBalance       MONEY
                , @pFinalBalance         MONEY
                , @pTimeStamp            BIGINT
                , @pUniqueId             BIGINT
AS
BEGIN   

  IF EXISTS (SELECT   1 
               FROM   CASHIER_MOVEMENTS_GROUPED_BY_HOUR
              WHERE   CM_SITE_ID      = @pSiteId 
                AND   CM_UNIQUE_ID    = @pUniqueId )
  BEGIN
      UPDATE   CASHIER_MOVEMENTS_GROUPED_BY_HOUR                   
         SET   CM_DATE                  = @pDate                   
             , CM_TYPE                  = @pType                   
             , CM_SUB_TYPE              = @pSubType                
             , CM_CURRENCY_ISO_CODE     = @pCurrencyIsoCode        
             , CM_CURRENCY_DENOMINATION = @pCurrencyDenomination   
             , CM_TYPE_COUNT            = @pTypeCount              
             , CM_SUB_AMOUNT            = @pSubAmount              
             , CM_ADD_AMOUNT            = @pAddAmount              
             , CM_AUX_AMOUNT            = @pAuxAmount              
             , CM_INITIAL_BALANCE       = @pInitialBalance         
             , CM_FINAL_BALANCE         = @pFinalBalance           
             , CM_TIMESTAMP             = @pTimeStamp              
       WHERE   CM_SITE_ID    = @pSiteId                        
         AND   CM_UNIQUE_ID  = @pUniqueId 
  END
  ELSE
  BEGIN
      INSERT INTO   CASHIER_MOVEMENTS_GROUPED_BY_HOUR
                  ( CM_SITE_ID
                  , CM_DATE
                  , CM_TYPE
                  , CM_SUB_TYPE
                  , CM_CURRENCY_ISO_CODE
                  , CM_CURRENCY_DENOMINATION
                  , CM_TYPE_COUNT
                  , CM_SUB_AMOUNT
                  , CM_ADD_AMOUNT
                  , CM_AUX_AMOUNT
                  , CM_INITIAL_BALANCE
                  , CM_FINAL_BALANCE
                  , CM_TIMESTAMP 
                  , CM_UNIQUE_ID )
          VALUES
                  ( @pSiteId    
                  , @pDate  
                  , @pType 
                  , @pSubType 
                  , @pCurrencyIsoCode 
                  , @pCurrencyDenomination 
                  , @pTypeCount 
                  , @pSubAmount 
                  , @pAddAmount 
                  , @pAuxAmount 
                  , @pInitialBalance 
                  , @pFinalBalance 
                  , @pTimeStamp  
                  , @pUniqueId )
  END
END
GO 

/****** RECORDS ******/

/* TASK */
SELECT ST_SITE_ID AS SITE_ID INTO #CURRENT_SITES FROM MS_SITE_TASKS GROUP BY ST_SITE_ID
INSERT INTO MS_SITE_TASKS (ST_TASK_ID, ST_ENABLED, ST_INTERVAL_SECONDS, ST_MAX_ROWS_TO_UPLOAD, ST_SITE_ID) 
                   SELECT          64,          1,                 900,                  1000,    SITE_ID  FROM #CURRENT_SITES;
DROP TABLE #CURRENT_SITES
GO

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY= 'ExternalLoyaltyProgram.Mode01' AND GP_SUBJECT_KEY='NotifySpaceChangesInAnonymousAccounts')
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('ExternalLoyaltyProgram.Mode01','NotifySpaceChangesInAnonymousAccounts','0')
GO

--
-- APPLY CHANGES IN PARTITION JOB 
--

USE [msdb];

EXEC dbo.sp_update_jobstep
            @job_name=N'Job_Add_partition_tables',
            @step_id=4, 
            @cmdexec_success_code=0, 
            @on_success_action=3, 
            @on_success_step_id=0, 
            @on_fail_action=3, 
            @on_fail_step_id=0, 
            @retry_attempts=2, 
            @retry_interval=60, 
            @os_run_priority=0, @subsystem=N'TSQL', 
            @command=N'EXEC Add_Partition_Tables @TableName=''wwp_sessions'', @TypeRange=''MONTH''', 
            @database_name=N'wgdb_100', 
            @flags=0;
GO
EXEC msdb.dbo.sp_add_jobstep 
            @job_name=N'Job_Add_partition_tables',
            @step_name=N'EXEC Add_Partition_Tables @TableName=''site_alarms'', @TypeRange=''MONTH''', 
            @step_id=5, 
            @cmdexec_success_code=0, 
            @on_success_action=1, 
            @on_success_step_id=0, 
            @on_fail_action=2, 
            @on_fail_step_id=0, 
            @retry_attempts=2, 
            @retry_interval=60, 
            @os_run_priority=0, @subsystem=N'TSQL', 
            @command=N'EXEC Add_Partition_Tables @TableName=''site_alarms'', @TypeRange=''MONTH''', 
            @database_name=N'wgdb_100', 
            @flags=0 ;
GO