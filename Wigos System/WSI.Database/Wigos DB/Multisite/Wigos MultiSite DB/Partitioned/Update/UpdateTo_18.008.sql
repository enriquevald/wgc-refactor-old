/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_100]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 7;

SET @New_ReleaseId = 8;
SET @New_ScriptName = N'UpdateTo_18.008.sql';
SET @New_Description = N'DELETE   MS_SITE_TASKS; UPDATE   MS_SITE_TASKS ';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/


/****** CONSTRAINTS ******/


/****** INDEXES ******/


/****** STORED PROCEDURES ******/


/****** TRIGGERS ******/


/****** RECORDS ******/ 

-- DownloadPoints_Site,DownloadPersonalInfo_Card,DownloadPersonalInfo_Site
DELETE   MS_SITE_TASKS
WHERE   st_task_id IN(21,31,32)   
GO
-- UploadPointsMovements,DownloadPoints_All,DownloadPersonalInfo_All
UPDATE   MS_SITE_TASKS 
   SET   ST_MAX_ROWS_TO_UPLOAD = 1000
WHERE   ST_TASK_ID IN (11,22,33)
GO
-- UploadPersonalInfo
UPDATE   MS_SITE_TASKS 
   SET   ST_MAX_ROWS_TO_UPLOAD = 200
WHERE   ST_TASK_ID = 12
GO
--TaskConfiguration
UPDATE   MS_SITE_TASKS 
   SET   ST_INTERVAL_SECONDS = 60 
       , ST_MAX_ROWS_TO_UPLOAD = 500 
 WHERE   ST_TASK_ID = 1 
GO
--UploadPointsMovements , UploadPersonalInfo
UPDATE   MS_SITE_TASKS 
   SET   ST_INTERVAL_SECONDS = 180
       , ST_MAX_ROWS_TO_UPLOAD = 200
WHERE   ST_TASK_ID IN (11,12) 
GO
-- DownloadPoints_All, DownloadPersonalInfo_All
UPDATE   MS_SITE_TASKS 
   SET   ST_INTERVAL_SECONDS = 600 
       , ST_MAX_ROWS_TO_UPLOAD = 500  
 WHERE   ST_TASK_ID IN (22,33)  
GO









