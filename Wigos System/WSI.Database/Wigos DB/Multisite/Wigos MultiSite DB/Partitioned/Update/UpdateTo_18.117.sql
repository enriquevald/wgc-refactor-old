/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_100]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 116;

SET @New_ReleaseId = 117;
SET @New_ScriptName = N'UpdateTo_18.117.sql';
SET @New_Description = N'New release v03.007.0017'; 

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/**** GENERAL PARAM *****/



/******* TABLES  *******/
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[report_tool_config]') AND type in (N'U'))
BEGIN
	CREATE TABLE [dbo].[report_tool_config](
		[rtc_report_tool_id] [int] IDENTITY(1,1) NOT NULL,
		[rtc_form_id] [int] NOT NULL,
		[rtc_location_menu] [int] NOT NULL,
		[rtc_report_name] [xml] NOT NULL,
		[rtc_store_name] [nvarchar](200) NOT NULL,
		[rtc_design_filter] [xml] NOT NULL,
		[rtc_design_sheets] [xml] NOT NULL,
		[rtc_mailing] [int] NOT NULL,
		[rtc_status] [int] NOT NULL,
		[rtc_mode_type] [int] NOT NULL,
		[rtc_html_header] [xml] NULL,
		[rtc_html_footer] [xml] NULL,
	PRIMARY KEY CLUSTERED 
	(
		[rtc_report_tool_id] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]

END
GO

IF NOT EXISTS (SELECT * FROM SYS.COLUMNS WHERE OBJECT_ID = OBJECT_ID(N'[DBO].[TERMINALS]') AND NAME = 'te_number_lines')
	ALTER TABLE [dbo].[TERMINALS] ADD te_number_lines NVARCHAR(50) NULL
GO

IF NOT EXISTS (SELECT * FROM SYS.COLUMNS WHERE OBJECT_ID = OBJECT_ID(N'[DBO].[TERMINALS]') AND NAME = 'te_contract_id')
	ALTER TABLE [dbo].[TERMINALS] ADD te_contract_id NVARCHAR(50) NULL
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[game_types]') AND type in (N'U'))
BEGIN
	CREATE TABLE [dbo].[game_types](
		[gt_game_type] [int] NOT NULL,
		[gt_name] [nvarchar](50) NOT NULL,
		[gt_language_id] [int] NOT NULL,
	 CONSTRAINT [PK_game_types] PRIMARY KEY CLUSTERED 
	(
		[gt_game_type] ASC,
		[gt_language_id] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]


	ALTER TABLE [dbo].[game_types] ADD  CONSTRAINT [DF_game_types_gt_language_id]  DEFAULT ((10)) FOR [gt_language_id]

END
GO

/******* RECORDS *******/

-- GAME_TYPES
IF NOT EXISTS(SELECT 1 FROM GAME_TYPES WHERE GT_GAME_TYPE = 0)
BEGIN
	INSERT INTO GAME_TYPES VALUES (0,'---',10)
	INSERT INTO GAME_TYPES VALUES (0,'---',9)
END
GO

IF NOT EXISTS(SELECT 1 FROM GAME_TYPES WHERE GT_GAME_TYPE = 1)
BEGIN
	INSERT INTO GAME_TYPES VALUES (1,'Clase II',10)
	INSERT INTO GAME_TYPES VALUES (1,'Class II',9)
END
GO

IF NOT EXISTS(SELECT 1 FROM GAME_TYPES WHERE GT_GAME_TYPE = 2)
BEGIN
	INSERT INTO GAME_TYPES VALUES (2,'Clase III',10)
	INSERT INTO GAME_TYPES VALUES (2,'Class III',9)
END
GO

IF NOT EXISTS(SELECT 1 FROM GAME_TYPES WHERE GT_GAME_TYPE = 3)
BEGIN
	INSERT INTO GAME_TYPES VALUES (3,'Latin Bingo',10)
	INSERT INTO GAME_TYPES VALUES (3,'Latin Bingo',9)
END
GO

IF NOT EXISTS(SELECT 1 FROM GAME_TYPES WHERE GT_GAME_TYPE = 4)
BEGIN
	INSERT INTO GAME_TYPES VALUES (4,'Mesas de Juego',10)
	INSERT INTO GAME_TYPES VALUES (4,'Gaming Tables',9)
END
GO


/******* PROCEDURES *******/



/******* TRIGGERS *******/
