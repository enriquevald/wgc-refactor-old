/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_100]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 110;

SET @New_ReleaseId = 111;
SET @New_ScriptName = N'UpdateTo_18.111.sql';
SET @New_Description = N'New release v03.007.0001'; 

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/**** GENERAL PARAM *****/

/******* TABLES  *******/


/******* RECORDS *******/
DELETE FROM ALARM_CATALOG WHERE ALCG_ALARM_CODE IN (SELECT ALCC_ALARM_CODE 
                                                      FROM ALARM_CATALOG_PER_CATEGORY 
                                                     WHERE ALCC_CATEGORY IN (SELECT ALC_CATEGORY_ID 
                                                                               FROM ALARM_CATEGORIES 
                                                                              WHERE ALC_ALARM_GROUP_ID = 6))
DELETE FROM ALARM_CATALOG_PER_CATEGORY WHERE ALCC_CATEGORY IN (SELECT ALC_CATEGORY_ID 
                                                                 FROM ALARM_CATEGORIES 
                                                                WHERE ALC_ALARM_GROUP_ID = 6)
DELETE FROM ALARM_CATEGORIES WHERE ALC_ALARM_GROUP_ID = 6
DELETE FROM ALARM_GROUPS WHERE ALG_ALARM_GROUP_ID = 6
GO

-- ALARM GROUPS
IF NOT EXISTS (SELECT 1 FROM ALARM_GROUPS WHERE ALG_ALARM_GROUP_ID = 6)
BEGIN
  INSERT INTO ALARM_GROUPS VALUES(6, 9,  0, N'Gambling tables', '', 1)            
  INSERT INTO ALARM_GROUPS VALUES(6, 10, 0, N'Mesas de juego',  '', 1)
END
GO

-- ALARM CATEGORIES                    
IF NOT EXISTS (SELECT 1 FROM ALARM_CATEGORIES WHERE ALC_ALARM_GROUP_ID = 6 AND ALC_CATEGORY_ID = 62)
BEGIN
  INSERT INTO ALARM_CATEGORIES VALUES(62, 9,  6, 0, N'Operations',  '', 1)
  INSERT INTO ALARM_CATEGORIES VALUES(62, 10, 6, 0, N'Operaciones', '', 1)
END
GO

-- ALARM CATALOG 262400
IF NOT EXISTS (SELECT 1 FROM ALARM_CATALOG WHERE ALCG_ALARM_CODE = 262400)
BEGIN
  INSERT ALARM_CATALOG VALUES ( 262400,  9, 0, N'Bets by customer', N'Bets by customer', 1)
  INSERT ALARM_CATALOG VALUES ( 262400, 10, 0,  N'Apuestas por cliente', N'Apuestas por cliente', 1)
END
GO

-- ALARM CATALOG PER CATEGORY 262400
IF NOT EXISTS (SELECT 1 FROM ALARM_CATALOG_PER_CATEGORY WHERE ALCC_ALARM_CODE = 262400 AND ALCC_CATEGORY = 62) 
BEGIN
  INSERT alarm_catalog_per_category VALUES (262400, 62, 0, GETDATE() )
END
GO

-- ALARM CATALOG 262401
IF NOT EXISTS (SELECT 1 FROM ALARM_CATALOG WHERE ALCG_ALARM_CODE = 262401)
BEGIN
  INSERT ALARM_CATALOG VALUES ( 262401,  9, 0, N'Gambling table with session loss', N'Gambling table with session loss', 1)
  INSERT ALARM_CATALOG VALUES ( 262401, 10, 0, N'Mesa con p�rdida por sesi�n', N'Mesa con p�rdida por sesi�n', 1)
END
GO

-- ALARM CATALOG PER CATEGORY 262401
IF NOT EXISTS (SELECT 1 FROM ALARM_CATALOG_PER_CATEGORY WHERE ALCC_ALARM_CODE = 262401 AND ALCC_CATEGORY = 62) 
BEGIN
  INSERT alarm_catalog_per_category VALUES (262401, 62, 0, GETDATE() )
END
GO

-- ALARM CATALOG 262402
IF NOT EXISTS (SELECT 1 FROM ALARM_CATALOG WHERE ALCG_ALARM_CODE = 262402)
BEGIN
  INSERT ALARM_CATALOG VALUES ( 262402,  9, 0, N'Investments made with credit / debit card', N'Investments made with credit / debit card', 1)
  INSERT ALARM_CATALOG VALUES ( 262402, 10, 0, N'Inversiones realizadas con tarjeta cr�dito/d�bito', N'Inversiones realizadas con tarjeta cr�dito/d�bito', 1)
END
GO

-- ALARM CATALOG PER CATEGORY 262402
IF NOT EXISTS (SELECT 1 FROM ALARM_CATALOG_PER_CATEGORY WHERE ALCC_ALARM_CODE = 262402 AND ALCC_CATEGORY = 62) 
BEGIN
  INSERT alarm_catalog_per_category VALUES (262402, 62, 0, GETDATE() )
END
GO

-- ALARM CATALOG 262403
IF EXISTS (SELECT 1 FROM ALARM_CATALOG WHERE ALCG_ALARM_CODE = 262403)
BEGIN
  DELETE FROM ALARM_CATALOG WHERE ALCG_ALARM_CODE = 262403
END
IF NOT EXISTS (SELECT 1 FROM ALARM_CATALOG WHERE ALCG_ALARM_CODE = 262403)
BEGIN
  INSERT ALARM_CATALOG VALUES ( 262403,  9, 0, N'Player sells chips without purchasing them', N'Customer gets paid without a Drop (purchase)', 1)
  INSERT ALARM_CATALOG VALUES ( 262403, 10, 0, N'Cliente vende fichas sin comprarlas', N'Cliente realiza cobro sin contar con Drop (compra)', 1)
END
GO

-- ALARM CATALOG PER CATEGORY 262403
IF NOT EXISTS (SELECT 1 FROM ALARM_CATALOG_PER_CATEGORY WHERE ALCC_ALARM_CODE = 262403 AND ALCC_CATEGORY = 62) 
BEGIN
  INSERT alarm_catalog_per_category VALUES (262403, 62, 0, GETDATE() )
END
GO

-- ALARM CATALOG 262404
IF NOT EXISTS (SELECT 1 FROM ALARM_CATALOG WHERE ALCG_ALARM_CODE = 262404)
BEGIN
  INSERT ALARM_CATALOG VALUES (262404,  9, 0, N'Variation of customer bet', N'Variation of customer bet per session in a given period', 1)
  INSERT ALARM_CATALOG VALUES (262404, 10, 0, N'Variaci�n de apuesta cliente', N'Variaci�n de apuesta cliente por sesi�n en determinado per�odo', 1)
END
GO

-- ALARM CATALOG PER CATEGORY 262404
IF NOT EXISTS (SELECT 1 FROM ALARM_CATALOG_PER_CATEGORY WHERE ALCC_ALARM_CODE = 262404 AND ALCC_CATEGORY = 62) 
BEGIN
  INSERT alarm_catalog_per_category VALUES (262404, 62, 0, GETDATE() )
END
GO

-- ALARM CATALOG 262405
IF NOT EXISTS (SELECT 1 FROM ALARM_CATALOG WHERE ALCG_ALARM_CODE = 262405)
BEGIN
  INSERT ALARM_CATALOG VALUES (262405,  9, 0, N'Purchase time limit', N'Payment done by player under minutes ', 1)
  INSERT ALARM_CATALOG VALUES (262405, 10, 0, N'L�mite de tiempo para compras', N'Jugador: realiza cobro en un tiempo menor a minutos', 1)
END
GO

-- ALARM CATALOG PER CATEGORY 262405
IF NOT EXISTS (SELECT 1 FROM ALARM_CATALOG_PER_CATEGORY WHERE ALCC_ALARM_CODE = 262405 AND ALCC_CATEGORY = 62) 
BEGIN
  INSERT alarm_catalog_per_category VALUES (262405, 62, 0, GETDATE() )
END
GO

-- ALARM CATALOG 262407
IF NOT EXISTS (SELECT 1 FROM ALARM_CATALOG WHERE ALCG_ALARM_CODE = 262407)
BEGIN
  INSERT ALARM_CATALOG VALUES (262407,  9, 0, N'Amount purchased in one operation', N'Purchase from : (1 single exhibition)', 1)
  INSERT ALARM_CATALOG VALUES (262407, 10, 0, N'Importe comprado en una operaci�n', N'Compras de : (1 sola exhibici�n)', 1)
END
GO

-- ALARM CATALOG PER CATEGORY 262407
IF NOT EXISTS (SELECT 1 FROM ALARM_CATALOG_PER_CATEGORY WHERE ALCC_ALARM_CODE = 262407 AND ALCC_CATEGORY = 62) 
BEGIN
  INSERT alarm_catalog_per_category VALUES (262407, 62, 0, GETDATE() )
END
GO

-- ALARM CATALOG 262408
IF NOT EXISTS (SELECT 1 FROM ALARM_CATALOG WHERE ALCG_ALARM_CODE = 262408)
BEGIN
  INSERT ALARM_CATALOG VALUES (262408,  9, 0, N'Amount paid in one operation', N'Payment from : (1 single exhibition)', 1)
  INSERT ALARM_CATALOG VALUES (262408, 10, 0, N'Importe pagado en una operaci�n', N'Pagos de : (1 sola exhibici�n) ', 1)
END
GO

-- ALARM CATALOG PER CATEGORY 262408
IF NOT EXISTS (SELECT 1 FROM ALARM_CATALOG_PER_CATEGORY WHERE ALCC_ALARM_CODE = 262408 AND ALCC_CATEGORY = 62) 
BEGIN
  INSERT alarm_catalog_per_category VALUES (262408, 62, 0, GETDATE() )
END
GO

/******* PROCEDURES *******/


/******* TRIGGERS *******/
