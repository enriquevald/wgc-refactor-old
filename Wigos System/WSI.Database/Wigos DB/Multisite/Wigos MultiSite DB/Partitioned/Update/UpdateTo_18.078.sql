/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_100]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 77;

SET @New_ReleaseId = 78;
SET @New_ScriptName = N'UpdateTo_18.078.sql';
SET @New_Description = N'new alarms catalog, new task SellsAndBuys';
/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/

CREATE TABLE [dbo].[task68_work_data](
      [twd_site_id] [int] NOT NULL,
      [twd_operation_id] [bigint] NOT NULL,
      [twd_datetime] [datetime] NOT NULL,
      [twd_account_id] [bigint] NOT NULL,
      [twd_external_reference] [numeric](15,0) NULL,
      [twd_external_card_number] [numeric](20,0) NULL,
      [twd_operation_type] [int] NOT NULL,
      [twd_total_cash_in] [money]  NULL,
      [twd_cash_in_split1] [money]  NULL,
      [twd_cash_in_split2] [money]  NULL,
      [twd_cash_in_tax1] [money]  NULL,
      [twd_cash_in_tax2] [money]  NULL,
      [twd_total_cash_out] [money]  NULL,
      [twd_devolution] [money]  NULL,
      [twd_prize] [money]  NULL,
      [twd_isr1] [money]  NULL,
      [twd_isr2] [money]  NULL,
      [twd_cash_out_money] [money]  NULL,
      [twd_cash_out_check] [money]  NULL,
      [twd_rfc] [nvarchar](20) NULL,
      [twd_curp] [nvarchar](20) NULL,
      [twd_name] [nvarchar](200) NULL,
      [twd_address] [nvarchar](200) NULL,
      [twd_city] [nvarchar](50) NULL,
      [twd_postal_code] [nvarchar](10) NULL,
      [twd_evidence_rfc] [nvarchar](20) NULL,
      [twd_evidence_curp] [nvarchar](20) NULL,
      [twd_evidence_name] [nvarchar](200) NULL,
      [twd_evidence_addres] [nvarchar](200) NULL,
      [twd_evidence_city] [nvarchar](50) NULL,
      [twd_evidence_postal_code] [nvarchar](10) NULL,
      [twd_evidence] [bit] NULL,
      [twd_document_id] [int] NULL,
      [twd_retention_rounding] [money]  NULL,
      [twd_service_charge] [money]  NULL,
      [twd_decimal_rounding] [money]  NULL,
      [twd_cashier_session_id] [bigint] NOT NULL,
      [twd_prize_above] [money]  NULL,
      [twd_cashier_name] [nvarchar](50) NOT NULL,
      [twd_prize_expired] [money]  NULL,
      [twd_voucher_id] [bigint] NULL,
      [twd_folio] [bigint] NULL,
      [twd_promotional_amount] [money]  NULL,
      [twd_payment_type_code] [nvarchar](10) NULL,
      [twd_payment_type] [nvarchar](50) NULL,
      [twd_recharge_points_prize] [money]  NULL,
CONSTRAINT [pk_task68_work_data] PRIMARY KEY CLUSTERED 
(
      [twd_site_id] ASC,
      [twd_operation_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

/****** INDEXES ******/

/****** RECORDS ******/

-- Scripts pertenece a la historia 481
IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'AntiMoneyLaundering' AND GP_SUBJECT_KEY = 'ExternalControl.Enabled')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE, GP_MS_DOWNLOAD_TYPE) VALUES ('AntiMoneyLaundering', 'ExternalControl.Enabled', '0', 1)
GO

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'AntiMoneyLaundering' AND GP_SUBJECT_KEY = 'ExternalControl.FileExpirationDays')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE, GP_MS_DOWNLOAD_TYPE) VALUES ('AntiMoneyLaundering', 'ExternalControl.FileExpirationDays', '0', 1)
GO

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'AntiMoneyLaundering' AND GP_SUBJECT_KEY = 'ExternalControl.BlockDescription')
	INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE, GP_MS_DOWNLOAD_TYPE) VALUES ('AntiMoneyLaundering', 'ExternalControl.BlockDescription', 'por PLD (externo)', 1)
GO

-- Scripts pertenece a la historia 327 del TFS
-- Codere M�xico: Fiscal - 46: Cambiar nombre de los clientes an�nimos en los tickets de caja
IF  NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier.Voucher' AND GP_SUBJECT_KEY ='AnonymousAccount.Description')
  IF (SELECT GP_KEY_VALUE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Cashier' AND GP_SUBJECT_KEY ='Language') = 'es'
    BEGIN
      INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE,GP_MS_DOWNLOAD_TYPE) VALUES ('Cashier.Voucher','AnonymousAccount.Description','An�nimo','1');
    END
  ELSE
    BEGIN
      INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE,GP_MS_DOWNLOAD_TYPE) VALUES ('Cashier.Voucher','AnonymousAccount.Description','Anonymous','1');
    END
GO

IF NOT EXISTS (SELECT 1 FROM ALARM_CATALOG WHERE ALCG_ALARM_CODE = 393264 AND ALCG_LANGUAGE_ID = 10)
BEGIN
    INSERT   [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) 
    VALUES   ( 393264, 10, 0, N'N�mero de serie no esperado', N'N�mero de serie no esperado', 1)
END

IF NOT EXISTS (SELECT 1 FROM ALARM_CATALOG WHERE ALCG_ALARM_CODE = 393264 AND ALCG_LANGUAGE_ID = 9)
BEGIN
    INSERT   [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) 
    VALUES   ( 393264,  9, 0, N'Unexpected serial number', N'Unexpected serial number', 1)
END

IF NOT EXISTS (SELECT 1 FROM ALARM_CATALOG_PER_CATEGORY WHERE ALCC_ALARM_CODE = 393264)
BEGIN
    INSERT   [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime])
    VALUES   ( 393264, 17, 0, GETDATE() )
END
GO 

IF NOT EXISTS (SELECT * FROM ms_site_tasks WHERE st_task_id = 68)
BEGIN
  SELECT ST_SITE_ID AS SITE_ID INTO #CURRENT_SITES FROM MS_SITE_TASKS GROUP BY ST_SITE_ID
  INSERT INTO MS_SITE_TASKS (ST_TASK_ID, ST_ENABLED, ST_INTERVAL_SECONDS, ST_MAX_ROWS_TO_UPLOAD, ST_SITE_ID) 
  SELECT       68,          1,                300,                   10,    SITE_ID  FROM #CURRENT_SITES;
  
  DROP TABLE #CURRENT_SITES
END
GO

IF NOT EXISTS (SELECT * FROM ALARM_CATALOG WHERE ALCG_ALARM_CODE = 1048585)
BEGIN
  INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (1048585, 9, 0, N'Error inserting Task 68 into Interface', N'Error Inserting into CashInterface the following Task 68 - TWD_SITE_ID=XXX, TWD_OPERATION_ID=XXX - DbError: XXXX.', 1)
  INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (1048585, 10,0, N'Error insertando la Tarea 68 en la Interfaz', N'Error en la inserci�n de la Tarea 68 en CashInterface - TWD_SITE_ID=XXX, TWD_OPERATION_ID=XXX - DbError: XXXX.', 1)
END

IF NOT EXISTS (SELECT * FROM ALARM_CATALOG_PER_CATEGORY WHERE ALCC_ALARM_CODE = 1048585)
BEGIN
  INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (1048585, 39, 0, GETDATE())
END
GO

/******* STORED PROCEDURES *******/

--------------------------------------------------------------------------------
-- Copyright � 2015 Win Systems International 
--------------------------------------------------------------------------------
-- 
--   MODULE NAME: [Insert_Task68WorkData]
-- 
--   DESCRIPTION: Insert data in Task68WorkData
-- 
--        AUTHOR: Alex de Nicol�s
-- 
-- CREATION DATE: 18-MAR-2015
-- 
-- REVISION HISTORY:
-- 
-- Date        Author Description
-- ----------- ------ ----------------------------------------------------------
-- 18-MAR-2015 ANM    First release.
-------------------------------------------------------------------------------- 
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Insert_Task68WorkData]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Insert_Task68WorkData]
GO

CREATE PROCEDURE [dbo].[Insert_Task68WorkData]
		  @pSiteId				int
		, @pOperationId			bigint
		, @pDatetime			datetime
		, @pAccountId			bigint
		, @pOperType			int
		, @pTotalCashIn			money
		, @pCashInSplit1		money
		, @pCashInSplit2		money
		, @pCashInTax1			money
		, @pCashInTax2			money
		, @pTotalCashOut		money
		, @pDevolution			money
		, @pPrize				money
		, @pIsr1				money
		, @pIsr2				money
		, @pCashOutMoney		money
		, @pCashOutCheck		money
		, @pRfc					nvarchar(20)
		, @pCurp				nvarchar(20)
		, @pName				nvarchar(200)
		, @pAddress				nvarchar(200)
		, @pCity				nvarchar(50)
		, @pPostalCode			nvarchar(10)
		, @pEvidenceRfc			nvarchar(20)
		, @pEvidenceCurp		nvarchar(20)
		, @pEvidenceName		nvarchar(200)
		, @pEvidenceAddres		nvarchar(200)
		, @pEvidenceCity		nvarchar(50)
		, @pEvidencePostalCode	nvarchar(10)
		, @pEvidence			bit
		, @pDocumentId			int
		, @pRetentionRounding	money
		, @pServiceCharge		money
		, @pDecimalRounding		money
		, @pCashierSessionId	bigint
		, @pPrizeAbove			money
		, @pCashierName			nvarchar(50)
		, @pPrizeExpired		money
		, @pVoucherId			bigint
		, @pFolio				bigint
		, @pPromotionalAmount	money
		, @pPaymentTypeCode		nvarchar(10)
		, @pPaymentType			nvarchar(50)
		, @pRechargePointsPrize	money
		
AS
BEGIN   

	DECLARE @externalRef as numeric(15,0)
	DECLARE @externalCard as numeric(20,0)
		  
	IF NOT EXISTS (SELECT 1 FROM Task68_work_data WHERE twd_site_id = @pSiteId And twd_operation_id = @pOperationId)
	BEGIN

		SELECT @externalCard = ET_EXTERNAL_CARD_ID
		      , @externalRef = ET_EXTERNAL_ACCOUNT_ID
		FROM   ELP01_TRACKDATA
		WHERE  ET_AC_ACCOUNT_ID = @pAccountId
	   
		INSERT INTO   Task68_work_data
					( twd_site_id
					, twd_operation_id
					, twd_datetime
					, twd_account_id
					, twd_external_reference
					, twd_external_card_number
					, twd_operation_type
					, twd_total_cash_in
					, twd_cash_in_split1
					, twd_cash_in_split2
					, twd_cash_in_tax1
					, twd_cash_in_tax2
					, twd_total_cash_out
					, twd_devolution
					, twd_prize
					, twd_isr1
					, twd_isr2
					, twd_cash_out_money
					, twd_cash_out_check
					, twd_rfc
					, twd_curp
					, twd_name
					, twd_address
					, twd_city
					, twd_postal_code
					, twd_evidence_rfc
					, twd_evidence_curp
					, twd_evidence_name
					, twd_evidence_addres
					, twd_evidence_city
					, twd_evidence_postal_code
					, twd_evidence
					, twd_document_id
					, twd_retention_rounding
					, twd_service_charge
					, twd_decimal_rounding
					, twd_cashier_session_id
					, twd_prize_above
					, twd_cashier_name
					, twd_prize_expired
					, twd_voucher_id
					, twd_folio
					, twd_promotional_amount
					, twd_payment_type_code
					, twd_payment_type
					, twd_recharge_points_prize
					)
			 VALUES
					(
					  @pSiteId
					, @pOperationId
					, @pDatetime
					, @pAccountId
					, @externalRef
					, @externalCard
					, @pOperType
					, @pTotalCashIn
					, @pCashInSplit1
					, @pCashInSplit2
					, @pCashInTax1
					, @pCashInTax2
					, @pTotalCashOut
					, @pDevolution
					, @pPrize
					, @pIsr1
					, @pIsr2
					, @pCashOutMoney
					, @pCashOutCheck
					, @pRfc
					, @pCurp
			    , @pName
					, @pAddress
					, @pCity
					, @pPostalCode
					, @pEvidenceRfc
					, @pEvidenceCurp
					, @pEvidenceName
					, @pEvidenceAddres
					, @pEvidenceCity
					, @pEvidencePostalCode
					, @pEvidence
					, @pDocumentId
					, @pRetentionRounding
					, @pServiceCharge
					, @pDecimalRounding
					, @pCashierSessionId
					, @pPrizeAbove
					, @pCashierName
					, @pPrizeExpired
					, @pVoucherId
					, @pFolio
					, @pPromotionalAmount
					, @pPaymentTypeCode
					, @pPaymentType
					, @pRechargePointsPrize
					)
	END
END
GO