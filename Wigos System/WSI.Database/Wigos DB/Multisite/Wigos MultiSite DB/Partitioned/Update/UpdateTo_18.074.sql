/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_100]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 73;

SET @New_ReleaseId = 74;
SET @New_ScriptName = N'UpdateTo_18.074.sql';
SET @New_Description = N'New alarms catalog';
/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[gui_audit]') and name = 'ga_related_type')
	ALTER TABLE [dbo].[gui_audit] ADD [ga_related_type] INT NULL, [ga_related_id] BIGINT NULL
GO

/****** INSERT TABLE LCD_MESSAGES ******/
IF NOT EXISTS (SELECT * FROM SYS.OBJECTS WHERE TYPE = 'U' AND NAME = 'lcd_messages')
BEGIN
       CREATE TABLE [dbo].[lcd_messages](
             [msg_unique_id] [bigint] NOT NULL,
             [msg_type] [int] NOT NULL,
             [msg_site_list] [xml] NULL,
             [msg_terminal_list] [xml] NULL,
             [msg_account_list] [xml] NULL,
             [msg_enabled] [bit] NOT NULL,
             [msg_order] [int] NOT NULL,
             [msg_schedule_start] [datetime] NOT NULL,
             [msg_schedule_end] [datetime] NULL,
             [msg_schedule_weekday] [int] NOT NULL,
             [msg_schedule1_time_from] [int] NOT NULL,
             [msg_schedule1_time_to] [int] NOT NULL,
             [msg_schedule2_enabled] [bit] NOT NULL,
             [msg_schedule2_time_from] [int] NULL,
             [msg_schedule2_time_to] [int] NULL,
             [msg_message] [xml] NOT NULL,
             [msg_resource_id] [bigint] NOT NULL,
             [msg_timestamp] [timestamp] NULL,
             [msg_master_sequence_id] [bigint] NULL,
             [msg_computed_order]  AS (case when isnull([MSG_MASTER_SEQUENCE_ID],(0))=(0) then [MSG_ORDER] else (1000000000)+[MSG_ORDER] end),
       CONSTRAINT [PK_lcd_messages] PRIMARY KEY CLUSTERED 
        (
             [msg_unique_id] ASC
       )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
       ) ON [PRIMARY];
       ALTER TABLE [dbo].[lcd_messages] ADD  CONSTRAINT [DF_lcd_messages_msg_order]  DEFAULT ((0)) FOR [msg_order];
END
ELSE
SELECT '***** Table lcd_messages already exists *****';
GO

/****** INDEXES ******/

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[play_sessions]') AND name = N'IX_ps_finished_terminal_id_site_id')
DROP INDEX [IX_ps_finished_terminal_id_site_id] ON [dbo].[play_sessions] WITH ( ONLINE = OFF )
GO
CREATE NONCLUSTERED INDEX [IX_ps_finished_terminal_id_site_id] ON [dbo].[play_sessions] 
(
      [ps_finished] ASC,
      [ps_terminal_id] ASC,
      [ps_site_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)  ON [PRIMARY]
GO

/****** INSERT INDEX IX_msg_enabled_start ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[lcd_messages]') AND name = N'IX_msg_enabled_start')
DROP INDEX [IX_msg_enabled_start] ON [dbo].[lcd_messages] WITH ( ONLINE = OFF )
GO
CREATE NONCLUSTERED INDEX [IX_msg_enabled_start] ON [dbo].[lcd_messages] 
(
       [msg_enabled] ASC,
       [msg_schedule_start] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

/****** RECORDS ******/

--
--
--
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (212994, 10, 1, 'Sas Meter Rollover.', 'Sas Meter Rollover.', 1)
INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (212994, 9, 1, 'Sas Meter Rollover.', 'Sas Meter Rollover.', 1)
INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (212994, 1, 1, GETDATE());
GO

IF NOT EXISTS (SELECT * FROM [dbo].[alarm_catalog] WHERE [alcg_alarm_code] = 393257)
BEGIN
	INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (196616, 9, 0, ' Application disconnected from DB', ' Application disconnected from DB', 1);
	INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) VALUES (196616, 10, 0, 'Aplicación desconectada de BD', 'Aplicación desconectada de BD', 1);
	INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) VALUES (196616, 48, 0, GETDATE());
END
ELSE
BEGIN
	UPDATE [dbo].[alarm_catalog_per_category] SET [alcc_alarm_code] = 196616 WHERE [alcc_alarm_code] = 393257
	UPDATE [dbo].[alarm_catalog] SET [alcg_alarm_code] = 196616 WHERE [alcg_alarm_code] = 393257
	UPDATE [dbo].[alarms] SET [al_alarm_code] = 196616 WHERE [al_alarm_code] = 393257
END
GO

INSERT INTO dbo.alarm_catalog (alcg_alarm_code, alcg_language_id, alcg_type, alcg_name, alcg_description, alcg_visible) VALUES (393256 , 9, 0, 'Memory corruption in one of the copies of the DB', 'Memory corruption in one of the copies of the database (data recovered from the other copy)', 1);
INSERT INTO dbo.alarm_catalog (alcg_alarm_code, alcg_language_id, alcg_type, alcg_name, alcg_description, alcg_visible) VALUES (393256 , 10, 0, 'Corrupción de memoria en una de las bases de datos', 'Corrupción de memoria en una de las copias de la base de datos (datos recuperados desde la otra copia)', 1);
INSERT INTO dbo.alarm_catalog (alcg_alarm_code, alcg_language_id, alcg_type, alcg_name, alcg_description, alcg_visible) VALUES (393257 , 9, 0, 'Memory corruption in both copies of the DB', 'Memory corruption in both copies of the database (data loss occurred)', 1);
INSERT INTO dbo.alarm_catalog (alcg_alarm_code, alcg_language_id, alcg_type, alcg_name, alcg_description, alcg_visible) VALUES (393257 , 10, 0, 'Corrupción de memoria en ambas bases de datos', 'Corrupción de memoria en ambas copias de la base de datos (se produjo perdida de información)', 1);
GO

INSERT INTO dbo.alarm_catalog_per_category (alcc_alarm_code, alcc_category, alcc_type, alcc_datetime) VALUES (393256 , 2, 0, GETDATE());
INSERT INTO dbo.alarm_catalog_per_category (alcc_alarm_code, alcc_category, alcc_type, alcc_datetime) VALUES (393257 , 2, 0, GETDATE());
GO

/****** INSERT TASK (66 - MESSAGE LCD) IN MULTISITE ******/
SELECT   ST_SITE_ID AS SITE_ID 
  INTO   #CURRENT_SITES 
  FROM   MS_SITE_TASKS 
 GROUP   BY ST_SITE_ID
  
       IF NOT EXISTS(SELECT ST_TASK_ID FROM MS_SITE_TASKS WHERE ST_TASK_ID = 66)
       BEGIN
         INSERT   INTO MS_SITE_TASKS 
                     ( ST_TASK_ID
                    , ST_ENABLED
                    , ST_INTERVAL_SECONDS
                    , ST_MAX_ROWS_TO_UPLOAD
                    , ST_SITE_ID
                    ) 
              SELECT   66
                    , 1
                    , 3600
                    , 50
                    , SITE_ID  
             FROM   #CURRENT_SITES;
       END
DROP TABLE #CURRENT_SITES
GO

/****** INSERT SEQUENCES ******/
IF NOT EXISTS(SELECT SEQ_ID FROM SEQUENCES WHERE SEQ_ID = 42)
BEGIN
    INSERT INTO SEQUENCES (SEQ_ID, SEQ_NEXT_VALUE) VALUES (42, 1);
END
GO

/******* STORED PROCEDURES *******/

