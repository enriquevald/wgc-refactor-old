/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_100]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 78;

SET @New_ReleaseId = 79;
SET @New_ScriptName = N'UpdateTo_18.079.sql';
SET @New_Description = N'Added base name for terminals;Added Asset number alarm';
/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/

--Terminals
IF  NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TMP_TERMINALS_TO_BASE_NAME]') AND type in (N'U'))
  SELECT * INTO [dbo].[TMP_TERMINALS_TO_BASE_NAME] FROM [dbo].[terminals]
GO
  
IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[terminals]') and name = 'te_master_id')
  ALTER TABLE [dbo].[terminals] ADD	te_master_id int NULL
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[terminals]') and name = 'te_change_id')
  ALTER TABLE [dbo].[terminals] ADD	te_change_id int not NULL DEFAULT 0
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[terminals]') and name = 'te_base_name')
  ALTER TABLE [dbo].[terminals] ADD	te_base_name nvarchar(40) NULL
GO

UPDATE [dbo].[terminals] SET te_master_id = te_terminal_id                           
                           , te_base_name = LEFT(te_name,40)
GO
                             
IF EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[terminals]') and name = 'te_master_id' and is_nullable = 1)
  ALTER TABLE [dbo].[terminals] ALTER COLUMN te_master_id int NOT NULL
GO

IF EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[terminals]') and name = 'te_base_name' and is_nullable = 1)
  ALTER TABLE [dbo].[terminals] ALTER COLUMN te_base_name nvarchar(40) NOT NULL
GO

IF EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[terminals]') and name = 'te_name')
		ALTER TABLE [dbo].[terminals]	   DROP COLUMN [te_name] 		
GO	  

ALTER TABLE [dbo].[terminals]	   ADD [te_name] AS te_base_name + case when te_change_id <= 0 then '' else '-' + cast(te_change_id as nvarchar(9)) end
GO

--terminals_connected
IF EXISTS (SELECT * FROM sys.key_constraints WHERE type = 'PK' AND parent_object_id = OBJECT_ID(N'[dbo].[terminals_connected]') AND Name = 'PK_terminals_connected')
   ALTER TABLE dbo.terminals_connected  DROP CONSTRAINT PK_terminals_connected
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[terminals_connected]') and name = 'tc_master_id')
  ALTER TABLE dbo.terminals_connected  ADD  tc_master_id INT null
GO

UPDATE dbo.terminals_connected
   SET tc_master_id = tc_terminal_id
GO

IF EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[terminals_connected]') and name = 'tc_master_id')
  ALTER TABLE dbo.terminals_connected  ALTER COLUMN  tc_master_id INT NOT NULL
GO

ALTER TABLE dbo.terminals_connected 
   ADD CONSTRAINT PK_terminals_connected
   PRIMARY KEY(TC_SITE_ID, TC_DATE,TC_MASTER_ID)
go

/****** INDEXES ******/

/****** RECORDS ******/

IF NOT EXISTS (SELECT [alcg_alarm_code] from [dbo].[alarm_catalog] WHERE [alcg_alarm_code] = 393265 AND [alcg_language_id] = 10)
BEGIN
  INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) 
    VALUES ( 393265, 10, 0, N'Asset number no esperado', N'Asset number no esperado', 1)
END
GO

IF NOT EXISTS (SELECT [alcg_alarm_code] from [dbo].[alarm_catalog] WHERE [alcg_alarm_code] = 393265 AND [alcg_language_id] = 9)
BEGIN
  INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) 
    VALUES ( 393265,  9, 0, N'Unexpected asset number', N'Unexpected asset number', 1)
END 
GO

IF NOT EXISTS (SELECT [alcc_alarm_code] from [dbo].[alarm_catalog_per_category] WHERE [alcc_alarm_code] = 393265 AND [alcc_category] = 17) 
BEGIN
  INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) 
    VALUES ( 393265, 17, 0, GETDATE() )
END
GO

/******* STORED PROCEDURES *******/

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Update_Terminals]') AND type in (N'P', N'PC'))
   DROP PROCEDURE [dbo].[Update_Terminals]
GO
  
CREATE PROCEDURE Update_Terminals
       @pSiteId	INT		
      ,@pTerminalId INT                  
      ,@pType   INT				    
      ,@pServerId   INT					
      ,@pBaseName   NVARCHAR(50)			
      ,@pExternalId   NVARCHAR(40)			
      ,@pBlocked   BIT					
      ,@pActive   BIT					
      ,@pProviderId   NVARCHAR(50)			
      ,@pClientId   SMALLINT				
      ,@pBuildId   SMALLINT				
      ,@pTerminalType   SMALLINT				
      ,@pVendorId   NVARCHAR(50)			
      ,@pStatus   INT					
      ,@pRetirementDate  DATETIME				
      ,@pRetirementRequested  DATETIME				
      ,@pDenomination  MONEY					
      ,@pMultiDenomination  NVARCHAR(40)			
      ,@pProgram  NVARCHAR(40)			
      ,@pTheoreticalPayout  MONEY					
      ,@pProvId  INT					
      ,@pBankId  INT					
      ,@pFloorId  NVARCHAR(20)			
      ,@pGameType  INT					
      ,@pActivationDate  DATETIME				
      ,@pCurrentAccountId  BIGINT			    	
      ,@pCurrentPlaySessionId  BIGINT				    
      ,@pRegistrationCode  NVARCHAR(50)			
      ,@pSasFlags  INT					
      ,@pSerialNumber  NVARCHAR(50)			
      ,@pCabinetType  NVARCHAR(50)			
      ,@pJackpotContributionPct  NUMERIC
      ,@pPosition  INT
      ,@pMachineId  NVARCHAR(50)
      ,@pMasterId INT
      ,@pChangeId INT
													
AS													
BEGIN												  
  IF EXISTS(SELECT   1   
              FROM   TERMINALS
             WHERE   TE_TERMINAL_ID = @pTerminalId
               AND   TE_SITE_ID			= @pSiteId)
 
            UPDATE   TERMINALS
               SET   TE_TYPE                      = @pType 
                   , TE_SERVER_ID									=	@pServerId 
                   , TE_BASE_NAME									=	@pBaseName 
                   , TE_EXTERNAL_ID								=	@pExternalId 
                   , TE_BLOCKED										=	@pBlocked 
                   , TE_ACTIVE										=	@pActive 
                   , TE_PROVIDER_ID								=	@pProviderId 
                   , TE_CLIENT_ID									=	@pClientId 
                   , TE_BUILD_ID									=	@pBuildId 
                   , TE_TERMINAL_TYPE							=	@pTerminalType 
                   , TE_VENDOR_ID									=	@pVendorId 
                   , TE_STATUS										=	@pStatus 
                   , TE_RETIREMENT_DATE						=	@pRetirementDate
                   , TE_RETIREMENT_REQUESTED		  =	@pRetirementRequested
                   , TE_DENOMINATION							=	@pDenomination
                   , TE_MULTI_DENOMINATION			  =	@pMultiDenomination
                   , TE_PROGRAM										=	@pProgram
                   , TE_THEORETICAL_PAYOUT			  =	@pTheoreticalPayout
                   , TE_PROV_ID										=	@pProvId
                   , TE_BANK_ID										=	@pBankId
                   , TE_FLOOR_ID									=	@pFloorId
                   , TE_GAME_TYPE							 		=	@pGameType
                   , TE_ACTIVATION_DATE						=	@pActivationDate
                   , TE_CURRENT_ACCOUNT_ID			  =	@pCurrentAccountId
                   , TE_CURRENT_PLAY_SESSION_ID		=	@pCurrentPlaySessionId
                   , TE_REGISTRATION_CODE					=	@pRegistrationCode
                   , TE_SAS_FLAGS									=	@pSasFlags
                   , TE_SERIAL_NUMBER							=	@pSerialNumber
                   , TE_CABINET_TYPE							=	@pCabinetType
                   , TE_JACKPOT_CONTRIBUTION_PCT  =	@pJackpotContributionPct
                   , TE_POSITION									=	@pPosition
                   , TE_MACHINE_ID								=	@pMachineId
                   , TE_MASTER_ID									=	@pMasterId
                   , TE_CHANGE_ID									=	@pChangeId
             WHERE   TE_TERMINAL_ID =		 @pTerminalId
							 AND   TE_SITE_ID			=		 @pSiteId
  ELSE
            INSERT   INTO  TERMINALS
                   ( TE_TERMINAL_ID
                   , TE_TYPE
                   , TE_SERVER_ID
                   , TE_BASE_NAME
                   , TE_EXTERNAL_ID
                   , TE_BLOCKED
                   , TE_ACTIVE
                   , TE_PROVIDER_ID
                   , TE_CLIENT_ID
                   , TE_BUILD_ID
                   , TE_TERMINAL_TYPE
                   , TE_VENDOR_ID
                   , TE_STATUS
                   , TE_RETIREMENT_DATE
                   , TE_RETIREMENT_REQUESTED
                   , TE_DENOMINATION
                   , TE_MULTI_DENOMINATION
                   , TE_PROGRAM
                   , TE_THEORETICAL_PAYOUT
                   , TE_PROV_ID
                   , TE_BANK_ID
                   , TE_FLOOR_ID
                   , TE_GAME_TYPE
                   , TE_ACTIVATION_DATE
                   , TE_CURRENT_ACCOUNT_ID
                   , TE_CURRENT_PLAY_SESSION_ID
                   , TE_REGISTRATION_CODE
                   , TE_SAS_FLAGS
                   , TE_SERIAL_NUMBER
                   , TE_CABINET_TYPE
                   , TE_JACKPOT_CONTRIBUTION_PCT
                   , TE_SITE_ID 
                   , TE_POSITION
                   , TE_MACHINE_ID
                   , TE_MASTER_ID	
								   , TE_CHANGE_ID)
            VALUES ( @pTerminalId
                   , @pType
                   , @pServerId 
                   , @pBaseName 
                   , @pExternalId 
                   , @pBlocked 
                   , @pActive 
                   , @pProviderId 
                   , @pClientId 
                   , @pBuildId 
                   , @pTerminalType 
                   , @pVendorId 
                   , @pStatus 
                   , @pRetirementDate
                   , @pRetirementRequested
                   , @pDenomination
                   , @pMultiDenomination
                   , @pProgram
                   , @pTheoreticalPayout
                   , @pProvId
                   , @pBankId
                   , @pFloorId
                   , @pGameType
                   , @pActivationDate
                   , @pCurrentAccountId
                   , @pCurrentPlaySessionId
                   , @pRegistrationCode
                   , @pSasFlags
                   , @pSerialNumber
                   , @pCabinetType
                   , @pJackpotContributionPct
                   , @pSiteId
                   , @pPosition
                   , @pMachineId
                   , @pMasterId
								   , @pChangeId)
                                          
END --Procedure InsertTerminals
GO

-- Insert_TerminalsOldVersion
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Update_TerminalsForOldVersion]') AND type in (N'P', N'PC'))
   DROP PROCEDURE [dbo].[Update_TerminalsForOldVersion]
GO
  
CREATE PROCEDURE Update_TerminalsForOldVersion
       @pSiteId	INT		
      ,@pTerminalId INT                  
      ,@pType   INT				    
      ,@pServerId   INT					
      ,@pBaseName   NVARCHAR(50)			
      ,@pExternalId   NVARCHAR(40)			
      ,@pBlocked   BIT					
      ,@pActive   BIT					
      ,@pProviderId   NVARCHAR(50)			
      ,@pClientId   SMALLINT				
      ,@pBuildId   SMALLINT				
      ,@pTerminalType   SMALLINT				
      ,@pVendorId   NVARCHAR(50)			
      ,@pStatus   INT					
      ,@pRetirementDate  DATETIME				
      ,@pRetirementRequested  DATETIME				
      ,@pDenomination  MONEY					
      ,@pMultiDenomination  NVARCHAR(40)			
      ,@pProgram  NVARCHAR(40)			
      ,@pTheoreticalPayout  MONEY					
      ,@pProvId  INT					
      ,@pBankId  INT					
      ,@pFloorId  NVARCHAR(20)			
      ,@pGameType  INT					
      ,@pActivationDate  DATETIME				
      ,@pCurrentAccountId  BIGINT			    	
      ,@pCurrentPlaySessionId  BIGINT				    
      ,@pRegistrationCode  NVARCHAR(50)			
      ,@pSasFlags  INT					
      ,@pSerialNumber  NVARCHAR(50)			
      ,@pCabinetType  NVARCHAR(50)			
      ,@pJackpotContributionPct  NUMERIC	
      ,@pPosition  INT
      ,@pMachineId  NVARCHAR(50)
AS													
BEGIN												  
	IF EXISTS(SELECT   1   
             FROM   TERMINALS
            WHERE   TE_TERMINAL_ID = @pTerminalId
              AND   TE_SITE_ID		  = @pSiteId)
                
   UPDATE   TERMINALS
      SET   TE_TYPE                     = @pType 
          , TE_SERVER_ID								=	@pServerId 
          , TE_BASE_NAME								=	@pBaseName 
          , TE_EXTERNAL_ID							=	@pExternalId 
          , TE_BLOCKED									=	@pBlocked 
          , TE_ACTIVE									  =	@pActive 
          , TE_PROVIDER_ID							=	@pProviderId 
          , TE_CLIENT_ID								=	@pClientId 
          , TE_BUILD_ID								  =	@pBuildId 
          , TE_TERMINAL_TYPE						=	@pTerminalType 
          , TE_VENDOR_ID								=	@pVendorId 
          , TE_STATUS									  =	@pStatus 
          , TE_RETIREMENT_DATE					=	@pRetirementDate
          , TE_RETIREMENT_REQUESTED		  =	@pRetirementRequested
          , TE_DENOMINATION						  =	@pDenomination
          , TE_MULTI_DENOMINATION			  =	@pMultiDenomination
          , TE_PROGRAM									=	@pProgram
          , TE_THEORETICAL_PAYOUT			  =	@pTheoreticalPayout
          , TE_PROV_ID									=	@pProvId
          , TE_BANK_ID									=	@pBankId
          , TE_FLOOR_ID								  =	@pFloorId
          , TE_GAME_TYPE								=	@pGameType
          , TE_ACTIVATION_DATE					=	@pActivationDate
          , TE_CURRENT_ACCOUNT_ID			  =	@pCurrentAccountId
          , TE_CURRENT_PLAY_SESSION_ID  =	@pCurrentPlaySessionId
          , TE_REGISTRATION_CODE				=	@pRegistrationCode
          , TE_SAS_FLAGS								=	@pSasFlags
          , TE_SERIAL_NUMBER						=	@pSerialNumber
          , TE_CABINET_TYPE						  =	@pCabinetType
          , TE_JACKPOT_CONTRIBUTION_PCT =	@pJackpotContributionPct
					, TE_POSITION									=	@pPosition
          , TE_MACHINE_ID								= @pMachineId
    WHERE   TE_TERMINAL_ID = @pTerminalId
  		AND   TE_SITE_ID		 = @pSiteId
                            
ELSE
  INSERT INTO   TERMINALS
							( TE_TERMINAL_ID
              , TE_TYPE
              , TE_SERVER_ID
              , TE_BASE_NAME
              , TE_EXTERNAL_ID
              , TE_BLOCKED
              , TE_ACTIVE
              , TE_PROVIDER_ID
              , TE_CLIENT_ID
              , TE_BUILD_ID
              , TE_TERMINAL_TYPE
              , TE_VENDOR_ID
              , TE_STATUS
              , TE_RETIREMENT_DATE
              , TE_RETIREMENT_REQUESTED
              , TE_DENOMINATION
              , TE_MULTI_DENOMINATION
              , TE_PROGRAM
              , TE_THEORETICAL_PAYOUT
              , TE_PROV_ID
              , TE_BANK_ID
              , TE_FLOOR_ID
              , TE_GAME_TYPE
              , TE_ACTIVATION_DATE
              , TE_CURRENT_ACCOUNT_ID
              , TE_CURRENT_PLAY_SESSION_ID
              , TE_REGISTRATION_CODE
              , TE_SAS_FLAGS
              , TE_SERIAL_NUMBER
              , TE_CABINET_TYPE
              , TE_JACKPOT_CONTRIBUTION_PCT
              , TE_SITE_ID 
              , TE_POSITION
              , TE_MACHINE_ID
              , TE_MASTER_ID)
       VALUES
              ( @pTerminalId
              , @pType
              , @pServerId 
              , @pBaseName 
              , @pExternalId 
              , @pBlocked 
              , @pActive 
              , @pProviderId 
              , @pClientId 
              , @pBuildId 
              , @pTerminalType 
              , @pVendorId 
              , @pStatus 
              , @pRetirementDate
              , @pRetirementRequested
              , @pDenomination
              , @pMultiDenomination
              , @pProgram
              , @pTheoreticalPayout
              , @pProvId
              , @pBankId
              , @pFloorId
              , @pGameType
              , @pActivationDate
              , @pCurrentAccountId
              , @pCurrentPlaySessionId
              , @pRegistrationCode
              , @pSasFlags
              , @pSerialNumber
              , @pCabinetType
              , @pJackpotContributionPct
              , @pSiteId
              , @pPosition
              , @pMachineId
              , @pTerminalId)
                                                                  
                                          
END --Procedure InsertTerminalsForOldVersion
GO

-- Insert_TerminalsConnected
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Update_TerminalsConnected]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[Update_TerminalsConnected]
GO

  
 CREATE PROCEDURE [dbo].[Update_TerminalsConnected]
  @pSiteId     INT  
, @pMasterId   INT 
, @pDate       DATETIME
, @pTerminalId INT
, @pStatus     INT
, @pConnected  BIT
, @pTimeStamp  BIGINT
AS
BEGIN   
  IF EXISTS (SELECT   1 
               FROM   TERMINALS_CONNECTED
              WHERE   TC_SITE_ID     = @pSiteId 
                AND   TC_DATE        = @pDate
                AND   TC_MASTER_ID   = @pMasterId)
                
    UPDATE   TERMINALS_CONNECTED
       SET   TC_STATUS      = @pStatus
           , TC_CONNECTED   = @pConnected
           , TC_TIMESTAMP   = @pTimeStamp
     WHERE   TC_SITE_ID     = @pSiteId 
       AND   TC_DATE        = @pDate
       AND   TC_MASTER_ID   = @pMasterId

  ELSE

    INSERT INTO   TERMINALS_CONNECTED
                ( TC_SITE_ID
                , TC_DATE
                , TC_MASTER_ID
                , TC_TERMINAL_ID
                , TC_STATUS
                , TC_CONNECTED
                , TC_TIMESTAMP)
         VALUES    
                ( @pSiteId    
                , @pDate      
                , @pMasterId
                , @pTerminalId
                , @pStatus    
                , @pConnected 
                , @pTimeStamp )

END -- Insert_Terminals_connected
GO