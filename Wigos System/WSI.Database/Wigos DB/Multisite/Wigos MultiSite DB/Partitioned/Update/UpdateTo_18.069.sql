/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_100]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 68;

SET @New_ReleaseId = 69;
SET @New_ScriptName = N'UpdateTo_18.069.sql';
SET @New_Description = N'Insert new occupations';
/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/

/****** INDEXES ******/

/****** RECORDS ******/

IF NOT EXISTS(SELECT 1 FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'ExternalLoyaltyProgram.Mode01' AND GP_SUBJECT_KEY = 'DescriptionCanceledAccount') 
  INSERT INTO GENERAL_PARAMS(GP_GROUP_KEY,GP_SUBJECT_KEY,GP_KEY_VALUE,GP_MS_DOWNLOAD_TYPE) VALUES ('ExternalLoyaltyProgram.Mode01','DescriptionCanceledAccount','Cancelada por SPACE',1)

---
--- Occupations
---
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'NO APLICA','1000000',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'RADIO, CINE, TELEVISI�N Y TEATRO - DISE�ADORES GR�FICOS','1014010',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'RADIO, CINE, TELEVISI�N Y TEATRO - EDITORES, PERIODISTAS, REPORTEROS Y REDACTORES','1014030',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'RADIO, CINE, TELEVISI�N Y TEATRO - ASISTENTES Y/U OPERADORES DE PRODUCCI�N DE CINE, RADIO Y TELEVISI�N','1023010',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'RADIO, CINE, TELEVISI�N Y TEATRO - LOCUTORES, COMENTARISTAS Y CRONISTAS DE RADIO Y TELEVISI�N','1023070',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'RADIO, CINE, TELEVISI�N Y TEATRO - PRODUCTORES Y DIRECTORES DE CINE, TELEVISI�N Y TEATRO','1024020',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'INTERPRETACI�N ART�STICA - ACTORES, BAILARINES, M�SICOS, ESCRITORES','1033010',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'INTERPRETACI�N ART�STICA - ARTESANOS','1033020',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'INTERPRETACI�N ART�STICA - FOT�GRAFOS','1033040',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'INTERPRETACI�N ART�STICA - ARTISTAS PL�STICOS','1034010',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'TRADUCCI�N E INTERPRETACI�N LING��STICA - INT�RPRETES Y/O TRADUCTORES','1044010',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'PUBLICIDAD, PROPAGANDA Y RELACIONES P�BLICAS - MODELOS Y EDECANES','1052010',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'PUBLICIDAD, PROPAGANDA Y RELACIONES P�BLICAS - DIRECTORES, GERENTES Y EMPLEADOS DE PUBLICIDAD','1054010',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'AGRICULTURA Y SILVICULTURA - ADMINISTRADORES O TRABAJADORES AGRICOLAS','1110100',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'AGRICULTURA Y SILVICULTURA - ADMINISTRADORES O TRABAJADORES SILV�COLAS Y FORESTALES','1110400',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'INVESTIGACI�N - ENCUESTADORES Y CODIFICADORES','1112010',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'INVESTIGACI�N - ASISTENTES DE INVESTIGADORES','1113010',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'INVESTIGACI�N - QUIMICOS Y/O T�CNICOS EN QU�MICA','1113020',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'INVESTIGACI�N - BI�LOGOS Y CIENT�FICOS RELACIONADOS','1114010',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'INVESTIGACI�N - ECONOMISTAS Y POLIT�LOGOS (NO FUNCIONARIOS PUBLICOS)','1114020',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'INVESTIGACI�N - F�SICOS ASTR�NOMOS','1114030',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'INVESTIGACI�N - GE�LOGOS, GEOQU�MICOS, GEOF�SICOS Y GE�GRAFOS','1114040',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'INVESTIGACI�N - INVESTIGADORES Y CONSULTORES EN MERCADOTECNIA','1114050',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'INVESTIGACI�N - MATEM�TICOS, ESTAD�STICOS Y ACTUARIOS','1114060',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'INVESTIGACI�N - METEOR�LOGOS','1114070',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'INVESTIGACI�N - SOCI�LOGOS, ANTROP�LOGOS E HISTORIADORES','1114090',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'ENSE�ANZA - CAPACITADORES E INSTRUCTORES','1123020',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'ENSE�ANZA - PROFESORES O DOCENTES','1124070',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'ENSE�ANZA - DIRECTORES GENERALES DE EDUCACI�N','1125010',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'DIFUSI�N CULTURAL - PROMOTORES DE DIFUSI�N CULTURAL','1133010',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'DIFUSI�N CULTURAL - TRABAJADORES DE BIBLIOTECA, ARCHIVO, MUSEO Y GALER�A DE ARTE','1134010',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'OTRAS OCUPACIONES - ESTUDIANTE � MENOR DE EDAD SIN OCUPACI�N','1135010',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'OTRAS OCUPACIONES - DESEMPLEADO','1135020',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'OTRAS OCUPACIONES - JUBILADO O PENSIONADO','1135030',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'OTRAS OCUPACIONES - AMA DE CASA O QUEHACERES DEL HOGAR','1135050',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'OTRAS OCUPACIONES - MINISTROS DE CULTO RELIGIOSO (SACERDOTE, PASTOR, MONJA, ETC)','1135060',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'OTRAS OCUPACIONES - AGENTE ADUANAL','1135070',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'OTRAS OCUPACIONES - PROPIETARIO, ACCIONISTA O SOCIO','1135080',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'SECTOR P�BLICO - EMPLEADO DEL PODER EJECUTIVO FEDERAL','1136010',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'SECTOR P�BLICO - EMPLEADO PODER EJECUTIVO ESTATAL O DEL DISTRITO FEDERAL','1136020',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'SECTOR P�BLICO - EMPLEADO DEL PODER EJECUTIVO MUNICIPAL O DELEGACIONAL','1136030',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'SECTOR P�BLICO - EMPLEADO DEL PODER JUDICIAL FEDERAL','1136040',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'SECTOR P�BLICO - EMPLEADO DEL PODER LEGISLATIVO FEDERAL','1136050',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'SECTOR P�BLICO - EMPLEADO DEL PODER LEGISLATIVO ESTATAL O DEL DISTRITO FEDERAL','1136060',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'SECTOR P�BLICO - EMPLEADO DEL PODER JUDICIAL ESTATAL O DEL DISTRITO FEDERAL','1136070',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'ORGANISMOS INTERNACIONALES Y EXTRATERRITORIALES - EMPLEADOS DE ORGANISMOS INTERNACIONALES Y EXTRATERRITORIALES','1136080',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'SECTOR P�BLICO - EJERCITO, ARMADA Y FUERZA AEREA','1136090',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'GANADER�A - APICULTORES','1220100',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'GANADER�A - ADMINISTRADORES, CRIADORES O SUPERVISORES AVICOLAS Y GANADEROS','1220200',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'PESCA Y ACUACULTURA - PESCADORES Y TRABAJADORES EN LA CR�A Y CULTIVO DE ESPECIES MARINAS','1330200',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'MINER�A, EXTRACCI�N Y SUMINISTRO - T�CNICOS GEOL�GICOS Y DE MINERALES','2130100',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'MINER�A, EXTRACCI�N Y SUMINISTRO - INGENIEROS, OPERADORES O AYUDANTES EN LA EXTRACCI�N Y REFINACI�N MINERA','2140100',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'MINER�A, EXTRACCI�N Y SUMINISTRO - AYUDANTES EN LA PERFORACI�N DE POZOS DE PETR�LEO Y GAS NATURAL','2210100',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'MINER�A, EXTRACCI�N Y SUMINISTRO - INGENIEROS PETROLEROS','2240200',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'MINER�A, EXTRACCI�N Y SUMINISTRO - OPERADORES DE CENTRALES O SISTEMAS  DE ENERG�A EL�CTRICAS','2420100',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'MINER�A, EXTRACCI�N Y SUMINISTRO - OPERADORES DE M�QUINAS DE VAPOR','2420200',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'MINER�A, EXTRACCI�N Y SUMINISTRO - GERENTE, SUPERVISOR U OPERADORES DE  TRATAMIENTO Y POTABILIZACI�N, ABASTECIMIENTO Y RECOLECCI�N DE AGUA','2530100',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'CONSTRUCCI�N - DECORADORES DE INTERIORES','3130100',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'CONSTRUCCI�N -  INGENIEROS, T�CNICOS Y OPERADORES DE LA CONSTRUCCI�N','3130300',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'CONSTRUCCI�N - COLOCADORES DE PRODUCTOS PREFABRICADOS EN INMUEBLES','3310300',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'CONSTRUCCI�N - PINTORES','3310400',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'CONSTRUCCI�N - VIDRIEROS','3310600',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'CONSTRUCCI�N - REPARADORES DE V�AS DE COMUNICACI�N','3410200',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'CONSTRUCCI�N - PLOMEROS E INSTALADORES DE TUBER�A','3420100',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'CONSTRUCCI�N - ARQUITECTOS','3420200',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'MEC�NICA - MEC�NICOS DE EQUIPO PESADO','4130300',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'MEC�NICA - INGENIERO O MEC�NICOS INSTALADORES DE MAQUINARIA INDUSTRIAL','4130500',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'MEC�NICA - INGENIERO O T�CNICO EN MEC�NICA DE VEH�CULOS TERRESTRES, A�REOS Y ACU�TICOS','4131000',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'ELECTRICIDAD - INGENIEROS O T�CNICOS ELECTRICISTAS','4230200',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'ELECTRICIDAD - T�CNICOS EN REFRIGERACI�N, AIRE ACONDICIONADO Y CALEFACCI�N','4230500',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'ELECTR�NICA - MEC�NICOS DE INSTRUMENTOS INDUSTRIALES','4330100',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'ELECTR�NICA - INGENIERO O T�CNICOS EN  ELECTR�NICA','4330200',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'INFORM�TICA - INGENIERO O T�CNICOS PROGRAMADORES EN INFORM�TICA','4430200',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'INFORM�TICA - PROFESIONISTAS O T�CNICOS DE SISTEMAS DE INFORMACI�N Y PROCESAMIENTO DE DATOS','4440100',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'TELECOMUNICACIONES - INGENIERO, INSTALADORES Y REPARADORES DE EQUIPOS Y ACCESORIOS DE TELECOMUNICACIONES','4530200',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'TELECOMUNICACIONES - TELEGRAFISTAS Y RADIO-OPERADORES','4530500',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'PROCESOS INDUSTRIALES - INGENIERO O T�CNICOS INDUSTRIAL Y DE PRODUCCI�N','4630100',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'PROCESOS INDUSTRIALES - INGENIEROS METAL�RGICOS Y DE MATERIALES','4640300',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'MINERALES NO MET�LICOS - OPERADORES O TRABAJADORES DE VIDRIO Y CONCRETO','5110100',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'MINERALES NO MET�LICOS - OPERADORES DE M�QUINAS PROCESADORAS DE MINERALES NO MET�LICOS','5121300',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'METALES - SUPERVISORES U OPERADORES DE PROCESAMIENTO Y FUNDICI�N DE METALES','5210100',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'ALIMENTOS Y BEBIDAS - TRABAJADORES EN LA ELABORACI�N  Y PROCESAMIENTO DE ALIMENTOS, BEBIDAS Y TABACO','5310700',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'TEXTILES Y PRENDAS DE VESTIR - TRABAJADORES EN LA PRODUCCI�N DE TEXTILES, PRENDAS DE VESTIR Y CALZADO','5410100',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'TEXTILES Y PRENDAS DE VESTIR - TRABAJADORES DE REPARACI�N DE PRENDAS DE VESTIR Y CALZADO','5420700',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'MADERA, PAPEL, Y PIEL - TRABAJADORES EN LA FABRICACI�N DE MUEBLES O PRODUCTOS DE MADERA Y/O PIEL','5510100',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'PRODUCTOS QU�MICOS - TRABAJADORES EN EL PROCESAMIENTO Y FABRICACI�N DE PRODUCTOS QU�MICOS Y FARMACOQU�MICAS','5610200',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'PRODUCTOS MET�LICOS Y DE HULE Y PL�STICO - TRABAJADORES  EN LA FABRICACI�N DE PRODUCTOS MET�LICOS, DE HULE Y PL�STICOS','5710200',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'PRODUCTOS MET�LICOS Y DE HULE Y PL�STICO - ENSAMBLADORES Y ACABADORES DE PRODUCTOS DE PL�STICO','5720800',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'PRODUCTOS MET�LICOS Y DE HULE Y PL�STICO - FABRICANTES DE HERRAMIENTAS Y TROQUELES','5720900',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'PRODUCTOS MET�LICOS Y DE HULE Y PL�STICO - HERREROS Y FORJADORES','5721000',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'PRODUCTOS MET�LICOS Y DE HULE Y PL�STICO - JOYEROS Y ORFEBRES','5721100',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'PRODUCTOS MET�LICOS Y DE HULE Y PL�STICO - SOLDADORES Y OXICORTADORES','5722900',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'PRODUCTOS MET�LICOS Y DE HULE Y PL�STICO - TRABAJADORES EN EL ENSAMBLADO DE VEH�CULOS, MOLDEADO, LAMINADO Y MONTAJE DE PIEZAS MET�LICAS, HULE O PLASTICO','5730600',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'PRODUCTOS MET�LICOS Y DE HULE Y PL�STICO - SUPERVISORES EN LA FABRICACI�N Y MONTAJE DE ART�CULOS DEPORTIVOS, DE JUGUETES Y SIMILARES','5731100',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'PRODUCTOS EL�CTRICOS Y ELECTR�NICOS - TRABAJADORES EN LA FABRICACI�N DE PRODUCTOS EL�CTRICOS Y ELECTR�NICOS','5810200',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'PRODUCTOS IMPRESOS - TRABAJADORES EN LA ELABORACI�N DE PRODUCTOS IMPRESOS','5910400',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'TRANSPORTE FERROVIARIO - CONDUCTORES Y OPERADORES DE TREN SUBTERR�NEO Y DE TREN LIGERO','6120100',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'TRANSPORTE FERROVIARIO - TRABAJADORES DE FERROCARRILES','6120200',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'TRANSPORTE TERRESTRE - CONDUCTORES DE VEH�CULOS DE TRANSPORTE, SERVICIOS DE CARGA Y/O REPARTO','6220100',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'TRANSPORTE A�REO - COORDINADORES Y SUPERVISORES EN SERVICIOS DE TRANSPORTE A�REO','6330100',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'TRANSPORTE A�REO - DESPACHADORES DE VUELO Y ESPECIALISTAS EN SERVICIOS A�REOS','6330200',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'TRANSPORTE A�REO - SUPERVISORES DE SISTEMAS DE COMUNICACI�N PARA LA AERONAVEGACI�N','6330400',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'TRANSPORTE A�REO - PILOTOS DE AVIACI�N E INSTRUCTORES DE VUELO','6330500',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'TRANSPORTE A�REO - SOBRECARGOS','6330600',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'TRANSPORTE MAR�TIMO Y FLUVIAL - CONDUCTORES DE EMBARCACIONES','6420100',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'TRANSPORTE MAR�TIMO Y FLUVIAL - JEFES Y CONTROLADORES DE TR�FICO MAR�TIMO','6430100',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'TRANSPORTE MAR�TIMO Y FLUVIAL - PILOTOS, CAPITANES DE PUERTOS Y OFICIALES DE CUBIERTA','6430300',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'COMERCIO - DESPACHADORES DE GASOLINERA','7110200',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'COMERCIO - EMPACADORES DE MERCANC�AS','7110300',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'COMERCIO - TAQUILLEROS','7110500',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'COMERCIO - VENDEDORES AMBULANTES','7110600',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'COMERCIO - CAJEROS REGISTRADORES','7120100',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'COMERCIO - REPRESENTANTES DE VENTAS POR TEL�FONO O POR TELEVISI�N','7130200',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'COMERCIO - VENDEDORES ESPECIALIZADOS','7130400',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'COMERCIO - GERENTES O SUPERVISOR DE ESTABLECIMIENTO COMERCIAL','7140100',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'COMERCIO - GERENTES O EMPLEADOS DE VENTAS','7140200',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'ALIMENTACI�N Y HOSPEDAJE - TRABAJADORES DE SERVICIO DE ALIMENTOS Y BEBIDAS','7210100',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'ALIMENTACI�N Y HOSPEDAJE - JEFES DE COCINA, RESTAURANTE Y/O BAR','7230200',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'ALIMENTACI�N Y HOSPEDAJE - TRABAJADORES DE SERVICIOS DE ALOJAMIENTO','7240200',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'TURISMO - COORDINADORES DE OPERACIONES EN AGENCIAS DE VIAJES','7330200',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'TURISMO - GU�AS DE EXCURSIONES O ECOTUR�STICO','7330300',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'DEPORTE Y ESPARCIMIENTO - ANIMADORES RECREATIVOS','7430100',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'DEPORTE Y ESPARCIMIENTO - ATLETAS, ENTRENADORES O INSTRUCTORES EN DEPORTE Y RECREACI�N','7430200',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'DEPORTE Y ESPARCIMIENTO - OFICIALES, JUECES Y �RBITROS DEPORTIVOS','7430400',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'SERVICIOS PERSONALES - ESTILISTAS, ESTETICISTAS Y MASAJISTAS','7530100',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'SERVICIOS PERSONALES - TRBAJADORES DE SERVICIOS FUNERARIOS O CEMENTERIOS','7540100',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'REPARACI�N DE ART�CULOS DE USO DOM�STICO Y PERSONAL - CERRAJEROS','7620100',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'REPARACI�N DE ART�CULOS DE USO DOM�STICO Y PERSONAL - REPARADORES DE ART�CULOS DE HULE','7620200',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'REPARACI�N DE ART�CULOS DE USO DOM�STICO Y PERSONAL - RELOJEROS Y REPARADORES DE RELOJES','7630100',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'REPARACI�N DE ART�CULOS DE USO DOM�STICO Y PERSONAL - REPARADORES DE APARATOS EL�CTRICOS','7630200',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'LIMPIEZA - SERVICIOS DE CAMARISTAS Y ASEADORES','7720100',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'LIMPIEZA - TRABAJADORES DE TINTORER�A Y LAVANDER�A','7720200',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'LIMPIEZA - FUMIGADORES DE PLAGAS','7720300',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'SERVICIO POSTAL Y MENSAJER�A - EMPLEADOS DE SERVICIOS DE MENSAJER�A','7810200',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'BOLSA, BANCA Y SEGUROS - GERENTES O TRABAJADORES DE SERVICIOS Y PRODUCTOS FINANCIEROS','8120100',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'BOLSA, BANCA Y SEGUROS - VALUADORES','8130500',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'BOLSA, BANCA Y SEGUROS - AGENTES DE VALORES, PROMOTORES Y CORREDORES DE INVERSI�N','8140100',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'ADMINISTRACI�N - TRABAJADORES DE ARCHIVO, ALMACEN DE INVENTARIOS','8210100',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'ADMINISTRACI�N - CAPTURISTA Y OPERADORES DE TEL�FONO','8220400',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'ADMINISTRACI�N - PAGADORES Y COBRADORES','8220600',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'ADMINISTRACI�N - DIRECTORES, GERENTES  Y EMPLEADOS DE COMPRAS, FINANZAS, RECURSOS HUMANOS Y SERVICIOS ADMINISTRATIVOS','8230300',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'ADMINISTRACI�N - ASISTENTES ADMINISTRATIVOS','8240100',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'ADMINISTRACI�N - CONTADORES Y AUDITORES','8240200',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'ADMINISTRACI�N - DIRECTORES, GERENTES Y EMPLEADOS DE PRODUCCI�N','8240600',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'ADMINISTRACI�N - DIRECTORES, GERENTES Y EMPLEADOS DE SERVICIOS DE TRANSPORTE','8240700',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'ADMINISTRACI�N - CONSULTORES','8250100',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'ADMINISTRACI�N - DIRECTORES, GERENTES Y EMPLEADOS DE COMERCIALIZACI�N','8250200',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'ADMINISTRACI�N - DIRECTORES, GERENTES Y EMPELADOS ADMINISTRATIVOS','8250800',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'SERVICIOS LEGALES - ABOGADOS Y ASESORES LEGALES','8340100',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'SERVICIOS LEGALES - NOTARIOS Y CORREDORES P�BLICOS','8340300',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'SERVICIOS M�DICOS - ENFERMERAS Y/O PARAMEDICOS','9120700',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'SERVICIOS M�DICOS - DIETISTAS Y NUTRI�LOGOS','9130300',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'SERVICIOS M�DICOS - T�CNICOS DE LABORATORIO M�DICO','9131200',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'SERVICIOS M�DICOS - DIRECTORES DE INSTITUCIONES EN EL CUIDADO DE LA SALUD','9140200',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'SERVICIOS M�DICOS - FARMAC�UTICOS','9140400',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'SERVICIOS M�DICOS - FISIOTERAP�UTAS Y QUIROPR�CTICOS','9140500',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'SERVICIOS M�DICOS - M�DICOS ESPECIALISTAS','9140700',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'SERVICIOS M�DICOS - M�DICOS GENERALES Y FAMILIARES','9140800',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'INSPECCI�N - INSPECTORES DE SALUD AMBIENTAL, SANIDAD Y DEL TRABAJO','9230100',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'INSPECCI�N - INSPECTORES DE TRANSPORTE DE CARGA Y DE PASAJEROS','9230200',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'INSPECCI�N - INSPECTORES FISCALES Y DE PRECIOS','9230300',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'INSPECCI�N - INSPECTORES SANITARIOS Y DE CONTROL DE CALIDAD DE PRODUCTOS C�RNICOS, PESQUEROS Y AGR�COLAS','9230400',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'SEGURIDAD SOCIAL - CONSEJEROS DE EMPLEO','9330100',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'SEGURIDAD SOCIAL - TRABAJADORES DE SERVICIO SOCIAL Y DE LA COMUNIDAD','9330300',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'PROTECCI�N DE BIENES Y/O PERSONAS - BOMBEROS','9420100',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'PROTECCI�N DE BIENES Y/O PERSONAS - GUARDIAS DE SEGURIDAD','9420300',1,1000,'MX')
INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ( 'PROTECCI�N DE BIENES Y/O PERSONAS - DETECTIVES PRIVADOS','9430100',1,1000,'MX')
GO

/******* STORED PROCEDURES *******/
