/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_100]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 28;

SET @New_ReleaseId = 29;
SET @New_ScriptName = N'UpdateTo_18.029.sql';
SET @New_Description = N'Create General Param ExternalLoyaltyProgram.Mode01.ExtendedLog';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/***/

  --------------------------------------------------------------------------------
  -- Copyright � 2013 Win Systems International
  --------------------------------------------------------------------------------
  --
  --   MODULE NAME: Update_LastActivity.sql
  --
  --   DESCRIPTION: Update account activity 
  --
  --        AUTHOR: Joan Marc Pepi�
  --
  -- CREATION DATE: 13-AUG-2013
  --
  -- REVISION HISTORY:
  --
  -- Date        Author Description
  -- ----------- ------ ----------------------------------------------------------
  -- 13-AUG-2013 JPJ    First release.  
  -------------------------------------------------------------------------------- 
  
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Update_LastActivity]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Update_LastActivity]
GO

CREATE   PROCEDURE [dbo].[Update_LastActivity]
      @pSite_Id                                                             INT
      ,@pAccount_Id                                                          BIGINT
      ,@pLast_Activity                                           DATETIME
      ,@pRE_Balance                                                          MONEY
      ,@pPromo_RE_Balance                                        MONEY
      ,@pPromo_NR_Balance                                        MONEY
      ,@pIn_Session_RE_Balance                       MONEY
      ,@pIn_Session_Promo_RE_Balance     MONEY
      ,@pIn_Session_Promo_NR_Balance     MONEY
AS
BEGIN   

  IF EXISTS (SELECT   1 
               FROM   LAST_ACCOUNT_ACTIVITY
              WHERE   LAA_SITE_ID                       = @pSite_Id
                AND   LAA_ACCOUNT_ID              = @pAccount_Id)
  BEGIN

     UPDATE   LAST_ACCOUNT_ACTIVITY
        SET   LAA_LAST_ACTIVITY                                                   = @pLast_Activity                    
                                   , LAA_RE_BALANCE                                                       = @pRE_Balance                           
                                   , LAA_PROMO_RE_BALANCE                                     = @pPromo_RE_Balance                     
                                   , LAA_PROMO_NR_BALANCE                                     = @pPromo_NR_Balance               
                                   , LAA_IN_SESSION_RE_BALANCE                          = @pIn_Session_RE_Balance      
                                    , LAA_IN_SESSION_PROMO_RE_BALANCE        = @pIn_Session_Promo_RE_Balance 
                                   , LAA_IN_SESSION_PROMO_NR_BALANCE        = @pIn_Session_Promo_NR_Balance 
                  WHERE   LAA_SITE_ID          = @pSite_Id
                AND   LAA_ACCOUNT_ID  = @pAccount_Id
  END      
  ELSE
  BEGIN
    INSERT INTO   LAST_ACCOUNT_ACTIVITY
                ( LAA_SITE_ID
                , LAA_ACCOUNT_ID
                , LAA_LAST_ACTIVITY
                                           , LAA_RE_BALANCE
                                           , LAA_PROMO_RE_BALANCE
                                           , LAA_PROMO_NR_BALANCE
                                               , LAA_IN_SESSION_RE_BALANCE
                                               , LAA_IN_SESSION_PROMO_RE_BALANCE
                                               , LAA_IN_SESSION_PROMO_NR_BALANCE)
         VALUES
                ( @pSite_Id
                , @pAccount_Id
                , @pLast_Activity                    
                                               , @pRE_Balance                           
                                               , @pPromo_RE_Balance                     
                                               , @pPromo_NR_Balance               
                                               , @pIn_Session_RE_Balance          
                                               , @pIn_Session_Promo_RE_Balance    
                                               , @pIn_Session_Promo_NR_Balance    )
  END

  UPDATE   ACCOUNTS
     SET   AC_LAST_ACTIVITY = CASE WHEN @pLast_Activity > AC_LAST_ACTIVITY THEN @pLast_Activity ELSE AC_LAST_ACTIVITY END
   WHERE   AC_ACCOUNT_ID = @pAccount_Id
   
END -- Update_LastActivity
GO


/****** RECORDS ******/
INSERT INTO [general_params]
           ([gp_group_key]
           ,[gp_subject_key]
           ,[gp_key_value]
           ,[gp_timestamp]
           ,[gp_ms_download_type])
     VALUES
           ('ExternalLoyaltyProgram.Mode01'
           ,'ExtendedLog'
           ,'255'
           ,NULL
           ,0)
GO



