/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_100]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 22;

SET @New_ReleaseId = 23;
SET @New_ScriptName = N'UpdateTo_18.023.sql';
SET @New_Description = N'Default sequence for account document task';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** RECORDS ******/

UPDATE   SEQUENCES 
   SET   SEQ_NEXT_VALUE = 1
WHERE    SEQ_ID = 15
   AND   SEQ_NEXT_VALUE = 0
GO

UPDATE   GENERAL_PARAMS
   SET   GP_KEY_VALUE = 'Por la ley antilavado de dinero, el cliente debe identificarse para realizar esta recarga.'
 WHERE   GP_GROUP_KEY = 'AntiMoneyLaundering'   AND GP_SUBJECT_KEY = 'Recharge.Identification.Message'
UPDATE   GENERAL_PARAMS
   SET   GP_KEY_VALUE = 'Por la ley antilavado de dinero, al realizar esta recarga, el cliente va a ser inclu�do en un reporte para el SAT.'
 WHERE   GP_GROUP_KEY = 'AntiMoneyLaundering'   AND GP_SUBJECT_KEY = 'Recharge.Report.Message'
UPDATE   GENERAL_PARAMS
   SET   GP_KEY_VALUE = 'Por la ley antilavado de dinero, el cliente debe identificarse para realizar este reintegro.'
 WHERE   GP_GROUP_KEY = 'AntiMoneyLaundering'   AND GP_SUBJECT_KEY = 'Prize.Identification.Message'
UPDATE   GENERAL_PARAMS
   SET   GP_KEY_VALUE = 'Por la ley antilavado de dinero, al realizar este reintegro, el cliente va a ser inclu�do en un reporte para el SAT.'
 WHERE   GP_GROUP_KEY = 'AntiMoneyLaundering'   AND GP_SUBJECT_KEY = 'Prize.Report.Message'
UPDATE   GENERAL_PARAMS
   SET   GP_KEY_VALUE = 'Por la ley antilavado de dinero, el cliente deber� identificarse en pr�ximos reintegros.'
 WHERE   GP_GROUP_KEY = 'AntiMoneyLaundering'   AND GP_SUBJECT_KEY = 'Prize.Identification.Warning.Message'
UPDATE   GENERAL_PARAMS
   SET   GP_KEY_VALUE = 'Por la ley antilavado de dinero, el cliente ser� reportado al SAT en pr�ximos reintegros.'
 WHERE   GP_GROUP_KEY = 'AntiMoneyLaundering'   AND GP_SUBJECT_KEY = 'Prize.Report.Warning.Message'

UPDATE   GENERAL_PARAMS
   SET   GP_KEY_VALUE = '001,002'
 WHERE   GP_GROUP_KEY = 'Account.RequestedField'   AND GP_SUBJECT_KEY = 'AntiMoneyLaundering.DocumentTypeList'
UPDATE   GENERAL_PARAMS
   SET   GP_KEY_VALUE = '001,002'
 WHERE   GP_GROUP_KEY = 'Beneficiary.RequestedField'   AND GP_SUBJECT_KEY = 'AntiMoneyLaundering.DocumentTypeList'

UPDATE   GENERAL_PARAMS
   SET   GP_KEY_VALUE = '1'
 WHERE   GP_GROUP_KEY = 'Account.RequestedField'   AND GP_SUBJECT_KEY = 'AntiMoneyLaundering.DocScan'
UPDATE   GENERAL_PARAMS
   SET   GP_KEY_VALUE = '1'
 WHERE   GP_GROUP_KEY = 'Beneficiary.RequestedField'   AND GP_SUBJECT_KEY = 'AntiMoneyLaundering.DocScan'

UPDATE   GENERAL_PARAMS
   SET   GP_KEY_VALUE = '003'
 WHERE   GP_GROUP_KEY = 'Account.RequestedField'   AND GP_SUBJECT_KEY = 'AntiMoneyLaundering.DocScanTypeList'
UPDATE   GENERAL_PARAMS
   SET   GP_KEY_VALUE = '003'
 WHERE   GP_GROUP_KEY = 'Beneficiary.RequestedField'   AND GP_SUBJECT_KEY = 'AntiMoneyLaundering.DocScanTypeList'

GO
