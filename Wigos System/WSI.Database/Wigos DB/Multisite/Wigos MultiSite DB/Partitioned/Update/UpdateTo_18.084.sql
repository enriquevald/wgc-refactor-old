/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_100]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 83;

SET @New_ReleaseId = 84;
SET @New_ScriptName = N'UpdateTo_18.084.sql';
SET @New_Description = N'New Alarms;Regionalization';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/

/****** INDEXES ******/

/****** RECORDS ******/

IF NOT EXISTS (SELECT [alcg_alarm_code] from [dbo].[alarm_catalog] WHERE [alcg_alarm_code] = 393261 AND [alcg_language_id] = 10)
BEGIN
  INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) 
    VALUES ( 393261, 10, 0, N'Ticket ID no coincide', N'Ticket ID no coincide', 1)
END
GO

IF NOT EXISTS (SELECT [alcg_alarm_code] from [dbo].[alarm_catalog] WHERE [alcg_alarm_code] = 393261 AND [alcg_language_id] = 9)
BEGIN
  INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) 
    VALUES ( 393261,  9, 0, N'Ticket ID mismatch', N'Ticket ID mismatch', 1)
END 
GO

IF NOT EXISTS (SELECT [alcg_alarm_code] from [dbo].[alarm_catalog] WHERE [alcg_alarm_code] = 393262 AND [alcg_language_id] = 10)
BEGIN
  INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) 
    VALUES ( 393262, 10, 0, N'No se puedo leer Ticket Info', N'No se puedo leer Ticket Info', 1)
END
GO

IF NOT EXISTS (SELECT [alcg_alarm_code] from [dbo].[alarm_catalog] WHERE [alcg_alarm_code] = 393262 AND [alcg_language_id] = 9)
BEGIN
  INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) 
    VALUES ( 393262,  9, 0, N'Can''t read Ticket Info', N'Can''t read Ticket Info', 1)
END 
GO

IF NOT EXISTS (SELECT [alcc_alarm_code] from [dbo].[alarm_catalog_per_category] WHERE [alcc_alarm_code] = 393261 AND [alcc_category] = 50) 
BEGIN
  INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) 
    VALUES ( 393261, 24, 0, GETDATE() )
END
GO


IF NOT EXISTS (SELECT [alcc_alarm_code] from [dbo].[alarm_catalog_per_category] WHERE [alcc_alarm_code] = 393262 AND [alcc_category] = 50) 
BEGIN
  INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) 
    VALUES ( 393262, 24, 0, GETDATE() )
END
GO

IF NOT EXISTS (SELECT [alcg_alarm_code] from [dbo].[alarm_catalog] WHERE [alcg_alarm_code] = 393263 AND [alcg_language_id] = 10)
BEGIN
  INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) 
    VALUES ( 393263, 10, 0, N'AFT No autorizada', N'AFT No autorizada', 1)
END
GO

IF NOT EXISTS (SELECT [alcg_alarm_code] from [dbo].[alarm_catalog] WHERE [alcg_alarm_code] = 393263 AND [alcg_language_id] = 9)
BEGIN
  INSERT [dbo].[alarm_catalog] ([alcg_alarm_code], [alcg_language_id], [alcg_type], [alcg_name], [alcg_description], [alcg_visible]) 
    VALUES ( 393263,  9, 0, N'AFT Not authorized', N'AFT Not authorized', 1)
END 
GO

IF NOT EXISTS (SELECT [alcc_alarm_code] from [dbo].[alarm_catalog_per_category] WHERE [alcc_alarm_code] = 393263 AND [alcc_category] = 24) 
BEGIN
  INSERT [dbo].[alarm_catalog_per_category] ([alcc_alarm_code], [alcc_category], [alcc_type], [alcc_datetime]) 
    VALUES ( 393263, 24, 0, GETDATE() )
END
GO

-- GENERAL PARAM
IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='PlayerTracking.ExternalLoyaltyProgram' AND GP_SUBJECT_KEY = 'WebService.LogEnabled')
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('PlayerTracking.ExternalLoyaltyProgram', 'WebService.LogEnabled', '1')

IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='PlayerTracking.ExternalLoyaltyProgram' AND GP_SUBJECT_KEY = 'WebService.Uri')
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('PlayerTracking.ExternalLoyaltyProgram', 'WebService.Uri', 'http://winsystemsintl.com:7773/WigosService')     
   
IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='PlayerTracking.ExternalLoyaltyProgram' AND GP_SUBJECT_KEY = 'WebService.UserId')
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('PlayerTracking.ExternalLoyaltyProgram', 'WebService.UserId', 'resu@user')
   
IF NOT EXISTS ( SELECT GP_GROUP_KEY, GP_SUBJECT_KEY FROM GENERAL_PARAMS WHERE GP_GROUP_KEY ='PlayerTracking.ExternalLoyaltyProgram' AND GP_SUBJECT_KEY = 'WebService.Password')
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('PlayerTracking.ExternalLoyaltyProgram', 'WebService.Password', '2016@reSu')   
GO

/*  
    Mode: 0 is configured/procesed in the MS
          1 is configured/procesed in the Site
          2 is mode by groups (future: configured in MS, procesed in site) 
*/

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Intrusion' AND GP_SUBJECT_KEY ='DisableAFT')
BEGIN
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE, GP_MS_DOWNLOAD_TYPE) VALUES ('Intrusion', 'DisableAFT', 0, 1); 
END

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Intrusion' AND GP_SUBJECT_KEY ='BlockMachine')
BEGIN
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE, GP_MS_DOWNLOAD_TYPE) VALUES ('Intrusion', 'BlockMachine', 0, 1); 
END

IF NOT EXISTS (SELECT * FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Intrusion' AND GP_SUBJECT_KEY ='BlockAccount')
BEGIN
   INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE, GP_MS_DOWNLOAD_TYPE) VALUES ('Intrusion', 'BlockAccount', 0, 1); 
END
GO

/****** STORED *******/


--
-- CHILE
--

-- CHILE REGIONS
IF (NOT EXISTS (SELECT * FROM federal_states WHERE fs_country_iso_code2 = 'CL'))
BEGIN
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Arica y Parinacota','CL')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Tarapac�','CL')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Antofagasta','CL')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Atacama','CL')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Coquimbo','CL')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Valpara�so','CL')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Metropolitana de Santiago','CL')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('O''Higgins','CL')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Maule','CL')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Biob�o','CL')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('La Araucan�a','CL')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Los R�os','CL')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Los Lagos','CL')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Ays�n','CL')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Magallanes','CL')
END
GO

-- CHILE OCCUPATIONS
IF (NOT EXISTS (SELECT * FROM occupations WHERE oc_country_iso_code2 = 'CL' ))
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('NO DISPONIBLE','0000001',1,1000,'CL')
GO
  

-- CHILE IDENTIFICATION_TYPES
IF (NOT EXISTS (SELECT * FROM identification_types WHERE idt_country_iso_code2 = 'CL' ))
BEGIN
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (50,1,100,'Otro','CL')
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (51,1,1,'C�dula de identidad','CL')
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (52,1,2,'Pasaporte','CL')
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (53,1,3,'Licencia de conducir','CL')
END
GO

--
-- PERU
--

-- PERU REGIONS
IF (NOT EXISTS (SELECT * FROM federal_states WHERE fs_country_iso_code2 = 'PE' ))
BEGIN
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Amazonas','PE')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('�ncash','PE')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Apur�mac','PE')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Arequipa','PE')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Ayacucho','PE')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Cajamarca','PE')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Callao','PE')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Cuzco','PE')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Huancavelica','PE')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Hu�nuco','PE')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Ica','PE')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Jun�n','PE')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('La Libertad','PE')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Lambayeque','PE')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Lima','PE')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Loreto','PE')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Madre de Dios','PE')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Moquegua','PE')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Pasco','PE')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Piura','PE')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Puno','PE')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('San Mart�n','PE')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Tacna','PE')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Tumbes','PE')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Ucayali','PE')
END
GO

-- PERU OCCUPATIONS
IF (NOT EXISTS (SELECT * FROM occupations WHERE oc_country_iso_code2 = 'PE' ))
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('NO DISPONIBLE','0000001',1,1000,'PE')
GO

-- PERU IDENTIFICATION_TYPES
IF (NOT EXISTS (SELECT * FROM identification_types WHERE idt_country_iso_code2 = 'PE' ))
BEGIN
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (40,1,100,'Otro','PE')
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (41,1,1,'DNI','PE')
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (42,1,2,'Pasaporte','PE')
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (43,1,3,'Licencia de conducir','PE')
END
GO

--
-- COLOMBIA
--

-- COLOMBIA REGIONS
IF (NOT EXISTS (SELECT * FROM federal_states WHERE fs_country_iso_code2 = 'CO' ))
BEGIN
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Amazonas','CO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Antioquia','CO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Arauca','CO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Atl�ntico','CO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Bol�var','CO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Boyac�','CO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Caldas','CO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Caquet�','CO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Casanare','CO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Cauca','CO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Cesar','CO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Choc�','CO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('C�rdoba','CO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Cundinamarca','CO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Guain�a','CO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Guaviare','CO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Huila','CO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('La Guajira','CO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Magdalena','CO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Meta','CO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Nari�o','CO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Norte de Santander','CO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Putumayo','CO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Quind�o','CO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Risaralda','CO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('San Andr�s y Providencia','CO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Santander','CO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Sucre','CO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Tolima','CO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Valle del Cauca','CO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Vaup�s','CO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Vichada','CO')
END
GO

-- COLOMBIA OCCUPATIONS
IF (NOT EXISTS (SELECT * FROM occupations WHERE oc_country_iso_code2 = 'CO' ))
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('NO DISPONIBLE','0000001',1,1000,'CO')
GO

-- COLOMBIA IDENTIFICATION_TYPES
IF (NOT EXISTS (SELECT * FROM identification_types WHERE idt_country_iso_code2 = 'CO' ))
BEGIN
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (54,1,100,'Otro','CO')
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (55,1,1,'C�dula de Ciudadan�a','CO')
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (56,1,2,'Pasaporte','CO')
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (57,1,3,'Licencia de conducir','CO')
END
GO

--
-- DOMINICAN REPUBLIC
--

-- DOMINICAN REPUBLIC REGIONS
IF (NOT EXISTS (SELECT * FROM federal_states WHERE fs_country_iso_code2 = 'DO' ))
BEGIN
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Azua','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Bahoruco','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Barahona','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Dajab�n','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Distrito Nacional','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Duarte','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('El Seibo','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('El�as Pi�a','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Espaillat','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Hato Mayor','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Hermanas Mirabal','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Independencia','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('La Altagracia','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('La Romana','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('La Vega','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Mar�a Trinidad S�nchez','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Monse�or Nouel','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Monte Cristi','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Monte Plata','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Pedernales','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Peravia','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Puerto Plata','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Saman�','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('San Crist�bal','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('San Jos� de Ocoa','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('San Juan','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('San Pedro de Macor�s','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('S�nchez Ram�rez','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Santiago','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Santiago Rodr�guez','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Santo Domingo','DO')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Valverde','DO')
END
GO

-- DOMINICAN REPUBLIC OCCUPATIONS
IF (NOT EXISTS (SELECT * FROM occupations WHERE oc_country_iso_code2 = 'DO' ))
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('NO DISPONIBLE','0000001',1,1000,'DO')
GO

-- DOMINICAN REPUBLIC IDENTIFICATION_TYPES
IF (NOT EXISTS (SELECT * FROM identification_types WHERE idt_country_iso_code2 = 'DO' ))
BEGIN
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (70,1,100,'Otro','DO')
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (71,1,1,'C�dula de Identidad','DO')
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (72,1,2,'Pasaporte','DO')
END
GO

--
-- TRINIDAD AND TOBAGO
--

--TRINIDAD AND TOBAGO REGIONS
IF (NOT EXISTS (SELECT * FROM federal_states WHERE fs_country_iso_code2 = 'TT' ))
BEGIN
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Port of Spain','TT')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('San Fernando','TT')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Chaguanas Borough','TT')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Arima Borough','TT')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Point Fortin','TT')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Couva-Tabaquite-Talparo','TT')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Diego Martin','TT')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Penal-Debe','TT')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Princes Town','TT')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Rio Claro-Mayaro','TT')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('San Juan-Laventille','TT')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Sangre Grande','TT')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Siparia','TT')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Tunapuna-Piarco','TT')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Tobago','TT')
END
GO

-- TRINIDAD AND TOBAGO OCCUPATIONS
IF (NOT EXISTS (SELECT * FROM occupations WHERE oc_country_iso_code2 = 'TT' ))
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('NOT AVAILABLE','0000001',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('OTHER','0000',1,1,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('ACCOUNTANT','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('ACCOUNTANT ASSISTANT','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('ACCOUNTS CLERK','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('ADMIN ASSISTANT','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('ADMIN CLERK','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('ADMIN COMMUNICATIONS','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('ADMINISTRATOR','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('AIR GUARD OFFICER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('AIRCRAFT TECHNICIAN','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('AIRPORT AUTHORITY STORE CLERK','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('ASSISTANT SUPERVISOR','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('ASSISTANT TRANSPORT COMMISSIONER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('AUDITOR','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('AUTO ELECTRICIAN','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('AUTO MECHANIC','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('BANK TELLER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('BAR MANAGER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('BAR OWNER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('BARBER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('BILINGUAL AGENT','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('BLENDING TECHNICIAN','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('BRANCH MANAGER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('BUSINESS OWNER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('CARGO AGENT','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('CARGO HANDLER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('CARGO TECHNICIAN','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('CARIBBEAN AIRLINES MAINTENANCE PERSONNEL','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('CARPENTER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('CARWASH OWNER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('CASH PROCESSING OFFICER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('CASHIER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('CATERER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('CATERSERVE UNILEVER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('CCTV OPERATOR','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('CEO','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('CEPEP LABORER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('CHECKER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('CHIEF','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('CIVIL ENGINEER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('CLEANER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('CLERICAL ASSISTANT','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('CLERK','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('CLERK II','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('CME FOREMAN','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('COAST GUARD','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('COMMUNITY ACTION OFFICER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('COMPUTER OPERATOR','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('COMPUTER SERVICE REPRESENTATIVE','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('CONSTRUCTION COMPANY OWNER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('CONSTRUCTION WORKER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('CONSULTANT','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('CONTRACTED WELDER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('CONTRACTOR','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('COOK','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('COORDINATOR','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('CORPORATE ENGAGEMENT OFFICER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('CORRECTIONAL OFFICER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('COURT MARSHALL','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('CRAFTSMAN','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('CREDIT CONTROL ASSISTANT','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('CSR','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('CSR SUPERVISOR','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('CUSTODIAN','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('CUSTOMS OFFICER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('DATA ENTRY CLARK','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('DAY CARE ASSISTANT','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('DEAN','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('DENTAL HYGIENIST','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('DEVELOPMENT COACH','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('DIALYSIS TECHNICIAN','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('DIRECTOR','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('DISPATCHING AND RECEIVING CLERK','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('DOCTOR','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('DOMESTIC WORKER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('DOMESTIC WORKER PRIVATE HOMES','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('DOUBLES VENDOR','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('DRIVER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('DRIVER / SALESMAN','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('ELECTRICIAN','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('ENA NURSE ASSISTANT','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('ENGINEER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('ENROLLED NURSE','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('ENTERTAINMENT','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('ESTATE CONSTABLE','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('ESTHETICIAN','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('EVENT MANAGER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('EXECUTIVE ASSISTANT','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('FACILITY SUPERVISOR','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('FARMER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('FIRE FIGHTER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('FISHERMAN','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('FOREMAN','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('FORKLIFT DRIVER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('FORM MISTRESS','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('FREELANCE ADVERTISER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('FREELANCE CONSTRUCTION WORKER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('FREELANCE CONSULTANT','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('FREELANCE CONTRACTOR','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('FREELANCE MASON','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('FREELANCE REAL ESTATE AGENT','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('FRUIT VENDOR','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('GARDENER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('GENERAL MANAGER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('GERIATRIC NURSE','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('GLASS TECHNICIAN','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('GRAPHIC ARTIST','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('GRAPHICS INSTALLER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('GRILL FOOD SHOP OWNER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('GYM INSTRUCTOR','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('HAIRDRESSER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('HANDYMAN','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('HEAD CUSTODIAN','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('HEALTH AND SAFETY OFFICER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('HOMEMAKER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('HOSTESS','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('HOUSEWIFE','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('HOUSEWIFE / PENSIONER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('HUMAN RESOURCES CLERK','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('HUMAN RESOURCES MANAGER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('IMMIGRATION DETENTION OFFICER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('IMPORTER / CONSULTANT','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('INDEPENDENT EDUCATION CONSULTANT','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('INSURANCE AGENT','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('INSURANCE MANAGER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('INSURANCE UNDERWRITER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('INTERNAL AUDITOR','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('IT TECHNICIAN','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('JANITOR','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('JEWELER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('JOURNALIST','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('KITCHEN ASSISTANT','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('LAB TECHNICIAN','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('LABORER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('LAND ASSISTANCE OFFICER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('LANDLADY','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('LANDLORD AND FRUIT VENDOR','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('LANDSCAPER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('LANDSCAPER / FARMER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('LAUNDRY ATTENDANT','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('LECTURER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('LEGAL CONSULTANT','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('LEGAL SECRETARY','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('LINES MAN','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('LOAN CONTROL AGENT','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('LOTTO MACHINE OPERATOR','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('MACHINE OPERATOR','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('MAILMAN','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('MAINTENANCE','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('MAINTENANCE SUPERVISOR','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('MAINTENANCE TECHNICIAN','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('MANAGER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('MARINE OPERATOR','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('MARKETING AND COMMUNICATIONS SPECIALIST','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('MARKETING CONSULTANT','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('MECHANIC','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('MEDICAL CLERK','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('MEDICAL SALES REPRESENTATIVE','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('MERCHANDISER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('NAIL TECHNICIAN','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('NANNY','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('NURSE','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('OFFICE ASSISTANT','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('OFFICE ATTENDANT','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('OFFICE CLERK','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('OFFICE MANAGER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('OFFICER / PLUMBER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('OPERATIONS MANAGER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('OPERATOR','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('ORDERLY','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('PAINT TECHNICIAN','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('PAINTER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('PATIENT CARE ASSISTANT','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('PERISHABLE MANAGER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('PET STORE OWNER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('PHARMACY TECHNICIAN','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('PLANT PROCESS OPERATOR','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('PLANT SHOP OWNER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('PLUMBER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('PLUMBING SUPERVISOR','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('POLICE OFFICER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('POSTAL WORKER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('PRE-SCHOOL TEACHER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('PRISON OFFICER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('PRIVATE CONTRACTOR','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('PROCESS TECHNICIAN','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('PRODUCTION WORKER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('PROFESSIONAL MERCHANDISER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('PROGRAM COORDINATOR','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('PROJECT ASSISTANT','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('PROJECT OFFICER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('PROPRIETOR','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('PROPRIETOR BEVECA SPORTS BAR AND LOUNGE','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('PROPRIETOR CALDON''S CORNER BAR','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('PROPRIETOR HAMLET APPLIANCE PARTS AND REPAIR SERVICES','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('PROPRIETOR HULDER ENTERPRISES','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('PROPRIETOR JOINER AT CASKET CLASSIQUE','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('PROPRIETOR RODUBA CONTRACTING CONCEPTS AND CAR SERVICES','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('PROPRIETOR VERTEX SECURITY SOLUTIONS','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('PSYCHOLOGIST','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('PUBLIC HEALTH OFFICER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('PURCHASING OFFICER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('QUALITY CONTROL INSPECTOR','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('RADIOGRAPHER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('RAMP ATTENDANT','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('REAL ESTATE AGENT','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('REAL ESTATE BROKER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('REALTOR','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('RECEIVING CLERK','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('RECEPTIONIST','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('RECRUITMENT OFFICER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('REGISTERED NURSE','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('REGISTRATION OFFICER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('REHABILITATION AID','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('RETIRED','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('RETIRED PILOT','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('RETIRED POLICE OFFICER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('RETIRED SOLDIER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('RIGGER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('SALES AGENT','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('SALES ASSOCIATE','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('SALES CLERK','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('SALES CONSULTANT','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('SALES REPRESENTATIVE','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('SALESMAN HELPER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('SALESWOMAN','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('SANITATION WORKER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('SCHOOL LABORATORY TECHNICIAN','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('SEAMAN','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('SECRETARY','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('SECURITY GUARD','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('SECURITY OFFICER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('SELF-EMPLOYED BARBECUE BOSS','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('SELF-EMPLOYED BHAM STYLING','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('SENIOR COURSE ADMINISTRATOR','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('SENIOR INTERFACE OFFICER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('SENIOR PROJECT OFFICER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('SENIOR REGISTRATION OFFICER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('SERVICE TECHNICIAN','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('SHIFT SUPERVISOR','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('SHIPPING MANAGER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('SKY CAP PORTER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('SOLDIER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('STENOGRAPHER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('STORE CLERK','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('STUDENT','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('SUPERVISOR','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('TAXI DRIVER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('TEACHER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('TEAM LEADER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('TECHNICAL INSTRUCTOR','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('TECHNICAL OFFICER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('TECHNICIAN','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('TELEPHONE OPERATOR','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('TELEPHONE TECHNICIAN','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('TRADE DEVELOPMENT SPECIALIST','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('TRADESMAN','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('TRAINING REPRESENTATIVE','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('TRANSPORT DRIVER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('TRUCK CONTRACTOR','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('TRUCK DRIVER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('TSTT ENGINEER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('TUTOR / CATERER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('UNDERTAKER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('UNDERWRITER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('UPHOLSTERER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('VALIDATOR','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('VICE PRINCIPAL','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('WARDSMAID','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('WAREHOUSE CLERK','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('WAREHOUSE COORDINATOR','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('WAREHOUSE MANAGER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('WELDER','0000',1,1000,'TT')
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('WOODSMAN','0000',1,1000,'TT')  
GO

-- TRINIDAD AND TOBAGO IDENTIFICATION_TYPES
IF (NOT EXISTS (SELECT * FROM identification_types WHERE idt_country_iso_code2 = 'TT' ))
BEGIN
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (80,1,100,'Other','TT')
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (81,1,1,'Driver''s license','TT')
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (82,1,2,'Passport','TT')
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (120,1,3,'National ID','TT')
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (121,1,4,'Membership Form','TT')
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (122,1,5,'Proof of Address','TT')  
END
GO

--
-- COSTA RICA 
--

-- COSTA RICA REGIONS
IF (NOT EXISTS (SELECT * FROM federal_states WHERE fs_country_iso_code2 = 'CR' ))
BEGIN
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('San Jos�','CR')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Alajuela','CR')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Cartago','CR')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Heredia','CR')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Lim�n','CR')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Guanacaste','CR')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Puntarenas','CR')
END
GO

-- COSTA RICA OCCUPATIONS
IF (NOT EXISTS (SELECT * FROM occupations WHERE oc_country_iso_code2 = 'CR' ))
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('NO DISPONIBLE','0000001',1,1000,'CR')
GO

-- COSTA RICA IDENTIFICATION_TYPES
IF (NOT EXISTS (SELECT * FROM identification_types WHERE idt_country_iso_code2 = 'CR' ))
BEGIN
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (100,1,100,'Otro','CR')
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (101,1,1,'C�dula de Identidad','CR')
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (102,1,2,'Pasaporte','CR')
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (103,1,3,'Licencia de conducir','CR')
END
GO

--
-- URUGUAYAN
--

-- URUGUAYAN REGIONS
IF (NOT EXISTS (SELECT * FROM federal_states WHERE fs_country_iso_code2 = 'UY' ))
BEGIN
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Artigas','UY')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Canelones','UY')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Cerro Largo','UY')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Colonia','UY')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Durazno','UY')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Flores','UY')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Florida','UY')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Lavalleja','UY')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Maldonado','UY')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Montevideo','UY')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Paysand�','UY')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('R�o Negro','UY')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Rivera','UY')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Rocha','UY')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Salto','UY')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('San Jos�','UY')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Soriano','UY')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Tacuaremb�','UY')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Treinta y Tres','UY')
END
GO

-- URUGUAYAN OCCUPATIONS
IF (NOT EXISTS (SELECT * FROM occupations WHERE oc_country_iso_code2 = 'UY' ))
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('NO DISPONIBLE','0000001',1,1000,'UY')
GO

-- URUGUAYAN IDENTIFICATION_TYPES
IF (NOT EXISTS (SELECT * FROM identification_types WHERE idt_country_iso_code2 = 'UY' ))
BEGIN
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (110,1,100,'Otro','UY')
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (111,1,1,'CI','UY')
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (112,1,2,'CPF','UY')
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (113,1,3,'DNI','UY')
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (114,1,4,'Licencia C�vica','UY')
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (115,1,5,'Licencia de Conductor','UY')
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (116,1,6,'Pasaporte','UY')
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (117,1,7,'RG','UY')
END
GO

--
-- SPAIN
--

-- SPAIN OCCUPATIONS
IF (NOT EXISTS (SELECT * FROM occupations WHERE oc_country_iso_code2 = 'ES' ))
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('NO DISPONIBLE','0000001',1,1000,'ES')
GO

-- SPAIN IDENTIFICATION_TYPES
IF (NOT EXISTS (SELECT * FROM identification_types WHERE idt_country_iso_code2 = 'ES' ))
BEGIN
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (90,1,100,'Otro','ES')
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (91,1,1,'DNI','ES')
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (92,1,2,'Pasaporte','ES')
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (93,1,3,'Permiso Conducir','ES')
END
GO

-- SPAIN REGIONS
IF (NOT EXISTS (SELECT * FROM federal_states WHERE fs_country_iso_code2 = 'ES' ))
BEGIN
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('�lava','ES')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Albacete','ES')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Alicante','ES')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Almer�a','ES')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Asturias','ES')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('�vila','ES')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Badajoz','ES')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Barcelona','ES')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Burgos','ES')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('C�ceres','ES')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('C�diz','ES')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Cantabria','ES')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Castell�n','ES')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Ciudad Real','ES')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('C�rdoba','ES')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('La Coru�a','ES')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Cuenca','ES')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Gerona','ES')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Granada','ES')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Guadalajara','ES')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Guip�zcoa','ES')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Huelva','ES')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Huesca','ES')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Islas Baleares','ES')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Ja�n','ES')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Le�n','ES')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('L�rida','ES')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Lugo','ES')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Madrid','ES')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('M�laga','ES')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Murcia','ES')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Navarra','ES')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Orense','ES')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Palencia','ES')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Las Palmas','ES')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Pontevedra','ES')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('La Rioja','ES')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Salamanca','ES')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Segovia','ES')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Sevilla','ES')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Soria','ES')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Tarragona','ES')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Santa Cruz de Tenerife','ES')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Teruel','ES')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Toledo','ES')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Valencia','ES')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Valladolid','ES')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Vizcaya','ES')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Zamora','ES')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Zaragoza','ES')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Melilla','ES')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Ceuta','ES')
END
GO

--
-- IRELAND
--

-- IRELAND IDENTIFICATION_TYPES
IF (NOT EXISTS (SELECT * FROM identification_types WHERE idt_country_iso_code2 = 'IE' ))
BEGIN
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (83,1,100,'Other','IE')
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (84,1,2,'Passport','IE')
END
GO

--
-- PANAMA
-- 

-- PANAM� PROVINCES
IF (NOT EXISTS (SELECT * FROM federal_states WHERE fs_country_iso_code2 = 'PA' ))
BEGIN
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Bocas del Toro','PA')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Cocl�','PA')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Col�n','PA')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Chiriqu�','PA')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Dari�n','PA')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Herrera','PA')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Los Santos','PA')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Panam�','PA')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Panam� Oeste','PA')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Veraguas','PA')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Guna Yala','PA')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Ember�-Wounaan','PA')
  INSERT INTO [dbo].[federal_states] (fs_name,fs_country_iso_code2) VALUES ('Ng�be-Bugl�','PA')
END

-- PANAM� OCCUPATIONS
IF (NOT EXISTS (SELECT * FROM occupations WHERE oc_country_iso_code2 = 'PA' ))
  INSERT INTO [dbo].[occupations] (oc_description,oc_code,oc_enabled,oc_order,oc_country_iso_code2) VALUES ('NO DISPONIBLE','0000001',1,1000,'PA')

-- PANAM� IDENTIFICATION_TYPES
IF (NOT EXISTS (SELECT * FROM identification_types WHERE idt_country_iso_code2 = 'PA' ))
BEGIN
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (30,1,100,'Otro','PA')
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (31,1,1,'Pasaporte','PA')
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (32,1,2,'C�dula de identidad','PA')
  INSERT [dbo].[identification_types] (idt_id,idt_enabled,idt_order,idt_name,idt_country_iso_code2) VALUES (33,1,3,'Licencia de conducir','PA')
END
GO


/****** STORED *******/


