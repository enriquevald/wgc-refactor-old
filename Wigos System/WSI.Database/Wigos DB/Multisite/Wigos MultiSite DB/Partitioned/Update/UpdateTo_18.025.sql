/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_100]
GO

/**** DECLARATION SECTION *****/
DECLARE 
@Exp_ClientId int, 
@Exp_CommonBuildId int,
@Exp_ClientBuildId int,
@Exp_ReleaseId int,
@New_ReleaseId int,
@New_ScriptName nvarchar(50),
@New_Description nvarchar(1000);

/**** INITIALIZATION SECTION *****/
SET @Exp_ClientId = 18;
SET @Exp_CommonBuildId = 100;
SET @Exp_ClientBuildId = 1;
SET @Exp_ReleaseId = 24;

SET @New_ReleaseId = 25;
SET @New_ScriptName = N'UpdateTo_18.025.sql';
SET @New_Description = N'Alter column of last_account_activity';

/**** CHECK VERSION SECTION *****/
IF (NOT EXISTS (SELECT * FROM db_version WHERE db_client_id = @Exp_ClientId and db_common_build_id = @Exp_CommonBuildId and db_client_build_id = @Exp_ClientBuildId and db_release_id = @Exp_ReleaseId))
BEGIN
/**** VERSION FAILURE SECTION *****/
SELECT 'Wrong DB db_release_id: ' + CAST(db_release_id AS varchar(5)) + '. Expected: ' + CAST(@Exp_ReleaseId AS varchar(5))
FROM DB_VERSION

raiserror('Not updated', 20, -1) with log
END

/**** BODY SECTION *****/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

/**** UPDATE VERSION SECTION *****/
UPDATE db_version
   SET db_release_id = @New_ReleaseId
     , db_updated_script = @New_ScriptName
     , db_updated = GetDate()
     , db_description = @New_Description
 WHERE db_client_id = @Exp_ClientId
   AND db_common_build_id = @Exp_CommonBuildId
   AND db_client_build_id = @Exp_ClientBuildId
   AND db_release_id = @Exp_ReleaseId;
GO

/****** TABLES ******/
ALTER TABLE [last_account_activity] ALTER COLUMN [laa_last_activity] DATETIME NOT NULL
GO

/******* RECORDS *******/
UPDATE   GENERAL_PARAMS
   SET   GP_KEY_VALUE = 'SMGVDF'
 WHERE   GP_GROUP_KEY = 'AntiMoneyLaundering'   AND GP_SUBJECT_KEY = 'BaseName'

UPDATE   GENERAL_PARAMS
   SET   GP_KEY_VALUE = 'Por la ley antilavado de dinero, al realizar este reintegro, el cliente va a ser incluido en un reporte mensual para el SAT.'
 WHERE   GP_GROUP_KEY = 'AntiMoneyLaundering'   AND GP_SUBJECT_KEY = 'Prize.Report.Message'
 
UPDATE   GENERAL_PARAMS
   SET   GP_KEY_VALUE = 'Por la ley antilavado de dinero, el cliente ser� incluido en un reporte mensual para el SAT en pr�ximos reintegros.'
 WHERE   GP_GROUP_KEY = 'AntiMoneyLaundering'   AND GP_SUBJECT_KEY = 'Prize.Report.Warning.Message'
 
UPDATE   GENERAL_PARAMS
   SET   GP_KEY_VALUE = 'Por la ley antilavado de dinero, al realizar esta recarga, el cliente va a ser incluido en un reporte mensual para el SAT.'
 WHERE   GP_GROUP_KEY = 'AntiMoneyLaundering'   AND GP_SUBJECT_KEY = 'Recharge.Report.Message'
 
UPDATE   GENERAL_PARAMS
   SET   GP_KEY_VALUE = 'Por la ley antilavado de dinero, el cliente ser� incluido en un reporte mensual para el SAT en pr�ximas recargas.'
 WHERE   GP_GROUP_KEY = 'AntiMoneyLaundering'   AND GP_SUBJECT_KEY = 'Recharge.Report.Warning.Message'
GO 
 