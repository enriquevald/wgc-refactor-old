/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_800]
GO

/**** FUNCTION SECTION *****/
CREATE FUNCTION [dbo].[TMP_SplitStringIntoTable] 
(
  @pString VARCHAR(MAX),
  @pDelimiter VARCHAR(10),
  @pTrimItems BIT = 1
)
RETURNS 
@ReturnTable TABLE 
(
  [SST_ID] [INT] IDENTITY(1,1) NOT NULL,
  [SST_VALUE] [VARCHAR](100) NULL
)
AS
BEGIN		   
  DECLARE @_ISPACES INT
  DECLARE @_PART VARCHAR(50)
  
  -- INITIALIZE SPACES
  SELECT @_ISPACES = CHARINDEX(@pDelimiter,@pString,0)
  WHILE @_ISPACES > 0
  
  BEGIN
    SELECT @_PART = SUBSTRING(@pString,0,CHARINDEX(@pDelimiter,@pString,0))
  
    IF @pTrimItems = 1
    SET @_PART = LTRIM(RTRIM(@_PART))
  
    INSERT INTO @ReturnTable(SST_VALUE) SELECT @_PART
  
    SELECT @pString = SUBSTRING(@pString, @_ISPACES + LEN(@pDelimiter), LEN(@pString) - CHARINDEX(@pDelimiter, @pString, 0))
  
    SELECT @_ISPACES = CHARINDEX(@pDelimiter,@pString,0)
  END
  
  IF LEN(@pString) > 0
    INSERT INTO @ReturnTable
  	  SELECT CASE WHEN @pTrimItems = 1 THEN LTRIM(RTRIM(@pString)) ELSE @pString END
  
    RETURN 
  END  
GO  

CREATE PROCEDURE [dbo].[TMP_InsertInSasMeterCatalog] 
  @pBill AS DECIMAL
AS
BEGIN   
  DECLARE @_meter_code AS INTEGER
	
  SET @_meter_code = -1
  
  SELECT @_meter_code = 
    CASE WHEN @pBill = 1       THEN 64	
         WHEN @pBill = 2       THEN 65	
         WHEN @pBill = 5       THEN 66	
         WHEN @pBill = 10      THEN 67	
         WHEN @pBill = 20      THEN 68	
         WHEN @pBill = 25      THEN 69	
         WHEN @pBill = 50      THEN 70	
         WHEN @pBill = 100     THEN 71	
         WHEN @pBill = 200     THEN 72	
         WHEN @pBill = 250     THEN 73	
         WHEN @pBill = 500     THEN 74	
         WHEN @pBill = 1000    THEN 75	
         WHEN @pBill = 2000    THEN 76	
         WHEN @pBill = 2500    THEN 77	
         WHEN @pBill = 5000    THEN 78	
         WHEN @pBill = 10000   THEN 79	
         WHEN @pBill = 20000   THEN 80	
         WHEN @pBill = 25000   THEN 81	
         WHEN @pBill = 50000   THEN 82	
         WHEN @pBill = 100000  THEN 83	
         WHEN @pBill = 200000  THEN 84	
         WHEN @pBill = 250000  THEN 85	
         WHEN @pBill = 500000  THEN 86	
         WHEN @pBill = 1000000 THEN 87	
    END

  IF (ISNULL(@_meter_code,-1) = -1)
    SELECT 'Meter code not inserted: Bill -> ' +  CONVERT(VARCHAR(50),@pBill)
  ELSE IF (EXISTS(SELECT * FROM SAS_METERS_CATALOG_PER_GROUP WHERE SMCG_GROUP_ID = 10002 AND SMCG_METER_CODE = @_meter_code))
    SELECT 'Bill already inserted: Bill -> ' +  CONVERT(VARCHAR(50),@pBill)
  ELSE
    INSERT INTO SAS_METERS_CATALOG_PER_GROUP (SMCG_GROUP_ID,SMCG_METER_CODE) VALUES (10002,@_meter_code)
  
END  
GO 

/**** DECLARATION SECTION *****/
DECLARE @CurrencyISOCode         AS VARCHAR(3)
DECLARE @CountryCode             AS VARCHAR(2)
DECLARE @CurrencySymbol          AS VARCHAR(5)
DECLARE @CommonLanguage          AS VARCHAR(5)
DECLARE @CurrenciesDenominations AS VARCHAR(MAX)
DECLARE @CurrentCurrencyDenom    AS DECIMAL

SET @CurrencyISOCode         = 'ARS'
SET @CountryCode             = 'AR'
SET @CurrencySymbol          = '$'    
SET @CommonLanguage          = 'es' -- 'es' OR 'en-US'
SET @CurrenciesDenominations = '-200,-100,-50,-2,-1,2,5,10,20,50,100,200,500' -- Negative values are reserved denominations. Specify bills denominations.

-- CurrencyISOCode   
DELETE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'RegionalOptions' AND GP_SUBJECT_KEY = 'CurrencyISOCode'
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('RegionalOptions', 'CurrencyISOCode', @CurrencyISOCode)

-- CountryISOCode2
DELETE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'RegionalOptions' AND GP_SUBJECT_KEY = 'CountryISOCode2'
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY, GP_KEY_VALUE, GP_MS_DOWNLOAD_TYPE) VALUES ('RegionalOptions', 'CountryISOCode2', @CountryCode,1)

-- CurrencySymbol
DELETE FROM GENERAL_PARAMS WHERE (GP_GROUP_KEY = 'SasHost' OR GP_GROUP_KEY = 'WigosGUI') AND GP_SUBJECT_KEY = 'CurrencySymbol'
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('SasHost',  'CurrencySymbol', @CurrencySymbol)
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('WigosGUI', 'CurrencySymbol', @CurrencySymbol)

-- CommonLenguage
DELETE FROM GENERAL_PARAMS WHERE (GP_GROUP_KEY = 'SasHost' OR GP_GROUP_KEY = 'WigosGUI' OR GP_GROUP_KEY = 'Cashier') AND GP_SUBJECT_KEY = 'Language'
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('SasHost',  'Language', @CommonLanguage)
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('WigosGUI', 'Language', @CommonLanguage)
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Cashier',  'Language', @CommonLanguage)

-- CurrenciesAccepted
DELETE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'RegionalOptions' AND GP_SUBJECT_KEY = 'CurrenciesAccepted'
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('RegionalOptions', 'CurrenciesAccepted', @CurrencyISOCode)

UPDATE GENERAL_PARAMS SET GP_KEY_VALUE = 'Provincia' WHERE GP_GROUP_KEY = 'Account.Fields'  AND GP_SUBJECT_KEY = 'State.Name'
UPDATE GENERAL_PARAMS SET GP_KEY_VALUE = 'AR'     WHERE GP_GROUP_KEY = 'Account.DefaultValues' AND GP_SUBJECT_KEY = 'Country'
UPDATE GENERAL_PARAMS SET GP_KEY_VALUE = '160'    WHERE GP_GROUP_KEY = 'Account.DefaultValues' AND GP_SUBJECT_KEY = 'DocumentType' -- C�dula de Identidad

SELECT SST_VALUE INTO #TMP_CURRENCY_DEN FROM [TMP_SplitStringIntoTable] (@CurrenciesDenominations, ',', 1)
    
DECLARE Curs_Denoms CURSOR FOR SELECT CAST(SST_VALUE AS DECIMAL) FROM #TMP_CURRENCY_DEN 

SET NOCOUNT ON;
OPEN Curs_Denoms

FETCH NEXT FROM Curs_Denoms INTO @CurrentCurrencyDenom
  
WHILE @@FETCH_STATUS = 0
  BEGIN      
   	  IF @CurrentCurrencyDenom > 0
   	  begin
	    EXEC dbo.TMP_InsertInSasMeterCatalog @CurrentCurrencyDenom
	  end 
	
    FETCH NEXT FROM Curs_Denoms INTO @CurrentCurrencyDenom    
  END
    
CLOSE Curs_Denoms
DEALLOCATE Curs_Denoms   
DROP TABLE #TMP_CURRENCY_DEN
DROP FUNCTION [dbo].[TMP_SplitStringIntoTable] 
DROP PROCEDURE [dbo].[TMP_InsertInSasMeterCatalog] 
GO

