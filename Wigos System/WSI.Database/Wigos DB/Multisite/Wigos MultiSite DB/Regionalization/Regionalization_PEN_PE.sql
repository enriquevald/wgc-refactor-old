/****** IMPORTANT: Database Id. has to be changed depending on the installation ******/
USE [wgdb_100]
GO

/**** DECLARATION SECTION *****/
DECLARE @CurrencyISOCode         AS VARCHAR(3)
DECLARE @CountryCode             AS VARCHAR(2)
DECLARE @CurrencySymbol          AS VARCHAR(5)
DECLARE @CommonLanguage          AS VARCHAR(5)

SET @CurrencyISOCode         = 'PEN'
SET @CountryCode             = 'PE'
SET @CurrencySymbol          = 'S/.'    
SET @CommonLanguage          = 'es' -- 'es' OR 'en-US'

-- CurrencyISOCode   
DELETE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'RegionalOptions' AND GP_SUBJECT_KEY = 'CurrencyISOCode'
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('RegionalOptions', 'CurrencyISOCode', @CurrencyISOCode)

-- CountryISOCode2
DELETE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'RegionalOptions' AND GP_SUBJECT_KEY = 'CountryISOCode2'
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY,GP_SUBJECT_KEY, GP_KEY_VALUE, GP_MS_DOWNLOAD_TYPE) VALUES ('RegionalOptions', 'CountryISOCode2', @CountryCode,1)

-- CurrencySymbol
DELETE FROM GENERAL_PARAMS WHERE (GP_GROUP_KEY = 'SasHost' OR GP_GROUP_KEY = 'WigosGUI') AND GP_SUBJECT_KEY = 'CurrencySymbol'
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('SasHost',  'CurrencySymbol', @CurrencySymbol)
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('WigosGUI', 'CurrencySymbol', @CurrencySymbol)

-- CommonLenguage
DELETE FROM GENERAL_PARAMS WHERE (GP_GROUP_KEY = 'SasHost' OR GP_GROUP_KEY = 'WigosGUI' OR GP_GROUP_KEY = 'Cashier') AND GP_SUBJECT_KEY = 'Language'
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('SasHost',  'Language', @CommonLanguage)
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('WigosGUI', 'Language', @CommonLanguage)
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('Cashier',  'Language', @CommonLanguage)

-- CurrenciesAccepted
DELETE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'RegionalOptions' AND GP_SUBJECT_KEY = 'CurrenciesAccepted'
INSERT INTO GENERAL_PARAMS (GP_GROUP_KEY, GP_SUBJECT_KEY, GP_KEY_VALUE) VALUES ('RegionalOptions', 'CurrenciesAccepted', @CurrencyISOCode)

UPDATE GENERAL_PARAMS SET GP_KEY_VALUE = 'Regi�n' WHERE GP_GROUP_KEY = 'Account.Fields'        AND GP_SUBJECT_KEY = 'State.Name'
UPDATE GENERAL_PARAMS SET GP_KEY_VALUE = 'PE'     WHERE GP_GROUP_KEY = 'Account.DefaultValues' AND GP_SUBJECT_KEY = 'Country'
UPDATE GENERAL_PARAMS SET GP_KEY_VALUE = '041'    WHERE GP_GROUP_KEY = 'Account.DefaultValues' AND GP_SUBJECT_KEY = 'DocumentType' -- DNI
GO