
/* 
**********************************************************
**********************************************************
						JOBS
**********************************************************
**********************************************************
 */
 /* 
**********************************************************
jb_Egm_Meters_Reporting
**********************************************************
 */
/*
----------------------------------------------------------------------------------------------------------------
Stored Procedure: 'jb_Egm_Meters_Reporting_wgdb_xxx'.

Job that links all related stored procedures to have an scheduling and recursive calling to them to perform
infinite loop on information getting.

Default value for new installation and new related model tables creation for the first execution is beggining 
of the previous working day.

Version     Date          User      Description
----------------------------------------------------------------------------------------------------------------
1.0.0       02-FEB-2018   OMC       New job
1.0.1       05-FEB-2018   DPC       Update job

Requeriments:

Parameters:
	
*/
DECLARE @start_job_return		INT;
DECLARE @schedule_id 			INT;
DECLARE @job_id                 BINARY(16);
DECLARE @job_id_To_Delete       BINARY(16);
DECLARE @job_name               VARCHAR(100);
DECLARE @dest_db_name           VARCHAR(100);
DECLARE @step_name_1            VARCHAR(100);
DECLARE @step_name_2			VARCHAR(100);

SET @dest_db_name = (SELECT
		DB_NAME());
SET @job_name = 'jb_Egm_Meters_Reporting_' + DB_NAME();
SET @step_name_1 = 'Drop, Create, UPSERT data in tables';

IF  EXISTS (SELECT job_id FROM msdb.dbo.sysjobs_view WHERE name = @job_name)
	BEGIN
		SET @job_id_To_Delete = (SELECT job_id FROM msdb.dbo.sysjobs_view WHERE name = @job_name);
		EXEC msdb.dbo.sp_delete_job @job_id = @job_id_To_Delete ,@delete_unused_schedule = 1;
		PRINT 'Job ''' + @job_name + ''' deleted succesfully.';
	END

BEGIN TRANSACTION
	DECLARE @ReturnCode INT
SELECT
	@ReturnCode = 0

IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name = N'Data Collector' AND category_class = 1)
	BEGIN
		EXEC @ReturnCode = msdb.dbo.sp_add_category  @class = N'JOB'
													,@type = N'LOCAL'
													,@name = N'Data Collector'
IF (@@ERROR <> 0 OR @ReturnCode <> 0)
	GOTO QuitWithRollback
END

	DECLARE @jobId BINARY(16)
	EXEC @ReturnCode = msdb.dbo.sp_add_job @job_name 				= @job_name
										  ,@enabled 				= 1
										  ,@notify_level_eventlog 	= 0
										  ,@notify_level_email 		= 0
										  ,@notify_level_netsend 	= 0
										  ,@notify_level_page 		= 0
										  ,@delete_level 			= 0
										  ,@description 			= N'This job is designed for collect meters history data to be reported into [egm_meters_by_period], [egm_meters_by_day] and [egm_meters_daily] tables.'
										  ,@category_name 			= N'Data Collector'
										  ,@owner_login_name 		= N'sa'
										  ,@job_id 					= @jobId OUTPUT
	PRINT 'Job ''' + @job_name + ''' created succesfully.'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) 
	GOTO QuitWithRollback
/* Step [1-. Drop, Create & Fill Temporary Working Table With Current Control Datetime Period] */
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_name = @job_name
										  ,@step_name = @step_name_1
										  ,@step_id = 1
										  ,@cmdexec_success_code = 0
										  ,@on_success_action = 1
										  ,@on_success_step_id = 0
										  ,@on_fail_action = 2
										  ,@on_fail_step_id = 0
										  ,@retry_attempts = 5
										  ,@retry_interval = 0
										  ,@os_run_priority = 0
										  ,@subsystem = N'TSQL'
										  ,@database_name = @dest_db_name
										  ,@flags = 12
										  ,@command = N'DECLARE @param_datetime		  DATETIME
DECLARE @updated_control_mark	  DATETIME;
DECLARE @sas_meters_interval	  INT;
DECLARE @param_int_datetime  	  INT;
DECLARE @param_site_id  		  INT;
DECLARE @interval_exe_job       INT;
DECLARE @is_multisite		  BIT;
DECLARE @SQL AS NVARCHAR(MAX)
DECLARE @SQL_AUX nvarchar(100)
DECLARE @SITES_AUX TABLE(SITE_ID BIGINT  NULL)

BEGIN TRY
BEGIN TRANSACTION
    --================================================
    
	SET @SQL_AUX = ''''
	
    -- 0.- Get GP interval
    SET @sas_meters_interval = ISNULL((SELECT gp_key_value FROM general_params WHERE gp_group_key = ''WCP'' AND gp_subject_key = ''IntervalBilling''), 60)
    
    SELECT @interval_exe_job = SYSS.freq_subday_interval 
    FROM msdb..sysjobs as SYSJ
    INNER JOIN msdb..sysjobschedules AS SYSJS ON SYSJ.job_id = SYSJS.job_id
    INNER JOIN msdb..sysschedules SYSS ON SYSS.schedule_id = SYSJS.schedule_id
          WHERE   SYSJ.name LIKE ''jb_Egm_Meters_Reporting_'' + DB_NAME()

	SET @is_multisite = 0
    SET @sas_meters_interval = CASE WHEN @sas_meters_interval < @interval_exe_job THEN @interval_exe_job ELSE @sas_meters_interval END
	
    SELECT   @is_multisite  = GP_KEY_VALUE FROM   GENERAL_PARAMS WHERE   GP_GROUP_KEY   = ''MultiSite'' AND   GP_SUBJECT_KEY = ''IsCenter''
    	
	IF (@is_multisite=1)
	   SET @SQL_AUX = '' ecm.ecm_site_id = tsmh.tsmh_site_id AND ''

	SET @SQL = ''            SELECT   ecm.ecm_site_id'' 
	SET @SQL = @SQL + ''       FROM   terminal_sas_meters_history tsmh WITH (INDEX(IX_tsmh_datetime_type))''
	SET @SQL = @SQL + '' INNER JOIN   egm_control_mark ecm ON #SITEID# tsmh.tsmh_datetime > DATEADD(MINUTE,-2*''+Convert(nvarchar(99),@sas_meters_interval)+'', ecm.ecm_control_mark)''
	SET @SQL = @SQL + ''	     WHERE ( tsmh_meter_origin IN (0, 1, 2)'' 
	SET @SQL = @SQL + ''	        OR   tsmh_meter_origin IS NULL)'' 
	SET @SQL = @SQL + ''	       AND   tsmh_type IN (1, 10, 11, 12, 13, 14, 15, 16 , 110, 140, 150, 160, 170) ''
	SET @SQL = @SQL + ''	       AND tsmh_meter_code in (0,1,2,5) ''
	SET @SQL = @SQL + ''   GROUP BY   ecm.ecm_site_id''

	SET @SQL = REPLACE(@SQL, ''#SITEID#'', @SQL_AUX)

	INSERT INTO @SITES_AUX  
	EXECUTE(@SQL)		

    DECLARE @SiteId AS INT
    DECLARE CurSite CURSOR FOR SELECT * FROM @SITES_AUX  
    OPEN CurSite
    FETCH NEXT FROM CurSite INTO @SiteId
    WHILE @@fetch_status = 0
    BEGIN   
	   -- 1.- Get mark date from stie id
	   EXEC sp_EGM_Meters_Get_Control_Mark  @param_site_id=@SiteId, @param_datetime=@param_datetime OUTPUT;
	   -- 2.- Generate data from site id and date mark
	   EXEC sp_EGM_Meters_Drop_Create_And_Fill_Temporary_Working_Table @param_site_id=@SiteId, @param_datetime=@param_datetime;	   	   		   
	   -- 3-. UPSERT egm_meters_by_period
	   EXEC sp_EGM_Meters_Upsert_Table_egm_meters_by_period							
	   SET @param_int_datetime = dbo.GamingDayFromDateTime(@param_datetime);
	   -- 4-. UPSERT egm_meters_by_day
	   EXEC sp_EGM_Meters_Upsert_Table_egm_meters_by_day @param_working_day = @param_int_datetime;
	   -- 5-. UPSERT egm_meters_daily
	   EXEC sp_EGM_Meters_Upsert_Table_egm_meters_daily @param_int_datetime  = @param_int_datetime;
	   -- 6-. UPSERT egm_meters_max_value
	   EXEC sp_EGM_Meters_Upsert_Table_egm_meters_max_value @param_datetime = @param_datetime;
	   -- 7-. UPDATE CONTROL MARK [Only If process ends succesfully control mark will be updated].
	   EXEC sp_EGM_Meters_Get_Interval @param_site_id = @SiteId, @param_interval = @sas_meters_interval OUTPUT
	   SET @updated_control_mark = DATEADD(MINUTE, @sas_meters_interval,  (SELECT TOP(1) ecm_control_mark FROM egm_control_mark where ecm_site_id = @SiteId));
	   EXEC sp_EGM_Meters_Update_Control_Mark @param_site_id=@SiteId, @param_datetime=@updated_control_mark;

	   FETCH NEXT FROM CurSite INTO @SiteId
    END
    CLOSE CurSite
    DEALLOCATE CurSite

    --================================================    
COMMIT
END TRY
BEGIN CATCH
EXEC dbo.spErrorHandling
ROLLBACK
END CATCH'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) 
	GOTO QuitWithRollback

EXEC @ReturnCode = msdb.dbo.sp_update_job @job_name 		= @job_name
										 ,@start_step_id 	= 1
PRINT ' - Job step ''' + @job_name + '.' + @step_name_1 + ''' appended succesfully.'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
	EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_name = @job_name
												,@server_name = N'(local)'
PRINT 'Job ''' + @job_name + ''' added succesfully.'
	
IF (@@ERROR <> 0 OR @ReturnCode <> 0) 
	GOTO QuitWithRollback
		COMMIT TRANSACTION
	GOTO EndSave
	QuitWithRollback:
		IF (@@TRANCOUNT > 0) 
			ROLLBACK TRANSACTION
	EndSave:
/* Creation And Appending Schedule to the Job */
EXEC msdb.dbo.sp_add_jobschedule @job_name 					= @job_name
								,@name 						= N'EGM_Meters_Schedule'
								,@enabled 					= 1
								,@freq_type 				= 4
								,@freq_interval 			= 1
								,@freq_subday_type 			= 4
								,@freq_subday_interval 		= 2
								,@freq_relative_interval 	= 0
								,@freq_recurrence_factor 	= 1
								,@active_start_date 		= 20171227
								,@active_end_date 			= 99991231
								,@active_start_time 		= 0
								,@active_end_time 			= 235959
								,@schedule_id 				= @schedule_id OUTPUT

/* Zero Execution Of Job */
EXEC @start_job_return = msdb.dbo.sp_start_job @job_name = @job_name;
 