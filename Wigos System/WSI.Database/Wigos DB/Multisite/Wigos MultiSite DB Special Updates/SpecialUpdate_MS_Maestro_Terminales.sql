--//STORED PROCEDUDE
/****** Object:  StoredProcedure [dbo].[sp_GetTerminals]    Script Date: 11/17/2017 13:58:38 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Get_MSLotery_TerminalMaster]') AND type in (N'P', N'PC'))
 DROP PROCEDURE [dbo].[Get_MSLotery_TerminalMaster]
GO

CREATE PROCEDURE [dbo].[Get_MSLotery_TerminalMaster] @SiteId Int
AS
BEGIN

DECLARE @DefaultHold as Money
DECLARE @pLanguageID as Int

SET @DefaultHold = (SELECT 1 - CAST (GP_KEY_VALUE AS MONEY) / 100
FROM GENERAL_PARAMS
WHERE GP_GROUP_KEY = 'PlayerTracking'
 AND GP_SUBJECT_KEY = 'TerminalDefaultPayout')
 
SELECT @pLanguageID = case UPPER(gp_key_value)
		when 'ES' then 9
		else 10 end
FROM GENERAL_PARAMS
WHERE GP_GROUP_KEY = 'WigosGUI'
 AND GP_SUBJECT_KEY = 'Language'


SELECT te_site_id '[Id Sala]'
 , te_name as '[Terminal]'
 , te_floor_id as '[ID de Planta]'
 , te_external_id as '[ID protocolo]'
 , te_provider_id as '[Fabricante]'
 , te_cabinet_type as '[Tipo de gabinete]'
 , te_vendor_id as '[Vendedor]'
 , te_program as '[Programa]' 
 , te_serial_number as '[Numero de serie]' 
 , te_sas_accounting_denom as '[Denominacion contable]'
 , ISNULL(TE_THEORETICAL_HOLD, @DefaultHold)'[% de payout]'
 , te_number_lines as '[Numero de linias]'
 , te_contract_id as '[ID contrato]'
 , gt_name as '[Tipo de juego]' 
 
FROM Terminals
INNER JOIN game_types on te_game_type = gt_game_type and gt_language_id = @pLanguageID
WHERE te_site_id = @SiteId
AND te_terminal_type in (1, 3, 5, 108)

END
GO

GRANT EXECUTE ON [Get_MSLotery_TerminalMaster] TO [wggui] WITH GRANT OPTION
GO

--//GENERIC REPORT
BEGIN
	DECLARE @rtc_store_name	NVARCHAR(200)
  DECLARE @rtc_form_id INT
  DECLARE @rtc_design_sheet VARCHAR(MAX)
  DECLARE @rtc_design_filter VARCHAR(MAX)
  DECLARE @rtc_report_name VARCHAR(MAX)

  SET @rtc_store_name = 'Get_MSLotery_TerminalMaster'
	
  SET @rtc_report_name = '<LanguageResources><NLS09><Label>Terminal Master</Label></NLS09><NLS10><Label>Maestro de m�quinas</Label></NLS10></LanguageResources>'
	
  SET @rtc_design_sheet = '<ArrayOfReportToolDesignSheetsDTO>
   <ReportToolDesignSheetsDTO>
      <LanguageResources>
         <NLS09>
            <Label>Terminal Master</Label>
         </NLS09>
         <NLS10>
            <Label>Maestro de m�quinas</Label>
         </NLS10>
      </LanguageResources>
      <Columns>
         <!--Id Sala-->	  
         <ReportToolDesignColumn>
            <Code>[Id Sala]</Code>
            <Width>300</Width>
            <EquityMatchType>Equality</EquityMatchType>
            <LanguageResources>
               <NLS09>
                  <Label>Site Id</Label>
               </NLS09>
               <NLS10>
                  <Label>Id Sala</Label>
               </NLS10>
            </LanguageResources>
         </ReportToolDesignColumn>
         <!--Terminal-->	           
		 <ReportToolDesignColumn>
            <Code>[Terminal]</Code>
            <Width>300</Width>
            <EquityMatchType>Equality</EquityMatchType>
            <LanguageResources>
               <NLS09>
                  <Label>Code</Label>
               </NLS09>
               <NLS10>
                  <Label>C�digo</Label>
               </NLS10>
            </LanguageResources>
         </ReportToolDesignColumn>
         <!--ID de Planta-->	  
         <ReportToolDesignColumn>
            <Code>[ID de Planta]</Code>
            <Width>300</Width>
            <EquityMatchType>Equality</EquityMatchType>
            <LanguageResources>
               <NLS09>
                  <Label>Abbreviated #</Label>
               </NLS09>
               <NLS10>
                  <Label># abreviado</Label>
               </NLS10>
            </LanguageResources>
         </ReportToolDesignColumn>      
         <!--ID Protocolo-->	           
		 <ReportToolDesignColumn>
            <Code>[ID Protocolo]</Code>
            <Width>300</Width>
            <EquityMatchType>Equality</EquityMatchType>
            <LanguageResources>
               <NLS09>
                  <Label>Concentrator</Label>
               </NLS09>
               <NLS10>
                  <Label>Concentrador</Label>
               </NLS10>
            </LanguageResources>
         </ReportToolDesignColumn>
         <!--Fabricante-->	  
         <ReportToolDesignColumn>
            <Code>[Fabricante]</Code>
            <Width>300</Width>
            <EquityMatchType>Equality</EquityMatchType>
            <LanguageResources>
               <NLS09>
                  <Label>Provider</Label>
               </NLS09>
               <NLS10>
                  <Label>Fabricante</Label>
               </NLS10>
            </LanguageResources>
         </ReportToolDesignColumn>
         <!--Modelo-->	  
         <ReportToolDesignColumn>
            <Code>[Tipo de gabinete]</Code>
            <Width>300</Width>
            <EquityMatchType>Equality</EquityMatchType>
            <LanguageResources>
               <NLS09>
                  <Label>Model</Label>
               </NLS09>
               <NLS10>
                  <Label>Modelo</Label>
               </NLS10>
            </LanguageResources>
         </ReportToolDesignColumn>
         <!--Juego-->	  
         <ReportToolDesignColumn>
            <Code>[Vendedor]</Code>
            <Width>300</Width>
            <EquityMatchType>Equality</EquityMatchType>
            <LanguageResources>
               <NLS09>
                  <Label>Game</Label>
               </NLS09>
               <NLS10>
                  <Label>Juego</Label>
               </NLS10>
            </LanguageResources>
         </ReportToolDesignColumn>
         <!--Programa-->	  
         <ReportToolDesignColumn>
            <Code>[Programa]</Code>
            <Width>300</Width>
            <EquityMatchType>Equality</EquityMatchType>
            <LanguageResources>
               <NLS09>
                  <Label>Program</Label>
               </NLS09>
               <NLS10>
                  <Label>Programa</Label>
               </NLS10>
            </LanguageResources>
         </ReportToolDesignColumn>
         <!--Numero de serie-->	  
         <ReportToolDesignColumn>
            <Code>[Numero de serie]</Code>
            <Width>300</Width>
            <EquityMatchType>Equality</EquityMatchType>
            <LanguageResources>
               <NLS09>
                  <Label>Serial #</Label>
               </NLS09>
               <NLS10>
                  <Label># de serie</Label>
               </NLS10>
            </LanguageResources>
         </ReportToolDesignColumn>
         <!--Denominaci�n contable-->	  
         <ReportToolDesignColumn>
            <Code>[Denominacion contable]</Code>
            <Width>300</Width>
            <EquityMatchType>Equality</EquityMatchType>
            <LanguageResources>
               <NLS09>
                  <Label>Denomination</Label>
               </NLS09>
               <NLS10>
                  <Label>Denominaci�n</Label>
               </NLS10>
            </LanguageResources>
         </ReportToolDesignColumn>
         <!--% Devoluci�n-->	           
		 <ReportToolDesignColumn>
            <Code>[% de payout]</Code>
            <Width>300</Width>
            <EquityMatchType>Equality</EquityMatchType>
            <LanguageResources>
               <NLS09>
                  <Label>Refund %</Label>
               </NLS09>
               <NLS10>
                  <Label>% Devoluci�n</Label>
               </NLS10>
            </LanguageResources>
         </ReportToolDesignColumn>
         <!--Ficha-->	           
		 <ReportToolDesignColumn>
            <Code>[Numero de linias]</Code>
            <Width>300</Width>
            <EquityMatchType>Equality</EquityMatchType>
            <LanguageResources>
               <NLS09>
                  <Label>File</Label>
               </NLS09>
               <NLS10>
                  <Label>Ficha</Label>
               </NLS10>
            </LanguageResources>
         </ReportToolDesignColumn>		
         <!--Grupo progresivo-->	           
		 <ReportToolDesignColumn>
            <Code>[ID contrato]</Code>
            <Width>300</Width>
            <EquityMatchType>Equality</EquityMatchType>
            <LanguageResources>
               <NLS09>
                  <Label>Progressive group</Label>
               </NLS09>
               <NLS10>
                  <Label>Grupo progresivo</Label>
               </NLS10>
            </LanguageResources>
         </ReportToolDesignColumn>
          <!--Tipo de m�quina-->	           
		 <ReportToolDesignColumn>
            <Code>[Tipo de juego]</Code>
            <Width>300</Width>
            <EquityMatchType>Equality</EquityMatchType>
            <LanguageResources>
               <NLS09>
                  <Label>Machine type</Label>
               </NLS09>
               <NLS10>
                  <Label>Tipo de m�quina</Label>
               </NLS10>
            </LanguageResources>
         </ReportToolDesignColumn>		
      </Columns>
   </ReportToolDesignSheetsDTO>
</ArrayOfReportToolDesignSheetsDTO>'
  
  SET @rtc_design_filter = '<ReportToolDesignFilterDTO>
   <FilterType>CustomFilters</FilterType>
   <FilterText>
     <LanguageResources>
       <NLS09>
         <Label>Filters</Label>
       </NLS09>
       <NLS10>
	     <Label>Filtros</Label>
       </NLS10>
     </LanguageResources>
   </FilterText>
   <Filters>
	  <ReportToolDesignFilter>
         <TypeControl>uc_combo</TypeControl>
         <TextControl>
            <LanguageResources>
               <NLS09>
                  <Label>Site</Label>
               </NLS09>
               <NLS10>
                  <Label>Sala</Label>
               </NLS10>
            </LanguageResources>
         </TextControl>
         <ParameterStoredProcedure>
            <Name>@SiteId</Name>
            <Type>Int</Type>
         </ParameterStoredProcedure>
		 <TypeSource>Query</TypeSource>
		 <Query>SELECT st_site_id, REPLACE(STR(st_site_id, 3), SPACE(1), ''0'') + '' - '' + ISNULL(st_name,'''') st_name FROM sites</Query>
         <Value></Value>
      </ReportToolDesignFilter>   
   </Filters>
</ReportToolDesignFilterDTO>'
  
  IF NOT EXISTS(SELECT 1 
				FROM report_tool_config 
				WHERE rtc_store_name = @rtc_store_name)
  BEGIN
    SELECT @rtc_form_id = ISNULL(MAX(rtc_form_id), 10999) + 1
    FROM report_tool_config
    
    INSERT INTO [dbo].[REPORT_TOOL_CONFIG] 
    VALUES	/*RTC_REPORT_TOOL_ID */
						/*RTC_FORM_ID*/        (@rtc_form_id,
						/*RTC_LOCATION_MENU*/   2, 
            /*RTC_REPORT_NAME*/     @rtc_report_name,
            /*RTC_STORE_NAME*/      @rtc_store_name,
            /*RTC_DESIGN_FILTER*/   @rtc_design_filter,
            /*RTC_DESIGN_SHEETS*/   @rtc_design_sheet,
            /*RTC_MAILING*/         0,
            /*RTC_STATUS*/          1,
            /*RTC_MODE*/            36,
						/*RTC_HTML_HEADER*/			NULL,
		        /*RTC_HTML_FOOTER*/			NULL)
                                      
  END
  ELSE
  BEGIN
      UPDATE [dbo].[REPORT_TOOL_CONFIG] 
      SET RTC_DESIGN_SHEETS = /*RTC_DESIGN_SHEETS*/   @rtc_design_sheet
        , RTC_DESIGN_FILTER = /*RTC_DESIGN_FILTER*/   @rtc_design_filter
        , RTC_REPORT_NAME   = /*RTC_REPORT_NAME*/     @rtc_report_name
        , RTC_MODE_TYPE     =  /*RTC_MODE*/ 36
      WHERE RTC_STORE_NAME = @rtc_store_name
  END
END
GO

