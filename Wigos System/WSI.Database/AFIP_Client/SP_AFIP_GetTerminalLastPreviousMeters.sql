IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_AFIP_GetTerminalMetersHistory]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[SP_AFIP_GetTerminalMetersHistory]
GO
/*
----------------------------------------------------------------------------------------------------------------
BASIC REPORT QUERY FOR TERMINAL METERS HISTORY

Version    Date             User       Description
----------------------------------------------------------------------------------------------------------------
1.0.0      29-MAR-2016      RGR        New procedure
1.1.0      25-MAY-2016      ESE        Add events reset, rollover, account denom change
1.2.0      22-JUN-2016      FGB        Use group movements (Group Rollover, Group Denom change, Service RAM Clear)

           18-ABR-2018      JML        Bug 32378:WIGOS-10107 AFIP - It don't send the record of the reset event correctly.
           18-ABR-2018      JML        Bug 32379:WIGOS-10108 AFIP - It don't send the record of the rollover event correctly.
           18-ABR-2018      JML        Bug 32380:WIGOS-10109 AFIP - It don't send the record of the denom. change event correctly.
           07-MAY-2018      JML        Fixed Bug 32575:WIGOS-10720 AFIPClient: The last known meters before connection & disconnection are not reported correctly to Jaza Local.
           04-JUN-2018      JML        Fixed Bug 32872:WIGOS-12544 AFIP Client: There is a machine not reported to Jaza Local when this machine was connected in the first hour only.

Requirements:
   -- Functions:

Parameters:
   -- pStarDateTime:          Start date for data collection.
   -- pEndDateTime:           End date for data collection.
   -- pPendingType:           Pending Type (1: Terminals, 4: Request)
   -- pTerminalsAFIP:         List of Terminals to filter
   -- pTerminalStatusActive:  Status Active
   -- pTerminalType:          Type of terminal
*/
CREATE PROCEDURE [dbo].[SP_AFIP_GetTerminalMetersHistory]
(
    @pStartDateTime           DATETIME
  , @pEndDateTime             DATETIME
  , @pPendingType             INT
  , @pTerminalsAFIP           NVARCHAR(4000)
  , @pTerminalStatusActive    INT
  , @pTerminalType            NVARCHAR(4000)
)
AS
BEGIN
  --Pending Types CONSTANTS
  DECLARE @PENDING_TYPE_REQUEST                   INT;

  --Meter Types CONSTANTS
  DECLARE @TYPE_DAILY                             INT;

  DECLARE @TYPE_ROLLOVER                          INT;
  DECLARE @TYPE_FIRST_TIME                        INT;
  DECLARE @TYPE_SAS_ACCOUNT_DENOM_CHANGE          INT;
  DECLARE @TYPE_SERVICE_RAM_CLEAR                 INT;

  DECLARE @TYPE_GROUP_SERVICE_RAM_CLEAR           INT;
  DECLARE @TYPE_GROUP_ROLLOVER                    INT;
  DECLARE @TYPE_GROUP_SAS_ACCOUNT_DENOM_CHANGE    INT;
  DECLARE @TYPE_GROUP_LAST_KONOWN_METERS          INT;
  DECLARE @TYPE_GROUP_LAST_KNOWN_METERS_BEFORE_DISCONNECTION INT;
  DECLARE @TYPE_GROUP_LAST_KNOWN_METERS_BEFORE_CONNECTION INT;


  --Meter Codes CONSTANTS
  DECLARE @METER_COIN_IN                          INT;
  DECLARE @METER_COIN_OUT                         INT;
  DECLARE @METER_JACKPOTS                         INT;
  DECLARE @METER_GAMES_PLAYED                     INT;

  --Internal variables
  DECLARE @LAST_TERMINAL_METER_DATE               DATETIME;
  DECLARE @INTERVAL_FROM                          DATETIME;
  DECLARE @INTERVAL_TO                            DATETIME;

  DECLARE @TERMINAL_ID                            INT;
  DECLARE @REGISTRATION_CODE                      NVARCHAR(50);
  
  DECLARE @SITE_ID                                INT;

  --Pending Types
  SET @PENDING_TYPE_REQUEST = 4;                  --Request

  --Meter Types
  SET @TYPE_DAILY = 1                             --Daily

  SET @TYPE_ROLLOVER = 11                         --Rollover
  SET @TYPE_FIRST_TIME = 12                       --First time
  SET @TYPE_SAS_ACCOUNT_DENOM_CHANGE = 14         --Denom change
  SET @TYPE_SERVICE_RAM_CLEAR = 15                --Service RAM Clear

  SET @TYPE_GROUP_ROLLOVER = 110                  --Group Rollover
  SET @TYPE_GROUP_SAS_ACCOUNT_DENOM_CHANGE = 140  --Group Denom change
  SET @TYPE_GROUP_SERVICE_RAM_CLEAR = 150         --Group Service RAM Clear
  SET @TYPE_GROUP_LAST_KONOWN_METERS = 170        --Meters of pre-event RAM Clear or SAS Account Denom Change
  SET @TYPE_GROUP_LAST_KNOWN_METERS_BEFORE_DISCONNECTION = 180  -- Grouped meters of E-Box Before Disconnection
  SET @TYPE_GROUP_LAST_KNOWN_METERS_BEFORE_CONNECTION = 190     -- Grouped meters of E-Box Before Connection

  --Meter Codes
  SET @METER_COIN_IN = 0                          --Coin In
  SET @METER_COIN_OUT = 1                         --Coin Out
  SET @METER_JACKPOTS = 2                         --Jackpots
  SET @METER_GAMES_PLAYED = 5                     --Games Played

  --Temporary table for terminals connected
  CREATE TABLE #TEMP_TERMINALS
  (
    TERMINAL_ID         INT
  , REGISTRATION_CODE   NVARCHAR(50)
  );

  --Temporary table for meters
  CREATE TABLE #TEMP_METERS
  (
    TERMINAL_ID           INT
  , REGISTRATION_CODE     NVARCHAR(50)
  , SAS_ACCOUNTING_DENOM  MONEY
  , METER_DATE            DATETIME
  , METER_CODE            INT
  , METER_TYPE            INT
  , INI_VALUE             BIGINT
  , FIN_VALUE             BIGINT
  , GROUP_ID              BIGINT
  , METER_ORIGIN          INT
  , METER_MAX_VALUE       BIGINT
  , INTERVAL_FROM         DATETIME
  , INTERVAL_TO           DATETIME
  );

  IF (@pPendingType = @PENDING_TYPE_REQUEST)
  BEGIN
    --Request type
    --Get Terminal ID
    SET @TERMINAL_ID = ( SELECT  MIN(te.TE_TERMINAL_ID)
                           FROM  TERMINALS te
                          WHERE  (te.TE_REGISTRATION_CODE = @pTerminalsAFIP)
                            -- Only show terminal with the terminal type
                            AND  ((ISNULL(@pTerminalType, '') = '') OR (te.TE_TERMINAL_TYPE IN (SELECT tt.SST_VALUE
                                                                                                FROM SplitStringIntoTable(@pTerminalType, ',', 1) AS tt)))
                        );

    --Search terminal last meter date previous
    INSERT INTO #TEMP_METERS
           EXEC SP_AFIP_GetTerminalLastPreviousMeters
                     @TERMINAL_ID
                   , @pTerminalsAFIP
                   , @pEndDateTime
                   ;
  END --@PENDING_TYPE_REQUEST
  ELSE
  BEGIN

    SET @SITE_ID = (SELECT CAST(GP_KEY_VALUE AS INT) FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Site' AND GP_SUBJECT_KEY = 'Identifier')

    If (@SITE_ID = 571 AND @pPendingType = 1 AND @pStartDateTime >= N'2018-07-21T00:00:00' AND @pStartDateTime < N'2018-07-26T00:00:00')
    BEGIN
        INSERT INTO #TEMP_METERS
                    SELECT * 
                      FROM AFIP_0_20_27 
                     WHERE METER_DATE >= @pStartDateTime AND METER_DATE < @pEndDateTime
    END
    ELSE
    BEGIN

        SET @INTERVAL_FROM = @pStartDateTime;
        SET @INTERVAL_TO   = @pEndDateTime;

        SET @INTERVAL_FROM = DATEADD(HOUR, - DATEPART(HOUR, @INTERVAL_FROM), @INTERVAL_FROM)
        SET @INTERVAL_TO = DATEADD(HOUR, - DATEPART(HOUR, @INTERVAL_TO), @INTERVAL_TO)

        SET @INTERVAL_FROM = DATEADD(MINUTE, - DATEPART(MINUTE, @INTERVAL_FROM), @INTERVAL_FROM)
        SET @INTERVAL_TO = DATEADD(MINUTE, - DATEPART(MINUTE, @INTERVAL_TO), @INTERVAL_TO)

        --Terminals Type
        --Get terminals connected
        INSERT INTO #TEMP_TERMINALS
        (
          TERMINAL_ID, REGISTRATION_CODE
        )
        SELECT  te.TE_TERMINAL_ID
              , te.TE_REGISTRATION_CODE
          FROM  TERMINALS te
            --Was the terminal active?
         WHERE  EXISTS ( SELECT   1
                           FROM   TERMINALS_CONNECTED tc
                          WHERE   tc.TC_DATE >= @INTERVAL_FROM
                            AND   tc.TC_DATE <  @INTERVAL_TO
                            AND   tc.TC_MASTER_ID = te.TE_MASTER_ID
                            AND   tc.TC_STATUS = @pTerminalStatusActive
                       )
           --Are there any terminal to filter with?
           AND  ((ISNULL(@pTerminalsAFIP, '') = '') OR (te.TE_REGISTRATION_CODE IN (SELECT tp.SST_VALUE
                                                                                    FROM SplitStringIntoTable(@pTerminalsAFIP, ',', 1) AS tp)))
           -- Only show terminal with the terminal type
           AND  ((ISNULL(@pTerminalType, '') = '') OR (te.TE_TERMINAL_TYPE IN (SELECT tt.SST_VALUE
                                                                               FROM SplitStringIntoTable(@pTerminalType, ',', 1) AS tt)))

        --Get meters for the terminals connected
        INSERT INTO #TEMP_METERS
        (
          TERMINAL_ID
        , REGISTRATION_CODE
        , SAS_ACCOUNTING_DENOM
        , METER_DATE
        , METER_CODE
        , METER_TYPE
        , INI_VALUE
        , FIN_VALUE
        , GROUP_ID
        , METER_ORIGIN
        , METER_MAX_VALUE
        , INTERVAL_FROM
        , INTERVAL_TO
        )
        SELECT  te.TERMINAL_ID
              , te.REGISTRATION_CODE
              , ISNULL(th.TSMH_SAS_ACCOUNTING_DENOM, 0)
              , th.TSMH_DATETIME
              , th.TSMH_METER_CODE
              , th.TSMH_TYPE
              , ISNULL(th.TSMH_METER_INI_VALUE, 0)
              , ISNULL(th.TSMH_METER_FIN_VALUE, 0)
              , th.TSMH_GROUP_ID
              , th.TSMH_METER_ORIGIN
              , ISNULL(th.TSMH_METER_MAX_VALUE, 0)
              , @pStartDateTime
              , @pEndDateTime
         FROM TERMINAL_SAS_METERS_HISTORY AS th
        INNER JOIN #TEMP_TERMINALS AS te ON th.TSMH_TERMINAL_ID = te.TERMINAL_ID
        WHERE th.TSMH_DATETIME >= @pStartDateTime
          AND th.TSMH_DATETIME <  @pEndDateTime
          AND th.TSMH_TYPE IN ( @TYPE_DAILY
                              , @TYPE_ROLLOVER, @TYPE_SAS_ACCOUNT_DENOM_CHANGE, @TYPE_SERVICE_RAM_CLEAR
                              , @TYPE_GROUP_ROLLOVER, @TYPE_GROUP_SAS_ACCOUNT_DENOM_CHANGE, @TYPE_GROUP_SERVICE_RAM_CLEAR
                              , @TYPE_GROUP_LAST_KONOWN_METERS
                              , @TYPE_FIRST_TIME
                              , @TYPE_GROUP_LAST_KNOWN_METERS_BEFORE_DISCONNECTION
                              , @TYPE_GROUP_LAST_KNOWN_METERS_BEFORE_CONNECTION
                              )
          AND th.TSMH_METER_CODE IN (@METER_COIN_IN, @METER_COIN_OUT, @METER_JACKPOTS, @METER_GAMES_PLAYED);

        -- Get terminals connected with no meters between the dates
        DECLARE curTerminalsWithNoMeters CURSOR FOR
          SELECT te.TERMINAL_ID
               , te.REGISTRATION_CODE
          FROM #TEMP_TERMINALS te
          WHERE NOT EXISTS (SELECT *
                            FROM #TEMP_METERS tm
                            WHERE tm.TERMINAL_ID = te.TERMINAL_ID);

        --Open cursor
        OPEN curTerminalsWithNoMeters

        FETCH NEXT FROM curTerminalsWithNoMeters
          INTO @TERMINAL_ID
             , @REGISTRATION_CODE;

        --Loop the cursor
        WHILE @@FETCH_STATUS = 0
        BEGIN
          -- For each terminal connected with no meters between the dates:
          --  * Search terminal last meter date previous
          INSERT INTO #TEMP_METERS
                 EXEC SP_AFIP_GetTerminalLastPreviousMeters
                           @TERMINAL_ID
                         , @REGISTRATION_CODE
                         , @pEndDateTime
                         ;
          --Get next record
          FETCH NEXT FROM curTerminalsWithNoMeters
          INTO @TERMINAL_ID
             , @REGISTRATION_CODE;
        END --end while CURSOR

        --Free memory
        CLOSE curTerminalsWithNoMeters
        DEALLOCATE curTerminalsWithNoMeters
    END
  END

  --Return meters data
  SELECT *
  FROM #TEMP_METERS
  ORDER BY TERMINAL_ID, METER_DATE, METER_TYPE, METER_CODE;

  --Drop temporary tables
  DROP TABLE #TEMP_TERMINALS;

  DROP TABLE #TEMP_METERS;
END
GO

GRANT EXECUTE ON [dbo].[SP_AFIP_GetTerminalMetersHistory] TO [WGGUI] WITH GRANT OPTION
GO
