--ATENTION: This script creates two STORED PROCS: SP_AFIP_GAMING_TABLES and SP_AFIP_GAMING_TABLES_TYPE--
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_AFIP_GAMING_TABLES]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[SP_AFIP_GAMING_TABLES]
GO


/*
----------------------------------------------------------------------------------------------------------------
REPORT QUERY FOR GAMING TABLE MOVEMENTS

Version     Date          User      Description
----------------------------------------------------------------------------------------------------------------
1.0.0       29-MAR-2016   ESE       New procedure
1.0.1       13-JAN-2017   FGB       The grants were in bad place, it must be after GO (of the create procedures).
1.0.2       02-FEB-2017   FOS       Filter Data exclude Sys-system, sys-tito user and sys-gamingtables users.
1.0.3       06-FEB-2017   FOS       Get correct information about gambling tables.
1.0.4       09-FEB-2017   FOS       Change all stored because data that extract is incorrect

Requeriments:
   -- Functions:

Parameters:
   -- pStarDateTime DATETIME:       Gaming table movement day
*/



CREATE PROCEDURE [dbo].[SP_AFIP_GAMING_TABLES]
(
  @pStarDateTime    DATETIME
 )
AS
BEGIN
DECLARE @Columns                           AS VARCHAR(MAX)	                                                                          
DECLARE @NationalCurrency                  as VARCHAR(5)  
DECLARE @pCageChipColor                    AS VARCHAR(3)-- X02
DECLARE @_MOVEMENT_CLOSE_SESSION           as integer
DECLARE @_Currency_ISO_Code                AS  VARCHAR(5);
DECLARE @_Default_Cashier_Afip_Type        AS  INT;
DECLARE @_old_chips_iso_code               AS  VARCHAR(3);
DECLARE @_opening_cash 					           AS INT;	
DECLARE @_cage_close_session               AS INT;
DECLARE @_cage_filler_in                   AS INT;
DECLARE @_cage_filler_out                  AS INT;
DECLARE @_chips_sale                       AS INT;
DECLARE @_chips_purchase                   AS INT;
DECLARE @_chips_sale_devolution_for_tito   AS INT;
DECLARE @_chips_sale_with_cash_in          AS INT;
DECLARE @_chips_sale_register_total        AS INT;
DECLARE @_chips_redeemable                 AS VARCHAR(5);
DECLARE @_GUI_User_Type_SYS_GamingTables   AS  INT;  
DECLARE @_GUI_User_Type_SYS_TITO_User      AS  INT;  
DECLARE @_GUI_User_Type_SYS_SYSTEM         AS  INT;  
DECLARE @_filler_in_closing_stock          AS  INT;  
DECLARE @_filler_out_closing_stock         AS  INT;
DECLARE @pEndDateTime                      AS  DATETIME;  

SET @_GUI_User_Type_SYS_SYSTEM         = 3;  
SET @_GUI_User_Type_SYS_TITO_User      = 4;  
SET @_GUI_User_Type_SYS_GamingTables   = 5; 
SET @_opening_cash 					           = 0   	
SET @_filler_in_closing_stock          = 190  
SET @_filler_out_closing_stock         = 191  
SET @_cage_close_session               = 201 
SET @_cage_filler_in                   = 202 
SET @_cage_filler_out                  = 203 
SET @_chips_sale                       = 300 
SET @_chips_purchase                   = 301 
SET @_chips_sale_devolution_for_tito   = 302 
SET @_chips_sale_with_cash_in          = 307 
SET @_chips_sale_register_total        = 311 
SET @_chips_redeemable                 = '1001'
SET @pCageChipColor                    = 'X02'
SET @_MOVEMENT_CLOSE_SESSION           = 1 
SET @pEndDateTime                      = DATEADD(day, 1, @pStartDateTime)


SET @NationalCurrency = (SELECT GP_KEY_VALUE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'RegionalOptions' AND GP_SUBJECT_KEY = 'CurrencyISOCode')
           	                                                                                                          
    SELECT   CS_SESSION_ID
           , CS_USER_ID
           , MAX(CM_DATE)							      AS DATE_MOV                                                     
           , CM_TYPE									      AS TYPE_MOV                                                    
           , ISNULL(CCMR.CGM_MOVEMENT_ID,0)	AS ID_MOV 
           , CASE CM_TYPE
           WHEN @_cage_filler_in THEN SUM(CM_ADD_AMOUNT)
           WHEN @_cage_filler_out THEN SUM(CM_SUB_AMOUNT)
           WHEN @_cage_close_session THEN SUM(CM_FINAL_BALANCE)
           WHEN @_filler_in_closing_stock THEN SUM(CM_ADD_AMOUNT)
           WHEN @_filler_out_closing_stock  THEN SUM(CM_SUB_AMOUNT)
           WHEN @_chips_sale THEN SUM(CM_SUB_AMOUNT)
           WHEN @_chips_sale_devolution_for_tito THEN SUM(CM_SUB_AMOUNT)
           WHEN @_chips_sale_with_cash_in  THEN SUM(CM_SUB_AMOUNT)
           WHEN @_chips_sale_register_total THEN SUM(CM_SUB_AMOUNT)
           WHEN @_chips_purchase THEN SUM(CM_ADD_AMOUNT)
           ELSE 0 END
            AS  AMOUNT                                                                   
           ,  CASE ISNULL(CM_CURRENCY_ISO_CODE,'') WHEN '' THEN @NationalCurrency                          
              WHEN @pCageChipColor THEN ''                                                      
              ELSE CM.CM_CURRENCY_ISO_CODE END AS CURRENCY_ISO_CODE                                        
           ,  CM.CM_CAGE_CURRENCY_TYPE AS CURRENCY_TYPE                                                    
           ,  0 OPENER_MOV                                                                                 
           , CM.CM_OPERATION_ID
     , CS_OPENING_DATE
     , CS_CLOSING_DATE
      INTO   #TABLE_MOVEMENTS	                                                                             
      FROM   CASHIER_MOVEMENTS   AS CM
INNER JOIN   CASHIER_SESSIONS         ON CM_SESSION_ID = CS_SESSION_ID
 LEFT JOIN   CAGE_CASHIER_MOVEMENT_RELATION AS CCMR ON CCMR.CM_MOVEMENT_ID = CM.CM_MOVEMENT_ID
INNER JOIN   GUI_USERS          ON CS_USER_ID    = GU_USER_ID          
     WHERE   (CS_CLOSING_DATE >= @pStartDateTime OR CS_CLOSING_DATE IS NULL) AND CS_OPENING_DATE <= @pEndDateTime
       AND   CM_TYPE IN (@_opening_cash,                                                                          
 						             @_cage_filler_in,                                                              
 						             @_cage_filler_out,	                                                          
 						             @_cage_close_session,
 						             @_filler_out_closing_stock ,	                                                              
 						             @_chips_sale, 
									       @_chips_purchase, 
									       @_chips_sale_devolution_for_tito, 
									       @_chips_sale_with_cash_in, 
									       @_chips_sale_register_total)      	                                                                          
  GROUP BY   CS_SESSION_ID
           , CS_USER_ID
                     , CM_TYPE                         	                                                              
                     , CCMR.CGM_MOVEMENT_ID                    	                                              
                     , CM.CM_CURRENCY_ISO_CODE	                                                                      
                     , CM.CM_CAGE_CURRENCY_TYPE	                                                                      
                     , CM.CM_OPERATION_ID
           , CS_OPENING_DATE
           , CS_CLOSING_DATE
            ORDER BY   MAX(CM_DATE) DESC                                                                                                                                           
            

DECLARE @cursor_id_mov AS INT

DECLARE _cursor CURSOR  
FOR SELECT MIN(ID_MOV) FROM #TABLE_MOVEMENTS WHERE TYPE_MOV IN (@_cage_filler_in , @_filler_in_closing_stock)  GROUP BY CS_SESSION_ID
      OPEN _cursor  
      FETCH NEXT FROM _cursor INTO @cursor_id_mov
      WHILE @@FETCH_STATUS = 0  
        BEGIN    
            UPDATE #TABLE_MOVEMENTS
            SET OPENER_MOV = 1
            WHERE ID_MOV = @cursor_id_mov AND TYPE_MOV IN (@_cage_filler_in , @_filler_in_closing_stock)

FETCH NEXT FROM _cursor INTO @cursor_id_mov 
END
CLOSE _cursor;  
DEALLOCATE _cursor;  

      SELECT GT_GAMING_TABLE_ID  AS TABLE_ID
           , ISNULL(GT_NAME, GU_FULL_NAME) AS TABLE_NAME
           , ISNULL(GTT_GAMING_TABLE_TYPE_ID, 99) AS GTABLE_TYPE_ID
           , ISNULL(GTT_NAME, 'OTROS') AS GTABLE_TYPE_DESC
           , SUM(case when OPENER_MOV=1 and currency_type =0 and currency_iso_code = @NationalCurrency then amount else 0 end ) as OPENING_CASH
           , SUM(case when TYPE_MOV in (@_filler_out_closing_stock,@_cage_close_session) and currency_type =0 and currency_iso_code = @NationalCurrency then amount else 0 end) as CLOSING_CASH
           , SUM(case when OPENER_MOV=1 and currency_type =@_chips_redeemable and currency_iso_code = @NationalCurrency then amount else 0 end) as OPENING_CHIPS
           , SUM(case when TYPE_MOV in (@_filler_out_closing_stock,@_cage_close_session) and currency_type =@_chips_redeemable and currency_iso_code = @NationalCurrency then amount else 0 end) as CLOSING_CHIPS
           , SUM(case when TYPE_MOV in (@_cage_filler_out) and currency_type =0 and currency_iso_code = @NationalCurrency then amount else 0 end) as CASH_OUT_TOTAL
           , SUM(case when OPENER_MOV=0 AND TYPE_MOV in (@_cage_filler_in) and currency_type =0 and currency_iso_code = @NationalCurrency then amount else 0 end) as CASH_IN_TOTAL
           , SUM(case when TYPE_MOV in (@_cage_filler_out) and currency_type =@_chips_redeemable and currency_iso_code = @NationalCurrency then amount else 0 end) as CHIPS_OUT_TOTAL
           , SUM(case when OPENER_MOV=0 AND TYPE_MOV in (@_cage_filler_in) and currency_type =@_chips_redeemable and currency_iso_code = @NationalCurrency then amount else 0 end) as CHIPS_IN_TOTAL
           , SUM(case when TYPE_MOV in (@_chips_sale, @_chips_sale_devolution_for_tito, @_chips_sale_with_cash_in, @_chips_sale_register_total) and currency_type = @_chips_redeemable and currency_iso_code = @NationalCurrency then amount else 0 end) as SALE_CHIPS_TOTAL
           , SUM(case when TYPE_MOV in (@_chips_purchase) and currency_type = @_chips_redeemable and currency_iso_code = @NationalCurrency then amount else 0 end) as BUY_CHIPS_TOTAL
      FROM   #TABLE_MOVEMENTS   AS CM
 LEFT JOIN   GAMING_TABLES_SESSIONS ON CS_SESSION_ID = GTS_CASHIER_SESSION_ID
 LEFT JOIN   gaming_tables on gts_gaming_table_id = gt_gaming_table_id
 LEFT JOIN   GAMING_TABLES_TYPES ON GTT_GAMING_TABLE_TYPE_ID = GT_TYPE_ID
INNER JOIN   GUI_USERS           ON CS_USER_ID    = GU_USER_ID   
     WHERE    @pStartDateTime <= DATE_MOV  AND DATE_MOV < @pEndDateTime
       AND   GU_USER_TYPE NOT IN (@_GUI_User_Type_SYS_SYSTEM,@_GUI_User_Type_SYS_TITO_User) 
  group by   GT_GAMING_TABLE_ID
           , ISNULL(GT_NAME, GU_FULL_NAME)
           , ISNULL(gtt_gaming_table_type_id, 99) 
           , ISNULL(gtt_name, 'OTROS') 
  ORDER BY   MAX(DATE_MOV) DESC 

DROP TABLE #TABLE_MOVEMENTS	

END  -- End procedure SP_AFIP_GAMING_TABLES
GO

GRANT EXECUTE ON [dbo].[SP_AFIP_GAMING_TABLES] TO [WGGUI] WITH GRANT OPTION
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_AFIP_GAMING_TABLES_TYPE]') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[SP_AFIP_GAMING_TABLES_TYPE]
GO

/*
----------------------------------------------------------------------------------------------------------------
REPORT QUERY FOR GAMING TABLE MOVEMENTS TYPE

Version   Date          User      Description
----------------------------------------------------------------------------------------------------------------
1.0.0     29-MAR-2016   ESE       New procedure
1.0.1     13-JAN-2017   FGB       The grants were in bad place, it must be after GO (of the create procedures).
1.0.2     09-FEB-2017   FOS       Delete innecesary columns.


Requeriments:
   -- Functions:

Parameters:
   -- pStarDateTime DATETIME:          Gaming table movement day
*/
CREATE PROCEDURE [dbo].[SP_AFIP_GAMING_TABLES_TYPE]
(
  @pStarDateTime    DATETIME
)
AS
BEGIN
  CREATE TABLE #GamingTable
  (
       TABLE_ID          INT
     , TABLE_NAME        VARCHAR(250)
     , ID_TABLE_TYPE     INT
     , DESC_TABLE_TYPE   VARCHAR(250)
     , OPENING_CASH      MONEY
     , CLOSING_CASH      MONEY
     , OPENING_CHIPS     MONEY
     , CLOSING_CHIPS     MONEY
     , CASH_OUT_TOTAL    MONEY
     , CASH_IN_TOTAL     MONEY
     , CHIPS_OUT_TOTAL   MONEY
     , CHIPS_IN_TOTAL    MONEY
     , CHIP_SALES_TOTAL  MONEY
     , CHIPS_BUY_TOTAL   MONEY
  )

  INSERT  #GamingTable EXEC SP_AFIP_GAMING_TABLES @pStarDateTime

  SELECT ID_TABLE_TYPE
       , DESC_TABLE_TYPE
       , COUNT(ID_TABLE_TYPE) AS TABLE_COUNT
       , SUM(OPENING_CASH) AS OPENING_CASH
       , SUM(CLOSING_CASH) AS CLOSING_CASH
       , SUM(OPENING_CHIPS) AS OPENING_CHIPS
       , SUM(CLOSING_CHIPS) AS CLOSING_CHIPS
       , SUM(CASH_OUT_TOTAL) AS CASH_OUT_TOTAL
       , SUM(CASH_IN_TOTAL) AS CASH_IN_TOTAL
       , SUM(CHIPS_OUT_TOTAL) AS CHIPS_OUT_TOTAL
       , SUM(CHIPS_IN_TOTAL) AS CHIPS_IN_TOTAL
       , SUM(CHIP_SALES_TOTAL) AS CHIP_SALES_TOTAL
       , SUM(CHIPS_BUY_TOTAL) AS CHIPS_BUY_TOTAL
  FROM #GamingTable
  GROUP BY ID_TABLE_TYPE, DESC_TABLE_TYPE

  DROP TABLE #GamingTable
END  -- END procedure SP_AFIP_GAMING_TABLES_TYPE
GO

GRANT EXECUTE ON [dbo].[SP_AFIP_GAMING_TABLES_TYPE] TO [WGGUI] WITH GRANT OPTION
GO