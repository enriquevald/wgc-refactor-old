﻿-------------------------------------------------------------------------------------
-- Copyright © 2014 Win Systems International
-------------------------------------------------------------------------------------
-- 
--   MODULE NAME: SP_AFIP_GetRequestedTerminalStatus.sql
-- 
--   DESCRIPTION: 
-- 
--        AUTHOR: José Martínez
-- 
-- CREATION DATE: 15-MAR-2018
-- 
-- REVISION HISTORY:
-- 
-- Date        Author      Description
-------------- ----------- ----------------------------------------------------------
-- 15-MAR-2018 JML         Bug 31907:WIGOS-8806 [Ticket #13051] [Ticket#2018030710003365] AFIP - Trámites con Mesa de Ayuda - Sistema Jaza - Apoyo ate
-- 21-MAR-2018 JML         
-------------- ----------- ----------------------------------------------------------

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[afip_log]') AND type in (N'U'))
  CREATE TABLE [dbo].[afip_log](
    [afl_id]                            [BIGINT] IDENTITY(1,1) NOT NULL
  , [afl_RequestDateTime]               [DATETIME]             NULL
  , [afl_TerminalAFIP]                  [NVARCHAR](100)        NULL
  , [afl_StatusRequestedTerminal]       [INT]                  NULL
  , [afl_activation_date]               [DATETIME]             NULL
  , [afl_retirement_date]               [DATETIME]             NULL
  , [afl_terminal_status]               [INT]                  NULL
  , [afl_terminal_id]                   [INT]                  NULL
  , [afl_exist_terminal_with_status_ok] [INT]                  NULL
  , [afl_exist_active_session_in_time]  [INT]                  NULL
  , [afl_session_timeout]               [INT]                  NULL
  , [afl_step]                          [NVARCHAR](100)        NOT NULL
  CONSTRAINT [PK_AFIP_LOG] PRIMARY KEY CLUSTERED 
  (
    [afl_id] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
  ) ON [PRIMARY] 
GO

IF EXISTS (SELECT * FROM sys.columns WHERE object_id = object_id(N'[dbo].[afip_log]') and name = '[afl_TerminalStatusRetired]')
ALTER TABLE [dbo].[afip_log]
	DROP COLUMN [afl_TerminalStatusRetired]

-----------------------------------------------------------------------------------------------------------------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_AFIP_TerminalStatusLog]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_AFIP_TerminalStatusLog]
GO
CREATE PROCEDURE [dbo].[SP_AFIP_TerminalStatusLog]
    @pRequestDateTime           DATETIME
  , @pTerminalAFIP              NVARCHAR(100)
  , @pStatusRequestedTerminal   INT
  , @pActivationDate            DATETIME
  , @pRetirementDate            DATETIME
  , @pTerminalStatus            INT
  , @pTerminalId                INT
  , @pExistTerminalWithStatusOk INT
  , @pExistActiveSessionInTime  INT
  , @pSessionTimeout            INT
  , @pStep                      NVARCHAR(100)
  
AS
BEGIN 

  DECLARE @active int

  -- Check general params
  SELECT @active = CAST(GP_KEY_VALUE AS int)
    FROM GENERAL_PARAMS 
   WHERE GP_GROUP_KEY = 'AFIPClient' AND GP_SUBJECT_KEY ='EnableMessageTrace'

  IF ( @active = 1 )
  BEGIN

    INSERT INTO AFIP_LOG ( [afl_RequestDateTime]              
                         , [afl_TerminalAFIP]                 
                         , [afl_StatusRequestedTerminal]      
                         , [afl_activation_date]              
                         , [afl_retirement_date]              
                         , [afl_terminal_status]              
                         , [afl_terminal_id]                  
                         , [afl_exist_terminal_with_status_ok]
                         , [afl_exist_active_session_in_time] 
                         , [afl_session_timeout]              
                         , [afl_step]                         
                         )
                  VALUES ( @pRequestDateTime          
                         , @pTerminalAFIP             
                         , @pStatusRequestedTerminal  
                         , @pActivationDate           
                         , @pRetirementDate           
                         , @pTerminalStatus           
                         , @pTerminalId               
                         , @pExistTerminalWithStatusOk
                         , @pExistActiveSessionInTime 
                         , @pSessionTimeout 
                         , @pStep                         
                         )      
                                                  
  END      
                      
END -- [SP_AFIP_TerminalStatusLog]
GO

GRANT EXECUTE ON [dbo].[SP_AFIP_TerminalStatusLog] TO [WGGUI] WITH GRANT OPTION
GO

-----------------------------------------------------------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_AFIP_GetRequestedTerminalStatus]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_AFIP_GetRequestedTerminalStatus]
GO

/*
---------------------------------------------------------------------------------
Given a date and a terminal, it returns the terminal status

Version    Date             User       Description
---------------------------------------------------------------------------------
1.0.0      14-DEC-2017      FGB        Initial version

Parameters:
   -- pRequestDateTime:             Date to check
   -- pTerminalStatusRetired:       Terminal status retired
   -- pTerminalAFIP:                Terminal request (only a terminal)
   -- pTerminalType:                Terminal type
   -- pStatusRequestedTerminal:     Status of the requested terminal (OUTPUT)
*/
CREATE PROCEDURE [dbo].[SP_AFIP_GetRequestedTerminalStatus]
(
    @pRequestDateTime               DATETIME
  , @pTerminalStatusRetired         INT
  , @pTerminalAFIP                  NVARCHAR(100)
  , @pTerminalType                  NVARCHAR(4000)
  , @pStatusRequestedTerminal       INT       OUTPUT
)
AS
BEGIN
  --Internal variables
  DECLARE @_activation_date                 DATETIME
  DECLARE @_retirement_date                 DATETIME
  DECLARE @_terminal_status                 INT

  DECLARE @_terminal_id                     INT

  --Terminal Status SAS error
  DECLARE @_SAS_Host_without_error          INT
  DECLARE @_exist_terminal_with_status_ok   INT

  --WCP Session
  DECLARE @_exist_active_session_in_time    INT
  DECLARE @_session_status_open             INT
  DECLARE @_session_status_close            INT
  DECLARE @_session_timeout                 INT
  
  
  --Status constants to return
  DECLARE @_STATUS_ACTIVE                   INT
  DECLARE @_STATUS_RETIRED                  INT
  DECLARE @_STATUS_NOT_AVAILABLE            INT

  --Status constants to return
  SET @_STATUS_ACTIVE          = 1
  SET @_STATUS_RETIRED         = 2
  SET @_STATUS_NOT_AVAILABLE   = 3

  --Terminal Status has no SAS error
  SET @_SAS_Host_without_error       = 0

  --WCP Session is connected
  SET @_session_status_open      = 0
  SET @_session_status_close     = 4

  -- Default status
  SET @pStatusRequestedTerminal = @_STATUS_RETIRED

  SET @pRequestDateTime = GETDATE()

  BEGIN TRY

    -- Request date is invalid
    IF (@pRequestDateTime IS NULL)
    BEGIN
      SET @pStatusRequestedTerminal = @_STATUS_NOT_AVAILABLE
      EXEC SP_AFIP_TerminalStatusLog 
           @pRequestDateTime, @pTerminalAFIP, @pStatusRequestedTerminal, @_activation_date, @_retirement_date
         , @_terminal_status, @_terminal_id, @_exist_terminal_with_status_ok, @_exist_active_session_in_time, @_session_timeout
         , N'Step 1'          
      RETURN
    END

    -- Terminal registration code is invalid
    IF (@pTerminalAFIP IS NULL)
    BEGIN
      SET @pStatusRequestedTerminal = @_STATUS_NOT_AVAILABLE
      EXEC SP_AFIP_TerminalStatusLog 
           @pRequestDateTime, @pTerminalAFIP, @pStatusRequestedTerminal, @_activation_date, @_retirement_date
         , @_terminal_status, @_terminal_id, @_exist_terminal_with_status_ok, @_exist_active_session_in_time, @_session_timeout
         , N'Step 2'          
      RETURN
    END

    --Get Terminal ID
    SELECT @_terminal_id = MAX(te.TE_TERMINAL_ID)
      FROM   TERMINALS te
     WHERE   (te.TE_REGISTRATION_CODE = @pTerminalAFIP)
    
    -- Terminal registration code is invalid
    IF (@_terminal_id IS NULL)
    BEGIN
      SET @pStatusRequestedTerminal = @_STATUS_NOT_AVAILABLE
      EXEC SP_AFIP_TerminalStatusLog 
           @pRequestDateTime, @pTerminalAFIP, @pStatusRequestedTerminal, @_activation_date, @_retirement_date
         , @_terminal_status, @_terminal_id, @_exist_terminal_with_status_ok, @_exist_active_session_in_time, @_session_timeout
         , N'Step 3'          
      RETURN
    END

    --Get terminal data
    SELECT   @_activation_date  =  TE_ACTIVATION_DATE
          ,  @_retirement_date  =  TE_RETIREMENT_DATE
          ,  @_terminal_status  =  TE_STATUS
      FROM   TERMINALS
     WHERE   TE_TERMINAL_ID = @_terminal_id

    --Terminal not found or is not yet active
    IF ((@_activation_date IS NULL) OR (@pRequestDateTime < @_activation_date))
    BEGIN
      SET @pStatusRequestedTerminal = @_STATUS_NOT_AVAILABLE
      EXEC SP_AFIP_TerminalStatusLog 
           @pRequestDateTime, @pTerminalAFIP, @pStatusRequestedTerminal, @_activation_date, @_retirement_date
         , @_terminal_status, @_terminal_id, @_exist_terminal_with_status_ok, @_exist_active_session_in_time, @_session_timeout
         , N'Step 4'          
      RETURN
    END

    --Terminal has been retired
    --  If a terminal is RETIRED after it can NOT BE activated again
    --    , it has to be created as a new terminal.
    IF (@_terminal_status = @pTerminalStatusRetired AND @pRequestDateTime >= @_retirement_date) 
    BEGIN
      SET @pStatusRequestedTerminal = @_STATUS_RETIRED
      EXEC SP_AFIP_TerminalStatusLog 
           @pRequestDateTime, @pTerminalAFIP, @pStatusRequestedTerminal, @_activation_date, @_retirement_date
         , @_terminal_status, @_terminal_id, @_exist_terminal_with_status_ok, @_exist_active_session_in_time, @_session_timeout
         , N'Step 5'          
      RETURN
    END

    -- Terminal checked. Is active
    SET @pStatusRequestedTerminal = @_STATUS_ACTIVE 

    --GENERAL PARAMS: Get timeout (in seconds) for WCP session open status
    SET @_session_timeout = (SELECT   CAST(GP_KEY_VALUE AS INT)
                               FROM   GENERAL_PARAMS
                              WHERE   GP_GROUP_KEY    =  'AFIPClient'
                                AND   GP_SUBJECT_KEY  =  'RequestStatusSessionTimeout')
    SET @_session_timeout = @_session_timeout * -1
    
    --Exists terminal active connection in wcp_session?
    SET @_exist_active_session_in_time = (SELECT   COUNT(1)
                                            FROM   WCP_SESSIONS
                                           WHERE   WS_TERMINAL_ID   =  @_terminal_id
                                             AND ( WS_STATUS        =  @_session_status_open
                                              OR   WS_STATUS        =  @_session_status_close 
                                                 )
                                             AND   WS_LAST_RCVD_MSG >= DATEADD(SECOND, @_session_timeout, @pRequestDateTime))
    IF @_exist_active_session_in_time < 1
    BEGIN
      SET @pStatusRequestedTerminal = @_STATUS_NOT_AVAILABLE
      EXEC SP_AFIP_TerminalStatusLog 
           @pRequestDateTime, @pTerminalAFIP, @pStatusRequestedTerminal, @_activation_date, @_retirement_date
         , @_terminal_status, @_terminal_id, @_exist_terminal_with_status_ok, @_exist_active_session_in_time, @_session_timeout
         , N'Step 6'          
      RETURN
    END

    --Check terminal status?
    SET @_exist_terminal_with_status_ok = (SELECT   COUNT(1)
                                             FROM   TERMINAL_STATUS
                                            WHERE   TS_TERMINAL_ID = @_terminal_id
                                              AND   ISNULL(TS_SAS_HOST_ERROR, @_SAS_Host_without_error) = @_SAS_Host_without_error)
    IF @_exist_terminal_with_status_ok < 1
    BEGIN
      SET @pStatusRequestedTerminal = @_STATUS_NOT_AVAILABLE
      EXEC SP_AFIP_TerminalStatusLog 
           @pRequestDateTime, @pTerminalAFIP, @pStatusRequestedTerminal, @_activation_date, @_retirement_date
         , @_terminal_status, @_terminal_id, @_exist_terminal_with_status_ok, @_exist_active_session_in_time, @_session_timeout
         , N'Step 7'          
      RETURN
    END

  END TRY
  BEGIN CATCH
    SET @pStatusRequestedTerminal = @_STATUS_NOT_AVAILABLE
    EXEC SP_AFIP_TerminalStatusLog 
         @pRequestDateTime, @pTerminalAFIP, @pStatusRequestedTerminal, @_activation_date, @_retirement_date
       , @_terminal_status, @_terminal_id, @_exist_terminal_with_status_ok, @_exist_active_session_in_time, @_session_timeout
       , N'Step 8'          
  END CATCH

  EXEC SP_AFIP_TerminalStatusLog 
       @pRequestDateTime, @pTerminalAFIP, @pStatusRequestedTerminal, @_activation_date, @_retirement_date
     , @_terminal_status, @_terminal_id, @_exist_terminal_with_status_ok, @_exist_active_session_in_time, @_session_timeout
     , N'Step 10'          

  
END
GO

GRANT EXECUTE ON [dbo].[SP_AFIP_GetRequestedTerminalStatus] TO [WGGUI] WITH GRANT OPTION
GO

