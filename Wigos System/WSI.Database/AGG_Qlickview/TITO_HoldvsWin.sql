/*------------------------------------------------------------------------------
-- Copyright © 2018 Win Systems Ltd.
--------------------------------------------------------------------------------
-- 
--   SP NAME: TITO_HoldvsWin
-- 
--   DESCRIPTION: --
-- 
--        AUTHOR: --
-- 
-- CREATION DATE: xx-xx-xxxx
-- 
-- REVISION HISTORY:
-- 
-- Date            Author Description
-- ----------- ------ ----------------------------------------------------------
-- xx-xxx-xxxx xxx    First release.
-- 05-MAR-2018 FJC    Fixed Defect: WIGOS-6828 [Ticket #10502] "Tickets TITO VS Hold" report includes raw meter increments, with no corrections
-- ------------------------------------------------------------------------------*/


/****** Object:  StoredProcedure [dbo].[TITO_HoldvsWin]    Script Date: 04/12/2016 10:27:11 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TITO_HoldvsWin]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[TITO_HoldvsWin]
GO


/****** Object:  StoredProcedure [dbo].[TITO_HoldvsWin]    Script Date: 04/12/2016 10:27:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[TITO_HoldvsWin]
      (@pIniDate DATETIME,
       @pGroupByOption INT = 1,
       @pOrderGroup0 INT = 0)
AS
BEGIN

DECLARE @_d0 AS DATETIME
DECLARE @_d1 AS DATETIME


SET @_d0 = @pIniDate
SET @_d1 = DATEADD (DAY, 1, @_d0)


SET @_d1 = dbo.Opening(0, @_d0)
SET @_d0 = @_d1 -1 

SELECT   AssetNumber       
       , Manufacturer
       , M0                                                         CoinIn
       , (M1+M2)                                                    TotalWon
       , M0-(M1+M2)                                                 Hold
       , M11_BILLS                                                  BillIn
       , NBILL                                                      BillInPerDenom
       , M128                                                       CashableTicketIn
       , M130                                                       RestrictedTicketIn
       , M132                                                       NonRestrictedTicketIn
       , M134                                                       CashableTicketOut
       , M136                                                       RestrictedTicketOut
       , M2                                                         Jackpots
       , M3                                                         HandPaidCanceledCredits
       , (M11_BILLS + M128 + M130 + M132)                           TotalIn
       , M36                                                        TotalDrop
       , (M134 + M136 + M2 + M3)                                    TotalOut
       , (M11_BILLS + M128 + M130 + M132) - (M134 + M136 + M2 + M3) TotalWin
       , (M11_BILLS + M128 + 0 + M132)                              TotalReIn
       , (M134 + 0 + M2 + M3)                                       TotalReOut
       , (M11_BILLS + M128 + 0 + M132) - (M134 + 0 + M2 + M3)       TotalReWin
       , M5                                                         Games
       , M6                                                         Won
       , M160                                                       CashableCardsIn
       , M162                                                       RestrictedCardsIn
       , M164                                                       NonRestrictedCardsIn
       , M184                                                       CashableCardsOut
       , M186                                                       RestrictedCardsOut
       , M188                                                       NonRestrictedCardsOut
INTO #TerminalsMeter
FROM
(  
      SELECT   MIN(TE_TERMINAL_ID)	TID
             , MIN(TE_NAME)					AssetNumber
             , MIN(TE_PROVIDER_ID)	Manufacturer
             , SUM (CASE WHEN TSMH_TYPE = 20 AND TSMH_METER_CODE = 0		THEN ISNULL(TSMH_METER_INCREMENT, 0) ELSE 0 END ) * 0.01 M0
             , SUM (CASE WHEN TSMH_TYPE = 20 AND TSMH_METER_CODE = 1		THEN ISNULL(TSMH_METER_INCREMENT, 0) ELSE 0 END ) * 0.01 M1
             , SUM (CASE WHEN TSMH_TYPE = 20 AND TSMH_METER_CODE = 2		THEN ISNULL(TSMH_METER_INCREMENT, 0) ELSE 0 END ) * 0.01 M2
						 , SUM (CASE WHEN TSMH_METER_CODE = 3 THEN 
												CASE WHEN TSMH_TYPE = 20 THEN ISNULL(TSMH_METER_INCREMENT, 0)
						 								 WHEN TSMH_TYPE = 10 
															AND ISNULL(TSMH_METER_INCREMENT, 0) > 0 THEN ISNULL(TSMH_METER_INCREMENT, 0) * -1
												ELSE 0 END
										ELSE 0 END)	* 0.01 M3

             , SUM (CASE WHEN TSMH_TYPE = 20 AND TSMH_METER_CODE = 4		THEN ISNULL(TSMH_METER_INCREMENT, 0) ELSE 0 END ) * 0.01 M4

						 , SUM (CASE WHEN TSMH_METER_CODE = 11 THEN 
												CASE WHEN TSMH_TYPE = 20 THEN ISNULL(TSMH_METER_INCREMENT, 0)
						 								 WHEN TSMH_TYPE = 10 
															AND ISNULL(TSMH_METER_INCREMENT, 0) > 0 THEN ISNULL(TSMH_METER_INCREMENT, 0) * -1
												ELSE 0 END
										ELSE 0 END)	* 0.01 M11_BILLS

             , SUM (CASE WHEN TSMH_TYPE = 20	THEN ISNULL(TSMH_METER_INCREMENT, 0) *
																								CASE TSMH_METER_CODE 
																								WHEN 64 THEN   1.00
                                                                           WHEN 65 THEN   2.00
                                                                           WHEN 66 THEN   5.00
                                                                           WHEN 67 THEN  10.00
                                                                           WHEN 68 THEN  20.00
                                                                           WHEN 69 THEN  25.00
                                                                           WHEN 70 THEN  50.00
                                                                           WHEN 71 THEN 100.00
                                                                           WHEN 72 THEN 200.00
                                                                           WHEN 73 THEN 250.00
                                                                           WHEN 74 THEN 500.00
                                                                           WHEN 75 THEN 1000.00
                                                                           WHEN 76 THEN 2000.00
                                                                           WHEN 77 THEN 2500.00
                                                                           WHEN 78 THEN 5000.00
                                                                           WHEN 79 THEN 10000.00
                                                                           WHEN 80 THEN 20000.00
                                                                           WHEN 81 THEN 25000.00
                                                                           WHEN 82 THEN 50000.00
                                                                           WHEN 83 THEN 100000.00
                                                                           WHEN 84 THEN 200000.00
                                                                           WHEN 85 THEN 250000.00
                                                                           WHEN 86 THEN 500000.00
                                                                           WHEN 87 THEN 1000000.00
																								ELSE 0  END 
										ELSE 0 END) NBILL
      
             , SUM (CASE WHEN TSMH_TYPE = 20 AND TSMH_METER_CODE = 128 THEN ISNULL(TSMH_METER_INCREMENT, 0) ELSE 0 END ) * 0.01 M128
             , SUM (CASE WHEN TSMH_TYPE = 20 AND TSMH_METER_CODE = 130 THEN ISNULL(TSMH_METER_INCREMENT, 0) ELSE 0 END ) * 0.01 M130
             , SUM (CASE WHEN TSMH_TYPE = 20 AND TSMH_METER_CODE = 132 THEN ISNULL(TSMH_METER_INCREMENT, 0) ELSE 0 END ) * 0.01 M132
      
             , SUM (CASE WHEN TSMH_TYPE = 20 AND TSMH_METER_CODE = 134 THEN ISNULL(TSMH_METER_INCREMENT, 0) ELSE 0 END ) * 0.01 M134
             , SUM (CASE WHEN TSMH_TYPE = 20 AND TSMH_METER_CODE = 136 THEN ISNULL(TSMH_METER_INCREMENT, 0) ELSE 0 END ) * 0.01 M136
      
             , SUM (CASE WHEN TSMH_TYPE = 20 AND TSMH_METER_CODE = 36 THEN ISNULL(TSMH_METER_INCREMENT, 0) ELSE 0 END )	* 0.01 M36
      
             , SUM(CASE WHEN (TSMH_TYPE = 20 AND TSMH_METER_CODE IN (11,128,130,132))	THEN ISNULL(TSMH_METER_INCREMENT, 0) ELSE 0 END ) CommputedTotalIn
             , SUM(CASE WHEN (TSMH_TYPE = 20 AND TSMH_METER_CODE IN (2,3,134,136))		THEN ISNULL(TSMH_METER_INCREMENT, 0) ELSE 0 END ) CommputedTotalOut
      
             , (SUM (CASE WHEN TSMH_TYPE = 20 AND TSMH_METER_CODE	= 0 THEN ISNULL(TSMH_METER_INCREMENT, 0) ELSE 0 END ) 
							- SUM (CASE WHEN TSMH_TYPE = 20 AND TSMH_METER_CODE = 1 THEN ISNULL(TSMH_METER_INCREMENT, 0) ELSE 0 END ) 
							- SUM (CASE WHEN TSMH_TYPE = 20 AND TSMH_METER_CODE = 2 THEN ISNULL(TSMH_METER_INCREMENT, 0) ELSE 0 END )) * 0.01 MNET0
      
             , (SUM (CASE WHEN TSMH_TYPE = 20 AND TSMH_METER_CODE = 11 THEN ISNULL(TSMH_METER_INCREMENT, 0) ELSE 0 END ) 
              + SUM (CASE WHEN TSMH_TYPE = 20 AND TSMH_METER_CODE = 128 THEN ISNULL(TSMH_METER_INCREMENT, 0) ELSE 0 END ) 
              + SUM (CASE WHEN TSMH_TYPE = 20 AND TSMH_METER_CODE = 130 THEN ISNULL(TSMH_METER_INCREMENT, 0) ELSE 0 END ) 
              + SUM (CASE WHEN TSMH_TYPE = 20 AND TSMH_METER_CODE = 132 THEN ISNULL(TSMH_METER_INCREMENT, 0) ELSE 0 END ) 
              - SUM (CASE WHEN TSMH_TYPE = 20 AND TSMH_METER_CODE = 134 THEN ISNULL(TSMH_METER_INCREMENT, 0) ELSE 0 END ) 
              - SUM (CASE WHEN TSMH_TYPE = 20 AND TSMH_METER_CODE = 136 THEN ISNULL(TSMH_METER_INCREMENT, 0) ELSE 0 END ) 
              - SUM (CASE WHEN TSMH_TYPE = 20 AND TSMH_METER_CODE = 3 THEN ISNULL(TSMH_METER_INCREMENT, 0) ELSE 0 END )) * 0.01 MNET1
      
             , SUM (CASE WHEN TSMH_TYPE = 20 AND TSMH_METER_CODE = 5 THEN ISNULL(TSMH_METER_INCREMENT, 0) ELSE 0 END )   M5
						 , SUM (CASE WHEN TSMH_TYPE = 20 AND TSMH_METER_CODE = 6 THEN ISNULL(TSMH_METER_INCREMENT, 0) ELSE 0 END )   M6
             , SUM (CASE WHEN TSMH_TYPE = 20 AND TSMH_METER_CODE = 160 THEN ISNULL(TSMH_METER_INCREMENT, 0) ELSE 0 END ) M160
             , SUM (CASE WHEN TSMH_TYPE = 20 AND TSMH_METER_CODE = 162 THEN ISNULL(TSMH_METER_INCREMENT, 0) ELSE 0 END ) M162
             , SUM (CASE WHEN TSMH_TYPE = 20 AND TSMH_METER_CODE = 164 THEN ISNULL(TSMH_METER_INCREMENT, 0) ELSE 0 END ) M164
             , SUM (CASE WHEN TSMH_TYPE = 20 AND TSMH_METER_CODE = 184 THEN ISNULL(TSMH_METER_INCREMENT, 0) ELSE 0 END ) M184
             , SUM (CASE WHEN TSMH_TYPE = 20 AND TSMH_METER_CODE = 186 THEN ISNULL(TSMH_METER_INCREMENT, 0) ELSE 0 END ) M186
             , SUM (CASE WHEN TSMH_TYPE = 20 AND TSMH_METER_CODE = 188 THEN ISNULL(TSMH_METER_INCREMENT, 0) ELSE 0 END ) M188
             
      FROM   TERMINAL_SAS_METERS_HISTORY H 
INNER JOIN   TERMINALS 
				ON	 TE_TERMINAL_ID			= TSMH_TERMINAL_ID 
     WHERE   TSMH_DATETIME			>= @_d0 
			 AND	 TSMH_DATETIME			< @_d1
       AND   TSMH_TYPE					IN(20, 10) -- DAILY (20); METER RESET (10)
       AND   TSMH_DENOMINATION	= 0
       AND   TSMH_GAME_ID				= 0
			 AND   TSMH_METER_CODE IN (0,1,2,3,4,5,6,11,36,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,128,130,132,134,136,160,162,164,184,186,188)
  GROUP BY   TE_TERMINAL_ID
					 , TE_PROVIDER_ID
					 , TE_NAME
) TerminalsMeters 

-- @pGroupByOption == 1 => Provider
-- @pGroupByOption == 2 => Total
-- @pGroupByOption == 0 => Terminal
-- @pGroupByOption == 3 => AGG

if @pGroupByOption = 1
   Begin
        select   Manufacturer,
                 sum(CoinIn) CoinIn, sum(TotalWon) TotalWon, sum(Hold) Hold,
                 sum(BillIn) BillIn, sum(CashableTicketIn) CashableTicketIn, sum(RestrictedTicketIn) RestrictedTicketIn, sum(NonRestrictedTicketIn) NonRestrictedTicketIn,
                 sum(CashableTicketOut) CashableTicketOut, sum(RestrictedTicketOut) RestrictedTicketOut, sum(Jackpots) Jackpots, sum(HandPaidCanceledCredits) HandPaidCanceledCredits,
        
                 sum(TotalIn) TotalIn,
                 sum(TotalOut) TotalOut,
                 sum(TotalOut) TotalWin,
                 
                 sum(TotalReIn) TotalReIn,
                 sum(TotalReOut) TotalReOut,
                 sum(TotalReWin) TotalReWin,
                 sum(1) NumEgms

        from
               ( select * from #TerminalsMeter ) ManufacturerHold
        GROUP BY  MANUFACTURER      
        ORDER BY  Manufacturer                                
   End
else if @pGroupByOption = 2
   begin    
        select   sum(CoinIn) CoinIn, sum(TotalWon) TotalWon, sum(Hold) Hold,
                 sum(BillIn) BillIn, sum(CashableTicketIn) CashableTicketIn, sum(RestrictedTicketIn) RestrictedTicketIn, sum(NonRestrictedTicketIn) NonRestrictedTicketIn,
                 sum(CashableTicketOut) CashableTicketOut, sum(RestrictedTicketOut) RestrictedTicketOut, sum(Jackpots) Jackpots, sum(HandPaidCanceledCredits) HandPaidCanceledCredits,
        
                 sum(TotalIn) TotalIn,
                 sum(TotalOut) TotalOut,
                 sum(TotalOut) TotalWin,
                 
                 sum(TotalReIn) TotalReIn,
                 sum(TotalReOut) TotalReOut,
                 sum(TotalReWin) TotalReWin

        from    #TerminalsMeter
   
    end
else if @pGroupByOption = 3 -- IN CASE OF ADDING COLUMN TO THIS SP, CHECK FOLLOWING OBJECTS:  sp_GenerateDailyMeters
begin

      select 
        AssetNumber,              
        CoinIn,        
        TotalWon,        
        BillIn,       
        CashableTicketIn,
        RestrictedTicketIn,
        NonRestrictedTicketIn,
        Jackpots,       
        Games,
        Won,
        CashableCardsIn,
        RestrictedCardsIn,
        NonRestrictedCardsIn,
        CashableCardsOut,
        RestrictedCardsOut,
        NonRestrictedCardsOut
      from #TerminalsMeter  
      ORDER BY AssetNumber
      
end
else
   begin  
   
      select 
        AssetNumber,       
        Manufacturer,
        CoinIn,
        TotalWon,
        Hold,
        BillIn,
        BillInPerDenom,
        CashableTicketIn,
        RestrictedTicketIn,
        NonRestrictedTicketIn,
        CashableTicketOut,
        RestrictedTicketOut,
        Jackpots,
        HandPaidCanceledCredits,
        TotalIn,
        TotalDrop,
        TotalOut,
        TotalWin,
        TotalReIn,
        TotalReOut,
        TotalReWin
      from #TerminalsMeter  
      ORDER BY 
      case @pOrderGroup0
        when 0
          then AssetNumber
        when 1
          then Manufacturer
      end
      , 
       case @pOrderGroup0
        when 0
          then Manufacturer
        when 1
          then AssetNumber
      end
                                
   end

DROP table #TerminalsMeter

END

GO

GRANT EXECUTE ON [dbo].[TITO_HoldvsWin] TO [wggui] WITH GRANT OPTION

GO

