﻿using WSI.IDCamera;
using WSI.IDCamera.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WSI.IDCamera.Model;
namespace FileCameraConnector
{

  public class Camera : ICameraConnector
  {
    //Properties
    public bool IsAvailable()
    {
      return true;
    } //IsAvailable
    public bool IsBusy()
    {
      return false;
    } //IsBusy
    public string Name
    {
      get
      {
        return "File Connector";
      }
    } //Name
    public IntPtr TargetHandle
    {
      get;
      set;
    } //TargetHandle
    public bool Initialize()
    {
      return true;
    } //Initialize
    public bool HasPreview
    {
      get
      {
        return false;
      }
    } //HasPreview

    //Methods
    public Image CapturePhoto()
    {
      Image retval = null;
      OpenFileDialog openFileDialog = null;
      bool notFinished = true;
      openFileDialog = new OpenFileDialog()
        {
          Filter = "Image Files|*.png;*.bmp;*.jpg|All Files (*.*)|*.*"
        };
      while (notFinished)
      {
        openFileDialog.FileName = null;
        openFileDialog.Multiselect = false;
        openFileDialog.FilterIndex = 1;
        switch (openFileDialog.ShowDialog())
        {
          case DialogResult.OK:
            {
              try
              {
                retval = Image.FromFile(openFileDialog.FileName);
                notFinished = false;
              }
              catch (Exception ex)
              {
                WSI.Common.Log.Error(string.Format("({0}) is not a valid image file", openFileDialog.FileName));
                MessageBox.Show("Could not load image, please try again", "Error");
              }
            } break;
          default:
            {
              notFinished = false;
            } break;
        }
      }

      return retval;
    } //CapturePhoto
    public IEnumerable<CameraDevice> GetDevices()
    {
      return new List<CameraDevice>() { new CameraDevice() { Connector = GetType(), ID = 0, Name = Name + " - Dialog" } };
    } //
    public void Dispose()
    {
      //nothing initialized
    } //Dispose
    public void SetDevice(object ID)
    {
      //
    } //SetDevice
  }
}
