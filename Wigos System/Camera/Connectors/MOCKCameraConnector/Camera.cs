﻿using WSI.IDCamera;
using WSI.IDCamera.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using WSI.IDCamera.Model;
namespace CameraMOCK
{

    
    public class Camera : ICameraConnector
    {
        //Private
        private int id = -1;
        
        //Properties
        public bool HasPreview
        {
            get
            {
                return false;
            }
        }
        public bool IsAvailable()
        {
            return true;
        }
        public bool IsBusy()
        {
            return false;
        }
        public IntPtr TargetHandle
        {
            get;
            set;
        }
        public string Name
        {
            get { return "Mock Connector"; }
        }

        //Methods
        public bool Initialize()
        {
            return true;
        }
        public IEnumerable<CameraDevice> GetDevices()
        {
            return new List<CameraDevice>() { new CameraDevice() { Connector = GetType(), ID = 1, Name = Name + " - Mock" } };
        }
        public System.Drawing.Image CapturePhoto()
        {
            return id == 1 ? new Bitmap(CameraMOCK.Properties.Resources.MockReturnImage) : null;
        }
        public void SetDevice(object ID)
        {
            int? retval = ID as int?;
            if (retval.HasValue)
                id = retval.Value;
            else 
                retval = -1;
        }
        public void Dispose()
        {
            //
        }
    }
}
