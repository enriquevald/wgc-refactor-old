﻿using WSI.IDCamera;
using WSI.IDCamera.Model.Interfaces;
using DirectShowLib;
using SnapShot;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using WSI.IDCamera.Model;
using WSI.IDCamera.Model.Exceptions;

namespace DirectShowCameraConnector
{
  public class Camera : ICameraConnector
  {
    //Private
    private string _deviceId;
    private Capture _cam = null;
    private IntPtr _ip = IntPtr.Zero;

    //Properties
    public bool IsAvailable()
    {
      return true;
    } //IsAvailable
    public bool IsBusy()
    {
      return false;
    } //IsBusy
    public string Name
    {
      get { return "DirectShow Connector"; }
    } // Name
    public bool HasPreview
    {
      get
      {
        return true;
      }
    } //HasPreview
    public IntPtr TargetHandle
    {
      get;
      set;
    } //TargetHandle

    //Methods
    public IEnumerable<CameraDevice> GetDevices()
    {
      Control _target_control = null;
      _target_control = Control.FromHandle(TargetHandle);
      List<CameraDevice> retval = new List<CameraDevice>();
      foreach (DsDevice dev in DsDevice.GetDevicesOfCat(FilterCategory.VideoInputDevice))
      {
        try
        {
          var _caam = new Capture(dev.DevicePath, 640, 480, 0, _target_control);
          retval.Add(new CameraDevice() { Connector = GetType(), ID = dev.DevicePath, Name = Name + " - " + dev.Name });
          _caam.Dispose();
          _caam = null;
        }
        catch(Exception)
        { 
          //
        }
      }
      return retval;
    } //GetDevices
    public Image CapturePhoto()
    {
      Bitmap b = null;
      Cursor.Current = Cursors.WaitCursor;
      if (_ip != IntPtr.Zero)
      {
        Marshal.FreeCoTaskMem(_ip);
        _ip = IntPtr.Zero;
      }
      _ip = _cam.Click();
      b = new Bitmap(_cam.Width, _cam.Height, _cam.Stride, PixelFormat.Format24bppRgb, _ip);
      b.RotateFlip(RotateFlipType.RotateNoneFlipY);
      Cursor.Current = Cursors.Default;
      return b;
    } //CapturePhoto
    public bool Initialize()
    {
      Control _target_control = null;
      _target_control = Control.FromHandle(TargetHandle);
      try
      {
        _cam = new Capture(_deviceId, 640, 480, 0, _target_control);
        
        
        return true;
      }
      catch (Exception _ex)
      {
        throw new CameraDeviceNotAvaiableException(string.Format("Camera Device ({0}) Not Available", _deviceId), _ex);
      }
    } //Initialize
    public void Dispose()
    {
      if (_ip != IntPtr.Zero)
      {
        Marshal.FreeCoTaskMem(_ip);
        _ip = IntPtr.Zero;
      }
      if (_cam != null)
        _cam.Dispose();
    } //Dispose
    public void SetDevice(object ID)
    {
      _deviceId = ID.ToString();
    } //SetDevice
  }
}
