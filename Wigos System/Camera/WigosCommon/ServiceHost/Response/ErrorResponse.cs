﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace WigosCommon.ServiceHost.Response
{
  [Serializable, XmlRoot(ElementName = "response")]
  public class ErrorResponse
  {
    [XmlElementAttribute(Order = 0, ElementName = "type")]
    public String Type { get; set; }

    [XmlElementAttribute(Order = 1, ElementName = "code")]
    public String Code { get; set; }

    [XmlElementAttribute(Order = 2, ElementName = "message")]
    public String Message { get; set; }
  }
}
