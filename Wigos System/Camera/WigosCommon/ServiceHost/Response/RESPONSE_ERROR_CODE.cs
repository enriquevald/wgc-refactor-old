﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WigosCommon.ServiceHost.Response
{
  public enum RESPONSE_ERROR_CODE
  {
    ERR_NO_BODY,
    ERR_NO_REQUESTTYPE,
    ERR_NO_TRUSTEDIP,
    ERR_INCORRECT_PARAMS
  }
}
