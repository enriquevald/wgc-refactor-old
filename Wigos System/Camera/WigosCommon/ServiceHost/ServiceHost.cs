﻿using System;
using System.IO;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Xml;
using System.Xml.Serialization;
using WigosCommon.ServiceHost.Response;
using WSI.Common;

namespace WigosCommon.ServiceHost
{
  public class ServiceHost<T> : IDisposable
  {
    private HttpListener m_listener;
    private Thread m_thread;
    private String m_uri_address;
    private String m_gp_group_key = typeof(T).Name;
    private WorkerPool m_worker_pool;

    private const String LOCAL_IP_CONF_SETTING = "LocalIP";
    private const String SERVICE_PORT = "WCP.Port";
    private const String DEFAULT_SERVICE_PORT = "7778";

    public Boolean Init(String OverrideGpGroupKey = null)
    {
      String _local_ip;
      String _port;

      if (!String.IsNullOrEmpty(OverrideGpGroupKey))
      {
        m_gp_group_key = OverrideGpGroupKey;
      }

      // Get Local IpAddress
      String _fixed_ip = null;
      _fixed_ip = GeneralParam.GetString(m_gp_group_key, "WCP.FixedIP", null);

      _local_ip = _fixed_ip ?? ConfigurationFile.GetSetting(LOCAL_IP_CONF_SETTING);

      if (!Misc.IsAValidIP(_local_ip))
      {
        Log.Error(String.Format("ServiceHost<{0}>: WSI.Configuration {1} or WCP.FixedIP: is empty or invalid: {2}", typeof(T).Name, LOCAL_IP_CONF_SETTING, _local_ip));

        return false;
      }

      _port = GeneralParam.GetString(m_gp_group_key, SERVICE_PORT, DEFAULT_SERVICE_PORT);

      m_uri_address = Misc.GetIPAndPort(_local_ip, _port);

      if (!HttpListener.IsSupported)
      {
        Log.Error(string.Format("HttpListener ServiceHost<{0}> Not Supported", typeof(T).Name));

        return false;
      }

      m_worker_pool = new WorkerPool(string.Format("ServiceHost<{0}>", typeof(T).Name), GeneralParam.GetInt32(m_gp_group_key, "NumWorkers", 2, 2, 10));

      m_thread = new Thread(ListenerThread);
      m_thread.Start();

      return true;
    } // Init

    private void ListenerThread()
    {
      try
      {
        if (!HttpListener.IsSupported)
        {
          Log.Error(string.Format("HttpListener ServiceHost<{0}> Not Supported", typeof(T).Name));

          return;
        }

        if (m_listener == null)
        {
          m_listener = new HttpListener();
          m_listener.Prefixes.Add(m_uri_address);
          m_listener.AuthenticationSchemes = AuthenticationSchemes.Anonymous;
          m_listener.Start();

          Log.Message(string.Format("HttpListener ServiceHost<{0}> Start. Uri: {1}", typeof(T).Name, m_uri_address));
        }

        while (true)
        {
          HttpListenerContext _ctx;

          _ctx = m_listener.GetContext();
          m_worker_pool.EnqueueTask(new RequestTask<T>(_ctx));
        }
      }
      catch (Exception _exception)
      {
        Log.Message(string.Format("ServiceHost<{0}> Error: {1}", typeof(T).Name, m_uri_address));
        Log.Exception(_exception);
      }
    } // ListenerThread

    public void Dispose()
    {
      Dispose(true);
      GC.SuppressFinalize(this);
    }

    protected virtual void Dispose(bool Disposing)
    {
      if (!Disposing)
      {
        return;
      }

      m_thread.Abort();
      m_thread = null;
      m_listener.Stop();
      m_listener = null;
    } // Dispose
  }

  public class Service<T> : IDisposable
  {
    private const String TypeType = "type";
    private const String TypeError = "error";

    private T m_instance;

    private readonly HttpListenerContext m_context;

    public Service(HttpListenerContext Context)
    {
      m_instance = (T)Activator.CreateInstance(typeof(T));
      m_context = Context;
    }

    public void ProcessRequest()
    {
      try
      {
        if (m_context.Request.HttpMethod == "GET")
        {
          ProcessGet();

          return;
        }

        if (m_context.Request.HttpMethod == "POST")
        {
          ProcessPost();

          return;
        }

        if (m_context.Request.HttpMethod == "HEAD")
        {
          m_context.Response.StatusCode = (int)HttpStatusCode.OK;
          m_context.Response.StatusDescription = HttpStatusCode.OK.ToString();
          m_context.Response.Close();

          return;
        }

        // default
        m_context.Response.StatusCode = (int)HttpStatusCode.NotImplemented;
        m_context.Response.StatusDescription = HttpStatusCode.NotImplemented.ToString();
        m_context.Response.Close();
      }
      catch (Exception _ex)
      {
        m_context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
        m_context.Response.StatusDescription = _ex.Message;
        m_context.Response.Close();
      }
      finally
      {
        try
        {
          m_context.Response.Close();
        }
        catch (Exception _exception)
        {
          Log.Message(string.Format("ServiceHost<{0}> Error:", typeof(T).Name));
          Log.Exception(_exception);
        }
      }
    } //  ProcessRequest

    private void ProcessGet()
    {
      String _xml_response;

      _xml_response = GetXmlResponse(null, m_context.Request.QueryString["rType"]);
      ManageContextResponse(_xml_response);
    } // ProcessGET

    private void ProcessPost()
    {
      String _xml_message = String.Empty;
      XmlDocument _xml_doc = new XmlDocument();
      String _xml_response;

      if (!ManageXmlMessage(ref _xml_message, ref _xml_doc))
      {
        return;
      }

      _xml_response = GetXmlResponse(_xml_message, _xml_doc.GetElementsByTagName(TypeType)[0].InnerText);

      ManageContextResponse(_xml_response);

      //ManageLog(_xml_message, _xml_response);
    }

    //private void ManageLog(string _xml_message, string _xml_response)
    //{
    //  throw new NotImplementedException();
    //}

    private string GetXmlResponse(string XmlMessage, string Type)
    {
      object[] _param_array;
      ParameterInfo[] _pinfo;
      MethodInfo _method;

      _method = typeof(T).GetMethod(Type);

      if (_method != null)
      {
        _param_array = null;
        _pinfo = _method.GetParameters();

        if (_pinfo.Length > 0)
        {
          _param_array = DeserializeRequest(XmlMessage, _pinfo[0].ParameterType);
        }

        try
        {
          var _v = _method.Invoke(m_instance, _param_array);

          if (_v != null)
          {
            return SerializeResponse(_v);
          }
          else
          {
            return SerializeResponse(true);
          }
        }
        catch (Exception _ex)
        {
          Log.Error(string.Format("GetXmlResponse: Exception Occured (ERR_INCORRECT_PARAMS > {0}:{1})", typeof(T), Type));
          Log.Exception(_ex);

          return ErrorResponse(RESPONSE_ERROR_CODE.ERR_INCORRECT_PARAMS, "Request Type : " + Type);
        }
      }

      Log.Error(string.Format("GetXmlResponse: Method not found({0}:{1})", typeof(T), Type));

      return ErrorResponse(RESPONSE_ERROR_CODE.ERR_NO_REQUESTTYPE, "Request Type : " + Type);
    } // ProcessPOST

    private Object[] DeserializeRequest(String Xml, Type Type)
    {
      MemoryStream _mem_stream;

      try
      {
        XmlSerializer _serializer = new XmlSerializer(Type);
        _mem_stream = new MemoryStream(Encoding.UTF8.GetBytes(Xml));

        return new[] { _serializer.Deserialize(_mem_stream) };
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return null;
    } // DeserializeRequest

    private Boolean ManageXmlMessage(ref String XmlMessage, ref XmlDocument XmlDoc)
    {
      StreamReader _data_text;

      try
      {
        _data_text = new StreamReader(m_context.Request.InputStream, m_context.Request.ContentEncoding);
        XmlMessage = _data_text.ReadToEnd();

        if (XmlMessage.Length > 0)
        {
          XmlDoc.LoadXml(XmlMessage);

          return true;
        }

        ManageContextResponse(ErrorResponse(RESPONSE_ERROR_CODE.ERR_NO_BODY, "No body"));
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        ManageContextResponse(ErrorResponse(RESPONSE_ERROR_CODE.ERR_NO_BODY, "Error body: " + _ex.Message));
      }

      return false;
    } // ManageXmlMessage

    private void ManageContextResponse(String XmlResponse)
    {
      Byte[] _bf;

      try
      {
        _bf = Encoding.UTF8.GetBytes(XmlResponse);

        m_context.Response.StatusCode = (int)HttpStatusCode.OK;
        m_context.Response.StatusDescription = "OK";
        m_context.Response.ContentLength64 = _bf.Length;
        m_context.Response.OutputStream.Write(_bf, 0, _bf.Length);
        m_context.Response.OutputStream.Close();
        m_context.Response.Close();
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        Log.Message(string.Format("ManageContextResponse: HttpListener ServiceHost<{0}> not allowed", typeof(T)));
      }
    } // ManageContextResponse

    private String ErrorResponse(RESPONSE_ERROR_CODE ErrorCode, String ErrorMessage)
    {
      ErrorResponse _response;

      _response = new ErrorResponse
      {
        Type = TypeError,
        Code = ((int)ErrorCode).ToString(),
        Message = ErrorMessage
      };

      return SerializeResponse(_response);
    } // ErrorResponse

    private String SerializeResponse(Object ObjectResponse)
    {
      XmlSerializer _serializer = new XmlSerializer(ObjectResponse.GetType());
      XmlSerializerNamespaces _name_spaces = new XmlSerializerNamespaces();

      try
      {
        using (StringWriter _writer = new Utf8StringWriter())
        {
          _name_spaces.Add("", "");
          _serializer.Serialize(_writer, ObjectResponse, _name_spaces);

          return _writer.ToString();
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return String.Empty;
    } // SerializeResponse


    public void Dispose()
    {
      Dispose(true);
      GC.SuppressFinalize(this);
    }

    protected virtual void Dispose(bool Disposing)
    {
      if (!Disposing) return;
      m_instance = default(T);
    }
  }

  public class RequestTask<T> : Task, IDisposable
  {
    private HttpListenerContext m_ctx;
    private Service<T> m_process_request;

    public RequestTask(HttpListenerContext Ctx)
    {
      m_ctx = Ctx;
    } // RequestTask

    public override void Execute()
    {
      try
      {
        m_process_request = new Service<T>(m_ctx);
        m_process_request.ProcessRequest();
      }
      catch (Exception _ex)
      {
        if (_ex is System.Reflection.ReflectionTypeLoadException)
        {
          var typeLoadException = _ex as ReflectionTypeLoadException;
          var loaderExceptions = typeLoadException.LoaderExceptions;
        }

        Log.Exception(_ex);
      }
    } // Execute

    public void Dispose()
    {
      Dispose(true);
      GC.SuppressFinalize(this);
    }

    protected virtual void Dispose(bool Disposing)
    {
      if (!Disposing)
      {
        return;
      }

      m_ctx = null;

      if (m_process_request != null)
      {
        m_process_request.Dispose();
      }
    }
  } // RequestTask


  public class Utf8StringWriter : StringWriter
  {
    public override Encoding Encoding
    {
      get { return Encoding.UTF8; }
    }
  } // Utf8StringWriter
}