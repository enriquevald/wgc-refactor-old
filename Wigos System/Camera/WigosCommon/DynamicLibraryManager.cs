﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using WigosModel.Interfaces;

namespace WigosCommon
{
  public class DynamicLibraryManager<T> where T : IWigosPlugin
  {
    public IList<Type> FindAllCompatibleTypes(string SearchLocation, string DllPrefix)
    {
      List<Type> retval = new List<Type>();

      String[] _assembly_files = null;
      Assembly _assembly = null;

      _assembly_files = Directory.GetFiles(
          SearchLocation, string.Format("{0}*.dll", DllPrefix)
          );
      foreach (string assemblyFile in _assembly_files)
      {
        _assembly = Assembly.LoadFile(assemblyFile);
        foreach (var foundType in _assembly.GetTypes())
        {
          foreach (var foundInterface in foundType.GetInterfaces())
          {
            if (foundInterface == typeof(T))
            {
              retval.Add(foundType);
            }
          }
        }
      }
      return retval;
    } // FindCompatibleTypes

    public IDictionary<Type, T> LoadAllCompatibleTypes(string SearchLocation, string DllPrefix, params object[] ParamArray) 
    {
      Dictionary<Type, T> retval;
      retval = new Dictionary<Type, T>();

      IList<Type> _found_types;

      _found_types = FindAllCompatibleTypes(SearchLocation, DllPrefix);

      foreach (var foundType in _found_types)
      {
        if (ParamArray == null || ParamArray.Length == 0)
        {
          try
          {
            var _lib = (T) Activator.CreateInstance(foundType);
            retval.Add(foundType, _lib);
          }
          catch
          { 
            //swallow
          }
        }
        else
        {
          try
          {
            var _lib = (T) Activator.CreateInstance(foundType, ParamArray);
            retval.Add(foundType, _lib);
          }
          catch
          {
            //swallow
          }
        }
      }

      return retval;
    } // LoadAllCompatibleTypes

    //public void RegisterAllCompatibleTypesInUnityContainer(UnityContainer Container) 
    //{
    //  List<Type> types = FindAllCompatibleTypes(SearchLocation, DllPrefix);
    //  foreach(var type in types)
    //  {
    //    Container.RegisterType<T>(type,type.Name);
    //  }
    //}
  }
}
