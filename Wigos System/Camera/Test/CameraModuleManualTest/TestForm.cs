﻿using WSI.IDCamera;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace CameraModuleManualTest
{
  public partial class TestForm : Form
  {
    public TestForm()
    {
      InitializeComponent();
    }
    private void pictureBox1_Click(object sender, EventArgs e)
    {
      WSI.IDCamera.CameraModule _camera_module;
      Image _img;

      _camera_module = new WSI.IDCamera.CameraModule();

      try
      {
        _img = _camera_module.GetImage();
        if (_img != null)
        {
          pictureBox1.Image = _img;
        }
      }
      catch (Exception _ex)
      {
        MessageBox.Show(_ex.Message);
      }
    } // pictureBox1_Click
    private void TestForm_Load(object sender, EventArgs e)
    {
      this.pictureBox1.SizeMode = PictureBoxSizeMode.Zoom;
    } 
  }
}
