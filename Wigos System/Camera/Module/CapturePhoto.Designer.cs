﻿using System.Windows.Forms;
using WSI.Common;

namespace WSI.IDCamera
{
  public interface IfrmCapturePhoto
  {
    System.Drawing.Image GetImage(CameraModule cameraModule, Form frm);
  }

  partial class frmCapturePhoto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.previewSuface = new System.Windows.Forms.PictureBox();
            this.btnCancel = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.previewSuface)).BeginInit();
            this.SuspendLayout();
            // 
            // previewSuface
            // 
            this.previewSuface.Dock = System.Windows.Forms.DockStyle.Fill;
            this.previewSuface.Location = new System.Drawing.Point(0, 0);
            this.previewSuface.Name = "previewSuface";
      this.previewSuface.Size = new System.Drawing.Size(320, 300);
            this.previewSuface.TabIndex = 2;
            this.previewSuface.TabStop = false;
      this.previewSuface.Click += new System.EventHandler(this.previewSuface_Click);
      this.previewSuface.MouseDown += new System.Windows.Forms.MouseEventHandler(this.previewSuface_MouseDown);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnCancel.Location = new System.Drawing.Point(0, 0);
            this.btnCancel.Name = "btnCancel";
      this.btnCancel.Size = new System.Drawing.Size(320, 53);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_CANCEL"); 
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // frmCapturePhoto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(320, 300);
            this.ControlBox = false;
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.previewSuface);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmCapturePhoto";
            this.Text = "CapturePhoto";
            ((System.ComponentModel.ISupportInitialize)(this.previewSuface)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox previewSuface;
        private System.Windows.Forms.Button btnCancel;
    }
}