﻿using WSI.IDCamera;
using WSI.IDCamera.Model;
using WSI.IDCamera.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Drawing;
using WigosCommon;
using System.Collections;
using System.Windows.Forms;
using WSI.IDCamera.Model.Exceptions;

namespace WSI.IDCamera
{

  public delegate void CamerPhotoHandler(CameraPhotoEvent Event);

  public class CameraPhotoEvent
  {
    public Image Image;
    public bool StatusFoobar { get; set; }
  }

  public class CameraModule : IDisposable


  {
    public bool EventMode { get; set; }
    private Control m_remote_control = null;
    private CamerPhotoHandler m_remote_handler = null;

    private delegate void InvokeNotify(CameraPhotoEvent Event);

    public void RegisterHandler(Control Ctrl, CamerPhotoHandler Handler)
    {
      if (Handler == null)
      {
        // Unregister
        m_remote_control = null;
        m_remote_handler = null;
      }
      else
      {
        m_remote_handler += Handler;
        m_remote_control = Ctrl;
      }
    }

    public void UnRegisterHandler(CamerPhotoHandler PhotoHandler)
    {
      if (PhotoHandler == null)
      {
        // Unregister
        m_remote_handler = null;
        m_remote_control = null;
      }
      else
      {
        m_remote_handler -= PhotoHandler;
        m_remote_control = null;
      }
    }

    private void Notify(CameraPhotoEvent Event)
    {
      try
      {
        Control _ctrl;
        CamerPhotoHandler _handler;

        _ctrl = m_remote_control;
        _handler = m_remote_handler;
        if (_handler == null)
        {
          return;
        }

        if (_ctrl != null)
        {
          if (_ctrl.InvokeRequired)
          {
            _ctrl.BeginInvoke(new InvokeNotify(Notify), Event);

            return;
          }
        }

        _handler(Event);
      }
      catch
      {
      }
    }

    //Private
    private ICameraConnector _connector = null;
    private CameraDevice _cameraDevice = null;
    private Type _capturePhotoForm;
    private Type _selecteDeviceForm;
    private Form _parent_form;
    //Contructor
    public CameraModule(Form ParentForm, Type CapturePhotoForm = null, Type SelecteDeviceForm = null)
    {
      _parent_form = ParentForm;

      _capturePhotoForm = CapturePhotoForm ?? typeof (frmCapturePhoto);
      _selecteDeviceForm = SelecteDeviceForm ?? typeof (SelectDevice);

      IFormatter _formatter = null;

      _cameraDevice = null;
      if (File.Exists("Camera.bin"))
        try
        {
          using (Stream _stream = new FileStream("Camera.bin", FileMode.Open, FileAccess.Read, FileShare.Read))
          {
            _formatter = new BinaryFormatter();
            _cameraDevice = (CameraDevice) _formatter.Deserialize(_stream);
          }
        }
        catch (Exception _ex)
        {
          WSI.Common.Log.Error("Camera.bin file was invalid, eminiated and resetting camera search funcionality");
          WSI.Common.Log.Exception(_ex);
        }
    } //CameraModule

    public CameraModule(CameraDevice CameraDevice)
    {
      this._cameraDevice = CameraDevice;
    }


    private IfrmCapturePhoto m_camera_form = null;

//CameraModule

    //Methods
    public Image GetImage(Form ViewForm = null)
    {
      try
      {
        ISelectDevice _device_form = null;
        IFormatter _formatter = null;
        Image _photo = null;

        if (_cameraDevice == null)
        {
          _device_form = Activator.CreateInstance(_selecteDeviceForm) as ISelectDevice;
          _device_form.TopMost = true;
          _cameraDevice = _device_form.GetDevice(this, _parent_form);
          if (_cameraDevice != null)
          {
            _formatter = new BinaryFormatter();
            using (Stream _stream = new FileStream("Camera.bin", FileMode.Create, FileAccess.Write, FileShare.None))
            {
              _formatter.Serialize(_stream, _cameraDevice);
            }
          }
        }
        if (_cameraDevice != null)
        {
          if (_connector == null)
          {
            _connector = (ICameraConnector) Activator.CreateInstance(_cameraDevice.Connector);

            _connector.SetDevice(_cameraDevice.ID);

            if (_connector.HasPreview)
            {
              if (m_camera_form == null)
                m_camera_form = Activator.CreateInstance(_capturePhotoForm) as IfrmCapturePhoto;
            }
          }
          _photo = m_camera_form.GetImage(this, ViewForm ?? _parent_form);
        }
        else
        {
          _photo = TakePhoto();
        }

        return _photo;
      }


      catch (Exception)
      {

        throw;
      }
    } //GetImage

    public IEnumerable<CameraDevice> GetAllAttatchedDevices(Form Form)
    {
      List<CameraDevice> _retval = null;
      ICollection<ICameraConnector> _found_types = null;
      DynamicLibraryManager<ICameraConnector> _lib_manager;

      _retval = new List<CameraDevice>();

      _lib_manager = new DynamicLibraryManager<ICameraConnector>();

      _found_types =
        _lib_manager.LoadAllCompatibleTypes(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location),
          "CameraConnector").Values;
      if (_found_types.Count > 0)
      {
        foreach (var foundType in _found_types)
        {
          foundType.TargetHandle = Form.Handle;
          _retval.AddRange(foundType.GetDevices());
        }
      }

      return _retval;
    } //GetAllAttatchedDevices

    public bool StartCapture(IntPtr Target)
    {
      if (_connector == null)
      {

        return false;
      }
      _connector.TargetHandle = Target;
      try
      {
        _connector.Initialize();
      }
      catch (CameraDeviceNotAvaiableException)
      {
        if (File.Exists("Camera.bin"))
          File.Delete("Camera.bin");
        throw;
      }
      catch (Exception)
      {
        return false;
      }

      return true;
    } //StartCapture

    public System.Drawing.Image TakePhoto()
    {
      if (_connector == null)
      {
        return null;
      }
      return _connector.CapturePhoto();
    } //TakePhoto

    public CameraDevice GetDeviceDetails()
    {
      return _cameraDevice ?? new CameraDevice();
    } //GetDeviceDetails

    public void TakePhotoEvent()
    {
      if (_connector == null)
      {
        return;
      }
      try
      {
        Notify(new CameraPhotoEvent() {Image = _connector.CapturePhoto()});
      }
      catch (Exception)
      {
        if (File.Exists("Camera.bin"))
          File.Delete("Camera.bin");
        Notify(new CameraPhotoEvent() { StatusFoobar = true, Image = null });
      }

    }



    public void Dispose()
    {
      Dispose(true);
      GC.SuppressFinalize(this);
    }
    protected virtual void Dispose(bool Disposing)
    {
      if (!Disposing) return;
      
      if (m_camera_form != null)
      {
        ((Form)m_camera_form).Dispose();
        m_camera_form = null;

      }
      if (_connector == null)
      {
        return;
      }
      _cameraDevice = null;
      _connector.Dispose();
      _connector = null;
    }
  }
}
