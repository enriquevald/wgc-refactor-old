﻿using WSI.Common;
namespace WSI.IDCamera
{
    partial class SelectDevice
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="Disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool Disposing)
        {
            if (Disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(Disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lstDevices = new System.Windows.Forms.ListBox();
            this.btnSelecteDevice = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lstDevices
            // 
            this.lstDevices.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstDevices.FormattingEnabled = true;
            this.lstDevices.Location = new System.Drawing.Point(0, 0);
            this.lstDevices.Name = "lstDevices";
            this.lstDevices.Size = new System.Drawing.Size(284, 262);
            this.lstDevices.TabIndex = 0;
            this.lstDevices.DoubleClick += new System.EventHandler(this.listBox1_DoubleClick);
            // 
            // btnSelecteDevice
            // 
            this.btnSelecteDevice.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnSelecteDevice.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnSelecteDevice.Location = new System.Drawing.Point(0, 239);
            this.btnSelecteDevice.Name = "btnSelecteDevice";
            this.btnSelecteDevice.Size = new System.Drawing.Size(284, 23);
            this.btnSelecteDevice.TabIndex = 1;
            this.btnSelecteDevice.Text = Resource.String("STR_FORMAT_GENERAL_BUTTONS_SELECT");
            this.btnSelecteDevice.UseVisualStyleBackColor = true;
            // 
            // SelectDevice
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.btnSelecteDevice);
            this.Controls.Add(this.lstDevices);
            this.Name = "SelectDevice";
            this.Text = Resource.String("STR_SELECT_DEVICE");
            this.Load += new System.EventHandler(this.SelectDevice_Load);
            this.ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.ListBox lstDevices;
        private System.Windows.Forms.Button btnSelecteDevice;
    }
}