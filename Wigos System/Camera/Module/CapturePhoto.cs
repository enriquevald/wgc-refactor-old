﻿using System;
using System.Drawing;
using System.Windows.Forms;
using WSI.Common;
namespace WSI.IDCamera
{

    public partial class frmCapturePhoto : Form, IfrmCapturePhoto
    {
        //Private
        private CameraModule _cameraModule;

        //Contructor
        public frmCapturePhoto()
        {
            InitializeComponent();
            //btnTakePhoto.Text=Resource.String("STR_FORMAT_GENERAL_BUTTONS_TAKE_PHOTO");
            btnCancel.Text=Resource.String("STR_FORMAT_GENERAL_BUTTONS_CANCEL");
        } //frmCapturePhoto

        //Properties
        public Image CapturedImage { get; set; } //CapturedImage

        //Methods
        public Image GetImage(CameraModule CameraModule,Form Frm)
        {
            this.CenterToScreen();
            this.TopMost = true;
            Image retval = null;
            this._cameraModule = CameraModule;
            if (CameraModule.StartCapture(previewSuface.Handle))
            {
                if (this.ShowDialog(Frm) == DialogResult.OK)
                    retval = CapturedImage;
            }
            return retval;
        } //GetImage
        private void btnTakePhoto_Click(object sender, EventArgs e)
        {
            CapturedImage = _cameraModule.TakePhoto();
        }

        private void previewSuface_Click(object sender, EventArgs e)
        {

        }

        private void previewSuface_MouseDown(object sender, MouseEventArgs e)
        {
          CapturedImage = _cameraModule.TakePhoto();
          this.DialogResult = DialogResult.OK;
        } //btnTakePhoto_Click
    }
}
