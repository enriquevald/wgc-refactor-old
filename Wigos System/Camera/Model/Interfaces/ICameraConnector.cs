﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Runtime.Serialization;
using WigosCommon;
using WigosModel.Interfaces;
namespace WSI.IDCamera.Model.Interfaces
{
    public interface ICameraConnector : IWigosPlugin, IDisposable
    {
        IntPtr TargetHandle { get; set; }
        string Name { get; }
        bool HasPreview { get; }
        bool Initialize();
        bool IsAvailable();
        bool IsBusy();
        IEnumerable<CameraDevice> GetDevices();
        void SetDevice(object ID);
        Image CapturePhoto();
    }
}
