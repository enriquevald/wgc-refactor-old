﻿using WSI.IDCamera.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
namespace WSI.IDCamera.Model
{

    [Serializable]
    public class CameraDevice
    {
        public Type Connector { get; set; }
        public object ID { get; set; }
        public string Name { get; set; }
    }
}
