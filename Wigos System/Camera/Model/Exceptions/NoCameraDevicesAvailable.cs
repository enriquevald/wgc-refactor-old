using System;

namespace WSI.IDCamera.Model.Exceptions
{
  [Serializable]
  public class NoCameraDevicesAvailable : Exception
  {
    public NoCameraDevicesAvailable(string Msg, Exception Ex)
      : base(Msg, Ex)
    {
    }
  }
}