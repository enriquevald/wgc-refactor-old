using System;

namespace WSI.IDCamera.Model.Exceptions
{
  [Serializable]
  public class CameraDeviceTimeoutException : Exception
  {
    public CameraDeviceTimeoutException(string Msg, Exception Ex):base(Msg,Ex)
    {
    }
  }
}