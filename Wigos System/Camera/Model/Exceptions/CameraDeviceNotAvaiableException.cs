using System;

namespace WSI.IDCamera.Model.Exceptions
{
  [Serializable]
  public class CameraDeviceNotAvaiableException : Exception
  {
    public CameraDeviceNotAvaiableException(string Msg, Exception Ex):base(Msg,Ex)
    {
    }
  }
}