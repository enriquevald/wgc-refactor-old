//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WXP_Service.cs
// 
//   DESCRIPTION: Procedures for WXP to run as a service
// 
//        AUTHOR: Alberto Cuesta
// 
// CREATION DATE: 03-JUL-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 03-JUL-2012 ACC    First release 
//------------------------------------------------------------------------------
using System;
using System.ServiceProcess;
using System.Configuration.Install;
using System.ComponentModel;
using System.Reflection;
using System.IO;
using System.Diagnostics;
using System.Net;
using System.Threading;
using WSI.Common;
using WSI.WXP_Service;

public partial class WXP_Service : CommonService
{
  private const string VERSION = "18.019";
  private const string SVC_NAME = "WXP_Service";
  private const string PROTOCOL_NAME = "WXP";
  private const int SERVICE_PORT = 13003;
  private const Boolean STAND_BY = true;
  private const string LOG_PREFIX = "WXP";
  private const string CONFIG_FILE = "WSI.WXP_Configuration";
  private const ENUM_TSV_PACKET TSV_PACKET = ENUM_TSV_PACKET.TSV_PACKET_WXP_SERVICE;

  public static void Main(String[] Args)
  {
    WXP_Service _this_service;

    _this_service = new WXP_Service();
    _this_service.Run(Args);
  } // Main

  protected override void SetParameters()
  {
    m_service_name = SVC_NAME;
    m_protocol_name = PROTOCOL_NAME;
    m_version = VERSION;
    m_default_port = SERVICE_PORT;
    m_stand_by = STAND_BY;
    m_tsv_packet = TSV_PACKET;
    m_log_prefix = LOG_PREFIX;
    m_old_config_file = CONFIG_FILE;
  } // SetParameters

  protected override void OnServiceBeforeInit()
  {

  } // OnServiceBeforeInit

  /// <summary>
  /// Starts the service
  /// </summary>
  /// <param name="ipe"></param>
  protected override void OnServiceRunning(IPEndPoint Ipe)
  {

    //
    // Initializes Send Peru frames process ...
    //
    WXP_SendPeruFrames.Init();
    WXP_CreateColJuegosEvents.Init();


    Log.Message("WXP_Service started ... ");
    Log.Message("");
    Log.Message("");

  } // OnServiceRunning

} // WXP_Service


[RunInstallerAttribute(true)]
public class ProjectInstaller : Installer
{
  private ServiceInstaller serviceInstaller;
  private ServiceProcessInstaller processInstaller;

  public ProjectInstaller()
  {
    processInstaller = new ServiceProcessInstaller();
    serviceInstaller = new ServiceInstaller();
    // Service will run under system account
    processInstaller.Account = ServiceAccount.NetworkService;
    // Service will have Start Type of Manual
    serviceInstaller.StartType = ServiceStartMode.Manual;
    // Here we are going to hook up some custom events prior to the install and uninstall
    BeforeInstall += new InstallEventHandler(BeforeInstallEventHandler);
    BeforeUninstall += new InstallEventHandler(BeforeUninstallEventHandler);
    // serviceInstaller.ServiceName = servicename;
    Installers.Add(serviceInstaller);
    Installers.Add(processInstaller);
  }
  
  private void BeforeInstallEventHandler(object sender, InstallEventArgs e)
  {
    //The service has a fixed name.
    serviceInstaller.ServiceName = "WXP_Service";
  }
  
  private void BeforeUninstallEventHandler(object sender, InstallEventArgs e)
  {
    //The service has a fixed name.
    serviceInstaller.ServiceName = "WXP_Service";
  }
}
