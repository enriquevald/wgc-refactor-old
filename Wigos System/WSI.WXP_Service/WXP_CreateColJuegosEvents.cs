//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WXP_Service.cs
// 
//   DESCRIPTION: Procedures for ColJuegos Events for all terminals
// 
//        AUTHOR: David Rigal
// 
// CREATION DATE: 15-JUN-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 15-JUN-2015 DRV    First release 
//------------------------------------------------------------------------------
using System;
using System.ServiceProcess;
using System.Configuration.Install;
using System.ComponentModel;
using System.Reflection;
using System.IO;
using System.Diagnostics;
using System.Net;
using System.Threading;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using WSI.Common;

namespace WSI.WXP_Service
{

  public static class WXP_CreateColJuegosEvents
  {
    #region Enums



    #endregion

    #region Members

    static DateTime m_last_daily_report = DateTime.MinValue;
    static Int32 m_hour;
    static Int32 m_minutes;

    #endregion

    //------------------------------------------------------------------------------
    // PURPOSE : Initializes SendPeruFrames and Starts PeriodicJobThread thread 
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    static public void Init()
    {
      Thread _thread;

      m_last_daily_report = GetLastReported();
      GetConfigReportTime();

      _thread = new Thread(PeriodicColJuegosJobThread);
      _thread.Name = "PeriodicColJuegosJobThread";
      _thread.Start();

    } // Init


    //------------------------------------------------------------------------------
    // PURPOSE : Periodic thread
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    static private void PeriodicColJuegosJobThread()
    {
      DateTime _now;
      Int32 _wait_hint;

      while (true)
      {
        try
        {
          _now = WGDB.Now;
          _wait_hint = 60000 - _now.Second * 1000 - _now.Millisecond;

          Thread.Sleep(_wait_hint);

          if (GeneralParam.GetBoolean("Interface.Coljuegos", "Enabled"))
          {
            GetConfigReportTime();

            if (_now.Date != m_last_daily_report.Date && (_now.Hour > m_hour || (_now.Hour == m_hour && _now.Minute >= m_minutes)))
            {
              if (!GenerateDailyEvents())
              {
                Log.Error("GenerateDailyReport failed!");
              }
            }
          }
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }

      } // while (true)

    } // PeriodicJobThread

    //------------------------------------------------------------------------------
    // PURPOSE : Gets Configured report hour and minutes
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private static void GetConfigReportTime()
    {
      string[] _gp_date;

      _gp_date = GeneralParam.GetString("Interface.Coljuegos", "ReportTime").Split(':');

      if (!Int32.TryParse(_gp_date[0], out m_hour))
      {
        m_hour = 0;
      }
      if (!Int32.TryParse(_gp_date[1], out m_minutes))
      {
        m_minutes = 0;
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Gets the last reported datetime
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS : Last reported datetime
    //
    //   NOTES :
    //
    private static DateTime GetLastReported()
    {
      StringBuilder _sb;
      Object _obj;

      _sb = new StringBuilder();
      _sb.AppendLine(" SELECT   MAX(WXM_DATETIME) ");
      _sb.AppendLine("   FROM   WXP_002_MESSAGES ");
      _sb.AppendLine("  WHERE   WXM_EVENT_ID = " + (Int32)ColJuegos.ENUM_COLJUEGOS_TECHNICAL_EVENTS.ENUM_COLJUEGOS_TECHNICAL_EVENTS_NONE);

      try
      {
        using (DB_TRX _trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _trx.SqlTransaction.Connection, _trx.SqlTransaction))
          {
            _obj = _cmd.ExecuteScalar();
            if (_obj != null && _obj != DBNull.Value)
            {
              return (DateTime)_obj;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      return DateTime.MinValue;
    }


    //------------------------------------------------------------------------------
    // PURPOSE : Generate all daily events
    //
    //  PARAMS :
    //      - INPUT: 
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True: sent ok,
    //      - False: otherwise
    //
    //   NOTES : 
    //
    static private Boolean GenerateDailyEvents()
    {
      try
      {
        if (!ColJuegos.InsertColJuegosEvent(ColJuegos.ENUM_COLJUEGOS_TECHNICAL_EVENTS.ENUM_COLJUEGOS_TECHNICAL_EVENTS_NONE, 0))
        {
          return false;
        }
        m_last_daily_report = WGDB.Now;
        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // GenerateDailyReport



  } // CreateColJuegosEvents class

} // WSI.WXP_Service
