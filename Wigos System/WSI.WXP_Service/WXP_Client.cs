using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Configuration;
using System.Net.Security;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.IO;
using WSI.Common;
using System.Data;

namespace WSI.WXP_Service
{
  public static class WXP_Client
  {
    public enum SendStatus
    {
      Disconnected = 0,
      AckReceived = 1,
      NAckReceived = 2,
      Timeout = 3,
      ErrorRetry = 4,
      ResponseReceived = 5,
    }

    const String DELIMITER = "|";

    static RijndaelManaged m_aes;
    static ICryptoTransform m_aes_encryptor;
    //
    static Boolean m_work_disconnected = false;
    const int RECEIVE_TIMEOUT = 30 * 1000;

    static TcpClient m_client = new TcpClient();

    static Boolean m_first_time_t = true;
    static Boolean m_first_time_e = true;

    static public Boolean SendFrames(PERU_FRAME_TYPE FrameType)
    {
      String _frame_name;

      String _username;
      String _password;
      String _key;
      String _iv;
      String _url;
      Boolean _processed;

      SendStatus _status;
      PeruFrameRow _pending_frame;
      Boolean _is_first_time;

      PeruFramesTable _table;

      try
      {
        _is_first_time = false;

        switch (FrameType)
        {
          case PERU_FRAME_TYPE.TYPE_ECONOMICAL:
            _frame_name = "E-Frame";
            _is_first_time = m_first_time_e;
            break;

          case PERU_FRAME_TYPE.TYPE_TECHNICAL:
            _frame_name = "T-Frame";
            _is_first_time = m_first_time_t;
            break;

          default:
            Log.Error("Unexpected FrameType: " + FrameType.ToString());

            return false;
        }

        using (DB_TRX _db_trx = new DB_TRX())
        {
          _table = PeruFramesTable.CreatePeruFrameTable();
          if (!_table.ReadPendingToSend(FrameType, _db_trx.SqlTransaction))
          {
            Log.Error("ReadPendingToSend(" + _frame_name + ") failed.");

            return false;
          }
        }

        // Try to connect on start-up to validate login/password
        if (_table.Rows.Count <= 0 && !_is_first_time)
        {
          return true;
        }

        _username = GeneralParam.Value("ExternalProtocol", "Login.Username");
        _password = GeneralParam.Value("ExternalProtocol", "Login.Password");
        _key = GeneralParam.Value("ExternalProtocol", "Login.Key");
        _iv = GeneralParam.Value("ExternalProtocol", "Login.IV");

        if (!InitCipher(_key, _iv))
        {
          Log.Error("InitCipher(" + _frame_name + ".Login) failed.");

          return false;
        }

        _url = GeneralParam.Value("ExternalProtocol", _frame_name + ".RemoteURL");
        if (!Connect(_url))
        {
          return false;
        }

        switch (FrameType)
        {
          case PERU_FRAME_TYPE.TYPE_ECONOMICAL:
            m_first_time_e = false;
            break;

          case PERU_FRAME_TYPE.TYPE_TECHNICAL:
            m_first_time_t = false;
            break;
        }

        _status = Authenticate(_username, _password);
        if (_status != SendStatus.AckReceived)
        {
          Log.Error("Authenticate returned: " + _status.ToString());

          return false;
        }

        Log.Message("Connected & Authenticated for " + _frame_name + ", URL: " + _url);


        _key = GeneralParam.Value("ExternalProtocol", "Data.Key");
        _iv = GeneralParam.Value("ExternalProtocol", "Data.IV");

        if (!InitCipher(_key, _iv))
        {
          Log.Error("InitCipher(" + _frame_name + ".Data) failed.");

          return false;
        }

        foreach (DataRow _row in _table.Rows)
        {
          _pending_frame = new PeruFrameRow(_row);

          // Send
          _status = SendFrame(_pending_frame.Data);

          //
          // Mark status into database
          //    - If Disconnected ==> leave as pending
          //

          _processed = false;
          switch (_status)
          {
            case SendStatus.AckReceived:
              _pending_frame.Status = PERU_FRAME_STATUS.STATUS_SENT;
              _processed = true;
              break;

            case SendStatus.NAckReceived:
              _pending_frame.Status = PERU_FRAME_STATUS.STATUS_ERROR;
              _processed = true;
              break;

            case SendStatus.Disconnected:
            case SendStatus.ErrorRetry:
            case SendStatus.Timeout:
              break;

            default:
              Log.Error("Unexpected value for SendStatus: " + _status.ToString());
              break;
          }

          if (!_processed)
          {
            break;
          }

          using (DB_TRX _db_trx = new DB_TRX())
          {
            // Save into Database
            if (!_table.Save(_db_trx.SqlTransaction))
            {
              Log.Error("Save Frame Status failed!");

              break;
            }

            _db_trx.Commit();
          } // using
        } // for

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      finally
      {
        Disconnect();
      }

      return false;
    }


    //------------------------------------------------------------------------------
    // PURPOSE : Send frame
    //
    //  PARAMS :
    //      - INPUT: 
    //            - FrameRow
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True: sent ok,
    //      - False: otherwise
    //
    //   NOTES : 
    //
    static private SendStatus SendFrame(Byte[] Frame)
    {
      SendStatus _status;
      String _response;
      int _snd_len;
      int _status_code;

      if (m_work_disconnected)
      {
        return SendStatus.AckReceived;
      }

      if (!WXP_Client.Connected)
      {
        return SendStatus.Disconnected;
      }

      _snd_len = -1;
      _response = "";
      _status = SendStatus.ErrorRetry;
      for (int _idx_try = 0; _idx_try < 2; _idx_try++)
      {
        _status = WXP_Client.SendRequest(Frame, out _snd_len, out _response);
        if (_status == SendStatus.Timeout)
        {
          continue;
        }
        break;
      }

      if (_status != SendStatus.ResponseReceived)
      {
        return _status;
      }

      if (!_response.Contains(DELIMITER))
      {
        Log.Error("Unexpected ack/nack (delimiter): " + _response);

        return SendStatus.ErrorRetry;
      }

      if (!int.TryParse(_response.Substring(_response.IndexOf(DELIMITER) + 1), out _status_code))
      {
        Log.Error("Unexpected ack/nack (status code): " + _response);

        return SendStatus.ErrorRetry;
      }

      // ACK ?
      if (_status_code == _snd_len)
      {
        return SendStatus.AckReceived;
      }

      return TranslateStatusCode(_status_code);

    } // Send

    static private SendStatus TranslateStatusCode(int StatusCode)
    {
      //  Error Types (Negative Acknowledgement)
      //  Indicator	Description	Reason
      //    0	User not Available	Blocked User or Inactive User
      //    1	Invalid Password	Incorrect Password
      //    2	Amount of bytes error	Encrypted Transaction: Financial 96 bytes and technical 64 bytes. Un-encrypted Transaction: financial 92 bytes and technical 54 bytes.
      //    3	Encryption Error	Erroneous Application: Seed Vector or Padding.
      //    4	CRC Error	CRC Calculation Error
      //    5	Non Authenticated User	Non Authenticated User or Expired Session
      //    6	Disallowed Character	The basic data of the transactions should use only numeric characters [0-9], except for the basic data from the collector ID (technical transactions) which could also include letters [A-Z], without distinction between upper and lower case.
      //    9	Others	 

      switch (StatusCode)
      {
        case 0:
          Log.Error("SendFrame, StatusCode: " + StatusCode + ", User not Available.	Blocked User or Inactive User");

          return SendStatus.ErrorRetry;

        case 1:
          Log.Error("SendFrame, StatusCode: " + StatusCode + ", Invalid Password.");

          return SendStatus.ErrorRetry;

        case 2:
          Log.Error("SendFrame, StatusCode: " + StatusCode + ", Amount of bytes error: E-Frame 96|92, T-Frame 64|54");

          return SendStatus.NAckReceived;

        case 3:
          Log.Error("SendFrame, StatusCode: " + StatusCode + ", Encryption Error.	Erroneous Application: Seed Vector or Padding");

          return SendStatus.ErrorRetry;

        case 4:
          Log.Error("SendFrame, StatusCode: " + StatusCode + ", CRC Error");

          return SendStatus.NAckReceived;

        case 5:
          Log.Error("SendFrame, StatusCode: " + StatusCode + ", Non Authenticated User / Expired Session");

          return SendStatus.Timeout;

        case 6:
          Log.Error("SendFrame, StatusCode: " + StatusCode + ", Disallowed Character");

          return SendStatus.NAckReceived;

        case 9:
          Log.Error("SendFrame, StatusCode: " + StatusCode + ", Other errors");

          return SendStatus.NAckReceived;

        default:
          // NAK
          Log.Error("SendFrame, StatusCode: " + StatusCode + ", Unknown error");

          break;
      }

      return SendStatus.NAckReceived;
    }

    static private SendStatus Authenticate(String Username, String Password)
    {
      Byte[] _snd_frame;
      int _snd_len;
      String _response;
      SendStatus _status;
      int _status_code;

      try
      {
        if (m_work_disconnected)
        {
          return SendStatus.AckReceived;
        }

        _snd_frame = Encoding.UTF8.GetBytes(Username + DELIMITER + Password);
        if (_snd_frame.Length != 16)
        {
          Log.Error(String.Format("Wrong login information. Unexpected length. Received: {0}, Expected: {1}", _snd_frame.Length, 16));

          return SendStatus.ErrorRetry;
        }

        _status = SendRequest(_snd_frame, out _snd_len, out _response);
        if (_status != SendStatus.ResponseReceived)
        {
          return _status;
        }

        if (_response == Username)
        {
          return SendStatus.AckReceived;
        }

        if (!_response.Contains(DELIMITER))
        {
          Log.Error("Unexpected ack/nack (delimiter): " + _response);

          return SendStatus.ErrorRetry;
        }

        if (!int.TryParse(_response.Substring(_response.IndexOf(DELIMITER) + 1), out _status_code))
        {
          Log.Error("Unexpected ack/nack (status code): " + _response);

          return SendStatus.ErrorRetry;
        }

        return TranslateStatusCode(_status_code);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return SendStatus.ErrorRetry;
    }



    static private Boolean InitCipher(String Key, String IV)
    {
      try
      {
        m_aes = (RijndaelManaged)RijndaelManaged.Create();
        m_aes.KeySize = 128;
        m_aes.BlockSize = 128;
        m_aes.Mode = CipherMode.CBC;
        m_aes.Padding = PaddingMode.PKCS7;
        m_aes_encryptor = m_aes.CreateEncryptor(Encoding.UTF8.GetBytes(Key), Encoding.UTF8.GetBytes(IV));

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    static private Byte[] EncryptFrame(Byte[] Frame)
    {
      Byte[] _raw;
      using (MemoryStream _ms = new MemoryStream())
      {
        using (CryptoStream _cs = new CryptoStream(_ms, m_aes_encryptor, CryptoStreamMode.Write))
        {
          _cs.Write(Frame, 0, Frame.Length);
          _cs.FlushFinalBlock();
          _raw = new Byte[_ms.Position];
          Array.Copy(_ms.GetBuffer(), 0, _raw, 0, _raw.Length);
          return _raw;
        }
      }
    }

    private static Boolean Connected
    {
      get
      {
        try
        {
          if (m_work_disconnected)
          {
            return true;
          }
          return m_client.Connected;
        }
        catch
        {
        }
        return false;
      }
    }


    private static void Disconnect()
    {
      try { m_client.Client.Close(); }
      catch { }
      try { m_client.Close(); }
      catch { }
    }

    private static Boolean Connect(String RemoteURL)
    {
      try
      {
        IPEndPoint _ipe;

        Disconnect();

        // Not Connected
        m_work_disconnected = (RemoteURL == "None");
        if (m_work_disconnected)
        {
          return true;
        }

        try
        {
          String[] _values;
          IPAddress _ip;
          int _port;

          _values = RemoteURL.Split(new String[] { ":" }, 2, StringSplitOptions.RemoveEmptyEntries);
          _ip = IPAddress.Parse(_values[0]);
          _port = int.Parse(_values[1]);

          _ipe = new IPEndPoint(_ip, _port);
        }
        catch
        {
          Log.Error("Remote IP-EndPoint wrong format: " + RemoteURL);

          return false;
        }

        m_client = new TcpClient();
        Log.Message("Remote IP/Port: " + _ipe.ToString() + ", connecting ...");
        m_client.Connect(_ipe);

        m_client.ReceiveTimeout = RECEIVE_TIMEOUT;

        return m_client.Connected;
      }
      catch (SocketException _soex)
      {
        Log.Message(String.Format("SocketErrorCode: {0}, Message: {1}", _soex.SocketErrorCode.ToString(), _soex.Message));
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      return false;
    }

    private static SendStatus SendRequest(Byte[] Request, out int RequestLength, out String Response)
    {
      Response = "";
      RequestLength = 0;

      try
      {
        Byte[] _snd_frame;
        Byte[] _rcv_frame;
        int _snd_len;
        int _rcv_len;
        int _int_value;
        Boolean _log_enabled;

        _log_enabled = false;
        if (int.TryParse(Environment.GetEnvironmentVariable("WXP_LOG_ENABLED"), out _int_value))
        {
          _log_enabled = (_int_value != 0);
        }


        if ( _log_enabled)
        {
          Log.Message(String.Format("Frame L={0}, {1}", Request.Length, BitConverter.ToString(Request).Replace("-", "").ToUpper()));
        }

        _snd_frame = EncryptFrame(Request);
        if ( _log_enabled)
        {
          Log.Message(String.Format(" ---> L={0}, {1}", _snd_frame.Length, BitConverter.ToString(_snd_frame).Replace("-", "").ToUpper()));
        }

        RequestLength = _snd_frame.Length;

        _snd_len = m_client.Client.Send(_snd_frame);
        if (_snd_len == _snd_frame.Length)
        {
          _rcv_frame = new Byte[128];
          _rcv_len = m_client.Client.Receive(_rcv_frame);
          Response = Encoding.UTF8.GetString(_rcv_frame, 0, _rcv_len);

          if (_log_enabled)
          {
            Log.Message(String.Format(" <--- L={0}, {1}", _rcv_len, Response));
          }

          return SendStatus.ResponseReceived;
        }
      }
      catch (SocketException _soex)
      {
        Log.Message(String.Format("SocketErrorCode: {0}, Message: {1}", _soex.SocketErrorCode.ToString(), _soex.Message));

        if (_soex.SocketErrorCode == SocketError.TimedOut)
        {
          return SendStatus.Timeout;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return SendStatus.ErrorRetry;
    }
  }
}
