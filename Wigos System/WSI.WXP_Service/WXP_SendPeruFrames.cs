﻿//------------------------------------------------------------------------------
// Copyright © 2012 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WXP_Service.cs
// 
//   DESCRIPTION: Procedures for send peru frames
// 
//        AUTHOR: Alberto Cuesta
// 
// CREATION DATE: 04-JUL-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 04-JUL-2012 ACC    First release 
// 07-OCT-2013 RRR    Added WXP SAS Meters mailing.
//------------------------------------------------------------------------------
using System;
using System.ServiceProcess;
using System.Configuration.Install;
using System.ComponentModel;
using System.Reflection;
using System.IO;
using System.Diagnostics;
using System.Net;
using System.Threading;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using WSI.Common;

namespace WSI.WXP_Service
{

  public static class WXP_SendPeruFrames
  {
    #region Enums



    #endregion

    #region Members

    static DateTime m_next_daily_report = DateTime.MinValue;

    #endregion

    //------------------------------------------------------------------------------
    // PURPOSE : Initializes SendPeruFrames and Starts PeriodicJobThread thread 
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    static public void Init()
    {
      Thread _thread;

      _thread = new Thread(PeriodicJobThread);
      _thread.Name = "PeriodicJobThread";
      _thread.Start();

    } // Init


    //------------------------------------------------------------------------------
    // PURPOSE : Periodic thread
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    static private void PeriodicJobThread()
    {
      int _wait_hint;
      DateTime _now;
      int _last_repeat_report;

      _last_repeat_report = Misc.GetTickCount();

      while (true)
      {
        try
        {
          _now = WGDB.Now;
          _wait_hint = 60000 - _now.Second * 1000 - _now.Millisecond;
          Thread.Sleep(_wait_hint);

          if (GeneralParam.GetBoolean("ExternalProtocol", "Enabled", false))
          {
            if (Misc.GetElapsedTicks(_last_repeat_report) >= 5 * 60 * 1000)
            {
              PeruEvents.RepeatTerminalStatusReport();
              _last_repeat_report = Misc.GetTickCount();
            }

            if (!GenerateDailyReport())
            {
              Log.Error("GenerateDailyReport failed!");

              m_next_daily_report = _now.AddMinutes(20);
            }

            if (!WXP_Client.SendFrames(PERU_FRAME_TYPE.TYPE_TECHNICAL))
            {
              Log.Error("Send (T-Frames) failed!");
            }

            if (!WXP_Client.SendFrames(PERU_FRAME_TYPE.TYPE_ECONOMICAL))
            {
              Log.Error("Send (E-Frames) failed!");
            }
          }
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }

      } // while (true)

    } // PeriodicJobThread



    //------------------------------------------------------------------------------
    // PURPOSE : Generate all daily report (economic frames)
    //
    //  PARAMS :
    //      - INPUT: 
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True: sent ok,
    //      - False: otherwise
    //
    //   NOTES : 
    //
    static private Boolean GenerateDailyReport()
    {
      String _report_time_str;
      Int32 _ct_hh;
      Int32 _ct_mm;
      DateTime _dt_today_report;
      DateTime _dt_previous_report;
      DateTime _dt_now;

      DataSet _ds_machine_meters;
      DataTable _machine_meters;
      PeruFramesTable _peru_frames;
      String _site_register;
      String _machine_register;
      Int32 _terminal_id;
      Decimal _played_amount;
      Decimal _won_amount;
      Decimal _jackpot_amount;
      byte[] _frame;
      String _aux;
      String _body;

      _ct_hh = 0;
      _ct_mm = 0;
      _report_time_str = "";

      try
      {
        _dt_now = WGDB.Now;

        if (_dt_now < m_next_daily_report)
        {
          return true;
        }

        try
        {
          _report_time_str = WSI.Common.Misc.ReadGeneralParams("ExternalProtocol", "ReportTime");
          Int32.TryParse(_report_time_str.Substring(0, _report_time_str.IndexOf(":")), out _ct_hh);
          Int32.TryParse(_report_time_str.Substring(_report_time_str.IndexOf(":") + 1, _report_time_str.Length - _report_time_str.IndexOf(":") - 1), out _ct_mm);
        }
        catch
        {}

        _aux = _ct_hh.ToString("00") + ":" + _ct_mm.ToString("00");
        if (_report_time_str != _aux)
        {
          Log.Error("ExternalProtocol.ReportTime empty/bad format (hh:mm). Found=" + _report_time_str);

          return false;
        }

        _dt_today_report = _dt_now.Date.AddHours(_ct_hh).AddMinutes(_ct_mm);
        if (_dt_now < _dt_today_report)
        {
          return true;
        }

        // Initialize site registry
        _site_register = Misc.ReadGeneralParams("ExternalProtocol", "SiteRegistrationCode");
        if (String.IsNullOrEmpty(_site_register))
        {
          Log.Error("ExternalProtocol.SiteRegistrationCode not defined.");

          return false;
        }

        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (!SetLastDailyReportExecution(_dt_today_report, out _dt_previous_report, _db_trx.SqlTransaction))
          {
            Log.Error("SetLastDailyReportDateTime failed!");

            return false;
          }

          // Already run
          if (_dt_previous_report >= _dt_today_report)
          {
            m_next_daily_report = _dt_today_report.AddDays(1);

            return true;
          }

          // AJQ 22-AUG-2013, Save datetime of data retrieval to be used as the frame timestamp
          _dt_now = WGDB.Now;
          if (!PeruTerminalInfo.GetMachineMeters(out _machine_meters, _db_trx.SqlTransaction))
          {
            Log.Error("PeruTerminalInfo.GetMachineMeters: return false. ");

            return false;
          }

          // Create frames datatable
          _peru_frames = PeruFramesTable.CreatePeruFrameTable();

          // Add frame items
          foreach (DataRow _machine_meter_row in _machine_meters.Rows)
          {
            _terminal_id = (Int32)_machine_meter_row["TE_TERMINAL_ID"];
            _machine_register = (String)_machine_meter_row["TE_REGISTRATION_CODE"];
            _played_amount = (Decimal)_machine_meter_row["MM_PLAYED_AMOUNT"];
            _won_amount = (Decimal)_machine_meter_row["MM_WON_AMOUNT"];
            _jackpot_amount = (Decimal)_machine_meter_row["MM_JACKPOT_AMOUNT"];

            if (String.IsNullOrEmpty(_machine_register))
            {
              continue;
            }
            // AJQ 22-AUG-2013, Use datetime of data retrieval as the frame timestamp
            _frame = PeruFrameCreator.EconomicalFrame(_site_register, _machine_register, _dt_now, _played_amount, _won_amount, _jackpot_amount);
            _peru_frames.AddNewFrame(_terminal_id, PERU_FRAME_TYPE.TYPE_ECONOMICAL, _frame, _dt_now);
          }

          // Save frames into database
          if (!_peru_frames.Save(_db_trx.SqlTransaction))
          {
            Log.Error("_peru_frames.Save: return false. ");

            return false;
          }

          _ds_machine_meters = new DataSet();
          _ds_machine_meters.Tables.Add(_machine_meters);
          _body = Misc.DataSetToXml(_ds_machine_meters, XmlWriteMode.WriteSchema);

          // Insert mailing instance in DataBase
          if (!Mailing.DB_InsertInstance(_dt_today_report,
                                         MAILING_PROGRAMMING_TYPE.WXP_MACHINE_METERS,
                                         _body,
                                         MAILING_INSTANCES_STATUS.READY,
                                         _db_trx.SqlTransaction))
          {
            return false;
          }

          _db_trx.Commit();

          m_next_daily_report = _dt_today_report.AddDays(1);

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // GenerateDailyReport

    //------------------------------------------------------------------------------
    // PURPOSE : Set last execution
    //
    //  PARAMS :
    //      - INPUT: 
    //            - pDate
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True: set ok,
    //      - False: otherwise
    //
    //   NOTES : 
    //
    private static Boolean SetLastDailyReportExecution(DateTime TodayReport, out DateTime PreviousReport, SqlTransaction Trx)
    {
      StringBuilder _sb;

      PreviousReport = DateTime.MinValue;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine("IF NOT EXISTS ( SELECT WXP_LAST_EXECUTION FROM WXP_PARAMETERS )");
        _sb.AppendLine("   INSERT INTO WXP_PARAMETERS ( WXP_LAST_EXECUTION ) VALUES ( NULL ); ");
        _sb.AppendLine("");
        _sb.AppendLine("UPDATE  WXP_PARAMETERS ");
        _sb.AppendLine("   SET  WXP_LAST_EXECUTION  = @pTodayReport ");
        _sb.AppendLine("OUTPUT  DELETED.WXP_LAST_EXECUTION ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pTodayReport", SqlDbType.DateTime).Value = TodayReport;
          using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
          {
            if (_reader.Read())
            {
              PreviousReport = _reader.IsDBNull(0) ? DateTime.MinValue:_reader.GetDateTime(0);

              return true;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // SetLastDailyReportDateTime

  } // SendPeruFrames class

} // WSI.WXP_Service

