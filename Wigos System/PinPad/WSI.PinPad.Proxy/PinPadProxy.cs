﻿using System;
using System.Collections.Generic;
using System.Text;
using WSI.Common;


// Notes:
//    To create Proxy Class use: wsdl.exe /out:PinPadCommonProxy.cs http://localhost:9001/PinPadService?wsdl

namespace WSI.PinPad
{
  public class PinPadProxy
  {
    #region Attributes

    private PinPadCommon m_common_proxy;
    private Boolean m_result;
    private Boolean m_resultSpecified;

    #endregion

    #region Constructor

    public PinPadProxy(Common.PinPadType Type)
    {
      try
      {
        PinPadType _type_proxy = PinPadType.NONE;

        m_common_proxy = new PinPadCommon();

        switch (Type)
        {
          case Common.PinPadType.BANORTE:
            _type_proxy = PinPadType.BANORTE;
            break;

          case Common.PinPadType.DUMMY:
            _type_proxy = PinPadType.DUMMY;
            break;

          default:
            throw new Exception("The PinPad type had not been defined: " + Type);
        }

        m_common_proxy.Initialize(_type_proxy, true);
      }
      catch (Exception ex)
      {
        SaveException(ex);
      }
    }

    #endregion

    #region Public Methods

    public bool InitDevice()
    {
      try
      {
        m_common_proxy.InitDevice(out m_result, out m_resultSpecified);
        return m_result;
      }
      catch (Exception ex)
      {
        SaveException(ex);
        return false;
      }
    }

    public bool ReleaseDevice()
    {
      try
      {
        m_common_proxy.ReleaseDevice(out m_result, out m_resultSpecified);
        return m_result;
      }
      catch (Exception ex)
      {
        SaveException(ex);
        return false;
      }
    }

    public void ResetDevice()
    {
      try
      {
        m_common_proxy.ResetDevice();
      }
      catch (Exception ex)
      {
        SaveException(ex);
      }
    }

    public void StartTransaction()
    {
      try
      {
        m_common_proxy.StartTransaction();
      }
      catch (Exception ex)
      {
        SaveException(ex);
      }
    
    }

    public void EndTransaction()
    {
      try
      {
        m_common_proxy.EndTransaction();
      }
      catch (Exception ex)
      {
        SaveException(ex);
      }
    }

    public bool IsAvailable()
    {
      try
      {
        m_common_proxy.IsAvailable(out m_result, out m_resultSpecified);
        return m_result;
      }
      catch (Exception ex)
      {
        SaveException(ex);
        return false;
      }
    }

    public bool Pay(decimal Amount)
    {
      try
      {
        PinPadResponse _pinpad_response;
        m_common_proxy.Pay(Amount, true, out m_result, out m_resultSpecified, out _pinpad_response);
        return m_result;
      }
      catch (Exception ex)
      {
        SaveException(ex);
        return false;
      }
    }

    public bool Read(out PinPadCard PinPadCard)
    {
      PinPadCard = null;

      try
      {
        m_common_proxy.Read(out m_result, out m_resultSpecified, out PinPadCard);
        return m_result;
      }
      catch (Exception ex)
      {
        SaveException(ex);
      }
      return false;

    }

    public bool SendTransaction(PinPadCard PinPadCard, Decimal Amount, out PinPadResponse PinPadResponse) 
    {
      PinPadResponse = null;

      try
      {
        m_common_proxy.SendTransaction(PinPadCard, Amount, true, out m_result, out m_resultSpecified, out PinPadResponse);
        return m_result;
      }
      catch (Exception ex)
      {
        SaveException(ex);
      }
      return false;
    }

    public bool NotifyResult(PinPadResponse ResponseIn, out PinPadResponse ResponseOut)
    {
      ResponseOut = null;

      try
      {
        m_common_proxy.NotifyResult(ResponseIn, out m_result, out m_resultSpecified, out ResponseOut);
        return m_result;
      }
      catch (Exception ex)
      {
        SaveException(ex);
      }
      return false;
    }

    public bool IsDriverInstalled()
    {
      try
      {
        m_common_proxy.IsDriverInstalled(out m_result, out m_resultSpecified);
        return m_result;
      }
      catch (Exception ex)
      {
        SaveException(ex);
        return false;
      }
    }

    public bool InstallDriver()
    {
      try
      {
        m_common_proxy.InstallDriver(out m_result, out m_resultSpecified);
        return m_result;
      }
      catch (Exception ex)
      {
        SaveException(ex);
        return false;
      }
    }

    #endregion

    #region Private Methods

    private void SaveException(Exception ex)
    {
      if (ex.GetType() == typeof(System.Net.WebException))
      {
        System.Net.WebException wex = (System.Net.WebException)ex;
        Log.Error(String.Format(" The WCF service hosted in the 'Pinpad Windows service' did not respond (status: {0}). {1}", wex.Status, wex.Message));
      }
      else
      {
        Log.Exception(ex);
      }
    }

    #endregion

  }
}
