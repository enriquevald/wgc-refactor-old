﻿//------------------------------------------------------------------------------
// Copyright © 2015-2020 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: PinPadCommon.cs
//  
//   DESCRIPTION: Class that implement methods to communicate to a Pin Pad
// 
//        AUTHOR: Francisco Vicente
// 
// CREATION DATE: 15-JUN-2016
//
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 15-JUN-2016 FAV     First release.
// 15-JUN-2016 ETP     Product Backlog Item 14340: Instalación del sofware para PinPad
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using WSI.Common;
using WSI.Common.PinPad;

namespace WSI.PinPad
{
  public class PinPadCommon
  {
    private IPinPad m_pin_pad;
    private PinPadCashierTerminal m_pin_pad_terminal;

    public PinPadCommon(IPinPad PinPad, PinPadCashierTerminal PinPadCashierTerminal)
    {
      if (PinPad == null)
        throw new ArgumentNullException("PinPad");

      m_pin_pad = PinPad;
      m_pin_pad_terminal = PinPadCashierTerminal;
    }
    
    public PinPadCommon(IPinPad PinPad)
    {
      if (PinPad == null)
        throw new ArgumentNullException("PinPad");

      m_pin_pad = PinPad;
      m_pin_pad_terminal = new PinPadCashierTerminal();
    }

    public void Initialize()
    {
      m_pin_pad.Initialize();
    }

    public bool InitDevice()
    {
      try
      {
        return m_pin_pad.InitDevice(m_pin_pad_terminal);
      }
      catch { }

      return false;
    }

    public bool ReleaseDevice()
    {
      try
      {
        return m_pin_pad.ReleaseDevice();
      }
      catch {}

      return false;
    }

    public void ResetDevice()
    {
      m_pin_pad.ResetDevice();
    }

    public void StartTransaction()
    {
      m_pin_pad.StartTransaction();
    }

    public void EndTransaction()
    {
      m_pin_pad.EndTransaction(); 
    }

    public bool IsAvailable()
    {
      return m_pin_pad.IsAvailable();
    }

    public bool Pay(decimal Amount, String ControlNumber, out PinPadResponse PinPadResponse)
    {
      PinPadResponse = null;
      return m_pin_pad.Pay(Amount, ControlNumber, out PinPadResponse);
    }

    public bool Read(out PinPadCard PinPadCard)
    {
      PinPadCard = null;
      return m_pin_pad.Read(out PinPadCard);
    }

    public bool SendTransaction(PinPadCard PinPadCard, decimal Amount, String ControlNumber, out PinPadResponse PinPadResponse)
    {
      PinPadResponse = null;
      return m_pin_pad.SendTransaction(PinPadCard, Amount, ControlNumber, out PinPadResponse);
    }

    public bool NotifyResult(PinPadResponse ResponseIn, out PinPadResponse ResponseOut)
    {
      ResponseOut = null;
      return m_pin_pad.NotifyResult(ResponseIn, out ResponseOut);
    }

    public bool Cancel(String ControlNumber, out PinPadResponse ResponseOut)
    {
      return m_pin_pad.Cancel(ControlNumber,out ResponseOut);
    }

    public bool Reverse(String ControlNumber, out PinPadResponse ResponseOut)
    {
      return m_pin_pad.Reverse(ControlNumber, out ResponseOut);
    }
    
    public PinPadType GetPinPadType()
    {
      return m_pin_pad.GetPinPadType();
    }

    public String GetPinPadTerminalID()
    {
      return m_pin_pad.GetPinPadTerminalID();
    }

    public void SetPinPadTerminalID(String TerminalID)
    {
      m_pin_pad.SetPinPadTerminalID(TerminalID);
    }

    public String GetPinPadMode()
    {
      return m_pin_pad.GetPinPadMode();
    }

    public bool IsDriverInstalled()
    {
      return m_pin_pad.IsDriverInstalled();
    }

    public bool InstallDriver()
    {
      return m_pin_pad.InstallDriver();
    }
  }
}
