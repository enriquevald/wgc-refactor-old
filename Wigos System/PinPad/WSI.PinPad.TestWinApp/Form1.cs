﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WSI.Common;
using WSI.Common.PinPad;
using WSI.PinPad.Banorte.Adapter;

namespace WSI.PinPad.TestWinApp
{
  public partial class Form1 : Form
  {
    PinPadCommon _obj;

    public Form1()
    {
      InitializeComponent();
    }

    private void button1_Click(object sender, EventArgs e)
    {
      PinPadResponse _pinpad_response;
      PinPadResponse _pinpad_response_out;
      PinPadCard _pinpad_card;
      try
      {
        lbl_result.Text = String.Empty;
        _obj.StartTransaction();
        if (_obj.Read(out _pinpad_card))
        {
          string _control_number = DateTime.Now.ToString("yyyyMMddhhmmss"); // Misc.GetControlNumberByOperationId(Int64.Parse(DateTime.Now.ToString("yyyyMMddhhmmss")));

          if (_obj.SendTransaction(_pinpad_card, 0.1m, _control_number, out _pinpad_response))
          {
            if (_obj.NotifyResult(_pinpad_response, out _pinpad_response_out))
            {
              lbl_result.Text = "AuthorizationCode: " + _pinpad_response_out.AuthorizationCode;
            }
          }
        }
      }
      catch
      {

      }
      finally
      {
        _obj.EndTransaction();
      }
    }

    private void button2_Click(object sender, EventArgs e)
    {
      PinPadResponse _pinpad_response;
      try
      {
        lbl_result.Text = String.Empty;
        _obj.StartTransaction();


        string _control_number = DateTime.Now.ToString("yyyyMMddhhmmss"); // Misc.GetControlNumberByOperationId(Int64.Parse(DateTime.Now.ToString("yyyyMMddhhmmss")));

        if (_obj.Pay(0.1m, _control_number, out _pinpad_response))
        {
          lbl_result.Text = "AuthorizationCode: " + _pinpad_response.AuthorizationCode;
        }
        else
        {
          lbl_result.Text = "ERROR";
        }

      }
      catch
      {

      }
      finally
      {
        _obj.EndTransaction();
      }
    }

    private void button3_Click(object sender, EventArgs e)
    {
      lbl_result.Text = String.Empty;
      _obj.ResetDevice();
    }

    private void Form1_Load(object sender, EventArgs e)
    {
      IPinPad _pinpad_banorte;
      PinPadCashierTerminal _pinpad_terminal;

      _pinpad_banorte = new PinPadBanorte(Common.PinPadType.BANORTE);
      _pinpad_terminal = new PinPadCashierTerminal()
      {
        Port = PinPadCashierTerminal.PINPAD_PORT.COM9,
        Enabled = true,
        Type = Common.PinPadType.BANORTE
      };

      _pinpad_banorte.Initialize();

      _obj = new PinPadCommon(_pinpad_banorte, _pinpad_terminal);
      _obj.InitDevice();
    }

    private void Form1_FormClosing(object sender, FormClosingEventArgs e)
    {
      _obj.ReleaseDevice();
    }
  }
}
