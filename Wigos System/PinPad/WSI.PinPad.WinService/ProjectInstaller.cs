﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;
using System.ServiceProcess;

namespace WSI.PinPad.WinService
{
  [RunInstaller(true)]
  public partial class ProjectInstaller : System.Configuration.Install.Installer
  {
    public ProjectInstaller()
    {
      //InitializeComponent();

      serviceProcessInstaller1 = new ServiceProcessInstaller();
      serviceProcessInstaller1.Account = ServiceAccount.LocalSystem;
      serviceInstaller1 = new ServiceInstaller();
      serviceInstaller1.ServiceName = "PinpadService";
      serviceInstaller1.DisplayName = "Pinpad Service";
      serviceInstaller1.Description = "Pinpad service for the Cashier system";
      serviceInstaller1.StartType = ServiceStartMode.Automatic;
      Installers.Add(serviceProcessInstaller1);
      Installers.Add(serviceInstaller1);
    }
  }
}
