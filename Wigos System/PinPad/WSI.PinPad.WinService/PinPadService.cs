﻿//------------------------------------------------------------------------------
// Copyright © 2015-2020 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: PinPadService.cs
//  
//   DESCRIPTION: PinPadService
// 
//        AUTHOR: Francisco Vicente
// 
// CREATION DATE: 13-JUN-2016
//
// Notes:
//     To install use: installutil.exe WSI.PinPad.WinService.exe
//     To uninstall use: installutil.exe WSI.PinPad.WinService.exe /u
//
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 13-JUN-2016 FAV     First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.ServiceModel.Discovery;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using WSI.Common;

namespace WSI.PinPad.WinService
{
  partial class PinPadService : ServiceBase
  {
    #region Attributes

    private ServiceHost m_svcHost = null;  

    #endregion

    #region Constructor

    public PinPadService()
    {
      InitializeComponent();
    }

    #endregion

    #region Public Methods

    protected override void OnStart(string[] args)
    {
      try
      {
        //Thread.Sleep(10000);

        Log.AddListener(new SimpleLogger("PINPAD"));

        if (m_svcHost != null) m_svcHost.Close();

        string strAdrHTTP = "http://localhost:9001/PinPadService";

        Uri[] adrbase = { new Uri(strAdrHTTP) };

        m_svcHost = new ServiceHost(typeof(PinPadCommon), adrbase);

        m_svcHost.Description.Behaviors.Add(new ServiceDiscoveryBehavior());
        m_svcHost.AddServiceEndpoint(new UdpDiscoveryEndpoint());

        ServiceMetadataBehavior mBehave = new ServiceMetadataBehavior();
        mBehave.HttpGetEnabled = true;
        mBehave.MetadataExporter.PolicyVersion = PolicyVersion.Policy15;
        m_svcHost.Description.Behaviors.Add(mBehave);

        BasicHttpBinding httpb = new BasicHttpBinding();
        m_svcHost.AddServiceEndpoint(typeof(IPinPad), httpb, strAdrHTTP);
        m_svcHost.AddServiceEndpoint(typeof(IMetadataExchange),
        MetadataExchangeBindings.CreateMexHttpBinding(), "mex");

#if DEBUG
        // Returns information with details about the error
        m_svcHost.Description.Behaviors.Remove(typeof(ServiceDebugBehavior));
        m_svcHost.Description.Behaviors.Add(new ServiceDebugBehavior { IncludeExceptionDetailInFaults = true });
#endif

        m_svcHost.Open();
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
    }

    protected override void OnStop()
    {
      try
      {
        if (m_svcHost != null)
        {
          m_svcHost.Close();
          m_svcHost = null;
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
    }

    #endregion

  }
}
