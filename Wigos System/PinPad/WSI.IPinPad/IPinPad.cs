﻿using System;
using System.Collections.Generic;
using System.Text;
using WSI.Common;
using WSI.Common.PinPad;


namespace WSI.PinPad
{
  public interface IPinPad
  {
    Boolean InitDevice();

    Boolean ReleaseDevice();

    void ResetDevice();

    void StartTransaction();

    void EndTransaction();

    Boolean IsAvailable();

    Boolean Pay(Decimal Amount, out PinPadResponse PinPadResponse);

    Boolean Read(out PinPadCard PinPadCard);

    Boolean SendTransaction(PinPadCard PinPadCard, Decimal Amount, out PinPadResponse PinPadResponse);

    Boolean NotifyResult(PinPadResponse ResponseIn, out PinPadResponse ResponseOut);

    Boolean CancelPay();

    Boolean IsDriverInstalled();

    Boolean InstallDriver();
  }
}
