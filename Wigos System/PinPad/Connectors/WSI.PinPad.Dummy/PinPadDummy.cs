﻿//------------------------------------------------------------------------------
// Copyright © 2007-2013 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: PinPadDummy.cs
// 
//   DESCRIPTION: Declare all functions and procedures related to TITO Business Logic
//
//        AUTHOR: e
//
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 05-AUG-2016 ETP    First release.
// 24-OCT-2016 ETP    Fixed Bug 19314 Pin Pad recharge refactoring
// 07-NOV-2016 ATB    Bug 20154:Televisa: No se muestra el numero de "Afiliación" en los correspondientes vouchers
// 21-MAY-2017 ETP    Fixed Bug WIGOS-2997: Added voucher data.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using WSI.Common;
using WSI.Common.PinPad;


namespace WSI.PinPad
{

  public class PinPadDummy : IPinPad
  {
    private PinPadType m_pinpad_type;
    private PinPadCard m_test_card;

    public PinPadDummy()
    {
      m_pinpad_type = PinPadType.DUMMY;
      m_test_card = new PinPadCard();
      m_test_card.CardBankName = "LA CAIXA";
      m_test_card.CardHolder = "TestTest TestTest/TestTest";
      m_test_card.CardNumber = "999999999999999999999";
      m_test_card.Track1 = "";
      m_test_card.Track2 = "";
      m_test_card.POSEntryMode = "CHIP";
      m_test_card.TagsEMV = "";
    }

    public void Initialize()
    {      
    } // Initialize

    public bool InitDevice(PinPadCashierTerminal PinPadCashierTerminal)
    {
      return true;
    } // InitDevice

    public bool ReleaseDevice()
    {
      return true;
    } // ReleaseDevice

    public void ResetDevice()
    {
      return;
    } // ResetDevice

    public bool IsAvailable()
    {
      return true;
    } // IsAvailable

    public bool Pay(Decimal Amount, String ControlNumber, out PinPadResponse PinPadResponse)
    {
      Random _rnd;

      PinPadResponse = new PinPadResponse();
      PinPadResponse.AuthorizationCode = "123456789";
      PinPadResponse.StatusResponse = PinPadTransactions.STATUS.PENDING_WIGOS;
      PinPadResponse.PinPadCard = m_test_card;
      PinPadResponse.Reference = "12345620202020";
      // ATB 07-NOV-2016
      PinPadResponse.MerchandId = "9876543";

      PinPadResponse.CardType = "MASTERCARD";
      PinPadResponse.ProductType = "Débito";
      //PinPadResponse.ProductType = "Crédito";
      PinPadResponse.ExpirationDate = "2203";
     

      PinPadResponse.AID = "A0000000000000000000";
      PinPadResponse.TVR = "C000000000";
      PinPadResponse.TSI = "6500";
      PinPadResponse.APN = "MASTERCARD";
      PinPadResponse.AL =  "MASTERCARD2";
     
      PinPadResponse.WithPin = false;

      Thread.Sleep(1500);

      if (GetPinPadMode() == "AUT")
      {
        return true;
      }

      if (GetPinPadMode() == "DEC")
      {
        PinPadResponse.StatusResponse = PinPadTransactions.STATUS.DECLINED;
        PinPadResponse.ResponseMessage = "Declined";
        return false;
      }

      _rnd = new Random();

      if (_rnd.NextDouble() > 0.50)
      {
        PinPadResponse.StatusResponse = PinPadTransactions.STATUS.DECLINED;
        PinPadResponse.ResponseMessage = "Declined";
        return false;
      }
      
      return true;

      
    } // Pay

    public Boolean Cancel(String ControlNumber, out PinPadResponse ResponseOut)
    {
      ResponseOut = new PinPadResponse();
      ResponseOut.AuthorizationCode = "123456789";
      ResponseOut.StatusResponse = PinPadTransactions.STATUS.APPROVED;
      ResponseOut.PinPadCard = m_test_card;
      return true;
    } // Cancel

    public Boolean Reverse(String ControlNumber, out PinPadResponse ResponseOut)
    {
      ResponseOut = new PinPadResponse();
      ResponseOut.AuthorizationCode = "123456789";
      ResponseOut.StatusResponse = PinPadTransactions.STATUS.APPROVED;
      ResponseOut.PinPadCard = m_test_card;
      return true;
    } // Reverse

    public PinPadType GetPinPadType()
    {
      return m_pinpad_type;
    }

    public bool IsDriverInstalled()
    {
      return true;
    } // IsDriverInstalled

    public bool InstallDriver()
    {
      if (IsDriverInstalled())
      {
        return true;
      }
      return true;
    } // InstallDriver

    public void StartTransaction()
    {
      return;
    }

    public void EndTransaction()
    {
      return;
    }

    public bool Read(out PinPadCard PinPadCard)
    {
      PinPadCard = new PinPadCard()
      {
        CardHolder = "TESTER SPAIN",
        CardNumber = "4160810000000000",
        POSEntryMode = "CHIP",
      };

      return true;
    }

    public bool SendTransaction(PinPadCard PinPadCard, decimal Amount, String ControlNumber, out PinPadResponse PinPadResponse)
    {
      PinPadResponse = new PinPadResponse()
      {
      };

      return true;
    }

    public bool NotifyResult(PinPadResponse ResponseIn, out PinPadResponse ResponseOut)
    {
      ResponseOut = new PinPadResponse()
      {
        AuthorizationCode = "123456789",
        StatusResponse = PinPadTransactions.STATUS.APPROVED,
      };

      return true;
    }
    public String GetPinPadTerminalID()
    {
      return "987654321";
    }

    public void SetPinPadTerminalID(String TerminalID)
    {
    } //SetPinPadTerminalID


    public string GetPinPadMode()
    {
      return GeneralParam.GetString("PinPad","Provider.001.Test.Mode","AUT");
    }
  }
}
