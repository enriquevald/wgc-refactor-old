﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : PinPadBanorte.cs
// 
//   DESCRIPTION : PinPad Connector for Banorte
//
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 07-JUN-2016 AMF    First release
// 15-JUN-2016 ETP    Product Backlog Item 14340: Instalación del sofware para PinPad
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Diagnostics;
using System.Security.Permissions;
using System.Security;
using System.Text;
using Microsoft.Win32;
using WSI.Common;
using WSI.Common.PinPad;
using ICSharpCode.SharpZipLib.Core;
using ICSharpCode.SharpZipLib.Zip;

namespace WSI.PinPad.Banorte.Adapter
{
  public class PinPadBanorte : IPinPad
  {
    #region " Members "

    private PinPadAdapterBanorte m_pinpad_banorte;
    private PinPadType m_pinpad_type;


    const string SOFTWARE_NAME_KEY = "DisplayName";
    const string SOFTWARE_NAME_VALUE = "VeriFone Vx Installer version 1.0.0.52.B5";
    const string SOFTWARE_NAME_VALUE2 = "VeriFone Vx Installer";

    const string DRIVER_NAME_KEY = "DriverDesc";
    const string DRIVER_NAME_VALUE = "VX 820 PIN pad";

    #endregion " Members "

    #region " Constructor "

    public PinPadBanorte()
    {      
      // Default type
      m_pinpad_type = PinPadType.BANORTE;
    }

    public PinPadBanorte(PinPadType Type)
    {
      m_pinpad_type = Type;
    }

    #endregion " Constructor "

    #region " Public Methods "

    public void Initialize()
    {
      if (m_pinpad_type == PinPadType.BANORTE)
      {
        m_pinpad_banorte = new PinPadAdapterBanorte();
      }
      else
      {
        throw new Exception("The PindPad type is not valid");
      }
    
    }// Initialize

    public Boolean InitDevice(PinPadCashierTerminal PinPadCashierTerminal)
    {
      return m_pinpad_banorte.PrepareDevice((byte)PinPadCashierTerminal.Port);
    } // InitDevice

    public Boolean ReleaseDevice()
    {
        return m_pinpad_banorte.ReleaseDevice();
    }  // ReleaseDevice

    public void ResetDevice()
    {
      m_pinpad_banorte.ResetDevice();
    } // ResetDevice

    public void StartTransaction()
    {
      m_pinpad_banorte.StartTransaction();
    }

    public void EndTransaction()
    {
      m_pinpad_banorte.EndTransaction();
    }

    public Boolean IsAvailable()
    {
      throw new NotImplementedException();
    } // IsAvailable

    public Boolean Pay(Decimal Amount, String ControlNumber, out PinPadResponse PinPadResponse)
    {
      return m_pinpad_banorte.Pay(Amount, ControlNumber, out PinPadResponse);
    } // Pay

    public bool Read(out PinPadCard PinPadCard)
    {
      return m_pinpad_banorte.Read(out PinPadCard);
    } // Read

    public Boolean Reverse(String ControlNumber, out PinPadResponse ResponseOut)
    {
      return m_pinpad_banorte.Reverse(ControlNumber, out ResponseOut);
    } // Reverse

    public Boolean Cancel(String ControlNumber, out PinPadResponse ResponseOut)
    {
      return m_pinpad_banorte.Cancel(ControlNumber, out ResponseOut);
    } // Cancel

    public bool SendTransaction(PinPadCard PinPadCard, decimal Amount, String ControlNumber, out PinPadResponse PinPadResponse)
    {
      return m_pinpad_banorte.SendTransaction(PinPadCard, Amount, ControlNumber, out PinPadResponse);
    } // SendTransaction

    public bool NotifyResult(PinPadResponse ResponseIn, out PinPadResponse ResponseOut)
    {
      return m_pinpad_banorte.NotifyResult(ResponseIn, out ResponseOut);
    } // NotifyResult

    public Boolean IsDriverInstalled()
    {
      try
      {
        String _key_name = @"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\VeriFone Vx Installer_is1";
        String _value_name = "DisplayName";
        String _display_name = "VeriFone Vx Installer";
        String _value_data = String.Empty;

        _value_data = RegistryWOW6432.GetRegKey64(RegHive.HKEY_LOCAL_MACHINE, _key_name, _value_name); // Read 64

        if (String.IsNullOrEmpty(_value_data))
        {
          _value_data = RegistryWOW6432.GetRegKey32(RegHive.HKEY_LOCAL_MACHINE, _key_name, _value_name); // Read 32

          if (String.IsNullOrEmpty(_value_data))
          {
            return false;
          }
        }

        if (_value_data.Contains(_display_name))
        {

          if (!IsUSBInstalled())
          {
            Log.Warning("The Pin Pad terminal has not connected to the USB port.");
          }

          return true;
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
      return false;
    } // IsDriverInstalled

    public Boolean InstallDriver()
    {
      if (IsDriverInstalled())
      {
        return true;
      }

      Stream _stream = _stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("WSI.PinPad.Banorte.instalation_package.zip");

      ZipFile _zf = null;
      string AppData = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "WigosTemp");
      string _extractionroot = Path.Combine(AppData, "icar_driver_package");

      try
      {
        _zf = new ZipFile(_stream);
        foreach (ZipEntry _zip_entry in _zf)
        {
          if (!_zip_entry.IsFile)
          {
            continue; // Ignore directories
          }
          String _entry_file_name = _zip_entry.Name;

          byte[] _buffer = new byte[4096]; // 4K is optimum
          Stream _zip_stream = _zf.GetInputStream(_zip_entry);

          String _full_zip_to_path = Path.Combine(_extractionroot, _entry_file_name);
          string _directory_name = Path.GetDirectoryName(_full_zip_to_path);
          if (!string.IsNullOrEmpty(_directory_name))
            Directory.CreateDirectory(_directory_name);

          using (FileStream _stream_writer = File.Create(_full_zip_to_path))
          {
            StreamUtils.Copy(_zip_stream, _stream_writer, _buffer);
          }
        }
      }
      finally
      {
        if (_zf != null)
        {
          _zf.IsStreamOwner = true;
          _zf.Close();

        }
      }
      ProcessStartInfo _start_info = new ProcessStartInfo
      {
        WorkingDirectory = _extractionroot,
        FileName = _extractionroot + "\\install.bat"
      };

      try
      {
        new FileIOPermission(FileIOPermissionAccess.Read, _start_info.FileName).Demand();

        Process _proc = Process.Start(_start_info);
        if (_proc == null)
        {
          return false;
        }
        _proc.WaitForExit((60 * 1000) * 7);
        //Directory.Delete(_extractionroot, true);
      }
      catch (SecurityException)
      {
        //
      }
      return IsDriverInstalled();

    } //InstallDriver

    public PinPadType GetPinPadType()
    {
      return m_pinpad_type;
    } //GetPinPadType 

    public String GetPinPadMode()
    {
      return m_pinpad_banorte.GetPinPadMode();
    }

    public String GetPinPadTerminalID()
    {
      return m_pinpad_banorte.GetPinPadTerminalId();
    } //GetPinPadTerminalID

    public void SetPinPadTerminalID(String TerminalID)
    {
      m_pinpad_banorte.SetPinPadTerminalId(TerminalID);
    } //SetPinPadTerminalID

    #endregion " Public Methods "

    #region " Private Methods "

    private bool IsUSBInstalled()
    {
      try
      {
        RegistryKey _usb_devices =
          Registry.LocalMachine.OpenSubKey(@"SYSTEM\ControlSet001\Control\Class\{4D36E978-E325-11CE-BFC1-08002BE10318}");

        if (_usb_devices == null)
        {
          return false;
        }
        foreach (string _reg_key_names in _usb_devices.GetSubKeyNames())
        {
          RegistryKey _sub_key = null;
          _sub_key = _usb_devices.OpenSubKey(_reg_key_names);

          if (_sub_key != null)
          {
            string _device_name = _sub_key.GetValue("DriverDesc") as string;

            if (!string.IsNullOrEmpty(_device_name) & _device_name.Contains("VX 820 PIN pad"))
            {
              return true;
            }
          }
        }
      }
      catch 
      {
      }
      return false;
    }

    #endregion
  }
}
