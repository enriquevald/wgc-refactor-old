﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : PinPadAdapterBanorte.cs
// 
//   DESCRIPTION : Class that implement the communication with the Verifone VX820 pinpad
//
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 07-JUN-2016 AMF    First release
// 29-SEP-2016 ETP    Fixed Bug Localization Isssues in error messages and PinPad.
// 06-OCT-2016 FAV    Fixed Bug 18280: Televisa: Add 'Canceled' status for TPV transactions. Messages fixed
// 26-OCT-2016 ETP    Fixed Bug 19731: Return states of pinpad transactions are incorrect.
// 26-OCT-2016 ETP    Fixed Bug 19731 Pin Pad recharge status update
// 28-OCT-2016 ETP    Fixed Bug 19731: Bad enabled return.
// 08-NOV-2016 ETP    Fixed Bug 20035: Exception out of memory when pinpad is used.
// 03-FEB-2016 ETP    Fixed Bug 24146: Not controlled exception When PinPad is enabled and disconected
// 21-MAY-2017 ETP    Fixed Bug WIGOS-2997: Added voucher data.
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Threading;
using WSI.Common;
using WSI.Common.PinPad;
using WSI.PinPad.Banorte.Adapter;


namespace WSI.PinPad.Banorte.Adapter
{
  internal class PinPadAdapterBanorte
  {

    #region " Members "

    private const string MODEL_CONFIGURATION_VALUE = "001"; // For production use "PRD". For Test use "AUT", "DEC" OR "RND"

    private Byte m_port_number;
    private String m_terminal_id;
    private IPinPadBanorteAdapter m_pinpad_vx820;
    private PinPadConfig m_pinpad_config;


    #endregion " Members "

    #region " Constructor "

    public PinPadAdapterBanorte()
    {
      Type _adapter_type = Type.GetTypeFromProgID("WSI.PinPad.Banorte.Adapter.PinPadBanorteAdapter");
      if (_adapter_type != null)
      {
        object myClassAdapterInstance = Activator.CreateInstance(_adapter_type);

        m_pinpad_vx820 = (IPinPadBanorteAdapter)myClassAdapterInstance;
        m_pinpad_config = new PinPadConfig(MODEL_CONFIGURATION_VALUE);
      }
      else
      {
        Log.Error(" The COM library for interop (PinPadBanorteAdapter) is not installed in the Cashier");
      }
    }

    #endregion " Constructor "

    #region " Public Methods "

    /// <summary>
    /// Init device and open port comm
    /// </summary>
    /// <param name="COMport">COM port number</param>
    /// <returns></returns>
    public Boolean PrepareDevice(Byte COMport)
    {
      Hashtable _config;

      try
      {
        m_port_number = COMport;

        _config = new Hashtable();
        _config.Add(m_pinpad_config.Atributes["PORT"], "COM" + m_port_number);
        _config.Add(m_pinpad_config.Atributes["BAUD_RATE"], "19200");
        _config.Add(m_pinpad_config.Atributes["PARITY"], "N");
        _config.Add(m_pinpad_config.Atributes["STOP_BITS"], "1");
        _config.Add(m_pinpad_config.Atributes["DATA_BITS"], "8");
        _config.Add("RESPONSE_LANGUAGE", m_pinpad_config.Language);

        // Open
        m_pinpad_vx820.prepareDevice(_config);

        Hashtable _output_parameters = new Hashtable();
        m_pinpad_vx820.getInformation(out _output_parameters);
        m_terminal_id = (String)_output_parameters[m_pinpad_config.Atributes["SERIAL_NUMBER"]];

        return true;
      }
      catch (Exception ex)
      {
        if (m_pinpad_vx820 != null)
        {
          ReleaseDevice();
        }
        Log.Exception(ex);
      }

      return false;
    }

    /// <summary>
    /// Release device and communication port
    /// </summary>
    /// <returns></returns>
    public Boolean ReleaseDevice()
    {
      try
      {

        if (m_pinpad_vx820 != null)
        {
          m_pinpad_vx820.releaseDevice();
        }

        m_pinpad_vx820 = null;
        GC.Collect();

        return true;
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }

      return false;
    }

    /// <summary>
    /// Gets PinPad TerminalId
    /// </summary>
    /// <returns></returns>
    public String GetPinPadTerminalId()
    {
      return m_terminal_id;
    }

    public String GetPinPadMode()
    {
      return m_pinpad_config.ModeOperation;
    }


    /// <summary>
    /// Set PinPad TerminalId
    /// </summary>
    /// <param name="TerminalID"></param>
    public void SetPinPadTerminalId(String TerminalID)
    {
      m_terminal_id = TerminalID;
    }

    /// <summary>
    /// Start a transaction
    /// </summary>
    public void StartTransaction()
    {
      try
      {
        m_pinpad_vx820.startTransaction();
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
    }

    /// <summary>
    /// End the current transaction
    /// </summary>
    public void EndTransaction()
    {
      try
      {
        m_pinpad_vx820.endTransaction();
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
    }

    /// <summary>
    /// Reset Device Vx820
    /// </summary>
    /// <returns></returns>
    public void ResetDevice()
    {
      try
      {
        m_pinpad_vx820.endTransaction();
        ReleaseDevice();
        PrepareDevice(m_port_number);
      }
      catch
      {
      }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="Amount"></param>
    /// <param name="ControlNumber"></param>
    /// <param name="ResponseOut"></param>
    /// <returns></returns>
    public Boolean Pay(Decimal Amount, String ControlNumber, out PinPadResponse ResponseOut)
    {
      Hashtable _input_parameters = new Hashtable();
      Hashtable _output_parameters = new Hashtable();
      String _declinada_offline;
      String _amount;
      StringBuilder _temp;

      PinPadTransactions.STATUS _status;
      String _message;
      Boolean _return;

      ResponseOut = new PinPadResponse();
      ResponseOut.StatusResponse = PinPadTransactions.STATUS.NO_RESPONSE;

      _return = false;

      try
      {
        m_pinpad_vx820.startTransaction();

        _amount = Amount.ToString(CultureInfo.GetCultureInfo("en-GB"));

        _input_parameters.Add(m_pinpad_config.Atributes["CMD_TRANS"], m_pinpad_config.Atributes["AUTH"]);
        _input_parameters.Add(m_pinpad_config.Atributes["CONTROL_NUMBER"], ControlNumber);
        _input_parameters.Add(m_pinpad_config.Atributes["AMOUNT"], _amount);

        AddCommonParameters(ref _input_parameters);

        m_pinpad_vx820.displayText(WSI.Common.Resource.String("STR_PINPAD_AMOUNT", _amount));

        m_pinpad_vx820.processTransaction(_input_parameters, out _output_parameters);

        _declinada_offline = (String)_output_parameters[m_pinpad_config.Atributes["CHIP_DECLINED"]];

        _message = String.Empty;


        if (String.Equals(_declinada_offline, "1"))
        {
          _status = PinPadTransactions.STATUS.DECLINED;
          _message = WSI.Common.Resource.String("STR_PINPAD_REJECTED_OFFLINE");
        }
        else
        {
          HashtableToOutput(_output_parameters, ref ResponseOut);


          // ETP 20170621 - Log for view exact bank answer
          _temp = new StringBuilder();
          _temp.Append("PinPadAdapterBanorte-->Pay-->Response: -->");

          foreach (DictionaryEntry entry in _output_parameters)
          {
            _temp.AppendLine();
            _temp.Append((String)entry.Key);
            _temp.Append(" - ");
            _temp.Append((String)entry.Value);
          }

          Misc.WriteLog(_temp.ToString(), Log.Type.Message);
          
          switch (ResponseOut.ResponsePay)
          {
            case "A":
              _status = PinPadTransactions.STATUS.PENDING_WIGOS;
              _message = WSI.Common.Resource.String("STR_PINPAD_APPROVED", ResponseOut.AuthorizationCode);
              ResponseOut.ResponseMessage = String.Empty;
              _return = true;
              break;
            case "D":
              _status = PinPadTransactions.STATUS.DECLINED;
              _message = WSI.Common.Resource.String("STR_PINPAD_DECLINED");
              break;
            case "R":
              _status = PinPadTransactions.STATUS.REJECTED;
              _message = WSI.Common.Resource.String("STR_PINPAD_REJECTED");
              break;
            case "T":
              _status = PinPadTransactions.STATUS.NO_RESPONSE;
              _message = WSI.Common.Resource.String("STR_PINPAD_TIMEOUT");
              break;
            default:
              _status = PinPadTransactions.STATUS.DECLINED;
              _message = WSI.Common.Resource.String("STR_PINPAD_DECLINED");
              Log.Warning("PinPadAdapterBanorte: Pay - new Pinpad Code: " + ResponseOut.ResponsePay);
              break;
          }

        }
        ResponseOut.StatusResponse = _status;
        m_pinpad_vx820.displayText(_message);
      }
      catch (Exception ex)
      {
        ResponseOut.ResponseMessage = ConvertSpanishTextToUTF8(ex.Message);
        ResponseOut.PinPadCard = new PinPadCard();

        Log.Exception(ex);
      }
      finally
      {
        m_pinpad_vx820.endTransaction();
      }

      return _return;
    }

    private void HashtableToOutput(Hashtable HashTable, ref PinPadResponse ResponseOut)
    {
      String _temp_pin_entry;

      ResponseOut.AuthorizationCode = (String)HashTable[m_pinpad_config.Atributes["AUTH_CODE"]];
      ResponseOut.Reference = (String)HashTable[m_pinpad_config.Atributes["REFERENCE"]];
      ResponseOut.ResponseDateTime = DateTime.ParseExact(HashTable[m_pinpad_config.Atributes["CUST_RSP_DATE"]].ToString(), "yyyyMMdd HH:mm:ss.fff", CultureInfo.InvariantCulture);

      ResponseOut.ResponsePay = (String)HashTable[m_pinpad_config.Atributes["PAYW_RESULT"]];
      ResponseOut.ControlNumber = (String)HashTable[m_pinpad_config.Atributes["CONTROL_NUMBER"]];
      ResponseOut.MerchandId = HashTable[m_pinpad_config.Atributes["MERCHANT_ID"]].Equals(null) ? (String)HashTable["MERCHANT_ID"] : m_pinpad_config.MerchandId;

      ResponseOut.ResponseMessage = ConvertSpanishTextToUTF8((String)HashTable[m_pinpad_config.Atributes["TEXT"]]);

      ResponseOut.ExpirationDate = (String)HashTable[m_pinpad_config.Atributes["CARD_EXP"]];
      ResponseOut.ProductType = (String)HashTable[m_pinpad_config.Atributes["CARD_TYPE"]];
      ResponseOut.CardType = (String)HashTable[m_pinpad_config.Atributes["CARD_BRAND"]];

      ResponseOut.AID = (String)HashTable[m_pinpad_config.Atributes["AID"]];
      ResponseOut.TVR = (String)HashTable[m_pinpad_config.Atributes["TVR"]];
      ResponseOut.TSI = (String)HashTable[m_pinpad_config.Atributes["TSI"]];
      ResponseOut.APN = (String)HashTable[m_pinpad_config.Atributes["APN"]];
      ResponseOut.AL = (String)HashTable[m_pinpad_config.Atributes["AL"]];

      _temp_pin_entry = (String)HashTable[m_pinpad_config.Atributes["PIN_ENTRY"]];

      if (!String.IsNullOrEmpty(_temp_pin_entry))
      {
        ResponseOut.WithPin = _temp_pin_entry.Equals("1") ? true : false;
      }




      ResponseOut.PinPadCard = new PinPadCard()
      {
        CardNumber = (String)HashTable[m_pinpad_config.Atributes["CARD_NUMBER"]],
        CardHolder = (String)HashTable[m_pinpad_config.Atributes["CARD_HOLDER"]],
        CardBankName = (String)HashTable[m_pinpad_config.Atributes["ISSUING_BANK"]],
        POSEntryMode = (String)HashTable[m_pinpad_config.Atributes["ENTRY_MODE"]],
        TagsEMV = (String)HashTable[m_pinpad_config.Atributes["EMV_TAGS"]],
        Track1 = null,
        Track2 = null,
      };
    }

    public Boolean Read(out PinPadCard PinPadCard)
    {
      Hashtable _read_input = new Hashtable();
      Hashtable _read_output = new Hashtable();
      String _declinada_offline;

      PinPadCard = new PinPadCard();

      try
      {
        _read_input = new Hashtable();
        _read_input.Add("AMOUNT", "0"); // Amount = 0, The Amount will be send with the sendTransaction method.
        _read_input.Add("PAGO_MOVIL", "0");

        m_pinpad_vx820.readCard(_read_input, out _read_output);

        _declinada_offline = (String)_read_output["CHIP_DECLINED"];
        if (String.Equals(_declinada_offline, "1"))
        {
          m_pinpad_vx820.displayText(WSI.Common.Resource.String("STR_PINPAD_REJECTED"));
        }
        else
        {
          PinPadCard.CardNumber = (String)_read_output[m_pinpad_config.Atributes["CARD_NUMBER"]];
          PinPadCard.CardHolder = (String)_read_output[m_pinpad_config.Atributes["CARD_HOLDER"]];
          PinPadCard.TagsEMV = (String)_read_output[m_pinpad_config.Atributes["EMV_TAGS"]];
          PinPadCard.Track1 = (String)_read_output[m_pinpad_config.Atributes["TRACK1"]];
          PinPadCard.Track2 = (String)_read_output[m_pinpad_config.Atributes["TRACK2"]];
          PinPadCard.POSEntryMode = (String)_read_output[m_pinpad_config.Atributes["ENTRY_MODE"]];
          return true;
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }

      return false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="PinPadCard"></param>
    /// <param name="Amount"></param>
    /// <param name="ControlNumber"></param>
    /// <param name="PinPadResponse"></param>
    /// <returns></returns>
    public Boolean SendTransaction(PinPadCard PinPadCard, Decimal Amount, String ControlNumber, out PinPadResponse PinPadResponse)
    {
      Hashtable _input_parameters = new Hashtable();
      Hashtable _output_parameters = new Hashtable();
      String _amount;

      PinPadResponse = new PinPadResponse();
      PinPadResponse.StatusResponse = PinPadTransactions.STATUS.NO_RESPONSE;

      try
      {

        _amount = Amount.ToString(CultureInfo.GetCultureInfo("en-GB"));

        _input_parameters.Add("CMD_TRANS", "AUTH");
        _input_parameters.Add("TRACK2", PinPadCard.Track2);
        _input_parameters.Add("AMOUNT", _amount);
        _input_parameters.Add("ENTRY_MODE", PinPadCard.POSEntryMode);
        _input_parameters.Add("CONTROL_NUMBER", ControlNumber);


        AddCommonParameters(ref _input_parameters);

        if (PinPadCard.POSEntryMode == "CHIP")
          _input_parameters.Add("EMV_TAGS", PinPadCard.TagsEMV);
        else if (PinPadCard.Track1 != "")
        {
          _input_parameters.Add("TRACK1", PinPadCard.Track1);
        }

        m_pinpad_vx820.displayText(WSI.Common.Resource.String("STR_PINPAD_AMOUNT", _amount));
        m_pinpad_vx820.sendTransaction(_input_parameters, out _output_parameters);

        PinPadResponse.AuthorizationCode = (String)_output_parameters[m_pinpad_config.Atributes["AUTH_CODE"]];
        PinPadResponse.Reference = (String)_output_parameters[m_pinpad_config.Atributes["REFERENCE"]];
        PinPadResponse.EMVData = (String)_output_parameters[m_pinpad_config.Atributes["EMV_DATA"]];
        PinPadResponse.ResponsePay = (String)_output_parameters[m_pinpad_config.Atributes["PAYW_RESULT"]];
        PinPadResponse.ControlNumber = (String)_output_parameters[m_pinpad_config.Atributes["CONTROL_NUMBER"]];
        PinPadResponse.ResponseMessage = ConvertSpanishTextToUTF8((String)_output_parameters[m_pinpad_config.Atributes["TEXT"]]);
        PinPadResponse.PinPadCard = PinPadCard;




        return true;
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }

      return false;
    }

    public Boolean NotifyResult(PinPadResponse ResponseIn, out PinPadResponse ResponseOut)
    {
      Hashtable entradaReversa = new Hashtable();
      Hashtable salidaReversa = new Hashtable();
      String codigoBanorte;
      String codigoAut;

      ResponseOut = new PinPadResponse();
      ResponseOut.StatusResponse = PinPadTransactions.STATUS.NO_RESPONSE;

      try
      {
        codigoBanorte = ResponseIn.ResponsePay;
        codigoAut = ResponseIn.AuthorizationCode;

        if (ResponseIn.PinPadCard.POSEntryMode == "CHIP")
        {
          Hashtable parametrosEntradaNotificacion = new Hashtable();
          Hashtable parametrosSalidaNotificacion = new Hashtable();

          if (!String.IsNullOrEmpty(codigoBanorte))
          {
            String _emv_data;
            _emv_data = ResponseIn.EMVData;
            if (!String.IsNullOrEmpty(_emv_data))
            {
              parametrosEntradaNotificacion.Add(m_pinpad_config.Atributes["EMV_DATA"], _emv_data);
            }

            if (codigoBanorte == "A")
            {
              parametrosEntradaNotificacion.Add(m_pinpad_config.Atributes["RESULT"], "APPROVED");
              parametrosEntradaNotificacion.Add("AUTH_CODE", codigoAut);
            }
            else if (codigoBanorte == "D")
            {
              parametrosEntradaNotificacion.Add(m_pinpad_config.Atributes["RESULT"], "DECLINED");
            }
            else
            {
              parametrosEntradaNotificacion.Add(m_pinpad_config.Atributes["RESULT"], "NO_RESPONSE");
            }
          }
          else
          {
            parametrosEntradaNotificacion.Add(m_pinpad_config.Atributes["RESULT"], "NO_RESPONSE");
          }

          m_pinpad_vx820.notifyResult(parametrosEntradaNotificacion, out parametrosSalidaNotificacion);


          String EMVResultado;
          EMVResultado = (String)parametrosSalidaNotificacion["EMV_RESULT"];
          if (EMVResultado == "A" && codigoBanorte == "A")
          {
            // APPROVED
            ResponseOut = (PinPadResponse)ResponseIn.Clone();
            ResponseOut.StatusResponse = PinPadTransactions.STATUS.APPROVED;

            m_pinpad_vx820.displayText(WSI.Common.Resource.String("STR_PINPAD_APPROVED", ResponseIn.AuthorizationCode));
            return true;
          }
          else if (EMVResultado == "D" && codigoBanorte == "A")
          {
            String referencia;

            referencia = ResponseIn.Reference;

            entradaReversa.Add(m_pinpad_config.Atributes["CMD_TRANS"], "REVERSAL");
            entradaReversa.Add(m_pinpad_config.Atributes["REFERENCE"], referencia);
            entradaReversa.Add(m_pinpad_config.Atributes["CAUSE"], "06");

            AddCommonParameters(ref entradaReversa);

            m_pinpad_vx820.sendTransaction(entradaReversa, out salidaReversa);
          }
          else
          {
            //  Rejected
            ResponseOut.StatusResponse = PinPadTransactions.STATUS.DECLINED;
            m_pinpad_vx820.displayText(WSI.Common.Resource.String("STR_PINPAD_REJECTED"));
          }
        }
        else
        {
          if (codigoBanorte == "A")
          {
            ResponseOut = (PinPadResponse)ResponseIn.Clone();
            ResponseOut.StatusResponse = PinPadTransactions.STATUS.APPROVED;

            m_pinpad_vx820.displayText(WSI.Common.Resource.String("STR_PINPAD_APPROVED", ResponseIn.AuthorizationCode));
            return true; // Approved
          }
          else
          {
            ResponseOut.StatusResponse = PinPadTransactions.STATUS.DECLINED;
            m_pinpad_vx820.displayText(WSI.Common.Resource.String("STR_PINPAD_REJECTED"));
          }
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }

      return false;
    }

    public Boolean Reverse(String ControlNumber, out PinPadResponse ResponseOut)
    {
      Hashtable _input_parameters = new Hashtable();
      Hashtable _output_parameters = new Hashtable();
      String _declinada_offline;
      String _codigo_banorte;

      ResponseOut = new PinPadResponse();
      ResponseOut.StatusResponse = PinPadTransactions.STATUS.NO_RESPONSE;

      try
      {
        _input_parameters.Add(m_pinpad_config.Atributes["CMD_TRANS"], "REVERSAL");
        _input_parameters.Add(m_pinpad_config.Atributes["CONTROL_NUMBER"], ControlNumber);



        AddCommonParameters(ref _input_parameters);

        m_pinpad_vx820.sendTransaction(_input_parameters, out _output_parameters);

        _declinada_offline = (String)_output_parameters[m_pinpad_config.Atributes["CHIP_DECLINED"]];
        _codigo_banorte = (String)_output_parameters[m_pinpad_config.Atributes["PAYW_RESULT"]];

        if (String.Equals(_codigo_banorte, "A"))
        {
          HashtableToOutput(_output_parameters, ref ResponseOut);

          return true;
        }
        else
        {
          ResponseOut.ResponseMessage = ConvertSpanishTextToUTF8((String)_output_parameters[m_pinpad_config.Atributes["TEXT"]]);
          if (ResponseOut.ResponseMessage.Contains("has been previously reversed"))
          {
            ResponseOut.StatusResponse = PinPadTransactions.STATUS.APPROVED;
            return true;
          }
          if (ResponseOut.ResponseMessage.Contains("does not exist"))
          {
            ResponseOut.StatusResponse = PinPadTransactions.STATUS.APPROVED;
            return true;
          }

          ResponseOut.StatusResponse = PinPadTransactions.STATUS.DECLINED;
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }

      return false;
    }

    public Boolean Cancel(String ControlNumber, out PinPadResponse ResponseOut)
    {
      Hashtable _input_parameters = new Hashtable();
      Hashtable _output_parameters = new Hashtable();
      String _declinada_offline;
      String _codigo_banorte;

      ResponseOut = new PinPadResponse();
      ResponseOut.StatusResponse = PinPadTransactions.STATUS.NO_RESPONSE;

      try
      {
        _input_parameters.Add(m_pinpad_config.Atributes["CMD_TRANS"], "VOID");
        _input_parameters.Add(m_pinpad_config.Atributes["CONTROL_NUMBER"], ControlNumber);

        AddCommonParameters(ref _input_parameters);

        m_pinpad_vx820.processTransaction(_input_parameters, out _output_parameters);

        _declinada_offline = (String)_output_parameters[m_pinpad_config.Atributes["CHIP_DECLINED"]];

        if (String.Equals(_declinada_offline, "1"))
        {
          ResponseOut.StatusResponse = PinPadTransactions.STATUS.DECLINED;
        }
        else
        {
          _codigo_banorte = (String)_output_parameters[m_pinpad_config.Atributes["PAYW_RESULT"]];

          if (String.Equals(_codigo_banorte, "A"))
          {
            HashtableToOutput(_output_parameters, ref ResponseOut);

            return true;
          }
          else
          {
            ResponseOut.StatusResponse = PinPadTransactions.STATUS.DECLINED;
          }
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }

      return false;
    }

    public Boolean IsDriverInstalled()
    {
      return m_pinpad_vx820.IsDriverInstalled();
    }

    public Boolean InstallDriver()
    {
      return m_pinpad_vx820.InstallDriver();

    }

    #endregion " Public Methods "

    #region " Private Methods "

    private void AddCommonParameters(ref Hashtable Hashtable)
    {
      m_pinpad_config.Load(MODEL_CONFIGURATION_VALUE); // Read General Params.

      Hashtable.Add(m_pinpad_config.Atributes["MERCHANT_ID"], m_pinpad_config.MerchandId);
      Hashtable.Add(m_pinpad_config.Atributes["USER"], m_pinpad_config.User);
      Hashtable.Add(m_pinpad_config.Atributes["PASSWORD"], m_pinpad_config.Password);
      Hashtable.Add(m_pinpad_config.Atributes["TERMINAL_ID"], m_terminal_id);
      Hashtable.Add(m_pinpad_config.Atributes["MODE"], m_pinpad_config.ModeOperation);
      Hashtable.Add(m_pinpad_config.Atributes["RESPONSE_LANGUAGE"], m_pinpad_config.Language);
      Hashtable.Add(m_pinpad_config.Atributes["BANORTE_URL"], m_pinpad_config.URL);
      Hashtable.Add(m_pinpad_config.Atributes["TRANS_TIMEOUT"], "30");

    }

    private string ConvertSpanishTextToUTF8(string Text)
    {
      try
      {
        if (m_pinpad_config.Language == "ES")
        {
          byte[] bytes = Encoding.Default.GetBytes(Text);
          return Encoding.UTF8.GetString(bytes);
        }
        else
        {
          return Text;
        }
      }
      catch
      {
        return Text;
      }
    }

    #endregion " Private Methods "
  }
}

