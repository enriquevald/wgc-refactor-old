﻿//------------------------------------------------------------------------------
// Copyright © 2015-2020 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: PinPadCommon.cs
//  
//   DESCRIPTION: Class that implement methods to expose in the WCF web service
// 
//        AUTHOR: Francisco Vicente
// 
// CREATION DATE: 15-JUN-2016
//
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 15-JUN-2016 FAV     First release.
// 15-JUN-2016 ETP    Product Backlog Item 14340: Instalación del sofware para PinPad
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using WSI.Common;
using WSI.Common.PinPad;

namespace WSI.PinPad
{
  public class PinPadCommon: IPinPad
  {
    private static IPinPad m_pin_pad;

    public void Initialize(PinPadType Type)
    {
      switch (Type)
      {
        case PinPadType.BANORTE:
          m_pin_pad = new PinPadConnectorBanorte();
          break;
        case PinPadType.DUMMY:
          m_pin_pad = new PinPadDummy();
          break;
        default:
          throw new Exception("The PinPad type had not been defined");
      }
    }

    public bool InitDevice()
    {
      if (CheckType())
        return m_pin_pad.InitDevice();
      
      return false;
    }

    public bool ReleaseDevice()
    {
      if (CheckType())
        return m_pin_pad.ReleaseDevice();
      
      return false;
    }

    public void ResetDevice()
    {
      if (CheckType())
        m_pin_pad.ResetDevice();

      return;
    }

    public void StartTransaction()
    {
      if (CheckType())
        m_pin_pad.StartTransaction();
      
      return;
    }

    public void EndTransaction()
    {
      if (CheckType())
        m_pin_pad.EndTransaction(); 
      
      return;
    }

    public bool IsAvailable()
    {
      if (CheckType())
        return m_pin_pad.IsAvailable();
     
      return false;
    }

    public bool Pay(decimal Amount, out PinPadResponse PinPadResponse)
    {
      PinPadResponse = null;

      if (CheckType())
        return m_pin_pad.Pay(Amount, out PinPadResponse);

      return false;
    }

    public bool Read(out PinPadCard PinPadCard)
    {
      PinPadCard = null;

      if (CheckType())
        return m_pin_pad.Read(out PinPadCard);
       
      return false;
    }

    public bool SendTransaction(PinPadCard PinPadCard, decimal Amount, out PinPadResponse PinPadResponse)
    {
      PinPadResponse = null;

      if (CheckType())
        return m_pin_pad.SendTransaction(PinPadCard, Amount, out PinPadResponse);
       
      return false;
    }

    public bool NotifyResult(PinPadResponse ResponseIn, out PinPadResponse ResponseOut)
    {
      ResponseOut = null;

      if (CheckType())
        return m_pin_pad.NotifyResult(ResponseIn, out ResponseOut);
      
      return false;
    }

    public bool CancelPay()
    {
      if (CheckType())
        return m_pin_pad.CancelPay();
       
      return false;
    }

    public bool IsDriverInstalled()
    {
      if (CheckType())
        return m_pin_pad.IsDriverInstalled();
      
      return false;
    }

    public bool InstallDriver()
    {
      if (CheckType())
        return m_pin_pad.InstallDriver();
        
      return false;
    }

    private Boolean CheckType()
    {
      if (m_pin_pad == null)
      {
        Log.Error("Pin pad type not defined");
        return false;
      }

      return true;
    }
  }
}
