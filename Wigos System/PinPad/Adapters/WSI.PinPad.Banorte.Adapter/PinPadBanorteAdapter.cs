﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : PinPadBanorteAdapter.cs
// 
//   DESCRIPTION : Class in .Net 4.0 that can be called from .Net 2.0
//
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 22-JUN-2016 FAV    First release
// 29-SEP-2016 ETP    Fixed Bug Localization Isssues in error messages and PinPad.
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;

using Banorte.PinPad;
using Banorte;
using System.Runtime.InteropServices;
using System.IO;
using System.Reflection;
using ICSharpCode.SharpZipLib.Zip;
using ICSharpCode.SharpZipLib.Core;
using System.Diagnostics;
using System.Security.Permissions;
using System.Security;
using Microsoft.Win32;

namespace WSI.PinPad.Banorte.Adapter
{
  [ComVisible(true)]
  [Guid("9DC30415-F1B8-44D8-BE43-3798BDEAF560")]
  public class PinPadBanorteAdapter : IPinPadBanorteAdapter
  {
    #region " Members "

    private Vx820Segura m_pinpad_vx820;

    #endregion " Members "

    #region " Constructor "

    public PinPadBanorteAdapter()
    {
      m_pinpad_vx820 = new Vx820Segura("ES");
    }

    #endregion " Constructor "

    #region " Public Methods "

    public void prepareDevice(Hashtable HashtableIn)
    {
      m_pinpad_vx820 = new Vx820Segura(HashtableIn["RESPONSE_LANGUAGE"].ToString());
      m_pinpad_vx820.prepareDevice(HashtableIn);
    }

    public void getInformation(out Hashtable HashtableOut)
    {
      HashtableOut = new Hashtable();
      m_pinpad_vx820.getInformation(HashtableOut);
    }

    public void releaseDevice()
    {
      m_pinpad_vx820.releaseDevice();
    }

    public void startTransaction()
    {
      m_pinpad_vx820.startTransaction();
    }

    public void endTransaction()
    {
      m_pinpad_vx820.endTransaction();
    }

    public void displayText(string texto)
    {
      m_pinpad_vx820.displayText(texto);
    }

    public void processTransaction(Hashtable HashtableIn, out Hashtable HashtableOut)
    {
      HashtableOut = new Hashtable();
      m_pinpad_vx820.processTransaction(HashtableIn, HashtableOut);
    }

    public void readCard(Hashtable HashtableIn, out Hashtable HashtableOut)
    {
      HashtableOut = new Hashtable();
      m_pinpad_vx820.readCard(HashtableIn, HashtableOut);
    }

    public void sendTransaction(Hashtable HashtableIn, out Hashtable HashtableOut)
    {
      HashtableOut = new Hashtable();
      ConectorBanorte.sendTransaction(HashtableIn, HashtableOut);
    }

    public void notifyResult(Hashtable HashtableIn, out Hashtable HashtableOut)
    {
      HashtableOut = new Hashtable();
      m_pinpad_vx820.notifyResult(HashtableIn, HashtableOut);
    }

    public Boolean IsDriverInstalled()
    {
      String _key_name = "VeriFone Vx Installer_is1";

      RegistryKey _basekey = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry32);
      RegistryKey _regkey = _basekey.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\" + _key_name);

      if (_regkey == null)
      {
        _basekey = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64);
        _regkey = _basekey.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\" + _key_name);
      }

      if (_regkey == null)
      {
        return false;
      }

      String _software_ver = _regkey.GetValue("DisplayVersion") as String;

      if (!String.IsNullOrEmpty(_software_ver) && _software_ver.Contains("1.0.0.52.B5"))
      {
        return true;
      }

      return false;
    } // IsDriverInstalled

    public Boolean InstallDriver()
    {
      if (IsDriverInstalled())
      {
        return true;
      }

      Stream _stream = _stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("WSI.PinPad.Banorte.Adapter.instalation_package.zip");

      ZipFile _zf = null;
      string AppData = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "WigosTemp");
      string _extractionroot = Path.Combine(AppData, "icar_driver_package");

      try
      {
        _zf = new ZipFile(_stream);
        foreach (ZipEntry _zip_entry in _zf)
        {
          if (!_zip_entry.IsFile)
          {
            continue; // Ignore directories
          }
          String _entry_file_name = _zip_entry.Name;

          byte[] _buffer = new byte[4096]; // 4K is optimum
          Stream _zip_stream = _zf.GetInputStream(_zip_entry);

          String _full_zip_to_path = Path.Combine(_extractionroot, _entry_file_name);
          string _directory_name = Path.GetDirectoryName(_full_zip_to_path);
          if (!string.IsNullOrEmpty(_directory_name))
            Directory.CreateDirectory(_directory_name);

          using (FileStream _stream_writer = File.Create(_full_zip_to_path))
          {
            StreamUtils.Copy(_zip_stream, _stream_writer, _buffer);
          }
        }
      }
      finally
      {
        if (_zf != null)
        {
          _zf.IsStreamOwner = true;
          _zf.Close();

        }
      }
      ProcessStartInfo _start_info = new ProcessStartInfo
      {
        WorkingDirectory = _extractionroot,
        FileName = _extractionroot + "\\install.bat"
      };

      try
      {
        new FileIOPermission(FileIOPermissionAccess.Read, _start_info.FileName).Demand();

        Process _proc = Process.Start(_start_info);
        if (_proc == null)
        {
          return false;
        }
        _proc.WaitForExit((60 * 1000) * 7);
        //Directory.Delete(_extractionroot, true);
      }
      catch (SecurityException)
      {
        //
      }
      return IsDriverInstalled();

    } //InstallDriver

    #endregion " Public Methods "
  }
}

