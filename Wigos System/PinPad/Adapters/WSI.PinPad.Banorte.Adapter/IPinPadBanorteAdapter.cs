﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace WSI.PinPad.Banorte.Adapter
{
  [ComVisible(true)]
  [Guid("66BD61A0-449F-40B9-9686-D5D3E7873A87")]
  public interface IPinPadBanorteAdapter
  {
    void prepareDevice(Hashtable HashtableIn);

    void getInformation(out Hashtable HashtableOut);

    void releaseDevice();

    void startTransaction();

    void endTransaction();

    void displayText(string texto);

    void processTransaction(Hashtable HashtableIn, out Hashtable HashtableOut);

    void readCard(Hashtable HashtableIn, out Hashtable HashtableOut);

    void sendTransaction(Hashtable HashtableIn, out Hashtable HashtableOut);

    void notifyResult(Hashtable HashtableIn, out Hashtable HashtableOut);

    Boolean IsDriverInstalled();

    Boolean InstallDriver();
  }
}
