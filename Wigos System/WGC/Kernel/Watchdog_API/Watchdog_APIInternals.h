//------------------------------------------------------------------------------
// Copyright � 2002 Win Systems Ltd.
//------------------------------------------------------------------------------
//
//   MODULE NAME: Watchdog_APIInternals.h
//   DESCRIPTION: Constants, types, variables definitions and prototypes
//                internals for the Watchdog_API
//        AUTHOR: Andreu Juli�
// CREATION DATE: 11-JUL-2002
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ -----------------------------------------------------------
// 11-JUL-2002 AJQ    Initial draft.
// 27-AUG-2002 AJQ    Added the auto query functionality.
//------------------------------------------------------------------------------

#ifndef __WATCHDOG_API_INTERNALS_H
#define __WATCHDOG_API_INTERNALS_H

//------------------------------------------------------------------------------
//  INCLUDES
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PUBLIC CONSTANTS
//------------------------------------------------------------------------------
// API Name
#define MODULE_NAME             _T("WATCHDOG_API")

#define WDOG_MAX_IUD_SIZE        sizeof (WDOG_IUD_QUERY_STATUS)  // Maximum user data size
#define WDOG_AUX_STRING_LENGTH   128

// API threads queue size and number of items
#define WDOG_QUEUE_ITEM_SIZE       sizeof (TYPE_API_REQUEST_PARAMS)  // bytes
#define WDOG_QUEUE_NUMBER_OF_ITEMS 1 // AJQ, 16-FEB-2003, Only 1 item (before -> 32 items)

// MSG
#define WDOG_MSG_CMD(n)     (99990+n)
#define WDOG_MSG_CMD_PING   WDOG_MSG_CMD(1)
#define WDOG_MSG_CMD_PONG   WDOG_MSG_CMD(2)

// Retries
#define WDOG_TIMEOUT_MSEC   20000 // 20 seg.
#define WDOG_MAX_RETRIES    2     // 2 retry + 1st try = 3 tries (60 seg.)

// AutoQuery Interval
#define WDOG_AUTO_QUERY_INTERVAL_MSEC 250 // 0.250 sec.

#define WDOG_AUTO_QUERY_RANGE         1000000 // The AutoQuery makes pings from          0 -   999,999
                                               // The UserQuery makes pings from: 1,000,000 - 1,999,999

#define WDOG_MAX_NO_ANSWER                        3   // Not answered pings 
#define WDOG_INTERVAL_OK_TO_UNKNOWN               5   // sec.
#define WDOG_INTERVAL_UNKNOWN_TO_NOT_RESPONDING   30  // sec.

#define WDOG_PING_INTERVAL_STATUS_OK              2   // sec.
#define WDOG_PING_INTERVAL_STATUS_UNKNOWN         5   // sec.
#define WDOG_PING_INTERVAL_STATUS_NOT_RESPONDING  45  // sec.

#define WDOG_TRICK_UNKNOWN                        1 // 13
#define WDOG_TRICK_NOT_RESPONDING                 1 // 11

#define WDOG_MAX_PING_UNKNOWN                     3
#define WDOG_MAX_PING_NOT_RESPONDING              1


//------------------------------------------------------------------------------
//  PUBLIC DATATYPES
//------------------------------------------------------------------------------
#define WDOG_API_MAX_IUD_SIZE sizeof (WDOG_IUD_QUERY_STATUS)

//------------------------------------------------------------------------------
//  PRIVATE DATATYPES 
//------------------------------------------------------------------------------
typedef struct
{
  API_INPUT_PARAMS      api_input_params;
  API_OUTPUT_PARAMS     * p_api_output_params;
  API_OUTPUT_USER_DATA  * p_api_output_user_data;
  BYTE                  api_input_user_data_buffer [WDOG_API_MAX_IUD_SIZE];

} TYPE_API_REQUEST_PARAMS;

// IPC msg
typedef struct
{
  DWORD             node_instance_id;
  BOOL              rcv_posted;
  HANDLE            ipc_event;
  API_OUTPUT_PARAMS api_output_params;
  IPC_OUD_RECEIVE   ipc_oud_receive;

} TYPE_IPC_DATA;

// Module data
typedef struct
{
  HANDLE            timer_event;

  HANDLE            queue_event;
  QUEUE_HANDLE      request_queue;
  DWORD             request_id;

  TYPE_IPC_DATA     ipc_data;

  BOOL                           auto_query_enabled;    // Indicates whether or not auto query has been requested
  BOOL                           auto_query_executed;   // First auto query executed
  HANDLE                         auto_query_timer;      // The auto query timer 
  // WDOG_OUD_GET_AUTO_QUERY_STATUS auto_get_query_status; // The nodes to be auto queried and stores their state
  WDOG_OUD_GET_AUTO_QUERY_STATUS auto_query_tmp_status; // Temporary status 
  DWORD                          auto_query_request_id; // Current request id

} TYPE_MODULE_DATA;

typedef struct
{
  DWORD         control_block;
  DWORD         ping_time;
  LARGE_INTEGER perf_counter;

} TYPE_PING_PONG_DATA;

//------------------------------------------------------------------------------
//  PUBLIC DATA STRUCTURES
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PUBLIC FUNCTION PROTOTYPES
//------------------------------------------------------------------------------
// DLL Functions
static BOOL Wdog_InitModuleData (VOID);

// Common API Functions
static BOOL         Wdog_CheckMode    (const API_INPUT_PARAMS * pApiInputParams);
static QUEUE_HANDLE Wdog_SelectQueue  (const API_INPUT_PARAMS * pApiInputParams);

// API Functions
static void Wdog_ModuleInit         (TYPE_API_REQUEST_PARAMS * pRequest, WORD * pStatus);
static void Wdog_ModuleStop         (TYPE_API_REQUEST_PARAMS * pRequest, WORD * pStatus);
static void Wdog_QueryStatus        (TYPE_API_REQUEST_PARAMS * pRequest, WORD * pStatus);
static void Wdog_SetAutoQueryStatus (TYPE_API_REQUEST_PARAMS * pRequest, WORD * pStatus);
static void Wdog_GetAutoQueryStatus (TYPE_API_REQUEST_PARAMS * pRequest, WORD * pStatus);

// API Thread
static DWORD WINAPI Wdog_Thread (LPVOID ThreadParameter);

static BOOL Wdog_PostReceive   (VOID);
static BOOL Wdog_PostQueueRead (TYPE_API_REQUEST_PARAMS * pRequest,
                                DWORD                   * pRequestSize);

static void Wdog_ProcessMessage (TYPE_API_REQUEST_PARAMS * pRequest);
static void Wdog_ProcessRequest (TYPE_API_REQUEST_PARAMS * pRequest);
static void Wdog_RunAutoQuery (VOID);

static BOOL Wdog_SendPingPong  (DWORD               PingPongCmd,
                                DWORD               SourceNodeInstanceId,
                                DWORD               TargetNodeInstanceId,
                                DWORD               PingRequestId,
                                DWORD               PingUserIndex,
                                TYPE_PING_PONG_DATA * pPingPongData);

static VOID Wdog_InitPingPongData (TYPE_PING_PONG_DATA * pPingPongData);

// Timer Functions
static BOOL Wdog_EnableTimer  (HANDLE Timer, DWORD  Milliseconds, TCHAR * pFunctionName);
static BOOL Wdog_DisableTimer (HANDLE Timer, TCHAR * pFunctionName);

#endif // __WATCHDOG_API_INTERNALS_H
