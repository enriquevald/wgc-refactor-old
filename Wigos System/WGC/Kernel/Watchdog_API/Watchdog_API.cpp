//------------------------------------------------------------------------------
// Copyright � 2002 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Watchdog_API.cpp
// 
//   DESCRIPTION: The Watchdog API
// 
//        AUTHOR: Andreu Juli�
// 
// CREATION DATE: 12-JUL-2002
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ -----------------------------------------------------------
// 12-JUL-2002 AJQ    Initial draft.
// 15-FEB-2003 AJQ    Headers / Pass the function_name to static of the most used functions-
// 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  INCLUDES 
//------------------------------------------------------------------------------
#define INCLUDE_API
#define INCLUDE_IPC_API
#define INCLUDE_NLS_API
#define INCLUDE_COMMON_MISC
#define INCLUDE_WATCHDOG_API
#define INCLUDE_PRIVATE_LOG
#include "CommonDef.h"
#include "Watchdog_APIInternals.h"

//------------------------------------------------------------------------------
//  PRIVATE CONSTANTS 
//------------------------------------------------------------------------------
// Module name
#define MODULE_NAME                 _T("WATCHDOG_API")

// Handles
#define HANDLE_IDX_TIMER            0 // When waiting for timer ... no queue read is posted
#define HANDLE_IDX_QUEUE            0
#define HANDLE_IDX_IPC              1
#define HANDLE_IDX_AUTO_QUERY_TIMER 2
#define HANDLE_COUNT                3

// Macros
#define MessageReceived() { GLB_ModuleData.ipc_data.rcv_posted = FALSE; }
#define MessagePosted()   { GLB_ModuleData.ipc_data.rcv_posted = TRUE;  }
#define IsReceivePosted() ( GLB_ModuleData.ipc_data.rcv_posted == TRUE )

//------------------------------------------------------------------------------
//  PRIVATE DATATYPES 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PRIVATE DATA STRUCTURES 
//------------------------------------------------------------------------------
static TYPE_MODULE_DATA GLB_ModuleData;

TYPE_COMMON_API_DATA GLB_API = { MODULE_NAME,         // API Name
                                 WDOG_MAX_IUD_SIZE,   // API Max Input Data Size
                                 FALSE,               // UsePrivateLog
                                 NULL,                // PrivateLog
                                 Common_LoggerMsg,    // Logger
                                 Wdog_CheckMode,      // Wdog_CheckMode Function
                                 Wdog_SelectQueue,    // Wdog_SelectQueue Function
                                 NULL                 // No auto init
                                };

//------------------------------------------------------------------------------
//  PRIVATE FUNCTION PROTOTYPES 
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
//  PUBLIC FUNCTIONS IMPLEMENTATION 
//------------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PURPOSE: Watchdog API public interface
//
//  PARAMS:
//     - INPUT:
//      - pApiInputParams
//      - pApiInputUserData
//     - OUTPUT:
//      - pApiOutputParams
//      - pApiOutputUserData
//
// RETURNS:
//     - API_STATUS_OK
//     - API_STATUS_PARAMETER_ERROR
//     - API_STATUS_CB_SIZE_ERROR
//     - API_STATUS_EVENT_OBJECT_ERROR
//     - API_STATUS_NO_RESOURCES_AVAILABLE
//
// NOTES:
//
WATCHDOG_API WORD WINAPI Wdog_API (API_INPUT_PARAMS     * pApiInputParams,
                                   API_INPUT_USER_DATA  * pApiInputUserData,
                                   API_OUTPUT_PARAMS    * pApiOutputParams,
                                   API_OUTPUT_USER_DATA * pApiOutputUserData)
{
  static TCHAR _function_name[] = _T("Wdog_API");

  return  Common_API_Entry (pApiInputParams,
                            pApiInputUserData,
                            pApiOutputParams,
                            pApiOutputUserData,
                            _function_name);
} // Wdog_API

//-----------------------------------------------------------------------------
// PURPOSE: DLL standard main function.
//
//  PARAMS:
//     - INPUT: None
//
//     - OUTPUT: None
//
//
// RETURNS:
//     - TRUE: OK
//     - FALSE: Force dll unload
//
// NOTES:
//
BOOL APIENTRY DllMain (HANDLE Module, 
                       DWORD  ReasonForCall, 
                       LPVOID Reserved)
{
  BOOL   _bool_rc;
  
  switch ( ReasonForCall )
  {
    case DLL_PROCESS_ATTACH:

      // DLL load action
      memset (&GLB_ModuleData, 0, sizeof (GLB_ModuleData));

      // Data
      _bool_rc = Wdog_InitModuleData ();
      
      if ( _bool_rc == TRUE )
      {
        // API Threads
        _bool_rc = Common_API_StartThread (Wdog_Thread,     // Thread routine
                                           &GLB_ModuleData, // Thread parameter
                                           NULL,            // Start notify event
                                           _T("Wdog_Thread"));
      }

      if ( _bool_rc == FALSE )
      {
        // Critical error
        // Force to unload DLL
        return FALSE;
      }

    break;

    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
    case DLL_PROCESS_DETACH:
      // Empty
    break;

  } // switch

  return TRUE;
} // DllMain

//------------------------------------------------------------------------------
//  PRIVATE FUNCTIONS IMPLEMENTATION 
//------------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PURPOSE: Initializes the module on process attach
//
//  PARAMS:
//     - INPUT:  None
//     - OUTPUT: None
//
// RETURNS:
//     - TRUE, on success
//     - FALSE, otherwise
//
// NOTES:
//
static BOOL Wdog_InitModuleData (VOID)
{
  TCHAR   _function_name [] = _T("Wdog_InitModuleData");
  TCHAR   _aux_tchar_1 [WDOG_AUX_STRING_LENGTH];
  WORD    _call_status;

  // Queue event
  GLB_ModuleData.queue_event = CreateEvent (NULL,  // Event attributes   
                                            FALSE, // ManualReset
                                            FALSE, // InitialState
                                            NULL); // Name
  if ( GLB_ModuleData.queue_event == NULL )
  {
    // Logger message
    _stprintf (_aux_tchar_1, _T("%lu"), GetLastError ());
    Common_Logger (NLS_ID_LOGGER(2), _aux_tchar_1, _T("CreateEvent"), _T(""));

    return FALSE;
  }

  // IPC Receive event
  GLB_ModuleData.ipc_data.ipc_event =  CreateEvent (NULL,  // Event attributes   
                                                    FALSE, // ManualReset
                                                    FALSE, // InitialState
                                                    NULL); // Name

  if ( GLB_ModuleData.ipc_data.ipc_event == NULL )
  {
    // Logger message
    _stprintf (_aux_tchar_1, _T("%lu"), GetLastError ());
    Common_Logger (NLS_ID_LOGGER(2), _aux_tchar_1, _T("CreateEvent"), _T(""));

    return FALSE;
  }

  // Queues
  _call_status = Queue_CreateQueue (WDOG_QUEUE_NUMBER_OF_ITEMS * WDOG_QUEUE_ITEM_SIZE,
                                    &GLB_ModuleData.request_queue);
  
  if ( _call_status != QUEUE_STATUS_OK )
  {
    // Logger message
    _stprintf (_aux_tchar_1, _T("%hu"), _call_status);
    Common_Logger (NLS_ID_LOGGER(2), _aux_tchar_1, _T("Queue_CreateQueue"), _T(""));

    return FALSE;
  }

  // Timer
  GLB_ModuleData.timer_event = CreateWaitableTimer (NULL,   // Attributes
                                                    TRUE,   // ManualReset
                                                    NULL);  // Name
  
  if ( GLB_ModuleData.timer_event == NULL )
  {
    // Logger message
    _stprintf (_aux_tchar_1, _T("%lu"), GetLastError ());
    Common_Logger (NLS_ID_LOGGER(2), _aux_tchar_1, _T("CreateWaitableTimer"), _T(""));

    return FALSE;
  }

  // Auto Query Data
  GLB_ModuleData.auto_query_enabled  = FALSE;
  GLB_ModuleData.auto_query_executed = FALSE;
  GLB_ModuleData.auto_query_timer    = CreateWaitableTimer (NULL,   // Attributes
                                                            TRUE,   // ManualReset
                                                            NULL);  // Name
  if ( GLB_ModuleData.auto_query_timer == NULL )
  {
    // Logger message
    _stprintf (_aux_tchar_1, _T("%lu"), GetLastError ());
    Common_Logger (NLS_ID_LOGGER(2), _aux_tchar_1, _T("CreateWaitableTimer"), _T(""));

    return FALSE;
  }

  return TRUE;
} // Wdog_InitModuleData

//------------------------------------------------------------------------------
// PURPOSE: API thread to handle user requests.
//
//  PARAMS:
//      - INPUT:
//        - ThreadParameter
//      - OUTPUT: None
//
// RETURNS:
//
//   NOTES:
//
static DWORD WINAPI Wdog_Thread (LPVOID ThreadParameter)
{
  static TCHAR              _function_name[] = _T("Wdog_Thread");
  TCHAR                     _aux_tchar_1[WDOG_AUX_STRING_LENGTH];
  HANDLE                    _handles [HANDLE_COUNT];
  DWORD                     _wait_status;
  TYPE_API_REQUEST_PARAMS   _request;
  DWORD                     _request_size;
  TYPE_MODULE_DATA          * p_module_data;
  
  p_module_data = (TYPE_MODULE_DATA *) ThreadParameter;

  // Events
  _handles[HANDLE_IDX_QUEUE]            = p_module_data->queue_event;
  _handles[HANDLE_IDX_IPC]              = p_module_data->ipc_data.ipc_event;
  _handles[HANDLE_IDX_AUTO_QUERY_TIMER] = p_module_data->auto_query_timer;

  // First call to read queue
  if ( Wdog_PostQueueRead (&_request, &_request_size) == FALSE )
  {
    return 0;
  }
  
  // Don't post a receive till the module has been initialized!!!

  //// Receive IPC message
  //if ( Wdog_PostReceive () == FALSE )
  //{
  //  return 0;
  //}

  // Infinite loop to handle requests and events
  while ( TRUE ) 
  {
    // Wait with using the private event
    _wait_status = WaitForMultipleObjects (HANDLE_COUNT,  // Count
                                           _handles,      // Handles
                                           FALSE,         // WaitAll
                                           INFINITE);     // Milliseconds
    switch ( _wait_status )
    {
      case WAIT_OBJECT_0 + HANDLE_IDX_QUEUE:
      {
        // Request received:
        //  - Process Request
        Wdog_ProcessRequest (&_request);

        // Call again to read snd queue
        if ( Wdog_PostQueueRead (&_request, &_request_size) == FALSE )
        {
          return 0;
        }
      }
      break;

      case WAIT_OBJECT_0 + HANDLE_IDX_IPC:
      {
        MessageReceived ();

        if ( ! GLB_API.stopped ) 
        {
          // Message received (No User Request)
          Wdog_ProcessMessage (NULL);

          // Receive IPC message
          if ( Wdog_PostReceive () == FALSE )
          {
            return 0;
          }
        }
        else
        {
          // Ignore the message
        }
      }
      break;

      case WAIT_OBJECT_0 + HANDLE_IDX_AUTO_QUERY_TIMER:
      {
        // Stop the timer
        if ( Wdog_DisableTimer (GLB_ModuleData.auto_query_timer, _function_name) == FALSE )
        {
          return 0;
        } // if

        // Perform the auto query
        Wdog_RunAutoQuery ();

        // Enable the timer again
        if ( Wdog_EnableTimer (GLB_ModuleData.auto_query_timer, WDOG_AUTO_QUERY_INTERVAL_MSEC, _function_name) == FALSE )
        {
          return 0;
        } // if
      }
      break;
     
      default:
        // Error
        // Logger message
        _stprintf (_aux_tchar_1, _T("%lu"), GetLastError ());
        Common_Logger (NLS_ID_LOGGER(2), _aux_tchar_1, _T("WaitForSingleObjectsEx"), _T(""));
   
        return 0;

      break;
      
    } // switch
      
  } // while

  return 0;
} // Wdog_Thread

//------------------------------------------------------------------------------
// PURPOSE: Posts a new IPC Receive
//
//  PARAMS:
//      - INPUT:  None
//      - OUTPUT: None
//
// RETURNS:
//    - TRUE, on success
//    - FALSE, otherwise
//
//   NOTES:
//
static BOOL Wdog_PostReceive (VOID)
{
  static TCHAR      _function_name [] = _T("Wdog_PostReceive");
  TCHAR             _aux_tchar_1 [WDOG_AUX_STRING_LENGTH];
  API_INPUT_PARAMS  _api_input_params;
  API_OUTPUT_PARAMS * p_api_output_params;
  IPC_IUD_RECEIVE   _ipc_iud_receive;
  IPC_OUD_RECEIVE   * p_ipc_oud_receive;
  WORD              _api_rc;

  // Check for a previously posted receive
  if ( IsReceivePosted () == TRUE )
  {
    return TRUE;
  }

  // INPUT
  //  - API Input Params
  _api_input_params.control_block = sizeof (API_INPUT_PARAMS);
  _api_input_params.event_object  = GLB_ModuleData.ipc_data.ipc_event;
  _api_input_params.function_code = IPC_CODE_RECEIVE;
  _api_input_params.mode          = API_MODE_ASYNC;
  _api_input_params.sub_function_code = 0;
  //  - IPC IUD Receive
  _ipc_iud_receive.control_block  = sizeof (IPC_IUD_RECEIVE);
  _ipc_iud_receive.node_inst_id   = GLB_ModuleData.ipc_data.node_instance_id;
  _ipc_iud_receive.timeout        = IPC_TIMEOUT_INFINITE;
  // OUTPUT
  //  - API Output Params
  p_api_output_params = &GLB_ModuleData.ipc_data.api_output_params;
  p_api_output_params->control_block = sizeof (API_OUTPUT_PARAMS);

  //  - IPC OUD Receive
  p_ipc_oud_receive = &GLB_ModuleData.ipc_data.ipc_oud_receive; 
  p_ipc_oud_receive->control_block   = sizeof (IPC_OUD_RECEIVE); 

  _api_rc = Ipc_API (&_api_input_params,   // API_INPUT_PARAMS
                     &_ipc_iud_receive,    // API_INPUT_USER_DATA
                     p_api_output_params,  // API_OUTPUT_PARAMS
                     p_ipc_oud_receive);   // API_OUTPUT_USER_DATA

  if ( _api_rc != IPC_STATUS_OK )
  {
    // Logger message
    _stprintf (_aux_tchar_1, _T("%hu"), _api_rc);
    Common_Logger (NLS_ID_LOGGER(2), _aux_tchar_1, _T("Ipc_API"), _T(""));

    return FALSE;
  }

  MessagePosted ();

  return TRUE;
} // Wdog_PostReceive

//------------------------------------------------------------------------------
// PURPOSE: Posts a new Queue Read
//
//  PARAMS:
//      - INPUT:
//        - pRequest
//        - pRequestSize
//
//      - OUTPUT: None
//
// RETURNS:
//    - TRUE, on success
//    - FALSE, otherwise
//
//   NOTES:
//
static BOOL Wdog_PostQueueRead (TYPE_API_REQUEST_PARAMS * pRequest,
                                DWORD                   * pRequestSize)
{
  static TCHAR  _function_name [] = _T("Wdog_PostQueueRead");
  TCHAR         _aux_tchar_1 [WDOG_AUX_STRING_LENGTH];
  WORD          _call_status;

  // Call again to read snd queue
  _call_status =  Queue_Read (GLB_ModuleData.request_queue, // Queue
                              GLB_ModuleData.queue_event,   // Notification Event object
                              NULL,                         // APC routine
                              NULL,                         // APC data
                              WDOG_QUEUE_ITEM_SIZE,         // Output buffer size
                              pRequest,                     // User Buffer
                              pRequestSize,                 // Actual Read buffer length
                              NULL);                        // Read handle

  if ( _call_status != QUEUE_STATUS_OK )
  {
    // Logger message
    _stprintf (_aux_tchar_1, _T("%hu"), _call_status);
    Common_Logger (NLS_ID_LOGGER(2), _aux_tchar_1, _T("Queue_Read"), _T(""));

    return FALSE;
  }

  return TRUE;
} // Wdog_PostQueueRead

//-----------------------------------------------------------------------------
// PURPOSE: Processes the given user request
//
//  PARAMS:
//     - INPUT/OUTPUT:
//      - pRequest
//
//     - OUTPUT: None
//
// RETURNS: Nothing
//
// NOTES:
//
static VOID Wdog_ProcessRequest (TYPE_API_REQUEST_PARAMS * pRequest)
{
  static TCHAR  _function_name [] = _T("Wdog_ProcessRequest");
  TCHAR   _aux_tchar_1 [WDOG_AUX_STRING_LENGTH];
  WORD    _output_1;
  WORD    _output_2;

  // Default Output Status
  _output_1 = WDOG_STATUS_ERROR;
  _output_2 = 0;

  switch ( pRequest->api_input_params.function_code )
  {
    case WDOG_CODE_INIT:
      Wdog_ModuleInit (pRequest, &_output_1);
    break;

    case WDOG_CODE_STOP:
      Wdog_ModuleStop (pRequest, &_output_1);
    break;

    case WDOG_CODE_QUERY_STATUS:
      Wdog_QueryStatus (pRequest, &_output_1);
    break;

    case WDOG_CODE_SET_AUTO_QUERY_STATUS:
      Wdog_SetAutoQueryStatus (pRequest, &_output_1);
    break;

    case WDOG_CODE_GET_AUTO_QUERY_STATUS:
      Wdog_GetAutoQueryStatus (pRequest, &_output_1);
    break;

    default:
    {
      // Logger message
      _stprintf (_aux_tchar_1, _T("%hu"), pRequest->api_input_params.function_code);
      Common_Logger (NLS_ID_LOGGER(7), _T("FunctionCode"), _aux_tchar_1, _T(""));
    }
    break;
  } // switch

  Common_API_NotifyRequestStatus (_output_1,
                                  _output_2,
                                  &pRequest->api_input_params,
                                  pRequest->p_api_output_params);

  return;
} // Wdog_ProcessRequest

//-----------------------------------------------------------------------------
// PURPOSE: Initialize the Module
//
//  PARAMS:
//     - INPUT/OUTPUT:
//      - pRequest
//
//     - OUTPUT:
//      - pStatus
//
// RETURNS: Nothing
//
// NOTES:
//
static VOID Wdog_ModuleInit (TYPE_API_REQUEST_PARAMS * pRequest, WORD * pStatus)
{
  TCHAR                   _function_name [] = _T("Wdog_ModuleInit");
  TCHAR                   _aux_tchar_1 [WDOG_AUX_STRING_LENGTH];
  API_INPUT_PARAMS        _api_input_params;
  API_OUTPUT_PARAMS       _api_output_params;
  IPC_IUD_INIT_LOCAL_NODE _ipc_iud_init;
  IPC_OUD_INIT_LOCAL_NODE _ipc_oud_init;
  WDOG_IUD_INIT           * p_wdog_iud_init;
  WDOG_OUD_INIT           * p_wdog_oud_init;
  BOOL                    _init;
  BOOL                    _bool_rc;
  WORD                    _api_rc;

  * pStatus = WDOG_STATUS_ERROR;  
  _init    = FALSE;

  if ( GLB_API.init )
  {
    // Already initialized
    Common_Logger (NLS_ID_LOGGER(7), _T("API Initialized"), _T("TRUE"), _T(""));

    return;
  }

  p_wdog_iud_init = (WDOG_IUD_INIT *) pRequest->api_input_user_data_buffer;
  p_wdog_oud_init = (WDOG_OUD_INIT *) pRequest->p_api_output_user_data; 

  // Check IUD/OUD
  _bool_rc = Common_API_CheckControlBlock (p_wdog_iud_init->control_block, // Received  CB
                                           sizeof (WDOG_IUD_INIT),         // Requested CB
                                           _function_name,                 // Function name
                                           _T("WDOG_IUD_INIT"));           // Structure name
  if ( _bool_rc == FALSE )
  {
    Common_API_Init (_init);

    return;
  }

  _bool_rc = Common_API_CheckControlBlock (p_wdog_oud_init->control_block, // Received  CB
                                           sizeof (WDOG_OUD_INIT),         // Requested CB
                                           _function_name,                 // Function name
                                           _T("WDOG_OUD_INIT"));           // Structure name
  
  if ( _bool_rc == FALSE )
  {
    Common_API_Init (_init);

    return;
  }
  
  // INPUT
  //  - API Input Params
  _api_input_params.control_block = sizeof (API_INPUT_PARAMS);
  _api_input_params.event_object  = NULL;
  _api_input_params.function_code = IPC_CODE_INIT_LOCAL_NODE;
  _api_input_params.mode          = API_MODE_SYNC;
  _api_input_params.sub_function_code = 0;
  //  - IPC IUD Init
  _ipc_iud_init.control_block = sizeof (IPC_IUD_INIT_LOCAL_NODE);
  _ipc_iud_init.input_buffer_size = 0;
  _tcscpy (_ipc_iud_init.node_name, p_wdog_iud_init->wdog_node_name);
  // OUTPUT
  //  - API Output Params
  _api_output_params.control_block = sizeof (API_OUTPUT_PARAMS);
  //  - IPC OUD Init
  _ipc_oud_init.control_block = sizeof (IPC_OUD_INIT_LOCAL_NODE);

  // API Call
  _api_rc = Ipc_API (&_api_input_params,    // API_INPUT_PARAMS
                     &_ipc_iud_init,        // API_INPUT_USER_DATA
                     &_api_output_params,   // API_OUTPUT_PARAMS
                     &_ipc_oud_init);       // API_OUTPUT_USER_DATA
  
  if ( _api_rc == IPC_STATUS_OK )
  {
    if ( _api_output_params.output_1 == IPC_STATUS_OK )
    {
      // Store MyInstanceId
      GLB_ModuleData.ipc_data.node_instance_id = _ipc_oud_init.node_inst_id;

      // API Initialized
      //  - Post a receive to be ready to answer any PONG MSG
      if ( Wdog_PostReceive () == TRUE )
      {
        p_wdog_oud_init->wdog_node_inst_id = _ipc_oud_init.node_inst_id; 
        * pStatus = WDOG_STATUS_OK;
        _init    = TRUE;
      }
    }
    else
    {
      // Logger message
      _stprintf (_aux_tchar_1, _T("%hu"), _api_output_params.output_1);
      Common_Logger (NLS_ID_LOGGER(2), _aux_tchar_1, _T("Ipc_API"), _T(""));
    }
  }
  else
  {
    // Logger message
    _stprintf (_aux_tchar_1, _T("%hu"), _api_rc);
    Common_Logger (NLS_ID_LOGGER(2), _aux_tchar_1, _T("Ipc_API"), _T(""));
  }

  Common_API_Init (_init);

  return;
} // Wdog_ModuleInit

//-----------------------------------------------------------------------------
// PURPOSE: Stop the Module
//
//  PARAMS:
//     - INPUT/OUTPUT:
//      - pRequest
//
//     - OUTPUT:
//      - pStatus
//
// RETURNS: Nothing
//
// NOTES:
//
static VOID Wdog_ModuleStop (TYPE_API_REQUEST_PARAMS * pRequest, WORD * pStatus)
{
  TCHAR _function_name [] = _T("Wdog_ModuleStop");

  // AJQ, 06-AUG-2002, Don't check the API Status, simply stop it, without taking care about the initialization
  //* pStatus = WDOG_STATUS_ERROR;

  //if ( Common_API_CheckStatus (_function_name) == FALSE )
  //{
  //  return;
  //}

  if ( GLB_ModuleData.auto_query_enabled == TRUE )
  {
    Wdog_DisableTimer (GLB_ModuleData.auto_query_timer, _function_name);
  } // if

  * pStatus = WDOG_STATUS_OK;

  Common_API_Stop ();

  return;
} // Wdog_ModuleStop


//-----------------------------------------------------------------------------
// PURPOSE: Query the status of the other nodes ...
//
//  PARAMS:
//     - INPUT/OUTPUT:
//      - pRequest
//
//     - OUTPUT:
//      - pStatus
//
// RETURNS: Nothing
//
// NOTES: Continues processing the IPC messages.
//
static VOID Wdog_QueryStatus (TYPE_API_REQUEST_PARAMS * pRequest, WORD * pStatus)
{
  static TCHAR          _function_name [] = _T("Wdog_QueryStatus");
  TCHAR                 _aux_tchar_1 [WDOG_AUX_STRING_LENGTH];
  WDOG_IUD_QUERY_STATUS * p_wdog_iud_query_status;
  WDOG_OUD_QUERY_STATUS * p_wdog_oud_query_status;
  DWORD                 _answer_count;
  BOOL                  _end_request;
  HANDLE                _handles [HANDLE_COUNT]; 
  DWORD                 _idx;
  DWORD                 _num_retry;
  DWORD                 _wait_status;
  LARGE_INTEGER         _timeout;
  BOOL                  _bool_rc;
  TYPE_PING_PONG_DATA   _ping_pong_data;

  p_wdog_iud_query_status = (WDOG_IUD_QUERY_STATUS *) pRequest->api_input_user_data_buffer;
  p_wdog_oud_query_status = (WDOG_OUD_QUERY_STATUS *) pRequest->p_api_output_user_data;

  * pStatus = WDOG_STATUS_ERROR;

  if ( Common_API_CheckStatus (_function_name) == FALSE )
  {
    return;
  }

  // Check IUD/OUD
  _bool_rc = Common_API_CheckControlBlock (p_wdog_iud_query_status->control_block,
                                           sizeof (WDOG_IUD_QUERY_STATUS),
                                           _function_name,
                                           _T("WDOG_IUD_QUERY_STATUS"));
  if ( _bool_rc == FALSE )
  {
    return;
  }

  // Check IUD/OUD
  _bool_rc = Common_API_CheckControlBlock (p_wdog_oud_query_status->control_block,
                                           sizeof (WDOG_OUD_QUERY_STATUS),
                                           _function_name,
                                           _T("WDOG_OUD_QUERY_STATUS"));
  if ( _bool_rc == FALSE )
  {
    return;
  }

  // Copy Input to Output
  memcpy (p_wdog_oud_query_status, p_wdog_iud_query_status, sizeof (WDOG_OUD_QUERY_STATUS)); 
  
  if ( p_wdog_oud_query_status->wdog_count == 0 )
  {
    * pStatus = WDOG_STATUS_OK;

    return;
  }

  _num_retry  = 0;
  GLB_ModuleData.request_id = WDOG_AUTO_QUERY_RANGE + (GLB_ModuleData.request_id + 1) % WDOG_AUTO_QUERY_RANGE;

  for ( _idx = 0; _idx < p_wdog_oud_query_status->wdog_count; _idx++ )
  {
    // Set status to Not Responding
    p_wdog_oud_query_status->wdog_node_status [_idx] = WDOG_NODE_STATUS_NOT_RESPONDING;
    

    // Get Ping Time
    Wdog_InitPingPongData (&_ping_pong_data);

    // Send Ping
    if ( Wdog_SendPingPong (WDOG_MSG_CMD_PING,
                            GLB_ModuleData.ipc_data.node_instance_id,
                            p_wdog_oud_query_status->wdog_node_id[_idx],
                            GLB_ModuleData.request_id,
                            _idx,
                            &_ping_pong_data) == FALSE )
    {
      // Ping Failed
      return;
    }
  }

  // Init Timer . . .
  // Convert milliseconds to nanoseconds
  // Note time is negatiuve because it is a relative timer
  _timeout.QuadPart = (__int64) WDOG_TIMEOUT_MSEC * (__int64) -10000L;
  
  // Set timer using the client specified timer
  _bool_rc = SetWaitableTimer (GLB_ModuleData.timer_event, // Handle to timer
                               &_timeout,                  // Timer due time
                               0,                          // Periodic timer interval
                               NULL,                       // Completion routine
                               NULL,                       // Completion routine parameter
                               FALSE);                     // Resume state
  if ( _bool_rc == FALSE )
  {
    // Logger message
    _stprintf (_aux_tchar_1, _T("%lu"), GetLastError ());
    Common_Logger (NLS_ID_LOGGER(2), _aux_tchar_1, _T("SetWaitableTimer"), _T(""));
    
    // Receive cannot be cancelled because ReadFileEx is already called
    return;
  }
  
  _end_request = FALSE;

  _handles [HANDLE_IDX_TIMER] = GLB_ModuleData.timer_event;
  _handles [HANDLE_IDX_IPC]   = GLB_ModuleData.ipc_data.ipc_event;

  // Infinite loop till the request is completed or timed-out
  while ( ! _end_request )
  {
    // Wait with using the private event
    _wait_status = WaitForMultipleObjects (2,             // Count
                                           _handles,      // Handles
                                           FALSE,         // WaitAll
                                           INFINITE);     // Milliseconds
    switch ( _wait_status )
    {
      case WAIT_OBJECT_0 + HANDLE_IDX_TIMER:
      {
        if ( _num_retry >= WDOG_MAX_RETRIES )
        {
          _end_request = TRUE;
          * pStatus = WDOG_STATUS_OK;

          break;
        }

        // Increment the number of tries
        _num_retry = _num_retry + 1;

        // Send Ping to nodes that not answered yet
        for ( _idx = 0; _idx < p_wdog_oud_query_status->wdog_count; _idx++ )
        {
          if ( p_wdog_oud_query_status->wdog_node_status[_idx] == WDOG_NODE_STATUS_NOT_RESPONDING )
          {
            // Get Ping Time
            Wdog_InitPingPongData (&_ping_pong_data);

            // Send ping
            if ( Wdog_SendPingPong (WDOG_MSG_CMD_PING,
                                    GLB_ModuleData.ipc_data.node_instance_id,
                                    p_wdog_oud_query_status->wdog_node_id[_idx],
                                    GLB_ModuleData.request_id,
                                    _idx,
                                    &_ping_pong_data) == FALSE )
            {
              // Ping failed ...
              _end_request = TRUE;
            }
          } // if
        } // for

        // Init Timer . . .
        // Convert milliseconds to nanoseconds
        // Note time is negatiuve because it is a relative timer
        _timeout.QuadPart = (__int64) WDOG_TIMEOUT_MSEC * (__int64) -10000L;
        
        // Set timer using the client specified timer
        _bool_rc = SetWaitableTimer (GLB_ModuleData.timer_event, // Handle to timer
                                    &_timeout,                  // Timer due time
                                    0,                          // Periodic timer interval
                                    NULL,                       // Completion routine
                                    NULL,                       // Completion routine parameter
                                    FALSE);                     // Resume state
        if ( _bool_rc == FALSE )
        {
          // Logger message
          _stprintf (_aux_tchar_1, _T("%lu"), GetLastError ());
          Common_Logger (NLS_ID_LOGGER(2), _aux_tchar_1, _T("SetWaitableTimer"), _T(""));
          
          // Receive cannot be cancelled because ReadFileEx is already called
          return;
        }
      }
      break;

      case WAIT_OBJECT_0 + HANDLE_IDX_IPC:
      {
        // Message received 
        MessageReceived ();
        Wdog_ProcessMessage (pRequest);
        
        // Count the number of answers
        _answer_count = 0;
        for ( _idx = 0; _idx < p_wdog_oud_query_status->wdog_count; _idx++ )
        {
          if ( p_wdog_oud_query_status->wdog_node_status[_idx] == WDOG_NODE_STATUS_OK )
          {
            _answer_count = _answer_count + 1;
          }
        }

        // Receive IPC message
        if ( Wdog_PostReceive () == FALSE )
        {
          _end_request = TRUE;
        }

        if ( _answer_count == p_wdog_oud_query_status->wdog_count )
        {
          _end_request = TRUE;
          * pStatus = WDOG_STATUS_OK;
        }
      }
      break;

      default:
      {
        // Error
        // Logger message
        _stprintf (_aux_tchar_1, _T("%lu"), GetLastError ());
        Common_Logger (NLS_ID_LOGGER(2), _aux_tchar_1, _T("WaitForMultipleObjects"), _T(""));
   
        _end_request = TRUE;
      }
      break;
      
    } // switch
  } // while

  // Cancel timeout
  _bool_rc = CancelWaitableTimer (GLB_ModuleData.timer_event);
  if ( ! _bool_rc )
  {
    // Logger message
    _stprintf (_aux_tchar_1, _T("%lu"), GetLastError ());
    Common_Logger (NLS_ID_LOGGER(2), _aux_tchar_1, _T("CancelWaitableTimer"), _T(""));
  }

  return;
} // Wdog_QueryStatus

//-----------------------------------------------------------------------------
// PURPOSE: Processes the received message.
//          The Ping is replied with a Pong.
//          The Pong source node is marked as OK when there is a request, 
//          otherwise the message is ignored.
//
//  PARAMS:
//     - INPUT:
//      - pRequest
//
//     - OUTPUT: None
//
// RETURNS:
//     - On success, TRUE
//     - othrerwise  FALSE
//
// NOTES:
//
static VOID Wdog_ProcessMessage (TYPE_API_REQUEST_PARAMS * pRequest)
{
  static TCHAR          _function_name[] = _T("Wdog_ProcessMessage");
  TCHAR                 _aux_tchar_1[WDOG_AUX_STRING_LENGTH];
  WDOG_OUD_QUERY_STATUS * p_wdog_oud_query_status;
  IPC_OUD_RECEIVE       * pMessage;
  DWORD                 _idx;
  TYPE_PING_PONG_DATA   * p_ping_pong_data;
  LARGE_INTEGER         _perf_counter;
  LARGE_INTEGER         _perf_freq;


  if ( GLB_ModuleData.ipc_data.api_output_params.output_1 != IPC_STATUS_OK )
  {
    // Logger message
    _stprintf (_aux_tchar_1, _T("%hu"), GLB_ModuleData.ipc_data.api_output_params.output_1);
    Common_Logger (NLS_ID_LOGGER(2), _aux_tchar_1, _T("ApiOutputParams"), _T(""));

    return;
  }
  
  pMessage = &GLB_ModuleData.ipc_data.ipc_oud_receive;
  p_ping_pong_data = (TYPE_PING_PONG_DATA *) &pMessage->user_buffer;
  if ( p_ping_pong_data->control_block != sizeof (TYPE_PING_PONG_DATA) )
  {
    // UNDONE Log Error
    return;
  }
  
  switch ( pMessage->msg_cmd )
  {
    case WDOG_MSG_CMD_PING:
    {
      // Send Pong ...
      Wdog_SendPingPong  (WDOG_MSG_CMD_PONG,
                          GLB_ModuleData.ipc_data.node_instance_id,
                          pMessage->source_node_inst_id,
                          pMessage->msg_id,
                          pMessage->user_dword,
                          p_ping_pong_data);
    }
    break;

    case WDOG_MSG_CMD_PONG:
    { 
      if ( pRequest != NULL )
      {
        p_wdog_oud_query_status = (WDOG_OUD_QUERY_STATUS *) pRequest->p_api_output_user_data;

        // Check RequestId
        if ( GLB_ModuleData.request_id == pMessage->msg_id ) 
        {
          _idx = pMessage->user_dword;
          if ( _idx < p_wdog_oud_query_status->wdog_count )
          {
            if ( p_wdog_oud_query_status->wdog_node_id [_idx] == pMessage->source_node_inst_id )
            {
              p_wdog_oud_query_status->wdog_node_status[_idx] = WDOG_NODE_STATUS_OK;

              {
                p_wdog_oud_query_status->wdog_no_answer[_idx]  = 0;
                p_wdog_oud_query_status->wdog_pong_time[_idx]  = time (NULL); // p_ping_pong_data->ping_time;
                p_wdog_oud_query_status->wdog_ping_delay[_idx] = 0;

                if ( QueryPerformanceFrequency (&_perf_freq) == TRUE )
                {
                  if ( QueryPerformanceCounter (&_perf_counter) == TRUE )
                  {
                    __int64 _ticks;
                    __int64 _freq;
                    _ticks = _perf_counter.QuadPart - p_ping_pong_data->perf_counter.QuadPart;
                    _freq  = _perf_freq.QuadPart;
                    _ticks = 1000000L * _ticks / _freq;
                    p_wdog_oud_query_status->wdog_ping_delay[_idx] = (DWORD) _ticks;
                  }
                }
              }

              return;
            }
            else
            {
              // Wrong index, ignore the message
            }
          }
          else
          {
            // Index is too big, ignore the message
          }

          return;
        } // if ( GLB_ModuleData.request_id == pMessage->msg_id ) 
      } // if ( pRequest != NULL )

      // AJQ 23-FEB-2003, The pRequest != NULL but the pong is an answer 
      //                  to the AutoQuery
      //
      if ( TRUE ) // Don't care about the request id GLB_ModuleData.auto_query_request_id == pMessage->msg_id )
      {
        p_wdog_oud_query_status = &GLB_ModuleData.auto_query_tmp_status;

        _idx = pMessage->user_dword;
        if ( _idx < GLB_ModuleData.auto_query_tmp_status.wdog_count )
        {
          if ( GLB_ModuleData.auto_query_tmp_status.wdog_node_id[_idx] == pMessage->source_node_inst_id )
          {
            GLB_ModuleData.auto_query_tmp_status.wdog_node_status[_idx] = WDOG_NODE_STATUS_OK;
            {
              p_wdog_oud_query_status->wdog_no_answer[_idx]  = 0;
              p_wdog_oud_query_status->wdog_pong_time[_idx]  = time (NULL); // p_ping_pong_data->ping_time;
              p_wdog_oud_query_status->wdog_ping_delay[_idx] = 0;

              if ( QueryPerformanceFrequency (&_perf_freq) == TRUE )
              {
                if ( QueryPerformanceCounter (&_perf_counter) == TRUE )
                {
                  __int64 _ticks;
                  __int64 _freq;
                  _ticks = _perf_counter.QuadPart - p_ping_pong_data->perf_counter.QuadPart;
                  _freq  = _perf_freq.QuadPart;
                  _ticks = 1000000L * _ticks / _freq;
                  p_wdog_oud_query_status->wdog_ping_delay[_idx] = (DWORD) _ticks;
                }
              }
            }
          }
          else
          {
            // Wrong index, ignore the message
          }
        }
        else
        {
          // Index is too big, ignore the message
        }
      }
      else
      {
        // Ignore message
      }
    }
    break;

    default:
    {
      // Logger message
      _stprintf (_aux_tchar_1, _T("%lu"), GLB_ModuleData.ipc_data.ipc_oud_receive.msg_cmd);
      Common_Logger (NLS_ID_LOGGER(7), "msg_cmd", _aux_tchar_1, _T(""));
    }
  }

  return;
} // Wdog_ProcessMessage

//-----------------------------------------------------------------------------
// PURPOSE: Send Ping/Pong message from SourceNode to TargetNode
//
//  PARAMS:
//     - INPUT:
//      - PingPongCmd
//      - SourceNodeInstanceId
//      - TargetNodeInstanceId
//      - PingRequestId
//      - PingUserIndex
//
//     - OUTPUT: None
//
// RETURNS:
//     - On success, TRUE
//     - othrerwise  FALSE
//
// NOTES:
//
static BOOL Wdog_SendPingPong  (DWORD               PingPongCmd,
                                DWORD               SourceNodeInstanceId,
                                DWORD               TargetNodeInstanceId,
                                DWORD               PingRequestId,
                                DWORD               PingUserIndex,
                                TYPE_PING_PONG_DATA * pPingPongData)
{
  static TCHAR      _function_name [] = _T("Wdog_SendPingPong");
  TCHAR             _aux_tchar_1 [WDOG_AUX_STRING_LENGTH];

  API_INPUT_PARAMS  _api_input_params;
  API_OUTPUT_PARAMS _api_output_params;
  IPC_IUD_SEND      _ipc_iud_send;
  WORD              _api_rc;

  
  // INPUT:
  //  - API Input Params
  _api_input_params.control_block = sizeof (API_INPUT_PARAMS);
  _api_input_params.event_object  = NULL;
  _api_input_params.function_code = IPC_CODE_SEND;
  _api_input_params.mode          = API_MODE_NO_WAIT;
  _api_input_params.sub_function_code = 0;
  //  - IPC IUD Send
  _ipc_iud_send.control_block       = sizeof (IPC_IUD_SEND);
  _ipc_iud_send.msg_cmd             = PingPongCmd;
  _ipc_iud_send.msg_id              = PingRequestId;
  _ipc_iud_send.msg_status          = 0;
  _ipc_iud_send.source_node_inst_id = SourceNodeInstanceId;
  _ipc_iud_send.target_node_inst_id = TargetNodeInstanceId;
  _ipc_iud_send.user_buffer_length  = 0;
  _ipc_iud_send.user_dword          = PingUserIndex;

  if ( pPingPongData != NULL )
  {
    _ipc_iud_send.user_buffer_length = sizeof (TYPE_PING_PONG_DATA);
    memcpy (_ipc_iud_send.user_buffer, pPingPongData, _ipc_iud_send.user_buffer_length);
  }

  // OUTPUT:
  //  - API Output Params
  _api_output_params.control_block  = sizeof (API_OUTPUT_PARAMS);

  // API Call
  _api_rc = Ipc_API (&_api_input_params,
                     &_ipc_iud_send,
                     &_api_output_params,
                     NULL);

  // Check API Status
  if ( _api_rc != IPC_STATUS_OK )
  {
    // Logger message
    _stprintf (_aux_tchar_1, _T("%hu"), _api_rc);
    Common_Logger (NLS_ID_LOGGER(2), _aux_tchar_1, _T("Ipc_API"), _T(""));

    return FALSE;
  }

  return TRUE;
} // Wdog_SendPingPong

//-----------------------------------------------------------------------------
// PURPOSE: Checks whether or not the given function code supports the 
//          requested mode.
//
//  PARAMS:
//     - INPUT:
//      - pApiInputParams
//
//     - OUTPUT: None
//
// RETURNS:
//     - On success, TRUE
//     - othrerwise  FALSE
//
// NOTES:
//
static BOOL Wdog_CheckMode (const API_INPUT_PARAMS * pApiInputParams)
{
  WORD  _function_code;
  WORD  _api_mode;

  _function_code = pApiInputParams->function_code;
  _api_mode      = pApiInputParams->mode;

  // Check for non supported modes 
  if ( _api_mode == API_MODE_NO_WAIT )
  {
    switch ( _function_code )
    {
      case WDOG_CODE_STOP:
      break;

      case WDOG_CODE_INIT:
      case WDOG_CODE_QUERY_STATUS:
      default:
      {
        return FALSE;
      }
      break;
    }
  } // if

  return TRUE;
}

//-----------------------------------------------------------------------------
// PURPOSE: Returns the Queue's handle where the Common_API will put the user
//          request.
//
//  PARAMS:
//     - INPUT:
//      - pApiInputParams
//
//     - OUTPUT: None
//
// RETURNS:
//     - The queue handle
//
// NOTES: Don't care about the input parameters, there is only one queue.
//  
//
static QUEUE_HANDLE Wdog_SelectQueue (const API_INPUT_PARAMS * pApiInputParams)
{
  return GLB_ModuleData.request_queue; 
} // Wdog_SelectQueue

//-----------------------------------------------------------------------------
// PURPOSE: Sets the nodes to be queried automatically
//
//  PARAMS:
//     - INPUT/OUTPUT:
//      - pRequest
//
//     - OUTPUT:
//      - pStatus
//
// RETURNS: Nothing
//
// NOTES:
//
static VOID Wdog_SetAutoQueryStatus (TYPE_API_REQUEST_PARAMS * pRequest, WORD * pStatus)
{
  TCHAR   _function_name [] = _T("Wdog_SetAutoQueryStatus");
  DWORD   _idx;

  * pStatus = WDOG_STATUS_ERROR;

  if ( GLB_ModuleData.auto_query_enabled == TRUE )
  {
    return;
  } // if

  // Copy the list of nodes to the internal data structure
  memcpy (&GLB_ModuleData.auto_query_tmp_status, 
          pRequest->api_input_user_data_buffer, 
          sizeof (WDOG_OUD_GET_AUTO_QUERY_STATUS));

  for ( _idx = 0; _idx < GLB_ModuleData.auto_query_tmp_status.wdog_count; _idx++ )
  {
    GLB_ModuleData.auto_query_tmp_status.wdog_node_status[_idx] = WDOG_NODE_STATUS_UNKNOWN;  
  } // for

  //// Copy the list of nodes to the internal data structure
  //memcpy (&GLB_ModuleData.auto_query_tmp_status, 
  //        &GLB_ModuleData.auto_get_query_status,
  //        sizeof (WDOG_OUD_GET_AUTO_QUERY_STATUS));

  for ( _idx = 0; _idx < GLB_ModuleData.auto_query_tmp_status.wdog_count; _idx++ )
  {
    GLB_ModuleData.auto_query_tmp_status.wdog_ping_time[_idx] = (DWORD) time (NULL) - WDOG_PING_INTERVAL_STATUS_UNKNOWN; 
    GLB_ModuleData.auto_query_tmp_status.wdog_pong_time[_idx] = (DWORD) time (NULL) - WDOG_PING_INTERVAL_STATUS_UNKNOWN; 
  }

  GLB_ModuleData.auto_query_enabled   = TRUE;
  GLB_ModuleData.auto_query_executed  = FALSE;

  // Execute the AutoQuery
  Wdog_RunAutoQuery ();

  // Mark as not executed yet!!!!
  GLB_ModuleData.auto_query_executed = FALSE;

  if ( Wdog_EnableTimer (GLB_ModuleData.auto_query_timer, WDOG_AUTO_QUERY_INTERVAL_MSEC, _function_name) == FALSE )
  {
    return;
  } // if


  * pStatus = WDOG_STATUS_OK;

} // Wdog_SetAutoQueryStatus

//-----------------------------------------------------------------------------
// PURPOSE: Gets the status of the nodes that are queried automatically
//
//  PARAMS:
//     - INPUT/OUTPUT:
//      - pRequest
//
//     - OUTPUT:
//      - pStatus
//
// RETURNS: Nothing
//
// NOTES:
//
static VOID Wdog_GetAutoQueryStatus (TYPE_API_REQUEST_PARAMS * pRequest, WORD * pStatus)
{
  static TCHAR _function_name [] = _T("Wdog_GetAutoQueryStatus");
  DWORD        _idx;
  WDOG_OUD_GET_AUTO_QUERY_STATUS * p_oud;


  * pStatus = WDOG_STATUS_ERROR;

  if ( GLB_ModuleData.auto_query_enabled == FALSE )
  {
    return;
  } // if

  // Has been executed ?
  if ( GLB_ModuleData.auto_query_executed == FALSE )
  {
    memcpy (pRequest->p_api_output_user_data,
            &GLB_ModuleData.auto_query_tmp_status, 
            sizeof (WDOG_OUD_GET_AUTO_QUERY_STATUS));
    
    // Mark all the nodes as UNKNOWN
    p_oud = (WDOG_OUD_GET_AUTO_QUERY_STATUS *) pRequest->p_api_output_user_data;
    for ( _idx = 0; _idx < p_oud->wdog_count; _idx++ )
    {
      // Mark the status as Unknown ...
      p_oud->wdog_node_status[_idx] = WDOG_NODE_STATUS_UNKNOWN; 
    }
  }
  else
  {
    // Copy the list of nodes from the internal data structure
    memcpy (pRequest->p_api_output_user_data,
            &GLB_ModuleData.auto_query_tmp_status, 
            sizeof (WDOG_OUD_GET_AUTO_QUERY_STATUS));
  }

  * pStatus = WDOG_STATUS_OK;

  return;
} // Wdog_GetAutoQueryStatus

//-----------------------------------------------------------------------------
// PURPOSE: Enables a timer
//
//  PARAMS:
//     - INPUT: 
//        - Timer
//        - FunctionName
//     - OUTPUT: None
//
// RETURNS:
//      - TRUE: on success
//      - FALSE: on failure
//
// NOTES:
//
BOOL Wdog_EnableTimer (HANDLE Timer, DWORD  Milliseconds, TCHAR * pFunctionName)
{
  BOOL          _bool_rc;
  LARGE_INTEGER _due_time;

  if ( Timer == NULL )
  {
    return FALSE;
  } // if

  _due_time.QuadPart = (__int64) -10000L * (__int64) Milliseconds;

  _bool_rc = SetWaitableTimer (Timer,         // Timer
                               &_due_time,    // DueTime
                               0,             // Period
                               NULL,          // APC Routine
                               NULL,          // APC Parameter
                               FALSE);        // Resume

  if ( _bool_rc == FALSE )
  {
    // UNDONE: LogError
  }

  return _bool_rc;

} // Wdog_EnableTimer

//-----------------------------------------------------------------------------
// PURPOSE: Disables a timer
//
//  PARAMS:
//     - INPUT: 
//        - Timer
//        - FunctionName
//     - OUTPUT: None
//
// RETURNS:
//      - TRUE: on success
//      - FALSE: on failure
//
// NOTES:
//
BOOL Wdog_DisableTimer (HANDLE Timer, TCHAR * pFunctionName)
{
  BOOL          _bool_rc;
  
  if ( Timer == NULL )
  {
    return TRUE;
  } // if

  _bool_rc = CancelWaitableTimer (Timer);

  if ( _bool_rc == FALSE )
  {
    // UNDONE: LogError
  }

  return _bool_rc;

} // Wdog_DisableTimer

//-----------------------------------------------------------------------------
// PURPOSE: Starts a new AutoQuery request.
//          1.) Makes a copy of the current status, this copy is the one 
//              returned by the AutoQueryGetStatus.
//          2.) Loop over all the auto-queried nodes, 
//              sets the status to NOT_RESPONDING and send the first PING
//
//  PARAMS:
//    - INPUT:  None
//    - OUTPUT: None
//
// RETURNS: Nothing
//
// NOTES:
//
static VOID Wdog_RunAutoQuery (VOID)
{
  static TCHAR        _function_name [] = _T("Wdog_RunAutoQuery");
  static DWORD        _idx;
  TYPE_PING_PONG_DATA _ping_pong_data;
  BOOL                _ping;
  DWORD               _trick;
  DWORD               _num_ping_to_not_responging;
  DWORD               _num_ping_to_unknown;
  time_t              _now;
  DWORD               _ellapsed_ping_time;
  DWORD               _ellapsed_pong_time;
  DWORD               _idx_loop;

  _now = time (NULL);

  // Increment the RequestId
  GLB_ModuleData.auto_query_request_id = (GLB_ModuleData.auto_query_request_id + 1) % WDOG_AUTO_QUERY_RANGE;


  _num_ping_to_not_responging = 0;
  _num_ping_to_unknown        = 0;

  for ( _idx_loop = 0; _idx_loop < GLB_ModuleData.auto_query_tmp_status.wdog_count; _idx_loop++ )
  {
    _idx = (_idx + 1) % GLB_ModuleData.auto_query_tmp_status.wdog_count;

    // Change the status
    _ellapsed_ping_time = (DWORD) difftime (_now, GLB_ModuleData.auto_query_tmp_status.wdog_ping_time[_idx]);
    _ellapsed_pong_time = (DWORD) difftime (_now, GLB_ModuleData.auto_query_tmp_status.wdog_pong_time[_idx]);

    _ping = FALSE;
    switch ( GLB_ModuleData.auto_query_tmp_status.wdog_node_status[_idx] )
    {
      case WDOG_NODE_STATUS_OK:
      {
        if (   _ellapsed_pong_time > WDOG_INTERVAL_OK_TO_UNKNOWN
            && GLB_ModuleData.auto_query_tmp_status.wdog_no_answer[_idx] > WDOG_MAX_NO_ANSWER )
        {
          GLB_ModuleData.auto_query_tmp_status.wdog_node_status[_idx] = WDOG_NODE_STATUS_UNKNOWN;
        }
        else
        {
          if (   _ellapsed_ping_time >= WDOG_PING_INTERVAL_STATUS_OK 
              && _ellapsed_pong_time >= WDOG_PING_INTERVAL_STATUS_OK )
          {
            _ping = TRUE;
          }
        }
      }
      break;

      case WDOG_NODE_STATUS_NOT_RESPONDING:
      {
        if ( _num_ping_to_not_responging <= WDOG_MAX_PING_NOT_RESPONDING )
        {
          if (   _ellapsed_ping_time >= WDOG_PING_INTERVAL_STATUS_NOT_RESPONDING 
              && _ellapsed_pong_time >= WDOG_PING_INTERVAL_STATUS_NOT_RESPONDING )
          {
            _trick = GLB_ModuleData.auto_query_request_id + _idx;
            _trick = _trick % WDOG_TRICK_NOT_RESPONDING;
            _trick = 0;

            if ( _trick == 0 )
            {
              _num_ping_to_not_responging++;
              _ping = TRUE;
            }
          }
        }
      }
      break;

      case WDOG_NODE_STATUS_UNKNOWN:
      {
        if (   _ellapsed_pong_time > WDOG_INTERVAL_UNKNOWN_TO_NOT_RESPONDING 
            && GLB_ModuleData.auto_query_tmp_status.wdog_no_answer[_idx] > WDOG_MAX_NO_ANSWER )
        {
          GLB_ModuleData.auto_query_tmp_status.wdog_node_status[_idx] = WDOG_NODE_STATUS_NOT_RESPONDING;
        }
        else
        {
          if ( _num_ping_to_unknown <= WDOG_MAX_PING_UNKNOWN )
          {
            if ( _ellapsed_ping_time > WDOG_PING_INTERVAL_STATUS_UNKNOWN )
            {
              _trick = GLB_ModuleData.auto_query_request_id + _idx;
              _trick = _trick % WDOG_TRICK_UNKNOWN;
              _trick = 0;

              if ( _trick == 0 )
              {
                _num_ping_to_unknown++;
                _ping = TRUE;
              }
            }
          }
        }
      }
      break;

      default:
      {
        // Do Nothing
      }
      break;
    }


    if ( !_ping )
    {
      continue;
    }

    //GLB_ModuleData.auto_query_tmp_status.wdog_node_status[_idx] = WDOG_NODE_STATUS_UNKNOWN;
    GLB_ModuleData.auto_query_tmp_status.wdog_no_answer[_idx]++;
    GLB_ModuleData.auto_query_tmp_status.wdog_ping_time[_idx] = time (NULL); 

    // Get Ping Time
    Wdog_InitPingPongData (&_ping_pong_data);

    // Ping ...
    // Don't care about the return code ...
    Wdog_SendPingPong  (WDOG_MSG_CMD_PING,
                        GLB_ModuleData.ipc_data.node_instance_id,
                        GLB_ModuleData.auto_query_tmp_status.wdog_node_id[_idx],
                        GLB_ModuleData.auto_query_request_id,
                        _idx,
                        &_ping_pong_data);
  } // for

  GLB_ModuleData.auto_query_executed = TRUE;

  return;
} // Wdog_RunAutoQuery

//-----------------------------------------------------------------------------
// PURPOSE: Initialize the PingPongData
//
//  PARAMS:
//    - INPUT:  None
//    - OUTPUT: 
//      - pPingPongData
//
// RETURNS: Nothing
//
// NOTES:
//

static VOID   Wdog_InitPingPongData (TYPE_PING_PONG_DATA * pPingPongData)
{
  LARGE_INTEGER _perf_counter;

  pPingPongData->control_block = sizeof (TYPE_PING_PONG_DATA);
  pPingPongData->ping_time     = time (NULL);
  pPingPongData->perf_counter.QuadPart = 0;

  if ( QueryPerformanceCounter (&_perf_counter) == TRUE )
  {
    pPingPongData->perf_counter = _perf_counter;
  }

  return;

} // Wdog_InitPingPongData
