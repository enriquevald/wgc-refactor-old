﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Test Reserved Terminal 
// 
//   DESCRIPTION: Test ReservedTerminalService 
// 
//        AUTHOR: Rodrigo González Rodríguez
// 
// CREATION DATE: 03-AUG-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 03-AUG-2016 RGR    Initial Version
// 12-AUG-2016 RGR    New test
// ----------  ------ ----------------------------------------------------------

using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WSI.Common;
using System.Threading;
using WSI.Common.ReservedTerminal;
using System.Collections.Generic;

namespace WSI.CommonUnitTest.ReservedTerminalTest
{
  [TestClass]
  public class ReservedTerminalConfigurationServiceUnitTest
  {
    [TestInitialize]
    public void Initialize()
    {
      DbConfig _config;

      _config = DbConfig.CreateFromFile("WSI.CommonUnitTest.cfg");
      WGDB.Init(_config.DbId, _config.Server1, _config.Server2);
      WGDB.SetApplication("WigosGUI", "18.059");
      WGDB.ConnectAs("wggui");

      while (WGDB.ConnectionState != System.Data.ConnectionState.Open)
      {
        Log.Warning("Waiting for database connection ...");
        Thread.Sleep(5000);
      }
    }

    [TestMethod]
    public void ReadTestMethod()
    {
      ReservedTerminalResponseBase _response;
      ReservedTerminalConfiguration _data;

      using (IReservedTerminalConfigurationService _service = new ReservedTerminalConfigurationService())
      {
        _response = _service.Read();
        if (_response.Success)
        {
          _data = ((ReservedTerminalConfigurationResponse)_response).Configurations;
        }
      }

      Assert.AreEqual(true, _response.Success);
    }

    [TestMethod]
    public void SaveTestMethod()
    {
      ReservedTerminalResponseBase _response;
      ReservedTerminalSaveRequest _request;

      _request = new ReservedTerminalSaveRequest();


      using (IReservedTerminalConfigurationService _service = new ReservedTerminalConfigurationService())
      {
        _request.TimeLevels = new List<AccountTimeLevel>();
        
        _request.TimeLevels.Add(new AccountTimeLevel { LevelType = HolderLevelType.A, Minutes = 10, 
          TerminalList = "<ProviderList> </ProviderList>", Terminals = new List<int> { 10415 } });
       
        _request.TimeLevels.Add(new AccountTimeLevel { LevelType = HolderLevelType.B, Minutes = 10, 
          TerminalList = "<ProviderList> </ProviderList>", Terminals = new List<int> { 10415 } });

        _response = _service.Save(_request);

        _request.TimeLevels = new List<AccountTimeLevel>();

        _request.TimeLevels.Add(new AccountTimeLevel { LevelType = HolderLevelType.A, Minutes = 10, 
          TerminalList = "<ProviderList> </ProviderList>", Terminals = new List<int> { 10415 } });
        
        _request.TimeLevels.Add(new AccountTimeLevel { LevelType = HolderLevelType.B, Minutes = 20, 
          TerminalList = "<ProviderList> </ProviderList>", Terminals = new List<int> { 10415 } });
        
        _request.TimeLevels.Add(new AccountTimeLevel { LevelType = HolderLevelType.C, Minutes = 30, 
          TerminalList = "<ProviderList> </ProviderList>", Terminals = new List<int> { 10415 } });
        
        _request.TimeLevels.Add(new AccountTimeLevel { LevelType = HolderLevelType.D, Minutes = 40,
          TerminalList = "<ProviderList> </ProviderList>", Terminals = new List<int> { 10415 } });

        _response = _service.Save(_request);
      }

      Assert.AreEqual(true, _response.Success);
    }
  }
}
