﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Test Reserved Terminal 
// 
//   DESCRIPTION: Test ReservedTerminalService 
// 
//        AUTHOR: Rodrigo González Rodríguez
// 
// CREATION DATE: 03-AUG-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 09-AUG-2016 RGR    Initial Version
// ----------  ------ ----------------------------------------------------------

using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WSI.Common;
using System.Threading;
using WSI.Common.ReservedTerminal;

namespace WSI.CommonUnitTest.ReservedTerminalTest
{
  [TestClass]
  public class ReserveTerminalServiceUnitTest
  {
    [TestInitialize]
    public void Initialize()
    {
      DbConfig _config;

      _config = DbConfig.CreateFromFile("WSI.CommonUnitTest.cfg");
      WGDB.Init(_config.DbId, _config.Server1, _config.Server2);
      WGDB.SetApplication("WigosGUI", "18.059");
      WGDB.ConnectAs("wggui");

      while (WGDB.ConnectionState != System.Data.ConnectionState.Open)
      {
        Log.Warning("Waiting for database connection ...");
        Thread.Sleep(5000);
      }
    }


    [TestMethod]
    public void IsReservedTerminalTestMethod()
    {
      ReservedTerminalResponse<bool> _response;

      using (IReservedTerminalService _service = new ReservedTerminalService())
      {
        _response = _service.IsReservedTerminal(10415);
      }
    }

    [TestMethod]
    public void AddTestMethod()
    {
      ReservedTerminalResponse<bool> _response;
      ReservedTerminalReservedRequest _request;

      _request = new ReservedTerminalReservedRequest();
      _request.AccountID = 1002301;
      _request.TerminalID = 10415;
      using (IReservedTerminalService _service = new ReservedTerminalService())
      {
        _response = _service.Add(_request);
      }
    }

    [TestMethod]
    public void RemoveTestMethod()
    {
      ReservedTerminalResponse<bool> _response;
      ReservedTerminalReservedRequest _request;

      _request = new ReservedTerminalReservedRequest();
      _request.AccountID = 1002301;
      _request.TerminalID = 10415;
      using (IReservedTerminalService _service = new ReservedTerminalService())
      {
        _response = _service.Remove(_request);
      }
    }
  }
}
