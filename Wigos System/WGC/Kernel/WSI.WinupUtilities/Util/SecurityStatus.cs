﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: SecurityStatus.cs
// 
//   DESCRIPTION: Utility to status security
// 
//        AUTHOR: Diego Tenutto
// 
// CREATION DATE: 10-Abr-2018
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 10-Abr-2018 DMT    First release 
// ----------- ------ ----------------------------------------------------------
using System;
using System.Collections.Generic;

namespace WSI.WinupUtilities.Util
{
  public class SecurityStatus
  {
    public enum LoginStatus
    {
      ERROR = 0,
      OK = 1,
      USER_NOT_EXISTS = 2,
      WRONG_PASSWORD = 3,
      WRONG_PASSWORD_USER_HAS_BEEN_BLOCKED = 4,
      WRONG_PASSWORD_USER_IS_BLOCKED = 5,
      WRONG_PASSWORD_SUPER_USER = 6,
      USER_BLOCKED = 7,
      ILLEGAL_ACCESS = 8,
      SESION_OPENED = 9,
      NOT_PERMISSIONS = 10,
      OK_PASSWORD_EXPIRED = 11,
      OK_PASSWORD_CHANGE_REQ = 12
    } // LoginStatus
  }
}
