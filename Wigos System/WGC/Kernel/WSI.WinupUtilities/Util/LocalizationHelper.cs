﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: LocalizationHelper.cs
// 
//   DESCRIPTION: Utility to format a error response
// 
//        AUTHOR: Ariel Vazquez
// 
// CREATION DATE: 27-Oct-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 27-Oct-2016 AGV    First release 
// ----------- ------ ----------------------------------------------------------
using System;
using System.Collections.Generic;
using WSI.WCP;

namespace WSI.WinupUtilities.Util
{
  public static class LocalizationHelper
  {
    private const string WKT_ERROR = "WKT_ERROR_{0}";
    /// <summary>
    /// Method to obtain a formated error code key
    /// </summary>
    /// <param name="code">error code</param>
    /// <returns></returns>
    public static string GetResourceKey(WCP_ResponseCodes code)
    {
      return string.Format(WKT_ERROR, ((int)code).ToString());
    }
  }
}
