﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Format.cs
// 
//   DESCRIPTION: Utility to format a date
// 
//        AUTHOR: Ariel Vazquez
// 
// CREATION DATE: 27-Oct-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 27-Oct-2016 AGV    First release 
// ----------- ------ ----------------------------------------------------------
using System;
using System.Collections.Generic;

namespace WSI.WinupUtilities.Util
{
  public class Format
  {
    /// <summary>
    /// Simple date dd/mm/yyyy format
    /// </summary>
    /// <param name="dateToFormat">string to format</param>
    /// <returns></returns>
    public static string DateString(string dateToFormat)
    {
      if (!string.IsNullOrEmpty(dateToFormat))
      {
        var date = Convert.ToDateTime(dateToFormat);
        return string.Format("{0}/{1}/{2}", date.Day, date.Month, date.Year);
      }

      return dateToFormat;
    }
  }
}
