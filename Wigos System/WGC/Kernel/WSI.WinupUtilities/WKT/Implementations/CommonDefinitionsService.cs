﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: CommonDefinitionsService.cs
// 
//   DESCRIPTION: Implementation of service to get enums from  original CommonDefinitions
// 
//        AUTHOR: Lionel Asensio
// 
// CREATION DATE: 01-DIC-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 27-Oct-2016 LA    First release 
// ----------- ------ ----------------------------------------------------------
using System;
using System.Collections;
using System.Data;
using System.Collections.Generic;
using WSI.WinupUtilities.WKT.Contracts;


namespace WSI.WinupUtilities.WKT.Implementations
{
  public class CommonDefinitionsService : ICommonDefinitionsService
  {
    public Dictionary<string, int> EnumGUI()
    {
      Dictionary<string, int> _enumGUI = new Dictionary<string, int>();

      foreach (Common.ENUM_GUI eg in Enum.GetValues(typeof(Common.ENUM_GUI)))
      {
        _enumGUI.Add(eg.ToString(),(int)eg);
      }
      return _enumGUI;
    
    }
    public Dictionary<string, int> Target()
    {
      Dictionary<string, int > _target = new Dictionary<string, int>();

      foreach (Common.ADS_TARGET tg in Enum.GetValues(typeof(Common.ADS_TARGET)))
      {
        _target.Add( tg.ToString(),(int)tg);
      }
      return _target;

    }

    public Dictionary<string, int> DrawGenderFilter()
    {
      Dictionary<string, int> _drawGenderFilter = new Dictionary<string, int>();

      foreach (Common.DrawGenderFilter dg in Enum.GetValues(typeof(Common.DrawGenderFilter)))
      {
        _drawGenderFilter.Add(dg.ToString(), (int)dg);
      }
      return _drawGenderFilter;

    }


  }
}
