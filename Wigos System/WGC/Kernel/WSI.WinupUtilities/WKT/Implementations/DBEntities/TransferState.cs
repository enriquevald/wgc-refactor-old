﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WSI.WinupUtilities.WKT.Implementations.DBEntities
{
  public class TransferState
  {
    public Nullable<long> Id { get; set; }
    public string TransactionId { get; set; }
    public decimal Amount { get; set; }
    public Provider Provider { get; set; }
    public TransferStatus TransferStatus { get; set; }
    public TransferType TransferType { get; set; }
  }
}
