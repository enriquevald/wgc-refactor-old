﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WigosSecurityService.cs
// 
//   DESCRIPTION: Implementation of service to get player info from WKT
// 
//        AUTHOR: Diego Tenutto
// 
// CREATION DATE: 10-Abr-2018
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 10-Abr-2018 DMT    First release 
// ----------- ------ ----------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using WSI.Common;
using WSI.WCP;
using WSI.WinupUtilities.Util;
using WSI.WinupUtilities.WKT.Contracts;
using WSI.WinupUtilities.WKT.Contracts.ContractData.PlayerInfo;
using WSI.WinupUtilities.WKT.Contracts.Exceptions;
using WSI.WinupUtilities.WKT.Implementations.DBEntities;

namespace WSI.WinupUtilities.WKT.Implementations
{
  public class WigosSecurityService : ISecurityService
  {
    public WigosSecurityService()
    {
      WSI.Common.Resource.Init();
    }

    // PURPOSE: Check if the user exists, check if password is OK, check if the user is blocked.
    //
    //  PARAMS:
    //      - INPUT:
    //          - AccountId
    //          - GuiId
    //
    // RETURNS:
    //      - IsValidUser
    // 
    //   NOTES:
    //     
    public WSI.WinupUtilities.Util.SecurityStatus.LoginStatus IsValidUser(int AccountId, WSI.Common.ENUM_GUI GuiId)
    {
      UserLogin.UserInfo _user_info;
      SecurityStatus.LoginStatus _status;
      Int32 _nls_id;
      Int32 _audit_code;
      String _description;
      AlarmCode _alarm_code;

      _user_info = new UserLogin.UserInfo();
      _status = SecurityStatus.LoginStatus.ERROR;
      _alarm_code = AlarmCode.Unknown;
      _description = "";

      _nls_id = 5000;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (!UserLogin.GetUserIfExistInDb(AccountId, _user_info, _db_trx.SqlTransaction))
          {
            _status = SecurityStatus.LoginStatus.USER_NOT_EXISTS;
          }
          else
          {
            if (_user_info.block_reason > 0)
            {
              _status = SecurityStatus.LoginStatus.USER_BLOCKED;
            }
            else if (!UserLogin.CheckIllegalAccess(_user_info))
            {
              _status = SecurityStatus.LoginStatus.ILLEGAL_ACCESS;
            }
            else if (!UserLogin.CheckPermissions(_user_info.user_id, _user_info.profile_id, GuiId, _db_trx.SqlTransaction))
            {
              _status = SecurityStatus.LoginStatus.NOT_PERMISSIONS;
            }
            else
            {
              if (_user_info.psw_chg_req)
              {
                _status = SecurityStatus.LoginStatus.OK_PASSWORD_CHANGE_REQ;
              }
              else
              {
                if (!UserLogin.CheckPasswordExpiration(_user_info))
                {
                  _status = SecurityStatus.LoginStatus.OK;
                }
                else
                {
                  _status = SecurityStatus.LoginStatus.OK_PASSWORD_EXPIRED;
                }
              }
            }
          }

          _db_trx.Commit();
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      finally
      {
        //AccountId = _user_info.user_id;

        switch (_status)
        {
          case SecurityStatus.LoginStatus.ERROR:
            break;
          case SecurityStatus.LoginStatus.OK:
            break;
          case SecurityStatus.LoginStatus.USER_NOT_EXISTS:
            AccountId = -1;
            break;
          case SecurityStatus.LoginStatus.WRONG_PASSWORD:
            break;
          case SecurityStatus.LoginStatus.WRONG_PASSWORD_USER_HAS_BEEN_BLOCKED:
            _nls_id += 106;
            _alarm_code = AlarmCode.User_MaxLoginAttemps;
            break;
          case SecurityStatus.LoginStatus.WRONG_PASSWORD_USER_IS_BLOCKED:
            break;
          case SecurityStatus.LoginStatus.WRONG_PASSWORD_SUPER_USER:
            break;
          case SecurityStatus.LoginStatus.USER_BLOCKED:
            break;
          case SecurityStatus.LoginStatus.ILLEGAL_ACCESS:
            break;
          case SecurityStatus.LoginStatus.SESION_OPENED:
            break;
          case SecurityStatus.LoginStatus.NOT_PERMISSIONS:
            break;
          case SecurityStatus.LoginStatus.OK_PASSWORD_EXPIRED:
            break;
          case SecurityStatus.LoginStatus.OK_PASSWORD_CHANGE_REQ:
            break;
          default:
            break;
        }
      }

      // insert alarm
      if (_status == SecurityStatus.LoginStatus.WRONG_PASSWORD_SUPER_USER
          || _status == SecurityStatus.LoginStatus.NOT_PERMISSIONS
          || _status == SecurityStatus.LoginStatus.WRONG_PASSWORD_USER_HAS_BEEN_BLOCKED
          || _status == SecurityStatus.LoginStatus.ILLEGAL_ACCESS
          || _status == SecurityStatus.LoginStatus.SESION_OPENED
          || _status == SecurityStatus.LoginStatus.NOT_PERMISSIONS)
      {
        Alarm.Register(AlarmSourceCode.User,                      // SourceCode
                        (Int64)GuiId,                              // SourceId
                        _user_info.user_name + "@" + Environment.MachineName,  // SourceName
                        _alarm_code,                               // Code
                        _description);                            // Description
      }

      return _status;
    } // IsValidUser
  }
}
