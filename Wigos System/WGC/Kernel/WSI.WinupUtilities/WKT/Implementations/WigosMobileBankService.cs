﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WSI.Common;
using WSI.Common.ExternalPaymentAndSaleValidation;
using WSI.WCP;
using WSI.WinupUtilities.WKT.Contracts;
using WSI.WinupUtilities.WKT.Contracts.ContractData.MobiBank;

namespace WSI.WinupUtilities.WKT.Implementations
{
  public class WigosMobileBankService : IMobileBankService
  {
    public Contracts.ContractData.MobiBank.RechargeResponse MobileBankCardTransfer(Int64 requestId, string userTrackData, string pin, long accountId, long amountToTransferx100, int terminalId)
    {
      WigosPromotionService _promotionservices;

      WCP_AccountManager.WCP_Account _account;
      RechargeResponse _response;
      Decimal previous_account_balance;
      Decimal current_account_balance;
      Int64 authorized_amount;
      WCP_ResponseCodes _rc;
      WSI.WCP.Terminal terminalInfo;
      int card_type;

      previous_account_balance = 0;
      current_account_balance = 0;
      _response = new RechargeResponse();
      _promotionservices = new WigosPromotionService();
      card_type = 0;

      using (DB_TRX _db_trx = new DB_TRX())
      {

        string mobileTrackData = "NOT SET";

        if (!CardNumber.TrackDataToExternal(out mobileTrackData, userTrackData, card_type))
        {
          _response.ResponseCode = (int)WCP_ResponseCodes.WCP_RC_CARD_NOT_VALID;
          _response.ResponseCodeText = "Invalid Track Number";
          return _response;
        }

        // Read Account
        _account = WCP_AccountManager.Account(accountId, _db_trx.SqlTransaction);

        // Check if account is anonymous in MB recharge
        if (_account.level == 0 && !GeneralParam.GetBoolean("Cashier", "AllowedAnonymous", true))
        {
          throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_CARD_NOT_VALID, "[Anonymous] Card not authorized");
        }

        // In case of mode Exped activated, an authorization must be requested
        if (ExternalPaymentAndSaleValidationCommon.ExpedMode == EnumExternalPaymentAndSaleValidationMode.MODE_EXPED
        && !Misc.ExternalWS.ExpedRequestAuthorization(_account.id, 0, 0))
        {
          if (ExternalPaymentAndSaleValidationCommon.ExpedCashierPaymentInCashLimit != 0
              && amountToTransferx100 >= ExternalPaymentAndSaleValidationCommon.ExpedCashierPaymentInCashLimit * 100)
          {

            throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_CARD_NOT_VALIDATED, "[Exped] Card not authorized");
          }
        }

        terminalInfo = WSI.WCP.Terminal.GetTerminal(terminalId, _db_trx.SqlTransaction);

        //Misc.GetTerminalName(terminalId, out _terminal_name, _db_trx.SqlTransaction);

        // Check Server or Terminal is blocked.
        if (DbCache.IsTerminalLocked(terminalInfo.serial_number))
        {
          throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_TERMINAL_NOT_AUTHORIZED, "Server or Terminal not authorized. Not found or blocked");
        }

        // Mobile Bank Set last terminal
        WCP_BusinessLogic.DB_MobileBankSetTerminal(mobileTrackData, terminalId, terminalInfo.name, _db_trx.SqlTransaction);

        // Check Mobile account
        _rc = WCP_BusinessLogic.DB_MobileBankAccountCheck(mobileTrackData, pin, amountToTransferx100, out authorized_amount, _db_trx.SqlTransaction);
        switch (_rc)
        {
          case WCP_ResponseCodes.WCP_RC_CARD_WRONG_PIN:
            {
              _response.ResponseCode = (int)WCP_ResponseCodes.WCP_RC_CARD_WRONG_PIN;
              _response.ResponseCodeText = "Wrong Pin.";
            }
            break;

          case WCP_ResponseCodes.WCP_RC_INVALID_AMOUNT:
            {
              _response.ResponseCode = (int)WCP_ResponseCodes.WCP_RC_INVALID_AMOUNT;
              _response.ResponseCodeText = "AuthorizedAmount lower than AmountToTransfer.";
            }
            break;

          case WCP_ResponseCodes.WCP_RC_MB_EXCEEDED_TOTAL_RECHARGES_LIMIT:
            {
              _response.ResponseCode = (int)WCP_ResponseCodes.WCP_RC_MB_EXCEEDED_TOTAL_RECHARGES_LIMIT;
              _response.ResponseCodeText = "Total recharges exceeded total recharges limit for MB.";
            }
            break;

          case WCP_ResponseCodes.WCP_RC_MB_EXCEEDED_RECHARGE_LIMIT:
            {
              _response.ResponseCode = (int)WCP_ResponseCodes.WCP_RC_MB_EXCEEDED_RECHARGE_LIMIT;
              _response.ResponseCodeText = "Recharge exceeded total recharge limit for MB.";
            }
            break;

          case WCP_ResponseCodes.WCP_RC_MB_EXCEEDED_RECHARGES_NUMBER_LIMIT:
            {
              _response.ResponseCode = (int)WCP_ResponseCodes.WCP_RC_MB_EXCEEDED_RECHARGES_NUMBER_LIMIT;
              _response.ResponseCodeText = "AuthorizedAmount lower than AmountToTransfer.";
            }
            break;

          case WCP_ResponseCodes.WCP_RC_MB_SESSION_CLOSED:
            {
              _response.ResponseCode = (int)WCP_ResponseCodes.WCP_RC_MB_SESSION_CLOSED;
              _response.ResponseCodeText = "Session closed.";
            }
            break;

          case WCP_ResponseCodes.WCP_RC_OK:
            {
              // Mobile account transfer credit
              WCP_BusinessLogic.DB_MobileBankAccountTransfer(mobileTrackData,
                                                             _account.ext_track_data,
                                                             terminalId,
                                                             terminalInfo.name,
                                                             amountToTransferx100,
                                                             0,
                                                             0,
                                                             out previous_account_balance,
                                                             out current_account_balance,
                                                             _db_trx.SqlTransaction);

              UpdateRequestStatus(requestId, _db_trx.SqlTransaction);

            }
            break;

          default:
            {
              _response.ResponseCode = (int)WCP_ResponseCodes.WCP_RC_ERROR;
              _response.ResponseCodeText = "DB_MobileBankAccountCheck. Unknown response code: " + _rc.ToString();
            }
            break;
        }

        _db_trx.Commit();

        // Prepare response
        _response.PlayerPreviousCardBalancex100 = (Int64)(previous_account_balance * 100);
        _response.PlayerCurrentCardBalancex100 = (Int64)(current_account_balance * 100);

        if (_rc == WCP_ResponseCodes.WCP_RC_OK)
        {
          _promotionservices.TransferToActiveSlotNew(requestId, _account.ext_track_data);
        }

      }

      return _response;
    }


    private void UpdateRequestStatus(Int64 requestId, SqlTransaction Trx)
    {
      WSI.Common.MobiBank.MobiBankRecharge MBRecharge;

      try
      {
        MBRecharge = new Common.MobiBank.MobiBankRecharge();
        MBRecharge.DB_MobiBankUpdateStatus(requestId, MobileBankStatusRequestRechare.Finished, Trx);
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
    }


    public bool InsertWCPCommand_CancelRequest(int terminalId, long accountId)
    {

      Boolean result = false;
      var XML = new StringBuilder();

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {

          // '<WCP_MsgMobiBankCancelRequest><AccountId>1070234</AccountId><Parameters><CancelReason>3</CancelReason></Parameters></WCP_MsgMobiBankCancelRequest>'

          XML.Append("<WCP_MsgMobiBankCancelRequest>");
          XML.Append("<AccountId>" + accountId.ToString() + "</AccountId>");
          XML.Append("<Parameters>");
          XML.Append("<CancelReason>" + ((int)MobileBankStatusCancelReason.CancelledByRunner).ToString() + "</CancelReason>");
          XML.Append("</Parameters>");
          XML.Append("</WCP_MsgMobiBankCancelRequest>");

          result = WcpCommands.InsertWcpCommand(terminalId, WCP_CommandCode.MobiBankCancelRequest, XML.ToString(), _db_trx.SqlTransaction);

          if (result)
          {
            _db_trx.Commit();
          }
          else
          {
            Log.Warning("WigosMobileBankService.InsertWCPCommand_CancelRequest: Can't cancel request. TerminalId:" + terminalId.ToString());
          }

        }

        return result;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        Log.Warning("WigosMobileBankService.InsertWCPCommand_CancelRequest: Error to send WCP Command (Cancel request).");
        return result;
      }
    }
  }
}
