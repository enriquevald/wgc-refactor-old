﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WigosGiftService.cs
// 
//   DESCRIPTION: Implementation of service to initial DB connect
// 
//        AUTHOR: Sergio Soria
// 
// CREATION DATE: 02-NOV-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 02-NOV-2016 SDS    First release 
// ----------- ------ ----------------------------------------------------------
using System;
using System.Collections;

using WSI.WCP;
using System.Data;
using System.Collections.Generic;
using WSI.WinupUtilities.WKT.Contracts;
using WSI.Common;
using System.Threading;


namespace WSI.WinupUtilities.WKT.Implementations
{
  public class WigosConnectionService 
  {

    public bool Connect(string server1, string server2, string database, string user, string password, int maxPoolSize, string appName, string version, string userBaseName)
    {
      try
      {

        WGDB.CreateConnectionString(server1, server2, database, user, password, 100);
        WGDB.Init(Convert.ToInt32(database), server1, server2);
        WGDB.SetApplication(appName, version);
        WGDB.ConnectAs(userBaseName);

        while (WGDB.ConnectionState != System.Data.ConnectionState.Open)
        {
          Log.Warning("Waiting for database connection ...");

          Thread.Sleep(5000);
        }
        return true;
      }
      catch 
      {
        return false;
      }
    }

    public void InitilizeParameters()
    {
      OperationVoucherParams.LoadData = true;
    }

    public void Init()
    {
      WSI.Common.Resource.Init();
    }
  }
}
