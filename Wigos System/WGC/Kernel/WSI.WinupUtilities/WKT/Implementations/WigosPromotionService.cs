﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WigosPromotionService.cs
// 
//   DESCRIPTION: Implementation of service to get promotions availables from WKT
// 
//        AUTHOR: Ariel Vazquez
// 
// CREATION DATE: 27-Oct-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 27-Oct-2016 AVZ    First release 
// ----------- ------ ----------------------------------------------------------
using System;
using System.Collections.Generic;
using WSI.WCP;
using WSI.WinupUtilities.Util;
using WSI.WinupUtilities.WKT.Contracts;
using WSI.WinupUtilities.WKT.Contracts.ContractData.Promotion;
using WSI.WinupUtilities.WKT.Contracts.Exceptions;
using WSI.Common;
using System.Text;

namespace WSI.WinupUtilities.WKT.Implementations
{
  public class WigosPromotionService : IPromotionService
  {
    public WigosPromotionService()
    {
      WSI.Common.Resource.Init();
    }
    public IList<ApplicablePromotion> GetAccountAvailablePromos(string trackdata, string pin)
    {
      WCP_Message _wcp_request;
      WCP_Message _wcp_response;
      var _request = new WCP_WKT_MsgGetPlayerPromos();
      var _response = new WCP_WKT_MsgGetPlayerPromosReply();

      _request.Player.Trackdata = trackdata;
      _request.Player.Pin = pin;

      var MsgTypeRequest = WCP_MsgTypes.WCP_WKT_MsgGetPlayerPromos;
      _wcp_request = WCP_Message.CreateMessage(MsgTypeRequest);
      _wcp_request.MsgContent = _request;
      _wcp_request.MsgHeader.TerminalId = WSI.Common.GeneralParam.GetString("WinUP", "Wkt.ExternalTerminalId");

      var MsgTypeResponse = WCP_MsgTypes.WCP_WKT_MsgGetPlayerPromosReply;
      _wcp_response = WCP_Message.CreateMessage(MsgTypeResponse);

      WCP_WKT.WKT_ProcessRequest(_wcp_request, _wcp_response);

      if (_wcp_response.MsgHeader.ResponseCode == WCP_ResponseCodes.WCP_RC_WKT_OK)
      {
        _response = (WCP_WKT_MsgGetPlayerPromosReply)_wcp_response.MsgContent;

        var promotionList = new List<ApplicablePromotion>();

        if (_response.ApplicablePromos == null)
          return promotionList;

        _response.ApplicablePromos.ForEach(pl => promotionList.Add(new ApplicablePromotion
        {
          Id = pl.Id,
          AvailableLimit = pl.AvailableLimit,
          CurrentReward = pl.CurrentReward,
          Description = pl.Description,
          LargeResourceId = pl.LargeResourceId,
          MinSpent = pl.MinSpent,
          MinSpentReward = pl.MinSpentReward,
          Name = pl.Name,
          PromoCreditType = (AccountPromoCreditType)pl.PromoCreditType,
          RechargesReward = pl.RechargesReward,
          SmallResourceId = pl.SmallResourceId,
          Spent = pl.Spent,
          SpentReward = pl.SpentReward
        }));

        return promotionList;
      }
      else
      {
        throw new WKTException(WSI.Common.Resource.String(LocalizationHelper.GetResourceKey(_wcp_response.MsgHeader.ResponseCode)), _wcp_response.MsgHeader.ResponseCode);
      }
    }

    public PromotionExchangeResponse ExchangePromotion(string trackdata, string pin, long promotionId, long creditType, decimal reward, bool transferToActiveSlot)
    {
      WSI.Common.OperationVoucherParams.LoadData = true;
      WSI.Common.Resource.Init();

      WCP_Message _wcp_request;
      WCP_Message _wcp_response;
      WCP_WKT_MsgPlayerRequestPromo _request = new WCP_WKT_MsgPlayerRequestPromo();
      WCP_WKT_MsgPlayerRequestPromoReply _response = new WCP_WKT_MsgPlayerRequestPromoReply();

      _request.Player.Trackdata = trackdata;
      _request.Player.Pin = pin;
      _request.PromoId = promotionId;
      _request.PromoCreditType = creditType;
      _request.PromoReward = reward;

      WCP_MsgTypes MsgTypeRequest = WCP_MsgTypes.WCP_WKT_MsgPlayerRequestPromo;
      _wcp_request = WCP_Message.CreateMessage(MsgTypeRequest);
      _wcp_request.MsgContent = _request;
      _wcp_request.MsgHeader.TerminalId = WSI.Common.GeneralParam.GetString("WinUP", "Wkt.ExternalTerminalId");

      WCP_MsgTypes MsgTypeResponse = WCP_MsgTypes.WCP_WKT_MsgPlayerRequestPromoReply;
      _wcp_response = WCP_Message.CreateMessage(MsgTypeResponse);

      WCP_WKT.WKT_ProcessRequest(_wcp_request, _wcp_response);

      if (_wcp_response.MsgHeader.ResponseCode == WCP_ResponseCodes.WCP_RC_WKT_OK)
      {
        _response = (WCP_WKT_MsgPlayerRequestPromoReply)_wcp_response.MsgContent;

        PromotionExchangeResponse response = new PromotionExchangeResponse();
        response.Points = Math.Floor(_response.PromoBalance.Points);
        response.TotalRedeemable = _response.PromoBalance.Balance.TotalRedeemable - _response.PromoBalance.Balance.BucketRECredit;
        response.TotalNotRedeemable = _response.PromoBalance.Balance.TotalNotRedeemable - _response.PromoBalance.Balance.BucketNRCredit;
        response.TotalBalance = response.TotalRedeemable + response.TotalNotRedeemable;

        if (transferToActiveSlot)
        {
          TransferToActiveSlot(trackdata);
        }

        return response;
      }
      else
      {
        throw new WKTException(WSI.Common.Resource.String(LocalizationHelper.GetResourceKey(_wcp_response.MsgHeader.ResponseCode)), _wcp_response.MsgHeader.ResponseCode);
      }
    }

    public void TransferToActiveSlot(string trackdata)
    {
      WCP_AccountManager.WCP_Account _account;
      using (DB_TRX _db_trx = new DB_TRX())
      {
        _account = WCP_AccountManager.Account(trackdata, _db_trx.SqlTransaction);

        // transfer to active slot
        if (_account != null && TerminalFundsTransfer.DB_UpdateTerminalFundsTransferStatus(_account.terminal_id, TerminalFundsTransfer.TRANSFER_STATUS.TRANSFER_PENDING, _db_trx.SqlTransaction))
        {
          WcpCommands.InsertWcpCommand(_account.terminal_id, WCP_CommandCode.RequestTransfer, "", _db_trx.SqlTransaction);

          //m_voucher_list = OutputParameter.VoucherList;
        }
        _db_trx.Commit();
      }

    }


    public bool TransferToActiveSlotNew(Int64 requestId, string trackdata)
    {

      WCP_AccountManager.WCP_Account _account;
      Boolean result = false;
      long id = 0;
      var XML = new StringBuilder();


      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {

          _account = WCP_AccountManager.Account(trackdata, _db_trx.SqlTransaction);

          // transfer to active slot
          if (_account != null && TerminalFundsTransfer.DB_UpdateTerminalFundsTransferStatus(_account.terminal_id, TerminalFundsTransfer.TRANSFER_STATUS.TRANSFER_PENDING, _db_trx.SqlTransaction))
          {

            id = WcpCommands.InsertEmptyWcpCommand(_account.terminal_id, WCP_CommandCode.MobiBankRequestTransFer, _db_trx.SqlTransaction);

            if (id > 0)
            {

              XML.Append("<WCP_MsgMobiBankRequestTransfer>");
              XML.Append("<CmdId>" + id.ToString() + "</CmdId>");
              XML.Append("<Parameters>");
              XML.Append("<RequestId>" + requestId.ToString() + "</RequestId>");
              XML.Append("</Parameters>");
              XML.Append("</WCP_MsgMobiBankRequestTransfer>");

              result = WcpCommands.UpdateCommandMessage(id, XML.ToString(), _db_trx.SqlTransaction);

              if (result)
              {
                _db_trx.Commit();
              }
              else
              {
                Log.Warning("WigosPromotionService.TransferToActiveSlotNew: Can't transfer to slot. TerminalId:" + _account.terminal_id.ToString() + " Status:" + TerminalFundsTransfer.TRANSFER_STATUS.TRANSFER_PENDING.ToString());
              }

            }
            else
            {
              Log.Warning("WigosPromotionService.TransferToActiveSlotNew: Can't transfer to slot. TerminalId:" + _account.terminal_id.ToString() + " Status:" + TerminalFundsTransfer.TRANSFER_STATUS.TRANSFER_PENDING.ToString());
            }

          }

        }

        return result;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        Log.Warning("WigosPromotionService.TransferToActiveSlotNew: Error to transfer to slot.");
        return result;
      }

    }


    //public bool NotifyToSlot(long accountId, int terminalId, long amount, bool egm)
    //{
    //  Boolean result = false;
    //  var XML = new StringBuilder();

    //  try
    //  {

    //    XML.Append("<WCP_MsgMobiBankRechargeNotification>");
    //    XML.Append("<AccountId>@id</AccountId>");
    //    XML.Append("<Parameters>");

    //    if (egm)
    //      XML.Append("<TransferType>" + MobileBankRechargeType.RechargeInEgm + "</TransferType>");
    //    else
    //      XML.Append("<TransferType>" + MobileBankRechargeType.RechargeInAccount + "</TransferType>");

    //    XML.Append("<TransferType>1</TransferType>");
    //    XML.Append("<TransferAmount>@amount</TransferAmount>");
    //    XML.Append("</Parameters>");
    //    XML.Append("</WCP_MsgMobiBankRechargeNotification>");

    //    XML.Replace("@id", accountId.ToString());
    //    XML.Replace("@amount", amount.ToString());

    //    using (DB_TRX _db_trx = new DB_TRX())
    //    {
    //      result = WcpCommands.InsertWcpCommand(terminalId, WCP_CommandCode.MobiBankRechargeNotification, XML.ToString(), _db_trx.SqlTransaction);

    //      if (result)
    //      {
    //        _db_trx.Commit();
    //      }
    //      else
    //      {
    //        Log.Warning("WigosPromotionService.NotifyToSlot: Error to insert in wcp command. Parameters: " + XML.ToString());
    //      }

    //    }

    //    return result;

    //  }
    //  catch (Exception _ex)
    //  {
    //    Log.Exception(_ex);
    //    Log.Warning("WigosPromotionService.NotifyToSlot: Error to insert in wcp command.");
    //    return result;
    //  }
    //}


  }
}
