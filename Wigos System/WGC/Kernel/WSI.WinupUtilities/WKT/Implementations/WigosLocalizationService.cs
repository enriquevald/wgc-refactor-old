﻿using System;
using System.Collections.Generic;
using System.Text;
using WSI.Common;
using WSI.WinupUtilities.WKT.Contracts;

namespace WSI.WinupUtilities.WKT.Implementations
{
  public class WigosLocalizationService : ILocalizationService
  {
    public string GetString(string resourceKey)
    {
      return WSI.Common.Resource.String(resourceKey);
    }

    public string GetStringByLanguage(string resourceKey, string language)
    {
      if (language == "en")
        return WSI.Common.Resource.String_En(resourceKey);
      else
      {
        if (WSI.Common.Resource.LanguageId == (Int32)LanguageIdentifier.Spanish)
          return WSI.Common.Resource.String(resourceKey);
        else
          return WSI.Common.Resource.String_En(resourceKey);
      }
    }
  }
}
