﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WigosGiftService.cs
// 
//   DESCRIPTION: Implementation of service to get gifts availables from WKT
// 
//        AUTHOR: Ariel Vazquez
// 
// CREATION DATE: 27-Oct-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 27-Oct-2016 AVZ    First release 
// ----------- ------ ----------------------------------------------------------
using System;
using System.Collections;

using WSI.WCP;
using System.Data;
using System.Collections.Generic;
using WSI.WinupUtilities.WKT.Contracts;
using WSI.WinupUtilities.WKT.Contracts.ContractData.Gift;
using WSI.WinupUtilities.Util;
using WSI.WinupUtilities.WKT.Contracts.Exceptions;

namespace WSI.WinupUtilities.WKT.Implementations
{
  public class WigosGiftService : IGiftService
  {
    public WigosGiftService()
    {
      WSI.Common.Resource.Init();
    }

    public GiftRequestResponse ExchangeGift(string trackdata, string pin, string giftId, string units, decimal price)
    {
      WSI.Common.OperationVoucherParams.LoadData = true;
      WSI.Common.Resource.Init();
      //string data = WSI.Common.Resource.String("STR_FRM_ACCOUNT_USER_EDIT_ACCOUNT_MODE_ANONYMOUS");
      //bool alla = WSI.Common.GeneralParam.GetBoolean("Intellia", "Camera.Enabled", false);

      WCP_Message _wcp_request;
      WCP_Message _wcp_response;
      WCP_WKT_MsgPlayerRequestGift _request = new WCP_WKT_MsgPlayerRequestGift();
      WCP_WKT_MsgPlayerRequestGiftReply _response = new WCP_WKT_MsgPlayerRequestGiftReply();

      _request.Player.Trackdata = trackdata;
      _request.Player.Pin = pin;
      _request.GiftId = Convert.ToInt64(giftId);
      _request.GiftPrice = price * 100;
      _request.GiftNumUnits = Convert.ToInt32(units);

      WCP_MsgTypes MsgTypeRequest = WCP_MsgTypes.WCP_WKT_MsgPlayerRequestGift;
      _wcp_request = WCP_Message.CreateMessage(MsgTypeRequest);
      _wcp_request.MsgContent = _request;
      _wcp_request.MsgHeader.TerminalId = WSI.Common.GeneralParam.GetString("WinUP", "Wkt.ExternalTerminalId");

      WCP_MsgTypes MsgTypeResponse = WCP_MsgTypes.WCP_WKT_MsgPlayerRequestGiftReply;
      _wcp_response = WCP_Message.CreateMessage(MsgTypeResponse);

      WCP_WKT.WKT_ProcessRequest(_wcp_request, _wcp_response);

      if (_wcp_response.MsgHeader.ResponseCode == WCP_ResponseCodes.WCP_RC_WKT_OK)
      {
        _response = (WCP_WKT_MsgPlayerRequestGiftReply)_wcp_response.MsgContent;

        GiftRequestResponse requestResponse = new GiftRequestResponse();
        requestResponse.Points = Math.Floor(_response.PromoBalance.Points);
        requestResponse.TotalRedeemable = _response.PromoBalance.Balance.TotalRedeemable - _response.PromoBalance.Balance.BucketRECredit;
        requestResponse.TotalNotRedeemable = _response.PromoBalance.Balance.TotalNotRedeemable - _response.PromoBalance.Balance.BucketNRCredit;
        requestResponse.TotalBalance = requestResponse.TotalRedeemable + requestResponse.TotalNotRedeemable;

        return requestResponse;
      }
      else
      {
        throw new WKTException(WSI.Common.Resource.String(LocalizationHelper.GetResourceKey(_wcp_response.MsgHeader.ResponseCode)), _wcp_response.MsgHeader.ResponseCode);
      }
    }

    public IList<Gift> GetAvailableGifts(string trackdata, string pin)
    {
      WCP_Message _wcp_request;
      WCP_Message _wcp_response;
      WCP_WKT_MsgGetPlayerGifts _request = new WCP_WKT_MsgGetPlayerGifts();
      WCP_WKT_MsgGetPlayerGiftsReply _response = new WCP_WKT_MsgGetPlayerGiftsReply();

      _request.Player.Trackdata = trackdata; // "10106490853362732300";
      _request.Player.Pin = pin; // "1234";

      WCP_MsgTypes MsgTypeRequest = WCP_MsgTypes.WCP_WKT_MsgGetPlayerGifts;
      _wcp_request = WCP_Message.CreateMessage(MsgTypeRequest);
      _wcp_request.MsgContent = _request;

      WCP_MsgTypes MsgTypeResponse = WCP_MsgTypes.WCP_WKT_MsgGetPlayerGiftsReply;
      _wcp_response = WCP_Message.CreateMessage(MsgTypeResponse);

      WCP_WKT.WKT_ProcessRequest(_wcp_request, _wcp_response);

      if (_wcp_response.MsgHeader.ResponseCode == WCP_ResponseCodes.WCP_RC_WKT_OK)
      {
        _response = (WCP_WKT_MsgGetPlayerGiftsReply)_wcp_response.MsgContent;

        var redeemeablesGifts = new List<Gift>();
        foreach (DataRow r in _response.Gifts.Tables["RedeemeablesGifts"].Rows)
        {
          redeemeablesGifts.Add(new Gift
          {
            Id = Convert.ToInt64(r["GI_GIFT_ID"]),
            Points = Convert.ToDecimal(r["GI_POINTS"]),
            Name = r["GI_NAME"].ToString(),
            Description = r["GI_DESCRIPTION"].ToString(),
            SmallResourceId = r["GI_SMALL_RESOURCE_ID"] == DBNull.Value ? (long?)null : Convert.ToInt64(r["GI_SMALL_RESOURCE_ID"]),
            LargeResourceId = r["GI_LARGE_RESOURCE_ID"] == DBNull.Value ? (long?)null : Convert.ToInt64(r["GI_LARGE_RESOURCE_ID"]),
            Type = Convert.ToInt32(r["GI_TYPE"]),
            CurrentStock = Convert.ToInt32(r["GI_CURRENT_STOCK"]),
            AllowedUnits = Convert.ToInt32(r["MAX_ALLOWED_UNITS"]),
            NonRedeemable = Convert.ToInt32(r["GI_NON_REDEEMABLE"]),
            DrawId = Convert.ToInt32(r["GI_DRAW_ID"]),
            status = "available"
          });
        }
        //_response.Resources[0].
        var nearGifts = new List<Gift>();
        foreach (DataRow r in _response.Gifts.Tables["NearRedeemableGifts"].Rows)
        {
          nearGifts.Add(new Gift
          {
            Id = Convert.ToInt64(r["GI_GIFT_ID"]),
            Points = Convert.ToDecimal(r["GI_POINTS"]),
            Name = r["GI_NAME"].ToString(),
            Description = r["GI_DESCRIPTION"].ToString(),
            SmallResourceId = r["GI_SMALL_RESOURCE_ID"] == DBNull.Value ? (long?)null : Convert.ToInt64(r["GI_SMALL_RESOURCE_ID"]),
            LargeResourceId = r["GI_LARGE_RESOURCE_ID"] == DBNull.Value ? (long?)null : Convert.ToInt64(r["GI_LARGE_RESOURCE_ID"]),
            Type = Convert.ToInt32(r["GI_TYPE"]),
            CurrentStock = Convert.ToInt32(r["GI_CURRENT_STOCK"]),
            AllowedUnits = Convert.ToInt32(r["MAX_ALLOWED_UNITS"]),
            NonRedeemable = Convert.ToInt32(r["GI_NON_REDEEMABLE"]),
            DrawId = Convert.ToInt32(r["GI_DRAW_ID"]),
            status = "notavailable"
          });
        }

        redeemeablesGifts.AddRange(nearGifts);
        
        return redeemeablesGifts;
      }
      else
      {
        throw new WKTException(WSI.Common.Resource.String(LocalizationHelper.GetResourceKey(_wcp_response.MsgHeader.ResponseCode)), _wcp_response.MsgHeader.ResponseCode);
      }
    }

    public IList<PendingGift> GetPendingGifts(string trackdata, string pin)
    {
      long accountId;
      WCP_ResponseCodes loginResponseCode;
      if (!WCP_WKT.WKT_PlayerLogin(trackdata, pin, out accountId, out loginResponseCode))
        throw new WKTException(WSI.Common.Resource.String(LocalizationHelper.GetResourceKey(loginResponseCode)), loginResponseCode);

      using (WSI.Common.DB_TRX _db_trx = new WSI.Common.DB_TRX())
      {
        DataTable _pendingGiftsTable = WSI.Common.GiftInstance.GetPendingGifts(accountId, _db_trx.SqlTransaction);
        var pendingGifts = new List<PendingGift>();
        foreach (DataRow r in _pendingGiftsTable.Rows)
        {
          pendingGifts.Add(new PendingGift
          {
            GiftId = Convert.ToInt64(r["GIN_GIFT_ID"]),
            InstanceId = Convert.ToInt64(r["GIN_GIFT_INSTANCE_ID"]),
            Name = r["GIN_GIFT_NAME"].ToString(),
            Type = Convert.ToInt32(r["GIN_GIFT_TYPE"]),
            SpentPoints = Convert.ToDecimal(r["GIN_SPENT_POINTS"]),
            ConversionToNRC = r["GIN_CONVERSION_TO_NRC"] == DBNull.Value ? (long?)null : Convert.ToInt64(r["GIN_CONVERSION_TO_NRC"]),
            Description = r["GI_DESCRIPTION"].ToString(),
            Date = Convert.ToDateTime(r["GIN_REQUESTED"]).ToLongDateString(),
            SmallResourceId = r["GI_SMALL_RESOURCE_ID"] == DBNull.Value ? (long?)null : Convert.ToInt64(r["GI_SMALL_RESOURCE_ID"]),
            LargeResourceId = r["GI_LARGE_RESOURCE_ID"] == DBNull.Value ? (long?)null : Convert.ToInt64(r["GI_LARGE_RESOURCE_ID"]),
            Quantity = Convert.ToInt32(r["GIN_NUM_ITEMS"]),
            Voucher = r["VOUCHER"].ToString(),
            Barcode = this.GetBarcode(r["VOUCHER"].ToString())
          });
        }

        return pendingGifts;
      }
    }

    private string GetBarcode(string voucherHTML)
    {
      string barcodeJqueryStartText = "$('#inputdata').barcode('";
      string barcodeJqueryEndText = "'";

      if (!voucherHTML.Contains(barcodeJqueryStartText))
        return string.Empty;

      int startIndex = voucherHTML.IndexOf(barcodeJqueryStartText, StringComparison.InvariantCultureIgnoreCase) + barcodeJqueryStartText.Length;
      int endIndex = voucherHTML.IndexOf(barcodeJqueryEndText, startIndex, StringComparison.InvariantCultureIgnoreCase);

      return voucherHTML.Substring(startIndex, endIndex - startIndex);
    }
  }
}
