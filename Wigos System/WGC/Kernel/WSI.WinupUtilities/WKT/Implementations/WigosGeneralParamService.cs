﻿using System;
using System.Collections.Generic;
using System.Text;
using WSI.WinupUtilities.WKT.Contracts;

namespace WSI.WinupUtilities.WKT.Implementations
{
  public class WigosGeneralParamService : IGeneralParamService
  {
    public string GetString(string groupKey, string subjectKey, string defaultValue)
    {
      return WSI.Common.GeneralParam.GetString(groupKey, subjectKey, defaultValue);
    }

    public int GetInt32(string groupKey, string subjectKey, int defaultValue)
    {
      return WSI.Common.GeneralParam.GetInt32(groupKey, subjectKey, defaultValue);
    }

    public bool GetBoolean(string groupKey, string subjectKey, bool defaultValue)
    {
      return WSI.Common.GeneralParam.GetBoolean(groupKey, subjectKey, defaultValue);
    }
  }
}
