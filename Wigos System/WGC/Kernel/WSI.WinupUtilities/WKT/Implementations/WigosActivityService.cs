﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WigosActivityService.cs
// 
//   DESCRIPTION: Implementation of service to get players activity from WKT
// 
//        AUTHOR: Ariel Vazquez
// 
// CREATION DATE: 27-Oct-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 27-Oct-2016 AVZ    First release 
// ----------- ------ ----------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Resources;
using WSI.WCP;
using WSI.WinupUtilities.Util;
using WSI.WinupUtilities.WKT.Contracts;
using WSI.WinupUtilities.WKT.Contracts.ContractData.Activity;

namespace WSI.WinupUtilities.WKT.Implementations
{
  public class WigosActivityService : IActivityService
  {
    public WigosActivityService()
    {
      WSI.Common.Resource.Init();
    }
    public IList<Activity> GetPlayerActivity(string trackData, string pin)
    {
      WCP_Message _wcp_request;
      WCP_Message _wcp_response;
      var _request = new WCP_WKT_MsgGetPlayerActivity();
      var _response = new WCP_WKT_MsgGetPlayerActivityReply();

      var MsgTypeRequest = WCP_MsgTypes.WCP_WKT_MsgGetPlayerActivity;
      _wcp_request = WCP_Message.CreateMessage(MsgTypeRequest);
      _wcp_request.MsgContent = _request;
      _wcp_request.MsgHeader.TerminalId = WSI.Common.GeneralParam.GetString("WinUP", "Wkt.ExternalTerminalId");

      var MsgTypeResponse = WCP_MsgTypes.WCP_WKT_MsgGetPlayerActivityReply;
      _wcp_response = WCP_Message.CreateMessage(MsgTypeResponse);

      _request.Player.Trackdata = trackData;
      _request.Player.Pin = pin;

      WCP_WKT.WKT_ProcessRequest(_wcp_request, _wcp_response);

      if (_wcp_response.MsgHeader.ResponseCode == WCP_ResponseCodes.WCP_RC_WKT_OK)
      {
        _response = (WCP_WKT_MsgGetPlayerActivityReply)_wcp_response.MsgContent;

        var activityList = new List<Activity>();
        _response.Activities.ForEach(al => activityList.Add(new Activity
        {
          Date = Format.DateString(al.Date),
          Description = al.Description,
          Quantity = al.Quantity
        }));

        return activityList;
      }
      else
      {
        return null;
      }
    }
  }
}
