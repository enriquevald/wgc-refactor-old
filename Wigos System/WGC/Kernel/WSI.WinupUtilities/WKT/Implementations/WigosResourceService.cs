﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WigosResourceService.cs
// 
//   DESCRIPTION: Implementation of service to get the resources of the promotions and gifts from WKT
// 
//        AUTHOR: Ariel Vazquez
// 
// CREATION DATE: 27-Oct-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 27-Oct-2016 AVZ    First release 
// ----------- ------ ----------------------------------------------------------
using System;
using System.Collections.Generic;
using WSI.WCP;
using WSI.WinupUtilities.WKT.Contracts;
using WSI.WinupUtilities.WKT.Contracts.ContractData.Resource;

namespace WSI.WinupUtilities.WKT.Implementations
{
  public class WigosResourceService : IResourceService
  {
    public string CountryISOCode2()
    {
      return  WSI.Common.Resource.CountryISOCode2;
    }
    public WigosResourceService()
    {
      WSI.Common.Resource.Init();
     
    }
    public Resource GetResource(string id)
    {
      Resource resource;

      WCP_Message _wcp_request;
      WCP_Message _wcp_response;
      WCP_WKT_MsgGetResource _request = new WCP_WKT_MsgGetResource();
      WCP_WKT_MsgGetResourceReply _response = new WCP_WKT_MsgGetResourceReply();

      _request.WktResourceId = Convert.ToInt64(id);

      WCP_MsgTypes MsgTypeRequest = WCP_MsgTypes.WCP_WKT_MsgPlayerRequestGift;
      _wcp_request = WCP_Message.CreateMessage(MsgTypeRequest);
      _wcp_request.MsgContent = _request;

      WCP_MsgTypes MsgTypeResponse = WCP_MsgTypes.WCP_WKT_MsgPlayerRequestGiftReply;
      _wcp_response = WCP_Message.CreateMessage(MsgTypeResponse);

      WCP_WKT.WKT_ProcessRequest(_wcp_request, _wcp_response);

      if (_wcp_response.MsgHeader.ResponseCode == WCP_ResponseCodes.WCP_RC_WKT_OK)
      {
        _response = (WCP_WKT_MsgGetResourceReply)_wcp_response.MsgContent;

        if (_response.ChunkDataLength <= 0)
          return null;

        resource = new Resource();
        resource.Extension = _response.Extension;
        resource.Content = _response.ChunkData;
        resource.ContentHash = _response.Hash;

        return resource;
      }
      else
      {
        return null;
      }
    }
  }
}
