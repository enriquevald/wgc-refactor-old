﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WigosPlayerInfoService.cs
// 
//   DESCRIPTION: Implementation of service to get player info from WKT
// 
//        AUTHOR: Ariel Vazquez
// 
// CREATION DATE: 27-Oct-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 27-Oct-2016 AVZ    First release 
// 01-DIC-2016 LA/SDS Bring all user data    
// ----------- ------ ----------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using WSI.WCP;
using WSI.WinupUtilities.Util;
using WSI.WinupUtilities.WKT.Contracts;
using WSI.WinupUtilities.WKT.Contracts.ContractData.PlayerInfo;
using WSI.WinupUtilities.WKT.Contracts.Exceptions;
using WSI.WinupUtilities.WKT.Implementations.DBEntities;

namespace WSI.WinupUtilities.WKT.Implementations
{
  public class WigosPlayerInfoService : IPlayerInfoService
  {
    public WigosPlayerInfoService()
    {
      WSI.Common.Resource.Init();
    }
    public PlayerInfo GetBalance(string trackData, string pin)
    {
      WCP_Message _wcp_request;
      WCP_Message _wcp_response;
      var _request = new WCP_WKT_MsgGetPlayerInfo();
      var _response = new WCP_WKT_MsgGetPlayerInfoReply();

      var MsgTypeRequest = WCP_MsgTypes.WCP_WKT_MsgGetPlayerInfo;
      _wcp_request = WCP_Message.CreateMessage(MsgTypeRequest);
      _wcp_request.MsgContent = _request;
      _wcp_request.MsgHeader.TerminalId = WSI.Common.GeneralParam.GetString("WinUP", "Wkt.ExternalTerminalId");

      var MsgTypeResponse = WCP_MsgTypes.WCP_WKT_MsgGetPlayerInfoReply;
      _wcp_response = WCP_Message.CreateMessage(MsgTypeResponse);

      _request.Player.Trackdata = trackData;
      _request.Player.Pin = pin;

      WCP_WKT.WKT_ProcessRequest(_wcp_request, _wcp_response);

      if (_wcp_response.MsgHeader.ResponseCode == WCP_ResponseCodes.WCP_RC_WKT_OK)
      {
        _response = (WCP_WKT_MsgGetPlayerInfoReply)_wcp_response.MsgContent;

        var playerInfoReponse = _response.PlayerInfo;
        var playerInfo = new PlayerInfo();

        #region BalanceParts

        var promotionList = new List<PlayerInfo.BalancePartsPromotion>();

        if (playerInfoReponse.BalanceParts != null && playerInfoReponse.BalanceParts.ActivePromotions.Count > 0)
          playerInfoReponse.BalanceParts.ActivePromotions.ForEach(pl => promotionList.Add(new PlayerInfo.BalancePartsPromotion
          {
            AwardedBalance = new Currency { CurrencyIsoCode = pl.AwardedBalance.CurrencyIsoCode, Value = pl.AwardedBalance },
            CurrentBalance = new Currency { CurrencyIsoCode = pl.CurrentBalance.CurrencyIsoCode, Value = pl.CurrentBalance },
            PromoDate = pl.PromoDate,
            PromoName = pl.PromoName
          }));

        playerInfo.Parts = new PlayerInfo.BalanceParts
        {
          ActivePromotions = promotionList,
          RedeemableBalance = (playerInfoReponse.BalanceParts != null) ? new Currency { CurrencyIsoCode = playerInfoReponse.BalanceParts.RedeemableBalance.CurrencyIsoCode, Value = playerInfoReponse.BalanceParts.RedeemableBalance } : null,
          NonRedeemableBalance = (playerInfoReponse.BalanceParts != null) ? new Currency { CurrencyIsoCode = playerInfoReponse.BalanceParts.NonRedeemableBalance.CurrencyIsoCode, Value = playerInfoReponse.BalanceParts.NonRedeemableBalance } : null,
        };

        #endregion BalanceParts

        #region AccountLevel

        playerInfo.Level = new PlayerInfo.AccountLevel
        {
          EnterDate = (playerInfoReponse.Level != null) ? playerInfoReponse.Level.EnterDate : DateTime.MinValue,
          LevelId = (playerInfoReponse.Level != null) ? playerInfoReponse.Level.LevelId : 0,
          Expiration = (playerInfoReponse.Level != null) ? playerInfoReponse.Level.Expiration : DateTime.MinValue,
          LevelName = (playerInfoReponse.Level != null) ? playerInfoReponse.Level.LevelName : string.Empty,
          PointsToEnter = new Currency { CurrencyIsoCode = playerInfoReponse.Level.PointsToEnter.CurrencyIsoCode, Value = playerInfoReponse.Level.PointsToEnter },
          Points = new Currency { Value = Math.Floor(playerInfoReponse.PointsBalance.SqlMoney.Value) },
        };

        #endregion AccountLevel

        #region AccountNextLevel

        playerInfo.NextLevel = new PlayerInfo.AccountNextLevel
        {
          PointsDisc = new Points { Value = playerInfoReponse.NextLevel.PointsDiscretionaries.SqlMoney.Value },
          LevelId = (playerInfoReponse.NextLevel != null) ? playerInfoReponse.NextLevel.LevelId : 0,
          PointsGenerated = new Points { Value = playerInfoReponse.NextLevel.PointsGenerated.SqlMoney.Value },
          LevelName = (playerInfoReponse.NextLevel != null) ? playerInfoReponse.NextLevel.LevelName : string.Empty,
          PointsToReach = new Points { Value = playerInfoReponse.NextLevel.PointsToReach.SqlMoney.Value },
        };

        #endregion AccountNextLevel

        return playerInfo;
      }
      else
      {
        throw new WKTException(WSI.Common.Resource.String(LocalizationHelper.GetResourceKey(_wcp_response.MsgHeader.ResponseCode)), _wcp_response.MsgHeader.ResponseCode);
      }
    }


    public PlayerInfo GetPersonalData(string trackData, string pin)
    {
      WCP_Message _wcp_request;
      WCP_Message _wcp_response;
      var _request = new WCP_WKT_MsgGetPlayerInfo();
      var _response = new WCP_WKT_MsgGetPlayerInfoReply();

      var MsgTypeRequest = WCP_MsgTypes.WCP_WKT_MsgGetPlayerInfo;
      _wcp_request = WCP_Message.CreateMessage(MsgTypeRequest);
      _wcp_request.MsgContent = _request;
      _wcp_request.MsgHeader.TerminalId = WSI.Common.GeneralParam.GetString("WinUP", "Wkt.ExternalTerminalId");

      var MsgTypeResponse = WCP_MsgTypes.WCP_WKT_MsgGetPlayerInfoReply;
      _wcp_response = WCP_Message.CreateMessage(MsgTypeResponse);

      _request.Player.Trackdata = trackData;
      _request.Player.Pin = pin;


      WCP_WKT.WKT_ProcessRequest(_wcp_request, _wcp_response);

      if (_wcp_response.MsgHeader.ResponseCode == WCP_ResponseCodes.WCP_RC_WKT_OK)
      {
        _response = (WCP_WKT_MsgGetPlayerInfoReply)_wcp_response.MsgContent;

        var playerInfoReponse = _response.PlayerInfo;
        var plaverFieldsResponse = _response.PlayerFields;
        var playerInfo = new PlayerInfo();

        #region PersonalData
        WCP_AccountManager.WCP_Account _account;
        using (WSI.Common.DB_TRX _db_trx = new WSI.Common.DB_TRX())
        {
          _account = WCP_AccountManager.Account(trackData, _db_trx.SqlTransaction);
          _db_trx.Commit();
        }

        playerInfo.PersonalData = new PlayerInfo.Info
        {
          Gender = (WSI.WinupUtilities.WKT.Contracts.ContractData.PlayerInfo.DrawGenderFilter)playerInfoReponse.Gender,
          Birthday = playerInfoReponse.Birthday,
          FullName = playerInfoReponse.FullName,
          avatar = null,
          AccountId = _account != null ? _account.id : 0
        };

        for (var i = 0; i < plaverFieldsResponse.FieldsNumber; i++)
        {
          WCP_WKT.WKT_IPlayerField _field = plaverFieldsResponse.GetField(i);
          _field.Value = _field.Value.Trim();


          switch ((PlayerInfoFieldType)_field.FieldId)
          {
            case PlayerInfoFieldType.ADDRESS_01:
              playerInfo.PersonalData.Address1 = _field.Value;
              break;
            case PlayerInfoFieldType.ADDRESS_02:
              playerInfo.PersonalData.Address2 = _field.Value;
              break;
            case PlayerInfoFieldType.ADDRESS_03:
              playerInfo.PersonalData.Address3 = _field.Value;
              break;
            case PlayerInfoFieldType.CITY:
              playerInfo.PersonalData.City = _field.Value;
              break;
            case PlayerInfoFieldType.EMAIL_01:
              playerInfo.PersonalData.Email1 = _field.Value;
              break;
            case PlayerInfoFieldType.EMAIL_02:
              playerInfo.PersonalData.Email2 = _field.Value;
              break;
            case PlayerInfoFieldType.NAME:
              playerInfo.PersonalData.Name = _field.Value;
              break;
            case PlayerInfoFieldType.PHONE_NUMBER_01:
              playerInfo.PersonalData.PhoneNumber1 = _field.Value;
              break;
            case PlayerInfoFieldType.PHONE_NUMBER_02:
              playerInfo.PersonalData.PhoneNumber2 = _field.Value;
              break;
            case PlayerInfoFieldType.PIN:
              playerInfo.PersonalData.Pin = _field.Value;
              break;
            case PlayerInfoFieldType.SURNAME1:
              playerInfo.PersonalData.SurName1 = _field.Value;
              break;
            case PlayerInfoFieldType.SURNAME2:
              playerInfo.PersonalData.SurName2 = _field.Value;
              break;
            case PlayerInfoFieldType.ZIP:
              playerInfo.PersonalData.Zip = _field.Value;
              break;
          }
        }

        #endregion PersonalData

        #region BalanceParts

        var promotionList = new List<PlayerInfo.BalancePartsPromotion>();

        if (playerInfoReponse.BalanceParts != null && playerInfoReponse.BalanceParts.ActivePromotions.Count > 0)
          playerInfoReponse.BalanceParts.ActivePromotions.ForEach(pl => promotionList.Add(new PlayerInfo.BalancePartsPromotion
          {
            AwardedBalance = new Currency { CurrencyIsoCode = pl.AwardedBalance.CurrencyIsoCode, Value = pl.AwardedBalance },
            CurrentBalance = new Currency { CurrencyIsoCode = pl.CurrentBalance.CurrencyIsoCode, Value = pl.CurrentBalance },
            PromoDate = pl.PromoDate,
            PromoName = pl.PromoName
          }));

        playerInfo.Parts = new PlayerInfo.BalanceParts
        {
          ActivePromotions = promotionList,
          RedeemableBalance = (playerInfoReponse.BalanceParts != null) ? new Currency { CurrencyIsoCode = playerInfoReponse.BalanceParts.RedeemableBalance.CurrencyIsoCode, Value = playerInfoReponse.BalanceParts.RedeemableBalance } : null,
          NonRedeemableBalance = (playerInfoReponse.BalanceParts != null) ? new Currency { CurrencyIsoCode = playerInfoReponse.BalanceParts.NonRedeemableBalance.CurrencyIsoCode, Value = playerInfoReponse.BalanceParts.NonRedeemableBalance } : null,
        };

        #endregion BalanceParts

        #region AccountLevel

        playerInfo.Level = new PlayerInfo.AccountLevel
        {
          EnterDate = (playerInfoReponse.Level != null) ? playerInfoReponse.Level.EnterDate : DateTime.MinValue,
          LevelId = (playerInfoReponse.Level != null) ? playerInfoReponse.Level.LevelId : 0,
          Expiration = (playerInfoReponse.Level != null) ? playerInfoReponse.Level.Expiration : DateTime.MinValue,
          LevelName = (playerInfoReponse.Level != null) ? playerInfoReponse.Level.LevelName : string.Empty,
          PointsToEnter = new Currency { CurrencyIsoCode = playerInfoReponse.Level.PointsToEnter.CurrencyIsoCode, Value = playerInfoReponse.Level.PointsToEnter },
          Points = new Currency { Value = Math.Floor(playerInfoReponse.PointsBalance.SqlMoney.Value) },
        };

        #endregion AccountLevel

        #region AccountNextLevel

        playerInfo.NextLevel = new PlayerInfo.AccountNextLevel
        {
          PointsDisc = new Points { Value = playerInfoReponse.NextLevel.PointsDiscretionaries.SqlMoney.Value },
          LevelId = (playerInfoReponse.NextLevel != null) ? playerInfoReponse.NextLevel.LevelId : 0,
          PointsGenerated = new Points { Value = playerInfoReponse.NextLevel.PointsGenerated.SqlMoney.Value },
          LevelName = (playerInfoReponse.NextLevel != null) ? playerInfoReponse.NextLevel.LevelName : string.Empty,
          PointsToReach = new Points { Value = playerInfoReponse.NextLevel.PointsToReach.SqlMoney.Value },
        };

        #endregion AccountNextLevel

        return playerInfo;
      }
      else
      {
        throw new WKTException(WSI.Common.Resource.String(LocalizationHelper.GetResourceKey(_wcp_response.MsgHeader.ResponseCode)), _wcp_response.MsgHeader.ResponseCode);
      }
    }


    public Common.CardData GetPersonalData(int documentType, string documentNumber)
    {
      WSI.Common.CardData cardData = new WSI.Common.CardData();
      if (WSI.Common.CardData.DB_CardGetPersonalData(documentType, documentNumber, cardData))
        return cardData;

      return null;
    }
    // 01-DIC-2016 LA/SDS
    public Common.CardData GetAllData(int accountId)
    {
      WSI.Common.CardData cardData = new WSI.Common.CardData();
      if (WSI.Common.CardData.DB_CardGetAllData(accountId, cardData))
        return cardData;
      return null;
    }

    public Common.CardData AuthenticatePlayer(string username, string password)
    {
      try
      {
        using (WSI.Common.DB_TRX _db_trx = new WSI.Common.DB_TRX())
        {
          long _accountId = this.GetAccountIdOfOnlineCasinoAccount(username, password, _db_trx.SqlTransaction);
          return GetAllData((int)_accountId);
        }
      }
      catch (Exception e)
      {
        //Log error
        throw e;
      }
    }

    /// <summary>
    /// Create an Online Casino Account for a landbase account
    /// </summary>
    /// <param name="accountId">Landbase account id</param>
    /// <param name="onlineAccountId">Online account id</param>
    /// <param name="username">Username</param>
    /// <param name="password">Password</param>
    /// <returns>Result of the creation process</returns>
    public LinkAccountResult CreateOnlineCasinoAccount(long accountId, string onlineAccountId, string username, string password)
    {
      try
      {
        using (WSI.Common.DB_TRX _db_trx = new WSI.Common.DB_TRX())
        {
          bool state = CreateOnlineCasinoAccountOnDataBase(accountId, onlineAccountId, username, password, _db_trx.SqlTransaction);
          return new LinkAccountResult
          {
            State = state,
            Message = "Account Created"
          };
        }
      }
      catch (Exception e)
      {
        return new LinkAccountResult
        {
          State = false,
          Message = e.Message
        };
      }
    }

    /// <summary>
    /// Check is already exist an online casino account for a landbase account
    /// </summary>
    /// <param name="accountId">Landbase account id</param>
    /// <returns>Result of the creation process</returns>
    public bool HasOnlineCasinoAccount(long accountId)
    {
      try
      {
        using (WSI.Common.DB_TRX _db_trx = new WSI.Common.DB_TRX())
        {
          return HasOnlineCasinoAccountOnDataBase(accountId, _db_trx.SqlTransaction);
        }
      }
      catch (Exception)
      {
        return false;
      }
    }

    public WSI.Common.MultiPromos.AccountBalance RemoveCreditFromAccount(long accountId, decimal amount, Provider provider, string transactionId)
    {
      TransferState transferState = new TransferState
      {
        Amount = amount,
        Provider = provider,
        TransactionId = transactionId,
        TransferType = TransferType.DEBIT
      };
      try
      {
        using (WSI.Common.DB_TRX _db_trx = new WSI.Common.DB_TRX())
        {
          //Traer los datos de la cuenta
          WSI.Common.CardData _cardData = GetCardData(accountId, _db_trx.SqlTransaction);

          //Chequear los fondos de la cuenta 
          if (_cardData.AccountBalance.TotalRedeemable < amount)
            throw new Exception("Amount must be less or equal than redeemable");

          //Actualizar el balance de la cuenta
          WSI.Common.MultiPromos.AccountBalance _final_balance = UpdateBalance(_cardData, amount, Common.MovementType.CashOut, _db_trx.SqlTransaction);

          //Insertar estado de transferencia 
          transferState.TransferStatus = TransferStatus.COMPLETED;
          InsertTransferState(transferState, _db_trx.SqlTransaction);

          _db_trx.Commit();

          return _final_balance;
        }
      }
      catch (Exception ex)
      {
        transferState.TransferStatus = TransferStatus.ERROR;
        InsertTransferState(transferState);
        throw ex;
      }
    }

    public WSI.Common.MultiPromos.AccountBalance AddCreditToAccount(long accountId, decimal amount, Provider provider, string transactionId)
    {
      TransferState transferState = new TransferState
      {
        Amount = amount,
        Provider = provider,
        TransactionId = transactionId,
        TransferType = TransferType.CREDIT
      };
      try
      {
        using (WSI.Common.DB_TRX _db_trx = new WSI.Common.DB_TRX())
        {
          //Traer los datos de la cuenta
          WSI.Common.CardData _cardData = GetCardData(accountId, _db_trx.SqlTransaction);

          //Actualizar el balance de la cuenta
          WSI.Common.MultiPromos.AccountBalance _final_balance = UpdateBalance(_cardData, amount, Common.MovementType.CashIn, _db_trx.SqlTransaction);

          //Agrega el estado de transferencia
          transferState.TransferStatus = TransferStatus.COMPLETED;
          InsertTransferState(transferState, _db_trx.SqlTransaction);

          _db_trx.Commit();

          return _final_balance;
        }
      }
      catch (Exception ex)
      {
        transferState.TransferStatus = TransferStatus.ERROR;
        InsertTransferState(transferState);
        throw ex;
      }
    }

    private WSI.Common.CardData GetCardData(long accountId, SqlTransaction sqlTrx)
    {
      //Traer los datos de la cuenta
      WSI.Common.CardData _cardData = new WSI.Common.CardData();
      if (!WSI.Common.CardData.DB_CardGetAllData(accountId, _cardData, sqlTrx))
        throw new Exception("Invalid Account");

      //Verificar si la cuenta está bloqueada
      if (_cardData.Blocked)
        throw new Exception("Account blocked");

      return _cardData;
    }

    private WSI.Common.MultiPromos.AccountBalance UpdateBalance(WSI.Common.CardData cardData, decimal amount, WSI.Common.MovementType movementType, SqlTransaction sqlTrx)
    {
      //Lockear la cuenta
      WSI.Common.MultiPromos.AccountBalance _account_balance;
      if (!WSI.Common.MultiPromos.Trx_UpdateAccountBalance(cardData.AccountId, WSI.Common.MultiPromos.AccountBalance.Zero, out _account_balance, sqlTrx))
        throw new Exception("UpdateAccountBalance - Error");

      //Actualizar los datos del balance de cuenta
      WSI.Common.MultiPromos.AccountBalance _balance = new WSI.Common.MultiPromos.AccountBalance();
      decimal _add_ammount = 0;
      decimal _sub_ammount = 0;
      if (movementType == Common.MovementType.CashIn)
      {
        _balance.Redeemable = amount;
        _add_ammount = amount;
      }
      else
      {
        if (cardData.AccountBalance.Redeemable >= amount)
        {
          _balance.Redeemable = amount;
        }
        else
        {
          _balance.Redeemable = cardData.AccountBalance.Redeemable;
          _balance.PromoRedeemable = amount - _balance.Redeemable;
        }
        _sub_ammount = amount;
        _balance = WSI.Common.MultiPromos.AccountBalance.Negate(_balance);
      }

      WSI.Common.MultiPromos.AccountBalance _final_balance;
      if (!WSI.Common.MultiPromos.Trx_UpdateAccountBalance(cardData.AccountId, _balance, out _final_balance, sqlTrx))
        throw new Exception("UpdateAccountBalance - Error");

      //Crear los movimientos de cuenta necesarios
      WSI.Common.CashierSessionInfo _cashierSessionInfo = WSI.Common.Cashier.GetSystemCashierSessionInfo(WSI.Common.GU_USER_TYPE.SYS_SYSTEM, sqlTrx);
      WSI.Common.AccountMovementsTable _account_movements = new WSI.Common.AccountMovementsTable(_cashierSessionInfo);
      if (!_account_movements.Add(0, cardData.AccountId, movementType, cardData.AccountBalance.TotalBalance, _sub_ammount, _add_ammount, _final_balance.TotalBalance))
        throw new Exception("AccountMovementsAdd - Error");

      _account_movements.Save(sqlTrx);

      return _final_balance;
    }

    private void InsertTransferState(TransferState transferState)
    {
      using (WSI.Common.DB_TRX _db_trx = new WSI.Common.DB_TRX())
      {
        InsertTransferState(transferState, _db_trx.SqlTransaction);
        _db_trx.Commit();
      }
    }

    private void InsertTransferState(TransferState transferState, SqlTransaction sqlTrx)
    {
      StringBuilder _sb = new StringBuilder();
      _sb.AppendLine(" INSERT INTO ONLINE_CASINO_TRANSFERS_STATES ");
      _sb.AppendLine(" (OCTS_AMOUNT, ");
      _sb.AppendLine(" OCTS_PROVIDER, ");
      _sb.AppendLine(" OCTS_TRANSACTION_ID, ");
      _sb.AppendLine(" OCTS_STATUS, ");
      _sb.AppendLine(" OCTS_TYPE) ");
      _sb.AppendLine(" VALUES ");
      _sb.AppendLine(" (@pAmount, ");
      _sb.AppendLine(" @pProvider, ");
      _sb.AppendLine(" @pTransactionId, ");
      _sb.AppendLine(" @pTransferStatus, ");
      _sb.AppendLine(" @pTransferType) ");

      using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), sqlTrx.Connection, sqlTrx))
      {
        _cmd.Parameters.Add("@pAmount", SqlDbType.Decimal).Value = transferState.Amount;
        _cmd.Parameters.Add("@pProvider", SqlDbType.SmallInt).Value = (int)transferState.Provider;
        _cmd.Parameters.Add("@pTransactionId", SqlDbType.VarChar).Value = transferState.TransactionId;
        _cmd.Parameters.Add("@pTransferStatus", SqlDbType.SmallInt).Value = (int)transferState.TransferStatus;
        _cmd.Parameters.Add("@pTransferType", SqlDbType.SmallInt).Value = (int)transferState.TransferType;
        _cmd.ExecuteNonQuery();
      }
    }
    private TransferState GetTransferState(string transactionId, SqlTransaction sqlTrx)
    {
      StringBuilder _sb = new StringBuilder();
      _sb.AppendLine(" SELECT ");
      _sb.AppendLine(" OCTS_ID, ");
      _sb.AppendLine(" OCTS_AMOUNT, ");
      _sb.AppendLine(" OCTS_PROVIDER, ");
      _sb.AppendLine(" OCTS_TRANSACTION_ID, ");
      _sb.AppendLine(" OCTS_STATUS, ");
      _sb.AppendLine(" OCTS_TYPE ");
      _sb.AppendLine(" FROM ONLINE_CASINO_TRANSFERS_STATES ");
      _sb.AppendLine(" WHERE OCTS_TRANSACTION_ID = @pTransactionId ");

      using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), sqlTrx.Connection, sqlTrx))
      {
        _cmd.Parameters.Add("@pTransactionId", SqlDbType.VarChar).Value = transactionId;

        using (SqlDataReader _reader = _cmd.ExecuteReader())
        {
          if (!_reader.Read())
            return null;

          TransferState transferState = new TransferState();
          transferState.Id = (long)_reader["OCTS_ID"];
          transferState.Amount = (decimal)_reader["OCTS_AMOUNT"];
          transferState.Provider = (Provider)((Int16)_reader["OCTS_PROVIDER"]);
          transferState.TransactionId = (string)_reader["OCTS_TRANSACTION_ID"];
          transferState.TransferStatus = (TransferStatus)((Int16)_reader["OCTS_STATUS"]);
          transferState.TransferType = (TransferType)((Int16)_reader["OCTS_TYPE"]);

          return transferState;
        }
      }
    }

    private long GetAccountIdOfOnlineCasinoAccount(string username, string password, SqlTransaction sqlTrx)
    {
      StringBuilder _sb = new StringBuilder();
      _sb.AppendLine(" SELECT ");
      _sb.AppendLine(" OCA_ACCOUNT_ID ");
      _sb.AppendLine(" FROM ONLINE_CASINO_ACCOUNTS ");
      _sb.AppendLine(" WHERE OCA_USERNAME = @pUsername ");
      _sb.AppendLine(" AND OCA_PASSWORD = @pPassword ");

      using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), sqlTrx.Connection, sqlTrx))
      {
        _cmd.Parameters.Add("@pUsername", SqlDbType.VarChar).Value = username;
        _cmd.Parameters.Add("@pPassword", SqlDbType.VarChar).Value = password;

        long _accountId = (long)_cmd.ExecuteScalar();

        return _accountId;
      }
    }

    private bool CreateOnlineCasinoAccountOnDataBase(long accountId, string onlineAccountId, string username, string password, SqlTransaction sqlTrx)
    {
      StringBuilder _sb = new StringBuilder();
      _sb.AppendLine(" INSERT INTO ONLINE_CASINO_ACCOUNTS");
      _sb.AppendLine(" (OCA_ACCOUNT_ID, ");
      _sb.AppendLine("  OCA_USERNAME, ");
      _sb.AppendLine("  OCA_PASSWORD, ");
      _sb.AppendLine("  OCA_ONLINE_ACCOUNT_ID) ");
      _sb.AppendLine(" VALUES ");
      _sb.AppendLine(" (@pAccountId,");
      _sb.AppendLine("  @pOnlineAccountId,");
      _sb.AppendLine("  @pUsername,");
      _sb.AppendLine("  @pPassword)");

      using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), sqlTrx.Connection, sqlTrx))
      {
        _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = username;
        _cmd.Parameters.Add("@pOnlineAccountId", SqlDbType.VarChar).Value = password;
        _cmd.Parameters.Add("@pUsername", SqlDbType.VarChar).Value = username;
        _cmd.Parameters.Add("@pPassword", SqlDbType.VarChar).Value = password;

        _cmd.ExecuteNonQuery();

        return true;
      }
    }

    private bool HasOnlineCasinoAccountOnDataBase(long accountId, SqlTransaction sqlTrx)
    {
      StringBuilder _sb = new StringBuilder();
      _sb.AppendLine(" SELECT ");
      _sb.AppendLine(" OCA_ACCOUNT_ID ");
      _sb.AppendLine(" FROM ONLINE_CASINO_ACCOUNTS ");
      _sb.AppendLine(" WHERE OCA_ACCOUNT_ID = @pAccountId ");

      using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), sqlTrx.Connection, sqlTrx))
      {
        _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = accountId;

        return _cmd.ExecuteScalar() != null;
      }
    }

    public TransferStateData GetTransferState(string transactionId)
    {
      using (WSI.Common.DB_TRX _db_trx = new WSI.Common.DB_TRX())
      {
        TransferState transferState = GetTransferState(transactionId, _db_trx.SqlTransaction);
        return new TransferStateData
        {
          Amount = transferState.Amount,
          Id = transferState.Id,
          Provider = transferState.Provider,
          TransactionId = transferState.TransactionId,
          TransferStatus = transferState.TransferStatus,
          TransferType = transferState.TransferType
        };
      }
    }
  }
}
