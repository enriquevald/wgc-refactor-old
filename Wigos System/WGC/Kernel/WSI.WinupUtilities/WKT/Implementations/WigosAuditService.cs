﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;
using WSI.Common;
using WSI.WinupUtilities.WKT.Contracts;
using WSI.WinupUtilities.WKT.Contracts.ContractData.Audit;

namespace WSI.WinupUtilities.WKT.Implementations
{
  public class WigosAuditService : IAuditService
  {
    public void Notify(AuditData auditData)
    {
      Auditor auditor = new Auditor(auditData.AuditCode);

      foreach (var field in auditData.AuditFields)
      {
        if (field.AuditFieldType == AuditFieldType.Name)
          auditor.SetName(field.NlsId, field.NlsParam01, field.NlsParam02, field.NlsParam03, field.NlsParam04, field.NlsParam05);
        else
          auditor.SetField(field.NlsId, field.NlsParam01, field.NlsParam02, field.NlsParam03, field.NlsParam04, field.NlsParam05);
      }

      bool ok = auditor.Notify(auditData.GuiId, auditData.UserId, auditData.UserName, auditData.ComputerName);

      if (!ok) { 
        for (int i = 0; i < 2; i++)
        {
          ok = auditor.Notify(auditData.GuiId, auditData.UserId, auditData.UserName, auditData.ComputerName);

          if (ok)
            break;
        }
      }

      if (!ok)
        throw new Exception("Ocurrió un error al realizar la auditoría");
    }
  }
}
