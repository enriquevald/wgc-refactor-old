﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WigosTerminalService.cs
// 
//   DESCRIPTION: Implementation of service to add credit to terminal
// 
//        AUTHOR: R. Darío Soria
// 
// CREATION DATE: 17-Jan-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 17-Jan-2017 RDS    First release 
// ----------- ------ ----------------------------------------------------------
using System;
using System.Collections.Generic;
using WSI.Common;
using WSI.WCP;
using WSI.WinupUtilities.Util;
using WSI.WinupUtilities.WKT.Contracts;
using WSI.WinupUtilities.WKT.Contracts.ContractData.Promotion;
using WSI.WinupUtilities.WKT.Contracts.Exceptions;

namespace WSI.WinupUtilities.WKT.Implementations
{
  public class WigosTerminalService : ITerminalService
  {
    public WigosTerminalService()
    {
      WSI.Common.Resource.Init();
    }

    //public Boolean AddCreditAndChangeStatus(string trackdata, Decimal Amount)
    //{
    //  Boolean _return = false;
    //  ParticipateInCashDeskDraw _participate_in_cash_draw;
    //  CurrencyExchangeResult _exchange_result;
    //  RechargeOutputParameters OutputParameter;
    //  CashierSessionInfo _cashier_session_info;
    //  WCP_AccountManager.WCP_Account _account;

    //  try
    //  {
    //    var m_card_data = new CardData();

    //    using (DB_TRX _db_trx = new DB_TRX())
    //    {
    //      _account = WCP_AccountManager.Account(trackdata, _db_trx.SqlTransaction);

    //      // Get card data info
    //      if (_account != null && CardData.DB_CardGetAllData(Accounts.GetOrCreateVirtualAccount(_account.terminal_id, _db_trx.SqlTransaction), m_card_data, _db_trx.SqlTransaction))
    //      {
    //        _db_trx.Commit();

    //        _participate_in_cash_draw = new ParticipateInCashDeskDraw();
    //        _exchange_result = null;
    //        _cashier_session_info = WSI.Cashier.Cashier.CashierSessionInfo();

    //        using (DB_TRX _trx = new DB_TRX())
    //        {
    //          if (Accounts.DB_CardCreditAdd(m_card_data, Amount, 0, 0, 0, 0, _exchange_result, _participate_in_cash_draw,
    //                                                     OperationCode.CASH_IN,
    //                                                     _cashier_session_info,
    //                                                     _trx.SqlTransaction,
    //                                                     out OutputParameter))
    //          {

    //            //m_operation_id = OutputParameter.OperationId;

    //            if (TerminalFundsTransfer.DB_UpdateTerminalFundsTransferStatus(_account.terminal_id, TerminalFundsTransfer.TRANSFER_STATUS.TRANSFER_PENDING, _trx.SqlTransaction))
    //            {
    //              WcpCommands.InsertWcpCommand(_account.terminal_id, WCP_CommandCode.RequestTransfer, "", _trx.SqlTransaction);

    //              //m_voucher_list = OutputParameter.VoucherList;
    //            }
    //          }

    //          _return = _trx.Commit();
    //        }

    //      }

    //    }

    //    return _return;
    //  }
    //  catch (Exception _ex)
    //  {
    //    Log.Exception(_ex);
    //  }

    //  return false;
    //}

    //public int GetActiveByUser(string trackdata)
    //{
    //  var _result = 0;
    //  WCP_AccountManager.WCP_Account _account;
    //  using (DB_TRX _db_trx = new DB_TRX())
    //  {
    //    _account = WCP_AccountManager.Account(trackdata, _db_trx.SqlTransaction);
    //    if (_account != null)
    //    {
    //      _result = _account.terminal_id;
    //      _db_trx.Commit();
    //    }
    //  }

    //  return _result;
    //}

    //public SlotStatus GetStatus(int TerminalId, long AccountId)
    //{
    //  return new SlotStatus();
    //}

    //public bool ReserveSlot(int TerminalId, long AccountId, out WCP.Terminal.ReserveTerminalResult Error, out Int32 ExpirationMinutes)
    //{
    //  bool result = false;

    //  using (DB_TRX _db_trx = new DB_TRX())
    //  {
    //    result = WCP.Terminal.Reserve(TerminalId, AccountId, out Error, out ExpirationMinutes, _db_trx.SqlTransaction);

    //    _db_trx.Commit();
    //  }

    //  return result;
    //}

    //public bool CleanReserveSlot(int TerminalId)
    //{
    //  bool result = false;

    //  using (DB_TRX _db_trx = new DB_TRX())
    //  {
    //    result = WCP.Terminal.CleanReserve(TerminalId, _db_trx.SqlTransaction);
    //    _db_trx.Commit();
    //  }

    //  return result;
    //}

    //public WSI.WCP.Terminal.TerminalReservationStatus CheckReservationStatus(int TerminalId, long AccountId, out Int32 ReservedTerminalId)
    //{
    //  using (DB_TRX _db_trx = new DB_TRX())
    //  {
    //    return WCP.Terminal.CheckReservationStatus(TerminalId, AccountId, out ReservedTerminalId, _db_trx.SqlTransaction);
    //  }
    //}
  }
}
