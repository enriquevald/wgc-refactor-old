﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: IPlayerInfoService.cs
// 
//   DESCRIPTION: Interface that expose methods to get players info from WKT
// 
//        AUTHOR: Gustavo Ali
// 
// CREATION DATE: 27-Oct-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 27-Oct-2016 GDA    First release 
// ----------- ------ ----------------------------------------------------------
using System;
using System.Collections.Generic;
using WSI.Common;
using WSI.WinupUtilities.WKT.Contracts.ContractData.PlayerInfo;
using WSI.WinupUtilities.WKT.Implementations.DBEntities;

namespace WSI.WinupUtilities.WKT.Contracts
{
 public interface IPlayerInfoService
  {
    PlayerInfo GetBalance(string trackdata, string pin);

    PlayerInfo GetPersonalData(string trackdata, string pin);

    CardData GetPersonalData(int documentType, string documentNumber);

    CardData GetAllData(int accountId);

    WSI.Common.MultiPromos.AccountBalance RemoveCreditFromAccount(long accountId, decimal amount, Provider provider, string transactionId);
    WSI.Common.MultiPromos.AccountBalance AddCreditToAccount(long accountId, decimal amount, Provider provider, string transactionId);
    TransferStateData GetTransferState(string transactionId);
  }
}
