﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WSI.WinupUtilities.WKT.Contracts.ContractData.MobiBank;

namespace WSI.WinupUtilities.WKT.Contracts
{
  public interface IMobileBankService
  {
    RechargeResponse MobileBankCardTransfer(Int64 requestId, string userTrackData, string pin, long accountId, long amountToTransferx100, int terminalId);
    bool InsertWCPCommand_CancelRequest(int terminalId, long accountId);
  }
}
