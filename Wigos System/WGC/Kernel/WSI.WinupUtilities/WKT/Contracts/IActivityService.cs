﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: IActivityService.cs
// 
//   DESCRIPTION: Interface that expose methods to get players activity from WKT
// 
//        AUTHOR: Gustavo Ali
// 
// CREATION DATE: 27-Oct-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 27-Oct-2016 GDA    First release 
// ----------- ------ ----------------------------------------------------------
using System;
using System.Collections.Generic;

using WSI.WinupUtilities.WKT.Contracts.ContractData.Activity;

namespace WSI.WinupUtilities.WKT.Contracts
{
  public interface IActivityService
  {
    IList<Activity> GetPlayerActivity(string trackData, string pin);
  }
}
