﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: IResourceService.cs
// 
//   DESCRIPTION: Interface that expose methods to get the resources from promotions and gifts from WKT
// 
//        AUTHOR: Gustavo Ali
// 
// CREATION DATE: 27-Oct-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 27-Oct-2016 GDA    First release 
// ----------- ------ ----------------------------------------------------------
using System;
using System.Collections.Generic;

using WSI.WinupUtilities.WKT.Contracts.ContractData.Resource;

namespace WSI.WinupUtilities.WKT.Contracts
{
  public interface IResourceService
  {
    Resource GetResource(string id);
    string CountryISOCode2();
  }
}
