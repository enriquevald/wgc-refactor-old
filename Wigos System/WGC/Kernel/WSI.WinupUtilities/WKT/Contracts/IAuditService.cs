﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;
using WSI.WinupUtilities.WKT.Contracts.ContractData.Audit;

namespace WSI.WinupUtilities.WKT.Contracts
{
  public interface IAuditService
  {
    void Notify(AuditData auditData);
  }
}
