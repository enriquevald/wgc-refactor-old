﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WSI.WinupUtilities.WKT.Contracts.ContractData.MobiBank
{
  public class RechargeResponse
  {
    public Int32 ResponseCode { get; set; }
    public string ResponseCodeText { get; set; }
    public Int64 PlayerPreviousCardBalancex100 { get; set; }
    public Int64 PlayerCurrentCardBalancex100 { get; set; }
  }
}
