﻿using System;
using System.Collections.Generic;
using System.Text;
using WSI.Common;

namespace WSI.WinupUtilities.WKT.Contracts.ContractData.Audit
{
  public class AuditData
  {
    public AuditData(int auditCode, int guiId, int userId, string userName, string computerName)
    {
      this.AuditCode = auditCode;
      this.GuiId =(ENUM_GUI) guiId;
      this.UserId = userId;
      this.UserName = userName;
      this.ComputerName = computerName;
      this.AuditFields = new List<AuditFieldData>();
    }

    public int AuditCode { get; set; }
    public ENUM_GUI GuiId { get; set; }
    public int UserId { get; set; }
    public string UserName { get; set; }
    public string ComputerName { get; set; }
    public List<AuditFieldData> AuditFields { get; set; }
  }
}
