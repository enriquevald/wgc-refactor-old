﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WSI.WinupUtilities.WKT.Contracts.ContractData.Audit
{
  public class AuditFieldData
  {
    public AuditFieldData(AuditFieldType fieldType, int nlsId,
                         string nlsParam01,
                         string nlsParam02,
                         string nlsParam03,
                         string nlsParam04,
                         string nlsParam05)
    {
      this.AuditFieldType = fieldType;
      this.NlsId = nlsId;
      this.NlsParam01 = nlsParam01;
      this.NlsParam02 = nlsParam02;
      this.NlsParam03 = nlsParam03;
      this.NlsParam04 = nlsParam04;
      this.NlsParam05 = nlsParam05;
    }

    public AuditFieldType AuditFieldType { get; set; }
    public int NlsId { get; set; }
    public string NlsParam01 { get; set; }
    public string NlsParam02 { get; set; }
    public string NlsParam03 { get; set; }
    public string NlsParam04 { get; set; }
    public string NlsParam05 { get; set; }

  }
}
