﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WSI.WinupUtilities.WKT.Contracts.ContractData.Audit
{
  public enum AuditFieldType
  {
    Name = 0,
    Field = 1
  }
}
