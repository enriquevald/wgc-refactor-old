﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WSI.WinupUtilities.WKT.Contracts.ContractData.Promotion
{
  public class PromotionExchangeResponse
  {
    public decimal TotalBalance { get; set; }
    public decimal TotalNotRedeemable { get; set; }
    public decimal TotalRedeemable { get; set; }
    public decimal Points { get; set; }
  }
}
