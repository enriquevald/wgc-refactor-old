﻿using System;
using System.Collections.Generic;


namespace WSI.WinupUtilities.WKT.Contracts.ContractData.Promotion
{
  public class Promotion
  {
    public Int64 Id { get; set; }
    public String Name { get; set; }
    public String Description { get; set; }
    public Int64 SmallResourceId { get; set; }
    public Int64 LargeResourceId { get; set; }
    public String ResourceExtension
    {
      get
      {
        return "png";
      }
    }

    private string SmallResourceFileName
    {
      get
      {
        return "wkt_" + this.SmallResourceId + "_" + this.Name + "." + this.ResourceExtension;
      }
    }

    public string SmallResourceFileUrl
    {
      get
      {
        if (this.SmallResourceId == 0)
          return string.Empty;

        return WSI.Common.GeneralParam.Get("WinUP", "Resources.Url", string.Empty) + SmallResourceFileName;
      }
    }
  }
}
