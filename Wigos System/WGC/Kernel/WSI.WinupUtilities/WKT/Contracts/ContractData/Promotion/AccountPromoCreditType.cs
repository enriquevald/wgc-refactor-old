﻿using System;
using System.Collections.Generic;


namespace WSI.WinupUtilities.WKT.Contracts.ContractData.Promotion
{
  public enum AccountPromoCreditType
  {
    UNKNOWN = 0,
    NR1 = 1,      // Non-Redeemable - Spent at the end (with withhold)
    NR2 = 2,      // Non-Redeemable - Spent at beggining
    REDEEMABLE = 3,
    POINT = 4,
    UNR1 = 5,  // Spent at the end
    UNR2 = 6,  // Spent at beggining
    NR3 = 7,   // Non-Redeemable - Spent at the end (without withhold)
    REDEEMABLE_IN_KIND = 8,   // Redeemable in kind
  }
}
