﻿using System;
using System.Collections.Generic;


namespace WSI.WinupUtilities.WKT.Contracts.ContractData.Promotion
{
  public class ApplicablePromotion : Promotion
  {
    public AccountPromoCreditType PromoCreditType { get; set; } //UNKNOWN=0; NR1=1; NR2=2; REDEEMABLE=3; POINT=4 
    public Decimal MinSpent { get; set; }
    public Decimal MinSpentReward { get; set; }
    public Decimal Spent { get; set; }
    public Decimal SpentReward { get; set; }
    public Decimal CurrentReward { get; set; }
    public Decimal RechargesReward { get; set; }
    public Decimal AvailableLimit { get; set; }
  }
}
