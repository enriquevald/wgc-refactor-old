﻿using System;
using System.Collections.Generic;


namespace WSI.WinupUtilities.WKT.Contracts.ContractData.PlayerInfo
{
  public enum PlayerInfoFieldType
  {
    FULL_NAME = 1,
    CITY = 2,
    ZIP = 3,
    PIN = 4,
    ADDRESS_01 = 10,
    ADDRESS_02 = 11,
    ADDRESS_03 = 12,
    EMAIL_01 = 20,
    EMAIL_02 = 21,
    PHONE_NUMBER_01 = 30,
    PHONE_NUMBER_02 = 31,
    ID1 = 40,
    ID2 = 41,
    NAME = 52,
    SURNAME1 = 50,
    SURNAME2 = 51,
    BIRTHDAY = 60
  };
}
