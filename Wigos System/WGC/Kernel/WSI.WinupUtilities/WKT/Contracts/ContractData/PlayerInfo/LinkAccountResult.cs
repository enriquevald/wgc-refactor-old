﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WSI.WinupUtilities.WKT.Contracts.ContractData.PlayerInfo
{
  public class LinkAccountResult
  {
    public bool State { get; set; }
    public string Message { get; set; }
  }
}
