﻿using System;
using System.Collections.Generic;


namespace WSI.WinupUtilities.WKT.Contracts.ContractData.PlayerInfo
{
    public class PlayerInfo
    {
      public Info PersonalData { get; set; }
      public AccountLevel Level { get; set; }
      public AccountNextLevel NextLevel { get; set; }
      public BalanceParts Parts { get; set; }

      public class Info
      {
      public long AccountId { get; set; }
      public String Name { get; set; }
        public String SurName1 { get; set; }
        public String SurName2 { get; set; }
        public String FullName { get; set; }
        public DrawGenderFilter Gender { get; set; }
        public DateTime Birthday { get; set; }
        public String City { get; set; }
        public String Zip { get; set; }
        public String Pin { get; set; }
        public String Address1 { get; set; }
        public String Address2 { get; set; }
        public String Address3 { get; set; }
        public String Email1 { get; set; }
        public String Email2 { get; set; }
        public String PhoneNumber1 { get; set; }
        public String PhoneNumber2 { get; set; }
        public String avatar { get; set; }
    }

      public class AccountLevel
      {
        public Int32 LevelId { get; set; }
        public String LevelName { get; set; }
        public DateTime EnterDate { get; set; }
        public DateTime? Expiration { get; set; }
        public Currency PointsToEnter { get; set; }
        public Currency Points { get; set; }
      }

      public class BalanceParts
      {
        public Currency RedeemableBalance { get; set; }
        public Currency NonRedeemableBalance { get; set; }
        public List<BalancePartsPromotion> ActivePromotions { get; set; } //   public List<WKT_BalanceParts_Promotion> ActivePromotions;
      }

      public class AccountNextLevel
      {
        public Int32 LevelId { get; set; }
        public String LevelName { get; set; }
        public Points PointsToReach { get; set; }
        public Points PointsGenerated { get; set; }
        public Points PointsDisc { get; set; }
      }

      public class BalancePartsPromotion
      {
        public DateTime PromoDate { get; set; }
        public String PromoName { get; set; }
        public Currency AwardedBalance { get; set; }
        public Currency CurrentBalance { get; set; }
      }
    }

  }

