﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Gift.cs
// 
//   DESCRIPTION: Class that represent Currency from price of gifts in WKT
// 
//        AUTHOR: Gustavo Ali
// 
// CREATION DATE: 27-Oct-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 27-Oct-2016 GDV    First release 
// ----------- ------ ----------------------------------------------------------

using System;
using System.Collections.Generic;


namespace WSI.WinupUtilities.WKT.Contracts.ContractData.PlayerInfo
{
    public class Currency
    {
    public Decimal Value;
    public String CurrencyIsoCode;
  }
}
