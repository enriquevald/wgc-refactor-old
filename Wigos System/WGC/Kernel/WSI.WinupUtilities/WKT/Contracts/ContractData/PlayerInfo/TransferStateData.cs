﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WSI.WinupUtilities.WKT.Implementations.DBEntities;

namespace WSI.WinupUtilities.WKT.Contracts.ContractData.PlayerInfo
{
  public class TransferStateData
  {
    public Nullable<long> Id { get; set; }
    public string TransactionId { get; set; }
    public decimal Amount { get; set; }
    public Provider Provider { get; set; }
    public TransferStatus TransferStatus { get; set; }
    public TransferType TransferType { get; set; }
  }
}
