﻿using System;
using System.Collections.Generic;

namespace WSI.WinupUtilities.WKT.Contracts.ContractData.PlayerInfo
{
    public enum DrawGenderFilter
    {
    ALL = 0,
    MEN_ONLY = 1,
    WOMEN_ONLY = 2
    }
}
