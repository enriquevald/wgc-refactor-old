﻿using System;
using System.Collections.Generic;


namespace WSI.WinupUtilities.WKT.Contracts.ContractData.Resource
{
  public class Resource
  {
    public long Id { get; set; }
    public byte[] Content { get; set; }
    public byte[] ContentHash { get; set; }
    public string Extension { get; set; }
  }
}
