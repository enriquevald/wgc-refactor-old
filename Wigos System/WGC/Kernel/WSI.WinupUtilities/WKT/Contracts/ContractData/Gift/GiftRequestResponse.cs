﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Gift.cs
// 
//   DESCRIPTION: Class that represent a gift request response from WKT
// 
//        AUTHOR: Ariel Vazquez
// 
// CREATION DATE: 27-Oct-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 27-Oct-2016 AVZ    First release 
// ----------- ------ ----------------------------------------------------------
using System;
using System.Collections.Generic;

namespace WSI.WinupUtilities.WKT.Contracts.ContractData.Gift
{
  public class GiftRequestResponse
  {
    public decimal TotalBalance { get; set; }
    public decimal TotalNotRedeemable { get; set; }
    public decimal TotalRedeemable { get; set; }
    public decimal Points { get; set; }
  }
}
