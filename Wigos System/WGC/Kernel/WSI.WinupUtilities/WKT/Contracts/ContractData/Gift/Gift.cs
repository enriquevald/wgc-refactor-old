﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Gift.cs
// 
//   DESCRIPTION: Class that represent a gift from WKT
// 
//        AUTHOR: Ariel Vazquez
// 
// CREATION DATE: 27-Oct-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 27-Oct-2016 AVZ    First release 
// ----------- ------ ----------------------------------------------------------
using System;
using System.Collections.Generic;

namespace WSI.WinupUtilities.WKT.Contracts.ContractData.Gift
{
  public class Gift
  {
    public long Id { get; set; }
    public decimal Points { get; set; }
    public string Name { get; set; }
    public string Description { get; set; }
    public long? SmallResourceId { get; set; }
    public long? LargeResourceId { get; set; }
    public int Type { get; set; }
    public int CurrentStock { get; set; }
    public int AllowedUnits { get; set; }
    public int NonRedeemable { get; set; }
    public int DrawId { get; set; } // NEW OBJET IN MODEL
    public string status { get; set; } // NEW OBJET IN MODEL
    //Limits ?
    //REQ ?

    
    public String ResourceExtension
    {
      get
      {
        return "png";
      }
    }

    private string SmallResourceFileName
    {
      get
      {
        return "wkt_" + this.SmallResourceId + "_" + this.Name +"." + this.ResourceExtension;
      }
    }

    public string SmallResourceFileUrl
    {
      get
      {
        if (!this.SmallResourceId.HasValue)
          return string.Empty;

        return WSI.Common.GeneralParam.Get("WinUP", "Resources.Url", string.Empty) + SmallResourceFileName;
      }
    }

    private string LargeResourceFileName
    {
      get
      {
        return "wkt_" + this.LargeResourceId + "_" + this.Name + "." + this.ResourceExtension;
      }
    }

    public string LargeResourceFileUrl
    {
      get
      {
        if (!this.LargeResourceId.HasValue)
          return string.Empty;

        return WSI.Common.GeneralParam.Get("WinUP", "Resources.Url", string.Empty) + LargeResourceFileName;
      }
    }
  }
}
