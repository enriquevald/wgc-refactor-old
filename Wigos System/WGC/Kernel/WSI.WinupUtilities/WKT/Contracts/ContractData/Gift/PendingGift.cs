﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Gift.cs
// 
//   DESCRIPTION: Class that represent a pending to exchange gift in WKT
// 
//        AUTHOR: Ariel Vazquez
// 
// CREATION DATE: 27-Oct-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 27-Oct-2016 AVZ    First release 
// ----------- ------ ----------------------------------------------------------
using System;
using System.Collections.Generic;

namespace WSI.WinupUtilities.WKT.Contracts.ContractData.Gift
{
  public class PendingGift
  {
    public long InstanceId { get; set; }
    public long GiftId { get; set; }
    public string Name { get; set; }
    public int Type { get; set; }
    public decimal SpentPoints { get; set; }
    public Nullable<decimal> ConversionToNRC { get; set; }
    public string Description { get; set; }
    public string Date { get; set; }
    public int Quantity { get; set; }
    public long? SmallResourceId { get; set; }
    public long? LargeResourceId { get; set; }
    public string Voucher { get; set; }
    public string Barcode { get; set; }

    public String ResourceExtension
    {
      get
      {
        return "png";
      }
    }

    private string SmallResourceFileName
    {
      get
      {
        return "wkt_" + this.SmallResourceId + "_" + this.Name + "." + this.ResourceExtension;
      }
    }

    public string SmallResourceFileUrl
    {
      get
      {
        if (!this.SmallResourceId.HasValue)
          return string.Empty;

        return WSI.Common.GeneralParam.Get("WinUP", "Resources.Url", string.Empty) + SmallResourceFileName;
      }
    }

    private string LargeResourceFileName
    {
      get
      {
        return "wkt_" + this.LargeResourceId + "_" + this.Name + "." + this.ResourceExtension;
      }
    }

    public string LargeResourceFileUrl
    {
      get
      {
        if (!this.LargeResourceId.HasValue)
          return string.Empty;

        return WSI.Common.GeneralParam.Get("WinUP", "Resources.Url", string.Empty) + LargeResourceFileName;
      }
    }
  }
}

