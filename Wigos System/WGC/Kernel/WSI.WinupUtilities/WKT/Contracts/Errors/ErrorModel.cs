﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: ErrorModel.cs
// 
//   DESCRIPTION: Class that represent an ErrorModel Object who collects an Error Detail List
// 
//        AUTHOR: Gustavo Ali
// 
// CREATION DATE: 27-Oct-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 27-Oct-2016 GDA    First release 
// ----------- ------ ----------------------------------------------------------
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace WSI.WinupUtilities.WKT.Contracts.Errors
{
  public class ErrorModel
  {
    public ErrorModel()
    {
      this.ErrorList = new List<ErrorDetailModel>();
    }

    public ErrorModel(ErrorCode code, string message)
    {
      this.ErrorList = new List<ErrorDetailModel>();
      this.ErrorList.Add(new ErrorDetailModel(code,message));
    }

    [JsonProperty("errors")]
    public List<ErrorDetailModel> ErrorList { get; set; }
  }
}
