﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: ErrorCode.cs
// 
//   DESCRIPTION: Enum that express error codes types
// 
//        AUTHOR: Gustavo Ali
// 
// CREATION DATE: 27-Oct-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 27-Oct-2016 GDA    First release 
// ----------- ------ ----------------------------------------------------------
using System;
using System.Collections.Generic;

namespace WSI.WinupUtilities.WKT.Contracts.Errors
{
  public enum ErrorCode
  {
    InternalError = 1,
    AuthenticationError = 2,
    ConnectionError = 3,
    AccountBlockedError = 4,
  }
}
