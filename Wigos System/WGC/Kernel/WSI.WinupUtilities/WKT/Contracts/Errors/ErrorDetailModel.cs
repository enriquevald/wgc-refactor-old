﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: ErrorDetailModel.cs
// 
//   DESCRIPTION: Class that represent an Error Detail Object
// 
//        AUTHOR: Gustavo Ali
// 
// CREATION DATE: 27-Oct-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 27-Oct-2016 GDA    First release 
// ----------- ------ ----------------------------------------------------------
using System;
using System.Collections.Generic;

namespace WSI.WinupUtilities.WKT.Contracts.Errors
{
  public class ErrorDetailModel
  {
    public ErrorDetailModel(ErrorCode code, string message)
    {
      this.Code = (int)code;
      this.Message = message;
    }

    public int Code { get; set; }
    public string Message { get; set; }
  }
}
