﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WKTException.cs
// 
//   DESCRIPTION: Class that represent a WKT Exception
// 
//        AUTHOR: Gustavo Ali
// 
// CREATION DATE: 27-Oct-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 27-Oct-2016 GDA    First release 
// ----------- ------ ----------------------------------------------------------
using System;
using System.Collections.Generic;
using WSI.WCP;
using WSI.WinupUtilities.WKT.Contracts.Errors;

namespace WSI.WinupUtilities.WKT.Contracts.Exceptions
{
  public class WKTException : Exception
  {
    private WCP_ResponseCodes _responseCode;
    public WCP_ResponseCodes WKTErrorCode
    {
      get
      {
        return _responseCode;
      }
    }

    public ErrorCode ErrorCode
    {
      get
      {
        //Obtener error basado en el WCP_ResponseCodes
        return ErrorCode.InternalError;
      }
    }

    public WKTException(WCP_ResponseCodes code)
    {
      this._responseCode = code;
    }

    public WKTException(string message, WCP_ResponseCodes code)
        : base(message)
    {
      this._responseCode = code;
    }

    public WKTException(string message, Exception inner, WCP_ResponseCodes code)
        : base(message, inner)
    {
      this._responseCode = code;
    }
  }
}
