﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: IGiftService.cs
// 
//   DESCRIPTION: Interface that expose methods to get Gifts Availables from WKT
// 
//        AUTHOR: Gustavo Ali
// 
// CREATION DATE: 27-Oct-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 27-Oct-2016 GDA    First release 
// ----------- ------ ----------------------------------------------------------
using System;
using System.Collections.Generic;

using WSI.WinupUtilities.WKT.Contracts.ContractData.Gift;

namespace WSI.WinupUtilities.WKT.Contracts
{
  public interface IGiftService
  {
    IList<Gift> GetAvailableGifts(string trackdata, string pin);
    GiftRequestResponse ExchangeGift(string trackdata, string pin, string giftId, string units, decimal price);
    IList<PendingGift> GetPendingGifts(string trackdata, string pin);
  }
}
