﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: IPromotionService.cs
// 
//   DESCRIPTION: Interface that expose methods to add credit to terminal
// 
//        AUTHOR: R. Darío Soria
// 
// CREATION DATE: 17-Jan-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 17-Jan-2017 GDA    First release 
// ----------- ------ ----------------------------------------------------------
using System;
using System.Collections.Generic;
using WSI.WinupUtilities.WKT.Contracts.ContractData.Promotion;

namespace WSI.WinupUtilities.WKT.Contracts
{
  public interface ITerminalService
  {
    //int GetActiveByUser(string trackdata);
    //Boolean ReserveSlot(Int32 TerminalId, Int64 AccountId, out WSI.WCP.Terminal.ReserveTerminalResult Error, out Int32 ExpirationMinutes);
    //Boolean CleanReserveSlot(Int32 TerminalId);
    //WSI.WCP.Terminal.TerminalReservationStatus CheckReservationStatus(int TerminalId, long AccountId, out Int32 ReservedTerminalId);
  }
}
