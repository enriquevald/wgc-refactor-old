﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: IPromotionService.cs
// 
//   DESCRIPTION: Interface that expose methods to get promotions availables from WKT
// 
//        AUTHOR: Gustavo Ali
// 
// CREATION DATE: 27-Oct-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 27-Oct-2016 GDA    First release 
// ----------- ------ ----------------------------------------------------------
using System;
using System.Collections.Generic;
using WSI.WinupUtilities.WKT.Contracts.ContractData.Promotion;

namespace WSI.WinupUtilities.WKT.Contracts
{
  public interface IPromotionService
  {
    IList<ApplicablePromotion> GetAccountAvailablePromos(string trackdata, string pin);
    PromotionExchangeResponse ExchangePromotion(string trackdata, string pin, long promotionId, long creditType, decimal reward, bool transferToActiveSlot);
  }
}
