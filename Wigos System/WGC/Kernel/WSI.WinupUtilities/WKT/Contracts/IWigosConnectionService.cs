﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: IGiftService.cs
// 
//   DESCRIPTION: Interface that expose methods to get Gifts Availables from WKT
// 
//        AUTHOR: Sergio Soria
// 
// CREATION DATE: 02-NOV-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 02-NOV-2016 SDS    First release 
// ----------- ------ ----------------------------------------------------------
using System;
using System.Collections.Generic;


namespace WSI.WinupUtilities.WKT.Contracts
{
  public interface IWigosConnectionService
  {
    bool Connect(string server1, string server2, string database, string user, string password, int maxPoolSize, string appName, string version, string userBaseName);
    void Init();
  }
}
