﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: IActivityService.cs
// 
//   DESCRIPTION: Interface that expose methods to to get enums from  original CommonDefinitions
// 
//        AUTHOR: Sergio Soria
// 
// CREATION DATE: 01-Nov-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 27-Oct-2016 SDS    First release 
// ----------- ------ ----------------------------------------------------------
using System;
using System.Collections.Generic;

namespace WSI.WinupUtilities.WKT.Contracts
{
  public interface ICommonDefinitionsService
  {
    Dictionary<string,int> EnumGUI();
    Dictionary<string,int> Target();
    Dictionary<string, int> DrawGenderFilter();
  }
}
