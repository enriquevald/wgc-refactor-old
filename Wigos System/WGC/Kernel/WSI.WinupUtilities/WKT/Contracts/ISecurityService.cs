﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: ISecurityService.cs
// 
//   DESCRIPTION: Interface that expose methods to get valid user
// 
//        AUTHOR: Diego Tenutto
// 
// CREATION DATE: 10-Abr-2018
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 10-Abr-2018 DMT    First release 
// ----------- ------ ----------------------------------------------------------
using System;
using System.Collections.Generic;

namespace WSI.WinupUtilities.WKT.Contracts
{
  public interface ISecurityService
  {
    WSI.WinupUtilities.Util.SecurityStatus.LoginStatus IsValidUser(int AccountId, WSI.Common.ENUM_GUI GuiId);
  }
}
