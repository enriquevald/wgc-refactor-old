﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WSI.WinupUtilities.WKT.Contracts
{
  public interface IGeneralParamService
  {
    string GetString(string groupKey, string subjectKey, string defaultValue);
    int GetInt32(string groupKey, string subjectKey, int defaultValue);
    bool GetBoolean(string groupKey, string subjectKey, bool defaultValue);
  }
}
