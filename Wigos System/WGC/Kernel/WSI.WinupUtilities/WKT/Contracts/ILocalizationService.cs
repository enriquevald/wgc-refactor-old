﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WSI.WinupUtilities.WKT.Contracts
{
  public interface ILocalizationService
  {
    string GetString(string resourceKey);

    string GetStringByLanguage(string resourceKey, string language);
  }
}
