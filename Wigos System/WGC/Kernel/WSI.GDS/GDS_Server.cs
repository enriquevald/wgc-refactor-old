using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.IO;
using System.Net;
using System.Xml;
using System.Data;
using System.Data.SqlClient;

using WSI.Common;


namespace WSI.GDS
{
  public static class GDS
  {
    static GDS_Server server;

    public static GDS_Server Server
    {
      get { return GDS.server; }
      set { GDS.server = value; }
    }
  }

  public class GDS_Server:SecureTcpServer
  {
    private String GDS_STX = "<GDS_Message>";
    private String GDS_ETX = "</GDS_Message>";
    private const Int32 GDS_MESSAGE_MAX_LENGTH = 32 * 1024; 
    private IXmlSink  sink;
    private Encoding  encoding;

    public void SetStxEtx(String Stx, String Etx)
    {
      GDS_STX = Stx;
      GDS_ETX = Etx;
    }

    /// <summary>
    /// Send an Xml message
    /// </summary>
    /// <param name="TerminalId"></param>
    /// <param name="SessionId"></param>
    /// <param name="Xml"></param>
    public bool SendTo (Int32 TerminalId, Int64 SessionId, String Xml)
    {
      ClientsLock.AcquireReaderLock(System.Threading.Timeout.Infinite);
      try
      {
        foreach ( SecureTcpClient client in Clients )
        {
          if ( client.InternalId == TerminalId )
          {
            if ( SessionId == 0
                || SessionId == client.SessionId )
            {
              client.Send (Xml);

              return true;
            }
          }
        }
        return false;
      }
      finally
      {
        ClientsLock.ReleaseReaderLock();
      }
    }

    public IXmlSink Sink
    {
      set
      {
        sink = value;
      }
    }


    public GDS_Server (IPEndPoint IPEndPoint) : base (IPEndPoint)
    {
      encoding = Encoding.UTF8;
      
    }

    public override void ClientConnected (SecureTcpClient SecureClient)
    {
      base.ClientConnected (SecureClient);
    }

    public override void ClientDisconnected (SecureTcpClient SecureClient)
    {
      SqlConnection sql_conn;
      SqlTransaction sql_trx;

      if (SecureClient.InternalId != 0 && SecureClient.SessionId != 0)
      {
        sql_conn = null;
        sql_trx = null;

        try
        {
          // Get Sql Connection 
          sql_conn = WGDB.Connection();

          sql_trx = sql_conn.BeginTransaction();

          GDS_BusinessLogic.DB_SessionFinished(SecureClient.InternalId, SecureClient.SessionId, GDS_SessionStatus.Timeout, sql_trx);
          //SecureClient.InternalId = 0;
          //SecureClient.SessionId = 0;

          sql_trx.Commit();
        }

        catch (Exception ex)
        {
          Log.Exception(ex);
        }
        finally
        {
          if (sql_trx != null)
          {
            if (sql_trx.Connection != null)
            {
              sql_trx.Rollback();
            }

            sql_trx.Dispose();
            sql_trx = null;
          }

          // Close connection
          if (sql_conn != null)
          {
            sql_conn.Close();
            sql_conn = null;
          }
        }
      }

      base.ClientDisconnected (SecureClient);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="SecureTcpClient"></param>
    /// <param name="DataLength"></param>
    /// <param name="Data"></param>
    public override void DataReceived (SecureTcpClient SecureTcpClient, int DataLength, byte [] Data)
    {
      String GDS_message;
      String GDS_msg;
      int idx_stx;
      int idx_etx;
      int num_msgs;

      //////if (sink != null)
      //////{
      //////  sink.Process(this, SecureTcpClient, encoding.GetString(Data, 0, DataLength));

      //////  return;
      //////}

      GDS_msg = SecureTcpClient.StringBuffer;
      GDS_msg += encoding.GetString (Data, 0, DataLength);
      num_msgs = 0;

      try
      {
        // Index of starting tag
        idx_stx = GDS_msg.IndexOf (GDS_STX);
        // Index of ending tag
        idx_etx = GDS_msg.IndexOf (GDS_ETX);

        while ( idx_stx >= 0
              & idx_etx >= 0 )
        {
          // Both STX/ETX exist
          GDS_message = GDS_msg.Substring (idx_stx, idx_etx - idx_stx + GDS_ETX.Length);

          idx_stx = GDS_message.LastIndexOf (GDS_STX);
          if ( idx_stx > 0 )
          {
            // Skip all starting tags that are not ended
            GDS_message = GDS_message.Substring (idx_stx);
          }

          if ( sink != null )
          {
            try
            {
              // Message received: Enqueue it to be processed.

              num_msgs++;
              sink.Process (this, SecureTcpClient, GDS_message);
            }
            catch ( Exception ex )
            {
              Log.Exception (ex);
              Log.Message ("EXEP Msg: " + GDS_message);
            }
          }

          GDS_msg = GDS_msg.Substring (idx_etx + GDS_ETX.Length);
          idx_stx = GDS_msg.IndexOf (GDS_STX);
          idx_etx = GDS_msg.IndexOf (GDS_ETX);
        }

        if ( idx_stx == 0 )
        {
          return;
        }

        if ( idx_stx > 0 )
        {
          GDS_msg.Substring (idx_stx);

          return;
        }

        if ( GDS_msg.Length > GDS_STX.Length )
        {
          GDS_msg = GDS_msg.Substring (GDS_msg.Length - GDS_STX.Length);

          return;
        }
      }
      catch ( Exception ex )
      {
        Log.Exception (ex);
      }
      finally
      {
        if ( GDS_msg.Length >= GDS_MESSAGE_MAX_LENGTH )
        { 
          Log.Warning("GDS message length overflow: " + GDS_msg.Length.ToString()); 
          GDS_msg = "";
        }
        
        if ( num_msgs < 1 )
        {
          Log.Warning ("DataReceived/NoMessage: " + DataLength.ToString () + " Bytes.");
        }
        SecureTcpClient.StringBuffer = GDS_msg;
      }
    } // DataReceived
  } // Class SecureTcpServer
}
