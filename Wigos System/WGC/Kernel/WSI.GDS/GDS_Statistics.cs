using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Data.SqlClient;
using WSI.Common;
using System.Data;
using System.Timers;

namespace WSI.GDS
{
  public static class Statistics
  {
    public struct TYPE_STATS_ROW
    {
      public System.DateTime    base_hour;
      public int                terminal_id;
      public String             terminal_name;
      public int                game_id;
      public String             game_name;
      public int                num_plays;
      public Decimal            amount_played;
      public int                num_winning_plays;
      public Decimal            amount_winning_plays;
      public long               last_play_id;
    };

    static Thread               thread_stats;
    static Int64                last_play_id;
    static String               base_date;

    // When any of the connections (plays & sph) is wrong the statistics will 
    // wait 'timer_check_connections' miliseconds before stablishing new connections
    static int                  timer_check_connections;

    // Force a plays checking after 'timer_check_new_plays' miliseconds withouth any new play
    static int                  timer_check_new_plays;

    // Wait 'timer_after_new_play' miliseconds after receiving a new play signal in order to 
    // collect more plays in the same process
    static int                  timer_after_new_play;
    
    static internal System.Threading.AutoResetEvent   event_new_play;

    // Plays 
    static SqlDataAdapter       plays_da;
    static DataTable            plays_dt;
    static SqlConnection        plays_connection;
    static Int64                plays_num_active_terminals;

    // Sales Per Hour Control
    static SqlDataAdapter       sphc_da;
    static DataTable            sphc_dt;

    // Sales Per Hour 
    static SqlDataAdapter       sph_da;
    static DataTable            sph_dt;
    static SqlConnection        sph_connection;
    static SqlTransaction       sph_trx;
    static int                  sph_update_batch_size;

    //static int                  rows_to_process;

    /// <summary>
    /// Initializes Statistics class
    /// </summary>
    public static void Init ()
    {
      // Thread creation
      thread_stats = new Thread(StatsPerHourThread);
      thread_stats.Name = "StatsPerHour";

      event_new_play = new AutoResetEvent (false);

      last_play_id            = 0;
      base_date               = "'01/01/2007 00:00:00'";
      timer_check_connections = 3000;   // miliseconds 
      timer_check_new_plays   = 60000;  // miliseconds
      timer_after_new_play    = 1000;   // miliseconds

      // Plays 
      plays_da         = null;
      plays_dt         = null;
      plays_connection = null;

      // Sales Per Hour 
      sph_da                = null;
      sph_dt                = null;
      sph_connection        = null;
      sph_update_batch_size = 500;

      //rows_to_process = 0;

      return;
    } // Init

    /// <summary>
    /// Starts Statistics data gathering thread
    /// </summary>
    public static void Start ()
    {
      thread_stats.Start ();
    }

    /// <summary>
    /// Signals that a new play has occurred
    /// </summary>
    public static void NewPlay ()
    {
      event_new_play.Set ();
    }

    /// <summary>
    /// Reloads the statistics per hour
    /// </summary>
    static private void StatsPerHourThread()
    {
      TYPE_STATS_ROW    _sph_lower_bound;
      TYPE_STATS_ROW    _sph_upper_bound;
      Boolean           _rollback_changes;
      Boolean           _wait_for_new_play;
      Boolean           _log_error;
      //DateTime          _time_overall, _time_a, _time_b, _time_c;

      _sph_lower_bound = new TYPE_STATS_ROW ();
      _sph_upper_bound = new TYPE_STATS_ROW ();

      _rollback_changes  = true;
      _wait_for_new_play = false;
      _log_error         = false;

      //_time_overall = DateTime.Now;

      for ( ;; )
      {
        try 
        { 
          // Make sure that he required DB connections are ready
          if ( ! CheckConnections () )
          {
            // When any of the connections (plays & sph) is wrong the statistics will 
            // wait 'timer_check_connections' miliseconds before stablishing new connections
            Thread.Sleep (timer_check_connections);

            continue;
          }

          // Last play check is forced when the statistics thread starts
          if ( _wait_for_new_play )
          {
            // Wait for a new play 
            // Force a plays checking after 'timer_check_new_plays' miliseconds withouth any new play
            if ( event_new_play.WaitOne (timer_check_new_plays, false) )
            {
                              //Console.WriteLine ("Play Signaled");
              // New play signaled (no timeout)
              // Wait 'timer_after_new_play' miliseconds after receiving a new play signal in order to 
              // collect more plays in the same process
              Thread.Sleep (timer_after_new_play);
            }
          }
                              //Console.WriteLine ("Checking for new plays");

          _wait_for_new_play = true;

          // A new play was received
          //      - Get the last play that was processed by the statistics 
          //      - Retrieve new plays from the last saved play (last_play_id)
          //      - Retrieve existing statistics (date-time bounds criteria) that may be updated by the new plays
          //      - Update statistics with the retrieved plays 
          //      - Set the flag to commit updated and inserted statistics

          _rollback_changes = true;

          //rows_to_process = 0;

          //      - Get the last play that was processed by the statistics 
          if ( ! GetLastProcessedPlay (ref _log_error) )
          {
            if ( _log_error )
            {
              // DB error getting the last processed play
              Log.Error ("StatsPerHourThread ERROR : GetLastProcessedPlay");
            }

            continue;
          }

                              //Console.WriteLine ("Processing plays > {0}", last_play_id);
                              //_time_a = DateTime.Now;

                              //_time_overall = _time_a;
                              //Console.Write ("1. Checking for new plays \t\t{0} ", _time_a.TimeOfDay);

          //      - Retrieve new plays from the last saved play (last_play_id)
          if ( ! SelectPendingPlays (ref _sph_lower_bound, ref _sph_upper_bound) )
          {
                              //Console.WriteLine ("- Elapsed: {0} - NO New Plays", DateTime.Now.Subtract (_time_a));

            // No new plays to process
            continue;
          }

                              //Console.WriteLine ("- Elapsed: {0}", DateTime.Now.Subtract (_time_a));

                              //_time_b = DateTime.Now;
                              //Console.Write ("3. Getting related Sales Per Hour \t{0} ", _time_b.TimeOfDay);

          //      - Retrieve existing statistics (date-time bounds criteria) that may be updated by the new plays
          if ( ! SelectSalesPerHour (_sph_lower_bound, _sph_upper_bound) )
          {
            // DB error getting the sales per hour 
            Log.Error ("StatsPerHourThread ERROR : SelectSalesPerHour");
                              //Console.WriteLine ("StatsPerHourThread ERROR : SelectSalesPerHour");

            continue;
          }

                              //Console.WriteLine ("- Elapsed: {0}", DateTime.Now.Subtract (_time_b));

                              //_time_c = DateTime.Now;
                              //Console.WriteLine ("4. Updating Sales Per Hour \t\t{0} ", _time_c.TimeOfDay);

          //      - Update statistics with the retrieved plays 
          if ( ! UpdateSalesPerHour (_sph_lower_bound, _sph_upper_bound) )
          {
            // DB error getting the sales per hour 
            Log.Error ("StatsPerHourThread ERROR : UpdateSalesPerHour");
                              //Console.WriteLine ("StatsPerHourThread ERROR : UpdateSalesPerHour");

            continue;
          }

                              //Console.WriteLine ("\t-- Update Overall Time \t\t\t\t\t    {0} +++++", DateTime.Now.Subtract (_time_c));
                              //Console.WriteLine ("-- Overall Time \tRows processed: {1}\t\t\t    {0} *****", DateTime.Now.Subtract (_time_overall), rows_to_process);

          //      - Set the flag to commit updated and inserted statistics
          _rollback_changes = false; 
        } // try
        catch ( Exception _ex )
        {
                              //Console.WriteLine ("***** SPH exception: {0} {1}", _ex.Source, _ex.Message);
          Log.Exception (_ex);
        }
        finally
        {
          // Finish the statistics processing
          //    - Commit / Rollback changes
          //    - Dispose Plays allocated resources 
          //    - Dispose Sales per Hour allocated resources 

          //    - Commit / Rollback changes
          if ( sph_trx != null && _rollback_changes )
          {
            sph_trx.Rollback ();
          }

          //    - Dispose Plays allocated resources 
          DisposePlays ();

          //    - Dispose Sales per Hour allocated resources 
          DisposeSalesPerHour ();
        } // finally
      } // for
 
    } // StatsPerHourThread

    /// <summary>
    /// Make sure that the required DB connections are ready 
    /// </summary>
    static private Boolean CheckConnections ()
    {
      try
      {
        // Create connections
        if ( sph_connection == null )
        {
          sph_connection = WGDB.Connection ();
        }

        if ( plays_connection == null )
        {
          plays_connection = WGDB.Connection ();
        }

        // Check connection variables
        if ( sph_connection == null || plays_connection == null )
        {
          return false;
        }

        // Check connection states
        if ( sph_connection.State != ConnectionState.Open 
         && plays_connection.State != ConnectionState.Open )
        {
          if ( sph_connection.State != ConnectionState.Broken )
          {
            sph_connection.Close ();
          }

          sph_connection.Dispose ();
          sph_connection = null;

          if ( plays_connection.State != ConnectionState.Broken )
          {
            plays_connection.Close ();
          }

          plays_connection.Dispose ();
          plays_connection = null;

          return false;
        }

        // Both connections are opened and ready
        return true;
      }
      catch ( Exception _ex )
      {
        Log.Exception (_ex);

        return false;
      }
      finally
      {
      }
    } // CheckConnections

    /// <summary>
    /// Gets the sales per hour according to the received bounds
    /// </summary>
    static private Boolean GetLastProcessedPlay (ref Boolean LogError)
    {
      StringBuilder _sql_txt;
      SqlParameter  _sq_param;

      LogError = false;

      // Retrieve new plays from the last saved play
      //      - Start a new session
      //      - Build SQL command
      //      - SQL command parameters
      //      - Execute SQL command

      try
      {
        // Check connection state
        if ( sph_connection.State != ConnectionState.Open )
        {
          return false;
        }

        sph_trx = sph_connection.BeginTransaction ();

        // Sales per Hour Control
        //    - Set SqlDataAdapter commands
        //    - Execute Select command

        sphc_da = new SqlDataAdapter ();

        //    - Set SqlDataAdapter commands
        //        - SelectCommand 
        //        - DeleteCommand 
        //        - InsertCommand 
        //        - UpdateCommand 

        //        - SelectCommand 
        _sql_txt = new StringBuilder ();
        _sql_txt.AppendLine ("SELECT SPHC_LAST_PLAY_ID");
        _sql_txt.AppendLine ("     , SPHC_LAST_UPDATE");
        _sql_txt.AppendLine ("  FROM SPH_CONTROL");

        sphc_da.SelectCommand = new SqlCommand (_sql_txt.ToString ());
        sphc_da.SelectCommand.Connection  = sph_connection;
        sphc_da.SelectCommand.Transaction = sph_trx;

        //        - DeleteCommand 
        sphc_da.DeleteCommand = null;

        //        - InsertCommand 
        _sql_txt = new StringBuilder ();
        _sql_txt.AppendLine ("INSERT INTO SPH_CONTROL");
        _sql_txt.AppendLine ("          ( SPHC_LAST_PLAY_ID");
        _sql_txt.AppendLine ("          , SPHC_LAST_UPDATE");
        _sql_txt.AppendLine ("          )");
        _sql_txt.AppendLine ("   VALUES ( @p1, GETDATE () )");

        sphc_da.InsertCommand = new SqlCommand (_sql_txt.ToString ());
        sphc_da.InsertCommand.Connection  = sph_connection;
        sphc_da.InsertCommand.Transaction = sph_trx;

        sphc_da.InsertCommand.Parameters.Add ("@p1", SqlDbType.BigInt, 8, "SPHC_LAST_PLAY_ID");

        //        - UpdateCommand 
        _sql_txt = new StringBuilder ();
        _sql_txt.AppendLine ("UPDATE SPH_CONTROL");
        _sql_txt.AppendLine ("   SET SPHC_LAST_PLAY_ID = @p1");
        _sql_txt.AppendLine ("     , SPHC_LAST_UPDATE  = GETDATE ()");
        _sql_txt.AppendLine (" WHERE SPHC_LAST_PLAY_ID = @p2");

        sphc_da.UpdateCommand = new SqlCommand (_sql_txt.ToString ());
        sphc_da.UpdateCommand.Connection  = sph_connection;
        sphc_da.UpdateCommand.Transaction = sph_trx;

        sphc_da.UpdateCommand.Parameters.Add ("@p1", SqlDbType.BigInt, 8, "SPHC_LAST_PLAY_ID");
        _sq_param = sphc_da.UpdateCommand.Parameters.Add ("@p2", SqlDbType.BigInt, 8, "SPHC_LAST_PLAY_ID");
        _sq_param.SourceVersion = DataRowVersion.Original;

        //    - Execute Select command
        sphc_dt = new DataTable ("SPH_CONTROL");
        sphc_da.Fill (sphc_dt);

        // Add the first and only record if the table is empty
        if ( sphc_dt.Rows.Count == 0 )
        {
          DataRow     _sphc_row;

          // Empty table => start with the first play
          _sphc_row = sphc_dt.NewRow ();
          _sphc_row["SPHC_LAST_PLAY_ID"] = 0;
          sphc_dt.Rows.Add (_sphc_row);
        }

        //    - Get last play
        last_play_id = ( Int64 ) sphc_dt.Rows[0]["SPHC_LAST_PLAY_ID"];

        // Update the row to lock the SPHC table 
        sphc_dt.Rows[0]["SPHC_LAST_PLAY_ID"] = last_play_id;
        sphc_da.Update (sphc_dt);
        sphc_dt.AcceptChanges ();

        return true;
      }
      catch (DBConcurrencyException)
      {
        // SPHC table gets locked just after executing sphc_da.Update method.
        // It can happen that 2 service's instances get the same row after executing sphc_da.Fill method.
        // In this case the first one that executes sphc_da.Update will gaign the control but it won't lock 
        // the other instance (the lock only happens when executing sphc_da.Fill). The looser instance 
        // will raise a Concurrency Exception that must not be handled as an error since the new plays will 
        // be processed by the other service instance.
        return false;
      }
      catch ( Exception _ex )
      {
        // Calling function must log the error
        LogError = true;

        Log.Exception (_ex);

        return false;
      }
      finally
      {
      }
    } // GetLastProcessedPlay

    /// <summary>
    /// Gets the plays pending to be processed by the statistics thread.
    /// Additionally it returns the bounds (Datetime) that will be used 
    /// to limit the number of records of the sales per hour's SqlDataAdapter
    /// and the number of active terminals at this time.    
    /// Returns TRUE if there are plays to be processed by the statistics.
    /// </summary>
    static private Boolean SelectPendingPlays (ref TYPE_STATS_ROW LowerBound,
                                               ref TYPE_STATS_ROW UpperBound)
    {
      SqlCommand      _sql_command;
      SqlParameter    _sql_param;
      StringBuilder   _sql_txt;

      // Tasks
      //    - Retrieve new plays from the last saved play
      //    - Get the plays' bounds (Datetime) that will be used to limit the number 
      //      of records of the sales per hour's SqlDataAdapter (LowerBound & UpperBound).
      //    - Get the number of active terminals at this time (plays_num_active_terminals)

      //    - Retrieve new plays from the last saved play
      //        - Start a new session
      //          All the tasks will share the same connection (plays_connection)
      //        - Build SQL command
      //        - SQL command parameters
      //        - Set SqlDataAdapter commands
      //        - Execute SQL command

      try
      {
        // Check connection state
        if ( plays_connection.State != ConnectionState.Open )
        {
          return false;
        }
     
        //      - Build SQL command
        _sql_command = new SqlCommand ();
        _sql_command.Connection = plays_connection;

        _sql_txt = new StringBuilder ();
        _sql_txt.AppendLine ("SELECT DATEADD (hour, DATEDIFF (hour, " + base_date + ", PL_DATETIME), " + base_date + ") SPH_BASE_HOUR");
        _sql_txt.AppendLine ("     , PL_TERMINAL_ID, TE_NAME");
        _sql_txt.AppendLine ("     , PL_GAME_ID, GM_NAME");
        _sql_txt.AppendLine ("     , COUNT (*) PL_PLAYED_COUNT");
        _sql_txt.AppendLine ("     , SUM (CASE WHEN PL_WON_AMOUNT > 0 THEN 1 ELSE 0 END) PL_WON_COUNT");
        _sql_txt.AppendLine ("     , SUM (PL_PLAYED_AMOUNT) SPH_PLAYED_AMOUNT");
        _sql_txt.AppendLine ("     , SUM (PL_WON_AMOUNT) SPH_WON_AMOUNT");
        _sql_txt.AppendLine ("     , MAX (PL_PLAY_ID) SPH_LAST_PLAY_ID");
        _sql_txt.AppendLine ("  FROM PLAYS");
        _sql_txt.AppendLine ("     , TERMINALS");
        _sql_txt.AppendLine ("     , GAMES");
        _sql_txt.AppendLine (" WHERE PL_PLAY_ID > @p1");
        _sql_txt.AppendLine ("   AND PL_TERMINAL_ID = TE_TERMINAL_ID");
        _sql_txt.AppendLine ("   AND PL_GAME_ID = GM_GAME_ID");
        _sql_txt.AppendLine (" GROUP BY DATEADD (hour, DATEDIFF (hour, " + base_date + ", PL_DATETIME), " + base_date + ")");
        _sql_txt.AppendLine ("     , PL_TERMINAL_ID, TE_NAME");
        _sql_txt.AppendLine ("     , PL_GAME_ID, GM_NAME");
        _sql_txt.AppendLine (" ORDER BY SPH_BASE_HOUR");
        _sql_command.CommandText = _sql_txt.ToString ();

        //      - SQL command parameters
        _sql_param = _sql_command.Parameters.AddWithValue ("@p1", last_play_id);
        _sql_param.Direction = ParameterDirection.Input;

        //      - Set SqlDataAdapter commands
        plays_da = new SqlDataAdapter ();
        plays_da.SelectCommand = _sql_command;
        plays_da.DeleteCommand = null;
        plays_da.InsertCommand = null;
        plays_da.UpdateCommand = null;

//Console.WriteLine ("*** Plays from {0}", last_play_id);

        //      - Execute SQL command
        plays_dt = new DataTable ("NEW_PENDING_PLAYS");
        plays_da.Fill (plays_dt);

        // Check whether new plays are pending to be processed by the statistics 
        // If so get the bounds (datetime) and the number of active terminals.
        if ( plays_dt.Rows.Count > 0 )
        {
          //  - Get the plays' bounds (Datetime) that will be used to limit the number 
          //    of records of the sales per hour's SqlDataAdapter (LowerBound & UpperBound).
          //    Since plays are sorted by SPH_BASE_HOUR, base_hour bounds can be found in the first 
          //    and the last position of the dataset

          LowerBound.base_hour = ( System.DateTime ) plays_dt.Rows[0]["SPH_BASE_HOUR"];
          UpperBound.base_hour = ( System.DateTime ) plays_dt.Rows[plays_dt.Rows.Count - 1]["SPH_BASE_HOUR"];

          //  - Get the number of active terminals at this time (plays_num_active_terminals)
          if ( GetActiveTerminals () )
          {
            // There are new pending plays and the bounds and number of active terminals were retrieved
            return true;
          }
        }

        // There are no new pending plays 
        return false;
      }
      catch ( Exception _ex )
      {
        Log.Exception (_ex);

        return false;
      }
      finally
      {
      }
    } // SelectPendingPlays

    /// <summary>
    /// Get the number of active terminals at this time 
    /// </summary>
    static private Boolean GetActiveTerminals ()
    {
      SqlCommand    _sql_command;
      SqlDataReader _sql_data_reader;
      StringBuilder _sql_text;

      // Retrieve the number of active terminals at this time
      //      - Build SQL command
      //      - Execute SQL command

      _sql_data_reader = null;

      plays_num_active_terminals = 0;

      try
      {
        //    - Build SQL command
        //      Use existing plays_connection
        _sql_command = new SqlCommand ();
        _sql_command.Connection = plays_connection;

        _sql_text = new StringBuilder ();
        _sql_text.AppendLine ("SELECT COUNT (*) NUM_ACTIVE_TERMINALS");
        _sql_text.AppendLine ("  FROM TERMINALS");
        _sql_text.AppendLine (" WHERE TE_ACTIVE = 1");
        _sql_command.CommandText = _sql_text.ToString ();

        //    - Execute SQL command
        _sql_data_reader = _sql_command.ExecuteReader ();
        if ( _sql_data_reader.Read () && !_sql_data_reader.IsDBNull (0) )
        {
          plays_num_active_terminals = ( int ) _sql_data_reader["NUM_ACTIVE_TERMINALS"];
        }

        return true;
      }
      catch ( Exception _ex )
      {
        Log.Exception (_ex);

        return false;
      }
      finally
      {
        if ( _sql_data_reader != null )
        {
          _sql_data_reader.Close ();
        }
      }
    } // GetActiveTerminals 

    /// <summary>
    /// Gets the sales per hour according to the received bounds
    /// </summary>
    static private Boolean SelectSalesPerHour (TYPE_STATS_ROW   LowerBound,
                                               TYPE_STATS_ROW   UpperBound)
    {
      StringBuilder   _sql_txt;
      SqlParameter    _sql_param;

      // Retrieve new plays from the last saved play

      sph_dt = null;

      try
      {
        // Sales per Hour stats
        //    - Set SqlDataAdapter commands
        //    - Execute Select command

        sph_da = new SqlDataAdapter ();

        // When batch updates are enabled (UpdateBatchSize != 1), the UpdatedRowSource property value 
        // of the DataAdapter's UpdateCommand, InsertCommand, and DeleteCommand should be set to None 
        // or OutputParameters. 
        sph_da.UpdateBatchSize       = sph_update_batch_size;
        sph_da.ContinueUpdateOnError = false;

        //    - Set SqlDataAdapter commands
        //        - SelectCommand 
        //        - DeleteCommand 
        //        - InsertCommand 
        //        - UpdateCommand 

        //        - SelectCommand 
        _sql_txt = new StringBuilder ();
        _sql_txt.AppendLine ("SELECT SPH_BASE_HOUR");
        _sql_txt.AppendLine ("     , SPH_TERMINAL_ID");
        _sql_txt.AppendLine ("     , SPH_TERMINAL_NAME");
        _sql_txt.AppendLine ("     , SPH_GAME_ID");
        _sql_txt.AppendLine ("     , SPH_GAME_NAME");
        _sql_txt.AppendLine ("     , SPH_PLAYED_COUNT");
        _sql_txt.AppendLine ("     , SPH_PLAYED_AMOUNT");
        _sql_txt.AppendLine ("     , SPH_WON_COUNT");
        _sql_txt.AppendLine ("     , SPH_WON_AMOUNT");
        _sql_txt.AppendLine ("     , SPH_NUM_ACTIVE_TERMINALS");
        _sql_txt.AppendLine ("     , SPH_LAST_PLAY_ID");
        _sql_txt.AppendLine ("  FROM SALES_PER_HOUR");
        _sql_txt.AppendLine (" WHERE ( SPH_BASE_HOUR >= @p1 AND SPH_GAME_ID <= @p2 )");
        _sql_txt.AppendLine (" ORDER BY SPH_BASE_HOUR");

        sph_da.SelectCommand = new SqlCommand (_sql_txt.ToString ());
        sph_da.SelectCommand.Connection  = sph_connection;
        sph_da.SelectCommand.Transaction = sph_trx;

        _sql_param = sph_da.SelectCommand.Parameters.AddWithValue ("@p1", LowerBound.base_hour);
        _sql_param.Direction = ParameterDirection.Input;
        _sql_param = sph_da.SelectCommand.Parameters.AddWithValue ("@p2", UpperBound.base_hour);
        _sql_param.Direction = ParameterDirection.Input;

        //        - DeleteCommand 
        sph_da.DeleteCommand = null;

        //        - InsertCommand 
        _sql_txt = new StringBuilder ();
        _sql_txt.AppendLine ("INSERT INTO SALES_PER_HOUR");
        _sql_txt.AppendLine ("          ( SPH_BASE_HOUR");
        _sql_txt.AppendLine ("          , SPH_TERMINAL_ID");
        _sql_txt.AppendLine ("          , SPH_TERMINAL_NAME");
        _sql_txt.AppendLine ("          , SPH_GAME_ID");
        _sql_txt.AppendLine ("          , SPH_GAME_NAME");
        _sql_txt.AppendLine ("          , SPH_PLAYED_COUNT");
        _sql_txt.AppendLine ("          , SPH_PLAYED_AMOUNT");
        _sql_txt.AppendLine ("          , SPH_WON_COUNT");
        _sql_txt.AppendLine ("          , SPH_WON_AMOUNT");
        _sql_txt.AppendLine ("          , SPH_NUM_ACTIVE_TERMINALS");
        _sql_txt.AppendLine ("          , SPH_LAST_PLAY_ID");
        _sql_txt.AppendLine ("          )");
        _sql_txt.AppendLine ("   VALUES ( @p1, @p2, @p3, @p4, @p5, @p6, @p7, @p8, @p9, @p10, @p11 )");

        sph_da.InsertCommand = new SqlCommand (_sql_txt.ToString ());
        sph_da.InsertCommand.Connection       = sph_connection;
        sph_da.InsertCommand.Transaction      = sph_trx;
        sph_da.InsertCommand.UpdatedRowSource = UpdateRowSource.None;

        sph_da.InsertCommand.Parameters.Add ("@p1", SqlDbType.DateTime, 8, "SPH_BASE_HOUR");
        sph_da.InsertCommand.Parameters.Add ("@p2", SqlDbType.Int, 4, "SPH_TERMINAL_ID");
        sph_da.InsertCommand.Parameters.Add ("@p3", SqlDbType.VarChar, 50, "SPH_TERMINAL_NAME");
        sph_da.InsertCommand.Parameters.Add ("@p4", SqlDbType.Int, 4, "SPH_GAME_ID");
        sph_da.InsertCommand.Parameters.Add ("@p5", SqlDbType.VarChar, 50, "SPH_GAME_NAME");
        sph_da.InsertCommand.Parameters.Add ("@p6", SqlDbType.BigInt, 8, "SPH_PLAYED_COUNT");
        sph_da.InsertCommand.Parameters.Add ("@p7", SqlDbType.Money, 8, "SPH_PLAYED_AMOUNT");
        sph_da.InsertCommand.Parameters.Add ("@p8", SqlDbType.BigInt, 8, "SPH_WON_COUNT");
        sph_da.InsertCommand.Parameters.Add ("@p9", SqlDbType.Money, 8, "SPH_WON_AMOUNT");
        sph_da.InsertCommand.Parameters.Add ("@p10", SqlDbType.SmallInt, 2, "SPH_NUM_ACTIVE_TERMINALS");
        sph_da.InsertCommand.Parameters.Add ("@p11", SqlDbType.BigInt, 8, "SPH_LAST_PLAY_ID");

        //        - UpdateCommand 
        _sql_txt = new StringBuilder ();
        _sql_txt.AppendLine ("UPDATE SALES_PER_HOUR");
        _sql_txt.AppendLine ("   SET SPH_PLAYED_COUNT         = @p1");
        _sql_txt.AppendLine ("     , SPH_PLAYED_AMOUNT        = @p2");
        _sql_txt.AppendLine ("     , SPH_WON_COUNT            = @p3");
        _sql_txt.AppendLine ("     , SPH_WON_AMOUNT           = @p4");
        _sql_txt.AppendLine ("     , SPH_NUM_ACTIVE_TERMINALS = @p5");
        _sql_txt.AppendLine ("     , SPH_LAST_PLAY_ID         = @p6");
        _sql_txt.AppendLine (" WHERE SPH_BASE_HOUR   = @p7");
        _sql_txt.AppendLine ("   AND SPH_TERMINAL_ID = @p8");
        _sql_txt.AppendLine ("   AND SPH_GAME_ID     = @p9");

        sph_da.UpdateCommand = new SqlCommand (_sql_txt.ToString ());
        sph_da.UpdateCommand.Connection       = sph_connection;
        sph_da.UpdateCommand.Transaction      = sph_trx;
        sph_da.UpdateCommand.UpdatedRowSource = UpdateRowSource.None;

        sph_da.UpdateCommand.Parameters.Add ("@p1", SqlDbType.BigInt, 8, "SPH_PLAYED_COUNT");
        sph_da.UpdateCommand.Parameters.Add ("@p2", SqlDbType.Money, 8, "SPH_PLAYED_AMOUNT");
        sph_da.UpdateCommand.Parameters.Add ("@p3", SqlDbType.BigInt, 8, "SPH_WON_COUNT");
        sph_da.UpdateCommand.Parameters.Add ("@p4", SqlDbType.Money, 8, "SPH_WON_AMOUNT");
        sph_da.UpdateCommand.Parameters.Add ("@p5", SqlDbType.SmallInt, 2, "SPH_NUM_ACTIVE_TERMINALS");
        sph_da.UpdateCommand.Parameters.Add ("@p6", SqlDbType.BigInt, 8, "SPH_LAST_PLAY_ID");
        sph_da.UpdateCommand.Parameters.Add ("@p7", SqlDbType.DateTime, 8, "SPH_BASE_HOUR");
        sph_da.UpdateCommand.Parameters.Add ("@p8", SqlDbType.Int, 4, "SPH_TERMINAL_ID");
        sph_da.UpdateCommand.Parameters.Add ("@p9", SqlDbType.Int, 4, "SPH_GAME_ID");

        //    - Execute Select command
        sph_dt = new DataTable ("SALES_PER_HOUR");
        sph_da.Fill (sph_dt);

        return true;
      }
      catch ( Exception _ex )
      {
        Log.Exception (_ex);

        return false;
      }
      finally
      {
      }
    } // SelectSalesPerHour

    /// <summary>
    /// Process the pending plays and updates the sales per hour
    /// </summary>
    static private Boolean UpdateSalesPerHour (TYPE_STATS_ROW LowerBound,
                                               TYPE_STATS_ROW UpperBound)
    {
      TYPE_STATS_ROW    _play_row;
      int               _idx_row;
      DataRow           _sph_insert_row;
      Boolean           _data_found;
      Int64             _new_last_play_id;
      int               _num_update_retries;
      Boolean           _update_ok;
      Boolean           _exit_loop;
      //DateTime          _time_c, _time_d;

      _play_row = new TYPE_STATS_ROW ();

      _new_last_play_id = last_play_id;

      //rows_to_process += plays_dt.Rows.Count;

                                //_time_c = DateTime.Now;
                                //Console.WriteLine ("\t4.1. Rows to process:\t{0}", rows_to_process);
                                //Console.Write ("\t4.2. Browsing new plays "); // {0} ", _time_c.TimeOfDay);

      try
      {
        // Browse new plays to update the sales per hour stats
        for ( _idx_row = 0; _idx_row < plays_dt.Rows.Count; _idx_row++ )
        {
          _play_row.base_hour            = ( System.DateTime ) plays_dt.Rows[_idx_row]["SPH_BASE_HOUR"];
          _play_row.terminal_id          = ( int ) plays_dt.Rows[_idx_row]["PL_TERMINAL_ID"];
          _play_row.terminal_name        = ( String ) plays_dt.Rows[_idx_row]["TE_NAME"];
          _play_row.game_id              = ( int ) plays_dt.Rows[_idx_row]["PL_GAME_ID"];
          _play_row.game_name            = ( String ) plays_dt.Rows[_idx_row]["GM_NAME"];
          _play_row.num_plays            = ( int ) plays_dt.Rows[_idx_row]["PL_PLAYED_COUNT"];
          _play_row.amount_played        = ( Decimal ) plays_dt.Rows[_idx_row]["SPH_PLAYED_AMOUNT"];
          _play_row.num_winning_plays    = ( int ) plays_dt.Rows[_idx_row]["PL_WON_COUNT"];
          _play_row.amount_winning_plays = ( Decimal ) plays_dt.Rows[_idx_row]["SPH_WON_AMOUNT"];
          _play_row.last_play_id         = ( long ) plays_dt.Rows[_idx_row]["SPH_LAST_PLAY_ID"];

          _data_found = false;
          foreach ( DataRow _sph_row in sph_dt.Rows )
          {
            // Since plays and sph lists are sorted by base_hour, we can dismiss 
            // the search when it is bypassed and insert a new record
            if ( _play_row.base_hour > ( System.DateTime ) _sph_row["SPH_BASE_HOUR"] )
            {
              break;
            }

            if (   _play_row.base_hour == ( System.DateTime ) _sph_row["SPH_BASE_HOUR"]
                && _play_row.terminal_id == ( int ) _sph_row["SPH_TERMINAL_ID"]
                && _play_row.game_id == ( int ) _sph_row["SPH_GAME_ID"] )
            {
              // UPDATE
              _data_found = true;

              //_sph_row["SPH_NUM_ACTIVE_TERMINALS"] = plays_num_active_terminals;
              _sph_row["SPH_PLAYED_COUNT"]         = ( Int64 ) _sph_row["SPH_PLAYED_COUNT"] + _play_row.num_plays;
              _sph_row["SPH_PLAYED_AMOUNT"]        = ( Decimal ) _sph_row["SPH_PLAYED_AMOUNT"] + _play_row.amount_played;
              _sph_row["SPH_WON_COUNT"]            = ( Int64 ) _sph_row["SPH_WON_COUNT"] + _play_row.num_winning_plays;
              _sph_row["SPH_WON_AMOUNT"]           = ( Decimal ) _sph_row["SPH_WON_AMOUNT"] + _play_row.amount_winning_plays;
              _sph_row["SPH_LAST_PLAY_ID"]         = _play_row.last_play_id;

              break;
            }
          } // foreach

          if ( !_data_found )
          {
            // INSERT
            _sph_insert_row = sph_dt.NewRow ();
            _sph_insert_row["SPH_NUM_ACTIVE_TERMINALS"] = plays_num_active_terminals;
            _sph_insert_row["SPH_BASE_HOUR"]            = _play_row.base_hour;
            _sph_insert_row["SPH_TERMINAL_ID"]          = _play_row.terminal_id;
            _sph_insert_row["SPH_TERMINAL_NAME"]        = _play_row.terminal_name;
            _sph_insert_row["SPH_GAME_ID"]              = _play_row.game_id;
            if (_play_row.game_name.Length > 50)
            {
              _sph_insert_row["SPH_GAME_NAME"] = _play_row.game_name.Substring(0, 50);
            }
            else
            {
              _sph_insert_row["SPH_GAME_NAME"] = _play_row.game_name;
            }          
            _sph_insert_row["SPH_PLAYED_COUNT"]         = _play_row.num_plays;
            _sph_insert_row["SPH_PLAYED_AMOUNT"]        = _play_row.amount_played;
            _sph_insert_row["SPH_WON_COUNT"]            = _play_row.num_winning_plays;
            _sph_insert_row["SPH_WON_AMOUNT"]           = _play_row.amount_winning_plays;
            _sph_insert_row["SPH_LAST_PLAY_ID"]         = _play_row.last_play_id;

            sph_dt.Rows.Add (_sph_insert_row);
          }

          // Save last play id 
          if ( _play_row.last_play_id > _new_last_play_id )
          {
            _new_last_play_id = _play_row.last_play_id;
          }
        } // for

                              //Console.WriteLine ("Last Processed play = {0} ({1} plays)", _new_last_play_id
                              //                                                          , _new_last_play_id - last_play_id);

        // Update the id of the last processed play 
        last_play_id = _new_last_play_id;
        sphc_dt.Rows[0]["SPHC_LAST_PLAY_ID"] = last_play_id;

                              //Console.WriteLine ("\t\t\t - Elapsed: {0}", DateTime.Now.Subtract (_time_c));

                              //_time_d = DateTime.Now;
                              //Console.Write ("\t4.3. Updating SPH (BSize={0}) ", sph_da.UpdateBatchSize);

        _exit_loop = false;
        _update_ok = false;
        _num_update_retries = 0;
        while ( ! _exit_loop )
        {
          _exit_loop = true;

          try
          {
            // Update sales per hour control
            sphc_da.Update (sphc_dt);
            sphc_dt.AcceptChanges ();

            _update_ok = true;
          }
          catch ( DBConcurrencyException _ex_db_concurrency )
          {
            _num_update_retries++;
            if ( _num_update_retries < 5 )
            {
              _exit_loop = false;

              Thread.Sleep (1000);
            }
            else
            {
              Log.Exception (_ex_db_concurrency);
            }
          }
          catch ( Exception _ex )
          {
            Log.Exception (_ex);
          }
          finally
          {
          }
        }

        if ( ! _update_ok )
        {
          return false;
        }

        // Update sales per hour
        sph_da.Update (sph_dt);
        sph_dt.AcceptChanges ();

                              //Console.WriteLine ("\t\t\t - Elapsed: {0}", DateTime.Now.Subtract (_time_d));

        // Commit changes 
        sph_trx.Commit ();

        return true;
      }
      catch ( Exception _ex )
      {
                              //Console.WriteLine ("UpdateSalesPerHour: {0}", DateTime.Now.Subtract (_time_c));
        Log.Exception (_ex);

        return false;
      }
      finally
      {
      }
    } // UpdateSalesPerHour

    /// <summary>
    /// Releases all the resources allocated for the Sales Per Hour 
    /// </summary>
    static private Boolean DisposeSalesPerHour ()
    {
      try
      {
        // Sales per hour data table
        if ( sphc_dt != null )
	    {
          sphc_dt.Dispose ();
          sphc_dt = null;
        }

        // Sales per hour data adapter
        if ( sphc_da != null )
	    {
          sphc_da.Dispose ();
          sphc_da = null;
        }

        // Sales per hour transaction
        if ( sph_trx != null )
        {
          sph_trx.Dispose ();
          sph_trx = null;
        }

        return true;
      }
      catch ( Exception _ex )
      {
        Log.Exception (_ex);

        return false;
      }
      finally
      {
      }
    } // DisposeSalesPerHour

    /// <summary>
    /// Releases all the resources allocated for the Plays
    /// </summary>
    static private Boolean DisposePlays ()
    {
      try
      {
        // Plays data table
        if ( plays_dt != null )
        {
          plays_dt.Dispose ();
          plays_dt = null;
        }

        // Plays data adapter
        if ( plays_da != null )
        {
          plays_da.Dispose ();
          plays_da = null;
        }

        return true;
      }
      catch ( Exception _ex )
      {
        Log.Exception (_ex);

        return false;
      }
      finally
      {
      }
    } // DisposePlays

  } // class Statistics
}
