using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Data.SqlClient;
using WSI.Common;
using System.Data;
using System.Timers;

namespace WSI.GDS
{
  internal enum CachedTable
  {
    games = 0,
    terminals = 1,
    GDS_sessions = 2,
    play_sessions = 3,
    GDS_services = 4,
    site = 5, 
  }

  public static class DbCache
  {
    static Thread thread_cache;
    //static Thread thread_cleaner;

    static internal System.Threading.AutoResetEvent[] event_reload;
    static internal System.Threading.ManualResetEvent[] event_reloaded;
    static internal DateTime [] when_reloaded;
    
    static SqlDataAdapter da;
    static DataSet ds;
    static ReaderWriterLock dbcache_lock;

    /// <summary>
    /// Initialize class attributes.
    /// </summary>
    public static void Init()
    {
      // Cache
      thread_cache = new Thread(DbCacheThread);
      thread_cache.Name = "DbCache";

      event_reload = new AutoResetEvent[6];
      event_reloaded = new ManualResetEvent[event_reload.Length];
      when_reloaded = new DateTime[event_reload.Length];

      // Init events array
      for (int i = 0; i < event_reload.Length; i++)
      {
        event_reload[i] = new AutoResetEvent(true);
        event_reloaded[i] = new ManualResetEvent(false);
        when_reloaded[i] = DateTime.Now;
      }

      // Init database connection
      ds = new DataSet();
      da = new SqlDataAdapter();

      // Init ReaderWriterLock
      dbcache_lock = new ReaderWriterLock();

      //////// Init cleaner thread
      //////thread_cleaner = new Thread(DbCleanerThread);
      //////thread_cleaner.Name = "DbCleaner";
    }

    /// <summary>
    /// Start DbCache thread.
    /// </summary>
    public static void Start()
    {
      thread_cache.Start();
      //thread_cleaner.Start();
    }

    /// <summary>
    /// Set events to reload data.
    /// </summary>
    /// <param name="Table"></param>
    private static void ManageEvents(CachedTable Table)
    {
      DbCache.event_reloaded[(int)Table].Reset();
      DbCache.event_reload[(int)Table].Set();
      DbCache.event_reloaded[(int)Table].WaitOne();
    }

    /// <summary>
    /// Time lapse actions:
    /// 1. Update games table on database.
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    private static void OnTimedEvent(object source, ElapsedEventArgs e)
    {
      Update(ds.Tables["GAMES"]);
      ds.Tables["GAMES"].AcceptChanges();
    }    

    /// <summary>
    /// Apply changes on database for a specific table.
    /// </summary>
    /// <param name="Table"></param>
    private static void Update(DataTable Table)
    {
      DataTable table_aux;

      lock (Table)
      {
        table_aux = Table.GetChanges();
      }

      if (table_aux != null)
      {
        if (table_aux.Rows.Count > 0)
        {
          da.InsertCommand.Connection = WGDB.Connection();
          da.InsertCommand.Transaction = da.InsertCommand.Connection.BeginTransaction();
          da.Update(table_aux);
          da.InsertCommand.Transaction.Commit();
        }
      }
    }

    /// <summary>
    /// Row post-update actions. Must exist.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private static void GameRowUpdated(object sender, SqlRowUpdatedEventArgs e)
    {

    }

    /// <summary>
    /// Create a new game on memory.
    /// </summary>
    /// <param name="GameId"></param>
    /// <returns></returns>
    private static DataRow RegisterGame(String GameName)
    {
      DataTable table;
      DataRow game;
      DataRow[] games;
      String game_name;
      StringBuilder sql_str;
      SqlCommand cmd_insert;

      if (GameName.Length > 50)
      {
        game_name = GameName.Substring(0, 50);
      }
      else
      {
        game_name = GameName;
      }

      dbcache_lock.AcquireWriterLock(Timeout.Infinite);
      try
      {

        GetTableFromDataset("GAMES", out table);
        games = table.Select("GM_NAME = '" + game_name + "'");
        if (games.Length > 0)
        {
          DataRow r;

          r = table.NewRow();
          r.ItemArray = (Object[])games[0].ItemArray.Clone();
          return r;
        }

        GetTableFromDataset("GAMES", out table);
        game = table.NewRow();
        game["GM_NAME"] = game_name;
        table.Rows.Add(game);

        // Insert New Game
        sql_str = new StringBuilder();
        sql_str.AppendLine("INSERT INTO GAMES (GM_NAME) ");
        sql_str.AppendLine("VALUES ");
        sql_str.AppendLine("(@p1)");

        cmd_insert = new SqlCommand(sql_str.ToString());
        da.InsertCommand = cmd_insert;
        cmd_insert.Parameters.Add("@p1", SqlDbType.NVarChar, 50, "GM_NAME").Value = game["GM_NAME"];

        Update(ds.Tables["GAMES"]);
        ds.Tables["GAMES"].AcceptChanges();
      }
      finally
      {
        dbcache_lock.ReleaseWriterLock();
      }

      return game;
      
    }

    /// <summary>
    /// Set event to reload Terminals table.
    /// </summary>
    /// <param name="TerminalId"></param>
    public static void ReloadTerminal(Int32 TerminalId)
    {
      DbCache.event_reload[(int)CachedTable.terminals].Set();      
    }

    /// <summary>
    /// Set event to reload GDS_Sessions table.
    /// </summary>
    /// <param name="TerminalId"></param>
    public static void ReloadGDSSession(Int32 TerminalId)
    {
      DbCache.event_reload[(int)CachedTable.GDS_sessions].Set();     
    }

    /// <summary>
    /// Set event to reload Play_Sessions table.
    /// </summary>
    /// <param name="TerminalId"></param>
    public static void ReloadPlaySession(Int32 TerminalId)
    {
      DbCache.event_reload[(int)CachedTable.play_sessions].Set();
    }

    /// <summary>
    /// Set event to reload GDS_Services table.
    /// </summary>
    public static void ReloadGDSServices()
    {
      DbCache.event_reload[(int)CachedTable.GDS_services].Set();
    }

    /// <summary>
    /// Set event to reload Site table.
    /// </summary>
    public static void ReloadSite()
    {
      DbCache.event_reload[(int)CachedTable.site].Set();
    }

    /// <summary>
    /// Retrieve the list of games.
    /// </summary>
    /// <param name="GameName"></param>
    /// <returns></returns>
    public static DataRow Games(String GameName)
    {
      DataTable table;
      DataRow[] games;

      for (int i = 0; i < 2; i++)
      {
        GetTableFromDataset("GAMES", out table);
        games = table.Select("GM_NAME = '" + GameName + "'");
        if (games.Length > 0)
        {
          DataRow r;

          r = table.NewRow();
          r.ItemArray = (Object[])games[0].ItemArray.Clone();
          return r;
        }

        if (i == 0)
        {
          ManageEvents(CachedTable.games);
        }
      }
      return null;
    }

    /// <summary>
    /// Return the game name of a specific game id.
    /// Creates the row if does not exist.
    /// </summary>
    /// <param name="GameName"></param>
    /// <returns></returns>
    public static String GameName(String GameName)
    {
      DataRow game;

      game = Games(GameName);
      if ( game != null )
      {
        return game["GM_NAME"].ToString();
      }

      // Game name doesn't exist insert it into database.
      game = RegisterGame(GameName);
      if (game == null)
      {
        Log.Error("Game couldn't be registered. GameName: " + GameName);

        return "";
      }

      return game["GM_NAME"].ToString();
    }

    /// <summary>
    /// Returns terminal data for a specific terminal Id.
    /// </summary>
    /// <param name="TerminalId"></param>
    /// <returns></returns>
    public static DataRow Terminal(Int32 TerminalId) 
    {
      DataTable table;
      DataRow[] terminals;

      for (int i = 0; i < 2; i++)
      {
        GetTableFromDataset("TERMINALS", out table);
        terminals = table.Select("TE_TERMINAL_ID = '" + TerminalId + "'");
        if (terminals.Length > 0)
        {
          DataRow r;

          r = table.NewRow();
          r.ItemArray = (Object[])terminals[0].ItemArray.Clone();
          return r;
        }

        if (i == 0)
        {
          ManageEvents(CachedTable.terminals);
        }
      } 
      return null;
    }

    /// <summary>
    /// Return terminal data for a specific external terminal id.
    /// </summary>
    /// <param name="ExternalId"></param>
    /// <returns></returns>
    public static DataRow Terminal(String ExternalId)
    {
      DataTable table;
      DataRow[] terminals;

      GetTableFromDataset("TERMINALS", out table);
      terminals = table.Select("TE_EXTERNAL_ID = '" + ExternalId + "'");
      if (terminals.Length > 0)
      {
        DataRow r;

        r = table.NewRow();
        r.ItemArray = (Object[])terminals[0].ItemArray.Clone();
        return r;
      }

      return null;
    }

    public static Boolean IsTerminalLocked(String ExternalId)
    {
      DataRow dr;
      Int32 ServerId;

      dr = DbCache.Terminal(ExternalId);
      if (dr == null)
      {
        return true;
      }
      if (((Boolean)dr["TE_BLOCKED"]))
      {
        return true;
      }

      if (((Int32) dr["TE_TYPE"]) == 0 )
      {
        return false;
      }

      if (dr.IsNull("TE_SERVER_ID"))
      {
        return false;
      }

      ServerId = (Int32)dr["TE_SERVER_ID"];

      dr = DbCache.Terminal(ServerId);
      if (dr == null)
      {
        return true;
      }
      if (((Boolean)dr["TE_BLOCKED"]))
      {
        return true;
      }

      return false;
    }

    /// <summary>
    /// Return the session for data for a specific session id. 
    /// </summary>
    /// <param name="SessionId"></param>
    /// <returns></returns>
    public static DataRow Session(Int64 SessionId)
    {
      DataTable table;
      DataRow[] GDS_sessions;

      for (int i = 0; i < 2; i++)
      {
        GetTableFromDataset("GDS_SESSIONS", out table);
        GDS_sessions = table.Select("WS_SESSION_ID = '" + SessionId + "'");
        if (GDS_sessions.Length > 0)
        {
          DataRow r;

          r = table.NewRow();
          r.ItemArray = (Object[])GDS_sessions[0].ItemArray.Clone();

          return r;
        }

        if (i == 0)
        {
          ManageEvents(CachedTable.GDS_sessions);
        }
      }

      return null;
    }
    
    /// <summary>
    /// Return the session status for a specific session id.
    /// True: Session opened.
    /// False: Sesssion closed.
    /// </summary>
    /// <param name="SessionId"></param>
    /// <param name="Reload"></param>
    /// <returns></returns>
    public static Boolean IsSessionActive(Int64 SessionId, Boolean Reload)
    {
      DataTable table;
      DataRow[] GDS_sessions;

      for (int i = 0; i < 2; i++)
      {
        GetTableFromDataset("GDS_SESSIONS", out table);
        GDS_sessions = table.Select("WS_SESSION_ID = '" + SessionId + "'");

        if (GDS_sessions.Length > 0)
        {
          GDS_SessionStatus session_status;

          session_status = (GDS_SessionStatus) GDS_sessions[0]["ws_status"];
          if ( session_status == GDS_SessionStatus.Opened )
          {
            return true;
          }

          return false;
        }

        if (!Reload)
        {
          return false;
        }

        if (i == 0)
        {
          ManageEvents(CachedTable.GDS_sessions);
        }
      }

      return false;
    }

    /// <summary>
    /// Return the play session data for a specific play session.
    /// </summary>
    /// <param name="PlaySessionId"></param>
    /// <returns></returns>
    public static DataRow PlaySession(Int64 PlaySessionId)
    {
      DataTable table;
      DataRow[] play_session;

      for (int i = 0; i < 2; i++)
      {
        GetTableFromDataset("PLAY_SESSIONS", out table);
        play_session = table.Select("PS_PLAY_SESSION_ID = '" + PlaySessionId + "'");
        if (play_session.Length > 0)
        {
          DataRow r;

          r = table.NewRow();
          r.ItemArray = (Object[])play_session[0].ItemArray.Clone();
          return r;
        }

        if (i == 0)
        {
          ManageEvents(CachedTable.play_sessions);
        }
      }

      return null;
    }


    /// <summary>
    /// Return the play session status for a specific play session id.
    /// True: Play Session opened.
    /// False: Play Sesssion closed.
    /// </summary>
    /// <param name="PlaySessionId"></param>
    /// <param name="Reload"></param>
    /// <returns></returns>
    public static Boolean IsPlaySessionActive(Int64 PlaySessionId, Boolean Reload)
    {
      DataTable table;
      DataRow[] play_sessions;

      for (int i = 0; i < 2; i++)
      {
        GetTableFromDataset("PLAY_SESSIONS", out table);
        play_sessions = table.Select("PS_PLAY_SESSION_ID = '" + PlaySessionId + "'");

        if (play_sessions.Length > 0)
        {
          GDS_PlaySessionStatus play_session_status;

          play_session_status = (GDS_PlaySessionStatus)play_sessions[0]["PS_STATUS"];
          if (play_session_status == GDS_PlaySessionStatus.Opened)
          {
            return true;
          }

          return false;
        }

        if (!Reload)
        {
          return false;
        }

        if (i == 0)
        {
          ManageEvents(CachedTable.play_sessions);
        }
      }

      return false;
    }

    /// <summary>
    ///  Get Play Sessions Opened
    /// </summary>
    /// <param name="PlaySessionId"></param>
    /// <param name="Reload"></param>
    /// <returns></returns>
    public static Boolean GetPlaySessionsOpened(out Int32 NumPlaySessionsOpened)
    {
      DataTable table;
      DataRow[] play_sessions;

      NumPlaySessionsOpened = 0;
      for (int i = 0; i < 2; i++)
      {
        GetTableFromDataset("PLAY_SESSIONS", out table);
        play_sessions = table.Select("PS_STATUS = 0"); // Opened

        if (play_sessions.Length > 1)
        {
          NumPlaySessionsOpened = play_sessions.Length;

          return true;
        }

        if (i == 0)
        {
          ManageEvents(CachedTable.play_sessions);
        }
      }

      return false;

    } // GetPlaySessionsOpened

    /// <summary>
    /// Get GDS Service
    /// </summary>
    /// <param name="GDSServiceId"></param>
    /// <returns></returns>
    public static DataRow GDSService(Int64 GDSServiceId)
    {
      DataTable table;
      DataRow[] GDS_services;

      for (int i = 0; i < 2; i++)
      {
        GetTableFromDataset("GDS_SERVICES", out table);
        GDS_services = table.Select("WSVR_SERVICE_ID = '" + GDSServiceId + "'");
        if (GDS_services.Length > 0)
        {
          DataRow r;

          r = table.NewRow();
          r.ItemArray = (Object[])GDS_services[0].ItemArray.Clone();

          return r;
        }

        if (i == 0)
        {
          ManageEvents(CachedTable.GDS_services);
        }
      }

      return null;
    } // GDSService

    /// <summary>
    /// Get GDS service id
    /// </summary>
    /// <param name="GDSServiceName"></param>
    /// <param name="GDSServiceId"></param>
    /// <returns></returns>
    private static Boolean GetGDSServiceId (String GDSServiceName, out Int64 GDSServiceId)
    {
      DataTable table;
      DataRow[] GDS_services;
      Boolean insert_queues;

      GDSServiceId = 0;
      insert_queues = false;

      for (int i = 0; i < 3; i++)
      {
        GetTableFromDataset("GDS_SERVICES", out table);
        GDS_services = table.Select("WSVR_NAME = '" + GDSServiceName.ToUpper() + "'");

        if (GDS_services.Length > 0)
        {
          try
          {
            GDSServiceId = (Int64)GDS_services[0]["WSVR_SERVICE_ID"];
          }
          catch 
          {
            // Do nothing
          }
        }

        if (GDSServiceId > 0)
        {
          if (insert_queues)
          {
            RegisterCjQueue(GDSServiceId);
          }
          return true;
        }

        if (i == 0)
        {
          ManageEvents(CachedTable.GDS_services);
        }

        if (i > 0)
        {
          RegisterGDSService(GDSServiceName);
          ManageEvents(CachedTable.GDS_services);
          insert_queues = true;
        }
      }

      return false;
    } // GetGDSServiceId

    /// <summary>
    /// Create a new GDS service on database.
    /// </summary>
    /// <param name="GDSServiceName"></param>
    /// <returns></returns>
    private static void RegisterGDSService(String GDSServiceName)
    {
      DataTable table;
      DataRow GDS_service;
      StringBuilder sql_str;
      SqlCommand cmd_insert;

      GetTableFromDataset("GDS_SERVICES", out table);
      GDS_service = table.NewRow();
      GDS_service["WSVR_NAME"] = GDSServiceName;
      table.Rows.Add(GDS_service);

      // Insert New Service
      sql_str = new StringBuilder();
      sql_str.AppendLine("INSERT INTO GDS_SERVICES (WSVR_NAME) ");
      sql_str.AppendLine("VALUES ");
      sql_str.AppendLine("(@p1) ");

      cmd_insert = new SqlCommand(sql_str.ToString());
      da.InsertCommand = cmd_insert;
      cmd_insert.Parameters.Add("@p1", SqlDbType.NVarChar, 50, "WSVR_NAME").Value = GDS_service["WSVR_NAME"];

      Update(ds.Tables["GDS_SERVICES"]);
      ds.Tables["GDS_SERVICES"].AcceptChanges();

      return;
    }

    /// <summary>
    /// Create a new cj_queue on database.
    /// </summary>
    /// <param name="GDSServiceName"></param>
    /// <returns></returns>
    private static void RegisterCjQueue(Int64 GDSServiceId)
    {
      SqlCommand sql_command;
      String sql_str;
      Int32 num_rows_inserted;

      try
      {
        //
        // CJ_QUEUES
        //
        sql_str = "INSERT INTO CJ_QUEUES (CQ_QUEUE_ID) VALUES (@p1) ";

        sql_command = new SqlCommand(sql_str);
        sql_command.Connection = WGDB.Connection();
        sql_command.Transaction = sql_command.Connection.BeginTransaction();

        sql_command.Parameters.Add("@p1", SqlDbType.BigInt, 8, "CQ_QUEUE_ID").Value = GDSServiceId;

        num_rows_inserted = sql_command.ExecuteNonQuery();
        if (num_rows_inserted != 1)
        {
          return;
        }
        sql_command.Transaction.Commit();

        //
        // GDS_SERVICES_TO_CJ_QUEUES
        //
        sql_str = "INSERT INTO GDS_SERVICES_TO_CJ_QUEUES (WSCQ_SERVICE_ID, WSCQ_QUEUE_ID ) VALUES (@p1, @p2) ";

        sql_command = new SqlCommand(sql_str);
        sql_command.Connection = WGDB.Connection();
        sql_command.Transaction = sql_command.Connection.BeginTransaction();

        sql_command.Parameters.Add("@p1", SqlDbType.BigInt, 8, "WSCQ_SERVICE_ID").Value = GDSServiceId;
        sql_command.Parameters.Add("@p2", SqlDbType.BigInt, 8, "WSCQ_QUEUE_ID").Value = GDSServiceId;

        num_rows_inserted = sql_command.ExecuteNonQuery();
        if (num_rows_inserted != 1)
        {
          return;
        }
        sql_command.Transaction.Commit();

      }
      catch
      {
        return;
      }
    }

    /// <summary>
    /// Get Service Id
    /// </summary>
    /// <returns></returns>
    public static Int64 GetGDSServiceId()
    {
      Int64 service_id;

      if (!GetGDSServiceId(Environment.MachineName, out service_id))
      {
        return -1;
      }

      return service_id;
    }

    /// <summary>
    /// Get site
    /// </summary>
    /// <param name="SiteName"></param>
    /// <param name="SiteId"></param>
    /// <returns></returns>
    private static Boolean GetSite(out String SiteName, out Guid SiteId)
    {
      DataTable table;
      DataRow[] site;

      SiteName = "";
      SiteId = Guid.Empty;

      for (int i = 0; i < 2; i++)
      {
        GetTableFromDataset("SITE", out table);
        site = table.Select();

        if (site.Length > 0)
        {
          SiteId = (Guid)site[0]["ST_ID"];
          SiteName = (String)site[0]["ST_NAME"];

          return true;
        }

        if (i == 0)
        {
          ManageEvents(CachedTable.site);
        }
      }

      return false;
    } // GetSite

    /// <summary>
    /// Reload in memory a specific table.
    /// </summary>
    /// <param name="Table"></param>
    /// <param name="Connection"></param>
    /// <returns></returns>
    private static Boolean ReloadTable(CachedTable Table, SqlConnection Connection)
    {
      SqlCommand cmd;
      SqlDataAdapter da;
      DataTable dt_load;
      String table_name;
      StringBuilder sb;

      dt_load = null;
      da = null;
      cmd = null;

      table_name = Table.ToString();
      try
      {
        dt_load = new DataTable (table_name);
        da = new SqlDataAdapter();
        cmd = new SqlCommand();
        cmd.Connection = Connection;
        da.SelectCommand = cmd;

        switch (Table)
        {
          case CachedTable.GDS_sessions:

            sb = new StringBuilder ();
            sb.AppendLine("SELECT * FROM GDS_SESSIONS WHERE ws_session_id in");
            sb.AppendLine ("(SELECT MAX(ws_session_id) FROM GDS_SESSIONS GROUP BY ws_terminal_id)");
            cmd.CommandText = sb.ToString();
          break;

          case CachedTable.play_sessions:

            sb = new StringBuilder();
            sb.AppendLine (" SELECT   *");
            sb.AppendLine ("   FROM   PLAY_SESSIONS");
            sb.AppendLine ("  WHERE   PS_PLAY_SESSION_ID IN ( SELECT   MAX (PS_PLAY_SESSION_ID)");
            sb.AppendLine ("                                    FROM   PLAY_SESSIONS");
            sb.AppendLine ("                                GROUP BY   PS_TERMINAL_ID )");
            cmd.CommandText = sb.ToString();
          break;

          case CachedTable.GDS_services:

            sb = new StringBuilder();
            sb.AppendLine("SELECT WSVR_SERVICE_ID, UPPER(WSVR_NAME) AS WSVR_NAME, WSVR_WATCHDOG FROM GDS_SERVICES ");
            cmd.CommandText = sb.ToString();
          break;

          case CachedTable.site:

            sb = new StringBuilder();
            sb.AppendLine("SELECT TOP 1 ST_ID, UPPER(ST_NAME) AS ST_NAME FROM SITE ");
            cmd.CommandText = sb.ToString();
          break;

          // Tables: Games, Terminals
          default:
            cmd.CommandText = "SELECT * FROM " + table_name;
          break;
        }

        // Retrieve data
        da.Fill(dt_load);
        dbcache_lock.AcquireWriterLock(Timeout.Infinite);
        try
        {
          if (ds.Tables.IndexOf(table_name) >= 0)
          {
            ds.Tables.Remove(table_name);
          }

          ds.Tables.Add(dt_load);
        }
        finally
        {
          dbcache_lock.ReleaseWriterLock();
        }

        return true;
      }
      catch(Exception ex)
      {
        Log.Exception(ex);

        return false;
      }
      finally
      {
        if (da != null)
        {
          da.SelectCommand = null;
          da = null;
        }

        if (cmd != null)
        {
          cmd.Connection = null;
          cmd = null;
        }
      }
    }

    /// <summary>
    /// Get a table from dataset 'ds'. 
    /// Lock the ds for access.
    /// </summary>
    /// <param name="TableName"></param>
    /// <param name="Table"></param>
    private static void GetTableFromDataset(String TableName, out DataTable Table)
    {
      dbcache_lock.AcquireReaderLock(Timeout.Infinite);
      try
      {
        Table = ds.Tables[TableName];
      }
      finally
      {
        dbcache_lock.ReleaseReaderLock();
      }
    }

    /// <summary>
    /// Manage table reloads.
    /// </summary>
    static private void DbCacheThread()
    {
      int idx_table;
      TimeSpan elapsed;
      DateTime last_timeout;


      last_timeout = DateTime.Now;

      while (true)
      {
        SqlConnection conn;
        int idx_event;

        conn = null;

        try
        {
          idx_event = AutoResetEvent.WaitAny(event_reload, 2000, false);

          conn = WGDB.Connection ();

          if ( idx_event == AutoResetEvent.WaitTimeout )
          {
            //
            // Timeout
            //
            elapsed = DateTime.Now.Subtract(last_timeout);
            if ( elapsed.TotalMinutes >= 5 )
            {
              GDS_BusinessLogic.DB_UpdateTimeoutGDSSessions(conn);
              last_timeout = DateTime.Now;
            }
          }
          else
          {
            Boolean reloaded;

            reloaded = false;

            try
            {
              if (conn.State == ConnectionState.Open)
              {
                // Call the proper method depending on the the index
                reloaded = ReloadTable((CachedTable)idx_event, conn);

                if (reloaded)
                {
                  event_reloaded[idx_event].Set();
                  when_reloaded[idx_event] = DateTime.Now;
                }
              }
            }
            catch (Exception ex)
            {
              Log.Exception(ex);
            }
            finally
            {
              if (!reloaded)
              {
                event_reload[idx_event].Set();

                System.Threading.Thread.Sleep(1000);
              }
            }
          }

          idx_table = (int) CachedTable.terminals;
          elapsed = DateTime.Now.Subtract(when_reloaded[idx_table]);
          if ( elapsed.TotalSeconds >= 10 )
          {
            event_reload[idx_table].Set();
          }

          idx_table = (int) CachedTable.play_sessions;
          elapsed = DateTime.Now.Subtract(when_reloaded[idx_table]);
          if ( elapsed.TotalSeconds >= 10 )
          {
            event_reload[idx_table].Set();
          }
        }
        catch (Exception ex)
        {
          Log.Exception(ex);
        }
        finally
        {
          if (conn != null)
          {
            conn.Close();
            conn = null;
          }
        }
      }
    }


  }
}
