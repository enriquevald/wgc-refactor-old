using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using WSI.Common;
using System.Xml;
using System.Collections;

namespace WSI.GDS
{
  #region Enums

  public enum GDS_TerminalType
  { 
      IntermediateServer    = 0
    , GamingTerminal  = 1
  }

  public enum GDS_SessionStatus
  {
      Opened   = 0
    , Closed = 1
    , Abandoned = 2
    , Timeout= 3
  }

  public enum GDS_PlaySessionStatus
  {
    Opened = 0
  , Closed = 1
  , Abandoned = 2
  , ManualClosed = 3
  }

  public enum GDS_PlaySessionType
  {
    NONE = 0,
    CADILLAC_JACK = 1,
    WIN = 2,
    ALESIS = 3,
  }

  public enum AccountType
  {
    ACCOUNT_UNKNOWN = 0,
    ACCOUNT_CADILLAC_JACK = 1,
    ACCOUNT_WIN = 2,
    ACCOUNT_ALESIS = 3,
  }

  public enum CashlessMode
  {
    NONE           = 0,
    WIN            = 1,
    ALESIS         = 2,
    WIN_AND_ALESIS = 3, // WIN & ALESIS
  }

  #endregion Enums

  public class GDS_BusinessLogic
  {
    static CashlessMode cashless_mode = CashlessMode.NONE;

    #region CashlessMode
    
    //------------------------------------------------------------------------------
    // PURPOSE: Cashless mode property
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    //        
    //   NOTES :
    //
    static public CashlessMode CashlessMode
    {
      get
      {
        SqlConnection sql_conn;
        SqlCommand sql_command;
        SqlDataReader sql_reader;
        String sql_str;

        if (cashless_mode != CashlessMode.NONE)
        {
          return cashless_mode;
        }

        sql_conn = null;
        sql_reader = null;

        // Read from General Params
        try
        {
          sql_conn = WGDB.Connection();

          //
          // Read Location Repository values
          //
          sql_str = " SELECT GP_KEY_VALUE                   " +
                    "   FROM GENERAL_PARAMS                 " +
                    "  WHERE GP_GROUP_KEY    = 'Cashless'   " +
                    "    AND GP_SUBJECT_KEY  = 'Server'     ";

          sql_command = new SqlCommand(sql_str);
          sql_command.Connection = sql_conn;

          sql_reader = sql_command.ExecuteReader();

          while (sql_reader.Read())
          {
            if (!sql_reader.IsDBNull(0))
            {
              if (sql_reader.GetString(0) == "ALESIS")
              {
                cashless_mode = CashlessMode.ALESIS;
              }
              else if (sql_reader.GetString(0) == "WIN")
              {
                cashless_mode = CashlessMode.WIN;
              }
              else if (sql_reader.GetString(0) == "WIN+ALESIS")
              {
                cashless_mode = CashlessMode.WIN_AND_ALESIS;
              }
            }
          }
          sql_reader.Close();
        }
        catch (Exception ex)
        {
          Log.Exception(ex);
        }
        finally
        {
          if (sql_reader != null)
          {
            sql_reader.Close();
            sql_reader.Dispose();
            sql_reader = null;
          }

          if (sql_conn != null)
          {
            sql_conn.Close();
            sql_conn = null;
          }
        }

        return cashless_mode;
      }
    } // CasshlessMode

    #endregion


    #region EnrollAndSession

    public static Boolean DB_UpdateTerminalProvider(String ProviderId, int TerminalId, SqlTransaction Trx)
    {
      String sql_str;
      SqlCommand sql_command;
      Int32 num_rows_updated;

      //
      // Update Abandoned Sessions
      //
      sql_str = "UPDATE TERMINALS SET TE_PROVIDER_ID = @p1 " +
                                  "WHERE TE_TERMINAL_ID = @p2 ";

      sql_command = new SqlCommand(sql_str);
      sql_command.Connection = Trx.Connection;
      sql_command.Transaction = Trx;

      sql_command.Parameters.Add("@p1", SqlDbType.NVarChar, 50, "TE_PROVIDER_ID").Value = ProviderId;
      sql_command.Parameters.Add("@p2", SqlDbType.Int, 4, "TE_TERMINAL_ID").Value = TerminalId;

      try
      {
        num_rows_updated = sql_command.ExecuteNonQuery();
      }
      catch
      {
        return false;
      }
      return true;
    }

    public static bool DB_InsertTerminal(String ExternalTerminalId, GDS_TerminalType Type, SqlTransaction Trx)
    {
      SqlCommand sql_command;
      String sql_str;
      int affected_rows;
      // 
      // Get Terminal Id
      //
      sql_str = "INSERT INTO TERMINALS (TE_TYPE" + 
                                        ", TE_NAME" + 
                                        ", TE_EXTERNAL_ID" + 
                                        ", TE_BLOCKED" + 
                                        ", TE_ACTIVE) " + 
                                        "VALUES (@p1,@p2,@p3,@p4,@p5);";

      sql_command = new SqlCommand(sql_str);
      sql_command.Connection = Trx.Connection;
      sql_command.Transaction = Trx;

      sql_command.Parameters.Add("@p1", SqlDbType.Int, 4, "TE_TYPE").Value = Type;
      sql_command.Parameters.Add("@p2", SqlDbType.NVarChar, 40, "TE_NAME").Value = ExternalTerminalId;
      sql_command.Parameters.Add("@p3", SqlDbType.NVarChar, 40, "TE_EXTERNAL_ID").Value = ExternalTerminalId;
      sql_command.Parameters.Add("@p4", SqlDbType.Bit, 1, "TE_BLOCKED").Value = 0;
      sql_command.Parameters.Add("@p5", SqlDbType.Bit, 1, "TE_ACTIVE").Value = 1;

      try
      {
        affected_rows = sql_command.ExecuteNonQuery();
        if (affected_rows > 0)
        {
          return true;
        }
        else
        {
          return false;
        }
      }
      catch
      {
        return false;
      }
      
    }

    /// <summary>
    /// Returns true if the given terminal name exists in the database
    /// </summary>
    /// <param name="ExternalTerminalId"></param>
    /// <param name="Type"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    public static Boolean ExistsTerminal(String ExternalTerminalId, GDS_TerminalType Type, SqlTransaction Trx)
    {
      SqlCommand sql_command;
      SqlDataReader reader;
      String sql_str;
      // 
      // Get Terminal Id
      //
      sql_str = "SELECT TE_TERMINAL_ID FROM TERMINALS WHERE TE_TYPE = @p1 AND TE_EXTERNAL_ID = @p2";

      sql_command = new SqlCommand(sql_str);
      sql_command.Connection = Trx.Connection;
      sql_command.Transaction = Trx;

      sql_command.Parameters.Add("@p1", SqlDbType.Int, 4, "TE_TYPE").Value = Type;
      sql_command.Parameters.Add("@p2", SqlDbType.NVarChar, 40, "TE_EXTERNAL_ID").Value = ExternalTerminalId;

      try
      {
        reader = sql_command.ExecuteReader();
        if (reader.HasRows)
        {
          reader.Close();
          reader.Dispose();
          reader = null;
          return true;
        }
        else
        {
          reader.Close();
          reader.Dispose();
          reader = null;
          return false;
        }
      }
      catch
      {
        reader = null;
        return false;
      }
    }




    /// <summary>
    /// Get Database enroll data
    /// </summary>
    /// <param name="ExternalTerminalId"></param>
    /// <param name="Type"></param>
    /// <param name="TerminalId"></param>
    /// <param name="SessionId"></param>
    /// <param name="SequenceId"></param>
    /// <param name="TransactionId"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    public static Boolean DB_GetEnrollData(String ExternalTerminalId, 
                                           GDS_TerminalType Type,
                                           out Int32 TerminalId,
                                           out Int64 SessionId,
                                           out Int64 SequenceId,
                                           out Int64 TransactionId,
                                           out Boolean EnrolledTerminal,
                                           out String ProviderId, 
                                           SqlTransaction Trx)
    {
      SqlCommand sql_command;
      SqlDataReader reader;
      SqlParameter p;
      String sql_str;
      Int32 num_rows_inserted;
      Int32 num_rows_updated;
      Int32 max_licensed_terminals;
      Int32 num_enrolled_terminals;

      // Initialize output parameters
      TerminalId = 0;
      SessionId = 0;
      SequenceId = 0;
      TransactionId = 0;
      ProviderId = "";
      EnrolledTerminal = true;

      // 
      // Insert Terminal if it doesn't exists.
      //
      if (!ExistsTerminal(ExternalTerminalId, Type,Trx))
      {
        if (!DB_InsertTerminal(ExternalTerminalId, Type, Trx))
        {
          return false;
        }
      }


      // 
      // Get Terminal Id
      //
      sql_str = "SELECT TE_TERMINAL_ID, TE_PROVIDER_ID FROM TERMINALS WHERE TE_TYPE = @p1 AND TE_EXTERNAL_ID = @p2 AND TE_BLOCKED = 0";

      sql_command = new SqlCommand (sql_str);
      sql_command.Connection = Trx.Connection;
      sql_command.Transaction = Trx;

      sql_command.Parameters.Add("@p1", SqlDbType.Int, 4, "TE_TYPE").Value = Type;
      sql_command.Parameters.Add("@p2", SqlDbType.NVarChar, 40, "TE_EXTERNAL_ID").Value = ExternalTerminalId;

      reader = null;

      try
      {
        reader = sql_command.ExecuteReader();
        if (reader.Read())
        {
          TerminalId = (Int32)reader[0];
          ProviderId = reader[1].ToString();
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
        Log.Warning("Exception throw: Error reading Terminal information" + TerminalId.ToString());
        return false;
      }
      finally
      {
        if (reader != null)
        {
          reader.Close();
          reader.Dispose();
          reader = null;
        }
      }


      //
      // Get Last Opened Session Id 
      //
      sql_str = "SELECT WS_SESSION_ID, " +
                       "WS_LAST_SEQUENCE_ID, " +
                       "WS_LAST_TRANSACTION_ID " +
                  "FROM GDS_SESSIONS " +
                 "WHERE WS_TERMINAL_ID = @p1 " +
                    "AND WS_STATUS = 0 " +
               "ORDER BY WS_SESSION_ID DESC";

      sql_command = new SqlCommand(sql_str);
      sql_command.Connection = Trx.Connection;
      sql_command.Transaction = Trx;

      sql_command.Parameters.Add("@p1", SqlDbType.Int, 4, "WS_TERMINAL_ID").Value = TerminalId;

      reader = null;

      try
      {
        reader = sql_command.ExecuteReader();
        if (reader.Read() && !reader.IsDBNull(0))
        {
          SessionId = (Int64)reader[0];
          SequenceId = (Int64)reader[1];
          TransactionId = (Int64)reader[2];
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
        Log.Warning("Exception throw: Error reading Max session. TerminalId: " + TerminalId.ToString());
        throw new GDS_Exception(GDS_ResponseCodes.GDS_RC_ERROR, "DB Error reading sessions.");
      }
      finally
      {
        if (reader != null)
        {
          reader.Close();
          reader.Dispose();
          reader = null;
        }
      }

      if ( SessionId == 0 )
      {
        //
        // Get MAX Last Sequence Id, Transaction Id
        //
        sql_str = "SELECT MAX(WS_LAST_SEQUENCE_ID), " +
                         "MAX(WS_LAST_TRANSACTION_ID) " +
                    "FROM GDS_SESSIONS " +
                   "WHERE WS_TERMINAL_ID = @p1";

        sql_command = new SqlCommand(sql_str);
        sql_command.Connection = Trx.Connection;
        sql_command.Transaction = Trx;

        sql_command.Parameters.Add("@p1", SqlDbType.Int, 4, "WS_TERMINAL_ID").Value = TerminalId;

        reader = null;

        try
        {
          reader = sql_command.ExecuteReader();
          if (reader.Read() && !reader.IsDBNull(0))
          {
            SequenceId = (Int64)reader[0];
            TransactionId = (Int64)reader[1];
          }
        }
        catch (Exception ex)
        {
          Log.Exception(ex);
          Log.Warning("Exception throw: Error reading Max session. TerminalId: " + TerminalId.ToString());
          throw new GDS_Exception(GDS_ResponseCodes.GDS_RC_ERROR, "DB Error reading sessions.");
        }
        finally
        {
          if (reader != null)
          {
            reader.Close();
            reader.Dispose();
            reader = null;
          }
        }
      }

      //
      // Update Abandoned Sessions
      //
      sql_str = "UPDATE GDS_SESSIONS SET WS_STATUS = @p1, " + 
                                        "WS_FINISHED = GETDATE() " +
                                  "WHERE WS_TERMINAL_ID = @p2 " +
                                    "AND WS_STATUS = 0 " +
                                    "AND WS_FINISHED IS NULL " +
                                    "AND WS_SESSION_ID < @p3 ";

      sql_command = new SqlCommand(sql_str);
      sql_command.Connection = Trx.Connection;
      sql_command.Transaction = Trx;

      sql_command.Parameters.Add("@p1", SqlDbType.Int, 4, "WS_STATUS").Value = GDS_SessionStatus.Abandoned;
      sql_command.Parameters.Add("@p2", SqlDbType.Int, 4, "WS_TERMINAL_ID").Value = TerminalId;
      sql_command.Parameters.Add("@p3", SqlDbType.BigInt, 8, "WS_SESSION_ID").Value = SessionId;

      num_rows_updated = sql_command.ExecuteNonQuery();

      if (Type == GDS_TerminalType.GamingTerminal)
      {
        // TODO: Get max licensed terminals from license.
        max_licensed_terminals = Int32.MaxValue;

        // 
        // Get num terminals enrolled
        //
        num_enrolled_terminals = 0;
        sql_str = "SELECT COUNT(*) " +
                    "FROM GDS_SESSIONS, TERMINALS " +
                   "WHERE WS_STATUS       = 0 " +   // Opened
                     "AND WS_TERMINAL_ID  = TE_TERMINAL_ID " + 
                     "AND TE_TYPE         = 1";     // Gaming Terminal

        sql_command = new SqlCommand(sql_str);
        sql_command.Connection = Trx.Connection;
        sql_command.Transaction = Trx;

        reader = null;

        try
        {
          reader = sql_command.ExecuteReader();
          if (reader.Read() && !reader.IsDBNull(0))
          {
            num_enrolled_terminals = (Int32) reader[0];
          }
        }
        catch (Exception ex)
        {
          Log.Exception(ex);          
          throw new GDS_Exception(GDS_ResponseCodes.GDS_RC_ERROR, "DB Error reading opened sessions.");
        }
        finally
        {
          if (reader != null)
          {
            reader.Close();
            reader.Dispose();
            reader = null;
          }
        }

        if (num_enrolled_terminals >= max_licensed_terminals)
        {
          EnrolledTerminal = false;

          return true;
        }
      }

      // Max Licensed terminals reached.

      if ( SessionId == 0 )
      {
        //
        // Insert New Session
        //
        sql_str = "INSERT INTO GDS_SESSIONS (WS_TERMINAL_ID, " +
                                            "WS_LAST_SEQUENCE_ID, " +
                                            "WS_LAST_TRANSACTION_ID) " +
                                  "VALUES " +
                                            "(@p1, @p2, @p3) " +
                                    " SET   @p4 = SCOPE_IDENTITY()";

        sql_command = new SqlCommand(sql_str);
        sql_command.Connection = Trx.Connection;
        sql_command.Transaction = Trx;

        sql_command.Parameters.Add("@p1", SqlDbType.Int, 4, "WS_TERMINAL_ID").Value = TerminalId;
        sql_command.Parameters.Add("@p2", SqlDbType.BigInt, 8, "WS_LAST_SEQUENCE_ID").Value = SequenceId;
        sql_command.Parameters.Add("@p3", SqlDbType.BigInt, 8, "WS_LAST_TRANSACTION_ID").Value = TransactionId;

        p = sql_command.Parameters.Add("@p4", SqlDbType.BigInt, 8, "WS_SESSION_ID");
        p.Direction = ParameterDirection.Output;

        num_rows_inserted = sql_command.ExecuteNonQuery();

        if (num_rows_inserted != 1)
        {
          Log.Warning("Exception throw: GDS_Session not inserted.TerminalId: " + TerminalId.ToString() + " SeqId: " + SequenceId.ToString());
          throw new GDS_Exception(GDS_ResponseCodes.GDS_RC_ERROR, "SESSION Not Inserted.");
        }

        SessionId = (Int64) p.Value;
      }

      return true;
    } // DB_GetEnrollData

    /// <summary>
    /// Set session finished in DB.
    /// </summary>
    /// <param name="TerminalId"></param>
    /// <param name="SessionId"></param>
    /// <param name="Status"></param>
    /// <param name="Trx"></param>
      public static void DB_SessionFinished(Int32 TerminalId,
                                           Int64 SessionId,
                                           GDS_SessionStatus Status,
                                           SqlTransaction Trx)
    {
      SqlCommand sql_command;
      String sql_str;
      Int32 num_rows_updated;

      // Avoid the update when one of those fields is zero.
      // - They were reset to zero in a previous message
      if (TerminalId == 0
          || SessionId == 0)
      {
        return;
      }
      

      sql_str = "UPDATE GDS_SESSIONS SET WS_STATUS = @p1, " +  
                                        "WS_FINISHED = GETDATE() " +
                                  "WHERE WS_TERMINAL_ID = @p2 " +
                                    "AND WS_SESSION_ID = @p3 ";

      sql_command = new SqlCommand(sql_str);
      sql_command.Connection = Trx.Connection;
      sql_command.Transaction = Trx;

      sql_command.Parameters.Add("@p1", SqlDbType.Int, 4, "WS_STATUS").Value = Status;
      sql_command.Parameters.Add("@p2", SqlDbType.Int, 4, "WS_TERMINAL_ID").Value = TerminalId;
      sql_command.Parameters.Add("@p3", SqlDbType.BigInt, 8, "WS_SESSION_ID").Value = SessionId;

      num_rows_updated = sql_command.ExecuteNonQuery();

      if (num_rows_updated != 1)
      {
        Log.Warning("Exception throw: Session not updated.TerminalId: " + TerminalId.ToString() + " SessionId: " + SessionId.ToString());
        throw new GDS_Exception(GDS_ResponseCodes.GDS_RC_ERROR, "SESSION Not Updated.");
      }

    } // DB_SessionFinshed

    /// <summary>
    /// Update timeout GDS sessions
    /// </summary>
    /// <param name="Connection"></param>
    public static void DB_UpdateTimeoutGDSSessions(SqlConnection Connection)
    {
      SqlTransaction sql_trx;
      SqlCommand sql_command;
      String sql_str;
      Int32 num_rows_updated;
      Boolean error;

      error = true;
      sql_trx = null;

      try
      {
        sql_trx = Connection.BeginTransaction();

        //
        // Update Timeout Sessions
        //
        sql_str = "UPDATE GDS_SESSIONS SET WS_STATUS = @p1, " +
                                          "WS_FINISHED = GETDATE() " +
                                    "WHERE DATEDIFF(MINUTE ,WS_LAST_RCVD_MSG, GETDATE()) >= 5 " +
                                      "AND WS_STATUS = 0 " +
                                      "AND WS_FINISHED IS NULL ";

        sql_command = new SqlCommand(sql_str);
        sql_command.Connection = sql_trx.Connection;
        sql_command.Transaction = sql_trx;

        sql_command.Parameters.Add("@p1", SqlDbType.Int, 4, "WS_STATUS").Value = GDS_SessionStatus.Timeout;

        num_rows_updated = sql_command.ExecuteNonQuery();

        error = false;
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
      finally
      {
        if (sql_trx != null)
        {
          if (error)
          {
            sql_trx.Rollback();
          }
          else
          {
            sql_trx.Commit();
          }
        }
      }

    } // DB_UpdateTimeoutGDSSessions

    #endregion EnrollAndSession

    #region GDS_MessagesAndTrx

    public class GDS_InsertGDSMessage
    {
      public Int32 terminal_id;
      public Int64 session_id;
      public Boolean towards_to_terminal;
      public String msg_xml_text;
      public Int64 sequence_id;
      public Int64 out_message_id;
      public Boolean error;
      public string error_message;
    }

    public class class_InsertGDSMessages
    {
      public Int32 num_items;
      private ArrayList items = new ArrayList();  // GDS_InsertGDSMessage

      public void InsertItem(GDS_InsertGDSMessage Item)
      {
        items.Add(Item);
        num_items++;
      }

      public GDS_InsertGDSMessage GetItem(Int32 IdxItem)
      {
        if (items.Count <= IdxItem)
        {
          return null;
        }

        return (GDS_InsertGDSMessage)items[IdxItem];
      }
    }

    /// <summary>
    /// For row updated in DB_InsertGDSMessageArray function
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    static void da_GDSMessageRowUpdated(object sender, SqlRowUpdatedEventArgs e)
    {
      if (e.Status == UpdateStatus.ErrorsOccurred)
      {
        e.Row.RowError = e.Errors.Message;
        e.Status = UpdateStatus.SkipCurrentRow;
        e.Row["WM_MESSAGE_ID"] = 0;
      }
      else if (e.StatementType == StatementType.Insert)
      {
        e.Row["WM_MESSAGE_ID"] = (Int64)e.Command.Parameters["@p6"].Value;
      }
    }

    /// <summary>
    /// Insert into GDS_message in array.
    /// </summary>
    /// <param name="TerminalId"></param>
    /// <param name="SessionId"></param>
    /// <param name="TowardsToTerminal"></param>
    /// <param name="MsgXmlText"></param>
    /// <param name="SequenceId"></param>
    /// <param name="NumInserts"></param>
    /// <param name="MessageId"></param>
    /// <param name="Trx"></param>
    public static void DB_InsertGDSMessageArray(class_InsertGDSMessages InsertArray,
                                                SqlTransaction Trx)
    {
      DataTable data_table;
      SqlDataAdapter da;
      StringBuilder sb;
      SqlCommand cmd_select;
      SqlCommand cmd_insert;
      String sql_str;
      Int64 idx_insert; 
      DataRow message_row;
      SqlParameter p;
      GDS_BusinessLogic.GDS_InsertGDSMessage insert_message;

      try
      {
        da = new SqlDataAdapter();
        data_table = new DataTable();

        sb = new StringBuilder();
        sb.AppendLine("SELECT * FROM GDS_MESSAGES");
        cmd_select = new SqlCommand();
        cmd_select.Connection = Trx.Connection;
        cmd_select.Transaction = Trx;
        cmd_select.CommandText = sb.ToString();
        da.SelectCommand = cmd_select;

        da.FillSchema(data_table, SchemaType.Source);

        data_table.Columns["WM_MESSAGE_ID"].AutoIncrement = true;
        data_table.Columns["WM_MESSAGE_ID"].AutoIncrementSeed = -1;
        data_table.Columns["WM_MESSAGE_ID"].AutoIncrementStep = -1;
        data_table.Columns["WM_MESSAGE_ID"].ReadOnly = false;
        data_table.Columns["WM_DATETIME"].AllowDBNull = true;

        // test
        data_table.Columns["WM_MESSAGE"].AllowDBNull = true;

        sql_str = "INSERT INTO GDS_MESSAGES (WM_TERMINAL_ID, WM_SESSION_ID, WM_TOWARDS_TO_TERMINAL, WM_MESSAGE, WM_SEQUENCE_ID) " +
                  "VALUES (@p1, @p2, @p3, @p4, @p5)" +
                  "   SET   @p6 = SCOPE_IDENTITY()";

        sb = new StringBuilder();
        sb.AppendLine(sql_str);
        cmd_insert = new SqlCommand();
        cmd_insert.Connection = Trx.Connection;
        cmd_insert.Transaction = Trx;
        cmd_insert.CommandText = sb.ToString();
        da.InsertCommand = cmd_insert;

        cmd_insert.Parameters.Add("@p1", SqlDbType.Int, 4, "WM_TERMINAL_ID");
        cmd_insert.Parameters.Add("@p2", SqlDbType.BigInt, 8, "WM_SESSION_ID");
        cmd_insert.Parameters.Add("@p3", SqlDbType.Bit, 1, "WM_TOWARDS_TO_TERMINAL");
        cmd_insert.Parameters.Add("@p4", SqlDbType.Xml, Int32.MaxValue, "WM_MESSAGE");
        cmd_insert.Parameters.Add("@p5", SqlDbType.BigInt, 8, "WM_SEQUENCE_ID");

        p = cmd_insert.Parameters.Add("@p6", SqlDbType.BigInt, 8, "WM_MESSAGE_ID");
        p.Direction = ParameterDirection.Output;

        da.RowUpdated += new SqlRowUpdatedEventHandler (da_GDSMessageRowUpdated);

        for (idx_insert = 0; idx_insert < InsertArray.num_items; idx_insert++)
        {
          insert_message = InsertArray.GetItem((Int32)idx_insert);

          message_row = data_table.NewRow();
          message_row["WM_TERMINAL_ID"] = insert_message.terminal_id;
          message_row["WM_SESSION_ID"] = insert_message.session_id;
          message_row["WM_TOWARDS_TO_TERMINAL"] = insert_message.towards_to_terminal;
          message_row["WM_MESSAGE"] = insert_message.msg_xml_text;
          message_row["WM_SEQUENCE_ID"] = insert_message.sequence_id;
          data_table.Rows.Add(message_row);
        }

        da.UpdateBatchSize = 500;
        da.InsertCommand.UpdatedRowSource = UpdateRowSource.OutputParameters;
        da.ContinueUpdateOnError = true;
        da.Update(data_table);

        for (idx_insert = 0; idx_insert < InsertArray.num_items; idx_insert++)
        {
          insert_message = InsertArray.GetItem((Int32)idx_insert);

          if (data_table.Rows[((Int32)(idx_insert))].HasErrors)
          {
            insert_message.out_message_id = 0;
            insert_message.error = true;
            insert_message.error_message = data_table.Rows[((Int32)(idx_insert))].RowError;
          }
          else
          {
            insert_message.out_message_id = (Int64)data_table.Rows[((Int32)(idx_insert))]["WM_MESSAGE_ID"];
          }
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);

        throw new Exception ("TEST Insert Array Failed.");
      }
    }

    /// <summary>
    /// Insert message in the database
    /// </summary>
    /// <param name="TerminalId"></param>
    /// <param name="SessionId"></param>
    /// <param name="TowardsToTerminal"></param>
    /// <param name="MsgXmlText"></param>
    /// <param name="SequenceId"></param>
    /// <param name="Trx"></param>
    public static void DB_InsertGDSMessage(Int32 TerminalId,
                                           Int64 SessionId,
                                           Boolean TowardsToTerminal,
                                           String MsgXmlText,
                                           Int64 SequenceId,
                                           out Int64 MessageId,
                                           SqlTransaction Trx)
    {
      SqlCommand sql_command;
      SqlParameter p;
      String sql_str;
      Int32 num_rows_inserted;

      sql_str = "INSERT INTO GDS_MESSAGES (WM_TERMINAL_ID, WM_SESSION_ID, WM_TOWARDS_TO_TERMINAL, WM_MESSAGE, WM_SEQUENCE_ID) " +
                "VALUES (@p1, @p2, @p3, @p4, @p5) " +
                   "SET @p6 = SCOPE_IDENTITY()";

      sql_command = new SqlCommand(sql_str);
      sql_command.Connection = Trx.Connection;
      sql_command.Transaction = Trx;

      if (TerminalId != 0)
      {
        sql_command.Parameters.Add("@p1", SqlDbType.Int, 4, "WM_TERMINAL_ID").Value = TerminalId;
      }
      else
      {
        sql_command.Parameters.Add("@p1", SqlDbType.Int, 4, "WM_TERMINAL_ID").Value = DBNull.Value;
      }

      if (SessionId != 0)
      {
        sql_command.Parameters.Add("@p2", SqlDbType.BigInt, 8, "WM_SESSION_ID").Value = SessionId;
      }
      else
      {
        sql_command.Parameters.Add("@p2", SqlDbType.BigInt, 8, "WM_SESSION_ID").Value = DBNull.Value;
      }

      sql_command.Parameters.Add("@p3", SqlDbType.Bit, 1, "WM_TOWARDS_TO_TERMINAL").Value = TowardsToTerminal;
      sql_command.Parameters.Add("@p4", SqlDbType.Xml, Int32.MaxValue, "WM_MESSAGE").Value = MsgXmlText;
      sql_command.Parameters.Add("@p5", SqlDbType.BigInt, 8, "WM_SEQUENCE_ID").Value = SequenceId;

      p = sql_command.Parameters.Add("@p6", SqlDbType.BigInt, 8, "WM_MESSAGE_ID");
      p.Direction = ParameterDirection.Output;

      try
      {
        num_rows_inserted = sql_command.ExecuteNonQuery();
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
        Log.Warning("Exception throw: DB_InsertGDSMessage. TerminalId: " + TerminalId.ToString() + ", SessionId: " + SessionId.ToString() + ", SequenceId: " + SequenceId.ToString());

        throw new GDS_Exception(GDS_ResponseCodes.GDS_RC_ERROR, "DB Error inserting GDS message.");
      }

      if (num_rows_inserted != 1)
      {
        Log.Warning("Exception throw: GDS message not inserted.TerminalId: " + TerminalId.ToString() + " SessionId: " + SessionId.ToString() + " SeqId: " + SequenceId.ToString());
        throw new GDS_Exception(GDS_ResponseCodes.GDS_RC_ERROR, "MESSAGE Not Inserted.");
      }

      MessageId = (Int64)p.Value;

    } // DB_InsertGDSMessage

    /// <summary>
    /// Load GDS Request Message
    /// </summary>
    /// <param name="SequenceNumber"></param>
    /// <param name="TerminalId"></param>
    /// <param name="SequenceId"></param>
    /// <param name="SessionId"></param>
    /// <param name="GDSRequestXml"></param>
    /// <param name="Trx"></param>
    public static void DB_LoadGDSRequest(Int32 TerminalId,
                                         Int64 SequenceId,
                                         Int64 SessionId,
                                         out String GDSRequestXml,
                                         SqlTransaction Trx)
    {
      SqlCommand sql_command;
      SqlDataReader reader;
      SqlDataReader reader2;
      String sql_str;
      Int64 GDS_request_msg_id;

      GDSRequestXml = "";
      GDS_request_msg_id = 0;

      //
      // Get GDS Request Msg Id
      //
      sql_str = "SELECT WTX_REQUEST_MSG_ID " +
                  "FROM GDS_TRANSACTIONS " +
                 "WHERE WTX_TERMINAL_ID = @p1 " +
                   "AND WTX_SEQUENCE_ID = @p2 " +
                   "AND WTX_REQUESTED_BY_TERMINAL = 1 " +
                   "AND WTX_SESSION_ID = @p3";

      sql_command = new SqlCommand(sql_str);
      sql_command.Connection = Trx.Connection;
      sql_command.Transaction = Trx;

      sql_command.Parameters.Add("@p1", SqlDbType.Int, 4, "WTX_TERMINAL_ID").Value = TerminalId;
      sql_command.Parameters.Add("@p2", SqlDbType.BigInt, 8, "WTX_SEQUENCE_ID").Value = SequenceId;
      sql_command.Parameters.Add("@p3", SqlDbType.BigInt, 8, "WTX_SESSION_ID").Value = SessionId;

      reader = null;

      try
      {
        reader = sql_command.ExecuteReader();
        if (reader.Read() && !reader.IsDBNull(0))
        {
          GDS_request_msg_id = (Int64)reader[0];
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
        Log.Warning("Exception throw: reading GDS request transaction.TerminalId: " + TerminalId.ToString() + " SessionId: " + SessionId.ToString() + " SeqId: " + SequenceId.ToString());
        throw new GDS_Exception(GDS_ResponseCodes.GDS_RC_ERROR, "DB Error reading GDS request trx.");
      }
      finally
      {
        if (reader != null)
        {
          reader.Close();
          reader.Dispose();
          reader = null;
        }
      }

      sql_str = "SELECT WM_MESSAGE " +
                  "FROM GDS_MESSAGES " +
                 "WHERE WM_MESSAGE_ID = @p1 ";

      sql_command = new SqlCommand(sql_str);
      sql_command.Connection = Trx.Connection;
      sql_command.Transaction = Trx;

      sql_command.Parameters.Add("@p1", SqlDbType.BigInt, 8, "WM_MESSAGE_ID").Value = GDS_request_msg_id;

      reader2 = null;

      try
      {
        reader2 = sql_command.ExecuteReader();
        if (reader2.Read() && !reader2.IsDBNull(0))
        {
          GDSRequestXml = (String)reader2[0];
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
        Log.Warning("Exception throw: reading GDS request message.MessageId: " + GDS_request_msg_id.ToString());
        throw new GDS_Exception(GDS_ResponseCodes.GDS_RC_ERROR, "DB Error reading GDS request trx.");
      }
      finally
      {
        if (reader2 != null)
        {
          reader2.Close();
          reader2.Dispose();
          reader2 = null;
        }
      }

    } // DB_LoadGDSRequest

    /// <summary>
    /// Insert GDS transaction
    /// </summary>
    /// <param name="TerminalId"></param>
    /// <param name="SessionId"></param>
    /// <param name="SequenceId"></param>
    /// <param name="RequestedByTerminal"></param>
    /// <param name="TransactionId"></param>
    /// <param name="RequestMsgXmlText"></param>
    /// <param name="Trx"></param>
    public static void DB_InsertGDSTransaction(Int32 TerminalId,
                                               Int64 SessionId,
                                               Int64 SequenceId,
                                               Boolean RequestedByTerminal,
                                               Int64 TransactionId,
                                               Int64 RequestMsgId,
                                               SqlTransaction Trx)
    {
      SqlCommand  sql_command;
      String sql_str;
      Int32 num_rows_inserted;
      Int32 num_rows_updated;

      try
      {
        //
        // Insert into GDS_TRANSACTIONS table
        //
        sql_str = "INSERT INTO GDS_TRANSACTIONS (WTX_TERMINAL_ID, " +
                                                "WTX_SEQUENCE_ID, " +
                                                "WTX_REQUESTED_BY_TERMINAL, " +
                                                "WTX_SESSION_ID, " +
                                                "WTX_STATUS, " +
                                                "WTX_REQUEST_MSG_ID) " +
                                        "VALUES (@p1, " +
                                                "@p2, " +
                                                "@p3, " +
                                                "@p4, " +
                                                "0, " +   // Running
                                                "@p5)";

        sql_command = new SqlCommand(sql_str);
        sql_command.Connection = Trx.Connection;
        sql_command.Transaction = Trx;

        sql_command.Parameters.Add("@p1", SqlDbType.Int, 4, "WTX_TERMINAL_ID").Value = TerminalId;
        sql_command.Parameters.Add("@p2", SqlDbType.BigInt, 8, "WTX_SEQUENCE_ID").Value = SequenceId;
        sql_command.Parameters.Add("@p3", SqlDbType.Bit, 1, "WTX_REQUESTED_BY_TERMINAL").Value = RequestedByTerminal;
        sql_command.Parameters.Add("@p4", SqlDbType.BigInt, 8, "WTX_SESSION_ID").Value = SessionId;
        sql_command.Parameters.Add("@p5", SqlDbType.BigInt, 8, "WTX_REQUEST_MSG_ID").Value = RequestMsgId;

        num_rows_inserted = sql_command.ExecuteNonQuery();

        if (num_rows_inserted != 1)
        {
          Log.Warning("Exception throw: inserting GDS transaction.TerminalId: " + TerminalId.ToString() + " SessionId: " + SessionId.ToString() + " SeqId: " + SequenceId.ToString() + " RequestMsgId: " + RequestMsgId.ToString());
          throw new GDS_Exception(GDS_ResponseCodes.GDS_RC_ERROR, "TRANSACTION Not Inserted.");
        }

        //
        // Update SequenceId GDS_SESSIONS table
        //
        if (TransactionId > 0)
        {
          sql_str = "UPDATE GDS_SESSIONS SET WS_LAST_SEQUENCE_ID = dbo.Maximum_Bigint(WS_LAST_SEQUENCE_ID, @p1), " +
                                            "WS_LAST_TRANSACTION_ID = dbo.Maximum_Bigint(WS_LAST_TRANSACTION_ID , @p2), " +
                                            "WS_LAST_RCVD_MSG = dbo.Maximum_Datetime(WS_LAST_RCVD_MSG, GETDATE()) " +
                                      "WHERE WS_TERMINAL_ID = @p3 " +
                                        "AND WS_SESSION_ID = @p4 " + 
                                        "AND WS_STATUS = 0";
        }
        else
        {
          sql_str = "UPDATE GDS_SESSIONS SET WS_LAST_SEQUENCE_ID = dbo.Maximum_Bigint(WS_LAST_SEQUENCE_ID, @p1), " +
                                            "WS_LAST_RCVD_MSG = dbo.Maximum_Datetime(WS_LAST_RCVD_MSG, GETDATE()) " +
                                      "WHERE WS_TERMINAL_ID = @p3 " +
                                        "AND WS_SESSION_ID = @p4 " +
                                        "AND WS_STATUS = 0"; 
        }

        sql_command = new SqlCommand(sql_str);
        sql_command.Connection = Trx.Connection;
        sql_command.Transaction = Trx;

        sql_command.Parameters.Add("@p1", SqlDbType.BigInt, 8, "WTX_SEQUENCE_ID").Value = SequenceId;

        if (TransactionId > 0)
        {
          sql_command.Parameters.Add("@p2", SqlDbType.BigInt, 8, "WS_LAST_TRANSACTION_ID").Value = TransactionId;
        }

        sql_command.Parameters.Add("@p3", SqlDbType.Int, 4, "WTX_TERMINAL_ID").Value = TerminalId;
        sql_command.Parameters.Add("@p4", SqlDbType.BigInt, 8, "WTX_SESSION_ID").Value = SessionId;

        num_rows_updated = sql_command.ExecuteNonQuery();

        if (num_rows_updated != 1)
        {
          Log.Warning("Exception throw: updating GDS session.TerminalId: " + TerminalId.ToString() + " SessionId: " + SessionId.ToString());
          throw new GDS_Exception(GDS_ResponseCodes.GDS_RC_TERMINAL_SESSION_NOT_VALID, "SESSION Not Updated.");
        }
      }
      catch (GDS_Exception GDS_ex)
      {
        throw GDS_ex;
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
        Log.Warning("Exception throw: inserting GDS transaction.TerminalId: " + TerminalId.ToString() + " SessionId: " + SessionId.ToString() + " SeqId: " + SequenceId.ToString() + " TransactionId: " + TransactionId.ToString() + " RequestMsgId: " + RequestMsgId.ToString());
        throw new GDS_Exception(GDS_ResponseCodes.GDS_RC_ERROR, "TRANSACTION Not Inserted.");
      }

    } // DB_InsertGDSTransaction





    public static void DB_UpdateGDSSessionMessageReceived (Int32 TerminalId, Int64 SessionId)
    {
      SqlConnection sql_conn;
      SqlTransaction sql_trx;
      SqlCommand sql_command;
      String sql_str;
      Boolean error;

      sql_conn = null;
      sql_trx = null;
      error = true;

      try
      {
        sql_conn = WGDB.Connection ();
        sql_trx = sql_conn.BeginTransaction ();

        sql_str = "UPDATE GDS_SESSIONS SET   WS_LAST_RCVD_MSG = dbo.Maximum_Datetime(WS_LAST_RCVD_MSG, GETDATE()) " +
                                    "WHERE   WS_TERMINAL_ID   = @p1 " +
                                      "AND   WS_SESSION_ID    = @p2 " +
                                      "AND   WS_STATUS        = 0";

        sql_command = new SqlCommand (sql_str);
        sql_command.Connection = sql_trx.Connection;
        sql_command.Transaction = sql_trx;

        sql_command.Parameters.Add ("@p1", SqlDbType.Int, 4, "WS_TERMINAL_ID").Value = TerminalId;
        sql_command.Parameters.Add ("@p2", SqlDbType.BigInt, 8, "WS_SESSION_ID").Value = SessionId;

        sql_command.ExecuteNonQuery ();
        sql_trx.Commit ();

        error = false;
      }
      catch ( Exception ex )
      {
        Log.Exception (ex);
      }
      finally
      {
        if ( sql_trx != null )
        {
          if ( error && sql_trx.Connection != null )
          {
            sql_trx.Rollback ();
          }
          sql_trx.Dispose ();
          sql_trx = null;
        }

        // Close connection
        if ( sql_conn != null )
        {
          sql_conn.Close ();
          sql_conn = null;
        }
      }
    }

    /// <summary>
    /// Update trx, insert response
    /// </summary>
    /// <param name="TerminalId"></param>
    /// <param name="SequenceId"></param>
    /// <param name="RequestedByTerminal"></param>
    /// <param name="ResponseMsgXmlText"></param>
    /// <param name="Trx"></param>
    public static void DB_UpdateGDSTransaction(Int32 TerminalId,
                                               Int64 SequenceId,
                                               Boolean RequestedByTerminal,
                                               Int64 ResponseMsgId,
                                               SqlTransaction Trx)
    {
      SqlCommand sql_command;
      String sql_str;
      Int32 num_rows_updated;

      sql_str = "UPDATE GDS_TRANSACTIONS SET WTX_RESPONSE_MSG_ID = @p1, WTX_STATUS = 1" + // Finished
                                     " WHERE WTX_TERMINAL_ID = @p2 " +
                                        "AND WTX_SEQUENCE_ID = @p3 " +
                                        "AND WTX_REQUESTED_BY_TERMINAL = @p4 ";

      sql_command = new SqlCommand(sql_str);
      sql_command.Connection = Trx.Connection;
      sql_command.Transaction = Trx;

      sql_command.Parameters.Add("@p1", SqlDbType.BigInt, 8, "WTX_RESPONSE_MSG_ID").Value = ResponseMsgId;
      sql_command.Parameters.Add("@p2", SqlDbType.Int, 4, "WTX_TERMINAL_ID").Value = TerminalId;
      sql_command.Parameters.Add("@p3", SqlDbType.BigInt, 8, "WTX_SEQUENCE_ID").Value = SequenceId;
      sql_command.Parameters.Add("@p4", SqlDbType.Bit, 1, "WTX_REQUESTED_BY_TERMINAL").Value = RequestedByTerminal;

      try
      {
        num_rows_updated = sql_command.ExecuteNonQuery();
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
        Log.Warning("Exception throw: DB_UpdateGDSTransaction. TerminalId: " + TerminalId.ToString() + ", SequenceId: " + SequenceId.ToString());

        throw new GDS_Exception(GDS_ResponseCodes.GDS_RC_ERROR, "DB Error updating GDS transaction.");
      }

      if (num_rows_updated != 1)
      {
        Log.Warning("Exception throw: updating GDS transaction.TerminalId: " + TerminalId.ToString() + " SeqId: " + SequenceId.ToString());
        throw new GDS_Exception(GDS_ResponseCodes.GDS_RC_ERROR, "GDS_TRANSACTION Not Updated.");
      }

    } // DB_UpdateGDSTransaction

    /// <summary>
    /// Check if this messange is a retry.
    /// </summary>
    /// <param name="TerminalId"></param>
    /// <param name="SequenceId"></param>
    /// <param name="RequestedByTerminal"></param>
    /// <param name="MsgXmlText"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    public static Boolean DB_CheckMsgIsRetry(Int32 TerminalId,
                                             Int64 SequenceId,
                                             Boolean RequestedByTerminal,
                                             out String MsgXmlText,
                                             SqlTransaction Trx)
    {
      SqlCommand sql_command;
      SqlDataReader reader;
      String sql_str;

      MsgXmlText = null;

      sql_str = "SELECT WM_MESSAGE " +
                  "FROM GDS_MESSAGES, GDS_TRANSACTIONS " +
                 "WHERE WM_MESSAGE_ID = WTX_RESPONSE_MSG_ID " +
                   "AND WTX_TERMINAL_ID = @p1 " +
                   "AND WTX_SEQUENCE_ID = @p2 " +
                   "AND WTX_REQUESTED_BY_TERMINAL = @p3 ";

      sql_command = new SqlCommand(sql_str);
      sql_command.Connection = Trx.Connection;
      sql_command.Transaction = Trx;

      sql_command.Parameters.Add("@p1", SqlDbType.Int, 4, "WTX_TERMINAL_ID").Value = TerminalId;
      sql_command.Parameters.Add("@p2", SqlDbType.BigInt, 8, "WTX_SEQUENCE_ID").Value = SequenceId;
      sql_command.Parameters.Add("@p3", SqlDbType.Bit, 1, "WTX_REQUESTED_BY_TERMINAL").Value = RequestedByTerminal;

      reader = null;

      try
      {
        reader = sql_command.ExecuteReader();
        if (reader.Read())
        {
          if (reader.IsDBNull(0))
          {
            MsgXmlText = null;
          }
          else
          {

            MsgXmlText = (String)reader[0];
          }

          return true;
        }

        return false;
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
      finally
      {
        if (reader != null)
        {
          reader.Close();
          reader.Dispose();
          reader = null;
        }
      }

      return false;
    } // DB_CheckMsgIsRetry

    #endregion GDS_MessagesAndTrx

    #region PlaySessions

    public static Boolean DB_EnoughActivePlaySessions (Int64 PlaySessionId)
    {
      SqlConnection sql_con;
      SqlTransaction sql_trx;
      String sql_str;
      SqlCommand sql_cmd;
      Int32 sql_value;
      Int64 minimum_players;
      DateTime wait_started;
      TimeSpan wait_elapsed;

      sql_con = null;
      sql_trx = null;

      wait_started = DateTime.Now;

      try
      {
        sql_con = WGDB.Connection ();
        if ( sql_con == null )
        {
          Log.Error ("Connection is Null");

          return false;
        }

        sql_trx = sql_con.BeginTransaction ();

        sql_str = "";
        sql_str += "UPDATE   PLAY_SESSIONS ";
        sql_str += "   SET   PS_FINISHED        = GETDATE() ";
        sql_str += " WHERE   PS_PLAY_SESSION_ID = @p1 ";
        sql_str += "   AND   PS_STATUS          = 0 ";   // Opened

        sql_cmd = new SqlCommand (sql_str);
        sql_cmd.Connection = sql_con;
        sql_cmd.Transaction = sql_trx;
        sql_cmd.Parameters.Add ("@p1", SqlDbType.BigInt, 8, "PS_PLAY_SESSION_ID").Value = PlaySessionId;

        //Don't care about the return code.
        sql_cmd.ExecuteNonQuery (); 

        sql_trx.Commit ();

        sql_str = "";
        sql_str += "SELECT   CAST (GP_KEY_VALUE AS BIGINT) ";
        sql_str += "  FROM   GENERAL_PARAMS ";
        sql_str += " WHERE   GP_GROUP_KEY   = 'Play' ";
        sql_str += "   AND   GP_SUBJECT_KEY = 'MinimumPlayers' ";

        sql_cmd = new SqlCommand(sql_str);
        sql_cmd.Connection = sql_con;

        minimum_players = (Int64)sql_cmd.ExecuteScalar();
        if (minimum_players <= 1)
        {
          return true;
        }

        while ( true )
        {
          sql_str = "";
          sql_str += "SELECT   COUNT(*) ";
          sql_str += "  FROM ( SELECT   COUNT(*) NUM_ACTIVE_SESSIONS ";
          sql_str += "           FROM   PLAY_SESSIONS ";
          sql_str += "          WHERE   PS_STATUS = 0 ";
          // ACC on 08-JAN-2009 due to avoid the overflow in "DATEDIFF (MILLISECOND, PS_FINISHED, GETDATE())" sentence
          sql_str += "            AND   DATEDIFF (DAY, PS_FINISHED, GETDATE()) < 24 ";
          sql_str += "            AND   DATEDIFF (MILLISECOND, PS_FINISHED, GETDATE()) <= ";
          sql_str += "                  ( SELECT   CAST (GP_KEY_VALUE AS BIGINT) ";
          sql_str += "                      FROM   GENERAL_PARAMS ";
          sql_str += "                     WHERE   GP_GROUP_KEY   = 'Play' ";
          sql_str += "                       AND   GP_SUBJECT_KEY = 'ActivityTimeout' ";
          sql_str += "                  ) ";
          sql_str += "       ) ACTIVE_SESSIONS ";
          sql_str += " WHERE   NUM_ACTIVE_SESSIONS >= ";
          sql_str += "       ( SELECT   CAST (GP_KEY_VALUE AS BIGINT) ";
          sql_str += "           FROM   GENERAL_PARAMS ";
          sql_str += "          WHERE   GP_GROUP_KEY   = 'Play' ";
          sql_str += "            AND   GP_SUBJECT_KEY = 'MinimumPlayers' ) ";

          sql_cmd = new SqlCommand (sql_str);
          sql_cmd.Connection = sql_con;

          sql_value = (Int32) sql_cmd.ExecuteScalar ();
          if ( sql_value == 1 )
          {
            return true;
          }

          sql_str = "";
          sql_str += "SELECT   CAST (GP_KEY_VALUE AS INT) ";
          sql_str += "  FROM   GENERAL_PARAMS ";
          sql_str += " WHERE   GP_GROUP_KEY   = 'Play' ";
          sql_str += "   AND   GP_SUBJECT_KEY = 'WaitForMinimumPlayers' ";

          sql_cmd = new SqlCommand (sql_str);
          sql_cmd.Connection = sql_con;

          sql_value = (Int32) sql_cmd.ExecuteScalar ();
          if ( sql_value == 0 )
          {
            return false;
          }

          wait_elapsed = DateTime.Now.Subtract (wait_started);

          if ( wait_elapsed.TotalMilliseconds >= sql_value
              || wait_elapsed.TotalMilliseconds < 0 )
          {
            return false;
          }

          System.Threading.Thread.Sleep (250);
        }
      }
      catch ( Exception ex )
      {
        Log.Exception (ex);

        return false;
      }
      finally
      {
        if ( sql_trx != null )
        {
          if ( sql_trx.Connection != null )
          {
            sql_trx.Rollback ();
          }
        }
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Closes a Play Session
    //
    //  PARAMS:
    //      - INPUT:
    //          - PlaySessionId
    //          - AccountID
    //          - Trx
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    //      - Nothing
    //        
    //   NOTES :
    //
    public static void DB_PlaySessionClose(Int64 PlaySessionId,
                                           Int64 AccountID,
                                           SqlTransaction Trx)
    {
      SqlCommand sql_command;
      String sql_str;

      sql_str = "UPDATE   PLAY_SESSIONS SET PS_STATUS   = @pNewStatus " +
                                         ", PS_LOCKED   = NULL " +
                                         ", PS_FINISHED = GETDATE() " +
                " WHERE   PS_PLAY_SESSION_ID = @pPlaySessionID " +
                "   AND   PS_STATUS          = @pOldStatus ";
                
      sql_command = new SqlCommand(sql_str);
      sql_command.Connection = Trx.Connection;
      sql_command.Transaction = Trx;

      sql_command.Parameters.Add("@pNewStatus", SqlDbType.Int).Value = (Int32)GDS_PlaySessionStatus.Closed;
      sql_command.Parameters.Add("@pOldStatus", SqlDbType.Int).Value = (Int32)GDS_PlaySessionStatus.Opened;
      sql_command.Parameters.Add("@pPlaySessionID", SqlDbType.BigInt).Value = PlaySessionId;

      sql_command.ExecuteNonQuery();

      if (AccountID > 0)
      {
        sql_str = "UPDATE ACCOUNTS SET AC_LAST_TERMINAL_ID      = AC_CURRENT_TERMINAL_ID " +
                                 ", AC_LAST_TERMINAL_NAME       = AC_CURRENT_TERMINAL_NAME " +
                                 ", AC_LAST_PLAY_SESSION_ID     = AC_CURRENT_PLAY_SESSION_ID " +
                                 ", AC_CURRENT_TERMINAL_ID      = NULL " +
                                 ", AC_CURRENT_TERMINAL_NAME    = NULL " +
                                 ", AC_CURRENT_PLAY_SESSION_ID  = NULL " +
                  " WHERE AC_ACCOUNT_ID = @pAccountID " + 
                  "   AND AC_CURRENT_TERMINAL_ID IS NOT NULL ";

        sql_command = new SqlCommand(sql_str);
        sql_command.Connection  = Trx.Connection;
        sql_command.Transaction = Trx;
        sql_command.Parameters.Add("@pAccountID", SqlDbType.BigInt).Value = AccountID;
        sql_command.ExecuteNonQuery();
      }
    } // DB_PlaySessionClose




    #endregion PlaySessions

    #region Cards

    /// <summary>
    /// If the LockDateTime received is not null the cardsession will be locked
    /// </summary>
    /// <param name="CardSessionId"></param>
    /// <param name="LockDateTime"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    public static Boolean DB_LockPlaySession(Int64 PlaySessionID, SqlTransaction Trx)
    {      
      SqlCommand sql_command;
      int num_rows_updated;
      DateTime lock_date_time;

      lock_date_time = new DateTime();
      lock_date_time = DateTime.Now;

      String sql_str;
      //
      // Update cards
      //
      sql_str = "UPDATE PLAY_SESSIONS SET PS_LOCKED     = @p1 " +
                          " WHERE PS_PLAY_SESSION_ID    = @p2 " +
                          "   AND PS_TYPE               = @p3 ";

      sql_command = new SqlCommand(sql_str);
      sql_command.Connection = Trx.Connection;
      sql_command.Transaction = Trx;

      sql_command.Parameters.Add("@p1", SqlDbType.DateTime).Value = lock_date_time;
      sql_command.Parameters.Add("@p2", SqlDbType.BigInt).Value = PlaySessionID;
      sql_command.Parameters.Add("@p3", SqlDbType.Int).Value = (Int32) GDS_PlaySessionType.WIN;

      try
      {
        num_rows_updated = sql_command.ExecuteNonQuery();
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
        return false;
      }

      if (num_rows_updated != 1)
      {
        return false;
      }
      return true;

    } //DB_LockSession


    public static Boolean DB_UnlockPlaySession(Int64 CardSessionId, SqlTransaction Trx)
    {
      SqlCommand sql_command;
      int num_rows_updated;

      String sql_str;
      //
      // Update cards
      //
      sql_str = "UPDATE PLAY_SESSIONS SET PS_LOCKED     = NULL " +
                          " WHERE PS_PLAY_SESSION_ID    = @p2 ";

      sql_command = new SqlCommand(sql_str);
      sql_command.Connection = Trx.Connection;
      sql_command.Transaction = Trx;

      sql_command.Parameters.Add("@p2", SqlDbType.BigInt, 8, "PS_PLAY_SESSION_ID").Value = CardSessionId;

      try
      {
        num_rows_updated = sql_command.ExecuteNonQuery();
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
        return false;
      }

      if (num_rows_updated != 1)
      {
        return false;
      }
      return true;

    } //DB_UnlockSession



    /// <summary>
    /// Returns the timeout for a locked terminal
    /// </summary>
    /// <param name="LockTimeOut"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    public static Boolean Db_GetLockTimeOut(out int LockTimeOut, SqlTransaction Trx)
    {

      String sql_str;
      SqlCommand sql_command;

      LockTimeOut = 0;

      try
      {
        sql_str = "";
        sql_str += "SELECT   CAST (GP_KEY_VALUE AS INT) ";
        sql_str += "  FROM   GENERAL_PARAMS ";
        sql_str += " WHERE   GP_GROUP_KEY   = 'Play' ";
        sql_str += "   AND   GP_SUBJECT_KEY = 'LockTimeOut' ";

        sql_command = new SqlCommand(sql_str);
        sql_command.Connection = Trx.Connection;
        sql_command.Transaction = Trx;

        LockTimeOut = (Int32)sql_command.ExecuteScalar();
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
        return false;
      }
      return true;
    }


    /// <summary>
    /// Get Card Id 
    /// </summary>
    /// <param name="CardName"></param>
    /// <param name="TrackData"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    public static Int64 DB_GetCardId(String CardName,
                                     String TrackData,
                                     SqlTransaction Trx)
    {
      SqlCommand sql_command;
      SqlParameter p;
      String sql_str;
      Int32 num_rows_inserted;
      Int64 card_id;

      Boolean rc;
      String internal_card_id;
      int card_type;

      card_id = 0;

      // Convert incoming TrackData into the internal format fro DB access
      rc = WSI.Common.CardNumber.TrackDataToCardNumber(TrackData, out internal_card_id, out card_type);
      if (rc == false)
      {
        Log.Warning("Exception throw: Invalid Track Number: " + TrackData);
        throw new GDS_Exception(GDS_ResponseCodes.GDS_RC_CARD_NOT_VALID, "Invalid Track Number");
      }
      //
      // Search Card
      //
      sql_str = "SELECT AC_ACCOUNT_ID FROM ACCOUNTS WHERE AC_TRACK_DATA = @p1 AND AC_TYPE = 1"; // 1 - GDS_CardType.PLAYER_CJ

      sql_command = new SqlCommand(sql_str);
      sql_command.Connection = Trx.Connection;
      sql_command.Transaction = Trx;

      sql_command.Parameters.Add("@p1", SqlDbType.NVarChar, 50, "AC_TRACK_DATA").Value = internal_card_id;

      try
      {
        card_id = ((Int64)sql_command.ExecuteScalar());

      }
      catch
      {
        // Not Found. Continue.
      }

      if ( card_id > 0 )
      {
        return card_id;
      }

      //
      // Card Not Found. Insert card
      //

      sql_str = "INSERT INTO ACCOUNTS (AC_HOLDER_NAME " +
                                 ", AC_TRACK_DATA " +
                                 ", AC_TYPE " +
                                 ", AC_BLOCKED " +
                                 ", AC_BALANCE) " +
                "VALUES (@p1, @p2, @p3, 0, 0) " +
                 " SET @p4 = SCOPE_IDENTITY()";

      sql_command = new SqlCommand(sql_str);
      sql_command.Connection = Trx.Connection;
      sql_command.Transaction = Trx;

      sql_command.Parameters.Add("@p1", SqlDbType.NVarChar, 50, "AC_HOLDER_NAME").Value = CardName;
      sql_command.Parameters.Add("@p2", SqlDbType.NVarChar, 50, "AC_TRACK_DATA").Value = internal_card_id;
      sql_command.Parameters.Add("@p3", SqlDbType.Int, 4, "AC_TYPE").Value = (Int32)AccountType.ACCOUNT_CADILLAC_JACK;

      p = sql_command.Parameters.Add("@p4", SqlDbType.BigInt, 8, "AC_ACCOUNT_ID");
      p.Direction = ParameterDirection.Output;

      num_rows_inserted = 0;
      try
      {
        num_rows_inserted = sql_command.ExecuteNonQuery();
      }
      catch
      {
        // Track data already exists ! with type != 1 ??
      }

      if (num_rows_inserted != 1)
      {
        Log.Warning("Exception throw: inserting Card with Track Number: " + TrackData);
        throw new GDS_Exception(GDS_ResponseCodes.GDS_RC_ERROR, "CARD Not Inserted.");
      }

      return (Int64)p.Value;
    } // DB_GetCardId








    //------------------------------------------------------------------------------
    // PURPOSE: Sets the given balance to the account identified by the track data
    //
    //  PARAMS:
    //      - INPUT:
    //        - CardType: CJ | ALESIS
    //        - CardTrackData: Track data
    //        - Balance
    //
    //      - OUTPUT:
    //
    // RETURNS: The AccountID
    //        
    //   NOTES :
    //
    public static Int64 DB_SetAccountBalance (AccountType     CardType,
                                              String          CardTrackData,
                                              Decimal         Balance,
                                              SqlTransaction  Trx)
    {
      SqlCommand sql_command;
      SqlParameter p;
      String sql_str;
      Int32 num_rows_affected;
      Int64 account_id;
      String holder_name;

      account_id = 0;

      //
      // Search Card
      //
      sql_str = "SELECT AC_ACCOUNT_ID FROM ACCOUNTS WHERE AC_TRACK_DATA = @pTrackData AND AC_TYPE = @pCardType";

      sql_command = new SqlCommand(sql_str);
      sql_command.Connection  = Trx.Connection;
      sql_command.Transaction = Trx;
      sql_command.Parameters.Add("@pTrackData", SqlDbType.NVarChar, 50, "AC_TRACK_DATA").Value = CardTrackData;
      sql_command.Parameters.Add("@pCardType", SqlDbType.Int).Value = (int) CardType;

      try
      {
        account_id = ((Int64)sql_command.ExecuteScalar());
      }
      catch
      {
        // Not Found. Continue.
      }


      switch ( CardType )
      {
        case AccountType.ACCOUNT_ALESIS:
          holder_name = "ALESIS - " + CardTrackData;
        break;
        
        case AccountType.ACCOUNT_CADILLAC_JACK:
          holder_name = "CADILLAC JACK - " + CardTrackData;
        break;
        
        default:
          Log.Warning("Exception throw: CardType Not Supported, Track Number: " + CardTrackData);
          
          throw new GDS_Exception(GDS_ResponseCodes.GDS_RC_ERROR, "CardType Not Supported.");
        // break;
      }

      sql_str = "UPDATE   ACCOUNTS " +
                "   SET   AC_HOLDER_NAME     = @pHolderName" +
                "       , AC_BLOCKED         = 0" +
                "       , AC_BALANCE         = @pBalance" +
                "       , AC_CASH_IN         = @pBalance" +
                "       , AC_CASH_WON        = 0" +
                "       , AC_NOT_REDEEMABLE  = 0" +
                " WHERE   AC_TRACK_DATA      = @pTrackData" +
                "   AND   AC_TYPE            = @pCardType" +
                "   AND   AC_ACCOUNT_ID      = @pAccountID";

      sql_command = new SqlCommand(sql_str);
      sql_command.Connection = Trx.Connection;
      sql_command.Transaction = Trx;

      sql_command.Parameters.Add("@pHolderName", SqlDbType.NVarChar, 50, "AC_HOLDER_NAME").Value = holder_name;
      sql_command.Parameters.Add("@pTrackData", SqlDbType.NVarChar, 50, "AC_TRACK_DATA").Value = CardTrackData;
      sql_command.Parameters.Add("@pCardType", SqlDbType.Int).Value = (Int32)CardType;
      sql_command.Parameters.Add("@pBalance", SqlDbType.Money).Value = Balance;
      p = sql_command.Parameters.Add("@pAccountID", SqlDbType.BigInt);
      p.Value = account_id;

      if (account_id == 0)
      {
        sql_str = "INSERT INTO ACCOUNTS   (AC_HOLDER_NAME, AC_TRACK_DATA, AC_TYPE,    AC_BLOCKED, AC_BALANCE, AC_CASH_IN, AC_CASH_WON, AC_NOT_REDEEMABLE) " +
                  "              VALUES   (@pHolderName,   @pTrackData,   @pCardType, 0,          @pBalance,  @pBalance,  0,           0) " +
                  "                 SET    @pAccountID = SCOPE_IDENTITY()";

        // Account doesn't exist
        p.Direction = ParameterDirection.Output;
        
        sql_command.CommandText = sql_str;
      }


      num_rows_affected = 0;
      try
      {
        num_rows_affected = sql_command.ExecuteNonQuery();
      }
      catch (Exception ex)
      {
        // Track data already exists ! with type != 1 ??
        Log.Exception (ex);
      }

      if (num_rows_affected != 1)
      {
        Log.Warning("Exception throw: inserting Card with Track Number: " + CardTrackData);
        throw new GDS_Exception(GDS_ResponseCodes.GDS_RC_ERROR, "CARD Not Inserted.");
      }

      if ( account_id == 0 )
      {
        account_id = (Int64)p.Value;
      }

      return account_id;
      
    } // DB_SetAccountBalance

    /// <summary>
    /// Get Card Data
    /// </summary>
    /// <param name="CardName"></param>
    /// <param name="TrackData"></param>
    /// <param name="CardId"></param>
    /// <param name="Balance"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    public static void DB_UpdateAccountBalance(Int64 CardId,
                                               Int64 PlaySessionId,
                                               Int32 TerminalId,
                                               Decimal PlayedAmount,
                                               Decimal WonAmount,
                                               SqlTransaction Trx)
    {
      SqlCommand sql_command;
      SqlDataReader reader;
      String sql_str;
      Int32 num_rows_updated;
      Decimal balance;
      Decimal cash_in;
      Decimal cash_won;
      Decimal not_redeemable;
      Decimal amount_to_sub;
      Decimal aux_amount;

      //
      // Search Card
      //
      sql_str = "SELECT AC_BALANCE, " +
                       "AC_CASH_IN, " +
                       "AC_CASH_WON, " +
                       "AC_NOT_REDEEMABLE " +
                 " FROM ACCOUNTS " +
                " WHERE AC_ACCOUNT_ID = @p1 ";

      sql_command = new SqlCommand(sql_str);
      sql_command.Connection = Trx.Connection;
      sql_command.Transaction = Trx;

      sql_command.Parameters.Add("@p1", SqlDbType.BigInt, 8, "AC_ACCOUNT_ID").Value = CardId;

      reader = null;
      balance = 0;
      cash_in = 0;
      cash_won = 0;
      not_redeemable = 0;

      try
      {
        reader = sql_command.ExecuteReader();
        if (reader.Read() && !reader.IsDBNull(0))
        {
          balance = (Decimal)reader[0];
          cash_in = (Decimal)reader[1];
          cash_won = (Decimal)reader[2];
          not_redeemable = (Decimal)reader[3];
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
        Log.Warning("Exception throw: Error reading Cards. CardId: " + CardId.ToString());
        throw new GDS_Exception(GDS_ResponseCodes.GDS_RC_ERROR, "DB Error reading cards.");
      }
      finally
      {
        if (reader != null)
        {
          reader.Close();
          reader.Dispose();
          reader = null;
        }
      }

      if (balance != cash_in + cash_won + not_redeemable)
      {
        throw new GDS_Exception(GDS_ResponseCodes.GDS_RC_ERROR, "Balance mismatch in Database.");
      }

      amount_to_sub = PlayedAmount;

      if (amount_to_sub > balance)
      {
        throw new GDS_Exception(GDS_ResponseCodes.GDS_RC_ERROR, "Balance mismatch in Database.");
      }

      aux_amount = Math.Min(not_redeemable, amount_to_sub);
      amount_to_sub -= aux_amount;
      not_redeemable -= aux_amount;

      aux_amount = Math.Min(cash_won, amount_to_sub);
      amount_to_sub -= aux_amount;
      cash_won -= aux_amount;

      aux_amount = Math.Min(cash_in, amount_to_sub);
      amount_to_sub -= aux_amount;
      cash_in -= aux_amount;

      if (amount_to_sub != 0)
      {
        throw new GDS_Exception(GDS_ResponseCodes.GDS_RC_ERROR, "Balance mismatch in Database.");
      }

      balance -= PlayedAmount;
      balance += WonAmount;
      cash_won += WonAmount;

      //
      // Update cards
      //
      sql_str = "UPDATE ACCOUNTS SET AC_BALANCE     = @p1 " +
                               ", AC_CASH_IN        = @p2 " +
                               ", AC_CASH_WON       = @p3 " +
                               ", AC_NOT_REDEEMABLE = @p4 " +
                          " WHERE AC_ACCOUNT_ID = @p5 " +
                            " AND AC_CURRENT_TERMINAL_ID = @p6" +
                            " AND AC_CURRENT_PLAY_SESSION_ID = @p7"; 

      sql_command = new SqlCommand(sql_str);
      sql_command.Connection = Trx.Connection;
      sql_command.Transaction = Trx;

      sql_command.Parameters.Add("@p1", SqlDbType.Money, 8, "AC_BALANCE").Value = balance;
      sql_command.Parameters.Add("@p2", SqlDbType.Money, 8, "AC_CASH_IN").Value = cash_in;
      sql_command.Parameters.Add("@p3", SqlDbType.Money, 8, "AC_CASH_WON").Value = cash_won;
      sql_command.Parameters.Add("@p4", SqlDbType.Money, 8, "AC_NOT_REDEEMABLE").Value = not_redeemable;

      sql_command.Parameters.Add("@p5", SqlDbType.BigInt, 8, "AC_ACCOUNT_ID").Value = CardId;
      sql_command.Parameters.Add("@p6", SqlDbType.Int, 4, "AC_CURRENT_TERMINAL_ID").Value = TerminalId;
      sql_command.Parameters.Add("@p7", SqlDbType.BigInt, 8, "AC_CURRENT_PLAY_SESSION_ID").Value = PlaySessionId;

      num_rows_updated = sql_command.ExecuteNonQuery();

      if (num_rows_updated != 1)
      {
        DbCache.ReloadPlaySession(TerminalId);
        throw new GDS_Exception(GDS_ResponseCodes.GDS_RC_CARD_NOT_VALIDATED, "Card/Terminal/PlaySession Not valid.");
      }

    } // DB_UpdateAccountBalance

    #endregion Cards

    #region Plays

    /// <summary>
    /// Insert play in the database
    /// </summary>
    /// <param name="PlaySessionId"></param>
    /// <param name="CardId"></param>
    /// <param name="TerminalId"></param>
    /// <param name="GameId"></param>
    /// <param name="InitialBalance"></param>
    /// <param name="PlayedAmount"></param>
    /// <param name="WonAmount"></param>
    /// <param name="FinalBalance"></param>
    /// <param name="Trx"></param>
    public static void DB_InsertPlay(Int64 PlaySessionId,
                                     Int64 CardId,
                                     Int32 TerminalId,
                                     Int64 SequenceId,
                                     Int64 TransactionId,
                                     String GameId,
                                     Decimal InitialBalance,
                                     Decimal PlayedAmount,
                                     Decimal WonAmount,
                                     Decimal FinalBalance,
                                     SqlTransaction Trx)
      {
      SqlCommand sql_command;
      String sql_str;
      Int32 num_rows_inserted;
      Int32 game_id;

      game_id = 0;

      // Get game id from the game name.
      sql_str = "SELECT GM_GAME_ID FROM GAMES WHERE GM_NAME = @p1";

      sql_command = new SqlCommand(sql_str);
      sql_command.Connection = Trx.Connection;
      sql_command.Transaction = Trx;
      sql_command.Parameters.Add("@p1", SqlDbType.NVarChar, 50, "GM_NAME").Value = GameId;

      try
      {
        game_id = ((Int32)sql_command.ExecuteScalar());
      }
      catch
      {
        game_id = 0;

        Log.Error("DB_InsertPlay: Error getting game code. GameName: " + GameId);
      }

      sql_str = "INSERT INTO PLAYS (PL_PLAY_SESSION_ID " +
                                 ", PL_ACCOUNT_ID " +
                                 ", PL_TERMINAL_ID " +
                                 ", PL_GDS_SEQUENCE_ID" +
                                 ", PL_GDS_TRANSACTION_ID" +
                                 ", PL_GAME_ID " +
                                 ", PL_INITIAL_BALANCE " +
                                 ", PL_PLAYED_AMOUNT " +
                                 ", PL_WON_AMOUNT " +
                                 ", PL_FINAL_BALANCE " +
                                 ", PL_DATETIME) " +
                           "VALUES (@p1, @p2, @p3, @p4, @p5, @p6, @p7, @p8, @p9, @p10, GETDATE()) ";

      sql_command = new SqlCommand(sql_str);
      sql_command.Connection = Trx.Connection;
      sql_command.Transaction = Trx;

      sql_command.Parameters.Add("@p1", SqlDbType.BigInt, 8, "PL_PLAY_SESSION_ID").Value = PlaySessionId;
      if (CardId == 0)
      {
        sql_command.Parameters.Add("@p2", SqlDbType.BigInt, 8, "PL_ACCOUNT_ID").Value = DBNull.Value;
      }
      else
      {
        sql_command.Parameters.Add("@p2", SqlDbType.BigInt, 8, "PL_ACCOUNT_ID").Value = CardId;
      }
      sql_command.Parameters.Add("@p3", SqlDbType.Int, 4, "PL_TERMINAL_ID").Value = TerminalId;
      sql_command.Parameters.Add("@p4", SqlDbType.BigInt, 8, "PL_GDS_SEQUENCE_ID").Value = SequenceId;
      sql_command.Parameters.Add("@p5", SqlDbType.BigInt, 8, "PL_GDS_TRANSACTION_ID").Value = TransactionId;
      sql_command.Parameters.Add("@p6", SqlDbType.Int, 4, "PL_GAME_ID").Value = game_id;
      sql_command.Parameters.Add("@p7", SqlDbType.Money, 8, "PL_INITIAL_BALANCE").Value = InitialBalance;
      sql_command.Parameters.Add("@p8", SqlDbType.Money, 8, "PL_PLAYED_AMOUNT").Value = PlayedAmount;
      sql_command.Parameters.Add("@p9", SqlDbType.Money, 8, "PL_WON_AMOUNT").Value = WonAmount;
      sql_command.Parameters.Add("@p10", SqlDbType.Money, 8, "PL_FINAL_BALANCE").Value = FinalBalance;

      num_rows_inserted = sql_command.ExecuteNonQuery();

      if (num_rows_inserted != 1)
      {
        Log.Warning("Exception throw: inserting plays.PlaySessionId: " + PlaySessionId.ToString());
        throw new GDS_Exception(GDS_ResponseCodes.GDS_RC_ERROR, "PLAY Not Inserted.");
      }
    } // DB_InsertPlay

    #endregion Plays

    #region Movements

    public static void DB_InsertMovement(Int64 PlaySessionId,
                                         Int64 CardId,
                                         Int32 TerminalId,
                                         Int64 SequenceId,
                                         Int64 TransactionId,
                                         MovementType Type,
                                         Decimal InitialBalance,
                                         Decimal SubAmount,
                                         Decimal AddAmount,
                                         Decimal FinalBalance,
                                         SqlTransaction Trx)
      {
      SqlCommand sql_command;
      String sql_str;
      Int32 num_rows_inserted;
      String terminal_name;

      terminal_name = "";

      sql_str = " SELECT  TE_NAME " +
                "   FROM  TERMINALS " +
                "   WHERE TE_TERMINAL_ID = @p1 ";

      sql_command = new SqlCommand(sql_str);
      sql_command.Connection = Trx.Connection;
      sql_command.Transaction = Trx;

      sql_command.Parameters.Add("@p1", SqlDbType.Int, 4, "TE_TERMINAL_ID").Value = TerminalId;

      try
      {
        if (sql_command.ExecuteScalar() != null)
        {
          terminal_name = (String)sql_command.ExecuteScalar();
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }

      sql_str = "INSERT INTO ACCOUNT_MOVEMENTS (AM_PLAY_SESSION_ID " +
                                 ", AM_ACCOUNT_ID " +
                                 ", AM_TERMINAL_ID " +
                                 ", AM_GDS_SEQUENCE_ID " +
                                 ", AM_GDS_TRANSACTION_ID " +
                                 ", AM_TYPE " +
                                 ", AM_INITIAL_BALANCE " +
                                 ", AM_SUB_AMOUNT " +
                                 ", AM_ADD_AMOUNT " +
                                 ", AM_FINAL_BALANCE " +
                                 ", AM_DATETIME " +
                                 ", AM_TERMINAL_NAME) " +
                           "VALUES (@p1, @p2, @p3, @p4, @p5, @p6, @p7, @p8, @p9, @p10, GETDATE(), @p11) ";

      sql_command = new SqlCommand(sql_str);
      sql_command.Connection = Trx.Connection;
      sql_command.Transaction = Trx;

      sql_command.Parameters.Add("@p1", SqlDbType.BigInt, 8, "AM_PLAY_SESSION_ID").Value = PlaySessionId;
      if (CardId == 0)
      {
        sql_command.Parameters.Add("@p2", SqlDbType.BigInt, 8, "AM_ACCOUNT_ID").Value = DBNull.Value;
      }
      else
      {
        sql_command.Parameters.Add("@p2", SqlDbType.BigInt, 8, "AM_ACCOUNT_ID").Value = CardId;
      }
      sql_command.Parameters.Add("@p3", SqlDbType.Int, 4, "AM_TERMINAL_ID").Value = TerminalId;
      sql_command.Parameters.Add("@p4", SqlDbType.BigInt, 8, "AM_GDS_SEQUENCE_ID").Value = SequenceId;
      if (TransactionId == 0)
      {
        sql_command.Parameters.Add("@p5", SqlDbType.BigInt, 8, "AM_GDS_TRANSACTION_ID").Value = DBNull.Value;
      }
      else
      {
        sql_command.Parameters.Add("@p5", SqlDbType.BigInt, 8, "AM_GDS_TRANSACTION_ID").Value = TransactionId;
      }

      sql_command.Parameters.Add("@p6", SqlDbType.Int, 4, "AM_TYPE").Value = (Int32) Type;
      sql_command.Parameters.Add("@p7", SqlDbType.Money, 8, "AM_INITIAL_BALANCE").Value = InitialBalance;
      sql_command.Parameters.Add("@p8", SqlDbType.Money, 8, "AM_SUB_AMOUNT").Value = SubAmount;
      sql_command.Parameters.Add("@p9", SqlDbType.Money, 8, "AM_ADD_AMOUNT").Value = AddAmount;
      sql_command.Parameters.Add("@p10", SqlDbType.Money, 8, "AM_FINAL_BALANCE").Value = FinalBalance;

      if (terminal_name == "")
      {
        sql_command.Parameters.Add("@p11", SqlDbType.NVarChar, 50, "AM_TERMINAL_NAME").Value = DBNull.Value;
      }
      else
      {
        sql_command.Parameters.Add("@p11", SqlDbType.NVarChar, 50, "AM_TERMINAL_NAME").Value = terminal_name;
      }

      num_rows_inserted = sql_command.ExecuteNonQuery();

      if (num_rows_inserted != 1)
      {
        Log.Warning("Exception throw: inserting movements.Mov. PlaySessionId: " + PlaySessionId.ToString() + " TerminalId: " + TerminalId.ToString());
        throw new GDS_Exception(GDS_ResponseCodes.GDS_RC_ERROR, "MOVEMENT Not Inserted.");
      }
    } // DB_InsertMovement

    #endregion Movements

    //#region Events

    ///// <summary>
    ///// Insert event
    ///// </summary>
    ///// <param name="TerminalId"></param>
    ///// <param name="SessionId"></param>
    ///// <param name="EventType"></param>
    ///// <param name="EventData"></param>
    ///// <param name="EventDateTime"></param>
    ///// <param name="Trx"></param>
    //public static void DB_InsertEvent(Int32 TerminalId,
    //                                  Int64 SessionId,
    //                                  GDS_EventTypesCodes EventType,
    //                                  String EventData,
    //                                  DateTime EventDateTime,
    //                                  SqlTransaction Trx)
    //  {
    //  SqlCommand sql_command;
    //  String sql_str;
    //  Int32 num_rows_inserted;

    //  sql_str = "INSERT INTO EVENT_HISTORY (EH_TERMINAL_ID " +
    //                                     ", EH_SESSION_ID " +
    //                                     ", EH_DATETIME " +
    //                                     ", EH_EVENT_TYPE " +
    //                                     ", EH_EVENT_DATA) " +
    //                       "VALUES (@p1, @p2, @p3, @p4, @p5) ";

    //  sql_command = new SqlCommand(sql_str);
    //  sql_command.Connection = Trx.Connection;
    //  sql_command.Transaction = Trx;

    //  sql_command.Parameters.Add("@p1", SqlDbType.Int, 4, "EH_TERMINAL_ID").Value = TerminalId;
    //  sql_command.Parameters.Add("@p2", SqlDbType.BigInt, 8, "EH_SESSION_ID").Value = SessionId;
    //  sql_command.Parameters.Add("@p3", SqlDbType.DateTime, 8, "EH_DATETIME").Value = EventDateTime;
    //  sql_command.Parameters.Add("@p4", SqlDbType.Int, 4, "EH_EVENT_TYPE").Value = (Int32)EventType;
    //  sql_command.Parameters.Add("@p5", SqlDbType.NVarChar, 50, "EH_EVENT_DATA").Value = EventData;

    //  num_rows_inserted = sql_command.ExecuteNonQuery();

    //  if (num_rows_inserted != 1)
    //  {
    //    Log.Warning("Exception throw: inserting events.TerminalId: " + TerminalId.ToString() + " SessionId: " + SessionId.ToString());
    //    throw new GDS_Exception(GDS_ResponseCodes.GDS_RC_ERROR, "EVENT Not Inserted.");
    //  }
    //} // DB_InsertEvent


    ////------------------------------------------------------------------------------
    //// PURPOSE: Insert notified events into database
    ////
    ////  PARAMS:
    ////      - INPUT:
    ////          - TerminalId
    ////          - SessionId
    ////          - EventType
    ////          - DeviceStatus
    ////          - Operation
    ////          - Trx
    ////
    ////      - OUTPUT:
    ////
    //// RETURNS: 
    ////      - StatusCode
    ////        
    ////   NOTES :
    ////
    //public static void DB_NewInsertEvent(Int32 TerminalId,
    //                                     Int64 SessionId,
    //                                     GDS_EventTypesCodes EventType,
    //                                     GDS_DeviceStatus DeviceStatus,
    //                                     GDS_Operation Operation,
    //                                     SqlTransaction Trx)
    //{
    //  SqlCommand sql_command;
    //  String sql_str;
    //  Int32 num_rows_inserted;
    //  Decimal operation_data;

    //  sql_str = "INSERT INTO EVENT_HISTORY (EH_TERMINAL_ID " +
    //                                     ", EH_SESSION_ID " +
    //                                     ", EH_DATETIME " +
    //                                     ", EH_EVENT_TYPE " +
    //                                     ", EH_DEVICE_CODE " +
    //                                     ", EH_DEVICE_STATUS " +
    //                                     ", EH_DEVICE_PRIORITY " +
    //                                     ", EH_OPERATION_CODE " +
    //                                     ", EH_OPERATION_DATA) " +
    //                       "VALUES (@p1, @p2, @p3, @p4, @p5, @p6, @p7, @p8, @p9) ";

    //  sql_command = new SqlCommand(sql_str);
    //  sql_command.Connection = Trx.Connection;
    //  sql_command.Transaction = Trx;

    //  sql_command.Parameters.Add("@p1", SqlDbType.Int, 4, "EH_TERMINAL_ID").Value = TerminalId;
    //  sql_command.Parameters.Add("@p2", SqlDbType.BigInt, 8, "EH_SESSION_ID").Value = SessionId;
    //  sql_command.Parameters.Add("@p4", SqlDbType.Int, 4, "EH_EVENT_TYPE").Value = (Int32)EventType;

    //  if (EventType == GDS_EventTypesCodes.GDS_EV_DEVICE_STATUS)
    //  {
    //    sql_command.Parameters.Add("@p3", SqlDbType.DateTime, 8, "EH_DATETIME").Value = DeviceStatus.LocalTime;
    //    sql_command.Parameters.Add("@p5", SqlDbType.Int, 4, "EH_DEVICE_CODE").Value = DeviceStatus.Code;
    //    sql_command.Parameters.Add("@p6", SqlDbType.Int,   4, "EH_DEVICE_STATUS").Value = DeviceStatus.Status;
    //    sql_command.Parameters.Add("@p7", SqlDbType.Int,   4, "EH_DEVICE_PRIORITY").Value = DeviceStatus.Priority;
    //    sql_command.Parameters.Add("@p8", SqlDbType.Int,   4, "EH_OPERATION_CODE").Value = DBNull.Value;
    //    sql_command.Parameters.Add("@p9", SqlDbType.Money, 8, "EH_OPERATION_DATA").Value = DBNull.Value;
    //  }
    //  else if (EventType == GDS_EventTypesCodes.GDS_EV_OPERATION)
    //  {
    //    operation_data = (Decimal) Operation.Data;
    //    if (   Operation.Code == OperationCodes.GDS_OPERATION_CODE_JACKPOT_WON
    //        || Operation.Code == OperationCodes.GDS_OPERATION_CODE_KIOSK_BLOCKED_HIGH_PRIZE)
    //    {
    //      operation_data = ((Decimal)Operation.Data) / 100;
    //    }

    //    sql_command.Parameters.Add("@p3", SqlDbType.DateTime, 8, "EH_DATETIME").Value = Operation.LocalTime;
    //    sql_command.Parameters.Add("@p5", SqlDbType.Int, 4, "EH_DEVICE_CODE").Value = DBNull.Value;
    //    sql_command.Parameters.Add("@p6", SqlDbType.Int, 4, "EH_DEVICE_STATUS").Value = DBNull.Value;
    //    sql_command.Parameters.Add("@p7", SqlDbType.Int, 4, "EH_DEVICE_PRIORITY").Value = DBNull.Value;
    //    sql_command.Parameters.Add("@p8", SqlDbType.Int, 4, "EH_OPERATION_CODE").Value = Operation.Code;
    //    sql_command.Parameters.Add("@p9", SqlDbType.Money, 8, "EH_OPERATION_DATA").Value = operation_data;
    //  }
    //  else
    //  {
    //    Log.Warning("Exception throw: Unexpected EVENT Type. TerminalId: " + TerminalId.ToString() + " SessionId: " + SessionId.ToString());
    //    throw new GDS_Exception(GDS_ResponseCodes.GDS_RC_ERROR, "Unexpected EVENT Type.");
    //  }

    //  num_rows_inserted = sql_command.ExecuteNonQuery();

    //  if (num_rows_inserted != 1)
    //  {
    //    Log.Warning("Exception throw: inserting events.TerminalId: " + TerminalId.ToString() + " SessionId: " + SessionId.ToString());
    //    throw new GDS_Exception(GDS_ResponseCodes.GDS_RC_ERROR, "EVENT Not Inserted.");
    //  }
    //} // DB_InsertEvent


    //#endregion Events

    //#region Meters

    ///// <summary>
    ///// Update meters
    ///// </summary>
    ///// <param name="TerminalId"></param>
    ///// <param name="GDSMeters"></param>
    ///// <param name="Trx"></param>
    //public static void DB_UpdateMeters(Int32 TerminalId,
    //                                   GDS_MsgReportMeters  GDSMeters,
    //                                   SqlTransaction Trx)
    //{
    //  SqlCommand sql_command;
    //  String sql_str;
    //  Int32 num_rows_inserted;
    //  Int32 num_rows_updated;
    //  GDS_MoneyMeter GDS_money_meter_item;
    //  Int64 meter_id;
    //  SqlParameter p;

    //  //
    //  // Get Meter Id
    //  //
    //  sql_str = "SELECT ME_METER_ID FROM METERS WHERE ME_TERMINAL_ID = @p1";

    //  sql_command = new SqlCommand(sql_str);
    //  sql_command.Connection = Trx.Connection;
    //  sql_command.Transaction = Trx;

    //  sql_command.Parameters.Add("@p1", SqlDbType.Int, 4, "ME_TERMINAL_ID").Value = TerminalId;

    //  try
    //  {
    //    meter_id = ((Int64)sql_command.ExecuteScalar());
    //  }
    //  catch
    //  {
    //    meter_id = 0;
    //  }

    //  if (meter_id == 0)
    //  {
    //    //
    //    // Insert Meter for this terminal
    //    //
    //    sql_str = "INSERT INTO METERS (ME_TERMINAL_ID   , ME_PLAYED_COUNT " +
    //                                ", ME_PLAYED_AMOUNT , ME_WON_COUNT " +
    //                                ", ME_WON_AMOUNT    , ME_CASH_IN " +
    //                                ", ME_CASH_OUT      , ME_LAST_REPORTED) " +
    //                          "VALUES (@p1, @p2, @p3, @p4, @p5, @p6, @p7, GETDATE()) " +
    //                            " SET  @p8 = SCOPE_IDENTITY()";

    //    sql_command = new SqlCommand(sql_str);
    //    sql_command.Connection = Trx.Connection;
    //    sql_command.Transaction = Trx;

    //    sql_command.Parameters.Add("@p1", SqlDbType.Int, 4, "ME_TERMINAL_ID").Value = TerminalId;
    //    sql_command.Parameters.Add("@p2", SqlDbType.Int, 4, "ME_PLAYED_COUNT").Value = GDSMeters.PlayedCount;
    //    sql_command.Parameters.Add("@p3", SqlDbType.Money, 8, "ME_PLAYED_AMOUNT").Value = ((decimal) GDSMeters.PlayedAmount) / 100;
    //    sql_command.Parameters.Add("@p4", SqlDbType.Int, 4, "ME_WON_COUNT").Value = GDSMeters.WonCount;
    //    sql_command.Parameters.Add("@p5", SqlDbType.Money, 8, "ME_WON_AMOUNT").Value = ((decimal) GDSMeters.WonAmount) / 100;
    //    sql_command.Parameters.Add("@p6", SqlDbType.Money, 8, "ME_CASH_IN").Value = ((decimal) GDSMeters.CashInAmount) / 100;
    //    sql_command.Parameters.Add("@p7", SqlDbType.Money, 8, "ME_CASH_OUT").Value = ((decimal) GDSMeters.CashOutAmount) / 100;

    //    p = sql_command.Parameters.Add("@p8", SqlDbType.BigInt, 8, "ME_METER_ID");
    //    p.Direction = ParameterDirection.Output;

    //    num_rows_inserted = sql_command.ExecuteNonQuery();

    //    if (num_rows_inserted != 1)
    //    {
    //      Log.Warning("Exception throw: inserting meter.TerminalId: " + TerminalId.ToString());
    //      throw new GDS_Exception(GDS_ResponseCodes.GDS_RC_ERROR, "METER Not Inserted.");
    //    }

    //    meter_id = (Int64)p.Value;
    //  }
    //  else
    //  {
    //    //
    //    // Update Meter for this terminal
    //    //
    //    sql_str = "UPDATE METERS SET ME_PLAYED_COUNT = @p1  , ME_PLAYED_AMOUNT = @p2" +
    //                              ", ME_WON_COUNT = @p3     , ME_WON_AMOUNT = @p4 " +
    //                              ", ME_CASH_IN = @p5       , ME_CASH_OUT = @p6 " +
    //                              ", ME_LAST_REPORTED = GETDATE() " +
    //                         " WHERE ME_METER_ID = @p7 ";

    //    sql_command = new SqlCommand(sql_str);
    //    sql_command.Connection = Trx.Connection;
    //    sql_command.Transaction = Trx;

    //    sql_command.Parameters.Add("@p1", SqlDbType.Int, 4, "ME_PLAYED_COUNT").Value = GDSMeters.PlayedCount;
    //    sql_command.Parameters.Add("@p2", SqlDbType.Money, 8, "ME_PLAYED_AMOUNT").Value = GDSMeters.PlayedAmount;
    //    sql_command.Parameters.Add("@p3", SqlDbType.Int, 4, "ME_WON_COUNT").Value = GDSMeters.WonCount;
    //    sql_command.Parameters.Add("@p4", SqlDbType.Money, 8, "ME_WON_AMOUNT").Value = GDSMeters.WonAmount;
    //    sql_command.Parameters.Add("@p5", SqlDbType.Money, 8, "ME_CASH_IN").Value = GDSMeters.CashInAmount;
    //    sql_command.Parameters.Add("@p6", SqlDbType.Money, 8, "ME_CASH_OUT").Value = GDSMeters.CashOutAmount;
    //    sql_command.Parameters.Add("@p7", SqlDbType.BigInt, 8, "ME_METER_ID").Value = meter_id;

    //    num_rows_updated = sql_command.ExecuteNonQuery();

    //    if (num_rows_updated != 1)
    //    {
    //      Log.Warning("Exception throw: updating meter.MeterId: " + meter_id.ToString());
    //      throw new GDS_Exception(GDS_ResponseCodes.GDS_RC_ERROR, "METER Not Updated.");
    //    }
    //  }

    //  for (int idx_money_meter = 0; idx_money_meter < GDSMeters.MoneyMeters.Count; idx_money_meter++)
    //  {
    //    GDS_money_meter_item = (GDS_MoneyMeter)GDSMeters.MoneyMeters[idx_money_meter];

    //    //
    //    // Update Money Meter for this terminal
    //    //
    //    sql_str = "UPDATE MONEY_METERS SET MM_COUNT = @p1 " +
    //                                    ", MM_AMOUNT  = @p2" +
    //                                    ", MM_LAST_REPORTED = GETDATE() " +
    //                         " WHERE MM_METER_ID = @p3 " + 
    //                         "   AND MM_CASH_TYPE = @p4 " +
    //                         "   AND MM_MONEY_TYPE = @p5 " +
    //                         "   AND MM_FACE_VALUE = @p6 ";

    //    sql_command = new SqlCommand(sql_str);
    //    sql_command.Connection = Trx.Connection;
    //    sql_command.Transaction = Trx;

    //    sql_command.Parameters.Add("@p1", SqlDbType.Int, 4, "MM_COUNT").Value = GDS_money_meter_item.Count;
    //    sql_command.Parameters.Add("@p2", SqlDbType.Money, 8, "MM_AMOUNT").Value = GDS_money_meter_item.Amount;
    //    sql_command.Parameters.Add("@p3", SqlDbType.BigInt, 8, "MM_METER_ID").Value = meter_id;
    //    sql_command.Parameters.Add("@p4", SqlDbType.Int, 4, "MM_CASH_TYPE").Value = GDS_money_meter_item.CashType;
    //    sql_command.Parameters.Add("@p5", SqlDbType.Int, 4, "MM_MONEY_TYPE").Value = GDS_money_meter_item.MoneyType;
    //    sql_command.Parameters.Add("@p6", SqlDbType.Money, 8, "MM_FACE_VALUE").Value = GDS_money_meter_item.FaceValue;

    //    num_rows_updated = sql_command.ExecuteNonQuery();

    //    if (num_rows_updated != 1)
    //    {
    //      //
    //      // Insert Money Meter for this terminal
    //      //
    //      sql_str = "INSERT INTO MONEY_METERS (MM_METER_ID      , MM_CASH_TYPE " +
    //                                        ", MM_MONEY_TYPE    , MM_FACE_VALUE " +
    //                                        ", MM_COUNT         , MM_AMOUNT " +
    //                                        ", MM_LAST_REPORTED ) " +
    //                            "VALUES (@p1, @p2, @p3, @p4, @p5, @p6, GETDATE()) ";

    //      sql_command = new SqlCommand(sql_str);
    //      sql_command.Connection = Trx.Connection;
    //      sql_command.Transaction = Trx;

    //      sql_command.Parameters.Add("@p1", SqlDbType.BigInt, 8, "MM_METER_ID").Value = meter_id;
    //      sql_command.Parameters.Add("@p2", SqlDbType.Int, 4, "MM_CASH_TYPE").Value = GDS_money_meter_item.CashType;
    //      sql_command.Parameters.Add("@p3", SqlDbType.Int, 4, "MM_MONEY_TYPE").Value = GDS_money_meter_item.MoneyType;
    //      sql_command.Parameters.Add("@p4", SqlDbType.Money, 8, "MM_FACE_VALUE").Value = GDS_money_meter_item.FaceValue;
    //      sql_command.Parameters.Add("@p5", SqlDbType.Int, 4, "MM_COUNT").Value = GDS_money_meter_item.Count;
    //      sql_command.Parameters.Add("@p6", SqlDbType.Money, 8, "MM_AMOUNT").Value = GDS_money_meter_item.Amount;

    //      num_rows_inserted = sql_command.ExecuteNonQuery();

    //      if (num_rows_inserted != 1)
    //      {
    //        Log.Warning("Exception throw: inserting money meter.MeterId: " + meter_id.ToString());
    //        throw new GDS_Exception(GDS_ResponseCodes.GDS_RC_ERROR, "METER Not Inserted.");
    //      }
    //    }
    //  }

    //} // DB_UpdateMeters

    //#endregion Meters

  } // class

  public class Terminal
  {
    public Int32    terminal_id;
    public String   name;
    public String   serial_number;
    public Boolean  blocked;
    
    public String   card_track_data;
    public Int64    account_id;
    public GDS_PlaySessionType play_session_type;
    public Int64    play_session_id;
    public Int32    total_played_count;
    public Decimal  total_played_amount;
    public Int32    total_won_count;
    public Decimal  total_won_amount;
    public Decimal  balance;

    protected virtual Boolean Read (int TerminalID, SqlTransaction SqlTrx)
    {
      SqlCommand sql_cmd;
      SqlDataReader reader;
      String sql_str;

      terminal_id = TerminalID;
      name = "";
      serial_number = "";
      blocked = true;
      card_track_data = "";
      account_id = 0;
      play_session_id = 0;
      play_session_type = GDS_PlaySessionType.NONE;
      total_played_count = 0;
      total_played_amount = 0;
      total_won_count = 0;
      total_won_amount = 0;
      balance = 0;

      sql_str = "SELECT   TE_NAME            "
              + "       , TE_EXTERNAL_ID     "
              + "       , TE_BLOCKED         "
              + "       , AC_TRACK_DATA      "
              + "       , AC_ACCOUNT_ID      "
              + "       , PS_PLAY_SESSION_ID "
              + "       , PS_PLAYED_COUNT    "
              + "       , PS_PLAYED_AMOUNT   "
              + "       , PS_WON_COUNT       "
              + "       , PS_WON_AMOUNT      "
              + "       , AC_BALANCE         "
              + "       , PS_TYPE            "
              + "  FROM   TERMINALS_VIEW     "
              + " WHERE   TE_TERMINAL_ID = @pTerminalID";

      sql_cmd = new SqlCommand(sql_str);
      sql_cmd.Connection = SqlTrx.Connection;
      sql_cmd.Transaction = SqlTrx;
      sql_cmd.Parameters.Add("@pTerminalID", SqlDbType.Int).Value = TerminalID;

      reader = null;

      try
      {
        reader = sql_cmd.ExecuteReader();
        if (reader.Read())
        {
          if (!reader.IsDBNull(0)) { name = reader.GetString(0); }
          
          serial_number  = reader.GetString(1);
          blocked        = reader.GetBoolean(2);

          if (!reader.IsDBNull(3))  { card_track_data     = reader.GetString(3); }
          if (!reader.IsDBNull(4))  { account_id          = reader.GetInt64(4); }
          if (!reader.IsDBNull(5))  { play_session_id     = reader.GetInt64(5); }
          if (!reader.IsDBNull(6))  { total_played_count  = reader.GetInt32(6); }
          if (!reader.IsDBNull(7))  { total_played_amount = reader.GetDecimal(7); }
          if (!reader.IsDBNull(8))  { total_won_count     = reader.GetInt32(8); }
          if (!reader.IsDBNull(9))  { total_won_amount    = reader.GetDecimal(9); }
          if (!reader.IsDBNull(10)) { balance             = reader.GetDecimal(10); }
          
          play_session_type = GDS_PlaySessionType.NONE;
          if (!reader.IsDBNull(11)) 
          {
            int _ps_type_value;
                        
            _ps_type_value = reader.GetInt32(11);
            
            if (_ps_type_value == (int) GDS_PlaySessionType.WIN)
            {
              play_session_type = GDS_PlaySessionType.WIN;
            }
            if (_ps_type_value == (int)GDS_PlaySessionType.ALESIS)
            {
              play_session_type = GDS_PlaySessionType.ALESIS;
            }
            if (_ps_type_value == (int)GDS_PlaySessionType.CADILLAC_JACK)
            {
              play_session_type = GDS_PlaySessionType.CADILLAC_JACK;
            }
          }
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);

        Log.Warning("Exception throw: Error reading Terminal information" + TerminalID.ToString());
      }
      finally
      {
        if (reader != null)
        {
          reader.Close();
          reader.Dispose();
          reader = null;
        }
      }
      
      return true;
    }

    public static Terminal GetTerminal (int TerminalId, SqlTransaction SqlTrx)
    {
      //if (GDS_BusinessLogic.CashlessMode == CashlessMode.WIN)
      //{
        Terminal t;

        t = new Terminal();
        t.Read(TerminalId, SqlTrx);

        return t;
      //}

      //if  ( GDS_BusinessLogic.CashlessMode == CashlessMode.ALESIS
      //    ||GDS_BusinessLogic.CashlessMode == CashlessMode.WIN_AND_ALESIS )
      //{
      //  AleTerminal t;

      //  t = new AleTerminal();
      //  t.Read(TerminalId, SqlTrx);
      
      //  return t;
      //}

      //return null;
    }
    
  }

} // namespace
