//------------------------------------------------------------------------------
// Copyright � 2008 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: GDS_PendingnrollsQueue.cs
// 
//   DESCRIPTION: GDS_PendingEnrollsQueue class
// 
//        AUTHOR: Alberto Cuesta
// 
// CREATION DATE: 10-NOV-2008
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 10-NOV-2008 ACC    First release.
//------------------------------------------------------------------------------

using System;
using System.Messaging;
using System.Collections.Generic;
using System.Text;
using System.Net;
using WSI.Common;
using System.Xml;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using WSI.GDS;
using System.Collections;

namespace WSI.GDS
{
  /// <summary>
  /// Represents a enroll that is waiting to be processed 
  /// </summary>
  public class EnrollQueueElement
  {
    private Object m_GDS_listener;
    private Object m_GDS_client;
    private GDS_Message m_xml_message_enroll;

    // Constructor
    public EnrollQueueElement(Object GDSListener, Object GDSClient, GDS_Message XmlMessageEnroll)
    {
      m_GDS_listener = GDSListener;
      m_GDS_client = GDSClient;
      m_xml_message_enroll = XmlMessageEnroll;
    }

    //
    // Properties
    //

    public Object GDSListener
    {
      get
      {
        return m_GDS_listener;
      }
      set
      {
        m_GDS_listener = value;
      }
    }

    public Object GDSClient
    {
      get
      {
        return m_GDS_client;
      }
      set
      {
        m_GDS_client = value;
      }
    }

    public GDS_Message XmlMessageEnroll
    {
      get
      {
        return m_xml_message_enroll;
      }
      set
      {
        m_xml_message_enroll = value;
      }
    }
  }

  public class EnrollProcessTask : Task
  {
    Object m_GDS_listener;
    Object m_GDS_client;
    GDS_Message m_xml_message_enroll;

    public EnrollProcessTask(Object GDSListener, Object GDSClient, GDS_Message XmlMessageEnroll)
    {
      m_GDS_listener = GDSListener;
      m_GDS_client = GDSClient;
      m_xml_message_enroll = XmlMessageEnroll;
    }

    public override void Execute()
    {
      GDS_PendingEnrollsQueue.EnrollWorker(m_GDS_listener, m_GDS_client, m_xml_message_enroll);
    }
  }

  /// <summary>
  /// Holds enroll data that awaits to be processed
  /// </summary>
  public class GDS_PendingEnrollsQueue
  {
    private Worker GDS_enroll_worker;
    private WaitableQueue GDS_enroll_waitable_queue;

    private static GDS_PendingEnrollsQueue GDS_pending_enrolls;

    //------------------------------------------------------------------------------
    // PURPOSE : Initialize an instance of the GDS_PendingEnrollsQueue class 
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public static void Init()
    {
      GDS_pending_enrolls = new GDS_PendingEnrollsQueue();
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Returns the Current instance of the GDS_PendingEnrollsQueue Class
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - instance of the GDS_PendingEnrollsQueue
    //
    //   NOTES :
    //
    public static GDS_PendingEnrollsQueue GetInstance()
    {
      return GDS_pending_enrolls;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Start workers for this tasks
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void StartWorker()
    {
      // Create queue enrolls
      GDS_enroll_waitable_queue = new WaitableQueue();

      // - Message received processing worker
      // Start workers for GDS requests
      //
      // ONLY 1 WORKER for enroll trx !!!
      //
      GDS_enroll_worker = new Worker(GDS_enroll_waitable_queue);
      GDS_enroll_worker.Start();
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Public function for start workers
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public static void Start()
    {
      // Workers start
      GDS_pending_enrolls.StartWorker();
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Enqueues a enroll element
    //
    //  PARAMS :
    //      - INPUT :
    //         - EnrollElement
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public void EnqueueEnroll(EnrollQueueElement EnrollElement)
    {
      EnrollProcessTask enroll_process_task;

      // Enqueue DRAW
      enroll_process_task = new EnrollProcessTask(EnrollElement.GDSListener, EnrollElement.GDSClient, EnrollElement.XmlMessageEnroll);
      GDS_enroll_waitable_queue.Enqueue(enroll_process_task);
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Enroll worker
    //
    //  PARAMS :
    //      - INPUT :
    //         - GDSListener
    //         - GDSClient
    //         - XmlMessageEnroll
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public static void EnrollWorker(Object GDSListener, Object GDSClient, GDS_Message XmlMessageEnroll)
    {
      GDS_Message GDS_request;
      GDS_Message GDS_response;
      SecureTcpClient client;
      SqlConnection sql_conn;
      SqlTransaction sql_trx;
      String str_response;
      Int64 GDS_message_id;
      Boolean error;
      Int64 response_session_id;
      Int32 response_terminal_id;

      GDS_request = XmlMessageEnroll;

      client = (SecureTcpClient)GDSClient;

      sql_conn = null;
      sql_trx = null;
      error = true;
      GDS_response = null;

      try
      {
        GDS_response = GDS_Message.CreateMessage(GDS_request.MsgHeader.MsgType + 1);
        GDS_response.MsgHeader.PrepareReply(GDS_request.MsgHeader);

        if (GDS_request.MsgHeader.TerminalId.Length > 0)
        {
          response_session_id = GDS_request.MsgHeader.TerminalSessionId;
        }
        else
        {
          response_session_id = GDS_request.MsgHeader.ServerSessionId;
        }

        response_terminal_id = client.InternalId;

        // Get Sql Connection 
        sql_conn = WGDB.Connection();

        // Begin Transaction
        sql_trx = sql_conn.BeginTransaction();

        switch (GDS_request.MsgHeader.MsgType)
        {
          case GDS_MsgTypes.GDS_MsgEnrollServer:
          {
            EnrollServer(client, GDS_request, GDS_response, sql_trx);
            response_terminal_id = client.InternalId;
            response_session_id = client.SessionId;
          }
          break;

          case GDS_MsgTypes.GDS_MsgEnrollTerminal:
          {
            EnrollTerminal(client, GDS_request, GDS_response, sql_trx);
            response_terminal_id = client.InternalId;
            response_session_id = client.SessionId;
          }
          break;

          case GDS_MsgTypes.GDS_MsgUnenrollServer:
          {
            UnenrollServer(client, GDS_request, GDS_response, sql_trx);
          }
          break;

          case GDS_MsgTypes.GDS_MsgUnenrollTerminal:
          {
            UnenrollTerminal(client, GDS_request, GDS_response, sql_trx);
          }
          break;

          default:
            throw new GDS_Exception(GDS_ResponseCodes.GDS_RC_ERROR, "Unexpected MsgType");
          // Unreachable code detected
          // break;
        }

        str_response = GDS_response.ToXml();

        // Insert GDS message and Update GDS trx
        GDS_BusinessLogic.DB_InsertGDSMessage(response_terminal_id, response_session_id, true, str_response, GDS_response.MsgHeader.SequenceId, out GDS_message_id, sql_trx);

        // Insert GDS Trx
        if (GDS_request.MsgHeader.MsgType != GDS_MsgTypes.GDS_MsgEnrollServer
            && GDS_request.MsgHeader.MsgType != GDS_MsgTypes.GDS_MsgEnrollTerminal)
        {
          // Update GDS trx
          GDS_BusinessLogic.DB_UpdateGDSTransaction(response_terminal_id, GDS_request.MsgHeader.SequenceId, true, GDS_message_id, sql_trx);
        }

        // End Transaction
        sql_trx.Commit();

        // Call to reload play session data.
        DbCache.ReloadGDSSession(0);
        DbCache.ReloadPlaySession(0);

        // Send Enroll response
        client.Send(str_response);
        NetLog.Add(NetEvent.MessageSent, client.Identity, str_response);

        error = false;
      }
      catch (GDS_Exception GDS_ex)
      {
        String GDS_error_response;
        GDS_error_response = null;
        try
        {
          if (GDS_response == null)
          {
            GDS_response = GDS_Message.CreateMessage(GDS_MsgTypes.GDS_MsgError);
          }
          GDS_response.MsgHeader.ResponseCode = GDS_ex.ResponseCode;
          GDS_response.MsgHeader.ResponseCodeText = GDS_ex.ResponseCodeText;

          // Create error response
          GDS_error_response = GDS_response.ToXml();

          client.Send(GDS_error_response);
          NetLog.Add(NetEvent.MessageSent, client.Identity, GDS_error_response);

        }
        catch (Exception ex)
        {
          Log.Exception(ex);
        }
      }
      catch (Exception ex)
      {
        NetLog.Add(NetEvent.MessageFormatError, client.Identity + " a.1)", ex.Message);
        NetLog.Add(NetEvent.MessageFormatError, client.Identity + " b.1)", XmlMessageEnroll.ToXml());

        Log.Exception(ex);

        return;
      }
      finally
      {
        if (sql_trx != null)
        {
          if (error && sql_trx.Connection != null)
          {
            sql_trx.Rollback();
          }

          sql_trx.Dispose();
          sql_trx = null;
        }

        // Close connection
        if (sql_conn != null)
        {
          sql_conn.Close();
          sql_conn = null;
        }

      } // try - finally

    } // EnrollWorker

    #region Private Functions

    //------------------------------------------------------------------------------
    // PURPOSE : Enroll Server
    //
    //  PARAMS :
    //      - INPUT :
    //         - GDSClient
    //         - GDSRequestMessage
    //         - GDSResponseMessage
    //         - SqlTrx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private static void EnrollServer(SecureTcpClient GDSClient, GDS_Message GDSRequestMessage, GDS_Message GDSResponseMessage, SqlTransaction SqlTrx)
    {
      Int32 terminal_id;
      Int64 session_id;
      String provider_id;
      Int64 dummy1;
      Boolean dummy2;

      GDS_MsgEnrollServer request_enroll;
      GDS_MsgEnrollServerReply response_enroll;

      request_enroll = (GDS_MsgEnrollServer)GDSRequestMessage.MsgContent;
      response_enroll = (GDS_MsgEnrollServerReply)GDSResponseMessage.MsgContent;

      GDSClient.Identity = GDSRequestMessage.MsgHeader.TerminalId;

      if (!GDS_BusinessLogic.DB_GetEnrollData(GDSClient.Identity,
                                                GDS_TerminalType.IntermediateServer,
                                                out terminal_id,
                                                out session_id,
                                                out response_enroll.LastSequenceId,
                                                out dummy1,
                                                out dummy2,
                                                out provider_id,
                                                SqlTrx))
      {
        throw new GDS_Exception(GDS_ResponseCodes.GDS_RC_SERVER_NOT_AUTHORIZED, "Server not authorized. Not found or blocked");
      }

      if ((request_enroll.ProviderId != "") && (provider_id != request_enroll.ProviderId))
      {
        if (!GDS_BusinessLogic.DB_UpdateTerminalProvider(request_enroll.ProviderId, terminal_id, SqlTrx))
        {
          throw new GDS_Exception(GDS_ResponseCodes.GDS_RC_ERROR_UPDATING_PROVIDER, "Error updating Terminal provider");
        }
        else
        {
          provider_id = request_enroll.ProviderId;
        }
      }

      GDSClient.InternalId = terminal_id;
      GDSClient.SessionId = session_id;
      GDSResponseMessage.MsgHeader.ServerSessionId = GDSClient.SessionId;

    } // EnrollServer

    //------------------------------------------------------------------------------
    // PURPOSE : Enroll Terminal
    //
    //  PARAMS :
    //      - INPUT :
    //         - GDSClient
    //         - GDSRequestMessage
    //         - GDSResponseMessage
    //         - SqlTrx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private static void EnrollTerminal(SecureTcpClient GDSClient, GDS_Message GDSRequestMessage, GDS_Message GDSResponseMessage, SqlTransaction SqlTrx)
    {
      Int32 terminal_id;
      Int64 session_id;
      Boolean enrolled_terminal;
      String provider_id;

      GDS_MsgEnrollTerminal request_enroll;
      GDS_MsgEnrollTerminalReply response_enroll;

      request_enroll = (GDS_MsgEnrollTerminal)GDSRequestMessage.MsgContent;
      response_enroll = (GDS_MsgEnrollTerminalReply)GDSResponseMessage.MsgContent;

      GDSClient.Identity = GDSRequestMessage.MsgHeader.TerminalId;

      if (!GDS_BusinessLogic.DB_GetEnrollData(GDSClient.Identity,
                                                GDS_TerminalType.GamingTerminal,
                                                out terminal_id,
                                                out session_id,
                                                out response_enroll.LastSequenceId,
                                                out response_enroll.LastTransactionId,
                                                out enrolled_terminal,
                                                out provider_id,
                                                SqlTrx))
      {
        throw new GDS_Exception(GDS_ResponseCodes.GDS_RC_TERMINAL_NOT_AUTHORIZED, "Terminal not authorized. Not found or blocked");
      }

      if ((request_enroll.ProviderId != "") && (provider_id != request_enroll.ProviderId))
      {
        if (!GDS_BusinessLogic.DB_UpdateTerminalProvider(request_enroll.ProviderId, terminal_id, SqlTrx))
        {
          throw new GDS_Exception(GDS_ResponseCodes.GDS_RC_ERROR_UPDATING_PROVIDER, "Error updating Terminal provider");
        }
        else
        {
          provider_id = request_enroll.ProviderId;
        }
      }

      if (enrolled_terminal)
      {
        GDSClient.InternalId = terminal_id;
        GDSClient.SessionId = session_id;
        GDSResponseMessage.MsgHeader.TerminalSessionId = GDSClient.SessionId;
      }
      else
      {
        // Commit update for abandoned sessions.
        SqlTrx.Commit();

        throw new GDS_Exception(GDS_ResponseCodes.GDS_RC_TERMINAL_NOT_AUTHORIZED, "Terminal not authorized. Max licensed terminals reached.");
      }

    } // EnrollTerminal

    //------------------------------------------------------------------------------
    // PURPOSE : Unenroll Server
    //
    //  PARAMS :
    //      - INPUT :
    //         - GDSClient
    //         - GDSRequestMessage
    //         - GDSResponseMessage
    //         - SqlTrx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private static void UnenrollServer(SecureTcpClient GDSClient, GDS_Message GDSRequestMessage, GDS_Message GDSResponseMessage, SqlTransaction SqlTrx)
    {

      GDS_BusinessLogic.DB_SessionFinished(GDSClient.InternalId, GDSRequestMessage.MsgHeader.ServerSessionId, GDS_SessionStatus.Closed, SqlTrx);
      GDSClient.InternalId = 0;
      GDSClient.SessionId = 0;

    } // UnenrollServer

    //------------------------------------------------------------------------------
    // PURPOSE : Unenroll Terminal
    //
    //  PARAMS :
    //      - INPUT :
    //         - GDSClient
    //         - GDSRequestMessage
    //         - GDSResponseMessage
    //         - SqlTrx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private static void UnenrollTerminal(SecureTcpClient GDSClient, GDS_Message GDSRequestMessage, GDS_Message GDSResponseMessage, SqlTransaction SqlTrx)
    {

      GDS_BusinessLogic.DB_SessionFinished(GDSClient.InternalId, GDSRequestMessage.MsgHeader.TerminalSessionId, GDS_SessionStatus.Closed, SqlTrx);
      GDSClient.InternalId = 0;
      GDSClient.SessionId = 0;

    } // UnenrollTerminal

    #endregion Private Functions

  } // classs GDS_PendingEnrollsQueue

} // namespace WSI.GDS
