using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using WSI.Common;
using System.Data;
using System.Data.SqlClient;

 

namespace WSI.GDS
{
  public enum GDS_Recurrence
  { 
    None        = 0,
    Daily       = 1,
    Weekly      = 2,
    WorkingDays = 4,
    Weekends    = 8,
    Manual      = 16,
  }

  public class GDS_Scheduler
  {
    private Thread  m_thread_sched;
    private Thread  m_thread_worker;
    private Boolean m_stop;


    public void Init ()
    {
      m_stop = false;
      m_thread_sched = new Thread (new ThreadStart (GdsSchedulerThread));
      m_thread_worker = new Thread (new ThreadStart (GdsWorkerThread));
    }

    public void Start ()
    {
      m_thread_sched.Start ();
      m_thread_worker.Start ();
    }


    /// <summary>
    /// Computes the Next Run
    /// </summary>
    /// <param name="Now"></param>
    /// <param name="Executed"></param>
    /// <param name="LastDate"></param>
    /// <param name="Recurrence"></param>
    /// <returns></returns>
    private DateTime ComputeNextRun (DateTime Now, DateTime Executed, DateTime LastDate, GDS_Recurrence Recurrence)
    {
      DateTime next_run;

      next_run = Executed;

      do
      {
        switch ( Recurrence )
        {
          default:
          case GDS_Recurrence.None:
          {
            next_run = DateTime.MinValue;
          }
          break;

          case GDS_Recurrence.Daily:
          {
            next_run = next_run.AddDays (1);
          }
          break;

          case GDS_Recurrence.Weekly:
          {
            next_run = next_run.AddDays (7);
          }
          break;

          case GDS_Recurrence.WorkingDays:
          {
            do
            {
              next_run = next_run.AddDays (1);

            } while ( next_run.DayOfWeek == DayOfWeek.Saturday || next_run.DayOfWeek == DayOfWeek.Sunday );
          }
          break;

          case GDS_Recurrence.Weekends:
          {
            do
            {
              next_run = next_run.AddDays (1);

            } while ( next_run.DayOfWeek != DayOfWeek.Saturday && next_run.DayOfWeek != DayOfWeek.Sunday );
          }
          break;
        }

        if ( next_run > LastDate )
        {
          next_run = DateTime.MinValue;
        }

      } while ( next_run != DateTime.MinValue && next_run < Now );

      return next_run;
    }


    private void GdsSchedulerThread ()
    {
      String str_sql;
      SqlConnection sql_con;


      ////str_sql = "";
      ////str_sql += "UPDATE   GDS_SCHEDULE_TIME          ";
      ////str_sql += "   SET   GST_LOCK_ID         = NULL ";
      ////str_sql += "       , GST_LOCK_DATETIME   = NULL ";
      ////str_sql += " WHERE   GST_LOCK_SERVICE_ID = @p1  ";
      ////str_sql += "      OR DATEDIFF(MINUTE, GST_LOCK_DATETIME, GETDATE()) > 1";


      sql_con = new SqlConnection ();

      while ( !m_stop )
      {
        System.Threading.Thread.Sleep (1000);

        if ( sql_con.State != ConnectionState.Open )
        {
          sql_con.Close ();
          sql_con = null;
          sql_con = WGDB.Connection ();
          if ( sql_con == null )
          {
            sql_con = new SqlConnection ();
          }
          if ( sql_con.State != ConnectionState.Open )
          {
            continue;
          }
        }

        SqlTransaction sql_trx;

        sql_trx = sql_con.BeginTransaction ();
        try
        {
          str_sql = "";
          str_sql += "SELECT   TOP 1 * ";
          str_sql += "  FROM   GDS_SCHEDULE_TIME  ";
          str_sql += " WHERE   GST_ENABLED    = 1 ";
          str_sql += "   AND   GST_NEXT_RUN  <= GETDATE ()  ";
          str_sql += "ORDER BY GST_NEXT_RUN  ASC  ";
          str_sql += "       , GST_ORDER     ASC  ";

          SqlCommand sql_cmd;
          SqlDataReader sql_reader;

          sql_cmd = new SqlCommand (str_sql, sql_trx.Connection, sql_trx);

          sql_reader = sql_cmd.ExecuteReader (CommandBehavior.SingleRow);
          if ( sql_reader.Read () )
          {
            Int64 sched_id;
            Int32 int_recurrence;
            GDS_Recurrence gds_recurrence;
            DateTime next_run;
            DateTime last_run;
            DateTime sched_end;

            sched_id = (Int64) sql_reader["GST_SCHEDULE_ID"];
            next_run = (DateTime) sql_reader["GST_NEXT_RUN"]; 
            int_recurrence = (Int32) sql_reader["GST_RECURRENCE"];
            if ( sql_reader.IsDBNull (sql_reader.GetOrdinal ("GST_LAST_RUN")) )
            {
              last_run = DateTime.MinValue;
            }
            else
            {
              last_run = (DateTime) sql_reader["GST_LAST_RUN"];
            }

            if ( sql_reader.IsDBNull (sql_reader.GetOrdinal ("GST_END")) )
            {
              sched_end = DateTime.MaxValue;
            }
            else
            {
              sched_end = (DateTime) sql_reader["GST_END"];
            }

            sql_reader.Close ();

            switch ( int_recurrence )
            {
              default:
              case (Int32) GDS_Recurrence.None:
                gds_recurrence = GDS_Recurrence.None;
              break;

              case (Int32) GDS_Recurrence.Manual:
                gds_recurrence = GDS_Recurrence.None;
              break;

              case (Int32) GDS_Recurrence.Daily:
                gds_recurrence = GDS_Recurrence.Daily;
              break;

              case (Int32) GDS_Recurrence.Weekly:
                gds_recurrence = GDS_Recurrence.Weekly;
              break;

              case (Int32) GDS_Recurrence.WorkingDays:
                gds_recurrence = GDS_Recurrence.WorkingDays;
              break;

              case (Int32) GDS_Recurrence.Weekends:
                gds_recurrence = GDS_Recurrence.Weekends;
              break;
            } // switch ( int_recurrence ) 

            // Compute Next Run
            TimeSpan elapsed;

            elapsed = WGDB.Now.Subtract (next_run);
            if ( elapsed.TotalMinutes <= 15 )
            {
              str_sql = "";
              str_sql += "SELECT   * ";
              str_sql += "  FROM   GDS_SCHEDULE_STEPS ";
              str_sql += " WHERE   GSS_SCHEDULE_ID = @p1 ";
              str_sql += "ORDER BY GSS_STEP_INDEX  ASC  ";

              sql_cmd = new SqlCommand (str_sql, sql_trx.Connection, sql_trx);
              sql_cmd.Parameters.Add ("@p1", SqlDbType.BigInt, 8, "GSS_SCHEDULE_ID").Value = sched_id;

              sql_reader = sql_cmd.ExecuteReader ();
              try
              {
                while ( sql_reader.Read () )
                {
                  Int32 target_type;
                  Int64 target_id;
                  Int32 action_type;
                  Int64 action_data;
                  Int32 step_index;

                  GDS_Group target_group;

                  target_type = (Int32) sql_reader["GSS_TARGET_TYPE"];
                  target_id = (Int64) sql_reader["GSS_TARGET_ID"];
                  action_type = (Int32) sql_reader["GSS_ACTION_TYPE"];
                  action_data = (Int64) sql_reader["GSS_ACTION_DATA"];
                  step_index = (Int32) sql_reader["GSS_STEP_INDEX"];

                  if ( target_type == 1 )
                  {
                    target_group = GDS_Group.LoadGroup(target_id);
                  }
                  else
                  {
                    target_group = new GDS_Group(0);
                    target_group.Add (target_id);
                  }

                  // !!!!!
                  sql_reader.Close ();
                  {
                    String str_ins;
                    SqlCommand sql_cmd_insert;
                    String str_txt;


                    str_txt = "";
                    if ( action_type == 0 )
                    {
                      str_txt = "Games: " + action_data.ToString ("000");
                    }
                    else
                    {
                      GDS_Group games;
                      games = GDS_Group.LoadGroup(action_data);
                      foreach ( Int64 game_id in games.Elements )
                      {
                        if ( str_txt.Length > 0 )
                        {
                          str_txt += ", " + game_id.ToString ("000");
                        }
                        else
                        {
                          str_txt = "Games: " + game_id.ToString ("000");
                        }
                      }
                    }


                    str_ins = "";
                    str_ins += "INSERT INTO   GDS_SCHEDULE_DETAILS ";
                    str_ins += "            ( GSD_SCHEDULE_ID   ";
                    str_ins += "            , GSD_STEP_INDEX    ";
                    str_ins += "            , GSD_DATETIME      ";
                    str_ins += "            , GSD_TERMINAL_ID   ";
                    str_ins += "            , GSD_ACTION_TYPE   ";
                    str_ins += "            , GSD_ACTION_DATA   ";
                    str_ins += "            , GSD_TXT           ";
                    str_ins += "            , GSD_RECURRENCE    ";
                    str_ins += "            ) ";
                    str_ins += "            VALUES ";
                    str_ins += "            ( @p1, @p2, @p3, @p4, @p5, @p6, @p7, @p8 ) ";

                    sql_cmd_insert = new SqlCommand (str_ins, sql_trx.Connection, sql_trx);
                    sql_cmd_insert.Parameters.Add ("@p1", SqlDbType.BigInt, 8, "GSD_SCHEDULE_ID").Value = sched_id;
                    sql_cmd_insert.Parameters.Add ("@p2", SqlDbType.Int, 4, "GSD_STEP_INDEX").Value = step_index;
                    sql_cmd_insert.Parameters.Add ("@p3", SqlDbType.DateTime, 8, "GSD_DATETIME").Value = next_run;
                    sql_cmd_insert.Parameters.Add ("@p4", SqlDbType.Int, 4, "GSD_TERMINAL_ID").Value = (Int32) 0;
                    sql_cmd_insert.Parameters.Add ("@p5", SqlDbType.Int, 4, "GSD_ACTION_TYPE").Value = action_type;
                    sql_cmd_insert.Parameters.Add ("@p6", SqlDbType.BigInt, 8, "GSD_ACTION_DATA").Value = action_data;
                    sql_cmd_insert.Parameters.Add ("@p7", SqlDbType.NVarChar, Int32.MaxValue, "GSD_TXT").Value = str_txt;
                    sql_cmd_insert.Parameters.Add ("@p8", SqlDbType.Int, 4, "GSD_RECURRENCE").Value = int_recurrence;


                    foreach ( Int64 terminal_id in target_group.Elements )
                    {
                      sql_cmd_insert.Parameters["@p4"].Value = (Int32) terminal_id;

                      sql_cmd_insert.ExecuteNonQuery ();
                    }
                  }
                }
              }
              catch ( Exception ex )
              {
                Log.Exception (ex);
              }
              finally
              {
                sql_reader.Close ();
              }



              // Explote details
              last_run = WGDB.Now;
            }

            next_run = ComputeNextRun (WGDB.Now, next_run, sched_end, gds_recurrence);

            // Update Next Run
            str_sql = "";
            str_sql += "UPDATE   GDS_SCHEDULE_TIME          ";
            str_sql += "   SET   GST_NEXT_RUN        = @p1  ";
            str_sql += "       , GST_LAST_RUN        = @p2  ";
            str_sql += " WHERE   GST_SCHEDULE_ID     = @p3  ";

            sql_cmd = new SqlCommand (str_sql, sql_trx.Connection, sql_trx);
            if ( next_run == DateTime.MinValue )
            {
              sql_cmd.Parameters.Add ("@p1", SqlDbType.DateTime, 8, "GST_NEXT_RUN").Value = DBNull.Value;
            }
            else
            {
              sql_cmd.Parameters.Add ("@p1", SqlDbType.DateTime, 8, "GST_NEXT_RUN").Value = next_run;
            }
            if ( last_run == DateTime.MinValue )
            {
              sql_cmd.Parameters.Add ("@p2", SqlDbType.DateTime, 8, "GST_LAST_RUN").Value = DBNull.Value;
            }
            else
            {
              sql_cmd.Parameters.Add ("@p2", SqlDbType.DateTime, 8, "GST_LAST_RUN").Value = last_run;
            }

            sql_cmd.Parameters.Add ("@p3", SqlDbType.BigInt, 8, "GST_SCHEDULE_ID").Value = sched_id;

            sql_cmd.ExecuteNonQuery ();

            sql_trx.Commit ();
          }

          sql_reader.Close ();

        }
        catch ( Exception ex )
        {
          Log.Exception (ex);
        }
        finally
        {
          if ( sql_trx != null )
          {
            if ( sql_trx.Connection != null )
            {
              sql_trx.Rollback ();
            }
            
            sql_trx = null;
          }
        }



      









      }
    }

    private void GdsWorkerThread ()
    {
      String str_sql;
      SqlConnection sql_con;
      SqlCommand sql_cmd_count;
      SqlCommand sql_cmd_terminal;
      SqlCommand sql_cmd_details;
      SqlCommand sql_cmd_update;
      SqlTransaction sql_trx;
      Int32 count;
      Int32 terminal_id;
      Int32 wait_hint;

      sql_con = new SqlConnection ();

      str_sql = "";
      str_sql += "SELECT   COUNT(*) ";
      str_sql += "  FROM   GDS_SCHEDULE_DETAILS ";
      str_sql += " WHERE   GSD_DATETIME <= GETDATE () ";
      str_sql += "   AND   GSD_ACTION_STATUS = 0 ";

      sql_cmd_count = new SqlCommand (str_sql);

      str_sql = "";
      str_sql += "SELECT   MIN(GSD_TERMINAL_ID) ";
      str_sql += "  FROM   GDS_SCHEDULE_DETAILS ";
      str_sql += " WHERE   GSD_DATETIME <= GETDATE () ";
      str_sql += "   AND   GSD_ACTION_STATUS = 0 ";

      sql_cmd_terminal = new SqlCommand (str_sql);

      str_sql = "";
      str_sql += "SELECT   TOP 1 * ";
      str_sql += "  FROM   GDS_SCHEDULE_DETAILS ";
      str_sql += " WHERE   GSD_DATETIME <= GETDATE () ";
      str_sql += "   AND   GSD_ACTION_STATUS = 0 ";
      str_sql += "   AND   GSD_TERMINAL_ID   = @p1 ";
      str_sql += "ORDER BY GSD_DATETIME   ASC ";
      str_sql += "       , GSD_ORDER      ASC ";
      str_sql += "       , GSD_STEP_INDEX ASC ";

      sql_cmd_details = new SqlCommand (str_sql);
      sql_cmd_details.Parameters.Add ("@p1", SqlDbType.Int, 4, "GSD_TERMINAL_ID").Value = (Int32) 0;

      str_sql = "";
      str_sql += "UPDATE   GDS_SCHEDULE_DETAILS    ";
      str_sql += "   SET   GSD_ACTION_STATUS = 1   ";
      str_sql += " WHERE   GSD_ID            = @p1 ";

      sql_cmd_update = new SqlCommand (str_sql);
      sql_cmd_update.Parameters.Add ("@p1", SqlDbType.BigInt, 8, "GSD_ID").Value = (Int64) 0;

      sql_trx = null;

      wait_hint = 1000;

      while ( !m_stop )
      {
        System.Threading.Thread.Sleep (wait_hint);
        wait_hint = 1000;

        try
        {
          if ( sql_con.State != ConnectionState.Open )
          {
            sql_con.Close ();
            sql_con = null;
            sql_con = WGDB.Connection ();
            if ( sql_con == null )
            {
              sql_con = new SqlConnection ();
            }
            if ( sql_con.State != ConnectionState.Open )
            {
              continue;
            }
          }

          sql_trx = sql_con.BeginTransaction ();

          sql_cmd_count.Connection = sql_trx.Connection;
          sql_cmd_count.Transaction = sql_trx;

          count = (Int32) sql_cmd_count.ExecuteScalar ();

          sql_trx.Commit ();

          if ( count <= 0 )
          {
            continue;
          }

          terminal_id = 0;

          sql_trx = sql_con.BeginTransaction ();

          sql_cmd_terminal.Connection = sql_trx.Connection;
          sql_cmd_terminal.Transaction = sql_trx;

          terminal_id = (Int32) sql_cmd_terminal.ExecuteScalar ();

          sql_trx.Commit ();

          if ( terminal_id == 0 )
          {
            continue;
          }

          sql_trx = sql_con.BeginTransaction ();

          sql_cmd_details.Connection = sql_trx.Connection;
          sql_cmd_details.Transaction = sql_trx;
          sql_cmd_details.Parameters["@p1"].Value = (Int32) terminal_id;

          SqlDataReader sql_details;
          sql_details = sql_cmd_details.ExecuteReader (CommandBehavior.SingleRow);
          try
          {
            if ( sql_details.Read () )
            {
              Int64 activation_id;
              Int32 action_type;
              Int64 action_data;
              GDS_Group games;

              activation_id = (Int64) sql_details["GSD_ID"];
              action_type = (Int32) sql_details["GSD_ACTION_TYPE"];
              action_data = (Int64) sql_details["GSD_ACTION_DATA"];

              sql_details.Close ();

              switch ( action_type )
              {
                case 0: // Game
                {
                  games = new GDS_Group(0);
                  games.Add (action_data);
                }
                break;

                case 1: // Menu
                {
                  games = GDS_Group.LoadGroup(action_data);
                }
                break;

                default:
                {
                  // Empty Group
                  games = new GDS_Group(0);
                }
                break;
              } // ( action_type )


              {
                sql_cmd_update.Connection = sql_trx.Connection;
                sql_cmd_update.Transaction = sql_trx;

                sql_cmd_update.Parameters["@p1"].Value = (Int64) activation_id;
                if ( sql_cmd_update.ExecuteNonQuery () == 1 )
                {
                  SecureTcpClient remote;

                  remote = null;

                  foreach (SecureTcpClient client in GDS.Server.Clients)
                  {
                    if (client.InternalId == terminal_id)
                    {
                      remote = client;

                      break;
                    } 
                  }

                  if (remote != null)
                  {
                    GDS_Message gds_request;
                    //GDS_MsgActivateGameList gds_agl;
                    //GDS_MsgActivateGameList.GDS_Game gds_game;
                    //int idx_game;

                    gds_request = GDS_Message.CreateMessage(GDS_MsgTypes.GDS_MsgNotifyCurrentParameters);
                    //wcp_agl = (GDS_MsgActivateGameList)wcp_request.MsgContent;

                    //wcp_agl.ActivationId = (ulong)activation_id;
                    //idx_game = 0;
                    //foreach (Int64 game_id in games.Elements)
                    //{
                    //  gds_game = new GDS_MsgActivateGameList.GDS_Game();
                    //  gds_game.id = (int)game_id;
                    //  gds_game.name = "GAME " + game_id.ToString();
                    //  gds_game.version = "VERSION " + game_id.ToString();
                    //  wcp_agl.SetGame(idx_game, gds_game);
                    //  idx_game++;
                    //}

                    remote.Send(gds_request.ToXml());
                  }
                }

                sql_trx.Commit ();

                wait_hint = 0;
              }

            }
          }
          finally
          {
            sql_details.Close ();
            if ( sql_trx != null )
            {
              if ( sql_trx.Connection != null )
              {
                sql_trx.Rollback ();
              }
            }
          }
        }
        catch ( Exception ex )
        {
          Log.Exception (ex);

          continue;
        }
        finally
        {
          if ( sql_trx != null )
          {
            if ( sql_trx.Connection != null )
            {
              sql_trx.Rollback ();
            }
          }
        }









        str_sql  = "";
        str_sql += "SELECT   * ";
        str_sql += "  FROM   GDS_SCHEDULE_DETAILS ";
        str_sql += " WHERE   GSD_DATETIME <= GETDATE ()  ";
        str_sql += "   AND   GSD_ACTION_STATUS = 0       ";
        str_sql += "ORDER BY GSD_TERMINAL_ID ASC         ";
        str_sql += "       , GSD_DATETIME    ASC         ";
        str_sql += "       , GSD_STEP_INDEX  ASC         ";
        str_sql += "       , GSD_SCHEDULE_ID ASC         ";




      
      
      }
    }



  }
}
