using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Xml;
using System.Data.SqlClient;
using System.Data;



namespace WSI.GDS
{
  public static class Protocol
  {
    public const int PORT_NUMBER = 65533;          // Listener port number
    public const String SERVICE_NAME = "GDS_Service";  // Service Name
  }
  
  public static class GDS_Process
  {
    public static void InsertMessage (SqlTransaction SqlTrx, 
                                      Int32 TerminalId, 
                                      Int64 SequenceId, 
                                      Boolean TowardsToTerminal, 
                                      XmlDocument Message)
    {
      SqlCommand cmd;
      SqlParameter p1;
      SqlParameter p2;
      SqlParameter p3;
      SqlParameter p4;

      cmd = new SqlCommand("INSERT INTO GDS_MESSAGES (wm_terminal_id, wm_sequence_id, wm_towards_to_terminal, wm GDS_message) VALUES (@p1, @p2, @p3, @p4)");

      p1 = cmd.Parameters.Add ("@p1", SqlDbType.Int);
      p1.Direction = ParameterDirection.Input;
      p2 = cmd.Parameters.Add ("@p2", SqlDbType.BigInt);
      p2.Direction = ParameterDirection.Input;
      p3 = cmd.Parameters.Add ("@p3", SqlDbType.Bit);
      p3.Direction = ParameterDirection.Input;
      p4 = cmd.Parameters.Add ("@p4", SqlDbType.Xml);
      p4.Direction = ParameterDirection.Input;

      if ( TerminalId != 0 )
      {
        p1.Value = TerminalId;
      }
      p2.Value = SequenceId;
      p3.Value = TowardsToTerminal;
      p4.Value = Message;

      cmd.Connection = SqlTrx.Connection;
      cmd.Transaction = SqlTrx;
      if ( cmd.ExecuteNonQuery () != 1 )
      { 
        // TODO: Log
      }
    }
  }

  public partial class GDS_Client
  {
    private DataSet ds;

    void P1 (SqlConnection Connection)
    { 
      // Create the Data Set
      ds = new DataSet ();
      
      AddTable ("GDS_messages", Connection );
      AddTable ("terminals", Connection);
      AddTable ("GDS_sessions", Connection);
      AddTable ("GDS_transactions", Connection);

    }

    /// <summary>
    /// Adds the given table to the dataset. 
    /// Retrieves the table schema from the DB.
    /// </summary>
    /// <param name="TableName">Table Name</param>
    /// <param name="Connection">Connection</param>
    /// <returns></returns>
    DataTable AddTable (String TableName, SqlConnection Connection)
    {
      SqlDataAdapter da;
      SqlCommand cmd;
      DataTable table;
      SqlCommandBuilder cb;



      table = ds.Tables.Add (TableName);
      cmd = new SqlCommand ("SELECT * FROM " + TableName);
      da = new SqlDataAdapter ();
      da.SelectCommand = cmd;
      da.SelectCommand.Connection = Connection;
      da.FillSchema (table, SchemaType.Source);
      
      cb = new SqlCommandBuilder (da);


      return table;
    }














    void LoadGDSSession (Int32 TerminalId)
    { 
    
    }

    void UpdateGDSSession (Int32 TerminalId, Int64 SequenceId)
    {
      //DataTable GDS_sessions;
      //DataRow[] sessions;
      //DataRow session;

      //sessions = GDS_sessions.Select ("ws_terminal_id = " + TerminalId.ToString ());
      //if ( sessions.Length > 1 )
      //{ 
      
      //}
      //session = sessions[0];

      //sessions["ws_last_sequence_id"] = SequenceId;





      //// 
      //message = GDS_messages.NewRow ();

      //if ( InternalId != 0 )
      //{
      //  message["wm_terminal_id"] = TerminalId;
      //}
      //message["wm_sequence_id"] = SequenceId;
      //message["wm_towards_to_terminal"] = TowardsToTerminal;
      //message["wm_message"] = XmlMessage;

      //GDS_messages.Rows.Add (message);
    
    }

    void AddGDSMessage (Int32 TerminalId, Int64 SequenceId, Boolean TowardsToTerminal, XmlDocument XmlMessage)
    {
      //DataTable GDS_messages;
      //DataRow   message;

      //GDS_messages = ds.Tables["GDS_messages"];

      //message = GDS_messages.NewRow ();

      //if ( InternalId != 0 )
      //{
      //  message["wm_terminal_id"] = TerminalId;
      //}
      //message["wm_sequence_id"] = SequenceId;
      //message["wm_towards_to_terminal"] = TowardsToTerminal;
      //message["wm_message"] = XmlMessage;

      //GDS_messages.Rows.Add (message);
    }

    void AddGDSTransaction (Int32 TerminalId, Int64 SequenceId, Boolean RequestedByTerminal, XmlDocument XmlRequest)
    {
      //DataTable GDS_transactions;
      //DataRow   transaction;

      //// 
      //transaction = GDS_transactions.NewRow ();

      //transaction["wt_terminal_id"] = TerminalId;
      //transaction["wt_sequence_id"] = SequenceId;
      //transaction["wt_requested_by_terminal"] = RequestedByTerminal;
      //transaction["wt_request"] = XmlRequest;

      //GDS_transactions.Rows.Add (transaction);
    }



    void LoadGDSTransactionX (Int32 TerminalId, Int64 SequenceId, Boolean RequestedByTerminal)
    {
      SqlParameter   param;
      String         sb;
      SqlCommand cmd;

      cmd = new SqlCommand ();
      sb = "";
      sb += "SELECT * FROM GDS_TRANSACTION";
      sb += " WHERE   wt_terminal_id = @p1";
      sb += "   AND   wt_sequence_id = @p2";
      sb += "   AND   wt_requested_by_terminal = @p3";

      cmd.CommandText = sb;
      param = cmd.Parameters.Add ("@p1", SqlDbType.Int);
      param.Direction = ParameterDirection.Input;
      param = cmd.Parameters.Add ("@p2", SqlDbType.BigInt);
      param.Direction = ParameterDirection.Input;
      param = cmd.Parameters.Add ("@p3", SqlDbType.Bit);
      param.Direction = ParameterDirection.Input;

      //DataSet ds;

      //GDS_transactions = ds.Tables.Add ("GDS_transactions");



      //GDS_transactions = ds.Tables["GDS_transactions"];
      //da = new SqlDataAdapter ();
      //da.SelectCommand = cmd;
      //da.Fill (GDS_transactions);




      //{
      //  DataTable GDS_transactions;

      //  da.SelectCommand.Parameters[0] = TerminalId;
      //  da.SelectCommand.Parameters[1] = SequenceId;
      //  da.SelectCommand.Parameters[2] = RequestedByTerminal;

      //  da.Fill (GDS_transactions);

      //  if ( GDS_transactions.Rows.Count == 0 )
      //  {
      //    AddGDSTransaction (TerminalId, SequenceId, RequestedByTerminal);
      //  }

      //  //status = (int) GDS_transactions.Rows[0]["wt_status"];

      //  //switch ( status )
      //  //{ 
        
      //  //}
      }



    }


}


