using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using WSI.Common;

namespace WSI.GDS
{
  /// <summary>
  /// Group
  /// </summary>
  public class GDS_Group
  {
    private Int64 m_group_id;
    private String m_name;
    private String m_element_type;
    private ArrayList m_elements;

    /// <summary>
    /// Return the collection of elements
    /// </summary>
    public ArrayList Elements
    {
      get { return m_elements; }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="GroupId"></param>
    public GDS_Group (Int64 GroupId)
    {
      m_group_id = GroupId;
      m_name = "Group " + GroupId.ToString ();
      m_element_type = "Unknown";
      m_elements = new ArrayList ();
    }

    public GDS_Group (Int64 GroupId, String Name, String ElementType)
    {
      m_group_id = GroupId;
      m_name = Name;
      m_element_type = ElementType;
      m_elements = new ArrayList ();
    }


    /// <summary>
    /// Adds an element to the group
    /// </summary>
    /// <param name="ElementId"></param>
    public void Add (Int64 ElementId)
    {
      if ( m_elements.Contains (ElementId) )
      {
        return;
      }
      m_elements.Add (ElementId);
    }

    /// <summary>
    /// Adds all the elements of the given group 
    /// </summary>
    /// <param name="Group"></param>
    public void Add (GDS_Group Group)
    {
      if ( this.m_element_type != Group.m_element_type )
      {
        return;
      }

      foreach ( Int64 element_id in Group.m_elements )
      {
        this.Add (element_id);
      }
    }

    /// <summary>
    /// Explote all elements for this group
    /// </summary>
    /// <param name="GroupId"></param>
    /// <returns></returns>
    public static GDS_Group LoadGroup (Int64 GroupId)
    {
      GDS_Group group;
      GDS_Group subgroup;

      SqlConnection sql_conn;
      SqlTransaction sql_trx;
      SqlCommand sql_command;
      SqlDataReader reader;
      String sql_str;

      Boolean group_is_explicit;
      String sql_definition;

      Boolean element_is_group;
      Int64 element_id;

      group = new GDS_Group (GroupId);


      group_is_explicit = false;
      sql_definition = "";

      element_is_group = false;
      element_id = 0;

      sql_conn = null;
      sql_trx = null;

      try
      {
        sql_conn = WGDB.Connection ();
        sql_trx = sql_conn.BeginTransaction ();

        //
        // Get Group
        //
        sql_str = "SELECT GG_GROUP_IS_EXPLICIT, " +
                        " GG_ELEMENT_TYPE, " +
                        " GG_NAME, " +
                        " GG_SQL_DEFINITION " +
                   " FROM GDS_GROUPS " +
                  " WHERE GG_GROUP_ID = @p1 ";

        sql_command = new SqlCommand (sql_str);
        sql_command.Connection = sql_trx.Connection;
        sql_command.Transaction = sql_trx;

        sql_command.Parameters.Add ("@p1", SqlDbType.BigInt, 4, "GG_GROUP_ID").Value = GroupId;

        reader = null;

        try
        {
          reader = sql_command.ExecuteReader ();
          if ( reader.Read () && !reader.IsDBNull (0) )
          {
            group = new GDS_Group (GroupId, (String) reader[2], (String) reader[1]);
            group_is_explicit = (Boolean) reader[0];
            if ( !group_is_explicit )
            {
              if ( !reader.IsDBNull (3) )
              {
                sql_definition = (String) reader[3];
              }
            }
          }
        }
        catch ( Exception ex )
        {
          Log.Exception (ex);
          Log.Warning ("Exception throw: Error reading groups. GroupId: " + GroupId.ToString ());

          return group;
        }
        finally
        {
          if ( reader != null )
          {
            reader.Close ();
            reader.Dispose ();
            reader = null;
          }
        }

        if ( !group_is_explicit )
        {
          //
          // Get all elements from sql_definition 
          //
          sql_command = new SqlCommand (sql_definition);
          sql_command.Connection = sql_trx.Connection;
          sql_command.Transaction = sql_trx;

          reader = null;

          try
          {
            reader = sql_command.ExecuteReader ();
            while ( reader.Read () && !reader.IsDBNull (0) )
            {
              Type type;
              
              type = reader[0].GetType ();
              if ( type == System.Type.GetType ("System.Int32") )
              {
                element_id = (Int32) reader[0];
              }
              else
              {
                element_id = (Int64) reader[0];
              }

              group.Add (element_id);
            }
          }
          catch ( Exception ex )
          {
            Log.Exception (ex);
            Log.Warning ("Exception throw: Error reading groups. GroupId: " + GroupId.ToString ());

            return group;
          }
          finally
          {
            if ( reader != null )
            {
              reader.Close ();
              reader.Dispose ();
              reader = null;
            }
          }
        }
        else
        {
          //
          // Get explicit Group elements
          //
          sql_str = "SELECT GGE_IS_GROUP, " +
                          " GGE_ELEMENT_ID " +
                     " FROM GDS_GROUP_ELEMENTS " +
                    " WHERE GGE_GROUP_ID = @p1 ";

          sql_command = new SqlCommand (sql_str);
          sql_command.Connection = sql_trx.Connection;
          sql_command.Transaction = sql_trx;

          sql_command.Parameters.Add ("@p1", SqlDbType.BigInt, 4, "GG_GROUP_ID").Value = GroupId;

          reader = null;

          try
          {
            reader = sql_command.ExecuteReader ();
            while ( reader.Read () && !reader.IsDBNull (0) )
            {
              element_is_group = (Boolean) reader[0];
              element_id = (Int64) reader[1];

              if ( element_is_group )
              {
                subgroup = LoadGroup (element_id);
                group.Add (subgroup);
              }
              else
              {
                group.Add (element_id);
              }
            }
          }
          catch ( Exception ex )
          {
            Log.Exception (ex);
            Log.Warning ("Exception throw: Error reading groups. GroupId: " + GroupId.ToString ());

            return group;
          }
          finally
          {
            if ( reader != null )
            {
              reader.Close ();
              reader.Dispose ();
              reader = null;
            }
          }
        }

      }
      catch ( Exception ex )
      {
        Log.Exception (ex);
      }
      finally
      {
        // Close connection
        if ( sql_conn != null )
        {
          sql_conn.Close ();
          sql_conn = null;
        }
      }

      return group;

    } // LoadGroup
  }
}
