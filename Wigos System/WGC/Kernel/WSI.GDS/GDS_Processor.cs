//------------------------------------------------------------------------------
// Copyright � 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: CJ_Processor.cs
// 
//   DESCRIPTION: CJ_Processor class
// 
//        AUTHOR: Andreu Juli�
// 
// CREATION DATE: 14-MAR-2007
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 14-MAR-2007 AJQ    First release.
//------------------------------------------------------------------------------
using System;
using System.Messaging;
using System.Collections.Generic;
using System.Text;
using System.Net;
using WSI.Common;
using System.Xml;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using WSI.GDS;

namespace WSI.GDS
{
  public class GDSInputMsgProcessTask : Task
  {
    Object m_GDS_listener;
    Object m_GDS_client;
    String m_xml_message;

    public  GDSInputMsgProcessTask(Object GDSListener, Object GDSClient, String XmlMessage)
    {
      m_GDS_listener = GDSListener;
      m_GDS_client = GDSClient;
      m_xml_message = XmlMessage;
    }

    public override void Execute()
    {
      GDS_Client.ProcessGDSInputMsg(m_GDS_listener, m_GDS_client, m_xml_message);
    }
  }    

  public partial class GDS_Client: IXmlSink 
  {

    private Worker gds_msg_recv_worker;
    private WaitableQueue gds_msg_recv_waitable_queue;

    private static GDS_Client gds_client;


    public static void Init()
    {
      gds_client = new GDS_Client();
    }


    public static GDS_Client GetInstance()
    {
      return gds_client;
    }

    public void StartWorker()
    {
      // - Workers input queue
      gds_msg_recv_waitable_queue = new WaitableQueue();

      // - Message received processing worker
      // Start workers for GDS requests
      for (int i = 0; i < 10; i++)
      {
        gds_msg_recv_worker = new Worker(gds_msg_recv_waitable_queue);
        gds_msg_recv_worker.Start();
      }
    }


    public static void Start()
    {
      // Workers start
      gds_client.StartWorker();
    }

    ///// <summary>
    ///// CJ Message received
    ///// </summary>
    ///// <param name="Xml"></param>
    ///// <param name="DequeueMessage"></param>
    //protected override void MessageReceived (string Xml, ref Boolean DequeueMessage)
    //{
    //  // Message received: Enqueue message to be processed.
    //  cj_rcv_queue.Enqueue(Xml);
    //}


    public virtual void Process(Object GDSListener, Object GDSClient, String XmlMessage)
    {
      GDSInputMsgProcessTask msg_input_task;

      msg_input_task = new GDSInputMsgProcessTask(GDSListener, GDSClient,XmlMessage);

      gds_msg_recv_waitable_queue.Enqueue(msg_input_task);
    }


    public static void ProcessGDSInputMsg (Object GDSListener, Object GDSClient, String XmlMessage)
    {
      GDS_Message GDS_request;
      GDS_Message GDS_response;
      SecureTcpClient client;
      SqlConnection sql_conn;
      SqlTransaction sql_trx;
      String msg_response_xml_text;
      Boolean bool_rc;
      Boolean error;
      Int64 response_session_id;
      Int32 response_terminal_id;
      Int64 GDS_message_id;

      client = (SecureTcpClient) GDSClient;

      sql_conn = null;
      sql_trx = null;
      error = true;
      GDS_response = null;

      try
      {
        GDS_request = GDS_Message.CreateMessage (XmlMessage);
        GDS_response = GDS_Message.CreateMessage (GDS_request.MsgHeader.MsgType + 1);

        GDS_response.MsgHeader.PrepareReply (GDS_request.MsgHeader);

        if ( GDS_request.MsgHeader.TerminalId.Length > 0 )
        {
          response_session_id = GDS_request.MsgHeader.TerminalSessionId;
        }
        else
        {
          response_session_id = GDS_request.MsgHeader.ServerSessionId;
        }

        response_terminal_id = client.InternalId;

        // AJQ 24-JUL-2007, Do NOT log/store the KeepAlive messages
        // ACC 25-JUL-2007, Do NOT log/store the GetCardType messages
        switch (GDS_request.MsgHeader.MsgType)
        {
          case GDS_MsgTypes.GDS_MsgKeepAlive:
          {
            GDS_BusinessLogic.DB_UpdateGDSSessionMessageReceived(response_terminal_id, response_session_id);

            client.Send(GDS_response.ToXml());

            error = false; 

            return;
          }
          // Unreached code. 
          //break;

          default:
          break;
        }

        NetLog.Add (NetEvent.MessageReceived, client.Identity, XmlMessage);

        // Get Sql Connection 
        sql_conn = WGDB.Connection ();


        //
        // Request from Gaming processing:
        //

        // Begin Transaction
        sql_trx = sql_conn.BeginTransaction();

        // Insert GDS message.
        GDS_BusinessLogic.DB_InsertGDSMessage (client.InternalId, response_session_id, false, XmlMessage, GDS_request.MsgHeader.SequenceId, out GDS_message_id, sql_trx);

        if ( GDS_request.MsgHeader.MsgType != GDS_MsgTypes.GDS_MsgEnrollServer
            && GDS_request.MsgHeader.MsgType != GDS_MsgTypes.GDS_MsgEnrollTerminal )
        {
          if ( client.InternalId == 0 )
          {
            if ( GDS_request.MsgHeader.TerminalId.Length > 0 )
            {
              throw new GDS_Exception (GDS_ResponseCodes.GDS_RC_TERMINAL_NOT_AUTHORIZED, "Terminal not enrolled");
            }
            else
            {
              throw new GDS_Exception (GDS_ResponseCodes.GDS_RC_SERVER_NOT_AUTHORIZED, "Server not enrolled");
            }
          }

          // Check for Gaming retry.
          bool_rc = GDS_BusinessLogic.DB_CheckMsgIsRetry (client.InternalId, GDS_request.MsgHeader.SequenceId, true, out msg_response_xml_text, sql_trx);

          if ( bool_rc )
          {
            // Check to send again answer back to Gaming.
            if (msg_response_xml_text != null)
            {
              GDS_Message GDS_old_response;

              GDS_old_response = GDS_Message.CreateMessage(msg_response_xml_text);
              if ( GDS_old_response.MsgHeader.MsgType == GDS_response.MsgHeader.MsgType )
              {
                client.Send(msg_response_xml_text);
                NetLog.Add(NetEvent.MessageSent, client.Identity, "RETRY: " + msg_response_xml_text);
              }
              else
              {
                throw new GDS_Exception(GDS_ResponseCodes.GDS_RC_INVALID_MSG_TYPE, "Retry: previous request had another type");
              }
            }
            else
            {
              // Answer is not available. 
              // AJQ 26-SEP-2007, It is not possible to send a retry to CJ. 
              //  - The CJ messages are not stored on the DB
              //  - The answer will be sent to the Gaming System on the arrival of the CJ response
              Log.Warning ("A retry from GDS do not have an available CJ request. TerminalId: " + client.InternalId + " - SeqId: " + GDS_request.MsgHeader.SequenceId.ToString ());
            }

            return;
          }

          if (!DbCache.IsSessionActive(response_session_id, true))
          {
            if ( GDS_request.MsgHeader.TerminalId.Length > 0 )
            {
              throw new GDS_Exception(GDS_ResponseCodes.GDS_RC_TERMINAL_SESSION_NOT_VALID, "a.TerminalSessionId is not active");
            }
            else
            {
              throw new GDS_Exception(GDS_ResponseCodes.GDS_RC_SERVER_SESSION_NOT_VALID, "a.ServerSessionId is not active");
            }
          }

          // Insert GDS trx 
          GDS_BusinessLogic.DB_InsertGDSTransaction (client.InternalId, response_session_id, GDS_request.MsgHeader.SequenceId, true, GDS_request.TransactionId, GDS_message_id, sql_trx);
        }

        if ( GDS_request.MsgHeader.MsgType.ToString ().IndexOf ("Reply") >= 0 )
        {
          return;
        }

        if ( GDS_request.MsgHeader.MsgType == GDS_MsgTypes.GDS_MsgEnrollServer )
        {
          if ( GDS_request.MsgHeader.ServerId.Length == 0 )
          {
            throw new GDS_Exception (GDS_ResponseCodes.GDS_RC_INVALID_SERVER_ID, "ServerId cannot be empty.");
          }
          if ( GDS_request.MsgHeader.ServerSessionId != 0 )
          {
            throw new GDS_Exception (GDS_ResponseCodes.GDS_RC_INVALID_SERVER_SESSION_ID, "ServerSessionId should be 0");
          }
        }
        else
        {
          if ( GDS_request.MsgHeader.ServerId.Length > 0 )
          {
            if ( GDS_request.MsgHeader.ServerSessionId == 0 )
            {
              throw new GDS_Exception (GDS_ResponseCodes.GDS_RC_INVALID_SERVER_SESSION_ID, "ServerSessionId cannot be 0");
            }
            if ( !DbCache.IsSessionActive (GDS_request.MsgHeader.ServerSessionId, true) )
            {
              throw new GDS_Exception(GDS_ResponseCodes.GDS_RC_SERVER_SESSION_NOT_VALID, "ServerSessionId is not active");
            }
          }
        }

        if ( GDS_request.MsgHeader.MsgType == GDS_MsgTypes.GDS_MsgEnrollTerminal )
        {
          if ( GDS_request.MsgHeader.TerminalId.Length == 0 )
          {
            throw new GDS_Exception (GDS_ResponseCodes.GDS_RC_INVALID_TERMINAL_ID, "TerminalId cannot be empty");
          }
          if ( GDS_request.MsgHeader.TerminalSessionId != 0 )
          {
            throw new GDS_Exception (GDS_ResponseCodes.GDS_RC_INVALID_TERMINAL_SESSION_ID, "TerminalSessionId should be 0");
          }
        }
        else
        {
          if ( GDS_request.MsgHeader.TerminalId.Length > 0 )
          {
            if ( GDS_request.MsgHeader.TerminalSessionId == 0 )
            {
              throw new GDS_Exception (GDS_ResponseCodes.GDS_RC_INVALID_TERMINAL_SESSION_ID, "TerminalSessionId cannot be 0");
            }

            if ( !DbCache.IsSessionActive (GDS_request.MsgHeader.TerminalSessionId, true) )
            {
              throw new GDS_Exception(GDS_ResponseCodes.GDS_RC_TERMINAL_SESSION_NOT_VALID, "TerminalSessionId is not active");
            }
          }
        }

        if ( GDS_request.MsgHeader.TerminalId.Length > 0 )
        {
          if ( GDS_request.MsgHeader.TerminalSessionId > 0 )
          {
            if ( !DbCache.IsSessionActive (GDS_request.MsgHeader.TerminalSessionId, true) )
            {
              throw new GDS_Exception(GDS_ResponseCodes.GDS_RC_TERMINAL_NOT_AUTHORIZED, "TerminalSession is not active");
            }
          }
        }

        if ( GDS_request.MsgHeader.ServerId.Length > 0 )
        {
          if ( GDS_request.MsgHeader.ServerSessionId > 0 )
          {
            if ( !DbCache.IsSessionActive (GDS_request.MsgHeader.ServerSessionId, true) )
            {
              throw new GDS_Exception(GDS_ResponseCodes.GDS_RC_SERVER_NOT_AUTHORIZED, "ServerSession is not active");
            }
          }
        }

        switch ( GDS_request.MsgHeader.MsgType )
        {
          case GDS_MsgTypes.GDS_MsgEnrollServer:
          {
            // Enqueue enroll in pending enrolls queue.
            GDS_PendingEnrollsQueue.GetInstance().EnqueueEnroll(new EnrollQueueElement(GDSListener, GDSClient, GDS_request));

            // NO RESPONSE IS NEEDED: The Enroll worker sends the response.

            // Commit the message & trx
            sql_trx.Commit();

            error = false;

            return;
          }
          // Unreachable code detected
          // break;

          case GDS_MsgTypes.GDS_MsgEnrollTerminal:
          {
            // Enqueue enroll in pending enrolls queue.
            GDS_PendingEnrollsQueue.GetInstance().EnqueueEnroll(new EnrollQueueElement(GDSListener, GDSClient, GDS_request));

            // NO RESPONSE IS NEEDED: The Enroll worker sends the response.

            // Commit the message & trx
            sql_trx.Commit();

            error = false;

            return;
          }
          // Unreachable code detected
          // break;

          // Unenroll
          //  - Server
          //  - Terminal
          case GDS_MsgTypes.GDS_MsgUnenrollServer:
          {
            // Enqueue enroll in pending enrolls queue.
            GDS_PendingEnrollsQueue.GetInstance().EnqueueEnroll(new EnrollQueueElement(GDSListener, GDSClient, GDS_request));

            // NO RESPONSE IS NEEDED: The Enroll worker sends the response.

            // Commit the message & trx
            sql_trx.Commit();

            error = false;

            return;
          }
          // Unreachable code detected
          // break;

          case GDS_MsgTypes.GDS_MsgUnenrollTerminal:
          {
            // Enqueue enroll in pending enrolls queue.
            GDS_PendingEnrollsQueue.GetInstance().EnqueueEnroll(new EnrollQueueElement(GDSListener, GDSClient, GDS_request));

            // NO RESPONSE IS NEEDED: The Enroll worker sends the response.

            // Commit the message & trx
            sql_trx.Commit();

            error = false;

            return;
          }
          // Unreachable code detected
          // break;

          //case GDS_MsgTypes.GDS_MsgStartCardSession:
          //{
          //  // ACC 25-JUL-2007
          //  // Check Server or Terminal is blocked.
          //  if (DbCache.IsTerminalLocked(GDS_request.MsgHeader.TerminalId))
          //  {
          //    throw new GDS_Exception(GDS_ResponseCodes.GDS_RC_TERMINAL_NOT_AUTHORIZED, "Server or Terminal not authorized. Not found or blocked");
          //  }
          //}
          //break;

          //case GDS_MsgTypes.GDS_MsgEndSession:
          //{
          //  GDS_MsgEndSession request;

          //  request = (GDS_MsgEndSession)GDS_request.MsgContent;

          //  // Check Play Session Id
          //  if (!DbCache.IsPlaySessionActive(request.CardSessionId, true))
          //  {
          //    throw new GDS_Exception(GDS_ResponseCodes.GDS_RC_CARD_NOT_VALIDATED, "CardSessionId isn't active.");
          //  }
          //}
          //break;

          //case GDS_MsgTypes.GDS_MsgLockCard:
          //{
          //  GDS_MsgLockCard request;
          //  request = (GDS_MsgLockCard)GDS_request.MsgContent;

          //  // Check Play Session Id
          //  if (!DbCache.IsPlaySessionActive(request.CardSessionId, true))
          //  {
          //    throw new GDS_Exception(GDS_ResponseCodes.GDS_RC_CARD_NOT_VALIDATED, "CardSessionId isn't active.");
          //  }

          //}
          //break;

          //case GDS_MsgTypes.GDS_MsgReportPlay:
          //{
          //  GDS_MsgReportPlay request;
          //  Int64 aux_balance;

          //  request = (GDS_MsgReportPlay) GDS_request.MsgContent;

          //  // Check Play Session Id
          //  if (!DbCache.IsPlaySessionActive(request.CardSessionId, true))
          //  {
          //    throw new GDS_Exception(GDS_ResponseCodes.GDS_RC_CARD_NOT_VALIDATED, "CardSessionId isn't active.");
          //  }

          //  // Check played amount
          //  if (request.SessionBalanceBeforePlay < request.PlayedAmount)
          //  {
          //    throw new GDS_Exception(GDS_ResponseCodes.GDS_RC_INVALID_AMOUNT, "Play Not Possible: BalanceBeforePlay lower than PlayedAmount");
          //  }

          //  if (request.SessionBalanceBeforePlay < 0)
          //  {
          //    throw new GDS_Exception(GDS_ResponseCodes.GDS_RC_INVALID_AMOUNT, "Play Not Possible: BalanceBeforePlay lower than 0");
          //  }

          //  if (request.PlayedAmount < 0)
          //  {
          //    throw new GDS_Exception(GDS_ResponseCodes.GDS_RC_INVALID_AMOUNT, "Play Not Possible: PlayedAmount lower than 0");
          //  }

          //  if (request.WonAmount < 0)
          //  {
          //    throw new GDS_Exception(GDS_ResponseCodes.GDS_RC_INVALID_AMOUNT, "Play Not Possible: WonAmount lower than 0");
          //  }

          //  if (request.SessionBalanceAfterPlay < 0 )
          //  {
          //    throw new GDS_Exception(GDS_ResponseCodes.GDS_RC_INVALID_AMOUNT, "Play Not Possible: SessionBalanceAfterPlay lower than 0");
          //  }

          //  // Check final balance
          //  aux_balance = request.SessionBalanceBeforePlay;
          //  aux_balance -= request.PlayedAmount;
          //  aux_balance += request.WonAmount;
          //  if (aux_balance != request.SessionBalanceAfterPlay)
          //  {
          //    throw new GDS_Exception(GDS_ResponseCodes.GDS_RC_CARD_BALANCE_MISMATCH, "Final Balance mismatch.");
          //  }
          //}
          //break;

          //case GDS_MsgTypes.GDS_MsgAddCredit:
          //{
          //  throw new GDS_Exception(GDS_ResponseCodes.GDS_RC_ERROR, "AddCredit: Not supported.");
          //}
          //// break;

          //case GDS_MsgTypes.GDS_MsgReportEvent:
          //{
          //  GDS_MsgReportEvent request;

          //  request = (GDS_MsgReportEvent) GDS_request.MsgContent;

          //  GDS_BusinessLogic.DB_InsertEvent(client.InternalId, response_session_id, request.EventType, request.EventData, request.EventDateTime, sql_trx);
          //}
          //break;

          //case GDS_MsgTypes.GDS_MsgReportMeters:
          //{
          //  GDS_MsgReportMeters request;

          //  request = (GDS_MsgReportMeters)GDS_request.MsgContent;

          //  GDS_BusinessLogic.DB_UpdateMeters(client.InternalId, request, sql_trx);
          //}
          //break;

          default:
          break;
        }

        switch (GDS_request.MsgHeader.MsgType)
        {
          default:
          break;
        }

        String str_response;

        str_response = GDS_response.ToXml ();
 
        GDS_BusinessLogic.DB_InsertGDSMessage (response_terminal_id, response_session_id, true, str_response, GDS_response.MsgHeader.SequenceId, out GDS_message_id, sql_trx);

        if ( GDS_request.MsgHeader.MsgType != GDS_MsgTypes.GDS_MsgEnrollServer
            && GDS_request.MsgHeader.MsgType != GDS_MsgTypes.GDS_MsgEnrollTerminal )
        {
          // Update GDS trx
          GDS_BusinessLogic.DB_UpdateGDSTransaction (response_terminal_id, GDS_request.MsgHeader.SequenceId, true, GDS_message_id, sql_trx);
        }

        // End Transaction
        sql_trx.Commit ();

        client.Send (str_response);
        NetLog.Add (NetEvent.MessageSent, client.Identity, str_response);

        error = false;
      }
      catch ( GDS_Exception GDS_ex )
      {
        String GDS_error_response;

        GDS_error_response = null;
        try
        {
          if ( GDS_response == null )
          {
            GDS_response = GDS_Message.CreateMessage (GDS_MsgTypes.GDS_MsgError);
          }
          GDS_response.MsgHeader.ResponseCode = GDS_ex.ResponseCode;
          GDS_response.MsgHeader.ResponseCodeText = GDS_ex.ResponseCodeText;

          // Create error response
          GDS_error_response = GDS_response.ToXml();

          client.Send(GDS_error_response);
          NetLog.Add(NetEvent.MessageSent, client.Identity, GDS_error_response);
        }
        catch ( Exception ex )
        {
          Log.Exception (ex);
        }
      }
      catch ( Exception ex )
      {
        NetLog.Add (NetEvent.MessageFormatError, client.Identity + " a.1)", ex.Message);
        NetLog.Add (NetEvent.MessageFormatError, client.Identity + " b.1)", XmlMessage);

        Log.Exception (ex);

        return;
      }
      finally
      {
        if ( sql_trx != null )
        {
          if ( error && sql_trx.Connection != null )
          {
            sql_trx.Rollback();
          }

          sql_trx.Dispose ();
          sql_trx = null;
        }

        // Close connection
        if (sql_conn != null)
        {
          sql_conn.Close();
          sql_conn = null;
        }
      }
    }
  }
}
