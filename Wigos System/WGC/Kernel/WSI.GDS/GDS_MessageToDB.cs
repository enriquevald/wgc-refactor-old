using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Data.SqlClient;


namespace WSI.GDS
{

  public class GDSMessageId
  {
    //Int64 wm_message_id;
  }

  public class DbAsyncResult:IAsyncResult
  {

    WaitHandle  wait_handle;
    Object      state;
    //Int32       db_status;

    public DbAsyncResult ()
    {
      wait_handle = new ManualResetEvent (false);
      state = null;
    }

    public Boolean IsCompleted
    {
      get
      {
        return wait_handle.WaitOne (0, false);
      }
    }
    public WaitHandle AsyncWaitHandle
    {
      get
      {
        return wait_handle;
      }
    }
    public Boolean CompletedSynchronously
    {
      get
      {
        return false;
      }
    }
    public Object AsyncState
    {
      get
      {
        return state;
      }
    }
  }

  //////static public class GDS_DbMsgLog
  //////{
  //////  static SqlDataAdapter da;





  //////  public GDS_DbMsgLog ()
  //////  {
  //////  }

  //////  public static IAsyncResult BeginInsertGDSMessage (Int32 TerminalId,
  //////                                                    Int64 SessionId,
  //////                                                    Boolean TowardsToTerminal,
  //////                                                    String MsgXmlText,
  //////                                                    Int64 SequenceId)
  //////  {
  //////    MyAsyncResult x;

  //////    x = new MyAsyncResult ();

  //////    return x;

  //////  }

  //////  public static Boolean EndInsertGDSMessage (IAsyncResult Result, out Int64 GDSMessageId)
  //////  {
  //////    MyAsyncResult myx;
  //////    myx = (MyAsyncResult) Result;
  //////    GDSMessageId = myx.MessageId;
  //////    return myx.success;

  //////  }
  //////}
}
