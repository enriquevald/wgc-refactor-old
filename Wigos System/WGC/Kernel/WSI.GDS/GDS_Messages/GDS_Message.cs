//------------------------------------------------------------------------------
// Copyright � 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: GDS_Mesage.cs
// 
//   DESCRIPTION: GDS_Mesage class
// 
//        AUTHOR: Andreu Juli�
// 
// CREATION DATE: 12-MAR-2007
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 12-MAR-2007 AJQ    First release.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Reflection;
using WSI.Common;


namespace WSI.GDS
{
  public interface IXml
  {
    ///TODO: cambiar tambi�n todos los ToXml para que en lugar del append child hagan 
    ///una string directa con los valores.
    String ToXml();
    // void LoadXml (String Xml);
    void LoadXml(XmlReader Xml);
  }

  public interface IXml_CJ
  {
    String ToXml();
    void LoadXml (String Xml);
  }

  public class GDS_Exception : Exception
  {
    GDS_ResponseCodes response_code;
    String response_code_text;

    public GDS_ResponseCodes ResponseCode
    {
      get
      {
        return response_code;
      }
    }
    public String ResponseCodeText
    {
      get
      {
        return response_code_text;
      }
    }

    public GDS_Exception (GDS_ResponseCodes Value)
    {
      response_code = Value;
    }

    public GDS_Exception (GDS_ResponseCodes Code, String Text):base(Text)
    {
      response_code = Code;
      response_code_text = Text;
    }

    public GDS_Exception (GDS_ResponseCodes Code, String Text, Exception InnerException): base (Text, InnerException)
    {
      response_code = Code;
      response_code_text = Text;
    }
  }


  public partial class GDS_Message
  {
    public GDS_MsgHeader MsgHeader;
    public IXml MsgContent;

    public Int64 TransactionId
    {
      get
      {
        switch ( MsgHeader.MsgType )
        {
          //case GDS_MsgTypes.GDS_MsgReportPlay:
          //  return ( (GDS_MsgReportPlay) MsgContent ).TransactionId;

          //case GDS_MsgTypes.GDS_MsgAddCredit:
          //  return ( (GDS_MsgAddCredit) MsgContent ).TransactionId;

          default:
            return 0;
        }
      }
    }

    private GDS_Message ()
    {
    }


    //////public static GDS_Message CreateMessage (String Xml)
    //////{
    //////  GDS_Message GDS_message;

    //////  GDS_message = null;

    //////  try
    //////  {
    //////    XmlDocument document;
    //////    XmlNode node;
    //////    GDS_MsgTypes msg_type;
    //////    String target_type;

    //////    document = new XmlDocument ();
    //////    document.LoadXml (Xml);

    //////    node = document.SelectSingleNode ("GDS_Message/GDS_MsgHeader/MsgType");
    //////    if ( node == null )
    //////    { 
    //////      throw new GDS_Exception(GDS_ResponseCodes.GDS_RC_MSG_FORMAT_ERROR, "Tag not found: MsgType"); 
    //////    }

    //////    target_type = node.InnerText;

    //////    for ( msg_type = 0; msg_type < GDS_MsgTypes.GDS_MsgUnknown; msg_type++ )
    //////    {
    //////      if ( target_type == msg_type.ToString () )
    //////      {
    //////        break;
    //////      }
    //////    }
    //////    if ( msg_type >= GDS_MsgTypes.GDS_MsgUnknown )
    //////    {
    //////      return null;
    //////    }

    //////    GDS_message = CreateMessage (msg_type);

    //////    node = document.SelectSingleNode ("GDS_Message/GDS_MsgHeader");
    //////    if ( node == null )
    //////    {
    //////      throw new GDS_Exception (GDS_ResponseCodes.GDS_RC_MSG_FORMAT_ERROR, "Tag not found: GDS_MsgHeader");
    //////    }
    //////    GDS_message.MsgHeader.LoadXml (node.OuterXml);
    //////    node = document.SelectSingleNode ("GDS_Message/GDS_MsgContent");
    //////    if ( node == null )
    //////    {
    //////      throw new GDS_Exception (GDS_ResponseCodes.GDS_RC_MSG_FORMAT_ERROR, "Tag not found: GDS_MsgContent");
    //////    }
    //////    node = node.SelectSingleNode (target_type);
    //////    if ( node == null )
    //////    {
    //////      throw new GDS_Exception (GDS_ResponseCodes.GDS_RC_MSG_FORMAT_ERROR, "Tag not found: " + target_type.ToString ());
    //////    }
    //////    GDS_message.MsgContent.LoadXml (node.OuterXml);
    //////  }
    //////  catch ( GDS_Exception GDS_ex )
    //////  {
    //////    throw GDS_ex;
    //////  }
    //////  catch ( Exception ex )
    //////  {
    //////    // Format Error in message
    //////    throw new GDS_Exception(GDS_ResponseCodes.GDS_RC_MSG_FORMAT_ERROR, ex.Message, ex);
    //////  }

    //////  return GDS_message;
    //////}

    //////public static GDS_Message CreateMessage (GDS_MsgTypes MsgType)
    //////{
    //////  GDS_Message msg;
    //////  IXml content;

    //////  Assembly assembly;
    //////  String type_name;
    //////  Type target_type;


    //////  // The Power of Reflection
    //////  content = null;

    //////  type_name = "WSI.GDS." + MsgType.ToString ();

    //////  assembly = Assembly.GetExecutingAssembly ();
    //////  target_type = assembly.GetType (type_name);

    //////  Type[] types = new Type[0];
    //////  ConstructorInfo info = target_type.GetConstructor (types);
    //////  content = (IXml) info.Invoke (null);

    //////  msg = new GDS_Message ();

    //////  msg.MsgHeader = new GDS_MsgHeader ();
    //////  msg.MsgHeader.MsgType = MsgType;
    //////  msg.MsgContent = content;

    //////  return msg;
    //////}


    public static GDS_Message CreateMessage(String Xml)
    {
      GDS_Message GDS_message;

      GDS_message = null;

      try
      {
        XmlReader xmlreader;

        bool found;

        GDS_MsgHeader hdr;
        hdr = new GDS_MsgHeader();
        System.IO.StringReader textreader = new System.IO.StringReader(Xml);
        xmlreader = XmlReader.Create(textreader);

        //Loads the message header
        hdr.LoadXml(xmlreader);
        if (hdr.MsgType >= GDS_MsgTypes.GDS_MsgUnknown)
        {
          return null;
        }
        //Creates the message if the message header is correct.
        GDS_message = CreateMessage(hdr.MsgType);
        //Assign the header to the current message.
        GDS_message.MsgHeader = hdr;

        //Put the xmlreader at the beginning of the Message Content
        found = false;
        while (xmlreader.Read() && !found)
        {
          if ((xmlreader.Name == "GDS_MsgContent") && (xmlreader.NodeType.Equals(XmlNodeType.Element)))
          {
            found = true;
          }
        }
        if (found)
        {
          GDS_message.MsgContent.LoadXml(xmlreader);
          if (GDS_message.MsgContent == null)
          {
            throw new GDS_Exception(GDS_ResponseCodes.GDS_RC_MSG_FORMAT_ERROR, "Tag not found: " + GDS_message.MsgHeader.MsgType.ToString());
          }
        }
        else
        {
          throw new GDS_Exception(GDS_ResponseCodes.GDS_RC_MSG_FORMAT_ERROR, "Tag not found: " + GDS_message.MsgHeader.MsgType.ToString());
        }
        xmlreader.Close();
      }
      catch (GDS_Exception GDS_ex)
      {
        throw GDS_ex;
      }
      catch (Exception ex)
      {
        // Format Error in message
        throw new GDS_Exception(GDS_ResponseCodes.GDS_RC_MSG_FORMAT_ERROR, ex.Message, ex);
      }
      return GDS_message;
    }


    public static GDS_Message CreateMessage(GDS_MsgTypes MsgType)
    {
      GDS_Message msg;
      IXml content;

      Assembly assembly;
      String type_name;
      Type target_type;

      msg = new GDS_Message();

      msg.MsgHeader = new GDS_MsgHeader();
      msg.MsgHeader.MsgType = MsgType;


      //if (MsgType == GDS_MsgTypes.GDS_MsgReportPlay)
      //{
      //  msg.MsgContent = new WSI.GDS.GDS_MsgReportPlay();

      //  return msg;
      //}

      // The Power of Reflection
      content = null;

      type_name = "WSI.GDS." + MsgType.ToString();

      assembly = Assembly.GetExecutingAssembly();
      target_type = assembly.GetType(type_name);

      Type[] types = new Type[0];
      ConstructorInfo info = target_type.GetConstructor(types);
      content = (IXml)info.Invoke(null);

      msg.MsgContent = content;

      return msg;
    }










    public String ToXml ()
    {
      String xml;
      String xml_header;
      String xml_content;

      xml_header = MsgHeader.ToXml ();
      xml_content = MsgContent.ToXml ();

      xml = "";
      xml += "<GDS_Message>";
      xml += xml_header;
      xml += "<GDS_MsgContent>";
      xml += xml_content;
      xml += "</GDS_MsgContent>";
      xml += "</GDS_Message>";

      return xml;
    }
  }

}
