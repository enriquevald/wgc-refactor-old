//------------------------------------------------------------------------------
// Copyright � 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: GDS_MsgHeader.cs
// 
//   DESCRIPTION: GDS Message Header
// 
//        AUTHOR: Andreu Juli�
// 
// CREATION DATE: 12-MAR-2007
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 12-MAR-2007 AJQ    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.IO;

using WSI.Common;


namespace WSI.GDS
{
  /// <summary>
  /// GDS message types
  /// </summary>
  public enum GDS_MsgTypes
  { 
      // Request                 	// Reply
      GDS_MsgRequestService,            GDS_MsgRequestServiceReply
    , GDS_MsgEnrollServer,              GDS_MsgEnrollServerReply
    , GDS_MsgEnrollTerminal,            GDS_MsgEnrollTerminalReply
    , GDS_MsgUnenrollTerminal,          GDS_MsgUnenrollTerminalReply
    , GDS_MsgUnenrollServer,            GDS_MsgUnenrollServerReply
    , GDS_MsgGetParameters,             GDS_MsgGetParametersReply
    , GDS_MsgNewParamsNotification ,    GDS_MsgNewParamsNotificationReply
    , GDS_MsgNotifyCurrentParameters,   GDS_MsgNotifyCurrentParametersReply
    , GDS_MsgExecuteCommand,            GDS_MsgExecuteCommandReply
    , GDS_MsgKeepAlive,                 GDS_MsgKeepAliveReply
    , GDS_MsgError
    , GDS_MsgUnknown
  }
  
  /// <summary>
  /// GDS response codes
  /// </summary>
  public enum GDS_ResponseCodes
  {
      GDS_RC_OK
    , GDS_RC_ERROR
    , GDS_RC_DATABASE_OFFLINE
    , GDS_RC_SERVICE_NOT_AVAILABLE
    , GDS_RC_SERVICE_NOT_FOUND
    , GDS_RC_SERVER_NOT_AUTHORIZED
    , GDS_RC_SERVER_SESSION_NOT_VALID
    , GDS_RC_TERMINAL_NOT_AUTHORIZED
    , GDS_RC_TERMINAL_SESSION_NOT_VALID
    , GDS_RC_CARD_NOT_VALID
    , GDS_RC_CARD_BLOCKED
    , GDS_RC_CARD_EXPIRED
    , GDS_RC_CARD_ALREADY_IN_USE
    , GDS_RC_CARD_NOT_VALIDATED
    , GDS_RC_CARD_BALANCE_MISMATCH
    , GDS_RC_CARD_REMOTE_BALANCE_MISMATCH
    , GDS_RC_CARD_SESSION_NOT_CLOSED
    , GDS_RC_CARD_SESSION_NOT_VALID
    , GDS_RC_INVALID_SESSION_ID
    , GDS_RC_UNKNOWN_GAME_ID
    , GDS_RC_INVALID_AMOUNT
    , GDS_RC_EVENT_NOT_VALID
    , GDS_RC_CRC_MISMATCH
    , GDS_RC_NOT_ENOUGH_PLAYERS
    , GDS_RC_PROVIDER_NOT_VALID
    , GDS_RC_ERROR_UPDATING_PROVIDER

    , GDS_RC_MSG_FORMAT_ERROR
    , GDS_RC_INVALID_MSG_TYPE
    , GDS_RC_INVALID_PROTOCOL_VERSION
    , GDS_RC_INVALID_SEQUENCE_ID
    , GDS_RC_INVALID_SERVER_ID
    , GDS_RC_INVALID_TERMINAL_ID
    , GDS_RC_INVALID_SERVER_SESSION_ID
    , GDS_RC_INVALID_TERMINAL_SESSION_ID
    , GDS_RC_INVALID_COMPRESSION_METHOD
    , GDS_RC_INVALID_RESPONSE_CODE

    , GDS_RC_UNKNOWN
    , GDS_RC_LAST
  }

  public enum GDS_EventTypesCodes
  {
    GDS_EV_DEVICE_STATUS = 1,
    GDS_EV_OPERATION     = 2,
    
    GDS_EV_DOOR_OPENED = 101,
    GDS_EV_DOOR_CLOSED = 102,
    GDS_EV_NOTE_ACCEPTOR_JAM = 103,
    GDS_EV_NOTE_ACCEPTOR_FULL = 104,
    GDS_EV_NOTE_ACCEPTOR_ERROR = 105,
    GDS_EV_PRINTER_NO_PAPER = 106,
    GDS_EV_PRINTER_JAM = 107,
    GDS_EV_PRINTER_ERROR = 108,
    GDS_EV_POWER_FAILURE = 109,
    GDS_EV_LOST_CONNECTION = 110,
    GDS_EV_STARTUP  = 111, 
    GDS_EV_SHUTDOWN = 112,
    GDS_EV_CREDIT_LIMIT = 113,
    GDS_EV_DISPLAY_OFF = 114,
    GDS_EV_CARDREADER_OFF = 115,
  }

  public enum GDS_CardTypes
  { 
      CARD_TYPE_UNKNOWN
    , CARD_TYPE_PLAYER
  }


  public enum GDS_CashTypes
  {
    CASH_TYPE_CASH_IN   = 1
  , CASH_TYPE_CASH_OUT  = 2
  }


  public enum GDS_MoneyTypes
  {
    MONEY_TYPE_NOTE     = 1
  , MONEY_TYPE_COIN     = 2
  }

  public enum GDS_CommandTypes
  {
     GDS_CMD_SHUTDOWN
   , GDS_CMD_RESTART
   , GDS_CMD_RELOAD_PARAMETERS
   , GDS_CMD_RESET_METERS
   , GDS_CMD_REPORT_METERS
   , GDS_CMD_LOCK
   , GDS_CMD_DISCONNECT
   , GDS_CMD_INTEGRITY_CHECK
  }

  /// <summary>
  /// GDS compression methods
  /// </summary>
  public enum GDS_CompressionMethod
  { 
    NONE
  }

  /// <summary>
  /// Device Codes
  /// </summary>
  public enum DeviceCodes
  {
    GDS_DEVICE_CODE_NO_DEVICE = 0
  , GDS_DEVICE_CODE_PRINTER = 1
  , GDS_DEVICE_CODE_NOTE_ACCEPTOR = 2
  , GDS_DEVICE_CODE_SMARTCARD_READER = 3
  , GDS_DEVICE_CODE_CUSTOMER_DISPLAY = 4
  , GDS_DEVICE_CODE_COIN_ACCEPTOR = 5
  , GDS_DEVICE_CODE_UPPER_DISPLAY = 6
  , GDS_DEVICE_CODE_BAR_CODE_READER = 7
  , GDS_DEVICE_CODE_UPS = 9
  , GDS_DEVICE_CODE_MODULE_IO = 15
  }

  /// <summary>
  /// Device Status
  /// </summary>
  static public class DeviceStatus
  {
    static public readonly Int32 GDS_DEVICE_STATUS_UNKNOWN = 0;
    static public readonly Int32 GDS_DEVICE_STATUS_OK = 1;
    static public readonly Int32 GDS_DEVICE_STATUS_ERROR = 2;
    static public readonly Int32 GDS_DEVICE_STATUS_NOT_INSTALLED = 100;
    static public readonly Int32 GDS_DEVICE_STATUS_PRINTER_OFF = 3;
    static public readonly Int32 GDS_DEVICE_STATUS_PRINTER_READY = 4;
    static public readonly Int32 GDS_DEVICE_STATUS_PRINTER_NOT_READY = 5;
    static public readonly Int32 GDS_DEVICE_STATUS_PRINTER_PAPER_OUT = 6;
    static public readonly Int32 GDS_DEVICE_STATUS_PRINTER_PAPER_LOW = 7;
    static public readonly Int32 GDS_DEVICE_STATUS_PRINTER_OFFLINE = 8;
    static public readonly Int32 GDS_DEVICE_STATUS_NOTE_ACCEPTOR_JAM = 3;
    static public readonly Int32 GDS_DEVICE_STATUS_NOTE_ACCEPTOR_SN_ERROR = 4;
    static public readonly Int32 GDS_DEVICE_STATUS_NOTE_ACCEPTOR_STACKER_FULL = 5;
    static public readonly Int32 GDS_DEVICE_STATUS_UPS_NO_AC = 3;
    static public readonly Int32 GDS_DEVICE_STATUS_UPS_BATTERY_LOW = 4;
    static public readonly Int32 GDS_DEVICE_STATUS_UPS_OVERLOAD = 5;
    static public readonly Int32 GDS_DEVICE_STATUS_UPS_OFF = 6;
    static public readonly Int32 GDS_DEVICE_STATUS_UPS_BATTERY_FAIL = 7;
    static public readonly Int32 GDS_DEVICE_STATUS_SMARTCARD_REMOVED = 3;
    static public readonly Int32 GDS_DEVICE_STATUS_SMARTCARD_INSERTED = 4;
    static public readonly Int32 GDS_DEVICE_STATUS_COIN_ACCEPTOR_JAM = 3;
    static public readonly Int32 GDS_DEVICE_STATUS_COIN_ACCEPTOR_SN_ERROR = 4;
    static public readonly Int32 GDS_DEVICE_STATUS_COIN_ACCEPTOR_STACKER_FULL = 5;
  }

  /// <summary>
  /// Priority
  /// </summary>
  public enum DevicePriority
  {
    GDS_DEVICE_PRIORITY_OK = 0
  , GDS_DEVICE_PRIORITY_WARNING = 1
  , GDS_DEVICE_PRIORITY_ERROR = 2
  }

  /// <summary>
  /// Operations
  /// </summary>
  public enum OperationCodes
  {
    GDS_OPERATION_CODE_NO_OPERATION = 0
  , GDS_OPERATION_CODE_START = 1
  , GDS_OPERATION_CODE_SHUTDOWN = 2
  , GDS_OPERATION_CODE_RESTART = 3
  , GDS_OPERATION_CODE_TUNE_SCREEN = 4
  , GDS_OPERATION_CODE_LAUNCH_EXPLORER = 5
  , GDS_OPERATION_CODE_ASSIGN_KIOSK = 6
  , GDS_OPERATION_CODE_DEASSIGN_KIOSK = 7
  , GDS_OPERATION_CODE_UNLOCK_DOOR = 8
  , GDS_OPERATION_CODE_DISPLAY_SETTINGS = 11
  , GDS_OPERATION_CODE_DOOR_OPENED = 12
  , GDS_OPERATION_CODE_DOOR_CLOSED = 13
  , GDS_OPERATION_CODE_BLOCK_KIOSK = 14
  , GDS_OPERATION_CODE_UNBLOCK_KIOSK = 15
  , GDS_OPERATION_CODE_JACKPOT_WON = 16
  , GDS_OPERATION_CODE_KIOSK_BLOCKED_HIGH_PRIZE = 17
  , GDS_OPERATION_CODE_CALL_ATTENDANT = 18
  , GDS_OPERATION_CODE_TEST_MODE_ENTER = 19
  , GDS_OPERATION_CODE_TEST_MODE_LEAVE = 20

}

  /// <summary>
  /// The GDS message header
  /// </summary>
  public class GDS_MsgHeader  
  {
    #region CONSTANTS

    public const string PROTOCOL_VERSION = "1.0.0.4";
    const string COMPRESSION_METHOD = "NONE";
    
    #endregion // CONSTANTS

    #region MEMBERS

    private string ProtocolVersion;
    public  string CompressionMethod;
    private DateTime SystemTime;
    private Int32 TimeZone;
    public string ServerId;
    public string TerminalId;
    public Int64 ServerSessionId;
    public Int64 TerminalSessionId;
    public GDS_MsgTypes MsgType;
    public Int64 SequenceId;
    public int MsgContentSize;
    public GDS_ResponseCodes ResponseCode;
    public string ResponseCodeText;
    //////public static Int64 SequenceIdResponse = 0;

    #endregion // MEMBERS

    #region PUBLIC

    /// <summary>
    ///   Gets the Protocol Version
    /// </summary>
    public string GetProtocolVersion
    {
      get { return ProtocolVersion; }
    }

    /// <summary>
    ///   Gets the Compression Method
    /// </summary>
    GDS_CompressionMethod GetCompressionMethod
    {
      get { return GDS_CompressionMethod.NONE; }
    }

    /// <summary>
    /// Gets the System Time
    /// </summary>
    public DateTime GetSystemTime
    {
      get { return SystemTime; }
    }

    /// <summary>
    /// Get time Zone
    /// </summary>
    public Int32 GetTimeZone
    {
      get { return TimeZone; }
    }

    /// <summary>
    /// Creates the reply header.
    /// The following fields are copied from the request:
    /// - ServerId
    /// - TerminalId
    /// - ServerSessionId
    /// - TerminalSessionId
    /// - SequenceId
    /// </summary>
    /// <param name="ResponseCode">Response Code</param>
    /// <returns>The reply header</returns>
    public GDS_MsgHeader ReplyHeader (GDS_ResponseCodes ResponseCode)
    {
      GDS_MsgHeader reply;

      reply = new GDS_MsgHeader ();
      reply.ServerId = ServerId;
      reply.TerminalId = TerminalId;
      reply.ServerSessionId = ServerSessionId;
      reply.TerminalSessionId = TerminalSessionId;
      reply.MsgType = MsgType;
      reply.MsgType++;
      reply.SequenceId = SequenceId;
      reply.ResponseCode = ResponseCode;
      reply.ResponseCodeText = "";

      return reply;
    }

    public void PrepareReply (GDS_MsgHeader RequestHeader)
    {
      ServerId = RequestHeader.ServerId;
      TerminalId = RequestHeader.TerminalId;
      ServerSessionId = RequestHeader.ServerSessionId;
      TerminalSessionId = RequestHeader.TerminalSessionId;
      SequenceId = RequestHeader.SequenceId;

      if ( MsgType == GDS_MsgTypes.GDS_MsgUnknown )
      {
        MsgType = RequestHeader.MsgType + 1;
      }
    }


    /// <summary>
    /// Extracts an enum object value from a 
    /// Enum Object giving the string name of 
    /// the value and getting the index.
    /// </summary>
    /// <param name="ResponseCode">Response Code</param>
    /// <returns>The reply header</returns>
    public static Object Decode(String str, Object defaultValue)
    {
      foreach (String s in System.Enum.GetNames(defaultValue.GetType()))
      {
        if (s.Equals(str))
        {
          return System.Enum.Parse(defaultValue.GetType(), s);
        }
      }
      return defaultValue;
    }

    public void LoadXml(XmlReader reader)
    {
      ProtocolVersion = XML.GetValue(reader, "ProtocolVersion");
      CompressionMethod = XML.GetValue(reader, "CompressionMethod");

      string time;
      string time_zone_hours;
      string time_zone_minutes;
      Int32 minutes;

      time = XML.GetValue(reader, "SystemTime");

      DateTime.TryParse(time, out SystemTime);

      time_zone_hours = time.Substring(time.Length - 6, 3);
      time_zone_minutes = time.Substring(time.Length - 2, 2);

      Int32.TryParse(time_zone_hours, out TimeZone);
      Int32.TryParse(time_zone_minutes, out minutes);
      TimeZone *= 60;
      if (TimeZone >= 0)
      {
        TimeZone += minutes;
      }
      else
      {
        TimeZone -= minutes;
      }

      ServerId = XML.GetValue(reader, "ServerId");
      TerminalId = XML.GetValue(reader, "TerminalId");
      long.TryParse(XML.GetValue(reader, "ServerSessionId"), out ServerSessionId);
      long.TryParse(XML.GetValue(reader, "TerminalSessionId"), out TerminalSessionId);
      MsgType = (GDS_MsgTypes)Decode(XML.GetValue(reader, "MsgType"), GDS_MsgTypes.GDS_MsgUnknown);
      long.TryParse(XML.GetValue(reader, "SequenceId"), out SequenceId);
      int.TryParse(XML.GetValue(reader, "MsgContentSize"), out MsgContentSize);
      ResponseCode = (GDS_ResponseCodes)Decode(XML.GetValue(reader, "ResponseCode"), GDS_ResponseCodes.GDS_RC_UNKNOWN);
      ResponseCodeText = XML.GetValue(reader, "ResponseCodeText");
    }


    public String ToXml ()
    {

      DateTime now;
      string str;

      string strtemp;
      strtemp = "";

      now = DateTime.Now;
      str = XML.XmlDateTimeString(now);

      strtemp += "<GDS_MsgHeader>";

      strtemp += "<ProtocolVersion>";
      strtemp += ProtocolVersion;
      strtemp += "</ProtocolVersion>";

      strtemp += "<CompressionMethod>";
      strtemp += CompressionMethod;
      strtemp += "</CompressionMethod>";

      strtemp += "<SystemTime>";
      strtemp += str;
      strtemp += "</SystemTime>";

      strtemp += "<ServerId>";
      strtemp += ServerId;
      strtemp += "</ServerId>";

      strtemp += "<TerminalId>";
      strtemp += TerminalId;
      strtemp += "</TerminalId>";

      strtemp += "<ServerSessionId>";
      strtemp += ServerSessionId.ToString();
      strtemp += "</ServerSessionId>";

      strtemp += "<TerminalSessionId>";
      strtemp += TerminalSessionId.ToString();
      strtemp += "</TerminalSessionId>";

      strtemp += "<MsgType>";
      strtemp += MsgType.ToString();
      strtemp += "</MsgType>";

      strtemp += "<SequenceId>";
      strtemp += SequenceId.ToString();
      strtemp += "</SequenceId>";

      strtemp += "<MsgContentSize>";
      strtemp += MsgContentSize.ToString();
      strtemp += "</MsgContentSize>";

      strtemp += "<ResponseCode>";
      strtemp += ResponseCode.ToString();
      strtemp += "</ResponseCode>";

      strtemp += "<ResponseCodeText>";
      strtemp += ResponseCodeText;
      strtemp += "</ResponseCodeText>";

      strtemp += "</GDS_MsgHeader>";


      return strtemp;
    }
    /// <summary>
    /// Adds the header to the given Xml GDS message
    /// </summary>
    /// <param name="XmlGDSMessage">The message</param>
    public void ToXml (XmlDocument XmlGDSMessage)
    {
      XmlNode root;
      XmlNode header;
      XmlNode old_header;
      DateTime now;
      string str;

      now = DateTime.Now;
      str = XML.XmlDateTimeString(now);

      root = XmlGDSMessage.SelectSingleNode ("GDS_Message");
      header = XmlGDSMessage.CreateNode (XmlNodeType.Element, "", "GDS_MsgHeader", "");

      XML.AppendChild (header, "ProtocolVersion", ProtocolVersion);
      XML.AppendChild (header, "CompressionMethod", CompressionMethod);
      XML.AppendChild (header, "SystemTime", str);
      XML.AppendChild (header, "ServerId", ServerId);
      XML.AppendChild (header, "TerminalId", TerminalId);
      XML.AppendChild (header, "ServerSessionId", ServerSessionId.ToString ());
      XML.AppendChild (header, "TerminalSessionId", TerminalSessionId.ToString ());
      XML.AppendChild (header, "MsgType", MsgType.ToString ());
      XML.AppendChild (header, "SequenceId", SequenceId.ToString ());
      XML.AppendChild (header, "MsgContentSize", MsgContentSize.ToString ());
      XML.AppendChild (header, "ResponseCode", ResponseCode.ToString ());
      XML.AppendChild (header, "ResponseCodeText", ResponseCodeText);

      old_header = root.SelectSingleNode ("GDS_MsgHeader");
      if ( old_header != null )
      {
        root.RemoveChild (old_header);
      }

      if ( root.FirstChild != null )
      {
        root.InsertBefore (header, root.FirstChild);
      }
      else
      {
        root.AppendChild (header);
      }
    }

    /// <summary>
    /// Creates a header
    /// </summary>
    public GDS_MsgHeader ()
    {
      Init ();
    }

    #endregion // PUBLIC

    #region PRIVATE

    /// <summary>
    /// Initializes the header
    /// </summary>
    private void Init()
    {
      ProtocolVersion = PROTOCOL_VERSION;
      CompressionMethod = COMPRESSION_METHOD;
      SystemTime = System.DateTime.Now;
      ServerId = "";
      TerminalId = "";
      ServerSessionId = 0;
      TerminalSessionId = 0;
      MsgType = GDS_MsgTypes.GDS_MsgUnknown;
      SequenceId = 0;
      MsgContentSize = 0;
      ResponseCode = GDS_ResponseCodes.GDS_RC_OK;
      ResponseCodeText = "";
    }


    #endregion // PRIVATE
  }
}
