//------------------------------------------------------------------------------
// Copyright � 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: GDS_MsgUnenrollServer.cs
// 
//   DESCRIPTION: GDS_MsgUnenrollServer class
// 
//        AUTHOR: Andreu Juli�
// 
// CREATION DATE: 14-MAR-2007
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 14-MAR-2007 AJQ    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Net;
using WSI.Common;


namespace WSI.GDS
{
  public class GDS_MsgUnenrollServer:IXml
  {
    public String ToXml ()
    {
      XmlDocument xml;
      XmlNode node;

      xml = new XmlDocument ();

      node = xml.CreateNode (XmlNodeType.Element, "GDS_MsgUnenrollServer", "");
      xml.AppendChild (node);

      return xml.OuterXml;
    }


    public void LoadXml (XmlReader reader)
    {
      if (reader.Name != "GDS_MsgUnenrollServer")
      {
        throw new GDS_Exception(GDS_ResponseCodes.GDS_RC_MSG_FORMAT_ERROR, "Tag GDS_MsgUnenrollServer not found in content area");
      }
    }
  }

  public class GDS_MsgUnenrollServerReply:IXml
  {
    public String ToXml ()
    {
      XmlDocument xml;
      XmlNode node;

      xml = new XmlDocument ();

      node = xml.CreateNode (XmlNodeType.Element, "GDS_MsgUnenrollServerReply", "");
      xml.AppendChild (node);

      return xml.OuterXml;
    }

    public void LoadXml (XmlReader reader)
    {
      if (reader.Name != "GDS_MsgUnenrollServerReply")
      {
        throw new GDS_Exception(GDS_ResponseCodes.GDS_RC_MSG_FORMAT_ERROR, "Tag GDS_MsgUnenrollServerReply not found in content area");
      }
    }
  }
}
