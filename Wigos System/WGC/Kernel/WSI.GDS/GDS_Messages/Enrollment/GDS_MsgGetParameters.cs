//------------------------------------------------------------------------------
// Copyright � 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: GDS_MsgEnrollTerminal.cs
// 
//   DESCRIPTION: GDS_MsgGetParameters class
// 
//        AUTHOR: Andreu Juli�
// 
// CREATION DATE: 14-MAR-2007
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 14-MAR-2007 AJQ    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic; 
using System.Text;
using System.Xml;
using System.Net;
using WSI.Common;

namespace WSI.GDS
{
  public class GDS_MsgGetParameters:IXml
  {
    public String ToXml ()
    {
      XmlDocument xml;
      XmlNode node;

      xml = new XmlDocument ();

      node = xml.CreateNode (XmlNodeType.Element, "GDS_MsgGetParameters", "");
      xml.AppendChild (node);

      return xml.OuterXml;
    }


    public void LoadXml(XmlReader reader)
    {
      if (reader.Name != "GDS_MsgGetParameters")
      {
        throw new GDS_Exception(GDS_ResponseCodes.GDS_RC_MSG_FORMAT_ERROR, "Tag GDS_MsgGetParameters not found in content area");
      }
    }
  }

  public class GDS_MsgGetParametersReply:IXml
  {
    public int ConnectionTimeout;
    public int KeepAliveInterval;
    public String LocationName;
    public String LocationAddress;

    public GDS_MsgGetParametersReply ()
    {
      ConnectionTimeout = 60; // secs
      KeepAliveInterval = 30; // secs
    }

    public String ToXml ()
    {
      XmlDocument xml;
      XmlNode node;

      xml = new XmlDocument ();

      node = xml.CreateNode (XmlNodeType.Element, "GDS_MsgGetParametersReply", "");
      xml.AppendChild (node);

      XML.AppendChild (node, "ConnectionTimeout", ConnectionTimeout.ToString ());
      XML.AppendChild (node, "KeepAliveInterval", KeepAliveInterval.ToString ());
      XML.AppendChild (node, "LocationName", LocationName);
      XML.AppendChild (node, "LocationAddress", LocationAddress);

      return xml.OuterXml;
    }

    public void LoadXml(XmlReader reader)
    {

      if (reader.Name != "GDS_MsgGetParametersReply")
      {
        throw new GDS_Exception(GDS_ResponseCodes.GDS_RC_MSG_FORMAT_ERROR, "Tag GDS_MsgGetParametersReply not found in content area");
      }
      else
      {
        ConnectionTimeout = Int32.Parse(XML.GetValue(reader, "ConnectionTimeout"));
        KeepAliveInterval = Int32.Parse(XML.GetValue(reader, "KeepAliveInterval"));
        LocationName = XML.GetValue(reader, "LocationName");
        LocationAddress = XML.GetValue(reader, "LocationAddress");
      }
    }

  }
}
