//------------------------------------------------------------------------------
// Copyright � 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: GDS_MsgUnenrollTerminal.cs
// 
//   DESCRIPTION: GDS_MsgUnenrollTerminal class
// 
//        AUTHOR: Andreu Juli�
// 
// CREATION DATE: 14-MAR-2007
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 14-MAR-2007 AJQ    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Net;
using WSI.Common;


namespace WSI.GDS
{
  public class GDS_MsgUnenrollTerminal:IXml
  {
    public String ToXml ()
    {
      XmlDocument xml;
      XmlNode node;

      xml = new XmlDocument ();

      node = xml.CreateNode (XmlNodeType.Element, "GDS_MsgUnenrollTerminal", "");
      xml.AppendChild (node);

      return xml.OuterXml;
    }

    public void LoadXml(XmlReader reader)
    {
      if (reader.Name != "GDS_MsgUnenrollTerminal")
      {
        throw new GDS_Exception(GDS_ResponseCodes.GDS_RC_MSG_FORMAT_ERROR, "Tag GDS_MsgUnenrollTerminal not found in content area");
      }
    }
  }

  public class GDS_MsgUnenrollTerminalReply:IXml
  {
    public String ToXml ()
    {
      XmlDocument xml;
      XmlNode node;

      xml = new XmlDocument ();

      node = xml.CreateNode (XmlNodeType.Element, "GDS_MsgUnenrollTerminalReply", "");
      xml.AppendChild (node);

      return xml.OuterXml;
    }

    public void LoadXml(XmlReader reader)
    {
      if (reader.Name != "GDS_MsgUnenrollTerminalReply")
      {
        throw new GDS_Exception(GDS_ResponseCodes.GDS_RC_MSG_FORMAT_ERROR, "Tag GDS_MsgUnenrollTerminalReply not found in content area");
      }
    }
  }
}
