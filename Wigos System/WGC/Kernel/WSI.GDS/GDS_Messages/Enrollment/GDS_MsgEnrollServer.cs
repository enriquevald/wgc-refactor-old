//------------------------------------------------------------------------------
// Copyright � 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: GDS_MsgEnrollServer.cs
// 
//   DESCRIPTION: GDS_MsgEnrollServer class
// 
//        AUTHOR: Andreu Juli�
// 
// CREATION DATE: 14-MAR-2007
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 14-MAR-2007 AJQ    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Net;
using WSI.Common;


namespace WSI.GDS
{
  public class GDS_MsgEnrollServer:IXml
  {
    public String ProviderId;

    public String ToXml()
    {
      XmlDocument xml;
      XmlNode node;

      xml = new XmlDocument ();

      node = xml.CreateNode (XmlNodeType.Element, "GDS_MsgEnrollServer", "");
      xml.AppendChild (node);

      XML.AppendChild(node, "ProviderId", ProviderId);

      return xml.OuterXml;
    }


    public void LoadXml(XmlReader Xmlreader)
    {
      Boolean found_provider;

      found_provider = false;
      
      ProviderId = "";

      //Like the old function "LoadXml(String)" this only has an integrity check.
      if (Xmlreader.Name != "GDS_MsgEnrollServer")
      {
        throw new GDS_Exception(GDS_ResponseCodes.GDS_RC_MSG_FORMAT_ERROR, "Tag GDS_MsgEnrollServer not found in content area");
      }
      else
      {
        try
        {
          for (; ; )
          {
            if (Xmlreader.Read())
            {
              // Try to find the Provider ID tag
              if (Xmlreader.Name == "ProviderId")
              {
                found_provider = true;
                break;
              }

              // The next Tag after Provider ID is GDS_MsgEnrollServer
              if (Xmlreader.Name == "GDS_MsgEnrollServer")
              {
                found_provider = false;
                break;
              }
            }
            else
            {
              //There are no more tags
              found_provider = false;
              break;
            }
          }

          if (found_provider)
          {
            if (Xmlreader.Read())
            {
              if (Xmlreader.HasValue)
              {
                ProviderId = Xmlreader.Value;
              }
            }
            else
            {
              throw new GDS_Exception(GDS_ResponseCodes.GDS_RC_MSG_FORMAT_ERROR, "Tag Provider ID not correctly ended");
            }
          }
        }
        catch (Exception ex)
        {
          Log.Exception(ex);
        }
      }
    }


  }

  public class GDS_MsgEnrollServerReply:IXml
  {
    public Int64 LastSequenceId;

    public String ToXml ()
    {
      XmlDocument xml;
      XmlNode node;

      xml = new XmlDocument ();

      node = xml.CreateNode (XmlNodeType.Element, "GDS_MsgEnrollServerReply", "");
      xml.AppendChild (node);

      XML.AppendChild (node, "LastSequenceId", LastSequenceId.ToString ());

      return xml.OuterXml;
    }

    public void LoadXml(XmlReader reader)
    {
      LastSequenceId = Int64.Parse(XML.GetValue(reader, "LastSequenceId"));
    }
  }
}
