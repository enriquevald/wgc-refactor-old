//------------------------------------------------------------------------------
// Copyright � 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: GDS_MsgEnrollTerminal.cs
// 
//   DESCRIPTION: GDS_MsgEnrollTerminal class
// 
//        AUTHOR: Andreu Juli�
// 
// CREATION DATE: 14-MAR-2007
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 14-MAR-2007 AJQ    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Net;
using WSI.Common;

namespace WSI.GDS
{
  public class GDS_MsgEnrollTerminal:IXml
  {
    public String ProviderId;

    public String ToXml()
    {
      XmlDocument xml;
      XmlNode node;

      xml = new XmlDocument ();

      node = xml.CreateNode (XmlNodeType.Element, "GDS_MsgEnrollTerminal", "");
      xml.AppendChild (node);

      XML.AppendChild(node, "ProviderId", ProviderId);

      return xml.OuterXml;
    }

    public void LoadXml(XmlReader Xmlreader)
    {
      Boolean found_provider;

      found_provider = false;

      ProviderId = "";

      //Like the old function "LoadXml(String)" this only has an integrity check.
      if (Xmlreader.Name != "GDS_MsgEnrollTerminal")
      {
        throw new GDS_Exception(GDS_ResponseCodes.GDS_RC_MSG_FORMAT_ERROR, "Tag GDS_MsgEnrollTerminal not found in content area");
      }
      else
      {
        try
        {
          for (; ; )
          {
            if (Xmlreader.Read())
            {
              // Try to find the Provider ID tag
              if (Xmlreader.Name == "ProviderId")
              {
                found_provider = true;
                break;
              }

              // The next Tag after Provider ID is GDS_MsgEnrollTerminal
              if (Xmlreader.Name == "GDS_MsgEnrollTerminal")
              {
                break;
              }
            }
            else
            {
              //There are no more tags
              break;
            }
          }

          if (found_provider)
          {
            if (Xmlreader.Read())
            {
              if (Xmlreader.HasValue)
              {
                ProviderId = Xmlreader.Value;
              }
            }
            else
            {
              throw new GDS_Exception(GDS_ResponseCodes.GDS_RC_MSG_FORMAT_ERROR, "Tag Provider ID not correctly ended");
            }
          }
        }
        catch (Exception ex)
        {
          Log.Exception(ex);
        }
      }
    }
  }

  public class GDS_MsgEnrollTerminalReply:IXml
  {
    public Int64 LastSequenceId;
    public Int64 LastTransactionId;

    public String ToXml ()
    {
      XmlDocument xml;
      XmlNode node;

      xml = new XmlDocument ();

      node = xml.CreateNode (XmlNodeType.Element, "GDS_MsgEnrollTerminalReply", "");
      xml.AppendChild (node);

      XML.AppendChild (node, "LastSequenceId", LastSequenceId.ToString ());
      XML.AppendChild (node, "LastTransactionId", LastTransactionId.ToString ());

      return xml.OuterXml;
    }

    public void LoadXml(XmlReader reader)
    {
      if (reader.Name != "GDS_MsgEnrollTerminalReply")
      {
        throw new GDS_Exception(GDS_ResponseCodes.GDS_RC_MSG_FORMAT_ERROR, "Tag GDS_MsgEnrollTerminalReply not found in content area");
      }
      else
      {
        LastSequenceId = Int64.Parse(XML.GetValue(reader, "LastSequenceId"));
        LastTransactionId = Int64.Parse(XML.GetValue(reader, "LastTransactionId"));
      }
    }
  }
}
