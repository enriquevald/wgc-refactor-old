//------------------------------------------------------------------------------
// Copyright © 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: GDS_MsgKeepAlive.cs
// 
//   DESCRIPTION: GDS_MsgKeepAlive class
// 
//        AUTHOR: Ronald Rodríguez
// 
// CREATION DATE: 14-MAR-2007
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 14-MAR-2007 RRT    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Net;
using WSI.Common;

namespace WSI.GDS
{
    public class GDS_MsgKeepAlive : IXml
    {
        public String ToXml()
        {
            XmlDocument xml;
            XmlNode node;

            xml = new XmlDocument();

            node = xml.CreateNode(XmlNodeType.Element, "GDS_MsgKeepAlive", "");
            xml.AppendChild(node);

            return xml.OuterXml;
        }

      public void LoadXml(XmlReader reader)
      {
        if (reader.Name != "GDS_MsgKeepAlive")
        {
          throw new GDS_Exception(GDS_ResponseCodes.GDS_RC_MSG_FORMAT_ERROR, "Tag GDS_MsgKeepAlive not found in content area");
        }
      }

    }

    public class GDS_MsgKeepAliveReply : IXml
    {
        public String ToXml()
        {
            XmlDocument xml;
            XmlNode node;

            xml = new XmlDocument();

            node = xml.CreateNode(XmlNodeType.Element, "GDS_MsgKeepAliveReply", "");
            xml.AppendChild(node);

            return xml.OuterXml;
        }

      public void LoadXml(XmlReader reader)
      {
        if (reader.Name != "GDS_MsgKeepAliveReply")
        {
          throw new GDS_Exception(GDS_ResponseCodes.GDS_RC_MSG_FORMAT_ERROR, "Tag GDS_MsgKeepAliveReply not found in content area");
        }
      }
    }
}
