//------------------------------------------------------------------------------
// Copyright © 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: GDS_MsgKeepAlive.cs
// 
//   DESCRIPTION: GDS_MsgKeepAlive class
// 
//        AUTHOR: Ronald Rodríguez
// 
// CREATION DATE: 15-MAR-2007
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 15-MAR-2007 RRT    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Net;
using WSI.Common;

namespace WSI.GDS
{
  public class GDS_MsgError : IXml
  {
    public String ToXml()
    {
      XmlDocument xml;
      XmlNode node;

      xml = new XmlDocument();

      node = xml.CreateNode(XmlNodeType.Element, "GDS_MsgError", "");
      xml.AppendChild(node);

      return xml.OuterXml;
    }


    public void LoadXml(XmlReader reader)
    {
      if (reader.Name != "GDS_MsgError")
      {
        throw new GDS_Exception(GDS_ResponseCodes.GDS_RC_MSG_FORMAT_ERROR, "Tag GDS_MsgError not found in content area");
      }
    }
  }
}