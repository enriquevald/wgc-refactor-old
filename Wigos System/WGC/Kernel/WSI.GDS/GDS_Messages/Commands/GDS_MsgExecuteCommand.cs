//------------------------------------------------------------------------------
// Copyright © 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: GDS_MsgExecuteCommand.cs
// 
//   DESCRIPTION: GDS_MsgExecuteCommand class
// 
//        AUTHOR: Ronald Rodríguez
// 
// CREATION DATE: 14-MAR-2007
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 14-MAR-2007 RRT    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using WSI.Common;

namespace WSI.GDS.GDS_Messages.Commands
{
  public class GDS_MsgExecuteCommand : IXml
  {
    public GDS_CommandTypes Command;
    public String CommandParameters;

    public String ToXml()
    {
      XmlDocument xml;
      XmlNode node;

      xml = new XmlDocument();

      node = xml.CreateNode(XmlNodeType.Element, "GDS_MsgExecuteCommand", "");
      xml.AppendChild(node);

      XML.AppendChild(node, "Command", Command.ToString ());
      XML.AppendChild(node, "CommandParameters", CommandParameters);

      return xml.OuterXml;
    }

    public void LoadXml(XmlReader reader)
    {
      if (reader.Name != "GDS_MsgExecuteCommand")
      {
        throw new GDS_Exception(GDS_ResponseCodes.GDS_RC_MSG_FORMAT_ERROR, "Tag GDS_MsgExecuteCommand not found in content area");
      }
      else
      {
        Command = (GDS_CommandTypes)Enum.Parse(typeof(GDS_CommandTypes), XML.GetValue(reader, "Command"), true);
        CommandParameters = XML.GetValue(reader, "CommandParameters");
      }
    }
  }

  public class GDS_MsgExecuteCommandReply : IXml
  {
    public String ToXml()
    {
      XmlDocument xml;
      XmlNode node;

      xml = new XmlDocument();

      node = xml.CreateNode(XmlNodeType.Element, "GDS_MsgExecuteCommandReply", "");
      xml.AppendChild(node);

      return xml.OuterXml;
    }

    public void LoadXml(XmlReader reader)
    {
      if (reader.Name != "GDS_MsgExecuteCommandReply")
      {
        throw new GDS_Exception(GDS_ResponseCodes.GDS_RC_MSG_FORMAT_ERROR, "Tag GDS_MsgExecuteCommandReply not found in content area");
      }
    }

  }
}
