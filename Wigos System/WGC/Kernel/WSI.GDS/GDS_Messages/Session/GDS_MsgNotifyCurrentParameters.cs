using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Xml;
using WSI.Common;

namespace WSI.GDS
{
  public class GDS_MsgNotifyCurrentParameters : IXml
  {

    public String ToXml()
    {
      XmlDocument xml;
      //XmlNode node;
      //XmlNode game;

      xml = new XmlDocument();
      //xml.LoadXml("<GDS_MsgNotifyCurrentParameters></GDS_MsgNotifyCurrentParameters>");

      //node = xml.SelectSingleNode("GDS_MsgNotifyCurrentParameters");
      ////XML.AppendChild(node, "ActivationId", activation_id.ToString());
      //XML.AppendChild(node, "GameList", "");

      //node = xml.SelectSingleNode("GDS_MsgNotifyCurrentParameters/GameList");

      //foreach (GDS_Game gds_game in game_list)
      //{
      //  game = xml.CreateNode(XmlNodeType.Element, "Game", "");

      //  node.AppendChild(game);

      //  XML.AppendChild(game, "Id", gds_game.id.ToString());
      //  XML.AppendChild(game, "Name", gds_game.name);
      //}

      return xml.OuterXml;
    }


    public void LoadXml(XmlReader reader)
    {
      if (reader.Name != "GDS_MsgNotifyCurrentParametersReply")
      {
        throw new GDS_Exception(GDS_ResponseCodes.GDS_RC_MSG_FORMAT_ERROR, "Tag GDS_MsgNotifyCurrentParametersReply not found in content area");
      }
    }

    public class GDS_MsgNotifyCurrentParametersReply : IXml
    {
      public String ToXml()
      {
        XmlDocument xml;

        xml = new XmlDocument();
        xml.LoadXml("<GDS_MsgNotifyCurrentParameters></GDS_MsgNotifyCurrentParameters>");

        return xml.OuterXml;
      }


      public void LoadXml(XmlReader reader)
      {
        if (reader.Name != "GDS_MsgNotifyCurrentParametersReply")
        {
          throw new GDS_Exception(GDS_ResponseCodes.GDS_RC_MSG_FORMAT_ERROR, "Tag GDS_MsgNotifyCurrentParametersReply not found in content area");
        }
      }
    }
  }
}
 