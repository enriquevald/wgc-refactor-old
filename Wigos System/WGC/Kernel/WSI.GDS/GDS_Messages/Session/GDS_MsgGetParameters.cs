//------------------------------------------------------------------------------
// Copyright � 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: GDS_MsgEnrollTerminal.cs
// 
//   DESCRIPTION: GDS_MsgGetParameters class
// 
//        AUTHOR: Andreu Juli�
// 
// CREATION DATE: 14-MAR-2007
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 14-MAR-2007 AJQ    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic; 
using System.Text;
using System.Xml;
using System.Net;
using WSI.Common;
using System.Collections;

namespace WSI.GDS
{
  public class GDS_MsgGetParameters:IXml
  {

    public String ToXml ()
    {
      StringBuilder str_builder;

      str_builder = new StringBuilder();

      str_builder.Append("<WC2_MsgGetParameters></WC2_MsgGetParameters>");
      return str_builder.ToString();
    }


    public void LoadXml(XmlReader reader)
    {
      if (reader.Name != "GDS_MsgGetParameters")
      {
        throw new GDS_Exception(GDS_ResponseCodes.GDS_RC_MSG_FORMAT_ERROR, "Tag GDS_MsgGetParameters not found in content area");
      }
    }
  }

  public class GDS_MsgGetParametersReply:IXml
  {
    public int Denomination;
    public int NumGames;
    public ArrayList GameList = new ArrayList();
    public class GDS_Game
    {
      public int Id;
      public String Name;
      public int Payout;
    }

    public String ToXml ()
    {
      StringBuilder str_builder;

      str_builder = new StringBuilder();

      str_builder.Append("<GDS_MsgGetParametersReply><Denomination>");
      str_builder.Append(Denomination);
      str_builder.Append("</Denomination><GameList><NumGames>");
      str_builder.Append(NumGames);
      str_builder.Append("</NumGames>");
      foreach (GDS_Game temp_game in GameList)
      {
        str_builder.Append("<Game><Id>");
        str_builder.Append(temp_game.Id);
        str_builder.Append("</Id><Name>");
        str_builder.Append(temp_game.Name);
        str_builder.Append("</Name><Payout>");
        str_builder.Append(temp_game.Payout);
        str_builder.Append("</Payout></Game>");
      }
      str_builder.Append("</GameList></GDS_MsgGetParametersReply>");
      return str_builder.ToString();
    }

    public void LoadXml(XmlReader reader)
    {
      GDS_Game temp_game;
      if (reader.Name != "GDS_MsgGetParametersReply")
      {
        throw new GDS_Exception(GDS_ResponseCodes.GDS_RC_MSG_FORMAT_ERROR, "Tag GDS_MsgGetParametersReply not found in content area");
      }
      else
      {
        Denomination = int.Parse(XML.GetValue(reader, "Denomination"));
        NumGames = int.Parse(XML.GetValue(reader, "NumGames"));
        if (NumGames > 0)
        {
          GameList = new ArrayList();
          while (((reader.Name != "GameList") || (reader.NodeType != XmlNodeType.EndElement)) && !reader.EOF)
          {
            temp_game = new GDS_Game();
            //temp_game.Id = XML.GetValue(reader, "Id").ToString();
            //temp_game.Name = XML.GetValue(reader, "Name").ToString();
            //temp_game.Payout = XML.GetValue(reader, "Payout").ToString();
            GameList.Add(temp_game);
            reader.Read(); //After this "read" the reader will be at the end of Payout
            reader.Read(); //After this "read" the reader will be at the end of Game
            reader.Read(); //After this "read" the reader will be at the end of GameList or the beginning of new a Game
          }
        }
      }
    }
  }
}
