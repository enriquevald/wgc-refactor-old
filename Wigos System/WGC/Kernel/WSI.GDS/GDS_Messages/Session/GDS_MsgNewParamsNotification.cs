using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using WSI.Common;

namespace WSI.GDS
{
  public class GDS_NewParamsNotification : IXml 
  {


    public String ToXml()
    {
      XmlDocument xml;
      XmlNode node;

      xml = new XmlDocument();

      node = xml.CreateNode(XmlNodeType.Element, "GDS_MsgGetParameters", "");
      xml.AppendChild(node);

      return xml.OuterXml;
    }


    public void LoadXml(XmlReader reader)
    {
      if (reader.Name != "GDS_MsgGetParameters")
      {
        throw new GDS_Exception(GDS_ResponseCodes.GDS_RC_MSG_FORMAT_ERROR, "Tag GDS_MsgGetParameters not found in content area");
      }
    }


  }

  public class GDS_MsgNewParamsNotificationReply : IXml
  {
    public int ConnectionTimeout;
    public int KeepAliveInterval;
    public String LocationName;
    public String LocationAddress;

    public String ToXml()
    {
      XmlDocument xml;
      XmlNode node;

      xml = new XmlDocument();

      node = xml.CreateNode(XmlNodeType.Element, "GDS_MsgNewParamsNotificationReply", "");
      xml.AppendChild(node);

      XML.AppendChild(node, "ConnectionTimeout", ConnectionTimeout.ToString());
      XML.AppendChild(node, "KeepAliveInterval", KeepAliveInterval.ToString());
      XML.AppendChild(node, "LocationName", LocationName);
      XML.AppendChild(node, "LocationAddress", LocationAddress);

      return xml.OuterXml;
    }

    public void LoadXml(XmlReader reader)
    {

      if (reader.Name != "GDS_MsgGetParametersReply")
      {
        throw new GDS_Exception(GDS_ResponseCodes.GDS_RC_MSG_FORMAT_ERROR, "Tag GDS_MsgGetParametersReply not found in content area");
      }
      else
      {
        ConnectionTimeout = Int32.Parse(XML.GetValue(reader, "ConnectionTimeout"));
        KeepAliveInterval = Int32.Parse(XML.GetValue(reader, "KeepAliveInterval"));
        LocationName = XML.GetValue(reader, "LocationName");
        LocationAddress = XML.GetValue(reader, "LocationAddress");
      }
    }

  }

}
   