//------------------------------------------------------------------------------
// Copyright (c) 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: GDS_DirectoryService.cs
// 
//   DESCRIPTION: The GDS directory service
// 
//        AUTHOR: Andreu Julia
// 
// CREATION DATE: 12-MAR-2007
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 12-MAR-2007 AJQ    First release.
// 12-SEP-2008 AJQ    GDS Directory Service inherits from UdpServer
//------------------------------------------------------------------------------

using System;
using System.Text;
using System.Xml;
using System.Net;
using System.Net.Sockets;
using System.Data;
using System.Threading;

using WSI.Common;

using WSI.GDS;


namespace WSI.GDS
{
  /// <summary>
  /// The GDS Directory Service
  /// </summary>
  public class GDS_DirectoryService : UdpServer
  {
    #region MEMBERS
    // ------------------------------------------------------------------------- MEMBERS
    IPEndPoint   m_service_ip_endpoint;         // Service IP EndPoint

    #endregion // MEMBERS

    #region PUBLIC METHODS
    // ------------------------------------------------------------------------- PUBLIC METHODS
    /// <summary>
    /// Creates a new instance of a GDS Directory Service
    /// </summary>
    public GDS_DirectoryService(IPEndPoint ServiceIPEndPoint)
      : base(Protocol.PORT_NUMBER)
    {
      m_service_ip_endpoint = ServiceIPEndPoint;
    } // GDS_DirectoryService

    #endregion // PUBLIC METHODS

    #region PRIVATE METHODS
    // ------------------------------------------------------------------------- PRIVATE METHODS

    /// <summary>
    /// Process the received UPD datagram.
    /// - Only GDS_MsgRequestService is allowed
    /// - 
    /// </summary>
    /// <param name="RemoteIPEndPoint">Source IP endpoint</param>
    /// <param name="Datagram">Received Datagram</param>
    protected override void ProcessDataReceived(IPEndPoint RemoteIPEndPoint, Byte[] Datagram)
    {
      String xml_msg;
      GDS_Message GDS_request;
      GDS_Message GDS_response;
      GDS_MsgRequestService request;
      GDS_MsgRequestServiceReply response;

      try 
      {
        // Data received?
        if (Datagram.Length == 0)
        {
          return;
        }
        
        // Datagram to String using the current Encoding
        xml_msg = this.Encoding.GetString(Datagram);

        // Build the GDS Request
        GDS_request = GDS_Message.CreateMessage(xml_msg);

        // Only GDS_MsgRequestService allowed
        if (GDS_request.MsgHeader.MsgType != GDS_MsgTypes.GDS_MsgRequestService)
        {
          // Ignore message
          return;
        }
        //
        // Process Request: GDS_MsgRequestService
        //
        request = (GDS_MsgRequestService)GDS_request.MsgContent;
        
        // - Check service name
        if (String.Equals(request.ServiceName.Trim(), Protocol.SERVICE_NAME, StringComparison.InvariantCultureIgnoreCase) == false)
        {
          return;
        }

        GDS_response = GDS_Message.CreateMessage(GDS_MsgTypes.GDS_MsgRequestServiceReply);
        response = (GDS_MsgRequestServiceReply)GDS_response.MsgContent;
        GDS_response.MsgHeader.SequenceId = GDS_request.MsgHeader.SequenceId;
        response.ServiceAddress = m_service_ip_endpoint;
        GDS_response.MsgHeader.ResponseCode = GDS_ResponseCodes.GDS_RC_OK;

        // Send the response to the received ReplyTo address
        // AJQ 15-SEP-08, reply to the source IP (ignore the ReplyToAddress)
        Send(RemoteIPEndPoint, GDS_response.ToXml());
      }
      catch (Exception)
      {
      }
      finally 
      {
      }
    }
    #endregion // PRIVATE METHODS

  } // class DirectoryService

  public class GDS_QueryService : UDP_QueryService
  {
    protected override int RequestToPortNumber()
    {
      return Protocol.PORT_NUMBER;
    }

    protected override Byte[] GetRequestDatagram(IPEndPoint LocalEndPoint)
    {
      GDS_Message GDS_request;
      GDS_MsgRequestService GDS_content;

      GDS_request = GDS_Message.CreateMessage(GDS_MsgTypes.GDS_MsgRequestService);

      GDS_content = (GDS_MsgRequestService)GDS_request.MsgContent;

      GDS_content.ServiceName = Protocol.SERVICE_NAME;
      GDS_content.ReplyToUDPAddress = LocalEndPoint;

      return Encoding.UTF8.GetBytes(GDS_request.ToXml());
    }

    protected override IPEndPoint GetResponseIPEndPoint(byte[] ReceivedDatagram)
    {
      String xml_response;
      GDS_Message GDS_response;
      GDS_MsgRequestServiceReply GDS_content;
      IPEndPoint ipe;

      ipe = null;

      try
      {
        xml_response = Encoding.UTF8.GetString(ReceivedDatagram);
        GDS_response = GDS_Message.CreateMessage(xml_response);
        if (GDS_response.MsgHeader.ResponseCode == GDS_ResponseCodes.GDS_RC_OK)
        {
          GDS_content = (GDS_MsgRequestServiceReply)GDS_response.MsgContent;

          ipe = GDS_content.ServiceAddress;
        }
      }
      catch (Exception)
      {
      }
      finally
      {
      }

      GDS_content = null;
      GDS_response = null;
      xml_response = null;

      return ipe;

    }
  } // GDS_QueryService
} // namespace WSI.GDS 
