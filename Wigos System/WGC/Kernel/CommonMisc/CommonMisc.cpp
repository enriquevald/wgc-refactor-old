//------------------------------------------------------------------------------
// Copyright � 2002 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: CommonMisc.cpp
//   DESCRIPTION: Functions and Methods to handle the CommonMisc.
//        AUTHOR: Alberto Cuesta
// CREATION DATE: 08-MAR-2002
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ -----------------------------------------------------------
// 08-MAR-2002 ACC    Initial draft.
// 04-NOV-2002 NIR    Added DllMain function to initialize critical section used
//                    in Common_RetryConnect
// 
//-------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  INCLUDES 
//------------------------------------------------------------------------------

#define INCLUDE_COMMON_MISC
#define INCLUDE_NLS_API
#include "CommonDef.h"
#include "CommonMiscInternals.h"


//------------------------------------------------------------------------------
//  PRIVATE CONSTANTS 
//------------------------------------------------------------------------------

#define HIERARCHIES_GESTOR_ID 0

//------------------------------------------------------------------------------
//  PRIVATE DATATYPES 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PRIVATE DATA STRUCTURES 
//------------------------------------------------------------------------------

// NIR, 04-NOV-2002
// critical section for the retry connect operation
CRITICAL_SECTION  GLB_DbRetryConnectCS; 

//------------------------------------------------------------------------------
//  PRIVATE FUNCTION PROTOTYPES 
//------------------------------------------------------------------------------

DWORD         GetRandomInRange (DWORD UpperBound);

//------------------------------------------------------------------------------
//  PUBLIC FUNCTIONS IMPLEMENTATION 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PURPOSE: Convert array of TCHAR in Hexa mode in array of bytes in decimal mode.
// 
//  PARAMS:
//      - INPUT:
//          - StringHexa: string to convert
//          - LengthHexa: length of the string
//          - LengthByte: length of the array of bytes
//
//      - OUTPUT: 
//          - pByte: output value in decimal mode
// 
// RETURNS: 
//          - COMMON_OK if successfull
//          - COMMON_ERROR otherwise
// 
//   NOTES:

COMMONMISC_API WORD WINAPI Common_HexaToByte (TCHAR * pStringHexa,
                                              WORD  LengthHexa,
                                              BYTE  * pByte,
                                              WORD  LengthByte)
{
  WORD   _idx_hexa, _idx_byte;
  TCHAR  _string_buffer [3];
  INT    _result_byte;

  memset (_string_buffer, 0, sizeof(_string_buffer));
  _idx_byte = 0;
  for ( _idx_hexa = 0; _idx_hexa < (LengthHexa - 1); _idx_hexa += 2 )
  {
    if ( _idx_byte == LengthByte )
    {
      return COMMON_ERROR;
    } // if

    _string_buffer[0] = pStringHexa [_idx_hexa];
    _string_buffer[1] = pStringHexa [_idx_hexa + 1];
    _stscanf (_string_buffer, _T("%X"), &_result_byte);
    pByte [_idx_byte] = _result_byte;
    _idx_byte ++;
  } // for

  return COMMON_OK;

} // Common_HexaToByte


//------------------------------------------------------------------------------
// PURPOSE: Convert array of bytes in array of TCHAR in Hexa mode.
// 
//  PARAMS:
//      - INPUT:
//          - pByte: input value in decimal mode
//          - LengthByte: length of the array of bytes
//          - LengthHexa: length of the string
//
//      - OUTPUT: 
//          - StringHexa: output string in hexa mode.
// 
// RETURNS: 
//          - COMMON_OK if successfull
//          - COMMON_ERROR otherwise
// 
//   NOTES:

COMMONMISC_API WORD WINAPI Common_ByteToHexa (BYTE  * pByte,
                                              WORD  LengthByte,
                                              TCHAR * pStringHexa,
                                              WORD  LengthHexa)
{
  WORD _idx_byte;
  
  for ( _idx_byte = 0; _idx_byte < LengthByte; _idx_byte ++ )
  {
    if ( (_idx_byte * 2) >= LengthHexa - 1 )
    {
      return COMMON_ERROR;
    } // if
    _stprintf ( pStringHexa + (_idx_byte * 2), _T("%02X"), pByte [_idx_byte]);
  }  // for

  return COMMON_OK;


} // Common_ByteToHexa

//------------------------------------------------------------------------------
// PURPOSE: Similar to system GetProcAddress, but accepts a unicode module name.
// 
//  PARAMS:
//      - INPUT:
//          - ModuleHandle
//          - pFunctionName
//
//      - OUTPUT: 
//          - None
// 
// RETURNS: Address of the exported function or variable
// 
//   NOTES:

COMMONMISC_API FARPROC WINAPI Common_GetProcAddress (HMODULE ModuleHandle,
                                                     TCHAR   * pFunctionName)
{
  char _str_function_name [COMMON_DEFAULT_STRING_SIZE];

  memset (_str_function_name, 0, sizeof (_str_function_name));

  // Convert TCHAR to MultiByte
  Common_WC2MB (pFunctionName,
                _tcslen (pFunctionName),
                _str_function_name, 
                sizeof (_str_function_name));

  return GetProcAddress (ModuleHandle,        // Module 
                         _str_function_name); // ProcName

} // Common_GetProcAddress 


//------------------------------------------------------------------------------
// PURPOSE: Similar to system LoadLibrary, but accepts a unicode module name.
// 
//  PARAMS:
//      - INPUT:
//          - pFileName
//
//      - OUTPUT: 
//          - None
// 
// RETURNS: Address of the exported function or variable
// 
//   NOTES:

COMMONMISC_API HMODULE WINAPI Common_LoadLibrary (TCHAR * pFileName)
{

  return LoadLibrary (pFileName); // ProcName
} // Common_LoadLibrary 


//------------------------------------------------------------------------------
// PURPOSE: Get gestor Id.
// 
//  PARAMS:
//      - INPUT:
//          - pGestorId: Gestor Id.
//          - pContext:  Database connection context
//
//      - OUTPUT:
//          - None
// RETURNS:
//      - COMMON_OK
// 
//   NOTES: 
//      - The Gestor Id value is always 1 in LKC system. 
//      - If change Gestor Id in LKC system then must change this function.
//
COMMONMISC_API DWORD WINAPI Common_GetGestorId (DWORD          * pGestorId,
                                                SQL_CONTEXT    pContext)
{
  *pGestorId = HIERARCHIES_GESTOR_ID;

  return COMMON_OK;
} // Common_GetGestorId

// NIR, 04-NOV-2002
//-----------------------------------------------------------------------------
// PURPOSE: DLL standard main function.
//
//  PARAMS:
//     - INPUT: None
//
//     - OUTPUT: None
//
//
// RETURNS:
//     - TRUE: OK
//     - FALSE: Force dll unload
//
// NOTES:
//

BOOL APIENTRY DllMain (HANDLE Module, 
                       DWORD  ReasonForCall, 
                       LPVOID Reserved)
{
  switch ( ReasonForCall )
  {
    case DLL_PROCESS_ATTACH:
    {
      // DLL load action
      InitializeCriticalSection (&GLB_DbRetryConnectCS);
    }
    break;

    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
    case DLL_PROCESS_DETACH:
    default:
      // Empty
    break;

  } // switch ( ReasonForCall )

  return TRUE;

} // DllMain

//------------------------------------------------------------------------------
// PURPOSE : Permutes the given vector
// 
//  PARAMS :
//      - INPUT :
//          - pElement : @ Vector
//          - NumElements : Number of elements that contain the vector
//          - SizeElement : Size element of the vector
//
//      - OUTPUT :
//          - pElement : Vector random permute
//
// RETURNS :
//      - COMMON_OK
//      - COMMON_ERROR
//
//   NOTES :
//

COMMONMISC_API DWORD WINAPI  Common_PermuteVector (void   * pElement, 
                                                   DWORD  NumElements,
                                                   DWORD  SizeElement)
{
  DWORD   _idx_elem;
  DWORD   _rnd_elem;
  BYTE    * p_item_tmp;
  BYTE    * p_base;
  BYTE    _aux_item_buffer [100];

  if ( sizeof (_aux_item_buffer) < SizeElement )
  {
    return COMMON_ERROR;
  }

  p_item_tmp = &_aux_item_buffer[0];

  p_base = (BYTE *) pElement;

  // Permute the position of all elements of the vector.
  for ( _idx_elem = 0; _idx_elem < NumElements; _idx_elem++ )
  {
    // Get a random element (position into the vector)
    _rnd_elem  = GetRandomInRange (NumElements);
    // Swap values
    memcpy (p_item_tmp, &p_base[_idx_elem * SizeElement], SizeElement);
    memcpy (&p_base[_idx_elem * SizeElement], &p_base[_rnd_elem * SizeElement], SizeElement);
    memcpy (&p_base[_rnd_elem * SizeElement], p_item_tmp, SizeElement);
  } // for

  return COMMON_OK;
} // Common_PermuteVector

//------------------------------------------------------------------------------
// PURPOSE : Convert string mac address to mac address common structure
// 
//  PARAMS :
//      - INPUT :
//          - pMacString
//          - pMacAddress
//
//      - OUTPUT :
//
// RETURNS :
//      - COMMON_OK
//      - COMMON_ERROR
//
//   NOTES :
//
COMMONMISC_API DWORD WINAPI  Common_StringToMacAddress (TCHAR                     * pMacString, 
                                                        TYPE_COMMON_MAC_ADDRESS   * pMacAddress)
{
  ZeroMemory (pMacAddress, sizeof (TYPE_COMMON_MAC_ADDRESS));

  //
  // TODO
  //

  return COMMON_OK; 
} // Common_StringToMacAddress

//------------------------------------------------------------------------------
// PURPOSE : Convert mac address common structure to string mac address
// 
//  PARAMS :
//      - INPUT :
//          - pMacString
//          - pMacAddress
//
//      - OUTPUT :
//
// RETURNS :
//      - COMMON_OK
//      - COMMON_ERROR
//
//   NOTES :
//
COMMONMISC_API DWORD WINAPI  Common_MacAddressToString (TYPE_COMMON_MAC_ADDRESS   * pMacAddress,
                                                        TCHAR                     * pMacString)
{

  _stprintf (pMacString, _T("%02X-%02X-%02X-%02X-%02X-%02X"),
            pMacAddress->data[0],
            pMacAddress->data[1],
            pMacAddress->data[2],
            pMacAddress->data[3],
            pMacAddress->data[4],
            pMacAddress->data[5]);

  return COMMON_OK; 
} // Common_MacAddressToString

//------------------------------------------------------------------------------
//  PRIVATE FUNCTIONS IMPLEMENTATION 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PURPOSE : Returns a random number 
// 
//  PARAMS :
//      - INPUT :
//          - UpperBound : Upper limit to number
//
//      - OUTPUT :
//
// RETURNS :
//    - Random number
// 
//   NOTES :
//

DWORD         GetRandomInRange (DWORD UpperBound)
{
  DWORD _rnd;

  // Random number
  _rnd = Common_Random ();
  // Modulus
  _rnd = _rnd % UpperBound;

  return _rnd;
} // GetRandomInRange


