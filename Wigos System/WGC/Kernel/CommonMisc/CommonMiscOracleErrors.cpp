//------------------------------------------------------------------------------
// Copyright � 2002 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: CommonMiscOracleErrors.pc
//   DESCRIPTION: Functions and Methods to handle Oracle Errors
//        AUTHOR: Andreu Juli�
// CREATION DATE: 04-SET-2002 
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ -----------------------------------------------------------
// 04-SET-2002 AJQ    Management of the Oracle Errors moved here.
//-------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  INCLUDES 
//------------------------------------------------------------------------------

#define INCLUDE_COMMON_MISC
#define INCLUDE_NLS_API
#define INCLUDE_PRIVATE_LOG
#define INCLUDE_ALARMS
#include "CommonDef.h"
#include "CommonMiscInternals.h"

//------------------------------------------------------------------------------
//  PRIVATE CONSTANTS 
//------------------------------------------------------------------------------

#define MAX_SQL_CODES 100

#define CHAR_NUL    0
#define CHAR_CR     10
#define CHAR_LF     13  
#define CHAR_BLANK  ' '
#define CHAR_DOT    '.'

//------------------------------------------------------------------------------
//  PRIVATE DATATYPES 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PRIVATE DATA STRUCTURES 
//------------------------------------------------------------------------------
////////////////////static time_t GLB_LastNotLoggedMsg = (time_t) 0;
////////////////////
////////////////////static DWORD  GLB_NumSqlCodes = 0;
////////////////////static DWORD  GLB_SqlCode [MAX_SQL_CODES];
////////////////////static time_t GLB_LastLogTime [MAX_SQL_CODES];
////////////////////
////////////////////BOOL LogSqlCode (DWORD SqlCode)
////////////////////{
////////////////////  DWORD _idx;
////////////////////  
////////////////////  for ( _idx = 0; _idx < GLB_NumSqlCodes; _idx++ )
////////////////////  {
////////////////////    if ( GLB_SqlCode[_idx] == SqlCode )
////////////////////    {
////////////////////      if ( difftime (time (NULL), GLB_LastLogTime[_idx]) > 300 )
////////////////////      {
////////////////////        GLB_LastLogTime[_idx] = time (NULL);
////////////////////        
////////////////////        return TRUE;
////////////////////      }
////////////////////      else
////////////////////      {
////////////////////        return FALSE;
////////////////////      } // if
////////////////////    } // if
////////////////////  } // for 
////////////////////  
////////////////////  if ( GLB_NumSqlCodes < MAX_SQL_CODES )
////////////////////  {
////////////////////    _idx = GLB_NumSqlCodes;
////////////////////    GLB_NumSqlCodes = GLB_NumSqlCodes + 1;
////////////////////    
////////////////////    GLB_SqlCode[_idx] = SqlCode;
////////////////////    GLB_LastLogTime[_idx] = time (NULL);
////////////////////  }
////////////////////
////////////////////  return TRUE;  
////////////////////}

//------------------------------------------------------------------------------
//  PRIVATE FUNCTION PROTOTYPES 
//------------------------------------------------------------------------------

//////////////////static VOID OracleProcessMessage (DWORD Len, CHAR *pOracleMsg);
//////////////////
//////////////////static VOID OracleGetLastError (SQL_CONTEXT SqlContext, 
//////////////////                                WORD        ErrorLength,
//////////////////                                TCHAR       * pError);
//////////////////
//////////////////static VOID OracleGetLastQuery (SQL_CONTEXT SqlContext, 
//////////////////                                WORD        QueryLength,
//////////////////                                TCHAR       * pQuery);
//////////////////                                                                
//////////////////static VOID OracleErrorToAlarm (TCHAR       * pModuleName, 
//////////////////                                TCHAR       * pFunctionName,
//////////////////                                SQL_CONTEXT SqlContext,
//////////////////                                int         SqlCode);
                                
 //------------------------------------------------------------------------------
//  PUBLIC FUNCTIONS IMPLEMENTATION 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PURPOSE: Log the oracle error and the query
// 
//  PARAMS:
//      - INPUT:
//          - pModuleName:    The module name
//          - pFunctionName:  The function name
//          - SqlContext:     The SQL Context
//          - SqlCode:        The error code
//          - RollBack:       Flag indicating whether or not to perform a rollback on failure
//
//      - OUTPUT: None
//
//  RETURNS:
//          - COMMON_OK
//          - COMMON_ERROR_*
//

COMMONMISC_API DWORD WINAPI Common_OracleError(TCHAR       * pModuleName, 
                                               TCHAR       * pFunctionName,
                                               SQL_CONTEXT SqlContext,
                                               int         SqlCode,
                                               BOOL        Rollback)
{
  return COMMON_ERROR;

  ////////////////////EXEC SQL CONTEXT USE :(SqlContext);
  ////////////////////TCHAR  _function_name [] = _T("Common_OracleError");
  ////////////////////TCHAR  _msg    [MAX_ORACLE_MSG_LEN + 1];
  ////////////////////TCHAR  _param3 [NLS_MSG_PARAM_MAX_LENGTH + 1];
  ////////////////////TCHAR  _param4 [NLS_MSG_PARAM_MAX_LENGTH + 1];
  ////////////////////TCHAR  _param5 [NLS_MSG_PARAM_MAX_LENGTH + 1];
  ////////////////////DWORD  _rc;
  ////////////////////BOOL   _send_alarm;
  ////////////////////BOOL   _log_error;
  ////////////////////
  ////////////////////_send_alarm = FALSE;
  ////////////////////_log_error  = FALSE;
  ////////////////////  
  ////////////////////switch ( SqlCode )
  ////////////////////{
  ////////////////////  case SQLCODE_OK:                // Ok
  ////////////////////    _rc = COMMON_OK;
  ////////////////////  break;
  ////////////////////  
  ////////////////////  case SQLCODE_NOT_FOUND:         // Not Found
  ////////////////////    _rc = COMMON_ERROR_NOT_FOUND;
  ////////////////////  break;

  ////////////////////  case SQLCODE_NOT_LOGGED:        // Not Logged On
  ////////////////////  case SQLCODE_NOT_CONNECTED:     // Not Connected to Oracle
  ////////////////////  case SQLCODE_END_OF_FILE:       // End Of File
  ////////////////////  case SQLCODE_TNS_12560:         // TNS:protocol adapter error. (This happens when the cable network is unplugged).
  ////////////////////  case SQLCODE_CONNECT_FAILED:    // Connect failed because target host or object does not exist.
  ////////////////////    Rollback = FALSE;
  ////////////////////    _log_error = LogSqlCode (SqlCode);
  ////////////////////    _rc = COMMON_ERROR_DB_NOT_CONNECTED;
  ////////////////////  break;
  ////////////////////  
  ////////////////////  case SQLCODE_TNS_12154:         // ORA-12154: TNS:could not resolve service name
  ////////////////////    Rollback = FALSE;
  ////////////////////    _log_error = TRUE;
  ////////////////////    _rc = COMMON_ERROR_DB_FATAL;
  ////////////////////  break;
  ////////////////////  
  ////////////////////  default:
  ////////////////////    _send_alarm = TRUE;
  ////////////////////    _log_error  = TRUE;
  ////////////////////    _rc = COMMON_ERROR_DB;
  ////////////////////  break;
  ////////////////////} // switch SqlCode
  ////////////////////
  ////////////////////if ( _log_error == TRUE ) // Log Error
  ////////////////////{
  ////////////////////  // Write the error text to the log ...
  ////////////////////  OracleGetLastError (SqlContext, MAX_ORACLE_MSG_LEN, _msg);

  ////////////////////  // Split the error message
  ////////////////////  NLS_SplitString (_msg, _param4, _param5);

  ////////////////////  _stprintf (_param3, _T("%d"), SqlCode);
  ////////////////////  Common_LoggerMsg (NLS_ID_LOGGER(4), pModuleName, pFunctionName,
  ////////////////////                    _param3, _param4, _param5);
  ////////////////////                    
  ////////////////////  // Split the error message
  ////////////////////  NLS_SplitString (_msg, _param3, _param4, _param5);
  ////////////////////  PrivateLog (5,             // Message Id
  ////////////////////              pModuleName,   // Module name
  ////////////////////              pFunctionName, // Function name
  ////////////////////              _param3,       // Message's 1st. parameter
  ////////////////////              _param4,       // Message's 2nd. parameter
  ////////////////////              _param5);      // Message's 3rd. parameter
  ////////////////////  
  ////////////////////  // Write the query to the log ...
  ////////////////////  OracleGetLastQuery (SqlContext, MAX_ORACLE_MSG_LEN, _msg);
  ////////////////////                      
  ////////////////////  // Write the query if it exists (ex. Connec/Commit don't provide a query)
  ////////////////////  if ( _tcslen (_msg) > 0 ) 
  ////////////////////  { 
  ////////////////////    // Split the SQL statement
  ////////////////////    NLS_SplitString (_msg, _param3, _param4, _param5);
  ////////////////////    Common_LoggerMsg (NLS_ID_LOGGER(6), pModuleName, pFunctionName, _param3, _param4, _param5);
  ////////////////////                    
  ////////////////////    PrivateLog (5,             // Message Id
  ////////////////////                pModuleName,   // Module name
  ////////////////////                pFunctionName, // Function name
  ////////////////////                _param3,       // Message's 1st. parameter
  ////////////////////                _param4,       // Message's 2nd. parameter
  ////////////////////                _param5);      // Message's 3rd. parameter
  ////////////////////  } // if
  ////////////////////           
  ////////////////////  if ( Rollback == TRUE )
  ////////////////////  {
  ////////////////////    if ( SqlCode != SQLCODE_NOT_LOGGED &&
  ////////////////////         SqlCode != SQLCODE_NOT_CONNECTED )
  ////////////////////    {
  ////////////////////      // Always returs Ok
  ////////////////////      Common_Rollback (SqlContext);
  ////////////////////    } // if
  ////////////////////  } // if
  ////////////////////} // if 
  ////////////////////
  ////////////////////return _rc;  
} // Common_OracleError

////////////////////////------------------------------------------------------------------------------
//////////////////////// PURPOSE: Get the last error text
//////////////////////// 
////////////////////////  PARAMS:
////////////////////////      - INPUT:
////////////////////////          - SqlContext:     The SQL Context
////////////////////////          - ErrorLength: The buffer length
////////////////////////
////////////////////////      - OUTPUT:
////////////////////////          - pError:      The error text
//////////////////////// 
//////////////////////static VOID OracleGetLastError (SQL_CONTEXT SqlContext, 
//////////////////////                                WORD        ErrorLength,
//////////////////////                                TCHAR       * pError)
//////////////////////{
//////////////////////  char   _str_sql_error [MAX_ORACLE_MSG_LEN + 1];
//////////////////////  size_t _msg_len;
//////////////////////  size_t _buffer_len;
//////////////////////  
//////////////////////  // Obtain the error message
//////////////////////  _buffer_len = MAX_ORACLE_MSG_LEN - 1;
//////////////////////  sqlglmt (SqlContext, _str_sql_error, &_buffer_len, &_msg_len);
//////////////////////
//////////////////////  // Remove CrLf
//////////////////////  OracleProcessMessage (_msg_len, _str_sql_error);
//////////////////////    
//////////////////////  // Convert to TCHAR
//////////////////////  Common_MB2WC (_str_sql_error, _msg_len, pError, ErrorLength);
//////////////////////  
//////////////////////} // OracleGetLastError
//////////////////////
////////////////////////------------------------------------------------------------------------------
//////////////////////// PURPOSE: Get the last executed query
//////////////////////// 
////////////////////////  PARAMS:
////////////////////////      - INPUT:
////////////////////////          - SqlContext:     The SQL Context
////////////////////////          - QueryLength: The buffer length
////////////////////////
////////////////////////      - OUTPUT:
////////////////////////          - pQuery:      The error text
////////////////////////
//////////////////////static VOID OracleGetLastQuery (SQL_CONTEXT SqlContext, 
//////////////////////                                WORD        QueryLength,
//////////////////////                                TCHAR       * pQuery)
//////////////////////{
//////////////////////  char   _str_sql_statement [MAX_ORACLE_MSG_LEN + 1];
//////////////////////  size_t _buffer_len;
//////////////////////  size_t _function_code;
//////////////////////  
//////////////////////  // Obtain the last executed statement
//////////////////////  _buffer_len = MAX_ORACLE_MSG_LEN - 1;
//////////////////////  SQLStmtGetText (SqlContext, _str_sql_statement, &_buffer_len, &_function_code);
//////////////////////
//////////////////////  if ( _buffer_len > 0 )
//////////////////////  {
//////////////////////    OracleProcessMessage (_buffer_len, _str_sql_statement);
//////////////////////
//////////////////////    // Convert to TCHAR
//////////////////////    Common_MB2WC (_str_sql_statement, _buffer_len, pQuery, QueryLength);
//////////////////////  }
//////////////////////  else
//////////////////////  {
//////////////////////    memset (pQuery, 0, sizeof (TCHAR) * QueryLength);
//////////////////////  } // if
//////////////////////  
//////////////////////} // OracleGetLastQuery
//////////////////////
////////////////////////------------------------------------------------------------------------------
//////////////////////// PURPOSE: Remove the CrLf characters from the Oracle Message and ensures that
////////////////////////          the message is null terminated.
//////////////////////// 
////////////////////////  PARAMS:
////////////////////////      - INPUT:
////////////////////////          - Len:        The message length
////////////////////////
////////////////////////      - INPUT/OUTPUT:
////////////////////////          - pOracleMsg: The message itself
////////////////////////
//////////////////////
//////////////////////static void OracleProcessMessage (DWORD Len, CHAR *pOracleMsg)
//////////////////////{
//////////////////////  DWORD _idx;
//////////////////////  
//////////////////////  if ( Len == 0 )
//////////////////////  {
//////////////////////    return;
//////////////////////  } // if
//////////////////////
//////////////////////  // Ensure that the message is null terminated
//////////////////////  pOracleMsg[Len-1] = CHAR_NUL;
//////////////////////  
//////////////////////  // Replace CrLf
//////////////////////  for ( _idx = 0; _idx < Len; _idx++ )
//////////////////////  {
//////////////////////    switch ( pOracleMsg[_idx] )
//////////////////////    {
//////////////////////      case CHAR_LF:
//////////////////////        pOracleMsg[_idx] = CHAR_BLANK;  // CHAR_DOT
//////////////////////      break;
//////////////////////      
//////////////////////      case CHAR_CR:
//////////////////////        pOracleMsg[_idx] = CHAR_BLANK;  
//////////////////////      break;
//////////////////////      
//////////////////////      default:
//////////////////////      break;
//////////////////////    } // switch 
//////////////////////  } // for
//////////////////////  
//////////////////////  for ( _idx = Len - 1; _idx >= 0; _idx-- )
//////////////////////  {
//////////////////////    if ( pOracleMsg[_idx] == CHAR_BLANK )
//////////////////////    {
//////////////////////      pOracleMsg[_idx] = CHAR_NUL;
//////////////////////    }
//////////////////////    else
//////////////////////    {
//////////////////////      break;
//////////////////////    } // if
//////////////////////  } // for
//////////////////////  
//////////////////////} // OracleProcessMessage
//////////////////////
////////////////////////------------------------------------------------------------------------------
//////////////////////// PURPOSE: Remove the CrLf characters from the Oracle Message and ensures that
////////////////////////          the message is null terminated.
//////////////////////// 
////////////////////////  PARAMS:
////////////////////////      - INPUT:
////////////////////////          - Len:        The message length
////////////////////////
////////////////////////      - INPUT/OUTPUT:
////////////////////////          - pOracleMsg: The message itself
////////////////////////
//////////////////////
//////////////////////static VOID OracleErrorToAlarm (TCHAR       * pModuleName, 
//////////////////////                                TCHAR       * pFunctionName,
//////////////////////                                SQL_CONTEXT SqlContext,
//////////////////////                                int         SqlCode)
//////////////////////{
//////////////////////  EXEC SQL CONTEXT USE :(SqlContext);
//////////////////////  TCHAR         _function_name [] = _T("OracleErrorToAlarm");
//////////////////////  TCHAR         _error_msg [MAX_ORACLE_MSG_LEN + 1];
//////////////////////  TCHAR         _param2 [NLS_MSG_PARAM_MAX_LENGTH + 1];
//////////////////////  TCHAR         _param3 [NLS_MSG_PARAM_MAX_LENGTH + 1];
//////////////////////  TCHAR         _param4 [NLS_MSG_PARAM_MAX_LENGTH + 1];
//////////////////////  TCHAR         _param5 [NLS_MSG_PARAM_MAX_LENGTH + 1];
//////////////////////  BOOL          _send_alarm;
//////////////////////    
//////////////////////  switch ( SqlCode )
//////////////////////  {
//////////////////////    case SQLCODE_OK:            // Ok
//////////////////////    case SQLCODE_NOT_FOUND:     // Not Found
//////////////////////    case SQLCODE_NOT_LOGGED:    // Not Logged On
//////////////////////    case SQLCODE_NOT_CONNECTED: // Not Connected to Oracle
//////////////////////      _send_alarm = FALSE;
//////////////////////    break;
//////////////////////    
//////////////////////    default:
//////////////////////      _send_alarm = TRUE;
//////////////////////    break;
//////////////////////  } // switch
//////////////////////
//////////////////////  if ( _send_alarm == TRUE )
//////////////////////  {  
//////////////////////    // Write the error text to the log ...
//////////////////////    OracleGetLastError (SqlContext, MAX_ORACLE_MSG_LEN, _error_msg);
//////////////////////    
//////////////////////    // Split the Error Message
//////////////////////    NLS_SplitString (_error_msg, _param3, _param4, _param5);
//////////////////////          
//////////////////////    // Sql Error Code to string      
//////////////////////    _stprintf (_param2, _T("%d"), SqlCode);
//////////////////////    
//////////////////////    // "Error=%2 llamando a la BD (App=%1). %3%4%5"
//////////////////////    Common_Alarm (0, 0, 0, ALARM_PRIORITY_SYSTEM, TRUE, SqlContext,
//////////////////////                  NLS_ID_LOGGER(4),
//////////////////////                  pModuleName, _param2, _param3, _param4, _param5);
//////////////////////    
//////////////////////  }// if
//////////////////////  
//////////////////////} // OracleErrorToAlarm
//////////////////////

//------------------------------------------------------------------------------
// PURPOSE: 
// 
//  PARAMS:
//      - INPUT:
//      - OUTPUT: None
//
//  RETURNS:
//

COMMONMISC_API BOOL  WINAPI Common_IsConnected  (SQL_CONTEXT SqlCtx)
{
  return TRUE;

  //////////////////TCHAR  _msg [MAX_ORACLE_MSG_LEN + 1];
  //////////////////int    _sql_code;
  //////////////////int    _int_rc;
  //////////////////
  //////////////////if ( SqlCtx == NULL )
  //////////////////{
  //////////////////  return FALSE;
  //////////////////}
  ////////////////// 
  //////////////////OracleGetLastError (SqlCtx, MAX_ORACLE_MSG_LEN, _msg);

  //////////////////if ( _tcslen (_msg) == 0 )
  //////////////////{
  //////////////////  return TRUE;
  //////////////////}

  //////////////////_int_rc = _stscanf (_msg, _T("ORA%d"), &_sql_code);
  //////////////////if ( _int_rc == 1 )
  //////////////////{
  //////////////////  switch ( _sql_code )
  //////////////////  {
  //////////////////    case SQLCODE_NOT_LOGGED:        // Not Logged On
  //////////////////    case SQLCODE_NOT_CONNECTED:     // Not Connected to Oracle
  //////////////////    case SQLCODE_END_OF_FILE:       // End Of File
  //////////////////    case SQLCODE_TNS_12560:         // TNS:protocol adapter error. (This happens when the cable network is unplugged).
  //////////////////    case SQLCODE_CONNECT_FAILED:    // Connect failed because target host or object does not exist.
  //////////////////    case SQLCODE_TNS_12154:         // ORA-12154: TNS:could not resolve service name
  //////////////////      return FALSE;
  //////////////////    break;
  //////////////////    
  //////////////////    default:
  //////////////////      return TRUE;
  //////////////////    break;
  //////////////////  } // switch SqlCode
  //////////////////}

  //////////////////return FALSE;

} // Common_IsConnected

