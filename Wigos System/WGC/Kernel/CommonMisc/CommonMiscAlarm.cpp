//------------------------------------------------------------------------------
// Copyright � 2002 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: CommonMiscAlarm.cpp
//   DESCRIPTION: Functions and Methods to handle the CommonMiscAlarm.
//        AUTHOR: Andreu Juli�
// CREATION DATE: 14-MAY-2002
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ -----------------------------------------------------------
// 14-MAY-2002 AJQ    Initial draft.
// 
//-------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  INCLUDES 
//------------------------------------------------------------------------------

#define INCLUDE_COMMON_MISC
#define INCLUDE_QUEUE_API
#define INCLUDE_IPC_API
#define INCLUDE_NLS_API
#define INCLUDE_PRIVATE_LOG
#define INCLUDE_ALARMS

#include "CommonDef.h"
#include "CommonMiscInternals.h"


//------------------------------------------------------------------------------
//  PRIVATE CONSTANTS 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PRIVATE DATATYPES 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PRIVATE DATA STRUCTURES 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PRIVATE FUNCTION PROTOTYPES 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PUBLIC FUNCTIONS IMPLEMENTATION 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PURPOSE: Handle Alarms
//          
// 
//  PARAMS:
//      - INPUT:  
//          - ClientId01: Client Id - 0
//          - ClientId01: Client Id - 1
//          - ClientId01: Client Id - 2
//          - Priority: Priority
//          - Activate: Whether the alarm is active or no
//          - SQL_CONTEXT: Sql Context
//          - pParam1: parameter 1 for NLS.
//          - pParam2: parameter 2 for NLS.
//          - pParam3: parameter 3 for NLS.
//          - pParam4: parameter 4 for NLS.
//          - pParam5: parameter 5 for NLS.
//      - OUTPUT: None
//          
// RETURNS: Nothing
//      
//
//   NOTES:


COMMONMISC_API void WINAPI Common_Alarm  (DWORD       ClientId0,
                                          DWORD       ClientId1,
                                          DWORD       ClientId2,
                                          DWORD       Priority,
                                          BOOL        Activate,
                                          SQL_CONTEXT SqlContext,
                                          DWORD       NlsId,
                                          TCHAR       * pParam1,
                                          TCHAR       * pParam2,
                                          TCHAR       * pParam3,
                                          TCHAR       * pParam4,
                                          TCHAR       * pParam5)
{
  TCHAR             _function_name [] = _T("Common_Alarm");
  static DWORD      _alarm_mgr_node_instance_id = IPC_NODE_UNKNOWN;
  API_INPUT_PARAMS  _api_input;
  API_OUTPUT_PARAMS _api_output;
  IPC_IUD_SEND      _ipc_iud;
  WORD              _rc;
  TCHAR             _rc_str [NLS_MSG_PARAM_MAX_LENGTH];
  DWORD             _alarm_id;
  SQL_CONTEXT       _sql_context;

  // AJQ, 02-SET-2002, Broadcast the alarm 
  //                   Now the local node instance id is not used.
  if ( _alarm_mgr_node_instance_id == IPC_NODE_UNKNOWN )
  {
    if ( Common_IpcGetNodeInstanceId (ALARM_MGR_NODE_NAME, 
                                      &_alarm_mgr_node_instance_id, 
                                      FALSE) == FALSE )
    {
      PrivateLog (2, MODULE_NAME, _function_name, _T("FALSE"), _T("Common_IpcGetNodeInstanceId"));

      return;
    } // if
  } // if

  // INPUT PARAMS:
  //   - API
  _api_input.control_block = sizeof (API_INPUT_PARAMS);
  _api_input.event_object = NULL;
  _api_input.mode = API_MODE_NO_WAIT;
  _api_input.function_code = IPC_CODE_SEND;
  //   - IPC
  _ipc_iud.control_block = sizeof (IPC_IUD_SEND);
  _ipc_iud.msg_id = 0; 
  _ipc_iud.msg_status = 0; 
  _ipc_iud.source_node_inst_id = IPC_NODE_UNKNOWN;

  // Get Alarm Mgr Id
  _ipc_iud.target_node_inst_id = _alarm_mgr_node_instance_id;

  if ( Activate == TRUE )
  {
    TYPE_ALARM_ACTIVATION * p_alarm;

    switch ( Priority )
    {
      case ALARM_PRIORITY_CRITICAL:
      case ALARM_PRIORITY_URGENT:
        _alarm_id = ALARM_ID_NOT_SET;
      break;

      case ALARM_PRIORITY_SYSTEM:
      case ALARM_PRIORITY_EVENT:
      case ALARM_PRIORITY_OPERATIVE:
        _sql_context = SqlContext;

        _alarm_id = 1;

      break;

      default:
        // Unknown Priority
        return;
      break;
    } // switch

    p_alarm = (TYPE_ALARM_ACTIVATION *) _ipc_iud.user_buffer;

    _ipc_iud.msg_cmd = ALARM_MSG_ACTIVATION;
    _ipc_iud.user_buffer_length = sizeof (TYPE_ALARM_ACTIVATION);

    p_alarm->control_block = sizeof (TYPE_ALARM_ACTIVATION);

    p_alarm->client_id[0] = ClientId0;
    p_alarm->client_id[1] = ClientId1;
    p_alarm->client_id[2] = ClientId2;
    p_alarm->priority     = Priority;
    p_alarm->alarm_id     = _alarm_id;  // Sets the AlarmId
    p_alarm->nls_code     = NlsId;
    _stprintf (p_alarm->nls_param[0], pParam1);
    _stprintf (p_alarm->nls_param[1], pParam2);
    _stprintf (p_alarm->nls_param[2], pParam3);
    _stprintf (p_alarm->nls_param[3], pParam4);
    _stprintf (p_alarm->nls_param[4], pParam5);
    
    Common_GetComputerName (p_alarm->computer, ALARM_TERMINAL_NAME_LEN);
  }
  else
  {
    TYPE_ALARM_DEACTIVATION  * p_alarm;

    p_alarm = (TYPE_ALARM_DEACTIVATION *) _ipc_iud.user_buffer;

    _ipc_iud.msg_cmd = ALARM_MSG_DEACTIVATION;
    _ipc_iud.user_buffer_length = sizeof (TYPE_ALARM_DEACTIVATION);

    p_alarm->control_block = sizeof (TYPE_ALARM_DEACTIVATION);

    p_alarm->client_id[0] = ClientId0;
    p_alarm->client_id[1] = ClientId1;
    p_alarm->client_id[2] = ClientId2;
    p_alarm->priority     = Priority;
  }

  // OUPUT PARAMS:
  //   - API
  _api_output.control_block = sizeof (API_OUTPUT_PARAMS);

  // API CALL
  _rc = Ipc_API (&_api_input,    // Input Params
                 &_ipc_iud,      // Input User Data
                 &_api_output,   // Output Params
                 NULL);          // Output User Data


  if ( _rc != API_STATUS_OK )
  {
    _stprintf (_rc_str, _T("%u"), _rc);
    PrivateLog (2, MODULE_NAME, _function_name, _rc_str, _T("Ipc_API"));
  }
}
