//-----------------------------------------------------------------------------
// Copyright � 2002 Win Systems Ltd.
//-----------------------------------------------------------------------------
// 
//   MODULE NAME: CommonMiscFile.cpp
//   DESCRIPTION: Functions and Methods of files to handle the CommonMisc dll.
//        AUTHOR: Alberto Cuesta
// CREATION DATE: 08-MAR-2002
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 08-MAR-2002 ACC    Initial draft.
// 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  INCLUDES 
//------------------------------------------------------------------------------

#define INCLUDE_COMMON_MISC
#define INCLUDE_NLS_API
#include "CommonDef.h"

#include "CommonMiscInternals.h"

//------------------------------------------------------------------------------
//  PRIVATE CONSTANTS 
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
//  PRIVATE DATATYPES 
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
//  PRIVATE DATA STRUCTURES 
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
//  PRIVATE FUNCTION PROTOTYPES 
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
//  PUBLIC FUNCTIONS IMPLEMENTATION 
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
// PURPOSE: Generic function to read from a file.
// 
//  PARAMS:
//      - INPUT:
//          - FileName
//          - DefaultValue: value to be returned if read operation failed
//          - DefaultLength: length of the default value
//
//      - OUTPUT: 
//          - OutputBuffer: address to store read value
//          - BufferLength: length of the buffer 
// 
// RETURNS: 
//          - COMMON_OK
//          - COMMON_ERROR
// 
//   NOTES:

COMMONMISC_API DWORD WINAPI Common_FileRead  (TCHAR  * pFileName, 
                                              void   * pDefaultValue, 
                                              WORD   DefaultLength, 
                                              void   * pOutputBuffer, 
                                              WORD   * pBufferLength)
{
  TCHAR     _function_name[] = _T("Common_FileRead");
  TCHAR     _log_param_1 [COMMON_LOGGER_PARAMETER_SIZE];
  HANDLE    _file_handle;
  DWORD     _bytes_to_read;
  DWORD     _bytes_read;
  DWORD     _rc;
  BOOL      _rc_read;

  _file_handle = CreateFile (pFileName,                                       // File name
                             GENERIC_READ | GENERIC_WRITE,                    // READ/WRITE access
                             0,                                               // Share mode = NO SHARED
                             NULL,                                            // Security attributes
                             OPEN_ALWAYS,                                     // Opens the file, if it exists. Creates it if not
                             FILE_ATTRIBUTE_NORMAL | FILE_FLAG_WRITE_THROUGH, // Flags and attributes
                             0);                                              // Template file

  if ( _file_handle == INVALID_HANDLE_VALUE )
  {
    // Unable to read or create file
    // Logger message
    _stprintf (_log_param_1, _T("%lu"), GetLastError ());
    Common_Logger (NLS_ID_LOGGER(2), _log_param_1, _T("CreateFile"), _T(""));

    * pBufferLength = 0;

    return COMMON_ERROR;
  } // if

  // Read from file
  _bytes_to_read = (DWORD) * pBufferLength;
  _bytes_read = 0;
  _rc_read = ReadFile (_file_handle, pOutputBuffer, _bytes_to_read, &_bytes_read, NULL);
  if ( ! _rc_read ||  _bytes_to_read != _bytes_read )
  {
    DWORD   _bytes_to_write;
    DWORD   _bytes_written;
    BOOL    _rc_write;

    // Reading error: empty file, not enough bytes

    // Set the file pointer to previous position to overwrite
    _rc = SetFilePointer (_file_handle, 0, NULL, FILE_BEGIN);
    if ( _rc == 0xFFFFFFFF )
    {
      // Error positioning file pointer
      _stprintf (_log_param_1, _T("%lu"), GetLastError ());
      Common_Logger (NLS_ID_LOGGER(2), _log_param_1, _T("SetFilePointer"), _T(""));

      // Close file
      CloseHandle (_file_handle);

      * pBufferLength = 0;

      return COMMON_ERROR;
    } // if

    // Error reading file
    _bytes_to_write = (DWORD) DefaultLength;
    _bytes_written = 0;
    _rc_write = WriteFile (_file_handle, pDefaultValue, _bytes_to_write, &_bytes_written, NULL);
    if ( ! _rc_write || _bytes_to_write != _bytes_written )
    {
      // Logger message
      _stprintf (_log_param_1, _T("%lu"), GetLastError ());
      Common_Logger (NLS_ID_LOGGER(2), _log_param_1, _T("WriteFile"), _T(""));

      // Close file
      CloseHandle (_file_handle);

      * pBufferLength = 0;

      return COMMON_ERROR;
    } // if

    // Close file
    CloseHandle (_file_handle);

    memcpy(pOutputBuffer, pDefaultValue, DefaultLength);
    * pBufferLength = DefaultLength;

    return COMMON_OK;
  } // if

  // Close file
  CloseHandle (_file_handle);

  return COMMON_OK;
} // Common_FileRead


//------------------------------------------------------------------------------
// PURPOSE: Generic function to write from a file.
// 
//  PARAMS:
//      - INPUT:
//          - FileName
//          - pInputBuffer
//          - BufferLength
//
//      - OUTPUT: 
// 
// RETURNS: 
//          - COMMON_OK
//          - COMMON_ERROR
// 
//   NOTES:

COMMONMISC_API DWORD WINAPI Common_FileWrite (TCHAR  * pFileName, 
                                              void   * pInputBuffer,  
                                              WORD   BufferLength)
{
  TCHAR     _function_name[] = _T("Common_FileWrite");
  TCHAR     _log_param_1 [COMMON_LOGGER_PARAMETER_SIZE];

  HANDLE    _file_handle;
  DWORD     _bytes_to_write;
  DWORD     _bytes_written;
  BOOL      _rc_write;

  // If file does not exist, it creates file
  _file_handle = CreateFile (pFileName,                                         // lpFileName
                             GENERIC_WRITE,                                     // dwDesiredAccess
                             0,                                                 // dwShareMode
                             NULL,                                              // lpSecurityAttributes
                             OPEN_ALWAYS,                                       // CreationDisposition
                             FILE_ATTRIBUTE_NORMAL | FILE_FLAG_WRITE_THROUGH,   // dwFlagsAndAttributes
                             0);                                                // hTemplateFile

  if ( _file_handle == INVALID_HANDLE_VALUE )
  {
    // Unable to create the file 
    _stprintf (_log_param_1, _T("%lu"), GetLastError ());
    Common_Logger (NLS_ID_LOGGER(2), _log_param_1, _T("CreateFile"), _T(""));

    return COMMON_ERROR;
  } // if

  _bytes_to_write = (DWORD) BufferLength;
  _bytes_written = 0;
  _rc_write = WriteFile (_file_handle, pInputBuffer, _bytes_to_write, &_bytes_written, NULL);
  if ( ! _rc_write || _bytes_to_write != _bytes_written )
  {
    // Logger message
    _stprintf (_log_param_1, _T("%lu"), GetLastError ());
    Common_Logger (NLS_ID_LOGGER(2), _log_param_1, _T("WriteFile"), _T(""));

    // Close file
    CloseHandle (_file_handle);

    return COMMON_ERROR;
  } // if

  // Close file
  CloseHandle (_file_handle);

  return COMMON_OK;
} // Common_FileWrite


//------------------------------------------------------------------------------
//  PRIVATE FUNCTIONS IMPLEMENTATION 
//------------------------------------------------------------------------------
