//------------------------------------------------------------------------------
// Copyright � 2002 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: CommonMiscIpc.cpp
//   DESCRIPTION: Functions and Methods to handle the CommonMiscIpc.
//        AUTHOR: Andreu Juli�
// CREATION DATE: 17-MAY-2002
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ -----------------------------------------------------------
// 17-MAY-2002 AJQ    Initial draft.
// 
//-------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  INCLUDES 
//------------------------------------------------------------------------------
#define INCLUDE_COMMON_MISC
#define INCLUDE_QUEUE_API
#define INCLUDE_IPC_API
#define INCLUDE_PRIVATE_LOG
#include "CommonDef.h"

#include "CommonMiscInternals.h"

//------------------------------------------------------------------------------
//  PRIVATE CONSTANTS 
//------------------------------------------------------------------------------
#define RETURN_CODE_STR_LEN         20
#define MAX_IPC_NODE_INDEX          100
#define IPC_NODE_INDEX_NOT_INDEXED  (-1)


//------------------------------------------------------------------------------
//  PRIVATE DATATYPES 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PRIVATE DATA STRUCTURES 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PRIVATE FUNCTION PROTOTYPES 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PUBLIC FUNCTIONS IMPLEMENTATION 
//------------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PURPOSE: Inits the IPC Node
//
//  PARAMS:
//     - INPUT:
//         pNodeName, the node name
//         BufferSize, 0 = Default
//
//     - OUTPUT:
//         pNodeId, the node id
//
//
// RETURNS:
//     - Nothing
//
// NOTES:
//

COMMONMISC_API BOOL WINAPI Common_IpcNodeInit (TCHAR * pNodeName, DWORD * pNodeId, DWORD BufferSize)
{
  TCHAR                   _function_name [] = _T("Common_IpcNodeInit");
  TCHAR                   _rc_str [RETURN_CODE_STR_LEN];
  API_INPUT_PARAMS        _api_input_params;
  API_OUTPUT_PARAMS       _api_output_params;
  IPC_IUD_INIT_LOCAL_NODE _ipc_iud_node_init;
  IPC_OUD_INIT_LOCAL_NODE _ipc_oud_node_init;
  WORD                    _rc;

  // INPUT PARAMS:
  //   - API
  _api_input_params.control_block = sizeof (API_INPUT_PARAMS);
  _api_input_params.event_object = NULL;
  _api_input_params.mode = API_MODE_SYNC;
  _api_input_params.function_code = IPC_CODE_INIT_LOCAL_NODE;
  //   - IPC
  _ipc_iud_node_init.control_block = sizeof (IPC_IUD_INIT_LOCAL_NODE);
  _stprintf (_ipc_iud_node_init.node_name, _T("%s"), pNodeName);
  _ipc_iud_node_init.input_buffer_size = BufferSize;

  // OUPUT PARAMS:
  //   - API
  _api_output_params.control_block = sizeof (API_OUTPUT_PARAMS);
  //   - IPC
  _ipc_oud_node_init.control_block = sizeof (IPC_OUD_INIT_LOCAL_NODE);
  

  // API CALL
  _rc = Ipc_API (&_api_input_params,    // Input Params
                 &_ipc_iud_node_init,   // Input User Data
                 &_api_output_params,   // Output Params
                 &_ipc_oud_node_init);  // Output User Data


  if ( _rc == API_STATUS_OK )
  {
    if ( _api_output_params.output_1 == IPC_STATUS_OK )
    {
      * pNodeId = _ipc_oud_node_init.node_inst_id ;

      return TRUE;  
    }
    else
    {
      _stprintf (_rc_str, _T("%u"), _api_output_params.output_1);
      PrivateLog (2, MODULE_NAME, _function_name, _rc_str, _T("Ipc_API"));
    } // if
  }
  else
  {
    _stprintf (_rc_str, _T("%u"), _rc);
    PrivateLog (2, MODULE_NAME, _function_name, _rc_str, _T("Ipc_API"));
  } // if

  return FALSE;
} // Common_IpcNodeInit

//-----------------------------------------------------------------------------
// PURPOSE: Gets node instance id
//
//  PARAMS:
//     - INPUT:
//      pNodeName: Node name
//
//     - OUTPUT:
//      pNodeId: The node instance id
//
//
// RETURNS:
//     - TRUE on success.
//
// NOTES:
//

COMMONMISC_API BOOL WINAPI Common_IpcGetNodeInstanceId (TCHAR * pNodeName, DWORD * pNodeId, BOOL LocalInstance)
{
  TCHAR                     _function_name [] = _T("Common_IpcGetNodeInstanceId");
  TCHAR                     _rc_str [RETURN_CODE_STR_LEN];
  API_INPUT_PARAMS          _api_input;
  API_OUTPUT_PARAMS         _api_output;
  IPC_IUD_GET_NODE_INST_ID  _ipc_iud;
  IPC_OUD_GET_NODE_INST_ID  _ipc_oud;
  WORD                      _rc;

  // INPUT PARAMS:
  //   - API
  _api_input.control_block = sizeof (API_INPUT_PARAMS);
  _api_input.event_object = NULL;
  _api_input.mode = API_MODE_SYNC;
  _api_input.function_code = IPC_CODE_GET_NODE_INST_ID;
  //   - IPC
  _ipc_iud.control_block = sizeof (IPC_IUD_GET_NODE_INST_ID);
  _stprintf (_ipc_iud.node_name, _T("%s"), pNodeName);
  _ipc_iud.local_instance = LocalInstance;

  // OUPUT PARAMS:
  //   - API
  _api_output.control_block = sizeof (API_OUTPUT_PARAMS);
  //   - IPC
  _ipc_oud.control_block = sizeof (IPC_OUD_GET_NODE_INST_ID);
  

  // API CALL
  _rc = Ipc_API (&_api_input,    // Input Params
                 &_ipc_iud,      // Input User Data
                 &_api_output,   // Output Params
                 &_ipc_oud);     // Output User Data

  if ( _rc == API_STATUS_OK )
  {
    if ( _api_output.output_1 == IPC_STATUS_OK )
    {
      * pNodeId = _ipc_oud.node_inst_id ;

      return TRUE;  
    } // if

    // Don't log the error ...
    // The caller may look for another local instance ({1st Local/Later Non local: ALARM_MGR},{COMM_MGRnn})
    return FALSE;
  } // if

  _stprintf (_rc_str, _T("%u"), _rc);
  PrivateLog (2, MODULE_NAME, _function_name, _rc_str, _T("Ipc_API"));

  return FALSE;
} // Common_IpcGetNodeInstanceId

//-----------------------------------------------------------------------------
// PURPOSE: Gets instance id data
//
//  PARAMS:
//     - INPUT:
//          - NodeInstId
//
//      - OUTPUT :
//          - pComputerId
//          - pNodeId
//
// RETURNS:
//     - TRUE on success.
//
// NOTES:
//

COMMONMISC_API BOOL WINAPI Common_IpcGetInstIdData (DWORD NodeInstId, 
                                                    WORD  * pComputerId,
                                                    WORD  * pNodeId)
{
  TCHAR                     _function_name [] = _T("Common_IpcGetInstIdData");
  TCHAR                     _rc_str [RETURN_CODE_STR_LEN];
  API_INPUT_PARAMS          _api_input;
  API_OUTPUT_PARAMS         _api_output;
  IPC_IUD_GET_INST_ID_DATA  _ipc_iud;
  IPC_OUD_GET_INST_ID_DATA  _ipc_oud;
  WORD                      _rc;

  // INPUT PARAMS:
  //   - API
  _api_input.control_block = sizeof (API_INPUT_PARAMS);
  _api_input.event_object = NULL;
  _api_input.mode = API_MODE_SYNC;
  _api_input.function_code = IPC_CODE_GET_INST_ID_DATA;
  //   - IPC
  _ipc_iud.control_block = sizeof (IPC_IUD_GET_INST_ID_DATA);
  _ipc_iud.node_inst_id = NodeInstId;

  // OUPUT PARAMS:
  //   - API
  _api_output.control_block = sizeof (API_OUTPUT_PARAMS);
  //   - IPC
  _ipc_oud.control_block = sizeof (IPC_OUD_GET_INST_ID_DATA);
  

  // API CALL
  _rc = Ipc_API (&_api_input,    // Input Params
                 &_ipc_iud,      // Input User Data
                 &_api_output,   // Output Params
                 &_ipc_oud);     // Output User Data

  if ( _rc == API_STATUS_OK )
  {
    if ( _api_output.output_1 == IPC_STATUS_OK )
    {
      *pComputerId = _ipc_oud.ipc_computer_id;
      *pNodeId = _ipc_oud.ipc_node_id;

      return TRUE;  
    } 

    return FALSE;
  } // if

  _stprintf (_rc_str, _T("%u"), _rc);
  PrivateLog (2, MODULE_NAME, _function_name, _rc_str, _T("Ipc_API"));

  return FALSE;
} // Common_IpcGetInstIdData

//-----------------------------------------------------------------------------
// PURPOSE: Gets node instance id
//
//  PARAMS:
//    - INPUT:
//      - pNodeName: Node name
//
//     - OUTPUT:
//      - pNumInstances:  The number of instances
//      - pNodeInstId:    The list of node instances
//      - pLocalInstance: Flag indicating whether or not is the local instance
//
// RETURNS:
//     - TRUE on success.
//     - FALSE on failure
//
// NOTES:
//
// AJQ, 09-DEC-2002

COMMONMISC_API BOOL WINAPI Common_IpcGetNodeInstanceIdList (TCHAR * pNodeName,
                                                            DWORD * pNumInstances,
                                                            DWORD * pCfgInstId,
                                                            DWORD * pNodeInstId, 
                                                            BOOL  * pLocalInstance)
{
  TCHAR                           _function_name [] = _T("Common_IpcGetNodeInstanceIdList");
  TCHAR                           _rc_str [RETURN_CODE_STR_LEN];
  API_INPUT_PARAMS                _api_input;
  API_OUTPUT_PARAMS               _api_output;
  IPC_IUD_GET_NODE_INST_ID_LIST   _ipc_iud;
  IPC_OUD_GET_NODE_INST_ID_LIST   _ipc_oud;
  WORD                            _rc;
  DWORD                           _idx;

  // Number of instances set to zero
  * pNumInstances = 0;

  // INPUT PARAMS:
  //   - API
  _api_input.control_block = sizeof (API_INPUT_PARAMS);
  _api_input.event_object = NULL;
  _api_input.mode = API_MODE_SYNC;
  _api_input.function_code = IPC_CODE_GET_NODE_INST_ID_LIST;
  //   - IPC
  _ipc_iud.control_block = sizeof (IPC_IUD_GET_NODE_INST_ID_LIST);
  _stprintf (_ipc_iud.node_name, _T("%s"), pNodeName);

  // OUPUT PARAMS:
  //   - API
  _api_output.control_block = sizeof (API_OUTPUT_PARAMS);
  //   - IPC
  _ipc_oud.control_block = sizeof (IPC_OUD_GET_NODE_INST_ID_LIST);
  

  // API CALL
  _rc = Ipc_API (&_api_input,    // Input Params
                 &_ipc_iud,      // Input User Data
                 &_api_output,   // Output Params
                 &_ipc_oud);     // Output User Data


  if ( _rc == API_STATUS_OK )
  {
    if ( _api_output.output_1 == IPC_STATUS_OK )
    {
      * pNumInstances = _ipc_oud.num_instances;
      for ( _idx = 0; _idx < _ipc_oud.num_instances; _idx++ )
      {
        pCfgInstId[_idx]     = _ipc_oud.cfg_inst_id[_idx]; 
        pNodeInstId[_idx]    = _ipc_oud.node_inst_id[_idx];
        pLocalInstance[_idx] = _ipc_oud.local_instance[_idx];         
      } // for

      return TRUE;  
    } // if

    // Don't log the error ...
    // The caller may look for another local instance ({1st Local/Later Non local: ALARM_MGR},{COMM_MGRnn})
    return FALSE;
  }

  _stprintf (_rc_str, _T("%u"), _rc);
  PrivateLog (2, MODULE_NAME, _function_name, _rc_str, _T("Ipc_API"));

  return FALSE;
} // Common_IpcGetNodeInstanceIdList

//-----------------------------------------------------------------------------
// PURPOSE: Gets local node instance id
//
//  PARAMS:
//     - INPUT:
//      pNodeBaseName:    The node base name (The full name can be: BaseName, BaseNamenn)
//
//     - OUTPUT:
//      pNodeName:        The full node name
//      pNodeIndex:       The node index (0: BaseName used, 1-99 when the sufix is appended, 
//      pNodeInstanceId:  The node instance id
//      
//
// RETURNS:
//     - TRUE on success.
//     - FALSE on failure
//
// NOTES:
//

COMMONMISC_API BOOL WINAPI Common_IpcGetLocalNodeInstanceId (TCHAR * pNodeBaseName, 
                                                             TCHAR * pNodeName, 
                                                             DWORD * pNodeIndex, 
                                                             DWORD * pNodeInstanceId)
{
  TCHAR _function_name [] = _T("Common_IpcGetLocalNodeInstanceId");
  TCHAR _node_name [IPC_NODE_NAME_LENGTH];
  DWORD _idx;
  DWORD _node_instance_id;
  BOOL  _bool_rc;


  // Build the node name
  _stprintf (_node_name, _T("%s"), pNodeBaseName);
  _bool_rc = Common_IpcGetNodeInstanceId (_node_name, &_node_instance_id, TRUE);

  if ( _bool_rc == TRUE )
  {
    _stprintf (pNodeName, _T("%s"), _node_name);
    *pNodeIndex      = IPC_NODE_INDEX_NOT_INDEXED;
    *pNodeInstanceId = _node_instance_id;

    return TRUE;
  } // if
  for ( _idx = 0; _idx < MAX_IPC_NODE_INDEX; _idx++ )
  {
    // Build the node name
    IpcBuildNodeName (pNodeBaseName, _idx, _node_name);
    // _stprintf (_node_name, _T("%s_%02ld"), pNodeBaseName, _idx);
    _bool_rc = Common_IpcGetNodeInstanceId (_node_name, &_node_instance_id, TRUE);

    if ( _bool_rc == TRUE )
    {
      _stprintf (pNodeName, _T("%s"), _node_name);
      *pNodeIndex      = _idx;
      *pNodeInstanceId = _node_instance_id;

      return TRUE;
    } // if
  } // for

  // Log Unexpected node name
  PrivateLog (6, MODULE_NAME, _function_name, _T("IPC Node Name"), pNodeBaseName);

  return FALSE;

} // Common_IpcGetLocalNodeInstanceId

//-----------------------------------------------------------------------------
// PURPOSE: Init/Stop the IPC, and wait till the send queue is empty
//
//  PARAMS:
//     - INPUT:
//      FunctionCode: IPC_CODE_MODULE_INIT
//                    IPC_CODE_MODULE_STOP
//                    IPC_CODE_WAIT_SEND_QUEUE_EMPTY
//
//     - OUTPUT: None
//
//
// RETURNS:
//     - TRUE on success.
//     - FALSE on failure
//
// NOTES:
//

COMMONMISC_API BOOL WINAPI Common_Ipc (WORD FunctionCode)
{
  TCHAR                     _function_name [] = _T("Common_Ipc");
  TCHAR                     _rc_str [RETURN_CODE_STR_LEN];
  API_INPUT_PARAMS          _api_input;
  API_OUTPUT_PARAMS         _api_output;
  WORD                      _rc;

  switch ( FunctionCode )
  {
    case IPC_CODE_MODULE_INIT:
    case IPC_CODE_MODULE_STOP:
    case IPC_CODE_WAIT_SEND_QUEUE_EMPTY:
    break;
    
    default:
      _stprintf (_rc_str, _T("%u"), FunctionCode);
      PrivateLog (6, MODULE_NAME, _function_name, _T("FunctionCode"), _rc_str);

      return FALSE;
    break;
  } // switch ( FunctionCode )

  // INPUT PARAMS:
  //   - API
  _api_input.control_block = sizeof (API_INPUT_PARAMS);
  _api_input.event_object = NULL;
  _api_input.mode = API_MODE_SYNC;
  _api_input.function_code = FunctionCode;
  // OUPUT PARAMS:
  //   - API
  _api_output.control_block = sizeof (API_OUTPUT_PARAMS);

  // API CALL
  _rc = Ipc_API (&_api_input,  // Input Params
                 NULL,         // Input User Data
                 &_api_output, // Output Params
                 NULL);        // Output User Data


  if ( _rc == API_STATUS_OK )
  {
    if ( _api_output.output_1 == IPC_STATUS_OK )
    {
      return TRUE;  
    } // if

    _stprintf (_rc_str, _T("%u"), _api_output.output_1);
    PrivateLog (2, MODULE_NAME, _function_name, _rc_str, _T("Ipc_API"));

    return FALSE;
  } // if

  _stprintf (_rc_str, _T("%u"), _rc);
  PrivateLog (2, MODULE_NAME, _function_name, _rc_str, _T("Ipc_API"));

  return FALSE;
} // Common_Ipc 

//-----------------------------------------------------------------------------
// PURPOSE: Wait till a message is received
//
//  PARAMS:
//     - INPUT:
//            - NodeInstanceId: Id of the node instance
//
//     - OUTPUT:         
//            - pMessage: The received message
//
//
// RETURNS:
//     - TRUE on success.
//     - FALSE on failure
//
// NOTES:
//

COMMONMISC_API BOOL WINAPI Common_IpcReceive (DWORD NodeInstanceId, IPC_OUD_RECEIVE * pMessage)
{
  TCHAR                     _function_name [] = _T("Common_IpcReceive");
  TCHAR                     _rc_str [RETURN_CODE_STR_LEN];
  API_INPUT_PARAMS          _api_input;
  API_OUTPUT_PARAMS         _api_output;
  IPC_IUD_RECEIVE           _ipc_iud_receive;
  WORD                      _rc;

  // INPUT PARAMS:
  //   - API
  _api_input.control_block = sizeof (API_INPUT_PARAMS);
  _api_input.event_object = NULL;
  _api_input.mode = API_MODE_SYNC;
  _api_input.function_code = IPC_CODE_RECEIVE;
  //   - IPC
  _ipc_iud_receive.control_block = sizeof (IPC_IUD_RECEIVE);
  _ipc_iud_receive.node_inst_id = NodeInstanceId;
  _ipc_iud_receive.timeout = IPC_TIMEOUT_INFINITE;
  // OUPUT PARAMS:
  //   - API
  _api_output.control_block = sizeof (API_OUTPUT_PARAMS);
  //   - IPC
  pMessage->control_block = sizeof (IPC_OUD_RECEIVE);

  // API CALL
  _rc = Ipc_API (&_api_input,       // Input Params
                 &_ipc_iud_receive, // Input User Data
                 &_api_output,      // Output Params
                 pMessage);         // Output User Data


  if ( _rc == API_STATUS_OK )
  {
    if ( _api_output.output_1 == IPC_STATUS_OK )
    {
      return TRUE;  
    } // if

    _stprintf (_rc_str, _T("%u"), _api_output.output_1);
    PrivateLog (2, MODULE_NAME, _function_name, _rc_str, _T("Ipc_API"));

    return FALSE;
  } // if

  _stprintf (_rc_str, _T("%u"), _rc);
  PrivateLog (2, MODULE_NAME, _function_name, _rc_str, _T("Ipc_API"));

  return FALSE;
} // Common_IpcReceive

//-----------------------------------------------------------------------------
// PURPOSE: Post a receive
//
//  PARAMS:
//     - INPUT: 
//          - NodeInstanceId: Id of the node instance
//          - handle
//
//     - OUTPUT:         
//          - pOutputParams: output params
//          - pMessage: The received message
//
//
// RETURNS:
//     - TRUE on success.
//     - FALSE on failure
//
// NOTES:
//

COMMONMISC_API BOOL WINAPI Common_IpcReceiveAsync (DWORD             NodeInstanceId, 
                                                   HANDLE            handle,
                                                   API_OUTPUT_PARAMS * pOutputParams,
                                                   IPC_OUD_RECEIVE   * pMessage)
{
  TCHAR                     _function_name [] = _T("Common_IpcReceive");
  TCHAR                     _rc_str [RETURN_CODE_STR_LEN];
  API_INPUT_PARAMS          _api_input;
  IPC_IUD_RECEIVE           _ipc_iud_receive;
  WORD                      _rc;

  // INPUT PARAMS:
  //   - API
  _api_input.control_block = sizeof (API_INPUT_PARAMS);
  _api_input.event_object = handle;
  _api_input.mode = API_MODE_ASYNC;
  _api_input.function_code = IPC_CODE_RECEIVE;
  //   - IPC
  _ipc_iud_receive.control_block = sizeof (IPC_IUD_RECEIVE);
  _ipc_iud_receive.node_inst_id = NodeInstanceId;
  _ipc_iud_receive.timeout = IPC_TIMEOUT_INFINITE;
  // OUPUT PARAMS:
  //   - API
  pOutputParams->control_block = sizeof (API_OUTPUT_PARAMS);
  //   - IPC
  pMessage->control_block = sizeof (IPC_OUD_RECEIVE);

  // API CALL
  _rc = Ipc_API (&_api_input,       // Input Params
                 &_ipc_iud_receive, // Input User Data
                 pOutputParams,     // Output Params
                 pMessage);         // Output User Data


  if ( _rc == API_STATUS_OK )
  {
    return TRUE;  
  } // if

  _stprintf (_rc_str, _T("%u"), _rc);
  PrivateLog (2, MODULE_NAME, _function_name, _rc_str, _T("Ipc_API"));

  return FALSE;
} // Common_IpcReceiveAsync

//-----------------------------------------------------------------------------
// PURPOSE: Sends a message
//
//  PARAMS:
//     - INPUT:         
//          - pMessage: The message to be sent
//
//     - OUTPUT:
//          - None.
//
//
// RETURNS:
//     - TRUE on success.
//     - FALSE on failure
//
// NOTES:
//

COMMONMISC_API BOOL WINAPI Common_IpcSend (IPC_IUD_SEND * pMessage)
{
  TCHAR                     _function_name [] = _T("Common_IpcSend");
  TCHAR                     _rc_str [RETURN_CODE_STR_LEN];
  API_INPUT_PARAMS          _api_input;
  API_OUTPUT_PARAMS         _api_output;
  WORD                      _rc;

  // INPUT PARAMS:
  //   - API
  _api_input.control_block = sizeof (API_INPUT_PARAMS);
  _api_input.event_object = NULL;
  _api_input.mode = API_MODE_NO_WAIT;
  _api_input.function_code = IPC_CODE_SEND;
  //   - IPC
  pMessage->control_block = sizeof (IPC_IUD_SEND);

  // OUPUT PARAMS:
  //   - API
  _api_output.control_block = sizeof (API_OUTPUT_PARAMS);

  // API CALL
  _rc = Ipc_API (&_api_input,       // Input Params
                 pMessage,          // Input User Data
                 &_api_output,      // Output Params
                 NULL);             // Output User Data

  if ( _rc == API_STATUS_OK )
  {
    // API_MODE_NO_WAIT: There are not any output to check
    return TRUE;  
  } // if

  _stprintf (_rc_str, _T("%u"), _rc);
  PrivateLog (2, MODULE_NAME, _function_name, _rc_str, _T("Ipc_API"));

  return FALSE;
} // Common_IpcSend

//-----------------------------------------------------------------------------
// PURPOSE: Obtains all the node instances for an indexed ipc node
//
//  PARAMS:
//    - INPUT:
//      - pNodeBaseName: The node base name
//      - NumNodes:      The number of nodes to be scanned 
//
//    - OUTPUT:
//      - pNodeInstanceId: The node instance id list
//
// RETURNS: Nothing
//
// NOTES:
//  - pNodeInstanceId[n] is set to:
//    - IPC_NODE_UNKNOWN, the node doesn't exist
//    - The node instance id
//

void Common_IpcGetNodeListXXXXX (TCHAR *pNodeBaseName, DWORD NumNodes, DWORD *pNodeInstanceId)
{
  TCHAR _function_name [] = _T("Common_IpcNodeList");
  DWORD _idx;
  DWORD _num_nodes;
  TCHAR _node_name [IPC_NODE_NAME_LENGTH + 1];
  BOOL  _bool_rc;
  DWORD _node_instance_id;

  // Set all the list to IPC_NODE_UNKNOWN
  _num_nodes = NumNodes;
  for ( _idx = 0; _idx < _num_nodes; _idx++ )
  {
    pNodeInstanceId[_idx] = IPC_NODE_UNKNOWN;  
  } // for

  // Limit the number of nodes
  if ( NumNodes > IPC_NODE_MAX_INDEX )
  {
    _num_nodes = IPC_NODE_MAX_INDEX;
  } // if

  // Query the node instance id for the list of nodes
  for ( _idx = 0; _idx < _num_nodes; _idx ++ )
  {
    // Build the node name
    IpcBuildNodeName (pNodeBaseName, _idx, _node_name);

    _bool_rc = Common_IpcGetNodeInstanceId (_node_name, &_node_instance_id, FALSE);
    if ( _bool_rc == TRUE )
    {
      pNodeInstanceId[_idx] = _node_instance_id;
    }// if
  } // for
} // Common_IpcGetNodeList


//-----------------------------------------------------------------------------
// PURPOSE: Obtains the ID�s of the alive node instances for an indexed ipc node
//
//  PARAMS:
//    - INPUT:
//      - pNodeBaseName: The node base name
//      - NumNodes:      The number of nodes to be scanned 
//
//    - OUTPUT:
//      - pNodeInstanceId: The node instance id list
//
// RETURNS: 
//      - TRUE:
//      - FALSE: 
//
// NOTES:
//

COMMONMISC_API BOOL WINAPI Common_IpcGetAliveNodeInstanceId (TCHAR *pNodeBaseName, 
                                                             DWORD NumNodes, 
                                                             DWORD *pNodeInstanceId)
{
  TCHAR _node_name [IPC_NODE_NAME_LENGTH + 1];
  BOOL  _bool_rc;
  DWORD _index;

  _bool_rc = FALSE;

  for ( _index = 0; _index < 100; _index++ )
  {
    IpcBuildNodeName (pNodeBaseName, _index, _node_name);

    _bool_rc = Common_IpcGetNodeInstanceId (_node_name, pNodeInstanceId, FALSE);
  
    if ( _bool_rc == TRUE )
    {
      break;
    } // if
  } // for

  return _bool_rc;
} // Common_IpcGetAliveNodeInstanceId


//-----------------------------------------------------------------------------
// PURPOSE: Obtains the ID�s of the node instances for an indexed ipc node
//
//  PARAMS:
//    - INPUT:
//      - pNodeBaseName: The node base name
//
//    - OUTPUT:
//      - pIpcNodeList: The Ipc Node List
//
// RETURNS: 
//      - TRUE:
//      - FALSE: 
//
// NOTES:
//

COMMONMISC_API BOOL WINAPI Common_IpcGetNodeList (TCHAR * pNodeBaseName, TYPE_IPC_NODE_LIST * pIpcNodeList)
{
  TCHAR             _function_name [] = _T("Common_IpcGetNodeList");

  BOOL              _bool_rc;
  TCHAR             _node_name [IPC_NODE_NAME_LENGTH + 1];
  TCHAR             _wdog_name [IPC_NODE_NAME_LENGTH + 1];
  DWORD             _node_index;
  DWORD             _idx;
  // Node & Wdog Instances
  DWORD             _num_node_instances;
  DWORD             _num_wdog_instances;
  DWORD             _node_inst_id       [IPC_MAX_INSTANCES_PER_NODE];
  DWORD             _node_cfg_inst_id   [IPC_MAX_INSTANCES_PER_NODE];
  BOOL              _node_is_local_inst [IPC_MAX_INSTANCES_PER_NODE];
  DWORD             _wdog_inst_id       [IPC_MAX_INSTANCES_PER_NODE];
  DWORD             _wdog_cfg_inst_id   [IPC_MAX_INSTANCES_PER_NODE];
  BOOL              _wdog_is_local_inst [IPC_MAX_INSTANCES_PER_NODE];

  memset (pIpcNodeList, 0, sizeof (TYPE_IPC_NODE_LIST));

  _idx = 0;

  for ( _node_index = 0; _node_index < IPC_NODE_MAX_INDEX; _node_index++ )
  {
    IpcBuildNodeName (pNodeBaseName, _node_index, _node_name);
    IpcBuildWatchdogNodeName (_node_name, _wdog_name);

    _bool_rc = TRUE;
    _bool_rc = _bool_rc && Common_IpcGetNodeInstanceIdList (_node_name, 
                                                            &_num_node_instances, 
                                                            _node_cfg_inst_id,
                                                            _node_inst_id, 
                                                            _node_is_local_inst);

    _bool_rc = _bool_rc && Common_IpcGetNodeInstanceIdList (_wdog_name, 
                                                            &_num_wdog_instances,
                                                            _wdog_cfg_inst_id,
                                                            _wdog_inst_id, 
                                                            _wdog_is_local_inst);

    if ( _bool_rc == TRUE )
    {
      // Node found ...
      if ( (_num_node_instances == 1) && (_num_wdog_instances == 1) )
      {
        pIpcNodeList->node_index[_idx]   = _node_cfg_inst_id[0];
        pIpcNodeList->node_inst_id[_idx] = _node_inst_id[0];
        pIpcNodeList->wdog_inst_id[_idx] = _wdog_inst_id[0];
        if ( _node_is_local_inst[0] )
        {
          pIpcNodeList->is_local[_idx]   = 1;
        }
        else
        {
          pIpcNodeList->is_local[_idx]   = 0;
        } // if
        _idx = _idx + 1;  
      }
      else
      {
        // Log Error
        return FALSE;
      }
    } // if
  } // for (_node_index)

  pIpcNodeList->num_nodes = _idx;

  return TRUE;

} // Common_IpcGetNodeList
