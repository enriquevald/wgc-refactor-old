//------------------------------------------------------------------------------
// Copyright � 2011 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WWP_MsgTable.cs
// 
//   DESCRIPTION: Process for Table message.
// 
//        AUTHOR: Joaquim Cid
// 
// CREATION DATE: 12-MAR-2013
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 12-MAR-2013 JCM    First release.
//                    Moved Set_ReportingFlag and Read_ReportingFlag.
// 10-JUN-2013 DDM    Fixed Bug #837
// 05-DIC-2014 DCS    Added Task Download Lcd Messages
// 05-DIC-2014 DCS    Fixed Bug WIG-1808: Task DownloadAlarmPatterns don't show pending changes in Site
// 14-JAN-2015 DCS    Added Task Upload Handpays
// 17-MAR-2015 ANM    Added Task Upload Sells & buys
// 31-MAR-2017 MS     WIGOS-411 Creditlines
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using WSI.Common;
using System.Data.SqlClient;
using System.Data;

namespace WSI.WWP
{
  public partial class WWP_MultiSiteTask
  {

    public static bool Set_ReportingFlag(TYPE_MULTISITE_SITE_TASKS TaskId, Boolean IsRunning, Int64 SequenceId, out Boolean EnabledAndRunning)
    {
      EnabledAndRunning = false;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (WWP_MultiSiteTask.Set_ReportingFlag(TaskId, true, -1, out EnabledAndRunning, _db_trx.SqlTransaction))
          {
            _db_trx.Commit();

            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    }

    public static bool Set_ReportingFlag(Int32 SiteId, Int32 Pending, TYPE_MULTISITE_SITE_TASKS TaskId, Boolean IsRunning, Int64 SequenceId, out Boolean EnabledAndRunning)
    {
      EnabledAndRunning = false;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (WWP_MultiSiteTask.Set_ReportingFlag(SiteId, Pending, TaskId, true, -1, out EnabledAndRunning, _db_trx.SqlTransaction))
          {
            _db_trx.Commit();

            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    }

    //------------------------------------------------------------------------------
    // PURPOSE : Sets Reporting Flag 
    //
    //  PARAMS :
    //      - INPUT :
    //          - ReportFlagValue:    New Value of Reporting Flag 
    //          - SequenceId          Last sequence id
    //          - Trx:                Transaction
    //      - OUTPUT :
    //          - None
    // RETURNS :
    //          - True:               Executed succesfully
    //          - False:              Executed unsuccesfully
    //
    //   NOTES : 
    public static bool Set_ReportingFlag(TYPE_MULTISITE_SITE_TASKS TaskId, Boolean IsRunning, Int64 SequenceId, out Boolean EnabledAndRunning, SqlTransaction Trx)
    {
      return Set_ReportingFlag(-1, -1, TaskId, IsRunning, SequenceId, out EnabledAndRunning, Trx);
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Sets Reporting Flag 
    //
    //  PARAMS :
    //      - INPUT :
    //          - ReportFlagValue:    New Value of Reporting Flag 
    //          - SequenceId          Last sequence id
    //          - Trx:                Transaction
    //      - OUTPUT :
    //          - None
    // RETURNS :
    //          - True:               Executed succesfully
    //          - False:              Executed unsuccesfully
    //
    //   NOTES : 
    public static bool Set_ReportingFlag(Int32 SiteId, Int32 Pending, TYPE_MULTISITE_SITE_TASKS TaskId, Boolean IsRunning, Int64 SequenceId, out Boolean EnabledAndRunning, SqlTransaction Trx)
    {

      StringBuilder _sb;
      StringBuilder _count;

      EnabledAndRunning = false;

      try
      {
        _sb = new StringBuilder();
        _count = new StringBuilder();

        if (SiteId == -1)
        {
          // Is Site
          switch (TaskId)
          {
            case TYPE_MULTISITE_SITE_TASKS.UploadPointsMovements:
              _count.Append("SELECT COUNT(*) FROM MS_SITE_PENDING_ACCOUNT_MOVEMENTS");
              break;

            case TYPE_MULTISITE_SITE_TASKS.UploadPersonalInfo:
              _count.Append("SELECT COUNT(*) FROM MS_SITE_PENDING_ACCOUNTS");
              break;

            case TYPE_MULTISITE_SITE_TASKS.DownloadPersonalInfo_Card:
              _count.Append("SELECT COUNT(*) FROM MS_SITE_SYNCHRONIZE_ACCOUNTS WHERE SSA_TODAY >= dbo.TodayOpening(0) ");
              break;
            case TYPE_MULTISITE_SITE_TASKS.UploadGamePlaySessions:
              _count.Append("SELECT COUNT(*) FROM GAME_PLAY_SESSIONS ");
              break;
            case TYPE_MULTISITE_SITE_TASKS.UploadAccountDocuments:
              _count.Append("SELECT COUNT(*) FROM MS_SITE_PENDING_ACCOUNT_DOCUMENTS");
              break;
            case TYPE_MULTISITE_SITE_TASKS.UploadSellsAndBuys:
              _count.Append("SELECT COUNT(*) FROM MS_PENDING_TASK68_WORK_DATA ");
              break;
            case TYPE_MULTISITE_SITE_TASKS.DownloadPersonalInfo_All:
            case TYPE_MULTISITE_SITE_TASKS.DownloadPoints_All:
            case TYPE_MULTISITE_SITE_TASKS.DownloadProvidersGames:
            case TYPE_MULTISITE_SITE_TASKS.DownloadProviders:
            case TYPE_MULTISITE_SITE_TASKS.DownloadAccountDocuments:
              if (SequenceId != -1)
              {
                _count.AppendFormat("ISNULL(ST_REMOTE_SEQUENCE_ID, 0) - {0}", SequenceId);
              }
              else
              {
                _count.Append(" ST_NUM_PENDING ");
              }

              //_count.AppendFormat("{0} - ISNULL(ST_LOCAL_SEQUENCE_ID, 0)", SequenceId);
              break;
            case TYPE_MULTISITE_SITE_TASKS.DownloadMasterProfiles:
            case TYPE_MULTISITE_SITE_TASKS.DownloadCorporateUsers:
            case TYPE_MULTISITE_SITE_TASKS.DownloadLcdMessages:
            case TYPE_MULTISITE_SITE_TASKS.DownloadBucketsConfig:
            case TYPE_MULTISITE_SITE_TASKS.DownloadAlarmPatterns:
              if (SequenceId != -1)
              {
                _count.Append(" CASE WHEN (ST_NUM_PENDING - 1 < 0) THEN 0 ELSE  ST_NUM_PENDING - 1 END  ");
              }
              else
              {
                _count.Append(" ST_NUM_PENDING ");
              }


              break;
            case TYPE_MULTISITE_SITE_TASKS.UploadAreas:
            case TYPE_MULTISITE_SITE_TASKS.UploadBanks:
            case TYPE_MULTISITE_SITE_TASKS.UploadPlaySessions:
            case TYPE_MULTISITE_SITE_TASKS.UploadProviders:
            case TYPE_MULTISITE_SITE_TASKS.UploadSalesPerHour:
            case TYPE_MULTISITE_SITE_TASKS.UploadTerminals:
            case TYPE_MULTISITE_SITE_TASKS.UploadTerminalsConnected:
            case TYPE_MULTISITE_SITE_TASKS.UploadCashierMovementsGrupedByHour:
            case TYPE_MULTISITE_SITE_TASKS.UploadAlarms:
            case TYPE_MULTISITE_SITE_TASKS.UploadHandpays:
            case TYPE_MULTISITE_SITE_TASKS.UploadCreditLines:
            case TYPE_MULTISITE_SITE_TASKS.UploadCreditLineMovements:
              if (SequenceId == -1)
              {
                _count.AppendLine("   ST_NUM_PENDING");
              }
              else
              {
                _count.AppendFormat(" CASE WHEN (@pNumPending > 0) THEN @pNumPending ELSE 0 END ");
              }
              break;

            case TYPE_MULTISITE_SITE_TASKS.UploadLastActivity:
              _count.Append("SELECT COUNT(*) FROM MS_SITE_PENDING_LAST_ACTIVITY");
              break;

            default:
              _count.Append("NULL");
              break;
          } // switch
        }
        else
        {
          // Is MultiSite
          if (Pending >= 0)
          {
            _count.Append(" @pNumPending ");
          }
          else
          {
            _count.Append(" ST_NUM_PENDING ");
          }
        }

        if (SiteId == -1)
        {
          _sb.AppendLine("DECLARE @pTaskEnabled AS BIT");
          _sb.AppendLine("DECLARE @pNewRunning  AS BIT");
          _sb.AppendLine("SET @pTaskEnabled = ISNULL(( SELECT ST_ENABLED FROM MS_SITE_TASKS WHERE ST_TASK_ID = @pTaskId ), 0)");
          _sb.AppendLine("SET @pNewRunning  = CASE WHEN ((@pIsRunning = 1) AND (@pTaskEnabled = 1)) THEN 1 ELSE 0 END ");
          _sb.AppendLine("UPDATE   MS_SITE_TASKS                      ");
          _sb.AppendLine("   SET   ST_RUNNING            = @pNewRunning");
          _sb.AppendLine("       , ST_STARTED            = CASE WHEN (ST_RUNNING = 0 AND @pNewRunning = 1) THEN GETDATE() ELSE ST_STARTED END ");
          _sb.AppendLine("       , ST_LAST_RUN           = CASE WHEN (ST_RUNNING = 1 AND @pNewRunning = 0) THEN ST_STARTED ELSE ST_LAST_RUN END ");
          _sb.AppendLine("       , ST_LOCAL_SEQUENCE_ID  = CASE WHEN (@pSequenceId > ISNULL(ST_LOCAL_SEQUENCE_ID, 0)) THEN @pSequenceId ELSE ST_LOCAL_SEQUENCE_ID END");
          _sb.AppendLine("       , ST_NUM_PENDING        = (" + _count.ToString() + ")");
          _sb.AppendLine(" WHERE   ST_TASK_ID            = @pTaskId ");
          _sb.AppendLine("SELECT   @pTaskEnabled, @pNewRunning");
        }
        else
        {
          _sb.AppendLine("DECLARE @pTaskEnabled AS BIT");
          _sb.AppendLine("DECLARE @pNewRunning  AS BIT");
          _sb.AppendLine("SET @pTaskEnabled = 1");
          _sb.AppendLine("SET @pNewRunning  = CASE WHEN (@pIsRunning = 1) THEN 1 ELSE 0 END ");
          _sb.AppendLine("UPDATE   MS_SITE_TASKS  ");
          _sb.AppendLine("   SET   ST_RUNNING            = @pNewRunning");
          _sb.AppendLine("       , ST_STARTED            = CASE WHEN (ST_RUNNING = 0 AND @pNewRunning = 1) THEN GETDATE() ELSE ST_STARTED END ");
          _sb.AppendLine("       , ST_LAST_RUN           = CASE WHEN (ST_RUNNING = 1 AND @pNewRunning = 0) THEN ST_STARTED ELSE ST_LAST_RUN END ");
          _sb.AppendLine("       , ST_LOCAL_SEQUENCE_ID  = CASE WHEN (@pSequenceId > ISNULL(ST_LOCAL_SEQUENCE_ID, 0)) THEN @pSequenceId ELSE ST_LOCAL_SEQUENCE_ID END");
          _sb.AppendLine("       , ST_NUM_PENDING        = (" + _count.ToString() + ")");
          _sb.AppendLine(" WHERE   ST_TASK_ID            = @pTaskId ");
          _sb.AppendLine("   AND   ST_SITE_ID            = @pSiteId ");
          _sb.AppendLine("SELECT   @pTaskEnabled, @pNewRunning");
        }

        // TODO: Falta el Site ID

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pIsRunning", SqlDbType.Bit).Value = IsRunning ? 1 : 0;
          _cmd.Parameters.Add("@pTaskId", SqlDbType.Int).Value = TaskId;
          _cmd.Parameters.Add("@pSequenceId", SqlDbType.BigInt).Value = SequenceId;
          _cmd.Parameters.Add("@pNumPending", SqlDbType.Int).Value = Pending;
          _cmd.Parameters.Add("@pSiteId", SqlDbType.Int).Value = SiteId;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (_reader.Read())
            {
              EnabledAndRunning = (_reader.GetBoolean(0) && _reader.GetBoolean(1));

              return true;
            }
          }

          return false;
        }

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Read Reporting Flag 
    //
    //  PARAMS :
    //      - INPUT :
    //          - ReportFlagValue:    Value of Reporting Flag 
    //      - OUTPUT :
    //          - ReportFlagValue
    //          - WaitSeconds
    //          - SequenceId
    //
    // RETURNS :
    //          - True:               Executed succesfully
    //          - False:              Executed unsuccesfully
    //
    //   NOTES :
    public static bool Read_ReportingFlagValues(TYPE_MULTISITE_SITE_TASKS TypeFlag,
                                                out Boolean IsRunning,
                                                out Int32 WaitSeconds,
                                                out Int64 SequenceId,
                                                out Int32 MaxRows,
                                                SqlTransaction Trx)
    {

      Int64 _dummy_remote_sq_id;
      Boolean _dummy_enabled;

      return Read_ReportingFlagValues(TypeFlag, out IsRunning, out _dummy_enabled, out WaitSeconds, out SequenceId, out _dummy_remote_sq_id, out MaxRows, Trx);

    }

    //------------------------------------------------------------------------------
    // PURPOSE : Read Reporting Flag 
    //
    //  PARAMS :
    //      - INPUT :
    //          - ReportFlagValue:    Value of Reporting Flag 
    //      - OUTPUT :
    //          - ReportFlagValue
    //          - WaitSeconds
    //          - SequenceId
    //
    // RETURNS :
    //          - True:               Executed succesfully
    //          - False:              Executed unsuccesfully
    //
    //   NOTES :
    public static bool Read_ReportingFlagValues(TYPE_MULTISITE_SITE_TASKS TypeFlag,
                                                out Boolean IsRunning,
                                                out Boolean IsEnabled,
                                                out Int32 WaitSeconds,
                                                out Int64 LocalSequenceId,
                                                out Int64 RemoteSequenceId,
                                                out Int32 MaxRows,
                                                SqlTransaction Trx)
    {
      StringBuilder _sb;

      WaitSeconds = 0;
      IsRunning = false;
      IsEnabled = false;
      LocalSequenceId = 0;
      RemoteSequenceId = 0;
      MaxRows = 0;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" SELECT   ST_RUNNING            ");
        _sb.AppendLine("        , ST_INTERVAL_SECONDS   ");
        _sb.AppendLine("        , ST_LOCAL_SEQUENCE_ID  ");
        _sb.AppendLine("        , ST_REMOTE_SEQUENCE_ID ");
        _sb.AppendLine("        , ST_MAX_ROWS_TO_UPLOAD ");
        _sb.AppendLine("        , ST_ENABLED            ");
        _sb.AppendLine("   FROM   MS_SITE_TASKS         ");
        _sb.AppendLine("  WHERE   ST_TASK_ID = @pTaskId ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pTaskId", System.Data.SqlDbType.Int).Value = TypeFlag;

          using (SqlDataReader _sql_reader = _cmd.ExecuteReader())
          {
            if (!_sql_reader.Read())
            {
              return false;
            }

            IsRunning = _sql_reader.IsDBNull(0) ? false : _sql_reader.GetBoolean(0); ;
            // TODO: IF IS NULL TAKE 0 MINUTES AND REFRESH EVER 0 MINUTES ?
            WaitSeconds = _sql_reader.IsDBNull(1) ? 0 : _sql_reader.GetInt32(1);
            LocalSequenceId = _sql_reader.IsDBNull(2) ? 0 : _sql_reader.GetInt64(2);
            RemoteSequenceId = _sql_reader.IsDBNull(3) ? 0 : _sql_reader.GetInt64(3);
            MaxRows = _sql_reader.IsDBNull(4) ? 0 : _sql_reader.GetInt32(4);
            IsEnabled = _sql_reader.IsDBNull(5) ? false : _sql_reader.GetBoolean(5); ;

            return true;

          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Read maximum rows of task
    //
    //  PARAMS :
    //      - INPUT :
    //          - SiteId:    
    //          - TypeFlag
    //          - Transaction
    //      - OUTPUT :
    //          - MaxRows
    //
    // RETURNS :
    //          - True:               Executed succesfully
    //          - False:              Executed unsuccesfully
    //
    //   NOTES :
    public static bool Read_CenterMaxRowsOfTask(Int32 SiteId, TYPE_MULTISITE_SITE_TASKS TypeFlag, out Int32 MaxRows, SqlTransaction Trx)
    {
      StringBuilder _sb;
      Object _int32;

      MaxRows = 0;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" SELECT   ST_MAX_ROWS_TO_UPLOAD ");
        _sb.AppendLine("   FROM   MS_SITE_TASKS         ");
        _sb.AppendLine("  WHERE   ST_TASK_ID = @pTaskId ");
        _sb.AppendLine("    AND   ST_SITE_ID = @pSiteId ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pTaskId", System.Data.SqlDbType.Int).Value = TypeFlag;
          _cmd.Parameters.Add("@pSiteId", System.Data.SqlDbType.Int).Value = SiteId;

          _int32 = _cmd.ExecuteScalar();

          if (_int32 == null && _int32 == DBNull.Value)
          {
            return false;
          }

          MaxRows = (Int32)_int32;

          return true;

        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    public static bool Center_SetSiteSequence(Int32 SiteId, TYPE_MULTISITE_SITE_TASKS TaskId, Int64 RemoteSequenceId, SqlTransaction Trx)
    {
      StringBuilder _sb;
      String _local_sequence;
      String _num_pending;

      _sb = new StringBuilder();
      switch (TaskId)
      {
        case TYPE_MULTISITE_SITE_TASKS.DownloadPoints_All:
          _local_sequence = String.Format("(SELECT SEQ_NEXT_VALUE - 1 FROM SEQUENCES WHERE SEQ_ID = {0})", (Int32)WSI.Common.SequenceId.TrackPointChanges);
          _num_pending = "CASE WHEN (@pLocalSequence - @pRemoteSequenceId < 0) THEN 0 ELSE @pLocalSequence - @pRemoteSequenceId  END";
          break;

        case TYPE_MULTISITE_SITE_TASKS.DownloadPersonalInfo_All:
          _local_sequence = String.Format("(SELECT SEQ_NEXT_VALUE - 1 FROM SEQUENCES WHERE SEQ_ID = {0})", (Int32)WSI.Common.SequenceId.TrackAccountChanges);
          //_num_pending = "CASE WHEN (@pRemoteSeque  nceId - @pLocalSequence < 0) THEN 0 ELSE @pRemoteSequenceId - @pLocalSequence END";
          _num_pending = "CASE WHEN (@pLocalSequence - @pRemoteSequenceId < 0) THEN 0 ELSE @pLocalSequence - @pRemoteSequenceId  END";
          break;

        case TYPE_MULTISITE_SITE_TASKS.DownloadProvidersGames:
          _local_sequence = String.Format("(SELECT SEQ_NEXT_VALUE - 1 FROM SEQUENCES WHERE SEQ_ID = {0})", (Int32)WSI.Common.SequenceId.ProvidersGames);
          _num_pending = "CASE WHEN (@pLocalSequence - @pRemoteSequenceId < 0) THEN 0 ELSE @pLocalSequence - @pRemoteSequenceId  END";
          break;

        case TYPE_MULTISITE_SITE_TASKS.DownloadProviders:
          _local_sequence = String.Format("(SELECT SEQ_NEXT_VALUE - 1 FROM SEQUENCES WHERE SEQ_ID = {0})", (Int32)WSI.Common.SequenceId.Providers);
          _num_pending = "CASE WHEN (@pLocalSequence - @pRemoteSequenceId < 0) THEN 0 ELSE @pLocalSequence - @pRemoteSequenceId  END";
          break;

        case TYPE_MULTISITE_SITE_TASKS.DownloadAccountDocuments:
          _local_sequence = String.Format("(SELECT SEQ_NEXT_VALUE - 1 FROM SEQUENCES WHERE SEQ_ID = {0})", (Int32)WSI.Common.SequenceId.AccountDocuments);
          _num_pending = "CASE WHEN (@pLocalSequence - @pRemoteSequenceId < 0) THEN 0 ELSE @pLocalSequence - @pRemoteSequenceId  END";
          break;


        case TYPE_MULTISITE_SITE_TASKS.DownloadMasterProfiles:

          _sb.AppendLine("  DECLARE @MaxSeq AS BIGINT ");
          _sb.AppendLine("  DECLARE @Count AS INT");
          _sb.AppendLine("  SELECT   @MaxSeq = MAX(ISNULL(GUP_TIMESTAMP, 0))  ");
          _sb.AppendLine("         , @Count  = SUM(CASE WHEN (GUP_TIMESTAMP > ISNULL(@pRemoteSequenceId, 0)) THEN 1 ELSE 0 END) ");
          _sb.AppendLine("    FROM   GUI_USER_PROFILES  ");
          _sb.AppendLine("         , MS_SITE_TASKS ");
          _sb.AppendLine("   WHERE   GUP_TIMESTAMP >= ISNULL(@pRemoteSequenceId, 0) ");
          _sb.AppendLine("     AND   GUP_MASTER_ID IS NOT NULL ");
          _sb.AppendLine("     AND   ST_TASK_ID    = @pTaskId ");
          _sb.AppendLine("     AND   ST_SITE_ID    = @pSiteId ");


          _local_sequence = " @MaxSeq";
          _num_pending = "@Count";
          break;

        case TYPE_MULTISITE_SITE_TASKS.DownloadCorporateUsers:

          _sb.AppendLine("  DECLARE @MaxSeq AS BIGINT ");
          _sb.AppendLine("  DECLARE @Count AS INT");
          _sb.AppendLine("  SELECT   @MaxSeq = MAX(ISNULL(GU_TIMESTAMP, 0))  ");
          _sb.AppendLine("         , @Count  = SUM(CASE WHEN (GU_TIMESTAMP > ISNULL(@pRemoteSequenceId, 0)) THEN 1 ELSE 0 END) ");
          _sb.AppendLine("    FROM   GUI_USERS  ");
          _sb.AppendLine("         , MS_SITE_TASKS ");
          _sb.AppendLine("   WHERE   GU_TIMESTAMP >= ISNULL(@pRemoteSequenceId, 0) ");
          _sb.AppendLine("     AND   GU_MASTER_ID IS NOT NULL ");
          _sb.AppendLine("     AND   ST_TASK_ID    = @pTaskId ");
          _sb.AppendLine("     AND   ST_SITE_ID    = @pSiteId ");

          _local_sequence = " @MaxSeq";
          _num_pending = "@Count";
          break;

        case TYPE_MULTISITE_SITE_TASKS.DownloadAlarmPatterns:

          _sb.AppendLine("  DECLARE @MaxSeq AS BIGINT ");
          _sb.AppendLine("  DECLARE @Count AS INT");
          _sb.AppendLine("  SELECT   @MaxSeq = MAX(ISNULL(PT_TIMESTAMP, 0))  ");
          _sb.AppendLine("         , @Count  = SUM(CASE WHEN (PT_TIMESTAMP > ISNULL(@pRemoteSequenceId, 0)) THEN 1 ELSE 0 END) ");
          _sb.AppendLine("    FROM   PATTERNS  ");
          _sb.AppendLine("         , MS_SITE_TASKS ");
          _sb.AppendLine("   WHERE   PT_TIMESTAMP >= ISNULL(@pRemoteSequenceId, 0) ");
          _sb.AppendLine("     AND   ST_TASK_ID    = @pTaskId ");
          _sb.AppendLine("     AND   ST_SITE_ID    = @pSiteId ");

          _local_sequence = " @MaxSeq";
          _num_pending = "@Count";
          break;

        case TYPE_MULTISITE_SITE_TASKS.DownloadLcdMessages:

          _sb.AppendLine("  DECLARE @MaxSeq AS BIGINT ");
          _sb.AppendLine("  DECLARE @Count AS INT");
          _sb.AppendLine("  SELECT   @MaxSeq = MAX(ISNULL(MSG_TIMESTAMP, 0))  ");
          _sb.AppendLine("         , @Count  = SUM(CASE WHEN (MSG_TIMESTAMP > ISNULL(@pRemoteSequenceId, 0)) THEN 1 ELSE 0 END) ");
          _sb.AppendLine("    FROM   LCD_MESSAGES  ");
          _sb.AppendLine("         , MS_SITE_TASKS ");
          _sb.AppendLine("   WHERE   MSG_TIMESTAMP >= ISNULL(@pRemoteSequenceId, 0) ");
          _sb.AppendLine("     AND   MSG_MASTER_SEQUENCE_ID IS NOT NULL ");
          _sb.AppendLine("     AND   ST_TASK_ID    = @pTaskId ");
          _sb.AppendLine("     AND   ST_SITE_ID    = @pSiteId ");

          _local_sequence = " @MaxSeq";
          _num_pending = "@Count";
          break;


        case TYPE_MULTISITE_SITE_TASKS.DownloadBucketsConfig :

          _sb.AppendLine("  DECLARE @MaxSeq AS BIGINT ");
          _sb.AppendLine("  DECLARE @Count AS INT");
          _sb.AppendLine("  SELECT   @MaxSeq = MAX(ISNULL(BU_TIMESTAMP,0))  ");
          _sb.AppendLine("         , @Count  = SUM(CASE WHEN BU_TIMESTAMP > ISNULL(@pRemoteSequenceId , 0) THEN 1 ELSE 0 END) ");
          _sb.AppendLine("    FROM   BUCKETS  ");
          _sb.AppendLine("         , MS_SITE_TASKS ");
          _sb.AppendLine("   WHERE   BU_TIMESTAMP  >= ISNULL(@pRemoteSequenceId , 0) ");
          _sb.AppendLine("     AND   BU_MASTER_SEQUENCE_ID IS NOT NULL ");
          _sb.AppendLine("     AND   ST_TASK_ID    = @pTaskId ");
          _sb.AppendLine("     AND   ST_SITE_ID = @pSiteId ");

          _local_sequence = " @MaxSeq";
          _num_pending = "@Count";
          break;



        default:
          return false;
      }

      _sb.AppendLine("DECLARE @pLocalSequence as BIGINT");
      _sb.AppendLine("SET @pLocalSequence = " + _local_sequence);
      _sb.AppendLine("UPDATE   MS_SITE_TASKS");
      _sb.AppendLine("   SET   ST_REMOTE_SEQUENCE_ID = @pRemoteSequenceId");
      _sb.AppendLine("       , ST_LOCAL_SEQUENCE_ID  = " + _local_sequence);
      _sb.AppendLine("       , ST_NUM_PENDING        = " + _num_pending);
      _sb.AppendLine(" WHERE   ST_SITE_ID            = @pSiteId ");
      _sb.AppendLine("   AND   ST_TASK_ID            = @pTaskId ");

      using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
      {
        _cmd.Parameters.Add("@pSiteId", SqlDbType.Int).Value = SiteId;
        _cmd.Parameters.Add("@pTaskId", SqlDbType.Int).Value = TaskId;
        _cmd.Parameters.Add("@pRemoteSequenceId", SqlDbType.BigInt).Value = RemoteSequenceId;

        if (_cmd.ExecuteNonQuery() == 1)
        {
          return true;
        }
      }

      return false;

    }

    public static bool Site_SetCenterSequence(TYPE_MULTISITE_SITE_TASKS TaskId, Int64 RemoteSequenceId, Int32 NumRowsUpdated, SqlTransaction Trx)
    {
      StringBuilder _sb;
      String _num_pending;

      switch (TaskId)
      {
        case TYPE_MULTISITE_SITE_TASKS.DownloadPoints_All:
        case TYPE_MULTISITE_SITE_TASKS.DownloadPersonalInfo_All:
        case TYPE_MULTISITE_SITE_TASKS.DownloadProvidersGames:
        case TYPE_MULTISITE_SITE_TASKS.DownloadProviders:
        case TYPE_MULTISITE_SITE_TASKS.DownloadAccountDocuments:
          _num_pending = "CASE WHEN (@pRemoteSequenceId - ISNULL(ST_LOCAL_SEQUENCE_ID, 0) < 0) THEN 0 ELSE @pRemoteSequenceId - ISNULL(ST_LOCAL_SEQUENCE_ID, 0) END";
          break;

        case TYPE_MULTISITE_SITE_TASKS.UploadAreas:
        case TYPE_MULTISITE_SITE_TASKS.UploadBanks:
        case TYPE_MULTISITE_SITE_TASKS.UploadPlaySessions:
        case TYPE_MULTISITE_SITE_TASKS.UploadProviders:
        case TYPE_MULTISITE_SITE_TASKS.UploadSalesPerHour:
        case TYPE_MULTISITE_SITE_TASKS.UploadTerminals:
        case TYPE_MULTISITE_SITE_TASKS.UploadTerminalsConnected:
        case TYPE_MULTISITE_SITE_TASKS.UploadAccountDocuments:
        case TYPE_MULTISITE_SITE_TASKS.UploadLastActivity:
        case TYPE_MULTISITE_SITE_TASKS.UploadCashierMovementsGrupedByHour:
        case TYPE_MULTISITE_SITE_TASKS.UploadAlarms:
        case TYPE_MULTISITE_SITE_TASKS.UploadHandpays:
        case TYPE_MULTISITE_SITE_TASKS.UploadCreditLines:
        case TYPE_MULTISITE_SITE_TASKS.UploadCreditLineMovements:
          _num_pending = "CASE WHEN (ISNULL(ST_NUM_PENDING, 0) - @pNumRowsUpdated < 0) THEN 0 ELSE ST_NUM_PENDING - @pNumRowsUpdated END";
          break;

        default:
          return false;
      }

      _sb = new StringBuilder();

      _sb.AppendLine("DECLARE @pLocalSequence as BIGINT");
      _sb.AppendLine("UPDATE   MS_SITE_TASKS");
      _sb.AppendLine("   SET   ST_REMOTE_SEQUENCE_ID = @pRemoteSequenceId");
      _sb.AppendLine("       , ST_NUM_PENDING        = " + _num_pending);
      _sb.AppendLine(" WHERE   ST_TASK_ID            = @pTaskId ");

      using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
      {
        _cmd.Parameters.Add("@pTaskId", SqlDbType.Int).Value = TaskId;
        _cmd.Parameters.Add("@pRemoteSequenceId", SqlDbType.BigInt).Value = RemoteSequenceId;
        _cmd.Parameters.Add("@pNumRowsUpdated", SqlDbType.Int).Value = NumRowsUpdated;

        if (_cmd.ExecuteNonQuery() == 1)
        {
          return true;
        }
      }

      return false;

    }

    public static bool Site_SetCenterSequence(TYPE_MULTISITE_SITE_TASKS TaskId, Int64 RemoteSequenceId, SqlTransaction Trx)
    {

      return Site_SetCenterSequence(TaskId, RemoteSequenceId, 0, Trx);

    }

    //------------------------------------------------------------------------------
    // PURPOSE : Sets Reporting Flag 
    //
    //  PARAMS :
    //      - INPUT :
    //          - ReportFlagValue:    New Value of Reporting Flag 
    //          - SequenceId          Last sequence id
    //          - Trx:                Transaction
    //      - OUTPUT :
    //          - None
    // RETURNS :
    //          - True:               Executed succesfully
    //          - False:              Executed unsuccesfully
    //
    //   NOTES : 
    public static Boolean Center_SetSiteReportingFlag(Int32 CallingSiteId, TYPE_MULTISITE_SITE_TASKS TaskId, Int64 SiteSequence, Boolean IsRunning, SqlTransaction Trx)
    {

      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine("UPDATE   MS_SITE_TASKS  ");
        _sb.AppendLine("   SET   ST_RUNNING       = @pIsRunning");
        _sb.AppendLine("       , ST_STARTED       = CASE WHEN (ST_RUNNING = 0 AND @pIsRunning = 1) THEN GETDATE()  ELSE ST_STARTED END ");
        _sb.AppendLine("       , ST_LAST_RUN      = CASE WHEN (ST_RUNNING = 1 AND @pIsRunning = 0) THEN ST_STARTED ELSE ST_LAST_RUN END ");
        _sb.AppendLine("       , ST_FORCE_RESYNCH = CASE WHEN (@pSiteSequence = 0) THEN 0 ELSE ST_FORCE_RESYNCH END ");

        // For sequence timestamp update the ST_LOCAL_SEQUENCE_ID
        switch (TaskId)
        {
          case TYPE_MULTISITE_SITE_TASKS.Unknown:
          case TYPE_MULTISITE_SITE_TASKS.DownloadConfiguration:
          case TYPE_MULTISITE_SITE_TASKS.UploadPointsMovements:
          case TYPE_MULTISITE_SITE_TASKS.UploadPersonalInfo:
          case TYPE_MULTISITE_SITE_TASKS.UploadGamePlaySessions:
          case TYPE_MULTISITE_SITE_TASKS.UploadProvidersGames:
          case TYPE_MULTISITE_SITE_TASKS.DownloadPoints_Site:
          case TYPE_MULTISITE_SITE_TASKS.DownloadPoints_All:
          case TYPE_MULTISITE_SITE_TASKS.DownloadPersonalInfo_Card:
          case TYPE_MULTISITE_SITE_TASKS.DownloadPersonalInfo_Site:
          case TYPE_MULTISITE_SITE_TASKS.DownloadPersonalInfo_All:
          case TYPE_MULTISITE_SITE_TASKS.DownloadProvidersGames:
          case TYPE_MULTISITE_SITE_TASKS.DownloadProviders:
          case TYPE_MULTISITE_SITE_TASKS.DownloadMasterProfiles:
          case TYPE_MULTISITE_SITE_TASKS.DownloadCorporateUsers:
          case TYPE_MULTISITE_SITE_TASKS.DownloadLcdMessages:
          case TYPE_MULTISITE_SITE_TASKS.DownloadBucketsConfig:
          case TYPE_MULTISITE_SITE_TASKS.DownloadAlarmPatterns:
          case TYPE_MULTISITE_SITE_TASKS.UploadAccountDocuments:
          case TYPE_MULTISITE_SITE_TASKS.DownloadAccountDocuments:
          case TYPE_MULTISITE_SITE_TASKS.UploadLastActivity:
          case TYPE_MULTISITE_SITE_TASKS.UploadSellsAndBuys:
          default:
            break;
          case TYPE_MULTISITE_SITE_TASKS.UploadSalesPerHour:
          case TYPE_MULTISITE_SITE_TASKS.UploadPlaySessions:
          case TYPE_MULTISITE_SITE_TASKS.UploadProviders:
          case TYPE_MULTISITE_SITE_TASKS.UploadBanks:
          case TYPE_MULTISITE_SITE_TASKS.UploadAreas:
          case TYPE_MULTISITE_SITE_TASKS.UploadTerminals:
          case TYPE_MULTISITE_SITE_TASKS.UploadTerminalsConnected:
          case TYPE_MULTISITE_SITE_TASKS.UploadCashierMovementsGrupedByHour:
          case TYPE_MULTISITE_SITE_TASKS.UploadAlarms:
          case TYPE_MULTISITE_SITE_TASKS.UploadHandpays:
          case TYPE_MULTISITE_SITE_TASKS.UploadCreditLines:
          case TYPE_MULTISITE_SITE_TASKS.UploadCreditLineMovements:
            _sb.AppendLine("       , ST_LOCAL_SEQUENCE_ID = CASE WHEN (@pSiteSequence = 0 AND ISNULL(ST_LOCAL_SEQUENCE_ID, 0) < @pSiteSequence ) ");
            _sb.AppendLine("                                     THEN ISNULL(ST_LOCAL_SEQUENCE_ID, 0) ELSE @pSiteSequence END ");
            break;
        }

        _sb.AppendLine(" WHERE   ST_TASK_ID   = @pTaskId ");
        _sb.AppendLine("   AND   ST_SITE_ID   = @pSiteId ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pSiteId", SqlDbType.Int).Value = CallingSiteId;
          _cmd.Parameters.Add("@pTaskId", SqlDbType.Int).Value = TaskId;
          _cmd.Parameters.Add("@pIsRunning", SqlDbType.Bit).Value = IsRunning ? 1 : 0;
          _cmd.Parameters.Add("@pSiteSequence", SqlDbType.BigInt).Value = SiteSequence;

          return _cmd.ExecuteNonQuery() == 1;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

  }

}

