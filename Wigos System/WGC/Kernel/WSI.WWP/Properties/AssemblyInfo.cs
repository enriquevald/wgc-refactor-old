﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("WSI.WWP")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("WIN SYSTEMS, LTD.")]
[assembly: AssemblyProduct("WSI.WWP")]
[assembly: AssemblyCopyright("Copyright © WIN SYSTEMS, LTD. 2011")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
[assembly: AssemblyVersion("00.001")]
[assembly: AssemblyFileVersion("00.001")]
[assembly: ComVisibleAttribute(false)]
[assembly: GuidAttribute("95930af6-b7d3-466e-adce-f5884bd2c5fe")]
