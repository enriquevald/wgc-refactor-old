//------------------------------------------------------------------------------
// Copyright � 2011 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WWP_MsgTable.cs
// 
//   DESCRIPTION: Process for Table message.
// 
//        AUTHOR: Alberto Cuesta
// 
// CREATION DATE: 28-FEB-2013
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 28-FEB-2013 ACC    First release.
//------------------------------------------------------------------------------

using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using WSI.Common;
using WSI.WWP;

public partial class WWP_ProcessRequest
{
  // Function Table moved to Center->WWP_ProcessSiteRequest->ProcessSiteRequest->Table
}
