//------------------------------------------------------------------------------
// Copyright � 2011 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WWP_QueryLicense.cs
// 
//   DESCRIPTION: Request and Reply for Query License message.
// 
//        AUTHOR: Alberto Cuesta
// 
// CREATION DATE: 07-FEB-2011
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 07-FEB-2011 ACC    First release.
// 25-MAY-2017 FOS    Bug 27575 :Error in total outputs
//------------------------------------------------------------------------------

using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using WSI.Common;
using WSI.WWP;

public partial class WWP_ProcessRequest
{
  public static void SqlCommand(WWP_Message WwpRequest, WWP_Message WwpResponse, SqlTransaction SqlTrx)
  {
    WWP_MsgSqlCommand _request;
    WWP_MsgSqlCommandReply _response;
    SqlDataAdapter _da;
    DataSet _ds;
    TimeSpan _t0;

    _t0 = Performance.GetTickCount();

    _request = (WWP_MsgSqlCommand)WwpRequest.MsgContent;
    _response = (WWP_MsgSqlCommandReply)WwpResponse.MsgContent;

    try
    {
      _da = new SqlDataAdapter();

      //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
      // TODO DELETE: It's necessary new iStats version for fix that. The call to this functions is provissional.                                                     //
      _request.SqlCommand.CommandText = _request.SqlCommand.CommandText.Replace("PK_dbo.cashier_movements_grouped_by_hour", "PK_cashier_movements_grouped_by_hour");  //
      //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

      _da.SelectCommand = _request.SqlCommand;      
      _da.SelectCommand.Connection = WGDB.Connection();

      _ds = new DataSet("DataSet");
      _t0 = Performance.GetTickCount();
      _da.Fill(_ds);
      _t0 = Performance.GetElapsedTicks(_t0);

      _response.Format = _request.Format;
      _response.DataSet = _ds;
      _response.CommandExecutionTime = (Int64)_t0.TotalMilliseconds;

      WwpResponse.MsgHeader.ResponseCode = WWP_ResponseCodes.WWP_RC_OK;
    }
    catch (WWP_Exception wwp_ex)
    {
      throw wwp_ex;
    }
    catch (Exception _ex)
    {
      WWP_MsgError _content;
      _content = new WWP_MsgError();
      _content.Description = _ex.Message;

      WwpResponse.MsgHeader.MsgType = WWP_MsgTypes.WWP_MsgError;
      WwpResponse.MsgContent = _content;

      Log.Exception(_ex);
    }
  }
}
