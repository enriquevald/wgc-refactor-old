//------------------------------------------------------------------------------
// Copyright � 2011 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WWP_MsgTable.cs
// 
//   DESCRIPTION: WWP_MsgTable class
// 
//        AUTHOR: Alberto Cuesta
// 
// CREATION DATE: 28-FEB-2013
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 28-FEB-2013 ACC    First release.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Net;
using WSI.Common;
using System.Collections;
using System.Data.SqlClient;
using System.Data;
using Microsoft.SqlServer.Server;
using System.Runtime.Serialization;
using System.IO;

namespace WSI.WWP
{
  public class WWP_MsgTable : IXml
  {
    private WWP_TableTypes m_type;
    private DataSetRawFormat m_format = DataSetRawFormat.BIN_DEFLATE_BASE64;
    private DataSet m_ds = null;

    public WWP_TableTypes Type
    {
      get { return m_type; }
      set { m_type = value; }
    }

    public DataSetRawFormat Format
    {
      get { return m_format; }
      set { m_format = value; }
    }

    public DataSet DataSet
    {
      get { return m_ds; }
      set { m_ds = value; }
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Convert the properties of this class to a XML String representation.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - String
    //
    public String ToXml()
    {
      StringBuilder _str_builder;

      _str_builder = new StringBuilder();

      _str_builder.Append("<WWP_MsgTable>");
      _str_builder.Append("<Type>" + Type.ToString() + "</Type>");
      _str_builder.Append("<ResultFormat>" + Format.ToString() + "</ResultFormat>");
      _str_builder.Append("<RawResult>");
      _str_builder.Append(Misc.DataSetToBase64(DataSet, Format));
      _str_builder.Append("</RawResult>");
      _str_builder.Append("</WWP_MsgTable>");

      return _str_builder.ToString();
    } // ToXml

    //------------------------------------------------------------------------------
    // PURPOSE : Parse a XML Document and save values into the properties of this class.
    //
    //  PARAMS :
    //      - INPUT :
    //          - XmlReader Reader
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public void LoadXml(XmlReader XmlReader)
    {
      String _type_str;

      if (XmlReader.Name != "WWP_MsgTable")
      {
        Log.Error("Tag WWP_MsgTableReply not found in content area");

        throw new WWP_Exception(WWP_ResponseCodes.WWP_RC_MSG_FORMAT_ERROR);
      }

      _type_str = XML.GetValue(XmlReader, "Type");
      Type = (WWP_TableTypes)Enum.Parse(typeof(WWP_TableTypes), _type_str, true);

      Format = (DataSetRawFormat)Enum.Parse(typeof(DataSetRawFormat), XML.GetValue(XmlReader, "ResultFormat"), true);
      DataSet = Misc.DataSetFromBase64(XML.GetValue(XmlReader, "RawResult"), Format);
    } // LoadXml
 
  } // WWP_MsgTable

  public class WWP_MsgTableReply : IXml
  {
    private DataSetRawFormat m_format = DataSetRawFormat.BIN_DEFLATE_BASE64;
    private DataSet m_ds = null;

    public DataSetRawFormat Format
    {
      get { return m_format; }
      set { m_format = value; }
    }

    public DataSet DataSet
    {
      get { return m_ds; }
      set { m_ds = value; }
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Convert the properties of this class to a XML String representation.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - String
    //
    public String ToXml()
    {
      StringBuilder _str_builder;

      _str_builder = new StringBuilder();

      _str_builder.Append("<WWP_MsgTableReply>");
      _str_builder.Append("<ResultFormat>" + Format.ToString() + "</ResultFormat>");
      _str_builder.Append("<RawResult>");
      _str_builder.Append(Misc.DataSetToBase64(DataSet, Format));
      _str_builder.Append("</RawResult>");
      _str_builder.Append("</WWP_MsgTableReply>");

      return _str_builder.ToString();
    } // ToXml

    //------------------------------------------------------------------------------
    // PURPOSE : Parse a XML Document and save values into the properties of this class.
    //
    //  PARAMS :
    //      - INPUT :
    //          - XmlReader Reader
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public void LoadXml(XmlReader XmlReader)
    {
      if (XmlReader.Name != "WWP_MsgTableReply")
      {
        Log.Error("Tag WWP_MsgTableReply not found in content area");

        throw new WWP_Exception(WWP_ResponseCodes.WWP_RC_MSG_FORMAT_ERROR);
      }

      Format = (DataSetRawFormat)Enum.Parse(typeof(DataSetRawFormat), XML.GetValue(XmlReader, "ResultFormat"), true);
      DataSet = Misc.DataSetFromBase64(XML.GetValue(XmlReader, "RawResult"), Format);
    } // LoadXml

  } // WWP_MsgTableReply

} // WSI.WWP
