using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using WSI.Common;

namespace WSI.WWP
{

  public class WWP_MsgCreateAccounts : IXml
  {
    public class AccountItem
    {
      public Int64 account_id;
      public String trackdata;

      internal AccountItem(Int64 AccountId, String Trackdata)
      {
        account_id = AccountId;
        trackdata = Trackdata;
      }
    }

    public class AccountList : List<AccountItem>
    {
      public void Add(Int64 AccountId, String Trackdata)
      {
        this.Add(new AccountItem(AccountId, Trackdata));
      }
    }
    private AccountList m_account_list = new AccountList();

    public AccountList List
    {
      get
      {
        return m_account_list;
      }
    }


    public string ToXml()
    {
      XmlDocument _xml;
      XmlNode _node_message;
      XmlNode _node_account;

      _xml = new XmlDocument();
      _node_message = _xml.CreateNode(XmlNodeType.Element, "WWP_MsgCreateAccounts", "");
      _xml.AppendChild(_node_message);

      foreach (AccountItem _item in this.List)
      {
        _node_account = XML.AppendChild(_node_message, "Account", "");
        XML.AppendChild(_node_account, "account_id", _item.account_id.ToString());
        XML.AppendChild(_node_account, "trackdata", _item.trackdata);
        //TODO: A�adir fecha/hora?
      }

      return _xml.OuterXml;
    }

    public void LoadXml(XmlReader XmlReader)
    {
      Int64 _account_id;
      String _trackdata;

      _account_id = 0;
      _trackdata = null;

      //XmlReader.Read();
      while ((XmlReader.Name != "WWP_MsgCreateAccounts") || (XmlReader.NodeType != XmlNodeType.EndElement))
      {
        
        if ((XmlReader.Name == "Account") && (XmlReader.NodeType == XmlNodeType.Element))
        {
          _account_id = Int64.Parse(XML.ReadTagValue(XmlReader, "account_id"));
          _trackdata = XML.ReadTagValue(XmlReader, "trackdata");

          this.List.Add(new AccountItem(_account_id, _trackdata));
        }
        
        XmlReader.Read();
      }
    } // LoadXml
  } // WWP_MsgCreateAccounts
}
