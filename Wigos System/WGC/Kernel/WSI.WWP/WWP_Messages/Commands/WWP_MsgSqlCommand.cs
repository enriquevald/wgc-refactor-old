//------------------------------------------------------------------------------
// Copyright � 2011 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WWP_MsgSqlCommand.cs
// 
//   DESCRIPTION: WWP_MsgSqlCommand class
// 
//        AUTHOR: Andreu Julia, Alberto Cuesta, Ra�l Cervera
// 
// CREATION DATE: 14-FEB-2011 Valentine's day
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 14-FEB-2011 AJQ, ACC, RCI  First release.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Net;
using WSI.Common;
using System.Collections;
using System.Data.SqlClient;
using System.Data;
using Microsoft.SqlServer.Server;
using System.Runtime.Serialization;
using System.IO;

namespace WSI.WWP
{
  public class WWP_MsgSqlCommand : IXml
  {
    private DataSetRawFormat m_format = DataSetRawFormat.BIN_DEFLATE_BASE64;
    private SqlCommand m_sql_cmd = null;

    public DataSetRawFormat Format
    {
      get { return m_format; }
      set { m_format = value; }
    }

    public SqlCommand SqlCommand
    {
      get { return m_sql_cmd; }
      set { m_sql_cmd = value; }
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Convert the properties of this class to a XML String representation.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - String
    //
    public String ToXml()
    {
      StringBuilder _str_builder;
      
      _str_builder = new StringBuilder();
      _str_builder.Append("<WWP_MsgSqlCommand>");
      _str_builder.Append(Misc.DataSetToXml(RemoteSqlCmd.SqlCommandToDataSet(SqlCommand), XmlWriteMode.IgnoreSchema));
      _str_builder.Append("<ResultFormat>" + Format.ToString() + "</ResultFormat>");
      _str_builder.Append("</WWP_MsgSqlCommand>");
      
      return _str_builder.ToString();
    } // ToXml

    //------------------------------------------------------------------------------
    // PURPOSE : Parse a XML Document and save values into the properties of this class.
    //
    //  PARAMS :
    //      - INPUT :
    //          - XmlReader Reader
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public void LoadXml(XmlReader XmlReader)
    {
      DataSet _query_dataset;
      DataTable _table;

      if (XmlReader.Name != "WWP_MsgSqlCommand")
      {
        Log.Error("Tag WWP_MsgSqlCommand not found in content area");

        throw new WWP_Exception(WWP_ResponseCodes.WWP_RC_MSG_FORMAT_ERROR);
      }
     
      _query_dataset = new DataSet();
      _table = new DataTable("Command");
      _table.Columns.Add("CommandText", Type.GetType("System.String"));
      _table.Columns.Add("CommandTimeout", Type.GetType("System.Int32"));
      _query_dataset.Tables.Add(_table);
            
      _table = new DataTable("Parameter");
      _table.Columns.Add("Name", Type.GetType("System.String"));
      _table.Columns.Add("SqlDbType", Type.GetType("System.String"));
      _table.Columns.Add("Value", Type.GetType("System.Object"));
      _query_dataset.Tables.Add(_table);
        
      while ( XmlReader.Name != "Query" )
      {
        XmlReader.Read();
      }
      _query_dataset.ReadXml(XmlReader, XmlReadMode.IgnoreSchema);

      SqlCommand = RemoteSqlCmd.DataSetToSqlCommand(_query_dataset);
      Format = (DataSetRawFormat)Enum.Parse(typeof(DataSetRawFormat), XML.GetValue(XmlReader, "ResultFormat"), true);
    } // LoadXml

  } // WWP_MsgSqlCommand

  public class WWP_MsgSqlCommandReply : IXml
  {
    private DataSetRawFormat m_format = DataSetRawFormat.BIN_DEFLATE_BASE64;
    private DataSet m_ds = null;
    private Int64 m_exec_time = 0;

    public DataSetRawFormat Format
    {
      get { return m_format; }
      set { m_format = value; }
    }

    public DataSet DataSet
    {
      get { return m_ds; }
      set { m_ds = value; }
    }

    public Int64 CommandExecutionTime
    {
      get { return m_exec_time; }
      set { m_exec_time = value; }
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Convert the properties of this class to a XML String representation.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - String
    //
    public String ToXml()
    {
      StringBuilder _str_builder;

      _str_builder = new StringBuilder();

      _str_builder.Append("<WWP_MsgSqlCommandReply>");
      _str_builder.Append("<ResultFormat>" + Format.ToString() + "</ResultFormat>");
      _str_builder.Append("<RawResult>");
      _str_builder.Append(Misc.DataSetToBase64(DataSet, Format));
      _str_builder.Append("</RawResult>");
      _str_builder.Append("<CommandExecutionTime>");
      _str_builder.Append(CommandExecutionTime);
      _str_builder.Append("</CommandExecutionTime>");
      _str_builder.Append("</WWP_MsgSqlCommandReply>");

      return _str_builder.ToString();
    } // ToXml

    //------------------------------------------------------------------------------
    // PURPOSE : Parse a XML Document and save values into the properties of this class.
    //
    //  PARAMS :
    //      - INPUT :
    //          - XmlReader Reader
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public void LoadXml(XmlReader XmlReader)
    {
      if (XmlReader.Name != "WWP_MsgSqlCommandReply")
      {
        Log.Error("Tag WWP_MsgSqlCommandReply not found in content area");

        throw new WWP_Exception(WWP_ResponseCodes.WWP_RC_MSG_FORMAT_ERROR);
      }

      Format = (DataSetRawFormat)Enum.Parse(typeof(DataSetRawFormat), XML.GetValue(XmlReader, "ResultFormat"), true);
      DataSet = Misc.DataSetFromBase64(XML.GetValue(XmlReader, "RawResult"), Format);
      Int64.TryParse(XML.GetValue(XmlReader, "CommandExecutionTime"), out m_exec_time);
    } // LoadXml

  } // WWP_MsgSqlCommandReply

} // WSI.WWP
