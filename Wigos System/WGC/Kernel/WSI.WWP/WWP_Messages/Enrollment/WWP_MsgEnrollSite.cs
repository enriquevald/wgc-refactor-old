//------------------------------------------------------------------------------
// Copyright � 2008 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WWP_MsgEnrollSite.cs
// 
//   DESCRIPTION: WWP_MsgEnrollSite class
// 
//        AUTHOR: Alberto Cuesta
// 
// CREATION DATE: 04-FEB-2011
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 04-FEB-2011 ACC    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Net;
using WSI.Common;

namespace WSI.WWP
{
  public class WWP_MsgEnrollSite:IXml
  {
    public String SiteName = Site.Name;
    public String SiteDatabaseVersion = WGDB.GetDatabaseVersion();
    public String ServerMacAddress = Site.ServerMacAddress;

    //------------------------------------------------------------------------------
    // PURPOSE : Convert the properties of this class to a XML String representation.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - String
    //
    public String ToXml()
    {
      StringBuilder str_builder;

      str_builder = new StringBuilder ();

      str_builder.Append("<WWP_MsgEnrollSite><SiteName>");
      str_builder.Append(SiteName);
      str_builder.Append("</SiteName><SiteDatabaseVersion>");
      str_builder.Append(SiteDatabaseVersion);
      str_builder.Append("</SiteDatabaseVersion><ServerMacAddress>");
      str_builder.Append(ServerMacAddress);
      str_builder.Append("</ServerMacAddress></WWP_MsgEnrollSite>");

      return str_builder.ToString ();
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Parse a XML Document and save values into the properties of this class.
    //
    //  PARAMS :
    //      - INPUT :
    //          - XmlReader Reader
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public void LoadXml(XmlReader Xmlreader)
    {
      if (Xmlreader.Name != "WWP_MsgEnrollSite")
      {
        Log.Error("Tag WWP_MsgEnrollSite not found in content area");

        throw new WWP_Exception(WWP_ResponseCodes.WWP_RC_MSG_FORMAT_ERROR);
      }
      else
      {
        SiteName = XML.GetValue(Xmlreader, "SiteName");
        SiteDatabaseVersion = XML.GetValue(Xmlreader, "SiteDatabaseVersion");
        ServerMacAddress = XML.GetValue(Xmlreader, "ServerMacAddress");
      }
    }
  }

  public class WWP_MsgEnrollSiteReply:IXml
  {
    public Int64 LastSequenceId;

    //------------------------------------------------------------------------------
    // PURPOSE : Convert the properties of this class to a XML String representation.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - String
    //
    public String ToXml()
    {
      StringBuilder str_builder;

      str_builder = new StringBuilder();

      str_builder.Append("<WWP_MsgEnrollSiteReply><LastSequenceId>");
      str_builder.Append(LastSequenceId.ToString());
      str_builder.Append("</LastSequenceId></WWP_MsgEnrollSiteReply>");

      return str_builder.ToString();
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Parse a XML Document and save values into the properties of this class.
    //
    //  PARAMS :
    //      - INPUT :
    //          - XmlReader Reader
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public void LoadXml(XmlReader reader)
    {
      if (reader.Name != "WWP_MsgEnrollSiteReply")
      {
        Log.Error("Tag WWP_MsgEnrollSiteReply not found in content area");

        throw new WWP_Exception(WWP_ResponseCodes.WWP_RC_MSG_FORMAT_ERROR);
      }
      else
      {
        LastSequenceId = Int64.Parse(XML.GetValue(reader, "LastSequenceId"));
      }
    }
  }
}
