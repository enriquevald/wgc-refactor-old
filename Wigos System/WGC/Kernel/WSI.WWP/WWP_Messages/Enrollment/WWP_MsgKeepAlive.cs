//------------------------------------------------------------------------------
// Copyright � 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WWP_MsgKeepAlive.cs
// 
//   DESCRIPTION: WWP_MsgKeepAlive class
// 
//        AUTHOR: Alberto Cuesta
// 
// CREATION DATE: 01-MAR-2013
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 01-MAR-2013 ACC    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Net;
using WSI.Common;

namespace WSI.WWP
{
    public class WWP_MsgKeepAlive : IXml
    {
        public String ToXml()
        {
            XmlDocument xml;
            XmlNode node;

            xml = new XmlDocument();

            node = xml.CreateNode(XmlNodeType.Element, "WWP_MsgKeepAlive", "");
            xml.AppendChild(node);

            return xml.OuterXml;
        }

      public void LoadXml(XmlReader reader)
      {
        if (reader.Name != "WWP_MsgKeepAlive")
        {
          throw new WWP_Exception(WWP_ResponseCodes.WWP_RC_MSG_FORMAT_ERROR);
        }
      }

    }

    public class WWP_MsgKeepAliveReply : IXml
    {
        public String ToXml()
        {
            XmlDocument xml;
            XmlNode node;

            xml = new XmlDocument();

            node = xml.CreateNode(XmlNodeType.Element, "WWP_MsgKeepAliveReply", "");
            xml.AppendChild(node);

            return xml.OuterXml;
        }

      public void LoadXml(XmlReader reader)
      {
        if (reader.Name != "WWP_MsgKeepAliveReply")
        {
          throw new WWP_Exception(WWP_ResponseCodes.WWP_RC_MSG_FORMAT_ERROR);
        }
      }
    }
}
