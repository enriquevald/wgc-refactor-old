//------------------------------------------------------------------------------
// Copyright � 2008 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WWP_MsgUnenrollSite.cs
// 
//   DESCRIPTION: WWP_MsgUnenrollSite class
// 
//        AUTHOR: Alberto Cuesta
// 
// CREATION DATE: 04-FEB-2011
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 04-FEB-2011 ACC    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Net;
using WSI.Common;


namespace WSI.WWP
{
  public class WWP_MsgUnenrollSite:IXml
  {
    //------------------------------------------------------------------------------
    // PURPOSE : Convert the properties of this class to a XML String representation.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - String
    //
    public String ToXml()
    {
      StringBuilder str_builder;

      str_builder = new StringBuilder();
      str_builder.Append("<WWP_MsgUnenrollSite></WWP_MsgUnenrollSite>");
      return str_builder.ToString();
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Parse a XML Document and save values into the properties of this class.
    //
    //  PARAMS :
    //      - INPUT :
    //          - XmlReader Reader
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public void LoadXml(XmlReader reader)
    {
      if (reader.Name != "WWP_MsgUnenrollSite")
      {
        Log.Error("Tag WWP_MsgUnenrollSite not found in content area");

        throw new WWP_Exception(WWP_ResponseCodes.WWP_RC_MSG_FORMAT_ERROR);
      }
    }
  }

  public class WWP_MsgUnenrollSiteReply:IXml
  {
    //------------------------------------------------------------------------------
    // PURPOSE : Convert the properties of this class to a XML String representation.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - String
    //
    public String ToXml()
    {
      StringBuilder str_builder;

      str_builder = new StringBuilder();
      str_builder.Append("<WWP_MsgUnenrollSiteReply></WWP_MsgUnenrollSiteReply>");
      return str_builder.ToString();

    }

    //------------------------------------------------------------------------------
    // PURPOSE : Parse a XML Document and save values into the properties of this class.
    //
    //  PARAMS :
    //      - INPUT :
    //          - XmlReader Reader
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public void LoadXml(XmlReader reader)
    {
      if (reader.Name != "WWP_MsgUnenrollSiteReply")
      {
        Log.Error("Tag WWP_MsgUnenrollSiteReply not found in content area");

        throw new WWP_Exception(WWP_ResponseCodes.WWP_RC_MSG_FORMAT_ERROR);
      }
    }
  }
}
