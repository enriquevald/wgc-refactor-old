//------------------------------------------------------------------------------
// Copyright � 2011 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WWP_MsgHeader.cs
// 
//   DESCRIPTION: WWP Message Header
// 
//        AUTHOR: Alberto Cuesta
// 
// CREATION DATE: 03-FEB-2011
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 03-FEB-2011 ACC    First release.
// 05-DIC-2014 DCS    Added Task Download Lcd Messages
// 14-JAN-2015 DCS    Added Task Upload Handpays
// 17-MAR-2015 ANM    Added Task Upload Sells & buys
// 01-FEB-2016 AMF    PBI 8272: Space Requests
// 31-MAR-2017 MS     WIGOS-411 Creditlines
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.IO;

using WSI.Common;


namespace WSI.WWP
{
  /// <summary>
  /// WWP message types
  /// </summary>
  public enum WWP_MsgTypes
  { 
      // Request                      // Reply
      WWP_MsgEnrollSite = 1,          WWP_MsgEnrollSiteReply = 2
    , WWP_MsgUnenrollSite = 3,        WWP_MsgUnenrollSiteReply = 4

    , WWP_MsgSqlCommand   = 5,        WWP_MsgSqlCommandReply = 6
    , WWP_MsgTable   = 7,             WWP_MsgTableReply = 8

    , WWP_MsgKeepAlive = 9,           WWP_MsgKeepAliveReply = 10
    , WWP_MsgCreateAccounts = 11,     WWP_MsgCreateAccountsReply = 12 //this message don't reply

    , WWP_MsgError = 101
    , WWP_MsgUnknown = 102
  }

  /// <summary>
  /// WWP response codes
  /// </summary>
  public enum WWP_ResponseCodes
  {
      WWP_RC_OK = 0
    , WWP_RC_ERROR = 1
    , WWP_RC_DATABASE_OFFLINE = 2
    , WWP_RC_SERVICE_NOT_AVAILABLE = 3
    , WWP_RC_SERVICE_NOT_FOUND = 4
    , WWP_RC_SITE_NOT_AUTHORIZED = 5
    , WWP_RC_SITE_SESSION_NOT_VALID = 6
    , WWP_RC_SITE_UP_TO_DATE = 7
    , WWP_RC_USER_NOT_AUTHORIZED = 8
    , WWP_RC_USER_WRONG_LOGIN = 9

    , WWP_RC_MSG_FORMAT_ERROR = 101
    , WWP_RC_INVALID_MSG_TYPE = 102
    , WWP_RC_INVALID_PROTOCOL_VERSION = 103
    , WWP_RC_INVALID_SITE_ID = 104
    , WWP_RC_INVALID_SITE_SESSION_ID = 105
    , WWP_RC_INVALID_SEQUENCE_ID = 106
    , WWP_RC_INVALID_RESPONSE_CODE = 108

    , WWP_RC_UNKNOWN = 201

      // SPACE: 300 + WSI.WWP_CenterService.WigosService.Status
    , WWP_RC_SPACE_OK_TRANSFERRED = 301
    , WWP_RC_SPACE_OK_ALREADY_TRANSFERED = 302
    , WWP_RC_SPACE_ERROR_ACCOUNT_INFO = 320 
    , WWP_RC_SPACE_ERROR_NON_CASHABLE_GROUP = 322
    , WWP_RC_SPACE_ERROR_TRANSACTIONID_ALREADY_EXISTS = 330
    , WWP_RC_SPACE_ERROR_INTERNAL_SERVER_ERROR = 500
  }

  public enum WWP_TableTypes
  {
    TABLE_TYPE_UNKNOWN  = 0
  , TABLE_TYPE_CONFIGURATION = 1

  , TABLE_TYPE_POINTS_ACCOUNT_MOVEMENTS = 2  // UPLOAD_POINTS
  , TABLE_TYPE_ACCOUNT_PERSONAL_INFO = 3

  , TABLE_TYPE_DOWNLOAD_POINTS = 4
  , TABLE_TYPE_DOWNLOAD_PERSONAL_INFO = 5
  , TABLE_TYPE_SYNC_PERSONAL_INFO = 6

  , TABLE_TYPE_GET_CENTER_SEQUENCES = 7

  , TABLE_TYPE_UPLOAD_SALES_PER_HOUR = 8
  , TABLE_TYPE_UPLOAD_PLAY_SESSIONS = 9
  , TABLE_TYPE_UPLOAD_PROVIDERS = 10
  , TABLE_TYPE_UPLOAD_BANKS = 11
  , TABLE_TYPE_UPLOAD_AREAS = 12
  , TABLE_TYPE_UPLOAD_TERMINALS = 13
  , TABLE_TYPE_UPLOAD_TERMINALS_CONNECTION = 16

  , TABLE_TYPE_DOWNLOAD_PROVIDERS_GAMES = 17
  , TABLE_TYPE_UPLOAD_PROVIDERS_GAMES = 18

  , TABLE_TYPE_UPLOAD_GAME_PLAY_SESSIONS = 19

  , TABLE_TYPE_DOWNLOAD_PROVIDERS = 20
  , TABLE_TYPE_DOWNLOAD_MASTER_PROFILES = 21
  , TABLE_TYPE_UPLOAD_ACCOUNTS_DOCUMENTS = 22
  , TABLE_TYPE_DOWNLOAD_ACCOUNTS_DOCUMENTS = 23
  , TABLE_TYPE_DOWNLOAD_CORPORATE_USERS = 24
  , TABLE_TYPE_UPLOAD_LAST_ACTIVITY = 25
  , TABLE_TYPE_UPLOAD_CASHIER_SESIONS_GRUPED_BY_HOUR = 26
  , TABLE_TYPE_UPLOAD_ALARMS = 27
  , TABLE_TYPE_DOWNLOAD_ALARM_PATTERN = 28
  , TABLE_TYPE_DOWNLOAD_LCD_MESSAGES = 29
  , TABLE_TYPE_UPLOAD_HANDPAYS = 30

  , TABLE_TYPE_MULTISITE_REQUEST = 50
  , TABLE_TYPE_MULTISITE_RESPONSE = 51

  , TABLE_TYPE_UPLOAD_SELLS_AND_BUYS = 52

  , TABLE_TYPE_SPACE = 53
  , TABLE_TYPE_DOWNLOAD_BUCKETS_CONFIG = 54
  , TABLE_TYPE_UPLOAD_EXTERNAL_LINKED_ACCOUNT = 55
  , TABLE_TYPE_DOWNLOAD_EXTERNAL_LINKED_ACCOUNT = 56
  , TABLE_TYPE_UPLOAD_CREDITLINES = 57
  , TABLE_TYPE_UPLOAD_CREDITLINE_MOVEMENTS = 58

}

  /// <summary>
  /// The WWP message header
  /// </summary>
  public class WWP_MsgHeader  
  {
    #region CONSTANTS

    public const String PROTOCOL_VERSION = "1.0.0.1";
    
    #endregion // CONSTANTS

    #region MEMBERS

    // Only for internal use.
    private Boolean IsUser;

    private String ProtocolVersion;
    private DateTime SystemTime;
    public WWP_MsgTypes MsgType;
    public WWP_ResponseCodes ResponseCode;
    // For Site from/to Center messages.
    public Int32 SiteId = Site.Id;
    public Int64 SessionId;
    public Int64 SequenceId;
    // For iPad messages.
    public String Username;
    public String Password;
    public String TerminalId;

    #endregion // MEMBERS

    #region PUBLIC

    /// <summary>
    ///   Gets the Protocol Version
    /// </summary>
    public String GetProtocolVersion
    {
      get { return ProtocolVersion; }
    }

    /// <summary>
    /// Gets the System Time
    /// </summary>
    public DateTime GetSystemTime
    {
      get { return SystemTime; }
    }

    public void PrepareReply (WWP_MsgHeader RequestHeader)
    {
      ResponseCode = WWP_ResponseCodes.WWP_RC_ERROR; // Default return code

      IsUser = RequestHeader.IsUser;

      SiteId = RequestHeader.SiteId;
      SessionId = RequestHeader.SessionId;
      SequenceId = RequestHeader.SequenceId;

      Username = RequestHeader.Username;
      Password = "";  // Left blank, on purpouse.
      TerminalId = RequestHeader.TerminalId;
    }

    /// <summary>
    /// Extracts an enum object value from a 
    /// Enum Object giving the string name of 
    /// the value and getting the index.
    /// </summary>
    /// <param name="ResponseCode">Response Code</param>
    /// <returns>The reply header</returns>
    public static Object Decode(String str, Object defaultValue)
    {
      foreach (String s in System.Enum.GetNames(defaultValue.GetType()))
      {
        if (s.Equals(str))
        {
          return System.Enum.Parse(defaultValue.GetType(), s);
        }
      }
      return defaultValue;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Parse a XML Document and save values into the properties of this class.
    //
    //  PARAMS :
    //      - INPUT :
    //          - XmlReader Reader
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public void LoadXml(XmlReader reader)
    {
      ProtocolVersion = XML.GetValue(reader, "ProtocolVersion");
      DateTime.TryParse(XML.GetValue(reader, "SystemTime"), out SystemTime);
      MsgType = (WWP_MsgTypes)Decode(XML.GetValue(reader, "MsgType"), WWP_MsgTypes.WWP_MsgUnknown);
      ResponseCode = (WWP_ResponseCodes)Decode(XML.GetValue(reader, "ResponseCode"), WWP_ResponseCodes.WWP_RC_UNKNOWN);

      while (reader.Name != "SiteId" && reader.Name != "Username")
      {
        reader.Read();
      }

      IsUser = false;

      if (reader.Name == "SiteId")
      {
        Int32.TryParse(XML.GetValue(reader, "SiteId"), out SiteId);
        Int64.TryParse(XML.GetValue(reader, "SessionId"), out SessionId);
        Int64.TryParse(XML.GetValue(reader, "SequenceId"), out SequenceId);
      }
      else if (reader.Name == "Username")
      {
        Username = XML.GetValue(reader, "Username");
        Password = XML.GetValue(reader, "Password");
        TerminalId = XML.GetValue(reader, "TerminalId");
        Int64.TryParse(XML.GetValue(reader, "SequenceId"), out SequenceId);

        IsUser = true;
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Convert the properties of this class to a XML String representation.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - String
    //
    public String ToXml()
    {
      DateTime now;
      string str;

      now = DateTime.Now;
      str = XML.XmlDateTimeString(now);

      StringBuilder str_builder;

      str_builder = new StringBuilder();

      str_builder.Append("<WWP_MsgHeader><ProtocolVersion>");

      str_builder.Append(ProtocolVersion);
      str_builder.Append("</ProtocolVersion><SystemTime>");
      str_builder.Append(str);
      str_builder.Append("</SystemTime><MsgType>");
      str_builder.Append(MsgType);
      str_builder.Append("</MsgType><ResponseCode>");
      str_builder.Append(ResponseCode);
      str_builder.Append("</ResponseCode>");
      if (!IsUser)
      {
        str_builder.Append("<SiteId>");
        str_builder.Append(SiteId);
        str_builder.Append("</SiteId><SessionId>");
        str_builder.Append(SessionId);
        str_builder.Append("</SessionId><SequenceId>");
        str_builder.Append(SequenceId);
        str_builder.Append("</SequenceId>");
      }
      else
      {
        str_builder.Append("<Username>");
        str_builder.Append(Username);
        str_builder.Append("</Username><Password>");
        str_builder.Append(Password);
        str_builder.Append("</Password><TerminalId>");
        str_builder.Append(TerminalId);
        str_builder.Append("</TerminalId><SequenceId>");
        str_builder.Append(SequenceId);
        str_builder.Append("</SequenceId>");
      }
      str_builder.Append("</WWP_MsgHeader>");

      return str_builder.ToString();
    }

    /// <summary>
    /// Creates a header
    /// </summary>
    public WWP_MsgHeader ()
    {
      Init ();
    }

    #endregion // PUBLIC

    #region PRIVATE

    /// <summary>
    /// Initializes the header
    /// </summary>
    private void Init()
    {
      ProtocolVersion = PROTOCOL_VERSION;
      SystemTime = System.DateTime.Now;
      MsgType = WWP_MsgTypes.WWP_MsgUnknown;
      ResponseCode = WWP_ResponseCodes.WWP_RC_OK;

      IsUser = false;

      SiteId = Site.Id;
      SessionId = 0;
      SequenceId = 0;

      Username = "";
      Password = "";
      TerminalId = "";
    }

    #endregion // PRIVATE
  }
}
