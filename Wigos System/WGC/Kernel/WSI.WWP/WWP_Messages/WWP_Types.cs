//------------------------------------------------------------------------------
// Copyright � 2011 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WWP_MsgTypes.cs
// 
//   DESCRIPTION: WWP Message Types
// 
//        AUTHOR: Alberto Cuesta
// 
// CREATION DATE: 03-FEB-2011
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 03-FEB-2011 ACC    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Xml;
using WSI.Common;
using System.IO;

namespace WSI.WWP
{
  public static class Protocol
  {
    public const int    PORT_NUMBER  = 65533;          // Listener port number
    public const String SERVICE_NAME = "WWP_Service";  // Service Name
  }

  public class WWP_LicenseServer
  {
    public String Name;
    public String MacAddress;
    public String VolumeSerialNumber;

    public WWP_LicenseServer()
    {
    }

    public WWP_LicenseServer(String ServerXml)
    {
      XmlDocument _doc;

      try
      {
        _doc = new XmlDocument();
        _doc.LoadXml(ServerXml);

        Name = _doc.DocumentElement.Attributes["Name"].Value;
        MacAddress = _doc.DocumentElement.Attributes["MacAddress"].Value;
        VolumeSerialNumber = _doc.DocumentElement.Attributes["VolumeSerialNumber"].Value;
      }
      catch
      {
      }
    }

  } // WWP_LicenseServer

  public static class Site
  {
    private static Int32 m_id = 0;
    private static String m_name = "";
    private static String m_mac_address = "";

    public static Int32 Id
    {
      get { return m_id; }
      set { m_id = value; }
    }

    public static String Name
    {
      get { return m_name; }
      set { m_name = value; }
    }

    public static String ServerMacAddress
    {
      get { return Site.m_mac_address; }
      set { Site.m_mac_address = value; }
    }
  } // Site

}
