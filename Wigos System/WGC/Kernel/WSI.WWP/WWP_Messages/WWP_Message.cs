//------------------------------------------------------------------------------
// Copyright � 2011 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WWP_Mesage.cs
// 
//   DESCRIPTION: WWP_Mesage class
// 
//        AUTHOR: Alberto Cuesta
// 
// CREATION DATE: 03-FEB-2011
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 03-FEB-2011 ACC    First release.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Reflection;
using WSI.Common;


namespace WSI.WWP
{
  public interface IXml
  {
    String ToXml();
    void LoadXml(XmlReader Xml);
  }

  public class WWP_Exception : Exception
  {
    WWP_ResponseCodes response_code;

    public WWP_ResponseCodes ResponseCode
    {
      get
      {
        return response_code;
      }
    }

    public WWP_Exception (WWP_ResponseCodes Value)
    {
      response_code = Value;
    }
  }

  public partial class WWP_Message
  {
    public WWP_MsgHeader MsgHeader;
    public IXml MsgContent;

    private WWP_Message ()
    {
    }

    public static WWP_Message CreateMessage(String Xml)
    {
      WWP_Message WWP_message;

      WWP_message = null;

      try
      {
        XmlReader xmlreader;

        bool found;

        WWP_MsgHeader hdr;
        hdr = new WWP_MsgHeader();
        System.IO.StringReader textreader = new System.IO.StringReader(Xml);
        xmlreader = XmlReader.Create(textreader);

        //Loads the message header
        hdr.LoadXml(xmlreader);

        if ( hdr.MsgType == WWP_MsgTypes.WWP_MsgUnknown )
        {
          return null;
        }

        //Creates the message if the message header is correct.
        WWP_message = CreateMessage(hdr.MsgType);
        //Assign the header to the current message.
        WWP_message.MsgHeader = hdr;

        //Put the xmlreader at the beginning of the Message Content
        found = false;
        while (xmlreader.Read() && !found)
        {
          if ((xmlreader.Name == "WWP_MsgContent") && (xmlreader.NodeType.Equals(XmlNodeType.Element)))
          {
            found = true;
          }
        }
        if (found)
        {
          WWP_message.MsgContent.LoadXml(xmlreader);
          if (WWP_message.MsgContent == null)
          {
            Log.Error("Tag not found: " + WWP_message.MsgHeader.MsgType.ToString());
            throw new WWP_Exception(WWP_ResponseCodes.WWP_RC_MSG_FORMAT_ERROR);
          }
        }
        else
        {
          Log.Error("Tag not found: " + WWP_message.MsgHeader.MsgType.ToString());
          throw new WWP_Exception(WWP_ResponseCodes.WWP_RC_MSG_FORMAT_ERROR);
        }
        xmlreader.Close();
      }
      catch (WWP_Exception WWP_ex)
      {
        throw WWP_ex;
      }
      catch (Exception ex)
      {
        // Format Error in message
        Log.Exception(ex);

        throw new WWP_Exception(WWP_ResponseCodes.WWP_RC_MSG_FORMAT_ERROR);
      }
      return WWP_message;
    }

    public static WWP_Message CreateResponse (WWP_Message Request)
    {
      WWP_Message _response;
      WWP_MsgTypes _request_type;
      WWP_MsgTypes _response_type;

      _request_type = Request.MsgHeader.MsgType;

      if (_request_type.ToString().Contains("Reply")
          || _request_type == WWP_MsgTypes.WWP_MsgUnknown
          || _request_type == WWP_MsgTypes.WWP_MsgError)
      {
        _response_type = WWP_MsgTypes.WWP_MsgError;
      }
      else
      {
        _response_type = _request_type + 1;
      }

      _response = CreateMessage(_response_type);
      _response.MsgHeader.PrepareReply(Request.MsgHeader);

      return _response;
    }

    public static WWP_Message CreateMessage(WWP_MsgTypes MsgType)
    {
      WWP_Message _wwp_msg;
      IXml content;

      Assembly assembly;
      String type_name;
      Type target_type;

      _wwp_msg = new WWP_Message();

      _wwp_msg.MsgHeader = new WWP_MsgHeader();
      _wwp_msg.MsgHeader.MsgType = MsgType;

      // The Power of Reflection
      content = null;

      type_name = "WSI.WWP." + MsgType.ToString();

      assembly = Assembly.GetExecutingAssembly();
      target_type = assembly.GetType(type_name);

      Type[] types = new Type[0];
      ConstructorInfo info = target_type.GetConstructor(types);
      content = (IXml)info.Invoke(null);

      _wwp_msg.MsgContent = content;
      

      return _wwp_msg;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Convert the properties of this class to a XML String representation.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - String
    //
    public String ToXml()
    {
      StringBuilder str_builder;

      str_builder = new StringBuilder();
      String xml_header;
      String xml_content;

      xml_header = MsgHeader.ToXml ();
      xml_content = MsgContent.ToXml ();

      str_builder.Append("<WWP_Message>");
      str_builder.Append(xml_header);
      str_builder.Append("<WWP_MsgContent>");
      str_builder.Append(xml_content);
      str_builder.Append("</WWP_MsgContent></WWP_Message>");

      return str_builder.ToString();
    }
  }

  // Class Msg Error
  public class WWP_MsgError : IXml
  {
    public String Description;

    public String ToXml()
    {
      return "<WWP_MsgError><Description>" 
              + XML.StringToXmlValue(Description) 
              + "</Description></WWP_MsgError>";
    }

    public void LoadXml(XmlReader reader)
    {
      if (reader.Name != "WWP_MsgError")
      {
        throw new WWP_Exception(WWP_ResponseCodes.WWP_RC_MSG_FORMAT_ERROR);
      }
      else
      {
        Description = XML.GetValue(reader, "Description");
      }
    }
  }

}
