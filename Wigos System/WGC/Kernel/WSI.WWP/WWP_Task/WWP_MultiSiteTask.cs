//------------------------------------------------------------------------------
// Copyright � 2011 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WWP_MsgTable.cs
// 
//   DESCRIPTION: Process for Table message.
// 
//        AUTHOR: Joaquim Cid
// 
// CREATION DATE: 12-MAR-2013
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 12-MAR-2013 JCM    First release.
//                    Moved Set_ReportingFlag and Read_ReportingFlag.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using WSI.Common;
using System.Data.SqlClient;
using System.Data;

namespace WSI.WWP.WWP_Task
{
  public partial class WWP_MultiSiteTask
  {

        //------------------------------------------------------------------------------
    // PURPOSE : Sets Reporting Flag 
    //
    //  PARAMS :
    //      - INPUT :
    //          - ReportFlagValue:    New Value of Reporting Flag 
    //          - SequenceId          Last sequence id
    //          - Trx:                Transaction
    //      - OUTPUT :
    //          - None
    // RETURNS :
    //          - True:               Executed succesfully
    //          - Flase:              Executed unsuccesfully
    //
    //   NOTES : 
    public static bool Set_ReportingFlag(TYPE_MULTISITE_SITE_TASKS TaskId, Boolean IsRunning, Int64 SequenceId, out Boolean EnabledAndRunning, SqlTransaction Trx)
    {
      return Set_ReportingFlag(-1, -1, TaskId, IsRunning, SequenceId, out EnabledAndRunning, Trx);
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Sets Reporting Flag 
    //
    //  PARAMS :
    //      - INPUT :
    //          - ReportFlagValue:    New Value of Reporting Flag 
    //          - SequenceId          Last sequence id
    //          - Trx:                Transaction
    //      - OUTPUT :
    //          - None
    // RETURNS :
    //          - True:               Executed succesfully
    //          - Flase:              Executed unsuccesfully
    //
    //   NOTES : 
    public static bool Set_ReportingFlag(Int32 SiteId, Int32 Pending, TYPE_MULTISITE_SITE_TASKS TaskId, Boolean IsRunning, Int64 SequenceId, out Boolean EnabledAndRunning, SqlTransaction Trx)
    {

      StringBuilder _sb;
      StringBuilder _count;

      EnabledAndRunning = false;

      try
      {
        _sb = new StringBuilder();
        _count = new StringBuilder();

        if (Pending > 0 && SiteId > 0)
        {
          switch (TaskId)
          {
            case TYPE_MULTISITE_SITE_TASKS.UploadPointsMovements:
              _count.Append("SELECT COUNT(*) FROM MS_SITE_PENDING_ACCOUNT_MOVEMENTS");
              break;

            case TYPE_MULTISITE_SITE_TASKS.UploadPersonalInfo:
              _count.Append("SELECT COUNT(*) FROM MS_SITE_PENDING_ACCOUNTS");
              break;

            case TYPE_MULTISITE_SITE_TASKS.DownloadPersonalInfo_Card:
              _count.Append("SELECT COUNT(*) FROM MS_SITE_SYNCHRONIZE_ACCOUNTS WHERE SSA_TODAY >= GETDATE() "); // TODO!!! Last Sync
              break;

            default:
              _count.Append("0");
              break;
          } // switch
        }
        else if (Pending > 0)
        {
          _count.Append(" @pNumPending ");
        } 
        else
        {
          _count.Append(" ST_NUM_PENDING ");
        }

        _sb.AppendLine("DECLARE @pTaskEnabled AS BIT");
        _sb.AppendLine("DECLARE @pNewRunning  AS BIT");
        _sb.AppendLine("SET @pTaskEnabled = ISNULL(( SELECT ST_ENABLED FROM MS_SITE_TASKS WHERE ST_TASK_ID = @pTaskId ), 0)");
        _sb.AppendLine("SET @pNewRunning  = CASE WHEN ( @pIsRunning = 1 ) AND ( @pTaskEnabled = 1 ) THEN 1 ELSE 0 END ");
        _sb.AppendLine("UPDATE   MS_SITE_TASKS                      ");
        _sb.AppendLine("   SET   ST_RUNNING            = @pNewRunning");
        _sb.AppendLine("       , ST_STARTED            = CASE WHEN (ST_RUNNING = 0 AND @pNewRunning = 1) THEN GETDATE() ELSE ST_STARTED END ");
        _sb.AppendLine("       , ST_LAST_RUN           = CASE WHEN (ST_RUNNING = 1 AND @pNewRunning = 0) THEN ST_STARTED ELSE ST_LAST_RUN END ");
        _sb.AppendLine("       , ST_LOCAL_SEQUENCE_ID  = CASE WHEN (@pSequenceId > ISNULL(ST_LOCAL_SEQUENCE_ID, 0)) THEN @pSequenceId ELSE ST_LOCAL_SEQUENCE_ID END");
        _sb.AppendLine("       , ST_NUM_PENDING        = (" + _count.ToString() + ")");
        _sb.AppendLine(" WHERE   ST_TASK_ID            = @pTaskId ");
        _sb.AppendLine("SELECT   @pTaskEnabled, @pNewRunning");

        // TODO: Falta el Site ID

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pIsRunning", SqlDbType.Bit).Value = IsRunning ? 1 : 0;
          _cmd.Parameters.Add("@pTaskId", SqlDbType.Int).Value = TaskId;
          _cmd.Parameters.Add("@pSequenceId", SqlDbType.BigInt).Value = SequenceId;
          _cmd.Parameters.Add("@pNumPending", SqlDbType.Int).Value = Pending;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (_reader.Read())
            {
              EnabledAndRunning = (_reader.GetBoolean(0) && _reader.GetBoolean(1));

              return true;
            }
          }

          return false;
        }

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Read Reporting Flag 
    //
    //  PARAMS :
    //      - INPUT :
    //          - ReportFlagValue:    Value of Reporting Flag 
    //      - OUTPUT :
    //          - ReportFlagValue
    //          - WaitSeconds
    //          - SequenceId
    //
    // RETURNS :
    //          - True:               Executed succesfully
    //          - Flase:              Executed unsuccesfully
    //
    //   NOTES :
    public static bool Read_ReportingFlagValues(TYPE_MULTISITE_SITE_TASKS TypeFlag, out Boolean IsRunning, out Int32 WaitSeconds, out Int64 SequenceId, SqlTransaction Trx)
    {
      return Read_ReportingFlagValues(-1, TypeFlag, out IsRunning, out WaitSeconds, out SequenceId, Trx);
    }
    public static bool Read_ReportingFlagValues(Int32 SiteId, TYPE_MULTISITE_SITE_TASKS TypeFlag, out Boolean IsRunning, out Int32 WaitSeconds, out Int64 SequenceId, SqlTransaction Trx)
    {
      StringBuilder _sb;

      WaitSeconds = 0;
      IsRunning = false;
      SequenceId = 0;

      try
      {

        _sb = new StringBuilder();
        _sb.AppendLine(" SELECT   ST_RUNNING            ");
        _sb.AppendLine("        , ST_INTERVAL_SECONDS   ");
        _sb.AppendLine("        , ST_LOCAL_SEQUENCE_ID  ");
        _sb.AppendLine("   FROM   MS_SITE_TASKS         ");
        _sb.AppendLine("  WHERE   ST_TASK_ID = @pTaskId ");
        _sb.AppendLine(SiteId > 0 ? " AND ST_SITE_ID = @pSiteId" : "");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pTaskId", System.Data.SqlDbType.Int).Value = TypeFlag;
          _cmd.Parameters.Add("@pSiteId", System.Data.SqlDbType.Int).Value = SiteId;

          using (SqlDataReader _sql_reader = _cmd.ExecuteReader())
          {
            if (!_sql_reader.Read())
            {
              return false;
            }

            IsRunning = _sql_reader.IsDBNull(0) ? false : _sql_reader.GetBoolean(0); ;
            // TODO: IF IS NULL TAKE 0 MINUTES AND REFRESH EVER 0 MINUTES ?
            WaitSeconds = _sql_reader.IsDBNull(1) ? 0 : _sql_reader.GetInt32(1);
            SequenceId = _sql_reader.IsDBNull(2) ? 0 : _sql_reader.GetInt64(2);

            return true;

          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }
  }
}
