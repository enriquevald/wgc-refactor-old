//------------------------------------------------------------------------------
// Copyright � 2010 Win Systems Ltd.
//------------------------------------------------------------------------------
//
//   MODULE NAME : WCP_AccountsCreditsExpiration.cs
//
//   DESCRIPTION : WCP_AccountsCreditsExpiration class
//
// REVISION HISTORY :
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 26-FEB-2010 ACC    First release.
// 20-OCT-2010 TJG    Gifts Expiration
// 05-NOV-2010 RCI    Gifts Expiration for type Service
// 07-MAR-2012 MPO    AccountsLevels --> Avoid anonymous account become level 1 when AC_HOLDER_LEVEL_EXPIRATION is not null
// 02-MAY-2012 DDM    GenerateSegmentationReport --> Create Segmentation Report when is new month
// 07-MAY-2012 JCM    GenerateSegmentationReport --> Segmentation Report,
//                    new frequency of execution: Every day check if has been generated previous month report
// 09-MAY-2012 JCM    Fixed Bug #295: Generate DataTable only if there are not open sessions and is necessary 
// 22-MAY-2012 DDM    Added ProcessPeriodicPromotions, AccountsPromotionExpiration and ActivateAwardedPromotion.
// 16-AUG-2012 RCI    Release inactive play sessions and block related accounts.
// 23-OCT-2012 RCI    Fixed Bug: Error expiring redeemable credits.
// 07-JAN-2013 RCI & AJQ   Fixed bug #475: AccountsLevels: Rewritten NoActivity check in levels.
// 23-JAN-2013 DDM    Fixed Bug #470.
// 05-FEB-2013 JCM    Revised Message Error Log on PeriodicJobThread()
// 19-FEB-2013 LEM    Added new routine for the automatic closing of the system cashier sessions preceding the current day, ClosePreviousDaySystemCashierSession.
// 16-MAY-2013 RBG    Eliminated function to calculate Hourly Liabilities
// 13-AUG-2013 SMN    Added new routine: Create_AML_Daily
// 11-NOV-2013 JRM    Added new process, Tito ticket expiration
// 22-NOV-2013 SMN    Delete job Create_CashierMovementsGroupedDate
//                    Call jobs: CashierMovementsOldHistoryById and CashierMovementsOldHistoryByHour
// 03-JAN-2014 JPJ    Insert a Redeemable credit expiration operation.
// 21-JAN-2014 JPJ    Insert the chashier movements associated with TITO tickets expirations.
// 04-FEB-2014 ICS    Added new process, Tito ticket offline discard
// 03-MAR-2014 ICS    Added support to discard Tito handpay tickets redeemed offline
// 11-MAR-2014 SMN    Moved calls functions: CashierMovementsOldHistoryById, CashierMovementsOldHistoryByHour to HistoricalCashierSessionsThread
// 27-MAY-2014 AJQ    New expiration routines for redeemable credits
// 04-DEC-2014 XCD    Apply daily expiration credits on anonymous accounts. Renamed function AccountsRedeemableCreditsDailyExpirationDueToLowBalance to AccountsRedeemableCreditsDailyExpiration
// 13-DEC-2014 OPC    Added new function for undo handpay's.
// 15-ENE-2014 JML    Product Backlog Item 151:AccountPointsCache --> Task 154: account_points_cache maintenance
// 22-APR-2015 AMF    Backlog Item 1128
// 11-NOV-2015 FOS    Product Backlog Item: 3709 Payment tickets & handpays
// 23-NOV-2015 FOS    Bug 6771:Payment tickets & handpays
// 18-DEC-2015 FOS    Fixed Bug 6769: It doesn't delete taxes when TITO tickets are paid
// 13-JAN-2016 JML    Product Backlog Item 2541: MultiSite Multicurrency PHASE3: Loyalty program in the sites.
// 19-JAN-2016 JRC    Product Backlog Item 7909: Multiple buckets. Waking up the thread
// 21-JAN-2016 FGB    Product Backlog Item 8300: Multiple buckets: Caducidad de buckets
// 08-MAR-2016 DLL    Fixed Bug 10409: Voided Handpay don't set as Expired
// 20-JUN-2016 LTC    Product Backlog Item 13612,13613,13614: Witholding Evidence.
// 27-JUN-2016 LTC    Bug 14878:Witholding Evidence Generation: error in windows thread - wcp service
// 30-NOV-2016 RAB    PBI 21157: Withholding generation - Thread.
// 22-MAR-2017 RAB    PBI 25973: MES10 Ticket validation - Dealer copy link to TITO ticket
// 29-MAR-2017 AMF    WIGOS-366: CreditLine - Expire a CreditLine
// 21-APR-2017 AMF    WIGOS-706: Junkets - Technical story
// 26-JUN-2017 FJC    WIGOS-2878 Terminal draw. Play sessions opened for over a month
// 27-JUN-2017 FJC    WIGOS-2878 Terminal draw. Play sessions opened for over a month (optimized query by DBA)
// 07-SEP-2017 DPC    WIGOS-4925: WS2S - Bucket expiration
// 21-JUN-2018 FJC    WIGOS-13083 [Ticket #14935] bloqueo de tarjeta de playerclub

//------------------------------------------------------------------------------

using System;
using System.Messaging;
using System.Collections.Generic;
using System.Text;
using System.Net;
using WSI.Common;
using System.Xml;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using WSI.WCP;
using System.Collections;
using System.Threading;
using WSI.Common.TITO;
using WSI.Common.CreditLines;
using WSI.Common.Junkets;

namespace WSI.WCP
{
  //------------------------------------------------------------------------------
  // PURPOSE : WCP_AccountsCreditsExpiration class 
  //
  //   NOTES :

  public static class WCP_AccountsCreditsExpiration
  {
    private const Int32 WAIT_FOR_NEXT_TRY = (5 * 60 * 1000);      // 5 minutes
    private const Int32 GIFT_EXPIRATION_CHECK = 12;               // 60 minutes = 12 * WAIT_FOR_NEXT_TRY
    private const Int32 TITO_TICKET_EXPIRATION_CHECK = 12;        // 60 minutes = 12 * WAIT_FOR_NEXT_TRY
    private const Int32 TITO_OFFLINE_TICKET_DISCARD_CHECK = 12;   // 60 minutes = 12 * WAIT_FOR_NEXT_TRY
    private const Int32 TITO_OFFLINE_TICKETS_DISCARD_DAYS = 2;    // Days to check if there are any redimed offline tickets
    private const Int32 UPDATE_HANDPAYS_CHECK = 12;               // 60 minutes = 12 * WAIT_FOR_NEXT_TRY
    //LTC 27-JUN-2016
    private const Int32 NUM_JOBS = 25;                            // Number of Jobs to perform on thread

    static Thread m_thread;

    static Int32 m_gift_expiration_check;             // Gift expiration timer counter to make the check every hour instead of every 5 minutes
    static Int32 m_tito_ticket_expiration_check;      // TITO ticket expiration timer counter to make the check every hour instead of every 5 minutes
    static Int32 m_tito_offline_ticket_discard_check; // TITO offline ticket discard timer counter to make the check every hour instead of every 5 minutes

    static DateTime m_credits_expiration_last_run;
    static DateTime m_retry_running_segmentation_rpt;
    static DateTime m_last_closed_system_session;
    static DateTime m_daily_expire_last_execution;
    static DateTime m_daily_expire_last_handpay_execution;

    //------------------------------------------------------------------------------
    // PURPOSE : Initialize WCP_AccountsCreditsExpiration class 
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :

    public static void Init()
    {
      m_thread = new Thread(PeriodicJobThread);
      m_thread.Name = "PeriodicJobThread";

      m_gift_expiration_check = -1;   // Force gift expiration after first WAIT_FOR_NEXT_TRY period (5 mins).
      m_tito_ticket_expiration_check = -1;   // Force tickets expiration after first WAIT_FOR_NEXT_TRY period (5 mins).
      m_tito_offline_ticket_discard_check = -1;   // Force tickets discard after first WAIT_FOR_NEXT_TRY period (5 mins).
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Public function for thread starts
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :

    public static void Start()
    {
      // Thread starts
      m_thread.Start();
    }

    #region Private Functions

    //------------------------------------------------------------------------------
    // PURPOSE : Main thread
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    static private void PeriodicJobThread()
    {
      SqlConnection _sql_conn;
      Int32 _step;
      Int32 _tick0;
      Boolean _is_multisite_member;
      Boolean _is_tito_mode;
      Int64[] _ellapsed;
      DateTime _yesterday;

      _ellapsed = new Int64[NUM_JOBS];

      _is_tito_mode = Common.TITO.Utils.IsTitoMode();

      // Initialize last running Date
      _yesterday = WGDB.Now.Date.AddDays(-1);
      m_credits_expiration_last_run = _yesterday;
      m_retry_running_segmentation_rpt = _yesterday;
      m_last_closed_system_session = _yesterday;
      m_daily_expire_last_execution = _yesterday;
      m_daily_expire_last_handpay_execution = _yesterday;

      CommonStepJobs.Init();

      _is_multisite_member = false;
      while (true)
      {        
        System.Threading.Thread.Sleep(WAIT_FOR_NEXT_TRY);
        //System.Threading.Thread.Sleep(60000);

        // AJQ 19-AUG-2013, Only the service running as 'Principal' will execute the tasks.
        if (!Services.IsPrincipal("WCP"))
        {
          continue;
        }

        // RCI & ACM 06-AUG-2013: Upgrade account documents from old format to new one.
        AccountDocuments.UpgradeToNewFormat();

        // ACM 11-OCT-2013: Update accounts documents from an old document type to a new one.
        AccountDocuments.UpdateToNewDocumentType();
        _is_multisite_member = GeneralParam.GetBoolean("Site", "MultiSiteMember", false);

        _sql_conn = null;
        _step = 0;

        try
        {
          //
          // Connect to DB
          //
          _sql_conn = WGDB.Connection();

          if (_sql_conn == null)
          {
            continue;
          }

          // Initializate the _ellapsed array
          for (int _idx_step = 0; _idx_step < NUM_JOBS; _idx_step++)
          {
            _ellapsed[_idx_step] = 0;
          }

          // JCM 12-FEB-2013: Cycle Credit and Passive (Cash/Credit on Player Accounts)
          _step = 0;
          _tick0 = Environment.TickCount;
          if (!Accounts.UpdateHourlyLiabilities())
          {
            Log.Warning("WCP_Users.UsersThread. Exception in function: " + "UpdateHourlyLiabilities");
          }
          _ellapsed[_step] = Misc.GetElapsedTicks(_tick0);

          //
          // Close SYS-SYSTEM Cashier Session if the open date is NOT in the current day.
          //       
          _step++;
          _tick0 = Environment.TickCount;
          ClosePreviousDaySystemCashierSession();
          _ellapsed[_step] = Misc.GetElapsedTicks(_tick0);

          // RCI 16-AUG-2012: Release session due to inactivity
          _step++;
          _tick0 = Environment.TickCount;
          ReleaseInactiveSessions();
          _ellapsed[_step] = Misc.GetElapsedTicks(_tick0);

          //
          // Account Flags Expiration
          //
          _step++;
          _tick0 = Environment.TickCount;
          AccountFlag.AccountsFlagsExpiration();
          _ellapsed[_step] = Misc.GetElapsedTicks(_tick0);

          //
          // Process Periodic Promotions
          //
          _step++;
          _tick0 = Environment.TickCount;
          WCP_PeriodicPromotions.ProcessPeriodicPromotions();
          _ellapsed[_step] = Misc.GetElapsedTicks(_tick0);

          //
          // Accounts Promotions Expiration
          //
          _step++;
          _tick0 = Environment.TickCount;
          AccountPromotion.AccountsPromotionExpiration();
          _ellapsed[_step] = Misc.GetElapsedTicks(_tick0);

          //
          // Accounts Promotions Activate
          //
          _step++;
          _tick0 = Environment.TickCount;
          AccountPromotion.ActivateAwardedPromotion(0);
          _ellapsed[_step] = Misc.GetElapsedTicks(_tick0);

          //
          // Generate Segmentation Report
          //
          _sql_conn = WGDB.Reconnect(_sql_conn);
          _step++;
          _tick0 = Environment.TickCount;
          GenerateSegmentationReport(_sql_conn);
          _ellapsed[_step] = Misc.GetElapsedTicks(_tick0);

          //
          // Bucket Expiration by Inactivity Days (Old POINTS Expiration)
          //
          _step++;
          if (CommonMultiSite.GetPlayerTrackingMode() == PlayerTracking_Mode.Site)
          {
            _tick0 = Environment.TickCount;
            // SMN: Se ha sustituido los puntos por un Bucket (Puntos de canje).
            ///CommonStepJobs.AccountsPointsExpiration();
            if (!Buckets.BucketsExpirationByInactivityDays())
            {
              Log.Warning("Exception in function: Buckets.BucketsExpiration Step:" + _tick0.ToString());
            } 
            _ellapsed[_step] = Misc.GetElapsedTicks(_tick0);
          }

          //
          // REDEEMABLE Credits Expiration
          //
          _step++;
          _tick0 = Environment.TickCount;
          AccountsRedeemableCreditsExpiration();
          _ellapsed[_step] = Misc.GetElapsedTicks(_tick0);

          //
          // GIFTS Expiration
          //
          _sql_conn = WGDB.Reconnect(_sql_conn);
          _step++;
          _tick0 = Environment.TickCount;
          GiftsExpiration(_sql_conn);
          _ellapsed[_step] = Misc.GetElapsedTicks(_tick0);

          //
          // Accounts Levels (Upgrade & Downgrade)
          //
          _step++;
          if (CommonMultiSite.GetPlayerTrackingMode() == PlayerTracking_Mode.Site)
          {
            _sql_conn = WGDB.Reconnect(_sql_conn);
            _tick0 = Environment.TickCount;
            CommonStepJobs.AccountsLevels(_sql_conn);
            _ellapsed[_step] = Misc.GetElapsedTicks(_tick0);
          }

          // Create AML Daily
          _step++;
          _tick0 = Environment.TickCount;
          if (!Accounts.Create_AML_Daily())
          {
            Log.Warning("WCP_Users.UsersThread. Exception in function: " + "Create_AML_Daily");
          }
          _ellapsed[_step] = Misc.GetElapsedTicks(_tick0);

          // Draws
          _step++;
          _tick0 = Environment.TickCount;
          CashDeskDrawBusinessLogic.UploadDraws();
          _ellapsed[_step] = Misc.GetElapsedTicks(_tick0);

          // Enable/Disable Gifts --> In Process
          // JML 08-OCT-2013
          _step++;
          _tick0 = Environment.TickCount;
          if (!WSI.Common.PointsToCredit.EnableDisableGifts())
          {
            Log.Warning("WCP_Users.UsersThread. Exception in function: " + "EnableDisableGifts");
          }
          _ellapsed[_step] = Misc.GetElapsedTicks(_tick0);

          //
          // Tito ticket expiration
          //
          _step++;
          if (_is_tito_mode)
          {
            _tick0 = Environment.TickCount;
            TitoTicketExpiration();
            _ellapsed[_step] = Misc.GetElapsedTicks(_tick0);
          }

          //
          // Tito offline ticket discard
          // ICS 04-FEB-2014
          _step++;

          if (_is_tito_mode)
          {
            _tick0 = Environment.TickCount;
            TitoOfflineTicketDiscard();
            _ellapsed[_step] = Misc.GetElapsedTicks(_tick0);
          }

          //
          // Update expired handpays.
          // OPC 30-OCT-2014
          _step++;
          _tick0 = Environment.TickCount;
          HandpayExpiration();
          _ellapsed[_step] = Misc.GetElapsedTicks(_tick0);

          // Clear ACCOUNT_POINTS_CACHE (past registers)
          // JML 13-JAN-2015
          _step++;
          _tick0 = Environment.TickCount;
          if (!CommonStepJobs.ClearAccountPointsCache())
          {
            Log.Warning("Exception in function: CommonStepJobs.ClearAccountPointsCache. Step:" + _tick0.ToString());
          }
          _ellapsed[_step] = Misc.GetElapsedTicks(_tick0);

          // Win/Loss Statments
          // AMF 23-APR-2015
          _step++;
          _tick0 = Environment.TickCount;
          if (!WCP_AccountsWinLossStatements.WinLossStatements())
          {
            Log.Warning("Exception in function: WCP_AccountsWinLossStatements.WinLossStatements Step:" + _tick0.ToString());
          }
          _ellapsed[_step] = Misc.GetElapsedTicks(_tick0);

          // Delete Empty Buckets
          // FGB 20-JAN-2016
          _step++;
          _tick0 = Environment.TickCount;
          if (!Buckets.BucketsEmpty())
          {
            Log.Warning("Exception in function: Buckets.BucketsEmpty Step:" + _tick0.ToString());
          }
          _ellapsed[_step] = Misc.GetElapsedTicks(_tick0);

          // Delete Empty Buckets When account_id 0
          // RLO 07-MAR-2017
          _step++;
          _tick0 = Environment.TickCount;
          if (!Buckets.BucketsCustomerIdEmpty())
          {
            Log.Warning("Exception in function: Buckets.BucketsCustomerIdEmpty Step:" + _tick0.ToString());
          }
          _ellapsed[_step] = Misc.GetElapsedTicks(_tick0);

          // AMF 29-MAR-2017
          _step++;
          _tick0 = Environment.TickCount;
          if (!CreditLine.ExpireApprovedCreditLines())
          {
            Log.Warning("Exception in function: CreditLine.ExpireApprovedCreditLines Step:" + _tick0.ToString());
          }
          _ellapsed[_step] = Misc.GetElapsedTicks(_tick0);

          // AMF 21-APR-2017
          _step++;
          _tick0 = Environment.TickCount;
          if (!JunketsBusinessLogic.ProcessCommissionsPending())
          {
            Log.Warning("Exception in function: JunketsBusinessLogic.ProcessCommissionsPending Step:" + _tick0.ToString());
          }
          _ellapsed[_step] = Misc.GetElapsedTicks(_tick0);

          // CODE
          _step++;
          _ellapsed[_step] = 0;
          for (int _idx_step = 0; _idx_step < _step; _idx_step++)
          {
            if (_ellapsed[_idx_step] > 10000)
            {
              Log.Warning("PeriodicJob.Step[" + _idx_step.ToString() + "], Duration: " + _ellapsed[_idx_step].ToString() + " ms.");
            }
          }

        }
        catch (Exception _ex)
        {
          String _function = "NONE";

          switch (_step)
          {
            case 0:
              _function = "UpdateHourlyLiabilities";
              break;
            case 1:
              _function = "ClosePreviousDaySystemCashierSession";
              break;
            case 2:
              _function = "ReleaseInactiveSessions";
              break;
            case 3:
              _function = "AccountsFlagsExpiration";
              break;
            case 4:
              _function = "ProcessPeriodicPromotions";
              break;
            case 5:
              _function = "AccountsPromotionExpiration";
              break;
            case 6:
              _function = "ActivateAwardedPromotion";
              break;
            case 7:
              _function = "GenerateSegmentationReport";
              break;
            case 8:
              //_function = "AccountsPointsExpiration";
              _function = "BucketsExpiration";
              break;
            case 9:
              _function = "AccountsRedeemableCreditsExpiration";
              break;
            case 10:
              _function = "GiftsExpiration";
              break;
            case 11:
              _function = "AccountsLevels";
              break;
            case 12:
              _function = "Create_AML_Daily";
              break;
            case 13:
              _function = "UploadDraws";
              break;
            case 14:
              _function = "EnableDisableGifts";
              break;
            case 15:
              _function = "TitoTicketExpiration";
              break;
            case 16:
              _function = "TitoOfflineTicketDiscard";
              break;
            case 17:
              _function = "HandpayExpiration";
              break;
            case 18:
              _function = "ClearAccountPointsCache";
              break;
            case 19:
              _function = "WCP_AccountsWinLossStatements";
              break;
            case 20:
              _function = "BucketsEmpty";
              break;
            case 21:
              _function = "BucketsCustomerIdEmpty";
              break;
            case 22:
              _function = "ExpireApprovedCreditLines";
              break;
            case 23:
              _function = "ProcessCommissionsPending";
              break;
            default:
              _function = "NONE";
              break;
          }

          Log.Warning("WCP_AccountCreditsExpiration.PeriodicJobThread. Exception in function: " + _function);
          Log.Exception(_ex);
        }
        finally
        {
          if (_sql_conn != null)
          {
            _sql_conn.Close();
            _sql_conn = null;
          }
        }

      } // while

    } // PeriodicJobThread

    //------------------------------------------------------------------------------
    // PURPOSE : Release inactive play sessions:
    //           - Mark as Abandoned (if not)
    //           - Unlink play session from account
    //           - Lock account
    //
    //  PARAMS :
    //      - INPUT :
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private static Boolean ReleaseInactiveSessions()
    {
      StringBuilder _sb;
      Int32 _inactivity_hours;
      DataTable _play_sessions_to_release;
      Int64 _account_id;
      Int64 _play_session_id;
      Int32 _terminal_id;
      TerminalTypes _terminal_type;
      String _terminal_name;
      StringBuilder _error_msg;
      Int32 _system_user_id;
      String _system_user_name;
      Decimal _ps_final_balance;

      try
      {
        if (!Int32.TryParse(GeneralParam.Value("Account", "AutoCloseSessionHours"), out _inactivity_hours))
        {
          _inactivity_hours = 0;
        }
        if (_inactivity_hours == 0)
        {
          return true;
        }

        _sb = new StringBuilder();

        _sb.AppendLine("SELECT   PS_PLAY_SESSION_ID                                                             ");
        _sb.AppendLine("       , ISNULL(PS_FINAL_BALANCE, 0)                                                    ");
        _sb.AppendLine("       , PS_ACCOUNT_ID                                                                  ");
        _sb.AppendLine("       , PS_TERMINAL_ID                                                                 ");
        _sb.AppendLine("       , TE_TERMINAL_TYPE                                                               ");
        _sb.AppendLine("       , TE_NAME                                                                        ");
        _sb.AppendLine("  FROM   PLAY_SESSIONS                                                                  ");
        _sb.AppendLine(" INNER   JOIN TERMINALS                                                                 ");
        _sb.AppendLine("    ON   TE_TERMINAL_ID = PS_TERMINAL_ID                                                ");
        _sb.AppendLine(" WHERE   PS_PLAY_SESSION_ID IN                                                          ");
        _sb.AppendLine("         (                                                                              ");
        _sb.AppendLine("            SELECT   AC_CURRENT_PLAY_SESSION_ID                                         ");
        _sb.AppendLine("              FROM   ACCOUNTS WITH (INDEX (IX_AC_CURRENT_PLAY_SESSION_ID))              ");
        _sb.AppendLine("             WHERE   AC_CURRENT_PLAY_SESSION_ID IS NOT NULL                             ");
        _sb.AppendLine("               AND   AC_ACCOUNT_ID = PS_ACCOUNT_ID                                      "); // 21-JUN-2018 FJC    WIGOS-13083 [Ticket #14935] bloqueo de tarjeta de playerclub
        _sb.AppendLine("         )                                                                              ");
        _sb.AppendLine("   AND   ISNULL(PS_FINISHED, PS_STARTED) < DATEADD(HOUR, -@pInactivityHours, GETDATE()) ");
        _sb.AppendLine(" UNION                                                                                  ");
        _sb.AppendLine("SELECT   PS_PLAY_SESSION_ID                                                             ");
        _sb.AppendLine("       , ISNULL(PS_FINAL_BALANCE, 0)                                                    ");
        _sb.AppendLine("       , PS_ACCOUNT_ID                                                                  ");
        _sb.AppendLine("       , PS_TERMINAL_ID                                                                 ");
        _sb.AppendLine("       , TE_TERMINAL_TYPE                                                               ");
        _sb.AppendLine("       , TE_NAME                                                                        ");
        _sb.AppendLine("  FROM   PLAY_SESSIONS                                                                  ");
        _sb.AppendLine(" INNER   JOIN TERMINALS                                                                 ");
        _sb.AppendLine("    ON   TE_TERMINAL_ID = PS_TERMINAL_ID                                                ");
        _sb.AppendLine(" WHERE   PS_TYPE        = @pPsType                                                      ");
        _sb.AppendLine("   AND   PS_STATUS      = @pPsStatus                                                    ");
        _sb.AppendLine("   AND   ISNULL(PS_FINISHED, PS_STARTED) < DATEADD(HOUR, -@pInactivityHours, GETDATE()) ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString()))
          {
            _cmd.Parameters.Add("@pInactivityHours", SqlDbType.Int).Value = _inactivity_hours;
            _cmd.Parameters.Add("@pPsType", SqlDbType.Int).Value = PlaySessionType.TERMINAL_DRAW;
            _cmd.Parameters.Add("@pPsStatus", SqlDbType.Int).Value = PlaySessionStatus.Opened;

            using (SqlDataAdapter _da = new SqlDataAdapter(_cmd))
            {
              _play_sessions_to_release = new DataTable();
              _db_trx.Fill(_da, _play_sessions_to_release);
            }
          }

          if (!Cashier.GetSystemUser(GU_USER_TYPE.SYS_SYSTEM, _db_trx.SqlTransaction, out _system_user_id, out _system_user_name))
          {
            Log.Error("ReleaseInactiveSessions: GetSystemUser SYS_SYSTEM failed.");
            // It doesn't matter if failed here... Continue.
          }
          _system_user_name = _system_user_name + "@" + Environment.MachineName;
        }

        foreach (DataRow _ps_account in _play_sessions_to_release.Rows)
        {
          _play_session_id = (Int64)_ps_account[0];
          _ps_final_balance = (Decimal)_ps_account[1];
          _account_id = (Int64)_ps_account[2];
          _terminal_id = (Int32)_ps_account[3];
          _terminal_type = (TerminalTypes)((Int16)_ps_account[4]);
          _terminal_name = (String)_ps_account[5];

          using (DB_TRX _db_trx = new DB_TRX())
          {
            if (!MultiPromos.Trx_ReleaseInactivePlaySession(_play_session_id, _ps_final_balance, _account_id,
                                                            _terminal_id, _terminal_type, _terminal_name,
                                                            _system_user_name, out _error_msg, _db_trx.SqlTransaction))
            {
              _error_msg.AppendFormat("Trx_ReleaseInactivePlaySession failed. AccountId: {0}, PlaySessionId: {1}, TerminalId: {2}.",
                                      _account_id, _play_session_id, _terminal_id);
              _error_msg.AppendLine();
              Log.Error(_error_msg.ToString());

              continue;
            }

            _db_trx.Commit();
            #if !SQL_BUSINESS_LOGIC
            BucketsUpdate.SetDataAvailable(); /* wakes up the thread*/
            #endif

          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // ReleaseInactiveSessions

    //------------------------------------------------------------------------------
    // PURPOSE : Generate the segmentation report for the previous month
    //
    //  PARAMS :
    //      - INPUT :
    //          - SqlConn
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private static void GenerateSegmentationReport(SqlConnection _sql_conn)
    {
      Boolean _saved;
      DateTime _dt_rpt;
      Int32 _open_sessions;
      DateTime _opening_date;
      DateTime _date_now;

      _date_now = WGDB.Now;
      _opening_date = Misc.TodayOpening();

      _saved = false;
      _open_sessions = 0;

      // DDM 26-APR-2012: Create segmentation report 
      if (m_retry_running_segmentation_rpt <= _date_now)
      {
        // On error, retry after 5 minutes.
        m_retry_running_segmentation_rpt = _date_now.AddMinutes(5);

        _dt_rpt = new DateTime(_opening_date.Year, _opening_date.Month, 1, _opening_date.Hour, 0, 0);
        _dt_rpt = _dt_rpt.AddMonths(-1);

        if (Reports.SegmentationReportSave(_dt_rpt, out _saved, out _open_sessions))
        {
          if (_saved)
          {
            //if report exists, check again at next opening date.
            m_retry_running_segmentation_rpt = _opening_date.AddDays(1);
          }
          else
          {
            if (_open_sessions != 0)
            {
              Log.Message("Segmentation report has not been saved, currently there are " + _open_sessions + " open play sessions in : " + _dt_rpt.Year + "/" + _dt_rpt.ToString("MM"));
              //if _open_sessions != 0 --> open sessions, retry 1 hour later
              m_retry_running_segmentation_rpt = _date_now.AddHours(1);
            }
          }
          if (!Reports.DeleteReportsGeneratedOnTheFuture())
          {
            Log.Error(" Calling Reports.DeleteReportsGeneratedOnTheFuture");
          }

        }
      }
    } // GenerateSegmentationReport

    //------------------------------------------------------------------------------
    // PURPOSE : Credits expiration management
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private static Boolean AccountsRedeemableCreditsExpiration()
    {
      Int32 _credit_expiration_days;
      Int32 _prize_expiration_days;
      Int32 _min_days;
      Int32 _max_days;
      DateTime _now;
      TimeSpan _elapsed;

      DataTable _accounts;
      StringBuilder _sb;

      _now = WGDB.Now;
      _elapsed = _now - m_credits_expiration_last_run;

      if (_elapsed.TotalHours < 1)
      {
        return true;
      }

      // AJQ 27-MAY-2014, Expire credits due to low balance
      if (!AccountsRedeemableCreditsDailyExpiration())
      {
        Log.Error("AccountsRedeemableCreditsDailyExpirationDueToLowBalance failed.");
        // continue ...
      }


      if (!Int32.TryParse(Misc.ReadGeneralParams("Cashier", "CreditsExpireAfterDays"), out _credit_expiration_days))
      {
        _credit_expiration_days = 0;
      }
      if (!Int32.TryParse(Misc.ReadGeneralParams("Cashier", "PrizesExpireAfterDays"), out _prize_expiration_days))
      {
        _prize_expiration_days = _credit_expiration_days;
      }

      _credit_expiration_days = Math.Max(0, Math.Min(_credit_expiration_days, 5 * 366));  // Maximum 5 years
      _prize_expiration_days = Math.Max(0, Math.Min(_prize_expiration_days, 5 * 366));    // Maximum 5 years

      _min_days = Math.Min(_credit_expiration_days, _prize_expiration_days);
      _max_days = Math.Max(_credit_expiration_days, _prize_expiration_days);
      if (_min_days == 0)
      {
        _min_days = _max_days;
      }
      if (_max_days == 0)
      {
        return true;
      }

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          DateTime _last_activity_from;
          DateTime _last_activity_to;

          _last_activity_to = _now.AddDays(-_min_days);
          _last_activity_from = _now.AddDays(-(_max_days + 90));  // Add 90 days as a security margin

          _sb = new StringBuilder();
          _sb.AppendLine(" SELECT   AC_ACCOUNT_ID                                                                                                ");
          _sb.AppendLine("        , AC_RE_BALANCE                                                                                                ");
          _sb.AppendLine("        , AC_PROMO_RE_BALANCE                                                                                          ");
          _sb.AppendLine("        , AC_PROMO_NR_BALANCE                                                                                          ");
          _sb.AppendLine("        , AC_INITIAL_CASH_IN                                                                                           ");
          _sb.AppendLine("        , CASE WHEN DATEDIFF (MINUTE, AC_LAST_ACTIVITY, @pNow) > @pCreditExpiration THEN 1 ELSE 0 END AS DEV_EXPIRED   ");
          _sb.AppendLine("        , CASE WHEN DATEDIFF (MINUTE, AC_LAST_ACTIVITY, @pNow) > @pPrizeExpiration  THEN 1 ELSE 0 END AS PRIZE_EXPIRED ");
          _sb.AppendLine("        , AC_LAST_ACTIVITY                                                                                             ");
          _sb.AppendLine("        , AC_RE_RESERVED                                                                                               ");
          _sb.AppendLine("   FROM   ACCOUNTS WITH (INDEX (IX_ac_last_activity))                                                                  ");
          _sb.AppendLine("  WHERE   AC_LAST_ACTIVITY >= @pLastActivityFrom                                                                       ");
          _sb.AppendLine("    AND   AC_LAST_ACTIVITY <  @pLastActivityTo                                                                         ");
          _sb.AppendLine("    AND   AC_CURRENT_PLAY_SESSION_ID IS NULL                                                                           ");
          _sb.AppendLine("    AND ( AC_RE_BALANCE + AC_PROMO_RE_BALANCE ) > 0                                                                    ");

          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pNow", SqlDbType.DateTime).Value = _now;
            _sql_cmd.Parameters.Add("@pLastActivityFrom", SqlDbType.DateTime).Value = _last_activity_from;
            _sql_cmd.Parameters.Add("@pLastActivityTo", SqlDbType.DateTime).Value = _last_activity_to;
            _sql_cmd.Parameters.Add("@pCreditExpiration", SqlDbType.Int).Value = (_credit_expiration_days == 0 ? Int32.MaxValue : _credit_expiration_days * 60 * 24);
            _sql_cmd.Parameters.Add("@pPrizeExpiration", SqlDbType.Int).Value = (_prize_expiration_days == 0 ? Int32.MaxValue : _prize_expiration_days * 60 * 24);

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
            {
              _accounts = new DataTable("ACCOUNTS");
              _sql_da.Fill(_accounts);
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }

      DevolutionPrizeParts _parts;

      _parts = new DevolutionPrizeParts();

      foreach (DataRow _account in _accounts.Rows)
      {
        try
        {
          using (DB_TRX _db_trx = new DB_TRX())
          {
            if (AccountExpireRedeemableCredits(_parts, _account, _db_trx.SqlTransaction))
            {
              _db_trx.Commit();
            }
          }
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);

          continue;
        }
      } // foreach

      m_credits_expiration_last_run = _now;

      return true;

    } // AccountsRedeemableCreditsExpiration



    //------------------------------------------------------------------------------
    // PURPOSE : Expire redeemable credit when the balance is lower than a fixed amount
    //           or if is anonymous account.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private static Boolean AccountsRedeemableCreditsDailyExpiration()
    {
      Decimal _low_balance;
      DateTime _last_activity_from;
      DateTime _last_activity_to;
      DataTable _accounts;
      StringBuilder _sb;
      Boolean _expire_anonymous_accounts;
      String _low_balance_condition;
      String _anonymous_condition;

      _last_activity_to = Common.Misc.TodayOpening();

      // Executed Today?
      if (m_daily_expire_last_execution >= _last_activity_to)
      {
        return true;
      }

      _low_balance = GeneralParam.GetDecimal("Cashier", "CreditsExpireDailyWhenBalanceLowerThanCents", 0m);
      _expire_anonymous_accounts = GeneralParam.GetBoolean("Cashier", "CreditsExpireDailyOnAnonymousAccounts", false);

      // Enabled?
      if (_low_balance <= 0 && !_expire_anonymous_accounts)
      {
        m_daily_expire_last_execution = _last_activity_to;

        return true;
      }
      _low_balance *= 0.01m; // Cents to units

      // The player came yesterday
      _last_activity_from = _last_activity_to.AddDays(-1);

      try
      {
        _low_balance_condition = " ( ( AC_RE_BALANCE + AC_PROMO_RE_BALANCE ) < @pLowBalance ) ";
        _anonymous_condition = " ( AC_USER_TYPE = @pUserType AND AC_HOLDER_LEVEL = @pAccountHolderLevel ) ";
        using (DB_TRX _db_trx = new DB_TRX())
        {
          _sb = new StringBuilder();
          _sb.AppendLine(" SELECT   AC_ACCOUNT_ID                               ");
          _sb.AppendLine("        , AC_RE_BALANCE                               ");
          _sb.AppendLine("        , AC_PROMO_RE_BALANCE                         ");
          _sb.AppendLine("        , AC_PROMO_NR_BALANCE                         ");
          _sb.AppendLine("        , AC_INITIAL_CASH_IN                          ");
          _sb.AppendLine("        , 1 DEV_EXPIRED                               ");
          _sb.AppendLine("        , 1 PRIZE_EXPIRED                             ");
          _sb.AppendLine("        , AC_LAST_ACTIVITY                            ");
          _sb.AppendLine("        , AC_RE_RESERVED                              ");
          _sb.AppendLine("   FROM   ACCOUNTS WITH (INDEX (IX_ac_last_activity)) ");
          _sb.AppendLine("  WHERE   AC_LAST_ACTIVITY >= @pLastActivityFrom      ");
          _sb.AppendLine("    AND   AC_LAST_ACTIVITY <  @pLastActivityTo        ");
          _sb.AppendLine("    AND   AC_CURRENT_PLAY_SESSION_ID IS NULL          ");
          _sb.AppendLine("    AND ( AC_RE_BALANCE + AC_PROMO_RE_BALANCE ) > 0   ");
          _sb.AppendLine("    AND   AC_TYPE = @pAccountType                     ");

          if (_low_balance > 0 && _expire_anonymous_accounts)
          {
            _sb.AppendLine("    AND   ( " + _low_balance_condition + " OR " + _anonymous_condition + " ) ");
          }
          else if (_low_balance > 0)
          {
            _sb.AppendLine("    AND  " + _low_balance_condition);
          }
          else if (_expire_anonymous_accounts)
          {
            _sb.AppendLine("    AND  " + _anonymous_condition);
          }

          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pLastActivityFrom", SqlDbType.DateTime).Value = _last_activity_from;
            _sql_cmd.Parameters.Add("@pLastActivityTo", SqlDbType.DateTime).Value = _last_activity_to;
            _sql_cmd.Parameters.Add("@pLowBalance", SqlDbType.Money).Value = _low_balance;
            _sql_cmd.Parameters.Add("@pUserType", SqlDbType.Int).Value = ACCOUNT_USER_TYPE.ANONYMOUS;
            _sql_cmd.Parameters.Add("@pAccountType", SqlDbType.Int).Value = AccountType.ACCOUNT_WIN;
            _sql_cmd.Parameters.Add("@pAccountHolderLevel", SqlDbType.Int).Value = 0;

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
            {
              _accounts = new DataTable("ACCOUNTS");
              _sql_da.Fill(_accounts);
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }

      DevolutionPrizeParts _parts;

      _parts = new DevolutionPrizeParts();

      foreach (DataRow _account in _accounts.Rows)
      {
        try
        {
          using (DB_TRX _db_trx = new DB_TRX())
          {
            if (AccountExpireRedeemableCredits(_parts, _account, _db_trx.SqlTransaction))
            {
              _db_trx.Commit();
            }
          }
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);

          continue;
        }
      } // foreach

      m_daily_expire_last_execution = _last_activity_to;

      return true;

    } // AccountsRedeemableCreditsDailyExpiration


    //------------------------------------------------------------------------------
    // PURPOSE : Mark the gift instance as expired
    //
    //  PARAMS :
    //      - INPUT :
    //          - DataRowAccount : Row containing the gift instance id and its gift id 
    //          - SqlTrx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :

    private static Boolean ForceGiftExpiration(DataRow DataRowAccount,
                                                SqlTransaction SqlTrx)
    {
      SqlCommand _sql_cmd_gift;
      SqlCommand _sql_cmd_gift_instance;
      String _sql_query;
      Int64 _gift_id;
      Int64 _gift_instance_id;
      Int32 _gift_type;
      Int32 _num_rows_updated;

      // Steps
      //    - Mark the gift instance as expired
      //    - Decrement the gift's request counter
      //    - There is no related movement for gifts expiration

      _gift_id = (Int64)DataRowAccount["GIN_GIFT_ID"];
      _gift_instance_id = (Int64)DataRowAccount["GIN_GIFT_INSTANCE_ID"];
      _gift_type = (Int32)DataRowAccount["GIN_GIFT_TYPE"];

      //    - Mark the gift instance as expired
      //      But make sure that the conditions are still valid:
      //        - Only objects
      //        - Not delivered
      //        - Gift's date-time is over
      //        - Not processed yet by the gift expiration thread

      _sql_query = "UPDATE   GIFT_INSTANCES"
                   + " SET   GIN_REQUEST_STATUS   = @GiftRequestStatusNew"
                 + " WHERE   GIN_GIFT_INSTANCE_ID = @GiftInstanceId"
                   + " AND   GIN_GIFT_TYPE        = @GiftType"
                   + " AND   GIN_DELIVERED        IS NULL"
                   + " AND   GIN_EXPIRATION       <= GETDATE()"
                   + " AND   GIN_REQUEST_STATUS   = @GiftRequestStatusOld";

      _sql_cmd_gift_instance = new SqlCommand(_sql_query, SqlTrx.Connection, SqlTrx);
      _sql_cmd_gift_instance.Parameters.Add("@GiftInstanceId", SqlDbType.BigInt).Value = _gift_instance_id;
      _sql_cmd_gift_instance.Parameters.Add("@GiftType", SqlDbType.Int).Value = _gift_type;
      _sql_cmd_gift_instance.Parameters.Add("@GiftRequestStatusOld", SqlDbType.Int).Value = GIFT_REQUEST_STATUS.PENDING;
      _sql_cmd_gift_instance.Parameters.Add("@GiftRequestStatusNew", SqlDbType.Int).Value = GIFT_REQUEST_STATUS.EXPIRED;
      _num_rows_updated = _sql_cmd_gift_instance.ExecuteNonQuery();

      if (_num_rows_updated != 1)
      {
        return false;
      }

      //    - Decrement the gift's request counter

      _sql_query = "UPDATE   GIFTS"
                   + " SET   GI_REQUEST_COUNTER = GI_REQUEST_COUNTER - 1"
                 + " WHERE   GI_GIFT_ID         = @GiftId"
                 + " AND   GI_REQUEST_COUNTER > 0";

      _sql_cmd_gift = new SqlCommand(_sql_query, SqlTrx.Connection, SqlTrx);
      _sql_cmd_gift.Parameters.Add("@GiftId", SqlDbType.BigInt).Value = _gift_id;
      // Since the gift may have been removed from the catalog there is no further checking after the update.
      _num_rows_updated = _sql_cmd_gift.ExecuteNonQuery();

      return true;

    } // ForceGiftExpiration

    #region Tito Tickets Expiration

    //------------------------------------------------------------------------------
    // PURPOSE : TITO Ticket expiration management
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    private static Boolean TitoTicketExpiration()
    {
      StringBuilder _sb;
      DataTable _dt_tickets;
      CashierSessionInfo _session;
      Decimal _redeemable;
      Decimal _promo_redeemable;
      Decimal _promo_non_redeemable;

      _redeemable = 0;
      _promo_redeemable = 0;
      _promo_non_redeemable = 0;

      m_tito_ticket_expiration_check = (m_tito_ticket_expiration_check + 1) % TITO_TICKET_EXPIRATION_CHECK;

      if (m_tito_ticket_expiration_check != 0)
      {
        // Not the right time for tito tito expiration checking 
        return true;
      }

      _sb = new StringBuilder();
      _sb.AppendLine(" SELECT   TI_TICKET_ID, TI_AMOUNT, TI_TYPE_ID ");
      _sb.AppendLine("   FROM   TICKETS WITH(INDEX(IX_ti_status_expiration_datetime)) ");
      _sb.AppendLine("  WHERE   TI_EXPIRATION_DATETIME <= GETDATE() ");
      //if (GeneralParam.GetBoolean("TITO","PendingCancel.AllowToPlayOnEGM",false))
      //{
        _sb.AppendLine("    AND   TI_STATUS IN (@pTicketStateValid, @pTicketStatePendingPrint, @pTicketPendingCancel) ");
      //}
      //else
      //{
      //  _sb.AppendLine("    AND   TI_STATUS IN (@pTicketStateValid,  @pTicketStatePendingPrint) ");
      //}     

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pTicketStateValid", SqlDbType.Int).Value = (Int32)TITO_TICKET_STATUS.VALID;
            _cmd.Parameters.Add("@pTicketStatePendingPrint", SqlDbType.Int).Value = (Int32)TITO_TICKET_STATUS.PENDING_PRINT;
            _cmd.Parameters.Add("@pTicketPendingCancel", SqlDbType.Int).Value = (Int32)TITO_TICKET_STATUS.PENDING_CANCEL;            

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_cmd))
            {
              _dt_tickets = new DataTable();
              _sql_da.Fill(_dt_tickets);
            }
          }

          // Are there expired tickets? 
          if (_dt_tickets.Rows.Count == 0)
          {

            return true;
          }

          _session = Cashier.GetSystemCashierSessionInfo(GU_USER_TYPE.SYS_SYSTEM);
          if (_session == null)
          {
            Log.Error("TitoTicketExpiration: There's no session");

            return false;
          }

          // Tickets expiration and accumulates amounts
          TitoTicketExpirationStatus(_dt_tickets, _session, out _redeemable, out _promo_redeemable, out _promo_non_redeemable, _db_trx.SqlTransaction);

          // Create cashier movements 
          TitoTicketExpirationCashierMovements(_session, _redeemable, _promo_redeemable, _promo_non_redeemable, _db_trx.SqlTransaction);

          _db_trx.Commit();
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }

      return true;

    } // TitoTicketExpiration

    //------------------------------------------------------------------------------
    // PURPOSE : TITO Ticket expiration (changes status of the tickets and accumulates amounts)
    //
    //  PARAMS :
    //      - INPUT :
    //        - DtTickets
    //
    //      - OUTPUT :
    //        - Cashable
    //        - Redeemable
    //        - NonRedeemable
    //        - SqlTransaction
    //
    // RETURNS : Boolean
    //
    //   NOTES :
    private static Boolean TitoTicketExpirationStatus(DataTable DtTickets, CashierSessionInfo _session, out Decimal Redeemable, out Decimal PromoRedeemable,
                                                      out Decimal PromoNonRedeemable, SqlTransaction Trx)
    {
      Redeemable = 0;
      PromoRedeemable = 0;
      PromoNonRedeemable = 0;

      // Force the expiration for every expired ticket instance 
      foreach (DataRow _ticket in DtTickets.Rows)
      {
        if (!Ticket.DB_ForceTicketCancellationByExpiration((Int64)_ticket["TI_TICKET_ID"], _session, Trx))
        {
          Log.Error("TitoTicketExpiration: TICKET_ID = " + (Int64)_ticket["TI_TICKET_ID"]);
          continue;
        }
        switch ((TITO_TICKET_TYPE)_ticket["TI_TYPE_ID"])
        {
          case TITO_TICKET_TYPE.CASHABLE:
            Redeemable += (Decimal)_ticket["TI_AMOUNT"];
            // CASHOUT TICKET
            break;
          case TITO_TICKET_TYPE.PROMO_REDEEM:
            PromoRedeemable += (Decimal)_ticket["TI_AMOUNT"];
            // CASHABLE PROMO
            break;
          case TITO_TICKET_TYPE.PROMO_NONREDEEM:
            // PLAYABLE ONLY
            PromoNonRedeemable += (Decimal)_ticket["TI_AMOUNT"];
            break;
          case TITO_TICKET_TYPE.HANDPAY:
          case TITO_TICKET_TYPE.JACKPOT:
          case TITO_TICKET_TYPE.CHIPS_DEALER_COPY:
          default:
            break;
        }
      }

      return true;

    } // TitoTicketExpirationStatus

    //------------------------------------------------------------------------------
    // PURPOSE : TITO Ticket expiration movements (generates the cashier movements of the expired tickets)
    //
    //  PARAMS :
    //      - INPUT :
    //        - CashierSessionInfo
    //        - Redeemable
    //        - PromoRedeemable
    //        - NonRedeemable
    //        - SqlTransaction
    //
    //      - OUTPUT :
    //
    // RETURNS : Boolean
    //
    //   NOTES :
    private static Boolean TitoTicketExpirationCashierMovements(CashierSessionInfo Session, Decimal Redeemable,
                                                                Decimal PromoRedeemable, Decimal PromoNonRedeemable,
                                                                SqlTransaction Trx)
    {
      CashierMovementsTable _cashier_movements;

      _cashier_movements = new CashierMovementsTable(Session);
      if (PromoRedeemable != 0)
      {
        _cashier_movements.Add(0, CASHIER_MOVEMENT.TITO_TICKET_EXPIRED_PROMO_REDEEMABLE, PromoRedeemable, 0, "");
      }
      if (PromoNonRedeemable != 0)
      {
        _cashier_movements.Add(0, CASHIER_MOVEMENT.TITO_TICKET_EXPIRED_PROMO_NOT_REDEEMABLE, PromoNonRedeemable, 0, "");
      }
      if (Redeemable != 0)
      {
        _cashier_movements.Add(0, CASHIER_MOVEMENT.TITO_TICKET_EXPIRED_REDEEMABLE, Redeemable, 0, "");
      }

      if (!_cashier_movements.Save(Trx))
      {
        Log.Error("TitoTicketExpiration: Cashier movement");
        return false;
      }

      return true;
    } // TitoTicketExpirationCashierMovements

    #endregion

    #region Tito Offline Tickets Discard

    //------------------------------------------------------------------------------
    // PURPOSE : TITO Offline Ticket discard management
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    private static Boolean TitoOfflineTicketDiscard()
    {
      StringBuilder _sb;
      DataTable _dt_tickets;
      CashierSessionInfo _session;
      Decimal _cashable;
      Decimal _playable;
      Int64 _out_operation_id;

      _cashable = 0;
      _playable = 0;
      _out_operation_id = 0;

      m_tito_offline_ticket_discard_check = (m_tito_offline_ticket_discard_check + 1) % TITO_OFFLINE_TICKET_DISCARD_CHECK;

      if (m_tito_offline_ticket_discard_check != 0)
      {
        // Not the right time for tito offline ticket discard checking
        return true;
      }

      _sb = new StringBuilder();
      _sb.AppendLine(" SELECT   TICKETS.TI_TICKET_ID AS TICKET_ID ");
      _sb.AppendLine("        , TICKETS.TI_AMOUNT AS AMOUNT ");
      _sb.AppendLine("        , TI_TYPE_ID ");
      _sb.AppendLine("        , OFFLINE_TICKETS.TI_AMOUNT AS OFFLINE_AMOUNT ");
      _sb.AppendLine("        , OFFLINE_TICKETS.TI_TICKET_ID AS OFFLINE_ID ");
      _sb.AppendLine("   FROM   TICKETS, ");
      _sb.AppendLine("        ( SELECT TI_TICKET_ID, TI_VALIDATION_NUMBER, TI_CREATED_TERMINAL_ID, TI_CREATED_DATETIME, ");
      _sb.AppendLine("                 TI_AMOUNT FROM TICKETS ");
      _sb.AppendLine("           WHERE TI_TYPE_ID = @pTypeOffline ");
      _sb.AppendLine("             AND DATEDIFF(DAY, TI_LAST_ACTION_DATETIME, GETDATE()) <= @pDays ");
      _sb.AppendLine("		    ) OFFLINE_TICKETS ");
      _sb.AppendLine("  WHERE   OFFLINE_TICKETS.TI_VALIDATION_NUMBER = TICKETS.TI_VALIDATION_NUMBER ");
      _sb.AppendLine("    AND   OFFLINE_TICKETS.TI_CREATED_TERMINAL_ID = TICKETS.TI_CREATED_TERMINAL_ID ");
      _sb.AppendLine("    AND   TICKETS.TI_DB_INSERTED >= DATEADD(ms, -2 * (@pElapsedMiliseconds), GETDATE())");
      _sb.AppendLine("    AND   TICKETS.TI_TYPE_ID <> @pTypeOffline ");
      _sb.AppendLine("    AND   (TICKETS.TI_STATUS = @pTicketStatusValid ");
      _sb.AppendLine("    OR    TICKETS.TI_STATUS = @pTicketStatusPendingPrint) ");
      _sb.AppendLine("    AND   TICKETS.TI_CREATED_TERMINAL_TYPE = @pTerminalType ");

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pTicketStatusValid", SqlDbType.Int).Value = (Int32)TITO_TICKET_STATUS.VALID;
            _cmd.Parameters.Add("@pTicketStatusPendingPrint", SqlDbType.Int).Value = (Int32)TITO_TICKET_STATUS.PENDING_PRINT;
            _cmd.Parameters.Add("@pTypeOffline", SqlDbType.Int).Value = (Int32)TITO_TICKET_TYPE.OFFLINE;
            _cmd.Parameters.Add("@pDays", SqlDbType.Int).Value = TITO_OFFLINE_TICKETS_DISCARD_DAYS;
            _cmd.Parameters.Add("@pElapsedMiliseconds", SqlDbType.Int).Value = TITO_OFFLINE_TICKET_DISCARD_CHECK * WAIT_FOR_NEXT_TRY;
            _cmd.Parameters.Add("@pTerminalType", SqlDbType.Int).Value = TITO_TERMINAL_TYPE.TERMINAL;

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_cmd))
            {
              _dt_tickets = new DataTable();
              _sql_da.Fill(_dt_tickets);
            }
          }

          // Are there tickets that must be discarded? 
          if (_dt_tickets.Rows.Count == 0)
          {
            return true;
          }

          _session = Cashier.GetSystemCashierSessionInfo(GU_USER_TYPE.SYS_SYSTEM);
          if (_session == null)
          {
            Log.Error("TitoOfflineTicketDiscard: There's no session");

            return false;
          }

          // Tickets discard and accumulates amounts
          TitoOfflineTicketProcessDiscard(_dt_tickets, _session, ref _out_operation_id, out _cashable, out _playable, _db_trx.SqlTransaction);

          // Create cashier movements
          TitoTicketDiscardCashierMovements(_session, _cashable, _playable, _out_operation_id, _db_trx.SqlTransaction);

          _db_trx.Commit();

        } // end using DB_TRX _db_trx
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }

      return true;

    } // TitoOfflineTicketDiscard

    //------------------------------------------------------------------------------
    // PURPOSE : TITO Offline Ticket discard (changes status of the tickets and accumulates amounts)
    //
    //  PARAMS :
    //      - INPUT :
    //        - DtTickets
    //        - CashierSessionInfo
    //
    //      - OUTPUT :
    //        - OutOperationId
    //        - Cashable
    //        - Playable
    //
    // RETURNS : Boolean
    //
    //   NOTES :
    private static Boolean TitoOfflineTicketProcessDiscard(DataTable DtTickets, CashierSessionInfo Session,
                                                          ref Int64 OutOperationId, out Decimal Cashable,
                                                          out Decimal Playable, SqlTransaction Trx)
    {
      Decimal _ticket_amount;
      Decimal _offline_amount;
      String _alarm_description;
      TITO_TICKET_TYPE _ticket_type;
      Int64 _offline_ticket_id;
      Int64 _online_ticket_id;

      _ticket_amount = 0;
      _offline_amount = 0;
      Cashable = 0;
      Playable = 0;

      // Force to discard tickets for every online ticket that has offline ticket associated
      foreach (DataRow _row in DtTickets.Rows)
      {
        _ticket_amount = (Decimal)_row["AMOUNT"];
        _offline_amount = (Decimal)_row["OFFLINE_AMOUNT"];
        _ticket_type = (TITO_TICKET_TYPE)_row["TI_TYPE_ID"];
        _online_ticket_id = (Int64)_row["TICKET_ID"];
        _offline_ticket_id = (Int64)_row["OFFLINE_ID"];

        if (_ticket_amount != _offline_amount)
        {
          // Generate alarm
          _alarm_description = Resource.String("STR_ALARM_TITO_OFFLINE_TICKET_DISCARD_INVALID_AMOUNT");

          Log.Error(_alarm_description);
          Alarm.Register(AlarmSourceCode.Service,
                         0,
                         Environment.MachineName + @"\\WCP_Service",
                         (UInt32)AlarmCode.Service_TITO_OfflineTicketDiscardError,
                         _alarm_description,
                         AlarmSeverity.Warning);
        }

        switch (_ticket_type)
        {
          case TITO_TICKET_TYPE.HANDPAY:
          case TITO_TICKET_TYPE.JACKPOT:
          case TITO_TICKET_TYPE.CASHABLE:
            Cashable += _ticket_amount;
            // CASHOUT TICKET
            break;
          case TITO_TICKET_TYPE.PROMO_NONREDEEM:
            // PLAYABLE ONLY
            Playable += _ticket_amount;
            break;

          default:
            // Generate alarm
            _alarm_description = Resource.String("STR_ALARM_TITO_OFFLINE_TICKET_DISCARD_INVALID_TYPE");

            Log.Error(_alarm_description);
            Alarm.Register(AlarmSourceCode.Service,
                           0,
                           Environment.MachineName + @"\\WCP_Service",
                           (UInt32)AlarmCode.Service_TITO_OfflineTicketDiscardError,
                           _alarm_description,
                           AlarmSeverity.Warning);
            continue;
        }

        // Checks if it's a handpay and process it before discard the ticket
        if (_ticket_type == TITO_TICKET_TYPE.HANDPAY || _ticket_type == TITO_TICKET_TYPE.JACKPOT)
        {
          if (!TitoOfflineHandpayDiscard(_offline_ticket_id, _online_ticket_id, Session, ref OutOperationId, Trx))
          {
            continue;
          }
        }

        if (!Ticket.DB_ForceOfflineTicketDiscard(_online_ticket_id, Trx))
        {
          Log.Error("TitoOfflineTicketDiscardStatus: Can't discard TICKET_ID = " + _online_ticket_id);
          continue;
        }
      }

      return true;

    } // TitoOfflineTicketDiscardStatus

    //------------------------------------------------------------------------------
    // PURPOSE : TITO Ticket discard movements (generates the cashier movements of the discarded tickets)
    //
    //  PARAMS :
    //      - INPUT :
    //        - CashierSessionInfo
    //        - Cashable
    //        - Playable
    //        - OperationId
    //        - SqlTransaction
    //
    //      - OUTPUT :
    //
    // RETURNS : 
    //            - True: if it was able to generate cashier movements correctly
    //            - False: otherwise
    //    
    private static Boolean TitoTicketDiscardCashierMovements(CashierSessionInfo Session, Decimal Cashable, Decimal Playable, Int64 OperationId, SqlTransaction Trx)
    {
      CashierMovementsTable _cashier_movements;

      _cashier_movements = new CashierMovementsTable(Session);
      if (Playable != 0)
      {
        _cashier_movements.Add(OperationId, CASHIER_MOVEMENT.TITO_TICKET_PLAYABLE_REDEEMED_OFFLINE, Playable, 0, "");
      }
      if (Cashable != 0)
      {
        _cashier_movements.Add(OperationId, CASHIER_MOVEMENT.TITO_TICKET_CASHABLE_REDEEMED_OFFLINE, Cashable, 0, "");
      }

      if (!_cashier_movements.Save(Trx))
      {
        Log.Error("TitoTicketDiscardCashierMovements: Error creating cashier movement");
        return false;
      }

      return true;
    } // TitoTicketDiscardCashierMovements

    private static Boolean TitoOfflineHandpayDiscard(Int64 OfflineTicketId, Int64 OnlineTicketId,
                                                     CashierSessionInfo Session, ref Int64 OutOperationId, SqlTransaction Trx)
    {
      Int64 _play_session_id;
      Handpays.HandpayRegisterOrCancelParameters _handpay_param;
      MultiPromos.AccountBalance _ini_balance;
      MultiPromos.AccountBalance _fin_balance;
      HandPay.TICKET_HANDPAY_STATUS _handpay_status;
      Ticket _offline_ticket;
      Int16 _handpay_error_msg;

      // Change Handpay ticket id
      if (!HandPay.DB_ReplaceHandPayTicketId(OnlineTicketId, OfflineTicketId, Trx))
      {
        Log.Error("TitoOfflineHandpayDiscard: Can't change handpay ticket id linked to TICKET_ID = " + OnlineTicketId);
        return false;
      }

      // Change ticket created play session id
      if (!HandPay.DB_ReplaceTicketPlaySessionId(OnlineTicketId, OfflineTicketId, Trx))
      {
        Log.Error("TitoOfflineHandpayDiscard: Can't change handpay ticket id linked to TICKET_ID = " + OnlineTicketId);
        return false;
      }

      // Load ticket information
      _offline_ticket = Ticket.LoadTicket(OfflineTicketId, Trx, false);

      // Check if ticket has a valid Handpay
      _handpay_param = new Handpays.HandpayRegisterOrCancelParameters();
      _handpay_status = HandPay.GetTicketHandpayStatus(_offline_ticket, out _handpay_param, Trx);

      if (_handpay_status != HandPay.TICKET_HANDPAY_STATUS.VALID && _handpay_status != HandPay.TICKET_HANDPAY_STATUS.INCORRECT_AMOUNTS)
      {
        Log.Message("TitoOfflineHandpayDiscard: GetTicketHandpayStatus: Ticket [" + OnlineTicketId + "] hasn't a valid handpay.");
        return false;
      }

      _handpay_param.CardData = new CardData();

      // Load card data information
      if (!CardData.DB_CardGetAllData(_offline_ticket.LastActionAccountID, _handpay_param.CardData, Trx))
      {
        Log.Message("TitoOfflineHandpayDiscard: DB_CardGetAllData: Ticket [" + OnlineTicketId + "] unable to get card data information.");
        return false;
      }

      // Register the handpay
      if (!Handpays.RegisterOrCancel(_handpay_param, Session, false, ref OutOperationId, out _play_session_id, out _handpay_error_msg,  Trx))
      {
        Log.Message("TitoOfflineHandpayDiscard: RegisterOrCancel: Ticket [" + OnlineTicketId + "] unable to register handpay.");
        return false;
      }

      // Update offline ticket play session
      if (!HandPay.DB_SetTicketPlaySessionId(OfflineTicketId, _play_session_id, Trx))
      {
        Log.Message("TitoOfflineHandpayDiscard: DB_SetTicketPlaySessionId: Ticket [" + OfflineTicketId + "] unable to update play session.");
        return false;
      }

      // Update online ticket play session
      if (!HandPay.DB_SetTicketPlaySessionId(OnlineTicketId, _play_session_id, Trx))
      {
        Log.Message("TitoOfflineHandpayDiscard: DB_SetTicketPlaySessionId: Ticket [" + OnlineTicketId + "] unable to update play session.");
        return false;
      }

      MultiPromos.AccountBalance _amount = new MultiPromos.AccountBalance(_offline_ticket.Amount, 0, 0);

      // Create the cash_out operation
      if (!Utils.AccountBalanceOut(_handpay_param.CardData.AccountId, _amount, 0, out _ini_balance, out _fin_balance, Trx))
      {
        Log.Message("TitoOfflineHandpayDiscard: AccountBalanceIn: Ticket [" + OfflineTicketId + "] unable to update account balance.");
        return false;
      }

      return true;
    }

    #endregion

    //------------------------------------------------------------------------------
    // PURPOSE : Gifts expiration management
    //
    //  PARAMS :
    //      - INPUT :
    //          - SqlConn
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    private static Boolean GiftsExpiration(SqlConnection SqlConn)
    {
      SqlTransaction _sql_trx;
      String _sql_query;
      SqlCommand _sql_cmd;
      SqlDataAdapter _sql_da;
      DataSet _sql_ds;

      m_gift_expiration_check = (m_gift_expiration_check + 1) % GIFT_EXPIRATION_CHECK;

      if (m_gift_expiration_check != 0)
      {
        // Not the right time for gift expiration checking 
        return true;
      }

      // Steps 
      //    - Get all expired gift instances pending to be marked
      //    - Force the expiration for every expired gift instance 

      //    - Get all expired gift instances pending to be marked
      //        - Only objects
      //        - Not delivered
      //        - Gift's date-time is over
      //        - Not processed yet by the gift expiration thread

      _sql_ds = new DataSet();

      _sql_query = "SELECT   GIN_GIFT_INSTANCE_ID"
                      + "  , GIN_GIFT_ID"
                      + "  , GIN_GIFT_TYPE"
                  + " FROM   GIFT_INSTANCES"
                 + " WHERE   GIN_GIFT_TYPE     IN ( @GiftTypeObject, @GiftTypeServices)"
                   + " AND   GIN_DELIVERED     IS NULL"
                   + " AND   GIN_EXPIRATION    <= GETDATE()"
                   + " AND   GIN_REQUEST_STATUS = @GiftRequestStatus"
                           ;

      _sql_cmd = new SqlCommand(_sql_query, SqlConn);
      _sql_cmd.Parameters.Add("@GiftTypeObject", SqlDbType.Int).Value = GIFT_TYPE.OBJECT;
      _sql_cmd.Parameters.Add("@GiftTypeServices", SqlDbType.Int).Value = GIFT_TYPE.SERVICES;
      _sql_cmd.Parameters.Add("@GiftRequestStatus", SqlDbType.Int).Value = GIFT_REQUEST_STATUS.PENDING;

      _sql_da = new SqlDataAdapter(_sql_cmd);
      _sql_da.Fill(_sql_ds);

      //    - Force the expiration for every expired gift instance 
      foreach (DataRow _ds_row in _sql_ds.Tables[0].Rows)
      {
        _sql_trx = null;

        try
        {
          _sql_trx = SqlConn.BeginTransaction();

          if (ForceGiftExpiration(_ds_row, _sql_trx))
          {
            _sql_trx.Commit();
          }
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);

          return false;
        }
        finally
        {
          if (_sql_trx != null)
          {
            if (_sql_trx.Connection != null)
            {
              _sql_trx.Rollback();
            }

            _sql_trx = null;
          }
        }
      }

      return true;

    } // GiftsExpiration

    //------------------------------------------------------------------------------
    // PURPOSE : Credits expiration for one account
    //
    //  PARAMS :
    //      - INPUT :
    //          - DataRowAccount
    //          - SqlTrx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :

    private static Boolean AccountExpireRedeemableCredits(DevolutionPrizeParts Parts, DataRow DataRowAccount, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      Int64 _account_id;
      Currency _reserved_ini;
      Currency _reserved_fin;
      Int64 _operation_id;
      Boolean _dev_expired;
      Decimal _dummy_points;
      Currency _ini_cash_in;
      Boolean _prize_expired;
      //Currency _sum_cash_in;
      DateTime _last_activity;
      Currency _dev_from_cash_in;
      Currency _total_re_expired;
      Decimal _ini_total_balance;
      CashierSessionInfo _session;
      Currency _expired_dev_amount;
      Int64 _dummy_play_session_id;
      Currency _expired_prize_amount;
      MultiPromos.AccountBalance _ini_bal;
      MultiPromos.AccountBalance _ex_bal;
      MultiPromos.AccountBalance _fin_bal;
      AccountMovementsTable _account_movements;
      CashierMovementsTable _cashier_movements;
      MultiPromos.AccountBalance _dummy_cancel;

      _account_id = (Int64)DataRowAccount["AC_ACCOUNT_ID"];

      _ini_bal = new MultiPromos.AccountBalance((Decimal)DataRowAccount["AC_RE_BALANCE"],
                                                (Decimal)DataRowAccount["AC_PROMO_RE_BALANCE"],
                                                (Decimal)DataRowAccount["AC_PROMO_NR_BALANCE"]);


      _ini_cash_in = (Decimal)DataRowAccount["AC_INITIAL_CASH_IN"];
      _last_activity = (DateTime)DataRowAccount["AC_LAST_ACTIVITY"];
      _dev_expired = ((Int32)DataRowAccount["DEV_EXPIRED"] == 1);
      _prize_expired = ((Int32)DataRowAccount["PRIZE_EXPIRED"] == 1);
      _reserved_ini = (Decimal)DataRowAccount["AC_RE_RESERVED"];

      if (!Accounts.GetMaxDevolution(_account_id, _ini_cash_in, out _dev_from_cash_in, SqlTrx))
      {
        return false;
      }

      Parts.Compute(_ini_bal.TotalRedeemable, _dev_from_cash_in, true, true);

      _expired_dev_amount = _dev_expired ? Parts.Devolution : 0;
      _expired_prize_amount = _prize_expired ? Parts.Prize : 0;

      _expired_dev_amount = Math.Max(0, _expired_dev_amount);
      _expired_prize_amount = Math.Max(0, _expired_prize_amount);
      if (_expired_dev_amount == 0 && _expired_prize_amount == 0)
      {
        return true;
      }

      // AJQ 27-MAY-2014, Any Redeemable Credit expires as "Prize" to be later reported to the PSA as "PremioNoReclamado"
      if (GeneralParam.GetBoolean("Cashier", "AnyRedeemableExpireAsPrize", false))
      {
        _expired_prize_amount = _expired_prize_amount + _expired_dev_amount;
        _expired_dev_amount = 0;
        _dev_expired = true;
        _prize_expired = true;
      }

      _total_re_expired = _expired_dev_amount + _expired_prize_amount;

      _ex_bal = MultiPromos.AccountBalance.Zero;
      _ex_bal.PromoRedeemable = Math.Min(_ini_bal.PromoRedeemable, _total_re_expired);
      _ex_bal.Redeemable = Math.Min(_ini_bal.Redeemable, _total_re_expired - _ex_bal.PromoRedeemable);

      if (_ex_bal.TotalRedeemable != _total_re_expired)
      {
        return false;
      }

      _ex_bal = MultiPromos.AccountBalance.Negate(_ex_bal);
      if (!MultiPromos.Trx_UpdateAccountBalance(_account_id, _ex_bal, 0, out _fin_bal, out _dummy_points, out _dummy_play_session_id, out _dummy_cancel, SqlTrx))
      {
        return false;
      }
      if (_fin_bal.TotalBalance + _total_re_expired != _ini_bal.TotalBalance)
      {
        return false;
      }
      _ini_total_balance = _ini_bal.TotalBalance;

      _reserved_fin = (_reserved_ini + _ex_bal.Redeemable <= 0 ? 0 : _reserved_ini + _ex_bal.Redeemable);


      // Ensure the data has not changed from the initial read & update the initial cash in when needed

      _sb = new StringBuilder();
      _sb.AppendLine(" UPDATE   ACCOUNTS                                                                                   ");
      _sb.AppendLine("    SET   AC_INITIAL_CASH_IN  = CASE WHEN @pDevolutionExpired = 1 THEN 0 ELSE AC_INITIAL_CASH_IN END ");
      _sb.AppendLine("        , AC_RE_RESERVED      = @pReserved                                                           ");
      _sb.AppendLine("  WHERE   AC_ACCOUNT_ID       = @pAccountId                                                          ");
      _sb.AppendLine("    AND   AC_RE_BALANCE       = @pReBalance                                                          ");
      _sb.AppendLine("    AND   AC_PROMO_RE_BALANCE = @pPromoReBalance                                                     ");
      _sb.AppendLine("    AND   AC_PROMO_NR_BALANCE = @pPromoNrBalance                                                     ");
      _sb.AppendLine("    AND   AC_INITIAL_CASH_IN  = @pInitialCashIn                                                      ");
      _sb.AppendLine("    AND   AC_LAST_ACTIVITY    = @pLastActivity                                                       ");
      _sb.AppendLine("    AND   AC_CURRENT_PLAY_SESSION_ID IS NULL                                                         ");

      using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
      {
        _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = _account_id;
        // RCI 23-OCT-2012: Error expiring redeemable credits:
        //                  - Use _fin_bal instead of _ini_bal. The routine Trx_UpdateAccountBalance() has already changed values.
        _sql_cmd.Parameters.Add("@pReBalance", SqlDbType.Money).Value = _fin_bal.Redeemable;
        _sql_cmd.Parameters.Add("@pPromoReBalance", SqlDbType.Money).Value = _fin_bal.PromoRedeemable;
        _sql_cmd.Parameters.Add("@pPromoNrBalance", SqlDbType.Money).Value = _fin_bal.PromoNotRedeemable;
        _sql_cmd.Parameters.Add("@pInitialCashIn", SqlDbType.Money).Value = _ini_cash_in.SqlMoney;
        _sql_cmd.Parameters.Add("@pDevolutionExpired", SqlDbType.Bit).Value = _dev_expired;
        _sql_cmd.Parameters.Add("@pLastActivity", SqlDbType.DateTime).Value = _last_activity;
        _sql_cmd.Parameters.Add("@pReserved", SqlDbType.Decimal).Value = (Decimal)_reserved_fin;

        if (_sql_cmd.ExecuteNonQuery() != 1)
        {
          return false;
        }
      }

      _session = Cashier.GetSystemCashierSessionInfo(GU_USER_TYPE.SYS_SYSTEM);
      if (_session == null)
      {
        return false;
      }
      _cashier_movements = new CashierMovementsTable(_session);
      _account_movements = new AccountMovementsTable(_session);
      _operation_id = 0;

      //03-JAN-2014 JPJ Insert a Redeemable credit expiration operation.
      if ((_expired_dev_amount > 0) || (_expired_prize_amount > 0))
      {
        // Insert a Redeemable credit expiration operation.
        if (!Operations.DB_InsertOperation(OperationCode.REDEEMABLE_CREDITS_EXPIRED, _account_id, _session.CashierSessionId, 0,
                                           0, _expired_prize_amount + _expired_dev_amount, 0, 0, 0, string.Empty, out _operation_id, SqlTrx))
        {
          return false;
        }
      }

      if (_expired_dev_amount > 0)
      {
        _account_movements.Add(_operation_id, _account_id, MovementType.CreditsExpired, _ini_total_balance, _expired_dev_amount, 0, _ini_total_balance - _expired_dev_amount);
        _ini_total_balance = _ini_total_balance - _expired_dev_amount;

        // TODO: On utilitzar aquests nous moviments per tenir-los en compte en la Caixa??
        _cashier_movements.Add(_operation_id, CASHIER_MOVEMENT.REDEEMABLE_DEV_EXPIRED, _expired_dev_amount, _account_id, "");
      }

      if (_expired_prize_amount > 0)
      {
        _account_movements.Add(_operation_id, _account_id, MovementType.PrizeExpired, _ini_total_balance, _expired_prize_amount, 0, _ini_total_balance - _expired_prize_amount);
        _ini_total_balance = _ini_total_balance - _expired_prize_amount;

        // TODO: On utilitzar aquests nous moviments per tenir-los en compte en la Caixa??
        _cashier_movements.Add(_operation_id, CASHIER_MOVEMENT.REDEEMABLE_PRIZE_EXPIRED, _expired_prize_amount, _account_id, "");
      }

      if (_reserved_ini != _reserved_fin)
      {
         _account_movements.Add(_operation_id, _account_id, MovementType.ReservedExpired, _reserved_ini, _reserved_ini - _reserved_fin, 0, _reserved_fin);
      }

      // At this point the _ini_re_balance must be _fin_re_balance
      if (_ini_total_balance != _fin_bal.TotalBalance)
      {
        return false;
      }

      if (!_account_movements.Save(SqlTrx))
      {
        return false;
      }

      if (!_cashier_movements.Save(SqlTrx))
      {
        return false;
      }

      return true;

    } // AccountExpireRedeemableCredits

    //------------------------------------------------------------------------------
    // PURPOSE : Update the handpays with expired status 
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :  Result Succesfully
    //
    //   NOTES :
    private static Boolean HandpayExpiration()
    {
      StringBuilder _sb;
      Int32 _time_period;
      Int32 _cancel_time_period;
      Int32 _rows_affected;
      DateTime _last_activity_to;

      _last_activity_to = Common.Misc.TodayOpening();

      if (m_daily_expire_last_handpay_execution >= _last_activity_to)
      {
        return true;
      }

      Handpays.GetParameters(out _time_period, out _cancel_time_period);

      _sb = new StringBuilder();

      _sb.Length = 0;
      _sb.AppendLine("UPDATE   HANDPAYS ");
      _sb.AppendLine("   SET   HP_STATUS = (HP_STATUS & ~@pMaskStatus) + @pStatusExpired ");
      _sb.AppendLine("       , HP_STATUS_CHANGED = GETDATE() ");
      _sb.AppendLine("  FROM   HANDPAYS WITH(INDEX(IX_hp_status_calculated))");
      _sb.AppendLine(" WHERE   HP_DATETIME < @pCreatedDate ");
      _sb.AppendLine("   AND   HP_STATUS_CALCULATED NOT IN (@pStatusPaid, @pStatusExpired, @pStatusVoided) ");
      _sb.AppendLine("   AND   HP_TICKET_ID IS NULL");

      try
      {
        using (DB_TRX _trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _trx.SqlTransaction.Connection, _trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pMaskStatus", SqlDbType.Int).Value = HANDPAY_STATUS.MASK_STATUS;
            _cmd.Parameters.Add("@pStatusExpired", SqlDbType.Int).Value = HANDPAY_STATUS.EXPIRED;
            _cmd.Parameters.Add("@pCreatedDate", SqlDbType.DateTime).Value = WGDB.Now.AddMinutes(-_time_period);
            _cmd.Parameters.Add("@pStatusPaid", SqlDbType.Int).Value = HANDPAY_STATUS.PAID;
            _cmd.Parameters.Add("@pStatusVoided", SqlDbType.Int).Value = HANDPAY_STATUS.VOIDED;

            _rows_affected = _cmd.ExecuteNonQuery();

            if (_rows_affected > 0)
            {
              Log.Message("Number of Handpays expired: " + _rows_affected.ToString());
            }

            _trx.Commit();

            m_daily_expire_last_handpay_execution = _last_activity_to;

            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // HandpayExpiration


    #endregion Private Functions

    //------------------------------------------------------------------------------
    // PURPOSE : Automatic closing of the system cashier sessions preceding the current day 
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :  Result Succesfully
    //
    //   NOTES :
    public static void ClosePreviousDaySystemCashierSession()
    {
      DateTime _today_opening;
      Int32 _user_id;
      String _user_name;
      Int64 _session_id;
      DateTime _session_opening;
      Int32 _terminal_id;
      String _terminal_name;
      StringBuilder _sb;
      CashierSessionInfo _session_info;
      CashierMovementsTable _cashier_movement;

      _sb = new StringBuilder();
      _today_opening = Misc.TodayOpening();
      _session_id = 0;
      _session_opening = _today_opening;

      try
      {
        if (m_last_closed_system_session == _today_opening)
        {
          return; //Already executed today
        }

        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (!Cashier.GetSystemUser(GU_USER_TYPE.SYS_SYSTEM, _db_trx.SqlTransaction, out _user_id, out _user_name))
          {
            Log.Error("ClosePreviousDaySystemCashierSession: Error in GetSystemUser.");

            return;
          }

          _sb.AppendLine(" SELECT   CS.CS_SESSION_ID                       ");
          _sb.AppendLine("        , CS.CS_OPENING_DATE                     ");
          _sb.AppendLine("   FROM   CASHIER_SESSIONS CS                    ");
          _sb.AppendLine("  WHERE   CS.CS_SESSION_ID =                     ");
          _sb.AppendLine("         (SELECT   MIN(CSI.CS_SESSION_ID)        ");
          _sb.AppendLine("            FROM   CASHIER_SESSIONS CSI          ");
          _sb.AppendLine("           WHERE   CSI.CS_STATUS  = @pStatusOpen ");
          _sb.AppendLine("             AND   CSI.CS_USER_ID = @pSysUser)   ");

          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pSysUser", SqlDbType.BigInt).Value = _user_id;
            _cmd.Parameters.Add("@pStatusOpen", SqlDbType.Int).Value = CASHIER_SESSION_STATUS.OPEN;

            using (SqlDataReader _reader = _cmd.ExecuteReader())
            {
              if (_reader.Read())
              {
                _session_id = _reader.GetInt64(0);
                _session_opening = _reader.GetDateTime(1);
              }
            }
          }

          if (_session_id > 0 && _session_opening < _today_opening)
          {
            _terminal_name = Environment.MachineName;
            _terminal_id = Cashier.ReadTerminalId(_db_trx.SqlTransaction, _terminal_name, GU_USER_TYPE.SYS_SYSTEM);

            if (_terminal_id == 0)
            {
              Log.Error(String.Format("GetOrOpenSystemCashiserSession.ReadTerminalId: Error getting the Terminal Id. Terminal Name = {0}", _terminal_name));

              return;
            }

            CommonCashierInformation.SetCashierInformation(_session_id, _user_id, _user_name, _terminal_id, _terminal_name);

            _session_info = CommonCashierInformation.CashierSessionInfo();
            _session_info.UserType = GU_USER_TYPE.SYS_SYSTEM;

            _cashier_movement = new CashierMovementsTable(_session_info);
            _cashier_movement.Add(0, CASHIER_MOVEMENT.CLOSE_SESSION, 0, 0, "");

            if (!_cashier_movement.Save(_db_trx.SqlTransaction))
            {
              Log.Error("ClosePreviousDaySystemCashierSession: Error in CashierMovementsTable.Save.");

              return;
            }

            if (!Cashier.CashierSessionCloseById(_session_id, _terminal_id, _terminal_name, _db_trx.SqlTransaction))
            {
              Log.Error("ClosePreviousDaySystemCashierSession: Error in CashierSessionCloseById.");

              return;
            }

            _db_trx.Commit();
          }
        }

        m_last_closed_system_session = _today_opening;

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    }  //ClosePreviousDaySystemCashierSession

  } // classs WCP_AccountsCreditsExpiration

} // namespace WSI.WCP