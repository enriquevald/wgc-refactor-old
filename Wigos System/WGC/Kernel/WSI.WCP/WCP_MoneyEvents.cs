//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_MoneyEvents.cs
// 
//   DESCRIPTION: WCP_MsgMoneyEvents class
// 
//        AUTHOR: Susana Sams�
// 
// CREATION DATE: 29-FEB-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 29-FEB-2012 SSC    First version.
// 01-JUN-2012 ACC    Fixed Bug #309: Error when transferring notes 
// 11-JUN-2012 SSC    Moved GetOrOpenSystemCashierSession(), DB_MobileBankInsertCashierMovement() and DB_MobileBankGetCashierInfo() methods to WSI.Common.Cashier
// 03-OCT-2012 ACC    Moved EnsureMobileBankInCashierSession() and UpdateMBOnOpenCashierSession methods to WSI.Common.Cashier
// 13-FEB-2013 HBB & MPO    Added functionality of one cashier session for each PromoBOX.
// 03-MAY-2013 ICS    Added Cashier Session ID to DB_MoneyEvent and DB_AddCredit
// 10-JUL-2013 RCI    When there are DB errors, a RC of WCP_RC_DATABASE_OFFLINE must be returned to the terminal.
// 15-NOV-2013 JBP    New parameter in DB_AddCredit for detect if is called from event. 
//                    Addapted DB_AddCredit for TITOMode.
//                    Added Is_EnabledMoneyEvent and Is_EnabledTransferAmount for TITOMode. 
// 18-DEC-2013 DRV    AC_VIRTUAL_TERMINAL will be deleted, the information is now on the tables terminals and cashier_terminals
// 17-MAR-2014 ICS    New routine 'DB_UpdateMoneyCollection' that updates expected total amounts when a bill is inserted
// 28-MAY-2015 DLL    Fixed Bug WIG-2397: Error recharge / withdrawal in PromoBox
// 14-SEP-2015 DHA    Product Backlog Item 3705: Added coins from terminals
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using WSI.Common;
using System.Xml;
using System.Collections;

namespace WSI.WCP
{
  public class WCP_MoneyEvents
  {

    //------------------------------------------------------------------------------
    // PURPOSE : Checks if amount and balance is ok and adds credit to the account.
    //
    //  PARAMS :
    //      - INPUT :
    //          - TrackData: Account trackdata
    //          - CardSessionId: Current play session ID
    //          - AmountToAdd: Amount to add to the account.
    //          - TransactionId
    //          - TerminalId:
    //          - TerminalName:
    //          - AcceptedDate: 
    //          - WcpSequenceId
    //          - WcpTransactionId
    //          - SqlTrx
    //
    //      - OUTPUT :
    //          - SessionBalanceAfterAdd: Balance AFTER adding the amount, is = SessionBalanceBeforeAdd if any error found.
    //
    // RETURNS :
    //      
    //   NOTES : MBF 26-JAN-2012
    //   Steps:
    //          1. Check if amount parameters are correct
    //          2. Check if trackdata and session are correct
    //          3. Check if the money event exists 
    //          4. Transfer amount to account
    //
    internal static void DB_AddCredit(String TrackData,
                                      Int64 CardSessionId,
                                      Decimal AmountToAdd,
                                      Int64 TransactionId,
                                      TerminalTypes TerminalType,
                                      Int32 TerminalId,
                                      String TerminalName,
                                      DateTime AcceptedDate,
                                      Int64 StackerId,
                                      Int64 WcpSequenceId,
                                      Int64 WcpTransactionId,
                                      Boolean IsEvent,
                                      out Decimal SessionBalanceAfterAdd,
                                      out ArrayList VoucherList,
                                      SqlTransaction SqlTrx)
    {
      WCP_AccountManager.WCP_Account _account = null;
      Int64 _account_id;
      PlaySession _playsession;
      Currency _promotion_amount;
      Currency _spent_used;
      Decimal previous_account_balance = 0;

      String _mb_track_data;
      Int64 _mb_account_id = 0;

      Boolean _already_transferred = false;

      MB_USER_TYPE _mb_user_type;

      Int64 _cashier_session_id;
      GU_USER_TYPE _gu_user_type;

      Cashier.GetMobileBankAndGUIUserType(TerminalType, out _mb_user_type, out _gu_user_type);

      _promotion_amount = 0;
      _spent_used = 0;

      // Output parameters initialization
      SessionBalanceAfterAdd = 0;
      VoucherList = new ArrayList();

      // 1. Check if amount parameters are correct
      // 2. Check if trackdata and session are correct
      // 3. Check if the money event exists 
      // 4. Ensure the system Mobile Bank used for Note Acceptor has an opened cashier session.
      // 5. Transfer amount to account

      //  1. Check if amount parameters are correct
      //
      if (AmountToAdd <= 0)
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_INVALID_AMOUNT, "Amount to transfer cannot be zero");
      }

      // 2.  Check if trackdata and session are correct
      //

      if (CardSessionId > 0)
      {
        _playsession = WSI.WCP.PlaySession.Get(TerminalId, CardSessionId, SqlTrx);
        if (_playsession.PlaySessionId != CardSessionId)
        {
          Log.Message("*** Money Events: Play Session not found. TerminalId / TransactionId / TrackData = " + TerminalId.ToString() +
                      "/ " + TransactionId.ToString() + "/ " + TrackData + ".");
        }
        else
        {
          _account_id = _playsession.AccountId;
          _account = WCP_AccountManager.Account(_account_id, SqlTrx);
        }
      }

      if (_account == null)
      {
        // Using the trackdata to obtain the account ID
        _account = WCP_AccountManager.Account(TrackData, SqlTrx);

        if (_account == null)
        {
          throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_CARD_NOT_VALID);
        }
      }
      // RCI 09-JUL-2013: If _account.id == 0, a DB error has occurred.
      if (_account.id == 0)
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_DATABASE_OFFLINE);
      }

      _account_id = _account.id;

      // QMP
      //  Insert a cashier session
      if (!Cashier.GetOrOpenSystemCashierSession(_gu_user_type, TerminalName, out _cashier_session_id, SqlTrx))
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR);  // TODO: Add a specific WCP_ResponseCode for the MoneyEvent.
      }

      //JBP 13-NOV-2013: Only if is not SYS_TITO or if is SYS_TITO and come from Event. 
      if (Is_EnabledMoneyEvent(_mb_user_type, IsEvent))
      {
        // 3. Check if the money event exists, if not, insert an empty money event using the money event function.
        if (!DB_MoneyEvent(TerminalId, TransactionId, AmountToAdd, AcceptedDate, TrackData, CardSessionId, _account_id, AcceptedDate, WGDB.Now,
                           StackerId, _cashier_session_id, out _already_transferred, SqlTrx))
        {
          throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR);  // TODO: Add a specific WCP_ResponseCode for the MoneyEvent.
        }

        if (_already_transferred)
        {
          Log.Message("*** Money Events: Money already transferred. TerminalId/ TransactionId/ AccountId = " + TerminalId.ToString() +
                      "/ " + TransactionId.ToString() + "/ " + _account_id.ToString() + ".");
          return;
        }
      }
      
      // 4. Ensure the system Mobile Bank used for Note Acceptor has an opened cashier session.
      if (!Cashier.EnsureMobileBankInCashierSession(_mb_user_type, TerminalName, _cashier_session_id, out _mb_account_id, out _mb_track_data, SqlTrx))
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR);  // TODO: Add a specific WCP_ResponseCode for the MoneyEvent.
      }

      //JBP 13-NOV-2013: Only if is not SYS_TITO or if is SYS_TITO and not come from Event. 
      if (Is_EnabledTransferAmount(_mb_user_type, IsEvent))
      {
        // 5. Transfer amount to account      
        WCP_TransferAmounts.DB_TransferAmountToPlayerAccount(_mb_track_data, TrackData, TerminalId, TerminalName, _mb_account_id, AmountToAdd, WcpSequenceId, WcpTransactionId,
                                                             out previous_account_balance, out SessionBalanceAfterAdd, out VoucherList, SqlTrx, _account, _mb_user_type);
      }

    } // DB_AddCredit

    //------------------------------------------------------------------------------
    // PURPOSE : Gets if is enabled MoneyEvent if is SYS_TITO and come from Event
    //
    //  PARAMS :
    //      - INPUT :
    //          - Boolean: IsEvent
    //          - MB_USER_TYPE: UserType
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean
    //      
    //
    internal static Boolean Is_EnabledMoneyEvent(MB_USER_TYPE UserType, Boolean IsEvent)
    {
      if (!WSI.Common.TITO.Utils.IsTitoMode() || (UserType != MB_USER_TYPE.SYS_TITO && UserType != MB_USER_TYPE.SYS_PROMOBOX))
      {
        return true;
      }
      else
      {
        if (IsEvent)
        {
          return true;
        }
      }

      return false;
    } // Is_EnabledMoneyEvent

    //------------------------------------------------------------------------------
    // PURPOSE : Gets if is enabled TransferAmount if is SYS_TITO and not come from Thread
    //
    //  PARAMS :
    //      - INPUT :
    //          - Boolean: IsEvent
    //          - MB_USER_TYPE: UserType
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean
    //      
    //
    internal static Boolean Is_EnabledTransferAmount(MB_USER_TYPE UserType, Boolean IsEvent)
    {
      if (!WSI.Common.TITO.Utils.IsTitoMode() ||
          (UserType != MB_USER_TYPE.SYS_TITO && UserType != MB_USER_TYPE.SYS_PROMOBOX)
        || WSI.Common.Misc.IsMico2Mode())
      {
        return true;
      }
      else
      {
        if (!IsEvent)
        {
          return true;
        }
      }

      return false;
    } // Is_EnabledTransferAmount


    //------------------------------------------------------------------------------
    // PURPOSE : Inserts or updates the money event
    //
    //  PARAMS :
    //      - INPUT :
    //          - TerminalId
    //          - TransactionId
    //          - Amount: Note amount
    //          - AcceptedDate: Note accepted date, provided both by Money Event and Add Credit, 
    //                          should be equal to ReportedDate.
    //          - TrackData: Players trackdata provided by Add Credit.
    //          - PlaySessionId: Provided both by Money Event and Add Credit.
    //          - AccountId: Date provided by Add Credit.
    //          - ReportedDate: Ignored!
    //          - TransferedDate: Date provided by Add Credit.
    //          - StackerId: Provided by Money Event.
    //          - CashierSessionId: Cashier session currently assigned to the terminal.
    //          - SqlTrx:
    //
    //      - OUTPUT :
    //          - AlreadyTransferred: True if amount is already transferred to account
    //
    // RETURNS :
    //      
    //
    internal static Boolean DB_MoneyEvent(Int32 TerminalId,
                                          Int64 TransactionId,
                                          Decimal Amount,
                                          DateTime AcceptedDate,
                                          String TrackData,
                                          Int64 PlaySessionId,
                                          Int64 AccountId,
                                          DateTime ReportedDate,
                                          DateTime TransferedDate,
                                          Int64 StackerId,
                                          Int64 CashierSessionId,
                                          out Boolean AlreadyTransferred,
                                          SqlTransaction SqlTrx)
    {
      SqlCommand _sql_cmd;
      StringBuilder _sql_txt;
      Int64 _new_account_id = 0;
      Int64 _old_account_id = 0;

      AlreadyTransferred = false;

      _sql_txt = new StringBuilder();

      try
      {
        // IF NOT EXISTS --> INSERT
        _sql_txt.AppendLine("IF NOT EXISTS ");
        _sql_txt.AppendLine("( ");
        _sql_txt.AppendLine("   SELECT   TM_TRANSFERRED_TO_ACCOUNT_ID ");
        _sql_txt.AppendLine("     FROM   TERMINAL_MONEY ");
        _sql_txt.AppendLine("    WHERE   TM_TERMINAL_ID    = @pTerminalId ");
        _sql_txt.AppendLine("      AND   TM_TRANSACTION_ID = @pTransactionId ");
        _sql_txt.AppendLine(") ");
        _sql_txt.AppendLine("   INSERT INTO   TERMINAL_MONEY ");
        _sql_txt.AppendLine("               ( TM_TERMINAL_ID ");
        _sql_txt.AppendLine("               , TM_TRANSACTION_ID  ");
        _sql_txt.AppendLine("               , TM_STACKER_ID ");
        _sql_txt.AppendLine("               , TM_ACCEPTED ");
        _sql_txt.AppendLine("               , TM_AMOUNT ");
        _sql_txt.AppendLine("               , TM_TRACKDATA ");
        _sql_txt.AppendLine("               , TM_PLAY_SESSION_ID ");
        _sql_txt.AppendLine("               , TM_TRANSFERRED_TO_ACCOUNT_ID ");
        _sql_txt.AppendLine("               , TM_TRANSFERRED  ");
        _sql_txt.AppendLine("               , TM_INTO_ACCEPTOR ");

        if (CashierSessionId != 0)
          _sql_txt.AppendLine("               , TM_CASHIER_SESSION_ID ");

        _sql_txt.AppendLine("               ) ");
        _sql_txt.AppendLine("        VALUES ( @pTerminalId ");
        _sql_txt.AppendLine("               , @pTransactionId  ");
        _sql_txt.AppendLine("               , @pStackerId  ");
        _sql_txt.AppendLine("               , @pAcceptedDate  ");
        _sql_txt.AppendLine("               , @pAmount  ");
        _sql_txt.AppendLine("               , @pTrackData  ");
        _sql_txt.AppendLine("               , @pPlaySessionId  ");
        _sql_txt.AppendLine("               , @pAccountId ");
        _sql_txt.AppendLine("               , @pTransferredDate   ");
        _sql_txt.AppendLine("               , @pIntoAcceptor ");

        if (CashierSessionId != 0)
          _sql_txt.AppendLine("               , @pCashierSessionId ");

        _sql_txt.AppendLine("               ) ");
        _sql_txt.AppendLine(" ELSE ");

        _sql_txt.AppendLine("   UPDATE   TERMINAL_MONEY ");
        _sql_txt.AppendLine("      SET   TM_TRANSFERRED_TO_ACCOUNT_ID = CASE WHEN (TM_TRANSFERRED_TO_ACCOUNT_ID IS NULL) THEN @pAccountId ");
        _sql_txt.AppendLine("               ELSE TM_TRANSFERRED_TO_ACCOUNT_ID END ");
        _sql_txt.AppendLine("          , TM_TRANSFERRED               = CASE WHEN( TM_TRANSFERRED IS NULL) THEN @pTransferredDate ");
        _sql_txt.AppendLine("               ELSE TM_TRANSFERRED END ");

        if (CashierSessionId != 0)
          _sql_txt.AppendLine("          , TM_CASHIER_SESSION_ID = @pCashierSessionId ");

        _sql_txt.AppendLine("   OUTPUT   INSERTED.TM_TRANSFERRED_TO_ACCOUNT_ID AS NEW_ACCOUNT ");
        _sql_txt.AppendLine("          , DELETED.TM_TRANSFERRED_TO_ACCOUNT_ID AS OLD_ACCOUNT  ");
        _sql_txt.AppendLine("    WHERE   TM_TERMINAL_ID     = @pTerminalId ");
        _sql_txt.AppendLine("      AND   TM_TRANSACTION_ID  = @pTransactionId ");


        _sql_cmd = new SqlCommand(_sql_txt.ToString(), SqlTrx.Connection, SqlTrx);

        _sql_cmd.Parameters.Add("@pTransactionId", SqlDbType.Int).Value = TransactionId;
        _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.BigInt).Value = TerminalId;

        _sql_cmd.Parameters.Add("@pAcceptedDate", SqlDbType.DateTime).Value = AcceptedDate;
        _sql_cmd.Parameters.Add("@pAmount", SqlDbType.Money).Value = Amount;
        _sql_cmd.Parameters.Add("@pStackerId", SqlDbType.BigInt).Value = StackerId;
        _sql_cmd.Parameters.Add("@pTrackData", SqlDbType.NVarChar).Value = TrackData;
        _sql_cmd.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt).Value = PlaySessionId;
        _sql_cmd.Parameters.Add("@pIntoAcceptor", SqlDbType.Bit).Value = 1;

        _sql_cmd.Parameters.Add("@pCashierSessionId", SqlDbType.BigInt).Value = CashierSessionId;


        // RRR 21-NOV-2013
        // Force transferred status in TITO mode
        if (Common.TITO.Utils.IsTitoMode())
        {
          _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
          _sql_cmd.Parameters.Add("@pTransferredDate", SqlDbType.DateTime).Value = WGDB.Now;
        }

        else
        {
          if (TransferedDate == DateTime.MinValue)
          {
            _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = DBNull.Value;
            _sql_cmd.Parameters.Add("@pTransferredDate", SqlDbType.DateTime).Value = DBNull.Value;
          }
          else
          {
            _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
            _sql_cmd.Parameters.Add("@pTransferredDate", SqlDbType.DateTime).Value = TransferedDate;
          }
        }

        for (int _idx_try = 0; _idx_try < 2; _idx_try++)
        {
          try
          {
            using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
            {
              if (_reader.Read())
              {
                if (!_reader.IsDBNull(1)) //OLD_ACCOUNT
                {
                  AlreadyTransferred = true;

                  _old_account_id = _reader.GetInt64(1);

                  if (!_reader.IsDBNull(0)) //NEW_ACCOUNT
                  {
                    _new_account_id = _reader.GetInt64(0);
                  }
                  if (_old_account_id != _new_account_id)
                  {
                    Log.Message("*** Money Events: Money is already transferred to another account. TerminalId/ TransactionId/ Transferred to AccountId/ Current AccountId = "
                                                                                                  + TerminalId.ToString() + "/ " + TransactionId.ToString()
                                                                                                  + "/ " + _old_account_id.ToString()
                                                                                                  + "/ " + _new_account_id.ToString()
                                                                                                  + ".");
                  }
                }
              } // if _reader.Read()
            } // using SqlDataReader

            // Executed OK !
            break;
          } // try
          catch (SqlException _sql_ex)
          {
            if (_sql_ex.Number != 2601 && _sql_ex.Number != 2627) // Unique Key Violation
            {
              Log.Exception(_sql_ex);

              return false;
            }
          }
        } // for _idx_try

        // Check if it must update money collection
        if (!AlreadyTransferred && CashierSessionId > 0 && Cage.IsCageEnabled())
        {
          if (!DB_UpdateMoneyCollection(Amount, CashierSessionId, SqlTrx))
          {
            Log.Error("DB_MoneyEvent: Can't update money collection. Cashier Session Id: " + CashierSessionId.ToString() 
                      + " Bill Amount: " + Amount.ToString());
            return false;
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    }


    public static Boolean TITO_TransferBillFromTerminal(Int32 TerminalId, String TerminalName, Int64 VirtualAccountId, String VirtualTrackData,
                                                        Int64 MbAccountId, Decimal Amount, Int64 TransactionId, Int64 SequenceId, GU_USER_TYPE UserType,
                                                        SqlTransaction Trx)
    {
      CashierMovementsTable _cashier_movements;
      TYPE_SPLITS _splits;
      Decimal _cash_in_split1;
      Decimal _cash_in_split2;
      CashierSessionInfo _csi;
      Int64 _operation_id;
      Terminal _terminal_info;
      Currency _amount_exchange;
      Currency _amount_issue;
      String _terminal_iso_code;


      Split.ReadSplitParameters(out _splits);

      _csi = CommonCashierInformation.CashierSessionInfo();
      _csi.UserType = UserType;
      _csi.IsMobileBank = true;
      _amount_exchange = 0;
      _amount_issue = Amount;
      _terminal_iso_code = CurrencyExchange.GetNationalCurrency();

      _cashier_movements = new CashierMovementsTable(_csi, TerminalName);
      
      // DHA: 27-JAN-2016: dual currency - added conversion movement for bill 
      if (Misc.IsFloorDualCurrencyEnabled())
      {
        _amount_exchange = Amount;
        // Get terminal ISO Code
        _terminal_info = Terminal.GetTerminal(TerminalId, Trx);

        if (String.IsNullOrEmpty(_terminal_info.terminal_ISO_code))
        {
          _terminal_info.terminal_ISO_code = _terminal_iso_code;
        }

        _terminal_iso_code = _terminal_info.terminal_ISO_code;
                
        if(_terminal_info.terminal_ISO_code != CurrencyExchange.GetNationalCurrency())
        {
          // Translate Amount to national currency if terminal is configured with a different currency than national
          _amount_exchange = CurrencyExchange.GetExchange(Amount, _terminal_info.terminal_ISO_code, CurrencyExchange.GetNationalCurrency(), Trx);
        }

        // Create the cashier movement for bill with conversion
        _cashier_movements.Add(TransactionId, CASHIER_MOVEMENT.BILL_IN_EXCHANGE_DUAL_CURRENCY, 0, VirtualAccountId, VirtualTrackData, "", _terminal_info.terminal_ISO_code);
        _cashier_movements.SetInitialBalanceInLastMov(Amount);
        _cashier_movements.SetAddAmountInLastMov(_amount_exchange);

        Amount = _amount_exchange;
      }
      
      // Calculate cash in splits
      _cash_in_split1 = Math.Round(Amount * _splits.company_a.cashin_pct / 100, 2, MidpointRounding.AwayFromZero);
      _cash_in_split2 = Amount - _cash_in_split1;

      // MPO & JBC & RCI 03-MAR-2014: These movements (in Cashier and MB) don't have an associated operation.
      _operation_id = 0;

      _cashier_movements.Add(_operation_id, CASHIER_MOVEMENT.NA_CASH_IN_SPLIT1, _cash_in_split1,
                             VirtualAccountId, VirtualTrackData);
      if (_cash_in_split2 > 0)
      {
        _cashier_movements.Add(_operation_id, CASHIER_MOVEMENT.NA_CASH_IN_SPLIT2, _cash_in_split2,
                               VirtualAccountId, VirtualTrackData);
      }

      if (!_cashier_movements.Save(Trx))
      {
        Log.Error("DB_MoneyEvent. Unable to save cashier_movements NA_CASH_IN_SPLIT1/2 for terminal ID = " + TerminalId.ToString());

        return false;
      }

      if (!WCP_BusinessLogic.DB_MobileBankInsertMovement(_csi.CashierSessionId, MbAccountId, TerminalId, TerminalName, SequenceId, TransactionId,
                                                         VirtualAccountId, VirtualTrackData, 0, MBMovementType.TransferCredit,
                                                         0, _amount_issue, 0, -_amount_issue, _operation_id, _splits, _terminal_iso_code, Trx))
      {
        Log.Error("DB_MoneyEvent - DB_MobileBankInsertMovement. Unable to insert MB movement for terminal ID = " + TerminalId.ToString());

        return false;
      }

      return true;
    }

    internal static Boolean GetAccountIdFromTerminalId(Int32 TerminalId, out Int64 AccountId, SqlTransaction Trx)
    {
      DataSet _ds;
      StringBuilder _sb;

      _ds = new DataSet();
      _sb = new StringBuilder();

      AccountId = 0;

      // DRV 18-DEC-2013: the virtual account for each terminal it's now in terminals and cashier_terminals
      _sb.AppendLine(" SELECT   ISNULL ((SELECT  PS_ACCOUNT_ID  ");
      _sb.AppendLine("                     FROM  PLAY_SESSIONS  ");
      _sb.AppendLine("                    WHERE  PS_TERMINAL_ID = @TerminalId  ");
      _sb.AppendLine("                      AND  PS_STATUS      = 0 ) ");

      _sb.AppendLine("         , (SELECT  TOP 1  TE_VIRTUAL_ACCOUNT_ID ");
      _sb.AppendLine("                     FROM  TERMINALS ");
      _sb.AppendLine("                    WHERE  TE_TERMINAL_ID = @TerminalId ) ");
      _sb.AppendLine("          ) AS ACCOUNT_ID ");

      try
      {
        using (SqlDataAdapter _da = new SqlDataAdapter())
        {
          _da.SelectCommand = new SqlCommand(_sb.ToString());
          _da.SelectCommand.Connection = Trx.Connection;
          _da.SelectCommand.Transaction = Trx;

          //        - UpdateCommand, DeleteCommand & InsertCommand
          _da.DeleteCommand = null;
          _da.InsertCommand = null;
          _da.UpdateCommand = null;

          _da.SelectCommand.Parameters.Add("@TerminalId", SqlDbType.Int).Value = TerminalId;

          //    - Execute Select command
          _da.Fill(_ds);

          if (_ds.Tables[0].Rows.Count == 0)
          {
            return false;
          }
        }

        AccountId = (Int64)_ds.Tables[0].Rows[0].ItemArray[0];

        return true;
      }

      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
    }


    public static Boolean DB_UpdateMoneyCollection(Decimal Amount, Int64 CashierSessionId, SqlTransaction Trx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      _sb.AppendLine("UPDATE  MONEY_COLLECTIONS ");
      _sb.AppendLine("   SET  MC_EXPECTED_BILL_AMOUNT = ISNULL(MC_EXPECTED_BILL_AMOUNT, 0) + @pAmount ");
      _sb.AppendLine("     ,  MC_EXPECTED_BILL_COUNT  = ISNULL(MC_EXPECTED_BILL_COUNT,  0) + 1 ");
      _sb.AppendLine(" WHERE  MC_CASHIER_SESSION_ID   = @pCashierSessionId ");
      _sb.AppendLine("   AND  MC_STATUS               = @pOpenStatus ");

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pAmount", SqlDbType.Money).Value = Amount;
          _cmd.Parameters.Add("@pCashierSessionId", SqlDbType.BigInt).Value = CashierSessionId;
          _cmd.Parameters.Add("@pOpenStatus", SqlDbType.Int).Value = (Int32)TITO_MONEY_COLLECTION_STATUS.OPEN;

          return (_cmd.ExecuteNonQuery() == 1);
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // DB_UpdateMoneyCollection

    public static Boolean DB_UpdateMoneyCollectionCoins(Decimal Amount, Int64 CashierSessionId, SqlTransaction Trx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      _sb.AppendLine("UPDATE  MONEY_COLLECTIONS ");
      _sb.AppendLine("   SET  MC_EXPECTED_COIN_AMOUNT = ISNULL(MC_EXPECTED_COIN_AMOUNT, 0) + @pAmount ");
      _sb.AppendLine(" WHERE  MC_CASHIER_SESSION_ID   = @pCashierSessionId ");
      _sb.AppendLine("   AND  MC_STATUS               = @pOpenStatus ");

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pAmount", SqlDbType.Money).Value = Amount;
          _cmd.Parameters.Add("@pCashierSessionId", SqlDbType.BigInt).Value = CashierSessionId;
          _cmd.Parameters.Add("@pOpenStatus", SqlDbType.Int).Value = (Int32)TITO_MONEY_COLLECTION_STATUS.OPEN;

          return (_cmd.ExecuteNonQuery() == 1);
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // DB_UpdateMoneyCollectionCoins

    public static Boolean DB_UpdateMoneyCollectionOuts(Int32 MeterCode, Decimal Amount, Int64 CashierSessionId, SqlTransaction Trx)
    {
      StringBuilder _sb;
      Int32 _meter_out_bills;
      Int32 _meter_out_coins;
      Int32 _meter_out_cents;

      _meter_out_bills = GeneralParam.GetInt32("GamingHall.SasMeter", "Meter.OutBills", 0);
      _meter_out_coins = GeneralParam.GetInt32("GamingHall.SasMeter", "Meter.OutCoins", 0);
      _meter_out_cents = GeneralParam.GetInt32("GamingHall.SasMeter", "Meter.OutCents", 0);

      if (_meter_out_bills == 0 && _meter_out_cents == 0 && _meter_out_coins == 0)
      {
        return true;
      }

      _sb = new StringBuilder();

      _sb.AppendLine("UPDATE  MONEY_COLLECTIONS ");
      _sb.AppendLine("   SET  ");

      if (_meter_out_bills == MeterCode)
      {
        _sb.AppendLine("        MC_OUT_BILLS = ISNULL(MC_OUT_BILLS, 0) + @pAmount ");
      }

      if (_meter_out_coins == MeterCode)
      {
        _sb.AppendLine("        MC_OUT_COINS = ISNULL(MC_OUT_COINS, 0) + @pAmount ");
      }

      if (_meter_out_cents == MeterCode)
      {
        _sb.AppendLine("        MC_OUT_CENTS = ISNULL(MC_OUT_CENTS, 0) + @pAmount ");
      }

      _sb.AppendLine(" WHERE  MC_CASHIER_SESSION_ID   = @pCashierSessionId ");
      _sb.AppendLine("   AND  MC_STATUS               = @pOpenStatus ");

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pAmount", SqlDbType.Money).Value = Amount;
          _cmd.Parameters.Add("@pCashierSessionId", SqlDbType.BigInt).Value = CashierSessionId;
          _cmd.Parameters.Add("@pOpenStatus", SqlDbType.Int).Value = (Int32)TITO_MONEY_COLLECTION_STATUS.OPEN;

          return (_cmd.ExecuteNonQuery() == 1);
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // DB_UpdateMoneyCollectionOuts


    public static Boolean DB_InsertOrUpdateCashierTerminalMoney(Decimal Denomination, Int32 TerminalId, SqlTransaction Trx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();


      _sb.AppendLine(" DECLARE @CashierSessionId AS BIGINT ");
      _sb.AppendLine(" DECLARE @CollectionId AS BIGINT ");
      _sb.AppendLine(" SELECT   @CashierSessionId = MC_CASHIER_SESSION_ID ");
      _sb.AppendLine("        , @CollectionId     = MC_COLLECTION_ID ");
      _sb.AppendLine("   FROM   MONEY_COLLECTIONS ");
      _sb.AppendLine("  WHERE   MC_STATUS = @pMCStatus ");
      _sb.AppendLine("    AND   MC_TERMINAL_ID = @pTerminalId ");
      _sb.AppendLine(" IF NOT EXISTS (SELECT   1 ");
      _sb.AppendLine("                  FROM   CASHIER_TERMINAL_MONEY ");
      _sb.AppendLine("                 WHERE   CTM_SESSION_ID = @CashierSessionId ");
      _sb.AppendLine("                   AND   CTM_TERMINAL_ID = @pTerminalId ");
      _sb.AppendLine("                   AND   CTM_DENOMINATION = @pDenomination ) ");
      _sb.AppendLine(" INSERT INTO   CASHIER_TERMINAL_MONEY");
      _sb.AppendLine("             ( CTM_SESSION_ID ");
      _sb.AppendLine("             , CTM_TERMINAL_ID ");
      _sb.AppendLine("             , CTM_DENOMINATION ");
      _sb.AppendLine("             , CTM_QUANTITY ");
      _sb.AppendLine("             , CTM_MONEY_COLLECTION_ID )");
      _sb.AppendLine("      VALUES ( @CashierSessionId");
      _sb.AppendLine("             , @pTerminalId ");
      _sb.AppendLine("             , @pDenomination ");
      _sb.AppendLine("             , @pQuantity ");
      _sb.AppendLine("             , @CollectionId ) ");
      _sb.AppendLine(" ELSE                             ");
      _sb.AppendLine(" UPDATE   CASHIER_TERMINAL_MONEY ");
      _sb.AppendLine("    SET   CTM_QUANTITY = CTM_QUANTITY + @pQuantity ");
      _sb.AppendLine("  WHERE   CTM_SESSION_ID =  @CashierSessionId ");
      _sb.AppendLine("    AND   CTM_TERMINAL_ID = @pTerminalId ");
      _sb.AppendLine("    AND   CTM_DENOMINATION = @pDenomination  ");

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pTerminalId", SqlDbType.BigInt).Value = TerminalId;
          _cmd.Parameters.Add("@pDenomination", SqlDbType.Int).Value = Denomination;
          _cmd.Parameters.Add("@pQuantity", SqlDbType.Int).Value = 1;
          _cmd.Parameters.Add("@pMCStatus", SqlDbType.Int).Value = (Int32)TITO_MONEY_COLLECTION_STATUS.OPEN;

          return (_cmd.ExecuteNonQuery() == 1);
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // DB_UpdateMoneyCollection

  }
}
