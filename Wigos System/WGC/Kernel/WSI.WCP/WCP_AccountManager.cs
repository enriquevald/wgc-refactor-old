//------------------------------------------------------------------------------
// Copyright � 2010 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : WCP_AccountManager.cs
// 
//   DESCRIPTION : 
//
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 20-JUL-2010 AJQ    First release
// 10-JUL-2013 RCI    When there are DB errors, a RC of WCP_RC_DATABASE_OFFLINE must be returned to the terminal.
// 06-SEP-2013 ACM    Implements new GP RequestPin.DisabledForAnonymous
// 17-MAR-2014 JRM    Added new functionality. Description contained in RequestPinCodeDUT.docx DUT
// 02-JUN-2014 RRR    Fixed Bug WIG-959: PIN required in anonymous accounts when general param disables it
// 03-JUL-2014 JMM    Only first name, instead of the whole name, will be shown on EGM Win and LKT SAS Host
// 11-DIC-2014 JML    Add output for UserType & BlockReason in CardType
// 08-JUL-2015 DLL    WIG-2555: Change text anonymous account by general param
// 28-NOV-2016 FAV    PBI 20372: EGASA - Allow repeats cards
// 17-APR-2018 ACC    WIGOS-10157 Create new account on card-in
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Threading;
using System.Data.SqlClient;
using WSI.Common;

namespace WSI.WCP
{
  public static class WCP_AccountManager
  {
    public class WCP_Account
    {
      public Int64 id;
      public String display_holder_name;
      public Decimal display_balance;
      public String int_track_data;
      public String ext_track_data;

      internal String holder_name;
      internal String holder_first_name;
      internal Decimal real_balance;

      public AccountType type;
      public Boolean blocked;
      public Int64 play_session_id;
      public int terminal_id;
      public Decimal points;
      public int last_terminal_id;
      public DateTime birth_date;
      public int level;

      // RCI 31-MAY-2011: Needed to allow MB Transfers.
      public TerminalTypes terminal_type;
      public String pin;
      public Int32 pin_failures;

      // FJC 01-NOV-2015
      public Boolean reserved_mode;

      internal string[] VoucherAccountInfo(String VisibleTrackdata)
      {
        String[] _voucher_account_info;
        String _name;

        _name = "";

        if (holder_name != "")
        {
          _name = holder_name;
        }
        else
        {
          _name = GeneralParam.GetString("Cashier.Voucher", "AnonymousAccount.Description");
        }

        _voucher_account_info = new String[3];

        _voucher_account_info[0] = Resource.String("STR_VOUCHER_CARD_DRAW_HOLDER_NAME")
                              + ": " + _name + "\n";
        _voucher_account_info[1] = Resource.String("STR_VOUCHER_CARD_ACCOUNT_ID")
                              + ": " + id.ToString("000000") + "\n";
        _voucher_account_info[2] = Resource.String("STR_VOUCHER_CARD_CARD_ID")
                              + ": " + WSI.Common.Misc.VisibleTrackdata(VisibleTrackdata) + "\n";

        return _voucher_account_info;
      } // VoucherAccountInfo

    } // WCP_Account

    private static CashlessMode m_cashless_mode = CashlessMode.NONE;

    public static WCP_Account[] Account(String[] ExternalTrackData, SqlTransaction SqlTrx)
    {
      WCP_Account[] _accounts;
      int _idx;

      _accounts = new WCP_Account[ExternalTrackData.Length];

      _idx = 0;
      foreach (String _trackdata in ExternalTrackData)
      {
        _accounts[_idx] = Account(_trackdata, SqlTrx);
        _idx++;
      }

      return _accounts;
    }

    public static WCP_Account Account(String ExternalTrackData, SqlTransaction SqlTrx)
    {
      WCP_Account _account;

      _account = new WCP_Account();
      _account.id = 0;
      _account.ext_track_data = ExternalTrackData;
      _account.type = AccountType.ACCOUNT_UNKNOWN;
      _account.blocked = true;

      // RCI 09-JUL-2013: If ExternalTrackData is not valid, return NULL.
      //                  When a DB error occurrs, return an empty WCP_Account object.

      try
      {
        if (!WCP_BusinessLogic.DB_ReadAccount(ExternalTrackData,
                                              out _account.int_track_data,
                                              out _account.id,
                                              out _account.type,
                                              out _account.blocked,
                                              out _account.real_balance,
                                              out _account.play_session_id,
                                              out _account.terminal_id,
                                              out _account.holder_name,
                                              out _account.holder_first_name,
                                              out _account.points,
                                              out _account.last_terminal_id,
                                              out _account.terminal_type,
                                              out _account.birth_date,
                                              out _account.level,
                                              out _account.pin,
                                              out _account.pin_failures,
                                              out _account.reserved_mode,
                                              SqlTrx))
        {
          return null;
        }

        _account.display_balance = _account.real_balance;
        _account.display_holder_name = _account.holder_first_name;

        return _account;
      }
      catch (WCP_Exception _wcp_ex)
      {
        if (_wcp_ex.ResponseCode == WCP_ResponseCodes.WCP_RC_CARD_NOT_VALID)
        {
          // ExternalTrackData is not valid, return null.
          return null;
        }

        Log.Exception(_wcp_ex);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      // DB error, return an empty WCP_Account object.
      return new WCP_Account();
    }

    public static WCP_Account Account(Int64 AccountID, SqlTransaction SqlTrx)
    {
      WCP_Account _account;

      _account = new WCP_Account();
      _account.id = AccountID;
      _account.type = AccountType.ACCOUNT_UNKNOWN;
      _account.blocked = true;

      // RCI 09-JUL-2013: If AccountID is not valid, return NULL.
      //                  When a DB error occurrs, return an empty WCP_Account object.

      try
      {
        if (!WCP_BusinessLogic.DB_ReadAccount(AccountID,
                                              out _account.int_track_data,
                                              out _account.type,
                                              out _account.blocked,
                                              out _account.real_balance,
                                              out _account.play_session_id,
                                              out _account.terminal_id,
                                              out _account.holder_name,
                                              out _account.holder_first_name,
                                              out _account.points,
                                              out _account.last_terminal_id,
                                              out _account.terminal_type,
                                              out _account.birth_date,
                                              out _account.level,
                                              out _account.pin,
                                              out _account.pin_failures,
                                              out _account.reserved_mode,
                                              SqlTrx))
        {
          return null;
        }

        _account.display_balance = _account.real_balance;
        _account.display_holder_name = _account.holder_first_name;
        _account.ext_track_data = _account.int_track_data;

        if (_account.type == AccountType.ACCOUNT_WIN || _account.type == AccountType.ACCOUNT_VIRTUAL_TERMINAL)
        {
          if (!CardNumber.TrackDataToExternal(out _account.ext_track_data, _account.int_track_data, (int)CardTrackData.CardsType.PLAYER))
          {
            _account.ext_track_data = "";
          }
        }

        return _account;
      }
      catch (WCP_Exception _wcp_ex)
      {
        if (_wcp_ex.ResponseCode == WCP_ResponseCodes.WCP_RC_CARD_NOT_VALID)
        {
          // AccountID is not valid, return null.
          return null;
        }

        Log.Exception(_wcp_ex);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      // DB error, return an empty WCP_Account object.
      return new WCP_Account();
    }

    public static void Init()
    {
      while (true)
      {
        m_cashless_mode = WCP_BusinessLogic.CashlessMode;
        if (m_cashless_mode != CashlessMode.NONE)
        {
          break;
        }
        System.Threading.Thread.Sleep(1000);
      } // while (true)
    } // Init

    // 11-DIC-2014 JML Add output for UserType & BlockReason
    public static WCP_CardTypes CardType(String ExternalTrackData, SqlTransaction SqlTrx, out Int64 LinkedId, out String HolderName, out Int32 UserType, out Int32 BlockReason)
    {
      Boolean _is_anonymous;
      WCP_CardTypes _type;
      Boolean _found_account;
      Boolean _account_create_on_card_in;

      _type = WCP_CardTypes.CARD_TYPE_UNKNOWN;
      LinkedId = 0;
      HolderName = "";
      UserType = -1; //Non valid user Type
      BlockReason = 0;

      _is_anonymous = true;

      if (m_cashless_mode == CashlessMode.ALESIS)
      {
        return WCP_CardTypes.CARD_TYPE_PLAYER;
      }

      try
      {
        using (SqlCommand _cmd = new SqlCommand("SELECT CA_LINKED_TYPE, CA_LINKED_ID FROM CARDS WHERE CA_TRACKDATA = @pExternalTrackdata AND CA_LINKED_TYPE <>  @pCardTypeEmployee", SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pExternalTrackdata", SqlDbType.Char, 20).Value = ExternalTrackData;
          _cmd.Parameters.Add("@pCardTypeEmployee", SqlDbType.Int).Value = WCP_CardTypes.CARD_TYPE_EMPLOYEE;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (_reader.Read())
            {
              _type = (WCP_CardTypes)_reader.GetInt32(0);
              LinkedId = _reader.GetInt64(1);
            }
            _reader.Close();
          }
        }

        if (_type == WCP_CardTypes.CARD_TYPE_UNKNOWN)
        {
          _type = WCP_CardTypes.CARD_TYPE_PLAYER;
          _found_account = false;

          using (SqlCommand _cmd = new SqlCommand("SELECT AC_ACCOUNT_ID, AC_HOLDER_NAME, AC_USER_TYPE, AC_BLOCK_REASON FROM ACCOUNTS WHERE AC_TRACK_DATA = dbo.TrackDataToInternal (@pExternalTrackdata)", SqlTrx.Connection, SqlTrx))
          {
            _cmd.Parameters.Add("@pExternalTrackdata", SqlDbType.Char, 20).Value = ExternalTrackData;

            try
            {
            using (SqlDataReader _reader = _cmd.ExecuteReader())
            {
              if (_reader.Read())
              {
                LinkedId = _reader.GetInt64(0);
                HolderName = _reader.IsDBNull(1) ? "" : _reader.GetString(1);
                UserType = _reader.IsDBNull(2) ? 0 : _reader.GetInt32(2);
                BlockReason = _reader.IsDBNull(3) ? 0 : _reader.GetInt32(3);

                  _found_account = true;
              }
                else
                {
                  // ACC 13-APR-2018 Account NOT Exist for this card.
                  _found_account = false;
                }
              _reader.Close();
            }
            }
            catch { };

            if ( !_found_account  )
            {
              // Check Parameter and create Account if not found
              _account_create_on_card_in = GeneralParam.GetBoolean("Account", "CreateOnCardIn", false);

              //
              // ACC 18-APR-2018
              // WIGOS-10157 Create new account on card-in
              // CREATE ACCOUNT
              //   - ACC: #account
              //   - Level 1
              //

              if (_account_create_on_card_in)
              {
                ExternalTrackData _track_data;

                _track_data = new ExternalTrackData(ExternalTrackData);

                if (_track_data.IsCorrectTrackData)
                {
                StringBuilder _sql_sb;

                try
                {
                  _sql_sb = new StringBuilder();

                  _sql_sb.AppendLine(" DECLARE   @AccountId bigint ");
                  _sql_sb.AppendLine(" DECLARE   @SeqId     bigint ");
                    _sql_sb.AppendLine(" DECLARE   @SiteId    as INT ");
                  _sql_sb.AppendLine(" DECLARE   @HolderName     nvarchar(200) ");
                  _sql_sb.AppendLine("  UPDATE   SEQUENCES ");
                  _sql_sb.AppendLine("     SET   SEQ_NEXT_VALUE = SEQ_NEXT_VALUE + 1 ");
                  _sql_sb.AppendLine("   WHERE   SEQ_ID         = 4 ");
                  _sql_sb.AppendLine("     SET   @SeqId      = (SELECT SequenceValue = SEQ_NEXT_VALUE - 1 FROM SEQUENCES WHERE SEQ_ID  = 4) ");
                    _sql_sb.AppendLine("     SET   @SiteId     = (SELECT CAST(GP_KEY_VALUE AS INT) FROM  GENERAL_PARAMS WHERE   GP_GROUP_KEY = 'Site' AND  GP_SUBJECT_KEY = 'Identifier' AND ISNUMERIC(GP_KEY_VALUE) = 1) ");
                    _sql_sb.AppendLine("     IF ( ISNULL(@SiteId, 0) <= 0 ) SET @SiteId = 0 ");
                    _sql_sb.AppendLine("     IF ( @SiteId > 999 ) SET @SiteId = 0 ");
                    _sql_sb.AppendLine("     SET   @AccountId  = 1000000 + @SeqId * 1000 + @SiteId % 1000 ");
                  _sql_sb.AppendLine("     SET   @HolderName  =  concat('ACC: ', @AccountId) ");
                  _sql_sb.AppendLine("  INSERT   INTO ACCOUNTS(");
                  _sql_sb.AppendLine("           AC_ACCOUNT_ID");
                  _sql_sb.AppendLine("         , AC_HOLDER_NAME");
                  _sql_sb.AppendLine("         , AC_HOLDER_NAME3");
                  _sql_sb.AppendLine("         , AC_HOLDER_NAME1");
                    _sql_sb.AppendLine("         , AC_HOLDER_GENDER");
                    _sql_sb.AppendLine("         , AC_HOLDER_BIRTH_DATE");
                  _sql_sb.AppendLine("         , AC_CREATED");
                  _sql_sb.AppendLine("         , AC_HOLDER_LEVEL");
                  _sql_sb.AppendLine("         , AC_BLOCKED");
                  _sql_sb.AppendLine("         , AC_BLOCK_REASON");
                  _sql_sb.AppendLine("         , AC_BLOCK_DESCRIPTION ");
                  _sql_sb.AppendLine("         , AC_TRACK_DATA");
                  _sql_sb.AppendLine("         , AC_TYPE");
                    _sql_sb.AppendLine("         , AC_USER_TYPE ");
                    _sql_sb.AppendLine("         , AC_CARD_PAID ");
                    _sql_sb.AppendLine("         , AC_DEPOSIT ");
                    _sql_sb.AppendLine("         , AC_PIN ");
                  _sql_sb.AppendLine("  )VALUES(");
                  _sql_sb.AppendLine("           @AccountId");
                  _sql_sb.AppendLine("         , @HolderName ");
                  _sql_sb.AppendLine("         , @HolderName ");
                  _sql_sb.AppendLine("         , @HolderName ");
                    _sql_sb.AppendLine("         , 1 ");
                    _sql_sb.AppendLine("         , '1900-01-01' ");
                  _sql_sb.AppendLine("         , GETDATE()");
                  _sql_sb.AppendLine("         , 1 ");
                  _sql_sb.AppendLine("         , 0 ");
                  _sql_sb.AppendLine("         , @pBlockReason_None ");
                  _sql_sb.AppendLine("         , NULL ");
                  _sql_sb.AppendLine("         , dbo.TrackDataToInternal (@pExternalTrackdata) ");
                  _sql_sb.AppendLine("         , 2 ");
                  _sql_sb.AppendLine("         , @pAcUserType ");
                  _sql_sb.AppendLine("         , 1 ");
                  _sql_sb.AppendLine("         , 0 ");
                    _sql_sb.AppendLine("         , '0000' ");
                  _sql_sb.AppendLine("  ) ");
                  _sql_sb.AppendLine(" SELECT @AccountId ");
                  _sql_sb.AppendLine("      , @HolderName ");

                  using (SqlCommand _sql_cmd = new SqlCommand(_sql_sb.ToString(), SqlTrx.Connection, SqlTrx))
                  {
                    _sql_cmd.Parameters.Clear();
                    _sql_cmd.Parameters.Add("@pAcUserType", SqlDbType.Int).Value = (Int32)ACCOUNT_USER_TYPE.PERSONAL;
                    _sql_cmd.Parameters.Add("@pBlockReason_None", SqlDbType.Int).Value = (Int32)AccountBlockReason.NONE;
                    _sql_cmd.Parameters.Add("@pExternalTrackdata", SqlDbType.NVarChar).Value = ExternalTrackData;

                    using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
                    {
                      if (!_reader.Read())
                      {
                        Log.Warning("Account NOT inserted with CreateOnCardIn param enabled. ");

                        return WCP_CardTypes.CARD_TYPE_UNKNOWN;
                      }

                      LinkedId = _reader.GetInt64(0);
                      HolderName = _reader.IsDBNull(1) ? "" : _reader.GetString(1);

                      UserType = (Int32)ACCOUNT_USER_TYPE.PERSONAL;
                      BlockReason = (Int32)AccountBlockReason.NONE;

                    } // using (SqlDataReader

                  } // Using SqlCommand

                }
                catch (Exception _ex)
                {
                  Log.Exception(_ex);
                }
              }
            }
            }

          }
        }
        else
        {
          //
          // Found on 'CARDS'
          //
          String _cmd_txt;

          switch (_type)
          {
            case WCP_CardTypes.CARD_TYPE_PLAYER:
              _cmd_txt = "SELECT AC_HOLDER_NAME FROM ACCOUNTS WHERE AC_ACCOUNT_ID = @pId";
              break;

            case WCP_CardTypes.CARD_TYPE_MOBILE_BANK:
              _cmd_txt = "SELECT MB_HOLDER_NAME FROM MOBILE_BANKS WHERE MB_ACCOUNT_ID = @pId";
              break;

            case WCP_CardTypes.CARD_TYPE_CHANGE_STACKER:
            case WCP_CardTypes.CARD_TYPE_TECH:
              _cmd_txt = "SELECT GU_FULL_NAME FROM GUI_USERS WHERE GU_USER_ID = @pId";
              break;

            default:
              return _type;
          }

          using (SqlCommand _cmd = new SqlCommand(_cmd_txt, SqlTrx.Connection, SqlTrx))
          {
            _cmd.Parameters.Add("@pId", SqlDbType.BigInt).Value = LinkedId;

            using (SqlDataReader _reader = _cmd.ExecuteReader())
            {
              if (_reader.Read())
              {
                HolderName = _reader.IsDBNull(0) ? "" : _reader.GetString(0);
              }
              _reader.Close();
            }
          }
        }

        if (String.IsNullOrEmpty(HolderName))
        {
          HolderName = LinkedId.ToString("00000000");
          _is_anonymous = true;
        }


        if (_type != WCP_CardTypes.CARD_TYPE_PLAYER)
        {
          return _type;
        }

        if (Accounts.DoesActionRequirePlayerPinRequest(Accounts.PinRequestSource.EBOX, Accounts.PinRequestOperationType.PLAY, _is_anonymous))
        {
          return WCP_CardTypes.CARD_TYPE_PLAYER_PIN;
        }

        return WCP_CardTypes.CARD_TYPE_PLAYER;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        LinkedId = 0;
        HolderName = "";
        _is_anonymous = false;

        return WCP_CardTypes.CARD_TYPE_UNKNOWN;
      }
    } // CardType
  }
}
