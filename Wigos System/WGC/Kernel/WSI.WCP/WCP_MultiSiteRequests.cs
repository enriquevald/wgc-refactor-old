//------------------------------------------------------------------------------
// Copyright � 2013 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: MultiSite_MultiSiteRequests.cs
// 
//   DESCRIPTION: Ms Requests processes for WCP.
// 
//        AUTHOR: Alberto Cuesta
// 
// CREATION DATE: 30-APR-2013
// 
// REVISION HISTORY:
// 
// Date        Author    Description
// ----------- ------    ----------------------------------------------------------
// 30-APR-2013 ACC       First release.
// 10-JUL-2013 RCI       When there are DB errors, a RC of WCP_RC_DATABASE_OFFLINE must be returned to the terminal.
// 04-MAR-2015 JCA & AMF Fixed Bug WIG-2015: Space points sometimes not displayed
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using WSI.Common;
using WSI.WCP;
using System.Threading;

namespace WSI.WCP
{
  public static class WCP_MultiSiteRequests
  {
    private static Int64 m_sequence_id = 1;
    private const Int32 MAX_TIME_TO_NOTIFY = 5;

    //------------------------------------------------------------------------------
    // PURPOSE : Init multisite requests: start "get and send responses" thread 
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public static void Init()
    {
      Thread _thread;

      _thread = new Thread(MsRequests_GetAndSendResponsesThread);
      _thread.Name = "MsRequests_GetAndSendResponses";
      _thread.Start();

    } // Init

    //------------------------------------------------------------------------------
    // PURPOSE : Requests needed when start session trx is executed.
    //
    //  PARAMS :
    //      - INPUT :
    //          - TerminalId
    //          - AccountId
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public static void StartCardSession(Int32 TerminalId, Int64 AccountId, Int32 AccountLevel, SqlTransaction Trx)
    {
      Int32 _elp_mode;
      Int64 _unique_id;

      try
      {
        // Request for ELP01
        _elp_mode = GeneralParam.GetInt32("PlayerTracking.ExternalLoyaltyProgram", "Mode");
        if (_elp_mode == 1)
        {
          Elp01InputData _input_data;
          String _input_data_str;

          if (AccountLevel > 0)
          {
            // If IgnoreRequestPoints not AddRequest
            if (!GeneralParam.GetBoolean("ExternalLoyaltyProgram.Mode01", "IgnoreRequestPoints", true))
            {
              if (WGDB.GetMultiSiteStatus(Trx) == WGDB.MULTISITE_STATUS.MEMBER_CONNECTED)
              {
                _input_data = new Elp01InputData();
                _input_data.RequestType = ELP01_Requests.ELP01_REQUEST_QUERY_POINTS;
                _input_data.TerminalId = TerminalId;
                _input_data.AccountId = AccountId;
                _input_data_str = _input_data.ToXml();
                // Add Request "Consulta de Puntos" into database
                MultiSiteRequests.AddRequest(WWP_MultiSiteRequestType.MULTISITE_REQUEST_TYPE_ELP01, _input_data_str, 1, 0, out _unique_id, Trx);
              }
            }
          }
        }
      }

      catch (Exception ex)
      {
        Log.Error("ELP_StartCardSession failed. " + ex.Message);
      }

    } // SessionStart

    //------------------------------------------------------------------------------
    // PURPOSE : Process pending wcp commands
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    static private void MsRequests_GetAndSendResponsesThread()
    {
      Int32 _wait_hint;
      DataTable _dt_requests_responses;
      Elp01InputData _input_data;
      Elp01OutputData _output_data;
      DataRow _terminal;
      WCP_AccountManager.WCP_Account _account;
      WCP_Message _wcp_request;
      WCP_MsgExecuteCommand _request;
      ShowMessageParameters _cmd_show_message;
      Boolean _has_to_be_deleted;
      Boolean _is_principal;

      _wait_hint = 0;

      while (true)
      {
        try
        {
          Thread.Sleep(_wait_hint);
          _wait_hint = 1000;

          using (DB_TRX _db_trx = new DB_TRX())
          {
            // Read Responses into _dt_requests_responses
            if (!Elp01Requests.GetResponsesQueryPoints(out _dt_requests_responses, _db_trx.SqlTransaction))
            {
              Log.Warning("Function GetRequestsElp01QueryPoints failed...");
              _wait_hint = 5 * 60 * 1000; // 5 minute

              continue;
            }

            _is_principal = Services.IsPrincipal("WCP");

            foreach (DataRow _dr in _dt_requests_responses.Rows)
            {
              switch ((WWP_MultiSiteRequestType)(Int32)_dr["MR_REQUEST_TYPE"])
              {
                case WWP_MultiSiteRequestType.MULTISITE_REQUEST_TYPE_ELP01:
                  {
                    _has_to_be_deleted = false;

                    _input_data = new Elp01InputData();
                    _input_data.LoadXml((String)_dr["MR_INPUT_DATA"]);
                    if (_input_data.RequestType != ELP01_Requests.ELP01_REQUEST_QUERY_POINTS)
                    {
                      // This response NOT belongs to the WCP service ...
                      continue;
                    }

                    if ((WWP_MSRequestStatus)_dr["MR_STATUS"] != WWP_MSRequestStatus.MS_REQUEST_STATUS_OK)
                    {
                      _has_to_be_deleted = true;
                    }

                    if (_is_principal && IsRequestExpired(_dr))
                    {
                      _has_to_be_deleted = true;
                    }

                    if (!_has_to_be_deleted)
                    {
                      if (!WCP.Server.IsConnected(_input_data.TerminalId))
                      {
                        // Not connected or connected to the other WCP service.
                        continue;
                      }

                      _has_to_be_deleted = true;

                      _output_data = new Elp01OutputData();
                      _output_data.LoadXml((String)_dr["MR_OUTPUT_DATA"]);

                      //
                      // Try to Send message
                      //
                      if (_output_data.ResponseCodes == ELP01_ResponseCodes.ELP01_RESPONSE_CODE_OK
                          && _output_data.Points > 0)
                      {
                        _terminal = DbCache.Terminal(_input_data.TerminalId);
                        _account = WCP_AccountManager.Account(_input_data.AccountId, _db_trx.SqlTransaction);

                        if (ValidateMessageToSend(_input_data, _terminal, _account))
                        {
                          // Send Response to terminal (SasHost or EGM WIN)
                          _wcp_request = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_MsgExecuteCommand);
                          _request = (WCP_MsgExecuteCommand)_wcp_request.MsgContent;
                          _request.Command = WCP_CommandTypes.WCP_CMD_SHOW_MESSAGE;
                          _cmd_show_message = new ShowMessageParameters();
                          _cmd_show_message.Line1 = "";
                          _cmd_show_message.Line2 = Resource.String("STR_WCP_CMD_SHOW_MSG_POINTS", _output_data.Points.ToString(Gift.FORMAT_POINTS));
                          _cmd_show_message.NumTimes = 3;
                          _cmd_show_message.TimeBetweenMessages = 5000;
                          _cmd_show_message.TimeShowMessage = 3000;
                          _request.CommandParameters = _cmd_show_message.ToXml();

                          _wcp_request.MsgHeader.TerminalId = (String)_terminal["TE_EXTERNAL_ID"];
                          _wcp_request.MsgHeader.TerminalSessionId = 0;
                          _wcp_request.MsgHeader.SequenceId = Interlocked.Increment(ref m_sequence_id);

                          if (!WCP.Server.SendTo(_input_data.TerminalId, 0, _wcp_request.ToXml()))
                          {
                            // Try to send in next iteration.
                            _has_to_be_deleted = false;
                          }
                        }
                      }
                    }

                    if (_has_to_be_deleted)
                    {
                      if (!MultiSiteRequests.DeleteRequest((Int64)_dr["MR_UNIQUE_ID"]))
                      {
                        Log.Warning("Function MultiSiteRequests.DeleteRequest failed...");
                      }
                    }
                  }
                  break;

                default:
                  break;

              } // switch
            } // foreach
          }
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }

      } // while(True)

    } // MsRequests_GetAndSendResponsesThread

    //------------------------------------------------------------------------------
    // PURPOSE : Validate Message to send. If not valid, delete register
    //
    //  PARAMS :
    //      - INPUT : 
    //              - Elp01InputData
    //              - DataRow
    //              - WCP_AccountManager.WCP_Account
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //          True or False
    //   NOTES :
    //
    private static Boolean ValidateMessageToSend(Elp01InputData InputData, DataRow Terminal, WCP_AccountManager.WCP_Account Account)
    {
      // Can't send command to these terminal (p.ex: RETIRED).
      if (Terminal.IsNull("TE_EXTERNAL_ID"))
      {
        return false;
      }
      else if (Account == null || Account.id == 0)
      {
        return false;
      }
      else if (!WSI.Common.TITO.Utils.IsTitoMode() && Account.terminal_id != InputData.TerminalId)
      {
        return false;
      }

      return true;
    } // ValidateMessageToSend

    //------------------------------------------------------------------------------
    // PURPOSE : Check if the request is expired
    //
    //  PARAMS :
    //      - INPUT : DataRow
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //          True or False
    //   NOTES :
    //
    static private bool IsRequestExpired(DataRow Dr)
    {
      try
      {
        if (Dr["MR_STATUS_CHANGED"] != DBNull.Value)
        {
          return ((DateTime)Dr["MR_STATUS_CHANGED"]).AddSeconds(MAX_TIME_TO_NOTIFY) < WGDB.Now;
        }
      }
      catch
      {
        Log.Warning("MsRequests_GetAndSendResponsesThread.IsRequestExpired MR_STATUS_CHANGED is null");
      }

      return true;
    } // IsRequestExpired

  } // class WCP_MultiSiteRequests

} // namespace WSI.WCP
