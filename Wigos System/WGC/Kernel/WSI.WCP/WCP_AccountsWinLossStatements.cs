//------------------------------------------------------------------------------
// Copyright � 2015 Win Systems Ltd.
//------------------------------------------------------------------------------
//
//   MODULE NAME : WCP_AccountsWinLossStatements.cs
//
//   DESCRIPTION : Process Win/Loss Statements
//
// REVISION HISTORY :
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 22-APR-2015 AMF    First release.
// ----------- ------ ----------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Data;
using System.Data.SqlClient;
using WSI.Common;
using WSI.WCP;

namespace WSI.WCP
{
  public static class WCP_AccountsWinLossStatements
  {

    #region Members

    private const Double WORKING_TIME = (5 * 60 * 1000); // 5 minutes

    #endregion Members

    #region Public Functions

    //------------------------------------------------------------------------------
    // PURPOSE : Process Win Loss Statements
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    static public Boolean WinLossStatements()
    {
      DateTime _ini_process;
      DataTable _dt_statements;
      WinLossStatement.WIN_LOSS_STATEMENT _win_loss_statement;

      _ini_process = WGDB.Now;

      // Enabled functionality
      if (GeneralParam.GetInt32("WinLossStatement", "Enabled", 0) == 0)
      {
        return true;
      }

      while (_ini_process.AddMilliseconds(WORKING_TIME) > WGDB.Now)
      {
        try
        {
          // Get TOP 500 register            
          if (!GetWinLossStatements(out _dt_statements))
          {
            return false;
          }

          // Don't exist pending statements
          if (_dt_statements.Rows.Count == 0)
          {
            return true;
          }

          foreach (DataRow _statement in _dt_statements.Rows)
          {
            _win_loss_statement = new WinLossStatement.WIN_LOSS_STATEMENT();

            _win_loss_statement.AccountId = (Int64)_statement[0];
            _win_loss_statement.DateFrom = (DateTime)_statement[1];
            _win_loss_statement.DateTo = (DateTime)_statement[2];
            _win_loss_statement.RequestDate = (DateTime)_statement[3];
            _win_loss_statement.RequestUserId = (Int32)_statement[4];
            _win_loss_statement.Status = WinLossStatement.WinLossStatementStatus.InProgress;
            _win_loss_statement.StatusPrev = (WinLossStatement.WinLossStatementStatus)_statement[5];

            // Change status statement: In progress
            if (!WinLossStatement.UpdateWinLossStatementGenerate(_win_loss_statement))
            {
              continue;
            }

            //// Calculate Statement for all Accounts (DEPRECATED, for futures uses)
            //if (_win_loss_statement.AccountId == 0)
            //{
            //  // Insert register of win loss statements
            //  if (!ExploitRegister(_win_loss_statement))
            //  {
            //    continue;
            //  }

            //  // Change status statement: Ready
            //  _win_loss_statement.Status = WinLossStatement.WinLosssStatementStatus.Ready;
            //  _win_loss_statement.StatusPrev = WinLossStatement.WinLosssStatementStatus.InProgress;

            //  if (!WinLossStatement.UpdateWinLossStatementStatus(_win_loss_statement))
            //  {
            //    continue;
            //  }

            //  break;
            //}

            // Get Amounts
            if (!GetPlaySessionsAmounts(ref _win_loss_statement))
            {
              _win_loss_statement.Status = WinLossStatement.WinLossStatementStatus.Pending;
              _win_loss_statement.StatusPrev = WinLossStatement.WinLossStatementStatus.InProgress;
              WinLossStatement.UpdateWinLossStatementGenerate(_win_loss_statement);
              continue;
            }

            // Update statement
            if (!UpdateWinLossStatement(_win_loss_statement))
            {
              _win_loss_statement.Status = WinLossStatement.WinLossStatementStatus.Pending;
              _win_loss_statement.StatusPrev = WinLossStatement.WinLossStatementStatus.InProgress;
              WinLossStatement.UpdateWinLossStatementGenerate(_win_loss_statement);
              continue;
            }

            // Working time expired. No continue the process
            if (_ini_process.AddMilliseconds(WORKING_TIME) < WGDB.Now)
            {
              return true;
            }

          } // foreach
        } // try

        catch (Exception _ex)
        {
          Log.Exception(_ex);

          return false;
        }
      } // while

      return true;
    } // WinLossStatements

    #endregion Public Functions

    #region Private Functions

    //------------------------------------------------------------------------------
    // PURPOSE : Get the win loss statements
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //          - DataTable
    //
    // RETURNS :
    //      - True or False
    //
    //   NOTES :
    //      - true: Ok.
    //      - false: error.
    //
    static private Boolean GetWinLossStatements(out DataTable DtWinLossStatements)
    {

      StringBuilder _sb;

      DtWinLossStatements = new DataTable("WIN_LOSS_STATEMENTS");

      try
      {
        _sb = new StringBuilder();

        //GET REGISTERS Pending and Not generated correctly past 10 min.
        _sb.AppendLine("   SELECT   TOP 500 WLS_ACCOUNT_ID                                                  ");
        _sb.AppendLine("          , WLS_DATE_FROM                                                           ");
        _sb.AppendLine("          , WLS_DATE_TO                                                             ");
        _sb.AppendLine("          , WLS_REQUEST_DATE                                                        ");
        _sb.AppendLine("          , WLS_REQUEST_USER_ID                                                     ");
        _sb.AppendLine("          , WLS_STATUS                                                              ");
        _sb.AppendLine("     FROM   WIN_LOSS_STATEMENTS                                                     ");
        _sb.AppendLine("    WHERE   WLS_STATUS = @pStatus                                                   ");
        _sb.AppendLine("       OR  (WLS_STATUS = 1 AND DATEDIFF(MINUTE,WLS_GENERATION_DATE,GETDATE()) > 10) ");
        _sb.AppendLine(" ORDER BY   WLS_REQUEST_DATE ASC                                                    ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = WinLossStatement.WinLossStatementStatus.Pending;

            using (SqlDataAdapter _da = new SqlDataAdapter(_cmd))
            {
              _da.Fill(DtWinLossStatements);
            }
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("Error on WCP_AccountsWinLossStatements.GetWinLossStatements");
        Log.Exception(_ex);
      }

      return false;
    } // GetWinLossStatements

    //------------------------------------------------------------------------------
    // PURPOSE : Generate win loss statements for all accounts
    //
    //  PARAMS :
    //      - INPUT :
    //          - Statement
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True or False
    //
    //   NOTES :
    //      - true: Ok.
    //      - false: error.
    //
    static private Boolean ExploitRegister(WinLossStatement.WIN_LOSS_STATEMENT Statement)
    {

      StringBuilder _sb;

      try
      {

        _sb = new StringBuilder();

        _sb.AppendLine(" INSERT INTO   WIN_LOSS_STATEMENTS                                                                                                  ");
        _sb.AppendLine("      SELECT   DISTINCT PS_ACCOUNT_ID                                                                                               ");
        _sb.AppendLine("             , @pDateFrom                                                                                                           ");
        _sb.AppendLine("             , @pDateTo                                                                                                             ");
        _sb.AppendLine("             , @pStatus                                                                                                             ");
        _sb.AppendLine("             , @pRequestDate                                                                                                        ");
        _sb.AppendLine("             , NULL                                                                                                                 ");
        _sb.AppendLine("             , @pRequestUserId                                                                                                      ");
        _sb.AppendLine("             , NULL                                                                                                                 ");
        _sb.AppendLine("             , NULL                                                                                                                 ");
        _sb.AppendLine("             , 0                                                                                                                    ");
        _sb.AppendLine("             , 0                                                                                                                    ");
        _sb.AppendLine("             , 0                                                                                                                    ");
        _sb.AppendLine("             , NULL                                                                                                                 ");
        _sb.AppendLine("        FROM   PLAY_SESSIONS WITH(INDEX(IX_ps_finished_status))                                                                     ");
        _sb.AppendLine("       WHERE   PS_FINISHED  >= @pDateFrom                                                                                           ");
        _sb.AppendLine("         AND   PS_FINISHED  <  @pDateTo                                                                                             ");
        _sb.AppendLine("         AND   PS_TYPE IN (@pWin,@pAlesis)                                                                                          ");
        _sb.AppendLine("         AND   PS_STATUS IN (@pClosed,@pAbandoned,@pManualClosed,@pHandpayPayment,@pHandpayCancelPayment,@pDrawWinner,@pDrawLoser)  ");
        _sb.AppendLine("         AND   PS_ACCOUNT_ID NOT IN (SELECT   WLS_ACCOUNT_ID                                                                        ");
        _sb.AppendLine("                                       FROM   WIN_LOSS_STATEMENTS WITH(INDEX(IX_wls_date_from_date_to))                             ");
        _sb.AppendLine("                                      WHERE   WLS_DATE_FROM = @pDateFrom                                                            ");
        _sb.AppendLine("                                        AND   WLS_DATE_TO   = @pDateTo  )                                                           ");
        _sb.AppendLine("                                                                                                                                    ");
        _sb.AppendLine("      UPDATE   WIN_LOSS_STATEMENTS                                                                                                  ");
        _sb.AppendLine("         SET   WLS_PRINT = 0                                                                                                        ");
        _sb.AppendLine("       WHERE   WLS_DATE_FROM = @pDateFrom                                                                                           ");
        _sb.AppendLine("         AND   WLS_DATE_TO   = @pDateTo                                                                                             ");


        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pDateFrom", SqlDbType.DateTime).Value = Statement.DateFrom;
            _cmd.Parameters.Add("@pDateTo", SqlDbType.DateTime).Value = Statement.DateTo;
            _cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = WinLossStatement.WinLossStatementStatus.Pending;
            _cmd.Parameters.Add("@pRequestDate", SqlDbType.DateTime).Value = Statement.RequestDate;
            _cmd.Parameters.Add("@pRequestUserId", SqlDbType.Int).Value = Statement.RequestUserId;
            _cmd.Parameters.Add("@pClosed", SqlDbType.Int).Value = PlaySessionStatus.Closed;
            _cmd.Parameters.Add("@pAbandoned", SqlDbType.Int).Value = PlaySessionStatus.Abandoned;
            _cmd.Parameters.Add("@pManualClosed", SqlDbType.Int).Value = PlaySessionStatus.ManualClosed;
            _cmd.Parameters.Add("@pHandpayPayment", SqlDbType.Int).Value = PlaySessionStatus.HandpayPayment;
            _cmd.Parameters.Add("@pHandpayCancelPayment", SqlDbType.Int).Value = PlaySessionStatus.HandpayCancelPayment;
            _cmd.Parameters.Add("@pDrawWinner", SqlDbType.Int).Value = PlaySessionStatus.DrawWinner;
            _cmd.Parameters.Add("@pDrawLoser", SqlDbType.Int).Value = PlaySessionStatus.DrawLoser;
            _cmd.Parameters.Add("@pWin", SqlDbType.BigInt).Value = PlaySessionType.WIN;
            _cmd.Parameters.Add("@pAlesis", SqlDbType.BigInt).Value = PlaySessionType.ALESIS;

            _cmd.ExecuteNonQuery();
            _db_trx.Commit();

            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error("Error on WCP_AccountsWinLossStatements.ExploitRegister Period: " + Statement.DateFrom.ToString() + " - " + Statement.DateTo.ToString());
        Log.Exception(_ex);
      }

      return false;
    } // UpdateWinLossStatementStatus

    //------------------------------------------------------------------------------
    // PURPOSE : Get played and won amount from play_sessions
    //
    //  PARAMS :
    //      - INPUT :
    //          - Statement
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True or False
    //
    //   NOTES :
    //      - true: Ok.
    //      - false: error.
    //
    static private Boolean GetPlaySessionsAmounts(ref WinLossStatement.WIN_LOSS_STATEMENT Statement)
    {

      StringBuilder _sb;
      DataTable _dt_amounts;

      _dt_amounts = new DataTable("PLAY_SESSIONS");

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine("SELECT   *                                                                                      ");
        _sb.AppendLine("  FROM  (                                                                                       ");
        _sb.AppendLine("          SELECT   ISNULL(SUM(PS_PLAYED_AMOUNT),0) AS PLAYED_AMOUNT                             ");
        _sb.AppendLine("                 , ISNULL(SUM(PS_WON_AMOUNT),0)    AS WON_AMOUNT                                ");
        _sb.AppendLine("                 , ''                              AS JACKPOT_AMOUNT                            ");
        _sb.AppendLine("            FROM   PLAY_SESSIONS WITH(INDEX(IX_ps_account_id_finished))                         ");
        _sb.AppendLine("           WHERE	 PS_ACCOUNT_ID = @pAccountId                                                  ");
        _sb.AppendLine("             AND	 PS_TYPE IN (@pWin,@pAlesis)                                                  ");
        _sb.AppendLine("             AND   PS_FINISHED  >= @pDateFrom                                                   ");
        _sb.AppendLine("             AND   PS_FINISHED  <  @pDateTo                                                     ");
        _sb.AppendLine("             AND   PS_STATUS IN (                                                               ");
        _sb.AppendLine("                                  @pClosed,@pAbandoned,@pManualClosed,@pHandpayPayment          ");
        _sb.AppendLine("                                 ,@pHandpayCancelPayment,@pDrawWinner, @pDrawLoser              ");
        _sb.AppendLine("                                 )                                                              ");
        _sb.AppendLine(" UNION                                                                                          ");
        _sb.AppendLine("          SELECT   ''                       AS PLAYED_AMOUNT                                    ");
        _sb.AppendLine("                 , ''                       AS WON_AMOUNT                                       ");
        _sb.AppendLine("                 , ISNULL(SUM(HP_AMOUNT),0) AS JACKPOT_AMOUNT                                   ");
        _sb.AppendLine("            FROM   PLAY_SESSIONS WITH(INDEX(IX_ps_account_id_finished))                         ");
        _sb.AppendLine("      INNER JOIN   HANDPAYS ON PS_PLAY_SESSION_ID = HP_PLAY_SESSION_ID                          ");
        _sb.AppendLine("           WHERE	 PS_ACCOUNT_ID = @pAccountId                                                  ");
        _sb.AppendLine("             AND   HP_TYPE IN (@pJackpot,@pSpecial_Progressive,@pManual_Jackpot,@pSite_Jackpot) ");
        _sb.AppendLine("             AND   PS_FINISHED  >= @pDateFrom                                                   ");
        _sb.AppendLine("             AND   PS_FINISHED  <  @pDateTo                                                     ");
        _sb.AppendLine("         ) AS X");
        //_sb.AppendLine("          SELECT   ''                       AS PLAYED_AMOUNT                                    ");
        //_sb.AppendLine("                 , ''                       AS WON_AMOUNT                                       ");
        //_sb.AppendLine("                 , ISNULL(SUM(HP_AMOUNT),0) AS JACKPOT_AMOUNT                                   ");
        //_sb.AppendLine("            FROM   HANDPAYS                                                                     ");
        //_sb.AppendLine("      INNER JOIN   PLAY_SESSIONS ON HP_PLAY_SESSION_ID = PS_PLAY_SESSION_ID                     ");
        //_sb.AppendLine("           WHERE   PS_ACCOUNT_ID = @pAccountId                                                  ");
        //_sb.AppendLine("             AND   HP_TYPE IN (@pJackpot,@pSpecial_Progressive,@pManual_Jackpot,@pSite_Jackpot) ");
        

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = Statement.AccountId;

            _cmd.Parameters.Add("@pJackpot", SqlDbType.BigInt).Value = HANDPAY_TYPE.JACKPOT;
            _cmd.Parameters.Add("@pSpecial_Progressive", SqlDbType.BigInt).Value = HANDPAY_TYPE.SPECIAL_PROGRESSIVE;
            _cmd.Parameters.Add("@pManual_Jackpot", SqlDbType.BigInt).Value = HANDPAY_TYPE.MANUAL_JACKPOT;
            _cmd.Parameters.Add("@pSite_Jackpot", SqlDbType.BigInt).Value = HANDPAY_TYPE.SITE_JACKPOT;

            _cmd.Parameters.Add("@pWin", SqlDbType.BigInt).Value = PlaySessionType.WIN;
            _cmd.Parameters.Add("@pAlesis", SqlDbType.BigInt).Value = PlaySessionType.ALESIS;

            _cmd.Parameters.Add("@pDateFrom", SqlDbType.DateTime).Value = Statement.DateFrom;
            _cmd.Parameters.Add("@pDateTo", SqlDbType.DateTime).Value = Statement.DateTo;

            _cmd.Parameters.Add("@pClosed", SqlDbType.Int).Value = PlaySessionStatus.Closed;
            _cmd.Parameters.Add("@pAbandoned", SqlDbType.Int).Value = PlaySessionStatus.Abandoned;
            _cmd.Parameters.Add("@pManualClosed", SqlDbType.Int).Value = PlaySessionStatus.ManualClosed;
            _cmd.Parameters.Add("@pHandpayPayment", SqlDbType.Int).Value = PlaySessionStatus.HandpayPayment;
            _cmd.Parameters.Add("@pHandpayCancelPayment", SqlDbType.Int).Value = PlaySessionStatus.HandpayCancelPayment;
            _cmd.Parameters.Add("@pDrawWinner", SqlDbType.Int).Value = PlaySessionStatus.DrawWinner;
            _cmd.Parameters.Add("@pDrawLoser", SqlDbType.Int).Value = PlaySessionStatus.DrawLoser;

            using (SqlDataAdapter _da = new SqlDataAdapter(_cmd))
            {
              _da.Fill(_dt_amounts);

              if (_dt_amounts.Rows.Count > 1)
              {
                Statement.PlayedAmount = (Currency)(Decimal)_dt_amounts.Rows[1][0];
                Statement.WonAmount = (Currency)(Decimal)_dt_amounts.Rows[1][1];
                Statement.JackpotAmount = (Currency)(Decimal)_dt_amounts.Rows[0][2];
              }
            }
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("Error on WCP_AccountsWinLossStatements.GetPlaySessionsAmounts AccountId: " + Statement.AccountId.ToString() +
                  " Period: " + Statement.DateFrom.ToString() + " - " + Statement.DateTo.ToString());
        Log.Exception(_ex);
      }

      return false;
    } // GetPlaySessionsAmounts

    //------------------------------------------------------------------------------
    // PURPOSE : Update win loss statement
    //
    //  PARAMS :
    //      - INPUT :
    //          - Statement
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True or False
    //
    //   NOTES :
    //      - true: Ok.
    //      - false: error.
    //
    static private Boolean UpdateWinLossStatement(WinLossStatement.WIN_LOSS_STATEMENT Statement)
    {

      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine("   UPDATE   WIN_LOSS_STATEMENTS                    ");
        _sb.AppendLine("      SET   WLS_STATUS          = @pStatus         ");
        _sb.AppendLine("          , WLS_PLAYED_AMOUNT   = @pPlayedAmount   ");
        _sb.AppendLine("          , WLS_WON_AMOUNT      = @pWonAmount      ");
        _sb.AppendLine("          , WLS_JACKPOT_AMOUNT  = @pJackpotAmount  ");
        _sb.AppendLine("          , WLS_GENERATION_DATE = @pGenerationDate ");
        _sb.AppendLine("    WHERE   WLS_ACCOUNT_ID = @pAccountId           ");
        _sb.AppendLine("      AND   WLS_DATE_FROM  = @pDateFrom            ");
        _sb.AppendLine("      AND   WLS_DATE_TO    = @pDateTo              ");
        _sb.AppendLine("      AND   WLS_STATUS     = @pPrevStatus          ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = WinLossStatement.WinLossStatementStatus.Ready;
            _cmd.Parameters.Add("@pPrevStatus", SqlDbType.Int).Value = WinLossStatement.WinLossStatementStatus.InProgress;
            _cmd.Parameters.Add("@pPlayedAmount", SqlDbType.Money).Value = Statement.PlayedAmount.SqlMoney;
            _cmd.Parameters.Add("@pWonAmount", SqlDbType.Money).Value = Statement.WonAmount.SqlMoney;
            _cmd.Parameters.Add("@pJackpotAmount", SqlDbType.Money).Value = Statement.JackpotAmount.SqlMoney;
            _cmd.Parameters.Add("@pGenerationDate", SqlDbType.DateTime).Value = WGDB.Now;
            _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = Statement.AccountId;
            _cmd.Parameters.Add("@pDateFrom", SqlDbType.DateTime).Value = Statement.DateFrom;
            _cmd.Parameters.Add("@pDateTo", SqlDbType.DateTime).Value = Statement.DateTo;

            if (_cmd.ExecuteNonQuery() == 1)
            {
              _db_trx.Commit();

              return true;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error("Error on WCP_AccountsWinLossStatements.UpdateWinLossStatement AccountId: " + Statement.AccountId.ToString() +
                  " Period: " + Statement.DateFrom.ToString() + " - " + Statement.DateTo.ToString());
        Log.Exception(_ex);
      }

      return false;
    } // UpdateWinLossStatement

    #endregion Private Functions
  }
}
