//
// AJQ 25-OCT-2011, Avoid to register new machines with the "WSI" provider. New machines will be regitered as "UNKOWN".
//
// MPO 26-OCT-2011, Set the name of a mobile bank
// MBF 15-NOV-2011, Prevent exception when reading a retired terminal
// RCI 24-NOV-2011, Method DB_CheckAccountPIN() moved to WSI.Common.Acounts.
// ACC 18-JAN-2012, Update spent used per provider in the play sessions
// MBF 26-JAN-2012, Implemented Add Credit logic and money event.
// RCI 22-FEB-2012, Split NR1 and NR2 in two fields when creating account operations: routine DB_InsertOperation().
// JMM 29-FEB-2012, On MB CashIn, insert the related movements to NR1 & NR2 into CASHIER_MOVEMENTS
// SSC 29-FEB-2012, Routine DB_MobileBankAccountTransfer() modified, common part moved to WCP_TranferAmounts.
// SSC 11-JUN-2012: Moved GetOrOpenSystemCashierSession(), DB_MobileBankInsertCashierMovement() and DB_MobileBankGetCashierInfo() methods to WSI.Common.Cashier
// XIT 21-JUN 2012, Added GetRejectedCurrencies,DB_GetCurrencies
// RCI 10-JUL-2013, When there are DB errors, a RC of WCP_RC_DATABASE_OFFLINE must be returned to the terminal.
// LEM 22-OCT-2013, Save TrackData to account movement
// JRM 26-OCT-2013, Added new method to get money collection id from terminal id 
// NMR 29-OCT-2013, Changes in protocol for parameters send
// NMR 06-NOV-2013, Removed Asset Number and Machine Validation
// NMR 08-NOV-2013, Improvement in query execution
// JBP 21-NOV-2013, Updated general params used in GetEgmParams.
// SMN 22-NOV-2013, Call SP CashierMovementsHistory when insert a new MB_MOVEMENTS
// NMR 26-NOV-2013, Fixed error when searching TITO virtual account
// NMR 27-NOV-2013, Fixed error when loading TITO params
// NMR 28-NOV-2013, Clear some account columns when is TITO mode
// NMR 29-NOV-2013, Method translated to MoneyCollection class
// JCOR 05-DEC-2013  Added private functions ExistsHandpays and DB_InsertHandpay.
// LEM 10-JAN-2014  Fixed error (GetEgmParams) getting default values of AllowTicketIn, AllowCashableTicketOut, AllowPromotionalTicketOut
// RCI & RMS 10-FEB-2014   The historicize by hour will be made in a separate thread
// AMF 04-APR-2014  Added new information from Long Poll 1B
// 12-APR-2014 DHA  Added 'Insert' on Terminal_status for WASS Mode
// 03-JUL-2014 JMM  Added first name as parameter on DB_ReadAccount to allow display only first name on EGM Win and LKT SAS Host
// 30-SEP-2014 LEM  Fixed Bug WIG-1246: Allowed cash in without cage activated
// 09-MAR-2015 FJC  Fixed Bug: WIG-2144
// 28-APR-2015 YNM  Fixed Bug WIG-2252: System allows recharged with closed session
// 06-JAN-2016 SGB  Backlog Item 7910: Change column AC_POINTS to bucket 1.
// 21-MAR-2016 DHA  Product Backlog Item 10051: add CageCurrencyType to historification
// 17-FEB-2017 FAV  PBI 24761:JOP09: Terminal reserved, check time an terminal
// 01-AUG-2017 ATB  PBI 28982: EGM Reserve - Phase 1 - Set free on GUI
// 17-OCT-2017 ETL  Fixed Bug 30289: WIGOS-5736 [Ticket #9499] Garcia River - Alarma - Terminal configuration Discrepancy
// 17-NOV-2017 DPC  PBI 30845:[WIGOS-4375]: Associate TITO handpays to the account
// 07-FEB-2018 JML  Fixed Bug 31432:WIGOS-7898 [Ticket #12012] Incremento en Payout Teórico por Salto de Contadores
// 23-MAY-2018 GDA  Bug 32752 -- WIGOS-11644---[Ticket #14222] Slow performance on credit add in operation
// -------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using WSI.Common;
using System.Xml;
using System.Collections;
using WSI.Common.ReservedTerminal;

namespace WSI.WCP
{
  #region Enums

  public enum WCP_SessionStatus
  {
    Opened = 0
  ,
    Closed = 1
  ,
    Abandoned = 2
  ,
    Timeout = 3
  , Disconnected = 4
  }

  #endregion Enums

  public class WCP_BusinessLogic
  {
    static CashlessMode cashless_mode = CashlessMode.NONE;

    #region CashlessMode

    //------------------------------------------------------------------------------
    // PURPOSE: Cashless mode property
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    //        
    //   NOTES :
    //
    static public CashlessMode CashlessMode
    {
      get
      {
        SqlConnection sql_conn;
        SqlCommand sql_command;
        SqlDataReader sql_reader;
        String sql_str;

        if (cashless_mode != CashlessMode.NONE)
        {
          return cashless_mode;
        }

        sql_conn = null;
        sql_reader = null;

        // Read from General Params
        try
        {
          sql_conn = WGDB.Connection();

          //
          // Read Location Repository values
          //
          sql_str = " SELECT GP_KEY_VALUE                   " +
                    "   FROM GENERAL_PARAMS                 " +
                    "  WHERE GP_GROUP_KEY    = 'Cashless'   " +
                    "    AND GP_SUBJECT_KEY  = 'Server'     ";

          sql_command = new SqlCommand(sql_str);
          sql_command.Connection = sql_conn;

          sql_reader = sql_command.ExecuteReader();

          while (sql_reader.Read())
          {
            if (!sql_reader.IsDBNull(0))
            {
              if ((sql_reader.GetString(0) == "3GS") || (sql_reader.GetString(0) == "ALESIS"))
              {
                cashless_mode = CashlessMode.ALESIS;
              }
              else if (sql_reader.GetString(0) == "WIN")
              {
                cashless_mode = CashlessMode.WIN;
              }
            }
          }
          sql_reader.Close();
        }
        catch (Exception ex)
        {
          Log.Exception(ex);
        }
        finally
        {
          if (sql_reader != null)
          {
            sql_reader.Close();
            sql_reader.Dispose();
            sql_reader = null;
          }

          if (sql_conn != null)
          {
            sql_conn.Close();
            sql_conn = null;
          }
        }

        return cashless_mode;
      }
    } // CasshlessMode

    #endregion

    #region EnrollAndSession

    public static bool DB_InsertTerminalPending(String ExternalId, String ProviderId, TerminalTypes TerminalType, SqlTransaction Trx)
    {
      SqlConnection _sql_conn;
      SqlCommand sql_command;
      String sql_str;
      int affected_rows;
      int count;

      // Check if terminal existis in TERMINALS_PENDING

      sql_str = " SELECT COUNT(*) FROM TERMINALS_PENDING WHERE TP_SERIAL_NUMBER = @p1 AND TP_VENDOR_ID = @p2 ";

      sql_command = new SqlCommand(sql_str);
      sql_command.Connection = Trx.Connection;
      sql_command.Transaction = Trx;

      sql_command.Parameters.Add("@p1", SqlDbType.NVarChar, 40, "TP_SERIAL_NUMBER").Value = ExternalId;
      sql_command.Parameters.Add("@p2", SqlDbType.NVarChar, 50, "TP_VENDOR_ID").Value = ProviderId;

      try
      {
        count = (int)sql_command.ExecuteScalar();

        if (count > 0)
        {
          return true;
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
        Log.Warning("Exception throw: TERMINALS_PENDING read error");
        return false;
      }

      // Insert terminal in TERMINALS_PENDING

      _sql_conn = null;

      try
      {
        // Get Sql Connection 
        _sql_conn = WGDB.Connection();

        sql_str = "INSERT INTO   TERMINALS_PENDING (TP_SOURCE " +
                  "                               , TP_SERIAL_NUMBER " +
                  "                               , TP_VENDOR_ID " +
                  "                               , TP_TERMINAL_TYPE " +
                  "                                ) " +
                  "                         VALUES (@pSource " +
                  "                               , @pSerialNumber " +
                  "                               , @pVendorId " +
                  "                               , @pTerminalType " +
                  "                                ) ";

        sql_command = new SqlCommand(sql_str);
        sql_command.Connection = _sql_conn;

        sql_command.Parameters.Add("@pSource", SqlDbType.Int).Value = 1;
        sql_command.Parameters.Add("@pSerialNumber", SqlDbType.NVarChar).Value = ExternalId;
        sql_command.Parameters.Add("@pVendorId", SqlDbType.NVarChar).Value = ProviderId;
        sql_command.Parameters.Add("@pTerminalType", SqlDbType.SmallInt).Value = (Int16)TerminalType;

        affected_rows = sql_command.ExecuteNonQuery();

        return (affected_rows > 0);
      }
      catch
      {
        return false;
      }
      finally
      {
        // Close connection
        if (_sql_conn != null)
        {
          _sql_conn.Close();
          _sql_conn = null;
        }
      }

    } // DB_InsertTerminalPending


    public static bool DB_InsertTerminal(String ExternalServerId, String ExternalTerminalId, WCP_TerminalType Type, String ProviderId, TerminalTypes TerminalType, SqlTransaction Trx)
    {
      SqlCommand sql_command;
      String sql_str;
      String external_id;
      int affected_rows;
      int server_id;
      SqlDataReader reader;
      Int32 terminal_id;
      SqlParameter parameter;
      SYSTEM_MODE system_mode;

      server_id = -1;
      external_id = ExternalTerminalId;
      terminal_id = 0;
      parameter = new SqlParameter();
      system_mode = Misc.SystemMode();

      if (Type == WCP_TerminalType.IntermediateServer)
      {
        external_id = ExternalServerId;
      }
      else
      {
        if (ExternalServerId != "")
        {
          // 
          // Get Terminal Id
          //
          sql_str = "SELECT TE_TERMINAL_ID FROM TERMINALS WHERE TE_TYPE = @p1 AND TE_EXTERNAL_ID = @p2";

          sql_command = new SqlCommand(sql_str);
          sql_command.Connection = Trx.Connection;
          sql_command.Transaction = Trx;

          sql_command.Parameters.Add("@p1", SqlDbType.Int, 4, "TE_TYPE").Value = (int)WCP_TerminalType.IntermediateServer;
          sql_command.Parameters.Add("@p2", SqlDbType.NVarChar, 40, "TE_EXTERNAL_ID").Value = ExternalServerId;

          reader = null;

          try
          {
            reader = sql_command.ExecuteReader();
            if (reader.Read())
            {
              server_id = (Int32)reader[0];
            }
          }
          catch (Exception ex)
          {
            Log.Exception(ex);
            Log.Warning("Exception throw: Server doesn't exist" + ExternalServerId.ToString());
            return false;
          }
          finally
          {
            if (reader != null)
            {
              reader.Close();
              reader.Dispose();
              reader = null;
            }
          }
        }
      }

      // 
      // Get Terminal Id
      //

      StringBuilder _str_bld;

      _str_bld = new StringBuilder();

      _str_bld.AppendLine("INSERT   INTO TERMINALS");
      _str_bld.AppendLine("       ( TE_TYPE ");
      _str_bld.AppendLine("       , TE_BASE_NAME ");
      _str_bld.AppendLine("       , TE_EXTERNAL_ID ");
      _str_bld.AppendLine("       , TE_BLOCKED ");
      _str_bld.AppendLine("       , TE_ACTIVE ");
      _str_bld.AppendLine("       , TE_SERVER_ID ");
      _str_bld.AppendLine("       , TE_PROVIDER_ID ");
      _str_bld.AppendLine("       , TE_TERMINAL_TYPE ");
      _str_bld.AppendLine("       , TE_MASTER_ID ");
      _str_bld.AppendLine("       , TE_CHANGE_ID ");
      _str_bld.AppendLine("       , TE_ISO_CODE ");
      _str_bld.AppendLine("       , TE_CREATION_DATE) ");
      _str_bld.AppendLine("VALUES");
      _str_bld.AppendLine("      ( @pType ");
      _str_bld.AppendLine("      , @pBaseName ");
      _str_bld.AppendLine("      , @pExternalId ");
      _str_bld.AppendLine("      , @pBlocked ");
      _str_bld.AppendLine("      , @pActive ");
      _str_bld.AppendLine("      , @pServerId ");
      _str_bld.AppendLine("      , @pProviderId ");
      _str_bld.AppendLine("      , @pTerminalType ");
      _str_bld.AppendLine("      , @pMasterId ");
      _str_bld.AppendLine("      , @pChangeId ");
      _str_bld.AppendLine("      , @pIsoCode  ");
      _str_bld.AppendLine("      , @pDateTimeNow) ");
      _str_bld.AppendLine("  SET @pTerminalId = SCOPE_IDENTITY() ");
      _str_bld.AppendLine(" ");
      _str_bld.AppendLine("UPDATE   TERMINALS ");
      _str_bld.AppendLine("   SET   TE_MASTER_ID = TE_TERMINAL_ID ");
      _str_bld.AppendLine(" WHERE   TE_TERMINAL_ID = @pTerminalId ");

      sql_command = new SqlCommand(_str_bld.ToString());
      sql_command.Connection = Trx.Connection;
      sql_command.Transaction = Trx;

      sql_command.Parameters.Add("@pType", SqlDbType.Int, 4, "TE_TYPE").Value = Type;
      sql_command.Parameters.Add("@pBaseName", SqlDbType.NVarChar, 40, "TE_BASE_NAME").Value = external_id;
      sql_command.Parameters.Add("@pExternalId", SqlDbType.NVarChar, 40, "TE_EXTERNAL_ID").Value = external_id;
      sql_command.Parameters.Add("@pBlocked", SqlDbType.Bit, 1, "TE_BLOCKED").Value = 0;
      sql_command.Parameters.Add("@pActive", SqlDbType.Bit, 1, "TE_ACTIVE").Value = 1;
      sql_command.Parameters.Add("@pIsoCode", SqlDbType.NVarChar, 3, "TE_ISO_CODE").Value = CurrencyExchange.GetNationalCurrency();
      if (server_id >= 0)
      {
        sql_command.Parameters.Add("@pServerId", SqlDbType.Int, 4, "TE_SERVER_ID").Value = server_id;
      }
      else
      {
        sql_command.Parameters.Add("@pServerId", SqlDbType.Int, 4, "TE_SERVER_ID").Value = DBNull.Value;
      }
      sql_command.Parameters.Add("@pProviderId", SqlDbType.NVarChar, 50, "TE_PROVIDER_ID").Value = ProviderId;
      sql_command.Parameters.Add("@pTerminalType", SqlDbType.SmallInt, 4, "TE_TERMINAL_TYPE").Value = (Int16)TerminalType;
      sql_command.Parameters.Add("@pMasterId", SqlDbType.Int, 4, "TE_MASTER_ID").Value = 0;
      sql_command.Parameters.Add("@pChangeId", SqlDbType.Int, 4, "TE_CHANGE_ID").Value = 0;
      sql_command.Parameters.Add("@pDateTimeNow", SqlDbType.DateTime).Value = WGDB.Now;

      parameter = sql_command.Parameters.Add("@pTerminalId", SqlDbType.Int);
      parameter.Direction = ParameterDirection.Output;

      try
      {
        affected_rows = sql_command.ExecuteNonQuery();
        if (affected_rows > 0)
        {
          if (system_mode == SYSTEM_MODE.WASS)
          {
            terminal_id = (Int32)parameter.Value;
            if (!TerminalStatusFlags.DB_SetValue(TerminalStatusFlags.BITMASK_TYPE.Machine_status, terminal_id, (Int32)TerminalStatusFlags.MACHINE_FLAGS.BLOCKED_BY_SIGNATURE, Trx))
            {
              return false;
            }
          }
          return true;
        }
        else
        {
          return false;
        }
      }
      catch
      {
        return false;
      }
    }

    /// <summary>
    /// Get Database enroll data
    /// </summary>
    /// <param name="ExternalTerminalId"></param>
    /// <param name="Type"></param>
    /// <param name="TerminalId"></param>
    /// <param name="SessionId"></param>
    /// <param name="SequenceId"></param>
    /// <param name="TransactionId"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    public static Boolean DB_GetEnrollData(String ExternalServerId,
                                           String ExternalTerminalId,
                                           String ReceivedProviderId,
                                           TerminalTypes ReceivedTerminalType,
                                           WCP_TerminalType Type,
                                           out Int32 TerminalId,
                                           out Int64 SessionId,
                                           out Int64 SequenceId,
                                           out Int64 TransactionId,
                                           out Boolean EnrolledTerminal,
                                           out String ProviderId,
                                           out String Name,
                                           SqlTransaction Trx)
    {
      SqlCommand _sql_cmd;
      SqlDataReader _sql_reader;
      SqlParameter p;
      String _cmd_txt;
      String _terminal_ext_id;
      String _server_ext_id;
      String _config_svr_ext_id;
      Int16 _sql_value;
      Object _obj;
      bool _exists;
      TerminalStatus _status;
      Currency _sas_account_denom;

      // Initialize output parameters
      TerminalId = 0;
      SessionId = 0;
      SequenceId = 0;
      TransactionId = 0;
      ProviderId = "";
      Name = "";
      EnrolledTerminal = true;


      _sql_value = 0;


      // AJQ 25-OCT-2011, Avoid to register new machines with the "WSI" provider.
      //                  All new machines will be regitered as "UNKOWN" and the technician should manually configure it.
      switch (ReceivedTerminalType)
      {
        case TerminalTypes.SAS_HOST:
        case TerminalTypes.WIN:
          ReceivedProviderId = "UNKNOWN";
          break;

        default:
          // Do nothing
          break;
      } // switch (ReceivedTerminalType)

      if (ExternalServerId.Length > 40)
      {
        ExternalServerId = ExternalServerId.Substring(0, 40);
      }
      if (ExternalTerminalId.Length > 40)
      {
        ExternalTerminalId = ExternalTerminalId.Substring(0, 40);
      }
      if (ReceivedProviderId.Length > 40)
      {
        ReceivedProviderId = ReceivedProviderId.Substring(0, 40);
      }


      // 
      // Insert Terminal if it doesn't exists.
      //
      _terminal_ext_id = ExternalTerminalId;
      _server_ext_id = ExternalServerId;
      if (Type == WCP_TerminalType.IntermediateServer)
      {
        if (ExternalServerId == "")
        {
          throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR, "ServerId should not be empty.");
        }
        if (ExternalTerminalId != "")
        {
          throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR, "TerminalId must be empty.");
        }
        _terminal_ext_id = ExternalServerId;
        _server_ext_id = "";
      }
      else
      {
        if (ExternalTerminalId == "")
        {
          throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR, "TerminalId should not be empty.");
        }
      }

      _exists = Misc.GetTerminalInfo(_terminal_ext_id, (int)Type, out TerminalId, out ProviderId, out Name, out _status, out _config_svr_ext_id, out _sas_account_denom, Trx);

      if (!_exists)
      {
        try
        {
          _cmd_txt = "";
          _cmd_txt += "SELECT   CAST (GP_KEY_VALUE AS SMALLINT) "; 
          _cmd_txt += "  FROM   GENERAL_PARAMS ";
          _cmd_txt += " WHERE   GP_GROUP_KEY   = 'WCP' ";
          _cmd_txt += "   AND   GP_SUBJECT_KEY = 'ProvisioningManual' ";

          _sql_cmd = new SqlCommand(_cmd_txt);
          _sql_cmd.Connection = Trx.Connection;
          _sql_cmd.Transaction = Trx;

          _obj = _sql_cmd.ExecuteScalar();
          if (_obj != null)
          {
            _sql_value = (Int16)_obj;
          }
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }

        if ((Type == WCP_TerminalType.IntermediateServer) || (_sql_value == 0))
        {
          if (!DB_InsertTerminal(ExternalServerId, ExternalTerminalId, Type, ReceivedProviderId, ReceivedTerminalType, Trx))
          {
            return false;
          }
        }
        else
        {
          EnrolledTerminal = false;
          return DB_InsertTerminalPending(ExternalTerminalId, ReceivedProviderId, ReceivedTerminalType, Trx);
        }

        _exists = Misc.GetTerminalInfo(_terminal_ext_id, (int)Type, out TerminalId, out ProviderId, out Name, out _status, out _config_svr_ext_id, out _sas_account_denom, Trx);
        if (!_exists)
        {
          return false;
        }
      }

      // Not found or blocked
      if (TerminalId == 0
          || _status != TerminalStatus.ACTIVE)
      {
        return false;
      }

      //
      // Get & Check Server Id
      //
      if (Type == WCP_TerminalType.GamingTerminal && ExternalServerId != "")
      {
        Boolean _exists_server;
        int _dummy_terminal_id;
        String _dummy_provider_id;
        String _dummy_name;
        TerminalStatus _server_status;
        String _dummy_server;

        _exists_server = Misc.GetTerminalInfo(ExternalServerId, (int)WCP_TerminalType.IntermediateServer,
                                          out _dummy_terminal_id, out _dummy_provider_id, out _dummy_name, out _server_status, out _dummy_server, out _sas_account_denom, Trx);

        if (!_exists_server
            || _server_status != TerminalStatus.ACTIVE)
        {
          throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_SERVER_NOT_AUTHORIZED, "Server Not Authorized.");
        }
      }

      _sql_reader = null;
      _sql_cmd = null;

      try
      {
        //
        // Update Terminal: Type & Server
        //
        _cmd_txt = "";
        _cmd_txt += "UPDATE   TERMINALS ";
        _cmd_txt += "   SET   TE_TERMINAL_TYPE = CASE WHEN ( @pTerminalType >= 0 ) THEN @pTerminalType ELSE TE_TERMINAL_TYPE END ";
        _cmd_txt += "       , TE_SERVER_ID     = (SELECT TE_TERMINAL_ID FROM TERMINALS WHERE TE_EXTERNAL_ID = @pServerExternalId AND TE_TYPE = @pServerType)";
        _cmd_txt += " WHERE   TE_TERMINAL_ID   = @pTerminalId     ";
        _cmd_txt += "   AND   TE_EXTERNAL_ID   = @pExternalId     ";
        _cmd_txt += "   AND   TE_STATUS        = @pTerminalStatus ";

        _sql_cmd = new SqlCommand(_cmd_txt);
        _sql_cmd.Connection = Trx.Connection;
        _sql_cmd.Transaction = Trx;

        _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;
        _sql_cmd.Parameters.Add("@pExternalId", SqlDbType.NVarChar).Value = _terminal_ext_id;
        _sql_cmd.Parameters.Add("@pTerminalType", SqlDbType.SmallInt).Value = (Int16)ReceivedTerminalType;
        _sql_cmd.Parameters.Add("@pTerminalStatus", SqlDbType.SmallInt).Value = TerminalStatus.ACTIVE;
        _sql_cmd.Parameters.Add("@pServerExternalId", SqlDbType.NVarChar).Value = _server_ext_id;
        _sql_cmd.Parameters.Add("@pServerType", SqlDbType.SmallInt).Value = (int)WCP_TerminalType.IntermediateServer;

        if (_sql_cmd.ExecuteNonQuery() != 1)
        {
          return false;
        }
        //
        // Get MAX Last Sequence Id, Transaction Id
        //
        _cmd_txt = "";
        _cmd_txt += "SELECT   MAX (WS_LAST_SEQUENCE_ID)    ";
        _cmd_txt += "       , MAX (WS_LAST_TRANSACTION_ID) ";
        _cmd_txt += "  FROM   WCP_SESSIONS ";
        _cmd_txt += " WHERE   WS_TERMINAL_ID = @pTerminalId";

        _sql_cmd = new SqlCommand(_cmd_txt);
        _sql_cmd.Connection = Trx.Connection;
        _sql_cmd.Transaction = Trx;

        _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;

        _sql_reader = _sql_cmd.ExecuteReader();
        if (_sql_reader.Read())
        {
          SequenceId = _sql_reader.IsDBNull(0) ? 0 : _sql_reader.GetInt64(0);
          TransactionId = _sql_reader.IsDBNull(1) ? 0 : _sql_reader.GetInt64(1);
        }

        _sql_reader.Close();
        _sql_reader.Dispose();
        _sql_reader = null;

        //
        // Insert New Session
        //
        _cmd_txt = "";
        _cmd_txt += "INSERT INTO WCP_SESSIONS ( WS_TERMINAL_ID ";
        _cmd_txt += "                         , WS_LAST_SEQUENCE_ID ";
        _cmd_txt += "                         , WS_LAST_TRANSACTION_ID ";
        _cmd_txt += "                         , WS_LAST_RCVD_MSG ";
        _cmd_txt += "                         , WS_SERVER_NAME ";
        _cmd_txt += "                         )  ";
        _cmd_txt += "                  VALUES ( @pTerminalId ";
        _cmd_txt += "                         , @pSequenceId ";
        _cmd_txt += "                         , @pTransactionId ";
        _cmd_txt += "                         , GETDATE() ";
        _cmd_txt += "                         , @pServerName ";
        _cmd_txt += "                         ) ";
        _cmd_txt += "                     SET   @pSessionId = SCOPE_IDENTITY() ";

        _sql_cmd = new SqlCommand(_cmd_txt);
        _sql_cmd.Connection = Trx.Connection;
        _sql_cmd.Transaction = Trx;

        _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;
        _sql_cmd.Parameters.Add("@pSequenceId", SqlDbType.BigInt).Value = SequenceId;
        _sql_cmd.Parameters.Add("@pTransactionId", SqlDbType.BigInt).Value = TransactionId;
        _sql_cmd.Parameters.Add("@pServerName", SqlDbType.NVarChar).Value = Environment.MachineName;
        p = _sql_cmd.Parameters.Add("@pSessionId", SqlDbType.BigInt);
        p.Direction = ParameterDirection.Output;

        if (_sql_cmd.ExecuteNonQuery() != 1)
        {
          return false;
        }

        SessionId = (Int64)p.Value;

        //
        // Abandoned sessions: Moved to BatchUpdate
        //

        return true;
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
      finally
      {
        if (_sql_reader != null)
        {
          _sql_reader.Close();
          _sql_reader.Dispose();
        }
        if (_sql_cmd != null)
        {
          _sql_cmd.Dispose();
        }
      }

      return false;
    } // DB_GetEnrollData

    /// <summary>
    /// Update timeout wcp sessions
    /// </summary>
    /// <param name="Connection"></param>
    public static void DB_UpdateTimeoutWcpSessions(SqlConnection Connection)
    {
      SqlTransaction sql_trx;
      SqlCommand sql_command;
      String sql_str;
      Int32 num_rows_updated;
      Boolean error;

      error = true;
      sql_trx = null;

      try
      {
        sql_trx = Connection.BeginTransaction();

        //
        // Update Timeout Sessions
        //
        sql_str = "UPDATE WCP_SESSIONS SET WS_STATUS = @p1, " +
                                          "WS_FINISHED = GETDATE() " +
                                    "WHERE DATEDIFF(MINUTE ,WS_LAST_RCVD_MSG, GETDATE()) >= 5 " +
                                      "AND WS_STATUS = 0 " +
                                      "AND WS_FINISHED IS NULL ";

        sql_command = new SqlCommand(sql_str);
        sql_command.Connection = sql_trx.Connection;
        sql_command.Transaction = sql_trx;

        sql_command.Parameters.Add("@p1", SqlDbType.Int, 4, "WS_STATUS").Value = WCP_SessionStatus.Timeout;

        num_rows_updated = sql_command.ExecuteNonQuery();

        error = false;
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
      finally
      {
        if (sql_trx != null)
        {
          if (error)
          {
            sql_trx.Rollback();
          }
          else
          {
            sql_trx.Commit();
          }
        }
      }
    } // DB_UpdateTimeoutWcpSessions


    //------------------------------------------------------------------------------
    // PURPOSE: Get Currencies from database
    // 
    //  PARAMS:
    //      - INPUT: 
    //
    //      - OUTPUT: 
    //
    // RETURNS: WCP_NoteAcceptor_Parameters
    // 
    //   NOTES:
    //
    public static WCP_MsgGetParametersReply.WCP_NoteAcceptor_Parameters DB_GetCurrencies()
    {
      StringBuilder _sb;
      WSI.WCP.WCP_MsgGetParametersReply.WCP_NoteAcceptor_Parameters _note_acceptor;

      _note_acceptor = new WSI.WCP.WCP_MsgGetParametersReply.WCP_NoteAcceptor_Parameters();
      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine("    SELECT   CUR_ISO_CODE                                       ");
        _sb.AppendLine("           , ISNULL(CUR_ALIAS1, '')      CUR_ALIAS1             ");
        _sb.AppendLine("           , ISNULL(CUR_ALIAS2, '')      CUR_ALIAS2             ");
        _sb.AppendLine("	         , (CAST(CAST(CUD_DENOMINATION  AS float) AS INT))    ");
        _sb.AppendLine("      FROM   CURRENCIES                                         ");
        _sb.AppendLine(" LEFT JOIN   CURRENCY_DENOMINATIONS                             ");
        _sb.AppendLine("        ON   CUR_ISO_CODE = CUD_ISO_CODE                        ");
        _sb.AppendLine("       AND   CUD_TYPE = 0                                       ");
        _sb.AppendLine("       AND   CUD_REJECTED = 1                                   ");
        _sb.AppendLine("     WHERE   CUR_ALLOWED  = 1                                   ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.CommandText = _sb.ToString();
            using (SqlDataReader _sql_reader = _sql_cmd.ExecuteReader())
            {
              if (_sql_reader.Read())
              {
                _note_acceptor.ReadBarcode = false;
                _note_acceptor.AcceptedCurrencies = GetRejectedCurrencies(_sql_reader);
                _note_acceptor.NumCurrencies = _note_acceptor.AcceptedCurrencies.Count;
                _note_acceptor.Enabled = Misc.IsNoteAcceptorEnabled(); 
              }
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return _note_acceptor;

    }//DB_GetCurrencies

    //------------------------------------------------------------------------------
    // PURPOSE: Get Currencies and their rejected values
    // 
    //  PARAMS:
    //      - INPUT: SqlDataReader Sql_reader
    //
    //      - OUTPUT: 
    //
    // RETURNS: Dictionary<string, List<int>>
    // 
    //   NOTES:
    //
    private static List<WSI.WCP.WCP_MsgGetParametersReply.WCP_NoteAcceptor_Currency> GetRejectedCurrencies(SqlDataReader SqlReader)
    {
      List<WSI.WCP.WCP_MsgGetParametersReply.WCP_NoteAcceptor_Currency> _currencies;
      WSI.WCP.WCP_MsgGetParametersReply.FindCurrency _find_currencies;
      WSI.WCP.WCP_MsgGetParametersReply.WCP_NoteAcceptor_Currency _currency;

      _currencies = new List<WCP_MsgGetParametersReply.WCP_NoteAcceptor_Currency>();
      _find_currencies = new WCP_MsgGetParametersReply.FindCurrency();

      _currencies.Add(GetNewCurrency(SqlReader));
      while (SqlReader.Read())
      {
        _find_currencies.Name = SqlReader.GetString(0);
        _currency = _currencies.Find(_find_currencies.FindCurrencyByName);
        if (_currency != null)
        {
          if (!SqlReader.IsDBNull(3))
          {
            _currency.RejectedValues.Add(SqlReader.GetInt32(3));
          }
        }
        else
        {
          _currencies.Add(GetNewCurrency(SqlReader));
        }
      }

      return _currencies;
    }//GetRejectedCurrencies


    private static WSI.WCP.WCP_MsgGetParametersReply.WCP_NoteAcceptor_Currency GetNewCurrency(SqlDataReader SqlReader)
    {
      WSI.WCP.WCP_MsgGetParametersReply.WCP_NoteAcceptor_Currency _currency;

      _currency = new WCP_MsgGetParametersReply.WCP_NoteAcceptor_Currency();
      _currency = new WCP_MsgGetParametersReply.WCP_NoteAcceptor_Currency();
      _currency.IsoCode = SqlReader.GetString(0);
      _currency.Alias_1 = SqlReader.GetString(1);
      _currency.Alias_2 = SqlReader.GetString(2);
      if (!SqlReader.IsDBNull(3))
      {
        _currency.RejectedValues.Add(SqlReader.GetInt32(3));
      }

      return _currency;
    }


    #endregion EnrollAndSession

    #region PlaySessions

    //------------------------------------------------------------------------------
    // PURPOSE: Retrieves if the new PlaySession has to be created as StandAlone or not
    //
    //  PARAMS:
    //      - INPUT:
    //          - TerminalID
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    //      - True : 
    //      - False :
    //        
    //   NOTES :
    //
    public static Boolean DB_PlayStandAlone(Int32 TerminalID,
                                            out Boolean InsertPlay,
                                            SqlTransaction Trx)
    {
      SqlCommand _sql_cmd;
      String _sql_str;
      Int16 _sql_value;
      Boolean _stand_alone;
      Object _obj;

      _stand_alone = false;
      InsertPlay = true;

      try
      {
        _sql_str = "";
        _sql_str += "SELECT   ISNULL (TE_TERMINAL_TYPE, -1) ";
        _sql_str += "  FROM   TERMINALS ";
        _sql_str += " WHERE   TE_TERMINAL_ID = @pTerminalID ";

        _sql_cmd = new SqlCommand(_sql_str);
        _sql_cmd.Connection = Trx.Connection;
        _sql_cmd.Transaction = Trx;
        _sql_cmd.Parameters.Add("@pTerminalID", SqlDbType.Int).Value = TerminalID;

        _sql_value = -1;
        _obj = _sql_cmd.ExecuteScalar();
        if (_obj != null)
        {
          _sql_value = (Int16)_obj;
        }

        switch (_sql_value)
        {
          case 1: // WIN
            {
              try
              {
                _sql_str = "";
                _sql_str += "SELECT   CAST (GP_KEY_VALUE AS SMALLINT) ";
                _sql_str += "  FROM   GENERAL_PARAMS ";
                _sql_str += " WHERE   GP_GROUP_KEY   = 'Play' ";
                _sql_str += "   AND   GP_SUBJECT_KEY = 'IgnoreSasClientStatistics' ";

                _sql_cmd = new SqlCommand(_sql_str);
                _sql_cmd.Connection = Trx.Connection;
                _sql_cmd.Transaction = Trx;

                _sql_value = 0;
                _obj = _sql_cmd.ExecuteScalar();
                if (_obj != null)
                {
                  _sql_value = (Int16)_obj;
                }

                if (_sql_value == 1)
                {
                  InsertPlay = false;
                }
              }
              catch (Exception _ex)
              {
                Log.Exception(_ex);
              }
              return false;
            }
          case 5: // SAS_HOST
            return true;

          case 100: // SITE
          case 101: // SITE_JACKPOT
          case 102: // MOBILE_BANK
            return false;

          default:
            break;
        } // switch ( _sql_value )

        _sql_str = "";
        _sql_str += "SELECT   CAST (GP_KEY_VALUE AS SMALLINT) ";
        _sql_str += "  FROM   GENERAL_PARAMS ";
        _sql_str += " WHERE   GP_GROUP_KEY   = 'Play' ";
        _sql_str += "   AND   GP_SUBJECT_KEY = 'StandAlone' ";

        _sql_cmd = new SqlCommand(_sql_str);
        _sql_cmd.Connection = Trx.Connection;
        _sql_cmd.Transaction = Trx;

        _sql_value = 0;
        _obj = _sql_cmd.ExecuteScalar();
        if (_obj != null)
        {
          _sql_value = (Int16)_obj;
        }

        if (_sql_value != 0)
        {
          _stand_alone = true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      finally
      {
      }

      return _stand_alone;
    } // DB_PlayStandAlone

    /// <summary>
    /// Insert Play Session and return Id
    /// </summary>
    /// <param name="CardId"></param>
    /// <param name="AccountType"></param>
    /// <param name="TerminalId"></param>
    /// <param name="XmlTypeData"></param>
    /// <param name="InitialBalance"></param>
    /// <param name="StandAlone"></param>
    /// <param name="Promotion"></param>
    /// <param name="TransactionId"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    public static Int64 DB_InsertPlaySession(Int64 CardId,
                                             AccountType AccountType,
                                             Int32 TerminalId,
                                             String XmlTypeData,
                                             Decimal InitialBalance,
                                             Boolean StandAlone,
                                             Int64 TransactionId,
                                             SqlTransaction Trx)
    {
      SqlCommand sql_command;
      SqlCommand _sql_cmd;
      SqlDataReader reader;
      SqlParameter p;
      String sql_str;
      Int32 num_rows_inserted;
      //Int32 num_rows_updated;
      Int64 current_play_session_id;
      Int32 current_terminal_id;
      Decimal current_balance;
      Int32 _sql_value;
      Boolean activity_due_to_card_in;
      PlaySessionType ps_type;
      Object _obj;

      switch (AccountType)
      {
        case AccountType.ACCOUNT_WIN:
          ps_type = PlaySessionType.WIN;
          break;

        case AccountType.ACCOUNT_ALESIS:
          ps_type = PlaySessionType.ALESIS;
          break;

        case AccountType.ACCOUNT_CADILLAC_JACK:
          ps_type = PlaySessionType.CADILLAC_JACK;
          break;

        case AccountType.ACCOUNT_VIRTUAL_TERMINAL:
          ps_type = PlaySessionType.WIN;
          break;

        default:
          ps_type = PlaySessionType.NONE;
          Log.Error("Invalid Account Type:" + AccountType.ToString() + " AccID:" + CardId.ToString());
          throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR, "Invalid Account Type.");
        // break;
      }

      activity_due_to_card_in = false;

      try
      {
        sql_str = "";
        sql_str += "SELECT   CAST (GP_KEY_VALUE AS INT) ";
        sql_str += "  FROM   GENERAL_PARAMS ";
        sql_str += " WHERE   GP_GROUP_KEY   = 'Play' ";
        sql_str += "   AND   GP_SUBJECT_KEY = 'ActivityDueToCardIn' ";

        _sql_cmd = new SqlCommand(sql_str);
        _sql_cmd.Connection = Trx.Connection;
        _sql_cmd.Transaction = Trx;

        _sql_value = 0;
        _obj = _sql_cmd.ExecuteScalar();
        if (_obj != null)
        {
          _sql_value = (Int32)_obj;
        }

        if (_sql_value != 0)
        {
          activity_due_to_card_in = true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      finally
      {
      }

      // Mark as ABANONDED all the PREVIOUS SESSIONS
      //  - Abandoned, Non Stand Alone
      //  - Abandoned, Stand Alone
      sql_str = "UPDATE PLAY_SESSIONS SET PS_STATUS        = 2 " + // Abandoned
                                       ", PS_FINAL_BALANCE = PS_INITIAL_BALANCE - PS_PLAYED_AMOUNT + PS_WON_AMOUNT + PS_CASH_IN - PS_CASH_OUT " +
                                       ", PS_FINISHED      = GETDATE() " +
                                  " WHERE PS_TERMINAL_ID   = @p1 " +
                                  "   AND PS_STATUS        = 0 " +  // Opened
                                  "   AND PS_STAND_ALONE   = 0 ";     // Not stand alone

      sql_command = new SqlCommand(sql_str);
      sql_command.Connection = Trx.Connection;
      sql_command.Transaction = Trx;

      sql_command.Parameters.Add("@p1", SqlDbType.Int, 4, "PS_TERMINAL_ID").Value = TerminalId;
      sql_command.ExecuteNonQuery();

      sql_str = "UPDATE PLAY_SESSIONS SET PS_STATUS        = 2 " + // Abandoned
                                       ", PS_FINISHED      = GETDATE() " +
                                  " WHERE PS_TERMINAL_ID   = @p1 " +
                                  "   AND PS_STATUS        = 0 " +  // Opened
                                  "   AND PS_STAND_ALONE   = 1 ";   // Stand alone

      sql_command = new SqlCommand(sql_str);
      sql_command.Connection = Trx.Connection;
      sql_command.Transaction = Trx;

      sql_command.Parameters.Add("@p1", SqlDbType.Int, 4, "PS_TERMINAL_ID").Value = TerminalId;
      sql_command.ExecuteNonQuery();


      //
      // Search Current Terminal/Session on Card
      //
      sql_str = "";
      sql_str += "SELECT   AC_CURRENT_TERMINAL_ID";
      sql_str += "       , AC_CURRENT_PLAY_SESSION_ID";
      sql_str += "       , AC_BALANCE";
      sql_str += "  FROM   ACCOUNTS ";
      sql_str += " WHERE  AC_ACCOUNT_ID = @p1";

      sql_command = new SqlCommand(sql_str);
      sql_command.Connection = Trx.Connection;
      sql_command.Transaction = Trx;
      sql_command.Parameters.Add("@p1", SqlDbType.BigInt, 8, "AC_ACCOUNT_ID").Value = CardId;

      reader = null;
      current_play_session_id = 0;
      current_terminal_id = 0;
      current_balance = 0;

      try
      {
        reader = sql_command.ExecuteReader();
        if (reader.Read())
        {
          if (!reader.IsDBNull(0)) { current_terminal_id = (Int32)reader[0]; }
          if (!reader.IsDBNull(1)) { current_play_session_id = (Int64)reader[1]; }
          if (!reader.IsDBNull(2)) { current_balance = (Decimal)reader[2]; }
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
        Log.Warning("Exception throw: Error reading Cards. CardId: " + CardId.ToString());
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR, "DB Error reading cards.");
      }
      finally
      {
        if (reader != null)
        {
          reader.Close();
          reader.Dispose();
          reader = null;
        }
      }

      if (InitialBalance != current_balance)
      {
        Log.Error("Balance changed while opening session, AccID:" + CardId.ToString() + " Ini/Fin:" + InitialBalance.ToString() + "/" + current_balance.ToString());
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR, "Balance changed while opening session.");
      }
      if (activity_due_to_card_in)
      {
        sql_str = "INSERT INTO PLAY_SESSIONS (PS_ACCOUNT_ID " +
                                           ", PS_TERMINAL_ID " +
                                           ", PS_TYPE " +
                                           ", PS_TYPE_DATA " +
                                           ", PS_INITIAL_BALANCE " +
                                           ", PS_FINAL_BALANCE " +
                                           ", PS_FINISHED " +
                                           ", PS_STAND_ALONE " +
                                           ", PS_WCP_TRANSACTION_ID) " +
                                     "VALUES (@p1, @p2, @p3, @p4, @p5, @p6, GETDATE(), @p7, @p9) " +
                                     " SET @p10 = SCOPE_IDENTITY()";
      }
      else
      {
        sql_str = "INSERT INTO PLAY_SESSIONS (PS_ACCOUNT_ID " +
                                           ", PS_TERMINAL_ID " +
                                           ", PS_TYPE " +
                                           ", PS_TYPE_DATA " +
                                           ", PS_INITIAL_BALANCE " +
                                           ", PS_FINAL_BALANCE " +
                                           ", PS_FINISHED " +
                                           ", PS_STAND_ALONE " +
                                           ", PS_WCP_TRANSACTION_ID) " +
                                     "VALUES (@p1, @p2, @p3, @p4, @p5, @p6, NULL, @p7, @p9) " +
                                     " SET @p10 = SCOPE_IDENTITY()";
      }


      // PS_STATUS --> Default 0: Opened.

      sql_command = new SqlCommand(sql_str);
      sql_command.Connection = Trx.Connection;
      sql_command.Transaction = Trx;

      sql_command.Parameters.Add("@p1", SqlDbType.BigInt, 8, "PS_ACCOUNT_ID").Value = CardId;
      sql_command.Parameters.Add("@p2", SqlDbType.Int, 4, "PS_TERMINAL_ID").Value = TerminalId;
      sql_command.Parameters.Add("@p3", SqlDbType.Int, 4, "PS_TYPE").Value = (Int32)ps_type;
      if (XmlTypeData == "")
      {
        sql_command.Parameters.Add("@p4", SqlDbType.Xml, Int32.MaxValue, "PS_TYPE_DATA").Value = DBNull.Value;
      }
      else
      {
        sql_command.Parameters.Add("@p4", SqlDbType.Xml, Int32.MaxValue, "PS_TYPE_DATA").Value = XmlTypeData;
      }

      sql_command.Parameters.Add("@p5", SqlDbType.Money).Value = InitialBalance;
      if (StandAlone)
      {
        sql_command.Parameters.Add("@p6", SqlDbType.Money).Value = 0;
      }
      else
      {
        sql_command.Parameters.Add("@p6", SqlDbType.Money).Value = InitialBalance;
      }
      sql_command.Parameters.Add("@p7", SqlDbType.Bit, 1, "PS_STAND_ALONE").Value = StandAlone;
      sql_command.Parameters.Add("@p9", SqlDbType.BigInt, 8, "PS_WCP_TRANSACTION_ID").Value = TransactionId;

      p = sql_command.Parameters.Add("@p10", SqlDbType.BigInt, 8, "PS_PLAY_SESSION_ID");
      p.Direction = ParameterDirection.Output;

      num_rows_inserted = sql_command.ExecuteNonQuery();

      if (num_rows_inserted != 1)
      {
        Log.Warning("Exception throw: inserting Play Sessions.TerminalId: " + TerminalId.ToString());
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR, "PLAY_SESSION Not Inserted.");
      }
      current_play_session_id = (Int64)p.Value;

      return current_play_session_id;

    } // DB_InsertPlaySession

    //------------------------------------------------------------------------------
    // PURPOSE: Check the number of active sessions depending on the 
    //          General parameter: MinimumPlayers
    //
    //  PARAMS:
    //      - INPUT:
    //          - PlaySessionId
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    //      - True : 
    //      - False :
    //        
    //   NOTES :
    //
    public static Boolean DB_EnoughActivePlaySessions(Int64 PlaySessionId)
    {
      SqlConnection sql_con;
      SqlTransaction sql_trx;
      String _sql_str;
      SqlCommand _sql_cmd;
      Int32 _sql_value;
      Int64 minimum_players;
      DateTime wait_started;
      TimeSpan wait_elapsed;
      Object _obj;

      sql_con = null;
      sql_trx = null;

      wait_started = DateTime.Now;

      try
      {
        sql_con = WGDB.Connection();
        if (sql_con == null)
        {
          Log.Error("Connection is Null");

          return false;
        }

        _sql_str = "";
        _sql_str += "SELECT   CAST (GP_KEY_VALUE AS BIGINT) ";
        _sql_str += "  FROM   GENERAL_PARAMS ";
        _sql_str += " WHERE   GP_GROUP_KEY   = 'Play' ";
        _sql_str += "   AND   GP_SUBJECT_KEY = 'MinimumPlayers' ";

        _sql_cmd = new SqlCommand(_sql_str);
        _sql_cmd.Connection = sql_con;

        _obj = _sql_cmd.ExecuteScalar();
        if (_obj == null)
        {
          return false;
        }
        minimum_players = (Int64)_obj;

        if (minimum_players <= 1)
        {
          return true;
        }

        sql_trx = sql_con.BeginTransaction();

        _sql_str = "";
        _sql_str += "UPDATE   PLAY_SESSIONS ";
        _sql_str += "   SET   PS_FINISHED        = GETDATE() ";
        _sql_str += " WHERE   PS_PLAY_SESSION_ID = @p1 ";
        _sql_str += "   AND   PS_STATUS          = 0 ";   // Opened

        _sql_cmd = new SqlCommand(_sql_str);
        _sql_cmd.Connection = sql_con;
        _sql_cmd.Transaction = sql_trx;
        _sql_cmd.Parameters.Add("@p1", SqlDbType.BigInt, 8, "PS_PLAY_SESSION_ID").Value = PlaySessionId;

        // Don't care about the return code.
        _sql_cmd.ExecuteNonQuery();

        sql_trx.Commit();

        while (true)
        {
          _sql_str = "";
          _sql_str += "SELECT   COUNT(*) ";
          _sql_str += "  FROM ( SELECT   COUNT(*) NUM_ACTIVE_SESSIONS ";
          _sql_str += "           FROM   PLAY_SESSIONS ";
          _sql_str += "          WHERE   PS_STATUS = 0 ";
          // ACC on 08-JAN-2009 due to avoid the overflow in "DATEDIFF (MILLISECOND, PS_FINISHED, GETDATE())" sentence
          _sql_str += "            AND   DATEDIFF (DAY, PS_FINISHED, GETDATE()) < 24 ";
          _sql_str += "            AND   DATEDIFF (MILLISECOND, PS_FINISHED, GETDATE()) <= ";
          _sql_str += "                  ( SELECT   CAST (GP_KEY_VALUE AS BIGINT) ";
          _sql_str += "                      FROM   GENERAL_PARAMS ";
          _sql_str += "                     WHERE   GP_GROUP_KEY   = 'Play' ";
          _sql_str += "                       AND   GP_SUBJECT_KEY = 'ActivityTimeout' ";
          _sql_str += "                  ) ";
          _sql_str += "       ) ACTIVE_SESSIONS ";
          _sql_str += " WHERE   NUM_ACTIVE_SESSIONS >= ";
          _sql_str += "       ( SELECT   CAST (GP_KEY_VALUE AS BIGINT) ";
          _sql_str += "           FROM   GENERAL_PARAMS ";
          _sql_str += "          WHERE   GP_GROUP_KEY   = 'Play' ";
          _sql_str += "            AND   GP_SUBJECT_KEY = 'MinimumPlayers' ) ";

          _sql_cmd = new SqlCommand(_sql_str);
          _sql_cmd.Connection = sql_con;

          _sql_value = (Int32)_sql_cmd.ExecuteScalar();
          if (_sql_value == 1)
          {
            return true;
          }

          _sql_str = "";
          _sql_str += "SELECT   CAST (GP_KEY_VALUE AS INT) ";
          _sql_str += "  FROM   GENERAL_PARAMS ";
          _sql_str += " WHERE   GP_GROUP_KEY   = 'Play' ";
          _sql_str += "   AND   GP_SUBJECT_KEY = 'WaitForMinimumPlayers' ";

          _sql_cmd = new SqlCommand(_sql_str);
          _sql_cmd.Connection = sql_con;

          _sql_value = 0;
          _obj = _sql_cmd.ExecuteScalar();
          if (_obj != null)
          {
            _sql_value = (Int32)_obj;
          }

          if (_sql_value == 0)
          {
            return false;
          }

          wait_elapsed = DateTime.Now.Subtract(wait_started);

          if (wait_elapsed.TotalMilliseconds >= _sql_value
              || wait_elapsed.TotalMilliseconds < 0)
          {
            return false;
          }

          System.Threading.Thread.Sleep(250);
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
      finally
      {
        if (sql_trx != null)
        {
          if (sql_trx.Connection != null)
          {
            sql_trx.Rollback();
          }
        }
      }
    } // DB_EnoughActivePlaySessions

    //------------------------------------------------------------------------------
    // PURPOSE: Closes a Play Session
    //
    //  PARAMS:
    //      - INPUT:
    //          - PlaySessionId
    //          - AccountID
    //          - FinalBalance
    //          - IsPromotion
    //          - Trx
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    //      - Nothing
    //        
    //   NOTES :
    //
    public static void DB_PlaySessionClose(Int64 PlaySessionId,
                                           Int64 AccountID,
                                           Decimal FinalBalance,
                                           SqlTransaction Trx)
    {
      SqlCommand sql_command;
      String sql_str;
      Boolean _stand_alone;

      sql_str = "";
      sql_str += "UPDATE PLAY_SESSIONS SET   PS_FINAL_BALANCE   = @pFinalBalance";
      sql_str += "                         , PS_STATUS          = @pNewStatus";
      sql_str += "                         , PS_LOCKED          = NULL";
      sql_str += "                         , PS_FINISHED        = GETDATE()";
      sql_str += "                   WHERE   PS_PLAY_SESSION_ID = @pPlaySessionID";
      sql_str += "                     AND   PS_STATUS          = @pOldStatus";

      sql_command = new SqlCommand(sql_str);
      sql_command.Connection = Trx.Connection;
      sql_command.Transaction = Trx;

      sql_command.Parameters.Add("@pFinalBalance", SqlDbType.Money).Value = FinalBalance;
      sql_command.Parameters.Add("@pNewStatus", SqlDbType.Int).Value = (Int32)PlaySessionStatus.Closed;
      sql_command.Parameters.Add("@pOldStatus", SqlDbType.Int).Value = (Int32)PlaySessionStatus.Opened;
      sql_command.Parameters.Add("@pPlaySessionID", SqlDbType.BigInt).Value = PlaySessionId;

      if (sql_command.ExecuteNonQuery() == 1)
      {
        if (AccountID > 0)
        {
          //
          // AJQ 16-FEB-2010, The play session was active
          //
          sql_str = "";
          sql_str += "SELECT   PS_STAND_ALONE ";
          sql_str += "  FROM   PLAY_SESSIONS";
          sql_str += " WHERE   PS_PLAY_SESSION_ID = @pPlaySessionID";

          sql_command = new SqlCommand(sql_str);
          sql_command.Connection = Trx.Connection;
          sql_command.Transaction = Trx;

          sql_command.Parameters.Add("@pPlaySessionID", SqlDbType.BigInt).Value = PlaySessionId;

          _stand_alone = (Boolean)sql_command.ExecuteScalar();

          if (_stand_alone)
          {
            // Add the received amount to the current balance
            DB_AddToAccountBalance(AccountID, FinalBalance, Trx);
          }
          else
          {
            // Not stand alone, set the final balance
            DB_SetAccountBalance(AccountID, FinalBalance, false, 0, Trx);
          }
        }
      }

      //
      // AJQ 16-FEB-2010, Ensure still the same play session
      //
      sql_str = "";
      sql_str += "UPDATE ACCOUNTS SET   AC_LAST_TERMINAL_ID        = AC_CURRENT_TERMINAL_ID";
      sql_str += "                    , AC_LAST_TERMINAL_NAME      = AC_CURRENT_TERMINAL_NAME";
      sql_str += "                    , AC_LAST_PLAY_SESSION_ID    = AC_CURRENT_PLAY_SESSION_ID";
      sql_str += "                    , AC_CURRENT_TERMINAL_ID     = NULL";
      sql_str += "                    , AC_CURRENT_TERMINAL_NAME   = NULL";
      sql_str += "                    , AC_CURRENT_PLAY_SESSION_ID = NULL";
      sql_str += "              WHERE   AC_ACCOUNT_ID              = @pAccountID";
      sql_str += "                AND   AC_CURRENT_PLAY_SESSION_ID = @pPlaySessionID";

      sql_command = new SqlCommand(sql_str);
      sql_command.Connection = Trx.Connection;
      sql_command.Transaction = Trx;
      sql_command.Parameters.Add("@pAccountID", SqlDbType.BigInt).Value = AccountID;
      sql_command.Parameters.Add("@pPlaySessionID", SqlDbType.BigInt).Value = PlaySessionId;

      sql_command.ExecuteNonQuery();

    } // DB_PlaySessionClose

    //------------------------------------------------------------------------------
    // PURPOSE: Reports a Play on the given Play Session
    //
    //  PARAMS:
    //      - INPUT:
    //          - PlaySessionId
    //          - PlayedAmount
    //          - WonAmount
    //          - CashIn
    //          - CashOut
    //          - Trx
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    //      - Nothing
    //        
    //   NOTES :
    //

    public static void DB_PlaySessionReportPlay(Int64 PlaySessionId,
                                                Decimal PlayedAmount,
                                                Decimal WonAmount,
                                                Decimal CashIn,
                                                Decimal CashOut,
                                                Decimal FinalBalance,
                                                SqlTransaction Trx)
    {
      SqlCommand sql_command;
      String sql_str;
      Int32 num_rows_updated;
      Int32 won_count;

      // ACC 18-DEC-2009 Support plays with PlayedAmount == 0
      if (PlayedAmount < 0)
      {
        Log.Warning("PlayedAmount less than 0, PlaySessionId: " + PlaySessionId.ToString());
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR, "PlayedAmount less than 0");
      }
      // AJQ 25-JAN-2010 Check >= 0
      if (WonAmount < 0)
      {
        Log.Warning("WonAmount less than 0, PlaySessionId: " + PlaySessionId.ToString());
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR, "WonAmount less than 0");
      }
      // AJQ 25-JAN-2010 Check >= 0
      if (CashIn < 0)
      {
        Log.Warning("CashIn less than 0, PlaySessionId: " + PlaySessionId.ToString());
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR, "CashIn less than 0");
      }
      // AJQ 25-JAN-2010 Check >= 0
      if (CashOut < 0)
      {
        Log.Warning("CashOut less than 0, PlaySessionId: " + PlaySessionId.ToString());
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR, "CashOut less than 0");
      }
      // AJQ 25-JAN-2010 Check >= 0
      if (FinalBalance < 0)
      {
        Log.Warning("FinalBalance less than 0, PlaySessionId: " + PlaySessionId.ToString());
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR, "FinalBalance less than 0");
      }

      won_count = 0;
      if (WonAmount > 0)
      {
        won_count = 1;
      }

      sql_str = "";
      sql_str += "UPDATE PLAY_SESSIONS SET    PS_PLAYED_COUNT    = PS_PLAYED_COUNT  + 1 ";
      sql_str += "                          , PS_PLAYED_AMOUNT   = PS_PLAYED_AMOUNT + @pPlayedAmount";
      sql_str += "                          , PS_WON_COUNT       = PS_WON_COUNT     + @pWonCount";
      sql_str += "                          , PS_WON_AMOUNT      = PS_WON_AMOUNT    + @pWonAmount";
      sql_str += "                          , PS_CASH_IN         = PS_CASH_IN       + @pCashIn";
      sql_str += "                          , PS_CASH_OUT        = PS_CASH_OUT      + @pCashOut";
      sql_str += "                          , PS_FINAL_BALANCE   = CASE WHEN (PS_STAND_ALONE = 1) THEN PS_FINAL_BALANCE ELSE @pFinalBalance END ";
      sql_str += "                          , PS_LOCKED          = NULL";
      sql_str += "                          , PS_FINISHED        = GETDATE()";
      sql_str += "                    WHERE   PS_PLAY_SESSION_ID = @pPlaySessionID ";
      sql_str += "                      AND (   ( PS_STAND_ALONE = 1 )";
      sql_str += "                           OR ( PS_STAND_ALONE = 0 AND PS_STATUS = @pStatus ) )";

      sql_command = new SqlCommand(sql_str);
      sql_command.Connection = Trx.Connection;
      sql_command.Transaction = Trx;

      sql_command.Parameters.Add("@pPlayedAmount", SqlDbType.Money).Value = PlayedAmount;
      sql_command.Parameters.Add("@pWonCount", SqlDbType.Int).Value = won_count;
      sql_command.Parameters.Add("@pWonAmount", SqlDbType.Money).Value = WonAmount;
      sql_command.Parameters.Add("@pCashIn", SqlDbType.Money).Value = CashIn;
      sql_command.Parameters.Add("@pCashOut", SqlDbType.Money).Value = CashOut;
      sql_command.Parameters.Add("@pFinalBalance", SqlDbType.Money).Value = FinalBalance;
      sql_command.Parameters.Add("@pPlaySessionID", SqlDbType.BigInt).Value = PlaySessionId;

      sql_command.Parameters.Add("@pStatus", SqlDbType.Int).Value = (Int32)PlaySessionStatus.Opened;

      num_rows_updated = sql_command.ExecuteNonQuery();

      if (num_rows_updated != 1)
      {
        Log.Warning("Exception throw: updating balance.PlaySessionId: " + PlaySessionId.ToString());
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR, "PLAY_SESSION Not Updated.");
      }

    } // DB_PlaySessionReportPlay

    /// <summary>
    /// Get play session track number
    /// </summary>
    /// <param name="PlaySessionId"></param>
    /// <param name="TrackNumber"></param>
    /// <param name="SqlTrx"></param>
    public static void DB_GetSessionTrackNumber(Int64 PlaySessionId,
                                                out String TrackNumber,
                                                SqlTransaction SqlTrx)
    {
      SqlCommand sql_command;
      String sql_str;

      TrackNumber = "";

      // Get Track data
      sql_str = "SELECT PS_TYPE_DATA " +
                  "FROM PLAY_SESSIONS " +
                 "WHERE PS_PLAY_SESSION_ID = @p1";

      sql_command = new SqlCommand(sql_str);
      sql_command.Connection = SqlTrx.Connection;
      sql_command.Transaction = SqlTrx;
      sql_command.Parameters.Add("@p1", SqlDbType.BigInt, 8, "PS_PLAY_SESSION_ID").Value = PlaySessionId;

      try
      {
        TrackNumber = ((String)sql_command.ExecuteScalar());
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
        Log.Warning("Exception throw: reading session track number.PlaySessionId: " + PlaySessionId.ToString());
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR, "DB Error reading session track number.");
      }

    } // DB_GetSessionTrackNumber

    #endregion PlaySessions

    #region Cards

    /// <summary>
    /// If the LockDateTime received is not null the cardsession will be locked
    /// </summary>
    /// <param name="CardSessionId"></param>
    /// <param name="LockDateTime"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    public static Boolean DB_LockPlaySession(Int64 PlaySessionID, SqlTransaction Trx)
    {
      SqlCommand sql_command;
      int num_rows_updated;
      DateTime lock_date_time;

      lock_date_time = new DateTime();
      lock_date_time = DateTime.Now;

      String sql_str;
      //
      // Update cards
      //
      sql_str = "UPDATE PLAY_SESSIONS SET PS_LOCKED     = @p1 " +
                          " WHERE PS_PLAY_SESSION_ID    = @p2 " +
                          "   AND PS_TYPE               = @p3 ";

      sql_command = new SqlCommand(sql_str);
      sql_command.Connection = Trx.Connection;
      sql_command.Transaction = Trx;

      sql_command.Parameters.Add("@p1", SqlDbType.DateTime).Value = lock_date_time;
      sql_command.Parameters.Add("@p2", SqlDbType.BigInt).Value = PlaySessionID;
      sql_command.Parameters.Add("@p3", SqlDbType.Int).Value = (Int32)PlaySessionType.WIN;

      try
      {
        num_rows_updated = sql_command.ExecuteNonQuery();
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
        return false;
      }

      if (num_rows_updated != 1)
      {
        return false;
      }
      return true;

    } //DB_LockSession

    /// <summary>
    /// Unlock the Play Session
    /// </summary>
    /// <param name="CardSessionId"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    public static Boolean DB_UnlockPlaySession(Int64 CardSessionId, SqlTransaction Trx)
    {
      SqlCommand sql_command;
      int num_rows_updated;

      String sql_str;
      //
      // Update cards
      //
      sql_str = "UPDATE PLAY_SESSIONS SET PS_LOCKED     = NULL " +
                          " WHERE PS_PLAY_SESSION_ID    = @p2 ";

      sql_command = new SqlCommand(sql_str);
      sql_command.Connection = Trx.Connection;
      sql_command.Transaction = Trx;

      sql_command.Parameters.Add("@p2", SqlDbType.BigInt, 8, "PS_PLAY_SESSION_ID").Value = CardSessionId;

      try
      {
        num_rows_updated = sql_command.ExecuteNonQuery();
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
        return false;
      }

      if (num_rows_updated != 1)
      {
        return false;
      }
      return true;

    } //DB_UnlockSession


    /// <summary>
    /// Returns the timeout for a locked terminal
    /// </summary>
    /// <param name="LockTimeOut"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    public static Boolean Db_GetLockTimeOut(out int LockTimeOut, SqlTransaction Trx)
    {

      String _sql_str;
      SqlCommand _sql_command;
      Object _obj;

      LockTimeOut = 0;

      try
      {
        _sql_str = "";
        _sql_str += "SELECT   CAST (GP_KEY_VALUE AS INT) ";
        _sql_str += "  FROM   GENERAL_PARAMS ";
        _sql_str += " WHERE   GP_GROUP_KEY   = 'Play' ";
        _sql_str += "   AND   GP_SUBJECT_KEY = 'LockTimeOut' ";

        _sql_command = new SqlCommand(_sql_str);
        _sql_command.Connection = Trx.Connection;
        _sql_command.Transaction = Trx;

        _obj = _sql_command.ExecuteScalar();
        if (_obj == null)
        {
          return false;
        }
        LockTimeOut = (Int32)_obj;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        return false;
      }

      return true;

    } // Db_GetLockTimeOut


    //------------------------------------------------------------------------------
    // PURPOSE: Sets the given balance to the account identified by the track data
    //
    //  PARAMS:
    //      - INPUT:
    //        - CardType: CJ | ALESIS
    //        - CardTrackData: Track data
    //        - Balance
    //
    //      - OUTPUT:
    //
    // RETURNS: The AccountID
    //        
    //   NOTES :
    //
    public static Int64 DB_SetAccountBalance(AccountType CardType,
                                              String CardTrackData,
                                              Decimal Balance,
                                              Int64 PlaySessionID,
                                              SqlTransaction Trx)
    {
      SqlCommand _sql_command;
      SqlParameter p;
      String sql_str;
      Int32 num_rows_affected;
      Int64 _account_id;
      String holder_name;
      Int32 num_rows_updated;
      Object _obj;

      _account_id = 0;

      //
      // Search Card
      //
      sql_str = "SELECT AC_ACCOUNT_ID FROM ACCOUNTS WHERE AC_TRACK_DATA = @pTrackData AND AC_TYPE = @pCardType";

      _sql_command = new SqlCommand(sql_str);
      _sql_command.Connection = Trx.Connection;
      _sql_command.Transaction = Trx;
      _sql_command.Parameters.Add("@pTrackData", SqlDbType.NVarChar, 50, "AC_TRACK_DATA").Value = CardTrackData;
      _sql_command.Parameters.Add("@pCardType", SqlDbType.Int).Value = (int)CardType;

      try
      {
        _obj = _sql_command.ExecuteScalar();
        if (_obj != null)
        {
          _account_id = (Int64)_obj;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        // Not Found. Continue.
      }


      switch (CardType)
      {
        case AccountType.ACCOUNT_ALESIS:
          holder_name = "ALESIS - " + CardTrackData;
          break;

        case AccountType.ACCOUNT_CADILLAC_JACK:
          holder_name = "CADILLAC JACK - " + CardTrackData;
          break;

        default:
          Log.Warning("Exception throw: CardType Not Supported, Track Number: " + CardTrackData);

          throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR, "CardType Not Supported.");
        // break;
      }

      sql_str = "UPDATE   ACCOUNTS " +
                "   SET   AC_HOLDER_NAME     = @pHolderName" +
                "       , AC_BLOCKED         = 0" +
                "       , AC_BALANCE         = @pBalance" +
                "       , AC_LAST_ACTIVITY   = GETDATE() " +
      " WHERE   AC_TRACK_DATA      = @pTrackData" +
                "   AND   AC_TYPE            = @pCardType" +
                "   AND   AC_ACCOUNT_ID      = @pAccountID";

      _sql_command = new SqlCommand(sql_str);
      _sql_command.Connection = Trx.Connection;
      _sql_command.Transaction = Trx;

      _sql_command.Parameters.Add("@pHolderName", SqlDbType.NVarChar, 200, "AC_HOLDER_NAME").Value = holder_name;
      _sql_command.Parameters.Add("@pTrackData", SqlDbType.NVarChar, 50, "AC_TRACK_DATA").Value = CardTrackData;
      _sql_command.Parameters.Add("@pCardType", SqlDbType.Int).Value = (Int32)CardType;
      _sql_command.Parameters.Add("@pBalance", SqlDbType.Money).Value = Balance;


      p = _sql_command.Parameters.Add("@pAccountID", SqlDbType.BigInt);
      p.Value = _account_id;

      if (_account_id == 0)
      {
        sql_str = "INSERT INTO ACCOUNTS   (AC_HOLDER_NAME, AC_TRACK_DATA, AC_TYPE,    AC_BLOCKED, AC_BALANCE) " +
                  "              VALUES   (@pHolderName,   @pTrackData,   @pCardType, 0,          @pBalance) " +
                  "                 SET    @pAccountID = SCOPE_IDENTITY()";

        // Account doesn't exist
        p.Direction = ParameterDirection.Output;

        _sql_command.CommandText = sql_str;
      }


      num_rows_affected = 0;
      try
      {
        num_rows_affected = _sql_command.ExecuteNonQuery();
      }
      catch (Exception ex)
      {
        // Track data already exists ! with type != 1 ??
        Log.Exception(ex);
      }

      if (num_rows_affected != 1)
      {
        Log.Warning("Exception throw: inserting Card with Track Number: " + CardTrackData);
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR, "CARD Not Inserted.");
      }

      if (_account_id == 0)
      {
        _account_id = (Int64)p.Value;
      }

      // ACC 08-MAY-2009 Set initial balance into card session
      //
      // If card session is opened --> SET INITIAL CARD BALANCE
      //

      if (PlaySessionID > 0)
      {
        sql_str = "UPDATE PLAY_SESSIONS SET PS_INITIAL_BALANCE  = @pInitialBalance " +
                  "                       , PS_FINAL_BALANCE    = @pInitialBalance - PS_PLAYED_AMOUNT + PS_WON_AMOUNT + PS_CASH_IN - PS_CASH_OUT " +
                  " WHERE PS_PLAY_SESSION_ID = @pPlaySessionID " +
                  "   AND PS_STATUS          = @pStatus " +
                  "   AND PS_PLAYED_AMOUNT   = 0 " +
                  "   AND PS_WON_AMOUNT      = 0 " +
                  "   AND PS_CASH_IN         = 0 " +
                  "   AND PS_CASH_OUT        = 0 ";

        _sql_command = new SqlCommand(sql_str);
        _sql_command.Connection = Trx.Connection;
        _sql_command.Transaction = Trx;

        _sql_command.Parameters.Add("@pInitialBalance", SqlDbType.Money).Value = Balance;
        _sql_command.Parameters.Add("@pPlaySessionID", SqlDbType.BigInt).Value = PlaySessionID;
        _sql_command.Parameters.Add("@pStatus", SqlDbType.Int).Value = (Int32)PlaySessionStatus.Opened;

        num_rows_updated = _sql_command.ExecuteNonQuery();

        if (num_rows_updated != 1)
        {
          Log.Warning("Exception throw: updating balance.PlaySessionId: " + PlaySessionID.ToString());
          throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR, "PLAY_SESSION Not Updated.");
        }
      }

      return _account_id;

    } // DB_SetAccountBalance


    //------------------------------------------------------------------------------
    // PURPOSE: Sets the given balance to the account identified by the track data
    //
    //  PARAMS:
    //      - INPUT:
    //        - CardType: CJ | ALESIS
    //        - CardTrackData: Track data
    //        - Balance
    //
    //      - OUTPUT:
    //
    // RETURNS: The AccountID
    //        
    //   NOTES :
    //
    public static void DB_SetAccountBalance(Int64 AccountID,
                                            Decimal NewBalance,
                                            Boolean CheckPreviousBalance,
                                            Decimal OldBalance,
                                            SqlTransaction Trx)
    {
      SqlCommand sql_command;
      String sql_str;
      Int32 num_rows_affected;

      sql_str = "";
      sql_str += "UPDATE   ACCOUNTS ";
      sql_str += "   SET   AC_BALANCE         = @pNewBalance ";
      sql_str += "       , AC_LAST_ACTIVITY   = GETDATE() ";
      sql_str += " WHERE   AC_ACCOUNT_ID      = @pAccountID  ";

      if (CheckPreviousBalance)
      {
        sql_str += "   AND   AC_BALANCE         = @pOldBalance ";
      }

      sql_command = new SqlCommand(sql_str);
      sql_command.Connection = Trx.Connection;
      sql_command.Transaction = Trx;

      sql_command.Parameters.Add("@pAccountID", SqlDbType.BigInt).Value = AccountID;
      sql_command.Parameters.Add("@pNewBalance", SqlDbType.Money).Value = NewBalance;
      if (CheckPreviousBalance)
      {
        sql_command.Parameters.Add("@pOldBalance", SqlDbType.Money).Value = OldBalance;
      }

      num_rows_affected = 0;
      try
      {
        num_rows_affected = sql_command.ExecuteNonQuery();
      }
      catch (Exception ex)
      {
        // Track data already exists ! with type != 1 ??
        Log.Exception(ex);
      }

      if (num_rows_affected != 1)
      {
        Log.Warning("Account not found: " + AccountID);
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR, "Account not found.");
      }
    } // DB_SetAccountBalance

    //------------------------------------------------------------------------------
    // PURPOSE: Adds the given amount to the given account
    //
    //  PARAMS:
    //      - INPUT:
    //        - AccountID
    //        - Increment
    //
    // RETURNS: 
    //        
    //   NOTES :
    //
    public static void DB_AddToAccountBalance(Int64 AccountID,
                                              Decimal Increment,
                                              SqlTransaction Trx)
    {
      SqlCommand sql_command;
      String sql_str;
      Int32 num_rows_affected;

      sql_str = "";
      sql_str += "UPDATE   ACCOUNTS ";
      sql_str += "   SET   AC_BALANCE       = AC_BALANCE + @pIncrement ";
      sql_str += "       , AC_LAST_ACTIVITY = GETDATE() ";
      sql_str += " WHERE   AC_ACCOUNT_ID = @pAccountID ";

      sql_command = new SqlCommand(sql_str);
      sql_command.Connection = Trx.Connection;
      sql_command.Transaction = Trx;

      sql_command.Parameters.Add("@pAccountID", SqlDbType.BigInt).Value = AccountID;
      sql_command.Parameters.Add("@pIncrement", SqlDbType.Money).Value = Increment;

      num_rows_affected = 0;
      try
      {
        num_rows_affected = sql_command.ExecuteNonQuery();
      }
      catch (Exception ex)
      {
        // Track data already exists ! with type != 1 ??
        Log.Exception(ex);
      }

      if (num_rows_affected != 1)
      {
        Log.Warning("Account not found: " + AccountID);
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR, "Account not found.");
      }
    } // DB_AddToAccountBalance

    //------------------------------------------------------------------------------
    // PURPOSE: Read an account from database.
    //
    // PARAMS:
    //      - INPUT:
    //          - AccountID : Account id.
    //          - Trx : Sql transaction.
    //
    //      - OUTPUT:
    //          - DbTrackData : Track data obtained from database.
    //          - AccountType : Type of account provider:
    //                          ACCOUNT_UNKNOWN
    //                          ACCOUNT_CADILLAC_JACK
    //                          ACCOUNT_WIN
    //                          ACCOUNT_ALESIS
    //          - AccountBlocked : Boolean to indicate if the account is blocked.
    //          - AccountBalance : Current balance on database.
    //          - CurrentPlaySessionID : Play session id.
    //          - CurrentTerminalID : Terminal id. where the account holder is playing.
    //          - CardHolderName : Card holder name.
    //
    // RETURNS: 
    //    - true: Account exist.
    //    - false: Account does not exist.
    //        
    // NOTES :
    //
    public static Boolean DB_ReadAccount(Int64 AccountID,
                                         out String DbTrackData,
                                         out AccountType AccountType,
                                         out Boolean AccountBlocked,
                                         out Decimal AccountBalance,
                                         out Int64 CurrentPlaySessionID,
                                         out Int32 CurrentTerminalID,
                                         out String CardHolderName,
                                         out String CardHolderFirstName,
                                         out Decimal AccountPoints,
                                         out Int32 LastTerminalID,
                                         out TerminalTypes TerminalType,
                                         out DateTime CardHolderBirthDate,
                                         out Int32 Level,
                                         out String Pin,
                                         out Int32 PinFailures,
                                         out Boolean ReservedMode,
                                         SqlTransaction SqlTrx)
    {
      String _sql_txt;
      Int32 _logged_in_terminal_id;
      Object _obj;

      // Output params initialization
      DbTrackData = "";
      AccountType = AccountType.ACCOUNT_UNKNOWN;
      AccountBlocked = false;
      AccountBalance = 0;
      CurrentPlaySessionID = 0;
      CurrentTerminalID = 0;
      CardHolderName = "";
      CardHolderFirstName = "";
      AccountPoints = 0;
      LastTerminalID = 0;
      TerminalType = TerminalTypes.UNKNOWN;
      CardHolderBirthDate = DateTime.MinValue;
      Level = 0;
      Pin = "";
      PinFailures = 0;
      ReservedMode = false;

      try
      {
        _sql_txt = "SELECT   AC_TYPE                        " +
                   "       , AC_BLOCKED                     " +
                   "       , AC_BALANCE                     " +
                   "       , AC_CURRENT_PLAY_SESSION_ID     " +
                   "       , AC_CURRENT_TERMINAL_ID         " +
                   "       , AC_TRACK_DATA                  " +
                   "       , ISNULL(AC_HOLDER_NAME, '')     " +
                   "       , ISNULL (DBO.GETBUCKETVALUE(" + (Int32)Buckets.BucketId.RedemptionPoints + ", @pAccountID), 0) " +
                   "       , ISNULL(AC_LAST_TERMINAL_ID, 0) " +
                   "       , AC_HOLDER_BIRTH_DATE           " +
                   "       , ISNULL (AC_HOLDER_LEVEL, 1)    " +
                   "       , ISNULL (AC_PIN, '')            " +
                   "       , ISNULL (AC_PIN_FAILURES, 0)    " +
                   "       , ISNULL (AC_HOLDER_NAME3, '')   " +                   
                   "       , AC_MODE_RESERVED               " +                   
                   "  FROM   ACCOUNTS                       " +
                   " WHERE   AC_ACCOUNT_ID = @pAccountID    ";

        using (SqlCommand _sql_cmd = new SqlCommand(_sql_txt, SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pAccountID", SqlDbType.BigInt).Value = AccountID;

          using (SqlDataReader _sql_reader = _sql_cmd.ExecuteReader())
          {
            if (!_sql_reader.Read())
            {
              return false;
            }
            AccountType = (AccountType)_sql_reader.GetInt32(0);
            AccountBlocked = _sql_reader.GetBoolean(1);
            AccountBalance = _sql_reader.GetDecimal(2);
            if (!_sql_reader.IsDBNull(3)) { CurrentPlaySessionID = _sql_reader.GetInt64(3); }
            if (!_sql_reader.IsDBNull(4)) { CurrentTerminalID = _sql_reader.GetInt32(4); }
            DbTrackData = _sql_reader.GetString(5);
            CardHolderName = _sql_reader.GetString(6);
            AccountPoints = _sql_reader.GetDecimal(7);
            if (!_sql_reader.IsDBNull(8)) { LastTerminalID = _sql_reader.GetInt32(8); }
            if (!_sql_reader.IsDBNull(9)) { CardHolderBirthDate = _sql_reader.GetDateTime(9); }
            if (!_sql_reader.IsDBNull(10)) { Level = _sql_reader.GetInt32(10); }
            if (!_sql_reader.IsDBNull(11)) { Pin = _sql_reader.GetString(11); }
            if (!_sql_reader.IsDBNull(12)) { PinFailures = _sql_reader.GetInt32(12); }
            CardHolderFirstName = _sql_reader.GetString(13);
            ReservedMode = _sql_reader.GetBoolean(14);
          } // using _sql_reader
        } // using _sql_cmd

        _logged_in_terminal_id = 0;
        if (CurrentTerminalID > 0)
        {
          _logged_in_terminal_id = CurrentTerminalID;
        }
        else if (LastTerminalID > 0)
        {
          _logged_in_terminal_id = LastTerminalID;
        }

        if (_logged_in_terminal_id > 0)
        {
          // Get Terminal type
          _sql_txt = "SELECT   TE_TERMINAL_TYPE " +
                     "  FROM   TERMINALS " +
                     " WHERE   TE_TERMINAL_ID = @pTerminalId ";

          using (SqlCommand _sql_cmd = new SqlCommand(_sql_txt, SqlTrx.Connection, SqlTrx))
          {
            _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = _logged_in_terminal_id;
            _obj = _sql_cmd.ExecuteScalar();
            if (_obj != null && _obj != DBNull.Value)
            {
              TerminalType = (TerminalTypes)((Int16)_obj);
            }
          }
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
        Log.Warning("Exception throw: Error reading Cards. AccountID: " + AccountID.ToString());

        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR, "DB Error reading cards.");
      }

      return true;
    } // DB_ReadAccount

    //------------------------------------------------------------------------------
    // PURPOSE: Read an account from database.
    //
    // PARAMS:
    //      - INPUT:
    //          - TrackData : Track data to read from database.
    //          - Trx : Sql transaction.
    //
    //      - OUTPUT:
    //          - DbTrackData : Track data obtained from database.
    //          - AccountID : Account id.
    //          - AccountType : Type of account provider:
    //                          ACCOUNT_UNKNOWN
    //                          ACCOUNT_CADILLAC_JACK
    //                          ACCOUNT_WIN
    //                          ACCOUNT_ALESIS
    //          - AccountBlocked : Boolean to indicate if the account is blocked.
    //          - AccountBalance : Current balance on database.
    //          - CurrentPlaySessionID : Play session id.
    //          - CurrentTerminalID : Terminal id. where the account holder is playing.
    //          - CardHolderName : Card holder name.
    //
    // RETURNS: 
    //    - true: Account exist.
    //    - false: Account does not exist.
    //        
    // NOTES :
    //
    public static Boolean DB_ReadAccount(String TrackData,
                                         out String DbTrackData,
                                         out Int64 AccountID,
                                         out AccountType AccountType,
                                         out Boolean AccountBlocked,
                                         out Decimal AccountBalance,
                                         out Int64 CurrentPlaySessionID,
                                         out Int32 CurrentTerminalID,
                                         out String CardHolderName,
                                         out String CardHolderFirstName,
                                         out Decimal AccountPoints,
                                         out Int32 LastTerminalID,
                                         out TerminalTypes TerminalType,
                                         out DateTime CardHolderBirthDate,
                                         out Int32 Level,
                                         out String Pin,
                                         out Int32 PinFailures,
                                         out Boolean ReservedMode,
                                         SqlTransaction Trx)
    {
      SqlCommand sql_command;
      SqlDataReader reader;
      String sql_str;

      String win_track_data;
      Boolean create_new_account;
      AccountType new_account_type;

      // Output params initialization
      DbTrackData = "";
      AccountID = 0;
      AccountType = AccountType.ACCOUNT_UNKNOWN;
      AccountBlocked = false;
      AccountBalance = 0;
      CurrentPlaySessionID = 0;
      CurrentTerminalID = 0;
      CardHolderName = "";
      CardHolderFirstName = "";
      AccountPoints = 0;
      LastTerminalID = 0;
      TerminalType = TerminalTypes.UNKNOWN;
      CardHolderBirthDate = DateTime.MinValue;
      Level = 0;
      Pin = "";
      PinFailures = 0;
      ReservedMode = false;

      // Variables initialization
      win_track_data = "NOT SET";
      create_new_account = false;
      new_account_type = AccountType.ACCOUNT_UNKNOWN;

      if (WCP_BusinessLogic.CashlessMode == CashlessMode.WIN)
      {
        if (!CardNumber.ConvertTrackData(WSI.Common.CardTrackData.CriptMode.WCP, TrackData, out win_track_data))
        {
          if (WCP_BusinessLogic.CashlessMode == CashlessMode.WIN)
          {
            Log.Warning("Invalid Track Number: " + TrackData);

            throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_CARD_NOT_VALID, "Invalid Track Number");
          }
        }
      }

      sql_command = new SqlCommand();
      sql_command.Connection = Trx.Connection;
      sql_command.Transaction = Trx;

      sql_str = "SELECT   AC_ACCOUNT_ID               " +
                "  FROM   ACCOUNTS                    " +
                " WHERE   " +
                "        ( AC_TRACK_DATA = @p1 AND AC_TYPE = @p2 ) ";

      // RRR 15-NOV-2013: TODO REVISE
      // TITO MODE
      if (Common.TITO.Utils.IsTitoMode())
      {
        sql_str = "SELECT   AC_ACCOUNT_ID " +
          "  FROM   ACCOUNTS " +
          " WHERE   " +
          "        ( AC_TRACK_DATA = @p1) ";
      }

      switch (WCP_BusinessLogic.CashlessMode)
      {
        case CashlessMode.WIN:
          sql_command.Parameters.Add("@p1", SqlDbType.NVarChar, 50).Value = win_track_data;
          sql_command.Parameters.Add("@p2", SqlDbType.Int).Value = (Int32)AccountType.ACCOUNT_WIN;
          break;

        case CashlessMode.ALESIS:
          sql_command.Parameters.Add("@p1", SqlDbType.NVarChar, 50).Value = TrackData;
          sql_command.Parameters.Add("@p2", SqlDbType.Int).Value = (Int32)AccountType.ACCOUNT_ALESIS;
          create_new_account = true;
          new_account_type = AccountType.ACCOUNT_ALESIS;
          break;

        default:
          Log.Warning("Invalid CashlessMode: " + WCP_BusinessLogic.CashlessMode.ToString());

          throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_CARD_NOT_VALID, "Invalid CashlessMode");
      }

      reader = null;

      try
      {
        sql_command.CommandText = sql_str;
        reader = sql_command.ExecuteReader();
        if (reader.Read() && !reader.IsDBNull(0))
        {
          AccountID = (Int64)reader.GetInt64(0);
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
        Log.Warning("Exception throw: Error reading Cards. CardTrackData: " + TrackData);

        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR, "DB Error reading cards.");
      }
      finally
      {
        if (reader != null)
        {
          reader.Close();
          reader.Dispose();
          reader = null;
        }
      }

      if (AccountID == 0
          && create_new_account)
      {
        AccountID = DB_SetAccountBalance(new_account_type, TrackData, 0, 0, Trx);
        AccountType = new_account_type;
      }

      if (AccountID == 0)
      {
        return false;
      }

      return DB_ReadAccount(AccountID,
                            out DbTrackData,
                            out AccountType,
                            out AccountBlocked,
                            out AccountBalance,
                            out CurrentPlaySessionID,
                            out CurrentTerminalID,
                            out CardHolderName,
                            out CardHolderFirstName,
                            out AccountPoints,
                            out LastTerminalID,
                            out TerminalType,
                            out CardHolderBirthDate,
                            out Level,
                            out Pin,
                            out PinFailures,
                            out ReservedMode,
                            Trx);

    } // DB_ReadAccount

    //------------------------------------------------------------------------------
    // PURPOSE: Mark Account as Blocked indicating the reason why.
    //
    //  PARAMS:
    //      - INPUT:
    //          - AccountId
    //          - BlockReason
    //          - SqlTrx
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //        
    //   NOTES :
    //
    public static void DB_SetAccountBlocked(Int64 AccountId,
                                            AccountBlockReason BlockReason,
                                            SqlTransaction SqlTrx)
    {
      try
      {
        if (MultiPromos.Trx_SetAccountBlocked(AccountId, BlockReason, SqlTrx))
        {
          // Account blocked correctly. Return.
          return;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      Log.Warning("DB_SetAccountBlocked. Account not found: " + AccountId);

    } // DB_SetAccountBlocked

    //------------------------------------------------------------------------------
    // PURPOSE: Check account PIN and mark failures.
    //
    //  PARAMS:
    //      - INPUT:
    //        - AccountID
    //        - PinToCheck
    //
    //      - OUTPUT:
    //
    // RETURNS: WCP_ResponseCodes
    //        
    //   NOTES :
    //
    public static WCP_ResponseCodes DB_CheckAccountPIN(Int64 AccountID, String PinToCheck)
    {
      WCP_ResponseCodes _rc;
      PinCheckStatus _pin_status;

      _rc = WCP_ResponseCodes.WCP_RC_ERROR;

      _pin_status = Accounts.DB_CheckAccountPIN(AccountID, PinToCheck);

      switch (_pin_status)
      {
        case PinCheckStatus.OK:
          _rc = WCP_ResponseCodes.WCP_RC_OK;
          break;

        case PinCheckStatus.WRONG_PIN:
          _rc = WCP_ResponseCodes.WCP_RC_CARD_WRONG_PIN;
          break;

        case PinCheckStatus.ACCOUNT_BLOCKED:
          _rc = WCP_ResponseCodes.WCP_RC_CARD_BLOCKED;
          break;

        case PinCheckStatus.ERROR:
        default:
          _rc = WCP_ResponseCodes.WCP_RC_ERROR;
          break;
      }

      return _rc;
    } // DB_CheckAccountPIN

    //------------------------------------------------------------------------------
    // PURPOSE : Checks if a mobile bank account exist on database. 
    //
    // PARAMS:
    //      - INPUT:
    //          - TrackData
    //          - Trx
    //
    //      - OUTPUT:
    //
    // RETURNS : 
    //      - true: Account exist.
    //      - false: Account does not exist.
    //        
    // NOTES :
    //
    public static Boolean DB_MobileBankAccountExist(String TrackData, SqlTransaction Trx)
    {
      SqlCommand sql_command;
      SqlDataReader reader;
      String sql_str;
      Int32 num_accounts;
      String win_track_data;

      num_accounts = 0;

      sql_command = new SqlCommand();
      sql_command.Connection = Trx.Connection;
      sql_command.Transaction = Trx;

      win_track_data = "NOT SET";
      if (!CardNumber.ConvertTrackData(WSI.Common.CardTrackData.CriptMode.WCP, TrackData, out win_track_data))
      {
        return false;
      }

      sql_str = "SELECT  COUNT(*)                " +
                "  FROM  MOBILE_BANKS            " +
                " WHERE  ( MB_TRACK_DATA = @p1 ) ";

      sql_command.Parameters.Add("@p1", SqlDbType.NVarChar, 50).Value = win_track_data;

      reader = null;

      try
      {
        sql_command.CommandText = sql_str;
        reader = sql_command.ExecuteReader();
        if (reader.Read() && !reader.IsDBNull(0))
        {
          num_accounts = (Int32)reader.GetInt32(0);
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
        Log.Warning("Exception throw: Error reading Mobile Bank Card. CardTrackData: " + TrackData);
      }
      finally
      {
        if (reader != null)
        {
          reader.Close();
          reader.Dispose();
          reader = null;
        }
      }

      if (num_accounts != 1)
      {
        return false;
      }

      return true;
    } // DB_MobileBankAccountExist

    //------------------------------------------------------------------------------
    // PURPOSE: Check mobile bank card
    //
    //  PARAMS:
    //      - INPUT:
    //          - TrackData
    //          - AmountToTransferx100
    //          - AuthorizedAmountx100
    //          - Trx
    //
    //      - OUTPUT:
    //          - AuthorizedAmount
    //
    // RETURNS: 
    //        
    //   NOTES :
    //
    public static WCP_ResponseCodes DB_MobileBankAccountCheck(String TrackData
                                                            , String Pin
                                                            , Int64 AmountToTransferx100
                                                            , out Int64 AuthorizedAmountx100
                                                            , SqlTransaction Trx)
    {
      //SqlCommand _sql_command;
      //SqlDataReader _reader;
      StringBuilder _sql_str;
      Boolean _blocked;
      String _pin;
      Int16 _failed_login_attempts;
      Int16 _new_pin_failed;
      String _win_track_data;
      Int32 _num_rows_updated;

      Int64 _total_limit_amount_x_100;
      Int64 _recharge_limit_amount_x_100;
      Int32 _number_of_recharges_limit;
      Int64 _actual_total_recharges_amount_x_100;
      Int32 _actual_number_of_recharges;
      Int64 _mb_account_id;

      _win_track_data = "NOT SET";
      if (!CardNumber.ConvertTrackData(WSI.Common.CardTrackData.CriptMode.WCP, TrackData, out _win_track_data))
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_CARD_NOT_VALID, "Invalid Track Number");
      }

      _blocked = true;
      _pin = "";
      _failed_login_attempts = 0;
      AuthorizedAmountx100 = 0;

      _total_limit_amount_x_100 = 0;
      _recharge_limit_amount_x_100 = 0;
      _number_of_recharges_limit = 0;
      _actual_total_recharges_amount_x_100 = 0;
      _actual_number_of_recharges = 0;
      _mb_account_id = 0;

      _sql_str = new StringBuilder();
      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          _sql_str.AppendLine("SELECT   MB_BLOCKED                    ");
          _sql_str.AppendLine("       , MB_PIN                        ");
          _sql_str.AppendLine("       , MB_FAILED_LOGIN_ATTEMPTS      ");
          _sql_str.AppendLine("       , MB_BALANCE                    ");
          /* JML */
          _sql_str.AppendLine("       , MB_TOTAL_LIMIT                ");
          _sql_str.AppendLine("       , MB_RECHARGE_LIMIT             ");
          _sql_str.AppendLine("       , MB_NUMBER_OF_RECHARGES_LIMIT  ");
          _sql_str.AppendLine("       , MB_CASH_IN                    ");
          _sql_str.AppendLine("       , MB_ACTUAL_NUMBER_OF_RECHARGES ");
          _sql_str.AppendLine("       , MB_ACCOUNT_ID                 ");
          _sql_str.AppendLine("  FROM   MOBILE_BANKS                  ");
          _sql_str.AppendLine(" WHERE   MB_TRACK_DATA = @p1          ");

          using (SqlCommand _sql_command = new SqlCommand(_sql_str.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_command.Parameters.Add("@p1", SqlDbType.NVarChar, 50).Value = _win_track_data;
            _sql_command.Parameters.Add("@p2", SqlDbType.Int, 50).Value = MBMovementType.TransferCredit;

            using (SqlDataReader _reader = _sql_command.ExecuteReader())
            {
              if (_reader.Read())
              {
                if (!_reader.IsDBNull(0)) { _blocked = _reader.GetBoolean(0); }
                if (!_reader.IsDBNull(1)) { _pin = _reader.GetString(1); }
                if (!_reader.IsDBNull(2)) { _failed_login_attempts = _reader.GetInt16(2); }
                if (!_reader.IsDBNull(3)) { AuthorizedAmountx100 = (Int64)(_reader.GetDecimal(3) * 100); }
                // JML 
                _total_limit_amount_x_100 = (_reader.IsDBNull(4) ? 0 : (Int64)(_reader.GetDecimal(4) * 100));
                _recharge_limit_amount_x_100 = (_reader.IsDBNull(5) ? 0 : (Int64)(_reader.GetDecimal(5) * 100));
                _number_of_recharges_limit = (_reader.IsDBNull(6) ? 0 : _reader.GetInt32(6));

                _actual_total_recharges_amount_x_100 = (_reader.IsDBNull(7) ? 0 : (Int64)(_reader.GetDecimal(7) * 100));
                _actual_number_of_recharges = (_reader.IsDBNull(8) ? 0 : _reader.GetInt32(8));
                _mb_account_id = (_reader.IsDBNull(9)) ? 0 : (Int64)(_reader.GetInt64(9));
              }
            }
          }
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
        Log.Warning("Exception throw: Error reading Mobile Bank Card. CardTrackData: " + TrackData);
      }

      //
      // Check Blocked
      //
      if (_blocked)
      {
        AuthorizedAmountx100 = 0;
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_CARD_BLOCKED, "Mobile bank Card blocked.");
      }

      // Set failed login attempts
      if (Pin != _pin)
      {
        _new_pin_failed = _failed_login_attempts;
        _new_pin_failed++;
      }
      else
      {
        _new_pin_failed = 0;
      }

      //
      // Update failed_login_attempts
      //
      if (_new_pin_failed != _failed_login_attempts)
      {
        if (_new_pin_failed >= 3)
        {
          _new_pin_failed = 3;
          _blocked = true;
        }

        try
        {
          using (DB_TRX _db_trx = new DB_TRX())
          {
            _sql_str = new StringBuilder();
            _sql_str.AppendLine("UPDATE   MOBILE_BANKS                   ");
            _sql_str.AppendLine("   SET   MB_FAILED_LOGIN_ATTEMPTS = @p1 ");
            _sql_str.AppendLine("       , MB_BLOCKED  = @p2              ");
            _sql_str.AppendLine(" WHERE   MB_TRACK_DATA = @p3            ");

            using (SqlCommand _sql_command = new SqlCommand(_sql_str.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
            {
              _sql_command.Parameters.Add("@p1", SqlDbType.SmallInt).Value = _new_pin_failed;
              _sql_command.Parameters.Add("@p2", SqlDbType.Bit, 1, "MB_BLOCKED").Value = _blocked;
              _sql_command.Parameters.Add("@p3", SqlDbType.NVarChar, 50).Value = _win_track_data;

              _num_rows_updated = _sql_command.ExecuteNonQuery();

              if (_num_rows_updated != 1)
              {
                Log.Warning("Exception throw: DB_MobileBankAccountCheck. TrackData: " + TrackData);
                throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR, "DB Error Mobile Bank updating login failed.");
              }
            }
          }
        }
        catch (Exception ex)
        {
          Log.Exception(ex);
          Log.Warning("Exception throw: DB_MobileBankAccountCheck. TrackData: " + TrackData);
          throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR, "DB Error Mobile Bank update failed login.");
        }
      }

      //
      // Check PIN
      //
      if (Pin != _pin)
      {
        AuthorizedAmountx100 = 0;
        return WCP_ResponseCodes.WCP_RC_CARD_WRONG_PIN;
      }

      //
      // Check Amount To Transfer
      //
      if (AmountToTransferx100 > AuthorizedAmountx100)
      {
        return WCP_ResponseCodes.WCP_RC_INVALID_AMOUNT;
      }
      if (_total_limit_amount_x_100 > 0 && (_actual_total_recharges_amount_x_100 + AmountToTransferx100) > _total_limit_amount_x_100)
      {
        return WCP_ResponseCodes.WCP_RC_MB_EXCEEDED_TOTAL_RECHARGES_LIMIT;
      }
      if (_recharge_limit_amount_x_100 > 0 && AmountToTransferx100 > _recharge_limit_amount_x_100)
      {
        return WCP_ResponseCodes.WCP_RC_MB_EXCEEDED_RECHARGE_LIMIT;
      }
      if (_number_of_recharges_limit > 0 && (_actual_number_of_recharges + 1) > _number_of_recharges_limit)
      {
        return WCP_ResponseCodes.WCP_RC_MB_EXCEEDED_RECHARGES_NUMBER_LIMIT;
      }
      if (MobileBank.DB_IsClosedSesionMB(_mb_account_id, Trx))
      {
        return WCP_ResponseCodes.WCP_RC_MB_SESSION_CLOSED;
      } 

      return WCP_ResponseCodes.WCP_RC_OK;

    } // DB_MobileBankAccountCheck

    //------------------------------------------------------------------------------
    // PURPOSE: Mobile Bank Account transfer of funds to a Player Account.
    //
    // PARAMS:
    //    - INPUT:
    //          - MobileBankTrackData : Mobile bank account track data.
    //          - PlayerTrackData : Player account track data.
    //          - TerminalId : Terminal id. where executed the operation of funds transfer.
    //          - TerminalName : Terminal name.
    //          - AmountToTransferx100 : Amount to transfer.
    //          - WcpSequenceId : Sequence id.
    //          - WcpTransactionId : Transaction id.
    //          - Trx : Sql transaction.
    //
    //    - OUTPUT:
    //          - PreviousAccountBalance : Previous account balance.
    //          - CurrentAccountBalance : Current account balance.
    //
    // RETURNS: 
    //        
    // NOTES :
    //  Steps related to mobile bank funds transfer:
    //  1. Check amount to transfer.
    //  2. Transfer amount to account. 
    //
    public static void DB_MobileBankAccountTransfer(String MobileBankTrackData,
                                                    String PlayerTrackData,
                                                    Int32 TerminalId,
                                                    String TerminalName,
                                                    Int64 AmountToTransferx100,
                                                    Int64 WcpSequenceId,
                                                    Int64 WcpTransactionId,
                                                    out Decimal PreviousAccountBalance,
                                                    out Decimal CurrentAccountBalance,
                                                    SqlTransaction Trx)
    {

      Decimal _amount_to_transfer;

      WCP_AccountManager.WCP_Account _account = null;

      Currency _promotion_amount = 0;
      Currency _spent_used = 0;
      ArrayList _dummy = new ArrayList();

      // Output parameters initialization
      PreviousAccountBalance = 0;
      CurrentAccountBalance = 0;

      _amount_to_transfer = ((Decimal)AmountToTransferx100) / 100;

      //
      // 1. Check amount to transfer
      //
      if (_amount_to_transfer == 0)
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_INVALID_AMOUNT, "Amount to transfer cannot be zero");
      }

      // 2. Transfer amount to account
      // SSC 29-FEB-2012: Common part moved to WCP_TranferAmounts.
      WCP_TransferAmounts.DB_TransferAmountToPlayerAccount(MobileBankTrackData,
                                                           PlayerTrackData,
                                                           TerminalId,
                                                           TerminalName,
                                                           0,
                                                           _amount_to_transfer,
                                                           WcpSequenceId,
                                                           WcpTransactionId,
                                                           out PreviousAccountBalance,
                                                           out CurrentAccountBalance,
                                                           out _dummy,
                                                           Trx,
                                                           _account,
                                                           MB_USER_TYPE.PERSONAL);


    } // DB_MobileBankAccountTransfer


    //------------------------------------------------------------------------------
    // PURPOSE: mobile bank account transfer to player account
    //
    //  PARAMS:
    //      - INPUT:
    //          - CashierSessionId
    //          - MbAccountId
    //          - TerminalId
    //          - WcpSequenceId
    //          - WcpTransactionId
    //          - PlayerAccountId
    //          - PlayerTrackData
    //          - PlayerAccountMovementId
    //          - Type
    //          - InitialBalance
    //          - SubAmount
    //          - AddAmount
    //          - FinalBalance
    //          - Trx
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    //        
    //   NOTES :
    //
    public static void DB_MobileBankInsertMovement(Int64 CashierSessionId,
                                                   Int64 MbAccountId,
                                                   Int32 TerminalId,
                                                   Int64 WcpSequenceId,
                                                   Int64 WcpTransactionId,
                                                   Int64 PlayerAccountId,
                                                   String WinPlayerTrackData,
                                                   Int64 PlayerAccountMovementId,
                                                   MBMovementType Type,
                                                   Decimal InitialBalance,
                                                   Decimal SubAmount,
                                                   Decimal AddAmount,
                                                   Decimal FinalBalance,
                                                   Int64 OperationId,
                                                   SqlTransaction Trx)
    {
      String _sql_str;
      String _terminal_name;
      Object _obj;
      TYPE_SPLITS _splits;

      if (Type == MBMovementType.TransferCredit)
      {
        if (!Split.ReadSplitParameters(out _splits))
        {
          Log.Error("Split.ReadSplitParameters failed!");

          throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR, "MOVEMENT Not Inserted.");
        }
      }
      else
      {
        _splits = new TYPE_SPLITS();
      }

      _terminal_name = "";

      _sql_str = "SELECT TE_NAME FROM TERMINALS WHERE TE_TERMINAL_ID = @p1 ";

      using (SqlCommand _cmd = new SqlCommand(_sql_str, Trx.Connection, Trx))
      {
        _cmd.Parameters.Add("@p1", SqlDbType.Int).Value = TerminalId;

        try
        {
          _obj = _cmd.ExecuteScalar();
          if (_obj != null)
          {
            _terminal_name = (String)_obj;
          }
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }
      }

      if (!DB_MobileBankInsertMovement(CashierSessionId, MbAccountId, TerminalId, _terminal_name, WcpSequenceId, WcpTransactionId, PlayerAccountId,
                                       WinPlayerTrackData, PlayerAccountMovementId, Type, InitialBalance, SubAmount, AddAmount, FinalBalance,
                                       OperationId, _splits, Trx))
      {
        Log.Warning("Exception throw: inserting MB movement CashierSessionId: " + CashierSessionId.ToString() + " TerminalId: " + TerminalId.ToString() + " MbAccountId: " + MbAccountId.ToString() + " PlayerAccountId: " + PlayerAccountId.ToString());

        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR, "MOVEMENT Not Inserted.");
      }

    } // DB_MobileBankInsertMovement

    public static Boolean DB_MobileBankInsertMovement(Int64 CashierSessionId,
                                                      Int64 MbAccountId,
                                                      Int32 TerminalId,
                                                      String TerminalName,
                                                      Int64 WcpSequenceId,
                                                      Int64 WcpTransactionId,
                                                      Int64 PlayerAccountId,
                                                      String WinPlayerTrackData,
                                                      Int64 PlayerAccountMovementId,
                                                      MBMovementType Type,
                                                      Decimal InitialBalance,
                                                      Decimal SubAmount,
                                                      Decimal AddAmount,
                                                      Decimal FinalBalance,
                                                      Int64 OperationId,
                                                      TYPE_SPLITS Splits,
                                                      SqlTransaction Trx)
    {
      return DB_MobileBankInsertMovement(CashierSessionId,
                                                      MbAccountId,
                                                      TerminalId,
                                                      TerminalName,
                                                      WcpSequenceId,
                                                      WcpTransactionId,
                                                      PlayerAccountId,
                                                      WinPlayerTrackData,
                                                      PlayerAccountMovementId,
                                                      Type,
                                                      InitialBalance,
                                                      SubAmount,
                                                      AddAmount,
                                                      FinalBalance,
                                                      OperationId,
                                                      Splits,
                                                      "",
                                                      Trx);
    }

    public static Boolean DB_MobileBankInsertMovement(Int64 CashierSessionId,
                                                      Int64 MbAccountId,
                                                      Int32 TerminalId,
                                                      String TerminalName,
                                                      Int64 WcpSequenceId,
                                                      Int64 WcpTransactionId,
                                                      Int64 PlayerAccountId,
                                                      String WinPlayerTrackData,
                                                      Int64 PlayerAccountMovementId,
                                                      MBMovementType Type,
                                                      Decimal InitialBalance,
                                                      Decimal SubAmount,
                                                      Decimal AddAmount,
                                                      Decimal FinalBalance,
                                                      Int64 OperationId,
                                                      TYPE_SPLITS Splits,
                                                      String IsoCode,
                                                      SqlTransaction Trx)
    {
      String _sql_str;

      try
      {
        _sql_str = " DECLARE @_cm_movement_id AS BIGINT" +
                   " DECLARE @_datetime       AS DATETIME" +
                   " SET @_datetime = GETDATE() " +
                   " INSERT INTO MB_MOVEMENTS  (MBM_TYPE " +
                                            ", MBM_DATETIME " +
                                            ", MBM_CASHIER_SESSION_ID " +
                                            ", MBM_MB_ID " +
                                            ", MBM_TERMINAL_ID " +
                                            ", MBM_TERMINAL_NAME " +
                                            ", MBM_PLAYER_ACCT_ID " +
                                            ", MBM_PLAYER_TRACKDATA " +
                                            ", MBM_WCP_SEQUENCE_ID " +
                                            ", MBM_WCP_TRANSACTION_ID " +
                                            ", MBM_ACCT_MOVEMENT_ID " +
                                            ", MBM_INITIAL_BALANCE " +
                                            ", MBM_ADD_AMOUNT " +
                                            ", MBM_SUB_AMOUNT " +
                                            ", MBM_FINAL_BALANCE " +
                                            ", MBM_OPERATION_ID " +
                                            ", MBM_AMOUNT_01 " +
                                            ", MBM_AMOUNT_02) " +
                                      "VALUES (@p1, @_datetime, @p2, @p3, @p4, @p5, @p6, @p7, @p8, @p9, @p10, @p11, @p12, @p13, @p14, @OperationId, @pAmount1, @pAmount2) " +

                   // RCI & RMS 10-FEB-2014: The historicize by hour will be made in a separate thread.
          //                        The movements to historicize are in table CASHIER_MOVEMENTS_PENDING_HISTORY.
                   " SET   @_cm_movement_id = SCOPE_IDENTITY() " +
                   "EXEC dbo.CashierMovementsHistory @p2, @_cm_movement_id, @p1, 1, 0, @p12, @p13, 0, @pCurrencyCode, 0, 0, 0 ";

        using (SqlCommand _cmd = new SqlCommand(_sql_str, Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@p1", SqlDbType.Int).Value = (Int32)Type;
          _cmd.Parameters.Add("@p2", SqlDbType.BigInt).Value = CashierSessionId;
          _cmd.Parameters.Add("@p3", SqlDbType.BigInt).Value = MbAccountId;
          _cmd.Parameters.Add("@p4", SqlDbType.Int).Value = TerminalId;
          _cmd.Parameters.Add("@p5", SqlDbType.NVarChar, 50).Value = TerminalName;
          _cmd.Parameters.Add("@p6", SqlDbType.BigInt).Value = PlayerAccountId;
          _cmd.Parameters.Add("@p7", SqlDbType.NVarChar, 50).Value = WinPlayerTrackData;
          _cmd.Parameters.Add("@p8", SqlDbType.BigInt).Value = WcpSequenceId;
          _cmd.Parameters.Add("@p9", SqlDbType.BigInt).Value = WcpTransactionId;
          _cmd.Parameters.Add("@p10", SqlDbType.BigInt).Value = PlayerAccountMovementId;
          _cmd.Parameters.Add("@p11", SqlDbType.Money).Value = InitialBalance;
          _cmd.Parameters.Add("@p12", SqlDbType.Money).Value = AddAmount;
          _cmd.Parameters.Add("@p13", SqlDbType.Money).Value = SubAmount;
          _cmd.Parameters.Add("@p14", SqlDbType.Money).Value = FinalBalance;

          if (Misc.IsFloorDualCurrencyEnabled())
          {
            _cmd.Parameters.Add("@pCurrencyCode", SqlDbType.NVarChar).Value = IsoCode;
          }
          else
          {
          _cmd.Parameters.Add("@pCurrencyCode", SqlDbType.NVarChar).Value = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode");
          }

          if (Type == MBMovementType.TransferCredit)
          {
            Decimal _split1;

            _split1 = Math.Round(Splits.company_a.cashin_pct * SubAmount / 100.00m, 2, MidpointRounding.AwayFromZero);
            _cmd.Parameters.Add("@pAmount1", SqlDbType.Money).Value = _split1;
            _cmd.Parameters.Add("@pAmount2", SqlDbType.Money).Value = SubAmount - _split1;
          }
          else
          {
            _cmd.Parameters.Add("@pAmount1", SqlDbType.Money).Value = DBNull.Value;
            _cmd.Parameters.Add("@pAmount2", SqlDbType.Money).Value = DBNull.Value;
          }

          if (OperationId == 0)
          {
            _cmd.Parameters.Add("@OperationId", SqlDbType.BigInt).Value = DBNull.Value;
          }
          else
          {
            _cmd.Parameters.Add("@OperationId", SqlDbType.BigInt).Value = OperationId;
          }

          return (_cmd.ExecuteNonQuery() > 0);
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Set mobile bank last terminal 
    //
    //  PARAMS:
    //      - INPUT:
    //          - TrackData
    //          - TerminalId
    //          - TerminalName
    //          - Trx
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    //        
    //   NOTES :
    //
    public static void DB_MobileBankSetTerminal(String TrackData, Int32 TerminalId, String TerminalName, SqlTransaction Trx)
    {
      SqlCommand sql_command;
      String sql_str;

      //
      // Update Terminal 
      //
      sql_str = "UPDATE MOBILE_BANKS SET MB_LAST_TERMINAL_ID    = @p1   " +
                                 "     , MB_LAST_TERMINAL_NAME  = @p2   " +
                                 " WHERE MB_TRACK_DATA = @p3            ";

      sql_command = new SqlCommand(sql_str);
      sql_command.Connection = Trx.Connection;
      sql_command.Transaction = Trx;
      sql_command.Parameters.Add("@p1", SqlDbType.Int).Value = TerminalId;
      sql_command.Parameters.Add("@p2", SqlDbType.NVarChar, 50).Value = TerminalName;
      sql_command.Parameters.Add("@p3", SqlDbType.NVarChar, 50).Value = TrackData;
      sql_command.ExecuteNonQuery();

    } // DB_MobileBankSetTerminal

    /// <summary>
    /// Get Card Data
    /// </summary>
    /// <param name="CardName"></param>
    /// <param name="TrackData"></param>
    /// <param name="CardId"></param>
    /// <param name="Balance"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    public static void DB_UpdateAccountBalance(Int64 CardId,
                                               Int64 PlaySessionId,
                                               Int32 TerminalId,
                                               Decimal PlayedAmount,
                                               Decimal WonAmount,
                                               SqlTransaction Trx)
    {
      SqlCommand sql_command;
      SqlDataReader reader;
      String sql_str;
      Int32 num_rows_updated;
      Decimal balance;

      //
      // Search Card
      //
      sql_str = "SELECT AC_BALANCE ";
      sql_str += " FROM ACCOUNTS " +
                " WHERE AC_ACCOUNT_ID = @p1 ";

      sql_command = new SqlCommand(sql_str);
      sql_command.Connection = Trx.Connection;
      sql_command.Transaction = Trx;

      sql_command.Parameters.Add("@p1", SqlDbType.BigInt, 8, "AC_ACCOUNT_ID").Value = CardId;

      reader = null;
      balance = 0;

      try
      {
        reader = sql_command.ExecuteReader();
        if (reader.Read() && !reader.IsDBNull(0))
        {
          balance = (Decimal)reader[0];
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
        Log.Warning("Exception throw: Error reading Cards. CardId: " + CardId.ToString());
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR, "DB Error reading cards.");
      }
      finally
      {
        if (reader != null)
        {
          reader.Close();
          reader.Dispose();
          reader = null;
        }
      }

      if (PlayedAmount > balance)
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR, "Balance mismatch in Database.");
      }

      balance -= PlayedAmount;
      balance += WonAmount;

      //
      // Update cards
      //
      sql_str = "          UPDATE ACCOUNTS " +
                            " SET AC_BALANCE        = @p1 " +
                               ", AC_LAST_ACTIVITY  = GETDATE() ";
      sql_str += "          WHERE AC_ACCOUNT_ID = @pAccountID " +
         " AND AC_CURRENT_TERMINAL_ID = @pTerminalID" +
         " AND AC_CURRENT_PLAY_SESSION_ID = @pPlaySessionID";

      sql_command = new SqlCommand(sql_str);
      sql_command.Connection = Trx.Connection;
      sql_command.Transaction = Trx;

      sql_command.Parameters.Add("@p1", SqlDbType.Money, 8, "AC_BALANCE").Value = balance;
      sql_command.Parameters.Add("@pAccountID", SqlDbType.BigInt).Value = CardId;
      sql_command.Parameters.Add("@pTerminalID", SqlDbType.Int).Value = TerminalId;
      sql_command.Parameters.Add("@pPlaySessionID", SqlDbType.BigInt).Value = PlaySessionId;

      num_rows_updated = sql_command.ExecuteNonQuery();

      if (num_rows_updated != 1)
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_CARD_NOT_VALIDATED, "Card/Terminal/PlaySession Not valid.");
      }

    } // DB_UpdateAccountBalance

    #endregion Cards

    #region Plays

    //------------------------------------------------------------------------------
    // PURPOSE : Check if new version is available
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //      - TRUE: Play already exist, return TRX -> OK
    //      - FALSE: play inserted.
    //
    public static Boolean DB_InsertPlay(Int64 PlaySessionId,
                                        Int32 TerminalId,
                                        Int64 SequenceId,
                                        Int64 TransactionId,
                                        String GameId,
                                        Decimal InitialBalance,
                                        Decimal PlayedAmount,
                                        Decimal WonAmount,
                                        Decimal FinalBalance,
                                        SqlTransaction Trx)
    {
      SqlCommand _sql_command;
      String sql_str;
      Int32 _num_rows_affected;
      Int32 _game_id;
      Int64 _account_id;
      Boolean stand_alone_session;
      Boolean _insert_play;
      Object _obj;

      stand_alone_session = WCP_BusinessLogic.DB_PlayStandAlone(TerminalId, out _insert_play, Trx);
      if (!_insert_play)
      {
        // Don't insert play
        return false;
      }

      _game_id = 0;
      _account_id = 0;
      _num_rows_affected = 0;

      // Get game id from the game name.
      sql_str = "SELECT GM_GAME_ID FROM GAMES WHERE GM_NAME = @p1";

      _sql_command = new SqlCommand(sql_str);
      _sql_command.Connection = Trx.Connection;
      _sql_command.Transaction = Trx;
      _sql_command.Parameters.Add("@p1", SqlDbType.NVarChar, 50, "GM_NAME").Value = GameId;

      try
      {
        _obj = _sql_command.ExecuteScalar();
        if (_obj != null)
        {
          _game_id = (Int32)_obj;
        }
      }
      catch (Exception _ex)
      {
        Log.Error("DB_InsertPlay: Error getting game code. GameName: " + GameId);
        Log.Exception(_ex);
      }

      if (PlaySessionId != 0)
      {
        // Get account ID
        sql_str = "SELECT PS_ACCOUNT_ID FROM PLAY_SESSIONS WHERE PS_PLAY_SESSION_ID = @p1";

        _sql_command = new SqlCommand(sql_str);
        _sql_command.Connection = Trx.Connection;
        _sql_command.Transaction = Trx;
        _sql_command.Parameters.Add("@p1", SqlDbType.BigInt).Value = PlaySessionId;

        try
        {
          _obj = _sql_command.ExecuteScalar();
          if (_obj != null)
          {
            _account_id = (Int64)_obj;
          }
        }
        catch (Exception _ex)
        {
          if (!stand_alone_session)
          {
            Log.Error("DB_InsertPlay: Error getting AccountID. PlaySessionId: " + PlaySessionId.ToString());
            Log.Exception(_ex);
          }
        }
      }

      sql_str = "INSERT INTO PLAYS (PL_PLAY_SESSION_ID " +
                                 ", PL_ACCOUNT_ID " +
                                 ", PL_TERMINAL_ID " +
                                 ", PL_WCP_SEQUENCE_ID" +
                                 ", PL_WCP_TRANSACTION_ID" +
                                 ", PL_GAME_ID " +
                                 ", PL_INITIAL_BALANCE " +
                                 ", PL_PLAYED_AMOUNT " +
                                 ", PL_WON_AMOUNT " +
                                 ", PL_FINAL_BALANCE " +
                                 ", PL_DATETIME) " +
                           "VALUES (@p1, @p2, @p3, @p4, @p5, @p6, @p7, @p8, @p9, @p10, GETDATE()) ";

      _sql_command = new SqlCommand(sql_str);
      _sql_command.Connection = Trx.Connection;
      _sql_command.Transaction = Trx;


      if (PlaySessionId == 0)
      {
        _sql_command.Parameters.Add("@p1", SqlDbType.BigInt, 8, "PL_PLAY_SESSION_ID").Value = DBNull.Value;
      }
      else
      {
        _sql_command.Parameters.Add("@p1", SqlDbType.BigInt, 8, "PL_PLAY_SESSION_ID").Value = PlaySessionId;
      }

      if (_account_id == 0)
      {
        _sql_command.Parameters.Add("@p2", SqlDbType.BigInt, 8, "PL_ACCOUNT_ID").Value = DBNull.Value;
      }
      else
      {
        _sql_command.Parameters.Add("@p2", SqlDbType.BigInt, 8, "PL_ACCOUNT_ID").Value = _account_id;
      }
      _sql_command.Parameters.Add("@p3", SqlDbType.Int, 4, "PL_TERMINAL_ID").Value = TerminalId;
      _sql_command.Parameters.Add("@p4", SqlDbType.BigInt, 8, "PL_WCP_SEQUENCE_ID").Value = SequenceId;
      _sql_command.Parameters.Add("@p5", SqlDbType.BigInt, 8, "PL_WCP_TRANSACTION_ID").Value = TransactionId;
      _sql_command.Parameters.Add("@p6", SqlDbType.Int, 4, "PL_GAME_ID").Value = _game_id;
      _sql_command.Parameters.Add("@p7", SqlDbType.Money, 8, "PL_INITIAL_BALANCE").Value = InitialBalance;
      _sql_command.Parameters.Add("@p8", SqlDbType.Money, 8, "PL_PLAYED_AMOUNT").Value = PlayedAmount;
      _sql_command.Parameters.Add("@p9", SqlDbType.Money, 8, "PL_WON_AMOUNT").Value = WonAmount;
      _sql_command.Parameters.Add("@p10", SqlDbType.Money, 8, "PL_FINAL_BALANCE").Value = FinalBalance;

      try
      {
        _num_rows_affected = _sql_command.ExecuteNonQuery();
      }
      catch
      {
        SqlCommand sql_sel_command;
        String sql_sel_str;
        SqlDataReader reader;
        Int32 num_plays;

        // Check if play already exist
        num_plays = 0;
        sql_sel_str = "SELECT COUNT(*) " +
                        "FROM PLAYS " +
                       "WHERE PL_TERMINAL_ID          = @p1 " +
                       "  AND PL_WCP_TRANSACTION_ID   = @p2 " +
                       "  AND PL_GAME_ID              = @p3 " +
                       "  AND PL_INITIAL_BALANCE      = @p4 " +
                       "  AND PL_PLAYED_AMOUNT        = @p5 " +
                       "  AND PL_WON_AMOUNT           = @p6 " +
                       "  AND PL_FINAL_BALANCE        = @p7 ";

        sql_sel_command = new SqlCommand(sql_sel_str);
        sql_sel_command.Connection = Trx.Connection;
        sql_sel_command.Transaction = Trx;

        sql_sel_command.Parameters.Add("@p1", SqlDbType.Int, 4, "PL_TERMINAL_ID").Value = TerminalId;
        sql_sel_command.Parameters.Add("@p2", SqlDbType.BigInt, 8, "PL_WCP_TRANSACTION_ID").Value = TransactionId;
        sql_sel_command.Parameters.Add("@p3", SqlDbType.Int, 4, "PL_GAME_ID").Value = _game_id;
        sql_sel_command.Parameters.Add("@p4", SqlDbType.Money, 8, "PL_INITIAL_BALANCE").Value = InitialBalance;
        sql_sel_command.Parameters.Add("@p5", SqlDbType.Money, 8, "PL_PLAYED_AMOUNT").Value = PlayedAmount;
        sql_sel_command.Parameters.Add("@p6", SqlDbType.Money, 8, "PL_WON_AMOUNT").Value = WonAmount;
        sql_sel_command.Parameters.Add("@p7", SqlDbType.Money, 8, "PL_FINAL_BALANCE").Value = FinalBalance;

        reader = null;

        try
        {
          reader = sql_sel_command.ExecuteReader();
          if (reader.Read() && !reader.IsDBNull(0))
          {
            num_plays = (Int32)reader[0];
          }
        }
        catch (Exception ex)
        {
          Log.Exception(ex);
          Log.Warning("Exception throw: reading play. TransactionId: " + TransactionId.ToString());
          throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR, "DB Error reading PLAY.");
        }
        finally
        {
          if (reader != null)
          {
            reader.Close();
            reader.Dispose();
            reader = null;
          }
        }

        if (num_plays > 0)
        {
          // Play already was inserted
          return true;
        }
      }

      if (_num_rows_affected != 1)
      {
        Log.Warning("Exception throw: inserting plays.PlaySessionId: " + PlaySessionId.ToString());
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR, "PLAY Not Inserted.");
      }

      // Play has been inserted
      return false;

    } // DB_InsertPlay

    #endregion Plays

    #region Movements

    //------------------------------------------------------------------------------
    // PURPOSE: Insert an account movement. ˇDEPRECATED! 
    //
    //  PARAMS:
    //      - INPUT:
    //          - PlaySessionId : Play session id.
    //          - CardId : Account id.
    //          - TerminalId : Terminal id.
    //          - SequenceId : Sequence id.
    //          - TransactionId :
    //          - Type :
    //              Play = 0,
    //              CashIn = 1,
    //              CashOut = 2,
    //              Devolution = 3,
    //              Tax = 4,
    //              StartCardSession = 5,
    //              EndCardSession = 6,
    //              Activate = 7,
    //              Deactivate = 8,
    //              CashInNotRedeemable = 9,
    //              DepositIn = 10,
    //              DepositOut = 11,
    //              CancelNotRedeemable = 12
    //          - InitialBalance :
    //          - SubAmount :
    //          - AddAmount :
    //          - FinalBalance :
    //          - MovementId :
    //          - Trx :
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    //        
    //   NOTES :
    //
    public static void DB_InsertMovement(Int64 PlaySessionId,
                                         Int64 CardId,
                                         Int32 TerminalId,
                                         Int64 SequenceId,
                                         Int64 TransactionId,
                                         MovementType Type,
                                         Decimal InitialBalance,
                                         Decimal SubAmount,
                                         Decimal AddAmount,
                                         Decimal FinalBalance,
                                         out Int64 MovementId,
                                         SqlTransaction Trx)
    {
      DB_InsertMovement(0,
                        PlaySessionId,
                        CardId,
                        TerminalId,
                        SequenceId,
                        TransactionId,
                        Type,
                        InitialBalance,
                        SubAmount,
                        AddAmount,
                        FinalBalance,
                        out MovementId,
                        Trx);
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Insert an account movement.
    //
    //  PARAMS:
    //      - INPUT:
    //          - PlaySessionId : Play session id.
    //          - CardId : Account id.
    //          - TerminalId : Terminal id.
    //          - SequenceId : Sequence id.
    //          - TransactionId :
    //          - Type :
    //              Play = 0,
    //              CashIn = 1,
    //              CashOut = 2,
    //              Devolution = 3,
    //              Tax = 4,
    //              StartCardSession = 5,
    //              EndCardSession = 6,
    //              Activate = 7,
    //              Deactivate = 8,
    //              CashInNotRedeemable = 9,
    //              DepositIn = 10,
    //              DepositOut = 11,
    //              CancelNotRedeemable = 12
    //          - InitialBalance :
    //          - SubAmount :
    //          - AddAmount :
    //          - FinalBalance :
    //          - MovementId :
    //          - Trx :
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    //        
    //   NOTES :
    //
    public static void DB_InsertMovement(Int64 OperationID,
                                         Int64 PlaySessionId,
                                         Int64 CardId,
                                         Int32 TerminalId,
                                         Int64 SequenceId,
                                         Int64 TransactionId,
                                         MovementType Type,
                                         Decimal InitialBalance,
                                         Decimal SubAmount,
                                         Decimal AddAmount,
                                         Decimal FinalBalance,
                                         out Int64 MovementId,
                                         SqlTransaction Trx)
    {
      SqlCommand _sql_command;
      SqlParameter p;
      String sql_str;
      Int32 num_rows_inserted;
      String _terminal_name;
      Object _obj;

      _terminal_name = "";

      if (TerminalId > 0)
      {
        sql_str = " SELECT  TE_NAME " +
                  "   FROM  TERMINALS " +
                  "   WHERE TE_TERMINAL_ID = @p1 ";

        _sql_command = new SqlCommand(sql_str);
        _sql_command.Connection = Trx.Connection;
        _sql_command.Transaction = Trx;

        _sql_command.Parameters.Add("@p1", SqlDbType.Int, 4, "TE_TERMINAL_ID").Value = TerminalId;

        try
        {
          _obj = _sql_command.ExecuteScalar();
          if (_obj != null)
          {
            _terminal_name = (String)_obj;
          }
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }
      }

      MovementId = 0;

      sql_str = "INSERT INTO ACCOUNT_MOVEMENTS (AM_PLAY_SESSION_ID " +
                                 ", AM_ACCOUNT_ID " +
                                 ", AM_TERMINAL_ID " +
                                 ", AM_WCP_SEQUENCE_ID " +
                                 ", AM_WCP_TRANSACTION_ID " +
                                 ", AM_TYPE " +
                                 ", AM_INITIAL_BALANCE " +
                                 ", AM_SUB_AMOUNT " +
                                 ", AM_ADD_AMOUNT " +
                                 ", AM_FINAL_BALANCE " +
                                 ", AM_DATETIME " +
                                 ", AM_TERMINAL_NAME " +
                                 ", AM_OPERATION_ID) " +
                           "VALUES (@p1, @p2, @p3, @p4, @p5, @p6, @p7, @p8, @p9, @p10, GETDATE(), @p11, @OperationID) " +
                              "SET @p12 = SCOPE_IDENTITY() ";

      _sql_command = new SqlCommand(sql_str);
      _sql_command.Connection = Trx.Connection;
      _sql_command.Transaction = Trx;

      if (PlaySessionId == 0)
      {
        _sql_command.Parameters.Add("@p1", SqlDbType.BigInt, 8, "AM_PLAY_SESSION_ID").Value = DBNull.Value;
      }
      else
      {
        _sql_command.Parameters.Add("@p1", SqlDbType.BigInt, 8, "AM_PLAY_SESSION_ID").Value = PlaySessionId;
      }

      if (CardId == 0)
      {
        _sql_command.Parameters.Add("@p2", SqlDbType.BigInt, 8, "AM_ACCOUNT_ID").Value = DBNull.Value;
      }
      else
      {
        _sql_command.Parameters.Add("@p2", SqlDbType.BigInt, 8, "AM_ACCOUNT_ID").Value = CardId;
      }

      if (TerminalId == 0)
      {
        _sql_command.Parameters.Add("@p3", SqlDbType.Int, 4, "AM_TERMINAL_ID").Value = DBNull.Value;
      }
      else
      {
        _sql_command.Parameters.Add("@p3", SqlDbType.Int, 4, "AM_TERMINAL_ID").Value = TerminalId;
      }

      if (SequenceId == 0)
      {
        _sql_command.Parameters.Add("@p4", SqlDbType.BigInt, 8, "AM_WCP_SEQUENCE_ID").Value = DBNull.Value;
      }
      else
      {
        _sql_command.Parameters.Add("@p4", SqlDbType.BigInt, 8, "AM_WCP_SEQUENCE_ID").Value = SequenceId;
      }

      if (TransactionId == 0)
      {
        _sql_command.Parameters.Add("@p5", SqlDbType.BigInt, 8, "AM_WCP_TRANSACTION_ID").Value = DBNull.Value;
      }
      else
      {
        _sql_command.Parameters.Add("@p5", SqlDbType.BigInt, 8, "AM_WCP_TRANSACTION_ID").Value = TransactionId;
      }

      _sql_command.Parameters.Add("@p6", SqlDbType.Int, 4, "AM_TYPE").Value = (Int32)Type;
      _sql_command.Parameters.Add("@p7", SqlDbType.Money, 8, "AM_INITIAL_BALANCE").Value = InitialBalance;
      _sql_command.Parameters.Add("@p8", SqlDbType.Money, 8, "AM_SUB_AMOUNT").Value = SubAmount;
      _sql_command.Parameters.Add("@p9", SqlDbType.Money, 8, "AM_ADD_AMOUNT").Value = AddAmount;
      _sql_command.Parameters.Add("@p10", SqlDbType.Money, 8, "AM_FINAL_BALANCE").Value = FinalBalance;

      if (_terminal_name == "")
      {
        _sql_command.Parameters.Add("@p11", SqlDbType.NVarChar, 50, "AM_TERMINAL_NAME").Value = DBNull.Value;
      }
      else
      {
        _sql_command.Parameters.Add("@p11", SqlDbType.NVarChar, 50, "AM_TERMINAL_NAME").Value = _terminal_name;
      }

      if (OperationID == 0)
      {
        _sql_command.Parameters.Add("@OperationID", SqlDbType.BigInt, 8, "AM_OPERATION_ID").Value = DBNull.Value;
      }
      else
      {
        _sql_command.Parameters.Add("@OperationID", SqlDbType.BigInt, 8, "AM_OPERATION_ID").Value = OperationID;
      }

      p = _sql_command.Parameters.Add("@p12", SqlDbType.BigInt, 8, "AM_MOVEMENT_ID");
      p.Direction = ParameterDirection.Output;

      num_rows_inserted = _sql_command.ExecuteNonQuery();

      if (num_rows_inserted != 1)
      {
        Log.Warning("Exception throw: inserting movements.Mov. PlaySessionId: " + PlaySessionId.ToString() + " TerminalId: " + TerminalId.ToString());
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR, "MOVEMENT Not Inserted.");
      }

      MovementId = (Int64)p.Value;

    } // DB_InsertMovement

    #endregion Movements

    #region Events

    /// <summary>
    /// Insert event
    /// </summary>
    /// <param name="TerminalId"></param>
    /// <param name="SessionId"></param>
    /// <param name="EventType"></param>
    /// <param name="EventData"></param>
    /// <param name="EventDateTime"></param>
    /// <param name="Trx"></param>
    public static void DB_InsertEventOperation(Int32 TerminalId,
                                               Int64 SessionId,
                                               OperationCodes OperationCode,
                                               Decimal OperationData,
                                               DateTime EventDateTime,
                                               SqlTransaction Trx)
    {
      SqlCommand sql_command;
      String sql_str;
      Int32 num_rows_inserted;

      sql_str = "INSERT INTO EVENT_HISTORY (EH_TERMINAL_ID " +
                                         ", EH_SESSION_ID " +
                                         ", EH_DATETIME " +
                                         ", EH_EVENT_TYPE " +
                                         ", EH_OPERATION_CODE " +
                                         ", EH_OPERATION_DATA) " +
                           "VALUES (@p1, @p2, @p3, @p4, @p5, @p6) ";

      sql_command = new SqlCommand(sql_str);
      sql_command.Connection = Trx.Connection;
      sql_command.Transaction = Trx;

      sql_command.Parameters.Add("@p1", SqlDbType.Int, 4, "EH_TERMINAL_ID").Value = TerminalId;
      sql_command.Parameters.Add("@p2", SqlDbType.BigInt, 8, "EH_SESSION_ID").Value = SessionId;
      sql_command.Parameters.Add("@p3", SqlDbType.DateTime, 8, "EH_DATETIME").Value = EventDateTime;
      sql_command.Parameters.Add("@p4", SqlDbType.Int, 4, "EH_EVENT_TYPE").Value = (Int32)WCP_EventTypesCodes.WCP_EV_OPERATION;
      sql_command.Parameters.Add("@p5", SqlDbType.Int, 4, "EH_OPERATION_CODE").Value = (Int32)OperationCode;
      sql_command.Parameters.Add("@p6", SqlDbType.Money).Value = OperationData;

      num_rows_inserted = sql_command.ExecuteNonQuery();

      if (num_rows_inserted != 1)
      {
        Log.Warning("Exception throw: inserting events.TerminalId: " + TerminalId.ToString() + " SessionId: " + SessionId.ToString());
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR, "EVENT Not Inserted.");
      }
    } // DB_InsertEvent

    /// <summary>
    /// Insert event
    /// </summary>
    /// <param name="TerminalId"></param>
    /// <param name="SessionId"></param>
    /// <param name="EventType"></param>
    /// <param name="EventData"></param>
    /// <param name="EventDateTime"></param>
    /// <param name="Trx"></param>
    public static void DB_InsertEvent(Int32 TerminalId,
                                      Int64 SessionId,
                                      WCP_EventTypesCodes EventType,
                                      String EventData,
                                      DateTime EventDateTime,
                                      SqlTransaction Trx)
    {
      SqlCommand sql_command;
      String sql_str;
      Int32 num_rows_inserted;

      sql_str = "INSERT INTO EVENT_HISTORY (EH_TERMINAL_ID " +
                                         ", EH_SESSION_ID " +
                                         ", EH_DATETIME " +
                                         ", EH_EVENT_TYPE " +
                                         ", EH_EVENT_DATA) " +
                           "VALUES (@p1, @p2, @p3, @p4, @p5) ";

      sql_command = new SqlCommand(sql_str);
      sql_command.Connection = Trx.Connection;
      sql_command.Transaction = Trx;

      sql_command.Parameters.Add("@p1", SqlDbType.Int, 4, "EH_TERMINAL_ID").Value = TerminalId;
      sql_command.Parameters.Add("@p2", SqlDbType.BigInt, 8, "EH_SESSION_ID").Value = SessionId;
      sql_command.Parameters.Add("@p3", SqlDbType.DateTime, 8, "EH_DATETIME").Value = EventDateTime;
      sql_command.Parameters.Add("@p4", SqlDbType.Int, 4, "EH_EVENT_TYPE").Value = (Int32)EventType;
      sql_command.Parameters.Add("@p5", SqlDbType.NVarChar, 50, "EH_EVENT_DATA").Value = EventData;

      num_rows_inserted = sql_command.ExecuteNonQuery();

      if (num_rows_inserted != 1)
      {
        Log.Warning("Exception throw: inserting events.TerminalId: " + TerminalId.ToString() + " SessionId: " + SessionId.ToString());
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR, "EVENT Not Inserted.");
      }
    } // DB_InsertEvent


    //------------------------------------------------------------------------------
    // PURPOSE: Insert notified events into database.
    //
    //  PARAMS:
    //      - INPUT:
    //          - TerminalId
    //          - SessionId
    //          - EventType
    //          - DeviceStatus
    //          - Operation
    //          - Trx
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    //      - StatusCode
    //        
    //   NOTES :
    //
    public static void DB_NewInsertEvent(Int32 TerminalId,
                                         Int64 SessionId,
                                         WCP_EventTypesCodes EventType,
                                         WCP_DeviceStatus DeviceStatus,
                                         WCP_Operation Operation,
                                         SqlTransaction Trx)
    {
      SqlCommand sql_command;
      String sql_str;
      Int32 num_rows_inserted;
      Decimal operation_data;

      num_rows_inserted = 0;

      sql_str = "INSERT INTO EVENT_HISTORY (EH_TERMINAL_ID " +
                                         ", EH_SESSION_ID " +
                                         ", EH_DATETIME " +
                                         ", EH_EVENT_TYPE " +
                                         ", EH_DEVICE_CODE " +
                                         ", EH_DEVICE_STATUS " +
                                         ", EH_DEVICE_PRIORITY " +
                                         ", EH_OPERATION_CODE " +
                                         ", EH_OPERATION_DATA) " +
                           "VALUES (@p1, @p2, @p3, @p4, @p5, @p6, @p7, @p8, @p9) ";

      sql_command = new SqlCommand(sql_str);
      sql_command.Connection = Trx.Connection;
      sql_command.Transaction = Trx;

      sql_command.Parameters.Add("@p1", SqlDbType.Int, 4, "EH_TERMINAL_ID").Value = TerminalId;
      sql_command.Parameters.Add("@p2", SqlDbType.BigInt, 8, "EH_SESSION_ID").Value = SessionId;
      sql_command.Parameters.Add("@p4", SqlDbType.Int, 4, "EH_EVENT_TYPE").Value = (Int32)EventType;

      if (EventType == WCP_EventTypesCodes.WCP_EV_DEVICE_STATUS)
      {
        sql_command.Parameters.Add("@p3", SqlDbType.DateTime, 8, "EH_DATETIME").Value = DeviceStatus.LocalTime;
        sql_command.Parameters.Add("@p5", SqlDbType.Int, 4, "EH_DEVICE_CODE").Value = DeviceStatus.Code;
        sql_command.Parameters.Add("@p6", SqlDbType.Int, 4, "EH_DEVICE_STATUS").Value = DeviceStatus.Status;
        sql_command.Parameters.Add("@p7", SqlDbType.Int, 4, "EH_DEVICE_PRIORITY").Value = DeviceStatus.Priority;
        sql_command.Parameters.Add("@p8", SqlDbType.Int, 4, "EH_OPERATION_CODE").Value = DBNull.Value;
        sql_command.Parameters.Add("@p9", SqlDbType.Money, 8, "EH_OPERATION_DATA").Value = DBNull.Value;
      }
      else if (EventType == WCP_EventTypesCodes.WCP_EV_OPERATION)
      {
        operation_data = (Decimal)Operation.Data;
        if (Operation.Code == OperationCodes.WCP_OPERATION_CODE_JACKPOT_WON
            || Operation.Code == OperationCodes.WCP_OPERATION_CODE_KIOSK_BLOCKED_HIGH_PRIZE)
        {
          operation_data = ((Decimal)Operation.Data) / 100;
        }

        sql_command.Parameters.Add("@p3", SqlDbType.DateTime, 8, "EH_DATETIME").Value = Operation.LocalTime;
        sql_command.Parameters.Add("@p5", SqlDbType.Int, 4, "EH_DEVICE_CODE").Value = DBNull.Value;
        sql_command.Parameters.Add("@p6", SqlDbType.Int, 4, "EH_DEVICE_STATUS").Value = DBNull.Value;
        sql_command.Parameters.Add("@p7", SqlDbType.Int, 4, "EH_DEVICE_PRIORITY").Value = DBNull.Value;
        sql_command.Parameters.Add("@p8", SqlDbType.Int, 4, "EH_OPERATION_CODE").Value = Operation.Code;
        sql_command.Parameters.Add("@p9", SqlDbType.Money, 8, "EH_OPERATION_DATA").Value = operation_data;
      }
      else
      {
        Log.Warning("Exception throw: Unexpected EVENT Type. TerminalId: " + TerminalId.ToString() + " SessionId: " + SessionId.ToString());
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR, "Unexpected EVENT Type.");
      }

      try
      {
        num_rows_inserted = sql_command.ExecuteNonQuery();
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }

      if (num_rows_inserted != 1)
      {
        Log.Warning("Exception throw: inserting events.TerminalId: " + TerminalId.ToString() + " SessionId: " + SessionId.ToString());
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR, "EVENT Not Inserted.");
      }
    } // DB_InsertEvent

    #endregion Events

    #region Meters

    /// <summary>
    /// Update meters
    /// </summary>
    /// <param name="TerminalId"></param>
    /// <param name="WcpMeters"></param>
    /// <param name="Trx"></param>
    public static void DB_UpdateMeters(Int32 TerminalId,
                                       WCP_MsgReportMeters WcpMeters,
                                       SqlTransaction Trx)
    {
      SqlCommand _sql_command;
      String sql_str;
      Int32 num_rows_inserted;
      Int32 num_rows_updated;
      WCP_MoneyMeter wcp_money_meter_item;
      Int64 _meter_id;
      SqlParameter p;
      Object _obj;

      //
      // Get Meter Id
      //
      sql_str = "SELECT ME_METER_ID FROM METERS WHERE ME_TERMINAL_ID = @p1";

      _sql_command = new SqlCommand(sql_str);
      _sql_command.Connection = Trx.Connection;
      _sql_command.Transaction = Trx;

      _sql_command.Parameters.Add("@p1", SqlDbType.Int, 4, "ME_TERMINAL_ID").Value = TerminalId;

      _meter_id = 0;

      try
      {
        _obj = _sql_command.ExecuteScalar();
        if (_obj != null)
        {
          _meter_id = (Int64)_obj;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      if (_meter_id == 0)
      {
        //
        // Insert Meter for this terminal
        //
        sql_str = "INSERT INTO METERS (ME_TERMINAL_ID   , ME_PLAYED_COUNT " +
                                    ", ME_PLAYED_AMOUNT , ME_WON_COUNT " +
                                    ", ME_WON_AMOUNT    , ME_CASH_IN " +
                                    ", ME_CASH_OUT      , ME_LAST_REPORTED) " +
                              "VALUES (@p1, @p2, @p3, @p4, @p5, @p6, @p7, GETDATE()) " +
                                " SET  @p8 = SCOPE_IDENTITY()";

        _sql_command = new SqlCommand(sql_str);
        _sql_command.Connection = Trx.Connection;
        _sql_command.Transaction = Trx;

        _sql_command.Parameters.Add("@p1", SqlDbType.Int, 4, "ME_TERMINAL_ID").Value = TerminalId;
        _sql_command.Parameters.Add("@p2", SqlDbType.Int, 4, "ME_PLAYED_COUNT").Value = WcpMeters.PlayedCount;
        _sql_command.Parameters.Add("@p3", SqlDbType.Money, 8, "ME_PLAYED_AMOUNT").Value = ((decimal)WcpMeters.PlayedAmount) / 100;
        _sql_command.Parameters.Add("@p4", SqlDbType.Int, 4, "ME_WON_COUNT").Value = WcpMeters.WonCount;
        _sql_command.Parameters.Add("@p5", SqlDbType.Money, 8, "ME_WON_AMOUNT").Value = ((decimal)WcpMeters.WonAmount) / 100;
        _sql_command.Parameters.Add("@p6", SqlDbType.Money, 8, "ME_CASH_IN").Value = ((decimal)WcpMeters.CashInAmount) / 100;
        _sql_command.Parameters.Add("@p7", SqlDbType.Money, 8, "ME_CASH_OUT").Value = ((decimal)WcpMeters.CashOutAmount) / 100;

        p = _sql_command.Parameters.Add("@p8", SqlDbType.BigInt, 8, "ME_METER_ID");
        p.Direction = ParameterDirection.Output;

        num_rows_inserted = _sql_command.ExecuteNonQuery();

        if (num_rows_inserted != 1)
        {
          Log.Warning("Exception throw: inserting meter.TerminalId: " + TerminalId.ToString());
          throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR, "METER Not Inserted.");
        }

        _meter_id = (Int64)p.Value;
      }
      else
      {
        //
        // Update Meter for this terminal
        //
        sql_str = "UPDATE METERS SET ME_PLAYED_COUNT = @p1  , ME_PLAYED_AMOUNT = @p2" +
                                  ", ME_WON_COUNT = @p3     , ME_WON_AMOUNT = @p4 " +
                                  ", ME_CASH_IN = @p5       , ME_CASH_OUT = @p6 " +
                                  ", ME_LAST_REPORTED = GETDATE() " +
                             " WHERE ME_METER_ID = @p7 ";

        _sql_command = new SqlCommand(sql_str);
        _sql_command.Connection = Trx.Connection;
        _sql_command.Transaction = Trx;

        _sql_command.Parameters.Add("@p1", SqlDbType.Int, 4, "ME_PLAYED_COUNT").Value = WcpMeters.PlayedCount;
        _sql_command.Parameters.Add("@p2", SqlDbType.Money, 8, "ME_PLAYED_AMOUNT").Value = WcpMeters.PlayedAmount;
        _sql_command.Parameters.Add("@p3", SqlDbType.Int, 4, "ME_WON_COUNT").Value = WcpMeters.WonCount;
        _sql_command.Parameters.Add("@p4", SqlDbType.Money, 8, "ME_WON_AMOUNT").Value = WcpMeters.WonAmount;
        _sql_command.Parameters.Add("@p5", SqlDbType.Money, 8, "ME_CASH_IN").Value = WcpMeters.CashInAmount;
        _sql_command.Parameters.Add("@p6", SqlDbType.Money, 8, "ME_CASH_OUT").Value = WcpMeters.CashOutAmount;
        _sql_command.Parameters.Add("@p7", SqlDbType.BigInt, 8, "ME_METER_ID").Value = _meter_id;

        num_rows_updated = _sql_command.ExecuteNonQuery();

        if (num_rows_updated != 1)
        {
          Log.Warning("Exception throw: updating meter.MeterId: " + _meter_id.ToString());
          throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR, "METER Not Updated.");
        }
      }

      for (int idx_money_meter = 0; idx_money_meter < WcpMeters.MoneyMeters.Count; idx_money_meter++)
      {
        wcp_money_meter_item = (WCP_MoneyMeter)WcpMeters.MoneyMeters[idx_money_meter];

        //
        // Update Money Meter for this terminal
        //
        sql_str = "UPDATE MONEY_METERS SET MM_COUNT = @p1 " +
                                        ", MM_AMOUNT  = @p2" +
                                        ", MM_LAST_REPORTED = GETDATE() " +
                             " WHERE MM_METER_ID = @p3 " +
                             "   AND MM_CASH_TYPE = @p4 " +
                             "   AND MM_MONEY_TYPE = @p5 " +
                             "   AND MM_FACE_VALUE = @p6 ";

        _sql_command = new SqlCommand(sql_str);
        _sql_command.Connection = Trx.Connection;
        _sql_command.Transaction = Trx;

        _sql_command.Parameters.Add("@p1", SqlDbType.Int, 4, "MM_COUNT").Value = wcp_money_meter_item.Count;
        _sql_command.Parameters.Add("@p2", SqlDbType.Money, 8, "MM_AMOUNT").Value = wcp_money_meter_item.Amount;
        _sql_command.Parameters.Add("@p3", SqlDbType.BigInt, 8, "MM_METER_ID").Value = _meter_id;
        _sql_command.Parameters.Add("@p4", SqlDbType.Int, 4, "MM_CASH_TYPE").Value = wcp_money_meter_item.CashType;
        _sql_command.Parameters.Add("@p5", SqlDbType.Int, 4, "MM_MONEY_TYPE").Value = wcp_money_meter_item.MoneyType;
        _sql_command.Parameters.Add("@p6", SqlDbType.Money, 8, "MM_FACE_VALUE").Value = wcp_money_meter_item.FaceValue;

        num_rows_updated = _sql_command.ExecuteNonQuery();

        if (num_rows_updated != 1)
        {
          //
          // Insert Money Meter for this terminal
          //
          sql_str = "INSERT INTO MONEY_METERS (MM_METER_ID      , MM_CASH_TYPE " +
                                            ", MM_MONEY_TYPE    , MM_FACE_VALUE " +
                                            ", MM_COUNT         , MM_AMOUNT " +
                                            ", MM_LAST_REPORTED ) " +
                                "VALUES (@p1, @p2, @p3, @p4, @p5, @p6, GETDATE()) ";

          _sql_command = new SqlCommand(sql_str);
          _sql_command.Connection = Trx.Connection;
          _sql_command.Transaction = Trx;

          _sql_command.Parameters.Add("@p1", SqlDbType.BigInt, 8, "MM_METER_ID").Value = _meter_id;
          _sql_command.Parameters.Add("@p2", SqlDbType.Int, 4, "MM_CASH_TYPE").Value = wcp_money_meter_item.CashType;
          _sql_command.Parameters.Add("@p3", SqlDbType.Int, 4, "MM_MONEY_TYPE").Value = wcp_money_meter_item.MoneyType;
          _sql_command.Parameters.Add("@p4", SqlDbType.Money, 8, "MM_FACE_VALUE").Value = wcp_money_meter_item.FaceValue;
          _sql_command.Parameters.Add("@p5", SqlDbType.Int, 4, "MM_COUNT").Value = wcp_money_meter_item.Count;
          _sql_command.Parameters.Add("@p6", SqlDbType.Money, 8, "MM_AMOUNT").Value = wcp_money_meter_item.Amount;

          num_rows_inserted = _sql_command.ExecuteNonQuery();

          if (num_rows_inserted != 1)
          {
            Log.Warning("Exception throw: inserting money meter.MeterId: " + _meter_id.ToString());
            throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR, "METER Not Inserted.");
          }
        }
      }

    } // DB_UpdateMeters

    //------------------------------------------------------------------------------
    // PURPOSE : Update Sales Per Hour Table 
    //
    //  PARAMS :
    //      - INPUT : 
    //          - DeltaGameMeter: class GameMeters
    //          - TerminalId: Terminal Id
    //          - SqlTrx: SQL Transaction
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True or False  
    //
    //   NOTES :
    //
    static public Boolean DB_UpdateSalesPerHour(GameMeters DeltaGameMeter, Int32 TerminalId, SqlTransaction SqlTrx)
    {
      DateTime _base_hour;
      Boolean _update_ok;
      SqlCommand _sql_command;
      StringBuilder _sql_txt;
      Int32 _affected_rows;
      String _sql_str;
      Int32 _game_id;
      Object _obj;
      Decimal _terminal_default_payout;

      _update_ok = false;
      _base_hour = WGDB.Now;
      _base_hour = _base_hour.AddMinutes(-_base_hour.Minute);
      _base_hour = _base_hour.AddSeconds(-_base_hour.Second);
      _base_hour = _base_hour.AddMilliseconds(-_base_hour.Millisecond);

      // Get game id from the game name.
      _sql_str = "SELECT GM_GAME_ID FROM GAMES WHERE GM_NAME = @p1";

      _sql_command = new SqlCommand(_sql_str);
      _sql_command.Connection = SqlTrx.Connection;
      _sql_command.Transaction = SqlTrx;
      _sql_command.Parameters.Add("@p1", SqlDbType.NVarChar, 50, "GM_NAME").Value = DeltaGameMeter.GameId;

      _game_id = 0;

      _terminal_default_payout = GeneralParam.GetDecimal("PlayerTracking", "TerminalDefaultPayout", 95) / 100m;

      try
      {
        _obj = _sql_command.ExecuteScalar();
        if (_obj != null)
        {
          _game_id = (Int32)_obj;
        }
      }
      catch (Exception _ex)
      {
        Log.Error("DB_UpdateSalesPerHour: Error getting game code. GameName: " + DeltaGameMeter.GameId);
        Log.Exception(_ex);
      }

      // Update 
      _sql_txt = new StringBuilder();

      _sql_txt.AppendLine("DECLARE  @payout AS MONEY ");
      _sql_txt.AppendLine("SELECT   @payout = ISNULL(TE_THEORETICAL_PAYOUT, @pTerminalDefaultPayout) ");
      _sql_txt.AppendLine("  FROM   TERMINALS ");
      _sql_txt.AppendLine(" WHERE   TE_TERMINAL_ID = @pTerminalId ; ");

      _sql_txt.AppendLine("UPDATE SALES_PER_HOUR");
      _sql_txt.AppendLine("   SET SPH_PLAYED_COUNT  = SPH_PLAYED_COUNT  + @p1");
      _sql_txt.AppendLine("     , SPH_PLAYED_AMOUNT = SPH_PLAYED_AMOUNT + @p2");
      _sql_txt.AppendLine("     , SPH_WON_COUNT     = SPH_WON_COUNT     + @p3");
      _sql_txt.AppendLine("     , SPH_WON_AMOUNT    = SPH_WON_AMOUNT    + @p4");
      _sql_txt.AppendLine("     , SPH_THEORETICAL_WON_AMOUNT = ( SPH_PLAYED_AMOUNT + @p2 ) * @payout ");
      _sql_txt.AppendLine(" WHERE SPH_GAME_ID       = @p5");
      _sql_txt.AppendLine("   AND SPH_TERMINAL_ID   = @pTerminalId");
      _sql_txt.AppendLine("   AND SPH_BASE_HOUR     = @p7");

      _sql_command = new SqlCommand(_sql_txt.ToString());
      _sql_command.Connection = SqlTrx.Connection;
      _sql_command.Transaction = SqlTrx;

      _sql_command.Parameters.Add("@p1", SqlDbType.BigInt).Value = DeltaGameMeter.PlayedCount;
      _sql_command.Parameters.Add("@p2", SqlDbType.Money).Value = ((Decimal)DeltaGameMeter.PlayedCents) / 100;
      _sql_command.Parameters.Add("@p3", SqlDbType.BigInt).Value = DeltaGameMeter.WonCount;
      _sql_command.Parameters.Add("@p4", SqlDbType.Money).Value = ((Decimal)DeltaGameMeter.WonCents) / 100;
      _sql_command.Parameters.Add("@p5", SqlDbType.Int).Value = _game_id;
      _sql_command.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;
      _sql_command.Parameters.Add("@p7", SqlDbType.DateTime).Value = _base_hour;
      _sql_command.Parameters.Add("@pTerminalDefaultPayout", SqlDbType.Decimal).Value = _terminal_default_payout;

      try
      {
        _affected_rows = _sql_command.ExecuteNonQuery();
        if (_affected_rows == 1)
        {
          _update_ok = true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      if (!_update_ok)
      {
        _update_ok = DB_InsertSalesPerHour(DeltaGameMeter, TerminalId, _game_id, _base_hour, SqlTrx);
      }
      return _update_ok;

    } // DB_UpdateSalesPerHour

    //------------------------------------------------------------------------------
    // PURPOSE : Insert into Sales Per Hour Table 
    //
    //  PARAMS :
    //      - INPUT : 
    //          - DeltaGameMeter: class GameMeters
    //          - TerminalId: Terminal Id
    //          - IdGameId: Game Id
    //          - BaseHour: Formatted base hour
    //          - SqlTrx: SQL Transaction
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True or False  
    //
    //   NOTES :
    //
    static private Boolean DB_InsertSalesPerHour(GameMeters DeltaGameMeter, Int32 TerminalId, Int32 IdGameId, DateTime BaseHour, SqlTransaction SqlTrx)
    {
      Int32 _terminal_active;
      Boolean _insert_ok;
      String _terminal_name;
      String _sql_str;
      SqlCommand _sql_command;
      StringBuilder _sql_txt;
      Int32 _affected_rows;
      Object _obj;
      Decimal _terminal_default_payout;

      _insert_ok = false;

      _terminal_default_payout = GeneralParam.GetDecimal("PlayerTracking", "TerminalDefaultPayout", 95) / 100m;

      // Get terminal name.
      _sql_str = "SELECT TE_NAME FROM TERMINALS WHERE TE_TERMINAL_ID = @p1";

      _sql_command = new SqlCommand(_sql_str);
      _sql_command.Connection = SqlTrx.Connection;
      _sql_command.Transaction = SqlTrx;
      _sql_command.Parameters.Add("@p1", SqlDbType.NVarChar, 50, "TE_TERMINAL_ID").Value = TerminalId;

      _terminal_name = "";

      try
      {
        _obj = _sql_command.ExecuteScalar();
        if (_obj != null)
        {
          _terminal_name = (String)_obj;
        }
      }
      catch (Exception _ex)
      {
        Log.Error("DB_InsertSalesPerHour: Error getting Terminal Name. TerminalId: " + TerminalId);
        Log.Exception(_ex);
      }

      // Get terminal active.
      _sql_str = "SELECT COUNT(*) FROM TERMINALS WHERE TE_ACTIVE = 1";

      _sql_command = new SqlCommand(_sql_str);
      _sql_command.Connection = SqlTrx.Connection;
      _sql_command.Transaction = SqlTrx;

      try
      {
        _terminal_active = (Int32)_sql_command.ExecuteScalar();
      }
      catch (Exception _ex)
      {
        _terminal_active = 0;

        Log.Error("DB_InsertSalesPerHour: Error getting Terminal Actived. TerminalId: " + TerminalId);
        Log.Exception(_ex);
      }

      // Insert
      _sql_txt = new StringBuilder();

      _sql_txt.AppendLine("DECLARE  @payout AS MONEY ");
      _sql_txt.AppendLine("SELECT   @payout = ISNULL(TE_THEORETICAL_PAYOUT, @pTerminalDefaultPayout) ");
      _sql_txt.AppendLine("  FROM   TERMINALS ");
      _sql_txt.AppendLine(" WHERE   TE_TERMINAL_ID = @pTerminalId ; ");

      _sql_txt.AppendLine("INSERT INTO  SALES_PER_HOUR ");
      _sql_txt.AppendLine("           ( SPH_BASE_HOUR ");
      _sql_txt.AppendLine("           , SPH_TERMINAL_ID ");
      _sql_txt.AppendLine("           , SPH_TERMINAL_NAME ");
      _sql_txt.AppendLine("           , SPH_GAME_ID ");
      _sql_txt.AppendLine("           , SPH_GAME_NAME ");
      _sql_txt.AppendLine("           , SPH_PLAYED_COUNT ");
      _sql_txt.AppendLine("           , SPH_PLAYED_AMOUNT ");
      _sql_txt.AppendLine("           , SPH_WON_COUNT ");
      _sql_txt.AppendLine("           , SPH_WON_AMOUNT ");
      _sql_txt.AppendLine("           , SPH_THEORETICAL_WON_AMOUNT ");
      _sql_txt.AppendLine("           , SPH_NUM_ACTIVE_TERMINALS ");
      _sql_txt.AppendLine("           , SPH_LAST_PLAY_ID) ");
      _sql_txt.AppendLine("VALUES (@p1, @pTerminalId, @p3, @p4, @p5, @p6, @p7, @p8, @p9, @p7 * @payout , @p10, @p11) ");

      _sql_command = new SqlCommand(_sql_txt.ToString());
      _sql_command.Connection = SqlTrx.Connection;
      _sql_command.Transaction = SqlTrx;

      _sql_command.Parameters.Add("@p1", SqlDbType.DateTime).Value = BaseHour;
      _sql_command.Parameters.Add("@pTerminalId", SqlDbType.BigInt).Value = TerminalId;
      _sql_command.Parameters.Add("@p3", SqlDbType.NVarChar, 50).Value = _terminal_name;
      _sql_command.Parameters.Add("@p4", SqlDbType.BigInt).Value = IdGameId;
      _sql_command.Parameters.Add("@p5", SqlDbType.NVarChar, 50).Value = DeltaGameMeter.GameId;
      _sql_command.Parameters.Add("@p6", SqlDbType.BigInt).Value = DeltaGameMeter.PlayedCount;
      _sql_command.Parameters.Add("@p7", SqlDbType.Money).Value = ((Decimal)DeltaGameMeter.PlayedCents) / 100;
      _sql_command.Parameters.Add("@p8", SqlDbType.BigInt).Value = DeltaGameMeter.WonCount;
      _sql_command.Parameters.Add("@p9", SqlDbType.Money).Value = ((Decimal)DeltaGameMeter.WonCents) / 100;
      _sql_command.Parameters.Add("@p10", SqlDbType.Int).Value = _terminal_active;
      _sql_command.Parameters.Add("@p11", SqlDbType.BigInt).Value = 0;
      _sql_command.Parameters.Add("@pTerminalDefaultPayout", SqlDbType.Decimal).Value = _terminal_default_payout;


      try
      {
        _affected_rows = _sql_command.ExecuteNonQuery();

        if (_affected_rows == 1)
        {
          _insert_ok = true;
        }
      }

      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return _insert_ok;

    } // DB_InsertSalesPerHour


    static public Boolean DB_InsertHandpay(Int32 TerminalId, WCP_MsgEGMHandpays Request, SqlTransaction Trx)
    {
      Int32 _num_rows_inserted;

      String _egm_iso;
      Decimal _egm_amt;
      Decimal _sys_amt;

      //INSERT HANDPAY with HP_TYPE MachineReported = 0.
      String _sql_query = "INSERT INTO HANDPAYS(HP_TERMINAL_ID " +
                                            " , HP_DATETIME " +
                                            " , HP_AMOUNT " +
                                            " , HP_TE_NAME " +
                                            " , HP_TE_PROVIDER_ID " +
                                            " , HP_TYPE " +
                                            " , HP_CANDIDATE_PLAY_SESSION_ID " +
                                            " , HP_CANDIDATE_PREV_PLAY_SESSION_ID " +
                                            " , HP_TRANSACTION_ID " +
                                            " , HP_LONG_POLL_1B_DATA " +
                                            " , HP_LEVEL " +
                                            " , HP_PROGRESSIVE_ID " +
                                            " , HP_AMT0  " +
                                            " , HP_CUR0  " +
                                            " , HP_PLAY_SESSION_ID ) " +
                                      " VALUES (@TerminalId " +
                                            " , @DateTime " +
                                            " , @Amount " +
                                            " , @TermName " +
                                            " , @ProviderId " +
                                            " , @HandpayType " +
                                            " , @CandidatePlaySessionId" +
                                            " , @CandidatePrevPlaySessionId" +
                                            " , @TransactionId " +
                                            " , @LongPoll1bData " +
                                            " , @pLevel " +
                                            " , @pProgressiveID " +
                                            " , @pAmount0 " +
                                            " , @pCurren0 " +
                                            " , @PlaySessionID ) ";
      try
      {
        // Get info terminal        
        Common.Terminal.TerminalInfo _terminal_info;
        Common.Terminal.Trx_GetTerminalInfo(TerminalId, out _terminal_info, Trx);
        WSI.Common.TITO.HandPay.GetFloorDualCurrency(((Decimal)Request.HandpayCents / 100), TerminalId, out _sys_amt, out _egm_amt, out _egm_iso, Trx);

        using (SqlCommand _sql_command = new SqlCommand(_sql_query, Trx.Connection, Trx))
        {
          SqlParameter _sql_param;

          _sql_command.Parameters.Add("@TerminalId", SqlDbType.Int).Value = TerminalId;
          _sql_command.Parameters.Add("@DateTime", SqlDbType.DateTime).Value = Request.Datetime;
          _sql_command.Parameters.Add("@Amount", SqlDbType.Money).Value = _sys_amt;
          _sql_command.Parameters.Add("@TermName", SqlDbType.NVarChar).Value = _terminal_info.Name;
          _sql_command.Parameters.Add("@ProviderId", SqlDbType.NVarChar).Value = _terminal_info.ProviderName;
          _sql_command.Parameters.Add("@HandpayType", SqlDbType.Int).Value = Request.HandpayType;
          _sql_command.Parameters.Add("@CandidatePlaySessionId", SqlDbType.BigInt).Value = Request.PlaySessionID;
          _sql_command.Parameters.Add("@CandidatePrevPlaySessionId", SqlDbType.BigInt).Value = Request.PreviousPlaySessionID;
          _sql_command.Parameters.Add("@TransactionId", SqlDbType.BigInt).Value = Request.TransactionID;
          _sql_command.Parameters.Add("@LongPoll1bData", SqlDbType.Xml).Value = Request.ToXml();
          _sql_command.Parameters.Add("@pLevel", SqlDbType.Int).Value = (Int32)Request.Level;
          _sql_command.Parameters.Add("@pAmount0", SqlDbType.Money).Value = (Decimal)_egm_amt;
          _sql_command.Parameters.Add("@pCurren0", SqlDbType.NVarChar, 3).Value = _egm_iso;
          _sql_command.Parameters.Add("@PlaySessionID", SqlDbType.BigInt).Value = Request.PlaySessionID;


          _sql_param = _sql_command.Parameters.Add("@pProgressiveID", SqlDbType.Int);
          _sql_param.Value = DBNull.Value;
          if (Request.Level >= 0x01 && Request.Level <= 0x20)
          {
            Int64 _progressive_id;

            if (Progressives.GetProgressiveFromTerminal(TerminalId, out _progressive_id, Trx, (Int32)Request.Level))
            {
              _sql_param.Value = _progressive_id;
            }
          }

          _num_rows_inserted = _sql_command.ExecuteNonQuery();
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        Log.Warning("Inserting HANDPAY [Exception]: TerminalId: " + TerminalId.ToString());
        return false;
      }

      if (_num_rows_inserted != 1)
      {
        Log.Warning("Inserting HANDPAY [Not updated]: TerminalId: " + TerminalId.ToString());
        return false;
      }

      return true;
    } // DB_InsertHandpay

    //------------------------------------------------------------------------------
    // PURPOSE: Fills hp_candidate_play_session_id, hp_candidate_prev_play_session_id  and hp_long_poll_1b_data of a handpay.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    //  RETURNS:
    //      - True: All went OK; False: otherwise
    //
    public static Boolean DB_UpdateHandPayCandidatePlaySession(Int32 TerminalId,
                                                               Decimal HandpayAmount,
                                                               WCP_MsgEGMHandpays Request,
                                                               SqlTransaction Trx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      _sb.AppendLine("UPDATE     HANDPAYS                                                               ");
      _sb.AppendLine("   SET     HP_CANDIDATE_PLAY_SESSION_ID         = @pCandidatePlaySessionID,       ");
      _sb.AppendLine("           HP_CANDIDATE_PREV_PLAY_SESSION_ID    = @pCandidatePrevPlaySessionID,   ");
      _sb.AppendLine("           HP_LONG_POLL_1B_DATA                 = @PpLongPoll1bData,              ");
      _sb.AppendLine("           HP_LEVEL                             = @pLevel,                        ");
      _sb.AppendLine("           HP_PROGRESSIVE_ID                    = @pProgressiveID                 ");
      _sb.AppendLine(" WHERE     HP_TERMINAL_ID                       = @pTerminalID                    ");
      _sb.AppendLine("   AND     HP_AMOUNT                            = @pHandpayAmount                 ");
      _sb.AppendLine("   AND     HP_TYPE                              = @pHandpayType                   ");
      _sb.AppendLine("   AND     HP_TRANSACTION_ID                    = @pTransactionID                 ");
      _sb.AppendLine("   AND     HP_CANDIDATE_PLAY_SESSION_ID IS NULL                                   ");

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          SqlParameter _sql_param;

          _cmd.Parameters.Add("@pCandidatePlaySessionID", SqlDbType.BigInt).Value = Request.PlaySessionID;
          _cmd.Parameters.Add("@pCandidatePrevPlaySessionID", SqlDbType.BigInt).Value = Request.PreviousPlaySessionID;
          _cmd.Parameters.Add("@PpLongPoll1bData", SqlDbType.Xml).Value = Request.ToXml();
          _cmd.Parameters.Add("@pTerminalID", SqlDbType.Int).Value = TerminalId;
          _cmd.Parameters.Add("@pHandpayAmount", SqlDbType.Money).Value = HandpayAmount;
          _cmd.Parameters.Add("@pHandpayType", SqlDbType.Int).Value = Request.HandpayType;
          _cmd.Parameters.Add("@pTransactionID", SqlDbType.BigInt).Value = Request.TransactionID;
          _cmd.Parameters.Add("@pLevel", SqlDbType.Int).Value = (Int32)Request.Level;
          _sql_param = _cmd.Parameters.Add("@pProgressiveID", SqlDbType.Int);
          _sql_param.Value = DBNull.Value;
          if (Request.Level >= 0x01 && Request.Level <= 0x20)
          {
            Int64 _progressive_id;

            if (Progressives.GetProgressiveFromTerminal(TerminalId, out _progressive_id, Trx, (Int32)Request.Level))
            {
              _sql_param.Value = _progressive_id;
            }
          }

          if (_cmd.ExecuteNonQuery() > 0)
          {
            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        Log.Warning("Updating HANDPAY [Exception]: TerminalId: " + TerminalId.ToString());
      }

      return false;
    } // DB_UpdateHandPayCandidatePlaySession

    //------------------------------------------------------------------------------
    // PURPOSE : Check if exists a handpay.
    //
    //  PARAMS :
    //      - INPUT :
    //          - TerminalId.
    //          - TransactionID.
    //          - HandpayCents.
    //
    //      - OUTPUT :    
    //
    // RETURNS :
    //      - True/False.
    //
    static public Boolean DB_ExistsHandpays(Int32 TerminalId, Int64 TransactionID, Int64 HandpayCents, SqlTransaction SqlTrx)
    {
      try
      {
        String _sql_str = "SELECT   HP_TERMINAL_ID                      "
                          + "       , HP_TRANSACTION_ID                   "
                          + "       , HP_AMOUNT    	                      "
                          + "  FROM   HANDPAYS                            "
                          + " WHERE   HP_TERMINAL_ID    = @pTerminalID    "
                          + "   AND   HP_TRANSACTION_ID = @pTransactionID "
                          + "   AND   HP_AMOUNT         = @pHandpayCents  ";

        using (SqlCommand _sql_cmd = new SqlCommand(_sql_str, SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pTerminalID", SqlDbType.Int).Value = TerminalId;
          _sql_cmd.Parameters.Add("@pTransactionID", SqlDbType.Int).Value = TransactionID;
          _sql_cmd.Parameters.Add("@pHandpayCents", SqlDbType.Int).Value = HandpayCents;

          return _sql_cmd.ExecuteScalar() != null;
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
        Log.Warning("Error reading handpay information from TerminalId: " + TerminalId.ToString() +
                                                       " TransactionID: " + TransactionID.ToString() +
                                                        " HandpayCents: " + HandpayCents.ToString());
        return false;
      } // try      
    } // ExistsHandpays

    public static Boolean DB_GetPlaySessionCashIn(Int64 PlaySessionId, Int64 PrevPlaySessionId, out Decimal RedeemCashIn, out Decimal NonRedeemCashIn, out Int64 AccountPromotionId, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      Int64 _play_session;
      Boolean _result;

      RedeemCashIn = 0;
      NonRedeemCashIn = 0;
      AccountPromotionId = 0;
      _sb = new StringBuilder();
      _result = true;

      if (PlaySessionId == 0 && PrevPlaySessionId == 0)
      {
        _result = false;
      }
      else
      {
        _play_session = PlaySessionId == 0 ? PrevPlaySessionId : PlaySessionId;

        _sb.AppendLine("    SELECT   PS_REDEEMABLE_CASH_IN                         ");
        _sb.AppendLine("           , PS_NON_REDEEMABLE_CASH_IN                     ");
        _sb.AppendLine("           , ISNULL(TE_ACCOUNT_PROMOTION_ID,0)             ");
        _sb.AppendLine("      FROM   PLAY_SESSIONS                                 ");
        _sb.AppendLine("INNER JOIN   TERMINALS ON TE_TERMINAL_ID = PS_TERMINAL_ID  ");
        _sb.AppendLine("     WHERE   PS_PLAY_SESSION_ID = @pPlaySessionId          ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt).Value = _play_session;
          using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
          {
            if (!_reader.Read())
            {
              _result = false;
            }
            else
            {
              if (_reader[0] != null && _reader.GetDecimal(0) > 0)
              {
                RedeemCashIn = _reader.GetDecimal(0);
              }
              if (_reader[1] != null && _reader.GetDecimal(1) > 0 && _reader[2] != null && _reader.GetInt64(2) > 0)
              {
                NonRedeemCashIn = _reader.GetDecimal(1);
                AccountPromotionId = _reader.GetInt64(2);
              }
              else
              {
                _result = false;
              }
            }
          }
        }
      }

      return _result;
    }


    #endregion Meters

  } // class

  public class Terminal
  {
    public Int32 terminal_id;
    public Int32 terminal_type;
    public String name;
    public String serial_number;
    public Boolean blocked;

    public String card_track_data;
    public Int64 account_id;
    public PlaySessionType play_session_type;
    public Int64 play_session_id;
    public Boolean stand_alone_session;
    public Int32 total_played_count;
    public Decimal total_played_amount;
    public Int32 total_won_count;
    public Decimal total_won_amount;
    public Decimal balance;
    public Boolean promotion;
    public Decimal promo_balance;
    public String provider_id;
    public String external_id;
    public TerminalStatus terminal_status;
    public Int64 money_collection_id;
    public String server_external_id;
    public String terminal_ISO_code;

    protected virtual Boolean Read(int TerminalID, SqlTransaction SqlTrx)
    {
      SqlCommand _sql_cmd;
      SqlDataReader _reader;
      String _sql_str;
      PlaySessionStatus _status;

      terminal_id = TerminalID;
      name = "";
      serial_number = "";
      blocked = true;
      card_track_data = "";
      account_id = 0;
      play_session_id = 0;
      play_session_type = PlaySessionType.NONE;
      total_played_count = 0;
      total_played_amount = 0;
      total_won_count = 0;
      total_won_amount = 0;
      balance = 0;
      stand_alone_session = false;
      _status = PlaySessionStatus.Opened;

      _sql_str = "GetTerminalAndMaxPlaySession";

      _sql_cmd = new SqlCommand(_sql_str);
      _sql_cmd.Connection = SqlTrx.Connection;
      _sql_cmd.Transaction = SqlTrx;
      _sql_cmd.Parameters.Add("@pTerminalID", SqlDbType.Int).Value = TerminalID;
      _sql_cmd.CommandType = System.Data.CommandType.StoredProcedure;

      _reader = null;

      try
      {
        _reader = _sql_cmd.ExecuteReader();
        if (_reader.Read())
        {
          if (!_reader.IsDBNull(0)) { name = _reader.GetString(0); }

          // MBF 15-NOV-2011 Prevent exception when reading a retired terminal
          if (!_reader.IsDBNull(1)) { serial_number = _reader.GetString(1); }
          else { serial_number = ""; }

          blocked = _reader.GetBoolean(2);

          if (!_reader.IsDBNull(3)) { card_track_data = _reader.GetString(3); }
          if (!_reader.IsDBNull(4)) { account_id = _reader.GetInt64(4); }
          if (!_reader.IsDBNull(5)) { play_session_id = _reader.GetInt64(5); }
          if (!_reader.IsDBNull(6)) { total_played_count = _reader.GetInt32(6); }
          if (!_reader.IsDBNull(7)) { total_played_amount = _reader.GetDecimal(7); }
          if (!_reader.IsDBNull(8)) { total_won_count = _reader.GetInt32(8); }
          if (!_reader.IsDBNull(9)) { total_won_amount = _reader.GetDecimal(9); }
          if (!_reader.IsDBNull(10)) { balance = _reader.GetDecimal(10); }

          play_session_type = PlaySessionType.NONE;
          if (!_reader.IsDBNull(11))
          {
            int _ps_type_value;

            _ps_type_value = _reader.GetInt32(11);

            if (_ps_type_value == (int)PlaySessionType.WIN)
            {
              play_session_type = PlaySessionType.WIN;
            }
            if (_ps_type_value == (int)PlaySessionType.ALESIS)
            {
              play_session_type = PlaySessionType.ALESIS;
            }
            if (_ps_type_value == (int)PlaySessionType.CADILLAC_JACK)
            {
              play_session_type = PlaySessionType.CADILLAC_JACK;
            }
          }
          if (!_reader.IsDBNull(12)) { stand_alone_session = _reader.GetBoolean(12); }
          if (!_reader.IsDBNull(13)) { _status = (PlaySessionStatus)_reader.GetInt32(13); }

          // DHA 27-JAN-2016: read terminal iso code
          if (!_reader.IsDBNull(14))
          {
            terminal_ISO_code = _reader.GetString(14);
          }
          else
          {
            terminal_ISO_code = CurrencyExchange.GetNationalCurrency();
          }
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
        Log.Warning("Exception throw: Error reading Terminal information. TermID: " + TerminalID.ToString());

        return false;
      }
      finally
      {
        if (_reader != null)
        {
          _reader.Close();
          _reader.Dispose();
          _reader = null;
        }
      }

      if (_status != PlaySessionStatus.Opened)
      {
        // Get Last Opened PlaySession
        _sql_str = "SELECT   PS_PLAY_SESSION_ID "
                + "       , PS_PLAYED_COUNT    "
                + "       , PS_PLAYED_AMOUNT   "
                + "       , PS_WON_COUNT       "
                + "       , PS_WON_AMOUNT      "
                + "       , PS_TYPE            "
                + "       , PS_STAND_ALONE     "
                + "       , AC_TRACK_DATA      "
                + "       , AC_ACCOUNT_ID      "
                + "       , AC_BALANCE         "
                + "  FROM   PLAY_SESSIONS      "
                + "       , ACCOUNTS           "
                + "       , ( SELECT   MAX(PS_PLAY_SESSION_ID) MAX_OPENED_SESSION_ID  "
                + "             FROM   PLAY_SESSIONS        "
                + "            WHERE   PS_STATUS       = 0  "   // Opened
                + "              AND   PS_TERMINAL_ID  = @pTerminalID  "
                + "         ) X  "
                + " WHERE   X.MAX_OPENED_SESSION_ID  = PS_PLAY_SESSION_ID "
                + "   AND   AC_ACCOUNT_ID  = PS_ACCOUNT_ID ";

        _sql_cmd = new SqlCommand(_sql_str);
        _sql_cmd.Connection = SqlTrx.Connection;
        _sql_cmd.Transaction = SqlTrx;
        _sql_cmd.Parameters.Add("@pTerminalID", SqlDbType.Int).Value = TerminalID;

        _reader = null;

        try
        {
          _reader = _sql_cmd.ExecuteReader();
          if (_reader.Read())
          {
            if (!_reader.IsDBNull(0)) { play_session_id = _reader.GetInt64(0); }
            if (!_reader.IsDBNull(1)) { total_played_count = _reader.GetInt32(1); }
            if (!_reader.IsDBNull(2)) { total_played_amount = _reader.GetDecimal(2); }
            if (!_reader.IsDBNull(3)) { total_won_count = _reader.GetInt32(3); }
            if (!_reader.IsDBNull(4)) { total_won_amount = _reader.GetDecimal(4); }

            play_session_type = PlaySessionType.NONE;
            if (!_reader.IsDBNull(5))
            {
              int _ps_type_value;

              _ps_type_value = _reader.GetInt32(5);

              if (_ps_type_value == (int)PlaySessionType.WIN)
              {
                play_session_type = PlaySessionType.WIN;
              }
              if (_ps_type_value == (int)PlaySessionType.ALESIS)
              {
                play_session_type = PlaySessionType.ALESIS;
              }
              if (_ps_type_value == (int)PlaySessionType.CADILLAC_JACK)
              {
                play_session_type = PlaySessionType.CADILLAC_JACK;
              }
            }
            if (!_reader.IsDBNull(6)) { stand_alone_session = _reader.GetBoolean(6); }
            if (!_reader.IsDBNull(7)) { card_track_data = _reader.GetString(7); }
            if (!_reader.IsDBNull(8)) { account_id = _reader.GetInt64(8); }
            if (!_reader.IsDBNull(9)) { balance = _reader.GetDecimal(9); }
          }
        }
        catch (Exception ex)
        {
          Log.Exception(ex);
          Log.Warning("Exception throw: Error reading PlaySession information. TermID: " + TerminalID.ToString());

          return false;
        }
        finally
        {
          if (_reader != null)
          {
            _reader.Close();
            _reader.Dispose();
            _reader = null;
          }
        }
      } // if

      return true;
    } // Read

    public static Terminal GetTerminal(int TerminalId, SqlTransaction SqlTrx)
    {
      if (WCP_BusinessLogic.CashlessMode == CashlessMode.WIN)
      {
        Terminal t;

        t = new Terminal();

        if (!t.Read(TerminalId, SqlTrx))
        {
          // DB error, return an empty Terminal object.
          return new Terminal();
        }

        return t;
      }

      if (WCP_BusinessLogic.CashlessMode == CashlessMode.ALESIS)
      {
        AleTerminal t;

        t = new AleTerminal();
        if (!t.Read(TerminalId, SqlTrx))
        {
          // DB error, return an empty AleTerminal object.
          return new AleTerminal();
        }

        return t;
      }

      return null;
    } //GetTerminal

    public Boolean UpdateEgmParameters(Int32 TerminalId,
                                       Int32 ValidationType,
                                       Int64 ValidationSequence,
                                       String SASVersion,
                                       String SASMachineName,
                                       Int32 Bonus,
                                       Int32 Features,
                                       String SerialNumberReport,
                                       Int64 AssetNumberReport,
                                       Int64 SASAccountDenom,
                                       SqlTransaction Trx)
    {
      Boolean _result;
      StringBuilder _sb;

      _result = false;
      _sb = new StringBuilder();

      try
      {
        //TODO DDM & AJQ!
        //if (ValidationSequence + 1 > Ticket.MAX_TERMINAL_SEQUENCE_ID)
        //{
        //  ValidationSequence = 1;
        //}
        _sb.AppendLine(" UPDATE   TERMINALS");
        _sb.AppendLine("    SET   TE_VALIDATION_TYPE  = @pValidationType");
        _sb.AppendLine("        , TE_SEQUENCE_ID      = CASE WHEN ISNULL(TE_SEQUENCE_ID, -1) < @pValidationSequence ");
        _sb.AppendLine("                                     THEN @pValidationSequence ");
        _sb.AppendLine("                                     ELSE TE_SEQUENCE_ID END   ");
        _sb.AppendLine("        , TE_SAS_VERSION      =  @pSASVersion                  ");
        _sb.AppendLine("        , TE_SAS_MACHINE_NAME =  @pSASMachineName              ");
        _sb.AppendLine("        , TE_BONUS_FLAGS      =  @pBonus                       ");
        _sb.AppendLine("        , TE_FEATURES_BYTES   =  @pFeatures                    ");
        _sb.AppendLine("        , TE_MACHINE_SERIAL_NUMBER   =  @pSASMachineSerialNumber ");                              //Reported
        _sb.AppendLine("        , TE_SERIAL_NUMBER    =  CASE WHEN TE_SERIAL_NUMBER IS NULL OR TE_SERIAL_NUMBER = '' ");  //Configured
        _sb.AppendLine("                                      THEN @pSASMachineSerialNumber ");
        _sb.AppendLine("                                      ELSE TE_SERIAL_NUMBER END");
        _sb.AppendLine("        , TE_MACHINE_ASSET_NUMBER   =  @pSASMachineAssetNumber ");                                //Reported
        _sb.AppendLine("        , TE_ASSET_NUMBER    =  CASE WHEN TE_ASSET_NUMBER IS NULL OR TE_ASSET_NUMBER = '' ");     //Configured
        _sb.AppendLine("                                      THEN @pSASMachineAssetNumber ");
        _sb.AppendLine("                                      ELSE TE_ASSET_NUMBER END");
        _sb.AppendLine("        , TE_SAS_ACCOUNTING_DENOM   =  @pSASAccountDenom ");     //Configured


        _sb.AppendLine("  WHERE   TE_TERMINAL_ID      =  @pTerminalId                  ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;
          _sql_cmd.Parameters.Add("@pValidationType", SqlDbType.Int).Value = ValidationType;
          _sql_cmd.Parameters.Add("@pValidationSequence", SqlDbType.BigInt).Value = ValidationSequence;
          _sql_cmd.Parameters.Add("@pSASVersion", SqlDbType.NVarChar, 10).Value = SASVersion;
          _sql_cmd.Parameters.Add("@pSASMachineName", SqlDbType.NVarChar, 10).Value = SASMachineName;
          _sql_cmd.Parameters.Add("@pBonus", SqlDbType.Int).Value = Bonus;
          _sql_cmd.Parameters.Add("@pFeatures", SqlDbType.Int).Value = Features;
          _sql_cmd.Parameters.Add("@pSASMachineSerialNumber", SqlDbType.NVarChar, 50).Value = SerialNumberReport;
          _sql_cmd.Parameters.Add("@pSASMachineAssetNumber", SqlDbType.BigInt).Value = AssetNumberReport;         
          _sql_cmd.Parameters.Add("@pSASAccountDenom", SqlDbType.Money).Value = (Decimal)SASAccountDenom / 100;

          _result = _sql_cmd.ExecuteNonQuery() == 1;
        }

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return _result;
    } // UpdateEgmParameters

    public static Boolean BonusLegacy(Int32 Bonus)
    {
      return (Bonus & 1) == 1;
    }

    public static Boolean BonusAFT(Int32 Bonus)
    {
      return (Bonus & 2) == 2;
    }

    public static Byte Feature1(Int32 Features)
    {
      return (Byte)(Features & 15);
    }

    public static Byte Feature2(Int32 Features)
    {
      return (Byte)((Features >> 4) & 15);
    }

    public static Byte Feature3(Int32 Features)
    {
      return (Byte)((Features >> 8) & 15);
    }


    public Boolean GetEgmParams(Int32 TerminalId,
                                out Boolean AllowCashableTicketOut,
                                out Boolean AllowPromotionalTicketOut,
                                out Boolean AllowTicketIn,
                                SqlTransaction SqlTrx)
    {
      String SerialNumberConfig;
      String SerialNumberReport;
      Int64 AssetNumberConfig;
      Int64 AssetNumberReport;
      Decimal SasAccountingDenom;

      AssetNumberConfig = 0;
      AssetNumberReport = 0;
      SerialNumberConfig = string.Empty;
      SerialNumberReport = string.Empty;

      return GetEgmParams(TerminalId, out AllowCashableTicketOut, 
                                      out AllowPromotionalTicketOut, 
                                      out AllowTicketIn,
                                      out SerialNumberConfig,
                                      out SerialNumberReport,
                                      out AssetNumberConfig,
                                      out AssetNumberReport, 
                                      out SasAccountingDenom, 
                                      SqlTrx);
    }

    public Boolean GetEgmParams(Int32 TerminalId,
                                out Boolean AllowCashableTicketOut,
                                out Boolean AllowPromotionalTicketOut,
                                out Boolean AllowTicketIn,
                                out String SerialNumberConfig,
                                out String SerialNumberReport,
                                out Int64 AssetNumberConfig,
                                out Int64 AssetNumberReport,
                                out Decimal SasAcountDenom,
                                SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      AllowCashableTicketOut = false;
      AllowPromotionalTicketOut = false;
      AllowTicketIn = false;
      SerialNumberConfig = String.Empty;
      SerialNumberReport = String.Empty;
      AssetNumberConfig = 0;
      AssetNumberReport = 0;
      SasAcountDenom = 0;

      _sb = new StringBuilder();
      _sb.AppendLine(" SELECT   TE_ALLOWED_REDEMPTION         ");
      _sb.AppendLine("        , TE_ALLOWED_CASHABLE_EMISSION  ");
      _sb.AppendLine("        , TE_ALLOWED_PROMO_EMISSION     ");
      _sb.AppendLine("        , TE_SERIAL_NUMBER              "); //Configurado
      _sb.AppendLine("        , TE_MACHINE_SERIAL_NUMBER      "); //Reportado
      _sb.AppendLine("        , TE_ASSET_NUMBER               "); //Configurado
      _sb.AppendLine("        , TE_MACHINE_ASSET_NUMBER       "); //Reportado
      _sb.AppendLine("        , TE_SAS_ACCOUNTING_DENOM       "); //Reportado
      _sb.AppendLine("        , TE_NAME                       "); //Name
      _sb.AppendLine("   FROM   TERMINALS                     ");
      _sb.AppendLine("  WHERE   TE_TERMINAL_ID = @pTerminalId ");

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int, 4).Value = TerminalId;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (!_reader.Read())
            {
              return false;
            }

            AllowTicketIn = _reader.IsDBNull(0) ? GeneralParam.GetBoolean("TITO", "AllowRedemption") : _reader.GetBoolean(0);
            AllowCashableTicketOut = _reader.IsDBNull(1) ? GeneralParam.GetBoolean("TITO", "CashableTickets.AllowEmission") : _reader.GetBoolean(1);
            AllowPromotionalTicketOut = _reader.IsDBNull(2) ? GeneralParam.GetBoolean("TITO", "PromotionalTickets.AllowEmission") : _reader.GetBoolean(2);

            if (!_reader.IsDBNull(3))
            {
              SerialNumberConfig = (String)_reader[3];
            }

            if (!_reader.IsDBNull(4))
            {
              SerialNumberReport = (String)_reader[4];
            }

            if (!_reader.IsDBNull(5))
            {
              AssetNumberConfig = (Int64)_reader[5];
            }

            if (!_reader.IsDBNull(6))
            {
              AssetNumberReport = (Int64)_reader[6];
            }

            if (!_reader.IsDBNull(7))
            {
              SasAcountDenom = (Decimal)_reader[7];
            }

            if (!_reader.IsDBNull(8))
            {
              this.name = (String)_reader[8];
          }
        }
      }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
        Log.Warning("Exception throw: Error reading Terminal information. TermID: " + TerminalId.ToString());

        return false;
      }

      return true;
    } // GetEgmParams

    /// <summary>
    /// Get terminal SAS accounting denom
    /// </summary>
    /// <param name="TerminalId"></param>
    /// <param name="SasAcountDenom"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public Boolean GetEgmSasAcountDenom(Int32 TerminalId,
                                        out Decimal SasAcountDenom,
                                        SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      SasAcountDenom = 0;

      _sb = new StringBuilder();
      _sb.AppendLine(" SELECT   TE_SAS_ACCOUNTING_DENOM         ");
      _sb.AppendLine("   FROM   TERMINALS                       ");
      _sb.AppendLine("  WHERE   TE_TERMINAL_ID  =  @pTerminalId ");

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int, 4).Value = TerminalId;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (!_reader.Read())
            {
              return false;
            }

            if (!_reader.IsDBNull(0))
            {
              SasAcountDenom = (Decimal)_reader[0];
            }

            return true;
          }
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
        Log.Warning("Exception throw: Error reading SAS accounting denomination. TermID: " + TerminalId.ToString());
      }

      return false;
    }
  } // Class Terminal

} // namespace
