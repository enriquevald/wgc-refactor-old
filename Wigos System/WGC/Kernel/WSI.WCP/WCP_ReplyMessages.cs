//------------------------------------------------------------------------------
// Copyright � 2010 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_ReplyMessages.cs
// 
//   DESCRIPTION: WCP_ReplyMessages class
// 
//        AUTHOR: Alberto Cuesta
// 
// CREATION DATE: 20-MAY-2010
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 20-MAY-2010 ACC    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using WSI.Common;
using System.Data;

namespace WSI.WCP
{
  class WCP_ReplyMessages
  {
    //------------------------------------------------------------------------------
    // PURPOSE : Process Reply Messages
    //
    //  PARAMS :
    //      - INPUT :
    //          - WcpReply: Wcp message
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public static void Process (Int32 TerminalId, WCP_Message WcpReply)
    {
      WCP_CommandStatus _command_status;
      SqlConnection _sql_conn;
      SqlTransaction _sql_trx;
      Boolean _error;
      String _response_data;

      _sql_conn = null;
      _sql_trx = null;
      _error = true;

      try
      {
        // Get Sql Connection 
        _sql_conn = WGDB.Connection();

        // Begin Transaction
        _sql_trx = _sql_conn.BeginTransaction();

        switch (WcpReply.MsgHeader.ResponseCode)
        {
          case WCP_ResponseCodes.WCP_RC_OK:
            _command_status = WCP_CommandStatus.RepliedOk;
          break;

          default:
            _command_status = WCP_CommandStatus.RepliedError;

            Log.Error("Reply Message GetLogger status error: " + WcpReply.MsgHeader.ResponseCode.ToString());
          break;
        }

        _response_data = "";

        switch (WcpReply.MsgHeader.MsgType)
        {
          case WCP_MsgTypes.WCP_MsgGetLoggerReply:
          {
            WCP_MsgGetLoggerReply _reply;

            _reply = (WCP_MsgGetLoggerReply)WcpReply.MsgContent;

            if (_command_status == WCP_CommandStatus.RepliedOk)
            {
              _response_data = _reply.LoggerMessages;
            }
          }
          break;

          case WCP_MsgTypes.WCP_MsgExecuteCommandReply:
          {
            // No data response
          }
          break;

          default:
          {
            Log.Error("Unexpected MsgType: " + WcpReply.MsgHeader.MsgType.ToString());
          }
          break;
        } // switch

        // Execute commands with 0 --> Not inserted into WCP_COMMANDS table (reply not update status). Ex: WCP_CMD_BONUS_AWARD, WCP_CMD_BONUS_STATUS, 
        //                             WCP_CMD_GAMEGATEWAY (this message inserted into GAMEGATEWAY_COMMAND_MESSAGES)
        if (WcpReply.MsgHeader.SequenceId > 0)
        {
          if (!WCP_Commands.DB_UpdateWcpCommand(WcpReply.MsgHeader.SequenceId, TerminalId, _command_status, _response_data, _sql_trx))
          {
            Log.Error("Calling DB_UpdateWcpCommand: TerminalId: " + TerminalId.ToString() + " CommandId: " + WcpReply.MsgHeader.SequenceId.ToString());
          }
        }

        _sql_trx.Commit();

        _error = false;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      finally
      {
        if (_sql_trx != null)
        {
          if (_sql_trx.Connection != null)
          {
            try 
            {
              _sql_trx.Rollback();
            }
            catch (Exception _ex)
            { 
              Log.Exception (_ex);
            }
            if ( !_error )
            {
              Log.Message("*** ERROR DB TRX ROLLBACK TerminalId: " + TerminalId.ToString() + " CommandId: " + WcpReply.MsgHeader.SequenceId.ToString()); 
            }
          }
          _sql_trx = null;
        }

        // Close connection
        if (_sql_conn != null)
        {
          try 
          {
            _sql_conn.Close();
          }
          catch (Exception _ex)
          { 
            Log.Exception (_ex);
          }
          _sql_conn = null;
        }
      }

      return;
    }
  }
}
