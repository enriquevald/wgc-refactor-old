using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using WSI.Common;
using System.Threading;

namespace WSI.WCP
{

  static public partial class Alesis
  {
    private static ReaderWriterLock db_lock = new ReaderWriterLock();
    private static ConnectionState connection_state = ConnectionState.Closed;
    private static String connection_string = "";

    public enum StatusCode
    {
      Success,
      AccountNumberNotValid,
      AccountAlreadyInUse,
      MachineNumberNotValid,
      SessionNumberNotValid,
      AccessDenied,
      NotConnectedToDatabase,
      Error,
    }

    public enum Procedure
    {
      SessionStart,
      SessionUpdate,
      SessionEnd,
      SendEvent
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Initialize alesis class (db_lock)
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    //        
    //   NOTES :
    //
    public static Boolean Init()
    {
      SqlConnection sql_conn;
      String str_conn;
      Boolean connected;

      str_conn = ConnectionString();
      if (str_conn == "")
      {
        Log.Warning("*** CONNECTION TO 3GS: Not configured!");

        return false;
      }

      sql_conn = null;
      connected = false;

      try
      {
        sql_conn = DBConnection();
        if (sql_conn != null)
        {
          connected = (sql_conn.State == ConnectionState.Open);
        }
      }
      finally
      {
        if (sql_conn != null)
        {
          sql_conn.Close();
          sql_conn = null;
        }
      }

      if (connected)
      {
        //Decimal cur_bal;
        //Int64   cur_session_id;
        //String  status_text;
        //Alesis.StatusCode status_code;
        
        
        Log.Message("*** CONNECTION TO 3GS: Succeded");


        //status_code = Alesis.ExecuteProcedure(Procedure.SessionEnd, "TRACK DATA", "VENDOR ID", "SERIAL NUMBER", 0, 0, 0, 0, 0, 0, 0, 0, out cur_bal, out cur_session_id, out status_text);
        
      }
      else
      {
        Log.Warning("*** CONNECTION TO 3GS: Failed!");
      }

      return connected;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Get Alesis DB Connection
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    //        
    //   NOTES :
    //
    private static SqlConnection DBConnection()
    {
      SqlConnection conn;

      conn = null;

      db_lock.AcquireWriterLock(Timeout.Infinite);
      try
      {
        conn = new SqlConnection();

        for (int idx = 0; idx < 3; idx++)
        {
          try
          {
            if (connection_state != ConnectionState.Open)
            {
              connection_state = ConnectionState.Connecting;
            }

            conn.Close();
            conn.ConnectionString = ConnectionString();
            conn.Open();

            connection_state = conn.State;

            return conn;
          }
          catch (Exception ex)
          {
            Log.Exception(ex);

            conn.Close();

            // Connection couldn't be open
            // Reset connection string
            connection_string = "";
            connection_state = ConnectionState.Closed;
          }

          System.Threading.Thread.Sleep(1000);
        }

        // Connection couldn't be open (all tries done)
        // Reset connection string
        connection_string = "";

        return conn;
      }
      finally
      {
        if (conn != null)
        {
          if (conn.State != ConnectionState.Open)
          {
            Log.Message("DATABASE, returned connection state: " + conn.State.ToString());
          }
        }
        else
        {
          Log.Message("DATABASE, returned NULL connection");
        }

        db_lock.ReleaseWriterLock();
      }

    }

    //------------------------------------------------------------------------------
    // PURPOSE: Initialize alesis class (db_lock and database connection)
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    //        
    //   NOTES :
    //
    private static String ConnectionString()
    {
      SqlConnectionStringBuilder csb;
      SqlConnection sql_conn;
      SqlCommand sql_command;
      SqlDataReader reader;
      String str_conn;

      str_conn = "";
      sql_conn = null;
      reader = null;

      try
      {
        db_lock.AcquireWriterLock(Timeout.Infinite);

        if (connection_string == "")
        {
          // Get Win Connection
          sql_conn = WGDB.Connection();

          sql_command = new SqlCommand("SELECT AP_SQL_SERVER_IP_ADDRESS " +
                                       "     , AP_SQL_DATABASE_NAME " +
                                       "     , AP_SQL_USER " +
                                       "     , AP_SQL_USER_PASSWORD " +
                                       " FROM  ALESIS_PARAMETERS ", sql_conn);

          reader = sql_command.ExecuteReader();

          if (reader.Read())
          {
            if (!reader.IsDBNull(0)
                && !reader.IsDBNull(1)
                && !reader.IsDBNull(2)
                && !reader.IsDBNull(3))
            {
              // Build str_conn
              csb = new SqlConnectionStringBuilder();
              csb.DataSource = reader.GetString(0); ;
              csb.InitialCatalog = reader.GetString(1);
              csb.IntegratedSecurity = false;
              csb.UserID = reader.GetString(2);
              csb.Password = reader.GetString(3);
              csb.Pooling = true;
              csb.AsynchronousProcessing = true;
              csb.MaxPoolSize = 500;
              csb.MinPoolSize = 10;
              csb.ConnectTimeout = 30;

              connection_string = csb.ConnectionString;
            }
          }
        }
        str_conn = connection_string;
      }
      catch (Exception ex)
      {
        Log.Exception(ex);

        Log.Warning("Error reading Alesis parameters.");
      }
      finally
      {
        if (reader != null)
        {
          reader.Close();
          reader.Dispose();
          reader = null;
        }

        if (sql_conn != null)
        {
          sql_conn.Close();
          sql_conn = null;
        }

        if (str_conn == "")
        {
          connection_string = "";
        }

        db_lock.ReleaseWriterLock();
      }

      return str_conn;

    }

    //------------------------------------------------------------------------------
    // PURPOSE: Get Status Code from alesis procedure return code
    //
    //  PARAMS:
    //      - INPUT:
    //          - Value: alesis procedure return code
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    //        
    //   NOTES :
    //
    private static StatusCode GetStatusCode(Procedure ProcedureID, int EventId, int Value)
    {
      switch (ProcedureID)
      {
        case Procedure.SessionStart:
          switch (Value)
          {
            case 0:
              return StatusCode.Success;
            case 1:
              return StatusCode.AccountNumberNotValid;
            case 2:
              return StatusCode.AccountAlreadyInUse;
            case 3:
              return StatusCode.MachineNumberNotValid;
            case 4:
              return StatusCode.AccessDenied;
            default:
              return StatusCode.Error;
          }
        //break;

        case Procedure.SessionUpdate:
          switch (Value)
          {
            case 0:
              return StatusCode.Success;
            case 1:
              return StatusCode.AccountNumberNotValid;
            case 2:
              return StatusCode.MachineNumberNotValid;
            case 3:
              return StatusCode.SessionNumberNotValid;
            case 4:
              return StatusCode.AccessDenied;
            default:
              return StatusCode.Error;
          }
        //break;

        case Procedure.SessionEnd:
          switch (Value)
          {
            case 0:
              return StatusCode.Success;
            case 1:
              return StatusCode.AccountNumberNotValid;
            case 2:
              return StatusCode.MachineNumberNotValid;
            case 3:
              return StatusCode.SessionNumberNotValid;
            case 4:
              return StatusCode.AccessDenied;
            default:
              return StatusCode.Error;
          }
        //break;

        case Procedure.SendEvent:
          // AJQ 30-JUL-2014, Ignore any error on events ... 
          //                  Different alesis servers return different return codes
          //                  On any error the session was closed and then the jackpot was not awarded.
          return StatusCode.Success;

        default:
          return StatusCode.Error;
      }
    }
  }

  /// <summary>
  /// Alesis terminal
  /// </summary>
  public class AleTerminal : WSI.WCP.Terminal
  {
    public enum ConfigurationStatus
    {
      OK = 0
    , PENDING = 1
    , ERROR = 2
    }

    public String ale_vendor_id;
    public Int32 ale_machine_id;
    public Int64 ale_session_id;
    public ConfigurationStatus ale_status;

    //------------------------------------------------------------------------------
    // PURPOSE: Reads all the Terminal data
    //
    //  PARAMS:
    //      - INPUT:
    //          - TerminalID
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    //      - True / false
    //        
    //   NOTES :
    //

    protected override Boolean Read(Int32 TerminalID, SqlTransaction SqlTrx)
    {
      SqlCommand sql_cmd;
      SqlDataReader reader;
      String sql_str;

      ale_vendor_id = "";
      ale_machine_id = 0;
      ale_session_id = 0;

      sql_str = "SELECT   AP_VENDOR_ID       "
              + "       , AT_MACHINE_ID      "
              + "       , AT_SESSION_ID	     "
              + "       , AT_STATUS	         "
              + "  FROM   ALESIS_VIEW        "
              + " WHERE   TE_TERMINAL_ID = @pTerminalID";

      sql_cmd = new SqlCommand(sql_str);
      sql_cmd.Connection = SqlTrx.Connection;
      sql_cmd.Transaction = SqlTrx;
      sql_cmd.Parameters.Add("@pTerminalID", SqlDbType.Int).Value = TerminalID;

      reader = null;

      try
      {
        reader = sql_cmd.ExecuteReader();
        if (reader.Read())
        {
          ale_vendor_id  = reader.GetString(0);
          ale_machine_id = 0;
          if ( !reader.IsDBNull(1))
          {
            ale_machine_id = reader.GetInt32(1);
          }
          ale_session_id = 0;
          if (!reader.IsDBNull(2))
          {
            ale_session_id = reader.GetInt64(2);
          }
          ale_status = 0;
          if (!reader.IsDBNull(3))
          {
            ale_status = (ConfigurationStatus)reader.GetInt32(3);
          }
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
        Log.Warning("Exception throw: Error reading Terminal information" + TerminalID.ToString());

        return false;
      }
      finally
      {
        if (reader != null)
        {
          reader.Close();
          reader.Dispose();
          reader = null;
        }
      }

      if (!base.Read(TerminalID, SqlTrx))
      {
        return false;
      }

      return true;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Links the session ID with the terminal
    //
    //  PARAMS:
    //      - INPUT:
    //          - Trx
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    //      - True / false
    //        
    //   NOTES :
    //
    public bool LinkSessionID(SqlTransaction SqlTrx)
    {
      return UpdateSessionID(this.ale_session_id, SqlTrx);
    } // LinkSessionID

    //------------------------------------------------------------------------------
    // PURPOSE: Unlinks any session with the terminal
    //
    //  PARAMS:
    //      - INPUT:
    //          - Trx
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    //      - True / false
    //        
    //   NOTES :
    //
    public bool UnlinkSessionID(SqlTransaction SqlTrx)
    {
      return UpdateSessionID (0, SqlTrx);
    } // UnlinkSessionID
    
    //------------------------------------------------------------------------------
    // PURPOSE: Set alesis session data
    //
    //  PARAMS:
    //      - INPUT:
    //          - TerminalID
    //          - SessionID
    //          - Trx
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    //      - True / false
    //        
    //   NOTES :
    //
    private bool UpdateSessionID (Int64 SessionID, SqlTransaction SqlTrx)
    {
      SqlCommand sql_cmd;
      String sql_str;

      sql_cmd = null;
      
      try
      {
        sql_str = " UPDATE   ALESIS_TERMINALS "
                + "    SET   AT_SESSION_ID    = @pSessionID  "
                + "  WHERE   AT_TERMINAL_ID   = @pTerminalID ";

        sql_cmd = new SqlCommand(sql_str);
        sql_cmd.Connection = SqlTrx.Connection;
        sql_cmd.Transaction = SqlTrx;

        sql_cmd.Parameters.Add("@pSessionID", SqlDbType.BigInt).Value = DBNull.Value;
        if (SessionID != 0)
        {
          sql_cmd.Parameters["@pSessionID"].Value = SessionID;
        }
        sql_cmd.Parameters.Add("@pTerminalID", SqlDbType.Int).Value = this.terminal_id;

        if (sql_cmd.ExecuteNonQuery() != 1)
        {
          return false;
        }
        return true;
      }
      catch (Exception ex)
      {
        Log.Exception(ex);

        return false;
      }
      finally
      {
        if (sql_cmd != null)
        {
          sql_cmd.Dispose();
        }
      }
    } // SetSessionID

    //------------------------------------------------------------------------------
    // PURPOSE: Set Terminal configuration status
    //
    //  PARAMS:
    //      - INPUT:
    //          - Value: alesis procedure return code
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    //        
    //   NOTES :
    //
    public void SetTerminalConfigurationStatus(ConfigurationStatus ConfStatus, SqlTransaction SqlTrx)
    {
      SqlCommand sql_cmd;
      String sql_str;

      sql_cmd = null;

      try
      {
        sql_str = " UPDATE   ALESIS_TERMINALS "
                + "    SET   AT_STATUS        = @pStatus  "
                + "  WHERE   AT_TERMINAL_ID   = @pTerminalID ";

        sql_cmd = new SqlCommand(sql_str);
        sql_cmd.Connection = SqlTrx.Connection;
        sql_cmd.Transaction = SqlTrx;

        sql_cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = (Int32) ConfStatus;
        sql_cmd.Parameters.Add("@pTerminalID", SqlDbType.Int).Value = this.terminal_id;

        sql_cmd.ExecuteNonQuery();

        return;
      }
      catch (Exception ex)
      {
        Log.Exception(ex);

        return;
      }
      finally
      {
        if (sql_cmd != null)
        {
          sql_cmd.Dispose();
        }
      }
    } // SetTerminalConfigurationStatus

  }

}    

