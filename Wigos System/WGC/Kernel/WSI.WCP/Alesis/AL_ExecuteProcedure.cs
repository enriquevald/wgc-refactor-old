using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using WSI.Common;


namespace WSI.WCP
{
  static public partial class Alesis
  {
    //------------------------------------------------------------------------------
    // PURPOSE: Start Alesis card session
    //
    //  PARAMS:
    //      - INPUT:
    //          - TerminalID
    //          - TrackData
    //          - Trx
    //
    //      - OUTPUT:
    //          - AccountID
    //          - Balance
    //
    // RETURNS: 
    //      - StatusCode
    //        
    //   NOTES :
    //
    public static StatusCode StartCardSession(int TerminalID,
                                               Int64 AccountID,
                                               String TrackData,
                                               Int64 PlaySessionID,
                                               out Decimal Balance,
                                               out String HolderNameToDisplay,
                                               SqlTransaction Trx)
    {
      AleTerminal _terminal;
      StatusCode _status_code;
      String _status_text;

      HolderNameToDisplay = "";
      Balance = 0;
      _terminal = (AleTerminal)Terminal.GetTerminal(TerminalID, Trx);

      _status_code = ExecuteProcedure(Procedure.SessionStart,
                                      TrackData,
                                      _terminal.ale_vendor_id,
                                      _terminal.serial_number,
                                      _terminal.ale_machine_id,
                                      PlaySessionID,
                                      0, 0, 0, 0, 0, 0, 0, 0, 0,
                                      out _terminal.balance, out _terminal.ale_session_id, out _status_text);

      if (_status_code != StatusCode.Success)
      {
        return _status_code;
      }

      if (!String.IsNullOrEmpty(_status_text))
      {
        String[] _welcome_msg = { "Bienvenido", "Welcome" };
        String _aux;

        _aux = _status_text.Trim();

        foreach (String _welcome in _welcome_msg)
        {
          if (_aux.StartsWith(_welcome, StringComparison.CurrentCultureIgnoreCase))
          {
            HolderNameToDisplay = _aux.Substring(_welcome.Length).Trim();
            break;
          }
        }
      }

      // Set alesis session id
      if (!_terminal.LinkSessionID(Trx))
      {
        Log.Error("SetSessionID, TerminalID:" + _terminal.terminal_id.ToString()
                 + " SN:" + _terminal.serial_number
                 + " AlesisSessionID:" + _terminal.ale_session_id);

      }

      StringBuilder _sb;
      SqlCommand sql_command;
      SqlParameter _param;
      Int32 num_rows_inserted;

      _sb = new StringBuilder();


      _sb.AppendLine("SET @pAccountId = (SELECT AC_ACCOUNT_ID FROM ACCOUNTS WHERE AC_TRACK_DATA = @p1)");
      _sb.AppendLine("IF @pAccountId IS NULL ");
      _sb.AppendLine("BEGIN");
      _sb.AppendLine("  EXECUTE CreateAccount @pAccountId OUTPUT");
      _sb.AppendLine("END");
      _sb.AppendLine("UPDATE  ACCOUNTS SET AC_TYPE = 3, AC_TRACK_DATA = @p1, AC_BLOCKED = 0, AC_BALANCE = @pBalance, AC_RE_BALANCE = @pBalance WHERE AC_ACCOUNT_ID = @pAccountId");

      sql_command = new SqlCommand(_sb.ToString());
      sql_command.Connection = Trx.Connection;
      sql_command.Transaction = Trx;

      sql_command.Parameters.Add("@p1", SqlDbType.NVarChar).Value = TrackData;
      sql_command.Parameters.Add("@pBalance", SqlDbType.Money).Value = _terminal.balance;

      _param = sql_command.Parameters.Add("@pAccountId", SqlDbType.BigInt);
      _param.Direction = ParameterDirection.Output;

      num_rows_inserted = sql_command.ExecuteNonQuery();

      if (num_rows_inserted < 1)
      {
        Log.Error("DB_CreateCard. Card could not be inserted.");
      }

      AccountID = (long)_param.Value;

      Balance = _terminal.balance;

      return _status_code;
    } // StartCardSession

    //------------------------------------------------------------------------------
    // PURPOSE: End Alesis card session
    //
    //  PARAMS:
    //      - INPUT:
    //          - AleTerminal
    //          - Trx
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    //      - StatusCode
    //        
    //   NOTES :
    //
    public static StatusCode EndCardSession(AleTerminal Terminal,
                                            SqlTransaction Trx)
    {
      StatusCode status_code;
      String status_text;
      Decimal aux_terminal_balance;
      long aux_current_session_id;

      status_code = ExecuteProcedure(Procedure.SessionEnd,
                                     Terminal.card_track_data,
                                     Terminal.ale_vendor_id,
                                     Terminal.serial_number,
                                     Terminal.ale_machine_id,
                                     Terminal.play_session_id,
                                     Terminal.ale_session_id,
                                     Terminal.total_played_count,
                                     Terminal.total_played_amount,
                                     Terminal.total_won_count,
                                     Terminal.total_won_amount,
                                     Terminal.balance,
                                     0, 0, 0,
                                     out aux_terminal_balance,
                                     out aux_current_session_id,
                                     out status_text);

      // Set alesis session id
      if (!Terminal.UnlinkSessionID(Trx))
      {
        Log.Error("UnlinkSessionID, TerminalID:" + Terminal.terminal_id.ToString()
                 + " SN:" + Terminal.serial_number
                 + " AlesisSessionID:" + Terminal.ale_session_id);

      }

      return status_code;
    } // EndCardSession

    //------------------------------------------------------------------------------
    // PURPOSE: Report play to Alesis
    //
    //  PARAMS:
    //      - INPUT:
    //          - AleTerminal
    //          - Trx
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    //      - StatusCode
    //        
    //   NOTES :
    //
    public static StatusCode ReportPlay(AleTerminal Terminal, SqlTransaction Trx)
    {
      StatusCode status_code;
      ////StatusCode status_end;
      String status_text;
      Decimal aux_terminal_balance;
      long aux_current_session_id;
      int idx_try;

      status_code = StatusCode.Error;

      for (idx_try = 0; idx_try < 3; idx_try++)
      {
        status_code = ExecuteProcedure(Procedure.SessionUpdate,
                                       Terminal.card_track_data,
                                       Terminal.ale_vendor_id,
                                       Terminal.serial_number,
                                       Terminal.ale_machine_id,
                                       Terminal.play_session_id,
                                       Terminal.ale_session_id,
                                       Terminal.total_played_count,
                                       Terminal.total_played_amount,
                                       Terminal.total_won_count,
                                       Terminal.total_won_amount,
                                       Terminal.balance,
                                       0, 0, 0,
                                       out aux_terminal_balance,
                                       out aux_current_session_id,
                                       out status_text);

        if (status_code != StatusCode.Error
            && status_code != StatusCode.NotConnectedToDatabase)
        {
          break;
        }
      } // for (idx_try) 

      // AJQ 30-JUL-2014, Always return Success to avoid closing the session
      return StatusCode.Success;

    } // ReportPlay

    //------------------------------------------------------------------------------
    // PURPOSE: Update Alesis card session
    //
    //  PARAMS:
    //      - INPUT:
    //          - AleTerminal
    //          - EventId
    //          - PrizeAmount
    //          - Trx
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    //      - StatusCode
    //        
    //   NOTES :
    //
    public static StatusCode ReportEvent(AleTerminal Terminal, Int32 EventId, Decimal PrizeAmount, SqlTransaction Trx)
    {
      StatusCode _status_code;
      String _status_text;
      Decimal _aux_terminal_balance;
      long _aux_current_session_id;
      int _idx_try;

      _status_code = StatusCode.Error;

      for (_idx_try = 0; _idx_try < 3; _idx_try++)
      {
        _status_code = ExecuteProcedure(Procedure.SendEvent,
                                       Terminal.card_track_data,
                                       Terminal.ale_vendor_id,
                                       Terminal.serial_number,
                                       Terminal.ale_machine_id,
                                       Terminal.play_session_id,
                                       Terminal.ale_session_id,
                                       Terminal.total_played_count,
                                       Terminal.total_played_amount,
                                       Terminal.total_won_count,
                                       Terminal.total_won_amount,
                                       Terminal.balance,
                                       0,
                                       EventId,
                                       PrizeAmount,
                                       out _aux_terminal_balance,
                                       out _aux_current_session_id,
                                       out _status_text);

        if (_status_code == StatusCode.NotConnectedToDatabase)
        {
          // AJQ 04-AUG-2014, Retry only when not connected to database
          continue;
        }

        break;
      } // for (idx_try) 

      // AJQ 30-JUL-2014, Always return Success to avoid closing the session
      return StatusCode.Success;
    } // ReportEvent

    //------------------------------------------------------------------------------
    // PURPOSE: Execute Alesis store procedure
    //
    //  PARAMS:
    //      - INPUT:
    //         - ProcedureID
    //         - TrackData
    //         - VendorID
    //         - SerialNumber
    //         - MachineID
    //         - SessionID
    //         - TotalGamesPlayed
    //         - TotalAmountPlayed
    //         - TotalGamesWon
    //         - TotalAmountWon
    //         - FinalBalance
    //         - CurrJackpotAmount
    //         - EventId
    //         - PrizeAmount
    //         - CurrentBalance
    //         - CurrentSessionID
    //         - StatusText
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    //      - StatusCode
    //        
    //   NOTES :
    //
    private static StatusCode ExecuteProcedure(Procedure ProcedureID,
                                               String TrackData,
                                               String VendorID,
                                               String SerialNumber,
                                               int MachineID,
                                               Int64 PlaySessionID,
                                               long SessionID,
                                               int TotalGamesPlayed,
                                               Decimal TotalAmountPlayed,
                                               int TotalGamesWon,
                                               Decimal TotalAmountWon,
                                               Decimal FinalBalance,
                                               Decimal CurrJackpotAmount,
                                               Int32 EventId,
                                               Decimal PrizeAmount,
                                               out Decimal CurrentBalance,
                                               out long CurrentSessionID,
                                               out String StatusText)
    {
      StatusCode _status_code;
      String _status_text;
      String _sql_txt;

      _status_code = StatusCode.Error;
      _status_text = "ERROR";
      _sql_txt = "";

      CurrentBalance = 0;
      CurrentSessionID = 0;
      StatusText = _status_text;

      switch (ProcedureID)
      {
        case Procedure.SessionStart:
          _sql_txt = "EXECUTE zsp_SessionStart  @pAccountID, @pVendorID, @pSerialNumber, @pMachineID, "
                  + "                          @pCurrJackpotAmt, "
                  + "                          @pEmgSessionID";
          break;

        case Procedure.SessionUpdate:
          _sql_txt = "EXECUTE zsp_SessionUpdate @pAccountID, @pVendorID, @pSerialNumber, @pMachineID, "
                  + "                          @pSessionID, "
                  + "                          @pCreditsPlayed, @pCreditsWon, @pGamesPlayed, @pGamesWon,  "
                  + "                          @pAcctBalance, "
                  + "                          @pCurrJackpotAmt";
          break;

        case Procedure.SessionEnd:
          _sql_txt = "EXECUTE zsp_SessionEnd    @pAccountID, @pVendorID, @pSerialNumber, @pMachineID, "
                  + "                          @pSessionID, "
                  + "                          @pCreditsPlayed, @pCreditsWon, @pGamesPlayed, @pGamesWon,  "
                  + "                          @pAcctBalance, "
                  + "                          @pCurrJackpotAmt";
          break;

        case Procedure.SendEvent:
          _sql_txt = "EXECUTE zsp_SendEvent    @pAccountID, @pVendorID, @pSerialNumber, @pMachineID, "
                  + "                         @pEventID, "
                  + "                         @pPauoutAmt";
          break;

        default:
          return StatusCode.Error;
      }

      try
      {
        using (SqlConnection _conn = Alesis.DBConnection())
        {
          if (_conn == null)
          {
            return StatusCode.NotConnectedToDatabase;
          }
          if (_conn.State != ConnectionState.Open)
          {
            return StatusCode.NotConnectedToDatabase;
          }
          using (SqlCommand _cmd = new SqlCommand(_sql_txt, _conn))
          {
            _cmd.Parameters.Add("@pAccountID", SqlDbType.NVarChar, 24).Value = TrackData;
            _cmd.Parameters.Add("@pVendorID", SqlDbType.NVarChar, 16).Value = VendorID;
            _cmd.Parameters.Add("@pSerialNumber", SqlDbType.NVarChar, 30).Value = SerialNumber;
            _cmd.Parameters.Add("@pMachineID", SqlDbType.Int).Value = MachineID;

            if (ProcedureID == Procedure.SessionUpdate || ProcedureID == Procedure.SessionEnd)
            {
              _cmd.Parameters.Add("@pSessionID", SqlDbType.BigInt).Value = SessionID;
              _cmd.Parameters.Add("@pCreditsPlayed", SqlDbType.Money).Value = TotalAmountPlayed;
              _cmd.Parameters.Add("@pCreditsWon", SqlDbType.Money).Value = TotalAmountWon;
              _cmd.Parameters.Add("@pGamesPlayed", SqlDbType.Int).Value = TotalGamesPlayed;
              _cmd.Parameters.Add("@pGamesWon", SqlDbType.Int).Value = TotalGamesWon;
              _cmd.Parameters.Add("@pAcctBalance", SqlDbType.Money).Value = FinalBalance;
            }

            if (ProcedureID != Procedure.SendEvent)
            {
              _cmd.Parameters.Add("@pCurrJackpotAmt", SqlDbType.Int).Value = CurrJackpotAmount;
            }

            if (ProcedureID == Procedure.SessionStart)
            {
              _cmd.Parameters.Add("@pEmgSessionID", SqlDbType.BigInt).Value = PlaySessionID;
            }

            if (ProcedureID == Procedure.SendEvent)
            {
              _cmd.Parameters.Add("@pEventID", SqlDbType.Int).Value = EventId;
              _cmd.Parameters.Add("@pPauoutAmt", SqlDbType.Money).Value = PrizeAmount;
            }

            using (SqlDataReader _reader = _cmd.ExecuteReader())
            {
              if (!_reader.Read())
              {
                return _status_code;
              }

              _status_code = Alesis.GetStatusCode(ProcedureID, EventId, _reader.GetInt32(0));
              _status_text = "";
              if (!_reader.IsDBNull(1))
              {
                _status_text = _reader.GetString(1);
              }
              StatusText = _status_text;

              if (_status_code == StatusCode.Success)
              {
                if (ProcedureID == Procedure.SessionStart)
                {
                  CurrentSessionID = _reader.GetInt64(2);
                  CurrentBalance = _reader.GetDecimal(3);
                }
                else
                {
                  CurrentSessionID = SessionID;
                  CurrentBalance = FinalBalance;
                }
              }

              try { _reader.Close(); }
              catch { ;}
            }
          }

          try { _conn.Close(); }
          catch { ;}

          return _status_code;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        StatusText = "ERROR: " + _ex.Message;

        return _status_code;
      }
    }
  }
}
