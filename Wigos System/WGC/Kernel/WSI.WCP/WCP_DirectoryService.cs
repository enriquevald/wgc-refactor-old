//------------------------------------------------------------------------------
// Copyright (c) 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_DirectoryService.cs
// 
//   DESCRIPTION: The WCP directory service
// 
//        AUTHOR: Andreu Julia
// 
// CREATION DATE: 12-MAR-2007
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 12-MAR-2007 AJQ    First release.
// 12-SEP-2008 AJQ    WCP Directory Service inherits from UdpServer
//------------------------------------------------------------------------------

using System;
using System.Text;
using System.Xml;
using System.Net;
using System.Net.Sockets;
using System.Data;
using System.Threading;

using WSI.Common;

using WSI.WCP;


namespace WSI.WCP
{
  /// <summary>
  /// The WCP Directory Service
  /// </summary>
  public class WCP_DirectoryService : UdpServer
  {
    #region MEMBERS
    // ------------------------------------------------------------------------- MEMBERS
    IPEndPoint   m_service_ip_endpoint;         // Service IP EndPoint

    #endregion // MEMBERS

    #region PUBLIC METHODS
    // ------------------------------------------------------------------------- PUBLIC METHODS
    /// <summary>
    /// Creates a new instance of a WCP Directory Service
    /// </summary>
    public WCP_DirectoryService(IPEndPoint ServiceIPEndPoint)
      : base(Protocol.PORT_NUMBER)
    {
      m_service_ip_endpoint = ServiceIPEndPoint;
      
      base.Start();
    } // WCP_DirectoryService

    #endregion // PUBLIC METHODS

    #region PRIVATE METHODS
    // ------------------------------------------------------------------------- PRIVATE METHODS

    /// <summary>
    /// Process the received UPD datagram.
    /// - Only WCP_MsgRequestService is allowed
    /// - 
    /// </summary>
    /// <param name="RemoteIPEndPoint">Source IP endpoint</param>
    /// <param name="Datagram">Received Datagram</param>
    protected override void ProcessDataReceived(IPEndPoint RemoteIPEndPoint, Byte[] Datagram)
    {
      String xml_msg;
      WCP_Message wcp_request;
      WCP_Message wcp_response;
      WCP_MsgRequestService request;
      WCP_MsgRequestServiceReply response;

      try 
      {
        // Data received?
        if (Datagram.Length == 0)
        {
          return;
        }
        
        // Datagram to String using the current Encoding
        xml_msg = this.Encoding.GetString(Datagram);

        // Build the WCP Request
        wcp_request = WCP_Message.CreateMessage(xml_msg);

        // Only WCP_MsgRequestService allowed
        if (wcp_request.MsgHeader.MsgType != WCP_MsgTypes.WCP_MsgRequestService)
        {
          // Ignore message
          return;
        }
        //
        // Process Request: WCP_MsgRequestService
        //
        request = (WCP_MsgRequestService)wcp_request.MsgContent;
        
        // - Check service name
        if (String.Equals(request.ServiceName.Trim(), Protocol.SERVICE_NAME, StringComparison.InvariantCultureIgnoreCase) == false)
        {
          return;
        }

        wcp_response = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_MsgRequestServiceReply);
        response = (WCP_MsgRequestServiceReply)wcp_response.MsgContent;
        wcp_response.MsgHeader.SequenceId = wcp_request.MsgHeader.SequenceId;
        response.ServiceAddress = m_service_ip_endpoint;
        wcp_response.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_OK;

        // Send the response to the received ReplyTo address
        // AJQ 15-SEP-08, reply to the source IP (ignore the ReplyToAddress)
        Send(RemoteIPEndPoint, wcp_response.ToXml());
        NetLog.Add(NetEvent.MessageSent, RemoteIPEndPoint.ToString(), "UDP " + wcp_response.ToXml());
      }
      catch (Exception)
      {
      }
      finally 
      {
      }
    }
    #endregion // PRIVATE METHODS

  } // class DirectoryService

  public class WCP_QueryService : UDP_QueryService
  {
    protected override int RequestToPortNumber()
    {
      return Protocol.PORT_NUMBER;
    }
    protected override int ServicePortNumber()
    {
      return Protocol.SERVICE_PORT_NUMBER;
    }
    protected override string ProtocolName()
    {
      return Protocol.PROTOCOL_NAME;
    }

    protected override Byte[] GetRequestDatagram(IPEndPoint LocalEndPoint)
    {
      WCP_Message wcp_request;
      WCP_MsgRequestService wcp_content;

      wcp_request = WCP_Message.CreateMessage(WCP_MsgTypes.WCP_MsgRequestService);

      wcp_content = (WCP_MsgRequestService)wcp_request.MsgContent;

      wcp_content.ServiceName = Protocol.SERVICE_NAME;
      wcp_content.ReplyToUDPAddress = LocalEndPoint;

      return Encoding.UTF8.GetBytes(wcp_request.ToXml());
    }

    protected override IPEndPoint GetResponseIPEndPoint(byte[] ReceivedDatagram)
    {
      String xml_response;
      WCP_Message wcp_response;
      WCP_MsgRequestServiceReply wcp_content;
      IPEndPoint ipe;

      ipe = null;

      try
      {
        xml_response = Encoding.UTF8.GetString(ReceivedDatagram);
        wcp_response = WCP_Message.CreateMessage(xml_response);
        if (wcp_response.MsgHeader.ResponseCode == WCP_ResponseCodes.WCP_RC_OK)
        {
          wcp_content = (WCP_MsgRequestServiceReply)wcp_response.MsgContent;

          ipe = wcp_content.ServiceAddress;
        }
      }
      catch (Exception)
      {
      }
      finally
      {
      }

      wcp_content = null;
      wcp_response = null;
      xml_response = null;

      return ipe;

    }
  } // WCP_QueryService
} // namespace WSI.WCP 
