﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_MonitorData.cs
// 
//   DESCRIPTION: Thread Monitor Data
// 
//        AUTHOR: Alberto Marcos
// 
// CREATION DATE: 21-JUL-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 21-JUL-2015 AMF    First release
// 11-NOV-2015 GMV    TFS 5275 : HighRollers Extended definition
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using WSI.Common;
using System.Threading;

namespace WSI.WCP
{
  public static class WCP_MonitorData
  {
    #region Enums

    public enum MONITOR_DATA_TYPE
    {
      CUSTOMERS_PLAYING = 1,
      MB_AND_CASHIER_SESSIONS_CASH = 2,
    }

    #endregion Enums

    #region Public Methods


    //------------------------------------------------------------------------------
    // PURPOSE : Initializes MonitorData thread
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public static void Init()
    {
      Thread _thread;

      _thread = new Thread(GenerateMonitorDataRegisters);
      _thread.Name = "GenerateMonitorDataRegisters";
      _thread.Start();

    } // Init

    //------------------------------------------------------------------------------
    // PURPOSE : Generate Customers Playing in MONITOR_DATA
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public static void GenerateMonitorDataRegisters()
    {
      DataSet _ds_customers_playing;
      DataSet _ds_mb_and_cashier_sessions_cash;
      String _sql_str;
      Boolean _data_present;

      Int32 _current_ticks;
      _current_ticks = Misc.GetTickCount();

      try
      {
        while (true)
        {
          System.Threading.Thread.Sleep(10000);

          _sql_str = "EXECUTE CustomersPlaying";
          _data_present = false;

          using (DB_TRX _db_trx = new DB_TRX())
          {
            if (ExecuteProcedure(_sql_str, out _ds_customers_playing, _db_trx.SqlTransaction))
            {
              _data_present = true;
              if (!InsertMonitorData(_ds_customers_playing, MONITOR_DATA_TYPE.CUSTOMERS_PLAYING, _db_trx.SqlTransaction))
              {
                Log.Message("Error: Error on InsertMonitorData Customers Playing not updated");
              }


            }
            else
            {
                Log.Message("Error: Error on MonitorData - ExecuteProcedure Customers Playing not updated");
            }
          }


        if (Misc.GetElapsedTicks(_current_ticks) > 60000 )
        {
            using (DB_TRX _db_trx_highroller = new DB_TRX())
            {
                if(_data_present)
                {
                    if (!CustomAlarms.ConditionalRegister(CustomAlarms.CustomAlarmsTypes.Highroller, 0, _db_trx_highroller.SqlTransaction, _ds_customers_playing))
                    {
                        Log.Message("Error: Error on ConditionalRegister - HighRoller status not updated");
                    }
                    else
                    {
                        _db_trx_highroller.Commit();
                    }
                }
            }

            _current_ticks = Misc.GetTickCount();
        }
   

          _sql_str = "EXECUTE MBAndCashierSessionsCashMonitor";

          using (DB_TRX _db_trx = new DB_TRX())
          {
            if (ExecuteProcedure(_sql_str, out _ds_mb_and_cashier_sessions_cash, _db_trx.SqlTransaction))
            {
              if (!InsertMonitorData(_ds_mb_and_cashier_sessions_cash, MONITOR_DATA_TYPE.MB_AND_CASHIER_SESSIONS_CASH, _db_trx.SqlTransaction))
              {
                Log.Message("Error: Error on InsertMonitorData MB and Cashier sessions cash not updated");
              }
            }
            else
            {
                Log.Message("Error: Error on MonitorData - ExecuteProcedure MB and Cashier sessions cash not updated");
            }
          }

        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    }


    #endregion Public Methods

    #region Private Methods

    //------------------------------------------------------------------------------
    // PURPOSE : execute global procedure
    //
    //  PARAMS :
    //      - INPUT :
    //                - String Sql
    //                - DataSet Ds
    //                - SqlTransaction SqlTrx
    //
    //      - OUTPUT :
    //
    // RETURNS : True or False
    //
    //   NOTES :
    //
    private static Boolean ExecuteProcedure(String Sql, out DataSet Ds, SqlTransaction SqlTrx)
    {
      Ds = new DataSet();

      try
      {
        using (SqlCommand _sql_cmd = new SqlCommand(Sql, SqlTrx.Connection, SqlTrx))
        {
          using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
          {
            _sql_da.Fill(Ds);

            return true;
          }
        }
      }
      catch (Exception _e)
      {
        Log.Error(_e.Message);
      }

      return false;
    } // ExecuteProcedure

    //------------------------------------------------------------------------------
    // PURPOSE: Insert MONITOR_DATA
    // 
    //  PARAMS:
    //      - INPUT: 
    //                - DataSet Ds
    //                - SqlTransaction SqlTrx
    //
    //      - OUTPUT:
    //
    // RETURNS: - True: Inserted ok. False: Otherwise.
    // 
    //   NOTES:
    //
    private static Boolean InsertMonitorData(DataSet Ds, MONITOR_DATA_TYPE DataType, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("IF (NOT EXISTS ( SELECT   1                   ");
        _sb.AppendLine("                   FROM   MONITOR_DATA        ");
        _sb.AppendLine("                  WHERE   MD_TYPE = @pType )) ");
        _sb.AppendLine(" BEGIN                                        ");
        _sb.AppendLine("  INSERT INTO   MONITOR_DATA                  ");
        _sb.AppendLine("              ( MD_TYPE                       ");
        _sb.AppendLine("              , MD_DATETIME                   ");
        _sb.AppendLine("              , MD_SCHEMA                     ");
        _sb.AppendLine("              , MD_DATA )                     ");
        _sb.AppendLine("       VALUES                                 ");
        _sb.AppendLine("              ( @pType                        ");
        _sb.AppendLine("              , @pDate                        ");
        _sb.AppendLine("              , @pSchema                      ");
        _sb.AppendLine("              , @pData )                      ");
        _sb.AppendLine(" END                                          ");
        _sb.AppendLine(" ELSE                                         ");
        _sb.AppendLine(" BEGIN                                        ");
        _sb.AppendLine("  UPDATE   MONITOR_DATA                       ");
        _sb.AppendLine("     SET   MD_DATA     = @pData               ");
        _sb.AppendLine("         , MD_DATETIME = @pDate               ");
        _sb.AppendLine("         , MD_SCHEMA   = @pSchema             ");
        _sb.AppendLine("   WHERE   MD_TYPE     = @pType               ");
        _sb.AppendLine("END                                           ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pType", SqlDbType.BigInt).Value = DataType;
          _sql_cmd.Parameters.Add("@pDate", SqlDbType.DateTime).Value = WGDB.Now;
          _sql_cmd.Parameters.Add("@pSchema", SqlDbType.Xml).Value = Ds.GetXmlSchema();
          _sql_cmd.Parameters.Add("@pData", SqlDbType.Xml).Value = Ds.GetXml();

          if (_sql_cmd.ExecuteNonQuery() == 1)
          {
            SqlTrx.Commit();

            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // InsertMonitorData

    #endregion Private Methods
  }
}
