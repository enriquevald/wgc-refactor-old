//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_SASMeters.cs
// 
//   DESCRIPTION: SASMeters updates
// 
//        AUTHOR: Rub�n Rodr�guez
// 
// CREATION DATE: 04-NOV-2013
// 
// REVISION HISTORY:
//  
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 04-NOV-2013 RRR        First release.
// 11-DIC-2013 NMR        Fixed error while saving bills-meters
// 17-DIC-2013 NMR        Ticket-Amount in MCM are saved in unities (not cents)
// 21-DIC-2013 NMR        Optimized some procedures; fixed error
// 27-DEC-2013 RRR        Added SASMetersHistory process
// 07-JAN-2014 RRR        Fixed Bug WIGOSTITO-946: Error en el log WCP en "InsertSASMetersHistory"
// 20-JAN-2014 JRM        Fixed bug of PK violation with Sas_meters_history
// 22-JAN-2014 JRM        Fixed bug of empty translated Meter code and also the updated date field in meters
// 13-JAN-2014 MPO        The meters should be saved in tsmh with delta <> 0
// 24-FEB-2015 JRM        Fixed various bugs with historification of sas meters.
// 02-MAR-2014 XIT        Code refactored and fixed ticket historification defect
// 04-MAR-2014 LEM & RCI  Fixed Bug WIGOSTITO-1109: SetModified when no necessary
// 31-MAR-2014 MPO        Fixed Bug WIGOSTITO-1180: Retired terminals in the sas_meters process
// 02-MAY-2014 MPO        It is possible that in the hourly historification lose deltas (WIG-878).
// 07-MAY-2014 MPO        Error when historicize tickets hourly (WIG-878).
// 26-MAY-2014 HBB        Moved GetSasMeters to SASMeters.cs
// 13-JAN-2015 RCI & AJQ & XIT    Daily historification become default process done everyday
// 19-JAN-2015 DRV        Fixed Bug WIG-1940: Daily historification hour not calculed properly
// 22-JAN-2015 HBB        Fixed Bug WIG-1949: Money collection expected values not calculed properly.
// 30-JAN-2015 DRV & MPO  Fixed Bug WIG-1905
// 26-FEB-2015 DRV        Fixed Bug WIG-2117: Play sessions are being created when meters are historified
// 11-MAR-2015 DRV        Evolutionary improvement WIG-2147: Historification musn't stop when an error occurred on a terminal
// 25-MAR-2015 MPO & RCI  Fixed Bug WIG-2151: The WCP Service must always save the "From" date.
// 07-SEP-2015 MPO        TFS ITEM 2194: SAS16: Estad�sticas multi-denominaci�n: WCP
// 14-SEP-2015 DHA        Product Backlog Item 3705: Added coins from terminals
// 30-SEP-2015 MPO        Bug 4837 - WSI.WCP.WCP_Exception unhandled error
// 17-MAR-2016 DLL        Bug 10756: history don' work
// 04-OCT-2016 JMM & JBC  Bug 16497: Last 24h alarm error.
// 14-FEB-2017 JBP & CCG  Bug 16497:Alarma "Contadores sin reportar en las �ltimas 24 horas" aparece sin cumplir la condici�n de 24h
// 27-JUN-2017 DHA        Bug 28409:[Ticket #5090] Not match Collection History with Colletion Details
//------------------------------------------------------------------------------

using System;
using System.Threading;
using System.Data.SqlClient;
using WSI.Common;
using System.Data;
using System.Text;
using System.Collections;
using WSI.Common.TITO;
using System.Collections.Generic;

namespace WSI.WCP
{

  public class WCP_SASMeters
  {
    private const Int32 DEFAULT_WAITING_TIME_BETWEEN_ALARMS = 23;

    private static DataTable m_gaming_hall_meters_by_session = null;
    private static DataTable m_bills_meters_by_session = null;
    private static DataTable m_tickets_meters_by_session = null;
    private static Dictionary<Int32, SAS_Meter.MeterProperties> m_bills_meters_properties = null;
    private static Dictionary<Int32, SAS_Meter.MeterProperties> m_tickets_meters_properties = null;
    private static Dictionary<Int32, Dictionary<Int32, DateTime>> m_alarm_meters_reported = null;

    //LEM 31-03-2014: Moved to WSI.Common.SAS_Meter
    //private const Int32 MIN_TICKET_METER = 128;
    //private const Int32 MAX_TICKET_METER = 139;
    //private const Int32 MIN_BILL_METER = 64;
    //private const Int32 MAX_BILL_METER = 87;

    #region Bill Meters Update

    //------------------------------------------------------------------------------
    // PURPOSE : Updates CashierTerminalMoney CTM and MCM table with delta SAS meters values
    //
    //  PARAMS :
    //      - INPUT :
    //          - DeltaSASMeters: DataTable with Delta SAS meters
    //          - Trx
    //          - CreateInfo : True if calls comes frome change stacker, otherwise(thread calls) false.
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True or false
    //
    //   NOTES :
    //      - true: cashier_terminal_money table updated.
    //      - false: error.
    //
    static public Boolean UpdateBillsMetersCTMAndMCM(DataRow[] Meters, SqlTransaction Trx)
    {
      StringBuilder _sb;
      Int32 _num_rows_inserted;
      Int32 _num_deltas;
      Int64 _num_bills;
      Decimal _amount_to_add;
      DataRow _mcm_row;
      SAS_Meter.MeterProperties _meter_properties;
      Int64 _session_id;
      Int64 _collection_id;
      Int32 _terminal_id;

      try
      {
        InitBillsData();

        _num_deltas = 0;
        foreach (DataRow _dr in Meters)
        {
          // AJQ & RCI 27-FEB-2014, Mark as modified when 'delta' is non zero.
          _num_bills = (Int64)_dr["TSM_DELTA_VALUE"];
          if (_num_bills == 0)
          {
            continue;
          }

          _meter_properties = SAS_Meter.GetBillsMeterProperties((Int32)_dr["TSM_METER_CODE"]);
          if (_meter_properties == null)
          {
            continue;
          }

          // DHA: if meter type is amount, avoid to use denomination
          if (_meter_properties.MeterType == SAS_Meter.METER_TYPE.AMOUNT)
          {
            _amount_to_add = _num_bills / 100m;
          }
          else
          {
            _amount_to_add = _num_bills * _meter_properties.Denomination;
          }

          if (_amount_to_add == 0)
          {
            continue;
          }

          _dr["TSM_BILL_DENOMINATION"] = _meter_properties.Denomination;
          _num_deltas++;

          //Fill MetersBySession 
          _session_id = (Int64)_dr["TSM_SESSION_ID"];
          _collection_id = (Int64)_dr["MC_COLLECTION_ID"];
          _terminal_id = (Int32)_dr["TSM_TERMINAL_ID"];

          _mcm_row = m_bills_meters_by_session.Rows.Find(_session_id);
          if (_mcm_row == null)
          {
            _mcm_row = m_bills_meters_by_session.NewRow();
            _mcm_row["MCM_SESSION_ID"] = _session_id;

            _mcm_row["MCM_MONEY_COLLECTION_ID"] = _collection_id;
            _mcm_row["MCM_TERMINAL_ID"] = _terminal_id;
            _mcm_row["MCM_BILLS_IN_STACKER_NUM"] = 0;
            m_bills_meters_by_session.Rows.Add(_mcm_row);
          }

          if (_meter_properties.MeterType == SAS_Meter.METER_TYPE.AMOUNT)
          {
            _mcm_row[_meter_properties.SqlColumn] = _amount_to_add;
            _mcm_row["MCM_BILLS_IN_STACKER_NUM"] = (Int32)_mcm_row["MCM_BILLS_IN_STACKER_NUM"];
          }
          else
          {
            _mcm_row[_meter_properties.SqlColumn] = _num_bills;
            _mcm_row["MCM_BILLS_IN_STACKER_NUM"] = (Int32)_mcm_row["MCM_BILLS_IN_STACKER_NUM"] + _num_bills;
          }

        }

        if (_num_deltas == 0)
        {
          return true;
        }

        //
        // CASHIER_TERMINAL_MONEY
        //
        _sb = new StringBuilder();
        // DHA: avoid to update/insert coins in cashier_terminal_money
        _sb.AppendLine(" IF @pDenomination = 0 ");
        _sb.AppendLine("    RETURN ");

        _sb.AppendLine(" IF NOT EXISTS(SELECT TOP 1 * FROM   CASHIER_TERMINAL_MONEY");
        _sb.AppendLine("                             WHERE  [CTM_TERMINAL_ID]         = @pTerminalId");
        _sb.AppendLine("                               AND  [CTM_DENOMINATION]        = @pDenomination");
        _sb.AppendLine("                               AND  [CTM_SESSION_ID]          = @pSessionId )");
        _sb.AppendLine(" INSERT INTO  CASHIER_TERMINAL_MONEY");
        _sb.AppendLine("            ( CTM_SESSION_ID");
        _sb.AppendLine("            , CTM_TERMINAL_ID");
        _sb.AppendLine("            , CTM_DENOMINATION");
        _sb.AppendLine("            , CTM_MONEY_COLLECTION_ID");
        _sb.AppendLine("            , CTM_QUANTITY )");
        _sb.AppendLine(" VALUES     ( @pSessionId, @pTerminalId, @pDenomination, @pCollectionId, @pDeltaValue )");

        _sb.AppendLine(" ELSE");
        _sb.AppendLine(" UPDATE   CASHIER_TERMINAL_MONEY");
        _sb.AppendLine("    SET   CTM_QUANTITY              = CTM_QUANTITY + @pDeltaValue");
        _sb.AppendLine("  WHERE   [CTM_TERMINAL_ID]         = @pTerminalId");
        _sb.AppendLine("    AND   [CTM_DENOMINATION]        = @pDenomination");
        _sb.AppendLine("    AND   [CTM_SESSION_ID]          = @pSessionId");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pSessionId", SqlDbType.BigInt).SourceColumn = "TSM_SESSION_ID";
          _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).SourceColumn = "TSM_TERMINAL_ID";
          _cmd.Parameters.Add("@pDenomination", SqlDbType.Money).SourceColumn = "TSM_BILL_DENOMINATION";
          _cmd.Parameters.Add("@pDeltaValue", SqlDbType.Int).SourceColumn = "TSM_DELTA_VALUE";
          _cmd.Parameters.Add("@pCollectionId", SqlDbType.BigInt).SourceColumn = "MC_COLLECTION_ID";
          _cmd.Parameters.Add("@pMeterCode", SqlDbType.Int).SourceColumn = "TSM_METER_CODE";

          using (SqlDataAdapter _da = new SqlDataAdapter())
          {
            // Execute Batch Update
            _da.UpdateCommand = WGDB.DownsizeCommandText(_cmd);
            _da.ContinueUpdateOnError = true;
            _da.UpdateBatchSize = 500;
            _da.UpdateCommand.UpdatedRowSource = UpdateRowSource.None;

            _num_rows_inserted = _da.Update(Meters);

            // Errors control
            if (_num_rows_inserted != _num_deltas)
            {
              foreach (DataRow _dr in Meters)
              {
                if (_dr.HasErrors)
                {
                  Log.Message(_dr["TSM_TERMINAL_ID"].ToString()
                          + "/" + _dr["TSM_SESSION_ID"].ToString()
                          + "/" + _dr["MC_COLLECTION_ID"].ToString()
                          + "/" + _dr["TSM_BILL_DENOMINATION"].ToString()
                          + "/" + _dr["TSM_DELTA_VALUE"].ToString()
                          + " (TSM_TERMINAL_ID/TSM_SESSION_ID/MC_COLLECTION_ID/TSM_BILL_DENOMINATION/TSM_DELTA_VALUE)");
                  Log.Error(_dr.RowError);
                }
              }

              return false;
            }
          }
        }

        //
        // MONEY_COLLECTION_METERS
        //
        _sb = new StringBuilder();
        _sb.AppendLine(" IF NOT EXISTS(SELECT TOP 1 * FROM   MONEY_COLLECTION_METERS");
        _sb.AppendLine("                             WHERE  [MCM_SESSION_ID]              = @pSessionId )");
        _sb.AppendLine(" INSERT INTO  MONEY_COLLECTION_METERS");
        _sb.AppendLine("           ( MCM_MONEY_COLLECTION_ID");
        _sb.AppendLine("           , MCM_SESSION_ID");
        _sb.AppendLine("           , MCM_BILLS_IN_STACKER_NUM");
        _sb.AppendLine("           , MCM_TERMINAL_ID");
        _sb.AppendLine("           , MCM_STARTED");
        _sb.AppendLine("           , MCM_LAST_REPORTED");
        foreach (SAS_Meter.MeterProperties _props in m_bills_meters_properties.Values)
        {
          _sb.AppendLine("        , " + _props.SqlColumn);
        }
        _sb.AppendLine("           )");
        _sb.AppendLine(" VALUES     ( @pCollectionId");
        _sb.AppendLine("            , @pSessionId");
        _sb.AppendLine("            , @pBillsInStackerNum");
        _sb.AppendLine("            , @pTerminalId");
        _sb.AppendLine("            , GETDATE()");
        _sb.AppendLine("            , GETDATE()");
        foreach (SAS_Meter.MeterProperties _props in m_bills_meters_properties.Values)
        {
          _sb.AppendLine("            , " + _props.SqlParameter);
        }
        _sb.AppendLine("            )");
        _sb.AppendLine(" ELSE");
        _sb.AppendLine(" UPDATE   MONEY_COLLECTION_METERS");
        _sb.AppendLine("    SET   MCM_LAST_REPORTED    = GETDATE()");
        _sb.AppendLine("        , MCM_BILLS_IN_STACKER_NUM = ISNULL(MCM_BILLS_IN_STACKER_NUM, 0) + @pBillsInStackerNum ");
        foreach (SAS_Meter.MeterProperties _props in m_bills_meters_properties.Values)
        {
          _sb.AppendLine("        , " + _props.SqlColumn + " = ISNULL(" + _props.SqlColumn + ", 0) + ISNULL(" + _props.SqlParameter + ", 0) ");
        }
        _sb.AppendLine("  WHERE   MCM_SESSION_ID = @pSessionId");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pSessionId", SqlDbType.BigInt).SourceColumn = "MCM_SESSION_ID";
          _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).SourceColumn = "MCM_TERMINAL_ID";
          _cmd.Parameters.Add("@pCollectionId", SqlDbType.BigInt).SourceColumn = "MCM_MONEY_COLLECTION_ID";
          _cmd.Parameters.Add("@pBillsInStackerNum", SqlDbType.Int).SourceColumn = "MCM_BILLS_IN_STACKER_NUM";

          foreach (SAS_Meter.MeterProperties _props in m_bills_meters_properties.Values)
          {
            _cmd.Parameters.Add(_props.SqlParameter, _props.SqlType).SourceColumn = _props.SqlColumn;
          }

          using (SqlDataAdapter _da = new SqlDataAdapter())
          {
            // Execute Batch Update
            _da.InsertCommand = WGDB.DownsizeCommandText(_cmd);
            _da.ContinueUpdateOnError = true;
            _da.UpdateBatchSize = 500;
            _da.InsertCommand.UpdatedRowSource = UpdateRowSource.None;

            _num_rows_inserted = _da.Update(m_bills_meters_by_session);

            if (_num_rows_inserted != m_bills_meters_by_session.Rows.Count)
            {
              // Errors control
              foreach (DataRow _dr in m_bills_meters_by_session.Rows)
              {
                if (_dr.HasErrors)
                {
                  Log.Message(_dr["MCM_TERMINAL_ID"].ToString()
                          + "/" + _dr["MCM_SESSION_ID"].ToString()
                          + "/" + _dr["MCM_MONEY_COLLECTION_ID"].ToString()
                          + " (MCM_TERMINAL_ID/MCM_SESSION_ID/MCM_MONEY_COLLECTION_ID)");
                  Log.Error(_dr.RowError);
                }
              }

              return false;
            }
          }
        }

        return true;
      }// try
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }// UpdateCashierTerminalMoney

    //------------------------------------------------------------------------------
    // PURPOSE : Updates TerminalMoney and generates cashier and movile bank movements for TITO
    //
    //  PARAMS :
    //      - INPUT :
    //          - Meters: DataTable with Delta SAS meters
    //          - Trx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True or false
    //
    //   NOTES :
    //      - true: Operation Succeed.
    //      - false: error.
    //
    static public Boolean UpdateTerminalMoneyAndGenerateCashierAndMBMovements(DataRow[] Meters, SqlTransaction Trx)
    {
      Int64 _num_bills;
      SAS_Meter.MeterProperties _meter_properties;
      Decimal _amount_to_add;
      Int64 _session_id;
      Int64 _collection_id;
      Int32 _terminal_id;
      MB_USER_TYPE _mb_user_type;
      GU_USER_TYPE _gu_user_type;
      String _terminal_name;
      Int64 _money_collection_id;
      Int32 _cashier_terminal_id;
      Int64 _mobile_bank_id;
      String _mobile_bank_track_data;
      Int64 _mobile_bank_session_id;
      String _user_name;
      Int32 _user_id;
      Int64 _play_session_id;
      String _provider_id;
      String _cashier_name;
      Int64 _account_id;
      String _card_track_data;
      Int64 _cashier_session_id;

      try
      {
        if (!Common.TITO.Utils.IsTitoMode())
        {

          return true;
        }

        foreach (DataRow _dr in Meters)
        {
          _num_bills = (Int64)_dr["TSM_DELTA_VALUE"];
          if (_num_bills == 0)
          {
            continue;
          }

          _meter_properties = SAS_Meter.GetBillsMeterProperties((Int32)_dr["TSM_METER_CODE"]);
          if (_meter_properties == null)
          {
            continue;
          }

          // DHA: added coins
          if (_meter_properties.MeterType == SAS_Meter.METER_TYPE.AMOUNT)
          {
            _amount_to_add = _num_bills / 100m;
          }
          else
          {
            _amount_to_add = _num_bills * _meter_properties.Denomination;
          }
          if (_amount_to_add == 0)
          {
            continue;
          }

          _session_id = (Int64)_dr["TSM_SESSION_ID"];
          _collection_id = (Int64)_dr["MC_COLLECTION_ID"];
          _terminal_id = (Int32)_dr["TSM_TERMINAL_ID"];

          Cashier.GetMobileBankAndGUIUserType(TerminalTypes.SAS_HOST, out _mb_user_type, out _gu_user_type);

          if (!WCP_PlaySessionLogic.GetTitoSessionObjects(_terminal_id,
                                                          0,
                                                          false,
                                                          _mb_user_type,
                                                          out _terminal_name,
                                                          out _play_session_id,
                                                          out _cashier_session_id,
                                                          out _money_collection_id,
                                                          out _account_id, //Terminal Virtual Account Id
                                                          out _card_track_data,
                                                          out _cashier_terminal_id,
                                                          out _mobile_bank_id,
                                                          out _mobile_bank_track_data,
                                                          out _mobile_bank_session_id,
                                                          out _user_name,
                                                          out _user_id,
                                                          out _provider_id,
                                                          out _cashier_name,
                                                          Trx))
          {
            throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR, "MoneyEvent - GetTitoSessionObjects");
          }

          // Log discrepancy between CashierSessionId and MoneyCollectionId
          if (_session_id != _cashier_session_id)
          {
            Log.Error(String.Format("ECP_SASMeters.UpdateTerminalMoneyAndGenerateCashierAndMBMovements - Discrepancy CashierSessionId: {0}/{1}", _session_id, _cashier_session_id));
          }

          if (_collection_id != _money_collection_id)
          {
            Log.Error(String.Format("ECP_SASMeters.UpdateTerminalMoneyAndGenerateCashierAndMBMovements - Discrepancy MoneyCollectionId: {0}/{1}", _collection_id, _money_collection_id));
          }

          // DHA: process coins amount. "_num_bills" is the total coins
          if (_meter_properties.MeterType == SAS_Meter.METER_TYPE.AMOUNT)
          {
            if (!WCP_MoneyEvents.TITO_TransferBillFromTerminal(_terminal_id, _terminal_name, _account_id, _card_track_data,
                                                                 _mobile_bank_id, _amount_to_add, 0,
                                                                 0, _gu_user_type, Trx))
            {
              throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR, "MoneyEvent - TITO_TransferBillFromTerminal (Coins)");
            }

            if (!WCP_MoneyEvents.DB_UpdateMoneyCollectionCoins(_amount_to_add, _cashier_session_id, Trx))
            {
              throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR);  // TODO: Add a specific WCP_ResponseCode for the MoneyEvent.
            }

            if (!TerminalStatusFlags.SetTerminalStatus(_terminal_id, TerminalStatusFlags.WCP_TerminalEvent.CoinsIn, Trx))
            {
              throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR, "MoneyEvent - SetTerminalStatus");
            }
          }
          else
          {
            // Process bills
            for (Int32 _idx = 0; _idx < _num_bills; _idx++)
            {
              if (!WCP_MoneyEvents.TITO_TransferBillFromTerminal(_terminal_id, _terminal_name, _account_id, _card_track_data,
                                                                 _mobile_bank_id, _meter_properties.Denomination, 0,
                                                                 0, _gu_user_type, Trx))
              {
                throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR, "MoneyEvent - TITO_TransferBillFromTerminal");
              }

              if (!WCP_MoneyEvents.DB_UpdateMoneyCollection(_meter_properties.Denomination, _cashier_session_id, Trx))
              {
                throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR);  // TODO: Add a specific WCP_ResponseCode for the MoneyEvent.
              }
            }

            if (!TerminalStatusFlags.SetTerminalStatus(_terminal_id, TerminalStatusFlags.WCP_TerminalEvent.BillIn, Trx))
            {
              throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR, "MoneyEvent - SetTerminalStatus");
            }
          }

        }

        return true;
      }
      catch (WCP_Exception _ex)
      {
        Log.Error(_ex.ResponseCode + " - " + _ex.ResponseCodeText);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Update delta SAS meters with new decreased values
    //
    //  PARAMS :
    //      - INPUT :
    //          - Trx
    //          - DeltaSASMeters
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True or False
    //
    //   NOTES :
    //      - true: delta SAS meters Selected from SAS Meters.
    //      - false: error.
    //
    static public Boolean UpdateHistorifiedMeters(DataRow[] DeltaSASMeters, SqlTransaction Trx)
    {
      StringBuilder _sql_txt;
      SqlDataAdapter _da;
      Int32 _nr;

      try
      {
        _da = new SqlDataAdapter();

        //        - UpdateCommand 
        _sql_txt = new StringBuilder();
        _sql_txt.AppendLine("UPDATE   TERMINAL_SAS_METERS   ");
        _sql_txt.AppendLine("   SET   TSM_DELTA_VALUE     = TSM_DELTA_VALUE     - @pDeltaValue    ");
        _sql_txt.AppendLine("       , TSM_RAW_DELTA_VALUE = TSM_RAW_DELTA_VALUE - @pRawDeltaValue ");
        _sql_txt.AppendLine(" WHERE   TSM_TERMINAL_ID     = @pTerminalId  ");
        _sql_txt.AppendLine("   AND   TSM_METER_CODE      = @pMeterCode   ");
        _sql_txt.AppendLine("   AND   TSM_GAME_ID         = @pGameId      ");
        _sql_txt.AppendLine("   AND   TSM_DENOMINATION    = @pDenomination");
        _sql_txt.AppendLine("   AND   TSM_DELTA_VALUE     >= @pDeltaValue ");
        _sql_txt.AppendLine("   AND   TSM_RAW_DELTA_VALUE >= @pRawDeltaValue ");

        _da.UpdateCommand = new SqlCommand(_sql_txt.ToString());
        _da.UpdateCommand.Connection = Trx.Connection;
        _da.UpdateCommand.Transaction = Trx;
        _da.UpdateCommand.UpdatedRowSource = UpdateRowSource.None;

        _da.UpdateCommand.Parameters.Add("@pDeltaValue", SqlDbType.BigInt).SourceColumn = "TSM_DELTA_VALUE";
        _da.UpdateCommand.Parameters.Add("@pRawDeltaValue", SqlDbType.BigInt).SourceColumn = "TSM_RAW_DELTA_VALUE";
        _da.UpdateCommand.Parameters.Add("@pTerminalId", SqlDbType.Int).SourceColumn = "TSM_TERMINAL_ID";
        _da.UpdateCommand.Parameters.Add("@pMeterCode", SqlDbType.Int).SourceColumn = "TSM_METER_CODE";
        _da.UpdateCommand.Parameters.Add("@pGameId", SqlDbType.Int).SourceColumn = "TSM_GAME_ID";
        _da.UpdateCommand.Parameters.Add("@pDenomination", SqlDbType.Money).SourceColumn = "TSM_DENOMINATION";

        //        - DeleteCommand & InsertCommand
        _da.DeleteCommand = null;
        _da.InsertCommand = null;

        //    - SetModified
        SetToModified(DeltaSASMeters);

        //    - Execute Update command
        _da.UpdateBatchSize = 500;
        _da.ContinueUpdateOnError = true;

        _nr = _da.Update(DeltaSASMeters);

        if (_nr != DeltaSASMeters.Length)
        {
          if (!Services.IsPrincipal("WCP"))
          {
            Log.Message("WCP_SASMeters.UpdateHistorifiedMeters: It isn't principal server");

            return false;
          }

          Log.Message("WCP_SASMeters.UpdateHistorifiedMeters unable to decrease the expected number of rows of TSM - No records will be updated");

          foreach (DataRow _dr in DeltaSASMeters)
          {
            if (_dr.HasErrors)
            {
              Log.Message(_dr["TSM_TERMINAL_ID"].ToString()
                          + "/" + _dr["TSM_METER_CODE"].ToString()
                          + "/" + _dr["TSM_GAME_ID"].ToString()
                          + "/" + _dr["TSM_DENOMINATION"].ToString()
                          + " (TSM_TERMINAL_ID/TSM_METER_CODE/TSM_GAME_ID/TSM_DENOMINATION) ");

              Log.Message("     *** Error: " + _dr.RowError);
            }
          }

          return false;
        }

        return true;

      }// try

      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // UpdateHistorifiedMeters

    #endregion //Bill Meters Update

    #region Ticket Meters Update

    //------------------------------------------------------------------------------
    // PURPOSE : Updates Money Collection Meters table with ticket delta SAS meters values
    //
    //  PARAMS :
    //      - INPUT :
    //          - TicketDeltaSASMeters: DataTable with Delta SAS meters
    //          - Trx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True or false
    //
    //   NOTES :
    //      - true: money_collection_meters table updated.
    //      - false: error.
    //
    static public Boolean UpdateTicketsMCM(DataRow[] Meters, SqlTransaction Trx)
    {
      StringBuilder _sb;
      Int32 _num_rows_inserted;
      Int32 _total_deltas;
      Int64 _delta_value;
      DataRow _mcm_row;
      SAS_Meter.MeterProperties _meter_properties;
      Int64 _session_id;
      Int64 _collection_id;
      Int32 _terminal_id;

      try
      {
        InitTicketsData();

        _total_deltas = 0;
        foreach (DataRow _dr in Meters)
        {
          _delta_value = (Int64)_dr["TSM_DELTA_VALUE"];
          if (_delta_value == 0)
          {
            continue;
          }

          _meter_properties = SAS_Meter.GetTicketsMeterProperties((Int32)_dr["TSM_METER_CODE"]);
          if (_meter_properties == null)
          {
            continue;
          }

          //Fill MetersBySession 
          _session_id = (Int64)_dr["TSM_SESSION_ID"];
          _collection_id = (Int64)_dr["MC_COLLECTION_ID"];
          _terminal_id = (Int32)_dr["TSM_TERMINAL_ID"];

          _mcm_row = m_tickets_meters_by_session.Rows.Find(_session_id);
          if (_mcm_row == null)
          {
            _mcm_row = m_tickets_meters_by_session.NewRow();
            _mcm_row["MCM_SESSION_ID"] = _session_id;

            _mcm_row["MCM_MONEY_COLLECTION_ID"] = _collection_id;
            _mcm_row["MCM_TERMINAL_ID"] = _terminal_id;
            m_tickets_meters_by_session.Rows.Add(_mcm_row);
          }
          _mcm_row[_meter_properties.SqlColumn] = _meter_properties.MeterType == SAS_Meter.METER_TYPE.AMOUNT ? _delta_value / 100.0 : _delta_value;
          _total_deltas++;
        }

        if (_total_deltas == 0)
        {
          return true;
        }

        //
        // UPDATE TICKET METERS IN MONEY_COLLECTION_METERS 
        //
        _sb = new StringBuilder();

        _sb.AppendLine(" IF NOT EXISTS(SELECT TOP 1 * FROM   MONEY_COLLECTION_METERS WHERE [MCM_SESSION_ID] = @pSessionId)");
        _sb.AppendLine(" INSERT INTO  MONEY_COLLECTION_METERS ");
        _sb.AppendLine("           ( MCM_MONEY_COLLECTION_ID");
        _sb.AppendLine("           , MCM_SESSION_ID ");
        _sb.AppendLine("           , MCM_TERMINAL_ID");
        _sb.AppendLine("           , MCM_STARTED");
        _sb.AppendLine("           , MCM_LAST_REPORTED");
        foreach (SAS_Meter.MeterProperties _props in m_tickets_meters_properties.Values)
        {
          _sb.AppendLine("        , " + _props.SqlColumn);
        }
        _sb.AppendLine("           )");
        _sb.AppendLine(" VALUES     ( @pCollectionId");
        _sb.AppendLine("            , @pSessionId ");
        _sb.AppendLine("            , @pTerminalId");
        _sb.AppendLine("            , GETDATE() ");
        _sb.AppendLine("            , GETDATE() ");
        foreach (SAS_Meter.MeterProperties _props in m_tickets_meters_properties.Values)
        {
          _sb.AppendLine("            , " + _props.SqlParameter);
        }
        _sb.AppendLine("            ) ");
        _sb.AppendLine(" ELSE ");
        _sb.AppendLine(" UPDATE  MONEY_COLLECTION_METERS");
        _sb.AppendLine("    SET  MCM_LAST_REPORTED   = GETDATE()");
        foreach (SAS_Meter.MeterProperties _props in m_tickets_meters_properties.Values)
        {
          _sb.AppendLine("        , " + _props.SqlColumn + " = ISNULL(" + _props.SqlColumn + ", 0) + ISNULL(" + _props.SqlParameter + ", 0) ");
        }
        _sb.AppendLine("  WHERE  MCM_SESSION_ID = @pSessionId ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pSessionId", SqlDbType.BigInt).SourceColumn = "MCM_SESSION_ID";
          _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).SourceColumn = "MCM_TERMINAL_ID";
          _sql_cmd.Parameters.Add("@pCollectionId", SqlDbType.BigInt).SourceColumn = "MCM_MONEY_COLLECTION_ID";

          foreach (SAS_Meter.MeterProperties _props in m_tickets_meters_properties.Values)
          {
            _sql_cmd.Parameters.Add(_props.SqlParameter, _props.SqlType).SourceColumn = _props.SqlColumn;
          }

          using (SqlDataAdapter _sql_adap = new SqlDataAdapter())
          {
            // Execute Batch Update
            _sql_adap.InsertCommand = _sql_cmd;
            _sql_adap.ContinueUpdateOnError = true;
            _sql_adap.UpdateBatchSize = 500;
            _sql_adap.InsertCommand.UpdatedRowSource = UpdateRowSource.None;

            _num_rows_inserted = _sql_adap.Update(m_tickets_meters_by_session);

            // Errors control
            if (_num_rows_inserted != m_tickets_meters_by_session.Rows.Count)
            {
              Log.Message("Unable to update dbo.money_collection_meters. No records will be updated/inserted");

              foreach (DataRow _dr in m_tickets_meters_by_session.Rows)
              {
                if (_dr.HasErrors)
                {
                  Log.Message(_dr["MCM_TERMINAL_ID"].ToString()
                          + "/" + _dr["MCM_SESSION_ID"].ToString()
                          + " (MCM_TERMINAL_ID/MCM_SESSION_ID)");

                  Log.Message("     *** Error: " + _dr.RowError);
                }
              }

              return false;
            }
          }
        }

        return true;

      }// try

      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }// UpdateTicketMCM


    #endregion // Tickets And Bills Meters Update

    #region SAS Meters History Update

    static public void GetHourlyInterval(DateTime Now, out DateTime Current, out DateTime Previous)
    {
      int _interval;
      int _minutes;
      List<int> _valid = new List<int>();
      _valid.AddRange(new int[] { 10, 15, 20, 30, 60 });

      _interval = GeneralParam.GetInt32("WCP", "SasMetersInterval", 60, 10, 60);
      if (!_valid.Contains(_interval))
      {
        int _x;

        _x = _valid[0];
        for (int i = 1; i < _valid.Count; i++)
        {
          if (_valid[i] < _interval)
          {
            _x = _valid[i];
            continue;
          }
          break;
        }

        Log.Warning(string.Format("WCP.SasMetersInterval = {0}, Changed to {1}", _interval, _x));

        _interval = _x;
      }

      _minutes = _interval * (Now.Minute / _interval);
      Current = new DateTime(Now.Year, Now.Month, Now.Day, Now.Hour, _minutes, 0);
      Previous = Current.AddMinutes(-_interval);
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Update TSMH
    //
    //  PARAMS :
    //      - INPUT :
    //          - Trx
    //          - NUM_SAS_METER_HISTORY
    //          - DeltaSASMeters
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True or False
    //
    //   NOTES :
    //      - true: meters updated in TSMH properly
    //      - false: error.
    //
    static public Boolean UpdateTSMH(DataRow[] DeltaSASMeters, DateTime HistorificationHourlyDateTime, WSI.Common.SAS_Meter.ENUM_METERS_TYPE MeterType, ENUM_SAS_METER_HISTORY Type, SqlTransaction Trx)
    {
      StringBuilder _sb;
      Int32 _nr;
      DateTime _previous_datetime;
      DateTime _datetime;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" IF NOT EXISTS(SELECT   TSMH_TERMINAL_ID ");
        _sb.AppendLine("                 FROM   TERMINAL_SAS_METERS_HISTORY ");
        _sb.AppendLine("                WHERE   TSMH_TERMINAL_ID        = @pTerminalId ");
        _sb.AppendLine("                  AND   TSMH_METER_CODE         = @pMeterCode ");
        _sb.AppendLine("                  AND   TSMH_GAME_ID            = @pGameId ");
        _sb.AppendLine("                  AND   TSMH_DENOMINATION       = @pDenomination ");
        _sb.AppendLine("                  AND   TSMH_TYPE               = @pType ");
        _sb.AppendLine("                  AND   TSMH_DATETIME           = @pDatetime ) ");
        _sb.AppendLine("    BEGIN ");
        _sb.AppendLine("       DECLARE @pInitialValue AS BIGINT ");
        _sb.AppendLine("       ");
        _sb.AppendLine("       SELECT   @pInitialValue = TSMH_METER_FIN_VALUE ");
        _sb.AppendLine("            FROM   TERMINAL_SAS_METERS_HISTORY ");
        _sb.AppendLine("           WHERE   TSMH_TERMINAL_ID  = @pTerminalId ");
        _sb.AppendLine("             AND   TSMH_METER_CODE   = @pMeterCode ");
        _sb.AppendLine("             AND   TSMH_GAME_ID      = @pGameId ");
        _sb.AppendLine("             AND   TSMH_DENOMINATION = @pDenomination ");
        _sb.AppendLine("             AND   TSMH_TYPE         = @pType ");
        _sb.AppendLine("             AND   TSMH_DATETIME     = @pPreviousDatetime");
        _sb.AppendLine("       ");
        //_sb.AppendLine("       IF @pInitialValue IS NULL ");
        //_sb.AppendLine("         SET @pInitialValue = 0");
        //_sb.AppendLine("       ");
        _sb.AppendLine("       INSERT INTO   TERMINAL_SAS_METERS_HISTORY  ");
        _sb.AppendLine("                   ( TSMH_TERMINAL_ID, TSMH_METER_CODE ");
        _sb.AppendLine("                   , TSMH_GAME_ID ");
        _sb.AppendLine("                   , TSMH_DENOMINATION ");
        _sb.AppendLine("                   , TSMH_TYPE ");
        _sb.AppendLine("                   , TSMH_DATETIME ");
        _sb.AppendLine("                   , TSMH_METER_INI_VALUE ");
        _sb.AppendLine("                   , TSMH_METER_FIN_VALUE ");
        _sb.AppendLine("                   , TSMH_METER_INCREMENT ");
        _sb.AppendLine("                   , TSMH_RAW_METER_INCREMENT ");
        _sb.AppendLine("                   , TSMH_LAST_REPORTED   ");
        _sb.AppendLine("                   , TSMH_SAS_ACCOUNTING_DENOM ) ");
        _sb.AppendLine("            VALUES ( @pTerminalId "); // TSMH_TERMINAL_ID
        _sb.AppendLine("                   , @pMeterCode "); // TSMH_METER_CODE
        _sb.AppendLine("                   , @pGameId "); // TSMH_GAME_ID
        _sb.AppendLine("                   , @pDenomination "); // TSMH_DENOMINATION
        _sb.AppendLine("                   , @pType "); // TSMH_TYPE
        _sb.AppendLine("                   , @pDatetime "); // TSMH_DATETIME
        _sb.AppendLine("                   , @pInitialValue "); // TSMH_METER_INI_VALUE
        _sb.AppendLine("                   , @pMeterValue "); // TSMH_METER_FIN_VALUE
        _sb.AppendLine("                   , @pDeltaValue "); // TSMH_METER_INCREMENT
        _sb.AppendLine("                   , @pRawDeltaValue "); // TSMH_RAW_METER_INCREMENT
        _sb.AppendLine("                   , @pLastReported "); // TSMH_LAST_REPORTED
        _sb.AppendLine("                   , @pSasAccounDenom ) ");
        _sb.AppendLine("    END ");
        _sb.AppendLine(" ELSE ");
        _sb.AppendLine("    BEGIN ");
        _sb.AppendLine("      UPDATE   TERMINAL_SAS_METERS_HISTORY ");
        _sb.AppendLine("         SET   TSMH_METER_FIN_VALUE     = @pMeterValue ");
        _sb.AppendLine("             , TSMH_METER_INCREMENT     = TSMH_METER_INCREMENT + @pDeltaValue ");
        _sb.AppendLine("             , TSMH_RAW_METER_INCREMENT = TSMH_RAW_METER_INCREMENT + @pRawDeltaValue ");
        _sb.AppendLine("             , TSMH_LAST_REPORTED       = @pLastReported ");
        _sb.AppendLine("             , TSMH_SAS_ACCOUNTING_DENOM = @pSasAccounDenom ");
        _sb.AppendLine("       WHERE   TSMH_TERMINAL_ID         = @pTerminalId ");
        _sb.AppendLine("         AND   TSMH_METER_CODE          = @pMeterCode ");
        _sb.AppendLine("         AND   TSMH_GAME_ID             = @pGameId ");
        _sb.AppendLine("         AND   TSMH_DENOMINATION        = @pDenomination ");
        _sb.AppendLine("         AND   TSMH_TYPE                = @pType ");
        _sb.AppendLine("         AND   TSMH_DATETIME            = @pDatetime ");
        _sb.AppendLine("    END ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pDeltaValue", SqlDbType.BigInt).SourceColumn = "TSM_DELTA_VALUE";
          _sql_cmd.Parameters.Add("@pRawDeltaValue", SqlDbType.BigInt).SourceColumn = "TSM_RAW_DELTA_VALUE";
          _sql_cmd.Parameters.Add("@pMeterValue", SqlDbType.BigInt).SourceColumn = "TSM_METER_VALUE";
          _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).SourceColumn = "TSM_TERMINAL_ID";
          _sql_cmd.Parameters.Add("@pMeterCode", SqlDbType.Int).SourceColumn = "TSM_METER_CODE";
          _sql_cmd.Parameters.Add("@pGameId", SqlDbType.Int).SourceColumn = "TSM_GAME_ID";
          _sql_cmd.Parameters.Add("@pDenomination", SqlDbType.Money).SourceColumn = "TSM_DENOMINATION";
          _sql_cmd.Parameters.Add("@pLastReported", SqlDbType.DateTime).SourceColumn = "TSM_LAST_REPORTED";
          _sql_cmd.Parameters.Add("@pSasAccounDenom", SqlDbType.Decimal).SourceColumn = "SAS_ACCOUNTING_DENOM";
          _sql_cmd.Parameters.Add("@pType", SqlDbType.Int).Value = (Int32)Type;

          _datetime = DateTime.MinValue;
          _previous_datetime = DateTime.MinValue;

          if (Type == ENUM_SAS_METER_HISTORY.TSMH_HOURLY)
          {
            WCP_SASMeters.GetHourlyInterval(HistorificationHourlyDateTime, out _datetime, out _previous_datetime);

            if (MeterType == SAS_Meter.ENUM_METERS_TYPE.ALL) // Hour changed
            {
              WCP_SASMeters.GetHourlyInterval(_previous_datetime, out _datetime, out _previous_datetime);
            }
          }
          else if (Type == ENUM_SAS_METER_HISTORY.TSMH_MINCETUR_DAILY_REPORT)
          {
            Int32 _daily_task_hour;

            // Parse daily task
            _daily_task_hour = GeneralParam.GetInt32("WCP", "DailySasMetersHour", GeneralParam.GetInt32("WigosGUI", "ClosingTime"));
            _daily_task_hour = ((_daily_task_hour % 24) + 24) % 24;

            _datetime = WSI.Common.Misc.Opening(HistorificationHourlyDateTime, _daily_task_hour, 0);
            _previous_datetime = _datetime.AddDays(-1);

            if (MeterType == SAS_Meter.ENUM_METERS_TYPE.ALL && _daily_task_hour == HistorificationHourlyDateTime.Hour) // Hour changed
            {
              _datetime = _datetime.AddDays(-1);
              _previous_datetime = _datetime.AddDays(-1);
            }
          }

          _sql_cmd.Parameters.Add("@pDatetime", SqlDbType.DateTime).Value = _datetime;
          _sql_cmd.Parameters.Add("@pPreviousDatetime", SqlDbType.DateTime).Value = _previous_datetime;

          SetToModified(DeltaSASMeters);

          using (SqlDataAdapter _sql_da = new SqlDataAdapter())
          {
            _sql_da.UpdateCommand = _sql_cmd;
            _sql_da.UpdateCommand.UpdatedRowSource = UpdateRowSource.None;
            _sql_da.UpdateBatchSize = 500;
            _sql_da.ContinueUpdateOnError = false;

            _nr = _sql_da.Update(DeltaSASMeters);

            if (_nr != DeltaSASMeters.Length)
            {
              foreach (DataRow _dr in DeltaSASMeters)
              {
                if (_dr.HasErrors)
                {
                  Log.Message(_dr["TSM_TERMINAL_ID"].ToString()
                          + "/" + _dr["TSM_SESSION_ID"].ToString()
                          + "/" + _dr["MC_COLLECTION_ID"].ToString()
                          + "/" + _dr["TSM_BILL_DENOMINATION"].ToString()
                          + "/" + _dr["TSM_DELTA_VALUE"].ToString()
                          + " (TSM_TERMINAL_ID/TSM_SESSION_ID/MC_COLLECTION_ID/TSM_BILL_DENOMINATION/TSM_DELTA_VALUE) ");
                  Log.Error(_dr.RowError);
                }
              }

              return false;
            }

            return true;
          }
        }

      }// try
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // UpdateTSMH

    //------------------------------------------------------------------------------
    // PURPOSE : Generates an alarm when a terminal hasn't reported his meters in 24 hours
    //
    //  PARAMS :
    //      - INPUT :
    //          - Trx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True or False
    //
    //   NOTES :
    //      - true: alarm generated if it's necessary
    //      - false: error.
    //
    static public Boolean GenerateUnreportedMetersAlarm(SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      DataTable _unreported_meters;

      if (!(GeneralParam.GetBoolean("Alarms", "Terminal.DailyUnreportedMeters")))
      {
        return true;
      }

      if (m_alarm_meters_reported == null)
      {
        m_alarm_meters_reported = new Dictionary<int, Dictionary<int, DateTime>>();
        }

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("    SELECT    TE_TERMINAL_ID                                                                                  ");
        _sb.AppendLine("            , TE_NAME                                                                                         ");
        _sb.AppendLine("            , TE_PROVIDER_ID                                                                                  ");
        _sb.AppendLine("            , TSM_METER_CODE                                                                                  ");
        _sb.AppendLine("            , SMC_DESCRIPTION                                                                                 ");
        _sb.AppendLine("       FROM   TERMINAL_SAS_METERS                                                                             ");
        _sb.AppendLine(" INNER JOIN   TERMINALS                                                                                       ");
        _sb.AppendLine("         ON   TSM_TERMINAL_ID = TE_TERMINAL_ID                                                                ");
        _sb.AppendLine(" INNER JOIN   SAS_METERS_CATALOG                                                                              ");
        _sb.AppendLine("         ON   SMC_METER_CODE = TSM_METER_CODE                                                                 ");
        _sb.AppendLine(" INNER JOIN   SAS_METERS_CATALOG_PER_GROUP                                                                    ");
        _sb.AppendLine("         ON   SMCG_METER_CODE = TSM_METER_CODE                                                                ");
        _sb.AppendLine("      WHERE   TSM_LAST_REPORTED < DATEADD(HH, -24, GETDATE())                                                 ");
        _sb.AppendLine("        AND   NOT ( TE_STATUS = @pTerminalRetiredStatus AND TE_RETIREMENT_DATE < DATEADD(DAY, -1, GETDATE())) ");
        _sb.AppendLine("        AND   SMCG_GROUP_ID = @pSasMetersGroup                                                                ");
        _sb.AppendLine("   ORDER BY   TSM_TERMINAL_ID                                                                                 ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pTerminalRetiredStatus", SqlDbType.Int).Value = (Int32)TerminalStatus.RETIRED;
          _sql_cmd.Parameters.Add("@pSasMetersGroup", SqlDbType.Int).Value = (Int32)SAS_METERS_GROUPS.IDX_SMG_METERS_REPORTED;

          using (SqlDataAdapter _da = new SqlDataAdapter(_sql_cmd))
          {
            _unreported_meters = new DataTable();
            _da.Fill(_unreported_meters);
          }
        }

        if (_unreported_meters.Rows.Count > 0)
        {
          AddUnreportedTerminalMetersAlarms(_unreported_meters, SqlTrx);

          return true;
        }
      }// try
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // GenerateUnreportedMetersAlarm

    /// <summary>
    /// Add unreported meters alarms
    /// </summary>
    /// <param name="UnreportedMeters"></param>
    /// <param name="SqlTrx"></param>
    private static void AddUnreportedTerminalMetersAlarms(DataTable UnreportedMeters, SqlTransaction SqlTrx)
          {
      Int32 _terminal_id;
      String _alarm_string;
      String _terminal_name;
      String _terminal_provider;
      String _separator;
      Boolean _first_meter;
      Int32 _meters_by_terminal;
      Int32 _i_meter;
      String _count_criteria;
      Boolean _must_be_register;

      _terminal_id = 0;
      _alarm_string = String.Empty;
      _terminal_name = String.Empty;
      _terminal_provider = String.Empty;
      _meters_by_terminal = 0;
      _i_meter = 0;

      _first_meter = true;

      foreach (DataRow _row in UnreportedMeters.Rows)
            {
        if(_first_meter)
              {
          _i_meter            = 0;
          _first_meter        = false;
          _separator          = String.Empty;
          _alarm_string       = String.Empty;

          //Get num meters of current terminal
          _count_criteria     = String.Format("TE_TERMINAL_ID = {0}", _row["TE_TERMINAL_ID"].ToString());
          _meters_by_terminal = UnreportedMeters.Select(_count_criteria).Length;

          // Set current terminal data
          _terminal_id        = (Int32)_row["TE_TERMINAL_ID"];
          _terminal_name      = _row["TE_NAME"].ToString();
          _terminal_provider  = _row["TE_PROVIDER_ID"].ToString();
              }

        // Check current terminal meters to report
        if (CheckToReportAlarm(_terminal_id, (Int32)_row["TSM_METER_CODE"]))
        {
          // Current terminal alarm description
          _separator = (_alarm_string != String.Empty) ? ", \n" : String.Empty;
          _alarm_string += _separator + _row["SMC_DESCRIPTION"].ToString();
            }

        _i_meter++;

        // Last meter of terminal and alarm description to register is not empty
        _must_be_register = (_meters_by_terminal == _i_meter && _alarm_string != String.Empty);

        // Last meter of current terminal
        if (_must_be_register)
            {
          // Add previous terminal alarm
          RegisterUnreportedTerminalMetersAlarm ( _terminal_id
                                                , _terminal_name
                                                , _terminal_provider
                                                , _alarm_string
                                                , SqlTrx);

          // Next is first meter of new terminal 
          _first_meter = true; 
        }
            }

    } // AddUnreportedTerminalMetersAlarms

    /// <summary>
    /// Check alarm to report
    /// </summary>
    /// <param name="TerminalId"></param>
    /// <param name="MeterCode"></param>
    /// <returns></returns>
    private static Boolean CheckToReportAlarm(Int32 TerminalId, Int32 MeterCode)
    {
      DateTime _date_time;
      Dictionary<Int32, DateTime> _meters;

      // Check if exist current terminal
      if (!m_alarm_meters_reported.TryGetValue(TerminalId, out _meters))
      {
        _meters = new Dictionary<Int32, DateTime>();
        _meters.Add(MeterCode, WGDB.Now);

        m_alarm_meters_reported.Add(TerminalId, _meters);

        return true;
          }

      // Check if exist current meter
      if (!_meters.TryGetValue(MeterCode, out _date_time))
      {
        _meters.Add(MeterCode, WGDB.Now);
        m_alarm_meters_reported[TerminalId] = _meters;

          return true;
        }

      // 23 hours + 1 (Find: "Check all meters every 1 hour")
      if (_date_time < WGDB.Now.AddHours(-DEFAULT_WAITING_TIME_BETWEEN_ALARMS))
      {
        m_alarm_meters_reported[TerminalId][MeterCode] = WGDB.Now;

        return true;
      }

      return false;
    } // CheckToReportAlarm

    /// <summary>
    /// Register unreported terminal meters alarm
    /// </summary>
    /// <param name="TerminalId"></param>
    /// <param name="TerminalName"></param>
    /// <param name="ProviderId"></param>
    /// <param name="AlarmString"></param>
    /// <param name="SqlTrx"></param>
    private static void RegisterUnreportedTerminalMetersAlarm(Int32 TerminalId, String TerminalName, String ProviderId, String AlarmString, SqlTransaction SqlTrx)
    {
      String _terminal_description;
      String _alarm_string;

      // Format alarm description
      _terminal_description = Misc.ConcatProviderAndTerminal(ProviderId, TerminalName);
      _alarm_string = Resource.String("STR_ALARM_UNREPORTED_METERS", _terminal_description) + "\n" + AlarmString;

      // Add alarm description
      if (!Alarm.Register(AlarmSourceCode.TerminalSystem,
                          TerminalId,
                          _terminal_description,
                          (UInt32)AlarmCode.TerminalSystem_UnreportedMetes,
                          _alarm_string,
                          AlarmSeverity.Warning,
                          WGDB.Now,
                          SqlTrx))
      {
        Log.Message("Error registering alarm " + _alarm_string);
      }
    } // RegisterUnreportedTerminalMetersAlarm

    //------------------------------------------------------------------------------
    // PURPOSE : Initialize given table. 
    //
    //  PARAMS :
    //      - INPUT :
    //            - out DataTable: PreviousDT
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private static void InitBillsData()
    {
      if (m_bills_meters_properties == null)
      {
        m_bills_meters_properties = SAS_Meter.GetBillsMetersProperties();
      }

      if (m_bills_meters_by_session == null)
      {
        m_bills_meters_by_session = new DataTable();
        m_bills_meters_by_session.Columns.Add("MCM_SESSION_ID", Type.GetType("System.Int64"));
        m_bills_meters_by_session.Columns.Add("MCM_MONEY_COLLECTION_ID", Type.GetType("System.Int64"));
        m_bills_meters_by_session.Columns.Add("MCM_TERMINAL_ID", Type.GetType("System.Int32"));
        m_bills_meters_by_session.Columns.Add("MCM_BILLS_IN_STACKER_NUM", Type.GetType("System.Int32"));

        foreach (SAS_Meter.MeterProperties _meter_props in m_bills_meters_properties.Values)
        {
          m_bills_meters_by_session.Columns.Add(_meter_props.SqlColumn, _meter_props.SystemType);
        }

        m_bills_meters_by_session.PrimaryKey = new DataColumn[] { m_bills_meters_by_session.Columns["MCM_SESSION_ID"] };
      }
      else
      {
        m_bills_meters_by_session.Clear();
      }

    } // InitBillsData

    private static void InitGamingHallData()
    {
      if (m_gaming_hall_meters_by_session == null)
      {
        m_gaming_hall_meters_by_session = new DataTable();

        m_gaming_hall_meters_by_session.Columns.Add("TERMINAL_ID", Type.GetType("System.Int32"));
        m_gaming_hall_meters_by_session.Columns.Add("SESSION_ID", Type.GetType("System.Int64"));
        m_gaming_hall_meters_by_session.Columns.Add("MONEY_COLLECTION_ID", Type.GetType("System.Int64"));

        m_gaming_hall_meters_by_session.Columns.Add("OUT_CENTS", Type.GetType("System.Decimal"));
        m_gaming_hall_meters_by_session.Columns.Add("OUT_BILLS", Type.GetType("System.Decimal"));
        m_gaming_hall_meters_by_session.Columns.Add("OUT_COINS", Type.GetType("System.Decimal"));
        m_gaming_hall_meters_by_session.Columns.Add("IN_CENTS", Type.GetType("System.Decimal"));

        foreach (DataColumn _dc in m_gaming_hall_meters_by_session.Columns)
        {
          if (_dc.ColumnName.EndsWith("_ID"))
            continue;

          _dc.AllowDBNull = true;
          _dc.DefaultValue = DBNull.Value;
        }

        m_gaming_hall_meters_by_session.PrimaryKey = new DataColumn[] { m_gaming_hall_meters_by_session.Columns["SESSION_ID"] };
      }
      else
      {
        m_gaming_hall_meters_by_session.Clear();
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Initialize given table. 
    //
    //  PARAMS :
    //      - INPUT :
    //            - out DataTable: PreviousDT
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private static void InitTicketsData()
    {
      if (m_tickets_meters_properties == null)
      {
        m_tickets_meters_properties = SAS_Meter.GetTicketsMetersProperties();
      }

      if (m_tickets_meters_by_session == null)
      {
        m_tickets_meters_by_session = new DataTable();
        m_tickets_meters_by_session.Columns.Add("MCM_SESSION_ID", Type.GetType("System.Int64"));
        m_tickets_meters_by_session.Columns.Add("MCM_MONEY_COLLECTION_ID", Type.GetType("System.Int64"));
        m_tickets_meters_by_session.Columns.Add("MCM_TERMINAL_ID", Type.GetType("System.Int32"));

        foreach (SAS_Meter.MeterProperties _meter_props in m_tickets_meters_properties.Values)
        {
          m_tickets_meters_by_session.Columns.Add(_meter_props.SqlColumn, _meter_props.SystemType);
        }

        m_tickets_meters_by_session.PrimaryKey = new DataColumn[] { m_tickets_meters_by_session.Columns["MCM_SESSION_ID"] };
      }
      else
      {
        m_tickets_meters_by_session.Clear();
      }

    } // InitTicketsData

    #endregion // SAS Meters History Update

    #region SAS Meters Denom Statistics

    static public Boolean ProcessMachineDenomStatsPerHour(SAS_Meter.ENUM_METERS_TYPE MeterType, DataTable TerminalMetersDenom)
    {
      SqlTransaction _sql_trx;
      DataTable _machine_denom_stat_per_hour;
      DataRow _dr;

      try
      {
        if (TerminalMetersDenom.Rows.Count == 0)
        {
          return true;
        }

        using (DB_TRX _db_trx = new DB_TRX())
        {
          _sql_trx = _db_trx.SqlTransaction;

          if (!UpdateHistorifiedMeters(TerminalMetersDenom.Select(), _sql_trx))
          {
            Log.Message("ProcessTerminalSasMeterDenom: Error on UpdateHistorifiedMeters");
          }

          Int32 _terminal_id;
          Decimal _denomination;
          DataRow _row_stats;
          Int64 _delta_value;

          _machine_denom_stat_per_hour = new DataTable();
          _machine_denom_stat_per_hour.Columns.Add("MDSH_TERMINAL_ID", Type.GetType("System.Int32"));
          _machine_denom_stat_per_hour.Columns.Add("MDSH_DENOMINATION", Type.GetType("System.Decimal"));
          _machine_denom_stat_per_hour.Columns.Add("MDSH_PLAYED_AMOUNT", Type.GetType("System.Decimal"));
          _machine_denom_stat_per_hour.Columns.Add("MDSH_WON_AMOUNT", Type.GetType("System.Decimal"));
          _machine_denom_stat_per_hour.Columns.Add("MDSH_JACKPOT_AMOUNT", Type.GetType("System.Decimal"));
          _machine_denom_stat_per_hour.Columns.Add("MDSH_PLAYED_COUNT", Type.GetType("System.Int64"));
          _machine_denom_stat_per_hour.Columns.Add("MDSH_WON_COUNT", Type.GetType("System.Int64"));

          _terminal_id = -1;
          _denomination = -1;
          _row_stats = null;

          for (int _idx = 0; _idx < TerminalMetersDenom.Rows.Count; _idx++)
          {
            _dr = TerminalMetersDenom.Rows[_idx];

            if (!(_terminal_id.Equals(_dr["TSM_TERMINAL_ID"]) && _denomination.Equals(_dr["TSM_DENOMINATION"])))
            {
              // Change
              _terminal_id = (Int32)_dr["TSM_TERMINAL_ID"];
              _denomination = (Decimal)_dr["TSM_DENOMINATION"];

              _row_stats = _machine_denom_stat_per_hour.NewRow();
              _row_stats["MDSH_TERMINAL_ID"] = _terminal_id;
              _row_stats["MDSH_DENOMINATION"] = _denomination;
              _row_stats["MDSH_PLAYED_AMOUNT"] = 0;
              _row_stats["MDSH_WON_AMOUNT"] = 0;
              _row_stats["MDSH_JACKPOT_AMOUNT"] = 0;
              _row_stats["MDSH_PLAYED_COUNT"] = 0;
              _row_stats["MDSH_WON_COUNT"] = 0;
              _machine_denom_stat_per_hour.Rows.Add(_row_stats);

              _row_stats.AcceptChanges();
              _row_stats.SetModified();
            }
            _delta_value = (Int64)_dr["TSM_DELTA_VALUE"];

            switch ((Int32)_dr["TSM_METER_CODE"])
            {
              case 0: //Total coin in credits (played)
                _row_stats["MDSH_PLAYED_AMOUNT"] = (Decimal)_row_stats["MDSH_PLAYED_AMOUNT"] + ((Decimal)_delta_value / 100);
                break;

              case 1: //Total coin out credits (won)
                _row_stats["MDSH_WON_AMOUNT"] = (Decimal)_row_stats["MDSH_WON_AMOUNT"] + ((Decimal)_delta_value / 100);
                break;

              case 2: //Total jackpot credits
                _row_stats["MDSH_JACKPOT_AMOUNT"] = (Decimal)_row_stats["MDSH_JACKPOT_AMOUNT"] + ((Decimal)_delta_value / 100);
                break;

              case 5: //Games played
                _row_stats["MDSH_PLAYED_COUNT"] = (Int64)_row_stats["MDSH_PLAYED_COUNT"] + _delta_value;
                break;

              case 6: //Games won
                _row_stats["MDSH_WON_COUNT"] = (Int64)_row_stats["MDSH_WON_COUNT"] + _delta_value;
                break;

              default:
                Log.Message(_terminal_id.ToString() + "-" + _dr["TSM_METER_CODE"].ToString() + " - " + _delta_value.ToString());
                break;
            }

          }

          Boolean _its_ok;

          _its_ok = true;
          if (_machine_denom_stat_per_hour.Rows.Count > 0)
          {
            UpdateMachineDenomStatsPerHour(_machine_denom_stat_per_hour, MeterType, _sql_trx);
            _its_ok = _db_trx.Commit();
          }

          return _its_ok;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    static public Boolean UpdateMachineDenomStatsPerHour(DataTable MachineDenom, SAS_Meter.ENUM_METERS_TYPE MeterType, SqlTransaction Trx)
    {
      StringBuilder _sb;
      Int32 _nr;
      DateTime _datetime;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" IF NOT EXISTS(SELECT   MDSH_TERMINAL_ID ");
        _sb.AppendLine("                 FROM   MACHINE_DENOM_STATS_PER_HOUR ");
        _sb.AppendLine("                WHERE   MDSH_TERMINAL_ID  = @pTerminalId ");
        _sb.AppendLine("                  AND   MDSH_DENOMINATION = @pDenomination ");
        _sb.AppendLine("                  AND   MDSH_BASE_HOUR    = @pBaseHour ) ");
        _sb.AppendLine("    BEGIN ");
        _sb.AppendLine("       INSERT INTO   MACHINE_DENOM_STATS_PER_HOUR ");
        _sb.AppendLine("                   ( MDSH_BASE_HOUR ");
        _sb.AppendLine("                   , MDSH_TERMINAL_ID ");
        _sb.AppendLine("                   , MDSH_DENOMINATION ");
        _sb.AppendLine("                   , MDSH_PLAYED_AMOUNT ");
        _sb.AppendLine("                   , MDSH_WON_AMOUNT ");
        _sb.AppendLine("                   , MDSH_JACKPOT_AMOUNT ");
        _sb.AppendLine("                   , MDSH_PLAYED_COUNT ");
        _sb.AppendLine("                   , MDSH_WON_COUNT ) ");
        _sb.AppendLine("            VALUES ");
        _sb.AppendLine("                   ( @pBaseHour ");
        _sb.AppendLine("                   , @pTerminalId ");
        _sb.AppendLine("                   , @pDenomination ");
        _sb.AppendLine("                   , @pPlayedAmount ");
        _sb.AppendLine("                   , @pWonAmount ");
        _sb.AppendLine("                   , @pJackpotAmount ");
        _sb.AppendLine("                   , @pPlayedCount ");
        _sb.AppendLine("                   , @pWonCount ) ");
        _sb.AppendLine("    END ");
        _sb.AppendLine(" ELSE ");
        _sb.AppendLine("    BEGIN ");
        _sb.AppendLine("       UPDATE   MACHINE_DENOM_STATS_PER_HOUR ");
        _sb.AppendLine("          SET   MDSH_PLAYED_AMOUNT  = MDSH_PLAYED_AMOUNT  + @pPlayedAmount ");
        _sb.AppendLine("              , MDSH_WON_AMOUNT     = MDSH_WON_AMOUNT     + @pWonAmount ");
        _sb.AppendLine("              , MDSH_JACKPOT_AMOUNT = MDSH_JACKPOT_AMOUNT + @pJackpotAmount ");
        _sb.AppendLine("              , MDSH_PLAYED_COUNT   = MDSH_PLAYED_COUNT   + @pPlayedCount ");
        _sb.AppendLine("              , MDSH_WON_COUNT      = MDSH_WON_COUNT      + @pWonCount ");
        _sb.AppendLine("        WHERE   MDSH_TERMINAL_ID    = @pTerminalId ");
        _sb.AppendLine("          AND   MDSH_DENOMINATION   = @pDenomination ");
        _sb.AppendLine("          AND   MDSH_BASE_HOUR      = @pBaseHour ");
        _sb.AppendLine("     END ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).SourceColumn = "MDSH_TERMINAL_ID";
          _sql_cmd.Parameters.Add("@pDenomination", SqlDbType.Money).SourceColumn = "MDSH_DENOMINATION";
          _sql_cmd.Parameters.Add("@pPlayedAmount", SqlDbType.Money).SourceColumn = "MDSH_PLAYED_AMOUNT";
          _sql_cmd.Parameters.Add("@pWonAmount", SqlDbType.Money).SourceColumn = "MDSH_WON_AMOUNT";
          _sql_cmd.Parameters.Add("@pJackpotAmount", SqlDbType.Money).SourceColumn = "MDSH_JACKPOT_AMOUNT";
          _sql_cmd.Parameters.Add("@pPlayedCount", SqlDbType.BigInt).SourceColumn = "MDSH_PLAYED_COUNT";
          _sql_cmd.Parameters.Add("@pWonCount", SqlDbType.BigInt).SourceColumn = "MDSH_WON_COUNT";
          _datetime = GetCurrentBaseHour(MeterType);
          _sql_cmd.Parameters.Add("@pBaseHour", SqlDbType.DateTime).Value = _datetime;

          using (SqlDataAdapter _sql_da = new SqlDataAdapter())
          {
            _sql_da.UpdateCommand = _sql_cmd;
            _sql_da.UpdateCommand.UpdatedRowSource = UpdateRowSource.None;
            _sql_da.UpdateBatchSize = 500;
            _sql_da.ContinueUpdateOnError = false;

            _nr = _sql_da.Update(MachineDenom);

            if (_nr != MachineDenom.Rows.Count)
            {
              foreach (DataRow _dr in MachineDenom.Rows)
              {
                if (_dr.HasErrors)
                {
                  Log.Message(_dr["MDSH_TERMINAL_ID"].ToString() + "/" + _dr["MDSH_DENOMINATION"].ToString() + "/" + _datetime.ToString()
                              + " (MDSH_TERMINAL_ID/MDSH_DENOMINATION/MDSH_BASE_HOUR) ");
                  Log.Error(_dr.RowError);
                }
              }

              return false;
            }

            return true;
          }
        }
      }// try
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // UpdateTSMH

    public static DateTime GetCurrentBaseHour(SAS_Meter.ENUM_METERS_TYPE MeterType)
    {
      DateTime _datetime;
      DateTime _now = WGDB.Now;

      _datetime = new DateTime(_now.Year, _now.Month, _now.Day, _now.Hour, 0, 0);
      if (MeterType == SAS_Meter.ENUM_METERS_TYPE.ALL) // Hour changed
      {
        _datetime = _datetime.AddHours(-1);
      }

      return _datetime;
    }

    #endregion

    #region SAS Meters Machine Process

    //------------------------------------------------------------------------------
    // PURPOSE : Update meters for all terminals. Called from thread
    //
    //  PARAMS :
    //      - INPUT :
    //          - ENUM_METERS_TYPE: Type
    //          - DataTable _terminal_meters
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True or false
    //
    //   NOTES :
    //
    public static Boolean ProcessTerminalSasMeters(WSI.Common.SAS_Meter.ENUM_METERS_TYPE Type, DateTime HistorificationDateTime, DataTable TerminalMeters)
    {
      DataRow[] _rows;
      Int32 _terminal_id;
      Int32 _num_retries;

      try
      {
        _num_retries = 0;

        while (TerminalMeters.Rows.Count > 0)
        {
          _terminal_id = (Int32)TerminalMeters.Rows[0]["TSM_TERMINAL_ID"];
          _rows = TerminalMeters.Select("TSM_TERMINAL_ID=" + _terminal_id);

          using (DB_TRX _db_trx = new DB_TRX())
          {
            if (!ProcessMetersByTerminal(_rows, HistorificationDateTime, Type, _db_trx.SqlTransaction))
            {
              if (_num_retries < 3)
              {
                _num_retries++;

                continue;
              }
              Log.Message("Error on ProcessMetersByTerminal Terminal: " + _terminal_id);
            }

            foreach (DataRow _dr in _rows)
            {
              TerminalMeters.Rows.Remove(_dr);
            }
            TerminalMeters.AcceptChanges();

            _db_trx.SqlTransaction.Commit();
            _num_retries = 0;
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    public static Boolean ChangeStackerProcessTerminalSasMeters(WSI.Common.SAS_Meter.ENUM_METERS_TYPE Type, DataTable TerminalMeters, Int32 TerminalId, SqlTransaction Trx)
    {
      DataRow[] _rows;
      DateTime _now;
      DateTime _now_hour;

      _now = WGDB.Now;
      _now_hour = new DateTime(_now.Year, _now.Month, _now.Day, _now.Hour, 0, 0);

      _rows = TerminalMeters.Select("TSM_TERMINAL_ID=" + TerminalId);

      if (!ProcessMetersByTerminal(_rows, _now_hour, Type, Trx))
      {
        Log.Message("Error on ProcessMetersByTerminal");

        return false;
      }

      return true;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Update meters for one terminal.
    //
    //  PARAMS :
    //      - INPUT :
    //          - ENUM_METERS_TYPE: Type
    //          - DataRow[] Rows
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True or false
    //
    //   NOTES :
    //
    private static Boolean ProcessMetersByTerminal(DataRow[] Rows, DateTime HistorificationDateTime, WSI.Common.SAS_Meter.ENUM_METERS_TYPE MeterType, SqlTransaction Trx)
    {
      //If doesn't exist any meter in the terminal to manage
      if (Rows.Length == 0)
      {
        return true;
      }

      //Decreases TSM deltas that are going to be managed and blocks the rows
      if (!WCP_SASMeters.UpdateHistorifiedMeters(Rows, Trx))
      {
        Log.Error("Error WCP_SASMeters.UpdateHistorifiedMeters");

        return false;
      }

      if (!WCP_SASMeters.UpdateBillsMetersCTMAndMCM(Rows, Trx))
      {
        Log.Error("Error WCP_SASMeters.UpdateBillsCTMAndMCM");

        return false;
      }

      if (!WCP_SASMeters.UpdateTerminalMoneyAndGenerateCashierAndMBMovements(Rows, Trx))
      {
        Log.Error("Error WCP_SASMeters.UpdateTerminalMoneyAndGenerateCashierAndMBMovements");

        return false;
      }

      if (!WCP_SASMeters.UpdateTicketsMCM(Rows, Trx))
      {
        Log.Error("Error WCP_SASMeters.UpdateTicketMCM");

        return false;
      }

      if (!WCP_SASMeters.UpdateMCAndMCMForGamingHall(Rows, Trx))
      {
        Log.Error("Error WCP_SASMeters.UpdateTicketsMCAndMCMForGM");

        return false;
      }

      //TYPE HOUR 
      if (!WSI.WCP.WCP_SASMeters.UpdateTSMH(Rows, HistorificationDateTime, MeterType, ENUM_SAS_METER_HISTORY.TSMH_HOURLY, Trx))
      {
        Log.Error("Error WCP_SASMeters.UpdateTSMH TSMH_HOURLY");

        return false;
      }

      //TYPE DAY
      if (!WSI.WCP.WCP_SASMeters.UpdateTSMH(Rows, HistorificationDateTime, MeterType, ENUM_SAS_METER_HISTORY.TSMH_MINCETUR_DAILY_REPORT, Trx))
      {
        Log.Error("Error WCP_SASMeters.UpdateTSMH TSMH_MINCETUR_DAILY_REPORT");

        return false;
      }

      return true;
    }

    private static bool UpdateMCAndMCMForGamingHall(DataRow[] Meters, SqlTransaction Trx)
    {
      StringBuilder _sb;
      Int32 _num_rows_inserted;
      Int32 _total_deltas;
      Int64 _delta_value;
      DataRow _mcm_row;
      Int64 _session_id;
      Int64 _collection_id;
      Int32 _terminal_id;
      Boolean _is_pcd_comm;
      Int32 _meter_code;
      Int32 _out_bill;
      Int32 _out_coins;
      Int32 _out_cents;
      Decimal _cents_value;
      String _column_affected;

      if (!Misc.IsGamingHallMode())
      {
        return true;
      }

      _out_bill = GeneralParam.GetInt32("GamingHall.SasMeter", "Meter.OutBills", 0);
      _out_coins = GeneralParam.GetInt32("GamingHall.SasMeter", "Meter.OutCoins", 0);
      _out_cents = GeneralParam.GetInt32("GamingHall.SasMeter", "Meter.OutCents", 0);
      InitGamingHallData();

      try
      {
        _total_deltas = 0;

        foreach (DataRow _dr in Meters)
        {
          _delta_value = (Int64)_dr["TSM_DELTA_VALUE"];
          if (_delta_value == 0)
          {
            continue;
          }

          _meter_code = (Int32)_dr["TSM_METER_CODE"];
          _session_id = (Int64)_dr["TSM_SESSION_ID"];
          _collection_id = (Int64)_dr["MC_COLLECTION_ID"];
          _terminal_id = (Int32)_dr["TSM_TERMINAL_ID"];
          _is_pcd_comm = (Int32)_dr["TE_PCD_COMM"] == 1;

          // For PCD communication:
          //   Input:  0x24 TotalIn   -> mc_expected_coin_amont
          //   Output: 0x09 TotalOut  -> mc_out_cents
          // For SAS:
          //   Input:  Bills/Coin/Tickets methods like TITO
          //   Output: Meters defined in GP
          //               Meter.OutBills -> mc_out_bills
          //               Meter.OutCoins -> mc_out_coins
          //               Meter.OutCents -> mc_out_cents

          _cents_value = _delta_value / 100m;
          _column_affected = "";

          if (_is_pcd_comm)
          {
            if (_meter_code == 32) // 0x24 TotalIn -> mc_expected_coin_amont
            {
              _column_affected = "IN_CENTS";
            }
            else if (_meter_code == 9) // 0x09 TotalOut  -> mc_out_cents
            {
              _column_affected = "OUT_CENTS";
            }
          }
          else
          {
            if (_meter_code == _out_bill) // Meter.OutBills -> mc_out_bills
            {
              _column_affected = "OUT_BILLS";
            }
            else if (_meter_code == _out_coins) // Meter.OutCoins -> mc_out_coins
            {
              _column_affected = "OUT_COINS";
            }
            else if (_meter_code == _out_cents) // Meter.OutCents -> mc_out_cents
            {
              _column_affected = "OUT_CENTS";
            }
          }

          if (String.IsNullOrEmpty(_column_affected))
          {
            
            continue;
          }

          _mcm_row = m_gaming_hall_meters_by_session.Rows.Find(_session_id);
          if (_mcm_row == null)
          {
            _mcm_row = m_gaming_hall_meters_by_session.NewRow();
            _mcm_row["SESSION_ID"] = _session_id;
            _mcm_row["MONEY_COLLECTION_ID"] = _collection_id;
            _mcm_row["TERMINAL_ID"] = _terminal_id;
            m_gaming_hall_meters_by_session.Rows.Add(_mcm_row);          
          }

          _mcm_row[_column_affected] = _mcm_row.IsNull(_column_affected) ? _cents_value : (Decimal)_mcm_row[_column_affected] + _cents_value;
          _total_deltas++;

        }

        if (_total_deltas == 0)
        {
          return true;
        }

        //
        // UPDATE MONEY_COLLECTION_METERS AND MONEY_COLLECTION
        //
        _sb = new StringBuilder();
        _sb.AppendLine(" IF NOT EXISTS(SELECT TOP 1 * FROM MONEY_COLLECTION_METERS WHERE MCM_SESSION_ID = @pSessionId)");
        _sb.AppendLine("    INSERT INTO   MONEY_COLLECTION_METERS ");
        _sb.AppendLine("                ( MCM_MONEY_COLLECTION_ID");
        _sb.AppendLine("                , MCM_SESSION_ID ");
        _sb.AppendLine("                , MCM_TERMINAL_ID");
        _sb.AppendLine("                , MCM_STARTED");
        _sb.AppendLine("                , MCM_LAST_REPORTED");
        _sb.AppendLine("                , MCM_OUT_CENTS");
        _sb.AppendLine("                , MCM_OUT_BILLS");
        _sb.AppendLine("                , MCM_OUT_COINS");
        _sb.AppendLine("                , MCM_TOTAL_COIN_AMOUNT");
        _sb.AppendLine("                ) ");
        _sb.AppendLine("         VALUES ( @pCollectionId");
        _sb.AppendLine("                , @pSessionId ");
        _sb.AppendLine("                , @pTerminalId");
        _sb.AppendLine("                , GETDATE() ");
        _sb.AppendLine("                , GETDATE() ");
        _sb.AppendLine("                , @pOutCents ");
        _sb.AppendLine("                , @pOutBills ");
        _sb.AppendLine("                , @pOutCoins ");
        _sb.AppendLine("                , @pInCents ");
        _sb.AppendLine("                ) ");
        _sb.AppendLine(" ELSE ");
        _sb.AppendLine("         UPDATE   MONEY_COLLECTION_METERS");
        _sb.AppendLine("            SET   MCM_LAST_REPORTED     = GETDATE()");
        _sb.AppendLine("                , MCM_OUT_CENTS         = CASE WHEN @pOutCents IS NULL THEN MCM_OUT_CENTS ELSE ISNULL(MCM_OUT_CENTS,0) + @pOutCents END ");
        _sb.AppendLine("                , MCM_OUT_BILLS         = CASE WHEN @pOutBills IS NULL THEN MCM_OUT_BILLS ELSE ISNULL(MCM_OUT_BILLS,0) + @pOutBills END ");
        _sb.AppendLine("                , MCM_OUT_COINS         = CASE WHEN @pOutCoins IS NULL THEN MCM_OUT_COINS ELSE ISNULL(MCM_OUT_COINS,0) + @pOutCoins END ");
        _sb.AppendLine("                , MCM_TOTAL_COIN_AMOUNT = CASE WHEN @pInCents  IS NULL THEN MCM_TOTAL_COIN_AMOUNT ELSE ISNULL(MCM_TOTAL_COIN_AMOUNT,0) + @pInCents END) ");
        _sb.AppendLine("          WHERE   MCM_SESSION_ID    = @pSessionId ");
        _sb.AppendLine(" ");
        _sb.AppendLine(" UPDATE   MONEY_COLLECTIONS ");
        _sb.AppendLine("    SET   MC_OUT_CENTS            = CASE WHEN @pOutCents IS NULL THEN MC_OUT_CENTS ELSE ISNULL(MC_OUT_CENTS,0) + @pOutCents END ");
        _sb.AppendLine("        , MC_OUT_BILLS            = CASE WHEN @pOutBills IS NULL THEN MC_OUT_BILLS ELSE ISNULL(MC_OUT_BILLS,0) + @pOutBills END ");
        _sb.AppendLine("        , MC_OUT_COINS            = CASE WHEN @pOutCoins IS NULL THEN MC_OUT_COINS ELSE ISNULL(MC_OUT_COINS,0) + @pOutCoins END ");
        _sb.AppendLine("        , MC_EXPECTED_COIN_AMOUNT = CASE WHEN @pInCents  IS NULL THEN MC_EXPECTED_COIN_AMOUNT ELSE ISNULL(MC_EXPECTED_COIN_AMOUNT,0)  + @pInCents END) ");
        _sb.AppendLine("  WHERE   MC_COLLECTION_ID        = @pSessionId ");
        _sb.AppendLine("    AND   MC_STATUS               = @pOpenStatus ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pSessionId", SqlDbType.BigInt).SourceColumn = "SESSION_ID";
          _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).SourceColumn = "TERMINAL_ID";
          _sql_cmd.Parameters.Add("@pCollectionId", SqlDbType.BigInt).SourceColumn = "MONEY_COLLECTION_ID";
          _sql_cmd.Parameters.Add("@pOutCents", SqlDbType.Decimal).SourceColumn = "OUT_CENTS";
          _sql_cmd.Parameters.Add("@pOutBills", SqlDbType.Decimal).SourceColumn = "OUT_BILLS";
          _sql_cmd.Parameters.Add("@pOutCoins", SqlDbType.Decimal).SourceColumn = "OUT_COINS";
          _sql_cmd.Parameters.Add("@pInCents", SqlDbType.Decimal).SourceColumn = "IN_CENTS";
          _sql_cmd.Parameters.Add("@pOpenStatus", SqlDbType.Int).Value = (Int32)TITO_MONEY_COLLECTION_STATUS.OPEN;

          using (SqlDataAdapter _sql_adap = new SqlDataAdapter())
          {
            // Execute Batch Update
            _sql_adap.InsertCommand = _sql_cmd;
            _sql_adap.ContinueUpdateOnError = true;
            _sql_adap.UpdateBatchSize = 500;
            _sql_adap.InsertCommand.UpdatedRowSource = UpdateRowSource.None;

            _num_rows_inserted = _sql_adap.Update(m_gaming_hall_meters_by_session);

            // Errors control
            if (_num_rows_inserted != m_gaming_hall_meters_by_session.Rows.Count)
            {
              Log.Message("Unable to update MONEY_COLLECTION_METERS or MONEY_COLLECTIONS. No records will be updated/inserted");

              foreach (DataRow _dr in m_gaming_hall_meters_by_session.Rows)
              {
                if (_dr.HasErrors)
                {
                  Log.Message(_dr["SESSION_ID"].ToString() + "/" + _dr["TERMINAL_ID"].ToString() + " (SESSION_ID/TERMINAL_ID)");
                  Log.Message("     *** Error: " + _dr.RowError);
                }
              }

        return false;
      }
          }
        }

      return true;

      }// try
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // UpdateMCAndMCMForGamingHall

    private static void SetToModified(DataRow[] Rows)
    {
      foreach (DataRow _row in Rows)
      {
        if (_row.RowState != DataRowState.Unchanged)
        {
          _row.AcceptChanges();
        }
        _row.SetModified();
      }
    }

    #endregion

  }// class WCP_SASMeters

}// namespace WSI.WCP
