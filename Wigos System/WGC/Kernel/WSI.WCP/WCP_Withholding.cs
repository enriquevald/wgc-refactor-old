//------------------------------------------------------------------------------
// Copyright � 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
//
//   MODULE NAME : WCP_Withholding.cs
//
//   DESCRIPTION : WCP_Withholding class
//
// REVISION HISTORY :
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 30-NOV-2016 RAB    First release.
// 29-MAY-2017 DHA    Bug 27734:Background constancy generation error
//------------------------------------------------------------------------------

using System;
using WSI.Common;
using System.Threading;

namespace WSI.WCP
{
  /// <summary>
  /// WCP_Withholding class
  /// </summary>
  public static class WCP_Withholding
  {
    #region Constants
    private const Int32 MINIMUM_WAIT = 1000;      // 1 second
    #endregion

    #region Members
    private static Thread m_thread;
    #endregion

    #region Public functions
    /// <summary>
    /// Initialize WCP_Withholding class 
    /// </summary>
    public static void Init()
    {
      m_thread = new Thread(WithholdingThread);
      m_thread.Name = "WithholdingThread";

      m_thread.Start();
    }
    #endregion

    #region Private Functions

    /// <summary>
    /// Main thread
    /// </summary>
    static private void WithholdingThread()
    {
      Int32 _wait_time;
      _wait_time = Witholding.WAIT_FOR_GENERATE_WITHHOLDING * 1000;

      while (true)
      {
        Thread.Sleep(_wait_time);

        _wait_time = Witholding.WitholdingDocumentThreadWaitSeconds() * 1000;
        if (_wait_time < MINIMUM_WAIT)
        {
          _wait_time = MINIMUM_WAIT;
        }

        if (!Services.IsPrincipal("WCP"))
        {
          Misc.WriteLog(String.Format("WCP_AccountsWitholding.WithholdingThread - Not is principal"), Log.Type.Message);
          continue;
        }

        try
        {
          Misc.WriteLog(String.Format("WCP_AccountsWitholding.WithholdingThread - Thread"), Log.Type.Message);
          // Withholding Evidence
          if (!WCP_AccountsWitholding.WithOldingEvidences())
          {
            Log.Warning("Exception in function: WCP_AccountsWitholdingEvidence");
          }
        }
        catch (Exception _ex)
        {
          Log.Warning("WCP_Withholding.WithholdingThread");
          Log.Exception(_ex);
        }
      }
    } // WithholdingThread

    #endregion
  } // Class WCP_Withholding
} // Namespace WSI.WCP