//------------------------------------------------------------------------------
// Copyright Â© 2012 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WKT_ProcessPlayerRequestGift.cs
// 
//   DESCRIPTION: WKT_ProcessPlayerRequestGift class
// 
//        AUTHOR: Susana Samso
// 
// CREATION DATE: 21-MAY-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 21-MAY-2012 SSC    First version.
// 09-AUG-2012 XIT    Added Response code error
// 13-FEB-2013 HBB & MPO    Added functionality of one cashier session for each PromoBOX.
// 13-NOV-2014 JMM    Fixed Bug WIG-1675: LCD can't redeem points when cage is not enabled
// 26-FEB-2015 AMF    Fixed Bug WIG-2100: Show terminal data
// 19-FEB-2016 ETP    Product Backlog Item 9761:Dual Currency: Promociones y canjes de puntos por crédito desde el Display Touch.
// 12-SEP-2016 ETP    PBI 17561: Print Tito Ticket in Promobox Gifts.
// 01-DIC-2016 FAV    PBI 20407: TITO Ticket: Calculated fields
//------------------------------------------------------------------------------

using System;
using WSI.Common;
using WSI.WCP;
using WSI.Common.InTouch;
using System.Collections;

public static partial class WCP_WKT
{
  private static void WKT_ProcessPlayerRequestGift(WCP_Message WktRequest, WCP_Message WktResponse)
  {
    WCP_WKT_MsgPlayerRequestGift _request;
    WCP_WKT_MsgPlayerRequestGiftReply _response;
    
    Int32 _response_code;
    WCPPlayerRequest _player = new WCPPlayerRequest();
    ArrayList _voucher_list;
    Ticket _tickets;
    MultiPromos.PromoBalance _promo_balance;

    
    _request = (WCP_WKT_MsgPlayerRequestGift)WktRequest.MsgContent;
    _response = (WCP_WKT_MsgPlayerRequestGiftReply)WktResponse.MsgContent;

    _player.GiftId = _request.GiftId;
    _player.TrackData = _request.Player.Trackdata;
    _player.PlayerPin  = _request.Player.Pin;
    _player.GiftPrice = _request.GiftPrice;
    _player.GiftNumUnits  = _request.GiftNumUnits;
    _player.DrawId  = _request.DrawId;
    _player.TitoPrinterIsReady = _request.TitoPrinterIsReady;
    _response_code = (int)WCP_ResponseCodes.WCP_RC_WKT_ERROR;
    _player.TerminalName = WktRequest.MsgHeader.TerminalId;

    _voucher_list = new ArrayList();
    _tickets = null;
    _promo_balance = null;
    _response.VoucherList = _voucher_list;
     

    if (!new WCPPlayerRequest().ProcessPlayerRequestGift(_player, out  _voucher_list, out  _tickets, out _promo_balance, out _response_code))
    {

      WktResponse.MsgHeader.ResponseCode = (WSI.WCP.WCP_ResponseCodes)_response_code;

      return;
    }

    if (_voucher_list != null)
    {
      _response.VoucherList = _voucher_list;
    }
    if (_tickets != null)
    {
      _response.Ticket = _tickets;
    }
   
     _response.PromoBalance = _promo_balance;
 
  }

  private static WCP_ResponseCodes GetResponseCode(Gift.GIFT_MSG _errmsg)
  {
    String _msg = "WKT_";
    WCP_ResponseCodes _response;

    _response = WCP_ResponseCodes.WCP_RC_ERROR;
    try
    {
      _msg += _errmsg.ToString();
      _response = (WCP_ResponseCodes)Enum.Parse(typeof(WCP_ResponseCodes), _msg);

    }
    catch (Exception _ex)
    {
      
      Log.Exception(_ex);
    }

    return _response;
  }

}
