//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WKT_ProcessGetPlayerActivity.cs
// 
//   DESCRIPTION:  WKT_ProcessGetPlayerActivity class
// 
//        AUTHOR: Xavier Iglesia
// 
// CREATION DATE: 14-AUG-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 14-AUG-2012 XIT    First version.
// 02-OCT-2012 JMM    Add new movemnents & some code cleaning.
// 06-FEB-2013 JMM    Quantity removed on holder level change movements.
// 07-FEB-2013 JMM    Account points redeem status checking added.
// 29-MAY-2013 LEM    Fixed Bug #807: Wrong description for Account Block and Unblock movements
// 20-AUG-2013 FBA    Added new movement (DepositCancelation) and sets default value for movements.
// 06-NOV-2013 JMM    Fixed Bug WIG-388: Quantity removed on the followin movements: DrawTicketPrint, AccountBlocked, AccountUnblocked, AccountPINChange, AccountPINRandom & AccountPersonalization
// 25-APR-2014 DHA & JML Rename/Added TITO Cashier/Account Movements
// 23-DIC-2015 SGB    Bug 7988: Don't show create ticket in history activities
// 03-FEB-2016 ETP    FIXED BUG 8941 NLS And bucket movement are incorrect
// 04-FEB-2016 JRC    PBI 7909: Multiple Buckets
// 02-MAR-2016 DHA    Product Backlog Item 10085:Winpot - Added Tax Provisions movements
// 08-FEB-2017 FAV    PBI 25268:Third TAX - Payment Voucher
// 19-JUN-2017 RAB    PBI 28000: WIGOS-2732 - Rounding - Movements
// 06-JUL-2017 RAB    PBI 28422: WIGOS-2702 Rounding - Consume the remaining amount
// 24-JUL-2017 ETP    Bug 28927:[WIGOS-3938] Missing credit line information in Promobox
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using WSI.Common;
using WSI.WCP;

public static partial class WCP_WKT
{

  public class WKT_Activity
  {
    public String Date;
    public String Description;
    public String Quantity;
  }

  //------------------------------------------------------------------------------
  // PURPOSE: Manages the bussiness logic of the message, doing the operations and filling the answer
  //  PARAMS:
  //      - INPUT:
  //          - WktRequest       : Input message
  //          - WktResponse      : Message to return
  //
  //      - OUTPUT:
  //         
  //
  //      - RETURNS:
  //       
  //
  private static void WKT_ProcessGetPlayerActivity(WCP_Message WktRequest, WCP_Message WktResponse)
  {
    WCP_WKT_MsgGetPlayerActivity _request;
    WCP_WKT_MsgGetPlayerActivityReply _response;
    WCP_ResponseCodes _response_code;
    Int64 _account_id;

    _request = (WCP_WKT_MsgGetPlayerActivity)WktRequest.MsgContent;
    _response = (WCP_WKT_MsgGetPlayerActivityReply)WktResponse.MsgContent;

    try
    {
      if (!WKT_PlayerLogin(_request.Player.Trackdata, _request.Player.Pin, out _account_id, out _response_code))
      {
        WktResponse.MsgHeader.ResponseCode = _response_code;
        return;
      }
      WktResponse.MsgHeader.ResponseCode = GetPlayerActivity(_account_id, _response);

      WktResponse.MsgContent = _response;

    }
    catch (Exception _ex)
    {
      Log.Exception(_ex);
      WktResponse.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_WKT_ERROR;
    }
  }//WKT_ProcessGetPlayerActivity

  enum ENUM_ACTIVITY_VALUE_TYPE
  {
    MONEY = 1,
    POINTS = 2
  }

  private static WCP_ResponseCodes GetPlayerActivity(Int64 AccountId, WCP_WKT_MsgGetPlayerActivityReply MsgResponse)
  {
    StringBuilder _sb;
    WKT_Activity _activity;
    MovementType _movement_type;
    String _movement_desc;
    Decimal _quantity;
    Boolean _is_currency;
    List<PromoBOX.Functionality> _func_list;
    String _aux_where;
    String _aux_index;

    try
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        // If PromoBOX functionality ACTIVITY_ONLY_POINTS is ON, tx will only return point related movements
        _func_list = PromoBOX.ReadPromoBoxFunc(PromoBOX.Functionality.ACTIVITY_ONLY_POINTS,
                                                true,
                                                _db_trx.SqlTransaction);

        _aux_where = " AND ( AM_TYPE NOT IN (@AvoidType01) )";
        _aux_index = "IX_MOVEMENTS_ACCOUNT_DATE";

        if (_func_list.Count > 0)
        {
          _aux_where = " AND ( AM_TYPE IN (@Type1, @Type2, @Type3, @Type4, @Type5, @Type6, @Type7, @Type8, @Type9, @Type10, @Type11, @Type12, @Type13, @Type14, @Type15, @Type16";
          _aux_where = _aux_where + ", @Type17,@Type18,@Type19,@Type20,@Type21,@Type22,@Type23,@Type24,@Type25,@Type26,@Type27,@Type28,@Type29,@Type30,@Type31,@Type32,@Type33,@Type34,@Type35,@Type36,@Type37,@Type38,@Type39) )"; 
          _aux_index = "IX_TYPE_DATE_ACCOUNT";
        }

        _sb = new StringBuilder();

        // PromoBOX displays up to 70 movements
        _sb.Append("SELECT TOP 100 AM_TYPE "
                              + " , '  ' AS TYPE_NAME"
                              + " , AM_DATETIME "
                              + " , '  ' AS USER_NAME "
                              + " , CASE WHEN AM_TERMINAL_ID IS NOT NULL THEN AM_TERMINAL_NAME "
                              + "         WHEN AM_CASHIER_ID IS NOT NULL THEN AM_CASHIER_NAME "
                              + "         WHEN AM_TERMINAL_ID IS NULL AND AM_CASHIER_ID IS NULL THEN AM_CASHIER_NAME"
                              + "         ELSE NULL "
                              + "   END AM_TERM_NAME "
                              + " , CASE WHEN AM_TERMINAL_ID IS NOT NULL THEN AM_TERMINAL_ID "
                              + "        WHEN AM_CASHIER_ID IS NOT NULL THEN AM_CASHIER_ID "
                              + "        ELSE NULL "
                              + "   END AM_TERM_ID "
                              + " , AM_INITIAL_BALANCE "
                              + " , AM_SUB_AMOUNT "
                              + " , AM_ADD_AMOUNT "
                              + " , AM_FINAL_BALANCE "
                              + " , ISNULL(AM_OPERATION_ID, 0) "
                              + " , '' AS COLOR_ROW "
                              + " , AM_DETAILS "
                           + " FROM ACCOUNT_MOVEMENTS WITH (INDEX (" + _aux_index + "))"
                          + " WHERE AM_ACCOUNT_ID = @pAccountId"
                                  + _aux_where
                            + " AND AM_DATETIME > DATEADD (YEAR, -1, GETDATE())"
                          + " ORDER BY AM_DATETIME DESC, AM_MOVEMENT_ID DESC "
                  );

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString()))
        {
          _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;

          if (_func_list.Count > 0)
          {
            // STR_FRM_LAST_MOVEMENTS_075  Puntos - Cancelaci�n de Regalo
            // STR_FRM_LAST_MOVEMENTS_046  Puntos - Cr�ditos No Redimibles
            // STR_FRM_LAST_MOVEMENTS_055  Puntos - Cr�ditos Redimibles
            // STR_FRM_LAST_MOVEMENTS_032  Puntos - Entrega Regalo
            // STR_FRM_LAST_MOVEMENTS_045  Puntos - Petici�n Regalo
            // STR_FRM_LAST_MOVEMENTS_051  Puntos - Servicios
            // STR_FRM_LAST_MOVEMENTS_050  Puntos - Ticket Sorteo
            // STR_FRM_LAST_MOVEMENTS_056  Premio Caducado
            // STR_FRM_LAST_MOVEMENTS_034  Puntos Caducados
            // STR_FRM_LAST_MOVEMENTS_061  A�adir/Fijar puntos
            // STR_FRM_LAST_MOVEMENTS_037  Ticket Sorteo
            // STR_FRM_LAST_MOVEMENTS_044  Puntos Ganados 
            // STR_FRM_LAST_MOVEMENTS_070  Promo. de Puntos
            // STR_FRM_LAST_MOVEMENTS_071  Cancelaci�n Promo. de Puntos

            _sql_cmd.Parameters.Add("@Type1", SqlDbType.Int).Value = MovementType.CancelGiftInstance;
            _sql_cmd.Parameters.Add("@Type2", SqlDbType.Int).Value = MovementType.PointsToNotRedeemable;
            _sql_cmd.Parameters.Add("@Type3", SqlDbType.Int).Value = MovementType.PointsToRedeemable;
            _sql_cmd.Parameters.Add("@Type4", SqlDbType.Int).Value = MovementType.PointsToGiftRequest;
            _sql_cmd.Parameters.Add("@Type5", SqlDbType.Int).Value = MovementType.PointsGiftDelivery;
            _sql_cmd.Parameters.Add("@Type6", SqlDbType.Int).Value = MovementType.PointsExpired;
            _sql_cmd.Parameters.Add("@Type7", SqlDbType.Int).Value = MovementType.PointsToDrawTicketPrint;
            _sql_cmd.Parameters.Add("@Type8", SqlDbType.Int).Value = MovementType.PointsGiftServices;
            _sql_cmd.Parameters.Add("@Type9", SqlDbType.Int).Value = MovementType.PrizeExpired;
            _sql_cmd.Parameters.Add("@Type10", SqlDbType.Int).Value = MovementType.ManuallyAddedPointsOnlyForRedeem;
            _sql_cmd.Parameters.Add("@Type11", SqlDbType.Int).Value = MovementType.DrawTicketPrint;
            _sql_cmd.Parameters.Add("@Type12", SqlDbType.Int).Value = MovementType.PointsAwarded;
            _sql_cmd.Parameters.Add("@Type13", SqlDbType.Int).Value = MovementType.PromotionPoint;
            _sql_cmd.Parameters.Add("@Type14", SqlDbType.Int).Value = MovementType.CancelPromotionPoint;
            _sql_cmd.Parameters.Add("@Type15", SqlDbType.Int).Value = MovementType.TITO_TicketCashierPrintedPromoRedeemable;
            _sql_cmd.Parameters.Add("@Type16", SqlDbType.Int).Value = MovementType.TITO_TicketCashierPrintedPromoNotRedeemable;

            _sql_cmd.Parameters.Add("@Type17", SqlDbType.Int).Value = (int)MovementType.MULTIPLE_BUCKETS_EndSession + (int)Buckets.BucketId.RankingLevelPoints; //JRC:MovementType.PointsAwarded;
            _sql_cmd.Parameters.Add("@Type18", SqlDbType.Int).Value = (int)MovementType.MULTIPLE_BUCKETS_Manual_Add + (int)Buckets.BucketId.RankingLevelPoints; //JRC:MovementType.PointsAwarded;
            _sql_cmd.Parameters.Add("@Type19", SqlDbType.Int).Value = (int)MovementType.MULTIPLE_BUCKETS_Manual_Sub + (int)Buckets.BucketId.RankingLevelPoints; //JRC:MovementType.PointsAwarded;
            _sql_cmd.Parameters.Add("@Type20", SqlDbType.Int).Value = (int)MovementType.MULTIPLE_BUCKETS_Expired + (int)Buckets.BucketId.RankingLevelPoints; //JRC:MovementType.PointsAwarded;
            _sql_cmd.Parameters.Add("@Type21", SqlDbType.Int).Value = (int)MovementType.MULTIPLE_BUCKETS_Manual_Set + (int)Buckets.BucketId.RankingLevelPoints; //JRC:MovementType.PointsAwarded;

            _sql_cmd.Parameters.Add("@Type22", SqlDbType.Int).Value = (int)MovementType.MULTIPLE_BUCKETS_EndSession + (int)Buckets.BucketId.RedemptionPoints; //JRC:MovementType.PointsAwarded;
            _sql_cmd.Parameters.Add("@Type23", SqlDbType.Int).Value = (int)MovementType.MULTIPLE_BUCKETS_Manual_Add + (int)Buckets.BucketId.RedemptionPoints; //JRC:MovementType.PointsAwarded;
            _sql_cmd.Parameters.Add("@Type24", SqlDbType.Int).Value = (int)MovementType.MULTIPLE_BUCKETS_Manual_Sub + (int)Buckets.BucketId.RedemptionPoints; //JRC:MovementType.PointsAwarded;
            _sql_cmd.Parameters.Add("@Type25", SqlDbType.Int).Value = (int)MovementType.MULTIPLE_BUCKETS_Expired + (int)Buckets.BucketId.RedemptionPoints; //JRC:MovementType.PointsAwarded;
            _sql_cmd.Parameters.Add("@Type26", SqlDbType.Int).Value = (int)MovementType.MULTIPLE_BUCKETS_Manual_Set + (int)Buckets.BucketId.RedemptionPoints; //JRC:MovementType.PointsAwarded;

            _sql_cmd.Parameters.Add("@Type27", SqlDbType.Int).Value = (int)MovementType.MULTIPLE_BUCKETS_EndSession + (int)Buckets.BucketId.Credit_NR; //JRC:MovementType.PointsAwarded;
            _sql_cmd.Parameters.Add("@Type28", SqlDbType.Int).Value = (int)MovementType.MULTIPLE_BUCKETS_Manual_Add + (int)Buckets.BucketId.Credit_NR; //JRC:MovementType.PointsAwarded;
            _sql_cmd.Parameters.Add("@Type29", SqlDbType.Int).Value = (int)MovementType.MULTIPLE_BUCKETS_Manual_Sub + (int)Buckets.BucketId.Credit_NR; //JRC:MovementType.PointsAwarded;
            _sql_cmd.Parameters.Add("@Type30", SqlDbType.Int).Value = (int)MovementType.MULTIPLE_BUCKETS_Expired + (int)Buckets.BucketId.Credit_NR; //JRC:MovementType.PointsAwarded;
            _sql_cmd.Parameters.Add("@Type31", SqlDbType.Int).Value = (int)MovementType.MULTIPLE_BUCKETS_Manual_Set + (int)Buckets.BucketId.Credit_NR; //JRC:MovementType.PointsAwarded;

            _sql_cmd.Parameters.Add("@Type32", SqlDbType.Int).Value = (int)MovementType.MULTIPLE_BUCKETS_EndSession + (int)Buckets.BucketId.Credit_RE; //JRC:MovementType.PointsAwarded;
            _sql_cmd.Parameters.Add("@Type33", SqlDbType.Int).Value = (int)MovementType.MULTIPLE_BUCKETS_Manual_Add + (int)Buckets.BucketId.Credit_RE; //JRC:MovementType.PointsAwarded;
            _sql_cmd.Parameters.Add("@Type34", SqlDbType.Int).Value = (int)MovementType.MULTIPLE_BUCKETS_Manual_Sub + (int)Buckets.BucketId.Credit_RE; //JRC:MovementType.PointsAwarded;
            _sql_cmd.Parameters.Add("@Type35", SqlDbType.Int).Value = (int)MovementType.MULTIPLE_BUCKETS_Expired + (int)Buckets.BucketId.Credit_RE; //JRC:MovementType.PointsAwarded;
            _sql_cmd.Parameters.Add("@Type36", SqlDbType.Int).Value = (int)MovementType.MULTIPLE_BUCKETS_Manual_Set + (int)Buckets.BucketId.Credit_RE; //JRC:MovementType.PointsAwarded;

            _sql_cmd.Parameters.Add("@Type37", SqlDbType.Int).Value = (int)MovementType.CreditLineGetMarker;
            _sql_cmd.Parameters.Add("@Type38", SqlDbType.Int).Value = (int)MovementType.CreditLineGetMarkerCardReplacement;
            _sql_cmd.Parameters.Add("@Type39", SqlDbType.Int).Value = (int)MovementType.CreditLinePayback;

          }

          _sql_cmd.Parameters.Add("@AvoidType01", SqlDbType.Int).Value = MovementType.PointsStatusChanged;

          using (SqlDataReader _sql_dr = _db_trx.ExecuteReader(_sql_cmd))
          {
            while (_sql_dr.Read())
            {
              _movement_type = (MovementType)_sql_dr.GetInt32(0);
              _quantity = GetQuantity(_sql_dr);

              // Get Activity text (NLS) and amount format (currecny or quantity)
              GetMovementProperties(_movement_type, _sql_dr, out _movement_desc, out _is_currency);

              _activity = new WKT_Activity();
              _activity.Description = _movement_desc;
              _activity.Date = XML.XmlDateTimeString(_sql_dr.GetDateTime(2));

              // JMM 06-FEB-2013: Holder level change movement has no quantity related
              if (_movement_type != MovementType.ManualHolderLevelChanged
                && _movement_type != MovementType.HolderLevelChanged
                && _movement_type != MovementType.DrawTicketPrint
                && _movement_type != MovementType.AccountBlocked
                && _movement_type != MovementType.AccountUnblocked
                && _movement_type != MovementType.AccountPINChange
                && _movement_type != MovementType.AccountPINRandom
                && _movement_type != MovementType.AccountPersonalization)
              {
                if (_is_currency)
                {
                  Currency _currency;
                  _currency = _quantity;
                  _activity.Quantity = _currency.ToString();
                }
                else
                {
                  _activity.Quantity = _quantity.ToString(Gift.FORMAT_POINTS_DEC);
                }
              }

              MsgResponse.Activities.Add(_activity);
            }
          }
        }
      } // Using DB_TRX

      return WCP_ResponseCodes.WCP_RC_WKT_OK;

    }
    catch (Exception _ex)
    {
      Log.Exception(_ex);
      return WCP_ResponseCodes.WCP_RC_WKT_ERROR;
    }
  }

  //------------------------------------------------------------------------------
  // PURPOSE: Gets the quantity added or substracted
  //  PARAMS:
  //      - INPUT:
  //          - SqlDataReader _sql_dr
  //
  //      - OUTPUT:
  //         
  //
  //      - RETURNS:
  //          - String 
  //
  private static Decimal GetQuantity(SqlDataReader _sql_dr)
  {
    if ((_sql_dr.GetDecimal(7) != 0) || (_sql_dr.GetDecimal(8) != 0))
    {
      if (_sql_dr.GetDecimal(7) != 0)
      {
        //TODO ADD -
        return _sql_dr.GetDecimal(7);
      }
      else
      {
        return _sql_dr.GetDecimal(8);
      }
    }
    else
    {
      return _sql_dr.GetDecimal(9);
    }

  } //GetQuantity

  //------------------------------------------------------------------------------
  // PURPOSE: Calculates the name of the movement done
  //  PARAMS:
  //      - INPUT:
  //          - Movement Type
  //
  //      - OUTPUT:
  //          - Movement Description
  //          - Movement Print Format
  //
  //      - RETURNS:
  //          - String 

  private static void GetMovementProperties(MovementType Type, SqlDataReader _sql_dr, out String Desc, out Boolean IsCurrency)
  {
    BucketsForm _buckets_forms;
    _buckets_forms = new BucketsForm();

    switch (Type)
    {
      case MovementType.DrawTicketPrint:
      case MovementType.PointsAwarded:
      case MovementType.PointsToGiftRequest:
      case MovementType.PointsGiftDelivery:
      case MovementType.PointsGiftServices:
      case MovementType.PointsExpired:
      case MovementType.ManuallyAddedPointsOnlyForRedeem:
      case MovementType.PromotionPoint:
      case MovementType.CancelPromotionPoint:
      case MovementType.CancelGiftInstance:
        IsCurrency = false;
        break;

      default:
        if (Type > MovementType.MULTIPLE_BUCKETS_EndSession && Type <= MovementType.MULTIPLE_BUCKETS_Manual_Set_Last) // (1100 - 1599)
        {
          BucketsForm _buckets_form;
          Buckets.BucketType _bucket_type;

          _buckets_form = new BucketsForm();
          _buckets_form.GetBucketType(Type, out _bucket_type);

          IsCurrency = Buckets.GetBucketUnits(_bucket_type) == Buckets.BucketUnits.Money;

          break;
        }

        IsCurrency = true;
        break;
    }

    // TO BE UNIFIED WITH CASHIER AND GUI !!!!!!!!!!!
    switch (Type)
    {
      case MovementType.Play:
        Desc = Resource.String("STR_FRM_LAST_MOVEMENTS_001");
        break;

      case MovementType.CashIn:
        {
          Desc = Misc.ReadGeneralParams("Cashier", "Split.A.Name");
          break;
        }
      case MovementType.CashOut:
        {
          Desc = Resource.String("STR_FRM_LAST_MOVEMENTS_003");
          break;
        }
      case MovementType.Devolution:
        {
          Desc = Resource.String("STR_FRM_LAST_MOVEMENTS_004");
          break;
        }
      case MovementType.TaxOnPrize1:
        {
          Desc = Misc.ReadGeneralParams("Cashier", "Tax.OnPrize.1.Name");
          break;
        }
      case MovementType.TaxOnPrize2:
        {
          Desc = Misc.ReadGeneralParams("Cashier", "Tax.OnPrize.2.Name");
          break;
        }
      case MovementType.TaxOnPrize3:
        {
          Desc = Misc.ReadGeneralParams("Cashier", "Tax.OnPrize.3.Name");
          break;
        }
      case MovementType.StartCardSession:
        {
          Desc = Resource.String("STR_FRM_LAST_MOVEMENTS_007");
          break;
        }
      case MovementType.CancelStartSession:
        {
          Desc = Resource.String("STR_FRM_LAST_MOVEMENTS_066");
          break;
        }
      case MovementType.EndCardSession:
        {
          Desc = Resource.String("STR_FRM_LAST_MOVEMENTS_008");
          break;
        }
      case MovementType.PromotionNotRedeemable:
        {
          Desc = Resource.String("STR_FRM_LAST_MOVEMENTS_009");
          break;
        }
      case MovementType.CashInCoverCoupon:
        {
          Desc = Resource.String("STR_FRM_LAST_MOVEMENTS_062");
          break;
        }
      case MovementType.DepositIn:
        {
          Desc = Resource.String("STR_FRM_LAST_MOVEMENTS_010");
          break;
        }
      case MovementType.DepositOut:
        {
          Desc = Resource.String("STR_FRM_LAST_MOVEMENTS_011");
          break;
        }
      case MovementType.CardReplacement:
        {
          Desc = Resource.String("STR_FRM_LAST_MOVEMENTS_036");
          break;
        }
      case MovementType.CancelNotRedeemable:
        {
          Desc = Resource.String("STR_FRM_LAST_MOVEMENTS_012");
          break;
        }
      case MovementType.PromoCredits:
        {
          Desc = Resource.String("STR_FRM_LAST_MOVEMENTS_015");
          break;
        }
      case MovementType.PromoToRedeemable:
        {
          Desc = Resource.String("STR_FRM_LAST_MOVEMENTS_016");
          break;
        }
      case MovementType.PromoExpired:
        {
          Desc = Resource.String("STR_FRM_LAST_MOVEMENTS_017");
          break;
        }
      case MovementType.PromoCancel:
        {
          Desc = Resource.String("STR_FRM_LAST_MOVEMENTS_018");
          break;
        }
      case MovementType.PromoStartSession:
        {
          Desc = Resource.String("STR_FRM_LAST_MOVEMENTS_019");
          break;
        }
      case MovementType.PromoEndSession:
        {
          Desc = Resource.String("STR_FRM_LAST_MOVEMENTS_020");
          break;
        }
      case MovementType.CreditsExpired:
        {
          Desc = Resource.String("STR_FRM_LAST_MOVEMENTS_021");
          break;
        }
      case MovementType.PrizeExpired:
        {
          Desc = Resource.String("STR_FRM_LAST_MOVEMENTS_056");
          break;
        }
      case MovementType.ReservedExpired:
        {
          Desc = Resource.String("STR_MOVEMENT_TYPE_RESERVED_EXPIRED");
          break;
        }
      case MovementType.CreditsNotRedeemableExpired:
        {
          Desc = Resource.String("STR_FRM_LAST_MOVEMENTS_022");
          break;
        }
      case MovementType.CreditsNotRedeemable2Expired:
        {
          Desc = Resource.String("STR_FRM_LAST_MOVEMENTS_063");
          break;
        }
      case MovementType.DrawTicketPrint:
        {
          Desc = Resource.String("STR_FRM_LAST_MOVEMENTS_037");

          break;
        }
      case MovementType.PointsToDrawTicketPrint:
        {
          Desc = Resource.String("STR_FRM_LAST_MOVEMENTS_050");
          break;
        }
      case MovementType.Handpay:
        {
          Desc = Resource.String("STR_FRM_LAST_MOVEMENTS_038");
          break;
        }
      case MovementType.HandpayCancellation:
        {
          Desc = Resource.String("STR_FRM_LAST_MOVEMENTS_039");
          break;
        }
      case MovementType.ManualEndSession:
        {
          Desc = Resource.String("STR_FRM_LAST_MOVEMENTS_040");
          break;
        }
      case MovementType.PromoManualEndSession:
        {
          Desc = Resource.String("STR_FRM_LAST_MOVEMENTS_041");
          break;
        }
      case MovementType.ManualHandpay:
        {
          Desc = Resource.String("STR_FRM_LAST_MOVEMENTS_042");
          break;
        }
      case MovementType.ManualHandpayCancellation:
        {
          Desc = Resource.String("STR_FRM_LAST_MOVEMENTS_043");
          break;
        }
      case MovementType.PointsAwarded:
        {
          Desc = Resource.String("STR_FRM_LAST_MOVEMENTS_044");
          break;
        }
      case MovementType.PointsToGiftRequest:
        {
          Desc = Resource.String("STR_FRM_LAST_MOVEMENTS_045");
          break;
        }
      case MovementType.PointsToNotRedeemable:
        {
          Desc = Resource.String("STR_FRM_LAST_MOVEMENTS_046");
          break;
        }
      case MovementType.PointsToRedeemable:
        {
          Desc = Resource.String("STR_FRM_LAST_MOVEMENTS_055");
          break;
        }
      case MovementType.PointsGiftDelivery:
        {
          Desc = Resource.String("STR_FRM_LAST_MOVEMENTS_032");
          break;
        }
      case MovementType.PointsGiftServices:
        {
          Desc = Resource.String("STR_FRM_LAST_MOVEMENTS_051");
          break;
        }
      case MovementType.PointsExpired:
        {
          Desc = Resource.String("STR_FRM_LAST_MOVEMENTS_034");
          break;
        }
      // RRB 29-AUG-2012 Added ManualHolderLevelChanged
      case MovementType.HolderLevelChanged:
      case MovementType.ManualHolderLevelChanged:
        {
          String _msg;
          String _new_level;
          String _old_level;
          String[] _level_name = new String[5];

          _level_name[0] = "---";
          _level_name[1] = Misc.ReadGeneralParams("PlayerTracking", "Level01.Name");
          _level_name[2] = Misc.ReadGeneralParams("PlayerTracking", "Level02.Name");
          _level_name[3] = Misc.ReadGeneralParams("PlayerTracking", "Level03.Name");
          _level_name[4] = Misc.ReadGeneralParams("PlayerTracking", "Level04.Name");

          _new_level = _level_name[((Int32)_sql_dr.GetDecimal(8)) % 5];
          _old_level = _level_name[((Int32)_sql_dr.GetDecimal(7)) % 5];

          _msg = Type == MovementType.HolderLevelChanged ? "STR_FRM_LAST_MOVEMENTS_052" : "STR_FRM_LAST_MOVEMENTS_073";

          Desc = Resource.String(_msg, _new_level, _old_level);
          break;

        }

      case MovementType.PrizeCoupon:
        {
          Desc = Resource.String("STR_FRM_LAST_MOVEMENTS_053");
          break;
        }
      case MovementType.DEPRECATED_RedeemableSpentUsedInPromotions:
        {
          Desc = Resource.String("STR_FRM_LAST_MOVEMENTS_054");
          break;
        }
      // MBF 11-OCT-2011
      case MovementType.CardCreated:
        {
          Desc = Resource.String("STR_FRM_LAST_MOVEMENTS_057");
          break;
        }
      case MovementType.AccountPersonalization:
        {
          Desc = Resource.String("STR_FRM_LAST_MOVEMENTS_058");
          break;
        }
      case MovementType.AccountPINChange:
        {
          Desc = Resource.String("STR_FRM_LAST_MOVEMENTS_059");
          break;
        }
      case MovementType.AccountPINRandom:
        {
          Desc = Resource.String("STR_FRM_LAST_MOVEMENTS_060");
          break;
        }
      // JCA 25-JAN-2012
      case MovementType.ManuallyAddedPointsOnlyForRedeem:
        {
          Desc = Resource.String("STR_FRM_LAST_MOVEMENTS_061");
          break;
        }
      // JCM 24-APR-2012
      case MovementType.CardRecycled:
        {
          Desc = Resource.String("STR_FRM_LAST_MOVEMENTS_067");
          break;
        }
      // DDM 05-JUL-2012
      case MovementType.PromotionRedeemable:
        {
          Desc = Resource.String("STR_FRM_LAST_MOVEMENTS_068");
          break;
        }
      case MovementType.CancelPromotionRedeemable:
        {
          Desc = Resource.String("STR_FRM_LAST_MOVEMENTS_069");
          break;
        }
      // DDM 31-JUL-2012
      case MovementType.PromotionPoint:
        {
          Desc = Resource.String("STR_FRM_LAST_MOVEMENTS_070");
          break;
        }
      case MovementType.CancelPromotionPoint:
        {
          Desc = Resource.String("STR_FRM_LAST_MOVEMENTS_071");
          break;
        }
      case MovementType.TaxReturningOnPrizeCoupon:
        {
          Desc = Resource.String("STR_FRM_LAST_MOVEMENTS_TAX_RETURNING_ON_PRIZE_COUPON");
          break;
        }
      case MovementType.DecimalRounding:
        {
          Desc = Resource.String("STR_FRM_LAST_MOVEMENTS_DECIMAL_ROUNDING");
          break;
        }
      case MovementType.ServiceCharge:
        {
          Desc = Resource.String("STR_FRM_LAST_MOVEMENTS_SERVICE_CHARGE");
          break;
        }
      case MovementType.CancelGiftInstance:
        {
          Desc = Resource.String("STR_FRM_LAST_MOVEMENTS_075");
          break;
        }
      case MovementType.ManuallyAddedPointsForLevel:         //= 68, Add points from GUI
        {
          Desc = Resource.String("STR_MOVEMENT_TYPE_MANUALLY_ADDED_POINTS_FOR_LEVEL");
          break;
        }
      case MovementType.ImportedAccount:                     //= 70, Imported account
        {
          Desc = Resource.String("STR_MOVEMENT_TYPE_IMPORTED_ACCOUNT");
          break;
        }
      case MovementType.ImportedPointsOnlyForRedeem:         //= 71, Imported points (not accounted for the level)
        {
          Desc = Resource.String("STR_MOVEMENT_TYPE_IMPORTED_POINTS_ONLY_FOR_REDEEM");
          break;
        }
      case MovementType.ImportedPointsForLevel:              //= 72, Imported points
        {
          Desc = Resource.String("STR_MOVEMENT_TYPE_IMPORTED_POINTS_FOR_LEVEL");
          break;
        }
      case MovementType.ImportedPointsHistory:               //= 73, History points imported
        {
          Desc = Resource.String("STR_MOVEMENT_TYPE_IMPORTED_POINTS_HISTORY");
          break;
        }
      case MovementType.MultiSiteCurrentLocalPoints:         //= 101, Multisite - Current points in site // similar ManuallyAddedPointsOnlyForRedeem
        {
          Desc = Resource.String("STR_MOVEMENT_TYPE_MULTI_SITE_CURRENT_LOCAL_POINTS");
          break;
        }
      case MovementType.AccountBlocked:                      //= 75, Block account 
        {
          Desc = Resource.String("STR_MOVEMENT_TYPE_BLOCKED");
          break;
        }

      case MovementType.AccountUnblocked:                    //= 76, Unblock account 
        {
          Desc = Resource.String("STR_MOVEMENT_TYPE_UNBLOCKED");
          break;
        }

      case MovementType.AccountInBlacklist:                      //= 240, Account add in blacklist 
        {
          Desc = Resource.String("STR_MOVEMENT_TYPE_ADD_TO_BLACKLIST");
          break;
        }

      case MovementType.AccountNotInBlacklist:                    //= 241,Account remove to blacklist
        {
          Desc = Resource.String("STR_MOVEMENT_TYPE_REMOVE_FROM_BLACKLIST");
          break;
        }

      // FBA 12-AUG-2013 new movement ID 77. Deposit Cancelation
      case MovementType.Cancellation:                       //= 77, Deposit Cancelation
        {
        Desc = Resource.String("STR_FRM_LAST_MOVEMENTS_DEPOSIT_CANCELATION");
        break;
        }
      // DHA 25-APR-2014 modified/added TITO tickets movements
      case MovementType.TITO_TicketCashierPrintedCashable:
        Desc = Resource.String("STR_UC_TITO_TICKET_CASHIER_PRINTED_CASHABLE");
        break;

      case MovementType.TITO_TicketCashierPrintedPromoRedeemable:
        Desc = Resource.String("STR_UC_TITO_TICKET_CASHIER_PRINTED_PROMO_REDEEMABLE");
        break;

      case MovementType.TITO_TicketCashierPrintedPromoNotRedeemable:
        Desc = Resource.String("STR_UC_TITO_TICKET_CASHIER_PRINTED_PROMO_NOT_REDEEMABLE");
        break;

      case MovementType.TITO_TicketMachinePrintedCashable:
        Desc = Resource.String("STR_UC_TITO_TICKET_MACHINE_PRINTED_CASHABLE");
        break;

      case MovementType.TITO_TicketCountRPrintedCashable:
        Desc = Resource.String("STR_UC_TITO_TICKET_COUNTR_PRINTED_CASHABLE");
        break;

      case MovementType.TITO_TicketMachinePrintedPromoNotRedeemable:
        Desc = Resource.String("STR_UC_TITO_TICKET_MACHINE_PRINTED_PROMO_NOT_REDEEMABLE");
        break;

      case MovementType.TITO_TicketMachinePlayedCashable:
        Desc = Resource.String("STR_UC_TITO_TICKET_MACHINE_PLAYED_CASHABLE");
        break;

      case MovementType.TITO_TicketMachinePlayedPromoRedeemable:
        Desc = Resource.String("STR_UC_TITO_TICKET_MACHINE_PLAYED_PROMO_REDEEMABLE");
        break;

      case MovementType.TITO_ticketMachinePlayedPromoNotRedeemable:
        Desc = Resource.String("STR_UC_TITO_TICKET_MACHINE_PLAYED_PROMO_NOT_REDEEMABLE");
        break;

      case MovementType.TITO_TicketReissue:
        Desc = Resource.String("STR_UC_TITO_TICKET_REISSUE");
        break;

      case MovementType.TITO_AccountToTerminalCredit:
        Desc = Resource.String("STR_MOV_TITO_ACCOUNT_TERMINAL");
        break;

      case MovementType.TITO_TerminalToAccountCredit:
        Desc = Resource.String("STR_MOV_TITO_TERMINAL_ACCOUNT");
        break;

      case MovementType.CashdeskDrawParticipation:
        Desc = Resource.String("STR_FRM_LAST_MOVEMENTS_091");
        break;

      case MovementType.CashdeskDrawPlaySession:
        Desc = Resource.String("STR_MOVEMENT_TYPE_CASH_DESK_DRAW_PLAY_SESSION");
        break;

      case MovementType.TITO_TicketMachinePrintedHandpay:
        Desc = Resource.String("STR_UC_TITO_TICKET_MACHINE_PRINTED_HANDPAY");
        break;

      case MovementType.TITO_TicketMachinePrintedJackpot:
        Desc = Resource.String("STR_UC_TITO_TICKET_MACHINE_PRINTED_JACKPOT");
        break;

      // AMF 01-DEC-2014
      case MovementType.CardReplacementForFree:
        Desc = Resource.String("STR_FRM_LAST_MOVEMENTS_FREE_CARD");
        break;

      case MovementType.CardReplacementInPoints:
        Desc = Resource.String("STR_FRM_LAST_MOVEMENTS_BY_POINTS");
        break;

      case MovementType.CashInTax:
        Desc = GeneralParam.GetString("Cashier", "CashInTax.Name", Resource.String("STR_MOVEMENT_TYPE_CASH_IN_TAX"));
        break;

      // DHA 02-MAR-2016
      case MovementType.TaxProvisions:
        Desc = GeneralParam.GetString("Cashier.TaxProvisions", "Voucher.Title", Resource.String("STR_VOUCHER_TAX_PROVISIONS_TITLE"));
        break;

      case MovementType.CashAdvance:
        Desc = Resource.String("STR_VOUCHER_CASH_ADVANCE");
        break;

      case MovementType.TransferCreditIn:
        Desc = Resource.String("STR_FRM_LAST_MOVEMENTS_095");
        break;

      case MovementType.TransferCreditOut:
        Desc = Resource.String("STR_FRM_LAST_MOVEMENTS_094");
        break;

      //case MovementType.ChipsSaleWithCashIn:
      //  Desc = Resource.String("STR_FRM_CHIPS_SALES_WITH_CACH_IN");
      //  break;

      //case MovementType.ChipsPurchaseWithCashOut:
      //  Desc = Resource.String("STR_FRM_CHIPS_PURCHASE_WITH_CASH_OUT");
      //  break;

      case MovementType.ExternalSystemPointsSubstract:
        Desc = Resource.String("STR_FRM_LAST_MOVEMENTS_108");
        break;

      case MovementType.ChipsPurchaseTotal:
        Desc = Resource.String("STR_MOVEMENT_TYPE_ACCOUNT_CHIPS_PURCHASE_TOTAL");
        break;

      case MovementType.ChipsSaleTotal:
        Desc = Resource.String("STR_MOVEMENT_TYPE_ACCOUNT_CHIPS_SALES_TOTAL");
        break;

      case MovementType.ChipsSaleRemainingAmount:
        Desc = Resource.String("STR_MOVEMENT_TYPE_CHIP_PURCHASE_REMAINING_AMOUNT");
        break;

      case MovementType.ChipsSaleConsumedRemainingAmount:
        Desc = Resource.String("STR_MOVEMENT_TYPE_CHIP_PURCHASE_CONSUMED_REMAINING_AMOUNT");
        break;

      case MovementType.ChipsSaleDevolutionForTito:
        Desc = Resource.String("STR_MOVEMENT_TYPE_ACCOUNT_CHIPS_SALES_DEVOLUTION_FOR_TITO");
        break;

      case MovementType.TITO_TicketCashierPaidCashable:
        Desc = Resource.String("STR_UC_TITO_TICKET_CASHIER_PAID_CASHABLE");
        break;

      case MovementType.TITO_TicketCashierPaidHandpay:
        Desc = Resource.String("STR_UC_TITO_TICKET_CASHIER_PAID_HANDPAY");
        break;

      case MovementType.TITO_TicketCashierPaidJackpot:
        Desc = Resource.String("STR_UC_TITO_TICKET_CASHIER_PAID_JACKPOT");
        break;

      case MovementType.TITO_TicketCashierPaidPromoRedeemable:
        Desc = Resource.String("STR_UC_TITO_TICKET_CASHIER_PAID_PROMO_REDEEMABLE");
        break;

      case MovementType.CreditLineGetMarker:
        Desc = Resource.String("STR_FRM_MOV_CREDITLINE_GETMARKER");
        break;

      case MovementType.CreditLinePayback:
        Desc = Resource.String("STR_FRM_MOV_CREDITLINE_PAYBACK");
        break;

      case MovementType.CreditLineGetMarkerCardReplacement:
        Desc = Resource.String("STR_FRM_MOV_CREDITLINE_GETMARKER_CARD_REPLACEMENT");
        break;

      default:
        {
          Desc = Resource.String("STR_FRM_LAST_MOVEMENTS_DEFAULT_MOVEMENT", ((int)Type).ToString());

          if (Type > MovementType.MULTIPLE_BUCKETS_EndSession && Type <= MovementType.MULTIPLE_BUCKETS_Manual_Set_Last) // (1100 - 1599)
          {
            String _bucket_name;
            Int32 _movement_base = Convert.ToInt32(Type) / 100;
            _movement_base = _movement_base * 100;
            _buckets_forms = new BucketsForm();

            if (_buckets_forms.GetBucketName((MovementType)Type, out _bucket_name))
            {
              // There are no ranks, this switch is implemented to cover multiple buckets ranges.
              // First, we divide _movements by the range and then check its range starting with 11
              switch ((MovementType)_movement_base)
              {
                case MovementType.MULTIPLE_BUCKETS_EndSession: //MULTIPLE_BUCKETS_EndSession Range (1100 to 1199)
                  Desc = Resource.String("STR_BUCKET_EARNED_MOVEMENT", _bucket_name);
                  break;
                case MovementType.MULTIPLE_BUCKETS_Expired: //MULTIPLE_BUCKETS_Expired Range (1200 to 1299)
                  Desc = Resource.String("STR_BUCKET_EXPIRED_MOVEMENT", _bucket_name);
                  break;
                case MovementType.MULTIPLE_BUCKETS_Manual_Add: //MULTIPLE_BUCKETS_Manual_Add Range (1300 to 1399)
                  Desc = Resource.String("STR_BUCKET_ADD_MOVEMENT", _bucket_name);
                  break;
                case MovementType.MULTIPLE_BUCKETS_Manual_Sub: //MULTIPLE_BUCKETS_MANUAL_Sub Range (1400 to 1499)     
                  Desc = Resource.String("STR_BUCKET_SUB_MOVEMENT", _bucket_name);
                  break;
                case MovementType.MULTIPLE_BUCKETS_Manual_Set: //MULTIPLE_BUCKETS_Manual_Set Range (1500 to 1599)
                  Desc = Resource.String("STR_BUCKET_SET_MOVEMENT", _bucket_name);
                  break;
              }
            }
          }

          break;
        }
    }
  }
}





