//------------------------------------------------------------------------------
// Copyright � 2013 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WKT_ProcessGetPlayerDraws.cs
// 
//   DESCRIPTION:  WKT_ProcessGetPlayerDraws class
// 
//        AUTHOR: Javier Molina
// 
// CREATION DATE: 29-APR-2013
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 29-APR-2013 JMM    First version.
// 17-JUL-2015 YNM    Fixed Bug WIG-2330: Promobox collection error 
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using WSI.Common;
using WSI.WCP;

public static partial class WCP_WKT
{

  public class WKT_Draw
  {
    public Int64      DrawID;
    public String     Name;
    public Int64      PendingNumbers;
    public Int64      RealPendingNumbers;
  }

  //------------------------------------------------------------------------------
  // PURPOSE : Manages the bussiness logic of WCP_WKT_MsgGetPlayerDraws message
  //
  //  PARAMS :
  //      - INPUT :
  //          - WktRequest : Input message
  //
  //      - OUTPUT :
  //          - WktResponse : Message response to return
  //
  //      - RETURNS :

  private static void WKT_ProcessGetPlayerDraws(WCP_Message WktRequest, 
                                                WCP_Message WktResponse)
  {
    WCP_WKT_MsgGetPlayerDraws       _request;
    WCP_WKT_MsgGetPlayerDrawsReply  _response;
    WCP_ResponseCodes               _response_code;
    Int64                           _account_id;

    _request = (WCP_WKT_MsgGetPlayerDraws)WktRequest.MsgContent;
    _response = (WCP_WKT_MsgGetPlayerDrawsReply)WktResponse.MsgContent;

    try
    {
      // Check the card's account and provided PIN
      if ( ! WKT_PlayerLogin (_request.m_player.Trackdata, _request.m_player.Pin, out _account_id, out _response_code) )
      {
        WktResponse.MsgHeader.ResponseCode = _response_code;

        return;
      }

      // Obtains the draws with pending tickets to be printed for the provided account
      WktResponse.MsgHeader.ResponseCode = GetPlayerDraws (_account_id, 
                                                           WktRequest.MsgHeader.TerminalId, 
                                                           _response);

      WktResponse.MsgContent = _response;
    }
    catch (Exception _ex)
    {
      Log.Exception(_ex);

      WktResponse.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_WKT_ERROR;
    }
    
  } // WKT_ProcessGetPlayerDraws

  //------------------------------------------------------------------------------
  // PURPOSE : Obtains the draws with pending tickets to be printed for the provided account
  //
  //  PARAMS :
  //      - INPUT :
  //          - AccountId
  //          - TerminalId : Used to obtain the cashier session 
  //
  //      - OUTPUT :
  //          - WktResponse : Message response to return
  //
  //      - RETURNS :

  private static WCP_ResponseCodes GetPlayerDraws (Int64 AccountId, 
                                                   String TerminalId, 
                                                   WCP_WKT_MsgGetPlayerDrawsReply MsgResponse)
  {
    CardData _card_data;
    DrawNumberList _draw_number_list;
    List<DrawTicket> _tickets;
    long _operation_id;
    String _terminal_name;
    TerminalStatus _status;    
    Int32 _terminal_id;
    String _provider_id;
    String _server_external_id;
    WKT_Draw _draw;
    CashierSessionInfo _cashier_session_info;
    StringBuilder _sb;
    Boolean _awardable_on_promobox;
    String _name_on_promobox;
    Currency _sas_account_denom;

    try
    {
      // Steps 
      //      - Get Card Data
      //      - Get Cashier session
      //      - Get the available draws for that card 
      //      - Select only those draws that have pending tickets for that card 

      //      - Get Card Data
      _card_data = new CardData ();
      CardData.DB_CardGetAllData(AccountId, _card_data);

      using (DB_TRX _db_trx = new DB_TRX())
      {
        //    - Get Cashier session

        // Get terminal name
        if ( ! Misc.GetTerminalInfo (TerminalId, 
                                     1, 
                                     out _terminal_id, 
                                     out _provider_id, 
                                     out _terminal_name,
                                     out _status, 
                                     out _server_external_id, 
                                     out _sas_account_denom,
                                     _db_trx.SqlTransaction))
        {
          return WCP_ResponseCodes.WCP_RC_WKT_ERROR;
        }

        // Use terminal name to obtain the cashier session id
        _cashier_session_info = Cashier.GetSystemCashierSessionInfo(Misc.GetUserTypePromobox(),
                                                                    _db_trx.SqlTransaction,
                                                                    _terminal_name);                                                      

        //    - Get the available draws for that card 
        if ( ! Draws.CommonGenerateTickets (_card_data,
                                            true,       // OnlyCheck
                                            null,
                                            _cashier_session_info.CashierSessionId, 
                                            _db_trx.SqlTransaction,
                                            out _draw_number_list, 
                                            out _tickets, 
                                            out _operation_id))
        {
          return WCP_ResponseCodes.WCP_RC_WKT_ERROR;
        }        

        //    - Select only those draws that have pending tickets for that card 
        foreach ( DrawNumbers _read_draw in _draw_number_list )
        {
          if ( _read_draw.PendingNumbers > 0 )
          {
            _sb = new StringBuilder();

            _sb.AppendLine("SELECT   DR_AWARD_ON_PROMOBOX                                                                                           ");
            _sb.AppendLine("       , CASE WHEN LEN(DR_TEXT_ON_PROMOBOX) > 0 THEN ISNULL (DR_TEXT_ON_PROMOBOX, DR_NAME) ELSE DR_NAME END AS DR_NAME  ");
            _sb.AppendLine("  FROM   DRAWS                                                                                                          ");
            _sb.AppendLine(" WHERE   DR_ID = @pDrawID                                                                                               ");

            using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
            {
              _sql_cmd.Parameters.Add("@pDrawID", SqlDbType.BigInt).Value = _read_draw.Draw.DrawId;

              using (SqlDataReader _sql_reader = _db_trx.ExecuteReader(_sql_cmd))
              {
                if (!_sql_reader.Read())
                {
                  Log.Error("WCP_ResponseCodes.GetPlayerDraws: An error occurred reading DRAWS.");

                  return WCP_ResponseCodes.WCP_RC_WKT_ERROR;
                }

                _awardable_on_promobox = _sql_reader.GetBoolean(_sql_reader.GetOrdinal("DR_AWARD_ON_PROMOBOX"));
                if (!_awardable_on_promobox)
                {
                  continue;
                }

                _name_on_promobox = _sql_reader.GetString(_sql_reader.GetOrdinal("DR_NAME"));
              }
            }

            _draw = new WKT_Draw();
            _draw.DrawID             = _read_draw.Draw.DrawId;
            _draw.Name               = _name_on_promobox;
            _draw.PendingNumbers     = _read_draw.PendingNumbers;
            _draw.RealPendingNumbers = _read_draw.RealPendingNumbers;

            MsgResponse.Draws.Add(_draw);
          }
        }

      } // Using DB_TRX

      return WCP_ResponseCodes.WCP_RC_WKT_OK;

    }
    catch (Exception _ex)
    {
      Log.Exception(_ex);
      return WCP_ResponseCodes.WCP_RC_WKT_ERROR;
    }
  }
}