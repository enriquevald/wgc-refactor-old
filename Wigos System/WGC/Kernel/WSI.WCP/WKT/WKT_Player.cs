//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WKT_PLayer.cs
// 
//   DESCRIPTION: WCP_WKT class
// 
//        AUTHOR: Xavier Iglesia
// 
// CREATION DATE: 15-JUN-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 15-JUN-2012 XIT    First version.
// 03-JUL-2012 XIT    Added Interfaces for WCP Communication
// 10-JUL-2012 MPO    Added index in query ACCOUNT_PROMOTIONS
// 23-JUL-2012 ANG    Added PlayerField class and interfaces.
// 02-AGO-2012 ANG    Add StringToXmlValue() call to protect message from special chars
// 23-NOV-2012 JMM    Level id & next level info added.
// 08-JAN-2013 RCI    Fixed Bug #476
// 08-APR-2013 LEM    Validated player data format on WKT_UpdatePlayerFields 
// 02-MAY-2013 LEM    Fixed Bug #729: Added cashier and account movements for personalization
// 14-MAY-2013 JMM    Get next level info calling AccountNextLevel
// 24-MAY-2013 LEM    Modified WKT_PlayerFieldsTable.WKT_UpdatePlayerFields to create AccountMovementsTable and CashierMovementsTable with TerminalName.
// 14-AUG-2013 JMM    Fixed Bug WIG-113: PromoBOX fails on note recharges when anti-moneylaundry activated
// 08-JUL-2014 JMM    Next Level query optimization
// 30-SEP-2014 LEM    Fixed Bug WIG-1246: Allowed cash in without cage activated
// 15-ENE-2014 JML    Product Backlog Item 151:AccountPointsCache --> Task 156: Promobox & DisplayLCD read
// 17-JUL-2015 YNM    Fixed Bug WIG-2330: Promobox collection error 
// 22-OCT-2015 SDS    Add promotion by played - Backlog Item 3856
// 23-NOV-2015 FOS    Bug 6771:Payment tickets & handpays
// 04-FEB-2016 ETP    Fixed bug 8976 added new Trx_GetTerminalInfoByExternalId 
// 04-FEB-2016 ETP    Fixed bug 10726: Transfer bucket credit to promobox.
// 07-JUN-2016 JMM    Fixed bug 13990: Missing recharges while gaming day is enabled
// 07-JUN-2016 JMM    Fixed bug 15455: Incorrect Promobox cashier sessions for LCD when pressing promotions menu.
// 05-SEP-2016 ETP    Fixed bug 17222: Removed buckets in promobox balance.
// 30-SEP-2016-SMN    Fixed bug 18019:PromoBOX: no se muestran los puntos negativos de la cuenta de un cliente
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using WSI.Common;
using System.Xml;
using WSI.WCP;
using System.Data.SqlClient;
using System.Data;
using System.Data.SqlTypes;
using System.IO;
using System.Web;

public static partial class WCP_WKT
{

  public interface IExtendedPlayer
  {
    WCP_WKT.WKT_Player Player { get; set;}
  }

  public interface IExtendedPlayerInfo
  {
    WKT_PlayerInfo PlayerInfo { get; set;}
  }


  public class WKT_Player : IXml
  {
    #region Fields
    private String m_track_data;
    private String m_pin;
    #endregion

    #region Properties
    public String Trackdata
    {
      get { return m_track_data; }
      set { m_track_data = value; }
    }

    public String Pin
    {
      get { return m_pin; }
      set { m_pin = value; }
    }

    #endregion

    //------------------------------------------------------------------------------
    // PURPOSE: Login Current User by Public Track Data and PIN
    // 
    //  PARAMS:
    //      - INPUT:
    //        - TrackData: Public Track Data
    //        - PIN:       Pin
    //
    //      - OUTPUT:
    //        - AccountId: Account Id
    //        - ErrorCode: Result of verify login
    // RETURNS:
    //    - true:   Executed Correctly and User Card and Pin are correct
    //    - false:  User Card or Pin are correct
    // 
    //   NOTES:
    //String ExternalTrackData, String PIN, 
    public Boolean Login(out Int64 AccountId, out WCP_ResponseCodes ErrorCode)
    {
      String _sql_txt;
      String _internal_track_data;
      Int32 _dummy;

      ErrorCode = WCP_ResponseCodes.WCP_RC_WKT_ERROR;
      AccountId = 0;

      try
      {
        // is card valid --> Not found WCP_RC_WKT_CARD_NOT_VALID
        if (!CardNumber.TrackDataToInternal(this.Trackdata, out _internal_track_data, out _dummy))
        {
          ErrorCode = WCP_ResponseCodes.WCP_RC_WKT_CARD_NOT_VALID;

          return false;
        }

        using (DB_TRX _db_trx = new DB_TRX())
        {
          _sql_txt = string.Empty;
          _sql_txt += "SELECT   AC_ACCOUNT_ID       ";
          _sql_txt += "  FROM   ACCOUNTS            ";
          _sql_txt += " WHERE   AC_TRACK_DATA = @pIntTrackData";

          using (SqlCommand _sql_cmd = new SqlCommand(_sql_txt))
          {
            Object _obj;

            _sql_cmd.Parameters.Add("@pIntTrackData", SqlDbType.NVarChar, 50).Value = _internal_track_data;
            _obj = _db_trx.ExecuteScalar(_sql_cmd);

            if (_obj == null)
            {
              ErrorCode = WCP_ResponseCodes.WCP_RC_WKT_CARD_NOT_VALID;

              return false;
            }

            AccountId = (Int64)_obj;
          }
        }

        switch (Accounts.DB_CheckAccountPIN(AccountId, this.Pin))
        {
          case PinCheckStatus.OK:
            ErrorCode = WCP_ResponseCodes.WCP_RC_WKT_OK;
            return true;

          case PinCheckStatus.WRONG_PIN:
            ErrorCode = WCP_ResponseCodes.WCP_RC_WKT_WRONG_PIN;
            break;

          case PinCheckStatus.ACCOUNT_BLOCKED:
            ErrorCode = WCP_ResponseCodes.WCP_RC_WKT_ACCOUNT_BLOCKED;
            break;

          case PinCheckStatus.ACCOUNT_NO_PIN:
            ErrorCode = WCP_ResponseCodes.WCP_RC_WKT_ACCOUNT_NO_PIN;
            break;

          case PinCheckStatus.ERROR:
          default:
            ErrorCode = WCP_ResponseCodes.WCP_RC_WKT_ERROR;
            break;
        } // switch (_pin_check_status)
      }
      catch
      {
        ErrorCode = WCP_ResponseCodes.WCP_RC_WKT_ERROR;
      }

      return false;
    } //WKT_PlayerLogin


    #region IXml Members

    public String ToXml()
    {
      StringBuilder _sb;
      _sb = new StringBuilder();
      _sb.Append("<PlayerData>");
      _sb.Append("<Pin>" + XML.StringToXmlValue(this.Pin) + "</Pin>");
      _sb.Append("<TrackData>" + XML.StringToXmlValue(this.Trackdata) + "</TrackData>");
      _sb.Append("</PlayerData>");

      return _sb.ToString();
    }//ToXml

    public void LoadXml(XmlReader Xml)
    {
      this.Pin = XML.ReadTagValue(Xml, "Pin");
      this.Trackdata = XML.ReadTagValue(Xml, "TrackData");
    }//LoadXml
    #endregion
  }

  public class WKT_PlayerInfo : IXml
  {
    #region Fields
    private String m_full_name;
    private DrawGenderFilter m_gender;
    private DateTime m_birthday;
    private Currency m_money_balance;
    private Points m_points_balance;
    private Currency m_money2_balance;
    private Points m_points2_balance;
    private WKT_PlayerInfo_Level m_level;
    private AccountNextLevel m_next_level;
    private WKT_PlayerInfo_BalanceParts m_balance_parts;
    private Boolean m_in_session;
    private String m_in_session_terminal_name;
    private bool m_noteacceptor_enabled = true;
    private Currency m_current_spent;
    private Currency m_current_played;
    #endregion

    #region Properties
    public String FullName
    {
      get { return m_full_name; }
      set { m_full_name = value; }
    }

    public DrawGenderFilter Gender
    {
      get { return m_gender; }
      set { m_gender = value; }
    }

    public DateTime Birthday
    {
      get { return m_birthday; }
      set { m_birthday = value; }
    }

    public Currency MoneyBalance
    {
      get { return m_money_balance; }
      set { m_money_balance = value; }
    }

    public Points PointsBalance
    {
      get { return m_points_balance; }
      set { m_points_balance = value; }
    }

    public Currency Money2Balance
    {
      get { return m_money2_balance; }
      set { m_money2_balance = value; }
    }

    public Points Points2Balance
    {
      get { return m_points2_balance; }
      set { m_points2_balance = value; }
    }

    public WKT_PlayerInfo_Level Level
    {
      get { return m_level; }
      set { m_level = value; }
    }

    public AccountNextLevel NextLevel
    {
      get { return m_next_level; }
      set { m_next_level = value; }
    }

    public WKT_PlayerInfo_BalanceParts BalanceParts
    {
      get { return m_balance_parts; }
      set { m_balance_parts = value; }
    }

    public Boolean InSession
    {
      get { return m_in_session; }
      set { m_in_session = value; }
    }

    public String InSessionTerminalName
    {
      get { return m_in_session_terminal_name; }
      set { m_in_session_terminal_name = value; }
    }

    public bool NoteAcceptorEnabled
    {
      get { return m_noteacceptor_enabled; }
      set { m_noteacceptor_enabled = value; }
    }

    public Currency CurrentSpent
    {
      get { return m_current_spent; }
      set { m_current_spent = value; }
    }
    public Currency CurrentPlayed
    {
        get { return m_current_played; }
        set { m_current_played = value; }
    }
    #endregion

    public WKT_PlayerInfo()
    {
      this.BalanceParts = new WKT_PlayerInfo_BalanceParts();
      this.BalanceParts.ActivePromotions = new List<WKT_BalanceParts_Promotion>();
      this.FullName = string.Empty;
      this.Level = new WKT_PlayerInfo_Level();
      this.Level.LevelName = string.Empty;
      this.NextLevel = new AccountNextLevel();
      this.NextLevel.LevelName = string.Empty;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Read the account info for WKT_ProcessGetPlayerInfoFields
    // 
    //  PARAMS:
    //      - INPUT :
    //          - AccountId
    //
    //      - OUTPUT :
    //          - PlayerInfo
    //
    // RETURNS : 
    // 
    //   NOTES :

    public static bool Read(Int64 AccountId, String TerminalName, SqlTransaction SqlTrx, out WKT_PlayerInfo PlayerInfo)
    {
      StringBuilder _sql_txt;
      Int64 _play_session_id;
      String _aux_text;
      //DateTime _day_start_counting_points;
      //Int32 _days_counting_points;
      ENUM_ANTI_MONEY_LAUNDERING_LEVEL _anti_ml_level;
      Boolean _account_already_registered;
      Boolean _threshold_crossed;
      Currency _redeemable_amount_to_add;
      DataTable _dt_remaining_spent_per_provider;
      Currency _current_spent;
      Currency _current_played;
      Points _points_generated;
      Points _points_discretionaries;      

      PlayerInfo = null;

      try
      {
        _sql_txt = new StringBuilder();

        _sql_txt.Append("EXECUTE   GetAccountPointsCache ");
        _sql_txt.Append("          @pAccountId           ");
        
        _sql_txt.Append("  SELECT   ACP_PROMO_DATE                                                            ");
        _sql_txt.Append("         , ACP_PROMO_NAME                                                            ");
        _sql_txt.Append("         , ACP_INI_BALANCE                                                           ");
        _sql_txt.Append("         , ACP_BALANCE                                                               ");
        _sql_txt.Append("    FROM   ACCOUNT_PROMOTIONS WITH (INDEX(IX_acp_account_status))                    ");
        _sql_txt.Append("   WHERE   ACP_ACCOUNT_ID = @pAccountId                                              ");
        _sql_txt.Append("     AND   ACP_STATUS     = @pStatusActive                                           ");
        _sql_txt.Append("     AND   ACP_PROMO_TYPE <> @pExcludePromoNR2;                                      ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sql_txt.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
          _sql_cmd.Parameters.Add("@pStatusActive", SqlDbType.Int).Value = ACCOUNT_PROMO_STATUS.ACTIVE;
          _sql_cmd.Parameters.Add("@pExcludePromoNR2", SqlDbType.Int).Value = Promotion.PROMOTION_TYPE.AUTOMATIC_PER_RECHARGE_COVER_COUPON;

          using (SqlDataReader _sql_reader = _sql_cmd.ExecuteReader())
          {
            if (!_sql_reader.Read())
            {
              return false;
            }

            PlayerInfo = new WKT_PlayerInfo();

            // FullName must match WCP_BasicPlayerInfoParameters->FullName size (=80)
            _aux_text = _sql_reader.GetString(_sql_reader.GetOrdinal("AC_HOLDER_NAME"));
            PlayerInfo.FullName = (_aux_text.Length > 80) ? _aux_text.Remove(80)
                                                          : _aux_text;
            PlayerInfo.Gender = (DrawGenderFilter)_sql_reader.GetInt32(_sql_reader.GetOrdinal("AC_HOLDER_GENDER"));
            PlayerInfo.Birthday = _sql_reader.GetDateTime(_sql_reader.GetOrdinal("AC_HOLDER_BIRTH_DATE"));
            PlayerInfo.MoneyBalance = (Currency)_sql_reader.GetSqlMoney(_sql_reader.GetOrdinal("AC_BALANCE"));

            PlayerInfo.PointsBalance = (Points)_sql_reader.GetDecimal(_sql_reader.GetOrdinal("AC_POINTS"));
            PlayerInfo.Level = WKT_PlayerInfo_GetLevel(_sql_reader);

            // Next Level
            _points_generated = (Points)_sql_reader.GetSqlMoney(_sql_reader.GetOrdinal("AM_POINTS_GENERATED"));
            _points_discretionaries = (Points)_sql_reader.GetSqlMoney(_sql_reader.GetOrdinal("AM_POINTS_DISCRETIONARIES"));
            
            PlayerInfo.NextLevel.SetLevel(_points_generated, _points_discretionaries, PlayerInfo.Level.LevelId);

            PlayerInfo.Money2Balance = 0;
            PlayerInfo.Points2Balance = 0;

            _play_session_id = _sql_reader.GetInt64(_sql_reader.GetOrdinal("AC_CURRENT_PLAY_SESSION_ID"));
            PlayerInfo.InSession = (_play_session_id != 0);
            PlayerInfo.InSessionTerminalName = "";

            if (PlayerInfo.InSession)
            {
              PlayerInfo.InSessionTerminalName = _sql_reader.GetString(_sql_reader.GetOrdinal("AC_CURRENT_TERMINAL_NAME"));
            }
            PlayerInfo.BalanceParts = WKT_PlayerInfo_GetBalanceParts(_sql_reader);

            PlayerInfo.CurrentSpent = 0;
            PlayerInfo.CurrentPlayed = 0;
          }
         
          if ( Misc.IsGamingDayEnabled() )
          {
            DateTime _next_gaming_day;
            CashierSessionInfo _session_info;
             WSI.Common.Terminal.TerminalInfo _terminalInfo;

            if (!WSI.Common.Terminal.Trx_GetTerminalInfo(TerminalName, out _terminalInfo, SqlTrx))
            {
              Log.Error(String.Format("Wkt_player.Read: Could not get terminalInfo. Terminal Name = {0}, Account_id = {1}", TerminalName, AccountId));
              return false;
            }
            
            if (_terminalInfo.TerminalType == TerminalTypes.PROMOBOX)
            {
              _session_info = Cashier.GetSystemCashierSessionInfo(Misc.GetUserTypePromobox(), SqlTrx, TerminalName);

              _next_gaming_day = _session_info.GamingDay.AddDays(1);

              if ( WGDB.Now >= _next_gaming_day )
              {
                // The WKT should disable its note acceptor.
                PlayerInfo.NoteAcceptorEnabled = false;

                return true;
              }
            }
          }

          // AntiMoneyLaundery only available for Mexico.  Highest value bank value in Mexico is 1000$
          _redeemable_amount_to_add = GeneralParam.GetCurrency("NoteAcceptor", "HighestBankNoteValue", 1000);          

          if (!Accounts.CheckAntiMoneyLaunderingFilters(AccountId, ENUM_CREDITS_DIRECTION.Recharge, _redeemable_amount_to_add, SqlTrx,
                                            out _anti_ml_level, out _threshold_crossed, out _account_already_registered))
          {
            PlayerInfo.NoteAcceptorEnabled = false;

            return false;
          }

          PlayerInfo.NoteAcceptorEnabled = WSI.Common.Misc.IsNoteAcceptorEnabled();

          switch (_anti_ml_level)
          {
            case ENUM_ANTI_MONEY_LAUNDERING_LEVEL.None:              
              break;

            case ENUM_ANTI_MONEY_LAUNDERING_LEVEL.IdentificationWarning:
              if (!_account_already_registered && _threshold_crossed)
              {
                if (GeneralParam.GetBoolean("AntiMoneyLaundering", "Recharge.Identification.DontAllowMB"))
                {
                  PlayerInfo.NoteAcceptorEnabled = false;
                }
              }
              break;

            case ENUM_ANTI_MONEY_LAUNDERING_LEVEL.ReportWarning:
              if (_threshold_crossed)
              {
                if (GeneralParam.GetBoolean("AntiMoneyLaundering", "Recharge.Report.DontAllowMB"))
                {
                  PlayerInfo.NoteAcceptorEnabled = false;
                }
              }
              break;

            case ENUM_ANTI_MONEY_LAUNDERING_LEVEL.Identification:
            case ENUM_ANTI_MONEY_LAUNDERING_LEVEL.Report:
              if (!_account_already_registered)
              {
                PlayerInfo.NoteAcceptorEnabled = false;
              }
              break;

            default:
              PlayerInfo.NoteAcceptorEnabled = false;
              break;
          }

          _dt_remaining_spent_per_provider = Promotion.AccountRemainingDataPerProvider(AccountId, SqlTrx);
          Promotion.CalculateDataByProvider(_dt_remaining_spent_per_provider, null, out _current_spent, out _current_played );

          PlayerInfo.CurrentSpent = _current_spent;
          //SDS 22-10-2015
            PlayerInfo.CurrentPlayed = _current_played;

          return true;          
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        return false;
      }
    }//Read

    //------------------------------------------------------------------------------
    // PURPOSE: Get Balance Parts Info
    // 
    //  PARAMS:
    //      - INPUT: SqlDataReader Sql_reader
    //
    //      - OUTPUT: 
    //
    // RETURNS: WKT_PlayerInfo_BalanceParts
    // 
    //   NOTES:
    //
    private static WKT_PlayerInfo_BalanceParts WKT_PlayerInfo_GetBalanceParts(SqlDataReader SqlReader)
     {
      WKT_PlayerInfo_BalanceParts _balance_parts;
      MultiPromos.AccountBalance _balance;

      _balance_parts = new WKT_PlayerInfo_BalanceParts();
      _balance = new MultiPromos.AccountBalance(SqlReader.GetDecimal(SqlReader.GetOrdinal("AC_RE_BALANCE")),
                                                SqlReader.GetDecimal(SqlReader.GetOrdinal("AC_PROMO_RE_BALANCE")),
                                                SqlReader.GetDecimal(SqlReader.GetOrdinal("AC_PROMO_NR_BALANCE")));

      _balance_parts.RedeemableBalance = _balance.TotalRedeemable;
      _balance_parts.NonRedeemableBalance = _balance.TotalNotRedeemable;
      _balance_parts.ActivePromotions = new List<WKT_BalanceParts_Promotion>();
      SqlReader.NextResult();

      while (SqlReader.Read())
      {
        _balance_parts.ActivePromotions.Add(WKT_BalanceParts_GetPromotion(SqlReader));
      }

      return _balance_parts;
    } // WKT_PlayerInfo_GetBalanceParts

    //------------------------------------------------------------------------------
    // PURPOSE: Get Level Info
    // 
    //  PARAMS:
    //      - INPUT: SqlDataReader SqlReader
    //
    //      - OUTPUT: 
    //
    // RETURNS: WKT_PlayerInfo_Level
    // 
    //   NOTES:
    //
    private static WKT_PlayerInfo_Level WKT_PlayerInfo_GetLevel(SqlDataReader SqlReader)
    {
      WKT_PlayerInfo_Level _level;
      Int32 _aux_level_id;

      _level = new WKT_PlayerInfo_Level();

      _aux_level_id = SqlReader.GetInt32(SqlReader.GetOrdinal("AC_CURRENT_HOLDER_LEVEL"));
      _level.LevelId = Math.Max(0, Math.Min(_aux_level_id, 4));  // Level id range: 0..4      

      _level.LevelName = GeneralParam.GetString("PlayerTracking", "Level0" + _level.LevelId + ".Name");

      if (SqlReader.IsDBNull(SqlReader.GetOrdinal("AC_HOLDER_LEVEL_ENTERED")))
      {
        _level.EnterDate = DateTime.MinValue;
      }
      else
      {
        _level.EnterDate = SqlReader.GetDateTime(SqlReader.GetOrdinal("AC_HOLDER_LEVEL_ENTERED"));
      }

      if (SqlReader.IsDBNull(SqlReader.GetOrdinal("AC_HOLDER_LEVEL_EXPIRATION")))
      {
        _level.Expiration = null;
      }
      else
      {
        _level.Expiration = SqlReader.GetDateTime(SqlReader.GetOrdinal("AC_HOLDER_LEVEL_EXPIRATION"));
      }

      return _level;
    }//WKT_PlayerInfo_GetLevel

    //------------------------------------------------------------------------------
    // PURPOSE: Get Promotions
    // 
    //  PARAMS:
    //      - INPUT: SqlDataReader Sql_reader
    //
    //      - OUTPUT: 
    //
    // RETURNS: WKT_BalanceParts_Promotion
    // 
    //   NOTES:
    //
    private static WKT_BalanceParts_Promotion WKT_BalanceParts_GetPromotion(SqlDataReader SqlReader)
    {
      WKT_BalanceParts_Promotion _promotion;

      _promotion = new WKT_BalanceParts_Promotion();
      _promotion.AwardedBalance = (Currency)SqlReader.GetSqlMoney(2);
      _promotion.CurrentBalance = (Currency)SqlReader.GetSqlMoney(3);
      _promotion.PromoDate = SqlReader.GetDateTime(0);
      _promotion.PromoName = SqlReader.GetString(1);

      return _promotion;
    }//WKT_BalanceParts_GetPromotion

    #region IXml Members

    public String ToXml()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.Append("<PlayerInfo>");
      _sb.Append("<FullName>" + XML.StringToXmlValue(this.FullName) + "</FullName>");
      _sb.Append("<Gender>" + this.Gender.ToString() + "</Gender>");
      _sb.Append("<Birthday>" + XML.XmlDateTimeString(this.Birthday) + "</Birthday>");
      _sb.Append("<MoneyBalance>" + ((Int64)(this.MoneyBalance * 100)).ToString() + "</MoneyBalance>");
      _sb.Append("<Money2Balance>" + ((Int64)(this.Money2Balance * 100)).ToString() + "</Money2Balance>");
      _sb.Append("<PointsBalance>" + ((Int64)(this.PointsBalance * 100)).ToString() + "</PointsBalance>");
      _sb.Append("<Points2Balance>" + ((Int64)(this.Points2Balance * 100)).ToString() + "</Points2Balance>");
      _sb.Append("<InSession>" + XML.StringToXmlValue(this.InSession.ToString()) + "</InSession>");
      _sb.Append("<InSessionTerminalName>" + XML.StringToXmlValue(this.InSessionTerminalName) + "</InSessionTerminalName>");
      _sb.Append("<NoteAcceptorEnabled>" + XML.StringToXmlValue(this.NoteAcceptorEnabled.ToString()) + "</NoteAcceptorEnabled>");
      _sb.Append("<CurrentSpent>" + ((Int64)(this.CurrentSpent * 100)).ToString() + "</CurrentSpent>");      
      _sb.Append("<Level>");
      _sb.Append("<LevelId>" + XML.StringToXmlValue(this.Level.LevelId.ToString()) + "</LevelId>");
      _sb.Append("<LevelName>" + XML.StringToXmlValue(this.Level.LevelName) + "</LevelName>");
      _sb.Append("<PointsToEnter>" + ((Int64)(this.Level.PointsToEnter * 100)).ToString() + "</PointsToEnter>");
      _sb.Append("<EnterDate>" + XML.XmlDateTimeString(this.Level.EnterDate) + "</EnterDate>");
      if (this.Level.Expiration != null)
      {
        _sb.Append("<Expiration>" + XML.XmlDateTimeString((DateTime)this.Level.Expiration) + "</Expiration>");
      }
      else
      {
        _sb.Append("<Expiration>" + "</Expiration>");
      }

      _sb.Append("<NextLevelId>" + XML.StringToXmlValue(this.NextLevel.LevelId.ToString()) + "</NextLevelId>");
      _sb.Append("<NextLevelName>" + XML.StringToXmlValue(this.NextLevel.LevelName) + "</NextLevelName>");
      _sb.Append("<NextLevelPointsToEnter>" + ((Int64)(this.NextLevel.PointsToReach * 100)).ToString() + "</NextLevelPointsToEnter>");
      _sb.Append("<NextLevelPointsGenerated>" + ((Int64)(this.NextLevel.PointsGenerated * 100)).ToString() + "</NextLevelPointsGenerated>");
      _sb.Append("<NextLevelPointsDisc>" + ((Int64)(this.NextLevel.PointsDiscretionaries * 100)).ToString() + "</NextLevelPointsDisc>");

      _sb.Append("</Level>");

      _sb.Append("<BalanceParts>");
      _sb.Append("<RedeemableBalance>" + ((Int64)(this.BalanceParts.RedeemableBalance * 100)).ToString() + "</RedeemableBalance>");
      _sb.Append("<NonRedeemableBalance>" + ((Int64)(this.BalanceParts.NonRedeemableBalance * 100)).ToString() + "</NonRedeemableBalance>");

      _sb.Append("<ActivePromotions>");
      foreach (WKT_BalanceParts_Promotion _promotion in this.BalanceParts.ActivePromotions)
      {
        _sb.Append("<Promotion>");
        _sb.Append("<PromoName>" + XML.StringToXmlValue(_promotion.PromoName) + "</PromoName>");
        _sb.Append("<PromoDate>" + XML.XmlDateTimeString(_promotion.PromoDate) + "</PromoDate>");
        _sb.Append("<AwardedBalance>" + ((Int64)(_promotion.AwardedBalance * 100)).ToString() + "</AwardedBalance>");
        _sb.Append("<CurrentBalance>" + ((Int64)(_promotion.CurrentBalance * 100)).ToString() + "</CurrentBalance>");
        _sb.Append("</Promotion>");
      }
      _sb.Append("</ActivePromotions>");
      _sb.Append("</BalanceParts>");
      _sb.Append("</PlayerInfo>");

      return _sb.ToString();

    }//ToXml

    public void LoadXml(XmlReader Xml)
    {
      WKT_BalanceParts_Promotion _promotion;
      String _date_value;

      this.BalanceParts = new WKT_PlayerInfo_BalanceParts();
      this.Level = new WKT_PlayerInfo_Level();
      BalanceParts.ActivePromotions = new List<WKT_BalanceParts_Promotion>();

      this.FullName = XML.ReadTagValue(Xml, "FullName");
      this.Gender = (DrawGenderFilter)Enum.Parse(typeof(DrawGenderFilter), XML.ReadTagValue(Xml, "Gender"));
      this.Birthday = XML.DateTimeFromXmlDateTimeString(XML.ReadTagValue(Xml, "Birthday"));
      this.MoneyBalance = SqlMoney.Parse(XML.ReadTagValue(Xml, "MoneyBalance"));
      this.Money2Balance = SqlMoney.Parse(XML.ReadTagValue(Xml, "Money2Balance"));
      this.PointsBalance = SqlMoney.Parse(XML.ReadTagValue(Xml, "PointsBalance"));
      this.Points2Balance = SqlMoney.Parse(XML.ReadTagValue(Xml, "Points2Balance"));
      this.InSession = Boolean.Parse(XML.ReadTagValue(Xml, "InSession"));
      this.InSessionTerminalName = XML.ReadTagValue(Xml, "InSessionTerminalName");

      // 13-AUG-2013 JMM: Note acceptor enabled flag.  True as default value.
      Xml.Read();

      this.NoteAcceptorEnabled = true;

      if (Xml.Name == "NoteAcceptorEnabled")
      {
        this.NoteAcceptorEnabled = Boolean.Parse(XML.ReadTagValue(Xml, "NoteAcceptorEnabled"));        
      }

      this.CurrentSpent = SqlMoney.Parse(XML.ReadTagValue(Xml, "CurrentSpent"));      

      this.Level.LevelId = Int32.Parse(XML.ReadTagValue(Xml, "LevelId"));
      this.Level.LevelName = XML.ReadTagValue(Xml, "LevelName");
      this.Level.EnterDate = XML.DateTimeFromXmlDateTimeString (XML.ReadTagValue(Xml, "EnterDate"));

      _date_value = XML.ReadTagValue(Xml, "Expiration");
      if (_date_value != string.Empty)
      {
        this.Level.Expiration = DateTime.Parse(_date_value);
      }
      else
      {
        this.Level.Expiration = null;
      }

      this.NextLevel.LevelId = Int32.Parse(XML.ReadTagValue(Xml, "NextLevelId"));
      this.NextLevel.LevelName = XML.ReadTagValue(Xml, "NextLevelName");
      this.NextLevel.PointsToReach = SqlMoney.Parse(XML.ReadTagValue(Xml, "NextLevelPointsToEnter"));

      this.NextLevel.PointsGenerated = SqlMoney.Parse(XML.ReadTagValue(Xml, "NextLevelPointsGenerated"));
      this.NextLevel.PointsDiscretionaries = SqlMoney.Parse(XML.ReadTagValue(Xml, "NextLevelPointsDisc"));

      this.BalanceParts.RedeemableBalance = SqlMoney.Parse(XML.ReadTagValue(Xml, "RedeemableBalance"));
      this.BalanceParts.NonRedeemableBalance = SqlMoney.Parse(XML.ReadTagValue(Xml, "NonRedeemableBalance"));

      while ((!(Xml.Name == "ActivePromotions" && Xml.NodeType == XmlNodeType.EndElement)) && (Xml.Name != "PlayerInfo"))
      {
        Xml.Read();

        if ((Xml.Name == "Promotion") && (Xml.NodeType != XmlNodeType.EndElement))
        {
          string _name = XML.ReadTagValue(Xml, "PromoName");
          if (_name != string.Empty)
          {
            _promotion = new WKT_BalanceParts_Promotion();

            _promotion.PromoName = _name;
            _promotion.PromoDate = XML.DateTimeFromXmlDateTimeString (XML.ReadTagValue(Xml, "PromoDate"));
            _promotion.AwardedBalance = SqlMoney.Parse(XML.ReadTagValue(Xml, "AwardedBalance"));
            _promotion.CurrentBalance = SqlMoney.Parse(XML.ReadTagValue(Xml, "CurrentBalance"));
            this.BalanceParts.ActivePromotions.Add(_promotion);
          }
        }
      }
    }

    #endregion

  }

  public class WKT_PlayerInfo_Level
  {
    public Int32 LevelId;
    public String LevelName;
    public DateTime EnterDate;
    public DateTime? Expiration;
    public Currency PointsToEnter;
  }

  public class WKT_PlayerInfo_BalanceParts
  {
    public Currency RedeemableBalance;
    public Currency NonRedeemableBalance;
    public List<WKT_BalanceParts_Promotion> ActivePromotions;
  }

  public class WKT_BalanceParts_Promotion
  {
    public DateTime PromoDate;
    public String PromoName;
    public Currency AwardedBalance;
    public Currency CurrentBalance;
  }

  public interface WKT_IGetPlayerField
  {
    Int32 FieldsNumber
    { get; }

    WKT_IPlayerField GetField(Int32 Idx);

    WKT_PlayerFieldsTable PlayerFields
    { get; set; }
  }

  public interface WKT_IPlayerField
  {
    PLAYER_INFO_FIELD_TYPE FieldId
    { get;set; }

    String Value
    { get;set; }

    Int32 Updateable
    { get;set; }

    WKT_FIELD_TYPE Type
    { get;set; }

    Int32 MinLength
    { get;set; }

    Int32 MaxLength
    { get;set; }

    WKT_UPDATE_FIELD_STATUS ErrorCode
    { get;set; }

    string ErrorDescription
    { get;set; }

  }

  public class WKT_PlayerFieldsTable : DataTable, IXml, WKT_IGetPlayerField
  {

    #region "Constructor"

    public WKT_PlayerFieldsTable()
    {
      InitDataTable();
    }

    #endregion

    #region "Overrides"

    public WKT_PlayerFieldsRow this[int Idx]
    {
      get { return (WKT_PlayerFieldsRow)Rows[Idx]; }
    }

    protected override Type GetRowType()
    {
      return typeof(WKT_PlayerFieldsRow);
    }

    protected override DataRow NewRowFromBuilder(DataRowBuilder Builder)
    {
      return new WKT_PlayerFieldsRow(Builder);
    }

    public void Remove(WKT_PlayerFieldsRow Row)
    {
      Rows.Remove(Row);
    }

    public new WKT_PlayerFieldsRow NewRow()
    {
      WKT_PlayerFieldsRow _row;
      _row = (WKT_PlayerFieldsRow)base.NewRow();

      return _row;
    }
    #endregion

    #region Properties

    public Int32 FieldsNumber
    {
      get { return this.Rows.Count; }
    }
    #endregion

    #region "Private Members"


    //------------------------------------------------------------------------------
    // PURPOSE:  Initialize DataTable, Create DataColumns
    //             FieldId*    : PLAYER_INFO_FIELD_TYPE 
    //             Value       : Field Value
    //             Error Code  : Error code at update ( if not right)
    //             Description : Error code description ( if not right )
    // 
    //  PARAMS:
    //      - INPUT: 
    //
    //      - OUTPUT:
    //
    // RETURNS: A String array with Primary Key Column Name
    //
    //   NOTES:
    //
    // "FieldId","Value","ErrorCode","Description" columns, And IF_FIELD_ID as PrimaryKey
    private void InitDataTable()
    {
      this.Columns.Add(new DataColumn("FieldID", typeof(PLAYER_INFO_FIELD_TYPE)));

      this.Columns.Add(new DataColumn("Value", typeof(String)));

      this.Columns.Add(new DataColumn("Updateable", typeof(Int32)));

      this.Columns.Add(new DataColumn("Type", typeof(WKT_FIELD_TYPE)));

      this.Columns.Add(new DataColumn("MinLength", typeof(Int32)));

      this.Columns.Add(new DataColumn("MaxLength", typeof(Int32)));

      this.Columns.Add(new DataColumn("ErrorCode", typeof(WKT_UPDATE_FIELD_STATUS)));

      this.Columns.Add(new DataColumn("ErrorDescription", typeof(String)));

      this.Columns.Add(new DataColumn("DiplayOrder", typeof(Int32)));

      this.PrimaryKey = new DataColumn[] { this.Columns["FieldId"] };

      this.TableName = "Field";
    }


    #endregion

    #region "Events"

    public delegate void PlayerFieldsRowChangedDlgt(WKT_PlayerFieldsTable sender,
                     PlayerFieldsRowChangedEventArgs args);
    //------------------------------------------------------------------------------
    // PURPOSE: On Row change event
    // 
    //  PARAMS:
    //      - INPUT: 
    //
    //      - OUTPUT: 
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    public event PlayerFieldsRowChangedDlgt PlayerFieldsRowChanged;
    //------------------------------------------------------------------------------
    // PURPOSE: On Row changed function
    // 
    //  PARAMS:
    //      - INPUT: 
    //
    //      - OUTPUT: 
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    protected override void OnRowChanged(DataRowChangeEventArgs e)
    {
      base.OnRowChanged(e);
      PlayerFieldsRowChangedEventArgs args =
        new PlayerFieldsRowChangedEventArgs(e.Action, (WKT_PlayerFieldsRow)e.Row);
      OnPlayerFieldsRowChanged(args);
    }
    //------------------------------------------------------------------------------
    // PURPOSE: On Row change
    // 
    //  PARAMS:
    //      - INPUT: 
    //
    //      - OUTPUT: 
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    protected virtual void OnPlayerFieldsRowChanged(PlayerFieldsRowChangedEventArgs args)
    {
      if (PlayerFieldsRowChanged != null)
      {
        PlayerFieldsRowChanged(this, args);
      }
    }

    #endregion

    #region "Arguments class"
    public class PlayerFieldsRowChangedEventArgs
    {
      protected DataRowAction action;
      protected WKT_PlayerFieldsRow row;

      public DataRowAction Action
      {
        get { return action; }
      }

      public WKT_PlayerFieldsRow Row
      {
        get { return row; }
      }

      public PlayerFieldsRowChangedEventArgs(DataRowAction action, WKT_PlayerFieldsRow row)
      {
        this.action = action;
        this.row = row;
      }
    }
    #endregion

    #region IXml Members

    public string ToXml()
    {
      DataSet _ds;
      TextWriter _txt_wr;

      _ds = new DataSet("Fields");
      _txt_wr = new StringWriter();

      this.EncodeSpecialChars();

      _ds.Tables.Add(this);
      this.WriteXml(_txt_wr);

      return _txt_wr.ToString();
    }

    public void LoadXml(XmlReader XmlReader)
    {
      this.ReadXml(XmlReader);
      this.DecodeSpecialChars();
    }


    #endregion

    #region "Public static functions"

    //------------------------------------------------------------------------------
    // PURPOSE: Get field information from wkt_player_info_fields database table.
    //
    //  PARAMS:
    //      - INPUT:
    //      - OUT:
    //  RETURN : Data Table with field info. 
    //
    private static DataTable Get_wtk_player_info_fields(SqlTransaction Trx)
    {
      StringBuilder _sb;
      DataTable _field_definitions;
      Int32 _field_id;
      DataRow _field_row;

      _sb = new StringBuilder();
      _field_definitions = new DataTable();

      _sb.AppendLine("  SELECT   PIF_FIELD_ID                                                                          ");
      _sb.AppendLine("         , PIF_NAME                                                                              ");
      _sb.AppendLine("         , PIF_SHOWN                                                                             ");
      _sb.AppendLine("         , CASE WHEN PIF_EDITABLE = 1 AND PIF_USER_ALLOW_EDIT=1 THEN 1 ELSE 0 END PIF_CAN_UPDATE ");
      _sb.AppendLine("         , PIF_TYPE                                                                              ");
      _sb.AppendLine("         , PIF_MIN_LENGTH                                                                        ");
      _sb.AppendLine("         , PIF_MAX_LENGTH                                                                        ");
      _sb.AppendLine("         , PIF_ORDER                                                                             ");
      _sb.AppendLine("    FROM   WKT_PLAYER_INFO_FIELDS                                                                ");
      _sb.AppendLine("ORDER BY   PIF_ORDER,PIF_FIELD_ID ASC                                                           ");

      using (SqlCommand _sql_command = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
      {
        using (SqlDataReader _reader = _sql_command.ExecuteReader())
        {
          _field_definitions.Load(_reader);
        }
      }

      // Set FieldId as Primary Key to allow do Table.Find()
      _field_definitions.PrimaryKey = new DataColumn[] { _field_definitions.Columns["PIF_FIELD_ID"] };

      _field_definitions.Columns.Add(new DataColumn("PIF_TABLE_NAME", typeof(String)));
      _field_definitions.Columns.Add(new DataColumn("PIF_FIELD_NAME", typeof(String)));

      for (Int32 _idx_row = 0; _idx_row < _field_definitions.Rows.Count; _idx_row++)
      {
        _field_row = _field_definitions.Rows[_idx_row];

        // Ignore if field doesn't exist in enum PLAYER_INFO_FIELD_TYPE
        _field_id = (Int32)_field_row["PIF_FIELD_ID"];
        if (!Enum.IsDefined(typeof(PLAYER_INFO_FIELD_TYPE), _field_id))
        {
          Log.Error("Field '" + _field_row["PIF_NAME"] + "' will be ignored,Not defined in PLAYER_INFO_FIELD_TYPE");
          _field_row.Delete();
          continue;
        }

        if (!_field_row.IsNull("PIF_NAME"))
        {
          String[] _tmp_split;
          _tmp_split = ((String)_field_row["PIF_NAME"]).Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries);
          if (_tmp_split.Length == 2)
          {
            _field_row["PIF_TABLE_NAME"] = _tmp_split[0].ToUpper();
            _field_row["PIF_FIELD_NAME"] = _tmp_split[1].ToUpper();
          }
          else
          {
            Log.Error("ERROR : Get_wtk_player_info_fields " + _field_row["PIF_NAME"] + " invalid name!");
          }

        }
      }

      _field_definitions.AcceptChanges();

      return _field_definitions;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Get account information by UserId
    //
    //  PARAMS :
    //      - INPUT : AccountId, Trx
    //
    //      - OUTPUT : UserFields
    //
    //  RETURN : PlayerFieldsTable with account field values.

    public static Boolean WKT_ReadPlayerFields(Int64 AccountId, out WKT_PlayerFieldsTable UserFields, SqlTransaction Trx)
    {
      WKT_PlayerFieldsRow _user_fields_row;
      DataTable _field_definitions;
      StringBuilder _sb_fields;
      DataRow _field_definition;
      Int32 _num_fields;
      DateTime _date;
      String _string_limited;

      UserFields = new WKT_PlayerFieldsTable();

      try
      {
        _field_definitions = Get_wtk_player_info_fields(Trx);

        _sb_fields = new StringBuilder();
        _num_fields = 0;
        _sb_fields.Append("SELECT   ");

        foreach (DataRow _field in _field_definitions.Rows)
        {
          if (_field.IsNull("PIF_TABLE_NAME") || _field.IsNull("PIF_FIELD_NAME"))
          {
            continue;
          }
          if ((String)_field["PIF_TABLE_NAME"] != "ACCOUNTS")
          {
            continue;
          }

          if (_num_fields > 0)
          {
            _sb_fields.Append("       , ");
          }
          _sb_fields.AppendLine((String)_field["PIF_FIELD_NAME"]);
          _num_fields++;
        }
        _sb_fields.AppendLine(" FROM   ACCOUNTS");
        _sb_fields.AppendLine("WHERE   AC_ACCOUNT_ID = @pId");


        using (SqlCommand _sql_cmd = new SqlCommand(_sb_fields.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pId", SqlDbType.BigInt).Value = AccountId;

          using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
          {

            while (_reader.Read())
            {

              for (int _idx_field = 0; _idx_field < _reader.FieldCount; _idx_field++)
              {
                _user_fields_row = UserFields.NewRow();
                if (_idx_field >= _field_definitions.Rows.Count)
                {
                  continue;
                }

                _field_definition = _field_definitions.Rows[_idx_field];

                _user_fields_row.FieldId = (PLAYER_INFO_FIELD_TYPE)_field_definition["PIF_FIELD_ID"];

                if (!(Boolean)_field_definition["PIF_SHOWN"])
                {
                  continue;
                }

                _user_fields_row.Updateable = (Int32)_field_definition["PIF_CAN_UPDATE"];
                _user_fields_row.Type = (WKT_FIELD_TYPE)_field_definition["PIF_TYPE"];
                _user_fields_row.MinLength = (Int32)_field_definition["PIF_MIN_LENGTH"];
                _user_fields_row.MaxLength = (Int32)_field_definition["PIF_MAX_LENGTH"];
                //_user_fields_row.Order = (Int32)_field_definition["PIF_ORDER"];


                if (!_reader.IsDBNull(_idx_field))
                {
                  switch (_user_fields_row.Type)
                  {
                    case WKT_FIELD_TYPE.DATE:
                      _date = (DateTime)_reader[_idx_field];
                      _user_fields_row.Value = String.Format("{0:dd/MM/yyyy}", _date);
                      break;

                    default:
                      _string_limited = _reader[_idx_field].ToString();

                      // Player field value is limited to 50 chars
                      _user_fields_row.Value = (_string_limited.Length > 50) ? _string_limited.Remove(50)
                                                                               : _string_limited;

                      //_user_fields_row.Value = String.Format ("{0}", _string_limited);
                      break;
                  }
                }

                UserFields.Rows.Add(_user_fields_row);

              }
            }
            _reader.Close();
          }
        }
        UserFields.AcceptChanges();

      }
      catch (Exception Ex)
      {
        Log.Error(Ex.Message);
        return false;
      }

      return true;

    }

    //------------------------------------------------------------------------------
    // PURPOSE: Get account information by UserId
    //
    //  PARAMS:
    //      - INPUT: AccountId, SqlTransaction
    //      - OUT: UserFields
    //  RETURN : IPlayerFieldsTable with account field values.
    //
    public static Boolean WKT_ReadPlayerFields(Int64 AccountId, out WKT_IGetPlayerField UserFields, SqlTransaction Trx)
    {
      UserFields = new WKT_PlayerFieldsTable();
      return WKT_ReadPlayerFields(AccountId, out UserFields, Trx);
    }

    //------------------------------------------------------------------------------
    // PURPOSE:  Update player info fields, Do SQL update command
    // 
    //  PARAMS:
    //      - INPUT: 'PlayerFieldsTable' DataTable
    //
    //      - OUTPUT: Fill "Description" and "ErrorCode" values with sql update result
    //
    // RETURNS: Boolean , true if everything right , and false IF SOMETHING FAIL
    //
    //   NOTES:
    //
    public static bool WKT_UpdatePlayerFields(Int64 AccountId, String OldPin, WKT_IGetPlayerField FieldValues, String TerminalName, SqlTransaction Trx)
    {

      DataTable _field_definitions;
      DataRow _field_definition;
      Int32 _field_min_length;
      Int32 _field_max_length;
      String _table_name;
      String _field_name;
      String _pk_column_name;
      SqlCommand _sql_command;
      SqlParameter _num_affected;
      StringBuilder _str_builder;
      Int32 _idx_field;
      WKT_UPDATE_FIELD_STATUS _new_status;
      Boolean _has_errors;
      SqlDbType _field_type;
      DateTime _tmp_date;
      Int32 _legal_age;
      DateTime _min_legal_age;
      Int64 _operation_id;
      AccountMovementsTable _account_movement;
      CashierMovementsTable _cashier_movement;
      CashierSessionInfo _session_info;
      SqlParameter _sql_balance;
      Currency _current_balance;
      String _new_pin;

      List<PLAYER_INFO_FIELD_TYPE> _calculated_fields = new List<PLAYER_INFO_FIELD_TYPE>(
          new PLAYER_INFO_FIELD_TYPE[] { PLAYER_INFO_FIELD_TYPE.FULL_NAME });

      _field_definitions = new DataTable();
      _sql_command = new SqlCommand();
      _idx_field = 0;
      _has_errors = false;
      _new_pin = "";

      _sql_command.CommandText = "DECLARE @affected INT";
      _sql_command.CommandText += "    SET @affected = 0";

      _field_definitions = Get_wtk_player_info_fields(Trx);

      try
      {
        GenerateCalculatedFields(FieldValues, AccountId, Trx);

        for (Int32 _idx = 0; _idx < FieldValues.FieldsNumber; _idx++)
        {
          WKT_IPlayerField _field = FieldValues.GetField(_idx);

          _field.Value = _field.Value.Trim();

          _field.ErrorCode = WKT_UPDATE_FIELD_STATUS.INTERNAL_BEING_UPDATED;
          _field.ErrorDescription = "Not updated";

          _field_definition = _field_definitions.Rows.Find(_field.FieldId);
          if (_field_definition == null)
          {
            _field.ErrorCode = WKT_UPDATE_FIELD_STATUS.ERROR;
            _field.ErrorDescription = "Field definition not found";
            Log.Error("Field definition not found for FieldId = " + _field.FieldId);
            _has_errors = true;
            continue;
          }

          // Check out whether field can be updated or not
          if ((Int32)_field_definition["PIF_CAN_UPDATE"] == 0)
          {
            if (_calculated_fields.Contains(_field.FieldId))
            {
              // Calculated fields should be automatically updated 
              // E.g.: FULL NAME = NAME + SURNAME 1 + SURNAME 2
            }
            else
            {
              // Field can not be updated => error
              _field.ErrorCode = WKT_UPDATE_FIELD_STATUS.ERROR_NOT_EDITABLE;
              _field.ErrorDescription = "Not editable";
              _has_errors = true;

              continue;
            }
          }

          _table_name = (String)_field_definition["PIF_TABLE_NAME"];
          _field_name = (String)_field_definition["PIF_FIELD_NAME"];

          if (String.IsNullOrEmpty(_table_name))
          {
            Log.Error("Invalid Table Name");
            _has_errors = true;
            continue;
          }

          if (String.IsNullOrEmpty(_field_name))
          {
            Log.Error("Invalid Field Name");
            _has_errors = true;
            continue;
          }

          switch (_table_name)
          {
            case "ACCOUNTS":
              _pk_column_name = "ac_account_id";
              break;

            default:
              Log.Error("Table name '" + _table_name + "' not supported");
              _has_errors = true;
              continue;
          }

          _field_min_length = (Int32)_field_definition["PIF_MIN_LENGTH"];
          _field_max_length = (Int32)_field_definition["PIF_MAX_LENGTH"];

          if (_field.Value.Length < _field_min_length)
          {
            _field.ErrorCode = WKT_UPDATE_FIELD_STATUS.ERROR_MIN_LENGTH;
            _field.ErrorDescription = "String length less than minimum" + _field_min_length.ToString() + "/" + _field_min_length.ToString();
            _has_errors = true;
            continue;
          }
          if (_field.Value.Length > _field_max_length)
          {
            _field.ErrorCode = WKT_UPDATE_FIELD_STATUS.ERROR_MAX_LENGTH;
            _field.ErrorDescription = "String length greater than maximum " + _field_min_length.ToString() + "/" + _field_max_length.ToString();
            _has_errors = true;
            continue;
          }

          switch (_field.FieldId)
          {
            case PLAYER_INFO_FIELD_TYPE.NAME:
            case PLAYER_INFO_FIELD_TYPE.SURNAME1:
            case PLAYER_INFO_FIELD_TYPE.SURNAME2:
            case PLAYER_INFO_FIELD_TYPE.ADDRESS_03:
            case PLAYER_INFO_FIELD_TYPE.CITY:
              _field_type = SqlDbType.NVarChar;
              if (!ValidateFormat.TextWithoutNumbers(_field.Value))
              {
                _field.ErrorCode = WKT_UPDATE_FIELD_STATUS.ERROR_VALIDATION;
                _field.ErrorDescription = "Formato incorrecto";
                _has_errors = true;
                continue;
              }
              break;

            case PLAYER_INFO_FIELD_TYPE.PHONE_NUMBER_01:
            case PLAYER_INFO_FIELD_TYPE.PHONE_NUMBER_02:
              _field_type = SqlDbType.NVarChar;
              if (!string.IsNullOrEmpty(_field.Value) && !ValidateFormat.PhoneNumber(_field.Value, false))
              {
                _field.ErrorCode = WKT_UPDATE_FIELD_STATUS.ERROR_VALIDATION;
                _field.ErrorDescription = "Formato incorrecto";
                _has_errors = true;
                continue;
              }
              break;

            case PLAYER_INFO_FIELD_TYPE.ADDRESS_01:
            case PLAYER_INFO_FIELD_TYPE.ADDRESS_02:
            case PLAYER_INFO_FIELD_TYPE.ID1:
            case PLAYER_INFO_FIELD_TYPE.ID2:
              _field_type = SqlDbType.NVarChar;
              if (!ValidateFormat.TextWithNumbers(_field.Value))
              {
                _field.ErrorCode = WKT_UPDATE_FIELD_STATUS.ERROR_VALIDATION;
                _field.ErrorDescription = "Formato incorrecto";
                _has_errors = true;
                continue;
              }
              break;

            case PLAYER_INFO_FIELD_TYPE.ZIP:
              _field_type = SqlDbType.NVarChar;
              if (!ValidateFormat.RawNumber(_field.Value))
              {
                _field.ErrorCode = WKT_UPDATE_FIELD_STATUS.ERROR_VALIDATION;
                _field.ErrorDescription = "Formato incorrecto";
                _has_errors = true;
                continue;
              }
              break;

            case PLAYER_INFO_FIELD_TYPE.PIN:
              _new_pin = _field.Value;
              _field_type = SqlDbType.NVarChar;
              if (!ValidateFormat.RawNumber(_field.Value))
              {
                _field.ErrorCode = WKT_UPDATE_FIELD_STATUS.ERROR_VALIDATION;
                _field.ErrorDescription = "Formato incorrecto";
                _has_errors = true;
                continue;
              }
              break;

            case PLAYER_INFO_FIELD_TYPE.EMAIL_01:
            case PLAYER_INFO_FIELD_TYPE.EMAIL_02:
              _field_type = SqlDbType.NVarChar;
              if (!string.IsNullOrEmpty(_field.Value) && !ValidateFormat.Email(_field.Value))
              {
                _field.ErrorCode = WKT_UPDATE_FIELD_STATUS.ERROR_VALIDATION;
                _field.ErrorDescription = "Formato incorrecto";
                _has_errors = true;
                continue;
              }
              break;

            case PLAYER_INFO_FIELD_TYPE.BIRTHDAY:

              _tmp_date = new DateTime();

              try
              {
                _tmp_date = DateTime.ParseExact(_field.Value, "d/M/yyyy", null);
                _field_type = SqlDbType.DateTime;
              }
              catch (Exception)
              {
                _field.ErrorCode = WKT_UPDATE_FIELD_STATUS.ERROR_INVALID_DATE;
                _field.ErrorDescription = "Fecha incorrecta";
                _has_errors = true;
                continue;
              }

              if (!Int32.TryParse(Misc.ReadGeneralParams("Cashier", "LegalAge"), out _legal_age))
              {
                _legal_age = 18;
              }

              _min_legal_age = DateTime.Now.AddYears(_legal_age * -1);

              if (_tmp_date >= _min_legal_age)
              {
                _field.ErrorCode = WKT_UPDATE_FIELD_STATUS.ERROR_LEGAL_AGE;

                // Trick to allow Kiosk to acces 'LegalAge' General Param
                _field.ErrorDescription = "" + _legal_age;
                _has_errors = true;
                continue;
              }

              break;

            default:
              _field_type = SqlDbType.NVarChar;
              break;
          }

          _str_builder = new StringBuilder();
          _str_builder.AppendFormat(" UPDATE   {0}", _table_name);
          _str_builder.AppendFormat("    SET   {0} = @pFieldValue{1}", _field_name, _idx_field);
          _str_builder.AppendFormat("  WHERE   {0} = @pId;", _pk_column_name);
          _str_builder.AppendFormat("    SET   @AFFECTED = @AFFECTED + @@ROWCOUNT ");

          _sql_command.CommandText += _str_builder.ToString();

          _sql_command.Parameters.Add("@pFieldValue" + _idx_field, _field_type).Value = _field.Value;

          // Refresh localvalues to set values from DataBase
          _field.MinLength = (Int32)_field_definition["PIF_MIN_LENGTH"];
          _field.MaxLength = (Int32)_field_definition["PIF_MAX_LENGTH"];
          _field.Type = (WKT_FIELD_TYPE)_field_definition["PIF_TYPE"];
          _field.Updateable = (Int32)_field_definition["PIF_CAN_UPDATE"];

          if (_field.Value.Length > 50)
          {
            _field.Value = _field.Value.Remove(50);
          }

          _idx_field++;

        } // End for field

        _sql_command.Parameters.Add("@pId", SqlDbType.BigInt).Value = AccountId;
        _sql_command.CommandText += "set @pAffected = @affected";

        _str_builder = new StringBuilder();
        _str_builder.AppendLine(" ; SELECT   @pBalance     = AC_BALANCE ");
        _str_builder.AppendLine("     FROM   ACCOUNTS                   ");
        _str_builder.AppendLine("    WHERE   AC_ACCOUNT_ID = @pId       ");

        _sql_command.CommandText += _str_builder.ToString();

        _sql_balance = _sql_command.Parameters.Add("@pBalance", SqlDbType.Money);
        _sql_balance.Direction = ParameterDirection.Output;

        _num_affected = new SqlParameter("@pAffected", SqlDbType.Int);
        _num_affected.Direction = ParameterDirection.Output;
        _sql_command.Parameters.Add(_num_affected);
        _sql_command.Connection = Trx.Connection;
        _sql_command.Transaction = Trx;
        _sql_command.ExecuteNonQuery();

        _current_balance = (SqlMoney)_sql_balance.SqlValue;

        if ((Int32)_num_affected.Value != _idx_field)
        {
          _new_status = WKT_UPDATE_FIELD_STATUS.ERROR;
        }
        else if (_has_errors)
        {
          _new_status = WKT_UPDATE_FIELD_STATUS.NOT_UPDATED;
        }
        else
        {
          _new_status = WKT_UPDATE_FIELD_STATUS.UPDATED;
        }

        for (Int32 _idx = 0; _idx < FieldValues.FieldsNumber; _idx++)
        {
          WKT_IPlayerField _field = FieldValues.GetField(_idx);

          if (_field.ErrorCode == WKT_UPDATE_FIELD_STATUS.INTERNAL_BEING_UPDATED)
          {
            _field.ErrorCode = _new_status;

            switch (_new_status)
            {
              case WKT_UPDATE_FIELD_STATUS.ERROR:
                _field.ErrorDescription = "Update failed!";
                break;

              case WKT_UPDATE_FIELD_STATUS.NOT_UPDATED:
                _field.ErrorDescription = "Not updated";
                break;
            }
          }
        }

        if (_new_status != WKT_UPDATE_FIELD_STATUS.UPDATED)
        {
          Log.Error("Player fields not updated, error has ocurred!");

          return false;
        }

        _session_info = Cashier.GetSystemCashierSessionInfo(Misc.GetUserTypePromobox(), Trx, TerminalName);

        if (_session_info == null)
        {
          Log.Error("WKT_UpdatePlayerFields. GetSystemCashierSessionInfo error. TerminalName: " + TerminalName + ".");

          return false;
        }

        _account_movement = new AccountMovementsTable(_session_info, 0, TerminalName);
        _cashier_movement = new CashierMovementsTable(_session_info, TerminalName);

        if (_new_pin != OldPin)
        {
          if (!Operations.DB_InsertOperation(OperationCode.ACCOUNT_PIN_CHANGED, AccountId,
                                           0, 0, 0, 0, 0, 0, 0, string.Empty, out _operation_id, Trx))
          {
            Log.Error("WKT_UpdatePlayerFields. DB_InsertOperation error. AccountId: " + AccountId + ".");

            return false;
          }

          if (!_account_movement.Add(_operation_id, AccountId, MovementType.AccountPINChange, _current_balance, 0, 0, _current_balance))
          {
            Log.Error("WKT_UpdatePlayerFields. AccountMovementsTable.Add() error. AccountId: " + AccountId + ".");

            return false;
          }

          if (!_cashier_movement.Add(_operation_id, CASHIER_MOVEMENT.ACCOUNT_PIN_CHANGE, 0, AccountId, ""))
          {
            Log.Error("WKT_UpdatePlayerFields. CashierMovementsTable.Add() error. AccountId: " + AccountId + ".");

            return false;
          }
        }

        if (!Operations.DB_InsertOperation(OperationCode.ACCOUNT_PERSONALIZATION, AccountId,
                                            0, 0, 0, 0, 0, 0, 0, string.Empty, out _operation_id, Trx))
        {
          Log.Error("WKT_UpdatePlayerFields. DB_InsertOperation error. AccountId: " + AccountId + ".");

          return false;
        }

        if (!_account_movement.Add(_operation_id, AccountId, MovementType.AccountPersonalization, _current_balance, 0, 0, _current_balance))
        {
          Log.Error("WKT_UpdatePlayerFields. AccountMovementsTable.Add() error. AccountId: " + AccountId + ".");

          return false;
        }

        if (!_cashier_movement.Add(_operation_id, CASHIER_MOVEMENT.CARD_PERSONALIZATION, 0, AccountId, ""))
        {
          Log.Error("WKT_UpdatePlayerFields. CashierMovementsTable.Add() error. AccountId: " + AccountId + ".");

          return false;
        }

        if (!_account_movement.Save(Trx))
        {
          Log.Error("WKT_UpdatePlayerFields. AccountMovementsTable.Save() error. AccountId: " + AccountId + ".");

          return false;
        }

        if (!_cashier_movement.Save(Trx))
        {
          Log.Error("WKT_UpdatePlayerFields. CashierMovementsTable.Save() error. AccountId: " + AccountId + ".");

          return false;
        }
      }
      catch (Exception _ex)
      {
        for (Int32 _idx = 0; _idx < FieldValues.FieldsNumber; _idx++)
        {
          WKT_IPlayerField _field = FieldValues.GetField(_idx);

          _field.ErrorCode = WKT_UPDATE_FIELD_STATUS.ERROR;
          _field.ErrorDescription = "Updated failed!";
        }

        Log.Exception(_ex);
        return false;
      }


      for (Int32 _idx = 0; _idx < FieldValues.FieldsNumber; _idx++)
      {
        WKT_IPlayerField _field = FieldValues.GetField(_idx);

        if (_field.ErrorCode == 0)
        {
          _field.ErrorDescription = "Updated!";
        }
      }

      return !_has_errors;
    } // WKT_UpdatePlayerFields


    #endregion

    #region "Private functions"

    //------------------------------------------------------------------------------
    // PURPOSE: Generate Calculated Fields
    // Contains calls to all necessary methods to calculate field value
    //  PARAMS:
    //      - INPUT: 
    //          Fields values : Current Field Values
    //          AccountId     : Account Id
    //          Transaction   : Necessary if want to acces to unavaliable data , and need to query in DB
    //      - OUTPUT
    //          Fields:       : Table with new fields

    private static void GenerateCalculatedFields(WKT_IGetPlayerField Fields, Int64 AccountId, SqlTransaction Trx)
    {
      WKT_PlayerFieldsTable _table = Fields.PlayerFields;

      GenerateFullName(_table, AccountId, Trx);
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Generate Full name from Name, First Surname and Second Surname
    //
    //  PARAMS:
    //      - INPUT: 
    //          Fields values : Current Field Values
    //          AccountId     : Account Id
    //          Transaction   : To acces to user attributes if not present.
    //      - OUTPUT
    //          Fields:       : Table with new fields

    private static void GenerateFullName(WKT_PlayerFieldsTable Table, Int64 AccountId, SqlTransaction Trx)
    {
      WKT_PlayerFieldsRow _field_full_name;
      WKT_PlayerFieldsRow _field_search;
      WKT_Player_Full_Name _calculated_full_name;
      String _full_name_value;
      StringBuilder _str_bld;
      Boolean _need_query_full_name;
      SqlDataReader _reader;

      _str_bld = new StringBuilder();
      _need_query_full_name = false;

      _calculated_full_name.Name = String.Empty;
      _calculated_full_name.Surname1 = String.Empty;
      _calculated_full_name.Surname2 = String.Empty;

      _field_search = ((WKT_PlayerFieldsRow)Table.Rows.Find(PLAYER_INFO_FIELD_TYPE.NAME));
      if (_field_search != null)
      {
        _calculated_full_name.Name = _field_search.Value;
      }
      else
      {
        _need_query_full_name = true;
      }

      _field_search = ((WKT_PlayerFieldsRow)Table.Rows.Find(PLAYER_INFO_FIELD_TYPE.SURNAME1));
      if (_field_search != null)
      {
        _calculated_full_name.Surname1 = _field_search.Value;
      }
      else
      {
        _need_query_full_name = true;
      }

      _field_search = ((WKT_PlayerFieldsRow)Table.Rows.Find(PLAYER_INFO_FIELD_TYPE.SURNAME2));
      if (_field_search != null)
      {
        _calculated_full_name.Surname2 = _field_search.Value;
      }
      else
      {
        _need_query_full_name = true;
      }

      if (_need_query_full_name)
      {

        _str_bld.AppendLine("SELECT   AC_HOLDER_NAME3");
        _str_bld.AppendLine("       , AC_HOLDER_NAME1");
        _str_bld.AppendLine("       , AC_HOLDER_NAME2");
        _str_bld.AppendLine(" FROM  ACCOUNTS WHERE AC_ACCOUNT_ID = @pAccountId");

        using (SqlCommand _cmd = new SqlCommand(_str_bld.ToString(), Trx.Connection))
        {
          _cmd.Transaction = Trx;
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
          _reader = _cmd.ExecuteReader();

          if (_reader.HasRows && _reader.Read())
          {

            if (_calculated_full_name.Name == String.Empty)
            {
              _calculated_full_name.Name = (_reader[0] != DBNull.Value) ? (String)_reader[0] : String.Empty;
            }
            if (_calculated_full_name.Surname1 == String.Empty)
            {
              _calculated_full_name.Surname1 = (_reader[1] != DBNull.Value) ? (String)_reader[1] : String.Empty;
            }
            if (_calculated_full_name.Surname2 == String.Empty)
            {
              _calculated_full_name.Surname2 = (_reader[2] != DBNull.Value) ? (String)_reader[2] : String.Empty;
            }

          }
          _reader.Close();
        }
      }

      _full_name_value = CardData.FormatFullName(FULL_NAME_TYPE.ACCOUNT,
                                                  _calculated_full_name.Name,
                                                  _calculated_full_name.Surname1,
                                                  _calculated_full_name.Surname2);

      _field_full_name = (WKT_PlayerFieldsRow)Table.Rows.Find(PLAYER_INFO_FIELD_TYPE.FULL_NAME);

      if (_field_full_name != null)
      {

        _field_full_name.Value = _full_name_value;
        _field_full_name.Updateable = 1;
      }
      else
      {
        _field_full_name = Table.NewRow();
        _field_full_name.FieldId = PLAYER_INFO_FIELD_TYPE.FULL_NAME;
        _field_full_name.Value = _full_name_value;
        _field_full_name.Updateable = 1;
        _field_full_name.MinLength = 0;
        _field_full_name.MaxLength = 50;
        Table.Rows.Add(_field_full_name);
      }

    }
    //------------------------------------------------------------------------------
    // PURPOSE: Replace current object Special Chars  to HTML chars to prevent XML errors.
    //
    //  PARAMS:
    //      - INPUT: 
    //          
    //      - OUTPUT
    //
    //
    public void EncodeSpecialChars()
    {
      WKT_PlayerFieldsRow _row;
      Int32 _idx_row;

      for (_idx_row = 0; _idx_row < this.Rows.Count; _idx_row++)
      {
        _row = this[_idx_row];
        _row.Value = XML.StringToXmlValue(_row.Value);
      }
    }
    //------------------------------------------------------------------------------
    // PURPOSE: Replace current object HTML chars to Special Chars to display in client
    //  PARAMS:
    //      - INPUT: 
    //      - OUTPUT
    //
    public void DecodeSpecialChars()
    {
      String _value;
      WKT_PlayerFieldsRow _row;
      Int32 _idx_row;

      for (_idx_row = 0; _idx_row < this.Rows.Count; _idx_row++)
      {
        _row = this[_idx_row];
        _value = _row.Value;

        _value = _value.Replace("&amp;", "&");
        _value = _value.Replace("&lt;", "<");
        _value = _value.Replace("&gt;", ">");
        _value = _value.Replace("&quot;", "\"");
        _value = _value.Replace("&apos;", "'");

        _row.Value = _value;
      }
    }

    #endregion

    #region IGetPlayerField Members


    public WKT_IPlayerField GetField(int Idx)
    {
      //if (this.FieldsNumber < Idx)
      if (Idx < this.FieldsNumber)
      {
        return (WKT_IPlayerField)this[Idx];
      }
      else
      {
        throw new Exception("Invalid Row Index!");
      }
    }

    public WKT_PlayerFieldsTable PlayerFields
    {
      get { return this; }
      set
      {
        this.Clear();
        foreach (DataRow _row in value.Rows)
        {
          this.ImportRow(_row);
        }
        this.AcceptChanges();
      }
    }

    #endregion

    private struct WKT_Player_Full_Name
    {
      public String Name;
      public String Surname1;
      public String Surname2;
    }
  }

  public class WKT_PlayerFieldsRow : DataRow, WKT_IPlayerField
  {

    internal WKT_PlayerFieldsRow(DataRowBuilder Builder)
      : base(Builder)
    {
      FieldId = 0;
      Value = String.Empty;
      Updateable = 0;
      Type = 0;
      MinLength = 0;
      MaxLength = 0;
      ErrorCode = 0;
      ErrorDescription = String.Empty;
    }

    #region "Members"
    public PLAYER_INFO_FIELD_TYPE FieldId
    {
      get { return (PLAYER_INFO_FIELD_TYPE)base["FieldId"]; }
      set { base["FieldId"] = value; }
    }

    public String Value
    {
      get { return (String)base["Value"]; }
      set { base["Value"] = value; }
    }

    public Int32 Updateable
    {
      get { return (Int32)base["Updateable"]; }
      set { base["Updateable"] = value; }
    }

    public WKT_FIELD_TYPE Type
    {
      get { return (WKT_FIELD_TYPE)base["Type"]; }
      set { base["Type"] = value; }
    }

    public Int32 MinLength
    {
      get { return (Int32)base["MinLength"]; }
      set { base["MinLength"] = value; }
    }

    public Int32 MaxLength
    {
      get { return (Int32)base["MaxLength"]; }
      set { base["MaxLength"] = value; }
    }

    public WKT_UPDATE_FIELD_STATUS ErrorCode
    {
      get { return (WKT_UPDATE_FIELD_STATUS)base["ErrorCode"]; }
      set { base["ErrorCode"] = value; }
    }

    public string ErrorDescription
    {
      get { return (string)base["ErrorDescription"]; }
      set { base["ErrorDescription"] = value; }
    }

    #endregion

  }
}
