//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WKT_PlayerFields
// 
//   DESCRIPTION: Kiosk player fields
// 
//        AUTHOR: Artur Nebot
// 
// CREATION DATE: 29-JUN-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 08-JUN-2012 ANG    First version.
// 29-APR-2013 LEM    Fixed Bug #729: Added cashier and account movements for personalization and PIN update:
//                    - Modified WKT_ProcessSetPlayerFields to get TerminalName required in WKT_UpdatePlayerFields
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Data.SqlClient;
using WSI.Common;
using WSI.WCP;
using System.Xml;
using System.Xml.Serialization;
using System.Runtime.InteropServices;
using System.Data;
using System.IO;

public static partial class WCP_WKT
{


  //------------------------------------------------------------------------------
  // PURPOSE: Process player fields update,
  // 
  //  PARAMS:
  //      - INPUT:
  //        - Input message with fields to update ( Message contains WCP_WKT.WKT_PlayerFieldsTable ) 
  //
  //      - OUTPUT:
  //        - Output message with same table with Status fields filled.
  //   NOTES:

  private static void WKT_ProcessSetPlayerFields(WCP_Message WktRequest, WCP_Message WktResponse)
  {
    WCP_WKT_MsgSetPlayerFields _request;
    WCP_WKT_MsgSetPlayerFieldsReply _response;
    WCP_WKT.WKT_PlayerFieldsTable _player_fields;
    WKT_Player _player;
    long _account_id;
    bool _is_update_Ok;
    WCP_ResponseCodes _rc;
    String _terminal_name;
    Int32 _not_used_param1;
    String _not_used_param2;
    TerminalStatus _not_used_param3;
    Currency _sas_account_denom;

    try
    {
      _request = (WCP_WKT_MsgSetPlayerFields)WktRequest.MsgContent;
      _response = (WCP_WKT_MsgSetPlayerFieldsReply)WktResponse.MsgContent;
      _player_fields = _request.m_player_fields_table;

      using (DB_TRX _trx = new DB_TRX())
      {
        _player = new WKT_Player();
        _player.Pin = _request.Player.Pin;
        _player.Trackdata = _request.Player.Trackdata;

        _player.Login(out _account_id, out _rc);
        WktResponse.MsgHeader.ResponseCode = _rc;

        if (WktResponse.MsgHeader.ResponseCode != WCP_ResponseCodes.WCP_RC_WKT_OK)
        {
          return;
        }

        Misc.GetTerminalInfo(WktRequest.MsgHeader.TerminalId, (Int32)WCP_TerminalType.GamingTerminal, out _not_used_param1
          , out _not_used_param2, out _terminal_name, out _not_used_param3, out _not_used_param2, out _sas_account_denom, _trx.SqlTransaction);

        _is_update_Ok = WCP_WKT.WKT_PlayerFieldsTable.WKT_UpdatePlayerFields(_account_id, _player.Pin, _player_fields, _terminal_name, _trx.SqlTransaction);

        if (_is_update_Ok)
        {
          _trx.Commit();
        }
        else {
          WktResponse.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_WKT_ERROR;
          _trx.Rollback();
        }

        _response.SetPlayerFieldTable(_player_fields);

        WktResponse.MsgContent = _response;

      }

      return;

    }
    catch (Exception _ex) {

      Log.Exception(_ex);
      WktResponse.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_WKT_ERROR;
    }

  }
}

