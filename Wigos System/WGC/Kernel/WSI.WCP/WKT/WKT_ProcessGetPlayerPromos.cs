//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WKT_ProcessGetPlayerPromos.cs
// 
//   DESCRIPTION: WKT_ProcessGetPlayerPromos class
// 
//        AUTHOR: Xavier Iglesia
// 
// CREATION DATE: 01-AUG-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 01-AUG-2012 XIT    First version.
// 09-DEC-2014 JMM    Added by recharges promotions to the promotion list sent to the WKT
// 19-FEB-2016 ETP    Product Backlog Item 9761:Dual Currency: Promociones y canjes de puntos por cr�dito desde el Display Touch.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using WSI.Common;
using WSI.WCP;

public static partial class WCP_WKT
{

  public class WKT_Promo
  {
    public Int64 Id;
    public String Name;
    public String Description;
    public Int64 SmallResourceId;
    public Int64 LargeResourceId;
  }

  public class WKT_ApplicablePromo : WKT_Promo
  {
    public ACCOUNT_PROMO_CREDIT_TYPE PromoCreditType; //UNKNOWN=0; NR1=1; NR2=2; REDEEMABLE=3; POINT=4 
    public Decimal MinSpent;
    public Decimal MinSpentReward;
    public Decimal Spent;
    public Decimal SpentReward;
    public Decimal CurrentReward;
    public Decimal RechargesReward;
    public Decimal AvailableLimit;
  }

  //------------------------------------------------------------------------------
  // PURPOSE: Manages the bussiness logic of the message, doing the operations and filling the answer
  //  PARAMS:
  //      - INPUT:
  //          - WktRequest       : Input message
  //          - WktResponse      : Message to return
  //
  //      - OUTPUT:
  //         
  //
  //      - RETURNS:
  //       
  //
  private static void WKT_ProcessGetPlayerPromos(WCP_Message WktRequest, WCP_Message WktResponse)
  {
    WCP_WKT_MsgGetPlayerPromos _request;
    WCP_WKT_MsgGetPlayerPromosReply _response;
    WCP_ResponseCodes _response_code;
    Int64 _account_id;
    String _external_terminal_id;
    int _terminal_id;
    string _provider_id;
    string _terminal_name;
    TerminalStatus _status;
    string _server_external_id;
    Currency _sas_account_denom;

    _request = (WCP_WKT_MsgGetPlayerPromos)WktRequest.MsgContent;
    _response = (WCP_WKT_MsgGetPlayerPromosReply)WktResponse.MsgContent;
    _external_terminal_id = WktRequest.MsgHeader.TerminalId;
    try
    {
      //Login
      if (!WKT_PlayerLogin(_request.Player.Trackdata, _request.Player.Pin, out _account_id, out _response_code))
      {
        WktResponse.MsgHeader.ResponseCode = _response_code;
        return;
      }

      using (DB_TRX _db_trx = new DB_TRX())
      {
        if (!Misc.GetTerminalInfo(_external_terminal_id,
                                  1,
                                  out _terminal_id,
                                  out _provider_id,
                                  out _terminal_name,
                                  out _status,
                                  out _server_external_id,
                                  out _sas_account_denom,
                                  _db_trx.SqlTransaction))
        {
          return;
        }
      }
      WktResponse.MsgHeader.ResponseCode = GetPlayerPromos(_terminal_id, _account_id, _response);

      GetResources(_response);
      WktResponse.MsgContent = _response;
    }
    catch (Exception _ex)
    {
      Log.Exception(_ex);
      WktResponse.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_WKT_ERROR;
    }
  }

  private static void GetResources(WCP_WKT_MsgGetPlayerPromosReply _response)
  {
    List<Int64> _resources_to_find;
    WKT_ResourceInfoList _list_resource_info;
    List<Int64> _list_resources_not_found;

    _resources_to_find = new List<Int64>();
    foreach (List<WKT_Promo> _list_promos in _response.Promos)
    {
      if (_list_promos != null)
      {
        foreach (WKT_Promo _promo in _list_promos)
        {
          _resources_to_find.Add(_promo.LargeResourceId);
          _resources_to_find.Add(_promo.SmallResourceId);
        }
      }
    }

    if (_response.ApplicablePromos != null)
    {
      foreach (WKT_ApplicablePromo _promo in _response.ApplicablePromos)
      {
        _resources_to_find.Add(_promo.LargeResourceId);
        _resources_to_find.Add(_promo.SmallResourceId);
      }
    }

    GetResourceInfo(_resources_to_find, out  _list_resource_info, out _list_resources_not_found);
    _response.Resources = _list_resource_info;
  }  // WKT_ProcessGetPlayerPromos


  //------------------------------------------------------------------------------
  // PURPOSE: Fills the response message with the promotions and also returns a response code operation
  //  PARAMS:
  //      - INPUT:
  //          - WktRequest       : Input message
  //          - WktResponse      : Message to return
  //
  //      - OUTPUT:
  //         
  //
  //      - RETURNS: WCP_ResponseCodes
  //       
  //
  private static WCP_ResponseCodes GetPlayerPromos(Int32 TerminalId, Int64 AccountId, WCP_WKT_MsgGetPlayerPromosReply MsgResponse)
  {
    try
    {
      WKT_Promo _promo = new WKT_Promo();

      // JMM 04-DEC-2014: No querying the DB for Current, Next & Future promotions
      ////////MsgResponse.Promos = new List<WKT_Promo>[3];
      ////////MsgResponse.Promos[0] = new List<WKT_Promo>();
      ////////MsgResponse.Promos[1] = new List<WKT_Promo>();
      ////////MsgResponse.Promos[2] = new List<WKT_Promo>();

      // JMM 04-DEC-2014: No resources for promotions in WKT or LCD are needed
      MsgResponse.Resources = new WKT_ResourceInfoList();

      MsgResponse.ApplicablePromos = new List<WKT_ApplicablePromo>();

      using (DB_TRX _db_trx = new DB_TRX())
      {
        DataRow[] _promos_table;
        Int32 _last_hours_to_look_up_for_recharges;

        // JMM 04-DEC-2014: No querying the DB for Current, Next & Future promotions
        //////////// Current Promotions (= Now)
        //////////_promos_table = Promotion.GetAvailablePromotions (AccountId, NoPromotionStr, 1, out Status, _db_trx.SqlTransaction);
        //////////MsgResponse.Promos[0] = PromosTableToPromosList(_promos_table);

        //////////// Next Promotions
        //////////_promos_table = Promotion.GetAvailablePromotions (AccountId, NoPromotionStr, 2, out Status, _db_trx.SqlTransaction);
        //////////MsgResponse.Promos[1] = PromosTableToPromosList (_promos_table);

        //////////// Remove from Next Promotions those ones that are already in Current Promotions
        //////////if ( MsgResponse.Promos[0].Count > 0 && MsgResponse.Promos[1].Count > 0 )
        //////////{
        //////////  foreach ( WKT_Promo _item in MsgResponse.Promos[0] )
        //////////  {
        //////////    // Check whether promotion in Next Promotions has been already included in Current Promotions
        //////////    // If so => remove promotion from Next Promotions

        //////////    WKT_Promo _find_item = MsgResponse.Promos[1].Find (delegate (WKT_Promo _aux)
        //////////                                                       {
        //////////                                                         return _aux.Id == _item.Id;
        //////////                                                       }
        //////////                                                      );
        //////////    if ( _find_item != null )
        //////////    {
        //////////      // Remove promotion from Next Promotions
        //////////      MsgResponse.Promos[1].Remove (_find_item);
        //////////    }
        //////////  }
        //////////}

        //////////// Future Promotions
        //////////_promos_table = Promotion.GetAvailablePromotions (AccountId, NoPromotionStr, 3, out Status, _db_trx.SqlTransaction);
        //////////MsgResponse.Promos[2] = PromosTableToPromosList (_promos_table);

        //////////// By spent promotions
        //////////_promos_table = Promotion.GetWKTApplicableAvailablePromos(AccountId, NoPromotionStr, out Status, _db_trx.SqlTransaction);
        //////////MsgResponse.ApplicablePromos = PromosTableToApplicablePromosList(AccountId, _promos_table, _db_trx.SqlTransaction);

        // Current Promotions (= Now)
        MsgResponse.Promos[0] = null;
        // Next Promotions
        MsgResponse.Promos[1] = null;
        // Future Promotions
        MsgResponse.Promos[2] = null;

        // Applicable Promotions (By Spent, By Recharges & For Free)
        _last_hours_to_look_up_for_recharges = GeneralParam.GetInt32("ManualPromotions", "LastHoursToGetRecharges", 24);

        _promos_table = Promotion.GetAccountAvailablePromos(TerminalId, AccountId, _last_hours_to_look_up_for_recharges, _db_trx.SqlTransaction);
        MsgResponse.ApplicablePromos = PromosTableToApplicablePromosList(TerminalId, AccountId, _promos_table, _db_trx.SqlTransaction);

        return WCP_ResponseCodes.WCP_RC_WKT_OK;
      }
    }
    catch (Exception _ex)
    {
      Log.Exception(_ex);

      return WCP_ResponseCodes.WCP_RC_WKT_ERROR;
    }
  }

  //------------------------------------------------------------------------------
  // PURPOSE : Gets the promotions from datatable.
  //
  //  PARAMS :
  //      - INPUT :
  //            - PromosTable
  //
  //      - OUTPUT :
  //
  //      - RETURNS : List<WKT_Promo> with the promotions

  private static List<WKT_Promo> PromosTableToPromosList(DataTable PromosTable)
  {
    List<WKT_Promo> _promotions;
    WKT_Promo _promo;

    _promotions = new List<WKT_Promo>();

    foreach (DataRow _row in PromosTable.Rows)
    {
      _promo = new WKT_Promo();

      _promo.Id = (Int64)_row["PM_PROMOTION_ID"];
      _promo.Name = (String)_row["PM_NAME"];
      _promo.Description = (String)_row["PM_DESCRIPTION"];
      _promo.SmallResourceId = (Int64)_row["PM_SMALL_RESOURCE_ID"];
      _promo.LargeResourceId = (Int64)_row["PM_LARGE_RESOURCE_ID"];

      _promotions.Add(_promo);
    }

    return _promotions;

  } // PromosTableToPromosList

  //------------------------------------------------------------------------------
  // PURPOSE : Gets the applicable promotions from datatable.
  //
  //  PARAMS :
  //      - INPUT :
  //            - PromosTable
  //
  //      - OUTPUT :
  //
  //      - RETURNS : List<WKT_ApplicablePromo> containing the promotions

  private static List<WKT_ApplicablePromo> PromosTableToApplicablePromosList(Int32 TerminalId, Int64 AccountId,
                                                                             DataRow[] Promos,
                                                                             SqlTransaction SqlTransaction)
  {
    List<WKT_ApplicablePromo> _promotions;
    WKT_ApplicablePromo _promo;
    Promotion.TYPE_PROMOTION_DATA _promo_data;
    DateTime _now;
    String _iso_code;

    WSI.Common.Terminal.TerminalInfo _terminal_info;

    _promotions = new List<WKT_ApplicablePromo>();

    _now = WGDB.Now;


    if (!WSI.Common.Terminal.Trx_GetTerminalInfo(TerminalId, out _terminal_info, SqlTransaction))
    {
      return null;
    }

    _iso_code = _terminal_info.CurrencyCode;

    foreach (DataRow _row in Promos)
    {
      _promo = new WKT_ApplicablePromo();

      _promo.Id = (Int64)_row["PM_PROMOTION_ID"];
      _promo.Name = (String)_row["PM_NAME"];
      _promo.Description = _row.IsNull("PM_TEXT_ON_PROMOBOX") ? "" : (String)_row["PM_TEXT_ON_PROMOBOX"];
      _promo.SmallResourceId = Convert.ToInt64(_row["PM_SMALL_RESOURCE_ID"]);
      _promo.LargeResourceId = Convert.ToInt64(_row["PM_LARGE_RESOURCE_ID"]);

      _promo.PromoCreditType = (ACCOUNT_PROMO_CREDIT_TYPE)_row["PM_CREDIT_TYPE"];

      _promo_data = new Promotion.TYPE_PROMOTION_DATA();

      Promotion.ReadPromotionData(SqlTransaction, _promo.Id, _promo_data);

      _promo.MinSpent = _promo_data.min_spent;
      _promo.MinSpentReward = _promo_data.min_spent_reward;
      _promo.Spent = _promo_data.spent;
      _promo.SpentReward = _promo_data.spent_reward;

      _promo.CurrentReward = (Decimal)_row["PM_REWARD"];
      if (_promo.PromoCreditType != ACCOUNT_PROMO_CREDIT_TYPE.POINT)
      {
        _promo.CurrentReward = CurrencyExchange.GetExchange(_promo.CurrentReward, CurrencyExchange.GetNationalCurrency(), _terminal_info.CurrencyCode, SqlTransaction);
      }


      _promo.RechargesReward = (Decimal)_row["PM_RECHARGES_REWARD"];
      _promo.AvailableLimit = (Decimal)_row["PM_AVAILABLE_LIMIT"];

      _promotions.Add(_promo);
    }

    return _promotions;

  } // PromosTableToApplicablePromosList

} // WCP_WKT