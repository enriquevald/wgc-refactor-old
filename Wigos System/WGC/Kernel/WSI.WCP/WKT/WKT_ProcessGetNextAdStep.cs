//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WKT_ProcessGetNextAdSteps.cs
// 
//   DESCRIPTION: Process get Next Steps process
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 08-JUN-2012 ANG    First version.
// 22-JUN-2012 JCM    Moved IsTimeInRange to Common.Schedule
// 24-AUG-2012 XIT    Added Area Fields.
// 27-AUG-2012 ANG    Added GetEnabledFunctionalities() reject not enabled Promotions and Draws.
// 05-SEP-2012 XIT    Code Refactored        
// 06-SEP-2012 ANG    Sleep thread if Promobox General Param isn't enabled         
// 03-OCT-2012 TJG    WKT_FUNCTIONALITY replaced by PromoBOX.Functionality
// 03-OCT-2012 TJG    GetEnabledFunctionalities() replaced by PromoBOX.ReadPromoBoxFunc()
// 05-NOV-2013 RMS    Check if step is a valid step in WKT_ProcessGetNextAdvStep
//------------------------------------------------------------------------------
//
// PENDING : Set Image resolution from DB ( Now , field not exist in database! ) 
//
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Threading;
using System.Data.SqlClient;
using WSI.Common;
using WSI.WCP;

public static partial class WCP_WKT
{
  //------------------------------------------------------------------------------
  // PURPOSE: Process GetNextAdvStep Message
  //
  //  PARAMS:
  //      - INPUT: Input Message ( blank ) 
  //
  //      - OUTPUT: Message with current step ( Image ) to display in kiosk
  //
  // RETURNS:
  private static void WKT_ProcessGetNextAdvStep(WCP_Message WktRequest, WCP_Message WktResponse)
  {
    WCP_WKT_MsgGetNextAdvStep _request;
    WCP_WKT_MsgGetNextAdvStepReply _response;
    WKT_Advertisement.WKT_AdvSteps _steps;
    WKT_Advertisement.WKT_AdvStep _step;
    WKT_Advertisement.WKT_AdvImage _image;
    WKT_Advertisement.WKT_AdvStaticImage _static_image;
    DataRow[] _rsteps;

    List<Int64> _resources;
    List<Int64> _not_found_resources;
    WKT_ResourceInfoList _list_resources;

    _resources = new List<Int64>();
    _not_found_resources = new List<Int64>();
    try
    {
      _request = (WCP_WKT_MsgGetNextAdvStep)WktRequest.MsgContent;
      _response = (WCP_WKT_MsgGetNextAdvStepReply)WktResponse.MsgContent;

      _rsteps = WKT_Advertisement.NextSteps("", new DataTable(), 1);
      _steps = new WKT_Advertisement.WKT_AdvSteps();

      foreach (DataRow _current_step in _rsteps)
      {
        // RMS 05-NOV-2013: Check if step is a valid step
        if (_current_step.IsNull("Type") || String.IsNullOrEmpty((String)_current_step["Type"]))
        {
          // Ignore empty step (without type)
          continue;
        }

        _step = new WKT_Advertisement.WKT_AdvStep();
        _static_image = new WKT_Advertisement.WKT_AdvStaticImage();

        // IMAGE
        _image = new WKT_Advertisement.WKT_AdvImage();
        _image.ImageWidth = 1280;  //TODO: XIT GET FROM DB
        _image.ImageHeight = 1024; //TODO: XIT GET FROM DB
        _image.Position.X = _current_step["RectX"] != DBNull.Value ? (Int64)_current_step["RectX"] : 0;
        _image.Position.Y = _current_step["RectY"] != DBNull.Value ? (Int64)_current_step["RectY"] : 0;
        _image.ResourceId = _current_step["ResourceId"] != null ? (Int64)_current_step["ResourceId"] : 0;

        _resources.Add(_image.ResourceId);

        _static_image.Image = _image;

        _static_image.AreaWidth = _current_step["AreaWidth"] != DBNull.Value ? (Int64)_current_step["AreaWidth"] : 1280;
        _static_image.AreaHeight = _current_step["AreaHeight"] != DBNull.Value ? (Int64)_current_step["AreaHeight"] : 1024;
        _static_image.AreaPosition.X = 0;
        _static_image.AreaPosition.Y = 0;

        _static_image.Duration = _current_step["StepDuration"] != DBNull.Value ? (Int32)_current_step["StepDuration"] : WKT_Advertisement.GetDefaultRefreshTime();
        _step.StaticImages.Add(_static_image);

        _steps.Add(_step);
      }

      // Get Resource info ( Len , hash ... )
      if (!GetResourceInfo(_resources, out _list_resources, out _not_found_resources))
      {
        return;
      }

      _response.m_steps = _steps;
      _response.Resources = _list_resources;

      if (_not_found_resources.Count > 0)
      {
        String _fail_step_id;
        _fail_step_id = String.Empty;

        foreach (Int64 _resource_id_not_found in _not_found_resources)
        {
          foreach (DataRow _row in _rsteps) // Find StepId with wrong ResourceId
          {
            if (!_row.IsNull("ResourceId") && ((Int64)_row["ResourceId"] == _resource_id_not_found))
            {
              _fail_step_id = (String)_row["StepId"];
              break;
            }
          }
          Log.Warning("Could not find resource: " + _resource_id_not_found.ToString() + ". Referenced on " + _fail_step_id + ".");
        }

        return;
      }

      WktResponse.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_WKT_OK;
      WktResponse.MsgContent = (WCP_WKT_MsgGetNextAdvStepReply)_response;
      return;
    }
    catch (Exception _ex)
    {
      Log.Exception(_ex);
      WktResponse.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_WKT_ERROR;
    }

  } // WKT_ProcessGetNextAdStep


  public partial class WKT_Advertisement
  {

    #region "Constants"
    private const int MAX_REFRESH_TIME = 60000;
    private const int MIN_REFRESH_TIME = 1000;
    #endregion

    #region "Private members"
    private static ReaderWriterLock m_lock = new ReaderWriterLock();
    private static DataRow m_current_step;
    private static Int32 m_current_step_tick;
    private static Int32 m_current_step_duration;
    private static AutoResetEvent m_new_step_available = new AutoResetEvent(false);
    #endregion


    //------------------------------------------------------------------------------
    // PURPOSE:  Attend Kiosk request for Steps
    //
    //  PARAMS:
    //      - INPUT:
    //               
    //      - OUTPUT:
    //
    // RETURNS:
    public static DataRow[] NextSteps(String LastStepId, DataTable Steps, Int32 MaxSteps)
    {
      DataRow[] _next_steps;
      Int32 _idx_try;
      Int32 _duration;
      Int32 _elapsed;
      Int32 _remaining;
      DataRow _current_step;
      Int32 _max_try = 5;

      _next_steps = null;

      try
      {
        m_lock.AcquireReaderLock(Timeout.Infinite);

        for (_idx_try = 0; _idx_try < _max_try; _idx_try++)
        {

          _duration = m_current_step_duration;
          _elapsed = (Int32)WSI.Common.Misc.GetElapsedTicks(m_current_step_tick);
          _remaining = Math.Max(0, _duration - _elapsed);

          if (_idx_try < _max_try - 1 && _remaining < 1000)
          {
            // Wait for next one
            m_lock.ReleaseReaderLock();
            m_new_step_available.WaitOne(Math.Max(100, _remaining));
            m_lock.AcquireReaderLock(Timeout.Infinite);
          }
          else
          {
            if (m_current_step != null)
            {
              _current_step = Clone(m_current_step);
              _next_steps = new DataRow[] { _current_step };
              // Important: recalculate the "duration", should be displayed only the "remaining" time
              _next_steps[0]["StepDuration"] = _remaining;
            }
            break;
          }
        } // for
      }
      finally
      {
        m_lock.ReleaseReaderLock();
      }

      if (_next_steps == null)
      {
        _next_steps = new DataRow[1];
        _next_steps[0] = GetNoDataRow();
      }

      return _next_steps;

    } // NextSteps

    //------------------------------------------------------------------------------
    // PURPOSE: Get a Datatable with all Advertisements to show in Koisk
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS: 

    private static DataTable GetUpperMonitorContents()
    {
      DataTable _dtable;
      DataTable _dtable_tmp;
      List<PromoBOX.Functionality> _func_list;

      using (DB_TRX _trx = new DB_TRX())
      {
        _dtable = GetDataTableSchemma();

        // Get ALL enabled functionalities 
        _func_list = PromoBOX.ReadPromoBoxFunc(PromoBOX.Functionality.ALL, true, _trx.SqlTransaction);

        if (_func_list.Contains(PromoBOX.Functionality.PROMOTIONS))
        {
          _dtable_tmp = GetPromotions(_trx.SqlTransaction);
          _dtable.Merge(_dtable_tmp);
        }

        if (_func_list.Contains(PromoBOX.Functionality.ADVERTISEMENTS))
        {
          _dtable_tmp = GetAdvertisements(_trx.SqlTransaction);
          FilterDataTable(_dtable_tmp);
          _dtable.Merge(_dtable_tmp);
        }

        if (_func_list.Contains(PromoBOX.Functionality.DRAWS))
        {
          _dtable_tmp = GetDraws(_trx.SqlTransaction);
          _dtable.Merge(_dtable_tmp);
        }

        if (_dtable.Rows.Count == 0)
        {
          _dtable.Clear();
          _dtable.ImportRow(GetNoDataRow());
        }

        return _dtable;
      }
    } // GetUpperMonitorContents

    //------------------------------------------------------------------------------
    // PURPOSE: Get Row with "No Data" default message.
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS: 

    private static DataRow GetNoDataRow()
    {

      DataTable _dtable;
      DataRow _default_ad;
      Int64 _default_resource_id;

      using (DB_TRX _trx = new DB_TRX())
      {
        _default_resource_id = GetResourceImageFromName("IMG01", _trx.SqlTransaction);
      }

      _dtable = GetDataTableSchemma();
      _default_ad = _dtable.NewRow();

      _default_ad["StepId"] = "00001";
      _default_ad["Type"] = "";
      _default_ad["ResourceId"] = _default_resource_id;
      _default_ad["Weekday"] = 127;
      _default_ad["TimeFrom1"] = 0;
      _default_ad["TimeTo1"] = 0;

      _dtable.Rows.Add(_default_ad);
      _default_ad.AcceptChanges();

      return _default_ad;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Get datatable with Promotions rows
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS: DataTable with promotion data.

    private static DataTable GetPromotions(SqlTransaction SqlTrx)
    {
      StringBuilder _sql_query;
      DateTime _date_now;
      DateTime _date_from;
      DateTime _date_to;
      DateTime _today_opening;
      DateTime _today_closing;
      Int32 _closing_time;
      DateTime _aux_date_time;
      DataTable _dt;
      Promotion.TYPE_PROMOTION_DATA _promo_data;

      _today_opening = Misc.TodayOpening();
      _today_closing = _today_opening.AddDays(1);
      _date_now = WGDB.Now;

      _date_from = _date_now;
      _date_to = _today_closing;

      // Criteria should be same as calling GetAvailablePromotions with WhichOnes = 2-next  
      _sql_query = new StringBuilder();

      _sql_query.AppendLine("SELECT 'PROMO-'+RIGHT('0000' + CAST(PM_PROMOTION_ID AS NVARCHAR),5) As STEPID");
      _sql_query.AppendLine("     , 'PRM'                                                        As TYPE");
      _sql_query.AppendLine("     , RES_RESOURCE_ID                                              As RESOURCEID");
      _sql_query.AppendLine("     , PM_SCHEDULE_WEEKDAY                                          As WEEKDAY");
      _sql_query.AppendLine("     , PM_SCHEDULE1_TIME_FROM                                       As TIMEFROM1");
      _sql_query.AppendLine("     , PM_SCHEDULE1_TIME_TO	                                        As TIMETO1");
      _sql_query.AppendLine("     , PM_SCHEDULE2_ENABLED                                         As SCHEDULE2_ENABLED");
      _sql_query.AppendLine("     , PM_SCHEDULE2_TIME_FROM	                                      As TIMEFROM2");
      _sql_query.AppendLine("     , PM_SCHEDULE2_TIME_TO	                                        As TIMETO2");
      _sql_query.AppendLine("     , PM_PROMOTION_ID");
      _sql_query.AppendLine("  FROM PROMOTIONS P");
      _sql_query.AppendLine(" INNER JOIN WKT_RESOURCES R");
      _sql_query.AppendLine("    ON P.PM_LARGE_RESOURCE_ID = R.RES_RESOURCE_ID");
      _sql_query.AppendLine(" WHERE PM_ENABLED = 1");
      _sql_query.AppendLine("   AND ( PM_TYPE = @PromoTypeManual");
      _sql_query.AppendLine("       OR PM_TYPE = @PromoTypePreassigned )");
      _sql_query.AppendLine("   AND PM_VISIBLE_ON_PROMOBOX = 1");
      _sql_query.AppendLine("   AND (   ( @Now BETWEEN PM_DATE_START AND PM_DATE_FINISH )");
      _sql_query.AppendLine("        OR ( @TodayClosing       BETWEEN PM_DATE_START AND PM_DATE_FINISH )");
      _sql_query.AppendLine("        OR (    ( PM_DATE_START  BETWEEN @Now AND @TodayClosing )");
      _sql_query.AppendLine("            AND ( PM_DATE_FINISH BETWEEN @Now AND @TodayClosing )");
      _sql_query.AppendLine("           )");
      _sql_query.AppendLine("       )");
      _sql_query.AppendLine(" ORDER BY PM_DATE_START");

      using (SqlCommand _sql_cmd = new SqlCommand(_sql_query.ToString(), SqlTrx.Connection, SqlTrx))
      {
        // Only manual promotions must be shown
        _sql_cmd.Parameters.Add("@PromoTypeManual", SqlDbType.Int).Value = (Int32)Promotion.PROMOTION_TYPE.MANUAL;
        _sql_cmd.Parameters.Add("@PromoTypePreassigned", SqlDbType.Int).Value = (Int32)Promotion.PROMOTION_TYPE.PREASSIGNED;

        _sql_cmd.Parameters.Add("@Now", SqlDbType.DateTime).Value = _date_from;
        _sql_cmd.Parameters.Add("@TodayClosing", SqlDbType.DateTime).Value = _date_to;

        _dt = GetData(_sql_cmd);

        Int32.TryParse(WSI.Common.Misc.ReadGeneralParams("WigosGUI", "ClosingTime"), out _closing_time);

        foreach (DataRow _row in _dt.Rows)
        {
          Int64 _promo_id;
          DayOfWeek _day_of_week;
          Int32 _num_days;

          _promo_id = (Int64)_row["PM_PROMOTION_ID"];
          _promo_data = new Promotion.TYPE_PROMOTION_DATA();
          if (!Promotion.ReadPromotionData(SqlTrx, _promo_id, _promo_data))
          {
            // Error reading promotion data => remove promotion
            _row.Delete();

            continue;
          }

          // Conditions to check
          //    - Match any of its enabled weekdays
          //    - Match any of its defined time slots

          //    - Match any of its enabled weekdays
          _num_days = 0;
          for (_aux_date_time = _date_from; _aux_date_time.CompareTo(_date_to) <= 0; _aux_date_time = _aux_date_time.AddDays(1))
          {
            _day_of_week = Promotion.GetWeekDay(_aux_date_time, _closing_time);

            if (ScheduleDay.WeekDayIsInSchedule(_day_of_week, _promo_data.schedule_weekdays))
            {
              _num_days++;

              break;
            }
          }

          if (_num_days == 0)
          {
            // None of the days in [_date_from, _date_to] match any of promo's weekdays => remove promotion
            _row.Delete();

            continue;
          }

          //    - Match any of its defined time slots

          // Check time slot #1 (schedule1)
          if (ScheduleDay.IsTimeRangeInRange((Int32)_date_from.TimeOfDay.TotalSeconds,   //Check time slot from current time...
                                              (Int32)_date_to.TimeOfDay.TotalSeconds - 1, //...to just before closing
                                              _promo_data.schedule1_time_from,
                                              _promo_data.schedule1_time_to))
          {
            // In time slot #1
            continue;
          }

          // Check time slot #2 (schedule2)
          if (_promo_data.schedule2_enabled)
          {
            if (ScheduleDay.IsTimeRangeInRange((Int32)_date_from.TimeOfDay.TotalSeconds,   //Check time slot from current time...
                                                (Int32)_date_to.TimeOfDay.TotalSeconds - 1, //...to just before closing
                                                _promo_data.schedule2_time_from,
                                                _promo_data.schedule2_time_to))
            {
              // In time slot #2
              continue;
            }
          }

          // Exclude the promotion from the list
          _row.Delete();

        } // foreach

        _dt.AcceptChanges();

        return _dt;
      }

    } // GetPromotions

    //------------------------------------------------------------------------------
    // PURPOSE:  Get datatable with Advertisements rows
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:  Datatable with Promotions rows
    //
    private static DataTable GetAdvertisements(SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      _sb.AppendLine(" SELECT   'AD-'+RIGHT('0000' + CAST(AD_ID AS NVARCHAR),5)                                As STEPID            ");
      _sb.AppendLine("        , 'ADS'                                                                          As TYPE              ");
      _sb.AppendLine("        , RES_RESOURCE_ID                                                                As RESOURCEID        ");
      _sb.AppendLine("        , AD_WEEKDAY                                                                     As WEEKDAY           ");
      _sb.AppendLine("        , AD_TIME_FROM1                                                                  As TIMEFROM1         ");
      _sb.AppendLine("        , AD_TIME_TO1	                                                                   As TIMETO1           ");
      _sb.AppendLine("        , case WHEN ( AD_TIME_FROM2 IS NULL AND AD_TIME_TO2 IS NULL ) THEN 0 ELSE 1 END  As SCHEDULE2_ENABLED ");
      _sb.AppendLine("        , ISNULL(AD_TIME_FROM2,0)                                                        As TIMEFROM2         ");
      _sb.AppendLine("        , ISNULL(AD_TIME_TO2,0)                                                          As TIMETO2           ");

      _sb.AppendLine("        , ASCR_RECT_X                                                                    As RECTX             ");
      _sb.AppendLine("        , ASCR_RECT_Y                                                                    As RECTY             ");

      _sb.AppendLine("        , ASCR_RECT_WIDTH                                                                As AREAWIDTH         ");
      _sb.AppendLine("        , ASCR_RECT_HEIGHT                                                               As AREAHEIGHT        ");

      _sb.AppendLine("        , ASCR_TEXT                                                                      As TEXTVALUE         ");
      _sb.AppendLine("        , ASCR_TEXT_COLOR                                                                As TEXTCOLOR         ");
      _sb.AppendLine("        , ASCR_TEXT_FONT                                                                 As TEXTFONT          ");
      _sb.AppendLine("        , ASCR_TEXT_ALIGNMENT                                                            As TEXTALIGNMENT     ");
      _sb.AppendLine("   FROM   WKT_ADS A                                                                                           ");
      _sb.AppendLine("  INNER   JOIN WKT_AD_STEPS S                                                                                 ");
      _sb.AppendLine("     ON   A.AD_ID = S.As_AD_ID                                                                                ");
      _sb.AppendLine("  INNER   JOIN WKT_AD_STEP_DETAILS	D                                                                         ");
      _sb.AppendLine("     ON   S.AS_STEP_ID = D.ASCR_STEP_ID                                                                       ");
      _sb.AppendLine("  INNER   JOIN WKT_RESOURCES	R                                                                               ");
      _sb.AppendLine("     ON   D.ASCR_RESOURCE_ID = R.RES_RESOURCE_ID                                                              ");
      _sb.AppendLine("  WHERE   AD_FROM < GETDATE()                                                                                 ");
      _sb.AppendLine("    AND   AD_TO > GETDATE()                                                                                   ");
      _sb.AppendLine("    AND   AD_ENABLED = 1                                                                                      ");

      using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
      {
        return GetData(_sql_cmd);
      }

    } // GetAdvertisements

    //------------------------------------------------------------------------------
    // PURPOSE:   Get Draws with Promotions rows
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:  Datatable with Draws rows
    //
    private static DataTable GetDraws(SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      _sb.AppendLine("SELECT   'DRW-'+RIGHT('0000' +cAst(dr_id As nvarchar),5) As STEPID      ");
      _sb.AppendLine("       , 'DRW'                                           As TYPE        ");
      _sb.AppendLine("       ,	res_resource_id                                As RESOURCEID  ");
      _sb.AppendLine("  FROM  DRAWS D                                                         ");
      _sb.AppendLine(" INNER  JOIN WKT_RESOURCES R                                            ");
      _sb.AppendLine("    ON  D.DR_LARGE_RESOURCE_ID = R.RES_RESOURCE_ID                      ");
      _sb.AppendLine(" WHERE  DR_STARTING_DATE < GETDATE()                                    ");
      _sb.AppendLine("   AND  DR_ENDING_DATE > GETDATE()                                      ");
      _sb.AppendLine("   AND  DR_STATUS= 0                                                    ");
      _sb.AppendLine("   AND  DR_VISIBLE_ON_PROMOBOX = 1                                      ");

      using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
      {
        return GetData(_sql_cmd);
      }
    } // GetDraws

    //------------------------------------------------------------------------------
    // PURPOSE: Get Data from database.
    //
    //  PARAMS:
    //      - INPUT: SqlCommand with Command and connection setted
    //
    //      - OUTPUT:
    //
    // RETURNS: DataTable filled with database rows.

    private static DataTable GetData(SqlCommand SqlCmd)
    {
      DataTable _data;

      _data = GetDataTableSchemma();
      _data.Load(SqlCmd.ExecuteReader());

      return _data;

    } // GetData

    //------------------------------------------------------------------------------
    // PURPOSE: Get Datatable default schemma
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:  Blank datatable with structure.
    //
    private static DataTable GetDataTableSchemma()
    {
      DataTable _dt;

      _dt = new DataTable();

      _dt.Columns.Add(new DataColumn("StepId", typeof(String)));
      _dt.Columns.Add(new DataColumn("Type", typeof(String)));
      _dt.Columns.Add(new DataColumn("ResourceId", typeof(Int64)));
      _dt.Columns.Add(new DataColumn("WeekDay", typeof(Byte)));

      _dt.Columns.Add(new DataColumn("TimeFrom1", typeof(Int32)));
      _dt.Columns["TimeFrom1"].DefaultValue = 0;

      _dt.Columns.Add(new DataColumn("TimeTo1", typeof(Int32)));
      _dt.Columns["TimeTo1"].DefaultValue = 0;

      _dt.Columns.Add(new DataColumn("Schedule2_enabled", typeof(bool)));
      _dt.Columns.Add(new DataColumn("TimeFrom2", typeof(Int32)));
      _dt.Columns.Add(new DataColumn("TimeTo2", typeof(Int32)));

      // Trick , Really this column should be located in step!
      _dt.Columns.Add(new DataColumn("StepDuration", typeof(Int32)));

      _dt.Columns.Add(new DataColumn("RectX", typeof(Int64)));
      _dt.Columns.Add(new DataColumn("RectY", typeof(Int64)));

      _dt.Columns.Add(new DataColumn("AreaWidth", typeof(Int64)));
      _dt.Columns.Add(new DataColumn("AreaHeight", typeof(Int64)));

      _dt.Columns.Add(new DataColumn("TextValue", typeof(String)));
      _dt.Columns.Add(new DataColumn("TextColor", typeof(Int32)));
      _dt.Columns.Add(new DataColumn("TextFont", typeof(Int32)));
      _dt.Columns.Add(new DataColumn("TextAlignment", typeof(Int32)));

      _dt.PrimaryKey = new DataColumn[] { _dt.Columns["StepId"] };

      return _dt;

    } // GetDataTableSchemma

    //------------------------------------------------------------------------------
    // PURPOSE: Filter data by campaign apply day and time.
    //
    //  PARAMS:
    //      - INPUT: DataTable
    //
    //      - OUTPUT: DataTable
    //
    // RETURNS: 
    //
    private static void FilterDataTable(DataTable AdTable)
    {
      Byte _weekDay;
      Int32 _idx;
      DataRow _current_row;
      Boolean _is_schedule2_enabled;
      Boolean _is_in_schedule;
      DateTime _current_time;

      _is_in_schedule = true;
      _is_schedule2_enabled = false;
      _current_time = System.DateTime.Now;

      for (_idx = 0; _idx < AdTable.Rows.Count; _idx++)
      {

        _current_row = AdTable.Rows[_idx];

        // Filter By Apply Day
        if (!_current_row.IsNull("WeekDay"))
        {
          _weekDay = (Byte)_current_row["WeekDay"];

          if (!ScheduleDay.WeekDayIsInSchedule(DateTime.Now.DayOfWeek, _weekDay))
          {
            _current_row.Delete();
            continue;
          }
        }

        // Filter by Time Range
        if (!_current_row.IsNull("Schedule2_enabled"))
        {
          _is_schedule2_enabled = (Boolean)_current_row["Schedule2_enabled"];
        }
        else
        {
          _is_schedule2_enabled = false;
        }

        _is_in_schedule = ScheduleDay.IsTimeInRange(_current_time.TimeOfDay, (int)_current_row["TimeFrom1"], (int)_current_row["TimeTo1"]);

        if (!_is_in_schedule && _is_schedule2_enabled)
        {
          _is_in_schedule = ScheduleDay.IsTimeInRange(_current_time.TimeOfDay, (int)_current_row["TimeFrom2"], (int)_current_row["TimeTo2"]);
        }

        if (!_is_in_schedule)
        {
          _current_row.Delete();
        }

      }

      AdTable.AcceptChanges();


    } // FilterDataTable

    //------------------------------------------------------------------------------
    // PURPOSE:  Init thread ,
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS: 

    public static void Init()
    {
      Thread _thread;

      _thread = new Thread(AdvertisementThread);
      _thread.Start();

    } // Init

    //------------------------------------------------------------------------------
    // PURPOSE: Get resourceId from Image Name
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS: 

    private static Int64 GetResourceImageFromName(String Name, SqlTransaction Trx)
    {
      SqlCommand _cmd;
      StringBuilder _str_bld;
      Int64 _resource_id;
      Object _id;
      _resource_id = 0;
      _str_bld = new StringBuilder();
      _str_bld.AppendLine("SELECT CIM_RESOURCE_ID     ");
      _str_bld.AppendLine("  FROM WKT_IMAGES          ");
      _str_bld.AppendLine(" WHERE CIM_NAME = @pImgName");

      _cmd = new SqlCommand(_str_bld.ToString(), Trx.Connection, Trx);
      _cmd.Parameters.Add("@pImgName", SqlDbType.NVarChar, 99).Value = Name;
      _id = _cmd.ExecuteScalar();
      if (_id != null)
      {
        _resource_id = (Int64)_id;
      }

      return _resource_id;
    }

    //------------------------------------------------------------------------------
    // PURPOSE:  Get default refresh time from general params
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS: 

    public static int GetDefaultRefreshTime()
    {

      int _value;

      if (!int.TryParse(GeneralParam.Value("WigosKiosk", "FrameRefreshTime"), out _value))
      {
        _value = MIN_REFRESH_TIME;
      }

      _value = Math.Min(_value, MAX_REFRESH_TIME);
      _value = Math.Max(_value, MIN_REFRESH_TIME);

      return _value;

    }

    //------------------------------------------------------------------------------
    // PURPOSE:  Get all steps from Advertisement
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    // 
    // IMPORTANT: NOW THERE AREN'T IMPLEMENTATION FOR STEPS,
    //            STEP[] ARE ONY ITEM ARRAY WITH THE SAME ADVERTISEMENT ;)
    // WKT_ADS.ad_id = WKT_AD_STEPS.as_ad_id
    // WKT_AD_STEPS.as_step_id = WKT_AD_STEP_DETAILS.ascr_step_id

    private static DataRow[] GetAllSteps(DataRow Ad)
    {
      DataRow[] _steps;

      // Trick at this moment there isn't details!
      _steps = new DataRow[] { Ad };

      foreach (DataRow _row in _steps)
      {
        _row["StepDuration"] = GetDefaultRefreshTime();
      }

      return _steps;
    }

    //------------------------------------------------------------------------------
    // PURPOSE:  Set the current Step each DefaultRefreshTime
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    //
    private static void AdvertisementThread()
    {

      DataTable _ads;
      DataRow[] _steps;
      int _elapsed;
      int _remaining;

      try
      {
        m_lock.AcquireWriterLock(Timeout.Infinite);
        m_current_step = null;
        m_current_step_tick = Misc.GetTickCount();
        m_current_step_duration = 0;
        m_lock.ReleaseWriterLock();

        while (true)
        {
          if (!IsPromoBoxEnabled())
          {
            m_lock.AcquireWriterLock(Timeout.Infinite);
            m_current_step = null;
            m_lock.ReleaseWriterLock();

            System.Threading.Thread.Sleep(30000);
            continue;
          }

          _ads = GetUpperMonitorContents();

          foreach (DataRow _ad in _ads.Rows)
          {
            if (!IsPromoBoxEnabled())
            {
              break;
            }
            _steps = GetAllSteps(_ad);

            foreach (DataRow _step in _steps)
            {
              m_lock.AcquireReaderLock(Timeout.Infinite);
              _elapsed = (int)Misc.GetElapsedTicks(m_current_step_tick);
              _remaining = Math.Max(0, m_current_step_duration - _elapsed);
              m_lock.ReleaseReaderLock();

              if (_remaining > 0)
              {
                // Wait till the current step finishes
                Thread.Sleep(_remaining);
              }

              m_lock.AcquireWriterLock(Timeout.Infinite);
              m_current_step = _step;
              m_current_step_tick = Misc.GetTickCount();
              m_current_step_duration = (Int32)_step["StepDuration"];
              m_lock.ReleaseWriterLock();
              // Notify readers, a new step is available
              m_new_step_available.Set();
            }
          }
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE:  Clone datarow
    //
    //  PARAMS:
    //      - INPUT: Row to clone
    //
    //      - OUTPUT:
    //
    // RETURNS: New DataRow as copy of input row.

    private static DataRow Clone(DataRow Row)
    {
      DataTable _tmp_table;

      _tmp_table = Row.Table.Clone();

      return _tmp_table.Rows.Add(Row.ItemArray);
    }

    //------------------------------------------------------------------------------
    // PURPOSE:  Query to General Params if Promobox is enabled
    //
    //  PARAMS:
    //      - INPUT: Row to clone
    //
    //      - OUTPUT:
    //
    // RETURNS: whether the PromoBOX is enabled or not in the Wigos System

    private static Boolean IsPromoBoxEnabled()
    {
      return (GeneralParam.Value("WigosKiosk", "Enabled") == "1");
    }
  } // WKT_Advertisement
}
