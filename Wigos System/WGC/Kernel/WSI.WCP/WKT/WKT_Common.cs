//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WKT_Common.cs
// 
//   DESCRIPTION: WKT_Common class
// 
//        AUTHOR: Andreu Juli�
// 
// CREATION DATE: 18-MAY-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 18-MAY-2012 AJQ    First version.
// 17-DEC-2012 JMM    Gifts split info added.
// 06-NOV-2013 RMS & RCI Check if there are resources in GetResourceInfo
// 27-NOV-2015 SGB    Product Backlog Item 6050: Add General Params in params promobox
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Data.SqlClient;
using WSI.Common;
using WSI.WCP;
using System.Xml;
using System.Xml.Serialization;
using System.Runtime.InteropServices;
using System.Data;
using System.IO;

public static partial class WCP_WKT
{
  public class WKT_Image : IXml
  {
    Int32 m_image_id;
    Int64 m_resource_id;

    public Int32 ImageId
    {
      get { return m_image_id; }
    }

    public Int64 ResourceId
    {
      get { return m_resource_id; }
    }

    public WKT_Image()
    {
      m_image_id = 0;
      m_resource_id = 0;
    }

    public WKT_Image(Int32 ImageId, Int64 ResourceId)
    {
      m_image_id = ImageId;
      m_resource_id = ResourceId;
    }

    #region IXml Members

    public string ToXml()
    {
      return ToXml(new StringBuilder()).ToString();
    }

    public void LoadXml(XmlReader Xml)
    {
      m_image_id = Int32.Parse(XML.ReadTagValue(Xml, "ImageId"));
      m_resource_id = Int64.Parse(XML.ReadTagValue(Xml, "ResourceId"));
    }

    #endregion

    public StringBuilder ToXml(StringBuilder StrBuilder)
    {
      StrBuilder.Append("<ImageId>" + ImageId.ToString() + "</ImageId>");
      StrBuilder.Append("<ResourceId>" + ResourceId.ToString() + "</ResourceId>");

      return StrBuilder;
    } //ToXml
  } // WKT_Image

  public class WKT_Images : List<WKT_Image>
  {
    #region IXml Members

    public string ToXml()
    {
      StringBuilder _sb;
      _sb = new StringBuilder();

      foreach (WKT_Image _img_info in this)
      {
        _sb.Append("<Image>");
        _img_info.ToXml(_sb);
        _sb.Append("</Image>");
      }

      return _sb.ToString();
    }  // ToXml()

    public void LoadXml(XmlReader Xml)
    {
      WKT_Image _image_info;

      while (true)
      {
        if (!Xml.Read())
        {
          throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_WKT_Common Image List element missing");
        }
        if (Xml.Name == "Image" && Xml.NodeType.Equals(XmlNodeType.Element))
        {
          _image_info = new WKT_Image();

          _image_info.LoadXml(Xml);

          this.Add(_image_info);

          // Consume the End Tag of <Image>
          Xml.Read();
        }
        else if (Xml.Name != "Image" && Xml.NodeType.Equals(XmlNodeType.EndElement))
        {
          // Reader positioned to expected ending node images list ( "</Images>" )
          // No more Image elements.
          break;
        }
      }
    } //LoadXml

    #endregion
  } //WKT_Images

  public class WKT_ResourceInfo : IXml
  {
    Int64 m_resource_id;
    String m_extension;
    Int32 m_length;
    Byte[] m_hash;

    public WKT_ResourceInfo()
    {
      m_resource_id = 0;
      m_extension = "";
      m_length = 0;
      m_hash = null;
    } //WKT_ResourceInfo()

    public WKT_ResourceInfo(Int64 ResourceId, String Extension, Byte[] Hash, Int32 Length)
    {
      m_resource_id = ResourceId;
      m_extension = Extension;
      m_length = Length;
      m_hash = Hash;
    } // WKT_ResourceInfo(...)

    public Int64 Resource_id
    {
      get { return m_resource_id; }
      set { m_resource_id = value; }
    }
    public String Extension
    {
      get { return m_extension; }
      set { m_extension = value; }
    }
    public Int32 Length
    {
      get { return m_length; }
      set { m_length = value; }
    }
    public Byte[] Hash
    {
      get { return m_hash; }
      set { m_hash = value; }
    }

    #region IXml Members

    public string ToXml()
    {
      return ToXml(new StringBuilder()).ToString();
    } // ToXml

    public void LoadXml(System.Xml.XmlReader Xml)
    {
      Resource_id = long.Parse(XML.ReadTagValue(Xml, "ResourceId"));
      Extension = XML.ReadTagValue(Xml, "Extension");
      Hash = Convert.FromBase64String(XML.ReadTagValue(Xml, "Hash"));
      Length = Int32.Parse(XML.ReadTagValue(Xml, "DataLength"));
    } // LoadXml

    #endregion

    public StringBuilder ToXml(StringBuilder StrBuilder)
    {
      StrBuilder.Append("<ResourceId>" + Resource_id.ToString() + "</ResourceId>");
      StrBuilder.Append("<Extension>" + XML.StringToXmlValue(Extension) + "</Extension>");
      StrBuilder.Append("<Hash>" + Convert.ToBase64String(Hash) + "</Hash>");
      StrBuilder.Append("<DataLength>" + Length.ToString() + "</DataLength>");

      return StrBuilder;
    } //ToXml

  } //WKT_ResourceInfo

  public class WKT_ResourceInfoList : List<WKT_ResourceInfo>
  {
    #region IXml Members

    public string ToXml()
    {
      StringBuilder _sb;
      _sb = new StringBuilder();

      foreach (WKT_ResourceInfo _res_info in this)
      {
        _sb.Append("<ResourceInfo>");
        _res_info.ToXml(_sb);
        _sb.Append("</ResourceInfo>");
      }

      return _sb.ToString();
    }  // ToXml()

    public void LoadXml(XmlReader Xml)
    {
      WKT_ResourceInfo _res_info;

      while (true)
      {
        if (!Xml.Read())
        {
          throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_WKT_Common Resources Gifts List element missing");
        }
        if (Xml.Name == "ResourceInfo" && Xml.NodeType.Equals(XmlNodeType.Element))
        {
          _res_info = new WKT_ResourceInfo();

          _res_info.LoadXml(Xml);

          this.Add(_res_info);

          // Consume the End Tag of <ResourceInfo>
          Xml.Read();
        }
        else if (Xml.Name != "ResourceInfo" && Xml.NodeType.Equals(XmlNodeType.EndElement))
        {
          // Reader positioned to expected ending node resources list ( "</Resources>" )
          // No more ResourceInfo elements.
          break;
        }
      }
    } //LoadXml

    #endregion
  } // WKT_ResourceInfoList

  public class WKT_Message : IXml
  {
    Int64 m_msg_id;
    String m_msg_text;

    public WKT_Message()
    {
      m_msg_id = 0;
      m_msg_text = "";
    } //WKT_Message()

    public WKT_Message(Int64 MsgId, String MsgText)
    {
      m_msg_id = MsgId;
      m_msg_text = MsgText;
    } // WKT_Message(...)

    public Int64 Msg_id
    {
      get { return m_msg_id; }
      set { m_msg_id = value; }
    }
    public String Msg_Text
    {
      get { return m_msg_text; }
      set { m_msg_text = value; }
    }

    public static String Msg_Id_Prefix
    {
      get { return "Msg."; }
    }

    #region IXml Members

    public string ToXml()
    {
      return ToXml(new StringBuilder()).ToString();
    } // ToXml

    public void LoadXml(System.Xml.XmlReader Xml)
    {
      Msg_id = long.Parse(XML.ReadTagValue(Xml, "MsgId"));
      Msg_Text = XML.ReadTagValue(Xml, "MsgText");
    } // LoadXml    

    public StringBuilder ToXml(StringBuilder StrBuilder)
    {
      StrBuilder.Append("<MsgId>" + Msg_id.ToString() + "</MsgId>");
      StrBuilder.Append("<MsgText>" + XML.StringToXmlValue(Msg_Text) + "</MsgText>");

      return StrBuilder;
    } //ToXml

    #endregion

    public static Int32 GetIdFromGeneralParamKey(String GeneralParamKey)
    {
      Int32 _msg_id;

      _msg_id = 0;

      Int32.TryParse(GeneralParamKey.Substring(WKT_Message.Msg_Id_Prefix.Length), out _msg_id);

      return _msg_id;
    }

  } //WKT_Message

  public class WKT_MessageList : List<WKT_Message>
  {
    #region IXml Members

    public string ToXml()
    {
      StringBuilder _sb;
      _sb = new StringBuilder();

      foreach (WKT_Message _message in this)
      {
        _sb.Append("<Message>");
        _message.ToXml(_sb);
        _sb.Append("</Message>");
      }

      return _sb.ToString();
    }  // ToXml()

    public void LoadXml(XmlReader Xml)
    {
      WKT_Message _message;

      while (true)
      {
        if (!Xml.Read())
        {
          throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_WKT_Common Message element missing");
        }
        if (Xml.Name == "Message" && Xml.NodeType.Equals(XmlNodeType.Element))
        {
          _message = new WKT_Message();

          _message.LoadXml(Xml);

          this.Add(_message);

          // Consume the End Tag of <Message>
          Xml.Read();
        }
        else if (Xml.Name != "Message" && Xml.NodeType.Equals(XmlNodeType.EndElement))
        {
          // Reader positioned to expected ending node resources list ( "</Message>" )
          // No more Message elements.
          break;
        }
      }
    } //LoadXml

    #endregion
  } // WKT_MessageList

  public class WKT_ScheduledTime : IXml
  {
    Int32 m_from_time;
    Int32 m_to_time;

    public WKT_ScheduledTime()
    {
      m_from_time = 0;
      m_to_time = 0;
    } //WKT_ScheduledTime()

    public WKT_ScheduledTime(Int32 FromTime, Int32 ToTime)
    {
      m_from_time = FromTime;
      m_to_time = ToTime;
    } // WKT_ScheduledTime(...)

    public Int32 From_Time
    {
      get { return m_from_time; }
      set { m_from_time = value; }
    }
    public Int32 To_Time
    {
      get { return m_to_time; }
      set { m_to_time = value; }
    }

    #region IXml Members

    public string ToXml()
    {
      return ToXml(new StringBuilder()).ToString();
    } // ToXml

    public StringBuilder ToXml(StringBuilder StrBuilder)
    {
      StrBuilder.Append("<FromTime>" + From_Time.ToString() + "</FromTime>");
      StrBuilder.Append("<ToTime>" + To_Time.ToString() + "</ToTime>");

      return StrBuilder;
    } //ToXml

    public void LoadXml(System.Xml.XmlReader Xml)
    {
      From_Time = Int32.Parse(XML.ReadTagValue(Xml, "FromTime"));
      To_Time = Int32.Parse(XML.ReadTagValue(Xml, "ToTime"));
    } // LoadXml

    #endregion

    public static Int32 GeneralParamToScheduleTime(String GeneralParam)
    {
      Int32 _aux_hours;
      Int32 _aux_mins;
      Int32 _schedule_time;

      _schedule_time = 0;

      //Time is stored in HHMM format, so the string length should be 4.  
      //In any other case, the return value will be 0
      if (GeneralParam.Length != 4)
      {
        return _schedule_time;
      }

      if (Int32.TryParse(GeneralParam.Substring(0, 2), out _aux_hours)
        && Int32.TryParse(GeneralParam.Substring(2, 2), out _aux_mins))
      {
        _schedule_time = (Int32)DateTime.Today.AddHours(_aux_hours).AddMinutes(_aux_mins).TimeOfDay.TotalSeconds;
      }

      return _schedule_time;
    } // GeneralParamToScheduleTime

  } //WKT_ScheduledTime

  public class WKT_GiftsSplit : IXml
  {
    Boolean m_enabled;
    String m_category_name_01;
    String m_category_name_02;
    String m_category_name_03;
    String m_category_short_01;
    String m_category_short_02;
    String m_category_short_03;

    public WKT_GiftsSplit()
    {
      m_enabled = false;
      m_category_name_01 = "";
      m_category_name_02 = "";
      m_category_name_03 = "";
      m_category_short_01 = "";
      m_category_short_02 = "";
      m_category_short_03 = "";

    } //WKT_GiftsSplit()

    public WKT_GiftsSplit(Boolean Enabled,
                          String CategoryName01,
                          String CategoryName02,
                          String CategoryName03,
                          String CategoryShort01,
                          String CategoryShort02,
                          String CategoryShort03)
    {
      m_enabled = Enabled;
      m_category_name_01 = CategoryName01;
      m_category_name_02 = CategoryName02;
      m_category_name_03 = CategoryName03;
      m_category_short_01 = CategoryShort01;
      m_category_short_02 = CategoryShort02;
      m_category_short_03 = CategoryShort03;

    } // WKT_GiftsSplit(...)

    public Boolean Enabled
    {
      get { return m_enabled; }
      set { m_enabled = value; }
    }

    public String CategoryName01
    {
      get { return m_category_name_01; }
      set { m_category_name_01 = value; }
    }

    public String CategoryName02
    {
      get { return m_category_name_02; }
      set { m_category_name_02 = value; }
    }

    public String CategoryName03
    {
      get { return m_category_name_03; }
      set { m_category_name_03 = value; }
    }

    public String CategoryShort01
    {
      get { return m_category_short_01; }
      set { m_category_short_01 = value; }
    }

    public String CategoryShort02
    {
      get { return m_category_short_02; }
      set { m_category_short_02 = value; }
    }

    public String CategoryShort03
    {
      get { return m_category_short_03; }
      set { m_category_short_03 = value; }
    }

    #region IXml Members

    public string ToXml()
    {
      return ToXml(new StringBuilder()).ToString();
    } // ToXml

    public StringBuilder ToXml(StringBuilder StrBuilder)
    {
      StrBuilder.Append("<Enabled>" + Enabled.ToString() + "</Enabled>");
      StrBuilder.Append("<CategoryName01>" + XML.StringToXmlValue(CategoryName01) + "</CategoryName01>");
      StrBuilder.Append("<CategoryName02>" + XML.StringToXmlValue(CategoryName02) + "</CategoryName02>");
      StrBuilder.Append("<CategoryName03>" + XML.StringToXmlValue(CategoryName03) + "</CategoryName03>");
      StrBuilder.Append("<CategoryShort01>" + XML.StringToXmlValue(CategoryShort01) + "</CategoryShort01>");
      StrBuilder.Append("<CategoryShort02>" + XML.StringToXmlValue(CategoryShort02) + "</CategoryShort02>");
      StrBuilder.Append("<CategoryShort03>" + XML.StringToXmlValue(CategoryShort03) + "</CategoryShort03>");

      return StrBuilder;
    } //ToXml

    public void LoadXml(System.Xml.XmlReader Xml)
    {
      Enabled = Boolean.Parse(XML.ReadTagValue(Xml, "Enabled"));
      CategoryName01 = XML.ReadTagValue(Xml, "CategoryName01");
      CategoryName02 = XML.ReadTagValue(Xml, "CategoryName02");
      CategoryName03 = XML.ReadTagValue(Xml, "CategoryName03");
      CategoryShort01 = XML.ReadTagValue(Xml, "CategoryShort01");
      CategoryShort02 = XML.ReadTagValue(Xml, "CategoryShort02");
      CategoryShort03 = XML.ReadTagValue(Xml, "CategoryShort03");

    } // LoadXml

    #endregion

  } //WKT_ScheduledTime


  //------------------------------------------------------------------------------
  // PURPOSE: Login Current User by Public Track Data and PIN
  // 
  //  PARAMS:
  //      - INPUT:
  //        - TrackData: Public Track Data
  //        - PIN:       Pin
  //
  //      - OUTPUT:
  //        - AccountId: Account Id
  //        - ErrorCode: Result of verify login
  // RETURNS:
  //    - true:   Executed Correctly and User Card and Pin are correct
  //    - false:  User Card or Pin are correct
  // 
  //   NOTES:
  //
  public static bool WKT_PlayerLogin(String ExternalTrackData, String PIN, out Int64 AccountId, out WCP_ResponseCodes ErrorCode)
  {
    String _sql_txt;
    String _internal_track_data;
    Int32 _dummy;

    ErrorCode = WCP_ResponseCodes.WCP_RC_WKT_ERROR;
    AccountId = 0;

    try
    {
      // is card valid --> Not found WCP_RC_WKT_CARD_NOT_VALID
      if (!CardNumber.TrackDataToInternal(ExternalTrackData, out _internal_track_data, out _dummy))
      {
        ErrorCode = WCP_ResponseCodes.WCP_RC_WKT_CARD_NOT_VALID;

        return false;
      }

      using (DB_TRX _db_trx = new DB_TRX())
      {
        _sql_txt = "";
        _sql_txt += "SELECT   AC_ACCOUNT_ID       ";
        _sql_txt += "  FROM   ACCOUNTS            ";
        _sql_txt += " WHERE   AC_TRACK_DATA = @pIntTrackData";

        using (SqlCommand _sql_cmd = new SqlCommand(_sql_txt))
        {
          Object _obj;

          _sql_cmd.Parameters.Add("@pIntTrackData", SqlDbType.NVarChar, 50).Value = _internal_track_data;
          _obj = _db_trx.ExecuteScalar(_sql_cmd);

          if (_obj == null)
          {
            ErrorCode = WCP_ResponseCodes.WCP_RC_WKT_CARD_NOT_VALID;

            return false;
          }

          AccountId = (Int64)_obj;
        }
      }

      switch (Accounts.DB_CheckAccountPIN(AccountId, PIN))
      {
        case PinCheckStatus.OK:
          ErrorCode = WCP_ResponseCodes.WCP_RC_WKT_OK;
          return true;

        case PinCheckStatus.WRONG_PIN:
          ErrorCode = WCP_ResponseCodes.WCP_RC_WKT_WRONG_PIN;
          break;

        case PinCheckStatus.ACCOUNT_BLOCKED:
          ErrorCode = WCP_ResponseCodes.WCP_RC_WKT_ACCOUNT_BLOCKED;
          break;

        case PinCheckStatus.ACCOUNT_NO_PIN:
          ErrorCode = WCP_ResponseCodes.WCP_RC_WKT_ACCOUNT_NO_PIN;
          break;

        case PinCheckStatus.ERROR:
        default:
          ErrorCode = WCP_ResponseCodes.WCP_RC_WKT_ERROR;
          break;
      } // switch (_pin_check_status)
    }
    catch
    {
      ErrorCode = WCP_ResponseCodes.WCP_RC_WKT_ERROR;
    }

    return false;
  } //WKT_PlayerLogin

  //------------------------------------------------------------------------------
  // PURPOSE: Get resource info of id List, also returns a list of id res. not found
  // 
  //  PARAMS:
  //      - INPUT:
  //        - ResourceId: List of Resources Id to return
  //
  //      - OUTPUT:
  //        - Resources: Resource info
  // RETURNS:
  //    - List: Resources could not been found
  //    
  //   NOTES:
  //
  public static Boolean GetResourceInfo(List<Int64> ResourceId, out WKT_ResourceInfoList ResourceInfo, out List<Int64> NotFound)
  {
    StringBuilder _sb;
    Boolean _table_created;

    // Default --> No one found
    //  - Info     = empty list
    //  - NotFound = input list
    ResourceInfo = new WKT_ResourceInfoList();
    NotFound = new List<Int64>(ResourceId);

    // RMS & RCI 06-NOV-2013: If no resources, return ok.
    if (ResourceId.Count == 0)
    {
      return true;
    }

    _table_created = false;

    try
    {
      _sb = new StringBuilder();

      using (DB_TRX _db_trx = new DB_TRX())
      {
        try
        {
          _sb.Length = 0;
          _sb.AppendLine("CREATE TABLE #TMP_RESOURCES (TMP_RESOURCE_ID BIGINT NOT NULL)");

          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString()))
          {
            if (_db_trx.ExecuteNonQuery(_sql_cmd) != -1)
            {
              ResourceInfo.Clear();
              Log.Warning("GetResourceInfo: Error in CREATE TABLE #TMP_RESOURCES. Returning an empty list.");

              return false;
            }
          }

          _table_created = true;

          _sb.Length = 0;
          _sb.AppendLine("  INSERT INTO #TMP_RESOURCES VALUES (@pResourceId); ");

          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString()))
          {
            _sql_cmd.Parameters.Add("@pResourceId", SqlDbType.BigInt).SourceColumn = "RES_RESOURCE_ID";
            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
            {
              DataTable _table = new DataTable();
              DataRow _row;

              _table.Columns.Add("RES_RESOURCE_ID", Type.GetType("System.Int64"));

              foreach (Int64 _id in ResourceId)
              {
                _row = _table.NewRow();
                _row[0] = _id;
                _table.Rows.Add(_row);
              }

              _sql_da.UpdateBatchSize = 500;
              _sql_da.InsertCommand = _sql_cmd;
              _sql_da.InsertCommand.UpdatedRowSource = UpdateRowSource.None;

              if (_db_trx.Update(_sql_da, _table) != ResourceId.Count)
              {
                ResourceInfo.Clear();
                Log.Warning("GetResourceInfo: Error in INSERT INTO #TMP_RESOURCES. Returning an empty list.");

                return false;
              }
            }
          }

          _sb.Length = 0;
          _sb.AppendLine("    SELECT   RES_RESOURCE_ID");
          _sb.AppendLine("           , RES_EXTENSION");
          _sb.AppendLine("           , RES_HASH");
          _sb.AppendLine("           , RES_LENGTH");
          _sb.AppendLine("      FROM   WKT_RESOURCES");
          _sb.AppendLine("INNER JOIN   #TMP_RESOURCES");
          _sb.AppendLine("        ON   RES_RESOURCE_ID = TMP_RESOURCE_ID");

          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString()))
          {
            using (SqlDataReader _reader = _db_trx.ExecuteReader(_sql_cmd))
            {
              while (_reader.Read())
              {
                NotFound.Remove(_reader.GetInt64(0));
                ResourceInfo.Add(new WKT_ResourceInfo(_reader.GetInt64(0), _reader.GetString(1), _reader.GetSqlBytes(2).Value, _reader.GetInt32(3)));
              }
            }
          }
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }
        finally
        {
          if (_table_created)
          {
            _sb.Length = 0;
            _sb.AppendLine("DROP TABLE #TMP_RESOURCES ");

            using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString()))
            {
              _db_trx.ExecuteNonQuery(_sql_cmd);
            }
          }

          _db_trx.Rollback();
        }

        return true;

      } //using _db_trx
    }
    catch (Exception _ex)
    {
      ResourceInfo.Clear();

      Log.Exception(_ex);
    }

    return false;
  } //GetResourceInfo

  //------------------------------------------------------------------------------
  // PURPOSE: Extract Id List from Table
  //
  //  PARAMS:
  //      - INPUT:
  //          - Table           : DataTable where to extract id_resources
  //          - Columns[]       : Array of columns of id_resource 
  //          - ResourceIdList  : List Resulted      
  //
  //      - OUTPUT:
  //          - ResourceIdList  : List Resulted      
  //
  private static void GetResourceIdFromTable(DataTable Table, Int32[] Columns, List<Int64> ResourceIdList)
  {
    Int64 _res_id;

    foreach (DataRow _row in Table.Rows)
    {
      foreach (Int32 _col_idx in Columns)
      {
        if (!_row.IsNull(_col_idx))
        {
          _res_id = (Int64)_row[_col_idx];
          if (_res_id != 0)
          {
            ResourceIdList.Add(_res_id);
          }
        }
      }
    }
  } // GetResourceIdFromTable

  //------------------------------------------------------------------------------
  // PURPOSE: Logs all resources not founds, example:
  //                  "Could not find resource: RRRRRR. Referenced on FFFFFF: [PPPPP], [PPPPP]...
  //          Also sets the not found id resources to 0 in the table.
  //  PARAMS:
  //      - INPUT:
  //          - Table             : Table where are stored resources not founds
  //          - Columns           : Columns where are stored the Resource Id
  //          - NotFoundResources : List of Resource Id not founds, RRRRRR.
  //          - ItemName          : Item which reference the Resource, used to show on Log String, FFFFFF.
  //          - ItemFields        : Columns of Table showed on the log, PPPPP
  //
  private static void TestResourceIdNotFound(DataTable Table, Int32[] Columns, List<Int64> NotFoundResources, String ItemName, Int32[] ItemFields)
  {
    String _item_fields_str;
    Int32 _num_founds;

    if (NotFoundResources.Count == 0)
    {
      return;
    }

    foreach (DataRow _item in Table.Rows)
    {
      //Foreach resource not found
      foreach (Int64 _resource_id in NotFoundResources)
      {
        _num_founds = 0;
        foreach (Int32 _column in Columns)
        {
          if (!_item.IsNull(_column) && (Int64)_item[_column] == _resource_id)
          {
            _item[_column] = 0;
            _num_founds++;
          }
        }
        if (_num_founds > 0)
        {
          //Define Error Message of each Item
          _item_fields_str = "";
          foreach (Int32 _col_desc in ItemFields)
          {
            _item_fields_str += _item_fields_str.Length > 0 ? ", " + _item[_col_desc].ToString() : ": " + _item[_col_desc].ToString();
          }
          Log.Warning("Could not find resource: " + _resource_id.ToString() + ". Referenced on " + ItemName + _item_fields_str + ".");
        }
        // If all item columns are verified, move to next item.
        if (_num_founds == Columns.Length)
        {
          break;
        }
      }
    }
  } //TestResourceIdNotFound

  //------------------------------------------------------------------------------
  // PURPOSE : Check whether a functionality is enabled or not
  // 
  //  PARAMS :
  //      - INPUT :
  //          - Functionality : Functionality to be checked out
  //
  //      - OUTPUT :
  //
  // RETURNS :
  //      - TRUE: functionality enabled
  //      - FALSE : functionality not enabled
  // 
  //   NOTES :

  public static bool WKT_Functionality(PromoBOX.Functionality Functionality)
  {
    List<PromoBOX.Functionality> _func_list;

    using (DB_TRX _trx = new DB_TRX())
    {
      _func_list = WSI.Common.PromoBOX.ReadPromoBoxFunc(Functionality, true, _trx.SqlTransaction);

      return (_func_list.Count > 0);
    }

  } // WKT_Functionality

  //------------------------------------------------------------------------------
  // PURPOSE : Gets if a functionality is in schedule
  // 
  //  PARAMS :
  //      - INPUT :
  //          - Functionality : Functionality resquested
  //
  //      - OUTPUT :
  //
  // RETURNS :
  //      - TRUE : functionality in schedule and it is allowed to run
  //      - FALSE : functionality not in schedule and it is not allowed to run
  // 
  //   NOTES :

  public static bool WKT_FunctionalitySchedule(PromoBOX.Functionality Functionality)
  {
    List<PromoBOX.Functionality> _func_list;
    Int32 _from_time;
    Int32 _to_time;
    String _aux_key_value;

    _from_time = 0;
    _to_time = 0;

    using (DB_TRX _trx = new DB_TRX())
    {
      _func_list = WSI.Common.PromoBOX.ReadPromoBoxFunc(Functionality, true, _trx.SqlTransaction);

      if (_func_list.Count == 0)
      {
        // Requested functionality is not limited by schedule => go forward with the request
        return true;
      }

      // Schedule time from
      _aux_key_value = GeneralParam.Value("WigosKiosk", "Schedule.From");
      _from_time = WKT_ScheduledTime.GeneralParamToScheduleTime(_aux_key_value);

      // Schedule time to
      _aux_key_value = GeneralParam.Value("WigosKiosk", "Schedule.To");
      _to_time = WKT_ScheduledTime.GeneralParamToScheduleTime(_aux_key_value);
    }

    return ScheduleDay.IsTimeInRange(DateTime.Now.TimeOfDay, _from_time, _to_time);
  } // WKT_FunctionalitySchedule
}
