
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Threading;
using System.Data.SqlClient;
using WSI.Common;
using WSI.WCP;

public static partial class WCP_WKT
{
  public static void WKT_ProcessRequest(WCP_Message WktRequest, WCP_Message WktResponse)
  {
    Int32 _gp_value;

    // WigosKiosk.Enabled?
    if ( !Int32.TryParse (GeneralParam.Value ("WigosKiosk", "Enabled"), out _gp_value))
    {
      _gp_value = 0;

      Log.Warning("WigosKiosk.Enabled has an incorrect value");
    }

    if ( _gp_value != 1 )
    {
      WktResponse.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_WKT_DISABLED;
      WktResponse.MsgHeader.ResponseCodeText = "WigosKiosk is not enabled";

      Log.Warning("WigosKiosk.Enabled is false.");

      return;
    }

    switch ( WktRequest.MsgHeader.MsgType ) 
    { 
      case WCP_MsgTypes.WCP_WKT_MsgGetParameters:
        WKT_ProcessGetParameters(WktRequest, WktResponse);
      break;

      case WCP_MsgTypes.WCP_WKT_MsgGetResource:
        WKT_ProcessGetResource(WktRequest, WktResponse);
      break;

      case WCP_MsgTypes.WCP_WKT_MsgGetPlayerGifts:
        WKT_ProcessGetPlayerGifts (WktRequest, WktResponse);
      break;

      case WCP_MsgTypes.WCP_WKT_MsgPlayerRequestGift:
        WKT_ProcessPlayerRequestGift(WktRequest, WktResponse);
      break;

      case WCP_MsgTypes.WCP_WKT_MsgGetNextAdvStep:
        WKT_ProcessGetNextAdvStep(WktRequest, WktResponse);
      break;

      case WCP_MsgTypes.WCP_WKT_MsgSetPlayerFields:
        WKT_ProcessSetPlayerFields(WktRequest, WktResponse);
        break;

      case WCP_MsgTypes.WCP_WKT_MsgGetPlayerInfo:
        WKT_ProcessGetPlayerInfoFields(WktRequest, WktResponse);
      break;
      
      case WCP_MsgTypes.WCP_WKT_MsgGetPlayerPromos:
        WKT_ProcessGetPlayerPromos(WktRequest, WktResponse);
      break;
      
      case WCP_MsgTypes.WCP_WKT_MsgGetPlayerActivity:
        WKT_ProcessGetPlayerActivity(WktRequest, WktResponse);
      break;

      case WCP_MsgTypes.WCP_WKT_MsgPrintRechargesList:
        WKT_ProcessPrintRechargesList(WktRequest, WktResponse);
      break;

      case WCP_MsgTypes.WCP_WKT_MsgGetPlayerDraws:
        WKT_ProcessGetPlayerDraws(WktRequest, WktResponse);
      break;

      case WCP_MsgTypes.WCP_WKT_MsgRequestPlayerDrawNumbers:
        WKT_ProcessRequestPlayerDrawNumbers (WktRequest, WktResponse);
      break;

      case WCP_MsgTypes.WCP_WKT_MsgPlayerRequestPromo:
        WKT_ProcessPlayerRequestPromo(WktRequest, WktResponse);
      break;

      case WCP_MsgTypes.WCP_LCD_MsgGetParameters:
        LCD_ProcessGetParameters(WktRequest, WktResponse);
      break;

      case WCP_MsgTypes.WCP_LCD_MsgGetPromoBalance:
        LCD_ProcessGetPromoBalance(WktRequest, WktResponse);
      break;

      default :
        WktResponse.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_WKT_MESSAGE_NOT_SUPPORTED;
        WktResponse.MsgHeader.ResponseCodeText = "WKT Message not supported.";

        return;
    } // switch (WktRequest.MsgHeader.MsgType)
  }
}
