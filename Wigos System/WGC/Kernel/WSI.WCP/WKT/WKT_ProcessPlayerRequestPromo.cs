//------------------------------------------------------------------------------
// Copyright Â© 2013 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WKT_ProcessPlayerRequestPromo.cs
// 
//   DESCRIPTION: WKT_ProcessPlayerRequestPromo class
// 
//        AUTHOR: Javier Molina
// 
// CREATION DATE: 10-OCT-2013
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 10-OCT-2013 JMM    First version.
// 09-DEC-2014 JMM    Added by recharges promotions to the promotion list sent to the WKT
// 26-FEB-2015 AMF    Fixed Bug WIG-2100: Show terminal data
// 17-JUL-2015 YNM    Fixed Bug WIG-2330: Promobox collection error 
// 22-OCT-2015 SDS    Add promotion by played - Backlog Item 3856
// 23-NOV-2015 FOS    Bug 6771:Payment tickets & handpays
// 27-NOV-2015 SGB    Product Backlog Item 4713: Return ticket TITO
// 14-JAN-2015 ETP    BUG fixed 7591: Can't applicate promotions in SAS_HOST
// 19-FEB-2016 ETP    Product Backlog Item 9761:Dual Currency: Promociones y canjes de puntos por crédito desde el Display Touch.
// 01-DIC-2016 FAV    PBI 20407: TITO Ticket: Calculated fields
// 13-DIC-2016 FJC    Bug 21271:Promobox: No se muestra correctamente el terminal en "Reporte de tickets"
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Threading;
using System.Data.SqlClient;
using WSI.Common;
using WSI.WCP;
using System.Security.Cryptography;

public static partial class WCP_WKT
{
  private static void WKT_ProcessPlayerRequestPromo(WCP_Message WktRequest, WCP_Message WktResponse)
  {
    WCP_WKT_MsgPlayerRequestPromo _request;
    WCP_WKT_MsgPlayerRequestPromoReply _response;

    Int64 _promo_id;

    Int64 _account_id;
    String _trackdata;
    String _pin;

    OperationCode _operation_code;
    Int64 _operation_id;
    AccountPromotion _account_promo;
    CashierSessionInfo _cashier_session_info;
    AccountMovementsTable _account_movements;
    CashierMovementsTable _cashier_movements;
    Promotion.PROMOTION_STATUS _promo_status;
    DataTable _account_flags;
    Currency _cash_in;
    Currency _current_spent;
    Currency _current_played;
    DataTable _dt_remaining_spent_per_provider;
    Int32 _num_tokens;
    Currency _promo_reward;
    Currency _terminal_currency_reward;
    Currency _won_lock;
    Currency _spent_used;
    Promotion.TYPE_PROMOTION_DATA _promo_data;
    RechargeOutputParameters _output_params;
    WCP_ResponseCodes _rsp_code;
    Currency _msg_promo_reward;
    ACCOUNT_PROMO_CREDIT_TYPE _msg_promo_credit_type;
    TerminalTypes _terminal_type;
    MultiPromos.AccountBalance _balance;
    Decimal _player_points;
    Boolean _gp_print_redeemable;
    Boolean _gp_print_no_redeemable;

    _request = (WCP_WKT_MsgPlayerRequestPromo)WktRequest.MsgContent;
    _response = (WCP_WKT_MsgPlayerRequestPromoReply)WktResponse.MsgContent;

    _response.VoucherList = new ArrayList();

    try
    {
      _promo_id = _request.PromoId;
      _msg_promo_reward = _request.PromoReward;
      _msg_promo_credit_type = (ACCOUNT_PROMO_CREDIT_TYPE)_request.PromoCreditType;

      _trackdata = _request.Player.Trackdata;
      _pin = _request.Player.Pin;
      _operation_id = 0;
      _operation_code = OperationCode.NA_PROMOTION;
      _cash_in = 0;
      _num_tokens = 0;

      //Login Ok?
      if (!WKT_PlayerLogin(_trackdata, _pin, out _account_id, out _rsp_code))
      {
        WktResponse.MsgHeader.ResponseCode = _rsp_code;

        return;
      }

      //Check current time is in schedule
      if (!WKT_FunctionalitySchedule(PromoBOX.Functionality.SCHEDULE_PROMOTIONS))
      {
        WktResponse.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_WKT_FUNCTIONALITY_OFF_SCHEDULE;

        return;
      }



      using (DB_TRX _db_trx = new DB_TRX())
      {
        String _terminal_name;
        String _terminal_external_id;
        Int32 _terminal_id;
        WCP_AccountManager.WCP_Account _account;
        RechargeInputParameters _input_params;
        Int32 _last_hours_to_look_up_for_recharges;
        GU_USER_TYPE _gu_user_type;
        FlagBucketId _flags_buckets;
        WSI.Common.Terminal.TerminalInfo _terminal_info;

        _account_promo = new AccountPromotion();
        _cashier_session_info = new CashierSessionInfo();

        _terminal_external_id = WktRequest.MsgHeader.TerminalId;

        if (!WSI.Common.Terminal.Trx_GetTerminalInfoByExternalId(_terminal_external_id, out _terminal_info, _db_trx.SqlTransaction))
        {
          WktResponse.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_WKT_ERROR;

          return;
        }

        _terminal_id = _terminal_info.TerminalId;
        _terminal_name = _terminal_info.Name;
        _terminal_type = _terminal_info.TerminalType;

        if (_terminal_type == TerminalTypes.PROMOBOX && WSI.Common.TITO.Utils.IsTitoMode())
        {
          _gp_print_redeemable = (ACCOUNT_PROMO_CREDIT_TYPE)_request.PromoCreditType == ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE
                             && GeneralParam.GetBoolean("WigosKiosk", "Promotions.RE.PrintTITOTicket", false);

          _gp_print_no_redeemable = ((ACCOUNT_PROMO_CREDIT_TYPE)_request.PromoCreditType == ACCOUNT_PROMO_CREDIT_TYPE.NR1
                                   || (ACCOUNT_PROMO_CREDIT_TYPE)_request.PromoCreditType == ACCOUNT_PROMO_CREDIT_TYPE.NR2)
                                   && GeneralParam.GetBoolean("WigosKiosk", "Promotions.NR.PrintTITOTicket", false);

          if ((_gp_print_redeemable || _gp_print_no_redeemable))
          {
            if (!_request.TitoPrinterIsReady)
            {
              WktResponse.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_WKT_TITO_PRINTER_ERROR;

              return;
            }
          }

        }

        // Read Account
        _account = WCP_AccountManager.Account(_trackdata, _db_trx.SqlTransaction);
        if (_account == null)
        {
          throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_CARD_NOT_VALID);
        }
        if (_account.type == AccountType.ACCOUNT_UNKNOWN)
        {
          throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_CARD_NOT_VALID);
        }

        if (!Promotion.ReadPromotionData(_db_trx.SqlTransaction, _promo_id, _account_promo))
        {
          Log.Error("WKT_ProcessPlayerRequestPromo. Promotion not found: " + _promo_id + ".");

          throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR, "Promotion not found.");
        }

        _promo_data = _account_promo;

        //Get promotion reward amount
        if (!Promotion.GetActiveFlags(_account_id, _db_trx.SqlTransaction, out  _account_flags))
        {
          throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR, "GetActiveFlags error");
        }

        _last_hours_to_look_up_for_recharges = GeneralParam.GetInt32("ManualPromotions", "LastHoursToGetRecharges", 24);

        Promotion.GetLastUnpromotionedRecharges(_account_id, _last_hours_to_look_up_for_recharges, _db_trx.SqlTransaction, out _cash_in);

        _dt_remaining_spent_per_provider = Promotion.AccountRemainingDataPerProvider(_account_id, _db_trx.SqlTransaction);

        //SDS 22-10-2015 calculate played & spent per terminal in one function only
        Promotion.CalculateDataByProvider(_dt_remaining_spent_per_provider, null, out _current_spent, out _current_played);

        _promo_status = Promotion.CheckPromotionApplicable(_db_trx.SqlTransaction,
                                                           _promo_id,
                                                           _account_id,
                                                           true,
                                                           _account_flags,
                                                           ref _cash_in,
                                                           _current_spent,
                                                           _current_played,
                                                           ref _num_tokens,
                                                           ref _promo_data,
                                                           out _promo_reward,
                                                           out _won_lock,
                                                           out _spent_used,
                                                           WGDB.Now);

        if (_promo_status != Promotion.PROMOTION_STATUS.PROMOTION_OK
          && _promo_status != Promotion.PROMOTION_STATUS.PROMOTION_REDUCED_REWARD)
        {
          //Account can't add this promotion
          WktResponse.MsgHeader.ResponseCode = WCP_ResponseCodes.WKT_PROMO_NOT_CURRENTLY_APPLICABLE;

          return;
        }

        _terminal_currency_reward = CurrencyExchange.GetExchange(_promo_reward, CurrencyExchange.GetNationalCurrency(), _terminal_info.CurrencyCode, _db_trx.SqlTransaction);

        if (_msg_promo_reward != _terminal_currency_reward
         || _msg_promo_credit_type != _promo_data.credit_type)
        {
          //Read promo data haven't match received promo data
          WktResponse.MsgHeader.ResponseCode = WCP_ResponseCodes.WKT_PROMO_NOT_CURRENTLY_APPLICABLE;

          return;
        }



        switch (_terminal_type)
        {
          case TerminalTypes.PROMOBOX:
          case TerminalTypes.WIN_UP:
            {
              _gu_user_type = Misc.GetUserTypePromobox();
            }
            break;
          case TerminalTypes.SAS_HOST:
            {
              _gu_user_type = Misc.IsNoteAcceptorEnabled() ? GU_USER_TYPE.SYS_ACCEPTOR : GU_USER_TYPE.SYS_SYSTEM;
            }
            break;
          default:
            {
              Log.Message(String.Format("ProcessRequestGift: Invalid TerminalType ({0})", _terminal_type));

              return;
            }
        }

        _cashier_session_info = Cashier.GetSystemCashierSessionInfo(_gu_user_type, _db_trx.SqlTransaction, _terminal_name);
        if (_cashier_session_info == null)
        {
          throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR, "Cashier Session Info not available.");
        }

        //Insert operation
        if (!Operations.DB_InsertOperation(_operation_code, _account_id, _cashier_session_info.CashierSessionId, 0, 0,
                                           _promo_id, _promo_reward, 0, 0, 0, 0, string.Empty, out _operation_id, _db_trx.SqlTransaction))
        {
          throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR, "Operation not available.");
        }

        _account_movements = new AccountMovementsTable(_cashier_session_info, _gu_user_type == GU_USER_TYPE.SYS_ACCEPTOR ? _terminal_id : 0, _terminal_name);
        _cashier_movements = new CashierMovementsTable(_cashier_session_info, _terminal_name);

        _input_params = new RechargeInputParameters();
        _input_params.AccountId = _account_id;
        _input_params.AccountLevel = _account.level;
        _input_params.CardPaid = true;
        _input_params.CardPrice = 0;
        _input_params.ExternalTrackData = _trackdata;
        _input_params.VoucherAccountInfo = _account.VoucherAccountInfo(_trackdata);
        _input_params.TerminalType = _terminal_type;
        _input_params.TerminalId = _terminal_id;

        if (!Accounts.DB_AddPromotion(_input_params, _operation_id, _account_movements, _cashier_movements, _promo_id, _promo_reward,
                                         _spent_used, _db_trx.SqlTransaction, out _output_params))
        {
          if (_output_params.ErrorCode == RECHARGE_ERROR_CODE.ERROR_RECHARGE_WITH_WITHOLDING)
          { 
            throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR,
                                String.Format("Can't apply promotion.  Withholding is Needed. AccountId {0}, PromoId {1}.", _account_id, _promo_id));
          }
          else
          throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_ERROR,
                                  String.Format("Can't apply promotion. AccountId {0}, PromoId {1}.", _account_id, _promo_id));
        }



        _flags_buckets = FlagBucketId.NR;

        //ETP 10-FEB-2016 Include flags for NR and RE Buckets
        if (!WSI.Common.TITO.Utils.IsTitoMode())
        {
          _flags_buckets |= FlagBucketId.RE;
        }


        if (!MultiPromos.Trx_UpdateAccountBalance(_account_id, MultiPromos.AccountBalance.Zero, 0, _flags_buckets, out _balance, out _player_points, _db_trx.SqlTransaction))
        {
          WktResponse.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_WKT_ERROR;

          return;
        }

        _balance = CurrencyExchange.GetExchange(_balance, CurrencyExchange.GetNationalCurrency(), _terminal_info.CurrencyCode, _db_trx.SqlTransaction);

        _db_trx.Commit();


        if (_terminal_type == TerminalTypes.PROMOBOX)
        {
          _response.VoucherList = _output_params.VoucherList;
        }

        if (_output_params.TicketCreated != null)
        {
          _output_params.TicketCreated.Address1 = WSI.Common.TITO.Utils.GetValueAddres(_terminal_info, 1);
          _output_params.TicketCreated.Address2 = WSI.Common.TITO.Utils.GetValueAddres(_terminal_info, 2);
          _response.Ticket = _output_params.TicketCreated;
        }

        _response.PromoBalance = new MultiPromos.PromoBalance(_balance, _player_points);


      }
      WktResponse.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_WKT_OK;
    }
    catch (Exception _ex)
    {
      Log.Exception(_ex);
      WktResponse.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_WKT_ERROR;
    }
  }
}
