//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WKT_ProcessPrintRechargesList.cs
// 
//   DESCRIPTION: WKT_ProcessPrintRechargesList class
// 
//        AUTHOR: Javier Molina
// 
// CREATION DATE: 29-OCT-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 29-OCT-2012 JMM    Initial reelase.
// 15-MAR-2013 ICS    Added functionality of one cashier session for each PromoBOX.
// 17-JUL-2015 YNM    Fixed Bug WIG-2330: Promobox collection error 
// 23-NOV-2015 FOS    Bug 6771:Payment tickets & handpays
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Threading;
using System.Data.SqlClient;
using WSI.Common;
using WSI.WCP;
using System.Security.Cryptography;

public static partial class WCP_WKT
{ 
  private static void WKT_ProcessPrintRechargesList(WCP_Message WktRequest, WCP_Message WktResponse)
  {
    WCP_WKT_MsgPrintRechargesList _request;
    WCP_WKT_MsgPrintRechargesListReply _response;

    String _trackdata;
    String _pin;
    List<String> _notes_str_list;
    Currency _aux_note_value;
    Currency _total_recharges;
    Int64 _operation_id;
    CardData _card_data;

    ArrayList _vouchers_list;

    WCP_ResponseCodes _rsp_code;
    Int64 _account_id;

    String _terminal_name;
    String _terminal_external_id;
    Int32 _terminal_id;
    String _provider_id;
    String _server_external_id;
    TerminalStatus _status;
    Currency _sas_account_denom;

    CashierSessionInfo _cashier_session_info;

    _request = (WCP_WKT_MsgPrintRechargesList)WktRequest.MsgContent;
    _response = (WCP_WKT_MsgPrintRechargesListReply)WktResponse.MsgContent;

    WktResponse.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_WKT_ERROR;

    _response.VoucherList = new ArrayList();
    _notes_str_list = new List<String>();

    try
    {
      _trackdata = _request.Player.Trackdata;
      _pin = _request.Player.Pin;

      //Login Ok?
      if (!WKT_PlayerLogin(_trackdata, _pin, out _account_id, out _rsp_code))
      {
        WktResponse.MsgHeader.ResponseCode = _rsp_code;

        return;
      }

      foreach (double _note_value in _request.NotesList)
      {
        _aux_note_value = (Decimal)_note_value;
        _notes_str_list.Add(_aux_note_value.ToString());
      }

      _total_recharges = (Decimal)_request.TotalRecharges;

      using (DB_TRX _db_trx = new DB_TRX())
      {
        _terminal_external_id = WktRequest.MsgHeader.TerminalId;

        // Get terminal information
        if (!Misc.GetTerminalInfo(_terminal_external_id, 1, out _terminal_id, out _provider_id, out _terminal_name,
                                  out _status, out _server_external_id, out _sas_account_denom, _db_trx.SqlTransaction))
        {          
          _db_trx.Rollback();

          return;
        }

        _cashier_session_info = Cashier.GetSystemCashierSessionInfo(Misc.GetUserTypePromobox(), _db_trx.SqlTransaction, _terminal_name);

        if (!Operations.DB_InsertOperation(OperationCode.PROMOBOX_TOTAL_RECHARGES_PRINT,
                                           _account_id,
                                           _cashier_session_info.CashierSessionId,
                                           0,                                           
                                           _total_recharges,
                                           0,
                                           0,
                                           0,
                                           0,                      // Operation Data
                                           0, string.Empty,
                                           out _operation_id,
                                           _db_trx.SqlTransaction))
        {
          _db_trx.Rollback();

          return;
        }

        //Get Card Data        
        _card_data = new CardData();
        CardData.DB_CardGetAllData(_account_id, _card_data);

        _vouchers_list = VoucherBuilder.RechargesSummary(_card_data.VoucherAccountInfo(),
                                                         _notes_str_list.ToArray(),
                                                         _total_recharges,
                                                         _account_id,
                                                         _operation_id,
                                                         PrintMode.Print,
                                                         _db_trx.SqlTransaction);

        _db_trx.Commit();
      }

      _response.VoucherList = _vouchers_list;

      WktResponse.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_WKT_OK;
    }
    catch (Exception _ex)
    {
      Log.Exception(_ex);
      WktResponse.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_WKT_ERROR;
    }
  }
}
