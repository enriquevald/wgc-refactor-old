//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WKT_ProcessGetPlayerInfoFields
// 
//   DESCRIPTION: Kiosk player info fields
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 09-JUL-2012 XIT    First version.
// 15-ENE-2014 JML    Product Backlog Item 151:AccountPointsCache --> Task 156: Promobox & DisplayLCD read
// 07-JUN-2016 JMM    Fixed bug 13990: Missing recharges while gaming day is enabled
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using WSI.WCP;
using WSI.Common;

public static partial class WCP_WKT
{
  //------------------------------------------------------------------------------
  // PURPOSE: Process Tx related to WCP_MsgTypes.WCP_WKT_MsgGetPlayerInfo
  // 
  //  PARAMS:
  //      - INPUT:
  //          - Input message with fields to update ( Message contains WKT_PlayerFieldsTable ) 
  //
  //      - OUTPUT:
  //          - Output message with same table with Status fields filled.
  //
  //   NOTES:

  private static void WKT_ProcessGetPlayerInfoFields(WCP_Message WktRequest, WCP_Message WktResponse)
  {
    WCP_WKT_MsgGetPlayerInfo       _request;
    WCP_WKT_MsgGetPlayerInfoReply  _response;
    WKT_PlayerFieldsTable          _user_fields;
    WKT_PlayerInfo                 _player_info;
    WKT_Player                     _player;
    long                           _account_id;
    WCP_ResponseCodes              _rc;
    int                            _terminal_id;
    String                         _provider_id;
    String                         _terminal_name;
    String                         _server_external_id;
    TerminalStatus                 _status;
    Currency                       _sas_account_denom;

    try
    {
      _request =  (WCP_WKT_MsgGetPlayerInfo) WktRequest.MsgContent;
      _response = (WCP_WKT_MsgGetPlayerInfoReply) WktResponse.MsgContent;
     
      using (DB_TRX _db_trx = new DB_TRX())
      {
        _player = new WKT_Player();
        _player.Pin = _request.Player.Pin;
        _player.Trackdata = _request.Player.Trackdata;

        _player.Login(out _account_id, out _rc);
        WktResponse.MsgHeader.ResponseCode = _rc;

        if ( WktResponse.MsgHeader.ResponseCode != WCP_ResponseCodes.WCP_RC_WKT_OK )
        {
          return;
        }

        if (!Misc.GetTerminalInfo(WktRequest.MsgHeader.TerminalId, (Int32)WCP_TerminalType.GamingTerminal, out _terminal_id, out _provider_id, out _terminal_name,
                                  out _status, out _server_external_id, out _sas_account_denom, _db_trx.SqlTransaction))
        {
          return;
        }

        // Obtain player info and player fields     
        if ( ! WKT_PlayerInfo.Read(_account_id, _terminal_name, _db_trx.SqlTransaction, out _player_info)
          || ! WKT_PlayerFieldsTable.WKT_ReadPlayerFields(_account_id, out _user_fields, _db_trx.SqlTransaction) )
        {
          throw new Exception("Can't process WKT information request");
        }
        else
        {
          // 15-ENE-2014 JML    Product Backlog Item 151:AccountPointsCache --> Task 156: Promobox & DisplayLCD read
          _db_trx.Commit(); 
          
          _response.PlayerInfo = _player_info;
          _response.PlayerFields = _user_fields;
          WktResponse.MsgContent = _response;
        }
       }
    }
    catch (Exception _ex)
    {
      Log.Exception(_ex);
      WktResponse.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_WKT_ERROR;
    }
  }
}
