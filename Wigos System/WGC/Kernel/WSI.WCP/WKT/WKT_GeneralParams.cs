﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WKT_GeneralParams.cs
// 
//   DESCRIPTION: WKT_GeneralParams class
// 
//        AUTHOR: Samuel González
// 
// CREATION DATE: 02-DIC-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 02-DIC-2015 SGB    First version.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using WSI.Common;
using WSI.WCP;
using System.Xml;
using System.Xml.Serialization;

public static partial class WCP_WKT
{
  public class WKT_GeneralParams : IXml
  {
    String m_key;
    String m_value;

    public WKT_GeneralParams()
    {
      m_key = "";
      m_value = "";
    } //WKT_GeneralParams()

    public WKT_GeneralParams(String Key, String Value)
    {
      m_key = Key;
      m_value = Value;
    } // WKT_GeneralParams(...)

    public String Key
    {
      get { return m_key; }
      set { m_key = value; }
    }
    public String Value
    {
      get { return m_value; }
      set { m_value = value; }
    }

    #region IXml Members

    public string ToXml()
    {
      return ToXml(new StringBuilder()).ToString();
    } // ToXml

    public void LoadXml(System.Xml.XmlReader Xml)
    {
      Key = XML.ReadTagValue(Xml, "K");
      Value = XML.ReadTagValue(Xml, "V");

    } // LoadXml    

    public StringBuilder ToXml(StringBuilder StrBuilder)
    {
      StrBuilder.Append("<K>" + XML.StringToXmlValue(Key) + "</K><V>" + XML.StringToXmlValue(Value) + "</V>");
      return StrBuilder;
    } //ToXml

    #endregion
  }

  public class WKT_GeneralParamsList : List<WKT_GeneralParams>
  {
    public WKT_GeneralParamsList Copy()
    {
      return (WKT_GeneralParamsList)this.MemberwiseClone();
    } // Copy

    public string ToXml()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      foreach (WKT_GeneralParams _general_param in this)
      {
        _sb.Append("<Item>");
        _general_param.ToXml(_sb);
        _sb.Append("</Item>");
      }

      return _sb.ToString();
    }  // ToXml()

    public void LoadXml(XmlReader Xml)
    {
      WKT_GeneralParams _general_param;

      while (true)
      {
        if (!Xml.Read())
        {
          throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_WKT_Common Param element missing");
        }
        if (Xml.Name == "Item" && Xml.NodeType.Equals(XmlNodeType.Element))
        {
          _general_param = new WKT_GeneralParams();

          _general_param.LoadXml(Xml);

          this.Add(_general_param);

          // Consume the End Tag of <Item>
          Xml.Read();
        }
        else if (Xml.Name != "Item" && Xml.NodeType.Equals(XmlNodeType.EndElement))
        {
          // Reader positioned to expected ending node resources list ( "</Item>" )
          // No more Message elements.
          break;
        }
      }

      // Update GeneralParamList
      if (this.Count > 0)
      {
        GeneralParam.ReloadGPFromPromobox(this.ConvertToGeneralParamDictionary());
      }

      GeneralParam.SetDataAvaiable();

    } //LoadXml

    public Boolean ContainsGeneralParam(WKT_GeneralParams WktGeneralParam)
    {
      foreach (WKT_GeneralParams _item in this)
      {
        if (_item.Key == WktGeneralParam.Key)
        {
          return true;
        }
      }
      return false;
    }

    public GeneralParam.Dictionary ConvertToGeneralParamDictionary()
    {
      GeneralParam.Dictionary _dictionary;

      _dictionary = new GeneralParam.Dictionary();

      foreach (WKT_GeneralParams _item in this)
      {
        if (!_dictionary.ContainsKey(_item.Key))
        {
          _dictionary.Add(_item.Key, _item.Value);
        }
      }

      return _dictionary;
    } // ConvertToGeneralParamDictionary

    public Boolean IsEmpty()
    {
      if (this.Count > 0)
      {
        return false;
      }

      return true;
    } // GeneralParamsEmpty

    public void GetGeneralParamItem(String Group, String Key, out String Value, out Boolean Exists)
    {
      Exists = false;
      Value = "";

      foreach (WKT_GeneralParams _general_param in this)
      {
        if (_general_param.Key == Group + "." + Key)
        {
          Exists = true;
          Value = _general_param.Value;

          break;
        }
      }
    } // GetGeneralParamItem

  } // WKT_GeneralParamsList

}
