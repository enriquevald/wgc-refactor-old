//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WKT_ProcessGetResource.cs
// 
//   DESCRIPTION: WKT_ProcessGetResource class
// 
//        AUTHOR: Susana Sams�
// 
// CREATION DATE: 18-MAY-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 18-MAY-2012 SSC    First version.
// 31-JUL-2012 XIT    Added Interface + Find class
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Threading;
using System.Data.SqlClient;
using WSI.Common;
using WSI.WCP;
using System.Security.Cryptography;
using System.Collections;

public static partial class WCP_WKT
{

  public interface IVouchers
  {
    ArrayList VoucherList { get; set;}
  }

  public interface IResources
  {
    WCP_WKT.WKT_ResourceInfoList Resources { get; set;}
  }


  //------------------------------------------------------------------------------
  // PURPOSE: Obtain the Resource that matches the specified filter
  //   
  //         
  // RETURNS: True or false according to the filter
  // 
  // NOTES:
  //
  public class FindResourceInfo
  {
    private readonly Int64 Id;

    public FindResourceInfo(Int64 FoundId)
    {
      this.Id = FoundId;
    }

    public Boolean FindById(WCP_WKT.WKT_ResourceInfo GiftResources)
    {
      return GiftResources.Resource_id == Id;
    }
  }



  private static void WKT_ProcessGetResource(WCP_Message WktRequest, WCP_Message WktResponse)
  {
    const int MAX_CHUNK_SIZE = 5 * 1024 * 1024;
    const int MIN_CHUNK_SIZE = 8 * 1024;

    WCP_WKT_MsgGetResource _request;
    WCP_WKT_MsgGetResourceReply _response;

    Int64 _resource_id;
    String _extension;
    Byte[] _hash;
    Byte[] _chunk_data;
    Int32 _offset;
    Int32 _max_chunk_size;
    Int32 _desired_chunk_size;
    Int32 _num_chunks;
    Int32 _resource_length;

    StringBuilder _sql_txt;

    // Initializations
    _resource_id = 0;
    _extension = "";
    _hash = new Byte[0];
    _chunk_data = new Byte[0];

    try
    {

      _request = (WCP_WKT_MsgGetResource)WktRequest.MsgContent;
      _response = (WCP_WKT_MsgGetResourceReply)WktResponse.MsgContent;

      _resource_id = _request.WktResourceId;

      _max_chunk_size = Math.Min(MAX_CHUNK_SIZE, _request.ChunkDataLength);
      _max_chunk_size = Math.Max(0, _max_chunk_size);
      if (_max_chunk_size == 0)
      {
        _max_chunk_size = MAX_CHUNK_SIZE;
      }

      using (DB_TRX _db_trx = new DB_TRX())
      {

        // Read Resource Info
        _sql_txt = new StringBuilder();

        _sql_txt.AppendLine(" SELECT   RES_EXTENSION                  ");
        _sql_txt.AppendLine("        , RES_LENGTH                     ");
        _sql_txt.AppendLine("        , RES_HASH                       ");
        _sql_txt.AppendLine("   FROM   WKT_RESOURCES                  ");
        _sql_txt.AppendLine("  WHERE   RES_RESOURCE_ID = @pResourceId ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sql_txt.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
        {
          _sql_cmd.Parameters.Add("@pResourceId", SqlDbType.Int).Value = _resource_id;

          using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
          {
            if (!_reader.Read())
            {
              WktResponse.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_WKT_RESOURCE_NOT_FOUND;

              return;
            }

            _extension = _reader.GetString(0);
            _resource_length = _reader.GetInt32(1);
            // If length = 0 --> RES_HASH is null
            if (_resource_length > 0)
            {
              _hash = (Byte[])_reader.GetSqlBinary(2);
            }
          }
        }

        // Get offset
        _offset = Math.Max(0, _request.Offset);
        _offset = Math.Min(_resource_length, _offset);

        // Get max chunk size
        _max_chunk_size = Math.Min(MAX_CHUNK_SIZE, _resource_length);
        _num_chunks = (_resource_length + _max_chunk_size - 1) / _max_chunk_size;
        _max_chunk_size = _max_chunk_size / _num_chunks;

        // Get desired chunk size
        if (_request.ChunkDataLength == 0)
        {
          _desired_chunk_size = _max_chunk_size;
        }
        else
        {
          _desired_chunk_size = Math.Max(MIN_CHUNK_SIZE, _request.ChunkDataLength);
          _desired_chunk_size = Math.Min(_max_chunk_size, _desired_chunk_size);
        }

        int _fin;
        int _bytes_to_read;

        _fin = Math.Min(_resource_length, _offset + _desired_chunk_size);
        _bytes_to_read = Math.Min(_desired_chunk_size, _fin - _offset);


        if (_bytes_to_read > 0)
        {
          // Read ChunkData (from Offset, MaxChunkSize)    
          _sql_txt = new StringBuilder();

          _sql_txt.AppendLine(" SELECT   SUBSTRING (RES_DATA,  @pOffset, @pBytesToRead) ");
          _sql_txt.AppendLine("   FROM   WKT_RESOURCES                                  ");
          _sql_txt.AppendLine("  WHERE   RES_RESOURCE_ID = @pResourceId                 ");

          using (SqlCommand _sql_cmd = new SqlCommand(_sql_txt.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pOffset", SqlDbType.Int).Value = _offset;
            _sql_cmd.Parameters.Add("@pBytesToRead", SqlDbType.Int).Value = _bytes_to_read;
            _sql_cmd.Parameters.Add("@pResourceId", SqlDbType.Int).Value = _resource_id;

            using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
            {
              if (!_reader.Read())
              {
                WktResponse.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_WKT_ERROR;

                return;
              }

              _chunk_data = (Byte[])_reader.GetSqlBinary(0);

            } // using _reader
          } // using _sql_cmd

        } //end if

      }

      _response.WktResourceId = _resource_id;
      _response.Extension = _extension;
      _response.DataLength = _resource_length;
      _response.Offset = _offset;
      if (_resource_length > 0)
      {
        _response.Hash = _hash;
        _response.ChunkDataLength = _chunk_data.Length;
        _response.ChunkData = _chunk_data;
      }

      WktResponse.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_WKT_OK;

    }
    catch (Exception _ex)
    {
      Log.Exception(_ex);
      WktResponse.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_WKT_ERROR;
    }

  }
}
