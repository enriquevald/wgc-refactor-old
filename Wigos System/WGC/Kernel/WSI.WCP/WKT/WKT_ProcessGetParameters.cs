//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WKT_ProcessGetPlayerGifts.cs
// 
//   DESCRIPTION: WKT_ProcessGetPlayerGifts class
// 
//        AUTHOR: Joaquim Cid
// 
// CREATION DATE: 30-MAY-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 30-MAY-2012 JCM    First version.
// 17-DEC-2012 JMM    Gifts split info added.
// 24-NOV-2015 SGB    Feature 4708:PromoBOX + TITO Printer
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Threading;
using System.Data.SqlClient;
using WSI.Common;
using WSI.WCP;

public static partial class WCP_WKT
{
  private const String WKT_GP_GROUP_KEY = "WigosKiosk";
  private static WKT_GeneralParamsList m_general_param_list = new WKT_GeneralParamsList();
  private static List<Int32> m_promobox_sent_gp_list = new List<Int32>();
  private static ReaderWriterLock m_rw_lock_promobox_sent_gp_list = new ReaderWriterLock();
  private static ReaderWriterLock m_rw_lock_general_param_list = new ReaderWriterLock();

  // General Params to send to Promobox
  private static String[][] GENERAL_PARAMS_PROMOBOX = { 
    new String[] { "TITO", "Tickets.Location" } ,
    new String[] { "TITO", "Tickets.Address1" } ,
    new String[] { "TITO", "Tickets.Address2" },
    new String[] { "TITO", "TicketInfo.LblSequence" },
    new String[] { "TITO", "TicketInfo.LblMachine" },
    new String[] { "TITO", "TicketInfo.LblExpiration" },
    new String[] { "TITO", "TicketInfo.LblValidation" },
    new String[] { "TITO", "TicketInfo.FormatDate" },
    new String[] { "TITO", "TicketInfo.FormatTime" },
    new String[] { "TITO", "TicketTemplate" },
    new String[] { "TITO", "CashableTickets.Title" },
    new String[] { "TITO", "PromotionalTickets.Redeemable.Title" },
    new String[] { "TITO", "PromotionalTickets.NonRedeemable.Title" },
    new String[] { "WigosKiosk", "Promotions.NR.PrintTITOTicket" },
    new String[] { "WigosKiosk", "Promotions.RE.PrintTITOTicket" },
    new String[] { "Site", "SystemMode" },
    new String[] { "Cage", "Enabled" }

  };

  private static void WKT_ProcessGetParameters(WCP_Message WktRequest, WCP_Message WktResponse)
  {
    WCP_WKT_MsgGetParametersReply _response;
    WCP_WKT_MsgGetParameters _request;
    StringBuilder _sb;
    String _aux_key_value;
    String _aux_subject_key;
    List<Int32> _list_message_id;
    _request = (WCP_WKT_MsgGetParameters)WktRequest.MsgContent;
    _response = (WCP_WKT_MsgGetParametersReply)WktResponse.MsgContent;

    _list_message_id = new List<Int32>();
    _list_message_id.Clear();

    try
    {
      _response.Functionalities = new List<Int32>();
      _response.Images = new WKT_Images();
      _response.Resources = new WKT_ResourceInfoList();
      _response.Messages = new WKT_MessageList();
      _response.ScheduleTime = new WKT_ScheduledTime();
      _response.GiftsSplit = new WKT_GiftsSplit();
      _response.WKT_GeneralParams = new WKT_GeneralParamsList();

      using (DB_TRX _db_trx = new DB_TRX())
      {
        // Get 
        //    - Kiosk Functionalities (only enabled)
        //    - Kiosk Customized Images
        //    - Customized Messages
        //      Access to GENERAL_PARAMS table to obtain the Id of every customized message. 
        //      Message contents will be accessed in the ordinary way with GeneralParam.Value ()
        //    - Operation schedule interval

        _sb = new StringBuilder();
        _sb.AppendLine("    SELECT   FUN_FUNCTION_ID                    ");
        _sb.AppendLine("      FROM   WKT_FUNCTIONALITIES                ");
        _sb.AppendLine("     WHERE   FUN_ENABLED = 1                    ");
        _sb.AppendLine(" ;                                              ");
        _sb.AppendLine("    SELECT   RES_RESOURCE_ID                    ");
        _sb.AppendLine("           , RES_EXTENSION                      ");
        _sb.AppendLine("           , RES_HASH                           ");
        _sb.AppendLine("           , RES_LENGTH                         ");
        _sb.AppendLine("           , CIM_IMAGE_ID                       ");
        _sb.AppendLine("      FROM   WKT_RESOURCES                      ");
        _sb.AppendLine("INNER JOIN   WKT_IMAGES                         ");
        _sb.AppendLine("        ON   RES_RESOURCE_ID = CIM_RESOURCE_ID  ");
        _sb.AppendLine(" ;                                              ");
        _sb.AppendLine("    SELECT   GP_SUBJECT_KEY	                    ");
        _sb.AppendLine("      FROM   GENERAL_PARAMS                     ");
        _sb.AppendLine("     WHERE   GP_GROUP_KEY = 'WigosKiosk'        ");
        _sb.AppendLine("       AND   GP_SUBJECT_KEY like 'Msg.%'        ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString()))
        {
          using (SqlDataReader _sql_dr = _db_trx.ExecuteReader(_sql_cmd))
          {
            // Fill Functionalities
            while (_sql_dr.Read())
            {
              _response.Functionalities.Add(_sql_dr.GetInt32(0));
            }

            // Fill Custom Images, and Resources of this Images
            if (_sql_dr.NextResult())
            {
              while (_sql_dr.Read())
              {
                _response.Images.Add(new WKT_Image(_sql_dr.GetInt32(4), _sql_dr.GetInt64(0)));
                _response.Resources.Add(new WKT_ResourceInfo(_sql_dr.GetInt64(0), _sql_dr.GetString(1), _sql_dr.GetSqlBytes(2).Value, _sql_dr.GetInt32(3)));
              }
            }

            // Build a list with the Id of all customized messages
            if (_sql_dr.NextResult())
            {
              while (_sql_dr.Read())
              {
                _list_message_id.Add(WKT_Message.GetIdFromGeneralParamKey(_sql_dr.GetString(0)));
              }
            }
          }
        }
      } // Using DB_TRX


      //    - Customized Messages
      foreach (Int32 _msg_id in _list_message_id)
      {
        _aux_subject_key = "Msg." + _msg_id;
        _aux_key_value = GeneralParam.Value(WKT_GP_GROUP_KEY, _aux_subject_key);
        _response.Messages.Add(new WKT_Message(_msg_id, _aux_key_value));
      }

      //    - Operation schedule interval
      //          - Time From (amount of seconds from day start)
      //          - Time To (amount of seconds from day start)

      //          - Time From (amount of seconds from day start)
      _aux_key_value = GeneralParam.Value(WKT_GP_GROUP_KEY, "Schedule.From");
      _response.ScheduleTime.From_Time = WKT_ScheduledTime.GeneralParamToScheduleTime(_aux_key_value);

      //          - Time To (amount of seconds from day start)
      _aux_key_value = GeneralParam.Value(WKT_GP_GROUP_KEY, "Schedule.To");
      _response.ScheduleTime.To_Time = WKT_ScheduledTime.GeneralParamToScheduleTime(_aux_key_value);

      //    - Gifts split
      //          - Gifts split Enabled
      //          - Free - Play category name
      //          - Food & drinks category name
      //          - Gifts certificates category name

      //          - Gifts split Enabled
      _response.GiftsSplit.Enabled = GeneralParam.GetBoolean(WKT_GP_GROUP_KEY, "Gifts.SplitCategories");

      //          - Free - Play category name
      _response.GiftsSplit.CategoryName01 = GeneralParam.Value(WKT_GP_GROUP_KEY, "Gifts.CategoryName01");

      //          - Food & drinks category name
      _response.GiftsSplit.CategoryName02 = GeneralParam.Value(WKT_GP_GROUP_KEY, "Gifts.CategoryName02");

      //          - Gifts certificates category name
      _response.GiftsSplit.CategoryName03 = GeneralParam.Value(WKT_GP_GROUP_KEY, "Gifts.CategoryName03");

      //          - Free - Play category short
      _response.GiftsSplit.CategoryShort01 = GeneralParam.Value(WKT_GP_GROUP_KEY, "Gifts.CategoryShort01");

      //          - Food & drinks category short
      _response.GiftsSplit.CategoryShort02 = GeneralParam.Value(WKT_GP_GROUP_KEY, "Gifts.CategoryShort02");

      //          - Gifts certificates category short
      _response.GiftsSplit.CategoryShort03 = GeneralParam.Value(WKT_GP_GROUP_KEY, "Gifts.CategoryShort03");

      CheckStatePromobox(WktRequest, _response);
      
      WktResponse.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_WKT_OK;
    }
    catch (Exception _ex)
    {
      Log.Exception(_ex);

      _response.Functionalities = new List<Int32>();
      _response.Images = new WKT_Images();
      _response.Resources = new WKT_ResourceInfoList();
      _response.Messages = new WKT_MessageList();
      _response.ScheduleTime = new WKT_ScheduledTime();
      _response.GiftsSplit = new WKT_GiftsSplit();
      _response.WKT_GeneralParams = new WKT_GeneralParamsList();

      WktResponse.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_WKT_ERROR;
    }

  } //WKT_ProcessGetParameters

  private static void CheckStatePromobox(WCP_Message WktRequest, WCP_WKT_MsgGetParametersReply Response)
  {
    Boolean _changed;
    Boolean _send_general_params;
    Boolean _is_promobox;
    Int32 _terminal_id;
    WKT_GeneralParamsList _new_general_params_list;

    _is_promobox = false;

    if (WSI.Common.TITO.Utils.IsTitoMode())
    {
      if (!IsPromobox(WktRequest, out _is_promobox, out _terminal_id))
      {
        throw new Exception("Error IsPromobox.");
      }

      if (_is_promobox)
      {
        _send_general_params = false;
        _changed = false;

        LoadGeneralParams(out _new_general_params_list, out _changed);

        if (_changed)
        {

          // Update m_general_param_list
          try
          {

            m_rw_lock_general_param_list.AcquireWriterLock(Timeout.Infinite);
            m_general_param_list = _new_general_params_list;
          }
          finally
          {
            m_rw_lock_general_param_list.ReleaseWriterLock();
          }

          // GPs changed. Clear all cache PromoBOX list to reload GP for all PromoBOXes.
          try
          {
            m_rw_lock_promobox_sent_gp_list.AcquireWriterLock(Timeout.Infinite);
            m_promobox_sent_gp_list.Clear();
          }
          finally
          {
            m_rw_lock_promobox_sent_gp_list.ReleaseWriterLock();
          }
        }

        try
        {
          // If GPs changed, PromoBOX list is empty, so when changed always _send_general_params = true
          m_rw_lock_promobox_sent_gp_list.AcquireWriterLock(Timeout.Infinite);
          if (!m_promobox_sent_gp_list.Contains(_terminal_id))
          {
            m_promobox_sent_gp_list.Add(_terminal_id);
            _send_general_params = true;
          }
        }
        finally
        {
          m_rw_lock_promobox_sent_gp_list.ReleaseWriterLock();
        }

        if (_send_general_params)
        {

          try
          {
            m_rw_lock_general_param_list.AcquireReaderLock(Timeout.Infinite);
            Response.WKT_GeneralParams = m_general_param_list;
          }
          finally
          {
            m_rw_lock_general_param_list.ReleaseReaderLock();
          }
        }
      }
    }
  } // CheckStatePromobox

  private static void LoadGeneralParams(out WKT_GeneralParamsList NewGeneralParamsList, out Boolean Change)
  {
    String _general_param_value_new;
    String _general_param_value_old;
    WKT_GeneralParamsList _m_general_param_list_copy;

    NewGeneralParamsList = new WKT_GeneralParamsList();
    Change = false;

    try
    {
      m_rw_lock_general_param_list.AcquireReaderLock(Timeout.Infinite);
      _m_general_param_list_copy = m_general_param_list.Copy();
    }
    finally
    {
      m_rw_lock_general_param_list.ReleaseReaderLock();
    }

    foreach (String[] _general_param in GENERAL_PARAMS_PROMOBOX)
    {
      _general_param_value_new = GeneralParam.Value(_general_param[0], _general_param[1]); //[0]:Group [1]:Key
      WKT_GeneralParams _wkt_general_param;

      _wkt_general_param = new WKT_GeneralParams(_general_param[0] + "." + _general_param[1], _general_param_value_new);

      if (!NewGeneralParamsList.ContainsGeneralParam(_wkt_general_param))
      {
        NewGeneralParamsList.Add(_wkt_general_param);

        Boolean _exists;
        _m_general_param_list_copy.GetGeneralParamItem(_general_param[0], _general_param[1], out _general_param_value_old, out _exists);

        if (!_exists || _general_param_value_old != _general_param_value_new)
        {
          Change = true;
        }
      }
    }

    
  } // LoadGeneralParams

  private static Boolean IsPromobox(WCP_Message WktRequest, out Boolean IsPromobox, out Int32 TerminalId)
  {
    String _provider_id;
    String _terminal_name;
    String _server_external_id;
    TerminalStatus _status;
    TerminalTypes _terminal_type;
    Currency _sas_account_denom;

    IsPromobox = false;
    TerminalId = 0;
    _terminal_type = TerminalTypes.UNKNOWN;

    try
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        if (!Misc.GetTerminalInfo(WktRequest.MsgHeader.TerminalId, (Int32)WCP_TerminalType.GamingTerminal, out TerminalId, out _provider_id, out _terminal_name,
                                  out _status, out _server_external_id, out _sas_account_denom, _db_trx.SqlTransaction))
        {
          return false;
        }

        if (!WSI.Common.Terminal.Trx_GetTerminalType(TerminalId, _db_trx.SqlTransaction, out _terminal_type))
        {
          return false;
        }
      }
    }
    catch (Exception _ex)
    {
      Log.Exception(_ex);

      return false;
    }

    IsPromobox = _terminal_type == TerminalTypes.PROMOBOX;
    
    return true;

  } // IsPromobox

  //------------------------------------------------------------------------------
  // PURPOSE: Remove Promobox from List to send the GeneralParams to the Promobox.
  //
  //  PARAMS:
  //      - INPUT:
  //          - TerminalId       
  //
  //      - OUTPUT:
  //
  // RETURNS:
  //      - Boolean     : Removed / Not removed  
  //
  public static Boolean Promobox_SendGeneralParams(Int32 TerminalId)
  {
    Boolean _removed;

    _removed = false;

    try
    {
      m_rw_lock_promobox_sent_gp_list.AcquireWriterLock(Timeout.Infinite);
      if (m_promobox_sent_gp_list.Contains(TerminalId))
      {
        _removed = true;
        m_promobox_sent_gp_list.Remove(TerminalId);
      }
    }
    finally
    {
      m_rw_lock_promobox_sent_gp_list.ReleaseWriterLock();
    }

    return _removed;
  } // Promobox_SendGeneralParams
}