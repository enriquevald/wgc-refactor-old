//------------------------------------------------------------------------------
// Copyright � 2013 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WKT_ProcessRequestPlayerDrawNumbers.cs
// 
//   DESCRIPTION: WKT_ProcessRequestPlayerDrawNumbers class
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 06-MAY-2013 JMM    Initial reelase.
// 24-MAY-2013 LEM    Modified WKT_ProcessRequestPlayerDrawNumbers to create AccountMovementsTable and CashierMovementsTable with TerminalName.
// 17-JUL-2015 YNM    Fixed Bug WIG-2330: Promobox collection error 
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Threading;
using System.Data.SqlClient;
using WSI.Common;
using WSI.WCP;
using System.Security.Cryptography;

public static partial class WCP_WKT
{
  //------------------------------------------------------------------------------
  // PURPOSE : Manages the bussiness logic of WCP_WKT_MsgRequestPlayerDrawNumbers message
  //
  //  PARAMS :
  //      - INPUT :
  //          - WktRequest : Input message
  //
  //      - OUTPUT :
  //          - WktResponse : Message response to return
  //
  //      - RETURNS :

  private static void WKT_ProcessRequestPlayerDrawNumbers(WCP_Message WktRequest,
                                                           WCP_Message WktResponse)
  {
    WCP_WKT_MsgRequestPlayerDrawNumbers _request;
    WCP_WKT_MsgRequestPlayerDrawNumbersReply _response;
    WCP_ResponseCodes _response_code;

    Int64 _operation_id;
    Int64 _account_id;
    CardData _card_data;
    ArrayList _vouchers_list;
    Int32 _terminal_id;
    String _terminal_name;
    String _terminal_external_id;
    String _provider_id;
    String _server_external_id;
    TerminalStatus _status;
    DrawNumberList _draw_number_list;
    List<DrawTicket> _tickets;
    CashierSessionInfo _session_info;
    AccountMovementsTable _account_movement;
    CashierMovementsTable _cashier_movement;
    Currency _current_balance;
    Currency _sas_account_denom;

    _request = (WCP_WKT_MsgRequestPlayerDrawNumbers)WktRequest.MsgContent;
    _response = (WCP_WKT_MsgRequestPlayerDrawNumbersReply)WktResponse.MsgContent;

    WktResponse.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_WKT_ERROR;

    // Steps
    //      - Check the card's account and provided PIN
    //      - Check whether the functionality is enabled
    //      - Check that functionality is not restricted by schedule
    //      - Get Card Data
    //      - Get Cashier session
    //      - Get the available draws for that card 
    //      - Select only those draws that have pending tickets for that card 
    //      - Generate the tickets for the selected draws

    try
    {
      //    - Check the card's account and provided PIN
      if (!WKT_PlayerLogin(_request.m_player.Trackdata, _request.m_player.Pin, out _account_id, out _response_code))
      {
        WktResponse.MsgHeader.ResponseCode = _response_code;

        return;
      }

      //    - Check whether the functionality is enabled
      if ( ! WKT_Functionality (PromoBOX.Functionality.DRAW_NUMBERS) )
      {
        WktResponse.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_WKT_FUNCTIONALITY_NOT_ENABLED;

        return;
      }

      //    - Check that functionality is not restricted by schedule
      if ( ! WKT_FunctionalitySchedule(PromoBOX.Functionality.SCHEDULE_DRAW_NUMBERS) )
      {
        WktResponse.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_WKT_FUNCTIONALITY_OFF_SCHEDULE;

        return;
      }

      //    - Get Card Data
      _card_data = new CardData();
      CardData.DB_CardGetAllData (_account_id, _card_data);

      using (DB_TRX _db_trx = new DB_TRX())
      {
        _terminal_external_id = WktRequest.MsgHeader.TerminalId;

        //  - Get Cashier session
        //      : Get terminal name
        //      : Use terminal name to obtain the cashier session id

        //      : Get terminal name
        if (!Misc.GetTerminalInfo(_terminal_external_id,
                                     1,
                                     out _terminal_id,
                                     out _provider_id,
                                     out _terminal_name,
                                     out _status,
                                     out _server_external_id,
                                     out _sas_account_denom,
                                     _db_trx.SqlTransaction))
        {
          return;
        }

        //      : Use terminal name to obtain the cashier session id
        _session_info = Cashier.GetSystemCashierSessionInfo(Misc.GetUserTypePromobox(), _db_trx.SqlTransaction, _terminal_name);

        if (_session_info == null)
        {
          Log.Error("WKT_ProcessRequestPlayerDrawNumbers. GetSystemCashierSessionInfo error. TerminalName: " + _terminal_name + ".");

          return;
        }

        //  - Generate the tickets for the selected draws
        if (_request.m_draws_list[0] == 0)
        {
          // All pending numbers from any draw
          if (!Draws.CommonGenerateTickets(_card_data,
                                              false,        // Generate the numbers for ALL draws (OnlyCheck = FALSE)
                                              null,         // All pending numbers of any draw (DrawsToGenerate = NULL)
                                              _session_info.CashierSessionId,
                                              _db_trx.SqlTransaction,
                                              out _draw_number_list,
                                              out _tickets,
                                              out _operation_id))
          {
            return;
          }
        }
        else
        {
          DrawNumberList _aux_draw_numbers_list;

          // All pending numbers from those draws in the received list
          //    : Get the available draws for that card 
          //    : Match selected draws in _draw_number_list to build a shorter list 
          //    : Generate tickets for all pending numbers from the selected draws list

          //    : Get the available draws for that card 
          if (!Draws.CommonGenerateTickets(_card_data,
                                              true,       // Just obtain the draws list (OnlyCheck = TRUE)
                                              null,       // All pending numbers of any draw (DrawsToGenerate = NULL)
                                              _session_info.CashierSessionId,
                                              _db_trx.SqlTransaction,
                                              out _draw_number_list,
                                              out _tickets,
                                              out _operation_id))
          {
            return;
          }

          _aux_draw_numbers_list = new DrawNumberList();

          //    : Match selected draws in _draw_number_list to build a shorter list 
          foreach (int _draw_id in _request.m_draws_list)
          {
            foreach (DrawNumbers _aux_draw_numbers in _draw_number_list)
            {
              if (_aux_draw_numbers.Draw.DrawId == _draw_id)
              {
                _aux_draw_numbers_list.Add(_aux_draw_numbers);

                break;
              }
            }
          }

          //    : Generate all pending numbers from the selected draws list
          if (!Draws.CommonGenerateTickets(_card_data,
                                              false,       // Generate the numbers for draws in _aux_draw_numbers_list (OnlyCheck = FALSE)
                                              _aux_draw_numbers_list,
                                              _session_info.CashierSessionId,
                                              _db_trx.SqlTransaction,
                                              out _draw_number_list,
                                              out _tickets,
                                              out _operation_id))
          {
            return;
          }
        }

        //    : Generate tickets for all pending numbers from the selected draws list
        //        . Add an account movement for every generated ticket
        //        . Add a cashier movement for every generated ticket
        //        . 'INSERT' generated account movements 
        //        . 'INSERT' generated cashier movements 
        //        . Generate vouchers for all new tickets 

        _account_movement = new AccountMovementsTable(_session_info, 0, _terminal_name);
        _cashier_movement = new CashierMovementsTable(_session_info, _terminal_name);

        for (int _idx = 0; _idx < _tickets.Count; _idx++)
        {
          _current_balance = _card_data.CurrentBalance;

          //      . Add an account movement for every generated ticket
          if (!_account_movement.Add(_operation_id, _account_id, MovementType.DrawTicketPrint, _current_balance, 0, 0, _current_balance))
          {
            Log.Error("WKT_ProcessRequestPlayerDrawNumbers. AccountMovementsTable.Add() error. AccountId: " + _account_id + ".");

            return;
          }

          //      . Add a cashier movement for every generated ticket
          if (!_cashier_movement.Add(_operation_id, CASHIER_MOVEMENT.DRAW_TICKET_PRINT, 0, _account_id, ""))
          {
            Log.Error("WKT_ProcessRequestPlayerDrawNumbers. CashierMovementsTable.Add() error. AccountId: " + _account_id + ".");

            return;
          }
        }

        //        . 'INSERT' generated account movements 
        if (!_account_movement.Save(_db_trx.SqlTransaction))
        {
          Log.Error("WKT_ProcessRequestPlayerDrawNumbers. AccountMovementsTable.Save() error. AccountId: " + _account_id + ".");

          return;
        }

        //        . 'INSERT' generated cashier movements 
        if (!_cashier_movement.Save(_db_trx.SqlTransaction))
        {
          Log.Error("WKT_ProcessRequestPlayerDrawNumbers. CashierMovementsTable.Save() error. AccountId: " + _account_id + ".");

          return;
        }

        //        . Generate vouchers for all new tickets 
        _vouchers_list = new ArrayList();

        foreach (DrawTicket _ticket in _tickets)
        {
          _vouchers_list.Add(_ticket.Voucher);
        }

        _response.VoucherList = _vouchers_list;

        // Commit tickets and movememnts
        _db_trx.Commit();

        WktResponse.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_WKT_OK;
      } // using
    }
    catch (Exception _ex)
    {
      Log.Exception(_ex);

      WktResponse.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_WKT_ERROR;
    }
  }

}