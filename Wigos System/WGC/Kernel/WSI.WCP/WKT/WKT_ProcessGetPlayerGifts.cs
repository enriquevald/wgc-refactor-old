//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WKT_ProcessGetPlayerGifts.cs
// 
//   DESCRIPTION: WKT_ProcessGetPlayerGifts class
// 
//        AUTHOR: Andreu Juli�
// 
// CREATION DATE: 18-MAY-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 18-MAY-2012 AJQ    First version.
// 07-FEB-2013 JMM    Account points redeem status checking added.
// 10-MAY-2013 JMA    Changed queries and other features to limit the amount of gifts redemptions
// 24-MAY-2013 HBB    Fixed Bug #797: PromoBOX: Gifts of Draws: The filters of draws aren't used
// 17-SEP-2013 LEM    Show gift list according points cost for account level (Modified GetPlayerGifts())
// 14-APR-2014 DLL    Added new functionality: VIP Account
// 06-JAN-2016 SGB    Backlog Item 7910: Change column AC_POINTS to bucket 1.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Threading;
using System.Data.SqlClient;
using WSI.Common;
using WSI.WCP;

public static partial class WCP_WKT
{
  private static void WKT_ProcessGetPlayerGifts(WCP_Message WktRequest, WCP_Message WktResponse)
  {
    WCP_WKT_MsgGetPlayerGifts _request;
    WCP_WKT_MsgGetPlayerGiftsReply _response;

    WCP_ResponseCodes _response_code;
    Int64 _account_id;
    List<Int64> _resources;
    List<Int64> _not_found_resources;
    WKT_ResourceInfoList _list_resources;

    _resources = new List<Int64>();

    _request = (WCP_WKT_MsgGetPlayerGifts)WktRequest.MsgContent;
    _response = (WCP_WKT_MsgGetPlayerGiftsReply)WktResponse.MsgContent;

    WktResponse.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_WKT_ERROR;

    try
    {
      // Init Gifts
      _response.InitGifts();

      //Login Ok?
      if (!WKT_PlayerLogin(_request.Player.Trackdata, _request.Player.Pin, out _account_id, out _response_code))
      {
        WktResponse.MsgHeader.ResponseCode = _response_code;

        return;
      }

      //Get Gifts 
      if (GetPlayerGifts(_account_id, _response.Gifts) != WCP_ResponseCodes.WCP_RC_WKT_OK)
      {
        return;
      }

      //Get id resources gifts    SMALL_RESOURCE_ID = 4 , LARGE_RESOURCE = 5
      GetResourceIdFromTable(_response.Gifts.Tables[0], new Int32[] { 4, 5 }, _resources);
      GetResourceIdFromTable(_response.Gifts.Tables[1], new Int32[] { 4, 5 }, _resources);

      //Get gifts resources 
      if (!GetResourceInfo(_resources, out _list_resources, out _not_found_resources))
      {
        return;
      }

      _response.Resources = _list_resources;
      if (_not_found_resources.Count > 0)
      {
        // Test & Log Gifts with not found Resources
        //  GIFT_ID = 0 , GIFT_NAME = 2
        TestResourceIdNotFound(_response.Gifts.Tables[0], new Int32[] { 4, 5 }, _not_found_resources, "Gift", new Int32[] { 0, 2 });
        TestResourceIdNotFound(_response.Gifts.Tables[1], new Int32[] { 4, 5 }, _not_found_resources, "Gift", new Int32[] { 0, 2 });
      }

      WktResponse.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_WKT_OK;
    }
    catch (Exception _ex)
    {
      Log.Exception(_ex);
    }
    finally
    {
      if (WktResponse.MsgHeader.ResponseCode != WCP_ResponseCodes.WCP_RC_WKT_OK)
      {
        _response.InitGifts();
        _response.Resources = new WKT_ResourceInfoList();
      }
    }

  } // WKT_ProcessGetPlayerGifts

  //------------------------------------------------------------------------------
  // PURPOSE: Returns a DataSet with all gifts redeemeable and not redeemable filtered by points.
  //
  //  PARAMS:
  //      - INPUT:
  //          - AccountId       : Holder account who's asking the gift reedimeables
  //
  //      - OUTPUT:
  //          - DataSet         : With 2 DataTables: [DATATABLE_NAME_REEDEMEABLE] = 0 / [DATATABLE_NAME_NONE_REEDEMEABLE] = 1
  //
  // RETURNS:
  //      - WktResponseCode     : Result of operation  
  //
  private static WCP_ResponseCodes GetPlayerGifts(Int64 AccountId, DataSet Gifts)
  {
    Int32 _gp_value;
    Decimal _player_points;
    Decimal _future_player_points;   // Value of prize Higher to show = holder_points * %_shows
    Decimal _future_pct_points;      // Limit % price points to show (%_show)
    Int32 _min_gifts;                // Min Gifts of catalog (if %_shows is defined)
    Int32 _max_gifts;                // Max Gifts of catalog 

    StringBuilder _sb;
    Decimal _price_points;
    DataRow _row;
    Boolean _add_row;
    Boolean _gift_inside_interval;
    DateTime _today;
    DateTime _first_day_of_month;
    Int32 _holder_level;
    Int32 _holder_gender;

    String _gi_points_col;
    String _dr_points_col;

    Boolean _is_vip;

    try
    {
      _sb = new StringBuilder();

      // Get actual balance points from account
      using (DB_TRX _db_trx = new DB_TRX())
      {
        _sb.Length = 0;
        _sb.AppendLine("SELECT   DBO.GETBUCKETVALUE(@pBucketId, @pAccountId) ");
        _sb.AppendLine("        ,AC_HOLDER_LEVEL             ");
        _sb.AppendLine("        ,AC_HOLDER_GENDER            ");
        _sb.AppendLine("        ,ISNULL(AC_HOLDER_IS_VIP, 0) ");
        _sb.AppendLine("  FROM   ACCOUNTS                    ");
        _sb.AppendLine(" WHERE   AC_ACCOUNT_ID = @pAccountId ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString()))
        {
          _cmd.Parameters.Add("@pBucketId", SqlDbType.Int).Value = (Int32)Buckets.BucketId.RedemptionPoints;
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;

          using (SqlDataReader _reader = _db_trx.ExecuteReader(_cmd))
          {
            if (!_reader.Read())
            {
              Gifts.Clear();

              return WCP_ResponseCodes.WCP_RC_WKT_ERROR;
            }

            _player_points = _reader.GetDecimal(0);
            _player_points = Math.Max(_player_points, 0);     // Ensure > 0
            _player_points = Math.Floor(_player_points);      // Truncate
            _holder_level = _reader.GetInt32(1);
            _holder_gender = _reader.GetInt32(2);
            _is_vip = _reader.GetBoolean(3);
          }
        }

        _future_player_points = 0;
        _future_pct_points = 0;
        _max_gifts = 0;
        _min_gifts = 0;

        // Get from General Params, % points of over valued gifts, Max/Min Gifts to show
        // FutureGifts.ExtraPointsPct
        if (Int32.TryParse(GeneralParam.Value("WigosKiosk", "FutureGifts.ExtraPointsPct"), out _gp_value))
        {
          _gp_value = Math.Max(_gp_value, 0);

          if (_gp_value != 0)
          {
            // Future Player Points = Player Points + 30%
            _future_pct_points = _gp_value;
            _future_player_points = Math.Floor(_player_points * (1.0m + (Decimal)_gp_value / 100.0m));
          }

          // FutureGifts.MinGifts to Return if % is defined
          if (Int32.TryParse(GeneralParam.Value("WigosKiosk", "FutureGifts.MinCount"), out _min_gifts))
          {
            _min_gifts = Math.Max(0, _min_gifts);
          }
        }

        // FutureGifts.MaxGifts to Return
        if (Int32.TryParse(GeneralParam.Value("WigosKiosk", "FutureGifts.MaxCount"), out _gp_value))
        {
          _max_gifts = Math.Max(0, _gp_value);
        }

        _today = Misc.TodayOpening();
        _first_day_of_month = new DateTime(_today.Year, _today.Month, 1, _today.Hour, _today.Minute, _today.Second);

        _gi_points_col = "GI_POINTS_LEVEL" + _holder_level;
        _dr_points_col = "DR_POINTS_LEVEL" + _holder_level;

        // Get Redeemable Gifts/Draws
        _sb.Length = 0;
        _sb.AppendLine("SELECT   GI_GIFT_ID                                                         ");
        _sb.AppendLine("       , " + _gi_points_col + " AS GI_POINTS                                ");
        _sb.AppendLine("       , CASE WHEN LEN(GI_TEXT_ON_PROMOBOX) > 0 THEN ISNULL (GI_TEXT_ON_PROMOBOX, GI_NAME) ELSE GI_NAME END AS GI_NAME ");
        _sb.AppendLine("       , GI_DESCRIPTION                                                     ");
        _sb.AppendLine("       , GI_SMALL_RESOURCE_ID                                               ");
        _sb.AppendLine("       , GI_LARGE_RESOURCE_ID                                               ");
        _sb.AppendLine("       , GI_TYPE                                                            ");
        _sb.AppendLine("       , GI_CURRENT_STOCK                                                   ");
        _sb.AppendLine("       , 0                                     AS GI_MAX_ALLOWED_UNITS  ");
        _sb.AppendLine("       , GI_CONVERSION_TO_NRC                  AS GI_NON_REDEEMABLE     ");
        _sb.AppendLine("       , 0                                     AS GI_DRAW_ID            ");
        _sb.AppendLine("       , ISNULL(GI_ACCOUNT_DAILY_LIMIT     , 0)AS GI_ACCOUNT_DAILY_LIMIT     ");
        _sb.AppendLine("       , ISNULL(GI_ACCOUNT_MONTHLY_LIMIT   , 0)AS GI_ACCOUNT_MONTHLY_LIMIT  ");
        _sb.AppendLine("       , ISNULL(GI_GLOBAL_DAILY_LIMIT      , 0)AS GI_GLOBAL_DAILY_LIMIT        ");
        _sb.AppendLine("       , ISNULL(GI_GLOBAL_MONTHLY_LIMIT    , 0)AS GI_GLOBAL_MONTHLY_LIMIT    ");
        _sb.AppendLine("       , ISNULL(TOT_REQUESTED.ACC_DAY      , 0)AS REQ_ACCOUNT_DAILY_TOTAL_SPENT    ");
        _sb.AppendLine("       , ISNULL(TOT_REQUESTED.ACC_MONTH    , 0)AS REQ_ACCOUNT_MONTH_TOTAL_SPENT    ");
        _sb.AppendLine("       , ISNULL(TOT_REQUESTED.GLOBAL_DAY   , 0)AS REQ_GLOBAL_DAILY_TOTAL_SPENT    ");
        _sb.AppendLine("       , ISNULL(TOT_REQUESTED.GLOBAL_MONTH , 0)AS REQ_GLOBAL_MONTHLY_TOTAL_SPENT    ");
        _sb.AppendLine("       , 0                                     AS GI_DRAW_LEVEL_FILTER      ");
        _sb.AppendLine("       , 0                                     AS GI_GENDER_FILTER          ");
        _sb.AppendLine("       , ISNULL(GI_VIP, 0)                     AS GI_VIP          ");


        // ADD Limit fields so that this can be compared on the while clause and filter out none-available gifts
        _sb.AppendLine("  FROM   GIFTS                                                        ");
        _sb.AppendLine("  LEFT   JOIN                                                         ");
        _sb.AppendLine("   (SELECT   SUM(CASE WHEN GIN_REQUESTED >= @pTodayOpening AND GIN_ACCOUNT_ID    = @pAccountId THEN GIN_NUM_ITEMS ELSE 0 END) AS ACC_DAY      ");
        _sb.AppendLine("           , SUM(CASE WHEN GIN_ACCOUNT_ID = @pAccountId                                        THEN GIN_NUM_ITEMS ELSE 0 END) AS ACC_MONTH    ");
        _sb.AppendLine("           , SUM(CASE WHEN GIN_REQUESTED >= @pTodayOpening                                     THEN GIN_NUM_ITEMS ELSE 0 END) AS GLOBAL_DAY   ");
        _sb.AppendLine("           , SUM(GIN_NUM_ITEMS)                                                                                               AS GLOBAL_MONTH ");
        _sb.AppendLine("           , GIN_GIFT_ID   ");
        _sb.AppendLine("      FROM   GIFT_INSTANCES                           ");
        _sb.AppendLine("     WHERE   GIN_REQUESTED >= @pFirstDayOfMonth   ");
        _sb.AppendLine("  GROUP BY   GIN_GIFT_ID   ");
        _sb.AppendLine("   ) AS TOT_REQUESTED ON TOT_REQUESTED.GIN_GIFT_ID = GI_GIFT_ID     ");
        _sb.AppendLine("     WHERE   GIFTS.GI_AVAILABLE = 1                                   ");
        _sb.AppendLine("       AND  ( ISNULL(GI_ACCOUNT_DAILY_LIMIT  , 0) = 0 OR GI_ACCOUNT_DAILY_LIMIT   > ISNULL(TOT_REQUESTED.ACC_DAY    , 0))  ");
        _sb.AppendLine("       AND  ( ISNULL(GI_ACCOUNT_MONTHLY_LIMIT, 0) = 0 OR GI_ACCOUNT_MONTHLY_LIMIT > ISNULL(TOT_REQUESTED.ACC_MONTH     , 0))  ");
        _sb.AppendLine("       AND  ( ISNULL(GI_GLOBAL_DAILY_LIMIT   , 0) = 0 OR GI_GLOBAL_DAILY_LIMIT    > ISNULL(TOT_REQUESTED.GLOBAL_DAY  , 0))  ");
        _sb.AppendLine("       AND  ( ISNULL(GI_GLOBAL_MONTHLY_LIMIT , 0) = 0 OR GI_GLOBAL_MONTHLY_LIMIT  > ISNULL(TOT_REQUESTED.GLOBAL_MONTH   , 0)) ");

        _sb.AppendLine("       AND (( GIFTS.GI_TYPE = @pGiftTypeObject  AND  GIFTS.GI_CURRENT_STOCK > 0 AND   GIFTS.GI_CURRENT_STOCK > GIFTS.GI_REQUEST_COUNTER )");
        _sb.AppendLine("               OR   GIFTS.GI_TYPE = @pGiftTypeNRC                      ");
        _sb.AppendLine("               OR   GIFTS.GI_TYPE = @pGiftTypeRC                       ");
        _sb.AppendLine("               OR   GIFTS.GI_TYPE = @pGiftTypeServices                 ");
        _sb.AppendLine("           )                                                           ");
        _sb.AppendLine("       AND  GI_AWARD_ON_PROMOBOX = 1                                   ");
        _sb.AppendLine("     UNION                                                             ");
        _sb.AppendLine("SELECT   0                                AS GI_GIFT_ID            ");
        _sb.AppendLine("       , " + _dr_points_col + "           AS GI_POINTS             ");
        _sb.AppendLine("       , CASE WHEN LEN(DR_TEXT_ON_PROMOBOX) > 0 THEN ISNULL (DR_TEXT_ON_PROMOBOX, DR_NAME) ELSE DR_NAME END AS GI_NAME ");
        _sb.AppendLine("       , DR_NAME                          AS GI_DESCRIPTION        ");    //TODO: CHANGE dr_name by Draw.Descritption
        _sb.AppendLine("       , ISNULL(DR_SMALL_RESOURCE_ID, 0)  AS GI_SMALL_RESOURCE_ID  ");
        _sb.AppendLine("       , ISNULL(DR_LARGE_RESOURCE_ID, 0)  AS GI_LARGE_RESOURCE_ID  ");
        _sb.AppendLine("       , @pGiftTypeDrawNumbers            AS GI_TYPE               ");
        _sb.AppendLine("       , CASE WHEN DR_LAST_NUMBER = -1                             ");    //Case not selled any draw number
        _sb.AppendLine("              THEN DR_MAX_NUMBER - DR_INITIAL_NUMBER + 1           ");
        _sb.AppendLine("              ELSE DR_MAX_NUMBER - DR_LAST_NUMBER                  ");
        _sb.AppendLine("         END AS GI_CURRENT_STOCK                                   ");
        _sb.AppendLine("       , 0                               AS GI_MAX_ALLOWED_UNITS   ");
        _sb.AppendLine("       , DR_NUMBER_PRICE                 AS GI_NON_REDEEMABLE      ");
        _sb.AppendLine("       , DR_ID                           AS GI_DRAW_ID             ");
        _sb.AppendLine("       , 0                                     ");
        _sb.AppendLine("       , 0                                     ");
        _sb.AppendLine("       , 0                                     ");
        _sb.AppendLine("       , 0                                     ");
        _sb.AppendLine("       , 0                                     ");
        _sb.AppendLine("       , 0                                     ");
        _sb.AppendLine("       , 0                                     ");
        _sb.AppendLine("       , 0                                     ");
        _sb.AppendLine("       , DR_LEVEL_FILTER                 AS GI_DRAW_LEVEL_FILTER   ");
        _sb.AppendLine("       , DR_GENDER_FILTER                AS GI_GENDER_FILTER       ");
        _sb.AppendLine("       , ISNULL(DR_VIP, 0)               AS GI_VIP                 ");
        _sb.AppendLine("  FROM   DRAWS                                                     ");
        _sb.AppendLine(" WHERE   DR_STARTING_DATE <= GETDATE ()                            ");
        _sb.AppendLine("   AND   DR_ENDING_DATE   >  GETDATE ()                            ");
        _sb.AppendLine("   AND   DR_STATUS        = 0                                      ");
        _sb.AppendLine("   AND   DR_LAST_NUMBER   < DR_MAX_NUMBER                          ");
        _sb.AppendLine("   AND   " + _dr_points_col + " > 0                                ");
        _sb.AppendLine("   AND   DR_AWARD_ON_PROMOBOX = 1                                  ");
        _sb.AppendLine(" ORDER BY GI_POINTS ASC, GI_NAME ASC                               ");


        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString()))
        {
          _sql_cmd.Parameters.Add("@pGiftTypeObject", SqlDbType.Int).Value = GIFT_TYPE.OBJECT;
          _sql_cmd.Parameters.Add("@pGiftTypeNRC", SqlDbType.Int).Value = GIFT_TYPE.NOT_REDEEMABLE_CREDIT;
          _sql_cmd.Parameters.Add("@pGiftTypeRC", SqlDbType.Int).Value = GIFT_TYPE.REDEEMABLE_CREDIT;
          _sql_cmd.Parameters.Add("@pGiftTypeServices", SqlDbType.Int).Value = GIFT_TYPE.SERVICES;
          _sql_cmd.Parameters.Add("@pGiftTypeDrawNumbers", SqlDbType.Int).Value = GIFT_TYPE.DRAW_NUMBERS;
          _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
          _sql_cmd.Parameters.Add("@pTodayOpening", SqlDbType.DateTime).Value = _today;
          _sql_cmd.Parameters.Add("@pFirstDayOfMonth", SqlDbType.DateTime).Value = _first_day_of_month;

          using (SqlDataReader _sql_reader = _db_trx.ExecuteReader(_sql_cmd))
          {
            while (_sql_reader.Read())
            {
              if (!Misc.IsVIP(_is_vip, _sql_reader.GetBoolean(21)))
              {
                // Skip
                continue;
              }

              _price_points = _sql_reader.IsDBNull(1) ? 0 : _sql_reader.GetDecimal(1);

              if (_price_points <= 0)
              {
                // Skip the gift
                continue;
              }

              if (_sql_reader.GetInt64(10) != 0)  // It's a draw
              {
                if (!Misc.IsLevelAllowed(_holder_level, _sql_reader.GetInt32(19))) // it's not availible for this account level
                {
                  // If the draw can't be reached from this account, skip the gift
                  continue;
                }

                if (!Misc.IsGenderAllowed(_holder_gender, (DrawGenderFilter)_sql_reader.GetInt32(20))) // it's not availible for this gender
                {
                  continue;
                }
              }

              //verificar el sexo para el cual esta validado el sorteo

              // Filter gift/draw redeemable or near redeemable
              if (_price_points <= _player_points)
              {
                // Add to Gifts list
                _row = Gifts.Tables[0].NewRow();
              }
              else
              {
                // Add row to Next Gifts
                _add_row = false;
                _gift_inside_interval = false;

                if (_future_pct_points == 0
                  || (_future_player_points > 0 && _price_points <= _future_player_points))
                {
                  _add_row = true;
                  _gift_inside_interval = true;
                }

                if (_future_pct_points > 0
                  && _min_gifts > 0
                  && Gifts.Tables[1].Rows.Count < _min_gifts)
                {
                  _add_row = true;
                }

                // Gift will be added if there is room in the future list
                if (_max_gifts > 0)
                {
                  _add_row = (Gifts.Tables[1].Rows.Count < _max_gifts);
                }

                // Restrictions
                if (_add_row)
                {
                  // Gift should be added, there is room in the future list
                  if (!_gift_inside_interval)
                  {
                    // But it is not in the interval range. 
                    // In this case we will check if we have already the minimum gifts to show. 
                    // If so gift will be excluded from the future list 
                    if (_future_pct_points > 0 && Gifts.Tables[1].Rows.Count >= _min_gifts)
                    {
                      _add_row = false;
                    }
                  }
                }

                if (!_add_row)
                {
                  // Candidate gift are sorted ascending by their value in points 
                  break;
                }

                // Add to Next Gifts list
                _row = Gifts.Tables[1].NewRow();
              }

              _row[0] = _sql_reader.GetInt64(0);
              _row[1] = _sql_reader.IsDBNull(1) ? 0 : _sql_reader.GetDecimal(1);
              _row[2] = _sql_reader.GetString(2);
              _row[3] = _sql_reader.GetString(3);
              _row[4] = _sql_reader.IsDBNull(4) ? 0 : _sql_reader.GetInt64(4);
              _row[5] = _sql_reader.IsDBNull(5) ? 0 : _sql_reader.GetInt64(5);
              _row[6] = _sql_reader.GetInt32(6);
              _row[7] = _sql_reader.GetInt64(7) > Int32.MaxValue ? Int32.MaxValue : (Int32)_sql_reader.GetInt64(7);
              _row[8] = _sql_reader.GetInt32(8);
              _row[9] = _sql_reader.IsDBNull(9) ? 0 : _sql_reader.GetDecimal(9);
              _row[10] = _sql_reader.GetInt64(10);

              _row[11] = _sql_reader.IsDBNull(11) ? 0 : _sql_reader.GetInt32(11);
              _row[12] = _sql_reader.IsDBNull(12) ? 0 : _sql_reader.GetInt32(12);
              _row[13] = _sql_reader.IsDBNull(13) ? 0 : _sql_reader.GetInt32(13);
              _row[14] = _sql_reader.IsDBNull(14) ? 0 : _sql_reader.GetInt32(14);

              _row[15] = _sql_reader.IsDBNull(15) ? 0 : _sql_reader.GetInt32(15);
              _row[16] = _sql_reader.IsDBNull(16) ? 0 : _sql_reader.GetInt32(16);
              _row[17] = _sql_reader.IsDBNull(17) ? 0 : _sql_reader.GetInt32(17);
              _row[18] = _sql_reader.IsDBNull(18) ? 0 : _sql_reader.GetInt32(18);

              _row.Table.Rows.Add(_row);

            } // while(_sql_reader.Read())

          } // using dataReader
        }   // using sqlCommand

        // Get max units reedeamable for each Gift/Draw
        if (!MaxAllowedUnits(AccountId, Gifts, _db_trx.SqlTransaction))
        {
          Gifts.Clear();

          return WCP_ResponseCodes.WCP_RC_WKT_ERROR;
        }
      }     // using transaction

      return WCP_ResponseCodes.WCP_RC_WKT_OK;
    }
    catch (Exception _ex)
    {
      Log.Exception(_ex);
      Gifts.Clear();

      return WCP_ResponseCodes.WCP_RC_WKT_ERROR;
    }
  } // GetPlayerGifts

  //------------------------------------------------------------------------------
  // PURPOSE: Check if Account has reached the allowed RC and NR daily limit  
  //          
  //  PARAMS:
  //      - INPUT:
  //          - CardData        : Holder account who's asking 
  //      - OUTPUT:
  //          -CanRedeemableCredit    : If can get redeem credit
  //          -CanNoRedeemableCredit  : If can get redeem credit
  // 
  private static void CanRequestCredit(CardData Card, out Boolean CanRedeemableCredit, out Boolean CanNoRedeemableCredit)
  {
    Gift _gift;
    Gift.GIFT_MSG _dummy_gift_msg;

    //Check if current account can request redeem and non redeem credit
    _gift = new Gift();
    _gift.Available = true;

    _gift.Type = GIFT_TYPE.NOT_REDEEMABLE_CREDIT;
    CanRedeemableCredit = _gift.CheckGiftRequest(Card, false, out _dummy_gift_msg);

    _gift.Type = GIFT_TYPE.REDEEMABLE_CREDIT;
    CanNoRedeemableCredit = _gift.CheckGiftRequest(Card, false, out _dummy_gift_msg);

  }

  //------------------------------------------------------------------------------
  // PURPOSE: Get limit of Max Redeemeable units for each Gift/Draw, without point limitation
  //
  //  PARAMS:
  //      - INPUT:
  //          - AccountId       : Holder account who's asking the gift reedimeables
  //          - Gfits           : DataSet where are stored de Gifts/Draws
  //      - OUTPUT:
  //
  // RETURNS:
  //      - Boolean             : True if there wasn't any error
  //                            : False if there was any error
  //
  private static Boolean MaxAllowedUnits(Int64 AccountId, DataSet Gifts, SqlTransaction SqlTrx)
  {
    Gift _gift;
    GiftInstance _gift_inst;
    GiftInstance.GIFT_INSTANCE_MSG _gift_inst_msg;
    CardData _card;
    Currency _max_redeem;
    Currency _max_no_redeem;
    Boolean _can_request_no_redeem_credit;
    Boolean _can_request_redeem_credit;

    try
    {
      _card = new CardData();
      CardData.DB_CardGetAllData(AccountId, _card, SqlTrx);

      //Check if current account can request redeem and non redeem credit
      CanRequestCredit(_card, out _can_request_redeem_credit, out _can_request_no_redeem_credit);

      //Check the max NRC and RC for the user
      if (!GiftRequest.MaxBuyableCredits(_card, out _max_redeem, out _max_no_redeem, SqlTrx))
      {
        Log.Error("MaxAllowedUnits. Cannot get max redeem and no redeem  allowed credits for account with Id: " + AccountId + " .");

        return false;
      }

      // 6   --> GI_TYPE                Gift Type
      // 8   --> GI_MAX_ALLOWED_UNITS   Max Units Can Request
      // 9   --> GI_CONVERSION_TO_NRC   Amount Credit Redeem or Not Redeem of the Gift

      foreach (DataRow _dr_gift in Gifts.Tables[0].Rows)
      {
        _dr_gift[8] = 0;

        if (_card.PointsStatus == ACCOUNT_POINTS_STATUS.REDEEM_ALLOWED)
        {
          switch ((GIFT_TYPE)_dr_gift[6])
          {
            case GIFT_TYPE.SERVICES:
            case GIFT_TYPE.OBJECT:
              // TODO: define this parameter in GeneralParams ...
              // Limit 1 per Operation
              _dr_gift[8] = 1;
              break;

            case GIFT_TYPE.DRAW_NUMBERS:
              _gift = new Gift(_dr_gift);
              _gift_inst = new GiftInstance(_gift, _card);

              if (!_gift_inst.CheckDrawNumbersGift(out _gift_inst_msg))
              {
                _dr_gift[8] = 0;
              }
              else
              {
                _dr_gift[8] = Math.Floor(_gift_inst.DrawMaxPointsToSpend / _gift_inst.DrawNumberPointsPrice);
              }
              break;

            case GIFT_TYPE.NOT_REDEEMABLE_CREDIT:

              // if account can request NonRedeem Credits
              if (_can_request_no_redeem_credit)
              {
                _dr_gift[8] = _max_no_redeem == Decimal.MaxValue ? Int32.MaxValue : Math.Floor(_max_no_redeem / (Decimal)_dr_gift[9]);
              }
              else
              {
                _dr_gift[8] = 0;
              }
              break;

            case GIFT_TYPE.REDEEMABLE_CREDIT:

              // if account can request Redeem Credits
              if (_can_request_redeem_credit)
              {
                _dr_gift[8] = _max_redeem == Decimal.MaxValue ? Int32.MaxValue : Math.Floor(_max_redeem / (Decimal)_dr_gift[9]);
              }
              else
              {
                _dr_gift[8] = 0;
              }
              break;

            case GIFT_TYPE.UNKNOWN:
            default:
              //TODO define limit for unknown Gift Type
              Log.Error("MaxAllowedUnits. Unknown Gift Type with Id: " + _dr_gift[0] + " .");
              _dr_gift[8] = 0;
              break;
          }
        }

        _dr_gift[8] = Math.Max(0, (Int32)_dr_gift[8]);

        // Now we check the gift redeem limit per account and globally
        _gift = new Gift(_dr_gift);
        _gift_inst = new GiftInstance(_gift, _card);
        _gift_inst.GetNumOfAvailableGifts((Int32)_dr_gift[15], (Int32)_dr_gift[16], (Int32)_dr_gift[17], (Int32)_dr_gift[18]);
        _dr_gift[8] = Math.Min(_gift_inst.NumAvailable, (Int32)_dr_gift[8]);

      }  // foreach row of redeem Gifts

      // For Near Gift, MaxUnits = Default value 0
    }
    catch (Exception _ex)
    {
      Log.Error("MaxAllowedUnits. Cannot get max allowed units assigned for account with Id: " + AccountId + " .");
      Log.Exception(_ex);

      return false;
    }

    return true;
  } // MaxAllowedUnits

} // WCP_WKT