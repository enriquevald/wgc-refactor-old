//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WKT_Advertisement.cs
// 
//   DESCRIPTION: WKT_Advertisement busines class
// 
//        AUTHOR: Artur Nebot
// 
// CREATION DATE: 26-JUL-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 26-JUL-2012 ANG    First version.
// 24-AUG-2012 XIT    Added Fields.
//------------------------------------------------------------------------------
//
 
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using WSI.Common;
using System.IO;
using WSI.WCP;


public static partial class WCP_WKT
{

  public partial class WKT_Advertisement
  {

    public class WKT_AdvSteps : List<WKT_AdvStep>, IXml
    {

      #region IXml Members

      public string ToXml()
      {
        StringBuilder _str_bld;
        _str_bld = new StringBuilder();

        _str_bld.AppendLine("<Steps>");
        foreach (WKT_AdvStep _step in this)
        {
          _str_bld.Append(_step.ToXml());
        }

        _str_bld.AppendLine("</Steps>");
        return _str_bld.ToString();
      }

      public void LoadXml(System.Xml.XmlReader Xml)
      {
        Boolean _has_finished;
        WKT_AdvStep _step;

        _has_finished = false;

        while (!Xml.EOF && Xml.Name != "Steps")
        {
          Xml.Read();
        }

        if (Xml.EOF)
        {
          throw new Exception("Tag Steps not found");
        }

        while (!_has_finished)
        {
          if (Xml.Name == "Step" && Xml.NodeType.Equals(XmlNodeType.Element))
          {
            _step = new WKT_AdvStep();
            _step.LoadXml(Xml);                // Cursor now is in </step>
            this.Add(_step);
          }
          else if (Xml.Name == "Steps" && Xml.NodeType.Equals(XmlNodeType.EndElement))
          {
            _has_finished = true;
          }
          if (!_has_finished)
          {
            _has_finished = !Xml.Read();
          }
        }
      }

      #endregion
    }

    public class WKT_AdvStep : IXml
    { 
      private WKT_AdvStaticImages m_images;

      public WKT_AdvStaticImages StaticImages
      {
        get { return this.m_images; }
        set { this.m_images = value; }
      }
      public Object PlayMedia
      {
        get { throw new Exception("Not Supported yet!"); }
        set { throw new Exception("Not Supported yet!"); }
      }

      #region IXml Members

      public String ToXml()
      {
        StringBuilder _str_bld;
        _str_bld = new StringBuilder();

        _str_bld.AppendLine("<Step>");
        //_str_bld.Append(this.PlayMedia.ToXml());
        _str_bld.Append(this.StaticImages.ToXml());
        _str_bld.AppendLine("</Step>");
        return _str_bld.ToString();
      }

      public void LoadXml(XmlReader Xml)
      {

        #region "Process PlayMedia"
        //while ( Xml.Name != "PlayMedia" && !Xml.EOF )
        //{
        //  XmlReader.Read();
        //}
        //if (XmlReader.EOF)
        //{
        //  throw new Exception("PlayMedia Tag not found!");
        //}
        //PlayMedia.LoadXml(Xml);
        #endregion

        StaticImages = new WKT_AdvStaticImages();
        StaticImages.LoadXml(Xml);

        // Cursor now is in  </Step>
      }

      #endregion

      public WKT_AdvStep()
      {
        this.StaticImages = new WKT_AdvStaticImages();
      }
    }

    public class WKT_AdvStaticImages : List<WKT_AdvStaticImage>, IXml
    {
      #region IXml Members

      public string ToXml()
      {
        StringBuilder _str_bld;
        _str_bld = new StringBuilder();

        foreach (WKT_AdvStaticImage _static_image in this)
        {
          _str_bld.Append(_static_image.ToXml());
        }

        return _str_bld.ToString();
      }

      public void LoadXml(System.Xml.XmlReader Xml)
      {
        Boolean _has_finished;
        WKT_AdvStaticImage _image_info;
        _has_finished = false;


        while (!Xml.EOF && Xml.Name != "StaticImage")
        {
          Xml.Read();
        }

        if (Xml.EOF)
        {
          throw new Exception("Tag StaticImage not found");
        }

        while (!_has_finished)
        {
          if (Xml.Name == "StaticImage" && Xml.NodeType.Equals(XmlNodeType.Element))
          {
            _image_info = new WKT_AdvStaticImage();

            _image_info.LoadXml(Xml);
            this.Add(_image_info);
          }
          else if (Xml.Name != "StaticImage" && Xml.NodeType.Equals(XmlNodeType.EndElement))
          {
            _has_finished = true;
          }

          if (!_has_finished)
          {
            _has_finished = !Xml.Read();
          }
          // Xml cursor position stay in </StaticImage>
        }
      }

      #endregion

    }

    public class WKT_AdvStaticImage : IXml
    {

      public Int64 Duration;
      public Int64 AreaWidth;
      public Int64 AreaHeight;
      public WKT_AdvPosition AreaPosition;
      public WKT_AdvImage Image;

      #region "Properties"
      #endregion

      #region "Constructor"
      public WKT_AdvStaticImage()
      {
        this.Image = new WKT_AdvImage();
        this.AreaPosition = new WKT_AdvPosition();
      }
      #endregion

      #region IXml Members

      public string ToXml()
      {
        StringBuilder _str_bld;
        _str_bld = new StringBuilder();

        _str_bld.AppendLine("<StaticImage>");
        _str_bld.AppendLine("<Duration>" + this.Duration + "</Duration>");
        _str_bld.AppendLine("<Area>");
        _str_bld.AppendLine("<AreaWidth>" + this.AreaWidth + "</AreaWidth>");
        _str_bld.AppendLine("<AreaHeight>" + this.AreaHeight + "</AreaHeight>");
        _str_bld.Append(this.AreaPosition.ToXml());
        _str_bld.AppendLine("</Area>");
        _str_bld.Append(this.Image.ToXml());
        _str_bld.AppendLine("</StaticImage>");
        return _str_bld.ToString();
      }

      public void LoadXml(System.Xml.XmlReader XmlReader)
      {
        while (XmlReader.Name != "StaticImage" && !XmlReader.EOF)
        {
          XmlReader.Read();
        }
        if (XmlReader.EOF)
        {
          throw new Exception("StaticImage Tag not found!");
        }

        // Duration
        if (!Int64.TryParse(XML.ReadTagValue(XmlReader, "Duration"), out this.Duration))
        {
          this.Duration = 0;
        }

        // AreaWidth
        if (!Int64.TryParse(XML.ReadTagValue(XmlReader, "AreaWidth"), out this.AreaWidth))
        {
          this.AreaWidth = 0;
        }

        // AreaHeight
        if (!Int64.TryParse(XML.ReadTagValue(XmlReader, "AreaHeight"), out this.AreaHeight))
        {
          this.AreaHeight = 0;
        }

        AreaPosition = new WKT_AdvPosition();
        AreaPosition.LoadXml(XmlReader);

        this.Image = new WKT_AdvImage();
        this.Image.LoadXml(XmlReader);

        // Move cursor to </StaticImage>
        while (!(XmlReader.Name == "StaticImage" && XmlReader.NodeType == XmlNodeType.EndElement) && !XmlReader.EOF)
        {
          XmlReader.Read();
        }

        // Next...
      }

      #endregion

      #region "Code to delete"
      //public static bool FillTextFromReader(XmlReader Reader, out WKT_AdvText Text)
      //{
      //  Text = new WKT_AdvText();

      //  while (!Reader.EOF && Reader.Name != "Text") Reader.Read();

      //  if (Reader.EOF || Reader.Name != "Text")
      //  {
      //    return false;
      //  }

      //  Text.LoadXml(Reader);

      //  return true;

      //}
      //public static bool FillImageFromReader(XmlReader Reader, out WKT_AdvImage Image)
      //{
      //  Image = new WKT_AdvImage();

      //  while (!Reader.EOF && Reader.Name != "Image") Reader.Read();

      //  if (Reader.EOF || Reader.Name != "Image")
      //  {
      //    return false;
      //  }

      //  Image.LoadXml(Reader);

      //  return true;

      //} // FillImageFromReader
      #endregion

    }

    public class WKT_AdvImage : IXml
    {
      public Int64 ImageWidth;
      public Int64 ImageHeight;
      public WKT_AdvPosition Position;
      public Int64 ResourceId;
      public WKT_ResourceInfo Resource;         // TO DO : DELETE! NOT NECESSARY!

      #region IXml Members

      public String ToXml()
      {
        StringBuilder _str_bld;
        _str_bld = new StringBuilder();

        _str_bld.AppendLine("<Image>");
        _str_bld.AppendLine("<ImageWidth>" + this.ImageWidth.ToString() + "</ImageWidth>");
        _str_bld.AppendLine("<ImageHeight>" + this.ImageHeight.ToString() + "</ImageHeight>");
        _str_bld.Append(this.Position.ToXml());
        _str_bld.AppendLine("<ResourceId>" + this.ResourceId + "</ResourceId>");
        _str_bld.AppendLine("</Image>");
        return _str_bld.ToString();
      }

      public void LoadXml(XmlReader XmlReader)
      {

        while (XmlReader.Name != "Image" && !XmlReader.EOF)
        {
          XmlReader.Read();
        }
        if (XmlReader.EOF)
        {
          throw new Exception("Image Tag not found!");
        }
        this.ImageWidth = long.Parse(XML.ReadTagValue(XmlReader, "ImageWidth").ToString());
        this.ImageHeight = long.Parse(XML.ReadTagValue(XmlReader, "ImageHeight").ToString());
        Position = new WKT_AdvPosition();
        Position.LoadXml(XmlReader);

        if (!long.TryParse(XML.ReadTagValue(XmlReader, "ResourceId"), out this.ResourceId))
        {
          this.ResourceId = 0;
        }

        // Move cursor to </Image> 
        while (!(XmlReader.Name == "Image" && XmlReader.NodeType == XmlNodeType.EndElement))
        {
          XmlReader.Read();
        }

      }

      #endregion

      #region "Constructor"
      public WKT_AdvImage()
      {
        this.Position = new WKT_AdvPosition();
        this.Resource = new WKT_ResourceInfo();

      }
      #endregion

    }
    public class WKT_AdvPosition : IXml
    {
      public Int64 X;
      public Int64 Y;

      #region IXml Members

      public string ToXml()
      {

        StringBuilder _str_bld;
        _str_bld = new StringBuilder();

        _str_bld.AppendLine("<Position>");
        _str_bld.AppendLine("<X>" + this.X + "</X>");
        _str_bld.AppendLine("<Y>" + this.Y + "</Y>");
        _str_bld.AppendLine("</Position>");

        return _str_bld.ToString();
      }

      public void LoadXml(XmlReader Xml)
      {
        while (Xml.Name != "Position" && !Xml.EOF)
        {
          Xml.Read();
        }
        if (Xml.EOF)
        {
          throw new Exception(" Tag position not found");
        }


        if (!Int64.TryParse(XML.ReadTagValue(Xml, "X"), out this.X))
        {
          this.X = 0;
        }
        if (!Int64.TryParse(XML.ReadTagValue(Xml, "Y"), out this.Y))
        {
          this.Y = 0;
        }

        // Mode cursor to </Position>
        while (!(Xml.Name == "Position" && Xml.NodeType == XmlNodeType.EndElement) && !Xml.EOF)
        {
          Xml.Read();
        }
      }

      #endregion

      public WKT_AdvPosition(Int64 X, Int64 Y)
      {
        this.X = X;
        this.Y = Y;
      }
      public WKT_AdvPosition()
      {
        this.X = 0;
        this.Y = 0;
      }
    }

  }
}
