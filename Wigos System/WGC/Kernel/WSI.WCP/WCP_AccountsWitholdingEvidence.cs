//------------------------------------------------------------------------------
// Copyright � 2015 Win Systems Ltd.
//------------------------------------------------------------------------------
//
//   MODULE NAME : WCP_AccountsWitholding.cs
//
//   DESCRIPTION : Process Witholding Evidences
//
// REVISION HISTORY :
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 20-JUN-2016 LTC    First release.
// 20-JUN-2016 LTC    Product Backlog Item 13612,13613,13614: Witholding Evidence.
// 30-NOV-2016 RAB    PBI 21157: Withholding generation - Thread.
// 01-MAR-2017 FGB    Bug 24126: Withholding generation: The Excel data is incorrect
// 29-MAY-2017 DHA    Bug 27734:Background constancy generation error
// ----------- ------ ----------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Data;
using System.Data.SqlClient;
using WSI.Common;
using WSI.WCP;

namespace WSI.WCP
{
  public static class WCP_AccountsWitholding
  {
    #region Members

    #endregion Members

    #region Public Functions

    //------------------------------------------------------------------------------
    // PURPOSE : Process Witholding Evidences
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    static public Boolean WithOldingEvidences()
    {
      Witholding.WITHHOLDING_PRINT_MODE _withholding_print_mode;
      Boolean _return_function;

      Misc.WriteLog(String.Format("WCP_AccountsWitholdingEvidence.WithOldingEvidences - BEGIN"), Log.Type.Message);
      _withholding_print_mode = Witholding.WitholdingDocumentModeGenerate();
      _return_function = true;

      if (_withholding_print_mode == Witholding.WITHHOLDING_PRINT_MODE.IMMEDIATE)
      {
        //Inmediate mode process
        _return_function = true;
      }
      else if (_withholding_print_mode == Witholding.WITHHOLDING_PRINT_MODE.BACKGROUND)
      {
        //Background mode process
        // Enabled functionality
        if (!Witholding.Enabled())
        {
          Misc.WriteLog(String.Format("WCP_AccountsWitholdingEvidence.WithOldingEvidences - END witholding not enabled"), Log.Type.Message);
          _return_function = true;
        }

        try
        {
          if (!GetWitHoldingEvidence())
          {
            _return_function = false;

            Misc.WriteLog(String.Format("WCP_AccountsWitholdingEvidence.WithOldingEvidences - ERROR"), Log.Type.Message);
          }
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);

          _return_function = false;
        }
      }

      Misc.WriteLog(String.Format("WCP_AccountsWitholdingEvidence.WithOldingEvidences - END"), Log.Type.Message);

      return _return_function;
    }

    #endregion Public Functions

    #region Private Functions

    //------------------------------------------------------------------------------
    // PURPOSE : Get the witholding evidences
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //          - DataTable
    //
    // RETURNS :
    //      - True or False
    //
    //   NOTES :
    //      - true: Ok.
    //      - false: error.
    //
    static private Boolean GetWitHoldingEvidence()
    {
      try
      {
        Misc.WriteLog(String.Format("WCP_AccountsWitholdingEvidenceGetWitHoldingEvidence - BEGIN"), Log.Type.Message);
        Witholding.DB_CreateWitholdingDoc();

        Misc.WriteLog(String.Format("WCP_AccountsWitholdingEvidenceGetWitHoldingEvidence - END OK"), Log.Type.Message);
        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("Error on WCP_AccountsWitholdingEvidence.GetWitholdingEvidence");
        Log.Exception(_ex);
      }

      Misc.WriteLog(String.Format("WCP_AccountsWitholdingEvidenceGetWitHoldingEvidence - END ERROR"), Log.Type.Message);
      return false;
    }

    #endregion Private Functions
  }
}
