using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.IO;
using System.Net;
using System.Xml;
using System.Data;
using System.Data.SqlClient;

using WSI.Common;


namespace WSI.WCP
{
  public static class WCP
  {
    static WCP_Server server;

    public static WCP_Server Server
    {
      get { return WCP.server; }
      set { WCP.server = value; }
    }
  }

  public class WCP_Server:SecureTcpServer
  {
    private const String WCP_STX = "<WCP_Message>";
    private const String WCP_ETX = "</WCP_Message>";
    private const Int32 WCP_MESSAGE_MAX_LENGTH = Int32.MaxValue; //0x7FFFFFFF; //32 * 1024; 
    private IXmlSink     sink;

    /// <summary>
    /// Send an Xml message
    /// </summary>
    /// <param name="TerminalId"></param>
    /// <param name="SessionId"></param>
    /// <param name="Xml"></param>
    public bool SendTo (Int32 TerminalId, Int64 SessionId, String Xml)
    {
      m_clients_rw_lock.AcquireReaderLock(System.Threading.Timeout.Infinite);
      try
      {
        foreach ( SecureTcpClient client in Clients )
        {
          if ( client.InternalId == TerminalId )
          {
            if ( SessionId == 0
                || SessionId == client.SessionId )
            {
              client.Send (Xml);

              return true;
            }
          }
        }
        return false;
      }
      finally
      {
        m_clients_rw_lock.ReleaseReaderLock();
      }
    }

    /// <summary>
    /// Returns if a kiosk is connected
    /// </summary>
    /// <param name="TerminalId"></param>
    /// <returns></returns>
    public bool IsConnected(Int32 TerminalId)
    {
      m_clients_rw_lock.AcquireReaderLock(System.Threading.Timeout.Infinite);
      try
      {
        foreach (SecureTcpClient client in Clients)
        {
          if (client.InternalId == TerminalId)
          {
            return client.IsConnected;
          }
        }
        return false;
      }
      finally
      {
        m_clients_rw_lock.ReleaseReaderLock();
      }
    }

    public IXmlSink Sink
    {
      set
      {
        sink = value;
      }
    }


    public WCP_Server (IPEndPoint IPEndPoint) : base (IPEndPoint)
    {
      this.SetXmlServerParameters (WCP_STX, WCP_ETX, WCP_MESSAGE_MAX_LENGTH, Encoding.UTF8);
    }

    public override void ClientConnected (SecureTcpClient SecureClient)
    {
      base.ClientConnected (SecureClient);
    }

    public override void ClientDisconnected (SecureTcpClient SecureClient)
    {
      Common.BatchUpdate.TerminalSession.CloseSession(SecureClient.InternalId, 
                                                      SecureClient.SessionId, 
                                                      (int) WCP_SessionStatus.Disconnected);
      base.ClientDisconnected (SecureClient);
    }

    public override void XmlMesageReceived(SecureTcpClient SecureTcpClient, string XmlMessage)
    {
      try
      {
        sink.Process(this, SecureTcpClient, XmlMessage);
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
        Log.Message("EXEP Msg: " + XmlMessage);
      }
    }

  } // Class SecureTcpServer
}
