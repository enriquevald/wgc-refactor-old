//------------------------------------------------------------------------------
// Copyright © 2012 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_SASMetersThread.cs
// 
//   DESCRIPTION: SASMeters updates
// 
//        AUTHOR: Sergi Martínez
// 
// CREATION DATE: 31-OCT-2013
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 31-OCT-2013 SMN    First release.
// 19-NOV-2013 SMN    Add Log.Warning into finally section
// 11-DIC-2013 NMR    Filtered ticket-meters
// 21-DIC-2013 NMR    Added exclusive access to db-table sas_meters
// 27-DEC-2013 RRR    Added Process SASMetersHistory
// 09-JAN-2014 RRR    Resolved Bug WIGOSTITO-954: Errores varios en el log del servicio WCP
// 20-JAN-2014 JRM    Resolved Bug WIGOSTITO-992,Also SaS Meter history PK violation problem
// 11-FEB-2015 AMF    Fixed Bug WIGOSTITO-1058: Error SqlDateTime
// 12-FEB-2015 MPO & AMF    Fixed Bug WIGOSTITO-1063: Error on InsertSASMetersHystory when insert.
// 24-FEB-2015 JRM    Fixed various bugs with historification of sas meters.
// 02-MAR-2014 XIT    Fixed Ticket historification and code refactored
// 07-MAY-2014 MPO    Error when historicize tickets hourly (WIG-878).
// 26-MAY-2014 HBB    Moved ENUM_METERS_TYPE to SASMeters.cs
// 07-SEP-2015 MPO    TFS ITEM 2194: SAS16: Estadísticas multi-denominación: WCP
// 14-FEB-2017 JBP    Bug 16497:Alarma "Contadores sin reportar en las últimas 24 horas" aparece sin cumplir la condición de 24h
//------------------------------------------------------------------------------

using System;
using System.Threading;
using System.Data.SqlClient;
using WSI.Common;
using System.Data;
using System.Text;
using WSI.Common.TITO;
using WSI.WCP;

namespace WSI.WCP
{
  public static class SASMeters
  {

    #region Members

    #endregion

    //------------------------------------------------------------------------------
    // PURPOSE : Initializes SASMeters thread
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public static void Init()
    {
      Thread _thread;

      _thread = new Thread(RefreshSASMetersThread);
      _thread.Name = "UpdateSASMetersThread";
      _thread.Start();

    } // Init


    //------------------------------------------------------------------------------
    // PURPOSE : Update Terminal SAS Meters
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    static private void RefreshSASMetersThread()
    {
      SAS_Meter.ENUM_METERS_TYPE _meter_type;
      DataTable _tsm_machine; // meters denom = 0
      DataTable _tsm_denom;   // meters denom <> 0
      DateTime _now;
      DateTime _now_hour;
      DateTime _prv_hour;
      DateTime _last_hour_reported;
      Int32 _sleep_in_ms;

      _tsm_denom = new DataTable();

      WCP_SASMeters.GetHourlyInterval(WGDB.Now, out _now_hour, out _prv_hour);

      _last_hour_reported = _prv_hour;

      while (true)
      {
        try
        {
          DateTime _aux;
          TimeSpan _ts;

          _now = WGDB.Now;
          _aux = new DateTime(_now.Year, _now.Month, _now.Day, _now.Hour, _now.Minute, 0);
          _aux = _aux.AddMinutes(1);
          _ts = _aux - _now;

          _sleep_in_ms = (int)Math.Max(_ts.TotalMilliseconds, 0);

          Thread.Sleep(_sleep_in_ms);

          if (!Services.IsPrincipal("WCP"))
          {
            _last_hour_reported = WGDB.Now;

            continue;
          }

          _meter_type = SAS_Meter.ENUM_METERS_TYPE.BILLS_AND_TICKETS;
          _now = WGDB.Now;


          WCP_SASMeters.GetHourlyInterval(_now, out _now_hour, out _prv_hour);

          if (_now_hour > _last_hour_reported)
          {
            _meter_type = SAS_Meter.ENUM_METERS_TYPE.ALL;
          }

          // Delete Meters with denomination null
          if (GeneralParam.GetBoolean("WCP", "SasMetersCleanDenomEmpty", false))
          {
            if (!SAS_Meter.CleanDenomEmpty())
            {
              Log.Error("WCP_SASMeters.CleanDenomEmpty in hour historification");
              continue;
            }
          }

          using (DB_TRX _db_trx = new DB_TRX())
          {
            //All counters
            if (!SAS_Meter.GetSasMeters(_meter_type, 0, true, out _tsm_machine, _db_trx.SqlTransaction))
            {
              Log.Error("WCP_SASMeters.GetSasMeters in hour historification");
              continue;
            }
          }

          if (!SAS_Meter.SplitDTMachineDenomMetersWithValue(ref _tsm_machine, ref _tsm_denom))
          {
            Log.Error("WCP_SASMeters.SplitDTMachineDenomMeters");
            continue;
          }

          if (!WCP_SASMeters.ProcessTerminalSasMeters(_meter_type, _now_hour, _tsm_machine))
          {
            Log.Error("WCP_SASMeters.NewProcessTerminalSasMeters in hour historification");
            continue;
          }

          if (!WCP_SASMeters.ProcessMachineDenomStatsPerHour(_meter_type, _tsm_denom))
          {
            Log.Error("WCP_SASMeters.ProcessMachineDenomStatsPerHour");
            continue;
          }

          if (_meter_type == SAS_Meter.ENUM_METERS_TYPE.ALL) // Hourly period
          {
            using (DB_TRX _db_trx = new DB_TRX())
            {
              if (!WCP_TerminalStatus.Trx_UpdateTerminalStatus(_db_trx.SqlTransaction))
              {
                Log.Error("WCP_TerminalStatus.Trx_UpdateTerminalStatus in hour historification");
                continue;
              }

              if (!WCP_SASMeters.GenerateUnreportedMetersAlarm(_db_trx.SqlTransaction))
              {
                Log.Error("Error WCP_SASMeters.GenerateUnreportedMetersAlarm(MeterType, Trx)");
                continue;
              }

              _db_trx.Commit();

              _last_hour_reported = _now_hour;
            }
          }

        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }
      } // while
    }

  } // class SASMeters

} // namespace WSI.WCP
