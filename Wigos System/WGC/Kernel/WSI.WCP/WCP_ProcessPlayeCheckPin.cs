//------------------------------------------------------------------------------
// Copyright � 2014 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_ProcessPlayeCheckPin
// 
//   DESCRIPTION: WCP player PIN validation
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 03-JUL-2014 JMM    First version.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using WSI.WCP;
using WSI.Common;

namespace WSI.WCP
{
  public static partial class WCP_Processor
  {
    //------------------------------------------------------------------------------
    // PURPOSE: Process Tx related to WCP_MsgTypes.WCP_MsgPlayerCheckPin
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    //   NOTES:
    private static void WCP_ProcessPlayerCheckPin(WCP_Message Request, WCP_Message Response)
    {
      WCP_MsgPlayerCheckPin _request;
      WCP_MsgPlayerCheckPinReply _response;
      WCP_WKT.WKT_Player _player;
      long _account_id;
      WCP_ResponseCodes _rc;

      try
      {
        _request = (WCP_MsgPlayerCheckPin)Request.MsgContent;
        _response = (WCP_MsgPlayerCheckPinReply)Response.MsgContent;

        using (DB_TRX _db_trx = new DB_TRX())
        {
          _player = new WCP_WKT.WKT_Player();
          _player.Pin = _request.Player.Pin;
          _player.Trackdata = _request.Player.Trackdata;

          _player.Login(out _account_id, out _rc);
          Response.MsgHeader.ResponseCode = _rc;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        Response.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_WKT_ERROR;
      }
    }
  }
}
