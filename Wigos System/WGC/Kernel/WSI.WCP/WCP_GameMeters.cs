//------------------------------------------------------------------------------
// Copyright � 2010 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_GameMeters.cs
// 
//   DESCRIPTION: WCP_GameMeters class
// 
//        AUTHOR: Armando Alva
// 
// CREATION DATE: 08-MAR-2010
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 08-MAR-2010 AAV    First release.
// 18-OCT-2011 JML    Add alarm register for Big Increment.
// 14-DEC-2011 ACC    Only One Game per Sas Id in memory
// 24-JUL-2012 ACC    Extended Meters: Add maximum meter values. Delta meters is zero if any maximum changed.
// 28-MAR-2013 RCI & HBB    Added column SPH_THEORETICAL_WON_AMOUNT in table SALES_PER_HOUR. Needed to calculate the theoretical payout %.
// 04-DEC-2013 JCOR   Control for hanpay in Game Meters.
// 07-AUG-2014 ACC    Add Progressive Jackpot Cents into Meters
// 22-APR-2015 FOS    Update LastGamePlayedId in table terminals.
// 01-SEP-2017 JML    PBI 29479: WIGOS-4700 Promotional credit reported as cashable after card out/in
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using WSI.Common;
using System.Data;

namespace WSI.WCP
{
  //------------------------------------------------------------------------------
  // PURPOSE : GameMeters class 
  //
  //   NOTES :
  //
  public class GameMeters
  {
    public String GameId;
    public String GameBaseName;
    public Int32 DenominationCents;
    public Int64 PlayedCount;
    public Int64 PlayedCountMaxValue;
    public Int64 PlayedCents;
    public Int64 PlayedMaxValueCents;
    public Int64 WonCount;
    public Int64 WonCountMaxValue;
    public Int64 WonCents;
    public Int64 WonMaxValueCents;
    public Int64 JackpotCents;
    public Int64 JackpotMaxValueCents;
    // ACC 04-AUG-2014 Add Jackpot Progressive Cents into Machine Meters
    public Int64 ProgressiveJackpotCents = 0;
    public Int64 ProgressiveJackpotMaxValueCents = 0;

    #region Operators Overloading

    public Boolean IsEqual(GameMeters GM2)
    {
      if (this.GameId == GM2.GameId &&
          this.GameBaseName == GM2.GameBaseName &&
          this.DenominationCents == GM2.DenominationCents &&
          this.PlayedCount == GM2.PlayedCount &&
          this.PlayedCents == GM2.PlayedCents &&
          this.WonCount == GM2.WonCount &&
          this.WonCents == GM2.WonCents &&
          this.JackpotCents == GM2.JackpotCents &&
          this.PlayedCountMaxValue == GM2.PlayedCountMaxValue &&
          this.PlayedMaxValueCents == GM2.PlayedMaxValueCents &&
          this.WonCountMaxValue == GM2.WonCountMaxValue &&
          this.WonMaxValueCents == GM2.WonMaxValueCents &&
          this.JackpotMaxValueCents == GM2.JackpotMaxValueCents &&
          this.ProgressiveJackpotCents == GM2.ProgressiveJackpotCents &&
          this.ProgressiveJackpotMaxValueCents == GM2.ProgressiveJackpotMaxValueCents)
      {
        return true;
      }
      else
      {
        return false;
      }
    }

    public Int32 SasIdFromBaseName()
    {
      return WCP_GameMeters.GetSasIdFromBaseName(this.GameBaseName);
    }

    #endregion
  }

  //------------------------------------------------------------------------------
  // PURPOSE : WCP_GameMeters class for process game meters
  //
  //   NOTES :
  //
  public static class WCP_GameMeters
  {
    //------------------------------------------------------------------------------
    // PURPOSE : Process new wcp GameMeters message
    //
    //  PARAMS :
    //      - INPUT :
    //          - NewGameMeters: Wcp message
    //          - TerminalId
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - true: update sales correctly.
    //      - false: otherwise.
    //
    //   NOTES : OBSOLETE FUNCTION. Now use BU_ProcessGameMeters
    //
    public static Boolean ProcessGameMeters(Int32 TerminalId, Int64 WcpSequenceId, WCP_MsgReportGameMeters NewGameMeters)
    {
      SqlConnection _sql_conn;
      SqlTransaction _sql_trx;
      Int64 _db_wcp_sequence_id;
      Int32 _elapsed_minutes;
      Boolean _big_increment;

      GameMeters _new_game_meters;
      GameMeters _old_game_meters;
      GameMeters _delta_game_meters;
      GameMeters _zero_game_meters;

      int _tick0;
      int _tick1;
      int _tick2;
      long _interval_select;
      long _interval_update;
      long _interval_all_process;

      _interval_select = 0;
      _interval_update = 0;
      _interval_all_process = 0;
      _tick0 = Environment.TickCount;

      _sql_conn = null;
      _sql_trx = null;
      _zero_game_meters = new GameMeters();

      try
      {
        //
        // Connect to DB
        //
        _sql_conn = WGDB.Connection();
        if (_sql_conn == null)
        {
          return false;
        }

        _sql_trx = _sql_conn.BeginTransaction();

        // Get wcp message game meters
        GetMsgReportGameMeters(NewGameMeters, out _new_game_meters);

        _tick1 = Environment.TickCount;

        // Get database Game Meters
        if (!DB_GetGameMeters(TerminalId, _new_game_meters.GameBaseName, out _old_game_meters, out _db_wcp_sequence_id, out _elapsed_minutes, _sql_trx))
        {
          // Insert Game Meters
          if (!DB_InsertGameMeters(TerminalId, WcpSequenceId, _new_game_meters, _sql_trx))
          {
            Log.Warning("Inserting Game Meters. TerminalId: " + TerminalId.ToString() + ", WcpSequenceId: " + WcpSequenceId.ToString());

            return false;
          }

          _sql_trx.Commit();

          return true;
        }

        _tick2 = Environment.TickCount;
        _interval_select = Misc.GetElapsedTicks(_tick1, _tick2);


        if (_db_wcp_sequence_id >= WcpSequenceId)
        {
          _sql_trx.Commit();

          return true;
        }

        _old_game_meters.GameId = _new_game_meters.GameId;
        _old_game_meters.GameBaseName = _new_game_meters.GameBaseName;

        if (_new_game_meters.IsEqual(_old_game_meters))
        {
          _sql_trx.Commit();

          return true;
        }

        GetDeltaGameMeters(TerminalId, _old_game_meters, _new_game_meters, out _delta_game_meters, out _big_increment);

        _delta_game_meters.GameId = NewGameMeters.GameId;
        _delta_game_meters.GameBaseName = NewGameMeters.GameBaseName;
        _delta_game_meters.DenominationCents = NewGameMeters.DenominationCents;

        _tick1 = Environment.TickCount;

        // AJQ 01-JUL-2010, Insert Handpay, that arrive in Jackpot counters.
        if (_delta_game_meters.JackpotCents > 0)
        {
          Boolean _insert_hp;
          WSI.Common.Terminal.TerminalInfo _terminal_info;

          _insert_hp = true;
          if (WSI.Common.Terminal.Trx_GetTerminalInfo(TerminalId, out _terminal_info, _sql_trx))
          {
            if (_terminal_info.SASFlagActivated(SAS_FLAGS.USE_HANDPAY_INFORMATION))
            {
              _insert_hp = false;
            } // if
          }

          if (_insert_hp)
          {
            if (!DB_InsertHandpay(TerminalId, _delta_game_meters, _sql_trx))
            {
              Log.Warning("Updating Game Meters Handpay. TerminalId: " + TerminalId.ToString() + ", Amount: " + _delta_game_meters.JackpotCents.ToString());
              return false;
            } // if
          }
        } // if

        // Update Game Meters
        if (!DB_UpdateGameMeters(TerminalId, WcpSequenceId, _new_game_meters, _old_game_meters, _delta_game_meters, _sql_trx))
        {
          Log.Warning("Updating Game Meters. TerminalId: " + TerminalId.ToString() + ", WcpSequenceId: " + WcpSequenceId.ToString());

          return false;
        }

        _tick2 = Environment.TickCount;
        _interval_update = Misc.GetElapsedTicks(_tick1, _tick2);

        // Register Terminal/Game
        DbCache.RegisterTerminalGame(TerminalId, _delta_game_meters.GameId, _sql_trx);

        _sql_trx.Commit();

        _tick2 = Environment.TickCount;
        _interval_all_process = Misc.GetElapsedTicks(_tick0, _tick2);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
      finally
      {
        if (_sql_trx != null)
        {
          if (_sql_trx.Connection != null)
          {
            _sql_trx.Rollback();
          }
          _sql_trx = null;
        }

        if (_sql_conn != null)
        {
          _sql_conn.Close();
          _sql_conn = null;
        }

        if (_interval_select > 3000
            || _interval_update > 3000
            || _interval_all_process > 5000)
        {
          Log.Warning("Terminal: " + TerminalId.ToString() + ". Message GameMeters times: Select/Update/Total = " + _interval_select.ToString() + "/" + _interval_update.ToString() + "/" + _interval_all_process.ToString());
        }
      }

      return true;

    } // ProcessGameMeters

    //------------------------------------------------------------------------------
    // PURPOSE : Process new wcp GameMeters message
    //
    //  PARAMS :
    //      - INPUT :
    //          - NewGameMeters: Wcp message
    //          - TerminalId
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - true: update sales correctly.
    //      - false: otherwise.
    //
    //   NOTES :
    //
    public static Boolean BU_ProcessGameMeters(Int32 TerminalId,
                                                Int64 SessionId,
                                                Int64 WcpSequenceId,
                                                WCP_MsgReportGameMeters NewGameMeters,
                                                WSI.WCP.IWcpTaskStatus WcpTaskStatus,
                                                SqlTransaction Trx)
    {
      Int64 _db_wcp_sequence_id;

      GameMeters _new_game_meters;
      GameMeters _old_game_meters;
      GameMeters _delta_game_meters;
      GameMeters _zero_game_meters;
      DataRow _row_game_meters;
      String _name;
      String _provider_id;
      Boolean _big_increment;
      Int64 _decrement;

      int _tick0;
      int _tick1;
      int _tick2;
      long _interval_select;
      long _interval_update;
      long _interval_term_game;
      long _interval_all_process;

      Boolean _load_received_meters;

      _interval_select = 0;
      _interval_update = 0;
      _interval_term_game = 0;
      _interval_all_process = 0;
      _tick0 = Environment.TickCount;

      _zero_game_meters = new GameMeters();

      try
      {
        // Get wcp message game meters
        GetMsgReportGameMeters(NewGameMeters, out _new_game_meters);

        _tick1 = Environment.TickCount;

        _load_received_meters = false;

        //
        // ACC 14-DEC-2011 Only One Game per Sas Id in memory
        //
        // Meters With this SasId loaded ?
        //
        if (!WCP_BU_GameMeters.GetGameMetersBySasId(TerminalId, _new_game_meters, out _old_game_meters, out _db_wcp_sequence_id, out _row_game_meters, Trx))
        {
          //
          // NOT EXIST SAS ID in memory
          //
          //  Load Last Meters With this SasId from database
          //
          //    If NOT Exist in database --> load Received.
          //
          if (!WCP_BU_GameMeters.ReadLastMetersFromDatabaseBySasId(TerminalId, _new_game_meters, out _old_game_meters, out _db_wcp_sequence_id, out _row_game_meters, Trx))
          {
            _load_received_meters = true;
          }
        }

        //
        // ACC 14-DEC-2011 Only One Game With this SasId in memory
        //
        // Part GameId match name ?
        //
        if (!_load_received_meters)
        {
          if (_new_game_meters.GameBaseName != _old_game_meters.GameBaseName)
          {
            //
            // Unload existing game with the same Sas Id: _old_game_meters
            //
            if (!WCP_BU_GameMeters.Unload(TerminalId, _old_game_meters.GameBaseName))
            {
              Log.Warning("BU GameMeters, Unload fails. TerminalId/SessionId/SequenceId=" + TerminalId + "/" + SessionId + "/" + WcpSequenceId);

              return false;
            }

            _load_received_meters = true;
          }
        }

        //
        // Load received meters
        //
        if (_load_received_meters)
        {
          // Search in Memory & Database
          if (!WCP_BU_GameMeters.Get(TerminalId, _new_game_meters.GameBaseName, out _old_game_meters, out _db_wcp_sequence_id, out _row_game_meters, Trx))
          {
            if (!Common.BatchUpdate.TerminalSession.GetData(TerminalId, SessionId, out _provider_id, out _name))
            {
              Log.Warning("BU GameMeters, TerminalSession Not Active. TerminalId/SessionId/SequenceId=" + TerminalId + "/" + SessionId + "/" + WcpSequenceId);

              return false;
            }

            // Insert Game Meters
            if (!WCP_BU_GameMeters.Insert(TerminalId, _provider_id, _name, WcpSequenceId, _new_game_meters, WcpTaskStatus))
            {
              Log.Warning("BU GameMeters, Insert failed. TerminalId/SessionId/SequenceId=" + TerminalId + "/" + SessionId + "/" + WcpSequenceId);

              return false;
            }

            return true;
          }
        }

        //////////////// Get database Game Meters
        //////////////if (!WCP_BU_GameMeters.Get(TerminalId, _new_game_meters.GameBaseName, out _old_game_meters, out _db_wcp_sequence_id, out _row_game_meters, Trx))
        //////////////{
        //////////////  if (!Common.BatchUpdate.TerminalSession.GetData(TerminalId, SessionId, out _provider_id, out _name))
        //////////////  {
        //////////////    Log.Warning("BU GameMeters, TerminalSession Not Active. TerminalId/SessionId/SequenceId=" + TerminalId + "/" + SessionId + "/" + WcpSequenceId);

        //////////////    return false;
        //////////////  }

        //////////////  // Insert Game Meters
        //////////////  if (!WCP_BU_GameMeters.Insert(TerminalId, _provider_id, _name, WcpSequenceId, _new_game_meters, WcpTaskStatus))
        //////////////  {
        //////////////    Log.Warning("BU GameMeters, Insert failed. TerminalId/SessionId/SequenceId=" + TerminalId + "/" + SessionId + "/" + WcpSequenceId);

        //////////////    return false;
        //////////////  }

        //////////////  return true;
        //////////////}

        _tick2 = Environment.TickCount;
        _interval_select = Misc.GetElapsedTicks(_tick1, _tick2);

        if (_db_wcp_sequence_id >= WcpSequenceId)
        {
          // RCI 21-JUL-2010: Message arrived is previous than message in DB.
          //                  Don't treat it, but mark Task as OK and return.
          WcpTaskStatus.SetResponseCode(WCP_ResponseCodes.WCP_RC_OK);

          return true;
        }

        _old_game_meters.GameId = _new_game_meters.GameId;
        _old_game_meters.GameBaseName = _new_game_meters.GameBaseName;

        if (_new_game_meters.IsEqual(_old_game_meters))
        {
          // RCI 21-JUL-2010: No changed in GameMeters counters. Mark Task as OK and return.
          WcpTaskStatus.SetResponseCode(WCP_ResponseCodes.WCP_RC_OK);

          return true;
        }

        // All "delta" as zero
        _big_increment = false;
        _delta_game_meters = new GameMeters();

        //
        // Calculate increments (only if match name with last received meters).
        //
        if (!_load_received_meters && !NewGameMeters.IgnoreDeltas)
        {
          GetDeltaGameMeters(TerminalId, _old_game_meters, _new_game_meters, out _delta_game_meters, out _big_increment);
        }

        if (_big_increment)
        {
          // AJQ 24-FEB-2011, Went PlayedCount back?
          if (_old_game_meters.PlayedCount > _new_game_meters.PlayedCount)
          {
            // ACC 26-JAN-2011 No update game meters if played counter back 5 or less units
            if (_old_game_meters.PlayedCount - _new_game_meters.PlayedCount <= 5)
            {
              _decrement = _old_game_meters.PlayedCount - _new_game_meters.PlayedCount;

              Log.Message("*** Game Meters: PlayedCount went back: " + _decrement.ToString() + " units. Game Meters NOT updated for Terminal: " + TerminalId.ToString() + ".");

              // No update GameMeters. Mark Task as OK and return.
              WcpTaskStatus.SetResponseCode(WCP_ResponseCodes.WCP_RC_OK);

              return true;
            }
          }
        }

        _delta_game_meters.GameId = NewGameMeters.GameId;
        _delta_game_meters.GameBaseName = NewGameMeters.GameBaseName;
        _delta_game_meters.DenominationCents = NewGameMeters.DenominationCents;

        _tick1 = Environment.TickCount;

        // Update Game Meters
        if (!WCP_BU_GameMeters.Update(TerminalId, WcpSequenceId, _new_game_meters, _old_game_meters, _delta_game_meters, _row_game_meters, WcpTaskStatus))
        {
          Log.Warning("BU Updating Game Meters. TerminalId: " + TerminalId.ToString() + ", WcpSequenceId: " + WcpSequenceId.ToString());

          return false;
        }

        _tick2 = Environment.TickCount;
        _interval_update = Misc.GetElapsedTicks(_tick1, _tick2);
        _tick1 = Environment.TickCount;

        // Register Terminal/Game
        DbCache.RegisterTerminalGame(TerminalId, _delta_game_meters.GameId, Trx);

        _tick2 = Environment.TickCount;
        WCP_BU_GameMeters.m_time_register_term_game += Misc.GetElapsedTicks(_tick1);

        _interval_term_game = Misc.GetElapsedTicks(_tick1, _tick2);
        _interval_all_process = Misc.GetElapsedTicks(_tick0, _tick2);

        // RCI 21-JUL-2010: Don't mark Task ResponseCode yet.
        //                  There are modifications in GameMeters. Wait for UpdateDB() to do it.
        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
      finally
      {
        if (_interval_select > 300
            || _interval_update > 300
            || _interval_all_process > 600)
        {
          Log.Warning("BU Terminal: " + TerminalId.ToString() + ". Message GameMeters times: Select/Update/TermGame/Total = " + _interval_select.ToString() + "/" + _interval_update.ToString() + "/" + _interval_term_game.ToString() + "/" + _interval_all_process.ToString());
        }
      }
    } // BU_ProcessGameMeters

    #region Private Functions

    //------------------------------------------------------------------------------
    // PURPOSE : Get Database Game Meters
    //
    //  PARAMS :
    //      - INPUT :
    //          - TerminalId
    //          - GameBaseName
    //          - Trx
    //
    //      - OUTPUT :
    //          - DbGameMeters
    //          - DbWcpSequenceId
    //          - ElapsedHours
    //
    // RETURNS :
    //      - true: successful.
    //      - false: not found or error.
    //
    //   NOTES :
    //
    private static Boolean DB_GetGameMeters(Int32 TerminalId, String GameBaseName, out GameMeters DbGameMeters, out Int64 DbWcpSequenceId, out Int32 ElapsedMinutes, SqlTransaction Trx)
    {
      String _str_sql;
      SqlCommand _sql_command;
      SqlDataReader _sql_reader;

      DbGameMeters = new GameMeters();
      DbWcpSequenceId = 0;
      ElapsedMinutes = 0;

      _sql_reader = null;

      try
      {
        _str_sql = "";
        _str_sql += "SELECT   GM_WCP_SEQUENCE_ID";
        _str_sql += "       , GM_DENOMINATION";
        _str_sql += "       , GM_PLAYED_COUNT";
        _str_sql += "       , GM_PLAYED_AMOUNT";
        _str_sql += "       , GM_WON_COUNT";
        _str_sql += "       , GM_WON_AMOUNT";
        _str_sql += "       , GM_JACKPOT_AMOUNT";
        _str_sql += "       , DATEDIFF (MINUTE, GM_LAST_REPORTED, GETDATE()) ";
        _str_sql += "  FROM   GAME_METERS ";
        _str_sql += " WHERE   GM_TERMINAL_ID    = @pTerminalID   ";
        _str_sql += "   AND   GM_GAME_BASE_NAME = @pGameBaseName ";

        _sql_command = new SqlCommand(_str_sql);
        _sql_command.Connection = Trx.Connection;
        _sql_command.Transaction = Trx;

        // Parameters
        _sql_command.Parameters.Add("@pTerminalID", SqlDbType.Int).Value = TerminalId;
        _sql_command.Parameters.Add("@pGameBaseName", SqlDbType.VarChar, 50).Value = GameBaseName;

        _sql_reader = _sql_command.ExecuteReader();

        if (_sql_reader.Read())
        {
          DbWcpSequenceId = _sql_reader.GetInt64(0);
          DbGameMeters.DenominationCents = (Int32)(_sql_reader.GetDecimal(1) * 100);
          DbGameMeters.PlayedCount = _sql_reader.GetInt64(2);
          DbGameMeters.PlayedCents = (Int64)(_sql_reader.GetDecimal(3) * 100);
          DbGameMeters.WonCount = _sql_reader.GetInt64(4);
          DbGameMeters.WonCents = (Int64)(_sql_reader.GetDecimal(5) * 100);
          DbGameMeters.JackpotCents = (Int64)(_sql_reader.GetDecimal(6) * 100);
          ElapsedMinutes = _sql_reader.GetInt32(7);
        }
        else
        {
          return false;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
      finally
      {
        if (_sql_reader != null)
        {
          _sql_reader.Close();
          _sql_reader.Dispose();
          _sql_reader = null;
        }
      }

      return true;

    } // DB_GetGameMeters

    //------------------------------------------------------------------------------
    // PURPOSE : Copy Wcp Msg Game Meters in GameMeters class
    //
    //  PARAMS :
    //      - INPUT :
    //          - WcpMsgGameMeters: Wcp report message 
    //
    //      - OUTPUT :
    //          - NewGameMeters
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private static void GetMsgReportGameMeters(WCP_MsgReportGameMeters WcpMsgGameMeters, out GameMeters NewGameMeters)
    {
      NewGameMeters = new GameMeters();

      NewGameMeters.GameId = WcpMsgGameMeters.GameId;
      NewGameMeters.GameBaseName = WcpMsgGameMeters.GameBaseName;
      NewGameMeters.DenominationCents = WcpMsgGameMeters.DenominationCents;
      NewGameMeters.PlayedCount = WcpMsgGameMeters.PlayedCount;
      NewGameMeters.PlayedCountMaxValue = WcpMsgGameMeters.PlayedCountMaxValue;
      NewGameMeters.PlayedCents = WcpMsgGameMeters.PlayedCents;
      NewGameMeters.PlayedMaxValueCents = WcpMsgGameMeters.PlayedMaxValueCents;
      NewGameMeters.WonCount = WcpMsgGameMeters.WonCount;
      NewGameMeters.WonCountMaxValue = WcpMsgGameMeters.WonCountMaxValue;
      NewGameMeters.WonCents = WcpMsgGameMeters.WonCents;
      NewGameMeters.WonMaxValueCents = WcpMsgGameMeters.WonMaxValueCents;
      NewGameMeters.JackpotCents = WcpMsgGameMeters.JackpotCents;
      NewGameMeters.JackpotMaxValueCents = WcpMsgGameMeters.JackpotMaxValueCents;
      // ACC 05-AUG-2014 Add ProgressiveJackpot Cents into Game Meters
      NewGameMeters.ProgressiveJackpotCents = WcpMsgGameMeters.ProgressiveJackpotCents;
      NewGameMeters.ProgressiveJackpotMaxValueCents = WcpMsgGameMeters.ProgressiveJackpotMaxValueCents;

    } // GetMsgReportGameMeters

    //------------------------------------------------------------------------------
    // PURPOSE : Get Sas id from base name
    //
    //  PARAMS :
    //      - INPUT :
    //          - GameBaseName
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public static Int32 GetSasIdFromBaseName(String GameBaseName)
    {
      Int32 _part_game_id;

      if (GameBaseName.Length < 8)
      {
        return -1;
      }

      if (GameBaseName.Substring(0, 6) != "SAS - ")
      {
        return -1;
      }

      if (!Int32.TryParse(GameBaseName.Substring(6, 2), out _part_game_id))
      {
        return -1;
      }

      return _part_game_id;
    } // GetSasIdFromBaseName

    //------------------------------------------------------------------------------
    // PURPOSE : Get Delta all Game Meters 
    //
    //  PARAMS :
    //      - INPUT :
    //          - TerminalId
    //          - OldGameMeters
    //          - NewGameMeters
    //
    //      - OUTPUT :
    //          - DeltaGameMeters
    //
    // RETURNS :
    //      - GameMeters
    //
    //   NOTES :
    //
    public static void GetDeltaGameMeters(Int32 TerminalId, GameMeters OldGameMeters, GameMeters NewGameMeters, out GameMeters DeltaGameMeters, out Boolean BigIncrement)
    {
      Boolean _big;
      Boolean _big_played_count;
      Boolean _big_played_cents;
      Boolean _big_won_count;
      Boolean _big_won_cents;
      Boolean _big_jackpot_cents;
      Boolean _big_progressive_jackpot_cents;

      String _terminal_name;
      String _provider_id;
      long _sesion_id;
      Boolean _alarm;
      String _alarm_name;
      AlarmCode _alarm_code;

      GameMeters _delta_game_meters_for_adjusments;

      _alarm = false;
      _alarm_name = "Not Set";
      _alarm_code = AlarmCode.Unknown;
      BigIncrement = false;
      DeltaGameMeters = new GameMeters();

      _big = false;
      _big_played_count = false;
      _big_played_cents = false;
      _big_won_count = false;
      _big_won_cents = false;
      _big_jackpot_cents = false;
      _big_progressive_jackpot_cents = false;

      _delta_game_meters_for_adjusments = DeltaGameMeters;

      try
      {
        //
        // Check Game Meters Reset
        // 
        if (NewGameMeters.JackpotCents == 0 
            && NewGameMeters.PlayedCents == 0
            && NewGameMeters.PlayedCount == 0
            && NewGameMeters.WonCents == 0
            && NewGameMeters.WonCount == 0
            && ( OldGameMeters.JackpotCents != 0
              || OldGameMeters.PlayedCents != 0
              || OldGameMeters.PlayedCount != 0
              || OldGameMeters.WonCents != 0
              || OldGameMeters.WonCount != 0 ))
        {
          _alarm = true;
          _alarm_name = "*** Game Meters Reset ***";
          _alarm_code = AlarmCode.TerminalSystem_GameMeterReset;

          DeltaGameMeters = new GameMeters();
          BigIncrement = false;

          return;
        }

        //
        // Check Denomination
        // 
        if (NewGameMeters.DenominationCents != OldGameMeters.DenominationCents)
        {
          _alarm = true;
          _alarm_name = "*** Game Meters Denomination Changed ***";
          _alarm_code = AlarmCode.TerminalSystem_GameMeterDenomination;

          DeltaGameMeters = new GameMeters();
          BigIncrement = false;

          return;
        }

        //
        // Check BigIncrement
        // 
        _big_played_count = TerminalMeterGroup.GetDeltaMeter(TerminalId, "GamePlayedCount", OldGameMeters.PlayedCount, NewGameMeters.PlayedCount, OldGameMeters.PlayedCountMaxValue, NewGameMeters.PlayedCountMaxValue, BigIncrementsData.QuantityGamesPlayed(TerminalId), out DeltaGameMeters.PlayedCount);
        _big_played_cents = TerminalMeterGroup.GetDeltaMeter(TerminalId, "GamePlayedCents", OldGameMeters.PlayedCents, NewGameMeters.PlayedCents, OldGameMeters.PlayedMaxValueCents, NewGameMeters.PlayedMaxValueCents, BigIncrementsData.CentsPlayed(TerminalId), out DeltaGameMeters.PlayedCents);
        _big_won_count = TerminalMeterGroup.GetDeltaMeter(TerminalId, "GameWonCount", OldGameMeters.WonCount, NewGameMeters.WonCount, OldGameMeters.WonCountMaxValue, NewGameMeters.WonCountMaxValue, BigIncrementsData.QuantityGamesWon(TerminalId), out DeltaGameMeters.WonCount);
        _big_won_cents = TerminalMeterGroup.GetDeltaMeter(TerminalId, "GameWonCents", OldGameMeters.WonCents, NewGameMeters.WonCents, OldGameMeters.WonMaxValueCents, NewGameMeters.WonMaxValueCents, BigIncrementsData.CentsWon(TerminalId), out DeltaGameMeters.WonCents);
        _big_jackpot_cents = TerminalMeterGroup.GetDeltaMeter(TerminalId, "GameJackpotCents", OldGameMeters.JackpotCents, NewGameMeters.JackpotCents, OldGameMeters.JackpotMaxValueCents, NewGameMeters.JackpotMaxValueCents, BigIncrementsData.CentsJackpot(TerminalId), out DeltaGameMeters.JackpotCents);

        DeltaGameMeters.WonCents += DeltaGameMeters.JackpotCents;
        // DeltaGameMeters.JackpotCents = 0;

        // ACC 05-AUG-2014 Progressive Jackpot Cents 
        if (OldGameMeters.ProgressiveJackpotCents != -1)
        {
          _big_progressive_jackpot_cents = TerminalMeterGroup.GetDeltaMeter(TerminalId, "GameProgressiveJackpotCents", OldGameMeters.ProgressiveJackpotCents, NewGameMeters.ProgressiveJackpotCents, OldGameMeters.ProgressiveJackpotMaxValueCents, NewGameMeters.ProgressiveJackpotMaxValueCents, BigIncrementsData.CentsJackpot(TerminalId), out DeltaGameMeters.ProgressiveJackpotCents);
        }

        _big = false;
        _big = _big || _big_played_count;
        _big = _big || _big_played_cents;
        _big = _big || _big_won_count;
        _big = _big || _big_won_cents;
        _big = _big || _big_jackpot_cents;
        _big = _big || _big_progressive_jackpot_cents;

        // Get "delta" for register adjusments
        _delta_game_meters_for_adjusments = new GameMeters();
        _delta_game_meters_for_adjusments.PlayedCount = DeltaGameMeters.PlayedCount;
        _delta_game_meters_for_adjusments.PlayedCents = DeltaGameMeters.PlayedCents;
        _delta_game_meters_for_adjusments.WonCount = DeltaGameMeters.WonCount;
        _delta_game_meters_for_adjusments.WonCents = DeltaGameMeters.WonCents;
        _delta_game_meters_for_adjusments.JackpotCents = DeltaGameMeters.JackpotCents;
        _delta_game_meters_for_adjusments.ProgressiveJackpotCents = DeltaGameMeters.ProgressiveJackpotCents;

        if (_big)
        {
          _alarm = true;
          _alarm_name = "*** Game Meters BigIncrement ***";
          _alarm_code = AlarmCode.TerminalSystem_GameMeterBigIncrement;

          if (_big_played_count)
          {
            DeltaGameMeters.PlayedCount = 0;
          }
          if (_big_played_cents)
          {
            DeltaGameMeters.PlayedCents = 0;
          }
          if (_big_won_count)
          {
            DeltaGameMeters.WonCount = 0;
          }
          if (_big_won_cents)
          {
            DeltaGameMeters.WonCents = 0;
          }
          if (_big_jackpot_cents)
          {
            DeltaGameMeters.JackpotCents = 0;
          }
          if (_big_progressive_jackpot_cents)
          {
            DeltaGameMeters.ProgressiveJackpotCents = 0;
          }

          BigIncrement = true;
        }
      }
      finally
      {
        if (_alarm)
        {
          WSI.Common.BatchUpdate.TerminalSession.GetOpenedTerminalSession(TerminalId, out _sesion_id);
          WSI.Common.BatchUpdate.TerminalSession.GetData(TerminalId, _sesion_id, out _provider_id, out _terminal_name);

          if (_big)
          {
            if (_big_played_count)
            {
              Alarm.GeneraAlarmByMeter(_alarm_name, AlarmCode.TerminalSystem_GameMeterBigIncrement_ByMeter, 0x0005, TerminalId, _provider_id, _terminal_name, OldGameMeters.PlayedCount, NewGameMeters.PlayedCount);
            }
            if (_big_played_cents)
            {
              Alarm.GeneraAlarmByMeter(_alarm_name, AlarmCode.TerminalSystem_GameMeterBigIncrement_ByMeter, 0x0000, TerminalId, _provider_id, _terminal_name, OldGameMeters.PlayedCents, NewGameMeters.PlayedCents);
            }
            if (_big_won_count)
            {
              Alarm.GeneraAlarmByMeter(_alarm_name, AlarmCode.TerminalSystem_GameMeterBigIncrement_ByMeter, 0x0006, TerminalId, _provider_id, _terminal_name, OldGameMeters.WonCount, NewGameMeters.WonCount);
            }
            if (_big_won_cents)
            {
              Alarm.GeneraAlarmByMeter(_alarm_name, AlarmCode.TerminalSystem_GameMeterBigIncrement_ByMeter, 0x0001, TerminalId, _provider_id, _terminal_name, OldGameMeters.WonCents, NewGameMeters.WonCents);
            }
            if (_big_jackpot_cents)
            {
              Alarm.GeneraAlarmByMeter(_alarm_name, AlarmCode.TerminalSystem_GameMeterBigIncrement_ByMeter, 0x0002, TerminalId, _provider_id, _terminal_name, OldGameMeters.JackpotCents, NewGameMeters.JackpotCents);
            }
            if (_big_progressive_jackpot_cents)
            {
              Alarm.GeneraAlarmByMeter(_alarm_name, AlarmCode.TerminalSystem_GameMeterBigIncrement_ByMeter, 0x1000, TerminalId, _provider_id, _terminal_name, OldGameMeters.JackpotCents, NewGameMeters.JackpotCents);
            }
          }

          StringBuilder _sb;
          _sb = new StringBuilder();
          _sb.AppendLine(_alarm_name);
          _sb.AppendLine(" Terminal: " + TerminalId.ToString());
          _sb.AppendLine("     Name: " + Misc.ConcatProviderAndTerminal(_provider_id, _terminal_name));
          _sb.AppendLine(" Old meter values:");
          _sb.AppendLine("     Game: " + OldGameMeters.GameBaseName);
          _sb.AppendLine("     Denomination Cents: " + OldGameMeters.DenominationCents.ToString());
          _sb.AppendLine("     Played Count/Cents: " + OldGameMeters.PlayedCount.ToString() + " / " + OldGameMeters.PlayedCents.ToString());
          _sb.AppendLine("     Won Count/Cents: " + OldGameMeters.WonCount.ToString() + " / " + OldGameMeters.WonCents.ToString());
          _sb.AppendLine("     Jackpot Cents: " + OldGameMeters.JackpotCents.ToString());
          _sb.AppendLine(" New meter values:");
          _sb.AppendLine("     Game: " + NewGameMeters.GameBaseName);
          _sb.AppendLine("     Denomination Cents: " + NewGameMeters.DenominationCents.ToString());
          _sb.AppendLine("     Played Count/Cents: " + NewGameMeters.PlayedCount.ToString() + " / " + NewGameMeters.PlayedCents.ToString());
          _sb.AppendLine("     Won Count/Cents: " + NewGameMeters.WonCount.ToString() + " / " + NewGameMeters.WonCents.ToString());
          _sb.AppendLine("     Jackpot Cents: " + NewGameMeters.JackpotCents.ToString());

          Alarm.Register(AlarmSourceCode.TerminalSystem,
                         TerminalId,
                         Misc.ConcatProviderAndTerminal(_provider_id, _terminal_name),
                         _alarm_code,
                         _sb.ToString());

          Log.Warning(_sb.ToString());

          // JML 14-APR-2015 ADD register at sas_meter_adjusments
          if (_alarm_code == AlarmCode.TerminalSystem_GameMeterReset)
          {
            if (NewGameMeters.PlayedCount == 0 && NewGameMeters.PlayedCount != OldGameMeters.PlayedCount)
            {
              TerminalMeterGroup.InsertSasMetersAdjustments(Misc.TodayOpening()
                                                        , TerminalId
                                                        , 0x0005
                                                        , WCP_Games.GameID(NewGameMeters.GameId)
                                                        , NewGameMeters.DenominationCents
                                                        , WGDB.Now
                                                        , TerminalMeterGroup.MetersAdjustmentsType.System
                                                        , TerminalMeterGroup.MetersAdjustmentsSubType.System_Game_Reset
                                                        , OldGameMeters.PlayedCount
                                                        , NewGameMeters.PlayedCount
                                                        , _delta_game_meters_for_adjusments.PlayedCount
                                                        , "Reset: Game Played Count");
            }
            if (NewGameMeters.PlayedCents == 0 && NewGameMeters.PlayedCents != OldGameMeters.PlayedCents)
            {
              TerminalMeterGroup.InsertSasMetersAdjustments(Misc.TodayOpening()
                                                        , TerminalId
                                                        , 0x0000
                                                        , WCP_Games.GameID(NewGameMeters.GameId)
                                                        , NewGameMeters.DenominationCents
                                                        , WGDB.Now
                                                        , TerminalMeterGroup.MetersAdjustmentsType.System
                                                        , TerminalMeterGroup.MetersAdjustmentsSubType.System_Game_Reset
                                                        , OldGameMeters.PlayedCents
                                                        , NewGameMeters.PlayedCents
                                                        , _delta_game_meters_for_adjusments.PlayedCents
                                                        , "Reset: Game Played Cents");
            }
            if (NewGameMeters.WonCount == 0 && NewGameMeters.WonCount != OldGameMeters.WonCount)
            {
              TerminalMeterGroup.InsertSasMetersAdjustments(Misc.TodayOpening()
                                                        , TerminalId
                                                        , 0x0006
                                                        , WCP_Games.GameID(NewGameMeters.GameId)
                                                        , NewGameMeters.DenominationCents
                                                        , WGDB.Now
                                                        , TerminalMeterGroup.MetersAdjustmentsType.System
                                                        , TerminalMeterGroup.MetersAdjustmentsSubType.System_Game_Reset
                                                        , OldGameMeters.WonCount
                                                        , NewGameMeters.WonCount
                                                        , _delta_game_meters_for_adjusments.WonCount
                                                        , "Reset: Game Won Count");
            }
            if (NewGameMeters.WonCents == 0 && NewGameMeters.WonCents != OldGameMeters.WonCents)
            {
              TerminalMeterGroup.InsertSasMetersAdjustments(Misc.TodayOpening()
                                                        , TerminalId
                                                        , 0x0001
                                                        , WCP_Games.GameID(NewGameMeters.GameId)
                                                        , NewGameMeters.DenominationCents
                                                        , WGDB.Now
                                                        , TerminalMeterGroup.MetersAdjustmentsType.System
                                                        , TerminalMeterGroup.MetersAdjustmentsSubType.System_Game_Reset
                                                        , OldGameMeters.WonCents
                                                        , NewGameMeters.WonCents
                                                        , _delta_game_meters_for_adjusments.WonCents
                                                        , "Reset: Game Won Cents");
            }
            if (NewGameMeters.JackpotCents == 0 && NewGameMeters.JackpotCents != OldGameMeters.JackpotCents)
            {
              TerminalMeterGroup.InsertSasMetersAdjustments(Misc.TodayOpening()
                                                        , TerminalId
                                                        , 0x0002
                                                        , WCP_Games.GameID(NewGameMeters.GameId)
                                                        , NewGameMeters.DenominationCents
                                                        , WGDB.Now
                                                        , TerminalMeterGroup.MetersAdjustmentsType.System
                                                        , TerminalMeterGroup.MetersAdjustmentsSubType.System_Game_Reset
                                                        , OldGameMeters.JackpotCents
                                                        , NewGameMeters.JackpotCents
                                                        , _delta_game_meters_for_adjusments.JackpotCents
                                                        , "Reset: Game Jackpot Cents");
            }
          }
          if (_alarm_code == AlarmCode.TerminalSystem_GameMeterBigIncrement)
          {
            if (_big_played_count)
            {
              TerminalMeterGroup.InsertSasMetersAdjustments(Misc.TodayOpening()
                                                        , TerminalId
                                                        , 0x0005
                                                        , WCP_Games.GameID(NewGameMeters.GameId)
                                                        , NewGameMeters.DenominationCents
                                                        , WGDB.Now
                                                        , TerminalMeterGroup.MetersAdjustmentsType.System
                                                        , TerminalMeterGroup.MetersAdjustmentsSubType.System_Game_BigIncrement
                                                        , OldGameMeters.PlayedCount
                                                        , NewGameMeters.PlayedCount
                                                        , _delta_game_meters_for_adjusments.PlayedCount
                                                        , "Big Increment: Game Played Count");
            }
            if (_big_played_cents)
            {
              TerminalMeterGroup.InsertSasMetersAdjustments(Misc.TodayOpening()
                                                        , TerminalId
                                                        , 0x0000
                                                        , WCP_Games.GameID(NewGameMeters.GameId)
                                                        , NewGameMeters.DenominationCents
                                                        , WGDB.Now
                                                        , TerminalMeterGroup.MetersAdjustmentsType.System
                                                        , TerminalMeterGroup.MetersAdjustmentsSubType.System_Game_BigIncrement
                                                        , OldGameMeters.PlayedCents
                                                        , NewGameMeters.PlayedCents
                                                        , _delta_game_meters_for_adjusments.PlayedCents
                                                        , "Big Increment: Game Played Cents");
            }

            if (_big_won_count)
            {
              TerminalMeterGroup.InsertSasMetersAdjustments(Misc.TodayOpening()
                                                        , TerminalId
                                                        , 0x0006
                                                        , WCP_Games.GameID(NewGameMeters.GameId)
                                                        , NewGameMeters.DenominationCents
                                                        , WGDB.Now
                                                        , TerminalMeterGroup.MetersAdjustmentsType.System
                                                        , TerminalMeterGroup.MetersAdjustmentsSubType.System_Game_BigIncrement
                                                        , OldGameMeters.WonCount
                                                        , NewGameMeters.WonCount
                                                        , _delta_game_meters_for_adjusments.WonCount
                                                        , "Big Increment: Game Won Count");
            }

            if (_big_won_cents)
            {
              TerminalMeterGroup.InsertSasMetersAdjustments(Misc.TodayOpening()
                                                        , TerminalId
                                                        , 0x0001
                                                        , WCP_Games.GameID(NewGameMeters.GameId)
                                                        , NewGameMeters.DenominationCents
                                                        , WGDB.Now
                                                        , TerminalMeterGroup.MetersAdjustmentsType.System
                                                        , TerminalMeterGroup.MetersAdjustmentsSubType.System_Game_BigIncrement
                                                        , OldGameMeters.WonCents
                                                        , NewGameMeters.WonCents
                                                        , _delta_game_meters_for_adjusments.WonCents
                                                        , "Big Increment: Game Won Cents");
            }

            if (_big_jackpot_cents)
            {
              TerminalMeterGroup.InsertSasMetersAdjustments(Misc.TodayOpening()
                                                        , TerminalId
                                                        , 0x0002
                                                        , WCP_Games.GameID(NewGameMeters.GameId)
                                                        , NewGameMeters.DenominationCents
                                                        , WGDB.Now
                                                        , TerminalMeterGroup.MetersAdjustmentsType.System
                                                        , TerminalMeterGroup.MetersAdjustmentsSubType.System_Game_BigIncrement
                                                        , OldGameMeters.JackpotCents
                                                        , NewGameMeters.JackpotCents
                                                        , _delta_game_meters_for_adjusments.JackpotCents
                                                        , "Big Increment: Game Jackpot Cents");
            }

            if (OldGameMeters.ProgressiveJackpotCents != -1)
            {
              if (_big_progressive_jackpot_cents)
              {
                TerminalMeterGroup.InsertSasMetersAdjustments(Misc.TodayOpening()
                                                          , TerminalId
                                                          , 0x1000
                                                          , WCP_Games.GameID(NewGameMeters.GameId)
                                                          , NewGameMeters.DenominationCents
                                                          , WGDB.Now
                                                          , TerminalMeterGroup.MetersAdjustmentsType.System
                                                          , TerminalMeterGroup.MetersAdjustmentsSubType.System_Game_BigIncrement
                                                          , OldGameMeters.ProgressiveJackpotCents
                                                          , NewGameMeters.ProgressiveJackpotCents
                                                          , _delta_game_meters_for_adjusments.ProgressiveJackpotCents
                                                          , "Big Increment: Game Progressive Jackpot Cents");
              }
            }
          }
        }
      }
    } // GetDeltaGameMeters

    //------------------------------------------------------------------------------
    // PURPOSE : Update Game Meters 
    //
    //  PARAMS :
    //      - INPUT :
    //          - TerminalId
    //          - WcpSequenceId
    //          - NewGameMeters: Wcp message game meters
    //          - OldGameMeters
    //          - Trx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //      - true: game meters updated.
    //      - false: not found or error.
    //
    private static Boolean DB_UpdateGameMeters(Int32 TerminalId, Int64 WcpSequenceId, GameMeters NewGameMeters, GameMeters OldGameMeters, GameMeters DeltaGameMeters, SqlTransaction Trx)
    {
      SqlCommand _sql_command;
      String _sql_str;
      Int32 _num_rows_updated;

      _sql_str = "";
      _sql_str += "UPDATE GAME_METERS SET GM_WCP_SEQUENCE_ID        = @pWcpSequenceId ";
      _sql_str += "                     , GM_DENOMINATION           = @pDenomination ";
      _sql_str += "                     , GM_PLAYED_COUNT           = @pNewPlayedCount ";
      _sql_str += "                     , GM_PLAYED_AMOUNT          = @pNewPlayedAmount ";
      _sql_str += "                     , GM_WON_COUNT              = @pNewWonCount ";
      _sql_str += "                     , GM_WON_AMOUNT             = @pNewWonAmount ";
      _sql_str += "                     , GM_JACKPOT_AMOUNT         = @pNewJackpotAmount ";
      _sql_str += "                     , GM_LAST_REPORTED          = GETDATE() ";
      _sql_str += "                     , GM_DELTA_GAME_NAME        = @pDeltaGameName ";
      _sql_str += "                     , GM_DELTA_PLAYED_COUNT     = GM_DELTA_PLAYED_COUNT    + @pDeltaPlayedCount ";
      _sql_str += "                     , GM_DELTA_PLAYED_AMOUNT    = GM_DELTA_PLAYED_AMOUNT   + @pDeltaPlayedAmount ";
      _sql_str += "                     , GM_DELTA_WON_COUNT        = GM_DELTA_WON_COUNT       + @pDeltaWonCount ";
      _sql_str += "                     , GM_DELTA_WON_AMOUNT       = GM_DELTA_WON_AMOUNT      + @pDeltaWonAmount ";
      _sql_str += "                     , GM_DELTA_JACKPOT_AMOUNT   = GM_DELTA_JACKPOT_AMOUNT  + @pDeltaJackpotAmount ";
      _sql_str += " WHERE   GM_TERMINAL_ID     = @pTerminalID   ";
      _sql_str += "   AND   GM_GAME_BASE_NAME  = @pGameBaseName ";
      _sql_str += "   AND   GM_PLAYED_COUNT    = @pOldPlayedCount ";
      _sql_str += "   AND   GM_PLAYED_AMOUNT   = @pOldPlayedAmount ";
      _sql_str += "   AND   GM_WON_COUNT       = @pOldWonCount ";
      _sql_str += "   AND   GM_WON_AMOUNT      = @pOldWonAmount ";
      _sql_str += "   AND   GM_JACKPOT_AMOUNT  = @pOldJackpotAmount ";


      _sql_command = new SqlCommand(_sql_str);
      _sql_command.Connection = Trx.Connection;
      _sql_command.Transaction = Trx;
      _sql_command.Parameters.Add("@pWcpSequenceId", SqlDbType.BigInt).Value = WcpSequenceId;
      _sql_command.Parameters.Add("@pDenomination", SqlDbType.Money).Value = ((Decimal)NewGameMeters.DenominationCents) / 100;
      _sql_command.Parameters.Add("@pNewPlayedCount", SqlDbType.BigInt).Value = NewGameMeters.PlayedCount;
      _sql_command.Parameters.Add("@pNewPlayedAmount", SqlDbType.Money).Value = ((Decimal)NewGameMeters.PlayedCents) / 100;
      _sql_command.Parameters.Add("@pNewWonCount", SqlDbType.BigInt).Value = NewGameMeters.WonCount;
      _sql_command.Parameters.Add("@pNewWonAmount", SqlDbType.Money).Value = ((Decimal)NewGameMeters.WonCents) / 100;
      _sql_command.Parameters.Add("@pNewJackpotAmount", SqlDbType.Money).Value = ((Decimal)NewGameMeters.JackpotCents) / 100;
      _sql_command.Parameters.Add("@pTerminalID", SqlDbType.Int).Value = TerminalId;
      _sql_command.Parameters.Add("@pGameBaseName", SqlDbType.NVarChar, 50).Value = NewGameMeters.GameBaseName;
      _sql_command.Parameters.Add("@pOldPlayedCount", SqlDbType.BigInt).Value = OldGameMeters.PlayedCount;
      _sql_command.Parameters.Add("@pOldPlayedAmount", SqlDbType.Money).Value = ((Decimal)OldGameMeters.PlayedCents) / 100;
      _sql_command.Parameters.Add("@pOldWonCount", SqlDbType.BigInt).Value = OldGameMeters.WonCount;
      _sql_command.Parameters.Add("@pOldWonAmount", SqlDbType.Money).Value = ((Decimal)OldGameMeters.WonCents) / 100;
      _sql_command.Parameters.Add("@pOldJackpotAmount", SqlDbType.Money).Value = ((Decimal)OldGameMeters.JackpotCents) / 100;
      _sql_command.Parameters.Add("@pDeltaGameName", SqlDbType.NVarChar, 50).Value = DeltaGameMeters.GameId;
      _sql_command.Parameters.Add("@pDeltaPlayedCount", SqlDbType.BigInt).Value = DeltaGameMeters.PlayedCount;
      _sql_command.Parameters.Add("@pDeltaPlayedAmount", SqlDbType.Money).Value = ((Decimal)DeltaGameMeters.PlayedCents) / 100;
      _sql_command.Parameters.Add("@pDeltaWonCount", SqlDbType.BigInt).Value = DeltaGameMeters.WonCount;
      _sql_command.Parameters.Add("@pDeltaWonAmount", SqlDbType.Money).Value = ((Decimal)DeltaGameMeters.WonCents) / 100;
      _sql_command.Parameters.Add("@pDeltaJackpotAmount", SqlDbType.Money).Value = ((Decimal)DeltaGameMeters.JackpotCents) / 100;

      try
      {
        _num_rows_updated = _sql_command.ExecuteNonQuery();
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        Log.Warning("Updating GAME_METERS [Exception]: TerminalId: " + TerminalId.ToString() + ", GameBaseName: " + NewGameMeters.GameBaseName);

        return false;
      }

      if (_num_rows_updated != 1)
      {
        Log.Warning("Updating GAME_METERS [Not updated]: TerminalId: " + TerminalId.ToString() + ", GameBaseName: " + NewGameMeters.GameBaseName);

        return false;
      }

      return true;

    } // DB_UpdateGameMeters

    //------------------------------------------------------------------------------
    // PURPOSE : Insert game meters received.
    //
    //  PARAMS :
    //      - INPUT :
    //          - TerminalId
    //          - WcpSequenceId
    //          - NewGameMeter: Wcp message game meters
    //          - Trx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - GameMeters
    //
    //   NOTES :
    //      - true: game meters inserted.
    //      - false: error.
    //
    private static Boolean DB_InsertGameMeters(Int32 TerminalId, Int64 WcpSequenceId, GameMeters NewGameMeters, SqlTransaction Trx)
    {
      SqlCommand _sql_command;
      String _sql_str;
      Int32 _num_rows_inserted;

      _sql_str = "";
      _sql_str += "INSERT INTO GAME_METERS ( GM_TERMINAL_ID ";
      _sql_str += "                        , GM_GAME_BASE_NAME ";
      _sql_str += "                        , GM_WCP_SEQUENCE_ID ";
      _sql_str += "                        , GM_DENOMINATION ";
      _sql_str += "                        , GM_PLAYED_COUNT ";
      _sql_str += "                        , GM_PLAYED_AMOUNT ";
      _sql_str += "                        , GM_WON_COUNT ";
      _sql_str += "                        , GM_WON_AMOUNT ";
      _sql_str += "                        , GM_JACKPOT_AMOUNT ";
      _sql_str += "                        , GM_LAST_REPORTED ) ";
      _sql_str += "               VALUES   ( @pTerminalID ";
      _sql_str += "                        , @pGameBaseName ";
      _sql_str += "                        , @pWcpSequenceId ";
      _sql_str += "                        , @pDenomination ";
      _sql_str += "                        , @pNewPlayedCount ";
      _sql_str += "                        , @pNewPlayedAmount ";
      _sql_str += "                        , @pNewWonCount ";
      _sql_str += "                        , @pNewWonAmount ";
      _sql_str += "                        , @pNewJackpotAmount ";
      _sql_str += "                        , GETDATE() ) ";

      _sql_command = new SqlCommand(_sql_str);
      _sql_command.Connection = Trx.Connection;
      _sql_command.Transaction = Trx;

      _sql_command.Parameters.Add("@pTerminalID", SqlDbType.Int).Value = TerminalId;
      _sql_command.Parameters.Add("@pGameBaseName", SqlDbType.NVarChar, 50).Value = NewGameMeters.GameBaseName;
      _sql_command.Parameters.Add("@pWcpSequenceId", SqlDbType.BigInt).Value = WcpSequenceId;
      _sql_command.Parameters.Add("@pDenomination", SqlDbType.Money).Value = ((Decimal)NewGameMeters.DenominationCents) / 100;
      _sql_command.Parameters.Add("@pNewPlayedCount", SqlDbType.BigInt).Value = NewGameMeters.PlayedCount;
      _sql_command.Parameters.Add("@pNewPlayedAmount", SqlDbType.Money).Value = ((Decimal)NewGameMeters.PlayedCents) / 100;
      _sql_command.Parameters.Add("@pNewWonCount", SqlDbType.BigInt).Value = NewGameMeters.WonCount;
      _sql_command.Parameters.Add("@pNewWonAmount", SqlDbType.Money).Value = ((Decimal)NewGameMeters.WonCents) / 100;
      _sql_command.Parameters.Add("@pNewJackpotAmount", SqlDbType.Money).Value = ((Decimal)NewGameMeters.JackpotCents) / 100;

      try
      {
        _num_rows_inserted = _sql_command.ExecuteNonQuery();
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        Log.Warning("Inserting GAME_METERS [Exception]: TerminalId: " + TerminalId.ToString() + ", GameBaseName: " + NewGameMeters.GameBaseName);

        return false;
      }

      if (_num_rows_inserted != 1)
      {
        Log.Warning("Inserting GAME_METERS [Not updated]: TerminalId: " + TerminalId.ToString() + ", GameBaseName: " + NewGameMeters.GameBaseName);

        return false;
      }

      return true;

    } // DB_InsertGameMeters

    //------------------------------------------------------------------------------
    // PURPOSE : Prepares delta game meters for Update game meters statistics.
    //
    //  PARAMS :
    //      - INPUT :
    //          - Trx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True or false
    //
    //   NOTES :
    //      - true: delta game meters inserted in Sales per Hour.
    //      - false: error.
    //
    static public Boolean UpdateStatisticsFromGameMeters(SqlTransaction Trx)
    {
      DataTable _dt;
      Boolean _rc;

      int _tick0;
      int _tick1;
      int _tick2;
      long _interval_select;
      long _interval_update_SPH;
      long _interval_update_GM;
      long _interval_all_process;

      _interval_select = 0;
      _interval_update_SPH = 0;
      _interval_update_GM = 0;
      _interval_all_process = 0;
      _tick0 = Environment.TickCount;

      _dt = null;

      try
      {
        _tick1 = Environment.TickCount;

        // Select Delta Game Meters
        _rc = GetDeltaGameMeters(out _dt, Trx);
        if (!_rc)
        {
          return false;
        }

        _tick1 = Environment.TickCount;

        // Update Last played game on terminals
        _rc = UpdateLastPlayedGame(_dt, Trx);
        if (!_rc)
        {
          return false;
        }


        _tick2 = Environment.TickCount;
        _interval_select = Misc.GetElapsedTicks(_tick1, _tick2);

        _tick1 = Environment.TickCount;

        // Update SPH
        _rc = UpdateStatistics(_dt, Trx);
        if (!_rc)
        {
          return false;
        }

        _tick2 = Environment.TickCount;
        _interval_update_SPH = Misc.GetElapsedTicks(_tick1, _tick2);

        _tick1 = Environment.TickCount;

        // Update Delta Game Meters
        _rc = UpdateDeltaGameMeters(_dt, Trx);
        if (!_rc)
        {
          return false;
        }

        _tick2 = Environment.TickCount;
        _interval_update_GM = Misc.GetElapsedTicks(_tick1, _tick2);

        _interval_all_process = Misc.GetElapsedTicks(_tick0, _tick2);

        if (_interval_select > 4000
            || _interval_update_SPH > 4000
            || _interval_update_GM > 4000
            || _interval_all_process > 8000)
        {
          Log.Warning("Thread Statistics GameMeters times: Select/UpdateSPH/UpdateGM/Total = " + _interval_select.ToString() + "/" + _interval_update_SPH.ToString() + "/" + _interval_update_GM.ToString() + "/" + _interval_all_process.ToString());
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
      finally
      {
      }
    } // UpdateStatisticsFromGameMeters

    //------------------------------------------------------------------------------
    // PURPOSE : Update statistics.
    //
    //  PARAMS :
    //      - INPUT :
    //          - DeltaGameMeter: Datatable with delta game meters
    //          - Trx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True or false
    //
    //   NOTES :
    //      - true: delta game meters inserted in Sales per Hour.
    //      - false: error.
    //
    static private Boolean UpdateStatistics(DataTable DeltaGameMeters, SqlTransaction Trx)
    {
      StringBuilder _sql_txt;
      StringBuilder _select;
      SqlDataAdapter _da;
      int _update_batch_size = 500;
      int _nr;
      Decimal _terminal_default_payout;
      Int32 _progressives_enabled;

      if (DeltaGameMeters.Rows.Count == 0)
      {
        return true;
      }

      try
      {
        _terminal_default_payout = GeneralParam.GetDecimal("PlayerTracking", "TerminalDefaultPayout", 95) / 100m;
        _progressives_enabled = GeneralParam.GetInt32("Progressives", "Enabled", 0);

        //    - Set SqlDataAdapter commands

        _da = new SqlDataAdapter();

        // When batch updates are enabled (UpdateBatchSize != 1), the UpdatedRowSource property value 
        // of the DataAdapter's UpdateCommand, InsertCommand, and DeleteCommand should be set to None 
        // or OutputParameters. 
        _da.UpdateBatchSize = _update_batch_size;
        //_da.ContinueUpdateOnError = false;

        //    - Set SqlDataAdapter commands
        //        - SelectCommand 
        //        - DeleteCommand 
        //        - InsertCommand 
        //        - UpdateCommand 

        _select = new StringBuilder();
        _select.AppendLine("DECLARE  @payout AS MONEY ");
        _select.AppendLine("SELECT   @payout = ISNULL(TE_THEORETICAL_PAYOUT, @pTerminalDefaultPayout) ");
        _select.AppendLine("  FROM   TERMINALS ");
        _select.AppendLine(" WHERE   TE_TERMINAL_ID = @pTerminalId ; ");

        //        - UpdateCommand 
        _sql_txt = new StringBuilder();

        _sql_txt.AppendLine(_select.ToString());

        _sql_txt.AppendLine("UPDATE SALES_PER_HOUR");
        _sql_txt.AppendLine("   SET SPH_PLAYED_COUNT           = SPH_PLAYED_COUNT    + @p1");
        _sql_txt.AppendLine("     , SPH_PLAYED_AMOUNT          = SPH_PLAYED_AMOUNT   + @p2");
        _sql_txt.AppendLine("     , SPH_WON_COUNT              = SPH_WON_COUNT       + @p3");
        _sql_txt.AppendLine("     , SPH_WON_AMOUNT             = SPH_WON_AMOUNT      + @p4");
        _sql_txt.AppendLine("     , SPH_THEORETICAL_WON_AMOUNT = ( SPH_PLAYED_AMOUNT + @p2 ) * @payout ");
        _sql_txt.AppendLine("     , SPH_JACKPOT_AMOUNT                = ISNULL(SPH_JACKPOT_AMOUNT, 0)      + ISNULL(@pDeltaJackpot, 0) ");
        _sql_txt.AppendLine("     , SPH_PROGRESSIVE_JACKPOT_AMOUNT    = ISNULL(SPH_PROGRESSIVE_JACKPOT_AMOUNT, 0)    + 0 "); // ACC 17-OCT-2014  ISNULL(@pDeltaProgressive, 0) ");
        _sql_txt.AppendLine("     , SPH_PROGRESSIVE_JACKPOT_AMOUNT_0  = ISNULL(SPH_PROGRESSIVE_JACKPOT_AMOUNT_0, 0)  + 0 "); // ACC 17-OCT-2014  (CASE WHEN @pProgressivesEnabled = 1 THEN ISNULL(@pDeltaProgressive, 0) ELSE 0 END) ");
        _sql_txt.AppendLine(" WHERE SPH_GAME_ID                = (SELECT GM_GAME_ID FROM GAMES WHERE GM_NAME = @p5)");
        _sql_txt.AppendLine("   AND SPH_TERMINAL_ID            = @pTerminalId");
        _sql_txt.AppendLine("   AND SPH_BASE_HOUR              = @p7");

        _da.UpdateCommand = new SqlCommand(_sql_txt.ToString(), Trx.Connection, Trx);
        _da.UpdateCommand.UpdatedRowSource = UpdateRowSource.None;

        _da.UpdateCommand.Parameters.Add("@p1", SqlDbType.BigInt, 8, "GM_DELTA_PLAYED_COUNT");
        _da.UpdateCommand.Parameters.Add("@p2", SqlDbType.Money, 8, "GM_DELTA_PLAYED_AMOUNT");
        _da.UpdateCommand.Parameters.Add("@p3", SqlDbType.BigInt, 8, "GM_DELTA_WON_COUNT");
        _da.UpdateCommand.Parameters.Add("@p4", SqlDbType.Money, 8, "GM_DELTA_WON_AMOUNT");
        _da.UpdateCommand.Parameters.Add("@p5", SqlDbType.NVarChar, 50, "GM_DELTA_GAME_NAME");
        _da.UpdateCommand.Parameters.Add("@pTerminalId", SqlDbType.Int, 4, "GM_TERMINAL_ID");
        _da.UpdateCommand.Parameters.Add("@p7", SqlDbType.DateTime).SourceColumn = "BASE_HOUR";
        _da.UpdateCommand.Parameters.Add("@pTerminalDefaultPayout", SqlDbType.Decimal).Value = _terminal_default_payout;
        _da.UpdateCommand.Parameters.Add("@pProgressivesEnabled", SqlDbType.Int).Value = _progressives_enabled;
        _da.UpdateCommand.Parameters.Add("@pDeltaJackpot", SqlDbType.Money, 8, "GM_DELTA_JACKPOT_AMOUNT");
        _da.UpdateCommand.Parameters.Add("@pDeltaProgressive", SqlDbType.Money, 8, "GM_DELTA_PROGRESSIVE_JACKPOT_AMOUNT");

        //        - DeleteCommand & SelectCommand   
        _da.DeleteCommand = null;
        _da.SelectCommand = null;

        //    - Execute Update command
        _da.InsertCommand = null;
        _da.UpdateBatchSize = _update_batch_size;
        _da.ContinueUpdateOnError = true;

        _nr = _da.Update(DeltaGameMeters);

        if (DeltaGameMeters.Rows.Count == _nr)
        {
          return true;
        }

        foreach (DataRow _dr in DeltaGameMeters.Rows)
        {
          if (_dr.HasErrors)
          {
            _dr.ClearErrors();
            _dr.AcceptChanges();
            _dr.SetAdded();
          }
          else
          {
            _dr.AcceptChanges();
            if (_dr.RowState != DataRowState.Unchanged)
            {
              return false;
            }
          }
        }

        _da.UpdateCommand = null;

        // Insert
        _sql_txt = new StringBuilder();

        _sql_txt.AppendLine(_select.ToString());

        _sql_txt.AppendLine("INSERT INTO  SALES_PER_HOUR ");
        _sql_txt.AppendLine("           ( SPH_BASE_HOUR ");
        _sql_txt.AppendLine("           , SPH_TERMINAL_ID ");
        _sql_txt.AppendLine("           , SPH_TERMINAL_NAME ");
        _sql_txt.AppendLine("           , SPH_GAME_ID ");
        _sql_txt.AppendLine("           , SPH_GAME_NAME ");
        _sql_txt.AppendLine("           , SPH_PLAYED_COUNT ");
        _sql_txt.AppendLine("           , SPH_PLAYED_AMOUNT ");
        _sql_txt.AppendLine("           , SPH_WON_COUNT ");
        _sql_txt.AppendLine("           , SPH_WON_AMOUNT ");
        _sql_txt.AppendLine("           , SPH_THEORETICAL_WON_AMOUNT ");
        _sql_txt.AppendLine("           , SPH_JACKPOT_AMOUNT ");
        _sql_txt.AppendLine("           , SPH_PROGRESSIVE_JACKPOT_AMOUNT ");
        _sql_txt.AppendLine("           , SPH_PROGRESSIVE_JACKPOT_AMOUNT_0 ");
        _sql_txt.AppendLine("           , SPH_NUM_ACTIVE_TERMINALS ");
        _sql_txt.AppendLine("           , SPH_LAST_PLAY_ID) ");
        // _sql_txt.AppendLine("VALUES     ( @p1, @pTerminalId, @p3, @p4, @p5, @p6, @p7, @p8, @p9, @p7 * @payout, ISNULL(@pDeltaJackpot, 0), ISNULL(@pDeltaProgressive, 0), (CASE WHEN @pProgressivesEnabled = 1 THEN ISNULL(@pDeltaProgressive, 0) ELSE 0 END), @p10, @p11) ");
        // ACC 17-OCT-2014 Progressives are inserted when it paid by cashier.
        _sql_txt.AppendLine("VALUES     ( @p1, @pTerminalId, @p3, @p4, @p5, @p6, @p7, @p8, @p9, @p7 * @payout, ISNULL(@pDeltaJackpot, 0), 0, 0, @p10, @p11) ");

        _da.InsertCommand = new SqlCommand(_sql_txt.ToString());
        _da.InsertCommand.Connection = Trx.Connection;
        _da.InsertCommand.Transaction = Trx;

        _da.InsertCommand.Parameters.Add("@p1", SqlDbType.DateTime).SourceColumn = "BASE_HOUR";
        _da.InsertCommand.Parameters.Add("@pTerminalId", SqlDbType.Int, 4, "GM_TERMINAL_ID");
        _da.InsertCommand.Parameters.Add("@p3", SqlDbType.NVarChar, 50, "TERMINAL_NAME");
        _da.InsertCommand.Parameters.Add("@p4", SqlDbType.Int, 4, "GAME_ID");
        _da.InsertCommand.Parameters.Add("@p5", SqlDbType.NVarChar, 50, "GM_DELTA_GAME_NAME");
        _da.InsertCommand.Parameters.Add("@p6", SqlDbType.BigInt, 8, "GM_DELTA_PLAYED_COUNT");
        _da.InsertCommand.Parameters.Add("@p7", SqlDbType.Money, 8, "GM_DELTA_PLAYED_AMOUNT");
        _da.InsertCommand.Parameters.Add("@p8", SqlDbType.BigInt, 8, "GM_DELTA_WON_COUNT");
        _da.InsertCommand.Parameters.Add("@p9", SqlDbType.Money, 8, "GM_DELTA_WON_AMOUNT");
        _da.InsertCommand.Parameters.Add("@p10", SqlDbType.Int, 4, "NUM_ACTIVE");
        _da.InsertCommand.Parameters.Add("@p11", SqlDbType.BigInt, 8, "NUM_ZERO");
        _da.InsertCommand.Parameters.Add("@pTerminalDefaultPayout", SqlDbType.Decimal).Value = _terminal_default_payout;
        _da.InsertCommand.Parameters.Add("@pProgressivesEnabled", SqlDbType.Int).Value = _progressives_enabled;
        _da.InsertCommand.Parameters.Add("@pDeltaJackpot", SqlDbType.Money, 8, "GM_DELTA_JACKPOT_AMOUNT");
        _da.InsertCommand.Parameters.Add("@pDeltaProgressive", SqlDbType.Money, 8, "GM_DELTA_PROGRESSIVE_JACKPOT_AMOUNT");

        _da.InsertCommand.UpdatedRowSource = UpdateRowSource.OutputParameters;
        _nr = _nr + _da.Update(DeltaGameMeters);

        if (DeltaGameMeters.Rows.Count != _nr)
        {
          // AJQ 08-JUN-2010, Some inserts failed!
          // Set the 'delta' to zero and continue with the transaction
          foreach (DataRow _dr in DeltaGameMeters.Rows)
          {
            if (_dr.HasErrors)
            {
              _dr.ClearErrors();
              _dr["GM_DELTA_PLAYED_COUNT"] = 0;
              _dr["GM_DELTA_PLAYED_AMOUNT"] = 0;
              _dr["GM_DELTA_WON_COUNT"] = 0;
              _dr["GM_DELTA_WON_AMOUNT"] = 0;
              _dr["GM_DELTA_JACKPOT_AMOUNT"] = 0;
              _dr["GM_DELTA_PROGRESSIVE_JACKPOT_AMOUNT"] = 0;
              _nr = _nr + 1;
            }
            _dr.AcceptChanges();
          }

          if (DeltaGameMeters.Rows.Count != _nr)
          {
            return false;
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
      finally
      {
      }
    } // UpdateStatistics

    //------------------------------------------------------------------------------
    // PURPOSE : Prepares delta game meters for Update game meters statistics.
    //
    //  PARAMS :
    //      - INPUT :
    //          - Trx
    //
    //      - OUTPUT :
    //          - DeltaGameMeters
    //
    // RETURNS :
    //      - True or False
    //
    //   NOTES :
    //      - true: delta game meters Selected from Game Meters.
    //      - false: error.
    //
    static public Boolean GetDeltaGameMeters(out DataTable DeltaGameMeters, SqlTransaction Trx)
    {
      SqlCommand _sql_command;
      StringBuilder _sql_txt;
      SqlDataAdapter _da;
      DateTime _base_hour;
      int _idx_row;
      Int32 _num_active_terminals;
      DataTable _rows_for_update;
      int _nr;

      // Get Active Terminals
      _num_active_terminals = 0;
      try
      {
        _sql_txt = new StringBuilder();
        _sql_txt.AppendLine("SELECT COUNT(*) FROM TERMINALS WHERE TE_ACTIVE = 1 ");

        _sql_command = new SqlCommand(_sql_txt.ToString());
        _sql_command.Connection = Trx.Connection;
        _sql_command.Transaction = Trx;

        _num_active_terminals = (Int32)_sql_command.ExecuteScalar();
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }

      _base_hour = DateTime.Now; // To avoid compiler warning
      DeltaGameMeters = new DataTable("GAME_METERS");

      //
      // ACC 23-NOV-2010 Lock by rows and with PK order --> Select GameMeters for UPDATE .. WHERE [PK]
      //
      _rows_for_update = new DataTable("GAME_METERS");

      // Set SqlDataAdapter commands
      _da = new SqlDataAdapter();

      // ACC 20-SEP-2010 Accept negative deltas

      //        - SelectCommand 
      _sql_txt = new StringBuilder();
      _sql_txt.AppendLine("  SELECT   GM_TERMINAL_ID ");
      _sql_txt.AppendLine("         , GM_GAME_BASE_NAME ");
      _sql_txt.AppendLine("    FROM   GAME_METERS ");
      _sql_txt.AppendLine("   WHERE   GM_DELTA_UPDATING  = 0 ");
      _sql_txt.AppendLine("     AND   (   GM_DELTA_PLAYED_COUNT <> 0 ");
      _sql_txt.AppendLine("            OR GM_DELTA_PLAYED_AMOUNT <> 0 ");
      _sql_txt.AppendLine("            OR GM_DELTA_WON_COUNT <> 0 ");
      _sql_txt.AppendLine("            OR GM_DELTA_WON_AMOUNT <> 0 ");
      _sql_txt.AppendLine("            OR GM_DELTA_JACKPOT_AMOUNT <> 0 ");
      _sql_txt.AppendLine("            OR GM_DELTA_PROGRESSIVE_JACKPOT_AMOUNT <> 0 ) ");
      _sql_txt.AppendLine("ORDER BY   GM_TERMINAL_ID  ");
      _sql_txt.AppendLine("         , GM_GAME_BASE_NAME  ");

      _da.SelectCommand = new SqlCommand(_sql_txt.ToString());
      _da.SelectCommand.Connection = Trx.Connection;
      _da.SelectCommand.Transaction = Trx;

      //        - UpdateCommand, DeleteCommand & InsertCommand
      _da.DeleteCommand = null;
      _da.InsertCommand = null;
      _da.UpdateCommand = null;

      //    - Execute Select command
      _da.Fill(_rows_for_update);

      if (_rows_for_update.Rows.Count == 0)
      {
        return false;
      }

      //
      // UPDATE GM_DELTA_UPDATING
      //
      _sql_txt = new StringBuilder();

      _sql_txt.AppendLine(" DECLARE @_delta_game_name AS NVARCHAR(50)        ");

      _sql_txt.AppendLine(" SELECT   @_delta_game_name =  GM_DELTA_GAME_NAME ");
      _sql_txt.AppendLine("   FROM   GAME_METERS                             ");
      _sql_txt.AppendLine("  WHERE   GM_TERMINAL_ID    = @pTerminalId        ");
      _sql_txt.AppendLine("    AND   GM_GAME_BASE_NAME = @pGameBaseName      ");

      _sql_txt.AppendLine(" IF NOT EXISTS (SELECT GM_GAME_ID                             ");
      _sql_txt.AppendLine("                  FROM GAMES                                  ");
      _sql_txt.AppendLine("                 WHERE GM_NAME = @_delta_game_name)           ");
      _sql_txt.AppendLine("    BEGIN                                                     ");
      _sql_txt.AppendLine("       INSERT INTO GAMES (GM_NAME)  VALUES(@_delta_game_name) ");
      _sql_txt.AppendLine("    END                                                       ");

      _sql_txt.AppendLine("UPDATE GAME_METERS                        ");
      _sql_txt.AppendLine("   SET GM_DELTA_UPDATING = 1              ");
      _sql_txt.AppendLine(" WHERE GM_TERMINAL_ID    = @pTerminalId   ");
      _sql_txt.AppendLine("   AND GM_GAME_BASE_NAME = @pGameBaseName ");
      _sql_txt.AppendLine("   AND GM_DELTA_UPDATING = 0              ");

      _da.UpdateCommand = new SqlCommand(_sql_txt.ToString());
      _da.UpdateCommand.Connection = Trx.Connection;
      _da.UpdateCommand.Transaction = Trx;
      _da.UpdateCommand.UpdatedRowSource = UpdateRowSource.None;

      _da.UpdateCommand.Parameters.Add("@pTerminalId", SqlDbType.Int, 4, "GM_TERMINAL_ID");
      _da.UpdateCommand.Parameters.Add("@pGameBaseName", SqlDbType.NVarChar, 50, "GM_GAME_BASE_NAME");

      //    - SetModified
      for (_idx_row = 0; _idx_row < _rows_for_update.Rows.Count; _idx_row++)
      {
        _rows_for_update.Rows[_idx_row].SetModified();
      }

      _da.UpdateBatchSize = 500;
      _da.ContinueUpdateOnError = true;

      _nr = _da.Update(_rows_for_update);
      if (_nr == 0)
      {
        return false;
      }

      //    - Set SqlDataAdapter commands
      //        - SelectCommand 
      //        - DeleteCommand 
      //        - InsertCommand 
      //        - UpdateCommand 
      _da = new SqlDataAdapter();

      //        - SelectCommand 
      _sql_txt = new StringBuilder();
      _sql_txt.AppendLine("  SELECT   GM_TERMINAL_ID ");
      _sql_txt.AppendLine("         , GM_GAME_BASE_NAME ");
      _sql_txt.AppendLine("         , GM_WCP_SEQUENCE_ID ");
      _sql_txt.AppendLine("         , GM_DENOMINATION ");
      _sql_txt.AppendLine("         , GM_PLAYED_COUNT ");
      _sql_txt.AppendLine("         , GM_PLAYED_AMOUNT ");
      _sql_txt.AppendLine("         , GM_WON_COUNT ");
      _sql_txt.AppendLine("         , GM_WON_AMOUNT ");
      _sql_txt.AppendLine("         , GM_JACKPOT_AMOUNT ");
      _sql_txt.AppendLine("         , GM_PROGRESSIVE_JACKPOT_AMOUNT ");
      _sql_txt.AppendLine("         , GM_LAST_REPORTED ");
      _sql_txt.AppendLine("         , GM_DELTA_GAME_NAME ");
      _sql_txt.AppendLine("         , GM_DELTA_PLAYED_COUNT ");
      _sql_txt.AppendLine("         , GM_DELTA_PLAYED_AMOUNT ");
      _sql_txt.AppendLine("         , GM_DELTA_WON_COUNT ");
      _sql_txt.AppendLine("         , GM_DELTA_WON_AMOUNT ");
      _sql_txt.AppendLine("         , GM_DELTA_JACKPOT_AMOUNT ");
      _sql_txt.AppendLine("         , GM_DELTA_PROGRESSIVE_JACKPOT_AMOUNT ");
      _sql_txt.AppendLine("         , (SELECT TE_NAME FROM TERMINALS WHERE TE_TERMINAL_ID = GM_TERMINAL_ID) AS 'TERMINAL_NAME' ");
      _sql_txt.AppendLine("         , (SELECT GM_GAME_ID FROM GAMES WHERE GM_NAME = GM_DELTA_GAME_NAME) AS 'GAME_ID' ");
      _sql_txt.AppendLine("         , 0 AS 'NUM_ACTIVE' ");
      _sql_txt.AppendLine("         , GETDATE() AS 'BASE_HOUR' ");
      _sql_txt.AppendLine("         , 0 AS 'NUM_ZERO' ");
      _sql_txt.AppendLine("    FROM   GAME_METERS ");
      _sql_txt.AppendLine("   WHERE   GM_DELTA_UPDATING = 1 ");
      _sql_txt.AppendLine("ORDER BY   GM_TERMINAL_ID  ");
      _sql_txt.AppendLine("         , GM_GAME_BASE_NAME  ");

      _da.SelectCommand = new SqlCommand(_sql_txt.ToString());
      _da.SelectCommand.Connection = Trx.Connection;
      _da.SelectCommand.Transaction = Trx;

      //        - UpdateCommand, DeleteCommand & InsertCommand
      _da.DeleteCommand = null;
      _da.InsertCommand = null;
      _da.UpdateCommand = null;

      //    - Execute Select command
      _da.Fill(DeltaGameMeters);

      if (DeltaGameMeters.Rows.Count == 0)
      {
        return false;
      }

      for (_idx_row = 0; _idx_row < DeltaGameMeters.Rows.Count; _idx_row++)
      {
        if (_idx_row == 0)
        {
          _base_hour = (DateTime)DeltaGameMeters.Rows[_idx_row]["BASE_HOUR"];
          _base_hour = _base_hour.AddMinutes(-_base_hour.Minute);
          _base_hour = _base_hour.AddSeconds(-_base_hour.Second);
          _base_hour = _base_hour.AddMilliseconds(-_base_hour.Millisecond);
        }

        // Truncate current datetime and set the row as mofidied
        DeltaGameMeters.Rows[_idx_row]["BASE_HOUR"] = _base_hour;
        DeltaGameMeters.Rows[_idx_row]["NUM_ACTIVE"] = _num_active_terminals;
        DeltaGameMeters.Rows[_idx_row].AcceptChanges();
        DeltaGameMeters.Rows[_idx_row].SetModified();
      }

      return true;

    } // GetDeltaGameMeters

    //------------------------------------------------------------------------------
    // PURPOSE : Update delta game meters.
    //
    //  PARAMS :
    //      - INPUT :
    //          - Trx
    //          - DeltaGameMeters
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True or False
    //
    //   NOTES :
    //      - true: delta game meters Selected from Game Meters.
    //      - false: error.
    //
    static public Boolean UpdateDeltaGameMeters(DataTable DeltaGameMeters, SqlTransaction Trx)
    {
      StringBuilder _sql_txt;
      SqlDataAdapter _da;
      int _update_batch_size = 500;
      int _idx_row;
      int _nr;

      //    - Set SqlDataAdapter commands
      //        - SelectCommand 
      //        - DeleteCommand 
      //        - InsertCommand 
      //        - UpdateCommand 
      _da = new SqlDataAdapter();

      //        - UpdateCommand 
      _sql_txt = new StringBuilder();
      _sql_txt.AppendLine("UPDATE GAME_METERS");
      _sql_txt.AppendLine("   SET GM_DELTA_PLAYED_COUNT   = GM_DELTA_PLAYED_COUNT   - @p1");
      _sql_txt.AppendLine("     , GM_DELTA_PLAYED_AMOUNT  = GM_DELTA_PLAYED_AMOUNT  - @p2");
      _sql_txt.AppendLine("     , GM_DELTA_WON_COUNT      = GM_DELTA_WON_COUNT      - @p3");
      _sql_txt.AppendLine("     , GM_DELTA_WON_AMOUNT     = GM_DELTA_WON_AMOUNT     - @p4");
      _sql_txt.AppendLine("     , GM_DELTA_JACKPOT_AMOUNT = GM_DELTA_JACKPOT_AMOUNT - @p5");
      _sql_txt.AppendLine("     , GM_DELTA_PROGRESSIVE_JACKPOT_AMOUNT = GM_DELTA_PROGRESSIVE_JACKPOT_AMOUNT - @p8");
      _sql_txt.AppendLine("     , GM_DELTA_UPDATING       = 0");
      _sql_txt.AppendLine(" WHERE GM_TERMINAL_ID          = @p6");
      _sql_txt.AppendLine("   AND GM_GAME_BASE_NAME       = @p7");
      _sql_txt.AppendLine("   AND GM_DELTA_UPDATING       = 1");

      _da.UpdateCommand = new SqlCommand(_sql_txt.ToString());
      _da.UpdateCommand.Connection = Trx.Connection;
      _da.UpdateCommand.Transaction = Trx;
      _da.UpdateCommand.UpdatedRowSource = UpdateRowSource.None;

      _da.UpdateCommand.Parameters.Add("@p1", SqlDbType.BigInt, 8, "GM_DELTA_PLAYED_COUNT");
      _da.UpdateCommand.Parameters.Add("@p2", SqlDbType.Money, 8, "GM_DELTA_PLAYED_AMOUNT");
      _da.UpdateCommand.Parameters.Add("@p3", SqlDbType.BigInt, 8, "GM_DELTA_WON_COUNT");
      _da.UpdateCommand.Parameters.Add("@p4", SqlDbType.Money, 8, "GM_DELTA_WON_AMOUNT");
      _da.UpdateCommand.Parameters.Add("@p5", SqlDbType.Money, 8, "GM_DELTA_JACKPOT_AMOUNT");
      _da.UpdateCommand.Parameters.Add("@p6", SqlDbType.Int, 4, "GM_TERMINAL_ID");
      _da.UpdateCommand.Parameters.Add("@p7", SqlDbType.NVarChar, 50, "GM_GAME_BASE_NAME");
      _da.UpdateCommand.Parameters.Add("@p8", SqlDbType.Money, 8, "GM_DELTA_PROGRESSIVE_JACKPOT_AMOUNT");

      //        - DeleteCommand & InsertCommand
      _da.DeleteCommand = null;
      _da.InsertCommand = null;

      //    - SetModified
      for (_idx_row = 0; _idx_row < DeltaGameMeters.Rows.Count; _idx_row++)
      {
        DeltaGameMeters.Rows[_idx_row].SetModified();
      }

      //    - Execute Update command
      _da.UpdateBatchSize = _update_batch_size;
      _da.ContinueUpdateOnError = true;

      _nr = _da.Update(DeltaGameMeters);
      if (_nr != DeltaGameMeters.Rows.Count)
      {
        return false;
      }

      return true;

    } // UpdateDeltaGameMeters

    //------------------------------------------------------------------------------
    // PURPOSE : Insert Handpay
    //
    //  PARAMS :
    //      - INPUT :
    //          - TerminalId
    //          - DeltaGameMeters
    //          - Trx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //      - true: Handpay inserted.
    //      - false: error.
    //
    static private Boolean DB_InsertHandpay(Int32 TerminalId, GameMeters DeltaGameMeters, SqlTransaction Trx)
    {
      SqlCommand _sql_command;
      SqlDataReader _reader;
      String _sql_query;
      DateTime _last_reported;
      String _terminal_name;
      String _provider_id;
      Int32 _num_rows_inserted;

      //SELECT to get HP_PREVIOUS_METERS = GM_LAST_REPORTED,
      //HP_TE_NAME = TE_NAME and HP_TE_PROVIDER_ID = TE_PROVIDER_ID.

      _sql_query = "SELECT GM_LAST_REPORTED " +
                   "     , TE_NAME " +
                   "     , TE_PROVIDER_ID " +
                   "FROM   GAME_METERS, TERMINALS " +
                   "WHERE  GM_TERMINAL_ID    = TE_TERMINAL_ID " +
                   "AND    GM_TERMINAL_ID    = @TerminalId " +
                   "AND    GM_GAME_BASE_NAME = @DeltaGameName ";

      _sql_command = new SqlCommand(_sql_query);
      _sql_command.Connection = Trx.Connection;
      _sql_command.Transaction = Trx;

      _sql_command.Parameters.Add("@TerminalId", SqlDbType.Int).Value = TerminalId;
      _sql_command.Parameters.Add("@DeltaGameName", SqlDbType.NVarChar).Value = DeltaGameMeters.GameBaseName;

      _reader = null;
      _last_reported = System.DateTime.Now;
      _terminal_name = null;
      _provider_id = null;

      try
      {
        _reader = _sql_command.ExecuteReader();
        if (_reader.Read())
        {
          _last_reported = (DateTime)_reader[0];
          _terminal_name = _reader[1].ToString();
          _provider_id = _reader[2].ToString();
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        Log.Warning("Exception throw: Error reading Handpay-GameMeter information " + TerminalId.ToString());
        return false;
      }
      finally
      {
        if (_reader != null)
        {
          _reader.Close();
          _reader.Dispose();
          _reader = null;
        }
      }

      //INSERT HANDPAY with HP_TYPE MachineReported = 0.

      _sql_query = "INSERT INTO HANDPAYS (HP_TERMINAL_ID " +
                                      " , HP_DATETIME " +
                                      " , HP_PREVIOUS_METERS " +
                                      " , HP_AMOUNT " +
                                      " , HP_TE_NAME " +
                                      " , HP_TE_PROVIDER_ID " +
                                      " , HP_TYPE) " +
                                " VALUES (@TerminalId " +
                                      " , GETDATE() " +
                                      " , @PreviousMeters " +
                                      " , @Amount " +
                                      " , @TermName " +
                                      " , @ProviderId " +
                                      " , " + ((int)HANDPAY_TYPE.JACKPOT) + ") ";

      _sql_command = new SqlCommand(_sql_query);
      _sql_command.Connection = Trx.Connection;
      _sql_command.Transaction = Trx;

      _sql_command.Parameters.Add("@TerminalId", SqlDbType.Int).Value = TerminalId;
      _sql_command.Parameters.Add("@PreviousMeters", SqlDbType.DateTime).Value = _last_reported;
      _sql_command.Parameters.Add("@Amount", SqlDbType.Money).Value = DeltaGameMeters.JackpotCents / 100;
      _sql_command.Parameters.Add("@TermName", SqlDbType.NVarChar).Value = _terminal_name;
      _sql_command.Parameters.Add("@ProviderId", SqlDbType.NVarChar).Value = _provider_id;

      try
      {
        _num_rows_inserted = _sql_command.ExecuteNonQuery();
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        Log.Warning("Inserting HANDPAY [Exception]: TerminalId: " + TerminalId.ToString());

        return false;
      }

      if (_num_rows_inserted != 1)
      {
        Log.Warning("Inserting HANDPAY [Not updated]: TerminalId: " + TerminalId.ToString());

        return false;
      }

      return true;
    } // DB_InsertHandpay

    //------------------------------------------------------------------------------
    // PURPOSE : Update Last Played Game.
    //
    //  PARAMS :
    //      - INPUT :
    //          - DeltaGameMeter: Datatable with delta game meters
    //          - Trx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True or false
    //
    //   NOTES :
    //      - true: Update Last game played in table terminals.
    //      - false: error.
    //
    static private Boolean UpdateLastPlayedGame(DataTable DeltaGameMeters, SqlTransaction Trx)
    {
      StringBuilder _sql_txt;
      StringBuilder _select;
      SqlDataAdapter _da;
      DataTable _clone_delta_game_meters;

      int _update_batch_size = 500;
      int _nr;

      if (DeltaGameMeters.Rows.Count == 0)
      {
        return true;
      }

      _clone_delta_game_meters = DeltaGameMeters.GetChanges();

      try
      {

        //    - Set SqlDataAdapter commands

        _da = new SqlDataAdapter();

        // When batch updates are enabled (UpdateBatchSize != 1), the UpdatedRowSource property value 
        // of the DataAdapter's UpdateCommand, InsertCommand, and DeleteCommand should be set to None 
        // or OutputParameters. 
        _da.UpdateBatchSize = _update_batch_size;

        _select = new StringBuilder();

        //        - UpdateCommand 
        _sql_txt = new StringBuilder();

        _sql_txt.AppendLine(_select.ToString());

        _sql_txt.AppendLine("UPDATE TERMINALS");
        _sql_txt.AppendLine("   SET TE_LAST_GAME_PLAYED_ID     = isnull(@pGameId, 0)  ");
        _sql_txt.AppendLine(" WHERE TE_TERMINAL_ID             = @pTerminalId");

        _da.UpdateCommand = new SqlCommand(_sql_txt.ToString(), Trx.Connection, Trx);
        _da.UpdateCommand.UpdatedRowSource = UpdateRowSource.None;

        _da.UpdateCommand.Parameters.Add("@pGameId", SqlDbType.Int, 4, "GAME_ID");
        _da.UpdateCommand.Parameters.Add("@pTerminalId", SqlDbType.Int, 4, "GM_TERMINAL_ID");

        //        - DeleteCommand & SelectCommand   
        _da.DeleteCommand = null;
        _da.SelectCommand = null;

        //    - Execute Update command
        _da.InsertCommand = null;
        _da.UpdateBatchSize = _update_batch_size;
        _da.ContinueUpdateOnError = false;

        _nr = _da.Update(_clone_delta_game_meters);


        if (_clone_delta_game_meters.Rows.Count == _nr)
        {
          return true;
        }


        _da.UpdateCommand = null;


        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
      finally
      {
      }
    } // UpdateLastGamePlayedTerminals



    #endregion // Private Functions

  }
}
