﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems Ltd.
//------------------------------------------------------------------------------
//
//   MODULE NAME: WCP_HistorificationTasks.cs
//
//   DESCRIPTION: WCP_HistorificationTasks class
//
//        AUTHOR: Alberto Marcos
// 
// CREATION DATE: 10-SEP-2015

// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 10-SEP-2015 AMF    First release.
// 06-ABR-2016 SMN    PBI 10737:SmartFloor: Bugs
// 07-ABR-2016 SMN    Task 11356:Historificación - Site
//------------------------------------------------------------------------------

using System;
using System.Messaging;
using System.Collections.Generic;
using System.Text;
using System.Net;
using WSI.Common;
using System.Xml;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using WSI.WCP;
using System.Collections;
using System.Threading;
using WSI.Common.TITO;
using System.Globalization;
namespace WSI.WCP
{
  public static class WCP_HistorificationTasks
  {
    #region Members

    static private Int32 m_previous_days;

    static private Int32 m_closing_time;

    static private DateTime m_date_initial_historification;

    static private Int32 m_site_identifier;

    #endregion Members

    #region Enums

    private enum HISTORIFICATION_TYPE
    {
      PLAYER = 1,
      TERMINALS_SITE = 2,
    }

    private enum FLAG_METER_TYPE
    {
      TERMINAL = 0x001,
      SITE = 0x002,
      // 0x004
      // 0x008
    }

    enum ENUM_TABLE_ID
    {

      Daily = 1,
      Weekly,
      Monthly,
      Yearly,

    }

    enum ENUM_ENTITY_ID
    {

      Terminals = 1,
      Sites,

    }

    enum ENUM_HISTORIFICATION
    {

      Current = 1,
      Past
    }

    #endregion Enums

    #region Constants

    private const String X2D_DATE = "X2D_DATE";
    private const String X2D_METER_TYPE = "X2D_METER_TYPE";
    private const String X2D_EXECUTION = "X2D_EXECUTION";
    private const String SP_NAME_GET_METERS = "SMARTFLOOR_TVH";
    private const String SP_NAME_UPDATE = "SP_UPDATE_METER_DATA_SITE";
    private const String EXCEPTION_WCP_HISTORIFACION_TASK = "Exception into class WCP_HistorificationTask.";
    private const String EXCEPTION_MESSAGE = ". Message: ";

    #endregion Constants

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Initialize WCP_HistorificationTasks class 
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    public static void Init()
    {
      try
      {
        Thread _thread;

        _thread = new Thread(HistorificationTasks);
        _thread.Name = "HistorificationTasks";
        _thread.Start();
      }

      catch (Exception _ex)
      {
        Log.Exception(_ex);
        Log.Warning(String.Format(EXCEPTION_WCP_HISTORIFACION_TASK + "Init" + EXCEPTION_MESSAGE + "{0} - {1} .", _ex.Message, WGDB.Now));
      }
    } // Init

    #endregion Public Methods

    #region Private Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Main thread
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    static private void HistorificationTasks()
    {
      int _wait_hint;
      DateTime _now;
      DateTime _date_start;
      DateTime _date_end;
      String _date_initial;
      Boolean _rc;


      // Read General Params
      m_closing_time = GeneralParam.GetInt32("WigosGUI", "ClosingTime", 0);
      m_previous_days = GeneralParam.GetInt32("HistoricalData", "HistorificationTasks.NumDaysProcessed", 1);
      m_site_identifier = GeneralParam.GetInt32("Site", "Identifier", 0);

      try
      {
        _date_initial = GeneralParam.GetString("HistoricalData", "HistorificationTasks.DateStart", WGDB.Now.ToString("yyyyMMdd"));
        m_date_initial_historification = DateTime.ParseExact(_date_initial, "yyyymmdd", CultureInfo.InvariantCulture);
        m_date_initial_historification = new DateTime(m_date_initial_historification.Year, m_date_initial_historification.Month, m_date_initial_historification.Day, 0, 0, 0);
      }
      catch (Exception _ex)
      {
        Log.Message("HistorificationTasks. Error in parse General Param: " + "HistoricalData.HistorificationTasks.DateStart");
        Log.Exception(_ex);
        _now = WGDB.Now;
        m_date_initial_historification = new DateTime(_now.Year, _now.Month, _now.Day, 0, 0, 0);
      }

      try
      {

        while (true)
        {

          if (!GeneralParam.GetBoolean("HistoricalData", "HistorificationTasks.Enabled", false))
          {
            Thread.Sleep(5 * 60 * 1000);  // 5 minutes.

            continue;
          }

          // Only the service running as 'Principal' will excute the tasks.
          if (!Services.IsPrincipal("WCP"))
          {
            Thread.Sleep(5 * 60 * 1000);   // 5 minutes.

            continue;
          }

          _now = WGDB.Now;
          _wait_hint = 60 * 1000 - _now.Second * 1000 - _now.Millisecond;

          Thread.Sleep(_wait_hint);

          //Players
          _date_end = new DateTime(_now.Year, _now.Month, _now.Day, m_closing_time, 0, 0);
          _date_start = _date_end.AddDays(-1);

          _rc = Historification(_now, _date_start, HISTORIFICATION_TYPE.PLAYER);

          //Terminals
          _date_end = new DateTime(_now.Year, _now.Month, _now.Day, _now.Hour, 0, 0);
          _date_start = _date_end.AddHours(-1);
          _rc = Historification(_now, _date_start, HISTORIFICATION_TYPE.TERMINALS_SITE);


        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        Log.Warning(String.Format(EXCEPTION_WCP_HISTORIFACION_TASK + "HistorificationTask" + EXCEPTION_MESSAGE + "{0} - {1} .", _ex.Message, WGDB.Now));
      }
    } // HistorificationTasks

    //------------------------------------------------------------------------------
    // PURPOSE : Executes Historification for all meter types.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - DateNow
    //      - DateStart
    //      - MeterType
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean
    //   NOTES :
    private static Boolean Historification(DateTime DateNow, DateTime DateStart, HISTORIFICATION_TYPE MeterType)
    {
      DateTime _date_end;
      DateTime _date_start;
      DataTable ControlRegisters;
      Int32 _closing_time;

      try
      {
        switch (MeterType)
        {
          case HISTORIFICATION_TYPE.PLAYER:
            _closing_time = m_closing_time;
            break;

          case HISTORIFICATION_TYPE.TERMINALS_SITE:
            _closing_time = DateNow.Hour;
            break;

          default:
            Log.Error("Historification.MeterType is incorrect");

            return false;
        }

        _date_end = new DateTime(DateNow.Year, DateNow.Month, DateNow.Day).AddHours((double)_closing_time);

        if (DateNow >= _date_end && !IsControlRegisterExecuted(DateStart, MeterType))
        {
          ControlRegisters = null;
          if (GenerateControlRegistersX2D(DateStart, MeterType, ENUM_HISTORIFICATION.Current, out ControlRegisters))
          {
            if (InsertControlRegistersX2D(ControlRegisters))
            {

              // Historicize current
              switch (MeterType)
              {
                case HISTORIFICATION_TYPE.PLAYER:
                  if (!HistorificationPVH(DateStart, _date_end))
                  {
                    Log.Warning(String.Format("WCP_HistorificationTasks.HistorificationPVH. DateStart={0}; DateEnd={1}", DateStart.ToString("dd/MM/yyyy HH:mm:ss"), _date_end.ToString("dd/MM/yyyy HH:mm:ss")));
                  }
                  break;
                case HISTORIFICATION_TYPE.TERMINALS_SITE:
                  if (!HistorificationTMHSMH(DateStart, _date_end))
                  {
                    Log.Warning(String.Format("WCP_HistorificationTasks.HistorificationTMHSMH. DateStart={0}; DateEnd={1}", DateStart.ToString("dd/MM/yyyy HH:mm:ss"), _date_end.ToString("dd/MM/yyyy HH:mm:ss")));
                  }
                  break;
                default:
                  Log.Error("Historification.MeterType is incorrect");

                  return false;
              }

              // Generate control days to past
              ControlRegisters = null;

              if (ExistsMissingDays(MeterType, DateStart))
              {
                if (GenerateControlRegistersX2D(DateStart, MeterType, ENUM_HISTORIFICATION.Past, out ControlRegisters))
                {
                  if (ControlRegisters.Rows.Count > 0)
                  {
                    if (!InsertControlRegistersX2D(ControlRegisters))
                    {
                      Log.Warning(String.Format("WCP_HistorificationTasks.Historification. Not inserted registers to historicize. {0} ", WGDB.Now));
                    }
                  }
                }
              }

              // Historicize pending
              ControlRegisters = null;
              if (GetControlRegistersX2D(DateStart, MeterType, out ControlRegisters))
              {
                foreach (DataRow _register in ControlRegisters.Rows)
                {
                  _date_start = ((DateTime)_register[X2D_DATE]);

                  switch (MeterType)
                  {
                    case HISTORIFICATION_TYPE.PLAYER:
                      _date_end = _date_start.AddDays(1);

                      if (!HistorificationPVH(_date_start, _date_end))
                      {
                        Log.Warning(String.Format("WCP_HistorificationTasks.HistorificationPVH. DateStart={0}; DateEnd={1}", _date_start.ToString("dd/MM/yyyy HH:mm:ss"), _date_end.ToString("dd/MM/yyyy HH:mm:ss")));
                      }
                      break;
                    case HISTORIFICATION_TYPE.TERMINALS_SITE:
                      _date_end = _date_start.AddHours(1);

                      if (!HistorificationTMHSMH(_date_start, _date_end))
                      {
                        Log.Warning(String.Format("WCP_HistorificationTasks.HistorificationTMHSMH. DateStart={0}; DateEnd={1}", _date_start.ToString("dd/MM/yyyy HH:mm:ss"), _date_end.ToString("dd/MM/yyyy HH:mm:ss")));
                      }
                      break;
                    default:
                      Log.Error("Historification.MeterType is incorrect");

                      return false;
                  }

                }
              }

            }
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Message("Error: WCP_HistorificationTask.Historification"); //TODO
        Log.Exception(_ex);
      }

      return false;

    } // Historification

    //------------------------------------------------------------------------------
    // PURPOSE : Checks if missing some register in  table H_X2D_Control
    //
    //  PARAMS :
    //      - INPUT :
    //      - MeterType
    //      - DateControl
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean
    //   NOTES :
    private static Boolean ExistsMissingDays(HISTORIFICATION_TYPE MeterType, DateTime DateControl)
    {

      StringBuilder _sb_select;
      String _day_hour;
      DateTime _initial_historification;

      _initial_historification = m_date_initial_historification.AddHours(m_closing_time);

      _day_hour = (MeterType == HISTORIFICATION_TYPE.TERMINALS_SITE) ? "HOUR" : "DAY";

      _sb_select = new StringBuilder();
      _sb_select.AppendLine("   DECLARE @NUM_REG INTEGER                       ");
      _sb_select.AppendLine("   DECLARE @NUM_REG_DIFF INTEGER                  ");
      _sb_select.AppendLine("    SELECT @NUM_REG = COUNT(*)                    ");
      _sb_select.AppendLine("      FROM H_X2D_CONTROL   ");
      _sb_select.AppendLine("     WHERE X2D_METER_TYPE = @pMeterType  ");
      _sb_select.AppendLine("       AND X2D_DATE < @pDateControl  ");
      _sb_select.AppendLine("       AND X2D_DATE >= @pInitialHistorification  ");

      _sb_select.AppendLine(String.Format("     SET @NUM_REG_DIFF =DATEDIFF({0}, @pInitialHistorification, @pDateControl)   ", _day_hour));

      _sb_select.AppendLine("     SELECT CASE WHEN @NUM_REG_DIFF <> @NUM_REG THEN 1 ELSE 0 END   "); // return 1 if exists differences


      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb_select.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {

            _sql_cmd.Parameters.Add("@pMeterType", SqlDbType.TinyInt).Value = MeterType;
            _sql_cmd.Parameters.Add("@pInitialHistorification", SqlDbType.DateTime).Value = _initial_historification;
            _sql_cmd.Parameters.Add("@pDateControl", SqlDbType.DateTime).Value = DateControl;

            if ((int)_sql_cmd.ExecuteScalar() == 1)
            {
              return true;
            }
            else
            {
              return false;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        Log.Warning(String.Format(EXCEPTION_WCP_HISTORIFACION_TASK + "ExistsMissingDays" + EXCEPTION_MESSAGE + "{0} - {1} .", _ex.Message, WGDB.Now));
      }

      return false;
    } //ExistingMissingDays

    //------------------------------------------------------------------------------
    // PURPOSE : Generate DataTable with the control registers to insert
    //
    //  PARAMS :
    //      - INPUT :
    //            - DateControl
    //            - MeterType
    //            - TypeHistorification
    //      - OUTPUT :
    //            - ControlRegisters
    // RETURNS :
    //      - DataTable
    //
    //   NOTES :
    private static Boolean GenerateControlRegistersX2D(DateTime DateControl, HISTORIFICATION_TYPE MeterType, ENUM_HISTORIFICATION TypeHistorification, out DataTable ControlRegisters)
    {
      TimeSpan _day;
      Int32 _init_day;
      Int32 _init_hours;
      Int32 _closing_time;
      DateTime _date_final;

      ControlRegisters = new DataTable();
      _closing_time = GeneralParam.GetInt32("WigosGUI", "ClosingTime", 0);
      try
      {

        ControlRegisters.Columns.Add(X2D_DATE, Type.GetType("System.DateTime"));
        ControlRegisters.Columns.Add(X2D_METER_TYPE, Type.GetType("System.Int32"));

        switch (MeterType)
        {
          case HISTORIFICATION_TYPE.PLAYER:
            switch (TypeHistorification)
            {
              case ENUM_HISTORIFICATION.Current:

                ControlRegisters.Rows.Add(DateControl, Convert.ToInt32(MeterType));

                break;

              case ENUM_HISTORIFICATION.Past:
                _day = DateControl.Subtract(m_date_initial_historification);
                _init_day = _day.Days;

                for (int id = 0; id < _init_day; id++)
                {
                  _date_final = m_date_initial_historification.AddDays(id).AddHours(_closing_time);
                  ControlRegisters.Rows.Add(_date_final, Convert.ToInt32(MeterType));
                }

                break;
            }

            break;

          case HISTORIFICATION_TYPE.TERMINALS_SITE:
            switch (TypeHistorification)
            {
              case ENUM_HISTORIFICATION.Current:

                ControlRegisters.Rows.Add(DateControl, Convert.ToInt32(MeterType));

                break;

              case ENUM_HISTORIFICATION.Past:
                _day = DateControl.Subtract(m_date_initial_historification);
                _init_hours = _day.Days * 24 + _day.Hours;

                for (int id = 0; id < _init_hours; id++)
                {
                  _date_final = m_date_initial_historification.AddHours(id);
                  ControlRegisters.Rows.Add(_date_final, Convert.ToInt32(MeterType));
                }

                break;
            }

            break;
        } // switch

        return true;

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        Log.Warning(String.Format(EXCEPTION_WCP_HISTORIFACION_TASK + "GenerateControlRegistersX2D" + EXCEPTION_MESSAGE + "{0} - {1} .", _ex.Message, WGDB.Now));
      }
      return false;
    } // GenerateControlRegistersX2D

    //------------------------------------------------------------------------------
    // PURPOSE : Inserts registers generates into GenerateControlRegistersX2D
    //  PARAMS :
    //      - INPUT :
    //            - DateTime
    //            - METER_TYPE
    //            - ENUM_HISTORIFICATION
    //      - OUTPUT :
    //            - DataTable
    // RETURNS :
    //      - Boolean
    //
    //   NOTES :
    private static Boolean InsertControlRegistersX2D(DataTable DataControl)
    {
      StringBuilder _sb_insert;

      _sb_insert = new StringBuilder();
      _sb_insert.AppendLine(" IF NOT EXISTS (SELECT   1                                                 ");
      _sb_insert.AppendLine("                  FROM   H_X2D_CONTROL                                     ");
      _sb_insert.AppendLine("                 WHERE   X2D_DATE                 = @pDate                 ");
      _sb_insert.AppendLine("                   AND   X2D_METER_TYPE           = @pMeterType)           ");
      _sb_insert.AppendLine("                                                                           ");
      _sb_insert.AppendLine(" BEGIN                                                                     ");
      _sb_insert.AppendLine(" INSERT INTO   H_X2D_CONTROL                                               ");
      _sb_insert.AppendLine("             ( X2D_DATE                                                    ");
      _sb_insert.AppendLine("             , X2D_METER_TYPE                                              ");
      _sb_insert.AppendLine("             )                                                             ");
      _sb_insert.AppendLine("      VALUES (                                                             ");
      _sb_insert.AppendLine("               @pDate                                                      ");
      _sb_insert.AppendLine("             , @pMeterType                                                 ");
      _sb_insert.AppendLine("             )                                                             ");
      _sb_insert.AppendLine(" END                                                                       ");

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb_insert.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pDate", SqlDbType.DateTime).SourceColumn = X2D_DATE;
            _sql_cmd.Parameters.Add("@pMeterType", SqlDbType.Int).SourceColumn = X2D_METER_TYPE;

            using (SqlDataAdapter _sql_da = new SqlDataAdapter())
            {
              _sql_da.InsertCommand = _sql_cmd;
              _sql_da.InsertCommand.UpdatedRowSource = UpdateRowSource.None;
              _sql_da.UpdateBatchSize = 500;

              _sql_da.Update(DataControl);

              _db_trx.Commit();

              return true;
            }
          }
        }
      }

      catch (Exception _ex)
      {
        Log.Warning(String.Format(EXCEPTION_WCP_HISTORIFACION_TASK + "InsertControlRegistersX2D" + EXCEPTION_MESSAGE + "{0} - {1} .", _ex.Message, WGDB.Now));
      }

      return false;
    } // InsertControlRegistersX2D

    //------------------------------------------------------------------------------
    // PURPOSE : Generate DataTable with the control registers to insert
    //
    //  PARAMS :
    //      - INPUT :
    //            - DateTime
    //            - METER_TYPE
    //            
    //
    //      - OUTPUT :
    //            - DataTable
    // RETURNS :
    //      - Boolean
    //
    //   NOTES :
    private static Boolean GetControlRegistersX2D(DateTime DateControl, HISTORIFICATION_TYPE MeterType, out DataTable Dt)
    {

      StringBuilder _sb_select;

      Dt = new DataTable();

      _sb_select = new StringBuilder();
      _sb_select.AppendLine(String.Format("    SELECT   TOP {0} X2D_DATE                         ", m_previous_days));
      _sb_select.AppendLine("           , X2D_METER_TYPE                         ");
      _sb_select.AppendLine("      FROM   H_X2D_CONTROL                          ");
      _sb_select.AppendLine("     WHERE   X2D_EXECUTION IS NULL                  ");
      _sb_select.AppendLine("       AND   X2D_DATE                 < @pDateEnd   ");
      _sb_select.AppendLine("       AND   X2D_DATE                >= @pDateStart ");
      _sb_select.AppendLine("       AND   X2D_METER_TYPE           = @pMeterType ");
      _sb_select.AppendLine("  ORDER BY   X2D_DATE DESC                          ");


      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb_select.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pDateEnd", System.Data.SqlDbType.DateTime).Value = DateControl;
            _sql_cmd.Parameters.Add("@pDateStart", System.Data.SqlDbType.DateTime).Value = m_date_initial_historification.AddHours(m_closing_time);// DateControl.AddDays(-m_previous_days);
            _sql_cmd.Parameters.Add("@pMeterType", System.Data.SqlDbType.Int).Value = MeterType;

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
            {
              _sql_da.Fill(Dt);

              return true;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        Log.Warning(String.Format(EXCEPTION_WCP_HISTORIFACION_TASK + "GetControlRegistersX2D" + EXCEPTION_MESSAGE + "{0} - {1} .", _ex.Message, WGDB.Now));
      }

      return false;
    } // GetControlRegistersX2D

    //------------------------------------------------------------------------------
    // PURPOSE : The control register is executed
    //
    //  PARAMS :
    //      - INPUT :
    //            - DateTime
    //            - METER_TYPE
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean
    //
    //   NOTES :
    private static Boolean IsControlRegisterExecuted(DateTime DateControl, HISTORIFICATION_TYPE MeterType) // IsControlRegisterExecuted
    {
      StringBuilder _sb_select;

      _sb_select = new StringBuilder();
      _sb_select.AppendLine("    SELECT   1                            ");
      _sb_select.AppendLine("      FROM   H_X2D_CONTROL                ");
      _sb_select.AppendLine("     WHERE   X2D_EXECUTION IS NOT NULL    ");
      _sb_select.AppendLine("       AND   X2D_DATE       = @pDate      ");
      _sb_select.AppendLine("       AND   X2D_METER_TYPE = @pMeterType ");

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb_select.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pDate", System.Data.SqlDbType.DateTime).Value = DateControl;
            _sql_cmd.Parameters.Add("@pMeterType", System.Data.SqlDbType.Int).Value = MeterType;

            using (SqlDataReader _sql_rd = _sql_cmd.ExecuteReader())
            {
              if (_sql_rd.Read())
              {
                return true;
              }
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        Log.Warning(String.Format(EXCEPTION_WCP_HISTORIFACION_TASK + "IsControlRegisterExecuted" + EXCEPTION_MESSAGE + "{0} - {1} .", _ex.Message, WGDB.Now));
      }

      return false;
    }//IsControlRegisterExecuted

    //------------------------------------------------------------------------------
    // PURPOSE : Update execution control registers
    //
    //  PARAMS :
    //      - INPUT :
    //            - DateTime
    //            - METER_TYPE
    //            - Trx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean
    //
    //   NOTES :
    private static Boolean UpdateControlRegistersX2D(DateTime DateControl, HISTORIFICATION_TYPE MeterType, SqlTransaction Trx)
    {
      StringBuilder _sb_update;

      _sb_update = new StringBuilder();
      _sb_update.AppendLine("    UPDATE   H_X2D_CONTROL                ");
      _sb_update.AppendLine("       SET   X2D_EXECUTION  = GETDATE()   ");
      _sb_update.AppendLine("     WHERE   X2D_DATE       = @pDate      ");
      _sb_update.AppendLine("       AND   X2D_METER_TYPE = @pMeterType ");

      try
      {
        using (SqlCommand _sql_cmd = new SqlCommand(_sb_update.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pDate", System.Data.SqlDbType.DateTime).Value = DateControl;
          _sql_cmd.Parameters.Add("@pMeterType", System.Data.SqlDbType.Int).Value = MeterType;

          if (_sql_cmd.ExecuteNonQuery() == 1)
          {
            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        Log.Warning(String.Format(EXCEPTION_WCP_HISTORIFACION_TASK + "UpdateControlRegistersX2D" + EXCEPTION_MESSAGE + "{0} - {1} .", _ex.Message, WGDB.Now));
      }

      return false;
    }//UpdatecontrolRegistersX2D


    #endregion Private Methods.
    #region PVH
    //------------------------------------------------------------------------------
    // PURPOSE : Players Visit History
    //
    //  PARAMS :
    //      - INPUT :
    //            - DateStart
    //            - DateEnd
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean
    //
    //   NOTES :
    private static Boolean HistorificationPVH(DateTime DateStart, DateTime DateEnd)
    {
      Boolean _is_tito_mode;
      _is_tito_mode = WSI.Common.TITO.Utils.IsTitoMode();

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (UpdateControlRegistersX2D(DateStart, HISTORIFICATION_TYPE.PLAYER, _db_trx.SqlTransaction))
          {
            if (GetAndInsertPVH(DateStart, DateEnd, _is_tito_mode, _db_trx.SqlTransaction))
            {
              _db_trx.Commit();

              return true;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Warning(EXCEPTION_WCP_HISTORIFACION_TASK + "HistorificationPVH" + EXCEPTION_MESSAGE + "DateStart=" + DateStart.ToString("yyyy/MM/dd HH:mm:ss") + "DateEnd=" + DateEnd.ToString("yyyy/MM/dd HH:mm:ss"));
        Log.Exception(_ex);
      }

      return false;
    } // HistorificationPVH


    //------------------------------------------------------------------------------
    // PURPOSE :Execute a stored procedure and inserts or updates records in Table H_PVH
    //
    //      - INPUT :   DateTime DateStart
    //                  DateTime DateEnd
    //                  Boolean IsTitoMode
    //                  SqlTransaction Trx
    //
    // RETURNS :   TRUE OR FALSE

    static Boolean GetAndInsertPVH(DateTime DateStart, DateTime DateEnd, Boolean IsTitoMode, SqlTransaction Trx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" EXEC  dbo.SmartFloor_PVH @pDateStart  ,@pDateEnd ,@pIsTitoMode ");


        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pDateStart", SqlDbType.DateTime).Value = DateStart;
          _sql_cmd.Parameters.Add("@pDateEnd", SqlDbType.DateTime).Value = DateEnd;
          _sql_cmd.Parameters.Add("@pIsTitoMode", SqlDbType.Bit).Value = IsTitoMode;
          Convert.ToInt32(_sql_cmd.ExecuteScalar());
          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Warning(EXCEPTION_WCP_HISTORIFACION_TASK + "GetAndInsertPVH" + EXCEPTION_MESSAGE + "DateStart=" + DateStart.ToString("yyyy/MM/dd HH:mm:ss") + "DateEnd=" + DateEnd.ToString("yyyy/MM/dd HH:mm:ss") + "IsTitoMode=" + IsTitoMode.ToString());
        Log.Exception(_ex);
      }

      return false;
    } // GetAndInsertPVH
    #endregion PVH

    #region TMH - SMH

    //------------------------------------------------------------------------------
    // PURPOSE : TMH - SMH
    //
    //  PARAMS :
    //      - INPUT :
    //            - DateStart
    //            - DateEnd
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean
    //
    //   NOTES :
    private static Boolean HistorificationTMHSMH(DateTime DateStart, DateTime DateEnd)
    {
      Boolean _is_tito_mode;
      DataSet _ds_TMHSMH;
      DataSet _ds_Final;

      _is_tito_mode = WSI.Common.TITO.Utils.IsTitoMode();

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (UpdateControlRegistersX2D(DateStart, HISTORIFICATION_TYPE.TERMINALS_SITE, _db_trx.SqlTransaction))
          {
            if (GetMeterData(DateStart, DateEnd, out _ds_TMHSMH, _db_trx.SqlTransaction))
            {
              if (GetMetersIds(_ds_TMHSMH, _db_trx.SqlTransaction, DateStart, out  _ds_Final))
              {
                if (InsertMetersTHMSMH(_ds_Final, _db_trx.SqlTransaction))
                {
                  _db_trx.Commit();

                  return true;
                }
              }
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        Log.Warning(String.Format(EXCEPTION_WCP_HISTORIFACION_TASK + "HistorificationTMHSMH" + EXCEPTION_MESSAGE + "{0} - {1} .", _ex.Message, WGDB.Now));
      }

      return false;
    } // HistorificationTMHSMH

    //------------------------------------------------------------------------------
    // PURPOSE : Calls to sp SmartFloor_TVH
    //  PARAMS :
    //      - INPUT :
    //            - DateTime DateStart
    //            - DateTime DateEnd
    //            - SqlTransaction Trx
    //      - OUTPUT :
    //
    // RETURNS :
    //      - DataSet
    //
    //   NOTES :
    static Boolean GetMeterData(DateTime DateStart, DateTime DateEnd, out DataSet DsMeters, SqlTransaction Trx)
    {

      Boolean _is_tito_mode;

      DsMeters = new DataSet();

      try
      {
        _is_tito_mode = WSI.Common.TITO.Utils.IsTitoMode();

        using (SqlCommand _sql_cmd = new SqlCommand(SP_NAME_GET_METERS, Trx.Connection, Trx))
        {
          _sql_cmd.CommandType = CommandType.StoredProcedure;

          _sql_cmd.Parameters.Add("@DateStart", SqlDbType.DateTime).Value = DateStart;
          _sql_cmd.Parameters.Add("@DateEnd", SqlDbType.DateTime).Value = DateEnd;
          _sql_cmd.Parameters.Add("@IsTitoMode", SqlDbType.Bit).Value = _is_tito_mode;

          using (SqlDataAdapter _da = new SqlDataAdapter(_sql_cmd))
          {
            _da.Fill(DsMeters);
            DsMeters.Tables[0].TableName = "Sites_Terminals";
            DsMeters.Tables[1].TableName = "Sites";

            return true;
          }
        }

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        Log.Warning(String.Format(EXCEPTION_WCP_HISTORIFACION_TASK + "GetMeterData" + EXCEPTION_MESSAGE + "{0} - {1} .", _ex.Message, WGDB.Now));
      }

      return false;
    } //GetMeterData

    //------------------------------------------------------------------------------
    // PURPOSE : Calls sp_Update_Data_Meters
    //  PARAMS :
    //      - INPUT :
    //            - DataTable DtX2D
    //            - SqlTransaction Trx
    //            
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean
    //
    //   NOTES :
    static Boolean InsertMetersTHMSMH(DataSet DsX2D, SqlTransaction Trx)
    {

      try
      {
        using (SqlDataAdapter _da_x2d = new SqlDataAdapter())
        {
          // UPDATE TERMINALS


          if (!UpdateTerminals(DsX2D.Tables["FinalData_Terminals"], Trx))
          {
            Log.Warning("Error in method UpdateTerminals");
            throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_OK, String.Format("Error in method UpdateTerminals"));
          }

          //UPDATE TERMINALS_SITES
          if (!UpdateSiteTerminals(DsX2D.Tables["FinalData_SitesTerminals"], Trx))
          {
            Log.Warning("Error in method UpdateSitesTerminals");
            throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_OK, String.Format("Error in method UpdateSitesTerminals"));
          }

          //UPDATE SITES
          if (!UpdateSites(DsX2D.Tables["FinalData_Sites"], Trx))
          {
            Log.Warning("Error in method UpdateSites");
            throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_OK, String.Format("Error in method UpdateSites"));
          }
          DsX2D = null;

          return true;

        }
      }
      catch (Exception _ex)
      {

        Log.Exception(_ex);
        Log.Warning(String.Format(EXCEPTION_WCP_HISTORIFACION_TASK + "InsertMetersTHMSMH" + EXCEPTION_MESSAGE + "{0} - {1} .", _ex.Message, WGDB.Now));
      }

      return false;

    } //InsertMetersTHMSMH

    //------------------------------------------------------------------------------
    // PURPOSE : Get Char with entity type
    //  PARAMS :
    //      - INPUT :
    //            - Enum_Entity_Id
    //            
    //      - OUTPUT :
    //
    // RETURNS :
    //      -String
    //
    //   NOTES :
    static String GetEntityId(ENUM_ENTITY_ID EntityID)
    {
      String _str_return;

      _str_return = "";

      try
      {
        switch (EntityID)
        {
          case ENUM_ENTITY_ID.Sites:
            _str_return = "S";
            break;

          case ENUM_ENTITY_ID.Terminals:
            _str_return = "T";
            break;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        Log.Warning(String.Format(EXCEPTION_WCP_HISTORIFACION_TASK + "GetEntityId" + EXCEPTION_MESSAGE + "{0} - {1} .", _ex.Message, WGDB.Now));
      }

      return _str_return;
    } //GetEntityId
    //------------------------------------------------------------------------------
    // PURPOSE : Get Char with table type
    //  PARAMS :
    //      - INPUT :
    //            - Enum_Table_ID
    //            
    //      - OUTPUT :
    //
    // RETURNS :
    //      -String
    //
    //   NOTES :
    static String GetTableId(ENUM_TABLE_ID TableID)
    {
      String _str_return;

      _str_return = "";
      try
      {
        switch (TableID)
        {
          case ENUM_TABLE_ID.Daily:
            _str_return = "T";
            break;
          case ENUM_TABLE_ID.Weekly:
            _str_return = "W";
            break;
          case ENUM_TABLE_ID.Monthly:
            _str_return = "M";
            break;
          case ENUM_TABLE_ID.Yearly:
            _str_return = "Y";
            break;

        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        Log.Warning(String.Format(EXCEPTION_WCP_HISTORIFACION_TASK + "GetTableId" + EXCEPTION_MESSAGE + "{0} - {1} .", _ex.Message, WGDB.Now));
      }
      return _str_return;
    } //GetTableId

    //------------------------------------------------------------------------------
    // PURPOSE : Calls to table Meters_Definition
    //  PARAMS :
    //      - INPUT :
    //            - DataSet DataTVH
    //            - SqlTransaction Trx
    //            - DateTime DateStart
    //            
    //      - OUTPUT :
    //            - DataSet DataFinal
    // RETURNS :
    //      - Boolean
    //
    //   NOTES :
    static Boolean GetMetersIds(DataSet DataTVH, SqlTransaction Trx, DateTime DateStart, out DataSet DataFinal)
    {

      StringBuilder _str_Sql;

      DataColumn _column;
      DataTable _dt_final_sites;
      DataTable _dt_final_sites_terminals;
      DataTable _dt_final_terminal;

      DataFinal = new DataSet();
      _str_Sql = new StringBuilder();

      _str_Sql.AppendLine("     SELECT * FROM H_METERS_DEFINITION   ");
      _str_Sql.AppendLine("      WHERE HMD_METER_NAME IS NOT NULL   ");
      _str_Sql.AppendLine("      ORDER BY   ");
      _str_Sql.AppendLine("          HMD_STORED_NAME      ");
      _str_Sql.AppendLine("         ,HMD_METER_TYPE      ");
      _str_Sql.AppendLine("         ,HMD_METER_ID      ");
      _str_Sql.AppendLine("         ,HMD_METER_ITEM      ");

      try
      {
        using (SqlDataAdapter _da_TVH = new SqlDataAdapter())
        {
          _da_TVH.SelectCommand = new SqlCommand(_str_Sql.ToString(), Trx.Connection, Trx);
          _da_TVH.Fill(DataTVH, "Meters_Definition");

          _dt_final_sites = SetFinalTable();
          _dt_final_sites_terminals = SetFinalTable();
          _dt_final_terminal = SetFinalTable();
          _dt_final_sites.TableName = "FinalData_Sites";
          _dt_final_sites_terminals.TableName = "FinalData_SitesTerminals";
          _dt_final_terminal.TableName = "FinalData_Terminals";

          // Fill DataTable Sites ----------------------------------------------------------------------------------------
          foreach (DataColumn _colum_TVH in DataTVH.Tables["Sites"].Columns)
          {
            _column = _colum_TVH;

            foreach (DataRow _row_meters in DataTVH.Tables["Meters_Definition"].Rows)
            {
              if (_column.ColumnName == (String)_row_meters["hmd_meter_name"])
              {

                foreach (DataRow _row_TVH in DataTVH.Tables["Sites"].Rows)
                {
                  _dt_final_sites.NewRow();
                  _dt_final_sites.Rows.Add(DateStart, m_site_identifier, _row_meters["hmd_meter_id"], _row_meters["hmd_meter_item"].ToString(), _row_TVH[_column.ColumnName], _row_meters["hmd_meter_type"]);
                }
              }
            }
          }
          //-------------------------------------------------------------------------------------------------

          // Fill DataTable Sites_Terminals --------------------------------------------------------------------------------

          foreach (DataColumn _colum_TVH in DataTVH.Tables["Sites_Terminals"].Columns)
          {
            _column = _colum_TVH;

            foreach (DataRow _row_meters in DataTVH.Tables["Meters_Definition"].Rows)
            {
              if (_column.ColumnName == (String)_row_meters["hmd_meter_name"] && (((Int32)_row_meters["hmd_meter_type"] & (Int32)FLAG_METER_TYPE.SITE) == (Int32)FLAG_METER_TYPE.SITE))
              {

                foreach (DataRow _row_TVH in DataTVH.Tables["Sites_Terminals"].Rows)
                {
                  _dt_final_sites_terminals.NewRow();
                  _dt_final_sites_terminals.Rows.Add(DateStart, m_site_identifier, _row_meters["hmd_meter_id"], _row_meters["hmd_meter_item"].ToString(), _row_TVH[_column.ColumnName], _row_meters["hmd_meter_type"]);
                }
              }
            }
          } //-------------------------------------------------------------------------------------------------------------------------

          // Fill DataTable Terminals --------------------------------------------------------------------------------------------------------
          foreach (DataColumn _colum_TVH in DataTVH.Tables["Sites_Terminals"].Columns)
          {
            _column = _colum_TVH;

            foreach (DataRow _row_meters in DataTVH.Tables["Meters_Definition"].Rows)
            {
              if (_column.ColumnName == (String)_row_meters["hmd_meter_name"] && (((Int32)_row_meters["hmd_meter_type"] & (Int32)FLAG_METER_TYPE.TERMINAL) == (Int32)FLAG_METER_TYPE.TERMINAL))
              {

                foreach (DataRow _row_TVH in DataTVH.Tables["Sites_Terminals"].Rows)
                {
                  _dt_final_terminal.NewRow();
                  _dt_final_terminal.Rows.Add(DateStart, _row_TVH["terminal_id"], _row_meters["hmd_meter_id"], _row_meters["hmd_meter_item"], _row_TVH[_column.ColumnName], _row_meters["hmd_meter_type"]);
                }
              }
            }
          } //----------------------------------------------------------------------------------------
        }
        //Add DataTables to DataSet OUTPUT
        DataFinal.Tables.Add(_dt_final_sites);
        DataFinal.Tables.Add(_dt_final_sites_terminals);
        DataFinal.Tables.Add(_dt_final_terminal);

        return true;
      }

      catch (Exception _ex)
      {

        Log.Exception(_ex);
        Log.Warning(String.Format(EXCEPTION_WCP_HISTORIFACION_TASK + "GetMetersId" + EXCEPTION_MESSAGE + "{0} - {1} .", _ex.Message, WGDB.Now));
      }

      return false;
    }//GetMetersIds

    //------------------------------------------------------------------------------
    // PURPOSE : Sets the format and headers of DataTable
    //
    //
    // RETURNS :
    //      - Datatable
    //
    //   NOTES :
    static DataTable SetFinalTable()
    {
      DataTable _dt_meters;

      _dt_meters = new DataTable();

      try
      {

        //Add columns

        _dt_meters.Columns.Add("date", typeof(DateTime));
        _dt_meters.Columns.Add("id", typeof(Int32));
        _dt_meters.Columns.Add("meter_id", typeof(Int32));
        _dt_meters.Columns.Add("meter_item", typeof(Int32));
        _dt_meters.Columns.Add("meter_value", typeof(Decimal));
        _dt_meters.Columns.Add("meter_type", typeof(Int32));
        return _dt_meters;
      }

      catch (Exception _ex)
      {
        Log.Exception(_ex);
        Log.Warning(String.Format(EXCEPTION_WCP_HISTORIFACION_TASK + "SetFinalTable" + EXCEPTION_MESSAGE + "{0} - {1} .", _ex.Message, WGDB.Now));
      }

      return null;
    }//SetFinalTable

    //------------------------------------------------------------------------------
    // PURPOSE : Update Terminals Meters
    //  PARAMS :
    //      - INPUT :
    //            - DataTable
    //            - SqlTransaction
    //      - OUTPUT :
    //
    // RETURNS :
    //           - Boolean
    //
    //   NOTES :

    static Boolean UpdateTerminals(DataTable DtTerminals, SqlTransaction Trx)
    {
      try
      {
        using (SqlDataAdapter _da_x2d = new SqlDataAdapter())
        {
          using (SqlCommand _sql_cmd_Sites = new SqlCommand(SP_NAME_UPDATE, Trx.Connection, Trx))
          {
            _sql_cmd_Sites.CommandType = CommandType.StoredProcedure;
            _sql_cmd_Sites.CommandTimeout = 5 * 60 * 1000;

            _sql_cmd_Sites.Parameters.Add("@pTableId", SqlDbType.Char).Value = GetTableId(ENUM_TABLE_ID.Daily);
            _sql_cmd_Sites.Parameters.Add("@pEntityId", SqlDbType.Char).Value = GetEntityId(ENUM_ENTITY_ID.Terminals);
            _sql_cmd_Sites.Parameters.Add("@pDate", SqlDbType.DateTime, 1, "date");
            _sql_cmd_Sites.Parameters.Add("@pId", SqlDbType.Int, 1, "id");
            _sql_cmd_Sites.Parameters.Add("@pMeterId", SqlDbType.Int, 1, "meter_id");
            _sql_cmd_Sites.Parameters.Add("@pMeterItem", SqlDbType.Int, 1, "meter_item");
            _sql_cmd_Sites.Parameters.Add("@pValue", SqlDbType.Decimal, 1, "meter_value");

            _da_x2d.InsertCommand = _sql_cmd_Sites;
            _da_x2d.InsertCommand.UpdatedRowSource = UpdateRowSource.None;
            _da_x2d.UpdateBatchSize = 500;

            if (_da_x2d.Update(DtTerminals) >= 0)
            {
              return true;
            }
          }
        }
      }
      catch (Exception _ex)
      {

        Log.Exception(_ex);
        Log.Warning(String.Format(EXCEPTION_WCP_HISTORIFACION_TASK + "UpdateTerminals" + EXCEPTION_MESSAGE + "{0} - {1} .", _ex.Message, WGDB.Now));
      }
      return false;
    } //UpdateTerminals

    //------------------------------------------------------------------------------
    // PURPOSE : Update SitesTerminals Meters
    //  PARAMS :
    //      - INPUT :
    //            - DataTable
    //            - SqlTransaction
    //      - OUTPUT :
    //
    // RETURNS :
    //           - Boolean
    //
    //   NOTES :

    static Boolean UpdateSiteTerminals(DataTable DtTerminalsSites, SqlTransaction Trx)
    {

      try
      {
        using (SqlDataAdapter _da_x2d = new SqlDataAdapter())
        {
          using (SqlCommand _sql_cmd_Sites = new SqlCommand(SP_NAME_UPDATE, Trx.Connection, Trx))
          {
            _sql_cmd_Sites.CommandType = CommandType.StoredProcedure;
            _sql_cmd_Sites.CommandTimeout = 5 * 60 * 1000;

            _sql_cmd_Sites.Parameters.Add("@pTableId", SqlDbType.Char).Value = GetTableId(ENUM_TABLE_ID.Daily);
            _sql_cmd_Sites.Parameters.Add("@pEntityId", SqlDbType.Char).Value = GetEntityId(ENUM_ENTITY_ID.Sites);
            _sql_cmd_Sites.Parameters.Add("@pDate", SqlDbType.DateTime, 1, "date");
            _sql_cmd_Sites.Parameters.Add("@pId", SqlDbType.Int, 1, "id");
            _sql_cmd_Sites.Parameters.Add("@pMeterId", SqlDbType.Int, 1, "meter_id");
            _sql_cmd_Sites.Parameters.Add("@pMeterItem", SqlDbType.Int, 1, "meter_item");
            _sql_cmd_Sites.Parameters.Add("@pValue", SqlDbType.Decimal, 1, "meter_value");

            _da_x2d.InsertCommand = _sql_cmd_Sites;
            _da_x2d.InsertCommand.UpdatedRowSource = UpdateRowSource.None;
            _da_x2d.UpdateBatchSize = 500;

            if (_da_x2d.Update(DtTerminalsSites) >= 0)
            {
              return true;
            }
          }

        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        Log.Warning(String.Format(EXCEPTION_WCP_HISTORIFACION_TASK + "UpdateSiteTerminals" + EXCEPTION_MESSAGE + "{0} - {1} .", _ex.Message, WGDB.Now));
      }
      return false;
    } //UpdateSiteTerminals

    //------------------------------------------------------------------------------
    // PURPOSE : Update SitesTerminals Meters
    //  PARAMS :
    //      - INPUT :
    //            - DataTable
    //            - SqlTransaction
    //      - OUTPUT :
    //
    // RETURNS :
    //           - Boolean
    //
    //   NOTES :

    static Boolean UpdateSites(DataTable DtSites, SqlTransaction Trx)
    {

      try
      {
        using (SqlDataAdapter _da_x2d = new SqlDataAdapter())
        {
          using (SqlCommand _sql_cmd_Sites = new SqlCommand(SP_NAME_UPDATE, Trx.Connection, Trx))
          {
            _sql_cmd_Sites.CommandType = CommandType.StoredProcedure;
            _sql_cmd_Sites.CommandTimeout = 5 * 60 * 1000;

            _sql_cmd_Sites.Parameters.Add("@pTableId", SqlDbType.Char).Value = GetTableId(ENUM_TABLE_ID.Daily);
            _sql_cmd_Sites.Parameters.Add("@pEntityId", SqlDbType.Char).Value = GetEntityId(ENUM_ENTITY_ID.Sites);
            _sql_cmd_Sites.Parameters.Add("@pDate", SqlDbType.DateTime, 1, "date");
            _sql_cmd_Sites.Parameters.Add("@pId", SqlDbType.Int, 1, "id");
            _sql_cmd_Sites.Parameters.Add("@pMeterId", SqlDbType.Int, 1, "meter_id");
            _sql_cmd_Sites.Parameters.Add("@pMeterItem", SqlDbType.Int, 1, "meter_item");
            _sql_cmd_Sites.Parameters.Add("@pValue", SqlDbType.Decimal, 1, "meter_value");

            _da_x2d.InsertCommand = _sql_cmd_Sites;
            _da_x2d.InsertCommand.UpdatedRowSource = UpdateRowSource.None;
            _da_x2d.UpdateBatchSize = 500;

            if (_da_x2d.Update(DtSites) >= 0)
            {
              return true;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        Log.Warning(String.Format(EXCEPTION_WCP_HISTORIFACION_TASK + "UpdateSites" + EXCEPTION_MESSAGE + "{0} - {1} .", _ex.Message, WGDB.Now));

      }
      return false;
    } //UpdateSites

    #endregion TMH - SMH


  }  // WCP_HistorificationTasks

} // WSI.WCP
