//------------------------------------------------------------------------------
// Copyright � 2013 Win Systems Ltd.
//------------------------------------------------------------------------------
//
//   MODULE NAME : WCP_Users.cs
//
//   DESCRIPTION : WCP_Users class
//
// REVISION HISTORY :
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 05-FEB-2013 JCM    First release.
//------------------------------------------------------------------------------


using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using WSI.Common;
using System.Data;

namespace WSI.WCP
{
  public static class WCP_Users
  {
    // Thread will execute ever 5-7 minutes
    private const Int32 WAIT_FOR_NEXT_TRY = (5 * 60 * 1000);    // 5 minutes in milisseconds

    private static DateTime m_last_running_user_accounts_block;

    //private static Int32 m_rand_minutes_shift;
    private static Thread m_thread;

    //------------------------------------------------------------------------------
    // PURPOSE : Initialize WCP_Users class 
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :

    public static void Init()
    {
      m_thread = new Thread(UsersThread);
      m_thread.Name = "UsersThread";

      m_thread.Start();
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Thread for Users background tasks
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private static void UsersThread()
    {
      Int32 _tick0;
      Int64 _ellapsed;
      Random _rnd;

      _rnd = new Random(Environment.TickCount);

      // Generate next random execution
      //m_rand_minutes_shift = _rnd.Next(WAIT_FOR_NEXT_TRY_MAX_RANDOM);  // User Account Block

      // Inititalize last running Date
      m_last_running_user_accounts_block = WGDB.Now.Date.AddDays(-1);  // User Account Block

      while (true)
      {

        // Run each 5 minutes
        Thread.Sleep(WAIT_FOR_NEXT_TRY);

        // AJQ 19-AUG-2013, Only the service running as 'Principal' will excute the tasks.
        if (!Services.IsPrincipal("WCP"))
        {
          continue;
        }
        
        try
        {
          _tick0 = Environment.TickCount;

          //  User Account Lock by Innactivity
          if (!Job_BlockInactiveUsers())
          {
            Log.Warning("WCP_Users.UsersThread. Exception in function: " + "Job_BlockInactiveUsers");
          }
          else
          {
            _ellapsed = Misc.GetElapsedTicks(_tick0);
            if (_ellapsed > 10000)
            {
              Log.Warning("UsersThread.Step[0], Duration: " + _ellapsed.ToString() + " ms.");
            }
          }

        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }
      } // while(true)
    }   // UsersThread


    // PURPOSE: Blocked User by Inactivity
    //          Thread if enabled will execute once a time by day trip
    //                 else will try again
    //  PARAMS:
    //     - INPUT:
    //     - OUTPUT:
    //
    // RETURNS:
    //     - Bool:  True  executed succesfully
    //              False executed unsuccesfully
    private static bool Job_BlockInactiveUsers()
    {
      Int32 _closing_time;
      DateTime _today;
      DateTime _run_date_time;

      DataTable _blocked_users;

      Int32 _inactivity_days;

      _inactivity_days = 0;

      // Get Closing Time
      _closing_time = GeneralParam.GetInt32("WigosGUI", "ClosingTime", 0);

      _today = WGDB.Now.Date;
      _today = _today.AddHours((double)_closing_time);

      // Add extra wait to not be exactly at closing time.
      _run_date_time = _today.AddMinutes(3);

      // Run after closing date
      if (WGDB.Now <= _run_date_time)
      {
        return true;
      }

      // Today already run
      if (m_last_running_user_accounts_block == _today)
      {
        return true;
      }

      try
      {
        Users.ReadMaxDaysWithoutLogin(out _inactivity_days);

        // Block Users Disabled
        if (_inactivity_days == 0)
        {
          return true;
        }

        // Block Users by Inactivity
        if (!Users.BlockInactiveUsers(_inactivity_days, out _blocked_users))
        {
          return false;
        }

        // Audit blocked Inactivity Users
        if (!Users.AuditBlockedInactiveUsers(_inactivity_days, _blocked_users))
        {
          return false;
        }

        m_last_running_user_accounts_block = _today;

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }

    } // Job_BlockInactiveUsers
  }
}
