﻿//------------------------------------------------------------------------------
// Copyright © 2011 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: MeterTSMHGroupList.cs
// 
//   DESCRIPTION: MeterTSMHGroupList class
// 
//        AUTHOR: Francis Gretz
// 
// CREATION DATE: 26-JAN-2018
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 26-JAN-2018 FGB    Initial version
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using WSI.Common;

namespace WSI.WCP.MeterTSMHGroup
{
  public class MeterTSMHGroupList
  {
    /// <summary>
    /// Get list of meters for group
    /// </summary>
    /// <param name="OldValues"></param>
    /// <param name="NewValues"></param>
    /// <returns></returns>
    public List<KeyValuePair<Int32, Boolean>> GetListMetersForGroup(MachineMeters OldValues, MachineMeters NewValues)
    {
      Boolean IsAftEnabled = (NewValues.AftFundTransferAvalaible && OldValues.AftFundTransferAvalaible);
      Boolean IsEftEnabled = (NewValues.EftFundTransferAvalaible && OldValues.EftFundTransferAvalaible);

      List<KeyValuePair<Int32, Boolean>> _list_meters_for_group;
      _list_meters_for_group = new List<KeyValuePair<Int32, Boolean>>();

      // Games played
      _list_meters_for_group.Add(new KeyValuePair<Int32, Boolean>(WCP_BU_MachineMeters.METER_CODE_GAMES_PLAYED, false));

      // Games won
      _list_meters_for_group.Add(new KeyValuePair<Int32, Boolean>(WCP_BU_MachineMeters.METER_CODE_GAMES_WON, false));

      // Total coin in credits
      _list_meters_for_group.Add(new KeyValuePair<Int32, Boolean>(WCP_BU_MachineMeters.METER_CODE_TOTAL_COIN_IN_CREDITS, false));

      // Total coin out credits
      _list_meters_for_group.Add(new KeyValuePair<Int32, Boolean>(WCP_BU_MachineMeters.METER_CODE_TOTAL_COIN_OUT_CREDITS, false));

      // Total jackpot credits
      _list_meters_for_group.Add(new KeyValuePair<Int32, Boolean>(WCP_BU_MachineMeters.METER_CODE_TOTAL_JACKPOT_CREDITS, false));

      // Total progressive jackpot credits
      _list_meters_for_group.Add(new KeyValuePair<Int32, Boolean>(WCP_BU_MachineMeters.METER_CODE_TOTAL_PROGRESSIVE_JACKPOT_CREDITS, false));

      //Is AFT enabled??
      if (IsAftEnabled)
      {
        // In-house transfers to gaming machine that included cashable amounts (quantity)
        _list_meters_for_group.Add(new KeyValuePair<Int32, Boolean>(WCP_BU_MachineMeters.METER_CODE_INHOUSE_TRANSFERS_TO_GAMING_MACHINE_QUANTITY, true));

        // In-house transfers to host that included cashable amounts (quantity)
        _list_meters_for_group.Add(new KeyValuePair<Int32, Boolean>(WCP_BU_MachineMeters.METER_CODE_INHOUSE_TRANSFERS_TO_HOST_QUANTITY, true));

        // In-house cashable transfers to gaming machine (cents)
        _list_meters_for_group.Add(new KeyValuePair<Int32, Boolean>(WCP_BU_MachineMeters.METER_CODE_INHOUSE_TRANSFERS_TO_GAMING_MACHINE_CENTS, true));

        // In-house cashable transfers to host (cents)
        _list_meters_for_group.Add(new KeyValuePair<Int32, Boolean>(WCP_BU_MachineMeters.METER_CODE_INHOUSE_TRANSFERS_TO_HOST_CENTS, true));
      }

      //Is EFT enabled??
      if (IsEftEnabled)
      {
        // In-house transfers to gaming machine that included cashable amounts (quantity)
        _list_meters_for_group.Add(new KeyValuePair<Int32, Boolean>(WCP_BU_MachineMeters.METER_CODE_INHOUSE_TRANSFERS_TO_GAMING_MACHINE_QUANTITY, false));

        // In-house transfers to host that included cashable amounts (quantity)
        _list_meters_for_group.Add(new KeyValuePair<Int32, Boolean>(WCP_BU_MachineMeters.METER_CODE_INHOUSE_TRANSFERS_TO_HOST_QUANTITY, false));

        // In-house cashable transfers to gaming machine (cents)
        _list_meters_for_group.Add(new KeyValuePair<Int32, Boolean>(WCP_BU_MachineMeters.METER_CODE_INHOUSE_TRANSFERS_TO_GAMING_MACHINE_CENTS, false));

        // In-house cashable transfers to host (cents)
        _list_meters_for_group.Add(new KeyValuePair<Int32, Boolean>(WCP_BU_MachineMeters.METER_CODE_INHOUSE_TRANSFERS_TO_HOST_CENTS, false));
      }

      return _list_meters_for_group;
    }
  }
}
