using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Data.SqlClient;
using WSI.Common;
using System.Data;
using System.Timers;
using System.Collections;

namespace WSI.WCP
{
  internal enum CachedTable
  {
    site = 0,
    terminals = 1,
    wcp_services = 2,
  }
  
  public static class WCP_Games
  {
    private static Boolean          m_init     = false;
    private static ReaderWriterLock m_rw_games = new ReaderWriterLock(); 
    private static Hashtable        m_ht_games = new Hashtable(3001); // Initial Capacity = Prime Number;
    private static ReaderWriterLock m_rw_terga = new ReaderWriterLock();
    private static Hashtable        m_ht_terga = new Hashtable(100003); // Initial Capacity = Prime Number;
   
    public static void Init ()
    {
      if ( m_init )
      {
        return;
      }
      
      LoadGames ();
      LoadRelations ();
      
      m_init = true;
    }
    
    public static Int32 GameID (String GameName)
    {
      SqlConnection _sql_conn;
      SqlCommand _sql_cmd;
      Int32  _game_id;
      String _game_name;
      Object _obj;

      if (GameName.Length > 50)
      {
        _game_name = GameName.Substring(0, 50);
      }
      else
      {
        _game_name = GameName;
      }
      
      try 
      {
        m_rw_games.AcquireReaderLock (Timeout.Infinite);
        if (m_ht_games.Contains(_game_name))
        {
          return (Int32)m_ht_games[_game_name];
        }
      }
      finally 
      {
        m_rw_games.ReleaseReaderLock ();
      }
      
      // Game Not Found

      _game_id = 0;
      _sql_conn = null;
      
      try 
      {
        _sql_conn = WGDB.Reconnect(null);

        _sql_cmd = new SqlCommand("INSERT INTO GAMES (GM_NAME) VALUES (@pGameName) SET @pGameId = SCOPE_IDENTITY()");
        _sql_cmd.Parameters.Add("@pGameName", SqlDbType.NVarChar).Value = _game_name;
        _sql_cmd.Parameters.Add("@pGameId", SqlDbType.Int).Direction = ParameterDirection.Output;
        _sql_cmd.Connection = _sql_conn;
        
        if (_sql_cmd.ExecuteNonQuery() == 1)
        {
          _game_id = (Int32)_sql_cmd.Parameters[1].Value;
          
          try 
          {
            m_rw_games.AcquireWriterLock(Timeout.Infinite);
            if (!m_ht_games.Contains(_game_name))
            {
              m_ht_games.Add(_game_name, _game_id);
            }
          }
          finally 
          {
            m_rw_games.ReleaseWriterLock ();
          }

          return _game_id;
        }
      }
      catch (SqlException _sql_ex)
      {
        if (_sql_ex.Number == 2601)
        {
          _sql_cmd = new SqlCommand("SELECT GM_GAME_ID FROM GAMES WHERE GM_NAME = @pGameName");
          _sql_cmd.Parameters.Add("@pGameName", SqlDbType.NVarChar).Value = _game_name;
          _sql_cmd.Connection = _sql_conn;
  
          try
          {
            _obj = _sql_cmd.ExecuteScalar();
            if (_obj != null)
            {
              _game_id = (Int32)_obj;

              try
              {
                m_rw_games.AcquireWriterLock(Timeout.Infinite);
                if (!m_ht_games.Contains(_game_name))
                {
                  m_ht_games.Add(_game_name, _game_id);
                }
              }
              finally
              {
                m_rw_games.ReleaseWriterLock();
              }

              return _game_id;
            }
          }
          catch (Exception _ex)
          {
            Log.Exception (_ex);
          }
        }
        else
        {
          Log.Exception (_sql_ex);
        }
      }
      
      Log.Error ("Unable to retrieve the GameID for game: " + _game_name );
      System.Threading.Thread.Sleep(1000);
      
      return 0;
    }
    
    private static void LoadGames ()
    {
      SqlConnection _sql_conn;
      SqlCommand _sql_cmd;
      SqlDataReader _sql_dr;
      String _game_name;
      Int32  _game_id;

      m_ht_games.Clear();

      _sql_cmd  = new SqlCommand("SELECT GM_GAME_ID, GM_NAME FROM GAMES");
      _sql_conn = null;
      
      try 
      {
        while (true)
        {
          try
          {
            _sql_conn = WGDB.Reconnect (_sql_conn);
            _sql_cmd.Connection = _sql_conn;
            _sql_dr = _sql_cmd.ExecuteReader();

            while (_sql_dr.Read())
            {
              _game_id = _sql_dr.GetInt32(0);
              _game_name = _sql_dr.GetString(1);
              m_ht_games.Add(_game_name, _game_id);
            }

            _sql_dr.Close();
            _sql_dr.Dispose();
            
            Log.Message (" Games Loaded: " + m_ht_games.Count + " games.");

            return;
          }
          catch (Exception _ex)
          {
            Log.Exception (_ex);
            
            System.Threading.Thread.Sleep(1000);
          }
        }      
      }
      finally 
      {
        if ( _sql_conn != null )
        {
          try { _sql_conn.Close (); } catch { ; }
          _sql_conn.Dispose ();
          _sql_conn = null;
        }
      }
    }

    private static Int64 GetTerminalGameKey(Int32 TerminalId, Int32 GameId)
    {
      Int64 _key;
      
      _key = TerminalId;
      _key <<= 32;
      _key += GameId;
      
      return _key;
    }

    private static void LoadRelations()
    {
      SqlConnection _sql_conn;
      SqlCommand _sql_cmd;
      SqlDataReader _sql_dr;
      Int32 _game_id;
      Int32 _terminal_id;
      

      m_ht_terga.Clear();

      _sql_cmd  = new SqlCommand("SELECT TGT_TERMINAL_ID, TGT_SOURCE_GAME_ID FROM TERMINAL_GAME_TRANSLATION");
      _sql_conn = null;

      try
      {
        while (true)
        {
          try
          {
            _sql_conn = WGDB.Reconnect(_sql_conn);
            _sql_cmd.Connection = _sql_conn;
            _sql_dr = _sql_cmd.ExecuteReader();

            while (_sql_dr.Read())
            {
              _terminal_id = _sql_dr.GetInt32(0);
              _game_id = _sql_dr.GetInt32(1);
              m_ht_terga.Add(GetTerminalGameKey (_terminal_id, _game_id), null);
            }

            _sql_dr.Close();
            _sql_dr.Dispose();

            Log.Message(" Terminal-Game translation loaded: " + m_ht_terga.Count + " Terminal-Games.");

            return;
          }
          catch (Exception _ex)
          {
            Log.Exception(_ex);

            System.Threading.Thread.Sleep(1000);
          }
        }
      }
      finally
      {
        if (_sql_conn != null)
        {
          try { _sql_conn.Close(); }
          catch { ; }
          _sql_conn.Dispose();
          _sql_conn = null;
        }
      }
    }


    public static Boolean RegisterTerminalGame(Int32 TerminalId, String GameName)
    {
      StringBuilder _sql_str; 
      SqlConnection _sql_conn;
      SqlCommand _sql_cmd;
      Int64 _key;
      Int32 _game_id;

      _game_id = GameID(GameName);
      if ( _game_id == 0 )
      {
        return false;
      }

      _key = GetTerminalGameKey(TerminalId, _game_id);
      
      try
      {
        m_rw_terga.AcquireReaderLock (Timeout.Infinite);
        if (m_ht_terga.Contains(_key))
        {
          return true;
        }
      }
      finally 
      {
        m_rw_terga.ReleaseReaderLock ();
      }

      // Relation Not Found

      _sql_conn = null;


      try
      {
        _sql_conn = WGDB.Reconnect(null);
        
        // Insert Terminal/OldGame/NewGame translation (Old=New)
        
        _sql_str = new StringBuilder("");
        _sql_str.AppendLine("INSERT INTO   TERMINAL_GAME_TRANSLATION ");
        _sql_str.AppendLine("            ( TGT_TERMINAL_ID");
        _sql_str.AppendLine("            , TGT_SOURCE_GAME_ID");
        _sql_str.AppendLine("            , TGT_TARGET_GAME_ID");
        _sql_str.AppendLine("            )");
        _sql_str.AppendLine("     VALUES ( @pTerminalID");
        _sql_str.AppendLine("            , @pGameID");
        _sql_str.AppendLine("            , @pGameID");
        _sql_str.AppendLine("            )");

        _sql_cmd = new SqlCommand(_sql_str.ToString());
        _sql_cmd.Parameters.Add("@pTerminalID", SqlDbType.Int).Value = TerminalId;
        _sql_cmd.Parameters.Add("@pGameID", SqlDbType.Int).Value = _game_id;

        _sql_cmd.Connection = _sql_conn;

        try
        {
          if (_sql_cmd.ExecuteNonQuery() == 1)
          {
            // Terminal/Game translation successfully inserted
            try 
            {
              m_rw_terga.AcquireWriterLock(Timeout.Infinite);
              if (!m_ht_terga.Contains(_key))
              {
                m_ht_terga.Add(_key, null);
              }
            }
            finally 
            {
              m_rw_terga.ReleaseWriterLock ();
            }
            return true;
          }
        }
        catch (SqlException _sql_ex)
        {
          if (_sql_ex.Number == 2627) // PK
          {
            // The relation already exists on database
            try
            {
              m_rw_terga.AcquireWriterLock(Timeout.Infinite);
              if (!m_ht_terga.Contains(_key))
              {
                m_ht_terga.Add(_key, null);
              }
            }
            finally
            {
              m_rw_terga.ReleaseWriterLock();
            }
            return true;
          }
          else
          {
            Log.Exception(_sql_ex);
          }
        }
        finally
        {
          if (_sql_conn != null)
          {
            try { _sql_conn.Close (); } catch { ; }
            _sql_conn.Dispose ();
            _sql_conn = null;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception (_ex);
      }

      Log.Error("Unable to register Terminal-Game: " + TerminalId + "-" + GameName);
      System.Threading.Thread.Sleep(1000);

      return false;
    }

    
    
  
  }

  public static class DbCache
  {
    static Thread thread_cache;

    static internal System.Threading.AutoResetEvent[] event_reload;
    static internal System.Threading.ManualResetEvent[] event_reloaded;
    static internal DateTime [] when_reloaded;
    
    static SqlDataAdapter da;
    static DataSet ds;
    static ReaderWriterLock dbcache_lock;

    /// <summary>
    /// Initialize class attributes.
    /// </summary>
    public static void Init()
    {
      WCP_Games.Init ();
      
      
      // Cache
      thread_cache = new Thread(DbCacheThread);
      thread_cache.Name = "DbCache";

      event_reload = new AutoResetEvent[3];
      event_reloaded = new ManualResetEvent[event_reload.Length];
      when_reloaded = new DateTime[event_reload.Length];

      // Init events array
      for (int i = 0; i < event_reload.Length; i++)
      {
        event_reload[i] = new AutoResetEvent(true);
        event_reloaded[i] = new ManualResetEvent(false);
        when_reloaded[i] = DateTime.Now;
      }

      // Init database connection
      ds = new DataSet();
      da = new SqlDataAdapter();

      // Init ReaderWriterLock
      dbcache_lock = new ReaderWriterLock();
    }

    /// <summary>
    /// Start DbCache thread.
    /// </summary>
    public static void Start()
    {
      thread_cache.Start();
    }

    /// <summary>
    /// Set events to reload data.
    /// </summary>
    /// <param name="Table"></param>
    private static void ManageEvents(CachedTable Table)
    {
      DbCache.event_reloaded[(int)Table].Reset();
      DbCache.event_reload[(int)Table].Set();
      DbCache.event_reloaded[(int)Table].WaitOne();
    }

    /// <summary>
    /// Apply changes on database for a specific table.
    /// </summary>
    /// <param name="Table"></param>
    private static void Update(DataTable Table)
    {
      DataTable table_aux;

      lock (Table)
      {
        table_aux = Table.GetChanges();
      }

      if (table_aux != null)
      {
        if (table_aux.Rows.Count > 0)
        {
          da.InsertCommand.Connection = WGDB.Connection();
          da.InsertCommand.Transaction = da.InsertCommand.Connection.BeginTransaction();
          da.Update(table_aux);
          da.InsertCommand.Transaction.Commit();
        }
      }
    }

    /// <summary>
    /// RegisterTerminalGame
    /// </summary>
    /// <param name="GameId"></param>
    /// <returns></returns>
    public static void RegisterTerminalGame(Int32 TerminalId, String ReportedGameName, SqlTransaction Trx)
    {
      WCP_Games.RegisterTerminalGame (TerminalId, ReportedGameName);
    }

    /// <summary>
    /// Set event to reload Terminals table.
    /// </summary>
    /// <param name="TerminalId"></param>
    public static void ReloadTerminal(Int32 TerminalId)
    {
      DbCache.event_reload[(int)CachedTable.terminals].Set();      
    }

    /// <summary>
    /// Set event to reload Wcp_Services table.
    /// </summary>
    public static void ReloadWcpServices()
    {
      DbCache.event_reload[(int)CachedTable.wcp_services].Set();
    }

    /// <summary>
    /// Set event to reload Site table.
    /// </summary>
    public static void ReloadSite()
    {
      DbCache.event_reload[(int)CachedTable.site].Set();
    }

    /// <summary>
    /// Returns terminal data for a specific terminal Id.
    /// </summary>
    /// <param name="TerminalId"></param>
    /// <returns></returns>
    public static DataRow Terminal(Int32 TerminalId) 
    {
      DataTable table;
      DataRow[] terminals;

      for (int i = 0; i < 2; i++)
      {
        GetTableFromDataset("TERMINALS", out table);
        terminals = table.Select("TE_TERMINAL_ID = '" + TerminalId + "'");
        if (terminals.Length > 0)
        {
          DataRow r;

          r = table.NewRow();
          r.ItemArray = (Object[])terminals[0].ItemArray.Clone();
          return r;
        }

        if (i == 0)
        {
          ManageEvents(CachedTable.terminals);
        }
      } 
      return null;
    }

    /// <summary>
    /// Return terminal data for a specific external terminal id.
    /// </summary>
    /// <param name="ExternalId"></param>
    /// <returns></returns>
    public static DataRow Terminal(String ExternalId)
    {
      DataTable table;
      DataRow[] terminals;

      GetTableFromDataset("TERMINALS", out table);
      terminals = table.Select("TE_EXTERNAL_ID = '" + ExternalId + "'");
      if (terminals.Length > 0)
      {
        DataRow r;

        r = table.NewRow();
        r.ItemArray = (Object[])terminals[0].ItemArray.Clone();
        return r;
      }

      return null;
    }

    public static Boolean IsTerminalLocked(String ExternalId)
    {
      DataRow dr;
      Int32 ServerId;

      try
      {
        dr = DbCache.Terminal(ExternalId);
        if (dr == null)
        {
          return true;
        }
        if (((Boolean)dr["TE_BLOCKED"]))
        {
          return true;
        }
        if ((TerminalStatus)dr["TE_STATUS"] != TerminalStatus.ACTIVE)
        {
          return true;
        }

        if (((Int32)dr["TE_TYPE"]) == 0)
        {
          return false;
        }

        if (dr.IsNull("TE_SERVER_ID"))
        {
          return false;
        }

        ServerId = (Int32)dr["TE_SERVER_ID"];

        dr = DbCache.Terminal(ServerId);
        if (dr == null)
        {
          return true;
        }
        if (((Boolean)dr["TE_BLOCKED"]))
        {
          return true;
        }
        if ((TerminalStatus)dr["TE_STATUS"] != TerminalStatus.ACTIVE)
        {
          return true;
        }

        return false;
      }
      catch
      {
        Log.Warning("IsTerminalLocked: Terminal external Id: " + ExternalId);
      }

      return false;
    }

    /// <summary>
    /// Get Wcp Service
    /// </summary>
    /// <param name="WcpServiceId"></param>
    /// <returns></returns>
    public static DataRow WcpService(Int64 WcpServiceId)
    {
      DataTable table;
      DataRow[] wcp_services;

      for (int i = 0; i < 2; i++)
      {
        GetTableFromDataset("WCP_SERVICES", out table);
        wcp_services = table.Select("WSVR_SERVICE_ID = '" + WcpServiceId + "'");
        if (wcp_services.Length > 0)
        {
          DataRow r;

          r = table.NewRow();
          r.ItemArray = (Object[])wcp_services[0].ItemArray.Clone();

          return r;
        }

        if (i == 0)
        {
          ManageEvents(CachedTable.wcp_services);
        }
      }

      return null;
    } // WcpService

    /// <summary>
    /// Get wcp service id
    /// </summary>
    /// <param name="WcpServiceName"></param>
    /// <param name="WcpServiceId"></param>
    /// <returns></returns>
    private static Boolean GetWcpServiceId (String WcpServiceName, out Int64 WcpServiceId)
    {
      DataTable table;
      DataRow[] wcp_services;
      Boolean insert_queues;

      WcpServiceId = 0;
      insert_queues = false;

      for (int i = 0; i < 3; i++)
      {
        GetTableFromDataset("WCP_SERVICES", out table);
        wcp_services = table.Select("WSVR_NAME = '" + WcpServiceName.ToUpper() + "'");

        if (wcp_services.Length > 0)
        {
          try
          {
            WcpServiceId = (Int64)wcp_services[0]["WSVR_SERVICE_ID"];
          }
          catch 
          {
            // Do nothing
          }
        }

        if (WcpServiceId > 0)
        {
          if (insert_queues)
          {
            RegisterCjQueue(WcpServiceId);
          }
          return true;
        }

        if (i == 0)
        {
          ManageEvents(CachedTable.wcp_services);
        }

        if (i > 0)
        {
          RegisterWcpService(WcpServiceName);
          ManageEvents(CachedTable.wcp_services);
          insert_queues = true;
        }
      }

      return false;
    } // GetWcpServiceId

    /// <summary>
    /// Create a new wcp service on database.
    /// </summary>
    /// <param name="WcpServiceName"></param>
    /// <returns></returns>
    private static void RegisterWcpService(String WcpServiceName)
    {
      DataTable table;
      DataRow wcp_service;
      StringBuilder sql_str;
      SqlCommand cmd_insert;

      GetTableFromDataset("WCP_SERVICES", out table);
      wcp_service = table.NewRow();
      wcp_service["WSVR_NAME"] = WcpServiceName;
      table.Rows.Add(wcp_service);

      // Insert New Service
      sql_str = new StringBuilder();
      sql_str.AppendLine("INSERT INTO WCP_SERVICES (WSVR_NAME) ");
      sql_str.AppendLine("VALUES ");
      sql_str.AppendLine("(@p1) ");

      cmd_insert = new SqlCommand(sql_str.ToString());
      da.InsertCommand = cmd_insert;
      cmd_insert.Parameters.Add("@p1", SqlDbType.NVarChar, 50, "WSVR_NAME").Value = wcp_service["WSVR_NAME"];

      Update(ds.Tables["WCP_SERVICES"]);
      ds.Tables["WCP_SERVICES"].AcceptChanges();

      return;
    }

    /// <summary>
    /// Create a new cj_queue on database.
    /// </summary>
    /// <param name="WcpServiceName"></param>
    /// <returns></returns>
    private static void RegisterCjQueue(Int64 WcpServiceId)
    {
      SqlCommand sql_command;
      String sql_str;
      Int32 num_rows_inserted;

      try
      {
        //
        // CJ_QUEUES
        //
        sql_str = "INSERT INTO CJ_QUEUES (CQ_QUEUE_ID) VALUES (@p1) ";

        sql_command = new SqlCommand(sql_str);
        sql_command.Connection = WGDB.Connection();
        sql_command.Transaction = sql_command.Connection.BeginTransaction();

        sql_command.Parameters.Add("@p1", SqlDbType.BigInt, 8, "CQ_QUEUE_ID").Value = WcpServiceId;

        num_rows_inserted = sql_command.ExecuteNonQuery();
        if (num_rows_inserted != 1)
        {
          return;
        }
        sql_command.Transaction.Commit();

        //
        // WCP_SERVICES_TO_CJ_QUEUES
        //
        sql_str = "INSERT INTO WCP_SERVICES_TO_CJ_QUEUES (WSCQ_SERVICE_ID, WSCQ_QUEUE_ID ) VALUES (@p1, @p2) ";

        sql_command = new SqlCommand(sql_str);
        sql_command.Connection = WGDB.Connection();
        sql_command.Transaction = sql_command.Connection.BeginTransaction();

        sql_command.Parameters.Add("@p1", SqlDbType.BigInt, 8, "WSCQ_SERVICE_ID").Value = WcpServiceId;
        sql_command.Parameters.Add("@p2", SqlDbType.BigInt, 8, "WSCQ_QUEUE_ID").Value = WcpServiceId;

        num_rows_inserted = sql_command.ExecuteNonQuery();
        if (num_rows_inserted != 1)
        {
          return;
        }
        sql_command.Transaction.Commit();

      }
      catch
      {
        return;
      }
    }

    /// <summary>
    /// Get Service Id
    /// </summary>
    /// <returns></returns>
    public static Int64 GetWcpServiceId()
    {
      Int64 service_id;

      if (!GetWcpServiceId(Environment.MachineName, out service_id))
      {
        return -1;
      }

      return service_id;
    }

    /// <summary>
    /// Get site
    /// </summary>
    /// <param name="SiteName"></param>
    /// <param name="SiteId"></param>
    /// <returns></returns>
    private static Boolean GetSite(out String SiteName, out Guid SiteId)
    {
      DataTable table;
      DataRow[] site;

      SiteName = "";
      SiteId = Guid.Empty;

      for (int i = 0; i < 2; i++)
      {
        GetTableFromDataset("SITE", out table);
        site = table.Select();

        if (site.Length > 0)
        {
          SiteId = (Guid)site[0]["ST_ID"];
          SiteName = (String)site[0]["ST_NAME"];

          return true;
        }

        if (i == 0)
        {
          ManageEvents(CachedTable.site);
        }
      }

      return false;
    } // GetSite

    /// <summary>
    /// Reload in memory a specific table.
    /// </summary>
    /// <param name="Table"></param>
    /// <param name="Connection"></param>
    /// <returns></returns>
    private static Boolean ReloadTable(CachedTable Table, SqlConnection Connection)
    {
      SqlCommand cmd;
      SqlDataAdapter da;
      DataTable dt_load;
      String table_name;
      String db_table_name;
      StringBuilder sb;

      dt_load = null;
      da = null;
      cmd = null;

      table_name = Table.ToString();
      db_table_name = table_name;
      try
      {
        dt_load = new DataTable (table_name);
        da = new SqlDataAdapter();
        cmd = new SqlCommand();
        cmd.Connection = Connection;
        da.SelectCommand = cmd;

        switch (Table)
        {
          case CachedTable.wcp_services:

            sb = new StringBuilder();
            sb.AppendLine("SELECT WSVR_SERVICE_ID, UPPER(WSVR_NAME) AS WSVR_NAME, WSVR_WATCHDOG FROM WCP_SERVICES ");
            cmd.CommandText = sb.ToString();

            db_table_name = "WCP_SERVICES";
          break;

          case CachedTable.site:

            sb = new StringBuilder();
            sb.AppendLine("SELECT TOP 1 ST_ID, UPPER(ST_NAME) AS ST_NAME FROM SITE ");
            cmd.CommandText = sb.ToString();

            db_table_name = "SITE";
          break;

          // Tables: Games, Terminals
          default:
            cmd.CommandText = "SELECT * FROM " + table_name;

            db_table_name = table_name;
          break;
        }

        // Retrieve data
        da.Fill(dt_load);
        dbcache_lock.AcquireWriterLock(Timeout.Infinite);
        try
        {
          if (ds.Tables.IndexOf(table_name) >= 0)
          {
            ds.Tables.Remove(table_name);
          }

          ds.Tables.Add(dt_load);
        }
        finally
        {
          dbcache_lock.ReleaseWriterLock();
        }

        return true;
      }
      catch(Exception ex)
      {
        Log.Exception(ex);
        Log.Warning("Exception: Loading table: " + db_table_name);

        return false;
      }
      finally
      {
        if (da != null)
        {
          da.SelectCommand = null;
          da = null;
        }

        if (cmd != null)
        {
          cmd.Connection = null;
          cmd = null;
        }
      }
    }

    /// <summary>
    /// Get a table from dataset 'ds'. 
    /// Lock the ds for access.
    /// </summary>
    /// <param name="TableName"></param>
    /// <param name="Table"></param>
    private static void GetTableFromDataset(String TableName, out DataTable Table)
    {
      dbcache_lock.AcquireReaderLock(Timeout.Infinite);
      try
      {
        Table = ds.Tables[TableName];
      }
      finally
      {
        dbcache_lock.ReleaseReaderLock();
      }
    }

    /// <summary>
    /// Manage table reloads.
    /// </summary>
    static private void DbCacheThread()
    {
      int idx_table;
      TimeSpan elapsed;
      DateTime last_timeout;
      Int32 timeout_seconds;
      Random rnd;

      rnd = new Random(Environment.TickCount);

      last_timeout = DateTime.Now;
      timeout_seconds = 5 * 60 + rnd.Next(30);

      while (true)
      {
        SqlConnection conn;
        int idx_event;

        conn = null;

        try
        {
          idx_event = AutoResetEvent.WaitAny(event_reload, 2000, false);

          conn = WGDB.Connection ();

          if ( idx_event == AutoResetEvent.WaitTimeout )
          {
            //
            // Timeout
            //
            elapsed = DateTime.Now.Subtract(last_timeout);
            if (elapsed.TotalSeconds >= timeout_seconds)
            {
              WCP_BusinessLogic.DB_UpdateTimeoutWcpSessions(conn);
              last_timeout = DateTime.Now;
              timeout_seconds = 5 * 60 + rnd.Next(30);
            }
          }
          else
          {
            Boolean reloaded;

            reloaded = false;

            try
            {
              if (conn.State == ConnectionState.Open)
              {
                // Call the proper method depending on the the index
                reloaded = ReloadTable((CachedTable)idx_event, conn);

                if (reloaded)
                {
                  event_reloaded[idx_event].Set();
                  when_reloaded[idx_event] = DateTime.Now;
                }
              }
            }
            catch (Exception ex)
            {
              Log.Exception(ex);
            }
            finally
            {
              if (!reloaded)
              {
                event_reload[idx_event].Set();

                System.Threading.Thread.Sleep(1000);
              }
            }
          }

          idx_table = (int) CachedTable.terminals;
          elapsed = DateTime.Now.Subtract(when_reloaded[idx_table]);
          if ( elapsed.TotalSeconds >= 10 )
          {
            event_reload[idx_table].Set();
          }
        }
        catch (Exception ex)
        {
          Log.Exception(ex);
        }
        finally
        {
          if (conn != null)
          {
            conn.Close();
            conn = null;
          }
        }
      }
    }
    
    

    
    


  }
}
