﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems Ltd.
//------------------------------------------------------------------------------
//
//   MODULE NAME: WCP_AccountsCreditsExpirationDayMonth.cs
//
//   DESCRIPTION: WCP_AccountsCreditsExpirationDayMonth class
//
//        AUTHOR: Alberto Marcos
// 
// CREATION DATE: 31-AUG-2015

// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 31-AUG-2015 AMF    First release.
// 22-SEP-2015 AMF    Fixed BUG TFS-4628: Don't expire in multisite. Problem with OUTPUT vs Trigger
// 13-JAN-2016 JML    Product Backlog Item 2541: MultiSite Multicurrency PHASE3: Loyalty program in the sites.
// 14-MAR-2016 FGB    Renamed the class and methods with DEPRECATED_ preffix.
//------------------------------------------------------------------------------

using System;
using System.Messaging;
using System.Collections.Generic;
using System.Text;
using System.Net;
using WSI.Common;
using System.Xml;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using WSI.WCP;
using System.Collections;
using System.Threading;
using WSI.Common.TITO;

namespace WSI.WCP
{
  public static class DEPRECATED_WCP_AccountsPointsExpirationDayMonth
  {
    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Initialize WCP_AccountsCreditsExpiration class (DEPRECATED)
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    public static void DEPRECATED_Init()
    {
      Thread _thread;

      _thread = new Thread(DEPRECATED_ExpirationDayMonth);
      _thread.Name = "ExpirationDayMonth";
      _thread.Start();
    } // Init

    #endregion Public Methods

    #region Private Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Main thread (DEPRECATED)
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    static private void DEPRECATED_ExpirationDayMonth()
    {
      int _wait_hint;
      DateTime _now;

      while (true)
      {
        _now = WGDB.Now;
        _wait_hint = 60000 - _now.Second * 1000 - _now.Millisecond;

        Thread.Sleep(_wait_hint);

        // Only the service running as 'Principal' will excute the tasks.
        if (!Services.IsPrincipal("WCP"))
        {
          continue;
        }
        if (!GeneralParam.GetBoolean("Site", "MultiSiteMember", false)
          || (CommonMultiSite.GetPlayerTrackingMode() != PlayerTracking_Mode.Site))
        {
          continue;
        }

        if (!WSI.Common.DEPRECATED_AccountsPointsExpirationDayMonth.DEPRECATED_ProcessAccountsPointsExpirationDayMonth())
        {
          Log.Warning("WCP_Service.WSI.Common.DEPRECATED_AccountsPointsExpirationDayMonth.DEPRECATED_ProcessAccountsPointsExpirationDayMonth");
        }

      }
    } // ExpirationDayMonth
    #endregion Private Methods
  } // WCP_AccountsPointsExpirationDayMonth
} // WSI.WCP
