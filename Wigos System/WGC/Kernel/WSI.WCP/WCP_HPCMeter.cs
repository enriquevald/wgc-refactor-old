//------------------------------------------------------------------------------
// Copyright � 2010 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_HPCMeter.cs
// 
//   DESCRIPTION: WCP_HPCMeter class
// 
//        AUTHOR: Xavier Ib��ez
// 
// CREATION DATE: 21-JUL-2010
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 21-JUL-2010 XID    First release.
// 18-OCT-2011 JML    Add alarm register for Big Increment.
// 24-JUL-2012 ACC    Extended Meters: Add maximum meter values. Delta meters is zero if any maximum changed.
// 04-DEC-2013 JCOR   Control for hanpay in Handpay Meters.
// 05-DEC-2013 JMM    Handpays & TITO Tickets Link function added.
// 24-JAN-2014 JRM    Handpays & TITO Tickets refactored. HandPay VO created and methods delegated.
// 25-APR-2014 RCI & DRV & LEM  Fixed Bug: WIG-834: Meter History: AFTs and HPs are not updated
// 20-NOV-2014 DRV    Fixed Bug: WIG-1716: Jackpot handpay produces imbalance
// 11-NOV-2015 FOS    Product Backlog Item: 3709 Payment tickets & handpays
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using WSI.Common;
using WSI.Common.TITO;
using System.Data;
using System.Collections;



namespace WSI.WCP
{
  //------------------------------------------------------------------------------
  // PURPOSE : HPCMeter class 
  //
  //   NOTES :
  //
  public class HPCMeter
  {
    public Int64 HandpaysCents = 0;
    public Int64 HandpaysMaxValueCents = 0;

    #region Operators Overloading

    public Boolean IsEqual(HPCMeter HM2)
    {
      if (this.HandpaysCents == HM2.HandpaysCents &&
           this.HandpaysMaxValueCents == HM2.HandpaysMaxValueCents)
      {
        return true;
      }
      else
      {
        return false;
      }
    }

    #endregion
  }

  //------------------------------------------------------------------------------
  // PURPOSE : WCP_HPCMeter class for process hpc meter
  //
  //   NOTES :
  //
  public static class WCP_HPCMeter
  {

    //------------------------------------------------------------------------------
    // PURPOSE : Process new wcp HPCMeter message
    //
    //  PARAMS :
    //      - INPUT :
    //          - NewHPCMeter: Wcp message
    //          - TerminalId
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - true: update sales correctly.
    //      - false: otherwise.
    //
    //   NOTES :
    //

    #region Public Functions

    public static Boolean ProcessHPCMeter(Int32 TerminalId, Int64 WcpSequenceId, WcpInputMsgProcessTask Task)
    {
      SqlConnection _sql_conn;
      SqlTransaction _sql_trx;
      Int64 _db_wcp_sequence_id;
      Int32 _elapsed_minutes;
      Boolean _big_increment;

      HPCMeter _new_hpc_meter;
      HPCMeter _old_hpc_meter;
      HPCMeter _delta_hpc_meter;
      HPCMeter _zero_hpc_meter;

      Int32 _tick0;
      Int32 _tick1;
      Int64 _interval_select;
      Int64 _interval_update;
      Int64 _interval_sas_meter;
      Int64 _interval_all_process;

      Boolean _insert_hp;
      WCP_MsgReportHPCMeter _request;
      ArrayList _pending_tasks;

      _request = (WCP_MsgReportHPCMeter)Task.Request.MsgContent;

      _interval_select = 0;
      _interval_update = 0;
      _interval_sas_meter = 0;
      _interval_all_process = 0;
      _tick0 = Environment.TickCount;

      _sql_conn = null;
      _sql_trx = null;
      _zero_hpc_meter = new HPCMeter();

      try
      {
        //
        // Connect to DB
        //
        _sql_conn = WGDB.Connection();
        if (_sql_conn == null)
        {
          return false;
        }

        _pending_tasks = new ArrayList();
        _pending_tasks.Add(Task);

        _sql_trx = _sql_conn.BeginTransaction();

        // Get wcp message hpc meter
        GetMsgReportHPCMeter(_request, out _new_hpc_meter);

        _tick1 = Environment.TickCount;

        // Get database HPC Meter
        if (!DB_GetHPCMeter(TerminalId, out _old_hpc_meter, out _db_wcp_sequence_id, out _elapsed_minutes, _sql_trx))
        {
          // Insert HPC Meter
          if (!DB_InsertHPCMeter(TerminalId, WcpSequenceId, _new_hpc_meter, _sql_trx))
          {
            Log.Warning("Inserting HPC Meter. TerminalId: " + TerminalId.ToString() + ", WcpSequenceId: " + WcpSequenceId.ToString());

            return false;
          }

          _interval_select = Misc.GetElapsedTicks(_tick1);

          // RCI & DRV & LEM 24-APR-2014
          _tick1 = Environment.TickCount;

          if (!WCP_BU_SasMeters.Instance.Save(_pending_tasks, _sql_trx))
          {
            Log.Warning("Inserting HPC Meter As SAS Meter. TerminalId: " + TerminalId.ToString() + ", WcpSequenceId: " + WcpSequenceId.ToString());

            return false;
          }

          _interval_sas_meter = Misc.GetElapsedTicks(_tick1);

          _sql_trx.Commit();

          return true;
        }

        _interval_select = Misc.GetElapsedTicks(_tick1);

        if (_db_wcp_sequence_id >= WcpSequenceId)
        {
          _sql_trx.Commit();

          return true;
        }

        // RCI & DRV & LEM 24-APR-2014
        _tick1 = Environment.TickCount;

        if (!WCP_BU_SasMeters.Instance.Save(_pending_tasks, _sql_trx))
        {
          Log.Warning("Updating HPC Meter As SAS Meter. TerminalId: " + TerminalId.ToString() + ", WcpSequenceId: " + WcpSequenceId.ToString());

          return false;
        }

        _interval_sas_meter = Misc.GetElapsedTicks(_tick1);

        if (_new_hpc_meter.IsEqual(_old_hpc_meter))
        {
          _sql_trx.Commit();

          return true;
        }

        _delta_hpc_meter = new HPCMeter();
        _big_increment = false;

        if (!_request.IgnoreDeltas)
        {
          GetDeltaHPCMeter(TerminalId, _old_hpc_meter, _new_hpc_meter, out _delta_hpc_meter, out _big_increment);
        }

        _tick1 = Environment.TickCount;

        if (_delta_hpc_meter.HandpaysCents > 0)
        {
          WSI.Common.Terminal.TerminalInfo _terminal_info;

          _insert_hp = true;
          if (WSI.Common.Terminal.Trx_GetTerminalInfo(TerminalId, out _terminal_info, _sql_trx))
          {
            if (_terminal_info.SASFlagActivated(SAS_FLAGS.USE_HANDPAY_INFORMATION))
            {
              _insert_hp = false;
            } // if
          }

          if (!DB_UpdateHPCMachineStats(TerminalId, ((Decimal)_delta_hpc_meter.HandpaysCents) / 100, _sql_trx))
          {
            Log.Error("Updating HPC Meter Machine Stats. TerminalId: " + TerminalId.ToString() + ", Amount: " +
                        _delta_hpc_meter.HandpaysCents.ToString());

            return false;
          } // if

          if (_insert_hp)
          {
            if (!DB_InsertHandpay(TerminalId, _delta_hpc_meter, Task.Request.MsgHeader.GetSystemTime, _sql_trx))
            {
              Log.Warning("Updating HPC Meter Handpay. TerminalId: " + TerminalId.ToString() + ", Amount: " +
                          _delta_hpc_meter.HandpaysCents.ToString());

              return false;
            } // if
          }
        } // if

        // Update HPC Meter
        if (!DB_UpdateHPCMeter(TerminalId, WcpSequenceId, _new_hpc_meter, _old_hpc_meter, _sql_trx))
        {
          Log.Warning("Updating HPC Meter. TerminalId: " + TerminalId.ToString() + ", WcpSequenceId: " + WcpSequenceId.ToString());

          return false;
        }

        _interval_update = Misc.GetElapsedTicks(_tick1);

        _sql_trx.Commit();
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
      finally
      {
        if (_sql_trx != null)
        {
          if (_sql_trx.Connection != null)
          {
            _sql_trx.Rollback();
          }
          _sql_trx = null;
        }

        if (_sql_conn != null)
        {
          _sql_conn.Close();
          _sql_conn = null;
        }

        _interval_all_process = Misc.GetElapsedTicks(_tick0);

        if (_interval_select > 3000
            || _interval_update > 3000
            || _interval_sas_meter > 3000
            || _interval_all_process > 5000)
        {
          Log.Warning("Terminal: " + TerminalId.ToString() + ". Message HPCMeter times: Select/Update/SASMeter/Total = " +
                      _interval_select.ToString() + "/" + _interval_update.ToString() + _interval_sas_meter.ToString() + "/" +
                      "/" + _interval_all_process.ToString());
        }
      }

      return true;

    } // ProcessHPCMeter

    public static bool DB_UpdateHPCMachineStats(Int32 TerminalId,
                                              Decimal DeltaHpcIncrement,
                                              SqlTransaction Trx)
    {
      StringBuilder _sb;
      DateTime _now;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine("IF NOT EXISTS ( SELECT   *");
        _sb.AppendLine("                  FROM   MACHINE_STATS_PER_HOUR");
        _sb.AppendLine("                 WHERE   MSH_BASE_HOUR = @NowHour");
        _sb.AppendLine("                   AND   MSH_TERMINAL_ID = @TerminalId)");
        _sb.AppendLine("BEGIN");
        _sb.AppendLine("");
        _sb.AppendLine("  INSERT INTO MACHINE_STATS_PER_HOUR");
        _sb.AppendLine("             ( MSH_BASE_HOUR");
        _sb.AppendLine("             , MSH_TERMINAL_ID");
        _sb.AppendLine("             , MSH_PLAYED_COUNT");
        _sb.AppendLine("             , MSH_PLAYED_AMOUNT");
        _sb.AppendLine("             , MSH_WON_COUNT");
        _sb.AppendLine("             , MSH_WON_AMOUNT");
        _sb.AppendLine("             , MSH_TO_GM_COUNT");
        _sb.AppendLine("             , MSH_TO_GM_AMOUNT");
        _sb.AppendLine("             , MSH_FROM_GM_COUNT");
        _sb.AppendLine("             , MSH_FROM_GM_AMOUNT");
        _sb.AppendLine("             , MSH_JACKPOT_AMOUNT");
        _sb.AppendLine("             , MSH_HPC_HANDPAYS_AMOUNT)");
        _sb.AppendLine("       VALUES");
        _sb.AppendLine("             ( @NowHour");
        _sb.AppendLine("             , @TerminalId");
        _sb.AppendLine("             , 0");
        _sb.AppendLine("             , 0");
        _sb.AppendLine("             , 0");
        _sb.AppendLine("             , 0");
        _sb.AppendLine("             , 0");
        _sb.AppendLine("             , 0");
        _sb.AppendLine("             , 0");
        _sb.AppendLine("             , 0");
        _sb.AppendLine("             , 0");
        _sb.AppendLine("             , @Amount)");
        _sb.AppendLine("");
        _sb.AppendLine("END");
        _sb.AppendLine("ELSE");
        _sb.AppendLine("BEGIN");
        _sb.AppendLine("");
        _sb.AppendLine(" UPDATE   MACHINE_STATS_PER_HOUR            ");
        _sb.AppendLine("    SET   MSH_HPC_HANDPAYS_AMOUNT = ISNULL(MSH_HPC_HANDPAYS_AMOUNT,0) + @Amount ");
        _sb.AppendLine("  WHERE   MSH_TERMINAL_ID = @TerminalId     ");
        _sb.AppendLine("    AND   MSH_BASE_HOUR =  @NowHour    ");
        _sb.AppendLine("");
        _sb.AppendLine("END");

        using (SqlCommand _cmd_update = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _now = WGDB.Now;
          _now = new DateTime(_now.Year, _now.Month, _now.Day, _now.Hour, 0, 0);

          _cmd_update.Parameters.Add("@TerminalId", SqlDbType.Int).Value = TerminalId;
          _cmd_update.Parameters.Add("@Amount", SqlDbType.Money).Value = DeltaHpcIncrement;
          _cmd_update.Parameters.Add("@NowHour", SqlDbType.DateTime).Value = _now;

          if (_cmd_update.ExecuteNonQuery() != 1)
          {
            return false;
          }

          return true;
        }

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        Log.Warning("Exception throw: Error updating HPC Meter Machine Stats " + TerminalId.ToString());

        return false;
      }

    } // DB_UpdateHPCMachineStats

    public static bool DB_UpdateHandpayTicketId(Int32 TerminalId,
                                                Int64 TransactionId,
                                                HANDPAY_TYPE HandpayType,
                                                Decimal HandpayAmount,
                                                Int64 TicketID,
                                                SqlTransaction Trx)
    {
      DataTable _dt_tickets;
      Int32 _affected_hand_pays;
      Int64 _ticket_id;
      String TerminalName;
      String ProviderId;
      DateTime LastReportedDateTime;
      HANDPAY_STATUS _hp_status;
      Ticket _ticket;

      if (!Ticket.LoadTicketsForHandPay(TerminalId,
                                        TransactionId,
                                        HandpayAmount,
                                        TITO_TICKET_STATUS.VALID,
                                        HandpayType == HANDPAY_TYPE.CANCELLED_CREDITS ? TITO_TICKET_TYPE.HANDPAY : TITO_TICKET_TYPE.JACKPOT,
                                        out _dt_tickets,
                                        Trx))
      {
        Log.Error(String.Format("DB_UpdateHandpayTicketID --> {0} tickets found for transaction id {1}", _dt_tickets.Rows.Count, TransactionId));

        return false;
      }

      if (_dt_tickets.Rows.Count > 1)
      {
        Log.Error(String.Format("DB_UpdateHandpayTicketID --> {0} tickets found for transaction id {1}", _dt_tickets.Rows.Count, TransactionId));

        return false;
      }

      if (_dt_tickets.Rows.Count == 1)
      {
        _ticket_id = (Int64)_dt_tickets.Rows[0]["TI_TICKET_ID"];
      }
      else
      {
        //No ticket found.  Exit function
        return true;
      }

      if (!HandPay.DB_UpdateHandPayTicket(_ticket_id,
                                          TerminalId,
                                          TransactionId,
                                          HandpayAmount,
                                          HandpayType,
                                          out _affected_hand_pays,
                                          out _hp_status,
                                          Trx))
      {
        Log.Error(String.Format("DB_UpdateHandPayTicket Trx Error --> {0} handpays found for transaction id = {1} ",
                                _affected_hand_pays,
                                TransactionId));

        return false;
      }

      if (_affected_hand_pays > 1)
      {
        Log.Error(String.Format("DB_UpdateHandpayTicketID --> {0} handpays found for transaction id = {1} ", _affected_hand_pays, TransactionId));

        return false;
      }

      if (_affected_hand_pays == 0)
      {
        if (!DB_GetLastReportedHandPayInfo(TerminalId,
                                       out LastReportedDateTime,
                                       out TerminalName,
                                       out ProviderId,
                                       Trx))
        {
          Log.Warning("Exception throw: Error reading Handpay history (HPC_LAST_REPORTED) information " + TerminalId.ToString());

          return false;
        }

        if (!HandPay.DB_InsertNewHandPay(_ticket_id,
                                         TerminalId,
                                         TransactionId,
                                         HandpayAmount,
                                         HandpayType,
                                         TerminalName,
                                         ProviderId,
                                         LastReportedDateTime,
                                         Trx))
        {
          Log.Error(String.Format("DB_InsertNewHandPay Trx Error --> handpays transaction id = {1} ", TransactionId));

          return false;
        }
      }
      else
      {
        if (_affected_hand_pays == 1 && Handpays.StatusActivated((Int32)_hp_status, HANDPAY_STATUS.PAID))
        {
          Int32 _cashier_terminal_id;
          if (DB_GetHandPayPaymentCashierTerminal(TerminalId, TransactionId, out _cashier_terminal_id, Trx))
          {
            _ticket = Ticket.LoadTicket(_ticket_id, Trx, false);
            _ticket.Status = TITO_TICKET_STATUS.REDEEMED;
            _ticket.LastActionDateTime = WGDB.Now;
            _ticket.LastActionTerminalID = _cashier_terminal_id;
            Ticket.DB_UpdateTicket(_ticket, Trx);
          }
        }
      }

      return true;
    } // DB_UpdateHandpayTicketID

    #endregion // Public Functions

    #region Private Functions

    //------------------------------------------------------------------------------
    // PURPOSE : Get Database HPC Meter
    //
    //  PARAMS :
    //      - INPUT :
    //          - TerminalId
    //          - Trx
    //
    //      - OUTPUT :
    //          - DbHPCMeter
    //          - DbWcpSequenceId
    //          - ElapsedHours
    //
    // RETURNS :
    //      - true: successful.
    //      - false: not found or error.
    //
    //   NOTES :
    //
    private static Boolean DB_GetHPCMeter(Int32 TerminalId, out HPCMeter DbHPCMeter, out Int64 DbWcpSequenceId, out Int32 ElapsedMinutes, SqlTransaction Trx)
    {
      String _str_sql;
      SqlCommand _sql_command;
      SqlDataReader _sql_reader;

      DbHPCMeter = new HPCMeter();
      DbWcpSequenceId = 0;
      ElapsedMinutes = 0;

      _sql_reader = null;

      try
      {
        _str_sql = "";
        _str_sql += "SELECT   HPC_WCP_SEQUENCE_ID";
        _str_sql += "       , HPC_HANDPAYS_AMOUNT";
        _str_sql += "       , HPC_MAX_HANDPAYS_AMOUNT";
        _str_sql += "       , DATEDIFF (MINUTE, HPC_LAST_REPORTED, GETDATE()) ";
        _str_sql += "  FROM   HPC_METER ";
        _str_sql += " WHERE   HPC_TERMINAL_ID = @pTerminalID ";

        _sql_command = new SqlCommand(_str_sql);
        _sql_command.Connection = Trx.Connection;
        _sql_command.Transaction = Trx;

        // Parameters
        _sql_command.Parameters.Add("@pTerminalID", SqlDbType.Int).Value = TerminalId;

        _sql_reader = _sql_command.ExecuteReader();

        if (_sql_reader.Read())
        {
          DbWcpSequenceId = _sql_reader.GetInt64(0);
          DbHPCMeter.HandpaysCents = (Int64)(_sql_reader.GetDecimal(1) * 100);
          if (!_sql_reader.IsDBNull(2))
          {
            DbHPCMeter.HandpaysMaxValueCents = (Int64)(_sql_reader.GetDecimal(2) * 100);
          }
          else
          {
            DbHPCMeter.HandpaysMaxValueCents = -1;
          }
          ElapsedMinutes = _sql_reader.GetInt32(3);
        }
        else
        {
          return false;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
      finally
      {
        if (_sql_reader != null)
        {
          _sql_reader.Close();
          _sql_reader.Dispose();
          _sql_reader = null;
        }
      }

      return true;

    } // DB_GetHPCMeter

    //------------------------------------------------------------------------------
    // PURPOSE : Copy Wcp Msg HPC Meter in HPCMeter class
    //
    //  PARAMS :
    //      - INPUT :
    //          - WcpMsgHPCMeter: Wcp report message 
    //
    //      - OUTPUT :
    //          - NewHPCMeter
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private static void GetMsgReportHPCMeter(WCP_MsgReportHPCMeter WcpMsgHPCMeter, out HPCMeter NewHPCMeter)
    {
      NewHPCMeter = new HPCMeter();

      NewHPCMeter.HandpaysCents = WcpMsgHPCMeter.HandPaysCents;
      NewHPCMeter.HandpaysMaxValueCents = WcpMsgHPCMeter.HandPaysMaxValueCents;

    } // GetMsgReportHPCMeter

    //------------------------------------------------------------------------------
    // PURPOSE : Get Delta all HPC Meter 
    //
    //  PARAMS :
    //      - INPUT :
    //          - TerminalId
    //          - OldHPCMeter
    //          - NewHPCMeter
    //
    //      - OUTPUT :
    //          - DeltaHPCMeter
    //
    // RETURNS :
    //      - HPCMeter
    //
    //   NOTES :
    //
    public static void GetDeltaHPCMeter(Int32 TerminalId,
                                         HPCMeter OldHPCMeter,
                                         HPCMeter NewHPCMeter,
                                     out HPCMeter DeltaHPCMeter,
                                     out Boolean BigIncrement)
    {
      Boolean _big;
      String _source_name;
      String _terminal_name;
      String _provider_id;
      long _sesion_id;

      BigIncrement = false;
      DeltaHPCMeter = new HPCMeter();

      if (NewHPCMeter.HandpaysCents == 0)
      {
        Log.Warning("*** HPC Meter Reset *** TerminalId: " + TerminalId.ToString() +
                    ". Old hpc meter value - Handpay Cents: " + OldHPCMeter.HandpaysCents.ToString());
        return;
      }

      _big = false;

      _big = _big || TerminalMeterGroup.GetDeltaMeter(TerminalId, "HPCHandpaysCents", OldHPCMeter.HandpaysCents, NewHPCMeter.HandpaysCents, OldHPCMeter.HandpaysMaxValueCents, NewHPCMeter.HandpaysMaxValueCents, BigIncrementsData.CentsHandPays(TerminalId), out DeltaHPCMeter.HandpaysCents);

      if (_big)
      {
        // Return all "delta" as zero
        DeltaHPCMeter = new HPCMeter();

        Log.Warning("*** HPC Meter BigIncrement *** TerminalId: " + TerminalId.ToString() +
                    ". Old hpc meter value - Handpay Cents: " + OldHPCMeter.HandpaysCents.ToString());

        Log.Warning("*** HPC Meter BigIncrement *** TerminalId: " + TerminalId.ToString() +
                    ". New meter values - Handpay Cents: " + NewHPCMeter.HandpaysCents.ToString());

        BigIncrement = true;

        WSI.Common.BatchUpdate.TerminalSession.GetOpenedTerminalSession(TerminalId, out _sesion_id);
        WSI.Common.BatchUpdate.TerminalSession.GetData(TerminalId, _sesion_id, out _provider_id, out _terminal_name);
        _source_name = WSI.Common.Misc.ConcatProviderAndTerminal(_provider_id, _terminal_name);

        StringBuilder _sb;
        _sb = new StringBuilder();
        _sb.AppendLine("BigIncrement. TerminalId: " + TerminalId.ToString());
        _sb.AppendLine(" Old hpc meter value - Handpay Cents: " + OldHPCMeter.HandpaysCents.ToString().PadLeft(12));
        _sb.AppendLine(" New hpc meter value - Handpay Cents: " + NewHPCMeter.HandpaysCents.ToString().PadLeft(12));

        WSI.Common.Alarm.Register(AlarmSourceCode.TerminalSystem,
                                  TerminalId,
                                  _source_name,
                                  AlarmCode.TerminalSystem_HPCMeterBigIncrement,
                                  _sb.ToString());

        TerminalMeterGroup.InsertSasMetersAdjustments(Misc.TodayOpening()
                                                  , TerminalId
                                                  , 0x0003
                                                  , 0
                                                  , 0
                                                  , WGDB.Now
                                                  , TerminalMeterGroup.MetersAdjustmentsType.System
                                                  , TerminalMeterGroup.MetersAdjustmentsSubType.System_Machine_BigIncrement
                                                  , OldHPCMeter.HandpaysCents
                                                  , NewHPCMeter.HandpaysCents
                                                  , DeltaHPCMeter.HandpaysCents
                                                  , "BigIncrement: Handpay Cents");


      }

    } // GetDeltaHPCMeter

    //------------------------------------------------------------------------------
    // PURPOSE : Update HPC Meter 
    //
    //  PARAMS :
    //      - INPUT :
    //          - TerminalId
    //          - WcpSequenceId
    //          - NewHPCMeter: Wcp message hpc meter
    //          - OldHPCMeter
    //          - Trx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //      - true: hpc meter updated.
    //      - false: not found or error.
    //
    private static Boolean DB_UpdateHPCMeter(Int32 TerminalId, Int64 WcpSequenceId, HPCMeter NewHPCMeter, HPCMeter OldHPCMeter, SqlTransaction Trx)
    {
      SqlCommand _sql_command;
      String _sql_str;
      Int32 _num_rows_updated;

      _sql_str = "";
      _sql_str += "UPDATE HPC_METER   SET HPC_WCP_SEQUENCE_ID     = @pWcpSequenceId ";
      _sql_str += "                     , HPC_HANDPAYS_AMOUNT     = @pNewHandpaysAmount ";
      _sql_str += "                     , HPC_MAX_HANDPAYS_AMOUNT = @pMaxHandpaysAmount ";
      _sql_str += "                     , HPC_LAST_REPORTED   = GETDATE() ";
      _sql_str += " WHERE   HPC_TERMINAL_ID     = @pTerminalID   ";
      _sql_str += "   AND   HPC_HANDPAYS_AMOUNT = @pOldHandpaysAmount ";


      _sql_command = new SqlCommand(_sql_str);
      _sql_command.Connection = Trx.Connection;
      _sql_command.Transaction = Trx;
      _sql_command.Parameters.Add("@pWcpSequenceId", SqlDbType.BigInt).Value = WcpSequenceId;
      _sql_command.Parameters.Add("@pNewHandpaysAmount", SqlDbType.Money).Value = ((Decimal)NewHPCMeter.HandpaysCents) / 100;
      _sql_command.Parameters.Add("@pMaxHandpaysAmount", SqlDbType.Money).Value = ((Decimal)NewHPCMeter.HandpaysMaxValueCents) / 100;
      _sql_command.Parameters.Add("@pTerminalID", SqlDbType.Int).Value = TerminalId;
      _sql_command.Parameters.Add("@pOldHandpaysAmount", SqlDbType.Money).Value = ((Decimal)OldHPCMeter.HandpaysCents) / 100;

      try
      {
        _num_rows_updated = _sql_command.ExecuteNonQuery();
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        Log.Warning("Updating HPC_METER [Exception]: TerminalId: " + TerminalId.ToString());

        return false;
      }

      if (_num_rows_updated != 1)
      {
        Log.Warning("Updating HPC_METER [Not updated]: TerminalId: " + TerminalId.ToString());

        return false;
      }

      return true;

    } // DB_UpdateHPCMeter

    //------------------------------------------------------------------------------
    // PURPOSE : Insert hpc meter received.
    //
    //  PARAMS :
    //      - INPUT :
    //          - TerminalId
    //          - WcpSequenceId
    //          - NewHPCMeter: Wcp message hpc meter
    //          - Trx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - HPCMeter
    //
    //   NOTES :
    //      - true: hpc meter inserted.
    //      - false: error.
    //
    private static Boolean DB_InsertHPCMeter(Int32 TerminalId, Int64 WcpSequenceId, HPCMeter NewHPCMeter, SqlTransaction Trx)
    {
      SqlCommand _sql_command;
      String _sql_str;
      Int32 _num_rows_inserted;

      _sql_str = "";
      _sql_str += "INSERT INTO HPC_METER ( HPC_TERMINAL_ID ";
      _sql_str += "                        , HPC_WCP_SEQUENCE_ID ";
      _sql_str += "                        , HPC_HANDPAYS_AMOUNT ";
      _sql_str += "                        , HPC_MAX_HANDPAYS_AMOUNT ";
      _sql_str += "                        , HPC_LAST_REPORTED ) ";
      _sql_str += "               VALUES   ( @pTerminalID ";
      _sql_str += "                        , @pWcpSequenceId ";
      _sql_str += "                        , @pNewHandpaysAmount ";
      _sql_str += "                        , @pMaxHandpaysAmount ";
      _sql_str += "                        , GETDATE() ) ";

      _sql_command = new SqlCommand(_sql_str);
      _sql_command.Connection = Trx.Connection;
      _sql_command.Transaction = Trx;

      _sql_command.Parameters.Add("@pTerminalID", SqlDbType.Int).Value = TerminalId;
      _sql_command.Parameters.Add("@pWcpSequenceId", SqlDbType.BigInt).Value = WcpSequenceId;
      _sql_command.Parameters.Add("@pNewHandpaysAmount", SqlDbType.Money).Value = ((Decimal)NewHPCMeter.HandpaysCents) / 100;
      _sql_command.Parameters.Add("@pMaxHandpaysAmount", SqlDbType.Money).Value = ((Decimal)NewHPCMeter.HandpaysMaxValueCents) / 100;

      try
      {
        _num_rows_inserted = _sql_command.ExecuteNonQuery();
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        Log.Warning("Inserting HPC_METER [Exception]: TerminalId: " + TerminalId.ToString());

        return false;
      }

      if (_num_rows_inserted != 1)
      {
        Log.Warning("Inserting HPC_METER [Not updated]: TerminalId: " + TerminalId.ToString());

        return false;
      }

      return true;

    } // DB_InsertHPCMeter

    //------------------------------------------------------------------------------
    // PURPOSE : Get Last reported, terminal name and provider name from terminal id
    //
    //  PARAMS :
    //      - INPUT :
    //          - TerminalId
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //      - true: Handpay inserted.
    //      - false: error.
    //
    static private Boolean DB_GetLastReportedHandPayInfo(Int32 TerminalId,
                                                         out DateTime LastReportedDatetime,
                                                         out String TerminalName,
                                                         out String ProviderId,
                                                         SqlTransaction Trx)
    {
      SqlDataReader _reader;

      StringBuilder _sb;

      _sb = new StringBuilder();
      LastReportedDatetime = WGDB.Now;
      TerminalName = String.Empty;
      ProviderId = String.Empty;

      _reader = null;

      _sb.AppendLine("SELECT   HPC_LAST_REPORTED                    ");
      _sb.AppendLine("       , TE_NAME                              ");
      _sb.AppendLine("       , TE_PROVIDER_ID                       ");
      _sb.AppendLine("  FROM   HPC_METER, TERMINALS                 ");
      _sb.AppendLine(" WHERE   HPC_TERMINAL_ID    = TE_TERMINAL_ID  ");
      _sb.AppendLine("   AND   HPC_TERMINAL_ID    = @TerminalId     ");

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@TerminalId", SqlDbType.Int).Value = TerminalId;
          _reader = _cmd.ExecuteReader();

          if (_reader.Read())
          {
            LastReportedDatetime = (DateTime)_reader[0];
            TerminalName = _reader[1].ToString();
            ProviderId = _reader[2].ToString();
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        Log.Warning("Exception throw: Error reading Handpay history (HPC_LAST_REPORTED) information " + TerminalId.ToString());

        return false;
      }
      finally
      {
        if (_reader != null)
        {
          _reader.Close();
          _reader.Dispose();
          _reader = null;
        }
      }

      return true;

    } // DB_GetLastReportedHandPay

    //------------------------------------------------------------------------------
    // PURPOSE : Get terminal id of the cashier that realise the payment
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //      - true: ok.
    //      - false: error.
    //
    static private Boolean DB_GetHandPayPaymentCashierTerminal(Int32 TerminalId,
                                                               Int64 TransactionId,
                                                               out Int32 CashierTerminalId,
                                                               SqlTransaction Trx)
    {
      SqlDataReader _reader;

      StringBuilder _sb;

      _sb = new StringBuilder();
      CashierTerminalId = 0;

      _reader = null;

      _sb.AppendLine("    SELECT   AM_CASHIER_ID    ");
      _sb.AppendLine("      FROM   HANDPAYS          ");
      _sb.AppendLine(" LEFT JOIN   ACCOUNT_MOVEMENTS ");
      _sb.AppendLine("        ON   AM_MOVEMENT_ID = HP_MOVEMENT_ID     ");
      _sb.AppendLine("     WHERE   HP_TERMINAL_ID = @pTerminalId       ");
      _sb.AppendLine("       AND   HP_TRANSACTION_ID = @pTransactionId ");

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;
          _cmd.Parameters.Add("@pTransactionId", SqlDbType.BigInt).Value = TransactionId;
          _reader = _cmd.ExecuteReader();

          if (_reader.Read())
          {
            CashierTerminalId = (Int32)_reader[0];
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        Log.Warning("Exception throw: Error DB_GetHandPayPaymentCashierTerminal ");

        return false;
      }
      finally
      {
        if (_reader != null)
        {
          _reader.Close();
          _reader.Dispose();
          _reader = null;
        }
      }

      return true;

    } // DB_GetHandPayPaymentTerminal

    //------------------------------------------------------------------------------
    // PURPOSE : Insert Handpay
    //
    //  PARAMS :
    //      - INPUT :
    //          - TerminalId
    //          - DeltaHPCMeter
    //          - Trx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //      - true: Handpay inserted.
    //      - false: error.
    //
    static private Boolean DB_InsertHandpay(Int32 TerminalId, HPCMeter DeltaHPCMeter, DateTime HPDateTime, SqlTransaction Trx)
    {
      DateTime _last_reported;
      String _terminal_name;
      String _provider_id;
      StringBuilder _sb;
      Currency _alarm_threshold;
      String _source_name;
      String _alarm;

      _sb = new StringBuilder();
      _last_reported = WGDB.Now;
      _terminal_name = String.Empty;
      _provider_id = String.Empty;

      if (!DB_GetLastReportedHandPayInfo(TerminalId,
                                         out _last_reported,
                                         out _terminal_name,
                                         out _provider_id,
                                         Trx))
      {
        Log.Warning("Exception throw: Error reading Handpay history (HPC_LAST_REPORTED) information " + TerminalId.ToString());

        return false;
      }

      //Insert Handpay with HP_TYPE MachineReported = 0.
      if (!HandPay.DB_InsertNewHandPay(0,
                                      TerminalId,
                                      0,
                                      ((Decimal)DeltaHPCMeter.HandpaysCents) / 100,
                                      HANDPAY_TYPE.CANCELLED_CREDITS,
                                      _terminal_name,
                                      _provider_id,
                                      _last_reported,
                                      Trx))
      {
        Log.Warning("Inserting HANDPAY [Exception]: TerminalId: " + TerminalId.ToString());

        return false;
      }

      _alarm_threshold = GeneralParam.GetCurrency("Alarms", "HandpaysAmountThreshold", 0);

      // Check if handpay amount exceeds the amount defined on the GP that signals the alarm point
      if ((DeltaHPCMeter.HandpaysCents / 100) > _alarm_threshold && _alarm_threshold > 0)
      {
        _source_name = WSI.Common.Misc.ConcatProviderAndTerminal(_provider_id, _terminal_name);
        _alarm = Resource.String("STR_ALARM_CANCELED_CREDITS_AMOUNT_THRESHOLD", ((Currency)((Currency)DeltaHPCMeter.HandpaysCents / 100)).ToString());

        // Handpay amount exceedds GP amount. Insert alarm 
        Alarm.Register(AlarmSourceCode.TerminalSystem,
                       TerminalId,
                       _source_name,
                       (UInt32)AlarmCode.TerminalSystem_HandpayWarningAmount,
                       _alarm,
                       AlarmSeverity.Warning,
                       HPDateTime,
                       Trx);
      }

      return true;
    } // DB_InsertHandpay

    #endregion // Private Functions

  }
}
