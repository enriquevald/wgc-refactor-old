//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_WSP_Thread.cs
// 
//   DESCRIPTION: Thread to make recharge of NR amounts to clients
// 
//        AUTHOR: Ra�l Cervera
// 
// CREATION DATE: 09-JAN-2013
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 09-JAN-2013 RCI    First release.
// 10-JAN-2013 HBB    Add Methods DoRecharge, ProcessRecharge, GetKeysFromPlayerRecharge
// 03-MAY-2013 JCA    Moved ProcessRecharge to AccountAddCredit.cs.
//------------------------------------------------------------------------------

using System;
using System.Text;
using System.Threading;
using WSI.Common;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace WSI.WCP
{
  public static class WCP_WSP
  {
    //------------------------------------------------------------------------------
    // PURPOSE : Initializes and Starts WCP_WSP_Thread
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public static void Init()
    {
      try
      {
        Thread _thread;
        _thread = new Thread(WSP_Thread);
        _thread.Name = "WSP_Thread";
        _thread.Start();
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    } // Init

    //------------------------------------------------------------------------------
    // PURPOSE : Main thread
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private static void WSP_Thread()
    {
      Int32 _wait_hint;

      _wait_hint = 0;

      while (true)
      {
        Thread.Sleep(_wait_hint);
        _wait_hint = 1000;

        try
        {
          DataTable _wsp_player_recharges;
          DateTime _now;
          TimeSpan _elapsed;
          ArrayList _voucherList = new ArrayList();
          PLAYER_RECHARGE_STATUS _old_status;
          PLAYER_RECHARGE_STATUS _new_status;
          CashierSessionInfo _session_info;
          Int64 _dummy_operation_id;
          
          _wsp_player_recharges = ReadAllPlayerRecharges();

          _now = WGDB.Now;
          foreach (DataRow _row in _wsp_player_recharges.Rows)
          {
            try
            {
              _old_status = (PLAYER_RECHARGE_STATUS)_row["WPR_STATUS"];

              _elapsed = _now - (DateTime)_row["WPR_CREATED"];
              if (_elapsed.TotalSeconds >= 60)
              {
                // Timeout --> Delete
                Delete((Int64)_row["WPR_UNIQUE_ID"], _old_status);

                continue;
              }

              if (_old_status != PLAYER_RECHARGE_STATUS.PENDING)
              {
                // Already processed
                continue;
              }

              // STATUS = PENDING

              using (DB_TRX _db_trx = new DB_TRX())
              {
                if (_elapsed.TotalSeconds >= 30)
                {
                  // Timeout --> Mark as Error
                  if (ChangeStatus((Int64)_row["WPR_UNIQUE_ID"], _old_status, PLAYER_RECHARGE_STATUS.TIMEOUT, _db_trx.SqlTransaction))
                  {
                    _db_trx.Commit();
                  }

                  continue;
                }

                // Lock row
                if (!ChangeStatus((Int64)_row["WPR_UNIQUE_ID"], _old_status, PLAYER_RECHARGE_STATUS.IN_PROGRESS, _db_trx.SqlTransaction))
                {
                  // Not locked!
                  _db_trx.Rollback();

                  continue;
                }

                _new_status = PLAYER_RECHARGE_STATUS.ERROR;

                _db_trx.SqlTransaction.Save("BeforeRecharge");

                _session_info = Cashier.GetSystemCashierSessionInfo(GU_USER_TYPE.SYS_SYSTEM, _db_trx.SqlTransaction);
                if (Accounts.ProcessRecharge(_session_info, (Int64)_row["WPR_ACCOUNT_ID"], (Decimal)_row["WPR_NR_AMOUNT"], false, Promotion.PROMOTION_TYPE.EXTERNAL_NON_REDEEM, "", _db_trx.SqlTransaction, out _voucherList, out _dummy_operation_id))
                {
                  _new_status = PLAYER_RECHARGE_STATUS.OK;
                }
                else
                {
                  _db_trx.SqlTransaction.Rollback("BeforeRecharge");
                }

                if (ChangeStatus((Int64)_row["WPR_UNIQUE_ID"], PLAYER_RECHARGE_STATUS.IN_PROGRESS, _new_status, _db_trx.SqlTransaction))
                {
                  _db_trx.Commit();

                  _wait_hint = 0;

                  continue;
                }

                _db_trx.Rollback();
              }
            }
            catch (Exception _ex)
            {
              Log.Exception(_ex);
            }
          }
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }
      } // while

    } // WSP_Thread

    //------------------------------------------------------------------------------
    // PURPOSE : Gets the keys from the table WSP_PLAYER_RECHARGE of those players who are pending of being recharged.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS : DataTable with the keys
    //
    private static DataTable ReadAllPlayerRecharges()
    {
      StringBuilder _sb;
      DataTable _wsp_player_recharges;

      _wsp_player_recharges = new DataTable();

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          // AJQ 23-JAN-2013, read the status & creation time to later delelte 'orphan' registers
          _sb = new StringBuilder();
          _sb.AppendLine("SELECT   WPR_UNIQUE_ID                ");
          _sb.AppendLine("       , WPR_ACCOUNT_ID               ");
          _sb.AppendLine("       , WPR_NR_AMOUNT                ");
          _sb.AppendLine("       , WPR_STATUS                   ");
          _sb.AppendLine("       , WPR_CREATED                  ");
          _sb.AppendLine("  FROM   WSP_PLAYER_RECHARGE          ");
          _sb.AppendLine("ORDER BY WPR_STATUS ASC, WPR_UNIQUE_ID ASC  ");

          using (SqlCommand _cmd = new SqlCommand(_sb.ToString()))
          {
            _cmd.Parameters.Add("@pStatusPending", SqlDbType.Int).Value = PLAYER_RECHARGE_STATUS.PENDING;
            using (SqlDataAdapter _da = new SqlDataAdapter(_cmd))
            {
              _db_trx.Fill(_da, _wsp_player_recharges);
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        // Clear the data after an exception to unsure no data is in.
        _wsp_player_recharges.Clear();
      }

      return _wsp_player_recharges;
    } // GetKeysFromPlayerRecharge

    



    private static Boolean ChangeStatus(Int64 Id, PLAYER_RECHARGE_STATUS OldStatus, PLAYER_RECHARGE_STATUS NewStatus, SqlTransaction Trx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.Length = 0;
        _sb.AppendLine("UPDATE   WSP_PLAYER_RECHARGE          ");
        _sb.AppendLine("   SET   WPR_STATUS    = @pNewStatus  ");
        _sb.AppendLine("  FROM   WSP_PLAYER_RECHARGE          ");
        _sb.AppendLine(" WHERE   WPR_UNIQUE_ID = @pUniqueId   ");
        _sb.AppendLine("   AND   WPR_STATUS    = @pOldStatus  ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pUniqueId", SqlDbType.BigInt).Value = Id;
          _cmd.Parameters.Add("@pNewStatus", SqlDbType.Int).Value = NewStatus;
          _cmd.Parameters.Add("@pOldStatus", SqlDbType.Int).Value = OldStatus;

          _cmd.CommandTimeout = 2;  // 2 seconds

          if (_cmd.ExecuteNonQuery() == 1)
          {
            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // ChangeStatus


    private static Boolean Delete(Int64 Id, PLAYER_RECHARGE_STATUS OldStatus)
    {
      StringBuilder _sb;

      try
      {
        using (DB_TRX _trx = new DB_TRX())
        {
          _sb = new StringBuilder();
          _sb.Length = 0;
          _sb.AppendLine("DELETE   WSP_PLAYER_RECHARGE          ");
          _sb.AppendLine(" WHERE   WPR_UNIQUE_ID = @pUniqueId   ");
          _sb.AppendLine("   AND   WPR_STATUS    = @pOldStatus  ");

          using (SqlCommand _cmd = new SqlCommand(_sb.ToString()))
          {
            _cmd.Parameters.Add("@pUniqueId", SqlDbType.BigInt).Value = Id;
            _cmd.Parameters.Add("@pOldStatus", SqlDbType.Int).Value = OldStatus;

            if (_trx.ExecuteNonQuery(_cmd) == 1)
            {
              if (_trx.Commit())
              {
                return true;
              }
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // ChangeStatus

  }// WCP_WSP
} // WSI.WCP
