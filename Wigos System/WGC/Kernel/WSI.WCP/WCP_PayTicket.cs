//------------------------------------------------------------------------------
// Copyright � 2014 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_PayTicket.cs
// 
//   DESCRIPTION: WCP_PayTicket class
// 
//        AUTHOR: Alberto Cuesta
// 
// CREATION DATE: 19-MAY-2014
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 19-MAY-2014 ACC    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using WSI.Common;
using System.Data;
using System.Threading;
using System.Xml;

namespace WSI.WCP
{

  #region Process Functions

  public static class WCP_PayTicket
  {



    //------------------------------------------------------------------------------
    // PURPOSE : Init wcp commands thread
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES : JCOR: TODO: queda aplazada el desarrollo de esta funci�n.
    //
    public static void ProcessCheckTicket(SecureTcpClient TcpClient, WCP_Message WcpRequestMsg, WCP_Message WcpResponseMsg, SqlTransaction SqlTrx)
    {
      WCP_MsgCheckTicket _request;
      WCP_MsgCheckTicketReply _response;
      //Int32 _terminal_id;

      try
      {
        _request = (WCP_MsgCheckTicket)WcpRequestMsg.MsgContent;
        _response = (WCP_MsgCheckTicketReply)WcpResponseMsg.MsgContent;

        //_terminal_id = TcpClient.InternalId;

        StringBuilder _sb;
        DataTable _tickets = new DataTable();

        _sb = new StringBuilder();
        _sb.AppendLine("  SELECT			TI_TICKET_ID                                  ,");
        _sb.AppendLine("				      TI_AMOUNT                                     ,");
        _sb.AppendLine("              TI_EXPIRATION_DATETIME                        ,");
        _sb.AppendLine("              TI_STATUS                                      ");
        _sb.AppendLine("  FROM			  TICKETS                                        ");
        _sb.AppendLine("  WHERE			  TI_VALIDATION_NUMBER     = @pValidationNumber  ");


        using (DB_TRX _db_trx = new DB_TRX()) //quitar al pasarle la trx por par�metro.
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pValidationNumber", SqlDbType.BigInt).Value = _request.ValidationNumberSequence;
            // TODO : a�adir el estado como parametro. 0 o 1
           
            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
            {
              _sql_da.Fill(_tickets);

              _response.SystemId = _request.SystemId;
              _response.ValidationNumberSequence = _request.ValidationNumberSequence;
              _response.AmountCents = (Int64)((Decimal)_tickets.Rows[0]["TI_AMOUNT"] * 100 );
              _response.ExpirationDate = (DateTime)_tickets.Rows[0]["TI_EXPIRATION_DATETIME"];
              _response.TicketId = (Int64)_tickets.Rows[0]["TI_TICKET_ID"];

            }
          }
        }        
      }        
      catch (Exception _ex)
      {        
        throw _ex;
      }
        

      ///////////////////////////////////////////////////////////////////
      ///// ONLY FOR TEST ///////////////////////////////////////////////
      ///////////////////////////////////////////////////////////////////
      _response.TicketId = 5005005005;

      if (_request.ValidationNumberSequence == 1363537746602292)
      {
        _response.AmountCents = 2700;
        _response.ValidateStatus = (Int32)ENUM_TITO_IS_VALID_TICKET_MSG.VALID_RESTRICTED_PROMOTIONAL_TICKET;
      }
      else if (_request.ValidationNumberSequence == 8201932730776820)
      {
        _response.AmountCents = 74;
        _response.ValidateStatus = (Int32)ENUM_TITO_IS_VALID_TICKET_MSG.VALID_CASHABLE;
      }
      else if (_request.ValidationNumberSequence == 8046487028153388)
      {
        _response.AmountCents = 760;
        _response.ValidateStatus = (Int32)ENUM_TITO_IS_VALID_TICKET_MSG.VALID_HANDPAY;
      }
      else if (_request.ValidationNumberSequence == 2632204901838668)
      {
        _response.AmountCents = 710;
        _response.ValidateStatus = (Int32)ENUM_TITO_IS_VALID_TICKET_MSG.VALID_JACKPOT;
      }
      else if (_request.ValidationNumberSequence == 7048315635528398)
      {
        _response.AmountCents = 60;
        _response.ValidateStatus = (Int32)ENUM_TITO_IS_VALID_TICKET_MSG.TICKET_OFFLINE;
      }
      else if (_request.ValidationNumberSequence == 0879354923467286)
      {
        _response.AmountCents = 4;
        _response.ValidateStatus = (Int32)ENUM_TITO_IS_VALID_TICKET_MSG.TICKET_EXPIRED;
      }
      else if (_request.ValidationNumberSequence == 1356932606725942)
      {
        _response.AmountCents = 1331;
        _response.ValidateStatus = (Int32)ENUM_TITO_IS_VALID_TICKET_MSG.TICKET_ALREADY_REDEEMED;
      }
      else if (_request.ValidationNumberSequence == 8369342764347488)
      {
        _response.AmountCents = 1700;
        _response.ValidateStatus = (Int32)ENUM_TITO_IS_VALID_TICKET_MSG.VALID_NONRESTRICTED_PROMOTIONAL_TICKET;
      }
      else 
      {
        WcpResponseMsg.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_INVALID_AMOUNT;
        WcpResponseMsg.MsgHeader.ResponseCodeText = "The Runnner not enough credit.";
      }

      ///////////////////////////////////////////////////////////////////
      ///// ONLY FOR TEST ///////////////////////////////////////////////
      ///////////////////////////////////////////////////////////////////

    } // ProcessCheckTicket


    //------------------------------------------------------------------------------
    // PURPOSE : Init wcp commands thread
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public static void ProcessPayTicket(SecureTcpClient TcpClient, WCP_Message WcpRequestMsg, WCP_Message WcpResponseMsg, SqlTransaction SqlTrx)
    {
    }

    #endregion

  } // class WCP_PayTicket
}
