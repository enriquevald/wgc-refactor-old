//------------------------------------------------------------------------------
// Copyright � 2010 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_StatisticsNew.cs
// 
//   DESCRIPTION: WCP_StatisticsNew class
// 
//        AUTHOR: Alberto Cuesta
// 
// CREATION DATE: 25-JUN-2010
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 25-JUN-2010 ACC    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Data.SqlClient;
using WSI.Common;
using System.Data;
using System.Timers;

namespace WSI.WCP
{
  public static class WCP_StatisticsNew
  {

    //------------------------------------------------------------------------------
    // PURPOSE : Reloads the statistics per hour from game meters (Sas Host Terminals)
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    static public void StatisticsNewThread()
    {
      SqlConnection   _sql_conn;
      SqlTransaction  _sql_trx;
      Random _rnd;

      _rnd = new Random(Environment.TickCount);

      while (true)
      {
        // Wait 1 minute
        System.Threading.Thread.Sleep(60000 + _rnd.Next(30000)); 

        _sql_conn = null;
        _sql_trx = null;

        try
        {
          //
          // Connect to DB
          //
          _sql_conn = WGDB.Connection();

          if (_sql_conn == null)
          {
            continue;
          }

          // Check connection states
          if (_sql_conn.State != ConnectionState.Open)
          {
            if (_sql_conn.State != ConnectionState.Broken)
            {
              _sql_conn.Close();
            }

            _sql_conn.Dispose();
            _sql_conn = null;

            continue;
          }

          _sql_trx = _sql_conn.BeginTransaction();

          //
          // Update SalesPerHour From Plays
          //
          if (WCP_StatisticsNew.UpdateSalesPerHourFromPlays(_sql_trx))
          {
            _sql_trx.Commit();
          }

        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }
        finally
        {
          if (_sql_trx != null)
          {
            if (_sql_trx.Connection != null)
            {
              _sql_trx.Rollback();
            }
            _sql_trx = null;
          }

          if (_sql_conn != null)
          {
            _sql_conn.Close();
            _sql_conn = null;
          }
        }

      } // while

    } // StatisticsNewThread

    //------------------------------------------------------------------------------
    // PURPOSE : Update sales per hour from plays. Maximum updates 5 hours each time.
    //
    //  PARAMS :
    //      - INPUT :
    //          - Trx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    static private Boolean UpdateSalesPerHourFromPlays(SqlTransaction Trx)
    {
      SqlCommand _sql_command;
      StringBuilder _sql_txt;
      DateTime _base_hour;
      Int32 _idx_update;
      Int32 _num_updates;
      Object _obj;

      _base_hour = DateTime.MinValue;
      try
      {
        _sql_txt = new StringBuilder();
        _sql_txt.AppendLine("SELECT MAX(SPH_BASE_HOUR) ");
        _sql_txt.AppendLine("FROM   SANTI ");

        _sql_command = new SqlCommand(_sql_txt.ToString());
        _sql_command.Connection = Trx.Connection;
        _sql_command.Transaction = Trx;

        _obj = _sql_command.ExecuteScalar();
        if (_obj != null)
        {
          _base_hour = (DateTime)_obj;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        // Nothing (_base_hour = DateTime.MinValue)
      }

      if ( _base_hour == DateTime.MinValue )
      {
        try
        {
          _sql_txt = new StringBuilder();
          _sql_txt.AppendLine("SELECT MIN(PL_DATETIME) ");
          _sql_txt.AppendLine("FROM   PLAYS ");

          _sql_command = new SqlCommand(_sql_txt.ToString());
          _sql_command.Connection = Trx.Connection;
          _sql_command.Transaction = Trx;

          _obj = _sql_command.ExecuteScalar();
          if (_obj != null)
          {
            _base_hour = (DateTime)_obj;
          }
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);

          // Nothing (_base_hour = DateTime.MinValue)
        }
      }

      if ( _base_hour == DateTime.MinValue )
      {
        _base_hour = DateTime.Now;
      }

      // Reset minutes, seconds and milliseconds
      _base_hour = _base_hour.AddMinutes(-_base_hour.Minute);
      _base_hour = _base_hour.AddSeconds(-_base_hour.Second);
      _base_hour = _base_hour.AddMilliseconds(-_base_hour.Millisecond);

      // Rest 1 hour
      _base_hour = _base_hour.AddHours(-1);

      _idx_update = 0;
      while ( _idx_update < 12 )
      {
        if ( !UpdateBaseHourFromPlays(_base_hour, Trx, out _num_updates) )
        {
          return false;
        }

        _base_hour = _base_hour.AddHours(1);
        if ( _num_updates > 0 )
        {
          _idx_update++;
        }

        if ( _base_hour.CompareTo(DateTime.Now) > 0 )
        {
          break;
        }
      }

      return true;

    } // UpdateSalesPerHourFromPlays

    //------------------------------------------------------------------------------
    // PURPOSE : Select plays and update sales per hour for one base hour
    //
    //  PARAMS :
    //      - INPUT :
    //          - BaseHour
    //          - Trx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    static private Boolean UpdateBaseHourFromPlays(DateTime BaseHour, SqlTransaction Trx, out Int32 NumUpdates)
    {
      SqlCommand      _sql_command;
      StringBuilder   _sql_txt;
      SqlDataAdapter  _da;
      DataTable       _dt_accumulate_plays;
      Int32           _update_batch_size = 500;
      Int32           _nr;
      Int64           _from_play_id;
      String          _base_date;
      Int32           _idx_row;
      Int32           _num_active_terminals;
      Object          _obj;

      int _tick0;
      int _tick1;
      int _tick2;
      long _interval_select;
      long _interval_update_SPH;
      long _interval_insert_SPH;
      long _interval_all_process;

      NumUpdates = 0;

      _interval_select = 0;
      _interval_update_SPH = 0;
      _interval_insert_SPH = 0;
      _interval_all_process = 0;
      _tick0 = Environment.TickCount;

      _base_date = "'01/01/2007 00:00:00'";

      // Get Active Terminals
      _num_active_terminals = 0;
      try
      {
        _sql_txt = new StringBuilder();
        _sql_txt.AppendLine("SELECT COUNT(*) FROM TERMINALS WHERE TE_ACTIVE = 1 ");

        _sql_command = new SqlCommand(_sql_txt.ToString());
        _sql_command.Connection = Trx.Connection;
        _sql_command.Transaction = Trx;

        _num_active_terminals = (Int32)_sql_command.ExecuteScalar();
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      // Get Active Terminals
      _from_play_id = 0;
      try
      {
        _sql_txt = new StringBuilder();
        _sql_txt.AppendLine("SELECT MAX(SPH_LAST_PLAY_ID) ");
        _sql_txt.AppendLine("FROM   SANTI ");
        _sql_txt.AppendLine("WHERE  SPH_BASE_HOUR < @p1 AND SPH_BASE_HOUR >= @p2 ");

        _sql_command = new SqlCommand(_sql_txt.ToString());
        _sql_command.Connection = Trx.Connection;
        _sql_command.Transaction = Trx;

        _sql_command.Parameters.Add("@p1", SqlDbType.DateTime, 8, "SPH_BASE_HOUR").Value = BaseHour.AddHours(-2);
        _sql_command.Parameters.Add("@p2", SqlDbType.DateTime, 8, "SPH_BASE_HOUR").Value = BaseHour.AddDays(-10);

        _obj = _sql_command.ExecuteScalar();
        if (_obj != null)
        {
          _from_play_id = (Int64)_obj;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        // Nothing (play_id = 0)
      }

      _dt_accumulate_plays = null;

      try
      {
        //    - Set SqlDataAdapter
        _da = new SqlDataAdapter();

        _tick1 = Environment.TickCount;

        //
        // Select Plays
        //
        _sql_txt = new StringBuilder();
        _sql_txt.AppendLine("SELECT DATEADD (hour, DATEDIFF (hour, " + _base_date + ", PL_DATETIME), " + _base_date + ")  AS 'PLAYS_BASE_HOUR'");
        _sql_txt.AppendLine("     , PL_TERMINAL_ID                                      AS 'PLAYS_TERMINAL_ID'");
        _sql_txt.AppendLine("     , TE_NAME                                             AS 'PLAYS_TERMINAL_NAME'");
        _sql_txt.AppendLine("     , PL_GAME_ID                                          AS 'PLAYS_GAME_ID'");
        _sql_txt.AppendLine("     , GM_NAME                                             AS 'PLAYS_GAME_NAME'");
        _sql_txt.AppendLine("     , COUNT (*)                                           AS 'PLAYS_PLAYED_COUNT'");
        _sql_txt.AppendLine("     , SUM (CASE WHEN PL_WON_AMOUNT > 0 THEN 1 ELSE 0 END) AS 'PLAYS_WON_COUNT'");
        _sql_txt.AppendLine("     , SUM (PL_PLAYED_AMOUNT)                              AS 'PLAYS_PLAYED_AMOUNT'");
        _sql_txt.AppendLine("     , SUM (PL_WON_AMOUNT)                                 AS 'PLAYS_WON_AMOUNT'");
        _sql_txt.AppendLine("     , 0                                                   AS 'PLAYS_NUM_ACTIVE_TERMINALS'");
        _sql_txt.AppendLine("     , MAX (PL_PLAY_ID)                                    AS 'PLAYS_LAST_PLAY_ID'");
        _sql_txt.AppendLine("  FROM PLAYS");
        _sql_txt.AppendLine("     , TERMINALS");
        _sql_txt.AppendLine("     , GAMES");
        _sql_txt.AppendLine(" WHERE PL_DATETIME       >= @p1");
        _sql_txt.AppendLine("   AND PL_DATETIME       <  @p2");
        _sql_txt.AppendLine("   AND PL_PLAY_ID        >  @p3");
        _sql_txt.AppendLine("   AND PL_TERMINAL_ID    = TE_TERMINAL_ID");
        _sql_txt.AppendLine("   AND TE_TERMINAL_TYPE  <> " + ((Int16)TerminalTypes.SAS_HOST).ToString());  
        _sql_txt.AppendLine("   AND PL_GAME_ID        = GM_GAME_ID");
        _sql_txt.AppendLine(" GROUP BY DATEADD (hour, DATEDIFF (hour, " + _base_date + ", PL_DATETIME), " + _base_date + ")");
        _sql_txt.AppendLine("     , PL_TERMINAL_ID, TE_NAME ");
        _sql_txt.AppendLine("     , PL_GAME_ID, GM_NAME ");
        _sql_txt.AppendLine(" ORDER BY PLAYS_BASE_HOUR");

        _da.SelectCommand = new SqlCommand(_sql_txt.ToString());
        _da.SelectCommand.Connection = Trx.Connection;
        _da.SelectCommand.Transaction = Trx;

        // Set Command timeout to 60 seconds
        _da.SelectCommand.CommandTimeout = 60;

        _da.SelectCommand.Parameters.Add("@p1", SqlDbType.DateTime, 8, "PL_DATETIME").Value = BaseHour;
        _da.SelectCommand.Parameters.Add("@p2", SqlDbType.DateTime, 8, "PL_DATETIME").Value = BaseHour.AddHours(1);
        _da.SelectCommand.Parameters.Add("@p3", SqlDbType.BigInt, 8, "PL_PLAY_ID").Value = _from_play_id;

        //        - UpdateCommand, DeleteCommand & InsertCommand
        _da.DeleteCommand = null;
        _da.InsertCommand = null;
        _da.UpdateCommand = null;

        //    - Execute Select command
        _dt_accumulate_plays = new DataTable("PLAYS");
        _da.Fill(_dt_accumulate_plays);

        _tick2 = Environment.TickCount;
        _interval_select = Misc.GetElapsedTicks(_tick1, _tick2);

        _tick1 = Environment.TickCount;

        if (_dt_accumulate_plays.Rows.Count == 0)
        {
          return true;
        }

        for (_idx_row = 0; _idx_row < _dt_accumulate_plays.Rows.Count; _idx_row++)
        {
          _dt_accumulate_plays.Rows[_idx_row]["PLAYS_NUM_ACTIVE_TERMINALS"] = _num_active_terminals;
          _dt_accumulate_plays.Rows[_idx_row].AcceptChanges();
          _dt_accumulate_plays.Rows[_idx_row].SetModified();
        }

        // When batch updates are enabled (UpdateBatchSize != 1), the UpdatedRowSource property value 
        // of the DataAdapter's UpdateCommand, InsertCommand, and DeleteCommand should be set to None 
        // or OutputParameters. 
        _da.UpdateBatchSize = _update_batch_size;

        //    - Set SqlDataAdapter commands
        //        - SelectCommand 
        //        - DeleteCommand 
        //        - InsertCommand 
        //        - UpdateCommand 
        //

        //        - UpdateCommand 
        _sql_txt = new StringBuilder();
        _sql_txt.AppendLine("UPDATE SANTI");
        _sql_txt.AppendLine("   SET SPH_PLAYED_COUNT         = @p1");
        _sql_txt.AppendLine("     , SPH_PLAYED_AMOUNT        = @p2");
        _sql_txt.AppendLine("     , SPH_WON_COUNT            = @p3");
        _sql_txt.AppendLine("     , SPH_WON_AMOUNT           = @p4");
        _sql_txt.AppendLine("     , SPH_NUM_ACTIVE_TERMINALS = @p5");
        _sql_txt.AppendLine("     , SPH_LAST_PLAY_ID         = @p6");
        _sql_txt.AppendLine(" WHERE SPH_BASE_HOUR   = @p7");
        _sql_txt.AppendLine("   AND SPH_TERMINAL_ID = @p8");
        _sql_txt.AppendLine("   AND SPH_GAME_ID     = @p9");

        _da.UpdateCommand = new SqlCommand(_sql_txt.ToString());
        _da.UpdateCommand.Connection = Trx.Connection;
        _da.UpdateCommand.Transaction = Trx;
        _da.UpdateCommand.UpdatedRowSource = UpdateRowSource.None;

        _da.UpdateCommand.Parameters.Add("@p1", SqlDbType.BigInt, 8, "PLAYS_PLAYED_COUNT");
        _da.UpdateCommand.Parameters.Add("@p2", SqlDbType.Money, 8, "PLAYS_PLAYED_AMOUNT");
        _da.UpdateCommand.Parameters.Add("@p3", SqlDbType.BigInt, 8, "PLAYS_WON_COUNT");
        _da.UpdateCommand.Parameters.Add("@p4", SqlDbType.Money, 8, "PLAYS_WON_AMOUNT");
        _da.UpdateCommand.Parameters.Add("@p5", SqlDbType.SmallInt, 2, "PLAYS_NUM_ACTIVE_TERMINALS");
        _da.UpdateCommand.Parameters.Add("@p6", SqlDbType.BigInt, 8, "PLAYS_LAST_PLAY_ID");
        _da.UpdateCommand.Parameters.Add("@p7", SqlDbType.DateTime, 8, "PLAYS_BASE_HOUR");
        _da.UpdateCommand.Parameters.Add("@p8", SqlDbType.Int, 4, "PLAYS_TERMINAL_ID");
        _da.UpdateCommand.Parameters.Add("@p9", SqlDbType.Int, 4, "PLAYS_GAME_ID");

        //        - DeleteCommand & SelectCommand & InsertCommand
        _da.DeleteCommand = null;
        _da.SelectCommand = null;
        _da.InsertCommand = null;

        //    - Execute Update command
        _da.UpdateBatchSize = _update_batch_size;
        _da.ContinueUpdateOnError = true;

        _nr = _da.Update(_dt_accumulate_plays);

        NumUpdates += _nr;

        _tick2 = Environment.TickCount;
        _interval_update_SPH = Misc.GetElapsedTicks(_tick1, _tick2);

        _tick1 = Environment.TickCount;

        if (_dt_accumulate_plays.Rows.Count == _nr)
        {
          return true;
        }

        foreach (DataRow _dr in _dt_accumulate_plays.Rows)
        {
          if (_dr.HasErrors)
          {
            _dr.ClearErrors();
            _dr.AcceptChanges();
            _dr.SetAdded();
          }
          else
          {
            _dr.AcceptChanges();
            if (_dr.RowState != DataRowState.Unchanged)
            {
              return false;
            }
          }
        }

        _da.UpdateCommand = null;

        // Insert
        _sql_txt = new StringBuilder();
        _sql_txt.AppendLine("INSERT INTO  SANTI ");
        _sql_txt.AppendLine("           ( SPH_BASE_HOUR ");
        _sql_txt.AppendLine("           , SPH_TERMINAL_ID ");
        _sql_txt.AppendLine("           , SPH_TERMINAL_NAME ");
        _sql_txt.AppendLine("           , SPH_GAME_ID ");
        _sql_txt.AppendLine("           , SPH_GAME_NAME ");
        _sql_txt.AppendLine("           , SPH_PLAYED_COUNT ");
        _sql_txt.AppendLine("           , SPH_PLAYED_AMOUNT ");
        _sql_txt.AppendLine("           , SPH_WON_COUNT ");
        _sql_txt.AppendLine("           , SPH_WON_AMOUNT ");
        _sql_txt.AppendLine("           , SPH_NUM_ACTIVE_TERMINALS ");
        _sql_txt.AppendLine("           , SPH_LAST_PLAY_ID) ");
        _sql_txt.AppendLine("VALUES     ( @p1, @p2, @p3, @p4, @p5, @p6, @p7, @p8, @p9, @p10, @p11) ");

        _da.InsertCommand = new SqlCommand(_sql_txt.ToString());
        _da.InsertCommand.Connection = Trx.Connection;
        _da.InsertCommand.Transaction = Trx;

        _da.InsertCommand.Parameters.Add("@p1", SqlDbType.DateTime).SourceColumn = "PLAYS_BASE_HOUR";
        _da.InsertCommand.Parameters.Add("@p2", SqlDbType.Int, 4, "PLAYS_TERMINAL_ID");
        _da.InsertCommand.Parameters.Add("@p3", SqlDbType.NVarChar, 50, "PLAYS_TERMINAL_NAME");
        _da.InsertCommand.Parameters.Add("@p4", SqlDbType.Int, 4, "PLAYS_GAME_ID");
        _da.InsertCommand.Parameters.Add("@p5", SqlDbType.NVarChar, 50, "PLAYS_GAME_NAME");
        _da.InsertCommand.Parameters.Add("@p6", SqlDbType.BigInt, 8, "PLAYS_PLAYED_COUNT");
        _da.InsertCommand.Parameters.Add("@p7", SqlDbType.Money, 8, "PLAYS_PLAYED_AMOUNT");
        _da.InsertCommand.Parameters.Add("@p8", SqlDbType.BigInt, 8, "PLAYS_WON_COUNT");
        _da.InsertCommand.Parameters.Add("@p9", SqlDbType.Money, 8, "PLAYS_WON_AMOUNT");
        _da.InsertCommand.Parameters.Add("@p10", SqlDbType.Int, 4, "PLAYS_NUM_ACTIVE_TERMINALS");
        _da.InsertCommand.Parameters.Add("@p11", SqlDbType.BigInt, 8, "PLAYS_LAST_PLAY_ID");

        _da.InsertCommand.UpdatedRowSource = UpdateRowSource.OutputParameters;
        _nr = _nr + _da.Update(_dt_accumulate_plays);

        NumUpdates += _nr;

        _tick2 = Environment.TickCount;
        _interval_insert_SPH = Misc.GetElapsedTicks(_tick1, _tick2);

        if (_dt_accumulate_plays.Rows.Count != _nr)
        {
          return false;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
      finally
      {
        _tick2 = Environment.TickCount;
        _interval_all_process = Misc.GetElapsedTicks(_tick0, _tick2);

        if (   _interval_select > 20000
            || _interval_update_SPH > 3000
            || _interval_insert_SPH > 3000
            || _interval_all_process > 25000)
        {
          Log.Warning("Thread StatisticsNew times: Select/UpdateSPH/InsertSPH/Total = " + _interval_select.ToString() + "/" + _interval_update_SPH.ToString() + "/" + _interval_insert_SPH.ToString() + "/" + _interval_all_process.ToString());
        }
      }

    } // UpdateBaseHourFromPlays

  }
}
