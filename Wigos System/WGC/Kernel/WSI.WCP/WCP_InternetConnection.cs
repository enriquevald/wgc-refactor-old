﻿//------------------------------------------------------------------------------
// Copyright © 2007-2009 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: InternetConnection.cs
// 
//   DESCRIPTION: Implements InternetConnection
//
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 13-NOV-2017 DHA     First release.
// 13-NOV-2017 DHA     Bug 30775:WIGOS-6110 [Ticket #9672] MTY I - Plaza Real - PinPad con error y autorizadas
//------------------------------------------------------------------------------



using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using WSI.Common;

namespace WSI.WCP
{
  public class WCP_InternetConnection
  {
    #region Attributes
    private static Thread m_thread;
    #endregion Attributes

    #region Public Methods
    //------------------------------------------------------------------------------
    // PURPOSE : Initialize WCP_BucketsExpirationDayMonth class 
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    public static void Init()
    {
      m_thread = new Thread(CheckInternetConnection);
      m_thread.Name = "CheckInternetConnection";
    } // Init

    //------------------------------------------------------------------------------
    // PURPOSE : Public function for thread starts
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :

    public static void Start()
    {
      // Thread starts
      m_thread.Start();
    }
    #endregion Public Methods

    #region Private Methods
    //------------------------------------------------------------------------------
    // PURPOSE : Main thread
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    private static void CheckInternetConnection()
    {
      Int32 _wait_hint;
      DateTime _now;
      Boolean _has_internet_connection;

      while (true)
      {
        _has_internet_connection = false;

        _now = WGDB.Now;
        _wait_hint = 60 * 1000; // check every minute

        Thread.Sleep(_wait_hint);

        // Only the service running as 'Principal' will excute the tasks.
        if (!Services.IsPrincipal("WCP"))
        {
          continue;
        }

        try
        {

          _has_internet_connection = false;
          _has_internet_connection = InternetConnection.CheckForInternetConnection();          

        }
        catch (Exception _ex)
        {
          Log.Warning("WCP_InternetConnection. Exception in function: CheckInternetConnection");
          Log.Exception(_ex);
        }
        finally
        {
          InternetConnection.UpdateServiceInternetConnectionStatus("WCP", _has_internet_connection);
        }
      }
    } // CheckInternetConnection

   
    #endregion Private Methods
  }
}
