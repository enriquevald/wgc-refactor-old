//------------------------------------------------------------------------------
// Copyright � 2014 Win Systems Ltd.
//------------------------------------------------------------------------------
//  
//   MODULE NAME: WCP_TerminalStatus.cs
// 
//   DESCRIPTION: WCP_TerminalStatus class Sets differents alerts 
// 
//        AUTHOR: David Rigal Vall
//  
// CREATION DATE: 15-JAN-2014
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 15-JAN-2014 DRV    Initial version
// 23-JAN-2014 DRV    Added new alerts (Plays per second, cents per second, jackpot, won amount and current payout)
// 26-MAR-2014 DRV    Code Refactoring 
// 23-APR-2014 AMF    Custom Alarms
// 05-MAY-2014 AMF    Fixed Bug WIG-876: Generate custom alarm
// 07-MAY-2014 AMF    Fixed Bug WIG-876: Calculate count correctly
// 08-MAY-2014 AMF    Terminals without plays
// 05-JUN-2014 MPO    WIG-1012: The base hour of SPH should add a hour  
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using WSI.Common;
using System.Data;

namespace WSI.WCP
{

  public static class WCP_TerminalStatus
  {

    #region Enums

    private enum RESOURCE_NAME_TYPE
    {
      NONE = 0,
      AMOUNT = 1,
      COUNT = 2,
      AMOUNT_COUNT = 3,
    }

    #endregion // Enums

    #region Attributes

    private static DataTable m_terminal_status_flags = null;
    private static SqlDataAdapter m_sql_data_adap = null;

    private static Int32 m_interval_value_counterfeit;
    private static Int32 m_interval_value_played;
    private static Int32 m_interval_value_won;
    private static Int32 m_interval_value_payout;
    private static Int32 m_interval_value_jackpot;
    private static Int32 m_interval_value_canceled_credit;
    private static Int32 m_interval_value_terminals_whitout_plays;
    private static Int32 m_interval_value_terminals_whitout_plays_range;

    private static Int32 m_interval_unit_counterfeit;
    private static Int32 m_interval_unit_played;
    private static Int32 m_interval_unit_won;
    private static Int32 m_interval_unit_payout;
    private static Int32 m_interval_unit_jackpot;
    private static Int32 m_interval_unit_canceled_credit;
    private static Int32 m_interval_unit_terminals_without_plays;

    private static DateTime m_now;
    private static DateTime m_now_hour;

    #endregion // Attributes

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Sets diferent alerts for each terminal and generates alarms if it's needed.
    //
    //  PARAMS :
    //      - INPUT : Trx
    //
    //      - OUTPUT :
    //
    // RETURNS : true  -> Everything ok 
    //           false -> An error occurred
    //
    public static Boolean Trx_UpdateTerminalStatus(SqlTransaction Trx)
    {
      Boolean _check_counterfeit;
      Boolean _check_played;
      Boolean _check_won;
      Boolean _check_payout;
      Boolean _check_jackpot;
      Boolean _check_canceled_credit;
      Boolean _check_terminals_without_plays;
      Boolean _get_from_tsmh;
      Boolean _get_from_handpay;

      Int32 _interval_value_max;
      Int32 _interval_unit_max;
      Int32 _interval_max;

      Int32 _interval_value_handpay;
      Int32 _interval_unit_handpay;

      StringBuilder _sb;
      SqlCommand _sql_cmd_select;
      SqlCommand _sql_cmd_update;

      try
      {
        m_now = WGDB.Now;
        m_now_hour = new DateTime(m_now.Year, m_now.Month, m_now.Day, m_now.Hour, 0, 0);

        _check_counterfeit = GeneralParam.GetBoolean("Alarms", "Counterfeit.Enabled", false);
        _check_played = GeneralParam.GetBoolean("Alarms", "Played.Enabled", false);
        _check_won = GeneralParam.GetBoolean("Alarms", "Won.Enabled", false);
        _check_payout = GeneralParam.GetBoolean("Alarms", "Payout.Enabled", false);
        _check_jackpot = GeneralParam.GetBoolean("Alarms", "Jackpot.Enabled", false);
        _check_canceled_credit = GeneralParam.GetBoolean("Alarms", "CanceledCredit.Enabled", false);
        _check_terminals_without_plays = GeneralParam.GetBoolean("Alarms", "MachineWithoutPlays.Enabled", false);

        _get_from_tsmh = _check_played || _check_won || _check_payout || _check_jackpot || _check_canceled_credit;
        _get_from_handpay = _check_jackpot || _check_canceled_credit;

        if (!(_check_counterfeit || _get_from_tsmh || _check_terminals_without_plays))
        {
          return true;
        }

        m_interval_value_counterfeit = GeneralParam.GetInt32("Alarms", "Counterfeit.PeriodUnderStudy.Value", 0);
        m_interval_value_played = GeneralParam.GetInt32("Alarms", "Played.PeriodUnderStudy.Value", 0);
        m_interval_value_won = GeneralParam.GetInt32("Alarms", "Won.PeriodUnderStudy.Value", 0);
        m_interval_value_payout = GeneralParam.GetInt32("Alarms", "Payout.PeriodUnderStudy.Value", 0);
        m_interval_value_jackpot = GeneralParam.GetInt32("Alarms", "Jackpot.PeriodUnderStudy.Value", 0);
        m_interval_value_canceled_credit = GeneralParam.GetInt32("Alarms", "CanceledCredit.PeriodUnderStudy.Value", 0);
        m_interval_value_terminals_whitout_plays = GeneralParam.GetInt32("Alarms", "MachineWithoutPlays.PeriodUnderStudy.Value", 0);

        m_interval_unit_counterfeit = GeneralParam.GetInt32("Alarms", "Counterfeit.PeriodUnderStudy.Unit", 0, 0, 2);
        m_interval_unit_played = GeneralParam.GetInt32("Alarms", "Played.PeriodUnderStudy.Unit", 0, 0, 2);
        m_interval_unit_won = GeneralParam.GetInt32("Alarms", "Won.PeriodUnderStudy.Unit", 0, 0, 2);
        m_interval_unit_payout = GeneralParam.GetInt32("Alarms", "Payout.PeriodUnderStudy.Unit", 0, 0, 2);
        m_interval_unit_jackpot = GeneralParam.GetInt32("Alarms", "Jackpot.PeriodUnderStudy.Unit", 0, 0, 2);
        m_interval_unit_canceled_credit = GeneralParam.GetInt32("Alarms", "CanceledCredit.PeriodUnderStudy.Unit", 0, 0, 2);
        m_interval_unit_terminals_without_plays = GeneralParam.GetInt32("Alarms", "MachineWithoutPlays.PeriodUnderStudy.Unit", 0, 0, 2);
        if (m_interval_unit_terminals_without_plays == 1)
        {
          m_interval_value_terminals_whitout_plays_range = 8 * 24;
        }
        else
        {
          m_interval_value_terminals_whitout_plays_range = 8;
        }

        //Calculate Max Interval
        // Played
        _interval_max = GetPeriodToHours(m_interval_value_played, m_interval_unit_played);
        _interval_value_max = m_interval_value_played;
        _interval_unit_max = m_interval_unit_played;
        // Won
        if (_interval_max < GetPeriodToHours(m_interval_value_won, m_interval_unit_won))
        {
          _interval_max = GetPeriodToHours(m_interval_value_won, m_interval_unit_won);
          _interval_value_max = m_interval_value_won;
          _interval_unit_max = m_interval_unit_won;
        }
        // Payout
        if (_interval_max < GetPeriodToHours(m_interval_value_payout, m_interval_unit_payout))
        {
          _interval_max = GetPeriodToHours(m_interval_value_payout, m_interval_unit_payout);
          _interval_value_max = m_interval_value_payout;
          _interval_unit_max = m_interval_unit_payout;
        }
        // Jackpot
        if (_interval_max < GetPeriodToHours(m_interval_value_jackpot, m_interval_unit_jackpot))
        {
          _interval_max = GetPeriodToHours(m_interval_value_jackpot, m_interval_unit_jackpot);
          _interval_value_max = m_interval_value_jackpot;
          _interval_unit_max = m_interval_unit_jackpot;
        }
        // Canceled Credit
        if (_interval_max < GetPeriodToHours(m_interval_value_canceled_credit, m_interval_unit_canceled_credit))
        {
          _interval_max = GetPeriodToHours(m_interval_value_canceled_credit, m_interval_unit_canceled_credit);
          _interval_value_max = m_interval_value_canceled_credit;
          _interval_unit_max = m_interval_unit_canceled_credit;
        }

        //Calcula Handpay Interval
        if (GetPeriodToHours(m_interval_value_jackpot, m_interval_unit_jackpot) > GetPeriodToHours(m_interval_value_canceled_credit, m_interval_unit_canceled_credit))
        {
          _interval_value_handpay = m_interval_value_jackpot;
          _interval_unit_handpay = m_interval_unit_jackpot;
        }
        else
        {
          _interval_value_handpay = m_interval_value_canceled_credit;
          _interval_unit_handpay = m_interval_unit_canceled_credit;
        }

        if (m_terminal_status_flags == null)
        {
          InitializeTerminalStatusFlags();
        }

        m_terminal_status_flags.Clear();

        _sb = new StringBuilder();
        _sb.AppendLine("   SELECT   TE_TERMINAL_ID AS TERMINAL_ID ");
        _sb.AppendLine("          , ISNULL(TS_BILL_FLAGS,0) AS TS_BILL_FLAGS ");
        _sb.AppendLine("          , ISNULL(TS_PLAYED_WON_FLAGS,0) AS TS_PLAYED_WON_FLAGS ");
        _sb.AppendLine("          , ISNULL(TS_JACKPOT_FLAGS,0) AS TS_JACKPOT_FLAGS ");
        _sb.AppendLine("          , TE_NAME ");
        _sb.AppendLine("          , TE_PROVIDER_ID ");

        if (_check_counterfeit)
        {
          _sb.AppendLine("          , A1.COUNTERFEIT_EXCEEDED ");
          _sb.AppendLine("          , TS_COUNTERFEIT_ALARM_ID ");
        }

        if (_check_played)
        {
          _sb.AppendLine("          , A2.PLAYED_COUNT ");
          _sb.AppendLine("          , A2.PLAYED_AMOUNT ");
          _sb.AppendLine("          , TS_PLAYED_ALARM_ID ");
        }

        if (_check_won)
        {
          _sb.AppendLine("          , A2.WON_COUNT ");
          _sb.AppendLine("          , A2.WON_AMOUNT ");
          _sb.AppendLine("          , TS_WON_ALARM_ID ");

        }

        if (_check_payout)
        {
          _sb.AppendLine("          , ISNULL(TE_THEORETICAL_PAYOUT, @pGPPayOut) AS THEORETICAL_PAYOUT ");
          _sb.AppendLine("          , NULL AS CURRENT_PAYOUT ");
          _sb.AppendLine("          , A2.CURRENT_PAYOUT_PLAYED ");
          _sb.AppendLine("          , A2.CURRENT_PAYOUT_WON ");
          _sb.AppendLine("          , TS_CURRENT_PAYOUT_ALARM_ID ");
        }

        if (_check_jackpot)
        {
          _sb.AppendLine("          , A3.JACKPOT_COUNT ");
          _sb.AppendLine("          , A2.JACKPOT_AMOUNT ");
          _sb.AppendLine("          , TS_JACKPOT_ALARM_ID ");
        }

        if (_check_canceled_credit)
        {
          _sb.AppendLine("          , A3.CANCELED_CREDIT_COUNT ");
          _sb.AppendLine("          , A2.CANCELED_CREDIT_AMOUNT ");
          _sb.AppendLine("          , TS_CANCELED_CREDIT_ALARM_ID ");
        }

        if (_check_terminals_without_plays)
        {
          _sb.AppendLine("          , A4.SPH ");
          _sb.AppendLine("          , TS_WITHOUT_PLAYS_ALARM_ID ");
        }

        _sb.AppendLine("       FROM   TERMINALS ");
        _sb.AppendLine("  LEFT JOIN   TERMINAL_STATUS ");
        _sb.AppendLine("         ON   TE_TERMINAL_ID = TS_TERMINAL_ID ");

        if (_check_counterfeit)
        {
          _sb.AppendLine("LEFT JOIN ( ");
          _sb.AppendLine("            SELECT   AL_SOURCE_ID AS TERMINAL_ID ");
          _sb.AppendLine("                   , COUNT(1)     AS COUNTERFEIT_EXCEEDED ");
          _sb.AppendLine("              FROM   ALARMS ");
          _sb.AppendLine("             WHERE   AL_ALARM_CODE  = @pAlarmCode ");
          _sb.AppendLine("               AND   AL_SEVERITY    = @pAlarmSeverity ");
          _sb.AppendLine("               AND   AL_SOURCE_CODE = @pAlarmSourceCode ");
          _sb.AppendLine("               AND   AL_DATETIME >= DATEADD(#UNITCOUNTERFEIT#, -@pIntervalCounterfeit, @NowHour) ");
          _sb.AppendLine("          GROUP BY   AL_SOURCE_ID ");
          _sb.AppendLine("          ) AS A1 ON A1.TERMINAL_ID = TE_TERMINAL_ID ");
          _sb.Replace("#UNITCOUNTERFEIT#", GetPeriodUnitString(m_interval_unit_counterfeit));
        }

        if (_get_from_tsmh)
        {
          _sb.AppendLine("LEFT JOIN ( ");
          _sb.AppendLine("            SELECT   TSMH_TERMINAL_ID AS TERMINAL_ID ");

          if (_check_played)
          {
            _sb.AppendLine("                 , SUM(CASE WHEN TSMH_METER_CODE = 5 AND TSMH_DATETIME >= DATEADD(#UNITPLAYED#, -@pIntervalPlayed, @NowHour) THEN TSMH_METER_INCREMENT ELSE 0 END) AS PLAYED_COUNT ");
            _sb.AppendLine("                 , CAST(SUM(CASE WHEN TSMH_METER_CODE = 0 AND TSMH_DATETIME >= DATEADD(#UNITPLAYED#, -@pIntervalPlayed, @NowHour) THEN TSMH_METER_INCREMENT ELSE 0 END)/100 AS DECIMAL(18,2)) AS PLAYED_AMOUNT ");
            _sb.Replace("#UNITPLAYED#", GetPeriodUnitString(m_interval_unit_played));
          }

          if (_check_won)
          {
            _sb.AppendLine("                 , SUM(CASE WHEN TSMH_METER_CODE = 6 AND TSMH_DATETIME >= DATEADD(#UNITWON#, -@pIntervalWon, @NowHour) THEN TSMH_METER_INCREMENT ELSE 0 END) AS WON_COUNT ");
            _sb.AppendLine("                 , CAST(SUM(CASE WHEN TSMH_METER_CODE = 1 AND TSMH_DATETIME >= DATEADD(#UNITWON#, -@pIntervalWon, @NowHour) THEN TSMH_METER_INCREMENT ELSE 0 END)/100 AS DECIMAL(18,2)) AS WON_AMOUNT ");
            _sb.Replace("#UNITWON#", GetPeriodUnitString(m_interval_unit_won));
          }

          if (_check_payout)
          {
            _sb.AppendLine("                 , CAST(SUM(CASE WHEN TSMH_METER_CODE = 0 AND TSMH_DATETIME >= DATEADD(#UNITPAYOUT#, -@pIntervalPayout, @NowHour) THEN TSMH_METER_INCREMENT ELSE 0 END)/100 AS DECIMAL(18,2)) AS CURRENT_PAYOUT_PLAYED ");
            _sb.AppendLine("                 , CAST(SUM(CASE WHEN TSMH_METER_CODE = 1 AND TSMH_DATETIME >= DATEADD(#UNITPAYOUT#, -@pIntervalPayout, @NowHour) THEN TSMH_METER_INCREMENT ELSE 0 END)/100 AS DECIMAL(18,2)) AS CURRENT_PAYOUT_WON ");
            _sb.Replace("#UNITPAYOUT#", GetPeriodUnitString(m_interval_unit_payout));
          }

          if (_check_jackpot)
          {
            _sb.AppendLine("                 , CAST(SUM(CASE WHEN TSMH_METER_CODE = 2 AND TSMH_DATETIME >= DATEADD(#UNITJACKPOT#, -@pIntervalJackpot, @NowHour) THEN TSMH_METER_INCREMENT ELSE 0 END)/100 AS DECIMAL(18,2)) AS JACKPOT_AMOUNT ");
            _sb.Replace("#UNITJACKPOT#", GetPeriodUnitString(m_interval_unit_jackpot));
          }

          if (_check_canceled_credit)
          {
            _sb.AppendLine("                 , CAST(SUM(CASE WHEN TSMH_METER_CODE = 3 AND TSMH_DATETIME >= DATEADD(#UNITCANCELEDCREDIT#, -@pIntervalCanceledCredit, @NowHour) THEN TSMH_METER_INCREMENT ELSE 0 END)/100 AS DECIMAL(18,2)) AS CANCELED_CREDIT_AMOUNT ");
            _sb.Replace("#UNITCANCELEDCREDIT#", GetPeriodUnitString(m_interval_unit_canceled_credit));
          }

          _sb.AppendLine("              FROM   TERMINAL_SAS_METERS_HISTORY ");
          _sb.AppendLine("             WHERE   TSMH_METER_CODE IN (0,1,2,3,5,6) ");
          _sb.AppendLine("               AND   TSMH_TYPE = @pTsmhType ");
          _sb.AppendLine("               AND   TSMH_DATETIME >= DATEADD(#UNIT#, -@pInterval, @NowHour) ");
          _sb.AppendLine("          GROUP BY   TSMH_TERMINAL_ID ");
          _sb.AppendLine("          ) AS A2 ON A2.TERMINAL_ID = TE_TERMINAL_ID ");
          _sb.Replace("#UNIT#", GetPeriodUnitString(_interval_unit_max));
        }

        if (_get_from_handpay)
        {
          _sb.AppendLine("LEFT JOIN ( ");
          _sb.AppendLine("            SELECT   HP_TERMINAL_ID AS TERMINAL_ID ");

          if (_check_jackpot)
          {
            _sb.AppendLine("                 , SUM(CASE WHEN HP_TYPE = 1 AND HP_DATETIME >= DATEADD(#UNITJACKPOT#, -@pIntervalJackpot, @NowHour) THEN 1 ELSE 0 END) AS JACKPOT_COUNT ");
            _sb.Replace("#UNITJACKPOT#", GetPeriodUnitString(m_interval_unit_jackpot));
          }

          if (_check_canceled_credit)
          {
            _sb.AppendLine("                 , SUM(CASE WHEN HP_TYPE = 0 AND HP_DATETIME >= DATEADD(#UNITCANCELEDCREDIT#, -@pIntervalCanceledCredit, @NowHour) THEN 1 ELSE 0 END) AS CANCELED_CREDIT_COUNT");
            _sb.Replace("#UNITCANCELEDCREDIT#", GetPeriodUnitString(m_interval_unit_canceled_credit));
          }

          _sb.AppendLine("              FROM   HANDPAYS ");
          _sb.AppendLine("WHERE   HP_TYPE IN (0,1) ");
          _sb.AppendLine("               AND   HP_DATETIME >= DATEADD(#UNITHANDPAY#, -@pIntervalHandpay, @NowHour) ");
          _sb.AppendLine("          GROUP BY   HP_TERMINAL_ID ");
          _sb.AppendLine("          ) AS A3 ON A3.TERMINAL_ID = TE_TERMINAL_ID ");
          _sb.Replace("#UNITHANDPAY#", GetPeriodUnitString(_interval_unit_handpay));
        }

        if (_check_terminals_without_plays)
        {
          _sb.AppendLine("LEFT JOIN ( ");
          _sb.AppendLine("            SELECT   SPH_TERMINAL_ID AS TERMINAL_ID ");
          _sb.AppendLine("                   , DATEDIFF (HOUR, MAX(SPH_BASE_HOUR), @NowHour) AS SPH ");
          _sb.AppendLine("              FROM   SALES_PER_HOUR WITH (INDEX (PK_SALES_PER_HOUR)) ");
          _sb.AppendLine("             WHERE   SPH_BASE_HOUR >= DATEADD(#UNITWITHOUTPLAYS#, -(@pIntervalWithoutPlays + @pIntervalWithoutPlaysRange), @NowHour) ");
          _sb.AppendLine("          GROUP BY   SPH_TERMINAL_ID ");
          _sb.AppendLine("          ) AS A4 ON A4.TERMINAL_ID = TE_TERMINAL_ID ");
          _sb.Replace("#UNITWITHOUTPLAYS#", GetPeriodUnitString(m_interval_unit_terminals_without_plays));
        }

        _sb.AppendLine("   WHERE   TE_TERMINAL_TYPE IN (" + WSI.Common.Misc.GamingTerminalTypeListToString() + ") ");
        _sb.AppendLine("     AND   TE_TYPE = 1 ");
        _sb.AppendLine("     AND   ISNULL(TE_SERVER_ID, 0) = 0 ");
        _sb.AppendLine("     AND   TE_BLOCKED = 0 ");
        _sb.AppendLine("     AND   TE_STATUS  = @pTerminalActiveStatus ");

        //JackpotCode = 2; Amount
        //CanceledCreditCode = 3; Amount
        //GamesPlayedCount = 5;
        //GamesPlayedAmount = 0;
        //GamesWonCount = 6;
        //GamesWonAmount = 1;

        _sql_cmd_select = new SqlCommand(_sb.ToString());
        _sql_cmd_select.Parameters.Add("@pGPPayOut", SqlDbType.Decimal).Value = GeneralParam.GetDecimal("PlayerTracking", "TerminalDefaultPayout");
        _sql_cmd_select.Parameters.Add("@pAlarmCode", SqlDbType.Int).Value = 65580;
        _sql_cmd_select.Parameters.Add("@pAlarmSeverity", SqlDbType.Int).Value = (Int32)AlarmSeverity.Info;
        _sql_cmd_select.Parameters.Add("@pAlarmSourceCode", SqlDbType.Int).Value = (Int32)AlarmSourceCode.TerminalSASMachine;
        _sql_cmd_select.Parameters.Add("@NowHour", SqlDbType.DateTime).Value = m_now_hour;
        _sql_cmd_select.Parameters.Add("@pIntervalCounterfeit", SqlDbType.Int).Value = m_interval_value_counterfeit;
        _sql_cmd_select.Parameters.Add("@pIntervalPlayed", SqlDbType.Int).Value = m_interval_value_played;
        _sql_cmd_select.Parameters.Add("@pIntervalWon", SqlDbType.Int).Value = m_interval_value_won;
        _sql_cmd_select.Parameters.Add("@pIntervalPayout", SqlDbType.Int).Value = m_interval_value_payout;
        _sql_cmd_select.Parameters.Add("@pIntervalJackpot", SqlDbType.Int).Value = m_interval_value_jackpot;
        _sql_cmd_select.Parameters.Add("@pIntervalCanceledCredit", SqlDbType.Int).Value = m_interval_value_canceled_credit;
        _sql_cmd_select.Parameters.Add("@pIntervalWithoutPlays", SqlDbType.Int).Value = m_interval_value_terminals_whitout_plays;
        _sql_cmd_select.Parameters.Add("@pIntervalWithoutPlaysRange", SqlDbType.Int).Value = m_interval_value_terminals_whitout_plays_range;
        _sql_cmd_select.Parameters.Add("@pTsmhType", SqlDbType.Int).Value = (Int32)ENUM_SAS_METER_HISTORY.TSMH_HOURLY;
        _sql_cmd_select.Parameters.Add("@pInterval", SqlDbType.Int).Value = _interval_value_max;
        _sql_cmd_select.Parameters.Add("@pIntervalHandpay", SqlDbType.Int).Value = _interval_value_handpay;
        _sql_cmd_select.Parameters.Add("@pTerminalActiveStatus", SqlDbType.Int).Value = TerminalStatus.ACTIVE;

        m_sql_data_adap = new SqlDataAdapter();
        m_sql_data_adap.SelectCommand = _sql_cmd_select;
        m_sql_data_adap.InsertCommand = null;
        m_sql_data_adap.UpdateCommand = null;
        m_sql_data_adap.DeleteCommand = null;

        m_sql_data_adap.SelectCommand.Connection = Trx.Connection;
        m_sql_data_adap.SelectCommand.Transaction = Trx;

        m_sql_data_adap.Fill(m_terminal_status_flags);

        foreach (DataRow _row in m_terminal_status_flags.Rows)
        {
          //Generate Counterfeit Alert and Alarm
          if (_check_counterfeit)
          {
            if (!Trx_CounterfeitAlarm(_row, Trx))
            {
              return false;
            }
          }

          //Generate Played and Alarm
          if (_check_played)
          {
            if (!Trx_PlayedAlarm(_row, Trx))
            {
              return false;
            }
          }

          //Generate Won Alert and Alarm
          if (_check_won)
          {
            if (!Trx_WonAlarm(_row, Trx))
            {
              return false;
            }
          }

          //Generate Jackpot Alert
          if (_check_jackpot)
          {
            if (!Trx_JackpotAlarm(_row, Trx))
            {
              return false;
            }
          }

          //Generate Canceled Credit Alert
          if (_check_canceled_credit)
          {
            if (!Trx_CanceledCreditAlarm(_row, Trx))
            {
              return false;
            }
          }

          //Generate Payout Alert and Alarm
          if (_check_payout)
          {
            if (!Trx_PayoutAlarm(_row, Trx))
            {
              return false;
            }
          }

          //Generate Terminals without plays Alert
          if (_check_terminals_without_plays)
          {
            if (!Trx_WithoutPlaysAlarm(_row, Trx))
            {
              return false;
            }
          }
        }

        _sb.Length = 0;

        _sb.AppendLine(" IF EXISTS (  SELECT   1                                             ");
        _sb.AppendLine("                FROM   TERMINAL_STATUS                               ");
        _sb.AppendLine("               WHERE   TS_TERMINAL_ID = @pTerminal                   ");
        _sb.AppendLine("           )                                                         ");
        _sb.AppendLine("      UPDATE   TERMINAL_STATUS                                       ");
        _sb.AppendLine("         SET   TS_BILL_FLAGS               = @pBillFlags             ");
        _sb.AppendLine("             , TS_PLAYED_WON_FLAGS         = @pPlayedWonFlags        ");
        _sb.AppendLine("             , TS_JACKPOT_FLAGS            = @pJackpotFlags          ");
        _sb.AppendLine("             , TS_PLAYED_ALARM_ID          = @pPlayedAlarmId         ");
        _sb.AppendLine("             , TS_WON_ALARM_ID             = @pWonAlarmId            ");
        _sb.AppendLine("             , TS_JACKPOT_ALARM_ID         = @pJackpotAlarmId        ");
        _sb.AppendLine("             , TS_CANCELED_CREDIT_ALARM_ID = @pCanceledCreditAlarmId ");
        _sb.AppendLine("             , TS_COUNTERFEIT_ALARM_ID     = @pCounterfeitAlarmId    ");
        _sb.AppendLine("             , TS_CURRENT_PAYOUT           = @pCurrentPayout         ");
        _sb.AppendLine("             , TS_CURRENT_PAYOUT_ALARM_ID  = @pCurrentPayoutAlarmId  ");
        _sb.AppendLine("             , TS_WITHOUT_PLAYS_ALARM_ID   = @pWithoutPlaysAlarmId   ");
        _sb.AppendLine("       WHERE   TS_TERMINAL_ID              = @pTerminal              ");
        _sb.AppendLine("        ELSE                                                         ");
        _sb.AppendLine(" INSERT INTO   TERMINAL_STATUS                                       ");
        _sb.AppendLine("           (   TS_TERMINAL_ID                                        ");
        _sb.AppendLine("             , TS_BILL_FLAGS                                         ");
        _sb.AppendLine("             , TS_PLAYED_WON_FLAGS                                   ");
        _sb.AppendLine("             , TS_JACKPOT_FLAGS                                      ");
        _sb.AppendLine("             , TS_PLAYED_ALARM_ID                                    ");
        _sb.AppendLine("             , TS_WON_ALARM_ID                                       ");
        _sb.AppendLine("             , TS_JACKPOT_ALARM_ID                                   ");
        _sb.AppendLine("             , TS_CANCELED_CREDIT_ALARM_ID                           ");
        _sb.AppendLine("             , TS_COUNTERFEIT_ALARM_ID                               ");
        _sb.AppendLine("             , TS_CURRENT_PAYOUT                                     ");
        _sb.AppendLine("             , TS_CURRENT_PAYOUT_ALARM_ID                            ");
        _sb.AppendLine("             , TS_WITHOUT_PLAYS_ALARM_ID                             ");
        _sb.AppendLine("           )                                                         ");
        _sb.AppendLine("      VALUES                                                         ");
        _sb.AppendLine("           (   @pTerminal                                            ");
        _sb.AppendLine("             , @pBillFlags                                           ");
        _sb.AppendLine("             , @pPlayedWonFlags                                      ");
        _sb.AppendLine("             , @pJackpotFlags                                        ");
        _sb.AppendLine("             , @pPlayedAlarmId                                       ");
        _sb.AppendLine("             , @pWonAlarmId                                          ");
        _sb.AppendLine("             , @pJackpotAlarmId                                      ");
        _sb.AppendLine("             , @pCanceledCreditAlarmId                               ");
        _sb.AppendLine("             , @pCounterfeitAlarmId                                  ");
        _sb.AppendLine("             , @pCurrentPayout                                       ");
        _sb.AppendLine("             , @pCurrentPayoutAlarmId                                ");
        _sb.AppendLine("             , @pWithoutPlaysAlarmId                                 ");
        _sb.AppendLine("           )                                                         ");

        _sql_cmd_update = new SqlCommand(_sb.ToString());
        _sql_cmd_update.Parameters.Add("@pBillFlags", SqlDbType.Int).SourceColumn = "TS_BILL_FLAGS";
        _sql_cmd_update.Parameters.Add("@pPlayedWonFlags", SqlDbType.Int).SourceColumn = "TS_PLAYED_WON_FLAGS";
        _sql_cmd_update.Parameters.Add("@pJackpotFlags", SqlDbType.Int).SourceColumn = "TS_JACKPOT_FLAGS";
        _sql_cmd_update.Parameters.Add("@pTerminal", SqlDbType.Int).SourceColumn = "TERMINAL_ID";
        _sql_cmd_update.Parameters.Add("@pPlayedAlarmId", SqlDbType.BigInt).SourceColumn = "TS_PLAYED_ALARM_ID";
        _sql_cmd_update.Parameters.Add("@pWonAlarmId", SqlDbType.BigInt).SourceColumn = "TS_WON_ALARM_ID";
        _sql_cmd_update.Parameters.Add("@pJackpotAlarmId", SqlDbType.BigInt).SourceColumn = "TS_JACKPOT_ALARM_ID";
        _sql_cmd_update.Parameters.Add("@pCanceledCreditAlarmId", SqlDbType.BigInt).SourceColumn = "TS_CANCELED_CREDIT_ALARM_ID";
        _sql_cmd_update.Parameters.Add("@pCounterfeitAlarmId", SqlDbType.BigInt).SourceColumn = "TS_COUNTERFEIT_ALARM_ID";
        _sql_cmd_update.Parameters.Add("@pCurrentPayout", SqlDbType.Decimal).SourceColumn = "CURRENT_PAYOUT";
        _sql_cmd_update.Parameters.Add("@pCurrentPayoutAlarmId", SqlDbType.BigInt).SourceColumn = "TS_CURRENT_PAYOUT_ALARM_ID";
        _sql_cmd_update.Parameters.Add("@pWithoutPlaysAlarmId", SqlDbType.BigInt).SourceColumn = "TS_WITHOUT_PLAYS_ALARM_ID";

        m_sql_data_adap.UpdateCommand = _sql_cmd_update;
        m_sql_data_adap.UpdateCommand.Connection = Trx.Connection;
        m_sql_data_adap.UpdateCommand.Transaction = Trx;
        m_sql_data_adap.UpdateBatchSize = 500;
        m_sql_data_adap.UpdateCommand.UpdatedRowSource = UpdateRowSource.None;
        m_sql_data_adap.ContinueUpdateOnError = true;

        m_sql_data_adap.Update(m_terminal_status_flags);

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }

    } // Trx_UpdateTerminalStatus

    #endregion // Public Methods

    #region Private Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Sets Counterfeit alerts for each terminal and generates an alarm if it's needed.
    //
    //  PARAMS :
    //      - INPUT : Trx
    //                Row
    //
    //      - OUTPUT :
    //
    // RETURNS : true  -> Everything ok 
    //           false -> An error occurred
    //
    private static Boolean Trx_CounterfeitAlarm(DataRow Row, SqlTransaction Trx)
    {

      Boolean _activate_alarm;
      Int32 _counterfeit_num;
      String[] _alarm_message_param = { "", "", "" };

      try
      {
        _counterfeit_num = 0;

        if (!Row.IsNull("COUNTERFEIT_EXCEEDED"))
        {
          _counterfeit_num = (Int32)Row["COUNTERFEIT_EXCEEDED"];
        }
        _activate_alarm = _counterfeit_num > GeneralParam.GetInt32("Alarms", "Counterfeit.Limit.Quantity", 0);

        _alarm_message_param[0] = _counterfeit_num.ToString();
        _alarm_message_param[1] = m_interval_value_counterfeit.ToString();
        _alarm_message_param[2] = GetPeriodUnitDescription(m_interval_unit_counterfeit);

        if (!Trx_SetStatusBitmaskAndInsertAlarm(Row,
                                                (Int32)TerminalStatusFlags.BILL_FLAG.COUNTERFEIT_EXCEEDED,
                                                _activate_alarm,
                                                "TS_BILL_FLAGS",
                                                "STR_CUSTOM_ALARMS_COUNTERFEIT",
                                                _alarm_message_param,
                                                "TS_COUNTERFEIT_ALARM_ID",
                                                AlarmCode.CustomAlarm_Counterfeit,
                                                GetPeriodToHours(m_interval_value_counterfeit, m_interval_unit_counterfeit),
                                                Trx))
        {
          return false;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
    } // Trx_CounterfeitAlarm

    //------------------------------------------------------------------------------
    // PURPOSE : Sets Played alerts for each terminal and generates an alarm if it's needed.
    //
    //  PARAMS :
    //      - INPUT : Trx
    //                Row
    //
    //      - OUTPUT :
    //
    // RETURNS : true  -> Everything ok 
    //           false -> An error occurred
    //
    private static Boolean Trx_PlayedAlarm(DataRow Row, SqlTransaction Trx)
    {
      Boolean _activate_alarm;
      RESOURCE_NAME_TYPE _resource_name_type;
      String _alarm_message = "";
      String[] _alarm_message_param = { "", "", "", "" };

      try
      {

        _activate_alarm = MustAlarmBeActivated(Row, "PLAYED_AMOUNT", "PLAYED_COUNT",
                                               GeneralParam.GetDecimal("Alarms", "Played.Limit.Amount", 0),
                                               GeneralParam.GetInt32("Alarms", "Played.Limit.Quantity", 0),
                                               out _resource_name_type);

        switch (_resource_name_type)
        {
          case RESOURCE_NAME_TYPE.NONE:
          default:
            break;

          case RESOURCE_NAME_TYPE.AMOUNT:
            _alarm_message = "STR_CUSTOM_ALARMS_PLAYED_AMOUNT";
            _alarm_message_param[0] = ((Currency)(Decimal)Row["PLAYED_AMOUNT"]).ToString();
            _alarm_message_param[1] = m_interval_value_played.ToString();
            _alarm_message_param[2] = GetPeriodUnitDescription(m_interval_unit_played);
            break;

          case RESOURCE_NAME_TYPE.COUNT:
            _alarm_message = "STR_CUSTOM_ALARMS_PLAYED_COUNT";
            _alarm_message_param[0] = Row["PLAYED_COUNT"].ToString();
            _alarm_message_param[1] = m_interval_value_played.ToString();
            _alarm_message_param[2] = GetPeriodUnitDescription(m_interval_unit_played);
            break;

          case RESOURCE_NAME_TYPE.AMOUNT_COUNT:
            _alarm_message = "STR_CUSTOM_ALARMS_PLAYED_AMOUNT_COUNT";
            _alarm_message_param[0] = Row["PLAYED_COUNT"].ToString();
            _alarm_message_param[1] = ((Currency)(Decimal)Row["PLAYED_AMOUNT"]).ToString();
            _alarm_message_param[2] = m_interval_value_played.ToString();
            _alarm_message_param[3] = GetPeriodUnitDescription(m_interval_unit_played);
            break;
        }

        if (!Trx_SetStatusBitmaskAndInsertAlarm(Row,
                                                (Int32)TerminalStatusFlags.PLAYS_FLAG.PLAYED,
                                                _activate_alarm,
                                                "TS_PLAYED_WON_FLAGS",
                                                _alarm_message,
                                                _alarm_message_param,
                                                "TS_PLAYED_ALARM_ID",
                                                AlarmCode.CustomAlarm_Played,
                                                GetPeriodToHours(m_interval_value_played, m_interval_unit_played),
                                                Trx))
        {
          return false;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
    } // Trx_PlayedAlarm

    //------------------------------------------------------------------------------
    // PURPOSE : Sets won alerts for each terminal and generates an alarm if it's needed.
    //
    //  PARAMS :
    //      - INPUT : Trx
    //                Row
    //
    //      - OUTPUT :
    //
    // RETURNS : true  -> Everything ok 
    //           false -> An error occurred
    //
    private static Boolean Trx_WonAlarm(DataRow Row, SqlTransaction Trx)
    {
      Boolean _activate_alarm;
      RESOURCE_NAME_TYPE _resource_name_type;
      String _alarm_message = "";
      String[] _alarm_message_param = { "", "", "", "" };

      try
      {

        _activate_alarm = MustAlarmBeActivated(Row, "WON_AMOUNT", "WON_COUNT",
                                               GeneralParam.GetDecimal("Alarms", "Won.Limit.Amount", 0),
                                               GeneralParam.GetInt32("Alarms", "Won.Limit.Quantity", 0),
                                               out _resource_name_type);

        switch (_resource_name_type)
        {
          case RESOURCE_NAME_TYPE.NONE:
          default:
            break;

          case RESOURCE_NAME_TYPE.AMOUNT:
            _alarm_message = "STR_CUSTOM_ALARMS_WON_AMOUNT";
            _alarm_message_param[0] = ((Currency)(Decimal)Row["WON_AMOUNT"]).ToString();
            _alarm_message_param[1] = m_interval_value_won.ToString();
            _alarm_message_param[2] = GetPeriodUnitDescription(m_interval_unit_won);
            break;

          case RESOURCE_NAME_TYPE.COUNT:
            _alarm_message = "STR_CUSTOM_ALARMS_WON_COUNT";
            _alarm_message_param[0] = Row["WON_COUNT"].ToString();
            _alarm_message_param[1] = m_interval_value_won.ToString();
            _alarm_message_param[2] = GetPeriodUnitDescription(m_interval_unit_won);
            break;

          case RESOURCE_NAME_TYPE.AMOUNT_COUNT:
            _alarm_message = "STR_CUSTOM_ALARMS_WON_AMOUNT_COUNT";
            _alarm_message_param[0] = Row["WON_COUNT"].ToString();
            _alarm_message_param[1] = ((Currency)(Decimal)Row["WON_AMOUNT"]).ToString();
            _alarm_message_param[2] = m_interval_value_won.ToString();
            _alarm_message_param[3] = GetPeriodUnitDescription(m_interval_unit_won);
            break;
        }

        if (!Trx_SetStatusBitmaskAndInsertAlarm(Row,
                                                (Int32)TerminalStatusFlags.PLAYS_FLAG.WON,
                                                _activate_alarm,
                                                "TS_PLAYED_WON_FLAGS",
                                                _alarm_message,
                                                _alarm_message_param,
                                                "TS_WON_ALARM_ID",
                                                AlarmCode.CustomAlarm_Won,
                                                GetPeriodToHours(m_interval_value_won, m_interval_unit_won),
                                                Trx))
        {
          return false;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
    } // Trx_WonAlarm

    //------------------------------------------------------------------------------
    // PURPOSE : Sets Jackpot alert for each terminal.
    //
    //  PARAMS :
    //      - INPUT : Trx
    //                Row
    //
    //      - OUTPUT :
    //
    // RETURNS : true  -> Everything ok 
    //           false -> An error occurred
    //
    private static Boolean Trx_JackpotAlarm(DataRow Row, SqlTransaction Trx)
    {
      Boolean _activate_alarm;
      RESOURCE_NAME_TYPE _resource_name_type;
      String _alarm_message = "";
      String[] _alarm_message_param = { "", "", "", "" };

      try
      {

        _activate_alarm = MustAlarmBeActivated(Row, "JACKPOT_AMOUNT", "JACKPOT_COUNT",
                                               GeneralParam.GetDecimal("Alarms", "Jackpot.Limit.Amount", 0),
                                               GeneralParam.GetInt32("Alarms", "Jackpot.Limit.Quantity", 0),
                                               out _resource_name_type);

        switch (_resource_name_type)
        {
          case RESOURCE_NAME_TYPE.NONE:
          default:
            break;

          case RESOURCE_NAME_TYPE.AMOUNT:
            _alarm_message = "STR_CUSTOM_ALARMS_JACKPOT_AMOUNT";
            _alarm_message_param[0] = ((Currency)(Decimal)Row["JACKPOT_AMOUNT"]).ToString();
            _alarm_message_param[1] = m_interval_value_jackpot.ToString();
            _alarm_message_param[2] = GetPeriodUnitDescription(m_interval_unit_jackpot);
            break;

          case RESOURCE_NAME_TYPE.COUNT:
            _alarm_message = "STR_CUSTOM_ALARMS_JACKPOT_COUNT";
            _alarm_message_param[0] = Row["JACKPOT_COUNT"].ToString();
            _alarm_message_param[1] = m_interval_value_jackpot.ToString();
            _alarm_message_param[2] = GetPeriodUnitDescription(m_interval_unit_jackpot);
            break;

          case RESOURCE_NAME_TYPE.AMOUNT_COUNT:
            _alarm_message = "STR_CUSTOM_ALARMS_JACKPOT_AMOUNT_COUNT";
            _alarm_message_param[0] = Row["JACKPOT_COUNT"].ToString();
            _alarm_message_param[1] = ((Currency)(Decimal)Row["JACKPOT_AMOUNT"]).ToString();
            _alarm_message_param[2] = m_interval_value_jackpot.ToString();
            _alarm_message_param[3] = GetPeriodUnitDescription(m_interval_unit_jackpot);
            break;
        }

        if (!Trx_SetStatusBitmaskAndInsertAlarm(Row,
                                                (Int32)TerminalStatusFlags.JACKPOT_FLAG.JACKPOTS,
                                                _activate_alarm,
                                                "TS_JACKPOT_FLAGS",
                                                _alarm_message,
                                                _alarm_message_param,
                                                "TS_JACKPOT_ALARM_ID",
                                                AlarmCode.CustomAlarm_Jackpot,
                                                GetPeriodToHours(m_interval_value_jackpot, m_interval_unit_jackpot),
                                                Trx))
        {
          return false;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
    } // Trx_JackpotAlarm

    //------------------------------------------------------------------------------
    // PURPOSE : Sets Canceled Credit alert for each terminal.
    //
    //  PARAMS :
    //      - INPUT : Trx
    //                Row
    //
    //      - OUTPUT :
    //
    // RETURNS : true  -> Everything ok 
    //           false -> An error occurred
    //
    private static Boolean Trx_CanceledCreditAlarm(DataRow Row, SqlTransaction Trx)
    {
      Boolean _activate_alarm;
      RESOURCE_NAME_TYPE _resource_name_type;
      String _alarm_message = "";
      String[] _alarm_message_param = { "", "", "", "" };

      try
      {
        _activate_alarm = MustAlarmBeActivated(Row, "CANCELED_CREDIT_AMOUNT", "CANCELED_CREDIT_COUNT",
                                               GeneralParam.GetDecimal("Alarms", "CanceledCredit.Limit.Amount", 0),
                                               GeneralParam.GetInt32("Alarms", "CanceledCredit.Limit.Quantity", 0),
                                               out _resource_name_type);

        switch (_resource_name_type)
        {
          case RESOURCE_NAME_TYPE.NONE:
          default:
            break;

          case RESOURCE_NAME_TYPE.AMOUNT:
            _alarm_message = "STR_CUSTOM_ALARMS_CANCELED_CREDIT_AMOUNT";
            _alarm_message_param[0] = ((Currency)(Decimal)Row["CANCELED_CREDIT_AMOUNT"]).ToString();
            _alarm_message_param[1] = m_interval_value_canceled_credit.ToString();
            _alarm_message_param[2] = GetPeriodUnitDescription(m_interval_unit_canceled_credit);
            break;

          case RESOURCE_NAME_TYPE.COUNT:
            _alarm_message = "STR_CUSTOM_ALARMS_CANCELED_CREDIT_COUNT";
            _alarm_message_param[0] = Row["CANCELED_CREDIT_COUNT"].ToString();
            _alarm_message_param[1] = m_interval_value_canceled_credit.ToString();
            _alarm_message_param[2] = GetPeriodUnitDescription(m_interval_unit_canceled_credit);
            break;

          case RESOURCE_NAME_TYPE.AMOUNT_COUNT:
            _alarm_message = "STR_CUSTOM_ALARMS_CANCELED_CREDIT_AMOUNT_COUNT";
            _alarm_message_param[0] = Row["CANCELED_CREDIT_COUNT"].ToString();
            _alarm_message_param[1] = ((Currency)(Decimal)Row["CANCELED_CREDIT_AMOUNT"]).ToString();
            _alarm_message_param[2] = m_interval_value_canceled_credit.ToString();
            _alarm_message_param[3] = GetPeriodUnitDescription(m_interval_unit_canceled_credit);
            break;
        }

        if (!Trx_SetStatusBitmaskAndInsertAlarm(Row,
                                                (Int32)TerminalStatusFlags.JACKPOT_FLAG.CANCELED_CREDIT,
                                                _activate_alarm,
                                                "TS_JACKPOT_FLAGS",
                                                _alarm_message,
                                                _alarm_message_param,
                                                "TS_CANCELED_CREDIT_ALARM_ID",
                                                AlarmCode.CustomAlarm_Canceled_Credit,
                                                GetPeriodToHours(m_interval_value_canceled_credit, m_interval_unit_canceled_credit),
                                                Trx))
        {
          return false;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
    } // Trx_CanceledCreditAlarm

    //------------------------------------------------------------------------------
    // PURPOSE : Sets payout alert for each terminal and generates an alarm if it's needed.
    //
    //  PARAMS :
    //      - INPUT : Trx
    //                Row
    //
    //      - OUTPUT :
    //
    // RETURNS : true  -> Everything ok 
    //           false -> An error occurred
    //
    private static Boolean Trx_PayoutAlarm(DataRow Row, SqlTransaction Trx)
    {
      Decimal _calculated_payout;
      Boolean _activate_alarm;
      String[] _alarm_message_param = { "", "", "" };

      try
      {
        _calculated_payout = -1;

        if (!Row.IsNull("CURRENT_PAYOUT_PLAYED") && !Row.IsNull("CURRENT_PAYOUT_WON") && (Decimal)Row["CURRENT_PAYOUT_PLAYED"] > 0)
        {
          _calculated_payout = ((Decimal)Row["CURRENT_PAYOUT_WON"] / (Decimal)Row["CURRENT_PAYOUT_PLAYED"]) * 100;
        }

        if (_calculated_payout == -1)
        {
          _activate_alarm = false;
        }
        else
        {
          //Calculated Payout must be out of the range and different from 0 to generate alarm.
          _activate_alarm = _calculated_payout > (Decimal)Row["THEORETICAL_PAYOUT"] + GeneralParam.GetDecimal("Alarms", "Payout.Limit.Upper", 0)
                         || _calculated_payout < (Decimal)Row["THEORETICAL_PAYOUT"] - GeneralParam.GetDecimal("Alarms", "Payout.Limit.Lower", 0);
        }

        _calculated_payout = Math.Round(_calculated_payout, 2);
        _alarm_message_param[0] = _calculated_payout.ToString();
        _alarm_message_param[1] = m_interval_value_payout.ToString();
        _alarm_message_param[2] = GetPeriodUnitDescription(m_interval_unit_payout);

        if (!Trx_SetStatusBitmaskAndInsertAlarm(Row,
                                                (Int32)TerminalStatusFlags.PLAYS_FLAG.CURRENT_PAYOUT,
                                                _activate_alarm,
                                                "TS_PLAYED_WON_FLAGS",
                                                "STR_CUSTOM_ALARMS_PAYOUT",
                                                _alarm_message_param,
                                                "TS_CURRENT_PAYOUT_ALARM_ID",
                                                AlarmCode.CustomAlarm_Payout,
                                                GetPeriodToHours(m_interval_value_payout, m_interval_unit_payout),
                                                Trx))
        {
          return false;
        }

        Row["CURRENT_PAYOUT"] = _calculated_payout;

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
    } // Trx_PayoutAlarm

    //------------------------------------------------------------------------------
    // PURPOSE : Sets without plays alert for each terminal and generates an alarm if it's needed.
    //
    //  PARAMS :
    //      - INPUT : Trx
    //                Row
    //
    //      - OUTPUT :
    //
    // RETURNS : true  -> Everything ok 
    //           false -> An error occurred
    //
    private static Boolean Trx_WithoutPlaysAlarm(DataRow Row, SqlTransaction Trx)
    {
      Boolean _activate_alarm;
      String[] _alarm_message_param = { "", "", "", "" };
      TimeSpan _last_activity;
      int _days = 0;
      int _hours = 0;
      String _param_2 = "";
      String _param_3 = "";
      String[] _param_3_params = { "", "" };

      try
      {
        if (Row.IsNull("SPH"))
        {
          _activate_alarm = true;
          _last_activity = new TimeSpan(GetPeriodToHours(m_interval_value_terminals_whitout_plays + m_interval_value_terminals_whitout_plays_range, m_interval_unit_terminals_without_plays), 0, 0);
          _param_2 = Resource.String("STR_CUSTOM_ALARMS_WITHOUT_PLAYS_MORE");
        }
        else
        {
          // MPO 05-JUN-2014 - WIG-1012: The base hour of SPH should add a hour  
          _activate_alarm = (Int32)Row["SPH"] > GetPeriodToHours(m_interval_value_terminals_whitout_plays, m_interval_unit_terminals_without_plays);
          _last_activity = new TimeSpan((Int32)Row["SPH"], 0, 0);
          _param_2 = Resource.String("STR_CUSTOM_ALARMS_WITHOUT_PLAYS_LAST");
        }

        _days = (int)_last_activity.TotalDays;
        _hours = (int)_last_activity.TotalHours - _days * 24;

        if (_days > 0 && _hours > 0)
        {
          _param_3_params[0] = _days.ToString();
          _param_3_params[1] = _hours.ToString();
          _param_3 = Resource.String("STR_CUSTOM_ALARMS_WITHOUT_PLAYS_DAYS_HOURS", _param_3_params);
        }
        else if (_days > 0)
        {
          _param_3_params[0] = _days.ToString();
          _param_3 = Resource.String("STR_CUSTOM_ALARMS_WITHOUT_PLAYS_DAYS", _param_3_params);
        }
        else
        {
          _param_3_params[0] = _hours.ToString();
          _param_3 = Resource.String("STR_CUSTOM_ALARMS_WITHOUT_PLAYS_HOURS", _param_3_params);
        }

        _alarm_message_param[0] = m_interval_value_terminals_whitout_plays.ToString();
        _alarm_message_param[1] = GetPeriodUnitDescription(m_interval_unit_terminals_without_plays);
        _alarm_message_param[2] = _param_2;
        _alarm_message_param[3] = _param_3;

        if (!Trx_SetStatusBitmaskAndInsertAlarm(Row,
                                                (Int32)TerminalStatusFlags.PLAYS_FLAG.WITHOUT_PLAYS,
                                                _activate_alarm,
                                                "TS_PLAYED_WON_FLAGS",
                                                "STR_CUSTOM_ALARMS_WITHOUT_PLAYS",
                                                _alarm_message_param,
                                                "TS_WITHOUT_PLAYS_ALARM_ID",
                                                AlarmCode.CustomAlarm_Without_Plays,
                                                GetPeriodToHours(m_interval_value_terminals_whitout_plays, m_interval_unit_terminals_without_plays),
                                                Trx))
        {
          return false;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
    } // Trx_WithoutPlaysAlarm

    //------------------------------------------------------------------------------
    // PURPOSE : Sets Terminal_Status bitmask for the specified column and Inserts an alarm if the flag changes from inactive to active.
    //
    //  PARAMS :
    //      - INPUT : Row: data
    //                TSFlag: TerminalStatusFlag to process
    //                NewFlagStatus: Indicates if the alarm must be activated or deactivated.
    //                BitmaskColum: The name of the row column that contains the bitmask
    //                AlarmMessage: String with the resoure id for the alarm.                
    //                AlarmMessageParam: String[] with param for alarm message.
    //                AlarmColumn: column of alarm id
    //                AlarmCod: code of alarm
    //                AlarmInterval: Interval of not insert the alarm
    //                Trx
    //
    //      - OUTPUT : AlarmId: Alarm Id inserted 
    //
    // RETURNS : true  -> Everything ok 
    //           false -> An error occurred
    //
    private static Boolean Trx_SetStatusBitmaskAndInsertAlarm(DataRow Row,
                                                              Int32 TSFlag,
                                                              Boolean NewFlagStatus,
                                                              String BitmaskColumn,
                                                              String AlarmMessage,
                                                              String[] AlarmMessageParam,
                                                              String AlarmColumn,
                                                              AlarmCode AlarmCod,
                                                              Int32 AlarmInterval,
                                                              SqlTransaction Trx)
    {
      String _al_source_name;
      String _al_message;
      Int64 _alarm_id = 0;

      try
      {
        if (NewFlagStatus)
        {
          _al_source_name = Misc.ConcatProviderAndTerminal(Row["TE_PROVIDER_ID"].ToString(), Row["TE_NAME"].ToString());
          _al_message = String.IsNullOrEmpty(AlarmMessageParam[0]) ? Resource.String(AlarmMessage) : Resource.String(AlarmMessage, AlarmMessageParam);

          Alarm.Register(AlarmSourceCode.TerminalSASMachine,
                        (Int32)Row["TERMINAL_ID"],
                        _al_source_name,
                        (UInt32)AlarmCod,
                        _al_message,
                        AlarmSeverity.Warning,
                        WGDB.Now,
                        true,
                        m_now_hour.AddHours(-AlarmInterval),
                        out _alarm_id, Trx);

          if (_alarm_id != 0)
          {
            Row[BitmaskColumn] = TerminalStatusFlags.UpdateBitmask((Int32)Row[BitmaskColumn], TSFlag, NewFlagStatus);
            Row[AlarmColumn] = _alarm_id;
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Message("UpdateStatusFlagAndGenerateAlarm - Error generating Alarm '" + Resource.String(AlarmMessage) + "'");
        Log.Exception(_ex);

        return false;
      }
    } // Trx_SetStatusBitmaskAndInsertAlarm

    //------------------------------------------------------------------------------
    // PURPOSE : Checks if the column value exceeds the limit.
    //
    //  PARAMS :
    //      - INPUT : Row: data
    //                ColumnAmount: The name of the row column that contains the amount
    //                ColumnCount: The name of the row column that contains the count
    //                LimitAmount: the limit amount
    //                LimitCount: the limit count
    //
    //      - OUTPUT : ResourceNameType
    //
    // RETURNS : true  -> the row value exceeds the limit value.
    //           false -> the row value doesn't exceed the limit value.
    //
    private static Boolean MustAlarmBeActivated(DataRow Row,
                                                String ColumnAmount,
                                                String ColumCount,
                                                Decimal LimitAmount,
                                                Int32 LimitCount,
                                                out RESOURCE_NAME_TYPE ResourceNameType)
    {
      Boolean _alarm_amount;
      Boolean _alarm_count;

      Decimal _amount;
      Int32 _count;

      ResourceNameType = RESOURCE_NAME_TYPE.NONE;

      _alarm_amount = false;
      _alarm_count = false;
      _amount = 0;
      _count = 0;

      if (!Row.IsNull(ColumnAmount))
      {
        _amount = (Decimal)Row[ColumnAmount];
      }

      if (!Row.IsNull(ColumCount))
      {
        _count = (Int32)Row[ColumCount];
      }

      if (LimitAmount >= 0)
      {
        _alarm_amount = _amount > LimitAmount;
      }

      if (LimitCount >= 0)
      {
        _alarm_count = (_count > LimitCount);
      }

      if (_alarm_amount && _alarm_count)
      {
        ResourceNameType = RESOURCE_NAME_TYPE.AMOUNT_COUNT;
      }
      else if (_alarm_amount)
      {
        ResourceNameType = RESOURCE_NAME_TYPE.AMOUNT;
      }
      else if (_alarm_count)
      {
        ResourceNameType = RESOURCE_NAME_TYPE.COUNT;
      }

      return _alarm_amount || _alarm_count;
    } // MustAlarmBeActivated

    //------------------------------------------------------------------------------
    // PURPOSE : Initializes the datatable columns.
    //
    //  PARAMS :
    //      - INPUT : 
    //
    //      - OUTPUT :
    //
    // RETURNS : 
    //
    private static void InitializeTerminalStatusFlags()
    {
      m_terminal_status_flags = new DataTable();
      AddColumn(ref m_terminal_status_flags, "TERMINAL_ID", "System.Int32", false);
      AddColumn(ref m_terminal_status_flags, "TS_BILL_FLAGS", "System.Int32", false);
      AddColumn(ref m_terminal_status_flags, "TS_PLAYED_WON_FLAGS", "System.Int32", false);
      AddColumn(ref m_terminal_status_flags, "TS_JACKPOT_FLAGS", "System.Int32", false);
      AddColumn(ref m_terminal_status_flags, "TE_NAME", "System.String", true);
      AddColumn(ref m_terminal_status_flags, "TE_PROVIDER_ID", "System.String", true);
      AddColumn(ref m_terminal_status_flags, "THEORETICAL_PAYOUT", "System.Decimal", true);
      AddColumn(ref m_terminal_status_flags, "COUNTERFEIT_EXCEEDED", "System.Int32", true);
      AddColumn(ref m_terminal_status_flags, "TS_COUNTERFEIT_ALARM_ID", "System.Int64", true);
      AddColumn(ref m_terminal_status_flags, "PLAYED_COUNT", "System.Int32", true);
      AddColumn(ref m_terminal_status_flags, "PLAYED_AMOUNT", "System.Decimal", true);
      AddColumn(ref m_terminal_status_flags, "TS_PLAYED_ALARM_ID", "System.Int64", true);
      AddColumn(ref m_terminal_status_flags, "WON_COUNT", "System.Int32", true);
      AddColumn(ref m_terminal_status_flags, "WON_AMOUNT", "System.Decimal", true);
      AddColumn(ref m_terminal_status_flags, "TS_WON_ALARM_ID", "System.Int64", true);
      AddColumn(ref m_terminal_status_flags, "CURRENT_PAYOUT", "System.Decimal", true);
      AddColumn(ref m_terminal_status_flags, "CURRENT_PAYOUT_PLAYED", "System.Decimal", true);
      AddColumn(ref m_terminal_status_flags, "CURRENT_PAYOUT_WON", "System.Decimal", true);
      AddColumn(ref m_terminal_status_flags, "TS_CURRENT_PAYOUT_ALARM_ID", "System.Int64", true);
      AddColumn(ref m_terminal_status_flags, "JACKPOT_COUNT", "System.Int32", true);
      AddColumn(ref m_terminal_status_flags, "JACKPOT_AMOUNT", "System.Decimal", true);
      AddColumn(ref m_terminal_status_flags, "TS_JACKPOT_ALARM_ID", "System.Int64", true);
      AddColumn(ref m_terminal_status_flags, "CANCELED_CREDIT_COUNT", "System.Int32", true);
      AddColumn(ref m_terminal_status_flags, "CANCELED_CREDIT_AMOUNT", "System.Decimal", true);
      AddColumn(ref m_terminal_status_flags, "TS_CANCELED_CREDIT_ALARM_ID", "System.Int64", true);
      AddColumn(ref m_terminal_status_flags, "SPH", "System.Int32", true);
      AddColumn(ref m_terminal_status_flags, "TS_WITHOUT_PLAYS_ALARM_ID", "System.Int64", true);

    } // InitializeTerminalStatusFlags

    //------------------------------------------------------------------------------
    // PURPOSE : Add column to datatable
    //
    //  PARAMS :
    //      - INPUT : DataTable
    //                Name
    //                Typee
    //                IsNullable
    //
    //      - OUTPUT :
    //
    // RETURNS : true  -> the row value exceeds the limit value.
    //           false -> the row value doesn't exceed the limit value.
    //
    private static Boolean AddColumn(ref DataTable Dt, String Name, String Typee, Boolean IsNullable)
    {
      DataColumn _col = Dt.Columns.Add(Name, Type.GetType(Typee));
      _col.AllowDBNull = IsNullable;

      return true;
    } // AddColumn

    //------------------------------------------------------------------------------
    // PURPOSE : Get the string with the period under study unit.
    //
    //  PARAMS :
    //      - INPUT :
    //          - PeriodUnit
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - String
    //
    private static String GetPeriodUnitString(Int32 PeriodUnit)
    {
      switch (PeriodUnit)
      {
        case 1:
        default:
          return "HOUR";

        case 2:

          return "DAY";
      }
    } // GetPeriodUnitString

    //------------------------------------------------------------------------------
    // PURPOSE : Get the period to hours
    //
    //  PARAMS :
    //      - INPUT :
    //          - PeriodValue
    //          - PeriodUnit
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Int32
    //
    private static Int32 GetPeriodToHours(Int32 PeriodValue, Int32 PeriodUnit)
    {
      switch (PeriodUnit)
      {
        case 1:
        default:
          // HOUR
          return PeriodValue;

        case 2:
          //DAY
          return PeriodValue * 24;
      }
    } // GetPeriodToHours

    //------------------------------------------------------------------------------
    // PURPOSE : Get the string with the description of the period under study unit.
    //
    //  PARAMS :
    //      - INPUT :
    //          - PeriodUnit
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - String
    //
    private static String GetPeriodUnitDescription(Int32 PeriodUnit)
    {
      switch (PeriodUnit)
      {
        case 1:
        default:
          return Resource.String("STR_CUSTOM_ALARMS_HOUR");

        case 2:
          return Resource.String("STR_CUSTOM_ALARMS_DAY");
      }
    } // GetPeriodUnitDescription

    #endregion // Private Methods

  } //WCP_TerminalStatus
}
