//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_PeriodicPromotions.cs
//   DESCRIPTION: Class to process periodic promotions
//        AUTHOR: Ra�l Cervera
// 
// CREATION DATE: 05-JUN-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 05-JUN-2012 RCI    First release.
//------------------------------------------------------------------------------

using System;
using System.Threading;
using WSI.Common;
using System.Data.SqlClient;
using System.Data;
using System.Collections.Generic;

namespace WSI.WCP
{
  public static class WCP_PeriodicPromotions
  {
    #region Public Functions

    //------------------------------------------------------------------------------
    // PURPOSE : Process periodic promotions
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public static Boolean ProcessPeriodicPromotions()
    {
      DateTime _date_now;
      DateTime _opening_date;
      DateTime _ini_date;
      DateTime _fin_date;
      DateTime _current_last_executed;
      DataTable _accounts;
      DataRow[] _candidates;
      DataTable _played_per_account_provider;
      DataTable _previously_awarded;
      List<AccountPromotion> _pending_promos;
      Boolean _all_ok;
      Boolean _have_to_apply;
      Boolean _is_pending_draw_promotion;

      _all_ok = true;

      _date_now = WGDB.Now;
      _opening_date = Misc.TodayOpening();

      _accounts = null;
      _played_per_account_provider = null;

      if ( !AccountPromotion.GetPendingPeriodicPromotions(out _pending_promos))
      {
        return false;
      }

      foreach (AccountPromotion _promo in _pending_promos)
      {
        try
        {
          using (DB_TRX _db_trx = new DB_TRX())
          {
            _current_last_executed = _promo.last_executed;
            if (!AccountPromotion.Lock(_promo.id, _promo.last_executed, _db_trx.SqlTransaction))
            {
              _db_trx.Rollback();

              continue;
            }
        
            if (_accounts == null)
            {
              if (!Accounts.ReadActiveAccounts(_db_trx.SqlTransaction, out _accounts))
              {
                // Error: Log is done inside.
                _db_trx.Rollback();
                _all_ok = false;

                continue;
              }
            }

            if (!Promotion.ReadPromotionData(_db_trx.SqlTransaction, _promo.id, _promo))
            {
              _db_trx.Rollback();
              _all_ok = false;

              continue;
            }

            _have_to_apply = AccountPromotion.HaveToApply(_promo.type, _current_last_executed, _date_now);

            _have_to_apply &= (_promo.actual_promo_date == _opening_date);

            if (!AccountPromotion.UpdateNextExecution(_promo.id, _promo.type, _promo.actual_promo_date, _have_to_apply, _db_trx.SqlTransaction))
            {
              _db_trx.Rollback();
              _all_ok = false;

              continue;
            }

            if (!_have_to_apply)
            {
              // Need to Commit. Have to update LAST_EXECUTED and NEXT_EXECUTION.
              _db_trx.Commit();

              continue;
            }

            if (_promo.min_played != 0 || _promo.played != 0 || _promo.played_reward != 0) //PromoNeedPlayed(_promo)
            {
              if (_played_per_account_provider == null)
              {
                // ReadPlayed is from 1 the MONTH-1 a 1 the month. 
                _fin_date = new DateTime(_date_now.Year, _date_now.Month, 1, _opening_date.Hour, _opening_date.Minute, 0);
                _ini_date = _fin_date.AddMonths(-1);

                if (!Accounts.ReadPlayedPerAccountAndProvider(_ini_date, _fin_date, _db_trx.SqlTransaction, out _played_per_account_provider))
                {
                  // Error: Log is done inside.
                  _db_trx.Rollback();
                  _all_ok = false;

                  continue;
                }
              }
            }

            // Promotions awarded in less than one year.
            switch (_promo.type)
            {
              case Promotion.PROMOTION_TYPE.PERIODIC_PER_BIRTHDAY:                
                _ini_date = new DateTime(_date_now.Year, 1, 1, _opening_date.Hour, _opening_date.Minute, 0);
                _previously_awarded = AccountPromotion.GetPreviouslyAwarded(_ini_date, _date_now, _promo.id, _db_trx.SqlTransaction);
                break;

              default:
                _previously_awarded = null;
                break;
            }

            // FILTER 1: Only accounts that can apply the promotion
            _candidates = _promo.FilterAccounts(_accounts, _previously_awarded);

            // Only promotions with PromoGame related, set status to PENDING_DRAW.
            _is_pending_draw_promotion = (_promo.PromoGameId > 0);

            if (!_promo.Apply(_candidates, _played_per_account_provider, _is_pending_draw_promotion, _db_trx.SqlTransaction))
            {
              _db_trx.Rollback();
              _all_ok = false;

              continue;
            }

            _db_trx.Commit();

          } // using DB_TRX

          // TODO: Activar aniria fora d'aqui amb un possible retard. Pendent de confirmaci� per Andreu.
          
          AccountPromotion.ActivateAwardedPromotion(_promo.id);

        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
          _all_ok = false;
        }

      } // foreach AccountPromotion

      return _all_ok;

    } // ProcessPeriodicPromotions

    #endregion // Public Functions

  } // WCP_PeriodicPromotions

} // WSI.WCP
