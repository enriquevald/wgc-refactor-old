//------------------------------------------------------------------------------
// Copyright © 2010 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_Commands.cs
// 
//   DESCRIPTION: WCP_Commands class
// 
//        AUTHOR: Alberto Cuesta
// 
// CREATION DATE: 21-MAY-2010
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 21-MAY-2010 ACC    First release.
// 02-APR-2014 RCI    Fixed Bug WIG-788: Deadlock in DB_TimeoutWcpCommands
// 12-NOV-2014 FJC    Added SiteJackPotAwarded message code for showing in display LCD
// 18-MAY-2015 YNM    Fixed Bug WIG-2362: Change stacker in cashless takes into account GP 'Terminal.UseStackerId'
// 14-JUL-2015 DRV    Fixed Bug WIG-2570: Promobox stacker change can't be permofed
// 07-SEP-2015 MPO    TFS ITEM 2194: SAS16: Estadísticas multi-denominación: WCP
// 08-OCT-2015 FOS    Change category of severity error in log
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Data.SqlClient;
using WSI.Common;
using System.Data;
using System.Threading;
using System.Xml;
using WSI.Common.TITO;

namespace WSI.WCP
{
  public static class WCP_ChangeStacker
  {

    //------------------------------------------------------------------------------
    // PURPOSE : Init wcp commands thread
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public static void MsgProcess(WcpInputMsgProcessTask Task, Boolean IsTitoMode, SqlTransaction SqlTrx)
    {
      WCP_Message _wcp_request;
      WCP_Message _wcp_response;
      SecureTcpClient _tcp_client;
      WCP_MsgCashSessionPendingClosing request;
      WCP_MsgCashSessionPendingClosingReply response;
      Int64 _new_stacker_id;
      Stacker _new_stacker;
      String _description;
      ArrayList _pending_tasks;
      DataTable _terminal_meters;
      Int64 _money_collection_id;
      MoneyCollection _money_collection;
      Int32 _count;
      StringBuilder _sb;
      WCP_ResponseCodes _failed_response_code;

      _tcp_client = Task.WcpClient;
      _wcp_request = Task.Request;
      _wcp_response = Task.Response;

      _failed_response_code = WCP_ResponseCodes.WCP_RC_OK;
      if (Misc.UseStackerId())
      {
        _failed_response_code = WCP_ResponseCodes.WCP_RC_ERROR;
      }

      _new_stacker = null;
      _description = null;

      request = (WCP_MsgCashSessionPendingClosing)_wcp_request.MsgContent;
      response = (WCP_MsgCashSessionPendingClosingReply)_wcp_response.MsgContent;

      // ACC 14-APR-2015 Check Retries with TransactionId
      // DRV 14-JUL-2015 Added terminal type and transactionId conditions because promobox doesn't send any TransactionId
      if (WSI.Common.TITO.MoneyCollection.DB_GetMoneyCollection(_tcp_client.InternalId, request.TransactionId, request.NewStackerId, out _money_collection_id, SqlTrx)
        && _tcp_client.TerminalType != TerminalTypes.PROMOBOX
        && request.TransactionId > 0)
      {
        if (_money_collection_id > 0)
        {
          // Retry: Trx already processed!  
          _wcp_response.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_OK;
          Log.Warning(String.Format("Changing Stacker in TerminalId={0}, StackerId={1}. Retry: Already processed.", _tcp_client.InternalId, request.NewStackerId));

          return;
        }
      }

      _pending_tasks = new ArrayList();
      _pending_tasks.Add(Task);

      if (request.ChangeDatetime > DateTime.MinValue)
      {
        if (request.ChangeDatetime < DateTime.Now.Subtract(new TimeSpan(6, 0, 0)))
        {
          // 6 hours transaction failed
          _wcp_response.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_OK;
          Log.Warning(String.Format("Change Stacker request timeout in TerminalId={0}.", _tcp_client.InternalId));

          return;
        }

        // XCD 12-AUG-2014 Check if another cashier session is opened after message
        _sb = new StringBuilder();

        _sb.AppendLine("     SELECT COUNT(*) ");
        _sb.AppendLine("       FROM CASHIER_TERMINALS");
        _sb.AppendLine(" INNER JOIN CASHIER_SESSIONS ");
        _sb.AppendLine("         ON CT_CASHIER_ID = CS_CASHIER_ID");
        _sb.AppendLine("      WHERE CT_TERMINAL_ID = @pTerminalId");
        _sb.AppendLine("	      AND CS_STATUS = @pCashierSessionStatus");
        _sb.AppendLine("        AND CS_OPENING_DATE > @pChangeDatetime");

        //DateTime.FromFileTime(request.ChangeDatetime);
        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = _tcp_client.InternalId;
          _sql_cmd.Parameters.Add("@pCashierSessionStatus", SqlDbType.Int).Value = CASHIER_SESSION_STATUS.OPEN;
          _sql_cmd.Parameters.Add("@pChangeDatetime", SqlDbType.DateTime).Value = request.ChangeDatetime;

          _count = (Int32)_sql_cmd.ExecuteScalar();
        }

        if (_count > 0)
        {
          // Nothing to do
          _wcp_response.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_OK;
          Log.Warning(String.Format("Error in Changing Stacker in TerminalId={0}. There is a cashier session open after change stacker request.", _tcp_client.InternalId));

          return;
        }
      }

      if (request.ReadMeters)
      {
        // MPO & XIT & DRV
        if (!WCP_BU_SasMeters.Instance.Save(_pending_tasks, SqlTrx))
        {
          throw new WCP_Exception(_failed_response_code, String.Format("Error in WCP_BU_SasMeters.ProcessChangeStacker. TerminalId={0}", _tcp_client.InternalId));
        }

        //Just recover bills & tickets with delta > 0              
        if (!WSI.Common.SAS_Meter.GetSasMeters(SAS_Meter.ENUM_METERS_TYPE.BILLS_AND_TICKETS, _tcp_client.InternalId, false, out _terminal_meters, SqlTrx))
        {
          throw new WCP_Exception(_failed_response_code, String.Format("Error in WCP_SASMeters.GetSasMeters. TerminalId={0}", _tcp_client.InternalId));
        }
        if (!WSI.WCP.WCP_SASMeters.ChangeStackerProcessTerminalSasMeters(SAS_Meter.ENUM_METERS_TYPE.BILLS_AND_TICKETS, _terminal_meters, _tcp_client.InternalId, SqlTrx))
        {
          throw new WCP_Exception(_failed_response_code, String.Format("Error in WCP_SASMeters.NewProcessTerminalSasMeters. TerminalId={0}", _tcp_client.InternalId));
        }
      }      

      response.Result = (Int32)Cashier.WCPCashierSessionPendingClosing(_tcp_client.InternalId, SqlTrx);
      
      if (Misc.SystemMode() == SYSTEM_MODE.TITO /* IsTitoMode*/ && response.Result != (Int32)MB_CASHIER_SESSION_CLOSE_STATUS.ERROR)
      {
        _new_stacker_id = request.NewStackerId;

        if (_new_stacker_id == 0 && Misc.UseStackerId())
        {
          throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_STACKER_ID_NOT_FOUND, String.Format("Error Changing Stacker. The new stacker is not valid. TerminalId={0}, StackerId={1}", _tcp_client.InternalId, _new_stacker_id));
        }

        // id stacker <= 0 => It is not any stacker to insert and old stacker is stracted from terminal, if exists.
        if (_new_stacker_id > 0 && Misc.UseStackerId())
        {
          // It checks for existing terminal with the given id.
          _new_stacker = Stacker.DB_Read(_new_stacker_id, SqlTrx);

          if (_new_stacker == null)
          {
            _description = Resource.String("STR_FRM_CONTAINER_EVENTS_DESC_TERMINAL_NEW_COLLECTION_WRONG_STACKER_ID",
                                           _new_stacker_id);

            Alarm.Register(AlarmSourceCode.TerminalWCP,
                           _tcp_client.InternalId,
                           "TITO_WCP",
                           (UInt32)AlarmCode.WCP_TITO_StackerChangeFail,
                           _description,
                           AlarmSeverity.Error);

            Log.Error(String.Format("Error Changing Stacker. The new stacker is not valid. TerminalId={0}, StackerId={1}", _tcp_client.InternalId, _new_stacker_id));

            _new_stacker_id = 0;

            // ACC 27-APR-2015 Return ERROR when Stacker not exist.
            throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_STACKER_ID_NOT_FOUND, String.Format("Error Changing Stacker. The new stacker is not valid. TerminalId={0}, StackerId={1}", _tcp_client.InternalId, _new_stacker_id));
          }

          // ACC 27-APR-2015 Return ERROR when Stacker is already use.
          if (_new_stacker.InsertedTerminalId > 0
              && _new_stacker.InsertedTerminalId != _tcp_client.InternalId)
          {
            String _mask_name;

            // Get terminal Name
            _mask_name = GeneralParam.GetString("SasHost", "DisplayName.Format");
            if (!WSI.Common.Terminal.GetTerminalDisplayName(_new_stacker.InsertedTerminalId, _mask_name, out response.AlreadyInsertedTerminalName, SqlTrx))
            {
              Log.Error(String.Format("Error in Misc.GetTerminalName. TerminalId={0}", _new_stacker.InsertedTerminalId));
            }

            Log.Error(String.Format("Error Changing Stacker. The new stacker is already use. TerminalId={0}, StackerId={1}, TerminalInUse: {2}", _tcp_client.InternalId, _new_stacker_id, _new_stacker.InsertedTerminalId));
            throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_STACKER_ID_ALREADY_IN_USE, String.Format("Error Changing Stacker. The new stacker is already use. TerminalId={0}, StackerId={1}, TerminalInUse: {2}", _tcp_client.InternalId, _new_stacker_id, _new_stacker.InsertedTerminalId));
          }

        }

        // Closes actual MC, MCM
        // Opens CS,MC,MCM if new stacker into terminal  
        if (!WCP_TITO.TITO_ProcessChangeStacker(_tcp_client.InternalId, _new_stacker, request.TransactionId, SqlTrx))
        {
          Log.Error(String.Format("Error in TITO_ProcessChangeStacker. TerminalId={0}, NewStacker={1}", _tcp_client.InternalId, _new_stacker.StackerId));
          throw new WCP_Exception(_failed_response_code, String.Format("Error in TITO_ProcessChangeStacker. TerminalId={0}", _tcp_client.InternalId));
        }

        // ICS 08-JAN-2014: Reset terminal stacker counter
        if (!TerminalStatusFlags.SetTerminalStatus(_tcp_client.InternalId, TerminalStatusFlags.WCP_TerminalEvent.ChangeStacker, SqlTrx))
        {
          Log.Error(String.Format("Error in TerminalStatusFlags.SetTerminalStatus. TerminalId={0}, NewStacker={1}", _tcp_client.InternalId, _new_stacker.StackerId));
          throw new WCP_Exception(_failed_response_code, String.Format("Error in TerminalStatusFlags.SetTerminalStatus. TerminalId={0}", _tcp_client.InternalId));
        }

        Log.Message(String.Format("Stacker changed: TerminalId={0}, StackerId={1}", _tcp_client.InternalId, _new_stacker_id));

      }
      else
      {
        if (Misc.SystemMode() != SYSTEM_MODE.TITO /*!IsTitoMode*/)
        {
          _wcp_response.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_OK;
        }

        if (!MoneyCollection.DB_GetMoneyCollection(_tcp_client.InternalId, 0, out _money_collection_id, SqlTrx))
        {
          Log.Error("Error getting Money Collection.");
          throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_OK, String.Format("Error in MoneyCollection.DB_GetMoneyCollection. TerminalId={0}", _tcp_client.InternalId));
        }

        _money_collection = MoneyCollection.DB_Read(_money_collection_id, SqlTrx);

        if (_money_collection != null)
        {
          if (!_money_collection.DB_ChangeStatusToPendingClosing(SqlTrx))
          {
            Log.Error("Error changing Money Collection status.");
            throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_OK, String.Format("Error in DB_ChangeStatusToPendingClosing. TerminalId={0}", _tcp_client.InternalId));
          }
        }
        else
        {
          Log.Warning(String.Format("No money collection pending to collect. TerminalId={0}", _tcp_client.InternalId));
          _wcp_response.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_OK;
        }
      }

    } // MsgProcess

  }
}
