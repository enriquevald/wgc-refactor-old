using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Xml;
using System.Data.SqlClient;
using System.Data;



namespace WSI.WCP
{
  public static class Protocol
  {
    public const int PORT_NUMBER = 65535;             // Listener port number
    public const int SERVICE_PORT_NUMBER = 13000;     // Service port number
    public const String PROTOCOL_NAME = "WCP";  // Service Name
    public const String SERVICE_NAME = "WCP_Service";  // Service Name
  }
  
  public partial class WCP_Client
  {
    private DataSet ds;

    void P1 (SqlConnection Connection)
    { 
      // Create the Data Set
      ds = new DataSet ();
      
      AddTable ("wcp_messages", Connection );
      AddTable ("terminals", Connection);
      AddTable ("wcp_sessions", Connection);
      AddTable ("wcp_transactions", Connection);

    }

    /// <summary>
    /// Adds the given table to the dataset. 
    /// Retrieves the table schema from the DB.
    /// </summary>
    /// <param name="TableName">Table Name</param>
    /// <param name="Connection">Connection</param>
    /// <returns></returns>
    DataTable AddTable (String TableName, SqlConnection Connection)
    {
      SqlDataAdapter da;
      SqlCommand cmd;
      DataTable table;
      SqlCommandBuilder cb;



      table = ds.Tables.Add (TableName);
      cmd = new SqlCommand ("SELECT * FROM " + TableName);
      da = new SqlDataAdapter ();
      da.SelectCommand = cmd;
      da.SelectCommand.Connection = Connection;
      da.FillSchema (table, SchemaType.Source);
      
      cb = new SqlCommandBuilder (da);


      return table;
    }














    void LoadWcpSession (Int32 TerminalId)
    { 
    
    }

    void UpdateWcpSession (Int32 TerminalId, Int64 SequenceId)
    {
      //DataTable wcp_sessions;
      //DataRow[] sessions;
      //DataRow session;

      //sessions = wcp_sessions.Select ("ws_terminal_id = " + TerminalId.ToString ());
      //if ( sessions.Length > 1 )
      //{ 
      
      //}
      //session = sessions[0];

      //sessions["ws_last_sequence_id"] = SequenceId;





      //// 
      //message = wcp_messages.NewRow ();

      //if ( InternalId != 0 )
      //{
      //  message["wm_terminal_id"] = TerminalId;
      //}
      //message["wm_sequence_id"] = SequenceId;
      //message["wm_towards_to_terminal"] = TowardsToTerminal;
      //message["wm_message"] = XmlMessage;

      //wcp_messages.Rows.Add (message);
    
    }

    void AddWcpMessage (Int32 TerminalId, Int64 SequenceId, Boolean TowardsToTerminal, XmlDocument XmlMessage)
    {
      //DataTable wcp_messages;
      //DataRow   message;

      //wcp_messages = ds.Tables["wcp_messages"];

      //message = wcp_messages.NewRow ();

      //if ( InternalId != 0 )
      //{
      //  message["wm_terminal_id"] = TerminalId;
      //}
      //message["wm_sequence_id"] = SequenceId;
      //message["wm_towards_to_terminal"] = TowardsToTerminal;
      //message["wm_message"] = XmlMessage;

      //wcp_messages.Rows.Add (message);
    }

    void AddWcpTransaction (Int32 TerminalId, Int64 SequenceId, Boolean RequestedByTerminal, XmlDocument XmlRequest)
    {
      //DataTable wcp_transactions;
      //DataRow   transaction;

      //// 
      //transaction = wcp_transactions.NewRow ();

      //transaction["wt_terminal_id"] = TerminalId;
      //transaction["wt_sequence_id"] = SequenceId;
      //transaction["wt_requested_by_terminal"] = RequestedByTerminal;
      //transaction["wt_request"] = XmlRequest;

      //wcp_transactions.Rows.Add (transaction);
    }



    void LoadWcpTransactionX (Int32 TerminalId, Int64 SequenceId, Boolean RequestedByTerminal)
    {
      SqlParameter   param;
      String         sb;
      SqlCommand cmd;

      cmd = new SqlCommand ();
      sb = "";
      sb += "SELECT * FROM WCP_TRANSACTION";
      sb += " WHERE   wt_terminal_id = @p1";
      sb += "   AND   wt_sequence_id = @p2";
      sb += "   AND   wt_requested_by_terminal = @p3";

      cmd.CommandText = sb;
      param = cmd.Parameters.Add ("@p1", SqlDbType.Int);
      param.Direction = ParameterDirection.Input;
      param = cmd.Parameters.Add ("@p2", SqlDbType.BigInt);
      param.Direction = ParameterDirection.Input;
      param = cmd.Parameters.Add ("@p3", SqlDbType.Bit);
      param.Direction = ParameterDirection.Input;

      //DataSet ds;

      //wcp_transactions = ds.Tables.Add ("wcp_transactions");



      //wcp_transactions = ds.Tables["wcp_transactions"];
      //da = new SqlDataAdapter ();
      //da.SelectCommand = cmd;
      //da.Fill (wcp_transactions);




      //{
      //  DataTable wcp_transactions;

      //  da.SelectCommand.Parameters[0] = TerminalId;
      //  da.SelectCommand.Parameters[1] = SequenceId;
      //  da.SelectCommand.Parameters[2] = RequestedByTerminal;

      //  da.Fill (wcp_transactions);

      //  if ( wcp_transactions.Rows.Count == 0 )
      //  {
      //    AddWcpTransaction (TerminalId, SequenceId, RequestedByTerminal);
      //  }

      //  //status = (int) wcp_transactions.Rows[0]["wt_status"];

      //  //switch ( status )
      //  //{ 
        
      //  //}
      }



    }


}


