//------------------------------------------------------------------------------
// Copyright � 2008 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_PendingnrollsQueue.cs
// 
//   DESCRIPTION: WCP_PendingEnrollsQueue class
// 
//        AUTHOR: Alberto Cuesta
// 
// CREATION DATE: 10-NOV-2008
// 
// REVISION HISTORY:
// 
// Date        Author     Description
// ----------- ---------  ------------------------------------------------------
// 10-NOV-2008 ACC        First release.
// 11-NOV-2014 RCI & LEM  When enrolling a terminal, ensure it has a cashier session and a money collection open.
// 24-FEB-2016 RCI & ETP  Fixed BUG 7247: Only open system sessions if is a sas host terminal.
// 22-MAR-2016 ETP        Fixed BUG 10907: General Params not correctly configured if promobox is disconected.
//------------------------------------------------------------------------------

using System;
using System.Messaging;
using System.Collections.Generic;
using System.Text;
using System.Net;
using WSI.Common;
using System.Xml;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using WSI.WCP;
using System.Collections;

namespace WSI.WCP
{
  /// <summary>
  /// Holds enroll data that awaits to be processed
  /// </summary>
  public class WCP_ProcessEnroll
  {
    //------------------------------------------------------------------------------
    // PURPOSE : Enroll Server
    //
    //  PARAMS :
    //      - INPUT :
    //         - WcpClient
    //         - WcpRequestMessage
    //         - WcpResponseMessage
    //         - SqlTrx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public static void EnrollServer(SecureTcpClient WcpClient, WCP_Message WcpRequestMessage, WCP_Message WcpResponseMessage, SqlTransaction SqlTrx)
    {
      Int32 _terminal_id;
      Int64 _session_id;
      Int64 _dummy1;
      Boolean _dummy2;
      String _provider_id;
      String _name;
      WCP_MsgEnrollServer _enroll_request;
      WCP_MsgEnrollServerReply _enroll_response;

      _enroll_request = (WCP_MsgEnrollServer)WcpRequestMessage.MsgContent;
      _enroll_response = (WCP_MsgEnrollServerReply)WcpResponseMessage.MsgContent;

      WcpClient.Identity = WcpRequestMessage.MsgHeader.ServerId;

      if (!WCP_BusinessLogic.DB_GetEnrollData(WcpRequestMessage.MsgHeader.ServerId,
                                              WcpRequestMessage.MsgHeader.TerminalId,
                                              _enroll_request.ProviderId,
                                              TerminalTypes.UNKNOWN,
                                              WCP_TerminalType.IntermediateServer,
                                              out _terminal_id,
                                              out _session_id,
                                              out _enroll_response.LastSequenceId,
                                              out _dummy1,
                                              out _dummy2,
                                              out _provider_id,
                                              out _name,
                                              SqlTrx))
      {
        WcpResponseMessage.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_SERVER_NOT_AUTHORIZED;

        return;
      }

      // AJQ 15-JAN-2010, Don't update the provider. 
      // - The provider was inserted on the 1st enroll, the GUI updates the field
      WcpClient.InternalId = _terminal_id;
      WcpClient.SessionId = _session_id;
      WcpResponseMessage.MsgHeader.ServerSessionId = WcpClient.SessionId;

      // RCI 12-JUL-2010: Add Server Session to Session Cache.
      Common.BatchUpdate.TerminalSession.AddSession(_terminal_id, _session_id, 0, 0, _provider_id, _name, 0);

    } // EnrollServer

    //------------------------------------------------------------------------------
    // PURPOSE : Enroll Terminal
    //
    //  PARAMS :
    //      - INPUT :
    //         - WcpClient
    //         - WcpRequestMessage
    //         - WcpResponseMessage
    //         - SqlTrx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public static void EnrollTerminal(SecureTcpClient WcpClient, WCP_Message WcpRequestMessage, WCP_Message WcpResponseMessage, SqlTransaction SqlTrx)
    {
      Int32 _terminal_id;
      Int64 _session_id;
      Boolean _enrolled;
      String _provider_id;
      String _name;
      WCP_MsgEnrollTerminal _enroll_request;
      WCP_MsgEnrollTerminalReply _enroll_response;

      _enroll_request = (WCP_MsgEnrollTerminal)WcpRequestMessage.MsgContent;
      _enroll_response = (WCP_MsgEnrollTerminalReply)WcpResponseMessage.MsgContent;

      WcpClient.Identity = WcpRequestMessage.MsgHeader.TerminalId;

      if (!WCP_BusinessLogic.DB_GetEnrollData(WcpRequestMessage.MsgHeader.ServerId,
                                              WcpRequestMessage.MsgHeader.TerminalId,
                                              _enroll_request.ProviderId,
                                              _enroll_request.TerminalType,
                                              WCP_TerminalType.GamingTerminal,
                                              out _terminal_id,
                                              out _session_id,
                                              out _enroll_response.LastSequenceId,
                                              out _enroll_response.LastTransactionId,
                                              out _enrolled,
                                              out _provider_id,
                                              out _name,
                                              SqlTrx))
      {
        WcpResponseMessage.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_TERMINAL_NOT_AUTHORIZED;

        return;
      }

      // AJQ 15-JAN-2010, Don't update the provider. 
      // - The provider was inserted on the 1st enroll, the GUI updates the field

      if (_enrolled)
      {
        WcpClient.InternalId = _terminal_id;
        WcpClient.SessionId = _session_id;
        WcpClient.TerminalType = _enroll_request.TerminalType;
        WcpResponseMessage.MsgHeader.TerminalSessionId = WcpClient.SessionId;

        Common.BatchUpdate.TerminalSession.AddSession(_terminal_id, _session_id, 0, 0, _provider_id, _name, (Int16)_enroll_request.TerminalType);

        PlaySession.LoadLastActivePlaySession(_terminal_id, SqlTrx);

        // RCI 15-JUL-2010: Unload GameMeters for terminal.
        WCP_BU_GameMeters.EnqueueUnload(_terminal_id);
        WCP_BU_MachineMeters.EnqueueUnload(_terminal_id);
        WCP_BU_SasMeters.Instance.EnqueueUnload(_terminal_id);

        // RCI 10-MAR-2014: Create VirtualAccount, CashierTerminal and MobileBank associated to the terminal (if they don't exist)
        // RCI & ETP 24-FEB-2016: Do the following only for SAS-Host terminals.
        if (WSI.Common.TITO.Utils.IsTitoMode() && WcpClient.TerminalType == TerminalTypes.SAS_HOST)
        {
          Int32 _cashier_terminal_id;
          Int64 _mb_id;
          Int64 _mb_session_id;
          Int64 _virtual_account_id;
          Boolean _created_virtual_account;
          String _mb_trackdata;

          _cashier_terminal_id = 0;
          _mb_id = 0;
          _mb_session_id = 0;
          _mb_trackdata = "";

          if (!WCP_PlaySessionLogic.WCP_TITO_CreateVirtualAccount(_terminal_id, out _virtual_account_id, out _created_virtual_account, SqlTrx))
          {
            WcpResponseMessage.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_TERMINAL_NOT_AUTHORIZED;

            return;
          }

          if (_created_virtual_account)
          {
            if (!WCP_PlaySessionLogic.WCP_TITO_CreateCashierTerminal(_name, _terminal_id, out _cashier_terminal_id, SqlTrx))
            {
              WcpResponseMessage.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_TERMINAL_NOT_AUTHORIZED;

              return;
            }

            if (!WCP_PlaySessionLogic.WCP_TITO_CreateMobileBank(_terminal_id, out _mb_id, out _mb_session_id, out _mb_trackdata, SqlTrx))
            {
              WcpResponseMessage.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_TERMINAL_NOT_AUTHORIZED;

              return;
            }

            // RCI & LEM 11-NOV-2014: When enrolling a terminal, ensure it has a cashier session and a money collection open.
            if (!WSI.Common.TITO.TITO_ChangeStacker.InitializeStacker(_terminal_id, null, 0, SqlTrx))
            {
              WcpResponseMessage.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_TERMINAL_NOT_AUTHORIZED;

              return;
            }
          }
        }

        // SMN 09-DEC-2015: if IsTITO and terminal enrolled is PROMOBOX, indicates that the promobox needs to receive general parameters.
        if (_enroll_request.TerminalType == TerminalTypes.PROMOBOX)
        {
          WCP_WKT.Promobox_SendGeneralParams(_terminal_id);
        }

        return;
      }

      WcpResponseMessage.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_TERMINAL_NOT_AUTHORIZED;

    } // EnrollTerminal

    //------------------------------------------------------------------------------
    // PURPOSE : Unenroll Server
    //
    //  PARAMS :
    //      - INPUT :
    //         - WcpClient
    //         - WcpRequestMessage
    //         - WcpResponseMessage
    //         - SqlTrx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public static void UnenrollServer(SecureTcpClient WcpClient, WCP_Message WcpRequestMessage, WCP_Message WcpResponseMessage, SqlTransaction SqlTrx)
    {
      Common.BatchUpdate.TerminalSession.CloseSession(WcpClient.InternalId, WcpRequestMessage.MsgHeader.ServerSessionId, (int)WCP_SessionStatus.Closed);
      WcpClient.InternalId = 0;
      WcpClient.SessionId = 0;

    } // UnenrollServer

    //------------------------------------------------------------------------------
    // PURPOSE : Unenroll Terminal
    //
    //  PARAMS :
    //      - INPUT :
    //         - WcpClient
    //         - WcpRequestMessage
    //         - WcpResponseMessage
    //         - SqlTrx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public static void UnenrollTerminal(SecureTcpClient WcpClient, WCP_Message WcpRequestMessage, WCP_Message WcpResponseMessage, SqlTransaction SqlTrx)
    {
      Common.BatchUpdate.TerminalSession.CloseSession(WcpClient.InternalId, WcpRequestMessage.MsgHeader.TerminalSessionId, (int)WCP_SessionStatus.Closed);
      WcpClient.InternalId = 0;
      WcpClient.SessionId = 0;

    } // UnenrollTerminal

  } // classs WCP_PendingEnrollsQueue

} // namespace WSI.WCP
