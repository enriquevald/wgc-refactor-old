//------------------------------------------------------------------------------
// Copyright � 2013 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: TITO_ProcessChangeStacker.cs
// 
//   DESCRIPTION: Process change stacker
//   AUTHOR     : Javier Fernandez
//
//
//   REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 27-SEP-2013 JFC    First version.
// 26-NOV-2013 JRM    Refactoring, code clean up
// 30-NOV-2013 JFC    Get or create system cash session when new stacker is inserted
// 19-FEB-2014 DRV    Removed calls to update meters function, because they must be perfomed before the cash session status is set to pending closing
// 26-FEB-2014 DHA    WIGOSTITO-1092: log message must be shown in english
//------------------------------------------------------------------------------
//

using System;
using System.Collections.Generic;
using System.Text;
using WSI.Common.TITO;
using System.Data.SqlClient;
using WSI.Common;

public static partial class WCP_TITO
{
  public static Boolean TITO_ProcessChangeStacker(Int32 TerminalContainerId,
                                                  Stacker NewStacker,
                                                  Int64 TransactionId,
                                                  SqlTransaction Trx)
  {
    String _description;
    String _stacker_description;
    Boolean _succesful_stacker_change;

    _succesful_stacker_change = WSI.Common.TITO.TITO_ChangeStacker.ChangeStackerInTitoMode(TerminalContainerId, NewStacker, TransactionId, Trx);

    if (!_succesful_stacker_change)
    {
      // Prepare and register alarm
      _stacker_description = NewStacker == null ? "-" : NewStacker.ToString();

      // DHA 26-FEB-2014: log message must be shown in english
      Log.Error(String.Format("Stacker not inserted. Stacker: {0}.", _stacker_description));

      _description = Resource.String("STR_FRM_CONTAINER_EVENTS_DESC_INSERT_STACKER_ERROR",
                                     _stacker_description);
      
      Alarm.Register(AlarmSourceCode.TerminalWCP,
                     TerminalContainerId,
                     "TITO_WCP",
                     (UInt32)AlarmCode.WCP_TITO_StackerChangeFail,
                     _description,
                     AlarmSeverity.Error);
    }

    return _succesful_stacker_change;
  }
}
