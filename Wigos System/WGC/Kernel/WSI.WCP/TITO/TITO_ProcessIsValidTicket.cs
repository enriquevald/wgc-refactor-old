//------------------------------------------------------------------------------
// Copyright � 2013 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: TITO_ProcessIsValidTicket.cs
// 
//   DESCRIPTION: Process get is ticket Valid
//   AUTHOR     : Joshwa Marcalle
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 27-SEP-2013 JRM    First version.
// 08-OCT-2013 NMR    Fixed errors in message processing: IsValidTicket and CancelTicket
// 09-OCT-2013 JRM    Change in member types, removal of some params such as isValid
// 11-OCT-2013 NMR    Fixed some errors while testing
// 26-OCT-2013 JRM    IsValid message call now cancels the ticket
// 26-OCT-2013 JRM    changed ti_cancel_in_progress for new ti_status in enum
// 29-NOV-2013 NMR    Fixed error when testing max ticket amount
// 04-DIC-2013 NMR    Fixed error when testing max ticket amount
// 14-DIC-2013 XIT    Fixed error with max ticket in.
// 14-OCT-2016 JBP    PBI 18246:eBox - Ticket Amount Not Multiple of Machine Denomination: L�gica WCP
// 03-NOV-2016 FJC    PBI 19605:TITO: SAS_HOST - informar motivo de rechazo
//------------------------------------------------------------------------------
//
//
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Threading;
using System.Data.SqlClient;
using WSI.Common;
using WSI.WCP;
using WSI.Common.TITO;

public static partial class WCP_TITO
{
  private static WaitableQueue m_queue_wcp_tito_is_valid_ticket = new WaitableQueue();
  private static WaitableQueue m_queue_wcp_tito_cancel_ticket = new WaitableQueue();
  private static WaitableQueue m_queue_wcp_tito_create_ticket = new WaitableQueue();

  public static WaitableQueue GetQueue(WCP_MsgTypes MsgType)
  {
    if (MsgType == WCP_MsgTypes.WCP_TITO_MsgCreateTicket)
    {
      return m_queue_wcp_tito_create_ticket;
    }
    if (MsgType == WCP_MsgTypes.WCP_TITO_MsgIsValidTicket)
    {
      return m_queue_wcp_tito_is_valid_ticket;
    }
    
    return m_queue_wcp_tito_cancel_ticket;
  }

  internal class WCP_Context
  {
    internal SecureTcpClient tcp_client;
    internal WCP_Message request;
    internal WCP_Message response;
  }

  //------------------------------------------------------------------------------
  // PURPOSE: Process IsValidTicket Message
  //
  //  PARAMS:
  //      - INPUT: Input Message ( blank ) 
  //
  //      - OUTPUT: Message with ticket validity information
  //
  // RETURNS:
  private static void TITO_EnqueueMessage(SecureTcpClient TcpClient, WCP_Message TitoRequest, WCP_Message TitoResponse)
  {
    WCP_Context _ctx;
    WaitableQueue _queue;

    _ctx = new WCP_Context();
    _ctx.tcp_client = TcpClient;
    _ctx.request = TitoRequest;
    _ctx.response = TitoResponse;

    _queue = GetQueue(TitoRequest.MsgHeader.MsgType);
    _queue.Enqueue(_ctx);
  }

  private static void TaskTitoBatch(Object Table)
  {
    WCP_Context _ctx;
    DataTable _table;
    Boolean _processed;
    WCP_TITO_MsgIsValidTicket _request;
    WCP_TITO_MsgIsValidTicketReply _response;
    WCP_TITO_MsgCancelTicket _request_cancel;
    WCP_TITO_MsgCancelTicketReply _response_cancel;
    int _num_valid;
    int _num_valid_cancel;

    _table = (DataTable)Table;
    _processed = false;

    try
    {
       // Preprocess        
      StringBuilder _sb = new StringBuilder();
      _sb.AppendLine("EXECUTE   TITO_ChangeTicketStatus");
      _sb.AppendLine("          @pTerminalId	");
      _sb.AppendLine("        , @pTicketValidationNumber   ");
      _sb.AppendLine("        , @pNewStatus");
      _sb.AppendLine("        , @pMsgSequenceId");
      _sb.AppendLine("        , @pDefaultAllowRedemption   ");
      _sb.AppendLine("        , @pDefaultMaxAllowedTicketIn");
      _sb.AppendLine("        , @pDefaultMinAllowedTicketIn");
      _sb.AppendLine("        , @pTicketRejectReasonEgm    ");
      _sb.AppendLine("        , @pTicketRejectReasonWcp    ");
      _sb.AppendLine("        , @pTicketId         OUTPUT  ");
      _sb.AppendLine("        , @pTicketType       OUTPUT  ");
      _sb.AppendLine("        , @pTicketAmount     OUTPUT  ");
      _sb.AppendLine("        , @pTicketExpiration OUTPUT  ");

      using (DB_TRX _db_trx = new DB_TRX())
      {
        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
        {
          // input
          _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).SourceColumn = "TerminalId";
          _sql_cmd.Parameters.Add("@pTicketValidationNumber", SqlDbType.BigInt).SourceColumn = "ValidationNumber";
          _sql_cmd.Parameters.Add("@pNewStatus", SqlDbType.Int).SourceColumn = "NewStatus";
          _sql_cmd.Parameters.Add("@pMsgSequenceId", SqlDbType.BigInt).SourceColumn = "MsgSequenceId";
          _sql_cmd.Parameters.Add("@pDefaultAllowRedemption", SqlDbType.Bit).Value = GeneralParam.GetBoolean("TITO", "AllowRedemption");
          _sql_cmd.Parameters.Add("@pDefaultMaxAllowedTicketIn", SqlDbType.Money).Value = GeneralParam.GetDecimal("TITO", "Tickets.MaxAllowedTicketIn", 0);
          _sql_cmd.Parameters.Add("@pDefaultMinAllowedTicketIn", SqlDbType.Money).Value = GeneralParam.GetDecimal("TITO", "Tickets.MinAllowedTicketIn", 0);
          _sql_cmd.Parameters.Add("@pTicketRejectReasonEgm", SqlDbType.BigInt).SourceColumn = "MachineStatus";
          _sql_cmd.Parameters.Add("@pTicketRejectReasonWcp", SqlDbType.BigInt).SourceColumn = "ValidateStatus";

          // output
          _sql_cmd.Parameters.Add("@pTicketId", SqlDbType.BigInt, 0, "TicketId").Direction = ParameterDirection.Output;
          _sql_cmd.Parameters.Add("@pTicketType", SqlDbType.Int, 0, "TicketType").Direction = ParameterDirection.Output;
          _sql_cmd.Parameters.Add("@pTicketAmount", SqlDbType.Money, 0, "TicketAmount").Direction = ParameterDirection.Output;
          _sql_cmd.Parameters.Add("@pTicketExpiration", SqlDbType.DateTime, 0, "TicketExpiration").Direction = ParameterDirection.Output;

          // BatchUpdate
          using (SqlDataAdapter _sql_adapt = new SqlDataAdapter())
          {
            _sql_adapt.InsertCommand = _sql_cmd;
            _sql_adapt.InsertCommand.UpdatedRowSource = UpdateRowSource.OutputParameters;
            _sql_adapt.UpdateBatchSize = 1000;
            _sql_adapt.ContinueUpdateOnError = true;
            Int32 updated = _sql_adapt.Update(_table);
            _db_trx.Commit();
            _processed = true;
          }
        }
      }
    }
    catch (Exception _ex)
    {
      _processed = false;

      Log.Exception(_ex);
    }

    try
    {
      _num_valid = 0;
      _num_valid_cancel = 0;

      // Send responses
      foreach (DataRow _row in _table.Rows)
      {
        try
        {
          _ctx = (WCP_Context)_row["CTX"];

          switch (_ctx.request.MsgHeader.MsgType)
          {
            case WCP_MsgTypes.WCP_TITO_MsgIsValidTicket:
              _request = (WCP_TITO_MsgIsValidTicket)_ctx.request.MsgContent;
              _response = (WCP_TITO_MsgIsValidTicketReply)_ctx.response.MsgContent;

              // Ticket not valid as default
              _response.AmountCents = 0;
              _response.ExpirationDate = DateTime.MaxValue; 
              _response.PoolId = 0;
              _response.SystemId = _request.SystemId;
              _response.ValidateStatus = (Int32)ENUM_TITO_IS_VALID_TICKET_MSG.UNABLE_TO_VALIDATE;
              if (!_row.IsNull("TicketIdOutput"))
              {
                _response.TicketId = (Int64)_row["TicketIdOutput"];
              }
              _ctx.response.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_OK;

              if (_processed
                  && !_row.IsNull("TicketId")
                  && !_row.IsNull("TicketType")
                  && !_row.IsNull("TicketAmount"))
              {
                TITO_TICKET_TYPE _type;

                _type = (TITO_TICKET_TYPE)_row["TicketType"];


                switch (_type)
                {
                  case TITO_TICKET_TYPE.CASHABLE:
                    _response.ValidateStatus = (Int32)ENUM_TITO_IS_VALID_TICKET_MSG.VALID_CASHABLE;
                    _response.AmountCents = (Int64)(100m * (Decimal)_row["TicketAmount"]);
                    break;

                  case TITO_TICKET_TYPE.PROMO_REDEEM:
                    _response.ValidateStatus = (Int32)ENUM_TITO_IS_VALID_TICKET_MSG.VALID_NONRESTRICTED_PROMOTIONAL_TICKET;
                    _response.AmountCents = (Int64)(100m * (Decimal)_row["TicketAmount"]);
                    break;

                  case TITO_TICKET_TYPE.PROMO_NONREDEEM:
                    _response.ValidateStatus = (Int32)ENUM_TITO_IS_VALID_TICKET_MSG.VALID_RESTRICTED_PROMOTIONAL_TICKET;
                    _response.AmountCents = (Int64)(100m * (Decimal)_row["TicketAmount"]);
                    if (!_row.IsNull("TicketExpiration"))
                    {
                      _response.ExpirationDate = (DateTime)_row["TicketExpiration"];
                    }


                    // TODO: Future use
                    // Conditionally send the PoolId
                    break;

                  case TITO_TICKET_TYPE.HANDPAY:
                  case TITO_TICKET_TYPE.JACKPOT:
                  case TITO_TICKET_TYPE.OFFLINE:
                  default:
                    // Do nothing
                    break;

                } // switch (_type) TITO_TICKET_TYPE
                
                if (_response.AmountCents > 0) _num_valid++;
                else
                {
                  Log.Warning(String.Format("** Could not validate ticket. TerminalId:{0}, ValidationNumber:{1} ", _row["TerminalId"], _row["ValidationNumber"]));
                }
              } // if

              // AJQ 03-APR-2014 Avoid using DateTime.MaxValue, this value is modified due time-zone information by the protocol
              if (_response.ExpirationDate > WGDB.Now.AddYears(1))
              {
                _response.ExpirationDate = WGDB.Now.AddYears(1);
              }

              break;

            case WCP_MsgTypes.WCP_TITO_MsgCancelTicket:
              _request_cancel = (WCP_TITO_MsgCancelTicket)_ctx.request.MsgContent;
              _response_cancel = (WCP_TITO_MsgCancelTicketReply)_ctx.response.MsgContent;


              if ( _request_cancel.StopCancellation 
                && !_row.IsNull("TicketIdOutput")
                && _request_cancel.ValidateStatus != ENUM_TITO_IS_VALID_TICKET_MSG.VALID_CASHABLE
                && _request_cancel.ValidateStatus != ENUM_TITO_IS_VALID_TICKET_MSG.VALID_RESTRICTED_PROMOTIONAL_TICKET
                && _request_cancel.ValidateStatus != ENUM_TITO_IS_VALID_TICKET_MSG.VALID_NONRESTRICTED_PROMOTIONAL_TICKET)
              {
                using (DB_TRX _db_trx = new DB_TRX())
                {
                  if ((Int64)_row["TicketIdOutput"] <= 0)
                  {
                    if (Ticket.DB_InsertTicketsAuditStatusChange(_request_cancel.ValidationNumberSequence, 
                                                                 _request_cancel.MachineStatus,
                                                                 _request_cancel.ValidateStatus, 
                                                                 _db_trx.SqlTransaction))
                    {
                      _db_trx.Commit();
                    }
                    else
                    {
                      Log.Error("Error in DB_InsertTicketsAuditStatusChange(). ValidationNumber: " + _request_cancel.ValidationNumberSequence.ToString());
                    }
                  }
                  else
                  {
                    if (Ticket.DB_UpdateTicketRejectReason((Int64)_row["TicketIdOutput"],
                                                           _request_cancel.MachineStatus,
                                                           _request_cancel.ValidateStatus,
                                                           _db_trx.SqlTransaction))
                    {
                      _db_trx.Commit();
                    }
                    else
                    {
                      Log.Error("Error in DB_UpdateTicketRejectReason(). ValidationNumber: " + _request_cancel.ValidationNumberSequence.ToString());
                    }
                  }
                }
              }

              if (!_processed)
              {
                _ctx.response.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_DATABASE_OFFLINE;
              }
              else
              {
                // Don't care whether the ticket exists or not. Answer OK to the sas-host to avoid any retry
                _ctx.response.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_OK;

                if (_row.IsNull("TicketId"))
                {
                  // TODO: SAS-Host reporting to Cancel or revert Cancel a ticket that doesn't exist.
                  Log.Warning(String.Format("** Could not cancel ticket. TerminalId:{0}, ValidationNumber:{1} ", _row["TerminalId"], _row["ValidationNumber"]));
                }
                else
                {
                  _num_valid_cancel++;
                }
              }
              break;

          }// switch (_ctx.request.MsgHeader.MsgType)

          _ctx.tcp_client.Send(_ctx.response.ToXml());
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }
      } // foreach (DataRow _row in _table.Rows)
    }
    catch (Exception _ex)
    {
      Log.Exception(_ex);
    }
  }

  public static void TitoChangeTicketStatusThread(Object Queue)
  {
    WaitableQueue _queue;
    Object _item;
    WCP_Context _ctx;

    DataTable _table;
    DataRow _row;
    int _tick0;
    int _timeout;
    int _max_wait;
    long _elapsed;

    WCP_TITO_MsgIsValidTicket _request;
    WCP_TITO_MsgCancelTicket _request_cancel;


    _queue = (WaitableQueue)Queue;

    _ctx = null;
    _request = null;
    _request_cancel = null;
    _tick0 = 0;

    int _tick_log = Misc.GetTickCount();


    while (true)
    {
      try
      {
        _max_wait = GeneralParam.GetInt32("", "", 10);
        _max_wait = Math.Max(0, Math.Min(1000, _max_wait));

        _table = new DataTable();
        _table.Columns.Add("CTX", Type.GetType("System.Object")); 
        _table.Columns.Add("TerminalId", Type.GetType("System.Int32"));
        _table.Columns.Add("ValidationNumber", Type.GetType("System.Int64"));
        _table.Columns.Add("NewStatus", Type.GetType("System.Int32"));
        _table.Columns.Add("MsgSequenceId", Type.GetType("System.Int64"));
        _table.Columns.Add("TicketId", Type.GetType("System.Int64"));
        _table.Columns.Add("TicketType", Type.GetType("System.Int32"));
        _table.Columns.Add("TicketAmount", Type.GetType("System.Decimal"));
        _table.Columns.Add("TicketExpiration", Type.GetType("System.DateTime"));
        _table.Columns.Add("MachineStatus", Type.GetType("System.Int64"));
        _table.Columns.Add("ValidateStatus", Type.GetType("System.Int64"));
        _table.Columns.Add("TicketIdOutput", Type.GetType("System.Int64"));

        _table.Rows.Clear();
        _tick0 = Misc.GetTickCount();
        _timeout = Timeout.Infinite;
        while (_queue.Dequeue(out _item, _timeout))
        {
          _ctx = (WCP_Context)_item;

          if (_ctx.tcp_client.IsConnected)
          {
            switch (_ctx.request.MsgHeader.MsgType)
            {
              case WCP_MsgTypes.WCP_TITO_MsgIsValidTicket:
                _request = (WCP_TITO_MsgIsValidTicket)_ctx.request.MsgContent;

                _row = _table.NewRow();
                _row["CTX"] = _ctx;
                _row["TerminalId"] = _ctx.tcp_client.InternalId;
                _row["ValidationNumber"] = ValidationNumberManager.GetCalculatedValidationNumber(_request.SystemId, _request.ValidationNumberSequence);
                _row["NewStatus"] = TITO_TICKET_STATUS.PENDING_CANCEL;
                _row["MsgSequenceId"] = _ctx.request.MsgHeader.SequenceId;

                _table.Rows.Add(_row);

                // Se valida el status del tiquet. Si es Pending Printing se informa a Log de que se est� intentando validar un tiket en estado 6
                // ser� el stored procedure quin permita validarlo o no.
                  using (DB_TRX _db_trx = new DB_TRX())
                  {
                    StringBuilder _sb = new StringBuilder();

                  _sb.Append(" SELECT   TI_TICKET_ID                              ");
                  _sb.Append("        , TI_STATUS                                 ");
                  _sb.Append("   FROM                                             ");
                  _sb.Append("TICKETS                                             ");
                  _sb.Append("  WHERE   TI_VALIDATION_NUMBER = @pValidationNumber ");

                    using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
                    {
                    _sql_cmd.Parameters.Add("@pValidationNumber", SqlDbType.BigInt).Value = (_row["ValidationNumber"]);

                      using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
                      {
                        if (_reader.Read() && !_reader.IsDBNull(0))
                        {
                        _row["TicketIdOutput"] = (Int64)_reader["TI_TICKET_ID"];
                        if (Convert.ToInt16(_reader["TI_STATUS"].ToString()) == (int)TITO_TICKET_STATUS.PENDING_PRINT)
                          {
                          Log.Message("Ticket " + _row["ValidationNumber"].ToString() + " has status PENDING PRINTING");
                          }
                        }
                      }
                    }
                  }
                // Fin de la validaci�n

                break;

              case WCP_MsgTypes.WCP_TITO_MsgCancelTicket:
                _request_cancel = (WCP_TITO_MsgCancelTicket)_ctx.request.MsgContent;
                _row = _table.NewRow();
                _row["CTX"] = _ctx;
                _row["TerminalId"] = _ctx.tcp_client.InternalId;
                _row["ValidationNumber"] = ValidationNumberManager.GetCalculatedValidationNumber(_request_cancel.SystemId, _request_cancel.ValidationNumberSequence);
                _row["NewStatus"] = (_request_cancel.StopCancellation) ? TITO_TICKET_STATUS.VALID : TITO_TICKET_STATUS.CANCELED;
                _row["MachineStatus"] = (Int64)_request_cancel.MachineStatus;
                _row["ValidateStatus"] = (Int64)_request_cancel.ValidateStatus;
                _row["TicketIdOutput"] = _request_cancel.TicketId;
                _row["MsgSequenceId"] = _ctx.request.MsgHeader.SequenceId;

                _table.Rows.Add(_row);
                break;

            }

            if (_table.Rows.Count == 1)
            {
              _tick0 = Misc.GetTickCount();
              _timeout = _max_wait;
            }

            if (_table.Rows.Count >= 1000) break;
          }

          if (_table.Rows.Count == 0)
          {
            _timeout = Timeout.Infinite;
            continue;
          }

          _elapsed = Misc.GetElapsedTicks(_tick0);
          if (_elapsed < _max_wait)
          {
            _timeout = (Int32)(_max_wait - _elapsed);
          }
          else
          {
            // Continue dequeueing items even we reached the 500 ms. 
            _timeout = 0;
          }
        } // while

        if (_table.Rows.Count <= 0)
        {
          continue;
        }

        TaskTitoBatch(_table);

        //ThreadPool.QueueUserWorkItem(new WaitCallback(TaskTitoBatch), _table);

        //////if (Misc.GetElapsedTicks(_tick_log) > 10000 && _request != null )
        //////{
        //////  _tick_log = Misc.GetTickCount();

        //////  int _avail;
        //////  int _max;
        //////  int _min;
        //////  int _b;
        //////  ThreadPool.GetMaxThreads(out _max, out _b);
        //////  ThreadPool.GetMinThreads(out _min, out _b);
        //////  ThreadPool.GetAvailableThreads(out _avail, out _b);

        //////  Log.Message(String.Format("ThreadPool, Max: {0}, Min: {1}, Available: {2}", _max, _min, _avail));

        //////  _tick_log = Misc.GetTickCount();
        //////}


        // TaskTitoBatch(_table);

        ////Log.Message(String.Format("ThreadTicketsToChangeStatus Tickets group: {0,4},    Valid: {1,4}, Time: {2,5}. MsgIsValidTicket {3}, MsgCancelTicket {4}."
        ////                                               , _table.Rows.Count, _num_valid + _num_valid_cancel, Misc.GetElapsedTicks(_tick0), _num_valid, _num_valid_cancel));

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    } // while (true)
  }
}



