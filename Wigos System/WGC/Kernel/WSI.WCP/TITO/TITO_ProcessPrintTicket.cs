 
//------------------------------------------------------------------------------
// Copyright � 2013 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME:  TITO_ProcessPrintTicket.cs
// 
//   DESCRIPTION: Process print ticket Message
//   AUTHOR     : Joshwa Marcalle
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 03-OCT-2013 JRM    First version.
//------------------------------------------------------------------------------
//
// 
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Threading;
using System.Data.SqlClient;
using WSI.Common;
using WSI.WCP;

public static partial class WCP_TITO
{

  //------------------------------------------------------------------------------
  // PURPOSE: Process TITO_ProcessPrintTicket Message
  //
  //  PARAMS:
  //      - INPUT: Input Message ( blank ) 
  //
  //      - OUTPUT: 
  //
  // RETURNS:
  private static void TITO_ProcessPrintTicket(WCP_Message TitoRequest, WCP_Message TitoResponse)
  {
    WCP_TITO_MsgPrintTicket _request;
    WCP_TITO_MsgPrintTicketReply _response;

    Ticket _tito_ticket;
    Boolean _trx;

    _tito_ticket = new Ticket();

    String _validation_number;

    _request  = (WCP_TITO_MsgPrintTicket)TitoRequest.MsgContent;
    _response = (WCP_TITO_MsgPrintTicketReply)TitoResponse.MsgContent;
   
    _tito_ticket.ValidationType       = _request.ValidationType;
    _tito_ticket.CreatedDateTime      =  WGDB.Now;
    _tito_ticket.ExpirationDateTime   = new DateTime(_request.ExpirationDate);
    _tito_ticket.ValidationNumber     = _request.ValidationNumber;
    _tito_ticket.Amount               = _request.Amount;
    _tito_ticket.ValidationSystemID   = _request.ValidationSystemID;
    _tito_ticket.PoolID               = _request.PoolID;
    _tito_ticket.CreatedTerminalExternalName  = _request.TerminalID;
    _tito_ticket.CreatedTerminalType  = _request.TerminalType;
    // JRM TODO: this goes from account logged in. In case of anonymous, then it goes from the session anonymous account, TBD
    _tito_ticket.CreatedAccountID     = _request.CreatedAccountID;
    _tito_ticket.CreatedCashierSession= _request.CreatedCashierSession;
    _tito_ticket.TypeId               = _request.TicketType;

    try
    {
      _validation_number = _request.ValidationNumber;

      using (DB_TRX _db_trx = new DB_TRX())
      {
        // Check Ticket Validity
        _trx = _tito_ticket.DB_CreateTicket();

        _db_trx.Commit();

        if (!_trx)
        {
          TitoResponse.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_TITO_NOT_ALLOWED_ISSUANCE;
          return;
        }

        TitoResponse.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_OK;
      }
    }
    catch (Exception _ex)
    {
      Log.Exception(_ex);

      TitoResponse.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_TITO_TICKET_ERROR;
    }

  } // TITO_ProcessPrintTicket  
}
