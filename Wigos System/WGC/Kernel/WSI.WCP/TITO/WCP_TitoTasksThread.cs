//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_TitoTasksThread.cs
// 
//   DESCRIPTION: WCP_TitoTasksThread thread to perform tasks of messages wcp
// 
//        AUTHOR: Dani Dom�nguez 
// 
// CREATION DATE: 28-JAN-2014
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 28-JAN-2014 DDM    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using WSI.Common;
using System.Threading;
using System.Data;
using System.Collections;
using System.Data.SqlClient;

namespace WSI.WCP
{
  public static class WCP_TitoTasksThread_Moved
  {
    //------------------------------------------------------------------------------
    // PURPOSE : Initializes TitoTasksStatus thread
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public static void Init()
    {
      Thread _thread;

      _thread = new Thread(TitoTasksStatusThread);
      _thread.Name = "TitoTasksStatusThread";
      _thread.Start();

      for (int _i = 0; _i < 10; _i++)
      {
        _thread = new Thread(WCP_TITO.TitoChangeTicketStatusThread);
        _thread.Name = "WCP_TITO_MsgIsValidTicket";
        _thread.Start(WCP_TITO.GetQueue(WCP_MsgTypes.WCP_TITO_MsgIsValidTicket));

        _thread = new Thread(WCP_TITO.TitoChangeTicketStatusThread);
        _thread.Name = "WCP_TITO_MsgCancelTicket";
        _thread.Start(WCP_TITO.GetQueue(WCP_MsgTypes.WCP_TITO_MsgCancelTicket));
      }


      for (int _i = 0; _i < 80; _i++)
      {
        _thread = new Thread(WCP_TITO.TitoCreateTicketThread);
        _thread.Name = "WCP_TITO_MsgCreateTicket";
        _thread.Start(WCP_TITO.GetQueue(WCP_MsgTypes.WCP_TITO_MsgCreateTicket));
      }
      _thread = new Thread(WCP_TITO.TitoValidationNumberThread);
      _thread.Name = "TitoValidationNumberThread";
      _thread.Start();

    } // Init

    //------------------------------------------------------------------------------
    // PURPOSE : TitoTasksStatusThread thread
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private static void TitoTasksStatusThread()
    {
      StringBuilder _sb;
      DataTable _pending_tickets;
      StringBuilder _sb_error;
      int _wait_hint;

      _sb = new StringBuilder();
      _sb_error = new StringBuilder();
      _pending_tickets = new DataTable();

      _wait_hint = 20000;

      while (true)
      {
        try
        {
          Thread.Sleep(_wait_hint);
          _wait_hint = 60000;

          if (WGDB.ConnectionState != ConnectionState.Open)
          {
            continue;
          }
          if (!Services.IsPrincipal("WCP"))
          {
            continue;
          }

          _pending_tickets.Clear();

          _sb.Length = 0;



          _sb.AppendLine("SELECT   TT_TASK_UNIQUE_ID      ");
          _sb.AppendLine("       , TI_TICKET_ID           ");
          _sb.AppendLine("       , TI_LAST_ACTION_TERMINAL_ID  ");
          _sb.AppendLine("       , ISNULL(TI_TRANSACTION_ID, 0) TI_TRANSACTION_ID");
          _sb.AppendLine("       , TI_VALIDATION_TYPE     ");
          _sb.AppendLine("       , TI_VALIDATION_NUMBER   ");
          _sb.AppendLine("       , TI_AMOUNT              ");
          _sb.AppendLine("  FROM   TITO_TASKS ");
          _sb.AppendLine("INNER JOIN	TICKETS ON TT_TICKET_ID = TI_TICKET_ID");



          using (DB_TRX _db_trx = new DB_TRX())
          {
            using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
            {
              using (SqlDataAdapter _sql_dap = new SqlDataAdapter(_sql_cmd))
              {
                _sql_dap.Fill(_pending_tickets);
              }
            }
          }

          if (_pending_tickets.Rows.Count == 0)
          {
            _wait_hint = 10000;

            continue;
          }

          _wait_hint = 1000;
          foreach (DataRow _pending_ticket in _pending_tickets.Rows)
          {
            if (WGDB.ConnectionState != ConnectionState.Open)
            {
              break;
            }

            using (DB_TRX _db_trx = new DB_TRX())
            {
              if (!WCP_TITO.Tito_ProcessCancelTicket((Int64)_pending_ticket["TI_TICKET_ID"],
                                                     (Int32)_pending_ticket["TI_LAST_ACTION_TERMINAL_ID"],
                                                     (Int64)_pending_ticket["TI_TRANSACTION_ID"],
                                                     (Int64)_pending_ticket["TI_VALIDATION_NUMBER"],
                                                     (Decimal)_pending_ticket["TI_AMOUNT"], _db_trx.SqlTransaction))
              {
                _sb_error.Length = 0;
                _sb_error.AppendLine(" TaskStatusThread: Error in process cancel ticket   ");
                _sb_error.AppendFormat(" - UniqueId: {0}, ", _pending_ticket["TT_TASK_UNIQUE_ID"]);
                _sb_error.AppendLine();
                _sb_error.AppendFormat(" - TerminalId: {0} ", _pending_ticket["TI_LAST_ACTION_TERMINAL_ID"]);
                _sb_error.AppendLine();
                _sb_error.AppendFormat(" - TicketId: {0} ", _pending_ticket["TI_TICKET_ID"]);

                Log.Error(_sb_error.ToString());

                continue;
              }

              _sb.Length = 0;
              _sb.AppendLine("DELETE   TITO_TASKS");
              _sb.AppendLine(" WHERE   TT_TASK_UNIQUE_ID = @pUniqueId");

              using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
              {
                _sql_cmd.Parameters.Add("@pUniqueId", SqlDbType.BigInt).Value = _pending_ticket["TT_TASK_UNIQUE_ID"];

                if (_db_trx.ExecuteNonQuery(_sql_cmd) != 1)
                {
                  Log.Warning(String.Format("TaskStatusThread: Not deleted all record on table TITO_TASKS. UniqueId {0}", _pending_ticket["TT_TASK_UNIQUE_ID"]));

                  continue;
                }
              }
              _db_trx.Commit();
            }// _db_trx
          } // foreach
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }

      }
    } // TitoTasksStatusThread
  }
}
