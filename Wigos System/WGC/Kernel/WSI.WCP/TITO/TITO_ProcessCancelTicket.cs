//------------------------------------------------------------------------------
// Copyright � 2013 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: TITO_ProcessCancelTicket.cs
// 
//   DESCRIPTION: Process cancelation of a ticket
//   AUTHOR     : Joshwa Marcalle
//
//
//   REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 27-SEP-2013 JRM    First version.
// 09-OCT-2013 JRM    Change member types
// 26-OCT-2013 JRM    Cancel ticket action creates new play session if none exists. A virtual account is used for this session
// 19-NOV-2013 NMR    Changed process for TicketIn movements
// 26-NOV-2013 JRM    Refactoring and code cleaning
// 28-NOV-2013 JRM    Return codes mus be OK or ERROR
// 10-DIC-2013 NMR    Saved related play-session ID
// 11-DIC-2013 NMR    Changed algo to get Money-Collection
// 19-DIC-2013 JRM    Changed all Sessions Acquirering methods for faster ones.
// 24-JAN-2014 JRM    Added Provider Id to the session objects
// 25-APR-2014 DHA & JML Rename/Added TITO Cashier/Account Movements
// 08-JUN-2015 MPO    WIG-2411: When cancel PromoNR is needed update TE_ACCOUNT_PROMO
// 04-SEP-2015 YNM    TFS-4096: Cashier: screen last inserted bills and tickets
// 13-JAN-2016 DHA    Task 8287: Floor Dual Currency
// 28-JAN-2016 DHA    Product Backlog Item 8351:Floor Dual Currency: Movimientos de tickets TITO en M.N.
//------------------------------------------------------------------------------
//
//
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Threading;
using System.Data.SqlClient;
using WSI.Common;
using WSI.WCP;
using WSI.Common.TITO;

public static partial class WCP_TITO
{
  //------------------------------------------------------------------------------
  // PURPOSE: Process CancelTicket Message
  //
  //  PARAMS:
  //      - INPUT: Input Message ( blank ) 
  //
  //      - OUTPUT: Messsage with cancel operation outcome. Outcome is expressed with RC Code
  //
  // RETURNS:
  //
  public static Boolean Tito_ProcessCancelTicket(Int64 TicketId, Int32 TerminalId, Int64 ValidationNumber, Decimal Amount, TITO_TICKET_TYPE TicketType, SqlTransaction Trx)
  {
    String _terminal_name;
    Int64 _play_session_id;
    Int64 _cashier_session_id;
    Int64 _money_collection_id;
    String _virtual_track_data;
    Int32 _cashier_terminal_id;
    Int64 _mobile_bank_id;
    String _mobile_bank_track_data;
    Int64 _mobile_bank_session_id;
    Int32 _user_id;
    String _user_name;
    String _provider_id;
    String _cashier_name;
    Int64 _virtual_account_id;
    Int64 _account_promotion_id;

    // Decipher validation number to get System Id
    try
    {
      if (!WCP_PlaySessionLogic.GetTitoSessionObjects(TerminalId,
                                                      0,
                                                      true,
                                                      MB_USER_TYPE.SYS_TITO,
                                                      out _terminal_name,
                                                      out _play_session_id,
                                                      out _cashier_session_id,
                                                      out _money_collection_id,
                                                      out _virtual_account_id,
                                                      out _virtual_track_data,
                                                      out _cashier_terminal_id,
                                                      out _mobile_bank_id,
                                                      out _mobile_bank_track_data,
                                                      out _mobile_bank_session_id,
                                                      out _user_name,
                                                      out _user_id,
                                                      out _provider_id,
                                                      out _cashier_name,
                                                      Trx))
      {
        Log.Error("Error in GetTitoSessionObjects");

        return false;
      }
      // DHA 13-JAN-2016: pass terminalId as parameter for dual currency
      if (!Tito_SetTicketSessionAndColletion(TicketId, _play_session_id, _money_collection_id, TerminalId, Trx))
      {
        Log.Error("Error in Tito_SetTicketSessionAndColletion");

        return false;
      }

      if (!WCP_PlaySessionLogic.GenerateTicketInAccountMovements(TerminalId,
                                                                 _terminal_name,
                                                                 _play_session_id,
                                                                 _cashier_session_id,
                                                                 Amount,
                                                                 _virtual_account_id,
                                                                 0,
                                                                 0,
                                                                 TicketType,
                                                                 Trx))
      {
        Log.Error("Error in GenerateTicketInAccountMovements");

        return false;
      }

      if (!WCP_PlaySessionLogic.GenerateTicketInCashierMovements(TerminalId,
                                                                 _terminal_name,
                                                                 Amount,
                                                                 _virtual_account_id,
                                                                 _virtual_track_data,
                                                                 TicketType,
                                                                 TicketId,
                                                                 ValidationNumber,
                                                                 Trx))
      {
        Log.Error("Error in GenerateTicketInCashierMovements");

        return false;
      }

      // DRV 15-DEC-2014: Promotion cost for promotional redeemable tickets is setted whith the ticket amount: 
      // MPO 08-JUN-2015: WIG-2411: When cancel PromoNR is needed update TE_ACCOUNT_PROMO
      if (TicketType == TITO_TICKET_TYPE.PROMO_REDEEM || TicketType == TITO_TICKET_TYPE.PROMO_NONREDEEM)
      //if (TicketType == TITO_TICKET_TYPE.PROMO_REDEEM)
      {
        if (!WCP_TITO.Tito_GetTicketAccountPromotionID(TicketId, Trx, out _account_promotion_id))
        {
          Log.Error("Error in Tito_GetTicketAccountPromotionID");

          return false;
        }

        if (_account_promotion_id > 0)
        {
          if (TicketType == TITO_TICKET_TYPE.PROMO_REDEEM)
          {
            if (!MultiPromos.Trx_UpdateAccountPromotionCost(Amount, _account_promotion_id, Trx))
            {
              Log.Error("Error in Trx_UpdateAccountPromotionCost");

              return false;
            }
          }
          else if (TicketType == TITO_TICKET_TYPE.PROMO_NONREDEEM)
          {
            if (!MultiPromos.DB_UpdateTerminalAccountPromotion(TerminalId, _account_promotion_id, Trx))
            {
              Log.Error("Error in DB_UpdateTerminalAccountPromotion");

              return false;
            }
          }
        }
      }

      // ICS 02-JAN-2014: Increase terminal stacker counter
      if (!TerminalStatusFlags.SetTerminalStatus(TerminalId, TerminalStatusFlags.WCP_TerminalEvent.TicketIn, Trx))
      {
        Log.Error("TITO_ProcessCancelTicket --> SetTerminalStatus: return false");

        return false;
      }

      return true;

    }
    catch (Exception _ex)
    {
      Log.Exception(_ex);
    }

    return false;
  } // TITO_ProcessCancelTicket

  public static Boolean Tito_GetTicketAccountPromotionID(Int64 TicketId, SqlTransaction SqlTrx, out Int64 AccountPromotion)
  {
    StringBuilder _sb;

    AccountPromotion = 0;

    _sb = new StringBuilder();
    _sb.AppendLine(" SELECT   TI_ACCOUNT_PROMOTION ");
    _sb.AppendLine("   FROM   TICKETS ");
    _sb.AppendLine("  WHERE   TI_TICKET_ID = @pTicketId ");

    try
    {
      using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
      {
        _cmd.Parameters.Add("@pTicketId", SqlDbType.BigInt).Value = TicketId;

        AccountPromotion = (Int64)_cmd.ExecuteScalar();

        return true;
      }
    }
    catch (Exception _ex)
    {
      Log.Exception(_ex);
    }

    return false;
  }

  //------------------------------------------------------------------------------
  // PURPOSE: Set MoneyCollectionID & CanceledPlaySessionId in ticket
  //
  //  PARAMS:
  //      - INPUT: TicketId
  //               PlaySessionId
  //               MoneyCollectionId
  //      - OUTPUT: 
  //
  // RETURNS: True if ok, false otherwise
  //
  public static Boolean Tito_SetTicketSessionAndColletion(Int64 TicketId, Int64 PlaySessionId, Int64 MoneyCollectionId, Int32 TerminalId, SqlTransaction Trx)
  {
    StringBuilder _sb;

    try
    {
      _sb = new StringBuilder();
      _sb.AppendLine("UPDATE   TICKETS");
      _sb.AppendLine("   SET   TI_MONEY_COLLECTION_ID      = @pMoneyCollectionId ");
      _sb.AppendLine("       , TI_CANCELED_PLAY_SESSION_ID = @pPlaySessionId ");
      _sb.AppendLine(" WHERE   TI_TICKET_ID  = @pTicketId ");

      _sb.AppendLine("IF @@ROWCOUNT > 0 ");
      _sb.AppendLine("   BEGIN ");
      _sb.AppendLine("   ");
      _sb.AppendLine("     DECLARE @_ti_type AS INTEGER ");
      _sb.AppendLine("     DECLARE @_ti_amount AS MONEY ");
      _sb.AppendLine("    ");

      // DHA 13-JAN-2016: get ticket amount converted to currency machine
      if (Misc.IsFloorDualCurrencyEnabled())
      {
        _sb.AppendLine("             DECLARE   @_terminal_iso_code AS NVARCHAR(3)    ");
        _sb.AppendLine("              SELECT   @_terminal_iso_code = TE_ISO_CODE   FROM TERMINALS   WHERE TE_TERMINAL_ID = @pTerminalId    ");

        _sb.AppendLine("              SELECT   @_ti_type = TI_TYPE_ID    ");
        _sb.AppendLine("                     , @_ti_amount =             ");
        _sb.AppendLine("                            CASE WHEN TI_AMT1 IS NULL AND @_terminal_iso_code = @pNationalCurrency THEN TI_AMOUNT    ");
        _sb.AppendLine("                                WHEN TI_AMT1 IS NULL AND @_terminal_iso_code <> @pNationalCurrency THEN 0           ");
        _sb.AppendLine("                                WHEN TI_CUR1 <> @_terminal_iso_code AND @_terminal_iso_code = @pNationalCurrency    THEN TI_AMOUNT   ");
        _sb.AppendLine("                                WHEN TI_CUR1 <> @_terminal_iso_code AND @_terminal_iso_code <> @pNationalCurrency   THEN 0           ");
        _sb.AppendLine("                                ELSE TI_AMT1                                                                      ");
        _sb.AppendLine("                            END                                                                       ");
        _sb.AppendLine("                FROM   TICKETS    ");
        _sb.AppendLine("               WHERE   TI_TICKET_ID = @pTicketId    ");
      }
      else
      {
        _sb.AppendLine("     SELECT @_ti_type = TI_TYPE_ID,@_ti_amount = TI_AMOUNT FROM TICKETS WHERE TI_TICKET_ID = @pTicketId ");
      }
      _sb.AppendLine("     IF @_ti_type = 0 -- CASHABLE ");
      _sb.AppendLine("       UPDATE   MONEY_COLLECTIONS ");
      _sb.AppendLine("          SET   MC_EXPECTED_TICKET_AMOUNT    = ISNULL(MC_EXPECTED_TICKET_AMOUNT,0) + @_ti_amount ");
      _sb.AppendLine("              , MC_EXPECTED_TICKET_COUNT     = ISNULL(MC_EXPECTED_TICKET_COUNT,0) + 1 ");
      _sb.AppendLine("              , MC_EXPECTED_RE_TICKET_AMOUNT = ISNULL(MC_EXPECTED_RE_TICKET_AMOUNT,0) + @_ti_amount ");
      _sb.AppendLine("              , MC_EXPECTED_RE_TICKET_COUNT  = ISNULL(MC_EXPECTED_RE_TICKET_COUNT,0) + 1 ");
      _sb.AppendLine("        WHERE   MC_COLLECTION_ID = @pMoneyCollectionId ");
      _sb.AppendLine("          AND   MC_STATUS        = @pOpenStatus ");
      _sb.AppendLine("     ELSE IF @_ti_type = 1 -- PROMO_REDEEM ");
      _sb.AppendLine("       UPDATE   MONEY_COLLECTIONS ");
      _sb.AppendLine("          SET   MC_EXPECTED_TICKET_AMOUNT          = ISNULL(MC_EXPECTED_TICKET_AMOUNT,0) + @_ti_amount ");
      _sb.AppendLine("              , MC_EXPECTED_TICKET_COUNT           = ISNULL(MC_EXPECTED_TICKET_COUNT,0) + 1 ");
      _sb.AppendLine("              , MC_EXPECTED_PROMO_RE_TICKET_AMOUNT = ISNULL(MC_EXPECTED_PROMO_RE_TICKET_AMOUNT,0) + @_ti_amount ");
      _sb.AppendLine("              , MC_EXPECTED_PROMO_RE_TICKET_COUNT  = ISNULL(MC_EXPECTED_PROMO_RE_TICKET_COUNT,0) + 1 ");
      _sb.AppendLine("        WHERE   MC_COLLECTION_ID = @pMoneyCollectionId ");
      _sb.AppendLine("          AND   MC_STATUS        = @pOpenStatus ");
      _sb.AppendLine("     ELSE IF @_ti_type = 2 -- PROMO_NONREDEEM ");
      _sb.AppendLine("       UPDATE   MONEY_COLLECTIONS ");
      _sb.AppendLine("          SET   MC_EXPECTED_TICKET_AMOUNT          = ISNULL(MC_EXPECTED_TICKET_AMOUNT,0) + @_ti_amount ");
      _sb.AppendLine("              , MC_EXPECTED_TICKET_COUNT           = ISNULL(MC_EXPECTED_TICKET_COUNT,0) + 1 ");
      _sb.AppendLine("              , MC_EXPECTED_PROMO_NR_TICKET_AMOUNT = ISNULL(MC_EXPECTED_PROMO_NR_TICKET_AMOUNT,0) + @_ti_amount ");
      _sb.AppendLine("              , MC_EXPECTED_PROMO_NR_TICKET_COUNT  = ISNULL(MC_EXPECTED_PROMO_NR_TICKET_COUNT,0) + 1 ");
      _sb.AppendLine("        WHERE   MC_COLLECTION_ID = @pMoneyCollectionId ");
      _sb.AppendLine("          AND   MC_STATUS        = @pOpenStatus ");
      _sb.AppendLine("    ");
      _sb.AppendLine("   END ");

      using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
      {
        _sql_cmd.Parameters.Add("@pMoneyCollectionId", SqlDbType.BigInt).Value = MoneyCollectionId;
        _sql_cmd.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt).Value = PlaySessionId;
        _sql_cmd.Parameters.Add("@pTicketId", SqlDbType.BigInt).Value = TicketId;
        _sql_cmd.Parameters.Add("@pOpenStatus", SqlDbType.Int).Value = (Int32)TITO_MONEY_COLLECTION_STATUS.OPEN;

        // DHA 13-JAN-2016:
        if (Misc.IsFloorDualCurrencyEnabled())
        {
          _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;
          _sql_cmd.Parameters.Add("@pNationalCurrency", SqlDbType.NVarChar).Value = CurrencyExchange.GetNationalCurrency();
        }

        if (_sql_cmd.ExecuteNonQuery() >= 1)
        {
          return true;
        }
      }
    }
    catch (SqlException _sql_ex)
    {
      Log.Warning("Error sql in Tito_SetTicketSessionAndColletion : " + _sql_ex.Number.ToString() + ". " + _sql_ex.Message.ToString() + " *** Details: \r\n" + _sql_ex.StackTrace + "\r\n");
    }
    catch (Exception _ex)
    {
      Log.Warning("Error in Tito_SetTicketSessionAndColletion : " + _ex);
    }

    return false;
  } // Tito_SetTicketSessionAndColletion
}
