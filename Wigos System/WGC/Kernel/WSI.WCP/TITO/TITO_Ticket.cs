
//------------------------------------------------------------------------------
// Copyright � 2013 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: TITO_Ticket.cs
// 
//   DESCRIPTION: WCP_TITO class
// 
//        AUTHOR: Joshwa Marcalle
// 
// CREATION DATE: 26-SEP-2013
//  
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 26-SEP-2013 JRM    First version.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using WSI.Common;
using System.Xml;
using WSI.WCP;
using System.Data.SqlClient;
using System.Data;
using System.Data.SqlTypes;
using System.IO;
using System.Web;

public static partial class WCP_TITO
{
  //public interface IExtendedTicket
  //{
  //  WCP_TITO.TITO_Ticket Ticket { get; set;}
  //}

  //public interface IExtendedTicketInfo
  //{
  //  TITO_TicketInfo TicketInfo { get; set;}
  //}

  //public class TITO_Ticket : IXml
  //{
  //  #region Fields
   
  //  private Int64 m_ticket_id;
  //  private Int64 m_validation_number;
    
  //  #endregion

  //  #region Properties

  //  public Int64 TicketID
  //  {
  //    get { return m_ticket_id; }
  //    set { m_ticket_id = value; }
  //  }

  //  public Int64 ValidationNumber
  //  {
  //    get { return m_validation_number; }
  //    set { m_validation_number = value; }
  //  }

  //  #endregion

  //  #region IXml Members

  //  public String ToXml()
  //  {
  //    StringBuilder _sb;

  //    _sb = new StringBuilder();

  //    _sb.Append("<TicketData>");
  //    _sb.Append("<TicketID>" + XML.StringToXmlValue(this.TicketID.ToString()) + "</TicketID>");
  //    _sb.Append("<ValidationNumber>" + XML.StringToXmlValue(this.ValidationNumber.ToString()) + "</ValidationNumber>");
  //    _sb.Append("</TicketData>");

  //    return _sb.ToString();
  //  }//ToXml

  //  public void LoadXml(XmlReader Xml)
  //  {
  //    this.TicketID = Int64.Parse(XML.ReadTagValue(Xml, "TicketID"));
  //    this.ValidationNumber = Int64.Parse(XML.ReadTagValue(Xml, "ValidationNumber"));
  //  }//LoadXml
  //  #endregion
  //}

  //public class TITO_TicketInfo : IXml
  //{
  //  #region Fields
  //  private Int64 m_ticket_id;
  //  private String m_validation_number;
  //  private Currency m_amount;
  //  private Int32 m_status;
  //  private Int32 m_type_id;
  //  private DateTime m_created_date_time;
  //  private Int64 m_created_terminal_id;
  //  private Int32 m_created_terminal_type;
  //  private DateTime m_expiration_datetime;
  //  private Int64 m_money_collection_id;
  //  private Int64 m_created_account_id;
  //  private Int64 m_last_action_terminal_id;
  //  private DateTime m_last_action_datetime;
  //  private Int64 m_last_action_account_id;
  //  private Int32 m_last_action_terminal_type;
  //  private Int64 m_promotion_id;
  //  private Boolean m_collected;
  //  private Int64 m_last_action_cashier_session;
  //  private Int64 m_created_cashier_session;

  //  #endregion

  //  #region Properties

  //  public Int64 TicketID
  //  {
  //    get { return m_ticket_id; }
  //    set { m_ticket_id = value; }
  //  }

  //  public String ValidationNumber
  //  {
  //    get { return m_validation_number; }
  //    set { m_validation_number = value; }
  //  }

  //  public DateTime CreatedDateTime
  //  {
  //    get { return m_created_date_time; }
  //    set { m_created_date_time = value; }
  //  }

  //  public Int64 CreatedTerminalId
  //  {
  //    get { return m_created_terminal_id; }
  //    set { m_created_terminal_id = value; }
  //  }

  //  public Int32 CreatedTerminalType
  //  {
  //    get { return m_created_terminal_type; }
  //    set { m_created_terminal_type = value; }
  //  }

  //  public DateTime ExpirationDateTime
  //  {
  //    get { return m_expiration_datetime; }
  //    set { m_expiration_datetime = value; }
  //  }

  //  public Int64 MoneyCollectionID
  //  {
  //    get { return m_money_collection_id; }
  //    set { m_money_collection_id = value; }
  //  }

  //  public Int64 CreatedAccountID
  //  {
  //    get { return m_created_account_id; }
  //    set { m_created_account_id = value; }
  //  }

  //  public Int64 LastActionTerminalID
  //  {
  //    get { return m_last_action_terminal_id; }
  //    set { m_last_action_terminal_id = value; }
  //  }

  //  public DateTime LastActionDateTime
  //  {
  //    get { return m_last_action_datetime; }
  //    set { m_last_action_datetime = value; }
  //  }

  //  public Int64 LastActionAccountID
  //  {
  //    get { return m_last_action_account_id; }
  //    set { m_last_action_account_id = value; }
  //  }

  //  public Int32 LastActionTerminalType
  //  {
  //    get { return m_last_action_terminal_type; }
  //    set { m_last_action_terminal_type = value; }
  //  }

  //  public Int64 PromotionID
  //  {
  //    get { return m_promotion_id; }
  //    set { m_promotion_id = value; }
  //  }

  //  public Boolean IsCollected
  //  {
  //    get { return m_collected; }
  //    set { m_collected = value; }
  //  }

  //  public Int64 LastActionCashierSession
  //  {
  //    get { return m_last_action_cashier_session; }
  //    set { m_last_action_cashier_session = value; }
  //  }

  //   public Int64 CreatedCashierSession
  //  {
  //    get { return m_created_cashier_session; }
  //    set { m_created_cashier_session = value; }
  //  }

  //  public Currency Amount
  //  {
  //    get { return m_amount; }
  //    set { m_amount = value; }
  //  }

  //  public Int32 Status
  //  {
  //    get { return m_status; }
  //    set { m_status = value; }
  //  }

  //  public Int32 TypeId
  //  {
  //    get { return m_type_id; }
  //    set { m_type_id = value; }
  //  } 
  //  #endregion

  //  public TITO_TicketInfo()
  //  {
      
  //  }

  //  #region IXml Members

  //  public String ToXml()
  //  {
  //    StringBuilder _sb;

  //    _sb = new StringBuilder();

  //    _sb.Append("<TicketInfo>");
  //    _sb.Append("<ValidationNumber>" + this.ValidationNumber + "</ValidationNumber>");
  //    _sb.Append("<Amount>" + this.Amount.ToString() + "</Amount>");
  //    _sb.Append("<Status>" + this.Status.ToString() + "</Status>");
  //    _sb.Append("<TypeId>" + this.TypeId.ToString() + "</TypeId>");
  //    _sb.Append("<CreatedDateTime>" + XML.XmlDateTimeString(this.CreatedDateTime) + "</CreatedDateTime>");
  //    _sb.Append("<CreatedTerminalId>" +  this.CreatedTerminalId.ToString() + "</CreatedTerminalId>");
  //    _sb.Append("<CreatedTerminalType>" + this.CreatedTerminalType.ToString() + "</CreatedTerminalType>");
  //    _sb.Append("<ExpirationDateTime>" + XML.XmlDateTimeString(this.ExpirationDateTime) + "</ExpirationDateTime>");
  //    _sb.Append("<MoneyCollectionId>" +  this.MoneyCollectionID.ToString()  + "</MoneyCollectionId>");
  //    _sb.Append("<CreatedAccountID>" + this.CreatedAccountID.ToString() + "</CreatedAccountID>");
  //    _sb.Append("<LastActionTerminalID>" +  this.LastActionTerminalID.ToString() + "</LastActionTerminalID>");
  //    _sb.Append("<LastActionDateTime>" + XML.XmlDateTimeString(this.LastActionDateTime) + "</LastActionDateTime>");
  //    _sb.Append("<LastActionAccountID>" +  this.LastActionAccountID.ToString() + "</LastActionAccountID>");
  //    _sb.Append("<LastActionTerminalType>" + this.LastActionTerminalType.ToString() + "</LastActionTerminalType>");
  //    _sb.Append("<PromotionID>" + this.PromotionID.ToString() + "</PromotionID>");
  //    _sb.Append("<IsCollected>" + this.IsCollected.ToString() + "</IsCollected>");
  //    _sb.Append("<LastActionCashierSession>" +  this.LastActionCashierSession.ToString() + "</LastActionCashierSession>");
  //    _sb.Append("<CreatedCashierSession>" + this.CreatedCashierSession.ToString() + "</CreatedCashierSession>");
  //    _sb.Append("</TicketInfo>");

  //    return _sb.ToString();

  //  }//ToXml

  //  public void LoadXml(XmlReader Xml)
  //  {
  //    //String _date_value;

  //    this.Amount = Int32.Parse(XML.ReadTagValue(Xml, "Amount"));
  //    this.ValidationNumber = XML.ReadTagValue(Xml, "ValidationNumber");
  //    this.Status = Int32.Parse(XML.ReadTagValue(Xml, "Status"));
  //    this.TypeId = Int32.Parse(XML.ReadTagValue(Xml, "TypeId"));
  //    this.CreatedDateTime = DateTime.Parse(XML.ReadTagValue(Xml, "CreatedDateTime"));
  //    this.CreatedTerminalId = Int64.Parse(XML.ReadTagValue(Xml, "CreatedTerminalId"));
  //    this.CreatedTerminalType = Int32.Parse(XML.ReadTagValue(Xml, "CreatedTerminalType"));
  //    this.ExpirationDateTime = DateTime.Parse(XML.ReadTagValue(Xml, "ExpirationDateTime"));
  //    this.MoneyCollectionID = Int64.Parse(XML.ReadTagValue(Xml, "MoneyCollectionID"));
  //    this.CreatedAccountID = Int64.Parse(XML.ReadTagValue(Xml, "CreatedAccountID"));
  //    this.LastActionTerminalID = Int64.Parse(XML.ReadTagValue(Xml, "LastActionTerminalID"));
  //    this.LastActionDateTime = DateTime.Parse(XML.ReadTagValue(Xml, "LastActionDateTime"));
  //    this.LastActionAccountID = Int64.Parse(XML.ReadTagValue(Xml, "LastActionAccountID"));
  //    this.LastActionTerminalType = Int32.Parse(XML.ReadTagValue(Xml, "LastActionTerminalType"));
  //    this.PromotionID = Int64.Parse(XML.ReadTagValue(Xml, "PromotionID"));
  //    this.IsCollected = Boolean.Parse(XML.ReadTagValue(Xml, "IsCollected"));
  //    this.LastActionCashierSession = Int64.Parse(XML.ReadTagValue(Xml, "LastActionCashierSession"));
  //    this.CreatedCashierSession = Int64.Parse(XML.ReadTagValue(Xml, "CreatedCashierSession"));
    
  //    Xml.Read();

  //  }

  //  #endregion

  //}

  //public interface TITO_IGetTicketField
  //{
  //  Int32 FieldsNumber
  //  { get; }

    //TITO_ITicketField GetField(Int32 Idx);

  //  TITO_TicketFieldsTable TicketFields
  //  { get; set; }
  //}

  //public interface TITO_ITicketField
  //{
  //  TICKET_INFO_FIELD_TYPE FieldId
  //  { get;set; }

  //  String Value
  //  { get;set; }

  //  Int32 Updateable
  //  { get;set; }

  //  TITO_FIELD_TYPE Type
  //  { get;set; }

  //  Int32 MinLength
  //  { get;set; }

  //  Int32 MaxLength
  //  { get;set; }

  //  TITO_UPDATE_FIELD_STATUS ErrorCode
  //  { get;set; }

  //  string ErrorDescription
  //  { get;set; }

  //}

  //public class TITO_TicketFieldsTable : DataTable, IXml 
  //{
    //#region "Constructor"

    //public TITO_TicketFieldsTable()
    //{
    //  InitDataTable();
    //}

    //#endregion

    //#region "Overrides"

    //public TITO_TicketFieldsRow this[int Idx]
    //{
    //  get { return (TITO_TicketFieldsRow)Rows[Idx]; }
    //}

    //protected override Type GetRowType()
    //{
    //  return typeof(TITO_TicketFieldsRow);
    //}

    //protected override DataRow NewRowFromBuilder(DataRowBuilder Builder)
    //{
    //  return new TITO_TicketFieldsRow(Builder);
    //}

    //public void Remove(TITO_TicketFieldsRow Row)
    //{
    //  Rows.Remove(Row);
    //}

    //public new TITO_TicketFieldsRow NewRow()
    //{
    //  TITO_TicketFieldsRow _row;
    //  _row = (TITO_TicketFieldsRow)base.NewRow();

    //  return _row;
    //}
    //#endregion

    //#region Properties

    //public Int32 FieldsNumber
    //{
    //  get { return this.Rows.Count; }
    //}
    //#endregion

    //#region "Private Members"


    //------------------------------------------------------------------------------
    // PURPOSE:  Initialize DataTable, Create DataColumns
    //             FieldId*    : TICKET_INFO_FIELD_TYPE 
    //             Value       : Field Value
    //             Error Code  : Error code at update ( if not right)
    //             Description : Error code description ( if not right )
    // 
    //  PARAMS:
    //      - INPUT: 
    //
    //      - OUTPUT:
    //
    // RETURNS: A String array with Primary Key Column Name
    //
    //   NOTES:
    //
    // "FieldId","Value","ErrorCode","Description" columns, And IF_FIELD_ID as PrimaryKey
    //private void InitDataTable()
    //{
    //  this.Columns.Add(new DataColumn("FieldID", typeof(TICKET_INFO_FIELD_TYPE)));

    //  this.Columns.Add(new DataColumn("Value", typeof(String)));

    //  this.Columns.Add(new DataColumn("Updateable", typeof(Int32)));

    //  this.Columns.Add(new DataColumn("Type", typeof(TITO_FIELD_TYPE)));

    //  this.Columns.Add(new DataColumn("MinLength", typeof(Int32)));

    //  this.Columns.Add(new DataColumn("MaxLength", typeof(Int32)));

    //  this.Columns.Add(new DataColumn("ErrorCode", typeof(TITO_UPDATE_FIELD_STATUS)));

    //  this.Columns.Add(new DataColumn("ErrorDescription", typeof(String)));

    //  this.Columns.Add(new DataColumn("DiplayOrder", typeof(Int32)));

    //  this.PrimaryKey = new DataColumn[] { this.Columns["FieldId"] };

    //  this.TableName = "Field";
    //}


    //#endregion

    //#region "Events"

    //public delegate void TicketFieldsRowChangedDlgt(TITO_TicketFieldsTable sender,
    //                                                TicketFieldsRowChangedEventArgs args);
    //------------------------------------------------------------------------------
    // PURPOSE: On Row change event
    // 
    //  PARAMS:
    //      - INPUT: 
    //
    //      - OUTPUT: 
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    //public event TicketFieldsRowChangedDlgt TicketFieldsRowChanged;
    //------------------------------------------------------------------------------
    // PURPOSE: On Row changed function
    // 
    //  PARAMS:
    //      - INPUT: 
    //
    //      - OUTPUT: 
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    //protected override void OnRowChanged(DataRowChangeEventArgs e)
    //{
    //  base.OnRowChanged(e);
    //  TicketFieldsRowChangedEventArgs args =
    //    new TicketFieldsRowChangedEventArgs(e.Action, (TITO_TicketFieldsRow)e.Row);
    //  OnTicketFieldsRowChanged(args);
    //}
    //------------------------------------------------------------------------------
    // PURPOSE: On Row change
    // 
    //  PARAMS:
    //      - INPUT: 
    //
    //      - OUTPUT: 
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    //protected virtual void OnTicketFieldsRowChanged(TicketFieldsRowChangedEventArgs args)
    //{
    //  if (TicketFieldsRowChanged != null)
    //  {
    //    TicketFieldsRowChanged(this, args);
    //  }
    //}

    //#endregion

    //#region "Arguments class"
    //public class TicketFieldsRowChangedEventArgs
    //{
    //  protected DataRowAction action;
    //  //protected TITO_TicketFieldsRow row;

    //  public DataRowAction Action
    //  {
    //    get { return action; }
    //  }

    //  //public TITO_TicketFieldsRow Row
    //  //{
    //  //  get { return row; }
    //  //}

    //  public TicketFieldsRowChangedEventArgs(DataRowAction action, TITO_TicketFieldsRow row)
    //  {
    //    this.action = action;
    //    this.row = row;
    //  }
    //}
    //#endregion

    //#region IXml Members

    //public string ToXml()
    //{
    //  DataSet _ds;
    //  TextWriter _txt_wr;

    //  _ds = new DataSet("Fields");
    //  _txt_wr = new StringWriter();

    //  this.EncodeSpecialChars();

    //  _ds.Tables.Add(this);
    //  this.WriteXml(_txt_wr);

    //  return _txt_wr.ToString();
    //}

    //public void LoadXml(XmlReader XmlReader)
    //{
    //  this.ReadXml(XmlReader);
    //  this.DecodeSpecialChars();
    //}
    
    //#endregion

    //#region "Public static functions"

    //public static Boolean TITO_ReadTicketFields(Int64 TicketId, out TITO_TicketFieldsTable TicketFields, SqlTransaction Trx)
    //{
    //  TITO_TicketFieldsRow _ticket_fields_row;
    //  DataTable _field_definitions;
    //  StringBuilder _sb_fields;
    //  DataRow _field_definition;
    //  Int32 _num_fields;
    //  DateTime _date;
    //  String _string_limited;

    //  TicketFields = new TITO_TicketFieldsTable();

    //  try
    //  {
    //    _field_definitions = Get_tito_ticket_info_fields(Trx);

    //    _sb_fields = new StringBuilder();
    //    _num_fields = 0;
    //    _sb_fields.Append("SELECT   ");

    //    foreach (DataRow _field in _field_definitions.Rows)
    //    {
    //      if (_field.IsNull("TIF_TABLE_NAME") || _field.IsNull("TIF_FIELD_NAME"))
    //      {
    //        continue;
    //      }
    //      if ((String)_field["TIF_TABLE_NAME"] != "TICKETS")
    //      {
    //        continue;
    //      }

    //      if (_num_fields > 0)
    //      {
    //        _sb_fields.Append("       , ");
    //      }
    //      _sb_fields.AppendLine((String)_field["TIF_FIELD_NAME"]);
    //      _num_fields++;
    //    }
    //    _sb_fields.AppendLine(" FROM   TICKETS");
    //    _sb_fields.AppendLine("WHERE   TI_TICKET_ID = @pId");

    //    using (SqlCommand _sql_cmd = new SqlCommand(_sb_fields.ToString(), Trx.Connection, Trx))
    //    {
    //      _sql_cmd.Parameters.Add("@pId", SqlDbType.BigInt).Value = TicketId;

    //      using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
    //      {

    //        while (_reader.Read())
    //        {

    //          for (int _idx_field = 0; _idx_field < _reader.FieldCount; _idx_field++)
    //          {
    //            _ticket_fields_row = TicketFields.NewRow();

    //            if (_idx_field >= _field_definitions.Rows.Count)
    //            {
    //              continue;
    //            }

    //            _field_definition = _field_definitions.Rows[_idx_field];

    //            _ticket_fields_row.FieldId = (TICKET_INFO_FIELD_TYPE)_field_definition["TIF_FIELD_ID"];

    //            if (!(Boolean)_field_definition["TIF_SHOWN"])
    //            {
    //              continue;
    //            }

    //            _ticket_fields_row.Updateable = (Int32)_field_definition["TIF_CAN_UPDATE"];
    //            _ticket_fields_row.Type = (TITO_FIELD_TYPE)_field_definition["TIF_TYPE"];
    //            _ticket_fields_row.MinLength = (Int32)_field_definition["TIF_MIN_LENGTH"];
    //            _ticket_fields_row.MaxLength = (Int32)_field_definition["TIF_MAX_LENGTH"];

    //            if (!_reader.IsDBNull(_idx_field))
    //            {
    //              switch (_ticket_fields_row.Type)
    //              {
    //                case TITO_FIELD_TYPE.DATE:
    //                  _date = (DateTime)_reader[_idx_field];
    //                  _ticket_fields_row.Value = String.Format("{0:dd/MM/yyyy}", _date);
    //                  break;

    //                default:
    //                  _string_limited = _reader[_idx_field].ToString();

    //                  // Ticket field value is limited to 50 chars
    //                  _ticket_fields_row.Value = (_string_limited.Length > 50) ? _string_limited.Remove(50)
    //                                                                           : _string_limited;
    //                  break;
    //              }
    //            }

    //            TicketFields.Rows.Add(_ticket_fields_row);

    //          }
    //        }
    //        _reader.Close();
    //      }
    //    }
    //    TicketFields.AcceptChanges();

    //  }
    //  catch (Exception Ex)
    //  {
    //    Log.Error(Ex.Message);
    //    return false;
    //  }

    //  return true;

    //}

    //------------------------------------------------------------------------------
    // PURPOSE: Get ticket information by TicketId
    //
    //  PARAMS:
    //      - INPUT: TicketId, SqlTransaction
    //      - OUT: TicketFields
    //  RETURN : ITicketFieldsTable with ticket field values.
    //
    //public static Boolean TITO_ReadTicketFields(Int64 TicketId, out TITO_IGetTicketField TicketFields, SqlTransaction Trx)
    //{
    //  TicketFields = new TITO_TicketFieldsTable();

    //  return TITO_ReadTicketFields(TicketId, out TicketFields, Trx);
    //}

    //------------------------------------------------------------------------------
    // PURPOSE:  Update ticket info fields, Do SQL update command
    // 
    //  PARAMS:
    //      - INPUT: 'TicketFieldsTable' DataTable
    //
    //      - OUTPUT: Fill "Description" and "ErrorCode" values with sql update result
    //
    // RETURNS: Boolean , true if everything right , and false IF SOMETHING FAILED
    //
    //   NOTES:
    //
    //public static bool TITO_UpdateTicketFields(Int64 TicketId,
    //                                           TITO_IGetTicketField FieldValues, 
    //                                           SqlTransaction Trx)
    //{

    //  DataTable _field_definitions;
    //  DataRow _field_definition;
    //  Int32 _field_min_length;
    //  Int32 _field_max_length;
    //  String _table_name;
    //  String _field_name;
    //  String _pk_column_name;
    //  SqlCommand _sql_command;
    //  StringBuilder _str_builder;
    //  Int32 _idx_field;
    //  Boolean _has_errors;
    //  SqlDbType _field_type;

    //  List<TICKET_INFO_FIELD_TYPE> _calculated_fields = new List<TICKET_INFO_FIELD_TYPE>(
    //      new TICKET_INFO_FIELD_TYPE[] { TICKET_INFO_FIELD_TYPE.TICKET_ID });

    //  _field_definitions = new DataTable();
    //  _sql_command = new SqlCommand();
    //  _idx_field = 0;
    //  _has_errors = false;
    //  //_new_validation_number = "";

    //  _sql_command.CommandText = "DECLARE   @affected INT";
    //  _sql_command.CommandText += "   SET   @affected = 0";

    //  _field_definitions = Get_tito_ticket_info_fields(Trx);

    //  try
    //  {
    //    GenerateCalculatedFields(FieldValues, TicketId, Trx);

    //    for (Int32 _idx = 0; _idx < FieldValues.FieldsNumber; _idx++)
    //    {
    //      TITO_ITicketField _field = FieldValues.GetField(_idx);

    //      _field.Value = _field.Value.Trim();

    //      _field.ErrorCode = TITO_UPDATE_FIELD_STATUS.INTERNAL_BEING_UPDATED;
    //      _field.ErrorDescription = "Not updated";

    //      _field_definition = _field_definitions.Rows.Find(_field.FieldId);

    //      if (_field_definition == null)
    //      {
    //        _field.ErrorCode = TITO_UPDATE_FIELD_STATUS.ERROR;
    //        _field.ErrorDescription = "Field definition not found";

    //        Log.Error("Field definition not found for FieldId = " + _field.FieldId);
    //        _has_errors = true;

    //        continue;
    //      }

    //      // Check out whether field can be updated or not
    //      if ((Int32)_field_definition["TIF_CAN_UPDATE"] == 0)
    //      {
    //           // Field can not be updated => error
    //          _field.ErrorCode = TITO_UPDATE_FIELD_STATUS.ERROR_NOT_EDITABLE;
    //          _field.ErrorDescription = "Not editable";
    //          _has_errors = true;

    //          continue;
    //      }

    //      _table_name = (String)_field_definition["TIF_TABLE_NAME"];
    //      _field_name = (String)_field_definition["TIF_FIELD_NAME"];

    //      if (String.IsNullOrEmpty(_table_name))
    //      {
    //        Log.Error("Invalid Table Name");
    //        _has_errors = true;

    //        continue;
    //      }

    //      if (String.IsNullOrEmpty(_field_name))
    //      {
    //        Log.Error("Invalid Field Name");
    //        _has_errors = true;

    //        continue;
    //      }

    //      switch (_table_name)
    //      {
    //        case "TICKETS":
    //          _pk_column_name = "ti_ticket_id";
    //          break;

    //        default:
    //          Log.Error("Table name '" + _table_name + "' not supported");
    //          _has_errors = true;
    //          continue;
    //      }

    //      _field_min_length = (Int32)_field_definition["TIF_MIN_LENGTH"];
    //      _field_max_length = (Int32)_field_definition["TIF_MAX_LENGTH"];

    //      if (_field.Value.Length < _field_min_length)
    //      {
    //        _field.ErrorCode = TITO_UPDATE_FIELD_STATUS.ERROR_MIN_LENGTH;
    //        _field.ErrorDescription = "String length less than minimum" + _field_min_length.ToString() + "/" + _field_min_length.ToString();
    //        _has_errors = true;

    //        continue;
    //      }
    //      if (_field.Value.Length > _field_max_length)
    //      {
    //        _field.ErrorCode = TITO_UPDATE_FIELD_STATUS.ERROR_MAX_LENGTH;
    //        _field.ErrorDescription = "String length greater than maximum " + _field_min_length.ToString() + "/" + _field_max_length.ToString();
    //        _has_errors = true;

    //        continue;
    //      }

    //      switch (_field.FieldId)
    //      {
    //        case TICKET_INFO_FIELD_TYPE.VALIDATION_NUMBER:
    //          _field_type = SqlDbType.NVarChar;

    //          if (!ValidateFormat.TextWithoutNumbers(_field.Value))
    //          {
    //            _field.ErrorCode = TITO_UPDATE_FIELD_STATUS.ERROR_VALIDATION;
    //            _field.ErrorDescription = "Formato incorrecto";
    //            _has_errors = true;
    //            continue;
    //          }
    //          break;

    //        case TICKET_INFO_FIELD_TYPE.AMOUNT:
    //        case TICKET_INFO_FIELD_TYPE.STATUS:
    //        case TICKET_INFO_FIELD_TYPE.TYPE_ID:
    //        case TICKET_INFO_FIELD_TYPE.CREATED_TERMINAL_TYPE:
    //        case TICKET_INFO_FIELD_TYPE.LAST_ACTION_TERMINAL_TYPE:
    //        case TICKET_INFO_FIELD_TYPE.IS_COLLECTED:
    //          _field_type = SqlDbType.Int;

    //          if (!string.IsNullOrEmpty(_field.Value) && !ValidateFormat.AlphaNumeric(_field.Value))
    //          {
    //            _field.ErrorCode = TITO_UPDATE_FIELD_STATUS.ERROR_VALIDATION;
    //            _field.ErrorDescription = "Formato incorrecto";
    //            _has_errors = true;
    //            continue;
    //          }
    //          break;

    //        case TICKET_INFO_FIELD_TYPE.CREATED_TERMINAL_ID:
    //        case TICKET_INFO_FIELD_TYPE.MONEY_COLLECTION_ID:
    //        case TICKET_INFO_FIELD_TYPE.CREATED_ACCOUNT_ID:
    //        case TICKET_INFO_FIELD_TYPE.LAST_ACTION_TERMINAL_ID:
    //        case TICKET_INFO_FIELD_TYPE.LAST_ACTION_ACCOUNT_ID:
    //        case TICKET_INFO_FIELD_TYPE.PROMOTION_ID:
    //        case TICKET_INFO_FIELD_TYPE.LAST_ACTION_CASHIER_SESSION:
    //        case TICKET_INFO_FIELD_TYPE.CREATED_CASHIER_SESSION:
    //          _field_type = SqlDbType.BigInt;

    //          if (!ValidateFormat.TextWithNumbers(_field.Value))
    //          {
    //            _field.ErrorCode = TITO_UPDATE_FIELD_STATUS.ERROR_VALIDATION;
    //            _field.ErrorDescription = "Formato incorrecto";
    //            _has_errors = true;
    //            continue;
    //          }
    //          break;
 
    //        case TICKET_INFO_FIELD_TYPE.CREATED_DATETIME:
    //        case TICKET_INFO_FIELD_TYPE.EXPIRATION_DATETIME:
    //        case TICKET_INFO_FIELD_TYPE.LAST_ACTION_DATETIME:
    //          _field_type = SqlDbType.DateTime;

    //          if (!ValidateFormat.RawNumber(_field.Value))
    //          {
    //            _field.ErrorCode = TITO_UPDATE_FIELD_STATUS.ERROR_VALIDATION;
    //            _field.ErrorDescription = "Formato incorrecto";
    //            _has_errors = true;
    //            continue;
    //          }
    //          break;

    //        default:
    //          _field_type = SqlDbType.NVarChar;
    //          break;
    //      }

    //      _str_builder = new StringBuilder();

    //      _str_builder.AppendFormat(" UPDATE   {0}", _table_name);
    //      _str_builder.AppendFormat("    SET   {0} = @pFieldValue{1}", _field_name, _idx_field);
    //      _str_builder.AppendFormat("  WHERE   {0} = @pId;", _pk_column_name);
    //      _str_builder.AppendFormat("    SET   @AFFECTED = @AFFECTED + @@ROWCOUNT ");

    //      _sql_command.CommandText += _str_builder.ToString();
    //      _sql_command.Parameters.Add("@pFieldValue" + _idx_field, _field_type).Value = _field.Value;

    //      // Refresh localvalues to set values from DataBase
    //      _field.MinLength = (Int32)_field_definition["TIF_MIN_LENGTH"];
    //      _field.MaxLength = (Int32)_field_definition["TIF_MAX_LENGTH"];
    //      _field.Type = (TITO_FIELD_TYPE)_field_definition["TIF_TYPE"];
    //      _field.Updateable = (Int32)_field_definition["TIF_CAN_UPDATE"];
          

    //      if (_field.Value.Length > 50)
    //      {
    //        _field.Value = _field.Value.Remove(50);
    //      }

    //      _idx_field++;

    //    } // End for field
    //  }
    //  catch (Exception _ex)
    //  {
    //    for (Int32 _idx = 0; _idx < FieldValues.FieldsNumber; _idx++)
    //    {
    //      TITO_ITicketField _field = FieldValues.GetField(_idx);

    //      _field.ErrorCode = TITO_UPDATE_FIELD_STATUS.ERROR;
    //      _field.ErrorDescription = "Updated failed!";
    //    }

    //    Log.Exception(_ex);
    //    return false;
    //  }

    //  for (Int32 _idx = 0; _idx < FieldValues.FieldsNumber; _idx++)
    //  {
    //    TITO_ITicketField _field = FieldValues.GetField(_idx);

    //    if (_field.ErrorCode == 0)
    //    {
    //      _field.ErrorDescription = "Updated!";
    //    }
    //  }

    //  return !_has_errors;
    //} // TITO_UpdateTicketFields


    //#endregion

    //#region "Private functions"

    //------------------------------------------------------------------------------
    // PURPOSE: Generate Calculated Fields
    // Contains calls to all necessary methods to calculate field value
    //  PARAMS:
    //      - INPUT: 
    //          Fields values : Current Field Values
    //          AccountId     : Ticket Id
    //          Transaction   : Necessary if want to acces to unavaliable data , and need to query in DB
    //      - OUTPUT
    //          Fields:       : Table with new fields

    //private static void GenerateCalculatedFields(TITO_IGetTicketField Fields, Int64 TicketId, SqlTransaction Trx)
    //{
    //  TITO_TicketFieldsTable _table = Fields.TicketFields;
    //}

    //------------------------------------------------------------------------------
    // PURPOSE: Replace current object Special Chars  to HTML chars to prevent XML errors.
    //
    //  PARAMS:
    //      - INPUT: 
    //          
    //      - OUTPUT
    //
    ////
    //public void EncodeSpecialChars()
    //{
    //  TITO_TicketFieldsRow _row;
    //  Int32 _idx_row;

    //  for (_idx_row = 0; _idx_row < this.Rows.Count; _idx_row++)
    //  {
    //    _row = this[_idx_row];
    //    _row.Value = XML.StringToXmlValue(_row.Value);
    //  }
    //}
    //------------------------------------------------------------------------------
    // PURPOSE: Replace current object HTML chars to Special Chars to display in client
    //  PARAMS:
    //      - INPUT: 
    //      - OUTPUT
    //
    //public void DecodeSpecialChars()
    //{
    //  String _value;
    //  TITO_TicketFieldsRow _row;
    //  Int32 _idx_row;

    //  for (_idx_row = 0; _idx_row < this.Rows.Count; _idx_row++)
    //  {
    //    _row = this[_idx_row];
    //    _value = _row.Value;

    //    _value = _value.Replace("&amp;", "&");
    //    _value = _value.Replace("&lt;", "<");
    //    _value = _value.Replace("&gt;", ">");
    //    _value = _value.Replace("&quot;", "\"");
    //    _value = _value.Replace("&apos;", "'");

    //    _row.Value = _value;
    //  }
    //}

    //#endregion

    //#region IGetTicketField Members


    //public TITO_ITicketField GetField(int Idx)
    //{
    //  if (Idx < this.FieldsNumber)
    //  {
    //    return (TITO_ITicketField)this[Idx];
    //  }
    //  else
    //  {
    //    throw new Exception("Invalid Row Index!");
    //  }
    //}

    //public TITO_TicketFieldsTable TicketFields
    //{
    //  get { return this; }
    //  set
    //  {
    //    this.Clear();
    //    foreach (DataRow _row in value.Rows)
    //    {
    //      this.ImportRow(_row);
    //    }
    //    this.AcceptChanges();
    //  }
    //}

    //#endregion
  //}

  //public class TITO_TicketFieldsRow : DataRow 
  //{
  //  internal TITO_TicketFieldsRow(DataRowBuilder Builder)
  //    : base(Builder)
  //  {
  //    FieldId = 0;
  //    Value = String.Empty;
  //    Updateable = 0;
  //    Type = 0;
  //    MinLength = 0;
  //    MaxLength = 0;
  //    ErrorCode = 0;
  //    ErrorDescription = String.Empty;
  //  }

  //  #region "Members"
  //  public TICKET_INFO_FIELD_TYPE FieldId
  //  {
  //    get { return (TICKET_INFO_FIELD_TYPE)base["FieldId"]; }
  //    set { base["FieldId"] = value; }
  //  }

  //  public String Value
  //  {
  //    get { return (String)base["Value"]; }
  //    set { base["Value"] = value; }
  //  }

  //  public Int32 Updateable
  //  {
  //    get { return (Int32)base["Updateable"]; }
  //    set { base["Updateable"] = value; }
  //  }

  //  public TITO_FIELD_TYPE Type
  //  {
  //    get { return (TITO_FIELD_TYPE)base["Type"]; }
  //    set { base["Type"] = value; }
  //  }

  //  public Int32 MinLength
  //  {
  //    get { return (Int32)base["MinLength"]; }
  //    set { base["MinLength"] = value; }
  //  }

  //  public Int32 MaxLength
  //  {
  //    get { return (Int32)base["MaxLength"]; }
  //    set { base["MaxLength"] = value; }
  //  }

  //  public TITO_UPDATE_FIELD_STATUS ErrorCode
  //  {
  //    get { return (TITO_UPDATE_FIELD_STATUS)base["ErrorCode"]; }
  //    set { base["ErrorCode"] = value; }
  //  }

  //  public string ErrorDescription
  //  {
  //    get { return (string)base["ErrorDescription"]; }
  //    set { base["ErrorDescription"] = value; }
  //  }

  //  #endregion

  //}
}
 
