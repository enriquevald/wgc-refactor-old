//------------------------------------------------------------------------------
// Copyright © 2013 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME:  TITO_ProcessCreateTicket.cs
// 
//   DESCRIPTION: Process print ticket Message
//   AUTHOR     : Joshwa Marcalle
// 
//   REVISION HISTORY:
//  
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 03-OCT-2013 JRM    First version.
// 09-OCT-2013 JRM    Change of "type" in some members
// 26-OCT-2013 JRM    Blocked sending of empty validation numbers, added functionallity to set new cancel ticket flag
// 07-NOV-2013 NMR    Fixed error wih ticket creation date and TerminalId
// 08-NOV-2013 NMR    Fixed error: Terminal not loaded
// 14-NOV-2013 NMR    Fixed error: ValidationSequenceId was incorrectly treated
// 19-NOV-2013 NMR    Processed new ticket property
// 26-NOV-2013 JRM    Refactoring and code cleaning
// 28-NOV-2013 JRM    Return codes must be OK or ERROR
// 03-DIC-2013 NMR    Improvement: process given Play Session or load the last asociated to Terminal
// 10-DIC-2013 NMR    Saved related play-session ID
// 12-DIC-2013 NMR    All tickets wit NO EXPIRATION DATE reported by terminal, will have Default Expiration date
// 19-DIC-2013 JRM    Changed all Sessions Acquirering methods for faster ones.
// 13-JAN-2014 JRM    Always get account id from play_session_id. This adjusment is because terminals now close the play session before sending the create ticket
// 24-JAN-2014 JRM    Make sure that created tickets have a related handpay. Also added Provider_id to session objects call
// 07-MAR-2014 QMP & DDM & DHA Fixed bug WIGOSTITO-1131: Created Cash/Account movements have the same Operation ID
// 08-JUN-2015 DDM & JML    Fixed bug WIG-2413.
// 02-FEB-2016 JML    Product Backlog Item 8352:Floor Dual Currency: Ticket Offline
// 29-MAR-2016 DHA    Bug 10986: error on ticket offline (handpay)
// 13-OCT-2016 FJC    PBI 18258:TITO: Nuevo estado "Pending Print" en tickets - Creación nuevo estado
// 18-JUL-2017 FJC    Defect WIGOS-3118 [Ticket #4699] Too many handpays being generated because of late arrival of ticket numbers
// 21-JUL-2017 FJC    Fixed Bug WIGOS-3888 Accounting - TITO tickets associated with anonymous accounts when using players card.
// 15-NOV-2017 JML    Bug 30798:WIGOS-5926 [Ticket #9396] error titios promo
//------------------------------------------------------------------------------
// 
//
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Threading;
using System.Data.SqlClient;
using WSI.Common;
using WSI.WCP;
using WSI.Common.TITO;

public static partial class WCP_TITO
{

  #region "Private Functions"

  //------------------------------------------------------------------------------
  // PURPOSE: Process TITO_ProcessCreateTicket Message
  //
  //  PARAMS:
  //      - INPUT: Input Message ( blank ) 
  //
  //      - OUTPUT: 
  //
  // RETURNS:
  private static void TaskTitoManageTicket(Object Table)
  {
    WCP_Context _ctx;
    WCP_TITO_MsgValidationNumberReply _response_validation_number;
    DataTable _table;
    Boolean _processed;
    WCP_TITO_MsgCreateTicket _request;
    WCP_TITO_MsgCreateTicketReply _response;
    Boolean _error;
    DataRow[] _rows;
    Int32 _num_reg_updated;

    _table = (DataTable)Table;
    _processed = false;
    _error = false;
    _response_validation_number = new WCP_TITO_MsgValidationNumberReply();

    try
    {
      StringBuilder _sb = new StringBuilder();
      _rows = _table.Select("", "TerminalId");
      using (DB_TRX _db_trx = new DB_TRX())
      {
        //if (!WSI.Common.Ticket.DB_CreateTicketBatch(_table, _db_trx.SqlTransaction))
        //{
        //  _error = true;
        //}

        if (!_error)
        {
          // Insert Movements...
          foreach (DataRow _row in _rows)
          {
            try
            {
              _num_reg_updated = 0;
              _ctx = (WCP_Context)_row["CTX"];

              if (GeneralParam.GetBoolean("TITO", "PendingPrint.Enabled", true))
              {
                // FJC 07-10-2016. (PBI 18258)
                // Update Data Ticket
                if (!TITO_ProcessUpdateTicket(_ctx.tcp_client, _ctx.request, _ctx.response, out _num_reg_updated, _db_trx.SqlTransaction))
                {
                  _error = true;

                  Log.Error("Error in TITO_ProcessUpdateTicket(). " +
                            "   TerminalId: " + ((WCP_TITO_MsgCreateTicket)_ctx.request.MsgContent).TerminalID +
                            " | TransactionId: " + ((WCP_TITO_MsgCreateTicket)_ctx.request.MsgContent).TransactionId +
                            " | ValidationNumber: " + ValidationNumberManager.GetCalculatedValidationNumber(((WCP_TITO_MsgCreateTicket)_ctx.request.MsgContent).SystemId,
                                                                                                              ((WCP_TITO_MsgCreateTicket)_ctx.request.MsgContent).ValidationNumberSequence));

                  break;
                }
              }
              // FJC 07-10-2016. (PBI 18258)
              // If TITO_ProcessUpdateTicket() doesn't update any file, we try to create ticket. (backward compatibility between WCP & SAS-HOST)
              if (_num_reg_updated <= 0) // FJC 17-07-2017 
              // When GP -TITO.PendingPrint.Enabled- is false num_reg_updated always will be zero.
              // We maintain this condition when GP -TITO.PendingPrint.Enabled is true, although it does not make sense.
              {
                if (!TITO_ProcessCreateTicket(_ctx.tcp_client, _ctx.request, _ctx.response, _response_validation_number, TITO_TICKET_STATUS.VALID, _db_trx.SqlTransaction))
                {
                  _error = true;
                  Log.Error("Error in TITO_ProcessCreateTicket(). TerminalId: " + _response_validation_number.machine_id +
                                                           " | TransactionId: " + _response_validation_number.transaction_id);

                  break;
                }
              }
            }
            catch (Exception _ex)
            {
              _error = true;
              Log.Exception(_ex);

              break;
            }
          }
        }

        if (_error)
        {
          _db_trx.Rollback();
        }
        else
        {
          _db_trx.Commit();

          _processed = true;
        }
      }
    }
    catch (Exception _ex)
    {
      _processed = false;

      Log.Exception(_ex);
    }

    try
    {
      // Send responses
      foreach (DataRow _row in _table.Rows)
      {
        try
        {
          _ctx = (WCP_Context)_row["CTX"];

          switch (_ctx.request.MsgHeader.MsgType)
          {
            case WCP_MsgTypes.WCP_TITO_MsgCreateTicket:
              _request = (WCP_TITO_MsgCreateTicket)_ctx.request.MsgContent;
              _response = (WCP_TITO_MsgCreateTicketReply)_ctx.response.MsgContent;

              _ctx.response.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_DATABASE_OFFLINE;

              if (_processed)  //&& !_row.IsNull("TicketId"))
              {
                _ctx.response.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_OK;
              } // if

              break;

            default:
              continue;
          }// switch (_ctx.request.MsgHeader.MsgType)

          _ctx.tcp_client.Send(_ctx.response.ToXml());
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }
      } // foreach (DataRow _row in _table.Rows)
    }
    catch (Exception _ex)
    {
      Log.Exception(_ex);
    }
  } // TaskTitoManageTicket

  /// <summary>
  /// Update Status, MachineNumber, etc. in one ticket 
  /// </summary>
  /// <param name="TCPClient"></param>
  /// <param name="TitoRequest"></param>
  /// <param name="TitoResponse"></param>
  /// <param name="NumRegProcessed"></param>
  /// <param name="Trx"></param>
  /// <returns></returns>
  private static Boolean TITO_ProcessUpdateTicket(SecureTcpClient TCPClient,
                                                   WCP_Message TitoRequest,
                                                   WCP_Message TitoResponse,
                                                   out Int32 NumRegProcessed,
                                                   SqlTransaction Trx)
  {
    Ticket _tito_ticket;
    WCP_TITO_MsgCreateTicket _request;
    StringBuilder _sb;
    Int64 _validation_number;

    SqlParameter _original_ticket_amt0;
    SqlParameter _original_ticket_cur0;
    SqlParameter _original_ticket_amount;

    SqlParameter _update_ticket_amt0;
    SqlParameter _update_ticket_cur0;
    SqlParameter _update_ticket_amount;

    SqlParameter _alarm_id;

    StringBuilder _alarm_description;

    Int64 _sesion_id;
    String _terminal_name;
    String _provider_id;

    Decimal _request_amount;
    Decimal _old_amount;

    _sb = new StringBuilder();
    _tito_ticket = new Ticket();
    _request = (WCP_TITO_MsgCreateTicket)TitoRequest.MsgContent;

    NumRegProcessed = 0;
    _validation_number = ValidationNumberManager.GetCalculatedValidationNumber(_request.SystemId, _request.ValidationNumberSequence);

    try
    {
      _sb.Append("TITO_UpdateTicketStatusToValid");

      //TODO: Revisar casuística de creación de un ticket si este no se ha podido "UPDATAR" (SMN & FJC & ACC)
      using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
      {
        _cmd.CommandType = CommandType.StoredProcedure;

        _cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = (Int32)TITO_TICKET_STATUS.VALID;
        _cmd.Parameters.Add("@pMachineNumber", SqlDbType.BigInt).Value = _request.MachineTicketNumber;
        _cmd.Parameters.Add("@pValidationType", SqlDbType.Int).Value = _request.ValidationType;

        _cmd.Parameters.Add("@pCreatedTerminalId", SqlDbType.Int).Value = TCPClient.InternalId;
        _cmd.Parameters.Add("@pValidationNumber", SqlDbType.BigInt).Value = _validation_number;
        _cmd.Parameters.Add("@pTransactionId", SqlDbType.BigInt).Value = _request.TransactionId;
        _cmd.Parameters.Add("@pStatusPendingPrint", SqlDbType.Int).Value = (Int32)TITO_TICKET_STATUS.PENDING_PRINT;

        _cmd.Parameters.Add("@pAmountCents", SqlDbType.BigInt).Value = _request.AmountCents;
        _cmd.Parameters.Add("@pCreatedAccountID", SqlDbType.BigInt).Value = _request.CreatedAccountID;
        _cmd.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt).Value = _request.PlaySessionId;


        _original_ticket_amt0 = _cmd.Parameters.Add("@OriginalTicketAmt0", SqlDbType.Money);
        _original_ticket_amt0.Direction = ParameterDirection.Output;
        _original_ticket_cur0 = _cmd.Parameters.Add("@OriginalTicketCur0", SqlDbType.NVarChar, 3);
        _original_ticket_cur0.Direction = ParameterDirection.Output;
        _original_ticket_amount = _cmd.Parameters.Add("@OriginalTicketAmount", SqlDbType.Money);
        _original_ticket_amount.Direction = ParameterDirection.Output;
        
        _update_ticket_amt0 = _cmd.Parameters.Add("@UpdateTicketAmt0", SqlDbType.Money);
        _update_ticket_amt0.Direction = ParameterDirection.Output;
        _update_ticket_cur0 = _cmd.Parameters.Add("@UpdateTicketCur0", SqlDbType.NVarChar, 3);
        _update_ticket_cur0.Direction = ParameterDirection.Output;
        _update_ticket_amount = _cmd.Parameters.Add("@UpdateTicketAmount", SqlDbType.Money);
        _update_ticket_amount.Direction = ParameterDirection.Output;

        _alarm_id = _cmd.Parameters.Add("@AlarmId", SqlDbType.Int);
        _alarm_id.Direction = ParameterDirection.Output;

        NumRegProcessed = _cmd.ExecuteNonQuery();

        if ((Int32)_alarm_id.Value > 0)
        {
          _alarm_description = new StringBuilder();
          WSI.Common.BatchUpdate.TerminalSession.GetOpenedTerminalSession(TCPClient.InternalId, out _sesion_id);
          WSI.Common.BatchUpdate.TerminalSession.GetData(TCPClient.InternalId, _sesion_id, out _provider_id, out _terminal_name);

          _request_amount = (Decimal)_request.AmountCents / (Decimal)100.00;
          _old_amount = (Decimal)_original_ticket_amt0.Value;

          // Insert Alarm
          if ((Int32)_alarm_id.Value == 1)
          {
            _alarm_description.AppendFormat("Ticket {0}: Validating: amount is null or zero", _validation_number);
            Alarm.Register(AlarmSourceCode.TerminalSystem
                           , TCPClient.InternalId
                           , Misc.ConcatProviderAndTerminal(_provider_id, _terminal_name)
                           , (Int32)AlarmCode.Service_Ticket_NewAmountIsNullOrZero
                           , _alarm_description.ToString()
                           , AlarmSeverity.Error
                           , WGDB.Now
                           , Trx);
          }
          if ((Int32)_alarm_id.Value == 2)
          {
            _alarm_description.AppendFormat("Ticket {0}: Validating: New amount ({1}) greater of old amount ({2})", _validation_number, _request_amount.ToString("#,##0.00"), _old_amount.ToString("#,##0.00"));
            Alarm.Register(AlarmSourceCode.TerminalSystem
                           , TCPClient.InternalId
                           , Misc.ConcatProviderAndTerminal(_provider_id, _terminal_name)
                           , (Int32)AlarmCode.Service_Ticket_NewAmountGreaterOfOldAmount
                           , _alarm_description.ToString()
                           , AlarmSeverity.Error
                           , WGDB.Now
                           , Trx);
          }
          if ((Int32)_alarm_id.Value == 3)
          {
            _alarm_description.AppendFormat("Ticket {0}: Validating: New amount ({1}) smaller of old amount ({2})", _validation_number, _request_amount.ToString("#,##0.00"), _old_amount.ToString("#,##0.00"));
            Alarm.Register(AlarmSourceCode.TerminalSystem
                           , TCPClient.InternalId
                           , Misc.ConcatProviderAndTerminal(_provider_id, _terminal_name)
                           , (Int32)AlarmCode.Service_Ticket_NewAmountSmallerOfOldAmount
                           , _alarm_description.ToString()
                           , AlarmSeverity.Error
                           , WGDB.Now
                           , Trx);
          }
          if ((Int32)_alarm_id.Value == 4)
          {
            _alarm_description.AppendFormat("Ticket {0}: Validating: New currency ({1}) is diferent of older ({2}) ", _validation_number, _update_ticket_cur0.Value.ToString(), _original_ticket_cur0.Value.ToString());
            Alarm.Register(AlarmSourceCode.TerminalSystem
                           , TCPClient.InternalId
                           , Misc.ConcatProviderAndTerminal(_provider_id, _terminal_name)
                           , (Int32)AlarmCode.Service_Ticket_NewCurrencyIsDiferentOfOlder
                           , _alarm_description.ToString()
                           , AlarmSeverity.Error
                           , WGDB.Now
                           , Trx);
          }
          if ((Int32)_alarm_id.Value == 5)
          {
            _alarm_description.AppendFormat("Ticket {0}: Validating: New amount ({1}) greater of old amount ({2}) with diferent currency ({3}->{4})", _validation_number, _request_amount.ToString("#,##0.00"), _old_amount.ToString("#,##0.00"), _original_ticket_cur0.Value.ToString(), _update_ticket_cur0.Value.ToString());
            Alarm.Register(AlarmSourceCode.TerminalSystem
                           , TCPClient.InternalId
                           , Misc.ConcatProviderAndTerminal(_provider_id, _terminal_name)
                           , (Int32)AlarmCode.Service_Ticket_NewAmountGreaterOfOldAmountWithDiferentCurrency
                           , _alarm_description.ToString()
                           , AlarmSeverity.Error
                           , WGDB.Now
                           , Trx);
          }
          if ((Int32)_alarm_id.Value == 6)
          {
            _alarm_description.AppendFormat("Ticket {0}: Validating: New amount ({1}) smaller of old amount ({2}) with diferent currency ({3})", _validation_number, _request_amount.ToString("#,##0.00"), _old_amount.ToString("#,##0.00"), _original_ticket_cur0.Value.ToString(), _update_ticket_cur0.Value.ToString());
            Alarm.Register(AlarmSourceCode.TerminalSystem
                           , TCPClient.InternalId
                           , Misc.ConcatProviderAndTerminal(_provider_id, _terminal_name)
                           , (Int32)AlarmCode.Service_Ticket_NewAmountSmallerOfOldAmountWithDiferentCurrency
                           , _alarm_description.ToString()
                           , AlarmSeverity.Error
                           , WGDB.Now
                           , Trx);
          }
        }

        return true;
      }
    }
    catch (Exception _ex)
    {
      Log.Exception(_ex);
    }

    return false;
  } // TITO_ProcessUpdateTicket  
  #endregion

  #region "Public Functions"

  /// <summary>
  /// Thread for updating status (and other fields) for tickets
  /// </summary>
  /// <param name="Queue"></param>
  public static void TitoUpdateTicketThread(Object Queue)
  {
    WaitableQueue _queue;
    Object _item;
    WCP_Context _ctx;

    DataTable _table;
    DataRow _row;
    int _tick0;
    int _timeout;
    int _max_wait;
    long _elapsed;
    String _alarm_description;

    WCP_TITO_MsgCreateTicket _request;
    _alarm_description = String.Empty;

    _queue = (WaitableQueue)Queue;

    _ctx = null;
    _request = null;
    _tick0 = 0;

    int _tick_log = Misc.GetTickCount();


    while (true)
    {
      try
      {
        _max_wait = GeneralParam.GetInt32("", "", 10);
        _max_wait = Math.Max(0, Math.Min(1000, _max_wait));

        _table = new DataTable();
        _table.Columns.Add("CTX", Type.GetType("System.Object"));
        _table.Columns.Add("TerminalId", Type.GetType("System.Int32"));
        //_table.Columns.Add("ValidationNumber", Type.GetType("System.Int64"));
        //_table.Columns.Add("ValidationType", Type.GetType("System.Int32"));
        //_table.Columns.Add("MachineTicketNumber", Type.GetType("System.Int32"));
        //_table.Columns.Add("NewStatus", Type.GetType("System.Int32"));
        //_table.Columns.Add("TransactionId", Type.GetType("System.Int64"));
        //_table.Columns.Add("TicketId", Type.GetType("System.Int64"));
        //_table.Columns.Add("TicketType", Type.GetType("System.Int32"));
        //_table.Columns.Add("TicketAmount", Type.GetType("System.Decimal"));
        //_table.Columns.Add("TicketExpiration", Type.GetType("System.DateTime"));
        //_table.Columns.Add("TicketCreated", Type.GetType("System.DateTime"));

        _table.Rows.Clear();
        _tick0 = Misc.GetTickCount();
        _timeout = Timeout.Infinite;
        while (_queue.Dequeue(out _item, _timeout))
        {
          _ctx = (WCP_Context)_item;

          if (_ctx.tcp_client.IsConnected)
          {
            switch (_ctx.request.MsgHeader.MsgType)
            {
              case WCP_MsgTypes.WCP_TITO_MsgCreateTicket:
                _request = (WCP_TITO_MsgCreateTicket)_ctx.request.MsgContent;

                if (_request.SystemId < 0
                    || _request.ValidationNumberSequence < 0) //FJC PBI 18258:TITO: ¿ValidationNumber = 0 is a wrong validation number?
                {
                  // Avoid terminal continue sending the "wrong" validation number
                  _ctx.response.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_OK;
                  _ctx.tcp_client.Send(_ctx.response.ToXml());

                  Log.Message(String.Format("Wrong Validation Number. SystemId:{0}, ValidationSequence: {1}, Terminal: {2}, Cents: {3}, TicketType: {4}",
                                             _request.SystemId, _request.ValidationNumberSequence, _ctx.tcp_client.InternalId, _request.AmountCents, _request.TicketType));


                  _alarm_description = Resource.String("STR_WCP_ALARM_CREATE_TICKET_WRONG_VALIDATION_NUMBER", new object[] { _request.SystemId, _request.ValidationNumberSequence, _ctx.tcp_client.InternalId, _request.AmountCents, _request.TicketType });


                  Alarm.Register(AlarmSourceCode.TerminalWCP,
                          _ctx.tcp_client.InternalId,
                          "TITO_WCP",
                          (UInt32)AlarmCode.WCP_TITO_WrongValidationNumber,
                          _alarm_description,
                          AlarmSeverity.Error);

                  continue;
                }

                _row = _table.NewRow();
                _row["CTX"] = _ctx;
                _row["TerminalId"] = _ctx.tcp_client.InternalId;
                //_row["ValidationNumber"] = ValidationNumberManager.GetCalculatedValidationNumber(_request.SystemId, _request.ValidationNumberSequence);
                //_row["ValidationType"] = _request.ValidationType;
                //_row["MachineTicketNumber"] = _request.MachineTicketNumber;
                //_row["TicketAmount"] = ((Decimal)_request.AmountCents) / 100m;
                //_row["TicketType"] = _request.TicketType;
                //_row["TicketCreated"] = _request.Date; 
                //_row["TransactionId"] = _request.TransactionId;

                //if ( _request.ExpirationDate > DateTime.MinValue) 
                //{
                //  _row["TicketExpiration"] = _request.ExpirationDate;
                //}
                //else
                //{
                //  _expiration_days = Utils.DefaultTicketExpiration((TITO_TICKET_TYPE) _request.TicketType);
                // _row["TicketExpiration"] = _request.Date.AddDays(_expiration_days);
                //}

                _table.Rows.Add(_row);
                break;

              default:
                break;
            }

            if (_table.Rows.Count == 1)
            {
              _tick0 = Misc.GetTickCount();
              _timeout = _max_wait;
            }

            if (_table.Rows.Count >= 1000) break;
          }

          if (_table.Rows.Count == 0)
          {
            _timeout = Timeout.Infinite;
            continue;
          }

          _elapsed = Misc.GetElapsedTicks(_tick0);
          if (_elapsed < _max_wait)
          {
            _timeout = (Int32)(_max_wait - _elapsed);
          }
          else
          {
            // Continue dequeueing items even we reached the 500 ms. 
            _timeout = 0;
          }
        } // while

        if (_table.Rows.Count <= 0)
        {
          continue;
        }

        TaskTitoManageTicket(_table);

        // ThreadPool.QueueUserWorkItem(new WaitCallback(TaskTitoCreateTicket), _table);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    } // while (true)
  } // TitoUpdateTicketThread

  /// <summary>
  /// Create one ticket in DB
  /// </summary>
  /// <param name="TCPClient"></param>
  /// <param name="TitoRequest"></param>
  /// <param name="TitoResponse"></param>
  /// <param name="TitoResponseValidNumber"></param>
  /// <param name="Trx"></param>
  /// <returns></returns>
  public static Boolean TITO_ProcessCreateTicket(SecureTcpClient TCPClient,
                                                  WCP_Message TitoRequest,
                                                  WCP_Message TitoResponse,
                                                  WCP_TITO_MsgValidationNumberReply TitoResponseValidNumber,
                                                  SqlTransaction Trx)
  {
    return WCP_TITO.TITO_ProcessCreateTicket(TCPClient, TitoRequest, TitoResponse, TitoResponseValidNumber, TITO_TICKET_STATUS.PENDING_PRINT, Trx);
  }

  /// <summary>
  /// Create one ticket in DB
  /// </summary>
  /// <param name="TCPClient"></param>
  /// <param name="TitoRequest"></param>
  /// <param name="TitoResponse"></param>
  /// <param name="TitoResponseValidNumber"></param>
  /// <param name="ForceTicketStatus"></param>
  /// <param name="Trx"></param>
  /// <returns></returns>
  public static Boolean TITO_ProcessCreateTicket(SecureTcpClient TCPClient,
                                                  WCP_Message TitoRequest,
                                                  WCP_Message TitoResponse,
                                                  WCP_TITO_MsgValidationNumberReply TitoResponseValidNumber,
                                                  TITO_TICKET_STATUS ForceTicketStatus,
                                                  SqlTransaction Trx)
  {
    WCP_TITO_MsgCreateTicket _request;

    Int32 _expiration_days;
    Ticket _tito_ticket;
    Int64 _validation_number;
    Int32 _terminal_id;
    String _terminal_name;
    Int64 _play_session_id;
    Int64 _last_open_ps_id;
    Int64 _cashier_session_id;
    Int64 _money_collection_id;
    Int64 _virtual_account_id;
    String _virtual_track_data;
    Int32 _cashier_terminal_id;
    Int64 _mobile_bank_id;
    String _mobile_bank_track_data;
    Int64 _mobile_bank_session_id;
    Int32 _user_id;
    String _user_name;
    HANDPAY_TYPE _handpay_type;
    Boolean _does_handpay_exists;
    String _provider_id;
    String _cashier_name;
    CashierMovementsTable _cashier_movements;
    AccountMovementsTable _account_movements;
    Int64 _player_account_id;

    _tito_ticket = new Ticket();

    _request = (WCP_TITO_MsgCreateTicket)TitoRequest.MsgContent;

    // FJC 07-10-2016. (PBI 18258) If not comment, doesn't make a cash-out in EGM
    if (!GeneralParam.GetBoolean("TITO", "PendingPrint.Enabled", true))
    {
      TitoResponse.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_DATABASE_OFFLINE;
    }

    _validation_number = ValidationNumberManager.GetCalculatedValidationNumber(_request.SystemId, _request.ValidationNumberSequence);

    _tito_ticket.PromotionID = 0;
    //_tito_ticket.Status = TITO_TICKET_STATUS.VALID; // FJC 07-10-2016. (PBI 18258)
    _tito_ticket.Status = ForceTicketStatus;
    _tito_ticket.TransactionId = _request.TransactionId;
    _tito_ticket.MachineTicketNumber = _request.MachineTicketNumber;
    _tito_ticket.TicketType = (TITO_TICKET_TYPE)_request.TicketType;
    _tito_ticket.ValidationType = (TITO_VALIDATION_TYPE)_request.ValidationType;
    _tito_ticket.CreatedDateTime = _request.Date;

    // DDM & JML 06-MAY-2015: Fixed Bug WIG-2413 Sometimes, the creation date of the ticket is the year "0015"
    if (WGDB.Now.Date.AddDays(-7) > _tito_ticket.CreatedDateTime)
    {
      //DDM 06-MAY-2015 Creation DateTime out of range. The ticket is created with today's date
      Log.Warning(String.Format("TITO_ProcessCreateTicket (DateTime: {0}, ValidationNumber: {1}, TerminalId: {2}), Creation DateTime out of range.",
                  _tito_ticket.CreatedDateTime.ToString("yyyy-MM-ddTHH:mm:ss"), _validation_number, TCPClient.InternalId));

      _tito_ticket.CreatedDateTime = WGDB.Now;

    }

    //if (_request.ExpirationDate > DateTime.MinValue)
    //{
    //  _tito_ticket.ExpirationDateTime = _request.ExpirationDate.AddDays(1); // AJQ 20/OCT/2014, This is a "date" so we need to add a day
    //}
    //else
    //{
    //  _expiration_days = Utils.DefaultTicketExpiration(_tito_ticket.TicketType);
    //  _tito_ticket.ExpirationDateTime = _tito_ticket.CreatedDateTime.AddDays(_expiration_days);
    //}

    // AJQ 20/OCT/2014, Expiration defined by the system (ignore machine)
    _expiration_days = Utils.DefaultTicketExpiration(_tito_ticket.TicketType);
    _tito_ticket.ExpirationDateTime = _tito_ticket.CreatedDateTime.AddDays(_expiration_days);

    _tito_ticket.ValidationNumber = _validation_number;
    _tito_ticket.Amount = _request.AmountCents / (Decimal)100.0;
    _tito_ticket.PoolID = _request.PoolId;
    _tito_ticket.CreatedTerminalType = (Int32)TITO_TERMINAL_TYPE.TERMINAL;
    _tito_ticket.CreatedTerminalId = TCPClient.InternalId;
    _tito_ticket.Amt_0 = _request.AmountCents / (Decimal)100.0;

    try
    {
      {
        SqlTransaction _sql_trx = Trx;

        // Check if the ticket already exists        
        if (_tito_ticket.DB_IsTicketAlreadyCreated(_sql_trx))
        {
          TitoResponse.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_OK;

          return true;
        }

        _terminal_id = TCPClient.InternalId;
        _last_open_ps_id = 0;

        if (!WCP_PlaySessionLogic.GetTitoSessionObjects(_terminal_id,
                                                        _request.TransactionId,
                                                        false,
                                                        MB_USER_TYPE.SYS_TITO,
                                                        out _terminal_name,
                                                        out _last_open_ps_id,
                                                        out _cashier_session_id,
                                                        out _money_collection_id,
                                                        out _virtual_account_id,
                                                        out _virtual_track_data,
                                                        out _cashier_terminal_id,
                                                        out _mobile_bank_id,
                                                        out _mobile_bank_track_data,
                                                        out _mobile_bank_session_id,
                                                        out _user_name,
                                                        out _user_id,
                                                        out _provider_id,
                                                        out _cashier_name,
                                                        out _player_account_id,
                                                        _sql_trx))
        {
          return false;
        }

        _tito_ticket.CreatedTerminalId = _terminal_id;

        _play_session_id = 0;
        // use reported play session for ticket
        if (_request.PlaySessionId > 0)
        {
          _play_session_id = _request.PlaySessionId;
        }

        if (_play_session_id == 0)
        {
          if (_last_open_ps_id > 0)
          {
            _play_session_id = _last_open_ps_id;
          }
          else
          {
            // if play-session not found, then search the last created for terminal
            // ¡ NEVER create it !
            //
            PlaySession.LoadLastPlaySession(_terminal_id, false/*ActiveSession*/, out _play_session_id, _sql_trx);
          }
        }

        _tito_ticket.CreatedAccountID = (_player_account_id == 0 ? _virtual_account_id : _player_account_id);
        _tito_ticket.CreatedPlaySessionId = _play_session_id;

        CashierSessionInfo _cashier_session;
        _cashier_session = new CashierSessionInfo();
        _cashier_session.CashierSessionId = _cashier_session_id;
        _cashier_session.TerminalId = _cashier_terminal_id;
        _cashier_session.TerminalName = _cashier_name;
        _cashier_session.AuthorizedByUserId = _user_id;
        _cashier_session.AuthorizedByUserName = _user_name;

        _cashier_movements = new CashierMovementsTable(_cashier_session);
        _account_movements = new AccountMovementsTable(_cashier_session);

        if (!_tito_ticket.DB_CreateTicket(_sql_trx, _virtual_track_data, false, Ticket.ENUM_PAYMENT_TYPE.Ticket, 0, _cashier_movements, _account_movements))
        {
          return false;
        }
        TitoResponseValidNumber.transaction_id = _tito_ticket.TransactionId;
        TitoResponseValidNumber.validation_number_sequence = _tito_ticket.ValidationNumber;

        if (!_account_movements.Add(0, _virtual_account_id, MovementType.TITO_TerminalToAccountCredit, 0, 0, _tito_ticket.Amount, _tito_ticket.Amount, ""))
        {
          return false;
        }

        if (_tito_ticket.TicketType == TITO_TICKET_TYPE.HANDPAY ||
            _tito_ticket.TicketType == TITO_TICKET_TYPE.JACKPOT)
        {
          _handpay_type = _tito_ticket.TicketType == TITO_TICKET_TYPE.HANDPAY ? HANDPAY_TYPE.CANCELLED_CREDITS : HANDPAY_TYPE.JACKPOT;

          // If there is no related hand pay, create one
          if (!HandPay.DB_DoesHandPayExists(_terminal_id,
                                            _request.TransactionId,
                                            _handpay_type,
                                            _tito_ticket.Amount,
                                            out _does_handpay_exists,
                                            _sql_trx))
          {
            Log.Error("TITO_ProcessCreateTicket -->  DB_DoesHandPayExists failed!!! ");

            return false;
          }

          if (!_does_handpay_exists)
          {
            // DHA 29-MAR-2016: send issued amount
            if (!HandPay.DB_InsertNewHandPay(_tito_ticket.TicketID,
                                        _terminal_id,
                                        _request.TransactionId,
                                        _tito_ticket.Amt_0,
                                        _handpay_type,
                                        _terminal_name,
                                        _provider_id,
                                        WGDB.Now,
                                        _sql_trx))
            {
              Log.Error("TITO_ProcessCreateTicket -->  DB_InsertNewHandPay failed!!! ");

              return false;
            }
          }
          else
          {
            if (!WCP_HPCMeter.DB_UpdateHandpayTicketId(_terminal_id,
                                                       _request.TransactionId,
                                                       _handpay_type,
                                                       _tito_ticket.Amount,
                                                       _tito_ticket.TicketID,
                                                       _sql_trx))
            {
              Log.Error("TITO_ProcessCreateTicket -->  DB_UpdateHandpayTicketID failed!!! ");
            }
          }
        }

        if (!_account_movements.Save(_sql_trx))
        {
          return false;
        }

        if (!_cashier_movements.Save(_sql_trx))
        {
          return false;
        }

        // ICS 24-12-2013: Reset terminal status
        if (!TerminalStatusFlags.SetTerminalStatus(_terminal_id, TerminalStatusFlags.WCP_TerminalEvent.TicketOut, _sql_trx))
        {
          Log.Error("TITO_ProcessCreateTicket --> SetTerminalStatus: retunr false");
        }

        return true;
      }
    }
    catch (Exception _ex)
    {
      Log.Exception(_ex);
    }

    return false;

  } // TITO_ProcessCreateTicket  
  #endregion

}
