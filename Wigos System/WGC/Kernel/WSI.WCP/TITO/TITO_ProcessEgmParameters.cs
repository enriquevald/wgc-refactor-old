//------------------------------------------------------------------------------
// Copyright © 2013 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: TITO_ProcessEgmParameters.cs
// 
//   DESCRIPTION: Set Parameters for a terminal 
//   AUTHOR     : Joshwa Marcalle
// 
//   REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 07-Oct-2013 JRM    First version.
// 09-Oct-2013 JRM    Renaming variables according to new specifications. Added parameters saving capability
// 26-OCT-2013 JRM    An alarm is sent if configuration params are different then in central
// 29-OCT-2013 NMR    Changes in protocol for parameters send
// 06-NOV-2013 NMR    Removed Asset Number and MachineValidation
// 08-NOV-2013 NMR    Process improvement (query)
// 21-NOV-2013 JBP    Edited alarm for TITO_ProcessEgmParameters ()
// 26-NOV-2013 JRM    refactoring and code cleaning
// 13-APR-2015 FJC    BackLog Item 940
// 03-JAN-2017 MS     Bug 3911:Alarma "Discrepancia en la configuración del terminal": los valores Esperado y Actual están intercambiados
// 15-FEB-2017 JML    Fixed Bug 24635:WCP: Comparison of accounting denomination, incorrect
// 17-OCT-2017 ETL    Fixed Bug 30289: WIGOS-5736 [Ticket #9499] Garcia River - Alarma - Terminal configuration Discrepancy
// 25-JAN-2018 FGB    WIGOS-7569: AGG SITE-Terminal analyse's screen: error identifiying the the anomaly "change of denomination"
//------------------------------------------------------------------------------

using System;
using System.Data.SqlClient;
using System.Text;
using WSI.Common;
using WSI.WCP;

public static partial class WCP_TITO
{
  //------------------------------------------------------------------------------
  // PURPOSE: Process TITO_ProcessEgmParameters Message
  //
  //  PARAMS:
  //      - INPUT: Input Message ( request message  ) 
  //
  //      - OUTPUT:
  //
  // RETURNS:
  private static void TITO_ProcessEgmParameters(SecureTcpClient TcpClient, WCP_Message TitoRequest, WCP_Message TitoResponse)
  {
    WCP_TITO_MsgEgmParameters _request;
    WSI.WCP.Terminal _terminal;
    Boolean _update_result;
    StringBuilder _alarm_description;
    Boolean _current_allow_cashable_ticket_out;
    Boolean _current_allow_promotional_ticket_out;
    Boolean _current_allow_ticket_in;

    String _serial_number_config;
    String _serial_number_report;
    Int64 _asset_number_config;
    Int64 _asset_number_report;
    Decimal _sas_accounting_denom;

    Decimal _old_sas_accounting_denom;
    Decimal _new_sas_accounting_denom;
    Boolean _sas_accounting_denom_has_changed;

    _serial_number_config = string.Empty;
    _serial_number_report = string.Empty;
    _asset_number_config = 0;
    _asset_number_report = 0;
    _sas_accounting_denom = 0;

    _old_sas_accounting_denom = 0;
    _new_sas_accounting_denom = 0;

    _request = (WCP_TITO_MsgEgmParameters)TitoRequest.MsgContent;
    _terminal = new WSI.WCP.Terminal();

    _terminal.external_id = TitoRequest.MsgHeader.TerminalId;
    _terminal.terminal_id = TcpClient.InternalId;

    try
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        SqlTransaction _sql_trx = _db_trx.SqlTransaction;

        // Get the current parameters of the terminal to determine state accuracy. If it is not accurate, throw an alarm
        //  * Get serial number (reported and configured)
        _terminal.GetEgmParams(_terminal.terminal_id,
                               out _current_allow_cashable_ticket_out,
                               out _current_allow_promotional_ticket_out,
                               out _current_allow_ticket_in,
                               out _serial_number_config,
                               out _serial_number_report,
                               out _asset_number_config,
                               out _asset_number_report,
                               out _sas_accounting_denom,
                               _sql_trx);

        if (_request.SASAccountDenomCents == 0 && _sas_accounting_denom > 0)
        {
          _request.SASAccountDenomCents = (long)(_sas_accounting_denom * 100);
        }

        // Check SERIAL NUMBER (Reported and Configured)
        //  If reported(SAS-Host) is different from reported(BD) AND reported(SAS-Host) is different from configured(BD) throw alarm.
        if (_serial_number_report != _request.SASSerialNumber && _request.SASSerialNumber != _serial_number_config)
        {
          _alarm_description = new StringBuilder();
          _alarm_description.AppendLine(Resource.String("STR_ALARM_TERMINAL_SERIAL_NUMBER_DISCRPANCY", new String[] { _terminal.name, _request.SASSerialNumber.ToString(), _serial_number_config.ToString() }));

          Alarm.Register(AlarmSourceCode.TerminalSystem,
                         (long)_terminal.terminal_id,
                         _terminal.name,
                         (UInt32)AlarmCode.Machine_Serial_Number_Not_Expected,
                         _alarm_description.ToString().TrimEnd('\n', '\r'),
                         AlarmSeverity.Warning);
        }

        // Check ASSET NUMBER (Reported and Configured)
        //  If reported(SAS-Host) is different than reported(BD) AND reported(SAS-Host) is different than configured(BD) throw alarm.
        if (_asset_number_report != _request.SASAssetNumber && _request.SASAssetNumber != _asset_number_config)
        {
          _alarm_description = new StringBuilder();
          _alarm_description.AppendLine(Resource.String("STR_ALARM_TERMINAL_ASSET_NUMBER_DISCRPANCY", new String[] { _terminal.name, _request.SASAssetNumber.ToString(), _asset_number_config.ToString() }));

          Alarm.Register(AlarmSourceCode.TerminalSystem,
                         (long)_terminal.terminal_id,
                         _terminal.name,
                         (UInt32)AlarmCode.Machine_Asset_Number_Not_Expected,
                         _alarm_description.ToString().TrimEnd('\n', '\r'),
                         AlarmSeverity.Warning);
        }

        // Update the terminal with the information received
        _update_result = _terminal.UpdateEgmParameters(_terminal.terminal_id,
                                                       _request.ValidationType,
                                                       _request.ValidationSequence,
                                                       _request.SASVersion,
                                                       _request.SASMachineName,
                                                       _request.Bonus,
                                                       _request.Features,
                                                       _request.SASSerialNumber,
                                                       _request.SASAssetNumber,
                                                       _request.SASAccountDenomCents,
                                                       _sql_trx);

        _old_sas_accounting_denom = Decimal.Round(_sas_accounting_denom, 2);
        _new_sas_accounting_denom = Decimal.Round(((Decimal)_request.SASAccountDenomCents / 100.00m), 2);

        //Detected a SAS Accounting Denom change
        _sas_accounting_denom_has_changed = ((_old_sas_accounting_denom != _new_sas_accounting_denom) && (_sas_accounting_denom != 0) && (_new_sas_accounting_denom != WCP_BU_MachineMeters.EMPTY_SAS_ACCOUNTING_DENOM));

        if (_sas_accounting_denom_has_changed)
        {
          _alarm_description = new StringBuilder();
          _alarm_description.AppendLine(Resource.String("STR_ALARM_SAS_ACCOUNTING_DENOMINATION_CHANGE", new String[] { _terminal.name, _old_sas_accounting_denom.ToString("0.00"), _new_sas_accounting_denom.ToString("0.00") }));

          //Write log
          Log.Warning(_alarm_description.ToString());

          //Write alarm
          Alarm.Register(AlarmSourceCode.TerminalSystem,
                         (long)_terminal.terminal_id,
                         _terminal.name,
                         (UInt32)AlarmCode.Machine_Sas_Accounting_Denomination_Changed,
                         _alarm_description.ToString().TrimEnd('\n', '\r'),
                         AlarmSeverity.Warning);

          //Call to generate denom change meters for current meters
          WCP_BU_MachineMeters.Init();
          WCP_BU_MachineMeters.GenerateDenomChange(_terminal.terminal_id, _old_sas_accounting_denom, _new_sas_accounting_denom, _sql_trx);
        }

        if (_current_allow_cashable_ticket_out != _request.AllowCashableTicketOut ||
            _current_allow_promotional_ticket_out != _request.AllowPromotionalTicketOut ||
            _current_allow_ticket_in != _request.AllowTicketIn)
        {
          _alarm_description = new StringBuilder();
          _alarm_description.AppendLine(Resource.String("STR_ALARM_TITO_TERMINAL_CONFIG_DISCRPANCY"));
          _alarm_description.AppendLine("");

          // Bug 3911:Alarma "Discrepancia en la configuración del terminal": los valores Esperado y Actual están intercambiados
          // Swapped the third and second parameter of each
          if (_current_allow_cashable_ticket_out != _request.AllowCashableTicketOut)
          {
            _alarm_description.AppendLine(Resource.String("STR_ALARM_TITO_TERMINAL_CONFIG_DISCRPANCY_DETAIL",
                                          new String[] { "CashableTickets.AllowEmission", _current_allow_cashable_ticket_out.ToString(), _request.AllowCashableTicketOut.ToString() }));
          }

          if (_current_allow_promotional_ticket_out != _request.AllowPromotionalTicketOut)
          {
            _alarm_description.AppendLine(Resource.String("STR_ALARM_TITO_TERMINAL_CONFIG_DISCRPANCY_DETAIL",
                                          new String[] { "PromotionalTickets.AllowEmission", _current_allow_promotional_ticket_out.ToString(), _request.AllowPromotionalTicketOut.ToString() }));
          }

          if (_current_allow_ticket_in != _request.AllowTicketIn)
          {
            _alarm_description.AppendLine(Resource.String("STR_ALARM_TITO_TERMINAL_CONFIG_DISCRPANCY_DETAIL",
                                          new String[] { "AllowRedemption", _current_allow_ticket_in.ToString(), _request.AllowTicketIn.ToString() }));
          }

          Alarm.Register(AlarmSourceCode.System,
                         (long)_terminal.terminal_id,
                         _terminal.name,
                         (UInt32)AlarmCode.WCP_TerminalParamsDoNotMatch,
                         _alarm_description.ToString().TrimEnd('\n', '\r'),
                         AlarmSeverity.Warning);
        }

        if (!_update_result)
        {
          TitoResponse.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_TITO_TICKET_ERROR;
        }
        else
        {
          _db_trx.Commit();
          TitoResponse.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_OK;
        }
      }
    }
    catch (Exception _ex)
    {
      Log.Exception(_ex);
      TitoResponse.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_TITO_TICKET_ERROR;
    }
  } // TITO_ProcessEgmParameters  
}