//------------------------------------------------------------------------------
// Copyright � 2013 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME:  TITO_ProcessValidationNumber.cs
// 
//   DESCRIPTION: Process message for ValidationNumberSequence creation
//
//   AUTHOR     : Nelson Madrigal Reyes
// 
//   REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 20-NOV-2013 NMR    First version.
// 05-MAY-2014 HBB    Fixed Bug: WIGOSTITO-1214: It is possible to make a ticket out even when it is not available in TITO Configuration.
// 06-MAY-2014 HBB    Fixed Bug: WIGOSTITO-1216: It is not taken in account the custom configuration for terminals. 
// 13-OCT-2016 FJC    PBI 18258:TITO: Nuevo estado "Pending Print" en tickets - Creaci�n nuevo estado
// 18-JUL-2017 FJC    Defect WIGOS-3118 [Ticket #4699] Too many handpays being generated because of late arrival of ticket numbers
// 29-AUG-2017 DPC    Bug 29484:[WIGOS-4276]: Cashier - The screen "Ultimos tickets" is displaying a ticket tito that shouldn't be displayed
//------------------------------------------------------------------------------

using System;
using System.Text;
using WSI.WCP;
using WSI.Common;
using WSI.Common.TITO;
using System.Data;
using System.Threading;
using System.Data.SqlClient;

public static partial class WCP_TITO
{
  #region "Members"
  private static WaitableQueue m_validation_number = new WaitableQueue();
  #endregion

  #region "Private functions"

  //------------------------------------------------------------------------------
  // PURPOSE: Process TITO_ProcessValidationNumber Message
  //
  //  PARAMS:
  //      - INPUT: 
  //         - TcpClient
  //         - TitoRequest
  //         - TitoResponse
  //
  //      - OUTPUT: 
  //         -  NONE
  //
  // RETURNS:
  // 
  private static void TITO_ProcessValidationNumber(SecureTcpClient TcpClient, WCP_Message TitoRequest, WCP_Message TitoResponse)
  {

    WCP_Context _ctx;

    _ctx = new WCP_Context();
    _ctx.tcp_client = TcpClient;
    _ctx.request = TitoRequest;
    _ctx.response = TitoResponse;

    m_validation_number.Enqueue(_ctx);
  } // TITO_ProcessValidationNumber

  /// <summary>
  /// Send Response Messages to SAS-HOST from DataTable
  /// </summary>
  /// <param name="DtTicketsForResponsing"></param>
  /// <returns></returns>
  private static Boolean SendResponseTicketsFromDt(DataTable DtTicketsForResponsing)
  {
    WCP_Context _ctx;

    try
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        foreach (DataRow _row_processed in DtTicketsForResponsing.Rows)
        {
          _ctx = (WCP_Context)_row_processed["CTX"];

          // FJC 07-10-2016. (PBI 18258). We made send response after ticket creation.
          _ctx.tcp_client.Send(((WCP_Message)_row_processed["ResponseObject"]).ToXml());
        }
      }
    }
    catch (Exception _ex)
    {
      Log.Exception(_ex);

      return false;
    }

    return true;
  } // SendResponseTicketsFromDt
  #endregion

  #region "Public functions"

  /// <summary>
  /// Converts an WCP Message "Request Validation Number" into WCP Message "Request Validation Number"
  /// </summary>
  /// <param name="_table"></param>
  public static Boolean ConvertWCPMsgValidNumberToWCPMsgCreateTicket(DataRow _row, ref SecureTcpClient TCPClient, ref WCP_Message TitoRequest, ref WCP_Message TitoResponse)
  {
    Object _item;
    WCP_Context _ctx;
    WCP_TITO_MsgCreateTicket _request;
    WCP_TITO_MsgCreateTicketReply _response;
    decimal _amount;

    _request = new WCP_TITO_MsgCreateTicket();
    _response = new WCP_TITO_MsgCreateTicketReply();

    try
    {
      _item = _row["CTX"];
      _ctx = (WCP_Context)_item;

      _request.TransactionId = (Int64)_row["TransactionId"];
      _request.TicketType = (Int32)_row["TicketType"];
      _amount = (Decimal)_row["TicketAmount"];
      _request.AmountCents = Convert.ToInt64(_amount * 100);
      _request.ValidationNumberSequence = (Int64)_row["NewSequenceValue"];
      _request.SystemId = (Int32)_row["SystemId"];
      _request.Date = WGDB.Now;

      //Request
      _ctx.request.MsgContent = _request;

      TCPClient = _ctx.tcp_client;
      TitoRequest = _ctx.request;
      TitoResponse = _ctx.response;

      return true;
    }
    catch (Exception _ex)
    {
      Log.Exception(_ex);
    }

    return false;
  } // ConvertWCPMsgValidNumberToWCPMsgCreateTicket

  /// <summary>
  /// Thread for creating tickets TITO
  /// </summary>
  public static void TitoCreateTicketThread()
  {
    Object _item;
    WCP_Context _ctx;

    DataTable _table;
    DataRow _row;
    Int32 _tick0;
    Int32 _timeout;
    Int32 _max_wait;
    Int64 _elapsed;
    WCP_TITO_MsgValidationNumber _request;
    WCP_TITO_MsgValidationNumberReply _response;
    WCP_TITO_MsgValidationNumberReply _response_valid_number;
    Boolean _processed;
    Int32 _num_validation_number_processed;
    Int64 _last_sequence;
    SystemValidationNumber _generator;
    WSI.WCP.Terminal _terminal;
    bool _allow_cashable_ticket_out;
    bool _allow_promotional_ticket_out;
    bool _allow_ticket_in;
    SecureTcpClient TCPClient;
    WCP_Message TitoRequest;
    WCP_Message TitoResponse;
    Boolean _error_inserting_ticket;


    _ctx = null;
    _request = null;
    _response = null;
    _tick0 = 0;
    _allow_cashable_ticket_out = false;
    _allow_promotional_ticket_out = false;
    _allow_ticket_in = false;

    _generator = new SystemValidationNumber();
    _terminal = new WSI.WCP.Terminal();

    _table = new DataTable();
    _table.Columns.Add("CTX", Type.GetType("System.Object"));
    _table.Columns.Add("TerminalId", Type.GetType("System.Int32"));
    _table.Columns.Add("TransactionId", Type.GetType("System.Int64"));
    _table.Columns.Add("TicketType", Type.GetType("System.Int32"));
    _table.Columns.Add("TicketAmount", Type.GetType("System.Decimal"));
    _table.Columns.Add("NewSequenceValue", Type.GetType("System.Int64"));
    _table.Columns.Add("SystemId", Type.GetType("System.Int32"));
    _table.Columns.Add("ResponseObject", Type.GetType("System.Object"));

    TCPClient = null;
    TitoRequest = null;
    TitoResponse = null;
    _error_inserting_ticket = false;
    _response_valid_number = new WCP_TITO_MsgValidationNumberReply();

    while (true)
    {
      try
      {
        _max_wait = GeneralParam.GetInt32("", "", 100);
        _max_wait = Math.Max(0, Math.Min(1000, _max_wait));

        _last_sequence = 0;
        _table.Rows.Clear();
        _tick0 = Misc.GetTickCount();
        _timeout = Timeout.Infinite;
        while (m_validation_number.Dequeue(out _item, _timeout))
        {
          _ctx = (WCP_Context)_item;

          if (_ctx.tcp_client.IsConnected)
          {
            switch (_ctx.request.MsgHeader.MsgType)
            {
              case WCP_MsgTypes.WCP_TITO_MsgValidationNumber:
                _request = (WCP_TITO_MsgValidationNumber)_ctx.request.MsgContent;
                _row = _table.NewRow();
                _row["CTX"] = _ctx;
                _row["TerminalId"] = _ctx.tcp_client.InternalId;
                _row["TransactionId"] = _request.TransactionId;  // FJC 07-10-2016. (PBI 18258)  // 0; // TODO: _request.TransactionId;
                _row["TicketType"] = _request.TicketType;
                _row["TicketAmount"] = _request.AmountCents * 0.01m;
                _table.Rows.Add(_row);

                break;
            }

            if (_table.Rows.Count == 1)
            {
              _tick0 = Misc.GetTickCount();
              _timeout = _max_wait;
            }

            if (_table.Rows.Count >= 500) break;
          }

          if (_table.Rows.Count == 0)
          {
            _timeout = Timeout.Infinite;
            continue;
          }

          _elapsed = Misc.GetElapsedTicks(_tick0);
          if (_elapsed < _max_wait)
          {
            _timeout = (Int32)(_max_wait - _elapsed);
          }
          else
          {
            // Continue dequeueing items even we reached the 500 ms. 
            _timeout = 0;
          }
        } // while

        _processed = false;
        try
        {
          StringBuilder _sb;

          _sb = new StringBuilder();
          _sb.AppendLine("DECLARE @pLastSeq as BigInt");
          _sb.AppendLine("");
          _sb.AppendLine("UPDATE   SEQUENCES         ");
          _sb.AppendLine("   SET   @pLastSeq      = ISNULL (SEQ_NEXT_VALUE, 1)  ");
          _sb.AppendLine("       , SEQ_NEXT_VALUE = SEQ_NEXT_VALUE + @pIncrement");
          _sb.AppendLine(" WHERE   SEQ_ID         = @pSeqId           ");
          _sb.AppendLine("SELECT   @pLastSeq");

          using (DB_TRX _db_trx = new DB_TRX())
          {
            using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
            {
              _sql_cmd.Parameters.Add("@pSeqId", SqlDbType.BigInt).Value = SequenceId.TITO_ValidationNumber;
              _sql_cmd.Parameters.Add("@pIncrement", SqlDbType.Int).Value = _table.Rows.Count;

              _last_sequence = (Int64)_sql_cmd.ExecuteScalar();
              _processed = true;
              _db_trx.Commit();
            }
          }

        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }

        if (_table.Rows.Count <= 0)
        {
          continue;
        }

        _processed = true;
        _num_validation_number_processed = 0;

        using (DB_TRX _db_trx_create_ticket = new DB_TRX())
        {
          // Send responses
          foreach (DataRow _row_processed in _table.Rows)
          {
            try
            {
              _ctx = (WCP_Context)_row_processed["CTX"];
              _ctx.response.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_TITO_INVALID_VALIDATION_NUMBER;

              using (DB_TRX _db_trx = new DB_TRX())
              {
                _terminal.GetEgmParams(((Int32)_row_processed["TerminalId"]), out _allow_cashable_ticket_out, out _allow_promotional_ticket_out, out _allow_ticket_in, _db_trx.SqlTransaction);
              }

              if (_processed)
              {
                switch (_ctx.request.MsgHeader.MsgType)
                {
                  case WCP_MsgTypes.WCP_TITO_MsgValidationNumber:
                    _request = (WCP_TITO_MsgValidationNumber)_ctx.request.MsgContent;
                    _response = (WCP_TITO_MsgValidationNumberReply)_ctx.response.MsgContent;

                    if (_request.TicketType == (Int32)TITO_TICKET_TYPE.CASHABLE && !_allow_cashable_ticket_out)
                    {
                      break;
                    }

                    if (_request.TicketType == (Int32)TITO_TICKET_TYPE.PROMO_NONREDEEM && !_allow_promotional_ticket_out)
                    {
                      break;
                    }

                    Int64 _validation_number;

                    _validation_number = _generator.ValidationNumber(false, (Int32)_row_processed["TerminalId"], _last_sequence);
                    if (_validation_number > 0)
                    {
                      _response.machine_id = (Int32)_row_processed["TerminalId"];
                      _response.system_id = (Int32)(_validation_number / 10000000000000000L);
                      _response.sequence_id = (Int32)(_last_sequence % 10000);
                      _response.validation_number_sequence = _validation_number % 10000000000000000L;

                      // FJC 07-10-2016. (PBI 18258)
                      if (GeneralParam.GetBoolean("TITO", "PendingPrint.Enabled", true))
                      {
                        _row_processed["NewSequenceValue"] = _response.validation_number_sequence;
                        _response.transaction_id = (Int64)_row_processed["TransactionId"];
                        _row_processed["SystemId"] = _response.system_id;

                        // FJC 07-10-2016. (PBI 18258)
                        // Convert Message: "ValidationNumber" into ==> Message: "CreateTicket"
                        if (!_error_inserting_ticket
                           && !ConvertWCPMsgValidNumberToWCPMsgCreateTicket(_row_processed, ref TCPClient, ref TitoRequest, ref TitoResponse))
                        {
                          _error_inserting_ticket = true;

                          Log.Error("Error in ConvertWCPMsgValidNumberToWCPMsgCreateTicket(). TerminalId:" + _response.machine_id + "| Amount:" + _request.AmountCents + "| TransactionId: " + _request.TransactionId);
                        }

                        if (!(((WCP_TITO_MsgCreateTicket)TitoRequest.MsgContent).AmountCents > 0))
                        {
                          _ctx.response.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_OK;

                          break;
                        }

                        // FJC 07-10-2016. (PBI 18258)
                        // Create one Ticket
                        if (!_error_inserting_ticket
                           && !WCP_TITO.TITO_ProcessCreateTicket(TCPClient, TitoRequest, TitoResponse, _response_valid_number, _db_trx_create_ticket.SqlTransaction))
                        {
                          _error_inserting_ticket = true;

                          Log.Error("Error in TITO_ProcessCreateTicket_Old(). TerminalId:" + _response.machine_id + "| Amount:" + _request.AmountCents + "| TransactionId: " + _request.TransactionId);
                        }
                        _response.transaction_id = _response_valid_number.transaction_id;

                        // FJC 07-10-2016. (PBI 18258)
                        if (_response.validation_number_sequence > 0)
                        {
                          _ctx.response.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_OK;
                        }
                      }
                      else
                      {
                        _ctx.response.MsgHeader.ResponseCode = WCP_ResponseCodes.WCP_RC_OK;
                      }

                      _num_validation_number_processed++;

                    }
                    break;
                }// switch (_ctx.request.MsgHeader.MsgType)
              }

              if (GeneralParam.GetBoolean("TITO", "PendingPrint.Enabled", true))
              {
                _ctx.response.MsgContent = _response;
                _row_processed["ResponseObject"] = _ctx.response;
              }
              else
              {
                // FJC 07-10-2016 (PBI 18258). We made send response after ticket creation.
                // Creation ticket and send response to SAS-HOST are in function: CreateTicketsFromDataTable()
                _ctx.tcp_client.Send(_ctx.response.ToXml());
              }

            }

            catch (Exception _ex)
            {
              Log.Exception(_ex);
            }
            finally
            {
              _last_sequence++;
            }
          } // foreach (DataRow _row in _table.Rows)

          if (GeneralParam.GetBoolean("TITO", "PendingPrint.Enabled", true))
          {
            if (!_error_inserting_ticket)
            {
              _db_trx_create_ticket.Commit();
            }
          }

        } // using (DB_TRX _db_trx_create_ticket = new DB_TRX())


        if (GeneralParam.GetBoolean("TITO", "PendingPrint.Enabled", true))
        {
          // FJC 07-10-2016 (PBI 18258). We made send response after ticket creation.
          if (!SendResponseTicketsFromDt(_table))
          {
            Log.Error("Error in SendResponseTicketsFromDt()");
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    } // while (true)
  } // TitoCreateTicketThread

  #endregion
}
