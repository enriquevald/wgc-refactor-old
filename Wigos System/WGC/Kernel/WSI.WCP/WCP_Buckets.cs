﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
//
//   MODULE NAME : WCP_Buckets.cs
//
//   DESCRIPTION : WCP_Buckets class. Erase and reload the table BUCKETS_MULTIPLIER_SCHEDULE
//
// REVISION HISTORY :
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 05-FEB-2013 XGJ    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading;
using WSI.Common;

namespace WSI.WCP
{
  public static class WCP_Buckets
  {

#region Atributes
    // Thread will execute ever 1 minutes
    private const Int32 WAIT_FOR_NEXT_TRY = (1 * 60 * 1000);    // 1 minutes in milisseconds
    private static Thread m_thread;
#endregion

    #region const

    private const String EXCEPTION_WCP_BUCKETMAINTENANCE_TASK = "Exception into class WCP_BucketMaintenance.";
    private const String EXCEPTION_MESSAGE = ". Message: ";

    #endregion

    # region public
    //------------------------------------------------------------------------------
    // PURPOSE : Initialize WCP_Buckets class 
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :

    public static void Init()
    {
      m_thread = new Thread(BucketsMultiplierMaintenance);
      m_thread.Name = "BucketsThread";

      m_thread.Start();
    }
    #endregion

#region Private

    /// <summary>
    /// Delete all rows from BUCKETS_MULTIPLIER_TO_APPLY
    /// Find all the active buckets for insert them to BUCKETS_MULTIPLIER_TO_APPLY
    /// </summary>
    private static void BucketsMultiplierMaintenance()
    {
      StringBuilder _sb;
      Int32 _wait_hint = 0;

        while (true)
        {
          try
          {
            Thread.Sleep(_wait_hint);

            if (!Services.IsPrincipal("WCP"))
            {
              _wait_hint = 1000;

              continue;
            }

            if (GeneralParam.GetBoolean("PlayerTracking.ExternalLoyaltyProgram", "Mode", false))
            {
              _wait_hint = 5 * 60 * 1000; // 5 minutes

              continue;
            }

            _sb = new StringBuilder();
            _wait_hint = WAIT_FOR_NEXT_TRY;

            using (DB_TRX _db_trx = new DB_TRX())
            {
              _sb.AppendLine("DELETE FROM BUCKETS_MULTIPLIER_TO_APPLY");

              using (SqlCommand _cmd = new SqlCommand(_sb.ToString()))
              {
                _db_trx.ExecuteNonQuery(_cmd);
                if (FindActiveBuckets(_db_trx.SqlTransaction))
                {
                  _db_trx.Commit();
                }
              }
              
            }
          }

          catch (Exception _ex)
          {
            Log.Exception(_ex);
            Log.Warning(String.Format(EXCEPTION_WCP_BUCKETMAINTENANCE_TASK + "BucketsUpdateThread" + EXCEPTION_MESSAGE + "{0} - {1} .", _ex.Message, WGDB.Now));
          }
        }
    } // BucketsMultiplierMaintenance

    /// <summary>
    /// Select all the active buckets from BUCKETS_MULTIPLIER_SCHEDULE
    /// </summary>
    /// <param name="Transaction">Databese transaction</param>
    /// <returns>True if everything is ok False otherwise</returns>
    private static bool FindActiveBuckets(SqlTransaction Transaction)
    {
      StringBuilder _sb;
      DataTable _dt;
      DateTime _date_time_now;
      bool _isOk;

      try
      {
        _sb = new StringBuilder();
        _dt = new DataTable();
        _date_time_now = WGDB.Now;
        _isOk = true; 

        _sb.AppendLine("SELECT * FROM BUCKETS_MULTIPLIER_SCHEDULE ");
        _sb.AppendLine("  WHERE   BM_BUCKET_ENABLED = 1 ");
        _sb.AppendLine("    AND   BM_BUCKET_SCHEDULE_START <= @pNow  ");
        _sb.AppendLine("    AND   (BM_BUCKET_SCHEDULE_END IS NULL OR BM_BUCKET_SCHEDULE_END > @pNow) ");
        _sb.AppendLine(" ORDER BY BM_BUCKET_ORDER");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Transaction.Connection, Transaction))
        {
          _cmd.Parameters.Add("@pNow", SqlDbType.DateTime).Value = _date_time_now;

          using (SqlDataAdapter _da = new SqlDataAdapter(_cmd))
          {
            _da.Fill(_dt);
            if (_dt.Rows.Count != 0)
            {
              foreach (DataRow _row in _dt.Rows)
              {
                if (CheckIsBucketActive(_row) && _isOk)
                {
                  _isOk = InsertActiveBuckets((Int64)_row[0], (decimal)_row[3], _row[17].ToString(), Transaction);
                }
              }
            }
          }
        }

        return _isOk;
      }
      catch (Exception ex)
      {
        Log.Exception(ex);

        return false;
      }
    } // FindActiveBuckets

    /// <summary>
    /// Determines if the bucket is active
    /// </summary>
    /// <param name="_bucketsRow"></param>
    /// <returns>TRUE if the bucket is active, FALSE otherwise </returns>
    private static bool CheckIsBucketActive(DataRow BucketsRow) 
    {
      DateTime _date_time_now;
      Boolean _is_day_of_week_in_schedule;
      DayOfWeek _day_of_week;
      Int32 _closing_time;
                
     _closing_time = GeneralParam.GetInt32("WigosGUI", "ClosingTime");
      _date_time_now = WGDB.Now;
      _day_of_week = Promotion.GetWeekDay(_date_time_now, _closing_time);
      _is_day_of_week_in_schedule = ScheduleDay.WeekDayIsInSchedule(_day_of_week, (Int32)BucketsRow["BM_BUCKET_SCHEDULE_WEEKDAY"]);

      // Check for enable Buckets
      //    If day of week is enable
      //       If Schedule is enable
      if (_is_day_of_week_in_schedule)
      {
        if (ScheduleDay.IsTimeInRange(_date_time_now.TimeOfDay, (Int32)BucketsRow["BM_BUCKET_SCHEDULE1_TIME_FROM"], (Int32)BucketsRow["BM_BUCKET_SCHEDULE1_TIME_TO"])
            || ((Boolean)BucketsRow["BM_BUCKET_SCHEDULE2_ENABLED"]
                && ScheduleDay.IsTimeInRange(_date_time_now.TimeOfDay, (Int32)BucketsRow["BM_BUCKET_SCHEDULE2_TIME_FROM"], (Int32)BucketsRow["BM_BUCKET_SCHEDULE2_TIME_TO"])))
        {
          return true;
        }
      }

      return false;
    } // CheckIsBucketActive

    /// <summary>
    /// Insert the bucket into BUCKETS_MULTIPLIER_TO APPLY
    /// </summary>
    /// <param name="_bucket_ID">Bucket ID</param>
    /// <param name="_bucket_afected_id">Bucket afected id</param>
    /// <param name="_multiplier">multiplier of the bucket</param>
    /// <param name="Transaction">Database transaction</param>
    /// <returns>>True if everything is ok False otherwise</returns>
    private static bool InsertActiveBuckets(Int64 Bucket_ID, decimal Multiplier, String Levels, SqlTransaction Transaction)
    {
      StringBuilder _sb;
      StringBuilder _sb_buckets_terminal;
      DataTable _dt;
      String _level;

      try
      {
        _sb = new StringBuilder();
        _dt = new DataTable();
        _level = string.Empty;

        _sb.AppendLine("SELECT * FROM TERMINAL_GROUPS ");
        _sb.AppendLine("WHERE TG_ELEMENT_ID = @bucket");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Transaction.Connection, Transaction))
        {
          _cmd.Parameters.Add("@bucket", SqlDbType.BigInt).Value = Bucket_ID;

          using (SqlDataAdapter _da = new SqlDataAdapter(_cmd))
          {
            _da.Fill(_dt);
            if (_dt.Rows.Count != 0)
            {
              foreach (DataRow _row in _dt.Rows)
              {
                if (!TerminalHasBucket((int)_row[0], Transaction))
                {
                  for (int _index = 1; _index<=Levels.Length; _index++ )
                  {
                    if (Levels.Substring(_index-1, 1) == "1")
                    {
                      _sb_buckets_terminal = new StringBuilder();
                      _sb_buckets_terminal.AppendLine("INSERT INTO  BUCKETS_MULTIPLIER_TO_APPLY ");
                      _sb_buckets_terminal.AppendLine("       VALUES (@bucket_id                ");
                      _sb_buckets_terminal.AppendLine("             , @terminal_id              ");
                      _sb_buckets_terminal.AppendLine("             , @level               ");
                      _sb_buckets_terminal.AppendLine("             , @multiplier)                   ");

                      using (SqlCommand _cmd_buckets_terminals = new SqlCommand(_sb_buckets_terminal.ToString(), Transaction.Connection, Transaction))
                      {
                        _cmd_buckets_terminals.Parameters.Add("@bucket_id", SqlDbType.BigInt).Value = Bucket_ID;
                        _cmd_buckets_terminals.Parameters.Add("@terminal_id", SqlDbType.Int).Value = Convert.ToInt16(_row[0].ToString());
                        _cmd_buckets_terminals.Parameters.Add("@multiplier", SqlDbType.Decimal).Value = Multiplier;
                        _cmd_buckets_terminals.Parameters.Add("@level", SqlDbType.NVarChar).Value = _index.ToString();

                        _cmd_buckets_terminals.ExecuteNonQuery();
                      }

                    }
                  }

                  _sb_buckets_terminal = null;
                }
              }
            }
          }
        }

        return true;
      }
      catch (Exception ex)
      {
        Log.Message("BucketId= " + Bucket_ID);
        Log.Exception(ex);

        return false;
      }
    } // InsertActiveBuckets

    /// <summary>
    /// If there are more than one bucket active by some terminal, only must to be apply de first (by order)
    /// </summary>
    /// <param name="_terminal_ID">Id of the terminal to find</param>
    /// <returns>TRUE if the terminal has another bucket active, FALSE otherwise</returns>
    private static bool TerminalHasBucket(int Terminal_ID, SqlTransaction Transaction)
    {

      bool _terminal_has_bucket;
      StringBuilder _sb;
      DataTable _dt;

      _terminal_has_bucket = false;
      try
      {

        _sb = new StringBuilder();
        _dt = new DataTable();
        

        _sb.AppendLine("SELECT * FROM BUCKETS_MULTIPLIER_TO_APPLY ");
        _sb.AppendLine("WHERE BMA_TERMINAL_ID = @terminal");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Transaction.Connection, Transaction))
        {
          _cmd.Parameters.Add("@terminal", SqlDbType.BigInt).Value = Terminal_ID;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (!_reader.Read())
              _terminal_has_bucket =  false;
            else 
            _terminal_has_bucket = true;
          }
        }
      }
      catch (Exception ex)
      {
        Log.Message("TerminalId = " + Terminal_ID);
        Log.Exception(ex);
      }

      return _terminal_has_bucket;
    } // TerminalHasBucket
    #endregion

  }
}
